#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_8(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[5];
    z[5]=c[3];
    z[6]=c[31];
    z[7]=d[1];
    z[8]= - z[2] + z[7];
    z[8]=z[3]*z[8];
    z[9]=z[3] - 2*z[4] + z[2];
    z[9]=i*z[1]*z[9];
    z[8]=z[8] + z[9];
    z[9]=z[2] - n<T>(34,27) + z[4];
    z[9]=z[4]*z[9];
    z[10]= - 6*z[7] + n<T>(85,9) - 2*z[2];
    z[10]=z[7]*z[10];
    z[11]=n<T>(8,3) + z[4];
    z[11]=z[5]*z[11];
    z[8]=n<T>(4,9)*z[6] + n<T>(2,3)*z[11] + 4*z[9] + z[10] + 2*z[8];

    r += 2*z[8];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_8(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_8(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d
);
#endif
