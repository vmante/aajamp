#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_85(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[57];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[18];
    z[20]=c[19];
    z[21]=c[23];
    z[22]=c[25];
    z[23]=c[26];
    z[24]=c[28];
    z[25]=c[29];
    z[26]=c[30];
    z[27]=c[31];
    z[28]=e[10];
    z[29]=e[3];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[7];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[14];
    z[38]=f[16];
    z[39]=f[17];
    z[40]=f[18];
    z[41]=f[23];
    z[42]=n<T>(1,4)*z[2];
    z[43]=z[1]*i;
    z[44]= - z[43] + n<T>(5,6)*z[2];
    z[44]=z[44]*z[42];
    z[45]=n<T>(1,3)*z[3];
    z[46]= - n<T>(4,3)*z[3] - n<T>(5,12)*z[43] + z[2];
    z[46]=z[46]*z[45];
    z[47]=n<T>(1,9)*z[5];
    z[48]= - n<T>(5,2)*z[5] + 5*z[43] - n<T>(1,4)*z[3];
    z[48]=z[48]*z[47];
    z[49]=n<T>(2,3)*z[7];
    z[50]=z[43] - z[5];
    z[51]=n<T>(71,3)*z[3] + z[2] + n<T>(23,3)*z[50];
    z[51]=n<T>(1,8)*z[51] + z[49];
    z[51]=z[7]*z[51];
    z[44]=n<T>(1,3)*z[51] + z[48] + z[44] + z[46];
    z[44]=z[7]*z[44];
    z[46]=n<T>(1,3)*z[2];
    z[48]=n<T>(5,6)*z[3] + z[43] - z[46];
    z[48]=z[3]*z[48];
    z[45]= - z[43] + z[45];
    z[45]=z[4]*z[45];
    z[51]= - 2*z[50] - z[7];
    z[51]=z[51]*z[49];
    z[52]=z[4] + n<T>(7,3)*z[3] - z[2] + z[50];
    z[49]= - n<T>(4,9)*z[8] + n<T>(1,2)*z[52] - z[49];
    z[49]=z[8]*z[49];
    z[52]= - n<T>(1,2)*z[5] + z[43] - z[3];
    z[52]=z[5]*z[52];
    z[45]=z[49] + z[51] + z[45] + z[48] + z[52];
    z[45]=z[8]*z[45];
    z[48]=n<T>(2,3)*z[4];
    z[49]=z[48] + z[8];
    z[49]=z[43]*z[49];
    z[49]=n<T>(2,3)*z[16] + z[49];
    z[49]=z[10]*z[49];
    z[51]=z[43] - z[4];
    z[51]=n<T>(2,3)*z[51] - z[8];
    z[51]=z[14]*z[51];
    z[44]=z[49] + z[44] + z[45] + z[51] - z[32];
    z[45]=z[43] + z[5];
    z[42]= - z[42] - n<T>(2,11)*z[45];
    z[42]=z[5]*z[42];
    z[49]= - n<T>(29,2)*z[43] + z[2];
    z[49]=z[2]*z[49];
    z[51]=n<T>(1,4)*z[43];
    z[52]=z[51] - z[2];
    z[52]=n<T>(1,11)*z[52] + n<T>(1,8)*z[3];
    z[52]=z[3]*z[52];
    z[45]=2*z[45] - z[4];
    z[45]=z[4]*z[45];
    z[53]= - n<T>(1,4)*z[7] - z[3] + n<T>(5,2)*z[43] - z[2];
    z[53]=z[7]*z[53];
    z[42]=n<T>(1,22)*z[53] + n<T>(2,11)*z[45] + n<T>(1,3)*z[42] + n<T>(1,33)*z[49]
     + 
    z[52];
    z[45]= - z[7] - n<T>(13,9)*z[5] + n<T>(61,9)*z[2] + z[3];
    z[45]= - z[48] + n<T>(1,8)*z[45];
    z[45]=z[6]*z[45];
    z[42]=n<T>(1,3)*z[42] + n<T>(1,11)*z[45];
    z[42]=z[6]*z[42];
    z[45]=2*z[43];
    z[49]= - n<T>(2,3)*z[5] + z[45] - z[46];
    z[49]=z[5]*z[49];
    z[52]= - n<T>(1,33)*z[3] - n<T>(1,3)*z[43] + n<T>(1,22)*z[2];
    z[52]=z[2]*z[52];
    z[53]= - n<T>(1,4) + n<T>(1,11)*z[43];
    z[53]= - n<T>(1,33)*z[4] - n<T>(5,66)*z[5] + n<T>(1,66)*z[3] + n<T>(1,3)*z[53]
     + n<T>(3,22)*z[2];
    z[53]=z[4]*z[53];
    z[49]=z[53] + n<T>(1,11)*z[49] + n<T>(1,6)*z[43] + z[52];
    z[49]=z[4]*z[49];
    z[52]= - n<T>(41,9)*z[2] + z[3];
    z[47]=n<T>(13,6)*z[8] + n<T>(1,18)*z[6] + n<T>(23,18)*z[7] + z[48] + n<T>(5,8)*z[52]
    + z[47];
    z[47]=z[16]*z[47];
    z[47]=z[47] - z[41];
    z[48]= - 16*z[43] + n<T>(23,8)*z[2];
    z[48]=z[2]*z[48];
    z[52]=10*z[43];
    z[53]= - z[52] + n<T>(37,8)*z[3];
    z[53]=z[3]*z[53];
    z[54]=8*z[2] + 7*z[3];
    z[54]=z[5]*z[54];
    z[48]=z[54] + z[48] + z[53];
    z[48]=z[5]*z[48];
    z[52]= - n<T>(23,4)*z[6] + z[52] - n<T>(17,4)*z[2];
    z[52]=z[15]*z[52];
    z[48]=z[35] + z[48] + z[52];
    z[52]=n<T>(1,11)*z[2];
    z[53]=3*z[43] - n<T>(1,6)*z[2];
    z[53]=z[53]*z[52];
    z[52]= - n<T>(41,11)*z[3] + z[52] + n<T>(5,2) - n<T>(41,33)*z[43];
    z[52]=z[3]*z[52];
    z[52]=z[53] + n<T>(1,6)*z[52];
    z[52]=z[3]*z[52];
    z[53]= - n<T>(85,132)*z[2] - n<T>(1,8) + n<T>(47,33)*z[43];
    z[53]=z[2]*z[53];
    z[51]=z[51] + z[53];
    z[46]=z[51]*z[46];
    z[46]=z[46] + n<T>(1,2)*z[52];
    z[51]=z[5] + z[3];
    z[43]= - z[43]*z[51];
    z[43]= - z[16] + z[43];
    z[43]=z[9]*z[43];
    z[50]= - z[3] + z[50];
    z[50]=z[13]*z[50];
    z[43]= - z[40] + z[43] + z[50];
    z[50]= - z[39] - z[38] + z[31] + z[20];
    z[51]=z[22] - z[19];
    z[52]=z[34] + z[30];
    z[53]= - n<T>(68,99)*z[23] - n<T>(2,9)*z[21] + n<T>(31,2376)*z[17];
    z[53]=i*z[53];
    z[54]=z[6] - z[2];
    z[54]= - n<T>(61,132)*z[7] - n<T>(5,12)*z[3] - n<T>(2,11)*z[54];
    z[54]=z[28]*z[54];
    z[45]=z[45] - z[2];
    z[55]= - z[4] + z[45];
    z[55]=z[11]*z[55];
    z[45]= - z[5] + z[45];
    z[45]=z[12]*z[45];
    z[56]= - z[3] - z[8];
    z[56]=z[29]*z[56];

    r +=  - n<T>(43,594)*z[18] + n<T>(56,99)*z[24] + n<T>(1,9)*z[25] + n<T>(4,9)*z[26]
       - n<T>(919,1188)*z[27] + n<T>(7,198)*z[33] + n<T>(5,66)*z[36] + n<T>(1,66)*z[37]
       + z[42] + n<T>(4,99)*z[43] + n<T>(1,11)*z[44] + n<T>(8,99)*z[45] + n<T>(1,2)*
      z[46] + n<T>(1,33)*z[47] + n<T>(1,99)*z[48] + z[49] + n<T>(2,33)*z[50] + n<T>(4,27)*z[51]
     - n<T>(4,33)*z[52]
     + z[53]
     + n<T>(1,3)*z[54]
     + n<T>(7,33)*z[55]
     + n<T>(5,33)*z[56];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_85(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_85(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
