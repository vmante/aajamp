#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_32(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,82>& f,
  const std::array<std::complex<T>,330>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[67];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[8];
    z[8]=c[12];
    z[9]=c[13];
    z[10]=c[20];
    z[11]=c[23];
    z[12]=c[32];
    z[13]=c[33];
    z[14]=c[35];
    z[15]=c[36];
    z[16]=c[37];
    z[17]=c[38];
    z[18]=c[39];
    z[19]=c[40];
    z[20]=c[43];
    z[21]=c[44];
    z[22]=c[47];
    z[23]=c[48];
    z[24]=c[49];
    z[25]=c[50];
    z[26]=c[55];
    z[27]=c[56];
    z[28]=c[57];
    z[29]=c[59];
    z[30]=c[81];
    z[31]=c[84];
    z[32]=f[24];
    z[33]=f[44];
    z[34]=f[69];
    z[35]=f[80];
    z[36]=f[22];
    z[37]=f[42];
    z[38]=d[2];
    z[39]=g[24];
    z[40]=g[27];
    z[41]=g[40];
    z[42]=g[48];
    z[43]=g[50];
    z[44]=g[79];
    z[45]=g[82];
    z[46]=g[90];
    z[47]=g[92];
    z[48]=g[99];
    z[49]=g[101];
    z[50]=g[170];
    z[51]=g[183];
    z[52]=g[189];
    z[53]=g[245];
    z[54]=g[270];
    z[55]=g[278];
    z[56]=g[281];
    z[57]=g[312];
    z[58]=z[3] - z[7];
    z[59]= - z[10] + n<T>(1,20)*z[11];
    z[60]= - n<T>(6,5)*z[9] + n<T>(1,2)*z[59];
    z[61]=n<T>(53,1080)*z[1] + 3*z[60];
    z[61]=z[61]*z[58];
    z[62]=z[2] - z[4];
    z[59]=n<T>(11,540)*z[1] + z[59] - n<T>(12,5)*z[9];
    z[59]=z[59]*z[62];
    z[62]=z[5] - z[6];
    z[60]=n<T>(31,1080)*z[1] + z[60];
    z[60]=z[60]*z[62];
    z[59]= - n<T>(1,12)*z[13] - 6*z[16] + n<T>(7,18)*z[17] + n<T>(109,648)*z[18]
     - n<T>(5,2)*z[21]
     - n<T>(157,1080)*z[22]
     + n<T>(12,5)*z[24]
     + 3*z[25]
     + z[27] - n<T>(3,8)
   *z[28] - n<T>(1,120)*z[29] - z[31] + n<T>(65,72)*z[30] + z[60] + z[59] + 
    z[61];
    z[59]=i*z[59];
    z[60]=z[36] - z[37];
    z[61]=z[60] + z[35];
    z[63]=n<T>(1,3)*z[8];
    z[64]=n<T>(1,4)*z[32];
    z[61]= - n<T>(1,4)*z[61] + n<T>(1,2)*z[33] + z[63] + z[64];
    z[61]= - z[61]*z[62];
    z[62]=z[33] - z[35] - z[34];
    z[62]= - z[64] + z[63] + n<T>(1,4)*z[62];
    z[62]=z[2]*z[62];
    z[65]=z[33] - z[34];
    z[60]=z[65] + z[60];
    z[58]=n<T>(1,4)*z[58];
    z[58]=z[60]*z[58];
    z[60]=z[35] - z[65];
    z[65]=n<T>(1,4)*z[38];
    z[60]=z[65]*z[60];
    z[65]=z[39] - z[20] - z[54] - z[55] - z[57] + n<T>(1,3)*z[56] - z[46] + 
    z[47] - z[50] - z[51] - z[40] + z[41] + z[44] + z[45];
    z[66]= - z[52] + z[42] + z[43] + z[48] - z[49];
    z[64]= - z[38]*z[64];
    z[63]= - z[63] + n<T>(1,2)*z[32];
    z[63]=z[4]*z[63];
    z[58]=z[59] + z[62] + z[63] + z[64] + n<T>(131,1080)*z[12] - n<T>(6,5)*z[14]
    - 2*z[15] + n<T>(5,3)*z[19] - n<T>(5,6)*z[23] + n<T>(1,40)*z[26] + z[53] + 
    z[60] + z[58] + z[61] + n<T>(1,6)*z[66] - n<T>(1,2)*z[65];

    r += z[58];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_32(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,82>& f,
  const std::array<std::complex<double>,330>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_32(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,82>& f,
  const std::array<std::complex<dd_real>,330>& g
);
#endif
