#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf298(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[13];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=c[13];
    z[4]=c[20];
    z[5]=c[23];
    z[6]=f[22];
    z[7]=f[24];
    z[8]=f[70];
    z[9]=f[77];
    z[10]=f[79];
    z[11]=n<T>(15,16)*z[5] - n<T>(75,4)*z[4] - 45*z[3];
    z[11]=i*z[11];
    z[12]=z[1]*i;
    z[12]= - n<T>(21247,216)*z[12] + 2689*z[2];
    z[11]=n<T>(1,2)*z[12] + z[11];
    z[11]=n<T>(3217,608)*z[10] - z[9] - n<T>(6881,608)*z[8] + n<T>(229,304)*z[7]
     + n<T>(1,38)*z[11]
     + z[6];

    r += n<T>(1,2)*z[11];
 
    return r;
}

template std::complex<double> qg_2lha_tf298(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf298(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
