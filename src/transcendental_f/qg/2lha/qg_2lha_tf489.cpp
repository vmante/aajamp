#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf489(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=c[12];
    z[5]=c[13];
    z[6]=c[20];
    z[7]=c[23];
    z[8]=f[44];
    z[9]=f[42];
    z[10]=f[77];
    z[11]=f[79];
    z[12]=f[81];
    z[13]=g[82];
    z[14]=g[90];
    z[15]=g[101];
    z[16]=g[131];
    z[17]=g[214];
    z[18]=g[245];
    z[19]=g[345];
    z[20]=g[347];
    z[21]=g[348];
    z[22]=z[3] - z[2];
    z[23]= - i*z[22];
    z[24]=5*i + 2*z[23];
    z[24]=z[6]*z[24];
    z[25]= - 1 + z[22];
    z[25]=z[8]*z[25];
    z[24]=z[14] + z[24] + z[25];
    z[25]= - n<T>(1,2)*i - n<T>(1,5)*z[23];
    z[25]=z[7]*z[25];
    z[25]= - z[13] - z[16] + z[25] + z[17];
    z[22]=2 + z[22];
    z[22]=z[4]*z[22];
    z[22]=z[11] - z[12] + z[22] + z[21];
    z[26]=z[9] + z[18] - z[10];
    z[27]=z[20] - z[15];
    z[28]= - n<T>(23,2)*i - n<T>(11,5)*z[23];
    z[28]=z[1]*z[28];
    z[23]=i + n<T>(2,5)*z[23];
    z[23]=z[5]*z[23];

    r += z[19] + 4*z[22] + 72*z[23] + 6*z[24] + 3*z[25] + 9*z[26] + 2*
      z[27] + n<T>(1,9)*z[28];
 
    return r;
}

template std::complex<double> qg_2lha_tf489(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf489(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
