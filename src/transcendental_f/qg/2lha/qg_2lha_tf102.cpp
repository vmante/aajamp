#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf102(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[7];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=f[69];
    z[4]=f[77];
    z[5]=f[81];
    z[6]=i*z[1];
    z[6]=n<T>(101,108)*z[6] + n<T>(3,8)*z[5] + 5*z[4] - n<T>(101,4)*z[2] - 8*z[3];

    r += 5*z[6];
 
    return r;
}

template std::complex<double> qg_2lha_tf102(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf102(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
