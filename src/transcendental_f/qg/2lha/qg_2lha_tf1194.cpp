#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1194(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[37];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[3];
    z[7]=d[4];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=e[0];
    z[11]=e[2];
    z[12]=e[3];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[31];
    z[18]=e[10];
    z[19]=f[2];
    z[20]=f[3];
    z[21]=f[4];
    z[22]=f[5];
    z[23]=f[11];
    z[24]=f[12];
    z[25]=f[13];
    z[26]=z[1]*i;
    z[27]=2*z[26];
    z[28]=z[27] + z[6];
    z[28]= - z[2] + 2*z[28];
    z[28]=z[28]*z[2];
    z[29]=z[26]*z[6];
    z[30]=npow(z[6],2);
    z[28]=z[28] - z[30] - 4*z[29];
    z[31]=z[2] - z[6];
    z[31]=2*z[31];
    z[32]= - z[3]*z[31];
    z[32]=z[32] + z[28];
    z[32]=z[3]*z[32];
    z[31]=z[4]*z[31];
    z[28]=z[31] - z[28];
    z[28]=z[4]*z[28];
    z[31]=z[4] - z[3];
    z[31]= - z[8] - 8*z[7] + n<T>(2,3)*z[31];
    z[31]=z[14]*z[31];
    z[28]=z[31] + z[32] + z[28];
    z[31]=z[27]*z[6];
    z[31]=z[31] - z[30];
    z[32]=z[26] - z[6];
    z[33]= - 2*z[5] - z[32];
    z[33]=z[5]*z[33];
    z[34]=z[5] + z[32];
    z[35]= - z[8]*z[34];
    z[33]=z[35] + z[33] - z[31];
    z[33]=z[8]*z[33];
    z[35]=z[5] + z[8];
    z[35]=z[18]*z[35];
    z[33]=z[21] + z[33] + z[35];
    z[35]=z[26] - z[5];
    z[35]= - z[5]*z[35];
    z[29]=z[35] - z[30] + z[29];
    z[29]=z[5]*z[29];
    z[30]= - z[8] + n<T>(2,3)*z[7];
    z[34]= - z[34]*z[30];
    z[35]=z[32] - z[5];
    z[36]=z[5]*z[35];
    z[31]= - 2*z[31] + z[36];
    z[31]=n<T>(1,3)*z[31] + z[34];
    z[31]=z[7]*z[31];
    z[27]=z[27] - z[2];
    z[34]=z[3] - z[27];
    z[34]=z[10]*z[34];
    z[27]= - z[4] + z[27];
    z[27]=z[13]*z[27];
    z[27]= - z[34] - z[27] - z[25] + z[23] + z[22] - z[20];
    z[30]=n<T>(2,3)*z[5] - z[32] + z[30];
    z[30]=z[12]*z[30];
    z[32]=z[5] + z[6];
    z[26]=z[26]*z[32];
    z[26]=z[14] + z[26];
    z[26]=z[9]*z[26];
    z[32]=z[24] - z[19];
    z[34]= - z[11]*z[35];
    z[35]=z[15]*i;

    r +=  - z[16] + n<T>(3,2)*z[17] + z[26] - n<T>(2,9)*z[27] + n<T>(1,9)*z[28] + 
      z[29] + z[30] + z[31] - n<T>(2,3)*z[32] + n<T>(1,3)*z[33] + z[34] + n<T>(1,6)
      *z[35];
 
    return r;
}

template std::complex<double> qg_2lha_tf1194(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1194(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
