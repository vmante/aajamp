#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf496(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[39];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=e[6];
    z[17]=f[30];
    z[18]=f[31];
    z[19]=f[32];
    z[20]=f[33];
    z[21]=f[36];
    z[22]=f[39];
    z[23]=f[43];
    z[24]=z[1]*i;
    z[25]=2*z[4];
    z[26]= - z[25] + 5*z[3];
    z[26]=z[26]*z[24];
    z[27]=npow(z[3],2);
    z[27]=n<T>(3,2)*z[27];
    z[28]=npow(z[4],2);
    z[29]=3*z[4];
    z[30]=z[29] - n<T>(5,2)*z[3];
    z[30]=z[8]*z[30];
    z[26]=z[30] + z[26] + z[28] - z[27];
    z[26]=z[8]*z[26];
    z[30]=2*z[3];
    z[31]= - z[8] + z[30];
    z[32]=z[3] - z[4];
    z[31]=z[32]*z[31];
    z[33]=2*z[24];
    z[34]= - z[4] - z[3];
    z[34]=z[34]*z[33];
    z[35]=z[24] - z[5];
    z[36]=z[4] + z[35];
    z[36]=z[5]*z[36];
    z[31]=z[36] + z[34] + z[28] + z[31];
    z[31]=z[5]*z[31];
    z[34]=z[7] + z[4];
    z[36]=n<T>(3,2)*z[3];
    z[37]=n<T>(1,2)*z[8];
    z[38]= - z[37] + n<T>(1,2)*z[24] - z[36] + z[34];
    z[38]=z[7]*z[38];
    z[25]= - z[25] + 3*z[3];
    z[37]= - z[37] + z[24] + z[25];
    z[37]=z[8]*z[37];
    z[25]= - z[25]*z[24];
    z[25]=z[38] + z[37] - z[27] + z[25];
    z[25]=z[7]*z[25];
    z[27]=z[5] + z[3];
    z[27]=z[24]*z[27];
    z[27]=z[13] + z[27];
    z[27]=z[6]*z[27];
    z[37]= - z[3] + z[35];
    z[37]=z[10]*z[37];
    z[34]=z[16]*z[34];
    z[27]= - z[27] - z[37] - z[34] + z[19] + z[17] - z[15];
    z[34]=z[8] + z[4];
    z[37]= - z[24]*z[34];
    z[37]= - z[13] + z[37];
    z[37]=z[9]*z[37];
    z[34]=z[24] - z[34];
    z[34]=z[12]*z[34];
    z[34]=z[37] + z[34];
    z[35]= - n<T>(1,2)*z[2] + z[8] - z[35];
    z[32]=z[32]*z[35];
    z[30]=z[4] - z[30];
    z[30]=z[3]*z[30];
    z[30]=z[28] + z[30] + z[32];
    z[30]=z[2]*z[30];
    z[29]= - z[29] + n<T>(23,3)*z[3];
    z[29]=z[3]*z[29];
    z[29]= - 3*z[28] + n<T>(1,2)*z[29];
    z[29]=z[3]*z[29];
    z[32]=z[4] + z[36];
    z[32]=z[3]*z[32];
    z[28]=z[28] + z[32];
    z[24]=z[28]*z[24];
    z[28]= - z[8] + z[2] - z[5];
    z[28]= - z[33] + 7*z[4] + 9*z[3] - 2*z[28];
    z[28]=z[11]*z[28];
    z[32]=npow(z[4],3);
    z[33]=n<T>(2,3)*z[7] + n<T>(1,2)*z[5] - n<T>(1,3)*z[8] - z[4] + n<T>(10,3)*z[3];
    z[33]=z[13]*z[33];
    z[35]=n<T>(1,2)*i;
    z[35]= - z[14]*z[35];

    r +=  - n<T>(7,2)*z[18] - 8*z[20] + n<T>(1,2)*z[21] - n<T>(3,2)*z[22] + z[23]
       + z[24] + z[25] + z[26] - 2*z[27] + z[28] + z[29] + z[30] + 
      z[31] + n<T>(2,3)*z[32] + z[33] + 4*z[34] + z[35];
 
    return r;
}

template std::complex<double> qg_2lha_tf496(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf496(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
