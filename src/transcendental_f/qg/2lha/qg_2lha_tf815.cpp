#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf815(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[73];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[7];
    z[7]=d[1];
    z[8]=d[3];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[15];
    z[12]=d[12];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[2];
    z[16]=e[5];
    z[17]=e[7];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[17];
    z[24]=c[18];
    z[25]=c[19];
    z[26]=c[21];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[29];
    z[32]=c[30];
    z[33]=c[31];
    z[34]=e[3];
    z[35]=e[10];
    z[36]=e[6];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[10];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[13];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[19];
    z[47]=f[30];
    z[48]=f[31];
    z[49]=f[32];
    z[50]=f[33];
    z[51]=f[34];
    z[52]=f[35];
    z[53]=f[36];
    z[54]=f[39];
    z[55]=f[43];
    z[56]=npow(z[8],2);
    z[57]=z[1]*i;
    z[58]=2*z[57];
    z[59]=z[58] + z[8];
    z[60]= - z[6] + z[59];
    z[60]=z[6]*z[60];
    z[61]=z[57] - z[8];
    z[62]=z[61] - z[6];
    z[63]=z[5] + z[62];
    z[63]=z[5]*z[63];
    z[64]=z[5] - 3*z[57] - z[6] - z[8];
    z[64]=n<T>(1,2)*z[64] + z[2];
    z[64]=z[2]*z[64];
    z[60]=z[64] + z[63] - n<T>(1,2)*z[56] + z[60];
    z[60]=z[2]*z[60];
    z[63]=n<T>(1,2)*z[8];
    z[64]=z[63] - z[57];
    z[65]=z[64]*z[8];
    z[66]=n<T>(1,2)*z[6];
    z[67]=z[66] - z[57];
    z[68]= - z[8] + z[67];
    z[69]=3*z[6];
    z[68]=z[68]*z[69];
    z[68]=z[65] + z[68];
    z[59]= - n<T>(1,2)*z[2] + z[59];
    z[59]=z[2]*z[59];
    z[70]=z[8] + z[69];
    z[70]=n<T>(1,4)*z[70] - z[2];
    z[70]=z[3]*z[70];
    z[59]=z[70] + n<T>(1,2)*z[68] + z[59];
    z[59]=z[3]*z[59];
    z[68]=z[8] + z[4];
    z[68]=z[57]*z[68];
    z[68]=z[20] + z[68];
    z[68]=z[12]*z[68];
    z[62]=z[62] + z[2];
    z[62]=7*z[4] + 5*z[5] - 2*z[62];
    z[62]=z[17]*z[62];
    z[58]=z[58] - z[2];
    z[70]=z[3] - z[58];
    z[70]=z[14]*z[70];
    z[71]= - z[4] + z[61];
    z[71]=z[16]*z[71];
    z[58]=z[6] - z[58];
    z[58]=z[18]*z[58];
    z[72]=z[5] + z[9];
    z[72]=z[36]*z[72];
    z[58]=z[58] + z[72] + z[55] - z[52] + z[62] + z[68] + z[70] + z[71]
    + z[60] + z[59] - z[47] + z[43] + z[39] - z[30];
    z[59]=n<T>(1,2)*z[9];
    z[60]= - z[59] - z[61];
    z[62]=3*z[9];
    z[60]=z[60]*z[62];
    z[60]= - z[65] + z[60];
    z[68]=n<T>(1,2)*z[7];
    z[70]=z[68] + z[61];
    z[70]=z[7]*z[70];
    z[62]= - z[62] + z[61];
    z[62]=n<T>(1,4)*z[62] + z[7];
    z[62]=z[10]*z[62];
    z[60]=z[62] + n<T>(1,2)*z[60] + z[70];
    z[60]=z[10]*z[60];
    z[62]=z[57] - n<T>(7,3)*z[8];
    z[56]=z[62]*z[56];
    z[62]=z[6]*z[63];
    z[62]=z[65] + z[62];
    z[62]=z[62]*z[69];
    z[70]= - z[7] - z[10];
    z[70]=z[35]*z[70];
    z[56]=z[60] + z[56] + z[62] + z[70] + z[40];
    z[60]=z[64]*z[63];
    z[62]= - z[67]*z[69];
    z[60]=z[60] + z[62];
    z[62]=3*z[5];
    z[64]=z[57] - z[6];
    z[67]=z[64]*z[62];
    z[70]=11*z[57] - 5*z[8];
    z[69]=n<T>(1,2)*z[70] - z[69];
    z[62]=n<T>(11,3)*z[9] + n<T>(1,2)*z[69] + z[62];
    z[62]=z[62]*z[59];
    z[60]=z[62] + n<T>(1,2)*z[60] + z[67];
    z[60]=z[9]*z[60];
    z[59]= - z[59] - z[64];
    z[59]=z[9]*z[59];
    z[62]= - n<T>(7,4)*z[6] + n<T>(7,2)*z[57] - z[8];
    z[62]=z[6]*z[62];
    z[67]= - z[8] - n<T>(5,2)*z[5];
    z[67]=z[5]*z[67];
    z[64]=z[64] - z[5];
    z[69]= - z[2]*z[64];
    z[59]=z[69] + n<T>(3,2)*z[59] + z[62] + z[67];
    z[62]= - z[2] + n<T>(1,2)*z[57] - z[66] + z[8];
    z[62]= - n<T>(3,4)*z[9] - 2*z[5] + n<T>(3,2)*z[62];
    z[62]=3*z[62] + n<T>(17,2)*z[4];
    z[62]=z[4]*z[62];
    z[59]=3*z[59] + z[62];
    z[59]=z[4]*z[59];
    z[62]=z[66] + z[57] + z[63];
    z[62]=3*z[62] + n<T>(7,2)*z[5];
    z[62]=z[5]*z[62];
    z[66]=2*z[6] - z[61];
    z[66]=z[6]*z[66];
    z[65]=z[65] + z[66];
    z[62]=3*z[65] + z[62];
    z[62]=z[5]*z[62];
    z[63]=z[63]*z[57];
    z[65]= - n<T>(11,3)*z[7] + z[9] - z[57] + 3*z[8];
    z[65]=z[65]*z[68];
    z[66]=z[9] + z[61];
    z[66]=z[9]*z[66];
    z[65]=z[65] - z[63] + z[66];
    z[65]=z[7]*z[65];
    z[66]=z[5] + z[6];
    z[67]= - z[57]*z[66];
    z[67]= - z[20] + z[67];
    z[67]=z[13]*z[67];
    z[64]=z[19]*z[64];
    z[64]=z[67] + z[64];
    z[57]= - z[7]*z[57];
    z[57]=n<T>(1,2)*z[20] + z[63] + z[57];
    z[57]=z[11]*z[57];
    z[63]=z[53] + z[41] + z[37] - z[25];
    z[67]=z[51] + z[31];
    z[68]=z[54] - z[45];
    z[69]= - z[9] - z[7];
    z[69]=z[34]*z[69];
    z[69]=z[69] - z[24];
    z[70]=z[27] + z[26];
    z[70]=9*z[29] + n<T>(9,4)*z[23] - n<T>(1,3)*z[21] + 3*z[70];
    z[70]=i*z[70];
    z[66]= - n<T>(1,4)*z[3] + n<T>(1,6)*z[10] - n<T>(1,3)*z[7] + 8*z[4] + z[9] - n<T>(3,8)*z[8]
     - z[66];
    z[66]=z[20]*z[66];
    z[61]= - n<T>(1,2)*z[61] - z[7];
    z[61]=z[15]*z[61];

    r += n<T>(55,8)*z[22] - n<T>(5,2)*z[28] + 6*z[32] - n<T>(167,16)*z[33] - n<T>(1,8)*
      z[38] + n<T>(13,8)*z[42] - n<T>(3,8)*z[44] + z[46] - n<T>(33,4)*z[48] - n<T>(9,2)
      *z[49] - 24*z[50] + n<T>(1,2)*z[56] + z[57] + 3*z[58] + z[59] + z[60]
       + z[61] + z[62] + n<T>(3,4)*z[63] + 9*z[64] + z[65] + z[66] + n<T>(3,2)*
      z[67] - n<T>(9,4)*z[68] + 2*z[69] + z[70];
 
    return r;
}

template std::complex<double> qg_2lha_tf815(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf815(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
