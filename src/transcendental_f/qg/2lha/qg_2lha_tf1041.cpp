#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1041(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[116];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[18];
    z[12]=d[4];
    z[13]=d[15];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[17];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[21];
    z[37]=c[23];
    z[38]=c[25];
    z[39]=c[26];
    z[40]=c[28];
    z[41]=c[29];
    z[42]=c[30];
    z[43]=c[31];
    z[44]=e[3];
    z[45]=e[6];
    z[46]=e[14];
    z[47]=f[0];
    z[48]=f[1];
    z[49]=f[2];
    z[50]=f[3];
    z[51]=f[4];
    z[52]=f[5];
    z[53]=f[6];
    z[54]=f[7];
    z[55]=f[8];
    z[56]=f[11];
    z[57]=f[12];
    z[58]=f[13];
    z[59]=f[14];
    z[60]=f[16];
    z[61]=f[17];
    z[62]=f[18];
    z[63]=f[21];
    z[64]=f[23];
    z[65]=f[30];
    z[66]=f[31];
    z[67]=f[32];
    z[68]=f[33];
    z[69]=f[34];
    z[70]=f[35];
    z[71]=f[36];
    z[72]=f[39];
    z[73]=f[43];
    z[74]=f[50];
    z[75]=f[51];
    z[76]=f[52];
    z[77]=f[53];
    z[78]=f[55];
    z[79]=f[56];
    z[80]=f[58];
    z[81]=f[60];
    z[82]=f[78];
    z[83]=n<T>(1,2)*z[5];
    z[84]=z[1]*i;
    z[85]=z[84] - n<T>(5,2)*z[5];
    z[85]=z[85]*z[83];
    z[86]=3*z[84];
    z[87]=z[86] - z[5];
    z[88]=2*z[2];
    z[89]=z[88] - z[87];
    z[89]=z[2]*z[89];
    z[90]=2*z[84];
    z[91]=n<T>(1,2)*z[2];
    z[92]= - z[8] + z[90] - z[91];
    z[92]=z[8]*z[92];
    z[93]=z[2] - z[5];
    z[94]= - n<T>(9,4)*z[6] - z[84] - z[93];
    z[94]=z[6]*z[94];
    z[95]=n<T>(1,3)*z[84];
    z[96]=z[8] - z[2];
    z[97]=z[95] + z[96];
    z[97]=n<T>(19,12)*z[9] + n<T>(1,2)*z[97] - 3*z[6];
    z[97]=z[9]*z[97];
    z[98]=z[84] - z[5];
    z[99]=z[98] + z[6];
    z[100]=n<T>(1,2)*z[3];
    z[101]=z[99]*z[100];
    z[102]=n<T>(1,2)*z[8];
    z[103]= - n<T>(5,12)*z[4] - n<T>(1,6)*z[9] + z[6] + z[102] + z[5] - n<T>(3,2)*
    z[2];
    z[103]=z[4]*z[103];
    z[87]= - 13*z[6] - z[8] + 3*z[2] - z[87];
    z[87]=n<T>(1,2)*z[87] - z[9];
    z[87]=n<T>(1,2)*z[87] + 3*z[10];
    z[87]=z[10]*z[87];
    z[85]=z[87] + z[101] + z[103] + z[97] + z[94] + z[92] + z[85] + 
    z[89];
    z[85]=z[10]*z[85];
    z[87]=3*z[8];
    z[89]=9*z[6];
    z[92]=n<T>(1,4)*z[9];
    z[94]=n<T>(1,6)*z[3];
    z[97]=n<T>(1,2)*z[4];
    z[101]= - 37*z[84] + z[5];
    z[101]=n<T>(1,6)*z[101] - z[2];
    z[101]= - n<T>(19,3)*z[12] + z[94] + z[97] - z[92] + z[89] + n<T>(1,2)*
    z[101] + z[87];
    z[103]=n<T>(1,2)*z[12];
    z[101]=z[101]*z[103];
    z[104]=z[83] - z[84];
    z[105]=z[104]*z[5];
    z[106]= - z[84] + z[102];
    z[87]=z[106]*z[87];
    z[106]=n<T>(1,2)*z[6];
    z[107]=z[84] - z[8];
    z[108]=z[106] + z[107];
    z[89]=z[108]*z[89];
    z[108]=n<T>(1,2)*z[9];
    z[109]= - z[108] - z[98];
    z[92]=z[109]*z[92];
    z[109]= - z[84]*z[97];
    z[110]=z[100] + z[98];
    z[110]=z[3]*z[110];
    z[87]=z[101] + n<T>(1,3)*z[110] + z[109] + z[92] + z[89] + n<T>(1,12)*z[105]
    + z[87];
    z[87]=z[87]*z[103];
    z[89]=n<T>(5,12)*z[9] - z[106] - n<T>(5,6)*z[84] - z[96];
    z[89]=z[89]*z[108];
    z[92]=n<T>(7,4)*z[2];
    z[101]=z[92] - n<T>(41,4)*z[84] - 3*z[5];
    z[101]=z[2]*z[101];
    z[109]=51*z[84] + 55*z[5];
    z[109]= - n<T>(51,8)*z[8] + n<T>(1,4)*z[109] - z[2];
    z[109]=z[109]*z[102];
    z[110]= - n<T>(3,8)*z[6] - n<T>(5,4)*z[84] + z[93];
    z[110]=z[6]*z[110];
    z[95]= - 3*z[4] + n<T>(5,3)*z[9] + 5*z[6] - n<T>(51,2)*z[8] + 41*z[2] + 
    z[95] - n<T>(31,2)*z[5];
    z[95]=z[4]*z[95];
    z[89]=n<T>(1,8)*z[95] + z[89] + z[110] + z[109] - n<T>(31,8)*z[105] + z[101]
   ;
    z[89]=z[4]*z[89];
    z[95]=z[90] - z[5];
    z[95]=z[5]*z[95];
    z[101]= - 2*z[98] - z[2];
    z[101]=z[2]*z[101];
    z[95]=z[95] + z[101];
    z[86]= - n<T>(13,3)*z[8] + z[86] + n<T>(4,3)*z[93];
    z[86]=z[8]*z[86];
    z[101]=2*z[6];
    z[109]=z[101] - z[93];
    z[109]=z[6]*z[109];
    z[103]= - z[103] - z[107];
    z[103]=z[12]*z[103];
    z[110]=4*z[2];
    z[111]= - z[110] - 5*z[84] - n<T>(7,2)*z[5];
    z[111]= - n<T>(13,18)*z[7] + n<T>(23,6)*z[6] + n<T>(1,3)*z[111] - n<T>(3,2)*z[8];
    z[111]=z[7]*z[111];
    z[86]=z[111] + 3*z[103] + n<T>(4,3)*z[109] + n<T>(2,3)*z[95] + z[86];
    z[86]=z[7]*z[86];
    z[95]= - z[102] - z[90] + z[93];
    z[95]=z[8]*z[95];
    z[103]= - n<T>(1,2)*z[98] - z[2];
    z[103]=z[103]*z[91];
    z[88]=n<T>(5,4)*z[9] + 2*z[8] - n<T>(7,4)*z[98] - z[88];
    z[88]=z[9]*z[88];
    z[109]=n<T>(1,4)*z[84] + z[5];
    z[109]=z[5]*z[109];
    z[88]=z[88] + z[95] + z[109] + z[103];
    z[95]= - z[99]*z[106];
    z[97]=z[97] - z[84];
    z[99]=z[4]*z[97];
    z[103]=z[84] + z[2];
    z[103]= - z[5] + n<T>(5,2)*z[103];
    z[103]= - n<T>(11,6)*z[3] - n<T>(7,4)*z[9] + n<T>(1,2)*z[103] + z[8];
    z[94]=z[103]*z[94];
    z[88]=z[94] + n<T>(1,4)*z[99] + z[95] + n<T>(1,3)*z[88];
    z[88]=z[3]*z[88];
    z[94]=4*z[5];
    z[95]=n<T>(43,4)*z[8] - z[110] - n<T>(43,2)*z[84] + z[94];
    z[95]=z[8]*z[95];
    z[99]=n<T>(25,2)*z[84] - z[94];
    z[99]=z[5]*z[99];
    z[83]=n<T>(17,4)*z[2] - 8*z[84] + z[83];
    z[83]=z[2]*z[83];
    z[83]=z[95] + z[99] + z[83];
    z[95]= - 1 - z[84];
    z[95]=9*z[8] + n<T>(23,3)*z[2] + n<T>(11,2)*z[95] - n<T>(29,3)*z[5];
    z[95]=n<T>(1,4)*z[95] - n<T>(16,9)*z[6];
    z[95]=z[6]*z[95];
    z[83]=n<T>(1,3)*z[83] + z[95];
    z[83]=z[6]*z[83];
    z[95]=2*z[5];
    z[92]= - z[92] - n<T>(7,2)*z[84] + z[95];
    z[92]=z[2]*z[92];
    z[95]= - n<T>(7,4)*z[8] + n<T>(7,2)*z[2] - z[84] - z[95];
    z[95]=z[8]*z[95];
    z[92]=z[95] - n<T>(17,8)*z[105] + z[92];
    z[95]=7*z[84] - 17*z[5];
    z[95]=n<T>(1,8)*z[95] + z[96];
    z[95]=n<T>(71,18)*z[9] + n<T>(1,3)*z[95] - n<T>(31,4)*z[6];
    z[95]=z[95]*z[108];
    z[96]=z[84] + n<T>(13,2)*z[6];
    z[96]=z[6]*z[96];
    z[92]=z[95] + n<T>(1,3)*z[92] + n<T>(1,4)*z[96];
    z[92]=z[9]*z[92];
    z[95]=z[84]*z[4];
    z[95]=z[95] + z[30];
    z[96]=z[12]*z[84];
    z[96]=z[96] + z[95];
    z[96]=z[15]*z[96];
    z[99]=n<T>(25,3)*z[5] + n<T>(11,2) - 9*z[84];
    z[99]=z[99]*npow(z[5],2);
    z[103]=z[84] - z[4];
    z[106]= - z[12] + z[103];
    z[106]=z[21]*z[106];
    z[96]=z[47] - z[106] - z[99] - z[96] + z[76] - z[63];
    z[99]=z[107] + z[93];
    z[106]= - 29*z[7] + 8*z[99] - 37*z[6];
    z[106]=z[23]*z[106];
    z[99]= - n<T>(13,4)*z[3] - n<T>(17,4)*z[9] + z[99];
    z[99]=z[26]*z[99];
    z[99]=z[74] + z[106] + z[99];
    z[106]=19*z[2] - n<T>(25,4)*z[5] + z[107];
    z[106]=z[106]*z[102];
    z[108]=n<T>(13,4)*z[2] - 21*z[84] - n<T>(11,2)*z[5];
    z[108]=z[2]*z[108];
    z[105]=z[106] - n<T>(33,4)*z[105] + z[108];
    z[105]=z[105]*z[102];
    z[106]=z[94] + n<T>(11,4);
    z[101]=z[101] - 4*z[84] + z[106];
    z[101]=z[22]*z[101];
    z[108]= - z[5] - z[3];
    z[108]=z[84]*z[108];
    z[108]= - z[30] + z[108];
    z[108]=z[13]*z[108];
    z[109]= - z[3] + z[98];
    z[109]=z[20]*z[109];
    z[108]=z[108] + z[109] + z[79] - z[53];
    z[109]=z[8] + z[7];
    z[109]=z[84]*z[109];
    z[109]=z[30] + z[109];
    z[109]=z[17]*z[109];
    z[107]=z[7] - z[107];
    z[107]=z[25]*z[107];
    z[107]=z[109] + z[107];
    z[109]=z[10]*z[84];
    z[95]=z[109] + z[95];
    z[95]=z[14]*z[95];
    z[103]= - z[10] + z[103];
    z[103]=z[28]*z[103];
    z[95]=z[95] + z[103];
    z[94]= - n<T>(19,4)*z[84] + z[94];
    z[94]=z[5]*z[94];
    z[103]= - n<T>(163,6)*z[2] + 63*z[84] + 5*z[5];
    z[103]=z[2]*z[103];
    z[94]=z[94] + n<T>(1,4)*z[103];
    z[94]=z[2]*z[94];
    z[103]=n<T>(1,2)*z[30];
    z[109]= - n<T>(145,36)*z[12] + n<T>(5,9)*z[10] + n<T>(5,18)*z[3] + n<T>(1,12)*z[4]
    + n<T>(23,9)*z[9] - n<T>(65,3)*z[6] + n<T>(275,36)*z[8] - z[5] + n<T>(41,3)*z[2];
    z[109]=n<T>(1,2)*z[109] + n<T>(5,9)*z[7];
    z[109]=z[109]*z[103];
    z[98]=2*z[7] + n<T>(13,2)*z[10] - z[100] + n<T>(11,2)*z[9] + n<T>(3,2)*z[98]
     + 7
   *z[6];
    z[98]=z[29]*z[98];
    z[93]=z[3] + z[93] + z[102] + n<T>(3,2)*z[9];
    z[100]=z[84]*z[93];
    z[100]=z[103] + z[100];
    z[100]=z[11]*z[100];
    z[93]=n<T>(1,2)*z[84] - z[93];
    z[93]=z[27]*z[93];
    z[102]= - z[84]*z[106];
    z[90]= - z[6]*z[90];
    z[90]= - 4*z[30] + z[90] + z[102];
    z[90]=z[16]*z[90];
    z[97]= - z[91] - z[97];
    z[97]=z[18]*z[97];
    z[91]= - z[91] - z[104];
    z[91]=z[19]*z[91];
    z[102]=z[54] + z[73] - z[70];
    z[103]=z[34] + z[64] - z[49];
    z[104]=z[41] + z[59] - z[48];
    z[106]=z[71] - z[69];
    z[110]=z[81] - z[42];
    z[111]=z[82] - z[55];
    z[112]= - z[12] - z[7];
    z[112]=z[45]*z[112];
    z[112]=z[112] + z[65];
    z[113]=z[3] + z[12];
    z[113]=z[44]*z[113];
    z[113]=z[113] - z[62];
    z[114]= - n<T>(159,4)*z[39] - n<T>(137,8)*z[37] + n<T>(11,4)*z[36] - z[33] + n<T>(31,36)*z[31];
    z[114]=i*z[114];
    z[84]=z[10] - z[9] - n<T>(29,4)*z[8] - n<T>(25,4)*z[2] + n<T>(29,2)*z[84] - z[5]
   ;
    z[84]=z[24]*z[84];
    z[115]=z[9] + z[10];
    z[115]=z[46]*z[115];

    r +=  - n<T>(101,24)*z[32] - n<T>(11,16)*z[35] + n<T>(83,12)*z[38] + n<T>(73,4)*
      z[40] + n<T>(385,16)*z[43] - n<T>(31,16)*z[50] - n<T>(25,48)*z[51] - n<T>(13,4)*
      z[52] - n<T>(33,16)*z[56] - n<T>(1,48)*z[57] - 2*z[58] - n<T>(1,16)*z[60] - 
      n<T>(51,16)*z[61] + n<T>(59,12)*z[66] + n<T>(5,2)*z[67] + n<T>(32,3)*z[68]
     + n<T>(9,4)
      *z[72] + n<T>(5,8)*z[75] - 12*z[77] - n<T>(5,24)*z[78] - n<T>(31,8)*z[80] + 
      z[83] + z[84] + z[85] + z[86] + z[87] + z[88] + z[89] + z[90] + 9
      *z[91] + z[92] + z[93] + z[94] + n<T>(5,6)*z[95] - n<T>(1,4)*z[96] + n<T>(27,2)*z[97]
     + z[98]
     + n<T>(1,3)*z[99]
     + z[100]
     + z[101] - n<T>(4,3)*z[102]
       + n<T>(1,6)*z[103] - n<T>(1,8)*z[104] + z[105] - n<T>(3,4)*z[106] + n<T>(17,3)*
      z[107] + n<T>(1,2)*z[108] + z[109] - n<T>(3,2)*z[110] + 4*z[111] + 3*
      z[112] + n<T>(1,12)*z[113] + z[114] + n<T>(25,6)*z[115];
 
    return r;
}

template std::complex<double> qg_2lha_tf1041(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1041(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
