#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1185(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[70];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[18];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[3];
    z[15]=e[8];
    z[16]=e[10];
    z[17]=e[11];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[30];
    z[27]=c[31];
    z[28]=f[0];
    z[29]=f[1];
    z[30]=f[3];
    z[31]=f[4];
    z[32]=f[5];
    z[33]=f[6];
    z[34]=f[7];
    z[35]=f[8];
    z[36]=f[9];
    z[37]=f[10];
    z[38]=f[11];
    z[39]=f[12];
    z[40]=f[13];
    z[41]=f[14];
    z[42]=f[15];
    z[43]=f[16];
    z[44]=f[17];
    z[45]=f[18];
    z[46]=f[23];
    z[47]=f[26];
    z[48]=f[27];
    z[49]=n<T>(1,3)*z[4];
    z[50]=n<T>(1,3)*z[8];
    z[51]=z[50] + z[6];
    z[52]= - n<T>(11,3)*z[7] + z[49] - z[51];
    z[53]=n<T>(1,3)*z[3];
    z[54]=n<T>(1,3)*z[5];
    z[52]=n<T>(5,12)*z[2] + z[53] + n<T>(1,4)*z[52] - z[54];
    z[52]=z[2]*z[52];
    z[55]=2*z[16];
    z[56]=z[12] + 5*z[11];
    z[56]= - 2*z[15] + n<T>(5,4)*z[56] - z[55];
    z[57]=n<T>(1,2)*z[4];
    z[58]=z[6] - n<T>(7,6)*z[4];
    z[58]=z[58]*z[57];
    z[59]= - z[51] - n<T>(5,3)*z[7] + n<T>(7,3)*z[4];
    z[59]=n<T>(1,2)*z[59];
    z[60]=n<T>(2,3)*z[5];
    z[61]= - z[59] - z[60];
    z[61]=z[5]*z[61];
    z[62]=n<T>(1,3)*z[7];
    z[63]=z[62] + z[4];
    z[51]= - z[63] - z[51];
    z[51]= - n<T>(1,6)*z[3] + n<T>(1,2)*z[51] + z[54];
    z[51]=z[3]*z[51];
    z[64]=npow(z[6],2);
    z[65]=z[14] + n<T>(1,4)*z[64];
    z[66]=npow(z[8],2);
    z[67]=n<T>(7,12)*z[66];
    z[68]=n<T>(5,12)*z[7] + n<T>(1,6)*z[8] + z[4];
    z[68]=z[7]*z[68];
    z[51]=z[52] + z[51] + z[61] + z[68] + z[58] + z[67] + n<T>(167,144)*
    z[18] + n<T>(1,3)*z[56] + z[65];
    z[51]=z[2]*z[51];
    z[52]=z[6] + z[57];
    z[52]=z[52]*z[57];
    z[56]=n<T>(1,6)*z[7];
    z[58]=z[8] - n<T>(5,2)*z[7];
    z[58]=z[58]*z[56];
    z[61]= - n<T>(13,3)*z[8] + z[63] + z[6];
    z[60]=n<T>(1,2)*z[61] + z[60];
    z[60]=z[5]*z[60];
    z[54]= - n<T>(11,9)*z[3] - n<T>(1,4)*z[61] + z[54];
    z[54]=z[3]*z[54];
    z[55]= - z[13] + z[55];
    z[52]=z[54] + z[60] + z[58] + z[52] + n<T>(5,12)*z[66] + n<T>(7,4)*z[64] - n<T>(7,18)*z[18]
     - 3*z[14]
     + n<T>(1,3)*z[55]
     + z[15];
    z[52]=z[3]*z[52];
    z[54]=n<T>(2,3)*z[16];
    z[55]= - z[14] + z[54] - z[64];
    z[58]=n<T>(5,3)*z[4];
    z[60]= - z[6] + z[58];
    z[60]=z[60]*z[57];
    z[61]= - z[50] - z[63];
    z[63]=n<T>(1,2)*z[7];
    z[61]=z[61]*z[63];
    z[56]= - z[56] + n<T>(5,6)*z[4] - z[6] + z[50];
    z[56]=z[5]*z[56];
    z[66]= - z[13] + n<T>(5,4)*z[12];
    z[68]=z[50] + n<T>(3,2)*z[6];
    z[69]=z[8]*z[68];
    z[56]=z[56] + z[61] + z[60] + z[69] + n<T>(1,3)*z[66] + z[11] + z[55];
    z[56]=z[5]*z[56];
    z[60]=z[6] - z[57];
    z[60]=z[60]*z[57];
    z[61]= - z[63] + z[57] - z[10] + n<T>(7,6)*z[8];
    z[61]=z[7]*z[61];
    z[66]= - z[10] - z[68];
    z[66]=z[8]*z[66];
    z[55]=z[61] + z[60] + z[66] + n<T>(10,3)*z[15] - n<T>(25,6)*z[11] - n<T>(5,6)*
    z[12] - z[17] + n<T>(1,3)*z[13] - z[55];
    z[55]=z[1]*z[55];
    z[60]=n<T>(1,3)*z[9];
    z[58]=z[62] - z[58] - n<T>(2,3)*z[8] - z[60] + 2*z[6];
    z[61]=z[5]*z[1];
    z[58]=z[58]*z[61];
    z[62]=z[3] - z[6];
    z[57]=n<T>(5,6)*z[7] - z[57] + n<T>(13,6)*z[8] - z[60] + n<T>(1,2)*z[62];
    z[57]=z[1]*z[57];
    z[57]= - 2*z[61] + z[57];
    z[57]=z[3]*z[57];
    z[53]= - n<T>(1,12)*z[2] - z[53] + z[59];
    z[53]=z[1]*z[53];
    z[53]=n<T>(7,3)*z[61] + z[53];
    z[53]=z[2]*z[53];
    z[59]= - n<T>(1,3)*z[19] + 5*z[24] + n<T>(1,4)*z[22];
    z[53]=z[53] + z[57] + z[58] + n<T>(1,3)*z[59] + z[55];
    z[53]=i*z[53];
    z[50]=z[50] + z[4];
    z[50]= - n<T>(1,2)*z[50] + z[7];
    z[50]=z[50]*z[63];
    z[55]=npow(z[4],2);
    z[50]=z[50] - n<T>(1,4)*z[55] - z[67] + n<T>(1,18)*z[18] - n<T>(5,3)*z[15] - 
    z[11] + z[54] + z[17];
    z[50]=z[7]*z[50];
    z[54]= - 3*z[6] + n<T>(25,18)*z[8];
    z[54]=z[8]*z[54];
    z[54]=n<T>(1,4)*z[54] - n<T>(3,4)*z[64] - n<T>(17,32)*z[18] - z[15] + z[17] + n<T>(4,3)*z[16];
    z[54]=z[8]*z[54];
    z[49]= - z[6] + z[49];
    z[49]=z[4]*z[49];
    z[49]=n<T>(1,4)*z[49] - n<T>(55,144)*z[18] + n<T>(25,12)*z[11] - z[15] - z[65];
    z[49]=z[4]*z[49];
    z[55]=z[33] + z[39];
    z[57]=z[29] - z[41];
    z[58]=z[20] + z[37];
    z[59]= - z[48] - 3*z[47];
    z[59]= - z[23] - z[43] - z[44] + n<T>(11,6)*z[45] + n<T>(1,2)*z[59] - n<T>(1,3)*
    z[46];
    z[60]= - z[10] - z[60];
    z[60]=z[18]*z[60];
    z[61]=n<T>(5,24)*z[64] - 4*z[14] + n<T>(41,32)*z[18];
    z[61]=z[6]*z[61];

    r +=  - n<T>(107,144)*z[21] - n<T>(5,6)*z[25] + 2*z[26] - n<T>(34,9)*z[27] + n<T>(17,8)*z[28]
     + n<T>(7,12)*z[30]
     + n<T>(7,24)*z[31]
     + n<T>(11,24)*z[32]
     + n<T>(8,3)*
      z[34] - n<T>(23,8)*z[35] + n<T>(3,8)*z[36] - n<T>(1,6)*z[38] - n<T>(13,24)*z[40]
       + 4*z[42] + z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + n<T>(5,8)
      *z[55] + z[56] - n<T>(1,8)*z[57] + n<T>(4,3)*z[58] + n<T>(1,4)*z[59] + z[60]
       + z[61];
 
    return r;
}

template std::complex<double> qg_2lha_tf1185(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1185(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
