#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1268(
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[7];
  std::complex<T> r(0,0);

    z[1]=d[0];
    z[2]=d[2];
    z[3]=d[3];
    z[4]=d[7];
    z[5]= - z[4] + z[1];
    z[6]=z[3] - z[2];

    r += z[6]*z[5];
 
    return r;
}

template std::complex<double> qg_2lha_tf1268(
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1268(
  const std::array<std::complex<dd_real>,25>& d
);
#endif
