#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1309(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[45];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[18];
    z[13]=c[19];
    z[14]=c[23];
    z[15]=c[25];
    z[16]=c[26];
    z[17]=c[28];
    z[18]=c[29];
    z[19]=c[30];
    z[20]=c[31];
    z[21]=f[0];
    z[22]=f[3];
    z[23]=f[5];
    z[24]=f[8];
    z[25]=f[11];
    z[26]=f[13];
    z[27]=f[16];
    z[28]=f[17];
    z[29]=f[18];
    z[30]=f[26];
    z[31]=f[27];
    z[32]= - z[2] + 2*z[4];
    z[33]=2*z[2];
    z[32]=z[32]*z[33];
    z[33]=npow(z[4],2);
    z[32]=z[32] + n<T>(7,4)*z[33];
    z[34]= - 11*z[2] + n<T>(7,2)*z[4];
    z[35]=n<T>(5,2)*z[3];
    z[36]=z[35] + n<T>(1,3)*z[34];
    z[36]=z[36]*z[5];
    z[37]=n<T>(1,2)*z[3];
    z[38]= - z[4] + z[37];
    z[35]=z[38]*z[35];
    z[35]=n<T>(1,2)*z[36] + n<T>(1,3)*z[32] + z[35];
    z[35]=z[5]*z[35];
    z[38]=n<T>(1,3)*z[3];
    z[39]= - z[34]*z[38];
    z[40]=7*z[4] - n<T>(29,2)*z[2];
    z[40]=z[2]*z[40];
    z[36]= - z[36] + z[39] + n<T>(1,4)*z[33] + n<T>(1,3)*z[40];
    z[39]=z[1]*i;
    z[36]=z[36]*z[39];
    z[34]=z[34]*z[37];
    z[32]=z[34] + z[32];
    z[32]=z[32]*z[38];
    z[34]= - n<T>(4,3)*z[4] + n<T>(5,2)*z[2];
    z[34]=z[2]*z[34];
    z[33]= - n<T>(7,6)*z[33] + z[34];
    z[33]=z[2]*z[33];
    z[34]=z[4] + z[2];
    z[34]=n<T>(1,2)*z[34] - z[39];
    z[34]=z[7]*z[34];
    z[37]= - z[29] + z[31] + z[30];
    z[38]=z[26] + z[25] + z[23] + z[22];
    z[40]=z[24] + z[21];
    z[41]=n<T>(23,3)*z[16] + n<T>(29,6)*z[14] + n<T>(1,6)*z[10];
    z[41]=i*z[41];
    z[42]=npow(z[4],3);
    z[39]= - z[4] - n<T>(8,3)*z[39] + n<T>(4,3)*z[2];
    z[43]=z[5] + n<T>(4,3)*z[3] + z[39];
    z[43]=z[6]*z[43];
    z[39]=n<T>(4,3)*z[5] + z[3] + z[39];
    z[39]=z[8]*z[39];
    z[44]=z[5] + z[3];
    z[44]=n<T>(43,16)*z[4] - 4*z[2] - n<T>(7,4)*z[44];
    z[44]=z[9]*z[44];

    r += n<T>(2,3)*z[11] + n<T>(8,3)*z[12] + n<T>(5,18)*z[13] - n<T>(19,6)*z[15]
     - n<T>(47,6)*z[17]
     - 2*z[18]
     - 8*z[19]
     + n<T>(152,9)*z[20] - z[27]
     + n<T>(3,2)*
      z[28] + z[32] + z[33] + n<T>(7,3)*z[34] + z[35] + z[36] + n<T>(1,4)*z[37]
       + n<T>(7,12)*z[38] + z[39] - n<T>(3,4)*z[40] + z[41] - n<T>(1,12)*z[42] + 
      z[43] + n<T>(1,9)*z[44];
 
    return r;
}

template std::complex<double> qg_2lha_tf1309(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1309(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
