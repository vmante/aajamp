#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1236(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[64];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=e[0];
    z[12]=e[2];
    z[13]=e[4];
    z[14]=e[8];
    z[15]=e[10];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[18];
    z[20]=c[19];
    z[21]=c[23];
    z[22]=c[25];
    z[23]=c[26];
    z[24]=c[28];
    z[25]=c[29];
    z[26]=c[30];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=f[0];
    z[30]=f[1];
    z[31]=f[2];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[6];
    z[35]=f[7];
    z[36]=f[9];
    z[37]=f[10];
    z[38]=f[11];
    z[39]=f[12];
    z[40]=f[13];
    z[41]=f[14];
    z[42]=f[16];
    z[43]=f[17];
    z[44]=f[18];
    z[45]=f[21];
    z[46]=f[23];
    z[47]=z[1]*i;
    z[48]=z[47]*z[5];
    z[49]=npow(z[5],2);
    z[50]= - z[48] + n<T>(1,2)*z[49];
    z[51]=3*z[50];
    z[52]=n<T>(1,2)*z[7];
    z[53]=z[47] - z[5];
    z[54]=z[2] + z[53];
    z[55]=z[52] - z[3] - z[54];
    z[55]=z[7]*z[55];
    z[56]=n<T>(1,2)*z[6];
    z[57]=z[7] + 9*z[2] - 3*z[5] - z[3];
    z[57]=z[57]*z[56];
    z[58]=z[53] + n<T>(1,2)*z[3];
    z[58]=z[58]*z[3];
    z[59]=z[5] + 3*z[47];
    z[60]=n<T>(3,2)*z[2] - 3*z[59] + z[3];
    z[60]=z[2]*z[60];
    z[55]=z[57] + z[55] + z[60] - z[51] + z[58];
    z[55]=z[55]*z[56];
    z[57]= - z[58] + z[50];
    z[52]=z[52] + z[53];
    z[52]=z[7]*z[52];
    z[60]=n<T>(1,2)*z[4];
    z[61]= - z[47]*z[60];
    z[52]=z[61] + n<T>(1,2)*z[57] + z[52];
    z[57]= - 3*z[3] - z[54];
    z[57]=z[60] + n<T>(1,2)*z[57] + z[7];
    z[57]=3*z[57] - z[8];
    z[57]=z[8]*z[57];
    z[52]=3*z[52] + n<T>(1,2)*z[57];
    z[52]=z[8]*z[52];
    z[57]=z[8] + z[4];
    z[61]=z[47]*z[57];
    z[61]=z[16] + z[61];
    z[61]=z[10]*z[61];
    z[57]=z[47] - z[57];
    z[57]=z[13]*z[57];
    z[57]=z[61] + z[57] + z[31] - z[29] + z[25] - z[20] + z[45] - z[43]
    + z[42] - z[40];
    z[61]= - z[2] + 2*z[47];
    z[62]= - z[4] + z[61];
    z[62]=z[11]*z[62];
    z[61]= - z[6] + z[61];
    z[61]=z[14]*z[61];
    z[63]=z[3] + z[8];
    z[63]=z[28]*z[63];
    z[61]=z[62] + z[61] + z[63] - z[18];
    z[62]= - z[5] + n<T>(11,3)*z[3];
    z[62]=z[3]*z[62];
    z[49]=z[62] - 2*z[49] + 3*z[48];
    z[49]=z[3]*z[49];
    z[62]= - n<T>(1,4)*z[3] + n<T>(1,2)*z[5] + z[47];
    z[62]=z[3]*z[62];
    z[63]=z[5] + n<T>(9,2)*z[47];
    z[63]= - n<T>(13,4)*z[2] + n<T>(3,2)*z[63] - z[3];
    z[63]=z[2]*z[63];
    z[51]=z[63] + z[51] + z[62];
    z[51]=z[2]*z[51];
    z[62]=n<T>(1,2)*z[2];
    z[63]=z[62] + z[3] + z[53];
    z[63]=z[2]*z[63];
    z[58]=z[63] + 5*z[50] - z[58];
    z[63]= - z[2] - 5*z[53] - 11*z[3];
    z[63]=n<T>(1,4)*z[63] - n<T>(2,3)*z[7];
    z[63]=z[7]*z[63];
    z[58]=n<T>(1,2)*z[58] + z[63];
    z[58]=z[7]*z[58];
    z[62]=z[62] - z[59];
    z[62]=z[2]*z[62];
    z[63]=z[47]*z[3];
    z[50]=z[62] - z[63] - z[50];
    z[47]= - z[56] + z[5] + z[47];
    z[47]=z[6]*z[47];
    z[47]=n<T>(1,2)*z[50] + z[47];
    z[50]=3*z[2] + z[3] - z[59];
    z[50]=n<T>(1,2)*z[50] - z[6];
    z[50]=3*z[50] + z[60];
    z[50]=z[50]*z[60];
    z[47]=3*z[47] + z[50];
    z[47]=z[4]*z[47];
    z[48]=z[16] + z[48] + z[63];
    z[48]=z[9]*z[48];
    z[50]=z[38] - z[36] + z[34] + z[32] - z[30] + z[41] + z[39];
    z[56]= - 12*z[23] - n<T>(27,4)*z[21] - n<T>(1,24)*z[17];
    z[56]=i*z[56];
    z[59]=n<T>(1,4)*z[5] + 2*z[3];
    z[59]= - n<T>(7,8)*z[8] - n<T>(11,4)*z[4] + n<T>(1,4)*z[6] - n<T>(3,2)*z[7] + n<T>(1,3)*
    z[59] + n<T>(7,4)*z[2];
    z[59]=z[16]*z[59];
    z[54]=z[6] + 5*z[7] + 4*z[3] - z[54];
    z[54]=z[15]*z[54];
    z[53]=z[3] - z[53];
    z[53]=z[12]*z[53];

    r +=  - 2*z[19] + n<T>(15,4)*z[22] + 9*z[24] + 6*z[26] - n<T>(49,4)*z[27]
       + n<T>(1,4)*z[33] + 4*z[35] - z[37] - n<T>(1,2)*z[44] + z[46] + z[47] + 
      z[48] + z[49] - n<T>(3,4)*z[50] + z[51] + z[52] + z[53] + z[54] + 
      z[55] + z[56] + n<T>(3,2)*z[57] + z[58] + z[59] + 3*z[61];
 
    return r;
}

template std::complex<double> qg_2lha_tf1236(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1236(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
