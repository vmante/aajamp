#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf287(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[9];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=f[24];
    z[4]=f[62];
    z[5]=f[70];
    z[6]=f[79];
    z[7]=z[4] - z[3];
    z[8]=i*z[1];

    r +=  - 4*z[2] + z[5] - z[6] - n<T>(1,2)*z[7] + n<T>(4,27)*z[8];
 
    return r;
}

template std::complex<double> qg_2lha_tf287(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf287(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
