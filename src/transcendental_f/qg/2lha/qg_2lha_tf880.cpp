#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf880(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[117];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[16];
    z[14]=d[11];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[17];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[21];
    z[37]=c[23];
    z[38]=c[25];
    z[39]=c[26];
    z[40]=c[28];
    z[41]=c[29];
    z[42]=c[30];
    z[43]=c[31];
    z[44]=e[3];
    z[45]=e[6];
    z[46]=e[14];
    z[47]=f[0];
    z[48]=f[1];
    z[49]=f[3];
    z[50]=f[4];
    z[51]=f[5];
    z[52]=f[6];
    z[53]=f[7];
    z[54]=f[10];
    z[55]=f[11];
    z[56]=f[12];
    z[57]=f[13];
    z[58]=f[14];
    z[59]=f[16];
    z[60]=f[17];
    z[61]=f[18];
    z[62]=f[21];
    z[63]=f[23];
    z[64]=f[30];
    z[65]=f[31];
    z[66]=f[32];
    z[67]=f[33];
    z[68]=f[34];
    z[69]=f[35];
    z[70]=f[36];
    z[71]=f[37];
    z[72]=f[39];
    z[73]=f[43];
    z[74]=f[50];
    z[75]=f[51];
    z[76]=f[52];
    z[77]=f[53];
    z[78]=f[54];
    z[79]=f[55];
    z[80]=f[56];
    z[81]=f[58];
    z[82]=f[60];
    z[83]=f[76];
    z[84]=f[84];
    z[85]=f[105];
    z[86]=f[106];
    z[87]=z[8] - z[7];
    z[88]=n<T>(1,4)*z[5];
    z[89]=n<T>(3,4)*z[4];
    z[90]=z[1]*i;
    z[87]=n<T>(5,4)*z[6] - z[89] - z[88] - n<T>(5,3) + z[90] - n<T>(1,4)*z[87];
    z[87]=z[8]*z[87];
    z[91]=2*z[5];
    z[92]=n<T>(61,8)*z[90] + z[91];
    z[92]=z[5]*z[92];
    z[93]=67*z[2] - 13*z[90] - 77*z[5];
    z[93]=z[2]*z[93];
    z[92]=z[92] + n<T>(1,8)*z[93];
    z[93]=z[2] - z[5];
    z[94]= - n<T>(5,16)*z[4] + n<T>(21,8)*z[90] - z[93];
    z[94]=z[4]*z[94];
    z[95]=z[90] - z[5];
    z[96]=z[4] - z[2];
    z[97]=n<T>(23,48)*z[6] + n<T>(5,12)*z[95] - z[96];
    z[97]=z[6]*z[97];
    z[98]=n<T>(1,2)*z[6];
    z[99]= - n<T>(5,2)*z[7] - 5*z[95] - z[98];
    z[100]=n<T>(1,2)*z[7];
    z[99]=z[99]*z[100];
    z[101]= - z[90] + n<T>(1,2)*z[9];
    z[102]= - z[93] + z[101];
    z[102]=z[9]*z[102];
    z[103]=n<T>(5,12)*z[9];
    z[104]=n<T>(137,8)*z[2] - n<T>(17,2)*z[5] + 5 + n<T>(77,8)*z[90];
    z[104]= - n<T>(7,12)*z[6] + n<T>(1,3)*z[104] - n<T>(1,4)*z[4];
    z[104]= - n<T>(55,36)*z[3] - z[103] + n<T>(1,2)*z[104] + z[7];
    z[104]=z[3]*z[104];
    z[87]=z[104] + n<T>(5,6)*z[102] + z[87] + z[99] + z[97] + n<T>(1,3)*z[92] + 
    z[94];
    z[87]=z[3]*z[87];
    z[92]=n<T>(7,2)*z[2];
    z[94]=n<T>(1,2)*z[4];
    z[97]= - 2*z[6] + z[94] - 4*z[90] + z[92];
    z[97]=z[6]*z[97];
    z[99]=n<T>(19,4)*z[8] + z[98] - z[94] + 10*z[90] - n<T>(19,2)*z[5];
    z[99]=z[8]*z[99];
    z[102]=3*z[4];
    z[104]=5*z[6];
    z[105]= - 11*z[11] + 19*z[8] - n<T>(161,6)*z[7] - z[104] + z[102] - n<T>(119,6)*z[95]
     - 3*z[2];
    z[106]=n<T>(1,4)*z[11];
    z[105]=z[105]*z[106];
    z[107]=n<T>(1,2)*z[5];
    z[108]=z[107] - z[90];
    z[109]=z[108]*z[5];
    z[92]= - z[92] + 5*z[90] + z[5];
    z[92]=z[2]*z[92];
    z[110]=n<T>(3,2)*z[90];
    z[111]= - n<T>(53,24)*z[4] - z[110] + z[93];
    z[111]=z[4]*z[111];
    z[112]= - 131*z[90] + 127*z[5];
    z[112]= - n<T>(53,24)*z[7] - z[98] + n<T>(4,3)*z[4] + n<T>(1,12)*z[112] - z[2];
    z[112]=z[7]*z[112];
    z[113]=z[9]*z[101];
    z[92]=z[105] + n<T>(5,2)*z[113] + z[99] + z[112] + z[97] + z[111] - n<T>(13,12)*z[109]
     + z[92];
    z[92]=z[11]*z[92];
    z[97]=n<T>(1,2)*z[2];
    z[99]= - z[97] - z[95];
    z[105]=n<T>(5,3)*z[2];
    z[99]=z[99]*z[105];
    z[111]= - z[90] + z[94];
    z[111]=z[4]*z[111];
    z[98]= - z[98] - z[95];
    z[98]=z[6]*z[98];
    z[112]=z[90] - z[4];
    z[113]=z[100] + z[112];
    z[113]=z[7]*z[113];
    z[114]=n<T>(5,3)*z[9];
    z[105]=z[105] - z[114];
    z[115]= - n<T>(19,8)*z[3] - n<T>(19,4)*z[95] - z[105];
    z[115]=z[3]*z[115];
    z[116]= - n<T>(11,3)*z[90] - 41*z[5];
    z[105]= - n<T>(241,36)*z[10] - n<T>(37,12)*z[11] + n<T>(11,4)*z[3] + n<T>(47,8)*z[7]
    - n<T>(3,16)*z[6] + n<T>(67,24)*z[4] + n<T>(1,16)*z[116] - z[105];
    z[105]=z[10]*z[105];
    z[98]=z[105] + z[115] + n<T>(47,4)*z[113] + n<T>(3,8)*z[98] + n<T>(67,12)*z[111]
    - n<T>(41,8)*z[109] + z[99];
    z[99]= - n<T>(1,4)*z[9] - z[107] + z[97] - z[90];
    z[99]=z[99]*z[114];
    z[105]= - n<T>(37,3)*z[112] + 5*z[11];
    z[105]=z[105]*z[106];
    z[98]=z[105] + z[99] + n<T>(1,2)*z[98];
    z[98]=z[10]*z[98];
    z[99]=n<T>(67,8)*z[4];
    z[105]=n<T>(1,3)*z[6];
    z[106]=3*z[90];
    z[111]= - z[106] + n<T>(19,6)*z[5];
    z[103]=z[103] - n<T>(53,12)*z[8] + n<T>(49,12)*z[7] + z[105] + z[99] + n<T>(5,4)
   *z[111] - n<T>(37,3)*z[2];
    z[103]=z[9]*z[103];
    z[96]=z[105] - n<T>(2,3)*z[90] + z[96];
    z[96]=z[6]*z[96];
    z[105]= - n<T>(65,4)*z[2] + 74*z[90] + n<T>(59,2)*z[5];
    z[105]=z[2]*z[105];
    z[105]=n<T>(95,4)*z[109] + z[105];
    z[111]= - 67*z[90] - 71*z[5];
    z[99]=z[99] + n<T>(1,4)*z[111] + z[2];
    z[99]=z[4]*z[99];
    z[111]= - 49*z[90] - n<T>(23,2)*z[93];
    z[111]=n<T>(17,6)*z[7] + n<T>(1,3)*z[111] - n<T>(7,2)*z[6];
    z[111]=z[111]*z[100];
    z[113]=z[7] - z[6];
    z[114]=n<T>(35,6)*z[2] + z[90] - n<T>(23,6)*z[5];
    z[113]=n<T>(1,2)*z[114] - z[4] - n<T>(13,12)*z[113];
    z[113]=z[8]*z[113];
    z[96]=z[103] + z[113] + z[111] + z[96] + n<T>(1,3)*z[105] + z[99];
    z[96]=z[9]*z[96];
    z[99]=47*z[5];
    z[103]= - n<T>(59,2)*z[2] + 367*z[90] + z[99];
    z[103]=z[103]*z[97];
    z[105]= - n<T>(173,4)*z[4] - n<T>(367,2)*z[2] + n<T>(151,4)*z[90] + 83*z[5];
    z[105]=z[105]*z[94];
    z[103]=z[105] + 83*z[109] + z[103];
    z[103]=z[4]*z[103];
    z[104]= - z[104] + n<T>(61,6)*z[7];
    z[105]=z[90]*z[104];
    z[109]=z[90]*z[5];
    z[111]= - n<T>(121,2)*z[90] - n<T>(71,3)*z[109];
    z[105]= - n<T>(71,6)*z[30] + n<T>(1,2)*z[111] + z[105];
    z[105]=z[15]*z[105];
    z[111]=185*z[90] - 79*z[5];
    z[111]=z[5]*z[111];
    z[113]=n<T>(809,6)*z[2] - 273*z[90] - n<T>(137,2)*z[5];
    z[113]=z[2]*z[113];
    z[111]=z[111] + z[113];
    z[111]=z[2]*z[111];
    z[113]=n<T>(121,2) - n<T>(71,3)*z[95];
    z[104]=n<T>(1,2)*z[113] - z[104];
    z[104]=z[22]*z[104];
    z[113]=n<T>(43,6)*z[5] + n<T>(121,8) - n<T>(35,3)*z[90];
    z[113]=z[113]*npow(z[5],2);
    z[103]=z[71] + z[104] + z[105] + n<T>(1,3)*z[103] + z[113] + n<T>(1,4)*
    z[111];
    z[104]=2*z[90];
    z[88]= - n<T>(7,8)*z[6] - z[94] + z[104] + z[88];
    z[88]=z[6]*z[88];
    z[105]=175*z[90] - n<T>(269,2)*z[5];
    z[105]=z[105]*z[107];
    z[107]=n<T>(55,2)*z[2] - n<T>(73,2)*z[90] - z[5];
    z[107]=z[2]*z[107];
    z[105]=z[105] + z[107];
    z[107]=n<T>(181,48)*z[4] - n<T>(181,24)*z[90] - z[93];
    z[107]=z[4]*z[107];
    z[111]= - n<T>(271,3) - 47*z[90];
    z[111]=n<T>(109,6)*z[4] + n<T>(161,3)*z[2] + n<T>(1,2)*z[111] - n<T>(179,3)*z[5];
    z[111]= - n<T>(481,36)*z[7] + n<T>(1,4)*z[111] + 3*z[6];
    z[100]=z[111]*z[100];
    z[88]=z[100] + z[88] + n<T>(1,6)*z[105] + z[107];
    z[88]=z[7]*z[88];
    z[99]= - n<T>(53,2)*z[2] - 41*z[90] + z[99];
    z[99]=z[99]*z[97];
    z[100]= - 23*z[90] + n<T>(11,2)*z[5];
    z[100]=z[5]*z[100];
    z[99]=z[100] + z[99];
    z[100]= - n<T>(25,3)*z[90] - 3*z[5];
    z[89]= - n<T>(7,6)*z[6] + z[89] + n<T>(1,4)*z[100] + z[2];
    z[89]=z[6]*z[89];
    z[100]= - n<T>(13,2) - z[90];
    z[100]= - n<T>(71,4)*z[2] + n<T>(1,2)*z[100] + 20*z[5];
    z[94]=n<T>(55,8)*z[7] + n<T>(1,3)*z[100] + z[94];
    z[94]=z[7]*z[94];
    z[100]= - n<T>(19,2)*z[2] - n<T>(41,2)*z[90] + z[91];
    z[100]= - n<T>(205,36)*z[8] + n<T>(19,6)*z[7] - n<T>(7,4)*z[6] + n<T>(1,3)*z[100]
     + 
    z[4];
    z[100]=z[8]*z[100];
    z[105]=z[104] - z[2];
    z[107]= - z[4]*z[105];
    z[89]=z[100] + z[94] + z[89] + n<T>(1,6)*z[99] + z[107];
    z[89]=z[8]*z[89];
    z[94]= - z[3]*z[90];
    z[94]= - z[30] - z[109] + z[94];
    z[94]=z[12]*z[94];
    z[99]=z[90]*z[9];
    z[99]=z[99] + z[30];
    z[100]=z[10]*z[90];
    z[100]=z[100] + z[99];
    z[100]=z[17]*z[100];
    z[95]= - z[3] + z[95];
    z[95]=z[20]*z[95];
    z[107]=z[90] - z[9];
    z[109]= - z[10] + z[107];
    z[109]=z[27]*z[109];
    z[94]=z[82] + z[94] + z[100] + z[95] + z[109];
    z[95]=133*z[90] + n<T>(83,2)*z[5];
    z[95]=z[5]*z[95];
    z[100]= - n<T>(7,4)*z[2] + n<T>(11,2)*z[90] - z[5];
    z[100]=z[2]*z[100];
    z[109]= - n<T>(23,16)*z[4] - z[110] - z[5];
    z[109]=z[4]*z[109];
    z[110]= - 445*z[90] + 107*z[5];
    z[110]=n<T>(61,6)*z[6] + n<T>(37,2)*z[4] + n<T>(1,12)*z[110] + 7*z[2];
    z[110]=z[6]*z[110];
    z[95]=n<T>(1,8)*z[110] + z[109] + n<T>(1,48)*z[95] + z[100];
    z[95]=z[6]*z[95];
    z[91]= - z[8] - 3*z[7] + z[6] - z[102] + 2*z[2] + z[106] - z[91];
    z[91]=z[29]*z[91];
    z[100]=n<T>(5,2)*z[6];
    z[102]=z[90]*z[100];
    z[106]=z[90]*z[4];
    z[102]=z[30] + z[106] + z[102];
    z[109]=z[3] - z[8];
    z[104]= - z[104]*z[109];
    z[102]=z[104] + n<T>(7,4)*z[102];
    z[102]=z[13]*z[102];
    z[104]=z[11]*z[90];
    z[104]=z[30] + z[106] + z[104];
    z[104]=z[14]*z[104];
    z[106]= - z[11] + z[112];
    z[106]=z[28]*z[106];
    z[104]=z[49] + z[104] + z[106];
    z[106]=z[8]*z[90];
    z[99]=z[106] + z[99];
    z[99]=z[16]*z[99];
    z[106]=z[8] - z[107];
    z[106]=z[25]*z[106];
    z[99]=z[99] + z[106];
    z[93]=z[93] + z[107];
    z[93]=5*z[93] - n<T>(83,8)*z[3];
    z[93]=n<T>(1,3)*z[93] - n<T>(41,8)*z[10];
    z[93]=z[26]*z[93];
    z[100]= - z[100] + z[112];
    z[100]=n<T>(7,4)*z[100] + 2*z[109];
    z[100]=z[21]*z[100];
    z[106]=z[97] + z[108];
    z[106]=z[19]*z[106];
    z[97]=z[97] + z[101];
    z[97]=z[24]*z[97];
    z[101]=z[3] + z[6];
    z[101]=5 - n<T>(37,8)*z[101];
    z[101]=z[44]*z[101];
    z[101]=z[64] + z[101] - z[70];
    z[107]=z[80] + z[52];
    z[108]=n<T>(709,8)*z[39] + n<T>(565,16)*z[37] - n<T>(19,8)*z[36] + n<T>(5,2)*z[33]
    + n<T>(187,96)*z[31];
    z[108]=i*z[108];
    z[109]= - n<T>(1,6)*z[10] + n<T>(19,4)*z[11] + n<T>(107,12)*z[3] - n<T>(119,2)*z[9]
    - n<T>(151,3)*z[8] - n<T>(587,6)*z[7] + n<T>(337,12)*z[6] - n<T>(43,6)*z[4] - n<T>(511,8)*z[5]
     + 41*z[2];
    z[109]=z[30]*z[109];
    z[90]= - n<T>(181,4)*z[7] + n<T>(47,2)*z[2] + 28*z[90] - 37*z[5];
    z[90]=n<T>(7,2)*z[11] - n<T>(29,6)*z[9] + n<T>(1,3)*z[90] - n<T>(25,4)*z[8];
    z[90]=z[23]*z[90];
    z[105]=z[4] - z[105];
    z[105]=z[18]*z[105];
    z[110]=z[6] + z[8];
    z[110]=z[45]*z[110];
    z[111]= - z[11] - z[10];
    z[111]=z[46]*z[111];

    r +=  - n<T>(553,48)*z[32] + n<T>(25,4)*z[34] - n<T>(263,96)*z[35] - n<T>(221,12)*
      z[38] - n<T>(105,2)*z[40] - n<T>(75,16)*z[41] - n<T>(75,4)*z[42] + n<T>(12079,288)
      *z[43] + n<T>(33,4)*z[47] - n<T>(7,8)*z[48] - n<T>(203,96)*z[50] + n<T>(47,6)*
      z[51] - n<T>(20,3)*z[53] - n<T>(37,24)*z[54] + n<T>(95,24)*z[55] + n<T>(37,96)*
      z[56] + n<T>(47,12)*z[57] + n<T>(17,16)*z[58] - n<T>(3,32)*z[59] + n<T>(79,8)*
      z[60] + n<T>(7,4)*z[61] - n<T>(19,8)*z[62] + n<T>(5,6)*z[63] + 8*z[65] + n<T>(15,4)*z[66]
     + n<T>(94,3)*z[67]
     + n<T>(23,8)*z[68]
     + n<T>(7,2)*z[69] - z[72] - n<T>(53,12)*z[73]
     + n<T>(37,12)*z[74]
     + n<T>(217,48)*z[75] - n<T>(179,24)*z[76]
     + 12
      *z[77] + n<T>(1,6)*z[78] - n<T>(67,48)*z[79] + n<T>(47,16)*z[81] + n<T>(21,4)*
      z[83] + z[84] + n<T>(3,4)*z[85] - n<T>(1,4)*z[86] + z[87] + z[88] + z[89]
       + z[90] + z[91] + z[92] + z[93] + n<T>(5,2)*z[94] + z[95] + z[96] + 
      n<T>(83,3)*z[97] + z[98] + n<T>(25,3)*z[99] + z[100] + n<T>(1,3)*z[101] + 
      z[102] + n<T>(1,2)*z[103] + n<T>(71,12)*z[104] + n<T>(74,3)*z[105] + n<T>(37,4)*
      z[106] - n<T>(5,4)*z[107] + z[108] + n<T>(1,12)*z[109] + n<T>(7,6)*z[110] + 
      n<T>(67,12)*z[111];
 
    return r;
}

template std::complex<double> qg_2lha_tf880(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf880(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
