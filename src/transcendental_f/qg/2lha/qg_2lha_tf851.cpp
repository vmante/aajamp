#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf851(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[94];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[5];
    z[21]=e[7];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=e[10];
    z[25]=e[11];
    z[26]=e[12];
    z[27]=c[3];
    z[28]=c[11];
    z[29]=c[15];
    z[30]=c[21];
    z[31]=c[23];
    z[32]=c[25];
    z[33]=c[26];
    z[34]=c[28];
    z[35]=c[30];
    z[36]=c[31];
    z[37]=e[3];
    z[38]=e[6];
    z[39]=e[14];
    z[40]=f[0];
    z[41]=f[3];
    z[42]=f[4];
    z[43]=f[5];
    z[44]=f[6];
    z[45]=f[7];
    z[46]=f[10];
    z[47]=f[11];
    z[48]=f[12];
    z[49]=f[13];
    z[50]=f[16];
    z[51]=f[17];
    z[52]=f[23];
    z[53]=f[30];
    z[54]=f[31];
    z[55]=f[32];
    z[56]=f[33];
    z[57]=f[34];
    z[58]=f[35];
    z[59]=f[36];
    z[60]=f[39];
    z[61]=f[43];
    z[62]=f[51];
    z[63]=f[52];
    z[64]=f[54];
    z[65]=f[55];
    z[66]=f[56];
    z[67]=f[58];
    z[68]=f[60];
    z[69]=n<T>(1,2)*z[5];
    z[70]=z[1]*i;
    z[71]=z[69] - z[70];
    z[72]=z[71]*z[5];
    z[73]=n<T>(1,2)*z[8];
    z[74]=z[73] - z[70];
    z[75]= - z[8]*z[74];
    z[75]= - z[72] + z[75];
    z[76]=z[70] - z[8];
    z[77]=n<T>(1,2)*z[6];
    z[78]= - z[77] - z[76];
    z[78]=z[6]*z[78];
    z[79]=z[70] - z[5];
    z[80]=n<T>(1,2)*z[9];
    z[81]= - z[80] - z[79];
    z[81]=z[9]*z[81];
    z[75]=z[81] + n<T>(1,3)*z[75] + z[78];
    z[78]= - z[73] - z[71];
    z[78]= - z[80] + n<T>(1,3)*z[78] - z[77];
    z[81]=z[11] + z[7];
    z[82]=n<T>(1,3)*z[3];
    z[78]=n<T>(1,2)*z[78] + z[82] + n<T>(1,6)*z[81];
    z[78]=z[11]*z[78];
    z[83]=n<T>(1,2)*z[3] + z[79];
    z[83]=z[83]*z[82];
    z[84]=n<T>(1,3)*z[7];
    z[85]=z[76]*z[84];
    z[75]=z[78] + z[85] + n<T>(1,2)*z[75] + z[83];
    z[75]=z[11]*z[75];
    z[78]= - z[5] - z[3];
    z[78]=z[70]*z[78];
    z[78]= - z[27] + z[78];
    z[78]=z[12]*z[78];
    z[83]=z[70]*z[8];
    z[83]=z[83] + z[27];
    z[85]= - z[7]*z[70];
    z[85]=z[85] - z[83];
    z[85]=z[15]*z[85];
    z[86]=z[9]*z[70];
    z[83]=z[86] + z[83];
    z[83]=z[16]*z[83];
    z[86]= - z[3] + z[79];
    z[86]=z[19]*z[86];
    z[87]= - z[7] + z[76];
    z[87]=z[23]*z[87];
    z[88]= - z[9] + z[76];
    z[88]=z[25]*z[88];
    z[89]= - z[9] - z[10];
    z[89]=z[39]*z[89];
    z[75]=z[87] + z[88] + z[89] + z[68] + z[85] + z[75] + z[78] + z[83]
    + z[86];
    z[78]=n<T>(1,2)*z[2];
    z[83]= - z[5] + z[78] + z[76];
    z[83]=z[83]*z[78];
    z[85]=z[74] + z[5];
    z[86]=z[85]*z[73];
    z[85]=n<T>(5,4)*z[9] - z[78] + z[85];
    z[85]=z[9]*z[85];
    z[87]=z[80] - z[70];
    z[88]=n<T>(1,4)*z[8];
    z[89]= - n<T>(11,12)*z[3] + z[2] - z[88] - n<T>(1,4)*z[5] - z[87];
    z[89]=z[3]*z[89];
    z[90]=n<T>(1,2)*z[70];
    z[91]= - z[90] + z[5];
    z[91]=z[5]*z[91];
    z[85]=z[89] + z[85] + z[83] + z[91] + z[86];
    z[82]=z[85]*z[82];
    z[85]=z[71]*z[69];
    z[86]=z[69] + z[70];
    z[89]=n<T>(7,6)*z[7] - n<T>(5,2)*z[6] + z[2] + z[73] + z[86];
    z[89]=z[7]*z[89];
    z[91]= - n<T>(1,2)*z[79] + z[8];
    z[91]=z[8]*z[91];
    z[92]=z[2] - z[5];
    z[93]=n<T>(1,2)*z[92] - z[6];
    z[93]=z[6]*z[93];
    z[89]=n<T>(1,2)*z[89] + z[93] + z[83] + z[85] + z[91];
    z[84]=z[89]*z[84];
    z[89]=n<T>(1,3)*z[6];
    z[90]=z[90] + z[5];
    z[90]=z[80] - z[89] + n<T>(1,3)*z[90] - z[78];
    z[90]=z[10]*z[90];
    z[89]= - n<T>(1,6)*z[4] + z[89] - z[92];
    z[89]=z[4]*z[89];
    z[89]=z[89] + z[90];
    z[90]= - z[2] + n<T>(1,3)*z[5];
    z[90]=z[71]*z[90];
    z[91]=z[74]*z[73];
    z[93]= - z[70] - z[5];
    z[93]= - n<T>(1,6)*z[6] + n<T>(1,3)*z[93] + z[78];
    z[93]=z[6]*z[93];
    z[89]=z[93] + z[91] + z[90] + n<T>(1,2)*z[89];
    z[89]=z[10]*z[89];
    z[90]=z[10] + z[4];
    z[91]=z[70]*z[90];
    z[91]=z[27] + z[91];
    z[91]=z[13]*z[91];
    z[90]=z[70] - z[90];
    z[90]=z[26]*z[90];
    z[93]= - z[3] - z[11];
    z[93]=z[37]*z[93];
    z[81]=z[38]*z[81];
    z[81]= - z[91] - z[90] + z[58] + z[53] - z[52] + z[46] - z[93] - 
    z[81] + z[63] - z[61];
    z[86]= - z[88] - z[86];
    z[86]=z[8]*z[86];
    z[83]= - z[83] - n<T>(7,4)*z[72] + z[86];
    z[86]=z[70] + z[77];
    z[90]=n<T>(1,4)*z[6];
    z[86]=z[86]*z[90];
    z[91]= - z[2] + z[8] + z[70] - n<T>(7,4)*z[5];
    z[91]= - n<T>(13,18)*z[9] + n<T>(1,3)*z[91] + z[90];
    z[80]=z[91]*z[80];
    z[80]=z[80] + n<T>(1,3)*z[83] + z[86];
    z[80]=z[9]*z[80];
    z[72]=n<T>(5,3)*z[72];
    z[83]= - z[5] + z[74];
    z[83]=z[8]*z[83];
    z[83]=z[72] + z[83];
    z[86]=n<T>(1,4)*z[2];
    z[91]=z[86] + 2*z[70] - z[69];
    z[91]=z[2]*z[91];
    z[83]=n<T>(1,4)*z[83] + n<T>(1,3)*z[91];
    z[91]=n<T>(1,12)*z[6] - n<T>(1,6)*z[70] + z[92];
    z[77]=z[91]*z[77];
    z[87]= - z[6] + z[87];
    z[91]=n<T>(1,4)*z[9];
    z[87]=z[87]*z[91];
    z[93]=z[70] + n<T>(25,2)*z[5];
    z[93]=n<T>(1,3)*z[93] + n<T>(5,2)*z[8];
    z[93]= - n<T>(1,12)*z[4] + n<T>(1,8)*z[9] + n<T>(1,24)*z[6] + n<T>(1,4)*z[93]
     - n<T>(5,3)
   *z[2];
    z[93]=z[4]*z[93];
    z[77]=z[93] + z[87] + 5*z[83] + z[77];
    z[77]=z[4]*z[77];
    z[76]=z[76] + z[92];
    z[83]=n<T>(5,2)*z[7] + n<T>(7,2)*z[6] - z[76];
    z[83]=z[21]*z[83];
    z[76]= - n<T>(5,2)*z[3] - n<T>(7,2)*z[9] + z[76];
    z[76]=z[24]*z[76];
    z[87]=z[79]*npow(z[5],2);
    z[76]= - z[76] - z[87] - z[83] + z[49] + z[29];
    z[83]=n<T>(1,3)*z[8];
    z[87]=z[83] - 3*z[70] + n<T>(5,6)*z[5];
    z[87]=z[8]*z[87];
    z[72]=z[72] + z[87];
    z[72]=z[72]*z[88];
    z[87]=z[69] - z[74];
    z[83]=z[87]*z[83];
    z[83]= - z[85] + z[83];
    z[85]=n<T>(17,3)*z[2] - 17*z[70] - n<T>(5,3)*z[8];
    z[85]=z[85]*z[86];
    z[83]=5*z[83] + z[85];
    z[83]=z[2]*z[83];
    z[69]= - 5*z[70] + z[69];
    z[69]=z[5]*z[69];
    z[85]= - n<T>(7,4)*z[8] + n<T>(7,2)*z[70] - z[5];
    z[85]=z[8]*z[85];
    z[69]=z[69] + z[85];
    z[85]= - n<T>(5,2)*z[2] + z[73] + z[70] + 2*z[5];
    z[85]=z[2]*z[85];
    z[69]=n<T>(1,2)*z[69] + z[85];
    z[85]=z[70] + 7*z[5];
    z[73]=n<T>(17,9)*z[6] - n<T>(7,3)*z[2] + n<T>(1,3)*z[85] - z[73];
    z[73]=z[73]*z[90];
    z[69]=n<T>(1,3)*z[69] + z[73];
    z[69]=z[6]*z[69];
    z[73]=z[7] + z[3];
    z[85]=n<T>(5,3)*z[6] - n<T>(10,3)*z[8] - z[78];
    z[73]= - n<T>(1,36)*z[10] + n<T>(1,9)*z[11] - n<T>(7,18)*z[4] + n<T>(1,3)*z[85]
     + 
    z[91] - n<T>(1,18)*z[73];
    z[73]=z[27]*z[73];
    z[85]=z[5] + z[6];
    z[85]=z[70]*z[85];
    z[85]=z[27] + z[85];
    z[85]=z[14]*z[85];
    z[79]= - z[6] + z[79];
    z[79]=z[20]*z[79];
    z[79]= - z[64] + z[85] + z[79];
    z[71]=z[78] + z[71];
    z[71]=z[18]*z[71];
    z[74]=z[78] + z[74];
    z[74]=z[22]*z[74];
    z[78]=z[48] + z[62] + z[59];
    z[85]= - z[67] + z[65] + z[60] + z[50];
    z[86]=z[66] + z[57] + z[55] + z[44];
    z[87]=z[54] + z[42];
    z[88]=z[56] + z[45];
    z[90]=14*z[33] + n<T>(19,4)*z[31] - z[30] - n<T>(13,36)*z[28];
    z[90]=i*z[90];
    z[70]=z[10] + n<T>(25,6)*z[4] - z[6] + n<T>(19,6)*z[2] - n<T>(25,3)*z[70] + z[5]
   ;
    z[70]=z[17]*z[70];

    r +=  - n<T>(19,12)*z[32] - n<T>(13,2)*z[34] - 2*z[35] + n<T>(7,3)*z[36] + 4*
      z[40] + n<T>(25,24)*z[41] + n<T>(5,6)*z[43] + n<T>(5,24)*z[47] + n<T>(5,8)*z[51]
       + z[69] + z[70] + 3*z[71] + z[72] + z[73] + n<T>(5,3)*z[74] + n<T>(1,2)*
      z[75] - n<T>(1,3)*z[76] + z[77] + n<T>(1,24)*z[78] + n<T>(2,3)*z[79] + z[80]
       - n<T>(1,6)*z[81] + z[82] + z[83] + z[84] - n<T>(1,8)*z[85] - n<T>(1,4)*
      z[86] - n<T>(11,24)*z[87] - n<T>(4,3)*z[88] + z[89] + z[90];
 
    return r;
}

template std::complex<double> qg_2lha_tf851(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf851(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
