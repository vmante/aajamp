#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf952(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[11];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=c[13];
    z[4]=c[20];
    z[5]=c[23];
    z[6]=f[44];
    z[7]=f[61];
    z[8]=f[79];
    z[9]= - n<T>(167,240)*z[5] + n<T>(167,12)*z[4] + n<T>(167,5)*z[3];
    z[9]=i*z[9];
    z[10]=z[1]*i;
    z[10]= - n<T>(6077,180)*z[10] + 469*z[2];
    z[9]=n<T>(53,6)*z[8] - n<T>(3,8)*z[7] - n<T>(79,12)*z[6] + n<T>(1,36)*z[10] + z[9];

    r += n<T>(1,4)*z[9];
 
    return r;
}

template std::complex<double> qg_2lha_tf952(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf952(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
