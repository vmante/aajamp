#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf501(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[36];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=e[6];
    z[17]=f[30];
    z[18]=f[31];
    z[19]=f[32];
    z[20]=f[33];
    z[21]=f[36];
    z[22]=f[39];
    z[23]=f[43];
    z[24]=z[1]*i;
    z[25]=n<T>(1,2)*z[24];
    z[26]=z[7] + z[4];
    z[27]=n<T>(3,2)*z[3];
    z[28]= - n<T>(1,2)*z[8] + z[25] - z[27] + z[26];
    z[29]=n<T>(1,2)*z[7];
    z[28]=z[28]*z[29];
    z[27]=z[27] - z[4];
    z[30]= - z[27]*z[24];
    z[27]= - n<T>(1,4)*z[8] + z[25] + z[27];
    z[27]=z[8]*z[27];
    z[31]=npow(z[3],2);
    z[27]=z[28] + z[27] - n<T>(3,4)*z[31] + z[30];
    z[27]=z[7]*z[27];
    z[26]=z[16]*z[26];
    z[26]= - z[17] + z[27] + z[26];
    z[27]= - z[4] + n<T>(9,2)*z[3];
    z[25]=z[27]*z[25];
    z[27]=npow(z[4],2);
    z[28]=z[4] - n<T>(13,8)*z[3];
    z[28]=z[3]*z[28];
    z[30]=z[4] - n<T>(3,4)*z[3];
    z[30]=z[8]*z[30];
    z[25]=n<T>(3,2)*z[30] + z[25] + n<T>(1,4)*z[27] + z[28];
    z[25]=z[8]*z[25];
    z[28]=2*z[3];
    z[30]= - z[8] + z[28];
    z[32]=z[3] - z[4];
    z[30]=z[32]*z[30];
    z[33]= - z[4] - z[3];
    z[33]=z[33]*z[24];
    z[34]=z[24] - z[5];
    z[35]=z[4] + z[34];
    z[35]=z[5]*z[35];
    z[30]=z[35] + 2*z[33] + z[27] + z[30];
    z[30]=z[5]*z[30];
    z[33]= - 5*z[4] + n<T>(31,3)*z[3];
    z[33]=z[3]*z[33];
    z[33]= - 9*z[27] + z[33];
    z[33]=z[3]*z[33];
    z[31]=z[27] + n<T>(13,2)*z[31];
    z[31]=z[31]*z[24];
    z[31]=z[33] + z[31];
    z[33]= - n<T>(1,2)*z[2] + z[8] - z[34];
    z[32]=z[32]*z[33];
    z[28]=z[4] - z[28];
    z[28]=z[3]*z[28];
    z[27]=z[27] + z[28] + z[32];
    z[27]=z[2]*z[27];
    z[28]=z[5] + z[3];
    z[28]=z[24]*z[28];
    z[28]=z[13] + z[28];
    z[28]=z[6]*z[28];
    z[32]= - z[3] + z[34];
    z[32]=z[10]*z[32];
    z[28]= - z[19] + z[28] + z[32];
    z[32]=z[8] + z[4];
    z[32]= - z[24]*z[32];
    z[32]= - z[13] + z[32];
    z[32]=z[9]*z[32];
    z[24]=z[24] - z[8];
    z[33]= - z[4] + z[24];
    z[33]=z[12]*z[33];
    z[32]=z[32] + z[33];
    z[33]=z[2] - z[5];
    z[24]=9*z[4] + 13*z[3] - z[24];
    z[24]=n<T>(1,2)*z[24] - 2*z[33];
    z[24]=z[11]*z[24];
    z[33]=z[14]*i;
    z[33]=z[33] - z[21];
    z[34]=npow(z[4],3);
    z[29]=z[29] + n<T>(1,2)*z[5] - n<T>(5,24)*z[8] - z[4] + n<T>(7,4)*z[3];
    z[29]=z[13]*z[29];

    r += n<T>(1,2)*z[15] - n<T>(17,8)*z[18] - 5*z[20] - n<T>(9,8)*z[22] + z[23] + 
      z[24] + z[25] + n<T>(3,2)*z[26] + z[27] + 2*z[28] + z[29] + z[30] + n<T>(1,4)*z[31]
     + n<T>(5,2)*z[32] - n<T>(3,8)*z[33]
     + n<T>(2,3)*z[34];
 
    return r;
}

template std::complex<double> qg_2lha_tf501(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf501(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
