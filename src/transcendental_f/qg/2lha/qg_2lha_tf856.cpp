#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf856(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[96];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[9];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=e[0];
    z[17]=e[4];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[18];
    z[27]=c[19];
    z[28]=c[21];
    z[29]=c[23];
    z[30]=c[25];
    z[31]=c[26];
    z[32]=c[28];
    z[33]=c[29];
    z[34]=c[30];
    z[35]=c[31];
    z[36]=e[3];
    z[37]=e[10];
    z[38]=e[6];
    z[39]=e[14];
    z[40]=f[0];
    z[41]=f[1];
    z[42]=f[3];
    z[43]=f[4];
    z[44]=f[5];
    z[45]=f[10];
    z[46]=f[11];
    z[47]=f[12];
    z[48]=f[13];
    z[49]=f[14];
    z[50]=f[16];
    z[51]=f[17];
    z[52]=f[18];
    z[53]=f[21];
    z[54]=f[30];
    z[55]=f[31];
    z[56]=f[32];
    z[57]=f[33];
    z[58]=f[34];
    z[59]=f[35];
    z[60]=f[36];
    z[61]=f[37];
    z[62]=f[39];
    z[63]=f[43];
    z[64]=f[50];
    z[65]=f[51];
    z[66]=f[52];
    z[67]=f[55];
    z[68]=f[58];
    z[69]=z[1]*i;
    z[70]=z[69] - z[5];
    z[71]=n<T>(1,2)*z[6];
    z[72]=z[70] + z[71];
    z[72]=z[72]*z[6];
    z[73]=n<T>(1,2)*z[4];
    z[74]=z[73] - z[69];
    z[75]=z[74]*z[4];
    z[76]=n<T>(1,2)*z[5];
    z[77]= - z[69]*z[76];
    z[78]=n<T>(1,2)*z[2];
    z[79]=z[69] + z[5];
    z[80]= - z[2] + z[79];
    z[80]=z[80]*z[78];
    z[81]=z[69] + z[2];
    z[81]= - n<T>(1,6)*z[9] + z[73] - n<T>(5,3) - n<T>(1,4)*z[81];
    z[81]=z[3]*z[81];
    z[82]=z[69] - z[4];
    z[83]=n<T>(1,2)*z[7] + n<T>(10,3) + z[82];
    z[83]=z[7]*z[83];
    z[84]= - n<T>(1,4)*z[9] - z[70];
    z[84]=z[9]*z[84];
    z[77]=z[81] + n<T>(1,3)*z[84] + z[80] + z[83] + n<T>(3,2)*z[75] + z[77] + 
    z[72];
    z[77]=z[3]*z[77];
    z[80]=n<T>(1,3)*z[4];
    z[81]=n<T>(1,2)*z[9];
    z[79]=z[81] - z[80] + n<T>(1,6)*z[79] - z[6];
    z[83]=n<T>(1,3)*z[3];
    z[84]=n<T>(1,3)*z[10];
    z[79]=z[84] + n<T>(1,2)*z[79] - z[83];
    z[79]=z[10]*z[79];
    z[85]=z[71] + z[69];
    z[85]=z[85]*z[6];
    z[86]=z[76] - z[69];
    z[86]=z[86]*z[5];
    z[87]=n<T>(1,6)*z[4];
    z[88]= - z[87] + n<T>(1,3)*z[69] + z[6];
    z[88]=z[4]*z[88];
    z[89]=z[81] + z[70];
    z[89]=z[89]*z[81];
    z[90]=n<T>(1,2)*z[3];
    z[91]= - z[90] - z[70];
    z[83]=z[91]*z[83];
    z[79]=z[79] + z[83] + z[89] + z[88] + n<T>(1,6)*z[86] - z[85];
    z[83]=n<T>(1,2)*z[10];
    z[79]=z[79]*z[83];
    z[88]=n<T>(11,3)*z[5];
    z[89]=n<T>(1,3)*z[6];
    z[91]=n<T>(5,2)*z[7] - z[89] - 6*z[69] + z[88];
    z[91]=z[7]*z[91];
    z[92]= - n<T>(13,3)*z[6] + 13*z[69] - z[88];
    z[92]=z[6]*z[92];
    z[93]=z[5] - z[74];
    z[94]=n<T>(7,4)*z[4];
    z[93]=z[93]*z[94];
    z[76]= - z[69] - z[76];
    z[95]=11*z[6];
    z[76]=n<T>(7,4)*z[2] - 11*z[7] + 7*z[76] + z[95];
    z[76]=z[2]*z[76];
    z[94]= - z[94] - n<T>(7,12)*z[5] - 13*z[6];
    z[94]=n<T>(7,6)*z[2] + n<T>(1,2)*z[94] + n<T>(26,3)*z[7];
    z[94]=z[8]*z[94];
    z[76]=z[94] + n<T>(1,3)*z[76] + z[91] + z[93] - n<T>(7,12)*z[86] + z[92];
    z[76]=z[8]*z[76];
    z[91]=z[2] - z[4];
    z[92]=n<T>(13,6)*z[8];
    z[93]=53*z[69] - z[5];
    z[91]=n<T>(5,2)*z[9] - z[92] + n<T>(17,6)*z[7] + n<T>(1,24)*z[93] - 4*z[6] - n<T>(5,4)*z[91];
    z[91]=z[9]*z[91];
    z[92]= - z[92] - n<T>(14,3)*z[7] + n<T>(13,3)*z[69] + 9*z[6];
    z[92]=z[8]*z[92];
    z[93]=z[69]*z[4];
    z[94]=n<T>(14,3)*z[69] - z[7];
    z[94]=z[7]*z[94];
    z[85]=z[91] + z[92] + z[94] - n<T>(5,2)*z[93] - n<T>(1,12)*z[86] - 9*z[85];
    z[85]=z[9]*z[85];
    z[91]= - 8*z[70] - 19*z[6];
    z[91]=z[91]*z[89];
    z[92]=n<T>(19,4)*z[4] - n<T>(19,2)*z[69] - z[5];
    z[80]=z[92]*z[80];
    z[92]=n<T>(19,2)*z[7] + z[95] + 14*z[69] - 11*z[5];
    z[92]=z[7]*z[92];
    z[87]= - n<T>(13,12)*z[2] + n<T>(4,3)*z[7] + z[87] - n<T>(11,6)*z[6] + n<T>(3,2)*
    z[69] + z[5];
    z[87]=z[2]*z[87];
    z[94]=z[5] - n<T>(5,2)*z[69];
    z[94]=z[5]*z[94];
    z[80]=z[87] + n<T>(1,3)*z[92] + z[80] + z[94] + z[91];
    z[80]=z[2]*z[80];
    z[87]=z[93] + z[23];
    z[91]= - z[13]*z[69];
    z[91]=z[91] - z[87];
    z[91]=z[11]*z[91];
    z[81]= - z[90] - 10 - z[81];
    z[81]=z[36]*z[81];
    z[90]=z[13] - z[82];
    z[90]=z[22]*z[90];
    z[92]=z[10] + z[13];
    z[92]=z[39]*z[92];
    z[81]=z[81] + z[92] - z[64] - z[48] - z[26] + z[91] + z[90];
    z[71]=z[69] - z[71];
    z[71]=z[6]*z[71];
    z[90]=z[69] - n<T>(17,2)*z[5];
    z[90]= - 3*z[4] + n<T>(1,3)*z[90] - z[6];
    z[90]=z[90]*z[73];
    z[71]=z[90] - n<T>(17,6)*z[86] + z[71];
    z[71]=z[71]*z[73];
    z[90]=29*z[6];
    z[91]=7*z[5];
    z[92]= - z[90] + n<T>(17,2)*z[69] + z[91];
    z[73]=n<T>(34,9)*z[7] + n<T>(1,3)*z[92] + z[73];
    z[73]=z[7]*z[73];
    z[92]= - 2*z[69] + z[5];
    z[92]=z[92]*z[91];
    z[93]=4*z[69];
    z[94]= - 23*z[6] - 14*z[5] + n<T>(13,2) + z[93];
    z[94]=z[6]*z[94];
    z[92]=z[92] + z[94];
    z[73]=z[73] + n<T>(1,3)*z[92] + z[75];
    z[73]=z[7]*z[73];
    z[72]=z[86] + z[72];
    z[75]=z[83] + z[82];
    z[75]=z[75]*z[84];
    z[83]=npow(z[4],2);
    z[70]=z[6] + z[70];
    z[70]=z[13]*z[70];
    z[70]=n<T>(1,2)*z[70] + z[75] + 3*z[72] + n<T>(1,6)*z[83];
    z[70]=z[13]*z[70];
    z[72]=z[8] + z[7];
    z[75]= - z[69]*z[72];
    z[75]= - z[23] + z[75];
    z[75]=z[15]*z[75];
    z[72]=z[69] - z[72];
    z[72]=z[21]*z[72];
    z[72]=z[75] + z[72];
    z[75]=z[9]*z[69];
    z[75]=z[75] + z[87];
    z[75]=z[12]*z[75];
    z[82]= - z[9] + z[82];
    z[82]=z[17]*z[82];
    z[75]= - z[40] + z[75] + z[82];
    z[82]= - z[88] - n<T>(11,4) + z[93];
    z[82]=z[82]*npow(z[5],2);
    z[83]= - 19*z[69] + n<T>(13,2)*z[5];
    z[83]=z[5]*z[83];
    z[84]= - 13 + 61*z[69];
    z[84]=n<T>(1,2)*z[84] + 35*z[5];
    z[84]=n<T>(1,2)*z[84] + n<T>(116,3)*z[6];
    z[84]=z[6]*z[84];
    z[83]=z[83] + z[84];
    z[83]=z[83]*z[89];
    z[84]= - 22*z[69] + 25*z[5];
    z[84]=z[13] + n<T>(19,3)*z[8] - n<T>(22,3)*z[2] + n<T>(59,3)*z[7] + n<T>(1,3)*z[84]
    + z[90];
    z[84]=z[19]*z[84];
    z[86]=3*z[6] + z[91] + n<T>(11,2);
    z[87]=z[69]*z[86];
    z[87]=7*z[23] + z[87];
    z[87]=z[14]*z[87];
    z[86]=7*z[69] - z[86];
    z[86]=z[18]*z[86];
    z[74]= - z[78] - z[74];
    z[74]=z[16]*z[74];
    z[69]= - n<T>(1,2)*z[8] + z[69] - z[78];
    z[69]=z[20]*z[69];
    z[78]= - z[66] - z[61] + z[58] - z[53];
    z[88]= - z[68] - z[65] + z[49] + z[33];
    z[89]=z[44] - z[30];
    z[90]=z[47] + z[43];
    z[91]=z[3] + z[10];
    z[91]=z[37]*z[91];
    z[91]=z[91] - z[45];
    z[92]= - n<T>(13,2)*z[31] - n<T>(3,4)*z[29] - n<T>(3,2)*z[28] - n<T>(5,2)*z[24];
    z[92]=i*z[92];
    z[93]= - n<T>(25,12)*z[4] + n<T>(29,2)*z[5] + n<T>(143,3)*z[6];
    z[93]=n<T>(1,2)*z[93] - n<T>(20,3)*z[7];
    z[93]= - n<T>(11,9)*z[13] + n<T>(1,18)*z[10] - n<T>(11,12)*z[3] + n<T>(215,72)*z[9]
    - n<T>(61,72)*z[8] + n<T>(1,3)*z[93] - 2*z[2];
    z[93]=z[23]*z[93];
    z[94]=z[7] + z[9];
    z[94]=z[38]*z[94];

    r += n<T>(49,12)*z[25] + n<T>(29,24)*z[27] + n<T>(9,2)*z[32] + z[34] + n<T>(43,8)*
      z[35] + n<T>(5,4)*z[41] - n<T>(17,24)*z[42] - n<T>(7,24)*z[46] + n<T>(1,8)*z[50]
       - n<T>(7,8)*z[51] - n<T>(5,6)*z[52] - n<T>(31,6)*z[54] - n<T>(55,6)*z[55]
     - 5*
      z[56] - n<T>(94,3)*z[57] - n<T>(4,3)*z[59] + n<T>(13,6)*z[60] - 4*z[62] + n<T>(8,3)*z[63]
     + n<T>(1,12)*z[67]
     + n<T>(7,3)*z[69]
     + z[70]
     + z[71]
     + n<T>(34,3)*
      z[72] + z[73] + n<T>(17,3)*z[74] + n<T>(5,2)*z[75] + z[76] + z[77] - n<T>(1,2)
      *z[78] + z[79] + z[80] + n<T>(1,3)*z[81] + z[82] + z[83] + z[84] + 
      z[85] + z[86] + z[87] + n<T>(1,4)*z[88] - n<T>(7,6)*z[89] + n<T>(1,24)*z[90]
       + n<T>(1,6)*z[91] + z[92] + z[93] + n<T>(20,3)*z[94];
 
    return r;
}

template std::complex<double> qg_2lha_tf856(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf856(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
