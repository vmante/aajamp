#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf696(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[45];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[9];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[12];
    z[10]=d[17];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=e[13];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[17];
    z[18]=c[31];
    z[19]=e[6];
    z[20]=e[5];
    z[21]=f[30];
    z[22]=f[31];
    z[23]=f[32];
    z[24]=f[33];
    z[25]=f[35];
    z[26]=f[36];
    z[27]=f[39];
    z[28]=f[43];
    z[29]=f[52];
    z[30]=f[53];
    z[31]=f[68];
    z[32]= - z[5] - 2*z[8];
    z[33]=z[4] - z[3];
    z[32]=z[33]*z[32];
    z[34]=z[1]*i;
    z[35]=2*z[34];
    z[36]=2*z[3];
    z[37]=z[36] + z[4];
    z[37]=z[37]*z[35];
    z[38]=npow(z[3],2);
    z[36]=z[36] - z[4];
    z[36]=z[4]*z[36];
    z[32]=z[37] - 4*z[38] + z[36] + z[32];
    z[32]=z[5]*z[32];
    z[36]=z[34] - z[5];
    z[37]=z[36] - z[8];
    z[39]= - z[33]*z[37];
    z[40]=z[4] + z[3];
    z[41]= - z[4]*z[40];
    z[39]=2*z[38] + z[41] + z[39];
    z[33]= - z[2]*z[33];
    z[33]=2*z[39] + z[33];
    z[33]=z[2]*z[33];
    z[39]=n<T>(13,2)*z[3] - n<T>(7,3)*z[4];
    z[39]=z[4]*z[39];
    z[39]=n<T>(11,2)*z[38] + z[39];
    z[39]=z[4]*z[39];
    z[41]=npow(z[3],3);
    z[37]=z[37] + z[2];
    z[37]= - 26*z[3] - 19*z[4] + 4*z[37];
    z[37]=z[11]*z[37];
    z[32]=z[32] - n<T>(73,6)*z[41] + z[39] + z[33] + z[37];
    z[33]= - n<T>(1,3)*z[4] + n<T>(1,2)*z[3];
    z[37]=z[33]*z[34];
    z[39]=n<T>(1,6)*z[34];
    z[41]=n<T>(1,12)*z[8] - z[39] - z[33];
    z[41]=z[8]*z[41];
    z[33]= - n<T>(1,3)*z[7] + n<T>(1,6)*z[8] - z[39] + z[33];
    z[33]=z[7]*z[33];
    z[33]=n<T>(1,2)*z[33] + z[41] + n<T>(1,4)*z[38] + z[37];
    z[33]=z[7]*z[33];
    z[37]= - n<T>(23,2)*z[3] + 5*z[4];
    z[37]=z[37]*z[34];
    z[39]=n<T>(23,4)*z[3] - 7*z[4];
    z[39]=z[8]*z[39];
    z[37]=z[37] + z[39];
    z[39]=npow(z[4],2);
    z[41]=n<T>(1,2)*z[38] - n<T>(1,3)*z[39];
    z[37]=n<T>(5,2)*z[41] + n<T>(1,3)*z[37];
    z[37]=z[8]*z[37];
    z[35]= - z[5]*z[35];
    z[41]= - z[6] - z[36];
    z[41]=z[6]*z[41];
    z[35]=z[41] + z[38] + z[35];
    z[35]=z[6]*z[35];
    z[41]=z[8] + z[4];
    z[41]=z[34]*z[41];
    z[41]=z[14] + z[41];
    z[41]=z[10]*z[41];
    z[42]=z[4] - z[34] + z[8];
    z[42]=z[12]*z[42];
    z[41]=z[41] + z[42];
    z[42]=z[9]*z[34];
    z[42]=z[42] - z[20];
    z[42]=2*z[42];
    z[43]=z[6] - z[3];
    z[42]=z[43]*z[42];
    z[38]= - n<T>(5,2)*z[38] - n<T>(7,3)*z[39];
    z[34]=z[38]*z[34];
    z[36]= - z[36] - z[40];
    z[36]=z[13]*z[36];
    z[38]=z[25] + z[31] - z[28];
    z[39]=z[27] - z[18];
    z[40]= - z[4] - z[7];
    z[40]=z[19]*z[40];
    z[40]=z[40] + z[21];
    z[43]=z[17] + n<T>(5,12)*z[15];
    z[43]=i*z[43];
    z[44]= - 8*z[3] + n<T>(5,3)*z[4];
    z[44]= - n<T>(2,3)*z[6] - n<T>(5,9)*z[7] + n<T>(1,3)*z[44] + n<T>(1,4)*z[8];
    z[44]=z[14]*z[44];

    r +=  - n<T>(5,2)*z[16] + n<T>(31,12)*z[22] + z[23] + n<T>(22,3)*z[24] - n<T>(5,12)
      *z[26] - z[29] + 4*z[30] + n<T>(1,3)*z[32] + 5*z[33] + n<T>(1,2)*z[34] + 
      z[35] + 2*z[36] + z[37] + n<T>(2,3)*z[38] + n<T>(5,4)*z[39] + n<T>(5,3)*z[40]
       + 3*z[41] + z[42] + z[43] + z[44];
 
    return r;
}

template std::complex<double> qg_2lha_tf696(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf696(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
