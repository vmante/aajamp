#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf378(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[109];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[18];
    z[12]=d[4];
    z[13]=d[15];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[14];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[17];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[21];
    z[37]=c[23];
    z[38]=c[25];
    z[39]=c[26];
    z[40]=c[28];
    z[41]=c[29];
    z[42]=c[31];
    z[43]=e[3];
    z[44]=e[6];
    z[45]=f[0];
    z[46]=f[1];
    z[47]=f[2];
    z[48]=f[3];
    z[49]=f[4];
    z[50]=f[5];
    z[51]=f[6];
    z[52]=f[7];
    z[53]=f[8];
    z[54]=f[9];
    z[55]=f[10];
    z[56]=f[11];
    z[57]=f[12];
    z[58]=f[13];
    z[59]=f[14];
    z[60]=f[16];
    z[61]=f[17];
    z[62]=f[18];
    z[63]=f[20];
    z[64]=f[21];
    z[65]=f[23];
    z[66]=f[30];
    z[67]=f[31];
    z[68]=f[32];
    z[69]=f[33];
    z[70]=f[36];
    z[71]=f[39];
    z[72]=f[43];
    z[73]=f[50];
    z[74]=f[51];
    z[75]=f[52];
    z[76]=f[54];
    z[77]=f[55];
    z[78]=f[56];
    z[79]=f[57];
    z[80]=f[58];
    z[81]=f[66];
    z[82]=f[67];
    z[83]=f[78];
    z[84]=n<T>(1,2)*z[5];
    z[85]=z[1]*i;
    z[86]=z[84] - z[85];
    z[87]=z[86]*z[5];
    z[88]=13*z[5];
    z[89]= - 11*z[85] - z[88];
    z[89]=5*z[89] + n<T>(77,2)*z[2];
    z[89]=z[2]*z[89];
    z[89]= - z[87] + z[89];
    z[90]=z[2] - z[5];
    z[91]= - n<T>(67,24)*z[6] + n<T>(67,12)*z[85] + z[90];
    z[92]=n<T>(1,2)*z[6];
    z[91]=z[91]*z[92];
    z[93]=z[8] - z[2];
    z[94]= - n<T>(97,48)*z[9] + n<T>(65,8)*z[6] + n<T>(193,24)*z[85] + z[93];
    z[94]=z[9]*z[94];
    z[95]=n<T>(55,2)*z[2] - 103*z[85] - z[84];
    z[95]=n<T>(35,3)*z[4] - n<T>(193,12)*z[9] - n<T>(67,12)*z[6] + n<T>(1,3)*z[95]
     - 9*
    z[8];
    z[95]=z[4]*z[95];
    z[96]=9*z[85] + 11*z[5];
    z[96]= - n<T>(9,4)*z[8] + n<T>(1,2)*z[96] - z[2];
    z[96]=z[8]*z[96];
    z[89]=n<T>(1,4)*z[95] + z[94] + z[91] + n<T>(1,12)*z[89] + z[96];
    z[91]=n<T>(1,2)*z[4];
    z[89]=z[89]*z[91];
    z[94]=n<T>(5,2)*z[2];
    z[95]=5*z[85];
    z[96]= - z[94] - z[95] + n<T>(13,2)*z[5];
    z[96]=z[2]*z[96];
    z[88]= - 23*z[85] - z[88];
    z[97]=n<T>(5,2)*z[8];
    z[88]= - z[97] + n<T>(1,2)*z[88] + 5*z[2];
    z[88]=z[8]*z[88];
    z[88]=z[88] - n<T>(173,16)*z[87] + z[96];
    z[96]=45*z[85] - n<T>(173,6)*z[5];
    z[96]=n<T>(263,36)*z[9] - n<T>(81,8)*z[6] + n<T>(1,4)*z[96] + n<T>(23,3)*z[93];
    z[96]=z[9]*z[96];
    z[98]= - n<T>(49,32)*z[6] - n<T>(49,16)*z[85] - z[5];
    z[98]=z[6]*z[98];
    z[88]=n<T>(1,4)*z[96] + n<T>(1,3)*z[88] + z[98];
    z[88]=z[9]*z[88];
    z[96]=n<T>(1,4)*z[5];
    z[98]= - z[85] - z[96];
    z[98]=z[5]*z[98];
    z[99]=n<T>(1,2)*z[2];
    z[100]= - n<T>(83,4)*z[2] - n<T>(109,4)*z[85] + 47*z[5];
    z[100]=z[100]*z[99];
    z[98]=7*z[98] + z[100];
    z[98]=z[2]*z[98];
    z[100]=npow(z[5],2);
    z[101]= - n<T>(99,4) + n<T>(71,3)*z[85];
    z[101]=n<T>(1,2)*z[101] - 9*z[5];
    z[101]=z[101]*z[100];
    z[98]=z[101] + n<T>(1,3)*z[98];
    z[101]= - z[85] - n<T>(3,2)*z[5];
    z[101]=9*z[101] + n<T>(31,4)*z[2];
    z[101]=z[2]*z[101];
    z[100]=n<T>(5,4)*z[100] + z[101];
    z[101]=n<T>(1,2)*z[8];
    z[95]=z[101] + n<T>(1,8)*z[2] - z[95] + z[5];
    z[95]=z[8]*z[95];
    z[95]=n<T>(1,4)*z[100] + z[95];
    z[95]=z[8]*z[95];
    z[100]=n<T>(7,4)*z[8];
    z[102]= - z[100] + n<T>(7,2)*z[85] + z[90];
    z[102]=z[8]*z[102];
    z[103]=37*z[5];
    z[104]= - n<T>(193,2)*z[2] + 65*z[85] + z[103];
    z[104]=z[2]*z[104];
    z[105]= - n<T>(109,3)*z[85] + n<T>(51,2)*z[5];
    z[105]=z[5]*z[105];
    z[102]=n<T>(85,6)*z[102] + z[105] + n<T>(1,6)*z[104];
    z[104]=99 + n<T>(221,3)*z[85];
    z[104]=n<T>(1,4)*z[104] + n<T>(43,3)*z[5];
    z[104]=n<T>(1445,72)*z[6] - n<T>(85,16)*z[8] + n<T>(1,2)*z[104] - n<T>(29,3)*z[2];
    z[104]=z[6]*z[104];
    z[102]=n<T>(1,2)*z[102] + z[104];
    z[102]=z[102]*z[92];
    z[104]=13*z[85];
    z[94]=n<T>(13,2)*z[8] - z[104] + z[94];
    z[94]= - z[10] + z[4] + n<T>(1,4)*z[94] + z[9];
    z[94]=z[24]*z[94];
    z[105]=z[85] - z[8];
    z[106]=z[105] + z[90];
    z[107]=7*z[106] - n<T>(173,8)*z[9];
    z[107]=n<T>(1,3)*z[107] - n<T>(39,8)*z[3];
    z[107]=z[26]*z[107];
    z[88]= - z[56] + z[107] + z[94] + z[89] + z[88] + z[102] + n<T>(1,2)*
    z[98] + z[95];
    z[89]=107*z[85] - z[103];
    z[94]=n<T>(107,12)*z[6];
    z[89]=z[94] + n<T>(1,6)*z[89] - z[2];
    z[89]=z[6]*z[89];
    z[95]=137*z[85] - 179*z[5];
    z[95]=n<T>(1,6)*z[95] - z[2];
    z[94]=n<T>(19,2)*z[10] + 5*z[4] - 9*z[9] + z[94] + n<T>(1,2)*z[95] - z[8];
    z[94]=z[10]*z[94];
    z[89]=z[89] + z[94];
    z[94]=n<T>(37,48)*z[9] + z[6] - z[101] + z[99] + n<T>(25,24)*z[85] + z[5];
    z[95]=n<T>(1,2)*z[9];
    z[94]=z[94]*z[95];
    z[98]=z[101] + z[85];
    z[102]=n<T>(1,4)*z[2];
    z[96]=z[102] + z[96] - z[98];
    z[96]=n<T>(77,96)*z[4] - n<T>(49,48)*z[9] + n<T>(1,2)*z[96] - n<T>(4,3)*z[6];
    z[96]=z[4]*z[96];
    z[103]= - z[104] + n<T>(61,2)*z[5];
    z[103]=z[5]*z[103];
    z[104]= - n<T>(9,2)*z[5] + z[85] + z[2];
    z[104]=z[2]*z[104];
    z[103]=n<T>(1,12)*z[103] + z[104];
    z[104]=z[101] - z[85];
    z[102]=z[102] - z[104];
    z[102]=z[8]*z[102];
    z[89]=z[96] + z[94] + n<T>(1,4)*z[103] + z[102] + n<T>(1,8)*z[89];
    z[89]=z[10]*z[89];
    z[94]=7*z[85];
    z[96]= - z[97] - z[94] - z[90];
    z[96]=z[8]*z[96];
    z[97]=z[85] - z[5];
    z[102]=z[92] + z[97];
    z[103]=5*z[6];
    z[102]=z[102]*z[103];
    z[107]= - n<T>(35,12)*z[85] + 3*z[5];
    z[107]=z[5]*z[107];
    z[108]= - 37*z[2] + 55*z[85] + 23*z[5];
    z[108]=z[2]*z[108];
    z[96]=z[102] + n<T>(1,3)*z[96] + z[107] + n<T>(1,12)*z[108];
    z[93]= - n<T>(79,4)*z[97] + 13*z[93];
    z[93]=n<T>(47,24)*z[9] + n<T>(1,6)*z[93] + z[6];
    z[93]=z[9]*z[93];
    z[91]=z[91] - z[85];
    z[102]=z[4]*z[91];
    z[93]=n<T>(17,8)*z[102] + n<T>(1,2)*z[96] + z[93];
    z[86]= - z[95] - z[92] + z[101] + z[86];
    z[86]=z[10]*z[86];
    z[96]=n<T>(1,2)*z[3];
    z[101]= - n<T>(1,18)*z[3] - n<T>(79,48)*z[9] - n<T>(7,12)*z[8] + n<T>(61,48)*z[2]
     + 
   n<T>(143,48)*z[85] - z[5];
    z[101]=z[101]*z[96];
    z[86]=z[101] + n<T>(1,2)*z[93] + z[86];
    z[86]=z[3]*z[86];
    z[93]=z[8]*z[104];
    z[93]=29*z[87] - 85*z[93];
    z[92]= - z[92] - z[105];
    z[92]=z[6]*z[92];
    z[95]= - z[95] - z[97];
    z[95]=z[9]*z[95];
    z[92]=5*z[95] + n<T>(1,3)*z[93] + 85*z[92];
    z[93]=z[85]*z[4];
    z[92]=n<T>(1,2)*z[92] - 17*z[93];
    z[95]=z[96] + z[97];
    z[95]=z[3]*z[95];
    z[96]= - z[2] + z[12] + z[4];
    z[94]=z[94] + n<T>(29,8)*z[5];
    z[94]= - n<T>(7,12)*z[3] - n<T>(5,8)*z[9] - n<T>(85,8)*z[6] - n<T>(85,24)*z[8]
     + n<T>(1,3)*z[94]
     + n<T>(17,4)*z[96];
    z[94]=z[12]*z[94];
    z[92]=z[94] + n<T>(1,2)*z[92] + n<T>(11,3)*z[95];
    z[92]=z[12]*z[92];
    z[94]= - n<T>(17,3)*z[6] + n<T>(99,2) + n<T>(91,3)*z[5];
    z[95]=z[85]*z[94];
    z[95]=n<T>(91,3)*z[30] + z[95];
    z[95]=z[16]*z[95];
    z[94]=n<T>(91,3)*z[85] - z[94];
    z[94]=z[22]*z[94];
    z[92]=z[82] + z[94] + z[92] + z[95];
    z[94]=z[99] + z[97];
    z[94]=z[2]*z[94];
    z[84]=n<T>(7,6)*z[7] - n<T>(5,2)*z[6] + z[2] + z[84] + z[98];
    z[84]=z[7]*z[84];
    z[84]=z[84] + z[87] + z[94];
    z[87]=z[90] + z[85];
    z[87]= - n<T>(1,2)*z[87] + z[8];
    z[87]=z[8]*z[87];
    z[94]=n<T>(1,2)*z[12];
    z[95]=z[94] + z[105];
    z[94]=z[95]*z[94];
    z[95]=n<T>(1,2)*z[90] - z[6];
    z[95]=z[6]*z[95];
    z[84]=z[94] + z[95] + z[87] + n<T>(1,2)*z[84];
    z[84]=z[7]*z[84];
    z[87]=n<T>(5,2)*z[7] + n<T>(7,2)*z[6] - z[106];
    z[87]=z[23]*z[87];
    z[84]=z[84] + z[87];
    z[87]=z[93] + z[30];
    z[93]=z[12]*z[85];
    z[93]=z[93] + z[87];
    z[93]=z[15]*z[93];
    z[94]=z[85] - z[4];
    z[95]= - z[12] + z[94];
    z[95]=z[21]*z[95];
    z[93]=z[93] + z[95] + z[64] - z[55];
    z[95]= - z[8] - z[7];
    z[95]=z[85]*z[95];
    z[95]= - z[30] + z[95];
    z[95]=z[17]*z[95];
    z[96]= - z[7] + z[105];
    z[96]=z[25]*z[96];
    z[95]=z[95] + z[96];
    z[96]= - z[10]*z[85];
    z[87]=z[96] - z[87];
    z[87]=z[14]*z[87];
    z[96]=z[10] - z[94];
    z[96]=z[28]*z[96];
    z[87]=z[87] + z[96];
    z[96]= - z[5] - z[3];
    z[96]=z[85]*z[96];
    z[96]= - z[30] + z[96];
    z[96]=z[13]*z[96];
    z[97]= - z[3] + z[97];
    z[97]=z[20]*z[97];
    z[96]=z[96] + z[97];
    z[90]=z[3] + z[90] + z[100] + n<T>(11,4)*z[9];
    z[97]=z[85]*z[90];
    z[97]=n<T>(7,4)*z[30] + z[97];
    z[97]=z[11]*z[97];
    z[98]=11*z[2] - n<T>(149,2)*z[85] + 41*z[5];
    z[98]=5*z[10] + n<T>(15,4)*z[4] + n<T>(1,6)*z[98] - z[103];
    z[98]=z[19]*z[98];
    z[85]=n<T>(7,4)*z[85] - z[90];
    z[85]=z[27]*z[85];
    z[90]=z[10] + z[9];
    z[90]= - z[6] - z[94] + n<T>(133,48)*z[90];
    z[90]=z[29]*z[90];
    z[91]= - z[99] - z[91];
    z[91]=z[18]*z[91];
    z[94]=z[12] + z[7];
    z[94]=z[44]*z[94];
    z[94]= - z[66] + z[94] + z[72];
    z[99]=z[59] - z[46];
    z[100]=z[60] - z[41];
    z[101]=n<T>(13,12)*z[39] + n<T>(119,48)*z[37] - n<T>(43,16)*z[36] - z[33] - n<T>(35,18)*z[31];
    z[101]=i*z[101];
    z[102]=n<T>(1117,8)*z[5] - 109*z[2];
    z[102]= - n<T>(85,6)*z[7] + n<T>(275,24)*z[12] - n<T>(101,12)*z[3] + n<T>(73,4)*
    z[10] - n<T>(1171,24)*z[4] + n<T>(359,6)*z[9] + n<T>(575,6)*z[6] + n<T>(1,3)*z[102]
    - n<T>(643,8)*z[8];
    z[102]=z[30]*z[102];
    z[103]=z[3] + z[12];
    z[103]=z[43]*z[103];

    r += n<T>(551,192)*z[32] - n<T>(5,48)*z[34] + n<T>(47,144)*z[35] + n<T>(83,96)*
      z[38] + n<T>(115,48)*z[40] + n<T>(6137,1152)*z[42] - n<T>(13,4)*z[45] - n<T>(11,24)*z[47]
     - n<T>(25,96)*z[48]
     - n<T>(95,64)*z[49]
     - n<T>(63,32)*z[50]
     - n<T>(7,8)
      *z[51] - n<T>(14,3)*z[52] + 2*z[53] - z[54] - n<T>(29,192)*z[57] - n<T>(7,24)
      *z[58] - n<T>(3,16)*z[61] - n<T>(17,48)*z[62] + n<T>(35,48)*z[63] + n<T>(7,12)*
      z[65] - n<T>(935,192)*z[67] - n<T>(85,32)*z[68] - n<T>(85,6)*z[69] + n<T>(85,192)
      *z[70] - n<T>(85,64)*z[71] - n<T>(7,48)*z[73] - n<T>(67,192)*z[74] + n<T>(107,96)
      *z[75] + n<T>(23,24)*z[76] + n<T>(145,192)*z[77] - n<T>(9,8)*z[78] + n<T>(21,8)*
      z[79] - n<T>(49,64)*z[80] - n<T>(3,8)*z[81] + 4*z[83] + n<T>(85,24)*z[84] + 
      z[85] + z[86] + n<T>(53,48)*z[87] + n<T>(1,2)*z[88] + z[89] + z[90] + n<T>(19,3)*z[91]
     + n<T>(1,8)*z[92]
     + n<T>(17,16)*z[93]
     + n<T>(85,48)*z[94]
     + n<T>(85,16)*
      z[95] + n<T>(4,3)*z[96] + z[97] + n<T>(1,4)*z[98] - n<T>(17,32)*z[99] - n<T>(5,64)
      *z[100] + z[101] + n<T>(1,24)*z[102] + n<T>(29,48)*z[103];
 
    return r;
}

template std::complex<double> qg_2lha_tf378(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf378(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
