#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1196(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[33];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[19];
    z[11]=c[23];
    z[12]=c[25];
    z[13]=c[26];
    z[14]=c[28];
    z[15]=c[30];
    z[16]=c[31];
    z[17]=f[0];
    z[18]=f[3];
    z[19]=f[5];
    z[20]=f[11];
    z[21]=f[13];
    z[22]=f[17];
    z[23]=f[20];
    z[24]=i*z[1];
    z[25]= - z[2] + n<T>(3,2)*z[24] + z[4];
    z[26]=n<T>(1,2)*z[5];
    z[25]=z[3] + z[26] + 3*z[25];
    z[25]=z[2]*z[25];
    z[27]=3*z[7];
    z[28]=z[4]*z[24];
    z[29]=z[4] + 2*z[24];
    z[30]=z[5] - z[29];
    z[30]=z[5]*z[30];
    z[29]=z[3] - z[29];
    z[29]=z[3]*z[29];
    z[25]=z[25] + 2*z[29] + z[30] - 3*z[28] + n<T>(1,2)*z[9] - 5*z[6] - 
    z[27] - z[8];
    z[25]=z[2]*z[25];
    z[28]=n<T>(1,2)*z[4];
    z[29]=5*z[24] + z[28];
    z[29]=z[29]*z[28];
    z[26]= - z[26] + z[24] + z[4];
    z[26]=z[5]*z[26];
    z[30]=n<T>(1,12)*z[9];
    z[31]= - z[6] + z[30];
    z[32]= - 5*z[4] - 3*z[5];
    z[32]=z[3]*z[32];
    z[26]=n<T>(1,4)*z[32] + n<T>(3,2)*z[26] + 5*z[31] + z[29];
    z[26]=z[3]*z[26];
    z[24]=z[24] - z[28];
    z[24]=z[24]*z[28];
    z[28]=n<T>(1,4)*z[4];
    z[29]= - z[5]*z[28];
    z[24]=z[29] + z[24] - z[8] + z[30];
    z[24]=z[5]*z[24];
    z[29]= - 5*z[13] - n<T>(3,2)*z[11];
    z[27]=10*z[6] + z[27] + 2*z[8];
    z[27]=z[1]*z[27];
    z[27]=3*z[29] + z[27];
    z[27]=i*z[27];
    z[29]=z[18] + z[21];
    z[30]=z[15] - z[17];
    z[28]=z[9]*z[28];

    r +=  - n<T>(3,8)*z[10] + n<T>(13,4)*z[12] + n<T>(21,2)*z[14] + n<T>(7,2)*z[16]
     - 
      z[19] - n<T>(1,4)*z[20] - n<T>(3,4)*z[22] + z[23] + z[24] + z[25] + z[26]
       + z[27] + z[28] - n<T>(5,4)*z[29] + 3*z[30];
 
    return r;
}

template std::complex<double> qg_2lha_tf1196(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1196(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
