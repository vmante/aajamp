#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1383(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[41];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[4];
    z[8]=d[8];
    z[9]=e[0];
    z[10]=e[1];
    z[11]=e[8];
    z[12]=c[3];
    z[13]=c[19];
    z[14]=c[23];
    z[15]=c[25];
    z[16]=c[26];
    z[17]=c[28];
    z[18]=c[31];
    z[19]=e[3];
    z[20]=e[10];
    z[21]=f[3];
    z[22]=f[4];
    z[23]=f[5];
    z[24]=f[10];
    z[25]=f[11];
    z[26]=f[12];
    z[27]=f[13];
    z[28]=z[1]*i;
    z[29]=2*z[28];
    z[30]=z[29] - z[5];
    z[31]=z[2] - z[4];
    z[32]=z[31]*z[30];
    z[33]=npow(z[4],2);
    z[34]=z[4] - 2*z[2];
    z[34]=z[2]*z[34];
    z[32]=z[33] + z[34] + z[32];
    z[32]=z[5]*z[32];
    z[34]=z[28] - z[4];
    z[35]=z[34] + n<T>(1,2)*z[6];
    z[35]=z[35]*z[6];
    z[36]=z[28]*z[4];
    z[35]=z[35] - z[36] + n<T>(1,2)*z[33];
    z[34]=z[34] + z[6];
    z[36]= - z[7]*z[34];
    z[36]=z[36] - z[35];
    z[36]=z[7]*z[36];
    z[34]=z[8]*z[34];
    z[34]=z[34] + z[35];
    z[34]=z[8]*z[34];
    z[35]=z[8] - z[7];
    z[37]=z[2] + z[4];
    z[38]=n<T>(2,3)*z[3];
    z[35]= - z[38] - n<T>(1,2)*z[37] + n<T>(2,3)*z[5] - n<T>(1,6)*z[35];
    z[35]=z[12]*z[35];
    z[30]=z[2] - z[30];
    z[30]=z[11]*z[30];
    z[39]=z[6] + z[7];
    z[39]=z[19]*z[39];
    z[40]= - z[6] - z[8];
    z[40]=z[20]*z[40];
    z[30]=z[25] + z[24] + z[23] - z[22] + z[30] + z[39] + z[40] - z[26]
    + z[32] + z[36] + z[34] + z[35];
    z[32]= - z[31]*z[29];
    z[34]=npow(z[2],2);
    z[32]=z[32] - z[33] + z[34];
    z[32]=z[2]*z[32];
    z[34]=z[31]*z[28];
    z[35]= - z[2]*z[37];
    z[33]=4*z[34] + 2*z[33] + z[35];
    z[34]=z[38] - z[5];
    z[31]= - z[31]*z[34];
    z[31]=n<T>(1,3)*z[33] + z[31];
    z[31]=z[3]*z[31];
    z[28]= - n<T>(4,3)*z[28] + z[4] + n<T>(2,3)*z[2] + z[34];
    z[28]=z[9]*z[28];
    z[29]= - z[29] + z[37];
    z[29]=z[10]*z[29];
    z[33]=z[18] + z[17];
    z[34]=z[27] + z[21];
    z[35]=2*i;
    z[35]=z[14]*z[35];
    z[36]=z[16]*i;

    r += n<T>(1,6)*z[13] - z[15] + z[28] + z[29] + n<T>(1,3)*z[30] + z[31] + 
      z[32] - 2*z[33] + n<T>(2,3)*z[34] + z[35] + 4*z[36];
 
    return r;
}

template std::complex<double> qg_2lha_tf1383(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1383(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
