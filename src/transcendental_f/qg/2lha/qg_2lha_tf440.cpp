#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf440(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[9];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[8];
    z[4]=d[3];
    z[5]=d[5];
    z[6]= - i*z[1];
    z[6]=z[6] + z[4];
    z[7]=z[3] - z[2];
    z[6]=z[7]*z[6];
    z[7]= - z[3]*z[2];
    z[8]=2*z[2] - z[5];
    z[8]=z[5]*z[8];

    r += z[6] + z[7] + z[8];
 
    return r;
}

template std::complex<double> qg_2lha_tf440(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf440(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d
);
#endif
