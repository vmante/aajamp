#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf706(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[30];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[11];
    z[6]=d[9];
    z[7]=e[12];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[31];
    z[12]=e[13];
    z[13]=e[14];
    z[14]=f[50];
    z[15]=f[51];
    z[16]=f[53];
    z[17]=f[55];
    z[18]=f[58];
    z[19]= - z[4] + n<T>(1,2)*z[2];
    z[20]=n<T>(1,3)*z[2];
    z[21]=z[19]*z[20];
    z[22]=npow(z[4],2);
    z[23]=n<T>(1,2)*z[22];
    z[24]=n<T>(1,2)*z[3];
    z[25]= - 2*z[4] - z[24];
    z[25]=z[3]*z[25];
    z[26]= - z[3] + z[6];
    z[26]=z[6]*z[26];
    z[21]=n<T>(1,2)*z[26] + n<T>(1,3)*z[25] + z[23] + z[21];
    z[21]=z[6]*z[21];
    z[25]=z[6] + z[2];
    z[26]=z[1]*i;
    z[27]= - z[25]*z[26];
    z[27]=z[27] - z[8];
    z[27]=z[5]*z[27];
    z[25]= - z[26] + z[25];
    z[25]=z[7]*z[25];
    z[25]= - z[27] - z[25] + z[14] - z[10];
    z[27]=n<T>(5,3)*z[3];
    z[28]= - z[2] + n<T>(1,3)*z[4];
    z[28]=n<T>(1,2)*z[28] + z[27];
    z[28]=z[3]*z[28];
    z[29]= - z[2]*z[19];
    z[22]=z[28] - n<T>(11,6)*z[22] + z[29];
    z[22]=z[3]*z[22];
    z[28]= - n<T>(1,2)*z[4] + z[2];
    z[28]=z[2]*z[28];
    z[28]= - z[23] + z[28];
    z[20]=z[28]*z[20];
    z[28]=npow(z[4],3);
    z[20]=z[22] + z[28] + z[20];
    z[22]=z[2] - z[4];
    z[28]= - z[2]*z[22];
    z[23]=z[23] + z[28];
    z[22]=z[24] + z[22];
    z[22]=z[3]*z[22];
    z[22]=n<T>(1,3)*z[23] + z[22];
    z[23]=n<T>(1,3)*z[6];
    z[24]=z[4]*z[23];
    z[22]=n<T>(1,2)*z[22] + z[24];
    z[22]=z[22]*z[26];
    z[19]=z[23] + n<T>(7,3)*z[3] - z[19];
    z[19]=z[8]*z[19];
    z[23]=n<T>(5,3)*z[6] + z[4] + z[27];
    z[23]=z[12]*z[23];
    z[24]=z[4] + z[6];
    z[24]=z[13]*z[24];
    z[26]=z[9]*i;

    r += n<T>(7,3)*z[11] - n<T>(1,4)*z[15] - 2*z[16] + n<T>(1,12)*z[17] - n<T>(11,12)*
      z[18] + n<T>(1,6)*z[19] + n<T>(1,2)*z[20] + z[21] + z[22] + z[23] + z[24]
       - n<T>(1,3)*z[25] + n<T>(1,36)*z[26];
 
    return r;
}

template std::complex<double> qg_2lha_tf706(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf706(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
