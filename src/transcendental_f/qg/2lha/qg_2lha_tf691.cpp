#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf691(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[107];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[7];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=e[10];
    z[25]=e[11];
    z[26]=e[12];
    z[27]=c[3];
    z[28]=c[11];
    z[29]=c[15];
    z[30]=c[19];
    z[31]=c[21];
    z[32]=c[23];
    z[33]=c[25];
    z[34]=c[26];
    z[35]=c[28];
    z[36]=c[31];
    z[37]=e[3];
    z[38]=e[6];
    z[39]=e[13];
    z[40]=e[14];
    z[41]=f[0];
    z[42]=f[1];
    z[43]=f[2];
    z[44]=f[3];
    z[45]=f[4];
    z[46]=f[5];
    z[47]=f[6];
    z[48]=f[7];
    z[49]=f[11];
    z[50]=f[12];
    z[51]=f[14];
    z[52]=f[17];
    z[53]=f[18];
    z[54]=f[21];
    z[55]=f[23];
    z[56]=f[30];
    z[57]=f[31];
    z[58]=f[32];
    z[59]=f[33];
    z[60]=f[34];
    z[61]=f[35];
    z[62]=f[36];
    z[63]=f[39];
    z[64]=f[43];
    z[65]=f[50];
    z[66]=f[51];
    z[67]=f[54];
    z[68]=f[55];
    z[69]=f[56];
    z[70]=f[58];
    z[71]=z[1]*i;
    z[72]=z[71] - z[8];
    z[73]=n<T>(1,2)*z[5];
    z[74]=z[72] - z[73];
    z[75]=z[74]*z[73];
    z[76]=z[72] - z[5];
    z[77]=n<T>(1,2)*z[2];
    z[78]=z[76] + z[77];
    z[79]= - z[78]*z[77];
    z[80]=n<T>(1,2)*z[10];
    z[81]= - z[80] - z[72];
    z[81]=z[81]*z[80];
    z[82]=n<T>(1,2)*z[8];
    z[83]=z[73] + z[82];
    z[84]= - n<T>(7,6)*z[7] + n<T>(5,2)*z[6] - z[2] - z[71] - z[83];
    z[84]=z[7]*z[84];
    z[85]=n<T>(1,2)*z[71];
    z[86]=z[85] - z[8];
    z[86]=z[8]*z[86];
    z[87]=z[2] - z[5];
    z[88]= - n<T>(1,2)*z[87] + z[6];
    z[88]=z[6]*z[88];
    z[75]=n<T>(1,2)*z[84] + z[81] + z[88] + z[79] + z[86] + z[75];
    z[75]=z[7]*z[75];
    z[79]=z[5] + z[3];
    z[79]=z[71]*z[79];
    z[79]=z[27] + z[79];
    z[79]=z[12]*z[79];
    z[76]= - n<T>(5,2)*z[7] - n<T>(7,2)*z[6] + z[2] + z[76];
    z[76]=z[21]*z[76];
    z[81]=2*z[71];
    z[84]=z[81] - z[2];
    z[86]=z[5] - z[84];
    z[86]=z[18]*z[86];
    z[88]=z[71] - z[5];
    z[89]=z[3] - z[88];
    z[89]=z[19]*z[89];
    z[90]=z[3] + z[10];
    z[90]=z[37]*z[90];
    z[91]=z[6] + z[11];
    z[91]=z[39]*z[91];
    z[75]= - z[55] + z[89] + z[90] + z[91] - z[65] + z[75] + z[76] + 
    z[79] + z[86];
    z[76]=n<T>(1,2)*z[3];
    z[79]=z[76] - z[73];
    z[86]=n<T>(5,12)*z[6];
    z[89]=n<T>(1,3)*z[4];
    z[90]=n<T>(5,3)*z[71];
    z[91]=n<T>(1,12)*z[9];
    z[92]=z[89] + z[91] - z[86] + n<T>(3,2)*z[2] - z[90] - z[8] + z[79];
    z[92]=z[4]*z[92];
    z[93]=z[85] + z[8];
    z[94]=n<T>(1,4)*z[5];
    z[95]=z[93] - z[94];
    z[95]=z[95]*z[5];
    z[96]=z[82] - z[71];
    z[97]=z[96]*z[8];
    z[98]= - 3*z[71] + z[77] - z[5];
    z[98]=z[98]*z[77];
    z[99]=n<T>(1,2)*z[6];
    z[100]=z[71] - z[99];
    z[86]=z[100]*z[86];
    z[100]=n<T>(1,3)*z[71];
    z[101]=n<T>(1,6)*z[9] - z[100] + z[6];
    z[101]=z[9]*z[101];
    z[85]= - z[3]*z[85];
    z[85]=n<T>(1,2)*z[92] + z[85] + n<T>(1,4)*z[101] + z[86] + z[98] - z[97] + 
    z[95];
    z[85]=z[4]*z[85];
    z[86]=z[71]*z[4];
    z[92]=z[86] + z[27];
    z[98]=z[10]*z[71];
    z[98]=z[98] + z[92];
    z[98]=z[14]*z[98];
    z[101]=z[71]*z[8];
    z[101]=z[101] + z[27];
    z[102]=z[7]*z[71];
    z[102]=z[102] + z[101];
    z[102]=z[15]*z[102];
    z[103]=z[72]*npow(z[8],2);
    z[104]=z[71] - z[4];
    z[105]= - z[10] + z[104];
    z[105]=z[20]*z[105];
    z[106]=z[7] - z[72];
    z[106]=z[23]*z[106];
    z[98]=z[106] + z[54] - z[52] - z[41] - z[103] + z[98] + z[102] + 
    z[105];
    z[102]=2*z[2];
    z[103]= - z[102] + z[81] + z[5] + z[8];
    z[103]=z[2]*z[103];
    z[105]= - z[71] - z[8];
    z[105]=z[5]*z[105];
    z[103]=z[103] - n<T>(5,2)*z[97] + z[105];
    z[91]=z[91] - z[99];
    z[93]= - z[93] + z[73] + z[2];
    z[93]=n<T>(1,3)*z[93] + z[91];
    z[93]=z[9]*z[93];
    z[87]=z[87]*z[99];
    z[105]= - n<T>(11,4)*z[2] + z[81] - n<T>(1,4)*z[8];
    z[91]=n<T>(4,9)*z[3] + n<T>(1,3)*z[105] - z[91];
    z[91]=z[3]*z[91];
    z[87]=z[91] + z[93] + n<T>(1,3)*z[103] + z[87];
    z[87]=z[3]*z[87];
    z[91]=n<T>(1,3)*z[3];
    z[93]=n<T>(1,3)*z[5];
    z[103]= - z[71] + n<T>(1,3)*z[8];
    z[103]= - z[10] + z[4] - z[91] + z[99] - z[2] + n<T>(1,2)*z[103] + z[93]
   ;
    z[103]=z[103]*z[80];
    z[105]=z[96]*z[82];
    z[73]=z[73] - z[71];
    z[73]=z[5]*z[73];
    z[73]=z[105] + z[73];
    z[105]=z[99] + z[72];
    z[105]=z[105]*z[99];
    z[76]=z[76] + z[88];
    z[76]=z[76]*z[91];
    z[73]=z[103] - z[86] + z[76] + n<T>(1,3)*z[73] + z[105];
    z[73]=z[73]*z[80];
    z[76]= - z[11]*z[71];
    z[76]=z[76] - z[92];
    z[76]=z[13]*z[76];
    z[80]=z[11] - z[104];
    z[80]=z[26]*z[80];
    z[86]= - z[10] - z[7];
    z[86]=z[38]*z[86];
    z[91]= - z[9] - z[11];
    z[91]=z[40]*z[91];
    z[76]= - z[53] - z[49] - z[43] - z[29] - z[67] - z[64] + z[61] + 
    z[56] + z[76] + z[80] + z[86] + z[91];
    z[74]= - z[5]*z[74];
    z[74]=n<T>(7,2)*z[97] + z[74];
    z[77]= - z[77] + z[81] - z[83];
    z[77]=z[2]*z[77];
    z[74]=n<T>(1,2)*z[74] + z[77];
    z[77]= - n<T>(11,9)*z[6] + n<T>(1,3)*z[2] - z[93] + z[100] + z[82];
    z[80]=n<T>(1,4)*z[6];
    z[77]=z[77]*z[80];
    z[74]=n<T>(1,3)*z[74] + z[77];
    z[74]=z[6]*z[74];
    z[77]=z[81] + z[82];
    z[77]=z[8]*z[77];
    z[78]=z[2]*z[78];
    z[77]=z[78] + z[77] + z[95];
    z[78]= - z[80] - n<T>(3,2)*z[71] + z[5];
    z[78]=z[78]*z[99];
    z[80]=z[2] - z[94] + n<T>(1,8)*z[71] - z[8];
    z[80]=n<T>(5,36)*z[9] + n<T>(1,3)*z[80] - n<T>(1,8)*z[6];
    z[80]=z[9]*z[80];
    z[77]=z[80] + n<T>(1,3)*z[77] + z[78];
    z[77]=z[9]*z[77];
    z[78]=n<T>(1,3)*z[9];
    z[80]=z[78] + z[90] - z[5];
    z[80]=z[9]*z[80];
    z[83]= - z[71] + z[6];
    z[83]=z[6]*z[83];
    z[80]=z[80] - z[97] + n<T>(1,3)*z[83];
    z[79]=n<T>(1,2)*z[9] - z[99] + z[96] - z[79];
    z[79]=z[3]*z[79];
    z[83]=n<T>(1,4)*z[4] + z[99] - z[9];
    z[83]=z[83]*z[89];
    z[86]=z[9] + z[72];
    z[86]=z[11]*z[86];
    z[79]=n<T>(1,4)*z[86] + z[83] + n<T>(1,2)*z[80] + z[79];
    z[79]=z[11]*z[79];
    z[80]=2*z[8];
    z[81]= - z[81] + z[8];
    z[81]=z[81]*z[80];
    z[83]=n<T>(5,4)*z[5] - n<T>(5,2)*z[71] - z[80];
    z[83]=z[5]*z[83];
    z[81]=z[81] + z[83];
    z[83]=n<T>(7,4)*z[5] + n<T>(13,4)*z[71] + z[8];
    z[83]=n<T>(1,3)*z[83] - n<T>(3,4)*z[2];
    z[83]=z[2]*z[83];
    z[81]=n<T>(1,3)*z[81] + z[83];
    z[81]=z[2]*z[81];
    z[80]= - z[102] + z[80] + z[88];
    z[78]= - z[11] - z[78] + n<T>(1,3)*z[80] + z[6];
    z[78]=z[24]*z[78];
    z[80]= - z[5]*z[82];
    z[80]= - z[97] + z[80];
    z[80]=z[80]*z[93];
    z[71]= - z[9]*z[71];
    z[71]=z[71] - z[101];
    z[71]=z[16]*z[71];
    z[83]= - z[58] + z[51] - z[45] + z[44] - z[42] + z[69] + z[60];
    z[86]=z[35] + z[59] - z[48];
    z[88]=z[47] + z[33];
    z[89]=z[68] + z[62];
    z[90]=z[70] - z[63];
    z[91]= - n<T>(5,3)*z[34] - n<T>(7,12)*z[32] - z[31] + n<T>(1,24)*z[28];
    z[91]=i*z[91];
    z[82]=z[82] + z[2];
    z[82]= - 11*z[82] + z[6];
    z[82]=n<T>(1,3)*z[82] + z[9];
    z[82]= - n<T>(23,8)*z[4] + n<T>(1,4)*z[82] + n<T>(4,3)*z[3];
    z[82]=n<T>(7,18)*z[11] + n<T>(1,18)*z[7] + n<T>(1,3)*z[82] - n<T>(1,8)*z[10];
    z[82]=z[27]*z[82];
    z[92]= - z[4] + z[84];
    z[92]=z[17]*z[92];
    z[84]= - z[8] + z[84];
    z[84]=z[22]*z[84];
    z[72]=z[9] - z[72];
    z[72]=z[25]*z[72];

    r +=  - n<T>(1,36)*z[30] - n<T>(7,3)*z[36] - n<T>(7,6)*z[46] - n<T>(1,12)*z[50]
     +  n<T>(11,24)*z[57] - n<T>(5,24)*z[66] + z[71] + z[72] + z[73] + z[74] + n<T>(1,3)*z[75]
     + n<T>(1,6)*z[76]
     + z[77]
     + z[78]
     + z[79]
     + z[80]
     + z[81]
     +  z[82] - n<T>(1,4)*z[83] + n<T>(2,3)*z[84] + z[85] + n<T>(4,3)*z[86] + z[87]
       + n<T>(3,4)*z[88] - n<T>(1,24)*z[89] - n<T>(1,8)*z[90] + z[91] + z[92] + n<T>(1,2)*z[98];
 
    return r;
}

template std::complex<double> qg_2lha_tf691(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf691(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
