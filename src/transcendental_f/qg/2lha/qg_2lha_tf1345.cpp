#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1345(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[40];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[19];
    z[12]=c[23];
    z[13]=c[25];
    z[14]=c[26];
    z[15]=c[28];
    z[16]=c[31];
    z[17]=f[0];
    z[18]=f[3];
    z[19]=f[8];
    z[20]=f[11];
    z[21]=f[13];
    z[22]=f[16];
    z[23]=f[17];
    z[24]=f[18];
    z[25]=f[26];
    z[26]=f[27];
    z[27]= - z[2] + 2*z[4];
    z[28]=2*z[2];
    z[27]=z[27]*z[28];
    z[28]=npow(z[4],2);
    z[27]=z[27] + n<T>(7,4)*z[28];
    z[29]= - 11*z[2] + n<T>(7,2)*z[4];
    z[30]=n<T>(5,2)*z[3];
    z[31]=z[30] + n<T>(1,3)*z[29];
    z[31]=z[31]*z[5];
    z[32]=n<T>(1,2)*z[3];
    z[33]= - z[4] + z[32];
    z[30]=z[33]*z[30];
    z[30]=n<T>(1,2)*z[31] + n<T>(1,3)*z[27] + z[30];
    z[30]=z[5]*z[30];
    z[33]=n<T>(1,3)*z[3];
    z[34]= - z[29]*z[33];
    z[35]=7*z[4] - n<T>(29,2)*z[2];
    z[35]=z[2]*z[35];
    z[31]= - z[31] + z[34] + n<T>(1,4)*z[28] + n<T>(1,3)*z[35];
    z[34]=z[1]*i;
    z[31]=z[31]*z[34];
    z[29]=z[29]*z[32];
    z[27]=z[29] + z[27];
    z[27]=z[27]*z[33];
    z[29]= - n<T>(4,3)*z[4] + n<T>(5,2)*z[2];
    z[29]=z[2]*z[29];
    z[28]= - n<T>(7,6)*z[28] + z[29];
    z[28]=z[2]*z[28];
    z[29]=z[4] + z[2];
    z[29]=n<T>(1,2)*z[29] - z[34];
    z[29]=z[7]*z[29];
    z[32]= - z[24] + z[26] + z[25];
    z[33]=z[19] + z[17];
    z[35]=z[20] + z[18];
    z[36]=n<T>(23,3)*z[14] + n<T>(29,6)*z[12] + n<T>(1,6)*z[10];
    z[36]=i*z[36];
    z[37]=npow(z[4],3);
    z[34]= - z[4] - n<T>(8,3)*z[34] + n<T>(4,3)*z[2];
    z[38]=z[5] + n<T>(4,3)*z[3] + z[34];
    z[38]=z[6]*z[38];
    z[34]=n<T>(4,3)*z[5] + z[3] + z[34];
    z[34]=z[8]*z[34];
    z[39]=z[5] + z[3];
    z[39]=n<T>(43,16)*z[4] - 4*z[2] - n<T>(7,4)*z[39];
    z[39]=z[9]*z[39];

    r += n<T>(11,18)*z[11] - n<T>(5,2)*z[13] - n<T>(23,6)*z[15] + n<T>(5,9)*z[16]
     + n<T>(19,6)*z[21] - z[22]
     + n<T>(3,2)*z[23]
     + z[27]
     + z[28]
     + n<T>(7,3)*z[29]
     +  z[30] + z[31] + n<T>(1,4)*z[32] - n<T>(3,4)*z[33] + z[34] + n<T>(7,12)*z[35]
       + z[36] - n<T>(1,12)*z[37] + z[38] + n<T>(1,9)*z[39];
 
    return r;
}

template std::complex<double> qg_2lha_tf1345(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1345(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
