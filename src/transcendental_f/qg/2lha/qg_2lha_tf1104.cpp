#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1104(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[16];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[5];
    z[3]=d[8];
    z[4]=d[12];
    z[5]=d[18];
    z[6]=c[3];
    z[7]=d[3];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=e[5];
    z[11]=e[11];
    z[12]=npow(z[7],2);
    z[12]=z[12] + z[6];
    z[13]=z[2] - z[3];
    z[14]=n<T>(1,2)*z[9] + z[13];
    z[14]=z[9]*z[14];
    z[15]= - z[9] + z[7] - z[13];
    z[15]=z[8]*z[15];
    z[13]= - z[5] + z[4] - z[13];
    z[13]=z[1]*i*z[13];

    r +=  - z[10] + z[11] - n<T>(1,2)*z[12] + z[13] + z[14] + z[15];
 
    return r;
}

template std::complex<double> qg_2lha_tf1104(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1104(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
