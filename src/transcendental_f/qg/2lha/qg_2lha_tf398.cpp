#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf398(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[114];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[19];
    z[34]=c[21];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[31];
    z[40]=e[3];
    z[41]=e[6];
    z[42]=e[14];
    z[43]=f[0];
    z[44]=f[1];
    z[45]=f[2];
    z[46]=f[3];
    z[47]=f[4];
    z[48]=f[5];
    z[49]=f[6];
    z[50]=f[7];
    z[51]=f[8];
    z[52]=f[10];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[13];
    z[56]=f[14];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[18];
    z[60]=f[21];
    z[61]=f[23];
    z[62]=f[30];
    z[63]=f[31];
    z[64]=f[32];
    z[65]=f[33];
    z[66]=f[34];
    z[67]=f[36];
    z[68]=f[37];
    z[69]=f[39];
    z[70]=f[41];
    z[71]=f[43];
    z[72]=f[50];
    z[73]=f[51];
    z[74]=f[52];
    z[75]=f[53];
    z[76]=f[54];
    z[77]=f[55];
    z[78]=f[56];
    z[79]=f[58];
    z[80]=f[60];
    z[81]=f[83];
    z[82]=n<T>(4,3)*z[4];
    z[83]=n<T>(1,3)*z[9];
    z[84]=n<T>(1,3)*z[2];
    z[85]=z[1]*i;
    z[86]=n<T>(26,9)*z[10] - z[82] - n<T>(47,6)*z[3] + n<T>(11,2)*z[6] - z[83] + 
    z[84] - n<T>(7,2)*z[7] - z[85] + n<T>(7,3)*z[5];
    z[86]=z[10]*z[86];
    z[87]=z[2] - z[5];
    z[88]=2*z[85];
    z[89]=n<T>(1,2)*z[9];
    z[90]=z[89] + z[88] - z[87];
    z[90]=z[90]*z[83];
    z[91]=n<T>(1,2)*z[7];
    z[92]= - z[85] - z[91];
    z[92]=z[7]*z[92];
    z[93]=z[88] - z[5];
    z[94]=z[93]*z[5];
    z[92]= - n<T>(1,3)*z[94] + z[92];
    z[95]=z[85] - z[5];
    z[96]=n<T>(1,2)*z[2];
    z[97]=z[96] + z[95];
    z[97]=z[97]*z[84];
    z[98]=n<T>(1,2)*z[6];
    z[99]=z[98] + z[95];
    z[99]=z[6]*z[99];
    z[100]=n<T>(1,3)*z[3];
    z[101]= - n<T>(19,2)*z[3] - z[9] - 19*z[95] + z[2];
    z[101]=z[101]*z[100];
    z[82]= - z[82] + n<T>(8,3)*z[85] + 7*z[7];
    z[82]=z[4]*z[82];
    z[82]=z[86] + z[82] + z[101] + 11*z[99] + z[90] + 7*z[92] + z[97];
    z[82]=z[10]*z[82];
    z[86]= - z[85] + n<T>(1,2)*z[5];
    z[90]=n<T>(79,2)*z[5];
    z[92]=z[86]*z[90];
    z[90]=n<T>(95,4)*z[7] + 43*z[85] - z[90];
    z[90]=z[7]*z[90];
    z[90]=z[92] + z[90];
    z[92]=n<T>(1,2)*z[4];
    z[97]= - z[92] - z[98] - n<T>(1,2)*z[85] + z[2];
    z[97]=z[8]*z[97];
    z[99]=z[89] - z[85];
    z[101]= - z[9]*z[99];
    z[102]=z[7] - z[2];
    z[102]=z[102]*z[98];
    z[103]=n<T>(1,3)*z[7];
    z[104]=n<T>(9,2)*z[4] - z[103] - z[85] + z[6];
    z[104]=z[104]*z[92];
    z[105]=n<T>(3,2)*z[85];
    z[106]=n<T>(3,4)*z[2] - z[105] - z[7];
    z[106]=z[2]*z[106];
    z[107]=z[85] - z[4];
    z[108]=n<T>(1,2)*z[10] + z[107];
    z[108]=z[10]*z[108];
    z[109]=77*z[7] + 59*z[85] - 65*z[5];
    z[109]=z[4] + n<T>(1,3)*z[109] + z[2];
    z[109]= - n<T>(3,2)*z[11] + n<T>(1,2)*z[109] - z[10];
    z[109]=z[11]*z[109];
    z[90]=n<T>(1,2)*z[109] + n<T>(13,3)*z[108] + z[97] + z[104] + z[102] + 
    z[101] + n<T>(1,3)*z[90] + z[106];
    z[90]=z[11]*z[90];
    z[97]=7*z[5];
    z[101]=z[88] - z[97];
    z[101]=z[5]*z[101];
    z[102]=5*z[5];
    z[104]=4*z[85] + z[102];
    z[104]=2*z[104] - n<T>(19,2)*z[2];
    z[104]=z[104]*z[84];
    z[106]=z[87] + z[85];
    z[108]= - z[89] + z[106];
    z[83]=z[108]*z[83];
    z[108]=n<T>(191,6)*z[3] - n<T>(19,2)*z[6] + z[89] - n<T>(13,2)*z[2] - n<T>(11,2)*
    z[5] - 5 - n<T>(7,2)*z[85];
    z[100]=z[108]*z[100];
    z[108]=11*z[95] + n<T>(9,2)*z[7];
    z[108]=z[7]*z[108];
    z[109]= - n<T>(47,6)*z[6] - n<T>(19,3)*z[95] + z[7];
    z[109]=z[6]*z[109];
    z[83]=z[100] + z[109] + z[83] + z[104] + n<T>(2,3)*z[101] + z[108];
    z[83]=z[3]*z[83];
    z[100]=z[96] - z[85];
    z[101]=z[100]*z[96];
    z[104]=n<T>(1,6)*z[9];
    z[108]= - z[104] + n<T>(1,3)*z[85] + z[7];
    z[108]=z[9]*z[108];
    z[97]=n<T>(37,4)*z[85] + z[97];
    z[97]=10*z[6] - n<T>(65,12)*z[9] - z[2] + n<T>(1,3)*z[97] - n<T>(67,4)*z[7];
    z[97]=z[6]*z[97];
    z[109]= - n<T>(65,4)*z[7] - 31*z[85] - z[5];
    z[109]=z[7]*z[109];
    z[94]=z[97] + n<T>(65,2)*z[108] + z[101] - n<T>(7,3)*z[94] + z[109];
    z[94]=z[6]*z[94];
    z[97]=z[4] - z[7];
    z[101]=n<T>(19,2)*z[9];
    z[97]=3*z[3] - z[98] - z[101] + n<T>(95,6)*z[2] - n<T>(29,2)*z[85] - n<T>(19,3)*
    z[5] + n<T>(35,6)*z[97];
    z[97]=z[97]*z[92];
    z[98]=n<T>(5,2)*z[2];
    z[108]=19*z[85];
    z[102]=z[98] - z[108] - z[102];
    z[98]=z[102]*z[98];
    z[102]=35*z[85] - n<T>(41,2)*z[7];
    z[102]=z[102]*z[91];
    z[108]=z[108] - 8*z[5];
    z[108]=z[5]*z[108];
    z[98]=z[98] + z[108] + z[102];
    z[99]=z[5] - z[99];
    z[99]=z[99]*z[101];
    z[101]=z[6] - z[88] - z[91];
    z[101]=z[6]*z[101];
    z[102]=z[85]*z[3];
    z[97]=z[97] - 3*z[102] + z[101] + n<T>(1,3)*z[98] + z[99];
    z[97]=z[4]*z[97];
    z[98]=n<T>(131,2)*z[5];
    z[86]=z[86]*z[98];
    z[98]= - n<T>(251,2)*z[7] - z[98] + n<T>(13,2) + 5*z[85];
    z[98]=z[7]*z[98];
    z[99]=131*z[85];
    z[101]=n<T>(491,2)*z[9] + 5*z[7] - z[99] - 119*z[87];
    z[89]=z[101]*z[89];
    z[101]=n<T>(55,2)*z[2] + n<T>(113,2)*z[7] + 64*z[85] - n<T>(119,2)*z[5];
    z[101]=z[2]*z[101];
    z[86]=z[89] + z[101] + z[86] + z[98];
    z[89]=z[7] + z[6];
    z[89]=z[89]*z[92];
    z[98]=n<T>(34,3)*z[6] - n<T>(65,3)*z[9] + n<T>(121,6)*z[85] + z[5];
    z[98]=z[6]*z[98];
    z[101]= - z[6] + z[7] + n<T>(10,3) + z[95];
    z[101]=z[3]*z[101];
    z[108]= - 625*z[7] + 235*z[85] + 131*z[5];
    z[108]=n<T>(125,4)*z[9] + n<T>(1,4)*z[108] + 58*z[2];
    z[108]=n<T>(857,36)*z[8] - n<T>(1,4)*z[4] + n<T>(1,3)*z[108] + n<T>(1,2)*z[3];
    z[108]=z[8]*z[108];
    z[86]=z[108] + z[89] + z[101] + n<T>(1,3)*z[86] + z[98];
    z[86]=z[8]*z[86];
    z[89]=n<T>(1,3)*z[5];
    z[98]=10*z[85] - n<T>(13,2)*z[5];
    z[89]=z[98]*z[89];
    z[98]= - z[104] + n<T>(77,12)*z[2] - n<T>(71,2)*z[7] + z[105] - n<T>(5,3)*z[5];
    z[98]=z[9]*z[98];
    z[101]= - n<T>(50,3)*z[7] + 71*z[85] - n<T>(119,6)*z[5];
    z[101]=z[7]*z[101];
    z[104]=n<T>(31,2)*z[2] + 119*z[7] - 77*z[85] - 31*z[5];
    z[104]=z[2]*z[104];
    z[89]=z[98] + n<T>(1,6)*z[104] + z[89] + z[101];
    z[89]=z[9]*z[89];
    z[98]= - 211*z[7] - 53*z[85] + 83*z[5];
    z[91]=z[98]*z[91];
    z[98]= - n<T>(91,2)*z[2] - n<T>(149,2)*z[7] + n<T>(143,2)*z[85] + 37*z[5];
    z[96]=z[98]*z[96];
    z[98]= - 38*z[85] + n<T>(29,2)*z[5];
    z[98]=z[5]*z[98];
    z[91]=z[96] + z[98] + z[91];
    z[84]=z[91]*z[84];
    z[91]=z[85]*z[4];
    z[91]=z[91] + z[30];
    z[96]= - z[11]*z[85];
    z[96]=z[96] - z[91];
    z[96]=z[13]*z[96];
    z[98]=z[11] - z[107];
    z[98]=z[28]*z[98];
    z[96]=z[96] + z[98] + z[75] - z[43];
    z[98]= - z[9] - z[8];
    z[98]=z[85]*z[98];
    z[98]= - z[30] + z[98];
    z[98]=z[16]*z[98];
    z[101]=z[85] - z[9];
    z[104]= - z[8] + z[101];
    z[104]=z[25]*z[104];
    z[98]=z[98] + z[104];
    z[104]=z[6]*z[85];
    z[91]=z[104] + z[91];
    z[91]=z[14]*z[91];
    z[104]= - z[6] + z[107];
    z[104]=z[21]*z[104];
    z[91]=z[91] + z[104];
    z[104]=14*z[10] + 13*z[3] + z[9] - z[106];
    z[104]=z[26]*z[104];
    z[105]=z[3] + z[6];
    z[105]= - 5 + 14*z[105];
    z[105]=z[40]*z[105];
    z[104]=z[76] + z[104] + z[105];
    z[105]=z[5]*z[85];
    z[102]=z[30] + z[105] + z[102];
    z[102]=z[12]*z[102];
    z[95]=z[3] - z[95];
    z[95]=z[20]*z[95];
    z[95]= - z[58] + z[102] + z[95];
    z[99]= - z[99] + n<T>(103,2)*z[5];
    z[99]=z[5]*z[99];
    z[102]=n<T>(2173,12)*z[7] + n<T>(187,2)*z[5] + 38 + n<T>(289,4)*z[85];
    z[102]=z[7]*z[102];
    z[99]=z[99] + z[102];
    z[99]=z[99]*z[103];
    z[87]= - z[7] + z[87] + z[107];
    z[87]=z[29]*z[87];
    z[102]=n<T>(46,3)*z[7] + 33 + n<T>(118,3)*z[5];
    z[103]=z[85]*z[102];
    z[103]=n<T>(118,3)*z[30] + z[103];
    z[103]=z[15]*z[103];
    z[102]=n<T>(118,3)*z[85] - z[102];
    z[102]=z[22]*z[102];
    z[105]= - z[9] - z[10];
    z[105]=z[85]*z[105];
    z[105]= - z[30] + z[105];
    z[105]=z[17]*z[105];
    z[92]= - z[92] - z[100];
    z[92]=z[18]*z[92];
    z[100]= - n<T>(247,6)*z[5] - 33 + n<T>(269,6)*z[85];
    z[100]=z[100]*npow(z[5],2);
    z[100]=z[49] + z[78] + z[100] + z[81] + z[70] - z[68];
    z[106]= - z[73] + z[50] + z[47] - z[46];
    z[107]=z[56] + z[51];
    z[108]=z[66] - z[60];
    z[109]=z[74] - z[48];
    z[93]=z[2] - z[93];
    z[93]=z[19]*z[93];
    z[93]=z[93] - z[53];
    z[110]= - n<T>(805,48)*z[11] - n<T>(25,6)*z[10] - n<T>(1111,48)*z[8] - n<T>(479,24)*
    z[4] - n<T>(61,6)*z[3] + n<T>(311,24)*z[6] - n<T>(22,3)*z[9] - n<T>(443,24)*z[2]
     + 
   n<T>(2281,48)*z[5] + 134*z[7];
    z[110]=z[30]*z[110];
    z[110]=z[110] - z[61];
    z[111]= - n<T>(79,3)*z[37] - n<T>(95,12)*z[35] - 8*z[34] - n<T>(1025,72)*z[31];
    z[111]=i*z[111];
    z[85]= - n<T>(259,2)*z[85] + 128*z[5];
    z[85]=n<T>(623,6)*z[8] - 2*z[3] + n<T>(3,2)*z[6] + n<T>(235,6)*z[9] - n<T>(116,3)*
    z[2] + n<T>(1,3)*z[85] + n<T>(293,2)*z[7];
    z[85]=z[23]*z[85];
    z[88]= - z[9] + z[88] - z[2];
    z[88]=z[24]*z[88];
    z[101]=z[10] - z[101];
    z[101]=z[27]*z[101];
    z[112]=z[6] + z[8];
    z[112]=z[41]*z[112];
    z[113]=z[10] + z[11];
    z[113]=z[42]*z[113];

    r += n<T>(181,6)*z[32] + n<T>(95,72)*z[33] + n<T>(91,12)*z[36] + n<T>(103,6)*z[38]
       + n<T>(139,3)*z[39] + z[44] + n<T>(19,3)*z[45] - n<T>(20,3)*z[52] - n<T>(7,3)*
      z[54] - n<T>(11,3)*z[55] + n<T>(11,2)*z[57] - n<T>(5,6)*z[59] - n<T>(133,6)*z[62]
       - n<T>(655,12)*z[63] - n<T>(61,2)*z[64] - n<T>(500,3)*z[65] + n<T>(65,12)*z[67]
       - n<T>(67,4)*z[69] + n<T>(58,3)*z[71] - n<T>(13,3)*z[72] + n<T>(4,3)*z[77]
     - n<T>(7,2)*z[79]
     - z[80]
     + z[82]
     + z[83]
     + z[84]
     + z[85]
     + z[86]
     + z[87]
       + n<T>(23,3)*z[88] + z[89] + z[90] + n<T>(5,2)*z[91] + n<T>(67,3)*z[92] + n<T>(5,3)*z[93]
     + z[94]
     + 5*z[95]
     + 4*z[96]
     + z[97]
     + 60*z[98]
     + z[99]
       + n<T>(1,2)*z[100] + z[101] + z[102] + z[103] + n<T>(2,3)*z[104] + 
      z[105] + n<T>(8,3)*z[106] - n<T>(3,2)*z[107] - 3*z[108] + n<T>(37,6)*z[109]
       + n<T>(1,3)*z[110] + z[111] + n<T>(65,3)*z[112] + n<T>(16,3)*z[113];
 
    return r;
}

template std::complex<double> qg_2lha_tf398(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf398(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
