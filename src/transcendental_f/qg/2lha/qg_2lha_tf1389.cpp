#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1389(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[46];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[16];
    z[9]=e[0];
    z[10]=e[1];
    z[11]=e[4];
    z[12]=e[8];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[17];
    z[17]=c[19];
    z[18]=c[23];
    z[19]=c[25];
    z[20]=c[26];
    z[21]=c[28];
    z[22]=c[30];
    z[23]=c[31];
    z[24]=f[0];
    z[25]=f[1];
    z[26]=f[3];
    z[27]=f[5];
    z[28]=f[11];
    z[29]=f[17];
    z[30]=f[18];
    z[31]=f[76];
    z[32]=z[6] + z[3];
    z[33]=n<T>(1,2)*z[6];
    z[34]=z[32]*z[33];
    z[35]=n<T>(1,2)*z[4];
    z[36]=z[6] + n<T>(5,2)*z[4];
    z[36]=z[36]*z[35];
    z[37]=z[33] - z[7];
    z[38]=n<T>(1,2)*z[5];
    z[39]=z[38] + n<T>(1,2)*z[3] - z[37];
    z[40]=i*z[1];
    z[39]= - n<T>(5,4)*z[2] + n<T>(1,2)*z[39] + 3*z[40];
    z[39]=z[2]*z[39];
    z[40]=n<T>(1,2)*z[11];
    z[41]=z[12] + z[40] + z[10];
    z[42]=npow(z[7],2);
    z[37]=z[5] + z[37];
    z[37]=z[5]*z[37];
    z[36]=z[39] + z[36] + z[37] - z[34] + z[42] - n<T>(5,2)*z[9] - z[41];
    z[37]=z[4]*z[1];
    z[39]= - z[6] + z[8] - z[3];
    z[39]= - n<T>(3,4)*z[5] - z[7] + n<T>(1,4)*z[39];
    z[39]=z[1]*z[39];
    z[39]=z[39] - n<T>(3,2)*z[37];
    z[39]=i*z[39];
    z[36]=z[39] + n<T>(1,2)*z[36];
    z[36]=z[2]*z[36];
    z[39]=3*z[7];
    z[43]=n<T>(3,2)*z[5];
    z[44]= - z[43] + z[39] - z[6];
    z[44]=z[44]*z[38];
    z[32]= - z[39] - z[32];
    z[32]=n<T>(1,2)*z[32] - z[5];
    z[32]=z[32]*z[35];
    z[39]=3*z[9];
    z[32]=z[32] + z[44] + z[34] - n<T>(3,4)*z[42] - z[39] - n<T>(11,12)*z[13] - 
    z[11];
    z[32]=z[32]*z[35];
    z[34]=n<T>(1,12)*z[13];
    z[44]=n<T>(1,4)*z[5];
    z[45]= - z[7]*z[44];
    z[40]=z[45] - n<T>(1,4)*z[42] - n<T>(1,2)*z[9] + z[40] - z[10] + z[34];
    z[38]=z[40]*z[38];
    z[40]= - z[8] + z[7];
    z[40]=z[40]*z[44];
    z[42]=n<T>(1,4)*z[3];
    z[44]=n<T>(3,4)*z[6] + z[42];
    z[44]=z[8]*z[44];
    z[39]=z[40] + z[39] + z[44] + z[41];
    z[39]=z[1]*z[39];
    z[40]=z[43] - z[6] + z[8] + n<T>(3,2)*z[7];
    z[40]=z[1]*z[40];
    z[37]=z[40] - z[37];
    z[35]=z[37]*z[35];
    z[35]=z[35] + z[39] - n<T>(1,24)*z[14] - n<T>(1,4)*z[16] - 5*z[20] - n<T>(3,2)*
    z[18];
    z[35]=i*z[35];
    z[37]=z[8]*z[13];
    z[34]= - z[12] + z[34];
    z[34]=z[7]*z[34];
    z[34]=z[37] + z[34] + z[22] - z[27];
    z[37]= - z[11] + z[9];
    z[37]=z[37]*z[42];
    z[39]= - z[9] + n<T>(1,6)*z[13] - 3*z[11];
    z[40]=npow(z[6],2);
    z[39]=n<T>(1,2)*z[39] - n<T>(1,3)*z[40];
    z[33]=z[39]*z[33];
    z[39]=z[26] + z[29];
    z[40]=z[17] - z[30];

    r +=  - n<T>(5,8)*z[15] + n<T>(2,3)*z[19] + n<T>(5,2)*z[21] - n<T>(7,16)*z[23]
     - n<T>(3,2)*z[24]
     + n<T>(1,4)*z[25] - n<T>(1,8)*z[28]
     + z[31]
     + z[32]
     + z[33]
     + n<T>(1,2)*z[34]
     + z[35]
     + z[36]
     + z[37]
     + z[38] - n<T>(3,8)*z[39]
     + n<T>(1,6)*
      z[40];
 
    return r;
}

template std::complex<double> qg_2lha_tf1389(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1389(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
