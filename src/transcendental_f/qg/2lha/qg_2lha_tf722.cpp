#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf722(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[21];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[7];
    z[5]=e[7];
    z[6]=c[3];
    z[7]=c[31];
    z[8]=d[6];
    z[9]=f[31];
    z[10]=f[33];
    z[11]=f[35];
    z[12]=f[36];
    z[13]=f[39];
    z[14]=n<T>(1,2)*z[4];
    z[15]=n<T>(1,2)*z[2];
    z[16]=z[1]*i;
    z[17]=z[3] + z[15] + z[14] - z[8] - n<T>(1,2)*z[16];
    z[17]=z[3]*z[17];
    z[14]=z[14] - z[16];
    z[14]=z[14]*z[4];
    z[16]=z[16] - z[4];
    z[18]= - z[15] + z[16];
    z[18]=z[2]*z[18];
    z[19]=2*z[5];
    z[20]=npow(z[8],2);
    z[17]=z[17] + z[18] - z[14] + z[19] + n<T>(2,3)*z[6] - z[20];
    z[17]=z[3]*z[17];
    z[15]= - z[16]*z[15];
    z[14]=z[15] + z[14] - n<T>(1,2)*z[6] + z[5];
    z[14]=z[2]*z[14];
    z[15]= - z[9] - z[13] - z[12];
    z[16]= - z[5]*z[16];
    z[18]=npow(z[8],3);
    z[19]=z[8]*z[19];

    r += 7*z[7] - 4*z[10] - z[11] + z[14] + n<T>(1,2)*z[15] + z[16] + z[17]
       + z[18] + z[19];
 
    return r;
}

template std::complex<double> qg_2lha_tf722(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf722(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
