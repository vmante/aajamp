#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1232(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[52];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=d[4];
    z[11]=d[8];
    z[12]=c[11];
    z[13]=c[15];
    z[14]=c[18];
    z[15]=c[19];
    z[16]=c[23];
    z[17]=c[25];
    z[18]=c[26];
    z[19]=c[28];
    z[20]=c[29];
    z[21]=c[30];
    z[22]=c[31];
    z[23]=f[0];
    z[24]=f[1];
    z[25]=f[2];
    z[26]=f[3];
    z[27]=f[4];
    z[28]=f[6];
    z[29]=f[8];
    z[30]=f[9];
    z[31]=f[11];
    z[32]=f[12];
    z[33]=f[14];
    z[34]=f[16];
    z[35]=f[17];
    z[36]=f[18];
    z[37]=f[23];
    z[38]=f[26];
    z[39]=f[27];
    z[40]= - 23*z[4] + 41*z[2];
    z[41]=6*z[3];
    z[40]= - z[41] + n<T>(1,3)*z[40];
    z[40]=z[40]*z[5];
    z[42]=npow(z[2],2);
    z[43]= - 29*z[2] - n<T>(7,2)*z[4];
    z[43]=z[4]*z[43];
    z[43]=n<T>(29,2)*z[42] + z[43];
    z[44]=2*z[4] - z[3];
    z[41]=z[44]*z[41];
    z[41]=z[40] + n<T>(1,3)*z[43] + z[41];
    z[41]=z[5]*z[41];
    z[43]= - n<T>(20,3)*z[2] - z[4];
    z[43]=z[4]*z[43];
    z[44]=z[4] + 5*z[2];
    z[45]=z[3] - z[44];
    z[45]=z[3]*z[45];
    z[40]= - z[40] + z[45] + n<T>(35,3)*z[42] + z[43];
    z[43]=2*z[1];
    z[43]=z[43]*i;
    z[40]=z[40]*z[43];
    z[45]= - z[2] - n<T>(11,2)*z[4];
    z[45]=z[4]*z[45];
    z[44]= - n<T>(2,3)*z[3] + z[44];
    z[44]=z[3]*z[44];
    z[44]=z[44] + n<T>(1,2)*z[42] + z[45];
    z[44]=z[3]*z[44];
    z[45]=10*z[2] + z[4];
    z[45]=z[4]*z[45];
    z[42]=8*z[42] + z[45];
    z[42]=z[4]*z[42];
    z[45]=npow(z[2],3);
    z[42]= - 17*z[45] + z[42];
    z[45]=n<T>(1,4)*z[9];
    z[46]=npow(z[10],2);
    z[46]= - z[45] + z[46];
    z[46]=z[10]*z[46];
    z[47]=npow(z[11],2);
    z[45]=z[45] - z[47];
    z[45]=z[11]*z[45];
    z[47]= - z[23] + z[32] - z[29] + z[27] + z[26];
    z[48]= - n<T>(176,3)*z[18] - n<T>(70,3)*z[16] - n<T>(2,3)*z[12];
    z[48]=i*z[48];
    z[49]= - z[2] - 5*z[4];
    z[49]=n<T>(7,3)*z[49] + 11*z[3];
    z[49]=n<T>(1,2)*z[49] + n<T>(23,3)*z[5];
    z[49]=z[9]*z[49];
    z[43]=z[43] - z[2];
    z[50]= - z[3] + z[43];
    z[50]=z[6]*z[50];
    z[51]= - z[4] + z[43];
    z[51]=z[7]*z[51];
    z[43]= - z[5] + z[43];
    z[43]=z[8]*z[43];

    r +=  - n<T>(26,9)*z[13] - n<T>(104,9)*z[14] + n<T>(7,6)*z[15] + n<T>(128,9)*z[17]
       + n<T>(140,3)*z[19] + n<T>(26,3)*z[20] + n<T>(104,3)*z[21] - 73*z[22] - 
      z[24] - 13*z[25] + z[28] - z[30] - n<T>(23,3)*z[31] + z[33] + 4*z[34]
       - 8*z[35] + z[36] - 2*z[37] + z[38] - z[39] + z[40] + z[41] + n<T>(2,3)*z[42]
     + n<T>(53,3)*z[43]
     + z[44]
     + z[45]
     + z[46]
     + 3*z[47]
     + z[48]
       + n<T>(1,3)*z[49] + 7*z[50] + n<T>(14,3)*z[51];
 
    return r;
}

template std::complex<double> qg_2lha_tf1232(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1232(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
