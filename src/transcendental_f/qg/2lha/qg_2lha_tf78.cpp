#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf78(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[22];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[9];
    z[6]=e[13];
    z[7]=c[3];
    z[8]=e[14];
    z[9]=f[50];
    z[10]=f[51];
    z[11]=f[55];
    z[12]=f[58];
    z[13]=n<T>(1,3)*z[5];
    z[14]=z[1]*i;
    z[15]= - z[14]*z[13];
    z[16]=2*z[14];
    z[17]=z[16] - z[2] - z[5];
    z[17]=z[17]*z[2];
    z[18]= - z[8] - n<T>(1,3)*z[7];
    z[19]=z[14] - z[2];
    z[20]=z[5] + z[19];
    z[20]=z[3]*z[20];
    z[21]= - 2*z[5] - z[19];
    z[21]= - n<T>(2,3)*z[4] + n<T>(1,3)*z[21] + z[3];
    z[21]=z[4]*z[21];
    z[15]=z[21] + z[20] - n<T>(1,3)*z[17] + z[15] + n<T>(4,3)*z[18] - z[6];
    z[15]=z[4]*z[15];
    z[18]=2*z[6];
    z[14]=z[5]*z[14];
    z[16]= - z[3] + 2*z[2] - z[16] - z[5];
    z[16]=z[3]*z[16];
    z[14]=z[16] + z[17] + z[14] + n<T>(5,6)*z[7] - z[18];
    z[14]=z[3]*z[14];
    z[14]= - z[14] - z[9] + z[10] + z[11];
    z[16]= - z[18] - 4*z[8] - n<T>(1,2)*z[7];
    z[13]=z[16]*z[13];
    z[16]= - z[6]*z[19];

    r += z[12] + z[13] - n<T>(1,3)*z[14] + z[15] + z[16];
 
    return r;
}

template std::complex<double> qg_2lha_tf78(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf78(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
