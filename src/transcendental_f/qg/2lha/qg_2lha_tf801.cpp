#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf801(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[109];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[11];
    z[13]=d[15];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[18];
    z[33]=c[19];
    z[34]=c[21];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[29];
    z[40]=c[30];
    z[41]=c[31];
    z[42]=e[3];
    z[43]=e[6];
    z[44]=e[14];
    z[45]=f[0];
    z[46]=f[1];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[10];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[13];
    z[56]=f[14];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[18];
    z[60]=f[23];
    z[61]=f[30];
    z[62]=f[31];
    z[63]=f[32];
    z[64]=f[33];
    z[65]=f[35];
    z[66]=f[36];
    z[67]=f[37];
    z[68]=f[39];
    z[69]=f[43];
    z[70]=f[50];
    z[71]=f[51];
    z[72]=f[52];
    z[73]=f[54];
    z[74]=f[55];
    z[75]=f[56];
    z[76]=f[58];
    z[77]=f[60];
    z[78]=n<T>(1,2)*z[5];
    z[79]=z[1]*i;
    z[80]=z[78] - z[79];
    z[80]=z[80]*z[5];
    z[78]=z[78] + z[79];
    z[81]=z[2] - z[78];
    z[81]=z[2]*z[81];
    z[82]=n<T>(1,2)*z[7];
    z[83]= - 247*z[79] + 245*z[5];
    z[83]= - n<T>(235,12)*z[7] + n<T>(1,6)*z[83] + z[2];
    z[83]=z[83]*z[82];
    z[84]=n<T>(1,2)*z[9];
    z[85]=z[84] - z[79];
    z[86]= - z[9]*z[85];
    z[87]=n<T>(1,2)*z[6];
    z[88]=z[4] - z[2];
    z[89]= - z[7] + z[88];
    z[89]=z[89]*z[87];
    z[90]=n<T>(1,2)*z[8];
    z[91]=z[6] + z[5];
    z[92]= - z[4] - z[7] + z[91];
    z[92]=z[92]*z[90];
    z[93]=z[2] - z[5];
    z[94]= - n<T>(59,24)*z[4] - n<T>(1,2)*z[93] - n<T>(1,3)*z[7];
    z[94]=z[4]*z[94];
    z[95]=z[79] - z[4];
    z[96]= - n<T>(1,2)*z[10] - z[95];
    z[96]=z[10]*z[96];
    z[97]= - 193*z[79] + 181*z[5];
    z[97]= - z[10] - n<T>(181,12)*z[7] + n<T>(1,12)*z[97] + z[2];
    z[97]=z[11]*z[97];
    z[81]=n<T>(1,2)*z[97] + n<T>(21,4)*z[96] + z[92] + z[89] + z[94] + z[86] + 
    z[83] - n<T>(251,12)*z[80] + z[81];
    z[81]=z[11]*z[81];
    z[83]=n<T>(343,2)*z[2];
    z[86]=397*z[5];
    z[89]= - z[83] - 451*z[79] + z[86];
    z[89]=z[2]*z[89];
    z[89]= - 511*z[80] + z[89];
    z[86]= - z[86] + 397*z[2];
    z[92]=569*z[79] + z[86];
    z[92]=n<T>(1,4)*z[92] + 4*z[7];
    z[92]=n<T>(1,3)*z[92] - 76*z[9];
    z[92]=z[9]*z[92];
    z[94]= - n<T>(3,2)*z[4] + 4*z[79] - z[82];
    z[94]=z[4]*z[94];
    z[96]=n<T>(1,2)*z[4];
    z[97]= - n<T>(553,24)*z[6] - z[96] + n<T>(499,12)*z[9] - 15 - n<T>(493,12)*z[79]
   ;
    z[97]=z[6]*z[97];
    z[98]= - n<T>(397,6)*z[2] + n<T>(505,6)*z[5] - n<T>(39,2) - n<T>(59,3)*z[79];
    z[98]=n<T>(1,2)*z[98] + 66*z[7];
    z[98]=z[7]*z[98];
    z[99]= - n<T>(2479,72)*z[8] + n<T>(17,4)*z[6] - 2*z[4] - n<T>(515,24)*z[9] + 
   n<T>(2099,24)*z[7] - n<T>(185,6)*z[2] - 29*z[79] - n<T>(511,24)*z[5];
    z[99]=z[8]*z[99];
    z[89]=z[99] + z[97] + z[94] + z[92] + n<T>(1,12)*z[89] + z[98];
    z[89]=z[8]*z[89];
    z[92]=z[79] - z[5];
    z[94]=n<T>(1,2)*z[2];
    z[97]=z[94] + z[92];
    z[97]=z[2]*z[97];
    z[97]=n<T>(5,4)*z[80] + z[97];
    z[98]=2*z[79];
    z[99]=z[84] + z[98] - z[93];
    z[100]=n<T>(1,3)*z[9];
    z[99]=z[99]*z[100];
    z[101]=z[79] + z[82];
    z[101]=z[7]*z[101];
    z[87]= - z[87] - z[92];
    z[87]=z[6]*z[87];
    z[102]= - n<T>(61,2)*z[79] + 5*z[5];
    z[102]=n<T>(1,8)*z[102] + z[2];
    z[102]= - n<T>(137,72)*z[10] - n<T>(7,8)*z[6] + n<T>(17,16)*z[4] - z[100] + n<T>(1,3)
   *z[102] + n<T>(59,16)*z[7];
    z[102]=z[10]*z[102];
    z[103]=n<T>(17,2)*z[4] - 17*z[79] - 59*z[7];
    z[103]=z[4]*z[103];
    z[87]=z[102] + n<T>(7,4)*z[87] + n<T>(1,8)*z[103] + z[99] + n<T>(1,3)*z[97] + n<T>(59,8)*z[101];
    z[87]=z[10]*z[87];
    z[97]=2*z[5];
    z[99]=n<T>(29,2)*z[79] - z[97];
    z[99]=z[5]*z[99];
    z[101]=25*z[92] - z[2];
    z[101]=z[101]*z[94];
    z[102]=8*z[92] + n<T>(5,4)*z[6];
    z[102]=z[6]*z[102];
    z[103]=2*z[10] - z[9] + n<T>(13,2)*z[92] + z[2];
    z[103]=z[10]*z[103];
    z[99]=z[103] + z[102] + z[99] + z[101];
    z[101]=z[79] - z[96];
    z[101]=z[4]*z[101];
    z[90]= - z[90] - z[95];
    z[90]=z[8]*z[90];
    z[90]=z[101] + z[90];
    z[101]= - n<T>(3,4)*z[7] + z[94] - n<T>(3,2)*z[79] + z[5];
    z[101]=z[7]*z[101];
    z[102]=z[93] - z[85];
    z[102]=z[102]*z[100];
    z[103]=n<T>(19,2)*z[79] + z[5];
    z[103]=z[84] + n<T>(1,2)*z[103] - 2*z[2];
    z[103]=n<T>(11,18)*z[3] + n<T>(13,12)*z[10] + n<T>(4,3)*z[6] + n<T>(1,3)*z[103]
     - n<T>(9,4)*z[4];
    z[103]=z[3]*z[103];
    z[90]=z[103] + z[102] + 9*z[101] + n<T>(9,2)*z[90] + n<T>(1,3)*z[99];
    z[90]=z[3]*z[90];
    z[99]=z[94] - z[79];
    z[101]=z[2]*z[99];
    z[101]=n<T>(11,6)*z[80] + z[101];
    z[102]= - z[96] + 15*z[79] + z[7];
    z[102]=z[102]*z[96];
    z[103]= - n<T>(731,2)*z[79] + 11*z[5];
    z[88]= - n<T>(545,6)*z[6] + n<T>(709,12)*z[9] + n<T>(533,4)*z[7] + n<T>(1,6)*z[103]
    - 15*z[88];
    z[88]=z[6]*z[88];
    z[103]=3 + n<T>(113,8)*z[79];
    z[103]=5*z[103] + n<T>(569,16)*z[7];
    z[103]=z[7]*z[103];
    z[104]=n<T>(709,6)*z[9] - n<T>(709,3)*z[79] - 569*z[7];
    z[104]=z[9]*z[104];
    z[88]=n<T>(1,4)*z[88] + z[102] + n<T>(1,8)*z[104] + n<T>(1,2)*z[101] + z[103];
    z[88]=z[6]*z[88];
    z[101]=n<T>(17,2)*z[2] - n<T>(41,2)*z[79] - 17*z[5];
    z[101]=z[2]*z[101];
    z[101]= - n<T>(7,4)*z[80] + z[101];
    z[102]=n<T>(185,24)*z[7] - n<T>(185,12)*z[79] - z[93];
    z[82]=z[102]*z[82];
    z[102]=z[5] - z[85];
    z[102]=z[9]*z[102];
    z[103]= - 37*z[79] - 19*z[5];
    z[103]=n<T>(185,4)*z[7] + n<T>(1,2)*z[103] + 47*z[2];
    z[103]=n<T>(37,6)*z[4] + n<T>(1,3)*z[103] - n<T>(25,2)*z[9];
    z[103]=z[4]*z[103];
    z[82]=n<T>(1,4)*z[103] + n<T>(25,4)*z[102] + n<T>(1,3)*z[101] + z[82];
    z[82]=z[4]*z[82];
    z[97]= - n<T>(37,2)*z[2] + n<T>(155,4)*z[79] + z[97];
    z[97]=z[2]*z[97];
    z[101]=n<T>(35,2)*z[79] + 4*z[5];
    z[101]=z[5]*z[101];
    z[97]=z[101] + z[97];
    z[97]=z[2]*z[97];
    z[101]=z[79] - z[9];
    z[102]= - z[93] - z[101];
    z[102]=n<T>(1,2)*z[3] + 2*z[102] + n<T>(5,2)*z[10];
    z[102]=z[26]*z[102];
    z[97]= - z[32] - z[73] - z[97] - z[102] + z[60] - z[49];
    z[78]=n<T>(1,4)*z[2] - z[78];
    z[78]=z[2]*z[78];
    z[78]= - n<T>(1,4)*z[80] + z[78];
    z[80]=n<T>(1675,4)*z[7] - n<T>(2939,2)*z[79] - z[86];
    z[80]=z[7]*z[80];
    z[78]=25*z[78] + n<T>(1,4)*z[80];
    z[80]= - z[100] + n<T>(2939,24)*z[7] + n<T>(25,3)*z[2] + 3*z[79] - n<T>(25,12)*
    z[5];
    z[80]=z[80]*z[84];
    z[78]=n<T>(1,3)*z[78] + z[80];
    z[78]=z[9]*z[78];
    z[80]=z[11] + z[4];
    z[80]= - z[8] + n<T>(59,12)*z[80];
    z[84]= - z[6] + n<T>(59,12)*z[79] + z[93] - z[80];
    z[84]=z[28]*z[84];
    z[86]= - z[96] - z[99];
    z[86]=z[18]*z[86];
    z[86]=z[86] + z[59];
    z[93]=z[79]*z[9];
    z[93]=z[93] + z[29];
    z[96]=z[8]*z[79];
    z[96]=z[96] + z[93];
    z[96]=z[16]*z[96];
    z[99]=z[8] - z[101];
    z[99]=z[25]*z[99];
    z[96]=z[96] + z[99];
    z[99]= - z[4] - z[6];
    z[99]=z[79]*z[99];
    z[99]= - z[29] + z[99];
    z[99]=z[14]*z[99];
    z[95]=z[6] - z[95];
    z[95]=z[21]*z[95];
    z[95]=z[99] + z[95];
    z[99]=59*z[79] - 47*z[5];
    z[83]=5*z[99] + z[83];
    z[83]=z[83]*z[94];
    z[99]=313*z[79] - n<T>(251,2)*z[5];
    z[99]=z[5]*z[99];
    z[83]=z[99] + z[83];
    z[99]= - n<T>(7859,72)*z[7] + n<T>(659,12)*z[2] - n<T>(605,12)*z[5] - 15 - n<T>(173,4)*z[79];
    z[99]=z[7]*z[99];
    z[83]=n<T>(1,6)*z[83] + z[99];
    z[83]=z[7]*z[83];
    z[80]= - z[2] + z[80] + z[91];
    z[80]=z[79]*z[80];
    z[80]=n<T>(59,12)*z[29] + z[80];
    z[80]=z[12]*z[80];
    z[91]=n<T>(73,3)*z[7] + n<T>(99,2) + n<T>(181,3)*z[5];
    z[99]= - z[79]*z[91];
    z[99]= - n<T>(181,3)*z[29] + z[99];
    z[99]=z[15]*z[99];
    z[91]= - n<T>(181,3)*z[79] + z[91];
    z[91]=z[22]*z[91];
    z[100]=z[5] + z[3];
    z[100]=z[79]*z[100];
    z[100]=z[29] + z[100];
    z[100]=z[13]*z[100];
    z[102]= - z[10]*z[79];
    z[93]=z[102] - z[93];
    z[93]=z[17]*z[93];
    z[85]= - z[94] - z[85];
    z[85]=z[24]*z[85];
    z[94]=z[55] - z[38];
    z[102]=z[75] + z[50];
    z[103]=z[6] + z[3];
    z[103]=z[42]*z[103];
    z[103]=z[103] + z[52];
    z[104]= - n<T>(89,6)*z[37] - n<T>(41,3)*z[35] + n<T>(3,2)*z[34] + n<T>(77,4)*z[30];
    z[104]=i*z[104];
    z[105]=n<T>(95,3)*z[5] + n<T>(99,4) - n<T>(104,3)*z[79];
    z[105]=z[105]*npow(z[5],2);
    z[106]= - n<T>(1417,2)*z[5] + 359*z[2];
    z[106]=n<T>(1,4)*z[106] - 710*z[7];
    z[106]=n<T>(511,72)*z[11] + n<T>(301,36)*z[3] - n<T>(7,4)*z[10] + n<T>(679,36)*z[8]
    - n<T>(182,9)*z[6] - n<T>(97,144)*z[4] + n<T>(1,9)*z[106] + n<T>(177,16)*z[9];
    z[106]=z[29]*z[106];
    z[79]= - 185*z[9] - n<T>(3059,4)*z[7] + n<T>(397,2)*z[2] + 212*z[79] - n<T>(451,2)*z[5];
    z[79]=n<T>(1,3)*z[79] - n<T>(719,4)*z[8];
    z[79]=z[23]*z[79];
    z[98]= - z[2] + z[98] - z[5];
    z[98]=z[19]*z[98];
    z[92]=z[3] - z[92];
    z[92]=z[20]*z[92];
    z[101]=z[10] - z[101];
    z[101]=z[27]*z[101];
    z[107]= - z[6] - z[8];
    z[107]=z[43]*z[107];
    z[108]= - z[10] - z[11];
    z[108]=z[44]*z[108];

    r +=  - n<T>(709,12)*z[31] - n<T>(289,72)*z[33] + n<T>(73,12)*z[36] - n<T>(1,4)*
      z[39] - z[40] - n<T>(931,24)*z[41] + 7*z[45] - n<T>(15,4)*z[46] - n<T>(7,24)*
      z[47] + n<T>(13,24)*z[48] + n<T>(8,3)*z[51] - n<T>(25,24)*z[53] - n<T>(11,24)*
      z[54] - n<T>(9,4)*z[56] - n<T>(7,8)*z[57] - n<T>(25,8)*z[58] + n<T>(263,6)*z[61]
       + n<T>(1437,16)*z[62] + n<T>(399,8)*z[63] + n<T>(821,3)*z[64] + n<T>(91,4)*z[65]
       - n<T>(709,48)*z[66] - 2*z[67] + n<T>(533,16)*z[68] - n<T>(343,12)*z[69] + 
      n<T>(21,4)*z[70] + n<T>(185,48)*z[71] - n<T>(181,24)*z[72] - n<T>(17,16)*z[74]
     +  n<T>(59,16)*z[76] - z[77] + z[78] + z[79] + z[80] + z[81] + z[82] + 
      z[83] + z[84] + n<T>(25,3)*z[85] + n<T>(7,3)*z[86] + z[87] + z[88] + 
      z[89] + z[90] + z[91] + z[92] + z[93] - n<T>(37,6)*z[94] + 8*z[95] + 
      n<T>(1255,12)*z[96] - n<T>(1,3)*z[97] + n<T>(4,3)*z[98] + z[99] + z[100] + 
      z[101] + n<T>(1,2)*z[102] + n<T>(11,6)*z[103] + z[104] + z[105] + z[106]
       + n<T>(601,12)*z[107] + n<T>(17,4)*z[108];
 
    return r;
}

template std::complex<double> qg_2lha_tf801(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf801(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
