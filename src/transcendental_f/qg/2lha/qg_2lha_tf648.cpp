#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf648(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[37];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=e[6];
    z[17]=f[30];
    z[18]=f[31];
    z[19]=f[32];
    z[20]=f[33];
    z[21]=f[35];
    z[22]=f[36];
    z[23]=f[39];
    z[24]=f[43];
    z[25]=z[1]*i;
    z[26]=n<T>(1,2)*z[25];
    z[27]=z[7] + z[4];
    z[28]=n<T>(3,2)*z[3];
    z[29]= - n<T>(1,2)*z[8] + z[26] - z[28] + z[27];
    z[30]=n<T>(1,2)*z[7];
    z[29]=z[29]*z[30];
    z[28]=z[28] - z[4];
    z[31]= - z[28]*z[25];
    z[28]= - n<T>(1,4)*z[8] + z[26] + z[28];
    z[28]=z[8]*z[28];
    z[32]=npow(z[3],2);
    z[28]=z[29] + z[28] - n<T>(3,4)*z[32] + z[31];
    z[28]=z[7]*z[28];
    z[27]=z[16]*z[27];
    z[27]= - z[17] + z[28] + z[27];
    z[28]= - z[4] + n<T>(9,2)*z[3];
    z[26]=z[28]*z[26];
    z[28]=npow(z[4],2);
    z[29]=z[4] - n<T>(13,8)*z[3];
    z[29]=z[3]*z[29];
    z[31]=z[4] - n<T>(3,4)*z[3];
    z[31]=z[8]*z[31];
    z[26]=n<T>(3,2)*z[31] + z[26] + n<T>(1,4)*z[28] + z[29];
    z[26]=z[8]*z[26];
    z[29]=2*z[3];
    z[31]= - z[8] + z[29];
    z[33]=z[3] - z[4];
    z[31]=z[33]*z[31];
    z[34]= - z[4] - z[3];
    z[34]=z[34]*z[25];
    z[35]=z[25] - z[5];
    z[36]=z[4] + z[35];
    z[36]=z[5]*z[36];
    z[31]=z[36] + 2*z[34] + z[28] + z[31];
    z[31]=z[5]*z[31];
    z[34]= - 5*z[4] + n<T>(31,3)*z[3];
    z[34]=z[3]*z[34];
    z[34]= - 9*z[28] + z[34];
    z[34]=z[3]*z[34];
    z[32]=z[28] + n<T>(13,2)*z[32];
    z[32]=z[32]*z[25];
    z[32]=z[34] + z[32];
    z[34]= - n<T>(1,2)*z[2] + z[8] - z[35];
    z[33]=z[33]*z[34];
    z[29]=z[4] - z[29];
    z[29]=z[3]*z[29];
    z[28]=z[28] + z[29] + z[33];
    z[28]=z[2]*z[28];
    z[29]=z[5] + z[3];
    z[29]=z[25]*z[29];
    z[29]=z[13] + z[29];
    z[29]=z[6]*z[29];
    z[33]= - z[3] + z[35];
    z[33]=z[10]*z[33];
    z[29]= - z[19] + z[29] + z[33];
    z[33]=z[8] + z[4];
    z[33]= - z[25]*z[33];
    z[33]= - z[13] + z[33];
    z[33]=z[9]*z[33];
    z[25]=z[25] - z[8];
    z[34]= - z[4] + z[25];
    z[34]=z[12]*z[34];
    z[33]=z[33] + z[34];
    z[34]=z[2] - z[5];
    z[25]=9*z[4] + 13*z[3] - z[25];
    z[25]=n<T>(1,2)*z[25] - 2*z[34];
    z[25]=z[11]*z[25];
    z[34]=z[21] - z[15];
    z[35]=z[14]*i;
    z[35]=z[35] - z[22];
    z[36]=npow(z[4],3);
    z[30]=z[30] + n<T>(1,2)*z[5] - n<T>(5,24)*z[8] - z[4] + n<T>(7,4)*z[3];
    z[30]=z[13]*z[30];

    r +=  - n<T>(17,8)*z[18] - 5*z[20] - n<T>(9,8)*z[23] + z[24] + z[25] + 
      z[26] + n<T>(3,2)*z[27] + z[28] + 2*z[29] + z[30] + z[31] + n<T>(1,4)*
      z[32] + n<T>(5,2)*z[33] - n<T>(1,2)*z[34] - n<T>(3,8)*z[35] + n<T>(2,3)*z[36];
 
    return r;
}

template std::complex<double> qg_2lha_tf648(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf648(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
