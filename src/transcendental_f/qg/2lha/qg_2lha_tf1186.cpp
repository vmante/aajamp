#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1186(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[49];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=d[4];
    z[11]=d[8];
    z[12]=c[11];
    z[13]=c[19];
    z[14]=c[23];
    z[15]=c[25];
    z[16]=c[26];
    z[17]=c[28];
    z[18]=c[31];
    z[19]=f[0];
    z[20]=f[1];
    z[21]=f[3];
    z[22]=f[4];
    z[23]=f[5];
    z[24]=f[6];
    z[25]=f[8];
    z[26]=f[9];
    z[27]=f[11];
    z[28]=f[12];
    z[29]=f[13];
    z[30]=f[14];
    z[31]=f[16];
    z[32]=f[17];
    z[33]=f[18];
    z[34]=f[23];
    z[35]=f[26];
    z[36]=f[27];
    z[37]=z[3] - z[2];
    z[38]=2*z[4];
    z[39]=z[37]*z[38];
    z[40]=npow(z[2],2);
    z[41]=npow(z[3],2);
    z[42]= - z[3] + 2*z[2];
    z[43]=z[4] - z[42];
    z[44]= - z[5]*z[43];
    z[39]=z[44] + z[39] + z[40] - z[41];
    z[39]=z[5]*z[39];
    z[41]=2*z[5];
    z[43]=z[43]*z[41];
    z[44]=n<T>(1,2)*z[3];
    z[45]= - n<T>(8,3)*z[2] + z[44];
    z[45]=z[3]*z[45];
    z[46]=4*z[2];
    z[47]= - z[46] + z[3];
    z[47]=z[4]*z[47];
    z[43]=z[43] + n<T>(2,3)*z[47] + n<T>(25,6)*z[40] + z[45];
    z[45]=z[1]*i;
    z[43]=z[43]*z[45];
    z[47]= - z[2] - z[3];
    z[38]=n<T>(17,3)*z[45] + z[41] + n<T>(17,6)*z[47] - z[38];
    z[38]=z[6]*z[38];
    z[41]=n<T>(1,4)*z[9];
    z[47]=npow(z[10],2);
    z[47]= - z[41] + z[47];
    z[47]=z[10]*z[47];
    z[48]=npow(z[11],2);
    z[41]=z[41] - z[48];
    z[41]=z[11]*z[41];
    z[41]= - z[20] + z[30] - z[26] + z[24] - z[23] + z[47] + z[41] + 
    z[36] - z[33];
    z[42]=z[3]*z[42];
    z[37]=z[4]*z[37];
    z[37]= - 4*z[37] + 2*z[40] + z[42];
    z[37]=z[4]*z[37];
    z[42]=z[46] - z[44];
    z[42]=z[3]*z[42];
    z[40]= - z[40] + z[42];
    z[40]=z[3]*z[40];
    z[42]=npow(z[2],3);
    z[37]=z[37] - n<T>(11,2)*z[42] + z[40];
    z[40]= - z[2] - z[4];
    z[40]=n<T>(1,2)*z[40] + z[45];
    z[40]=z[7]*z[40];
    z[42]=z[22] + z[35] + z[28];
    z[44]=z[25] + z[19];
    z[46]= - n<T>(46,3)*z[16] - n<T>(25,6)*z[14] + n<T>(1,6)*z[12];
    z[46]=i*z[46];
    z[47]= - 23*z[2] + 71*z[3];
    z[47]=n<T>(1,8)*z[47] + z[4];
    z[47]=z[9]*z[47];
    z[45]=2*z[45] - z[2] - z[5];
    z[45]=z[8]*z[45];

    r += n<T>(35,72)*z[13] + n<T>(11,6)*z[15] + n<T>(23,3)*z[17] - n<T>(31,9)*z[18]
     + n<T>(1,6)*z[21] - z[27]
     + n<T>(5,12)*z[29] - z[31] - z[32] - n<T>(1,2)*z[34]
     +  n<T>(1,3)*z[37] + z[38] + z[39] + n<T>(5,3)*z[40] + n<T>(1,4)*z[41] + n<T>(3,4)*
      z[42] + z[43] - n<T>(9,4)*z[44] + 4*z[45] + z[46] + n<T>(1,9)*z[47];
 
    return r;
}

template std::complex<double> qg_2lha_tf1186(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1186(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
