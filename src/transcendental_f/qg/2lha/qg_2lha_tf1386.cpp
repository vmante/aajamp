#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1386(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[24];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=e[2];
    z[8]=e[10];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[31];
    z[13]=e[3];
    z[14]=f[4];
    z[15]=f[10];
    z[16]=f[12];
    z[17]=n<T>(2,3)*z[5];
    z[18]=n<T>(2,3)*z[4];
    z[19]=i*z[1];
    z[20]=z[3] - z[18] + z[17] - z[19];
    z[20]=z[3]*z[20];
    z[17]= - z[6] - z[17];
    z[17]=z[17]*z[19];
    z[18]=z[18] - z[5];
    z[21]=n<T>(2,3)*z[19] + z[18];
    z[21]=z[4]*z[21];
    z[22]=npow(z[5],2);
    z[22]=n<T>(1,3)*z[22];
    z[23]= - z[2] + n<T>(5,6)*z[4] + n<T>(1,6)*z[5] + z[19];
    z[23]=z[2]*z[23];
    z[17]=z[23] + z[20] + z[21] + z[17] + z[22] - n<T>(1,3)*z[8] - n<T>(2,3)*
    z[13] - z[7];
    z[17]=z[2]*z[17];
    z[20]= - z[6] + n<T>(5,3)*z[5];
    z[20]=z[20]*z[19];
    z[21]=n<T>(1,3)*z[19] - z[18];
    z[21]=z[4]*z[21];
    z[22]=z[22] + z[8] + z[7];
    z[23]= - 5*z[5] - z[4];
    z[23]=z[3]*z[23];
    z[20]=n<T>(1,6)*z[23] + z[21] + z[20] - z[22];
    z[20]=z[3]*z[20];
    z[18]=z[19]*z[18];
    z[19]= - 2*z[13] + n<T>(1,6)*z[9];
    z[18]=n<T>(1,3)*z[19] + z[8] + z[18];
    z[18]=z[4]*z[18];
    z[19]=n<T>(17,6)*z[9] - z[8];
    z[19]=z[5]*z[19];
    z[19]=z[19] - z[14] + 2*z[16] + z[15];
    z[21]= - z[6]*z[9];
    z[22]=z[1]*z[22];
    z[22]= - n<T>(1,6)*z[10] + z[22];
    z[22]=i*z[22];

    r += z[11] - n<T>(3,2)*z[12] + z[17] + z[18] + n<T>(1,3)*z[19] + z[20] + 
      z[21] + z[22];
 
    return r;
}

template std::complex<double> qg_2lha_tf1386(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1386(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
