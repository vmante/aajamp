#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf612(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[14];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[6];
    z[3]=d[8];
    z[4]=d[16];
    z[5]=d[18];
    z[6]=d[2];
    z[7]=d[7];
    z[8]=e[4];
    z[9]=e[11];
    z[10]= - z[5] - z[4] + z[2];
    z[11]=i*z[1];
    z[10]=z[11]*z[10];
    z[11]=z[11] + z[2];
    z[11]=z[3]*z[11];
    z[12]= - z[2] + n<T>(1,2)*z[6];
    z[12]=z[6]*z[12];
    z[13]= - z[3] + n<T>(1,2)*z[7];
    z[13]=z[7]*z[13];

    r += z[8] + z[9] + z[10] + z[11] + z[12] + z[13];
 
    return r;
}

template std::complex<double> qg_2lha_tf612(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf612(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
