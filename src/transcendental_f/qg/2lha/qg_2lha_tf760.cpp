#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf760(
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[13];
  std::complex<T> r(0,0);

    z[1]=d[0];
    z[2]=d[2];
    z[3]=d[6];
    z[4]=d[9];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=e[8];
    z[9]=z[3] - z[6];
    z[9]=z[7]*z[9];
    z[10]=npow(z[5],2);
    z[11]= - z[4] + z[6] + z[5];
    z[11]=z[2]*z[11];
    z[12]=n<T>(1,2)*z[1] - z[2] + z[4] - z[3];
    z[12]=z[1]*z[12];

    r += z[8] + z[9] - n<T>(1,2)*z[10] + z[11] + z[12];
 
    return r;
}

template std::complex<double> qg_2lha_tf760(
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf760(
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
