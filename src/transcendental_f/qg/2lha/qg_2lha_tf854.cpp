#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf854(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[34];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[7];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=e[0];
    z[7]=e[8];
    z[8]=c[3];
    z[9]=c[15];
    z[10]=c[18];
    z[11]=c[23];
    z[12]=c[25];
    z[13]=c[26];
    z[14]=c[28];
    z[15]=c[29];
    z[16]=c[30];
    z[17]=c[31];
    z[18]=e[1];
    z[19]=f[0];
    z[20]=f[3];
    z[21]=f[11];
    z[22]=f[17];
    z[23]=f[20];
    z[24]=n<T>(1,2)*z[5];
    z[25]=i*z[1];
    z[26]=z[24] - z[25];
    z[26]=z[26]*z[5];
    z[27]=n<T>(1,6)*z[8];
    z[28]=2*z[6];
    z[29]=z[27] - z[28];
    z[30]=z[25] + z[5];
    z[31]= - z[30] + n<T>(1,2)*z[3];
    z[31]=z[3]*z[31];
    z[32]=2*z[5] - z[2];
    z[32]=z[2]*z[32];
    z[33]= - z[5] + z[3];
    z[33]=z[4]*z[33];
    z[31]=n<T>(1,2)*z[33] + z[32] + z[31] - z[26] - 2*z[18] - z[29];
    z[31]=z[4]*z[31];
    z[32]=z[7] + z[18];
    z[25]=2*z[25] - z[3];
    z[25]=z[3]*z[25];
    z[30]=z[2] - z[30];
    z[30]=z[2]*z[30];
    z[25]=z[30] + z[25] + z[32] - z[29];
    z[25]=z[2]*z[25];
    z[24]=z[3]*z[24];
    z[24]=z[24] + z[26] + z[32];
    z[24]=z[3]*z[24];
    z[26]=2*z[1];
    z[28]= - z[7] - z[28];
    z[26]=z[28]*z[26];
    z[26]=z[26] + 6*z[13] + z[11];
    z[26]=i*z[26];
    z[28]= - z[15] - z[20] + z[21] + z[22];
    z[29]=z[10] - z[23];
    z[27]= - z[5]*z[27];

    r += n<T>(1,6)*z[9] - n<T>(4,3)*z[12] - 6*z[14] - 4*z[16] + n<T>(15,4)*z[17]
     +  2*z[19] + z[24] + z[25] + z[26] + z[27] + n<T>(1,2)*z[28] + n<T>(2,3)*
      z[29] + z[31];
 
    return r;
}

template std::complex<double> qg_2lha_tf854(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf854(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
