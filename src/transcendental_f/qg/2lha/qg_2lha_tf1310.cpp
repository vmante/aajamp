#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1310(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[53];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=d[4];
    z[11]=d[8];
    z[12]=c[11];
    z[13]=c[15];
    z[14]=c[18];
    z[15]=c[19];
    z[16]=c[23];
    z[17]=c[25];
    z[18]=c[26];
    z[19]=c[28];
    z[20]=c[29];
    z[21]=c[30];
    z[22]=c[31];
    z[23]=f[0];
    z[24]=f[1];
    z[25]=f[3];
    z[26]=f[4];
    z[27]=f[5];
    z[28]=f[6];
    z[29]=f[8];
    z[30]=f[9];
    z[31]=f[11];
    z[32]=f[12];
    z[33]=f[13];
    z[34]=f[14];
    z[35]=f[16];
    z[36]=f[17];
    z[37]=f[18];
    z[38]=f[23];
    z[39]=f[26];
    z[40]=f[27];
    z[41]=5*z[4];
    z[42]= - z[41] + 14*z[2];
    z[43]=3*z[3];
    z[44]= - z[43] + n<T>(1,3)*z[42];
    z[44]=z[44]*z[5];
    z[45]=z[41] + 8*z[2];
    z[45]=z[45]*z[4];
    z[46]=npow(z[2],2);
    z[45]=z[45] - 4*z[46];
    z[47]= - 2*z[4] + z[3];
    z[43]=z[47]*z[43];
    z[43]= - z[44] + n<T>(1,3)*z[45] + z[43];
    z[43]=z[5]*z[43];
    z[47]=n<T>(1,4)*z[9];
    z[48]=npow(z[10],2);
    z[48]=z[47] - z[48];
    z[48]=z[10]*z[48];
    z[49]=npow(z[11],2);
    z[47]= - z[47] + z[49];
    z[47]=z[11]*z[47];
    z[47]=z[24] - z[37] - z[34] + z[30] - z[28] + z[48] + z[47] + z[40]
    - z[39];
    z[48]= - 10*z[2] - z[4];
    z[48]=z[4]*z[48];
    z[48]= - 8*z[46] + z[48];
    z[48]=z[4]*z[48];
    z[49]=z[3] - z[42];
    z[49]=z[3]*z[49];
    z[45]=z[49] + z[45];
    z[45]=z[3]*z[45];
    z[49]=npow(z[2],3);
    z[45]=z[45] + 17*z[49] + z[48];
    z[48]=n<T>(20,3)*z[2] + z[4];
    z[48]=z[4]*z[48];
    z[42]=n<T>(2,3)*z[42] - z[3];
    z[42]=z[3]*z[42];
    z[42]=z[42] - n<T>(35,3)*z[46] + z[48];
    z[42]=i*z[42];
    z[46]=2*i;
    z[44]=z[46]*z[44];
    z[42]=z[42] + z[44];
    z[42]=z[1]*z[42];
    z[44]=z[32] - z[29] + z[26] - z[23];
    z[48]=z[25] + z[13];
    z[49]=z[35] + z[20];
    z[50]=n<T>(88,3)*z[18] + n<T>(35,3)*z[16] + n<T>(1,3)*z[12];
    z[50]=i*z[50];
    z[41]=z[2] + z[41];
    z[41]=7*z[41] - 59*z[3];
    z[41]=n<T>(1,4)*z[41] - 5*z[5];
    z[41]=z[9]*z[41];
    z[46]=z[46]*z[1];
    z[46]=z[46] - z[2];
    z[51]=z[3] - z[46];
    z[51]=z[6]*z[51];
    z[52]=z[4] - z[46];
    z[52]=z[7]*z[52];
    z[46]=z[5] - z[46];
    z[46]=z[8]*z[46];

    r += n<T>(8,3)*z[14] - n<T>(7,36)*z[15] - n<T>(19,3)*z[17] - n<T>(56,3)*z[19]
     - 8*
      z[21] + n<T>(157,9)*z[22] + n<T>(1,6)*z[27] + n<T>(5,3)*z[31] + n<T>(13,6)*z[33]
       + 4*z[36] + z[38] + n<T>(1,9)*z[41] + z[42] + z[43] - n<T>(3,2)*z[44] + 
      n<T>(1,3)*z[45] + n<T>(20,3)*z[46] + n<T>(1,2)*z[47] + n<T>(2,3)*z[48] - 2*z[49]
       + z[50] + n<T>(17,3)*z[51] + n<T>(7,3)*z[52];
 
    return r;
}

template std::complex<double> qg_2lha_tf1310(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1310(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
