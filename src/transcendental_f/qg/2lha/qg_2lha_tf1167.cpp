#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1167(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[18];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[12];
    z[4]=d[0];
    z[5]=d[2];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[3];
    z[10]=e[0];
    z[11]=e[5];
    z[12]=z[6] + z[7];
    z[13]=z[5] - z[8];
    z[14]=n<T>(1,2)*z[13];
    z[15]=z[1]*i;
    z[16]=z[9] - z[15] + z[14] - n<T>(1,2)*z[12];
    z[16]=z[2]*z[16];
    z[13]= - z[4] + z[12] + z[13];
    z[13]=z[4]*z[13];
    z[17]=npow(z[9],2);
    z[13]=z[17] - z[13];
    z[12]= - z[12]*z[14];
    z[14]=z[3]*z[15];

    r +=  - z[10] - z[11] + z[12] - n<T>(1,2)*z[13] + z[14] + z[16];
 
    return r;
}

template std::complex<double> qg_2lha_tf1167(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1167(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
