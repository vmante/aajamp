#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf503(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[2];
    z[6]=d[8];
    z[7]=d[11];
    z[8]=d[3];
    z[9]=d[9];
    z[10]=d[12];
    z[11]=d[4];
    z[12]=d[7];
    z[13]=d[17];
    z[14]=e[5];
    z[15]=e[7];
    z[16]=e[9];
    z[17]=e[12];
    z[18]=e[13];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=e[6];
    z[23]=e[14];
    z[24]=f[30];
    z[25]=f[31];
    z[26]=f[32];
    z[27]=f[33];
    z[28]=f[36];
    z[29]=f[39];
    z[30]=f[43];
    z[31]=f[50];
    z[32]=f[51];
    z[33]=f[52];
    z[34]=f[53];
    z[35]=f[55];
    z[36]=f[58];
    z[37]=z[1]*i;
    z[38]=z[37] + z[3];
    z[39]=n<T>(1,2)*z[4];
    z[40]= - z[39] + z[38];
    z[40]=z[40]*z[39];
    z[41]=z[37] - z[8];
    z[42]= - z[39] - z[41];
    z[43]=n<T>(1,2)*z[8];
    z[42]=z[42]*z[43];
    z[44]=z[37]*z[3];
    z[45]=npow(z[3],2);
    z[40]=z[42] + z[40] - z[45] + z[44];
    z[42]=5*z[8];
    z[40]=z[40]*z[42];
    z[46]=n<T>(17,2)*z[45] + 7*z[44];
    z[47]=z[37] - z[5];
    z[48]=z[47] - z[9];
    z[49]=n<T>(7,2)*z[3];
    z[50]= - z[49] - z[48];
    z[51]=n<T>(1,4)*z[5];
    z[50]=z[50]*z[51];
    z[52]=z[9]*z[3];
    z[46]=z[50] + n<T>(1,4)*z[46] - 2*z[52];
    z[46]=z[5]*z[46];
    z[50]=z[45]*z[37];
    z[52]= - n<T>(7,6)*z[4] + n<T>(5,2)*z[3] - z[37];
    z[52]=z[52]*z[39];
    z[52]=z[45] + z[52];
    z[53]=5*z[4];
    z[52]=z[52]*z[53];
    z[54]=npow(z[3],3);
    z[40]=z[46] + z[40] + z[52] - n<T>(85,12)*z[54] - 4*z[50];
    z[46]=n<T>(1,2)*z[45];
    z[50]=z[46] + z[44];
    z[52]=n<T>(1,3)*z[4];
    z[54]=n<T>(1,3)*z[37];
    z[55]=z[3] - z[54];
    z[55]= - n<T>(1,3)*z[11] + n<T>(1,6)*z[12] + n<T>(1,2)*z[55] - z[52];
    z[55]=z[11]*z[55];
    z[55]=z[50] + z[55];
    z[56]= - z[37]*z[52];
    z[54]= - z[3] - z[54];
    z[54]=n<T>(1,12)*z[12] + n<T>(1,2)*z[54] + z[52];
    z[54]=z[12]*z[54];
    z[54]=z[54] + z[56] + n<T>(1,2)*z[55];
    z[54]=z[11]*z[54];
    z[55]=z[12] + z[4];
    z[56]=z[37]*z[55];
    z[56]=z[19] + z[56];
    z[56]=z[13]*z[56];
    z[55]= - z[37] + z[55];
    z[55]=z[16]*z[55];
    z[54]=z[55] + z[54] + z[56];
    z[55]= - n<T>(1,4)*z[2] + n<T>(1,2)*z[12] + z[43];
    z[56]=z[4] - z[3];
    z[55]=z[56]*z[55];
    z[57]= - z[4] - z[38];
    z[57]=z[57]*z[39];
    z[55]=z[57] + z[45] + n<T>(1,2)*z[44] + z[55];
    z[55]=z[2]*z[55];
    z[49]=z[2] - z[12] - n<T>(5,2)*z[4] - z[49] + z[41];
    z[49]=z[15]*z[49];
    z[57]= - z[8]*z[37];
    z[57]= - z[19] - z[44] + z[57];
    z[57]=z[10]*z[57];
    z[41]=z[3] - z[41];
    z[41]=z[14]*z[41];
    z[41]=z[55] + z[49] + z[57] + z[41];
    z[49]=3*z[3];
    z[55]= - z[49] - z[37];
    z[55]=z[51] + n<T>(1,2)*z[55] + z[9];
    z[55]=z[5]*z[55];
    z[49]=z[5] + z[49] - z[37];
    z[57]=z[6] + z[9];
    z[49]= - z[57] + n<T>(1,2)*z[49];
    z[58]=n<T>(1,2)*z[6];
    z[49]=z[49]*z[58];
    z[59]=z[37]*z[9];
    z[49]=z[49] + z[55] + n<T>(3,2)*z[50] - z[59];
    z[49]=z[49]*z[58];
    z[38]= - z[43] + z[38];
    z[38]=z[38]*z[42];
    z[38]=z[38] - n<T>(7,2)*z[45] - z[44];
    z[42]= - z[42] - z[3] + 5*z[37];
    z[42]=n<T>(1,6)*z[42] + z[9];
    z[42]=z[9]*z[42];
    z[38]=n<T>(1,3)*z[38] + z[42];
    z[38]=z[9]*z[38];
    z[42]=z[23]*z[57];
    z[38]=z[31] + z[38] - z[42];
    z[42]= - 31*z[3] + z[53];
    z[42]= - n<T>(5,3)*z[11] - z[6] - z[51] + n<T>(19,6)*z[9] + n<T>(5,4)*z[12] + n<T>(1,3)*z[42]
     - n<T>(5,2)*z[8];
    z[42]=z[19]*z[42];
    z[43]= - z[5]*z[37];
    z[43]= - z[19] - z[59] + z[43];
    z[43]=z[7]*z[43];
    z[45]=z[17]*z[48];
    z[42]= - z[45] + z[42] + z[43];
    z[43]=z[46] - n<T>(7,3)*z[44];
    z[37]=z[37] - z[39];
    z[37]=z[37]*z[52];
    z[39]=z[8]*z[56];
    z[37]= - n<T>(1,3)*z[39] + n<T>(1,2)*z[43] + z[37];
    z[39]=n<T>(7,8)*z[3] - z[4];
    z[39]=z[12]*z[39];
    z[37]=n<T>(1,2)*z[37] + n<T>(1,3)*z[39];
    z[37]=z[12]*z[37];
    z[39]= - z[4] - z[11];
    z[39]=z[22]*z[39];
    z[39]=z[24] + z[39] - z[30];
    z[43]= - z[18]*z[47];
    z[44]=z[20]*i;

    r +=  - n<T>(2,3)*z[21] + n<T>(55,24)*z[25] + n<T>(5,4)*z[26] + n<T>(20,3)*z[27]
     - 
      n<T>(5,24)*z[28] + n<T>(5,8)*z[29] - n<T>(7,24)*z[32] + n<T>(5,12)*z[33] - 2*
      z[34] - n<T>(1,8)*z[35] + n<T>(3,8)*z[36] + 5*z[37] + n<T>(1,2)*z[38] + n<T>(5,6)
      *z[39] + n<T>(1,3)*z[40] + n<T>(5,3)*z[41] + n<T>(1,6)*z[42] + z[43] + n<T>(13,36)
      *z[44] + z[49] + n<T>(5,2)*z[54];
 
    return r;
}

template std::complex<double> qg_2lha_tf503(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf503(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
