#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1412(
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[13];
  std::complex<T> r(0,0);

    z[1]=d[0];
    z[2]=d[2];
    z[3]=d[4];
    z[4]=d[7];
    z[5]=d[8];
    z[6]=d[1];
    z[7]=d[3];
    z[8]=e[10];
    z[9]=z[4] + z[3];
    z[10]=z[6] + z[2];
    z[11]=z[10] - z[1];
    z[11]= - z[11]*z[9];
    z[10]=z[6]*z[10];
    z[12]= - z[1]*z[2];
    z[9]= - z[6] - z[1] + z[9];
    z[9]=z[5]*z[9];
    z[9]=z[9] + z[10] + z[12] + z[11];
    z[10]=z[2] - n<T>(1,2)*z[7];
    z[10]=z[7]*z[10];

    r += z[8] + n<T>(1,2)*z[9] + z[10];
 
    return r;
}

template std::complex<double> qg_2lha_tf1412(
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1412(
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
