#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf229(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[85];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=f[0];
    z[10]=f[3];
    z[11]=f[5];
    z[12]=f[8];
    z[13]=f[11];
    z[14]=f[13];
    z[15]=f[17];
    z[16]=f[20];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[18];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[29];
    z[27]=c[30];
    z[28]=c[31];
    z[29]=c[32];
    z[30]=c[43];
    z[31]=c[46];
    z[32]=c[47];
    z[33]=c[51];
    z[34]=c[54];
    z[35]=c[55];
    z[36]=c[58];
    z[37]=c[59];
    z[38]=c[60];
    z[39]=c[61];
    z[40]=c[62];
    z[41]=c[66];
    z[42]=c[68];
    z[43]=c[69];
    z[44]=c[72];
    z[45]=c[75];
    z[46]=c[76];
    z[47]=c[77];
    z[48]=c[78];
    z[49]=c[79];
    z[50]=c[80];
    z[51]=c[83];
    z[52]=g[0];
    z[53]=g[3];
    z[54]=g[5];
    z[55]=g[8];
    z[56]=g[11];
    z[57]=g[13];
    z[58]=g[17];
    z[59]=g[21];
    z[60]=g[29];
    z[61]=g[30];
    z[62]=g[35];
    z[63]=g[41];
    z[64]=g[43];
    z[65]=g[56];
    z[66]=g[57];
    z[67]=g[76];
    z[68]=g[93];
    z[69]=g[94];
    z[70]=g[107];
    z[71]=g[125];
    z[72]=n<T>(1,2)*z[5];
    z[73]=i*z[1];
    z[74]=373*z[73] + z[72];
    z[74]=z[5]*z[74];
    z[75]=3*z[8];
    z[76]=n<T>(493,36)*z[17] + z[75];
    z[76]= - n<T>(3827,108)*z[6] + n<T>(1,2)*z[76] - z[7];
    z[77]= - n<T>(221,4)*z[73] - n<T>(430,11)*z[5];
    z[77]=n<T>(1,3)*z[77] + n<T>(1291,88)*z[4];
    z[77]=z[4]*z[77];
    z[78]= - n<T>(131,54)*z[3] - n<T>(394,27)*z[4] + n<T>(262,27)*z[73] - n<T>(61,2)*
    z[5];
    z[78]=z[3]*z[78];
    z[74]=n<T>(1,33)*z[78] + n<T>(1,9)*z[77] + n<T>(1,11)*z[76] + n<T>(1,36)*z[74];
    z[74]=z[3]*z[74];
    z[76]=n<T>(6227,27)*z[7] - n<T>(15635,216)*z[17] - z[75];
    z[77]= - n<T>(5915,3)*z[73] - n<T>(2533,2)*z[5];
    z[77]=z[5]*z[77];
    z[78]=n<T>(2641,3)*z[4] - n<T>(7003,2)*z[73] + 6667*z[5];
    z[78]=z[4]*z[78];
    z[76]=n<T>(1,54)*z[78] + n<T>(1,18)*z[77] + n<T>(1,2)*z[76] - n<T>(4592,27)*z[6];
    z[77]=n<T>(1,11)*z[4];
    z[76]=z[76]*z[77];
    z[78]=3*z[12];
    z[79]= - n<T>(953,2)*z[26] + n<T>(48967,36)*z[28] - 809*z[27];
    z[79]= - n<T>(41,3)*z[14] - n<T>(169,4)*z[15] + n<T>(445,6)*z[16] + n<T>(953,24)*
    z[19] + n<T>(953,6)*z[20] - n<T>(811,36)*z[21] + n<T>(271,4)*z[23] + n<T>(1,4)*z[79]
    + n<T>(1027,3)*z[25];
    z[79]=n<T>(1,3)*z[79] - n<T>(97,4)*z[13];
    z[79]= - n<T>(353,9)*z[9] + n<T>(877,108)*z[10] - n<T>(27,2)*z[11] + n<T>(1,3)*z[79]
    + z[78];
    z[80]= - 4297*z[24] - 503*z[22];
    z[80]=n<T>(1,27)*z[80] - n<T>(117,2)*z[18];
    z[81]=n<T>(929,3)*z[6] + 16*z[8] - n<T>(4709,33)*z[7];
    z[81]=z[1]*z[81];
    z[80]=n<T>(1,22)*z[80] + n<T>(1,9)*z[81];
    z[80]=i*z[80];
    z[81]= - n<T>(56,3)*z[6] + n<T>(535,11)*z[7] + n<T>(256321,1584)*z[17] - 8*z[8];
    z[82]=n<T>(2825,3)*z[73] + 91*z[5];
    z[82]=z[5]*z[82];
    z[81]=n<T>(1,3)*z[81] + n<T>(1,44)*z[82];
    z[82]=n<T>(1,3)*z[5];
    z[81]=z[81]*z[82];
    z[74]=z[74] + z[76] + z[81] + n<T>(1,11)*z[79] + z[80];
    z[74]=z[3]*z[74];
    z[75]= - n<T>(12569,108)*z[17] + z[75];
    z[76]=461*z[73] - n<T>(373,6)*z[5];
    z[76]=z[5]*z[76];
    z[79]=916*z[4] - 3664*z[73] - 2401*z[5];
    z[79]=z[4]*z[79];
    z[75]=n<T>(1,81)*z[79] + n<T>(7,36)*z[76] - 2*z[6] + n<T>(1,2)*z[75] + n<T>(491,27)*
    z[7];
    z[75]=z[75]*z[77];
    z[76]= - 91*z[26] + n<T>(198491,162)*z[28] - 87*z[27];
    z[76]= - 140*z[9] + n<T>(3163,108)*z[10] - n<T>(3631,54)*z[11] + z[78] - 
   n<T>(1145,108)*z[13] - n<T>(955,54)*z[14] - n<T>(2065,36)*z[15] + n<T>(140,3)*z[16]
    + n<T>(91,6)*z[19] + n<T>(182,3)*z[20] - n<T>(2293,162)*z[21] + n<T>(3803,54)*z[23]
    + n<T>(1,2)*z[76] + n<T>(7151,27)*z[25];
    z[78]= - 11656*z[24] - n<T>(4177,2)*z[22];
    z[78]=n<T>(1,11)*z[78] + n<T>(889,12)*z[18];
    z[79]=n<T>(2296,3)*z[6] + n<T>(877,3)*z[8] - 157*z[7];
    z[79]=z[1]*z[79];
    z[78]=n<T>(1,3)*z[78] + n<T>(4,11)*z[79];
    z[79]=n<T>(1,9)*i;
    z[78]=z[78]*z[79];
    z[80]=n<T>(443,6)*z[7];
    z[81]=z[80] + n<T>(12257,16)*z[17] - n<T>(1754,3)*z[8];
    z[83]=1543*z[73] + n<T>(2603,3)*z[5];
    z[83]=z[5]*z[83];
    z[81]=n<T>(1,108)*z[83] + n<T>(1,9)*z[81] + 4*z[6];
    z[83]=n<T>(1,11)*z[5];
    z[81]=z[81]*z[83];
    z[75]=z[75] + z[81] + n<T>(1,11)*z[76] + z[78];
    z[75]=z[4]*z[75];
    z[76]= - 7166*z[73] + n<T>(3095,2)*z[5];
    z[76]=z[5]*z[76];
    z[76]=z[76] - n<T>(9739,4)*z[6] - 644*z[7] + n<T>(132401,16)*z[17] - 2896*
    z[8];
    z[78]=n<T>(1369,4)*z[4] - n<T>(6733,18)*z[73] - 13*z[5];
    z[78]=z[4]*z[78];
    z[76]=n<T>(1,9)*z[76] + z[78];
    z[78]= - 391*z[73] + n<T>(1423,27)*z[5];
    z[78]=n<T>(1747,36)*z[3] + n<T>(1,2)*z[78] + 18*z[4];
    z[78]=z[3]*z[78];
    z[81]=61961*z[73] + 6953*z[5];
    z[81]= - n<T>(65509,8)*z[2] + n<T>(2921,2)*z[3] + n<T>(1,2)*z[81] - 3163*z[4];
    z[81]=z[2]*z[81];
    z[76]=n<T>(1,81)*z[81] + n<T>(1,3)*z[76] + z[78];
    z[76]=z[2]*z[76];
    z[78]= - n<T>(27935,12)*z[18] - n<T>(22669,2)*z[24] - 3343*z[22];
    z[81]=n<T>(12959,3)*z[6] + 2798*z[8] + n<T>(3455,3)*z[7];
    z[81]=z[1]*z[81];
    z[78]=n<T>(1,3)*z[78] + z[81];
    z[78]=z[78]*z[79];
    z[81]= - n<T>(303443,108)*z[28] - 761*z[27];
    z[81]=n<T>(1,3)*z[81] - n<T>(299,2)*z[26];
    z[76]=z[76] + z[78] - n<T>(371,6)*z[9] - n<T>(82,27)*z[10] - n<T>(1829,54)*z[11]
    - 82*z[12] - n<T>(532,27)*z[13] - n<T>(1973,54)*z[14] - n<T>(235,6)*z[15] + n<T>(85,9)*z[16]
     + n<T>(299,24)*z[19]
     + n<T>(299,6)*z[20] - n<T>(1901,162)*z[21]
     + n<T>(5321,108)*z[23]
     + n<T>(1,4)*z[81]
     + n<T>(4414,27)*z[25];
    z[78]=2447*z[73] + 475*z[5];
    z[78]=n<T>(2,3)*z[78] - n<T>(2581,2)*z[4];
    z[78]=z[4]*z[78];
    z[78]=z[78] - n<T>(2065,2)*z[6] + n<T>(89,6)*z[7] - 1205*z[17] - n<T>(203,2)*
    z[8];
    z[81]=n<T>(2974,99)*z[73] - n<T>(17,2)*z[5];
    z[81]=z[5]*z[81];
    z[84]= - n<T>(449,2)*z[73] - n<T>(1832,11)*z[5];
    z[84]=n<T>(2435,594)*z[3] + n<T>(1,9)*z[84] + n<T>(103,22)*z[4];
    z[84]=z[3]*z[84];
    z[78]=z[84] + z[81] + n<T>(1,33)*z[78];
    z[78]=z[3]*z[78];
    z[81]= - n<T>(5009,22)*z[73] - n<T>(115,3)*z[5];
    z[81]=z[81]*z[82];
    z[80]= - n<T>(400,3)*z[6] + z[80] - n<T>(19487,8)*z[17] - n<T>(503,3)*z[8];
    z[80]=n<T>(1,11)*z[80] + z[81];
    z[80]=z[5]*z[80];
    z[81]= - n<T>(611,2)*z[17] - n<T>(3589,3)*z[8];
    z[82]=413*z[73] + 379*z[5];
    z[82]=z[5]*z[82];
    z[81]=n<T>(4,3)*z[82] - n<T>(4592,3)*z[6] + n<T>(1,2)*z[81] - 811*z[7];
    z[82]=n<T>(2161,162)*z[4] - n<T>(1339,54)*z[73] - 100*z[5];
    z[82]=z[4]*z[82];
    z[81]=n<T>(1,9)*z[81] + z[82];
    z[77]=z[81]*z[77];
    z[76]=n<T>(1,3)*z[78] + z[77] + n<T>(1,9)*z[80] + n<T>(1,11)*z[76];
    z[76]=z[2]*z[76];
    z[77]=n<T>(13487,12)*z[18] + 6007*z[24] + 2753*z[22];
    z[78]= - 676*z[8] - n<T>(443,3)*z[7];
    z[78]=n<T>(1,11)*z[78] + n<T>(112,3)*z[6];
    z[78]=z[1]*z[78];
    z[77]=n<T>(1,33)*z[77] + z[78];
    z[77]=z[77]*z[79];
    z[72]= - 2*z[73] + z[72];
    z[72]=z[5]*z[72];
    z[73]= - n<T>(47513,12)*z[17] + 4549*z[8];
    z[73]=n<T>(1,3)*z[73] - 535*z[7];
    z[72]=n<T>(1645,81)*z[72] + n<T>(1,18)*z[73] - 6*z[6];
    z[72]=z[72]*z[83];
    z[73]=n<T>(79829,9)*z[28] - 2575*z[27];
    z[73]= - 398*z[23] - n<T>(3926,3)*z[25] + n<T>(1,2)*z[73] - 301*z[26];
    z[73]=n<T>(731,33)*z[13] + n<T>(365,33)*z[14] + n<T>(235,22)*z[15] + n<T>(589,66)*
    z[16] + n<T>(301,33)*z[19] + n<T>(1204,33)*z[20] + n<T>(1,11)*z[73] - n<T>(17,36)*
    z[21];
    z[72]=z[72] + z[77] - n<T>(877,198)*z[9] + n<T>(293,297)*z[10] - n<T>(917,594)*
    z[11] + n<T>(1,9)*z[73] + n<T>(100,11)*z[12];
    z[72]=z[5]*z[72];
    z[73]=n<T>(27935,6)*z[32] - 26947*z[41] - n<T>(41903,3)*z[37];
    z[77]=n<T>(611,18)*z[13] + n<T>(380,3)*z[14] - n<T>(2032,9)*z[16] + n<T>(529,2)*
    z[15];
    z[77]=n<T>(1,3)*z[77] - 24*z[12];
    z[77]=n<T>(2608,99)*z[9] - n<T>(37,9)*z[10] + n<T>(1,11)*z[77] + n<T>(323,27)*z[11];
    z[77]=z[1]*z[77];
    z[73]=n<T>(1,594)*z[73] + z[77];
    z[73]=i*z[73];
    z[77]=z[38] - z[69];
    z[78]=z[36] + z[50];
    z[79]=n<T>(3739,3)*z[17] - n<T>(35,4)*z[6];
    z[79]=z[6]*z[79];
    z[79]=z[79] - z[67];
    z[80]=z[54] + n<T>(8,3)*z[71] + n<T>(1,2)*z[70];
    z[81]=n<T>(10495,594)*z[17] - 2*z[8];
    z[81]=z[8]*z[81];
    z[82]= - n<T>(15331,132)*z[17] + 23*z[7];
    z[82]=z[7]*z[82];

    r += n<T>(2,495)*z[29] - n<T>(23,132)*z[30] + n<T>(169,264)*z[31] - n<T>(223,264)*
      z[33] - n<T>(35,6)*z[34] - n<T>(383615,14256)*z[35] + n<T>(68921,14256)*z[39]
       - n<T>(17933,264)*z[40] + n<T>(2225,396)*z[42] + n<T>(1175,792)*z[43] + n<T>(331,88)*z[44]
     + n<T>(761,132)*z[45]
     + n<T>(41,88)*z[46] - n<T>(5,22)*z[47] - n<T>(21,11)*z[48]
     + z[49]
     + n<T>(302471,14256)*z[51] - 3*z[52]
     + n<T>(109,88)*
      z[53] - n<T>(350,33)*z[55] + n<T>(6719,792)*z[56] + n<T>(89,198)*z[57] + n<T>(857,792)*z[58]
     + n<T>(65,132)*z[59] - n<T>(1900,297)*z[60] - n<T>(263,36)*z[61]
       + n<T>(6701,2376)*z[62] + n<T>(1129,2376)*z[63] - n<T>(419,396)*z[64] - n<T>(203,792)*z[65]
     + n<T>(221,792)*z[66]
     + n<T>(3,44)*z[68]
     + z[72]
     + z[73]
     +  z[74] + z[75] + z[76] - n<T>(1,6)*z[77] + n<T>(27,44)*z[78] + n<T>(1,22)*
      z[79] + n<T>(1,11)*z[80] + z[81] + n<T>(1,18)*z[82];
 
    return r;
}

template std::complex<double> qg_2lha_tf229(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf229(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
