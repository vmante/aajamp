#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf862(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[98];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[16];
    z[11]=d[8];
    z[12]=d[11];
    z[13]=d[9];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[4];
    z[19]=e[5];
    z[20]=e[7];
    z[21]=e[8];
    z[22]=e[9];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[17];
    z[28]=c[18];
    z[29]=c[19];
    z[30]=c[21];
    z[31]=c[23];
    z[32]=c[25];
    z[33]=c[26];
    z[34]=c[28];
    z[35]=c[29];
    z[36]=c[30];
    z[37]=c[31];
    z[38]=e[3];
    z[39]=e[10];
    z[40]=e[6];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[3];
    z[45]=f[4];
    z[46]=f[5];
    z[47]=f[10];
    z[48]=f[11];
    z[49]=f[12];
    z[50]=f[13];
    z[51]=f[14];
    z[52]=f[16];
    z[53]=f[17];
    z[54]=f[18];
    z[55]=f[21];
    z[56]=f[30];
    z[57]=f[31];
    z[58]=f[32];
    z[59]=f[33];
    z[60]=f[34];
    z[61]=f[35];
    z[62]=f[36];
    z[63]=f[37];
    z[64]=f[39];
    z[65]=f[41];
    z[66]=f[43];
    z[67]=f[50];
    z[68]=f[51];
    z[69]=f[52];
    z[70]=f[55];
    z[71]=f[58];
    z[72]=f[76];
    z[73]=n<T>(1,2)*z[7];
    z[74]=n<T>(1,2)*z[6];
    z[75]=z[73] + z[74];
    z[76]=z[1]*i;
    z[77]=n<T>(1,2)*z[76];
    z[78]=z[77] - n<T>(5,3);
    z[79]=n<T>(1,2)*z[5];
    z[80]= - n<T>(1,4)*z[8] - z[79] + z[78] + z[75];
    z[80]=z[8]*z[80];
    z[81]=n<T>(1,2)*z[2];
    z[82]=z[81] - z[4];
    z[83]=n<T>(1,3)*z[6];
    z[78]= - z[83] - z[78] - z[82];
    z[84]=n<T>(1,2)*z[3];
    z[78]=z[78]*z[84];
    z[85]=z[76]*z[79];
    z[86]=z[76] - z[5];
    z[87]= - n<T>(1,4)*z[6] - z[86];
    z[87]=z[87]*z[83];
    z[88]=n<T>(1,4)*z[4];
    z[89]=n<T>(3,2)*z[76];
    z[90]= - z[88] + z[6] - z[89] - z[5];
    z[90]=z[4]*z[90];
    z[91]=z[86] + z[73];
    z[92]= - z[6] - z[91];
    z[92]=z[92]*z[73];
    z[82]= - n<T>(1,2)*z[86] - z[6] - z[82];
    z[82]=z[2]*z[82];
    z[78]=z[78] + z[80] + z[82] + z[92] + z[90] + z[85] + z[87];
    z[78]=z[78]*z[84];
    z[80]=z[79] - z[76];
    z[79]= - z[80]*z[79];
    z[82]=z[74] - z[76];
    z[85]=n<T>(1,4)*z[5];
    z[87]= - z[85] + z[82];
    z[87]=z[6]*z[87];
    z[79]=z[79] + z[87];
    z[79]=z[6]*z[79];
    z[87]=z[4] + z[13];
    z[87]=z[76]*z[87];
    z[87]=z[24] + z[87];
    z[87]=z[12]*z[87];
    z[90]=z[76] - z[4];
    z[92]= - z[13] + z[90];
    z[92]=z[23]*z[92];
    z[93]=z[3] + z[11];
    z[93]=z[39]*z[93];
    z[94]= - z[11] - z[13];
    z[94]=z[41]*z[94];
    z[79]=z[92] + z[93] + z[94] + z[67] - z[47] + z[79] + z[87];
    z[87]=n<T>(1,3)*z[5];
    z[92]=z[80]*z[87];
    z[93]=z[74] + z[86];
    z[93]=z[6]*z[93];
    z[94]=n<T>(1,2)*z[4];
    z[95]=z[94] - z[76];
    z[96]=z[4]*z[95];
    z[97]=z[73] + z[90];
    z[97]=z[7]*z[97];
    z[92]=z[97] + n<T>(1,3)*z[96] + z[92] + z[93];
    z[75]=n<T>(1,6)*z[4] + n<T>(1,3)*z[80] + z[75];
    z[93]=n<T>(1,3)*z[3];
    z[75]= - n<T>(1,6)*z[11] + n<T>(1,2)*z[75] - z[93];
    z[75]=z[11]*z[75];
    z[96]= - z[84] - z[86];
    z[93]=z[96]*z[93];
    z[75]=z[75] + n<T>(1,2)*z[92] + z[93];
    z[75]=z[11]*z[75];
    z[92]=z[80]*z[5];
    z[91]=z[7]*z[91];
    z[91]=z[92] + z[91];
    z[93]=npow(z[4],2);
    z[90]= - n<T>(1,2)*z[11] - z[90];
    z[90]=z[11]*z[90];
    z[86]= - z[7] - z[86];
    z[86]=z[13]*z[86];
    z[86]=n<T>(1,2)*z[86] + n<T>(1,3)*z[90] - n<T>(1,6)*z[93] + 5*z[91];
    z[86]=z[13]*z[86];
    z[75]=z[75] + z[86] + z[55] - z[54];
    z[86]=n<T>(13,2)*z[5];
    z[90]=7*z[76] - z[86];
    z[90]=z[90]*z[87];
    z[91]=n<T>(3,4)*z[6];
    z[93]=z[91] - z[5];
    z[77]=z[77] - z[93];
    z[77]=z[77]*z[74];
    z[88]=z[95]*z[88];
    z[95]=n<T>(3,8)*z[6];
    z[96]= - 5 - n<T>(17,2)*z[76];
    z[96]=n<T>(1,2)*z[96] - z[5];
    z[96]=n<T>(145,36)*z[7] + n<T>(1,8)*z[4] + n<T>(1,3)*z[96] + z[95];
    z[96]=z[7]*z[96];
    z[77]=z[96] + z[88] + z[90] + z[77];
    z[77]=z[77]*z[73];
    z[88]= - z[95] + z[80];
    z[88]=z[6]*z[88];
    z[90]=n<T>(193,4)*z[76] + 4*z[5];
    z[90]= - n<T>(167,24)*z[2] + n<T>(2,3)*z[7] + n<T>(19,12)*z[4] + n<T>(1,3)*z[90]
     - 
    z[74];
    z[90]=z[2]*z[90];
    z[95]= - n<T>(61,2)*z[76] + 25*z[5];
    z[95]=z[5]*z[95];
    z[96]= - n<T>(133,4)*z[76] - 8*z[5];
    z[96]=n<T>(121,24)*z[4] + n<T>(1,3)*z[96] + z[6];
    z[96]=z[4]*z[96];
    z[97]= - n<T>(17,8)*z[7] - 11*z[76] + n<T>(17,4)*z[5];
    z[97]=z[7]*z[97];
    z[88]=z[90] + n<T>(1,3)*z[97] + z[96] + n<T>(1,6)*z[95] + z[88];
    z[88]=z[2]*z[88];
    z[90]= - 10*z[76] + n<T>(13,4)*z[5];
    z[90]=n<T>(25,12)*z[2] - n<T>(13,12)*z[7] - z[94] + n<T>(1,3)*z[90] - z[74];
    z[90]=z[2]*z[90];
    z[85]=5*z[76] - z[85];
    z[85]=n<T>(17,9)*z[8] + n<T>(19,12)*z[2] - n<T>(13,3)*z[7] - z[94] + n<T>(1,3)*z[85]
    - z[91];
    z[85]=z[8]*z[85];
    z[91]=13*z[76] - n<T>(37,2)*z[5];
    z[87]=z[91]*z[87];
    z[91]= - n<T>(5,6)*z[6] + n<T>(1,3)*z[76] - z[5];
    z[91]=z[6]*z[91];
    z[87]=z[87] + z[91];
    z[91]=2*z[76];
    z[95]= - z[4] + z[91] + z[74];
    z[95]=z[4]*z[95];
    z[96]= - n<T>(1,4) + z[76];
    z[96]=13*z[96] + n<T>(19,2)*z[5];
    z[96]=n<T>(1,6)*z[96] - z[7];
    z[96]=z[7]*z[96];
    z[85]=n<T>(1,2)*z[85] + z[90] + z[96] + n<T>(1,4)*z[87] + z[95];
    z[85]=z[8]*z[85];
    z[87]=31*z[5];
    z[90]=157*z[7] + 73*z[4] + z[87] + n<T>(5,2)*z[6];
    z[90]= - n<T>(7,2)*z[8] + n<T>(1,2)*z[90] + 115*z[2];
    z[90]=n<T>(7,18)*z[9] + n<T>(1,9)*z[90] + z[84];
    z[90]= - n<T>(37,18)*z[13] + n<T>(1,2)*z[90] - n<T>(1,9)*z[11];
    z[90]=z[24]*z[90];
    z[95]= - 3*z[5] + z[7] - n<T>(11,2);
    z[96]=z[76]*z[95];
    z[96]= - 3*z[24] + z[96];
    z[96]=z[14]*z[96];
    z[95]= - 3*z[76] - z[95];
    z[95]=z[19]*z[95];
    z[90]=z[90] + z[96] + z[95] + z[63];
    z[89]= - z[89] + z[93];
    z[89]=z[6]*z[89];
    z[93]= - z[76] - n<T>(83,4)*z[5];
    z[93]= - z[94] + n<T>(1,6)*z[93] - z[6];
    z[93]=z[4]*z[93];
    z[89]=z[93] - n<T>(107,12)*z[92] + z[89];
    z[89]=z[89]*z[94];
    z[80]= - z[80]*z[87];
    z[92]=n<T>(5,3)*z[6];
    z[82]=z[82]*z[92];
    z[80]=z[80] + z[82];
    z[82]=n<T>(11,2)*z[76] - z[5];
    z[82]=n<T>(73,12)*z[7] + n<T>(11,3)*z[82] - n<T>(3,2)*z[6];
    z[73]=z[82]*z[73];
    z[82]=n<T>(15,4)*z[2] + n<T>(11,6)*z[7] - 21*z[76] - z[86] - z[4] + z[6];
    z[82]=z[2]*z[82];
    z[86]=53*z[76] + 57*z[5];
    z[86]= - n<T>(53,8)*z[4] + n<T>(1,4)*z[86] - z[6];
    z[86]=z[4]*z[86];
    z[73]=z[82] + z[73] + n<T>(1,4)*z[80] + z[86];
    z[80]= - 9*z[76] + n<T>(11,3)*z[5];
    z[80]=z[4] + n<T>(1,2)*z[80] + z[83];
    z[80]=n<T>(5,4)*z[8] - n<T>(17,12)*z[2] + n<T>(1,2)*z[80] - n<T>(8,3)*z[7];
    z[80]=z[8]*z[80];
    z[82]= - n<T>(121,3)*z[7] - 53*z[4] - z[87] + z[92];
    z[82]=n<T>(29,3)*z[8] + n<T>(1,4)*z[82] + 21*z[2];
    z[82]=z[9]*z[82];
    z[73]=n<T>(1,4)*z[82] + n<T>(1,2)*z[73] + z[80];
    z[73]=z[9]*z[73];
    z[80]=z[9] + z[8];
    z[82]= - z[76]*z[80];
    z[82]= - z[24] + z[82];
    z[82]=z[15]*z[82];
    z[80]=z[76] - z[80];
    z[80]=z[22]*z[80];
    z[80]=z[82] + z[80];
    z[74]= - z[84] + 5 - z[74];
    z[74]=z[38]*z[74];
    z[74]=z[28] + z[74] + z[66];
    z[82]= - 31*z[76] - 29*z[5];
    z[82]=z[81] + n<T>(1,4)*z[82] + 13*z[7];
    z[82]=n<T>(7,4)*z[13] + n<T>(29,6)*z[9] + n<T>(1,3)*z[82] + 5*z[8];
    z[82]=z[20]*z[82];
    z[83]=n<T>(7,4)*z[6] + z[3] + z[2] - z[5] + n<T>(3,4)*z[4];
    z[84]=z[76]*z[83];
    z[84]=n<T>(3,4)*z[24] + z[84];
    z[84]=z[10]*z[84];
    z[83]=n<T>(3,4)*z[76] - z[83];
    z[83]=z[18]*z[83];
    z[81]= - n<T>(1,2)*z[9] + z[76] - z[81];
    z[81]=z[21]*z[81];
    z[86]=z[52] + z[71] + z[68];
    z[87]= - z[45] + z[70] - z[49];
    z[92]= - z[36] + z[65] + z[61];
    z[93]= - z[69] - z[58] + z[51] - z[35];
    z[94]= - n<T>(497,12)*z[33] - n<T>(419,24)*z[31] + n<T>(11,4)*z[30] - z[27] + n<T>(5,72)*z[25];
    z[94]=i*z[94];
    z[95]=n<T>(5,6)*z[5] + n<T>(11,8) - z[76];
    z[95]=z[95]*npow(z[5],2);
    z[76]=z[8] - n<T>(83,12)*z[2] - n<T>(95,12)*z[4] - z[6] + n<T>(95,6)*z[76] - 
    z[5];
    z[76]=z[16]*z[76];
    z[91]= - z[2] + z[91] - z[5];
    z[91]=z[17]*z[91];
    z[96]=z[6] + z[8];
    z[96]=z[40]*z[96];

    r += n<T>(43,24)*z[26] - n<T>(67,144)*z[29] + n<T>(85,12)*z[32] + n<T>(229,12)*
      z[34] + n<T>(133,16)*z[37] - n<T>(19,4)*z[42] + n<T>(3,8)*z[43] - n<T>(107,48)*
      z[44] - n<T>(9,4)*z[46] - n<T>(31,16)*z[48] - n<T>(19,6)*z[50] - n<T>(53,16)*
      z[53] + n<T>(19,24)*z[56] - n<T>(43,16)*z[57] - n<T>(25,3)*z[59] + n<T>(3,4)*
      z[60] - n<T>(5,48)*z[62] - n<T>(3,16)*z[64] + 4*z[72] + z[73] + n<T>(1,6)*
      z[74] + n<T>(1,4)*z[75] + z[76] + z[77] + z[78] + n<T>(1,12)*z[79] + n<T>(31,12)*z[80]
     + n<T>(27,2)*z[81]
     + z[82]
     + z[83]
     + z[84]
     + z[85]
     + n<T>(1,16)
      *z[86] - n<T>(1,48)*z[87] + z[88] + z[89] + n<T>(1,2)*z[90] + n<T>(14,3)*
      z[91] - n<T>(3,2)*z[92] + n<T>(1,8)*z[93] + z[94] + z[95] + n<T>(7,12)*z[96];
 
    return r;
}

template std::complex<double> qg_2lha_tf862(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf862(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
