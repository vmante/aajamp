#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf816(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[107];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[9];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[4];
    z[19]=e[5];
    z[20]=e[7];
    z[21]=e[8];
    z[22]=e[9];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[18];
    z[28]=c[19];
    z[29]=c[21];
    z[30]=c[23];
    z[31]=c[25];
    z[32]=c[26];
    z[33]=c[28];
    z[34]=c[29];
    z[35]=c[30];
    z[36]=c[31];
    z[37]=e[3];
    z[38]=e[10];
    z[39]=e[6];
    z[40]=e[14];
    z[41]=f[0];
    z[42]=f[1];
    z[43]=f[3];
    z[44]=f[4];
    z[45]=f[5];
    z[46]=f[10];
    z[47]=f[11];
    z[48]=f[12];
    z[49]=f[13];
    z[50]=f[14];
    z[51]=f[16];
    z[52]=f[17];
    z[53]=f[18];
    z[54]=f[21];
    z[55]=f[30];
    z[56]=f[31];
    z[57]=f[32];
    z[58]=f[33];
    z[59]=f[34];
    z[60]=f[35];
    z[61]=f[36];
    z[62]=f[37];
    z[63]=f[39];
    z[64]=f[41];
    z[65]=f[43];
    z[66]=f[50];
    z[67]=f[51];
    z[68]=f[52];
    z[69]=f[55];
    z[70]=f[58];
    z[71]=n<T>(1,2)*z[5];
    z[72]=z[1]*i;
    z[73]=z[71] - z[72];
    z[73]=z[73]*z[5];
    z[74]=5*z[5];
    z[75]=n<T>(71,2)*z[72] - z[74];
    z[75]=n<T>(1,3)*z[75] - n<T>(27,4)*z[6];
    z[75]=z[6]*z[75];
    z[75]=n<T>(13,6)*z[73] + z[75];
    z[76]=z[7] - z[6];
    z[77]=7*z[72];
    z[76]= - n<T>(1,4)*z[2] + z[77] + z[71] - n<T>(5,2)*z[76];
    z[78]=n<T>(1,3)*z[2];
    z[76]=z[76]*z[78];
    z[79]=17*z[72];
    z[80]= - z[79] + z[74];
    z[80]=n<T>(7,6)*z[7] + n<T>(1,6)*z[80] + z[6];
    z[80]=z[7]*z[80];
    z[81]=23*z[6];
    z[82]=n<T>(29,3)*z[72] + z[81];
    z[82]= - n<T>(29,24)*z[9] + n<T>(1,4)*z[82] - n<T>(10,3)*z[7];
    z[82]=z[9]*z[82];
    z[83]=n<T>(1,2)*z[4];
    z[84]=z[83] - z[72];
    z[85]= - z[5] + z[84];
    z[86]=n<T>(5,4)*z[4];
    z[85]=z[85]*z[86];
    z[87]=13*z[5] - 71*z[6];
    z[87]= - 7*z[2] - n<T>(29,4)*z[9] + n<T>(1,4)*z[87] + 25*z[7];
    z[86]=n<T>(1,3)*z[87] + z[86];
    z[87]=n<T>(1,2)*z[8];
    z[86]=z[86]*z[87];
    z[75]=z[86] + z[85] + z[76] + z[82] + n<T>(1,2)*z[75] + z[80];
    z[75]=z[8]*z[75];
    z[76]= - n<T>(5,3) + n<T>(1,2)*z[72];
    z[80]=n<T>(1,2)*z[6];
    z[82]=n<T>(1,4)*z[7] - z[80] + z[71] - z[76];
    z[82]=z[7]*z[82];
    z[85]=n<T>(1,2)*z[7];
    z[86]=z[72] - z[5];
    z[88]=n<T>(1,12)*z[9] - z[85] + n<T>(1,3)*z[86] + z[80];
    z[88]=z[9]*z[88];
    z[89]=z[86] + z[80];
    z[89]=z[89]*z[6];
    z[90]= - z[5]*z[72];
    z[90]=z[90] + z[89];
    z[91]=n<T>(1,2)*z[2];
    z[92]= - z[2] + z[72] + z[5];
    z[92]=z[92]*z[91];
    z[93]=z[84]*z[83];
    z[76]= - z[91] + n<T>(1,3)*z[9] + z[76];
    z[94]=n<T>(1,2)*z[3];
    z[76]=z[76]*z[94];
    z[76]=z[76] + z[93] + z[92] + z[88] + n<T>(1,2)*z[90] + z[82];
    z[76]=z[3]*z[76];
    z[82]=n<T>(1,3)*z[73];
    z[88]=z[80] + z[72];
    z[90]=3*z[6];
    z[88]= - z[88]*z[90];
    z[92]=n<T>(1,2)*z[9];
    z[93]= - z[92] - z[86];
    z[93]=z[9]*z[93];
    z[95]=z[90] - z[84];
    z[95]=z[4]*z[95];
    z[88]=z[95] + z[93] - z[82] + z[88];
    z[93]=n<T>(1,4)*z[5];
    z[95]=z[72] - z[93];
    z[96]=z[4] + z[9];
    z[97]=n<T>(3,4)*z[6];
    z[98]=n<T>(1,3)*z[3];
    z[99]=n<T>(1,2)*z[10];
    z[95]=z[99] + z[98] + n<T>(1,3)*z[95] - z[97] - n<T>(1,4)*z[96];
    z[95]=z[10]*z[95];
    z[96]=z[94] + z[86];
    z[96]=z[96]*z[98];
    z[88]=z[95] + n<T>(1,2)*z[88] + z[96];
    z[88]=z[88]*z[99];
    z[95]=z[72] - z[4];
    z[96]=z[99] + z[95];
    z[96]=z[10]*z[96];
    z[98]=n<T>(3,2)*z[13];
    z[86]=z[6] + z[86];
    z[86]=z[86]*z[98];
    z[99]=npow(z[4],2);
    z[86]=z[86] + z[96] + n<T>(1,2)*z[99] + z[73] + z[89];
    z[86]=z[13]*z[86];
    z[89]=z[72]*z[4];
    z[89]=z[89] + z[24];
    z[96]= - z[13]*z[72];
    z[96]=z[96] - z[89];
    z[96]=z[11]*z[96];
    z[99]=z[13] - z[95];
    z[99]=z[23]*z[99];
    z[100]=z[10] + z[13];
    z[100]=z[40]*z[100];
    z[86]= - z[66] - z[62] + z[54] - z[53] + z[86] + z[96] + z[99] + 
    z[100];
    z[96]=n<T>(1,3)*z[7];
    z[99]=5*z[72];
    z[100]=n<T>(7,4)*z[2] - z[96] - n<T>(5,3)*z[6] - z[99] + z[71];
    z[100]=z[2]*z[100];
    z[101]=3*z[72];
    z[102]= - z[101] - z[5];
    z[102]=z[102]*z[71];
    z[103]= - n<T>(29,4)*z[6] + 8*z[72] - z[71];
    z[103]=z[6]*z[103];
    z[104]=n<T>(17,2)*z[5];
    z[105]=n<T>(13,4)*z[7] + n<T>(17,2)*z[6] + 13*z[72] - z[104];
    z[105]=z[105]*z[96];
    z[106]=npow(z[9],2);
    z[100]=z[100] - n<T>(3,4)*z[106] + z[105] + z[102] + n<T>(1,3)*z[103];
    z[100]=z[2]*z[100];
    z[80]=z[72] - z[80];
    z[80]=z[80]*z[90];
    z[80]= - z[82] + z[80];
    z[82]= - z[72] + z[92];
    z[82]=z[9]*z[82];
    z[90]= - 2*z[2] + n<T>(7,2)*z[72] + 4*z[5];
    z[78]=z[90]*z[78];
    z[90]=z[4] - n<T>(7,6)*z[2] + z[7] - z[97] - z[99] - n<T>(1,12)*z[5];
    z[83]=z[90]*z[83];
    z[90]=z[72]*z[7];
    z[78]=z[83] + z[78] + n<T>(3,2)*z[82] + n<T>(1,4)*z[80] - z[90];
    z[78]=z[4]*z[78];
    z[80]=z[85] + n<T>(17,3)*z[72] + z[5];
    z[80]=z[7]*z[80];
    z[77]=z[77] + z[93];
    z[77]=n<T>(11,6)*z[9] + n<T>(23,6)*z[7] + n<T>(1,3)*z[77] - n<T>(17,4)*z[6];
    z[77]=z[9]*z[77];
    z[82]= - n<T>(23,4)*z[6] - n<T>(21,2)*z[72] - z[5];
    z[82]=z[6]*z[82];
    z[73]=z[77] + z[80] + n<T>(1,6)*z[73] + z[82];
    z[73]=z[73]*z[92];
    z[77]= - z[91] - z[84];
    z[77]=z[16]*z[77];
    z[80]=z[94] - 5 + z[92];
    z[80]=z[37]*z[80];
    z[77]= - z[77] - z[80] + z[60] + z[27];
    z[80]=n<T>(13,3)*z[7] - z[81] + 11*z[72] + z[104];
    z[80]=z[80]*z[85];
    z[79]= - z[79] + n<T>(29,2)*z[5];
    z[71]=z[79]*z[71];
    z[79]=n<T>(13,2) - 23*z[5];
    z[79]=n<T>(1,2)*z[79] - 8*z[6];
    z[79]=z[6]*z[79];
    z[71]=z[80] + z[71] + z[79];
    z[71]=z[71]*z[96];
    z[79]=z[87] - z[72] + z[91];
    z[79]=z[21]*z[79];
    z[80]=z[9]*z[72];
    z[80]=z[80] + z[89];
    z[80]=z[12]*z[80];
    z[81]= - z[9] + z[95];
    z[81]=z[18]*z[81];
    z[80]= - z[41] + z[80] + z[81];
    z[81]= - z[8]*z[72];
    z[81]= - z[24] - z[90] + z[81];
    z[81]=z[15]*z[81];
    z[82]= - z[8] + z[72] - z[7];
    z[82]=z[22]*z[82];
    z[81]=z[81] + z[82];
    z[82]= - z[5] + 2*z[72];
    z[83]=z[5]*z[82];
    z[84]=n<T>(211,36)*z[6] + n<T>(19,6)*z[5] + n<T>(5,6) + z[101];
    z[84]=z[6]*z[84];
    z[83]= - n<T>(7,3)*z[83] + z[84];
    z[83]=z[6]*z[83];
    z[84]= - 19*z[72] + 37*z[5];
    z[84]=2*z[8] - 11*z[2] + 26*z[7] + n<T>(1,2)*z[84] + 40*z[6];
    z[84]=n<T>(1,3)*z[84] - z[98];
    z[84]=z[20]*z[84];
    z[74]=z[6] + z[74] + n<T>(11,2);
    z[72]=z[72]*z[74];
    z[72]=5*z[24] + z[72];
    z[72]=z[14]*z[72];
    z[74]=z[99] - z[74];
    z[74]=z[19]*z[74];
    z[85]=z[43] + z[48] + z[44];
    z[87]=z[50] - z[34];
    z[89]=z[65] + z[49];
    z[90]=z[68] + z[42];
    z[91]=z[69] - z[51];
    z[92]=z[70] + z[67];
    z[93]= - z[3] - z[10];
    z[93]=z[38]*z[93];
    z[93]=z[93] + z[46];
    z[94]=n<T>(23,2)*z[32] + n<T>(27,4)*z[30] - n<T>(7,2)*z[29] - n<T>(17,12)*z[25];
    z[94]=i*z[94];
    z[95]= - n<T>(8,3)*z[5] - n<T>(11,4) + z[101];
    z[95]=z[95]*npow(z[5],2);
    z[96]=n<T>(109,6)*z[9] - n<T>(25,3)*z[7] + 11*z[5] + 59*z[6];
    z[96]=z[13] - n<T>(23,12)*z[8] - n<T>(28,3)*z[4] + n<T>(1,4)*z[96] - 10*z[2];
    z[96]=n<T>(2,9)*z[10] - n<T>(1,4)*z[3] + n<T>(1,3)*z[96];
    z[96]=z[24]*z[96];
    z[82]=z[2] - z[82];
    z[82]=z[17]*z[82];
    z[97]=z[7] + z[9];
    z[97]=z[39]*z[97];

    r += n<T>(17,12)*z[26] + n<T>(7,8)*z[28] - n<T>(5,3)*z[31] - n<T>(7,2)*z[33]
     +  z[35] - n<T>(81,8)*z[36] - n<T>(5,6)*z[45] + n<T>(13,24)*z[47] + n<T>(5,8)*z[52]
       - n<T>(43,12)*z[55] - n<T>(103,24)*z[56] - n<T>(11,4)*z[57] - n<T>(32,3)*z[58]
       - z[59] + n<T>(29,24)*z[61] - n<T>(17,8)*z[63] + z[64] + z[71] + z[72]
       + z[73] + z[74] + z[75] + z[76] - n<T>(1,3)*z[77] + z[78] + n<T>(13,3)*
      z[79] + n<T>(3,2)*z[80] + n<T>(11,2)*z[81] + 2*z[82] + z[83] + z[84] - n<T>(1,24)*z[85]
     + n<T>(1,2)*z[86] - n<T>(1,4)*z[87]
     + z[88]
     + n<T>(4,3)*z[89]
     + n<T>(3,4)*z[90]
     + n<T>(1,8)*z[91] - n<T>(3,8)*z[92]
     + n<T>(1,6)*z[93]
     + z[94]
     +  z[95] + z[96] + n<T>(17,6)*z[97] + z[100];
 
    return r;
}

template std::complex<double> qg_2lha_tf816(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf816(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
