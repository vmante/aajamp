#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf487(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[26];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=c[12];
    z[5]=c[13];
    z[6]=c[20];
    z[7]=c[23];
    z[8]=f[44];
    z[9]=f[42];
    z[10]=f[77];
    z[11]=f[79];
    z[12]=f[81];
    z[13]=g[82];
    z[14]=g[90];
    z[15]=g[101];
    z[16]=g[131];
    z[17]=g[214];
    z[18]=g[245];
    z[19]=g[345];
    z[20]=g[347];
    z[21]=g[348];
    z[22]=z[20] + z[11] - z[15];
    z[23]= - z[17] + z[13] + z[16];
    z[24]=z[12] - z[19];
    z[22]= - n<T>(3,8)*z[9] - n<T>(5,8)*z[10] + n<T>(3,2)*z[14] + n<T>(9,4)*z[18]
     + 
    z[21] - n<T>(1,4)*z[24] - n<T>(3,4)*z[23] + n<T>(1,2)*z[22];
    z[23]=z[2] - z[3];
    z[24]= - n<T>(1,2)*z[23];
    z[25]=z[4] + n<T>(3,2)*z[8];
    z[24]=z[25]*z[24];
    z[22]=n<T>(1,2)*z[22] + z[4] + z[24];
    z[24]= - z[6] + n<T>(1,20)*z[7];
    z[24]= - n<T>(3,5)*z[5] + n<T>(1,4)*z[24];
    z[24]=3*z[24];
    z[25]=z[24] + n<T>(11,720)*z[1];
    z[23]= - z[25]*z[23];
    z[24]=z[24] - n<T>(107,2160)*z[1];
    z[23]=n<T>(1,4)*z[24] + z[23];
    z[23]=i*z[23];

    r += n<T>(1,2)*z[22] + z[23];
 
    return r;
}

template std::complex<double> qg_2lha_tf487(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf487(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
