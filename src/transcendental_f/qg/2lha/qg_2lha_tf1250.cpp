#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1250(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[25];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=e[1];
    z[6]=e[8];
    z[7]=c[3];
    z[8]=c[15];
    z[9]=c[18];
    z[10]=c[23];
    z[11]=c[25];
    z[12]=c[26];
    z[13]=c[28];
    z[14]=c[29];
    z[15]=c[30];
    z[16]=c[31];
    z[17]=f[11];
    z[18]=z[1]*z[3];
    z[19]=z[4]*z[1];
    z[18]=z[18] + z[19];
    z[20]=2*i;
    z[18]=z[18]*z[20];
    z[21]=z[5] + z[6];
    z[22]= - z[21] + n<T>(1,6)*z[7];
    z[23]=npow(z[4],2);
    z[24]= - z[1]*z[20];
    z[24]=z[2] - z[3] + z[24];
    z[24]=z[2]*z[24];
    z[18]=z[24] + z[18] - z[23] - z[22];
    z[18]=z[2]*z[18];
    z[23]=z[4]*z[3];
    z[22]=z[23] - z[22];
    z[22]=z[4]*z[22];
    z[21]= - z[1]*z[21];
    z[19]= - z[3]*z[19];
    z[19]=z[19] + z[21] + 2*z[12] + z[10];
    z[19]=z[19]*z[20];
    z[20]=z[13] + z[15];
    z[21]=z[9] - z[11];

    r += n<T>(1,3)*z[8] - z[14] + n<T>(37,6)*z[16] + z[17] + z[18] + z[19] - 4*
      z[20] + n<T>(4,3)*z[21] + z[22];
 
    return r;
}

template std::complex<double> qg_2lha_tf1250(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1250(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
