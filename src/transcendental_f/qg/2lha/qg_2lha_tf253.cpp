#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf253(
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[9];
  std::complex<T> r(0,0);

    z[1]=d[1];
    z[2]=d[9];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=e[10];
    z[6]=npow(z[1],2);
    z[7]=npow(z[3],2);
    z[6]=z[6] + z[7];
    z[7]= - z[2]*z[1];
    z[8]=z[2] - z[3];
    z[8]=z[4]*z[8];

    r += z[5] + n<T>(1,2)*z[6] + z[7] + z[8];
 
    return r;
}

template std::complex<double> qg_2lha_tf253(
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf253(
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
