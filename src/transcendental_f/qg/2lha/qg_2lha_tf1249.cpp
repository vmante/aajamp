#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1249(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[43];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[3];
    z[7]=d[4];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=e[0];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=e[10];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[18];
    z[18]=c[19];
    z[19]=c[25];
    z[20]=c[28];
    z[21]=c[29];
    z[22]=c[30];
    z[23]=c[31];
    z[24]=e[3];
    z[25]=f[2];
    z[26]=f[3];
    z[27]=f[4];
    z[28]=f[10];
    z[29]=f[11];
    z[30]=f[12];
    z[31]=f[13];
    z[32]=2*z[6];
    z[33]=z[1]*i;
    z[32]=z[32]*z[33];
    z[34]=npow(z[6],2);
    z[35]=z[32] - z[34];
    z[36]=z[33] - z[6];
    z[37]= - 2*z[5] - z[36];
    z[37]=z[5]*z[37];
    z[38]=z[5] + z[36];
    z[39]= - z[7]*z[38];
    z[37]=z[39] + z[37] - z[35];
    z[37]=z[7]*z[37];
    z[39]=2*z[33];
    z[40]= - z[39] - z[6] + n<T>(1,2)*z[2];
    z[40]=z[40]*z[2];
    z[32]=z[40] + z[32] + n<T>(1,2)*z[34];
    z[40]=z[2] - z[6];
    z[41]=z[3]*z[40];
    z[41]=z[41] + z[32];
    z[41]=z[3]*z[41];
    z[40]= - z[4]*z[40];
    z[32]=z[40] - z[32];
    z[32]=z[4]*z[32];
    z[39]=z[39] - z[2];
    z[40]= - z[3] + z[39];
    z[40]=z[10]*z[40];
    z[39]=z[4] - z[39];
    z[39]=z[12]*z[39];
    z[42]=z[5] + z[7];
    z[42]=z[24]*z[42];
    z[32]=z[29] - z[26] + z[25] - z[21] + z[39] + z[42] - z[31] - z[30]
    + z[37] + z[41] + z[32] + z[40];
    z[37]=z[6]*z[33];
    z[39]=z[33] - z[5];
    z[39]= - z[5]*z[39];
    z[34]=z[39] - z[34] + z[37];
    z[34]=z[5]*z[34];
    z[37]= - z[7] + n<T>(2,3)*z[8];
    z[38]= - z[38]*z[37];
    z[39]=z[36] - z[5];
    z[40]=z[5]*z[39];
    z[35]= - 2*z[35] + z[40];
    z[35]=n<T>(1,3)*z[35] + z[38];
    z[35]=z[8]*z[35];
    z[36]=n<T>(2,3)*z[5] - z[36] + z[37];
    z[36]=z[13]*z[36];
    z[37]=z[5] + z[6];
    z[33]=z[33]*z[37];
    z[33]=z[14] + z[33];
    z[33]=z[9]*z[33];
    z[37]=z[20] + z[28] - z[27];
    z[38]= - z[4] + z[3] - z[7] - 8*z[8];
    z[38]=z[14]*z[38];
    z[38]=z[38] - z[19];
    z[39]= - z[11]*z[39];
    z[40]=z[15]*i;

    r +=  - n<T>(8,9)*z[16] + n<T>(4,9)*z[17] - n<T>(1,18)*z[18] - n<T>(4,3)*z[22]
     + n<T>(38,9)*z[23]
     + n<T>(1,3)*z[32]
     + z[33]
     + z[34]
     + z[35]
     + z[36] - n<T>(2,3)*
      z[37] + n<T>(1,9)*z[38] + z[39] + n<T>(1,6)*z[40];
 
    return r;
}

template std::complex<double> qg_2lha_tf1249(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1249(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
