#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf992(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[125];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[17];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[21];
    z[37]=c[23];
    z[38]=c[25];
    z[39]=c[26];
    z[40]=c[28];
    z[41]=c[29];
    z[42]=c[30];
    z[43]=c[31];
    z[44]=e[3];
    z[45]=e[6];
    z[46]=e[14];
    z[47]=f[0];
    z[48]=f[1];
    z[49]=f[2];
    z[50]=f[3];
    z[51]=f[4];
    z[52]=f[5];
    z[53]=f[6];
    z[54]=f[7];
    z[55]=f[8];
    z[56]=f[9];
    z[57]=f[11];
    z[58]=f[12];
    z[59]=f[13];
    z[60]=f[14];
    z[61]=f[16];
    z[62]=f[17];
    z[63]=f[18];
    z[64]=f[21];
    z[65]=f[23];
    z[66]=f[30];
    z[67]=f[31];
    z[68]=f[32];
    z[69]=f[33];
    z[70]=f[34];
    z[71]=f[35];
    z[72]=f[36];
    z[73]=f[37];
    z[74]=f[39];
    z[75]=f[41];
    z[76]=f[43];
    z[77]=f[50];
    z[78]=f[51];
    z[79]=f[52];
    z[80]=f[53];
    z[81]=f[55];
    z[82]=f[56];
    z[83]=f[58];
    z[84]=f[60];
    z[85]=f[78];
    z[86]=f[100];
    z[87]=f[101];
    z[88]=z[6] - z[5];
    z[89]=5*z[8];
    z[90]=n<T>(7,4)*z[2];
    z[91]=z[1]*i;
    z[88]=z[90] - z[89] + n<T>(19,2)*z[91] + 8*z[88];
    z[88]=z[2]*z[88];
    z[92]=n<T>(1,2)*z[91];
    z[93]=n<T>(1,4)*z[9];
    z[94]=n<T>(1,2)*z[8];
    z[95]=z[93] - z[92] - z[94] + z[5];
    z[95]=z[9]*z[95];
    z[96]=n<T>(11,2)*z[5];
    z[97]= - 8*z[91] + z[96];
    z[98]=n<T>(1,3)*z[5];
    z[97]=z[97]*z[98];
    z[98]= - z[92] + z[98];
    z[98]=5*z[98] + n<T>(31,6)*z[8];
    z[98]=z[8]*z[98];
    z[99]=n<T>(19,3)*z[5];
    z[100]=z[9] - z[8];
    z[101]= - n<T>(29,3)*z[6] - z[99] - z[100];
    z[102]=n<T>(1,2)*z[6];
    z[101]=z[101]*z[102];
    z[103]=n<T>(1,2)*z[10];
    z[104]= - z[10] + 3*z[6] - z[5] - z[100];
    z[104]=z[104]*z[103];
    z[105]=n<T>(1,2)*z[4];
    z[106]=z[105] - z[91];
    z[105]=z[106]*z[105];
    z[107]=n<T>(5,2)*z[8];
    z[108]=17*z[91];
    z[109]=z[108] + 11*z[5];
    z[109]=z[9] + n<T>(1,3)*z[109] + z[107];
    z[109]=n<T>(79,36)*z[7] - z[103] + n<T>(23,12)*z[2] + n<T>(1,2)*z[109] - n<T>(17,3)*
    z[6];
    z[109]=z[7]*z[109];
    z[88]=z[109] + z[105] + z[104] + n<T>(1,3)*z[88] + z[101] + z[95] + 
    z[97] + z[98];
    z[88]=z[7]*z[88];
    z[95]=n<T>(1,2)*z[5];
    z[97]=z[95] - z[91];
    z[98]=n<T>(1,2)*z[9];
    z[101]= - z[102] - z[98] + z[94] + z[97];
    z[101]=z[101]*z[103];
    z[104]=7*z[5];
    z[105]=z[107] - 5*z[91] + z[104];
    z[109]=n<T>(1,8)*z[8];
    z[105]=z[105]*z[109];
    z[110]=z[91] - z[5];
    z[111]=n<T>(3,4)*z[6] - z[93] + z[110];
    z[111]=z[6]*z[111];
    z[112]=n<T>(7,4)*z[9];
    z[92]=n<T>(17,8)*z[2] - z[112] - n<T>(7,4)*z[8] + z[92] - 3*z[5];
    z[113]=n<T>(1,2)*z[2];
    z[92]=z[92]*z[113];
    z[114]=z[4]*z[106];
    z[115]=17*z[5];
    z[116]=55*z[91] - z[115];
    z[116]= - n<T>(47,3)*z[9] + n<T>(1,3)*z[116] - 9*z[8];
    z[116]= - n<T>(61,12)*z[3] + n<T>(33,4)*z[2] + n<T>(1,4)*z[116] - z[6];
    z[116]=z[3]*z[116];
    z[117]= - n<T>(1,4)*z[91] + z[5];
    z[117]=z[5]*z[117];
    z[118]=n<T>(89,6)*z[9] - n<T>(59,3)*z[110] + 7*z[8];
    z[118]=z[9]*z[118];
    z[92]=n<T>(1,4)*z[116] - n<T>(5,8)*z[114] + z[101] + z[92] + z[111] + n<T>(1,8)*
    z[118] + n<T>(5,3)*z[117] + z[105];
    z[92]=z[3]*z[92];
    z[101]=z[94] + z[6];
    z[105]=3*z[91];
    z[111]= - 2*z[2] + z[98] + z[105] - z[5] + z[101];
    z[111]=z[2]*z[111];
    z[114]=3*z[8];
    z[116]= - n<T>(19,2)*z[9] - z[114] - 9*z[91] + z[5];
    z[116]=z[116]*z[93];
    z[117]=z[91] + z[5];
    z[117]=n<T>(7,4)*z[6] + n<T>(13,2)*z[9] + n<T>(1,2)*z[117] - z[8];
    z[117]=z[117]*z[102];
    z[118]=n<T>(3,2)*z[2];
    z[119]=z[105] + z[5];
    z[119]=n<T>(1,2)*z[119] + z[8];
    z[119]= - n<T>(37,6)*z[10] - z[118] + n<T>(27,4)*z[6] + n<T>(1,2)*z[119] + z[9];
    z[103]=z[119]*z[103];
    z[119]=z[91] + n<T>(7,2)*z[5];
    z[119]=z[5]*z[119];
    z[120]=2*z[91];
    z[121]= - z[120] + z[8];
    z[121]=z[8]*z[121];
    z[103]=z[103] + z[111] + z[117] + z[116] + n<T>(1,4)*z[119] + z[121];
    z[103]=z[10]*z[103];
    z[111]=z[4] - z[2];
    z[116]=n<T>(17,2)*z[6];
    z[111]= - z[116] - z[112] - z[107] + n<T>(7,2)*z[91] - z[5] - n<T>(1,4)*
    z[111];
    z[111]=n<T>(31,12)*z[11] + n<T>(11,8)*z[3] + n<T>(1,2)*z[111] + 3*z[7];
    z[111]=z[11]*z[111];
    z[97]=z[97]*z[5];
    z[117]=z[91] - z[94];
    z[107]=z[117]*z[107];
    z[117]= - z[98] - z[110];
    z[112]=z[117]*z[112];
    z[117]=z[91] - z[8];
    z[119]= - z[102] - z[117];
    z[116]=z[119]*z[116];
    z[119]=z[91]*z[4];
    z[107]=z[111] + n<T>(1,4)*z[119] + z[116] + z[112] - z[97] + z[107];
    z[111]=n<T>(1,2)*z[3];
    z[112]=z[111] + z[110];
    z[112]=z[3]*z[112];
    z[116]=3*z[117] + n<T>(1,4)*z[7];
    z[116]=z[7]*z[116];
    z[107]=n<T>(3,8)*z[112] + z[116] + n<T>(1,2)*z[107];
    z[107]=z[11]*z[107];
    z[112]=z[91]*z[5];
    z[116]=n<T>(33,2)*z[91] + 25*z[112];
    z[121]=n<T>(13,2)*z[6];
    z[122]=z[91]*z[121];
    z[123]=z[9]*z[105];
    z[116]=n<T>(25,2)*z[30] + z[122] + n<T>(1,2)*z[116] + z[123];
    z[116]=z[15]*z[116];
    z[122]=13*z[91];
    z[123]= - n<T>(33,4) + z[122];
    z[99]=n<T>(1,2)*z[123] - z[99];
    z[99]=z[99]*npow(z[5],2);
    z[123]= - 11*z[91] + n<T>(251,12)*z[5];
    z[123]=n<T>(1,2)*z[123] - z[8];
    z[123]=z[8]*z[123];
    z[123]=n<T>(251,12)*z[97] + z[123];
    z[123]=z[123]*z[94];
    z[124]= - n<T>(33,2) + 25*z[110];
    z[121]= - z[121] + n<T>(1,2)*z[124] - 3*z[9];
    z[121]=z[22]*z[121];
    z[99]= - z[121] - z[116] - z[99] - z[123] + z[82] - z[75];
    z[116]=n<T>(131,2)*z[8];
    z[121]=z[116] - 131*z[91] - 139*z[5];
    z[121]=z[8]*z[121];
    z[97]=71*z[97] + z[121];
    z[121]=n<T>(5,8)*z[9] - n<T>(5,4)*z[91] + z[8];
    z[121]=z[9]*z[121];
    z[97]=n<T>(1,8)*z[97] + z[121];
    z[115]=n<T>(101,2)*z[91] + z[115];
    z[100]=n<T>(1,2)*z[115] - z[100];
    z[100]= - n<T>(19,8)*z[2] + n<T>(1,2)*z[100] - z[6];
    z[100]=z[2]*z[100];
    z[101]=z[118] + n<T>(9,4)*z[9] - z[5] - z[101];
    z[101]=z[10]*z[101];
    z[115]= - 101*z[2] + 15*z[6] + 5*z[9] + z[116] + z[122] + n<T>(71,2)*
    z[5];
    z[115]= - n<T>(31,6)*z[4] + n<T>(1,2)*z[115] - 5*z[10];
    z[115]=z[4]*z[115];
    z[116]=n<T>(31,16)*z[6] - n<T>(23,8)*z[9] - n<T>(15,8)*z[91] + z[5];
    z[116]=z[6]*z[116];
    z[97]=n<T>(1,8)*z[115] + z[101] + z[100] + n<T>(1,2)*z[97] + z[116];
    z[97]=z[4]*z[97];
    z[96]= - z[108] + z[96];
    z[96]=z[5]*z[96];
    z[100]=5*z[5];
    z[101]= - n<T>(109,8)*z[8] + n<T>(109,4)*z[91] - z[100];
    z[101]=z[8]*z[101];
    z[96]=n<T>(5,4)*z[96] + z[101];
    z[101]=z[5] + n<T>(17,2)*z[91];
    z[101]=n<T>(85,8)*z[9] + n<T>(1,2)*z[101] + z[8];
    z[98]=z[101]*z[98];
    z[101]=11 + z[91];
    z[101]= - n<T>(5,2)*z[9] - 19*z[8] + n<T>(3,2)*z[101] + n<T>(83,3)*z[5];
    z[101]=n<T>(1,4)*z[101] + n<T>(31,9)*z[6];
    z[101]=z[101]*z[102];
    z[96]=z[101] + n<T>(1,3)*z[96] + z[98];
    z[96]=z[6]*z[96];
    z[98]= - 9*z[9] + z[114] - z[105] + z[104];
    z[93]=z[98]*z[93];
    z[98]= - n<T>(161,2)*z[8] + 161*z[91] + n<T>(83,2)*z[5];
    z[94]=z[98]*z[94];
    z[98]=n<T>(127,4)*z[91] - 29*z[5];
    z[98]=z[5]*z[98];
    z[94]=z[98] + z[94];
    z[93]=n<T>(1,3)*z[94] + z[93];
    z[94]=23*z[91] - z[95];
    z[89]= - n<T>(77,8)*z[6] + n<T>(1,2)*z[94] + z[89];
    z[89]=z[6]*z[89];
    z[94]= - n<T>(95,16)*z[8] - n<T>(493,8)*z[91] - z[100];
    z[94]=n<T>(425,48)*z[2] - n<T>(53,24)*z[6] + n<T>(1,3)*z[94] - n<T>(3,16)*z[9];
    z[94]=z[2]*z[94];
    z[89]=z[94] + n<T>(1,2)*z[93] + n<T>(1,3)*z[89];
    z[89]=z[2]*z[89];
    z[93]=7*z[9];
    z[94]=z[91]*z[93];
    z[95]=z[91]*z[8];
    z[95]=z[95] + z[30];
    z[94]=z[94] + n<T>(17,2)*z[95];
    z[94]=z[17]*z[94];
    z[93]=n<T>(17,2)*z[117] - z[93];
    z[93]=z[27]*z[93];
    z[93]=z[87] - z[86] - z[73] + z[58] - z[56] + z[94] + z[93];
    z[94]=z[109] - z[120] - n<T>(7,8)*z[5];
    z[94]=z[8]*z[94];
    z[98]=z[108] - 13*z[5];
    z[98]=z[5]*z[98];
    z[101]= - n<T>(59,16)*z[9] + n<T>(5,3)*z[110] + n<T>(13,16)*z[8];
    z[101]=z[9]*z[101];
    z[94]=z[101] + n<T>(1,6)*z[98] + z[94];
    z[94]=z[9]*z[94];
    z[98]=z[119] + z[30];
    z[101]= - z[11]*z[91];
    z[101]=z[101] - z[98];
    z[101]=z[14]*z[101];
    z[102]=z[91] - z[4];
    z[104]=z[11] - z[102];
    z[104]=z[21]*z[104];
    z[101]=z[101] + z[104] + z[79] + z[47];
    z[104]= - z[7]*z[91];
    z[95]=z[104] - z[95];
    z[95]=z[16]*z[95];
    z[104]= - z[7] + z[117];
    z[104]=z[25]*z[104];
    z[95]=z[95] + z[104];
    z[104]=z[10]*z[91];
    z[98]=z[104] + z[98];
    z[98]=z[13]*z[98];
    z[102]= - z[10] + z[102];
    z[102]=z[28]*z[102];
    z[98]=z[98] + z[102];
    z[102]= - z[3]*z[91];
    z[102]= - z[30] - z[112] + z[102];
    z[102]=z[12]*z[102];
    z[104]= - z[3] + z[110];
    z[104]=z[20]*z[104];
    z[102]=z[102] + z[104];
    z[104]= - 3*z[110] - 11*z[9];
    z[104]=z[111] - 2*z[7] - n<T>(19,2)*z[10] + n<T>(1,2)*z[104] - 10*z[6];
    z[104]=z[29]*z[104];
    z[105]=z[110] - z[8];
    z[90]= - n<T>(47,12)*z[3] + z[90] + n<T>(7,4)*z[105] - n<T>(17,3)*z[9];
    z[90]=z[26]*z[90];
    z[106]=z[113] + z[106];
    z[106]=z[18]*z[106];
    z[108]=z[68] + z[66];
    z[109]=z[72] - z[64];
    z[110]=z[81] - z[60];
    z[111]=z[85] - z[42];
    z[112]=n<T>(1151,24)*z[39] + n<T>(1049,48)*z[37] - n<T>(21,8)*z[36] - n<T>(3,2)*
    z[33] - n<T>(293,144)*z[31];
    z[112]=i*z[112];
    z[100]=z[100] - n<T>(1153,9)*z[8];
    z[100]=n<T>(1,2)*z[100] + n<T>(215,9)*z[9];
    z[100]=n<T>(37,32)*z[11] - n<T>(19,48)*z[3] - n<T>(7,18)*z[7] + n<T>(5,32)*z[4]
     + n<T>(47,48)*z[10] - n<T>(47,9)*z[2]
     + n<T>(1,16)*z[100]
     + n<T>(13,3)*z[6];
    z[100]=z[30]*z[100];
    z[105]=z[105] + z[2];
    z[105]=37*z[7] + 50*z[6] - 13*z[105];
    z[105]=z[23]*z[105];
    z[91]= - z[10] + n<T>(203,24)*z[2] + z[9] + n<T>(227,24)*z[8] - n<T>(227,12)*
    z[91] + z[5];
    z[91]=z[24]*z[91];
    z[113]=z[2] - z[120] + z[5];
    z[113]=z[19]*z[113];
    z[114]= - z[3] - z[11];
    z[114]=z[44]*z[114];
    z[115]=z[7] + z[11];
    z[115]=z[45]*z[115];
    z[116]= - z[9] - z[10];
    z[116]=z[46]*z[116];

    r += n<T>(19,48)*z[32] - n<T>(11,12)*z[34] + n<T>(637,288)*z[35] - n<T>(211,24)*
      z[38] - n<T>(511,24)*z[40] + n<T>(11,16)*z[41] - n<T>(431,32)*z[43] - n<T>(1,16)*
      z[48] - n<T>(3,8)*z[49] + n<T>(71,32)*z[50] - n<T>(49,24)*z[51] + n<T>(95,24)*
      z[52] - n<T>(23,16)*z[53] - 7*z[54] + 4*z[55] + n<T>(251,96)*z[57] + n<T>(13,4)*z[59]
     - n<T>(7,16)*z[61]
     + n<T>(131,32)*z[62]
     + n<T>(1,24)*z[63]
     + n<T>(7,8)*
      z[65] - n<T>(173,24)*z[67] - n<T>(52,3)*z[69] - n<T>(9,8)*z[70] - n<T>(8,3)*z[71]
       - n<T>(17,8)*z[74] + n<T>(13,6)*z[76] + n<T>(7,4)*z[77] + n<T>(15,16)*z[78]
     + 12
      *z[80] + n<T>(87,16)*z[83] + n<T>(3,2)*z[84] + z[88] + z[89] + z[90] + 
      z[91] + z[92] + n<T>(1,4)*z[93] + z[94] + n<T>(47,6)*z[95] + z[96] + 
      z[97] + n<T>(5,4)*z[98] - n<T>(1,2)*z[99] + z[100] + n<T>(1,8)*z[101] + n<T>(55,24)*z[102]
     + z[103]
     + z[104]
     + n<T>(1,3)*z[105]
     + n<T>(63,4)*z[106]
     +  z[107] - 3*z[108] + n<T>(5,8)*z[109] - n<T>(5,16)*z[110] - n<T>(3,4)*z[111]
       + z[112] + n<T>(16,3)*z[113] + z[114] + n<T>(5,2)*z[115] + n<T>(25,4)*z[116]
      ;
 
    return r;
}

template std::complex<double> qg_2lha_tf992(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf992(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
