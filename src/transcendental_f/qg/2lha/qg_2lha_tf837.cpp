#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf837(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[32];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[7];
    z[6]=d[17];
    z[7]=e[6];
    z[8]=e[9];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[31];
    z[13]=e[7];
    z[14]=f[30];
    z[15]=f[31];
    z[16]=f[33];
    z[17]=f[35];
    z[18]=f[36];
    z[19]=f[38];
    z[20]=f[39];
    z[21]=3*z[3];
    z[22]=n<T>(5,3)*z[4];
    z[23]=z[21] - z[22];
    z[24]= - n<T>(7,6)*z[5] + z[23];
    z[24]=z[5]*z[24];
    z[25]=npow(z[4],2);
    z[26]=npow(z[3],2);
    z[27]=2*z[3];
    z[28]=n<T>(11,6)*z[2] - n<T>(2,3)*z[5] + n<T>(5,6)*z[4] - z[27];
    z[28]=z[2]*z[28];
    z[24]=z[28] + z[24] - z[25] - z[26];
    z[24]=z[2]*z[24];
    z[23]=n<T>(2,3)*z[2] + n<T>(7,3)*z[5] - z[23];
    z[23]=z[2]*z[23];
    z[26]=z[3] + z[4];
    z[28]=z[26]*z[3];
    z[29]= - z[22] + z[3];
    z[29]=z[5]*z[29];
    z[23]=z[23] + z[29] + n<T>(1,3)*z[25] + z[28];
    z[29]=z[1]*i;
    z[23]=z[23]*z[29];
    z[30]=z[5] + z[4];
    z[31]= - z[30]*z[29];
    z[31]=z[31] - z[9];
    z[31]=z[6]*z[31];
    z[30]=z[29] - z[30];
    z[30]=z[8]*z[30];
    z[30]=z[11] + z[31] + z[30];
    z[31]=n<T>(1,2)*z[3];
    z[22]=z[22] - z[31];
    z[22]=z[5]*z[22];
    z[22]=z[22] + n<T>(4,3)*z[25] - z[28];
    z[22]=z[5]*z[22];
    z[28]=npow(z[4],3);
    z[27]=n<T>(25,6)*z[2] - n<T>(1,6)*z[5] + n<T>(1,2)*z[4] + z[27];
    z[27]=z[9]*z[27];
    z[27]=z[28] + z[27];
    z[21]=z[4] + z[21];
    z[21]=z[21]*z[31];
    z[21]= - z[25] + z[21];
    z[21]=z[3]*z[21];
    z[25]=z[2] + z[4];
    z[25]= - z[29] + z[5] - z[3] + n<T>(11,3)*z[25];
    z[25]=z[7]*z[25];
    z[28]=z[19] - z[16];
    z[29]=z[20] + z[12];
    z[26]=z[13]*z[26];
    z[31]=z[10]*i;

    r +=  - n<T>(2,3)*z[14] - n<T>(1,2)*z[15] + z[17] + n<T>(7,6)*z[18] + z[21] + 
      z[22] + z[23] + z[24] + z[25] + 3*z[26] + n<T>(1,3)*z[27] + 2*z[28]
       - n<T>(3,2)*z[29] + n<T>(5,3)*z[30] - n<T>(2,9)*z[31];
 
    return r;
}

template std::complex<double> qg_2lha_tf837(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf837(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
