#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf353(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[37];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[15];
    z[11]=c[18];
    z[12]=c[19];
    z[13]=c[23];
    z[14]=c[25];
    z[15]=c[26];
    z[16]=c[28];
    z[17]=c[29];
    z[18]=c[31];
    z[19]=f[0];
    z[20]=f[3];
    z[21]=f[5];
    z[22]=f[11];
    z[23]=f[13];
    z[24]=f[17];
    z[25]=f[20];
    z[26]=npow(z[2],2);
    z[27]=n<T>(2,3)*z[2];
    z[28]= - z[3]*z[27];
    z[29]= - n<T>(4,3)*z[2] - n<T>(3,2)*z[3];
    z[29]=z[5]*z[29];
    z[30]=n<T>(17,6)*z[5] - 5*z[2] + n<T>(13,6)*z[3];
    z[30]=z[4]*z[30];
    z[28]=z[30] + z[29] + n<T>(7,2)*z[26] + z[28];
    z[29]=z[1]*i;
    z[28]=z[28]*z[29];
    z[30]=n<T>(3,4)*z[3];
    z[31]=z[27] + z[30];
    z[31]=z[5]*z[31];
    z[30]= - z[2] + z[30];
    z[30]=z[3]*z[30];
    z[30]=z[31] + n<T>(5,6)*z[26] + z[30];
    z[30]=z[5]*z[30];
    z[31]=n<T>(1,3)*z[3];
    z[32]=11*z[2] - n<T>(13,4)*z[3];
    z[32]=z[32]*z[31];
    z[27]= - n<T>(17,12)*z[5] - z[27] - n<T>(1,2)*z[3];
    z[27]=z[5]*z[27];
    z[33]=n<T>(7,12)*z[5] + z[2] - n<T>(19,12)*z[3];
    z[33]=z[4]*z[33];
    z[27]=z[33] + z[32] + z[27];
    z[27]=z[4]*z[27];
    z[32]=z[3]*z[2];
    z[26]= - 4*z[26] + z[32];
    z[26]=z[26]*z[31];
    z[31]=z[2] + z[3];
    z[31]= - n<T>(16,3)*z[29] - z[4] + n<T>(8,3)*z[31] + z[5];
    z[31]=z[6]*z[31];
    z[32]=z[24] + z[17];
    z[33]=2*z[29] - z[2] - z[5];
    z[33]=z[8]*z[33];
    z[33]=z[33] - z[21];
    z[34]=z[5] - z[3];
    z[34]= - n<T>(1,2)*z[4] + z[2] + n<T>(5,6)*z[34];
    z[34]=z[9]*z[34];
    z[34]=z[34] - z[12];
    z[35]= - z[15] - n<T>(7,2)*z[13];
    z[35]=i*z[35];
    z[36]=npow(z[2],3);
    z[29]=5*z[29] - z[4] - z[2] - 3*z[3];
    z[29]=z[7]*z[29];

    r +=  - n<T>(1,4)*z[10] - z[11] + z[14] - z[16] - n<T>(13,8)*z[18] + 3*
      z[19] - n<T>(13,12)*z[20] - n<T>(17,12)*z[22] - n<T>(1,3)*z[23] - z[25] + 
      z[26] + z[27] + z[28] + z[29] + z[30] + z[31] + n<T>(3,4)*z[32] + n<T>(2,3)*z[33]
     + n<T>(1,6)*z[34]
     + z[35] - z[36];
 
    return r;
}

template std::complex<double> qg_2lha_tf353(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf353(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
