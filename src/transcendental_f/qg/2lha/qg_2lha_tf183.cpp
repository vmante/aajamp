#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf183(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[113];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[3];
    z[22]=e[4];
    z[23]=e[5];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[10];
    z[28]=e[11];
    z[29]=e[12];
    z[30]=e[13];
    z[31]=c[3];
    z[32]=c[11];
    z[33]=c[15];
    z[34]=c[19];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[31];
    z[40]=e[6];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[7];
    z[50]=f[10];
    z[51]=f[11];
    z[52]=f[12];
    z[53]=f[14];
    z[54]=f[15];
    z[55]=f[16];
    z[56]=f[17];
    z[57]=f[18];
    z[58]=f[23];
    z[59]=f[30];
    z[60]=f[31];
    z[61]=f[32];
    z[62]=f[33];
    z[63]=f[35];
    z[64]=f[36];
    z[65]=f[37];
    z[66]=f[39];
    z[67]=f[43];
    z[68]=f[50];
    z[69]=f[51];
    z[70]=f[52];
    z[71]=f[53];
    z[72]=f[55];
    z[73]=f[56];
    z[74]=f[58];
    z[75]=f[74];
    z[76]=f[75];
    z[77]=z[9] + z[5];
    z[78]=z[1]*i;
    z[79]=z[78] + z[3];
    z[80]=n<T>(1,2)*z[6];
    z[81]=n<T>(1,2)*z[8];
    z[77]= - z[81] + z[80] - z[79] + n<T>(3,2)*z[77];
    z[77]= - n<T>(17,12)*z[11] + n<T>(1,4)*z[77] + z[7];
    z[77]=z[11]*z[77];
    z[82]=n<T>(11,6)*z[6];
    z[83]=5*z[5];
    z[84]=z[82] - 5*z[3] - n<T>(19,3)*z[78] + z[83];
    z[84]=z[6]*z[84];
    z[85]=3*z[3];
    z[86]=n<T>(7,3)*z[78];
    z[87]= - n<T>(25,6)*z[9] + 5*z[6] + z[85] - z[86] - z[5];
    z[87]=z[9]*z[87];
    z[84]=z[84] + z[87];
    z[87]=n<T>(1,2)*z[5];
    z[88]=z[87] - z[78];
    z[88]=z[88]*z[5];
    z[89]=n<T>(1,4)*z[5];
    z[90]=z[89] - z[79];
    z[90]=z[3]*z[90];
    z[79]= - z[81] + z[79];
    z[91]=n<T>(1,4)*z[8];
    z[79]=z[79]*z[91];
    z[92]=2*z[5];
    z[93]=n<T>(5,2)*z[78];
    z[94]=z[7] + z[3] + z[93] - z[92];
    z[94]=z[7]*z[94];
    z[95]=n<T>(1,2)*z[10];
    z[96]=z[7] - z[6];
    z[97]= - z[9] + z[3] - z[96];
    z[97]=z[97]*z[95];
    z[98]=n<T>(1,2)*z[7];
    z[99]=n<T>(1,2)*z[3];
    z[100]=z[98] - z[99];
    z[101]=n<T>(1,3)*z[6];
    z[102]= - n<T>(7,12)*z[4] + n<T>(5,6)*z[9] + z[101] - z[100];
    z[102]=z[4]*z[102];
    z[77]=z[77] + z[102] + z[97] + z[94] + z[79] + n<T>(3,4)*z[88] + z[90]
    + n<T>(1,4)*z[84];
    z[77]=z[11]*z[77];
    z[79]=2*z[78];
    z[84]=n<T>(1,4)*z[3];
    z[90]= - z[84] - z[79] - z[87];
    z[94]=n<T>(1,3)*z[2];
    z[90]=z[94] + n<T>(1,12)*z[9] - n<T>(1,6)*z[8] + n<T>(1,3)*z[90] + n<T>(5,8)*z[96];
    z[90]=z[2]*z[90];
    z[96]=z[79] - z[5];
    z[96]=z[5]*z[96];
    z[97]=z[78] - z[5];
    z[102]=n<T>(1,2)*z[97];
    z[103]= - z[102] - z[3];
    z[103]=z[3]*z[103];
    z[96]=z[96] + z[103];
    z[103]=z[99] + z[79] + z[5];
    z[104]=n<T>(1,3)*z[8];
    z[103]= - z[104] + n<T>(1,3)*z[103] + n<T>(5,4)*z[6];
    z[103]=z[8]*z[103];
    z[105]=z[78] - z[8];
    z[106]=z[3] - z[5];
    z[107]=z[9] + z[106] + z[105];
    z[107]=z[9]*z[107];
    z[102]= - z[102] - z[6];
    z[102]=z[6]*z[102];
    z[108]=z[97] - z[8];
    z[109]=z[7] + z[6] + z[108];
    z[109]=z[7]*z[109];
    z[110]=npow(z[10],2);
    z[111]=z[78] - n<T>(1,2)*z[4];
    z[111]=z[4]*z[111];
    z[90]=z[90] + z[111] + n<T>(1,2)*z[110] + n<T>(5,4)*z[109] + n<T>(1,6)*z[107]
     + 
    z[103] + n<T>(1,3)*z[96] + n<T>(5,2)*z[102];
    z[90]=z[2]*z[90];
    z[96]=n<T>(37,8)*z[6];
    z[102]=n<T>(15,8)*z[9];
    z[103]=z[86] + n<T>(9,2)*z[5];
    z[85]=n<T>(49,12)*z[10] + n<T>(35,12)*z[7] + z[102] - n<T>(41,24)*z[8] - z[96]
    + n<T>(1,4)*z[103] - z[85];
    z[85]=z[10]*z[85];
    z[103]=z[99] + z[78];
    z[107]=z[5] + z[103];
    z[107]=z[3]*z[107];
    z[107]=n<T>(9,2)*z[88] + z[107];
    z[96]= - z[96] - n<T>(29,4)*z[78] + z[106];
    z[96]=z[6]*z[96];
    z[109]= - z[98] - n<T>(35,6)*z[8] + n<T>(23,6)*z[78] - z[106];
    z[109]=z[7]*z[109];
    z[110]= - n<T>(41,6)*z[8] + n<T>(41,3)*z[78] + 37*z[6];
    z[110]=z[110]*z[91];
    z[111]=11*z[5];
    z[112]=7*z[78] - z[111];
    z[102]=z[102] + n<T>(1,4)*z[112] - z[3];
    z[102]=z[9]*z[102];
    z[85]=z[85] + z[109] + z[102] + z[110] + n<T>(1,2)*z[107] + z[96];
    z[85]=z[85]*z[95];
    z[96]=z[103]*z[3];
    z[102]=z[81] - z[78] - z[5];
    z[102]=z[8]*z[102];
    z[103]=z[3] + n<T>(9,2)*z[6];
    z[107]=n<T>(17,12)*z[9] - n<T>(17,6)*z[78] - z[103];
    z[107]=z[9]*z[107];
    z[109]= - z[98] + z[78] + z[6];
    z[109]=z[7]*z[109];
    z[110]= - 11*z[78] + n<T>(23,2)*z[6];
    z[110]=z[6]*z[110];
    z[102]=z[109] + z[107] + z[102] + n<T>(1,6)*z[110] + z[88] + z[96];
    z[107]=n<T>(1,2)*z[9];
    z[95]= - z[95] + z[107] - z[80] + z[78] + z[100];
    z[95]=z[10]*z[95];
    z[100]=z[78] - z[4];
    z[82]=n<T>(17,6)*z[9] + z[82] - z[106] - z[7] + z[8] + n<T>(1,3)*z[100];
    z[82]=z[4]*z[82];
    z[82]=n<T>(1,4)*z[82] + n<T>(1,2)*z[102] + z[95];
    z[82]=z[4]*z[82];
    z[95]=n<T>(13,3)*z[78] - z[83];
    z[95]=z[95]*z[87];
    z[86]=n<T>(65,18)*z[3] - z[86] - z[89];
    z[86]=z[3]*z[86];
    z[86]=z[95] + z[86];
    z[86]=z[3]*z[86];
    z[95]=z[78]*z[8];
    z[95]=z[95] + z[31];
    z[102]= - z[9]*z[78];
    z[102]=z[102] - z[95];
    z[102]=z[17]*z[102];
    z[109]=25*z[78] - 23*z[5];
    z[109]=z[109]*npow(z[5],2);
    z[110]=z[108] + z[2];
    z[110]=n<T>(133,6)*z[7] + n<T>(163,6)*z[6] - 5*z[110];
    z[110]=z[24]*z[110];
    z[112]=z[9] - z[105];
    z[112]=z[28]*z[112];
    z[86]= - z[43] + z[102] + z[110] + n<T>(1,6)*z[109] + z[86] + z[112] + 
    z[44];
    z[89]=z[80] - z[84] + z[79] + z[89];
    z[89]=z[6]*z[89];
    z[87]=z[91] + z[87] - z[99] + z[78];
    z[87]=z[87]*z[104];
    z[91]=n<T>(11,4)*z[88] + z[96];
    z[96]= - n<T>(67,3)*z[78] + z[111];
    z[96]=n<T>(1,2)*z[96] - 13*z[3];
    z[96]=n<T>(1,2)*z[96] + 7*z[6];
    z[96]= - n<T>(47,18)*z[9] + n<T>(1,2)*z[96] - z[104];
    z[96]=z[96]*z[107];
    z[87]=z[96] + z[87] + n<T>(1,2)*z[91] + z[89];
    z[87]=z[9]*z[87];
    z[89]=n<T>(31,8)*z[6] + z[99] + n<T>(85,48)*z[78] + z[92];
    z[89]=z[6]*z[89];
    z[91]=n<T>(1,3)*z[5];
    z[92]= - 3*z[78] - z[91];
    z[92]=z[5]*z[92];
    z[96]=z[97] + z[99];
    z[99]= - z[96]*z[84];
    z[89]=z[89] + z[92] + z[99];
    z[89]=z[6]*z[89];
    z[84]= - z[84] + z[93] - z[5];
    z[84]=z[3]*z[84];
    z[84]=z[88] + z[84];
    z[92]= - n<T>(107,12)*z[6] + n<T>(167,6)*z[78] - z[83];
    z[80]=z[92]*z[80];
    z[91]= - n<T>(167,12)*z[6] - n<T>(5,6)*z[3] + z[91] - z[105];
    z[91]=z[91]*z[81];
    z[80]=z[91] + n<T>(1,3)*z[84] + z[80];
    z[80]=z[80]*z[81];
    z[81]=z[3]*z[96];
    z[84]= - n<T>(19,6)*z[5] - z[103];
    z[84]=z[6]*z[84];
    z[81]=z[84] + n<T>(43,6)*z[88] + z[81];
    z[83]= - n<T>(37,3)*z[78] + z[83];
    z[83]=n<T>(1,2)*z[83] - z[101];
    z[83]=n<T>(1,2)*z[83] + n<T>(13,3)*z[8];
    z[83]=z[8]*z[83];
    z[84]=5*z[78] + n<T>(43,6)*z[5];
    z[84]=n<T>(7,4)*z[7] + n<T>(37,12)*z[8] - n<T>(127,12)*z[6] + n<T>(1,2)*z[84]
     + z[3]
   ;
    z[84]=z[84]*z[98];
    z[81]=z[84] + n<T>(1,2)*z[81] + z[83];
    z[81]=z[7]*z[81];
    z[83]=n<T>(101,2)*z[5] + 23*z[3];
    z[83]=n<T>(5,4)*z[10] - n<T>(37,2)*z[7] - 17*z[9] - n<T>(89,16)*z[8] + n<T>(1,4)*
    z[83] + 35*z[6];
    z[83]=n<T>(4,3)*z[2] - n<T>(35,48)*z[11] + n<T>(1,3)*z[83] - n<T>(1,8)*z[4];
    z[83]=z[31]*z[83];
    z[79]=z[79] - z[2];
    z[84]=z[5] - z[79];
    z[84]=z[19]*z[84];
    z[88]=z[8] - z[79];
    z[88]=z[25]*z[88];
    z[83]=z[83] + z[84] + z[88] + z[57] + z[47] - z[36];
    z[84]= - z[7]*z[78];
    z[84]=z[84] - z[95];
    z[84]=z[16]*z[84];
    z[88]= - z[7] + z[105];
    z[88]=z[26]*z[88];
    z[84]=z[84] + z[88];
    z[88]=z[78]*z[5];
    z[88]=z[88] + z[31];
    z[91]=z[6]*z[78];
    z[91]=z[91] + z[88];
    z[91]=z[15]*z[91];
    z[92]= - z[6] + z[97];
    z[92]=z[23]*z[92];
    z[91]=z[91] + z[92];
    z[92]=z[78]*z[4];
    z[92]=z[92] + z[31];
    z[93]=z[11]*z[78];
    z[93]=z[93] + z[92];
    z[93]=z[13]*z[93];
    z[95]= - z[11] + z[100];
    z[95]=z[29]*z[95];
    z[93]=z[93] + z[95];
    z[95]=z[3]*z[78];
    z[88]=z[95] + z[88];
    z[88]=z[12]*z[88];
    z[95]=z[3] - z[97];
    z[95]=z[20]*z[95];
    z[88]=z[88] + z[95];
    z[95]=n<T>(29,4)*z[3] - z[108];
    z[94]= - z[94] + n<T>(1,3)*z[95] + n<T>(11,4)*z[9];
    z[94]=z[27]*z[94];
    z[95]= - z[9] + z[78] + z[106];
    z[95]= - n<T>(23,3)*z[6] + 3*z[95];
    z[95]=n<T>(1,2)*z[95] - n<T>(7,3)*z[11];
    z[95]=z[30]*z[95];
    z[78]= - z[10]*z[78];
    z[78]=z[78] - z[92];
    z[78]=z[14]*z[78];
    z[92]= - z[76] + z[75] + z[65] - z[56] + z[53] - z[45];
    z[96]=z[64] + z[46];
    z[97]=z[68] + z[49];
    z[98]=z[73] + z[70];
    z[99]= - z[9] - z[11];
    z[99]=z[41]*z[99];
    z[99]=z[99] + z[39];
    z[101]=n<T>(10,3)*z[37] + n<T>(2,3)*z[35] - n<T>(5,8)*z[32];
    z[101]=i*z[101];
    z[102]= - z[11] + n<T>(13,4)*z[10] + z[9] + n<T>(9,4)*z[3] - z[100];
    z[102]=z[21]*z[102];
    z[79]=z[4] - z[79];
    z[79]=z[18]*z[79];
    z[100]=z[10] - z[100];
    z[100]=z[22]*z[100];
    z[103]=z[7] + z[10];
    z[103]=z[40]*z[103];

    r += n<T>(5,6)*z[33] - n<T>(4,9)*z[34] - n<T>(5,3)*z[38] + z[42] + n<T>(3,8)*z[48]
       - n<T>(5,12)*z[50] + n<T>(1,12)*z[51] - n<T>(9,16)*z[52] - n<T>(13,4)*z[54]
     + n<T>(19,16)*z[55] - n<T>(1,6)*z[58] - n<T>(35,12)*z[59] - n<T>(227,48)*z[60]
     - n<T>(85,24)*z[61]
     - 10*z[62]
     - n<T>(13,12)*z[63]
     - n<T>(45,16)*z[66]
     + n<T>(5,4)*
      z[67] + n<T>(11,24)*z[69] + 6*z[71] - n<T>(17,24)*z[72] + 2*z[74] + z[77]
       + z[78] + z[79] + z[80] + z[81] + z[82] + n<T>(1,3)*z[83] + n<T>(67,12)*
      z[84] + z[85] + n<T>(1,2)*z[86] + z[87] + n<T>(17,12)*z[88] + z[89] + 
      z[90] + n<T>(11,3)*z[91] - n<T>(1,4)*z[92] + n<T>(7,6)*z[93] + z[94] + z[95]
       + n<T>(41,48)*z[96] + n<T>(4,3)*z[97] - n<T>(1,8)*z[98] + n<T>(17,6)*z[99]
     +  z[100] + z[101] + z[102] + n<T>(41,12)*z[103];
 
    return r;
}

template std::complex<double> qg_2lha_tf183(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf183(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
