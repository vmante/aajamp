#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf339(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[93];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[9];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=e[0];
    z[17]=e[4];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[21];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[10];
    z[35]=e[6];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[2];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[10];
    z[44]=f[11];
    z[45]=f[12];
    z[46]=f[13];
    z[47]=f[14];
    z[48]=f[16];
    z[49]=f[17];
    z[50]=f[18];
    z[51]=f[21];
    z[52]=f[30];
    z[53]=f[31];
    z[54]=f[32];
    z[55]=f[33];
    z[56]=f[36];
    z[57]=f[39];
    z[58]=f[43];
    z[59]=f[50];
    z[60]=f[51];
    z[61]=f[52];
    z[62]=f[55];
    z[63]=f[58];
    z[64]=n<T>(1,3)*z[9];
    z[65]=n<T>(1,6)*z[10];
    z[66]=z[1]*i;
    z[67]=z[66] - z[2];
    z[67]=z[65] + n<T>(1,2)*z[67] + z[64];
    z[67]=z[3]*z[67];
    z[68]=n<T>(1,2)*z[5];
    z[69]=z[66]*z[68];
    z[70]=n<T>(1,2)*z[2];
    z[71]=3*z[66];
    z[72]=z[71] - z[5];
    z[72]= - z[70] + n<T>(1,2)*z[72] + z[6];
    z[72]=z[2]*z[72];
    z[73]=n<T>(1,2)*z[4];
    z[74]=z[73] - z[66];
    z[75]=z[74]*z[73];
    z[76]=n<T>(1,4)*z[9];
    z[77]=z[66] - z[5];
    z[78]=z[76] + z[77];
    z[64]=z[78]*z[64];
    z[78]=z[10] + z[77];
    z[78]=z[78]*z[65];
    z[79]=2*z[66];
    z[80]=z[79] + z[6];
    z[81]=z[5] - z[80];
    z[81]=z[6]*z[81];
    z[64]=n<T>(1,2)*z[67] + z[78] + z[64] + z[75] + z[72] + z[69] + z[81];
    z[64]=z[3]*z[64];
    z[67]=z[4] - z[2];
    z[69]=2*z[6];
    z[72]= - 17*z[66] + z[5];
    z[67]= - n<T>(3,2)*z[9] + n<T>(2,3)*z[8] + n<T>(1,24)*z[72] + z[69] + n<T>(1,4)*
    z[67];
    z[67]=z[9]*z[67];
    z[72]=z[68] - z[66];
    z[72]=z[72]*z[5];
    z[75]=n<T>(1,12)*z[72];
    z[78]=z[80]*z[69];
    z[80]= - z[66]*z[73];
    z[81]=2*z[8];
    z[82]=z[6] + n<T>(1,3)*z[66];
    z[83]= - 2*z[82] + n<T>(1,3)*z[8];
    z[83]=z[83]*z[81];
    z[67]=z[67] + z[83] + z[80] + z[75] + z[78];
    z[67]=z[9]*z[67];
    z[78]=z[5] + z[69];
    z[78]=z[78]*z[69];
    z[80]= - z[81] + z[2] + z[77];
    z[80]=z[80]*z[81];
    z[83]=2*z[2];
    z[84]= - n<T>(7,3)*z[7] - z[8] - z[83] + 5*z[6] - z[79] - z[5];
    z[84]=z[7]*z[84];
    z[85]=z[79] - z[5];
    z[85]=z[85]*z[5];
    z[86]=z[77] + z[6];
    z[87]= - 2*z[86] - z[2];
    z[87]=z[2]*z[87];
    z[88]=z[66] - z[8];
    z[89]= - 2*z[88] - z[9];
    z[89]=z[9]*z[89];
    z[78]=z[84] + z[89] + z[80] + z[87] + z[85] + z[78];
    z[78]=z[7]*z[78];
    z[80]=n<T>(1,2)*z[6];
    z[84]= - z[66] + z[80];
    z[84]=z[6]*z[84];
    z[87]=2*z[5];
    z[89]=z[2] - n<T>(11,2)*z[66] - z[87];
    z[90]=n<T>(1,3)*z[2];
    z[89]=z[89]*z[90];
    z[91]=5*z[66];
    z[92]= - z[91] - n<T>(7,2)*z[5];
    z[92]= - z[73] + n<T>(11,6)*z[2] + n<T>(1,6)*z[92] + z[6];
    z[73]=z[92]*z[73];
    z[73]=z[73] + z[89] - n<T>(7,12)*z[72] + z[84];
    z[73]=z[4]*z[73];
    z[84]=7*z[66];
    z[87]= - z[84] + z[87];
    z[87]=n<T>(2,3)*z[87] + z[6];
    z[69]=z[87]*z[69];
    z[87]= - z[66] - z[68];
    z[87]=n<T>(5,4)*z[2] + 5*z[87] - 8*z[6];
    z[87]=z[87]*z[90];
    z[89]=z[5] - z[74];
    z[89]=z[4]*z[89];
    z[90]=n<T>(5,2)*z[2] - n<T>(5,8)*z[5] + 14*z[6];
    z[90]=n<T>(1,3)*z[90] - n<T>(5,8)*z[4];
    z[90]=z[8]*z[90];
    z[69]=z[90] + n<T>(5,4)*z[89] + z[87] - n<T>(5,12)*z[72] + z[69];
    z[69]=z[8]*z[69];
    z[72]=z[66] + z[80];
    z[72]=z[6]*z[72];
    z[80]= - n<T>(1,2)*z[9] - z[77];
    z[80]=z[80]*z[76];
    z[87]= - z[66] - n<T>(1,3)*z[5];
    z[76]= - z[76] + n<T>(1,3)*z[4] + n<T>(1,4)*z[87] + z[6];
    z[87]=n<T>(1,3)*z[10];
    z[76]=n<T>(1,2)*z[76] - z[87];
    z[76]=z[10]*z[76];
    z[82]=n<T>(1,6)*z[4] - z[82];
    z[82]=z[4]*z[82];
    z[72]=z[76] + z[80] + z[82] - z[75] + z[72];
    z[72]=z[10]*z[72];
    z[75]=z[66] - z[4];
    z[76]= - 2*z[75] - z[10];
    z[76]=z[76]*z[87];
    z[80]=2*z[77];
    z[82]= - z[80] - z[6];
    z[82]=z[6]*z[82];
    z[87]=npow(z[4],2);
    z[86]= - z[13]*z[86];
    z[76]=z[86] + z[76] - n<T>(1,3)*z[87] + z[85] + z[82];
    z[76]=z[13]*z[76];
    z[82]=z[8] - z[4];
    z[86]= - n<T>(23,4)*z[5] - 17*z[6];
    z[65]=n<T>(7,9)*z[13] + n<T>(11,12)*z[3] - z[65] + n<T>(8,9)*z[7] - n<T>(65,72)*z[9]
    + n<T>(1,3)*z[86] + z[70] + n<T>(53,72)*z[82];
    z[65]=z[23]*z[65];
    z[82]=z[66]*z[4];
    z[82]=z[82] + z[23];
    z[86]=z[13]*z[66];
    z[86]=z[86] + z[82];
    z[86]=z[11]*z[86];
    z[87]= - z[13] + z[75];
    z[87]=z[22]*z[87];
    z[89]= - z[10] - z[13];
    z[89]=z[36]*z[89];
    z[86]= - z[86] - z[87] - z[89] - z[59] + z[56] + z[46];
    z[80]= - 5*z[7] - z[81] + z[83] + z[80] - 7*z[6];
    z[80]=z[19]*z[80];
    z[81]= - z[9] - z[7];
    z[81]=z[35]*z[81];
    z[80]= - z[80] - z[81] + z[58] - z[52];
    z[81]= - z[77]*z[68];
    z[68]= - n<T>(13,12)*z[2] + n<T>(4,3)*z[6] + z[79] + z[68];
    z[68]=z[2]*z[68];
    z[77]=5*z[77] + 13*z[6];
    z[77]=z[6]*z[77];
    z[68]=z[68] + z[81] + n<T>(1,3)*z[77];
    z[68]=z[2]*z[68];
    z[77]=z[9]*z[66];
    z[77]=z[77] + z[82];
    z[77]=z[12]*z[77];
    z[75]= - z[9] + z[75];
    z[75]=z[17]*z[75];
    z[75]=z[77] + z[75] + z[63] + z[60] + z[51] - z[37];
    z[77]=z[9] + z[3];
    z[77]=z[33]*z[77];
    z[79]= - z[10] - z[3];
    z[79]=z[34]*z[79];
    z[77]= - z[43] - z[77] - z[79] + z[62] + z[50];
    z[79]=z[7] + z[8];
    z[79]=z[66]*z[79];
    z[79]=z[23] + z[79];
    z[79]=z[15]*z[79];
    z[81]=z[7] - z[88];
    z[81]=z[21]*z[81];
    z[79]=z[79] + z[81];
    z[81]= - n<T>(11,2) - z[84];
    z[81]= - n<T>(68,9)*z[6] + n<T>(1,2)*z[81] - n<T>(10,3)*z[5];
    z[81]=z[6]*z[81];
    z[81]=n<T>(8,3)*z[85] + z[81];
    z[81]=z[6]*z[81];
    z[82]=5*z[5] + z[6] + n<T>(11,2);
    z[83]= - z[66]*z[82];
    z[83]= - 5*z[23] + z[83];
    z[83]=z[14]*z[83];
    z[82]= - z[91] + z[82];
    z[82]=z[18]*z[82];
    z[74]= - z[70] - z[74];
    z[74]=z[16]*z[74];
    z[66]= - n<T>(1,2)*z[8] + z[66] - z[70];
    z[66]=z[20]*z[66];
    z[70]=z[39] + z[26];
    z[84]=z[45] + z[41];
    z[85]=z[47] - z[38];
    z[87]=z[57] + z[31];
    z[88]= - n<T>(7,2)*z[30] - n<T>(7,4)*z[28] - n<T>(1,2)*z[27] + n<T>(29,18)*z[24];
    z[88]=i*z[88];
    z[71]=n<T>(11,4) - z[71] + n<T>(8,3)*z[5];
    z[71]=z[71]*npow(z[5],2);

    r +=  - n<T>(37,6)*z[25] + n<T>(13,12)*z[29] - n<T>(61,12)*z[32] - n<T>(7,24)*z[40]
       - n<T>(7,12)*z[42] - n<T>(5,24)*z[44] - n<T>(1,8)*z[48] - n<T>(5,8)*z[49]
     + n<T>(22,3)*z[53]
     + 4*z[54]
     + n<T>(64,3)*z[55] - z[61]
     + z[64]
     + z[65]
     + n<T>(5,3)
      *z[66] + z[67] + z[68] + z[69] - n<T>(1,3)*z[70] + z[71] + z[72] + 
      z[73] + n<T>(7,3)*z[74] + n<T>(1,2)*z[75] + z[76] - n<T>(1,6)*z[77] + n<T>(4,3)*
      z[78] + 8*z[79] - n<T>(8,3)*z[80] + z[81] + z[82] + z[83] - n<T>(1,24)*
      z[84] - n<T>(1,4)*z[85] - n<T>(2,3)*z[86] + 2*z[87] + z[88];
 
    return r;
}

template std::complex<double> qg_2lha_tf339(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf339(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
