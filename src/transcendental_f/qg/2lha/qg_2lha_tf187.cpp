#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf187(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[93];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[4];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[15];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[12];
    z[15]=d[7];
    z[16]=d[17];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[9];
    z[24]=e[12];
    z[25]=e[13];
    z[26]=c[3];
    z[27]=c[11];
    z[28]=c[15];
    z[29]=c[19];
    z[30]=c[23];
    z[31]=c[25];
    z[32]=c[26];
    z[33]=c[28];
    z[34]=c[31];
    z[35]=e[3];
    z[36]=e[10];
    z[37]=e[6];
    z[38]=e[14];
    z[39]=f[0];
    z[40]=f[1];
    z[41]=f[2];
    z[42]=f[3];
    z[43]=f[4];
    z[44]=f[10];
    z[45]=f[12];
    z[46]=f[13];
    z[47]=f[14];
    z[48]=f[16];
    z[49]=f[18];
    z[50]=f[21];
    z[51]=f[30];
    z[52]=f[31];
    z[53]=f[32];
    z[54]=f[33];
    z[55]=f[34];
    z[56]=f[35];
    z[57]=f[36];
    z[58]=f[39];
    z[59]=f[43];
    z[60]=f[50];
    z[61]=f[51];
    z[62]=f[52];
    z[63]=f[53];
    z[64]=f[55];
    z[65]=f[58];
    z[66]=7*z[6];
    z[67]=z[66] - 11;
    z[68]=z[1]*i;
    z[69]= - z[68]*z[67];
    z[70]=z[68]*z[5];
    z[70]=z[70] + z[26];
    z[69]=z[69] + z[70];
    z[69]=z[14]*z[69];
    z[71]=z[4] + z[2];
    z[72]=11 + n<T>(7,3)*z[68];
    z[73]=z[72] - n<T>(7,6)*z[71];
    z[73]=z[17]*z[73];
    z[74]=z[68] - z[5];
    z[67]=z[74] + z[67];
    z[67]=z[21]*z[67];
    z[75]=z[68]*z[4];
    z[75]=z[75] + z[26];
    z[76]= - z[10]*z[68];
    z[76]=z[76] - z[75];
    z[76]=z[12]*z[76];
    z[77]=z[8]*z[68];
    z[75]=z[77] + z[75];
    z[75]=z[13]*z[75];
    z[77]=z[68] - z[4];
    z[78]= - z[8] + z[77];
    z[78]=z[20]*z[78];
    z[77]=z[10] - z[77];
    z[77]=z[24]*z[77];
    z[79]=z[9] + z[3];
    z[79]= - 5 + n<T>(7,3)*z[79];
    z[79]=z[36]*z[79];
    z[80]=z[10] + z[9];
    z[80]=z[38]*z[80];
    z[67]=z[67] + z[69] + z[73] + z[79] + z[80] + z[50] - z[39] + z[76]
    + z[75] + z[78] + z[77];
    z[69]=n<T>(1,2)*z[10];
    z[73]=n<T>(1,2)*z[6];
    z[75]=z[69] - z[73];
    z[76]=n<T>(1,18)*z[2];
    z[77]=4*z[68];
    z[78]= - z[77] + z[5];
    z[78]= - z[76] + n<T>(1,9)*z[78] - z[75];
    z[78]=z[2]*z[78];
    z[79]=n<T>(1,3)*z[5];
    z[80]= - z[68] - z[79];
    z[81]= - n<T>(1,6)*z[10] + n<T>(1,4)*z[6];
    z[71]=n<T>(1,6)*z[3] + n<T>(2,3)*z[80] - z[81] + n<T>(2,9)*z[71];
    z[71]=z[4]*z[71];
    z[80]=z[6] + z[5];
    z[82]=z[80] - z[69];
    z[69]=z[82]*z[69];
    z[83]=n<T>(1,2)*z[5];
    z[77]=z[77] - z[83];
    z[77]=z[5]*z[77];
    z[84]=z[68] - z[80];
    z[84]=z[84]*z[73];
    z[85]=n<T>(1,3)*z[68];
    z[86]= - z[3]*z[85];
    z[71]=z[71] + z[86] + z[78] + z[69] + n<T>(1,9)*z[77] + z[84];
    z[71]=z[4]*z[71];
    z[72]=n<T>(1,6)*z[5] - z[72];
    z[72]=z[72]*z[79];
    z[77]=z[85] + z[73];
    z[77]=z[77]*z[73];
    z[78]=n<T>(5,2)*z[6];
    z[84]=1 + z[85];
    z[84]= - z[78] + 11*z[84] + n<T>(5,3)*z[5];
    z[84]= - n<T>(5,9)*z[2] + n<T>(1,3)*z[84] + z[10];
    z[86]=n<T>(1,2)*z[2];
    z[84]=z[84]*z[86];
    z[69]=z[84] - z[69] + z[72] + z[77];
    z[69]=z[2]*z[69];
    z[72]=z[68] - 4*z[5];
    z[72]=z[72]*z[79];
    z[77]=5*z[74] - z[73];
    z[77]=z[77]*z[73];
    z[72]=z[72] + z[77];
    z[77]=n<T>(1,4)*z[10] + z[73] - z[83] + n<T>(5,3) + n<T>(1,2)*z[68];
    z[77]=z[10]*z[77];
    z[84]=n<T>(1,3)*z[2];
    z[87]=z[68] + z[5];
    z[88]= - z[2] + z[87];
    z[88]=z[88]*z[84];
    z[89]= - 5 - z[85];
    z[89]=n<T>(10,3)*z[3] - z[86] + n<T>(1,2)*z[89] - n<T>(2,3)*z[5];
    z[90]=n<T>(1,3)*z[3];
    z[89]=z[89]*z[90];
    z[72]=z[89] + z[88] + n<T>(1,3)*z[72] + z[77];
    z[72]=z[3]*z[72];
    z[77]=z[83] - z[68];
    z[88]=z[77]*z[5];
    z[82]= - z[85] + z[82];
    z[82]=z[10]*z[82];
    z[80]= - z[6]*z[80];
    z[80]=z[82] + n<T>(7,9)*z[88] + z[80];
    z[82]=n<T>(1,12)*z[4];
    z[89]= - z[68] + n<T>(7,4)*z[5];
    z[81]=n<T>(1,6)*z[9] - z[82] - n<T>(7,9)*z[3] + n<T>(1,9)*z[89] - z[81];
    z[81]=z[9]*z[81];
    z[75]= - n<T>(7,18)*z[3] - n<T>(7,9)*z[74] - z[75];
    z[75]=z[3]*z[75];
    z[89]=z[85] + z[6];
    z[82]= - z[82] + n<T>(1,2)*z[89] - n<T>(1,3)*z[10];
    z[82]=z[4]*z[82];
    z[75]=z[81] + z[82] + n<T>(1,2)*z[80] + z[75];
    z[75]=z[9]*z[75];
    z[73]=z[68] + z[73];
    z[80]=n<T>(25,6)*z[6];
    z[73]=z[73]*z[80];
    z[73]=z[88] + z[73];
    z[80]=z[80] - n<T>(43,18)*z[68] + z[5];
    z[81]=n<T>(7,6)*z[9];
    z[80]= - n<T>(29,18)*z[8] + z[81] + n<T>(1,3)*z[4] - n<T>(5,3)*z[3] + n<T>(1,2)*
    z[80] - z[84];
    z[82]=n<T>(1,2)*z[8];
    z[80]=z[80]*z[82];
    z[91]= - 2*z[74] - z[3];
    z[90]=z[91]*z[90];
    z[91]=n<T>(1,2)*z[9];
    z[92]=z[91] + z[74];
    z[81]=z[92]*z[81];
    z[85]= - z[4]*z[85];
    z[73]=z[80] + z[81] + z[85] + n<T>(1,2)*z[73] + z[90];
    z[73]=z[8]*z[73];
    z[80]= - n<T>(7,2)*z[6] + n<T>(13,6) + z[74];
    z[80]=z[6]*z[80];
    z[81]= - 5*z[6] - z[74];
    z[81]=n<T>(1,2)*z[81] + n<T>(7,3)*z[10];
    z[81]=z[10]*z[81];
    z[80]=z[81] + z[88] + z[80];
    z[80]=z[10]*z[80];
    z[81]=npow(z[5],2);
    z[85]=z[68] - n<T>(5,9)*z[5];
    z[85]=z[85]*z[81];
    z[88]= - n<T>(1,3) - n<T>(5,2)*z[68];
    z[88]=n<T>(17,18)*z[6] + n<T>(1,2)*z[88] - z[79];
    z[88]=z[6]*z[88];
    z[81]=3*z[81] + z[88];
    z[81]=z[6]*z[81];
    z[80]= - z[28] + z[80] + z[85] + z[81];
    z[77]=z[77]*z[83];
    z[81]=z[86] + z[6] + z[74];
    z[81]=z[81]*z[86];
    z[82]= - z[68] - z[82];
    z[85]=n<T>(25,6)*z[8];
    z[82]=z[82]*z[85];
    z[83]= - z[83] - z[6];
    z[83]=z[6]*z[83];
    z[77]=z[82] + z[81] + z[77] + z[83];
    z[81]=n<T>(35,18)*z[7] + z[84] + n<T>(11,6)*z[6] + n<T>(13,9)*z[68] - n<T>(5,2)*z[5]
   ;
    z[82]=n<T>(1,2)*z[7];
    z[81]=z[81]*z[82];
    z[77]=n<T>(1,3)*z[77] + z[81];
    z[77]=z[7]*z[77];
    z[81]=z[15] + z[7];
    z[83]=z[68]*z[81];
    z[83]=z[26] + z[83];
    z[83]=z[16]*z[83];
    z[81]= - z[68] + z[81];
    z[81]=z[23]*z[81];
    z[81]=z[83] + z[81];
    z[79]=z[84] + n<T>(25,12)*z[6] - n<T>(7,2)*z[68] - z[79];
    z[79]=z[6]*z[79];
    z[83]=n<T>(1,6)*z[8] - z[89];
    z[83]=z[83]*z[85];
    z[85]=z[68] + z[8];
    z[85]= - n<T>(25,6)*z[7] - z[2] + z[5] + n<T>(25,3)*z[85];
    z[85]=z[7]*z[85];
    z[79]=n<T>(1,3)*z[85] + z[83] + z[79];
    z[83]=z[66] + n<T>(25,9)*z[8];
    z[83]=n<T>(1,8)*z[83] - n<T>(11,9)*z[7];
    z[83]=z[15]*z[83];
    z[79]=n<T>(1,2)*z[79] + z[83];
    z[79]=z[15]*z[79];
    z[83]=z[74] - z[10];
    z[66]=z[66] - 3*z[83];
    z[66]=z[82] + z[91] + n<T>(1,2)*z[66] - z[3];
    z[66]=z[25]*z[66];
    z[82]=z[74] - z[15];
    z[78]= - n<T>(17,6)*z[7] - z[84] - z[78] - n<T>(1,3)*z[82];
    z[78]=z[22]*z[78];
    z[68]=z[68]*z[3];
    z[68]=z[68] + z[70];
    z[68]=z[11]*z[68];
    z[70]=z[3] - z[74];
    z[70]=z[19]*z[70];
    z[68]=z[33] + z[68] + z[70];
    z[70]= - n<T>(1,6)*z[4] - z[76] + z[10] + n<T>(1,9)*z[87] - z[6];
    z[70]=z[18]*z[70];
    z[74]=z[40] + z[59] - z[56] + z[55] - z[47];
    z[76]=z[65] + z[62] + z[61] + z[45];
    z[82]=z[46] + z[42];
    z[83]=z[60] - z[48];
    z[84]= - z[8] - z[7];
    z[84]=z[37]*z[84];
    z[84]=z[84] + z[51];
    z[85]=31*z[5] - 49*z[6];
    z[85]= - n<T>(67,6)*z[4] - 7*z[3] - n<T>(17,6)*z[2] + n<T>(1,2)*z[85] - 13*z[10]
   ;
    z[85]=n<T>(19,24)*z[15] + n<T>(17,4)*z[7] - n<T>(83,12)*z[8] + n<T>(1,2)*z[85]
     - n<T>(4,3)*z[9];
    z[85]=z[26]*z[85];
    z[85]=z[85] - z[49];
    z[86]= - n<T>(8,9)*z[32] - n<T>(11,18)*z[30] - n<T>(35,72)*z[27];
    z[86]=i*z[86];
    z[87]=z[3] + z[8];
    z[87]=z[35]*z[87];

    r +=  - n<T>(17,54)*z[29] + n<T>(5,18)*z[31] + n<T>(55,9)*z[34] + n<T>(2,3)*z[41]
       + n<T>(7,36)*z[43] - n<T>(7,9)*z[44] + n<T>(17,24)*z[52] + n<T>(13,12)*z[53]
     - n<T>(4,3)*z[54]
     - n<T>(25,72)*z[57]
     + n<T>(25,24)*z[58] - 5*z[63]
     + n<T>(1,12)*
      z[64] + z[66] + n<T>(1,3)*z[67] + n<T>(4,9)*z[68] + z[69] + z[70] + z[71]
       + z[72] + z[73] + n<T>(1,6)*z[74] + z[75] - n<T>(1,4)*z[76] + z[77] + 
      z[78] + z[79] + n<T>(1,2)*z[80] + n<T>(19,18)*z[81] - n<T>(2,9)*z[82] - n<T>(7,12)
      *z[83] + n<T>(25,18)*z[84] + n<T>(1,9)*z[85] + z[86] + z[87];
 
    return r;
}

template std::complex<double> qg_2lha_tf187(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf187(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
