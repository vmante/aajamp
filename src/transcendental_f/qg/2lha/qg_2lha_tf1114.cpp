#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1114(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[24];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[5];
    z[4]=d[12];
    z[5]=d[15];
    z[6]=c[3];
    z[7]=d[0];
    z[8]=d[3];
    z[9]=d[7];
    z[10]=d[9];
    z[11]=d[2];
    z[12]=e[0];
    z[13]=e[2];
    z[14]=e[5];
    z[15]=e[8];
    z[16]=e[13];
    z[17]=2*z[3];
    z[18]=z[5] + 2*z[4] - z[17];
    z[19]=i*z[1];
    z[18]=z[19]*z[18];
    z[20]=z[8] - z[7];
    z[17]=z[17] - z[20];
    z[17]=z[8]*z[17];
    z[21]=z[10] - z[3];
    z[22]= - z[7] + z[21];
    z[22]=z[7]*z[22];
    z[23]=npow(z[3],2);
    z[19]=n<T>(1,2)*z[2] - z[3] - z[19];
    z[19]=z[2]*z[19];
    z[20]= - z[9]*z[20];
    z[21]= - z[11]*z[21];

    r += z[6] - z[12] + z[13] - 2*z[14] - z[15] - z[16] + z[17] + z[18]
       + z[19] + z[20] + z[21] + z[22] - n<T>(1,2)*z[23];
 
    return r;
}

template std::complex<double> qg_2lha_tf1114(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1114(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
