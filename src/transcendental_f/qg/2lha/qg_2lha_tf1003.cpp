#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1003(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[31];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[15];
    z[11]=c[18];
    z[12]=c[23];
    z[13]=c[25];
    z[14]=c[26];
    z[15]=c[28];
    z[16]=c[29];
    z[17]=c[30];
    z[18]=c[31];
    z[19]=f[3];
    z[20]=f[5];
    z[21]=f[11];
    z[22]=f[13];
    z[23]=n<T>(1,3)*z[3];
    z[24]=i*z[1];
    z[25]= - n<T>(2,3)*z[24] + z[23] - z[5];
    z[25]=z[25]*z[3];
    z[24]=2*z[24];
    z[26]=z[24] - z[5];
    z[27]=n<T>(2,3)*z[5];
    z[26]=z[26]*z[27];
    z[25]=z[25] - z[26];
    z[26]=n<T>(1,3)*z[5];
    z[23]=z[4] - z[23] - z[24] + z[26];
    z[23]=z[4]*z[23];
    z[24]= - z[2] + n<T>(2,3)*z[3] + z[24] + z[26];
    z[24]=z[2]*z[24];
    z[26]=n<T>(2,3)*z[8] + n<T>(1,3)*z[6];
    z[28]= - z[7] + n<T>(1,6)*z[9];
    z[23]=z[24] + z[23] + z[28] + z[25] - z[26];
    z[23]=z[2]*z[23];
    z[24]= - 2*z[5] - z[3];
    z[24]=z[4]*z[24];
    z[24]=n<T>(1,3)*z[24] - z[8] + z[28] - z[25];
    z[24]=z[4]*z[24];
    z[25]=2*i;
    z[26]=z[7] + z[26];
    z[26]=z[1]*z[26];
    z[26]=z[26] - 2*z[14] - z[12];
    z[25]=z[26]*z[25];
    z[26]=n<T>(1,3)*z[9] - z[8];
    z[26]=z[26]*z[27];
    z[27]=z[15] + z[17];
    z[28]=z[11] - z[13];
    z[29]= - z[20] + z[22] - z[21];
    z[29]= - z[10] - n<T>(37,2)*z[18] + 2*z[29] - z[19];
    z[30]= - z[6] - n<T>(2,3)*z[9];
    z[30]=n<T>(1,3)*z[30] + z[8];
    z[30]=z[3]*z[30];

    r += z[16] + z[23] + z[24] + z[25] + z[26] + 4*z[27] - n<T>(4,3)*z[28]
       + n<T>(1,3)*z[29] + z[30];
 
    return r;
}

template std::complex<double> qg_2lha_tf1003(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1003(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
