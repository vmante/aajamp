#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf149(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[109];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[6];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[10];
    z[28]=e[11];
    z[29]=e[12];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[19];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[31];
    z[38]=e[3];
    z[39]=e[13];
    z[40]=e[14];
    z[41]=f[0];
    z[42]=f[1];
    z[43]=f[2];
    z[44]=f[3];
    z[45]=f[4];
    z[46]=f[5];
    z[47]=f[6];
    z[48]=f[7];
    z[49]=f[10];
    z[50]=f[11];
    z[51]=f[12];
    z[52]=f[13];
    z[53]=f[14];
    z[54]=f[16];
    z[55]=f[17];
    z[56]=f[18];
    z[57]=f[21];
    z[58]=f[23];
    z[59]=f[30];
    z[60]=f[31];
    z[61]=f[32];
    z[62]=f[33];
    z[63]=f[34];
    z[64]=f[35];
    z[65]=f[36];
    z[66]=f[37];
    z[67]=f[38];
    z[68]=f[39];
    z[69]=f[43];
    z[70]=f[50];
    z[71]=f[51];
    z[72]=f[55];
    z[73]=f[56];
    z[74]=f[58];
    z[75]=f[60];
    z[76]= - 11 + n<T>(31,6)*z[2];
    z[77]=n<T>(5,2)*z[9];
    z[78]=n<T>(13,6)*z[5];
    z[79]=z[1]*i;
    z[80]= - z[77] - z[78] - n<T>(61,6)*z[79] + z[76];
    z[81]=n<T>(1,2)*z[2];
    z[80]=z[80]*z[81];
    z[82]= - z[78] + 11 + n<T>(10,3)*z[79];
    z[82]=z[5]*z[82];
    z[83]=n<T>(1,2)*z[9];
    z[84]=z[83] - z[79];
    z[85]=n<T>(1,2)*z[5];
    z[86]=z[85] - z[84];
    z[87]=5*z[9];
    z[86]=z[86]*z[87];
    z[80]=z[80] + z[82] + z[86];
    z[80]=z[2]*z[80];
    z[82]= - 11 + 7*z[7];
    z[86]= - z[5] + z[82];
    z[86]=z[79]*z[86];
    z[86]= - z[30] + z[86];
    z[86]=z[15]*z[86];
    z[76]=n<T>(31,6)*z[4] - n<T>(31,3)*z[79] + z[76];
    z[76]=z[18]*z[76];
    z[88]=5*z[79];
    z[89]= - n<T>(5,2)*z[2] + z[88] + z[85];
    z[89]=n<T>(1,3)*z[89] - z[4];
    z[89]=z[19]*z[89];
    z[90]=z[79] - z[5];
    z[82]= - z[90] - z[82];
    z[82]=z[22]*z[82];
    z[91]=z[79]*z[4];
    z[91]=z[91] + z[30];
    z[92]= - z[6]*z[79];
    z[92]=z[92] - z[91];
    z[92]=z[14]*z[92];
    z[93]=n<T>(5,2)*z[5];
    z[94]=n<T>(2,3)*z[11] + z[3] - n<T>(11,12)*z[8] + n<T>(67,36)*z[10] - n<T>(37,36)*
    z[6] + n<T>(55,36)*z[4] + n<T>(71,12)*z[7] + n<T>(25,18)*z[2] - z[93] - n<T>(29,9)*
    z[9];
    z[94]=z[30]*z[94];
    z[95]=z[79] - z[9];
    z[96]=z[5] - z[95];
    z[97]=z[96] - z[2];
    z[98]= - n<T>(29,6)*z[3] - n<T>(35,6)*z[10] - z[97];
    z[98]=z[27]*z[98];
    z[99]=z[79] - z[4];
    z[100]=z[6] - z[99];
    z[100]=z[21]*z[100];
    z[101]=z[7] + z[11];
    z[101]=z[39]*z[101];
    z[76]= - z[82] - z[92] - z[100] - z[101] - z[98] - z[86] - z[76] - 
    z[89] - z[80] - z[94] + z[70] - z[63] + z[57] - z[41];
    z[80]=z[81] - z[79];
    z[80]=z[80]*z[2];
    z[82]=z[84]*z[83];
    z[86]=n<T>(1,6)*z[7];
    z[89]= - z[79] + z[7];
    z[89]=z[89]*z[86];
    z[92]=n<T>(1,6)*z[4];
    z[94]=n<T>(1,2)*z[4];
    z[98]=z[94] + z[7];
    z[100]=z[98]*z[92];
    z[101]=n<T>(1,2)*z[6];
    z[102]=z[6] - z[4] + z[2] - z[7];
    z[102]=z[102]*z[101];
    z[103]=n<T>(1,3)*z[10];
    z[104]=n<T>(1,2)*z[10] + z[99];
    z[104]=z[104]*z[103];
    z[105]= - z[6] - z[99];
    z[105]=z[8]*z[105];
    z[106]=z[10] + z[79] - z[2];
    z[106]=z[11]*z[106];
    z[82]=n<T>(1,4)*z[106] + n<T>(1,2)*z[105] + z[104] + z[102] + z[100] + z[89]
    + z[82] - z[80];
    z[82]=z[11]*z[82];
    z[89]=z[85] - z[79];
    z[89]=z[89]*z[5];
    z[100]=z[9]*z[84];
    z[100]= - 7*z[89] + n<T>(29,3)*z[100];
    z[102]=13*z[79];
    z[104]=n<T>(1,2)*z[7];
    z[105]= - z[104] - z[102] + 7*z[9];
    z[86]=z[105]*z[86];
    z[105]=n<T>(1,3)*z[79];
    z[106]= - z[105] + z[98];
    z[106]=z[4]*z[106];
    z[107]=z[105] - n<T>(1,4)*z[5];
    z[107]= - n<T>(43,6)*z[6] - n<T>(5,2)*z[4] - n<T>(13,4)*z[7] - z[81] + 7*z[107]
    + n<T>(29,12)*z[9];
    z[107]=z[6]*z[107];
    z[80]=n<T>(1,3)*z[107] + z[106] + z[86] + n<T>(1,6)*z[100] - z[80];
    z[80]=z[80]*z[101];
    z[86]=z[96] - z[81];
    z[86]=z[86]*z[81];
    z[85]= - n<T>(1,4)*z[9] - z[79] - z[85];
    z[85]=z[9]*z[85];
    z[85]=z[86] - n<T>(35,12)*z[89] + z[85];
    z[96]= - z[79] - z[104];
    z[96]=z[7]*z[96];
    z[92]=z[92] - z[105] + z[7];
    z[100]=n<T>(1,4)*z[4];
    z[92]=z[92]*z[100];
    z[101]= - z[101] - z[90];
    z[101]=z[6]*z[101];
    z[106]=z[83] - z[81];
    z[107]=4*z[79] - n<T>(35,8)*z[5];
    z[107]=n<T>(1,3)*z[107] + z[106];
    z[107]= - n<T>(7,36)*z[10] - n<T>(17,24)*z[6] + n<T>(1,24)*z[4] + n<T>(1,3)*z[107]
    - n<T>(1,8)*z[7];
    z[107]=z[10]*z[107];
    z[85]=z[107] + n<T>(17,12)*z[101] + z[92] + n<T>(1,3)*z[85] + n<T>(1,4)*z[96];
    z[85]=z[10]*z[85];
    z[92]=n<T>(7,3)*z[79];
    z[96]= - z[92] - z[5];
    z[96]=n<T>(1,2)*z[96] + n<T>(2,3)*z[9];
    z[96]=z[9]*z[96];
    z[86]=z[86] + z[89] + z[96];
    z[96]=8*z[79] - n<T>(25,2)*z[9];
    z[96]=n<T>(13,9)*z[6] + z[94] + n<T>(1,9)*z[96] + z[104];
    z[96]=z[6]*z[96];
    z[101]=n<T>(1,6)*z[2];
    z[107]= - n<T>(53,36)*z[8] + n<T>(5,4)*z[6] - z[100] - n<T>(7,6)*z[7] - z[101]
    + n<T>(7,36)*z[9] - n<T>(11,9)*z[79] + n<T>(3,2)*z[5];
    z[107]=z[8]*z[107];
    z[108]=n<T>(1,3)*z[5];
    z[101]=n<T>(1,12)*z[7] - z[101] - z[108] + z[9];
    z[101]=z[7]*z[101];
    z[98]=z[79] - z[98];
    z[98]=z[98]*z[94];
    z[86]=z[107] + z[96] + z[98] + n<T>(1,3)*z[86] + z[101];
    z[86]=z[8]*z[86];
    z[96]=n<T>(5,2)*z[79];
    z[98]= - z[96] + 7*z[5];
    z[98]=z[98]*z[108];
    z[101]=z[83] - z[90];
    z[101]=z[101]*z[83];
    z[94]=z[79] - z[94];
    z[94]=z[4]*z[94];
    z[94]=z[94] + z[98] + z[101];
    z[98]=n<T>(1,3)*z[9];
    z[101]= - z[98] + n<T>(5,6)*z[2];
    z[105]= - z[105] - z[5] + z[101];
    z[105]=z[105]*z[81];
    z[106]=n<T>(43,12)*z[10] + n<T>(4,3)*z[90] + z[106];
    z[103]=z[106]*z[103];
    z[93]=7*z[79] + z[93];
    z[93]=n<T>(1,3)*z[93] - z[83];
    z[93]=n<T>(5,6)*z[6] + n<T>(1,3)*z[93] + z[2];
    z[93]= - n<T>(17,12)*z[3] + n<T>(1,2)*z[93] + n<T>(2,9)*z[10];
    z[93]=z[3]*z[93];
    z[106]= - 2*z[90] - z[7];
    z[106]=z[7]*z[106];
    z[107]=n<T>(5,6)*z[90] + z[6];
    z[107]=z[6]*z[107];
    z[93]=z[93] + z[103] + z[107] + n<T>(2,3)*z[106] + z[105] + n<T>(1,3)*z[94];
    z[93]=z[3]*z[93];
    z[94]=2*z[5];
    z[103]=z[79] - z[94];
    z[103]=z[5]*z[103];
    z[96]= - n<T>(5,4)*z[9] + z[96] + z[108];
    z[96]=z[96]*z[83];
    z[92]= - z[92] + z[5] + z[101];
    z[92]=z[92]*z[81];
    z[101]=n<T>(37,12)*z[7] - n<T>(19,8)*z[9] + z[94] - n<T>(11,2) + z[79];
    z[101]=z[7]*z[101];
    z[92]=n<T>(1,3)*z[101] + z[92] + z[103] + z[96];
    z[92]=z[7]*z[92];
    z[96]= - z[5] + z[84];
    z[87]=z[96]*z[87];
    z[96]= - z[102] + n<T>(37,2)*z[5];
    z[96]=z[5]*z[96];
    z[87]=n<T>(1,9)*z[96] + z[87];
    z[94]= - z[2] + n<T>(29,2)*z[79] + z[94];
    z[94]=z[2]*z[94];
    z[96]=z[79] - z[104];
    z[96]=z[7]*z[96];
    z[78]=z[88] + z[78];
    z[77]=n<T>(1,9)*z[4] - n<T>(5,6)*z[7] - n<T>(29,9)*z[2] + n<T>(1,3)*z[78] + z[77];
    z[77]=z[77]*z[100];
    z[77]=z[77] + n<T>(5,12)*z[96] + n<T>(1,4)*z[87] + n<T>(1,9)*z[94];
    z[77]=z[4]*z[77];
    z[78]=z[98] - 3*z[79] + n<T>(5,6)*z[5];
    z[78]=z[9]*z[78];
    z[78]=n<T>(5,3)*z[89] + z[78];
    z[78]=z[78]*z[83];
    z[83]=z[79]*z[9];
    z[83]=z[83] + z[30];
    z[87]=z[10]*z[79];
    z[87]=z[87] + z[83];
    z[87]=z[17]*z[87];
    z[88]= - z[79] + n<T>(5,9)*z[5];
    z[88]=z[88]*npow(z[5],2);
    z[89]= - z[10] + z[95];
    z[89]=z[28]*z[89];
    z[78]=z[87] + z[88] + z[78] + z[89] + z[75];
    z[87]= - z[11]*z[79];
    z[87]=z[87] - z[91];
    z[87]=z[13]*z[87];
    z[88]=z[11] - z[99];
    z[88]=z[29]*z[88];
    z[89]= - z[10] - z[11];
    z[89]=z[40]*z[89];
    z[87]=z[87] + z[88] + z[89] - z[69] + z[58] + z[53];
    z[88]= - z[8]*z[79];
    z[83]=z[88] - z[83];
    z[83]=z[16]*z[83];
    z[88]= - z[8] + z[95];
    z[88]=z[26]*z[88];
    z[83]=z[52] + z[83] + z[88];
    z[88]= - z[5] - z[3];
    z[79]=z[79]*z[88];
    z[79]= - z[30] + z[79];
    z[79]=z[12]*z[79];
    z[88]= - z[3] + z[90];
    z[88]=z[20]*z[88];
    z[79]=z[79] + z[88];
    z[81]=z[81] + z[84];
    z[81]=z[25]*z[81];
    z[84]=z[47] + z[73] + z[66];
    z[88]=z[43] + z[64] - z[46];
    z[89]= - z[48] + z[62] - z[61];
    z[90]=z[71] - z[50];
    z[91]=z[72] + z[68];
    z[94]=n<T>(41,9)*z[35] + n<T>(61,36)*z[33] + n<T>(11,18)*z[31];
    z[94]=i*z[94];
    z[95]=z[11] - n<T>(29,18)*z[8] - n<T>(47,18)*z[6] - z[7] + z[99];
    z[95]=z[23]*z[95];
    z[96]=n<T>(11,6)*z[8] - n<T>(1,3)*z[97] + n<T>(3,2)*z[7];
    z[96]=z[24]*z[96];
    z[97]= - z[6] - z[3];
    z[97]=z[38]*z[97];

    r +=  - n<T>(13,54)*z[32] - n<T>(31,36)*z[34] - n<T>(41,18)*z[36] - n<T>(119,18)*
      z[37] + n<T>(1,12)*z[42] + n<T>(13,72)*z[44] - n<T>(47,72)*z[45] + n<T>(11,18)*
      z[49] + n<T>(7,24)*z[51] - n<T>(17,24)*z[54] + n<T>(5,8)*z[55] + n<T>(1,9)*z[56]
       - n<T>(25,18)*z[59] - n<T>(11,24)*z[60] - n<T>(29,72)*z[65] - 4*z[67] - n<T>(1,8)
      *z[74] - n<T>(1,3)*z[76] + z[77] + n<T>(1,2)*z[78] + n<T>(17,18)*z[79] + 
      z[80] + n<T>(5,3)*z[81] + z[82] + n<T>(1,18)*z[83] - n<T>(1,4)*z[84] + z[85]
       + z[86] + n<T>(1,6)*z[87] - n<T>(5,6)*z[88] + n<T>(4,3)*z[89] - n<T>(5,24)*z[90]
       - n<T>(1,24)*z[91] + z[92] + z[93] + z[94] + z[95] + z[96] + n<T>(7,6)*
      z[97];
 
    return r;
}

template std::complex<double> qg_2lha_tf149(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf149(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
