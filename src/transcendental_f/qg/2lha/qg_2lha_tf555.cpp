#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf555(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[72];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[11];
    z[10]=d[12];
    z[11]=d[4];
    z[12]=d[9];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[5];
    z[17]=e[7];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=e[12];
    z[21]=c[3];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[13];
    z[31]=e[6];
    z[32]=e[14];
    z[33]=f[3];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[13];
    z[37]=f[30];
    z[38]=f[31];
    z[39]=f[32];
    z[40]=f[33];
    z[41]=f[36];
    z[42]=f[39];
    z[43]=f[43];
    z[44]=f[50];
    z[45]=f[51];
    z[46]=f[53];
    z[47]=f[55];
    z[48]=f[58];
    z[49]=2*z[4];
    z[50]=2*z[2];
    z[51]=z[49] - z[50];
    z[52]=n<T>(11,2)*z[7];
    z[53]=z[1]*i;
    z[54]=z[52] - n<T>(7,2)*z[53] + z[51];
    z[54]=z[7]*z[54];
    z[55]=11*z[53] - 29*z[5];
    z[55]=n<T>(23,6)*z[6] + n<T>(7,4)*z[7] + z[50] + n<T>(1,4)*z[55] + z[4];
    z[55]=z[6]*z[55];
    z[56]=z[53] + z[5];
    z[57]=2*z[56];
    z[58]=z[57] - z[4];
    z[58]=z[58]*z[4];
    z[59]=npow(z[5],2);
    z[56]= - z[4] + z[56];
    z[56]=2*z[56] + z[2];
    z[56]=z[2]*z[56];
    z[54]=z[55] + z[54] + z[56] - n<T>(25,4)*z[59] - z[58];
    z[55]=n<T>(1,3)*z[6];
    z[54]=z[54]*z[55];
    z[56]=n<T>(1,2)*z[5];
    z[60]=5*z[53];
    z[61]= - z[60] + n<T>(11,2)*z[5];
    z[61]=z[61]*z[56];
    z[62]=2*z[53];
    z[63]=z[62] - z[4];
    z[49]= - z[63]*z[49];
    z[63]=z[53] - z[3];
    z[51]=n<T>(5,4)*z[5] + z[63] + z[51];
    z[51]=z[3]*z[51];
    z[64]=4*z[53];
    z[65]=z[64] - z[2] - z[4];
    z[65]=z[2]*z[65];
    z[49]=z[51] + z[65] + z[61] + z[49];
    z[51]=z[2] - z[4];
    z[61]=z[7]*z[51];
    z[49]=z[61] + n<T>(1,3)*z[49];
    z[49]=z[3]*z[49];
    z[56]=z[56] + z[53];
    z[61]=3*z[5];
    z[65]=z[56]*z[61];
    z[66]=n<T>(1,2)*z[3] - z[53] - z[61];
    z[66]=z[3]*z[66];
    z[61]=z[61] - z[63];
    z[67]=z[8] + z[12];
    z[61]=n<T>(1,2)*z[61] - z[67];
    z[61]=z[8]*z[61];
    z[61]=z[61] + z[65] + z[66];
    z[65]= - z[12]*z[63];
    z[61]=z[65] + n<T>(1,2)*z[61];
    z[61]=z[8]*z[61];
    z[56]= - z[5]*z[56];
    z[65]=n<T>(1,3)*z[7];
    z[66]=n<T>(1,3)*z[53];
    z[68]= - z[65] + z[66] - z[5];
    z[68]=n<T>(1,3)*z[11] + n<T>(1,2)*z[68] + z[55];
    z[68]=z[11]*z[68];
    z[66]= - n<T>(1,6)*z[7] + z[66] + z[5];
    z[66]=z[7]*z[66];
    z[56]=z[68] + z[56] + z[66];
    z[66]=z[53] - z[7];
    z[55]=z[66]*z[55];
    z[55]=z[55] + n<T>(1,2)*z[56];
    z[55]=z[11]*z[55];
    z[55]=z[55] + z[29];
    z[56]=n<T>(1,3)*z[5];
    z[64]= - z[64] + n<T>(5,2)*z[5];
    z[64]=z[64]*z[56];
    z[68]=z[53] - z[4];
    z[69]=z[56] + z[68];
    z[69]=z[4]*z[69];
    z[64]=z[64] + n<T>(1,2)*z[69];
    z[64]=z[4]*z[64];
    z[69]=z[53] + z[56];
    z[69]=2*z[69] - z[4];
    z[69]=z[4]*z[69];
    z[62]=z[62] - z[2];
    z[56]= - z[56] - z[62];
    z[56]=z[2]*z[56];
    z[70]=z[53] + 2*z[5];
    z[70]=z[70]*z[5];
    z[56]=z[56] - n<T>(2,3)*z[70] + z[69];
    z[56]=z[2]*z[56];
    z[50]= - z[50] + z[57] + z[4];
    z[50]=z[2]*z[50];
    z[50]=z[58] - z[50];
    z[57]= - n<T>(37,8)*z[5] - z[51];
    z[57]=z[57]*z[65];
    z[58]=n<T>(37,3)*z[53] - n<T>(7,2)*z[5];
    z[58]=z[5]*z[58];
    z[50]=z[57] + n<T>(1,4)*z[58] - n<T>(1,3)*z[50];
    z[50]=z[7]*z[50];
    z[57]=z[3] + z[12];
    z[57]=z[53]*z[57];
    z[57]=z[21] + z[57];
    z[57]=z[9]*z[57];
    z[58]= - z[12] + z[63];
    z[58]=z[20]*z[58];
    z[57]=z[33] + z[57] + z[58] + z[43] + z[36];
    z[58]= - n<T>(5,12)*z[7] + n<T>(43,6)*z[5] - z[51];
    z[58]=n<T>(7,6)*z[11] - z[8] + n<T>(1,3)*z[12] + n<T>(1,12)*z[3] + n<T>(1,2)*z[58]
    - n<T>(2,3)*z[6];
    z[58]=z[21]*z[58];
    z[63]=z[7] - z[62];
    z[63]=z[18]*z[63];
    z[58]=z[58] + z[63] + z[35] + z[34];
    z[63]=z[6] + z[7];
    z[63]= - z[53]*z[63];
    z[63]= - z[21] + z[63];
    z[63]=z[13]*z[63];
    z[65]= - z[6] + z[66];
    z[65]=z[19]*z[65];
    z[63]=z[63] + z[65];
    z[65]=n<T>(11,2)*z[53] + n<T>(65,3)*z[5];
    z[59]=z[65]*z[59];
    z[65]= - z[5] - z[3];
    z[65]=z[3]*z[65];
    z[65]=z[70] + z[65];
    z[66]=z[5] - z[12];
    z[66]=z[12]*z[66];
    z[65]=n<T>(1,3)*z[65] + n<T>(1,2)*z[66];
    z[65]=z[12]*z[65];
    z[60]= - z[60] + 43*z[5];
    z[52]=n<T>(29,2)*z[6] + z[52] - 7*z[2] + n<T>(1,2)*z[60] + 4*z[4];
    z[52]=n<T>(1,3)*z[52] + z[12];
    z[52]=z[17]*z[52];
    z[60]=z[3] + z[2];
    z[60]= - z[7] - n<T>(4,3)*z[53] + z[4] + n<T>(2,3)*z[60];
    z[60]=z[14]*z[60];
    z[66]=z[5] + z[4];
    z[53]=z[53]*z[66];
    z[53]=z[21] + z[53];
    z[53]=z[10]*z[53];
    z[66]=z[46] - z[28];
    z[69]=z[6] + z[11];
    z[69]=z[31]*z[69];
    z[69]=z[69] - z[37];
    z[70]=4*z[27] - n<T>(31,72)*z[22];
    z[70]=i*z[70];
    z[51]= - n<T>(5,3)*z[12] - n<T>(8,3)*z[5] + z[51];
    z[51]=z[30]*z[51];
    z[62]=z[4] - z[62];
    z[62]=z[15]*z[62];
    z[68]= - z[5] + z[68];
    z[68]=z[16]*z[68];
    z[67]= - z[32]*z[67];
    z[71]=2*i;
    z[71]=z[25]*z[71];

    r += n<T>(5,6)*z[23] + n<T>(1,6)*z[24] - z[26] - n<T>(53,24)*z[38] - z[39] - n<T>(25,3)*z[40]
     + n<T>(7,24)*z[41] - n<T>(7,8)*z[42]
     + z[44]
     + n<T>(5,12)*z[45] - n<T>(1,4)*z[47]
     + n<T>(3,4)*z[48]
     + z[49]
     + z[50]
     + z[51]
     + z[52]
     + z[53]
       + z[54] + n<T>(7,2)*z[55] + z[56] + n<T>(2,3)*z[57] + n<T>(1,3)*z[58] + n<T>(1,12)*z[59]
     + z[60]
     + z[61]
     + z[62]
     + n<T>(5,2)*z[63]
     + z[64]
     + z[65]
       + 2*z[66] + z[67] + z[68] + n<T>(7,6)*z[69] + z[70] + z[71];
 
    return r;
}

template std::complex<double> qg_2lha_tf555(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf555(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
