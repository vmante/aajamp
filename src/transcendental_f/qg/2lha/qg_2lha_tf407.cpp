#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf407(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[166];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[1];
    z[6]=d[2];
    z[7]=d[3];
    z[8]=d[7];
    z[9]=d[9];
    z[10]=e[7];
    z[11]=d[4];
    z[12]=d[8];
    z[13]=d[15];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=e[12];
    z[17]=d[12];
    z[18]=d[17];
    z[19]=e[5];
    z[20]=e[9];
    z[21]=e[13];
    z[22]=e[6];
    z[23]=e[14];
    z[24]=e[0];
    z[25]=e[1];
    z[26]=e[2];
    z[27]=e[4];
    z[28]=e[8];
    z[29]=f[30];
    z[30]=f[31];
    z[31]=f[32];
    z[32]=f[33];
    z[33]=f[35];
    z[34]=f[36];
    z[35]=f[39];
    z[36]=f[43];
    z[37]=f[50];
    z[38]=f[51];
    z[39]=f[52];
    z[40]=f[55];
    z[41]=f[58];
    z[42]=f[68];
    z[43]=c[3];
    z[44]=c[11];
    z[45]=c[15];
    z[46]=c[17];
    z[47]=c[18];
    z[48]=c[19];
    z[49]=c[21];
    z[50]=c[23];
    z[51]=c[25];
    z[52]=c[26];
    z[53]=c[28];
    z[54]=c[29];
    z[55]=c[31];
    z[56]=c[32];
    z[57]=c[39];
    z[58]=c[43];
    z[59]=c[45];
    z[60]=c[46];
    z[61]=c[78];
    z[62]=c[81];
    z[63]=e[3];
    z[64]=e[10];
    z[65]=f[53];
    z[66]=f[0];
    z[67]=f[1];
    z[68]=f[2];
    z[69]=f[3];
    z[70]=f[4];
    z[71]=f[5];
    z[72]=f[10];
    z[73]=f[11];
    z[74]=f[12];
    z[75]=f[13];
    z[76]=f[14];
    z[77]=f[16];
    z[78]=f[17];
    z[79]=f[18];
    z[80]=f[20];
    z[81]=f[21];
    z[82]=g[66];
    z[83]=g[67];
    z[84]=g[84];
    z[85]=g[116];
    z[86]=g[117];
    z[87]=g[133];
    z[88]=g[159];
    z[89]=g[160];
    z[90]=g[162];
    z[91]=g[163];
    z[92]=g[165];
    z[93]=g[167];
    z[94]=g[174];
    z[95]=g[175];
    z[96]=g[178];
    z[97]=g[184];
    z[98]=g[185];
    z[99]=g[186];
    z[100]=g[195];
    z[101]=g[209];
    z[102]=g[221];
    z[103]=n<T>(23,12)*z[12];
    z[104]=n<T>(1,4)*z[2];
    z[105]=z[1]*i;
    z[106]=5*z[5] - z[104] - z[103] - n<T>(11,6)*z[11] - z[7] + n<T>(5,3) - n<T>(3,4)
   *z[105];
    z[107]=n<T>(1,2)*z[5];
    z[106]=z[106]*z[107];
    z[108]=n<T>(1,4)*z[4];
    z[109]=n<T>(1,2)*z[3];
    z[110]= - z[108] - n<T>(5,3) + z[109];
    z[110]=z[4]*z[110];
    z[111]=i*z[3];
    z[112]=z[4]*i;
    z[113]=5*z[111] + z[112];
    z[114]=n<T>(1,2)*z[1];
    z[113]=z[113]*z[114];
    z[115]=5*z[3];
    z[116]=n<T>(3,2)*z[105];
    z[117]=z[116] - z[115] - z[4];
    z[117]=n<T>(1,2)*z[117] - z[7];
    z[117]=z[7]*z[117];
    z[118]=z[105] - z[7];
    z[119]=z[4] - z[3];
    z[120]= - n<T>(47,12)*z[11] + z[119] - n<T>(11,3)*z[118];
    z[121]=n<T>(1,2)*z[11];
    z[120]=z[120]*z[121];
    z[122]=n<T>(1,2)*z[6];
    z[123]=z[122] - z[105];
    z[124]=z[6]*z[123];
    z[125]= - z[12] - z[118];
    z[103]=z[125]*z[103];
    z[125]= - z[2] + z[105] + z[7];
    z[125]=z[125]*z[104];
    z[126]=npow(z[3],2);
    z[103]=z[106] - n<T>(5,8)*z[43] + z[125] + z[103] + n<T>(1,4)*z[124] + 
    z[120] + z[117] + z[113] + n<T>(5,4)*z[126] + z[110];
    z[103]=z[103]*z[107];
    z[106]=n<T>(329,34)*z[105];
    z[107]=n<T>(677,17)*z[4];
    z[110]=n<T>(2029,17)*z[9];
    z[113]=z[110] - z[107] - z[106] + n<T>(329,34)*z[7];
    z[117]=n<T>(1681,17)*z[3];
    z[120]=z[117] + n<T>(73,3);
    z[120]=z[113] - n<T>(1,2)*z[120];
    z[124]=n<T>(1,2)*z[8];
    z[125]= - z[120]*z[124];
    z[127]= - 25 - z[117];
    z[127]=n<T>(1,2)*z[127] + z[113];
    z[127]=z[11]*z[127];
    z[128]=n<T>(5043,17)*z[3];
    z[129]=z[128] + 73;
    z[130]=n<T>(1,4)*z[3];
    z[131]=z[129]*z[130];
    z[132]=n<T>(175,17)*z[3];
    z[133]=z[132] - z[107];
    z[134]= - n<T>(73,6) + z[133];
    z[134]=z[4]*z[134];
    z[135]=n<T>(1,2)*i;
    z[136]= - n<T>(49,3) - n<T>(347,17)*z[3];
    z[136]=z[136]*z[135];
    z[136]=z[136] - n<T>(503,17)*z[112];
    z[136]=z[1]*z[136];
    z[137]=n<T>(987,17)*z[3];
    z[138]=n<T>(25,3) - z[137];
    z[106]=z[106] + n<T>(1,2)*z[138] + n<T>(329,17)*z[4];
    z[138]=n<T>(1,2)*z[7];
    z[106]=z[106]*z[138];
    z[139]=n<T>(3,2)*z[3];
    z[140]=z[139] - z[4];
    z[141]=n<T>(1,2)*z[105];
    z[142]=z[141] - z[140];
    z[142]=z[142]*z[110];
    z[106]=z[127] + z[125] + z[142] + z[106] + z[136] + z[131] + z[134];
    z[106]=z[11]*z[106];
    z[125]=n<T>(1,4)*z[111];
    z[127]=65 + n<T>(11073,34)*z[3];
    z[127]=z[127]*z[125];
    z[131]= - n<T>(61,6) + z[132];
    z[131]=i*z[131];
    z[131]=z[131] - n<T>(677,17)*z[112];
    z[131]=z[4]*z[131];
    z[127]=z[127] + z[131];
    z[127]=z[1]*z[127];
    z[131]=z[117] + 23;
    z[132]=n<T>(3,4)*z[126];
    z[134]=z[131]*z[132];
    z[136]=n<T>(2031,34)*z[126] + z[4];
    z[136]=z[4]*z[136];
    z[134]=z[134] + z[136];
    z[136]= - n<T>(25,3) - z[137];
    z[136]=z[136]*z[135];
    z[136]=z[136] + n<T>(329,17)*z[112];
    z[136]=z[136]*z[114];
    z[137]=1 - n<T>(987,136)*z[3];
    z[137]=z[3]*z[137];
    z[136]=n<T>(25,24)*z[7] + z[136] + z[137] - z[4];
    z[136]=z[7]*z[136];
    z[137]= - n<T>(3,2)*z[111] + z[112];
    z[137]=z[1]*z[137];
    z[132]= - z[132] + z[137];
    z[132]=z[132]*z[110];
    z[127]=z[132] + z[136] + n<T>(1,2)*z[134] + z[127];
    z[132]=n<T>(1,8)*z[8];
    z[134]= - z[120]*z[132];
    z[136]=z[140] + z[141];
    z[137]=n<T>(2029,34)*z[9] + n<T>(329,68)*z[7];
    z[136]=z[136]*z[137];
    z[133]=n<T>(67,6) - z[133];
    z[133]=z[4]*z[133];
    z[131]=z[3]*z[131];
    z[131]= - n<T>(3,4)*z[131] + z[133];
    z[133]= - n<T>(73,12) - n<T>(667,17)*z[3];
    z[133]=z[133]*z[135];
    z[133]=z[133] - n<T>(87,17)*z[112];
    z[133]=z[1]*z[133];
    z[131]=z[134] + n<T>(1,2)*z[131] + z[133] + z[136];
    z[131]=z[8]*z[131];
    z[106]=n<T>(1,4)*z[106] + n<T>(1,2)*z[127] + z[131];
    z[106]=z[106]*z[121];
    z[127]=31*z[9];
    z[131]=z[119]*z[127];
    z[133]=n<T>(739,17)*z[3];
    z[134]= - n<T>(1,2) + z[133];
    z[134]=z[134]*z[139];
    z[136]=n<T>(3827,17)*z[3];
    z[137]=z[136] - n<T>(3,2);
    z[140]=n<T>(805,17)*z[4];
    z[141]= - n<T>(1,2)*z[137] + z[140];
    z[141]=z[4]*z[141];
    z[142]=n<T>(631,17)*z[112];
    z[143]=n<T>(631,17)*z[3];
    z[144]= - n<T>(5,3) - z[143];
    z[144]=i*z[144];
    z[144]=z[144] + z[142];
    z[144]=z[144]*z[114];
    z[145]= - z[143] + n<T>(631,17)*z[4];
    z[146]= - n<T>(5,6) - z[145];
    z[146]=z[146]*z[138];
    z[147]=n<T>(5,12)*z[8];
    z[134]=z[147] - z[131] + z[146] + z[144] + z[134] + z[141];
    z[134]=z[134]*z[124];
    z[141]=n<T>(2217,17)*z[3];
    z[144]=z[141] - n<T>(1,2);
    z[146]= - z[144]*z[109];
    z[136]= - n<T>(1,2) + z[136];
    z[136]=n<T>(1,2)*z[136] - z[140];
    z[136]=z[4]*z[136];
    z[148]=n<T>(11,2) + z[143];
    z[148]=i*z[148];
    z[148]=z[148] - z[142];
    z[148]=z[148]*z[114];
    z[149]=1 + z[145];
    z[149]=z[149]*z[138];
    z[131]= - n<T>(13,12)*z[2] - n<T>(5,12)*z[6] + z[147] + z[131] + z[149] + 
    z[148] + z[146] + z[136];
    z[131]=z[131]*z[104];
    z[136]= - 1 - z[143];
    z[136]=i*z[136];
    z[136]=z[136] + z[142];
    z[136]=z[136]*z[114];
    z[142]= - n<T>(1,2) + n<T>(191,17)*z[3];
    z[142]=z[3]*z[142];
    z[143]=47*z[3];
    z[146]=n<T>(2241,68)*z[4] + n<T>(5,8) - z[143];
    z[146]=z[4]*z[146];
    z[147]=n<T>(1,2) - z[145];
    z[147]=z[7]*z[147];
    z[136]=n<T>(1,4)*z[147] + z[136] + n<T>(5,4)*z[142] + z[146];
    z[136]=z[7]*z[136];
    z[142]=n<T>(1,2)*z[4];
    z[146]=z[3] + z[4];
    z[146]=z[146]*z[142];
    z[147]= - z[111] + z[112];
    z[147]=z[147]*z[114];
    z[148]= - z[119]*z[138];
    z[146]=z[148] + z[147] - z[126] + z[146];
    z[146]=z[146]*z[127];
    z[147]= - n<T>(5,2) + n<T>(5437,17)*z[3];
    z[147]=z[147]*z[109];
    z[148]= - 1 + n<T>(607,17)*z[3];
    z[148]=n<T>(1,2)*z[148] - z[140];
    z[148]=z[4]*z[148];
    z[147]=z[147] + z[148];
    z[147]=z[4]*z[147];
    z[141]=n<T>(7,4) - z[141];
    z[141]=z[141]*z[126];
    z[141]=z[141] + z[147];
    z[147]=n<T>(7,2) - n<T>(955,17)*z[3];
    z[147]=z[147]*z[125];
    z[143]= - n<T>(7,8) + z[143];
    z[143]=i*z[143];
    z[143]=z[143] - n<T>(2241,68)*z[112];
    z[143]=z[4]*z[143];
    z[143]=z[147] + z[143];
    z[143]=z[1]*z[143];
    z[147]=npow(z[11],2);
    z[148]=n<T>(11,2)*z[6] - 11*z[105] + 5*z[7];
    z[148]=z[6]*z[148];
    z[131]=z[131] + n<T>(1,24)*z[148] - n<T>(1,16)*z[147] + z[134] + z[146] + 
    z[136] + n<T>(1,2)*z[141] + z[143];
    z[131]=z[2]*z[131];
    z[134]=n<T>(4137,17)*z[9];
    z[136]=z[134] + n<T>(2853,34)*z[7];
    z[141]= - n<T>(91,3) + n<T>(7187,17)*z[3];
    z[143]= - n<T>(1,2)*z[141] + n<T>(3897,17)*z[4];
    z[146]=n<T>(2853,34)*z[105] + z[143] - z[136];
    z[146]=z[8]*z[146];
    z[147]=z[105] - z[4];
    z[136]=z[147]*z[136];
    z[147]=i*z[141];
    z[147]=z[147] - n<T>(4941,17)*z[112];
    z[147]=z[147]*z[114];
    z[143]=z[4]*z[143];
    z[136]=n<T>(2853,34)*z[43] + z[146] + z[143] + z[147] + z[136];
    z[136]=z[20]*z[136];
    z[143]=1 - n<T>(5085,68)*z[3];
    z[115]=z[143]*z[115];
    z[115]= - n<T>(13,6) + z[115];
    z[115]=z[3]*z[115];
    z[143]=23 - n<T>(5095,17)*z[3];
    z[143]=z[143]*z[139];
    z[146]= - n<T>(19,2) + n<T>(18367,17)*z[3];
    z[146]=n<T>(1,3)*z[146] + n<T>(23,17)*z[4];
    z[146]=z[4]*z[146];
    z[143]=z[143] + z[146];
    z[143]=z[143]*z[142];
    z[115]=z[115] + z[143];
    z[115]=z[4]*z[115];
    z[143]=z[141]*z[135];
    z[143]= - z[143] + n<T>(3897,17)*z[112];
    z[143]=z[143]*z[1];
    z[146]=z[134]*z[105];
    z[147]=z[105]*z[7];
    z[143]=z[143] - z[146] - n<T>(2853,34)*z[147];
    z[146]=z[8] + z[4];
    z[143]=z[143]*z[146];
    z[141]= - n<T>(2853,17)*z[7] + n<T>(4941,17)*z[4] - z[141];
    z[134]= - n<T>(2853,34)*z[8] + n<T>(1,2)*z[141] - z[134];
    z[134]=z[43]*z[134];
    z[134]=z[134] + z[143];
    z[134]=z[18]*z[134];
    z[141]=z[11] + z[4];
    z[141]=z[22]*z[120]*z[141];
    z[113]=n<T>(35,3) + n<T>(1681,34)*z[3] - z[113];
    z[113]=z[29]*z[113];
    z[143]= - 259 - n<T>(3841,34)*z[3];
    z[143]=z[3]*z[143];
    z[143]=79 + z[143];
    z[143]=z[143]*z[126];
    z[146]= - n<T>(273,17)*z[9] + n<T>(927,17)*z[4] + n<T>(37,4) - n<T>(4155,17)*z[3]
     + 
   n<T>(729,17)*z[118];
    z[146]=z[31]*z[146];
    z[113]=z[141] + z[113] + z[146] + z[66] + z[60] + z[136] + z[134] + 
   n<T>(1,12)*z[143] + z[115];
    z[115]=n<T>(3,4) + n<T>(13,17)*z[3];
    z[134]=n<T>(1605,17)*z[4];
    z[136]=z[115] + z[134];
    z[141]=z[3]*z[136];
    z[143]= - n<T>(439,2)*z[111] + 1605*z[112];
    z[143]=z[1]*z[143];
    z[143]=n<T>(3,2)*z[141] + n<T>(1,17)*z[143];
    z[146]=n<T>(2319,34)*z[9] + n<T>(155,34)*z[7];
    z[148]= - z[146] + n<T>(1237,17)*z[105];
    z[149]= - n<T>(3,2) + n<T>(6931,17)*z[3];
    z[150]=n<T>(1,2)*z[149];
    z[151]=z[150] - z[134];
    z[151]=n<T>(1,2)*z[151] + z[148];
    z[151]=z[9]*z[151];
    z[152]= - n<T>(1,3) + n<T>(465,68)*z[3];
    z[153]=n<T>(155,34)*z[105];
    z[154]=z[153] + z[152];
    z[154]=z[7]*z[154];
    z[146]= - n<T>(1605,34)*z[4] - n<T>(13,34)*z[3] - z[146] + z[153] - n<T>(1,3);
    z[146]=z[6]*z[146];
    z[143]=z[146] - z[132] + z[151] + n<T>(1,2)*z[143] + z[154];
    z[143]=z[143]*z[122];
    z[146]=z[136]*z[126];
    z[146]=n<T>(1,2)*z[146];
    z[151]=n<T>(1,2)*z[111];
    z[153]= - n<T>(3,2) - n<T>(181,17)*z[3];
    z[153]=z[153]*z[151];
    z[154]=n<T>(1605,17)*z[112];
    z[155]= - z[3]*z[154];
    z[153]=z[153] + z[155];
    z[153]=z[1]*z[153];
    z[153]=z[146] + z[153];
    z[152]= - z[152]*z[105];
    z[152]= - n<T>(1,6)*z[7] + n<T>(465,136)*z[126] + z[152];
    z[152]=z[7]*z[152];
    z[155]=n<T>(1,2)*z[126];
    z[156]= - z[3]*z[105];
    z[156]=z[155] + z[156];
    z[156]=z[9]*z[156];
    z[124]=z[124] - z[105];
    z[157]=z[7] - z[124];
    z[157]=z[157]*z[132];
    z[158]= - z[105] + z[121];
    z[158]=z[11]*z[158];
    z[143]=z[143] + n<T>(1,8)*z[158] + z[157] + n<T>(6957,68)*z[156] + n<T>(3,4)*
    z[153] + z[152];
    z[143]=z[6]*z[143];
    z[152]=n<T>(155,17)*z[7];
    z[134]=z[152] + z[134];
    z[153]=n<T>(2319,17)*z[9];
    z[156]=z[134] + z[153];
    z[157]=n<T>(155,17)*z[105];
    z[158]= - z[156] + z[157] - z[115];
    z[159]=n<T>(1,8)*z[6];
    z[160]= - z[158]*z[159];
    z[161]= - n<T>(3,16) + n<T>(113,17)*z[3];
    z[161]=i*z[161];
    z[161]=z[161] - n<T>(1605,68)*z[112];
    z[161]=z[1]*z[161];
    z[162]=3*z[3];
    z[163]= - z[162] - z[105];
    z[163]=z[7]*z[163];
    z[150]=z[150] - z[156];
    z[164]= - n<T>(2629,34)*z[105] - z[150];
    z[165]=n<T>(1,2)*z[9];
    z[164]=z[164]*z[165];
    z[160]=z[160] + z[164] + n<T>(155,68)*z[163] - n<T>(3,4)*z[141] + z[161];
    z[160]=z[6]*z[160];
    z[159]=n<T>(1,4)*z[12] - z[159];
    z[159]=z[158]*z[159];
    z[161]= - n<T>(1,3) - n<T>(239,68)*z[3];
    z[161]=i*z[161];
    z[161]=z[161] - n<T>(1605,136)*z[112];
    z[161]=z[1]*z[161];
    z[163]=n<T>(465,17)*z[3];
    z[157]= - z[157] + n<T>(23,12) + z[163];
    z[164]=n<T>(1,8)*z[7];
    z[157]=z[157]*z[164];
    z[150]= - n<T>(2009,34)*z[105] + z[150];
    z[150]=z[9]*z[150];
    z[141]=n<T>(23,32)*z[11] + n<T>(1,4)*z[150] + z[157] + n<T>(3,8)*z[141] + z[161]
    + z[159];
    z[141]=z[12]*z[141];
    z[150]=n<T>(1,2) - n<T>(43,17)*z[3];
    z[150]=z[150]*z[151];
    z[157]=z[3]*z[112];
    z[150]=z[150] + n<T>(535,17)*z[157];
    z[150]=z[1]*z[150];
    z[146]=z[146] + 3*z[150];
    z[150]= - n<T>(23,12) + z[163];
    z[150]=z[150]*z[105];
    z[150]=n<T>(23,24)*z[7] + n<T>(465,34)*z[126] + z[150];
    z[150]=z[7]*z[150];
    z[146]=3*z[146] + z[150];
    z[150]=z[153]*z[105];
    z[152]=z[152]*z[105];
    z[150]=z[150] + z[152];
    z[149]=z[149]*z[135];
    z[149]=z[149] - z[154];
    z[149]=z[1]*z[149];
    z[149]=n<T>(6957,68)*z[126] + z[149] - z[150];
    z[149]=z[9]*z[149];
    z[153]=z[121] + z[118];
    z[153]=z[11]*z[153];
    z[146]=n<T>(23,8)*z[153] + n<T>(1,2)*z[146] + z[149];
    z[141]=z[141] + n<T>(1,2)*z[146] + z[160];
    z[141]=z[12]*z[141];
    z[146]=n<T>(15847,204)*z[4] + n<T>(11,2) - n<T>(23323,51)*z[3];
    z[146]=z[146]*z[142];
    z[149]=n<T>(23,6) - n<T>(3435,17)*z[3];
    z[149]=n<T>(16733,204)*z[9] - n<T>(2869,102)*z[7] + n<T>(1,2)*z[149] + n<T>(3635,51)
   *z[4];
    z[149]=z[149]*z[165];
    z[153]=35 + n<T>(29023,17)*z[3];
    z[153]= - n<T>(1379,17)*z[9] - n<T>(951,34)*z[7] + n<T>(1,6)*z[153] + n<T>(641,17)*
    z[4];
    z[132]=z[153]*z[132];
    z[153]= - n<T>(389,6) - n<T>(6323,17)*z[3];
    z[153]=n<T>(329,17)*z[7] + n<T>(1,2)*z[153] - n<T>(367,17)*z[4];
    z[110]=n<T>(1,2)*z[153] + z[110];
    z[110]=n<T>(329,136)*z[11] + n<T>(1,3)*z[110] + n<T>(329,68)*z[8];
    z[110]=z[110]*z[121];
    z[153]= - n<T>(47,36) - n<T>(917,17)*z[3] + z[156];
    z[153]=n<T>(1,2)*z[153] + n<T>(155,17)*z[6];
    z[153]=z[153]*z[122];
    z[156]= - 29 + n<T>(66575,272)*z[3];
    z[156]=z[3]*z[156];
    z[157]=n<T>(7,8) + n<T>(1025,17)*z[3];
    z[157]= - n<T>(3345,34)*z[7] + n<T>(1,2)*z[157] + n<T>(245,17)*z[4];
    z[157]=z[7]*z[157];
    z[110]=z[153] + z[110] + z[132] + z[149] + z[157] + n<T>(1,3)*z[156] + 
    z[146];
    z[132]=n<T>(1,3) + z[145];
    z[104]=z[132]*z[104];
    z[132]= - n<T>(4,3) + n<T>(1369,68)*z[3];
    z[132]= - n<T>(155,136)*z[12] - n<T>(155,68)*z[6] - n<T>(464,17)*z[9] - n<T>(155,102)
   *z[7] + n<T>(1,3)*z[132] - n<T>(535,34)*z[4];
    z[132]=z[12]*z[132];
    z[104]=z[104] + n<T>(1,2)*z[110] + z[132];
    z[104]=z[43]*z[104];
    z[110]=n<T>(631,34)*z[7];
    z[132]= - z[127] + z[140] - z[110] + n<T>(631,34)*z[105];
    z[133]= - 1 + z[133];
    z[133]=n<T>(3,2)*z[133] - z[132];
    z[133]=z[8]*z[133];
    z[140]=1 - n<T>(2217,34)*z[3] + z[132];
    z[140]=z[2]*z[140];
    z[146]= - n<T>(29,4) + n<T>(343,17)*z[3];
    z[146]=z[146]*z[162];
    z[149]= - 75 + n<T>(4133,17)*z[3];
    z[149]=n<T>(1,2)*z[149] - n<T>(3421,17)*z[4];
    z[142]=z[149]*z[142];
    z[142]=z[146] + z[142];
    z[146]= - n<T>(1,2) + n<T>(479,17)*z[3];
    z[110]=z[110] - n<T>(631,17)*z[105] + 5*z[146] + n<T>(11,136)*z[4];
    z[110]=z[7]*z[110];
    z[146]=31*z[7];
    z[149]=97*z[3] + n<T>(269,4)*z[4];
    z[149]=z[146] + n<T>(25,17)*z[149] - 31*z[105];
    z[149]=z[9]*z[149];
    z[153]=3 - n<T>(2395,17)*z[3];
    z[153]=i*z[153];
    z[153]=z[153] - n<T>(11,136)*z[112];
    z[153]=z[1]*z[153];
    z[110]= - n<T>(2673,68)*z[10] - n<T>(1367,68)*z[43] + z[140] - z[121] + 
    z[133] + z[149] + z[110] + n<T>(1,2)*z[142] + z[153];
    z[110]=z[10]*z[110];
    z[121]=n<T>(1,2)*z[144] - z[132];
    z[121]=z[36]*z[121];
    z[132]= - n<T>(329,51)*z[11] + n<T>(951,68)*z[8] - n<T>(8387,102)*z[9] + n<T>(65221,204)*z[7]
     - n<T>(5,9)
     - n<T>(10623,68)*z[3];
    z[132]=i*z[132];
    z[133]=n<T>(155,17)*i;
    z[140]= - z[6]*z[133];
    z[132]=z[140] + n<T>(209,6)*z[112] + z[132];
    z[140]=n<T>(2853,68)*z[18] - n<T>(691,34)*z[17] + n<T>(155,51)*z[12];
    z[140]=i*z[140];
    z[133]= - z[14]*z[133];
    z[132]=z[133] + n<T>(1,4)*z[132] + z[140];
    z[132]=z[44]*z[132];
    z[133]=17*z[9] + n<T>(439,34)*z[7];
    z[140]=n<T>(169,34)*z[3] + 5*z[4];
    z[140]=3*z[140] + n<T>(439,34)*z[105] - z[133];
    z[140]=z[42]*z[140];
    z[142]= - n<T>(2217,2)*z[3] + 805*z[4] + n<T>(631,2)*z[118];
    z[127]=n<T>(1,17)*z[142] - z[127];
    z[127]=z[33]*z[127];
    z[142]=z[5]*z[105];
    z[142]=z[142] + z[147] + z[43];
    z[142]=z[13]*z[142];
    z[144]=z[5] - z[118];
    z[144]=z[26]*z[144];
    z[121]=z[132] + z[121] + z[127] + z[140] + z[142] + z[144];
    z[127]=n<T>(11,4) + n<T>(515,17)*z[3];
    z[127]=z[127]*z[109];
    z[132]=n<T>(1,17)*z[4];
    z[140]= - 361*z[3] - n<T>(73,4)*z[4];
    z[140]=z[140]*z[132];
    z[142]=n<T>(11,4) - n<T>(1103,17)*z[3];
    z[142]=z[142]*z[135];
    z[142]=z[142] + n<T>(350,17)*z[112];
    z[142]=z[1]*z[142];
    z[144]=n<T>(7,17)*z[4] - n<T>(11,8) + n<T>(373,17)*z[3];
    z[144]= - n<T>(29,102)*z[7] + n<T>(1,2)*z[144] - n<T>(62,17)*z[105];
    z[144]=z[7]*z[144];
    z[127]=z[144] + z[142] + z[127] + z[140];
    z[127]=z[7]*z[127];
    z[140]= - n<T>(9,4) + n<T>(2651,17)*z[3];
    z[130]=z[140]*z[130];
    z[140]=n<T>(3,17)*z[4];
    z[142]=n<T>(1287,4)*z[3] - 113*z[4];
    z[142]=z[142]*z[140];
    z[144]=n<T>(9,16) + n<T>(208,17)*z[3];
    z[149]= - i*z[144];
    z[149]=z[149] - n<T>(171,68)*z[112];
    z[149]=z[1]*z[149];
    z[144]= - n<T>(185,17)*z[7] + n<T>(383,34)*z[105] + n<T>(171,68)*z[4] + z[144];
    z[144]=z[7]*z[144];
    z[153]= - n<T>(3533,4)*z[9] + n<T>(4303,4)*z[3] - 556*z[4] + n<T>(175,12)*z[118]
   ;
    z[153]=z[9]*z[153];
    z[130]=n<T>(1,17)*z[153] + z[144] + z[149] + z[130] + z[142];
    z[130]=z[9]*z[130];
    z[142]= - n<T>(11,2) - n<T>(19183,51)*z[3];
    z[142]=z[142]*z[155];
    z[144]=n<T>(487,2)*z[3] + n<T>(815,3)*z[4];
    z[144]=z[4]*z[144];
    z[144]= - n<T>(6061,2)*z[126] + z[144];
    z[144]=z[144]*z[132];
    z[142]=z[142] + z[144];
    z[144]= - 11 - n<T>(3515,34)*z[3];
    z[125]=z[144]*z[125];
    z[144]=195*z[111] + n<T>(4283,4)*z[112];
    z[132]=z[144]*z[132];
    z[125]=z[125] + z[132];
    z[125]=z[1]*z[125];
    z[125]=n<T>(1,2)*z[142] + z[125];
    z[125]=z[130] + n<T>(1,2)*z[125] + z[127];
    z[125]=z[9]*z[125];
    z[127]=1 - n<T>(6241,68)*z[3];
    z[127]=z[127]*z[139];
    z[130]=n<T>(1591,17)*z[112];
    z[132]=n<T>(7,3) + n<T>(8559,34)*z[3];
    z[132]=i*z[132];
    z[132]=z[132] - z[130];
    z[132]=z[132]*z[114];
    z[137]= - n<T>(6111,68)*z[4] + z[137];
    z[137]=z[4]*z[137];
    z[139]=n<T>(1,6) + z[145];
    z[139]=z[7]*z[139];
    z[127]=z[139] + z[132] + z[127] + z[137];
    z[127]=z[127]*z[138];
    z[132]= - 31 + n<T>(8791,34)*z[3];
    z[132]=z[132]*z[111];
    z[137]=n<T>(17,6) - n<T>(679,17)*z[3];
    z[137]=i*z[137];
    z[137]=5*z[137] + n<T>(2379,68)*z[112];
    z[137]=z[4]*z[137];
    z[132]=n<T>(3,4)*z[132] + z[137];
    z[132]=z[132]*z[114];
    z[137]=npow(z[4],2);
    z[137]= - n<T>(3,2)*z[126] + z[137];
    z[139]=n<T>(10303,2)*z[111] - 2029*z[112];
    z[139]=z[1]*z[139];
    z[137]=n<T>(2029,2)*z[137] + z[139];
    z[119]=z[119]*z[146];
    z[119]=n<T>(1,34)*z[137] + z[119];
    z[119]=z[9]*z[119];
    z[137]=n<T>(1,16)*z[126];
    z[128]=61 + z[128];
    z[128]=z[128]*z[137];
    z[117]= - n<T>(79,3) - z[117];
    z[107]=n<T>(1,2)*z[117] - z[107];
    z[107]=z[107]*z[108];
    z[108]=1 + n<T>(2031,136)*z[3];
    z[108]=z[3]*z[108];
    z[107]=z[108] + z[107];
    z[107]=z[4]*z[107];
    z[107]=z[119] + z[127] + z[132] + z[128] + z[107];
    z[108]=n<T>(355,4)*z[111] - z[130];
    z[108]=z[1]*z[108];
    z[117]= - n<T>(7,3) - n<T>(355,2)*z[3];
    z[117]=n<T>(1,2)*z[117] + n<T>(1591,17)*z[4];
    z[117]=z[117]*z[164];
    z[119]=31 - n<T>(4231,17)*z[3];
    z[119]=z[3]*z[119];
    z[127]= - n<T>(2287,68)*z[4] - n<T>(11,3) + n<T>(13977,272)*z[3];
    z[127]=z[4]*z[127];
    z[128]= - n<T>(10303,4)*z[3] + 3083*z[4];
    z[128]=z[9]*z[128];
    z[108]=n<T>(1,68)*z[128] + z[117] + n<T>(1,8)*z[108] + n<T>(3,32)*z[119] + 
    z[127];
    z[108]=z[8]*z[108];
    z[107]=n<T>(1,2)*z[107] + z[108];
    z[107]=z[8]*z[107];
    z[108]=n<T>(7,2)*z[9];
    z[117]= - z[109] + z[4];
    z[116]= - z[108] - n<T>(3,2)*z[7] + 7*z[117] + z[116];
    z[117]=3*z[9];
    z[116]=z[116]*z[117];
    z[119]=3*z[4];
    z[127]=n<T>(287,68)*z[3] - z[119];
    z[127]=z[127]*z[119];
    z[128]= - n<T>(7,2) + n<T>(2427,17)*z[3];
    z[130]=z[128]*i;
    z[130]= - z[130] + n<T>(3699,34)*z[112];
    z[132]= - z[130]*z[114];
    z[128]= - z[128] + n<T>(3699,34)*z[4];
    z[128]=n<T>(1,2)*z[128];
    z[139]= - n<T>(1821,136)*z[7] + z[128] + n<T>(75,17)*z[105];
    z[139]=z[7]*z[139];
    z[142]= - 7 + n<T>(8091,34)*z[3];
    z[142]=z[142]*z[3];
    z[142]=z[142] + n<T>(11,2);
    z[142]=n<T>(1,4)*z[142];
    z[144]=z[17]*z[105];
    z[116]= - n<T>(1521,136)*z[19] + n<T>(1521,68)*z[144] + n<T>(691,68)*z[43] + 
    z[116] + z[139] + z[132] - z[142] + z[127];
    z[116]=z[19]*z[116];
    z[127]=1 - n<T>(32881,136)*z[3];
    z[127]=z[127]*z[111];
    z[132]=n<T>(13,2) - n<T>(929,17)*z[3];
    z[132]=i*z[132];
    z[132]=z[132] + n<T>(8085,68)*z[112];
    z[132]=z[4]*z[132];
    z[127]=z[127] + z[132];
    z[127]=z[1]*z[127];
    z[132]= - 17 + n<T>(55591,102)*z[3];
    z[132]=z[132]*z[155];
    z[139]=17 - n<T>(24521,34)*z[3];
    z[109]=z[139]*z[109];
    z[139]= - 35 + n<T>(9243,17)*z[3];
    z[139]=n<T>(1,4)*z[139] + n<T>(807,17)*z[4];
    z[139]=z[4]*z[139];
    z[109]=z[109] + z[139];
    z[109]=z[4]*z[109];
    z[109]=z[127] + z[132] + z[109];
    z[127]= - n<T>(1,2) + n<T>(185,17)*z[3];
    z[127]=i*z[127];
    z[127]=3*z[127] - n<T>(1163,68)*z[112];
    z[114]=z[127]*z[114];
    z[127]=7 + n<T>(2435,34)*z[3];
    z[127]=z[3]*z[127];
    z[127]= - n<T>(11,2) + z[127];
    z[132]= - n<T>(15,2) + n<T>(1159,17)*z[3];
    z[132]=n<T>(1,8)*z[132] - n<T>(240,17)*z[4];
    z[132]=z[4]*z[132];
    z[139]=n<T>(2279,544)*z[7] - n<T>(1025,34)*z[105] + n<T>(8561,408)*z[4] + n<T>(5,6)
    - n<T>(497,17)*z[3];
    z[139]=z[7]*z[139];
    z[114]=z[139] + z[114] + n<T>(1,8)*z[127] + z[132];
    z[114]=z[7]*z[114];
    z[109]=n<T>(1,4)*z[109] + z[114];
    z[109]=z[7]*z[109];
    z[114]=z[115]*i;
    z[114]=z[114] + z[154];
    z[114]=z[114]*z[1];
    z[127]=z[114] + z[152];
    z[132]= - n<T>(1,2)*z[136] + z[148];
    z[132]=z[9]*z[132];
    z[122]=z[158]*z[122];
    z[122]=n<T>(155,34)*z[43] + z[122] + n<T>(1,2)*z[127] + z[132];
    z[122]=z[16]*z[122];
    z[120]= - z[34]*z[120];
    z[127]=97 - n<T>(30429,17)*z[3];
    z[127]= - n<T>(14519,17)*z[9] + n<T>(1,2)*z[127] + n<T>(14911,17)*z[4] + n<T>(11083,34)*z[118];
    z[127]=z[30]*z[127];
    z[129]= - n<T>(6087,17)*z[9] + n<T>(1,2)*z[129] + n<T>(2031,17)*z[4] + n<T>(987,34)*
    z[118];
    z[129]=z[35]*z[129];
    z[132]=n<T>(797,6) - n<T>(29109,17)*z[3];
    z[132]=n<T>(153181,51)*z[9] + n<T>(128857,34)*z[7] + n<T>(1,2)*z[132] - n<T>(14203,51)*z[4];
    z[132]=z[55]*z[132];
    z[120]=z[129] + z[132] - z[78] - z[76] + z[67] + z[127] - z[120];
    z[127]=z[151] - z[112];
    z[127]=z[1]*z[127];
    z[108]=z[105]*z[108];
    z[108]=z[108] + 7*z[127] + n<T>(3,2)*z[147];
    z[108]=z[108]*z[117];
    z[117]= - n<T>(287,68)*z[111] + 3*z[112];
    z[117]=z[117]*z[119];
    z[119]=i*z[142];
    z[117]=z[119] + z[117];
    z[117]=z[1]*z[117];
    z[119]= - z[1]*z[130];
    z[119]=z[119] + n<T>(1821,68)*z[147];
    z[119]=z[119]*z[138];
    z[127]=n<T>(1521,136)*z[17] + n<T>(9,2)*z[9] - z[128] + n<T>(75,17)*z[7];
    z[127]=z[43]*z[127];
    z[108]=z[108] + z[117] + z[119] + z[127];
    z[108]=z[17]*z[108];
    z[117]=159*z[111] - 110*z[112];
    z[117]=z[1]*z[117];
    z[119]=z[105] + z[4];
    z[119]= - 55*z[7] - 159*z[3] + 110*z[119];
    z[119]=z[7]*z[119];
    z[117]=z[117] + z[119];
    z[119]= - 721*z[3] - 165*z[4];
    z[119]=z[119]*z[140];
    z[127]= - 3862*z[3] - 2163*z[4] + 477*z[118];
    z[127]=n<T>(1,17)*z[127] - n<T>(325,2)*z[9];
    z[127]=z[9]*z[127];
    z[117]=n<T>(537,34)*z[21] + n<T>(97,17)*z[43] + z[127] + n<T>(29,2)*z[126] + 
    z[119] + n<T>(3,17)*z[117];
    z[117]=z[21]*z[117];
    z[119]=z[6] + z[11];
    z[119]=z[105]*z[119];
    z[119]=z[43] + z[119];
    z[119]=z[15]*z[119];
    z[126]= - n<T>(295,6) - n<T>(2209,17)*z[3];
    z[126]= - n<T>(1461,17)*z[9] + n<T>(3365,34)*z[7] + n<T>(1,2)*z[126] - n<T>(10521,17)
   *z[4];
    z[126]=z[45]*z[126];
    z[105]=z[105] - z[6];
    z[127]= - z[11] + z[105];
    z[127]=z[27]*z[127];
    z[119]=z[127] + z[81] - z[80] + z[53] - z[47] + z[126] + z[119];
    z[126]=z[41] + z[38];
    z[127]=z[9] + z[12];
    z[127]=z[23]*z[127];
    z[126]=n<T>(1,2)*z[127] + n<T>(1,8)*z[40] - n<T>(1,2)*z[37] - n<T>(3,8)*z[126];
    z[126]=z[158]*z[126];
    z[127]= - 2 - n<T>(1111,34)*z[3];
    z[127]=z[127]*z[111];
    z[128]=n<T>(7,24) + n<T>(274,17)*z[3];
    z[128]=i*z[128];
    z[128]=z[128] - n<T>(5511,136)*z[112];
    z[128]=z[4]*z[128];
    z[127]=z[127] + z[128];
    z[127]=z[4]*z[127];
    z[128]= - 15 - n<T>(20363,102)*z[3];
    z[128]=i*z[128]*z[137];
    z[127]=z[128] + z[127];
    z[127]=z[1]*z[127];
    z[114]=z[114] + z[150];
    z[128]=z[9] + z[6];
    z[114]=z[114]*z[128];
    z[115]=z[134] + z[115];
    z[115]=n<T>(155,34)*z[6] + n<T>(1,2)*z[115] + n<T>(1237,17)*z[9];
    z[115]=z[43]*z[115];
    z[114]=n<T>(1,2)*z[114] + z[115];
    z[114]=z[14]*z[114];
    z[111]=n<T>(169,34)*z[111] + 5*z[112];
    z[112]= - i*z[133];
    z[111]=3*z[111] + z[112];
    z[111]=z[46]*z[111];
    z[112]=n<T>(159,17)*z[9] + n<T>(179,17)*z[4] - n<T>(3,4) - n<T>(115,17)*z[3]
     - n<T>(261,17)*z[118];
    z[112]=z[39]*z[112];
    z[111]= - z[89] + z[112] + z[111];
    z[112]=n<T>(1,2)*z[2];
    z[115]=z[112] + z[123];
    z[115]=z[24]*z[115];
    z[115]= - z[69] + z[115] - z[75];
    z[123]=1 - n<T>(523,68)*z[3];
    z[118]= - n<T>(299,2)*z[9] + 9*z[123] + n<T>(1451,34)*z[4] + n<T>(6365,68)*
    z[118];
    z[118]=z[32]*z[118];
    z[112]= - z[112] - z[124];
    z[112]=z[28]*z[112];
    z[123]=z[73] + z[71];
    z[124]=z[12] + z[5];
    z[124]=z[64]*z[124];
    z[124]=z[124] - z[72];
    z[128]= - n<T>(21,2)*z[62] + z[59] - n<T>(2345,272)*z[57] - n<T>(5,8)*z[50] - n<T>(1,8)*z[49];
    z[128]=i*z[128];
    z[129]=z[5] + z[11];
    z[129]=1 + n<T>(5,4)*z[129];
    z[129]=z[63]*z[129];
    z[130]=89*z[3] + 330*z[4];
    z[130]=n<T>(1,17)*z[130] + 53*z[9];
    z[130]=z[65]*z[130];
    z[105]=z[25]*z[105];
    z[132]= - z[52]*z[135];

    r +=  - n<T>(1,12)*z[48] + n<T>(13,48)*z[51] + n<T>(3,32)*z[54] - n<T>(94973,32640)
      *z[56] - n<T>(1249,136)*z[58] + 6*z[61] + n<T>(11,3)*z[68] + n<T>(23,96)*
      z[70] - n<T>(25,96)*z[74] + n<T>(23,32)*z[77] - n<T>(1,24)*z[79] - n<T>(1119,68)*
      z[82] + n<T>(81,17)*z[83] - n<T>(109,17)*z[84] + n<T>(141,34)*z[85] - n<T>(29,17)
      *z[86] - n<T>(83,17)*z[87] - n<T>(849,34)*z[88] - n<T>(2417,68)*z[90] - n<T>(36,17)*z[91]
     + n<T>(681,34)*z[92]
     + n<T>(427,34)*z[93] - n<T>(317,34)*z[94] - 
      n<T>(393,68)*z[95] - n<T>(41,34)*z[96] - n<T>(86,17)*z[97] - n<T>(165,68)*z[98]
       + n<T>(783,68)*z[99] - n<T>(3,17)*z[100] + n<T>(220,17)*z[101] - n<T>(9,4)*
      z[102] + z[103] + z[104] + n<T>(3,8)*z[105] + z[106] + z[107] + 
      z[108] + z[109] + z[110] + n<T>(3,4)*z[111] + n<T>(5,12)*z[112] + n<T>(1,4)*
      z[113] + z[114] + n<T>(1,6)*z[115] + z[116] + z[117] + z[118] + n<T>(1,8)
      *z[119] + n<T>(1,16)*z[120] + n<T>(1,2)*z[121] + z[122] - n<T>(7,48)*z[123]
       + n<T>(23,24)*z[124] + z[125] + z[126] + z[127] + z[128] + n<T>(5,6)*
      z[129] + 4*z[130] + z[131] + z[132] + z[141] + z[143];
 
    return r;
}

template std::complex<double> qg_2lha_tf407(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf407(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
