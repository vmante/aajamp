#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf480(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=c[13];
    z[4]=c[20];
    z[5]=c[23];
    z[6]=f[42];
    z[7]=f[44];
    z[8]=f[62];
    z[9]=f[77];
    z[10]=f[81];
    z[11]=n<T>(27,80)*z[5] - n<T>(27,4)*z[4] - n<T>(81,5)*z[3] + n<T>(317,2160)*z[1];
    z[11]=i*z[11];

    r += 2*z[2] - n<T>(11,8)*z[6] + n<T>(1,2)*z[7] - n<T>(3,2)*z[8] - n<T>(1,8)*z[9]
     - 
      n<T>(1,4)*z[10] + z[11];
 
    return r;
}

template std::complex<double> qg_2lha_tf480(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf480(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
