#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf891(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[119];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[70];
    z[3]=f[77];
    z[4]=f[79];
    z[5]=f[81];
    z[6]=c[11];
    z[7]=d[0];
    z[8]=d[1];
    z[9]=d[3];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=d[6];
    z[13]=d[7];
    z[14]=d[8];
    z[15]=d[9];
    z[16]=c[12];
    z[17]=c[13];
    z[18]=c[20];
    z[19]=c[23];
    z[20]=c[32];
    z[21]=c[33];
    z[22]=c[35];
    z[23]=c[36];
    z[24]=c[37];
    z[25]=c[38];
    z[26]=c[39];
    z[27]=c[40];
    z[28]=c[43];
    z[29]=c[44];
    z[30]=c[47];
    z[31]=c[48];
    z[32]=c[49];
    z[33]=c[50];
    z[34]=c[55];
    z[35]=c[56];
    z[36]=c[57];
    z[37]=c[59];
    z[38]=c[81];
    z[39]=c[84];
    z[40]=f[24];
    z[41]=f[44];
    z[42]=f[69];
    z[43]=f[80];
    z[44]=f[22];
    z[45]=f[42];
    z[46]=f[62];
    z[47]=d[2];
    z[48]=g[24];
    z[49]=g[27];
    z[50]=g[38];
    z[51]=g[40];
    z[52]=g[48];
    z[53]=g[50];
    z[54]=g[79];
    z[55]=g[82];
    z[56]=g[90];
    z[57]=g[92];
    z[58]=g[95];
    z[59]=g[99];
    z[60]=g[101];
    z[61]=g[104];
    z[62]=g[110];
    z[63]=g[112];
    z[64]=g[113];
    z[65]=g[131];
    z[66]=g[138];
    z[67]=g[147];
    z[68]=g[170];
    z[69]=g[173];
    z[70]=g[181];
    z[71]=g[183];
    z[72]=g[189];
    z[73]=g[191];
    z[74]=g[198];
    z[75]=g[200];
    z[76]=g[201];
    z[77]=g[212];
    z[78]=g[214];
    z[79]=g[220];
    z[80]=g[243];
    z[81]=g[245];
    z[82]=g[251];
    z[83]=g[254];
    z[84]=g[270];
    z[85]=g[272];
    z[86]=g[276];
    z[87]=g[278];
    z[88]=g[281];
    z[89]=g[288];
    z[90]=g[289];
    z[91]=g[290];
    z[92]=g[292];
    z[93]=g[294];
    z[94]=g[308];
    z[95]=g[312];
    z[96]=g[339];
    z[97]=g[342];
    z[98]=g[345];
    z[99]=g[347];
    z[100]=g[348];
    z[101]= - z[7] - z[15];
    z[102]= - z[18] + n<T>(1,20)*z[19];
    z[103]=z[102] - n<T>(12,5)*z[17];
    z[104]=n<T>(73,1620)*z[6] + z[103];
    z[101]=z[104]*z[101];
    z[104]=z[3] - z[5];
    z[105]= - n<T>(1,3)*z[104];
    z[106]= - n<T>(3,4)*z[4] + z[2] + z[105];
    z[106]=z[1]*z[106];
    z[107]=n<T>(1,15)*z[37] + 2*z[39] - n<T>(65,36)*z[38];
    z[108]= - n<T>(1,5)*z[19] + 4*z[18];
    z[108]=n<T>(1,3)*z[108] + n<T>(16,5)*z[17];
    z[108]=z[10]*z[108];
    z[109]= - n<T>(1,2)*z[13] + n<T>(1,5)*z[10];
    z[109]=z[6]*z[109];
    z[103]=n<T>(11,540)*z[6] + z[103];
    z[103]=z[9]*z[103];
    z[110]=n<T>(91,1080)*z[6] + n<T>(1,2)*z[102] - n<T>(6,5)*z[17];
    z[110]=z[14]*z[110];
    z[111]= - n<T>(1,5)*z[17] + n<T>(1,12)*z[102];
    z[112]=19*z[111] + n<T>(689,6480)*z[6];
    z[112]=z[12]*z[112];
    z[111]=17*z[111] - n<T>(293,6480)*z[6];
    z[111]=z[11]*z[111];
    z[102]= - n<T>(1,6)*z[102] + n<T>(2,5)*z[17];
    z[102]=7*z[102] - n<T>(137,3240)*z[6];
    z[102]=z[8]*z[102];
    z[101]=z[102] + z[111] + z[112] + z[110] + z[103] + n<T>(1,9)*z[109] + 
    z[108] + n<T>(59,216)*z[21] + 4*z[24] - n<T>(295,108)*z[25] - n<T>(109,972)*
    z[26] + n<T>(5,3)*z[29] + n<T>(52,405)*z[30] - n<T>(12,5)*z[32] - 2*z[33] - 
    z[35] + n<T>(1,3)*z[107] + n<T>(1,4)*z[36] + z[106] + z[101];
    z[101]=i*z[101];
    z[102]=z[44] + z[45];
    z[103]= - z[46] + n<T>(1,2)*z[102];
    z[106]=n<T>(1,2)*z[42];
    z[107]=z[103] + z[106];
    z[108]=z[5] + z[43];
    z[109]=n<T>(1,2)*z[41];
    z[110]= - n<T>(13,2)*z[4] - 11*z[16] + z[109] + z[107] + z[108];
    z[110]=z[14]*z[110];
    z[110]= - z[88] + z[53] + z[54] + z[55] + z[72] - z[110] - z[48] + 
    z[50] - z[51];
    z[111]=z[4] + z[40];
    z[112]=n<T>(1,12)*z[41];
    z[103]=n<T>(1,2)*z[43] + z[103] + z[5];
    z[103]= - n<T>(53,18)*z[16] + z[112] + n<T>(1,3)*z[103] + z[2] - n<T>(7,6)*
    z[111];
    z[103]=z[12]*z[103];
    z[103]=z[91] + z[103] - z[76];
    z[111]=z[40] + z[2];
    z[108]=z[3] - z[108];
    z[108]=n<T>(1,3)*z[108] - z[111];
    z[113]=n<T>(1,3)*z[16];
    z[114]=n<T>(1,3)*z[4];
    z[108]= - z[114] + n<T>(1,2)*z[108] + z[113];
    z[108]=z[9]*z[108];
    z[115]=z[42] - z[43];
    z[105]= - n<T>(1,2)*z[115] + z[105];
    z[105]=n<T>(1,6)*z[4] + z[113] + n<T>(7,12)*z[40] + z[112] + n<T>(1,2)*z[105]
     - 
   n<T>(1,3)*z[2];
    z[105]=z[7]*z[105];
    z[104]=n<T>(1,6)*z[104];
    z[112]=z[13] + z[47];
    z[104]=z[112]*z[104];
    z[112]=z[115] - z[40];
    z[113]= - z[41] + z[112];
    z[115]=n<T>(1,12)*z[47];
    z[113]=z[115]*z[113];
    z[115]=n<T>(1,3)*z[10];
    z[111]= - n<T>(16,3)*z[16] - z[41] + z[111];
    z[111]=z[111]*z[115];
    z[116]=n<T>(1,3)*z[47] + n<T>(3,2)*z[13];
    z[115]=n<T>(1,2)*z[116] - z[115];
    z[115]=z[4]*z[115];
    z[102]=z[102] + z[43];
    z[102]=n<T>(7,8)*z[41] - n<T>(1,2)*z[3] - z[106] - 2*z[46] - n<T>(1,4)*z[102];
    z[102]=n<T>(11,12)*z[4] + n<T>(89,36)*z[16] + n<T>(1,3)*z[102] + n<T>(1,4)*z[40];
    z[102]=z[11]*z[102];
    z[106]=z[40] + z[2] - z[3] - z[107];
    z[106]= - z[109] + n<T>(1,3)*z[106];
    z[106]=z[114] + n<T>(1,2)*z[106] + n<T>(1,9)*z[16];
    z[106]=z[8]*z[106];
    z[107]= - z[90] + z[64] + z[56] + n<T>(1,24)*z[96] + n<T>(3,8)*z[97] - n<T>(1,2)
   *z[98] - z[100] - n<T>(3,2)*z[99] + z[75] - z[83];
    z[109]= - z[89] + z[62] + z[74];
    z[114]= - z[69] + z[63] + z[65];
    z[116]= - z[68] - z[71] + z[84] + z[87];
    z[112]=z[41] + 5*z[46] + z[112];
    z[112]=n<T>(1,2)*z[112] + z[16];
    z[112]=z[15]*z[112];
    z[112]= - z[79] + z[112] - z[77] + z[92] + z[93];
    z[117]=z[59] + z[60];
    z[118]=z[2] - z[42];
    z[118]=n<T>(3,2)*z[16] - n<T>(1,6)*z[118];
    z[118]=z[13]*z[118];

    r +=  - n<T>(43,270)*z[20] + n<T>(12,5)*z[22] + 3*z[23] - n<T>(5,2)*z[27] - n<T>(3,4)*z[28]
     + n<T>(2,3)*z[31] - n<T>(1,20)*z[34] - n<T>(7,6)*z[49] - n<T>(1,18)*
      z[52] - n<T>(1,24)*z[57] + n<T>(1,96)*z[58] + n<T>(3,32)*z[61] + n<T>(5,6)*z[66]
       - n<T>(5,18)*z[67] + n<T>(11,8)*z[70] + n<T>(17,36)*z[73] + n<T>(4,3)*z[78]
     - n<T>(3,2)*z[80]
     - n<T>(1,12)*z[81]
     + n<T>(13,12)*z[82]
     + n<T>(43,24)*z[85] - n<T>(19,24)
      *z[86] + z[94] - z[95] + z[101] + z[102] + n<T>(1,2)*z[103] + z[104]
       + z[105] + z[106] - n<T>(1,4)*z[107] + z[108] - n<T>(1,8)*z[109] - n<T>(1,6)
      *z[110] + z[111] + n<T>(1,3)*z[112] + z[113] - n<T>(3,8)*z[114] + z[115]
       + n<T>(5,24)*z[116] - n<T>(1,9)*z[117] + z[118];
 
    return r;
}

template std::complex<double> qg_2lha_tf891(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf891(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
