#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf834(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[105];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[3];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[10];
    z[26]=e[11];
    z[27]=e[12];
    z[28]=e[13];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[21];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[31];
    z[38]=e[6];
    z[39]=e[14];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[6];
    z[43]=f[7];
    z[44]=f[9];
    z[45]=f[10];
    z[46]=f[11];
    z[47]=f[12];
    z[48]=f[13];
    z[49]=f[14];
    z[50]=f[15];
    z[51]=f[16];
    z[52]=f[17];
    z[53]=f[23];
    z[54]=f[30];
    z[55]=f[31];
    z[56]=f[32];
    z[57]=f[33];
    z[58]=f[35];
    z[59]=f[36];
    z[60]=f[37];
    z[61]=f[39];
    z[62]=f[41];
    z[63]=f[43];
    z[64]=f[50];
    z[65]=f[51];
    z[66]=f[52];
    z[67]=f[53];
    z[68]=f[54];
    z[69]=f[55];
    z[70]=f[56];
    z[71]=f[58];
    z[72]=f[74];
    z[73]=f[75];
    z[74]=3*z[5];
    z[75]=3*z[8];
    z[76]=z[1]*i;
    z[77]= - z[75] + 3*z[3] + z[76] - z[74];
    z[77]=n<T>(1,4)*z[77] + z[9];
    z[77]=z[9]*z[77];
    z[78]=n<T>(1,2)*z[5];
    z[79]=z[78] - z[76];
    z[80]=z[79]*z[5];
    z[81]=n<T>(3,2)*z[3];
    z[82]=z[76] - z[5];
    z[83]= - n<T>(1,2)*z[82] - z[3];
    z[83]=z[83]*z[81];
    z[84]=n<T>(1,3)*z[6];
    z[85]=n<T>(1,4)*z[76];
    z[86]= - n<T>(19,8)*z[6] - z[85] + z[5];
    z[86]=z[86]*z[84];
    z[87]=n<T>(1,2)*z[3];
    z[88]=z[87] - z[76];
    z[89]=n<T>(3,4)*z[8] + n<T>(3,2)*z[88] + z[84];
    z[89]=z[8]*z[89];
    z[90]=n<T>(1,2)*z[9];
    z[91]= - z[8] + z[6] - n<T>(11,4)*z[76] - z[5];
    z[91]=n<T>(17,24)*z[7] + n<T>(1,3)*z[91] - z[90];
    z[91]=z[7]*z[91];
    z[92]= - n<T>(11,3)*z[2] + n<T>(19,6)*z[7] + n<T>(5,2)*z[9] - n<T>(7,6)*z[6]
     - z[81]
    + 9*z[76] + z[5];
    z[93]=n<T>(1,4)*z[2];
    z[92]=z[92]*z[93];
    z[77]=z[92] + z[91] + z[77] + z[89] + z[86] + z[80] + z[83];
    z[77]=z[2]*z[77];
    z[83]=n<T>(1,8)*z[6];
    z[86]=n<T>(1,8)*z[3];
    z[89]=n<T>(7,24)*z[9] - z[83] - z[86] + n<T>(1,3)*z[76] + n<T>(1,8)*z[5];
    z[89]=z[9]*z[89];
    z[91]=n<T>(5,8)*z[8];
    z[92]=n<T>(23,8)*z[76] - z[5];
    z[92]= - z[7] + n<T>(9,8)*z[9] - z[91] + n<T>(5,6)*z[6] + n<T>(1,3)*z[92] - 
    z[87];
    z[92]=z[11]*z[92];
    z[94]=n<T>(1,3)*z[5];
    z[95]= - z[79]*z[94];
    z[96]= - z[87] - z[76] - n<T>(1,4)*z[5];
    z[96]=z[96]*z[87];
    z[97]=2*z[5];
    z[98]=z[76] - z[97];
    z[98]= - n<T>(1,12)*z[6] + n<T>(1,3)*z[98] + z[86];
    z[98]=z[6]*z[98];
    z[99]=n<T>(1,2)*z[8];
    z[100]=z[99] - z[76];
    z[101]=z[3] - z[100];
    z[91]=z[101]*z[91];
    z[101]=n<T>(1,2)*z[7];
    z[102]= - z[101] - z[82];
    z[102]=z[7]*z[102];
    z[89]=n<T>(1,2)*z[92] + z[102] + z[89] + z[91] + z[98] + z[95] + z[96];
    z[89]=z[11]*z[89];
    z[88]=z[3]*z[88];
    z[91]=n<T>(1,2)*z[6];
    z[92]=z[76] - z[91];
    z[95]=n<T>(25,12)*z[6];
    z[92]=z[92]*z[95];
    z[96]=z[5] - z[100];
    z[96]=z[96]*z[75];
    z[98]=n<T>(1,4)*z[9];
    z[102]=n<T>(13,6)*z[9] - n<T>(13,3)*z[76] + 3*z[6];
    z[102]=z[102]*z[98];
    z[101]=z[101] + z[76] - z[3];
    z[101]=z[7]*z[101];
    z[88]=z[101] + z[102] + z[96] + z[92] - z[80] + z[88];
    z[92]=z[3] - z[5];
    z[75]= - z[7] + n<T>(13,12)*z[9] - z[75] - z[95] + n<T>(15,2)*z[76] + z[92];
    z[75]= - n<T>(13,24)*z[4] + n<T>(1,8)*z[11] + n<T>(1,4)*z[75] + z[2];
    z[95]=n<T>(1,2)*z[4];
    z[75]=z[75]*z[95];
    z[78]=z[93] - z[76] - z[78];
    z[78]=z[2]*z[78];
    z[93]=z[6] - n<T>(11,8)*z[9];
    z[93]=z[11]*z[93];
    z[75]=z[75] + n<T>(1,3)*z[93] + n<T>(1,4)*z[88] + z[78];
    z[75]=z[4]*z[75];
    z[78]=n<T>(1,4)*z[3];
    z[88]=n<T>(1,8)*z[8];
    z[93]=z[88] - z[78] + 2*z[76] + n<T>(3,4)*z[5];
    z[93]=z[8]*z[93];
    z[96]=n<T>(5,2)*z[76];
    z[74]=n<T>(7,4)*z[3] + z[96] - z[74];
    z[74]=z[3]*z[74];
    z[101]=n<T>(1,2)*z[76];
    z[102]= - n<T>(1,4)*z[6] - z[101] + z[92];
    z[83]=z[102]*z[83];
    z[102]= - n<T>(5,3)*z[76] + n<T>(3,2)*z[5];
    z[102]= - n<T>(5,2)*z[6] + 13*z[102] - 19*z[3];
    z[102]=n<T>(11,48)*z[9] + n<T>(1,16)*z[102] - z[8];
    z[102]=z[9]*z[102];
    z[74]=z[102] + z[93] + z[83] + n<T>(39,16)*z[80] + z[74];
    z[74]=z[9]*z[74];
    z[81]=z[81] + z[5] - n<T>(5,3) + z[101];
    z[81]=z[3]*z[81];
    z[81]= - n<T>(5,6)*z[80] + z[81];
    z[83]=5*z[76];
    z[93]= - 7*z[5] - n<T>(13,2) + z[83];
    z[93]=n<T>(1,6)*z[93] + z[3];
    z[84]=n<T>(1,2)*z[93] - z[84];
    z[84]=z[6]*z[84];
    z[93]=n<T>(7,4)*z[76];
    z[101]=z[93] + z[5];
    z[101]= - n<T>(1,6)*z[6] + n<T>(1,3)*z[101] - z[87];
    z[101]=z[8]*z[101];
    z[102]= - z[3] + z[8];
    z[102]=z[102]*z[90];
    z[103]=3*z[76];
    z[104]= - z[103] - n<T>(5,3)*z[5];
    z[104]= - n<T>(5,3)*z[6] + n<T>(1,4)*z[104] - z[3];
    z[104]=n<T>(73,72)*z[7] + n<T>(1,2)*z[104] + n<T>(1,3)*z[8];
    z[104]=z[7]*z[104];
    z[81]=z[104] + z[102] + z[101] + n<T>(1,2)*z[81] + z[84];
    z[81]=z[7]*z[81];
    z[84]= - n<T>(23,2)*z[3] - 47*z[76] + 35*z[5];
    z[84]=z[3]*z[84];
    z[84]= - n<T>(97,2)*z[80] + z[84];
    z[93]= - n<T>(3,8)*z[6] - z[93] - z[92];
    z[93]=z[6]*z[93];
    z[85]=z[88] + n<T>(3,4)*z[6] - z[85] + z[3];
    z[85]=z[8]*z[85];
    z[101]= - z[76] + 9*z[5];
    z[101]= - n<T>(1,16)*z[9] - z[8] + n<T>(1,8)*z[101] - z[3];
    z[101]=z[9]*z[101];
    z[84]=z[101] + z[85] + n<T>(1,12)*z[84] + z[93];
    z[85]= - n<T>(3,8)*z[7] + z[90] - z[99] - z[79];
    z[85]=z[7]*z[85];
    z[90]=91*z[76] - 97*z[5];
    z[90]=n<T>(1,4)*z[90] + 25*z[3];
    z[90]=n<T>(13,3)*z[10] + z[7] - z[98] + z[99] + n<T>(1,3)*z[90] - n<T>(13,2)*
    z[6];
    z[90]=z[10]*z[90];
    z[84]=n<T>(1,8)*z[90] + n<T>(1,2)*z[84] + z[85];
    z[84]=z[10]*z[84];
    z[78]= - z[78] + z[96] - z[5];
    z[78]=z[3]*z[78];
    z[78]= - z[80] + z[78];
    z[80]= - n<T>(5,48)*z[6] + n<T>(1,8)*z[76] - z[94];
    z[80]=z[6]*z[80];
    z[85]= - n<T>(5,2)*z[3] - z[103] - z[5];
    z[85]=9*z[8] + 3*z[85] - z[91];
    z[85]=z[85]*z[88];
    z[78]=z[85] + n<T>(3,4)*z[78] + z[80];
    z[78]=z[8]*z[78];
    z[80]=z[4] + z[11];
    z[85]= - z[76]*z[80];
    z[85]= - z[29] + z[85];
    z[85]=z[13]*z[85];
    z[80]= - z[76] + z[80];
    z[80]=z[27]*z[80];
    z[80]= - z[73] + z[85] + z[80] - z[72] + z[54] + z[49] - z[40];
    z[85]=5*z[5];
    z[88]= - 19*z[76] - z[85];
    z[88]=z[5]*z[88];
    z[90]=1 - z[76];
    z[90]=5*z[90] + n<T>(37,4)*z[5];
    z[90]=n<T>(1,3)*z[90] - z[87];
    z[90]=z[3]*z[90];
    z[88]=n<T>(1,6)*z[88] + z[90];
    z[88]=z[3]*z[88];
    z[90]=z[9] + z[6] - z[76] - z[92];
    z[90]=z[28]*z[90];
    z[83]= - z[83] + z[5];
    z[83]=z[83]*npow(z[5],2);
    z[83]=z[44] - z[56] + z[90] + n<T>(1,6)*z[83] + z[88];
    z[85]= - 11*z[76] + z[85];
    z[85]=z[85]*z[94];
    z[88]=3*z[82] + z[87];
    z[87]=z[88]*z[87];
    z[85]=z[85] + z[87];
    z[87]=23 + n<T>(47,4)*z[76];
    z[87]=n<T>(1,8)*z[87] + z[97];
    z[86]=n<T>(7,9)*z[6] + n<T>(1,3)*z[87] + z[86];
    z[86]=z[6]*z[86];
    z[85]=n<T>(1,4)*z[85] + z[86];
    z[85]=z[6]*z[85];
    z[86]=n<T>(1,2)*z[2];
    z[87]= - z[95] - z[86] + z[76];
    z[87]=z[17]*z[87];
    z[88]=z[76]*z[8];
    z[88]=z[88] + z[29];
    z[90]= - z[7]*z[76];
    z[90]=z[90] - z[88];
    z[90]=z[15]*z[90];
    z[91]=z[76] - z[8];
    z[92]= - z[7] + z[91];
    z[92]=z[24]*z[92];
    z[90]=z[63] + z[90] + z[92];
    z[92]= - z[9]*z[76];
    z[88]=z[92] - z[88];
    z[88]=z[16]*z[88];
    z[91]=z[9] - z[91];
    z[91]=z[26]*z[91];
    z[88]=z[88] + z[91];
    z[91]=z[76]*z[5];
    z[91]=z[91] + z[29];
    z[92]=z[6]*z[76];
    z[92]=z[92] + z[91];
    z[92]=z[14]*z[92];
    z[93]= - z[6] + z[82];
    z[93]=z[21]*z[93];
    z[92]=z[92] + z[93];
    z[76]=z[3]*z[76];
    z[76]=z[76] + z[91];
    z[76]=z[12]*z[76];
    z[91]=z[3] - z[82];
    z[91]=z[19]*z[91];
    z[76]=z[76] + z[91];
    z[91]=z[2] - z[8];
    z[93]= - n<T>(1,4)*z[11] + n<T>(11,6)*z[7] - n<T>(5,12)*z[82] + 2*z[6] - n<T>(2,3)*
    z[91];
    z[93]=z[22]*z[93];
    z[94]=z[10] + z[3];
    z[94]=z[9] + n<T>(5,6) + z[82] - n<T>(67,24)*z[94];
    z[94]=z[20]*z[94];
    z[82]=n<T>(13,4)*z[9] + n<T>(9,4)*z[3] - z[82] - z[91];
    z[82]=z[25]*z[82];
    z[79]= - z[86] - z[79];
    z[79]=z[18]*z[79];
    z[86]= - z[86] - z[100];
    z[86]=z[23]*z[86];
    z[91]= - z[46] + z[60] - z[52];
    z[95]=z[62] + z[48];
    z[96]= - n<T>(37,6)*z[5] + 17*z[3];
    z[96]=n<T>(91,12)*z[7] - n<T>(139,3)*z[9] - n<T>(29,3)*z[8] + n<T>(1,2)*z[96]
     + 11*
    z[6];
    z[96]=n<T>(179,48)*z[10] + n<T>(109,32)*z[4] + n<T>(43,24)*z[11] + n<T>(1,8)*z[96]
    + z[2];
    z[96]=z[29]*z[96];
    z[96]=z[96] - z[68];
    z[97]= - 6*z[35] - n<T>(11,4)*z[33] + z[32] + n<T>(35,144)*z[30];
    z[97]=i*z[97];
    z[98]=z[7] + z[10];
    z[98]=z[38]*z[98];
    z[99]= - z[9] - z[11];
    z[99]=z[39]*z[99];

    r += n<T>(5,3)*z[31] + n<T>(11,12)*z[34] + n<T>(5,2)*z[36] - n<T>(11,6)*z[37]
     + n<T>(63,32)*z[41]
     + n<T>(27,16)*z[42]
     + 6*z[43]
     + n<T>(17,8)*z[45]
     + n<T>(73,96)*
      z[47] + n<T>(15,8)*z[50] - n<T>(9,32)*z[51] - n<T>(3,4)*z[53] - n<T>(31,48)*z[55]
       - n<T>(11,3)*z[57] - n<T>(5,12)*z[58] - n<T>(1,16)*z[59] - n<T>(15,16)*z[61]
     - 
      n<T>(11,24)*z[64] - n<T>(25,96)*z[65] + n<T>(1,6)*z[66] - z[67] - n<T>(13,96)*
      z[69] - n<T>(5,16)*z[70] + n<T>(3,32)*z[71] + z[74] + z[75] + n<T>(29,24)*
      z[76] + z[77] + z[78] + z[79] + n<T>(1,8)*z[80] + z[81] + n<T>(3,2)*z[82]
       + n<T>(1,4)*z[83] + z[84] + z[85] + 3*z[86] + z[87] + n<T>(9,4)*z[88] + 
      z[89] + n<T>(7,12)*z[90] + n<T>(3,8)*z[91] + n<T>(1,12)*z[92] + z[93] + z[94]
       - n<T>(1,2)*z[95] + n<T>(1,3)*z[96] + z[97] + n<T>(5,4)*z[98] + n<T>(13,24)*
      z[99];
 
    return r;
}

template std::complex<double> qg_2lha_tf834(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf834(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
