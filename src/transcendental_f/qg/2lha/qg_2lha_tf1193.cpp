#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1193(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[42];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[3];
    z[7]=d[4];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=e[0];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[17];
    z[17]=c[31];
    z[18]=e[3];
    z[19]=e[10];
    z[20]=f[2];
    z[21]=f[3];
    z[22]=f[4];
    z[23]=f[5];
    z[24]=f[11];
    z[25]=f[12];
    z[26]=f[13];
    z[27]=f[16];
    z[28]=f[19];
    z[29]=4*z[6];
    z[30]=z[1]*i;
    z[31]=z[29]*z[30];
    z[32]=2*z[30];
    z[33]=z[32] + z[6];
    z[33]= - z[2] + 2*z[33];
    z[33]=z[33]*z[2];
    z[34]=npow(z[6],2);
    z[31]=z[33] - z[31] - z[34];
    z[33]=z[2] - z[6];
    z[33]=2*z[33];
    z[35]= - z[3]*z[33];
    z[35]=z[35] + z[31];
    z[35]=z[3]*z[35];
    z[33]=z[4]*z[33];
    z[31]=z[33] - z[31];
    z[31]=z[4]*z[31];
    z[31]=z[35] + z[31];
    z[33]=z[30]*z[6];
    z[35]= - z[33] + n<T>(1,2)*z[34];
    z[35]=7*z[35];
    z[36]=5*z[5];
    z[37]=5*z[6] + z[30];
    z[37]=2*z[37] - z[36];
    z[37]=z[5]*z[37];
    z[38]=z[30] - z[6];
    z[39]=n<T>(7,2)*z[38];
    z[40]=2*z[5];
    z[41]= - n<T>(4,3)*z[7] + z[39] + z[40];
    z[41]=z[7]*z[41];
    z[37]=z[41] - z[35] + z[37];
    z[37]=z[7]*z[37];
    z[41]=z[4] - z[3];
    z[41]= - n<T>(5,3)*z[8] + z[7] + n<T>(5,2)*z[6] + n<T>(4,3)*z[5] + n<T>(4,9)*z[41];
    z[41]=z[13]*z[41];
    z[37]=z[37] + z[41];
    z[29]=z[40] - z[29] - 5*z[30];
    z[29]=z[5]*z[29];
    z[29]=z[35] + z[29];
    z[35]=n<T>(1,2)*z[7];
    z[40]=z[35] + z[38];
    z[40]=z[7]*z[40];
    z[36]= - z[39] - z[36];
    z[35]=n<T>(1,3)*z[36] + z[35];
    z[35]=z[8]*z[35];
    z[29]=z[35] + n<T>(1,3)*z[29] + z[40];
    z[29]=z[8]*z[29];
    z[34]=z[34]*z[30];
    z[35]=npow(z[6],3);
    z[34]= - n<T>(1,3)*z[35] + z[34];
    z[35]=n<T>(13,9)*z[5] - z[6] + n<T>(2,3)*z[30];
    z[35]=z[5]*z[35];
    z[33]=n<T>(2,3)*z[33] + z[35];
    z[33]=z[5]*z[33];
    z[29]=z[29] + n<T>(4,3)*z[34] + z[33] + n<T>(1,3)*z[37] + n<T>(2,9)*z[31];
    z[31]=z[32] - z[2];
    z[32]=z[3] - z[31];
    z[32]=z[10]*z[32];
    z[31]= - z[4] + z[31];
    z[31]=z[12]*z[31];
    z[31]= - z[32] - z[31] - z[26] + z[24] + z[23] - z[21];
    z[32]=4*z[7];
    z[33]= - 2*z[6] + z[5];
    z[33]=n<T>(1,3)*z[33] - z[32];
    z[33]=n<T>(1,3)*z[33] + z[8];
    z[30]=z[30]*z[33];
    z[30]= - n<T>(2,9)*z[13] + z[30];
    z[30]=z[9]*z[30];
    z[33]=2*z[38] + z[5];
    z[32]=n<T>(1,3)*z[33] - z[32];
    z[32]=n<T>(1,3)*z[32] + z[8];
    z[32]=z[11]*z[32];
    z[33]=z[5] + z[7];
    z[33]=z[18]*z[33];
    z[33]=z[33] + z[20];
    z[34]= - z[16] - n<T>(1,54)*z[14];
    z[34]=i*z[34];
    z[35]=z[5] + z[8];
    z[35]=z[19]*z[35];

    r +=  - n<T>(5,18)*z[15] + n<T>(1,4)*z[17] + n<T>(7,18)*z[22] - n<T>(1,18)*z[25]
     +  n<T>(1,6)*z[27] - n<T>(4,9)*z[28] + n<T>(1,3)*z[29] + z[30] - n<T>(4,27)*z[31]
     +  z[32] + n<T>(2,9)*z[33] + z[34] + n<T>(5,9)*z[35];
 
    return r;
}

template std::complex<double> qg_2lha_tf1193(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1193(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
