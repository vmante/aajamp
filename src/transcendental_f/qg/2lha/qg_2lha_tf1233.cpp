#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1233(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[50];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=d[4];
    z[11]=d[8];
    z[12]=c[11];
    z[13]=c[15];
    z[14]=c[18];
    z[15]=c[19];
    z[16]=c[23];
    z[17]=c[25];
    z[18]=c[26];
    z[19]=c[28];
    z[20]=c[29];
    z[21]=c[30];
    z[22]=c[31];
    z[23]=f[0];
    z[24]=f[1];
    z[25]=f[3];
    z[26]=f[4];
    z[27]=f[6];
    z[28]=f[8];
    z[29]=f[9];
    z[30]=f[11];
    z[31]=f[12];
    z[32]=f[13];
    z[33]=f[14];
    z[34]=f[16];
    z[35]=f[17];
    z[36]=f[18];
    z[37]=f[23];
    z[38]=f[26];
    z[39]=f[27];
    z[40]=5*z[4];
    z[41]= - z[40] + 14*z[2];
    z[42]=3*z[3];
    z[43]= - z[42] + n<T>(1,3)*z[41];
    z[43]=z[43]*z[5];
    z[44]=z[40] + 8*z[2];
    z[44]=z[44]*z[4];
    z[45]=npow(z[2],2);
    z[44]=z[44] - 4*z[45];
    z[46]=2*z[4] - z[3];
    z[42]=z[46]*z[42];
    z[42]=z[43] - n<T>(1,3)*z[44] + z[42];
    z[42]=z[5]*z[42];
    z[46]=10*z[2] + z[4];
    z[46]=z[4]*z[46];
    z[46]=8*z[45] + z[46];
    z[46]=z[4]*z[46];
    z[47]= - z[3] + z[41];
    z[47]=z[3]*z[47];
    z[44]=z[47] - z[44];
    z[44]=z[3]*z[44];
    z[47]=npow(z[2],3);
    z[44]=z[44] - 17*z[47] + z[46];
    z[46]= - n<T>(20,3)*z[2] - z[4];
    z[46]=z[4]*z[46];
    z[41]= - n<T>(2,3)*z[41] + z[3];
    z[41]=z[3]*z[41];
    z[41]=z[41] + n<T>(35,3)*z[45] + z[46];
    z[41]=i*z[41];
    z[45]=2*i;
    z[43]= - z[45]*z[43];
    z[41]=z[41] + z[43];
    z[41]=z[1]*z[41];
    z[41]= - z[37] + z[41] + n<T>(1,3)*z[44] + z[42];
    z[42]=n<T>(1,4)*z[9];
    z[43]=npow(z[10],2);
    z[43]= - z[42] + z[43];
    z[43]=z[10]*z[43];
    z[44]=npow(z[11],2);
    z[42]=z[42] - z[44];
    z[42]=z[11]*z[42];
    z[44]=z[31] - z[28] + z[26] - z[23];
    z[46]=z[32] - z[20];
    z[47]= - n<T>(176,3)*z[18] - n<T>(70,3)*z[16] - n<T>(2,3)*z[12];
    z[47]=i*z[47];
    z[40]= - z[2] - z[40];
    z[40]=7*z[40] + 59*z[3];
    z[40]=n<T>(1,2)*z[40] + 10*z[5];
    z[40]=z[9]*z[40];
    z[45]=z[45]*z[1];
    z[45]=z[45] - z[2];
    z[48]= - z[3] + z[45];
    z[48]=z[6]*z[48];
    z[49]= - z[4] + z[45];
    z[49]=z[7]*z[49];
    z[45]= - z[5] + z[45];
    z[45]=z[8]*z[45];

    r +=  - n<T>(13,9)*z[13] - n<T>(52,9)*z[14] + n<T>(4,9)*z[15] + n<T>(115,9)*z[17]
       + 38*z[19] + n<T>(52,3)*z[21] - n<T>(677,18)*z[22] - z[24] - n<T>(4,3)*z[25]
       + z[27] - z[29] - n<T>(10,3)*z[30] + z[33] + 4*z[34] - 8*z[35] + 
      z[36] + z[38] - z[39] + n<T>(1,9)*z[40] + 2*z[41] + z[42] + z[43] + 3
      *z[44] + n<T>(40,3)*z[45] - n<T>(13,3)*z[46] + z[47] + n<T>(34,3)*z[48] + n<T>(14,3)*z[49];
 
    return r;
}

template std::complex<double> qg_2lha_tf1233(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1233(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
