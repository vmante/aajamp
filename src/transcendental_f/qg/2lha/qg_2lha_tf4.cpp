#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf4(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[23];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[9];
    z[4]=d[12];
    z[5]=d[1];
    z[6]=d[3];
    z[7]=d[5];
    z[8]=e[5];
    z[9]=e[13];
    z[10]=c[3];
    z[11]=c[11];
    z[12]=z[3]*z[7];
    z[13]=npow(z[7],2);
    z[12]= - z[12] + n<T>(1,2)*z[13];
    z[14]= - z[3] + n<T>(1,2)*z[6];
    z[14]=z[14]*z[6];
    z[15]=z[1]*i;
    z[16]=z[15]*z[3];
    z[16]= - z[14] - z[16] + z[12];
    z[17]=z[5] + z[2];
    z[18]=n<T>(1,4)*z[17];
    z[16]= - z[16]*z[18];
    z[19]=z[7] - n<T>(5,3);
    z[18]=z[18] + z[19];
    z[20]=n<T>(1,4)*z[15];
    z[21]=z[18] - z[20];
    z[22]= - z[9] + z[8];
    z[21]=z[21]*z[22];
    z[12]= - z[19]*z[12];
    z[22]= - n<T>(5,3) + n<T>(3,4)*z[7];
    z[22]=z[3]*z[22];
    z[13]=n<T>(1,8)*z[13] + z[22];
    z[13]=z[13]*z[15];
    z[20]=z[20] - z[19];
    z[14]= - z[20]*z[14];
    z[17]= - n<T>(1,3)*z[19] + n<T>(1,2)*z[3] - n<T>(1,12)*z[17];
    z[17]=z[10]*z[17];
    z[15]= - z[15]*z[18];
    z[15]= - n<T>(1,4)*z[10] + z[15];
    z[15]=z[4]*z[15];
    z[18]=z[11]*i;

    r += z[12] + z[13] + z[14] + z[15] + z[16] + n<T>(1,2)*z[17] + n<T>(1,24)*
      z[18] + z[21];
 
    return r;
}

template std::complex<double> qg_2lha_tf4(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf4(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
