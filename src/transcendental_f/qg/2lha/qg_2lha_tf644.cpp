#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf644(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[112];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[77];
    z[3]=f[79];
    z[4]=f[81];
    z[5]=c[11];
    z[6]=d[0];
    z[7]=d[1];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[7];
    z[13]=d[8];
    z[14]=d[9];
    z[15]=c[12];
    z[16]=c[13];
    z[17]=c[20];
    z[18]=c[23];
    z[19]=c[47];
    z[20]=c[48];
    z[21]=c[49];
    z[22]=c[56];
    z[23]=c[59];
    z[24]=f[22];
    z[25]=f[24];
    z[26]=f[42];
    z[27]=f[44];
    z[28]=f[61];
    z[29]=f[62];
    z[30]=f[69];
    z[31]=f[70];
    z[32]=f[80];
    z[33]=d[3];
    z[34]=g[24];
    z[35]=g[27];
    z[36]=g[38];
    z[37]=g[40];
    z[38]=g[48];
    z[39]=g[50];
    z[40]=g[79];
    z[41]=g[82];
    z[42]=g[90];
    z[43]=g[92];
    z[44]=g[99];
    z[45]=g[101];
    z[46]=g[110];
    z[47]=g[112];
    z[48]=g[113];
    z[49]=g[128];
    z[50]=g[131];
    z[51]=g[138];
    z[52]=g[140];
    z[53]=g[145];
    z[54]=g[147];
    z[55]=g[170];
    z[56]=g[173];
    z[57]=g[181];
    z[58]=g[183];
    z[59]=g[189];
    z[60]=g[191];
    z[61]=g[212];
    z[62]=g[214];
    z[63]=g[218];
    z[64]=g[220];
    z[65]=g[224];
    z[66]=g[232];
    z[67]=g[233];
    z[68]=g[234];
    z[69]=g[243];
    z[70]=g[245];
    z[71]=g[251];
    z[72]=g[254];
    z[73]=g[270];
    z[74]=g[272];
    z[75]=g[276];
    z[76]=g[278];
    z[77]=g[281];
    z[78]=g[285];
    z[79]=g[288];
    z[80]=g[289];
    z[81]=g[290];
    z[82]=g[292];
    z[83]=g[294];
    z[84]=g[324];
    z[85]=g[329];
    z[86]=g[330];
    z[87]=g[331];
    z[88]=g[332];
    z[89]=g[389];
    z[90]=g[392];
    z[91]=g[397];
    z[92]=g[398];
    z[93]=g[399];
    z[94]=g[402];
    z[95]=g[407];
    z[96]=g[408];
    z[97]= - z[17] + n<T>(1,20)*z[18];
    z[97]= - n<T>(1,5)*z[16] + n<T>(1,12)*z[97];
    z[98]= - 19*z[97] - n<T>(983,2160)*z[5];
    z[98]=z[13]*z[98];
    z[99]= - 5*z[17] + n<T>(1,4)*z[18];
    z[99]= - 3*z[16] + n<T>(1,4)*z[99];
    z[100]=n<T>(43,1296)*z[5] - z[99];
    z[100]=z[11]*z[100];
    z[101]= - 77*z[97] + n<T>(53,6480)*z[5];
    z[101]=z[10]*z[101];
    z[98]=z[101] + z[98] + z[100];
    z[100]= - n<T>(1,4)*z[23] + 5*z[22];
    z[101]=n<T>(125,1296)*z[5] + z[99];
    z[101]=z[6]*z[101];
    z[100]=z[101] - n<T>(1,2)*z[17] + n<T>(1,40)*z[18] - n<T>(125,1296)*z[19] + n<T>(1,4)
   *z[100] + 3*z[21];
    z[101]=n<T>(1,2)*z[8];
    z[99]= - n<T>(157,1296)*z[5] - z[99];
    z[99]=z[99]*z[101];
    z[102]=n<T>(1,2)*z[14];
    z[103]=23*z[97] - n<T>(79,2160)*z[5];
    z[103]=z[103]*z[102];
    z[104]=n<T>(17,96)*z[4];
    z[105]=n<T>(11,32)*z[2] + n<T>(59,96)*z[3] + z[104];
    z[105]=z[1]*z[105];
    z[106]=n<T>(59,3)*z[12];
    z[107]=n<T>(31,5) + z[106];
    z[107]=z[5]*z[107];
    z[108]=n<T>(13,240)*z[5] + z[97];
    z[108]=z[9]*z[108];
    z[97]=n<T>(71,6480)*z[5] + z[97];
    z[97]=z[7]*z[97];
    z[97]=z[103] + n<T>(17,8)*z[97] + z[99] + n<T>(1,4)*z[108] + n<T>(1,432)*z[107]
    - n<T>(3,5)*z[16] + z[105] + n<T>(1,2)*z[100] + n<T>(1,8)*z[98];
    z[97]=i*z[97];
    z[98]=3*z[30];
    z[99]=n<T>(49,3)*z[31] - 5*z[27] - n<T>(17,3)*z[25];
    z[99]=n<T>(1,2)*z[99] - z[98];
    z[100]=z[29] + z[28];
    z[99]=n<T>(7,16)*z[2] - n<T>(5,32)*z[3] + n<T>(15,16)*z[4] - n<T>(25,36)*z[15]
     + n<T>(1,16)*z[99]
     + n<T>(1,3)*z[100];
    z[99]=z[9]*z[99];
    z[100]=n<T>(7,16)*z[26];
    z[103]=n<T>(7,16)*z[25];
    z[105]=n<T>(13,16)*z[29] - n<T>(1,32)*z[28] + n<T>(31,32)*z[31] - n<T>(17,32)*z[24]
    - z[103] + n<T>(29,32)*z[27] + z[100] - z[32];
    z[105]=n<T>(53,96)*z[3] - n<T>(73,96)*z[4] + n<T>(1,3)*z[105] - n<T>(1,2)*z[15];
    z[105]=z[11]*z[105];
    z[107]= - z[32] + n<T>(7,16)*z[27];
    z[100]=n<T>(31,32)*z[25] - z[100] - z[107];
    z[108]=n<T>(31,96)*z[28] + n<T>(1,2)*z[30];
    z[100]=n<T>(1,4)*z[2] - n<T>(19,48)*z[3] + n<T>(11,16)*z[4] - n<T>(53,36)*z[15]
     + n<T>(21,32)*z[29] - n<T>(31,96)*z[31]
     + n<T>(1,3)*z[100]
     + n<T>(1,2)*z[24] - z[108];
    z[100]=z[10]*z[100];
    z[109]=n<T>(5,32)*z[27] + n<T>(1,3)*z[32];
    z[110]= - n<T>(7,12)*z[2] + n<T>(13,12)*z[3] - n<T>(29,48)*z[4] + n<T>(49,18)*z[15]
    + n<T>(1,48)*z[29] + n<T>(49,96)*z[30] - n<T>(3,16)*z[31] - n<T>(17,96)*z[24]
     + n<T>(5,32)*z[26]
     + z[109];
    z[110]=z[13]*z[110];
    z[99]=z[99] + z[110] + z[105] + z[100];
    z[100]=z[94] + z[86] + z[90];
    z[105]=z[91] + z[95];
    z[110]=z[89] + z[93];
    z[100]= - z[68] + n<T>(17,12)*z[82] - n<T>(29,6)*z[83] - n<T>(11,4)*z[84] + n<T>(27,4)*z[85]
     - n<T>(1,4)*z[87]
     - z[88]
     + z[92]
     + z[96]
     + n<T>(1,24)*z[110] - n<T>(1,2)*z[105]
     + n<T>(3,8)*z[100];
    z[105]= - z[27] + z[31] - z[25];
    z[110]=z[28] + z[26];
    z[111]=n<T>(31,2)*z[30];
    z[105]= - n<T>(29,2)*z[29] - z[111] + n<T>(31,2)*z[24] + n<T>(1,2)*z[110] - 7*
    z[105];
    z[105]=n<T>(13,24)*z[2] - n<T>(5,16)*z[3] - n<T>(17,24)*z[4] + n<T>(1,24)*z[105]
     - 3
   *z[15];
    z[105]=z[6]*z[105];
    z[110]=z[66] + z[71];
    z[100]=z[105] + n<T>(13,72)*z[54] + n<T>(31,24)*z[55] + n<T>(13,24)*z[56] + n<T>(7,24)*z[57]
     + n<T>(25,24)*z[58] - n<T>(55,72)*z[59]
     + n<T>(19,36)*z[60]
     + n<T>(49,12)*
    z[61] - n<T>(9,4)*z[62] + n<T>(17,8)*z[63] + n<T>(65,24)*z[64] - n<T>(37,36)*z[65]
    - n<T>(1,4)*z[67] + n<T>(59,12)*z[69] + n<T>(7,12)*z[70] - n<T>(59,72)*z[72]
     + n<T>(23,24)*z[73] - n<T>(77,24)*z[74]
     + n<T>(11,6)*z[75]
     + n<T>(29,24)*z[76] - n<T>(1,3)*
    z[77] + n<T>(3,16)*z[78] - n<T>(3,8)*z[79] - z[80] - z[81] + n<T>(1,2)*z[100]
     - 
   n<T>(1,8)*z[110];
    z[105]=z[24] + z[25];
    z[110]=z[28] + z[31];
    z[105]= - 17*z[29] - z[111] - 7*z[26] - n<T>(31,2)*z[110] + n<T>(17,2)*
    z[105];
    z[105]=n<T>(65,16)*z[2] + n<T>(7,8)*z[3] + n<T>(1,8)*z[105] - n<T>(17,3)*z[15];
    z[105]=z[7]*z[105];
    z[110]=z[2] - z[4];
    z[108]=n<T>(193,72)*z[15] - n<T>(89,96)*z[29] - n<T>(31,96)*z[24] - n<T>(17,96)*
    z[25] - n<T>(1,96)*z[26] - z[109] + z[108] - n<T>(3,32)*z[110];
    z[102]=z[108]*z[102];
    z[108]=1 - n<T>(1,24)*z[33];
    z[108]=z[28]*z[108];
    z[109]= - n<T>(3,4)*z[12] + 1 + n<T>(11,8)*z[33];
    z[109]=z[2]*z[109];
    z[108]=z[48] + z[108] + z[109];
    z[109]= - 31*z[24] + 49*z[31];
    z[98]= - n<T>(31,6)*z[29] + n<T>(1,6)*z[109] - z[98];
    z[98]=z[12]*z[98];
    z[106]=n<T>(11,2)*z[33] - z[106];
    z[106]=z[3]*z[106];
    z[98]=z[46] + z[98] + z[106];
    z[106]= - 1 - n<T>(59,8)*z[12];
    z[106]=z[15]*z[106];
    z[109]= - z[33] + n<T>(49,32)*z[12];
    z[109]=z[4]*z[109];
    z[106]=z[38] + z[106] + z[109];
    z[103]=n<T>(49,32)*z[30] - n<T>(35,32)*z[31] - z[103] - z[107];
    z[103]= - n<T>(107,96)*z[2] - n<T>(3,16)*z[3] - z[104] + n<T>(13,6)*z[15] + n<T>(1,3)
   *z[103] + n<T>(5,8)*z[29];
    z[101]=z[103]*z[101];
    z[103]= - z[44] + z[40] + z[43];
    z[104]=z[49] + z[52];
    z[107]=z[35] + z[36];
    z[109]=z[34] + z[37];
    z[110]= - n<T>(1,192)*z[26] - n<T>(1,6)*z[32];
    z[110]=z[33]*z[110];

    r += n<T>(3,4)*z[20] - n<T>(31,288)*z[39] - n<T>(23,96)*z[41] - n<T>(3,32)*z[42]
     - 
      n<T>(47,288)*z[45] + n<T>(1,16)*z[47] + n<T>(29,96)*z[50] - n<T>(19,24)*z[51]
     + n<T>(1,9)*z[53]
     + z[97]
     + n<T>(1,32)*z[98]
     + n<T>(1,2)*z[99]
     + n<T>(1,4)*z[100]
     +  z[101] + z[102] - n<T>(5,96)*z[103] - n<T>(31,96)*z[104] + n<T>(1,12)*z[105]
       + n<T>(1,6)*z[106] - n<T>(7,48)*z[107] + n<T>(1,8)*z[108] - n<T>(17,96)*z[109]
       + z[110];
 
    return r;
}

template std::complex<double> qg_2lha_tf644(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf644(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
