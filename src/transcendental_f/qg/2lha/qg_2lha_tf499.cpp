#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf499(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[57];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[2];
    z[6]=d[8];
    z[7]=d[9];
    z[8]=d[11];
    z[9]=d[3];
    z[10]=d[12];
    z[11]=d[4];
    z[12]=d[7];
    z[13]=d[17];
    z[14]=e[5];
    z[15]=e[7];
    z[16]=e[9];
    z[17]=e[12];
    z[18]=e[13];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=e[6];
    z[23]=e[14];
    z[24]=f[30];
    z[25]=f[31];
    z[26]=f[32];
    z[27]=f[33];
    z[28]=f[36];
    z[29]=f[39];
    z[30]=f[43];
    z[31]=f[50];
    z[32]=f[51];
    z[33]=f[52];
    z[34]=f[53];
    z[35]=f[55];
    z[36]=f[58];
    z[37]=f[59];
    z[38]=f[68];
    z[39]=npow(z[3],2);
    z[40]=n<T>(1,2)*z[39];
    z[41]=z[1]*i;
    z[42]=z[41]*z[3];
    z[43]= - z[40] - n<T>(11,3)*z[42];
    z[44]=n<T>(1,2)*z[4];
    z[45]= - z[41] + z[44];
    z[46]=n<T>(1,3)*z[4];
    z[45]=z[45]*z[46];
    z[47]=n<T>(1,6)*z[12];
    z[48]=n<T>(11,2)*z[3] - 5*z[4];
    z[48]=z[48]*z[47];
    z[49]=z[4] - z[3];
    z[50]=z[9]*z[49];
    z[43]=z[48] - n<T>(7,6)*z[50] + n<T>(1,2)*z[43] + z[45];
    z[43]=z[12]*z[43];
    z[45]=z[39]*z[41];
    z[48]=13*z[3];
    z[50]= - n<T>(49,6)*z[4] + z[48] - n<T>(5,2)*z[41];
    z[50]=z[4]*z[50];
    z[50]=n<T>(19,2)*z[39] + z[50];
    z[50]=z[4]*z[50];
    z[51]=npow(z[3],3);
    z[45]=z[50] - n<T>(1,3)*z[51] - n<T>(25,4)*z[45];
    z[50]= - n<T>(41,2)*z[39] - 23*z[42];
    z[51]= - 23*z[3] + 47*z[41];
    z[51]=n<T>(1,4)*z[51] + n<T>(31,3)*z[7];
    z[51]=z[7]*z[51];
    z[50]=n<T>(1,2)*z[50] + z[51];
    z[50]=z[7]*z[50];
    z[45]=n<T>(1,2)*z[45] + z[50];
    z[50]=z[41] + z[3];
    z[51]= - z[44] + z[50];
    z[51]=z[51]*z[44];
    z[51]=z[51] - z[39] + z[42];
    z[48]= - n<T>(31,2)*z[7] + z[48] + 31*z[41];
    z[52]=n<T>(1,2)*z[7];
    z[48]=z[48]*z[52];
    z[48]=7*z[51] + z[48];
    z[51]=n<T>(1,2)*z[9];
    z[53]=z[41] - z[9];
    z[54]= - n<T>(13,6)*z[7] - n<T>(7,6)*z[4] - z[3] - n<T>(13,3)*z[53];
    z[54]=z[54]*z[51];
    z[48]=n<T>(1,3)*z[48] + z[54];
    z[48]=z[9]*z[48];
    z[54]=n<T>(1,3)*z[5];
    z[55]= - z[54] + 13*z[7] - n<T>(41,2)*z[3] - 5*z[41];
    z[55]=z[5]*z[55];
    z[55]=z[55] + n<T>(31,2)*z[39] + 5*z[42];
    z[56]= - 4*z[7] + 5*z[3] - 2*z[41];
    z[56]=z[7]*z[56];
    z[55]=z[56] + n<T>(1,4)*z[55];
    z[54]=z[55]*z[54];
    z[43]= - z[38] + z[54] + z[43] + n<T>(1,3)*z[45] + z[48];
    z[45]=n<T>(1,3)*z[11];
    z[48]=n<T>(1,3)*z[41];
    z[54]= - z[3] + z[48];
    z[47]=z[45] - z[47] + n<T>(1,2)*z[54] + z[46];
    z[47]=z[11]*z[47];
    z[40]=z[40] + z[42];
    z[47]=z[40] - z[47];
    z[54]=z[41]*z[46];
    z[48]=z[3] + z[48];
    z[46]= - n<T>(1,12)*z[12] + n<T>(1,2)*z[48] - z[46];
    z[46]=z[12]*z[46];
    z[46]=z[46] + z[54] - n<T>(1,2)*z[47];
    z[46]=z[46]*z[45];
    z[47]=n<T>(1,2)*z[12];
    z[48]= - n<T>(1,4)*z[2] + z[47] + z[51];
    z[48]=z[49]*z[48];
    z[49]=z[50] + z[4];
    z[44]= - z[49]*z[44];
    z[39]=z[44] + z[39] + n<T>(1,2)*z[42] + z[48];
    z[39]=z[2]*z[39];
    z[42]=n<T>(53,4)*z[7] - n<T>(5,2)*z[3] - z[4];
    z[42]=z[45] - n<T>(5,3)*z[6] - n<T>(41,24)*z[5] + z[47] + n<T>(1,3)*z[42] - n<T>(13,4)*z[9];
    z[42]=z[19]*z[42];
    z[44]= - z[12] + z[53] + z[2];
    z[44]= - 4*z[4] - n<T>(13,2)*z[3] + 7*z[44];
    z[44]=z[15]*z[44];
    z[45]=z[4] + z[11];
    z[45]=z[22]*z[45];
    z[42]= - z[24] + z[45] + z[42] + z[44];
    z[44]=z[12] + z[4];
    z[45]=z[41]*z[44];
    z[45]=z[19] + z[45];
    z[45]=z[13]*z[45];
    z[44]= - z[41] + z[44];
    z[44]=z[16]*z[44];
    z[44]=z[45] + z[44];
    z[45]=z[41] + n<T>(7,3)*z[7];
    z[45]=z[7]*z[45];
    z[40]=n<T>(5,2)*z[40] + z[45];
    z[45]=n<T>(1,2)*z[6];
    z[47]=z[5] + n<T>(17,3)*z[3] - z[41];
    z[47]= - n<T>(25,27)*z[6] - n<T>(19,9)*z[7] + n<T>(1,2)*z[47];
    z[47]=z[47]*z[45];
    z[48]= - n<T>(5,3)*z[3] - z[41];
    z[48]=n<T>(7,12)*z[5] + n<T>(1,2)*z[48] - n<T>(1,3)*z[7];
    z[48]=z[5]*z[48];
    z[40]=z[47] + n<T>(1,3)*z[40] + z[48];
    z[40]=z[40]*z[45];
    z[45]=n<T>(1,3)*z[6] - n<T>(1,6)*z[5] + z[52] - z[3];
    z[47]= - z[41]*z[45];
    z[47]=n<T>(1,6)*z[19] + z[47];
    z[47]=z[8]*z[47];
    z[45]=n<T>(1,6)*z[41] + z[45];
    z[45]=z[17]*z[45];
    z[48]=z[7] + n<T>(13,9)*z[9];
    z[50]= - n<T>(4,9)*z[3] - z[48];
    z[50]=z[41]*z[50];
    z[50]= - n<T>(13,9)*z[19] + z[50];
    z[50]=z[10]*z[50];
    z[41]=4*z[3] - 13*z[41];
    z[41]=n<T>(1,9)*z[41] + z[48];
    z[41]=z[14]*z[41];
    z[48]= - z[7] - z[6];
    z[48]=z[23]*z[48];
    z[48]=z[21] + z[48] + z[31];
    z[49]= - z[9] + z[49];
    z[49]=z[18]*z[49];
    z[51]=z[20]*i;

    r += n<T>(25,36)*z[25] + n<T>(7,12)*z[26] + n<T>(19,9)*z[27] + n<T>(1,36)*z[28]
     - n<T>(1,12)*z[29]
     - n<T>(7,18)*z[30]
     - n<T>(5,72)*z[32]
     + n<T>(31,36)*z[33] - 4*
      z[34] + n<T>(7,72)*z[35] + n<T>(17,24)*z[36] + n<T>(2,9)*z[37] + n<T>(7,9)*z[39]
       + z[40] + z[41] + n<T>(1,9)*z[42] + n<T>(1,3)*z[43] + n<T>(2,3)*z[44] + 
      z[45] + z[46] + z[47] + n<T>(11,18)*z[48] + z[49] + z[50] + n<T>(35,216)*
      z[51];
 
    return r;
}

template std::complex<double> qg_2lha_tf499(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf499(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
