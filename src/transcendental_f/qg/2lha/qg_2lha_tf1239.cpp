#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1239(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[70];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[4];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[16];
    z[9]=d[1];
    z[10]=d[15];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=e[10];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[17];
    z[21]=c[18];
    z[22]=c[19];
    z[23]=c[23];
    z[24]=c[25];
    z[25]=c[26];
    z[26]=c[28];
    z[27]=c[29];
    z[28]=c[30];
    z[29]=c[31];
    z[30]=e[3];
    z[31]=f[0];
    z[32]=f[1];
    z[33]=f[2];
    z[34]=f[3];
    z[35]=f[4];
    z[36]=f[6];
    z[37]=f[7];
    z[38]=f[9];
    z[39]=f[10];
    z[40]=f[11];
    z[41]=f[12];
    z[42]=f[14];
    z[43]=f[16];
    z[44]=f[17];
    z[45]=f[18];
    z[46]=f[21];
    z[47]=f[23];
    z[48]=f[76];
    z[49]=3*z[4];
    z[50]=n<T>(7,2)*z[3];
    z[51]=n<T>(1,2)*z[7];
    z[52]=n<T>(1,2)*z[9];
    z[53]=z[51] - z[50] + n<T>(13,2)*z[2] - z[49] - z[52];
    z[53]=z[6]*z[53];
    z[54]=z[9] - z[4];
    z[55]=z[54] + z[2];
    z[56]=z[1]*i;
    z[57]=z[51] - z[56] - z[55];
    z[51]=z[57]*z[51];
    z[57]=3*z[56];
    z[58]=n<T>(1,2)*z[4];
    z[59]=z[57] - z[58];
    z[59]=z[4]*z[59];
    z[60]=z[56] - z[4];
    z[61]=z[60] + z[52];
    z[62]=z[61]*z[52];
    z[63]=n<T>(1,2)*z[2];
    z[64]= - z[9] + 13*z[56];
    z[65]=n<T>(5,2)*z[2] - 5*z[4] - z[64];
    z[65]=z[65]*z[63];
    z[66]= - n<T>(1,2)*z[3] + z[56] + z[4];
    z[50]=z[66]*z[50];
    z[50]=n<T>(1,2)*z[53] + z[51] + z[50] + z[65] + z[59] + z[62];
    z[51]=n<T>(1,4)*z[6];
    z[50]=z[50]*z[51];
    z[53]= - z[57] + z[52] - z[4];
    z[53]=z[53]*z[52];
    z[54]=z[54] + z[63];
    z[59]=9*z[56];
    z[62]= - z[59] + z[54];
    z[62]=z[62]*z[63];
    z[59]=n<T>(9,2)*z[5] - z[59] + z[55];
    z[65]=n<T>(1,2)*z[5];
    z[59]=z[59]*z[65];
    z[66]=n<T>(1,4)*z[9];
    z[57]= - n<T>(1,3)*z[3] - n<T>(1,4)*z[5] + n<T>(7,4)*z[2] - z[57] + z[66];
    z[57]=z[3]*z[57];
    z[67]=z[56] - n<T>(3,2)*z[4];
    z[67]=z[4]*z[67];
    z[53]=z[57] + z[59] + z[62] + z[67] + z[53];
    z[53]=z[3]*z[53];
    z[57]=z[4] + z[9];
    z[57]=z[56]*z[57];
    z[57]=z[17] + z[57];
    z[57]=z[10]*z[57];
    z[59]=z[9] - z[60];
    z[59]=z[13]*z[59];
    z[53]=z[46] + z[53] + z[57] + z[59] + z[47];
    z[57]=z[61]*z[9];
    z[58]=z[58] - z[56];
    z[59]=z[58]*z[4];
    z[61]=z[59] - n<T>(13,8)*z[57];
    z[54]=z[54] + z[56];
    z[62]=n<T>(1,8)*z[2];
    z[54]=z[54]*z[62];
    z[67]=n<T>(1,3)*z[5];
    z[68]= - n<T>(29,8)*z[9] - z[60];
    z[68]=n<T>(1,3)*z[68] - n<T>(9,8)*z[2];
    z[68]=n<T>(1,2)*z[68] - z[67];
    z[68]=z[5]*z[68];
    z[61]=z[68] + n<T>(1,3)*z[61] - z[54];
    z[61]=z[5]*z[61];
    z[68]=2*z[56];
    z[69]= - z[68] + z[4];
    z[69]=z[4]*z[69];
    z[57]=z[69] - n<T>(5,8)*z[57];
    z[65]=z[65] + z[60];
    z[65]=z[5]*z[65];
    z[69]= - n<T>(37,16)*z[9] - z[60];
    z[69]= - n<T>(1,6)*z[7] + n<T>(7,16)*z[5] + n<T>(1,3)*z[69] - n<T>(1,16)*z[2];
    z[69]=z[7]*z[69];
    z[54]=z[69] + n<T>(7,8)*z[65] + n<T>(1,3)*z[57] + z[54];
    z[54]=z[7]*z[54];
    z[57]=n<T>(11,3)*z[9] - n<T>(1,2)*z[56] - z[4];
    z[57]=z[57]*z[66];
    z[57]= - z[59] + z[57];
    z[57]=z[9]*z[57];
    z[49]= - n<T>(19,3)*z[2] + z[49] + z[64];
    z[49]=z[49]*z[62];
    z[49]=z[59] + z[49];
    z[49]=z[2]*z[49];
    z[59]=z[2] - z[4];
    z[59]= - z[3] - n<T>(5,4)*z[5] - z[66] + z[56] - n<T>(1,4)*z[59];
    z[59]=z[14]*z[59];
    z[52]=z[52] - z[67];
    z[52]=n<T>(1,8)*z[6] - n<T>(29,72)*z[7] + n<T>(5,12)*z[52] - z[3];
    z[52]=z[17]*z[52];
    z[55]=5*z[5] + z[55];
    z[55]=n<T>(1,4)*z[55] + z[3];
    z[55]=z[56]*z[55];
    z[55]=z[17] + z[55];
    z[55]=z[8]*z[55];
    z[56]= - z[2] + n<T>(13,3)*z[9] - z[60];
    z[51]=z[51] + n<T>(1,4)*z[56] + n<T>(4,3)*z[7];
    z[51]=z[16]*z[51];
    z[56]= - z[63] - z[58];
    z[56]=z[12]*z[56];
    z[58]=z[35] - z[33];
    z[60]=z[38] - z[36];
    z[62]=z[41] - z[22];
    z[63]=z[44] - z[43];
    z[64]=z[45] + z[39];
    z[65]= - n<T>(9,2)*z[25] - n<T>(13,8)*z[23] - n<T>(1,4)*z[20] - n<T>(1,24)*z[18];
    z[65]=i*z[65];
    z[66]=z[68] - z[2];
    z[67]= - z[3] + z[66];
    z[67]=z[11]*z[67];
    z[66]= - z[6] + z[66];
    z[66]=z[15]*z[66];
    z[68]=z[9] + z[5];
    z[68]=z[30]*z[68];

    r +=  - n<T>(13,8)*z[19] - z[21] + n<T>(25,24)*z[24] + n<T>(15,4)*z[26] + n<T>(3,4)
      *z[27] + 3*z[28] - n<T>(91,16)*z[29] - z[31] + n<T>(9,16)*z[32] - n<T>(1,8)*
      z[34] + z[37] - n<T>(3,8)*z[40] - n<T>(1,16)*z[42] + z[48] + z[49] + 
      z[50] + z[51] + z[52] + n<T>(1,4)*z[53] + z[54] + z[55] + n<T>(1,2)*z[56]
       + z[57] + n<T>(1,12)*z[58] + z[59] + n<T>(3,16)*z[60] + z[61] - n<T>(1,6)*
      z[62] - n<T>(7,16)*z[63] - n<T>(1,3)*z[64] + z[65] + z[66] + z[67] + n<T>(2,3)
      *z[68];
 
    return r;
}

template std::complex<double> qg_2lha_tf1239(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1239(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
