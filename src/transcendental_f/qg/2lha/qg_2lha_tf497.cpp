#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf497(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[27];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[11];
    z[6]=d[9];
    z[7]=e[12];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=e[13];
    z[12]=e[14];
    z[13]=f[50];
    z[14]=f[51];
    z[15]=f[53];
    z[16]=f[55];
    z[17]=f[58];
    z[18]=npow(z[3],2);
    z[19]=n<T>(5,6)*z[4];
    z[20]= - z[3] + z[19];
    z[20]=z[4]*z[20];
    z[21]=z[3] + 2*z[4];
    z[21]=n<T>(1,3)*z[21];
    z[22]= - z[21] + n<T>(1,2)*z[2];
    z[22]=z[2]*z[22];
    z[23]= - z[3] + z[6];
    z[23]=z[6]*z[23];
    z[20]=n<T>(1,2)*z[23] + z[22] + n<T>(1,6)*z[18] + z[20];
    z[20]=z[6]*z[20];
    z[22]=5*z[4];
    z[23]= - n<T>(1,2)*z[3] + n<T>(1,3)*z[4];
    z[23]=z[23]*z[22];
    z[23]= - n<T>(1,2)*z[18] + z[23];
    z[23]=z[4]*z[23];
    z[19]= - z[19] + 3*z[3];
    z[19]=z[19]*z[4];
    z[18]=z[19] - n<T>(7,6)*z[18];
    z[19]=z[22] + 7*z[3];
    z[22]= - n<T>(1,6)*z[19] + z[2];
    z[22]=z[2]*z[22];
    z[22]=z[22] + z[18];
    z[22]=z[2]*z[22];
    z[24]=npow(z[3],3);
    z[22]=z[22] + n<T>(7,3)*z[24] + z[23];
    z[23]=n<T>(1,3)*z[19] - z[2];
    z[23]=z[2]*z[23];
    z[18]=z[23] - z[18];
    z[21]=z[6]*z[21];
    z[18]=n<T>(1,2)*z[18] + z[21];
    z[21]=z[1]*i;
    z[18]=z[18]*z[21];
    z[23]=z[6] + z[2];
    z[24]= - z[23]*z[21];
    z[24]=z[24] - z[8];
    z[24]=z[5]*z[24];
    z[21]= - z[21] + z[23];
    z[21]=z[7]*z[21];
    z[19]=n<T>(1,9)*z[19] - n<T>(1,4)*z[2];
    z[19]=z[8]*z[19];
    z[23]=z[6] + z[3];
    z[23]=z[4] + n<T>(7,3)*z[23];
    z[23]=z[11]*z[23];
    z[25]=z[4] + z[6];
    z[25]=z[12]*z[25];
    z[26]=z[9]*i;

    r += z[10] - n<T>(2,3)*z[13] - n<T>(7,12)*z[14] - 2*z[15] + n<T>(5,12)*z[16]
     - 
      n<T>(5,4)*z[17] + z[18] + z[19] + z[20] + z[21] + n<T>(1,2)*z[22] + z[23]
       + z[24] + n<T>(5,3)*z[25] + n<T>(1,12)*z[26];
 
    return r;
}

template std::complex<double> qg_2lha_tf497(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf497(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
