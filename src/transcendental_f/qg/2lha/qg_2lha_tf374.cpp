#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf374(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[117];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[18];
    z[11]=d[4];
    z[12]=d[9];
    z[13]=d[15];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[17];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[21];
    z[37]=c[23];
    z[38]=c[25];
    z[39]=c[26];
    z[40]=c[28];
    z[41]=c[29];
    z[42]=c[31];
    z[43]=e[14];
    z[44]=e[3];
    z[45]=e[6];
    z[46]=f[0];
    z[47]=f[1];
    z[48]=f[2];
    z[49]=f[3];
    z[50]=f[4];
    z[51]=f[5];
    z[52]=f[6];
    z[53]=f[7];
    z[54]=f[8];
    z[55]=f[9];
    z[56]=f[10];
    z[57]=f[11];
    z[58]=f[12];
    z[59]=f[13];
    z[60]=f[14];
    z[61]=f[16];
    z[62]=f[17];
    z[63]=f[18];
    z[64]=f[20];
    z[65]=f[21];
    z[66]=f[23];
    z[67]=f[30];
    z[68]=f[31];
    z[69]=f[32];
    z[70]=f[33];
    z[71]=f[36];
    z[72]=f[39];
    z[73]=f[43];
    z[74]=f[50];
    z[75]=f[51];
    z[76]=f[52];
    z[77]=f[53];
    z[78]=f[54];
    z[79]=f[55];
    z[80]=f[56];
    z[81]=f[57];
    z[82]=f[58];
    z[83]=f[64];
    z[84]=f[65];
    z[85]=f[66];
    z[86]=f[67];
    z[87]=f[78];
    z[88]=n<T>(1,2)*z[5];
    z[89]=z[1]*i;
    z[90]=z[88] - z[89];
    z[91]=n<T>(1,2)*z[8];
    z[92]=n<T>(1,2)*z[9];
    z[93]=n<T>(1,2)*z[6];
    z[94]= - z[92] - z[93] + z[91] + z[90];
    z[94]=z[12]*z[94];
    z[95]=7*z[5];
    z[96]= - n<T>(5,2)*z[8] - 19*z[89] + z[95];
    z[96]=z[8]*z[96];
    z[95]=59*z[89] + z[95];
    z[95]= - 13*z[2] + n<T>(1,2)*z[95] - 7*z[8];
    z[95]=z[2]*z[95];
    z[95]=z[96] + z[95];
    z[96]=4*z[5];
    z[97]= - n<T>(47,16)*z[89] + z[96];
    z[97]=z[5]*z[97];
    z[95]=z[97] + n<T>(1,8)*z[95];
    z[97]=z[89] - z[5];
    z[98]=z[93] + z[97];
    z[98]=z[6]*z[98];
    z[99]=z[2] - z[8];
    z[100]= - n<T>(85,2)*z[97] - 31*z[99];
    z[100]=n<T>(43,12)*z[9] + n<T>(1,12)*z[100] + z[6];
    z[100]=z[100]*z[92];
    z[101]= - z[89] + n<T>(1,2)*z[4];
    z[102]=z[4]*z[101];
    z[103]=n<T>(37,2)*z[89] - 5*z[5];
    z[103]= - n<T>(119,3)*z[3] - n<T>(85,2)*z[9] + n<T>(107,2)*z[2] + 5*z[103] - 19*
    z[8];
    z[103]=z[3]*z[103];
    z[94]=n<T>(1,48)*z[103] + z[94] + n<T>(15,16)*z[102] + z[100] + n<T>(1,3)*z[95]
    + n<T>(3,4)*z[98];
    z[94]=z[3]*z[94];
    z[90]=z[90]*z[5];
    z[95]=n<T>(1,2)*z[2];
    z[98]= - z[89] - z[5];
    z[98]=n<T>(5,4)*z[2] + n<T>(3,2)*z[98] - z[8];
    z[98]=z[98]*z[95];
    z[100]= - n<T>(1,4)*z[8] + n<T>(1,2)*z[89] + z[5];
    z[100]=z[8]*z[100];
    z[98]=z[98] - n<T>(1,4)*z[90] + z[100];
    z[100]=3*z[5];
    z[102]=3*z[2];
    z[103]= - n<T>(39,8)*z[6] + z[102] + n<T>(23,4)*z[89] - z[100];
    z[103]=z[103]*z[93];
    z[104]=n<T>(13,8)*z[89];
    z[105]=3*z[8];
    z[102]= - n<T>(13,16)*z[9] + n<T>(55,8)*z[6] - z[102] + z[104] + z[105];
    z[102]=z[9]*z[102];
    z[106]=23*z[89];
    z[107]=3*z[4] - n<T>(13,4)*z[9] - n<T>(23,4)*z[6] + n<T>(9,2)*z[2] - z[105] - 
    z[106] - n<T>(3,2)*z[5];
    z[108]=n<T>(1,4)*z[4];
    z[107]=z[107]*z[108];
    z[98]=z[107] + z[102] + 3*z[98] + z[103];
    z[98]=z[98]*z[108];
    z[102]=n<T>(49,16)*z[9] + z[6] - z[95] + z[91] + z[104] + z[5];
    z[102]=z[9]*z[102];
    z[103]=z[91] - z[89];
    z[104]= - z[103]*z[105];
    z[107]=z[95] - n<T>(11,2)*z[5] + z[105];
    z[107]=z[107]*z[95];
    z[108]= - 9*z[89] + n<T>(25,2)*z[5];
    z[108]=z[5]*z[108];
    z[106]=z[106] - 17*z[5];
    z[106]=n<T>(39,4)*z[6] + n<T>(1,2)*z[106] + z[2];
    z[106]=z[6]*z[106];
    z[102]=z[102] + n<T>(1,4)*z[106] + z[107] + n<T>(1,8)*z[108] + z[104];
    z[104]=z[2] + z[5];
    z[104]=n<T>(17,32)*z[4] - n<T>(21,16)*z[9] - n<T>(3,4)*z[8] - z[93] - z[89] + n<T>(3,8)*z[104];
    z[104]=z[4]*z[104];
    z[106]=17*z[89] - 23*z[5];
    z[105]= - n<T>(23,6)*z[12] + 7*z[4] + z[9] + n<T>(15,4)*z[6] - n<T>(5,2)*z[2]
     + 
   n<T>(1,4)*z[106] - z[105];
    z[105]=z[12]*z[105];
    z[102]=n<T>(1,8)*z[105] + n<T>(1,2)*z[102] + z[104];
    z[102]=z[12]*z[102];
    z[104]=z[2] - z[5];
    z[105]=z[89] - z[8];
    z[106]=n<T>(1081,2)*z[6] - 155*z[105] - 143*z[104];
    z[106]=n<T>(1,3)*z[106] + n<T>(265,2)*z[7];
    z[106]=z[23]*z[106];
    z[107]=43*z[5] + n<T>(77,2) + 15*z[6];
    z[108]=z[89]*z[107];
    z[108]=43*z[30] + z[108];
    z[108]=z[16]*z[108];
    z[107]=43*z[89] - z[107];
    z[107]=z[22]*z[107];
    z[106]= - z[78] + z[107] + z[106] + z[108] + z[86] - z[84];
    z[107]=31*z[5];
    z[108]= - n<T>(13,2)*z[8] - 53*z[89] - z[107];
    z[108]=z[8]*z[108];
    z[109]=13*z[89];
    z[107]= - n<T>(13,2)*z[2] + 13*z[8] - z[109] + z[107];
    z[107]=z[2]*z[107];
    z[107]=z[107] - n<T>(257,4)*z[90] + z[108];
    z[108]=37*z[89] - n<T>(257,8)*z[5] - n<T>(29,2)*z[99];
    z[108]= - n<T>(311,36)*z[9] + n<T>(1,3)*z[108] - n<T>(71,8)*z[6];
    z[108]=z[9]*z[108];
    z[110]= - n<T>(39,32)*z[6] - n<T>(39,16)*z[89] - z[5];
    z[110]=z[6]*z[110];
    z[107]=n<T>(1,4)*z[108] + n<T>(1,12)*z[107] + z[110];
    z[107]=z[107]*z[92];
    z[103]=z[8]*z[103];
    z[103]=z[90] - 191*z[103];
    z[108]= - z[93] - z[105];
    z[108]=z[6]*z[108];
    z[110]= - z[92] - z[97];
    z[110]=z[9]*z[110];
    z[103]=29*z[110] + n<T>(1,3)*z[103] + 191*z[108];
    z[108]=z[89]*z[4];
    z[103]=n<T>(1,2)*z[103] - 15*z[108];
    z[110]= - n<T>(191,2)*z[8] + 95*z[89] + z[88];
    z[111]=z[4] - z[2];
    z[110]=n<T>(161,3)*z[11] + n<T>(43,3)*z[3] - n<T>(29,2)*z[9] - n<T>(191,2)*z[6]
     + n<T>(1,3)*z[110]
     + 15*z[111];
    z[110]=z[11]*z[110];
    z[111]=n<T>(1,2)*z[3] + z[97];
    z[111]=z[3]*z[111];
    z[103]=n<T>(1,8)*z[110] + n<T>(1,4)*z[103] + n<T>(11,3)*z[111];
    z[103]=z[11]*z[103];
    z[110]=119*z[5];
    z[111]= - 223*z[89] + z[110];
    z[111]=n<T>(1,6)*z[111] + 57*z[8];
    z[111]=z[8]*z[111];
    z[112]=z[95] + z[97] - z[8];
    z[112]=z[2]*z[112];
    z[90]=n<T>(119,6)*z[112] + n<T>(167,6)*z[90] + z[111];
    z[111]=n<T>(119,16)*z[2] - 2*z[8] + 5*z[89] - n<T>(167,16)*z[5];
    z[111]=n<T>(1,3)*z[111] - n<T>(43,8)*z[6];
    z[111]=z[6]*z[111];
    z[112]=n<T>(1,2)*z[11] + z[105];
    z[112]=z[11]*z[112];
    z[113]=n<T>(881,18)*z[7] - n<T>(715,6)*z[6] + n<T>(119,3)*z[2] + n<T>(223,6)*z[8]
     + 
   57*z[89] + n<T>(119,6)*z[5];
    z[113]=z[7]*z[113];
    z[90]=n<T>(1,16)*z[113] + n<T>(191,48)*z[112] + n<T>(1,8)*z[90] + z[111];
    z[90]=z[7]*z[90];
    z[108]=z[108] + z[30];
    z[111]= - z[12]*z[89];
    z[111]=z[111] - z[108];
    z[111]=z[14]*z[111];
    z[112]=n<T>(1163,3)*z[5] - 769*z[8];
    z[112]=n<T>(2353,3)*z[6] + n<T>(1,2)*z[112] - n<T>(343,3)*z[2];
    z[112]=n<T>(1,2)*z[112] + 101*z[9];
    z[112]= - n<T>(215,9)*z[7] + n<T>(805,36)*z[11] - n<T>(79,18)*z[3] + n<T>(79,6)*
    z[12] + n<T>(1,3)*z[112] - n<T>(107,4)*z[4];
    z[112]=z[30]*z[112];
    z[113]=z[89] - z[4];
    z[114]=z[12] - z[113];
    z[114]=z[28]*z[114];
    z[111]= - z[114] - z[112] - z[111] + z[62] + z[34];
    z[112]= - z[89] - n<T>(13,4)*z[5];
    z[88]=z[112]*z[88];
    z[112]= - n<T>(7,2)*z[8] + z[89] - n<T>(71,8)*z[5];
    z[112]=z[8]*z[112];
    z[88]=z[88] + z[112];
    z[96]=n<T>(89,32)*z[8] - n<T>(181,32)*z[89] + z[96];
    z[96]=n<T>(1,3)*z[96] - n<T>(9,32)*z[2];
    z[96]=z[2]*z[96];
    z[88]=n<T>(1,6)*z[88] + z[96];
    z[88]=z[2]*z[88];
    z[96]= - n<T>(1177,4)*z[8] + n<T>(1177,2)*z[89] - z[110];
    z[91]=z[96]*z[91];
    z[96]= - z[89] + 19*z[5];
    z[96]= - n<T>(227,2)*z[2] + 5*z[96] + 119*z[8];
    z[96]=z[96]*z[95];
    z[110]= - 137*z[89] + n<T>(113,2)*z[5];
    z[110]=z[5]*z[110];
    z[91]=z[96] + z[110] + z[91];
    z[96]=77 + n<T>(349,3)*z[89];
    z[96]=n<T>(2851,144)*z[6] - n<T>(101,12)*z[2] - n<T>(509,96)*z[8] + n<T>(1,16)*z[96]
    + n<T>(23,3)*z[5];
    z[96]=z[6]*z[96];
    z[91]=n<T>(1,12)*z[91] + z[96];
    z[91]=z[91]*z[93];
    z[93]=z[11]*z[89];
    z[93]=z[93] + z[108];
    z[93]=z[15]*z[93];
    z[96]= - z[11] + z[113];
    z[96]=z[21]*z[96];
    z[93]= - z[93] - z[96] + z[74] - z[65];
    z[96]= - z[8] - z[7];
    z[96]=z[89]*z[96];
    z[96]= - z[30] + z[96];
    z[96]=z[17]*z[96];
    z[105]= - z[7] + z[105];
    z[105]=z[25]*z[105];
    z[96]=z[96] + z[105];
    z[105]= - z[5] - z[3];
    z[105]=z[89]*z[105];
    z[105]= - z[30] + z[105];
    z[105]=z[13]*z[105];
    z[108]= - z[3] + z[97];
    z[108]=z[20]*z[108];
    z[105]=z[105] + z[108];
    z[108]= - 209*z[89] + 109*z[5];
    z[108]=n<T>(1,4)*z[108] + 19*z[2];
    z[108]=n<T>(1,3)*z[108] - 5*z[6];
    z[108]=n<T>(5,2)*z[12] + n<T>(1,2)*z[108] + z[4];
    z[108]=z[19]*z[108];
    z[97]=z[97] + z[2];
    z[110]=z[12] - z[4] + z[97];
    z[110]=z[29]*z[110];
    z[108]=z[108] + z[110];
    z[110]= - n<T>(77,4) + 29*z[89];
    z[100]=n<T>(1,8)*z[110] - z[100];
    z[100]=z[100]*npow(z[5],2);
    z[109]= - z[109] + n<T>(35,4)*z[5];
    z[109]=z[5]*z[109];
    z[110]= - 11*z[89] + n<T>(37,12)*z[5];
    z[110]=n<T>(1,4)*z[110] + n<T>(1,3)*z[8];
    z[110]=z[8]*z[110];
    z[109]=n<T>(1,24)*z[109] + z[110];
    z[109]=z[8]*z[109];
    z[92]= - n<T>(1,2)*z[12] + n<T>(3,2)*z[4] + z[92] + n<T>(61,48)*z[2] + n<T>(85,48)*
    z[8] - n<T>(85,24)*z[89] - z[5];
    z[92]=z[24]*z[92];
    z[104]=n<T>(11,4)*z[9] + n<T>(7,4)*z[8] + z[3] + z[104];
    z[110]=z[89]*z[104];
    z[110]=n<T>(7,4)*z[30] + z[110];
    z[110]=z[10]*z[110];
    z[89]=n<T>(7,4)*z[89] - z[104];
    z[89]=z[27]*z[89];
    z[95]= - z[95] - z[101];
    z[95]=z[18]*z[95];
    z[101]=z[57] + z[40];
    z[104]=z[60] - z[47];
    z[112]=z[63] + z[59];
    z[113]=z[85] + z[83];
    z[114]=z[11] + z[7];
    z[114]=z[45]*z[114];
    z[114]=z[114] - z[67];
    z[115]=n<T>(119,24)*z[39] + n<T>(73,24)*z[37] - n<T>(37,16)*z[36] - z[33] - n<T>(685,288)*z[31];
    z[115]=i*z[115];
    z[97]= - z[8] + z[97];
    z[97]= - n<T>(181,4)*z[3] + 19*z[97] - n<T>(257,4)*z[9];
    z[97]=z[26]*z[97];
    z[99]=n<T>(9,16)*z[12] - n<T>(7,16)*z[9] - z[6] + z[99];
    z[99]=z[43]*z[99];
    z[116]=z[3] + z[11];
    z[116]=z[44]*z[116];

    r += n<T>(953,192)*z[32] + n<T>(37,288)*z[35] + n<T>(9,32)*z[38] + n<T>(3,64)*z[41]
       + n<T>(1285,384)*z[42] - n<T>(43,16)*z[46] - n<T>(11,12)*z[48] - n<T>(11,32)*
      z[49] - n<T>(409,192)*z[50] - n<T>(211,96)*z[51] - n<T>(19,16)*z[52] - n<T>(19,3)
      *z[53] + n<T>(11,4)*z[54] - n<T>(17,16)*z[55] - n<T>(47,48)*z[56] - n<T>(1,192)*
      z[58] - n<T>(29,64)*z[61] + n<T>(7,12)*z[64] + n<T>(19,24)*z[66] - n<T>(551,64)*
      z[68] - n<T>(119,32)*z[69] - n<T>(149,6)*z[70] + n<T>(191,192)*z[71] - n<T>(191,64)*z[72]
     + n<T>(119,48)*z[73] - n<T>(31,64)*z[75]
     + n<T>(3,32)*z[76]
     + 2*
      z[77] - n<T>(27,64)*z[79] - n<T>(3,2)*z[80] - n<T>(11,8)*z[81] - n<T>(7,64)*z[82]
       + 4*z[87] + z[88] + z[89] + z[90] + z[91] + z[92] - n<T>(15,16)*
      z[93] + z[94] + n<T>(21,4)*z[95] + n<T>(461,48)*z[96] + n<T>(1,12)*z[97] + 
      z[98] + z[99] + z[100] - n<T>(11,48)*z[101] + z[102] + n<T>(1,4)*z[103]
       - n<T>(15,32)*z[104] + n<T>(17,8)*z[105] + n<T>(1,8)*z[106] + z[107] + n<T>(1,2)
      *z[108] + z[109] + z[110] - n<T>(1,16)*z[111] - n<T>(5,16)*z[112] - n<T>(3,8)
      *z[113] + n<T>(191,48)*z[114] + z[115] + n<T>(1,48)*z[116];
 
    return r;
}

template std::complex<double> qg_2lha_tf374(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf374(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
