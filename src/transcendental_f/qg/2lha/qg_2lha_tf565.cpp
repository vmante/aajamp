#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf565(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[65];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[9];
    z[6]=d[1];
    z[7]=d[2];
    z[8]=d[8];
    z[9]=d[11];
    z[10]=d[3];
    z[11]=d[12];
    z[12]=d[4];
    z[13]=d[7];
    z[14]=d[17];
    z[15]=e[5];
    z[16]=e[7];
    z[17]=e[9];
    z[18]=e[12];
    z[19]=e[13];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[31];
    z[24]=e[6];
    z[25]=e[14];
    z[26]=f[30];
    z[27]=f[31];
    z[28]=f[32];
    z[29]=f[33];
    z[30]=f[36];
    z[31]=f[39];
    z[32]=f[43];
    z[33]=f[50];
    z[34]=f[51];
    z[35]=f[52];
    z[36]=f[53];
    z[37]=f[55];
    z[38]=f[58];
    z[39]=2*z[3];
    z[40]=z[1]*i;
    z[41]=z[39]*z[40];
    z[42]=2*z[10];
    z[43]= - z[39] - z[5];
    z[43]=z[43]*z[42];
    z[44]=npow(z[3],2);
    z[45]=9*z[44];
    z[46]=z[3] + 2*z[40];
    z[46]=2*z[46];
    z[47]=z[46] + z[5];
    z[47]=z[5]*z[47];
    z[48]=z[40] + z[3];
    z[49]= - z[4] + z[10] - z[48];
    z[49]=z[4]*z[49];
    z[50]=z[5] - z[3];
    z[51]=2*z[50];
    z[52]= - z[7]*z[51];
    z[53]=z[4] - z[3];
    z[54]=z[2]*z[53];
    z[43]= - 3*z[54] + z[52] + 6*z[49] + z[43] + z[47] + z[45] + z[41];
    z[43]=z[2]*z[43];
    z[47]=n<T>(1,2)*z[3];
    z[49]=n<T>(1,3)*z[5];
    z[52]=n<T>(1,3)*z[40];
    z[54]= - n<T>(1,3)*z[7] + n<T>(1,6)*z[8] - z[49] + z[47] + z[52];
    z[54]=z[7]*z[54];
    z[50]=z[50]*z[42];
    z[55]=z[40]*z[3];
    z[56]=5*z[55];
    z[57]=n<T>(1,2)*z[44];
    z[58]=4*z[3] - z[5];
    z[58]=z[5]*z[58];
    z[59]=n<T>(5,3)*z[40];
    z[60]=n<T>(5,6)*z[8] + n<T>(4,3)*z[5] - 3*z[3] - z[59];
    z[60]=z[8]*z[60];
    z[54]=5*z[54] + z[60] + z[50] + z[58] - z[57] - z[56];
    z[54]=z[7]*z[54];
    z[58]=3*z[10];
    z[60]=n<T>(13,3)*z[40] - z[58];
    z[60]=2*z[60] - n<T>(13,3)*z[4];
    z[60]=z[4]*z[60];
    z[61]=6*z[2];
    z[53]=z[53]*z[61];
    z[62]=6*z[10];
    z[63]=z[3]*z[62];
    z[64]=n<T>(25,2)*z[3] - n<T>(44,3)*z[4];
    z[64]=z[13]*z[64];
    z[53]=z[64] + z[53] + z[60] + z[63] + n<T>(13,2)*z[44] - 25*z[55];
    z[53]=z[13]*z[53];
    z[60]=n<T>(5,3) + z[3];
    z[59]=2*z[60] - z[59];
    z[59]=2*z[59] - z[5];
    z[59]=z[5]*z[59];
    z[60]=z[52] - z[3];
    z[60]=n<T>(1,2)*z[60];
    z[49]= - n<T>(1,3)*z[8] - z[60] - z[49];
    z[49]=z[8]*z[49];
    z[47]= - n<T>(20,3) - z[47];
    z[47]=z[3]*z[47];
    z[47]=5*z[49] + z[50] + z[59] + z[47] + z[56];
    z[47]=z[8]*z[47];
    z[49]=z[12] + z[4];
    z[56]=n<T>(1,6)*z[13];
    z[59]=z[56] - z[60] - n<T>(1,3)*z[49];
    z[59]=z[12]*z[59];
    z[52]=z[56] + n<T>(2,3)*z[4] - z[3] - z[52];
    z[52]=z[13]*z[52];
    z[56]=z[40]*z[4];
    z[52]=z[59] + z[52] - n<T>(2,3)*z[56] + z[57] + z[55];
    z[52]=z[12]*z[52];
    z[48]=2*z[48];
    z[57]=z[48] + z[5];
    z[57]=z[5]*z[57];
    z[51]= - z[8]*z[51];
    z[50]=z[51] - z[50] + z[57] - 3*z[44] - z[41];
    z[50]=z[6]*z[50];
    z[51]=z[7] + z[5];
    z[57]=z[40]*z[51];
    z[57]=z[20] + z[57];
    z[57]=z[9]*z[57];
    z[51]=z[40] - z[51];
    z[51]=z[18]*z[51];
    z[59]= - z[5] - z[8];
    z[59]=z[25]*z[59];
    z[51]=z[59] + z[57] + z[51];
    z[57]=z[40] - z[13];
    z[59]=z[61] - 17*z[4] - z[62] - 23*z[3] + 6*z[57];
    z[59]=z[16]*z[59];
    z[39]=z[39] - z[40];
    z[60]=z[6] + z[2];
    z[39]=z[4] + 3*z[39] + 5*z[10] - 2*z[60];
    z[39]=z[19]*z[39];
    z[39]=z[59] + z[39];
    z[59]=n<T>(13,3) - 12*z[3];
    z[59]=z[3]*z[59];
    z[60]=n<T>(17,3)*z[5] - z[40] - 6*z[3];
    z[60]=z[5]*z[60];
    z[59]=z[60] + z[59] - 4*z[55];
    z[59]=z[5]*z[59];
    z[41]= - z[44] + z[41];
    z[60]= - z[48] + z[5];
    z[60]=z[5]*z[60];
    z[46]=4*z[10] - z[46] + z[5];
    z[46]=z[10]*z[46];
    z[41]=z[46] + 6*z[41] + z[60];
    z[41]=z[10]*z[41];
    z[46]=z[48] - z[10];
    z[46]=z[10]*z[46];
    z[46]=4*z[44] + z[46];
    z[48]= - n<T>(17,3)*z[4] - z[58] + 15*z[3] - n<T>(22,3)*z[40];
    z[48]=z[4]*z[48];
    z[46]=3*z[46] + z[48];
    z[46]=z[4]*z[46];
    z[48]=z[13]*z[40];
    z[48]=z[20] + z[56] + z[48];
    z[48]=z[14]*z[48];
    z[56]=z[4] - z[57];
    z[56]=z[17]*z[56];
    z[48]=z[48] + z[56];
    z[56]= - z[10]*z[40];
    z[55]= - z[20] - z[55] + z[56];
    z[55]=z[11]*z[55];
    z[56]=z[10] + z[3] - z[40];
    z[56]=z[15]*z[56];
    z[55]=z[55] + z[56];
    z[56]= - 13 - 38*z[3];
    z[44]=z[56]*z[44];
    z[44]=z[44] + z[33];
    z[40]= - z[40]*z[45];
    z[45]= - 11*z[3] - n<T>(2,3)*z[5];
    z[42]= - n<T>(26,9)*z[12] + n<T>(31,18)*z[13] + n<T>(5,6)*z[7] - n<T>(10,9)*z[8]
     + n<T>(7,3)*z[4]
     + n<T>(5,3)*z[45] - z[42];
    z[42]=z[20]*z[42];
    z[45]=z[38] + z[34];
    z[49]= - z[24]*z[49];
    z[49]=z[49] + z[26];
    z[56]=z[21]*i;

    r +=  - 16*z[22] + 14*z[23] + n<T>(37,2)*z[27] + 9*z[28] + 48*z[29] - 
      n<T>(13,6)*z[30] + n<T>(13,2)*z[31] - 6*z[32] - z[35] - 24*z[36] - n<T>(5,6)*
      z[37] + 2*z[39] + z[40] + z[41] + z[42] + z[43] + n<T>(1,3)*z[44] + n<T>(5,2)*z[45]
     + z[46]
     + z[47]
     + n<T>(62,3)*z[48]
     + n<T>(26,3)*z[49]
     + z[50]
       + n<T>(10,3)*z[51] + 13*z[52] + z[53] + z[54] + 8*z[55] + n<T>(19,9)*
      z[56] + z[59];
 
    return r;
}

template std::complex<double> qg_2lha_tf565(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf565(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
