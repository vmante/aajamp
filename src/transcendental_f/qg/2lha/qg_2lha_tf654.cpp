#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf654(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[99];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[6];
    z[10]=d[9];
    z[11]=d[15];
    z[12]=d[5];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[7];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=e[10];
    z[25]=e[11];
    z[26]=e[12];
    z[27]=c[3];
    z[28]=c[11];
    z[29]=c[15];
    z[30]=c[19];
    z[31]=c[23];
    z[32]=c[25];
    z[33]=c[26];
    z[34]=c[28];
    z[35]=e[3];
    z[36]=e[6];
    z[37]=e[13];
    z[38]=e[14];
    z[39]=f[0];
    z[40]=f[1];
    z[41]=f[2];
    z[42]=f[3];
    z[43]=f[4];
    z[44]=f[5];
    z[45]=f[6];
    z[46]=f[7];
    z[47]=f[10];
    z[48]=f[11];
    z[49]=f[12];
    z[50]=f[14];
    z[51]=f[16];
    z[52]=f[17];
    z[53]=f[18];
    z[54]=f[23];
    z[55]=f[30];
    z[56]=f[31];
    z[57]=f[33];
    z[58]=f[35];
    z[59]=f[36];
    z[60]=f[37];
    z[61]=f[39];
    z[62]=f[50];
    z[63]=f[51];
    z[64]=f[54];
    z[65]=f[55];
    z[66]=f[56];
    z[67]=f[58];
    z[68]=n<T>(1,2)*z[3];
    z[69]=z[1]*i;
    z[70]=z[68] - z[69];
    z[70]=z[70]*z[3];
    z[71]=5*z[6];
    z[72]= - z[69] - z[71];
    z[73]=z[5] - z[3];
    z[72]=n<T>(5,12)*z[8] + n<T>(1,6)*z[72] + z[73];
    z[74]=n<T>(1,2)*z[8];
    z[72]=z[72]*z[74];
    z[75]=n<T>(1,2)*z[4];
    z[76]=z[75] - z[69];
    z[77]=z[3] - z[76];
    z[77]=z[77]*z[75];
    z[78]=n<T>(1,2)*z[12];
    z[79]=n<T>(1,4)*z[12];
    z[80]= - z[3] + z[79];
    z[80]=z[80]*z[78];
    z[71]=11*z[69] + z[71];
    z[71]= - n<T>(1,3)*z[9] - z[78] - z[4] + n<T>(1,6)*z[71] + z[8];
    z[71]=z[9]*z[71];
    z[81]=z[69] - z[6];
    z[82]=z[6]*z[81];
    z[83]= - z[5]*z[68];
    z[71]=n<T>(1,4)*z[71] + z[80] + z[77] + z[72] + z[83] - n<T>(5,12)*z[82] + 
    z[70];
    z[71]=z[9]*z[71];
    z[72]=n<T>(1,2)*z[6];
    z[77]=z[72] - z[69];
    z[80]=z[77]*z[72];
    z[82]= - z[68] - z[77];
    z[82]=z[3]*z[82];
    z[83]=n<T>(1,2)*z[7];
    z[84]=n<T>(1,3)*z[69];
    z[85]=z[84] + z[3];
    z[86]= - n<T>(1,3)*z[7] + z[85];
    z[86]=z[86]*z[83];
    z[87]=z[7] - z[3];
    z[88]=n<T>(1,2)*z[5];
    z[89]= - z[87]*z[88];
    z[90]=n<T>(1,3)*z[4];
    z[91]=n<T>(1,4)*z[4];
    z[92]=z[7] - z[91];
    z[92]=z[92]*z[90];
    z[85]= - n<T>(1,3)*z[12] - z[90] + z[85];
    z[85]=z[85]*z[78];
    z[90]=z[81] + z[7];
    z[93]=z[10]*z[90];
    z[82]= - n<T>(1,4)*z[93] + z[85] + z[92] + z[89] + z[86] + z[80] + z[82]
   ;
    z[82]=z[10]*z[82];
    z[85]=z[77]*z[6];
    z[86]=n<T>(1,4)*z[6];
    z[89]=2*z[69];
    z[92]= - n<T>(11,6)*z[3] + z[89] + z[86];
    z[92]=z[3]*z[92];
    z[92]=n<T>(5,2)*z[85] + z[92];
    z[92]=z[3]*z[92];
    z[93]= - z[5] + z[81] + z[2];
    z[93]= - 7*z[7] - 5*z[3] + 2*z[93];
    z[93]=z[24]*z[93];
    z[94]=z[89] - z[2];
    z[95]=z[5] - z[94];
    z[95]=z[18]*z[95];
    z[96]=z[6] - z[94];
    z[96]=z[22]*z[96];
    z[97]=z[8] + z[3];
    z[97]=z[35]*z[97];
    z[98]= - z[12] - z[10];
    z[98]=z[37]*z[98];
    z[92]= - z[62] - z[54] - z[53] - z[49] - z[95] - z[96] + z[97] - 
    z[98] - z[92] - z[93] + z[47] - z[44] + z[41] + z[32];
    z[93]=n<T>(1,4)*z[7] - n<T>(1,2)*z[69] + z[73];
    z[93]=z[7]*z[93];
    z[95]=3*z[6];
    z[96]=z[69] + z[95];
    z[96]= - n<T>(3,8)*z[8] + n<T>(1,4)*z[96] - z[73];
    z[96]=z[8]*z[96];
    z[97]=n<T>(5,3)*z[69];
    z[98]=n<T>(5,6)*z[4] - z[97] - z[7];
    z[75]=z[98]*z[75];
    z[84]= - z[84] - z[95];
    z[95]=n<T>(5,3)*z[4];
    z[84]=n<T>(5,3)*z[12] + z[95] - n<T>(3,2)*z[8] + n<T>(1,2)*z[84] + z[7];
    z[79]=z[84]*z[79];
    z[75]=z[79] + z[75] + z[96] - n<T>(7,4)*z[85] + z[93];
    z[75]=z[75]*z[78];
    z[78]= - z[68] - z[81];
    z[78]=z[3]*z[78];
    z[79]=z[89] - z[6];
    z[84]= - z[7] + z[68] + z[79];
    z[84]=z[7]*z[84];
    z[93]=2*z[3];
    z[96]= - z[7] + z[86] + z[93];
    z[96]=z[5]*z[96];
    z[78]=z[96] + z[84] + z[80] + z[78];
    z[80]=n<T>(1,3)*z[5];
    z[78]=z[78]*z[80];
    z[84]=z[69] - z[83];
    z[96]=n<T>(1,6)*z[7];
    z[84]=z[84]*z[96];
    z[98]=z[88] - z[69] - z[6];
    z[98]=z[5]*z[98];
    z[70]=z[98] + z[84] + z[85] - z[70];
    z[73]=z[95] - z[96] - z[97] + z[6] + z[73];
    z[73]=z[73]*z[91];
    z[74]=z[69] - z[74];
    z[74]=z[8]*z[74];
    z[70]=z[73] + n<T>(1,2)*z[70] + z[74];
    z[70]=z[4]*z[70];
    z[73]= - z[88] - z[83] + z[68] - z[72] - z[94];
    z[73]=z[2]*z[73];
    z[74]=z[93] + z[81];
    z[74]=z[3]*z[74];
    z[84]=z[87] - z[5];
    z[85]=z[89] + z[6] + z[84];
    z[85]=z[5]*z[85];
    z[79]=z[6]*z[79];
    z[87]= - z[3] - z[90];
    z[87]=z[7]*z[87];
    z[73]=z[73] + z[85] + z[87] + z[79] + z[74];
    z[74]= - z[4]*z[76];
    z[76]=npow(z[8],2);
    z[73]=z[74] + n<T>(1,2)*z[76] + n<T>(1,3)*z[73];
    z[73]=z[2]*z[73];
    z[74]=z[77]*z[86];
    z[76]=5*z[69];
    z[77]=z[76] - z[68];
    z[77]=z[3]*z[77];
    z[74]=z[74] + z[77];
    z[77]= - z[69] - z[83];
    z[77]=z[7]*z[77];
    z[74]=n<T>(1,3)*z[74] + z[77];
    z[68]=z[89] + z[68];
    z[68]= - z[80] + n<T>(1,3)*z[68] + z[83];
    z[68]=z[5]*z[68];
    z[77]=n<T>(1,3)*z[6];
    z[76]=z[76] + z[77];
    z[76]= - z[7] + n<T>(1,4)*z[76] + n<T>(7,3)*z[3];
    z[76]=n<T>(7,24)*z[8] + n<T>(1,4)*z[76] - z[80];
    z[76]=z[8]*z[76];
    z[68]=z[76] + n<T>(1,2)*z[74] + z[68];
    z[68]=z[8]*z[68];
    z[74]=z[69]*z[4];
    z[74]=z[74] + z[27];
    z[76]=z[10]*z[69];
    z[76]=z[76] + z[74];
    z[76]=z[13]*z[76];
    z[79]=z[69] - z[4];
    z[80]= - z[10] + z[79];
    z[80]=z[26]*z[80];
    z[83]=z[7] + z[10];
    z[83]=z[38]*z[83];
    z[76]=z[76] + z[80] + z[83] + z[64];
    z[72]= - z[89] - z[72];
    z[72]=z[6]*z[72];
    z[80]= - n<T>(1,4)*z[3] - n<T>(7,2)*z[69] + z[6];
    z[80]=z[3]*z[80];
    z[72]=z[72] + z[80];
    z[77]= - n<T>(5,36)*z[7] + n<T>(7,12)*z[3] + n<T>(3,8)*z[69] + z[77];
    z[77]=z[7]*z[77];
    z[72]=n<T>(1,3)*z[72] + z[77];
    z[72]=z[7]*z[72];
    z[77]=z[9] + z[12];
    z[77]= - z[81] + 3*z[77];
    z[77]=z[21]*z[77];
    z[77]= - z[52] + z[51] + z[50] - z[42] - z[77] - z[66] + z[60] + 
    z[58];
    z[80]=z[69]*z[6];
    z[80]=z[80] + z[27];
    z[83]= - z[9]*z[69];
    z[83]=z[83] - z[80];
    z[83]=z[15]*z[83];
    z[85]= - z[9] + z[81];
    z[85]=z[23]*z[85];
    z[83]= - z[55] + z[83] + z[85];
    z[84]=z[9] + z[84] - z[10];
    z[85]= - z[8] + z[84];
    z[85]=z[69]*z[85];
    z[85]= - z[27] + z[85];
    z[85]=z[11]*z[85];
    z[84]=z[84] + z[69] - z[8];
    z[84]=z[19]*z[84];
    z[86]= - z[8]*z[69];
    z[74]=z[86] - z[74];
    z[74]=z[14]*z[74];
    z[69]=z[7]*z[69];
    z[69]=z[69] + z[80];
    z[69]=z[16]*z[69];
    z[80]=z[81]*npow(z[6],2);
    z[80]= - z[40] + z[80] - z[57];
    z[86]= - z[8] - z[9];
    z[86]=z[36]*z[86];
    z[86]=z[86] + z[48];
    z[87]=n<T>(10,3)*z[33] + n<T>(2,3)*z[31] - n<T>(19,144)*z[28];
    z[87]=i*z[87];
    z[88]=z[5] + z[3];
    z[88]= - n<T>(7,18)*z[10] + n<T>(5,12)*z[9] + n<T>(4,9)*z[2] + n<T>(19,72)*z[12]
     - n<T>(7,24)*z[4]
     + n<T>(1,12)*z[8]
     + n<T>(29,36)*z[7]
     + n<T>(3,16)*z[6] - n<T>(1,9)*z[88];
    z[88]=z[27]*z[88];
    z[89]=z[4] - z[94];
    z[89]=z[17]*z[89];
    z[79]=z[8] - z[79];
    z[79]=z[20]*z[79];
    z[81]= - z[7] + z[81];
    z[81]=z[25]*z[81];

    r += n<T>(5,4)*z[29] - n<T>(4,9)*z[30] - n<T>(5,3)*z[34] + z[39] - n<T>(2,3)*z[43]
       - n<T>(3,4)*z[45] - n<T>(8,3)*z[46] - n<T>(7,16)*z[56] - n<T>(1,48)*z[59]
     - n<T>(3,16)*z[61]
     + n<T>(5,24)*z[63]
     + n<T>(1,24)*z[65]
     + n<T>(1,8)*z[67]
     + z[68]
     +  z[69] + z[70] + z[71] + z[72] + z[73] + z[74] + z[75] + n<T>(1,6)*
      z[76] - n<T>(1,4)*z[77] + z[78] + z[79] + n<T>(1,2)*z[80] + z[81] + z[82]
       + n<T>(5,12)*z[83] + z[84] + z[85] + n<T>(1,12)*z[86] + z[87] + z[88] + 
      z[89] - n<T>(1,3)*z[92];
 
    return r;
}

template std::complex<double> qg_2lha_tf654(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf654(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
