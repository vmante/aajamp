#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf81(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[6];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=f[69];
    z[4]=f[77];
    z[5]=i*z[1];

    r += 6*z[2] + z[3] - 2*z[4] - n<T>(2,9)*z[5];
 
    return r;
}

template std::complex<double> qg_2lha_tf81(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf81(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
