#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf376(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[97];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[15];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[9];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[5];
    z[21]=e[7];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=e[12];
    z[25]=c[3];
    z[26]=c[11];
    z[27]=c[15];
    z[28]=c[18];
    z[29]=c[19];
    z[30]=c[21];
    z[31]=c[23];
    z[32]=c[25];
    z[33]=c[26];
    z[34]=c[28];
    z[35]=c[29];
    z[36]=c[31];
    z[37]=e[3];
    z[38]=e[10];
    z[39]=e[6];
    z[40]=e[14];
    z[41]=f[1];
    z[42]=f[2];
    z[43]=f[3];
    z[44]=f[4];
    z[45]=f[5];
    z[46]=f[10];
    z[47]=f[11];
    z[48]=f[12];
    z[49]=f[14];
    z[50]=f[16];
    z[51]=f[18];
    z[52]=f[20];
    z[53]=f[21];
    z[54]=f[30];
    z[55]=f[31];
    z[56]=f[32];
    z[57]=f[33];
    z[58]=f[36];
    z[59]=f[39];
    z[60]=f[43];
    z[61]=f[50];
    z[62]=f[51];
    z[63]=f[52];
    z[64]=f[55];
    z[65]=f[58];
    z[66]=3*z[2];
    z[67]=n<T>(11,6)*z[9];
    z[68]=z[1]*i;
    z[69]=n<T>(7,6)*z[68];
    z[70]=3*z[5];
    z[71]=n<T>(7,12)*z[7] - z[67] - z[66] + n<T>(2,3)*z[6] - z[69] + z[70];
    z[71]=z[7]*z[71];
    z[72]=n<T>(1,3)*z[5];
    z[73]=z[68] + n<T>(1,4)*z[5];
    z[73]=z[73]*z[72];
    z[74]= - n<T>(41,24)*z[6] + n<T>(89,12)*z[68] - z[70];
    z[74]=z[6]*z[74];
    z[75]=n<T>(1,2)*z[5];
    z[76]= - z[68] - z[75];
    z[76]=n<T>(1,12)*z[2] + n<T>(1,3)*z[76] + 3*z[6];
    z[76]=z[2]*z[76];
    z[77]=n<T>(11,4)*z[9];
    z[78]=n<T>(1,3)*z[68];
    z[79]=z[78] + z[6];
    z[80]= - n<T>(1,6)*z[9] + z[79];
    z[80]=z[80]*z[77];
    z[81]=z[2] - z[5];
    z[77]=25*z[7] - z[77] - n<T>(89,4)*z[6] + z[81];
    z[77]=z[8]*z[77];
    z[71]=n<T>(1,6)*z[77] + z[71] + z[80] + z[76] + z[73] + z[74];
    z[71]=z[8]*z[71];
    z[73]=z[75] - z[68];
    z[74]= - z[73]*z[75];
    z[76]=n<T>(1,2)*z[6];
    z[77]=z[68] - z[76];
    z[77]=z[6]*z[77];
    z[80]=z[68] - z[5];
    z[82]=n<T>(1,2)*z[2];
    z[83]= - z[82] - z[80];
    z[83]=z[83]*z[82];
    z[84]=n<T>(1,2)*z[9];
    z[85]= - z[68] + z[84];
    z[85]=z[85]*z[84];
    z[86]= - n<T>(13,3)*z[68] - z[5];
    z[86]=n<T>(5,6)*z[4] + z[82] + n<T>(1,2)*z[86] - z[6];
    z[87]=n<T>(1,2)*z[4];
    z[86]=z[86]*z[87];
    z[74]=z[86] + z[85] + z[83] + z[74] + z[77];
    z[74]=z[4]*z[74];
    z[77]=n<T>(1,4)*z[2];
    z[83]= - n<T>(1,4)*z[68] - z[5];
    z[83]=n<T>(5,3)*z[3] - z[84] + n<T>(1,3)*z[83] - z[77];
    z[83]=z[3]*z[83];
    z[85]=2*z[5];
    z[86]=n<T>(1,2)*z[68];
    z[88]=z[86] - z[85];
    z[88]=z[88]*z[72];
    z[81]=z[68] - z[81];
    z[81]=z[81]*z[82];
    z[89]= - z[68] + z[87];
    z[89]=z[89]*z[87];
    z[90]=z[6] + 2*z[80];
    z[90]=z[90]*z[6];
    z[91]= - n<T>(5,4)*z[9] - z[80];
    z[91]=z[9]*z[91];
    z[81]=z[83] + z[89] + z[91] + z[81] + z[88] + z[90];
    z[81]=z[3]*z[81];
    z[83]=z[76] + z[68];
    z[83]=z[83]*z[6];
    z[88]=z[5]*z[73];
    z[89]=z[84] + z[80];
    z[91]=n<T>(7,4)*z[9];
    z[89]=z[89]*z[91];
    z[92]= - z[68] + n<T>(7,3)*z[5];
    z[91]= - n<T>(7,3)*z[3] - n<T>(1,3)*z[4] + z[91] + n<T>(1,4)*z[92] - z[6];
    z[92]=n<T>(1,3)*z[10];
    z[91]=n<T>(1,2)*z[91] + z[92];
    z[91]=z[10]*z[91];
    z[79]= - n<T>(1,6)*z[4] + z[79];
    z[79]=z[4]*z[79];
    z[93]= - n<T>(1,2)*z[3] - z[80];
    z[93]=z[3]*z[93];
    z[79]=z[91] + n<T>(7,6)*z[93] + z[79] + z[89] + n<T>(7,12)*z[88] - z[83];
    z[79]=z[10]*z[79];
    z[88]=z[5] + z[3];
    z[88]=z[68]*z[88];
    z[88]=z[25] + z[88];
    z[88]=z[11]*z[88];
    z[89]=z[68]*z[4];
    z[89]=z[89] + z[25];
    z[91]= - z[14]*z[68];
    z[91]=z[91] - z[89];
    z[91]=z[12]*z[91];
    z[93]=z[3] - z[80];
    z[93]=z[18]*z[93];
    z[94]=z[68] - z[4];
    z[95]=z[14] - z[94];
    z[95]=z[24]*z[95];
    z[96]=z[10] + z[14];
    z[96]=z[40]*z[96];
    z[88]=z[88] + z[91] + z[93] + z[95] + z[96] - z[61];
    z[91]=2*z[6];
    z[93]=2*z[68];
    z[95]=2*z[2] + z[91] + z[93] - z[70];
    z[95]=z[2]*z[95];
    z[84]=z[68] + z[84];
    z[67]=z[84]*z[67];
    z[66]=n<T>(19,6)*z[7] + z[66] - n<T>(23,4)*z[6] + n<T>(31,12)*z[68] + z[5];
    z[66]=z[7]*z[66];
    z[84]=z[93] - z[5];
    z[93]=z[84]*z[5];
    z[96]= - z[78] - z[5];
    z[96]=2*z[96] - n<T>(61,12)*z[6];
    z[96]=z[6]*z[96];
    z[66]=z[66] + z[67] + z[95] - z[93] + z[96];
    z[66]=z[7]*z[66];
    z[67]=2*z[94] + z[10];
    z[67]=z[67]*z[92];
    z[92]=npow(z[4],2);
    z[80]=z[6] + z[80];
    z[80]=z[14]*z[80];
    z[67]=z[80] + z[67] + n<T>(1,3)*z[92] - z[93] + z[90];
    z[67]=z[14]*z[67];
    z[80]=n<T>(7,4)*z[68] + z[5];
    z[77]= - z[77] + n<T>(1,3)*z[80] - z[91];
    z[77]=z[2]*z[77];
    z[75]= - 4*z[68] + z[75];
    z[72]=z[75]*z[72];
    z[75]= - n<T>(9,2)*z[6] - z[68] + z[85];
    z[75]=z[6]*z[75];
    z[72]=z[77] + z[72] + z[75];
    z[72]=z[2]*z[72];
    z[73]=z[73]*z[70];
    z[75]=3*z[9] - z[2] - n<T>(11,2)*z[6] + z[78] + n<T>(3,2)*z[5];
    z[75]=z[9]*z[75];
    z[73]=z[75] + z[73] - 11*z[83];
    z[73]=z[9]*z[73];
    z[73]= - z[32] - z[73] + z[49] + z[43] - z[41];
    z[75]=z[8] + z[7];
    z[77]= - z[68]*z[75];
    z[77]= - z[25] + z[77];
    z[77]=z[16]*z[77];
    z[75]=z[68] - z[75];
    z[75]=z[23]*z[75];
    z[75]=z[77] + z[75];
    z[77]=5*z[2];
    z[78]=n<T>(167,2)*z[5] + 163*z[6];
    z[78]=n<T>(11,8)*z[9] + n<T>(1,4)*z[78] - z[77];
    z[78]= - n<T>(7,3)*z[14] - n<T>(1,2)*z[10] - n<T>(7,4)*z[3] - n<T>(11,8)*z[8]
     - n<T>(7,2)
   *z[4] + n<T>(1,3)*z[78] - 2*z[7];
    z[78]=z[25]*z[78];
    z[80]= - n<T>(1,2)*z[8] + z[68] - z[82];
    z[80]=z[22]*z[80];
    z[78]=z[78] + z[80];
    z[70]= - z[84]*z[70];
    z[69]=1 + z[69];
    z[69]=n<T>(71,6)*z[6] + n<T>(11,2)*z[69] + 7*z[5];
    z[69]=z[69]*z[76];
    z[69]=z[70] + z[69];
    z[69]=z[6]*z[69];
    z[70]=z[68] - z[8];
    z[76]=5*z[5];
    z[70]=n<T>(65,6)*z[7] - z[77] + n<T>(95,6)*z[6] + z[76] - n<T>(9,2)*z[70];
    z[70]=z[21]*z[70];
    z[77]=z[9]*z[68];
    z[77]=z[77] + z[89];
    z[77]=z[13]*z[77];
    z[80]= - z[9] + z[94];
    z[80]=z[19]*z[80];
    z[77]= - z[53] - z[77] - z[80] + z[65] + z[62];
    z[80]=z[33] - z[31];
    z[82]=n<T>(1,3)*i;
    z[80]=z[82]*z[80];
    z[82]=z[2] - z[86] + z[5];
    z[82]= - z[87] + z[7] - z[6] + n<T>(1,3)*z[82];
    z[82]=z[17]*z[82];
    z[76]=z[6] + z[76] + n<T>(11,2);
    z[83]=z[68]*z[76];
    z[83]=5*z[25] + z[83];
    z[83]=z[15]*z[83];
    z[76]=5*z[68] - z[76];
    z[76]=z[20]*z[76];
    z[84]= - z[64] + z[52] + z[51] + z[47] + z[34] + z[28];
    z[85]=z[9] + z[7];
    z[85]=z[39]*z[85];
    z[85]=z[85] - z[54];
    z[86]=z[3] + z[10];
    z[86]=z[38]*z[86];
    z[86]=z[86] - z[46];
    z[87]= - n<T>(1,2)*z[30] - n<T>(137,72)*z[26];
    z[87]=i*z[87];
    z[68]= - n<T>(8,3)*z[5] - n<T>(11,4) + 3*z[68];
    z[68]=z[68]*npow(z[5],2);
    z[89]=z[9] + z[3];
    z[89]=z[37]*z[89];

    r += n<T>(29,8)*z[27] - n<T>(7,36)*z[29] + n<T>(1,8)*z[35] + n<T>(199,48)*z[36]
     +  z[42] + n<T>(7,24)*z[44] - n<T>(5,12)*z[45] - n<T>(3,8)*z[48] + n<T>(7,8)*z[50]
       - n<T>(161,24)*z[55] - 4*z[56] - 19*z[57] + n<T>(11,24)*z[58] - n<T>(11,8)*
      z[59] + 3*z[60] + z[63] + z[66] + z[67] + z[68] + z[69] + z[70]
       + z[71] + z[72] - n<T>(1,4)*z[73] + z[74] + n<T>(43,6)*z[75] + z[76] - n<T>(1,2)*z[77]
     + n<T>(1,3)*z[78]
     + z[79]
     + z[80]
     + z[81]
     + z[82]
     + z[83]
       - n<T>(1,6)*z[84] + n<T>(11,6)*z[85] + n<T>(7,6)*z[86] + z[87] + n<T>(2,3)*z[88]
       + n<T>(3,2)*z[89];
 
    return r;
}

template std::complex<double> qg_2lha_tf376(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf376(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
