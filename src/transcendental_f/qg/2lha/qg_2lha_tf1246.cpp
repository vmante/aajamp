#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1246(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[2];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=c[3];
    z[9]=c[15];
    z[10]=c[18];
    z[11]=c[23];
    z[12]=c[26];
    z[13]=c[28];
    z[14]=c[29];
    z[15]=c[31];
    z[16]=f[0];
    z[17]=f[3];
    z[18]=f[11];
    z[19]=f[17];
    z[20]=f[20];
    z[21]=npow(z[4],2);
    z[22]=z[3]*z[4];
    z[23]=z[21] + z[22];
    z[24]=n<T>(1,2)*z[3];
    z[23]=z[23]*z[24];
    z[25]=z[3] - z[4];
    z[25]=z[25]*z[5];
    z[21]=z[25] - z[21];
    z[24]=z[4] - z[24];
    z[24]=z[3]*z[24];
    z[21]=z[24] + n<T>(1,2)*z[21];
    z[21]=z[5]*z[21];
    z[24]= - z[22] - z[25];
    z[25]=z[1]*i;
    z[24]=z[24]*z[25];
    z[21]=z[24] + z[23] + z[21];
    z[23]=z[3]*z[25];
    z[24]=z[25] - z[4];
    z[26]=z[2]*z[24];
    z[22]= - n<T>(1,2)*z[26] - z[22] + z[23];
    z[22]=z[2]*z[22];
    z[23]= - z[19] + z[18] + z[17] - z[14];
    z[26]=z[20] + z[10];
    z[27]= - z[5] + z[4] + z[3];
    z[27]=z[8]*z[27];
    z[27]=z[27] + z[9];
    z[28]= - z[12] + n<T>(1,2)*z[11];
    z[28]=i*z[28];
    z[25]= - z[2] - z[4] + 2*z[25];
    z[25]=z[6]*z[25];
    z[24]= - z[7]*z[24];

    r += z[13] + n<T>(29,24)*z[15] - z[16] + n<T>(1,2)*z[21] + z[22] + n<T>(1,4)*
      z[23] + z[24] + z[25] + n<T>(1,3)*z[26] + n<T>(1,12)*z[27] + z[28];
 
    return r;
}

template std::complex<double> qg_2lha_tf1246(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1246(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
