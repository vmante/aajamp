#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1436(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[65];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[16];
    z[10]=d[15];
    z[11]=d[18];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[4];
    z[16]=e[8];
    z[17]=e[10];
    z[18]=e[11];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[17];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[30];
    z[29]=c[31];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[6];
    z[36]=f[7];
    z[37]=f[8];
    z[38]=f[9];
    z[39]=f[11];
    z[40]=f[13];
    z[41]=f[14];
    z[42]=f[16];
    z[43]=f[17];
    z[44]=f[18];
    z[45]=f[23];
    z[46]=f[76];
    z[47]=z[3] + z[2];
    z[48]=z[47] - z[5];
    z[49]=n<T>(1,2)*z[4];
    z[50]=z[1]*i;
    z[51]=7*z[50];
    z[52]= - z[49] - z[51] + z[48];
    z[52]=z[4]*z[52];
    z[53]=n<T>(1,2)*z[2];
    z[54]=z[53] + z[50];
    z[54]=z[54]*z[2];
    z[55]=n<T>(1,2)*z[3];
    z[56]=z[55] + z[2];
    z[57]=z[56] + z[50];
    z[58]= - z[3]*z[57];
    z[59]=n<T>(1,2)*z[8];
    z[60]=z[50] - z[5];
    z[61]=z[59] + z[60];
    z[61]=z[8]*z[61];
    z[47]=z[5]*z[47];
    z[47]=z[52] + z[61] + z[47] - z[54] + z[58];
    z[52]=z[4] - z[2];
    z[52]=z[8] - z[3] + 7*z[52];
    z[52]=n<T>(1,4)*z[52] - z[6];
    z[52]=z[6]*z[52];
    z[47]=n<T>(1,2)*z[47] + z[52];
    z[47]=z[6]*z[47];
    z[52]=z[3] + z[5];
    z[52]=z[50]*z[52];
    z[52]=z[19] + z[52];
    z[52]=z[10]*z[52];
    z[58]=z[8] + z[7];
    z[61]= - z[50]*z[58];
    z[61]= - z[19] + z[61];
    z[61]=z[11]*z[61];
    z[58]= - z[50] + z[58];
    z[58]=z[18]*z[58];
    z[62]=z[3] - z[60];
    z[62]=z[14]*z[62];
    z[47]= - z[32] + z[58] - z[44] - z[40] + z[33] + z[47] + z[52] + 
    z[61] + z[62];
    z[52]=n<T>(1,2)*z[7];
    z[51]= - z[52] + z[51] + z[48];
    z[58]=n<T>(1,8)*z[7];
    z[51]=z[51]*z[58];
    z[61]=z[7] - z[2];
    z[62]=n<T>(1,3)*z[8];
    z[60]=z[62] - n<T>(11,8)*z[3] - z[60] - n<T>(7,8)*z[61];
    z[59]=z[60]*z[59];
    z[60]=5*z[50];
    z[61]=n<T>(5,2)*z[3] + z[60] - z[2];
    z[61]=z[3]*z[61];
    z[54]= - z[54] + z[61];
    z[61]=n<T>(1,2)*z[5];
    z[63]=z[61] - z[50];
    z[64]= - n<T>(5,8)*z[3] + n<T>(1,8)*z[2] + z[63];
    z[64]=z[5]*z[64];
    z[51]=z[59] + z[51] + n<T>(1,8)*z[54] + z[64];
    z[51]=z[51]*z[62];
    z[54]=n<T>(3,2)*z[2];
    z[59]= - 11*z[50] + z[54];
    z[59]=z[2]*z[59];
    z[62]= - z[50] + z[56];
    z[62]=z[3]*z[62];
    z[54]= - z[50] + z[54];
    z[54]= - z[4] - n<T>(7,2)*z[7] - z[5] + 3*z[54] - z[55];
    z[54]=z[4]*z[54];
    z[54]=z[54] + z[59] + z[62];
    z[59]=n<T>(1,4)*z[3];
    z[62]= - z[59] - n<T>(3,4)*z[2] - z[63];
    z[62]=z[5]*z[62];
    z[52]= - z[52] + z[50] + z[5];
    z[52]=z[7]*z[52];
    z[52]=n<T>(7,4)*z[52] + z[62] + n<T>(1,4)*z[54];
    z[49]=z[52]*z[49];
    z[52]=z[57]*z[55];
    z[54]= - n<T>(11,2)*z[50] + z[2];
    z[54]=z[2]*z[54];
    z[57]=n<T>(7,2)*z[2] - z[3];
    z[57]=z[5]*z[57];
    z[52]=z[57] + z[54] + z[52];
    z[52]=z[52]*z[61];
    z[54]=z[50] + z[2];
    z[57]=n<T>(11,12)*z[3] - z[54];
    z[55]=z[57]*z[55];
    z[57]= - z[50] + n<T>(1,4)*z[2];
    z[57]=z[2]*z[57];
    z[55]=z[57] + z[55];
    z[55]=z[3]*z[55];
    z[57]=z[60] - n<T>(17,8)*z[2];
    z[57]=z[57]*npow(z[2],2);
    z[52]= - z[34] - z[39] + z[52] + z[57] + z[55];
    z[55]=n<T>(1,3)*z[5];
    z[57]= - n<T>(5,6)*z[3] - z[50] + n<T>(37,6)*z[2];
    z[57]=z[58] + n<T>(1,8)*z[57] - z[55];
    z[57]=z[7]*z[57];
    z[56]=z[60] - z[56];
    z[56]=z[3]*z[56];
    z[58]= - 37*z[50] + n<T>(5,2)*z[2];
    z[58]=z[2]*z[58];
    z[56]=z[58] + z[56];
    z[58]=2*z[50];
    z[60]= - z[5] + n<T>(1,8)*z[3] + z[58] - n<T>(5,8)*z[2];
    z[60]=z[5]*z[60];
    z[56]=n<T>(1,8)*z[56] + z[60];
    z[56]=n<T>(1,3)*z[56] + z[57];
    z[56]=z[7]*z[56];
    z[57]=z[8] + z[5];
    z[60]=8*z[50] - n<T>(13,4)*z[2];
    z[57]= - n<T>(4,3)*z[7] + n<T>(1,3)*z[60] + z[59] - n<T>(1,4)*z[57];
    z[57]=z[16]*z[57];
    z[60]=z[7] + z[5];
    z[59]= - n<T>(17,8)*z[8] - z[2] + z[59] + n<T>(5,8)*z[60];
    z[59]=n<T>(1,3)*z[59] - n<T>(11,8)*z[4];
    z[59]=n<T>(1,3)*z[59] + n<T>(1,16)*z[6];
    z[59]=z[19]*z[59];
    z[48]=z[48] + 3*z[4];
    z[60]=n<T>(1,4)*z[48] + z[6];
    z[60]=z[50]*z[60];
    z[60]=n<T>(3,4)*z[19] + z[60];
    z[60]=z[9]*z[60];
    z[48]=3*z[50] - z[48];
    z[48]=n<T>(1,4)*z[48] - z[6];
    z[48]=z[15]*z[48];
    z[50]=z[54] - z[7];
    z[50]=z[55] + z[3] - n<T>(1,3)*z[50];
    z[50]=n<T>(1,2)*z[50] + n<T>(2,3)*z[8];
    z[50]=z[17]*z[50];
    z[53]= - z[53] - z[63];
    z[53]=z[13]*z[53];
    z[54]=z[29] + z[43] - z[31];
    z[55]=z[38] + z[42] + z[41];
    z[61]=z[30] - z[25];
    z[62]= - n<T>(35,6)*z[26] - n<T>(5,3)*z[24] - n<T>(1,4)*z[22] - n<T>(1,48)*z[20];
    z[62]=i*z[62];
    z[58]= - z[4] + z[58] - z[2];
    z[58]=z[12]*z[58];

    r +=  - n<T>(7,8)*z[21] + n<T>(5,18)*z[23] + n<T>(35,12)*z[27] + n<T>(1,2)*z[28]
     +  n<T>(3,16)*z[35] + n<T>(2,3)*z[36] - z[37] - n<T>(1,12)*z[45] + z[46] + n<T>(1,4)
      *z[47] + z[48] + z[49] + z[50] + z[51] + n<T>(1,3)*z[52] + n<T>(7,6)*
      z[53] - n<T>(7,16)*z[54] + n<T>(1,16)*z[55] + z[56] + z[57] + z[58] + 
      z[59] + z[60] - n<T>(3,4)*z[61] + z[62];
 
    return r;
}

template std::complex<double> qg_2lha_tf1436(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1436(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
