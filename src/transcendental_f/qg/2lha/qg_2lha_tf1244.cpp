#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1244(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[37];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[3];
    z[3]=d[6];
    z[4]=d[12];
    z[5]=d[4];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[17];
    z[9]=e[5];
    z[10]=e[7];
    z[11]=e[9];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[17];
    z[16]=c[31];
    z[17]=e[6];
    z[18]=f[30];
    z[19]=f[31];
    z[20]=f[32];
    z[21]=f[33];
    z[22]=f[36];
    z[23]=f[39];
    z[24]=f[68];
    z[25]=n<T>(3,2)*z[6];
    z[26]=z[25] - z[3];
    z[27]=z[1]*i;
    z[28]=n<T>(1,2)*z[27];
    z[29]=n<T>(1,4)*z[5];
    z[30]= - z[29] + z[28] + z[26];
    z[30]=z[5]*z[30];
    z[31]=n<T>(1,2)*z[6];
    z[32]=z[27] - z[31];
    z[32]=z[32]*z[25];
    z[33]=z[27]*z[3];
    z[34]=npow(z[3],2);
    z[29]= - z[29] + z[3] - n<T>(3,4)*z[6];
    z[29]=z[7]*z[29];
    z[29]=z[29] + z[30] + z[32] + n<T>(1,2)*z[34] - z[33];
    z[29]=z[7]*z[29];
    z[30]=z[7] + z[3];
    z[32]= - z[27]*z[30];
    z[32]= - z[12] + z[32];
    z[32]=z[8]*z[32];
    z[30]=z[27] - z[30];
    z[30]=z[11]*z[30];
    z[35]=z[5] + z[3];
    z[35]=z[17]*z[35];
    z[29]= - z[29] - z[32] - z[30] - z[35] + z[20] + z[18];
    z[30]=n<T>(1,4)*z[34];
    z[32]=z[28] + z[3];
    z[34]=3*z[6] - z[32];
    z[34]=z[6]*z[34];
    z[34]=n<T>(1,4)*z[34] - z[30] + z[33];
    z[34]=z[6]*z[34];
    z[35]= - z[27] - z[31];
    z[25]=z[35]*z[25];
    z[26]=z[5] + z[28] - z[26];
    z[28]=n<T>(1,2)*z[5];
    z[26]=z[26]*z[28];
    z[25]=z[26] + z[33] + z[25];
    z[25]=z[25]*z[28];
    z[26]=z[31] - z[3];
    z[28]=z[6]*z[26];
    z[31]= - n<T>(1,2)*z[2] + z[32];
    z[31]=z[2]*z[31];
    z[28]=z[31] - 2*z[33] + z[28];
    z[28]=z[2]*z[28];
    z[30]=z[27]*z[30];
    z[31]=z[23] + z[19];
    z[26]=n<T>(1,3)*z[2] - n<T>(1,24)*z[7] + n<T>(1,6)*z[5] + z[26];
    z[26]=z[12]*z[26];
    z[32]=z[27] - z[2];
    z[33]=z[6] + n<T>(1,2)*z[3] - z[32];
    z[33]=z[10]*z[33];
    z[27]=z[2]*z[27];
    z[27]=z[27] + z[12];
    z[27]=z[4]*z[27];
    z[32]=z[9]*z[32];
    z[35]=z[13]*i;
    z[36]=n<T>(1,2)*i;
    z[36]=z[15]*z[36];

    r += n<T>(1,4)*z[14] - n<T>(7,8)*z[16] - z[21] + n<T>(1,8)*z[22] + n<T>(1,3)*z[24]
       + z[25] + z[26] + z[27] + z[28] - n<T>(1,2)*z[29] + z[30] - n<T>(3,8)*
      z[31] + z[32] + z[33] + z[34] - n<T>(5,24)*z[35] + z[36];
 
    return r;
}

template std::complex<double> qg_2lha_tf1244(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1244(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
