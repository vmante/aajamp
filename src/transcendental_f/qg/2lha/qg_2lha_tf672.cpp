#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf672(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[118];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[18];
    z[34]=c[19];
    z[35]=c[21];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[29];
    z[41]=c[31];
    z[42]=e[3];
    z[43]=e[6];
    z[44]=e[14];
    z[45]=f[0];
    z[46]=f[1];
    z[47]=f[2];
    z[48]=f[3];
    z[49]=f[4];
    z[50]=f[5];
    z[51]=f[6];
    z[52]=f[7];
    z[53]=f[9];
    z[54]=f[10];
    z[55]=f[11];
    z[56]=f[12];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[18];
    z[60]=f[20];
    z[61]=f[23];
    z[62]=f[30];
    z[63]=f[31];
    z[64]=f[32];
    z[65]=f[33];
    z[66]=f[34];
    z[67]=f[35];
    z[68]=f[36];
    z[69]=f[37];
    z[70]=f[38];
    z[71]=f[39];
    z[72]=f[41];
    z[73]=f[43];
    z[74]=f[48];
    z[75]=f[50];
    z[76]=f[51];
    z[77]=f[52];
    z[78]=f[53];
    z[79]=f[54];
    z[80]=f[55];
    z[81]=f[56];
    z[82]=f[57];
    z[83]=f[58];
    z[84]=f[60];
    z[85]=f[66];
    z[86]=f[67];
    z[87]=n<T>(1,4)*z[7];
    z[88]=n<T>(1,4)*z[2];
    z[89]=z[1]*i;
    z[90]=z[88] - z[87] + z[89] - n<T>(5,4)*z[4];
    z[90]=z[6]*z[90];
    z[91]=n<T>(3,8)*z[5];
    z[92]=n<T>(1,2)*z[89];
    z[93]= - 1 - z[92];
    z[93]=n<T>(187,24)*z[4] + 5*z[93] + z[91];
    z[93]=z[4]*z[93];
    z[94]= - 1 + n<T>(5,2)*z[89];
    z[94]=n<T>(47,4)*z[7] + 13*z[94] - 15*z[4];
    z[94]=z[94]*z[87];
    z[95]=n<T>(3,2)*z[5];
    z[96]=n<T>(9,2)*z[4] - z[89] - z[95];
    z[97]=3*z[9];
    z[96]= - n<T>(35,4)*z[7] + n<T>(1,2)*z[96] + z[97];
    z[96]=n<T>(1,2)*z[96] - z[2];
    z[96]=z[2]*z[96];
    z[98]=z[89] - z[4];
    z[99]=n<T>(1,2)*z[6];
    z[100]=z[99] - n<T>(1,2)*z[98] - z[7];
    z[101]=n<T>(1,2)*z[8];
    z[100]=z[100]*z[101];
    z[102]=5*z[9];
    z[103]=n<T>(3,2)*z[89];
    z[104]= - z[103] + z[4];
    z[104]= - n<T>(11,4)*z[2] - 15*z[7] + n<T>(9,2)*z[104] + z[102];
    z[104]=n<T>(65,8)*z[11] + n<T>(1,2)*z[104] + z[6];
    z[105]=n<T>(1,2)*z[11];
    z[104]=z[104]*z[105];
    z[106]= - z[89] + n<T>(1,2)*z[5];
    z[107]=z[106]*z[5];
    z[108]=5*z[89];
    z[109]=n<T>(3,4)*z[9] - z[103] - z[4];
    z[109]=z[9]*z[109];
    z[90]=z[104] + z[100] + z[90] + z[96] + z[94] + n<T>(3,2)*z[109] + z[93]
    + z[108] + z[107];
    z[90]=z[90]*z[105];
    z[93]=n<T>(1,6)*z[9];
    z[94]=n<T>(3,2)*z[4];
    z[96]=23*z[89];
    z[100]= - z[96] + 5*z[5];
    z[100]= - n<T>(19,12)*z[7] + z[93] + n<T>(1,3)*z[100] + z[94];
    z[100]=z[7]*z[100];
    z[104]= - n<T>(119,2)*z[4] + 119*z[89] - n<T>(13,2)*z[5];
    z[104]=z[4]*z[104];
    z[104]= - n<T>(175,2)*z[107] + z[104];
    z[109]= - n<T>(9,2)*z[9] + 9*z[89] + n<T>(7,2)*z[5];
    z[109]=z[109]*z[97];
    z[110]=n<T>(1,2)*z[2];
    z[111]=n<T>(13,2)*z[4] - n<T>(617,2)*z[89] - 25*z[5];
    z[111]=n<T>(239,6)*z[2] + n<T>(13,3)*z[7] + n<T>(1,3)*z[111] - n<T>(21,2)*z[9];
    z[111]=z[111]*z[110];
    z[100]=z[111] + z[100] + n<T>(1,3)*z[104] + z[109];
    z[100]=z[100]*z[88];
    z[104]= - 7*z[89] + z[94];
    z[104]=z[4]*z[104];
    z[104]= - n<T>(19,6)*z[107] + z[104];
    z[109]=n<T>(37,8)*z[9] - n<T>(37,4)*z[89] - z[4];
    z[109]=z[9]*z[109];
    z[104]=n<T>(1,2)*z[104] + z[109];
    z[109]=n<T>(1,4)*z[4];
    z[111]=n<T>(77,16)*z[7] - n<T>(77,8)*z[9] + z[109] + n<T>(83,8)*z[89] - z[5];
    z[111]=z[7]*z[111];
    z[112]= - n<T>(3,4)*z[2] + z[9] - z[92] + z[4];
    z[112]=z[112]*z[110];
    z[113]=n<T>(19,4)*z[5];
    z[96]= - z[96] - z[113];
    z[114]=z[2] - z[4];
    z[96]= - n<T>(89,6)*z[6] + n<T>(81,4)*z[7] + n<T>(37,4)*z[9] + n<T>(1,3)*z[96]
     - n<T>(7,2)*z[114];
    z[114]=n<T>(1,4)*z[6];
    z[96]=z[96]*z[114];
    z[96]=z[96] + z[112] + n<T>(1,2)*z[104] + z[111];
    z[96]=z[6]*z[96];
    z[104]=1 + n<T>(13,4)*z[89];
    z[91]= - n<T>(65,8)*z[4] + 5*z[104] - z[91];
    z[91]=z[4]*z[91];
    z[104]=n<T>(1607,36)*z[7] + n<T>(77,4)*z[9] - n<T>(81,2)*z[4] + n<T>(151,12)*z[5]
     - 
   23 + n<T>(41,4)*z[89];
    z[104]=z[104]*z[87];
    z[111]= - n<T>(79,48)*z[5] - 5 + n<T>(43,24)*z[89];
    z[111]=z[5]*z[111];
    z[112]=n<T>(79,2)*z[9] - 79*z[89] - z[5];
    z[112]=z[9]*z[112];
    z[91]=z[104] + n<T>(1,24)*z[112] + z[111] + z[91];
    z[91]=z[7]*z[91];
    z[104]=n<T>(3,2)*z[7];
    z[95]= - z[104] - z[95] + 5 + z[103];
    z[95]=z[22]*z[95];
    z[111]=n<T>(49,8)*z[4] + n<T>(251,96)*z[5] - 5 - n<T>(151,24)*z[89];
    z[111]=z[4]*z[111];
    z[111]=n<T>(251,48)*z[107] + z[111];
    z[111]=z[4]*z[111];
    z[112]=n<T>(1,2)*z[4];
    z[115]=z[112] - z[89];
    z[116]= - z[5] + z[115];
    z[116]=z[4]*z[116];
    z[116]=11*z[107] + 25*z[116];
    z[117]= - n<T>(7,12)*z[9] + n<T>(75,32)*z[4] - z[89] + n<T>(33,32)*z[5];
    z[117]=z[9]*z[117];
    z[116]=n<T>(3,16)*z[116] + z[117];
    z[116]=z[9]*z[116];
    z[117]=z[7] + z[5];
    z[103]=z[103]*z[117];
    z[103]=n<T>(3,2)*z[30] - z[108] + z[103];
    z[103]=z[15]*z[103];
    z[117]=5 + 3*z[89];
    z[117]=n<T>(1,2)*z[117] - z[5];
    z[117]=z[117]*npow(z[5],2);
    z[91]= - z[103] - z[96] - z[100] - z[91] - z[116] - z[117] - z[111]
    - z[95] - z[74] + z[70] + z[33];
    z[95]= - 3*z[6] - z[110] + z[102] + z[109] - n<T>(23,4)*z[89] + z[5];
    z[95]=z[95]*z[99];
    z[96]= - n<T>(95,48)*z[9] + z[109] + 2*z[89] + n<T>(1,48)*z[5];
    z[96]=z[9]*z[96];
    z[100]= - n<T>(127,24)*z[7] - z[112] + 13 - n<T>(115,12)*z[5];
    z[87]=z[100]*z[87];
    z[100]=121*z[89] - z[5];
    z[100]= - n<T>(95,24)*z[2] + n<T>(121,12)*z[7] - n<T>(13,12)*z[9] + n<T>(1,12)*
    z[100] - z[4];
    z[100]=z[100]*z[88];
    z[103]= - 143*z[89] + 115*z[5];
    z[103]=n<T>(1,6)*z[103] - 5*z[4];
    z[103]=n<T>(55,72)*z[8] + n<T>(15,4)*z[6] - n<T>(5,12)*z[2] - n<T>(31,12)*z[7]
     + n<T>(1,4)*z[103] - z[102];
    z[109]=n<T>(1,4)*z[8];
    z[103]=z[103]*z[109];
    z[111]=n<T>(1,16)*z[4] + 5 - n<T>(1,8)*z[89];
    z[111]=z[4]*z[111];
    z[87]=z[103] + z[95] + z[100] + z[87] + z[96] + z[111] - z[108] + n<T>(91,48)*z[107];
    z[87]=z[8]*z[87];
    z[95]=149*z[4];
    z[96]= - z[115]*z[95];
    z[96]= - n<T>(133,2)*z[107] + z[96];
    z[100]= - z[108] - z[113];
    z[94]= - n<T>(13,24)*z[9] + n<T>(1,3)*z[100] + z[94];
    z[94]=z[9]*z[94];
    z[94]=n<T>(1,12)*z[96] + z[94];
    z[96]= - z[89] + 19*z[5];
    z[93]= - n<T>(1,12)*z[2] + z[93] + n<T>(1,6)*z[96] - 3*z[4];
    z[88]=z[93]*z[88];
    z[93]=z[9] - z[4];
    z[96]=z[89] - z[5];
    z[100]= - n<T>(19,16)*z[6] - n<T>(19,8)*z[96] - z[93];
    z[100]=z[100]*z[99];
    z[103]=z[89] - z[9];
    z[107]= - z[101] + z[6] - z[103];
    z[101]=z[107]*z[101];
    z[97]=3*z[2] - z[97] - n<T>(139,6)*z[4] + n<T>(133,6)*z[89] + z[5];
    z[97]= - 3*z[11] + n<T>(1,2)*z[97] - z[6];
    z[97]=z[97]*z[105];
    z[107]=431*z[89] - 133*z[5];
    z[95]=n<T>(1,2)*z[107] - z[95];
    z[95]=n<T>(1,4)*z[95] + z[102];
    z[95]=n<T>(527,36)*z[10] + n<T>(43,3)*z[11] - n<T>(19,8)*z[6] - n<T>(5,3)*z[2]
     + n<T>(1,3)*z[95] - n<T>(39,2)*z[7];
    z[95]=z[10]*z[95];
    z[102]= - 47*z[89] - z[5];
    z[102]= - 6*z[7] + n<T>(1,4)*z[102] + 12*z[4];
    z[102]=z[7]*z[102];
    z[88]=n<T>(1,4)*z[95] + z[97] + z[101] + z[100] + z[88] + n<T>(1,2)*z[94] + 
    z[102];
    z[88]=z[10]*z[88];
    z[94]=n<T>(1,2)*z[9];
    z[95]=z[94] - z[96];
    z[95]=z[95]*z[94];
    z[97]= - z[92] + z[5];
    z[97]=z[5]*z[97];
    z[95]=z[97] + z[95];
    z[97]= - z[104] - z[96];
    z[100]=3*z[7];
    z[97]=z[97]*z[100];
    z[101]=z[96] - z[9];
    z[102]=z[110] + z[101];
    z[102]=z[2]*z[102];
    z[95]=n<T>(19,6)*z[102] + n<T>(19,3)*z[95] + z[97];
    z[97]=z[96] + z[6];
    z[97]=z[7] + z[93] + n<T>(19,12)*z[97];
    z[97]=z[6]*z[97];
    z[95]=n<T>(1,2)*z[95] + z[97];
    z[97]=z[9] + z[5];
    z[102]=n<T>(1,2)*z[10];
    z[97]= - n<T>(11,12)*z[3] - z[102] + z[114] + z[2] + z[89] - n<T>(1,4)*z[97]
   ;
    z[97]=z[3]*z[97];
    z[100]=z[100] + z[96] - z[93];
    z[100]=z[109] + n<T>(1,2)*z[100] - z[6];
    z[100]=z[8]*z[100];
    z[93]=n<T>(1,4)*z[11] + z[6] - z[104] + n<T>(1,2)*z[96] - z[93];
    z[93]=z[93]*z[105];
    z[104]=n<T>(95,24)*z[10] + z[105] - z[8] - n<T>(19,12)*z[2] + n<T>(1,2)*z[7]
     + 
   n<T>(31,12)*z[9] - n<T>(19,6)*z[96] - z[4];
    z[102]=z[104]*z[102];
    z[93]=n<T>(19,12)*z[97] + z[102] + z[93] + n<T>(1,2)*z[95] + z[100];
    z[93]=z[3]*z[93];
    z[94]=z[110] - z[89] + z[94];
    z[94]=z[24]*z[94];
    z[95]=z[89]*z[9];
    z[95]=z[95] + z[30];
    z[97]=z[10]*z[89];
    z[97]=z[97] + z[95];
    z[97]=z[17]*z[97];
    z[100]= - z[10] + z[103];
    z[100]=z[27]*z[100];
    z[97]=z[97] + z[100];
    z[100]=z[8]*z[89];
    z[95]=z[100] + z[95];
    z[95]=z[16]*z[95];
    z[100]=z[8] - z[103];
    z[100]=z[25]*z[100];
    z[95]=z[95] + z[100];
    z[100]=z[11] + z[4];
    z[100]= - z[89]*z[100];
    z[100]= - z[30] + z[100];
    z[100]=z[13]*z[100];
    z[98]=z[11] - z[98];
    z[98]=z[28]*z[98];
    z[98]=z[100] + z[98];
    z[100]= - z[5] - z[3];
    z[100]=z[89]*z[100];
    z[100]= - z[30] + z[100];
    z[100]=z[12]*z[100];
    z[102]= - z[3] + z[96];
    z[102]=z[20]*z[102];
    z[100]=z[100] + z[102];
    z[102]=z[3] - z[8];
    z[96]=n<T>(163,16)*z[11] + n<T>(235,16)*z[7] + z[96] - n<T>(3,2)*z[102];
    z[96]=z[29]*z[96];
    z[99]=z[112] + z[99];
    z[102]=z[89]*z[99];
    z[102]=n<T>(1,2)*z[30] + z[108] + z[102];
    z[102]=z[14]*z[102];
    z[92]= - 5 + z[92] - z[99];
    z[92]=z[21]*z[92];
    z[99]=z[54] + z[47];
    z[103]=z[82] - z[53];
    z[104]= - z[6] - z[3];
    z[104]=z[42]*z[104];
    z[104]=z[104] + z[61];
    z[105]=n<T>(65,2)*z[9] - n<T>(245,2)*z[89] + 103*z[5];
    z[105]= - n<T>(59,2)*z[8] - n<T>(13,2)*z[2] + n<T>(1,2)*z[105] - 53*z[7];
    z[105]=n<T>(1,3)*z[105] - 9*z[11];
    z[105]=z[23]*z[105];
    z[105]=z[105] - z[72];
    z[107]=n<T>(493,24)*z[38] + n<T>(689,96)*z[36] + n<T>(3,16)*z[31];
    z[107]=i*z[107];
    z[108]=49*z[5] - 1259*z[4];
    z[108]=n<T>(1,2)*z[108] - 271*z[9];
    z[108]=n<T>(1,9)*z[108] + 151*z[7];
    z[108]= - n<T>(55,9)*z[8] - n<T>(247,9)*z[6] + n<T>(1,2)*z[108] - n<T>(19,9)*z[2];
    z[108]= - n<T>(19,9)*z[3] + n<T>(1801,36)*z[10] + n<T>(1,2)*z[108] - n<T>(179,9)*
    z[11];
    z[108]=z[30]*z[108];
    z[106]=z[7] + n<T>(89,3)*z[106] - 9*z[4];
    z[106]=n<T>(7,8)*z[11] - z[8] + n<T>(95,48)*z[2] + z[9] + n<T>(1,8)*z[106];
    z[106]=z[19]*z[106];
    z[89]= - z[8] + z[6] + n<T>(269,48)*z[2] + n<T>(317,48)*z[4] - n<T>(317,24)*
    z[89] + z[5];
    z[89]=z[18]*z[89];
    z[101]= - n<T>(5,2)*z[3] - n<T>(7,2)*z[10] + z[2] + z[101];
    z[101]=z[26]*z[101];
    z[109]= - z[6] - z[8];
    z[109]=z[43]*z[109];
    z[110]=z[11] + z[10];
    z[110]=z[44]*z[110];
    z[111]=n<T>(3,2)*i;
    z[111]= - z[35]*z[111];

    r += n<T>(101,24)*z[32] - n<T>(1,18)*z[34] - n<T>(239,96)*z[37] - n<T>(415,48)*
      z[39] + n<T>(3,8)*z[40] - n<T>(1355,144)*z[41] + n<T>(19,4)*z[45] - n<T>(5,16)*
      z[46] + n<T>(203,192)*z[48] - n<T>(209,96)*z[49] + n<T>(95,48)*z[50] - n<T>(19,16)
      *z[51] - n<T>(19,3)*z[52] + n<T>(65,64)*z[55] + n<T>(19,96)*z[56] - n<T>(19,32)*
      z[57] + n<T>(71,64)*z[58] + n<T>(1,6)*z[59] - n<T>(1,12)*z[60] + 2*z[62] + 
      n<T>(125,96)*z[63] + n<T>(9,32)*z[64] - n<T>(67,24)*z[65] + n<T>(3,32)*z[66]
     - n<T>(4,3)*z[67]
     - n<T>(21,32)*z[68]
     + n<T>(17,16)*z[69]
     + n<T>(89,32)*z[71] - n<T>(23,48)
      *z[73] - n<T>(251,48)*z[75] - n<T>(65,16)*z[76] - n<T>(9,8)*z[77] - 3*z[78]
       + n<T>(47,16)*z[79] + n<T>(119,48)*z[80] - n<T>(5,4)*z[81] - n<T>(21,4)*z[83]
     +  z[84] - n<T>(9,16)*z[85] - n<T>(1,16)*z[86] + z[87] + z[88] + z[89] + 
      z[90] - n<T>(1,2)*z[91] + z[92] + z[93] + n<T>(17,8)*z[94] + n<T>(47,24)*
      z[95] + z[96] + n<T>(11,8)*z[97] + n<T>(157,24)*z[98] + n<T>(5,24)*z[99] + n<T>(19,8)*z[100]
     + n<T>(19,12)*z[101]
     + z[102]
     + n<T>(7,16)*z[103]
     + n<T>(19,24)*
      z[104] + n<T>(1,4)*z[105] + z[106] + z[107] + n<T>(1,8)*z[108] + n<T>(35,8)*
      z[109] + n<T>(211,24)*z[110] + z[111];
 
    return r;
}

template std::complex<double> qg_2lha_tf672(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf672(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
