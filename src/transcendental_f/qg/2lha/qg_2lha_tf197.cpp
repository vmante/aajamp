#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf197(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[124];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[3];
    z[22]=e[4];
    z[23]=e[5];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[10];
    z[28]=e[11];
    z[29]=e[12];
    z[30]=e[13];
    z[31]=c[3];
    z[32]=c[11];
    z[33]=c[15];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[29];
    z[41]=c[31];
    z[42]=e[6];
    z[43]=e[14];
    z[44]=f[0];
    z[45]=f[1];
    z[46]=f[2];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[10];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[13];
    z[56]=f[14];
    z[57]=f[15];
    z[58]=f[16];
    z[59]=f[17];
    z[60]=f[18];
    z[61]=f[21];
    z[62]=f[23];
    z[63]=f[30];
    z[64]=f[31];
    z[65]=f[32];
    z[66]=f[33];
    z[67]=f[34];
    z[68]=f[35];
    z[69]=f[36];
    z[70]=f[37];
    z[71]=f[38];
    z[72]=f[39];
    z[73]=f[43];
    z[74]=f[46];
    z[75]=f[47];
    z[76]=f[48];
    z[77]=f[49];
    z[78]=f[50];
    z[79]=f[51];
    z[80]=f[52];
    z[81]=f[53];
    z[82]=f[55];
    z[83]=f[58];
    z[84]=f[60];
    z[85]=z[4] + z[2];
    z[86]=z[1]*i;
    z[87]=z[86] - z[5];
    z[88]=2*z[9];
    z[89]= - n<T>(13,3)*z[11] - z[10] + z[3] + z[88] - 5*z[87] - z[85];
    z[89]=z[11]*z[89];
    z[90]=10*z[7];
    z[91]=5*z[86];
    z[92]= - 2*z[6] + z[3] + z[90] + 4*z[4] - z[91] + z[5];
    z[92]=2*z[92] - 13*z[10];
    z[92]=z[10]*z[92];
    z[89]=z[92] + z[89];
    z[92]=2*z[3];
    z[93]=2*z[4];
    z[94]=z[92] - z[93];
    z[95]= - z[88] + z[7] + n<T>(10,3) + z[87] - z[94];
    z[96]=n<T>(2,3)*z[3];
    z[95]=z[95]*z[96];
    z[97]=n<T>(5,3)*z[5];
    z[98]=2*z[86];
    z[99]= - z[98] + z[97];
    z[99]=z[5]*z[99];
    z[100]=2*z[2];
    z[101]= - n<T>(2,3)*z[2] + n<T>(8,3)*z[86] - z[5];
    z[101]=z[101]*z[100];
    z[102]=n<T>(1,3)*z[4];
    z[103]=z[2] - z[5];
    z[104]= - 2*z[103] - n<T>(19,3)*z[4];
    z[104]=z[104]*z[102];
    z[105]=2*z[5];
    z[106]=z[105] - z[100];
    z[107]=13 - 20*z[86];
    z[107]= - n<T>(34,9)*z[7] + n<T>(20,9)*z[4] + n<T>(1,9)*z[107] - z[106];
    z[107]=z[7]*z[107];
    z[108]=z[98] - z[9];
    z[109]=n<T>(4,3)*z[9];
    z[110]= - z[108]*z[109];
    z[111]=n<T>(4,3)*z[6];
    z[112]=z[4] - z[2];
    z[113]=z[3] - z[112];
    z[113]=z[113]*z[111];
    z[89]=z[113] + z[95] + z[110] + z[107] + z[104] + z[99] + z[101] + n<T>(1,3)*z[89];
    z[89]=z[11]*z[89];
    z[95]=z[100] - z[93];
    z[99]=z[3] - z[7];
    z[101]=4*z[5];
    z[99]= - n<T>(151,18)*z[6] + n<T>(25,9)*z[9] + n<T>(11,9)*z[86] - z[101] + z[95]
    + 4*z[99];
    z[104]=n<T>(1,3)*z[6];
    z[99]=z[99]*z[104];
    z[107]=19*z[5];
    z[110]=z[2] + n<T>(7,3)*z[86] - z[107];
    z[113]=n<T>(4,3)*z[7];
    z[110]= - n<T>(1,27)*z[9] + z[113] + n<T>(1,9)*z[110] + z[93];
    z[110]=z[9]*z[110];
    z[114]=31*z[5];
    z[115]=5*z[2];
    z[116]= - z[115] - n<T>(61,3)*z[86] + z[114];
    z[116]= - n<T>(16,9)*z[8] - z[104] - n<T>(61,54)*z[9] + n<T>(7,6)*z[7] + n<T>(1,18)*
    z[116] + z[4];
    z[116]=z[8]*z[116];
    z[117]= - 11*z[86] - n<T>(13,2)*z[5];
    z[117]=z[5]*z[117];
    z[118]= - 8*z[2] - 10*z[86] + 43*z[5];
    z[118]=z[2]*z[118];
    z[117]=z[117] + z[118];
    z[118]= - z[98] + z[112];
    z[118]=z[118]*z[93];
    z[119]=n<T>(1,9)*z[7];
    z[120]=8*z[7] + z[5] - 13*z[2];
    z[120]=z[120]*z[119];
    z[121]=n<T>(4,3)*z[3];
    z[122]=z[98] - z[5];
    z[123]=z[7] - z[4] + z[122];
    z[123]=z[123]*z[121];
    z[99]=z[116] + z[99] + z[123] + z[110] + z[120] + n<T>(1,9)*z[117] + 
    z[118];
    z[99]=z[8]*z[99];
    z[110]=2*z[87];
    z[116]= - z[110] - z[2];
    z[100]=z[116]*z[100];
    z[116]=z[122]*z[5];
    z[117]= - z[86] + n<T>(1,2)*z[4];
    z[117]=z[4]*z[117];
    z[100]=13*z[117] + n<T>(31,9)*z[116] + z[100];
    z[105]=23*z[86] - z[105];
    z[117]=7*z[4];
    z[105]=n<T>(1,6)*z[7] + n<T>(1,3)*z[105] - z[117];
    z[105]=z[7]*z[105];
    z[118]= - z[98] + z[103];
    z[118]=2*z[118] - z[9];
    z[118]=z[9]*z[118];
    z[120]= - 41*z[86] + 50*z[5];
    z[85]=n<T>(1,9)*z[120] - z[85];
    z[85]= - n<T>(41,9)*z[3] + z[88] + 2*z[85] + z[7];
    z[85]=z[85]*z[96];
    z[96]=17*z[86] - 23*z[5];
    z[94]=n<T>(1,3)*z[96] - z[94];
    z[94]=2*z[94] + n<T>(29,3)*z[6];
    z[94]=z[94]*z[104];
    z[96]= - n<T>(55,2)*z[86] - z[114];
    z[96]=n<T>(13,2)*z[4] + n<T>(1,9)*z[96] - 4*z[2];
    z[96]= - n<T>(47,9)*z[10] + n<T>(29,9)*z[6] - n<T>(56,27)*z[3] + z[109] + n<T>(1,3)*
    z[96] + n<T>(13,2)*z[7];
    z[96]=z[10]*z[96];
    z[85]=z[96] + z[94] + z[85] + n<T>(2,3)*z[118] + n<T>(1,3)*z[100] + z[105];
    z[85]=z[10]*z[85];
    z[94]=10*z[8];
    z[96]= - n<T>(463,4)*z[2] + 191*z[86] - n<T>(185,2)*z[5];
    z[96]=n<T>(1,3)*z[96] - n<T>(97,4)*z[4];
    z[90]= - z[94] + 10*z[9] + n<T>(1,3)*z[96] + z[90];
    z[90]=z[19]*z[90];
    z[96]= - n<T>(61,6)*z[5] + 44 + n<T>(31,3)*z[86];
    z[96]=z[5]*z[96];
    z[100]=n<T>(337,12)*z[2] - n<T>(151,12)*z[5] - 22 - n<T>(541,12)*z[86];
    z[100]=z[2]*z[100];
    z[96]=z[96] + z[100];
    z[96]=z[2]*z[96];
    z[100]=119*z[86] - n<T>(95,2)*z[5];
    z[100]=z[5]*z[100];
    z[105]= - 67*z[2] + n<T>(53,2)*z[86] + 107*z[5];
    z[105]=z[2]*z[105];
    z[100]=n<T>(1,4)*z[100] + z[105];
    z[105]=13*z[86];
    z[109]=z[105] - n<T>(17,24)*z[5];
    z[109]= - n<T>(131,3)*z[4] + 7*z[109] - n<T>(53,12)*z[2];
    z[109]=z[4]*z[109];
    z[100]=n<T>(1,3)*z[100] + z[109];
    z[100]=z[100]*z[102];
    z[109]= - 341*z[5] - n<T>(1715,3)*z[2];
    z[109]= - n<T>(1465,6)*z[9] - 283*z[7] + n<T>(1,2)*z[109] + n<T>(3091,3)*z[4];
    z[109]= - 32*z[11] - n<T>(92,3)*z[10] - n<T>(691,12)*z[6] + n<T>(1,4)*z[109]
     - 
   10*z[3];
    z[94]=n<T>(1,9)*z[109] - z[94];
    z[94]=z[31]*z[94];
    z[91]= - z[91] + n<T>(13,9)*z[5];
    z[91]=z[91]*npow(z[5],2);
    z[109]=z[7] + z[112];
    z[109]=n<T>(29,9)*z[8] + 8*z[109] + n<T>(101,9)*z[6];
    z[109]=z[42]*z[109];
    z[90]=z[75] + z[90] + z[94] + z[100] + z[91] + n<T>(1,3)*z[96] + z[109]
    - z[77];
    z[91]= - z[98] - z[115];
    z[91]=z[2]*z[91];
    z[94]= - n<T>(11,12)*z[7] - n<T>(11,6)*z[86] + z[101];
    z[94]=z[7]*z[94];
    z[91]=z[94] - n<T>(13,3)*z[116] + z[91];
    z[94]=n<T>(2,9)*z[86] + z[2];
    z[94]=2*z[94] - z[102];
    z[94]=z[4]*z[94];
    z[95]= - n<T>(11,108)*z[9] - n<T>(13,18)*z[7] + n<T>(11,54)*z[86] + z[95];
    z[95]=z[9]*z[95];
    z[96]=5*z[5];
    z[100]= - z[98] + z[96];
    z[101]=n<T>(1,3)*z[3];
    z[100]= - z[101] - z[7] + n<T>(1,3)*z[100] - z[4];
    z[100]=z[100]*z[121];
    z[102]= - n<T>(11,12)*z[9] + n<T>(37,4)*z[7] - n<T>(145,12)*z[86] + 13*z[5] + 10
   *z[112];
    z[102]=n<T>(175,18)*z[6] + n<T>(1,3)*z[102] - 14*z[3];
    z[102]=z[102]*z[104];
    z[91]=z[102] + z[100] + z[95] + z[94] + n<T>(1,3)*z[91];
    z[91]=z[6]*z[91];
    z[94]=17*z[7];
    z[95]=z[94] - 22;
    z[100]=z[86]*z[95];
    z[102]=z[86]*z[5];
    z[102]=z[102] + z[31];
    z[100]=z[100] + z[102];
    z[100]=z[15]*z[100];
    z[95]=z[87] - z[95];
    z[95]=z[23]*z[95];
    z[104]=z[86]*z[4];
    z[104]=z[104] + z[31];
    z[109]=z[6]*z[86];
    z[109]=z[109] + z[104];
    z[109]=z[14]*z[109];
    z[112]=z[86] - z[4];
    z[114]= - z[6] + z[112];
    z[114]=z[22]*z[114];
    z[95]=z[100] + z[95] + z[109] + z[114];
    z[100]=n<T>(1,2)*z[5];
    z[109]= - z[86] + z[100];
    z[109]=z[5]*z[109];
    z[100]= - n<T>(17,4)*z[2] + 47*z[86] - z[100];
    z[100]=z[2]*z[100];
    z[100]=n<T>(113,4)*z[109] + z[100];
    z[109]=19*z[2];
    z[107]= - n<T>(11,4)*z[7] - z[109] - n<T>(5,2)*z[86] + z[107];
    z[107]=z[107]*z[119];
    z[114]= - 25*z[86] - 37*z[5];
    z[114]=n<T>(25,24)*z[4] + n<T>(1,12)*z[114] + z[2];
    z[114]=z[4]*z[114];
    z[115]= - n<T>(10,3)*z[9] + n<T>(5,12)*z[7] + n<T>(25,8)*z[4] - n<T>(47,6)*z[2]
     - 
    z[98] + n<T>(113,24)*z[5];
    z[115]=z[9]*z[115];
    z[100]=n<T>(1,3)*z[115] + z[107] + n<T>(1,9)*z[100] + z[114];
    z[100]=z[9]*z[100];
    z[96]= - z[98] - z[96];
    z[96]=4*z[96] + 17*z[2];
    z[96]=z[2]*z[96];
    z[107]=z[98] - z[4];
    z[107]=z[107]*z[117];
    z[114]=16*z[86] - z[5];
    z[114]=z[5]*z[114];
    z[96]=z[107] + n<T>(4,3)*z[114] + z[96];
    z[107]=z[110] - 5*z[7];
    z[107]=z[7]*z[107];
    z[96]=2*z[96] + z[107];
    z[107]= - z[103] - z[108];
    z[108]=4*z[9];
    z[107]=z[107]*z[108];
    z[109]=z[109] - n<T>(29,3)*z[5] - 5 - n<T>(32,3)*z[86];
    z[93]=n<T>(31,9)*z[3] + n<T>(1,3)*z[109] + z[93];
    z[93]=z[93]*z[92];
    z[93]=z[93] + n<T>(1,3)*z[96] + z[107];
    z[93]=z[93]*z[101];
    z[88]= - n<T>(31,9)*z[10] - n<T>(13,9)*z[3] - z[88] - n<T>(5,3) + z[98] - z[106]
   ;
    z[88]=z[27]*z[88];
    z[96]= - z[10] + z[112] + z[11];
    z[96]=n<T>(19,3)*z[6] + n<T>(13,3)*z[3] - 2*z[96];
    z[96]=z[21]*z[96];
    z[88]=z[88] + z[96] + z[84] + z[62];
    z[96]=n<T>(13,2)*z[2] - 7*z[86] - z[97];
    z[96]=z[2]*z[96];
    z[97]=n<T>(37,6)*z[4] - n<T>(37,3)*z[86] - z[106];
    z[97]=z[4]*z[97];
    z[98]=z[105] - n<T>(55,2)*z[5];
    z[98]=z[5]*z[98];
    z[96]=z[97] + n<T>(1,3)*z[98] + z[96];
    z[97]= - n<T>(215,54)*z[7] + n<T>(43,18)*z[4] - n<T>(13,18)*z[2] + n<T>(47,18)*z[5]
    - 5 - n<T>(143,36)*z[86];
    z[97]=z[7]*z[97];
    z[96]=n<T>(1,3)*z[96] + z[97];
    z[96]=z[7]*z[96];
    z[97]= - 44 + n<T>(67,6)*z[86];
    z[97]= - n<T>(67,36)*z[4] + n<T>(77,36)*z[2] + n<T>(1,3)*z[97] + 8*z[5];
    z[97]=n<T>(4,3)*z[8] - z[111] + n<T>(1,3)*z[97] - z[108];
    z[97]=z[18]*z[97];
    z[98]=z[86]*z[9];
    z[98]=z[98] + z[31];
    z[101]=z[10]*z[86];
    z[101]=z[101] + z[98];
    z[101]=z[17]*z[101];
    z[105]=z[86] - z[9];
    z[106]= - z[10] + z[105];
    z[106]=z[28]*z[106];
    z[101]=z[70] + z[101] + z[106];
    z[106]=z[9] + z[2];
    z[106]= - z[86] + n<T>(1,2)*z[106];
    z[106]=z[25]*z[106];
    z[106]=z[106] - z[44];
    z[107]= - z[8]*z[86];
    z[98]=z[107] - z[98];
    z[98]=z[16]*z[98];
    z[105]= - z[8] + z[105];
    z[105]=z[26]*z[105];
    z[98]=z[98] + z[105];
    z[105]=z[11]*z[86];
    z[104]=z[105] + z[104];
    z[104]=z[13]*z[104];
    z[105]= - z[11] + z[112];
    z[105]=z[29]*z[105];
    z[104]=z[104] + z[105];
    z[105]= - z[3]*z[86];
    z[102]=z[105] - z[102];
    z[102]=z[12]*z[102];
    z[105]= - z[3] + z[87];
    z[105]=z[20]*z[105];
    z[102]=z[102] + z[105];
    z[87]= - z[113] - z[87];
    z[87]= - n<T>(40,3)*z[11] - 8*z[10] + 7*z[87] - z[92];
    z[87]=n<T>(1,3)*z[87] - z[8];
    z[87]=z[30]*z[87];
    z[86]= - z[9] + z[86] + z[103];
    z[86]= - z[94] + 8*z[86];
    z[86]=n<T>(1,9)*z[86] - z[8];
    z[86]=z[24]*z[86];
    z[92]=z[57] + z[51];
    z[94]=z[68] - z[54];
    z[103]= - n<T>(131,54)*z[38] + n<T>(541,108)*z[36] - n<T>(41,108)*z[32];
    z[103]=i*z[103];
    z[105]= - z[10] - z[11];
    z[105]=z[43]*z[105];

    r +=  - n<T>(211,24)*z[33] + n<T>(31,18)*z[34] + n<T>(1763,648)*z[35] - n<T>(337,108)*z[37]
     + n<T>(17,27)*z[39] - n<T>(31,24)*z[40]
     + n<T>(6017,432)*z[41] - 
      n<T>(20,9)*z[45] + n<T>(8,9)*z[46] + n<T>(169,216)*z[47] - n<T>(67,27)*z[48]
     + n<T>(71,18)*z[49] - n<T>(8,3)*z[50] - n<T>(164,27)*z[52]
     + n<T>(161,72)*z[53]
     + n<T>(233,108)*z[55]
     + n<T>(14,9)*z[56]
     + n<T>(29,9)*z[58]
     + n<T>(17,24)*z[59]
     + n<T>(13,27)
      *z[60] - n<T>(28,9)*z[61] - n<T>(2,27)*z[63] + n<T>(19,12)*z[64] + n<T>(19,18)*
      z[65] + n<T>(32,9)*z[66] - n<T>(25,18)*z[67] + n<T>(191,108)*z[69] + n<T>(11,3)*
      z[71] - n<T>(59,36)*z[72] - n<T>(10,9)*z[73] + z[74] + z[76] + z[78] + n<T>(37,18)*z[79]
     - n<T>(5,3)*z[80]
     + n<T>(16,3)*z[81] - n<T>(13,6)*z[82]
     + n<T>(13,2)*
      z[83] + z[85] + z[86] + 2*z[87] + n<T>(4,3)*z[88] + z[89] + n<T>(1,3)*
      z[90] + z[91] - n<T>(32,3)*z[92] + z[93] + n<T>(13,9)*z[94] + n<T>(2,9)*z[95]
       + z[96] + z[97] + n<T>(5,27)*z[98] + z[99] + z[100] + 4*z[101] + n<T>(28,27)*z[102]
     + z[103]
     + n<T>(38,9)*z[104]
     + n<T>(26,3)*z[105]
     + n<T>(53,9)*
      z[106];
 
    return r;
}

template std::complex<double> qg_2lha_tf197(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf197(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
