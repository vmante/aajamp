#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf473(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[9];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=c[13];
    z[4]=c[20];
    z[5]=c[23];
    z[6]=f[42];
    z[7]=f[77];
    z[8]=n<T>(1,270)*z[1] - n<T>(24,5)*z[3] + n<T>(1,10)*z[5] - 2*z[4];
    z[8]=i*z[8];

    r += n<T>(5,3)*z[2] - z[6] - n<T>(1,2)*z[7] + z[8];
 
    return r;
}

template std::complex<double> qg_2lha_tf473(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf473(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
