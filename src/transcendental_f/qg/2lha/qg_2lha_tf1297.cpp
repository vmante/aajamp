#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1297(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[72];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[4];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[1];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[18];
    z[20]=c[19];
    z[21]=c[21];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[29];
    z[27]=c[30];
    z[28]=c[31];
    z[29]=e[3];
    z[30]=e[10];
    z[31]=f[0];
    z[32]=f[1];
    z[33]=f[3];
    z[34]=f[4];
    z[35]=f[5];
    z[36]=f[6];
    z[37]=f[8];
    z[38]=f[9];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[13];
    z[42]=f[14];
    z[43]=f[15];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[18];
    z[47]=f[21];
    z[48]=f[26];
    z[49]=f[27];
    z[50]=f[28];
    z[51]=f[29];
    z[52]=z[1]*i;
    z[53]=z[52] - z[4];
    z[54]=n<T>(1,2)*z[8];
    z[55]=z[53] + z[54];
    z[55]=z[55]*z[8];
    z[56]=z[52]*z[4];
    z[57]=npow(z[4],2);
    z[58]= - z[56] + n<T>(1,2)*z[57];
    z[59]= - n<T>(71,2)*z[58] + 41*z[55];
    z[60]=n<T>(1,2)*z[6];
    z[61]=n<T>(1,2)*z[2];
    z[62]=n<T>(71,8)*z[53] + 28*z[8];
    z[62]= - z[60] - n<T>(51,8)*z[5] + n<T>(1,3)*z[62] + z[61];
    z[62]=z[7]*z[62];
    z[63]=z[8] - z[4];
    z[64]=z[63] + z[52];
    z[65]=z[64] + z[61];
    z[65]=z[65]*z[2];
    z[66]=n<T>(1,2)*z[5];
    z[67]= - z[66] - z[53];
    z[67]=z[5]*z[67];
    z[60]= - z[60] + z[2] + z[64];
    z[60]=z[6]*z[60];
    z[59]=z[62] + z[60] + n<T>(51,4)*z[67] + n<T>(1,6)*z[59] - z[65];
    z[59]=z[7]*z[59];
    z[60]=n<T>(5,2)*z[52] + z[54] - z[4];
    z[60]=z[8]*z[60];
    z[62]= - n<T>(1,4)*z[5] + n<T>(1,2)*z[52] - z[63] - z[2];
    z[62]=z[5]*z[62];
    z[63]=n<T>(1,2)*z[3];
    z[64]= - z[8] + n<T>(17,6)*z[4] - z[52];
    z[64]=5*z[64] - n<T>(83,3)*z[2];
    z[64]=n<T>(11,6)*z[3] + n<T>(1,2)*z[64] + z[5];
    z[64]=z[64]*z[63];
    z[67]= - z[4] + n<T>(83,2)*z[52];
    z[67]=n<T>(1,6)*z[2] + n<T>(1,3)*z[67] + z[8];
    z[67]=z[2]*z[67];
    z[60]=z[64] + z[62] + z[67] + n<T>(85,12)*z[58] + z[60];
    z[60]=z[3]*z[60];
    z[62]=z[63] - z[4] - z[52];
    z[62]=z[3]*z[62];
    z[63]= - n<T>(2,3)*z[6] + n<T>(27,8)*z[3] - n<T>(11,3)*z[2] + z[54] + n<T>(7,24)*
    z[4] + 2*z[52];
    z[63]=z[6]*z[63];
    z[64]=n<T>(37,2)*z[4] + 22*z[52];
    z[64]= - n<T>(37,12)*z[2] + n<T>(1,3)*z[64] - z[8];
    z[64]=z[2]*z[64];
    z[62]=z[63] + n<T>(27,4)*z[62] + z[64] + n<T>(7,12)*z[58] - z[55];
    z[62]=z[6]*z[62];
    z[55]= - n<T>(101,4)*z[58] + 13*z[55];
    z[63]=n<T>(101,2)*z[53] + 127*z[8];
    z[63]=n<T>(1,3)*z[63] + z[2];
    z[63]=n<T>(1,2)*z[63] + n<T>(1,3)*z[5];
    z[63]=z[63]*z[66];
    z[55]=z[63] + n<T>(1,3)*z[55] + z[65];
    z[55]=z[5]*z[55];
    z[63]=z[3] + z[5];
    z[64]= - z[52]*z[63];
    z[64]= - z[16] + z[64];
    z[64]=z[10]*z[64];
    z[63]= - z[52] + z[63];
    z[63]=z[14]*z[63];
    z[63]= - z[64] - z[63] + z[50] - z[46];
    z[64]=67*z[4] + 7*z[52];
    z[64]=n<T>(1,2)*z[64] - 73*z[8];
    z[64]=z[8]*z[64];
    z[56]=z[64] + 43*z[57] - 49*z[56];
    z[56]=z[8]*z[56];
    z[64]= - z[4] - z[8];
    z[64]=z[52]*z[64];
    z[64]= - z[16] + z[64];
    z[64]=z[9]*z[64];
    z[53]= - z[8] + z[53];
    z[53]=z[13]*z[53];
    z[53]=z[64] + z[53];
    z[57]= - z[57]*z[52];
    z[64]= - n<T>(35,4)*z[4] - 41*z[52];
    z[64]=n<T>(1,3)*z[64] + n<T>(13,2)*z[2];
    z[64]=z[2]*z[64];
    z[58]= - n<T>(23,3)*z[58] + z[64];
    z[58]=z[2]*z[58];
    z[64]=2*z[4];
    z[54]=n<T>(71,18)*z[7] + n<T>(149,72)*z[6] - n<T>(71,36)*z[3] + n<T>(401,72)*z[5]
     - 
   n<T>(2,9)*z[2] - z[64] + z[54];
    z[54]=z[16]*z[54];
    z[64]= - z[64] + 2*z[8];
    z[65]=n<T>(85,6)*z[3] + 2*z[5] + n<T>(73,6)*z[2] - n<T>(85,3)*z[52] - z[64];
    z[65]=z[11]*z[65];
    z[64]= - 2*z[7] + n<T>(7,6)*z[6] + n<T>(19,6)*z[2] - n<T>(7,3)*z[52] + z[64];
    z[64]=z[15]*z[64];
    z[52]=z[61] + n<T>(1,2)*z[4] - z[52];
    z[52]=z[12]*z[52];
    z[61]=z[27] + z[26];
    z[66]=z[38] - z[36];
    z[67]=z[51] - z[47];
    z[68]=npow(z[4],3);
    z[68]=z[68] - z[41];
    z[69]=n<T>(112,3)*z[24] + n<T>(79,6)*z[22] + z[21] - n<T>(5,9)*z[17];
    z[69]=i*z[69];
    z[70]= - z[8] - z[5];
    z[70]=z[29]*z[70];
    z[71]= - z[8] - z[7];
    z[71]=z[30]*z[71];

    r += 7*z[18] - n<T>(8,3)*z[19] - n<T>(5,18)*z[20] - n<T>(19,3)*z[23] - n<T>(109,6)*
      z[25] - n<T>(94,9)*z[28] + n<T>(25,2)*z[31] + n<T>(9,4)*z[32] + n<T>(121,24)*
      z[33] - n<T>(71,24)*z[34] + n<T>(25,6)*z[35] - 5*z[37] - n<T>(17,24)*z[39]
     +  n<T>(77,24)*z[40] - n<T>(13,4)*z[42] + n<T>(21,2)*z[43] - n<T>(19,8)*z[44]
     + n<T>(3,8)
      *z[45] - z[48] - z[49] + n<T>(23,3)*z[52] + n<T>(37,6)*z[53] + z[54] + 
      z[55] + n<T>(1,6)*z[56] + z[57] + z[58] + z[59] + z[60] + 2*z[61] + 
      z[62] - n<T>(3,2)*z[63] + z[64] + z[65] + n<T>(5,2)*z[66] + n<T>(1,2)*z[67]
       + n<T>(1,3)*z[68] + z[69] + n<T>(95,6)*z[70] + n<T>(71,6)*z[71];
 
    return r;
}

template std::complex<double> qg_2lha_tf1297(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1297(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
