#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1149(
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[4];
  std::complex<T> r(0,0);

    z[1]=d[5];
    z[2]=d[8];

    r +=  - z[1] + z[2];
 
    return r;
}

template std::complex<double> qg_2lha_tf1149(
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1149(
  const std::array<std::complex<dd_real>,25>& d
);
#endif
