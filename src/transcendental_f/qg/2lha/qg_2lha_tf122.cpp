#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf122(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[97];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[11];
    z[13]=d[9];
    z[14]=d[17];
    z[15]=d[18];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[11];
    z[24]=e[12];
    z[25]=c[3];
    z[26]=c[11];
    z[27]=c[15];
    z[28]=c[19];
    z[29]=c[23];
    z[30]=c[25];
    z[31]=c[26];
    z[32]=c[28];
    z[33]=e[13];
    z[34]=e[3];
    z[35]=e[6];
    z[36]=e[14];
    z[37]=f[2];
    z[38]=f[3];
    z[39]=f[4];
    z[40]=f[5];
    z[41]=f[6];
    z[42]=f[7];
    z[43]=f[10];
    z[44]=f[11];
    z[45]=f[12];
    z[46]=f[13];
    z[47]=f[16];
    z[48]=f[17];
    z[49]=f[23];
    z[50]=f[30];
    z[51]=f[31];
    z[52]=f[32];
    z[53]=f[33];
    z[54]=f[34];
    z[55]=f[35];
    z[56]=f[36];
    z[57]=f[39];
    z[58]=f[43];
    z[59]=f[50];
    z[60]=f[51];
    z[61]=f[52];
    z[62]=f[53];
    z[63]=f[55];
    z[64]=f[56];
    z[65]=f[58];
    z[66]=f[60];
    z[67]=n<T>(1,2)*z[5];
    z[68]=z[1]*i;
    z[69]=z[67] - z[68];
    z[70]=z[69]*z[5];
    z[71]=n<T>(1,2)*z[8];
    z[72]=z[71] - z[68];
    z[73]= - z[5] + z[72];
    z[73]=z[8]*z[73];
    z[73]=n<T>(7,3)*z[70] + 3*z[73];
    z[74]=n<T>(1,4)*z[2];
    z[75]= - z[74] + 4*z[68] + z[67];
    z[76]=n<T>(1,3)*z[2];
    z[75]=z[75]*z[76];
    z[77]=n<T>(1,2)*z[9];
    z[78]=z[77] - z[68];
    z[79]=z[9]*z[78];
    z[80]=z[2] - z[5];
    z[81]=n<T>(35,12)*z[6] - n<T>(13,2)*z[9] - n<T>(23,6)*z[68] - z[80];
    z[82]=n<T>(1,2)*z[6];
    z[81]=z[81]*z[82];
    z[83]=n<T>(11,12)*z[4];
    z[84]=11*z[68] + n<T>(7,2)*z[5];
    z[84]=n<T>(1,3)*z[84] + n<T>(3,2)*z[8];
    z[84]= - z[83] + n<T>(23,24)*z[6] + n<T>(7,8)*z[9] + n<T>(1,4)*z[84] - n<T>(2,3)*
    z[2];
    z[84]=z[4]*z[84];
    z[73]=z[84] + z[81] + n<T>(7,4)*z[79] + n<T>(1,4)*z[73] + z[75];
    z[73]=z[4]*z[73];
    z[75]=z[72]*z[8];
    z[79]=3*z[9];
    z[81]= - z[68] - z[9];
    z[81]=z[81]*z[79];
    z[84]=n<T>(1,2)*z[2];
    z[85]=z[69] + z[84];
    z[86]= - n<T>(5,2)*z[13] + 3*z[6] + z[77] - z[85];
    z[86]=z[13]*z[86];
    z[87]= - z[2]*z[80];
    z[81]=z[86] + z[81] + z[87] - z[70] + z[75];
    z[86]=n<T>(3,2)*z[9];
    z[87]=n<T>(1,3)*z[6];
    z[88]=z[87] + z[86] - z[84] - n<T>(1,3)*z[68] + z[67];
    z[88]=z[6]*z[88];
    z[79]=z[79] + z[80];
    z[79]= - z[83] + n<T>(1,2)*z[79] + z[87];
    z[79]=z[4]*z[79];
    z[79]=z[79] + z[88] + n<T>(1,2)*z[81];
    z[79]=z[13]*z[79];
    z[69]=z[69]*z[67];
    z[75]= - z[69] - z[75];
    z[81]=n<T>(1,3)*z[8];
    z[83]=z[68] - n<T>(1,3)*z[5];
    z[83]= - z[6] - z[77] + n<T>(1,2)*z[83] - z[81];
    z[87]=n<T>(1,3)*z[3];
    z[88]=n<T>(1,3)*z[7];
    z[83]=n<T>(1,3)*z[10] + z[88] + n<T>(1,2)*z[83] + z[87];
    z[83]=z[10]*z[83];
    z[89]=z[68] - z[5];
    z[90]= - z[77] - z[89];
    z[90]=z[90]*z[77];
    z[91]=z[68] - z[8];
    z[82]= - z[82] - z[91];
    z[82]=z[6]*z[82];
    z[92]=n<T>(1,2)*z[3] + z[89];
    z[92]=z[92]*z[87];
    z[75]=z[83] + z[92] + z[82] + n<T>(1,3)*z[75] + z[90];
    z[82]=z[91]*z[88];
    z[75]=z[82] + n<T>(1,2)*z[75];
    z[75]=z[10]*z[75];
    z[82]= - z[5] + z[84] + z[91];
    z[83]=z[82]*z[84];
    z[90]=z[67] + z[68];
    z[92]=n<T>(1,4)*z[8];
    z[93]= - z[92] - z[90];
    z[93]=z[8]*z[93];
    z[94]= - n<T>(67,6)*z[9] - z[2] - n<T>(7,2)*z[90] + z[8];
    z[77]=z[94]*z[77];
    z[77]=z[77] - z[83] - n<T>(7,4)*z[70] + z[93];
    z[77]=z[9]*z[77];
    z[93]=z[91] + z[80];
    z[94]=5*z[7] - 2*z[93] + 7*z[6];
    z[94]=z[19]*z[94];
    z[95]=n<T>(7,2)*z[9];
    z[93]= - n<T>(5,2)*z[3] - z[95] + z[93];
    z[93]=z[22]*z[93];
    z[85]=z[17]*z[85];
    z[96]=z[7] + z[10];
    z[96]=z[35]*z[96];
    z[77]=z[96] + z[58] - z[55] - z[50] + z[77] + z[94] + z[93] + z[85];
    z[85]=z[72] + z[5];
    z[93]=z[85]*z[71];
    z[85]=n<T>(5,4)*z[9] - z[84] + z[85];
    z[85]=z[9]*z[85];
    z[78]= - n<T>(11,12)*z[3] + z[2] - z[92] - n<T>(1,4)*z[5] - z[78];
    z[78]=z[3]*z[78];
    z[94]= - n<T>(1,2)*z[68] + z[5];
    z[94]=z[5]*z[94];
    z[78]=z[78] + z[85] + z[83] + z[94] + z[93];
    z[78]=z[78]*z[87];
    z[82]=z[2]*z[82];
    z[71]=n<T>(7,6)*z[7] - n<T>(5,2)*z[6] + z[2] + z[71] + z[90];
    z[71]=z[7]*z[71];
    z[83]=2*z[8] - z[89];
    z[83]=z[8]*z[83];
    z[85]= - 2*z[6] + z[80];
    z[85]=z[6]*z[85];
    z[71]=z[71] + z[85] + z[82] + z[70] + z[83];
    z[71]=z[71]*z[88];
    z[82]= - z[5] - z[3];
    z[82]=z[68]*z[82];
    z[82]= - z[25] + z[82];
    z[82]=z[11]*z[82];
    z[83]=z[68]*z[8];
    z[83]=z[83] + z[25];
    z[85]=z[9]*z[68];
    z[85]=z[85] + z[83];
    z[85]=z[15]*z[85];
    z[87]= - z[3] + z[89];
    z[87]=z[18]*z[87];
    z[88]= - z[9] + z[91];
    z[88]=z[23]*z[88];
    z[82]=z[87] + z[88] + z[66] - z[52] + z[40] + z[82] + z[85];
    z[85]=z[67] - z[72];
    z[85]=z[8]*z[85];
    z[87]= - n<T>(13,4)*z[68] - z[5];
    z[87]=n<T>(7,12)*z[2] + n<T>(1,3)*z[87] - z[92];
    z[87]=z[2]*z[87];
    z[85]=z[87] - n<T>(5,6)*z[70] + z[85];
    z[85]=z[2]*z[85];
    z[87]= - n<T>(29,2)*z[68] - z[5];
    z[76]= - n<T>(71,9)*z[6] + z[95] + z[76] + n<T>(1,3)*z[87] - z[8];
    z[76]=z[6]*z[76];
    z[87]=13*z[68] + n<T>(19,2)*z[9];
    z[87]=z[9]*z[87];
    z[76]=z[87] + z[76];
    z[74]=z[74] + n<T>(1,2)*z[89] + z[8];
    z[74]=z[2]*z[74];
    z[87]= - n<T>(7,4)*z[8] + n<T>(7,2)*z[68] - z[5];
    z[87]=z[8]*z[87];
    z[69]=z[74] + z[69] + z[87];
    z[69]=n<T>(1,3)*z[69] + n<T>(1,4)*z[76];
    z[69]=z[6]*z[69];
    z[74]=z[13] + z[4];
    z[76]=z[68]*z[74];
    z[76]=z[25] + z[76];
    z[76]=z[12]*z[76];
    z[74]=z[68] - z[74];
    z[74]=z[24]*z[74];
    z[74]= - z[32] + z[76] + z[74];
    z[67]=z[81] - 3*z[68] + z[67];
    z[67]=z[8]*z[67];
    z[67]=z[70] + z[67];
    z[67]=z[67]*z[92];
    z[70]= - n<T>(29,6)*z[13] - n<T>(35,6)*z[6] - z[86] + z[80];
    z[70]=z[33]*z[70];
    z[76]= - z[7]*z[68];
    z[76]=z[76] - z[83];
    z[76]=z[14]*z[76];
    z[68]=n<T>(1,2)*z[4] + z[84] - z[68];
    z[68]=z[16]*z[68];
    z[72]=z[84] + z[72];
    z[72]=z[20]*z[72];
    z[80]= - z[3] - z[10];
    z[80]=z[34]*z[80];
    z[80]= - z[37] + z[80] + z[49] + z[46] - z[43];
    z[81]=z[41] + z[64] - z[61] + z[57] - z[54];
    z[83]=z[47] - z[44];
    z[84]=n<T>(11,3)*z[31] + n<T>(13,12)*z[29] - n<T>(23,72)*z[26];
    z[84]=i*z[84];
    z[86]= - n<T>(1,9)*z[5] - n<T>(9,4)*z[8];
    z[86]=n<T>(1,6)*z[10] - n<T>(1,9)*z[7] + n<T>(7,18)*z[13] - n<T>(1,18)*z[3] + n<T>(13,36)
   *z[4] - n<T>(11,18)*z[6] - n<T>(3,4)*z[9] + n<T>(1,2)*z[86] + n<T>(5,9)*z[2];
    z[86]=z[25]*z[86];
    z[87]= - z[7] + z[91];
    z[87]=z[21]*z[87];
    z[88]= - z[9] - z[13];
    z[88]=z[36]*z[88];

    r +=  - n<T>(5,6)*z[27] - n<T>(5,9)*z[28] - n<T>(7,12)*z[30] + n<T>(7,24)*z[38]
     - 
      n<T>(11,24)*z[39] - n<T>(4,3)*z[42] + n<T>(1,24)*z[45] + n<T>(3,8)*z[48] - n<T>(11,12)
      *z[51] - n<T>(8,3)*z[53] + n<T>(1,12)*z[56] + n<T>(3,2)*z[59] + n<T>(23,24)*z[60]
       + 5*z[62] - n<T>(7,8)*z[63] + n<T>(19,8)*z[65] + z[67] + n<T>(7,3)*z[68] + 
      z[69] + z[70] + z[71] + z[72] + z[73] + n<T>(11,6)*z[74] + z[75] + 
      z[76] + n<T>(1,3)*z[77] + z[78] + z[79] + n<T>(1,6)*z[80] - n<T>(1,4)*z[81]
       + n<T>(1,2)*z[82] - n<T>(1,8)*z[83] + z[84] + z[85] + z[86] + z[87] + n<T>(7,2)*z[88];
 
    return r;
}

template std::complex<double> qg_2lha_tf122(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf122(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
