#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf206(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[99];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[10];
    z[26]=e[12];
    z[27]=e[13];
    z[28]=c[3];
    z[29]=c[11];
    z[30]=c[15];
    z[31]=c[19];
    z[32]=c[23];
    z[33]=c[25];
    z[34]=c[26];
    z[35]=c[28];
    z[36]=c[31];
    z[37]=e[3];
    z[38]=e[6];
    z[39]=e[14];
    z[40]=f[0];
    z[41]=f[1];
    z[42]=f[2];
    z[43]=f[3];
    z[44]=f[4];
    z[45]=f[5];
    z[46]=f[7];
    z[47]=f[10];
    z[48]=f[11];
    z[49]=f[12];
    z[50]=f[13];
    z[51]=f[14];
    z[52]=f[16];
    z[53]=f[17];
    z[54]=f[18];
    z[55]=f[21];
    z[56]=f[30];
    z[57]=f[31];
    z[58]=f[32];
    z[59]=f[33];
    z[60]=f[34];
    z[61]=f[35];
    z[62]=f[36];
    z[63]=f[37];
    z[64]=f[39];
    z[65]=f[43];
    z[66]=f[50];
    z[67]=f[51];
    z[68]=f[52];
    z[69]=f[53];
    z[70]=f[55];
    z[71]=f[58];
    z[72]=f[72];
    z[73]=f[73];
    z[74]=n<T>(2,3)*z[3];
    z[75]=z[1]*i;
    z[76]=z[75] - z[5];
    z[77]= - n<T>(37,9)*z[76] - z[6];
    z[77]=2*z[77] - n<T>(37,9)*z[3];
    z[77]=z[77]*z[74];
    z[78]=4*z[3];
    z[79]=z[78] + n<T>(17,6)*z[4] + 10*z[6] + n<T>(7,6)*z[75] - 4*z[5];
    z[79]=z[11]*z[79];
    z[80]=11*z[5];
    z[81]= - n<T>(383,4)*z[75] + z[80];
    z[81]=n<T>(113,36)*z[4] + n<T>(1,27)*z[81] + n<T>(19,4)*z[6];
    z[81]= - n<T>(35,12)*z[10] + n<T>(53,18)*z[9] - n<T>(65,36)*z[11] + n<T>(1,2)*z[81]
    - n<T>(85,27)*z[3];
    z[81]=z[10]*z[81];
    z[82]= - z[75] + n<T>(1,2)*z[5];
    z[83]=z[82]*z[5];
    z[84]=n<T>(3,8)*z[6] + n<T>(11,4)*z[75] + n<T>(4,3)*z[5];
    z[84]=z[6]*z[84];
    z[85]=n<T>(113,6)*z[4] - n<T>(113,3)*z[75] - 49*z[6];
    z[85]=z[4]*z[85];
    z[86]=n<T>(1,2)*z[9] + z[76];
    z[86]=z[9]*z[86];
    z[77]=z[81] + n<T>(53,9)*z[86] + n<T>(1,3)*z[79] + z[77] + n<T>(1,12)*z[85] + n<T>(11,27)*z[83]
     + z[84];
    z[77]=z[10]*z[77];
    z[79]=5*z[5];
    z[81]=z[82]*z[79];
    z[82]=2*z[75];
    z[84]=z[82] - z[4];
    z[85]=n<T>(1,3)*z[4];
    z[86]=z[84]*z[85];
    z[87]=n<T>(1,3)*z[3];
    z[88]=z[75] - z[4];
    z[89]= - 2*z[88] - z[3];
    z[89]=z[89]*z[87];
    z[90]=z[6] - z[5];
    z[91]=z[90] + z[75];
    z[92]=2*z[91] + z[2];
    z[92]=z[2]*z[92];
    z[93]=n<T>(1,3)*z[7];
    z[94]=n<T>(283,3)*z[75] + 37*z[5];
    z[94]=n<T>(289,18)*z[7] + n<T>(44,3)*z[2] - z[3] - z[4] + n<T>(1,6)*z[94] - 34*
    z[6];
    z[94]=z[94]*z[93];
    z[95]= - z[79] - n<T>(167,18)*z[6];
    z[95]=z[6]*z[95];
    z[81]=z[94] + n<T>(22,9)*z[92] + z[89] + z[86] + z[81] + z[95];
    z[81]=z[7]*z[81];
    z[86]=n<T>(1,3)*z[2];
    z[89]= - n<T>(5,3)*z[76] + n<T>(1,2)*z[6];
    z[89]= - n<T>(31,36)*z[11] + z[86] + n<T>(1,2)*z[89] - z[85];
    z[89]=z[11]*z[89];
    z[92]=n<T>(1,3)*z[5];
    z[94]= - 17*z[75] + n<T>(13,2)*z[5];
    z[94]=z[94]*z[92];
    z[95]=z[5] + n<T>(13,3)*z[6];
    z[95]=2*z[95] - n<T>(19,4)*z[4];
    z[95]=z[95]*z[85];
    z[96]=n<T>(5,3) + z[91];
    z[96]=n<T>(4,3)*z[96] - z[3];
    z[96]=z[3]*z[96];
    z[97]=n<T>(2,3)*z[2];
    z[90]= - z[4] - z[90];
    z[90]=z[90]*z[97];
    z[98]=13 + 7*z[75];
    z[98]= - n<T>(55,36)*z[6] + n<T>(1,9)*z[98] - 3*z[5];
    z[98]=z[6]*z[98];
    z[89]=z[89] + z[90] + z[96] + z[95] + z[94] + z[98];
    z[89]=z[11]*z[89];
    z[90]=z[82] - z[5];
    z[90]=z[90]*z[92];
    z[94]=11*z[6] - z[82] - z[5];
    z[94]=2*z[94] + z[2];
    z[94]=z[94]*z[97];
    z[80]= - n<T>(28,3)*z[75] + z[80];
    z[80]=n<T>(56,3)*z[7] - 44*z[2] + 4*z[80] + z[6];
    z[80]=z[80]*z[93];
    z[93]=145*z[75] - 44*z[5];
    z[93]=n<T>(1,3)*z[93] - n<T>(19,2)*z[6];
    z[93]=z[6]*z[93];
    z[95]=z[75] + z[5];
    z[95]=2*z[95];
    z[96]=z[95] - z[4];
    z[96]=z[4]*z[96];
    z[97]= - z[5] - n<T>(145,2)*z[6];
    z[97]=n<T>(244,9)*z[7] + n<T>(4,3)*z[2] + n<T>(1,3)*z[97] - z[4];
    z[97]=z[8]*z[97];
    z[80]=z[97] + z[80] + z[94] + z[96] + z[90] + z[93];
    z[90]=n<T>(1,3)*z[8];
    z[80]=z[80]*z[90];
    z[93]=z[92] - 22 - n<T>(5,3)*z[75];
    z[93]=z[5]*z[93];
    z[94]=z[95] - z[3];
    z[74]=z[94]*z[74];
    z[94]=11 + n<T>(38,3)*z[75];
    z[78]= - n<T>(37,3)*z[2] - z[78] + n<T>(10,3)*z[4] - 20*z[6] + 2*z[94] + n<T>(19,3)*z[5];
    z[78]=z[78]*z[86];
    z[94]= - n<T>(26,3)*z[75] + 7*z[5];
    z[94]=2*z[94] - 29*z[6];
    z[94]=z[6]*z[94];
    z[95]=23*z[75];
    z[96]= - z[95] - 10*z[5];
    z[96]=n<T>(1,9)*z[96] + z[6];
    z[96]=2*z[96] + n<T>(23,9)*z[4];
    z[96]=z[4]*z[96];
    z[74]=z[78] + z[74] + z[96] + n<T>(2,3)*z[93] + z[94];
    z[74]=z[74]*z[86];
    z[78]=z[75]*z[4];
    z[93]= - z[82] - z[6];
    z[93]=z[6]*z[93];
    z[83]=8*z[78] + 23*z[83] + 28*z[93];
    z[93]=z[2] - z[4];
    z[94]=n<T>(53,6)*z[8];
    z[93]=n<T>(131,6)*z[9] - z[94] + n<T>(115,6)*z[7] - 38*z[3] - n<T>(47,2)*z[6]
     - 
   n<T>(8,3)*z[75] + n<T>(23,2)*z[5] + 4*z[93];
    z[96]=n<T>(1,3)*z[9];
    z[93]=z[93]*z[96];
    z[94]= - z[94] - n<T>(115,3)*z[7] + n<T>(53,3)*z[75] + 56*z[6];
    z[90]=z[94]*z[90];
    z[94]=2*z[76];
    z[97]= - z[94] - z[3];
    z[97]=z[3]*z[97];
    z[98]=n<T>(115,9)*z[75] + 2*z[7];
    z[98]=z[7]*z[98];
    z[83]=z[93] + z[90] + z[98] + n<T>(1,3)*z[83] + 5*z[97];
    z[83]=z[83]*z[96];
    z[90]=z[8] + z[7];
    z[93]= - z[75]*z[90];
    z[93]= - z[28] + z[93];
    z[93]=z[16]*z[93];
    z[90]=z[75] - z[90];
    z[90]=z[24]*z[90];
    z[90]=z[93] + z[90];
    z[79]=z[79] + n<T>(22,9) + n<T>(29,9)*z[6];
    z[93]=z[75]*z[79];
    z[93]=5*z[28] + z[93];
    z[93]=z[15]*z[93];
    z[79]=5*z[75] - z[79];
    z[79]=z[21]*z[79];
    z[79]=z[93] + z[79];
    z[93]=2 + n<T>(1,3)*z[75];
    z[85]= - z[86] + 2*z[93] - z[85];
    z[85]=z[17]*z[85];
    z[78]=z[78] + z[28];
    z[86]= - z[9]*z[75];
    z[86]=z[86] - z[78];
    z[86]=z[14]*z[86];
    z[93]=z[9] - z[88];
    z[93]=z[20]*z[93];
    z[86]=z[40] + z[86] + z[93];
    z[93]= - 32*z[75] + n<T>(23,3)*z[5];
    z[93]=z[5]*z[93];
    z[96]=n<T>(1139,36)*z[6] + n<T>(79,3)*z[5] - n<T>(1,3) + n<T>(83,8)*z[75];
    z[96]=z[6]*z[96];
    z[93]=z[93] + z[96];
    z[93]=z[6]*z[93];
    z[96]=49*z[75] - n<T>(139,3)*z[5];
    z[96]=z[96]*npow(z[5],2);
    z[93]= - z[73] - n<T>(1,3)*z[96] - z[93] + z[63] + z[53];
    z[96]=z[11]*z[75];
    z[78]=z[96] + z[78];
    z[78]=z[13]*z[78];
    z[88]= - z[11] + z[88];
    z[88]=z[26]*z[88];
    z[78]=z[78] + z[88];
    z[88]=z[5] + z[3];
    z[88]=z[75]*z[88];
    z[88]=z[28] + z[88];
    z[88]=z[12]*z[88];
    z[96]=z[3] - z[76];
    z[96]=z[19]*z[96];
    z[88]=z[88] + z[96];
    z[96]=2*z[5];
    z[97]=11*z[75] + z[96];
    z[97]= - n<T>(11,3)*z[2] + n<T>(2,3)*z[97] - 5*z[4];
    z[97]=z[18]*z[97];
    z[97]=z[97] - z[48];
    z[96]=n<T>(43,24)*z[6] - n<T>(115,12)*z[75] - z[96];
    z[96]=z[6]*z[96];
    z[98]=28*z[75] + z[5];
    z[98]=z[5]*z[98];
    z[96]=n<T>(1,9)*z[98] + z[96];
    z[98]= - n<T>(91,108)*z[4] + n<T>(115,72)*z[6] + n<T>(1,4)*z[75] - n<T>(14,27)*z[5];
    z[98]=z[4]*z[98];
    z[96]=n<T>(1,3)*z[96] + z[98];
    z[96]=z[4]*z[96];
    z[95]=z[95] - 20*z[5];
    z[92]=z[95]*z[92];
    z[95]=20*z[76] + 7*z[6];
    z[95]=z[6]*z[95];
    z[92]=z[92] + z[95];
    z[84]=z[4]*z[84];
    z[84]=2*z[92] + z[84];
    z[75]= - 1 + n<T>(4,3)*z[75];
    z[75]=10*z[75] - n<T>(47,3)*z[5];
    z[75]=n<T>(157,9)*z[3] - z[4] + n<T>(1,3)*z[75] + 4*z[6];
    z[75]=z[3]*z[75];
    z[75]=n<T>(1,3)*z[84] + z[75];
    z[75]=z[75]*z[87];
    z[84]= - z[11] - n<T>(5,3) + z[91];
    z[84]=n<T>(11,9)*z[10] + n<T>(29,9)*z[3] + 2*z[84];
    z[84]=z[25]*z[84];
    z[82]= - z[8] + z[82] - z[2];
    z[82]=z[23]*z[82];
    z[82]= - z[82] - z[55] + z[45] + z[41];
    z[87]= - n<T>(68,27)*z[34] - n<T>(76,27)*z[32] - n<T>(889,216)*z[29];
    z[87]=i*z[87];
    z[91]=n<T>(263,18)*z[2] - n<T>(20,3)*z[3] - n<T>(227,72)*z[4] + n<T>(91,2)*z[5]
     + 50
   *z[6];
    z[91]= - n<T>(595,324)*z[10] - n<T>(65,81)*z[9] - n<T>(91,81)*z[8] - n<T>(121,36)*
    z[11] + n<T>(1,9)*z[91] - n<T>(1,2)*z[7];
    z[91]=z[28]*z[91];
    z[76]= - 88*z[2] - 64*z[76] + 277*z[6];
    z[76]=n<T>(88,9)*z[8] + n<T>(8,3)*z[11] + n<T>(1,9)*z[76] + 22*z[7];
    z[76]=z[22]*z[76];
    z[92]= - n<T>(8,3)*z[10] - n<T>(115,18)*z[11] + n<T>(2,3)*z[7] - n<T>(4,3)*z[3]
     - 
    z[94] - n<T>(47,9)*z[6];
    z[92]=z[27]*z[92];
    z[94]=z[3] + z[9];
    z[94]=z[37]*z[94];
    z[95]=z[7] + z[9];
    z[95]=z[38]*z[95];
    z[98]= - z[11] - z[10];
    z[98]=z[39]*z[98];

    r += n<T>(47,18)*z[30] - n<T>(203,162)*z[31] + n<T>(37,27)*z[33] + n<T>(34,27)*
      z[35] + n<T>(832,27)*z[36] + n<T>(10,3)*z[42] - n<T>(14,27)*z[43] + n<T>(29,54)*
      z[44] - n<T>(16,3)*z[46] - n<T>(94,27)*z[47] - n<T>(5,18)*z[49] - n<T>(23,27)*
      z[50] - n<T>(5,9)*z[51] + n<T>(29,18)*z[52] + n<T>(8,27)*z[54] - n<T>(115,27)*
      z[56] - n<T>(233,18)*z[57] - n<T>(137,18)*z[58] - n<T>(334,9)*z[59] + n<T>(2,9)*
      z[60] - 5*z[61] + n<T>(53,54)*z[62] - n<T>(41,18)*z[64] + n<T>(44,9)*z[65]
     +  n<T>(11,18)*z[66] + n<T>(115,72)*z[67] - n<T>(5,6)*z[68] + 3*z[69] - n<T>(113,72)
      *z[70] + n<T>(65,24)*z[71] + z[72] + z[74] + z[75] + z[76] + z[77] + 
      n<T>(19,6)*z[78] + 2*z[79] + z[80] + z[81] - n<T>(4,9)*z[82] + z[83] + n<T>(2,3)*z[84]
     + n<T>(11,9)*z[85]
     + n<T>(8,9)*z[86]
     + z[87]
     + n<T>(22,27)*z[88]
     +  z[89] + n<T>(376,27)*z[90] + z[91] + z[92] - n<T>(1,3)*z[93] + n<T>(46,9)*
      z[94] + n<T>(79,27)*z[95] + z[96] + n<T>(1,9)*z[97] + n<T>(77,18)*z[98];
 
    return r;
}

template std::complex<double> qg_2lha_tf206(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf206(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
