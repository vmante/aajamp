#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1311(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[113];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[9];
    z[8]=e[7];
    z[9]=d[2];
    z[10]=d[8];
    z[11]=d[11];
    z[12]=e[12];
    z[13]=d[12];
    z[14]=d[4];
    z[15]=d[17];
    z[16]=e[5];
    z[17]=e[9];
    z[18]=e[13];
    z[19]=e[6];
    z[20]=e[14];
    z[21]=f[30];
    z[22]=f[31];
    z[23]=f[32];
    z[24]=f[33];
    z[25]=f[35];
    z[26]=f[36];
    z[27]=f[39];
    z[28]=f[43];
    z[29]=f[50];
    z[30]=f[51];
    z[31]=f[52];
    z[32]=f[55];
    z[33]=f[58];
    z[34]=f[68];
    z[35]=c[3];
    z[36]=c[11];
    z[37]=c[15];
    z[38]=c[17];
    z[39]=c[31];
    z[40]=c[32];
    z[41]=c[39];
    z[42]=c[43];
    z[43]=c[81];
    z[44]=f[53];
    z[45]=g[66];
    z[46]=g[67];
    z[47]=g[84];
    z[48]=g[116];
    z[49]=g[117];
    z[50]=g[133];
    z[51]=g[159];
    z[52]=g[160];
    z[53]=g[162];
    z[54]=g[163];
    z[55]=g[165];
    z[56]=g[167];
    z[57]=g[174];
    z[58]=g[175];
    z[59]=g[178];
    z[60]=g[184];
    z[61]=g[185];
    z[62]=g[186];
    z[63]=g[195];
    z[64]=g[209];
    z[65]=149*z[4];
    z[66]= - z[65] + n<T>(161,2)*z[7];
    z[67]=193*z[5];
    z[68]=z[66] + z[67];
    z[69]=z[1]*i;
    z[70]=193*z[69] - z[68] + n<T>(449,2)*z[3];
    z[71]=z[70]*z[9];
    z[72]=n<T>(1,6)*z[71];
    z[73]=z[65]*z[3];
    z[74]=npow(z[3],2);
    z[75]=z[73] + n<T>(449,2)*z[74];
    z[76]=298*z[4];
    z[77]=z[76] + n<T>(1381,2)*z[3];
    z[78]=161*z[7];
    z[79]= - z[78] + z[77];
    z[80]=n<T>(1,3)*z[7];
    z[79]=z[79]*z[80];
    z[81]=n<T>(1,3)*i;
    z[82]= - n<T>(1607,2)*z[3] - z[65];
    z[82]=z[82]*z[81];
    z[83]=z[7]*i;
    z[82]=z[82] + n<T>(311,2)*z[83];
    z[82]=z[1]*z[82];
    z[84]=n<T>(2,3)*z[7];
    z[85]=n<T>(1,3)*z[69];
    z[86]=z[85] + z[3] - z[84];
    z[86]=z[86]*z[67];
    z[79]=z[72] + z[86] + z[82] + z[79] - z[75];
    z[79]=z[9]*z[79];
    z[82]= - z[66] + n<T>(1381,4)*z[3];
    z[82]=z[82]*z[80];
    z[75]=z[82] - n<T>(1,2)*z[75];
    z[82]=n<T>(709,2)*z[3] - z[65];
    z[82]=i*z[82];
    z[82]=z[82] - n<T>(611,2)*z[83];
    z[82]=z[1]*z[82];
    z[86]=n<T>(1,2)*z[3];
    z[87]=z[86] - z[80];
    z[88]=n<T>(1,6)*z[69];
    z[89]=z[88] - z[87];
    z[89]=z[89]*z[67];
    z[90]=z[10]*z[70];
    z[72]= - n<T>(1,3)*z[90] + z[72] + z[89] + n<T>(1,6)*z[82] - z[75];
    z[72]=z[10]*z[72];
    z[82]=321*z[74] + z[73];
    z[82]=i*z[82];
    z[89]=161*z[83];
    z[77]= - i*z[77];
    z[77]=z[77] + z[89];
    z[77]=z[77]*z[80];
    z[77]=z[82] + z[77];
    z[77]=z[1]*z[77];
    z[66]=z[66]*z[74];
    z[82]=npow(z[3],3);
    z[66]=z[66] - n<T>(449,2)*z[82];
    z[66]=n<T>(1,2)*z[66];
    z[90]= - i*z[3];
    z[90]=z[90] + n<T>(2,3)*z[83];
    z[90]=z[1]*z[90];
    z[91]=n<T>(1,2)*z[74];
    z[90]= - z[91] + z[90];
    z[90]=z[90]*z[67];
    z[72]=z[72] + z[79] + z[90] - z[66] + z[77];
    z[72]=z[10]*z[72];
    z[77]=n<T>(649,2)*z[4] + 391*z[3];
    z[79]=n<T>(1,9)*z[4];
    z[77]=z[77]*z[79];
    z[77]=z[77] - n<T>(159,2)*z[74];
    z[90]=z[4] - z[3];
    z[92]=n<T>(631,18)*z[90];
    z[93]=z[92]*z[5];
    z[94]=z[90]*z[7];
    z[93]=z[93] + n<T>(1360,9)*z[94];
    z[95]=z[90]*z[69];
    z[95]= - n<T>(631,9)*z[95] + z[93] + z[77];
    z[95]=z[5]*z[95];
    z[96]= - z[69]*z[92];
    z[97]=n<T>(157,2)*z[3] + n<T>(640,9)*z[4];
    z[97]=z[4]*z[97];
    z[96]=z[96] - n<T>(2693,18)*z[74] + z[97] + z[93];
    z[96]=z[6]*z[96];
    z[97]=z[69] - z[5];
    z[98]=n<T>(631,36)*z[97];
    z[98]=z[90]*z[98];
    z[99]= - n<T>(157,4)*z[3] - n<T>(320,9)*z[4];
    z[99]=z[4]*z[99];
    z[94]= - n<T>(680,9)*z[94] + n<T>(2693,36)*z[74] + z[99] + z[98];
    z[94]=z[2]*z[94];
    z[98]=640*z[4];
    z[99]= - n<T>(3973,2)*z[3] - z[98];
    z[99]=z[4]*z[99];
    z[99]= - n<T>(133,2)*z[74] + z[99];
    z[99]=z[4]*z[99];
    z[100]= - z[3] - z[4];
    z[100]=z[4]*z[100];
    z[100]=2*z[74] + z[100];
    z[101]=1360*z[7];
    z[100]=z[100]*z[101];
    z[99]=z[100] + 2693*z[82] + z[99];
    z[77]= - i*z[77];
    z[90]=n<T>(1360,9)*z[90];
    z[90]= - z[83]*z[90];
    z[77]=z[77] + z[90];
    z[77]=z[1]*z[77];
    z[90]= - z[35]*z[92];
    z[77]=z[94] + z[90] + z[96] + z[95] + n<T>(1,9)*z[99] + z[77];
    z[77]=z[2]*z[77];
    z[90]= - 9547*z[3] + n<T>(7345,2)*z[4];
    z[90]=z[4]*z[90];
    z[90]= - n<T>(49085,4)*z[74] + z[90];
    z[92]=n<T>(1,4)*i;
    z[90]=z[90]*z[92];
    z[94]= - 7289*z[3] + 3046*z[4];
    z[94]=z[94]*z[83];
    z[90]=z[90] + z[94];
    z[94]=n<T>(1,3)*z[1];
    z[90]=z[90]*z[94];
    z[95]=877*z[4];
    z[96]=z[95] + n<T>(835,4)*z[3];
    z[99]=n<T>(1,3)*z[4];
    z[100]= - z[96]*z[99];
    z[100]=n<T>(877,2)*z[74] + z[100];
    z[100]=z[4]*z[100];
    z[100]=n<T>(835,8)*z[82] + z[100];
    z[102]=npow(z[4],2);
    z[102]=z[91] - n<T>(1,3)*z[102];
    z[103]=1523*z[7];
    z[102]=z[102]*z[103];
    z[90]=z[90] + n<T>(1,2)*z[100] + z[102];
    z[100]= - 157*z[3] - n<T>(5449,36)*z[4];
    z[100]=z[4]*z[100];
    z[100]=n<T>(22531,72)*z[74] + z[100];
    z[102]=n<T>(1,4)*z[69];
    z[104]= - n<T>(951,2)*z[3] + n<T>(1591,9)*z[4];
    z[104]=z[104]*z[102];
    z[93]=z[104] + n<T>(1,2)*z[100] - z[93];
    z[93]=z[5]*z[93];
    z[100]=n<T>(1,4)*z[5] - z[102];
    z[102]= - 1591*z[4] + n<T>(6035,4)*z[3];
    z[100]=z[102]*z[100];
    z[102]= - n<T>(515,2)*z[3] - 1517*z[4];
    z[102]=z[4]*z[102];
    z[104]=n<T>(7289,2)*z[3] - 4406*z[4];
    z[104]=z[7]*z[104];
    z[100]=z[104] + n<T>(24049,16)*z[74] + z[102] + z[100];
    z[102]=n<T>(1,9)*z[6];
    z[100]=z[100]*z[102];
    z[90]=z[100] + n<T>(1,3)*z[90] + z[93];
    z[90]=z[6]*z[90];
    z[93]=n<T>(329,8)*z[5];
    z[100]=n<T>(1,2)*z[96] - n<T>(329,8)*z[69] + z[93] + z[103];
    z[104]=n<T>(1,6)*z[6];
    z[105]=z[100]*z[104];
    z[106]= - z[95] + n<T>(4427,4)*z[3];
    z[107]=z[106]*z[99];
    z[107]=z[107] + n<T>(835,8)*z[74];
    z[108]= - z[3] + n<T>(2,3)*z[4];
    z[109]=z[108]*z[103];
    z[110]=19*z[3] - n<T>(2083,4)*z[4];
    z[110]=i*z[110];
    z[110]=z[110] - 1523*z[83];
    z[110]=z[110]*z[94];
    z[86]=z[86] - z[99];
    z[111]= - z[88] - z[86];
    z[112]=n<T>(329,4)*z[5];
    z[111]=z[111]*z[112];
    z[105]=z[105] + z[111] + z[110] + z[109] - z[107];
    z[109]=n<T>(1,3)*z[6];
    z[105]=z[105]*z[109];
    z[110]=n<T>(1,3)*z[14];
    z[104]= - z[110] + z[104];
    z[104]=z[100]*z[104];
    z[111]= - n<T>(911,3)*z[3] - 475*z[4];
    z[92]=z[111]*z[92];
    z[111]=n<T>(1523,3)*z[83];
    z[92]=z[92] - z[111];
    z[92]=z[1]*z[92];
    z[92]=z[107] + z[92];
    z[107]=z[86]*z[103];
    z[88]= - z[88] + z[86];
    z[88]=z[88]*z[93];
    z[88]=z[88] + z[107] + z[104] + n<T>(1,2)*z[92];
    z[88]=z[88]*z[110];
    z[92]=z[74]*z[95];
    z[92]=n<T>(835,4)*z[82] + z[92];
    z[93]=z[74]*z[103];
    z[92]=n<T>(1,2)*z[92] + z[93];
    z[79]=z[106]*z[79];
    z[79]=n<T>(447,16)*z[74] + z[79];
    z[79]=i*z[79];
    z[93]= - z[108]*z[111];
    z[79]=z[79] + z[93];
    z[79]=z[1]*z[79];
    z[86]=z[86]*z[69];
    z[86]=n<T>(1,4)*z[74] + z[86];
    z[86]=z[5]*z[86];
    z[79]=z[88] + z[105] + n<T>(329,12)*z[86] + n<T>(1,6)*z[92] + z[79];
    z[79]=z[14]*z[79];
    z[86]= - n<T>(1363,18)*z[3] - 239*z[4];
    z[86]=z[4]*z[86];
    z[86]=n<T>(1577,36)*z[74] + z[86];
    z[88]=3458*z[4];
    z[92]= - 271*z[3] - z[88];
    z[92]=n<T>(1,3)*z[92] - 92*z[7];
    z[92]=z[92]*z[80];
    z[93]= - 107*z[3] - n<T>(309,4)*z[4];
    z[93]=i*z[93];
    z[93]=z[93] - n<T>(1228,3)*z[83];
    z[93]=z[1]*z[93];
    z[95]=n<T>(1,3)*z[5];
    z[103]= - n<T>(2267,16)*z[5] + n<T>(689,2)*z[69] + n<T>(2588,3)*z[7] + 422*z[3]
    + n<T>(4921,12)*z[4];
    z[103]=z[103]*z[95];
    z[86]=z[103] + z[93] + n<T>(1,2)*z[86] + z[92];
    z[86]=z[5]*z[86];
    z[92]=n<T>(127,9)*z[3] + n<T>(55,4)*z[4];
    z[92]=z[4]*z[92];
    z[92]=n<T>(5837,8)*z[74] + 29*z[92];
    z[93]=n<T>(1,2)*i;
    z[92]=z[92]*z[93];
    z[88]=2311*z[3] + z[88];
    z[88]=z[88]*z[81];
    z[88]=z[88] + 92*z[83];
    z[84]=z[88]*z[84];
    z[84]=z[92] + z[84];
    z[84]=z[1]*z[84];
    z[88]=n<T>(5513,4)*z[3] - n<T>(10241,3)*z[4];
    z[88]=z[88]*z[99];
    z[88]=n<T>(1895,4)*z[74] + z[88];
    z[88]=z[4]*z[88];
    z[88]= - n<T>(46819,36)*z[82] + z[88];
    z[92]=550*z[3] - 47*z[4];
    z[92]=z[4]*z[92];
    z[92]= - 3014*z[74] + 7*z[92];
    z[99]= - 135*z[4] + 169*z[3];
    z[103]= - n<T>(835,9)*z[7] - z[99];
    z[103]=z[7]*z[103];
    z[92]=n<T>(1,9)*z[92] + z[103];
    z[92]=z[7]*z[92];
    z[84]=z[86] + z[84] + n<T>(1,6)*z[88] + z[92];
    z[84]=z[5]*z[84];
    z[86]=514*z[3] + z[65];
    z[86]=i*z[86];
    z[86]=z[86] - n<T>(547,2)*z[83];
    z[86]=z[86]*z[94];
    z[85]= - z[85] - z[87];
    z[85]=z[85]*z[67];
    z[71]= - n<T>(1,3)*z[71] + z[85] + z[86] - z[75];
    z[71]=z[9]*z[71];
    z[73]= - 128*z[74] - z[73];
    z[73]=i*z[73];
    z[75]=z[83]*z[3];
    z[73]=z[73] + n<T>(161,2)*z[75];
    z[73]=z[1]*z[73];
    z[85]=z[3]*z[69];
    z[85]= - z[91] + z[85];
    z[67]=z[85]*z[67];
    z[66]=z[71] + z[67] - z[66] + z[73];
    z[66]=z[9]*z[66];
    z[67]=n<T>(1,9)*z[2] - z[102];
    z[71]=1280*z[4] + 2693*z[3] + 2720*z[7] - 631*z[97];
    z[67]=z[71]*z[67];
    z[71]= - n<T>(20327,4)*z[3] - n<T>(6305,3)*z[4];
    z[71]=z[4]*z[71];
    z[71]= - n<T>(7540,3)*z[74] + z[71];
    z[73]=n<T>(2429,4)*z[4] + n<T>(4552,3)*z[3];
    z[85]=i*z[73];
    z[85]=z[85] + n<T>(3638,3)*z[83];
    z[85]=z[85]*z[94];
    z[73]= - n<T>(784,3)*z[5] + n<T>(1568,3)*z[69] - n<T>(3638,3)*z[7] - z[73];
    z[73]=z[73]*z[95];
    z[86]= - 6845*z[3] - 5179*z[4];
    z[86]=n<T>(2,9)*z[86] + 17*z[7];
    z[86]=z[7]*z[86];
    z[67]=123*z[8] + n<T>(2581,36)*z[35] + z[73] + z[85] + n<T>(1,3)*z[71] + 
    z[86] + z[67];
    z[67]=z[8]*z[67];
    z[71]=n<T>(1997,6)*z[4] + 315*z[3];
    z[73]=z[71] + n<T>(1360,3)*z[7];
    z[85]=n<T>(889,12)*z[5];
    z[86]= - z[85] + n<T>(86,3)*z[69] + z[73];
    z[86]=z[5]*z[86];
    z[71]=z[71]*i;
    z[71]=z[71] + n<T>(1360,3)*z[83];
    z[71]=z[71]*z[1];
    z[87]=z[4]*z[3];
    z[87]=563*z[87] + n<T>(4669,2)*z[74];
    z[88]=z[3]*z[101];
    z[88]=n<T>(1,2)*z[87] + z[88];
    z[91]=z[13]*z[69];
    z[86]= - n<T>(239,4)*z[16] + n<T>(239,2)*z[91] + n<T>(325,6)*z[35] + z[86] + n<T>(1,3)*z[88]
     - z[71];
    z[86]=z[16]*z[86];
    z[88]=4141*z[3] - n<T>(4241,3)*z[4];
    z[88]=z[4]*z[88];
    z[88]=2888*z[74] + z[88];
    z[88]=z[4]*z[88];
    z[88]= - n<T>(11095,3)*z[82] + z[88];
    z[91]= - 125*z[3] - 131*z[4];
    z[91]=z[4]*z[91];
    z[91]=275*z[74] + z[91];
    z[92]= - 69*z[3] - n<T>(209,9)*z[4];
    z[92]=2*z[92] + n<T>(22705,144)*z[7];
    z[92]=z[7]*z[92];
    z[91]=n<T>(1,3)*z[91] + z[92];
    z[91]=z[7]*z[91];
    z[88]=n<T>(1,9)*z[88] + z[91];
    z[88]=z[7]*z[88];
    z[85]=z[69]*z[85];
    z[71]= - z[71] + z[85];
    z[71]=z[5]*z[71];
    z[85]= - z[87]*z[93];
    z[75]=z[85] - 1360*z[75];
    z[75]=z[75]*z[94];
    z[73]=n<T>(239,4)*z[13] + n<T>(86,3)*z[5] - z[73];
    z[73]=z[35]*z[73];
    z[71]=z[75] + z[71] + z[73];
    z[71]=z[13]*z[71];
    z[73]=4*z[3] - n<T>(71,16)*z[4];
    z[73]=z[4]*z[73];
    z[73]=n<T>(15601,8)*z[74] + n<T>(721,9)*z[73];
    z[73]=z[4]*z[73];
    z[73]= - n<T>(49355,72)*z[82] + z[73];
    z[73]=z[4]*z[73];
    z[75]=npow(z[3],4);
    z[73]= - n<T>(235111,144)*z[75] + z[73];
    z[75]= - n<T>(571,16)*z[3] + n<T>(187,3)*z[4];
    z[75]= - n<T>(15881,16)*z[5] + 7*z[75];
    z[75]=i*z[75];
    z[75]=n<T>(6733,4)*z[83] + z[75];
    z[85]= - n<T>(325,2)*z[13] + 386*z[11] + n<T>(329,36)*z[14] - n<T>(386,3)*z[10]
    + n<T>(193,2)*z[9] - n<T>(317,16)*z[6];
    z[85]=i*z[85];
    z[75]=n<T>(1,3)*z[75] + z[85];
    z[85]=z[15]*i;
    z[75]=n<T>(1,3)*z[75] - n<T>(317,4)*z[85];
    z[75]=z[36]*z[75];
    z[85]=358*z[7] + 211*z[3] + 178*z[4] - 49*z[97];
    z[85]=z[23]*z[85];
    z[87]=n<T>(125,24)*z[5] - n<T>(401,3)*z[7] - n<T>(903,8)*z[3] - n<T>(377,3)*z[4];
    z[87]=z[37]*z[87];
    z[91]= - 7178*z[7] + n<T>(49645,16)*z[3] - 3734*z[4];
    z[91]=n<T>(1,9)*z[91] - n<T>(11809,16)*z[5];
    z[91]=z[39]*z[91];
    z[66]=z[79] + z[77] + z[67] + z[71] + z[90] + z[84] + n<T>(1,3)*z[73] + 
    z[88] + z[66] + z[72] + z[91] + z[86] + z[75] + z[85] + z[87];
    z[67]=z[76] + 449*z[3];
    z[71]=z[78] - z[67];
    z[72]=386*z[5];
    z[73]= - z[72] - z[71] + 386*z[69];
    z[75]= - z[9]*z[73];
    z[71]=z[7]*z[71];
    z[76]=z[67]*i;
    z[77]=z[76] - 547*z[83];
    z[77]=z[1]*z[77];
    z[78]=z[7] - z[69];
    z[78]=z[78]*z[72];
    z[71]= - 386*z[35] + z[75] + z[78] + z[71] + z[77];
    z[71]=z[12]*z[71];
    z[75]=z[89] - z[76];
    z[75]=z[75]*z[1];
    z[76]=z[72]*z[69];
    z[75]=z[75] + z[76];
    z[76]= - z[9] - z[7];
    z[75]=z[75]*z[76];
    z[67]= - 386*z[9] - z[72] - 547*z[7] + z[67];
    z[67]=z[35]*z[67];
    z[67]=z[67] + z[75];
    z[67]=z[11]*z[67];
    z[75]=286*z[7] + 355*z[3] + 106*z[4] + 190*z[97];
    z[75]=z[31]*z[75];
    z[67]=z[75] + z[67] + z[71];
    z[71]=n<T>(1,186)*z[27] - n<T>(1,558)*z[26];
    z[71]=z[100]*z[71];
    z[68]=n<T>(2765,2)*z[3] - z[68];
    z[75]=193*z[9];
    z[68]=n<T>(1,2)*z[68] - z[75];
    z[68]=z[9]*z[68];
    z[65]= - 1093*z[3] - z[65];
    z[65]=z[72] + 2*z[65] + 1319*z[7];
    z[65]=n<T>(193,2)*z[10] + n<T>(1,3)*z[65] + z[75];
    z[65]=z[10]*z[65];
    z[65]=z[68] + z[65];
    z[68]= - 20759*z[3] + n<T>(60083,8)*z[4];
    z[68]=z[4]*z[68];
    z[68]= - n<T>(5765,4)*z[74] + n<T>(1,27)*z[68];
    z[72]= - 3353*z[3] + n<T>(11638,3)*z[4];
    z[72]=n<T>(1,3)*z[72] + n<T>(4261,32)*z[7];
    z[72]=z[72]*z[80];
    z[75]=278*z[3] - n<T>(1393,12)*z[4];
    z[75]=n<T>(9005,48)*z[5] + n<T>(1,9)*z[75] - n<T>(653,2)*z[7];
    z[75]=z[5]*z[75];
    z[68]=z[75] + n<T>(1,4)*z[68] + z[72];
    z[72]= - n<T>(8201,16)*z[3] + 262*z[4];
    z[72]=n<T>(1,31)*z[72] + n<T>(31,2)*z[7];
    z[72]=n<T>(1,3)*z[72] + n<T>(317,496)*z[5];
    z[72]=z[72]*z[109];
    z[75]=n<T>(1291,62)*z[3] - 145*z[4];
    z[75]= - n<T>(329,124)*z[5] + n<T>(1,4)*z[75] - n<T>(3046,31)*z[7];
    z[75]= - n<T>(329,496)*z[14] + n<T>(1,3)*z[75] - n<T>(329,248)*z[6];
    z[75]=z[14]*z[75];
    z[65]=n<T>(1,9)*z[75] + n<T>(1,31)*z[68] + z[72] + n<T>(1,93)*z[65];
    z[65]=z[35]*z[65];
    z[68]= - n<T>(598,3)*z[3] + 25*z[4];
    z[68]=z[4]*z[68];
    z[72]= - 253*z[3] - 394*z[4];
    z[72]=n<T>(1,3)*z[72] + 119*z[7];
    z[72]=z[7]*z[72];
    z[68]=z[72] - 663*z[74] + z[68];
    z[72]= - z[4] + n<T>(431,93)*z[3];
    z[75]= - i*z[72];
    z[75]=z[75] - n<T>(431,93)*z[83];
    z[75]=z[1]*z[75];
    z[72]= - z[69] + n<T>(431,93)*z[7] + z[72];
    z[72]=2*z[72] + z[5];
    z[72]=z[5]*z[72];
    z[68]= - n<T>(105,31)*z[18] - n<T>(389,372)*z[35] + z[72] + n<T>(1,31)*z[68]
     + 2
   *z[75];
    z[68]=z[18]*z[68];
    z[72]=62*z[7];
    z[75]=719*z[4] + n<T>(3869,4)*z[3];
    z[75]=n<T>(1,31)*z[75];
    z[76]=z[72] + z[75];
    z[77]=z[4]*z[76];
    z[78]=62*z[83];
    z[79]=3827*z[4] + 3869*z[3];
    z[79]=n<T>(1,124)*z[79];
    z[80]= - i*z[79];
    z[80]=z[80] - z[78];
    z[80]=z[1]*z[80];
    z[77]=z[80] + z[77];
    z[80]=n<T>(317,124)*z[5];
    z[76]=z[80] + n<T>(1,3)*z[76] - n<T>(317,124)*z[69];
    z[76]=z[6]*z[76];
    z[84]=z[4] - z[69];
    z[84]=z[84]*z[80];
    z[76]= - n<T>(317,124)*z[35] + z[76] + n<T>(1,3)*z[77] + z[84];
    z[76]=z[17]*z[76];
    z[77]= - z[10] - z[7];
    z[77]=z[20]*z[77];
    z[77]=z[77] + z[29];
    z[77]=n<T>(1,93)*z[77];
    z[73]=z[73]*z[77];
    z[77]= - n<T>(329,4)*z[69] + 3046*z[7] + z[112] + z[96];
    z[84]= - z[14] - z[4];
    z[84]=z[19]*z[84];
    z[84]=z[84] + z[21];
    z[84]=n<T>(1,279)*z[84];
    z[77]=z[77]*z[84];
    z[84]=n<T>(2693,2)*z[3] + z[98] + z[101] - n<T>(631,2)*z[97];
    z[85]=z[28] - z[25];
    z[85]= - n<T>(1,279)*z[85];
    z[84]=z[84]*z[85];
    z[85]= - 415*z[3] - n<T>(1277,3)*z[4];
    z[85]=z[4]*z[85];
    z[85]= - n<T>(2171,4)*z[74] + 2*z[85];
    z[85]=z[85]*z[81];
    z[86]=i*z[99];
    z[83]=z[86] + n<T>(835,9)*z[83];
    z[83]=z[7]*z[83];
    z[83]=z[85] + z[83];
    z[83]=z[7]*z[83];
    z[85]= - 100*z[3] + n<T>(377,9)*z[4];
    z[85]=z[4]*z[85];
    z[74]= - n<T>(509,24)*z[74] + n<T>(5,31)*z[85];
    z[74]=z[4]*z[74];
    z[74]= - n<T>(91633,4464)*z[82] + z[74];
    z[74]=z[74]*z[81];
    z[74]=z[74] + n<T>(1,31)*z[83];
    z[74]=z[1]*z[74];
    z[75]=z[75]*i;
    z[75]=z[75] + z[78];
    z[75]=z[75]*z[94];
    z[78]=z[80]*z[69];
    z[75]=z[75] + z[78];
    z[78]=z[6] + z[4];
    z[75]=z[75]*z[78];
    z[72]=z[79] + z[72];
    z[72]=n<T>(317,124)*z[6] + n<T>(1,3)*z[72] + z[80];
    z[72]=z[35]*z[72];
    z[72]=z[72] + z[75];
    z[72]=z[15]*z[72];
    z[75]=z[33] + z[30];
    z[75]= - n<T>(1,186)*z[32] + n<T>(1,62)*z[75];
    z[70]=z[70]*z[75];
    z[75]=n<T>(45593,4)*z[3] + 7751*z[4];
    z[75]=n<T>(1,2)*z[75] + 10009*z[7] - n<T>(11083,8)*z[97];
    z[75]=z[22]*z[75];
    z[78]=z[5] - z[3];
    z[69]=z[69] - z[78];
    z[69]=z[34]*z[69];
    z[79]=z[60] - z[56];
    z[78]= - z[38]*z[78];
    z[78]=n<T>(239,124)*z[78] - n<T>(170,31)*z[43] - n<T>(125,744)*z[41];
    z[78]=i*z[78];
    z[80]=12104*z[7] + n<T>(22387,2)*z[3] + 9446*z[4] - n<T>(7793,2)*z[97];
    z[80]=z[24]*z[80];
    z[81]= - n<T>(935,3)*z[7] + n<T>(635,3)*z[3] + 12*z[4];
    z[81]=z[44]*z[81];

    r += n<T>(6533,29760)*z[40] - n<T>(239,124)*z[42] + n<T>(95,248)*z[45] + n<T>(1442,93)*z[46]
     + n<T>(50,93)*z[47]
     + n<T>(685,248)*z[48]
     + n<T>(314,93)*z[49]
     + n<T>(10,93)*z[50]
     + n<T>(4321,744)*z[51] - n<T>(221,248)*z[52] - n<T>(421,744)*z[53]
       - n<T>(335,744)*z[54] + n<T>(707,372)*z[55] + n<T>(953,744)*z[57] + n<T>(195,124)
      *z[58] + n<T>(115,744)*z[59] + n<T>(1,4)*z[61] - n<T>(190,93)*z[62] + n<T>(12,31)
      *z[63] - n<T>(56,93)*z[64] + z[65] + n<T>(1,31)*z[66] + n<T>(1,93)*z[67] + 
      z[68] + n<T>(239,186)*z[69] + z[70] + z[71] + z[72] + z[73] + z[74]
       + n<T>(1,558)*z[75] + z[76] + z[77] + z[78] + n<T>(895,744)*z[79] + n<T>(1,279)*z[80]
     + n<T>(2,31)*z[81]
     + z[84];
 
    return r;
}

template std::complex<double> qg_2lha_tf1311(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1311(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
