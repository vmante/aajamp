#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1206(
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[9];
  std::complex<T> r(0,0);

    z[1]=d[0];
    z[2]=d[1];
    z[3]=d[2];
    z[4]=d[4];
    z[5]=d[7];
    z[6]=e[8];
    z[7]=z[5] + z[2] - z[4];
    z[8]= - z[3]*z[7];
    z[7]= - z[1] + z[3] + z[7];
    z[7]=z[1]*z[7];

    r +=  - 2*z[6] + z[7] + z[8];
 
    return r;
}

template std::complex<double> qg_2lha_tf1206(
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1206(
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
