#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf321(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[105];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[9];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[14];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[19];
    z[34]=c[21];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[31];
    z[40]=e[3];
    z[41]=e[6];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[7];
    z[50]=f[10];
    z[51]=f[11];
    z[52]=f[12];
    z[53]=f[13];
    z[54]=f[14];
    z[55]=f[16];
    z[56]=f[17];
    z[57]=f[18];
    z[58]=f[21];
    z[59]=f[23];
    z[60]=f[30];
    z[61]=f[31];
    z[62]=f[32];
    z[63]=f[33];
    z[64]=f[34];
    z[65]=f[36];
    z[66]=f[39];
    z[67]=f[43];
    z[68]=f[50];
    z[69]=f[51];
    z[70]=f[52];
    z[71]=f[54];
    z[72]=f[55];
    z[73]=f[56];
    z[74]=f[57];
    z[75]=f[58];
    z[76]=n<T>(1,2)*z[6];
    z[77]=n<T>(1,2)*z[5];
    z[78]=z[1]*i;
    z[79]= - n<T>(11,6)*z[9] - z[76] - n<T>(8,3)*z[78] + z[77];
    z[79]=z[9]*z[79];
    z[80]=n<T>(1,2)*z[8];
    z[81]=z[80] - z[78];
    z[82]= - z[81]*z[80];
    z[83]=7*z[6];
    z[84]= - 7*z[78] + 8*z[5];
    z[84]=2*z[84] - z[83];
    z[85]=n<T>(1,3)*z[6];
    z[84]=z[84]*z[85];
    z[86]=n<T>(1,2)*z[3];
    z[87]= - z[9] + z[6] - z[5] + z[8];
    z[87]=z[87]*z[86];
    z[88]=7*z[5];
    z[89]= - n<T>(25,4)*z[78] + z[88];
    z[90]=n<T>(7,3)*z[6];
    z[89]=z[14] - n<T>(3,4)*z[9] - z[90] + n<T>(1,3)*z[89] - n<T>(1,4)*z[8];
    z[89]=z[14]*z[89];
    z[91]=2*z[78];
    z[92]=z[91] - z[5];
    z[93]=z[5]*z[92];
    z[94]= - 2*z[6] + n<T>(13,2)*z[9];
    z[94]=n<T>(1,3)*z[94] - n<T>(3,4)*z[4];
    z[94]=z[4]*z[94];
    z[79]=z[89] + z[94] + z[87] + z[79] + z[84] + n<T>(8,3)*z[93] + z[82];
    z[79]=z[14]*z[79];
    z[82]= - n<T>(7,6)*z[7] - z[2] + n<T>(5,2)*z[6] - z[80] - z[78] - z[77];
    z[82]=z[7]*z[82];
    z[84]=z[77] - z[78];
    z[87]= - z[84]*z[77];
    z[89]=z[78] - z[8];
    z[93]=z[5] - z[89];
    z[94]=n<T>(1,2)*z[2];
    z[95]= - z[94] - z[6] + z[93];
    z[95]=z[95]*z[94];
    z[96]=n<T>(1,2)*z[10];
    z[97]= - z[96] - z[89];
    z[96]=z[97]*z[96];
    z[97]=z[78] - z[5];
    z[98]=n<T>(1,2)*z[97] - z[8];
    z[98]=z[8]*z[98];
    z[99]=z[77] + z[6];
    z[99]=z[6]*z[99];
    z[82]=n<T>(1,2)*z[82] + z[96] + z[95] + z[99] + z[87] + z[98];
    z[82]=z[7]*z[82];
    z[87]=z[93] - z[2];
    z[95]= - n<T>(5,2)*z[7] - n<T>(7,2)*z[6] - z[87];
    z[95]=z[23]*z[95];
    z[82]=z[82] + z[95];
    z[95]=n<T>(1,3)*z[8];
    z[96]=2*z[9] - z[76] + n<T>(13,4)*z[97] - z[95];
    z[96]=z[9]*z[96];
    z[98]= - n<T>(5,4)*z[8] - z[5] + n<T>(5,2)*z[78];
    z[98]=z[98]*z[95];
    z[99]= - z[76] - z[97];
    z[99]=z[6]*z[99];
    z[100]=n<T>(5,3)*z[5];
    z[101]= - n<T>(49,9)*z[3] - n<T>(1,12)*z[2] + n<T>(13,4)*z[9] - n<T>(1,6)*z[8]
     - n<T>(3,4)*z[78]
     + z[100];
    z[101]=z[101]*z[86];
    z[102]=n<T>(1,4)*z[78];
    z[103]=z[102] + n<T>(2,3)*z[5];
    z[103]=z[5]*z[103];
    z[104]= - 19*z[78] - 11*z[5];
    z[104]=n<T>(13,4)*z[2] + z[9] + n<T>(1,4)*z[104] + z[8];
    z[104]=z[2]*z[104];
    z[96]=z[101] + n<T>(1,3)*z[104] + z[96] + 5*z[99] + z[103] + z[98];
    z[96]=z[3]*z[96];
    z[84]=z[84]*z[5];
    z[91]=z[80] + z[91] + z[5];
    z[91]=z[91]*z[95];
    z[98]=n<T>(7,2)*z[78];
    z[99]=z[98] + z[5];
    z[101]=n<T>(7,4)*z[6] + z[99];
    z[101]=z[101]*z[76];
    z[88]=n<T>(19,3)*z[78] - z[88];
    z[88]=n<T>(23,36)*z[9] + n<T>(11,8)*z[6] + n<T>(1,16)*z[88] - z[95];
    z[88]=z[9]*z[88];
    z[88]=z[88] + z[101] + n<T>(1,8)*z[84] + z[91];
    z[88]=z[9]*z[88];
    z[77]=z[77] - z[81];
    z[77]=z[8]*z[77];
    z[91]=z[98] - z[5];
    z[95]=z[5]*z[91];
    z[77]=n<T>(1,3)*z[95] + z[77];
    z[95]=n<T>(49,2)*z[8];
    z[98]=n<T>(181,4)*z[6] - z[95] + n<T>(19,2)*z[78] - 17*z[5];
    z[85]=z[98]*z[85];
    z[98]=n<T>(1,3)*z[9];
    z[93]=z[9] - z[93];
    z[93]=z[93]*z[98];
    z[101]=n<T>(1,6)*z[9];
    z[102]= - z[102] - z[5];
    z[102]=n<T>(1,3)*z[102] - n<T>(1,8)*z[8];
    z[102]=n<T>(25,24)*z[2] + z[101] + 5*z[102] + n<T>(16,3)*z[6];
    z[102]=z[2]*z[102];
    z[77]=z[102] + z[93] + n<T>(5,2)*z[77] + z[85];
    z[77]=z[2]*z[77];
    z[85]= - z[5] + z[81];
    z[85]=z[8]*z[85];
    z[85]=n<T>(5,3)*z[84] + 3*z[85];
    z[93]= - z[78] + z[76];
    z[90]=z[93]*z[90];
    z[85]=n<T>(1,2)*z[85] + z[90];
    z[90]=z[101] - n<T>(1,3)*z[78] - 9*z[6];
    z[90]=z[9]*z[90];
    z[85]=5*z[85] + z[90];
    z[90]=z[2] - z[6];
    z[90]= - n<T>(23,3)*z[4] - 5*z[3] + z[98] + n<T>(15,2)*z[8] + 21*z[78] + n<T>(25,6)*z[5]
     - n<T>(35,3)*z[90];
    z[90]=z[4]*z[90];
    z[93]= - z[94] + z[99];
    z[93]=z[2]*z[93];
    z[98]=z[78]*z[3];
    z[85]=n<T>(1,4)*z[90] + n<T>(5,2)*z[98] + n<T>(1,2)*z[85] + n<T>(5,3)*z[93];
    z[90]=n<T>(1,2)*z[4];
    z[85]=z[85]*z[90];
    z[93]=z[8]*z[81];
    z[93]= - n<T>(5,2)*z[84] + 7*z[93];
    z[99]=z[76] + z[89];
    z[99]=z[99]*z[83];
    z[93]=n<T>(1,3)*z[93] + z[99];
    z[99]=n<T>(25,2)*z[9];
    z[101]= - n<T>(1,2)*z[9] - z[97];
    z[101]=z[101]*z[99];
    z[93]=7*z[93] + z[101];
    z[100]= - 3*z[78] - z[100];
    z[83]=z[83] + n<T>(1,2)*z[100] + n<T>(7,3)*z[8];
    z[100]=z[4] - z[2];
    z[83]=n<T>(55,3)*z[3] + 7*z[83] - z[99] - 5*z[100];
    z[83]=n<T>(1,8)*z[83] - n<T>(11,3)*z[10];
    z[83]=z[10]*z[83];
    z[86]=z[86] + z[97];
    z[86]=z[3]*z[86];
    z[99]=z[78]*z[4];
    z[83]=z[83] + n<T>(5,4)*z[99] + n<T>(1,4)*z[93] + n<T>(5,3)*z[86];
    z[83]=z[10]*z[83];
    z[86]=n<T>(37,3)*z[6] + n<T>(55,2) + n<T>(97,3)*z[5];
    z[93]= - z[78]*z[86];
    z[93]= - n<T>(97,3)*z[30] + z[93];
    z[93]=z[15]*z[93];
    z[86]= - n<T>(97,3)*z[78] + z[86];
    z[86]=z[22]*z[86];
    z[86]=z[93] + z[86];
    z[93]=113*z[78] - n<T>(91,2)*z[5];
    z[93]=z[5]*z[93];
    z[91]=n<T>(7,4)*z[8] - z[91];
    z[91]=z[8]*z[91];
    z[91]=z[93] + 49*z[91];
    z[93]= - n<T>(5,2) - n<T>(11,3)*z[78];
    z[93]= - n<T>(833,9)*z[6] + z[95] + 11*z[93] - n<T>(151,3)*z[5];
    z[93]=z[93]*z[76];
    z[91]=n<T>(1,3)*z[91] + z[93];
    z[76]=z[91]*z[76];
    z[90]=z[90] - z[78] + z[94];
    z[90]=z[18]*z[90];
    z[91]=z[99] + z[30];
    z[93]= - z[10]*z[78];
    z[93]=z[93] - z[91];
    z[93]=z[13]*z[93];
    z[95]=z[78] - z[4];
    z[99]=z[10] - z[95];
    z[99]=z[21]*z[99];
    z[93]= - z[64] - z[93] - z[99] + z[58] - z[42];
    z[99]=z[78]*z[8];
    z[99]=z[99] + z[30];
    z[100]=z[7]*z[78];
    z[100]=z[100] + z[99];
    z[100]=z[16]*z[100];
    z[101]=z[7] - z[89];
    z[101]=z[25]*z[101];
    z[100]=z[100] + z[101];
    z[101]= - z[5]*z[78];
    z[98]= - z[30] + z[101] - z[98];
    z[98]=z[11]*z[98];
    z[101]= - z[3] + z[97];
    z[101]=z[20]*z[101];
    z[98]= - z[59] + z[98] + z[101];
    z[101]=z[14]*z[78];
    z[91]=z[101] + z[91];
    z[91]=z[12]*z[91];
    z[95]= - z[14] + z[95];
    z[95]=z[28]*z[95];
    z[91]=z[91] + z[95];
    z[95]=n<T>(5,8)*z[5] - z[89];
    z[95]=z[8]*z[95];
    z[84]=n<T>(5,4)*z[84] + z[95];
    z[80]=z[84]*z[80];
    z[84]= - z[9]*z[78];
    z[84]=z[84] - z[99];
    z[84]=z[17]*z[84];
    z[81]=z[94] + z[81];
    z[81]=z[24]*z[81];
    z[92]= - z[2] + z[92];
    z[92]=z[19]*z[92];
    z[92]= - z[38] + z[92] - z[44];
    z[94]= - z[10] - z[7];
    z[94]=z[41]*z[94];
    z[94]=z[60] + z[94] - z[67];
    z[95]=z[54] - z[43];
    z[99]=z[62] - z[32];
    z[101]= - n<T>(5,12)*z[37] - n<T>(35,24)*z[35] + n<T>(15,4)*z[34] + n<T>(449,72)*
    z[31];
    z[101]=i*z[101];
    z[78]=n<T>(17,2)*z[5] + n<T>(55,8) - n<T>(28,3)*z[78];
    z[78]=z[78]*npow(z[5],2);
    z[102]= - n<T>(463,9)*z[5] + n<T>(27,2)*z[8];
    z[102]= - n<T>(1,18)*z[9] + n<T>(1,4)*z[102] - n<T>(325,9)*z[6];
    z[102]=n<T>(49,18)*z[7] + n<T>(83,36)*z[14] - n<T>(89,48)*z[10] + n<T>(389,144)*z[4]
    + n<T>(137,72)*z[3] + n<T>(1,2)*z[102] + n<T>(25,9)*z[2];
    z[102]=z[30]*z[102];
    z[87]= - n<T>(5,12)*z[3] + n<T>(1,4)*z[9] + n<T>(2,3)*z[87];
    z[87]=z[26]*z[87];
    z[97]= - n<T>(1,6)*z[14] + z[3] + n<T>(5,6)*z[9] + z[6] + z[97];
    z[97]=z[29]*z[97];
    z[89]=z[9] - z[89];
    z[89]=z[27]*z[89];
    z[103]= - z[3] - z[10];
    z[103]=z[40]*z[103];

    r +=  - n<T>(25,36)*z[33] - n<T>(25,24)*z[36] - n<T>(445,24)*z[39] + n<T>(25,48)*
      z[45] + n<T>(19,48)*z[46] + n<T>(15,8)*z[47] + n<T>(3,4)*z[48] + n<T>(8,3)*z[49]
       + n<T>(41,12)*z[50] + n<T>(5,16)*z[51] + n<T>(35,48)*z[52] + n<T>(5,6)*z[53]
     - 
      n<T>(25,16)*z[55] + n<T>(15,16)*z[56] + n<T>(5,12)*z[57] + n<T>(539,24)*z[61]
     +  n<T>(196,3)*z[63] - n<T>(49,24)*z[65] + n<T>(49,8)*z[66] + n<T>(13,6)*z[68]
     + n<T>(35,24)*z[69] - n<T>(7,3)*z[70]
     + n<T>(2,3)*z[71] - n<T>(1,24)*z[72] - n<T>(1,4)*
      z[73] + 4*z[74] + n<T>(7,8)*z[75] + z[76] + z[77] + z[78] + z[79] + 
      z[80] + n<T>(5,2)*z[81] + n<T>(49,3)*z[82] + z[83] + z[84] + z[85] + n<T>(1,2)
      *z[86] + z[87] + z[88] + z[89] + n<T>(25,6)*z[90] + n<T>(3,2)*z[91] + n<T>(5,3)*z[92]
     - n<T>(5,4)*z[93]
     + n<T>(49,6)*z[94]
     + n<T>(5,8)*z[95]
     + z[96]
     +  z[97] + n<T>(1,3)*z[98] + n<T>(49,4)*z[99] + n<T>(49,2)*z[100] + z[101] + 
      z[102] + n<T>(35,12)*z[103];
 
    return r;
}

template std::complex<double> qg_2lha_tf321(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf321(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
