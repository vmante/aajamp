#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1040(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[104];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[9];
    z[10]=d[4];
    z[11]=d[8];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[12];
    z[26]=e[13];
    z[27]=c[3];
    z[28]=c[11];
    z[29]=c[15];
    z[30]=c[18];
    z[31]=c[19];
    z[32]=c[21];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[29];
    z[38]=c[30];
    z[39]=c[31];
    z[40]=e[3];
    z[41]=e[10];
    z[42]=e[6];
    z[43]=e[14];
    z[44]=f[0];
    z[45]=f[1];
    z[46]=f[2];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[11];
    z[51]=f[12];
    z[52]=f[13];
    z[53]=f[14];
    z[54]=f[15];
    z[55]=f[16];
    z[56]=f[17];
    z[57]=f[18];
    z[58]=f[30];
    z[59]=f[31];
    z[60]=f[32];
    z[61]=f[33];
    z[62]=f[34];
    z[63]=f[35];
    z[64]=f[36];
    z[65]=f[37];
    z[66]=f[39];
    z[67]=f[43];
    z[68]=f[50];
    z[69]=f[51];
    z[70]=f[52];
    z[71]=f[53];
    z[72]=f[55];
    z[73]=f[58];
    z[74]=f[74];
    z[75]=f[75];
    z[76]=n<T>(1,2)*z[5];
    z[77]=z[1]*i;
    z[78]=z[76] - z[77];
    z[79]=z[78]*z[5];
    z[80]=z[6] - z[5];
    z[81]=z[4] - 2*z[77] - z[80];
    z[81]=z[2]*z[81];
    z[82]=n<T>(1,2)*z[2];
    z[83]=z[77] - z[5];
    z[84]=3*z[83];
    z[85]=z[4] + z[84] + n<T>(23,4)*z[6];
    z[85]= - n<T>(53,24)*z[9] - z[82] + n<T>(1,2)*z[85] + z[7];
    z[85]=z[9]*z[85];
    z[86]=n<T>(103,24)*z[6] - n<T>(13,6) + 3*z[5];
    z[86]=z[6]*z[86];
    z[87]=2*z[6];
    z[88]=n<T>(3,8)*z[4] - z[5] - z[87];
    z[88]=z[4]*z[88];
    z[87]= - z[87] + z[7];
    z[87]=z[7]*z[87];
    z[89]=n<T>(1,2)*z[3];
    z[90]= - n<T>(10,3) + z[89];
    z[90]=z[3]*z[90];
    z[81]=z[85] + z[90] + z[81] + z[87] + z[88] - 3*z[79] + z[86];
    z[81]=z[9]*z[81];
    z[85]=n<T>(85,8)*z[6] + n<T>(85,4)*z[77] + z[5];
    z[85]=z[6]*z[85];
    z[85]=n<T>(19,2)*z[79] + z[85];
    z[86]=5*z[4];
    z[87]=z[77]*z[86];
    z[88]=3*z[7] - n<T>(23,2)*z[77] + z[80];
    z[90]=n<T>(1,2)*z[7];
    z[88]=z[88]*z[90];
    z[84]= - n<T>(3,2)*z[3] + z[7] - z[84] - z[6];
    z[84]=z[84]*z[89];
    z[91]=z[77] - z[6];
    z[92]=9*z[5] - n<T>(31,2)*z[91];
    z[92]= - n<T>(73,12)*z[10] - 13*z[3] + 5*z[2] - n<T>(27,4)*z[7] + n<T>(3,4)*
    z[92] - z[86];
    z[93]=n<T>(1,2)*z[10];
    z[92]=z[92]*z[93];
    z[84]=z[92] + z[84] + z[88] + n<T>(1,2)*z[85] + z[87];
    z[84]=z[10]*z[84];
    z[85]=3*z[77];
    z[87]=n<T>(1,2)*z[6];
    z[88]=n<T>(5,2)*z[4];
    z[92]= - n<T>(1,4)*z[7] + z[88] - z[87] - z[85] + z[76];
    z[92]=z[7]*z[92];
    z[80]=z[80] + z[77];
    z[94]=z[2]*z[80];
    z[91]= - z[6]*z[91];
    z[91]=z[91] + z[94];
    z[94]=n<T>(1,2)*z[4];
    z[95]=z[94] - z[77];
    z[96]= - z[95]*z[88];
    z[97]=14*z[77] - n<T>(47,4)*z[5];
    z[97]=z[5]*z[97];
    z[98]= - n<T>(59,8)*z[5] + 5 + n<T>(7,8)*z[77];
    z[98]=n<T>(22,3)*z[3] - n<T>(3,2)*z[7] - n<T>(5,4)*z[4] + n<T>(1,3)*z[98] - n<T>(1,8)*
    z[6];
    z[98]=z[3]*z[98];
    z[91]=z[98] + z[92] + z[96] + n<T>(1,3)*z[97] + n<T>(1,4)*z[91];
    z[91]=z[3]*z[91];
    z[92]=41*z[5];
    z[96]=n<T>(239,12)*z[6] - n<T>(731,6)*z[77] + z[92];
    z[96]=z[6]*z[96];
    z[97]=z[5] - z[95];
    z[97]=z[4]*z[97];
    z[96]=n<T>(125,2)*z[97] - n<T>(125,6)*z[79] + z[96];
    z[97]= - z[77] - z[76];
    z[98]=n<T>(41,2)*z[7];
    z[99]=n<T>(125,12)*z[2];
    z[97]=z[99] + z[98] + n<T>(125,3)*z[97] - n<T>(41,2)*z[6];
    z[97]=z[2]*z[97];
    z[100]=n<T>(89,3)*z[77] - z[92];
    z[100]= - n<T>(89,12)*z[7] + n<T>(1,2)*z[100] + n<T>(7,3)*z[6];
    z[100]=z[7]*z[100];
    z[101]= - 39*z[77] - 89*z[6];
    z[101]=n<T>(39,4)*z[10] + n<T>(1,2)*z[101] + 25*z[7];
    z[101]=z[101]*z[93];
    z[96]=z[101] + z[97] + n<T>(1,2)*z[96] + z[100];
    z[97]= - 125*z[5] + 731*z[6];
    z[97]=n<T>(1,3)*z[97] - 125*z[4];
    z[97]=n<T>(39,16)*z[10] + z[99] + n<T>(1,16)*z[97] - n<T>(53,3)*z[7];
    z[97]=z[8]*z[97];
    z[96]=n<T>(1,2)*z[96] + z[97];
    z[96]=z[8]*z[96];
    z[97]= - z[77] - n<T>(7,6)*z[6];
    z[97]=z[97]*z[87];
    z[99]=n<T>(3,2)*z[4];
    z[85]= - z[99] + z[85] + z[6];
    z[85]=z[85]*z[94];
    z[85]=z[85] + n<T>(37,3)*z[79] + z[97];
    z[89]= - z[89] - z[83];
    z[89]=z[3]*z[89];
    z[93]=z[93] + z[83];
    z[93]=z[10]*z[93];
    z[94]=z[94] - n<T>(1,2)*z[77] + n<T>(1,3)*z[6];
    z[94]=z[9]*z[94];
    z[85]=z[94] + n<T>(25,2)*z[93] + n<T>(1,2)*z[85] + n<T>(19,3)*z[89];
    z[89]=n<T>(53,2)*z[6] - n<T>(17,2)*z[77] + 13*z[5];
    z[89]=n<T>(1,3)*z[89] - z[99];
    z[89]= - n<T>(5,24)*z[11] - n<T>(17,24)*z[9] + n<T>(25,8)*z[10] + n<T>(1,8)*z[89]
     - 
   n<T>(11,3)*z[3];
    z[89]=z[11]*z[89];
    z[85]=n<T>(1,2)*z[85] + z[89];
    z[85]=z[11]*z[85];
    z[86]= - z[95]*z[86];
    z[89]= - 11*z[77] - n<T>(17,4)*z[5];
    z[89]=5*z[89] + n<T>(577,4)*z[6];
    z[88]= - n<T>(335,12)*z[7] + n<T>(1,3)*z[89] - z[88];
    z[88]=z[7]*z[88];
    z[89]=n<T>(109,6)*z[5] + 45*z[6];
    z[89]=z[6]*z[89];
    z[86]=z[88] + z[86] - n<T>(109,6)*z[79] + z[89];
    z[86]=z[86]*z[90];
    z[88]=z[77] + n<T>(5,4)*z[5];
    z[88]=z[5]*z[88];
    z[89]=23*z[77];
    z[90]=n<T>(125,4)*z[6] + z[89] + n<T>(5,2)*z[5];
    z[90]=z[6]*z[90];
    z[88]=7*z[88] + z[90];
    z[80]= - z[7] - z[80];
    z[80]=z[7]*z[80];
    z[90]=121*z[77] + z[92];
    z[90]= - z[98] + n<T>(145,6)*z[4] + n<T>(1,2)*z[90] - 3*z[6];
    z[92]=21*z[2];
    z[90]=n<T>(1,2)*z[90] - z[92];
    z[90]=z[90]*z[82];
    z[93]= - z[89] - n<T>(29,2)*z[5];
    z[93]=n<T>(115,12)*z[4] + n<T>(5,6)*z[93] - z[6];
    z[93]=z[4]*z[93];
    z[80]=z[90] + n<T>(41,4)*z[80] + n<T>(1,2)*z[88] + z[93];
    z[80]=z[2]*z[80];
    z[88]=z[8] + z[7];
    z[90]=z[77]*z[88];
    z[90]=z[27] + z[90];
    z[90]=z[16]*z[90];
    z[88]= - z[77] + z[88];
    z[88]=z[24]*z[88];
    z[88]=z[90] + z[88];
    z[90]=n<T>(61,3)*z[5] + n<T>(11,2) + n<T>(49,3)*z[6];
    z[93]= - z[77]*z[90];
    z[93]= - n<T>(61,3)*z[27] + z[93];
    z[93]=z[15]*z[93];
    z[90]= - n<T>(61,3)*z[77] + z[90];
    z[90]=z[21]*z[90];
    z[90]=z[93] + z[90];
    z[93]=n<T>(15,16)*z[6] + n<T>(9,8)*z[77] + z[5];
    z[93]=z[6]*z[93];
    z[89]=23*z[4] - n<T>(9,2)*z[6] - z[89] - n<T>(85,6)*z[5];
    z[89]=z[4]*z[89];
    z[79]=n<T>(1,8)*z[89] - n<T>(85,24)*z[79] + z[93];
    z[79]=z[4]*z[79];
    z[89]=z[77]*z[4];
    z[89]=z[89] + z[27];
    z[93]= - z[9]*z[77];
    z[93]=z[93] - z[89];
    z[93]=z[13]*z[93];
    z[94]=z[77] - z[4];
    z[97]=z[9] - z[94];
    z[97]=z[25]*z[97];
    z[93]=z[68] + z[93] + z[97];
    z[97]= - z[10]*z[77];
    z[89]=z[97] - z[89];
    z[89]=z[14]*z[89];
    z[94]=z[10] - z[94];
    z[94]=z[20]*z[94];
    z[89]=z[44] + z[89] + z[94];
    z[94]=z[5] + z[3];
    z[94]=z[77]*z[94];
    z[94]=z[27] + z[94];
    z[94]=z[12]*z[94];
    z[97]=z[3] - z[83];
    z[97]=z[19]*z[97];
    z[94]=z[94] + z[97];
    z[97]= - 15*z[77] - n<T>(7,6)*z[5];
    z[76]=z[97]*z[76];
    z[97]=n<T>(59,4) - 31*z[77];
    z[97]= - n<T>(121,2)*z[6] + n<T>(1,3)*z[97] - n<T>(131,4)*z[5];
    z[97]=z[6]*z[97];
    z[76]=z[76] + z[97];
    z[76]=z[76]*z[87];
    z[87]=n<T>(425,2)*z[7] - 61*z[4] - n<T>(7,2)*z[5] - 1693*z[6];
    z[87]=n<T>(1,9)*z[87] + z[92];
    z[87]=n<T>(115,9)*z[8] - n<T>(173,6)*z[10] + n<T>(1,2)*z[87] + 9*z[3];
    z[87]= - n<T>(113,72)*z[11] + n<T>(1,4)*z[87] + n<T>(8,9)*z[9];
    z[87]=z[27]*z[87];
    z[92]= - z[82] - z[95];
    z[92]=z[17]*z[92];
    z[78]=z[82] + z[78];
    z[78]=z[18]*z[78];
    z[82]= - n<T>(1,2)*z[8] - z[82] + z[77];
    z[82]=z[23]*z[82];
    z[95]= - z[46] + z[65] + z[53];
    z[97]=z[75] + z[74];
    z[98]=z[11] + z[3];
    z[98]=2 + n<T>(5,2)*z[98];
    z[98]=z[41]*z[98];
    z[98]=z[98] + z[57];
    z[99]= - n<T>(81,4)*z[35] - n<T>(37,4)*z[33] - n<T>(47,4)*z[32] + n<T>(29,24)*z[28];
    z[99]=i*z[99];
    z[100]=n<T>(11,4) - n<T>(29,3)*z[77];
    z[100]=n<T>(1,2)*z[100] + 5*z[5];
    z[100]=z[100]*npow(z[5],2);
    z[83]=41*z[2] - n<T>(541,6)*z[7] + 43*z[83] - n<T>(799,6)*z[6];
    z[83]= - n<T>(41,2)*z[8] - z[10] + n<T>(1,2)*z[83] + z[3];
    z[83]=z[22]*z[83];
    z[77]=z[2] + z[77] - 2*z[5];
    z[77]= - n<T>(7,4)*z[11] - n<T>(35,12)*z[9] - n<T>(83,12)*z[6] + 2*z[77];
    z[77]=z[26]*z[77];
    z[101]=z[3] + z[10];
    z[101]=z[40]*z[101];
    z[102]= - z[7] - z[10];
    z[102]=z[42]*z[102];
    z[103]= - z[9] - z[11];
    z[103]=z[43]*z[103];

    r +=  - n<T>(337,24)*z[29] + n<T>(19,2)*z[30] + n<T>(7,16)*z[31] + n<T>(65,8)*z[34]
       + n<T>(7,4)*z[36] - n<T>(57,8)*z[37] - n<T>(57,2)*z[38] + n<T>(365,16)*z[39]
     - n<T>(5,2)*z[45]
     - n<T>(85,48)*z[47]
     + n<T>(13,24)*z[48] - n<T>(125,12)*z[49] - n<T>(125,48)*z[50] - n<T>(23,8)*z[51]
     - n<T>(157,12)*z[52]
     - n<T>(15,4)*z[54]
     + n<T>(29,8)
      *z[55] - n<T>(125,16)*z[56] + n<T>(25,4)*z[58] + n<T>(1223,48)*z[59] + n<T>(325,24)*z[60]
     + 86*z[61] - n<T>(47,8)*z[62]
     + n<T>(109,12)*z[63] - n<T>(39,16)*
      z[64] + n<T>(97,16)*z[66] - n<T>(41,4)*z[67] - n<T>(9,16)*z[69] + n<T>(3,2)*z[70]
       + n<T>(23,2)*z[71] + n<T>(3,16)*z[72] + n<T>(29,48)*z[73] + z[76] + z[77]
     +  3*z[78] + z[79] + z[80] + z[81] + n<T>(125,6)*z[82] + z[83] + z[84]
       + z[85] + z[86] + z[87] + n<T>(335,12)*z[88] + 5*z[89] + n<T>(1,2)*z[90]
       + z[91] + n<T>(85,6)*z[92] + n<T>(3,4)*z[93] + n<T>(41,12)*z[94] - n<T>(5,4)*
      z[95] + z[96] + n<T>(1,4)*z[97] + n<T>(5,3)*z[98] + z[99] + z[100] + 11*
      z[101] + n<T>(39,4)*z[102] + n<T>(5,12)*z[103];
 
    return r;
}

template std::complex<double> qg_2lha_tf1040(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1040(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
