#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf758(
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=d[0];
    z[2]=d[6];
    z[3]=d[8];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[9];
    z[7]=d[7];
    z[8]=z[7] - z[1];
    z[9]=z[2]*z[8];
    z[10]=z[7] - z[4];
    z[11]=z[6]*z[10];
    z[8]= - z[6] - z[8];
    z[8]=z[3]*z[8];
    z[10]= - z[5] + z[3] + z[6] - z[10];
    z[10]=z[5]*z[10];

    r += z[8] + z[9] + z[10] + z[11];
 
    return r;
}

template std::complex<double> qg_2lha_tf758(
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf758(
  const std::array<std::complex<dd_real>,25>& d
);
#endif
