#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1308(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[71];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[4];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[1];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[18];
    z[20]=c[19];
    z[21]=c[23];
    z[22]=c[25];
    z[23]=c[26];
    z[24]=c[28];
    z[25]=c[29];
    z[26]=c[30];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=e[10];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[6];
    z[36]=f[8];
    z[37]=f[9];
    z[38]=f[11];
    z[39]=f[12];
    z[40]=f[13];
    z[41]=f[14];
    z[42]=f[15];
    z[43]=f[16];
    z[44]=f[17];
    z[45]=f[18];
    z[46]=f[23];
    z[47]=f[26];
    z[48]=f[27];
    z[49]=f[28];
    z[50]=3*z[4];
    z[51]=12*z[3];
    z[52]=z[1]*i;
    z[53]= - n<T>(10,3)*z[6] - z[7] + z[51] - 15*z[2] + z[8] + z[50] + 10*
    z[52];
    z[53]=z[6]*z[53];
    z[54]=2*z[4];
    z[55]=z[54]*z[52];
    z[56]=npow(z[4],2);
    z[55]=z[55] - z[56];
    z[57]=5*z[52];
    z[50]=z[50] + z[57];
    z[50]=3*z[50] - z[8];
    z[50]=2*z[50] - 9*z[2];
    z[50]=z[2]*z[50];
    z[58]= - z[4] - z[52];
    z[58]=2*z[58] + z[3];
    z[51]=z[58]*z[51];
    z[58]=z[8] - z[4];
    z[59]=z[58] + z[52];
    z[60]=z[2] + z[59];
    z[60]=2*z[60] - z[7];
    z[60]=z[7]*z[60];
    z[61]=z[52] - z[4];
    z[62]=2*z[61];
    z[63]=z[62] + z[8];
    z[63]=z[63]*z[8];
    z[50]=z[53] + z[60] + z[51] + z[50] - 3*z[55] - z[63];
    z[50]=z[6]*z[50];
    z[51]= - z[2] + z[57] - z[58];
    z[51]=2*z[51] - 5*z[5];
    z[51]=z[5]*z[51];
    z[53]= - z[4] + 7*z[52];
    z[53]=2*z[53] + z[8];
    z[53]=z[8]*z[53];
    z[60]=z[8] + z[4] + 23*z[52];
    z[60]=2*z[60] - z[2];
    z[60]=z[2]*z[60];
    z[57]=n<T>(17,3)*z[3] + z[5] - 23*z[2] - 7*z[8] + 11*z[4] - z[57];
    z[57]=z[3]*z[57];
    z[51]=z[57] + z[51] + z[60] - 11*z[55] + z[53];
    z[51]=z[3]*z[51];
    z[53]=z[2] + 2*z[59];
    z[53]=z[53]*z[2];
    z[57]=21*z[5];
    z[59]= - z[62] - z[5];
    z[59]=z[59]*z[57];
    z[57]=n<T>(1,2)*z[7] - z[57] + z[2] + 10*z[61] + 31*z[8];
    z[57]=z[7]*z[57];
    z[57]=z[57] + z[59] - z[53] + 10*z[55] + 11*z[63];
    z[57]=z[7]*z[57];
    z[59]=2*z[55] + z[63];
    z[60]=5*z[2];
    z[62]=z[62] + 5*z[8];
    z[62]=n<T>(13,6)*z[5] + 7*z[62] + z[60];
    z[62]=z[5]*z[62];
    z[53]=z[62] + 7*z[59] + z[53];
    z[53]=z[5]*z[53];
    z[59]=z[3] + z[5];
    z[62]= - z[52]*z[59];
    z[62]= - z[16] + z[62];
    z[62]=z[10]*z[62];
    z[59]= - z[52] + z[59];
    z[59]=z[14]*z[59];
    z[59]=z[32] + z[62] + z[59];
    z[62]=n<T>(13,3)*z[2] - z[54] - 9*z[52];
    z[60]=z[62]*z[60];
    z[55]=14*z[55] + z[60];
    z[55]=z[2]*z[55];
    z[60]=2*z[8];
    z[54]= - z[60] + z[54] - 43*z[52];
    z[54]=43*z[3] + 4*z[5] + 2*z[54] + 39*z[2];
    z[54]=z[11]*z[54];
    z[62]=z[52]*z[4];
    z[63]= - z[8]*z[52];
    z[63]= - z[16] - z[62] + z[63];
    z[63]=z[9]*z[63];
    z[61]= - z[8] + z[61];
    z[61]=z[13]*z[61];
    z[61]=z[63] + z[61];
    z[58]=3*z[6] - z[7] + 4*z[2] - 6*z[52] + z[58];
    z[58]=z[15]*z[58];
    z[58]=z[58] - z[49];
    z[63]=z[56]*z[52];
    z[56]=6*z[56] - 7*z[62];
    z[62]= - n<T>(58,3)*z[8] + 9*z[4] + z[52];
    z[62]=z[8]*z[62];
    z[56]=2*z[56] + z[62];
    z[56]=z[56]*z[60];
    z[60]= - z[31] + z[48] - z[40];
    z[62]=z[34] - z[33];
    z[64]=z[37] - z[35];
    z[65]=z[39] - z[36];
    z[52]=z[2] + z[4] - 2*z[52];
    z[52]=z[12]*z[52];
    z[52]=z[52] + z[27];
    z[66]=136*z[23] + 45*z[21] - n<T>(1,3)*z[17];
    z[66]=i*z[66];
    z[67]=npow(z[4],3);
    z[68]=n<T>(59,6)*z[6] + n<T>(317,24)*z[7] - n<T>(55,12)*z[3] + n<T>(145,8)*z[5]
     + n<T>(15,4)*z[2] - n<T>(25,4)*z[4]
     + n<T>(13,3)*z[8];
    z[68]=z[16]*z[68];
    z[69]= - z[8] - z[5];
    z[69]=z[28]*z[69];
    z[70]= - z[8] - z[7];
    z[70]=z[29]*z[70];

    r += n<T>(98,3)*z[18] + n<T>(8,3)*z[19] - n<T>(61,12)*z[20] - n<T>(67,3)*z[22]
     - 72
      *z[24] - 2*z[25] - 8*z[26] + n<T>(71,2)*z[30] + z[38] - n<T>(27,2)*z[41]
       + 36*z[42] - 17*z[43] + 11*z[44] + n<T>(11,2)*z[45] + z[46] - n<T>(5,2)*
      z[47] + z[50] + z[51] + 13*z[52] + z[53] + z[54] + z[55] + z[56]
       + z[57] + 4*z[58] + 12*z[59] - n<T>(3,2)*z[60] + 20*z[61] + n<T>(23,2)*
      z[62] - 3*z[63] + n<T>(15,2)*z[64] + n<T>(17,2)*z[65] + z[66] + z[67] + 
      z[68] + 52*z[69] + 40*z[70];
 
    return r;
}

template std::complex<double> qg_2lha_tf1308(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1308(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
