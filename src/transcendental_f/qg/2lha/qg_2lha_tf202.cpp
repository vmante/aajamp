#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf202(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[120];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[19];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=c[31];
    z[39]=e[3];
    z[40]=e[6];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[7];
    z[50]=f[8];
    z[51]=f[10];
    z[52]=f[11];
    z[53]=f[12];
    z[54]=f[13];
    z[55]=f[16];
    z[56]=f[17];
    z[57]=f[18];
    z[58]=f[23];
    z[59]=f[30];
    z[60]=f[31];
    z[61]=f[32];
    z[62]=f[33];
    z[63]=f[34];
    z[64]=f[35];
    z[65]=f[36];
    z[66]=f[37];
    z[67]=f[39];
    z[68]=f[41];
    z[69]=f[43];
    z[70]=f[50];
    z[71]=f[51];
    z[72]=f[52];
    z[73]=f[53];
    z[74]=f[55];
    z[75]=f[56];
    z[76]=f[58];
    z[77]=f[60];
    z[78]=f[83];
    z[79]=f[84];
    z[80]=n<T>(1,3)*z[3];
    z[81]=2*z[6];
    z[82]=z[1]*i;
    z[83]= - 13*z[82] + z[5];
    z[83]= - n<T>(8,3)*z[11] - z[7] + z[4] - z[80] + n<T>(1,4)*z[9] + z[81] + n<T>(1,12)*z[83]
     - z[2]
     + z[8];
    z[83]=z[11]*z[83];
    z[84]=n<T>(1,2)*z[5];
    z[85]=z[84] - z[82];
    z[85]=z[85]*z[5];
    z[86]=z[82] - z[8];
    z[87]=2*z[86];
    z[88]=z[87] + z[6];
    z[88]=z[88]*z[81];
    z[89]=z[82] - z[5];
    z[90]=n<T>(1,2)*z[9];
    z[91]=z[90] + z[89];
    z[91]=z[91]*z[90];
    z[92]= - n<T>(1,2)*z[3] - z[89];
    z[80]=z[92]*z[80];
    z[92]=2*z[82];
    z[93]=z[92] - z[8];
    z[94]= - z[8]*z[93];
    z[95]= - z[4]*z[92];
    z[87]= - z[87] + z[7];
    z[87]=z[7]*z[87];
    z[80]=z[83] + z[87] + z[95] + z[80] + z[91] + z[88] + n<T>(1,6)*z[85] + 
    z[94];
    z[80]=z[11]*z[80];
    z[83]=5*z[6];
    z[87]=n<T>(1,9)*z[3];
    z[88]=n<T>(137,2)*z[5] + 7*z[8];
    z[88]= - n<T>(23,18)*z[11] - n<T>(247,36)*z[7] + n<T>(1,12)*z[4] - n<T>(35,6)*z[10]
    + z[87] - n<T>(2,9)*z[2] + n<T>(1,6)*z[9] + n<T>(1,18)*z[88] + z[83];
    z[88]=z[30]*z[88];
    z[91]=z[82]*z[5];
    z[91]=z[91] + z[30];
    z[94]=z[3]*z[82];
    z[94]=z[94] + z[91];
    z[94]=z[12]*z[94];
    z[95]=z[82]*z[4];
    z[95]=z[95] + z[30];
    z[96]= - z[10]*z[82];
    z[96]=z[96] - z[95];
    z[96]=z[13]*z[96];
    z[97]=z[82]*z[8];
    z[97]=z[97] + z[30];
    z[98]= - z[9]*z[82];
    z[98]=z[98] - z[97];
    z[98]=z[17]*z[98];
    z[99]=z[3] - z[89];
    z[99]=z[20]*z[99];
    z[100]=z[9] - z[86];
    z[100]=z[27]*z[100];
    z[101]=z[82] - z[4];
    z[102]=z[10] - z[101];
    z[102]=z[28]*z[102];
    z[80]=z[102] + z[79] - z[77] - z[72] + z[96] + z[98] + z[99] + 
    z[100] + z[94] + z[80] + z[88] - z[66] - z[65] + z[54] + z[43];
    z[88]=n<T>(1,2)*z[8];
    z[94]=z[88] - z[82];
    z[96]=z[5] - z[94];
    z[96]=z[8]*z[96];
    z[98]= - z[90] + z[82] + n<T>(7,3)*z[6];
    z[98]=z[98]*z[90];
    z[99]=z[10] - z[90] - n<T>(5,6)*z[6] - z[8] - z[82] - n<T>(5,3)*z[5];
    z[100]=n<T>(4,3)*z[2];
    z[99]=n<T>(1,18)*z[4] + z[100] + n<T>(1,2)*z[99];
    z[99]=z[4]*z[99];
    z[102]=5*z[82];
    z[103]=z[102] - n<T>(13,2)*z[6];
    z[103]=z[6]*z[103];
    z[104]= - 4*z[82] + z[5];
    z[104]=2*z[104] - z[2];
    z[104]=z[2]*z[104];
    z[105]=z[82] + z[9];
    z[105]= - n<T>(1,3)*z[10] + z[6] - n<T>(2,3)*z[105];
    z[105]=z[10]*z[105];
    z[96]=z[99] + z[105] + n<T>(1,3)*z[104] + z[98] + n<T>(1,6)*z[103] - n<T>(5,3)*
    z[85] + z[96];
    z[96]=z[4]*z[96];
    z[98]=4*z[8];
    z[99]=4*z[6];
    z[103]=4*z[5];
    z[104]=z[99] - z[98] + 7*z[82] - z[103];
    z[104]=2*z[104] + z[2];
    z[105]=n<T>(1,9)*z[2];
    z[104]=z[104]*z[105];
    z[106]=z[92] - z[5];
    z[107]=2*z[5];
    z[108]= - z[106]*z[107];
    z[109]=z[102] + z[103];
    z[109]=2*z[109];
    z[110]=z[109] + z[8];
    z[111]=n<T>(1,9)*z[8];
    z[110]=z[110]*z[111];
    z[112]=n<T>(4,3)*z[6];
    z[113]= - z[112] - z[5] + n<T>(1,3)*z[8];
    z[112]=z[113]*z[112];
    z[113]=n<T>(4,3)*z[10];
    z[114]=2*z[89];
    z[115]=z[114] + z[10];
    z[115]=z[115]*z[113];
    z[116]=z[107] - z[82];
    z[113]=n<T>(37,27)*z[7] + z[113] + n<T>(8,9)*z[2] - n<T>(46,9)*z[6] - n<T>(5,9)*z[8]
    + z[116];
    z[113]=z[7]*z[113];
    z[117]= - z[92] + z[4];
    z[117]=z[4]*z[117];
    z[104]=z[113] + n<T>(1,3)*z[117] + z[115] + z[104] + z[112] + z[108] + 
    z[110];
    z[104]=z[7]*z[104];
    z[108]=2*z[2];
    z[110]= - 10 - z[82];
    z[84]=n<T>(11,6)*z[3] - z[108] + z[9] + z[88] + 2*z[110] + z[84];
    z[84]=z[84]*z[87];
    z[87]= - z[5]*z[116];
    z[110]= - z[88] + z[89];
    z[110]=z[8]*z[110];
    z[87]=z[87] + z[110];
    z[110]=z[114] - z[8];
    z[112]=n<T>(5,6)*z[9];
    z[113]= - z[112] + n<T>(1,3)*z[110] + z[99];
    z[114]=n<T>(1,3)*z[9];
    z[113]=z[113]*z[114];
    z[115]= - n<T>(2,3)*z[89] - z[6];
    z[115]=z[115]*z[81];
    z[117]=z[89] - z[8];
    z[118]=n<T>(1,2)*z[2];
    z[119]= - z[118] + z[9] - z[117];
    z[119]=z[119]*z[105];
    z[84]=z[84] + z[119] + z[113] + n<T>(1,9)*z[87] + z[115];
    z[84]=z[3]*z[84];
    z[87]= - z[106]*z[103];
    z[94]= - z[8]*z[94];
    z[87]=z[87] + z[94];
    z[94]=n<T>(34,9)*z[10] + n<T>(2,3)*z[3] + z[118] - z[112] - n<T>(16,3)*z[6] - n<T>(3,2)*z[82]
     + n<T>(4,3)*z[5];
    z[94]=z[10]*z[94];
    z[112]=z[81] + z[116];
    z[112]=2*z[112] + z[9];
    z[112]=z[112]*z[114];
    z[113]=z[82] - z[118];
    z[113]=z[2]*z[113];
    z[115]= - n<T>(35,6)*z[6] + n<T>(26,9) - z[82];
    z[115]=z[6]*z[115];
    z[116]=z[89] - z[9];
    z[118]=z[6] + n<T>(10,3) + z[116];
    z[118]=z[3]*z[118];
    z[87]=z[94] + n<T>(4,3)*z[118] + z[113] + z[112] + n<T>(1,3)*z[87] + z[115];
    z[87]=z[10]*z[87];
    z[94]=2*z[8];
    z[102]= - z[99] + z[94] - z[102] + z[107];
    z[99]=z[102]*z[99];
    z[81]= - n<T>(17,2)*z[2] + z[90] + z[81] + z[98] + n<T>(47,2)*z[82] + z[5];
    z[81]=z[2]*z[81];
    z[90]= - z[109] + 5*z[8];
    z[90]=z[8]*z[90];
    z[102]=z[5]*z[106];
    z[107]=z[9] + z[117];
    z[107]=z[9]*z[107];
    z[81]=z[81] + z[107] + z[99] - 8*z[102] + z[90];
    z[81]=z[81]*z[105];
    z[83]=n<T>(11,3)*z[7] - z[100] - n<T>(4,3)*z[117] + z[83];
    z[83]=z[23]*z[83];
    z[90]= - z[7]*z[82];
    z[90]=z[90] - z[97];
    z[90]=z[16]*z[90];
    z[86]= - z[7] + z[86];
    z[86]=z[25]*z[86];
    z[97]= - z[7] - z[11];
    z[97]=z[40]*z[97];
    z[83]= - z[83] - z[90] - z[86] - z[97] + z[70] + z[64];
    z[86]=z[88] + z[92] + z[5];
    z[86]=z[8]*z[86];
    z[86]=n<T>(7,2)*z[85] + z[86];
    z[90]=n<T>(1,2)*z[82] - z[103];
    z[90]=n<T>(1,3)*z[90] - n<T>(5,4)*z[6];
    z[90]=z[6]*z[90];
    z[97]=z[82] + n<T>(7,2)*z[5];
    z[97]=n<T>(1,2)*z[97] - z[8];
    z[97]=n<T>(31,18)*z[9] + n<T>(1,3)*z[97] - n<T>(7,4)*z[6];
    z[97]=z[97]*z[114];
    z[86]=z[97] + n<T>(1,9)*z[86] + z[90];
    z[86]=z[9]*z[86];
    z[90]=z[110]*z[94];
    z[92]= - z[92] - 7*z[5];
    z[92]=z[5]*z[92];
    z[90]=z[92] + z[90];
    z[92]=n<T>(457,6)*z[6] + z[98] + 22*z[5] - 46 - n<T>(1,4)*z[82];
    z[92]=z[6]*z[92];
    z[90]=2*z[90] + z[92];
    z[90]=z[6]*z[90];
    z[92]= - 20 - z[117];
    z[92]=5*z[3] - z[108] + 2*z[92] + 7*z[9];
    z[92]=z[26]*z[92];
    z[94]=z[3] + z[11];
    z[94]=z[39]*z[94];
    z[90]= - z[44] - z[94] - z[90] - z[92] + z[58] - z[51];
    z[92]= - z[111] + z[82] - n<T>(1,9)*z[5];
    z[88]=z[92]*z[88];
    z[85]= - n<T>(1,9)*z[85] + z[88];
    z[85]=z[8]*z[85];
    z[88]=z[11]*z[82];
    z[88]=z[88] + z[95];
    z[88]=z[14]*z[88];
    z[92]= - z[11] + z[101];
    z[92]=z[21]*z[92];
    z[88]=z[88] + z[92] + z[68] + z[67] + z[63] + z[59];
    z[82]=z[6]*z[82];
    z[82]=z[82] + z[91];
    z[82]=z[15]*z[82];
    z[91]= - z[6] + z[89];
    z[91]=z[22]*z[91];
    z[82]=z[82] + z[91];
    z[91]= - z[45] + z[75] + z[48];
    z[92]=z[71] - z[55];
    z[94]=z[106] - z[2];
    z[95]=z[19]*z[94];
    z[95]=z[95] + z[49];
    z[94]= - z[4] + z[8] + z[94];
    z[94]=z[18]*z[94];
    z[94]=z[94] - z[42];
    z[97]= - n<T>(116,9)*z[36] - n<T>(47,18)*z[34] - n<T>(41,108)*z[31];
    z[97]=i*z[97];
    z[89]=z[89]*npow(z[5],2);
    z[98]=4*z[7] + n<T>(17,3)*z[10] - n<T>(8,3)*z[3] + 11*z[6] - n<T>(4,3)*z[116];
    z[98]=z[29]*z[98];
    z[93]= - z[2] + z[93];
    z[93]=z[24]*z[93];
    z[99]=z[9] + z[10];
    z[99]=z[41]*z[99];

    r +=  - n<T>(23,9)*z[32] + n<T>(8,27)*z[33] + n<T>(17,18)*z[35] + n<T>(58,9)*z[37]
       + n<T>(524,27)*z[38] + n<T>(11,36)*z[46] + n<T>(13,9)*z[47] - 3*z[50] - n<T>(1,18)*z[52]
     - n<T>(1,36)*z[53]
     - n<T>(1,2)*z[56]
     - n<T>(2,9)*z[57]
     - n<T>(5,3)*
      z[60] - n<T>(35,9)*z[61] - n<T>(64,9)*z[62] + n<T>(14,9)*z[69] - n<T>(40,3)*z[73]
       + n<T>(1,4)*z[74] - n<T>(7,12)*z[76] + z[78] + n<T>(1,3)*z[80] + z[81] + n<T>(32,9)*z[82]
     - n<T>(4,3)*z[83]
     + z[84]
     + z[85]
     + z[86]
     + z[87]
     + n<T>(2,3)*
      z[88] + n<T>(16,9)*z[89] - n<T>(1,9)*z[90] + n<T>(1,6)*z[91] - n<T>(1,12)*z[92]
       + n<T>(26,9)*z[93] + n<T>(8,3)*z[94] + n<T>(8,9)*z[95] + z[96] + z[97] + 
      z[98] + z[99] + z[104];
 
    return r;
}

template std::complex<double> qg_2lha_tf202(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf202(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
