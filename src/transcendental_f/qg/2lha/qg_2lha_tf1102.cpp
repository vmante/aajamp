#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1102(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[21];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[5];
    z[3]=d[12];
    z[4]=c[3];
    z[5]=d[0];
    z[6]=d[9];
    z[7]=d[1];
    z[8]=d[2];
    z[9]=d[3];
    z[10]=d[6];
    z[11]=d[8];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[5];
    z[15]=e[7];
    z[16]=e[10];
    z[17]=npow(z[9],2);
    z[18]=npow(z[7],2);
    z[17]=z[4] + z[17] + z[18];
    z[18]= - z[11] + 2*z[10] + z[5] - z[8];
    z[19]=z[1]*i;
    z[20]=z[2] - 2*z[6] - z[19] + z[9] - z[18];
    z[20]=z[2]*z[20];
    z[19]=z[3]*z[19];
    z[18]=z[7] + z[18];
    z[18]=z[6]*z[18];

    r +=  - z[12] + z[13] - z[14] + 2*z[15] - z[16] - n<T>(1,2)*z[17] + 
      z[18] + z[19] + z[20];
 
    return r;
}

template std::complex<double> qg_2lha_tf1102(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1102(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
