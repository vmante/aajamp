#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1178(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[45];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[9];
    z[10]=d[17];
    z[11]=e[5];
    z[12]=e[7];
    z[13]=e[9];
    z[14]=e[13];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=e[6];
    z[19]=f[30];
    z[20]=f[31];
    z[21]=f[32];
    z[22]=f[33];
    z[23]=f[35];
    z[24]=f[36];
    z[25]=f[39];
    z[26]=f[43];
    z[27]=f[53];
    z[28]=n<T>(1,2)*z[2];
    z[29]=z[28] + z[7];
    z[30]=n<T>(1,2)*z[9];
    z[31]=z[5] + z[8];
    z[32]=2*z[10];
    z[33]=z[32] - z[30] + z[31] - z[29];
    z[34]=i*z[1];
    z[33]=z[33]*z[34];
    z[35]=npow(z[9],2);
    z[36]= - n<T>(1,2)*z[14] + n<T>(1,4)*z[35];
    z[37]=2*z[13];
    z[38]=z[37] - z[36];
    z[39]=n<T>(1,2)*z[5];
    z[40]=z[9] + z[2] - z[31];
    z[40]=z[40]*z[39];
    z[31]=3*z[3] - z[34] - z[2] - z[31];
    z[31]=n<T>(1,2)*z[31] - n<T>(1,3)*z[4];
    z[31]=z[4]*z[31];
    z[41]=npow(z[7],2);
    z[42]=z[18] + n<T>(1,2)*z[41];
    z[43]=npow(z[2],2);
    z[43]=n<T>(1,4)*z[43];
    z[29]= - n<T>(3,2)*z[8] + z[29];
    z[29]=z[8]*z[29];
    z[44]= - z[2] + z[5];
    z[44]=n<T>(1,2)*z[44] + z[3];
    z[44]=z[3]*z[44];
    z[29]=z[31] + z[44] + z[33] + z[40] + z[29] - z[43] + n<T>(1,2)*z[15] - 
   n<T>(7,2)*z[12] + z[38] - z[42];
    z[29]=z[4]*z[29];
    z[31]= - z[2] + z[8];
    z[31]=z[31]*z[39];
    z[33]=n<T>(5,2)*z[8];
    z[28]=z[5] - z[33] + n<T>(3,2)*z[7] - z[6] + z[28];
    z[28]=z[28]*z[34];
    z[40]= - z[7] + z[5] - z[8];
    z[34]= - n<T>(17,12)*z[3] - z[34] - z[9] + z[2] - n<T>(3,4)*z[40];
    z[34]=z[3]*z[34];
    z[33]=z[33] - z[2] - 3*z[7];
    z[33]=z[8]*z[33];
    z[28]=z[34] + z[28] + z[31] + n<T>(1,2)*z[33] + n<T>(3,4)*z[41] + z[43] - n<T>(7,4)*z[15]
     - n<T>(9,2)*z[12]
     + z[11]
     + z[14];
    z[28]=z[3]*z[28];
    z[31]= - z[12] + n<T>(1,4)*z[41];
    z[33]=n<T>(1,6)*z[15];
    z[34]=z[8]*z[7];
    z[34]=n<T>(1,4)*z[34] + z[33] + z[37] + z[31];
    z[34]=z[8]*z[34];
    z[37]=z[32] - n<T>(1,2)*z[7];
    z[37]=z[8]*z[37];
    z[39]= - z[6] - z[39];
    z[39]=z[5]*z[39];
    z[31]=z[39] + z[37] - z[11] - z[38] - z[31];
    z[31]=z[1]*z[31];
    z[31]=n<T>(1,4)*z[16] + z[31];
    z[31]=i*z[31];
    z[33]=z[33] + z[35];
    z[30]=z[33]*z[30];
    z[33]=npow(z[5],2);
    z[33]=n<T>(1,2)*z[33] - n<T>(1,4)*z[15] - z[36] - z[12] + z[11];
    z[33]=z[5]*z[33];
    z[35]=z[23] - z[26];
    z[32]=z[32] - z[6];
    z[32]=z[15]*z[32];
    z[36]=z[2]*z[12];
    z[37]= - n<T>(1,3)*z[15] - z[42];
    z[37]=z[7]*z[37];

    r +=  - z[17] + z[19] + n<T>(7,4)*z[20] + z[21] + 4*z[22] - n<T>(1,4)*z[24]
       + n<T>(3,4)*z[25] - 2*z[27] + z[28] + z[29] + z[30] + z[31] + z[32]
       + z[33] + z[34] + n<T>(1,2)*z[35] + z[36] + z[37];
 
    return r;
}

template std::complex<double> qg_2lha_tf1178(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1178(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
