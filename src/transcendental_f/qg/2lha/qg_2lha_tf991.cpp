#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf991(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[113];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[18];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[17];
    z[33]=c[18];
    z[34]=c[19];
    z[35]=c[21];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[29];
    z[41]=c[30];
    z[42]=c[31];
    z[43]=e[3];
    z[44]=e[6];
    z[45]=e[13];
    z[46]=e[14];
    z[47]=f[0];
    z[48]=f[1];
    z[49]=f[2];
    z[50]=f[3];
    z[51]=f[4];
    z[52]=f[5];
    z[53]=f[6];
    z[54]=f[7];
    z[55]=f[8];
    z[56]=f[9];
    z[57]=f[11];
    z[58]=f[12];
    z[59]=f[13];
    z[60]=f[16];
    z[61]=f[17];
    z[62]=f[18];
    z[63]=f[23];
    z[64]=f[30];
    z[65]=f[31];
    z[66]=f[32];
    z[67]=f[33];
    z[68]=f[34];
    z[69]=f[36];
    z[70]=f[37];
    z[71]=f[39];
    z[72]=f[41];
    z[73]=f[43];
    z[74]=f[50];
    z[75]=f[51];
    z[76]=f[53];
    z[77]=f[55];
    z[78]=f[56];
    z[79]=f[58];
    z[80]=f[78];
    z[81]=f[83];
    z[82]=f[84];
    z[83]=f[100];
    z[84]=f[101];
    z[85]=n<T>(1,2)*z[8];
    z[86]=z[1]*i;
    z[87]=z[85] - z[86];
    z[88]=z[87]*z[8];
    z[89]=n<T>(19,24)*z[9];
    z[90]= - z[89] + n<T>(19,12)*z[86] + z[8];
    z[90]=z[9]*z[90];
    z[91]=n<T>(13,6)*z[5];
    z[92]=z[91] - n<T>(13,3)*z[86] - 15*z[8];
    z[92]=z[5]*z[92];
    z[93]=z[9] - z[8];
    z[94]= - n<T>(11,6)*z[2] + n<T>(8,3)*z[5] + n<T>(23,6)*z[86] - z[93];
    z[94]=z[2]*z[94];
    z[95]=3*z[5];
    z[96]=19*z[86];
    z[97]=z[96] + 25*z[9];
    z[97]= - n<T>(31,4)*z[6] + 3*z[2] + n<T>(1,2)*z[97] - z[95];
    z[98]=n<T>(1,2)*z[6];
    z[97]=z[97]*z[98];
    z[99]=n<T>(3,2)*z[5];
    z[100]=n<T>(1,2)*z[2];
    z[101]=n<T>(3,2)*z[6] - z[100] + z[99] - z[8] - n<T>(14,3)*z[9];
    z[101]=z[10]*z[101];
    z[89]=n<T>(7,3)*z[4] + n<T>(19,12)*z[10] - n<T>(19,8)*z[6] - n<T>(23,12)*z[2]
     + n<T>(13,24)*z[5] - z[89] - n<T>(25,3)*z[86]
     + n<T>(11,8)*z[8];
    z[89]=z[4]*z[89];
    z[89]=z[89] + z[101] + z[97] + z[94] + n<T>(1,4)*z[92] + n<T>(11,4)*z[88] + 
    z[90];
    z[89]=z[4]*z[89];
    z[90]=n<T>(5,3)*z[8];
    z[92]=n<T>(1,2)*z[9];
    z[94]=n<T>(1,2)*z[5];
    z[97]=n<T>(79,12)*z[6] - n<T>(7,6)*z[2] - z[94] + z[90] - z[92];
    z[97]=z[6]*z[97];
    z[101]= - 2*z[9] - 4*z[86] + z[85];
    z[101]=z[9]*z[101];
    z[102]=41*z[86] + 7*z[8];
    z[103]=n<T>(7,3)*z[5];
    z[104]= - n<T>(55,6)*z[2] + z[103] + n<T>(1,3)*z[102] + 7*z[9];
    z[104]=z[104]*z[100];
    z[105]=n<T>(11,4)*z[10];
    z[106]=z[5] + z[93];
    z[106]=z[105] + n<T>(1,2)*z[106] - 6*z[6];
    z[106]=z[10]*z[106];
    z[107]=n<T>(1,2)*z[4];
    z[108]= - z[86] + z[107];
    z[108]=z[4]*z[108];
    z[109]=19*z[8];
    z[110]= - n<T>(79,4)*z[86] + z[109];
    z[105]= - n<T>(155,18)*z[7] + z[105] - n<T>(143,12)*z[6] - n<T>(29,12)*z[2] + n<T>(25,2)*z[5]
     + n<T>(1,3)*z[110] - n<T>(5,4)*z[9];
    z[105]=z[7]*z[105];
    z[110]= - n<T>(7,6)*z[5] - n<T>(38,3)*z[86] + n<T>(45,4)*z[8];
    z[110]=z[8]*z[110];
    z[97]=z[105] + n<T>(9,2)*z[108] + z[106] + z[97] + z[104] + z[101] + 
    z[110];
    z[97]=z[7]*z[97];
    z[101]=5*z[8];
    z[104]=n<T>(277,4)*z[9] - n<T>(37,2)*z[86] - z[101];
    z[104]=z[9]*z[104];
    z[104]=23*z[88] + n<T>(1,6)*z[104];
    z[105]=n<T>(31,6)*z[5] + n<T>(253,48)*z[9] + n<T>(23,4)*z[8] + 33 - n<T>(79,6)*z[86]
   ;
    z[105]=z[5]*z[105];
    z[104]=n<T>(1,2)*z[104] + z[105];
    z[104]=z[5]*z[104];
    z[90]= - n<T>(17,4)*z[86] - z[90];
    z[90]=z[90]*npow(z[8],2);
    z[105]=31*z[86];
    z[106]= - z[105] - n<T>(25,4)*z[8];
    z[106]=z[8]*z[106];
    z[108]=n<T>(451,9)*z[9] - n<T>(169,2)*z[86] + n<T>(199,3)*z[8];
    z[108]=z[9]*z[108];
    z[106]=n<T>(1,3)*z[106] + n<T>(1,8)*z[108];
    z[106]=z[9]*z[106];
    z[108]= - n<T>(2215,2)*z[5] - 733*z[8] + 277*z[9];
    z[108]=n<T>(1663,2)*z[6] + n<T>(1,4)*z[108] + 53*z[2];
    z[108]=n<T>(331,18)*z[3] - n<T>(527,36)*z[7] - n<T>(335,9)*z[4] + n<T>(1,9)*z[108]
    - n<T>(25,2)*z[10];
    z[108]=n<T>(1,2)*z[108] + n<T>(61,9)*z[11];
    z[108]=z[29]*z[108];
    z[90]=z[108] + z[104] + z[90] + z[106] + z[82] + z[81];
    z[104]=z[85] - 9*z[86];
    z[104]=z[8]*z[104];
    z[106]=n<T>(43,6)*z[9] + n<T>(41,3)*z[86] + 3*z[8];
    z[106]=z[9]*z[106];
    z[104]=z[104] + z[106];
    z[106]=z[94] - z[9];
    z[108]= - 3*z[86] + z[106];
    z[108]=z[5]*z[108];
    z[104]=n<T>(1,2)*z[104] + z[108];
    z[99]=n<T>(65,24)*z[6] - n<T>(3,2)*z[2] - z[99] + n<T>(1,3)*z[9] + z[86] + z[85]
   ;
    z[99]=z[6]*z[99];
    z[106]= - n<T>(3,4)*z[2] + n<T>(3,2)*z[86] + z[8] - z[106];
    z[106]=z[2]*z[106];
    z[108]= - 17*z[9] - 13*z[86] + 9*z[8];
    z[108]=n<T>(1,4)*z[108] - z[5];
    z[108]= - n<T>(5,24)*z[10] - n<T>(7,8)*z[6] + n<T>(1,2)*z[108] + z[2];
    z[108]=z[10]*z[108];
    z[99]=z[108] + z[99] + n<T>(1,2)*z[104] + z[106];
    z[99]=z[10]*z[99];
    z[96]=z[96] + n<T>(29,2)*z[8];
    z[85]=z[96]*z[85];
    z[96]=139*z[86] + z[101];
    z[104]=z[5] + z[9];
    z[96]=n<T>(1,2)*z[96] - 31*z[104];
    z[96]=z[5]*z[96];
    z[101]= - n<T>(19,2)*z[2] - 77*z[5] + 43*z[9] + 101*z[86] - z[101];
    z[101]=z[101]*z[100];
    z[104]= - n<T>(95,4)*z[9] + z[105] - n<T>(43,2)*z[8];
    z[104]=z[9]*z[104];
    z[85]=z[101] + z[96] + z[85] + z[104];
    z[95]=2*z[2] - 5*z[86] + z[95];
    z[95]=2*z[95] - 5*z[6];
    z[95]=z[6]*z[95];
    z[93]=z[86] + z[93];
    z[93]=z[10]*z[93];
    z[96]= - 19*z[2] - n<T>(29,4)*z[5] + n<T>(19,2)*z[9] - 7*z[86] + n<T>(19,4)*z[8]
   ;
    z[96]=n<T>(209,144)*z[3] + n<T>(1,12)*z[96] + z[6];
    z[96]=z[3]*z[96];
    z[85]=z[96] + z[93] + n<T>(1,12)*z[85] + z[95];
    z[85]=z[3]*z[85];
    z[92]=z[86] + z[92];
    z[93]=n<T>(19,4)*z[9];
    z[92]=z[92]*z[93];
    z[95]=z[86] - z[8];
    z[96]= - z[98] - z[95];
    z[96]=z[6]*z[96];
    z[98]=n<T>(1,6)*z[5] - n<T>(1,3)*z[86] - z[9];
    z[98]=z[5]*z[98];
    z[92]=77*z[96] + n<T>(19,4)*z[98] - n<T>(59,3)*z[88] + z[92];
    z[96]=n<T>(217,4)*z[86] - 59*z[8];
    z[93]=n<T>(19,12)*z[5] + n<T>(1,3)*z[96] + z[93];
    z[96]=z[4] - z[2];
    z[93]= - n<T>(77,2)*z[6] + n<T>(1,2)*z[93] + 9*z[96];
    z[93]=n<T>(41,6)*z[11] - n<T>(19,12)*z[3] + n<T>(1,2)*z[93] + n<T>(43,3)*z[7];
    z[93]=z[11]*z[93];
    z[96]=z[86]*z[4];
    z[92]=z[93] + n<T>(1,2)*z[92] - 9*z[96];
    z[93]=n<T>(43,3)*z[95] + n<T>(9,4)*z[7];
    z[93]=z[7]*z[93];
    z[98]=z[86] - z[5];
    z[101]= - n<T>(1,2)*z[3] - z[98];
    z[101]=z[3]*z[101];
    z[92]=n<T>(19,24)*z[101] + z[93] + n<T>(1,2)*z[92];
    z[92]=z[11]*z[92];
    z[93]=z[86]*z[8];
    z[101]= - z[7]*z[86];
    z[101]= - z[29] - z[93] + z[101];
    z[101]=z[17]*z[101];
    z[104]= - z[7] + z[95];
    z[104]=z[25]*z[104];
    z[105]=z[7] + z[11];
    z[105]=z[44]*z[105];
    z[101]=z[105] + z[101] + z[104];
    z[104]= - 23*z[9] + 151*z[86] - z[109];
    z[104]=z[9]*z[104];
    z[104]= - 17*z[88] + n<T>(1,12)*z[104];
    z[105]= - n<T>(65,24)*z[9] - n<T>(139,6)*z[86] + z[8];
    z[105]=n<T>(61,12)*z[2] + n<T>(1,2)*z[105] - z[103];
    z[105]=z[2]*z[105];
    z[106]= - n<T>(29,12)*z[5] - n<T>(43,24)*z[9] + n<T>(53,6)*z[86] - 2*z[8];
    z[106]=z[5]*z[106];
    z[104]=z[105] + n<T>(1,2)*z[104] + z[106];
    z[104]=z[2]*z[104];
    z[102]=n<T>(1,2)*z[102] - 52*z[5];
    z[102]=z[5]*z[102];
    z[105]=7*z[2] + 19*z[5] - 34*z[86] - n<T>(7,2)*z[8];
    z[105]=z[2]*z[105];
    z[102]=z[102] + z[105];
    z[105]= - n<T>(139,12)*z[9] - n<T>(23,2)*z[86] - z[8];
    z[105]=z[9]*z[105];
    z[88]= - n<T>(211,6)*z[88] + z[105];
    z[105]=n<T>(1313,72)*z[6] - n<T>(47,12)*z[2] + n<T>(107,12)*z[5] - n<T>(8,3)*z[9]
     - 
   n<T>(251,24)*z[8] - n<T>(33,2) + n<T>(28,3)*z[86];
    z[105]=z[6]*z[105];
    z[88]=z[105] + n<T>(1,2)*z[88] + n<T>(1,3)*z[102];
    z[88]=z[6]*z[88];
    z[102]=z[86]*z[10];
    z[105]=z[86]*z[3];
    z[106]=z[102] - z[105];
    z[108]=29*z[9];
    z[109]=z[86]*z[108];
    z[93]=n<T>(37,2)*z[93] + z[109];
    z[93]=n<T>(37,8)*z[29] + n<T>(1,4)*z[93] + 2*z[106];
    z[93]=z[13]*z[93];
    z[106]=z[5]*z[86];
    z[105]=z[29] + z[106] + z[105];
    z[105]=z[12]*z[105];
    z[98]=z[3] - z[98];
    z[98]=z[20]*z[98];
    z[98]= - z[75] + z[105] + z[98];
    z[96]=z[96] + z[29];
    z[105]=z[11]*z[86];
    z[105]=z[105] + z[96];
    z[105]=z[15]*z[105];
    z[106]=z[86] - z[4];
    z[109]= - z[11] + z[106];
    z[109]=z[21]*z[109];
    z[105]=z[105] + z[109] + z[72] - z[47];
    z[109]=z[100] - z[86];
    z[107]=z[107] + z[109];
    z[107]=z[18]*z[107];
    z[107]=z[107] - z[59];
    z[96]= - z[102] - z[96];
    z[96]=z[14]*z[96];
    z[102]=z[10] - z[106];
    z[102]=z[28]*z[102];
    z[96]=z[96] + z[102];
    z[102]=n<T>(91,3)*z[7] - z[10] + 27*z[6] + n<T>(7,3)*z[2] - z[103] + n<T>(10,3)*
    z[95] + z[9];
    z[102]=z[23]*z[102];
    z[91]= - z[91] - n<T>(5,2)*z[9] - 33 + n<T>(131,6)*z[6];
    z[103]=z[86]*z[91];
    z[103]= - n<T>(13,6)*z[29] + z[103];
    z[103]=z[16]*z[103];
    z[86]= - n<T>(13,6)*z[86] - z[91];
    z[86]=z[22]*z[86];
    z[91]=n<T>(37,2)*z[95] - z[108];
    z[106]=z[3] - z[10];
    z[91]=n<T>(1,4)*z[91] + 2*z[106];
    z[91]=z[27]*z[91];
    z[94]=z[94] + z[109];
    z[94]=z[19]*z[94];
    z[87]=z[100] + z[87];
    z[87]=z[24]*z[87];
    z[100]= - z[48] + z[70] - z[68];
    z[106]=z[3] + z[11];
    z[106]=z[43]*z[106];
    z[106]= - z[106] - z[77] + z[63] - z[49];
    z[108]= - z[76] + z[62] + z[55] + z[52];
    z[109]=z[67] - z[66];
    z[110]=z[10] + z[6];
    z[110]=5*z[9] + n<T>(107,3)*z[110];
    z[110]=z[45]*z[110];
    z[110]=z[110] - z[84];
    z[111]=n<T>(169,6)*z[38] + n<T>(89,6)*z[36] - n<T>(13,2)*z[35] + n<T>(5,2)*z[32]
     + 
   n<T>(413,144)*z[30];
    z[111]=i*z[111];
    z[95]=n<T>(5,2)*z[3] - z[2] + z[5] + n<T>(7,2)*z[9] - z[95];
    z[95]=z[26]*z[95];
    z[112]=z[9] + z[10];
    z[112]=z[46]*z[112];

    r +=  - n<T>(11,3)*z[31] + n<T>(26,3)*z[33] - n<T>(20,9)*z[34] - n<T>(29,4)*z[37]
       - n<T>(143,6)*z[39] - n<T>(13,2)*z[40] - 26*z[41] - n<T>(515,36)*z[42] + n<T>(13,24)*z[50]
     + n<T>(209,96)*z[51]
     + n<T>(19,16)*z[53]
     + n<T>(19,3)*z[54] - n<T>(7,4)
      *z[56] + n<T>(23,8)*z[57] - n<T>(19,96)*z[58] + n<T>(19,32)*z[60] + n<T>(15,8)*
      z[61] - n<T>(43,3)*z[64] - n<T>(59,8)*z[65] + n<T>(59,24)*z[69] - n<T>(77,8)*
      z[71] - n<T>(1,6)*z[73] - n<T>(17,3)*z[74] + n<T>(5,8)*z[78] - n<T>(71,12)*z[79]
       + n<T>(21,4)*z[80] + n<T>(3,4)*z[83] + z[85] + z[86] + 19*z[87] + z[88]
       + z[89] + n<T>(1,2)*z[90] + z[91] + z[92] + z[93] + n<T>(7,3)*z[94] + n<T>(19,12)*z[95]
     + n<T>(19,6)*z[96]
     + z[97]
     + n<T>(19,8)*z[98]
     + z[99] - n<T>(9,4)*
      z[100] + n<T>(59,6)*z[101] + z[102] + z[103] + z[104] + n<T>(9,2)*z[105]
       - n<T>(19,24)*z[106] + n<T>(1,3)*z[107] - n<T>(3,2)*z[108] + n<T>(40,3)*z[109]
       + n<T>(1,4)*z[110] + z[111] + n<T>(22,3)*z[112];
 
    return r;
}

template std::complex<double> qg_2lha_tf991(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf991(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
