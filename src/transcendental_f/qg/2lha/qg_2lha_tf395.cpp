#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf395(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[108];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[19];
    z[33]=c[21];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=c[31];
    z[39]=e[3];
    z[40]=e[6];
    z[41]=e[13];
    z[42]=e[14];
    z[43]=f[0];
    z[44]=f[1];
    z[45]=f[2];
    z[46]=f[3];
    z[47]=f[4];
    z[48]=f[5];
    z[49]=f[6];
    z[50]=f[7];
    z[51]=f[10];
    z[52]=f[11];
    z[53]=f[12];
    z[54]=f[13];
    z[55]=f[14];
    z[56]=f[16];
    z[57]=f[17];
    z[58]=f[18];
    z[59]=f[21];
    z[60]=f[23];
    z[61]=f[30];
    z[62]=f[31];
    z[63]=f[32];
    z[64]=f[33];
    z[65]=f[34];
    z[66]=f[36];
    z[67]=f[37];
    z[68]=f[39];
    z[69]=f[43];
    z[70]=f[50];
    z[71]=f[51];
    z[72]=f[52];
    z[73]=f[53];
    z[74]=f[54];
    z[75]=f[55];
    z[76]=f[56];
    z[77]=f[58];
    z[78]=f[72];
    z[79]=f[73];
    z[80]=n<T>(1,2)*z[5];
    z[81]=z[1]*i;
    z[82]=z[80] - z[81];
    z[82]=z[82]*z[5];
    z[83]=7*z[5];
    z[84]= - n<T>(5,2)*z[6] + z[81] + z[83];
    z[84]=z[6]*z[84];
    z[85]=2*z[7];
    z[86]=z[81] - z[5];
    z[87]=2*z[86];
    z[88]= - z[87] - z[7];
    z[88]=z[88]*z[85];
    z[89]=n<T>(1,2)*z[9];
    z[90]= - z[81] - z[89];
    z[91]=n<T>(25,4)*z[9];
    z[90]=z[90]*z[91];
    z[92]= - 49*z[81] + 41*z[5];
    z[85]=n<T>(3,4)*z[11] - 2*z[9] - z[85] - n<T>(31,8)*z[6] + z[3] + n<T>(1,8)*
    z[92] + z[8];
    z[85]=z[11]*z[85];
    z[92]=2*z[81];
    z[93]=z[92] - z[8];
    z[94]= - z[8]*z[93];
    z[95]=z[81] - z[8];
    z[96]=2*z[95] + z[3];
    z[96]=z[3]*z[96];
    z[91]= - n<T>(17,8)*z[4] - 2*z[6] + z[91];
    z[91]=z[4]*z[91];
    z[84]=z[85] + z[91] + z[90] + z[88] + n<T>(1,4)*z[84] + z[96] - n<T>(23,4)*
    z[82] + z[94];
    z[84]=z[11]*z[84];
    z[85]=13*z[8];
    z[88]= - z[81] + n<T>(1,2)*z[8];
    z[90]=z[5] - z[88];
    z[90]=z[90]*z[85];
    z[90]= - n<T>(35,3)*z[82] + z[90];
    z[91]=n<T>(5,3)*z[5];
    z[94]=n<T>(11,2)*z[81] - z[91];
    z[94]=7*z[94] - z[85];
    z[96]=n<T>(67,8)*z[6];
    z[97]=5*z[3];
    z[94]=z[96] + n<T>(1,2)*z[94] + z[97];
    z[98]=3*z[7];
    z[94]= - n<T>(199,24)*z[4] + n<T>(1,16)*z[9] + n<T>(37,6)*z[2] + n<T>(1,2)*z[94]
     + 
    z[98];
    z[94]=z[4]*z[94];
    z[99]=n<T>(1,2)*z[6];
    z[100]= - z[81] + z[99];
    z[96]=z[100]*z[96];
    z[100]= - z[81] - z[3];
    z[100]=2*z[100] + z[7];
    z[100]=z[100]*z[98];
    z[101]=2*z[5];
    z[102]=z[2] - 37*z[81] - z[101];
    z[102]=z[2]*z[102];
    z[103]=z[89] - z[81] - 51*z[6];
    z[103]=z[9]*z[103];
    z[104]=5*z[81];
    z[105]= - z[104] + 3*z[3];
    z[105]=z[3]*z[105];
    z[90]=z[94] + n<T>(1,8)*z[103] + n<T>(1,3)*z[102] + z[100] + z[96] + n<T>(1,2)*
    z[90] + z[105];
    z[90]=z[4]*z[90];
    z[94]=3*z[5];
    z[96]=3*z[6];
    z[98]= - z[98] - z[96] - z[85] + 29*z[81] + z[94];
    z[98]=z[7]*z[98];
    z[100]=4*z[5];
    z[102]= - 3*z[81] + z[100];
    z[102]=z[5]*z[102];
    z[103]=n<T>(13,3)*z[8];
    z[93]= - z[5] - z[93];
    z[93]=z[93]*z[103];
    z[83]=n<T>(19,2)*z[3] - 4*z[8] + z[81] - z[83];
    z[105]=n<T>(1,3)*z[3];
    z[83]=z[83]*z[105];
    z[94]= - 11*z[81] + z[94];
    z[85]=7*z[94] + z[85];
    z[85]=n<T>(1,2)*z[85] - z[96];
    z[85]=z[85]*z[99];
    z[94]= - n<T>(35,6)*z[2] - n<T>(19,8)*z[7] + n<T>(43,8)*z[6] + n<T>(5,3)*z[3]
     + 15*
    z[81] + n<T>(13,6)*z[8];
    z[94]=z[2]*z[94];
    z[83]=z[94] + n<T>(1,4)*z[98] + z[85] + z[83] + z[102] + z[93];
    z[83]=z[2]*z[83];
    z[85]=13*z[5];
    z[93]=n<T>(51,4)*z[81] - z[85];
    z[94]=n<T>(4,3)*z[8];
    z[93]=n<T>(23,72)*z[9] - n<T>(4,3)*z[2] + n<T>(67,16)*z[6] + z[97] + n<T>(1,4)*z[93]
    + z[94];
    z[93]=z[9]*z[93];
    z[96]= - z[92] - z[5];
    z[96]=2*z[96] - z[8];
    z[96]=z[8]*z[96];
    z[98]=z[86] - z[8];
    z[102]= - z[3] - z[98];
    z[102]=2*z[102] - z[2];
    z[102]=z[2]*z[102];
    z[96]=z[96] + z[102];
    z[102]=3*z[86];
    z[94]= - n<T>(3,2)*z[3] - z[102] + z[94];
    z[94]=z[3]*z[94];
    z[99]=z[81] + z[99];
    z[99]=z[6]*z[99];
    z[93]=z[93] + n<T>(51,8)*z[99] + z[94] - n<T>(13,2)*z[82] + n<T>(2,3)*z[96];
    z[93]=z[9]*z[93];
    z[94]=25*z[5];
    z[96]=n<T>(523,4)*z[81] - z[94];
    z[99]=n<T>(141,4)*z[8];
    z[96]=n<T>(1,3)*z[96] - z[99];
    z[106]=z[4] - z[2];
    z[107]=n<T>(7,2)*z[9];
    z[96]=n<T>(299,12)*z[10] - z[107] + n<T>(97,4)*z[7] - n<T>(311,8)*z[6] + n<T>(1,2)*
    z[96] + n<T>(23,3)*z[3] + 11*z[106];
    z[96]=z[10]*z[96];
    z[88]= - z[88]*z[99];
    z[88]=z[96] - n<T>(25,3)*z[82] + z[88];
    z[87]= - z[87] - z[3];
    z[87]=z[87]*z[105];
    z[96]= - z[100] + 4*z[3];
    z[99]= - n<T>(271,16)*z[6] + n<T>(271,8)*z[8] - n<T>(239,8)*z[81] + z[96];
    z[99]=z[6]*z[99];
    z[96]= - 8*z[7] - n<T>(65,4)*z[8] + n<T>(49,4)*z[81] - z[96];
    z[96]=z[7]*z[96];
    z[89]= - z[89] - z[86];
    z[89]=z[89]*z[107];
    z[100]=z[81]*z[4];
    z[87]= - 11*z[100] + z[89] + z[96] + z[99] + z[87] + n<T>(1,2)*z[88];
    z[87]=z[10]*z[87];
    z[88]= - n<T>(305,12)*z[8] + n<T>(305,6)*z[81] - z[85];
    z[88]=z[8]*z[88];
    z[89]= - 217 + n<T>(47,2)*z[81];
    z[89]= - n<T>(917,24)*z[8] + n<T>(1,3)*z[89] + z[94];
    z[89]=n<T>(359,8)*z[6] + n<T>(1,2)*z[89] - 11*z[3];
    z[89]=z[6]*z[89];
    z[94]=n<T>(39,2)*z[81] - 14*z[5];
    z[94]=z[5]*z[94];
    z[96]= - z[102] - z[3];
    z[96]=z[3]*z[96];
    z[88]=n<T>(3,2)*z[41] + z[89] + 2*z[96] + z[94] + n<T>(1,4)*z[88];
    z[88]=z[6]*z[88];
    z[89]= - 105*z[81] + n<T>(89,2)*z[5];
    z[80]=z[89]*z[80];
    z[85]= - n<T>(239,3)*z[81] + z[85];
    z[85]=n<T>(1,2)*z[85] + n<T>(91,3)*z[8];
    z[85]=z[8]*z[85];
    z[80]=z[80] + z[85];
    z[85]=13 + 14*z[81];
    z[85]= - n<T>(68,3)*z[6] + 12*z[3] + z[103] + n<T>(4,3)*z[85] - n<T>(89,4)*z[5];
    z[85]=z[6]*z[85];
    z[89]=n<T>(143,6)*z[8] - n<T>(59,3)*z[81] + n<T>(57,2)*z[5];
    z[89]=n<T>(311,24)*z[7] - n<T>(185,8)*z[6] + n<T>(1,4)*z[89] + z[97];
    z[89]=z[7]*z[89];
    z[94]= - 6*z[5] + n<T>(40,3) + 9*z[81];
    z[94]=2*z[94] + z[3];
    z[94]=z[3]*z[94];
    z[80]=z[89] + z[85] + n<T>(1,2)*z[80] + z[94];
    z[80]=z[7]*z[80];
    z[85]=n<T>(1,3)*z[8];
    z[89]= - n<T>(85,9)*z[3] + z[85] + z[91] - n<T>(40,3) - n<T>(7,2)*z[81];
    z[89]=z[3]*z[89];
    z[91]= - z[104] + z[101];
    z[91]=2*z[91] + 5*z[8];
    z[85]=z[91]*z[85];
    z[91]= - z[104] + n<T>(16,3)*z[5];
    z[91]=z[5]*z[91];
    z[85]=z[89] + z[91] + z[85];
    z[85]=z[3]*z[85];
    z[89]=z[100] + z[29];
    z[91]=z[10]*z[81];
    z[91]=z[91] + z[89];
    z[91]=z[14]*z[91];
    z[94]=z[81] - z[4];
    z[96]= - z[10] + z[94];
    z[96]=z[21]*z[96];
    z[91]= - z[43] + z[91] + z[96];
    z[96]=z[81]*z[8];
    z[96]=z[96] + z[29];
    z[99]=z[9]*z[81];
    z[99]=z[99] + z[96];
    z[99]=z[17]*z[99];
    z[100]= - z[9] + z[95];
    z[100]=z[27]*z[100];
    z[99]=z[99] + z[100];
    z[100]= - z[7]*z[81];
    z[96]=z[100] - z[96];
    z[96]=z[16]*z[96];
    z[95]= - z[7] + z[95];
    z[95]=z[25]*z[95];
    z[95]=z[96] + z[95];
    z[96]=z[11]*z[81];
    z[89]=z[96] + z[89];
    z[89]=z[13]*z[89];
    z[94]= - z[11] + z[94];
    z[94]=z[28]*z[94];
    z[89]=z[89] + z[94];
    z[94]= - z[5] - z[3];
    z[94]=z[81]*z[94];
    z[94]= - z[29] + z[94];
    z[94]=z[12]*z[94];
    z[86]= - z[3] + z[86];
    z[86]=z[20]*z[86];
    z[86]=z[94] + z[86];
    z[94]= - 2*z[8] + z[92] - n<T>(13,12)*z[5];
    z[94]=z[8]*z[94];
    z[82]= - n<T>(13,6)*z[82] + z[94];
    z[82]=z[8]*z[82];
    z[94]= - n<T>(3,2)*z[5] - 11 + n<T>(13,2)*z[6];
    z[96]=z[81]*z[94];
    z[96]= - n<T>(3,2)*z[29] + z[96];
    z[96]=z[15]*z[96];
    z[94]= - n<T>(3,2)*z[81] - z[94];
    z[94]=z[22]*z[94];
    z[100]= - z[73] + z[67] + z[65] - z[49];
    z[97]= - 5*z[10] - 16 - z[97];
    z[97]=z[39]*z[97];
    z[97]=z[97] - z[51];
    z[101]= - 55*z[36] - n<T>(37,2)*z[34] + 7*z[33] - n<T>(203,72)*z[30];
    z[101]=i*z[101];
    z[102]=n<T>(5,6)*z[5] + 11 + n<T>(1,2)*z[81];
    z[102]=z[102]*npow(z[5],2);
    z[103]=257*z[5] + n<T>(71,2)*z[8];
    z[103]=139*z[6] + n<T>(1,4)*z[103] - 107*z[3];
    z[103]=n<T>(1,9)*z[103] - n<T>(85,2)*z[7];
    z[103]=n<T>(77,24)*z[11] + n<T>(269,18)*z[10] + n<T>(1013,144)*z[4] + n<T>(32,9)*
    z[9] + n<T>(1,2)*z[103] + n<T>(8,3)*z[2];
    z[103]=z[29]*z[103];
    z[81]= - 11*z[8] - 53*z[81] + 69*z[5];
    z[81]=12*z[10] - n<T>(5,2)*z[2] + n<T>(563,12)*z[7] + n<T>(1013,12)*z[6] + n<T>(1,2)
   *z[81] - 16*z[3];
    z[81]=z[23]*z[81];
    z[98]=z[98] + z[2];
    z[98]= - 31*z[3] + 8*z[98];
    z[98]=n<T>(1,3)*z[98] - 13*z[9];
    z[98]=z[26]*z[98];
    z[92]=z[92] - z[2];
    z[104]= - z[4] + z[92];
    z[104]=z[18]*z[104];
    z[105]= - z[5] + z[92];
    z[105]=z[19]*z[105];
    z[92]= - z[8] + z[92];
    z[92]=z[24]*z[92];
    z[106]=z[7] + z[10];
    z[106]=z[40]*z[106];
    z[107]= - z[9] - z[11];
    z[107]=z[42]*z[107];

    r +=  - n<T>(20,3)*z[31] + n<T>(7,3)*z[32] + n<T>(35,6)*z[35] + 24*z[37] + n<T>(81,2)*z[38]
     + n<T>(11,2)*z[44] - n<T>(10,3)*z[45] - n<T>(35,12)*z[46] - n<T>(67,12)*
      z[47] - n<T>(11,6)*z[48] - n<T>(32,3)*z[50] - n<T>(13,12)*z[52] - n<T>(11,12)*
      z[53] - n<T>(2,3)*z[54] + n<T>(7,2)*z[55] - n<T>(23,4)*z[56] - n<T>(13,4)*z[57]
       - n<T>(11,3)*z[58] - z[59] + n<T>(4,3)*z[60] - n<T>(81,4)*z[61] - n<T>(77,48)*
      z[62] - n<T>(3,8)*z[63] - 88*z[64] + n<T>(141,16)*z[66] - n<T>(327,16)*z[68]
       - n<T>(19,4)*z[69] + n<T>(25,4)*z[70] + n<T>(67,16)*z[71] - n<T>(41,8)*z[72]
     + 2
      *z[74] - n<T>(1,16)*z[75] + z[76] + n<T>(51,16)*z[77] - z[78] + z[79] + 
      z[80] + z[81] + z[82] + z[83] + z[84] + z[85] + n<T>(14,3)*z[86] + 
      z[87] + z[88] + n<T>(17,4)*z[89] + z[90] + 11*z[91] + n<T>(13,3)*z[92] + 
      z[93] + z[94] + n<T>(125,12)*z[95] + z[96] + n<T>(5,3)*z[97] + z[98] + 4*
      z[99] + 3*z[100] + z[101] + n<T>(1,2)*z[102] + z[103] + n<T>(35,3)*z[104]
       + 8*z[105] + n<T>(129,4)*z[106] + n<T>(9,4)*z[107];
 
    return r;
}

template std::complex<double> qg_2lha_tf395(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf395(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
