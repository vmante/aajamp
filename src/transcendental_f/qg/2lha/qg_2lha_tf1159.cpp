#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1159(
  const std::array<std::complex<T>,24>& e
) {
    return e[15];
}

template std::complex<double> qg_2lha_tf1159(
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1159(
  const std::array<std::complex<dd_real>,24>& e
);
#endif
