#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf207(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[111];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[18];
    z[34]=c[19];
    z[35]=c[21];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[29];
    z[41]=c[31];
    z[42]=e[3];
    z[43]=e[6];
    z[44]=e[14];
    z[45]=f[0];
    z[46]=f[1];
    z[47]=f[2];
    z[48]=f[3];
    z[49]=f[4];
    z[50]=f[5];
    z[51]=f[6];
    z[52]=f[7];
    z[53]=f[10];
    z[54]=f[11];
    z[55]=f[12];
    z[56]=f[13];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[18];
    z[60]=f[23];
    z[61]=f[30];
    z[62]=f[31];
    z[63]=f[32];
    z[64]=f[33];
    z[65]=f[34];
    z[66]=f[35];
    z[67]=f[36];
    z[68]=f[37];
    z[69]=f[39];
    z[70]=f[41];
    z[71]=f[43];
    z[72]=f[50];
    z[73]=f[51];
    z[74]=f[52];
    z[75]=f[53];
    z[76]=f[55];
    z[77]=f[56];
    z[78]=f[58];
    z[79]=f[60];
    z[80]=f[85];
    z[81]=f[86];
    z[82]=4*z[6];
    z[83]=n<T>(7,6)*z[8];
    z[84]=n<T>(11,4)*z[9];
    z[85]=n<T>(11,3)*z[3];
    z[86]=n<T>(1,2)*z[4];
    z[87]=n<T>(1,2)*z[2];
    z[88]=z[1]*i;
    z[89]=z[88] + n<T>(11,3)*z[5];
    z[89]=2*z[11] + n<T>(17,6)*z[7] + z[86] - z[85] + z[84] - z[87] - z[83]
    + n<T>(1,4)*z[89] - z[82];
    z[89]=z[11]*z[89];
    z[90]=2*z[88];
    z[91]= - z[90] - z[6];
    z[82]=z[91]*z[82];
    z[83]= - z[83] + n<T>(7,3)*z[88] + 8*z[6];
    z[83]=z[8]*z[83];
    z[91]=z[88] - z[5];
    z[92]= - n<T>(1,2)*z[3] - z[91];
    z[85]=z[92]*z[85];
    z[92]= - z[88] + n<T>(1,2)*z[5];
    z[93]=z[92]*z[5];
    z[94]=n<T>(1,2)*z[9] + z[91];
    z[94]=z[9]*z[94];
    z[95]= - z[4]*z[88];
    z[96]=z[88] - z[8];
    z[97]=n<T>(17,3)*z[96] + n<T>(1,2)*z[7];
    z[97]=z[7]*z[97];
    z[82]=z[89] + z[97] + z[95] + z[85] + n<T>(11,2)*z[94] + z[83] + n<T>(11,6)*
    z[93] + z[82];
    z[82]=z[11]*z[82];
    z[83]=n<T>(1,4)*z[5];
    z[85]= - 26*z[88] - z[83];
    z[85]=n<T>(1,24)*z[2] + n<T>(1,3)*z[85] + z[6];
    z[85]=z[2]*z[85];
    z[89]=2*z[6];
    z[94]=n<T>(137,24)*z[9] - z[2] + z[89] - n<T>(1,2)*z[88] + z[5];
    z[94]=z[10]*z[94];
    z[95]=97*z[88] - n<T>(109,2)*z[5];
    z[95]=z[5]*z[95];
    z[97]=n<T>(101,32)*z[6] - n<T>(197,16)*z[88] - z[5];
    z[97]=z[6]*z[97];
    z[98]= - z[88] + n<T>(1,2)*z[8];
    z[99]=z[5] - z[98];
    z[99]=z[8]*z[99];
    z[100]=n<T>(125,6)*z[9] - n<T>(125,3)*z[88] - 133*z[6];
    z[100]=z[9]*z[100];
    z[101]=53*z[88];
    z[102]=z[101] - 97*z[5];
    z[102]= - 37*z[8] + n<T>(1,3)*z[102] + n<T>(197,2)*z[6];
    z[102]= - n<T>(157,48)*z[4] - n<T>(173,48)*z[10] + n<T>(125,96)*z[9] + n<T>(1,16)*
    z[102] + n<T>(13,3)*z[2];
    z[102]=z[4]*z[102];
    z[85]=z[102] + z[94] + n<T>(1,16)*z[100] + z[85] + n<T>(37,8)*z[99] + n<T>(1,24)
   *z[95] + z[97];
    z[85]=z[4]*z[85];
    z[94]=6*z[5];
    z[95]=n<T>(5,2)*z[8];
    z[97]=n<T>(1,2)*z[10];
    z[99]=n<T>(53,9)*z[7] - z[97] + n<T>(16,3)*z[2] + z[95] - n<T>(97,6)*z[6] + n<T>(17,6)*z[88]
     + z[94];
    z[99]=z[7]*z[99];
    z[100]=z[8] + z[5];
    z[102]=16*z[6];
    z[100]=n<T>(13,2)*z[2] + z[102] + 19*z[88] - 16*z[100];
    z[103]=n<T>(1,3)*z[2];
    z[100]=z[100]*z[103];
    z[86]= - z[88] + z[86];
    z[86]=z[4]*z[86];
    z[104]= - z[90] + z[5];
    z[104]=z[5]*z[104];
    z[94]= - z[94] - n<T>(67,6)*z[6];
    z[94]=z[6]*z[94];
    z[105]=n<T>(65,6)*z[8] + n<T>(2,3)*z[6] - 5*z[88] + n<T>(16,3)*z[5];
    z[105]=z[8]*z[105];
    z[97]=z[6] - z[97];
    z[97]=z[10]*z[97];
    z[86]=z[99] + z[86] + z[97] + z[100] + z[105] + 3*z[104] + z[94];
    z[86]=z[7]*z[86];
    z[94]=8*z[2];
    z[97]=5*z[5];
    z[99]= - 5 + z[88];
    z[95]=n<T>(23,3)*z[3] - 13*z[9] + z[94] + z[95] + n<T>(1,2)*z[99] - z[97];
    z[95]=z[3]*z[95];
    z[99]=n<T>(13,2)*z[8] - 13*z[88] + 4*z[5];
    z[99]=z[8]*z[99];
    z[95]=z[99] + z[95];
    z[99]=z[6] - z[5];
    z[100]=3*z[8];
    z[104]=n<T>(1,4)*z[10] + n<T>(5,2)*z[9] - z[100] + n<T>(5,3) + n<T>(13,2)*z[88]
     + n<T>(7,2)*z[99];
    z[104]=z[10]*z[104];
    z[105]=z[91] - z[8];
    z[105]=z[2] + 2*z[105];
    z[106]=n<T>(2,3)*z[2];
    z[105]=z[105]*z[106];
    z[106]=z[2] - z[8];
    z[107]= - n<T>(4,3)*z[106];
    z[108]= - n<T>(7,6)*z[9] - n<T>(26,3)*z[91] - n<T>(5,2)*z[6] + z[107];
    z[108]=z[9]*z[108];
    z[109]=z[88] + z[5];
    z[109]=z[5]*z[109];
    z[110]=11*z[91] + n<T>(9,2)*z[6];
    z[110]=z[6]*z[110];
    z[95]=z[104] + z[108] + z[105] + n<T>(2,3)*z[109] + n<T>(1,2)*z[110] + n<T>(1,3)
   *z[95];
    z[95]=z[3]*z[95];
    z[104]=n<T>(11,2)*z[5];
    z[108]= - 23*z[88] + z[104];
    z[108]=z[5]*z[108];
    z[109]= - n<T>(57,4)*z[6] + n<T>(13,3) - z[91];
    z[109]=z[6]*z[109];
    z[108]=z[108] + z[109];
    z[89]=n<T>(5,4)*z[2] - z[89] - n<T>(7,2)*z[88] + 2*z[5];
    z[89]=z[2]*z[89];
    z[109]= - n<T>(77,12)*z[88] - z[97];
    z[109]= - n<T>(245,48)*z[9] + n<T>(1,2)*z[109] + 5*z[6];
    z[109]=z[9]*z[109];
    z[110]=n<T>(63,2)*z[6] - 7*z[88] - 13*z[5];
    z[100]=n<T>(1,4)*z[110] + z[100];
    z[84]= - n<T>(71,48)*z[10] - z[84] + n<T>(1,2)*z[100] + z[2];
    z[84]=z[10]*z[84];
    z[100]=z[8]*z[98];
    z[84]=z[84] + z[109] + z[89] + n<T>(1,4)*z[108] + z[100];
    z[84]=z[10]*z[84];
    z[89]= - 31*z[88] + n<T>(67,2)*z[5];
    z[83]=z[89]*z[83];
    z[89]=37*z[88];
    z[100]= - z[89] - n<T>(43,2)*z[5];
    z[100]=n<T>(37,4)*z[8] + n<T>(1,2)*z[100] + z[102];
    z[100]=z[8]*z[100];
    z[102]= - n<T>(67,2)*z[6] - n<T>(127,4)*z[88] + 19*z[5];
    z[102]=z[6]*z[102];
    z[108]= - n<T>(121,8)*z[2] + n<T>(43,8)*z[8] - n<T>(25,8)*z[6] + n<T>(311,8)*z[88]
    + z[5];
    z[108]=z[2]*z[108];
    z[83]=z[108] + z[100] + z[83] + z[102];
    z[83]=z[83]*z[103];
    z[97]=n<T>(53,16)*z[6] + n<T>(93,8)*z[88] + z[97];
    z[97]=z[6]*z[97];
    z[93]= - n<T>(19,3)*z[93] + z[97];
    z[97]=n<T>(205,8)*z[6] + n<T>(9,8)*z[88] - n<T>(19,3)*z[5];
    z[97]= - n<T>(503,144)*z[9] + n<T>(1,4)*z[97] + z[107];
    z[97]=z[9]*z[97];
    z[90]= - z[90] - z[5];
    z[90]=2*z[90] - z[8];
    z[90]=z[8]*z[90];
    z[90]=z[97] - z[105] + n<T>(1,2)*z[93] + n<T>(2,3)*z[90];
    z[90]=z[9]*z[90];
    z[93]=z[5] + z[6];
    z[93]=z[88]*z[93];
    z[93]=z[30] + z[93];
    z[93]=z[15]*z[93];
    z[92]= - z[87] - z[92];
    z[92]=z[19]*z[92];
    z[97]= - z[6] + z[91];
    z[97]=z[22]*z[97];
    z[92]=z[97] + z[93] + z[92];
    z[93]=8*z[5];
    z[94]= - 11*z[3] - 19*z[9] + z[94] - z[93] - 5 + 8*z[96];
    z[94]=z[26]*z[94];
    z[94]= - z[33] + z[94] - z[59];
    z[97]=z[8] + z[9];
    z[97]=z[88]*z[97];
    z[97]=z[30] + z[97];
    z[97]=z[17]*z[97];
    z[100]= - z[9] + z[96];
    z[100]=z[27]*z[100];
    z[97]= - z[69] + z[97] + z[100];
    z[100]= - z[8] - z[7];
    z[100]=z[88]*z[100];
    z[100]= - z[30] + z[100];
    z[100]=z[16]*z[100];
    z[96]= - z[7] + z[96];
    z[96]=z[25]*z[96];
    z[96]=z[100] + z[96];
    z[100]=z[4] + z[10];
    z[100]=z[88]*z[100];
    z[100]=z[30] + z[100];
    z[100]=z[13]*z[100];
    z[102]=z[88] - z[4];
    z[103]= - z[10] + z[102];
    z[103]=z[28]*z[103];
    z[100]=z[100] + z[103];
    z[103]= - z[5] - z[3];
    z[103]=z[88]*z[103];
    z[103]= - z[30] + z[103];
    z[103]=z[12]*z[103];
    z[105]= - z[3] + z[91];
    z[105]=z[20]*z[105];
    z[103]= - z[79] + z[103] + z[105];
    z[104]= - 83*z[88] - z[104];
    z[104]=z[5]*z[104];
    z[105]=n<T>(1495,12)*z[6] + n<T>(199,2)*z[5] - 23 + n<T>(389,8)*z[88];
    z[105]=z[6]*z[105];
    z[104]=z[104] + z[105];
    z[104]=z[6]*z[104];
    z[101]=z[101] - 49*z[5];
    z[101]=z[101]*npow(z[5],2);
    z[101]=z[101] + z[104];
    z[93]=29*z[88] - z[93];
    z[93]=2*z[93] - 13*z[6];
    z[93]=z[6]*z[93];
    z[89]=z[89] - n<T>(25,2)*z[5];
    z[89]=z[5]*z[89];
    z[89]=n<T>(1,8)*z[89] + z[93];
    z[93]=9*z[88] - n<T>(37,24)*z[5];
    z[93]= - n<T>(17,6)*z[8] + n<T>(1,2)*z[93] - n<T>(29,3)*z[6];
    z[93]=z[8]*z[93];
    z[89]=n<T>(1,3)*z[89] + z[93];
    z[89]=z[8]*z[89];
    z[93]= - n<T>(103,12)*z[4] - z[10] - n<T>(91,12)*z[2] + n<T>(103,6)*z[88] + 
    z[99];
    z[93]=z[18]*z[93];
    z[99]=z[106] + z[91];
    z[99]=n<T>(185,6)*z[7] + n<T>(83,2)*z[6] - n<T>(32,3)*z[99];
    z[99]=z[23]*z[99];
    z[104]=z[11] + z[4];
    z[88]=z[88]*z[104];
    z[88]=z[30] + z[88];
    z[88]=z[14]*z[88];
    z[87]= - z[87] - z[98];
    z[87]=z[24]*z[87];
    z[98]=z[40] + z[81] + z[80];
    z[104]=z[68] - z[46];
    z[105]=z[78] + z[73];
    z[106]=z[3] + z[11];
    z[106]=z[42]*z[106];
    z[106]=z[106] + z[47];
    z[107]= - n<T>(104,3)*z[38] - n<T>(365,24)*z[36] + n<T>(9,2)*z[35] - n<T>(1697,288)*
    z[31];
    z[107]=i*z[107];
    z[108]=n<T>(395,18)*z[8] + n<T>(887,18)*z[5] + 9*z[6];
    z[108]= - n<T>(589,72)*z[10] + n<T>(43,36)*z[9] + n<T>(1,4)*z[108] + n<T>(71,9)*z[2]
   ;
    z[108]=n<T>(5,12)*z[11] - n<T>(23,9)*z[7] - n<T>(253,288)*z[4] + n<T>(1,2)*z[108]
     - 
   n<T>(31,9)*z[3];
    z[108]=z[30]*z[108];
    z[91]= - z[3] - n<T>(125,8)*z[10] - 7*z[9] - 2*z[91] - n<T>(47,4)*z[6];
    z[91]=z[29]*z[91];
    z[102]= - z[11] + z[102];
    z[102]=z[21]*z[102];
    z[109]=z[7] + z[11];
    z[109]=z[43]*z[109];
    z[110]= - z[9] - z[10];
    z[110]=z[44]*z[110];

    r +=  - n<T>(47,24)*z[32] - n<T>(95,72)*z[34] + n<T>(121,24)*z[37] + n<T>(169,12)*
      z[39] + n<T>(2587,72)*z[41] - 5*z[45] - n<T>(85,48)*z[48] - n<T>(35,12)*z[49]
       - n<T>(43,12)*z[50] - n<T>(7,2)*z[51] - n<T>(32,3)*z[52] - n<T>(71,6)*z[53]
     - n<T>(37,48)*z[54]
     - n<T>(11,12)*z[55]
     + n<T>(1,6)*z[56]
     + n<T>(11,4)*z[57] - n<T>(53,16)
      *z[58] + n<T>(4,3)*z[60] - n<T>(17,3)*z[61] - n<T>(31,2)*z[62] - n<T>(34,3)*z[63]
       - n<T>(128,3)*z[64] + n<T>(11,8)*z[65] - 6*z[66] + n<T>(7,6)*z[67] + z[70]
       + n<T>(16,3)*z[71] + n<T>(131,24)*z[72] + n<T>(17,8)*z[74] + n<T>(25,4)*z[75]
     - 
      n<T>(125,96)*z[76] + n<T>(5,2)*z[77] + z[82] + z[83] + z[84] + z[85] + 
      z[86] + n<T>(31,6)*z[87] + z[88] + z[89] + z[90] + z[91] + n<T>(47,6)*
      z[92] + z[93] + n<T>(1,3)*z[94] + z[95] + n<T>(50,3)*z[96] + 4*z[97] + n<T>(1,4)*z[98]
     + z[99]
     + n<T>(185,24)*z[100]
     + n<T>(1,12)*z[101]
     + z[102]
     + 2*
      z[103] - n<T>(1,2)*z[104] + n<T>(205,32)*z[105] + n<T>(11,3)*z[106] + z[107]
       + z[108] + n<T>(14,3)*z[109] + n<T>(125,24)*z[110];
 
    return r;
}

template std::complex<double> qg_2lha_tf207(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf207(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
