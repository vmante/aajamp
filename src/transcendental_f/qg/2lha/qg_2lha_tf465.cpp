#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf465(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[13];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=c[13];
    z[4]=c[20];
    z[5]=c[23];
    z[6]=f[42];
    z[7]=f[62];
    z[8]=f[77];
    z[9]=f[79];
    z[10]=f[81];
    z[11]=z[1]*i;
    z[11]= - z[10] - n<T>(73,540)*z[11] + z[2] + z[9] - z[8];
    z[12]= - n<T>(3,40)*z[5] + n<T>(3,2)*z[4] + n<T>(18,5)*z[3];
    z[12]=i*z[12];

    r += z[6] - n<T>(1,4)*z[7] + n<T>(1,2)*z[11] + z[12];
 
    return r;
}

template std::complex<double> qg_2lha_tf465(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf465(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
