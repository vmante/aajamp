#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf982(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[118];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[9];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[2];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[18];
    z[33]=c[19];
    z[34]=c[21];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[29];
    z[40]=c[30];
    z[41]=c[31];
    z[42]=e[3];
    z[43]=e[6];
    z[44]=e[13];
    z[45]=e[14];
    z[46]=f[0];
    z[47]=f[1];
    z[48]=f[2];
    z[49]=f[3];
    z[50]=f[4];
    z[51]=f[5];
    z[52]=f[6];
    z[53]=f[7];
    z[54]=f[11];
    z[55]=f[12];
    z[56]=f[13];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[18];
    z[60]=f[23];
    z[61]=f[31];
    z[62]=f[32];
    z[63]=f[33];
    z[64]=f[34];
    z[65]=f[35];
    z[66]=f[36];
    z[67]=f[37];
    z[68]=f[39];
    z[69]=f[41];
    z[70]=f[43];
    z[71]=f[50];
    z[72]=f[51];
    z[73]=f[52];
    z[74]=f[53];
    z[75]=f[55];
    z[76]=f[56];
    z[77]=f[58];
    z[78]=f[85];
    z[79]=f[86];
    z[80]=n<T>(9,8)*z[12];
    z[81]=n<T>(1,2)*z[10];
    z[82]=n<T>(1,2)*z[9];
    z[83]=z[80] - n<T>(11,6)*z[2] + z[81] - z[82];
    z[84]=z[17] + n<T>(13,3)*z[16];
    z[85]=n<T>(2,3)*z[6];
    z[86]=2*z[7];
    z[87]=n<T>(5,6)*z[3];
    z[88]= - z[86] + n<T>(2,3)*z[8] + z[87] + z[85] - z[83] - z[84];
    z[88]=z[7]*z[88];
    z[89]=z[11] + n<T>(10,3)*z[15];
    z[90]=n<T>(8,3)*z[6];
    z[91]=n<T>(7,4)*z[9];
    z[92]=n<T>(1,6)*z[3];
    z[93]=n<T>(8,3)*z[4] - n<T>(15,4)*z[5] - n<T>(17,24)*z[7] - n<T>(35,12)*z[2]
     - n<T>(5,4)
   *z[8] + z[91] - z[92] + z[80] - z[90] - n<T>(1,12)*z[10] + z[89];
    z[93]=z[4]*z[93];
    z[94]=4*z[23];
    z[95]=z[94] + n<T>(2,3)*z[26];
    z[96]=n<T>(13,3)*z[25];
    z[97]=z[27] + z[95] - z[96];
    z[98]=n<T>(1,3)*z[8];
    z[99]=z[87] - z[98];
    z[100]= - n<T>(35,24)*z[2] + z[82] + 3*z[6] - z[99];
    z[100]=z[2]*z[100];
    z[101]=n<T>(3,4)*z[9];
    z[102]=n<T>(17,96)*z[5] + n<T>(25,6)*z[7] + n<T>(9,4)*z[2] + n<T>(41,16)*z[8]
     - 
    z[101] + z[3] - n<T>(49,16)*z[12] - n<T>(2,3)*z[15] + z[81];
    z[102]=z[5]*z[102];
    z[103]= - z[20] + n<T>(10,3)*z[22];
    z[104]=z[21] + n<T>(15,8)*z[28];
    z[105]=z[14] - n<T>(7,24)*z[10];
    z[105]=z[10]*z[105];
    z[106]= - 13*z[16] + 7*z[6];
    z[106]=z[6]*z[106];
    z[107]=z[14] + n<T>(15,8)*z[13];
    z[108]= - n<T>(29,16)*z[12] - z[6] - z[10] + z[107];
    z[108]=z[12]*z[108];
    z[109]=n<T>(1,6)*z[10];
    z[110]= - n<T>(5,12)*z[3] + z[11] - z[109];
    z[110]=z[3]*z[110];
    z[111]=n<T>(15,4)*z[13] - z[12];
    z[111]=n<T>(9,8)*z[9] + n<T>(1,2)*z[111] - z[3];
    z[111]=z[9]*z[111];
    z[112]=n<T>(1,4)*z[10];
    z[113]= - n<T>(23,32)*z[8] - n<T>(19,8)*z[9] + z[3] - n<T>(3,16)*z[12] - z[17]
    + z[112];
    z[113]=z[8]*z[113];
    z[88]=z[93] + z[102] + z[88] + z[100] + z[113] + z[111] + z[110] + 
    z[108] + n<T>(1,3)*z[106] + z[105] + n<T>(9,2)*z[18] + n<T>(11,2)*z[15] - n<T>(5,6)*
    z[19] - n<T>(17,6)*z[24] - z[97] + z[104] + z[103];
    z[88]=z[1]*z[88];
    z[93]=z[37] + n<T>(47,4)*z[35];
    z[88]=z[88] - n<T>(337,288)*z[30] + n<T>(1,6)*z[93] - z[34];
    z[88]=i*z[88];
    z[93]=n<T>(3,2)*z[12];
    z[100]=n<T>(1,2)*z[2];
    z[102]=n<T>(1,2)*z[3];
    z[86]=n<T>(9,8)*z[5] - z[86] - z[100] - n<T>(1,2)*z[8] + z[101] - z[102] - 
    z[90] + z[93];
    z[86]=z[5]*z[86];
    z[90]=2*z[6];
    z[101]=z[90] + z[98];
    z[105]=n<T>(9,4)*z[12];
    z[87]= - n<T>(1,12)*z[2] - z[82] + z[87] + z[105] - z[101];
    z[87]=z[2]*z[87];
    z[106]=n<T>(5,8)*z[8];
    z[108]=n<T>(5,24)*z[2];
    z[110]=n<T>(17,48)*z[7];
    z[109]= - 11 + z[109];
    z[109]= - 2*z[4] + n<T>(65,24)*z[5] + z[110] + z[108] + z[106] + n<T>(1,8)*
    z[9] - n<T>(2,3)*z[3] - n<T>(9,16)*z[12] + n<T>(1,4)*z[109] + n<T>(4,3)*z[6];
    z[109]=z[4]*z[109];
    z[111]=z[82] - z[3];
    z[106]=z[106] - z[112] + z[111];
    z[106]=z[8]*z[106];
    z[112]=n<T>(1,3)*z[3];
    z[101]=z[101] - z[112];
    z[80]=z[110] + n<T>(5,12)*z[2] - z[80] + z[101];
    z[80]=z[7]*z[80];
    z[110]=npow(z[6],2);
    z[113]=npow(z[12],2);
    z[114]= - n<T>(1,3)*z[110] + n<T>(9,16)*z[113];
    z[115]=z[10] + z[3];
    z[115]=z[115]*z[92];
    z[116]=3*z[12];
    z[91]= - z[91] - z[116] + z[3];
    z[91]=z[91]*z[82];
    z[117]=npow(z[10],2);
    z[80]=z[109] + z[86] + z[80] + z[87] + z[106] + z[91] + z[115] + n<T>(1,24)*z[117]
     + n<T>(211,144)*z[29] - z[18]
     + n<T>(5,12)*z[19] - z[114]
     + z[95]
    - z[103];
    z[80]=z[4]*z[80];
    z[86]= - n<T>(9,8)*z[2] + z[82] - z[102] + z[90] - z[93];
    z[86]=z[2]*z[86];
    z[87]=n<T>(1,2)*z[12];
    z[90]=z[87] - z[6];
    z[91]=n<T>(27,16)*z[9] - z[102] - z[90];
    z[91]=z[9]*z[91];
    z[85]= - n<T>(25,12)*z[7] + 2*z[2] - z[81] + z[85];
    z[85]=z[7]*z[85];
    z[93]=n<T>(1,4)*z[117];
    z[95]=32*z[23] - n<T>(43,4)*z[44] + 2*z[22];
    z[102]= - n<T>(33,8)*z[12] + z[3];
    z[102]=n<T>(323,96)*z[8] + n<T>(1,2)*z[102] + n<T>(4,3)*z[9];
    z[102]=z[8]*z[102];
    z[103]=11 + z[10];
    z[103]=n<T>(19,8)*z[5] - n<T>(1,6)*z[7] - 5*z[2] + n<T>(35,48)*z[8] - n<T>(47,24)*
    z[9] + z[3] + n<T>(41,16)*z[12] + n<T>(1,2)*z[103] - 9*z[6];
    z[103]=z[5]*z[103];
    z[85]=n<T>(1,2)*z[103] + z[85] + z[86] + z[102] + z[91] + n<T>(49,32)*z[113]
    - n<T>(23,6)*z[110] + z[93] + n<T>(31,72)*z[29] + n<T>(1,3)*z[95] + z[18];
    z[85]=z[5]*z[85];
    z[86]=n<T>(23,12)*z[2] + z[98] - n<T>(3,2)*z[9] - z[112] + z[6] - z[105];
    z[86]=z[86]*z[100];
    z[91]=z[8] + z[3];
    z[91]=z[91]*z[98];
    z[95]=z[116] - z[82];
    z[95]=z[95]*z[82];
    z[98]=n<T>(1,2)*z[117];
    z[100]=npow(z[3],2);
    z[102]=17*z[24] + 5*z[19];
    z[102]=n<T>(1,4)*z[102] - 2*z[26];
    z[86]=z[86] + z[91] + z[95] - n<T>(2,3)*z[100] + 2*z[110] - z[98] - n<T>(67,36)*z[29]
     - n<T>(5,4)*z[18]
     + n<T>(1,3)*z[102] - z[94];
    z[86]=z[2]*z[86];
    z[83]=n<T>(11,3)*z[6] - z[99] + z[83];
    z[83]=n<T>(1,2)*z[83] + z[7];
    z[83]=z[7]*z[83];
    z[94]= - z[111]*z[82];
    z[95]= - z[108] - z[101];
    z[95]=z[2]*z[95];
    z[83]=z[83] + z[95] - z[91] + z[94] - n<T>(1,12)*z[100] + z[93] - n<T>(113,48)*z[29]
     + n<T>(17,12)*z[24]
     + z[114]
     + z[97];
    z[83]=z[7]*z[83];
    z[91]=n<T>(15,4)*z[12] + z[111];
    z[91]=z[91]*z[82];
    z[93]=z[100] - n<T>(89,12)*z[45] - 5*z[44];
    z[94]=z[10] + n<T>(3,4)*z[12];
    z[94]= - n<T>(235,36)*z[8] - n<T>(77,12)*z[9] + n<T>(1,2)*z[94] - 3*z[3];
    z[94]=z[8]*z[94];
    z[91]=n<T>(1,4)*z[94] + z[91] + n<T>(3,32)*z[113] + n<T>(1,8)*z[117] - n<T>(31,24)*
    z[29] + n<T>(5,2)*z[26] + z[27] + n<T>(1,2)*z[93];
    z[91]=z[8]*z[91];
    z[90]= - n<T>(7,24)*z[9] + z[90];
    z[82]=z[90]*z[82];
    z[90]= - 89*z[45] - 131*z[44];
    z[90]=n<T>(1,3)*z[90] - 15*z[28];
    z[82]=z[82] - n<T>(11,16)*z[113] - n<T>(1,2)*z[110] + n<T>(307,144)*z[29] + n<T>(1,8)
   *z[90] - z[18];
    z[82]=z[9]*z[82];
    z[84]= - z[84] + z[107] + z[89];
    z[84]=z[29]*z[84];
    z[89]=z[6] - n<T>(17,24)*z[12];
    z[87]=z[89]*z[87];
    z[87]=z[87] + z[98] - n<T>(271,96)*z[29] - n<T>(9,4)*z[18] - z[104];
    z[87]=z[12]*z[87];
    z[81]= - z[81] + n<T>(11,3)*z[3];
    z[81]=z[81]*z[92];
    z[89]=n<T>(1,6)*z[42];
    z[81]=z[81] - n<T>(1,6)*z[117] - n<T>(5,36)*z[29] + n<T>(11,6)*z[26] + z[89]
     + 
    z[20];
    z[81]=z[3]*z[81];
    z[89]= - n<T>(5,6)*z[117] - n<T>(11,36)*z[29] - z[21] - z[43] + z[89];
    z[89]=z[10]*z[89];
    z[90]=z[10] + n<T>(19,3)*z[6];
    z[90]=z[6]*z[90];
    z[90]=n<T>(1,2)*z[90] - n<T>(5,18)*z[29] + n<T>(20,3)*z[23] - z[43] - z[96];
    z[90]=z[6]*z[90];
    z[92]=z[68] - z[66] + n<T>(15,2)*z[71] + n<T>(57,8)*z[72] + n<T>(11,2)*z[73]
     + 5
   *z[74] - n<T>(3,8)*z[75] - z[76] + n<T>(323,24)*z[77] + z[79] + z[78];
    z[93]=z[59] + z[60];
    z[94]=z[53] - z[65];
    z[95]=z[47] - z[67];
    z[96]=z[36] - z[55];
    z[97]=z[31] - z[50];

    r +=  - n<T>(11,2)*z[22] - n<T>(13,3)*z[32] + n<T>(23,72)*z[33] + n<T>(65,12)*z[38]
       + n<T>(13,4)*z[39] + 12*z[40] - n<T>(1745,72)*z[41] - 5*z[46] + n<T>(1,6)*
      z[48] - n<T>(5,16)*z[49] + n<T>(5,12)*z[51] + n<T>(3,4)*z[52] + n<T>(17,48)*z[54]
       + n<T>(11,4)*z[56] + n<T>(1,8)*z[57] - n<T>(7,16)*z[58] - n<T>(55,12)*z[61]
     - n<T>(7,3)*z[62]
     - 16*z[63]
     - n<T>(3,8)*z[64]
     + z[69]
     + 2*z[70]
     + z[80]
     +  z[81] + z[82] + z[83] + z[84] + z[85] + z[86] + z[87] + z[88] + 
      z[89] + z[90] + z[91] + n<T>(1,4)*z[92] - n<T>(1,3)*z[93] + n<T>(8,3)*z[94]
       + n<T>(1,2)*z[95] + n<T>(1,24)*z[96] - n<T>(23,24)*z[97];
 
    return r;
}

template std::complex<double> qg_2lha_tf982(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf982(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
