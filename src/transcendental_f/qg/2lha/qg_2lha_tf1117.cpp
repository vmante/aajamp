#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1117(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[18];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[12];
    z[4]=c[3];
    z[5]=d[7];
    z[6]=d[3];
    z[7]=d[9];
    z[8]=d[5];
    z[9]=d[8];
    z[10]=e[5];
    z[11]=e[10];
    z[12]=e[13];
    z[13]= - z[9] + z[8];
    z[14]=z[8] - z[7];
    z[13]=z[14]*z[13];
    z[14]=z[3] - z[2];
    z[14]=z[14]*i*z[1];
    z[15]=npow(z[2],2);
    z[16]= - z[7] + z[2];
    z[16]=z[5]*z[16];
    z[17]=z[7] - n<T>(1,2)*z[6];
    z[17]=z[6]*z[17];

    r += n<T>(1,3)*z[4] - z[10] - z[11] + 2*z[12] + z[13] + z[14] - n<T>(1,2)*
      z[15] + z[16] + z[17];
 
    return r;
}

template std::complex<double> qg_2lha_tf1117(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1117(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
