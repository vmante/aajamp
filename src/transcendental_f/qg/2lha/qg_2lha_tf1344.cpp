#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1344(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[47];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=d[4];
    z[11]=d[8];
    z[12]=c[11];
    z[13]=c[19];
    z[14]=c[23];
    z[15]=c[25];
    z[16]=c[26];
    z[17]=c[28];
    z[18]=c[31];
    z[19]=f[0];
    z[20]=f[1];
    z[21]=f[3];
    z[22]=f[4];
    z[23]=f[6];
    z[24]=f[8];
    z[25]=f[9];
    z[26]=f[11];
    z[27]=f[12];
    z[28]=f[13];
    z[29]=f[14];
    z[30]=f[16];
    z[31]=f[17];
    z[32]=f[18];
    z[33]=f[23];
    z[34]=f[26];
    z[35]=f[27];
    z[36]=5*z[4];
    z[37]= - z[36] + 14*z[2];
    z[37]=n<T>(1,3)*z[37];
    z[38]=n<T>(1,2)*z[3];
    z[39]=z[37] - z[38];
    z[39]=z[3]*z[39];
    z[40]=3*z[3];
    z[37]=z[37] - z[40];
    z[37]=z[5]*z[37];
    z[41]=npow(z[2],2);
    z[42]=n<T>(1,2)*z[4];
    z[43]=n<T>(10,3)*z[2] + z[42];
    z[43]=z[4]*z[43];
    z[37]=z[37] + z[39] - n<T>(35,6)*z[41] + z[43];
    z[39]=z[1]*i;
    z[37]=z[37]*z[39];
    z[43]=n<T>(1,4)*z[9];
    z[44]=npow(z[10],2);
    z[44]=z[43] - z[44];
    z[44]=z[10]*z[44];
    z[45]=npow(z[11],2);
    z[43]= - z[43] + z[45];
    z[43]=z[11]*z[43];
    z[43]=z[20] - z[32] - z[29] + z[25] - z[23] + z[44] + z[43] + z[35]
    - z[34];
    z[44]=n<T>(5,2)*z[4];
    z[45]=z[44] + 4*z[2];
    z[45]=z[45]*z[4];
    z[45]=z[45] - 2*z[41];
    z[44]= - z[44] + 7*z[2];
    z[46]=z[38] - z[44];
    z[46]=z[3]*z[46];
    z[46]=z[46] + z[45];
    z[46]=z[3]*z[46];
    z[42]= - 5*z[2] - z[42];
    z[42]=z[4]*z[42];
    z[41]= - 4*z[41] + z[42];
    z[41]=z[4]*z[41];
    z[42]=npow(z[2],3);
    z[41]=z[21] + z[46] + n<T>(17,2)*z[42] + z[41];
    z[38]= - z[4] + z[38];
    z[38]=z[38]*z[40];
    z[40]= - n<T>(1,3)*z[44] + n<T>(3,2)*z[3];
    z[40]=z[5]*z[40];
    z[38]=z[40] + n<T>(1,3)*z[45] + z[38];
    z[38]=z[5]*z[38];
    z[40]=z[2] + z[3];
    z[40]=n<T>(1,2)*z[40] - z[39];
    z[40]=z[6]*z[40];
    z[42]=z[2] + z[4];
    z[42]=n<T>(1,2)*z[42] - z[39];
    z[42]=z[7]*z[42];
    z[44]=z[27] - z[24] + z[22] - z[19];
    z[45]=n<T>(44,3)*z[16] + n<T>(35,6)*z[14] + n<T>(1,6)*z[12];
    z[45]=i*z[45];
    z[36]=z[2] + z[36];
    z[36]=7*z[36] - 59*z[3];
    z[36]=n<T>(1,4)*z[36] - 5*z[5];
    z[36]=z[9]*z[36];
    z[39]= - 2*z[39] + z[2] + z[5];
    z[39]=z[8]*z[39];

    r += n<T>(5,72)*z[13] - n<T>(17,6)*z[15] - n<T>(22,3)*z[17] + n<T>(5,9)*z[18]
     + n<T>(5,6)*z[26]
     + n<T>(13,6)*z[28] - z[30]
     + 2*z[31]
     + n<T>(1,2)*z[33]
     + n<T>(1,18)*
      z[36] + z[37] + z[38] + n<T>(10,3)*z[39] + n<T>(17,3)*z[40] + n<T>(1,3)*z[41]
       + n<T>(7,3)*z[42] + n<T>(1,4)*z[43] - n<T>(3,4)*z[44] + z[45];
 
    return r;
}

template std::complex<double> qg_2lha_tf1344(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1344(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
