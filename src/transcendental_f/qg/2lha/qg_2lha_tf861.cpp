#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf861(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[7];
    z[6]=d[17];
    z[7]=e[9];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[31];
    z[12]=e[6];
    z[13]=e[7];
    z[14]=f[30];
    z[15]=f[31];
    z[16]=f[35];
    z[17]=f[36];
    z[18]=f[39];
    z[19]=z[5] + z[4];
    z[20]=z[1]*i;
    z[21]=z[19]*z[20];
    z[21]=z[21] + z[8];
    z[21]=z[6]*z[21];
    z[19]= - z[20] + z[19];
    z[19]=z[7]*z[19];
    z[22]=z[3] + z[4];
    z[23]=z[13]*z[22];
    z[19]= - z[21] - z[19] + z[23] + z[10];
    z[21]= - z[22] + 3*z[2];
    z[21]=z[21]*z[3];
    z[23]=z[2] + z[4];
    z[24]=z[23]*z[2];
    z[21]=z[21] - z[24];
    z[25]=npow(z[4],2);
    z[26]=z[3] + z[2];
    z[27]= - 2*z[4] + z[26];
    z[27]=z[5]*z[27];
    z[27]=z[27] - z[25] - z[21];
    z[27]=z[5]*z[27];
    z[28]=z[4] - z[26];
    z[28]=z[5]*z[28];
    z[21]=2*z[28] - z[25] + z[21];
    z[20]=z[21]*z[20];
    z[21]=z[5] - z[4] - 7*z[26];
    z[21]=z[8]*z[21];
    z[25]=z[9]*i;
    z[21]=z[21] + z[25];
    z[25]= - npow(z[2],2);
    z[25]= - 2*z[12] + z[25];
    z[23]=z[23]*z[25];
    z[22]=z[2] - z[22];
    z[22]=z[3]*z[22];
    z[22]=z[24] + z[22];
    z[22]=z[3]*z[22];
    z[24]=npow(z[4],3);

    r += 7*z[11] + z[14] + z[15] - z[16] - z[17] + z[18] - 2*z[19] + 
      z[20] + n<T>(1,6)*z[21] + z[22] + z[23] + z[24] + z[27];
 
    return r;
}

template std::complex<double> qg_2lha_tf861(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf861(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
