#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf703(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[30];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[7];
    z[6]=d[17];
    z[7]=e[6];
    z[8]=e[7];
    z[9]=e[9];
    z[10]=c[3];
    z[11]=c[11];
    z[12]=c[15];
    z[13]=c[31];
    z[14]=f[30];
    z[15]=f[31];
    z[16]=f[33];
    z[17]=f[35];
    z[18]=f[36];
    z[19]=f[38];
    z[20]=f[39];
    z[21]=npow(z[4],2);
    z[22]=npow(z[5],2);
    z[23]= - 9*z[5] + n<T>(13,2)*z[2];
    z[23]=z[2]*z[23];
    z[24]= - z[4] + n<T>(3,2)*z[5];
    z[24]= - z[3] + 3*z[24] + n<T>(5,2)*z[2];
    z[24]=z[3]*z[24];
    z[22]=z[24] + z[23] + z[21] - n<T>(7,2)*z[22];
    z[22]=z[3]*z[22];
    z[23]=z[5] + z[4];
    z[24]=z[1]*i;
    z[25]=z[23]*z[24];
    z[25]=z[25] + z[10];
    z[25]=z[6]*z[25];
    z[23]= - z[24] + z[23];
    z[23]=z[9]*z[23];
    z[23]=z[25] + z[23] + z[14] - z[12];
    z[25]=z[5]*z[4];
    z[26]=7*z[4];
    z[27]= - n<T>(13,4)*z[2] - z[26] - n<T>(25,2)*z[5];
    z[27]=z[2]*z[27];
    z[27]=z[27] + n<T>(5,2)*z[21] + z[25];
    z[28]= - n<T>(9,2)*z[3] + 7*z[5] + 9*z[2];
    z[28]=z[3]*z[28];
    z[27]=n<T>(1,3)*z[27] + n<T>(1,2)*z[28];
    z[27]=z[27]*z[24];
    z[24]=z[24] - z[5];
    z[28]= - z[3] - z[4] - 3*z[24];
    z[28]=z[8]*z[28];
    z[27]=z[27] + z[28];
    z[25]= - n<T>(7,2)*z[21] - z[25];
    z[25]=z[5]*z[25];
    z[28]=npow(z[4],3);
    z[29]= - z[4] - n<T>(5,12)*z[5];
    z[29]=n<T>(11,4)*z[3] + n<T>(1,2)*z[29] - n<T>(11,3)*z[2];
    z[29]=z[10]*z[29];
    z[25]=z[29] - z[28] + n<T>(1,2)*z[25];
    z[28]=z[26] + n<T>(25,4)*z[5];
    z[28]=z[5]*z[28];
    z[26]= - 19*z[2] - z[26] + n<T>(13,2)*z[5];
    z[26]=z[2]*z[26];
    z[21]=n<T>(1,12)*z[26] + z[21] + n<T>(1,6)*z[28];
    z[21]=z[2]*z[21];
    z[26]=z[2] + z[4];
    z[24]=z[3] + z[24] - n<T>(19,6)*z[26];
    z[24]=z[7]*z[24];
    z[26]=z[17] - z[13];
    z[28]=z[11]*i;

    r +=  - n<T>(7,8)*z[15] - z[16] - n<T>(25,24)*z[18] - 2*z[19] + n<T>(9,8)*z[20]
       + z[21] + n<T>(1,4)*z[22] + n<T>(1,6)*z[23] + z[24] + n<T>(1,3)*z[25] - n<T>(3,2)
      *z[26] + n<T>(1,2)*z[27] + n<T>(7,72)*z[28];
 
    return r;
}

template std::complex<double> qg_2lha_tf703(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf703(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
