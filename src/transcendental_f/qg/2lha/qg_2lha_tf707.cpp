#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf707(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[99];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[5];
    z[21]=e[7];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=e[10];
    z[25]=e[11];
    z[26]=e[12];
    z[27]=e[13];
    z[28]=c[3];
    z[29]=c[11];
    z[30]=c[15];
    z[31]=c[19];
    z[32]=c[23];
    z[33]=c[25];
    z[34]=c[26];
    z[35]=c[28];
    z[36]=c[31];
    z[37]=e[3];
    z[38]=e[6];
    z[39]=e[14];
    z[40]=f[2];
    z[41]=f[3];
    z[42]=f[4];
    z[43]=f[5];
    z[44]=f[6];
    z[45]=f[7];
    z[46]=f[10];
    z[47]=f[11];
    z[48]=f[12];
    z[49]=f[16];
    z[50]=f[17];
    z[51]=f[23];
    z[52]=f[30];
    z[53]=f[31];
    z[54]=f[32];
    z[55]=f[33];
    z[56]=f[34];
    z[57]=f[35];
    z[58]=f[36];
    z[59]=f[39];
    z[60]=f[43];
    z[61]=f[50];
    z[62]=f[51];
    z[63]=f[53];
    z[64]=f[55];
    z[65]=f[56];
    z[66]=f[58];
    z[67]=f[60];
    z[68]=n<T>(1,2)*z[8];
    z[69]=z[1]*i;
    z[70]=z[68] - z[69];
    z[71]=z[70]*z[8];
    z[72]=3*z[6];
    z[73]=n<T>(11,3)*z[10] + z[3] + z[69] - z[72];
    z[74]=n<T>(1,2)*z[10];
    z[73]=z[73]*z[74];
    z[75]= - n<T>(25,6)*z[6] + n<T>(13,6) - z[69];
    z[75]=z[6]*z[75];
    z[76]=n<T>(5,3) - z[69];
    z[76]= - n<T>(2,3)*z[9] + n<T>(4,3)*z[6] + 2*z[76] + z[5];
    z[76]=z[9]*z[76];
    z[77]=2*z[69];
    z[78]=z[77] - z[2];
    z[79]=z[2]*z[78];
    z[80]=z[69] - z[5];
    z[81]= - z[9] + z[6] + z[80];
    z[81]=z[3]*z[81];
    z[73]=z[73] + z[81] + z[79] + z[76] + z[71] + z[75];
    z[73]=z[10]*z[73];
    z[75]=n<T>(1,2)*z[5];
    z[76]=z[75] - z[69];
    z[76]=z[76]*z[5];
    z[79]=7*z[5];
    z[81]=14*z[6] - 7*z[8] + 4*z[69] - z[79];
    z[81]=z[6]*z[81];
    z[82]=n<T>(1,3)*z[9];
    z[83]=z[69] - z[8];
    z[84]=z[5] - z[83];
    z[85]= - z[9] + z[84];
    z[85]=z[85]*z[82];
    z[86]=2*z[5];
    z[87]= - n<T>(13,2)*z[69] - z[86];
    z[87]=n<T>(7,6)*z[2] - n<T>(1,6)*z[9] + n<T>(5,3)*z[6] + n<T>(1,3)*z[87] - z[68];
    z[87]=z[2]*z[87];
    z[88]=z[77] + z[5];
    z[89]= - z[8] + z[88];
    z[89]=z[8]*z[89];
    z[81]=z[87] + z[85] + n<T>(1,3)*z[81] - n<T>(5,3)*z[76] + z[89];
    z[81]=z[2]*z[81];
    z[85]=2*z[2];
    z[77]= - n<T>(11,6)*z[3] + z[85] - z[9] - z[68] + z[77] - z[75];
    z[87]=n<T>(1,3)*z[3];
    z[77]=z[77]*z[87];
    z[89]= - z[69] + z[86];
    z[89]=z[5]*z[89];
    z[90]=z[68] - z[80];
    z[90]=z[8]*z[90];
    z[89]=z[89] + z[90];
    z[90]=n<T>(3,2)*z[6];
    z[91]= - z[90] - z[80];
    z[91]=z[6]*z[91];
    z[92]=n<T>(1,2)*z[2];
    z[93]=z[84] - z[92];
    z[94]= - z[9] - z[93];
    z[95]=n<T>(1,3)*z[2];
    z[94]=z[94]*z[95];
    z[96]=2*z[80];
    z[97]= - z[96] + z[8];
    z[97]=n<T>(5,6)*z[9] + n<T>(1,3)*z[97] + z[6];
    z[97]=z[9]*z[97];
    z[77]=z[77] + z[94] + z[97] + n<T>(1,3)*z[89] + z[91];
    z[77]=z[3]*z[77];
    z[89]=n<T>(1,2)*z[9];
    z[72]=z[89] - z[69] - z[72];
    z[72]=z[9]*z[72];
    z[91]=n<T>(7,3)*z[76];
    z[70]= - z[5] + z[70];
    z[70]=z[8]*z[70];
    z[94]=n<T>(1,2)*z[6];
    z[97]=3*z[69];
    z[98]= - z[97] + z[94];
    z[98]=z[6]*z[98];
    z[70]=z[72] + z[98] + z[91] + 3*z[70];
    z[72]=n<T>(7,6)*z[5];
    z[90]=z[89] + z[90] + n<T>(3,2)*z[8] + z[69] + z[72];
    z[98]=n<T>(1,2)*z[4];
    z[90]= - z[98] - z[74] + n<T>(1,2)*z[90] - n<T>(4,3)*z[2];
    z[90]=z[4]*z[90];
    z[92]= - z[92] + 8*z[69] + z[5];
    z[92]=z[92]*z[95];
    z[74]= - z[74] + z[6] + z[9];
    z[74]=z[10]*z[74];
    z[70]=z[90] + z[74] + n<T>(1,2)*z[70] + z[92];
    z[70]=z[4]*z[70];
    z[74]=n<T>(7,3)*z[2];
    z[72]= - n<T>(37,18)*z[7] - z[74] + n<T>(35,6)*z[6] - n<T>(11,6)*z[8] - z[97]
     - 
    z[72];
    z[72]=z[7]*z[72];
    z[90]=11*z[69] - z[79];
    z[90]=n<T>(1,3)*z[90] - 6*z[8];
    z[90]=z[8]*z[90];
    z[92]= - z[6] + z[93];
    z[74]=z[92]*z[74];
    z[92]=z[5] + 2*z[6];
    z[92]=z[6]*z[92];
    z[72]=z[72] + z[74] + n<T>(7,3)*z[92] - z[91] + z[90];
    z[72]=z[7]*z[72];
    z[74]= - 5*z[3] + z[85] - 2*z[84] - 7*z[9];
    z[74]=z[24]*z[74];
    z[84]=z[84] - z[2];
    z[84]= - 41*z[7] - 55*z[6] - 14*z[84];
    z[84]=z[21]*z[84];
    z[85]=z[5] - z[78];
    z[85]=z[18]*z[85];
    z[90]= - z[3] - z[11];
    z[90]=z[37]*z[90];
    z[74]= - z[85] - z[84] - z[74] - z[90] - z[51] + z[46] + z[40];
    z[71]= - z[76] + 11*z[71];
    z[84]=z[94] + z[83];
    z[84]=z[6]*z[84];
    z[85]= - z[89] - z[80];
    z[85]=z[9]*z[85];
    z[71]=z[85] + n<T>(1,3)*z[71] + 11*z[84];
    z[84]=n<T>(11,2)*z[8];
    z[85]=z[84] - 5*z[69] - z[75];
    z[85]= - z[89] + n<T>(1,3)*z[85] + n<T>(11,2)*z[6];
    z[89]=z[11] + z[7];
    z[85]=n<T>(1,2)*z[85] + z[87] - n<T>(11,6)*z[89];
    z[85]=z[11]*z[85];
    z[90]=n<T>(1,2)*z[3] + z[80];
    z[87]=z[90]*z[87];
    z[90]=z[7]*z[83];
    z[71]=z[85] - n<T>(11,3)*z[90] + n<T>(1,2)*z[71] + z[87];
    z[71]=z[11]*z[71];
    z[75]=n<T>(1,3)*z[8] - z[97] + z[75];
    z[75]=z[8]*z[75];
    z[75]=z[76] + z[75];
    z[75]=z[75]*z[68];
    z[79]=n<T>(61,4)*z[8] - n<T>(61,2)*z[69] + z[79];
    z[79]=z[8]*z[79];
    z[85]=17*z[69] - n<T>(5,2)*z[5];
    z[85]=z[5]*z[85];
    z[79]=z[85] + z[79];
    z[84]=z[84] - n<T>(19,3)*z[5] - n<T>(13,3) - 9*z[69];
    z[84]=n<T>(1,2)*z[84] - n<T>(55,9)*z[6];
    z[84]=z[6]*z[84];
    z[79]=n<T>(1,3)*z[79] + z[84];
    z[79]=z[6]*z[79];
    z[68]= - z[68] - z[88];
    z[68]=z[8]*z[68];
    z[68]= - n<T>(7,2)*z[76] + z[68];
    z[76]= - n<T>(25,6)*z[9] + n<T>(19,4)*z[6] + z[8] + z[69] - n<T>(7,4)*z[5];
    z[76]=z[76]*z[82];
    z[82]= - n<T>(2,3) + n<T>(1,2)*z[69];
    z[82]=n<T>(1,12)*z[6] + 5*z[82] - z[5];
    z[82]=z[6]*z[82];
    z[68]=z[76] + n<T>(1,3)*z[68] + z[82];
    z[68]=z[9]*z[68];
    z[76]=z[11] - z[2];
    z[82]= - z[86] - n<T>(5,2)*z[8];
    z[76]=n<T>(17,2)*z[7] + z[98] + z[10] - z[3] + n<T>(7,2)*z[9] + 5*z[82] - 
   n<T>(131,2)*z[6] - 10*z[76];
    z[76]=z[28]*z[76];
    z[82]=z[4] - z[78];
    z[82]=z[17]*z[82];
    z[84]= - z[9] - z[10];
    z[84]=z[39]*z[84];
    z[82]= - z[36] - z[82] - z[84] + z[60] - z[57];
    z[84]=z[69]*z[8];
    z[84]=z[84] + z[28];
    z[85]=z[7]*z[69];
    z[85]=z[85] + z[84];
    z[85]=z[15]*z[85];
    z[86]=z[7] - z[83];
    z[86]=z[23]*z[86];
    z[85]=z[85] + z[86];
    z[86]=z[69]*z[5];
    z[86]=z[86] + z[28];
    z[87]= - z[6]*z[69];
    z[87]=z[87] - z[86];
    z[87]=z[14]*z[87];
    z[88]=z[6] - z[80];
    z[88]=z[20]*z[88];
    z[87]=z[87] + z[88];
    z[88]= - z[3]*z[69];
    z[86]=z[88] - z[86];
    z[86]=z[12]*z[86];
    z[88]=z[4] + z[10];
    z[90]=z[69]*z[88];
    z[90]=z[28] + z[90];
    z[90]=z[13]*z[90];
    z[91]=z[9]*z[69];
    z[84]=z[91] + z[84];
    z[84]=z[16]*z[84];
    z[69]=z[69] - z[88];
    z[69]=z[26]*z[69];
    z[88]= - z[38]*z[89];
    z[88]= - z[35] + z[88] + z[52];
    z[89]= - z[47] + z[64] + z[49];
    z[91]= - z[65] + z[61] + z[56] - z[44];
    z[92]=z[58] + z[42];
    z[93]=z[62] + z[50];
    z[94]=n<T>(22,3)*z[34] + n<T>(13,6)*z[32] + n<T>(7,9)*z[29];
    z[94]=i*z[94];
    z[95]= - npow(z[5],2)*z[96];
    z[96]=z[7] - n<T>(4,3)*z[10] - 2*z[3] - 3*z[80] + n<T>(8,3)*z[6];
    z[96]=z[27]*z[96];
    z[80]= - z[3] + z[80];
    z[80]=z[19]*z[80];
    z[78]=z[8] - z[78];
    z[78]=z[22]*z[78];
    z[83]= - z[9] + z[83];
    z[83]=z[25]*z[83];

    r +=  - n<T>(16,3)*z[30] - n<T>(10,9)*z[31] - n<T>(7,6)*z[33] + n<T>(7,12)*z[41]
     +  n<T>(4,3)*z[43] - n<T>(8,3)*z[45] + n<T>(1,12)*z[48] + n<T>(89,12)*z[53] + n<T>(7,2)*
      z[54] + n<T>(56,3)*z[55] + n<T>(11,4)*z[59] - 8*z[63] + n<T>(19,12)*z[66] + 
      z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] - n<T>(1,3)*
      z[74] + z[75] + n<T>(1,9)*z[76] + z[77] + z[78] + z[79] + z[80] + 
      z[81] - n<T>(7,3)*z[82] + z[83] + z[84] + n<T>(25,3)*z[85] + z[86] + 4*
      z[87] + n<T>(11,3)*z[88] - n<T>(1,4)*z[89] + z[90] + n<T>(1,2)*z[91] - n<T>(11,12)
      *z[92] + n<T>(3,4)*z[93] + z[94] + z[95] + z[96];
 
    return r;
}

template std::complex<double> qg_2lha_tf707(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf707(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
