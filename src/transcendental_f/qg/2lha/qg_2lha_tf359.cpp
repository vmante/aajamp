#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf359(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[33];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[7];
    z[6]=d[17];
    z[7]=e[9];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[17];
    z[12]=c[31];
    z[13]=e[6];
    z[14]=e[7];
    z[15]=f[30];
    z[16]=f[31];
    z[17]=f[33];
    z[18]=f[36];
    z[19]=f[39];
    z[20]=f[40];
    z[21]=i*z[1];
    z[22]= - z[21] + n<T>(1,2)*z[5];
    z[23]=n<T>(1,3)*z[5];
    z[24]= - z[22]*z[23];
    z[25]=n<T>(1,2)*z[2];
    z[26]=z[21] - z[5];
    z[27]=n<T>(7,9)*z[2] + n<T>(1,3)*z[26] + z[3];
    z[27]=z[27]*z[25];
    z[28]=z[21]*z[6];
    z[28]=z[28] + z[7];
    z[29]=z[13] + z[8];
    z[30]= - 2*z[26] - z[3];
    z[30]=z[3]*z[30];
    z[24]=z[27] + z[30] + z[24] + n<T>(1,3)*z[29] - z[28];
    z[24]=z[2]*z[24];
    z[24]=z[24] + n<T>(5,6)*z[18] + n<T>(2,3)*z[20] + n<T>(1,2)*z[19];
    z[27]=13*z[14];
    z[29]= - n<T>(5,2)*z[3] - 5*z[21] - 4*z[5];
    z[29]=z[3]*z[29];
    z[29]=z[29] - n<T>(13,6)*z[8] + z[27] + z[13] - 11*z[28];
    z[30]=2*z[5];
    z[31]= - n<T>(7,3)*z[21] + z[30];
    z[31]=z[5]*z[31];
    z[32]=8*z[21] - 5*z[5];
    z[25]=n<T>(1,3)*z[32] - z[25];
    z[25]=z[2]*z[25];
    z[25]=z[25] + z[31] + n<T>(1,3)*z[29];
    z[29]= - z[3] + n<T>(13,9)*z[21] + z[5];
    z[29]=n<T>(1,27)*z[4] + n<T>(1,2)*z[29] + n<T>(2,9)*z[2];
    z[29]=z[4]*z[29];
    z[25]=n<T>(1,3)*z[25] + z[29];
    z[25]=z[4]*z[25];
    z[26]=n<T>(13,2)*z[3] + z[26];
    z[26]=z[3]*z[26];
    z[26]=z[26] + z[27] + n<T>(22,3)*z[8];
    z[22]=z[5]*z[22];
    z[22]= - n<T>(11,9)*z[22] + z[28] + n<T>(1,9)*z[26];
    z[22]=z[3]*z[22];
    z[21]= - z[21] + z[23];
    z[21]=z[21]*z[30];
    z[23]= - n<T>(1,2)*z[8] - z[28];
    z[21]=5*z[23] + z[21];
    z[21]=z[5]*z[21];
    z[21]=z[21] - z[15];
    z[23]=z[6]*z[8];
    z[26]=z[1]*z[7];
    z[26]=n<T>(5,9)*z[26] + n<T>(1,2)*z[11] + n<T>(1,27)*z[9];
    z[26]=i*z[26];

    r += n<T>(29,36)*z[10] - n<T>(5,8)*z[12] - n<T>(11,18)*z[16] - 2*z[17] + n<T>(1,9)*
      z[21] + z[22] - n<T>(5,9)*z[23] + n<T>(1,3)*z[24] + z[25] + z[26];
 
    return r;
}

template std::complex<double> qg_2lha_tf359(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf359(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
