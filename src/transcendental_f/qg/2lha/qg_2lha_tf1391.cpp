#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1391(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[41];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[7];
    z[4]=d[1];
    z[5]=d[4];
    z[6]=d[8];
    z[7]=d[2];
    z[8]=d[3];
    z[9]=e[0];
    z[10]=e[8];
    z[11]=c[3];
    z[12]=c[19];
    z[13]=c[23];
    z[14]=c[25];
    z[15]=c[26];
    z[16]=c[28];
    z[17]=c[30];
    z[18]=c[31];
    z[19]=e[1];
    z[20]=e[3];
    z[21]=e[10];
    z[22]=f[0];
    z[23]=f[3];
    z[24]=f[4];
    z[25]=f[5];
    z[26]=f[10];
    z[27]=f[11];
    z[28]=f[12];
    z[29]=f[17];
    z[30]=f[20];
    z[31]=z[1]*i;
    z[32]=z[31]*z[8];
    z[33]=npow(z[8],2);
    z[33]=n<T>(1,2)*z[33];
    z[32]=z[32] - z[33];
    z[34]=z[31] - z[8];
    z[35]=z[34] + n<T>(1,2)*z[4];
    z[35]=z[35]*z[4];
    z[35]=z[35] - z[32];
    z[34]=z[34] + z[4];
    z[36]=z[5]*z[34];
    z[36]=z[36] + z[35];
    z[36]=z[5]*z[36];
    z[34]= - z[6]*z[34];
    z[34]=z[34] - z[35];
    z[34]=z[6]*z[34];
    z[35]=z[6] - z[5];
    z[37]=z[7] + z[8];
    z[35]=z[2] + z[37] + n<T>(1,6)*z[35];
    z[35]=z[11]*z[35];
    z[38]= - z[4] - z[5];
    z[38]=z[20]*z[38];
    z[39]=z[4] + z[6];
    z[39]=z[21]*z[39];
    z[34]=z[39] + z[28] - z[26] + z[24] + z[36] + z[34] + z[35] + z[38];
    z[35]=n<T>(1,2)*z[7];
    z[36]=z[35] - z[31];
    z[38]=z[8] - z[36];
    z[38]=z[7]*z[38];
    z[32]=z[38] + z[32];
    z[38]=n<T>(1,2)*z[3];
    z[39]= - n<T>(1,2)*z[37] + z[2];
    z[39]=z[39]*z[38];
    z[40]= - z[2]*z[31];
    z[32]=z[39] + n<T>(1,2)*z[32] + z[40];
    z[32]=z[3]*z[32];
    z[36]=z[8]*z[36];
    z[33]=z[33] + z[36];
    z[33]=z[33]*z[35];
    z[35]= - z[2] + z[31] + z[37];
    z[36]=n<T>(1,2)*z[2];
    z[35]=z[35]*z[36];
    z[37]=z[7]*z[8];
    z[35]= - z[37] + z[35];
    z[35]=z[2]*z[35];
    z[36]=z[36] + z[38];
    z[37]=z[31] - z[36];
    z[37]=z[10]*z[37];
    z[36]=z[7] - z[36];
    z[36]=z[19]*z[36];
    z[38]=z[29] + z[27] + z[25] - z[23];
    z[39]= - 3*z[15] - n<T>(1,2)*z[13];
    z[39]=i*z[39];
    z[31]= - z[2] + 2*z[31] - z[7];
    z[31]=z[9]*z[31];

    r +=  - n<T>(1,24)*z[12] + n<T>(7,12)*z[14] + n<T>(5,2)*z[16] + z[17] + n<T>(1,6)*
      z[18] - z[22] + n<T>(1,3)*z[30] + z[31] + z[32] + z[33] + n<T>(1,12)*
      z[34] + z[35] + z[36] + z[37] - n<T>(1,4)*z[38] + z[39];
 
    return r;
}

template std::complex<double> qg_2lha_tf1391(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1391(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
