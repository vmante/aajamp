#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf736(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[108];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[10];
    z[26]=e[11];
    z[27]=e[12];
    z[28]=e[13];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[19];
    z[33]=c[21];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=c[31];
    z[39]=e[3];
    z[40]=e[6];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[7];
    z[50]=f[10];
    z[51]=f[11];
    z[52]=f[12];
    z[53]=f[16];
    z[54]=f[17];
    z[55]=f[18];
    z[56]=f[21];
    z[57]=f[23];
    z[58]=f[30];
    z[59]=f[31];
    z[60]=f[32];
    z[61]=f[33];
    z[62]=f[34];
    z[63]=f[35];
    z[64]=f[36];
    z[65]=f[37];
    z[66]=f[39];
    z[67]=f[43];
    z[68]=f[50];
    z[69]=f[51];
    z[70]=f[54];
    z[71]=f[55];
    z[72]=f[56];
    z[73]=f[58];
    z[74]=f[73];
    z[75]=z[1]*i;
    z[76]=n<T>(7,3)*z[75];
    z[77]=n<T>(2,3)*z[8];
    z[78]=3*z[5];
    z[79]=z[77] + z[76] + z[78];
    z[79]=z[8]*z[79];
    z[80]=z[78] + z[3];
    z[81]= - n<T>(7,3)*z[8] - n<T>(1,3)*z[75] + z[80];
    z[82]=3*z[2];
    z[83]=n<T>(1,2)*z[4];
    z[81]=6*z[7] + z[83] - n<T>(11,2)*z[6] + n<T>(1,2)*z[81] + z[82];
    z[81]=z[7]*z[81];
    z[84]=n<T>(1,2)*z[5];
    z[85]=z[84] - z[75];
    z[86]=z[85]*z[78];
    z[87]=n<T>(1,2)*z[3];
    z[88]=z[87] + z[75];
    z[88]=z[3]*z[88];
    z[89]=z[8] + z[5];
    z[90]=z[89] - z[75];
    z[91]=n<T>(1,2)*z[2];
    z[92]=z[91] - z[90];
    z[82]=z[92]*z[82];
    z[92]=7*z[6];
    z[93]= - z[5] + z[2];
    z[93]=3*z[93] - z[92];
    z[93]=z[6]*z[93];
    z[94]=z[75] + z[3];
    z[83]=z[83] - z[94];
    z[83]=z[4]*z[83];
    z[79]=z[81] + z[83] + z[93] + z[82] + z[88] + z[86] + z[79];
    z[79]=z[7]*z[79];
    z[81]=n<T>(17,3)*z[75] - z[78];
    z[82]=n<T>(5,2)*z[3];
    z[83]=n<T>(3,2)*z[2];
    z[81]= - n<T>(2,3)*z[10] - n<T>(7,6)*z[7] + n<T>(3,2)*z[4] - n<T>(7,4)*z[9] + 2*z[6]
    - z[83] + z[82] + n<T>(1,4)*z[81] - z[77];
    z[81]=z[10]*z[81];
    z[85]=z[85]*z[5];
    z[88]=2*z[75];
    z[93]=z[88] - z[8];
    z[77]=z[93]*z[77];
    z[95]=z[75] - z[8];
    z[96]=n<T>(1,2)*z[6];
    z[97]=z[96] + z[95];
    z[97]=z[6]*z[97];
    z[98]=n<T>(1,2)*z[9];
    z[99]=z[75] - z[5];
    z[100]= - z[98] - z[99];
    z[100]=z[9]*z[100];
    z[101]=z[75]*z[4];
    z[102]=2*z[99];
    z[103]=z[102] + z[3];
    z[103]=z[3]*z[103];
    z[104]= - n<T>(7,3)*z[95] - z[7];
    z[104]=z[7]*z[104];
    z[77]=z[81] + z[104] - 3*z[101] + n<T>(7,2)*z[100] + z[97] + z[103] - n<T>(3,2)*z[85]
     + z[77];
    z[77]=z[10]*z[77];
    z[81]=3*z[8];
    z[97]= - z[75] + n<T>(1,2)*z[8];
    z[100]=z[5] - z[97];
    z[100]=z[100]*z[81];
    z[103]= - z[88] + z[87];
    z[103]=z[3]*z[103];
    z[104]=5*z[75];
    z[91]=z[91] - z[104] - z[5];
    z[91]=z[2]*z[91];
    z[105]= - z[75] + z[96];
    z[105]=z[6]*z[105];
    z[76]=n<T>(7,6)*z[9] - z[76] - 13*z[6];
    z[76]=z[76]*z[98];
    z[98]=n<T>(15,4)*z[6];
    z[106]= - 3*z[4] + n<T>(7,12)*z[9] + z[98] + n<T>(5,2)*z[2] + z[3] - n<T>(3,2)*
    z[8] + n<T>(5,3)*z[75] - z[5];
    z[106]=z[4]*z[106];
    z[107]=z[88] - z[5];
    z[107]=z[5]*z[107];
    z[76]=z[106] + z[76] + n<T>(15,2)*z[105] + z[91] + z[103] + z[107] + 
    z[100];
    z[76]=z[4]*z[76];
    z[91]=z[87] + z[95];
    z[91]=z[3]*z[91];
    z[97]=z[8]*z[97];
    z[100]=z[75] - z[6];
    z[100]=z[6]*z[100];
    z[103]= - z[88] - z[9];
    z[103]=z[9]*z[103];
    z[105]= - n<T>(13,6)*z[4] - z[6] + n<T>(16,3)*z[9];
    z[105]=z[4]*z[105];
    z[106]=z[6] - n<T>(1,2)*z[7];
    z[106]=z[7]*z[106];
    z[107]=3*z[6] + z[3] - z[95];
    z[107]= - n<T>(3,2)*z[7] + n<T>(1,2)*z[107] - z[9];
    z[107]=z[11]*z[107];
    z[91]=z[107] + 3*z[106] + z[105] + n<T>(8,3)*z[103] + z[100] + z[97] + 
    z[91];
    z[91]=z[11]*z[91];
    z[97]=2*z[8];
    z[100]= - z[5] - z[93];
    z[100]=z[100]*z[97];
    z[103]=4*z[75];
    z[89]=z[103] - z[89];
    z[82]=2*z[89] + z[82];
    z[82]=z[3]*z[82];
    z[89]=7*z[75];
    z[105]=z[89] + z[78];
    z[105]= - n<T>(13,6)*z[2] - n<T>(2,3)*z[3] + n<T>(1,2)*z[105] + z[8];
    z[105]=z[2]*z[105];
    z[82]=z[105] + n<T>(1,3)*z[82] + z[86] + z[100];
    z[82]=z[2]*z[82];
    z[86]=2*z[2];
    z[80]= - z[86] + z[81] - z[88] + z[80];
    z[80]=z[2]*z[80];
    z[81]= - z[104] + z[84];
    z[81]=z[5]*z[81];
    z[100]= - z[6] - 13*z[2] - z[3] + z[8] - n<T>(5,2)*z[75] + 7*z[5];
    z[100]=z[100]*z[96];
    z[78]= - z[8]*z[78];
    z[94]= - z[3]*z[94];
    z[78]=z[100] + z[80] + z[94] + z[81] + z[78];
    z[78]=z[6]*z[78];
    z[80]= - z[87] + z[97] - z[99];
    z[80]=z[3]*z[80];
    z[81]= - z[3] + z[90];
    z[81]=2*z[81] - z[2];
    z[81]=z[2]*z[81];
    z[87]= - z[88] - z[5];
    z[87]=2*z[87] - z[8];
    z[87]=z[8]*z[87];
    z[80]=z[81] + z[80] - n<T>(23,2)*z[85] + z[87];
    z[81]=11*z[3];
    z[86]= - z[86] + z[81] + z[97] + z[103] - n<T>(23,4)*z[5];
    z[86]= - n<T>(17,18)*z[9] + n<T>(1,3)*z[86] + z[98];
    z[86]=z[9]*z[86];
    z[87]=z[75] + z[96];
    z[87]=z[6]*z[87];
    z[80]=z[86] + n<T>(1,3)*z[80] + n<T>(13,2)*z[87];
    z[80]=z[9]*z[80];
    z[86]=z[101] + z[29];
    z[87]=z[10]*z[75];
    z[87]=z[87] + z[86];
    z[87]=z[14]*z[87];
    z[94]=z[75] - z[4];
    z[96]= - z[10] + z[94];
    z[96]=z[20]*z[96];
    z[97]= - z[3] - z[10];
    z[97]=z[39]*z[97];
    z[87]=z[97] + z[67] - z[59] - z[47] - z[42] + z[87] + z[96];
    z[96]=z[75]*z[5];
    z[96]=z[96] + z[29];
    z[97]=z[6]*z[75];
    z[97]=z[97] + z[96];
    z[97]=z[15]*z[97];
    z[88]= - z[4] + z[88] - z[2];
    z[88]=z[18]*z[88];
    z[98]= - z[6] + z[99];
    z[98]=z[21]*z[98];
    z[88]=z[98] + z[97] + z[88];
    z[97]=z[75]*z[8];
    z[97]=z[97] + z[29];
    z[98]=z[9]*z[75];
    z[98]=z[98] + z[97];
    z[98]=z[17]*z[98];
    z[93]= - z[2] + z[93];
    z[93]=z[23]*z[93];
    z[100]= - z[9] + z[95];
    z[100]=z[26]*z[100];
    z[93]=z[98] + z[93] + z[100] - z[44];
    z[84]= - z[84] + z[95];
    z[84]=z[8]*z[84];
    z[84]= - z[85] + z[84];
    z[84]=z[8]*z[84];
    z[85]= - 3*z[75] + n<T>(8,3)*z[5];
    z[85]=z[5]*z[85];
    z[98]=n<T>(5,2)*z[8] - z[104] + 2*z[5];
    z[98]=z[8]*z[98];
    z[100]=5*z[5];
    z[101]= - n<T>(85,3)*z[3] + z[100] + z[8];
    z[101]=z[3]*z[101];
    z[85]=n<T>(1,6)*z[101] + z[85] + n<T>(1,3)*z[98];
    z[85]=z[3]*z[85];
    z[89]=z[89] - z[100];
    z[89]=z[89]*npow(z[5],2);
    z[89]= - z[89] - z[72] - z[65] + z[63] + z[62] + z[51];
    z[90]=z[90] - z[2];
    z[90]= - 23*z[9] - 19*z[3] - 4*z[90];
    z[90]=z[25]*z[90];
    z[98]= - z[7] - z[10];
    z[98]=z[40]*z[98];
    z[90]=z[50] + z[90] + z[98];
    z[98]= - z[3]*z[75];
    z[96]=z[98] - z[96];
    z[96]=z[12]*z[96];
    z[98]= - z[3] + z[99];
    z[98]=z[19]*z[98];
    z[96]=z[58] + z[96] + z[98];
    z[98]= - z[7]*z[75];
    z[97]=z[98] - z[97];
    z[97]=z[16]*z[97];
    z[95]= - z[7] + z[95];
    z[95]=z[24]*z[95];
    z[95]=z[97] + z[95];
    z[97]=z[11]*z[75];
    z[86]=z[97] + z[86];
    z[86]=z[13]*z[86];
    z[94]= - z[11] + z[94];
    z[94]=z[27]*z[94];
    z[86]=z[86] + z[94];
    z[94]=n<T>(199,2)*z[5] + 13*z[8];
    z[81]=n<T>(1,4)*z[94] - z[81];
    z[81]=n<T>(5,9)*z[11] + n<T>(109,72)*z[10] - n<T>(5,4)*z[7] - n<T>(1,12)*z[4]
     + n<T>(103,72)*z[9] - 5*z[6]
     + n<T>(1,9)*z[81] - z[83];
    z[81]=z[29]*z[81];
    z[83]= - 7*z[11] + 2*z[7] + z[102] - z[92];
    z[83]=z[28]*z[83];
    z[92]= - z[66] + z[54] + z[48] - z[43];
    z[94]=z[64] + z[57];
    z[97]=z[68] - z[49];
    z[98]= - 10*z[36] - n<T>(5,2)*z[34] - n<T>(16,9)*z[30];
    z[98]=i*z[98];
    z[75]=2*z[11] + 5*z[7] + n<T>(25,2)*z[6] - 6*z[2] + n<T>(7,2)*z[8] - n<T>(3,2)*
    z[75] + 4*z[5];
    z[75]=z[22]*z[75];
    z[99]= - z[9] - z[11];
    z[99]=z[41]*z[99];
    z[100]=2*i;
    z[100]= - z[33]*z[100];

    r +=  - n<T>(22,3)*z[31] + n<T>(5,6)*z[32] + n<T>(13,6)*z[35] + 6*z[37] + n<T>(128,3)*z[38]
     - z[45]
     - n<T>(37,12)*z[46]
     - n<T>(3,4)*z[52]
     - n<T>(23,4)*z[53]
     - 
      z[55] + z[56] - n<T>(7,2)*z[60] - 22*z[61] + n<T>(15,4)*z[69] + z[70] - n<T>(7,12)*z[71]
     + n<T>(13,4)*z[73]
     + z[74]
     + z[75]
     + z[76]
     + z[77]
     + z[78]
       + z[79] + z[80] + z[81] + z[82] + z[83] + z[84] + z[85] + n<T>(13,3)
      *z[86] + 3*z[87] + 4*z[88] - n<T>(1,2)*z[89] + n<T>(1,3)*z[90] + z[91] - 
      n<T>(3,2)*z[92] + 2*z[93] + n<T>(2,3)*z[94] + n<T>(11,3)*z[95] + n<T>(7,3)*z[96]
       + n<T>(16,3)*z[97] + z[98] + n<T>(10,3)*z[99] + z[100];
 
    return r;
}

template std::complex<double> qg_2lha_tf736(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf736(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
