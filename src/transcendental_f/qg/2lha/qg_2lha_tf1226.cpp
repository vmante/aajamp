#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1226(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[13];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[5];
    z[3]=d[9];
    z[4]=d[6];
    z[5]=e[7];
    z[6]=c[3];
    z[7]=d[3];
    z[8]=f[33];
    z[9]=z[1]*i;
    z[9]=z[9] - z[7];
    z[10]=z[9]*z[3];
    z[10]=z[10] - n<T>(1,6)*z[6];
    z[9]=z[9] - z[3];
    z[11]= - n<T>(1,2)*z[9];
    z[12]= - z[11] + z[4];
    z[12]=z[4]*z[12];
    z[12]=z[12] - z[10];
    z[12]=z[4]*z[12];
    z[11]=z[2] + z[11] - 2*z[4];
    z[11]=z[2]*z[11];
    z[10]=z[11] + 2*z[5] + z[10];
    z[10]=z[2]*z[10];
    z[9]= - z[5]*z[9];

    r +=  - 4*z[8] + z[9] + z[10] + z[12];
 
    return r;
}

template std::complex<double> qg_2lha_tf1226(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1226(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
