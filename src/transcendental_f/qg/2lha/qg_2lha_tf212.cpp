#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf212(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[120];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[3];
    z[22]=e[4];
    z[23]=e[5];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[10];
    z[28]=e[11];
    z[29]=e[12];
    z[30]=e[13];
    z[31]=c[3];
    z[32]=c[11];
    z[33]=c[15];
    z[34]=c[17];
    z[35]=c[19];
    z[36]=c[21];
    z[37]=c[23];
    z[38]=c[25];
    z[39]=c[26];
    z[40]=c[28];
    z[41]=c[31];
    z[42]=e[6];
    z[43]=e[14];
    z[44]=f[0];
    z[45]=f[1];
    z[46]=f[2];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[10];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[13];
    z[56]=f[14];
    z[57]=f[15];
    z[58]=f[16];
    z[59]=f[17];
    z[60]=f[18];
    z[61]=f[23];
    z[62]=f[30];
    z[63]=f[31];
    z[64]=f[32];
    z[65]=f[33];
    z[66]=f[34];
    z[67]=f[35];
    z[68]=f[36];
    z[69]=f[37];
    z[70]=f[39];
    z[71]=f[41];
    z[72]=f[43];
    z[73]=f[50];
    z[74]=f[51];
    z[75]=f[52];
    z[76]=f[53];
    z[77]=f[55];
    z[78]=f[56];
    z[79]=f[58];
    z[80]=f[60];
    z[81]=f[74];
    z[82]=f[75];
    z[83]=f[78];
    z[84]=f[100];
    z[85]=f[101];
    z[86]=z[4] + n<T>(205,12)*z[9];
    z[87]=n<T>(85,2)*z[7];
    z[88]=n<T>(205,12)*z[2];
    z[89]= - n<T>(125,8)*z[3] - z[88] - n<T>(67,2)*z[11] - z[87] + n<T>(253,12)*z[6]
    + z[86];
    z[90]=n<T>(1,2)*z[3];
    z[89]=z[89]*z[90];
    z[91]=n<T>(1,2)*z[4];
    z[92]=n<T>(23,4)*z[4];
    z[93]= - z[6] - z[92];
    z[93]=z[93]*z[91];
    z[94]=n<T>(23,6)*z[9];
    z[95]=n<T>(25,18)*z[8] + z[4];
    z[95]=25*z[95] - z[94];
    z[96]=n<T>(1,2)*z[9];
    z[95]=z[95]*z[96];
    z[97]=n<T>(1,4)*z[7];
    z[98]=n<T>(625,9)*z[9] + 33*z[4];
    z[99]=n<T>(2641,18)*z[7] - n<T>(733,9)*z[8] - z[98];
    z[99]=z[99]*z[97];
    z[100]= - n<T>(205,12)*z[27] + n<T>(643,18)*z[24];
    z[101]= - z[100] - n<T>(61,6)*z[30] + 29*z[23];
    z[102]=npow(z[6],2);
    z[103]=npow(z[8],2);
    z[104]= - n<T>(7,4)*z[20] - z[19];
    z[105]=n<T>(3,4)*z[11] + n<T>(1,3)*z[7] + n<T>(33,4)*z[4] - n<T>(1,2)*z[8] + z[6];
    z[105]=z[11]*z[105];
    z[106]= - 33 + n<T>(27,2)*z[4];
    z[107]= - z[6] - n<T>(661,18)*z[8] - z[106];
    z[107]= - n<T>(19,24)*z[2] + n<T>(25,4)*z[11] + n<T>(515,18)*z[7] + n<T>(1,2)*z[107]
    - n<T>(26,3)*z[9];
    z[107]=z[2]*z[107];
    z[108]=n<T>(205,24)*z[2];
    z[109]= - n<T>(1345,96)*z[10] + n<T>(511,12)*z[3] + z[108] - n<T>(51,4)*z[11]
     + 
   n<T>(53,4)*z[7] - n<T>(205,24)*z[9] + z[8] - n<T>(233,16)*z[6];
    z[109]=z[10]*z[109];
    z[110]=n<T>(751,3)*z[8] + n<T>(193,4)*z[6];
    z[110]=n<T>(1,3)*z[110] - 23*z[4];
    z[110]= - n<T>(1393,24)*z[10] + 45*z[3] + n<T>(127,6)*z[2] + n<T>(85,3)*z[11]
     - 
   n<T>(949,18)*z[7] + n<T>(1,2)*z[110] - n<T>(23,3)*z[9];
    z[110]=n<T>(1,2)*z[110] - n<T>(83,3)*z[5];
    z[110]=z[5]*z[110];
    z[89]=n<T>(1,2)*z[110] + z[109] + z[89] + z[107] + z[105] + z[99] + 
    z[95] + z[93] + n<T>(241,96)*z[102] + n<T>(1957,72)*z[103] + n<T>(603,32)*z[31]
    + n<T>(67,6)*z[104] - z[101];
    z[89]=z[5]*z[89];
    z[93]=n<T>(41,4)*z[9];
    z[95]=n<T>(103,8)*z[11];
    z[99]=n<T>(119,9)*z[13] - z[6];
    z[99]= - z[95] + n<T>(9,4)*z[7] + 2*z[99] - z[93];
    z[99]=z[11]*z[99];
    z[104]= - 29*z[15] + n<T>(469,24)*z[12];
    z[92]=12*z[5] + n<T>(1321,48)*z[10] - n<T>(83,24)*z[3] + n<T>(77,12)*z[2] - n<T>(86,3)*z[11]
     - n<T>(863,36)*z[7]
     + z[94]
     + z[92] - n<T>(145,48)*z[6] - n<T>(715,36)*
    z[8] - z[104];
    z[92]=z[5]*z[92];
    z[94]=n<T>(1,2)*z[6];
    z[105]=11*z[14];
    z[107]=n<T>(599,144)*z[6] - z[105] + n<T>(841,18)*z[8];
    z[107]=z[107]*z[94];
    z[109]= - n<T>(697,3)*z[16] + n<T>(201,2)*z[17];
    z[110]=n<T>(661,18)*z[6] - n<T>(769,9)*z[8] + z[109];
    z[110]=n<T>(201,4)*z[9] + n<T>(1,2)*z[110] + 25*z[4];
    z[110]=z[110]*z[96];
    z[87]=n<T>(107,6)*z[3] + n<T>(217,12)*z[2] + 67*z[11] + z[87] - n<T>(565,12)*
    z[9] + n<T>(9,2)*z[4] - n<T>(319,12)*z[6] - n<T>(469,12)*z[12] - 3*z[8];
    z[87]=z[87]*z[90];
    z[90]=n<T>(293,18)*z[4];
    z[111]=z[90] + z[8];
    z[112]=n<T>(33,2)*z[17] + z[15];
    z[112]= - n<T>(101,3)*z[9] + n<T>(261,8)*z[6] + 3*z[112] - z[111];
    z[112]=n<T>(2953,288)*z[10] - n<T>(133,3)*z[3] - z[108] - n<T>(167,18)*z[11]
     + n<T>(1,2)*z[112]
     + n<T>(50,3)*z[7];
    z[112]=z[10]*z[112];
    z[113]=n<T>(3,2)*z[21];
    z[114]=z[113] - n<T>(11,2)*z[22] + n<T>(238,9)*z[29];
    z[115]= - 697*z[16] + n<T>(121,3)*z[8];
    z[115]=z[8]*z[115];
    z[116]=n<T>(238,9)*z[13];
    z[117]=n<T>(341,36)*z[4] + n<T>(31,4)*z[6] + z[8] - n<T>(11,2)*z[14] + z[116];
    z[117]=z[4]*z[117];
    z[118]=2*z[15] - n<T>(11,8)*z[6];
    z[118]=n<T>(153,16)*z[7] + n<T>(5059,72)*z[9] + n<T>(71,3)*z[118] - n<T>(179,4)*z[4]
   ;
    z[118]=z[7]*z[118];
    z[119]= - 73*z[4] + n<T>(679,9)*z[8] - z[6];
    z[119]=n<T>(233,6)*z[2] - 10*z[11] - n<T>(442,9)*z[7] + n<T>(1,4)*z[119] - n<T>(49,3)
   *z[9];
    z[119]=z[2]*z[119];
    z[87]=z[92] + z[112] + z[87] + z[119] + z[99] + z[118] + z[110] + 
    z[117] + z[107] + n<T>(1,12)*z[115] - n<T>(33,2)*z[15] + 23*z[18] + n<T>(67,3)*
    z[19] + n<T>(469,24)*z[20] + n<T>(46,3)*z[25] + n<T>(697,12)*z[26] + n<T>(201,8)*
    z[28] + z[101] + z[114];
    z[87]=z[1]*z[87];
    z[92]= - 242*z[39] - n<T>(293,2)*z[37];
    z[87]=z[87] - n<T>(9775,432)*z[32] - n<T>(3,2)*z[34] + n<T>(1,3)*z[92] + 20*
    z[36];
    z[87]=i*z[87];
    z[92]=z[6] + n<T>(73,4)*z[4];
    z[92]=z[92]*z[91];
    z[98]= - n<T>(3031,18)*z[7] + n<T>(661,9)*z[8] + z[98];
    z[97]=z[98]*z[97];
    z[98]=n<T>(1,2)*z[11];
    z[99]= - z[6] - 11*z[4];
    z[99]=3*z[99] - 25*z[7];
    z[99]=n<T>(1,2)*z[99] + 9*z[11];
    z[99]=z[99]*z[98];
    z[101]=z[94] + n<T>(607,18)*z[8] + z[106];
    z[101]= - n<T>(61,4)*z[2] + 6*z[11] - n<T>(23,9)*z[7] + n<T>(1,4)*z[101] + n<T>(13,3)
   *z[9];
    z[101]=z[2]*z[101];
    z[106]=n<T>(23,2)*z[18];
    z[107]=n<T>(31,8)*z[102];
    z[110]= - 23*z[25] - n<T>(67,2)*z[19];
    z[112]= - n<T>(625,6)*z[8] + 49*z[9];
    z[112]=z[9]*z[112];
    z[92]=z[101] + z[99] + z[97] + n<T>(1,6)*z[112] + z[92] + z[107] + n<T>(317,18)*z[103]
     + n<T>(829,36)*z[31]
     + n<T>(1,3)*z[110] - z[106] - z[100];
    z[92]=z[2]*z[92];
    z[97]=7*z[4];
    z[99]=n<T>(1,4)*z[6] - n<T>(17,9)*z[4];
    z[99]=z[99]*z[97];
    z[93]=z[8] + z[93];
    z[93]=z[93]*z[96];
    z[101]= - n<T>(247,12)*z[11] + n<T>(313,12)*z[7] + n<T>(61,4)*z[9] - 7*z[8] - 
    z[6];
    z[98]=z[101]*z[98];
    z[101]= - n<T>(293,2)*z[43] - 238*z[29];
    z[110]= - n<T>(317,12)*z[7] - z[9] + n<T>(25,3)*z[4] + 13 + 15*z[8];
    z[110]=z[7]*z[110];
    z[93]=z[98] + n<T>(1,2)*z[110] + z[93] + z[99] - n<T>(7,2)*z[103] - n<T>(4691,216)*z[31]
     + z[113]
     + n<T>(1,9)*z[101] - n<T>(239,4)*z[30];
    z[93]=z[11]*z[93];
    z[98]=715*z[8] - n<T>(661,8)*z[6];
    z[98]= - n<T>(67,2)*z[9] + n<T>(1,9)*z[98] - n<T>(25,2)*z[4];
    z[98]=z[98]*z[96];
    z[99]=npow(z[4],2);
    z[101]= - n<T>(201,2)*z[28] - n<T>(697,3)*z[26];
    z[110]= - 841*z[8] - n<T>(661,4)*z[6];
    z[110]=z[6]*z[110];
    z[98]=z[98] - n<T>(25,4)*z[99] + n<T>(1,36)*z[110] + n<T>(769,72)*z[103] + n<T>(577,36)*z[31]
     + n<T>(1,4)*z[101] - n<T>(23,3)*z[25]
     + z[100];
    z[98]=z[9]*z[98];
    z[90]= - 7*z[6] + z[90];
    z[90]=z[90]*z[91];
    z[100]=z[21] + z[23];
    z[101]= - z[8] - n<T>(95,12)*z[9];
    z[101]=z[9]*z[101];
    z[110]=n<T>(61,6)*z[7] + z[9] - z[8] - n<T>(365,6)*z[4];
    z[110]=z[7]*z[110];
    z[90]=z[110] + z[101] + z[90] + n<T>(237,16)*z[102] + z[103] + n<T>(6107,216)
   *z[31] - n<T>(1273,12)*z[27] - n<T>(127,2)*z[30] - n<T>(293,9)*z[43] - n<T>(99,2)*
    z[28] - 3*z[100];
    z[96]= - z[96] + n<T>(401,9)*z[4] - z[8] + n<T>(7,2)*z[6];
    z[95]= - z[95] + n<T>(1,2)*z[96] + 20*z[7];
    z[95]=z[11]*z[95];
    z[96]=n<T>(1,2)*z[2];
    z[100]=z[9] - z[96];
    z[100]=z[100]*z[108];
    z[97]= - 53*z[7] + n<T>(205,6)*z[9] - z[6] + z[97];
    z[97]= - n<T>(251,12)*z[3] - z[108] + n<T>(1,4)*z[97] + 11*z[11];
    z[97]=z[3]*z[97];
    z[101]= - n<T>(221,4)*z[10] + n<T>(305,12)*z[3] - n<T>(205,6)*z[2] - n<T>(1423,18)*
    z[11] + n<T>(575,6)*z[7] + n<T>(395,12)*z[9] + n<T>(237,8)*z[6] + z[111];
    z[101]=z[10]*z[101];
    z[90]=n<T>(1,4)*z[101] + z[97] + z[100] + n<T>(1,2)*z[90] + z[95];
    z[90]=z[10]*z[90];
    z[95]= - n<T>(365,18)*z[4] - z[8] - n<T>(9,4)*z[6];
    z[91]=z[95]*z[91];
    z[91]=z[91] - z[107] - n<T>(3,4)*z[103] + n<T>(407,72)*z[31] - z[106] - 
    z[114];
    z[91]=z[4]*z[91];
    z[95]= - n<T>(5059,144)*z[9] + 2*z[8] + n<T>(781,24)*z[6];
    z[95]=z[9]*z[95];
    z[97]=n<T>(3913,108)*z[7] - n<T>(829,48)*z[9] + n<T>(105,8)*z[4] - n<T>(781,48)*z[6]
    - n<T>(79,4) - n<T>(353,9)*z[8];
    z[97]=z[7]*z[97];
    z[95]=z[97] + z[95] + n<T>(179,8)*z[99] - n<T>(781,48)*z[102] - n<T>(4277,72)*
    z[103] + n<T>(4,9)*z[31] + n<T>(5275,36)*z[24] - n<T>(142,3)*z[23] - n<T>(205,4)*
    z[30];
    z[95]=z[7]*z[95];
    z[97]= - n<T>(3,4)*z[4] + z[8] + n<T>(3,2)*z[6];
    z[97]=z[4]*z[97];
    z[97]=z[103] - z[97];
    z[99]=npow(z[9],2);
    z[100]=npow(z[7],2);
    z[97]=n<T>(31,2)*z[100] + n<T>(565,24)*z[99] - n<T>(119,6)*z[102] - n<T>(1141,36)*
    z[31] - n<T>(863,12)*z[27] + n<T>(265,12)*z[21] - n<T>(469,12)*z[20] - 9*z[30]
    - 3*z[97];
    z[86]=z[108] + z[6] - z[86];
    z[86]=z[86]*z[96];
    z[96]=n<T>(5,4)*z[11] + n<T>(79,4)*z[7] - 15*z[9] - n<T>(7,4)*z[4] + 10 - n<T>(11,4)
   *z[6];
    z[96]=z[11]*z[96];
    z[88]=n<T>(205,144)*z[3] + z[88] + n<T>(9,4)*z[11] + n<T>(1,8)*z[7] + n<T>(155,48)*
    z[9] - n<T>(15,8)*z[4] - n<T>(247,48)*z[6] - 5 - n<T>(3,4)*z[8];
    z[88]=z[3]*z[88];
    z[86]=z[88] + z[86] + n<T>(1,2)*z[97] + z[96];
    z[86]=z[3]*z[86];
    z[88]=n<T>(1,2)*z[109] - z[105];
    z[88]=n<T>(1,2)*z[88] + z[116] - z[104];
    z[88]=z[31]*z[88];
    z[96]=n<T>(3989,3)*z[24] + n<T>(661,3)*z[42] - 697*z[26];
    z[96]=n<T>(3871,72)*z[103] + n<T>(1,4)*z[96] - n<T>(244,9)*z[31];
    z[96]=z[8]*z[96];
    z[97]=z[8] + z[6];
    z[97]=z[6]*z[97];
    z[97]=n<T>(841,36)*z[97] + 5*z[103] + n<T>(493,54)*z[31] + n<T>(229,12)*z[21]
     + 
   n<T>(661,18)*z[42] + 11*z[22];
    z[94]=z[97]*z[94];
    z[97]=z[23] - z[18];
    z[99]= - z[83] + z[81] - z[82];
    z[99]=z[85] - z[84] + 3*z[99];

    r +=  - 10*z[27] - n<T>(73,9)*z[33] - n<T>(283,36)*z[35] + n<T>(61,4)*z[38] + 
      n<T>(91,3)*z[40] + n<T>(1603,12)*z[41] + n<T>(15,2)*z[44] - n<T>(31,8)*z[45]
     +  n<T>(211,24)*z[46] - n<T>(23,8)*z[47] - n<T>(2117,96)*z[48] - n<T>(55,6)*z[49]
     - 
      n<T>(329,16)*z[50] - n<T>(205,3)*z[51] - n<T>(883,24)*z[52] - n<T>(23,12)*z[53]
       - n<T>(241,96)*z[54] + n<T>(9,4)*z[55] - n<T>(9,8)*z[56] + n<T>(15,4)*z[57]
     +  n<T>(213,32)*z[58] - n<T>(25,4)*z[59] + n<T>(5,2)*z[60] + n<T>(205,24)*z[61]
     - 
      n<T>(841,36)*z[62] - n<T>(7703,144)*z[63] - n<T>(1021,24)*z[64] - n<T>(1286,9)*
      z[65] + n<T>(51,8)*z[66] - n<T>(715,36)*z[67] + n<T>(661,144)*z[68] - z[69]
       - n<T>(709,48)*z[70] + n<T>(1,2)*z[71] + n<T>(643,36)*z[72] + n<T>(775,36)*z[73]
       + n<T>(179,8)*z[74] - n<T>(5,4)*z[75] + n<T>(235,6)*z[76] - n<T>(293,72)*z[77]
       + n<T>(89,8)*z[78] + n<T>(70,3)*z[79] - 6*z[80] + z[86] + z[87] + z[88]
       + z[89] + z[90] + z[91] + z[92] + z[93] + z[94] + z[95] + n<T>(1,3)*
      z[96] + n<T>(33,2)*z[97] + z[98] + n<T>(1,4)*z[99];
 
    return r;
}

template std::complex<double> qg_2lha_tf212(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf212(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
