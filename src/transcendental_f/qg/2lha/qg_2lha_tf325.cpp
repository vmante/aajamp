#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf325(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[114];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[19];
    z[34]=c[21];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[31];
    z[40]=e[3];
    z[41]=e[6];
    z[42]=e[14];
    z[43]=f[0];
    z[44]=f[1];
    z[45]=f[2];
    z[46]=f[3];
    z[47]=f[4];
    z[48]=f[5];
    z[49]=f[6];
    z[50]=f[7];
    z[51]=f[9];
    z[52]=f[10];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[13];
    z[56]=f[14];
    z[57]=f[15];
    z[58]=f[16];
    z[59]=f[17];
    z[60]=f[18];
    z[61]=f[21];
    z[62]=f[23];
    z[63]=f[30];
    z[64]=f[31];
    z[65]=f[32];
    z[66]=f[33];
    z[67]=f[34];
    z[68]=f[36];
    z[69]=f[37];
    z[70]=f[39];
    z[71]=f[41];
    z[72]=f[43];
    z[73]=f[50];
    z[74]=f[51];
    z[75]=f[52];
    z[76]=f[53];
    z[77]=f[54];
    z[78]=f[55];
    z[79]=f[56];
    z[80]=f[58];
    z[81]=f[74];
    z[82]=f[75];
    z[83]=n<T>(1,2)*z[5];
    z[84]=n<T>(1,2)*z[3];
    z[85]=n<T>(1,2)*z[8];
    z[86]=z[1]*i;
    z[87]= - n<T>(5,4)*z[9] + z[85] - z[84] + z[86] + z[83];
    z[88]=n<T>(1,3)*z[9];
    z[87]=z[87]*z[88];
    z[89]=13*z[5];
    z[90]=41*z[86] - z[89];
    z[90]=z[5]*z[90];
    z[91]=19*z[86];
    z[92]=35*z[5];
    z[93]=n<T>(29,2)*z[3] - z[91] - z[92];
    z[93]=z[3]*z[93];
    z[90]=z[90] + z[93];
    z[93]=13*z[86];
    z[94]= - n<T>(13,2)*z[8] - z[84] + z[93] + n<T>(17,4)*z[5];
    z[94]=z[8]*z[94];
    z[90]=n<T>(1,4)*z[90] + z[94];
    z[94]=z[6] - z[5];
    z[95]=39*z[8];
    z[94]=z[95] - 37*z[86] - 41*z[94];
    z[94]=n<T>(1,2)*z[94] + z[9];
    z[94]=n<T>(1,2)*z[94] - 10*z[7];
    z[94]=z[7]*z[94];
    z[93]= - z[95] + z[93] - 25*z[5];
    z[93]=n<T>(1,2)*z[93] + 37*z[6];
    z[95]=n<T>(1,2)*z[6];
    z[93]=z[93]*z[95];
    z[96]=23*z[86];
    z[97]= - z[96] - 29*z[5];
    z[97]= - n<T>(17,8)*z[8] + n<T>(1,4)*z[97] + 7*z[3];
    z[88]=n<T>(49,24)*z[2] - n<T>(43,8)*z[7] - z[88] + n<T>(1,3)*z[97] + n<T>(51,8)*z[6]
   ;
    z[88]=z[2]*z[88];
    z[87]=z[88] + z[94] + z[87] + n<T>(1,3)*z[90] + z[93];
    z[87]=z[2]*z[87];
    z[88]=z[83] - z[86];
    z[90]=z[88]*z[5];
    z[93]=3*z[3];
    z[94]=z[86] + z[93];
    z[94]=z[3]*z[94];
    z[97]=z[85] - z[86];
    z[98]= - z[5] + z[97];
    z[99]=n<T>(19,2)*z[8];
    z[98]=z[98]*z[99];
    z[100]= - z[86] + z[95];
    z[101]=15*z[6];
    z[100]=z[100]*z[101];
    z[102]=n<T>(23,6)*z[9] - n<T>(23,3)*z[86] - 17*z[6];
    z[102]=z[9]*z[102];
    z[94]=z[102] + z[100] + z[98] + n<T>(5,6)*z[90] + z[94];
    z[98]=z[96] + z[83];
    z[98]=n<T>(23,3)*z[9] + z[101] + z[99] + n<T>(5,3)*z[98] - z[3];
    z[98]= - n<T>(25,8)*z[4] - n<T>(31,24)*z[2] + n<T>(1,8)*z[98] + z[7];
    z[98]=z[4]*z[98];
    z[99]=n<T>(3,2)*z[3];
    z[100]=2*z[86];
    z[101]=n<T>(3,4)*z[7] - z[100] - z[99];
    z[101]=z[7]*z[101];
    z[89]= - n<T>(13,2)*z[2] + n<T>(31,2)*z[86] + z[89];
    z[89]=z[2]*z[89];
    z[102]=n<T>(1,2)*z[10];
    z[103]= - z[86] + z[102];
    z[103]=z[10]*z[103];
    z[89]=z[98] + n<T>(7,4)*z[103] + n<T>(1,6)*z[89] + n<T>(1,4)*z[94] + z[101];
    z[89]=z[4]*z[89];
    z[94]=3*z[5];
    z[98]= - n<T>(37,6)*z[9] + 3*z[6] + z[93] - n<T>(19,3)*z[86] - z[94];
    z[98]=z[9]*z[98];
    z[101]=z[3] - z[97];
    z[101]=z[8]*z[101];
    z[93]= - n<T>(23,2)*z[6] - z[93] - 35*z[86] + 33*z[5];
    z[93]=z[6]*z[93];
    z[93]=z[98] + z[101] + z[93];
    z[98]= - z[84] - z[86] + n<T>(3,4)*z[5];
    z[98]=z[3]*z[98];
    z[91]=n<T>(3,2)*z[9] - n<T>(35,2)*z[6] - z[85] - z[3] - z[91] + n<T>(39,2)*z[5];
    z[91]= - n<T>(17,12)*z[11] + n<T>(1,4)*z[91] + z[7];
    z[91]=z[11]*z[91];
    z[101]=z[86] - z[5];
    z[103]=2*z[101] + z[7];
    z[103]=z[7]*z[103];
    z[104]= - n<T>(17,12)*z[4] + z[95] + n<T>(7,3)*z[9];
    z[104]=z[4]*z[104];
    z[91]=z[91] + z[104] + z[103] - n<T>(25,4)*z[90] + z[98] + n<T>(1,4)*z[93];
    z[91]=z[11]*z[91];
    z[93]=5*z[86];
    z[98]=z[84] + z[93] - z[94];
    z[98]=z[3]*z[98];
    z[103]=29*z[86] - n<T>(31,2)*z[5];
    z[83]=z[103]*z[83];
    z[83]=z[83] + z[98];
    z[98]=z[86] - z[8];
    z[103]=n<T>(115,6)*z[6] + z[84] + n<T>(41,4)*z[5] - n<T>(2,3)*z[98];
    z[103]=z[6]*z[103];
    z[104]=n<T>(1,2)*z[9];
    z[105]=z[3] - z[8];
    z[105]=z[105]*z[104];
    z[106]=n<T>(119,3)*z[86] - 39*z[5];
    z[106]=n<T>(1,2)*z[106] + z[3];
    z[106]=n<T>(1,2)*z[106] - n<T>(59,3)*z[8];
    z[106]=z[8]*z[106];
    z[107]= - n<T>(95,8)*z[7] + n<T>(193,8)*z[6] - n<T>(131,24)*z[8] + z[3] - n<T>(31,3)
   *z[86] - n<T>(35,8)*z[5];
    z[107]=z[7]*z[107];
    z[83]=z[107] + z[105] + z[103] + n<T>(1,2)*z[83] + z[106];
    z[83]=z[7]*z[83];
    z[103]= - 29*z[101] - z[84];
    z[105]=n<T>(1,4)*z[3];
    z[103]=z[103]*z[105];
    z[94]=n<T>(65,12)*z[8] - n<T>(65,6)*z[86] + z[94];
    z[94]=z[8]*z[94];
    z[96]=z[96] - n<T>(45,4)*z[5];
    z[96]=z[5]*z[96];
    z[106]= - 77 - n<T>(587,6)*z[86];
    z[106]= - n<T>(227,8)*z[6] + n<T>(329,48)*z[8] - n<T>(7,2)*z[3] + n<T>(1,8)*z[106]
    - 15*z[5];
    z[106]=z[6]*z[106];
    z[94]=z[106] + n<T>(13,4)*z[94] + z[96] + z[103];
    z[94]=z[6]*z[94];
    z[96]=n<T>(43,2)*z[3] + 55*z[86] - 49*z[5];
    z[96]=z[3]*z[96];
    z[90]= - 23*z[90] + z[96];
    z[96]= - z[93] - z[5];
    z[96]=z[85] + n<T>(1,2)*z[96] - z[3];
    z[96]=z[8]*z[96];
    z[90]=n<T>(1,4)*z[90] + z[96];
    z[96]=n<T>(7,2)*z[6] - z[99] + 7*z[86] + n<T>(3,2)*z[5];
    z[95]=z[96]*z[95];
    z[96]= - 23*z[5] + 101*z[3];
    z[96]=n<T>(1,2)*z[96] + 5*z[8];
    z[96]= - n<T>(73,18)*z[9] + n<T>(1,6)*z[96] + 5*z[6];
    z[96]=z[96]*z[104];
    z[90]=z[96] + n<T>(1,3)*z[90] + z[95];
    z[90]=z[9]*z[90];
    z[88]= - z[88]*z[92];
    z[95]=n<T>(31,2)*z[3] + 43*z[86] - 37*z[5];
    z[95]=z[3]*z[95];
    z[88]=z[88] + z[95];
    z[95]=n<T>(33,8)*z[8] - n<T>(33,4)*z[86] - z[3];
    z[95]=z[8]*z[95];
    z[96]=n<T>(115,8)*z[6] - n<T>(115,4)*z[8] + z[3] + n<T>(119,4)*z[86] - z[5];
    z[96]=z[6]*z[96];
    z[88]=z[96] + n<T>(1,6)*z[88] + z[95];
    z[95]= - n<T>(13,4)*z[9] + z[85] + z[84] - n<T>(13,2)*z[86] + 6*z[5];
    z[95]=z[9]*z[95];
    z[92]=113*z[3] - n<T>(29,2)*z[86] - z[92];
    z[92]=n<T>(115,2)*z[6] + n<T>(1,3)*z[92] + n<T>(33,2)*z[8];
    z[92]= - n<T>(113,6)*z[10] - n<T>(7,2)*z[2] - n<T>(41,2)*z[7] + n<T>(1,2)*z[92]
     - 13
   *z[9];
    z[92]=z[10]*z[92];
    z[96]= - z[9] + n<T>(41,2)*z[8] - n<T>(43,2)*z[86] + z[5];
    z[96]=n<T>(1,2)*z[96] - z[7];
    z[96]=z[7]*z[96];
    z[88]=n<T>(1,4)*z[92] + z[96] + n<T>(1,2)*z[88] + z[95];
    z[88]=z[10]*z[88];
    z[92]=z[105] - n<T>(5,2)*z[86] + z[5];
    z[92]=z[3]*z[92];
    z[95]= - 47*z[86] + n<T>(23,2)*z[5];
    z[95]=z[5]*z[95];
    z[92]=n<T>(1,4)*z[95] + z[92];
    z[95]=n<T>(5,6)*z[3] + n<T>(47,12)*z[5] + z[98];
    z[95]=z[95]*z[85];
    z[92]=n<T>(1,3)*z[92] + z[95];
    z[85]=z[92]*z[85];
    z[92]=z[86]*z[8];
    z[92]=z[92] + z[30];
    z[95]=z[9]*z[86];
    z[95]=z[95] + z[92];
    z[95]=z[17]*z[95];
    z[96]= - z[9] + z[98];
    z[96]=z[27]*z[96];
    z[95]= - z[69] - z[95] - z[96] + z[77] - z[71];
    z[96]=z[86]*z[4];
    z[96]=z[96] + z[30];
    z[99]=z[10]*z[86];
    z[99]=z[99] + z[96];
    z[99]=z[14]*z[99];
    z[103]=z[86] - z[4];
    z[104]= - z[10] + z[103];
    z[104]=z[21]*z[104];
    z[99]= - z[99] - z[104] + z[61] + z[43];
    z[104]=z[7]*z[86];
    z[92]=z[104] + z[92];
    z[92]=z[16]*z[92];
    z[98]=z[7] - z[98];
    z[98]=z[25]*z[98];
    z[92]=z[92] + z[98];
    z[98]=z[11]*z[86];
    z[96]=z[98] + z[96];
    z[96]=z[13]*z[96];
    z[98]= - z[11] + z[103];
    z[98]=z[28]*z[98];
    z[96]=z[96] + z[98];
    z[98]= - z[5] - z[3];
    z[98]=z[86]*z[98];
    z[98]= - z[30] + z[98];
    z[98]=z[12]*z[98];
    z[103]= - z[3] + z[101];
    z[103]=z[20]*z[103];
    z[98]=z[98] + z[103];
    z[103]=n<T>(29,3)*z[5];
    z[104]=n<T>(11,4) - 3*z[86];
    z[104]=n<T>(7,2)*z[104] + z[103];
    z[104]=z[104]*npow(z[5],2);
    z[93]= - z[93] + z[103];
    z[93]=n<T>(1,2)*z[93] - n<T>(227,9)*z[3];
    z[93]=z[93]*z[84];
    z[103]= - z[86] + n<T>(35,6)*z[5];
    z[103]=z[5]*z[103];
    z[93]=z[103] + z[93];
    z[84]=z[93]*z[84];
    z[93]=z[86] + z[2];
    z[93]=z[102] - n<T>(587,12)*z[7] - n<T>(815,12)*z[6] - 21*z[8] - z[3] - 19*
    z[5] + 20*z[93];
    z[93]=z[23]*z[93];
    z[100]= - z[2] + z[100] - z[5];
    z[102]=z[6] - z[3];
    z[100]= - z[102] + n<T>(8,3)*z[100];
    z[100]=z[19]*z[100];
    z[102]= - z[9] + z[101] - z[102];
    z[102]=z[29]*z[102];
    z[103]=18*z[5] + n<T>(77,4) + 4*z[6];
    z[105]= - z[86]*z[103];
    z[105]= - 18*z[30] + z[105];
    z[105]=z[15]*z[105];
    z[103]= - 18*z[86] + z[103];
    z[103]=z[22]*z[103];
    z[106]=n<T>(1,2)*z[2];
    z[86]=n<T>(1,2)*z[4] - z[86] + z[106];
    z[86]=z[18]*z[86];
    z[97]=z[106] + z[97];
    z[97]=z[24]*z[97];
    z[106]= - z[51] + z[82] - z[81];
    z[107]=z[48] - z[36];
    z[108]=z[72] - z[63];
    z[109]= - z[9] - z[11];
    z[109]=z[42]*z[109];
    z[109]=z[109] - z[45];
    z[110]= - n<T>(41,12)*z[37] - n<T>(11,24)*z[35] + n<T>(19,4)*z[34] + n<T>(179,24)*
    z[31];
    z[110]=i*z[110];
    z[111]= - 541*z[5] + 89*z[3];
    z[111]= - n<T>(5,6)*z[9] - n<T>(773,6)*z[6] + n<T>(1,12)*z[111] + 13*z[8];
    z[111]=n<T>(1,3)*z[111] + z[7];
    z[111]=n<T>(329,144)*z[11] + n<T>(565,144)*z[4] - n<T>(47,144)*z[10] + n<T>(1,2)*
    z[111] + n<T>(16,9)*z[2];
    z[111]=z[30]*z[111];
    z[101]= - z[8] + z[101] + z[2];
    z[101]= - n<T>(29,6)*z[9] - n<T>(9,2)*z[3] + n<T>(1,3)*z[101];
    z[101]=z[26]*z[101];
    z[112]= - z[3] - z[10];
    z[112]=z[40]*z[112];
    z[113]= - z[7] - z[10];
    z[113]=z[41]*z[113];

    r +=  - n<T>(251,12)*z[32] + n<T>(17,36)*z[33] - n<T>(2,3)*z[38] - n<T>(349,24)*
      z[39] + n<T>(7,8)*z[44] + n<T>(5,48)*z[46] - n<T>(9,8)*z[47] - n<T>(3,8)*z[49]
     - 
      n<T>(4,3)*z[50] + n<T>(7,2)*z[52] + n<T>(47,48)*z[53] + n<T>(35,24)*z[54] + n<T>(13,6)
      *z[55] + n<T>(13,8)*z[56] + n<T>(3,4)*z[57] - 3*z[58] + n<T>(19,16)*z[59] - n<T>(7,12)*z[60]
     + n<T>(1,6)*z[62]
     + n<T>(1313,48)*z[64]
     + n<T>(109,8)*z[65]
     + 81*
      z[66] + 2*z[67] - n<T>(33,16)*z[68] + n<T>(107,16)*z[70] + n<T>(7,3)*z[73]
     +  n<T>(15,8)*z[74] - n<T>(37,8)*z[75] + 6*z[76] - n<T>(23,24)*z[78] - n<T>(1,8)*
      z[79] + n<T>(11,4)*z[80] + z[83] + z[84] + z[85] + n<T>(5,6)*z[86] + 
      z[87] + z[88] + z[89] + z[90] + z[91] + n<T>(353,12)*z[92] + z[93] + 
      z[94] - n<T>(1,2)*z[95] + n<T>(17,6)*z[96] + n<T>(35,6)*z[97] + n<T>(31,12)*z[98]
       - n<T>(7,4)*z[99] + z[100] + z[101] + n<T>(3,2)*z[102] + z[103] + z[104]
       + z[105] + n<T>(1,4)*z[106] + n<T>(49,24)*z[107] - n<T>(39,4)*z[108] + n<T>(23,6)
      *z[109] + z[110] + z[111] + n<T>(41,6)*z[112] + n<T>(33,4)*z[113];
 
    return r;
}

template std::complex<double> qg_2lha_tf325(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf325(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
