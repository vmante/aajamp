#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf490(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=c[12];
    z[5]=c[13];
    z[6]=c[20];
    z[7]=c[23];
    z[8]=f[44];
    z[9]=f[42];
    z[10]=f[62];
    z[11]=f[77];
    z[12]=f[79];
    z[13]=g[82];
    z[14]=g[90];
    z[15]=g[101];
    z[16]=g[131];
    z[17]=g[214];
    z[18]=g[245];
    z[19]=g[345];
    z[20]=g[347];
    z[21]=g[348];
    z[22]= - 2*z[6] - n<T>(24,5)*z[5];
    z[23]=z[3] - z[2];
    z[24]= - i*z[23];
    z[25]=7*i - 6*z[24];
    z[22]=z[25]*z[22];
    z[25]=z[14] - z[10];
    z[26]= - n<T>(1,3) + z[23];
    z[26]=z[4]*z[26];
    z[26]= - z[12] + z[26] + z[21];
    z[23]=1 + 3*z[23];
    z[23]=z[8]*z[23];
    z[23]= - z[15] + z[23] + z[20];
    z[27]= - z[13] + z[17] - z[16];
    z[28]=n<T>(137,6)*i - 11*z[24];
    z[28]=z[1]*z[28];
    z[24]=n<T>(7,2)*i - 3*z[24];
    z[24]=z[7]*z[24];

    r += z[9] - z[11] + 9*z[18] + z[19] + z[22] + 2*z[23] + n<T>(1,5)*z[24]
       + 6*z[25] + 4*z[26] + 3*z[27] + n<T>(1,45)*z[28];
 
    return r;
}

template std::complex<double> qg_2lha_tf490(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf490(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
