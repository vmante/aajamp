#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf184(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[108];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[5];
    z[21]=e[7];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=e[10];
    z[25]=e[11];
    z[26]=e[12];
    z[27]=e[13];
    z[28]=c[3];
    z[29]=c[11];
    z[30]=c[15];
    z[31]=c[19];
    z[32]=c[21];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[31];
    z[38]=e[3];
    z[39]=e[6];
    z[40]=e[14];
    z[41]=f[0];
    z[42]=f[2];
    z[43]=f[3];
    z[44]=f[4];
    z[45]=f[5];
    z[46]=f[6];
    z[47]=f[7];
    z[48]=f[8];
    z[49]=f[9];
    z[50]=f[10];
    z[51]=f[11];
    z[52]=f[12];
    z[53]=f[13];
    z[54]=f[16];
    z[55]=f[17];
    z[56]=f[23];
    z[57]=f[30];
    z[58]=f[31];
    z[59]=f[32];
    z[60]=f[33];
    z[61]=f[34];
    z[62]=f[35];
    z[63]=f[36];
    z[64]=f[39];
    z[65]=f[43];
    z[66]=f[50];
    z[67]=f[51];
    z[68]=f[52];
    z[69]=f[53];
    z[70]=f[55];
    z[71]=f[56];
    z[72]=f[58];
    z[73]=f[60];
    z[74]=f[83];
    z[75]=4*z[5];
    z[76]=n<T>(8,3)*z[6];
    z[77]=z[1]*i;
    z[78]= - n<T>(13,2) - 8*z[77];
    z[78]=z[76] + n<T>(1,3)*z[78] + z[75];
    z[78]=z[6]*z[78];
    z[79]=n<T>(1,2)*z[8];
    z[80]=2*z[5];
    z[81]= - n<T>(25,6)*z[10] - n<T>(1,2)*z[3] + 6*z[6] + z[79] + n<T>(3,2)*z[77]
     - 
    z[80];
    z[81]=z[10]*z[81];
    z[82]=2*z[77];
    z[83]=z[82] - z[5];
    z[84]=z[83]*z[80];
    z[85]=z[79] - z[77];
    z[86]=3*z[8];
    z[87]=z[85]*z[86];
    z[88]=3*z[77];
    z[89]=z[8] - z[5];
    z[90]= - n<T>(3,2)*z[2] - z[6] + z[88] + z[89];
    z[90]=z[2]*z[90];
    z[91]=z[8] + z[5];
    z[92]= - n<T>(4,3)*z[9] + z[2] - z[6] - n<T>(11,3)*z[77] - z[91];
    z[92]=z[9]*z[92];
    z[93]=z[77] - z[5];
    z[94]=z[9] - z[6] - n<T>(10,3) - z[93];
    z[94]=z[3]*z[94];
    z[78]=z[81] + z[94] + z[92] + z[90] + z[78] + z[84] + z[87];
    z[78]=z[10]*z[78];
    z[81]=n<T>(11,12)*z[9];
    z[84]=2*z[8];
    z[87]=7*z[77] + 17*z[5];
    z[87]= - n<T>(11,6)*z[4] - n<T>(8,3)*z[10] + z[81] - n<T>(29,6)*z[2] + n<T>(47,12)*
    z[6] + n<T>(1,6)*z[87] + z[84];
    z[87]=z[4]*z[87];
    z[90]=n<T>(1,2)*z[5];
    z[92]=z[90] - z[77];
    z[92]=z[92]*z[5];
    z[94]=4*z[77];
    z[95]=z[84] - z[94] - 5*z[5];
    z[95]=z[8]*z[95];
    z[96]=29*z[77] - z[80];
    z[96]= - n<T>(1,6)*z[2] + z[6] + n<T>(1,3)*z[96] + z[8];
    z[96]=z[2]*z[96];
    z[81]=z[81] - z[2] - n<T>(13,2)*z[6] - n<T>(11,6)*z[77] + z[8];
    z[81]=z[9]*z[81];
    z[97]=z[77] - z[8];
    z[98]=n<T>(1,3)*z[6];
    z[99]=n<T>(14,3)*z[9] - z[98] + z[5] + z[97];
    z[99]=z[10]*z[99];
    z[100]=n<T>(41,12)*z[6] - n<T>(47,6)*z[77] - z[5];
    z[100]=z[6]*z[100];
    z[81]=z[87] + z[99] + z[81] + z[96] + z[100] + n<T>(17,3)*z[92] + z[95];
    z[81]=z[4]*z[81];
    z[87]=z[11] + z[7];
    z[95]=z[87] - z[77];
    z[96]=n<T>(1,2)*z[6];
    z[99]=2*z[3];
    z[90]=z[99] - n<T>(3,2)*z[9] + z[96] + n<T>(1,6)*z[8] - z[90] - n<T>(1,3)*z[95];
    z[90]=z[11]*z[90];
    z[95]=n<T>(1,3)*z[8];
    z[85]=z[85]*z[95];
    z[96]=z[96] + z[97];
    z[96]=z[6]*z[96];
    z[100]=n<T>(1,2)*z[9];
    z[101]= - z[100] - z[93];
    z[101]=z[9]*z[101];
    z[102]=n<T>(2,3)*z[7];
    z[103]= - z[97]*z[102];
    z[104]=2*z[93];
    z[105]=z[104] + z[3];
    z[105]=z[3]*z[105];
    z[85]=z[90] + z[103] + z[105] + 3*z[101] + z[96] - z[92] + z[85];
    z[85]=z[11]*z[85];
    z[90]= - z[77] + z[80];
    z[90]=z[90]*z[80];
    z[96]=z[91] - z[77];
    z[101]= - 2*z[96] + z[2];
    z[101]=z[2]*z[101];
    z[103]=z[104] - z[8];
    z[104]=2*z[2];
    z[105]=5*z[9] - z[104] - 2*z[103] - z[6];
    z[105]=z[9]*z[105];
    z[106]= - n<T>(11,3)*z[3] - 2*z[9] + 4*z[2] + n<T>(5,3) + z[94] - z[91];
    z[106]=z[3]*z[106];
    z[103]= - z[8]*z[103];
    z[107]=n<T>(3,2)*z[6] + z[93];
    z[107]=z[6]*z[107];
    z[90]=z[106] + z[105] + z[101] + z[107] + z[90] + z[103];
    z[90]=z[3]*z[90];
    z[101]= - 32*z[77] - n<T>(7,2)*z[5];
    z[101]=n<T>(14,3)*z[2] - z[98] + n<T>(1,3)*z[101] - z[84];
    z[101]=z[2]*z[101];
    z[89]=z[82] - z[89];
    z[89]=z[89]*z[86];
    z[94]=5*z[6] - 4*z[8] + z[94] - z[5];
    z[94]=z[94]*z[98];
    z[89]=z[101] + z[94] - n<T>(23,3)*z[92] + z[89];
    z[89]=z[2]*z[89];
    z[79]= - z[79] - z[88] - z[80];
    z[79]=z[8]*z[79];
    z[88]= - n<T>(1,2)*z[2] + z[80] - z[97];
    z[88]=z[2]*z[88];
    z[86]= - n<T>(19,3)*z[9] - 3*z[2] + n<T>(9,2)*z[6] + z[86] + n<T>(31,6)*z[77]
     - 
   7*z[5];
    z[86]=z[86]*z[100];
    z[94]=n<T>(15,4)*z[6] + n<T>(11,2)*z[77] + z[5];
    z[94]=z[6]*z[94];
    z[79]=z[86] + z[88] + z[94] - 7*z[92] + z[79];
    z[79]=z[9]*z[79];
    z[76]=z[76] + z[5] + z[95];
    z[76]=z[6]*z[76];
    z[86]= - z[6] + z[96];
    z[86]=2*z[86] - z[2];
    z[86]=z[2]*z[86];
    z[88]=z[77] + z[2];
    z[88]= - n<T>(19,9)*z[7] + n<T>(19,3)*z[6] - z[91] - n<T>(5,3)*z[88];
    z[88]=z[7]*z[88];
    z[91]= - n<T>(11,6)*z[8] + z[77] - n<T>(4,3)*z[5];
    z[91]=z[8]*z[91];
    z[76]=n<T>(1,2)*z[88] + n<T>(2,3)*z[86] + z[76] - z[92] + z[91];
    z[76]=z[7]*z[76];
    z[86]= - 5*z[3] - 7*z[9] + z[104] - z[84] - z[80] + n<T>(5,3) + z[82];
    z[86]=z[24]*z[86];
    z[88]= - z[3] - z[11];
    z[88]=z[38]*z[88];
    z[86]=z[73] + z[86] + z[88] + z[56] - z[42];
    z[88]= - z[5] - z[3];
    z[88]=z[77]*z[88];
    z[88]= - z[28] + z[88];
    z[88]=z[12]*z[88];
    z[91]= - z[3] + z[93];
    z[91]=z[19]*z[91];
    z[82]=z[2] - z[82] + z[8];
    z[82]=z[22]*z[82];
    z[82]=z[82] + z[88] + z[91];
    z[88]=z[4] + z[10];
    z[91]=z[77]*z[88];
    z[91]=z[28] + z[91];
    z[91]=z[13]*z[91];
    z[88]=z[77] - z[88];
    z[88]=z[26]*z[88];
    z[88]=z[91] + z[88];
    z[75]= - z[77] + z[75];
    z[75]=z[5]*z[75];
    z[91]= - 5*z[77] + z[80];
    z[91]=2*z[91] + 5*z[8];
    z[91]=z[8]*z[91];
    z[75]=z[75] + z[91];
    z[91]=n<T>(23,3) - n<T>(7,2)*z[77];
    z[91]= - n<T>(106,9)*z[6] + z[95] + n<T>(1,2)*z[91] - n<T>(11,3)*z[5];
    z[91]=z[6]*z[91];
    z[75]=n<T>(1,3)*z[75] + z[91];
    z[75]=z[6]*z[75];
    z[80]= - z[84] - n<T>(25,3)*z[77] + z[80];
    z[84]=z[4] + z[2];
    z[80]=2*z[80] + n<T>(25,3)*z[84];
    z[80]=z[17]*z[80];
    z[84]= - z[5] - z[6];
    z[84]=z[77]*z[84];
    z[84]= - z[28] + z[84];
    z[84]=z[14]*z[84];
    z[91]=z[6] - z[93];
    z[91]=z[20]*z[91];
    z[84]=z[59] + z[84] + z[91];
    z[91]=z[77]*z[8];
    z[91]=z[91] + z[28];
    z[92]=z[7]*z[77];
    z[92]=z[92] + z[91];
    z[92]=z[15]*z[92];
    z[94]=z[7] - z[97];
    z[94]=z[23]*z[94];
    z[92]= - z[30] + z[92] + z[94];
    z[94]=z[9]*z[77];
    z[91]=z[94] + z[91];
    z[91]=z[16]*z[91];
    z[94]= - z[9] + z[97];
    z[94]=z[25]*z[94];
    z[91]=z[91] + z[94];
    z[94]=n<T>(55,2)*z[2] - 145*z[6] - n<T>(53,4)*z[5] - 59*z[8];
    z[94]=n<T>(1,2)*z[94] + 34*z[9];
    z[94]=n<T>(4,3)*z[11] - n<T>(17,24)*z[7] - n<T>(17,6)*z[4] + n<T>(313,24)*z[10]
     + n<T>(1,3)*z[94] - z[99];
    z[94]=z[28]*z[94];
    z[94]=z[94] + z[53];
    z[95]= - z[5]*z[83];
    z[97]= - n<T>(2,3)*z[8] - z[83];
    z[97]=z[8]*z[97];
    z[95]=z[95] + z[97];
    z[95]=z[8]*z[95];
    z[96]=z[96] - z[2];
    z[96]= - z[102] - z[6] - n<T>(1,3)*z[96];
    z[96]=z[21]*z[96];
    z[93]= - n<T>(35,3)*z[10] + z[99] - z[9] + 4*z[93] - n<T>(47,3)*z[6];
    z[93]=z[27]*z[93];
    z[97]= - z[71] + z[68] + z[64] - z[61] + z[52] + z[49];
    z[98]=z[41] + z[48] - z[46];
    z[99]=z[58] + z[55];
    z[87]= - z[39]*z[87];
    z[87]=z[87] + z[57];
    z[100]=n<T>(112,3)*z[35] + n<T>(35,3)*z[33] - n<T>(49,36)*z[29];
    z[100]=i*z[100];
    z[77]= - n<T>(2,3)*z[77] + z[5];
    z[77]=z[77]*npow(z[5],2);
    z[83]=z[2] - z[83];
    z[83]=z[18]*z[83];
    z[101]= - z[9] - z[10];
    z[101]=z[40]*z[101];
    z[102]=2*i;
    z[102]= - z[32]*z[102];

    r +=  - n<T>(73,36)*z[31] - n<T>(14,3)*z[34] - n<T>(53,3)*z[36] + n<T>(29,6)*z[37]
       + n<T>(11,6)*z[43] - n<T>(11,2)*z[44] + z[45] - 16*z[47] - 5*z[50] + 
      z[51] - n<T>(3,2)*z[54] + n<T>(32,3)*z[60] + z[62] - n<T>(1,6)*z[63] - n<T>(4,3)*
      z[65] + n<T>(25,6)*z[66] + n<T>(41,12)*z[67] + 14*z[69] - n<T>(11,12)*z[70]
       + n<T>(9,4)*z[72] - z[74] + z[75] + z[76] + z[77] + z[78] + z[79] + 
      z[80] + z[81] + 6*z[82] + n<T>(10,3)*z[83] + n<T>(7,3)*z[84] + z[85] + 2*
      z[86] + n<T>(2,3)*z[87] + n<T>(13,3)*z[88] + z[89] + z[90] + 4*z[91] + n<T>(8,3)*z[92]
     + z[93]
     + n<T>(1,3)*z[94]
     + z[95]
     + 8*z[96]
     + n<T>(1,2)*z[97]
     +  3*z[98] + n<T>(5,2)*z[99] + z[100] + n<T>(11,3)*z[101] + z[102];
 
    return r;
}

template std::complex<double> qg_2lha_tf184(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf184(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
