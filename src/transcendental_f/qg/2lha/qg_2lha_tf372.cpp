#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf372(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[115];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[3];
    z[22]=e[4];
    z[23]=e[5];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[10];
    z[28]=e[11];
    z[29]=e[12];
    z[30]=e[13];
    z[31]=c[3];
    z[32]=c[11];
    z[33]=c[15];
    z[34]=c[19];
    z[35]=c[21];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[31];
    z[41]=e[6];
    z[42]=e[14];
    z[43]=f[0];
    z[44]=f[1];
    z[45]=f[2];
    z[46]=f[3];
    z[47]=f[4];
    z[48]=f[5];
    z[49]=f[6];
    z[50]=f[7];
    z[51]=f[10];
    z[52]=f[11];
    z[53]=f[12];
    z[54]=f[14];
    z[55]=f[15];
    z[56]=f[16];
    z[57]=f[17];
    z[58]=f[18];
    z[59]=f[21];
    z[60]=f[23];
    z[61]=f[30];
    z[62]=f[31];
    z[63]=f[32];
    z[64]=f[33];
    z[65]=f[34];
    z[66]=f[36];
    z[67]=f[37];
    z[68]=f[39];
    z[69]=f[43];
    z[70]=f[50];
    z[71]=f[51];
    z[72]=f[52];
    z[73]=f[53];
    z[74]=f[54];
    z[75]=f[55];
    z[76]=f[56];
    z[77]=f[58];
    z[78]=f[74];
    z[79]=f[75];
    z[80]=n<T>(5,24)*z[6];
    z[81]=n<T>(7,2)*z[7];
    z[82]=n<T>(13,2)*z[2];
    z[83]=n<T>(13,4)*z[8];
    z[84]=z[1]*i;
    z[85]=3*z[84];
    z[86]=z[85] - z[5];
    z[86]= - n<T>(101,12)*z[4] + z[82] + z[81] + n<T>(55,24)*z[9] - z[83] + 
    z[80] + n<T>(13,4)*z[86] + 3*z[3];
    z[87]=n<T>(1,2)*z[4];
    z[86]=z[86]*z[87];
    z[88]=n<T>(1,2)*z[5];
    z[89]=z[88] - z[84];
    z[90]=z[89]*z[5];
    z[91]=n<T>(1,2)*z[6];
    z[92]= - z[84] + z[91];
    z[80]=z[92]*z[80];
    z[92]=n<T>(1,2)*z[8];
    z[93]=z[92] - z[84];
    z[94]=z[5] - z[93];
    z[94]=z[94]*z[83];
    z[95]=n<T>(1,2)*z[7];
    z[96]=z[95] - z[84] - z[3];
    z[81]=z[96]*z[81];
    z[82]= - z[84]*z[82];
    z[96]= - z[85] + n<T>(7,4)*z[3];
    z[96]=z[3]*z[96];
    z[97]=n<T>(11,3)*z[84];
    z[98]=n<T>(11,6)*z[9] - z[97] - 3*z[6];
    z[98]=z[9]*z[98];
    z[99]= - z[84] + n<T>(1,2)*z[10];
    z[99]=z[10]*z[99];
    z[80]=z[86] + n<T>(13,2)*z[99] + z[82] + z[81] + n<T>(5,8)*z[98] + z[94] + 
    z[80] - n<T>(13,4)*z[90] + z[96];
    z[80]=z[80]*z[87];
    z[81]=n<T>(7,4)*z[9];
    z[82]=n<T>(1,4)*z[3];
    z[86]=z[81] + z[83] + z[6] - z[82] + 19*z[84] + z[5];
    z[86]= - n<T>(31,8)*z[2] + n<T>(1,2)*z[86] + z[7];
    z[86]=z[2]*z[86];
    z[94]=n<T>(1,4)*z[5];
    z[96]= - 17*z[84] + 13*z[5];
    z[96]=z[96]*z[94];
    z[98]=7*z[5];
    z[99]= - 13*z[84] + z[98];
    z[99]=n<T>(1,2)*z[99] - 4*z[6];
    z[99]=z[6]*z[99];
    z[100]=z[88] + z[84];
    z[100]= - 13*z[100] + n<T>(7,2)*z[3];
    z[83]=z[83] + n<T>(1,2)*z[100] + 2*z[6];
    z[83]=z[8]*z[83];
    z[100]=z[84] - z[8];
    z[101]=z[3] - z[5];
    z[102]=z[9] + z[101] + z[100];
    z[81]=z[102]*z[81];
    z[102]=z[7] - z[8];
    z[103]=z[84] - z[5];
    z[104]=z[6] + z[103];
    z[102]=n<T>(1,2)*z[104] + 2*z[102];
    z[102]=z[7]*z[102];
    z[104]= - z[5] + n<T>(5,2)*z[84];
    z[105]= - n<T>(25,8)*z[3] - z[104];
    z[105]=z[3]*z[105];
    z[81]=z[86] + z[102] + z[81] + z[83] + z[99] + z[96] + z[105];
    z[81]=z[2]*z[81];
    z[83]=n<T>(1,3)*z[84];
    z[86]=n<T>(1,4)*z[6];
    z[94]= - n<T>(1,12)*z[9] + z[86] + z[82] + z[83] - z[94];
    z[94]=z[9]*z[94];
    z[96]=z[6] - n<T>(1,4)*z[9];
    z[96]=n<T>(1,3)*z[96] - n<T>(1,8)*z[4];
    z[96]=z[4]*z[96];
    z[94]=z[94] + z[96];
    z[96]= - z[85] + n<T>(5,4)*z[5];
    z[99]=n<T>(3,2)*z[3];
    z[102]= - z[99] + z[96];
    z[102]=z[3]*z[102];
    z[105]=n<T>(7,8)*z[8];
    z[106]= - n<T>(55,4)*z[84] + 19*z[5];
    z[106]= - n<T>(17,6)*z[11] + z[7] + n<T>(15,8)*z[9] - z[105] - n<T>(5,3)*z[6]
     + 
   n<T>(1,6)*z[106] - z[3];
    z[106]=z[11]*z[106];
    z[102]=z[106] + n<T>(1,3)*z[90] + z[102];
    z[106]=n<T>(1,12)*z[6] - n<T>(1,8)*z[3] + n<T>(1,3)*z[89];
    z[106]=z[6]*z[106];
    z[93]=z[3] - z[93];
    z[93]=z[93]*z[105];
    z[95]=z[95] + z[103];
    z[95]=z[7]*z[95];
    z[93]=z[95] + z[93] + 5*z[106] + n<T>(1,2)*z[102] + n<T>(5,2)*z[94];
    z[93]=z[11]*z[93];
    z[94]=5*z[3];
    z[95]= - n<T>(19,3)*z[6] + z[94] + 5*z[84] - n<T>(11,3)*z[5];
    z[95]=z[95]*z[86];
    z[102]=2*z[5];
    z[105]= - n<T>(23,4)*z[84] + z[102];
    z[105]=n<T>(31,12)*z[8] + n<T>(1,3)*z[105] - z[91];
    z[105]=z[8]*z[105];
    z[106]=z[84] + 17*z[5];
    z[94]=n<T>(23,3)*z[8] - n<T>(67,3)*z[6] + n<T>(1,3)*z[106] + z[94];
    z[94]=n<T>(1,2)*z[94] + n<T>(19,9)*z[7];
    z[94]=z[7]*z[94];
    z[106]= - 35*z[84] + n<T>(29,2)*z[5];
    z[106]=z[5]*z[106];
    z[96]=z[3] - z[96];
    z[96]=z[3]*z[96];
    z[94]=n<T>(1,4)*z[94] + z[105] + z[95] + n<T>(1,12)*z[106] + z[96];
    z[94]=z[7]*z[94];
    z[95]=n<T>(1,2)*z[3];
    z[96]=z[103] + z[95];
    z[105]=z[96]*z[3];
    z[106]= - n<T>(113,2)*z[90] - 49*z[105];
    z[107]= - n<T>(29,4)*z[6] - n<T>(27,2)*z[84] + z[101];
    z[107]=z[6]*z[107];
    z[108]= - n<T>(53,6)*z[8] + n<T>(53,3)*z[84] + 29*z[6];
    z[108]=z[108]*z[92];
    z[109]=n<T>(1,2)*z[9];
    z[110]= - z[109] - z[103];
    z[110]=z[9]*z[110];
    z[106]=n<T>(5,4)*z[110] + z[108] + n<T>(1,6)*z[106] + z[107];
    z[88]=z[95] - z[88];
    z[107]= - 3*z[7] - n<T>(17,6)*z[8] + n<T>(7,3)*z[84] - z[88];
    z[107]=z[7]*z[107];
    z[106]=n<T>(1,2)*z[106] + z[107];
    z[107]=73*z[84] - n<T>(113,3)*z[5];
    z[107]=n<T>(25,24)*z[10] - n<T>(13,8)*z[2] + n<T>(17,24)*z[7] - n<T>(5,32)*z[9]
     - n<T>(53,48)*z[8]
     - n<T>(29,16)*z[6]
     + n<T>(1,32)*z[107]
     + n<T>(4,3)*z[3];
    z[107]=z[10]*z[107];
    z[106]=n<T>(1,2)*z[106] + z[107];
    z[106]=z[10]*z[106];
    z[107]=z[84]*z[4];
    z[107]=z[107] + z[31];
    z[108]=z[10]*z[84];
    z[108]=z[108] + z[107];
    z[108]=z[14]*z[108];
    z[110]=z[84]*z[8];
    z[110]=z[110] + z[31];
    z[111]= - z[7]*z[84];
    z[111]=z[111] - z[110];
    z[111]=z[16]*z[111];
    z[112]=z[84] - z[4];
    z[113]= - z[10] + z[112];
    z[113]=z[22]*z[113];
    z[114]= - z[7] + z[100];
    z[114]=z[26]*z[114];
    z[108]= - z[43] + z[108] + z[111] + z[113] + z[114];
    z[82]= - z[82] + z[104];
    z[104]=7*z[3];
    z[82]=z[82]*z[104];
    z[82]= - n<T>(13,2)*z[90] + z[82];
    z[102]=n<T>(71,8)*z[84] - z[102];
    z[102]=n<T>(1,3)*z[102] - n<T>(25,16)*z[6];
    z[102]=z[6]*z[102];
    z[104]=7*z[8];
    z[111]=z[104] - n<T>(71,6)*z[6] - n<T>(35,6)*z[3] - 7*z[84] - n<T>(13,6)*z[5];
    z[111]=z[8]*z[111];
    z[82]=n<T>(1,8)*z[111] + n<T>(1,12)*z[82] + z[102];
    z[82]=z[8]*z[82];
    z[90]=n<T>(25,4)*z[90] + 7*z[105];
    z[86]=z[86] + n<T>(1,2)*z[84] - z[101];
    z[86]=z[86]*z[91];
    z[86]=n<T>(1,3)*z[90] + z[86];
    z[88]=n<T>(1,4)*z[8] + z[84] - z[88];
    z[90]=n<T>(7,3)*z[8];
    z[88]=z[88]*z[90];
    z[85]=n<T>(5,4)*z[6] - n<T>(11,6)*z[3] - z[85] + n<T>(25,12)*z[5];
    z[85]= - n<T>(109,36)*z[9] + n<T>(5,2)*z[85] - z[90];
    z[85]=z[85]*z[109];
    z[85]=z[85] + n<T>(5,2)*z[86] + z[88];
    z[85]=z[85]*z[109];
    z[83]= - z[83] - z[5];
    z[83]=z[83]*z[98];
    z[86]= - z[96]*z[95];
    z[83]=z[83] + z[86];
    z[86]=n<T>(19,3)*z[5];
    z[88]= - 1 + n<T>(11,12)*z[84];
    z[88]= - z[3] + n<T>(11,2)*z[88] + z[86];
    z[88]=n<T>(1,2)*z[88] + n<T>(55,9)*z[6];
    z[88]=z[6]*z[88];
    z[83]=n<T>(1,2)*z[83] + z[88];
    z[83]=z[6]*z[83];
    z[88]= - z[97] - z[5];
    z[88]=z[5]*z[88];
    z[90]= - n<T>(43,9)*z[3] - n<T>(35,3)*z[84] + n<T>(13,2)*z[5];
    z[90]=z[3]*z[90];
    z[88]=z[88] + z[90];
    z[88]=z[3]*z[88];
    z[90]= - n<T>(17,3)*z[5] + 11 + n<T>(13,3)*z[84];
    z[90]=z[90]*npow(z[5],2);
    z[88]=z[90] + z[88];
    z[90]=n<T>(211,2)*z[5] - 71*z[3];
    z[90]= - n<T>(841,8)*z[7] - n<T>(199,2)*z[9] - n<T>(89,4)*z[8] + n<T>(1,4)*z[90]
     + 
   73*z[6];
    z[90]=n<T>(1,2)*z[90] - 5*z[2];
    z[90]=n<T>(35,72)*z[11] + n<T>(173,48)*z[4] + n<T>(1,9)*z[90] + 7*z[10];
    z[90]=z[31]*z[90];
    z[83]=z[65] + z[90] + n<T>(1,4)*z[88] + z[83];
    z[86]=n<T>(43,3)*z[6] + z[86] - 11;
    z[88]=z[84]*z[86];
    z[88]=n<T>(19,3)*z[31] + z[88];
    z[88]=z[15]*z[88];
    z[86]=n<T>(19,3)*z[84] - z[86];
    z[86]=z[23]*z[86];
    z[86]= - z[59] + z[88] + z[86];
    z[88]= - z[9]*z[84];
    z[88]=z[88] - z[110];
    z[88]=z[17]*z[88];
    z[90]=z[9] - z[100];
    z[90]=z[28]*z[90];
    z[88]= - z[63] + z[88] + z[90];
    z[90]=z[11]*z[84];
    z[90]=z[90] + z[107];
    z[90]=z[13]*z[90];
    z[91]= - z[11] + z[112];
    z[91]=z[29]*z[91];
    z[90]=z[90] + z[91];
    z[91]=z[5] + z[3];
    z[91]=z[84]*z[91];
    z[91]=z[31] + z[91];
    z[91]=z[12]*z[91];
    z[95]=z[3] - z[103];
    z[95]=z[20]*z[95];
    z[91]=z[91] + z[95];
    z[95]= - n<T>(25,4)*z[84] + z[98];
    z[95]=n<T>(1,4)*z[10] - n<T>(5,6)*z[2] + n<T>(73,12)*z[7] + n<T>(7,12)*z[8] + n<T>(101,12)*z[6]
     + n<T>(1,3)*z[95] - z[99];
    z[95]=z[24]*z[95];
    z[96]=z[103] + z[2];
    z[96]=n<T>(101,4)*z[9] + z[104] + n<T>(73,4)*z[3] - 7*z[96];
    z[96]=z[27]*z[96];
    z[97]=z[10] + z[3];
    z[97]=z[9] + z[103] - n<T>(107,24)*z[97];
    z[97]=z[21]*z[97];
    z[98]= - z[9] - z[6] + z[84] + z[101];
    z[98]=z[30]*z[98];
    z[99]=n<T>(1,2)*z[2];
    z[84]=z[99] - z[84];
    z[87]= - z[87] - z[84];
    z[87]=z[18]*z[87];
    z[89]= - z[99] - z[89];
    z[89]=z[19]*z[89];
    z[84]= - z[92] - z[84];
    z[84]=z[25]*z[84];
    z[92]=z[57] + z[46];
    z[99]= - n<T>(163,12)*z[38] - n<T>(91,24)*z[36] + n<T>(5,4)*z[35] - n<T>(5,16)*z[32]
   ;
    z[99]=i*z[99];
    z[100]=z[7] + z[10];
    z[100]=z[41]*z[100];
    z[101]= - z[9] - z[11];
    z[101]=z[42]*z[101];

    r +=  - n<T>(29,12)*z[33] + n<T>(10,9)*z[34] + n<T>(31,24)*z[37] + n<T>(37,6)*z[39]
       + n<T>(77,24)*z[40] + n<T>(13,8)*z[44] + n<T>(11,12)*z[45] + n<T>(181,96)*z[47]
       - n<T>(17,24)*z[48] + n<T>(21,16)*z[49] + n<T>(14,3)*z[50] + n<T>(35,24)*z[51]
       - n<T>(13,48)*z[52] + n<T>(89,96)*z[53] + z[54] + n<T>(21,8)*z[55] - n<T>(5,32)*
      z[56] - n<T>(13,12)*z[58] - n<T>(7,12)*z[60] - n<T>(17,12)*z[61] - n<T>(103,48)*
      z[62] - n<T>(19,3)*z[64] + n<T>(53,48)*z[66] + n<T>(7,8)*z[67] - n<T>(39,16)*
      z[68] + n<T>(2,3)*z[69] - n<T>(5,24)*z[70] + n<T>(5,96)*z[71] - n<T>(4,3)*z[72]
       + 5*z[73] - n<T>(5,6)*z[74] - n<T>(55,96)*z[75] - n<T>(7,16)*z[76] + n<T>(41,32)
      *z[77] - n<T>(3,8)*z[78] + n<T>(1,8)*z[79] + z[80] + n<T>(1,3)*z[81] + z[82]
       + n<T>(1,2)*z[83] + n<T>(13,6)*z[84] + z[85] + n<T>(1,4)*z[86] + n<T>(13,2)*
      z[87] + n<T>(7,4)*z[88] + n<T>(11,3)*z[89] + n<T>(5,8)*z[90] + n<T>(23,24)*z[91]
       - n<T>(13,16)*z[92] + z[93] + z[94] + z[95] + n<T>(1,6)*z[96] + z[97] + 
      n<T>(5,4)*z[98] + z[99] + n<T>(53,12)*z[100] + n<T>(55,24)*z[101] + z[106]
     +  n<T>(13,4)*z[108];
 
    return r;
}

template std::complex<double> qg_2lha_tf372(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf372(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
