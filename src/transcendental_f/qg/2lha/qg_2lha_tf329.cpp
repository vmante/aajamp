#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf329(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[123];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[18];
    z[13]=d[15];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[17];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[21];
    z[37]=c[23];
    z[38]=c[25];
    z[39]=c[26];
    z[40]=c[28];
    z[41]=c[29];
    z[42]=c[31];
    z[43]=e[3];
    z[44]=e[6];
    z[45]=e[14];
    z[46]=f[0];
    z[47]=f[1];
    z[48]=f[2];
    z[49]=f[3];
    z[50]=f[4];
    z[51]=f[5];
    z[52]=f[6];
    z[53]=f[7];
    z[54]=f[8];
    z[55]=f[9];
    z[56]=f[10];
    z[57]=f[11];
    z[58]=f[12];
    z[59]=f[13];
    z[60]=f[14];
    z[61]=f[15];
    z[62]=f[16];
    z[63]=f[17];
    z[64]=f[18];
    z[65]=f[20];
    z[66]=f[21];
    z[67]=f[23];
    z[68]=f[30];
    z[69]=f[31];
    z[70]=f[32];
    z[71]=f[33];
    z[72]=f[36];
    z[73]=f[37];
    z[74]=f[39];
    z[75]=f[43];
    z[76]=f[50];
    z[77]=f[51];
    z[78]=f[52];
    z[79]=f[53];
    z[80]=f[54];
    z[81]=f[55];
    z[82]=f[56];
    z[83]=f[58];
    z[84]=f[74];
    z[85]=f[75];
    z[86]=f[78];
    z[87]=f[83];
    z[88]=n<T>(1,2)*z[4];
    z[89]=n<T>(1,2)*z[9];
    z[90]=n<T>(1,2)*z[2];
    z[91]=z[1]*i;
    z[92]= - 77*z[91] + 83*z[5];
    z[92]= - z[88] - z[89] - n<T>(83,12)*z[7] + z[90] + n<T>(1,12)*z[92] - z[3];
    z[93]=2*z[8];
    z[92]= - n<T>(25,12)*z[11] + n<T>(3,4)*z[10] + n<T>(1,2)*z[92] + z[93];
    z[92]=z[11]*z[92];
    z[94]=3*z[5];
    z[95]=3*z[3];
    z[96]=z[94] - z[95];
    z[97]=3*z[7];
    z[98]= - n<T>(19,12)*z[10] + n<T>(1,6)*z[4] + z[97] + n<T>(17,6)*z[91] - z[96];
    z[98]=z[10]*z[98];
    z[99]=n<T>(1,2)*z[5];
    z[100]=z[99] - z[91];
    z[100]=z[100]*z[5];
    z[101]=2*z[91];
    z[102]=n<T>(3,2)*z[5];
    z[103]= - z[3] - z[101] + z[102];
    z[103]=z[3]*z[103];
    z[104]=z[91] - z[5];
    z[105]=z[104] + z[90];
    z[106]=z[105]*z[90];
    z[107]=z[95] - z[2];
    z[108]= - 71*z[91] + 55*z[5];
    z[108]= - n<T>(23,12)*z[7] + n<T>(1,6)*z[108] - z[107];
    z[109]=n<T>(1,2)*z[7];
    z[108]=z[108]*z[109];
    z[110]=z[89] - z[91];
    z[111]=z[3] - z[110];
    z[111]=z[111]*z[89];
    z[112]=z[2] - z[5];
    z[113]= - n<T>(11,12)*z[4] + n<T>(5,3)*z[7] + z[91] - z[112];
    z[113]=z[113]*z[88];
    z[114]=2*z[104];
    z[115]=z[114] + z[8];
    z[93]=z[115]*z[93];
    z[92]=z[92] + n<T>(1,2)*z[98] + z[93] + z[113] + z[111] + z[108] + 
    z[106] - n<T>(7,12)*z[100] + z[103];
    z[92]=z[11]*z[92];
    z[93]=n<T>(1,4)*z[4];
    z[98]=n<T>(1,2)*z[3];
    z[103]=z[93] - z[98];
    z[106]= - n<T>(1,2)*z[91] - z[112] + z[103];
    z[106]=z[4]*z[106];
    z[108]=5*z[2];
    z[111]=n<T>(37,3)*z[91];
    z[113]= - z[111] - 11*z[5];
    z[93]= - n<T>(45,8)*z[8] + z[93] + n<T>(1,3)*z[9] + n<T>(27,4)*z[7] - z[108]
     + n<T>(1,8)*z[113]
     + z[3];
    z[93]=z[8]*z[93];
    z[113]=n<T>(1,4)*z[5];
    z[115]=11*z[91] - n<T>(9,2)*z[5];
    z[115]=z[115]*z[113];
    z[116]= - n<T>(10,3) - z[99] + z[98];
    z[116]=z[3]*z[116];
    z[117]= - 13 - z[91];
    z[117]=n<T>(217,12)*z[7] - n<T>(11,2)*z[2] - z[95] + n<T>(1,3)*z[117] + n<T>(23,2)*
    z[5];
    z[117]=z[117]*z[109];
    z[118]= - n<T>(10,3)*z[9] - n<T>(17,6)*z[7] + z[3] - n<T>(2,3)*z[91] + n<T>(21,4)*
    z[112];
    z[118]=z[9]*z[118];
    z[119]= - 15*z[104] - n<T>(17,2)*z[2];
    z[119]=z[2]*z[119];
    z[93]=z[93] + z[106] + z[118] + z[117] + n<T>(1,4)*z[119] + z[115] + 
    z[116];
    z[93]=z[8]*z[93];
    z[106]=13*z[5];
    z[115]= - z[106] + 17*z[91];
    z[116]=n<T>(9,2)*z[3] + z[115];
    z[116]=z[3]*z[116];
    z[116]= - n<T>(15,2)*z[100] + z[116];
    z[105]= - z[3] - z[105];
    z[105]=z[2]*z[105];
    z[117]=3*z[91];
    z[118]=z[117] - z[5];
    z[119]=z[7] + z[3] + z[118];
    z[119]=z[119]*z[97];
    z[105]=z[119] + n<T>(1,2)*z[116] + z[105];
    z[101]=z[90] + z[101] - z[99] - z[103];
    z[101]=z[4]*z[101];
    z[103]=n<T>(5,12)*z[9];
    z[97]=z[103] - z[97] - n<T>(5,6)*z[91] - z[3];
    z[97]=z[9]*z[97];
    z[116]=n<T>(11,3)*z[91];
    z[119]= - n<T>(1,4)*z[8] + n<T>(13,6)*z[9] - z[98] - z[116] + z[102];
    z[119]=z[8]*z[119];
    z[120]=n<T>(5,3)*z[91];
    z[121]=z[120] - z[94];
    z[122]=n<T>(49,16)*z[10];
    z[103]= - z[122] - n<T>(25,12)*z[8] + z[103] + n<T>(5,2)*z[7] + n<T>(5,16)*
    z[121] + 4*z[3] - z[4] + z[2];
    z[103]=z[6]*z[103];
    z[121]= - 49*z[91] + 41*z[5];
    z[121]= - z[122] - z[8] + z[9] + n<T>(1,8)*z[121] + z[3];
    z[121]=z[10]*z[121];
    z[97]=z[103] + z[121] + z[119] + z[101] + n<T>(1,2)*z[105] + z[97];
    z[97]=z[6]*z[97];
    z[101]= - 21*z[91] + n<T>(5,2)*z[5];
    z[101]=z[101]*z[113];
    z[103]=z[3]*z[118];
    z[101]=z[101] + z[103];
    z[103]= - n<T>(47,8)*z[2] + z[98] + 15*z[91] + n<T>(47,4)*z[5];
    z[103]=z[2]*z[103];
    z[105]=n<T>(5,12)*z[91];
    z[113]=n<T>(19,24)*z[7] + z[105] + z[112];
    z[113]=z[113]*z[109];
    z[118]= - z[5] + z[110];
    z[119]=n<T>(99,8)*z[9];
    z[118]=z[118]*z[119];
    z[121]=n<T>(47,3)*z[91] + n<T>(21,2)*z[5];
    z[95]=n<T>(1,2)*z[121] - z[95];
    z[95]= - n<T>(7,12)*z[4] + z[119] - n<T>(5,24)*z[7] + n<T>(1,2)*z[95] - 15*z[2];
    z[95]=z[95]*z[88];
    z[95]=z[95] + z[118] + z[113] + n<T>(1,2)*z[101] + z[103];
    z[95]=z[4]*z[95];
    z[101]=z[7] - z[5];
    z[103]=z[9] - z[2];
    z[113]=n<T>(37,6)*z[4];
    z[101]= - n<T>(151,9)*z[10] + z[113] + 37*z[3] + n<T>(19,3)*z[91] + n<T>(35,3)*
    z[103] + n<T>(25,2)*z[101];
    z[101]=z[10]*z[101];
    z[111]=z[113] - z[111] - 13*z[7];
    z[111]=z[4]*z[111];
    z[101]=z[111] + z[101];
    z[89]= - z[89] + 13*z[2] + z[3] - 35*z[91] - z[106];
    z[89]=z[9]*z[89];
    z[111]= - n<T>(25,2)*z[2] - 13*z[3] - z[91] + z[106];
    z[111]=z[2]*z[111];
    z[89]=z[111] + z[89];
    z[111]=z[98] + z[117] - 2*z[5];
    z[111]=z[3]*z[111];
    z[96]=n<T>(1,8)*z[7] + n<T>(1,4)*z[91] + z[96];
    z[96]=z[96]*z[109];
    z[103]=z[3] - z[103];
    z[103]=z[8]*z[103];
    z[89]=z[103] + z[96] - n<T>(25,8)*z[100] + z[111] + n<T>(1,12)*z[89] + n<T>(1,8)
   *z[101];
    z[89]=z[10]*z[89];
    z[96]=7*z[5];
    z[100]= - z[98] - 19*z[91] + z[96];
    z[101]=n<T>(1,3)*z[3];
    z[100]=z[100]*z[101];
    z[103]= - z[117] + n<T>(67,2)*z[5];
    z[103]=z[103]*z[99];
    z[109]= - 3*z[9] + n<T>(35,3)*z[7] - n<T>(53,2)*z[2] + n<T>(13,6)*z[3] - z[91]
    + n<T>(7,4)*z[5];
    z[109]=z[9]*z[109];
    z[100]=z[109] + z[103] + z[100];
    z[103]=4*z[5];
    z[109]= - 2*z[2] - n<T>(7,12)*z[3] + n<T>(51,4)*z[91] + z[103];
    z[109]=z[2]*z[109];
    z[102]=n<T>(5,6)*z[7] - n<T>(3,2)*z[2] - z[120] + z[102];
    z[102]=z[7]*z[102];
    z[100]=n<T>(7,2)*z[102] + z[109] + n<T>(1,4)*z[100];
    z[100]=z[9]*z[100];
    z[102]=z[91]*z[5];
    z[109]=z[102] + z[30];
    z[111]=z[7]*z[116];
    z[111]=z[111] - n<T>(33,2)*z[91] - n<T>(25,3)*z[109];
    z[111]=z[16]*z[111];
    z[113]= - n<T>(33,2) + n<T>(25,3)*z[91];
    z[116]= - n<T>(11,3)*z[7] + n<T>(25,3)*z[5] - z[113];
    z[116]=z[22]*z[116];
    z[111]= - z[87] + z[111] + z[116] + z[85] - z[84];
    z[116]=z[8] + z[9];
    z[118]=z[91]*z[116];
    z[118]=z[30] + z[118];
    z[118]=z[17]*z[118];
    z[116]= - z[91] + z[116];
    z[116]=z[25]*z[116];
    z[116]=z[118] + z[116];
    z[105]= - z[105] + z[94];
    z[105]=z[5]*z[105];
    z[118]=5 + n<T>(11,8)*z[91];
    z[99]= - n<T>(187,36)*z[3] + n<T>(1,3)*z[118] + z[99];
    z[99]=z[3]*z[99];
    z[99]=z[105] + z[99];
    z[99]=z[3]*z[99];
    z[96]=n<T>(29,4)*z[3] - n<T>(17,4)*z[91] - z[96];
    z[96]=z[96]*z[101];
    z[101]=n<T>(109,12)*z[2] + n<T>(23,12)*z[3] - n<T>(123,8)*z[91] - z[103];
    z[101]=z[2]*z[101];
    z[103]=n<T>(9,4)*z[91] - 5*z[5];
    z[103]=z[5]*z[103];
    z[96]=z[101] + z[103] + z[96];
    z[96]=z[2]*z[96];
    z[98]= - 9*z[104] - z[98];
    z[98]=z[3]*z[98];
    z[101]=n<T>(15,2)*z[2] + z[115];
    z[101]=z[101]*z[90];
    z[103]=13*z[91] - n<T>(59,6)*z[5];
    z[103]=z[5]*z[103];
    z[98]=z[101] + z[103] + z[98];
    z[101]= - n<T>(7,3) - n<T>(61,2)*z[91];
    z[101]=n<T>(1,4)*z[101] - z[106];
    z[101]= - n<T>(53,6)*z[7] + 7*z[2] + n<T>(1,2)*z[101] - z[3];
    z[101]=z[7]*z[101];
    z[98]=n<T>(1,2)*z[98] + z[101];
    z[98]=z[7]*z[98];
    z[101]=3*z[10];
    z[103]=2*z[11] - z[101] + z[4] + z[114] + z[107];
    z[103]=z[29]*z[103];
    z[105]=5*z[9];
    z[106]= - z[105] + z[108] + 5*z[104] - n<T>(79,4)*z[3];
    z[106]=n<T>(1,3)*z[106] - n<T>(33,4)*z[10];
    z[106]=z[26]*z[106];
    z[105]=z[105] + z[3];
    z[107]=5*z[91] - z[112] - z[105];
    z[101]=n<T>(1,2)*z[107] - z[101];
    z[101]=z[27]*z[101];
    z[88]=z[88] - z[91] + z[90];
    z[88]=z[18]*z[88];
    z[107]=z[91]*z[4];
    z[107]=z[107] + z[30];
    z[108]= - z[6]*z[91];
    z[108]=z[108] - z[107];
    z[108]=z[15]*z[108];
    z[112]=z[91] - z[4];
    z[114]=z[6] - z[112];
    z[114]=z[21]*z[114];
    z[108]=z[108] + z[114] + z[74] - z[66] + z[61] + z[54];
    z[114]=z[11]*z[91];
    z[107]=z[114] + z[107];
    z[107]=z[14]*z[107];
    z[112]= - z[11] + z[112];
    z[112]=z[28]*z[112];
    z[107]= - z[72] + z[107] + z[112];
    z[112]= - z[3]*z[91];
    z[109]=z[112] - z[109];
    z[109]=z[13]*z[109];
    z[104]= - z[3] + z[104];
    z[104]=z[20]*z[104];
    z[104]=z[109] + z[104];
    z[105]=z[2] + z[105];
    z[105]=z[91]*z[105];
    z[102]= - z[102] + z[105];
    z[105]=z[10]*z[117];
    z[102]=n<T>(5,2)*z[30] + n<T>(1,2)*z[102] + z[105];
    z[102]=z[12]*z[102];
    z[94]= - 7*z[4] + 3*z[2] + z[91] + z[94];
    z[94]=z[19]*z[94];
    z[90]=z[90] + z[110];
    z[90]=z[24]*z[90];
    z[105]=z[41] + z[35];
    z[109]=z[65] + z[34];
    z[110]=z[67] + z[64];
    z[112]=z[75] - z[59];
    z[113]=9*z[5] - z[113];
    z[113]=z[113]*npow(z[5],2);
    z[113]=z[73] + z[113] - z[82];
    z[114]= - n<T>(565,2)*z[5] + 73*z[3];
    z[114]= - n<T>(13,4)*z[11] + n<T>(83,6)*z[6] + n<T>(31,3)*z[10] - n<T>(23,4)*z[8]
     + 
   n<T>(25,2)*z[4] - n<T>(89,12)*z[9] - n<T>(95,6)*z[7] + n<T>(1,6)*z[114] - 25*z[2];
    z[114]=z[30]*z[114];
    z[114]=z[114] + z[76];
    z[115]=n<T>(201,4)*z[39] + n<T>(31,2)*z[37] - n<T>(1,4)*z[36] - n<T>(1,2)*z[33]
     + n<T>(43,12)*z[31];
    z[115]=i*z[115];
    z[91]=n<T>(1,2)*z[6] - n<T>(35,3)*z[8] - n<T>(25,4)*z[9] - n<T>(65,3)*z[7] + 8*z[2]
    + n<T>(33,4)*z[91] - 10*z[5];
    z[91]=z[23]*z[91];
    z[117]=z[6] + z[3];
    z[117]=n<T>(10,3) - n<T>(23,4)*z[117];
    z[117]=z[43]*z[117];
    z[118]= - z[8] - z[6];
    z[118]=z[44]*z[118];
    z[119]= - z[10] - z[11];
    z[119]=z[45]*z[119];

    r +=  - n<T>(61,12)*z[32] - n<T>(109,12)*z[38] - n<T>(57,2)*z[40] - n<T>(259,24)*
      z[42] + 11*z[46] - z[47] - n<T>(21,4)*z[48] + n<T>(13,16)*z[49] - n<T>(115,48)
      *z[50] + n<T>(25,8)*z[51] - n<T>(13,8)*z[52] - n<T>(20,3)*z[53] - n<T>(5,8)*z[55]
       + n<T>(19,12)*z[56] + n<T>(3,16)*z[57] + n<T>(15,16)*z[58] + n<T>(3,4)*z[60]
     - 
      n<T>(41,16)*z[62] + n<T>(103,16)*z[63] + n<T>(8,3)*z[68] + n<T>(83,12)*z[69]
     + n<T>(39,8)*z[70]
     + n<T>(63,2)*z[71] - n<T>(17,48)*z[77] - n<T>(89,24)*z[78]
     + 8*
      z[79] - n<T>(4,3)*z[80] - n<T>(37,48)*z[81] + n<T>(33,16)*z[83] + 2*z[86] + 
      n<T>(67,2)*z[88] + z[89] + n<T>(35,2)*z[90] + z[91] + z[92] + z[93] + 
      z[94] + z[95] + z[96] + z[97] + z[98] + z[99] + z[100] + z[101]
       + z[102] + z[103] + n<T>(10,3)*z[104] + n<T>(7,4)*z[105] + z[106] + n<T>(5,12)*z[107]
     + n<T>(3,2)*z[108] - n<T>(7,3)*z[109]
     + n<T>(5,6)*z[110]
     + n<T>(1,2)*
      z[111] - n<T>(17,4)*z[112] + n<T>(1,4)*z[113] + n<T>(1,12)*z[114] + z[115]
     +  n<T>(22,3)*z[116] + z[117] + n<T>(5,3)*z[118] + n<T>(37,12)*z[119];
 
    return r;
}

template std::complex<double> qg_2lha_tf329(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf329(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
