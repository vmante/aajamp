#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1307(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[36];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[15];
    z[11]=c[18];
    z[12]=c[19];
    z[13]=c[23];
    z[14]=c[25];
    z[15]=c[26];
    z[16]=c[28];
    z[17]=c[29];
    z[18]=c[30];
    z[19]=c[31];
    z[20]=f[3];
    z[21]=f[5];
    z[22]=f[11];
    z[23]=f[13];
    z[24]=f[17];
    z[25]=z[4] + z[3];
    z[26]=2*z[2];
    z[27]= - z[26] + z[25];
    z[27]=z[27]*z[5];
    z[28]=z[26] - z[3];
    z[28]=z[28]*z[4];
    z[26]=z[26]*z[3];
    z[29]=npow(z[2],2);
    z[30]= - z[27] + z[28] - 3*z[29] + z[26];
    z[31]=z[1]*i;
    z[31]=2*z[31];
    z[30]=z[30]*z[31];
    z[32]=z[3] + z[2];
    z[33]= - z[31] + z[32];
    z[33]=z[6]*z[33];
    z[34]=z[4] + z[2];
    z[35]= - z[31] + z[34];
    z[35]=z[7]*z[35];
    z[31]= - z[31] + z[2] + z[5];
    z[31]=z[8]*z[31];
    z[31]=z[31] + z[33] + z[35];
    z[32]=z[32]*z[3];
    z[32]=z[32] - z[29];
    z[33]= - 3*z[3] + z[34];
    z[33]=z[4]*z[33];
    z[27]=z[27] + z[33] + z[32];
    z[27]=z[5]*z[27];
    z[26]= - z[29] - z[26];
    z[26]=z[3]*z[26];
    z[28]= - z[28] + z[32];
    z[28]=z[4]*z[28];
    z[29]=z[12] + z[10];
    z[32]=12*z[15] + 6*z[13];
    z[32]=i*z[32];
    z[33]=npow(z[2],3);
    z[25]=z[25] + z[5];
    z[25]= - z[2] - n<T>(1,3)*z[25];
    z[25]=z[9]*z[25];

    r += n<T>(4,3)*z[11] - n<T>(10,3)*z[14] - 8*z[16] - z[17] - 4*z[18] + n<T>(49,6)
      *z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + n<T>(1,2)*z[25] + 
      z[26] + z[27] + z[28] + n<T>(1,3)*z[29] + z[30] + 2*z[31] + z[32] + 3
      *z[33];
 
    return r;
}

template std::complex<double> qg_2lha_tf1307(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1307(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
