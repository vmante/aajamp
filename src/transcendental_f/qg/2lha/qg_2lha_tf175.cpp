#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf175(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[33];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[9];
    z[6]=d[11];
    z[7]=e[12];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[17];
    z[12]=c[31];
    z[13]=e[13];
    z[14]=e[14];
    z[15]=f[50];
    z[16]=f[51];
    z[17]=f[53];
    z[18]=f[55];
    z[19]=f[58];
    z[20]=f[59];
    z[21]=n<T>(1,9)*z[3];
    z[22]= - z[3] + 5*z[5] - 11*z[2];
    z[22]=z[22]*z[21];
    z[23]=n<T>(1,3)*z[4];
    z[24]=8*z[5];
    z[25]= - z[24] - z[2];
    z[25]= - n<T>(1,6)*z[4] + n<T>(1,3)*z[25] + 2*z[3];
    z[25]=z[25]*z[23];
    z[26]=5*z[2] + 11*z[5];
    z[26]=z[23] - z[3] + n<T>(1,9)*z[26];
    z[27]=z[6]*z[26];
    z[28]=npow(z[5],2);
    z[29]=n<T>(1,2)*z[2];
    z[30]=n<T>(2,9)*z[5] + z[29];
    z[30]=z[2]*z[30];
    z[22]=z[27] + z[25] + z[22] - n<T>(4,9)*z[28] + z[30];
    z[25]=z[1]*i;
    z[22]=z[25]*z[22];
    z[27]= - 13*z[5] - n<T>(19,3)*z[2];
    z[27]=z[27]*z[29];
    z[27]=4*z[28] + z[27];
    z[27]=z[2]*z[27];
    z[29]=n<T>(1,2)*z[4];
    z[30]=2*z[5] + n<T>(5,3)*z[2];
    z[30]=5*z[6] - z[29] + 2*z[30] - n<T>(53,6)*z[3];
    z[30]=z[8]*z[30];
    z[31]=npow(z[5],3);
    z[32]= - z[5] - z[4];
    z[32]=z[14]*z[32];
    z[27]=z[30] - n<T>(35,6)*z[31] + z[27] + z[32] + z[15];
    z[30]= - n<T>(13,2)*z[3] + n<T>(5,2)*z[5] + z[2];
    z[21]=z[30]*z[21];
    z[30]=2*z[2];
    z[31]= - z[5] + z[30];
    z[31]=z[2]*z[31];
    z[21]=z[21] + z[28] + n<T>(5,9)*z[31];
    z[21]=z[3]*z[21];
    z[24]=z[24] - z[2];
    z[24]=z[2]*z[24];
    z[24]= - n<T>(7,2)*z[28] + z[24];
    z[28]= - n<T>(7,9)*z[4] - z[3] + z[5] + n<T>(1,3)*z[2];
    z[28]=z[28]*z[29];
    z[29]= - z[30] + z[3];
    z[29]=z[3]*z[29];
    z[24]=z[28] + n<T>(1,3)*z[24] + z[29];
    z[23]=z[24]*z[23];
    z[24]=n<T>(5,9)*z[25] - z[26];
    z[24]=z[7]*z[24];
    z[25]= - n<T>(1,2)*z[11] - n<T>(7,54)*z[9];
    z[25]=i*z[25];
    z[26]= - z[5] - z[3];
    z[26]=z[13]*z[26];

    r +=  - n<T>(29,36)*z[10] + n<T>(5,8)*z[12] + n<T>(11,18)*z[16] + 2*z[17] - n<T>(5,18)*z[18]
     - n<T>(1,6)*z[19]
     - n<T>(2,9)*z[20]
     + z[21]
     + z[22]
     + z[23]
     +  z[24] + z[25] + n<T>(13,9)*z[26] + n<T>(1,9)*z[27];
 
    return r;
}

template std::complex<double> qg_2lha_tf175(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf175(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
