#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf795(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[13];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=c[13];
    z[4]=c[20];
    z[5]=c[23];
    z[6]=f[24];
    z[7]=f[42];
    z[8]=f[62];
    z[9]=f[77];
    z[10]=f[79];
    z[11]=f[80];
    z[12]=n<T>(1,20)*z[5] - z[4];
    z[12]=n<T>(1,3)*z[12] - n<T>(4,5)*z[3];
    z[12]=2137*z[12] + n<T>(37267,1620)*z[1];
    z[12]=i*z[12];

    r += n<T>(73,189)*z[2] + z[6] + n<T>(5,2)*z[7] - n<T>(1289,63)*z[8] + n<T>(218,63)*
      z[9] - z[10] + z[11] + n<T>(1,21)*z[12];
 
    return r;
}

template std::complex<double> qg_2lha_tf795(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf795(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
