#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf420(
  const std::array<std::complex<T>,111>& f
) {
    return f[10];
}

template std::complex<double> qg_2lha_tf420(
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf420(
  const std::array<std::complex<dd_real>,111>& f
);
#endif
