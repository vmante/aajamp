#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf562(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[107];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[15];
    z[12]=d[16];
    z[13]=d[9];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[10];
    z[26]=e[11];
    z[27]=e[13];
    z[28]=c[3];
    z[29]=c[11];
    z[30]=c[15];
    z[31]=c[19];
    z[32]=c[21];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[30];
    z[38]=c[31];
    z[39]=e[14];
    z[40]=e[3];
    z[41]=e[6];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[3];
    z[45]=f[4];
    z[46]=f[5];
    z[47]=f[6];
    z[48]=f[7];
    z[49]=f[9];
    z[50]=f[11];
    z[51]=f[12];
    z[52]=f[13];
    z[53]=f[16];
    z[54]=f[17];
    z[55]=f[18];
    z[56]=f[20];
    z[57]=f[23];
    z[58]=f[30];
    z[59]=f[31];
    z[60]=f[32];
    z[61]=f[33];
    z[62]=f[34];
    z[63]=f[36];
    z[64]=f[37];
    z[65]=f[38];
    z[66]=f[39];
    z[67]=f[41];
    z[68]=f[43];
    z[69]=f[48];
    z[70]=f[50];
    z[71]=f[51];
    z[72]=f[52];
    z[73]=f[55];
    z[74]=f[56];
    z[75]=f[58];
    z[76]=f[64];
    z[77]=f[65];
    z[78]=z[1]*i;
    z[79]=z[78] + z[8];
    z[80]=n<T>(1,12)*z[10];
    z[81]=n<T>(1,2)*z[6];
    z[82]=2*z[9];
    z[79]= - n<T>(7,12)*z[4] + z[81] + n<T>(1,12)*z[7] - z[80] + n<T>(11,8)*z[5]
     - 
   n<T>(27,8)*z[2] + z[82] + n<T>(1,4)*z[79];
    z[79]=z[4]*z[79];
    z[83]=2*z[78];
    z[84]=z[83] - z[9];
    z[85]= - z[84]*z[82];
    z[86]=5*z[9];
    z[87]= - z[86] + n<T>(5,2)*z[2];
    z[88]=27*z[78] + z[87];
    z[88]=z[2]*z[88];
    z[89]=n<T>(1,2)*z[10];
    z[90]=z[89] - z[78];
    z[91]=z[10]*z[90];
    z[92]=n<T>(1,2)*z[7];
    z[93]=n<T>(1,3)*z[78];
    z[94]=n<T>(1,6)*z[7] - z[93] - z[10];
    z[94]=z[94]*z[92];
    z[95]=z[78] - z[8];
    z[96]=z[2] - z[9];
    z[95]=z[96] - n<T>(1,2)*z[95];
    z[95]=z[8]*z[95];
    z[97]=n<T>(5,4)*z[6] - z[8] - n<T>(5,2)*z[78] - z[96];
    z[97]=z[6]*z[97];
    z[98]= - z[78] + n<T>(1,2)*z[5];
    z[99]= - z[9] + z[98];
    z[99]=z[5]*z[99];
    z[79]=z[79] + z[97] + z[95] + z[94] - n<T>(1,6)*z[91] + n<T>(11,4)*z[99] + 
    z[85] + n<T>(1,4)*z[88];
    z[79]=z[4]*z[79];
    z[85]=z[78] - z[9];
    z[88]=n<T>(1,2)*z[2];
    z[91]=z[85] + z[88];
    z[91]=z[91]*z[2];
    z[94]=n<T>(1,2)*z[9];
    z[95]=z[94] - z[78];
    z[97]=z[95]*z[9];
    z[99]=z[78] - z[5];
    z[100]= - z[89] - z[99];
    z[100]=z[10]*z[100];
    z[92]= - z[92] - z[85];
    z[92]=z[7]*z[92];
    z[101]=z[8] - z[2];
    z[102]= - n<T>(5,12)*z[85] - z[101];
    z[102]=z[8]*z[102];
    z[103]=z[98]*z[5];
    z[83]=z[83] - n<T>(25,16)*z[9];
    z[83]=n<T>(1,8)*z[6] + n<T>(1,24)*z[8] - n<T>(5,16)*z[7] - n<T>(7,16)*z[10] - n<T>(7,48)
   *z[5] + n<T>(1,3)*z[83] - n<T>(5,4)*z[2];
    z[83]=z[6]*z[83];
    z[83]=z[83] + z[102] + n<T>(5,8)*z[92] + n<T>(7,8)*z[100] - n<T>(7,24)*z[103]
     - 
   n<T>(25,24)*z[97] + z[91];
    z[83]=z[6]*z[83];
    z[92]=z[97] + z[91];
    z[100]=z[6] - z[9];
    z[102]=n<T>(1,4)*z[5];
    z[90]= - n<T>(11,12)*z[3] - z[102] + z[2] - z[90] + n<T>(1,4)*z[100];
    z[90]=z[3]*z[90];
    z[100]=z[96] + z[78];
    z[100]= - n<T>(1,2)*z[100] + z[5];
    z[100]=z[5]*z[100];
    z[95]=n<T>(5,4)*z[10] + z[5] - z[88] + z[95];
    z[95]=z[10]*z[95];
    z[104]=z[6] + z[99];
    z[81]=z[104]*z[81];
    z[81]=z[90] + z[81] + z[95] + n<T>(1,2)*z[92] + z[100];
    z[81]=z[3]*z[81];
    z[90]= - n<T>(5,2)*z[3] - n<T>(7,2)*z[10] + z[96] + z[99];
    z[90]=z[25]*z[90];
    z[81]=z[81] + z[90];
    z[89]= - n<T>(1,3)*z[13] - z[4] + n<T>(3,2)*z[7] + z[89] + z[94] - z[98];
    z[89]=z[13]*z[89];
    z[90]=z[10] - z[9];
    z[92]= - n<T>(5,6)*z[7] - n<T>(5,3)*z[78] - z[90];
    z[92]=z[7]*z[92];
    z[93]= - n<T>(13,6)*z[10] - z[93] - z[9];
    z[93]=z[10]*z[93];
    z[90]= - z[7] + z[5] + z[90];
    z[90]=z[8]*z[90];
    z[89]=z[89] + z[90] + z[92] + z[93] + 3*z[97] - z[103];
    z[90]=2*z[10] + z[7];
    z[90]=z[4]*z[90];
    z[89]=n<T>(1,3)*z[90] + n<T>(1,2)*z[89];
    z[89]=z[13]*z[89];
    z[90]=z[78]*z[5];
    z[90]=z[90] + z[28];
    z[92]= - z[3]*z[78];
    z[92]=z[92] - z[90];
    z[92]=z[11]*z[92];
    z[93]=z[78]*z[9];
    z[93]=z[93] + z[28];
    z[95]=z[8]*z[78];
    z[95]=z[95] + z[93];
    z[95]=z[15]*z[95];
    z[100]= - z[3] + z[99];
    z[100]=z[19]*z[100];
    z[103]=z[8] - z[85];
    z[103]=z[24]*z[103];
    z[92]=z[92] + z[95] + z[100] + z[103];
    z[95]=7*z[9];
    z[100]= - z[88] - 13*z[78] + z[95];
    z[100]=z[2]*z[100];
    z[103]= - n<T>(7,4)*z[5] + n<T>(7,2)*z[78] + z[96];
    z[103]=z[5]*z[103];
    z[104]= - n<T>(49,2)*z[5] + n<T>(53,2)*z[78] - 11*z[96];
    z[105]=n<T>(1,3)*z[10];
    z[104]=n<T>(1,2)*z[104] + z[105];
    z[104]=z[10]*z[104];
    z[106]=z[78] - n<T>(13,2)*z[9];
    z[106]=z[9]*z[106];
    z[100]=z[104] + 7*z[103] + z[106] + z[100];
    z[80]=z[100]*z[80];
    z[100]=z[102] - z[88] + z[78] + z[94];
    z[100]=z[5]*z[100];
    z[91]=n<T>(77,4)*z[97] + z[91];
    z[91]=n<T>(1,2)*z[91] + z[100];
    z[100]=n<T>(1,3)*z[2];
    z[102]=n<T>(7,3)*z[78] - z[9];
    z[102]=n<T>(5,8)*z[102] - z[100];
    z[102]= - n<T>(109,72)*z[7] + n<T>(1,2)*z[102] - n<T>(1,3)*z[5];
    z[102]=z[7]*z[102];
    z[103]=z[10] - z[94] + z[2];
    z[103]=z[10]*z[103];
    z[91]=z[102] + n<T>(1,3)*z[91] + z[103];
    z[91]=z[7]*z[91];
    z[102]=13*z[9];
    z[103]=5*z[78];
    z[104]=z[103] - z[102];
    z[94]=z[104]*z[94];
    z[95]= - n<T>(19,2)*z[2] + z[103] + z[95];
    z[95]=z[2]*z[95];
    z[98]=z[96] - z[98];
    z[98]=z[5]*z[98];
    z[94]=z[98] + z[94] + z[95];
    z[95]= - z[10]*z[96];
    z[98]=z[5] - z[2];
    z[103]=n<T>(41,12)*z[7] + n<T>(1,3)*z[98] - z[10];
    z[103]=z[7]*z[103];
    z[94]=z[103] + n<T>(1,3)*z[94] + z[95];
    z[95]= - 31*z[78] - z[86];
    z[95]=n<T>(1,2)*z[95] - 5*z[2];
    z[95]=n<T>(1,6)*z[8] + n<T>(31,8)*z[7] + n<T>(1,4)*z[95] - z[5];
    z[95]=z[8]*z[95];
    z[94]=n<T>(1,2)*z[94] + n<T>(1,3)*z[95];
    z[94]=z[8]*z[94];
    z[95]= - z[7]*z[78];
    z[90]=z[95] - z[90];
    z[90]=z[14]*z[90];
    z[95]=7*z[8] - 143*z[7] + n<T>(217,2)*z[10] + n<T>(133,2)*z[5] - z[102] - 
   n<T>(109,2)*z[2];
    z[95]=n<T>(1,4)*z[95] + 19*z[6];
    z[95]= - n<T>(5,3)*z[13] - n<T>(7,18)*z[3] + n<T>(1,9)*z[95] - n<T>(13,8)*z[4];
    z[95]=z[28]*z[95];
    z[99]=z[7] - z[99];
    z[99]=z[21]*z[99];
    z[90]= - z[67] - z[65] + z[60] + z[55] - z[99] - z[95] - z[90] + 
    z[75] + z[69];
    z[86]=73*z[2] - 209*z[78] - z[86];
    z[86]=z[86]*z[88];
    z[86]= - 61*z[97] + z[86];
    z[86]=z[86]*z[100];
    z[88]=z[78] - n<T>(5,3)*z[9];
    z[88]=z[88]*npow(z[9],2);
    z[95]=n<T>(43,2)*z[5] - 43*z[78] + 17*z[2];
    z[95]=n<T>(1,3)*z[95] + n<T>(3,2)*z[4];
    z[95]=z[18]*z[95];
    z[86]=z[77] + z[95] + z[88] + z[86] + z[76] + z[72];
    z[88]=z[4] + z[6];
    z[95]=z[78]*z[88];
    z[95]=z[28] + z[95];
    z[95]=z[12]*z[95];
    z[88]=z[78] - z[88];
    z[88]=z[20]*z[88];
    z[88]=z[95] + z[88];
    z[87]=n<T>(23,2)*z[78] - z[87];
    z[87]=z[2]*z[87];
    z[87]=n<T>(13,4)*z[97] + z[87];
    z[95]= - n<T>(1,6)*z[5] - n<T>(23,12)*z[2] + z[78] + n<T>(13,24)*z[9];
    z[95]=z[5]*z[95];
    z[87]=n<T>(1,3)*z[87] + z[95];
    z[87]=z[5]*z[87];
    z[95]=z[6] - z[8];
    z[82]=n<T>(27,4)*z[4] + n<T>(19,4)*z[2] - n<T>(27,2)*z[78] + z[82] + 2*z[95];
    z[82]=z[17]*z[82];
    z[95]= - n<T>(31,4)*z[8] - n<T>(35,4)*z[7] + n<T>(25,4)*z[85] - z[98];
    z[95]=z[22]*z[95];
    z[95]=z[95] - z[70];
    z[97]= - n<T>(4,3)*z[13] - n<T>(1,3)*z[7] + z[78] + z[101];
    z[97]=z[27]*z[97];
    z[96]= - n<T>(2,3)*z[13] + z[7] + z[105] - z[96];
    z[96]=z[39]*z[96];
    z[78]=z[10]*z[78];
    z[78]=z[78] + z[93];
    z[78]=z[16]*z[78];
    z[93]= - z[58] + z[73] + z[71];
    z[98]=z[43] - z[30];
    z[99]=z[52] + z[49];
    z[100]=z[74] + z[62];
    z[101]= - z[6] - z[3];
    z[101]=z[40]*z[101];
    z[101]=z[101] + z[57];
    z[102]=n<T>(281,12)*z[35] + n<T>(221,24)*z[33] - z[32] + n<T>(1,12)*z[29];
    z[102]=i*z[102];
    z[84]=z[2] - z[84];
    z[84]=z[23]*z[84];
    z[85]= - z[10] + z[85];
    z[85]=z[26]*z[85];
    z[103]=z[8] + z[6];
    z[103]=z[41]*z[103];

    r += n<T>(109,144)*z[31] - n<T>(27,8)*z[34] - n<T>(65,6)*z[36] - 4*z[37] + n<T>(145,18)*z[38]
     + n<T>(49,8)*z[42]
     + n<T>(11,8)*z[44] - n<T>(77,48)*z[45]
     + n<T>(5,3)*
      z[46] - n<T>(7,8)*z[47] - n<T>(14,3)*z[48] + n<T>(13,24)*z[50] + n<T>(7,48)*z[51]
       - n<T>(7,16)*z[53] + n<T>(9,4)*z[54] + n<T>(1,8)*z[56] + n<T>(85,48)*z[59]
     + n<T>(41,6)*z[61]
     + n<T>(1,48)*z[63] - z[64] - n<T>(9,16)*z[66] - n<T>(1,6)*z[68]
     +  z[78] + z[79] + z[80] + n<T>(7,6)*z[81] + z[82] + z[83] + n<T>(8,3)*z[84]
       + z[85] + n<T>(1,4)*z[86] + z[87] + n<T>(3,2)*z[88] + z[89] - n<T>(1,2)*
      z[90] + z[91] + n<T>(7,4)*z[92] + n<T>(1,12)*z[93] + z[94] + n<T>(1,3)*z[95]
       + z[96] + z[97] + 2*z[98] + n<T>(5,8)*z[99] + n<T>(3,4)*z[100] + n<T>(7,12)*
      z[101] + z[102] + n<T>(19,12)*z[103];
 
    return r;
}

template std::complex<double> qg_2lha_tf562(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf562(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
