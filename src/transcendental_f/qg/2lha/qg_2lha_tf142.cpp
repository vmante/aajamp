#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf142(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[112];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[3];
    z[22]=e[4];
    z[23]=e[5];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[10];
    z[28]=e[11];
    z[29]=e[12];
    z[30]=e[13];
    z[31]=c[3];
    z[32]=c[11];
    z[33]=c[15];
    z[34]=c[17];
    z[35]=c[19];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[31];
    z[41]=e[6];
    z[42]=e[14];
    z[43]=f[0];
    z[44]=f[1];
    z[45]=f[2];
    z[46]=f[3];
    z[47]=f[4];
    z[48]=f[5];
    z[49]=f[6];
    z[50]=f[7];
    z[51]=f[10];
    z[52]=f[11];
    z[53]=f[12];
    z[54]=f[13];
    z[55]=f[14];
    z[56]=f[15];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[18];
    z[60]=f[21];
    z[61]=f[23];
    z[62]=f[30];
    z[63]=f[31];
    z[64]=f[32];
    z[65]=f[33];
    z[66]=f[34];
    z[67]=f[35];
    z[68]=f[36];
    z[69]=f[39];
    z[70]=f[43];
    z[71]=f[50];
    z[72]=f[51];
    z[73]=f[52];
    z[74]=f[53];
    z[75]=f[55];
    z[76]=f[58];
    z[77]=f[60];
    z[78]=f[76];
    z[79]=f[105];
    z[80]=f[106];
    z[81]=n<T>(1,2)*z[5];
    z[82]=z[1]*i;
    z[83]=z[81] - z[82];
    z[83]=z[83]*z[5];
    z[84]=n<T>(1,3)*z[7];
    z[85]=z[5] + 7*z[7];
    z[85]=z[85]*z[84];
    z[85]=z[83] + z[85];
    z[86]=n<T>(1,3)*z[11];
    z[87]=z[86] + z[82] - n<T>(2,3)*z[5];
    z[87]=z[11]*z[87];
    z[88]=n<T>(1,2)*z[3];
    z[89]=z[88] + z[11];
    z[90]= - z[7] + n<T>(91,9)*z[82] - z[5];
    z[90]=n<T>(73,36)*z[6] + n<T>(1,2)*z[90] + z[89];
    z[91]=n<T>(1,3)*z[6];
    z[90]=z[90]*z[91];
    z[92]=z[82] - z[5];
    z[93]=n<T>(1,2)*z[2];
    z[94]=z[92] + z[93];
    z[95]= - z[7] - z[94];
    z[95]=z[95]*z[93];
    z[96]=n<T>(1,2)*z[6];
    z[89]=z[96] + z[7] - z[89];
    z[89]=z[4]*z[89];
    z[97]=z[82] + z[6];
    z[98]=z[2] - z[5];
    z[97]=z[98] - n<T>(91,27)*z[97];
    z[97]=n<T>(1,2)*z[97] + n<T>(32,27)*z[9];
    z[97]=z[9]*z[97];
    z[99]= - n<T>(43,4)*z[7] - n<T>(41,3)*z[82] + n<T>(67,4)*z[5];
    z[99]=n<T>(1,3)*z[99] + z[11];
    z[99]= - n<T>(175,108)*z[8] + n<T>(91,108)*z[9] + n<T>(1,3)*z[99] - z[93];
    z[99]=z[8]*z[99];
    z[100]=z[5] - z[7];
    z[100]=z[3]*z[100];
    z[85]=z[99] + z[97] + n<T>(1,3)*z[89] + z[95] + z[90] + n<T>(1,6)*z[100] + n<T>(1,2)*z[85]
     + z[87];
    z[85]=z[8]*z[85];
    z[87]= - n<T>(91,36)*z[7] + n<T>(55,18)*z[82] + z[5];
    z[87]=z[7]*z[87];
    z[87]=n<T>(25,36)*z[83] + z[87];
    z[89]=n<T>(1,2)*z[11];
    z[90]=2*z[82];
    z[95]= - z[90] + z[89];
    z[86]=z[95]*z[86];
    z[95]=n<T>(1,3)*z[3];
    z[97]= - z[11] - z[90] + z[5];
    z[97]=z[97]*z[95];
    z[99]=z[82] + z[81];
    z[99]=n<T>(25,9)*z[99] - z[7];
    z[99]= - n<T>(25,72)*z[2] + n<T>(1,2)*z[99] - z[95];
    z[99]=z[2]*z[99];
    z[100]=n<T>(1,2)*z[4];
    z[101]=z[100] - z[82] - z[5];
    z[101]=z[4]*z[101];
    z[102]=z[3] + z[11];
    z[103]= - n<T>(55,12)*z[7] - z[82] + n<T>(25,24)*z[5];
    z[103]= - n<T>(5,6)*z[9] + n<T>(25,16)*z[4] - n<T>(25,12)*z[2] - n<T>(91,72)*z[6]
     + 
   n<T>(1,2)*z[103] + z[102];
    z[104]=n<T>(1,3)*z[9];
    z[103]=z[103]*z[104];
    z[105]= - n<T>(1,6)*z[6] + n<T>(1,3)*z[82] + z[7];
    z[105]=z[6]*z[105];
    z[86]=z[103] + n<T>(25,24)*z[101] + z[99] + n<T>(91,36)*z[105] + z[97] + n<T>(1,2)*z[87]
     + z[86];
    z[86]=z[9]*z[86];
    z[87]=n<T>(1,2)*z[7];
    z[97]=61*z[82] + 5*z[5];
    z[89]= - n<T>(5,18)*z[2] - z[89] + n<T>(1,9)*z[97] + z[87];
    z[97]=n<T>(1,3)*z[2];
    z[89]=z[89]*z[97];
    z[99]=n<T>(1,3)*z[5];
    z[101]=n<T>(1,2)*z[82];
    z[103]=n<T>(5,12)*z[7] - z[101] - z[99];
    z[103]=z[7]*z[103];
    z[105]= - 263*z[82] + n<T>(185,2)*z[5];
    z[105]=z[5]*z[105];
    z[103]=n<T>(1,108)*z[105] + z[103];
    z[105]=z[7] - z[90] + z[81];
    z[106]=n<T>(1,4)*z[11];
    z[105]=n<T>(1,3)*z[105] + z[106];
    z[105]=z[11]*z[105];
    z[107]=z[91] - z[3] + n<T>(1,12)*z[82] - z[7];
    z[107]=z[107]*z[91];
    z[108]=5*z[82];
    z[109]=z[108] + n<T>(263,72)*z[5];
    z[109]= - n<T>(53,54)*z[4] - n<T>(61,27)*z[2] - n<T>(1,12)*z[6] - n<T>(11,18)*z[3]
    - n<T>(1,6)*z[11] + n<T>(1,3)*z[109] + n<T>(1,4)*z[7];
    z[100]=z[109]*z[100];
    z[109]=n<T>(11,6)*z[82] + z[11];
    z[109]=n<T>(1,3)*z[109] + n<T>(1,4)*z[3];
    z[109]=z[3]*z[109];
    z[89]=z[100] + z[89] + z[107] + z[109] + n<T>(1,2)*z[103] + z[105];
    z[89]=z[4]*z[89];
    z[100]= - n<T>(11,4)*z[7] + n<T>(11,2)*z[82] - z[5];
    z[100]=z[7]*z[100];
    z[83]= - n<T>(137,18)*z[83] + z[100];
    z[100]= - z[106] - n<T>(1,2)*z[92] + 5*z[7];
    z[100]=z[11]*z[100];
    z[103]=7*z[82];
    z[105]= - z[103] + 25*z[5];
    z[105]= - n<T>(7,18)*z[3] + z[11] + n<T>(1,9)*z[105] + z[7];
    z[88]=z[105]*z[88];
    z[105]= - 41*z[82] + 29*z[5];
    z[102]= - n<T>(17,24)*z[6] + n<T>(1,12)*z[105] - z[102];
    z[102]=z[6]*z[102];
    z[94]= - z[3] - z[94];
    z[94]=z[2]*z[94];
    z[83]=z[94] + z[102] + z[88] + n<T>(1,2)*z[83] + z[100];
    z[88]=z[82] + z[7];
    z[94]=n<T>(3,8)*z[4];
    z[88]=z[94] + z[91] - n<T>(3,4)*z[88] - z[95];
    z[88]=z[4]*z[88];
    z[100]=z[103] - n<T>(137,8)*z[5];
    z[94]= - n<T>(35,36)*z[10] + z[104] + z[94] - z[97] - n<T>(17,72)*z[6] + n<T>(47,54)*z[3]
     - n<T>(3,4)*z[11]
     + n<T>(1,27)*z[100]
     + n<T>(9,8)*z[7];
    z[94]=z[10]*z[94];
    z[100]=n<T>(1,2)*z[9];
    z[98]= - z[100] + z[3] - z[90] + z[98];
    z[98]=z[98]*z[104];
    z[83]=z[94] + z[98] + n<T>(1,3)*z[83] + z[88];
    z[83]=z[10]*z[83];
    z[88]= - z[103] + n<T>(43,2)*z[5];
    z[88]=z[5]*z[88];
    z[94]= - n<T>(79,12)*z[7] - n<T>(73,6)*z[82] - z[5];
    z[94]=z[7]*z[94];
    z[88]=n<T>(1,6)*z[88] + z[94];
    z[94]=z[82] - z[11];
    z[98]=2*z[5];
    z[102]=z[7] - z[98] + z[94];
    z[102]=z[11]*z[102];
    z[103]=2*z[3] + z[11] + z[87] + z[108] - 4*z[5];
    z[103]=z[3]*z[103];
    z[104]=n<T>(11,3)*z[82];
    z[105]= - 85*z[7] - z[104] + 19*z[5];
    z[105]=n<T>(217,36)*z[6] + n<T>(5,6)*z[3] + n<T>(1,12)*z[105] - z[11];
    z[105]=z[105]*z[96];
    z[88]=z[105] + z[103] + n<T>(1,2)*z[88] + z[102];
    z[88]=z[88]*z[91];
    z[91]= - z[81] + z[106] + z[87];
    z[102]=z[90] - z[91];
    z[102]=z[11]*z[102];
    z[103]=4 + n<T>(23,12)*z[82];
    z[103]=11*z[103] - n<T>(65,12)*z[5];
    z[103]=z[5]*z[103];
    z[105]=n<T>(7,2)*z[82];
    z[106]= - z[105] + z[5];
    z[106]=5*z[106] + n<T>(17,4)*z[7];
    z[106]=z[7]*z[106];
    z[103]=z[103] + z[106];
    z[106]=8*z[3] - z[82] - 7*z[5];
    z[106]=z[106]*z[95];
    z[107]=n<T>(31,4)*z[7] - n<T>(109,12)*z[5] - 22 - n<T>(157,12)*z[82];
    z[96]=n<T>(209,72)*z[2] - z[96] + n<T>(11,6)*z[3] + n<T>(1,3)*z[107] - z[11];
    z[96]=z[2]*z[96];
    z[107]=z[82] + z[11];
    z[109]=n<T>(7,24)*z[6] + z[107];
    z[109]=z[6]*z[109];
    z[96]=z[96] + z[109] + z[106] + n<T>(1,3)*z[103] + z[102];
    z[96]=z[96]*z[97];
    z[97]= - z[104] + n<T>(3,2)*z[5];
    z[97]=z[97]*z[81];
    z[102]=n<T>(13,12) + z[82];
    z[81]= - n<T>(2,3)*z[7] + n<T>(1,3)*z[102] - z[81];
    z[81]=z[7]*z[81];
    z[102]= - 3*z[82] + n<T>(5,3)*z[5];
    z[84]= - n<T>(7,12)*z[11] + n<T>(1,4)*z[102] - z[84];
    z[84]=z[11]*z[84];
    z[81]=z[84] + z[97] + z[81];
    z[81]=z[11]*z[81];
    z[84]=11*z[82] + n<T>(29,2)*z[5];
    z[84]=z[84]*z[99];
    z[97]= - 35*z[92] - n<T>(41,2)*z[7];
    z[87]=z[97]*z[87];
    z[84]=z[84] + z[87];
    z[87]=n<T>(5,3) + z[101] + z[91];
    z[87]=z[11]*z[87];
    z[91]= - n<T>(17,3)*z[5] - 5 + n<T>(23,6)*z[82];
    z[91]=n<T>(1,3)*z[91] + z[7];
    z[91]= - n<T>(28,9)*z[3] + n<T>(1,2)*z[91] - z[11];
    z[91]=z[3]*z[91];
    z[84]=z[91] + n<T>(1,3)*z[84] + z[87];
    z[84]=z[84]*z[95];
    z[87]=z[9] - z[2];
    z[90]= - n<T>(137,18)*z[10] - n<T>(101,18)*z[3] - z[98] - n<T>(5,3) + z[90] - 2*
    z[87];
    z[90]=z[27]*z[90];
    z[91]= - z[4] + z[107] - z[10];
    z[91]=n<T>(19,6)*z[6] + n<T>(7,6)*z[3] - 2*z[91];
    z[91]=z[21]*z[91];
    z[90]=z[61] + z[77] + z[90] + z[91];
    z[91]=z[100] - z[82] + z[93];
    z[91]=z[25]*z[91];
    z[93]=z[82]*z[9];
    z[93]=z[93] + z[31];
    z[97]= - z[8]*z[82];
    z[97]=z[97] - z[93];
    z[97]=z[16]*z[97];
    z[98]=z[82] - z[9];
    z[99]= - z[8] + z[98];
    z[99]=z[26]*z[99];
    z[97]=z[97] + z[99];
    z[99]=z[82]*z[6];
    z[100]= - z[4]*z[105];
    z[100]= - n<T>(7,2)*z[31] + z[99] + z[100];
    z[100]=z[14]*z[100];
    z[101]=n<T>(7,2)*z[4] - z[105] - z[6];
    z[101]=z[22]*z[101];
    z[100]=z[100] + z[101];
    z[101]=z[4] + z[11];
    z[101]=z[82]*z[101];
    z[101]=z[31] + z[101];
    z[101]=z[13]*z[101];
    z[94]= - z[4] + z[94];
    z[94]=z[29]*z[94];
    z[94]=z[101] + z[94];
    z[101]= - z[5] - z[3];
    z[101]=z[82]*z[101];
    z[101]= - z[31] + z[101];
    z[101]=z[12]*z[101];
    z[102]= - z[3] + z[92];
    z[102]=z[20]*z[102];
    z[101]=z[101] + z[102];
    z[102]=z[4] + z[2];
    z[102]= - 44 - n<T>(409,12)*z[82] + n<T>(409,24)*z[102];
    z[102]=z[18]*z[102];
    z[103]= - 23*z[11] - n<T>(119,8)*z[5] + n<T>(113,3)*z[7];
    z[103]= - n<T>(233,24)*z[8] + n<T>(65,18)*z[10] - n<T>(797,144)*z[9] + n<T>(205,36)*
    z[4] + n<T>(1625,144)*z[2] + n<T>(107,144)*z[6] + n<T>(1,2)*z[103] + n<T>(14,3)*z[3]
   ;
    z[103]=z[31]*z[103];
    z[102]=z[103] + z[102];
    z[103]=23*z[82] - n<T>(121,4)*z[5];
    z[103]=z[5]*z[103];
    z[104]= - 37 - z[108];
    z[104]=n<T>(4,3)*z[7] + n<T>(1,12)*z[104] + z[5];
    z[104]=z[7]*z[104];
    z[103]=n<T>(1,9)*z[103] + z[104];
    z[103]=z[7]*z[103];
    z[104]= - n<T>(7,2)*z[92] - z[7];
    z[95]= - n<T>(1,2)*z[8] - n<T>(4,3)*z[10] - z[95] + n<T>(1,3)*z[104] - z[11];
    z[95]=z[30]*z[95];
    z[87]=n<T>(55,18)*z[8] + n<T>(37,18)*z[7] + z[92] - z[87];
    z[87]=z[24]*z[87];
    z[92]= - n<T>(59,2)*z[7] + 44 + n<T>(5,2)*z[5];
    z[104]= - z[82]*z[92];
    z[99]= - n<T>(5,18)*z[31] + n<T>(1,9)*z[104] - z[99];
    z[99]=z[15]*z[99];
    z[92]= - n<T>(5,2)*z[82] + z[92];
    z[92]=n<T>(1,9)*z[92] + z[6];
    z[92]=z[23]*z[92];
    z[104]=z[10]*z[82];
    z[93]=z[104] + z[93];
    z[93]=z[17]*z[93];
    z[104]=z[67] + z[78] - z[70];
    z[105]=z[47] + z[37];
    z[106]=z[56] + z[50];
    z[107]=z[80] - z[79];
    z[108]=z[6] + z[8];
    z[108]=z[41]*z[108];
    z[108]=z[108] - z[62];
    z[109]= - n<T>(59,2)*z[2] + 59*z[82] - 49*z[5];
    z[109]=n<T>(1,3)*z[109] + n<T>(13,2)*z[4];
    z[109]=z[19]*z[109];
    z[109]=z[109] - z[55];
    z[82]= - z[82] + n<T>(5,9)*z[5];
    z[82]=z[82]*npow(z[5],2);
    z[82]= - z[33] + z[82] - z[71];
    z[110]=n<T>(125,27)*z[38] + n<T>(157,108)*z[36] + z[34] + n<T>(301,432)*z[32];
    z[110]=i*z[110];
    z[98]= - z[10] + z[98];
    z[98]=z[28]*z[98];
    z[111]= - z[11] - z[10];
    z[111]=z[42]*z[111];

    r +=  - n<T>(1025,1296)*z[35] - n<T>(125,54)*z[39] - n<T>(925,108)*z[40] + n<T>(19,36)*z[43]
     - n<T>(7,72)*z[44]
     - n<T>(7,6)*z[45]
     + n<T>(263,432)*z[46]
     + n<T>(25,36)
      *z[48] - n<T>(2,3)*z[49] - n<T>(7,54)*z[51] + n<T>(25,144)*z[52] + n<T>(5,72)*
      z[53] + n<T>(163,216)*z[54] - n<T>(17,72)*z[57] + n<T>(25,48)*z[58] + n<T>(19,108)
      *z[59] - n<T>(4,9)*z[60] - n<T>(19,72)*z[63] - n<T>(55,36)*z[64] + 4*z[65]
     +  n<T>(11,18)*z[66] + n<T>(91,216)*z[68] - n<T>(91,72)*z[69] + n<T>(11,24)*z[72]
     - 
      n<T>(13,12)*z[73] + n<T>(4,3)*z[74] - n<T>(3,8)*z[75] + n<T>(9,8)*z[76] + z[81]
       + n<T>(5,12)*z[82] + z[83] + z[84] + z[85] + z[86] + z[87] + z[88]
       + z[89] + n<T>(1,3)*z[90] + n<T>(25,18)*z[91] + z[92] + z[93] + n<T>(5,6)*
      z[94] + z[95] + z[96] + n<T>(37,54)*z[97] + z[98] + z[99] + n<T>(1,18)*
      z[100] + n<T>(28,27)*z[101] + n<T>(1,9)*z[102] + z[103] + n<T>(1,2)*z[104]
     - 
      n<T>(209,216)*z[105] - n<T>(8,3)*z[106] - n<T>(1,6)*z[107] + n<T>(91,54)*z[108]
       + n<T>(1,36)*z[109] + z[110] + n<T>(3,2)*z[111];
 
    return r;
}

template std::complex<double> qg_2lha_tf142(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf142(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
