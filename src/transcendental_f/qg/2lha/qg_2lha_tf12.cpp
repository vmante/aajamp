#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf12(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[18];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=d[9];
    z[5]=e[7];
    z[6]=e[13];
    z[7]=c[3];
    z[8]=d[0];
    z[9]=d[1];
    z[10]=c[11];
    z[11]=z[9] + z[8];
    z[11]=n<T>(1,4)*z[11];
    z[12]=z[1]*i;
    z[12]=n<T>(1,4)*z[12];
    z[13]=z[2] - n<T>(5,3);
    z[14]=z[13] - z[12] + z[11];
    z[15]=z[3] - z[2];
    z[15]=z[15]*z[4];
    z[16]=z[3]*z[2];
    z[17]=npow(z[2],2);
    z[15]=z[15] - z[16] + z[17];
    z[14]=z[15]*z[14];
    z[11]=z[11] + z[13];
    z[12]=z[11] - z[12];
    z[13]=z[6] + z[5];
    z[12]=z[12]*z[13];
    z[11]=z[7]*z[11];
    z[13]=z[10]*i;

    r +=  - n<T>(1,6)*z[11] + z[12] + n<T>(1,24)*z[13] + z[14];
 
    return r;
}

template std::complex<double> qg_2lha_tf12(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf12(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
