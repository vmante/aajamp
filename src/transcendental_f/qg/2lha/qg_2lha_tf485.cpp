#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf485(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[26];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=c[12];
    z[5]=c[13];
    z[6]=c[20];
    z[7]=c[23];
    z[8]=f[44];
    z[9]=f[42];
    z[10]=f[77];
    z[11]=f[81];
    z[12]=g[82];
    z[13]=g[90];
    z[14]=g[101];
    z[15]=g[131];
    z[16]=g[214];
    z[17]=g[245];
    z[18]=g[345];
    z[19]=g[347];
    z[20]=g[348];
    z[21]=z[2] - z[3];
    z[22]=z[4] + n<T>(3,2)*z[8];
    z[22]=z[22]*z[21];
    z[23]= - z[6] + n<T>(1,20)*z[7];
    z[24]=z[23] - n<T>(12,5)*z[5];
    z[24]=n<T>(11,180)*z[1] + 3*z[24];
    z[21]=z[24]*z[21];
    z[23]= - n<T>(1,4)*z[23] + n<T>(3,5)*z[5];
    z[21]=7*z[23] - n<T>(17,2160)*z[1] + z[21];
    z[21]=i*z[21];
    z[23]= - z[16] + z[12] + z[15];
    z[24]=z[14] - z[19];
    z[25]=z[11] + z[18];

    r +=  - n<T>(4,3)*z[4] + n<T>(7,8)*z[9] + n<T>(1,8)*z[10] - n<T>(3,2)*z[13] - n<T>(9,4)
      *z[17] - z[20] + z[21] + z[22] + n<T>(3,4)*z[23] + n<T>(1,2)*z[24] - n<T>(1,4)
      *z[25];
 
    return r;
}

template std::complex<double> qg_2lha_tf485(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf485(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
