#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf805(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[112];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[18];
    z[34]=c[19];
    z[35]=c[21];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[29];
    z[41]=c[30];
    z[42]=c[31];
    z[43]=e[3];
    z[44]=e[6];
    z[45]=e[14];
    z[46]=f[0];
    z[47]=f[1];
    z[48]=f[3];
    z[49]=f[4];
    z[50]=f[5];
    z[51]=f[6];
    z[52]=f[7];
    z[53]=f[8];
    z[54]=f[10];
    z[55]=f[11];
    z[56]=f[12];
    z[57]=f[13];
    z[58]=f[14];
    z[59]=f[16];
    z[60]=f[17];
    z[61]=f[18];
    z[62]=f[23];
    z[63]=f[30];
    z[64]=f[31];
    z[65]=f[32];
    z[66]=f[33];
    z[67]=f[35];
    z[68]=f[36];
    z[69]=f[37];
    z[70]=f[39];
    z[71]=f[43];
    z[72]=f[50];
    z[73]=f[51];
    z[74]=f[52];
    z[75]=f[53];
    z[76]=f[54];
    z[77]=f[55];
    z[78]=f[56];
    z[79]=f[58];
    z[80]=f[60];
    z[81]=f[85];
    z[82]=f[86];
    z[83]=z[12] + n<T>(97,12)*z[15];
    z[84]=n<T>(3,2)*z[9];
    z[85]=n<T>(1,6)*z[11];
    z[86]= - n<T>(569,120)*z[5] + n<T>(61,10)*z[6] - n<T>(1,6)*z[8] + n<T>(7,6)*z[2]
     + 
   n<T>(171,20)*z[7] + n<T>(11,30)*z[3] - n<T>(29,10)*z[4] - z[85] + z[84] + n<T>(247,60)*z[10]
     - z[83];
    z[86]=z[5]*z[86];
    z[87]=n<T>(4,5)*z[29];
    z[88]=n<T>(2,3)*z[26];
    z[89]=z[20] - n<T>(97,12)*z[22] + z[87] + z[88];
    z[90]=4*z[14];
    z[84]= - n<T>(253,80)*z[11] - z[90] - z[84];
    z[84]=z[11]*z[84];
    z[91]=z[3] + z[11];
    z[92]=2*z[4];
    z[93]=z[92] - z[12] + n<T>(5,3)*z[91];
    z[93]=z[3]*z[93];
    z[94]=1057*z[16] - 493*z[11];
    z[94]= - n<T>(53,15)*z[7] - n<T>(53,20)*z[3] + n<T>(1,60)*z[94] + z[92];
    z[94]=z[7]*z[94];
    z[95]=n<T>(37,10)*z[4];
    z[96]=n<T>(1,3)*z[3];
    z[97]=z[10] + n<T>(739,120)*z[11] + z[95] + z[96];
    z[98]=z[17] + n<T>(1057,60)*z[16];
    z[99]= - n<T>(11,10)*z[8] + n<T>(58,15)*z[2] + n<T>(619,60)*z[7] - n<T>(2,3)*z[9]
     + 
    z[98] - z[97];
    z[99]=z[8]*z[99];
    z[100]=n<T>(1,2)*z[6];
    z[101]= - n<T>(149,5)*z[15] - 55*z[10];
    z[101]= - n<T>(1831,120)*z[6] - n<T>(871,20)*z[8] + n<T>(147,10)*z[2] - n<T>(97,10)*
    z[7] - n<T>(11,2)*z[3] - n<T>(5,3)*z[4] + n<T>(115,4)*z[11] + n<T>(1,6)*z[101]
     + n<T>(19,5)*z[9];
    z[101]=z[101]*z[100];
    z[102]= - z[27] + n<T>(1057,60)*z[25];
    z[103]=4*z[21];
    z[104]=n<T>(2,5)*z[13];
    z[105]=z[104] - n<T>(37,24)*z[10];
    z[105]=z[10]*z[105];
    z[106]=n<T>(4,15)*z[9] + z[17] - n<T>(14,15)*z[10];
    z[106]=z[9]*z[106];
    z[107]= - 2*z[14] + n<T>(1,5)*z[13];
    z[107]= - n<T>(21,10)*z[4] + 4*z[11] - n<T>(29,30)*z[9] + 2*z[107] + z[10];
    z[107]=z[4]*z[107];
    z[108]=n<T>(1,3)*z[9];
    z[109]= - z[108] + n<T>(31,30)*z[3];
    z[110]= - n<T>(137,30)*z[2] - n<T>(103,20)*z[7] + n<T>(33,5)*z[4] + z[10] + 
    z[109];
    z[110]=z[2]*z[110];
    z[84]=z[86] + z[101] + z[99] + z[110] + z[94] + z[93] + z[107] + 
    z[84] + z[106] + z[105] + n<T>(69,5)*z[23] - n<T>(2,3)*z[24] - n<T>(77,10)*z[15]
    - n<T>(58,5)*z[18] - n<T>(49,15)*z[19] - z[103] + n<T>(2,5)*z[28] - z[102] + 
    z[89];
    z[84]=z[1]*z[84];
    z[86]=n<T>(487,5)*z[38] + n<T>(59,2)*z[36];
    z[86]=n<T>(857,120)*z[31] + n<T>(1,3)*z[86] - n<T>(7,5)*z[35];
    z[84]=n<T>(1,2)*z[86] + z[84];
    z[84]=i*z[84];
    z[86]=n<T>(32,5)*z[7];
    z[93]=n<T>(1,12)*z[8];
    z[94]=z[93] + n<T>(53,15)*z[2] - z[86] - z[95] - z[108] + z[96];
    z[94]=z[8]*z[94];
    z[95]=3*z[9];
    z[85]=z[95] + z[85];
    z[99]=n<T>(1,2)*z[11];
    z[85]=z[85]*z[99];
    z[101]=n<T>(1,2)*z[3];
    z[105]= - 5*z[11] - z[101];
    z[105]=z[105]*z[96];
    z[106]=77 - n<T>(247,6)*z[10];
    z[95]=n<T>(29,5)*z[4] + n<T>(1,3)*z[11] + n<T>(1,5)*z[106] - z[95];
    z[93]=n<T>(171,40)*z[5] - n<T>(301,120)*z[6] + z[93] - n<T>(23,15)*z[2] - n<T>(119,40)*z[7]
     + n<T>(1,4)*z[95]
     + n<T>(2,3)*z[3];
    z[93]=z[5]*z[93];
    z[95]=npow(z[9],2);
    z[106]=n<T>(3,4)*z[95];
    z[107]=npow(z[10],2);
    z[110]=npow(z[4],2);
    z[91]=13*z[91] - n<T>(171,2)*z[7];
    z[91]=z[7]*z[91];
    z[109]= - n<T>(73,60)*z[2] + n<T>(19,5)*z[7] + n<T>(4,5)*z[4] - z[109];
    z[109]=z[2]*z[109];
    z[111]=n<T>(19,3)*z[10] - z[11];
    z[111]=13*z[111] + 41*z[3];
    z[111]=n<T>(1,5)*z[111] + 29*z[7];
    z[111]= - n<T>(61,8)*z[6] + n<T>(32,5)*z[8] + n<T>(1,4)*z[111] - 5*z[2];
    z[111]=z[6]*z[111];
    z[85]=z[93] + z[111] + z[94] + z[109] + n<T>(1,20)*z[91] + z[105] + n<T>(29,20)*z[110]
     + z[85] - z[106]
     + n<T>(49,24)*z[107] - n<T>(176,45)*z[30] - n<T>(119,10)*z[23] - z[24]
     + n<T>(49,30)*z[19] - z[89];
    z[85]=z[5]*z[85];
    z[89]=n<T>(81,8)*z[2] + n<T>(7,2)*z[3] - 19*z[7];
    z[89]=z[2]*z[89];
    z[91]=n<T>(549,8)*z[11];
    z[93]=n<T>(871,16)*z[8] - 32*z[2] - z[91] + 7*z[7];
    z[93]=z[8]*z[93];
    z[89]=z[89] + z[93];
    z[93]=9 + n<T>(547,40)*z[11];
    z[93]=z[93]*z[99];
    z[94]=n<T>(19,2)*z[9];
    z[91]=n<T>(43,6)*z[4] + z[91] + z[94] - n<T>(37,4) - n<T>(53,3)*z[10];
    z[91]= - n<T>(1223,30)*z[6] + n<T>(493,40)*z[8] + n<T>(67,4)*z[2] + n<T>(403,20)*
    z[7] + n<T>(1,5)*z[91] - n<T>(11,4)*z[3];
    z[91]=z[91]*z[100];
    z[100]= - 21*z[29] + n<T>(149,3)*z[22];
    z[94]=n<T>(7,3)*z[10] - z[94];
    z[94]=n<T>(1,5)*z[94] + n<T>(5,12)*z[4];
    z[94]=z[4]*z[94];
    z[105]=n<T>(13,20)*z[11];
    z[109]=z[3]*z[105];
    z[111]= - n<T>(9,2) - z[3];
    z[111]=n<T>(13,2)*z[111] + 151*z[7];
    z[111]=z[7]*z[111];
    z[89]=z[91] + n<T>(1,10)*z[111] + z[109] + z[94] + z[93] + n<T>(19,20)*z[95]
    - n<T>(91,60)*z[107] - n<T>(4573,360)*z[30] - n<T>(889,20)*z[23] + n<T>(1,20)*
    z[100] + z[24] + n<T>(1,5)*z[89];
    z[89]=z[6]*z[89];
    z[88]= - z[88] + n<T>(1,3)*z[95];
    z[91]=z[9] - z[101];
    z[91]=z[91]*z[96];
    z[93]=z[9] - z[3];
    z[86]= - n<T>(53,30)*z[2] + n<T>(1,3)*z[93] + z[86];
    z[86]=z[2]*z[86];
    z[93]= - n<T>(419,15)*z[7] - z[108] + z[97];
    z[93]=n<T>(1,30)*z[8] + n<T>(1,2)*z[93] - n<T>(29,15)*z[2];
    z[93]=z[8]*z[93];
    z[94]=npow(z[11],2);
    z[97]=227*z[11] - n<T>(269,2)*z[7];
    z[97]=z[7]*z[97];
    z[86]=z[93] + z[86] + n<T>(1,30)*z[97] + z[91] + n<T>(37,20)*z[110] + n<T>(739,240)*z[94]
     - n<T>(11,144)*z[30]
     - n<T>(111,10)*z[23]
     + n<T>(1,3)*z[24]
     + z[88]
    + z[102];
    z[86]=z[8]*z[86];
    z[91]= - z[9] + 2*z[3];
    z[91]=z[91]*z[96];
    z[93]=2*z[94];
    z[96]=n<T>(49,6)*z[19] + 29*z[18];
    z[97]=npow(z[7],2);
    z[100]= - z[10] - z[108];
    z[100]=n<T>(53,20)*z[2] - n<T>(101,40)*z[7] + n<T>(1,6)*z[3] + n<T>(1,2)*z[100]
     - n<T>(2,5)*z[4];
    z[100]=z[2]*z[100];
    z[88]=z[100] - n<T>(173,40)*z[97] + z[91] - n<T>(33,10)*z[110] + z[93] + n<T>(209,180)*z[30]
     + n<T>(46,5)*z[23]
     + n<T>(4,3)*z[24]
     + n<T>(1,5)*z[96] - z[88];
    z[88]=z[2]*z[88];
    z[91]= - 9 - n<T>(107,12)*z[11];
    z[91]=z[91]*z[99];
    z[92]= - z[105] + z[92];
    z[92]=z[3]*z[92];
    z[96]= - 131*z[44] + n<T>(1057,5)*z[25];
    z[97]= - n<T>(139,30)*z[7] - n<T>(27,40)*z[3] + n<T>(67,40)*z[11] - z[4];
    z[97]=z[7]*z[97];
    z[87]=z[97] + z[92] - z[110] + z[91] + n<T>(341,60)*z[30] - n<T>(651,20)*
    z[23] - z[24] + n<T>(1,12)*z[96] + z[87];
    z[87]=z[7]*z[87];
    z[91]=n<T>(1,15)*z[9];
    z[92]=14*z[10] + n<T>(29,4)*z[9];
    z[92]=z[92]*z[91];
    z[96]=n<T>(1,2)*z[107];
    z[97]= - n<T>(1,5)*z[28] + 2*z[21];
    z[99]=7*z[10];
    z[100]=19*z[4] - z[99] + n<T>(29,6)*z[9];
    z[100]=z[4]*z[100];
    z[92]=n<T>(1,10)*z[100] - z[93] + z[92] - z[96] - n<T>(199,120)*z[30] + 2*
    z[97] + n<T>(29,5)*z[18];
    z[92]=z[4]*z[92];
    z[83]=z[104] - z[90] + z[98] - z[83];
    z[83]=z[30]*z[83];
    z[90]= - n<T>(131,4)*z[44] + z[43];
    z[93]= - z[9] - n<T>(11,2)*z[11];
    z[93]=z[11]*z[93];
    z[90]=n<T>(3,4)*z[93] - z[106] - n<T>(133,30)*z[30] + n<T>(1,3)*z[90] + z[103];
    z[90]=z[11]*z[90];
    z[93]= - n<T>(11,18)*z[3] + n<T>(5,6)*z[11] - z[4];
    z[93]=z[3]*z[93];
    z[93]=z[93] - z[110] + n<T>(2,3)*z[94] + n<T>(3,2)*z[95] + n<T>(367,180)*z[30]
    - n<T>(7,3)*z[26] + n<T>(1,3)*z[43] - z[20];
    z[93]=z[3]*z[93];
    z[94]= - z[99] - n<T>(107,6)*z[9];
    z[91]=z[94]*z[91];
    z[91]=z[91] + z[96] + n<T>(37,90)*z[30] - 3*z[26] - n<T>(29,15)*z[45] - 
    z[27];
    z[91]=z[9]*z[91];
    z[94]=z[56] + z[73];
    z[95]=z[54] + z[62];
    z[96]=z[51] + z[78];
    z[97]=z[46] - z[53];
    z[98]=z[82] - 3*z[81];
    z[99]= - n<T>(71,24)*z[107] + n<T>(133,18)*z[30] - n<T>(29,3)*z[45] - 2*z[28];
    z[99]=z[10]*z[99];

    r += n<T>(77,10)*z[22] - n<T>(49,5)*z[32] - n<T>(64,15)*z[33] - n<T>(353,180)*z[34]
       - n<T>(91,60)*z[37] - n<T>(59,30)*z[39] + n<T>(16,5)*z[40] + n<T>(68,5)*z[41]
     - 
      n<T>(2363,72)*z[42] - 2*z[47] + n<T>(7,4)*z[48] - n<T>(13,12)*z[49] + n<T>(293,60)
      *z[50] - n<T>(8,3)*z[52] - n<T>(7,60)*z[55] + n<T>(4,5)*z[57] - z[58] - n<T>(3,4)
      *z[59] + n<T>(29,20)*z[60] + n<T>(4,3)*z[61] + n<T>(989,120)*z[63] + n<T>(1163,80)
      *z[64] + n<T>(301,40)*z[65] + 46*z[66] + n<T>(59,20)*z[67] - n<T>(739,240)*
      z[68] - z[69] + n<T>(547,80)*z[70] - n<T>(13,4)*z[71] + n<T>(14,15)*z[72] - 
      n<T>(269,120)*z[74] + n<T>(21,10)*z[75] - n<T>(7,15)*z[76] - n<T>(29,60)*z[77]
     +  n<T>(19,20)*z[79] + z[80] + z[83] + z[84] + z[85] + z[86] + z[87] + 
      z[88] + z[89] + z[90] + z[91] + z[92] + z[93] - n<T>(1,12)*z[94] + n<T>(1,3)*z[95]
     - n<T>(1,2)*z[96]
     + 4*z[97]
     + n<T>(1,10)*z[98]
     + n<T>(1,5)*z[99];
 
    return r;
}

template std::complex<double> qg_2lha_tf805(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf805(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
