#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf843(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[111];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[3];
    z[22]=e[4];
    z[23]=e[5];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[10];
    z[28]=e[11];
    z[29]=e[12];
    z[30]=e[13];
    z[31]=c[3];
    z[32]=c[11];
    z[33]=c[15];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[29];
    z[41]=c[30];
    z[42]=c[31];
    z[43]=e[6];
    z[44]=e[14];
    z[45]=f[0];
    z[46]=f[1];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[10];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[14];
    z[56]=f[15];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[18];
    z[60]=f[23];
    z[61]=f[30];
    z[62]=f[31];
    z[63]=f[32];
    z[64]=f[33];
    z[65]=f[35];
    z[66]=f[36];
    z[67]=f[37];
    z[68]=f[39];
    z[69]=f[43];
    z[70]=f[50];
    z[71]=f[51];
    z[72]=f[52];
    z[73]=f[53];
    z[74]=f[54];
    z[75]=f[55];
    z[76]=f[56];
    z[77]=f[58];
    z[78]=f[74];
    z[79]=f[75];
    z[80]=n<T>(1,2)*z[5];
    z[81]=z[1]*i;
    z[82]=z[80] - z[81];
    z[83]=z[82]*z[5];
    z[84]=n<T>(1,8)*z[8];
    z[85]=n<T>(1,3)*z[81];
    z[86]= - n<T>(1,6)*z[8] + z[85] - 3*z[6];
    z[86]=z[86]*z[84];
    z[87]=z[81] - z[5];
    z[88]=n<T>(1,2)*z[3];
    z[89]= - z[88] - z[87];
    z[89]=z[3]*z[89];
    z[90]=n<T>(1,12)*z[8];
    z[91]=z[90] - 1 - n<T>(1,12)*z[81];
    z[91]=5*z[91] - n<T>(7,4)*z[7];
    z[91]=z[7]*z[91];
    z[92]=9*z[81];
    z[90]= - n<T>(23,6)*z[7] - n<T>(11,4)*z[9] + n<T>(35,3)*z[3] - z[90] - n<T>(9,4)*
    z[6] + z[92] - n<T>(107,12)*z[5];
    z[93]=z[4] - z[2];
    z[90]= - n<T>(7,12)*z[10] + n<T>(1,2)*z[90] + 5*z[93];
    z[93]=n<T>(1,2)*z[10];
    z[90]=z[90]*z[93];
    z[94]=n<T>(3,16)*z[6] + 5 + n<T>(3,8)*z[81];
    z[94]=z[6]*z[94];
    z[95]= - n<T>(1,2)*z[9] - z[87];
    z[95]=z[9]*z[95];
    z[96]=5*z[81];
    z[97]= - z[4]*z[96];
    z[86]=z[90] + z[97] + z[91] + n<T>(11,8)*z[95] + n<T>(37,12)*z[89] + z[86]
    - n<T>(107,24)*z[83] + z[94];
    z[86]=z[86]*z[93];
    z[89]=n<T>(1,4)*z[5];
    z[90]=z[81] + z[89];
    z[91]=z[7] - z[6];
    z[93]=z[9] - z[3];
    z[94]=n<T>(5,4)*z[8];
    z[90]= - n<T>(5,2)*z[2] + z[94] + 5*z[90] + n<T>(7,8)*z[93] + n<T>(31,16)*z[91];
    z[90]=z[2]*z[90];
    z[91]=5*z[5];
    z[95]=z[82]*z[91];
    z[80]=z[80] + z[81];
    z[97]=n<T>(5,2)*z[8] - 5*z[80] + n<T>(13,8)*z[6];
    z[97]=z[8]*z[97];
    z[98]=z[8] - z[6];
    z[99]=z[96] + 13*z[5];
    z[98]= - z[99] - 13*z[98];
    z[98]=n<T>(1,2)*z[98] + 11*z[7];
    z[98]=z[7]*z[98];
    z[99]= - 35*z[6] + z[99];
    z[99]=z[6]*z[99];
    z[100]=z[87] - z[8];
    z[101]= - n<T>(1,2)*z[100] - z[3];
    z[101]=z[3]*z[101];
    z[102]=z[9] + z[3] + z[100];
    z[102]=z[9]*z[102];
    z[90]=z[90] + n<T>(1,4)*z[98] + n<T>(7,4)*z[102] + n<T>(7,2)*z[101] + z[97] + 
    z[95] + n<T>(1,8)*z[99];
    z[90]=z[2]*z[90];
    z[95]=z[81]*z[4];
    z[95]=z[95] + z[31];
    z[97]= - z[11]*z[81];
    z[97]=z[97] - z[95];
    z[97]=z[13]*z[97];
    z[98]=z[81] - z[4];
    z[99]=z[11] - z[98];
    z[99]=z[29]*z[99];
    z[90]= - z[99] - z[90] - z[97] + z[75] - z[34];
    z[92]= - n<T>(9,2)*z[6] - z[92] + z[91];
    z[92]=z[6]*z[92];
    z[92]=n<T>(119,6)*z[83] + z[92];
    z[97]=n<T>(1,4)*z[8] + z[80];
    z[99]=n<T>(7,3)*z[8];
    z[97]=z[97]*z[99];
    z[101]=n<T>(5,4)*z[6];
    z[102]=n<T>(19,6)*z[3] - n<T>(7,6)*z[8] + n<T>(19,3)*z[87] - z[101];
    z[102]=z[3]*z[102];
    z[103]= - 151*z[81] + 119*z[5];
    z[103]=n<T>(1,6)*z[103] + z[6];
    z[103]= - n<T>(43,12)*z[3] + n<T>(1,4)*z[103] - z[99];
    z[103]=n<T>(1,2)*z[103] - n<T>(5,9)*z[9];
    z[103]=z[9]*z[103];
    z[92]=z[103] + z[102] + n<T>(1,4)*z[92] + z[97];
    z[92]=z[9]*z[92];
    z[97]=7*z[8];
    z[102]=n<T>(5,3)*z[5];
    z[103]=z[97] - n<T>(21,4)*z[6] - 7*z[81] - z[102];
    z[104]=n<T>(1,2)*z[8];
    z[103]=z[103]*z[104];
    z[105]= - z[82]*z[102];
    z[106]=n<T>(13,3)*z[5];
    z[107]=n<T>(25,12)*z[6] + n<T>(21,2)*z[81] - z[106];
    z[108]=n<T>(1,2)*z[6];
    z[107]=z[107]*z[108];
    z[103]=z[103] + z[105] + z[107];
    z[103]=z[103]*z[104];
    z[105]=n<T>(17,3)*z[81];
    z[107]= - z[105] - z[5];
    z[107]=z[5]*z[107];
    z[109]=5*z[87] + 7*z[6];
    z[109]=z[6]*z[109];
    z[107]=z[107] + z[109];
    z[109]=n<T>(5,2)*z[81];
    z[110]= - z[94] + z[109] - z[5];
    z[99]=z[110]*z[99];
    z[85]= - z[85] + z[89];
    z[89]=n<T>(1,4)*z[6];
    z[85]= - n<T>(43,18)*z[3] - n<T>(7,12)*z[8] + 13*z[85] - z[89];
    z[85]=z[3]*z[85];
    z[85]=z[85] + n<T>(1,2)*z[107] + z[99];
    z[85]=z[85]*z[88];
    z[99]= - n<T>(127,18)*z[6] + z[102] + 13 + n<T>(53,4)*z[81];
    z[89]=z[99]*z[89];
    z[99]= - z[105] + n<T>(7,4)*z[5];
    z[99]=z[5]*z[99];
    z[89]=z[99] + z[89];
    z[89]=z[6]*z[89];
    z[99]=z[87]*npow(z[5],2);
    z[85]=z[92] + z[85] + z[103] + n<T>(13,12)*z[99] + z[89];
    z[89]= - z[81] + z[88];
    z[89]=z[3]*z[89];
    z[92]=n<T>(1,2)*z[7];
    z[99]=z[92] - z[81] - z[3];
    z[99]=z[7]*z[99];
    z[89]= - z[99] + z[83] - z[89];
    z[99]= - z[8] + z[7] + z[3];
    z[102]=n<T>(2,3)*z[6];
    z[103]=n<T>(1,3)*z[9];
    z[105]=n<T>(13,3)*z[81] - n<T>(5,2)*z[5];
    z[99]= - n<T>(13,12)*z[4] + n<T>(5,4)*z[2] + z[103] + n<T>(1,4)*z[105] - z[102]
    + n<T>(5,8)*z[99];
    z[99]=z[4]*z[99];
    z[103]=z[103] - n<T>(2,3)*z[81] + z[108];
    z[103]=z[9]*z[103];
    z[105]=2*z[81] - z[6];
    z[102]=z[105]*z[102];
    z[105]=z[104] - z[81];
    z[107]=z[5] - z[105];
    z[94]=z[107]*z[94];
    z[107]= - z[2]*z[109];
    z[89]=z[99] + z[107] + z[103] + z[94] + z[102] - n<T>(5,4)*z[89];
    z[89]=z[4]*z[89];
    z[94]= - n<T>(35,8)*z[6] + z[96] - n<T>(23,8)*z[5];
    z[84]=n<T>(67,72)*z[7] + z[88] + n<T>(1,3)*z[94] + z[84];
    z[84]=z[84]*z[92];
    z[88]= - n<T>(1,12)*z[6] + n<T>(23,12)*z[5] - n<T>(13,4) - n<T>(7,3)*z[81];
    z[88]=z[6]*z[88];
    z[88]= - n<T>(23,12)*z[83] + z[88];
    z[94]=z[96] + z[106];
    z[94]=n<T>(2,3)*z[8] + n<T>(1,8)*z[94] - n<T>(1,3)*z[6];
    z[94]=z[8]*z[94];
    z[96]=n<T>(5,4)*z[81] + z[3];
    z[96]=z[3]*z[96];
    z[84]=z[84] + z[96] + n<T>(1,2)*z[88] + z[94];
    z[84]=z[7]*z[84];
    z[88]=5*z[6];
    z[91]=n<T>(13,6)*z[9] + 5*z[3] + z[88] + n<T>(43,3)*z[81] - z[91];
    z[91]=z[9]*z[91];
    z[94]=n<T>(71,2)*z[6] - 13*z[81] - 7*z[5];
    z[94]=z[6]*z[94];
    z[83]=7*z[83] + z[94];
    z[94]= - z[105]*z[97];
    z[83]=z[91] + n<T>(1,3)*z[83] + z[94];
    z[91]= - n<T>(3,2)*z[3] + n<T>(7,4)*z[8] - z[101] - 3*z[81] + n<T>(5,4)*z[5];
    z[91]=z[3]*z[91];
    z[80]=7*z[80] + n<T>(29,2)*z[6];
    z[80]=n<T>(1,3)*z[80] - n<T>(7,2)*z[8];
    z[80]= - n<T>(17,6)*z[11] + z[7] + n<T>(15,8)*z[9] + n<T>(1,4)*z[80] - z[3];
    z[80]=z[11]*z[80];
    z[80]=z[80] + z[91] + n<T>(1,4)*z[83];
    z[83]= - z[6] + z[92];
    z[83]=z[7]*z[83];
    z[88]=z[4] + z[88] - 7*z[9];
    z[88]=z[4]*z[88];
    z[80]=n<T>(1,6)*z[88] + z[83] + n<T>(1,2)*z[80];
    z[80]=z[11]*z[80];
    z[83]=n<T>(1,2)*z[2];
    z[82]= - z[83] - z[82];
    z[82]=z[19]*z[82];
    z[83]=z[83] - z[81];
    z[88]= - z[104] - z[83];
    z[88]=z[25]*z[88];
    z[82]=z[82] + z[88];
    z[88]=z[81]*z[5];
    z[88]=z[88] + z[31];
    z[91]=z[3]*z[81];
    z[91]=z[91] + z[88];
    z[91]=z[12]*z[91];
    z[92]=z[3] - z[87];
    z[92]=z[20]*z[92];
    z[91]= - z[65] + z[91] + z[92];
    z[92]=z[10]*z[81];
    z[92]=z[92] + z[95];
    z[92]=z[14]*z[92];
    z[94]= - z[10] + z[98];
    z[94]=z[22]*z[94];
    z[92]= - z[45] + z[92] + z[94];
    z[94]=z[6]*z[81];
    z[88]=z[94] + z[88];
    z[88]=z[15]*z[88];
    z[94]=z[87] - z[6];
    z[95]=z[23]*z[94];
    z[88]= - z[49] + z[88] + z[95];
    z[83]= - n<T>(1,2)*z[4] - z[83];
    z[83]=z[18]*z[83];
    z[83]=z[83] + z[73];
    z[95]=z[81]*z[8];
    z[95]=z[95] + z[31];
    z[96]= - z[9]*z[81];
    z[96]=z[96] - z[95];
    z[96]=z[17]*z[96];
    z[97]=z[81] - z[8];
    z[98]=z[9] - z[97];
    z[98]=z[28]*z[98];
    z[96]=z[96] + z[98];
    z[81]= - z[7]*z[81];
    z[81]=z[81] - z[95];
    z[81]=z[16]*z[81];
    z[95]= - z[7] + z[97];
    z[95]=z[26]*z[95];
    z[81]=z[81] + z[95];
    z[93]=z[94] - z[93];
    z[93]=z[30]*z[93];
    z[93]=z[93] + z[46];
    z[94]=z[2] - z[8];
    z[94]=n<T>(49,6)*z[7] + n<T>(5,3)*z[87] + n<T>(13,2)*z[6] - n<T>(13,3)*z[94];
    z[94]=z[24]*z[94];
    z[94]=z[94] - z[40];
    z[95]=z[10] + z[3];
    z[87]=z[9] + z[87] - n<T>(101,24)*z[95];
    z[87]=z[21]*z[87];
    z[95]= - z[9] - z[11];
    z[95]=z[44]*z[95];
    z[95]=z[42] + z[95] - z[64];
    z[97]=z[67] - z[58] + z[55] - z[47];
    z[98]=z[61] - z[60];
    z[99]=z[74] + z[59];
    z[101]= - n<T>(25,3)*z[38] - n<T>(5,3)*z[36] - n<T>(3,16)*z[32];
    z[101]=i*z[101];
    z[102]=n<T>(29,4)*z[5] + 23*z[6];
    z[102]= - n<T>(41,18)*z[3] + n<T>(5,9)*z[102] - n<T>(17,4)*z[8];
    z[102]= - n<T>(61,72)*z[7] + n<T>(1,2)*z[102] - n<T>(85,9)*z[9];
    z[102]=n<T>(1,12)*z[11] + n<T>(97,48)*z[10] + n<T>(3,4)*z[4] + n<T>(1,4)*z[102]
     - n<T>(10,9)*z[2];
    z[102]=z[31]*z[102];
    z[100]=z[100] + z[2];
    z[100]=n<T>(95,4)*z[9] + n<T>(67,4)*z[3] - 7*z[100];
    z[100]=z[27]*z[100];
    z[103]=z[7] + z[10];
    z[103]=z[43]*z[103];

    r +=  - n<T>(5,12)*z[33] + n<T>(77,72)*z[35] + n<T>(3,4)*z[37] + n<T>(11,3)*z[39]
       - z[41] + n<T>(175,96)*z[48] + n<T>(21,16)*z[50] + n<T>(14,3)*z[51] + n<T>(41,24)
      *z[52] - n<T>(5,24)*z[53] + n<T>(83,96)*z[54] + n<T>(21,8)*z[56] - n<T>(11,32)*
      z[57] - n<T>(187,96)*z[62] - n<T>(19,16)*z[63] + n<T>(1,96)*z[66] - n<T>(29,32)*
      z[68] + n<T>(31,24)*z[69] - n<T>(7,6)*z[70] - n<T>(2,3)*z[71] + n<T>(5,48)*z[72]
       - n<T>(7,16)*z[76] + n<T>(9,16)*z[77] - n<T>(3,8)*z[78] + n<T>(1,8)*z[79]
     +  z[80] + n<T>(47,24)*z[81] + n<T>(5,3)*z[82] + 5*z[83] + z[84] + n<T>(1,2)*
      z[85] + z[86] + z[87] + n<T>(13,12)*z[88] + z[89] - n<T>(1,3)*z[90] + n<T>(23,24)*z[91]
     + n<T>(5,2)*z[92]
     + n<T>(5,4)*z[93]
     + n<T>(1,4)*z[94]
     + n<T>(4,3)*z[95]
       + n<T>(7,4)*z[96] + n<T>(5,8)*z[97] + n<T>(7,12)*z[98] - n<T>(5,6)*z[99] + n<T>(1,6)
      *z[100] + z[101] + z[102] + n<T>(37,24)*z[103];
 
    return r;
}

template std::complex<double> qg_2lha_tf843(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf843(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
