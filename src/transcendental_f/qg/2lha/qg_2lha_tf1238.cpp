#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1238(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[26];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[7];
    z[5]=d[3];
    z[6]=e[0];
    z[7]=e[8];
    z[8]=c[3];
    z[9]=c[15];
    z[10]=c[18];
    z[11]=c[19];
    z[12]=c[25];
    z[13]=c[28];
    z[14]=c[29];
    z[15]=c[30];
    z[16]=c[31];
    z[17]=f[2];
    z[18]=f[3];
    z[19]=f[11];
    z[20]=f[13];
    z[21]=z[6] - z[7];
    z[22]=z[1]*i;
    z[22]=2*z[22];
    z[23]=z[22] + z[5];
    z[24]=z[4] - z[23];
    z[24]=z[4]*z[24];
    z[23]= - z[3] + z[23];
    z[23]=z[3]*z[23];
    z[25]=z[4] - z[3];
    z[25]=z[2]*z[25];
    z[23]=n<T>(1,2)*z[25] + z[23] + z[24] + z[21];
    z[23]=z[2]*z[23];
    z[21]= - z[21]*z[22];
    z[22]=z[22] + n<T>(1,2)*z[5];
    z[24]= - z[4] + z[22];
    z[24]=z[5]*z[24];
    z[25]=n<T>(1,3)*z[8];
    z[24]=z[25] - z[7] + z[24];
    z[24]=z[4]*z[24];
    z[22]=z[3] - z[22];
    z[22]=z[5]*z[22];
    z[22]= - z[25] + z[6] + z[22];
    z[22]=z[3]*z[22];
    z[25]=z[9] - z[12];

    r +=  - n<T>(4,3)*z[10] + n<T>(1,6)*z[11] + 2*z[13] + z[14] + 4*z[15] - n<T>(49,6)*z[16]
     - 3*z[17]
     + z[18] - z[19]
     + z[20]
     + z[21]
     + z[22]
     +  z[23] + z[24] - n<T>(1,3)*z[25];
 
    return r;
}

template std::complex<double> qg_2lha_tf1238(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1238(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
