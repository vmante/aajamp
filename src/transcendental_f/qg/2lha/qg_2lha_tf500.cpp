#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf500(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[31];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[7];
    z[6]=d[17];
    z[7]=e[7];
    z[8]=e[9];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=e[6];
    z[13]=f[30];
    z[14]=f[31];
    z[15]=f[33];
    z[16]=f[36];
    z[17]=f[39];
    z[18]=n<T>(1,2)*z[4];
    z[19]=i*z[1];
    z[20]=z[5] + z[19] - z[3];
    z[20]=z[20]*z[18];
    z[21]=3*z[12];
    z[22]=3*z[7];
    z[23]=z[19]*z[6];
    z[24]=npow(z[3],2);
    z[25]=z[19] - z[5];
    z[26]= - z[5]*z[25];
    z[20]=z[20] + z[26] + n<T>(1,2)*z[24] - z[23] + z[22] + n<T>(1,3)*z[9] + 
    z[21] - z[8];
    z[18]=z[20]*z[18];
    z[20]=n<T>(1,2)*z[3];
    z[24]= - 5*z[19] - z[20];
    z[26]=n<T>(1,4)*z[3];
    z[24]=z[24]*z[26];
    z[26]=n<T>(1,4)*z[5];
    z[27]= - n<T>(3,2)*z[5] + 3*z[19] + 5*z[3];
    z[27]=z[27]*z[26];
    z[28]=n<T>(1,2)*z[5];
    z[29]= - z[28] + n<T>(1,2)*z[19] - z[3];
    z[29]=z[4]*z[29];
    z[30]=3*z[3];
    z[25]= - z[30] + z[25];
    z[25]=z[2] + n<T>(1,2)*z[25] + z[4];
    z[25]=z[2]*z[25];
    z[21]=z[21] + z[9];
    z[21]=n<T>(3,4)*z[25] + z[29] + z[27] + z[24] + n<T>(1,2)*z[21] + z[7];
    z[21]=z[2]*z[21];
    z[24]=z[19] - n<T>(5,2)*z[3];
    z[24]=z[24]*z[20];
    z[25]=z[7] + z[8];
    z[26]= - z[3]*z[26];
    z[23]=z[26] + z[24] - z[23] - n<T>(1,12)*z[9] - z[25];
    z[23]=z[23]*z[28];
    z[19]=n<T>(5,2)*z[19] + z[30];
    z[19]=z[19]*z[20];
    z[24]=n<T>(1,2)*z[9];
    z[19]=z[19] + z[24] + z[22];
    z[19]=z[19]*z[20];
    z[20]=n<T>(1,2)*i;
    z[22]=z[1]*z[25];
    z[22]= - n<T>(1,12)*z[10] + z[22];
    z[20]=z[22]*z[20];
    z[22]=z[11] - z[13];
    z[25]= - 3*z[17] + z[16];
    z[24]= - z[6]*z[24];

    r +=  - n<T>(1,8)*z[14] - z[15] + z[18] + z[19] + z[20] + z[21] + n<T>(1,2)
      *z[22] + z[23] + z[24] + n<T>(3,8)*z[25];
 
    return r;
}

template std::complex<double> qg_2lha_tf500(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf500(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
