#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1396(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[75];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[4];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[16];
    z[8]=d[1];
    z[9]=d[3];
    z[10]=d[15];
    z[11]=d[18];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[3];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=e[10];
    z[19]=e[11];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[17];
    z[24]=c[18];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[29];
    z[31]=c[30];
    z[32]=c[31];
    z[33]=f[0];
    z[34]=f[1];
    z[35]=f[3];
    z[36]=f[4];
    z[37]=f[5];
    z[38]=f[6];
    z[39]=f[7];
    z[40]=f[8];
    z[41]=f[9];
    z[42]=f[10];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[13];
    z[46]=f[14];
    z[47]=f[15];
    z[48]=f[16];
    z[49]=f[17];
    z[50]=f[18];
    z[51]=f[23];
    z[52]=f[28];
    z[53]=f[29];
    z[54]=f[76];
    z[55]=n<T>(15,16)*z[5];
    z[56]=n<T>(1,2)*z[8];
    z[57]=z[1]*i;
    z[58]= - n<T>(9,2)*z[3] + z[55] + n<T>(11,4)*z[4] - n<T>(39,4)*z[2] + z[56] - 
    z[57] + n<T>(141,16)*z[9];
    z[58]=z[3]*z[58];
    z[59]=n<T>(1,2)*z[9];
    z[60]=z[59] - z[57];
    z[61]=z[60]*z[9];
    z[62]=3*z[8];
    z[63]=n<T>(5,4)*z[8] - z[57] - n<T>(5,2)*z[9];
    z[63]=z[63]*z[62];
    z[64]=n<T>(11,4)*z[2] + z[62] + 7*z[57] - n<T>(11,2)*z[9];
    z[64]=z[2]*z[64];
    z[58]=z[58] + n<T>(5,2)*z[64] + n<T>(125,8)*z[61] + z[63];
    z[63]=z[2] - z[9];
    z[64]=n<T>(1,4)*z[8];
    z[63]=n<T>(1,2)*z[4] - z[64] - z[57] - n<T>(1,4)*z[63];
    z[63]=z[4]*z[63];
    z[65]=n<T>(1,2)*z[5];
    z[66]=z[65] - z[57] - z[9];
    z[55]=z[66]*z[55];
    z[55]=z[55] + 11*z[63] + n<T>(1,2)*z[58];
    z[55]=z[3]*z[55];
    z[58]=z[57] - z[9];
    z[56]=z[58] + z[56];
    z[62]=z[56]*z[62];
    z[62]=n<T>(35,8)*z[61] + z[62];
    z[63]=z[8] - z[9];
    z[66]=n<T>(1,2)*z[2];
    z[67]=z[57] + z[63] + z[66];
    z[68]=z[2]*z[67];
    z[69]= - n<T>(19,2)*z[58] - 15*z[8];
    z[69]=3*z[69] + 23*z[2];
    z[69]=n<T>(1,8)*z[69] - n<T>(1,3)*z[6];
    z[69]=z[6]*z[69];
    z[62]=z[69] + 3*z[62] - n<T>(5,4)*z[68];
    z[68]=n<T>(1,2)*z[6];
    z[62]=z[62]*z[68];
    z[69]=9*z[9];
    z[70]=29*z[57] - z[69];
    z[59]=z[70]*z[59];
    z[70]= - 41*z[57] + z[69];
    z[70]=n<T>(1,4)*z[70] + n<T>(23,3)*z[8];
    z[70]=z[8]*z[70];
    z[59]=z[59] + z[70];
    z[59]=z[59]*z[64];
    z[64]=13*z[8];
    z[70]= - 111*z[57] + 17*z[9];
    z[70]=n<T>(31,2)*z[2] + n<T>(1,2)*z[70] - z[64];
    z[70]=z[2]*z[70];
    z[71]=z[9] - n<T>(19,2)*z[8];
    z[71]=z[8]*z[71];
    z[70]=z[71] + z[70];
    z[71]=npow(z[9],2);
    z[70]= - z[71] + n<T>(1,8)*z[70];
    z[70]=z[2]*z[70];
    z[71]=n<T>(73,4)*z[2];
    z[72]=5*z[9];
    z[73]= - z[72] + n<T>(31,2)*z[8];
    z[73]=n<T>(1,3)*z[73] - z[71];
    z[73]= - n<T>(191,48)*z[3] + n<T>(71,32)*z[5] + n<T>(85,24)*z[4] + n<T>(1,4)*z[73]
    - n<T>(13,3)*z[6];
    z[73]=z[20]*z[73];
    z[63]=z[63] + z[2] + n<T>(29,4)*z[3] + n<T>(33,4)*z[4];
    z[74]=z[57]*z[63];
    z[74]=n<T>(29,4)*z[20] + z[74];
    z[74]=z[7]*z[74];
    z[63]=n<T>(29,4)*z[57] - z[63];
    z[63]=z[16]*z[63];
    z[55]=z[63] + z[55] + z[62] + z[59] + z[70] + z[73] + z[74];
    z[59]=n<T>(5,2)*z[8];
    z[62]= - z[59] + 13*z[57] + z[72];
    z[62]=z[8]*z[62];
    z[62]= - n<T>(93,2)*z[61] + z[62];
    z[63]= - z[72] + 5*z[8];
    z[70]=5*z[2];
    z[72]= - n<T>(23,2)*z[6] + z[70] + 23*z[57] + z[63];
    z[72]=z[72]*z[68];
    z[69]= - n<T>(13,2)*z[57] + z[69];
    z[59]= - n<T>(27,2)*z[2] + 3*z[69] - z[59];
    z[59]=z[2]*z[59];
    z[69]=5*z[5];
    z[73]=z[57] - n<T>(31,4)*z[9];
    z[73]=z[69] - n<T>(5,2)*z[6] + n<T>(39,2)*z[2] + 3*z[73] - n<T>(13,2)*z[8];
    z[65]=z[73]*z[65];
    z[59]=z[65] + z[72] + n<T>(1,2)*z[62] + z[59];
    z[59]=z[5]*z[59];
    z[59]=z[59] + z[51];
    z[62]=z[4] + z[9];
    z[62]=n<T>(125,4)*z[3] + z[71] - z[64] - n<T>(125,2)*z[57] + 13*z[62];
    z[62]=z[12]*z[62];
    z[64]=z[69] + n<T>(57,4)*z[6] - z[70] - 5*z[58] + n<T>(37,4)*z[8];
    z[64]=z[18]*z[64];
    z[62]=z[62] + z[64];
    z[56]=z[8]*z[56];
    z[56]= - n<T>(23,2)*z[61] - 17*z[56];
    z[61]=11*z[2];
    z[64]=z[67]*z[61];
    z[65]=z[68] + z[58];
    z[65]=z[6]*z[65];
    z[56]=n<T>(33,4)*z[65] + n<T>(3,2)*z[56] + z[64];
    z[64]=n<T>(7,4)*z[58] - z[8];
    z[61]=n<T>(33,16)*z[6] + n<T>(3,4)*z[64] - z[61];
    z[61]=n<T>(1,8)*z[61] - n<T>(2,3)*z[4];
    z[61]=z[4]*z[61];
    z[56]=n<T>(1,16)*z[56] + z[61];
    z[56]=z[4]*z[56];
    z[61]=z[5] + z[6];
    z[64]= - z[57]*z[61];
    z[64]= - z[20] + z[64];
    z[64]=z[11]*z[64];
    z[61]= - z[57] + z[61];
    z[61]=z[19]*z[61];
    z[61]=z[64] + z[61];
    z[64]=z[9] + z[8];
    z[64]=z[57]*z[64];
    z[64]=z[20] + z[64];
    z[64]=z[10]*z[64];
    z[65]=z[8] - z[58];
    z[65]=z[14]*z[65];
    z[64]=z[64] + z[65];
    z[57]= - n<T>(31,2)*z[5] - 5*z[6] - n<T>(21,2)*z[2] + 31*z[57] + z[63];
    z[57]=z[17]*z[57];
    z[63]=z[4] + z[8];
    z[58]=z[6] + z[58] - n<T>(13,8)*z[63];
    z[58]=z[15]*z[58];
    z[60]=z[66] + z[60];
    z[60]=z[13]*z[60];
    z[63]=z[49] - z[48];
    z[65]=z[52] + z[46];
    z[66]=n<T>(43,16)*z[28] + n<T>(111,64)*z[26] - n<T>(1,4)*z[23] - n<T>(1,128)*z[21];
    z[66]=i*z[66];

    r +=  - n<T>(61,32)*z[22] - n<T>(1,2)*z[24] + n<T>(179,192)*z[25] - n<T>(109,192)*
      z[27] - n<T>(37,32)*z[29] + n<T>(3,8)*z[30] - z[31] + n<T>(53,24)*z[32] + n<T>(75,16)*z[33]
     + n<T>(59,32)*z[34]
     + n<T>(161,128)*z[35]
     + n<T>(49,128)*z[36]
     + n<T>(27,32)*z[37] - n<T>(15,64)*z[38]
     + n<T>(5,2)*z[39] - n<T>(15,4)*z[40]
     + n<T>(51,64)
      *z[41] + n<T>(5,32)*z[42] - n<T>(117,128)*z[43] - n<T>(3,128)*z[44] - n<T>(25,32)
      *z[45] + n<T>(63,32)*z[47] - n<T>(29,48)*z[50] + n<T>(3,32)*z[53] + z[54] + n<T>(1,4)*z[55]
     + z[56]
     + n<T>(3,16)*z[57]
     + n<T>(3,4)*z[58]
     + n<T>(1,16)*z[59]
     +  n<T>(11,16)*z[60] + n<T>(9,16)*z[61] + n<T>(1,8)*z[62] - n<T>(33,128)*z[63]
     + n<T>(13,32)*z[64] - n<T>(9,32)*z[65]
     + z[66];
 
    return r;
}

template std::complex<double> qg_2lha_tf1396(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1396(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
