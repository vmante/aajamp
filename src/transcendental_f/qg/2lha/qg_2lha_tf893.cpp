#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf893(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[124];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[70];
    z[3]=f[77];
    z[4]=f[79];
    z[5]=f[81];
    z[6]=c[11];
    z[7]=d[0];
    z[8]=d[1];
    z[9]=d[3];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=d[6];
    z[13]=d[7];
    z[14]=d[8];
    z[15]=d[9];
    z[16]=c[12];
    z[17]=c[13];
    z[18]=c[20];
    z[19]=c[23];
    z[20]=c[32];
    z[21]=c[33];
    z[22]=c[35];
    z[23]=c[36];
    z[24]=c[37];
    z[25]=c[38];
    z[26]=c[39];
    z[27]=c[40];
    z[28]=c[43];
    z[29]=c[44];
    z[30]=c[47];
    z[31]=c[48];
    z[32]=c[49];
    z[33]=c[50];
    z[34]=c[55];
    z[35]=c[56];
    z[36]=c[57];
    z[37]=c[59];
    z[38]=c[81];
    z[39]=c[84];
    z[40]=f[24];
    z[41]=f[44];
    z[42]=f[69];
    z[43]=f[80];
    z[44]=f[22];
    z[45]=f[42];
    z[46]=f[62];
    z[47]=d[2];
    z[48]=g[24];
    z[49]=g[27];
    z[50]=g[38];
    z[51]=g[40];
    z[52]=g[48];
    z[53]=g[50];
    z[54]=g[79];
    z[55]=g[82];
    z[56]=g[90];
    z[57]=g[92];
    z[58]=g[95];
    z[59]=g[99];
    z[60]=g[101];
    z[61]=g[104];
    z[62]=g[110];
    z[63]=g[112];
    z[64]=g[113];
    z[65]=g[131];
    z[66]=g[138];
    z[67]=g[147];
    z[68]=g[170];
    z[69]=g[173];
    z[70]=g[181];
    z[71]=g[183];
    z[72]=g[189];
    z[73]=g[191];
    z[74]=g[198];
    z[75]=g[200];
    z[76]=g[201];
    z[77]=g[212];
    z[78]=g[214];
    z[79]=g[220];
    z[80]=g[243];
    z[81]=g[245];
    z[82]=g[251];
    z[83]=g[254];
    z[84]=g[270];
    z[85]=g[272];
    z[86]=g[276];
    z[87]=g[278];
    z[88]=g[281];
    z[89]=g[288];
    z[90]=g[289];
    z[91]=g[290];
    z[92]=g[292];
    z[93]=g[294];
    z[94]=g[308];
    z[95]=g[312];
    z[96]=g[339];
    z[97]=g[342];
    z[98]=g[345];
    z[99]=g[347];
    z[100]=g[348];
    z[101]= - 4*z[18] + n<T>(1,5)*z[19];
    z[102]=n<T>(1,3)*z[101] - n<T>(16,5)*z[17];
    z[102]=z[10]*z[102];
    z[102]=z[102] + z[35];
    z[103]=z[3] - z[5];
    z[103]=3*z[4] - 4*z[2] + n<T>(4,3)*z[103];
    z[103]=z[1]*z[103];
    z[104]= - n<T>(4,15)*z[37] - 8*z[39] + n<T>(65,9)*z[38];
    z[101]=z[101] - n<T>(48,5)*z[17];
    z[105]=z[15]*z[101];
    z[106]=2*z[13];
    z[107]=n<T>(73,45)*z[15] - n<T>(4,5)*z[10] + n<T>(4702253,36288) + z[106];
    z[107]=z[6]*z[107];
    z[108]= - n<T>(11,135)*z[6] - z[101];
    z[108]=z[9]*z[108];
    z[109]= - 2*z[18] + n<T>(1,10)*z[19];
    z[110]= - n<T>(91,270)*z[6] + n<T>(24,5)*z[17] - z[109];
    z[110]=z[14]*z[110];
    z[101]=n<T>(73,405)*z[6] + z[101];
    z[101]=z[7]*z[101];
    z[111]= - z[18] + n<T>(1,20)*z[19];
    z[111]= - n<T>(4,5)*z[17] + n<T>(1,3)*z[111];
    z[112]= - 19*z[111] - n<T>(689,1620)*z[6];
    z[112]=z[12]*z[112];
    z[111]= - 17*z[111] + n<T>(293,1620)*z[6];
    z[111]=z[11]*z[111];
    z[109]=n<T>(1,3)*z[109] - n<T>(8,5)*z[17];
    z[109]=7*z[109] + n<T>(137,810)*z[6];
    z[109]=z[8]*z[109];
    z[101]=z[109] + z[111] + z[112] + z[101] + z[110] + z[108] + n<T>(1,9)*
    z[107] + z[105] - n<T>(150515,252)*z[17] - n<T>(752575,3024)*z[18] + n<T>(150515,12096)*z[19]
     - n<T>(59,54)*z[21]
     - 16*z[24]
     + n<T>(295,27)*z[25]
     + n<T>(109,243)
   *z[26] - n<T>(20,3)*z[29] - n<T>(208,405)*z[30] + n<T>(48,5)*z[32] + 8*z[33] + n<T>(1,3)*z[104]
     - z[36]
     + z[103]
     + 4*z[102];
    z[101]=i*z[101];
    z[102]=z[16] - z[2];
    z[103]=n<T>(2,3)*z[5];
    z[104]=n<T>(7,3)*z[40];
    z[105]=z[42] - z[43];
    z[107]=n<T>(2,3)*z[4];
    z[108]=n<T>(2,3)*z[3];
    z[102]= - z[107] + z[108] - z[104] - n<T>(1,3)*z[41] - z[103] + z[105]
    - n<T>(4,3)*z[102];
    z[102]=z[7]*z[102];
    z[109]=z[40] + z[2];
    z[110]=n<T>(1,3)*z[3];
    z[111]=z[43] + z[5];
    z[107]=z[107] - n<T>(2,3)*z[16] - z[110] + n<T>(1,3)*z[111] + z[109];
    z[107]=z[9]*z[107];
    z[107]= - z[91] + z[107] + z[76];
    z[111]=2*z[5];
    z[112]=z[45] + z[44];
    z[113]= - z[112] + 2*z[46];
    z[114]= - z[111] - z[43] + z[113];
    z[115]=2*z[2];
    z[104]=n<T>(7,3)*z[4] + n<T>(53,9)*z[16] + z[104] - n<T>(1,6)*z[41] + n<T>(1,3)*
    z[114] - z[115];
    z[104]=z[12]*z[104];
    z[113]=z[113] - z[42];
    z[111]=13*z[4] + 22*z[16] - z[41] - z[111] - 2*z[43] + z[113];
    z[111]=z[14]*z[111];
    z[111]=z[111] + z[81];
    z[105]=z[105] - z[40];
    z[114]=z[41] - z[105];
    z[116]=n<T>(1,3)*z[47];
    z[114]=z[116]*z[114];
    z[116]=z[47] + z[13];
    z[103]=z[116]*z[103];
    z[106]= - z[106] + n<T>(761647,2016) - 2*z[47];
    z[106]=z[106]*z[110];
    z[110]=n<T>(4,3)*z[10];
    z[109]=z[41] - z[109];
    z[109]=z[109]*z[110];
    z[112]= - n<T>(7,2)*z[41] + 2*z[42] + z[43] + 8*z[46] + z[112];
    z[112]= - n<T>(11,3)*z[4] - n<T>(89,9)*z[16] + z[108] + n<T>(1,3)*z[112] - z[40]
   ;
    z[112]=z[11]*z[112];
    z[113]= - z[115] - z[113];
    z[108]= - n<T>(4,3)*z[4] - n<T>(4,9)*z[16] + z[108] - n<T>(2,3)*z[40] + n<T>(1,3)*
    z[113] + z[41];
    z[108]=z[8]*z[108];
    z[105]= - 2*z[16] - z[41] - 5*z[46] - z[105];
    z[105]=z[15]*z[105];
    z[105]= - z[88] + z[53] + z[54] + z[55] + z[72] + z[105] - z[48] + 
    z[50] - z[51];
    z[113]= - z[63] - z[65] + z[69] + z[99];
    z[115]= - z[68] - z[71] + z[84] + z[87];
    z[116]= - z[62] - z[74] + z[89] + z[98];
    z[117]= - z[77] - z[79] + z[92] + z[93];
    z[118]=z[94] - z[95];
    z[119]=z[61] - z[97];
    z[120]=z[59] + z[60];
    z[121]=z[58] - z[96];
    z[122]=z[2] - z[42];
    z[122]=n<T>(2,3)*z[122];
    z[122]=z[13]*z[122];
    z[123]=n<T>(64,9)*z[10] - n<T>(766183,4536) - 6*z[13];
    z[123]=z[16]*z[123];
    z[110]=z[110] - n<T>(2,3)*z[47] - 3*z[13];
    z[110]=z[4]*z[110];

    r += n<T>(86,135)*z[20] - n<T>(48,5)*z[22] - 12*z[23] + 10*z[27] + 3*z[28]
       - n<T>(8,3)*z[31] + n<T>(1,5)*z[34] + n<T>(5,2)*z[45] - n<T>(767695,6048)*z[46]
       + n<T>(14,3)*z[49] + n<T>(2,9)*z[52] + z[56] + n<T>(1,6)*z[57] + z[64] - n<T>(10,3)*z[66]
     + n<T>(10,9)*z[67] - n<T>(11,2)*z[70] - n<T>(17,9)*z[73]
     + z[75] - 
      n<T>(16,3)*z[78] + 6*z[80] - n<T>(13,3)*z[82] - z[83] - n<T>(43,6)*z[85] + n<T>(19,6)*z[86]
     - z[90]
     - z[100]
     + z[101]
     + z[102]
     + z[103]
     + z[104]
     +  n<T>(2,3)*z[105] + z[106] + 2*z[107] + z[108] + z[109] + z[110] + n<T>(1,3)*z[111]
     + z[112] - n<T>(3,2)*z[113]
     + z[114] - n<T>(5,6)*z[115] - n<T>(1,2)
      *z[116] - n<T>(4,3)*z[117] - 4*z[118] - n<T>(3,8)*z[119] + n<T>(4,9)*z[120]
       - n<T>(1,24)*z[121] + z[122] + z[123];
 
    return r;
}

template std::complex<double> qg_2lha_tf893(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf893(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
