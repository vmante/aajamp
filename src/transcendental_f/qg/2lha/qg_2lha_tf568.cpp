#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf568(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[91];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[4];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[4];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[21];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[30];
    z[33]=c[31];
    z[34]=e[6];
    z[35]=e[13];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[5];
    z[41]=f[9];
    z[42]=f[11];
    z[43]=f[13];
    z[44]=f[17];
    z[45]=f[18];
    z[46]=f[30];
    z[47]=f[31];
    z[48]=f[32];
    z[49]=f[33];
    z[50]=f[34];
    z[51]=f[36];
    z[52]=f[37];
    z[53]=f[38];
    z[54]=f[39];
    z[55]=f[41];
    z[56]=f[43];
    z[57]=f[48];
    z[58]=f[50];
    z[59]=f[51];
    z[60]=f[53];
    z[61]=f[55];
    z[62]=f[56];
    z[63]=f[58];
    z[64]=f[60];
    z[65]=f[67];
    z[66]=n<T>(2,3)*z[6];
    z[67]=n<T>(27,4)*z[8];
    z[68]=z[1]*i;
    z[69]=3*z[68];
    z[70]= - z[69] - 5*z[3];
    z[70]=z[67] + n<T>(9,2)*z[70] - z[66];
    z[70]=z[8]*z[70];
    z[71]=5*z[68];
    z[72]=2*z[6];
    z[73]=2*z[2];
    z[74]=z[7] - z[73] + 2*z[8] - z[71] - z[72];
    z[74]=z[7]*z[74];
    z[75]=n<T>(1,3)*z[6];
    z[71]=z[71] + z[3];
    z[71]=3*z[71] + z[75];
    z[71]= - n<T>(15,2)*z[2] + 2*z[71] + 9*z[8];
    z[71]=z[2]*z[71];
    z[67]= - n<T>(2,3)*z[4] + n<T>(5,6)*z[7] - 15*z[2] + z[67] + n<T>(33,4)*z[3]
     - 
    z[75];
    z[67]=z[4]*z[67];
    z[76]= - z[68] + n<T>(1,2)*z[3];
    z[76]=z[76]*z[3];
    z[77]=z[68] - z[6];
    z[78]=z[6]*z[77];
    z[67]=z[67] + n<T>(1,3)*z[74] + z[71] + z[70] + n<T>(33,2)*z[76] - n<T>(4,3)*
    z[78];
    z[67]=z[4]*z[67];
    z[70]=n<T>(21,4)*z[68];
    z[71]=2*z[3];
    z[74]=n<T>(19,4)*z[6];
    z[78]= - n<T>(21,8)*z[8] + z[74] + z[70] + z[71];
    z[78]=z[8]*z[78];
    z[79]=n<T>(1,2)*z[68];
    z[80]= - 5*z[7] + z[73] + n<T>(1,2)*z[8] - z[79] - z[71];
    z[80]=z[7]*z[80];
    z[81]=n<T>(1,2)*z[6];
    z[82]= - z[68] - z[81];
    z[82]=z[82]*z[74];
    z[70]=n<T>(65,6)*z[5] + n<T>(1,2)*z[7] + z[2] - n<T>(21,4)*z[8] - z[74] + z[70]
    - z[3];
    z[70]=z[5]*z[70];
    z[74]=z[68] + z[3];
    z[74]=z[3]*z[74];
    z[83]=z[68] - z[3];
    z[84]= - z[8] + z[83];
    z[84]=2*z[84] + z[2];
    z[84]=z[2]*z[84];
    z[70]=n<T>(1,2)*z[70] + z[80] + z[84] + z[78] + z[74] + z[82];
    z[70]=z[5]*z[70];
    z[71]= - n<T>(5,6)*z[8] + z[79] - z[71];
    z[71]=z[8]*z[71];
    z[74]=z[75] + z[3];
    z[78]=n<T>(2,3)*z[8] + n<T>(7,3)*z[68] + z[74];
    z[78]=2*z[78] - n<T>(11,3)*z[2];
    z[78]=z[2]*z[78];
    z[79]=npow(z[6],2);
    z[80]=n<T>(65,18)*z[7] - n<T>(1,3)*z[2] - n<T>(1,4)*z[8] + n<T>(13,12)*z[6] - n<T>(23,12)
   *z[68] + z[3];
    z[80]=z[7]*z[80];
    z[71]=z[80] + z[78] + z[71] - 5*z[76] + n<T>(23,12)*z[79];
    z[71]=z[7]*z[71];
    z[78]= - z[68] - z[72];
    z[66]=z[78]*z[66];
    z[74]=17*z[68] + z[74];
    z[74]=2*z[74] - 17*z[8];
    z[74]=z[8]*z[74];
    z[75]=n<T>(68,3)*z[2] - n<T>(11,2)*z[8] - z[75] - 51*z[68] - 4*z[3];
    z[75]=z[2]*z[75];
    z[66]=z[75] + z[74] - 37*z[76] + z[66];
    z[66]=z[2]*z[66];
    z[74]=2*z[83];
    z[75]= - z[74] - z[6];
    z[72]=z[75]*z[72];
    z[75]=2*z[68];
    z[78]=z[75] - n<T>(19,6)*z[3];
    z[78]=z[3]*z[78];
    z[79]= - z[68] + n<T>(1,2)*z[4];
    z[79]=z[4]*z[79];
    z[74]= - n<T>(10,3)*z[10] + z[74] + n<T>(7,2)*z[6];
    z[74]=z[10]*z[74];
    z[72]=z[74] + z[79] + z[78] + z[72];
    z[72]=z[10]*z[72];
    z[74]=z[7] + z[8];
    z[78]=z[68]*z[74];
    z[78]=z[23] + z[78];
    z[78]=z[14]*z[78];
    z[74]= - z[68] + z[74];
    z[74]=z[21]*z[74];
    z[74]=z[78] + z[74];
    z[78]=z[81] + z[83];
    z[79]=19*z[6];
    z[78]=z[78]*z[79];
    z[78]=n<T>(31,3)*z[76] + z[78];
    z[79]= - n<T>(31,3)*z[83] + z[79];
    z[79]= - n<T>(13,3)*z[9] + n<T>(1,4)*z[79] - n<T>(8,3)*z[10];
    z[79]=z[9]*z[79];
    z[80]= - n<T>(13,3)*z[83] + n<T>(5,2)*z[10];
    z[80]=z[10]*z[80];
    z[78]=z[79] + n<T>(1,2)*z[78] + z[80];
    z[78]=z[9]*z[78];
    z[79]=z[68]*z[3];
    z[79]=z[79] + z[23];
    z[80]= - z[5]*z[68];
    z[80]=z[80] - z[79];
    z[80]=z[12]*z[80];
    z[82]=z[5] - z[83];
    z[82]=z[17]*z[82];
    z[80]= - z[48] + z[80] + z[82];
    z[82]=z[4] + z[6];
    z[82]=z[68]*z[82];
    z[82]=z[23] + z[82];
    z[82]=z[13]*z[82];
    z[77]= - z[4] + z[77];
    z[77]=z[18]*z[77];
    z[77]=z[55] + z[82] + z[77];
    z[82]=z[10]*z[68];
    z[79]=z[82] + z[79];
    z[79]=z[11]*z[79];
    z[82]= - z[10] + z[83];
    z[82]=z[22]*z[82];
    z[79]=z[79] + z[82];
    z[82]= - n<T>(229,9)*z[6] - n<T>(3,2)*z[68] + 11*z[3];
    z[82]=z[82]*z[81];
    z[82]=11*z[76] + z[82];
    z[82]=z[82]*z[81];
    z[83]= - n<T>(83,3)*z[68] - n<T>(19,2)*z[6];
    z[81]=z[83]*z[81];
    z[76]=41*z[76] + z[81];
    z[69]=z[8] + n<T>(83,24)*z[6] - z[69] + n<T>(41,4)*z[3];
    z[69]=z[8]*z[69];
    z[69]=n<T>(1,2)*z[76] + z[69];
    z[69]=z[8]*z[69];
    z[76]= - n<T>(341,12)*z[8] - 23*z[3] - n<T>(85,2)*z[6];
    z[73]= - n<T>(127,36)*z[9] + n<T>(23,36)*z[10] + n<T>(10,3)*z[5] - 2*z[4] + n<T>(109,36)*z[7]
     + n<T>(1,6)*z[76] - z[73];
    z[73]=z[23]*z[73];
    z[76]=z[8] + z[5] - z[7];
    z[81]= - z[75] + z[3];
    z[76]=25*z[2] + 29*z[81] + 4*z[76];
    z[76]=z[15]*z[76];
    z[81]=z[4] - z[2];
    z[83]=z[68] - z[8];
    z[83]= - 5*z[6] + 31*z[83];
    z[81]= - n<T>(13,2)*z[7] + n<T>(1,2)*z[83] + 4*z[81];
    z[81]=z[19]*z[81];
    z[83]= - z[41] + z[62] - z[46];
    z[84]=z[52] - z[43];
    z[75]=z[75] - z[2];
    z[85]=z[8] - z[75];
    z[85]=z[20]*z[85];
    z[85]=z[85] + z[37];
    z[75]=z[4] - z[75];
    z[75]=z[16]*z[75];
    z[75]=z[75] + z[53];
    z[86]=128*z[30] + 52*z[28] - n<T>(121,72)*z[24];
    z[86]=i*z[86];
    z[68]=npow(z[3],2)*z[68];
    z[87]=z[7] + z[5];
    z[87]=z[34]*z[87];
    z[88]= - z[6] - z[10];
    z[88]=z[35]*z[88];
    z[89]= - z[10] - z[9];
    z[89]=z[36]*z[89];
    z[90]=2*i;
    z[90]= - z[27]*z[90];

    r +=  - n<T>(9,2)*z[25] + z[26] - n<T>(73,3)*z[29] - 69*z[31] - 20*z[32] + 
      n<T>(149,9)*z[33] + z[38] + n<T>(31,4)*z[39] + 9*z[40] + n<T>(9,4)*z[42] + n<T>(49,4)*z[44]
     + z[45]
     + n<T>(67,24)*z[47]
     + n<T>(35,3)*z[49]
     + z[50]
     + n<T>(13,8)
      *z[51] - n<T>(23,8)*z[54] + n<T>(2,3)*z[56] - z[57] + n<T>(7,3)*z[58] + n<T>(11,4)
      *z[59] + 4*z[60] - n<T>(19,12)*z[61] + n<T>(25,4)*z[63] - z[64] + z[65]
       + z[66] + z[67] - n<T>(4,3)*z[68] + z[69] + z[70] + z[71] + z[72] + 
      z[73] + n<T>(7,6)*z[74] + 15*z[75] + z[76] + 2*z[77] + z[78] + n<T>(13,3)
      *z[79] + 3*z[80] + n<T>(1,3)*z[81] + z[82] + n<T>(1,2)*z[83] - 6*z[84] + 
      19*z[85] + z[86] + n<T>(19,2)*z[87] + 11*z[88] + n<T>(28,3)*z[89] + z[90]
      ;
 
    return r;
}

template std::complex<double> qg_2lha_tf568(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf568(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
