#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1122(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[19];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[4];
    z[4]=d[5];
    z[5]=d[12];
    z[6]=d[16];
    z[7]=c[3];
    z[8]=d[3];
    z[9]=d[2];
    z[10]=d[9];
    z[11]=e[3];
    z[12]=e[4];
    z[13]=e[5];
    z[14]=z[3] - z[4];
    z[15]=z[8] - z[10];
    z[16]=z[15] + z[9];
    z[16]= - z[16]*z[14];
    z[17]=2*i;
    z[17]=z[17]*z[14];
    z[18]= - z[2] - z[6] + 2*z[5];
    z[18]=i*z[18];
    z[17]=z[17] + z[18];
    z[17]=z[1]*z[17];
    z[18]= - z[10] + n<T>(1,2)*z[9];
    z[18]=z[9]*z[18];
    z[15]= - z[8]*z[15];
    z[14]= - n<T>(1,2)*z[2] + z[8] + z[14];
    z[14]=z[2]*z[14];

    r += n<T>(1,6)*z[7] - z[11] + z[12] - 2*z[13] + z[14] + z[15] + z[16]
       + z[17] + z[18];
 
    return r;
}

template std::complex<double> qg_2lha_tf1122(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1122(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
