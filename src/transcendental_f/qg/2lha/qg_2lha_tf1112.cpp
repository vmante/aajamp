#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf1112(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[21];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[12];
    z[6]=d[3];
    z[7]=d[6];
    z[8]=d[9];
    z[9]=d[7];
    z[10]=e[5];
    z[11]=e[10];
    z[12]=e[13];
    z[13]=z[8] - z[7];
    z[14]=z[4] - z[3];
    z[15]=z[14] + z[6];
    z[16]= - z[15]*z[13];
    z[17]=n<T>(1,2)*z[2];
    z[15]= - z[17] + z[15];
    z[15]=z[2]*z[15];
    z[13]= - z[14] + z[13];
    z[13]=z[9]*z[13];
    z[18]=npow(z[3],2);
    z[19]=z[4]*z[3];
    z[20]= - n<T>(1,2)*z[6] - z[14];
    z[20]=z[6]*z[20];
    z[13]= - z[10] - z[11] + z[13] + z[15] + z[20] - z[18] + z[19] + 
    z[16];
    z[14]=n<T>(1,2)*z[5] - z[17] + z[14];
    z[14]=z[14]*i*z[1];

    r +=  - z[12] + n<T>(1,2)*z[13] + z[14];
 
    return r;
}

template std::complex<double> qg_2lha_tf1112(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf1112(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
