#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf975(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[126];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[18];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[17];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[21];
    z[37]=c[23];
    z[38]=c[25];
    z[39]=c[26];
    z[40]=c[28];
    z[41]=c[29];
    z[42]=c[30];
    z[43]=c[31];
    z[44]=e[3];
    z[45]=e[6];
    z[46]=e[14];
    z[47]=f[0];
    z[48]=f[1];
    z[49]=f[2];
    z[50]=f[3];
    z[51]=f[4];
    z[52]=f[5];
    z[53]=f[6];
    z[54]=f[7];
    z[55]=f[8];
    z[56]=f[9];
    z[57]=f[11];
    z[58]=f[12];
    z[59]=f[13];
    z[60]=f[14];
    z[61]=f[16];
    z[62]=f[17];
    z[63]=f[18];
    z[64]=f[21];
    z[65]=f[23];
    z[66]=f[30];
    z[67]=f[31];
    z[68]=f[32];
    z[69]=f[33];
    z[70]=f[34];
    z[71]=f[35];
    z[72]=f[36];
    z[73]=f[37];
    z[74]=f[39];
    z[75]=f[41];
    z[76]=f[43];
    z[77]=f[50];
    z[78]=f[51];
    z[79]=f[52];
    z[80]=f[53];
    z[81]=f[55];
    z[82]=f[56];
    z[83]=f[58];
    z[84]=f[60];
    z[85]=f[78];
    z[86]=f[83];
    z[87]=f[84];
    z[88]=f[100];
    z[89]=f[101];
    z[90]=n<T>(401,24)*z[8] + 7*z[7];
    z[91]=n<T>(5,2)*z[10];
    z[92]=n<T>(1,4)*z[9];
    z[93]=n<T>(1,2)*z[12] + n<T>(5,3)*z[16];
    z[93]= - n<T>(247,12)*z[4] + 11*z[93] + n<T>(1,6)*z[11];
    z[93]= - n<T>(7,6)*z[5] + z[92] + n<T>(79,4)*z[2] - n<T>(35,6)*z[6] + z[91] + n<T>(17,3)*z[3]
     + n<T>(1,2)*z[93] - z[90];
    z[93]=z[5]*z[93];
    z[94]=n<T>(11,4)*z[14];
    z[95]=2*z[13];
    z[96]=n<T>(1,2)*z[3];
    z[97]=n<T>(5,8)*z[10] - z[96] - z[94] + z[95];
    z[97]=z[10]*z[97];
    z[98]=n<T>(1,4)*z[4];
    z[99]=z[11] - z[15];
    z[99]= - n<T>(97,4)*z[4] - 11*z[14] - n<T>(25,2)*z[99];
    z[99]=z[99]*z[98];
    z[100]=n<T>(11,8)*z[12];
    z[95]= - n<T>(23,48)*z[3] + n<T>(11,8)*z[4] - n<T>(31,24)*z[11] + z[100] - z[95]
   ;
    z[95]=z[3]*z[95];
    z[101]=n<T>(33,4)*z[4];
    z[102]=31*z[11];
    z[103]=z[101] + n<T>(91,6)*z[16] - z[102];
    z[104]=n<T>(1,4)*z[10];
    z[103]=n<T>(511,48)*z[6] - z[104] + n<T>(1,2)*z[103] - z[3];
    z[103]=z[6]*z[103];
    z[105]=n<T>(9,2)*z[10];
    z[106]=n<T>(5,2)*z[3];
    z[107]= - z[106] + n<T>(377,8)*z[4] + 31*z[7];
    z[107]= - n<T>(225,8)*z[2] - n<T>(23,6)*z[6] + n<T>(1,3)*z[107] + z[105];
    z[107]=z[2]*z[107];
    z[108]=4*z[7];
    z[109]=n<T>(11,4)*z[4];
    z[110]=z[109] + n<T>(5,2)*z[11] - 5*z[16] + n<T>(33,2)*z[13];
    z[110]= - n<T>(79,16)*z[9] + n<T>(143,24)*z[2] - n<T>(27,8)*z[6] + n<T>(5,4)*z[10]
    + n<T>(13,8)*z[3] + n<T>(1,2)*z[110] - z[108];
    z[110]=z[9]*z[110];
    z[111]= - n<T>(169,16)*z[4] + n<T>(11,3)*z[11] + n<T>(227,12)*z[2] + n<T>(125,6)*
    z[6];
    z[112]= - n<T>(45,8)*z[13] + n<T>(43,3)*z[17];
    z[113]= - n<T>(37,8)*z[8] - n<T>(35,6)*z[9] - n<T>(7,2)*z[10] + n<T>(17,24)*z[3]
     - 
   n<T>(61,6)*z[7] - z[112] + z[111];
    z[113]=z[8]*z[113];
    z[114]=9*z[19];
    z[115]=z[114] + n<T>(199,12)*z[18] + n<T>(353,12)*z[24];
    z[116]= - n<T>(11,3)*z[23] + n<T>(45,8)*z[27];
    z[117]=n<T>(11,12)*z[26];
    z[118]=z[117] - z[29];
    z[119]=n<T>(25,8)*z[21];
    z[120]=25*z[15] + n<T>(89,6)*z[11];
    z[120]=z[11]*z[120];
    z[121]= - 43*z[17] + n<T>(71,2)*z[11];
    z[121]=n<T>(35,12)*z[7] + n<T>(1,3)*z[121] - n<T>(9,2)*z[4];
    z[121]=z[7]*z[121];
    z[93]=n<T>(1,2)*z[93] + z[113] + z[110] + z[107] + z[103] + z[97] + 
    z[95] + z[121] + z[99] + n<T>(1,8)*z[120] - n<T>(33,8)*z[16] + n<T>(55,12)*z[22]
    - n<T>(11,8)*z[20] + z[119] - n<T>(11,4)*z[28] + n<T>(43,3)*z[25] - z[118] + 
    z[116] - z[115];
    z[93]=z[1]*z[93];
    z[95]= - 15*z[36] + 103*z[39] + n<T>(105,2)*z[37];
    z[95]=n<T>(1,4)*z[95] + z[33];
    z[95]=5*z[95] - n<T>(67,24)*z[31];
    z[93]=n<T>(1,2)*z[95] + z[93];
    z[93]=i*z[93];
    z[95]=n<T>(3,2)*z[10];
    z[97]=n<T>(247,6)*z[4] + 33 - n<T>(1,3)*z[11];
    z[90]=n<T>(35,4)*z[9] - 23*z[2] - n<T>(65,6)*z[6] + z[95] - n<T>(23,3)*z[3] + n<T>(1,4)*z[97]
     + z[90];
    z[90]=n<T>(1,4)*z[90] - n<T>(5,3)*z[5];
    z[90]=z[5]*z[90];
    z[97]=n<T>(1,2)*z[10];
    z[99]= - n<T>(15,4)*z[10] + z[7] + z[106];
    z[99]=z[99]*z[97];
    z[103]=n<T>(133,24)*z[6] - z[104] - z[108] + z[3];
    z[103]=z[6]*z[103];
    z[106]=5*z[11];
    z[107]=n<T>(39,4)*z[9] - n<T>(35,6)*z[2] - z[6] - z[10] - z[106] - n<T>(13,2)*
    z[3];
    z[92]=z[107]*z[92];
    z[107]=n<T>(14,3)*z[23];
    z[108]=npow(z[7],2);
    z[110]=npow(z[11],2);
    z[113]=npow(z[4],2);
    z[114]= - n<T>(55,6)*z[22] + n<T>(11,4)*z[20] + z[114];
    z[120]=z[102] - n<T>(37,2)*z[3];
    z[120]=z[3]*z[120];
    z[109]=z[109] - z[7];
    z[109]=7*z[109] - n<T>(11,4)*z[3];
    z[109]= - 4*z[2] + n<T>(37,12)*z[6] + n<T>(1,3)*z[109] - 2*z[10];
    z[109]=z[2]*z[109];
    z[121]=n<T>(7,3)*z[6];
    z[122]= - z[121] + n<T>(13,24)*z[3] + n<T>(7,3)*z[7];
    z[123]=n<T>(401,96)*z[8] - n<T>(13,24)*z[9] + n<T>(77,24)*z[2] - n<T>(185,16)*z[4]
    + z[122];
    z[123]=z[8]*z[123];
    z[90]=z[90] + z[123] + z[92] + z[109] + z[103] + z[99] + n<T>(1,24)*
    z[120] + n<T>(21,4)*z[108] + n<T>(247,96)*z[113] - n<T>(1,48)*z[110] - n<T>(371,96)*
    z[30] + z[107] + n<T>(1,2)*z[114] + z[118];
    z[90]=z[5]*z[90];
    z[92]=n<T>(1,2)*z[7];
    z[99]=n<T>(35,24)*z[3];
    z[103]=n<T>(1,2)*z[6];
    z[109]=n<T>(215,48)*z[9] - n<T>(11,24)*z[2] - z[103] + n<T>(3,4)*z[10] - z[99]
    + z[4] + z[92];
    z[109]=z[9]*z[109];
    z[114]=z[10] - z[4];
    z[92]= - n<T>(3,4)*z[3] - z[92] + z[114];
    z[92]=z[10]*z[92];
    z[118]=n<T>(5,3)*z[7];
    z[120]= - n<T>(103,12)*z[6] + z[97] + n<T>(31,2)*z[11] + z[118];
    z[120]=z[6]*z[120];
    z[95]= - n<T>(29,12)*z[9] + z[95] + n<T>(31,24)*z[3] + n<T>(49,2)*z[7] - z[111];
    z[95]=n<T>(1,2)*z[95] - n<T>(1,3)*z[8];
    z[95]=z[8]*z[95];
    z[111]=npow(z[3],2);
    z[123]=43*z[25];
    z[124]= - z[123] + n<T>(353,8)*z[24];
    z[125]= - 71*z[11] + n<T>(61,2)*z[7];
    z[125]=z[7]*z[125];
    z[122]= - n<T>(101,48)*z[2] + z[10] + z[4] - z[122];
    z[122]=z[2]*z[122];
    z[92]=z[95] + z[109] + z[122] + z[120] + z[92] + n<T>(5,48)*z[111] + n<T>(1,6)*z[125]
     + n<T>(169,32)*z[113] - n<T>(11,6)*z[110] - n<T>(2599,288)*z[30]
     + 
    z[117] + n<T>(1,3)*z[124] - z[116];
    z[92]=z[8]*z[92];
    z[95]=n<T>(11,2)*z[4];
    z[91]= - z[91] + n<T>(3,2)*z[3] - z[95] + z[7];
    z[91]=z[91]*z[97];
    z[96]= - n<T>(139,24)*z[6] + n<T>(35,6)*z[10] + z[96] + z[101] - z[7];
    z[96]=z[96]*z[103];
    z[97]= - n<T>(73,48)*z[2] + z[99] + n<T>(7,2)*z[7] + z[114];
    z[97]=z[2]*z[97];
    z[95]=z[106] - z[95];
    z[95]= - n<T>(31,144)*z[9] - n<T>(31,24)*z[2] - n<T>(25,48)*z[6] - n<T>(11,24)*z[10]
    - n<T>(25,16)*z[3] + n<T>(1,8)*z[95] - 2*z[7];
    z[95]=z[9]*z[95];
    z[99]=n<T>(11,16)*z[113];
    z[106]=n<T>(13,3)*z[46];
    z[109]=z[106] - 33*z[27];
    z[109]=n<T>(1,2)*z[109] + 5*z[22];
    z[91]=z[95] + z[97] + z[96] + z[91] + n<T>(5,16)*z[111] - n<T>(5,4)*z[108]
    - z[99] + n<T>(5,8)*z[110] + n<T>(281,144)*z[30] + z[23] + n<T>(15,4)*z[26] + n<T>(1,2)*z[109]
     - 4*z[29];
    z[91]=z[9]*z[91];
    z[95]=n<T>(55,8)*z[3] - n<T>(89,4)*z[4] - 17*z[7];
    z[95]=n<T>(607,24)*z[2] - n<T>(19,12)*z[6] + n<T>(1,3)*z[95] - z[105];
    z[95]=z[2]*z[95];
    z[95]=z[95] - n<T>(11,6)*z[26] + z[115];
    z[96]=z[7] - n<T>(19,8)*z[6];
    z[96]=z[96]*z[121];
    z[97]=z[4] + z[104];
    z[97]=z[10]*z[97];
    z[95]=z[96] + z[97] - n<T>(11,48)*z[111] + n<T>(13,12)*z[108] - n<T>(377,48)*
    z[113] - n<T>(25,16)*z[110] - n<T>(55,12)*z[30] - z[107] + n<T>(1,2)*z[95];
    z[95]=z[2]*z[95];
    z[96]=2*z[27];
    z[97]= - n<T>(1,3)*z[44] + n<T>(11,2)*z[20];
    z[104]= - z[102] + n<T>(121,3)*z[3];
    z[104]=z[3]*z[104];
    z[97]=n<T>(1,48)*z[104] - z[99] - n<T>(29,48)*z[110] + n<T>(97,144)*z[30] + n<T>(17,6)*z[26]
     + z[29]
     + n<T>(1,4)*z[97]
     + z[96];
    z[97]=z[3]*z[97];
    z[99]=11*z[28];
    z[104]=z[106] + z[99];
    z[105]=n<T>(11,4)*z[7] - n<T>(10,3)*z[10];
    z[105]=z[10]*z[105];
    z[96]=z[105] + n<T>(11,4)*z[108] + n<T>(11,8)*z[113] - n<T>(1,9)*z[30] - z[23]
    - n<T>(7,6)*z[29] + n<T>(1,4)*z[104] - z[96];
    z[96]=z[10]*z[96];
    z[104]= - n<T>(7,2)*z[22] - z[29];
    z[104]= - n<T>(33,8)*z[113] - n<T>(31,2)*z[110] + n<T>(685,18)*z[30] + n<T>(13,3)*
    z[104] + 67*z[23];
    z[101]=n<T>(109,6)*z[10] - z[118] - z[101] - n<T>(33,4) - z[102];
    z[101]=n<T>(1,2)*z[101] + n<T>(245,9)*z[6];
    z[101]=z[101]*z[103];
    z[102]=n<T>(29,8)*z[10] - 6*z[7] - n<T>(5,4)*z[3];
    z[102]=z[10]*z[102];
    z[101]=z[101] + z[102] + z[111] + n<T>(1,2)*z[104] - n<T>(35,3)*z[108];
    z[101]=z[6]*z[101];
    z[94]=n<T>(55,12)*z[16] + z[100] - z[94] + n<T>(25,8)*z[15] - z[112];
    z[94]=z[30]*z[94];
    z[100]=22*z[45];
    z[102]=z[100] - n<T>(1,4)*z[44];
    z[102]=n<T>(21,8)*z[110] + n<T>(787,288)*z[30] + n<T>(1,3)*z[102] - z[119];
    z[102]=z[11]*z[102];
    z[99]=n<T>(91,12)*z[113] + n<T>(25,4)*z[110] - n<T>(2053,72)*z[30] + n<T>(199,6)*
    z[18] + z[99] - n<T>(25,2)*z[21];
    z[98]=z[99]*z[98];
    z[99]=n<T>(71,4)*z[110] - n<T>(263,48)*z[30] + n<T>(179,2)*z[23] + z[100] - 
    z[123];
    z[100]=9*z[11] - n<T>(55,9)*z[7];
    z[100]=z[7]*z[100];
    z[99]=n<T>(1,4)*z[100] + n<T>(1,3)*z[99] + n<T>(9,4)*z[113];
    z[99]=z[7]*z[99];
    z[100]=z[64] - z[79];
    z[103]= - z[82] - z[89] + 3*z[88];
    z[103]=n<T>(11,8)*z[81] - n<T>(31,24)*z[83] + 3*z[84] + n<T>(21,2)*z[85] + z[86]
    + z[87] + n<T>(1,2)*z[103];

    r += n<T>(33,8)*z[22] + n<T>(197,48)*z[32] + n<T>(35,12)*z[34] + n<T>(11,32)*z[35]
       - n<T>(107,8)*z[38] - n<T>(255,8)*z[40] - n<T>(35,16)*z[41] - n<T>(35,4)*z[42]
       - n<T>(12049,288)*z[43] - n<T>(25,8)*z[47] + n<T>(25,16)*z[48] + n<T>(31,24)*
      z[49] + n<T>(247,96)*z[50] + n<T>(67,48)*z[51] + n<T>(89,24)*z[52] + n<T>(13,16)*
      z[53] + n<T>(11,3)*z[54] - n<T>(3,2)*z[55] - n<T>(7,4)*z[56] + n<T>(401,96)*z[57]
       + n<T>(1,48)*z[58] + n<T>(59,12)*z[59] + n<T>(11,16)*z[60] + n<T>(5,8)*z[61]
     +  n<T>(185,32)*z[62] - n<T>(25,24)*z[63] - n<T>(11,24)*z[65] - n<T>(71,6)*z[66]
     - 
      n<T>(25,2)*z[67] - n<T>(115,12)*z[68] - n<T>(44,3)*z[69] + n<T>(7,8)*z[70]
     - n<T>(7,2)
      *z[71] + n<T>(11,6)*z[72] - n<T>(9,4)*z[73] - n<T>(31,4)*z[74] + n<T>(9,2)*z[75]
       + n<T>(10,3)*z[76] - n<T>(15,4)*z[77] - n<T>(33,16)*z[78] + 14*z[80] + z[90]
       + z[91] + z[92] + z[93] + z[94] + z[95] + z[96] + z[97] + z[98]
       + z[99] - n<T>(11,8)*z[100] + z[101] + z[102] + n<T>(1,2)*z[103];
 
    return r;
}

template std::complex<double> qg_2lha_tf975(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf975(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
