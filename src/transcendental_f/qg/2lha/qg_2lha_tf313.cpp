#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf313(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[115];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[18];
    z[13]=d[15];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[17];
    z[34]=c[19];
    z[35]=c[21];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[31];
    z[41]=e[3];
    z[42]=e[6];
    z[43]=e[14];
    z[44]=f[0];
    z[45]=f[1];
    z[46]=f[2];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[8];
    z[53]=f[9];
    z[54]=f[10];
    z[55]=f[11];
    z[56]=f[12];
    z[57]=f[13];
    z[58]=f[14];
    z[59]=f[15];
    z[60]=f[16];
    z[61]=f[17];
    z[62]=f[18];
    z[63]=f[21];
    z[64]=f[23];
    z[65]=f[30];
    z[66]=f[31];
    z[67]=f[32];
    z[68]=f[33];
    z[69]=f[34];
    z[70]=f[36];
    z[71]=f[37];
    z[72]=f[39];
    z[73]=f[41];
    z[74]=f[43];
    z[75]=f[50];
    z[76]=f[51];
    z[77]=f[52];
    z[78]=f[53];
    z[79]=f[54];
    z[80]=f[55];
    z[81]=f[56];
    z[82]=f[58];
    z[83]=f[74];
    z[84]=f[75];
    z[85]=f[78];
    z[86]=f[83];
    z[87]=z[4] - z[2];
    z[88]=z[87] + z[9];
    z[89]=2*z[8];
    z[90]=n<T>(1,2)*z[3];
    z[91]=z[1]*i;
    z[92]= - 58*z[91] + n<T>(235,4)*z[5];
    z[88]= - n<T>(25,12)*z[11] + n<T>(3,4)*z[10] + z[89] - n<T>(235,12)*z[7] + n<T>(1,3)
   *z[92] - z[90] - n<T>(1,4)*z[88];
    z[88]=z[11]*z[88];
    z[92]=n<T>(3,2)*z[3];
    z[93]=n<T>(1,2)*z[2];
    z[94]= - 29*z[91] + 28*z[5];
    z[94]= - n<T>(52,3)*z[7] + z[93] + n<T>(4,3)*z[94] - z[92];
    z[94]=z[7]*z[94];
    z[95]=n<T>(3,2)*z[5];
    z[96]= - n<T>(41,6)*z[10] + n<T>(73,6)*z[4] + n<T>(3,2)*z[7] + z[92] - n<T>(32,3)*
    z[91] - z[95];
    z[96]=z[10]*z[96];
    z[97]=2*z[91];
    z[95]= - z[3] - z[97] + z[95];
    z[95]=z[3]*z[95];
    z[98]=z[91] - z[5];
    z[99]=z[98] + z[93];
    z[100]=z[99]*z[93];
    z[101]=n<T>(1,2)*z[9];
    z[102]=z[101] - z[91];
    z[103]=z[3] - z[102];
    z[103]=z[103]*z[101];
    z[104]=z[2] - z[5];
    z[105]= - 13*z[4] + n<T>(5,3)*z[7] + z[91] - z[104];
    z[106]=n<T>(1,2)*z[4];
    z[105]=z[105]*z[106];
    z[107]=2*z[98];
    z[108]=z[107] + z[8];
    z[89]=z[108]*z[89];
    z[108]=z[97] - z[5];
    z[108]=z[5]*z[108];
    z[88]=z[88] + z[96] + z[89] + z[105] + z[103] + z[94] + z[100] + n<T>(50,3)*z[108]
     + z[95];
    z[88]=z[11]*z[88];
    z[89]=n<T>(1,6)*z[5];
    z[94]=253*z[91] - n<T>(259,2)*z[5];
    z[94]=z[94]*z[89];
    z[95]=z[90] - n<T>(7,2)*z[5] + n<T>(5,3) + n<T>(9,2)*z[91];
    z[95]=z[3]*z[95];
    z[96]=n<T>(1,3)*z[2];
    z[100]= - 73*z[91] + 76*z[5];
    z[100]=2*z[100] - n<T>(155,2)*z[2];
    z[100]=z[100]*z[96];
    z[103]=n<T>(2345,24)*z[7] - n<T>(149,3)*z[2] + z[92] + n<T>(289,6)*z[5] + n<T>(13,12)
    + z[91];
    z[103]=z[7]*z[103];
    z[105]=n<T>(5,4)*z[4] - 2*z[3] - n<T>(5,2)*z[91] - z[104];
    z[105]=z[4]*z[105];
    z[108]=n<T>(547,4)*z[91] - 152*z[5];
    z[108]= - 96*z[9] + n<T>(1,2)*z[7] + n<T>(152,3)*z[2] + n<T>(1,3)*z[108] + z[3];
    z[108]=z[9]*z[108];
    z[109]= - n<T>(1037,18)*z[8] + z[4] - n<T>(565,24)*z[9] + n<T>(2875,24)*z[7]
     - 
   n<T>(611,12)*z[2] + n<T>(7,4)*z[3] - n<T>(397,8)*z[91] - n<T>(70,3)*z[5];
    z[109]=z[8]*z[109];
    z[94]=z[109] + z[105] + z[108] + z[103] + z[100] + z[94] + z[95];
    z[94]=z[8]*z[94];
    z[92]=z[92] + n<T>(31,2)*z[91] - z[5];
    z[92]=z[3]*z[92];
    z[95]=n<T>(227,12)*z[7] - n<T>(215,6)*z[91] + z[104];
    z[95]=z[7]*z[95];
    z[100]= - z[5] + z[102];
    z[102]=n<T>(75,2)*z[9];
    z[100]=z[100]*z[102];
    z[102]= - n<T>(217,6)*z[4] + z[102] + n<T>(215,6)*z[7] - n<T>(355,6)*z[2] - n<T>(31,2)*z[3]
     + n<T>(163,2)*z[91]
     + n<T>(65,3)*z[5];
    z[102]=z[102]*z[106];
    z[103]= - 65*z[91] + n<T>(53,2)*z[5];
    z[103]=z[5]*z[103];
    z[105]=355*z[91] + 119*z[5];
    z[105]= - n<T>(119,12)*z[2] + n<T>(1,6)*z[105] + z[3];
    z[105]=z[2]*z[105];
    z[92]=z[102] + z[100] + z[95] + z[105] + n<T>(1,3)*z[103] + z[92];
    z[92]=z[92]*z[106];
    z[95]=3*z[5];
    z[100]=3*z[3];
    z[102]=z[95] - z[100];
    z[103]=n<T>(547,8)*z[7];
    z[105]=z[103] + n<T>(559,4)*z[91] - z[102];
    z[105]=z[7]*z[105];
    z[108]= - z[91] + n<T>(1,2)*z[5];
    z[109]=51*z[5];
    z[110]= - z[108]*z[109];
    z[111]=n<T>(59,2)*z[3] + 67*z[91] - 63*z[5];
    z[111]=z[3]*z[111];
    z[110]=z[110] + z[111];
    z[99]= - z[3] - z[99];
    z[99]=z[2]*z[99];
    z[95]= - 3*z[8] + n<T>(559,6)*z[9] - z[3] - n<T>(577,6)*z[91] + z[95];
    z[95]=z[8]*z[95];
    z[95]=z[95] + z[105] + n<T>(1,2)*z[110] + z[99];
    z[99]=z[104] + z[3];
    z[105]= - z[106] + n<T>(25,2)*z[91] + z[99];
    z[105]=z[105]*z[106];
    z[109]= - n<T>(565,3)*z[8] + n<T>(523,6)*z[9] + n<T>(551,2)*z[7] + 169*z[3] - 
   n<T>(217,6)*z[91] - z[109] - 25*z[87];
    z[109]= - n<T>(469,6)*z[6] + n<T>(1,2)*z[109] - 59*z[10];
    z[109]=z[6]*z[109];
    z[103]=n<T>(523,48)*z[9] - z[103] - n<T>(523,24)*z[91] - z[3];
    z[103]=z[9]*z[103];
    z[110]= - 59*z[91] + 57*z[5];
    z[110]= - n<T>(59,4)*z[10] - z[8] + z[9] + n<T>(1,2)*z[110] + z[3];
    z[110]=z[10]*z[110];
    z[95]=n<T>(1,4)*z[109] + z[110] + z[105] + z[103] + n<T>(1,2)*z[95];
    z[95]=z[6]*z[95];
    z[103]=701*z[91] - 317*z[5];
    z[89]=z[103]*z[89];
    z[103]=n<T>(203,2)*z[2] + 47*z[91] - n<T>(199,2)*z[5];
    z[103]=z[103]*z[96];
    z[105]= - 34*z[98] - n<T>(1,4)*z[3];
    z[105]=z[3]*z[105];
    z[109]= - n<T>(443,2)*z[5] - 146 - n<T>(2923,16)*z[91];
    z[109]= - n<T>(9775,72)*z[7] + n<T>(274,3)*z[2] + n<T>(1,3)*z[109] - n<T>(69,4)*z[3]
   ;
    z[109]=z[7]*z[109];
    z[89]=z[109] + z[103] + z[89] + z[105];
    z[89]=z[7]*z[89];
    z[103]=13*z[5];
    z[101]=13*z[2] - 35*z[91] - z[103] - z[101] + z[3];
    z[101]=z[9]*z[101];
    z[103]= - n<T>(25,2)*z[2] - 13*z[3] - z[91] + z[103];
    z[103]=z[2]*z[103];
    z[105]=z[5]*z[108];
    z[108]=n<T>(199,2)*z[3] + 223*z[91] - 211*z[5];
    z[108]=z[3]*z[108];
    z[101]=z[101] + z[103] - 131*z[105] + z[108];
    z[102]=n<T>(73,4)*z[7] + n<T>(73,2)*z[91] + z[102];
    z[102]=z[7]*z[102];
    z[103]=n<T>(91,6)*z[4] - n<T>(91,3)*z[91] - 79*z[7];
    z[103]=z[103]*z[106];
    z[101]=z[103] + z[102] + n<T>(1,6)*z[101];
    z[102]=z[3] - z[9] + z[2];
    z[102]=z[8]*z[102];
    z[103]=5*z[91];
    z[105]= - n<T>(35,8)*z[2] + n<T>(485,8)*z[3] + z[103] - n<T>(131,8)*z[5];
    z[105]= - n<T>(293,36)*z[10] + n<T>(91,24)*z[4] + n<T>(35,24)*z[9] + n<T>(1,3)*
    z[105] + n<T>(85,8)*z[7];
    z[105]=z[10]*z[105];
    z[101]=z[105] + n<T>(1,2)*z[101] + z[102];
    z[101]=z[10]*z[101];
    z[102]=3*z[10];
    z[87]= - z[102] - 2*z[7] + z[107] + z[100] + z[87];
    z[87]=z[29]*z[87];
    z[105]=187 + n<T>(169,3)*z[7];
    z[107]= - z[91]*z[105];
    z[108]=z[91]*z[5];
    z[108]=z[108] + z[30];
    z[107]=z[107] - n<T>(577,3)*z[108];
    z[107]=z[16]*z[107];
    z[105]=z[105] - n<T>(577,3)*z[98];
    z[105]=z[22]*z[105];
    z[109]=n<T>(305,3)*z[5] + n<T>(187,2) - n<T>(335,3)*z[91];
    z[109]=z[109]*npow(z[5],2);
    z[105]= - z[105] - z[109] - z[107] + z[86] - z[84] + z[83] - z[73];
    z[107]=z[8] + z[9];
    z[109]=z[91]*z[107];
    z[109]=z[30] + z[109];
    z[109]=z[17]*z[109];
    z[107]= - z[91] + z[107];
    z[107]=z[25]*z[107];
    z[107]=z[109] + z[107];
    z[109]= - z[90] - 19*z[91] + 7*z[5];
    z[109]=z[3]*z[109];
    z[110]= - n<T>(41,2)*z[2] - n<T>(7,3)*z[3] + 105*z[91] + 41*z[5];
    z[110]=z[110]*z[93];
    z[111]= - 15*z[91] + n<T>(17,2)*z[5];
    z[111]=z[5]*z[111];
    z[109]=z[110] + z[111] + n<T>(1,6)*z[109];
    z[104]= - n<T>(215,8)*z[91] - 8*z[104];
    z[104]=n<T>(19,3)*z[104] + n<T>(543,16)*z[7];
    z[104]=z[7]*z[104];
    z[110]= - n<T>(3,4)*z[9] + n<T>(4085,48)*z[7] - n<T>(107,8)*z[2] + n<T>(13,24)*z[3]
    - n<T>(1,4)*z[91] + 4*z[5];
    z[110]=z[9]*z[110];
    z[104]=z[110] + n<T>(1,2)*z[109] + z[104];
    z[104]=z[9]*z[104];
    z[93]=z[106] - z[91] + z[93];
    z[93]=z[18]*z[93];
    z[106]=z[91]*z[4];
    z[106]=z[106] + z[30];
    z[109]= - z[6]*z[91];
    z[109]=z[109] - z[106];
    z[109]=z[15]*z[109];
    z[110]=z[91] - z[4];
    z[111]=z[6] - z[110];
    z[111]=z[21]*z[111];
    z[109]=z[109] + z[111];
    z[111]=z[11]*z[91];
    z[106]=z[111] + z[106];
    z[106]=z[14]*z[106];
    z[110]= - z[11] + z[110];
    z[110]=z[28]*z[110];
    z[106]=z[106] + z[110];
    z[110]= - z[3]*z[91];
    z[108]=z[110] - z[108];
    z[108]=z[13]*z[108];
    z[110]= - z[3] + z[98];
    z[110]=z[20]*z[110];
    z[108]=z[108] + z[110];
    z[110]= - n<T>(1027,18)*z[3] + n<T>(31,3)*z[5] - n<T>(5,3) + z[91];
    z[90]=z[110]*z[90];
    z[110]= - z[97] + n<T>(37,3)*z[5];
    z[110]=z[5]*z[110];
    z[90]=z[110] + z[90];
    z[90]=z[3]*z[90];
    z[110]=161*z[91] - 55*z[5];
    z[110]=z[5]*z[110];
    z[111]= - z[103] - n<T>(11,2)*z[5];
    z[111]=11*z[111] + n<T>(151,4)*z[3];
    z[111]=z[3]*z[111];
    z[110]=z[110] + z[111];
    z[111]=n<T>(335,8)*z[2] + 29*z[3] - n<T>(419,8)*z[91] - 43*z[5];
    z[111]=z[2]*z[111];
    z[110]=n<T>(1,2)*z[110] + z[111];
    z[96]=z[110]*z[96];
    z[110]=n<T>(1177,4)*z[91] - 289*z[5];
    z[100]=2*z[6] - n<T>(2875,12)*z[8] - n<T>(1225,12)*z[9] - n<T>(4031,12)*z[7]
     + 
   n<T>(301,3)*z[2] + n<T>(1,3)*z[110] - z[100];
    z[100]=z[23]*z[100];
    z[110]=5*z[9];
    z[99]=z[110] + z[99];
    z[111]=n<T>(1,2)*z[99] + z[102];
    z[91]=z[91]*z[111];
    z[91]=n<T>(5,2)*z[30] + z[91];
    z[91]=z[12]*z[91];
    z[98]=z[98] + z[2];
    z[98]= - n<T>(143,2)*z[10] - z[110] - n<T>(133,2)*z[3] + 5*z[98];
    z[98]=z[26]*z[98];
    z[99]=z[103] - z[99];
    z[99]=n<T>(1,2)*z[99] - z[102];
    z[99]=z[27]*z[99];
    z[102]=z[59] + z[52];
    z[103]=n<T>(163,6)*z[38] + n<T>(125,24)*z[36] + n<T>(49,2)*z[35] - n<T>(1,2)*z[33]
    + n<T>(2707,72)*z[31];
    z[103]=i*z[103];
    z[110]=n<T>(1907,16)*z[11] - n<T>(853,16)*z[6] + n<T>(73,4)*z[10] + n<T>(1451,16)*
    z[8] + 172*z[4] + n<T>(1409,16)*z[9] - n<T>(7579,8)*z[7] + n<T>(587,4)*z[2] - 
   n<T>(5755,16)*z[5] + 77*z[3];
    z[110]=z[30]*z[110];
    z[97]=z[97] - z[2];
    z[111]= - z[5] + z[97];
    z[111]=z[19]*z[111];
    z[97]=z[9] - z[97];
    z[97]=z[24]*z[97];
    z[112]=z[6] + z[3];
    z[112]= - n<T>(1,3) - n<T>(11,2)*z[112];
    z[112]=z[41]*z[112];
    z[113]= - z[8] - z[6];
    z[113]=z[42]*z[113];
    z[114]= - z[10] - z[11];
    z[114]=z[43]*z[114];

    r +=  - n<T>(973,12)*z[32] - n<T>(89,36)*z[34] - n<T>(335,24)*z[37] - n<T>(155,6)*
      z[39] - n<T>(2419,24)*z[40] + n<T>(33,4)*z[44] - n<T>(25,8)*z[45] - n<T>(71,4)*
      z[46] + n<T>(59,12)*z[47] - n<T>(151,24)*z[48] + 14*z[49] - n<T>(13,8)*z[50]
       - n<T>(20,3)*z[51] - n<T>(5,8)*z[53] + n<T>(103,6)*z[54] + n<T>(15,4)*z[55]
     + n<T>(51,8)*z[56]
     + n<T>(113,12)*z[57]
     + n<T>(43,8)*z[58] - n<T>(57,4)*z[60]
     + n<T>(77,8)
      *z[61] + n<T>(9,4)*z[62] - n<T>(37,4)*z[63] + n<T>(5,6)*z[64] + n<T>(139,3)*z[65]
       + n<T>(6505,48)*z[66] + n<T>(293,4)*z[67] + n<T>(2357,6)*z[68] + n<T>(35,4)*
      z[69] - n<T>(523,48)*z[70] + n<T>(3,4)*z[71] + n<T>(535,16)*z[72] - n<T>(152,3)*
      z[74] + n<T>(73,6)*z[75] + n<T>(209,24)*z[76] - n<T>(119,6)*z[77] + 8*z[78]
       - n<T>(4,3)*z[79] - n<T>(91,24)*z[80] - n<T>(1,4)*z[81] + n<T>(89,8)*z[82]
     + 2*
      z[85] + z[87] + z[88] + z[89] + z[90] + z[91] + z[92] + n<T>(115,3)*
      z[93] + z[94] + z[95] + z[96] + 16*z[97] + n<T>(1,3)*z[98] + z[99] + 
      z[100] + z[101] + n<T>(3,2)*z[102] + z[103] + z[104] - n<T>(1,2)*z[105]
       + n<T>(25,2)*z[106] + n<T>(1757,12)*z[107] + n<T>(38,3)*z[108] + n<T>(23,4)*
      z[109] + n<T>(1,9)*z[110] + n<T>(28,3)*z[111] + 5*z[112] + n<T>(523,12)*
      z[113];
      r+=  n<T>(91,6)*z[114];
 
    return r;
}

template std::complex<double> qg_2lha_tf313(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf313(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
