#include "qg_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lha_tf773(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[14];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=c[13];
    z[4]=c[20];
    z[5]=c[23];
    z[6]=f[22];
    z[7]=f[42];
    z[8]=f[62];
    z[9]=f[77];
    z[10]=f[79];
    z[11]=f[80];
    z[12]= - n<T>(25,24)*z[5] + n<T>(125,6)*z[4] + 50*z[3];
    z[12]=i*z[12];
    z[13]=z[1]*i;
    z[13]= - n<T>(1027,36)*z[13] + 439*z[2];
    z[12]=n<T>(1,18)*z[13] + z[12];
    z[13]=z[6] + z[11] + z[10];

    r += n<T>(5,4)*z[7] + n<T>(46,63)*z[8] - n<T>(235,63)*z[9] + n<T>(5,21)*z[12]
     - n<T>(1,2)*z[13];
 
    return r;
}

template std::complex<double> qg_2lha_tf773(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lha_tf773(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
