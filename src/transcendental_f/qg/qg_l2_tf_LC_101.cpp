#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_101(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[52];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[8];
    z[14]=e[10];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[18];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[29];
    z[24]=c[30];
    z[25]=c[31];
    z[26]=e[3];
    z[27]=f[3];
    z[28]=f[4];
    z[29]=f[7];
    z[30]=f[11];
    z[31]=f[12];
    z[32]=f[16];
    z[33]=f[17];
    z[34]=f[23];
    z[35]=z[1]*i;
    z[36]=z[35]*z[5];
    z[37]=npow(z[5],2);
    z[38]= - z[36] + n<T>(1,2)*z[37];
    z[39]=z[35] - z[5];
    z[40]= - n<T>(1,2)*z[3] - z[39];
    z[40]=z[3]*z[40];
    z[41]=n<T>(1,2)*z[7] + z[39];
    z[41]=z[7]*z[41];
    z[42]=n<T>(3,4)*z[7] - n<T>(1,4)*z[39] - z[3];
    z[42]=z[8]*z[42];
    z[40]=z[42] + n<T>(3,2)*z[41] + n<T>(1,2)*z[38] + z[40];
    z[40]=z[8]*z[40];
    z[41]=z[3] + z[8];
    z[41]=z[26]*z[41];
    z[40]=z[40] + z[41];
    z[41]=n<T>(1,2)*z[5];
    z[42]=z[41] + z[35];
    z[43]= - n<T>(1,4)*z[2] + z[42];
    z[43]=z[2]*z[43];
    z[43]=n<T>(1,4)*z[38] + z[43];
    z[44]=z[35] + z[5];
    z[45]=n<T>(1,2)*z[6];
    z[46]=z[45] - z[44];
    z[47]=n<T>(1,4)*z[6];
    z[46]=z[46]*z[47];
    z[48]=n<T>(1,4)*z[5] - z[2];
    z[48]=n<T>(1,3)*z[48] + z[47];
    z[49]=n<T>(1,2)*z[4];
    z[48]=z[48]*z[49];
    z[43]=z[48] + n<T>(1,3)*z[43] + z[46];
    z[43]=z[4]*z[43];
    z[46]= - n<T>(11,2) + n<T>(1,3)*z[5];
    z[46]=n<T>(19,12)*z[2] + n<T>(1,4)*z[46] - n<T>(11,3)*z[35];
    z[48]=n<T>(1,2)*z[2];
    z[46]=z[46]*z[48];
    z[50]=n<T>(11,8) - n<T>(2,3)*z[5];
    z[50]=z[1]*i*z[50];
    z[46]=z[46] - n<T>(5,12)*z[37] + z[50];
    z[46]=z[2]*z[46];
    z[50]= - n<T>(7,2)*z[37] + 17*z[36];
    z[51]=n<T>(3,8)*z[2] - z[5] + n<T>(5,4)*z[35];
    z[51]=z[2]*z[51];
    z[44]=11*z[3] + 13*z[2] + n<T>(11,2) - n<T>(31,3)*z[44];
    z[44]=z[3]*z[44];
    z[44]=n<T>(1,8)*z[44] + n<T>(1,6)*z[50] + z[51];
    z[44]=z[3]*z[44];
    z[36]=n<T>(5,2)*z[37] + z[36];
    z[37]=7*z[42] - z[2];
    z[37]=z[2]*z[37];
    z[36]=n<T>(1,2)*z[36] + z[37];
    z[37]=z[5] - n<T>(17,2)*z[2];
    z[37]=n<T>(1,3)*z[37] - n<T>(7,2)*z[3];
    z[37]=z[37]*z[47];
    z[42]=n<T>(13,8)*z[3] + n<T>(7,4)*z[35] - z[2];
    z[42]=z[3]*z[42];
    z[36]=z[37] + n<T>(1,6)*z[36] + z[42];
    z[36]=z[6]*z[36];
    z[37]=z[35] - n<T>(5,2)*z[2];
    z[37]=z[2]*z[37];
    z[42]=7*z[3] + 19*z[5] + 11*z[35];
    z[42]=z[3]*z[42];
    z[37]=n<T>(1,3)*z[42] + n<T>(5,3)*z[38] + z[37];
    z[38]= - n<T>(1,3)*z[39] - z[48];
    z[38]=n<T>(7,2)*z[6] + 7*z[38] - n<T>(41,6)*z[3];
    z[38]=n<T>(1,4)*z[38] - n<T>(2,3)*z[7];
    z[38]=z[7]*z[38];
    z[39]=n<T>(7,8)*z[6] - n<T>(5,2)*z[3] - n<T>(11,4)*z[35] + z[2];
    z[39]=z[6]*z[39];
    z[37]=z[38] + n<T>(1,4)*z[37] + z[39];
    z[37]=z[7]*z[37];
    z[38]=z[35] - z[6];
    z[38]=n<T>(49,12)*z[7] + n<T>(25,12)*z[3] + z[48] - z[41] - 2*z[38];
    z[38]=z[14]*z[38];
    z[39]=z[3] + z[5];
    z[42]= - n<T>(2,3)*z[39] + z[6];
    z[42]=z[35]*z[42];
    z[42]=n<T>(1,3)*z[15] + z[42];
    z[42]=z[9]*z[42];
    z[39]= - z[35] - 2*z[39];
    z[39]=n<T>(1,3)*z[39] + z[6];
    z[39]=z[12]*z[39];
    z[35]=z[48] - z[35];
    z[47]=z[49] + z[35];
    z[47]=z[10]*z[47];
    z[41]=z[41] + z[35];
    z[41]=z[11]*z[41];
    z[35]=z[45] + z[35];
    z[35]=z[13]*z[35];
    z[45]=z[20] - z[18];
    z[48]=z[24] + z[22];
    z[49]=n<T>(19,6)*z[21] + n<T>(19,12)*z[19] + n<T>(1,9)*z[16];
    z[49]=i*z[49];
    z[50]= - 7*z[5] - n<T>(65,2)*z[2];
    z[50]= - n<T>(25,3)*z[7] - n<T>(5,6)*z[6] + n<T>(1,3)*z[50] + n<T>(35,2)*z[3];
    z[50]= - n<T>(5,36)*z[4] + n<T>(1,6)*z[50] - z[8];
    z[50]=z[15]*z[50];

    r += n<T>(31,72)*z[17] - n<T>(19,24)*z[23] + n<T>(1279,144)*z[25] + n<T>(5,24)*
      z[27] - n<T>(13,24)*z[28] + 3*z[29] - n<T>(1,24)*z[30] - n<T>(3,8)*z[31] + n<T>(9,8)*z[32]
     + n<T>(5,8)*z[33]
     + z[34]
     + n<T>(7,6)*z[35]
     + z[36]
     + z[37]
     +  z[38] + z[39] + n<T>(3,2)*z[40] + n<T>(1,3)*z[41] + z[42] + 5*z[43] + 
      z[44] - n<T>(19,18)*z[45] + z[46] + n<T>(5,3)*z[47] - n<T>(19,6)*z[48] + 
      z[49] + n<T>(1,2)*z[50];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_101(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_101(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
