#include "qg_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCA_77(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[18];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[16];
    z[6]=d[18];
    z[7]=c[3];
    z[8]=d[1];
    z[9]=d[2];
    z[10]=d[4];
    z[11]=d[7];
    z[12]=e[4];
    z[13]=e[11];
    z[14]=2*z[4];
    z[15]=z[3] + z[14];
    z[15]=z[4]*z[15];
    z[16]=z[4] + z[9] - z[10] - z[8];
    z[16]=z[2]*z[16];
    z[14]= - z[2] - z[14] + z[3] + z[6] + z[5];
    z[14]=z[1]*i*z[14];
    z[14]=z[14] + z[15] + z[16];
    z[15]=n<T>(2,3)*z[7] - z[13] - z[12];
    z[16]=npow(z[11],2);
    z[17]=z[10] - z[3];
    z[17]=2*z[17] - z[9];
    z[17]=z[9]*z[17];
    z[15]=z[17] + 2*z[15] - z[16];
    z[16]=n<T>(1,3)*z[11] - z[8];
    z[16]=z[8]*z[16];
    z[14]=2*z[16] + n<T>(1,3)*z[15] + n<T>(2,3)*z[14];

    r += n<T>(2,3)*z[14];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCA_77(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCA_77(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
