#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_67(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[54];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[8];
    z[14]=e[10];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[18];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[29];
    z[24]=c[30];
    z[25]=c[31];
    z[26]=e[3];
    z[27]=f[3];
    z[28]=f[4];
    z[29]=f[11];
    z[30]=f[12];
    z[31]=f[16];
    z[32]=f[17];
    z[33]=f[23];
    z[34]=n<T>(2,3)*z[9];
    z[35]=n<T>(1,8)*z[8];
    z[36]= - n<T>(1,12)*z[6] + n<T>(17,24)*z[7] + z[35] - z[34] - n<T>(1,4)*z[4];
    z[36]=z[1]*z[36];
    z[37]=z[2]*z[1];
    z[38]=z[3]*z[1];
    z[36]=n<T>(7,12)*z[37] + z[36] - n<T>(5,12)*z[38];
    z[36]=i*z[36];
    z[39]=n<T>(1,2)*z[14];
    z[40]=npow(z[8],2);
    z[41]=n<T>(1,16)*z[40];
    z[42]=npow(z[4],2);
    z[43]= - 2*z[12] + n<T>(5,4)*z[11];
    z[36]=z[36] - z[41] + n<T>(1,8)*z[42] - n<T>(7,144)*z[15] + n<T>(1,3)*z[43] - 
    z[39];
    z[43]= - z[8] + n<T>(5,6)*z[7];
    z[43]= - n<T>(5,24)*z[3] + n<T>(1,2)*z[43] + z[6];
    z[43]=z[3]*z[43];
    z[44]= - n<T>(13,24)*z[2] - n<T>(1,2)*z[3] + z[4] + n<T>(7,12)*z[6];
    z[44]=z[2]*z[44];
    z[43]=z[43] + z[44];
    z[44]= - z[8] + n<T>(7,9)*z[7];
    z[45]=n<T>(1,8)*z[7];
    z[44]=z[44]*z[45];
    z[46]=n<T>(1,4)*z[6];
    z[47]= - z[4] - n<T>(1,36)*z[6];
    z[47]=z[47]*z[46];
    z[48]=z[2] - z[3];
    z[49]=z[4] + n<T>(7,3)*z[6];
    z[50]= - n<T>(17,6)*z[7] - n<T>(1,2)*z[8] + z[49];
    z[48]=n<T>(1,2)*z[50] - n<T>(5,3)*z[48];
    z[48]=z[5]*z[48];
    z[36]=n<T>(1,12)*z[48] + z[47] - z[44] + n<T>(1,3)*z[36] + n<T>(1,6)*z[43];
    z[36]=z[5]*z[36];
    z[43]=npow(z[7],2);
    z[47]= - n<T>(19,24)*z[14] - n<T>(1,4)*z[26] - n<T>(2,3)*z[12];
    z[48]= - n<T>(1,3)*z[7] - n<T>(1,16)*z[6];
    z[48]=z[6]*z[48];
    z[50]= - n<T>(11,2)*z[3] + n<T>(7,2)*z[6] + n<T>(11,3)*z[7] - n<T>(11,4) + z[8];
    z[50]=z[3]*z[50];
    z[47]=n<T>(1,24)*z[50] + z[48] + n<T>(23,144)*z[43] + n<T>(1,12)*z[40] + n<T>(1,3)*
    z[47] + n<T>(1,16)*z[15];
    z[47]=z[3]*z[47];
    z[48]=n<T>(1,2)*z[7];
    z[50]=z[4] + z[48];
    z[50]=z[50]*z[46];
    z[43]=n<T>(1,8)*z[43];
    z[51]=n<T>(11,36)*z[15] + n<T>(7,12)*z[13] + z[14];
    z[50]=z[50] + z[43] + n<T>(1,3)*z[51] + n<T>(1,4)*z[42];
    z[51]=n<T>(1,2)*z[6];
    z[50]=z[50]*z[51];
    z[52]=n<T>(5,2)*z[13] + z[11];
    z[53]= - z[6] + n<T>(7,8)*z[3];
    z[53]=z[3]*z[53];
    z[42]=z[53] - z[42] - n<T>(25,72)*z[15] + z[14] + n<T>(5,6)*z[52] + z[10];
    z[52]=z[7] - n<T>(17,36)*z[6];
    z[51]=z[52]*z[51];
    z[52]=n<T>(1,2)*z[2];
    z[49]= - n<T>(7,4)*z[7] + n<T>(11,8) - z[49];
    z[49]=n<T>(47,36)*z[2] + n<T>(1,3)*z[49] + n<T>(1,4)*z[3];
    z[49]=z[49]*z[52];
    z[42]=z[49] + z[51] - z[43] + n<T>(1,3)*z[42];
    z[42]=z[42]*z[52];
    z[43]= - 2*z[13] + z[12];
    z[43]=2*z[43] - n<T>(5,2)*z[11];
    z[39]=z[41] - z[39] + n<T>(1,3)*z[43] - z[10];
    z[41]= - z[4] - n<T>(3,2)*z[7];
    z[41]=z[41]*z[46];
    z[39]=z[41] + n<T>(1,3)*z[39] + z[44];
    z[39]=z[1]*z[39];
    z[41]=n<T>(19,12)*z[6] + z[45] - n<T>(11,16) + z[4];
    z[41]=z[1]*z[41];
    z[37]= - n<T>(11,18)*z[37] + n<T>(1,3)*z[41] + n<T>(1,8)*z[38];
    z[37]=z[2]*z[37];
    z[34]=n<T>(19,24)*z[7] - z[34] + n<T>(1,4)*z[8];
    z[34]=n<T>(1,3)*z[34] + n<T>(1,8)*z[6];
    z[34]=z[1]*z[34];
    z[34]=z[34] - n<T>(5,144)*z[38];
    z[34]=z[3]*z[34];
    z[38]=z[21] + n<T>(1,2)*z[19];
    z[38]=19*z[38] - n<T>(7,12)*z[16];
    z[34]=z[37] + z[34] + n<T>(1,18)*z[38] + z[39];
    z[34]=i*z[34];
    z[37]=z[22] + z[24];
    z[38]=z[18] - z[20];
    z[39]=n<T>(613,108)*z[25] + n<T>(1,6)*z[27] - n<T>(29,36)*z[28] + n<T>(7,18)*z[29]
    + n<T>(1,12)*z[30] - n<T>(1,4)*z[31] + n<T>(1,3)*z[33] + n<T>(1,2)*z[32];
    z[37]=n<T>(67,108)*z[17] - n<T>(19,36)*z[23] + n<T>(1,2)*z[39] + n<T>(19,27)*z[38]
    - n<T>(19,9)*z[37];
    z[38]= - n<T>(7,2)*z[14] + n<T>(11,3)*z[15];
    z[38]=n<T>(1,9)*z[38] - n<T>(1,4)*z[40];
    z[35]= - z[35] - n<T>(1,9)*z[7];
    z[35]=z[7]*z[35];
    z[35]=n<T>(1,2)*z[38] + z[35];
    z[35]=z[35]*z[48];
    z[38]=z[15]*z[9];
    z[39]=z[10] - n<T>(1,12)*z[15];
    z[39]=z[4]*z[39];
    z[40]= - z[26] + n<T>(1,3)*z[15];
    z[40]=z[8]*z[40];

    r += z[34] + z[35] + z[36] + n<T>(1,2)*z[37] - n<T>(2,9)*z[38] + n<T>(1,6)*
      z[39] + n<T>(1,12)*z[40] + z[42] + z[47] + z[50];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_67(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_67(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
