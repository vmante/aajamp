#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_53(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[49];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[8];
    z[14]=e[10];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[18];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[29];
    z[24]=c[30];
    z[25]=c[31];
    z[26]=e[3];
    z[27]=f[3];
    z[28]=f[4];
    z[29]=f[7];
    z[30]=f[8];
    z[31]=f[11];
    z[32]=f[12];
    z[33]=f[16];
    z[34]=f[17];
    z[35]= - z[8] + z[7];
    z[35]=z[3]*z[35];
    z[36]=z[4] - z[6];
    z[36]=z[2]*z[36];
    z[35]=z[35] + z[36];
    z[36]= - z[8] + n<T>(5,6)*z[7];
    z[36]=z[36]*z[7];
    z[37]=npow(z[8],2);
    z[36]=z[36] + n<T>(1,6)*z[37] + z[14] + z[12];
    z[38]=n<T>(1,6)*z[15];
    z[39]=npow(z[4],2);
    z[40]= - z[4] + n<T>(5,6)*z[6];
    z[40]=z[6]*z[40];
    z[41]=z[7] - z[6];
    z[42]=z[4] - z[8] - 5*z[41];
    z[42]= - z[2] + n<T>(1,6)*z[42] + z[3];
    z[42]=z[5]*z[42];
    z[35]=z[42] + z[40] + n<T>(1,6)*z[39] - z[38] + z[13] + z[11] - z[36] + 
   n<T>(2,3)*z[35];
    z[35]=z[5]*z[35];
    z[40]= - z[3] + n<T>(1,3)*z[8];
    z[42]=n<T>(1,3)*z[4];
    z[43]=2*z[2] - z[9] - z[42] + z[40] + n<T>(5,3)*z[41];
    z[43]=z[5]*z[43];
    z[44]=z[6]*z[4];
    z[45]=n<T>(2,3)*z[8];
    z[46]=n<T>(3,2)*z[3] + n<T>(1,3)*z[7] - z[6] - z[9] + z[45];
    z[46]=z[3]*z[46];
    z[47]= - n<T>(5,2)*z[2] - z[3] - z[7] + n<T>(11,3)*z[6] - n<T>(11,2) + n<T>(4,3)*
    z[4];
    z[47]=z[2]*z[47];
    z[36]=z[43] + z[47] + z[46] - z[44] - n<T>(14,3)*z[13] - n<T>(4,3)*z[10] - 2
   *z[11] + z[36];
    z[36]=z[1]*z[36];
    z[36]= - n<T>(1,2)*z[16] + 8*z[21] + 3*z[19] + z[36];
    z[36]=i*z[36];
    z[43]=n<T>(1,2)*z[7];
    z[46]=z[2] + z[43] - n<T>(1,6)*z[6] + n<T>(11,4) - z[42];
    z[46]=z[2]*z[46];
    z[47]=npow(z[6],2);
    z[47]=z[47] - z[13];
    z[48]=z[39] - z[10];
    z[41]= - z[3]*z[41];
    z[41]=z[46] + z[41] + n<T>(7,6)*z[15] + z[11] - n<T>(2,3)*z[48] - n<T>(4,3)*
    z[47];
    z[41]=z[2]*z[41];
    z[46]= - 7*z[14] + n<T>(17,6)*z[15];
    z[43]= - z[8]*z[43];
    z[43]=z[43] + n<T>(1,3)*z[46] - n<T>(1,2)*z[37];
    z[43]=z[7]*z[43];
    z[46]=npow(z[7],2);
    z[46]= - z[14] + z[46] - z[15];
    z[37]=z[37] - z[26];
    z[40]=n<T>(1,6)*z[7] - n<T>(11,4) + z[40] - n<T>(1,2)*z[6];
    z[40]=z[3]*z[40];
    z[37]=z[40] - z[12] + n<T>(2,3)*z[37] + n<T>(4,3)*z[46];
    z[37]=z[3]*z[37];
    z[38]=2*z[10] - z[38];
    z[38]=z[38]*z[42];
    z[40]=n<T>(1,3)*z[15];
    z[42]= - z[26] + z[40];
    z[42]=z[42]*z[45];
    z[39]=z[44] + z[39];
    z[40]=7*z[13] - z[40];
    z[39]=n<T>(1,3)*z[40] + n<T>(1,2)*z[39];
    z[39]=z[6]*z[39];
    z[40]=z[29] - z[30];
    z[44]=z[17] + z[18];
    z[45]=n<T>(5,3)*z[31] + n<T>(1,3)*z[32] + z[34] - z[33];
    z[46]= - z[15]*z[9];

    r +=  - n<T>(5,3)*z[20] - 6*z[22] - z[23] - 5*z[24] + 7*z[25] + n<T>(1,6)*
      z[27] - n<T>(5,6)*z[28] + z[35] + z[36] + z[37] + z[38] + z[39] - 2*
      z[40] + z[41] + z[42] + z[43] + n<T>(4,3)*z[44] + n<T>(1,2)*z[45] + z[46]
      ;
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_53(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_53(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
