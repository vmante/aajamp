#include "qg_l1_tf_CF_decl.hpp"

template<class T>
std::complex<T> qg_l1_tf_CF_3(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[14];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[6];
    z[3]=d[8];
    z[4]=d[17];
    z[5]=d[18];
    z[6]=c[3];
    z[7]=d[7];
    z[8]=e[9];
    z[9]=e[11];
    z[10]=z[2] + z[3] - z[5] - z[4];
    z[10]=z[1]*i*z[10];
    z[10]=z[10] - n<T>(2,3)*z[6] + z[9] - z[8];
    z[11]=npow(z[7],2);
    z[12]=2*z[3];
    z[13]= - z[7]*z[12];
    z[12]=z[12] - z[2];
    z[12]=z[2]*z[12];
    z[10]=z[12] + z[13] + z[11] + 2*z[10];

    r += 4*z[10];
 
    return r;
}

template std::complex<double> qg_l1_tf_CF_3(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l1_tf_CF_3(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
