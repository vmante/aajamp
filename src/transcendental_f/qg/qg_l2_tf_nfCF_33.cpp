#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_33(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[35];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[2];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=d[1];
    z[11]=c[11];
    z[12]=c[15];
    z[13]=c[18];
    z[14]=c[23];
    z[15]=c[25];
    z[16]=c[26];
    z[17]=c[28];
    z[18]=c[29];
    z[19]=c[30];
    z[20]=c[31];
    z[21]=f[0];
    z[22]=f[3];
    z[23]=f[11];
    z[24]=f[17];
    z[25]=f[20];
    z[26]=z[4] - z[3];
    z[26]=z[26]*z[5];
    z[27]=npow(z[2],2);
    z[28]=4*z[2] - z[3];
    z[28]=z[3]*z[28];
    z[29]=2*z[3];
    z[30]= - z[29] + z[4];
    z[30]=z[4]*z[30];
    z[28]=z[26] + z[30] - 2*z[27] + z[28];
    z[28]=z[5]*z[28];
    z[30]=z[3]*z[2];
    z[31]=z[3] - z[2];
    z[32]= - z[4]*z[31];
    z[32]=z[32] - z[27] + z[30];
    z[32]=z[10]*z[32];
    z[31]= - z[4] + z[31];
    z[31]=z[3]*z[31];
    z[31]=z[27] + z[31];
    z[31]=z[4]*z[31];
    z[33]=npow(z[2],3);
    z[34]=z[3]*z[27];
    z[28]=z[32] + z[28] + z[31] - z[33] + z[34];
    z[31]= - n<T>(1,3)*z[2] + z[3];
    z[31]=z[4]*z[31];
    z[26]= - n<T>(2,3)*z[26] + z[31] + z[27] - n<T>(5,3)*z[30];
    z[27]=z[1]*i;
    z[26]=z[26]*z[27];
    z[26]=z[26] + n<T>(1,3)*z[28];
    z[28]=z[10] + z[2];
    z[30]=5*z[27] - 4*z[5] - z[28];
    z[30]=z[7]*z[30];
    z[31]=z[27] - z[28];
    z[31]=z[8]*z[31];
    z[30]= - z[30] - z[31] - z[24] - z[23] + z[22] + z[18];
    z[31]=z[5] - z[4];
    z[28]= - z[29] + z[28] - 2*z[31];
    z[28]=z[9]*z[28];
    z[29]=z[25] - z[13];
    z[27]= - 2*z[27] + z[2] + z[5];
    z[27]=z[6]*z[27];
    z[27]=z[27] + z[21];
    z[31]=8*z[16] + n<T>(4,3)*z[14] - n<T>(5,9)*z[11];
    z[31]=i*z[31];
    z[26]=5*z[20] - n<T>(16,3)*z[19] - 8*z[17] - n<T>(16,9)*z[15] + n<T>(2,9)*z[12]
    + 2*z[26] + n<T>(1,9)*z[28] + z[31] + n<T>(8,3)*z[27] - n<T>(8,9)*z[29] - n<T>(2,3)
   *z[30];

    r += 2*z[26];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_33(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_33(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
