#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_31(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[4];
    z[4]=d[15];
    z[5]=d[1];
    z[6]=d[3];
    z[7]=d[8];
    z[8]=e[2];
    z[9]=e[3];
    z[10]=c[3];
    z[11]=c[11];
    z[12]=c[15];
    z[13]=c[17];
    z[14]=c[31];
    z[15]=e[10];
    z[16]=f[2];
    z[17]=f[4];
    z[18]=f[10];
    z[19]=f[12];
    z[20]=f[16];
    z[21]=f[19];
    z[22]=npow(z[7],2);
    z[23]=i*z[1];
    z[24]=z[23] - z[6];
    z[24]= - n<T>(8,9)*z[3] + n<T>(16,3)*z[5] + z[7] + n<T>(19,3)*z[24];
    z[24]=z[3]*z[24];
    z[24]=z[22] + z[24];
    z[25]=z[2] + n<T>(2,3)*z[7];
    z[26]=n<T>(1,9)*z[6];
    z[27]= - z[26] - z[25];
    z[27]=z[6]*z[27];
    z[28]=z[6]*z[1];
    z[25]= - n<T>(8,3)*z[4] + z[25];
    z[25]=z[1]*z[25];
    z[25]=z[25] + n<T>(11,9)*z[28];
    z[25]=i*z[25];
    z[26]=n<T>(5,9)*z[5] + n<T>(16,9)*z[23] + z[2] - z[26];
    z[26]=z[5]*z[26];
    z[24]=z[26] + z[25] + z[27] - n<T>(8,3)*z[8] - n<T>(8,9)*z[9] + z[10] + n<T>(1,3)
   *z[24];
    z[24]=z[3]*z[24];
    z[25]= - z[2]*z[4];
    z[25]= - n<T>(1,9)*z[22] + z[25] + z[9] + n<T>(13,9)*z[8];
    z[25]=z[1]*z[25];
    z[26]= - 2*z[4] - z[7];
    z[26]=z[1]*z[26];
    z[26]=z[26] + 4*z[28];
    z[27]=n<T>(2,9)*z[6];
    z[26]=z[26]*z[27];
    z[25]=z[26] + z[25] - 2*z[13] - n<T>(1,27)*z[11];
    z[25]=i*z[25];
    z[26]= - 25*z[4] - 4*z[7];
    z[26]=z[1]*z[26];
    z[26]=z[26] - 5*z[28];
    z[26]=i*z[26];
    z[28]=z[22] - z[15];
    z[26]=z[26] - 25*z[8] + n<T>(8,3)*z[10] - 17*z[9] - 4*z[28];
    z[28]=z[2] + n<T>(4,9)*z[7];
    z[28]=z[6]*z[28];
    z[23]= - n<T>(55,27)*z[5] + n<T>(13,9)*z[23] + n<T>(7,3)*z[6] - z[2] - n<T>(2,9)*
    z[7];
    z[23]=z[5]*z[23];
    z[23]=z[23] + z[28] + n<T>(1,9)*z[26];
    z[23]=z[5]*z[23];
    z[26]= - z[9] - z[8];
    z[26]=z[2]*z[26];
    z[23]=z[26] + z[25] + z[23] + z[24];
    z[24]=z[7] - n<T>(8,3)*z[6];
    z[24]=z[6]*z[24];
    z[22]=z[24] + z[22] + 5*z[10] - 4*z[8];
    z[22]=z[22]*z[27];
    z[24]=z[16] + z[21];
    z[24]=n<T>(1,3)*z[17] - n<T>(4,3)*z[18] - n<T>(7,3)*z[19] + z[20] - n<T>(8,3)*z[24];
    z[25]=z[4]*z[10];
    z[26]=z[15] - n<T>(1,3)*z[10];
    z[26]=z[7]*z[26];
    z[22]=z[22] + n<T>(8,9)*z[26] - n<T>(26,9)*z[25] - n<T>(10,9)*z[12] + n<T>(2,3)*
    z[24] + z[14] + 2*z[23];

    r += n<T>(2,3)*z[22];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_31(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_31(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
