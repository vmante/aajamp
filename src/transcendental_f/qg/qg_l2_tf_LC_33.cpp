#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_33(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,82>& f,
  const std::array<std::complex<T>,330>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[63];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[1];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[8];
    z[6]=c[12];
    z[7]=c[13];
    z[8]=c[20];
    z[9]=c[23];
    z[10]=c[32];
    z[11]=c[33];
    z[12]=c[35];
    z[13]=c[36];
    z[14]=c[37];
    z[15]=c[38];
    z[16]=c[39];
    z[17]=c[40];
    z[18]=c[43];
    z[19]=c[44];
    z[20]=c[47];
    z[21]=c[48];
    z[22]=c[50];
    z[23]=c[55];
    z[24]=c[57];
    z[25]=c[59];
    z[26]=c[81];
    z[27]=c[84];
    z[28]=d[0];
    z[29]=f[24];
    z[30]=f[44];
    z[31]=f[69];
    z[32]=f[80];
    z[33]=f[22];
    z[34]=f[42];
    z[35]=d[2];
    z[36]=g[24];
    z[37]=g[27];
    z[38]=g[38];
    z[39]=g[40];
    z[40]=g[48];
    z[41]=g[50];
    z[42]=g[79];
    z[43]=g[82];
    z[44]=g[90];
    z[45]=g[92];
    z[46]=g[99];
    z[47]=g[101];
    z[48]=g[170];
    z[49]=g[183];
    z[50]=g[189];
    z[51]=g[245];
    z[52]=g[270];
    z[53]=g[278];
    z[54]=g[281];
    z[55]=g[312];
    z[56]=z[33] - z[34];
    z[57]=z[56] + z[32];
    z[58]=n<T>(1,4)*z[29];
    z[59]=n<T>(1,3)*z[6];
    z[57]= - n<T>(1,4)*z[57] + z[59] + z[58] + n<T>(1,2)*z[30];
    z[60]=z[3] - z[4];
    z[57]=z[57]*z[60];
    z[56]=z[56] - z[31];
    z[61]=n<T>(1,4)*z[30];
    z[56]=n<T>(1,4)*z[56] - z[59] + z[61] + n<T>(1,2)*z[29];
    z[59]=z[2] - z[5];
    z[56]= - z[56]*z[59];
    z[58]= - z[58] - z[61];
    z[61]=z[28] - z[35];
    z[58]=z[61]*z[58];
    z[61]=n<T>(1,4)*z[61];
    z[62]=z[31] + z[32];
    z[61]=z[62]*z[61];
    z[59]= - z[59] - z[60];
    z[60]= - z[8] + n<T>(1,20)*z[9];
    z[60]=n<T>(31,1080)*z[1] - n<T>(6,5)*z[7] + n<T>(1,2)*z[60];
    z[59]=z[60]*z[59];
    z[60]= - n<T>(1,12)*z[25] + 2*z[27] - n<T>(65,36)*z[26];
    z[59]= - n<T>(41,108)*z[11] + 4*z[14] + n<T>(253,54)*z[15] - n<T>(109,972)*z[16]
    + n<T>(5,3)*z[19] + n<T>(1,12)*z[20] - 2*z[22] + n<T>(1,3)*z[60] + n<T>(1,4)*z[24]
    + z[59];
    z[59]=i*z[59];
    z[60]=z[48] + z[49] + z[52] + z[53] - z[42] - z[43] + z[44] - z[45]
    - z[37] - z[18] - z[36] + z[38] - z[39];
    z[62]= - z[40] - z[41] - z[46] + z[47] + z[50] + z[54];
    z[56]=z[59] + n<T>(41,540)*z[10] - n<T>(12,5)*z[12] - 2*z[13] + n<T>(5,3)*z[17]
    - z[21] + n<T>(1,20)*z[23] - z[51] + z[55] + z[61] + z[58] + z[56] + 
    z[57] + n<T>(1,6)*z[62] - n<T>(1,2)*z[60];

    r += z[56];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_33(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,82>& f,
  const std::array<std::complex<double>,330>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_33(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,82>& f,
  const std::array<std::complex<dd_real>,330>& g
);
#endif
