#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_26(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[8];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[7];
    z[4]=c[3];
    z[5]=d[1];
    z[6]=z[1]*i;
    z[7]= - z[6] - z[5];
    z[7]=z[2]*z[7];
    z[6]=2*z[3] + z[2] - 5*z[6] + z[5];
    z[6]=z[3]*z[6];
    z[6]=z[7] + z[6];
    z[7]=npow(z[5],2);
    z[6]= - z[4] - z[7] + n<T>(1,3)*z[6];

    r += 2*z[6];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_26(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_26(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d
);
#endif
