#include "qg_l2_tf_nfnfaa_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfnfaa_6(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[23];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[6];
    z[4]=d[12];
    z[5]=d[1];
    z[6]=d[3];
    z[7]=d[5];
    z[8]=e[5];
    z[9]=e[7];
    z[10]=c[3];
    z[11]=c[11];
    z[12]=z[7] - n<T>(5,3);
    z[13]=4*z[12];
    z[14]=z[1]*i;
    z[15]=z[13] - z[14];
    z[16]=2*z[3];
    z[17]=z[16] - z[6];
    z[18]=z[6]*z[15]*z[17];
    z[19]=z[5] + z[2];
    z[20]=n<T>(1,3)*z[19];
    z[21]=n<T>(2,3)*z[12] - z[3];
    z[21]=2*z[21] + z[20];
    z[21]=z[10]*z[21];
    z[18]=z[18] + z[21];
    z[21]=z[14] + z[7];
    z[21]=z[21]*z[16];
    z[17]=z[17]*z[6];
    z[22]=npow(z[7],2);
    z[17]=z[22] + z[17] - z[21];
    z[17]=z[17]*z[20];
    z[15]=z[15] + z[19];
    z[20]=z[9] - z[8];
    z[20]=n<T>(2,3)*z[20];
    z[15]=z[15]*z[20];
    z[20]= - z[7]*z[16];
    z[20]=z[22] + z[20];
    z[12]=z[12]*z[20];
    z[20]=n<T>(20,9) - z[7];
    z[16]=z[20]*z[16];
    z[16]= - n<T>(1,3)*z[22] + z[16];
    z[16]=z[16]*z[14];
    z[13]=z[19] + z[13];
    z[13]=z[14]*z[13];
    z[13]=z[10] + z[13];
    z[13]=z[4]*z[13];
    z[14]=z[11]*i;
    z[12]= - n<T>(1,9)*z[14] + n<T>(2,3)*z[13] + n<T>(4,3)*z[12] + z[16] + z[15] + 
    z[17] + n<T>(1,3)*z[18];

    r += 4*z[12];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfnfaa_6(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfnfaa_6(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
