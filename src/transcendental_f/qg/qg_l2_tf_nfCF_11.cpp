#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_11(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[12];
    z[9]=d[1];
    z[10]=d[4];
    z[11]=d[16];
    z[12]=d[17];
    z[13]=d[15];
    z[14]=e[4];
    z[15]=e[5];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[17];
    z[23]=c[18];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[29];
    z[30]=c[30];
    z[31]=c[31];
    z[32]=e[6];
    z[33]=e[2];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[5];
    z[37]=f[11];
    z[38]=f[13];
    z[39]=f[17];
    z[40]=f[18];
    z[41]=f[31];
    z[42]=f[32];
    z[43]=f[33];
    z[44]=f[34];
    z[45]=f[35];
    z[46]=f[36];
    z[47]=f[37];
    z[48]=f[39];
    z[49]=f[41];
    z[50]=f[43];
    z[51]=f[68];
    z[52]=n<T>(1,3)*z[18];
    z[53]=n<T>(2,3)*z[16];
    z[54]= - z[53] - z[52] + 2*z[17];
    z[55]=npow(z[3],2);
    z[56]=2*z[55];
    z[57]=npow(z[10],2);
    z[58]=2*z[3];
    z[59]=z[58] + z[10];
    z[59]=z[7]*z[59];
    z[54]=z[59] + z[57] + z[56] + 4*z[54] - n<T>(7,9)*z[19];
    z[54]=z[7]*z[54];
    z[59]=4*z[17];
    z[60]=z[57] + n<T>(11,3)*z[16] - z[59] - 5*z[15];
    z[61]=2*z[2];
    z[62]= - z[3] + n<T>(5,3)*z[2];
    z[62]=z[62]*z[61];
    z[63]=npow(z[7],2);
    z[56]=z[62] - 4*z[63] - z[56] + z[19] - 2*z[60];
    z[56]=z[2]*z[56];
    z[60]=z[10] - n<T>(20,3);
    z[62]=n<T>(1,3)*z[7];
    z[63]= - 2*z[60] + z[62];
    z[63]=z[7]*z[63];
    z[64]=n<T>(2,3)*z[2];
    z[65]=z[7] - 10;
    z[66]= - 2*z[65] + z[2];
    z[66]=z[66]*z[64];
    z[67]=z[7] - z[10];
    z[68]=n<T>(13,3)*z[2];
    z[69]=z[68] + z[9];
    z[70]= - n<T>(22,9)*z[5] - z[69] - z[67];
    z[70]=z[5]*z[70];
    z[57]=z[70] + z[66] + z[63] - n<T>(16,3)*z[16] + z[57];
    z[57]=z[5]*z[57];
    z[63]=2*z[19];
    z[66]=n<T>(10,3) - z[12];
    z[66]=n<T>(1,3)*z[66] + z[11];
    z[66]=2*z[66] - z[8];
    z[66]=z[66]*z[63];
    z[70]=n<T>(1,3)*z[19];
    z[71]=z[15] - z[16];
    z[71]= - n<T>(20,3)*z[9] + 2*z[71] - z[70];
    z[71]=z[9]*z[71];
    z[63]= - z[14] - z[63];
    z[63]=2*z[63] + n<T>(1,3)*z[55];
    z[63]=z[63]*z[58];
    z[72]= - z[32] - z[14];
    z[73]=z[3] - n<T>(10,3);
    z[74]= - n<T>(5,3)*z[10] + z[73];
    z[74]=z[10]*z[74];
    z[72]=2*z[74] + 4*z[72] - z[19];
    z[72]=z[10]*z[72];
    z[74]= - z[47] + z[35] + z[37] + z[39] + z[44];
    z[75]=z[51] + z[50];
    z[75]= - z[38] + z[29] - n<T>(2,3)*z[75] - z[49] + z[34] + z[36];
    z[76]=z[43] - z[45];
    z[77]=z[28] + z[30];
    z[78]=z[32] + z[33];
    z[54]=z[57] + z[56] + z[54] + z[72] + z[63] + z[71] + z[66] - n<T>(22,3)
   *z[21] + n<T>(16,3)*z[23] - n<T>(10,3)*z[24] - n<T>(14,3)*z[26] + n<T>(95,3)*z[31]
    - n<T>(4,3)*z[40] - n<T>(7,3)*z[41] - 10*z[42] - z[46] + z[48] - n<T>(40,3)*
    z[78] - 16*z[77] + n<T>(8,3)*z[76] - 4*z[75] + 2*z[74];
    z[56]=n<T>(10,3)*z[5];
    z[57]=n<T>(2,3)*z[7];
    z[63]=n<T>(5,3)*z[4] - z[56] - z[57] + n<T>(20,3) - z[69];
    z[63]=z[4]*z[63];
    z[66]= - 10 - z[7];
    z[66]=z[66]*z[57];
    z[69]=z[7] - z[61];
    z[69]=z[69]*z[64];
    z[71]=z[9] + 2*z[7];
    z[68]=n<T>(4,3)*z[5] + z[68] - n<T>(20,3) + z[71];
    z[68]=z[5]*z[68];
    z[56]=n<T>(2,9)*z[6] + n<T>(11,3)*z[4] - z[56] + n<T>(1,3)*z[2] - z[67];
    z[56]=z[6]*z[56];
    z[52]=n<T>(1,3)*z[16] - 2*z[15] - z[32] - z[52];
    z[52]=z[56] + z[63] + z[68] + z[69] + z[66] + n<T>(20,3)*z[10] + z[55]
    + 2*z[52] + n<T>(23,9)*z[19];
    z[52]=z[6]*z[52];
    z[55]=n<T>(1,3)*z[12];
    z[56]= - z[55] + 2*z[8];
    z[56]=n<T>(19,3)*z[2] - z[58] + 2*z[56] + z[71];
    z[63]=z[6] + z[4];
    z[56]=n<T>(1,3)*z[56] - z[5] - n<T>(7,9)*z[63];
    z[56]=z[6]*z[56];
    z[63]= - 10*z[13] + z[18];
    z[59]=z[14] + n<T>(1,3)*z[63] - z[59];
    z[63]= - z[9]*z[8];
    z[59]=z[63] + n<T>(7,3)*z[16] + 2*z[59] - z[15];
    z[63]=n<T>(2,3)*z[11] - z[3];
    z[63]=z[3]*z[63];
    z[56]=z[56] + n<T>(1,3)*z[59] + z[63];
    z[59]=z[5] + z[64] - z[62] + z[60];
    z[59]=z[5]*z[59];
    z[60]=n<T>(1,3)*z[5];
    z[62]=n<T>(5,3) + z[8];
    z[62]= - z[60] + 2*z[62] + z[2] - z[7];
    z[62]=4*z[62] - z[4];
    z[62]=z[4]*z[62];
    z[63]=z[11] - z[3];
    z[63]=4*z[63] - z[10];
    z[63]=z[10]*z[63];
    z[62]=z[63] + z[62];
    z[55]= - z[55] - z[3];
    z[55]=2*z[55] - z[10];
    z[55]=z[55]*z[57];
    z[57]=n<T>(4,3) - z[8];
    z[57]=4*z[7] + 5*z[57] + z[58];
    z[57]=n<T>(1,3)*z[57] - z[2];
    z[57]=z[57]*z[61];
    z[55]=n<T>(2,3)*z[59] + z[57] + z[55] + 2*z[56] + n<T>(1,3)*z[62];
    z[55]=z[1]*z[55];
    z[56]=n<T>(2,3)*z[22] + n<T>(8,3)*z[27] + z[25];
    z[55]=z[55] + 2*z[56] + n<T>(5,27)*z[20];
    z[55]=i*z[55];
    z[53]=n<T>(5,3)*z[9] + z[70] - z[15] + z[53];
    z[56]= - z[2] + z[65];
    z[56]=2*z[56] + 5*z[5];
    z[56]=z[56]*z[60];
    z[57]= - z[58] + z[7];
    z[57]=z[7]*z[57];
    z[58]=2*z[73] - z[2];
    z[58]=z[2]*z[58];
    z[53]=z[56] + z[58] + 4*z[53] + z[57];
    z[56]= - n<T>(4,3)*z[4] + n<T>(2,9)*z[5] + n<T>(1,3)*z[71] + z[2];
    z[56]=z[4]*z[56];
    z[53]=n<T>(2,3)*z[53] + z[56];
    z[53]=z[4]*z[53];
    z[52]=z[55] + n<T>(2,3)*z[52] + n<T>(1,3)*z[54] + z[53];

    r += 2*z[52];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_11(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_11(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
