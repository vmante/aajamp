#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_48(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=c[3];
    z[5]=d[4];
    z[6]=d[7];
    z[7]=e[6];
    z[8]=e[7];
    z[9]=z[1]*i;
    z[9]=z[9] - z[6];
    z[10]= - z[5] - z[9];
    z[10]=z[3]*z[10];
    z[10]=z[10] + z[8] + z[7];
    z[11]=npow(z[5],2);
    z[9]=2*z[9] + z[2];
    z[9]=z[2]*z[9];
    z[9]=z[9] + z[11] + n<T>(1,3)*z[4] + 2*z[10];

    r += n<T>(16,9)*z[9];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_48(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_48(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
