#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_84(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[47];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=e[0];
    z[10]=e[1];
    z[11]=e[8];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[18];
    z[16]=c[23];
    z[17]=c[25];
    z[18]=c[26];
    z[19]=c[28];
    z[20]=c[29];
    z[21]=c[30];
    z[22]=c[31];
    z[23]=e[10];
    z[24]=e[3];
    z[25]=f[3];
    z[26]=f[4];
    z[27]=f[7];
    z[28]=f[11];
    z[29]=f[12];
    z[30]=f[16];
    z[31]=f[17];
    z[32]=n<T>(1,3)*z[4];
    z[33]= - z[9] + n<T>(1,12)*z[12];
    z[33]=z[33]*z[32];
    z[34]=n<T>(1,2)*z[5];
    z[35]=z[34]*z[4];
    z[35]=z[35] + z[11];
    z[36]=npow(z[4],2);
    z[37]= - n<T>(1,2)*z[36] + n<T>(1,18)*z[12] - z[35];
    z[37]=z[37]*z[34];
    z[38]=z[19] + z[21];
    z[39]=z[15] - z[17];
    z[40]= - z[28] + n<T>(1,9)*z[29] - z[31] - n<T>(1,3)*z[30];
    z[41]=n<T>(1,3)*z[12];
    z[42]= - z[24] + z[41];
    z[42]=z[8]*z[42];
    z[33]=z[37] + n<T>(1,9)*z[42] + z[33] - n<T>(7,36)*z[14] + n<T>(7,12)*z[20] - 
   n<T>(439,72)*z[22] - n<T>(1,12)*z[25] + n<T>(1,36)*z[26] + n<T>(1,4)*z[40] + n<T>(2,3)*
    z[27] - n<T>(7,9)*z[39] + n<T>(7,3)*z[38];
    z[37]=npow(z[8],2);
    z[38]= - n<T>(1,12)*z[37] - n<T>(1,4)*z[36] + z[41] - z[10] + z[23];
    z[39]=z[4] - n<T>(1,3)*z[5];
    z[39]=z[39]*z[34];
    z[40]=n<T>(1,3)*z[6];
    z[42]=n<T>(1,2)*z[8] + z[40];
    z[42]=z[42]*z[40];
    z[32]=n<T>(1,4)*z[2] - z[32] - z[34];
    z[32]=z[2]*z[32];
    z[34]=i*z[1];
    z[43]=z[4] + n<T>(1,3)*z[8];
    z[44]= - n<T>(7,9)*z[6] + n<T>(1,3)*z[43] + z[5];
    z[44]=z[44]*z[34];
    z[32]=n<T>(1,2)*z[44] + z[32] + z[42] + n<T>(1,3)*z[38] + z[39];
    z[38]= - n<T>(1,2) + n<T>(1,33)*z[8];
    z[39]= - n<T>(1,11)*z[34] + n<T>(2,11)*z[2] - n<T>(13,66)*z[6] - z[38];
    z[39]=n<T>(1,3)*z[39] + n<T>(1,44)*z[3];
    z[39]=z[3]*z[39];
    z[42]=n<T>(7,3)*z[6] - z[5] - z[43];
    z[42]= - z[3] + n<T>(1,2)*z[42] + z[2];
    z[42]=z[7]*z[42];
    z[32]=n<T>(1,66)*z[42] + n<T>(1,11)*z[32] + z[39];
    z[32]=z[7]*z[32];
    z[39]= - z[8] + z[5];
    z[39]=n<T>(1,2)*z[39] - z[40];
    z[39]=z[39]*z[40];
    z[40]=z[9] + z[10];
    z[35]=z[39] + n<T>(1,36)*z[37] + z[35] + n<T>(2,3)*z[40];
    z[35]=z[1]*z[35];
    z[39]= - z[18] - n<T>(1,2)*z[16];
    z[39]=7*z[39] - n<T>(1,6)*z[13];
    z[35]=n<T>(1,3)*z[39] + z[35];
    z[39]=n<T>(1,11)*z[5];
    z[42]=n<T>(1,4) - n<T>(2,11)*z[4];
    z[42]=n<T>(1,66)*z[6] + n<T>(1,3)*z[42] - z[39];
    z[42]=z[1]*z[42];
    z[43]=z[2]*z[1];
    z[42]=z[42] + n<T>(1,11)*z[43];
    z[42]=z[2]*z[42];
    z[35]=n<T>(1,11)*z[35] + z[42];
    z[35]=i*z[35];
    z[42]=npow(z[5],2);
    z[44]=npow(z[2],2);
    z[44]=z[42] - z[44];
    z[45]=z[37] - n<T>(7,4)*z[12] - z[24] + n<T>(5,2)*z[23];
    z[46]=z[5] - n<T>(1,12)*z[6];
    z[46]=z[6]*z[46];
    z[44]=z[46] + n<T>(1,3)*z[45] + n<T>(1,4)*z[44];
    z[45]=n<T>(1,22)*z[5];
    z[38]=n<T>(7,66)*z[6] - z[45] + z[38];
    z[38]=z[1]*z[38];
    z[38]=n<T>(1,3)*z[38] - n<T>(1,22)*z[43];
    z[38]=i*z[38];
    z[34]=z[34] - z[2];
    z[34]= - n<T>(1,66)*z[3] + n<T>(5,99)*z[6] - z[45] - n<T>(1,4) + n<T>(1,99)*z[8]
     + n<T>(1,22)*z[34];
    z[34]=z[3]*z[34];
    z[34]=n<T>(1,2)*z[34] + n<T>(1,33)*z[44] + z[38];
    z[34]=z[3]*z[34];
    z[38]= - z[8] - z[5];
    z[38]=z[6]*z[38];
    z[37]= - z[38] + z[42] + z[37];
    z[38]=n<T>(5,11)*z[23] - z[41];
    z[37]=n<T>(1,3)*z[38] - n<T>(1,22)*z[37];
    z[37]=z[6]*z[37];
    z[36]= - z[40] + z[36] - z[23];
    z[38]=z[42] + z[12];
    z[40]=npow(z[6],2);
    z[36]=n<T>(1,12)*z[40] - n<T>(1,2)*z[11] + n<T>(5,12)*z[38] + n<T>(1,3)*z[36];
    z[38]= - n<T>(1,4) + n<T>(1,11)*z[4];
    z[38]= - n<T>(7,44)*z[2] - n<T>(1,44)*z[6] + n<T>(1,2)*z[38] + z[39];
    z[38]=z[2]*z[38];
    z[36]=n<T>(1,11)*z[36] + n<T>(1,3)*z[38];
    z[36]=z[2]*z[36];
    z[32]=z[32] + z[34] + z[35] + z[36] + n<T>(1,11)*z[33] + n<T>(1,6)*z[37];

    r += n<T>(1,3)*z[32];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_84(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_84(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
