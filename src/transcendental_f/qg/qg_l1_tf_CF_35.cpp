#include "qg_l1_tf_CF_decl.hpp"

template<class T>
std::complex<T> qg_l1_tf_CF_35(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[9];
  std::complex<T> r(0,0);

    z[1]=c[3];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=e[1];
    z[6]=e[8];
    z[7]=z[3]*z[4];
    z[8]= - z[2] + z[4] + z[3];
    z[8]=z[2]*z[8];
    z[7]=z[8] - z[7] - z[6] - z[5];
    z[7]=n<T>(1,3)*z[1] + 2*z[7];

    r += 4*z[7];
 
    return r;
}

template std::complex<double> qg_l1_tf_CF_35(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l1_tf_CF_35(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
