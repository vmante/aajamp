#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_15(
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[3];
  std::complex<T> r(0,0);

    z[1]=f[10];

    r += n<T>(1,27)*z[1];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_15(
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_15(
  const std::array<std::complex<dd_real>,82>& f
);
#endif
