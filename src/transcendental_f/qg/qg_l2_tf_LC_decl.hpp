#ifndef QG_L2_TF_LC_DECL_H
#define QG_L2_TF_LC_DECL_H

#include <array>
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
std::complex<T> qg_l2_tf_LC_1(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_2(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_3(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_4(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_5(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_6(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_7(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_8(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_9(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_10(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_11(
  const std::array<T,85>&);

template <class T>
std::complex<T> qg_l2_tf_LC_12(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_13(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_14(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_15(
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_16(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_17(
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_18(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l2_tf_LC_19(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l2_tf_LC_20(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l2_tf_LC_21(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l2_tf_LC_22(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l2_tf_LC_23(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_24(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_25(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_26(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_27(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_28(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l2_tf_LC_29(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_30(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_31(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_32(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_33(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_34(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_35(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_36(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_37(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_38(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_39(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_40(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_41(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_42(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_43(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_44(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_45(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_46(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_47(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_48(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_49(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_50(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_51(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_52(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_53(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_54(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_55(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_56(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_57(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_58(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l2_tf_LC_59(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_60(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_61(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_62(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_63(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_64(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_65(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_66(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_67(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_68(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_69(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_70(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_71(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_72(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_73(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_74(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_75(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_76(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_77(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_78(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_79(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_80(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_81(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_82(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_83(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_84(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_85(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_86(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_87(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_88(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_89(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_90(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_91(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_92(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_93(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_94(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_95(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_96(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_97(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_98(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_99(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l2_tf_LC_100(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_101(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_102(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_103(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_104(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_105(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_106(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_107(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_108(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_109(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_110(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qg_l2_tf_LC_111(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_112(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_113(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_114(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_115(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qg_l2_tf_LC_116(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

#endif /* QG_L2_TF_LC_DECL_H */
