#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_39(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[27];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[8];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=e[10];
    z[9]=c[3];
    z[10]=e[3];
    z[11]=f[4];
    z[12]=f[7];
    z[13]=f[12];
    z[14]=f[16];
    z[15]=f[23];
    z[16]=2*i;
    z[16]=z[16]*z[1];
    z[16]= - z[5] + z[16];
    z[17]=n<T>(1,3)*z[6];
    z[18]=z[17] - z[3] + n<T>(2,3)*z[4];
    z[16]=z[18]*z[16];
    z[18]=n<T>(1,3)*z[4];
    z[19]=n<T>(7,2)*z[4];
    z[20]=z[3] - z[19];
    z[20]=z[20]*z[18];
    z[21]= - z[4] + n<T>(2,3)*z[3];
    z[22]= - 2*z[21] - z[17];
    z[22]=z[6]*z[22];
    z[23]=npow(z[3],2);
    z[24]=n<T>(1,2)*z[23];
    z[16]=z[22] + z[24] + z[20] + z[16];
    z[16]=z[5]*z[16];
    z[20]=npow(z[4],2);
    z[22]=n<T>(4,3)*z[3] - z[4];
    z[22]=z[6]*z[22];
    z[22]=z[22] + n<T>(2,3)*z[23] - z[20];
    z[22]=z[6]*z[22];
    z[25]=4*z[3];
    z[19]= - z[25] + z[19];
    z[18]=z[19]*z[18];
    z[18]=z[24] + z[18];
    z[18]=z[1]*z[18];
    z[19]=2*z[1];
    z[19]=z[21]*z[19];
    z[17]=z[1]*z[17];
    z[17]=z[19] + z[17];
    z[17]=z[6]*z[17];
    z[17]=z[18] + z[17];
    z[17]=i*z[17];
    z[18]=i*z[1];
    z[19]= - z[7] + 2*z[18];
    z[21]=z[4] - z[3];
    z[19]=z[21]*z[19];
    z[26]=z[3] - n<T>(1,2)*z[4];
    z[26]=z[4]*z[26];
    z[19]= - z[24] + z[26] + z[19];
    z[19]=z[7]*z[19];
    z[24]=n<T>(1,2)*z[2] - z[18];
    z[21]=z[21]*z[24];
    z[20]= - z[23] + z[20];
    z[20]=n<T>(1,2)*z[20] + z[21];
    z[20]=z[2]*z[20];
    z[21]=n<T>(4,9)*z[6] - z[3] + n<T>(8,9)*z[4];
    z[21]=z[9]*z[21];
    z[16]=z[20] + z[21] - z[15] - z[14] + z[22] + z[17] + z[16] + z[19];
    z[17]=n<T>(1,2) - n<T>(5,33)*z[3];
    z[17]=z[17]*z[23];
    z[19]= - 1 - n<T>(7,33)*z[3];
    z[19]=z[3]*z[19];
    z[20]=n<T>(13,2)*z[3] + 2*z[4];
    z[20]=z[4]*z[20];
    z[19]=n<T>(1,2)*z[19] + n<T>(1,33)*z[20];
    z[19]=z[4]*z[19];
    z[20]= - z[25] - 7*z[4];
    z[18]= - z[7] + n<T>(1,3)*z[20] + z[18];
    z[18]=z[8]*z[18];
    z[20]=z[13] + z[11];
    z[21]= - z[3] - z[6];
    z[21]=z[10]*z[21];
    z[16]= - n<T>(4,11)*z[12] + n<T>(4,33)*z[21] + n<T>(2,11)*z[18] + z[17] + z[19]
    + n<T>(1,33)*z[20] + n<T>(1,11)*z[16];

    r += n<T>(1,9)*z[16];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_39(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_39(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
