#include "qg_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCA_29(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[4];
    z[5]=d[16];
    z[6]=d[2];
    z[7]=e[0];
    z[8]=e[4];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[18];
    z[13]=c[19];
    z[14]=c[21];
    z[15]=c[23];
    z[16]=c[25];
    z[17]=c[26];
    z[18]=c[29];
    z[19]=c[30];
    z[20]=c[31];
    z[21]=f[1];
    z[22]=z[1]*i;
    z[23]=z[22]*z[2];
    z[24]=npow(z[2],2);
    z[25]=z[24] - 2*z[23];
    z[25]=z[3]*z[25];
    z[26]=2*z[4] - z[6];
    z[27]= - z[22] + z[3] + z[2];
    z[26]=z[6]*z[27]*z[26];
    z[28]=npow(z[2],3);
    z[25]=z[26] + z[28] + z[25];
    z[26]= - z[3]*z[2];
    z[26]=z[26] - z[24] + z[23];
    z[26]=z[4]*z[26];
    z[28]=z[3]*z[22];
    z[23]=z[23] + z[28];
    z[23]=z[5]*z[23];
    z[28]= - z[2] + z[5];
    z[28]=z[9]*z[28];
    z[23]=z[26] + z[23] + z[28] - z[18];
    z[26]= - z[8] + z[7];
    z[27]=n<T>(2,3)*z[27];
    z[26]=z[27]*z[26];
    z[22]= - z[24]*z[22];
    z[24]=z[13] - z[11];
    z[27]= - n<T>(16,3)*z[17] - 4*z[15] + n<T>(8,3)*z[14] + n<T>(2,3)*z[10];
    z[27]=i*z[27];

    r += n<T>(8,9)*z[12] + n<T>(4,9)*z[16] - n<T>(8,3)*z[19] + n<T>(11,9)*z[20] - n<T>(4,3)
      *z[21] + z[22] + n<T>(2,3)*z[23] - n<T>(14,9)*z[24] + n<T>(1,3)*z[25] + z[26]
       + z[27];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCA_29(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCA_29(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
