#ifndef QG_L1_TF_nfaa_DECL_H
#define QG_L1_TF_nfaa_DECL_H

#include <array>
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
std::complex<T> qg_l1_tf_nfaa_2(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_nfaa_3(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l1_tf_nfaa_4(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l1_tf_nfaa_5(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_nfaa_6(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_nfaa_7(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

#endif /* QG_L1_TF_nfaa_DECL_H */
