#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_58(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[17];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[8];
    z[5]=d[7];
    z[6]=e[8];
    z[7]=e[10];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=z[1]*i;
    z[11]= - z[10] - z[3];
    z[11]=z[2]*z[11];
    z[12]=npow(z[2],2);
    z[11]=z[12] + z[11];
    z[11]=z[4]*z[11];
    z[13]=z[3] - z[2];
    z[14]=z[13] + z[10];
    z[15]=z[4] - z[3];
    z[15]=z[5]*z[14]*z[15];
    z[11]=z[11] + z[15];
    z[15]=n<T>(11,2) - z[2];
    z[15]=z[15]*z[12];
    z[16]= - n<T>(11,2) + z[13];
    z[16]=z[3]*z[16];
    z[12]=z[12] + z[16];
    z[12]=z[3]*z[12];
    z[16]=z[9]*i;
    z[12]= - z[16] + z[15] + z[12];
    z[14]=n<T>(1,3)*z[14];
    z[15]=z[7] + z[6];
    z[14]=z[14]*z[15];
    z[15]= - n<T>(11,3) + z[2];
    z[15]=z[2]*z[15];
    z[16]= - n<T>(2,3)*z[2] + n<T>(1,2)*z[3];
    z[16]=z[3]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[10]=z[15]*z[10];
    z[13]=z[8]*z[13];

    r += z[10] + n<T>(1,3)*z[11] + n<T>(1,6)*z[12] - n<T>(1,2)*z[13] + z[14];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_58(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_58(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
