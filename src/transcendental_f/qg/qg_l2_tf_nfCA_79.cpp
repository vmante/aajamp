#include "qg_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCA_79(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[59];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[4];
    z[8]=d[8];
    z[9]=d[16];
    z[10]=e[0];
    z[11]=e[4];
    z[12]=e[8];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[18];
    z[17]=c[19];
    z[18]=c[21];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[29];
    z[24]=c[30];
    z[25]=c[31];
    z[26]=e[3];
    z[27]=e[10];
    z[28]=f[0];
    z[29]=f[1];
    z[30]=f[2];
    z[31]=f[3];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[10];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[13];
    z[38]=f[14];
    z[39]=f[16];
    z[40]=f[17];
    z[41]=f[18];
    z[42]=f[21];
    z[43]=n<T>(1,2)*z[5];
    z[44]=z[1]*i;
    z[45]=z[43] - z[44];
    z[46]=n<T>(1,3)*z[5];
    z[47]=z[45]*z[46];
    z[48]=n<T>(1,3)*z[3];
    z[49]=z[44] - z[5];
    z[50]=2*z[49];
    z[51]= - z[50] - z[3];
    z[51]=z[51]*z[48];
    z[52]=n<T>(1,2)*z[7] + z[49];
    z[52]=z[7]*z[52];
    z[53]= - n<T>(1,3)*z[49] + z[7];
    z[53]=n<T>(1,2)*z[53] - n<T>(2,3)*z[3];
    z[53]=z[8]*z[53];
    z[51]=z[53] + z[51] + z[47] + z[52];
    z[51]=z[8]*z[51];
    z[52]=2*z[2];
    z[46]= - n<T>(5,18)*z[6] - n<T>(2,9)*z[8] + z[48] + n<T>(1,18)*z[7] + n<T>(65,18)*
    z[4] - z[46] - z[52];
    z[46]=z[13]*z[46];
    z[50]=z[5]*z[50];
    z[53]= - 4*z[44] - z[5];
    z[53]=2*z[53] + n<T>(13,3)*z[2];
    z[53]=z[2]*z[53];
    z[50]=z[50] + z[53];
    z[50]=z[2]*z[50];
    z[46]= - z[50] - z[51] - z[46] - z[38] + z[29] - z[23];
    z[50]=z[5]*z[44];
    z[51]=z[44] - z[2];
    z[53]= - z[5] - z[51];
    z[53]=z[2]*z[53];
    z[50]=z[50] + z[53];
    z[53]=n<T>(1,3)*z[7];
    z[54]= - 4*z[49] - z[7];
    z[54]=z[54]*z[53];
    z[55]=n<T>(2,3)*z[7];
    z[51]= - z[55] - z[51];
    z[51]=z[3]*z[51];
    z[56]=2*z[44];
    z[57]=z[56] - z[4];
    z[58]=z[4]*z[57];
    z[50]=z[51] + z[54] + 2*z[50] + z[58];
    z[48]=z[50]*z[48];
    z[45]=z[45]*z[5];
    z[50]=z[56] + z[5];
    z[50]=2*z[50] - z[2];
    z[50]=z[2]*z[50];
    z[50]=z[45] + z[50];
    z[51]=n<T>(1,2)*z[4];
    z[54]=z[51] - z[44] - z[5];
    z[54]=z[4]*z[54];
    z[43]=z[43] - z[52];
    z[43]=n<T>(1,3)*z[43] + z[51];
    z[43]=z[6]*z[43];
    z[43]=z[43] + n<T>(1,3)*z[50] + z[54];
    z[43]=z[6]*z[43];
    z[50]=z[7] + z[4];
    z[51]= - z[44]*z[50];
    z[51]= - z[13] + z[51];
    z[51]=z[9]*z[51];
    z[50]= - z[44] + z[50];
    z[50]=z[11]*z[50];
    z[50]= - z[51] - z[50] + z[42] - z[28];
    z[51]= - z[52] + 11*z[44] + 4*z[5];
    z[51]=z[51]*z[52];
    z[45]=7*z[45] + z[51];
    z[44]= - n<T>(1,9)*z[4] - n<T>(11,9)*z[2] + z[44] + n<T>(7,18)*z[5];
    z[44]=z[4]*z[44];
    z[44]=n<T>(1,9)*z[45] + z[44];
    z[44]=z[4]*z[44];
    z[45]=z[55] - z[4] + n<T>(1,6)*z[49] + z[2];
    z[45]=z[7]*z[45];
    z[49]=z[4]*z[56];
    z[45]=z[45] - z[47] + z[49];
    z[45]=z[45]*z[53];
    z[47]= - z[7] - z[3];
    z[47]=z[26]*z[47];
    z[49]=z[3] + z[8];
    z[49]=z[27]*z[49];
    z[47]=z[47] + z[49] + z[41] - z[34];
    z[49]=z[24] - z[20];
    z[51]=z[30] - z[16];
    z[52]=z[36] + z[32];
    z[53]=z[6] - z[56] + z[2];
    z[53]=z[12]*z[53];
    z[53]=z[53] + z[33];
    z[54]=n<T>(14,3)*z[21] + n<T>(7,3)*z[19] + n<T>(2,3)*z[18] + n<T>(2,9)*z[14];
    z[54]=i*z[54];
    z[55]=z[2] - z[57];
    z[55]=z[10]*z[55];

    r += n<T>(5,9)*z[15] + n<T>(1,2)*z[17] - 2*z[22] - n<T>(35,18)*z[25] + n<T>(7,18)*
      z[31] + n<T>(5,18)*z[35] + n<T>(8,9)*z[37] + n<T>(1,6)*z[39] + n<T>(5,6)*z[40] + 
      n<T>(5,3)*z[43] + z[44] + z[45] - n<T>(1,3)*z[46] + n<T>(2,9)*z[47] + z[48]
       + n<T>(4,3)*z[49] - n<T>(2,3)*z[50] + n<T>(4,9)*z[51] + n<T>(1,18)*z[52] + n<T>(10,9)
      *z[53] + z[54] + n<T>(14,9)*z[55];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCA_79(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCA_79(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
