#include "qg_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCA_10(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[19];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[8];
    z[7]=d[18];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[31];
    z[11]=d[7];
    z[12]=e[11];
    z[13]=npow(z[2],2);
    z[14]=z[1]*i;
    z[15]=z[2]*z[14];
    z[16]=125*z[3] + 29*z[2] - 11*z[14];
    z[16]=z[3]*z[16];
    z[17]=z[9]*i;
    z[13]= - z[17] + z[16] - 40*z[15] - n<T>(130,9) + 11*z[13];
    z[15]=z[14] - z[2];
    z[16]= - z[5] + n<T>(34,27) + z[15];
    z[16]=z[5]*z[16];
    z[15]= - z[6] - z[15];
    z[15]=z[4]*z[15];
    z[15]=z[12] + z[16] + z[15];
    z[14]=2*z[14];
    z[16]=2*z[6];
    z[17]= - z[16] + z[14] - n<T>(80,9) - z[2];
    z[16]=z[17]*z[16];
    z[17]=z[3] - n<T>(59,3) + z[2];
    z[17]=n<T>(1,3)*z[17] - z[5];
    z[17]=z[8]*z[17];
    z[18]= - 2*z[3] + z[11];
    z[18]=z[11]*z[18];
    z[14]= - z[7]*z[14];
    z[13]=n<T>(26,9)*z[10] + z[14] + z[18] + n<T>(1,3)*z[17] + z[16] + n<T>(1,9)*
    z[13] + 2*z[15];

    r += 2*z[13];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCA_10(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCA_10(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
