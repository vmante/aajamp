#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_25(
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[3];
  std::complex<T> r(0,0);

    z[1]=d[4];

    r += n<T>(1,36)*z[1];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_25(
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_25(
  const std::array<std::complex<dd_real>,19>& d
);
#endif
