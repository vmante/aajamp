#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf422(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[193];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=e[21];
    z[4]=d[1];
    z[5]=d[20];
    z[6]=d[22];
    z[7]=d[23];
    z[8]=d[4];
    z[9]=d[24];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[8];
    z[13]=d[9];
    z[14]=d[2];
    z[15]=d[3];
    z[16]=d[7];
    z[17]=f[22];
    z[18]=f[24];
    z[19]=f[42];
    z[20]=f[44];
    z[21]=f[61];
    z[22]=f[62];
    z[23]=f[70];
    z[24]=f[71];
    z[25]=f[77];
    z[26]=f[79];
    z[27]=f[80];
    z[28]=f[81];
    z[29]=c[3];
    z[30]=d[19];
    z[31]=c[11];
    z[32]=c[12];
    z[33]=c[13];
    z[34]=c[20];
    z[35]=c[23];
    z[36]=c[32];
    z[37]=c[33];
    z[38]=c[35];
    z[39]=c[36];
    z[40]=c[37];
    z[41]=c[38];
    z[42]=c[39];
    z[43]=c[40];
    z[44]=c[43];
    z[45]=c[44];
    z[46]=c[47];
    z[47]=c[48];
    z[48]=c[49];
    z[49]=c[50];
    z[50]=c[55];
    z[51]=c[56];
    z[52]=c[57];
    z[53]=c[59];
    z[54]=c[81];
    z[55]=c[84];
    z[56]=e[17];
    z[57]=e[19];
    z[58]=e[20];
    z[59]=e[18];
    z[60]=e[23];
    z[61]=e[15];
    z[62]=e[16];
    z[63]=e[22];
    z[64]=f[25];
    z[65]=f[45];
    z[66]=f[63];
    z[67]=f[69];
    z[68]=f[82];
    z[69]=f[87];
    z[70]=f[92];
    z[71]=f[97];
    z[72]=f[102];
    z[73]=g[24];
    z[74]=g[27];
    z[75]=g[38];
    z[76]=g[40];
    z[77]=g[44];
    z[78]=g[48];
    z[79]=g[50];
    z[80]=g[60];
    z[81]=g[62];
    z[82]=g[63];
    z[83]=g[79];
    z[84]=g[82];
    z[85]=g[90];
    z[86]=g[92];
    z[87]=g[95];
    z[88]=g[99];
    z[89]=g[101];
    z[90]=g[110];
    z[91]=g[112];
    z[92]=g[113];
    z[93]=g[128];
    z[94]=g[131];
    z[95]=g[138];
    z[96]=g[140];
    z[97]=g[141];
    z[98]=g[145];
    z[99]=g[147];
    z[100]=g[155];
    z[101]=g[157];
    z[102]=g[170];
    z[103]=g[173];
    z[104]=g[181];
    z[105]=g[183];
    z[106]=g[187];
    z[107]=g[189];
    z[108]=g[191];
    z[109]=g[198];
    z[110]=g[200];
    z[111]=g[201];
    z[112]=g[212];
    z[113]=g[214];
    z[114]=g[218];
    z[115]=g[220];
    z[116]=g[224];
    z[117]=g[232];
    z[118]=g[233];
    z[119]=g[234];
    z[120]=g[243];
    z[121]=g[245];
    z[122]=g[249];
    z[123]=g[251];
    z[124]=g[254];
    z[125]=g[270];
    z[126]=g[272];
    z[127]=g[276];
    z[128]=g[278];
    z[129]=g[281];
    z[130]=g[288];
    z[131]=g[289];
    z[132]=g[290];
    z[133]=g[292];
    z[134]=g[294];
    z[135]=g[298];
    z[136]=g[301];
    z[137]=g[302];
    z[138]=g[308];
    z[139]=g[324];
    z[140]=g[329];
    z[141]=g[331];
    z[142]=g[339];
    z[143]=g[345];
    z[144]=g[347];
    z[145]=g[348];
    z[146]=g[349];
    z[147]=g[359];
    z[148]=g[378];
    z[149]=g[379];
    z[150]=g[389];
    z[151]=g[395];
    z[152]=g[397];
    z[153]=g[399];
    z[154]=g[427];
    z[155]=g[428];
    z[156]=g[430];
    z[157]=g[431];
    z[158]=g[433];
    z[159]=g[434];
    z[160]=n<T>(1,2)*z[12];
    z[161]=z[2] + z[4];
    z[162]=z[160] - z[161];
    z[162]=z[162]*z[12];
    z[163]=n<T>(1,2)*z[14];
    z[164]=z[161] - z[12];
    z[165]=z[163] - z[164];
    z[165]=z[165]*z[14];
    z[166]=z[162] + z[165];
    z[167]=z[2]*z[10];
    z[168]=z[4] + n<T>(1,2)*z[10];
    z[169]=z[168]*z[10];
    z[167]=z[167] + z[169];
    z[170]=n<T>(1,2)*z[13];
    z[171]=z[161] + z[10];
    z[172]= - z[170] + z[171];
    z[172]=z[13]*z[172];
    z[173]=z[171] - z[13];
    z[174]=n<T>(1,2)*z[11];
    z[175]= - z[174] + z[173];
    z[175]=z[11]*z[175];
    z[173]=z[173] - z[11];
    z[176]=n<T>(1,2)*z[15];
    z[177]= - z[176] + z[173];
    z[177]=z[15]*z[177];
    z[172]=z[177] + z[175] + z[172] + z[166] - z[167];
    z[172]=z[61]*z[172];
    z[175]=npow(z[10],2);
    z[177]=npow(z[4],2);
    z[175]=z[175] - z[177];
    z[178]=z[10] - z[4];
    z[178]=z[178]*z[2];
    z[175]=z[178] + n<T>(1,2)*z[175];
    z[178]=z[2] + z[10];
    z[179]= - z[170] + z[178];
    z[179]=z[13]*z[179];
    z[180]=z[178] - z[13];
    z[181]=n<T>(1,2)*z[16];
    z[182]= - z[181] + z[180];
    z[182]=z[16]*z[182];
    z[179]=z[182] + z[179] + z[166] - z[175];
    z[179]=z[59]*z[179];
    z[182]=z[10] + z[4];
    z[183]=z[182] - z[12];
    z[184]=z[183] - z[174];
    z[184]=z[184]*z[11];
    z[185]=n<T>(1,2)*z[2];
    z[186]=z[185] + z[4];
    z[186]=z[186]*z[2];
    z[187]=z[10] - z[2];
    z[187]=z[12]*z[187];
    z[187]=z[184] + z[165] + z[187] + z[186] - z[169];
    z[187]=z[58]*z[187];
    z[188]=z[164] + z[10];
    z[189]=n<T>(1,2)*z[8];
    z[190]=z[189] - z[188];
    z[190]=z[8]*z[190];
    z[188]=z[188] - z[8];
    z[191]=z[176] - z[188];
    z[191]=z[15]*z[191];
    z[168]= - z[12] + z[168] + z[2];
    z[168]=z[10]*z[168];
    z[165]=z[191] + z[190] - z[165] + z[168];
    z[165]=z[63]*z[165];
    z[168]=z[182] - z[8];
    z[170]=z[168] - z[170];
    z[170]=z[170]*z[13];
    z[190]=z[189] - z[182];
    z[190]=z[190]*z[8];
    z[170]=z[170] - z[190];
    z[186]=z[186] + z[166];
    z[190]= - z[169] + z[186] + z[170];
    z[190]=z[57]*z[190];
    z[178]=z[178] - z[14];
    z[174]=z[174] - z[178];
    z[174]=z[11]*z[174];
    z[191]=z[12] + z[10];
    z[192]=z[4] - z[191];
    z[192]=z[14]*z[192];
    z[174]=z[174] + z[192] - z[162] + z[175];
    z[174]=z[60]*z[174];
    z[175]= - z[189] + z[161];
    z[175]=z[8]*z[175];
    z[161]=z[161] - z[8];
    z[189]= - z[181] + z[161];
    z[189]=z[16]*z[189];
    z[166]=z[189] + z[175] + z[166];
    z[166]=z[62]*z[166];
    z[165]=z[190] + z[179] + z[187] + z[174] + z[166] + z[172] + z[165];
    z[162]=z[162] - z[167];
    z[166]=z[1]*i;
    z[163]=z[166] - z[163] + n<T>(1,2)*z[171];
    z[167]= - n<T>(1,4)*z[15] + z[163];
    z[167]=z[15]*z[167];
    z[163]=z[163] - z[176];
    z[172]= - n<T>(1,4)*z[16] + z[163];
    z[172]=z[16]*z[172];
    z[174]=z[4]*i;
    z[175]=z[10]*i;
    z[176]=z[174] + z[175];
    z[179]=z[2]*i;
    z[187]= - z[179] - z[176];
    z[187]=z[1]*z[187];
    z[189]=n<T>(1,2)*z[191] + z[166];
    z[189]=z[14]*z[189];
    z[162]=z[172] + z[167] + z[189] + n<T>(1,2)*z[162] + z[187];
    z[167]=n<T>(1,2)*z[3];
    z[162]=z[162]*z[167];
    z[160]= - z[160] + z[182];
    z[160]=z[12]*z[160];
    z[172]=n<T>(1,2)*z[177];
    z[160]=z[160] + z[170] + z[184] + z[172] + z[169];
    z[169]=z[185] + z[182];
    z[169]=z[2]*z[169];
    z[160]=z[167] + z[169] + n<T>(1,3)*z[160];
    z[160]=z[29]*z[160];
    z[167]=z[176]*z[1];
    z[169]=n<T>(1,2)*z[166];
    z[170]=z[169]*z[13];
    z[182]=z[166]*z[8];
    z[170]=z[182] + z[170] - z[167];
    z[170]=z[170]*z[13];
    z[182]=z[169]*z[8];
    z[167]=z[182] - z[167];
    z[167]=z[167]*z[8];
    z[167]=z[170] + z[167];
    z[170]=z[12]*i;
    z[182]=z[170] - z[176];
    z[182]=z[182]*z[1];
    z[169]=z[169]*z[11];
    z[169]=z[182] + z[169];
    z[169]=z[169]*z[11];
    z[182]=n<T>(1,2)*i;
    z[184]=z[182]*z[12];
    z[176]=z[184] - z[176];
    z[176]=z[176]*z[12];
    z[184]= - z[1]*z[176];
    z[184]= - z[169] + z[184] + z[167];
    z[184]=z[9]*z[184];
    z[160]=n<T>(1,2)*z[184] + z[162] + z[160] + n<T>(1,4)*z[165];
    z[162]= - z[15] + z[173];
    z[162]=z[64]*z[162];
    z[165]=z[15] - z[188];
    z[165]=z[68]*z[165];
    z[173]=z[16] - z[180];
    z[173]=z[65]*z[173];
    z[178]= - z[11] + z[178];
    z[178]=z[66]*z[178];
    z[161]= - z[16] + z[161];
    z[161]=z[69]*z[161];
    z[164]=z[14] - z[164];
    z[164]=z[70]*z[164];
    z[168]=z[13] - z[168];
    z[168]=z[71]*z[168];
    z[180]= - z[11] + z[183];
    z[180]=z[72]*z[180];
    z[161]=z[161] + z[164] + z[168] + z[180] + z[162] + z[165] + z[173]
    + z[178];
    z[162]=z[13] + z[8];
    z[164]=z[11] + z[14];
    z[165]=z[16] + z[12];
    z[168]=237*z[166];
    z[173]= - z[168] + n<T>(237,2)*z[15];
    z[178]=n<T>(391129,96)*z[2] + n<T>(1861,3)*z[4] + n<T>(161603,32)*z[10];
    z[162]=n<T>(1,3)*z[178] - z[173] - n<T>(237,2)*z[165] - n<T>(391129,288)*z[164]
    - n<T>(1861,9)*z[162];
    z[162]=z[18]*z[162];
    z[164]=n<T>(1108511,3)*z[174] + 25589*z[175];
    z[165]=n<T>(468311,96)*z[170] - n<T>(1,32)*z[164] + 2003*z[179];
    z[178]=z[14]*i;
    z[165]=n<T>(1,2)*z[165] - n<T>(79,3)*z[178];
    z[180]=z[8]*i;
    z[165]=n<T>(1,3)*z[165] + n<T>(5759,8)*z[180];
    z[183]=z[16]*i;
    z[184]=z[13]*i;
    z[185]=z[11]*i;
    z[187]=z[15]*i;
    z[165]= - n<T>(79,45)*z[183] + n<T>(8053,90)*z[187] + n<T>(285343,2880)*z[185]
    + n<T>(1,5)*z[165] - n<T>(167,36)*z[184];
    z[165]=z[35]*z[165];
    z[188]=z[8] + z[14];
    z[189]=z[16] + z[13];
    z[190]=41849*z[4] + 19169*z[10];
    z[190]= - n<T>(41849,72)*z[12] + n<T>(1,72)*z[190] + 159*z[2];
    z[188]= - n<T>(41849,288)*z[11] + n<T>(1,4)*z[190] + z[173] - n<T>(159,4)*z[189]
    + n<T>(237,2)*z[188];
    z[188]=z[17]*z[188];
    z[189]=n<T>(454489,48)*z[12] + n<T>(490849,48)*z[2] - n<T>(205675,16)*z[4] + 
   n<T>(7607,3)*z[10];
    z[173]= - n<T>(1009,2)*z[16] - n<T>(17369,36)*z[11] + n<T>(6421,36)*z[13] + 
   n<T>(16051,36)*z[8] - n<T>(311425,288)*z[14] + n<T>(1,6)*z[189] + z[173];
    z[173]=z[20]*z[173];
    z[189]= - n<T>(7177,2)*z[2] - n<T>(7321,2)*z[4] + 5611*z[10];
    z[189]=n<T>(23465,3)*z[14] - n<T>(22393,3)*z[166] + n<T>(1,3)*z[189] - n<T>(4457,2)*
    z[12];
    z[189]=n<T>(493,2)*z[11] + n<T>(509,2)*z[13] + n<T>(1,3)*z[189] - 1989*z[8];
    z[189]=n<T>(15877,36)*z[16] + n<T>(1,2)*z[189] - n<T>(2711,9)*z[15];
    z[189]=z[25]*z[189];
    z[190]=n<T>(57469,2)*z[4] - 17825*z[10];
    z[190]=n<T>(1,3)*z[190] - n<T>(5377,2)*z[2];
    z[190]=n<T>(1,2)*z[190] - n<T>(10153,3)*z[12];
    z[190]= - n<T>(1235,4)*z[16] - n<T>(25399,36)*z[11] + n<T>(52517,36)*z[13] - n<T>(925,4)*z[8]
     + n<T>(3119,6)*z[14]
     + n<T>(1,3)*z[190]
     + z[168];
    z[190]=z[22]*z[190];
    z[162]=z[190] + z[165] + z[148] - z[81] + z[188] + z[162] + z[173]
    + z[189];
    z[165]=1133791*z[174] + 102047*z[175];
    z[165]= - 2737*z[187] - n<T>(305567,96)*z[185] - n<T>(143,2)*z[184] - n<T>(54359,12)*z[180]
     - n<T>(162845,32)*z[170]
     + n<T>(1,96)*z[165] - 1529*z[179];
    z[165]=z[34]*z[165];
    z[173]= - 12754961*z[174] + 16573343*z[175];
    z[173]=n<T>(1,64)*z[173] - 87773*z[179];
    z[173]=n<T>(136267,72)*z[180] + n<T>(137489,18)*z[178] + n<T>(1,27)*z[173] - 
   n<T>(355477,64)*z[170];
    z[173]=n<T>(188381,60)*z[183] + n<T>(17227,30)*z[187] - n<T>(4346687,8640)*
    z[185] + n<T>(1,5)*z[173] + n<T>(55673,108)*z[184];
    z[173]=z[31]*z[173];
    z[164]= - n<T>(17277,2)*z[180] + n<T>(316,3)*z[178] - n<T>(468311,48)*z[170]
     + n<T>(1,16)*z[164] - 4006*z[179];
    z[164]=n<T>(316,15)*z[183] - n<T>(16106,15)*z[187] - n<T>(285343,240)*z[185]
     + n<T>(1,5)*z[164]
     + n<T>(167,3)*z[184];
    z[164]=z[33]*z[164];
    z[170]=z[174] + n<T>(1,2)*z[175];
    z[170]=z[170]*z[10];
    z[174]=z[177]*z[182];
    z[170]=z[170] + z[174];
    z[174]=z[170]*z[1];
    z[167]=z[174] + z[167];
    z[174]=z[6] - z[5];
    z[174]= - n<T>(79,6)*z[174];
    z[167]=z[167]*z[174];
    z[174]=280601*z[4] - 348485*z[10];
    z[174]=n<T>(233989,4)*z[12] + n<T>(1,4)*z[174] - 12377*z[2];
    z[174]=n<T>(363049,36)*z[11] - 479*z[13] - n<T>(16337,9)*z[8] + n<T>(32935,9)*
    z[14] + n<T>(1,9)*z[174] + n<T>(26705,2)*z[166];
    z[174]= - n<T>(88579,12)*z[16] + n<T>(1,2)*z[174] - n<T>(3931,9)*z[15];
    z[174]=z[26]*z[174];
    z[175]=z[14] + z[12];
    z[177]=n<T>(8567,8)*z[4] + 4871*z[10];
    z[175]=n<T>(1,3)*z[177] + n<T>(61349,8)*z[2] - n<T>(8567,24)*z[175];
    z[177]=z[8] + z[15] + z[11];
    z[175]= - n<T>(17669,18)*z[16] - n<T>(4871,18)*z[13] + n<T>(1,6)*z[175] - 237*
    z[177];
    z[175]=z[67]*z[175];
    z[174]= - z[80] + z[174] + z[175];
    z[170]= - z[176] - z[170];
    z[170]=z[1]*z[170];
    z[169]=z[170] - z[169];
    z[169]=z[7]*z[169];
    z[170]= - z[172] - z[186];
    z[170]=z[56]*z[170];
    z[163]=z[181] - z[163];
    z[163]=z[24]*z[163];
    z[172]= - 151585*z[4] - 150481*z[10];
    z[172]= - 30*z[16] - n<T>(2189,24)*z[15] + n<T>(146161,1152)*z[11] - n<T>(1975,24)*z[13]
     - n<T>(51,2)*z[8]
     - n<T>(649,24)*z[14]
     + n<T>(237,4)*z[166]
     + n<T>(280081,1152)*z[12]
     + n<T>(1,1152)*z[172]
     + 89*z[2];
    z[172]=z[19]*z[172];
    z[175]=n<T>(379,2)*z[12] - n<T>(3413,6)*z[2] - 1747*z[4] - n<T>(10603,6)*z[10];
    z[175]=n<T>(10961,12)*z[13] + n<T>(6131,12)*z[8] + n<T>(487,4)*z[14] + n<T>(1,2)*
    z[175] - z[168];
    z[175]=n<T>(1,2)*z[175] + 119*z[11];
    z[175]=n<T>(307,12)*z[16] + n<T>(1,2)*z[175] + n<T>(223,3)*z[15];
    z[175]=z[21]*z[175];
    z[176]=n<T>(35959,3)*z[4] - 101867*z[10];
    z[176]=n<T>(1,16)*z[176] - n<T>(14453,3)*z[2];
    z[176]=n<T>(1,3)*z[176] - n<T>(711,16)*z[12];
    z[168]= - n<T>(781,36)*z[16] + n<T>(1185,8)*z[15] + n<T>(79115,192)*z[11] + n<T>(237,8)*z[13]
     - n<T>(5047,36)*z[8]
     + n<T>(12289,24)*z[14]
     + n<T>(1,4)*z[176] - z[168]
   ;
    z[168]=z[23]*z[168];
    z[176]=79*z[4];
    z[177]= - n<T>(4117,9)*z[12] - n<T>(23873,16)*z[2] + z[176] + n<T>(62777,144)*
    z[10];
    z[177]=n<T>(237,8)*z[16] + n<T>(2879,36)*z[15] - n<T>(63497,1152)*z[11] + n<T>(5731,72)*z[13]
     + n<T>(121,4)*z[8]
     + n<T>(85529,1152)*z[14]
     + n<T>(1,8)*z[177]
     + n<T>(8,3)
   *z[166];
    z[177]=z[27]*z[177];
    z[176]=n<T>(7055,9)*z[2] - z[176] - n<T>(3457,3)*z[10];
    z[166]= - n<T>(4643,72)*z[16] + n<T>(6943,72)*z[15] + n<T>(2767,36)*z[11] - 30*
    z[13] - n<T>(2335,12)*z[8] + n<T>(4849,72)*z[14] - n<T>(3551,18)*z[166] + n<T>(1,8)*
    z[176] + n<T>(406,9)*z[12];
    z[166]=z[28]*z[166];
    z[171]=z[30] - z[171];
    z[171]=z[29]*z[171];
    z[176]= - z[31]*i;
    z[171]=z[176] + z[171];
    z[171]=z[30]*z[171];
    z[176]=z[146] + z[153] - z[147];
    z[178]=z[130] - z[109];
    z[179]=z[131] - z[110];
    z[180]=z[132] - z[111];
    z[181]=z[159] + z[141];
    z[182]= - n<T>(841,6)*z[55] + n<T>(54665,432)*z[54] - n<T>(997,72)*z[53] - n<T>(841,16)*z[52]
     + n<T>(4411,12)*z[51]
     + n<T>(841,2)*z[49]
     + 881*z[48]
     + n<T>(24437,1944)*z[46] - n<T>(4205,12)*z[45]
     + n<T>(91669,3888)*z[42] - n<T>(573925,432)*
    z[41] - 1225*z[40] + n<T>(80323,288)*z[37];
    z[182]=i*z[182];
    z[183]=142597*z[4] - 689965*z[10];
    z[183]=n<T>(445099,4)*z[12] + n<T>(1,12)*z[183] + 122701*z[2];
    z[183]=n<T>(1,9)*z[183] - 9977*z[14];
    z[183]= - n<T>(117895,48)*z[16] - n<T>(1162,9)*z[15] + n<T>(699025,864)*z[11]
     - 
   n<T>(9911,24)*z[13] + n<T>(1,8)*z[183] + n<T>(9346,27)*z[8];
    z[183]=z[32]*z[183];

    r += n<T>(1337659,38880)*z[36] - n<T>(10993,10)*z[38] - n<T>(947,4)*z[39] + 
      n<T>(4735,24)*z[43] + n<T>(947,16)*z[44] - n<T>(131287,72)*z[47] + n<T>(10993,480)
      *z[50] - n<T>(30689,576)*z[73] - n<T>(402289,576)*z[74] - n<T>(390913,576)*
      z[75] - n<T>(42065,576)*z[76] + n<T>(43,72)*z[77] + n<T>(41921,1728)*z[78]
     - 
      n<T>(153875,576)*z[79] - n<T>(1,2)*z[82] - n<T>(118369,576)*z[83] - n<T>(540853,576)*z[84]
     - n<T>(176875,192)*z[85]
     - n<T>(107389,576)*z[86]
     + n<T>(619,288)*
      z[87] + n<T>(284401,1728)*z[88] - n<T>(419465,1728)*z[89] + n<T>(893,24)*
      z[90] + n<T>(1819,24)*z[91] + n<T>(1753,12)*z[92] - n<T>(18925,48)*z[93] + 
      n<T>(6397,24)*z[94] + n<T>(28747,72)*z[95] - n<T>(3277,8)*z[96] + n<T>(7,36)*
      z[97] + n<T>(743,9)*z[98] - n<T>(56531,432)*z[99] - n<T>(17,16)*z[100] - n<T>(17,4)*z[101]
     + n<T>(5149,576)*z[102]
     + n<T>(99581,96)*z[103]
     + n<T>(38533,48)*
      z[104] + n<T>(16525,576)*z[105] - n<T>(43,144)*z[106] + n<T>(36701,576)*
      z[107] + n<T>(205445,864)*z[108] - n<T>(87749,144)*z[112] + n<T>(148081,288)*
      z[113] + n<T>(17225,48)*z[114] - n<T>(37429,288)*z[115] + n<T>(5383,72)*
      z[116] - n<T>(64,3)*z[117] - n<T>(2759,48)*z[118] - n<T>(1337,24)*z[119] + 
      n<T>(81061,48)*z[120];
      r+=  - n<T>(19975,72)*z[121] - n<T>(623,2)*z[122] - n<T>(11791,36)*z[123] - 
      n<T>(82823,288)*z[124] + n<T>(72533,576)*z[125] - n<T>(57415,72)*z[126] + 
      n<T>(367387,288)*z[127] + n<T>(62549,576)*z[128] - n<T>(63059,576)*z[129] - 
      n<T>(14027,72)*z[133] + n<T>(10825,72)*z[134] - n<T>(7,16)*z[135] + n<T>(7,24)*
      z[136] + n<T>(7,6)*z[137] - n<T>(711,4)*z[138] + n<T>(3713,8)*z[139] - n<T>(983,3)
      *z[140] - n<T>(91,144)*z[142] - z[143] - 2*z[144] - 4*z[145] + z[149]
       - n<T>(869,288)*z[150] - n<T>(237,32)*z[151] - n<T>(237,8)*z[152] + n<T>(1,3)*
      z[154] + n<T>(11,6)*z[155] - n<T>(5,16)*z[156] - n<T>(5,8)*z[157] - n<T>(3,8)*
      z[158] + n<T>(79,3)*z[160] + n<T>(79,72)*z[161] + n<T>(1,4)*z[162] + n<T>(79,36)*
      z[163] + z[164] + n<T>(1,6)*z[165] + z[166] + z[167] + z[168] + n<T>(79,6)
      *z[169] + n<T>(79,12)*z[170] + n<T>(158,3)*z[171] + z[172] + n<T>(1,36)*
      z[173] + n<T>(1,8)*z[174] + z[175] + n<T>(79,144)*z[176] + z[177] - n<T>(697,48)*z[178]
     - n<T>(697,24)*z[179]
     - n<T>(697,12)*z[180]
     + n<T>(3,4)*z[181]
     +  z[182] + z[183];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf422(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf422(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
