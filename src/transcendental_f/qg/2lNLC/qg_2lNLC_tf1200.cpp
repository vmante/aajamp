#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1200(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[54];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[7];
    z[7]=d[2];
    z[8]=d[12];
    z[9]=d[4];
    z[10]=d[17];
    z[11]=e[0];
    z[12]=e[5];
    z[13]=e[7];
    z[14]=e[8];
    z[15]=e[9];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[28];
    z[23]=c[30];
    z[24]=c[31];
    z[25]=e[1];
    z[26]=e[6];
    z[27]=f[0];
    z[28]=f[3];
    z[29]=f[5];
    z[30]=f[11];
    z[31]=f[17];
    z[32]=f[20];
    z[33]=f[31];
    z[34]=f[32];
    z[35]=f[33];
    z[36]=f[36];
    z[37]=f[39];
    z[38]=f[43];
    z[39]=z[1]*i;
    z[40]=n<T>(3,2)*z[39];
    z[41]=z[40] - z[6];
    z[42]=n<T>(1,2)*z[3];
    z[43]= - z[42] - z[41];
    z[43]=z[3]*z[43];
    z[44]=n<T>(3,2)*z[3];
    z[45]=z[5] - z[44] + z[41];
    z[46]=n<T>(3,2)*z[5];
    z[45]=z[45]*z[46];
    z[47]=z[5] + z[3];
    z[47]=n<T>(1,4)*z[2] - n<T>(1,2)*z[6] - z[39] + n<T>(3,4)*z[47];
    z[47]=z[2]*z[47];
    z[48]=z[39]*z[6];
    z[49]=npow(z[6],2);
    z[43]=z[47] + z[45] + z[43] - z[49] + 2*z[48];
    z[43]=z[2]*z[43];
    z[45]=17*z[49] - 11*z[48];
    z[47]=3*z[3];
    z[41]=z[3] - z[41];
    z[41]=z[41]*z[47];
    z[50]=z[6] + z[39];
    z[50]=3*z[5] + n<T>(11,4)*z[50] + z[47];
    z[50]=z[5]*z[50];
    z[41]=z[50] + n<T>(1,2)*z[45] + z[41];
    z[45]=n<T>(1,2)*z[5];
    z[41]=z[41]*z[45];
    z[50]=z[39] + z[2];
    z[46]=z[46] + z[42] + z[6] - n<T>(1,2)*z[50];
    z[46]=z[2]*z[46];
    z[49]= - z[48] + n<T>(1,2)*z[49];
    z[47]= - z[47] - n<T>(13,2)*z[5];
    z[45]=z[47]*z[45];
    z[40]= - z[6] - z[40];
    z[40]=z[3]*z[40];
    z[40]=z[46] + z[45] - n<T>(19,4)*z[49] + z[40];
    z[45]=z[2] - z[3];
    z[46]=z[39] - z[6];
    z[47]= - n<T>(9,8)*z[5] + n<T>(11,16)*z[46] - z[45];
    z[47]=3*z[47] + n<T>(49,8)*z[4];
    z[47]=z[4]*z[47];
    z[40]=n<T>(3,2)*z[40] + z[47];
    z[40]=z[4]*z[40];
    z[47]=z[39] - z[3];
    z[44]=z[47]*z[44];
    z[44]=z[44] + z[49];
    z[44]=z[3]*z[44];
    z[50]=z[9] + z[5];
    z[51]=n<T>(3,2)*z[4];
    z[52]= - z[51] + n<T>(1,2)*z[46] + z[50];
    z[52]=z[9]*z[52];
    z[52]=z[49] - z[52];
    z[53]= - n<T>(1,2)*z[4] - z[46];
    z[51]=z[53]*z[51];
    z[53]=z[5]*z[46];
    z[51]=z[51] + z[53] - n<T>(1,2)*z[52];
    z[51]=z[9]*z[51];
    z[42]=z[42] + z[46];
    z[42]=z[3]*z[42];
    z[52]=z[6] - z[3];
    z[52]=z[7]*z[52];
    z[42]=n<T>(1,2)*z[52] + z[42] + z[49];
    z[42]=z[7]*z[42];
    z[49]= - z[2] + 2*z[39];
    z[52]= - z[7] + z[49];
    z[52]=z[11]*z[52];
    z[49]=z[6] - z[49];
    z[49]=z[14]*z[49];
    z[40]= - z[22] - z[49] - z[52] - z[42] - n<T>(11,4)*z[51] - z[40] - 
    z[43] - z[44] - z[41] + z[27] - z[23];
    z[41]=n<T>(19,4)*z[4] + n<T>(7,2)*z[5] - z[46] - n<T>(5,4)*z[45];
    z[41]=z[13]*z[41];
    z[42]=z[4] + z[3];
    z[42]=z[39]*z[42];
    z[42]=z[16] + z[42];
    z[42]=z[8]*z[42];
    z[43]= - z[4] + z[47];
    z[43]=z[12]*z[43];
    z[41]=z[41] + z[42] + z[43] - z[34];
    z[39]= - z[5]*z[39];
    z[39]= - z[16] - z[48] + z[39];
    z[39]=z[10]*z[39];
    z[42]= - z[5] + z[46];
    z[42]=z[15]*z[42];
    z[39]=z[39] + z[42];
    z[42]=z[9] - z[2];
    z[43]= - n<T>(31,2)*z[6] + 13*z[3];
    z[42]=n<T>(123,2)*z[4] + n<T>(1,2)*z[43] - 9*z[5] + 11*z[42];
    z[42]=n<T>(1,2)*z[42] + z[7];
    z[42]=z[16]*z[42];
    z[42]=z[42] - z[21];
    z[43]=z[25]*z[45];
    z[43]= - z[28] - z[43] + z[31] + z[30] + z[29];
    z[44]=z[26]*z[50];
    z[44]=z[44] + z[18];
    z[45]=3*z[20] - n<T>(29,16)*z[17];
    z[45]=i*z[45];

    r += n<T>(1,4)*z[19] + n<T>(47,4)*z[24] + z[32] - n<T>(243,16)*z[33] - n<T>(81,2)*
      z[35] + n<T>(33,16)*z[36] - n<T>(99,16)*z[37] + n<T>(9,2)*z[38] + n<T>(69,4)*
      z[39] - 3*z[40] + 9*z[41] + n<T>(1,2)*z[42] + n<T>(3,2)*z[43] + n<T>(33,4)*
      z[44] + z[45];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1200(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1200(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
