#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf723(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[59];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[8];
    z[17]=e[9];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[10];
    z[28]=e[6];
    z[29]=f[3];
    z[30]=f[4];
    z[31]=f[5];
    z[32]=f[11];
    z[33]=f[12];
    z[34]=f[16];
    z[35]=f[17];
    z[36]=f[31];
    z[37]=f[36];
    z[38]=f[39];
    z[39]=n<T>(1,2)*z[5];
    z[40]=z[1]*i;
    z[41]=z[39] - z[40];
    z[41]=z[41]*z[5];
    z[42]=npow(z[6],2);
    z[42]=5*z[42] + n<T>(38171,754)*z[41];
    z[43]=5*z[2];
    z[44]=n<T>(1,2)*z[2];
    z[45]= - z[6] + z[44];
    z[45]=z[45]*z[43];
    z[46]=z[40] - z[5];
    z[47]=n<T>(1,2)*z[9] + z[46];
    z[47]=z[9]*z[47];
    z[48]= - 45711*z[46] + 122053*z[9];
    z[48]=z[7]*z[48];
    z[42]=n<T>(1,3016)*z[48] + n<T>(122053,1508)*z[47] + n<T>(1,2)*z[42] + z[45];
    z[42]=z[7]*z[42];
    z[45]=z[44] - z[40];
    z[47]=z[2]*z[45];
    z[48]=n<T>(41941,8)*z[40] - 5007*z[5];
    z[48]=z[5]*z[48];
    z[49]= - z[7] - z[46];
    z[49]=z[7]*z[49];
    z[50]=n<T>(3,2)*z[2] - n<T>(1,2)*z[40] + z[5];
    z[50]= - n<T>(34401,2)*z[3] + 38171*z[50] - n<T>(41941,2)*z[7];
    z[50]=z[3]*z[50];
    z[47]=n<T>(1,8)*z[50] + n<T>(41941,8)*z[49] + z[48] - n<T>(114513,8)*z[47];
    z[47]=z[3]*z[47];
    z[48]=125823*z[40] - n<T>(133363,2)*z[6];
    z[48]=z[6]*z[48];
    z[49]= - n<T>(133363,2)*z[5] - 110743*z[40] + 133363*z[6];
    z[49]=z[5]*z[49];
    z[48]=z[48] + z[49];
    z[49]= - z[40]*z[43];
    z[50]= - 125823*z[6] + 110743*z[5];
    z[43]=n<T>(1,3016)*z[50] + z[43];
    z[50]=n<T>(1,2)*z[4];
    z[43]=z[43]*z[50];
    z[43]=z[43] + n<T>(1,3016)*z[48] + z[49];
    z[43]=z[4]*z[43];
    z[48]= - z[40] + n<T>(1,2)*z[6];
    z[49]=z[48]*z[6];
    z[51]=z[40] - z[6];
    z[52]=n<T>(1,4)*z[51] + 3*z[9];
    z[52]=z[9]*z[52];
    z[53]= - z[9] - z[51];
    z[53]=z[10]*z[53];
    z[52]=n<T>(85,16)*z[53] - n<T>(11,2)*z[49] + z[52];
    z[52]=z[10]*z[52];
    z[53]=4*z[6];
    z[54]=z[51]*z[53];
    z[55]= - z[10]*z[40];
    z[56]= - 3*z[40] + n<T>(4,3)*z[11];
    z[56]=z[11]*z[56];
    z[54]=z[56] + n<T>(1,3)*z[18] + z[54] + z[55];
    z[54]=z[11]*z[54];
    z[39]=z[39] - z[3];
    z[39]=z[40]*z[39];
    z[55]=n<T>(1,2)*z[18];
    z[39]=z[55] + z[39];
    z[39]=z[8]*z[39];
    z[46]= - n<T>(1,2)*z[46] - z[3];
    z[46]=z[15]*z[46];
    z[39]=z[39] + z[46];
    z[41]=103*z[49] + n<T>(122053,1508)*z[41];
    z[46]=n<T>(7,2)*z[9] - n<T>(122053,6032)*z[5] + n<T>(236661,6032)*z[40] - 19*
    z[6];
    z[46]=z[9]*z[46];
    z[41]=n<T>(1,2)*z[41] + z[46];
    z[41]=n<T>(7,4)*z[28] + n<T>(1,4)*z[41];
    z[41]=z[9]*z[41];
    z[46]=npow(z[6],3);
    z[49]= - 186143*z[48] - n<T>(140903,2)*z[5];
    z[49]=z[5]*z[6]*z[49];
    z[46]=5*z[46] + n<T>(1,754)*z[49];
    z[49]= - 73279*z[40] + n<T>(140903,4)*z[6];
    z[49]=z[6]*z[49];
    z[56]=73043*z[40];
    z[57]=z[56] - 129593*z[6];
    z[57]=n<T>(1,16)*z[57] + 8453*z[5];
    z[57]=z[5]*z[57];
    z[49]=n<T>(1,4)*z[49] + z[57];
    z[57]= - 106973*z[5] - 50423*z[40] + n<T>(140903,2)*z[6];
    z[57]=n<T>(1,3)*z[57] + n<T>(19321,2)*z[2];
    z[57]=z[2]*z[57];
    z[49]=n<T>(1,3)*z[49] + n<T>(1,16)*z[57];
    z[49]=z[2]*z[49];
    z[57]= - n<T>(568795,4)*z[2] - n<T>(383881,8)*z[6] + 31927*z[5];
    z[57]=n<T>(1,9)*z[57] - n<T>(84919,4)*z[9];
    z[57]=n<T>(122053,48)*z[3] + n<T>(1,4)*z[57] - 1669*z[7];
    z[57]= - n<T>(118283,36192)*z[4] + n<T>(1,377)*z[57] + n<T>(23,4)*z[10];
    z[55]=z[57]*z[55];
    z[57]=z[10] + 3*z[11];
    z[53]=z[53] + z[57];
    z[40]=z[40]*z[53];
    z[40]=4*z[18] + z[40];
    z[40]=z[12]*z[40];
    z[51]= - 4*z[51] + z[57];
    z[51]=z[17]*z[51];
    z[45]= - z[50] - z[45];
    z[45]=z[13]*z[45];
    z[44]= - z[44] - z[48];
    z[44]=z[16]*z[44];
    z[48]=z[31] - z[23];
    z[50]=z[34] + z[33];
    z[53]=z[38] - z[36];
    z[57]= - n<T>(231383,18096)*z[24] - n<T>(231383,36192)*z[22] + n<T>(77191,36192)
   *z[19];
    z[57]=i*z[57];
    z[56]= - z[56] - 140903*z[5];
    z[56]=n<T>(1,2)*z[56] + 106973*z[2];
    z[56]=z[14]*z[56];
    z[58]=z[7] + z[3];
    z[58]=z[27]*z[58];

    r +=  - n<T>(10085,12064)*z[20] - n<T>(231383,434304)*z[21] + n<T>(231383,36192)
      *z[25] + n<T>(65509,72384)*z[26] + n<T>(110743,24128)*z[29] + n<T>(45711,24128)*z[30]
     - n<T>(186143,72384)*z[32]
     - n<T>(125823,24128)*z[35]
     - n<T>(25,16)*z[37]
     + n<T>(38171,3016)*z[39]
     + z[40]
     + z[41]
     + n<T>(1,8)*z[42]
     + n<T>(1,4)*z[43]
     + n<T>(140903,9048)*z[44]
     + n<T>(5,4)*z[45]
     + n<T>(1,48)*z[46]
     + n<T>(1,754)*z[47] - n<T>(231383,72384)*z[48]
     + n<T>(1,377)*z[49]
     + n<T>(122053,24128)
      *z[50] + z[51] + z[52] - n<T>(39,16)*z[53] + z[54] + z[55] + n<T>(1,9048)
      *z[56] + z[57] + n<T>(41941,6032)*z[58];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf723(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf723(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
