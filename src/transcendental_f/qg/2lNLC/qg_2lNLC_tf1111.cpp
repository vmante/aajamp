#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1111(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[63];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[4];
    z[9]=d[16];
    z[10]=d[12];
    z[11]=d[17];
    z[12]=e[0];
    z[13]=e[4];
    z[14]=e[5];
    z[15]=e[7];
    z[16]=e[8];
    z[17]=e[9];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[30];
    z[27]=c[31];
    z[28]=e[1];
    z[29]=e[6];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[5];
    z[34]=f[11];
    z[35]=f[17];
    z[36]=f[18];
    z[37]=f[31];
    z[38]=f[32];
    z[39]=f[33];
    z[40]=f[34];
    z[41]=f[36];
    z[42]=f[37];
    z[43]=f[39];
    z[44]=f[43];
    z[45]=f[46];
    z[46]=f[47];
    z[47]=z[1]*i;
    z[48]= - z[47] + n<T>(1,2)*z[7];
    z[49]=z[48]*z[7];
    z[50]=2*z[47];
    z[51]=z[50] - n<T>(1,2)*z[6];
    z[51]=z[6]*z[51];
    z[52]=z[2]*z[47];
    z[53]= - n<T>(23,2)*z[4] - 55*z[47] + 23*z[7];
    z[53]=z[4]*z[53];
    z[54]=n<T>(13,12)*z[3] + n<T>(55,16)*z[4] - 2*z[2] - n<T>(23,16)*z[7] - n<T>(5,4)*
    z[47] - z[6];
    z[54]=z[3]*z[54];
    z[51]=z[54] + n<T>(1,8)*z[53] + 4*z[52] + z[51] - n<T>(23,8)*z[49];
    z[51]=z[3]*z[51];
    z[52]=npow(z[6],2);
    z[49]=769*z[52] + 1183*z[49];
    z[53]=207*z[6];
    z[54]=133*z[47] - z[53];
    z[55]= - n<T>(69,2)*z[2] + 101*z[7];
    z[54]=n<T>(1,2)*z[54] - z[55];
    z[54]=z[2]*z[54];
    z[53]=143*z[47] + z[53];
    z[53]=n<T>(1,2)*z[53] + z[55];
    z[53]=z[4]*z[53];
    z[49]=z[53] + n<T>(1,4)*z[49] + z[54];
    z[53]=z[4] - z[2];
    z[54]=z[47] - z[7];
    z[55]=749*z[6] - n<T>(503,2)*z[54];
    z[55]= - n<T>(1047,8)*z[5] + n<T>(1,8)*z[55] - 77*z[53];
    z[55]=z[5]*z[55];
    z[49]=n<T>(1,2)*z[49] + z[55];
    z[49]=z[5]*z[49];
    z[55]=85*z[47];
    z[56]= - n<T>(1247,4)*z[5] - 85*z[7] + z[55] - n<T>(419,2)*z[6] - n<T>(409,4)*
    z[53];
    z[56]=z[15]*z[56];
    z[49]=z[49] + z[56];
    z[56]=n<T>(3,2)*z[47] - z[6];
    z[56]=z[6]*z[56];
    z[57]= - n<T>(31,2)*z[7] + 31*z[47] - n<T>(101,8)*z[6];
    z[57]=z[7]*z[57];
    z[58]=73*z[47] + n<T>(271,2)*z[6];
    z[58]=n<T>(19,4)*z[2] + n<T>(1,2)*z[58] - 69*z[7];
    z[58]=z[2]*z[58];
    z[59]=z[4] - z[47] - z[6];
    z[59]=z[2] + z[7] + 3*z[59];
    z[59]=z[4]*z[59];
    z[56]=n<T>(23,8)*z[59] + n<T>(1,4)*z[58] + n<T>(69,8)*z[56] + z[57];
    z[56]=z[4]*z[56];
    z[57]=z[3] - z[2];
    z[58]=n<T>(631,2)*z[47];
    z[59]=n<T>(631,2)*z[7] - z[58] - 247*z[6];
    z[59]= - n<T>(205,64)*z[8] + n<T>(503,128)*z[5] + n<T>(1,192)*z[59] - 3*z[57];
    z[59]=z[8]*z[59];
    z[60]= - n<T>(439,96)*z[47] + z[6];
    z[60]=z[6]*z[60];
    z[58]=n<T>(631,4)*z[7] - z[58] + 439*z[6];
    z[58]=z[7]*z[58];
    z[58]=z[59] + z[60] + n<T>(1,96)*z[58];
    z[59]= - z[2]*z[6];
    z[60]=503*z[54] + n<T>(631,2)*z[5];
    z[60]=z[5]*z[60];
    z[50]=z[50] + z[6];
    z[50]=z[3]*z[50];
    z[50]=z[50] + n<T>(1,128)*z[60] + z[59] + n<T>(1,2)*z[58];
    z[50]=z[8]*z[50];
    z[58]=z[5] + z[4];
    z[59]= - z[47]*z[58];
    z[59]= - z[18] + z[59];
    z[59]=z[10]*z[59];
    z[58]= - z[47] + z[58];
    z[58]=z[14]*z[58];
    z[58]=z[59] + z[58];
    z[59]=z[8] + z[3];
    z[60]= - z[47]*z[59];
    z[60]= - z[18] + z[60];
    z[60]=z[9]*z[60];
    z[59]= - z[47] + z[59];
    z[59]=z[13]*z[59];
    z[59]=z[60] + z[59];
    z[60]=z[7] + z[6];
    z[60]=z[47]*z[60];
    z[60]=z[18] + z[60];
    z[60]=z[11]*z[60];
    z[54]=z[6] - z[54];
    z[54]=z[17]*z[54];
    z[54]=z[60] + z[54];
    z[60]= - n<T>(439,12)*z[47] - 69*z[6];
    z[52]=z[60]*z[52];
    z[60]= - 1045*z[7] + 631*z[47] - n<T>(439,2)*z[6];
    z[60]=z[7]*z[6]*z[60];
    z[52]=z[52] + n<T>(1,6)*z[60];
    z[60]= - n<T>(67,2)*z[47] - 17*z[6];
    z[60]=z[6]*z[60];
    z[55]=n<T>(85,2)*z[7] - z[55] + n<T>(101,4)*z[6];
    z[55]=z[7]*z[55];
    z[55]=n<T>(5,4)*z[60] + z[55];
    z[60]= - n<T>(159,64)*z[2] + n<T>(69,32)*z[7] + 5*z[47] - n<T>(69,64)*z[6];
    z[60]=z[2]*z[60];
    z[55]=n<T>(1,8)*z[55] + z[60];
    z[55]=z[2]*z[55];
    z[60]=n<T>(1,2)*z[2];
    z[47]=n<T>(1,2)*z[3] + z[60] - z[47];
    z[47]=z[12]*z[47];
    z[48]= - z[60] - z[48];
    z[48]=z[16]*z[48];
    z[60]=z[46] - z[40];
    z[61]= - n<T>(29,8)*z[24] - 5*z[22] + n<T>(1681,2304)*z[19];
    z[61]=i*z[61];
    z[62]= - n<T>(2713,2)*z[5] - n<T>(235,2)*z[4] + 685*z[2] + 367*z[6] + n<T>(4051,12)*z[7];
    z[62]= - n<T>(805,48)*z[8] + n<T>(1,16)*z[62] - 19*z[3];
    z[62]=z[18]*z[62];
    z[57]= - n<T>(823,192)*z[8] - z[5] - n<T>(631,192)*z[6] - z[57];
    z[57]=z[29]*z[57];
    z[53]= - z[28]*z[53];

    r +=  - n<T>(247,192)*z[20] - n<T>(343,192)*z[21] + n<T>(81,32)*z[23] + n<T>(29,16)
      *z[25] + n<T>(9,16)*z[26] - n<T>(8083,576)*z[27] + n<T>(25,8)*z[30] - n<T>(7,8)*
      z[31] + n<T>(55,16)*z[32] - n<T>(107,32)*z[33] - n<T>(31,8)*z[34] - n<T>(27,16)*
      z[35] + n<T>(19,24)*z[36] + n<T>(1639,256)*z[37] + n<T>(67,16)*z[38] + n<T>(621,32)*z[39]
     - n<T>(919,768)*z[41]
     - n<T>(9,8)*z[42]
     + n<T>(759,256)*z[43] - n<T>(61,32)*z[44] - n<T>(3,8)*z[45]
     + n<T>(15,2)*z[47]
     + n<T>(89,8)*z[48]
     + n<T>(1,16)*
      z[49] + z[50] + z[51] + n<T>(1,32)*z[52] + n<T>(9,32)*z[53] + n<T>(1459,192)*
      z[54] + z[55] + n<T>(1,4)*z[56] + z[57] + n<T>(69,16)*z[58] + 2*z[59] - n<T>(1,8)*z[60]
     + z[61]
     + n<T>(1,12)*z[62];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1111(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1111(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
