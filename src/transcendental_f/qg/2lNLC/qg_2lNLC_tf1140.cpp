#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1140(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[36];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[17];
    z[17]=c[31];
    z[18]=e[6];
    z[19]=f[31];
    z[20]=f[32];
    z[21]=f[33];
    z[22]=f[36];
    z[23]=f[39];
    z[24]=f[43];
    z[25]=f[68];
    z[26]=npow(z[4],2);
    z[27]=npow(z[3],2);
    z[28]=n<T>(3,2)*z[27] - z[26];
    z[29]=z[1]*i;
    z[30]= - n<T>(43,2)*z[3] + 9*z[4];
    z[30]=z[30]*z[29];
    z[31]=n<T>(43,4)*z[3] - 13*z[4];
    z[31]=z[8]*z[31];
    z[28]=z[31] + n<T>(9,2)*z[28] + z[30];
    z[28]=z[8]*z[28];
    z[30]=n<T>(29,2)*z[3] - n<T>(11,3)*z[4];
    z[30]=z[4]*z[30];
    z[30]=n<T>(19,2)*z[27] + z[30];
    z[30]=z[4]*z[30];
    z[26]= - n<T>(27,2)*z[27] - 7*z[26];
    z[31]=n<T>(1,2)*z[29];
    z[26]=z[26]*z[31];
    z[32]=npow(z[3],3);
    z[33]= - 3*z[7] - z[5] + n<T>(17,12)*z[8] - 15*z[3] + n<T>(7,3)*z[4];
    z[33]=z[13]*z[33];
    z[26]=z[33] + z[28] + z[26] - n<T>(113,6)*z[32] + z[30];
    z[28]= - z[4] + n<T>(3,2)*z[3];
    z[30]=z[28]*z[29];
    z[32]=n<T>(1,4)*z[8] - z[31] - z[28];
    z[32]=z[8]*z[32];
    z[28]= - z[7] + n<T>(1,2)*z[8] - z[31] + z[28];
    z[28]=z[7]*z[28];
    z[28]=n<T>(1,2)*z[28] + z[32] + n<T>(3,4)*z[27] + z[30];
    z[28]=z[7]*z[28];
    z[30]=z[7] + z[4];
    z[30]=z[18]*z[30];
    z[28]=z[20] + z[28] - z[30];
    z[30]=2*z[3];
    z[31]=z[30] + z[4];
    z[31]=z[31]*z[29];
    z[32]=z[4] - z[3];
    z[33]=z[8]*z[32];
    z[31]=z[31] - z[33];
    z[33]=z[29] - z[5];
    z[34]= - z[32] - n<T>(3,2)*z[33];
    z[34]=z[5]*z[34];
    z[35]=z[30] - n<T>(5,2)*z[4];
    z[35]=z[4]*z[35];
    z[31]=z[34] - 4*z[27] + z[35] + 2*z[31];
    z[31]=z[5]*z[31];
    z[30]=z[30] + z[5];
    z[34]= - z[29]*z[30];
    z[34]= - z[13] + z[34];
    z[34]=z[6]*z[34];
    z[30]= - z[29] + z[30];
    z[30]=z[10]*z[30];
    z[30]=z[34] + z[30];
    z[33]=z[33] - z[8];
    z[34]= - z[32]*z[33];
    z[35]= - z[3] - z[4];
    z[35]=z[4]*z[35];
    z[27]=2*z[27] + z[35] + z[34];
    z[32]= - z[2]*z[32];
    z[27]=2*z[27] + z[32];
    z[27]=z[2]*z[27];
    z[32]=z[8] + z[4];
    z[32]=z[29]*z[32];
    z[32]=z[13] + z[32];
    z[32]=z[9]*z[32];
    z[29]=z[4] - z[29] + z[8];
    z[29]=z[12]*z[29];
    z[29]=z[32] + z[29];
    z[32]=n<T>(3,2)*z[16] + n<T>(17,24)*z[14];
    z[32]=i*z[32];
    z[33]=z[33] + z[2];
    z[33]= - 23*z[3] - n<T>(35,2)*z[4] + 4*z[33];
    z[33]=z[11]*z[33];

    r +=  - n<T>(13,4)*z[15] - n<T>(21,8)*z[17] + n<T>(59,8)*z[19] + 19*z[21] - n<T>(9,8)*z[22]
     + n<T>(27,8)*z[23] - 2*z[24]
     + z[25]
     + n<T>(1,2)*z[26]
     + z[27]
       + n<T>(9,2)*z[28] + n<T>(17,2)*z[29] + 3*z[30] + z[31] + z[32] + z[33];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1140(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1140(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
