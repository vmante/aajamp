#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf308(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[16];
    z[10]=d[4];
    z[11]=d[6];
    z[12]=d[5];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[6];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[0];
    z[30]=e[3];
    z[31]=e[10];
    z[32]=e[7];
    z[33]=f[0];
    z[34]=f[1];
    z[35]=f[3];
    z[36]=f[4];
    z[37]=f[5];
    z[38]=f[11];
    z[39]=f[12];
    z[40]=f[16];
    z[41]=f[17];
    z[42]=f[18];
    z[43]=f[31];
    z[44]=z[1]*i;
    z[45]=5*z[44];
    z[46]=5*z[2];
    z[47]=3*z[4];
    z[48]=n<T>(1,6)*z[3] - z[46] - n<T>(7,2)*z[5] - z[45] + z[47];
    z[48]=z[3]*z[48];
    z[49]=n<T>(3,2)*z[4];
    z[50]= - z[44] - z[49];
    z[49]=z[50]*z[49];
    z[50]=npow(z[10],2);
    z[51]=n<T>(5,4)*z[50];
    z[52]= - n<T>(7,8)*z[5] - z[44] + n<T>(9,2)*z[4];
    z[52]=z[5]*z[52];
    z[53]=z[2]*z[44];
    z[48]=n<T>(1,4)*z[48] + n<T>(5,2)*z[53] + z[51] + z[49] + z[52];
    z[48]=z[3]*z[48];
    z[49]= - z[4] + n<T>(13,4)*z[44];
    z[52]= - z[49]*z[47];
    z[47]=n<T>(5,2)*z[5] - n<T>(41,4)*z[44] - z[47];
    z[47]=z[5]*z[47];
    z[45]=n<T>(11,4)*z[5] + z[45] + n<T>(9,4)*z[4];
    z[45]=n<T>(3,2)*z[45] - z[46];
    z[45]=z[2]*z[45];
    z[45]=z[45] - z[51] + z[52] + z[47];
    z[45]=z[2]*z[45];
    z[46]= - z[44] + n<T>(1,2)*z[4];
    z[46]=z[46]*z[4];
    z[47]=z[44] - z[4];
    z[51]=3*z[47] + z[10];
    z[51]=z[10]*z[51];
    z[51]=z[46] + z[51];
    z[52]= - n<T>(1,4)*z[6] - z[47];
    z[52]=z[6]*z[52];
    z[51]=n<T>(1,2)*z[51] + z[52];
    z[52]=15*z[47] + 11*z[10];
    z[52]=n<T>(1,4)*z[52] + z[6];
    z[52]=z[7]*z[52];
    z[51]=n<T>(11,2)*z[51] + z[52];
    z[51]=z[7]*z[51];
    z[52]= - n<T>(15,4)*z[6] - z[7];
    z[52]=z[31]*z[52];
    z[53]= - z[11] - z[12];
    z[53]=z[32]*z[53];
    z[45]=z[45] + z[48] + z[51] + z[52] + z[53] + z[43];
    z[48]=n<T>(37,2)*z[6];
    z[51]=11*z[4] + z[48];
    z[51]=z[44]*z[51];
    z[51]=11*z[20] + z[51];
    z[51]=z[8]*z[51];
    z[48]= - 11*z[47] + z[48];
    z[48]=z[15]*z[48];
    z[52]=9*z[5];
    z[53]= - n<T>(23,2)*z[2] + n<T>(41,2)*z[44] - z[52];
    z[53]=z[18]*z[53];
    z[54]= - n<T>(35,2)*z[3] - n<T>(7,3)*z[2] - n<T>(437,6)*z[10] + n<T>(17,3)*z[4]
     + 29
   *z[5];
    z[54]= - n<T>(13,3)*z[11] + n<T>(19,12)*z[7] + n<T>(1,4)*z[54] + n<T>(1,3)*z[6];
    z[54]=z[20]*z[54];
    z[48]=z[41] + z[54] + z[51] + z[48] + z[53];
    z[51]= - 53*z[44] + 89*z[4];
    z[51]= - n<T>(31,12)*z[10] + n<T>(1,4)*z[51] - z[52];
    z[51]=z[10]*z[51];
    z[52]=2*z[44];
    z[53]= - z[52] + z[5];
    z[53]=z[5]*z[53];
    z[46]=n<T>(1,4)*z[51] + n<T>(33,8)*z[46] + 2*z[53];
    z[46]=z[10]*z[46];
    z[47]=z[4]*z[47];
    z[51]= - z[44] - z[4];
    z[51]=n<T>(37,4)*z[51] - 13*z[10];
    z[51]=n<T>(1,4)*z[51] + 5*z[6];
    z[51]=z[6]*z[51];
    z[47]=z[51] + n<T>(11,4)*z[47] - 3*z[50];
    z[47]=z[6]*z[47];
    z[50]=z[44]*z[5];
    z[51]=z[44] - z[5];
    z[53]= - n<T>(1,2)*z[51] - z[10];
    z[53]=z[10]*z[53];
    z[54]=z[10] + z[51];
    z[54]=3*z[54] + n<T>(1,3)*z[11];
    z[54]=z[11]*z[54];
    z[53]=n<T>(1,4)*z[54] - z[50] + z[53];
    z[53]=z[11]*z[53];
    z[54]=z[3]*z[44];
    z[54]=z[54] + z[20];
    z[54]=z[9]*z[54];
    z[55]=z[44] - z[3];
    z[55]=z[16]*z[55];
    z[54]= - z[33] + z[54] + z[55];
    z[44]=15*z[44] - z[4];
    z[44]=z[4]*z[44];
    z[52]=z[52] - n<T>(45,16)*z[4];
    z[52]=z[5]*z[52];
    z[44]=n<T>(3,8)*z[44] + z[52];
    z[44]=z[5]*z[44];
    z[52]=n<T>(1,2)*z[12];
    z[55]= - z[52] - n<T>(1,2)*z[11] - z[51];
    z[55]=z[12]*z[55];
    z[56]=npow(z[11],2);
    z[55]=z[55] + n<T>(1,2)*z[56] - n<T>(1,3)*z[20];
    z[52]=z[55]*z[52];
    z[55]= - z[25] + z[37] + z[36];
    z[56]= - z[2] + z[3];
    z[56]=z[29]*z[56];
    z[56]=z[56] + z[34];
    z[57]= - 10*z[26] - n<T>(15,4)*z[24] + n<T>(23,48)*z[21];
    z[57]=i*z[57];
    z[49]= - n<T>(9,4)*z[2] + z[49];
    z[49]=z[14]*z[49];
    z[58]=z[12] + z[51];
    z[58]=z[17]*z[58];
    z[50]=z[50] + z[20];
    z[50]=z[13]*z[50];
    z[51]= - z[19]*z[51];
    z[59]=37*z[10] + 63*z[6];
    z[59]=z[30]*z[59];

    r +=  - n<T>(77,16)*z[22] + n<T>(5,16)*z[23] + 5*z[27] + n<T>(1681,96)*z[28]
     +  n<T>(3,8)*z[35] - n<T>(45,16)*z[38] - n<T>(89,16)*z[39] + n<T>(11,16)*z[40]
     - n<T>(5,12)*z[42]
     + z[44]
     + n<T>(1,2)*z[45]
     + z[46]
     + z[47]
     + n<T>(1,4)*z[48]
     + n<T>(3,2)*z[49]
     + z[50]
     + z[51]
     + z[52]
     + z[53]
     + n<T>(5,4)*z[54] - n<T>(15,8)*
      z[55] + n<T>(5,8)*z[56] + z[57] + z[58] + n<T>(1,8)*z[59];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf308(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf308(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
