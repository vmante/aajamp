#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1208(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[41];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=d[9];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[31];
    z[18]=e[6];
    z[19]=f[31];
    z[20]=f[32];
    z[21]=f[33];
    z[22]=f[36];
    z[23]=f[39];
    z[24]=f[43];
    z[25]=2*z[3];
    z[26]=z[25] + z[4];
    z[27]=z[1]*i;
    z[28]=z[26]*z[27];
    z[29]=8*z[8];
    z[30]=z[4] - z[3];
    z[31]=z[30]*z[29];
    z[32]=npow(z[3],2);
    z[33]= - 8*z[3] + z[4];
    z[33]=z[4]*z[33];
    z[34]=z[27] - z[5];
    z[34]=5*z[3] + 4*z[4] + 6*z[34];
    z[34]=z[5]*z[34];
    z[28]=z[34] + z[31] - 8*z[28] + 7*z[32] + z[33];
    z[28]=z[5]*z[28];
    z[31]=2*z[27];
    z[33]=2*z[4];
    z[34]=z[3] + z[33];
    z[34]=z[34]*z[31];
    z[35]=3*z[32];
    z[36]=npow(z[4],2);
    z[37]= - z[8]*z[3];
    z[34]=z[37] + z[34] + z[35] - 2*z[36];
    z[34]=z[8]*z[34];
    z[37]=2*z[7];
    z[38]=z[27] - z[8];
    z[33]= - z[33] + 3*z[3];
    z[39]= - z[37] + z[33] - z[38];
    z[39]=z[7]*z[39];
    z[40]=z[33]*z[31];
    z[33]= - z[27] - z[33];
    z[33]=2*z[33] + z[8];
    z[33]=z[8]*z[33];
    z[33]=z[39] + z[33] + z[35] + z[40];
    z[33]=z[33]*z[37];
    z[35]= - z[5] + z[38];
    z[35]=z[30]*z[35];
    z[37]=z[3] + z[4];
    z[37]=z[4]*z[37];
    z[35]= - 2*z[32] + z[37] + z[35];
    z[30]=z[2]*z[30];
    z[30]=2*z[35] + z[30];
    z[30]=z[2]*z[30];
    z[25]= - z[25] + n<T>(7,3)*z[4];
    z[25]=z[4]*z[25];
    z[25]= - z[32] + z[25];
    z[25]=z[4]*z[25];
    z[32]=z[32] + z[36];
    z[32]=z[32]*z[27];
    z[35]=npow(z[3],3);
    z[25]= - z[16] + 4*z[30] + z[33] + z[28] + 2*z[34] + 3*z[32] - n<T>(19,3)
   *z[35] + z[25];
    z[28]=2*z[5];
    z[26]= - 8*z[2] + z[28] + z[29] - z[31] - z[26];
    z[26]=z[11]*z[26];
    z[26]=z[26] - z[22];
    z[28]=z[28] + z[3];
    z[29]=z[27]*z[28];
    z[29]=2*z[13] + z[29];
    z[29]=z[6]*z[29];
    z[28]=z[31] - z[28];
    z[28]=z[10]*z[28];
    z[28]=z[23] + z[29] + z[28];
    z[29]=z[8] + z[4];
    z[27]= - z[27]*z[29];
    z[27]= - z[13] + z[27];
    z[27]=z[9]*z[27];
    z[29]= - z[4] + z[38];
    z[29]=z[12]*z[29];
    z[30]= - z[4] - z[7];
    z[30]=z[18]*z[30];
    z[27]=z[27] + z[29] + z[30] + z[24];
    z[29]=z[4] - n<T>(4,11)*z[8];
    z[29]= - z[14] - n<T>(16,33)*z[7] + n<T>(1,3)*z[29] + n<T>(8,11)*z[5];
    z[29]=z[13]*z[29];
    z[30]=z[15]*i;

    r += n<T>(279,11)*z[17] - n<T>(20,11)*z[19] - n<T>(18,11)*z[20] - n<T>(8,11)*z[21]
       + n<T>(2,11)*z[25] + n<T>(4,11)*z[26] + n<T>(16,11)*z[27] + n<T>(12,11)*z[28]
     +  z[29] - n<T>(19,33)*z[30];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1208(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1208(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
