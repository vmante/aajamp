#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf681(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[56];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[12];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[5];
    z[16]=e[8];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[19];
    z[21]=c[23];
    z[22]=c[25];
    z[23]=c[26];
    z[24]=c[28];
    z[25]=c[31];
    z[26]=e[10];
    z[27]=e[6];
    z[28]=f[3];
    z[29]=f[4];
    z[30]=f[5];
    z[31]=f[11];
    z[32]=f[12];
    z[33]=f[16];
    z[34]=f[17];
    z[35]=f[31];
    z[36]=f[36];
    z[37]=f[39];
    z[38]=n<T>(1,2)*z[5];
    z[39]=z[1]*i;
    z[40]=z[39]*z[38];
    z[41]=z[2] - z[5];
    z[42]= - z[39] + z[41];
    z[42]=z[2]*z[42];
    z[43]=z[39] - z[7];
    z[44]=z[43] + z[11];
    z[41]= - z[41] - n<T>(3,2)*z[44];
    z[44]=n<T>(1,2)*z[6];
    z[41]=z[41]*z[44];
    z[45]=2*z[39] - z[7];
    z[45]=z[7]*z[45];
    z[43]= - z[11]*z[43];
    z[40]=z[41] + z[43] + z[45] + z[40] + z[42];
    z[40]=z[6]*z[40];
    z[41]=z[38] - z[39];
    z[41]=z[41]*z[5];
    z[42]=npow(z[2],2);
    z[42]=n<T>(5755,2262)*z[41] + 3*z[42];
    z[43]=n<T>(1,2)*z[7];
    z[45]= - z[2] + z[43];
    z[45]=z[7]*z[45];
    z[46]=z[39] - z[5];
    z[47]=n<T>(1,2)*z[3] + z[46];
    z[47]=z[3]*z[47];
    z[48]=n<T>(5635,8)*z[46] + 1121*z[3];
    z[48]=z[8]*z[48];
    z[42]=n<T>(5,1131)*z[48] + n<T>(5605,1131)*z[47] + n<T>(1,2)*z[42] + 3*z[45];
    z[45]=n<T>(1,2)*z[8];
    z[42]=z[42]*z[45];
    z[47]=277*z[39] - n<T>(13295,2)*z[5];
    z[47]=z[5]*z[47];
    z[48]= - n<T>(13295,2)*z[7] + 8771*z[39] + 13295*z[5];
    z[48]=z[7]*z[48];
    z[47]=z[47] + z[48];
    z[48]=3*z[2];
    z[49]= - z[39]*z[48];
    z[50]=n<T>(1,2)*z[4];
    z[48]= - n<T>(8771,3016)*z[7] - n<T>(277,3016)*z[5] + z[48];
    z[48]=z[48]*z[50];
    z[47]=z[48] + z[49] + n<T>(1,3016)*z[47];
    z[47]=z[4]*z[47];
    z[48]=3493*z[3];
    z[49]=n<T>(1231,2)*z[5] - z[48];
    z[49]=z[39]*z[49];
    z[49]=n<T>(1231,2)*z[17] + z[49];
    z[49]=z[10]*z[49];
    z[48]= - n<T>(1231,2)*z[46] - z[48];
    z[48]=z[14]*z[48];
    z[51]=19127*z[39] - 12541*z[5];
    z[51]=n<T>(1,2)*z[51] - 3293*z[2];
    z[51]=z[13]*z[51];
    z[48]=z[48] + z[49] + z[51];
    z[45]= - z[45] - z[46];
    z[45]=z[8]*z[45];
    z[41]=z[41] - z[45];
    z[45]=z[43] - z[39];
    z[45]=z[7]*z[45];
    z[41]=z[45] - n<T>(5555,1508)*z[41];
    z[45]=477*z[39] + 5555*z[5];
    z[45]=n<T>(1,2)*z[11] - n<T>(5555,6032)*z[8] + n<T>(1,6032)*z[45] - z[7];
    z[45]=z[11]*z[45];
    z[41]=z[27] + n<T>(1,2)*z[41] + z[45];
    z[41]=z[11]*z[41];
    z[45]=37423*z[39] - n<T>(21589,2)*z[5];
    z[45]=z[5]*z[45];
    z[49]=n<T>(7967,4)*z[2] - n<T>(19327,2)*z[39] - 2287*z[5];
    z[49]=z[2]*z[49];
    z[45]=n<T>(1,8)*z[45] + z[49];
    z[49]= - n<T>(37423,8)*z[5] + 7967*z[2];
    z[49]=n<T>(1,1131)*z[49] + z[43];
    z[49]=z[49]*z[43];
    z[45]=n<T>(1,1131)*z[45] + z[49];
    z[45]=z[7]*z[45];
    z[49]=n<T>(2739,2)*z[2];
    z[51]= - z[39] + n<T>(1,2)*z[2];
    z[52]= - z[51]*z[49];
    z[53]= - n<T>(1231,2)*z[39] + 3493*z[5];
    z[49]=n<T>(1,3)*z[53] + z[49];
    z[49]=n<T>(1,2)*z[49] - n<T>(4549,3)*z[3];
    z[49]=z[3]*z[49];
    z[53]= - 3343*z[39] + n<T>(9979,4)*z[5];
    z[53]=z[5]*z[53];
    z[49]=z[49] + n<T>(1,3)*z[53] + z[52];
    z[49]=z[3]*z[49];
    z[52]= - 23651*z[39] + n<T>(26213,2)*z[5];
    z[52]=z[5]*z[52];
    z[53]= - 3946*z[2] + 9023*z[39] + n<T>(3293,4)*z[5];
    z[53]=z[2]*z[53];
    z[52]=n<T>(1,4)*z[52] + z[53];
    z[52]=z[2]*z[52];
    z[44]=z[44] - z[3];
    z[38]=z[38] + z[44];
    z[38]=z[39]*z[38];
    z[38]=n<T>(1,2)*z[17] + z[38];
    z[38]=z[9]*z[38];
    z[39]=n<T>(1,2)*z[46] - z[44];
    z[39]=z[15]*z[39];
    z[44]= - z[50] - z[51];
    z[44]=z[12]*z[44];
    z[43]= - z[43] - z[51];
    z[43]=z[16]*z[43];
    z[46]=z[30] - z[22];
    z[50]=z[33] + z[32];
    z[51]= - z[35] + z[37] - z[36];
    z[53]= - n<T>(64567,4524)*z[23] - n<T>(64567,9048)*z[21] + n<T>(12341,27144)*
    z[18];
    z[53]=i*z[53];
    z[54]= - n<T>(7817,8)*z[7] - n<T>(3343,2)*z[5] - 125*z[2];
    z[54]=n<T>(5555,4)*z[11] + n<T>(5455,12)*z[8] + n<T>(1,3)*z[54] - n<T>(2539,4)*z[3];
    z[54]= - n<T>(4247,3016)*z[4] + n<T>(1,377)*z[54] + n<T>(11,2)*z[6];
    z[54]=z[17]*z[54];
    z[55]= - z[3] - z[8];
    z[55]=z[26]*z[55];

    r += n<T>(5755,9048)*z[19] - n<T>(64567,108576)*z[20] + n<T>(64567,9048)*z[24]
       - n<T>(62905,18096)*z[25] - n<T>(277,6032)*z[28] - n<T>(28175,18096)*z[29]
       - n<T>(37423,18096)*z[31] - n<T>(8771,6032)*z[34] + z[38] + z[39] + 
      z[40] + z[41] + z[42] + n<T>(7967,1131)*z[43] + 3*z[44] + z[45] - 
      n<T>(64567,18096)*z[46] + z[47] + n<T>(1,2262)*z[48] + n<T>(1,754)*z[49] - 
      n<T>(5555,6032)*z[50] - n<T>(1,4)*z[51] + n<T>(1,1131)*z[52] + z[53] + n<T>(1,6)*
      z[54] + n<T>(5605,2262)*z[55];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf681(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf681(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
