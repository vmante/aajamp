#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf691(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[54];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[8];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[31];
    z[24]=e[10];
    z[25]=e[6];
    z[26]=f[3];
    z[27]=f[4];
    z[28]=f[5];
    z[29]=f[11];
    z[30]=f[12];
    z[31]=f[16];
    z[32]=f[17];
    z[33]=f[31];
    z[34]=f[36];
    z[35]=f[39];
    z[36]=z[1]*i;
    z[37]=z[36]*z[6];
    z[38]=npow(z[6],2);
    z[39]= - z[37] + n<T>(1,2)*z[38];
    z[40]=n<T>(1,2)*z[2];
    z[41]=n<T>(6581,6032)*z[2] - z[6] - n<T>(3565,3016)*z[36];
    z[41]=z[41]*z[40];
    z[42]= - n<T>(39413,18096)*z[7] - z[40] + n<T>(21317,18096)*z[5] + z[6] - 
   n<T>(30365,18096)*z[36];
    z[42]=z[7]*z[42];
    z[43]=z[6] + n<T>(21317,9048)*z[36];
    z[43]=n<T>(1,2)*z[43] - n<T>(1403,1131)*z[5];
    z[43]=z[5]*z[43];
    z[44]=n<T>(100831,36192)*z[3] - n<T>(66557,36192)*z[7] - n<T>(549,12064)*z[2]
     - 
   n<T>(17137,18096)*z[5] - z[6] + n<T>(3565,36192)*z[36];
    z[44]=z[3]*z[44];
    z[41]=z[44] + z[42] + z[41] + z[43] + z[39];
    z[41]=z[3]*z[41];
    z[42]= - n<T>(80129,2)*z[5] + 80129*z[6] - 205*z[36];
    z[42]=z[5]*z[42];
    z[42]=z[42] - n<T>(80129,2)*z[38] + 57509*z[37];
    z[43]=19*z[2];
    z[44]= - z[36]*z[43];
    z[45]=205*z[5] - 57509*z[6];
    z[43]=n<T>(1,3016)*z[45] + z[43];
    z[45]=n<T>(1,2)*z[4];
    z[43]=z[43]*z[45];
    z[42]=z[43] + n<T>(1,3016)*z[42] + z[44];
    z[42]=z[4]*z[42];
    z[43]=n<T>(46909,4)*z[2] + n<T>(98591,8)*z[6] + n<T>(12775,3)*z[5];
    z[43]= - n<T>(11105,8)*z[3] + n<T>(1,2)*z[43] - n<T>(24839,3)*z[7];
    z[43]= - n<T>(22825,48)*z[4] + n<T>(1,3)*z[43] - n<T>(7357,8)*z[9];
    z[43]=n<T>(1,377)*z[43] + n<T>(9,2)*z[10];
    z[43]=z[15]*z[43];
    z[42]=z[42] + z[43];
    z[43]=n<T>(23579,36192)*z[5] - z[6] - n<T>(23579,18096)*z[36];
    z[43]=z[5]*z[43];
    z[44]=n<T>(15,8)*z[2] + z[5] - n<T>(7,4)*z[6] - 3*z[36];
    z[44]=z[2]*z[44];
    z[46]= - z[2] + n<T>(163823,36192)*z[5] - z[6] - n<T>(91439,36192)*z[36];
    z[46]=z[7]*z[46];
    z[43]=z[46] + z[44] + n<T>(11,8)*z[38] + z[43];
    z[44]=n<T>(1,2)*z[7];
    z[43]=z[43]*z[44];
    z[46]=z[36] - z[6];
    z[47]= - 3*z[46] + z[9];
    z[47]=z[9]*z[47];
    z[46]= - z[9] - z[46];
    z[46]=z[10]*z[46];
    z[46]=n<T>(15,16)*z[46] + n<T>(1,4)*z[47] - z[38] + 2*z[37];
    z[46]=z[10]*z[46];
    z[47]=17137*z[3];
    z[48]=n<T>(14531,2)*z[5] + z[47];
    z[48]=z[36]*z[48];
    z[48]=n<T>(14531,2)*z[15] + z[48];
    z[48]=z[8]*z[48];
    z[49]=z[36] - z[5];
    z[47]= - n<T>(14531,2)*z[49] + z[47];
    z[47]=z[13]*z[47];
    z[47]=z[48] + z[47];
    z[37]=n<T>(12757,4)*z[38] - 7321*z[37];
    z[48]= - 817*z[6] - 1445*z[36];
    z[48]=n<T>(33,8)*z[48] + 3323*z[5];
    z[48]=z[5]*z[48];
    z[50]=n<T>(12757,2)*z[6] + 29467*z[36];
    z[50]= - n<T>(236551,6)*z[2] + 3*z[50] + 10739*z[5];
    z[50]=z[2]*z[50];
    z[37]=n<T>(1,8)*z[50] + n<T>(3,2)*z[37] + z[48];
    z[37]=z[2]*z[37];
    z[44]=z[44] + z[49];
    z[44]=z[7]*z[44];
    z[48]= - z[36] + n<T>(1,2)*z[5];
    z[48]=z[5]*z[48];
    z[44]=z[48] + z[44];
    z[44]=13*z[39] + n<T>(22071,1508)*z[44];
    z[48]=z[7] - z[5];
    z[48]= - z[6] + n<T>(19421,12064)*z[36] + n<T>(7357,12064)*z[48];
    z[49]=n<T>(5,4)*z[9];
    z[48]=3*z[48] + z[49];
    z[48]=z[9]*z[48];
    z[44]=n<T>(1,4)*z[44] + z[48];
    z[44]=z[9]*z[44];
    z[40]= - z[45] + z[36] - z[40];
    z[40]=z[11]*z[40];
    z[45]=50701*z[36] - 29223*z[5];
    z[45]=n<T>(1,2)*z[45] - 10739*z[2];
    z[45]=z[12]*z[45];
    z[48]= - n<T>(38271,2)*z[2] - n<T>(41287,2)*z[6] + 39779*z[36];
    z[48]=z[14]*z[48];
    z[45]=z[45] + z[48];
    z[36]=z[38]*z[36];
    z[38]=z[5]*z[6];
    z[38]= - 101607*z[39] - n<T>(26207,2)*z[38];
    z[38]=z[5]*z[38];
    z[39]= - z[33] + z[35] - z[34];
    z[48]=z[28] - z[20];
    z[50]=z[31] + z[30];
    z[51]= - n<T>(158911,6032)*z[21] - n<T>(158911,12064)*z[19] + n<T>(73687,108576)
   *z[16];
    z[51]=i*z[51];
    z[52]=npow(z[6],3);
    z[53]=n<T>(66557,18096)*z[3] + n<T>(48461,18096)*z[7] - z[6] + z[2];
    z[53]=z[24]*z[53];
    z[49]=z[25]*z[49];

    r +=  - n<T>(48805,36192)*z[17] - n<T>(158911,144768)*z[18] + n<T>(158911,12064)
      *z[22] - n<T>(36419,24128)*z[23] + n<T>(205,24128)*z[26] + n<T>(163823,72384)
      *z[27] - n<T>(101607,24128)*z[29] - n<T>(57509,24128)*z[32] + z[36] + n<T>(1,754)*z[37]
     + n<T>(1,12064)*z[38] - n<T>(5,16)*z[39]
     + n<T>(19,4)*z[40]
     +  z[41] + n<T>(1,4)*z[42] + z[43] + n<T>(1,2)*z[44] + n<T>(1,3016)*z[45] + 
      z[46] + n<T>(1,9048)*z[47] - n<T>(158911,24128)*z[48] + z[49] + n<T>(22071,24128)*z[50]
     + z[51]
     + n<T>(1,16)*z[52]
     + z[53];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf691(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf691(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
