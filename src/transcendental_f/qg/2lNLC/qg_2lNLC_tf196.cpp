#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf196(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[48];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[19];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[31];
    z[22]=d[6];
    z[23]=d[9];
    z[24]=e[10];
    z[25]=f[3];
    z[26]=f[4];
    z[27]=f[5];
    z[28]=f[11];
    z[29]=f[12];
    z[30]=f[16];
    z[31]=f[17];
    z[32]=z[1]*i;
    z[33]=z[32] - z[3];
    z[34]=7*z[6];
    z[35]= - 2*z[33] + z[34];
    z[35]=z[6]*z[35];
    z[36]=z[32]*z[3];
    z[37]=npow(z[3],2);
    z[38]=2*z[32];
    z[39]= - n<T>(11,2)*z[5] + n<T>(7,2)*z[6] - z[3] + z[38];
    z[39]=z[5]*z[39];
    z[35]=z[39] + z[35] + n<T>(5,2)*z[37] - z[36];
    z[35]=z[5]*z[35];
    z[39]=2*z[3];
    z[40]=z[39]*z[32];
    z[41]=z[33]*z[34];
    z[41]=z[41] - z[37] + z[40];
    z[41]=z[6]*z[41];
    z[42]=7*z[4];
    z[39]= - z[42] - z[39] + 23*z[32];
    z[39]=z[4]*z[39];
    z[43]=n<T>(1,2)*z[37];
    z[44]=z[43] - z[36];
    z[45]=n<T>(11,2)*z[2] - n<T>(7,2)*z[4] + z[3] - 14*z[32];
    z[45]=z[2]*z[45];
    z[39]=z[45] - 5*z[44] + z[39];
    z[39]=z[2]*z[39];
    z[45]=4*z[3];
    z[46]= - z[45] - z[5];
    z[46]=z[32]*z[46];
    z[46]= - 4*z[13] + z[46];
    z[46]=z[7]*z[46];
    z[45]=z[2] + z[45] - 5*z[32];
    z[45]=z[10]*z[45];
    z[42]=10*z[2] - 17*z[32] + z[42];
    z[42]=z[12]*z[42];
    z[47]=4*z[33] - z[5];
    z[47]=z[11]*z[47];
    z[34]= - z[34] - 10*z[5];
    z[34]=z[24]*z[34];
    z[34]=z[45] + z[47] + z[42] + z[34] + z[41] + z[35] + z[39] + z[46];
    z[35]=z[4] - z[3] - z[38];
    z[35]=z[4]*z[35];
    z[38]=n<T>(1,6)*z[8] - z[3] + n<T>(1,2)*z[4];
    z[38]=z[8]*z[38];
    z[35]=z[38] + z[35] + z[43] + z[40];
    z[35]=z[8]*z[35];
    z[38]= - z[6] - z[33];
    z[38]=z[6]*z[38];
    z[33]= - n<T>(1,6)*z[9] - n<T>(1,2)*z[6] - z[33];
    z[33]=z[9]*z[33];
    z[33]=z[33] + z[38] - z[44];
    z[33]=z[9]*z[33];
    z[36]=z[37] - 11*z[36];
    z[32]=n<T>(7,3)*z[3] - z[32];
    z[32]=z[4]*z[32];
    z[32]=n<T>(1,3)*z[36] + z[32];
    z[32]=z[4]*z[32];
    z[36]=z[8] + z[9];
    z[37]= - z[3] - 5*z[4];
    z[37]=2*z[37] + 13*z[6];
    z[37]=n<T>(11,6)*z[2] + n<T>(1,3)*z[37] - n<T>(1,2)*z[5];
    z[36]=n<T>(1,3)*z[37] + n<T>(1,4)*z[36];
    z[36]=z[13]*z[36];
    z[37]=npow(z[4],2);
    z[38]=2*z[4] - z[2];
    z[38]=z[2]*z[38];
    z[37]= - z[37] + z[38];
    z[37]=z[22]*z[37];
    z[38]=npow(z[6],2);
    z[39]= - 2*z[6] + z[5];
    z[39]=z[5]*z[39];
    z[38]=z[38] + z[39];
    z[38]=z[23]*z[38];
    z[39]= - z[28] - z[27] + z[26] + z[18];
    z[40]=n<T>(22,3)*z[19] + n<T>(11,3)*z[17] - n<T>(5,18)*z[14];
    z[40]=i*z[40];

    r += n<T>(5,6)*z[15] + n<T>(11,36)*z[16] - n<T>(11,3)*z[20] - n<T>(35,12)*z[21]
     - 
      z[25] - z[29] - z[30] + z[31] + z[32] + z[33] + n<T>(1,3)*z[34] + 
      z[35] + z[36] + z[37] + z[38] - n<T>(11,6)*z[39] + z[40];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf196(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf196(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
