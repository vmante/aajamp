#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1431(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[70];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[16];
    z[10]=d[15];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=e[10];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[17];
    z[21]=c[19];
    z[22]=c[21];
    z[23]=c[23];
    z[24]=c[25];
    z[25]=c[26];
    z[26]=c[28];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=f[0];
    z[30]=f[1];
    z[31]=f[2];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[6];
    z[36]=f[7];
    z[37]=f[8];
    z[38]=f[9];
    z[39]=f[10];
    z[40]=f[11];
    z[41]=f[12];
    z[42]=f[13];
    z[43]=f[14];
    z[44]=f[15];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[23];
    z[49]=f[28];
    z[50]=f[29];
    z[51]=f[76];
    z[52]=17*z[3];
    z[53]=z[1]*i;
    z[54]=n<T>(1,2)*z[53];
    z[55]=n<T>(1,4)*z[5] - z[54] - z[52];
    z[55]=z[5]*z[55];
    z[56]= - z[53] + n<T>(11,2)*z[3];
    z[56]=z[3]*z[56];
    z[55]=z[56] + z[55];
    z[56]=z[53] + z[3];
    z[57]=z[56] - z[5];
    z[58]=z[2]*z[57];
    z[55]=n<T>(1,6)*z[55] + z[58];
    z[58]=n<T>(1,2)*z[5];
    z[59]=n<T>(1,4)*z[4] + 9*z[53] + z[58];
    z[59]=z[4]*z[59];
    z[60]=z[4] - z[2];
    z[61]=z[53] - z[5];
    z[62]=z[3] - n<T>(1,8)*z[61];
    z[60]=n<T>(17,3)*z[6] - n<T>(3,8)*z[8] + n<T>(1,3)*z[62] - n<T>(17,2)*z[60];
    z[62]=n<T>(1,2)*z[6];
    z[60]=z[60]*z[62];
    z[63]= - n<T>(3,16)*z[8] - z[3] - n<T>(11,8)*z[61];
    z[63]=z[8]*z[63];
    z[64]= - z[4] + z[56];
    z[64]=z[7]*z[64];
    z[55]=z[60] + z[64] + z[59] + n<T>(1,2)*z[55] + z[63];
    z[55]=z[55]*z[62];
    z[59]=n<T>(35,8)*z[5];
    z[60]=z[59] - n<T>(35,4)*z[53] - z[3];
    z[60]=z[60]*z[58];
    z[62]=15*z[53];
    z[63]=n<T>(5,4)*z[2];
    z[64]=z[63] - n<T>(11,2)*z[5] + z[62] - z[3];
    z[65]=n<T>(1,2)*z[2];
    z[64]=z[64]*z[65];
    z[66]=n<T>(5,2)*z[53] - z[3];
    z[66]=z[3]*z[66];
    z[67]=z[53] - z[3];
    z[67]= - z[8]*z[67];
    z[60]=z[67] + z[64] + z[66] + z[60];
    z[64]=n<T>(1,2)*z[3];
    z[59]=z[59] + z[53] + z[64];
    z[59]=n<T>(11,12)*z[4] - n<T>(1,2)*z[8] + n<T>(1,4)*z[59] - z[2];
    z[59]=z[4]*z[59];
    z[59]=n<T>(1,2)*z[60] + z[59];
    z[59]=z[4]*z[59];
    z[60]=n<T>(1,8)*z[3];
    z[66]=z[53] - n<T>(5,2)*z[3];
    z[66]=z[66]*z[60];
    z[67]= - n<T>(73,32)*z[5] + n<T>(73,16)*z[53] + z[3];
    z[67]=z[5]*z[67];
    z[68]= - n<T>(7,4)*z[53] - z[3];
    z[68]= - n<T>(19,8)*z[2] + n<T>(5,2)*z[68] + 7*z[5];
    z[68]=z[2]*z[68];
    z[56]= - n<T>(11,16)*z[8] + n<T>(1,4)*z[2] + n<T>(5,8)*z[56] - z[5];
    z[56]=z[8]*z[56];
    z[56]=z[56] + z[68] + z[66] + z[67];
    z[66]=n<T>(1,2)*z[4];
    z[67]= - z[53] - z[5];
    z[67]=n<T>(13,16)*z[4] + n<T>(13,8)*z[67] + z[8];
    z[67]=z[67]*z[66];
    z[68]=z[8] - z[3];
    z[69]=n<T>(1,3)*z[7] + n<T>(13,4)*z[4] + n<T>(17,6)*z[2] - n<T>(73,12)*z[5] - z[53]
    - n<T>(11,6)*z[68];
    z[69]=z[7]*z[69];
    z[56]=n<T>(1,8)*z[69] + n<T>(1,3)*z[56] + z[67];
    z[56]=z[7]*z[56];
    z[67]= - 3*z[53] + n<T>(5,6)*z[3];
    z[64]=z[67]*z[64];
    z[54]= - n<T>(1,6)*z[5] + z[54] + n<T>(7,3)*z[3];
    z[54]=z[5]*z[54];
    z[54]=z[64] + z[54];
    z[54]=z[5]*z[54];
    z[64]=z[68] + z[5];
    z[64]= - n<T>(55,6)*z[7] - n<T>(37,6)*z[2] + n<T>(55,3)*z[53] - 3*z[64];
    z[64]=z[15]*z[64];
    z[62]=z[62] - n<T>(37,9)*z[3];
    z[62]=z[62]*npow(z[3],2);
    z[54]=z[64] + n<T>(1,4)*z[62] + z[54];
    z[62]=11*z[53] + n<T>(17,2)*z[3];
    z[60]=z[62]*z[60];
    z[62]=n<T>(1,4)*z[3];
    z[64]= - z[5] + 2*z[53] - z[62];
    z[64]=z[5]*z[64];
    z[60]=z[60] + z[64];
    z[52]= - 23*z[5] + 73*z[53] + z[52];
    z[52]=n<T>(1,3)*z[52] - 3*z[2];
    z[52]=z[2]*z[52];
    z[52]=n<T>(1,3)*z[60] + n<T>(1,16)*z[52];
    z[52]=z[2]*z[52];
    z[60]= - 7*z[53] - n<T>(3,2)*z[3];
    z[60]=z[60]*z[62];
    z[62]= - n<T>(19,16)*z[5] + n<T>(19,8)*z[53] + z[3];
    z[62]=z[5]*z[62];
    z[60]=z[60] + z[62];
    z[57]= - n<T>(1,8)*z[2] - z[57];
    z[57]=z[2]*z[57];
    z[62]=n<T>(11,3)*z[2] + 11*z[3] + n<T>(19,2)*z[61];
    z[62]=n<T>(1,4)*z[62] - n<T>(5,9)*z[8];
    z[62]=z[8]*z[62];
    z[57]=n<T>(1,4)*z[62] + n<T>(1,2)*z[60] + n<T>(1,3)*z[57];
    z[57]=z[8]*z[57];
    z[60]= - z[3] - z[5];
    z[60]=z[53]*z[60];
    z[60]= - z[17] + z[60];
    z[60]=z[10]*z[60];
    z[62]= - z[3] + z[61];
    z[62]=z[13]*z[62];
    z[60]=z[60] + z[62];
    z[62]=n<T>(9,2)*z[6] + n<T>(7,2)*z[4] + z[2] - z[68];
    z[64]= - z[53]*z[62];
    z[64]= - n<T>(9,2)*z[17] + z[64];
    z[64]=z[9]*z[64];
    z[62]= - n<T>(9,2)*z[53] + z[62];
    z[62]=z[14]*z[62];
    z[61]=z[63] - 4*z[3] + n<T>(5,4)*z[61];
    z[61]= - n<T>(5,12)*z[7] + n<T>(1,3)*z[61] - n<T>(7,4)*z[8];
    z[61]=z[16]*z[61];
    z[53]=z[65] - z[53];
    z[63]=z[66] + z[53];
    z[63]=z[11]*z[63];
    z[53]=z[58] + z[53];
    z[53]=z[12]*z[53];
    z[58]=n<T>(373,9)*z[2] + n<T>(89,9)*z[3] + n<T>(3,2)*z[5];
    z[58]= - n<T>(5,18)*z[6] - n<T>(11,9)*z[7] + n<T>(137,12)*z[4] + n<T>(1,2)*z[58]
     + 
   n<T>(37,9)*z[8];
    z[58]=z[17]*z[58];
    z[58]= - z[49] + z[58] + z[50];
    z[65]=z[38] - z[35];
    z[66]=n<T>(31,12)*z[25] - n<T>(97,48)*z[23] + z[22] + z[20] + n<T>(113,288)*
    z[18];
    z[66]=i*z[66];
    z[67]=z[3] + z[6];
    z[67]=z[28]*z[67];

    r += n<T>(115,24)*z[19] - n<T>(385,144)*z[21] + n<T>(3,16)*z[24] - n<T>(61,24)*
      z[26] - n<T>(53,36)*z[27] + n<T>(17,4)*z[29] - n<T>(15,8)*z[30] - n<T>(7,12)*
      z[31] + n<T>(43,32)*z[32] - n<T>(77,96)*z[33] + n<T>(41,24)*z[34] - n<T>(5,3)*
      z[36] - 3*z[37] - n<T>(5,12)*z[39] - n<T>(85,96)*z[40] - n<T>(1,96)*z[41] - 
      n<T>(13,8)*z[42] - n<T>(5,8)*z[43] + n<T>(3,8)*z[44] + n<T>(1,32)*z[45] - n<T>(3,32)*
      z[46] + n<T>(17,12)*z[47] + n<T>(5,24)*z[48] - 4*z[51] + z[52] + n<T>(11,12)*
      z[53] + n<T>(1,4)*z[54] + z[55] + z[56] + z[57] + n<T>(1,8)*z[58] + z[59]
       + n<T>(19,24)*z[60] + z[61] + z[62] + n<T>(29,4)*z[63] + z[64] + n<T>(9,16)*
      z[65] + z[66] + n<T>(7,24)*z[67];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1431(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1431(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
