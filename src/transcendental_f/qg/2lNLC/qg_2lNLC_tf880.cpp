#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf880(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[116];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=e[0];
    z[10]=e[1];
    z[11]=e[8];
    z[12]=d[15];
    z[13]=d[5];
    z[14]=d[4];
    z[15]=d[6];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=e[2];
    z[19]=e[5];
    z[20]=e[9];
    z[21]=f[0];
    z[22]=f[3];
    z[23]=f[5];
    z[24]=f[8];
    z[25]=f[11];
    z[26]=f[13];
    z[27]=f[17];
    z[28]=f[20];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[18];
    z[33]=c[19];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=c[29];
    z[39]=c[30];
    z[40]=c[31];
    z[41]=c[47];
    z[42]=c[51];
    z[43]=c[54];
    z[44]=c[55];
    z[45]=c[59];
    z[46]=c[61];
    z[47]=c[62];
    z[48]=c[66];
    z[49]=c[68];
    z[50]=c[69];
    z[51]=c[72];
    z[52]=c[75];
    z[53]=c[83];
    z[54]=e[3];
    z[55]=e[10];
    z[56]=e[6];
    z[57]=e[7];
    z[58]=e[13];
    z[59]=e[14];
    z[60]=f[4];
    z[61]=f[12];
    z[62]=f[16];
    z[63]=f[31];
    z[64]=f[36];
    z[65]=f[39];
    z[66]=f[51];
    z[67]=f[52];
    z[68]=f[55];
    z[69]=f[58];
    z[70]=g[0];
    z[71]=g[3];
    z[72]=g[5];
    z[73]=g[8];
    z[74]=g[11];
    z[75]=g[13];
    z[76]=g[17];
    z[77]=g[21];
    z[78]=g[29];
    z[79]=g[30];
    z[80]=g[35];
    z[81]=g[41];
    z[82]=g[42];
    z[83]=g[43];
    z[84]=g[56];
    z[85]=g[57];
    z[86]=g[76];
    z[87]=g[94];
    z[88]=g[107];
    z[89]=g[125];
    z[90]=n<T>(1,4)*z[3];
    z[91]= - n<T>(103265,1508) + 113*z[3];
    z[91]=z[91]*z[90];
    z[92]=z[2]*z[1];
    z[93]=z[3]*z[1];
    z[94]= - n<T>(20,3)*z[92] - n<T>(109319,6032)*z[1] + n<T>(1117,3)*z[93];
    z[94]=i*z[94];
    z[95]=n<T>(1,2)*z[5];
    z[96]=i*z[1];
    z[97]=n<T>(845,18)*z[5] - n<T>(697,2)*z[96] - n<T>(1361,3)*z[2] + n<T>(109319,6032)
    + n<T>(887,3)*z[3];
    z[97]=z[97]*z[95];
    z[98]= - z[15] + n<T>(115,2)*z[10];
    z[99]=n<T>(3,2)*z[6];
    z[100]=n<T>(1,2)*z[7];
    z[101]= - n<T>(202,3)*z[2] + n<T>(30903,3016) + n<T>(268,3)*z[3];
    z[101]=z[2]*z[101];
    z[91]=z[97] + z[94] + z[101] + z[91] - z[100] + z[99] - n<T>(11999,36)*
    z[29] + 6*z[9] + n<T>(658,3)*z[11] + z[98];
    z[91]=z[5]*z[91];
    z[94]=n<T>(4595,6)*z[93] - 117*z[92];
    z[97]=n<T>(1,2)*i;
    z[94]=z[94]*z[97];
    z[101]=n<T>(41459,3016) - n<T>(4883,3)*z[3];
    z[101]=n<T>(1097,6)*z[5] - n<T>(3893,6)*z[96] + n<T>(1,2)*z[101] + n<T>(1517,3)*z[2]
   ;
    z[95]=z[101]*z[95];
    z[101]= - 27*z[11] + z[15] - 237*z[10];
    z[102]=n<T>(103265,12064) - 103*z[3];
    z[102]=z[3]*z[102];
    z[103]= - n<T>(1121,3)*z[2] - n<T>(36181,3016) + n<T>(1618,3)*z[3];
    z[103]=z[2]*z[103];
    z[104]= - n<T>(1643,12)*z[4] + n<T>(887,3)*z[5] + n<T>(1643,3)*z[96] + n<T>(41,3)*
    z[2] - n<T>(1,2) - n<T>(928,3)*z[3];
    z[104]=z[4]*z[104];
    z[94]=n<T>(1,3)*z[104] + z[95] + z[94] + z[103] + z[102] - n<T>(25625,12064)
   *z[7] + n<T>(27133,3016)*z[6] + n<T>(33839,144)*z[29] - n<T>(88939,12064)*z[14]
    + n<T>(1,2)*z[101] - 3*z[9];
    z[94]=z[4]*z[94];
    z[95]=n<T>(2021,3)*z[9];
    z[101]=n<T>(3,2)*z[7];
    z[102]=npow(z[3],2);
    z[103]=n<T>(3085,9)*z[2] - n<T>(18861,1508) - n<T>(1003,3)*z[3];
    z[103]=z[2]*z[103];
    z[103]=n<T>(1,2)*z[103] - n<T>(74,3)*z[102] - z[101] + z[13] - n<T>(713,36)*
    z[29] + z[95] + n<T>(1343,6)*z[11] + z[15] + n<T>(847,3)*z[10];
    z[103]=z[2]*z[103];
    z[104]=n<T>(88939,6032)*z[14];
    z[105]=z[104] + z[8];
    z[106]= - z[105] + n<T>(31657,3016)*z[6];
    z[107]=z[16] - n<T>(27133,1508)*z[12];
    z[107]=n<T>(31657,6032)*z[7] - n<T>(3,2)*z[13] - n<T>(4042,3)*z[9] - n<T>(1316,3)*
    z[11] + n<T>(1201,3)*z[10] + n<T>(1,2)*z[107] - z[15] - z[106];
    z[107]=z[1]*z[107];
    z[108]= - n<T>(46027,1508)*z[1] + 501*z[93];
    z[108]=z[108]*z[90];
    z[109]=n<T>(1403,3)*z[92] + n<T>(79181,3016)*z[1] - n<T>(2353,3)*z[93];
    z[109]=z[2]*z[109];
    z[107]=z[109] + z[108] + z[107] - n<T>(8933,72)*z[30] + 1585*z[36] + 166
   *z[34];
    z[107]=i*z[107];
    z[108]=n<T>(1,2)*z[3];
    z[109]=n<T>(46027,6032) + n<T>(463,9)*z[3];
    z[109]=z[109]*z[108];
    z[110]=z[7] - z[8];
    z[95]=z[109] + n<T>(8621,36)*z[29] + z[95] + n<T>(45,2)*z[11] - n<T>(3019,6)*
    z[10] + z[110];
    z[95]=z[3]*z[95];
    z[109]=npow(z[8],2);
    z[111]=n<T>(1,2)*z[109];
    z[112]=npow(z[6],2);
    z[113]=npow(z[14],2);
    z[114]=n<T>(813,2)*z[38] + 373*z[39] - z[19] - n<T>(27133,1508)*z[18];
    z[115]= - z[13]*z[15];
    z[106]= - n<T>(101025,12064)*z[7] - z[106];
    z[106]=z[7]*z[106];
    z[91]=z[94] + z[91] + z[107] + z[103] + z[95] + z[106] + z[115] + 
   n<T>(16599,3016)*z[112] - n<T>(22049,9048)*z[29] - n<T>(19637,12064)*z[113] + 
    z[111] + n<T>(39951,3016)*z[10] - n<T>(463,3)*z[22] + n<T>(760,3)*z[23] + n<T>(112,3)
   *z[25] + 200*z[27] + 631*z[21] - 9*z[24] + n<T>(211,6)*z[26] - n<T>(631,3)*
    z[28] - n<T>(271,4)*z[31] + n<T>(577,12)*z[33] - n<T>(679,3)*z[35] - 1017*z[37]
    - n<T>(93943,36)*z[40] + n<T>(1,2)*z[114] - 271*z[32];
    z[91]=z[4]*z[91];
    z[94]=n<T>(115,9)*z[2] - n<T>(36181,6032) - n<T>(617,3)*z[3];
    z[94]=z[2]*z[94];
    z[94]=z[94] + n<T>(311,2)*z[102] + n<T>(23,4)*z[7] - z[99] + n<T>(70075,72)*
    z[29] + 105*z[9] + n<T>(155,3)*z[11] - z[8] + z[98];
    z[94]=z[2]*z[94];
    z[95]=5*z[17];
    z[98]= - n<T>(19,2)*z[13] + z[99] + n<T>(67,4)*z[14] - 168*z[9] - n<T>(133,3)*
    z[11] - 115*z[10] - z[95] - 3*z[15];
    z[98]=z[1]*z[98];
    z[99]= - n<T>(59533,1508)*z[1] - 1735*z[93];
    z[90]=z[99]*z[90];
    z[99]=n<T>(2073,754)*z[1] - n<T>(35,3)*z[93];
    z[99]=11*z[99] + n<T>(3125,6)*z[92];
    z[99]=z[2]*z[99];
    z[90]=z[99] + z[90] + z[98] - n<T>(6725,36)*z[30] - 230*z[36] - n<T>(141,2)*
    z[34];
    z[90]=i*z[90];
    z[98]=1081*z[92] - 3*z[1] - n<T>(1465,2)*z[93];
    z[97]=z[98]*z[97];
    z[98]= - n<T>(11,2) + 107*z[3];
    z[98]=n<T>(1,2)*z[98] - n<T>(583,3)*z[2];
    z[96]= - n<T>(791,6)*z[5] + n<T>(1,2)*z[98] + n<T>(1582,3)*z[96];
    z[96]=z[5]*z[96];
    z[98]=n<T>(97233,12064) + 61*z[3];
    z[98]=z[3]*z[98];
    z[99]= - n<T>(1717,12)*z[2] - n<T>(42967,3016) + n<T>(209,3)*z[3];
    z[99]=z[2]*z[99];
    z[96]=n<T>(1,3)*z[96] + z[97] + z[99] + z[98] - n<T>(17,8)*z[7] + n<T>(19,4)*
    z[13] - n<T>(3,4)*z[6] + n<T>(5327,18)*z[29] - n<T>(67,8)*z[14] - 15*z[9] - 132*
    z[11] + 5*z[15] + n<T>(241,2)*z[10];
    z[96]=z[5]*z[96];
    z[97]=n<T>(65565,12064) + n<T>(128,3)*z[3];
    z[97]=z[3]*z[97];
    z[97]=z[97] - n<T>(50531,72)*z[29] + 87*z[9] + n<T>(112,3)*z[11] - 241*z[10]
    - z[110];
    z[97]=z[3]*z[97];
    z[98]=5*z[20];
    z[99]=n<T>(241,6)*z[28];
    z[103]= - z[14] - n<T>(69,8)*z[13];
    z[103]=z[13]*z[103];
    z[106]= - z[6] + z[100];
    z[106]=z[7]*z[106];
    z[90]=z[96] + z[90] + z[94] + z[97] + n<T>(7,2)*z[106] + z[103] + n<T>(5,2)*
    z[112] + n<T>(29417,36192)*z[29] + n<T>(37,4)*z[113] + n<T>(45229,3016)*z[11]
     - 
   n<T>(101,4)*z[22] + 74*z[23] - n<T>(151,4)*z[25] + n<T>(111,4)*z[27] + n<T>(145,2)*
    z[21] - 282*z[24] + 16*z[26] - z[99] - n<T>(57,4)*z[31] + 15*z[33] - n<T>(109,6)*z[35]
     + 80*z[37] - n<T>(113365,72)*z[40] - 57*z[32]
     + n<T>(171,4)*z[38]
    - z[98] + n<T>(431,2)*z[39];
    z[90]=z[5]*z[90];
    z[94]=n<T>(1,2)*z[19];
    z[96]= - 3*z[17] + n<T>(5,2)*z[15];
    z[96]=z[15]*z[96];
    z[97]= - 5533*z[12] - n<T>(2517,4)*z[6];
    z[97]=z[6]*z[97];
    z[103]=n<T>(1,2)*z[16];
    z[106]=n<T>(53,8)*z[13] + z[14] - z[8] + z[103] + 2*z[15];
    z[106]=z[13]*z[106];
    z[105]=n<T>(94993,12064)*z[7] + n<T>(37689,3016)*z[6] - z[105];
    z[105]=z[7]*z[105];
    z[96]=z[105] + z[106] + n<T>(3,1508)*z[97] - n<T>(91955,12064)*z[113] - n<T>(35,2)*z[9]
     - n<T>(42213,1508)*z[11]
     - z[111]
     - n<T>(77673,3016)*z[10]
     + z[96]
    + n<T>(2735,12)*z[22] - n<T>(1447,3)*z[23] - n<T>(793,12)*z[25] - n<T>(1469,4)*
    z[27] - 796*z[21] + 288*z[24] - n<T>(496,3)*z[26] + n<T>(964,3)*z[28] + 
   n<T>(27133,3016)*z[18] + z[98] + z[94];
    z[96]=z[1]*z[96];
    z[97]=35*z[1] + 617*z[93];
    z[97]=z[97]*z[108];
    z[92]= - n<T>(20485,18)*z[92] - n<T>(132715,3016)*z[1] + n<T>(1571,3)*z[93];
    z[92]=z[2]*z[92];
    z[93]=n<T>(3136,3)*z[30] + n<T>(3155,2)*z[36] + 533*z[34];
    z[98]=3*z[7] + n<T>(30193,3016)*z[6] - n<T>(4013,3)*z[9] - n<T>(2123,3)*z[11]
     - 
   n<T>(1012,3)*z[10] + z[8];
    z[98]=z[1]*z[98];
    z[92]=z[92] + z[97] + n<T>(1,3)*z[93] + z[98];
    z[92]=z[2]*z[92];
    z[93]=z[7] - z[13];
    z[97]= - 4042*z[9] + 2125*z[10] - 242*z[11];
    z[97]= - n<T>(337,18)*z[102] + n<T>(1,3)*z[97] - 2*z[93];
    z[97]=z[1]*z[97];
    z[98]=n<T>(8279,24)*z[30] + n<T>(2477,2)*z[36] + 413*z[34];
    z[97]=n<T>(1,3)*z[98] + z[97];
    z[97]=z[3]*z[97];
    z[98]=11141*z[48] + n<T>(17287,3)*z[45];
    z[98]=n<T>(1,2)*z[98] - n<T>(3136,3)*z[41];
    z[92]=z[92] + z[97] + z[96] - n<T>(90491,18096)*z[30] + n<T>(214879,6032)*
    z[34] + n<T>(1,3)*z[98] + n<T>(214879,3016)*z[36];
    z[92]=i*z[92];
    z[96]=npow(z[15],2);
    z[97]=965*z[39] + n<T>(653,2)*z[38];
    z[97]=z[96] + n<T>(192157,72)*z[40] + n<T>(1,2)*z[97] - n<T>(653,3)*z[32];
    z[98]=z[6] + z[100];
    z[98]=z[98]*z[101];
    z[100]=n<T>(8423,4)*z[29] + 1675*z[9] + 215*z[10] + 431*z[11];
    z[101]= - n<T>(37,2) - n<T>(1231,9)*z[3];
    z[101]=z[3]*z[101];
    z[100]=n<T>(1,3)*z[100] + z[101];
    z[100]=z[100]*z[108];
    z[101]=n<T>(73,2)*z[9] - 5*z[10] + 47*z[11];
    z[101]=23*z[101] - n<T>(24523,4)*z[29];
    z[105]=1 - n<T>(1405,6)*z[3];
    z[105]=z[3]*z[105];
    z[101]=z[105] - n<T>(29,4)*z[7] - z[13] + n<T>(1,3)*z[101] - n<T>(25669,3016)*
    z[6];
    z[105]=n<T>(8335,12)*z[2] + n<T>(336317,6032) + n<T>(250,3)*z[3];
    z[105]=z[2]*z[105];
    z[101]=n<T>(1,2)*z[101] + n<T>(1,3)*z[105];
    z[101]=z[2]*z[101];
    z[105]=z[8] - z[15];
    z[106]=z[13]*z[105];
    z[97]=z[101] + z[100] + z[98] + z[106] + n<T>(25669,6032)*z[112] - n<T>(30681,6032)*z[29]
     + n<T>(33,4)*z[9]
     + n<T>(39197,3016)*z[11]
     + n<T>(18861,1508)*z[10]
    - n<T>(137,6)*z[22] + n<T>(239,3)*z[23] + n<T>(101,3)*z[25] + 78*z[27] + n<T>(37,2)
   *z[21] + 60*z[24] + n<T>(565,6)*z[26] - n<T>(92,3)*z[28] - n<T>(653,24)*z[31]
     + 
   n<T>(1043,36)*z[33] - n<T>(1121,12)*z[35] - n<T>(575,3)*z[37] + n<T>(1,2)*z[97];
    z[97]=z[2]*z[97];
    z[98]=npow(z[13],2);
    z[100]=n<T>(39951,1508)*z[6] + z[8] - z[104];
    z[100]=n<T>(1,3)*z[7] + n<T>(1,2)*z[100] + z[13];
    z[100]=z[7]*z[100];
    z[100]=z[100] - n<T>(1,2)*z[98] + n<T>(37689,6032)*z[112] + n<T>(59555,9048)*
    z[29] - n<T>(58779,12064)*z[113] - n<T>(42213,3016)*z[55] - z[109];
    z[100]=z[7]*z[100];
    z[101]=2*z[57];
    z[104]=n<T>(1,2)*z[13] + n<T>(53,8)*z[14] - z[105];
    z[104]=z[13]*z[104];
    z[94]=z[104] - n<T>(25,6)*z[29] - n<T>(15,4)*z[113] - z[94] - z[58] + z[101]
   ;
    z[94]=z[13]*z[94];
    z[104]= - 17*z[11] + n<T>(205,2)*z[9];
    z[104]=3*z[104] - n<T>(6053,72)*z[29];
    z[93]=n<T>(337,72)*z[102] + n<T>(1,2)*z[104] + z[93];
    z[93]=z[3]*z[93];
    z[102]=npow(z[7],2);
    z[98]=z[98] - z[102];
    z[102]= - 53*z[39] + n<T>(1,2)*z[38];
    z[102]=n<T>(4481,72)*z[40] + n<T>(1,2)*z[102] - n<T>(1,3)*z[32];
    z[93]=z[93] - n<T>(50551,36192)*z[29] + n<T>(37,4)*z[9] - n<T>(51,2)*z[22] + n<T>(226,3)*z[23]
     + n<T>(197,6)*z[25]
     + n<T>(123,2)*z[27]
     + 74*z[21] - 57*z[24]
     + 20
   *z[26] - z[99] - n<T>(5,24)*z[31] + n<T>(143,9)*z[33] - n<T>(1409,12)*z[35] + n<T>(5,2)*z[102]
     - n<T>(977,3)*z[37]
     + 2*z[98];
    z[93]=z[3]*z[93];
    z[98]=13*z[54];
    z[99]= - n<T>(291077,9048)*z[112] + n<T>(30193,9048)*z[29] + 13*z[113] - 
   n<T>(16599,754)*z[18] - n<T>(37689,1508)*z[55] - z[98];
    z[99]=z[6]*z[99];
    z[98]= - n<T>(11,2)*z[113] - 11*z[56] - z[98];
    z[98]=z[14]*z[98];
    z[98]= - z[98] - z[99] + z[67] - z[69];
    z[96]= - n<T>(3,2)*z[96] + z[101] - 3*z[20];
    z[96]=z[15]*z[96];
    z[95]=n<T>(70865,6032)*z[14] - n<T>(50575,24)*z[9] - n<T>(3859,6)*z[11] + n<T>(2,3)*
    z[8] + n<T>(2789,8)*z[10] - n<T>(27133,3016)*z[12] - z[95] + z[103];
    z[95]=z[29]*z[95];
    z[99]=z[23] - z[35];
    z[101]=2*z[89] - n<T>(1,2)*z[88];
    z[102]=npow(z[10],2);
    z[103]=n<T>(1,6)*z[109] + z[59] - z[58];
    z[103]=z[8]*z[103];
    z[104]=npow(z[11],2);
    z[105]=npow(z[9],2);

    r += n<T>(46027,12064)*z[22] + n<T>(109319,12064)*z[25] + n<T>(59533,12064)*
      z[27] + n<T>(81443,6032)*z[31] + n<T>(214879,72384)*z[33] - n<T>(214879,6032)
      *z[37] - n<T>(457001,12064)*z[40] + n<T>(653,24)*z[42] + n<T>(653,6)*z[43]
     +  n<T>(71483,72)*z[44] - n<T>(1243,9)*z[46] + n<T>(14411,6)*z[47] - n<T>(437,4)*
      z[49] + n<T>(395,8)*z[50] - n<T>(653,8)*z[51] - n<T>(965,4)*z[52] - n<T>(192157,144)*z[53]
     - n<T>(101025,12064)*z[60]
     + n<T>(19637,12064)*z[61] - n<T>(58779,12064)*z[62] - n<T>(35,8)*z[63] - n<T>(3,8)*z[64]
     + n<T>(19,8)*z[65]
     + z[66]
       + z[68] - n<T>(71,4)*z[70] - n<T>(201,4)*z[71] + n<T>(21,8)*z[72] + n<T>(1451,4)
      *z[73] - n<T>(1517,4)*z[74] - n<T>(45,4)*z[75] + n<T>(81,4)*z[76] - n<T>(259,12)*
      z[77] + n<T>(848,3)*z[78] + n<T>(2513,8)*z[79] - n<T>(1511,12)*z[80] - n<T>(337,12)*z[81]
     - n<T>(9,4)*z[82]
     + n<T>(63,2)*z[83]
     + n<T>(13,2)*z[84] - n<T>(19,2)*
      z[85] + n<T>(23,2)*z[86] - n<T>(89,4)*z[87] + z[90] + z[91] + z[92] + 
      z[93] + z[94] + z[95] + z[96] + z[97] - n<T>(1,2)*z[98] + n<T>(214879,12064)*z[99]
     + z[100]
     + 3*z[101] - n<T>(149,2)*z[102]
     + z[103]
     + 33*
      z[104] - n<T>(63,8)*z[105];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf880(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf880(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
