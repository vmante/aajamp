#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1195(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[105];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[16];
    z[13]=d[9];
    z[14]=d[11];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[10];
    z[26]=e[11];
    z[27]=e[12];
    z[28]=c[3];
    z[29]=c[11];
    z[30]=c[15];
    z[31]=c[17];
    z[32]=c[19];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[31];
    z[38]=e[3];
    z[39]=e[6];
    z[40]=e[13];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[3];
    z[45]=f[4];
    z[46]=f[5];
    z[47]=f[6];
    z[48]=f[7];
    z[49]=f[11];
    z[50]=f[12];
    z[51]=f[14];
    z[52]=f[16];
    z[53]=f[17];
    z[54]=f[18];
    z[55]=f[23];
    z[56]=f[31];
    z[57]=f[32];
    z[58]=f[33];
    z[59]=f[34];
    z[60]=f[36];
    z[61]=f[37];
    z[62]=f[39];
    z[63]=f[43];
    z[64]=f[51];
    z[65]=f[52];
    z[66]=f[55];
    z[67]=f[56];
    z[68]=f[58];
    z[69]=f[76];
    z[70]=f[105];
    z[71]=f[106];
    z[72]= - z[17] + n<T>(6355,216)*z[16];
    z[73]=n<T>(1,3)*z[3];
    z[74]=z[72] + z[73] + z[13];
    z[75]=n<T>(45,16)*z[4];
    z[76]=n<T>(1,4)*z[8];
    z[77]=n<T>(1,3)*z[9];
    z[78]=n<T>(33,4)*z[2];
    z[74]= - z[76] - n<T>(17749,864)*z[6] - z[78] - n<T>(3743,864)*z[10] + z[75]
    + n<T>(4391,432)*z[7] + n<T>(1,2)*z[74] + z[77];
    z[74]=z[8]*z[74];
    z[79]=3*z[3];
    z[80]=n<T>(7,2)*z[9];
    z[81]=z[11] - n<T>(17,3)*z[15];
    z[81]= - n<T>(395,36)*z[5] + n<T>(87,8)*z[8] + n<T>(1505,216)*z[6] + n<T>(9,4)*z[2]
    - n<T>(7,6)*z[10] - n<T>(63,8)*z[4] + n<T>(2845,216)*z[7] - z[80] + n<T>(5,3)*z[81]
    + z[79];
    z[82]=n<T>(1,2)*z[5];
    z[81]=z[81]*z[82];
    z[83]=n<T>(5,6)*z[11];
    z[84]=2*z[12];
    z[85]=z[83] - z[84];
    z[85]=z[3]*z[85];
    z[86]=n<T>(2,9)*z[14];
    z[87]=z[86] + n<T>(15,8)*z[12];
    z[88]=n<T>(1,4)*z[7];
    z[89]= - n<T>(163,72)*z[4] - z[88] - n<T>(16,9)*z[9] - n<T>(1,2)*z[13] + z[87];
    z[89]=z[4]*z[89];
    z[90]=n<T>(1,2)*z[10];
    z[80]= - n<T>(10403,864)*z[10] - n<T>(13,4)*z[4] - n<T>(5363,216)*z[7] + z[80]
    + n<T>(5,3)*z[3] + z[15] + n<T>(15,2)*z[12];
    z[80]=z[80]*z[90];
    z[91]=n<T>(1,2)*z[2];
    z[92]=z[9] - z[3];
    z[93]= - n<T>(2629,72)*z[7] + z[92];
    z[93]=n<T>(27,4)*z[2] + n<T>(1,3)*z[93] + n<T>(9,4)*z[4];
    z[93]=z[93]*z[91];
    z[94]=13*z[9];
    z[95]= - n<T>(499,6)*z[15] - z[13];
    z[95]= - n<T>(3527,192)*z[6] + n<T>(2143,144)*z[2] + n<T>(4175,96)*z[10] + n<T>(11,2)
   *z[4] + n<T>(1,2)*z[95] + z[94];
    z[95]=z[6]*z[95];
    z[96]=n<T>(15,8)*z[20] + n<T>(2,9)*z[27];
    z[97]=n<T>(1,2)*z[26];
    z[98]=n<T>(33,4)*z[23];
    z[99]=n<T>(1,3)*z[25];
    z[86]=z[13]*z[86];
    z[100]= - n<T>(55,72)*z[9] - n<T>(1,2)*z[17] - n<T>(23,9)*z[13];
    z[100]=z[9]*z[100];
    z[84]=n<T>(227,288)*z[7] + n<T>(6355,432)*z[16] + z[84];
    z[84]=z[7]*z[84];
    z[74]=z[81] + z[74] + n<T>(1,3)*z[95] + z[93] + z[80] + z[89] + z[84] + 
    z[100] + z[85] + z[86] + n<T>(103,54)*z[22] - z[99] - n<T>(55,8)*z[15] - n<T>(85,18)*z[21]
     - n<T>(9,4)*z[18]
     - n<T>(5,6)*z[19]
     + z[98] - n<T>(6355,432)*z[24] - 
    z[97] + z[96];
    z[74]=z[1]*z[74];
    z[80]= - 2*z[35] - n<T>(9,8)*z[33];
    z[74]=z[74] + n<T>(2611,5184)*z[29] + 3*z[80] - n<T>(1,2)*z[31];
    z[74]=i*z[74];
    z[77]=z[77] - z[73];
    z[80]=n<T>(3,2)*z[2];
    z[81]=n<T>(2629,216)*z[7];
    z[84]=5*z[13];
    z[85]=z[80] - z[77] + z[81] - z[84];
    z[85]=z[85]*z[91];
    z[86]= - n<T>(7,4)*z[9] + n<T>(7,3)*z[3];
    z[89]=n<T>(11,8) + z[13];
    z[75]=n<T>(245,36)*z[5] + n<T>(21,16)*z[8] + n<T>(709,108)*z[6] + z[80] + n<T>(1,12)
   *z[10] - z[75] - n<T>(1123,108)*z[7] + 5*z[89] - z[86];
    z[75]=z[75]*z[82];
    z[80]= - z[92] + n<T>(491,36)*z[7];
    z[80]=n<T>(1,3)*z[80];
    z[78]=z[80] + z[78];
    z[82]=n<T>(45,8)*z[4];
    z[89]= - n<T>(87,16)*z[8] + n<T>(491,108)*z[6] + z[82] - z[78];
    z[89]=z[8]*z[89];
    z[81]=z[81] + 5*z[4];
    z[92]= - n<T>(2215,108)*z[6] - n<T>(415,216)*z[2] + z[81];
    z[92]=z[6]*z[92];
    z[89]=z[92] + z[89];
    z[92]=n<T>(1,2)*z[4];
    z[93]= - z[84] + n<T>(63,16)*z[4];
    z[93]=z[93]*z[92];
    z[95]=n<T>(1,2)*z[9];
    z[73]=z[73] - z[95];
    z[73]=n<T>(31,12)*z[10] + 7*z[73] + z[7];
    z[73]=z[73]*z[90];
    z[90]=npow(z[9],2);
    z[100]=npow(z[13],2);
    z[101]=n<T>(5,4)*z[100];
    z[102]=npow(z[3],2);
    z[103]=npow(z[7],2);
    z[104]=z[19] + n<T>(17,3)*z[21];
    z[104]= - n<T>(431,36)*z[28] - n<T>(2147,144)*z[22] + n<T>(5,2)*z[104] + z[25];
    z[73]=z[75] + z[85] + z[73] + z[93] - n<T>(893,108)*z[103] - n<T>(1,8)*z[90]
    - n<T>(7,12)*z[102] + n<T>(1,3)*z[104] + z[101] + n<T>(1,2)*z[89];
    z[73]=z[5]*z[73];
    z[75]=z[9] + z[3];
    z[85]=n<T>(1,6)*z[9];
    z[75]=z[75]*z[85];
    z[75]=z[75] - z[99];
    z[80]=z[80] + n<T>(33,8)*z[2];
    z[80]=z[80]*z[91];
    z[77]=z[8] + n<T>(17749,432)*z[6] + n<T>(33,2)*z[2] + n<T>(3743,432)*z[10] - 
    z[82] - n<T>(199,4)*z[7] - z[13] + z[77];
    z[76]=z[77]*z[76];
    z[77]= - z[98] + z[26] + n<T>(6355,216)*z[24];
    z[82]=npow(z[4],2);
    z[85]=4391*z[7] + n<T>(3743,4)*z[10];
    z[85]=z[10]*z[85];
    z[89]=n<T>(4175,8)*z[6] - n<T>(4175,4)*z[10] - n<T>(491,3)*z[2];
    z[89]=z[6]*z[89];
    z[76]=z[76] + n<T>(1,72)*z[89] + z[80] + n<T>(1,432)*z[85] - n<T>(45,32)*z[82]
    - n<T>(4391,864)*z[103] + n<T>(1,12)*z[102] + n<T>(8893,5184)*z[28] - n<T>(89,27)*
    z[22] + n<T>(1,2)*z[77] - z[75];
    z[76]=z[8]*z[76];
    z[77]=z[84] - n<T>(9,8)*z[4];
    z[77]=z[77]*z[92];
    z[78]=z[2]*z[78];
    z[80]= - 11*z[23] + 3*z[18];
    z[82]=npow(z[10],2);
    z[75]= - n<T>(1,4)*z[78] + n<T>(9,16)*z[82] + z[77] - n<T>(491,216)*z[103] - n<T>(1,3)*z[102]
     - z[101]
     + n<T>(89,48)*z[28]
     + n<T>(2747,432)*z[22]
     + n<T>(3,8)*z[80]
    + z[75];
    z[75]=z[2]*z[75];
    z[77]= - n<T>(4591,8)*z[28] + 499*z[21] - n<T>(19129,12)*z[22];
    z[78]= - 13 + z[13];
    z[78]=z[13]*z[78];
    z[77]=n<T>(1,9)*z[77] + z[78];
    z[78]=n<T>(167,216)*z[2] + z[84] - z[81];
    z[78]=z[78]*z[91];
    z[80]=2*z[13];
    z[81]= - z[80] + n<T>(1,2)*z[3];
    z[81]=z[3]*z[81];
    z[82]=5 + n<T>(17,12)*z[9];
    z[82]=z[9]*z[82];
    z[85]=3*z[13];
    z[89]=z[85] + n<T>(12119,432)*z[7];
    z[89]=z[7]*z[89];
    z[91]= - n<T>(11,4)*z[4] - 7*z[13] - z[94];
    z[91]=z[4]*z[91];
    z[92]= - z[13] + n<T>(4607,576)*z[10];
    z[92]=z[10]*z[92];
    z[93]= - n<T>(3,4) - n<T>(7,3)*z[13];
    z[93]= - n<T>(50303,2592)*z[6] + n<T>(313,54)*z[2] + n<T>(4391,576)*z[10] + n<T>(10,3)*z[4]
     + n<T>(5839,864)*z[7]
     + n<T>(13,6)*z[9]
     + n<T>(1,4)*z[93]
     + z[3];
    z[93]=z[6]*z[93];
    z[77]=z[93] + z[78] + z[92] + n<T>(1,3)*z[91] + n<T>(1,2)*z[89] + z[82] + n<T>(1,4)*z[77]
     + z[81];
    z[77]=z[6]*z[77];
    z[78]= - n<T>(4931,432)*z[7] - z[85] - z[86];
    z[78]= - n<T>(5165,864)*z[10] + n<T>(1,2)*z[78] + 2*z[4];
    z[78]=z[10]*z[78];
    z[80]=z[80] - n<T>(7,12)*z[3];
    z[80]=z[3]*z[80];
    z[81]= - z[13] - n<T>(1,8)*z[7];
    z[81]=z[7]*z[81];
    z[82]= - n<T>(17,16)*z[4] - 2*z[3] + n<T>(7,4)*z[7];
    z[82]=z[4]*z[82];
    z[78]=z[78] + z[82] + z[81] + n<T>(7,8)*z[90] + z[80] + z[101] - n<T>(9385,2592)*z[28]
     - n<T>(15,4)*z[20]
     - n<T>(1,2)*z[21]
     + n<T>(7,6)*z[38]
     + z[40] - 
   n<T>(4607,432)*z[39];
    z[78]=z[10]*z[78];
    z[80]= - z[13] - n<T>(325,162)*z[7];
    z[80]=z[80]*z[88];
    z[81]=2*z[20];
    z[82]= - 4607*z[39] + 6355*z[24];
    z[80]=z[80] - n<T>(1,4)*z[100] + n<T>(3101,648)*z[28] - n<T>(7165,216)*z[22]
     + n<T>(1,432)*z[82] - z[81];
    z[80]=z[7]*z[80];
    z[82]=23*z[13] + 8*z[9];
    z[82]=z[9]*z[82];
    z[85]=n<T>(1,18)*z[4] - n<T>(7,8)*z[7] + n<T>(8,9)*z[9] + n<T>(5,36)*z[13] + z[3];
    z[85]=z[4]*z[85];
    z[82]=z[85] + n<T>(1,8)*z[103] + n<T>(1,9)*z[82] - n<T>(971,288)*z[28] + n<T>(9,8)*
    z[18] - z[96];
    z[82]=z[4]*z[82];
    z[72]= - n<T>(85,18)*z[15] + z[83] + n<T>(1,2)*z[72] + z[87];
    z[72]=z[28]*z[72];
    z[83]=7*z[38] + 5*z[19];
    z[81]=n<T>(41,36)*z[102] - n<T>(17,72)*z[28] + n<T>(7,6)*z[25] + n<T>(1,6)*z[83]
     + 
    z[81];
    z[81]=z[3]*z[81];
    z[79]= - n<T>(37,9)*z[13] - z[79];
    z[79]=n<T>(1,2)*z[79] - n<T>(7,3)*z[9];
    z[79]=z[79]*z[95];
    z[79]=z[79] - z[84] - n<T>(97,54)*z[28] + n<T>(3,2)*z[25] - n<T>(23,9)*z[41]
     + 
    z[97];
    z[79]=z[9]*z[79];
    z[83]= - z[65] + z[47] - z[67] + n<T>(17,3)*z[68] + 15*z[69] + z[71] + 
    z[70];
    z[84]=z[54] - z[61];
    z[85]= - 23*z[41] - 2*z[27];
    z[85]=z[13]*z[85];

    r += n<T>(55,8)*z[21] - n<T>(3607,432)*z[30] + n<T>(11,32)*z[32] + n<T>(33,16)*
      z[34] + 3*z[36] + n<T>(175,24)*z[37] + n<T>(9,8)*z[42] - n<T>(9,16)*z[43] + 
      n<T>(63,32)*z[44] + n<T>(1,24)*z[45] - n<T>(45,16)*z[46] + n<T>(4,3)*z[48]
     - n<T>(87,32)*z[49]
     - n<T>(19,24)*z[50]
     + n<T>(1,2)*z[51]
     + n<T>(7,8)*z[52] - n<T>(45,32)*
      z[53] - n<T>(1,6)*z[55] + n<T>(21677,1728)*z[56] + n<T>(253,24)*z[57] + n<T>(5623,216)*z[58]
     - n<T>(3,4)*z[59]
     - n<T>(3743,1728)*z[60]
     + n<T>(4679,576)*z[62]
       - n<T>(491,216)*z[63] - n<T>(2,3)*z[64] - n<T>(8,9)*z[66] + z[72] + z[73]
     +  z[74] + z[75] + z[76] + z[77] + z[78] + z[79] + z[80] + z[81] + 
      z[82] + n<T>(1,4)*z[83] + n<T>(3,8)*z[84] + n<T>(1,9)*z[85];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1195(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1195(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
