#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf500(
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[10];
  std::complex<T> r(0,0);

    z[1]=g[79];
    z[2]=g[82];
    z[3]=g[90];
    z[4]=g[92];
    z[5]=g[101];
    z[6]=g[110];
    z[7]=g[112];
    z[8]=g[113];
    z[9]=z[4] - z[3] + z[1] - z[2];
    z[9]=z[7] + n<T>(1,2)*z[6] + n<T>(3,2)*z[9] - z[5];

    r += z[8] + n<T>(1,2)*z[9];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf500(
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf500(
  const std::array<std::complex<dd_real>,472>& g
);
#endif
