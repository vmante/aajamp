#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1141(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[39];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[17];
    z[6]=d[3];
    z[7]=d[12];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[17];
    z[17]=c[31];
    z[18]=e[6];
    z[19]=f[31];
    z[20]=f[32];
    z[21]=f[33];
    z[22]=f[36];
    z[23]=f[39];
    z[24]=f[40];
    z[25]=f[43];
    z[26]=n<T>(1,2)*z[6];
    z[27]=z[2] + z[6];
    z[27]=z[27]*z[26];
    z[28]=z[6] - z[2];
    z[29]= - 3*z[28];
    z[30]=n<T>(7,4)*z[8];
    z[31]= - n<T>(23,8)*z[9] + z[30] - z[29];
    z[31]=z[9]*z[31];
    z[32]=npow(z[2],2);
    z[33]=npow(z[8],2);
    z[34]= - z[10] + n<T>(1,2)*z[11];
    z[35]= - z[8] - z[9];
    z[35]=n<T>(1,2)*z[35] - n<T>(1,3)*z[3];
    z[35]=z[3]*z[35];
    z[27]=n<T>(7,4)*z[35] + z[31] + z[27] + n<T>(1,8)*z[33] - z[32] + 3*z[34] + 
   n<T>(11,4)*z[13];
    z[31]=n<T>(1,2)*z[3];
    z[27]=z[27]*z[31];
    z[34]=n<T>(1,3)*z[9];
    z[35]=9*z[5];
    z[36]= - z[35] - n<T>(1,6)*z[8];
    z[36]=n<T>(1,4)*z[36] - z[34];
    z[36]=z[9]*z[36];
    z[37]=z[7] + n<T>(3,2)*z[2];
    z[30]=n<T>(7,8)*z[3] + n<T>(23,4)*z[9] - n<T>(15,2)*z[6] + 3*z[37] - z[30];
    z[30]=z[31]*z[30];
    z[31]=z[10] + n<T>(3,2)*z[12];
    z[31]=3*z[31] + z[11];
    z[37]= - z[2]*z[5];
    z[38]=n<T>(3,4)*z[6] + n<T>(3,2)*z[7] + z[5];
    z[38]=z[6]*z[38];
    z[30]=z[30] + z[36] + z[38] - n<T>(1,48)*z[33] + n<T>(1,2)*z[31] + z[37];
    z[30]=z[1]*z[30];
    z[31]=3*z[16] - n<T>(5,4)*z[14];
    z[30]=n<T>(1,4)*z[31] + z[30];
    z[30]=i*z[30];
    z[31]=i*z[1];
    z[35]= - n<T>(7,3)*z[9] - z[6] + n<T>(11,3)*z[8] - z[35] - 5*z[2];
    z[35]=z[35]*z[31];
    z[28]=n<T>(7,2)*z[3] - z[28];
    z[28]=z[3]*z[28];
    z[28]=z[35] + z[28] + z[18] - 9*z[12];
    z[29]=n<T>(17,6)*z[9] - n<T>(11,6)*z[8] + z[29];
    z[29]=z[9]*z[29];
    z[29]=z[32] + z[29];
    z[32]= - n<T>(1,2)*z[2] + z[6];
    z[26]=z[32]*z[26];
    z[31]= - n<T>(25,36)*z[4] + n<T>(25,24)*z[31] - n<T>(5,8)*z[3] + n<T>(11,24)*z[9]
     + 
    z[6] - n<T>(1,4)*z[2] + n<T>(1,3)*z[8];
    z[31]=z[4]*z[31];
    z[26]=z[31] + z[26] - n<T>(5,24)*z[33] - n<T>(13,36)*z[13] + 2*z[11] + n<T>(1,2)
   *z[29] + n<T>(1,4)*z[28];
    z[26]=z[4]*z[26];
    z[28]= - z[12] + n<T>(5,4)*z[11];
    z[29]=npow(z[6],2);
    z[29]= - n<T>(3,4)*z[29] + n<T>(3,8)*z[13] - n<T>(3,2)*z[10] - z[28];
    z[29]=z[6]*z[29];
    z[31]=n<T>(1,16)*z[8] + z[34];
    z[31]=z[31]*z[34];
    z[32]=n<T>(1,24)*z[33] - n<T>(59,72)*z[13] - n<T>(9,2)*z[12] - z[11];
    z[31]=n<T>(1,2)*z[32] + z[31];
    z[31]=z[9]*z[31];
    z[32]=z[7] - n<T>(3,2)*z[5];
    z[32]=z[13]*z[32];
    z[32]=z[21] + z[32] - z[20];
    z[28]=z[2]*z[28];
    z[33]=n<T>(17,72)*z[33] + n<T>(1,4)*z[18] + n<T>(1,9)*z[13];
    z[33]=z[8]*z[33];

    r += n<T>(9,8)*z[15] - n<T>(35,16)*z[17] - n<T>(39,16)*z[19] + n<T>(5,16)*z[22]
     + n<T>(1,16)*z[23]
     + n<T>(1,3)*z[24]
     + z[25]
     + z[26]
     + z[27]
     + z[28]
     + z[29]
       + z[30] + z[31] + n<T>(3,2)*z[32] + z[33];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1141(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1141(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
