#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf205(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[23];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=e[1];
    z[6]=e[8];
    z[7]=c[3];
    z[8]=c[19];
    z[9]=c[23];
    z[10]=c[25];
    z[11]=c[26];
    z[12]=c[28];
    z[13]=c[31];
    z[14]=f[5];
    z[15]=f[11];
    z[16]=z[1]*z[3];
    z[17]=z[4]*z[1];
    z[16]=z[16] + z[17];
    z[18]=2*i;
    z[16]=z[16]*z[18];
    z[19]=z[5] + z[6];
    z[20]= - z[19] + n<T>(1,6)*z[7];
    z[21]=npow(z[4],2);
    z[22]= - z[1]*z[18];
    z[22]=z[2] - z[3] + z[22];
    z[22]=z[2]*z[22];
    z[16]=z[22] + z[16] - z[21] - z[20];
    z[16]=z[2]*z[16];
    z[21]=z[4]*z[3];
    z[20]=z[21] - z[20];
    z[20]=z[4]*z[20];
    z[19]= - z[1]*z[19];
    z[17]= - z[3]*z[17];
    z[17]=z[17] + z[19] + 2*z[11] + z[9];
    z[17]=z[17]*z[18];
    z[18]=z[12] + z[13];

    r += n<T>(1,6)*z[8] - z[10] + z[14] + z[15] + z[16] + z[17] - 2*z[18]
       + z[20];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf205(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf205(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
