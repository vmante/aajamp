#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf62(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[36];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[12];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=e[5];
    z[11]=e[8];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[19];
    z[15]=c[23];
    z[16]=c[25];
    z[17]=c[26];
    z[18]=c[28];
    z[19]=e[10];
    z[20]=f[3];
    z[21]=f[4];
    z[22]=f[5];
    z[23]=f[11];
    z[24]=f[12];
    z[25]=f[16];
    z[26]=f[17];
    z[27]=z[6] - z[9];
    z[28]=z[27] + n<T>(1,2)*z[7];
    z[28]=z[28]*z[7];
    z[29]=npow(z[9],2);
    z[29]=n<T>(1,2)*z[29];
    z[28]=z[28] - z[29];
    z[30]=z[4]*z[8];
    z[31]= - z[6]*z[5];
    z[32]= - z[2] + z[5] + 2*z[4];
    z[32]=z[2]*z[32];
    z[33]=z[2] - z[8];
    z[34]= - z[4] + z[9] - z[33];
    z[34]=z[3]*z[34];
    z[31]=z[34] + z[32] - z[30] + z[31] - 2*z[11] + z[10] + z[28];
    z[31]=z[1]*z[31];
    z[31]=n<T>(1,3)*z[13] + 2*z[17] + z[15] + z[31];
    z[31]=i*z[31];
    z[32]=npow(z[6],2);
    z[32]=n<T>(1,2)*z[32];
    z[34]=n<T>(1,6)*z[12];
    z[35]= - n<T>(1,2)*z[9] + z[6];
    z[35]=z[7]*z[35];
    z[29]=z[35] + z[32] - z[29] - z[19] + z[34];
    z[29]=z[7]*z[29];
    z[27]=z[27] - z[33];
    z[27]=z[3]*z[27];
    z[35]=npow(z[8],2);
    z[35]=n<T>(1,2)*z[35];
    z[33]=n<T>(1,2)*z[4] + z[33];
    z[33]=z[4]*z[33];
    z[27]=n<T>(1,2)*z[27] - z[12] - z[35] + z[33] - z[28];
    z[27]=z[3]*z[27];
    z[28]=n<T>(1,2)*z[30] + z[35] + z[11] - z[34];
    z[28]=z[4]*z[28];
    z[30]= - z[4] + z[2];
    z[30]=z[2]*z[30];
    z[30]=z[30] - z[12];
    z[33]=npow(z[4],2);
    z[30]= - z[33] + z[11] - z[10] + n<T>(1,2)*z[30];
    z[30]=z[2]*z[30];
    z[33]= - z[16] - z[20] - z[21] + z[22] + z[23] - z[24] + z[26] - 
    z[25];
    z[35]=n<T>(1,3)*z[9] + z[5];
    z[35]=z[12]*z[35];
    z[34]=z[8]*z[34];
    z[32]= - z[32] - z[19] + z[10];
    z[32]=z[6]*z[32];

    r += n<T>(1,12)*z[14] - z[18] + z[27] + z[28] + z[29] + z[30] + z[31]
       + z[32] + n<T>(1,2)*z[33] + z[34] + z[35];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf62(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf62(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
