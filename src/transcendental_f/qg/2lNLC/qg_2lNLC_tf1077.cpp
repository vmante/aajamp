#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1077(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[21];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=c[12];
    z[5]=c[13];
    z[6]=c[20];
    z[7]=c[23];
    z[8]=f[44];
    z[9]=g[82];
    z[10]=g[90];
    z[11]=g[101];
    z[12]=g[131];
    z[13]=g[214];
    z[14]=g[245];
    z[15]=g[345];
    z[16]=g[347];
    z[17]=g[348];
    z[18]= - n<T>(3,5)*z[7] + 12*z[6] + n<T>(144,5)*z[5] - n<T>(11,45)*z[1];
    z[18]= - z[18]*i;
    z[18]=z[18] + 6*z[8] + 4*z[4];
    z[19]=z[3] - z[2];
    z[18]=z[19]*z[18];
    z[19]= - z[9] + z[13] - z[12];
    z[20]=z[16] - z[11];

    r += 6*z[10] + 9*z[14] + z[15] + 4*z[17] + z[18] + 3*z[19] + 2*
      z[20];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1077(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1077(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
