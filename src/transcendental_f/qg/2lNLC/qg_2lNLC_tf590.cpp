#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf590(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[4];
    z[3]=d[6];
    z[4]=d[8];
    z[5]=d[9];
    z[6]=c[12];
    z[7]=c[13];
    z[8]=c[20];
    z[9]=c[23];
    z[10]=d[0];
    z[11]=f[42];
    z[12]=f[61];
    z[13]=f[77];
    z[14]=f[79];
    z[15]=d[3];
    z[16]=f[44];
    z[17]=f[62];
    z[18]=g[99];
    z[19]=g[101];
    z[20]=g[145];
    z[21]=g[147];
    z[22]=g[224];
    z[23]=g[254];
    z[24]=z[12] - z[14];
    z[25]=z[11] - z[13];
    z[26]=z[24] + z[25];
    z[27]=z[10] - z[15];
    z[26]=z[26]*z[27];
    z[27]=n<T>(8,3)*z[6] + z[16] - z[17];
    z[24]=z[27] - z[24];
    z[28]=z[2] - z[3];
    z[24]= - z[24]*z[28];
    z[25]=z[27] + z[25];
    z[27]=z[4] - z[5];
    z[25]=z[25]*z[27];
    z[27]=z[28] - z[27];
    z[28]=2*z[8] - n<T>(1,10)*z[9] + n<T>(1,30)*z[1] + n<T>(24,5)*z[7];
    z[27]=i*z[28]*z[27];
    z[28]=z[18] - z[19] + z[20] - z[21] - z[23] - z[22];

    r += z[24] + z[25] + z[26] + z[27] + n<T>(2,3)*z[28];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf590(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf590(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
