#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf54(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[64];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[70];
    z[3]=f[79];
    z[4]=c[11];
    z[5]=d[1];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=c[12];
    z[11]=c[13];
    z[12]=c[20];
    z[13]=c[23];
    z[14]=d[0];
    z[15]=f[24];
    z[16]=f[44];
    z[17]=f[69];
    z[18]=f[80];
    z[19]=f[22];
    z[20]=f[42];
    z[21]=d[2];
    z[22]=d[3];
    z[23]=g[24];
    z[24]=g[27];
    z[25]=g[38];
    z[26]=g[40];
    z[27]=g[48];
    z[28]=g[50];
    z[29]=g[79];
    z[30]=g[82];
    z[31]=g[90];
    z[32]=g[92];
    z[33]=g[99];
    z[34]=g[101];
    z[35]=g[170];
    z[36]=g[173];
    z[37]=g[181];
    z[38]=g[183];
    z[39]=g[189];
    z[40]=g[191];
    z[41]=g[243];
    z[42]=g[251];
    z[43]=g[254];
    z[44]=g[270];
    z[45]=g[272];
    z[46]=g[276];
    z[47]=g[278];
    z[48]=g[281];
    z[49]=g[308];
    z[50]=g[395];
    z[51]=g[397];
    z[52]=g[398];
    z[53]=z[19] - z[20];
    z[54]= - z[17] + z[53] - z[16];
    z[54]=n<T>(17,4)*z[54];
    z[55]=n<T>(17,2)*z[3];
    z[56]=n<T>(1,3)*z[10];
    z[57]=n<T>(11,2)*z[2] + z[55] + z[54] + z[56];
    z[57]=z[5]*z[57];
    z[58]=n<T>(9,2)*z[3];
    z[54]= - n<T>(3,2)*z[2] + z[58] - z[54] + n<T>(53,3)*z[10];
    z[54]=z[9]*z[54];
    z[53]=z[53] + z[15] + z[18];
    z[53]= - z[56] + n<T>(1,4)*z[53];
    z[56]= - n<T>(9,2)*z[2] + 17*z[53] - z[58];
    z[56]=z[6]*z[56];
    z[54]=z[50] + z[54] + z[56] + z[57] + z[40];
    z[56]=z[4]*z[8];
    z[55]=z[1]*z[55];
    z[57]= - z[12] + n<T>(1,20)*z[13];
    z[57]= - n<T>(3,5)*z[11] + n<T>(1,4)*z[57];
    z[58]=17*z[57];
    z[59]= - z[58] - n<T>(1007,2160)*z[4];
    z[59]=z[9]*z[59];
    z[58]=z[58] + n<T>(287,2160)*z[4];
    z[58]=z[5]*z[58];
    z[55]=z[58] + z[59] + n<T>(1,3)*z[56] + z[55];
    z[56]=z[6] - z[7];
    z[56]=n<T>(17,2)*z[56];
    z[57]=n<T>(31,2160)*z[4] + z[57];
    z[56]=z[57]*z[56];
    z[57]=z[2]*z[1];
    z[55]=2*z[57] + z[56] + n<T>(1,2)*z[55];
    z[55]=i*z[55];
    z[56]=z[2] + z[3];
    z[53]= - z[53] + n<T>(1,2)*z[56];
    z[53]=z[7]*z[53];
    z[53]=z[53] + z[46];
    z[56]=z[14] - z[21];
    z[56]=n<T>(17,16)*z[56];
    z[57]= - z[18] - z[17] + z[15] + z[16];
    z[56]=z[57]*z[56];
    z[57]= - z[35] - z[38] - z[44] - z[47] + z[29] + z[30] + z[31] + 
    z[32] + z[23] + z[24] + z[25] + z[26];
    z[58]= - z[27] + z[28] - z[33] + z[34] + z[39] + z[48];
    z[59]=z[42] - z[43];
    z[60]=z[36] - z[45];
    z[61]=z[10]*z[8];
    z[62]= - z[22] - n<T>(13,4)*z[8];
    z[62]=z[3]*z[62];
    z[63]= - z[22] - z[8];
    z[63]=z[2]*z[63];

    r += n<T>(7,2)*z[37] + n<T>(13,2)*z[41] + 2*z[49] + n<T>(1,2)*z[51] + z[52] + 
      n<T>(17,4)*z[53] + n<T>(1,4)*z[54] + z[55] + z[56] - n<T>(17,8)*z[57] - n<T>(17,24)*z[58]
     + n<T>(3,4)*z[59]
     + n<T>(3,2)*z[60] - n<T>(9,2)*z[61]
     + z[62]
     +  z[63];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf54(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf54(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
