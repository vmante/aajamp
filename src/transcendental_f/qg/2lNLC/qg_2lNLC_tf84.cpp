#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf84(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[73];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[5];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[0];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[7];
    z[35]=e[13];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[11];
    z[43]=f[12];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[18];
    z[47]=f[31];
    z[48]=f[36];
    z[49]=f[39];
    z[50]=f[43];
    z[51]=f[51];
    z[52]=f[58];
    z[53]=f[83];
    z[54]=n<T>(1,2)*z[4];
    z[55]=n<T>(13,4)*z[4] + n<T>(13,2)*z[12] - z[11];
    z[55]=z[55]*z[54];
    z[56]=n<T>(1,3)*z[8];
    z[57]= - n<T>(1871,24)*z[8] + n<T>(1417,8)*z[10];
    z[58]=n<T>(379,3)*z[3] - z[57];
    z[58]=z[58]*z[56];
    z[59]=3*z[10];
    z[60]=z[8] + z[3];
    z[61]= - n<T>(3,2)*z[7] + n<T>(3511,72)*z[2] + z[59] - z[14] - n<T>(37,4)*z[4]
    - z[60];
    z[61]=z[7]*z[61];
    z[62]=npow(z[13],2);
    z[63]=n<T>(1,2)*z[62];
    z[64]=npow(z[6],2);
    z[65]= - z[11] - z[15];
    z[65]=z[15]*z[65];
    z[66]=n<T>(1,2)*z[3];
    z[67]= - z[9] + z[66];
    z[67]=z[3]*z[67];
    z[68]=z[12] - z[4];
    z[68]=4*z[68] + n<T>(7,16)*z[10];
    z[68]=z[10]*z[68];
    z[60]=z[60] + z[6];
    z[69]= - n<T>(979,18)*z[2] + n<T>(53,4)*z[4] - z[60];
    z[69]=z[2]*z[69];
    z[70]=n<T>(1399,36)*z[9];
    z[71]= - n<T>(2629,72)*z[7] + n<T>(2917,72)*z[2] + n<T>(1363,72)*z[8] + n<T>(1417,24)
   *z[10] - n<T>(361,9)*z[3] - z[70] - 4*z[4];
    z[71]=z[5]*z[71];
    z[55]=z[71] + z[61] + z[69] + z[58] + z[68] + n<T>(1115,72)*z[67] + 
    z[55] + z[65] + 2*z[64] + z[63] - n<T>(3061,72)*z[16] + n<T>(1399,36)*z[17]
    + n<T>(13,4)*z[18] - n<T>(3007,72)*z[19] + z[20] - n<T>(1,2)*z[21];
    z[55]=z[1]*z[55];
    z[58]= - n<T>(167,3)*z[23] + 1517*z[28] + 979*z[26];
    z[55]=n<T>(1,18)*z[58] + z[55];
    z[55]=i*z[55];
    z[58]=npow(z[15],2);
    z[61]=z[64] - z[58];
    z[57]= - n<T>(361,3)*z[3] + z[57];
    z[56]=z[57]*z[56];
    z[57]=npow(z[2],2);
    z[65]=npow(z[4],2);
    z[67]=npow(z[3],2);
    z[68]=npow(z[10],2);
    z[69]= - n<T>(1399,4)*z[17] + 265*z[16];
    z[71]=n<T>(2665,12)*z[7] - 503*z[4] + n<T>(961,3)*z[2];
    z[71]=z[7]*z[71];
    z[72]= - n<T>(2093,3)*z[2] - n<T>(1363,6)*z[8] - n<T>(1417,2)*z[10] + 509*z[4]
    + n<T>(2843,3)*z[3];
    z[72]=n<T>(1,2)*z[72] + n<T>(265,3)*z[7];
    z[72]=n<T>(1,2)*z[72] + z[5];
    z[72]=z[5]*z[72];
    z[56]=n<T>(1,6)*z[72] + n<T>(1,12)*z[71] - n<T>(653,144)*z[57] + z[56] - n<T>(19,16)
   *z[68] + n<T>(1115,144)*z[67] + n<T>(7,4)*z[65] - n<T>(2549,432)*z[22] + n<T>(1,9)*
    z[69] - n<T>(3,2)*z[61];
    z[56]=z[5]*z[56];
    z[61]=n<T>(3,4)*z[62];
    z[69]=z[8]*z[3];
    z[60]= - n<T>(1049,144)*z[2] + z[60];
    z[60]=z[2]*z[60];
    z[59]=n<T>(625,24)*z[4] - z[59];
    z[59]=n<T>(5,36)*z[7] + n<T>(1,2)*z[59] - n<T>(274,9)*z[2];
    z[59]=z[7]*z[59];
    z[59]=z[59] + z[60] - z[69] + n<T>(3,4)*z[68] + n<T>(201,16)*z[65] - n<T>(493,432)*z[22]
     - n<T>(1,2)*z[64]
     + z[61] - z[20]
     + n<T>(256,9)*z[19];
    z[59]=z[7]*z[59];
    z[60]=n<T>(941,9)*z[16] - 5*z[31] + n<T>(959,9)*z[19];
    z[60]= - n<T>(29,4)*z[68] - n<T>(53,4)*z[65] - n<T>(4565,216)*z[22] + n<T>(1,4)*
    z[60] - z[58];
    z[57]=n<T>(1589,72)*z[57] + n<T>(1,2)*z[60] - z[69];
    z[57]=z[2]*z[57];
    z[60]= - n<T>(131,36)*z[10] + n<T>(29,2)*z[4] + 69*z[3];
    z[60]=z[10]*z[60];
    z[60]=n<T>(1,4)*z[60] - n<T>(23,8)*z[67] + n<T>(8059,288)*z[22] + z[61] - n<T>(141,8)
   *z[32] - 4*z[18];
    z[60]=z[10]*z[60];
    z[58]=n<T>(1745,72)*z[67] + n<T>(5323,216)*z[22] + z[58] - z[63] + z[36] - 
   n<T>(857,9)*z[33];
    z[61]=n<T>(1,2)*z[15];
    z[63]= - n<T>(171,8)*z[10] - z[61] + n<T>(433,9)*z[3];
    z[63]=z[8]*z[63];
    z[58]=z[63] + n<T>(1,2)*z[58] - n<T>(64,3)*z[68];
    z[58]=z[8]*z[58];
    z[63]=3*z[13];
    z[64]=z[35] - z[34];
    z[64]=z[64]*z[63];
    z[65]= - z[63] + z[15];
    z[61]=z[65]*z[61];
    z[61]=z[61] + 3*z[35] + z[21];
    z[61]=z[15]*z[61];
    z[65]=z[15] - n<T>(193,24)*z[4];
    z[65]=z[4]*z[65];
    z[62]=z[65] + n<T>(1423,144)*z[22] - n<T>(5,2)*z[62] - n<T>(13,2)*z[18] + n<T>(5,4)*
    z[31] + z[21];
    z[54]=z[62]*z[54];
    z[62]= - n<T>(1115,9)*z[17] - n<T>(2213,9)*z[33] - 89*z[32];
    z[62]= - n<T>(2345,36)*z[67] + n<T>(1,4)*z[62] + n<T>(97,9)*z[22];
    z[62]=z[62]*z[66];
    z[65]= - z[51] + z[48] + z[49];
    z[66]=z[47] - z[52];
    z[63]=z[63] - z[6];
    z[63]=z[6]*z[63];
    z[63]= - 3*z[34] + n<T>(1,2)*z[63];
    z[63]=z[6]*z[63];
    z[67]= - n<T>(23,24)*z[15] + n<T>(19,24)*z[6] - z[70] - n<T>(1,2)*z[11] - z[14]
    + n<T>(13,4)*z[12];
    z[67]=z[22]*z[67];

    r += n<T>(3571,144)*z[24] + n<T>(254,27)*z[25] - n<T>(991,36)*z[27] - n<T>(1517,36)
      *z[29] - n<T>(30901,288)*z[30] - n<T>(53,4)*z[37] + n<T>(29,8)*z[38] + 3*
      z[39] - n<T>(1871,72)*z[40] + n<T>(1087,36)*z[41] + n<T>(2629,144)*z[42] + n<T>(19,16)*z[43]
     - n<T>(211,16)*z[44]
     + n<T>(33,8)*z[45] - n<T>(29,12)*z[46]
     +  z[50] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + 
      z[60] + z[61] + z[62] + z[63] + z[64] + n<T>(3,4)*z[65] + n<T>(1,4)*z[66]
       + z[67];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf84(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf84(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
