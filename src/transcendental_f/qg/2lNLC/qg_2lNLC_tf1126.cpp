#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1126(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[55];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[7];
    z[7]=d[2];
    z[8]=d[12];
    z[9]=d[4];
    z[10]=d[17];
    z[11]=e[5];
    z[12]=e[7];
    z[13]=e[8];
    z[14]=e[9];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[31];
    z[24]=e[1];
    z[25]=e[6];
    z[26]=f[3];
    z[27]=f[5];
    z[28]=f[11];
    z[29]=f[17];
    z[30]=f[31];
    z[31]=f[32];
    z[32]=f[33];
    z[33]=f[36];
    z[34]=f[39];
    z[35]=f[43];
    z[36]=n<T>(1,2)*z[3];
    z[37]=z[36] + n<T>(3,2)*z[5];
    z[38]=2*z[6];
    z[39]=n<T>(3,2)*z[4];
    z[40]=z[1]*i;
    z[41]= - n<T>(3,2)*z[2] + z[39] + 3*z[40] + z[38] - z[37];
    z[41]=z[2]*z[41];
    z[37]=2*z[4] + n<T>(1,2)*z[40] - z[6] - z[37];
    z[37]=z[4]*z[37];
    z[42]=npow(z[6],2);
    z[37]=z[42] + z[37];
    z[43]=z[3]*i;
    z[44]=i*z[6];
    z[45]= - 2*z[44] + z[43];
    z[46]=3*z[1];
    z[45]=z[45]*z[46];
    z[46]=3*z[5];
    z[47]=z[40] - z[3];
    z[47]= - z[5] + z[6] - n<T>(3,2)*z[47];
    z[47]=z[47]*z[46];
    z[48]= - 4*z[6] + z[3];
    z[48]=z[3]*z[48];
    z[37]=z[41] + z[47] + z[45] + z[48] + 3*z[37];
    z[37]=z[2]*z[37];
    z[41]=z[40] + z[6];
    z[45]=3*z[3];
    z[41]= - z[46] - z[45] - n<T>(11,4)*z[41];
    z[41]=z[5]*z[41];
    z[47]= - z[6] - z[3];
    z[47]=z[47]*z[45];
    z[48]=11*z[44] + 9*z[43];
    z[49]=n<T>(1,2)*z[1];
    z[48]=z[48]*z[49];
    z[41]=z[41] + z[48] - n<T>(17,2)*z[42] + z[47];
    z[41]=z[5]*z[41];
    z[47]= - n<T>(19,2)*z[44] + 3*z[43];
    z[47]=z[47]*z[49];
    z[48]=z[3]*z[6];
    z[49]=z[45] + n<T>(13,2)*z[5];
    z[49]=z[5]*z[49];
    z[47]=n<T>(1,2)*z[49] + z[47] + n<T>(19,8)*z[42] + z[48];
    z[48]=z[40] - z[6];
    z[49]=n<T>(9,4)*z[5] - 2*z[3] - n<T>(11,8)*z[48];
    z[49]=3*z[49] - n<T>(49,4)*z[4];
    z[49]=z[4]*z[49];
    z[47]=3*z[47] + z[49];
    z[47]=z[4]*z[47];
    z[49]=z[2] - z[3];
    z[50]=2*z[40];
    z[38]= - n<T>(19,2)*z[4] - 7*z[5] + z[50] - z[38] + n<T>(5,2)*z[49];
    z[38]=z[12]*z[38];
    z[49]= - z[2] - z[6] + z[50];
    z[49]=z[13]*z[49];
    z[38]=z[22] - z[35] + z[38] + z[49];
    z[49]=z[4] + z[3];
    z[50]= - z[40]*z[49];
    z[50]= - z[15] + z[50];
    z[50]=z[8]*z[50];
    z[49]= - z[40] + z[49];
    z[49]=z[11]*z[49];
    z[49]=z[31] + z[50] + z[49];
    z[50]=z[9] + z[5];
    z[51]=z[39] - n<T>(1,2)*z[48] - z[50];
    z[51]=z[9]*z[51];
    z[52]=n<T>(1,2)*z[42];
    z[53]=z[40]*z[6];
    z[51]=z[51] + z[52] - z[53];
    z[54]=n<T>(1,2)*z[4] + z[48];
    z[39]=z[54]*z[39];
    z[54]= - z[5]*z[48];
    z[39]=z[39] + z[54] + n<T>(1,2)*z[51];
    z[39]=z[9]*z[39];
    z[50]=z[25]*z[50];
    z[39]= - z[17] + z[39] - z[50];
    z[43]=z[43] - z[44];
    z[43]=z[43]*z[1];
    z[36]=z[6] - z[36];
    z[36]=z[3]*z[36];
    z[36]= - z[43] - z[52] + z[36];
    z[44]=z[3] - z[6];
    z[50]=z[7]*z[44];
    z[36]=n<T>(3,2)*z[50] + 3*z[36] - n<T>(1,2)*z[15];
    z[36]=z[7]*z[36];
    z[40]=z[5]*z[40];
    z[40]=z[15] + z[53] + z[40];
    z[40]=z[10]*z[40];
    z[48]=z[5] - z[48];
    z[48]=z[14]*z[48];
    z[40]=z[40] + z[48];
    z[48]=n<T>(1,2)*z[6] + z[45];
    z[48]=z[3]*z[48];
    z[42]= - n<T>(3,2)*z[42] + z[48];
    z[42]=z[3]*z[42];
    z[43]= - z[45]*z[43];
    z[45]=n<T>(13,2)*z[6] - n<T>(11,3)*z[3];
    z[45]= - n<T>(41,2)*z[4] + n<T>(1,2)*z[45] + z[46];
    z[45]= - n<T>(11,6)*z[9] + n<T>(1,2)*z[45] + 2*z[2];
    z[45]=z[15]*z[45];
    z[46]= - z[20] + z[29] + z[28] + z[27] - z[26];
    z[48]= - 6*z[21] - 3*z[19] + n<T>(29,24)*z[16];
    z[48]=i*z[48];
    z[44]= - z[24]*z[44];

    r +=  - n<T>(1,4)*z[18] - n<T>(15,2)*z[23] + n<T>(81,8)*z[30] + 27*z[32] - n<T>(11,8)*z[33]
     + n<T>(33,8)*z[34]
     + z[36]
     + z[37]
     + 3*z[38]
     + n<T>(11,2)*z[39]
       + n<T>(23,2)*z[40] + z[41] + z[42] + z[43] + z[44] + z[45] - n<T>(3,2)*
      z[46] + z[47] + z[48] + 6*z[49];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1126(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1126(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
