#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf719(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[58];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[8];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[30];
    z[24]=c[31];
    z[25]=e[10];
    z[26]=e[6];
    z[27]=f[0];
    z[28]=f[3];
    z[29]=f[4];
    z[30]=f[5];
    z[31]=f[11];
    z[32]=f[12];
    z[33]=f[16];
    z[34]=f[17];
    z[35]=f[31];
    z[36]=f[36];
    z[37]=f[39];
    z[38]=n<T>(283,3)*z[3];
    z[39]= - z[38] + n<T>(2545,2)*z[9];
    z[40]=z[39] + n<T>(6503,12)*z[7];
    z[41]=n<T>(1,4)*z[7];
    z[42]= - z[40]*z[41];
    z[43]=n<T>(1,4)*z[6];
    z[44]=n<T>(114325,12)*z[6] - n<T>(10273,2)*z[4] + n<T>(8011,3)*z[2];
    z[44]=z[44]*z[43];
    z[45]=npow(z[9],2);
    z[46]=n<T>(2545,16)*z[45];
    z[47]=npow(z[4],2);
    z[48]=npow(z[3],2);
    z[49]=7069*z[13];
    z[50]=z[49] + 14797*z[12];
    z[50]=n<T>(1,4)*z[50] - 161*z[15];
    z[51]=npow(z[2],2);
    z[52]=n<T>(14797,48)*z[6] - n<T>(2851,3)*z[2] + n<T>(7069,48)*z[7] + n<T>(2545,16)*
    z[9] + n<T>(10273,16)*z[4] - n<T>(919,3)*z[3];
    z[52]=z[5]*z[52];
    z[42]=z[52] + z[44] - n<T>(5561,12)*z[51] + z[42] - z[46] + n<T>(7069,12)*
    z[48] + n<T>(1,3)*z[50] - n<T>(20829,16)*z[47];
    z[42]=z[5]*z[42];
    z[44]=n<T>(1,1508)*z[7];
    z[40]=z[40]*z[44];
    z[50]=npow(z[10],2);
    z[51]=n<T>(3,4)*z[50];
    z[52]=13*z[11];
    z[53]= - n<T>(25919,2)*z[12] - 32893*z[14] - n<T>(7069,2)*z[13];
    z[54]= - z[8] - n<T>(1,4)*z[3];
    z[54]=z[3]*z[54];
    z[55]=z[10] - n<T>(3487,6032)*z[9];
    z[55]=z[9]*z[55];
    z[56]= - n<T>(48539,4524)*z[2] - 5*z[4] + n<T>(7069,1508)*z[3];
    z[56]=z[2]*z[56];
    z[40]=z[56] + z[40] + z[55] + n<T>(7069,2262)*z[54] + z[51] + n<T>(1,2262)*
    z[53] + z[52];
    z[53]=n<T>(1,4)*z[9];
    z[54]=z[53] - z[10];
    z[55]=n<T>(18143,2262)*z[2] - n<T>(5749,6032)*z[4] + z[54];
    z[55]=z[6]*z[55];
    z[39]= - n<T>(114325,6)*z[6] + n<T>(25919,3)*z[2] - n<T>(7069,6)*z[7] + n<T>(7069,3)
   *z[8] + n<T>(20829,2)*z[4] - z[39];
    z[39]=z[5]*z[39];
    z[39]=n<T>(1,3016)*z[39] + n<T>(1,2)*z[40] + z[55];
    z[39]=z[1]*z[39];
    z[40]= - n<T>(22903,6)*z[16] + 32893*z[21] + n<T>(69085,2)*z[19];
    z[39]=n<T>(1,9048)*z[40] + z[39];
    z[39]=i*z[39];
    z[40]=n<T>(1,2)*z[2];
    z[55]=3*z[7];
    z[56]=n<T>(40999,2262)*z[2] - n<T>(7069,754)*z[3] - z[55];
    z[56]=z[56]*z[40];
    z[57]=n<T>(32893,2)*z[14] + 5561*z[12];
    z[56]=z[56] + n<T>(7069,1508)*z[48] + 5*z[47] - n<T>(43639,13572)*z[15] + n<T>(1,1131)*z[57]
     - z[52];
    z[56]=z[2]*z[56];
    z[52]= - z[52] + n<T>(8451,3016)*z[15];
    z[52]=z[4]*z[52];
    z[52]=z[52] + z[56];
    z[47]=n<T>(5749,1508)*z[47] - n<T>(69085,13572)*z[15] + n<T>(32893,1131)*z[14]
    - 3*z[50];
    z[50]=z[55] - n<T>(14797,4524)*z[2];
    z[40]=z[50]*z[40];
    z[50]= - z[43] - n<T>(32893,4524)*z[2] - n<T>(3,4)*z[7] + n<T>(10273,6032)*z[4]
    - z[54];
    z[50]=z[6]*z[50];
    z[54]= - z[10] + z[9];
    z[54]=z[9]*z[54];
    z[40]=z[50] + z[40] + n<T>(1,4)*z[47] + z[54];
    z[40]=z[40]*z[43];
    z[38]= - z[38] + n<T>(2545,4)*z[9];
    z[38]=z[38]*z[41];
    z[41]= - n<T>(283,8)*z[48] + n<T>(283,4)*z[25] - n<T>(919,3)*z[15];
    z[38]=z[38] + n<T>(1,3)*z[41] + z[46];
    z[38]=z[38]*z[44];
    z[41]= - n<T>(13855,4)*z[48] + n<T>(2545,4)*z[15] + n<T>(283,2)*z[25] - z[49];
    z[41]=z[3]*z[41];
    z[43]= - n<T>(1,2)*z[45] - n<T>(2545,9048)*z[15] - z[26] + z[51];
    z[43]=z[43]*z[53];
    z[44]=z[32] + z[33];
    z[44]= - n<T>(20829,1508)*z[28] - n<T>(6503,4524)*z[29] + n<T>(105277,4524)*
    z[30] + n<T>(114325,4524)*z[31] + n<T>(5749,1508)*z[34] - z[35] + z[37] - 
    z[36] + n<T>(2545,1508)*z[44];
    z[45]=n<T>(7069,754)*z[8] - 7*z[10];
    z[45]=z[15]*z[45];

    r += n<T>(7069,36192)*z[17] + n<T>(105277,434304)*z[18] - n<T>(81149,72384)*
      z[20] - n<T>(32893,36192)*z[22] + z[23] + n<T>(45521,72384)*z[24] - 2*
      z[27] + z[38] + n<T>(1,2)*z[39] + z[40] + n<T>(1,9048)*z[41] + n<T>(1,1508)*
      z[42] + z[43] + n<T>(1,16)*z[44] + n<T>(1,24)*z[45] + n<T>(1,8)*z[52];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf719(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf719(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
