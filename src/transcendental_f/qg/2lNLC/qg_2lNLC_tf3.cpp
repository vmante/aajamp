#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf3(
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[4];
  std::complex<T> r(0,0);

    z[1]=f[5];
    z[2]=f[13];

    r +=  - z[1] + z[2];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf3(
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf3(
  const std::array<std::complex<dd_real>,111>& f
);
#endif
