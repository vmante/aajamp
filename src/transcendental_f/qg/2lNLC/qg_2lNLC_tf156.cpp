#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf156(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[67];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[16];
    z[10]=d[4];
    z[11]=d[17];
    z[12]=d[6];
    z[13]=d[5];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=d[9];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[17];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[0];
    z[31]=e[3];
    z[32]=e[10];
    z[33]=e[7];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[18];
    z[44]=f[31];
    z[45]=f[36];
    z[46]=f[39];
    z[47]=f[40];
    z[48]=5*z[6];
    z[49]=z[1]*i;
    z[50]= - n<T>(325,36)*z[5] + n<T>(113,18)*z[49] + z[48];
    z[50]=z[5]*z[50];
    z[51]=z[49]*z[6];
    z[51]=n<T>(7,2)*z[51];
    z[52]=npow(z[10],2);
    z[53]=z[49] - z[6];
    z[54]=z[2]*z[53];
    z[55]=z[49] + z[5];
    z[55]= - n<T>(365,36)*z[3] + n<T>(173,18)*z[55] + 17*z[10];
    z[55]=z[3]*z[55];
    z[50]=z[55] + z[52] + n<T>(3,2)*z[54] - z[51] + z[50];
    z[50]=z[3]*z[50];
    z[54]=3*z[2];
    z[55]= - n<T>(11,9)*z[49] - z[6];
    z[55]=n<T>(31,18)*z[3] + z[54] + 7*z[55] + n<T>(113,9)*z[5];
    z[55]=z[3]*z[55];
    z[56]=n<T>(85,18)*z[5] - n<T>(103,18)*z[49] + z[6];
    z[56]= - n<T>(437,9)*z[3] + 7*z[56] + n<T>(17,2)*z[10];
    z[56]=z[7]*z[56];
    z[55]=z[55] + z[56];
    z[48]= - z[48] + 3*z[49];
    z[48]=z[5] + n<T>(1,2)*z[48];
    z[56]=z[2]*z[48];
    z[57]=n<T>(1,6)*z[10];
    z[58]=z[49] - z[5];
    z[59]=73*z[58] + n<T>(79,2)*z[10];
    z[59]=z[59]*z[57];
    z[60]=n<T>(53,18)*z[5] - n<T>(53,9)*z[49] - z[6];
    z[60]=z[5]*z[60];
    z[51]=z[59] + z[56] + z[51] + z[60] + n<T>(1,2)*z[55];
    z[51]=z[7]*z[51];
    z[50]=z[50] + z[51];
    z[51]=n<T>(1,2)*z[6];
    z[55]= - z[49] - z[51];
    z[56]=n<T>(1,2)*z[5];
    z[55]= - n<T>(7,36)*z[12] - z[57] + n<T>(1,3)*z[55] + z[56];
    z[55]=z[12]*z[55];
    z[57]=n<T>(1,3)*z[6];
    z[59]= - z[49] + n<T>(5,4)*z[6];
    z[59]=z[59]*z[57];
    z[60]=n<T>(1,2)*z[2];
    z[61]=z[6] - z[60];
    z[54]=z[61]*z[54];
    z[61]=z[5]*z[6];
    z[62]=2*z[53] + n<T>(1,2)*z[10];
    z[62]=z[10]*z[62];
    z[54]=z[55] + n<T>(1,3)*z[62] + z[54] + z[59] - z[61];
    z[54]=z[12]*z[54];
    z[55]=5*z[49];
    z[57]= - z[55] + z[57];
    z[57]=z[57]*npow(z[6],2);
    z[59]= - 23*z[49] + n<T>(131,2)*z[6];
    z[59]=z[6]*z[59];
    z[59]=z[59] - 23*z[61];
    z[59]=z[5]*z[59];
    z[57]=13*z[57] + n<T>(1,3)*z[59];
    z[59]=17*z[49] + n<T>(191,4)*z[6];
    z[59]=z[59]*z[51];
    z[61]=139*z[49] - n<T>(133,2)*z[6];
    z[61]= - n<T>(67,8)*z[2] + n<T>(1,4)*z[61] + 7*z[5];
    z[60]=z[61]*z[60];
    z[61]=n<T>(301,16)*z[5] - z[55] - n<T>(317,8)*z[6];
    z[61]=z[5]*z[61];
    z[59]=z[60] + z[59] + z[61];
    z[59]=z[2]*z[59];
    z[57]=z[47] + n<T>(1,8)*z[57] + n<T>(1,3)*z[59];
    z[59]=11*z[49] - n<T>(67,6)*z[6];
    z[59]=z[6]*z[59];
    z[60]= - n<T>(85,6)*z[5] + 9*z[49] + n<T>(85,3)*z[6];
    z[60]=z[5]*z[60];
    z[59]=z[59] + z[60];
    z[60]=n<T>(1,2)*z[4];
    z[61]=n<T>(13,6)*z[4] + 5*z[2] - n<T>(9,4)*z[5] + z[55] - n<T>(17,4)*z[6];
    z[61]=z[61]*z[60];
    z[55]= - z[2]*z[55];
    z[52]=z[61] - n<T>(5,2)*z[52] + n<T>(1,4)*z[59] + z[55];
    z[52]=z[52]*z[60];
    z[55]=173*z[3];
    z[59]=61*z[5] - z[55];
    z[59]=z[49]*z[59];
    z[59]=61*z[19] + z[59];
    z[59]=z[8]*z[59];
    z[55]= - 61*z[58] - z[55];
    z[55]=z[15]*z[55];
    z[55]=z[59] + z[55];
    z[51]=z[49] - z[51];
    z[51]=z[6]*z[51];
    z[56]= - z[49] + z[56];
    z[56]=z[5]*z[56];
    z[51]=67*z[51] + n<T>(73,2)*z[56];
    z[56]=n<T>(113,16)*z[49] + 10*z[6];
    z[56]=n<T>(25,24)*z[10] + n<T>(5,4)*z[2] + n<T>(1,3)*z[56] - n<T>(91,16)*z[5];
    z[56]=z[10]*z[56];
    z[51]=n<T>(1,12)*z[51] + z[56];
    z[51]=z[10]*z[51];
    z[56]= - z[4]*z[49];
    z[56]=z[56] - z[19];
    z[56]=z[9]*z[56];
    z[58]= - z[49] + z[4];
    z[58]=z[16]*z[58];
    z[56]=z[34] + z[56] + z[58];
    z[58]=29*z[49] - 55*z[5];
    z[58]=n<T>(1,2)*z[58] + 13*z[2];
    z[58]=z[14]*z[58];
    z[59]=n<T>(61,4)*z[2] + 19*z[49] - n<T>(137,4)*z[6];
    z[59]=z[17]*z[59];
    z[58]=z[58] + z[59];
    z[59]=z[13] + z[12];
    z[53]=9*z[53] + z[10];
    z[53]=n<T>(1,4)*z[53] + z[59];
    z[53]=z[13]*z[53];
    z[60]=npow(z[12],2);
    z[53]=z[60] - z[53];
    z[53]=n<T>(1,3)*z[19] - n<T>(1,2)*z[53];
    z[53]=z[13]*z[53];
    z[60]= - n<T>(5,2)*z[6] + z[5];
    z[49]=z[49]*z[60];
    z[49]= - n<T>(3,2)*z[19] + z[49];
    z[49]=z[11]*z[49];
    z[48]=z[18]*z[48];
    z[60]=z[38] - z[26];
    z[61]=z[46] + z[45];
    z[62]=z[2] - z[4];
    z[62]=z[30]*z[62];
    z[62]=z[62] - z[35];
    z[63]=n<T>(481,8)*z[2] - n<T>(851,16)*z[6] + 5*z[5];
    z[63]=n<T>(89,8)*z[4] + n<T>(125,16)*z[12] - n<T>(511,48)*z[7] - n<T>(347,16)*z[3]
    + n<T>(1,3)*z[63] + n<T>(359,16)*z[10];
    z[63]=n<T>(5,48)*z[20] + n<T>(1,9)*z[63];
    z[63]=z[19]*z[63];
    z[64]= - n<T>(67,36)*z[27] - n<T>(247,72)*z[25] + n<T>(3,4)*z[23] - n<T>(8,27)*z[21]
   ;
    z[64]=i*z[64];
    z[65]= - 2*z[10] - 7*z[3];
    z[65]=z[31]*z[65];
    z[66]=329*z[3] + 437*z[7];
    z[66]=z[32]*z[66];
    z[59]=z[33]*z[59];

    r += n<T>(283,72)*z[22] - n<T>(1327,864)*z[24] + n<T>(67,72)*z[28] - n<T>(2023,144)
      *z[29] - n<T>(9,16)*z[36] + n<T>(595,144)*z[37] + n<T>(23,144)*z[39] + n<T>(91,16)
      *z[40] + n<T>(19,16)*z[41] - n<T>(11,16)*z[42] + n<T>(5,6)*z[43] - n<T>(9,8)*
      z[44] + z[48] + z[49] + n<T>(1,4)*z[50] + z[51] + z[52] + z[53] + 
      z[54] + n<T>(1,36)*z[55] + n<T>(5,2)*z[56] + n<T>(1,3)*z[57] + n<T>(1,18)*z[58]
       + z[59] - n<T>(247,144)*z[60] + n<T>(1,8)*z[61] + n<T>(5,4)*z[62] + z[63]
     +  z[64] + z[65] + n<T>(1,72)*z[66];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf156(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf156(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
