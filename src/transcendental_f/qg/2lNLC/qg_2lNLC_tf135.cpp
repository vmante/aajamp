#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf135(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[80];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[0];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[6];
    z[44]=f[11];
    z[45]=f[12];
    z[46]=f[16];
    z[47]=f[17];
    z[48]=f[18];
    z[49]=f[31];
    z[50]=f[36];
    z[51]=f[39];
    z[52]=f[51];
    z[53]=f[55];
    z[54]=f[56];
    z[55]=f[58];
    z[56]=z[21] + 5*z[18];
    z[57]=5*z[20];
    z[58]=npow(z[7],2);
    z[59]=n<T>(5,2)*z[58];
    z[60]= - z[59] - n<T>(155,2)*z[16] + n<T>(119,3)*z[17] - n<T>(213,2)*z[19] + 
    z[57] + z[56];
    z[61]=z[15]*z[12];
    z[60]=n<T>(1,2)*z[60] + z[61];
    z[61]=n<T>(3,4)*z[3];
    z[62]=n<T>(1,2)*z[11];
    z[63]= - z[14] + z[7];
    z[63]= - n<T>(1,4)*z[8] + n<T>(213,8)*z[2] - n<T>(181,8)*z[5] + n<T>(3,4)*z[9]
     - 
    z[61] + z[62] + n<T>(5,4)*z[63] - 4*z[4];
    z[63]=z[8]*z[63];
    z[64]=n<T>(1,8)*z[11] - z[13] + z[4];
    z[64]=z[64]*z[62];
    z[65]=n<T>(1,2)*z[9];
    z[66]=n<T>(155,6)*z[3];
    z[67]=n<T>(211,12)*z[9] + z[66] + z[15] - n<T>(161,4)*z[11];
    z[67]=z[67]*z[65];
    z[68]=n<T>(119,3)*z[10];
    z[69]=n<T>(161,2)*z[11];
    z[70]=n<T>(101,6)*z[9] - n<T>(191,3)*z[3] + z[69] - z[68] + 13*z[4];
    z[70]=z[5]*z[70];
    z[71]=z[6] + z[7];
    z[72]= - z[71] + n<T>(7,4)*z[3];
    z[73]=n<T>(1,4)*z[9];
    z[74]= - n<T>(97,4)*z[2] + n<T>(167,8)*z[5] - z[73] + n<T>(3,4)*z[4] + z[72];
    z[74]=z[2]*z[74];
    z[71]=z[6]*z[71];
    z[75]=5*z[13] + z[12];
    z[75]=n<T>(1,4)*z[75] - z[4];
    z[75]=z[4]*z[75];
    z[76]= - 187*z[10] + n<T>(211,2)*z[3];
    z[76]=z[3]*z[76];
    z[60]=z[63] + z[74] + n<T>(1,4)*z[70] + z[67] + n<T>(1,24)*z[76] + z[64] + 
    z[75] + n<T>(1,2)*z[60] + z[71];
    z[60]=z[1]*z[60];
    z[60]=z[60] + n<T>(67,36)*z[23] + 46*z[28] + n<T>(95,4)*z[26];
    z[60]=i*z[60];
    z[63]=z[66] + n<T>(5,2)*z[15] - 13*z[11];
    z[63]=z[63]*z[65];
    z[64]=npow(z[15],2);
    z[66]=z[64] + n<T>(9,2)*z[37] + n<T>(73,3)*z[33];
    z[67]=npow(z[3],2);
    z[70]=npow(z[6],2);
    z[71]=npow(z[11],2);
    z[74]=n<T>(1,2)*z[15];
    z[75]=z[4]*z[74];
    z[63]=z[63] + n<T>(169,48)*z[67] - n<T>(11,2)*z[71] + z[75] + n<T>(3,8)*z[70]
     + 
   n<T>(41,9)*z[22] - n<T>(1,2)*z[66];
    z[63]=z[9]*z[63];
    z[66]=67*z[16] - 7*z[31] + 99*z[19];
    z[75]=n<T>(127,6)*z[2] - z[6] - n<T>(51,8)*z[5];
    z[75]=z[2]*z[75];
    z[66]=z[75] + n<T>(1,4)*z[66] - n<T>(17,3)*z[22];
    z[75]=npow(z[4],2);
    z[76]=z[75] + z[71];
    z[77]= - z[6]*z[7];
    z[78]=z[3]*z[15];
    z[61]= - z[15] + z[61];
    z[61]=z[9]*z[61];
    z[65]=z[65] + z[6];
    z[79]= - n<T>(53,4)*z[5] + z[65];
    z[79]=z[5]*z[79];
    z[61]=z[79] + z[61] + z[78] + z[77] - n<T>(3,8)*z[76] + n<T>(1,2)*z[66];
    z[61]=z[2]*z[61];
    z[57]= - z[57] + 57*z[19];
    z[57]=n<T>(3,2)*z[71] + n<T>(89,8)*z[75] - n<T>(9,4)*z[70] + n<T>(5,4)*z[58]
     + n<T>(1,2)
   *z[57] - 7*z[22];
    z[58]=z[3] + z[9];
    z[58]=z[58]*z[73];
    z[66]= - n<T>(69,16)*z[2] + 12*z[5] - z[73] - z[72];
    z[66]=z[2]*z[66];
    z[72]= - z[15] + n<T>(1,2)*z[3];
    z[72]=z[3]*z[72];
    z[65]=n<T>(25,8)*z[5] + n<T>(3,2)*z[3] - n<T>(81,4)*z[4] - z[65];
    z[65]=z[5]*z[65];
    z[76]=n<T>(197,8)*z[5] + z[3] - z[62] - z[7] + n<T>(105,8)*z[4];
    z[76]= - n<T>(29,24)*z[8] + n<T>(1,2)*z[76] - 15*z[2];
    z[76]=z[8]*z[76];
    z[57]=z[76] + z[66] + z[65] + z[58] + n<T>(1,2)*z[57] + z[72];
    z[57]=z[8]*z[57];
    z[58]= - n<T>(217,6)*z[9] + z[69] - n<T>(167,3)*z[3];
    z[58]=z[58]*z[73];
    z[65]=81*z[4] - z[69];
    z[65]= - n<T>(101,24)*z[9] + n<T>(1,4)*z[65] + n<T>(73,3)*z[3];
    z[65]=z[5]*z[65];
    z[58]=n<T>(1,2)*z[65] + z[58] + n<T>(139,48)*z[67] - n<T>(13,16)*z[71] - n<T>(13,8)*
    z[75] - n<T>(227,144)*z[22] - n<T>(119,12)*z[17] + 11*z[16];
    z[58]=z[5]*z[58];
    z[65]=n<T>(19,12)*z[11] + 5*z[7] + n<T>(3,2)*z[4];
    z[65]=z[65]*z[62];
    z[66]= - 5*z[34] - n<T>(17,2)*z[32];
    z[59]=z[65] + n<T>(1,4)*z[70] - z[59] + n<T>(983,48)*z[22] + n<T>(1,2)*z[66]
     + 
    z[18];
    z[59]=z[59]*z[62];
    z[62]= - z[15] - n<T>(65,12)*z[4];
    z[62]=z[4]*z[62];
    z[56]=z[62] + n<T>(1,2)*z[70] - n<T>(11,8)*z[22] + n<T>(7,2)*z[31] - z[56];
    z[56]=z[4]*z[56];
    z[62]= - z[14] + z[13];
    z[62]= - z[68] + 5*z[62] + z[12];
    z[62]=z[22]*z[62];
    z[56]=z[62] + z[56];
    z[62]=z[51] + z[52];
    z[65]=z[48] + z[53];
    z[62]= - z[43] - n<T>(9,4)*z[49] - z[50] + n<T>(3,4)*z[55] - z[54] - n<T>(1,2)*
    z[65] + n<T>(1,4)*z[62];
    z[65]=n<T>(1,6)*z[64] + n<T>(11,12)*z[22] - z[36] - z[21];
    z[65]=z[65]*z[74];
    z[66]=n<T>(1,2)*z[6];
    z[67]=z[66] + z[7] - z[74];
    z[66]=z[67]*z[66];
    z[64]=z[66] + n<T>(1,4)*z[64] - n<T>(1,2)*z[36] + z[35];
    z[64]=z[6]*z[64];
    z[66]= - n<T>(61,6)*z[22] - n<T>(187,3)*z[17] - n<T>(211,3)*z[33] - 27*z[32];
    z[66]=n<T>(1,2)*z[66] + 7*z[71];
    z[67]=z[11] - 10*z[3];
    z[67]=z[3]*z[67];
    z[66]=n<T>(1,4)*z[66] + z[67];
    z[66]=z[3]*z[66];
    z[67]=z[27] - z[42];
    z[68]=z[35] + n<T>(17,24)*z[22];
    z[68]=z[7]*z[68];

    r += n<T>(419,48)*z[24] + n<T>(113,48)*z[25] - 23*z[29] - n<T>(807,32)*z[30]
     - 
      n<T>(3,4)*z[38] + n<T>(3,8)*z[39] - n<T>(13,8)*z[40] - n<T>(193,24)*z[41] + n<T>(181,16)*z[44]
     + n<T>(13,16)*z[45] - n<T>(31,16)*z[46]
     + 2*z[47]
     + n<T>(1,4)*z[56]
       + z[57] + z[58] + z[59] + z[60] + z[61] + n<T>(1,2)*z[62] + z[63] + 
      z[64] + z[65] + z[66] - n<T>(95,8)*z[67] + z[68];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf135(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf135(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
