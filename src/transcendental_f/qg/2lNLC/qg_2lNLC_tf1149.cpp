#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1149(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[4];
    z[9]=d[16];
    z[10]=d[12];
    z[11]=d[17];
    z[12]=e[0];
    z[13]=e[4];
    z[14]=e[5];
    z[15]=e[7];
    z[16]=e[8];
    z[17]=e[9];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[31];
    z[25]=e[6];
    z[26]=f[0];
    z[27]=f[1];
    z[28]=f[3];
    z[29]=f[5];
    z[30]=f[11];
    z[31]=f[17];
    z[32]=f[18];
    z[33]=f[31];
    z[34]=f[32];
    z[35]=f[33];
    z[36]=f[36];
    z[37]=f[37];
    z[38]=f[38];
    z[39]=f[39];
    z[40]=f[43];
    z[41]=z[2] + z[6];
    z[42]=z[1]*i;
    z[43]=z[42] - z[3];
    z[44]=n<T>(1,2)*z[7];
    z[45]=n<T>(3,2)*z[4] - z[44] - z[43] - z[41];
    z[46]=n<T>(1,2)*z[3];
    z[45]=z[45]*z[46];
    z[41]=z[42]*z[41];
    z[47]=z[44] - z[42];
    z[48]= - z[47]*z[44];
    z[49]=n<T>(1,2)*z[4];
    z[50]= - z[49] - 3*z[42] + z[7];
    z[50]=z[50]*z[49];
    z[51]=n<T>(1,2)*z[8];
    z[52]=z[42] - z[51];
    z[52]=z[8]*z[52];
    z[41]=z[45] + z[52] + z[50] + z[48] + z[41];
    z[41]=z[41]*z[46];
    z[45]=z[47]*z[7];
    z[48]=z[6] + z[7];
    z[50]=n<T>(3,2)*z[42];
    z[52]=z[50] - z[48];
    z[52]=z[6]*z[52];
    z[53]=z[42] - z[7];
    z[54]=n<T>(3,2)*z[6];
    z[55]=z[54] + z[53];
    z[55]=z[2]*z[55];
    z[56]=n<T>(1,4)*z[7];
    z[57]=z[42] - z[4];
    z[58]= - z[6] + z[56] - z[57];
    z[58]=z[4]*z[58];
    z[52]=z[58] + z[55] - n<T>(3,2)*z[45] + z[52];
    z[49]=z[52]*z[49];
    z[52]=z[3] + z[8];
    z[55]= - z[42]*z[52];
    z[55]= - z[18] + z[55];
    z[55]=z[9]*z[55];
    z[52]= - n<T>(23,6)*z[5] - n<T>(1,3)*z[4] + n<T>(7,6)*z[2] + n<T>(7,12)*z[7]
     + z[6]
    - n<T>(5,12)*z[52];
    z[52]=z[18]*z[52];
    z[43]=z[8] - z[43];
    z[43]=z[13]*z[43];
    z[58]= - z[6] - z[8];
    z[58]=z[25]*z[58];
    z[43]=z[43] + z[52] + z[55] + z[58] - z[40] + z[26] - z[20];
    z[52]=n<T>(1,2)*z[2];
    z[55]=z[52] - z[42];
    z[58]=n<T>(1,2)*z[6];
    z[44]= - z[58] + z[44] - z[55];
    z[44]=z[44]*z[52];
    z[59]= - z[6] - z[50] + z[7];
    z[58]=z[59]*z[58];
    z[44]=z[44] + z[45] + z[58];
    z[44]=z[2]*z[44];
    z[58]=npow(z[6],2);
    z[45]=11*z[45] + 7*z[58];
    z[54]=z[7] + z[54] - z[52];
    z[58]=n<T>(1,2)*z[42] - z[54];
    z[58]=z[2]*z[58];
    z[50]=z[50] + z[54];
    z[50]=z[4]*z[50];
    z[45]=z[50] + n<T>(1,2)*z[45] + z[58];
    z[50]=z[4] - z[2];
    z[54]= - n<T>(1,2)*z[53] + z[6];
    z[54]= - n<T>(13,6)*z[5] + n<T>(5,4)*z[54] - z[50];
    z[54]=z[5]*z[54];
    z[45]=n<T>(1,2)*z[45] + z[54];
    z[45]=z[5]*z[45];
    z[54]=n<T>(5,4)*z[5];
    z[58]= - n<T>(1,6)*z[8] + z[54] + z[52] - n<T>(1,4)*z[53] - z[6];
    z[51]=z[58]*z[51];
    z[56]=z[47]*z[56];
    z[58]=n<T>(1,2)*z[5] + z[53];
    z[54]=z[58]*z[54];
    z[58]= - n<T>(1,4)*z[6] - z[53];
    z[58]=z[6]*z[58];
    z[51]=z[51] + z[54] + z[56] + z[58];
    z[51]=z[8]*z[51];
    z[48]=z[42]*z[48];
    z[48]=z[18] + z[48];
    z[48]=z[11]*z[48];
    z[54]=z[6] - z[53];
    z[54]=z[17]*z[54];
    z[48]=z[48] + z[54];
    z[50]= - 3*z[6] + z[53] - z[50];
    z[50]=n<T>(1,4)*z[50] - z[5];
    z[50]=z[15]*z[50];
    z[50]=z[50] + z[35];
    z[53]=z[42] - n<T>(3,2)*z[7];
    z[53]=z[7]*z[53];
    z[54]= - z[42] - z[7];
    z[54]=n<T>(1,2)*z[54] - n<T>(1,3)*z[6];
    z[54]=z[6]*z[54];
    z[53]=z[53] + z[54];
    z[53]=z[6]*z[53];
    z[54]= - z[4] - z[5];
    z[42]=z[42]*z[54];
    z[42]= - z[18] + z[42];
    z[42]=z[10]*z[42];
    z[46]=z[46] + z[55];
    z[46]=z[12]*z[46];
    z[47]= - z[52] - z[47];
    z[47]=z[16]*z[47];
    z[52]=z[37] + z[29] + z[27] - z[23];
    z[54]=z[30] - z[28];
    z[55]=z[36] + z[31];
    z[56]= - n<T>(1,2)*z[22] + n<T>(5,24)*z[19];
    z[56]=i*z[56];
    z[57]=z[5] - z[57];
    z[57]=z[14]*z[57];

    r +=  - n<T>(7,24)*z[21] - n<T>(5,3)*z[24] + n<T>(1,6)*z[32] + n<T>(15,8)*z[33]
     +  z[34] + z[38] + n<T>(5,8)*z[39] + z[41] + z[42] + n<T>(1,2)*z[43] + z[44]
       + z[45] + z[46] + z[47] + 2*z[48] + z[49] + 5*z[50] + z[51] - n<T>(1,4)*z[52]
     + z[53] - n<T>(3,8)*z[54] - n<T>(1,8)*z[55]
     + z[56]
     + z[57];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1149(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1149(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
