#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf173(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[77];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[21];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[0];
    z[33]=e[3];
    z[34]=e[10];
    z[35]=e[6];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[6];
    z[44]=f[11];
    z[45]=f[12];
    z[46]=f[16];
    z[47]=f[17];
    z[48]=f[18];
    z[49]=f[31];
    z[50]=f[36];
    z[51]=f[39];
    z[52]=f[51];
    z[53]=f[55];
    z[54]=f[56];
    z[55]=f[58];
    z[56]=n<T>(33,8)*z[3];
    z[57]=z[56] - z[6];
    z[58]=n<T>(3,8)*z[9];
    z[59]= - n<T>(2267,288)*z[2] + n<T>(1687,72)*z[5] - z[58] - 6*z[7] + z[14]
    - z[57];
    z[59]=z[2]*z[59];
    z[60]= - z[6] + n<T>(41,4)*z[11];
    z[61]=7*z[7];
    z[62]= - n<T>(307,9)*z[2] + n<T>(4435,144)*z[5] + n<T>(715,48)*z[4] + z[3] + 
    z[61] - z[14] - z[60];
    z[62]=n<T>(1,2)*z[62] - n<T>(14,9)*z[8];
    z[62]=z[8]*z[62];
    z[63]=z[14]*z[7];
    z[64]=npow(z[4],2);
    z[65]=npow(z[6],2);
    z[66]=npow(z[3],2);
    z[67]=npow(z[7],2);
    z[68]= - n<T>(11,2)*z[67] + 11*z[20];
    z[69]= - n<T>(4171,216)*z[22] + n<T>(695,9)*z[19] - z[68];
    z[70]=23*z[3] - 13*z[9];
    z[70]=z[9]*z[70];
    z[71]= - z[6] + n<T>(49,8)*z[11];
    z[71]=z[11]*z[71];
    z[72]=n<T>(551,144)*z[5] + n<T>(3,4)*z[9] - n<T>(337,12)*z[4] - z[6] + n<T>(1,4)*
    z[3];
    z[72]=z[5]*z[72];
    z[59]=z[62] + z[59] + z[72] + z[71] + n<T>(1,8)*z[70] + n<T>(229,32)*z[64]
    + n<T>(1,2)*z[66] - n<T>(15,8)*z[65] + n<T>(1,4)*z[69] - z[63];
    z[59]=z[8]*z[59];
    z[62]=11*z[15];
    z[61]= - z[62] + z[61];
    z[69]=z[8] + z[4];
    z[58]=n<T>(3805,144)*z[2] - n<T>(4867,144)*z[5] - z[58] + n<T>(23,8)*z[3] + n<T>(1,4)
   *z[61] + z[60] - n<T>(9,8)*z[69];
    z[58]=z[8]*z[58];
    z[60]=n<T>(7,2)*z[18] + 5*z[21];
    z[61]=5*z[12] + n<T>(7,2)*z[13];
    z[69]= - n<T>(29,4)*z[4] + z[61];
    z[69]=z[4]*z[69];
    z[68]=z[69] - n<T>(5047,36)*z[16] + n<T>(1081,18)*z[17] - n<T>(5029,36)*z[19]
     + 
    z[68] + z[60];
    z[69]=n<T>(1,2)*z[11];
    z[70]=n<T>(103,16)*z[11] - n<T>(1021,24)*z[9] - z[13] + z[4];
    z[70]=z[70]*z[69];
    z[71]=n<T>(1,2)*z[14];
    z[72]= - z[71] + n<T>(1,2)*z[12] + z[7];
    z[72]=z[14]*z[72];
    z[73]=z[7] + 2*z[6];
    z[73]=z[6]*z[73];
    z[74]= - z[10] + n<T>(1,2)*z[3];
    z[74]=z[3]*z[74];
    z[75]=z[14] + n<T>(115,9)*z[3];
    z[75]=n<T>(1,2)*z[75] + n<T>(44,9)*z[9];
    z[75]=z[9]*z[75];
    z[76]=n<T>(1069,48)*z[11] + n<T>(1423,144)*z[9] - n<T>(5,4)*z[4] - n<T>(259,18)*z[3]
    - n<T>(1081,72)*z[10] + z[6];
    z[76]=z[5]*z[76];
    z[57]= - n<T>(1201,36)*z[2] + n<T>(4507,144)*z[5] + n<T>(25,8)*z[9] + n<T>(27,8)*
    z[4] - z[7] + z[57];
    z[57]=z[2]*z[57];
    z[57]=z[58] + z[57] + z[76] + z[70] + z[75] + n<T>(1973,144)*z[74] + 
    z[73] + z[72] + n<T>(1,4)*z[68];
    z[57]=z[1]*z[57];
    z[58]=2483*z[29] + 1273*z[27];
    z[57]=z[57] - n<T>(1531,864)*z[23] + n<T>(1,36)*z[58] + z[26];
    z[57]=i*z[57];
    z[58]=npow(z[14],2);
    z[68]= - n<T>(6359,54)*z[22] + n<T>(2447,9)*z[16] - 19*z[32] + n<T>(2249,9)*
    z[19];
    z[68]=n<T>(1,8)*z[68] + z[58];
    z[70]=n<T>(2447,144)*z[2] - n<T>(2807,288)*z[5] + z[69] + 3*z[7] - z[71];
    z[70]=z[2]*z[70];
    z[72]= - z[6]*z[7];
    z[73]=z[7] - n<T>(27,16)*z[4];
    z[73]=z[4]*z[73];
    z[56]=z[56] - z[14];
    z[56]=z[9]*z[56];
    z[74]=npow(z[11],2);
    z[75]= - n<T>(2537,144)*z[5] - n<T>(11,4)*z[9] - z[11];
    z[75]=z[5]*z[75];
    z[56]=z[70] + z[75] - n<T>(3,16)*z[74] + z[56] + z[73] + n<T>(1,2)*z[68] + 
    z[72];
    z[56]=z[2]*z[56];
    z[68]= - z[35] - n<T>(7,4)*z[33];
    z[68]= - n<T>(15,2)*z[67] + n<T>(8431,288)*z[22] + n<T>(19,2)*z[68] + z[18];
    z[70]= - n<T>(311,144)*z[11] - n<T>(163,12)*z[9] + n<T>(3,8)*z[4] + n<T>(23,2)*z[3]
    + n<T>(13,2)*z[7] + z[6];
    z[69]=z[70]*z[69];
    z[70]= - z[14] + n<T>(15,8)*z[6];
    z[70]=z[6]*z[70];
    z[72]=npow(z[9],2);
    z[63]=z[69] - n<T>(17,4)*z[72] + 4*z[66] + z[70] + n<T>(1,2)*z[68] + z[63];
    z[63]=z[11]*z[63];
    z[68]=3*z[4] + 7*z[14] + n<T>(451,18)*z[3];
    z[68]=z[9]*z[68];
    z[68]=z[68] + n<T>(3313,216)*z[22] - 9*z[37] - n<T>(577,18)*z[34];
    z[69]=z[4]*z[71];
    z[68]=z[69] + n<T>(677,288)*z[66] - n<T>(3,8)*z[65] - z[58] + n<T>(1,4)*z[68];
    z[68]=z[9]*z[68];
    z[69]=z[6] + z[14];
    z[70]=n<T>(1,2)*z[6];
    z[69]=z[69]*z[70];
    z[69]=z[69] + n<T>(1,2)*z[58] + z[36] + z[67];
    z[69]=z[69]*z[70];
    z[70]= - 241*z[3] - n<T>(289,4)*z[9];
    z[70]=z[9]*z[70];
    z[70]=z[70] + n<T>(1829,16)*z[66] - n<T>(3029,48)*z[22] - n<T>(1081,4)*z[17]
     + 
   325*z[16];
    z[64]=n<T>(7,4)*z[64] + n<T>(1,9)*z[70];
    z[70]= - n<T>(299,32)*z[11] + z[6] + n<T>(1021,48)*z[9];
    z[70]=z[11]*z[70];
    z[72]= - n<T>(1021,4)*z[11] - n<T>(1135,12)*z[9] + n<T>(2099,6)*z[3] + 331*z[4];
    z[72]=z[5]*z[72];
    z[64]=n<T>(1,24)*z[72] + n<T>(1,2)*z[64] + z[70];
    z[64]=z[5]*z[64];
    z[60]= - n<T>(1,2)*z[65] + n<T>(463,144)*z[22] + n<T>(19,4)*z[32] - z[60];
    z[65]= - n<T>(71,16)*z[4] - z[7] - z[71];
    z[65]=z[4]*z[65];
    z[60]=n<T>(1,2)*z[60] + z[65];
    z[60]=z[4]*z[60];
    z[60]= - z[60] + n<T>(3,4)*z[55] + z[54] + z[43] + z[50];
    z[65]= - n<T>(1973,9)*z[17] - n<T>(551,9)*z[34] - 179*z[33];
    z[65]= - n<T>(2075,18)*z[66] + n<T>(1,2)*z[65] + n<T>(335,9)*z[22];
    z[65]=z[3]*z[65];
    z[65]=z[65] - z[52];
    z[61]= - n<T>(1081,18)*z[10] - z[62] + z[61];
    z[61]=z[22]*z[61];
    z[58]=n<T>(1,6)*z[58] + n<T>(31,24)*z[22] + z[36] - z[21];
    z[58]=z[58]*z[71];
    z[62]=z[49] - z[51];
    z[66]=n<T>(37,8)*z[22] + z[67];
    z[66]=z[7]*z[66];

    r += n<T>(4225,288)*z[24] + n<T>(695,216)*z[25] - n<T>(1309,72)*z[28] - n<T>(2519,72)*z[30]
     - n<T>(4999,576)*z[31]
     - n<T>(11,8)*z[38]
     + n<T>(3,16)*z[39]
     + n<T>(5,8)
      *z[40] - n<T>(361,72)*z[41] + n<T>(1345,72)*z[42] + n<T>(4867,288)*z[44] + 
      n<T>(299,32)*z[45] - n<T>(13,32)*z[46] + n<T>(9,16)*z[47] - n<T>(11,24)*z[48]
     - 
      z[53] + z[56] + z[57] + z[58] + z[59] - n<T>(1,2)*z[60] + n<T>(1,4)*z[61]
       - n<T>(15,8)*z[62] + z[63] + z[64] + n<T>(1,8)*z[65] + n<T>(1,6)*z[66] + 
      z[68] + z[69];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf173(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf173(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
