#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1310(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[42];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[4];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[1];
    z[8]=d[3];
    z[9]=d[16];
    z[10]=d[18];
    z[11]=e[0];
    z[12]=e[3];
    z[13]=e[4];
    z[14]=e[8];
    z[15]=e[10];
    z[16]=e[11];
    z[17]=c[3];
    z[18]=f[1];
    z[19]=f[3];
    z[20]=f[4];
    z[21]=f[6];
    z[22]=f[7];
    z[23]=f[9];
    z[24]=f[11];
    z[25]=f[12];
    z[26]=f[14];
    z[27]=f[15];
    z[28]=z[4] + z[3];
    z[29]=i*z[1];
    z[30]= - z[29]*z[28];
    z[30]= - z[17] + z[30];
    z[30]=z[9]*z[30];
    z[31]=z[6] + z[5];
    z[32]=z[29]*z[31];
    z[32]=z[17] + z[32];
    z[32]=z[10]*z[32];
    z[33]=n<T>(1,2)*z[2];
    z[34]=z[33] - z[29];
    z[35]=n<T>(1,2)*z[3];
    z[36]= - z[35] - z[34];
    z[36]=z[11]*z[36];
    z[28]= - z[29] + z[28];
    z[28]=z[13]*z[28];
    z[37]=n<T>(1,2)*z[5];
    z[38]=z[37] + z[34];
    z[38]=z[14]*z[38];
    z[31]=z[29] - z[31];
    z[31]=z[16]*z[31];
    z[28]=z[30] + z[32] + z[36] + z[28] + z[38] + z[31];
    z[30]=z[33] + z[29];
    z[30]=z[30]*z[2];
    z[31]=z[29] - z[2];
    z[32]=z[35] + z[31];
    z[32]=z[3]*z[32];
    z[33]=19*z[29];
    z[36]=z[3] - z[2];
    z[38]= - z[33] - z[36];
    z[38]=z[4]*z[38];
    z[32]=n<T>(1,2)*z[38] + z[30] + z[32];
    z[32]=z[4]*z[32];
    z[31]= - z[37] - z[31];
    z[31]=z[5]*z[31];
    z[38]=z[5] - z[2];
    z[33]=z[33] + z[38];
    z[33]=z[6]*z[33];
    z[30]=n<T>(1,2)*z[33] - z[30] + z[31];
    z[30]=z[6]*z[30];
    z[31]=z[36] + n<T>(19,2)*z[4];
    z[31]=z[31]*z[4];
    z[33]=z[38] + n<T>(19,2)*z[6];
    z[33]=z[33]*z[6];
    z[31]=z[31] - z[33];
    z[33]=z[5] - z[3];
    z[39]=z[33]*z[7];
    z[40]=z[2] + 3*z[29];
    z[41]= - n<T>(3,2)*z[3] + z[40];
    z[41]=z[3]*z[41];
    z[40]=n<T>(3,2)*z[5] - z[40];
    z[40]=z[5]*z[40];
    z[40]= - n<T>(1,2)*z[39] + z[41] + z[40] - z[31];
    z[40]=z[7]*z[40];
    z[35]=z[35] - z[29];
    z[35]= - z[3]*z[35];
    z[37]= - z[29] + z[37];
    z[37]=z[5]*z[37];
    z[31]=z[39] + z[35] + z[37] + z[31];
    z[31]=z[8]*z[31];
    z[35]=z[3] + z[34];
    z[35]=z[3]*z[35];
    z[37]=z[29]*z[2];
    z[35]= - z[37] + z[35];
    z[35]=z[3]*z[35];
    z[34]= - z[5] - z[34];
    z[34]=z[5]*z[34];
    z[34]=z[37] + z[34];
    z[34]=z[5]*z[34];
    z[30]=z[31] + z[40] + z[30] + z[32] + z[35] + z[34];
    z[31]=z[6] - z[4];
    z[31]=z[33] + 7*z[31];
    z[31]=z[17]*z[31];
    z[31]=z[18] - z[31] - z[24] + z[23] + z[19];
    z[29]=z[8] - z[29] + n<T>(15,4)*z[7];
    z[32]=n<T>(19,4)*z[4] + z[36] + z[29];
    z[32]=z[12]*z[32];
    z[29]= - n<T>(19,4)*z[6] - z[38] - z[29];
    z[29]=z[15]*z[29];
    z[33]=z[25] + z[20];
    z[34]=z[26] + z[21];
    z[35]=z[27] + z[22];

    r += n<T>(1,2)*z[28] + z[29] + n<T>(1,4)*z[30] - n<T>(1,8)*z[31] + z[32] - n<T>(19,8)*z[33]
     - n<T>(3,8)*z[34]
     - 4*z[35];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1310(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1310(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
