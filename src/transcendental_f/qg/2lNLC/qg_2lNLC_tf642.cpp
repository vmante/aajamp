#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf642(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[61];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[5];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[11];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[8];
    z[17]=e[12];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[3];
    z[28]=e[10];
    z[29]=e[6];
    z[30]=e[14];
    z[31]=f[3];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[11];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[17];
    z[38]=f[31];
    z[39]=f[36];
    z[40]=f[39];
    z[41]=f[51];
    z[42]=f[55];
    z[43]=f[58];
    z[44]= - z[10] + n<T>(32061,12064)*z[11];
    z[45]=z[44] - n<T>(14719,18096)*z[3];
    z[46]= - z[12] + n<T>(57697,18096)*z[8];
    z[47]=n<T>(9409,36192)*z[6];
    z[48]=n<T>(30553,36192)*z[7] + z[47] + n<T>(13933,18096)*z[2] - n<T>(6393,12064)
   *z[4] - z[46] + z[45];
    z[48]=z[5]*z[48];
    z[49]=3*z[9];
    z[50]= - z[49] + n<T>(46355,6032)*z[11];
    z[50]=z[11]*z[50];
    z[51]= - n<T>(27505,4524)*z[2] + z[4] + n<T>(17703,1508)*z[3];
    z[51]=z[2]*z[51];
    z[50]= - z[13] + z[50] + z[51];
    z[51]=n<T>(1,2)*z[2];
    z[52]=n<T>(16981,72384)*z[7] + z[51] + n<T>(32815,18096)*z[3] + z[4] - z[44]
   ;
    z[52]=z[7]*z[52];
    z[53]=z[17] + n<T>(57697,18096)*z[15];
    z[54]=npow(z[9],2);
    z[55]=z[10]*z[12];
    z[56]=n<T>(1,2)*z[4];
    z[57]=z[56] - z[10] - z[9];
    z[57]=z[4]*z[57];
    z[58]= - 37307*z[8] - n<T>(14687,4)*z[3];
    z[58]=z[3]*z[58];
    z[59]=n<T>(7,8)*z[11] + z[3] + z[9];
    z[60]= - z[6] - n<T>(6401,4524)*z[2] + n<T>(3377,12064)*z[4] + z[59];
    z[60]=z[6]*z[60];
    z[48]=z[48] + z[52] + z[60] + n<T>(1,9048)*z[58] + z[57] - n<T>(7,16)*z[54]
    + z[55] - n<T>(4885,18096)*z[14] + n<T>(4885,9048)*z[16] + z[53] + n<T>(1,4)*
    z[50];
    z[48]=z[1]*z[48];
    z[50]= - z[24] - n<T>(1,2)*z[22];
    z[50]=19*z[50] + n<T>(1489,6)*z[19];
    z[48]=n<T>(19,18096)*z[50] + z[48];
    z[48]=i*z[48];
    z[50]=n<T>(1,2)*z[6];
    z[45]= - n<T>(125557,72384)*z[7] + z[50] + z[51] + z[45];
    z[45]=z[7]*z[45];
    z[47]= - z[47] - n<T>(2623,9048)*z[2] + n<T>(7901,6032)*z[4] - z[3];
    z[47]=z[47]*z[50];
    z[52]=npow(z[11],2);
    z[55]=npow(z[3],2);
    z[57]=z[10] - n<T>(5671,24128)*z[4];
    z[57]=z[4]*z[57];
    z[58]=npow(z[2],2);
    z[60]= - 32061*z[11] - 7901*z[4];
    z[60]= - n<T>(66745,48)*z[7] - n<T>(18457,48)*z[6] + n<T>(2635,3)*z[2] + n<T>(1,16)*
    z[60] + n<T>(10183,3)*z[3];
    z[60]=z[5]*z[60];
    z[45]=n<T>(1,1508)*z[60] + z[45] + z[47] - n<T>(16195,18096)*z[58] + n<T>(37307,18096)*z[55]
     + z[57] - n<T>(46355,24128)*z[52]
     + n<T>(335,754)*z[18] - n<T>(27505,18096)*z[14] - z[53];
    z[45]=z[5]*z[45];
    z[44]= - n<T>(1,6)*z[7] + z[50] + z[2] + n<T>(41863,9048)*z[3] + n<T>(3,2)*z[4]
    - z[9] - z[44];
    z[44]=z[7]*z[44];
    z[47]=npow(z[4],2);
    z[44]=z[47] - z[44];
    z[53]= - z[3] + n<T>(3,8)*z[2];
    z[51]=z[53]*z[51];
    z[53]=n<T>(7,8)*z[6] - z[3] - n<T>(11,4)*z[2];
    z[53]=z[53]*z[50];
    z[57]= - n<T>(50911,16)*z[28] + n<T>(6787,3)*z[18];
    z[44]=z[53] + z[51] + n<T>(32815,36192)*z[55] - n<T>(13965,24128)*z[52] + n<T>(1,1131)*z[57]
     - n<T>(1,4)*z[54]
     - n<T>(1,2)*z[44];
    z[44]=z[7]*z[44];
    z[51]= - n<T>(9409,2)*z[16] + 16195*z[14];
    z[53]= - 20719*z[3] + n<T>(25997,3)*z[2];
    z[53]=z[2]*z[53];
    z[51]=n<T>(1,1508)*z[53] + n<T>(20719,1508)*z[55] - z[47] - n<T>(164669,13572)*
    z[18] + n<T>(1,1131)*z[51] + z[13];
    z[51]=z[2]*z[51];
    z[53]= - 361*z[16] - n<T>(289175,12)*z[18];
    z[53]=n<T>(1,1131)*z[53] + 3*z[54];
    z[49]=z[11]*z[49];
    z[47]=3*z[55] - n<T>(3377,6032)*z[47] + n<T>(1,4)*z[53] + z[49];
    z[49]=z[3] + n<T>(9409,18096)*z[2];
    z[49]=z[2]*z[49];
    z[53]=n<T>(5,8)*z[6] + n<T>(9409,9048)*z[2] - n<T>(7901,12064)*z[4] - z[59];
    z[53]=z[6]*z[53];
    z[47]=z[53] + n<T>(1,2)*z[47] + z[49];
    z[47]=z[47]*z[50];
    z[49]= - z[40] + z[38] + z[39];
    z[50]=z[41] - z[43];
    z[53]=z[23] - z[33];
    z[49]=n<T>(96119,9048)*z[20] - n<T>(361,108576)*z[21] + n<T>(361,9048)*z[25]
     - 
   n<T>(918637,18096)*z[26] + n<T>(6393,6032)*z[31] - n<T>(125557,18096)*z[32] - 
   n<T>(9409,18096)*z[34] + n<T>(46355,6032)*z[35] - n<T>(13965,6032)*z[36] - n<T>(3377,6032)*z[37]
     - z[42]
     + n<T>(361,18096)*z[53]
     + 3*z[50]
     + n<T>(1,4)*z[49];
    z[50]=n<T>(1,2)*z[11];
    z[53]= - z[9] + z[50];
    z[53]=z[53]*z[50];
    z[53]=z[53] - n<T>(3,8)*z[54] + n<T>(74285,18096)*z[18] + n<T>(1,2)*z[29] - 5*
    z[27];
    z[50]=z[53]*z[50];
    z[53]=z[13] - n<T>(16949,9048)*z[18];
    z[53]=n<T>(1,2)*z[53] + z[54];
    z[54]=z[9] - z[10];
    z[54]=z[4]*z[54];
    z[53]=n<T>(1,2)*z[53] + z[54];
    z[53]=z[53]*z[56];
    z[52]=z[52] - n<T>(6563,9048)*z[28] - z[27];
    z[52]= - n<T>(167749,18096)*z[55] + n<T>(25505,6032)*z[18] - n<T>(37307,4524)*
    z[15] + 5*z[52];
    z[52]=z[3]*z[52];
    z[46]=n<T>(1,8)*z[9] - z[46];
    z[46]=z[18]*z[46];
    z[54]= - n<T>(2,3)*z[18] + z[30] - z[17];
    z[54]=z[10]*z[54];

    r += z[44] + z[45] + z[46] + z[47] + z[48] + n<T>(1,4)*z[49] + z[50] + 
      n<T>(1,8)*z[51] + n<T>(1,2)*z[52] + z[53] + z[54];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf642(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf642(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
