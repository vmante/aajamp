#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf646(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[62];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[15];
    z[10]=d[5];
    z[11]=d[4];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[31];
    z[25]=e[3];
    z[26]=e[10];
    z[27]=e[6];
    z[28]=f[0];
    z[29]=f[3];
    z[30]=f[4];
    z[31]=f[5];
    z[32]=f[11];
    z[33]=f[12];
    z[34]=f[16];
    z[35]=f[17];
    z[36]=f[18];
    z[37]=f[31];
    z[38]=f[36];
    z[39]=f[39];
    z[40]=f[51];
    z[41]=f[55];
    z[42]=f[56];
    z[43]=f[58];
    z[44]=f[60];
    z[45]=n<T>(1,2)*z[6];
    z[46]=z[1]*i;
    z[47]=z[45] - z[46];
    z[48]=z[47]*z[6];
    z[49]=n<T>(1,2)*z[2];
    z[50]=z[46] - z[49];
    z[51]=n<T>(24151,3016)*z[2];
    z[50]=z[50]*z[51];
    z[51]= - n<T>(31927,3016)*z[3] + z[51] + n<T>(14303,9048)*z[7] + n<T>(28675,4524)
   *z[5] - n<T>(24151,9048)*z[46] + z[6];
    z[51]=z[3]*z[51];
    z[52]= - n<T>(14303,8)*z[46] + 1081*z[5];
    z[52]=z[5]*z[52];
    z[53]=z[46] - z[5];
    z[54]=14303*z[53] + 18827*z[7];
    z[54]=z[7]*z[54];
    z[50]=n<T>(1,2)*z[51] + z[50] + n<T>(1,9048)*z[54] - z[48] + n<T>(1,1131)*z[52];
    z[50]=z[3]*z[50];
    z[51]=28675*z[3];
    z[52]= - n<T>(2993,2)*z[5] - z[51];
    z[52]=z[46]*z[52];
    z[52]= - n<T>(2993,2)*z[16] + z[52];
    z[52]=z[9]*z[52];
    z[51]=n<T>(2993,2)*z[53] - z[51];
    z[51]=z[14]*z[51];
    z[54]=92765*z[46];
    z[55]= - z[54] + 29383*z[5];
    z[55]=n<T>(1,2)*z[55] + 31691*z[2];
    z[55]=z[13]*z[55];
    z[56]=n<T>(38431,2)*z[2] - 42955*z[46] + n<T>(47479,2)*z[6];
    z[56]=z[15]*z[56];
    z[51]=z[56] + z[51] + z[52] + z[55];
    z[52]= - 8271*z[46] + n<T>(15811,2)*z[6];
    z[52]=z[6]*z[52];
    z[55]=n<T>(21843,2)*z[5] - 12841*z[46] - 21843*z[6];
    z[55]=z[5]*z[55];
    z[52]=z[52] + z[55];
    z[55]=7*z[46];
    z[56]=z[55] + z[2];
    z[49]=z[56]*z[49];
    z[56]=n<T>(1,2)*z[4];
    z[57]=14303*z[6] + 12841*z[5];
    z[57]= - n<T>(9,2)*z[2] + n<T>(1,6032)*z[57] + z[7];
    z[57]=z[57]*z[56];
    z[58]=z[46] + z[7];
    z[59]= - z[7]*z[58];
    z[49]=z[57] + z[49] + n<T>(1,6032)*z[52] + z[59];
    z[49]=z[4]*z[49];
    z[52]=npow(z[7],2);
    z[57]= - z[11] + z[46] - z[6];
    z[57]=z[11]*z[57];
    z[57]=z[52] + z[57];
    z[56]=z[46] - z[56];
    z[56]=z[4]*z[56];
    z[59]=2*z[46];
    z[60]= - z[59] + z[6];
    z[60]=z[6]*z[60];
    z[61]=z[46] - 9*z[6];
    z[61]=z[4] + n<T>(9,8)*z[11] + n<T>(1,8)*z[61] - z[7];
    z[61]=z[10]*z[61];
    z[56]=z[61] + z[56] + z[60] + n<T>(1,2)*z[57];
    z[56]=z[10]*z[56];
    z[57]=n<T>(1,2)*z[7];
    z[53]= - z[57] - z[53];
    z[53]=z[7]*z[53];
    z[60]= - z[46] + n<T>(1,2)*z[5];
    z[60]=z[60]*z[5];
    z[53]=z[60] - z[53];
    z[48]= - 11*z[48] - n<T>(10533,1508)*z[53];
    z[53]=z[3] + z[6];
    z[53]= - n<T>(13,12)*z[11] - n<T>(19627,12064)*z[5] - n<T>(4501,12064)*z[58]
     + 2
   *z[53];
    z[53]=z[11]*z[53];
    z[48]= - n<T>(3,2)*z[27] + n<T>(1,4)*z[48] + z[53];
    z[48]=z[11]*z[48];
    z[53]=22043*z[46] - n<T>(42955,4)*z[6];
    z[45]=z[53]*z[45];
    z[53]=z[54] + 18073*z[6];
    z[53]=n<T>(1,8)*z[53] - 2966*z[5];
    z[53]=z[5]*z[53];
    z[45]=z[45] + z[53];
    z[53]=n<T>(1,4)*z[6] + z[57] + z[46];
    z[53]=z[7]*z[53];
    z[54]= - 31691*z[5] - 147053*z[46] - n<T>(20335,2)*z[6];
    z[54]=n<T>(40975,754)*z[2] + n<T>(1,1131)*z[54] - 5*z[7];
    z[54]=z[2]*z[54];
    z[45]=n<T>(1,8)*z[54] + n<T>(1,1131)*z[45] + z[53];
    z[45]=z[2]*z[45];
    z[53]=npow(z[6],2);
    z[54]= - 3*z[46] + n<T>(19,24)*z[6];
    z[54]=z[54]*z[53];
    z[47]=147007*z[47] + n<T>(29383,2)*z[5];
    z[47]=z[5]*z[6]*z[47];
    z[53]= - z[53] - n<T>(2993,2262)*z[60];
    z[46]=79901*z[46] - 61805*z[5];
    z[46]=n<T>(1,6032)*z[46] - z[7];
    z[46]=z[7]*z[46];
    z[46]=n<T>(1,4)*z[53] + n<T>(1,3)*z[46];
    z[46]=z[46]*z[57];
    z[53]=z[59] - z[2];
    z[53]=z[2]*z[53];
    z[52]=z[16] + z[52] + z[53];
    z[52]=z[8]*z[52];
    z[53]=n<T>(9,2)*z[4] - z[55] + n<T>(5,2)*z[2];
    z[53]=z[12]*z[53];
    z[53]=z[40] + z[41] + z[53] - z[43];
    z[55]= - z[37] + z[39] - z[38];
    z[57]= - n<T>(373207,24)*z[6] - 1349*z[5];
    z[57]= - n<T>(17365,32)*z[4] + n<T>(36169,16)*z[11] + n<T>(28675,16)*z[3] - 
   n<T>(151669,48)*z[2] + n<T>(1,4)*z[57] + n<T>(3343,3)*z[7];
    z[57]=n<T>(1,377)*z[57] - n<T>(7,4)*z[10];
    z[57]=z[16]*z[57];
    z[57]=z[57] - z[36];
    z[58]=n<T>(210343,9048)*z[22] + n<T>(201295,18096)*z[20] - n<T>(44509,54288)*
    z[17];
    z[58]=i*z[58];
    z[59]= - z[3] - z[11];
    z[59]=z[25]*z[59];
    z[60]= - 23351*z[7] - 14303*z[3];
    z[60]=z[26]*z[60];

    r += n<T>(60343,18096)*z[18] + n<T>(29383,217152)*z[19] - n<T>(204311,36192)*
      z[21] - n<T>(210343,18096)*z[23] - n<T>(402797,36192)*z[24] - z[28] + 
      n<T>(12841,12064)*z[29] - n<T>(61805,36192)*z[30] + n<T>(210343,36192)*z[31]
       + n<T>(147007,36192)*z[32] + n<T>(19627,12064)*z[33] - n<T>(4501,12064)*
      z[34] + n<T>(8271,12064)*z[35] - z[42] + z[44] + z[45] + z[46] + n<T>(1,18096)*z[47]
     + z[48]
     + z[49]
     + z[50]
     + n<T>(1,4524)*z[51]
     + z[52]
     + n<T>(1,2)*z[53]
     + z[54]
     + n<T>(3,8)*z[55]
     + z[56]
     + n<T>(1,3)*z[57]
     + z[58]
     + 2
      *z[59] + n<T>(1,9048)*z[60];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf646(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf646(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
