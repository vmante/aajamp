#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1428(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[19];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[6];
    z[4]=d[16];
    z[5]=d[4];
    z[6]=e[4];
    z[7]=e[6];
    z[8]=c[3];
    z[9]=c[15];
    z[10]=c[31];
    z[11]=f[37];
    z[12]=z[1]*i;
    z[13]= - z[4] + 2*z[3];
    z[13]=z[13]*z[12];
    z[14]=z[6] - z[7];
    z[15]= - z[14] + n<T>(1,6)*z[8];
    z[16]=npow(z[5],2);
    z[17]=z[12] - z[5];
    z[17]= - z[3] - n<T>(1,2)*z[17];
    z[18]=n<T>(1,2)*z[2] + z[17];
    z[18]=z[2]*z[18];
    z[13]=z[18] - n<T>(1,2)*z[16] + z[13] - z[15];
    z[13]=z[2]*z[13];
    z[16]= - z[4]*z[12];
    z[17]= - z[5]*z[17];
    z[15]=z[17] + z[16] - z[15];
    z[15]=z[5]*z[15];
    z[12]= - z[14]*z[12];
    z[14]=z[3] - z[4];
    z[14]=z[8]*z[14];

    r += z[9] - n<T>(3,2)*z[10] - z[11] + z[12] + z[13] + z[14] + z[15];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1428(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1428(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
