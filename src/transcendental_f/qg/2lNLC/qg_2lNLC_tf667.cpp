#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf667(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[53];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[6];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[31];
    z[25]=e[10];
    z[26]=f[3];
    z[27]=f[4];
    z[28]=f[5];
    z[29]=f[11];
    z[30]=f[12];
    z[31]=f[16];
    z[32]=f[17];
    z[33]=f[31];
    z[34]=f[36];
    z[35]=f[39];
    z[36]=npow(z[9],2);
    z[37]=n<T>(6529,8)*z[36];
    z[38]=npow(z[4],2);
    z[39]=npow(z[3],2);
    z[40]=n<T>(9665,24)*z[16] + n<T>(8791,2)*z[13] - 437*z[12];
    z[41]=n<T>(2699,3)*z[3];
    z[42]= - n<T>(6529,4)*z[9] + z[41] + n<T>(2005,24)*z[7];
    z[43]=z[7]*z[42];
    z[44]=npow(z[2],2);
    z[45]=n<T>(6349,12)*z[6] + 784*z[4] - n<T>(4267,6)*z[2];
    z[45]=z[6]*z[45];
    z[46]=n<T>(6529,8)*z[9];
    z[47]= - n<T>(437,12)*z[6] + n<T>(5141,12)*z[2] + n<T>(8791,24)*z[7] - n<T>(14189,12)
   *z[3] - 392*z[4] + z[46];
    z[47]=z[5]*z[47];
    z[40]=z[47] + z[45] - n<T>(11053,6)*z[44] + z[43] + n<T>(8791,6)*z[39] - 
    z[37] + n<T>(1,3)*z[40] + n<T>(4961,4)*z[38];
    z[43]=n<T>(1,377)*z[5];
    z[40]=z[40]*z[43];
    z[44]=n<T>(1,377)*z[7];
    z[42]= - z[42]*z[44];
    z[45]= - n<T>(6529,2)*z[9] + n<T>(8791,3)*z[8] - 4961*z[4];
    z[45]= - n<T>(6349,6)*z[6] + n<T>(10616,3)*z[2] - n<T>(8791,12)*z[7] + n<T>(1,2)*
    z[45] + z[41];
    z[43]=z[45]*z[43];
    z[45]=npow(z[10],2);
    z[45]=n<T>(9,4)*z[45];
    z[47]=z[45] + z[14];
    z[48]=3*z[11];
    z[49]= - z[8] - n<T>(1,4)*z[3];
    z[49]=z[3]*z[49];
    z[50]= - n<T>(14009,1131)*z[2] + 3*z[4] + n<T>(8791,754)*z[3];
    z[50]=z[2]*z[50];
    z[51]=3*z[10];
    z[52]=n<T>(1645,2262)*z[2] - n<T>(9,2)*z[9] + z[51] + n<T>(2699,754)*z[4];
    z[52]=z[6]*z[52];
    z[42]=z[43] + z[52] + z[50] + z[42] + n<T>(8791,1131)*z[49] + n<T>(15577,3016)*z[36]
     - z[48]
     - n<T>(10616,1131)*z[12]
     - n<T>(8791,2262)*z[13]
     + n<T>(874,1131)*z[15] - z[47];
    z[42]=z[1]*z[42];
    z[43]=z[22] + n<T>(1,2)*z[20];
    z[43]=2627*z[43] + n<T>(365,6)*z[17];
    z[42]=n<T>(5,1131)*z[43] + z[42];
    z[42]=i*z[42];
    z[43]=3*z[7];
    z[49]=n<T>(437,1131)*z[2];
    z[50]=z[43] + z[49];
    z[50]=z[2]*z[50];
    z[52]=n<T>(3,2)*z[10];
    z[49]= - n<T>(1,4)*z[6] + z[49] - n<T>(3,4)*z[7] + n<T>(9,4)*z[9] - z[52] - n<T>(392,377)*z[4];
    z[49]=z[6]*z[49];
    z[36]=z[49] + n<T>(1,2)*z[50] - 3*z[36] - n<T>(2699,1508)*z[38] - n<T>(15397,13572)*z[16]
     - n<T>(437,1131)*z[15]
     + z[47];
    z[36]=z[6]*z[36];
    z[41]=z[46] - z[41];
    z[41]=z[7]*z[41];
    z[46]=2699*z[25];
    z[47]=z[46] - n<T>(14189,12)*z[16];
    z[37]=z[41] - n<T>(2699,6)*z[39] + n<T>(1,3)*z[47] + z[37];
    z[37]=z[37]*z[44];
    z[38]=z[38] - z[11];
    z[41]= - 437*z[15] + 11053*z[12];
    z[43]= - n<T>(8791,377)*z[3] - z[43];
    z[43]=n<T>(1,4)*z[43] + n<T>(6439,1131)*z[2];
    z[43]=z[2]*z[43];
    z[38]=z[43] + n<T>(8791,1508)*z[39] - n<T>(22543,6786)*z[16] + n<T>(1,1131)*
    z[41] - n<T>(3,2)*z[38];
    z[38]=z[2]*z[38];
    z[41]=z[30] + z[31];
    z[43]=z[33] - z[35] + z[34];
    z[41]= - n<T>(50227,2262)*z[24] + n<T>(4961,377)*z[26] + n<T>(2005,2262)*z[27]
    + n<T>(13135,1131)*z[28] + n<T>(6349,1131)*z[29] + 3*z[43] - n<T>(2699,377)*
    z[32] + n<T>(6529,754)*z[41];
    z[43]=n<T>(8791,1131)*z[8] + z[51];
    z[43]=z[16]*z[43];
    z[44]=z[48] - n<T>(1915,1131)*z[16];
    z[44]=z[4]*z[44];
    z[41]=z[44] + z[43] + n<T>(8791,2262)*z[18] + n<T>(13135,13572)*z[19] - 
   n<T>(13135,2262)*z[21] + n<T>(1,2)*z[41] - n<T>(13135,1131)*z[23];
    z[43]=z[52] + z[9];
    z[43]=z[9]*z[43];
    z[43]=z[43] - n<T>(3433,1508)*z[16] + 2*z[14] - z[45];
    z[43]=z[9]*z[43];
    z[39]= - 3046*z[39] + n<T>(6529,4)*z[16] + z[46] - 8791*z[13];
    z[39]=z[3]*z[39];

    r += z[36] + z[37] + z[38] + n<T>(1,1131)*z[39] + z[40] + n<T>(1,2)*z[41]
       + z[42] + z[43];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf667(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf667(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
