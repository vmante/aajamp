#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf561(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[16];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[6];
    z[3]=d[18];
    z[4]=d[0];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=d[5];
    z[9]=d[9];
    z[10]=e[11];
    z[11]=z[7] - z[8];
    z[11]=z[9]*z[11];
    z[12]= - z[7] + n<T>(1,2)*z[6];
    z[12]=z[6]*z[12];
    z[13]=z[5] - n<T>(1,2)*z[4];
    z[13]=z[4]*z[13];
    z[14]=z[1]*i;
    z[15]= - z[3]*z[14];
    z[14]=z[14] + z[8] - z[5];
    z[14]=z[2]*z[14];

    r += z[10] + z[11] + z[12] + z[13] + z[14] + z[15];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf561(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf561(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
