#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf542(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[11];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[1];
    z[5]=d[8];
    z[6]=d[9];
    z[7]=z[5]*z[6];
    z[8]= - z[5] + n<T>(1,2)*z[4];
    z[8]=z[4]*z[8];
    z[9]=z[1]*i;
    z[10]= - z[6] - z[9];
    z[10]=z[3]*z[10];
    z[9]= - n<T>(1,2)*z[2] + z[9] + z[3];
    z[9]=z[2]*z[9];

    r += z[7] + z[8] + z[9] + z[10];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf542(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf542(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d
);
#endif
