#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1337(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[31];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=c[3];
    z[9]=c[19];
    z[10]=c[23];
    z[11]=c[25];
    z[12]=c[26];
    z[13]=c[28];
    z[14]=c[30];
    z[15]=c[31];
    z[16]=f[0];
    z[17]=f[3];
    z[18]=f[11];
    z[19]=f[13];
    z[20]=f[17];
    z[21]=f[20];
    z[22]=npow(z[3],2);
    z[23]= - z[3] + z[2];
    z[23]=z[2]*z[23];
    z[24]=z[4]*z[3];
    z[23]=n<T>(1,4)*z[24] - n<T>(3,4)*z[22] + z[23];
    z[23]=z[4]*z[23];
    z[24]=n<T>(1,2)*z[5];
    z[25]=z[1]*i;
    z[26]=z[24] - z[25];
    z[27]=z[4] - z[3];
    z[26]=z[27]*z[26];
    z[27]=n<T>(1,2)*z[4];
    z[28]=z[3] - z[27];
    z[28]=z[4]*z[28];
    z[26]= - n<T>(1,2)*z[22] + z[28] + z[26];
    z[26]=z[26]*z[24];
    z[28]=n<T>(1,2)*z[3] - z[2];
    z[28]=z[2]*z[28];
    z[22]=z[22] + z[28];
    z[22]=z[2]*z[22];
    z[28]= - 2*z[3] + n<T>(3,2)*z[2];
    z[28]=z[2]*z[28];
    z[29]=n<T>(3,2)*z[3];
    z[30]=z[29] - z[2];
    z[30]=z[4]*z[30];
    z[28]=z[28] + z[30];
    z[28]=z[28]*z[25];
    z[30]= - z[2] + z[25];
    z[30]=z[7]*z[30];
    z[22]= - z[30] - z[26] - z[28] - z[22] - z[23] + z[16] - z[14];
    z[23]= - z[24] + z[27] + z[29] + z[2];
    z[23]=z[8]*z[23];
    z[24]=z[19] + z[17];
    z[26]=z[20] - z[18];
    z[27]= - 15*z[12] - n<T>(9,2)*z[10];
    z[27]=i*z[27];
    z[25]=2*z[25] - z[3] - z[2];
    z[25]=z[6]*z[25];

    r +=  - n<T>(3,8)*z[9] + n<T>(13,4)*z[11] + n<T>(21,2)*z[13] + n<T>(7,2)*z[15]
     +  z[21] - 3*z[22] + n<T>(1,2)*z[23] - n<T>(9,4)*z[24] + 6*z[25] - n<T>(3,4)*
      z[26] + z[27];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1337(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1337(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
