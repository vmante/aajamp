#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf919(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[118];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=e[0];
    z[10]=e[1];
    z[11]=e[8];
    z[12]=d[6];
    z[13]=d[15];
    z[14]=d[5];
    z[15]=d[4];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=e[2];
    z[19]=e[5];
    z[20]=e[9];
    z[21]=f[0];
    z[22]=f[3];
    z[23]=f[5];
    z[24]=f[8];
    z[25]=f[11];
    z[26]=f[13];
    z[27]=f[17];
    z[28]=f[20];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[18];
    z[33]=c[19];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=c[29];
    z[39]=c[30];
    z[40]=c[31];
    z[41]=c[47];
    z[42]=c[51];
    z[43]=c[54];
    z[44]=c[55];
    z[45]=c[59];
    z[46]=c[61];
    z[47]=c[62];
    z[48]=c[66];
    z[49]=c[68];
    z[50]=c[69];
    z[51]=c[72];
    z[52]=c[75];
    z[53]=c[83];
    z[54]=e[3];
    z[55]=e[10];
    z[56]=e[6];
    z[57]=e[7];
    z[58]=e[13];
    z[59]=e[14];
    z[60]=f[4];
    z[61]=f[6];
    z[62]=f[12];
    z[63]=f[16];
    z[64]=f[31];
    z[65]=f[36];
    z[66]=f[39];
    z[67]=f[51];
    z[68]=f[52];
    z[69]=f[55];
    z[70]=f[56];
    z[71]=f[58];
    z[72]=g[0];
    z[73]=g[3];
    z[74]=g[5];
    z[75]=g[8];
    z[76]=g[11];
    z[77]=g[13];
    z[78]=g[17];
    z[79]=g[21];
    z[80]=g[29];
    z[81]=g[30];
    z[82]=g[35];
    z[83]=g[41];
    z[84]=g[42];
    z[85]=g[43];
    z[86]=g[56];
    z[87]=g[57];
    z[88]=g[76];
    z[89]=g[94];
    z[90]=g[107];
    z[91]=g[125];
    z[92]=2*z[12];
    z[93]=2*z[6];
    z[94]=npow(z[3],2);
    z[95]=2*z[8];
    z[96]= - n<T>(14015,36)*z[29] - 42*z[9] - 23*z[10] - n<T>(62,3)*z[11];
    z[97]= - n<T>(230,9)*z[2] + n<T>(145869,12064) + n<T>(1234,3)*z[3];
    z[97]=z[2]*z[97];
    z[96]=z[97] - 311*z[94] - n<T>(121,8)*z[7] + z[93] + z[92] + 5*z[96] + 
    z[95];
    z[96]=z[2]*z[96];
    z[97]=z[2]*z[1];
    z[98]=1 + n<T>(1465,2)*z[3];
    z[98]=z[1]*z[98];
    z[98]=z[98] - 1081*z[97];
    z[98]=i*z[98];
    z[99]=i*z[1];
    z[100]=n<T>(43,8) - n<T>(107,3)*z[3];
    z[100]=n<T>(791,9)*z[5] - n<T>(3164,9)*z[99] + n<T>(1,2)*z[100] + n<T>(583,9)*z[2];
    z[100]=z[5]*z[100];
    z[101]=241*z[10];
    z[102]= - n<T>(268711,24128) - 122*z[3];
    z[102]=z[3]*z[102];
    z[103]=n<T>(13535,6032) - n<T>(38,3)*z[3];
    z[103]=11*z[103] + n<T>(1717,6)*z[2];
    z[103]=z[2]*z[103];
    z[98]=z[100] + z[98] + z[103] + z[102] + n<T>(137,16)*z[7] - n<T>(7,4)*z[14]
    + n<T>(3,2)*z[6] - 12*z[12] - z[8] - n<T>(5327,9)*z[29] + n<T>(91,16)*z[15] + 
   30*z[9] - z[101] + 264*z[11];
    z[98]=z[5]*z[98];
    z[100]=n<T>(1,2)*z[3];
    z[102]=n<T>(74179,6032) + 1735*z[3];
    z[102]=z[102]*z[100];
    z[103]=6*z[12];
    z[104]=115*z[10];
    z[105]=168*z[9] + n<T>(133,3)*z[11] + 6*z[17] + z[104];
    z[102]=z[102] + n<T>(7,2)*z[14] - 3*z[6] + z[103] + 2*z[105] - n<T>(91,8)*
    z[15];
    z[102]=z[1]*z[102];
    z[105]= - n<T>(91219,1508) + n<T>(770,3)*z[3];
    z[105]=z[1]*z[105];
    z[105]=z[105] - n<T>(3125,3)*z[97];
    z[105]=z[2]*z[105];
    z[102]=z[105] + z[102] + n<T>(6725,18)*z[30] + 460*z[36] + 141*z[34];
    z[102]=i*z[102];
    z[101]= - 87*z[9] + z[101] - n<T>(112,3)*z[11];
    z[105]= - n<T>(98307,24128) - n<T>(256,3)*z[3];
    z[105]=z[3]*z[105];
    z[101]=z[105] + 2*z[101] + n<T>(50531,36)*z[29];
    z[101]=z[3]*z[101];
    z[105]=12*z[20];
    z[106]=n<T>(241,3)*z[28];
    z[107]=npow(z[8],2);
    z[108]=n<T>(1,2)*z[107];
    z[109]=z[12]*z[8];
    z[110]=npow(z[6],2);
    z[111]=npow(z[15],2);
    z[112]= - 5*z[15] + n<T>(169,4)*z[14];
    z[112]=z[14]*z[112];
    z[113]=z[6] - n<T>(1,2)*z[7];
    z[113]=z[7]*z[113];
    z[96]=z[98] + z[102] + z[96] + z[101] + n<T>(23,2)*z[113] + n<T>(1,4)*z[112]
    - n<T>(25,4)*z[110] - z[109] - z[108] + n<T>(174521,72384)*z[29] - n<T>(15,4)*
    z[111] - n<T>(151901,6032)*z[11] + n<T>(101,2)*z[22] - 148*z[23] + n<T>(151,2)*
    z[25] - n<T>(111,2)*z[27] - 145*z[21] + 564*z[24] - 32*z[26] + z[106] + 
   n<T>(57,2)*z[31] - 30*z[33] + n<T>(109,3)*z[35] - 160*z[37] + n<T>(113365,36)*
    z[40] + 114*z[32] - n<T>(171,2)*z[38] + z[105] - 431*z[39];
    z[96]=z[5]*z[96];
    z[98]=n<T>(292839,6032) - 113*z[3];
    z[98]=z[98]*z[100];
    z[101]=n<T>(545489,12064) - n<T>(2234,3)*z[3];
    z[101]=z[1]*z[101];
    z[101]=z[101] + n<T>(40,3)*z[97];
    z[101]=i*z[101];
    z[102]=4*z[12];
    z[112]=2*z[7];
    z[113]=n<T>(404,3)*z[2] - n<T>(78763,6032) - n<T>(536,3)*z[3];
    z[113]=z[2]*z[113];
    z[114]= - n<T>(845,18)*z[5] + n<T>(697,2)*z[99] + n<T>(1361,3)*z[2] - n<T>(545489,24128)
     - n<T>(887,3)*z[3];
    z[114]=z[5]*z[114];
    z[98]=z[114] + z[101] + z[113] + z[98] - z[112] - z[93] + z[102] + 
   n<T>(11999,18)*z[29] - 12*z[9] - z[104] - n<T>(1316,3)*z[11];
    z[98]=z[5]*z[98];
    z[101]=z[1]*z[3];
    z[101]= - n<T>(4595,6)*z[101] + 117*z[97];
    z[101]=i*z[101];
    z[104]= - n<T>(135313,12064) + n<T>(4883,3)*z[3];
    z[104]= - n<T>(1097,6)*z[5] + n<T>(3893,6)*z[99] + n<T>(1,2)*z[104] - n<T>(1517,3)*
    z[2];
    z[104]=z[5]*z[104];
    z[113]=2*z[9] + 79*z[10] + 9*z[11];
    z[114]= - n<T>(292839,24128) + 206*z[3];
    z[114]=z[3]*z[114];
    z[115]=n<T>(2242,3)*z[2] + n<T>(53519,3016) - n<T>(3236,3)*z[3];
    z[115]=z[2]*z[115];
    z[99]=n<T>(1643,6)*z[4] - n<T>(1774,3)*z[5] - n<T>(3286,3)*z[99] - n<T>(82,3)*z[2]
    + n<T>(1,2) + n<T>(1856,3)*z[3];
    z[99]=z[4]*z[99];
    z[99]=n<T>(1,3)*z[99] + z[104] + z[101] + z[115] + z[114] + n<T>(98367,24128)
   *z[7] - n<T>(52011,3016)*z[6] - n<T>(3,2)*z[12] + z[8] - n<T>(33839,72)*z[29]
     + 
   3*z[113] + n<T>(329785,24128)*z[15];
    z[99]=z[4]*z[99];
    z[101]=4042*z[9];
    z[104]=n<T>(713,6)*z[29] - z[101] - 1694*z[10] - 1343*z[11];
    z[113]= - n<T>(3085,9)*z[2] + n<T>(162517,6032) + n<T>(1003,3)*z[3];
    z[113]=z[2]*z[113];
    z[92]=z[113] + n<T>(148,3)*z[94] + z[112] - 5*z[14] + n<T>(1,3)*z[104] - 
    z[92];
    z[92]=z[2]*z[92];
    z[104]=n<T>(314885,6032) - 501*z[3];
    z[100]=z[104]*z[100];
    z[104]= - z[16] + n<T>(116463,3016)*z[13];
    z[100]=z[100] - n<T>(98367,12064)*z[7] + n<T>(11,2)*z[14] + n<T>(103645,6032)*
    z[6] + z[12] - z[95] - n<T>(329785,12064)*z[15] + n<T>(8084,3)*z[9] + n<T>(2632,3)*z[11]
     + n<T>(1,2)*z[104] - n<T>(2402,3)*z[10];
    z[100]=z[1]*z[100];
    z[104]= - n<T>(460347,6032) + n<T>(4706,3)*z[3];
    z[104]=z[1]*z[104];
    z[104]=z[104] - n<T>(2806,3)*z[97];
    z[104]=z[2]*z[104];
    z[112]= - 1585*z[36] - 166*z[34];
    z[100]=z[104] + z[100] + 2*z[112] + n<T>(8933,36)*z[30];
    z[100]=i*z[100];
    z[104]=z[19] + n<T>(116463,3016)*z[18];
    z[112]= - z[15] - z[8];
    z[112]=z[12]*z[112];
    z[113]=z[12] - z[8];
    z[114]= - n<T>(157993,6032)*z[6] - z[113];
    z[114]=z[6]*z[114];
    z[102]=z[14]*z[102];
    z[115]=z[8] + n<T>(317721,12064)*z[15];
    z[116]=n<T>(621643,24128)*z[7] + z[14] + n<T>(109677,6032)*z[6] - z[115];
    z[116]=z[7]*z[116];
    z[117]= - n<T>(314885,24128) - n<T>(463,9)*z[3];
    z[117]=z[3]*z[117];
    z[117]=z[117] - n<T>(8621,18)*z[29] - n<T>(4042,3)*z[9] + n<T>(3019,3)*z[10]
     - 
   45*z[11];
    z[117]=z[3]*z[117];
    z[92]=z[99] + z[98] + z[100] + z[92] + z[117] + z[116] + z[102] + 
    z[114] + z[112] - z[107] + n<T>(1060,377)*z[29] + n<T>(231191,24128)*z[111]
    - n<T>(105153,6032)*z[10] + n<T>(926,3)*z[22] - n<T>(1520,3)*z[23] - n<T>(224,3)*
    z[25] - 400*z[27] - 1262*z[21] + 18*z[24] - n<T>(211,3)*z[26] + n<T>(1262,3)
   *z[28] + n<T>(271,2)*z[31] - n<T>(577,6)*z[33] + n<T>(1358,3)*z[35] + 2034*z[37]
    + n<T>(93943,18)*z[40] + 542*z[32] - n<T>(813,2)*z[38] + n<T>(1,2)*z[104] - 373
   *z[39];
    z[92]=z[4]*z[92];
    z[98]=z[103] - z[95];
    z[99]=7*z[17] + z[15] - z[98];
    z[99]=z[12]*z[99];
    z[100]= - z[16] + n<T>(5,2)*z[15];
    z[98]= - n<T>(157,16)*z[14] + n<T>(1,2)*z[100] - z[98];
    z[98]=z[14]*z[98];
    z[100]=z[7] - z[14];
    z[101]=z[101] - 2125*z[10] + 242*z[11];
    z[101]=n<T>(1,3)*z[101] - z[100];
    z[101]=2*z[101] + n<T>(337,9)*z[94];
    z[101]=z[3]*z[101];
    z[102]=n<T>(1,2)*z[19];
    z[103]=n<T>(76561,12064)*z[6] + n<T>(154977,3016)*z[13] + z[113];
    z[103]=z[6]*z[103];
    z[104]= - n<T>(488939,24128)*z[7] - n<T>(166981,6032)*z[6] + z[115];
    z[104]=z[7]*z[104];
    z[98]=z[101] + z[104] + z[98] + z[103] + z[99] + n<T>(3,2)*z[107] - 
   n<T>(140711,24128)*z[111] + n<T>(129,4)*z[9] + n<T>(154917,3016)*z[11] + n<T>(430187,6032)*z[10]
     - n<T>(2735,6)*z[22]
     + n<T>(2894,3)*z[23]
     + n<T>(793,6)*z[25]
     + n<T>(1469,2)*z[27]
     + 1592*z[21] - 576*z[24]
     + n<T>(992,3)*z[26] - n<T>(1928,3)*z[28]
    - n<T>(116463,6032)*z[18] - z[105] - z[102];
    z[98]=z[1]*z[98];
    z[99]=92*z[10] + 193*z[11];
    z[99]=11*z[99] + 4013*z[9];
    z[99]=n<T>(1,3)*z[99] - z[8];
    z[101]= - n<T>(129,4) - 617*z[3];
    z[101]=z[3]*z[101];
    z[99]=z[101] - 5*z[7] + 2*z[99] - n<T>(317147,6032)*z[6];
    z[99]=z[1]*z[99];
    z[101]=n<T>(689563,6032) - n<T>(3142,3)*z[3];
    z[101]=z[1]*z[101];
    z[97]=z[101] + n<T>(20485,9)*z[97];
    z[97]=z[2]*z[97];
    z[101]= - n<T>(6272,3)*z[30] - 3155*z[36] - 1066*z[34];
    z[97]=z[97] + n<T>(1,3)*z[101] + z[99];
    z[97]=z[2]*z[97];
    z[99]= - n<T>(8279,12)*z[30] - 2477*z[36] - 826*z[34];
    z[99]=z[3]*z[99];
    z[99]=z[99] + n<T>(6272,3)*z[41] - 11141*z[48] - n<T>(17287,3)*z[45];
    z[97]=z[97] + z[98] + n<T>(372883,36192)*z[30] - n<T>(934553,12064)*z[34] - 
   n<T>(934553,6032)*z[36] + n<T>(1,3)*z[99];
    z[97]=i*z[97];
    z[98]= - n<T>(73,2)*z[9] + 5*z[10] - 47*z[11];
    z[98]=23*z[98] + n<T>(24523,4)*z[29];
    z[99]= - 1 + n<T>(1405,6)*z[3];
    z[99]=z[3]*z[99];
    z[101]= - n<T>(1667,18)*z[2] - n<T>(122531,12064) - n<T>(100,9)*z[3];
    z[101]=z[2]*z[101];
    z[98]=5*z[101] + z[99] + n<T>(145,16)*z[7] + n<T>(5,2)*z[14] + n<T>(1,3)*z[98]
    + n<T>(305083,12064)*z[6];
    z[98]=z[2]*z[98];
    z[99]=2*z[14];
    z[101]=z[113]*z[99];
    z[103]= - n<T>(8423,4)*z[29] - 1675*z[9] - 215*z[10] - 431*z[11];
    z[104]=n<T>(137,8) + n<T>(1231,9)*z[3];
    z[104]=z[3]*z[104];
    z[103]=n<T>(1,3)*z[103] + z[104];
    z[103]=z[3]*z[103];
    z[104]=npow(z[12],2);
    z[105]= - 965*z[39] - n<T>(653,2)*z[38];
    z[93]= - z[93] - n<T>(3,2)*z[7];
    z[93]=z[7]*z[93];
    z[93]=z[98] + z[103] + z[93] + z[101] - n<T>(305083,12064)*z[110] - 
    z[104] + n<T>(670079,36192)*z[29] - n<T>(121,8)*z[9] - n<T>(157933,6032)*z[11]
    - n<T>(162517,3016)*z[10] + n<T>(137,3)*z[22] - n<T>(478,3)*z[23] - n<T>(202,3)*
    z[25] - 156*z[27] - 37*z[21] - 120*z[24] - n<T>(565,3)*z[26] + n<T>(184,3)*
    z[28] + n<T>(653,12)*z[31] - n<T>(1043,18)*z[33] + n<T>(1121,6)*z[35] + n<T>(1150,3)
   *z[37] - n<T>(192157,72)*z[40] + n<T>(1,2)*z[105] + n<T>(653,3)*z[32];
    z[93]=z[2]*z[93];
    z[98]=n<T>(7,4)*z[7];
    z[101]= - n<T>(185077,3016)*z[6] + n<T>(293593,12064)*z[15] - z[8];
    z[99]= - z[98] + n<T>(1,2)*z[101] + z[99];
    z[99]=z[7]*z[99];
    z[101]=185077*z[55] + n<T>(142793,4)*z[111];
    z[101]=n<T>(1,2)*z[101] - n<T>(145507,3)*z[29];
    z[101]=n<T>(1,1508)*z[101] + 3*z[107];
    z[103]= - z[12] - n<T>(11,4)*z[14];
    z[103]=z[14]*z[103];
    z[99]=z[99] + z[103] - n<T>(166981,12064)*z[110] + n<T>(1,2)*z[101] + z[109]
   ;
    z[99]=z[7]*z[99];
    z[101]=17*z[11] - n<T>(205,2)*z[9];
    z[94]= - n<T>(337,36)*z[94] + 3*z[101] + n<T>(6053,72)*z[29] + z[100];
    z[94]=z[3]*z[94];
    z[100]=53*z[39] - n<T>(1,2)*z[38];
    z[100]= - n<T>(4481,72)*z[40] + n<T>(1,2)*z[100] + n<T>(1,3)*z[32];
    z[101]=npow(z[14],2);
    z[98]= - z[14] + z[98];
    z[98]=z[7]*z[98];
    z[94]=z[94] + z[98] - n<T>(3,4)*z[101] + n<T>(162577,72384)*z[29] - n<T>(137,8)*
    z[9] + 51*z[22] - n<T>(452,3)*z[23] - n<T>(197,3)*z[25] - 123*z[27] - 148*
    z[21] + 114*z[24] - 40*z[26] + z[106] + n<T>(5,12)*z[31] - n<T>(286,9)*z[33]
    + n<T>(1409,6)*z[35] + 5*z[100] + n<T>(1954,3)*z[37];
    z[94]=z[3]*z[94];
    z[98]=z[111] + z[58] - 2*z[57];
    z[100]=z[6]*z[113];
    z[95]= - n<T>(3,2)*z[14] - 3*z[12] - n<T>(73,16)*z[15] + z[95];
    z[95]=z[14]*z[95];
    z[95]=z[95] + z[100] + n<T>(83,24)*z[29] + z[102] + 3*z[98];
    z[95]=z[14]*z[95];
    z[98]=n<T>(7,2)*z[104] + z[108] - n<T>(7,6)*z[29] + n<T>(1,2)*z[111] + 7*z[20]
    - 6*z[57] + z[56];
    z[98]=z[12]*z[98];
    z[100]=n<T>(51659,1508)*z[18] + n<T>(57671,3016)*z[55] + 9*z[54];
    z[100]= - n<T>(409135,18096)*z[29] + 3*z[100] - 31*z[111];
    z[101]=z[15] + n<T>(603607,12064)*z[6];
    z[101]=z[6]*z[101];
    z[100]=n<T>(1,2)*z[100] + z[101];
    z[100]=z[6]*z[100];
    z[101]=z[67] + z[69];
    z[102]=z[23] - z[35];
    z[103]=n<T>(3,4)*z[111] + n<T>(3,2)*z[56] + 31*z[54];
    z[103]=z[15]*z[103];
    z[103]=z[103] + z[68];
    z[104]= - 4*z[91] + z[90];
    z[105]=npow(z[10],2);
    z[106]=npow(z[11],2);
    z[108]=npow(z[9],2);
    z[109]= - n<T>(214483,12064)*z[15] + n<T>(50575,12)*z[9] + n<T>(3859,3)*z[11]
     - 
   n<T>(2789,4)*z[10] + n<T>(116463,6032)*z[13] + 12*z[17] - n<T>(1,2)*z[16];
    z[109]=z[29]*z[109];
    z[107]= - n<T>(1,6)*z[107] - n<T>(5,3)*z[29] - 4*z[59] + 3*z[58];
    z[107]=z[8]*z[107];

    r +=  - n<T>(314885,24128)*z[22] - n<T>(545489,24128)*z[25] - n<T>(74179,24128)
      *z[27] - n<T>(534993,12064)*z[31] - n<T>(934553,144768)*z[33] + n<T>(934553,12064)*z[37]
     + n<T>(3993771,24128)*z[40] - n<T>(653,12)*z[42] - n<T>(653,3)*
      z[43] - n<T>(71483,36)*z[44] + n<T>(2486,9)*z[46] - n<T>(14411,3)*z[47] + n<T>(437,2)*z[49]
     - n<T>(395,4)*z[50]
     + n<T>(653,4)*z[51]
     + n<T>(965,2)*z[52]
     +  n<T>(192157,72)*z[53] + n<T>(621643,24128)*z[60] - z[61] - n<T>(231191,24128)
      *z[62] + n<T>(118665,24128)*z[63] + n<T>(131,16)*z[64] - n<T>(21,16)*z[65]
     - 
      n<T>(35,16)*z[66] - z[70] - n<T>(5,4)*z[71] + n<T>(71,2)*z[72] + n<T>(201,2)*
      z[73] - n<T>(21,4)*z[74] - n<T>(1451,2)*z[75] + n<T>(1517,2)*z[76] + n<T>(45,2)*
      z[77] - n<T>(81,2)*z[78] + n<T>(259,6)*z[79] - n<T>(1696,3)*z[80] - n<T>(2513,4)*
      z[81] + n<T>(1511,6)*z[82] + n<T>(337,6)*z[83] + n<T>(9,2)*z[84] - 63*z[85]
       - 13*z[86] + 19*z[87] - 23*z[88] + n<T>(89,2)*z[89] + z[92] + z[93]
       + z[94] + z[95] + z[96] + z[97] + z[98] + z[99] + z[100] - n<T>(9,4)
      *z[101] - n<T>(934553,24128)*z[102] + n<T>(1,2)*z[103] + 3*z[104] + 149*
      z[105] - 66*z[106] + z[107] + n<T>(63,4)*z[108] + z[109];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf919(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf919(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
