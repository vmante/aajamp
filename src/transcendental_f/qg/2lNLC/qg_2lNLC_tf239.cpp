#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf239(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[46];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[5];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[2];
    z[10]=d[4];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[19];
    z[18]=c[23];
    z[19]=c[25];
    z[20]=c[26];
    z[21]=c[28];
    z[22]=c[31];
    z[23]=e[10];
    z[24]=f[3];
    z[25]=f[4];
    z[26]=f[5];
    z[27]=f[11];
    z[28]=f[12];
    z[29]=f[16];
    z[30]=f[17];
    z[31]=npow(z[3],2);
    z[32]=n<T>(1,2)*z[31];
    z[33]=z[1]*i;
    z[34]=z[33]*z[3];
    z[35]=z[32] - z[34];
    z[36]=z[33] - z[3];
    z[37]=z[36] + n<T>(1,2)*z[5];
    z[38]=5*z[5];
    z[39]= - z[37]*z[38];
    z[38]= - n<T>(11,4)*z[36] - z[38];
    z[38]=z[7]*z[38];
    z[38]=z[38] - n<T>(1,2)*z[35] + z[39];
    z[38]=z[7]*z[38];
    z[39]=z[32] + 11*z[34];
    z[40]= - z[3] + n<T>(1,2)*z[2];
    z[41]=2*z[33];
    z[42]= - z[41] + z[40];
    z[43]=5*z[2];
    z[42]=z[42]*z[43];
    z[43]= - n<T>(11,4)*z[3] + z[43];
    z[43]=z[4]*z[43];
    z[39]=z[43] + n<T>(1,2)*z[39] + z[42];
    z[39]=z[4]*z[39];
    z[42]= - z[31] + z[34];
    z[43]=z[33] + z[3];
    z[44]=n<T>(1,2)*z[43] + 2*z[5];
    z[44]=z[5]*z[44];
    z[42]=2*z[42] + z[44];
    z[42]=z[5]*z[42];
    z[31]=z[31] + z[34];
    z[44]= - 2*z[2] - n<T>(1,2)*z[3] + 4*z[33];
    z[44]=z[2]*z[44];
    z[31]=2*z[31] + z[44];
    z[31]=z[2]*z[31];
    z[44]=2*z[3];
    z[45]=z[44] - z[5];
    z[45]=z[33]*z[45];
    z[45]=2*z[14] + z[45];
    z[45]=z[8]*z[45];
    z[33]=z[2] - z[44] + z[33];
    z[33]=z[11]*z[33];
    z[44]= - 2*z[36] - z[5];
    z[44]=z[12]*z[44];
    z[31]=z[45] + z[39] + z[38] + z[42] + z[31] + z[33] + z[44];
    z[33]=n<T>(1,2)*z[14];
    z[37]=z[5]*z[37];
    z[38]= - z[2]*z[40];
    z[37]= - z[33] + z[38] - z[34] + z[37];
    z[37]=z[6]*z[37];
    z[38]=n<T>(1,2)*z[10];
    z[39]=z[7] + z[36];
    z[39]=z[39]*z[38];
    z[36]=n<T>(1,2)*z[7] + z[36];
    z[36]=z[7]*z[36];
    z[35]=z[39] + z[36] + z[35];
    z[35]=z[10]*z[35];
    z[36]= - n<T>(1,2)*z[4] + z[43];
    z[36]=z[4]*z[36];
    z[32]=z[36] - z[32] - z[34];
    z[34]=z[3] - z[4];
    z[34]=z[9]*z[34];
    z[32]=n<T>(3,2)*z[34] + 3*z[32] - z[33];
    z[32]=z[9]*z[32];
    z[33]= - z[4] + z[41] - z[2];
    z[33]=z[13]*z[33];
    z[34]=z[5] + z[7];
    z[34]=z[23]*z[34];
    z[33]=z[33] + z[34];
    z[34]=n<T>(11,9)*z[3] - z[5];
    z[34]= - z[38] + n<T>(11,36)*z[4] - n<T>(2,9)*z[7] + n<T>(1,2)*z[34] + n<T>(4,9)*
    z[2];
    z[34]=z[14]*z[34];
    z[36]= - z[30] + z[29] + z[28] + z[24];
    z[38]= - z[27] - z[26] + z[25] + z[19];
    z[39]= - n<T>(11,3)*z[20] - n<T>(11,6)*z[18] - n<T>(4,9)*z[15];
    z[39]=i*z[39];

    r +=  - n<T>(1,6)*z[16] - n<T>(11,72)*z[17] + n<T>(11,6)*z[21] + n<T>(7,12)*z[22]
       + n<T>(1,3)*z[31] + n<T>(1,2)*z[32] + n<T>(5,3)*z[33] + z[34] + n<T>(3,2)*z[35]
       + n<T>(3,4)*z[36] + z[37] + n<T>(11,12)*z[38] + z[39];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf239(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf239(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
