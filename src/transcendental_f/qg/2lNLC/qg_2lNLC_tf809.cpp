#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf809(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[57];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[31];
    z[25]=e[3];
    z[26]=e[10];
    z[27]=e[6];
    z[28]=e[14];
    z[29]=f[3];
    z[30]=f[4];
    z[31]=f[5];
    z[32]=f[11];
    z[33]=f[12];
    z[34]=f[16];
    z[35]=f[17];
    z[36]=f[31];
    z[37]=f[36];
    z[38]=f[39];
    z[39]=f[51];
    z[40]=f[55];
    z[41]=f[58];
    z[42]=n<T>(1,1508)*z[5];
    z[43]= - n<T>(59789,3)*z[3] + n<T>(60543,2)*z[10];
    z[44]= - n<T>(62051,6)*z[7] - n<T>(56327,3)*z[2] + n<T>(62051,3)*z[9] - n<T>(2485,2)
   *z[4] - z[43];
    z[44]=z[44]*z[42];
    z[45]=15*z[12];
    z[46]=npow(z[8],2);
    z[47]=npow(z[11],2);
    z[48]=n<T>(56327,2)*z[13] + 65821*z[15] - n<T>(62051,2)*z[14];
    z[49]= - z[11] + n<T>(54511,6032)*z[10];
    z[49]=z[10]*z[49];
    z[50]= - 15*z[4] + n<T>(124187,4524)*z[2];
    z[50]=z[2]*z[50];
    z[51]=2*z[8];
    z[52]= - n<T>(16811,9048)*z[3] + n<T>(7763,1508)*z[2] + n<T>(5809,2262)*z[9]
     + 
    z[51];
    z[52]=z[3]*z[52];
    z[53]= - n<T>(111815,18096)*z[7] - n<T>(68837,4524)*z[3] + n<T>(60543,3016)*
    z[10] - 4*z[2];
    z[53]=z[7]*z[53];
    z[54]=z[6] - z[3];
    z[54]=n<T>(120109,9048)*z[5] - n<T>(32345,1131)*z[2] - n<T>(21,2)*z[10] + 6*
    z[11] + n<T>(47725,3016)*z[4] + 4*z[54];
    z[54]=z[6]*z[54];
    z[44]=z[54] + z[44] + z[53] + z[52] + z[50] + z[49] - n<T>(19,4)*z[47]
    - z[46] + n<T>(1,2262)*z[48] + z[45];
    z[44]=z[1]*z[44];
    z[48]= - z[22] - n<T>(1,2)*z[20];
    z[48]=255829*z[48] + n<T>(48787,6)*z[17];
    z[44]=n<T>(1,4524)*z[48] + z[44];
    z[44]=i*z[44];
    z[48]=2*z[3];
    z[49]= - z[8] - z[3];
    z[49]=z[49]*z[48];
    z[50]=n<T>(1,2)*z[2];
    z[52]= - z[7] + z[50] + z[48];
    z[52]=z[7]*z[52];
    z[53]= - n<T>(56773,12)*z[5] + n<T>(58281,2)*z[4] - n<T>(59035,3)*z[2];
    z[42]=z[53]*z[42];
    z[53]=n<T>(7,4)*z[10] - z[11] - n<T>(19427,6032)*z[4];
    z[48]= - n<T>(3,4)*z[6] - n<T>(120109,18096)*z[5] - n<T>(1,4)*z[7] + z[48] + 3*
    z[53] + n<T>(65821,4524)*z[2];
    z[48]=z[6]*z[48];
    z[53]=n<T>(15,4)*z[47];
    z[54]=npow(z[4],2);
    z[55]=npow(z[2],2);
    z[56]=z[11] - 7*z[10];
    z[56]=z[10]*z[56];
    z[42]=z[48] + z[42] + z[52] + z[49] + n<T>(56773,9048)*z[55] + z[56] - 
   n<T>(47725,6032)*z[54] + z[53] + n<T>(201541,54288)*z[16] - n<T>(74869,4524)*
    z[15] + z[46];
    z[42]=z[6]*z[42];
    z[48]=npow(z[3],2);
    z[49]=npow(z[10],2);
    z[51]=n<T>(1,3)*z[7] - n<T>(77885,4524)*z[3] - 2*z[2] + n<T>(60543,6032)*z[10]
    + z[51] - z[4];
    z[51]=z[7]*z[51];
    z[51]=z[51] - n<T>(77885,9048)*z[48] + n<T>(7,4)*z[55] + n<T>(48479,6032)*z[49]
    - z[47] - n<T>(26540,3393)*z[16] + n<T>(86933,4524)*z[26] - 2*z[46];
    z[51]=z[7]*z[51];
    z[52]=62051*z[14] - 56773*z[13];
    z[52]=n<T>(1,2)*z[52] + 4951*z[16];
    z[43]=n<T>(184199,12)*z[7] - z[43];
    z[43]=z[7]*z[43];
    z[43]=n<T>(1,2)*z[43] - n<T>(5809,6)*z[48] - n<T>(223,6)*z[55] - n<T>(12287,8)*z[49]
    + n<T>(1,3)*z[52] + n<T>(2485,8)*z[54];
    z[48]= - 19427*z[4] + 20181*z[10];
    z[48]=n<T>(62051,48)*z[7] - n<T>(15230,3)*z[3] + n<T>(3,16)*z[48] + n<T>(14476,3)*
    z[2];
    z[48]=z[5]*z[48];
    z[43]=n<T>(1,2)*z[43] + z[48];
    z[43]=z[5]*z[43];
    z[48]=6*z[25];
    z[52]=7763*z[2] + n<T>(125695,3)*z[3];
    z[52]=z[3]*z[52];
    z[46]=n<T>(1,3016)*z[52] - n<T>(7763,3016)*z[55] - 6*z[49] - n<T>(3947,3016)*
    z[16] + z[46] + n<T>(5809,2262)*z[14] + n<T>(77885,4524)*z[26] + z[48];
    z[46]=z[3]*z[46];
    z[49]= - n<T>(56773,2)*z[15] + 223*z[13];
    z[49]= - n<T>(101567,4524)*z[55] + 15*z[54] + n<T>(50911,13572)*z[16] + n<T>(1,1131)*z[49]
     - z[45];
    z[49]=z[49]*z[50];
    z[50]=2*z[11] + n<T>(5,2)*z[10];
    z[50]=z[10]*z[50];
    z[48]=z[50] - z[53] - n<T>(102767,9048)*z[16] + 5*z[27] + z[48];
    z[48]=z[10]*z[48];
    z[45]= - z[45] - n<T>(19073,9048)*z[16];
    z[45]=n<T>(1,2)*z[45] + z[47];
    z[45]=z[4]*z[45];
    z[47]= - z[38] + z[36] + z[37];
    z[50]=z[21] - z[31];
    z[52]=n<T>(62051,1508)*z[9] - z[8];
    z[52]=n<T>(19,6)*z[11] + n<T>(1,3)*z[52];
    z[52]=z[16]*z[52];
    z[53]=z[8]*z[28];

    r +=  - n<T>(73669,9048)*z[18] - n<T>(255829,108576)*z[19] + n<T>(255829,9048)*
      z[23] + n<T>(414647,18096)*z[24] + n<T>(2485,6032)*z[29] + n<T>(184199,18096)
      *z[30] - n<T>(120109,18096)*z[32] + n<T>(12287,6032)*z[33] + n<T>(48479,6032)
      *z[34] - n<T>(47725,6032)*z[35] + z[39] + z[40] - z[41] + z[42] + n<T>(1,377)*z[43]
     + z[44]
     + z[45]
     + z[46]
     + n<T>(5,4)*z[47]
     + z[48]
     + z[49]
       + n<T>(255829,18096)*z[50] + z[51] + z[52] + 4*z[53];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf809(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf809(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
