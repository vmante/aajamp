#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1411(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[56];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[4];
    z[7]=d[6];
    z[8]=d[16];
    z[9]=d[5];
    z[10]=d[17];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[4];
    z[14]=e[8];
    z[15]=e[9];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=e[6];
    z[25]=e[7];
    z[26]=f[0];
    z[27]=f[1];
    z[28]=f[3];
    z[29]=f[5];
    z[30]=f[11];
    z[31]=f[17];
    z[32]=f[18];
    z[33]=f[31];
    z[34]=f[35];
    z[35]=f[36];
    z[36]=f[37];
    z[37]=f[39];
    z[38]=n<T>(1,2)*z[3];
    z[39]=z[1]*i;
    z[40]=z[38] - z[39];
    z[41]= - z[3]*z[40];
    z[42]=z[39] - z[3];
    z[43]=n<T>(1,2)*z[6];
    z[44]= - z[43] - z[42];
    z[44]=z[6]*z[44];
    z[45]=z[39] - z[5];
    z[46]=z[5]*z[45];
    z[47]=z[7] + z[6];
    z[48]= - z[5] - z[3] + z[47];
    z[48]=z[7]*z[48];
    z[41]=n<T>(1,2)*z[48] + z[44] + z[46] + z[41];
    z[41]=z[7]*z[41];
    z[44]=3*z[3];
    z[46]= - z[40]*z[44];
    z[48]=2*z[39];
    z[49]=z[48] - z[2];
    z[50]=n<T>(1,2)*z[5];
    z[51]= - z[50] - z[49];
    z[51]=z[2]*z[51];
    z[52]=z[48] - z[5];
    z[52]=z[5]*z[52];
    z[53]=npow(z[6],2);
    z[46]=z[51] + n<T>(3,2)*z[53] + z[52] + z[46];
    z[46]=z[2]*z[46];
    z[50]=z[50] - z[39];
    z[51]=z[50]*z[5];
    z[43]=z[43] + z[45];
    z[52]=3*z[6];
    z[43]=z[43]*z[52];
    z[52]=z[52] - z[45];
    z[53]=z[9] + z[7];
    z[52]=n<T>(1,2)*z[52] - z[53];
    z[52]=z[9]*z[52];
    z[43]=z[52] + z[51] + z[43];
    z[52]= - z[7]*z[45];
    z[43]=z[52] + n<T>(1,2)*z[43];
    z[43]=z[9]*z[43];
    z[52]=z[6] + z[3];
    z[52]= - z[39]*z[52];
    z[52]= - z[16] + z[52];
    z[52]=z[8]*z[52];
    z[54]=z[6] - z[42];
    z[54]=z[13]*z[54];
    z[47]=z[24]*z[47];
    z[47]= - z[47] + z[52] + z[54];
    z[52]=n<T>(3,2)*z[5];
    z[50]=z[50]*z[52];
    z[38]=3*z[39] - z[38];
    z[38]=z[3]*z[38];
    z[52]= - n<T>(1,2)*z[45] - z[3];
    z[52]=z[6]*z[52];
    z[38]=n<T>(3,2)*z[52] + z[50] + z[38];
    z[38]=z[6]*z[38];
    z[40]= - z[5] + z[40];
    z[40]=z[40]*z[44];
    z[40]=z[51] + z[40];
    z[48]= - n<T>(1,2)*z[2] + z[48] + z[5];
    z[48]=z[2]*z[48];
    z[51]=z[5] + z[44];
    z[51]=n<T>(1,4)*z[51] - z[2];
    z[51]=z[4]*z[51];
    z[40]=z[51] + n<T>(1,2)*z[40] + z[48];
    z[40]=z[4]*z[40];
    z[42]=n<T>(3,4)*z[5] - z[42];
    z[42]=z[3]*z[42];
    z[42]=z[50] + z[42];
    z[42]=z[3]*z[42];
    z[48]=z[7] + z[5];
    z[39]=z[39]*z[48];
    z[39]=z[16] + z[39];
    z[39]=z[10]*z[39];
    z[48]=z[37] - z[35] + z[31] + z[28];
    z[50]=z[33] + z[30];
    z[51]=z[3] - z[49];
    z[51]=z[11]*z[51];
    z[51]=z[51] + z[26];
    z[52]=10*z[22] + n<T>(1,4)*z[17];
    z[52]=i*z[52];
    z[44]= - z[44] - n<T>(13,3)*z[6];
    z[44]= - n<T>(1,12)*z[4] - n<T>(1,3)*z[9] + n<T>(4,3)*z[2] + n<T>(1,4)*z[44]
     + n<T>(2,3)
   *z[7];
    z[44]=z[16]*z[44];
    z[54]=z[4] - z[49];
    z[54]=z[12]*z[54];
    z[49]=z[5] - z[49];
    z[49]=z[14]*z[49];
    z[45]=z[7] - z[45];
    z[45]=z[15]*z[45];
    z[53]= - z[25]*z[53];
    z[55]=2*i;
    z[55]=z[20]*z[55];

    r += z[18] - n<T>(4,3)*z[19] - z[21] - 5*z[23] - n<T>(3,2)*z[27] + z[29] + 
      z[32] - z[34] - n<T>(1,2)*z[36] + z[38] + z[39] + z[40] + z[41] + 
      z[42] + z[43] + z[44] + z[45] + z[46] + 2*z[47] + n<T>(3,4)*z[48] + 
      z[49] + n<T>(1,4)*z[50] + 3*z[51] + z[52] + z[53] + z[54] + z[55];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1411(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1411(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
