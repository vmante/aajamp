#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf61(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[32];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[70];
    z[3]=f[79];
    z[4]=c[11];
    z[5]=d[1];
    z[6]=d[7];
    z[7]=c[12];
    z[8]=c[32];
    z[9]=c[33];
    z[10]=c[35];
    z[11]=c[36];
    z[12]=c[38];
    z[13]=c[40];
    z[14]=c[43];
    z[15]=c[47];
    z[16]=c[55];
    z[17]=d[3];
    z[18]=d[5];
    z[19]=g[141];
    z[20]=g[173];
    z[21]=g[181];
    z[22]=g[191];
    z[23]=g[245];
    z[24]=g[251];
    z[25]=g[254];
    z[26]=g[258];
    z[27]=g[272];
    z[28]=g[308];
    z[29]= - z[4]*i;
    z[29]=n<T>(2,27)*z[29] + 2*z[7] - z[2];
    z[30]=z[6] - z[5];
    z[29]=z[30]*z[29];
    z[30]=z[18] - z[17];
    z[31]=z[3] + z[2];
    z[30]=z[31]*z[30];
    z[31]=2*z[2] + z[3];
    z[31]=z[1]*i*z[31];
    z[29]=z[31] + z[30] + z[29];
    z[30]= - n<T>(4,27)*z[15] - n<T>(310,9)*z[12] + n<T>(23,9)*z[9];
    z[30]=i*z[30];
    z[29]= - z[23] + n<T>(1,5)*z[16] - 2*z[14] - n<T>(20,3)*z[13] + 8*z[11] - n<T>(48,5)*z[10]
     + 4*z[29] - n<T>(59,135)*z[8]
     + z[30];
    z[30]= - z[20] + z[27] + z[21];
    z[31]=z[25] - z[22];

    r +=  - n<T>(1,3)*z[19] + 6*z[24] + z[26] + 16*z[28] + 2*z[29] + 8*
      z[30] + n<T>(8,3)*z[31];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf61(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf61(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
