#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf296(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[54];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[16];
    z[10]=d[4];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[4];
    z[14]=e[8];
    z[15]=c[3];
    z[16]=d[5];
    z[17]=d[6];
    z[18]=d[9];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[0];
    z[28]=e[3];
    z[29]=e[10];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[17];
    z[39]=f[18];
    z[40]=n<T>(1,4)*z[7];
    z[41]= - n<T>(45,2)*z[10] + n<T>(13,3)*z[7];
    z[41]=z[41]*z[40];
    z[42]=n<T>(1,6)*z[3];
    z[43]=n<T>(29,8)*z[3];
    z[44]=23*z[7] - z[43];
    z[44]=z[44]*z[42];
    z[45]=n<T>(1,2)*z[7];
    z[46]=z[45] - z[16];
    z[47]=n<T>(1,2)*z[3];
    z[48]= - n<T>(17,16)*z[6] - n<T>(3,4)*z[2] - z[47] + 2*z[4] - z[46];
    z[48]=z[6]*z[48];
    z[49]=npow(z[4],2);
    z[50]=npow(z[10],2);
    z[46]=n<T>(21,16)*z[2] + z[46];
    z[46]=z[2]*z[46];
    z[51]= - n<T>(5,8)*z[6] + n<T>(13,8)*z[2] - n<T>(89,24)*z[3] + n<T>(43,48)*z[7]
     - 
    z[4] + n<T>(45,16)*z[10];
    z[51]=z[5]*z[51];
    z[41]=z[51] + z[48] + z[46] + z[44] + z[41] + n<T>(25,16)*z[50] - n<T>(5,4)*
    z[49] + n<T>(49,144)*z[15] + n<T>(49,12)*z[12] - 2*z[11];
    z[41]=z[5]*z[41];
    z[44]=npow(z[7],2);
    z[46]=z[44] + z[50];
    z[48]= - z[3]*z[40];
    z[51]= - n<T>(11,16)*z[2] + n<T>(3,4)*z[3] - z[16] + z[40];
    z[51]=z[2]*z[51];
    z[52]=n<T>(1,4)*z[4];
    z[53]=z[52] - z[10];
    z[53]=n<T>(1,12)*z[6] + n<T>(1,4)*z[53] + z[2];
    z[53]=z[6]*z[53];
    z[46]=z[53] + z[51] + z[48] + n<T>(1,16)*z[49] - z[14] - n<T>(25,48)*z[15]
    + n<T>(1,4)*z[46];
    z[46]=z[6]*z[46];
    z[48]=n<T>(1,2)*z[6];
    z[45]= - z[48] + n<T>(1,4)*z[2] - z[47] + z[45] - n<T>(9,2)*z[4] + z[10];
    z[45]=z[45]*z[48];
    z[48]=z[3] + z[7];
    z[48]= - z[4] - 3*z[48];
    z[48]=n<T>(1,2)*z[48] + 7*z[2];
    z[48]=z[2]*z[48];
    z[51]=n<T>(17,4)*z[6] - n<T>(41,4)*z[2] + n<T>(23,3)*z[3] - n<T>(43,12)*z[7]
     - n<T>(45,4)
   *z[10] + n<T>(49,6)*z[8] + 5*z[4];
    z[51]=z[5]*z[51];
    z[48]=z[48] + z[51];
    z[51]= - z[9] + n<T>(1,2)*z[4];
    z[51]=z[4]*z[51];
    z[51]= - n<T>(29,4)*z[50] + z[51] + n<T>(7,2)*z[14] + n<T>(37,2)*z[11] - z[13]
    - n<T>(49,3)*z[12];
    z[43]= - z[43] + n<T>(29,4)*z[8] - 17*z[7];
    z[42]=z[43]*z[42];
    z[43]=n<T>(45,8)*z[10] - n<T>(4,3)*z[7];
    z[43]=z[7]*z[43];
    z[42]=z[45] + z[42] + n<T>(1,4)*z[51] + z[43] + n<T>(1,2)*z[48];
    z[42]=z[1]*z[42];
    z[43]=n<T>(79,72)*z[19] - 9*z[24] - 5*z[22];
    z[42]=n<T>(1,2)*z[43] + z[42];
    z[42]=i*z[42];
    z[43]=n<T>(19,2)*z[3] + z[10] - n<T>(5,12)*z[7];
    z[43]=z[43]*z[47];
    z[45]= - n<T>(25,3)*z[15] + n<T>(29,3)*z[12] + n<T>(11,3)*z[29] + 17*z[28];
    z[43]=z[43] - n<T>(37,6)*z[44] + n<T>(1,4)*z[45] - 5*z[50];
    z[43]=z[43]*z[47];
    z[44]=n<T>(19,12)*z[49] + n<T>(25,24)*z[15] - n<T>(1,2)*z[27] + z[13];
    z[44]=z[44]*z[52];
    z[45]=z[28] - n<T>(13,12)*z[15];
    z[47]= - z[4] - n<T>(13,6)*z[10];
    z[47]=z[10]*z[47];
    z[45]=19*z[45] + z[47];
    z[45]=z[10]*z[45];
    z[47]=z[7]*z[10];
    z[47]=z[50] + z[47];
    z[48]=37*z[29] - n<T>(91,12)*z[15];
    z[47]=n<T>(1,3)*z[48] + 7*z[47];
    z[40]=z[47]*z[40];
    z[47]=z[50] + z[49] + n<T>(31,6)*z[15] + z[14] + z[27] - 21*z[11];
    z[48]=z[3]*z[7];
    z[47]=n<T>(1,2)*z[47] - 3*z[48];
    z[48]=z[16] - n<T>(11,8)*z[2];
    z[48]=z[2]*z[48];
    z[47]=n<T>(1,4)*z[47] + z[48];
    z[47]=z[2]*z[47];
    z[48]= - z[34] + z[23] - z[32];
    z[48]=9*z[25] + n<T>(1183,24)*z[26] + z[30] - n<T>(1,2)*z[31] + n<T>(13,3)*z[33]
    - n<T>(17,4)*z[35] - n<T>(25,4)*z[36] + n<T>(11,4)*z[37] + n<T>(1,3)*z[39] + n<T>(9,2)*
    z[38] + 5*z[48];
    z[49]=z[16]*z[14];
    z[50]= - z[18] + z[17];
    z[50]=n<T>(49,3)*z[8] + n<T>(1,6)*z[50] - z[9];
    z[50]=n<T>(1,2)*z[50] - n<T>(1,3)*z[16];
    z[50]=z[15]*z[50];

    r +=  - n<T>(121,48)*z[20] - n<T>(1,3)*z[21] + z[40] + z[41] + z[42] + 
      z[43] + z[44] + n<T>(1,8)*z[45] + z[46] + z[47] + n<T>(1,4)*z[48] + z[49]
       + n<T>(1,2)*z[50];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf296(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf296(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
