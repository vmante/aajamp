#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf923(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[121];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=e[0];
    z[9]=e[1];
    z[10]=e[8];
    z[11]=d[6];
    z[12]=d[9];
    z[13]=d[15];
    z[14]=d[5];
    z[15]=d[4];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=d[11];
    z[19]=e[2];
    z[20]=e[5];
    z[21]=e[9];
    z[22]=f[0];
    z[23]=f[3];
    z[24]=f[5];
    z[25]=f[8];
    z[26]=f[11];
    z[27]=f[13];
    z[28]=f[17];
    z[29]=f[20];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[18];
    z[34]=c[19];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[29];
    z[40]=c[30];
    z[41]=c[31];
    z[42]=c[47];
    z[43]=c[51];
    z[44]=c[54];
    z[45]=c[55];
    z[46]=c[59];
    z[47]=c[61];
    z[48]=c[62];
    z[49]=c[66];
    z[50]=c[68];
    z[51]=c[69];
    z[52]=c[72];
    z[53]=c[75];
    z[54]=c[83];
    z[55]=e[3];
    z[56]=e[10];
    z[57]=e[6];
    z[58]=e[12];
    z[59]=e[14];
    z[60]=f[4];
    z[61]=f[12];
    z[62]=f[14];
    z[63]=f[16];
    z[64]=f[31];
    z[65]=f[36];
    z[66]=f[37];
    z[67]=f[39];
    z[68]=f[51];
    z[69]=f[52];
    z[70]=f[55];
    z[71]=g[0];
    z[72]=g[3];
    z[73]=g[5];
    z[74]=g[8];
    z[75]=g[11];
    z[76]=g[13];
    z[77]=g[17];
    z[78]=g[21];
    z[79]=g[29];
    z[80]=g[30];
    z[81]=g[35];
    z[82]=g[41];
    z[83]=g[42];
    z[84]=g[43];
    z[85]=g[56];
    z[86]=g[57];
    z[87]=g[76];
    z[88]=g[94];
    z[89]=g[107];
    z[90]=g[125];
    z[91]=4*z[6];
    z[92]=4*z[7];
    z[93]=2*z[11];
    z[94]= - 12*z[8] - 115*z[9] - n<T>(1316,3)*z[10];
    z[95]=n<T>(2567,3016) - 113*z[3];
    z[95]=z[3]*z[95];
    z[96]= - n<T>(845,9)*z[5] + n<T>(23775,6032) - n<T>(1774,3)*z[3];
    z[96]=z[5]*z[96];
    z[94]=z[96] + z[95] + z[91] - z[92] + z[93] + 2*z[94] + n<T>(11999,9)*
    z[30];
    z[94]=z[5]*z[94];
    z[95]=6*z[11];
    z[96]=npow(z[3],2);
    z[97]=4042*z[8];
    z[98]= - z[97] - 1694*z[9] - 1343*z[10];
    z[98]=2*z[98] + n<T>(713,3)*z[30];
    z[99]=n<T>(2722,3)*z[5] + n<T>(9449,1508) - n<T>(1072,3)*z[3];
    z[99]=z[5]*z[99];
    z[100]= - n<T>(1307,1508) + n<T>(118,3)*z[3];
    z[100]= - n<T>(6170,9)*z[2] + 17*z[100] + n<T>(808,3)*z[5];
    z[100]=z[2]*z[100];
    z[98]=z[100] + z[99] + n<T>(296,3)*z[96] - z[92] - z[95] + n<T>(1,3)*z[98]
    - 4*z[14];
    z[98]=z[2]*z[98];
    z[99]=n<T>(14679,1508)*z[13];
    z[100]=n<T>(24433,3016)*z[15];
    z[101]=2*z[12];
    z[102]=2*z[14];
    z[103]= - n<T>(58363,3016) - 501*z[3];
    z[103]=z[3]*z[103];
    z[103]=z[103] + n<T>(6385,1508)*z[6] + n<T>(6433,3016)*z[7] + z[93] + z[102]
    - z[101] - z[100] + n<T>(16168,3)*z[8] + n<T>(5264,3)*z[10] - n<T>(4804,3)*z[9]
    + 2*z[16] + z[99];
    z[103]=z[1]*z[103];
    z[104]=z[2]*z[1];
    z[105]=z[5]*z[1];
    z[106]=n<T>(19957,1508) + n<T>(9412,3)*z[3];
    z[106]=z[1]*z[106];
    z[106]= - n<T>(5612,3)*z[104] + z[106] + n<T>(80,3)*z[105];
    z[106]=z[2]*z[106];
    z[107]= - n<T>(17743,3016) - n<T>(4468,3)*z[3];
    z[107]=z[1]*z[107];
    z[107]=z[107] + 697*z[105];
    z[107]=z[5]*z[107];
    z[108]= - 1585*z[37] - 166*z[35];
    z[103]=z[106] + z[107] + z[103] + 4*z[108] + n<T>(8933,18)*z[31];
    z[103]=i*z[103];
    z[106]=z[1]*z[3];
    z[107]= - 4595*z[106] + 3893*z[105];
    z[107]=n<T>(1,3)*z[107] + 234*z[104];
    z[107]=i*z[107];
    z[108]=2*z[8] + 79*z[9] + 9*z[10];
    z[109]= - n<T>(2567,6032) + 412*z[3];
    z[109]=z[3]*z[109];
    z[110]= - n<T>(1097,3)*z[5] - n<T>(27497,6032) + n<T>(4883,3)*z[3];
    z[110]=z[5]*z[110];
    z[111]=n<T>(4484,3)*z[2] - n<T>(3034,3)*z[5] + n<T>(1879,377) - n<T>(6472,3)*z[3];
    z[111]=z[2]*z[111];
    z[112]=i*z[1];
    z[112]= - n<T>(3286,3)*z[112] - n<T>(82,3)*z[2] - n<T>(1774,3)*z[5] - 1 + n<T>(1856,3)*z[3];
    z[112]=2*z[112] + n<T>(1643,3)*z[4];
    z[112]=z[4]*z[112];
    z[107]=n<T>(1,3)*z[112] + z[107] + z[111] + z[110] + z[109] - n<T>(3387,377)
   *z[6] + n<T>(17695,6032)*z[7] + z[11] + z[12] + n<T>(24433,6032)*z[15] + 6*
    z[108] - n<T>(33839,36)*z[30];
    z[107]=z[4]*z[107];
    z[108]=4*z[12];
    z[100]=z[100] - z[108];
    z[109]= - n<T>(71277,6032)*z[7] - z[100];
    z[109]=z[7]*z[109];
    z[110]=3*z[11];
    z[111]= - z[101] + z[110];
    z[112]=n<T>(9401,1508)*z[7];
    z[111]=n<T>(20711,1508)*z[6] + 2*z[111] + z[112];
    z[111]=z[6]*z[111];
    z[113]= - n<T>(4042,3)*z[8] + n<T>(3019,3)*z[9] - 45*z[10];
    z[114]=n<T>(58363,6032) - n<T>(926,9)*z[3];
    z[114]=z[3]*z[114];
    z[113]=z[114] + 2*z[113] - n<T>(8621,9)*z[30];
    z[113]=z[3]*z[113];
    z[114]=2*z[20];
    z[115]=n<T>(14679,1508)*z[19];
    z[116]=npow(z[12],2);
    z[117]=npow(z[15],2);
    z[118]=z[15]*z[102];
    z[119]=z[14] - z[12];
    z[120]= - z[15] + z[119];
    z[120]=2*z[120] - z[11];
    z[120]=z[11]*z[120];
    z[94]=z[107] + z[103] + z[98] + z[94] + z[113] + z[111] + z[109] + 
    z[120] + z[118] + 2*z[116] + n<T>(5727,6032)*z[117] - n<T>(6415,2262)*z[30]
    - n<T>(6385,1508)*z[9] + n<T>(1852,3)*z[23] - n<T>(3040,3)*z[24] - n<T>(448,3)*
    z[26] - 800*z[28] - 2524*z[22] + 36*z[25] - n<T>(422,3)*z[27] + n<T>(2524,3)
   *z[29] + 271*z[32] - n<T>(577,3)*z[34] + n<T>(2716,3)*z[36] + 4068*z[38] + 
   n<T>(93943,9)*z[41] + 1084*z[33] - 813*z[39] - 746*z[40] - z[114] + 
    z[115];
    z[94]=z[4]*z[94];
    z[98]=n<T>(65149,3016)*z[6];
    z[103]=5*z[9] - 47*z[10];
    z[103]=2*z[103] - 73*z[8];
    z[103]=23*z[103] + n<T>(24523,2)*z[30];
    z[107]= - 4 + n<T>(1405,3)*z[3];
    z[107]=z[3]*z[107];
    z[109]=n<T>(1717,3)*z[5] + n<T>(353,3016) + n<T>(2468,3)*z[3];
    z[109]=z[5]*z[109];
    z[111]= - n<T>(8335,9)*z[2] - n<T>(460,9)*z[5] + n<T>(65197,3016) - n<T>(1000,9)*
    z[3];
    z[111]=z[2]*z[111];
    z[103]=z[111] + z[109] + z[107] - z[98] + n<T>(1,4)*z[7] + n<T>(1,3)*z[103]
    + z[102];
    z[103]=z[2]*z[103];
    z[107]= - 42*z[8] - 23*z[9] - n<T>(62,3)*z[10];
    z[107]=2*z[107] - n<T>(14015,18)*z[30];
    z[109]=n<T>(1166,9)*z[5] - n<T>(8695,1508) - n<T>(836,3)*z[3];
    z[109]=z[5]*z[109];
    z[91]=z[109] - 622*z[96] - z[91] - n<T>(1,2)*z[7] + 5*z[107] + z[93];
    z[91]=z[5]*z[91];
    z[96]=z[92] + z[98];
    z[96]=z[6]*z[96];
    z[98]= - 1675*z[8] - 215*z[9] - 431*z[10];
    z[98]=2*z[98] - n<T>(8423,2)*z[30];
    z[107]= - n<T>(15,2) + n<T>(2462,9)*z[3];
    z[107]=z[3]*z[107];
    z[98]=z[107] + n<T>(1,3)*z[98] + 4*z[11];
    z[98]=z[3]*z[98];
    z[107]=n<T>(19,2)*z[8];
    z[109]= - z[12]*z[102];
    z[111]=z[102] - z[11];
    z[111]=z[11]*z[111];
    z[91]=z[103] + z[91] + z[98] + z[96] + z[111] + z[109] + z[116] - 
   n<T>(164629,9048)*z[30] + z[107] + n<T>(2663,1508)*z[10] + n<T>(23727,754)*z[9]
    + n<T>(274,3)*z[23] - n<T>(956,3)*z[24] - n<T>(404,3)*z[26] - 312*z[28] - 74*
    z[22] - 240*z[25] - n<T>(1130,3)*z[27] + n<T>(368,3)*z[29] + n<T>(653,6)*z[32]
    - n<T>(1043,9)*z[34] + n<T>(1121,3)*z[36] + n<T>(2300,3)*z[38] - n<T>(192157,36)*
    z[41] + n<T>(1306,3)*z[33] - 965*z[40] - n<T>(653,2)*z[39];
    z[91]=z[2]*z[91];
    z[96]=2*z[7];
    z[97]=z[97] - 2125*z[9] + 242*z[10];
    z[97]=z[96] + n<T>(1,3)*z[97] - z[119];
    z[98]= - 3 + n<T>(337,9)*z[3];
    z[98]=z[3]*z[98];
    z[97]=2*z[97] + z[98];
    z[97]=z[3]*z[97];
    z[98]=7*z[21];
    z[97]=z[97] - z[98] + z[20];
    z[95]= - n<T>(23727,3016)*z[6] - z[112] - z[95] - n<T>(23727,754)*z[13] + 
    z[108];
    z[95]=z[6]*z[95];
    z[103]= - z[110] - z[102] + 5*z[17] + z[12] + z[15];
    z[103]=z[103]*z[93];
    z[100]=n<T>(59213,6032)*z[7] + z[100];
    z[100]=z[7]*z[100];
    z[109]= - 4*z[18] - 3*z[12];
    z[109]=z[12]*z[109];
    z[110]=z[12] + z[16] - z[15];
    z[110]=2*z[110] - n<T>(67,4)*z[14];
    z[110]=z[14]*z[110];
    z[95]=z[95] + z[100] + z[103] + z[110] + z[109] + n<T>(93801,6032)*
    z[117] - 19*z[8] - n<T>(5679,754)*z[10] - n<T>(41069,1508)*z[9] - n<T>(2735,3)*
    z[23] + n<T>(5788,3)*z[24] + n<T>(793,3)*z[26] + 1469*z[28] + 3184*z[22] - 
   1152*z[25] + n<T>(1984,3)*z[27] - n<T>(3856,3)*z[29] - z[115] + 2*z[97];
    z[95]=z[1]*z[95];
    z[97]= - n<T>(69721,1508) - n<T>(6284,3)*z[3];
    z[97]=z[1]*z[97];
    z[97]=n<T>(40970,9)*z[104] + z[97] - n<T>(6250,3)*z[105];
    z[97]=z[2]*z[97];
    z[100]=92*z[9] + 193*z[10];
    z[100]=11*z[100] + 4013*z[8];
    z[100]=n<T>(1,3)*z[100] + z[7];
    z[103]=23 - 1234*z[3];
    z[103]=z[3]*z[103];
    z[100]=z[103] + 4*z[100] + n<T>(71181,1508)*z[6];
    z[100]=z[1]*z[100];
    z[103]=n<T>(271,377) + n<T>(220,3)*z[3];
    z[103]=z[1]*z[103];
    z[103]=7*z[103] - 2162*z[105];
    z[103]=z[5]*z[103];
    z[104]= - n<T>(6272,3)*z[31] - 3155*z[37] - 1066*z[35];
    z[97]=z[97] + z[103] + n<T>(2,3)*z[104] + z[100];
    z[97]=z[2]*z[97];
    z[100]=7*z[17];
    z[103]=336*z[8] + n<T>(266,3)*z[10] + z[100] + 230*z[9];
    z[104]=n<T>(1059,3016) + 1735*z[3];
    z[104]=z[3]*z[104];
    z[103]=z[104] + 10*z[11] + 17*z[14] + 2*z[103] - n<T>(81,2)*z[15];
    z[103]=z[1]*z[103];
    z[104]=1465*z[106] - n<T>(6328,9)*z[105];
    z[104]=z[5]*z[104];
    z[105]=460*z[37] + 141*z[35];
    z[103]=z[104] + z[103] + 2*z[105] + n<T>(6725,9)*z[31];
    z[103]=z[5]*z[103];
    z[104]=n<T>(1,3)*z[3];
    z[105]= - 2477*z[37] - 826*z[35];
    z[105]=2*z[105] - n<T>(8279,6)*z[31];
    z[105]=z[105]*z[104];
    z[106]=n<T>(6272,3)*z[42] - 11141*z[49] - n<T>(17287,3)*z[46];
    z[95]=z[97] + z[103] + z[95] + z[105] + n<T>(18481,3016)*z[31] + n<T>(81079,3016)*z[35]
     + n<T>(2,3)*z[106]
     + n<T>(81079,1508)*z[37];
    z[95]=i*z[95];
    z[97]=npow(z[7],2);
    z[96]= - z[96] + z[6];
    z[96]=z[6]*z[96];
    z[96]=z[96] + z[97] + z[98] - 431*z[40];
    z[98]=241*z[9];
    z[103]= - n<T>(5327,9)*z[30] + 30*z[8] - z[98] + 264*z[10];
    z[105]= - n<T>(2567,6032) - 244*z[3];
    z[105]=z[3]*z[105];
    z[106]=n<T>(1582,9)*z[5] + n<T>(3,4) - n<T>(107,3)*z[3];
    z[106]=z[5]*z[106];
    z[103]=z[106] + z[105] + n<T>(9,4)*z[7] - 14*z[11] - n<T>(17,2)*z[14] + 2*
    z[103] + n<T>(81,4)*z[15];
    z[103]=z[5]*z[103];
    z[98]= - 87*z[8] + z[98] - n<T>(112,3)*z[10];
    z[105]= - n<T>(1059,6032) - n<T>(512,3)*z[3];
    z[105]=z[3]*z[105];
    z[98]=z[105] + 4*z[98] + n<T>(50531,18)*z[30];
    z[98]=z[3]*z[98];
    z[105]=n<T>(482,3)*z[29];
    z[106]=npow(z[14],2);
    z[96]=z[103] + z[98] + n<T>(67,4)*z[106] - n<T>(33,2)*z[117] - n<T>(120287,18096)
   *z[30] + n<T>(8695,1508)*z[10] + 101*z[23] - 296*z[24] + 151*z[26] - 111
   *z[28] - 290*z[22] + 1128*z[25] - 64*z[27] + z[105] + 57*z[32] - 60*
    z[34] + n<T>(218,3)*z[36] - 320*z[38] + n<T>(113365,18)*z[41] + 228*z[33] - 
   171*z[39] + 2*z[96];
    z[96]=z[5]*z[96];
    z[98]= - z[15] - z[102];
    z[98]=z[98]*z[93];
    z[103]=z[14]*z[108];
    z[109]= - n<T>(165431,9048)*z[6] - z[15] + n<T>(2663,3016)*z[7];
    z[109]=z[6]*z[109];
    z[97]=z[109] + n<T>(5679,1508)*z[97] + z[98] + z[103] - 3*z[117] + n<T>(33481,9048)*z[30]
     - n<T>(23727,754)*z[19]
     + 6*z[55] - n<T>(17743,1508)*z[56];
    z[97]=z[6]*z[97];
    z[98]=4 - n<T>(337,6)*z[3];
    z[98]=z[98]*z[104];
    z[103]=34*z[10] - 205*z[8];
    z[92]=z[98] - z[92] - z[93] + z[102] + 3*z[103] + n<T>(6053,36)*z[30];
    z[92]=z[3]*z[92];
    z[93]=npow(z[11],2);
    z[98]=npow(z[6],2);
    z[93]=z[93] - z[98];
    z[98]= - n<T>(4481,36)*z[41] + n<T>(2,3)*z[33] + 53*z[40] - n<T>(1,2)*z[39];
    z[92]=z[92] - n<T>(44085,6032)*z[30] + z[107] + 102*z[23] - n<T>(904,3)*
    z[24] - n<T>(394,3)*z[26] - 246*z[28] - 296*z[22] + 228*z[25] - 80*z[27]
    + z[105] + n<T>(5,6)*z[32] - n<T>(572,9)*z[34] + n<T>(1409,3)*z[36] + 5*z[98]
    + n<T>(3908,3)*z[38] - 2*z[93];
    z[92]=z[3]*z[92];
    z[93]= - z[15]*z[101];
    z[98]= - n<T>(67,4)*z[15] - z[101];
    z[98]=z[14]*z[98];
    z[93]=z[98] + z[93] + n<T>(17,2)*z[117] - z[114] + n<T>(43,6)*z[30];
    z[93]=z[14]*z[93];
    z[98]=n<T>(2,3)*z[7] - z[102] + n<T>(36497,6032)*z[15] + z[108];
    z[98]=z[7]*z[98];
    z[101]=n<T>(12369,16)*z[117] - n<T>(5679,4)*z[56] + n<T>(13201,3)*z[30];
    z[98]=z[98] + 2*z[106] + n<T>(1,377)*z[101] - 6*z[116];
    z[98]=z[7]*z[98];
    z[101]= - z[57] + 5*z[21];
    z[102]=z[12] + 5*z[11];
    z[102]=z[11]*z[102];
    z[101]=z[102] + 4*z[117] + 2*z[101] + z[30];
    z[101]=z[11]*z[101];
    z[100]=z[100] + z[16];
    z[99]=n<T>(50575,6)*z[8] + n<T>(7718,3)*z[10] - n<T>(2789,2)*z[9] + 2*z[100]
     + 
    z[99];
    z[99]=z[30]*z[99];
    z[100]= - n<T>(1,3)*z[30] + 2*z[59] + z[58];
    z[102]=z[15] + n<T>(2,3)*z[12];
    z[102]=z[12]*z[102];
    z[100]=4*z[100] + z[102];
    z[100]=z[12]*z[100];
    z[102]=z[62] - z[66] + z[68] - z[69];
    z[103]=z[64] - z[67];
    z[104]=z[24] - z[36];
    z[105]= - 4*z[90] + z[89];
    z[106]=npow(z[9],2);
    z[107]=npow(z[10],2);
    z[108]=npow(z[8],2);
    z[109]=n<T>(13,6)*z[117] + 7*z[57] - n<T>(145073,9048)*z[30];
    z[109]=z[15]*z[109];

    r += n<T>(58363,6032)*z[23] + n<T>(23775,6032)*z[26] - n<T>(1059,6032)*z[28]
     - 
      n<T>(3417,3016)*z[32] + n<T>(81079,36192)*z[34] - n<T>(81079,3016)*z[38] + 
      n<T>(115907,6032)*z[41] - n<T>(653,6)*z[43] - n<T>(1306,3)*z[44] - n<T>(71483,18)
      *z[45] + n<T>(4972,9)*z[47] - n<T>(28822,3)*z[48] + 437*z[50] - n<T>(395,2)*
      z[51] + n<T>(653,2)*z[52] + 965*z[53] + n<T>(192157,36)*z[54] - n<T>(71277,6032)*z[60]
     - n<T>(5727,6032)*z[61]
     + n<T>(24433,6032)*z[63] - n<T>(15,4)*
      z[65] + 4*z[70] + 71*z[71] + 201*z[72] - n<T>(21,2)*z[73] - 1451*
      z[74] + 1517*z[75] + 45*z[76] - 81*z[77] + n<T>(259,3)*z[78] - n<T>(3392,3)*z[79]
     - n<T>(2513,2)*z[80]
     + n<T>(1511,3)*z[81]
     + n<T>(337,3)*z[82]
     + 9*
      z[83] - 126*z[84] - 26*z[85] + 38*z[86] - 46*z[87] + 89*z[88] + 
      z[91] + z[92] + z[93] + z[94] + z[95] + z[96] + z[97] + z[98] + 
      z[99] + z[100] + z[101] + 2*z[102] + n<T>(33,4)*z[103] + n<T>(81079,6032)
      *z[104] + 6*z[105] + 298*z[106] - 132*z[107] + n<T>(63,2)*z[108] + 
      z[109];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf923(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf923(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
