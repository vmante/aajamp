#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1088(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[105];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[9];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[10];
    z[26]=e[11];
    z[27]=e[12];
    z[28]=c[3];
    z[29]=c[11];
    z[30]=c[15];
    z[31]=c[17];
    z[32]=c[19];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[31];
    z[38]=e[3];
    z[39]=e[6];
    z[40]=e[13];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[3];
    z[45]=f[4];
    z[46]=f[5];
    z[47]=f[6];
    z[48]=f[7];
    z[49]=f[11];
    z[50]=f[12];
    z[51]=f[14];
    z[52]=f[16];
    z[53]=f[17];
    z[54]=f[18];
    z[55]=f[23];
    z[56]=f[31];
    z[57]=f[32];
    z[58]=f[33];
    z[59]=f[34];
    z[60]=f[36];
    z[61]=f[37];
    z[62]=f[39];
    z[63]=f[43];
    z[64]=f[51];
    z[65]=f[52];
    z[66]=f[55];
    z[67]=f[56];
    z[68]=f[58];
    z[69]=f[76];
    z[70]=f[105];
    z[71]=f[106];
    z[72]=2*z[17];
    z[73]= - z[72] + n<T>(5615,792)*z[16];
    z[74]=n<T>(27,8)*z[4];
    z[75]=n<T>(2,3)*z[3];
    z[76]=2*z[12];
    z[77]=z[8] + n<T>(16945,1584)*z[6] - n<T>(15,2)*z[2] + z[74] - n<T>(1163,88)*
    z[7] - z[75] - n<T>(4,3)*z[9] - z[76] - z[73];
    z[77]=z[8]*z[77];
    z[78]=2*z[11];
    z[79]=z[78] + n<T>(229,66)*z[15];
    z[80]=n<T>(1,2)*z[9];
    z[81]=z[80] - z[12];
    z[82]=n<T>(9,8)*z[4];
    z[83]= - n<T>(229,132)*z[5] - n<T>(5,6)*z[10] + n<T>(33,8)*z[8] + n<T>(2971,792)*
    z[6] - n<T>(1,4)*z[2] - z[82] + n<T>(2723,792)*z[7] - z[75] + z[81] - z[79];
    z[83]=z[5]*z[83];
    z[84]=3*z[12];
    z[72]= - z[80] + z[72] - z[84];
    z[72]=z[9]*z[72];
    z[85]=n<T>(1,2)*z[7];
    z[86]= - z[85] + n<T>(13,2)*z[9];
    z[87]=3*z[13] + n<T>(1,4)*z[14];
    z[88]=n<T>(11,4)*z[4] + z[76] + z[87] - z[86];
    z[88]=z[4]*z[88];
    z[89]=2*z[26];
    z[90]=n<T>(4,3)*z[25];
    z[91]=n<T>(5615,792)*z[24] + z[89] + z[90];
    z[92]=2*z[9];
    z[93]=n<T>(2723,264)*z[7];
    z[94]= - z[93] - z[92] - z[3];
    z[95]=z[2] - z[4];
    z[94]=n<T>(1,3)*z[94] + n<T>(9,4)*z[95];
    z[94]=z[2]*z[94];
    z[95]=n<T>(1,3)*z[3];
    z[96]=n<T>(5293,1056)*z[10] + n<T>(1691,176)*z[8] - n<T>(4193,176)*z[6] - n<T>(7,4)*
    z[4] + n<T>(1119,88)*z[7] + z[95] - z[80] - n<T>(1,2)*z[14] + 3*z[15];
    z[96]=z[10]*z[96];
    z[97]= - n<T>(229,66)*z[21] + 2*z[19];
    z[98]=n<T>(1,4)*z[20] + 3*z[27];
    z[99]=z[13]*z[84];
    z[78]= - z[78] + n<T>(5,6)*z[3];
    z[78]=z[3]*z[78];
    z[100]= - 5615*z[16] + n<T>(1387,2)*z[7];
    z[100]=z[7]*z[100];
    z[101]=n<T>(3,11)*z[15] + z[80];
    z[101]= - n<T>(157,1056)*z[6] + n<T>(3713,792)*z[2] + 19*z[101] - n<T>(3,2)*z[4]
   ;
    z[101]=z[6]*z[101];
    z[72]=z[83] + z[96] + z[77] + z[101] + z[94] + z[88] + n<T>(1,792)*
    z[100] + z[78] + z[72] + z[99] - n<T>(166,99)*z[22] - 11*z[15] + n<T>(9,2)*
    z[18] + n<T>(15,2)*z[23] + z[91] + z[98] + z[97];
    z[72]=z[1]*z[72];
    z[77]= - z[31] - 4*z[35] - n<T>(3,4)*z[33];
    z[72]=z[72] + 3*z[77] - n<T>(1319,9504)*z[29];
    z[72]=i*z[72];
    z[77]=z[3] - z[9];
    z[78]=2*z[77];
    z[83]= - z[78] + n<T>(1213,132)*z[7];
    z[83]=n<T>(1,3)*z[83];
    z[88]=n<T>(15,4)*z[2];
    z[94]= - n<T>(33,16)*z[8] + n<T>(1213,396)*z[6] - z[88] - z[83] + z[74];
    z[94]=z[8]*z[94];
    z[96]=npow(z[12],2);
    z[99]=n<T>(1,2)*z[96];
    z[90]=z[99] - z[90];
    z[78]= - z[78] + z[93];
    z[78]=n<T>(1,3)*z[78] + n<T>(3,2)*z[2];
    z[78]=z[2]*z[78];
    z[74]=n<T>(91,22)*z[5] - n<T>(25,6)*z[10] + n<T>(3,8)*z[8] - n<T>(2041,198)*z[6]
     + 
    z[2] - z[74] - n<T>(755,198)*z[7] + n<T>(11,3)*z[3] + 11 - z[81];
    z[74]=z[5]*z[74];
    z[93]=npow(z[3],2);
    z[100]=npow(z[9],2);
    z[101]=z[3] + z[12];
    z[102]=n<T>(289,99)*z[7] - z[101];
    z[102]=z[7]*z[102];
    z[103]= - z[3] + n<T>(9,16)*z[4];
    z[103]=z[4]*z[103];
    z[104]=3515*z[7] - 1337*z[2];
    z[104]=n<T>(1,2)*z[104] + 469*z[6];
    z[104]=z[6]*z[104];
    z[80]=n<T>(5,12)*z[10] + z[2] + z[4] + z[7] + z[80] + n<T>(5,3)*z[3];
    z[80]=z[10]*z[80];
    z[74]=n<T>(1,2)*z[74] + z[80] + z[94] + n<T>(1,396)*z[104] + z[78] + z[103]
    + z[102] + n<T>(1,6)*z[93] - n<T>(11,4)*z[100] - n<T>(89,264)*z[28] + n<T>(1823,792)
   *z[22] + z[90] - z[97];
    z[74]=z[5]*z[74];
    z[78]=z[82] - z[85] - z[101];
    z[78]=z[4]*z[78];
    z[80]=z[76] - z[95];
    z[80]=z[3]*z[80];
    z[81]=z[81] - z[4];
    z[81]=n<T>(3115,528)*z[10] - n<T>(1691,352)*z[8] - n<T>(3137,352)*z[6] - n<T>(9,8)*
    z[2] + n<T>(1295,176)*z[7] - z[75] - n<T>(1,2)*z[81];
    z[81]=z[10]*z[81];
    z[85]= - z[101] + n<T>(1,4)*z[7];
    z[85]=z[85]*z[7];
    z[94]=z[4] - z[12];
    z[97]= - z[3] - z[94];
    z[97]=z[2]*z[97];
    z[94]= - n<T>(4105,352)*z[6] + z[94];
    z[94]=z[6]*z[94];
    z[101]= - n<T>(1691,4)*z[8] - 1163*z[7] + n<T>(4017,2)*z[6];
    z[101]=z[8]*z[101];
    z[78]=z[81] + n<T>(1,88)*z[101] + z[94] + z[97] + z[78] + z[85] + z[80]
    - n<T>(1,4)*z[100] + z[99] + n<T>(7847,1584)*z[28] - 3*z[21] + n<T>(1,2)*z[20]
    + n<T>(1163,88)*z[39] + n<T>(2,3)*z[38];
    z[78]=z[10]*z[78];
    z[80]=z[92] - z[3];
    z[80]=z[80]*z[95];
    z[81]=n<T>(15,8)*z[2];
    z[83]=z[83] + z[81];
    z[83]=z[2]*z[83];
    z[88]= - z[8] - n<T>(16945,3168)*z[6] + z[88] - n<T>(27,16)*z[4] + n<T>(731,72)*
    z[7] + z[95] + z[12] - n<T>(1,3)*z[9];
    z[88]=z[8]*z[88];
    z[92]=n<T>(2,3)*z[100];
    z[94]=npow(z[7],2);
    z[95]=npow(z[4],2);
    z[97]= - n<T>(1213,9)*z[2] - n<T>(4017,8)*z[6];
    z[97]=z[6]*z[97];
    z[80]=z[88] + n<T>(1,44)*z[97] + z[83] - n<T>(27,16)*z[95] + n<T>(1163,176)*
    z[94] + z[80] + z[92] - n<T>(665,9504)*z[28] - n<T>(1213,198)*z[22] - n<T>(15,4)
   *z[23] - z[91];
    z[80]=z[8]*z[80];
    z[83]= - z[9] + 2*z[3];
    z[75]=z[83]*z[75];
    z[77]= - n<T>(1213,264)*z[7] + z[77];
    z[77]=n<T>(1,3)*z[77] - z[81];
    z[77]=z[2]*z[77];
    z[81]= - 5*z[23] - 3*z[18];
    z[82]=z[3] + z[82];
    z[82]=z[4]*z[82];
    z[75]=z[77] + z[82] - n<T>(1213,396)*z[94] + z[75] - z[92] - n<T>(47,24)*
    z[28] + n<T>(4357,792)*z[22] + n<T>(3,4)*z[81] - z[90];
    z[75]=z[2]*z[75];
    z[77]=z[4] + z[9];
    z[81]=n<T>(1,2)*z[3];
    z[77]=n<T>(73283,4752)*z[6] + n<T>(557,99)*z[2] - n<T>(9211,1584)*z[7] + z[81]
    + n<T>(19,6) - z[76] + n<T>(19,4)*z[77];
    z[77]=z[6]*z[77];
    z[76]= - z[76] - z[81];
    z[76]=z[3]*z[76];
    z[81]= - n<T>(26,3) + n<T>(5,2)*z[12];
    z[81]=z[12]*z[81];
    z[82]=n<T>(40,3) + n<T>(3,4)*z[9];
    z[82]=z[9]*z[82];
    z[83]= - n<T>(17003,1584)*z[7] + z[84] + z[3];
    z[83]=z[7]*z[83];
    z[88]=n<T>(3,4)*z[4] - n<T>(19,2)*z[9] - z[7];
    z[88]=z[4]*z[88];
    z[90]= - 2723*z[7] + 25*z[2];
    z[90]=z[2]*z[90];
    z[76]=z[77] + n<T>(1,792)*z[90] + z[88] + z[83] + z[76] + z[82] + z[81]
    + n<T>(1339,144)*z[28] + n<T>(26689,792)*z[22] - 4*z[40] - n<T>(57,11)*z[21];
    z[76]=z[6]*z[76];
    z[77]=z[84] + n<T>(13,4)*z[9];
    z[77]=z[9]*z[77];
    z[81]= - 5*z[12] + z[86];
    z[81]=n<T>(1,2)*z[81] - 2*z[4];
    z[81]=z[4]*z[81];
    z[77]=z[81] - z[85] + z[77] + n<T>(89,48)*z[28] - n<T>(9,4)*z[18] - z[98];
    z[77]=z[4]*z[77];
    z[73]= - z[73] + z[87] - z[79];
    z[73]=z[28]*z[73];
    z[79]= - n<T>(7,3)*z[25] + n<T>(1,3)*z[38] - z[19];
    z[79]= - n<T>(11,9)*z[93] + 3*z[100] + 2*z[79] - n<T>(7,18)*z[28];
    z[79]=z[3]*z[79];
    z[81]=1163*z[39] - n<T>(5615,9)*z[24];
    z[81]=n<T>(1,2)*z[81] + n<T>(9007,9)*z[22];
    z[81]=n<T>(1,22)*z[81] + n<T>(73,27)*z[28];
    z[82]= - z[12] + n<T>(1657,1188)*z[7];
    z[82]=z[7]*z[82];
    z[81]=z[82] + n<T>(1,2)*z[81] - z[96];
    z[81]=z[7]*z[81];
    z[82]= - z[41] - z[27];
    z[82]=z[82]*z[84];
    z[83]= - z[12] - n<T>(7,9)*z[9];
    z[83]=z[9]*z[83];
    z[83]=n<T>(5,2)*z[83] - n<T>(40,3)*z[12] - n<T>(31,18)*z[28] - 6*z[25] - 3*z[41]
    - z[89];
    z[83]=z[9]*z[83];
    z[84]=z[64] + z[52] + z[61];
    z[85]=z[59] - z[65];
    z[86]=z[51] + n<T>(3,2)*z[68] - 3*z[69] + z[71] - z[70];

    r += 11*z[21] - n<T>(13,792)*z[30] + n<T>(53,16)*z[32] + n<T>(15,8)*z[34] + 6*
      z[36] - n<T>(16613,132)*z[37] - n<T>(9,4)*z[42] + n<T>(9,8)*z[43] + n<T>(9,16)*
      z[44] - n<T>(41,12)*z[45] - n<T>(27,8)*z[46] - z[47] - n<T>(16,3)*z[48] - n<T>(33,16)*z[49]
     - n<T>(17,12)*z[50]
     - n<T>(27,16)*z[53]
     - n<T>(3,4)*z[54]
     + n<T>(2,3)*
      z[55] - n<T>(7241,3168)*z[56] + n<T>(85,132)*z[57] - n<T>(10003,396)*z[58]
     +  n<T>(1691,352)*z[60] - n<T>(3049,352)*z[62] - n<T>(1213,396)*z[63] - n<T>(13,4)*
      z[66] + z[67] + z[72] + z[73] + z[74] + z[75] + z[76] + z[77] + 
      z[78] + z[79] + z[80] + z[81] + z[82] + z[83] - n<T>(1,4)*z[84] - n<T>(3,2)*z[85]
     + n<T>(1,2)*z[86];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1088(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1088(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
