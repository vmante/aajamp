#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf684(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[52];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[8];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[31];
    z[24]=e[10];
    z[25]=e[6];
    z[26]=f[3];
    z[27]=f[4];
    z[28]=f[5];
    z[29]=f[11];
    z[30]=f[12];
    z[31]=f[16];
    z[32]=f[17];
    z[33]=f[31];
    z[34]=f[36];
    z[35]=f[39];
    z[36]=n<T>(645,2)*z[9];
    z[37]=z[36] + n<T>(2801,4)*z[7];
    z[38]=2477*z[3];
    z[39]=z[38] - z[37];
    z[40]=n<T>(1,4)*z[7];
    z[39]=z[39]*z[40];
    z[41]=npow(z[9],2);
    z[42]=npow(z[4],2);
    z[43]=npow(z[3],2);
    z[44]= - 1293*z[13] - n<T>(13463,3)*z[12];
    z[45]= - 8939*z[4] + 645*z[9];
    z[45]= - n<T>(22511,48)*z[6] + n<T>(3083,3)*z[2] - n<T>(4309,16)*z[7] + n<T>(1,16)*
    z[45] + 229*z[3];
    z[45]=z[5]*z[45];
    z[39]=z[45] + z[39] + n<T>(215,4)*z[43] - n<T>(645,16)*z[41] - n<T>(7649,16)*
    z[42] + n<T>(1,4)*z[44] + n<T>(599,9)*z[15];
    z[44]= - z[7] - n<T>(15725,9048)*z[2];
    z[44]=z[2]*z[44];
    z[45]=n<T>(58921,36192)*z[6] - n<T>(2153,9048)*z[2] + z[7] + n<T>(8939,6032)*
    z[4] - z[3];
    z[45]=z[6]*z[45];
    z[39]=z[45] + z[44] + n<T>(1,754)*z[39];
    z[39]=z[5]*z[39];
    z[44]=n<T>(1,754)*z[3];
    z[45]= - 215*z[8] + n<T>(1293,4)*z[3];
    z[45]=z[45]*z[44];
    z[46]=539*z[3];
    z[37]=z[46] + z[37];
    z[37]=z[7]*z[37];
    z[47]=npow(z[10],2);
    z[48]=n<T>(9,4)*z[47];
    z[49]=5*z[11];
    z[50]= - n<T>(9157,3)*z[14] + n<T>(1293,2)*z[13];
    z[51]= - z[10] + n<T>(24773,6032)*z[9];
    z[51]=z[9]*z[51];
    z[37]=n<T>(1,1508)*z[37] + z[45] + z[51] - z[48] + n<T>(109,4524)*z[12] + n<T>(1,754)*z[50]
     + z[49];
    z[45]=n<T>(4633,9048)*z[2];
    z[50]= - 5*z[4] - n<T>(3879,1508)*z[3];
    z[50]=z[45] + n<T>(1,2)*z[50] - z[7];
    z[50]=z[2]*z[50];
    z[37]=n<T>(1,2)*z[37] + z[50];
    z[36]= - n<T>(58921,6)*z[6] + n<T>(8939,3)*z[2] + n<T>(4309,2)*z[7] + z[38] - 
    z[36] - 1293*z[8] + n<T>(7649,2)*z[4];
    z[36]=z[5]*z[36];
    z[38]= - z[10] + n<T>(11,8)*z[9];
    z[50]=n<T>(6275,4524)*z[2] + n<T>(7431,12064)*z[4] - z[38];
    z[50]=z[6]*z[50];
    z[36]=n<T>(1,6032)*z[36] + n<T>(1,2)*z[37] + z[50];
    z[36]=z[1]*z[36];
    z[37]=z[21] + n<T>(1,2)*z[19];
    z[37]=13681*z[37] + n<T>(5063,2)*z[16];
    z[36]=n<T>(1,18096)*z[37] + z[36];
    z[36]=i*z[36];
    z[37]=z[20] - z[28];
    z[50]=z[30] + z[31];
    z[50]= - n<T>(2477,1508)*z[32] + z[33] - z[35] + z[34] + n<T>(215,1508)*
    z[50];
    z[51]=z[15]*z[8];
    z[37]= - n<T>(1293,377)*z[51] + n<T>(1723,754)*z[17] + n<T>(13681,27144)*z[18]
    - n<T>(13681,2262)*z[22] - n<T>(200555,4524)*z[23] - n<T>(7649,1508)*z[26] - 
   n<T>(2801,1508)*z[27] + 3*z[50] + n<T>(58921,4524)*z[29] - n<T>(13681,4524)*
    z[37];
    z[50]=z[10]*z[15];
    z[51]= - z[49] + n<T>(2047,3016)*z[15];
    z[51]=z[4]*z[51];
    z[37]=z[51] + n<T>(1,2)*z[37] + n<T>(13,3)*z[50];
    z[50]=z[10] + n<T>(3,2)*z[9];
    z[50]=z[9]*z[50];
    z[48]=z[50] - z[48] + 3*z[25] - n<T>(12709,9048)*z[15];
    z[48]=z[9]*z[48];
    z[50]= - n<T>(969,4)*z[43] - n<T>(2371,12)*z[15] - n<T>(539,2)*z[24] - 215*z[13]
   ;
    z[44]=z[50]*z[44];
    z[37]=z[44] + n<T>(1,2)*z[37] + z[48];
    z[44]=4633*z[14] - n<T>(86065,12)*z[15];
    z[44]= - n<T>(7431,1508)*z[42] + n<T>(1,1131)*z[44] + 9*z[47];
    z[47]=n<T>(1,2)*z[2];
    z[40]= - n<T>(13681,18096)*z[2] + z[3] - z[40];
    z[40]=z[40]*z[47];
    z[38]= - n<T>(1,8)*z[6] - z[45] - n<T>(3,8)*z[7] - n<T>(8939,12064)*z[4] + z[38]
   ;
    z[38]=z[6]*z[38];
    z[45]=n<T>(1,4)*z[10] - z[9];
    z[45]=z[9]*z[45];
    z[38]=n<T>(1,2)*z[38] + z[40] + n<T>(1,16)*z[44] + z[45];
    z[38]=z[6]*z[38];
    z[40]=n<T>(645,4)*z[9] + z[46];
    z[40]=z[7]*z[40];
    z[40]=n<T>(1,6032)*z[40] + n<T>(539,12064)*z[43] + n<T>(645,24128)*z[41] - n<T>(37,1131)*z[15]
     - n<T>(539,6032)*z[24]
     + z[12];
    z[40]=z[7]*z[40];
    z[41]= - n<T>(863,1508)*z[43] + 5*z[42] + n<T>(41261,13572)*z[15] + n<T>(6677,1131)*z[12]
     + n<T>(13681,2262)*z[14] - z[49];
    z[42]= - z[7]*z[3];
    z[43]=n<T>(3985,754)*z[2] + n<T>(863,754)*z[3] + 9*z[7];
    z[43]=z[2]*z[43];
    z[41]=n<T>(1,8)*z[43] + n<T>(1,4)*z[41] + z[42];
    z[41]=z[41]*z[47];

    r += z[36] + n<T>(1,4)*z[37] + z[38] + n<T>(1,2)*z[39] + z[40] + z[41];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf684(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf684(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
