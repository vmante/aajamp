#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf350(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[16];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=d[9];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[0];
    z[30]=e[3];
    z[31]=e[10];
    z[32]=e[6];
    z[33]=f[0];
    z[34]=f[1];
    z[35]=f[3];
    z[36]=f[4];
    z[37]=f[5];
    z[38]=f[11];
    z[39]=f[12];
    z[40]=f[16];
    z[41]=f[17];
    z[42]=f[18];
    z[43]=f[31];
    z[44]=f[39];
    z[45]=npow(z[11],2);
    z[45]=n<T>(7,2)*z[45];
    z[46]=npow(z[12],2);
    z[47]= - z[45] + n<T>(7,2)*z[46];
    z[48]=n<T>(1,12)*z[7];
    z[49]=n<T>(1483,2)*z[10] - n<T>(367,3)*z[7];
    z[49]=z[49]*z[48];
    z[50]=z[3] - z[7];
    z[51]=z[13] - z[12];
    z[51]=n<T>(59,4)*z[6] + n<T>(5893,72)*z[5] - n<T>(6703,72)*z[2] - n<T>(59,2)*z[10]
    + 7*z[51] + n<T>(45,4)*z[4] - n<T>(9,4)*z[50];
    z[51]=z[6]*z[51];
    z[52]=npow(z[10],2);
    z[53]= - z[18] + n<T>(1,4)*z[16];
    z[54]=z[9] - n<T>(1,2)*z[4];
    z[54]=z[4]*z[54];
    z[55]= - n<T>(2111,8)*z[3] + n<T>(2039,4)*z[8] - 647*z[7];
    z[55]=z[3]*z[55];
    z[56]=3*z[7];
    z[57]=7*z[4] - z[56] - 3*z[3];
    z[57]=n<T>(1,4)*z[57] + n<T>(713,9)*z[2];
    z[57]=z[2]*z[57];
    z[58]=n<T>(1519,36)*z[8];
    z[59]= - n<T>(4849,72)*z[2] + n<T>(719,18)*z[3] - n<T>(1645,72)*z[7] - n<T>(1459,24)
   *z[10] + z[58] - 13*z[4];
    z[59]=z[5]*z[59];
    z[49]=z[51] + z[59] + z[57] + n<T>(1,18)*z[55] + z[49] - n<T>(209,16)*z[52]
    + n<T>(7,4)*z[54] + n<T>(4957,72)*z[14] - n<T>(1519,36)*z[15] + 7*z[53] + n<T>(6703,72)*z[17]
     + z[47];
    z[49]=z[1]*z[49];
    z[51]=n<T>(1591,48)*z[21] - n<T>(2915,2)*z[26] - 713*z[24];
    z[49]=n<T>(1,9)*z[51] + z[49];
    z[49]=i*z[49];
    z[46]=z[46] + z[32];
    z[51]=n<T>(1,8)*z[4];
    z[53]= - z[12] + z[51];
    z[53]=7*z[53] - n<T>(43,144)*z[10];
    z[53]=z[10]*z[53];
    z[45]=z[53] - n<T>(12769,288)*z[19] - z[45] + n<T>(271,8)*z[30] + 7*z[46];
    z[45]=z[10]*z[45];
    z[46]= - n<T>(53,8)*z[12] + z[58] + n<T>(7,4)*z[9] + n<T>(1,8)*z[20] + 7*z[13];
    z[46]=z[19]*z[46];
    z[45]=z[49] + z[46] + z[45];
    z[46]= - n<T>(1483,4)*z[10] + n<T>(143,3)*z[7];
    z[46]=z[46]*z[48];
    z[48]=n<T>(1,4)*z[2];
    z[49]=z[56] + n<T>(2573,72)*z[2];
    z[49]=z[49]*z[48];
    z[53]=npow(z[4],2);
    z[54]=n<T>(2711,96)*z[19] + n<T>(1519,8)*z[15] - 149*z[14];
    z[55]=701*z[7] - n<T>(1967,8)*z[3];
    z[55]=z[3]*z[55];
    z[57]=n<T>(2357,12)*z[2] - n<T>(3011,12)*z[3] + n<T>(1645,24)*z[7] - 149*z[4]
     + 
   n<T>(1459,8)*z[10];
    z[57]=z[5]*z[57];
    z[46]=n<T>(1,12)*z[57] + z[49] + n<T>(1,36)*z[55] + z[46] + n<T>(445,32)*z[52]
    + n<T>(1,9)*z[54] + n<T>(13,4)*z[53];
    z[46]=z[5]*z[46];
    z[49]=z[3]*z[7];
    z[54]=npow(z[7],2);
    z[49]=z[49] - z[54];
    z[55]=3*z[50] + n<T>(3509,36)*z[2];
    z[48]=z[55]*z[48];
    z[50]= - n<T>(569,72)*z[5] - n<T>(1219,36)*z[2] + n<T>(149,3)*z[4] + n<T>(3,2)*z[50]
   ;
    z[50]=z[5]*z[50];
    z[47]=z[50] + z[48] - n<T>(59,4)*z[52] - n<T>(223,16)*z[53] + n<T>(8203,432)*
    z[19] + 7*z[18] - n<T>(415,9)*z[17] - z[47] - n<T>(9,4)*z[49];
    z[48]= - n<T>(793,12)*z[4] + 59*z[10];
    z[48]=n<T>(43,72)*z[6] - n<T>(5893,288)*z[5] + n<T>(1,8)*z[48] + n<T>(176,9)*z[2];
    z[48]=z[6]*z[48];
    z[47]=n<T>(1,2)*z[47] + z[48];
    z[47]=z[6]*z[47];
    z[48]=n<T>(1,2)*z[3];
    z[49]=n<T>(3107,36)*z[3] - 25*z[10] - n<T>(1211,72)*z[7];
    z[49]=z[49]*z[48];
    z[50]= - n<T>(541,9)*z[19] + n<T>(2039,9)*z[15] + n<T>(761,9)*z[31] + 345*z[30];
    z[49]=z[49] - n<T>(1069,36)*z[54] + n<T>(1,8)*z[50] - 27*z[52];
    z[48]=z[49]*z[48];
    z[49]=1069*z[31] - n<T>(3049,12)*z[19];
    z[49]=n<T>(1,3)*z[49] + 283*z[52];
    z[50]=z[7]*z[10];
    z[49]=n<T>(1,6)*z[49] + 39*z[50];
    z[49]=z[7]*z[49];
    z[50]=z[25] - z[37];
    z[54]= - n<T>(1,6)*z[42] - z[44] + z[43];
    z[49]=z[49] - n<T>(5707,72)*z[22] - n<T>(1237,108)*z[23] + n<T>(2915,18)*z[27]
    + n<T>(41293,144)*z[28] - n<T>(7,2)*z[33] + n<T>(7,4)*z[34] + 13*z[35] + n<T>(143,9)
   *z[36] - n<T>(5893,72)*z[38] - n<T>(437,8)*z[39] + n<T>(195,8)*z[40] + 7*z[54]
    - n<T>(45,4)*z[41] + n<T>(713,9)*z[50];
    z[50]=z[29] + z[53] + z[52];
    z[50]=n<T>(3083,54)*z[19] - n<T>(2573,9)*z[14] - n<T>(3383,9)*z[17] - 7*z[50];
    z[52]= - z[3]*z[56];
    z[54]=npow(z[2],2);
    z[50]= - n<T>(2915,18)*z[54] + n<T>(1,2)*z[50] + z[52];
    z[50]=z[2]*z[50];
    z[52]=n<T>(1,2)*z[29] - z[16];
    z[52]=n<T>(119,12)*z[53] + 7*z[52] - n<T>(2113,72)*z[19];
    z[51]=z[52]*z[51];
    z[52]=z[12]*z[30];

    r += n<T>(1,2)*z[45] + z[46] + z[47] + z[48] + n<T>(1,4)*z[49] + n<T>(1,8)*
      z[50] + z[51] + z[52];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf350(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf350(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
