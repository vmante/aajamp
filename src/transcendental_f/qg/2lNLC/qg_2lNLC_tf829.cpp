#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf829(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[109];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[77];
    z[3]=f[79];
    z[4]=f[81];
    z[5]=c[11];
    z[6]=d[0];
    z[7]=d[1];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[7];
    z[13]=d[8];
    z[14]=d[9];
    z[15]=c[12];
    z[16]=c[13];
    z[17]=c[20];
    z[18]=c[23];
    z[19]=c[32];
    z[20]=c[33];
    z[21]=c[35];
    z[22]=c[37];
    z[23]=c[38];
    z[24]=c[39];
    z[25]=c[44];
    z[26]=c[47];
    z[27]=c[48];
    z[28]=c[49];
    z[29]=c[50];
    z[30]=c[55];
    z[31]=c[56];
    z[32]=c[57];
    z[33]=c[59];
    z[34]=c[81];
    z[35]=c[84];
    z[36]=f[24];
    z[37]=f[42];
    z[38]=f[44];
    z[39]=f[61];
    z[40]=f[62];
    z[41]=f[69];
    z[42]=f[70];
    z[43]=f[80];
    z[44]=f[22];
    z[45]=d[3];
    z[46]=g[24];
    z[47]=g[27];
    z[48]=g[38];
    z[49]=g[40];
    z[50]=g[48];
    z[51]=g[50];
    z[52]=g[79];
    z[53]=g[82];
    z[54]=g[90];
    z[55]=g[92];
    z[56]=g[99];
    z[57]=g[101];
    z[58]=g[131];
    z[59]=g[138];
    z[60]=g[145];
    z[61]=g[147];
    z[62]=g[170];
    z[63]=g[173];
    z[64]=g[181];
    z[65]=g[183];
    z[66]=g[189];
    z[67]=g[191];
    z[68]=g[198];
    z[69]=g[200];
    z[70]=g[201];
    z[71]=g[212];
    z[72]=g[214];
    z[73]=g[218];
    z[74]=g[220];
    z[75]=g[224];
    z[76]=g[243];
    z[77]=g[245];
    z[78]=g[251];
    z[79]=g[254];
    z[80]=g[270];
    z[81]=g[272];
    z[82]=g[276];
    z[83]=g[278];
    z[84]=g[281];
    z[85]=g[292];
    z[86]=g[294];
    z[87]=g[324];
    z[88]=g[329];
    z[89]=z[6]*i;
    z[90]=z[10]*i;
    z[91]=z[11]*i;
    z[92]=z[7]*i;
    z[93]=z[8]*i;
    z[94]=z[13]*i;
    z[95]=z[9]*i;
    z[96]= - n<T>(7,18)*z[95] - z[89] + z[93] - n<T>(13,36)*z[92] - n<T>(1,4)*z[94]
    + n<T>(67,36)*z[90] + n<T>(23,36)*z[91];
    z[97]=z[14]*i;
    z[98]= - n<T>(1,2)*z[97] + n<T>(1,3)*z[96];
    z[99]= - n<T>(1,16)*z[18] + n<T>(5,4)*z[17];
    z[98]=z[98]*z[99];
    z[96]= - n<T>(3,2)*z[97] + z[96];
    z[96]=z[16]*z[96];
    z[99]=z[7] - z[10];
    z[100]=z[9] + z[6];
    z[101]=z[100] + z[8];
    z[102]=z[13] + z[11];
    z[99]= - n<T>(11,8)*z[102] - n<T>(5,8)*z[101] - 2*z[99];
    z[99]=z[3]*z[99];
    z[101]=n<T>(1,3)*z[9];
    z[102]=z[7] + z[6];
    z[102]= - n<T>(31,2)*z[8] - z[10] + n<T>(19,2)*z[102];
    z[102]=n<T>(1,9)*z[102] - n<T>(1,2)*z[13];
    z[102]=n<T>(1,2)*z[102] + z[101];
    z[102]=z[2]*z[102];
    z[103]=n<T>(1,3)*z[11];
    z[104]=z[9] - z[103] - n<T>(1,3)*z[6] + z[10];
    z[105]=n<T>(1,3)*z[8];
    z[104]= - n<T>(1,6)*z[13] - z[105] + n<T>(1,2)*z[104];
    z[104]=z[4]*z[104];
    z[99]=n<T>(5,12)*z[104] + n<T>(1,9)*z[99] + n<T>(1,4)*z[102];
    z[89]=n<T>(2003,4)*z[92] + n<T>(407,4)*z[91] + 719*z[89] - n<T>(2717,4)*z[90];
    z[89]=n<T>(413,4)*z[94] + n<T>(1,3)*z[89] - 173*z[93];
    z[89]=n<T>(277,18)*z[97] + n<T>(1,3)*z[89] - n<T>(7,2)*z[95];
    z[89]=z[5]*z[89];
    z[89]=5*z[99] + n<T>(1,432)*z[89];
    z[90]=n<T>(31,6)*z[8];
    z[91]=n<T>(1,3)*z[14];
    z[92]=z[11] - z[6];
    z[92]= - 11*z[10] + n<T>(31,3)*z[92];
    z[92]=z[91] + z[101] + z[90] + n<T>(1,2)*z[92] - n<T>(1,3)*z[7];
    z[92]=z[36]*z[92];
    z[92]=z[92] + z[66];
    z[93]=z[10] + z[7] - z[11];
    z[93]= - z[14] - n<T>(13,18)*z[13] + z[6] + n<T>(31,18)*z[93];
    z[93]=z[37]*z[93];
    z[94]= - z[45] - z[11] + z[100];
    z[94]=z[39]*z[94];
    z[93]=z[69] + z[93] + z[94];
    z[94]=z[37] - z[42];
    z[95]=z[4] - z[3];
    z[95]=n<T>(1,2)*z[2] - 5*z[95];
    z[94]= - n<T>(11,36)*z[43] + n<T>(1,9)*z[95] - n<T>(1,4)*z[94];
    z[94]=z[45]*z[94];
    z[95]=n<T>(1,2)*z[8];
    z[97]= - n<T>(29,36)*z[14] - n<T>(1,4)*z[9] + n<T>(1,36)*z[13] + z[95] - n<T>(5,18)*
    z[7] + n<T>(19,36)*z[11] - n<T>(1,2)*z[6] + n<T>(7,9)*z[10];
    z[97]=z[40]*z[97];
    z[94]=z[97] + z[94] + z[73] + z[58];
    z[97]= - z[105] + z[103] + z[6] - n<T>(1,3)*z[10];
    z[91]= - z[91] + n<T>(1,2)*z[97] + n<T>(1,3)*z[13];
    z[91]=z[43]*z[91];
    z[91]=z[91] + z[54];
    z[97]= - z[10] + z[12] + z[14];
    z[99]=z[13] - z[7];
    z[100]=z[99] + z[8];
    z[97]=n<T>(1,4)*z[6] + n<T>(11,36)*z[100] - n<T>(5,9)*z[97];
    z[97]=z[41]*z[97];
    z[97]=z[97] + z[70];
    z[100]=n<T>(5,27)*z[2];
    z[101]=n<T>(5,27)*z[4];
    z[102]=z[101] + z[3] - z[100];
    z[103]=z[5]*i;
    z[102]=n<T>(5,54)*z[42] + z[15] + n<T>(1,2)*z[102] - n<T>(1,27)*z[103];
    z[102]=z[12]*z[102];
    z[102]=z[102] - z[76];
    z[103]=11*z[11] - n<T>(31,2)*z[6] - z[10];
    z[90]= - n<T>(13,6)*z[13] + z[90] + n<T>(1,3)*z[103] + n<T>(11,2)*z[7];
    z[103]=z[14] + z[9];
    z[90]=n<T>(1,2)*z[90] - n<T>(5,3)*z[103];
    z[90]=z[38]*z[90];
    z[103]=z[8] - z[10];
    z[103]=n<T>(1,6)*z[9] - n<T>(5,3)*z[7] - n<T>(7,2)*z[11] + n<T>(11,6)*z[6] - 5*
    z[103];
    z[103]=z[42]*z[103];
    z[90]=z[60] + z[90] + z[103];
    z[103]= - n<T>(133,24)*z[7] - n<T>(61,24)*z[11] - 7*z[6] + n<T>(115,24)*z[10];
    z[95]=n<T>(1,9)*z[103] + z[95];
    z[95]=n<T>(5,108)*z[14] + n<T>(7,162)*z[9] + n<T>(1,3)*z[95] - n<T>(1,8)*z[13];
    z[95]=z[15]*z[95];
    z[100]= - n<T>(1,2)*z[3] + z[100];
    z[100]=n<T>(1,2)*z[100] + z[101];
    z[100]=z[1]*i*z[100];
    z[101]=z[84] - z[83] - z[80] + z[51] + z[49] + z[46];
    z[103]=z[47] + z[55] + z[53] + z[52] + z[48];
    z[104]= - z[59] + z[74] - z[72];
    z[105]=z[57] - z[50];
    z[106]=z[64] + z[63];
    z[107]=z[65] + z[62];
    z[108]= - n<T>(5,18)*z[35] + n<T>(325,1296)*z[34] - n<T>(1,108)*z[33] - n<T>(5,48)*
    z[32] + n<T>(5,12)*z[31] + n<T>(5,6)*z[29] + z[28] - n<T>(487,5832)*z[26] - n<T>(25,36)*z[25]
     + n<T>(545,11664)*z[24] - n<T>(1075,324)*z[23] - n<T>(5,3)*z[22]
     + n<T>(35,72)*z[20];
    z[108]=i*z[108];
    z[99]=z[99] + z[11] - z[10];
    z[99]=z[44]*z[99];

    r +=  - n<T>(89,7776)*z[19] - n<T>(1,6)*z[21] + n<T>(95,108)*z[27] + n<T>(1,288)*
      z[30] - n<T>(65,1296)*z[56] + n<T>(95,648)*z[61] - n<T>(245,1296)*z[67] + n<T>(5,96)*z[68]
     + n<T>(155,216)*z[71] - n<T>(35,216)*z[75]
     + n<T>(5,216)*z[77]
     + n<T>(25,216)*z[78]
     + n<T>(5,54)*z[79]
     + n<T>(55,108)*z[81] - n<T>(185,216)*z[82]
     +  n<T>(25,54)*z[85] - n<T>(25,108)*z[86] - n<T>(5,18)*z[87] + n<T>(25,36)*z[88]
     + n<T>(1,3)*z[89]
     + n<T>(5,72)*z[90]
     + n<T>(55,144)*z[91]
     + n<T>(5,144)*z[92]
     + n<T>(5,48)
      *z[93] + n<T>(5,12)*z[94] + 5*z[95] + z[96] + n<T>(5,24)*z[97] + z[98] + 
      n<T>(55,864)*z[99] + n<T>(5,2)*z[100] + n<T>(55,432)*z[101] + n<T>(5,4)*z[102]
     +  n<T>(155,432)*z[103] + n<T>(95,216)*z[104] + n<T>(55,1296)*z[105] - n<T>(185,288)
      *z[106] + n<T>(245,864)*z[107] + z[108];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf829(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf829(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
