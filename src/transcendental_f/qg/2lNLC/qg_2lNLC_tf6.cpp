#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf6(
  const std::array<std::complex<T>,25>& d
) {
    return d[1];
}

template std::complex<double> qg_2lNLC_tf6(
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf6(
  const std::array<std::complex<dd_real>,25>& d
);
#endif
