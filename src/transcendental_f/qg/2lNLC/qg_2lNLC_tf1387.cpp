#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1387(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[19];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[9];
    z[3]=d[16];
    z[4]=d[18];
    z[5]=d[0];
    z[6]=d[4];
    z[7]=d[1];
    z[8]=d[2];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=e[4];
    z[12]=e[11];
    z[13]=n<T>(1,2)*z[9];
    z[14]= - z[7] + z[13];
    z[13]=z[14]*z[13];
    z[14]= - n<T>(1,2)*z[4] + n<T>(3,2)*z[3] - z[2];
    z[14]=z[14]*i*z[1];
    z[15]=n<T>(1,2)*z[7];
    z[16]= - z[6]*z[15];
    z[15]= - n<T>(3,4)*z[8] + z[6] + z[15];
    z[15]=z[8]*z[15];
    z[17]=z[6] + z[7];
    z[17]=n<T>(1,2)*z[17] - z[2];
    z[17]=z[10]*z[17];
    z[18]= - z[6] + z[2];
    z[18]=z[5]*z[18];

    r +=  - n<T>(3,2)*z[11] + n<T>(1,2)*z[12] + z[13] + z[14] + z[15] + z[16]
       + z[17] + z[18];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1387(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1387(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
