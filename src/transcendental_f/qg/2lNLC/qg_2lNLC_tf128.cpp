#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf128(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[34];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[4];
    z[9]=e[1];
    z[10]=e[2];
    z[11]=e[8];
    z[12]=c[3];
    z[13]=d[2];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[19];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[31];
    z[22]=e[10];
    z[23]=f[4];
    z[24]=f[5];
    z[25]=f[11];
    z[26]=z[5] + z[3];
    z[27]=z[1]*i;
    z[26]= - z[27]*z[26];
    z[26]= - z[12] + z[26];
    z[26]=z[7]*z[26];
    z[28]=2*z[27];
    z[29]=z[28] - z[2];
    z[30]=z[3] - z[29];
    z[30]=z[9]*z[30];
    z[31]=z[27] - z[3];
    z[32]= - z[5] + z[31];
    z[32]=z[10]*z[32];
    z[29]=z[4] - z[29];
    z[29]=z[11]*z[29];
    z[33]= - z[6] - z[5];
    z[33]=z[22]*z[33];
    z[26]=z[26] + z[30] + z[24] - z[23] - z[18] + z[15] + z[32] + z[29]
    + z[33] + z[25];
    z[29]=npow(z[3],2);
    z[30]= - z[3]*z[27];
    z[32]=z[6] + z[31];
    z[32]=z[6]*z[32];
    z[30]=z[32] + z[29] + z[30];
    z[32]= - 2*z[5] + z[3] + z[27] + z[6];
    z[32]=z[5]*z[32];
    z[30]=2*z[30] + z[32];
    z[30]=z[5]*z[30];
    z[32]=2*z[3];
    z[32]=z[32]*z[27];
    z[32]=z[32] - z[29];
    z[28]=z[3] + z[28] - z[4];
    z[28]=z[4]*z[28];
    z[28]=z[28] + z[32];
    z[33]=2*z[2] - z[4] - z[3] - 4*z[27];
    z[33]=z[2]*z[33];
    z[28]=2*z[28] + z[33];
    z[28]=z[2]*z[28];
    z[28]=z[30] + z[28];
    z[30]=2*z[32];
    z[32]=8*z[31] + z[6];
    z[32]=z[6]*z[32];
    z[32]=z[30] + z[32];
    z[32]=z[6]*z[32];
    z[33]=8*z[3];
    z[27]= - z[27]*z[33];
    z[27]=z[29] + z[27];
    z[33]=z[33] - z[4];
    z[33]=z[4]*z[33];
    z[27]=2*z[27] + z[33];
    z[27]=z[4]*z[27];
    z[31]= - 4*z[31] - z[6];
    z[31]=z[6]*z[31];
    z[30]=z[30] + z[31];
    z[31]= - 3*z[6] + z[8];
    z[31]=z[8]*z[31];
    z[30]=3*z[30] + z[31];
    z[30]=z[8]*z[30];
    z[31]=4*z[3];
    z[33]= - z[31] + z[4];
    z[33]=z[4]*z[33];
    z[29]=n<T>(1,2)*z[12] + 2*z[29] + z[33];
    z[33]=3*z[4] - z[13];
    z[33]=z[13]*z[33];
    z[29]=3*z[29] + z[33];
    z[29]=z[13]*z[29];
    z[33]=32*z[19] + 16*z[17] - n<T>(2,3)*z[14];
    z[33]=i*z[33];
    z[31]= - n<T>(7,2)*z[4] - z[31] + n<T>(19,2)*z[6];
    z[31]=n<T>(9,2)*z[8] - n<T>(7,3)*z[2] + n<T>(1,3)*z[31] + z[5];
    z[31]=z[12]*z[31];

    r += n<T>(4,3)*z[16] - 16*z[20] - 28*z[21] + 8*z[26] + z[27] + 4*z[28]
       + z[29] + z[30] + z[31] + z[32] + z[33];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf128(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf128(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
