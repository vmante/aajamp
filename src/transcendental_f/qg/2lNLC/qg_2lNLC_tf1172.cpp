#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1172(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[41];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[9];
    z[7]=d[12];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[17];
    z[11]=e[5];
    z[12]=e[7];
    z[13]=e[9];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[31];
    z[18]=e[6];
    z[19]=f[31];
    z[20]=f[32];
    z[21]=f[33];
    z[22]=f[36];
    z[23]=f[39];
    z[24]=f[43];
    z[25]=z[1]*i;
    z[26]=2*z[3];
    z[27]= - z[26] - z[4];
    z[27]=z[27]*z[25];
    z[28]=z[25] - z[5];
    z[29]=z[4] - z[3];
    z[30]=n<T>(1,2)*z[29] + z[28];
    z[30]=z[5]*z[30];
    z[31]=npow(z[3],2);
    z[32]=2*z[31];
    z[33]=2*z[4];
    z[34]= - z[3] + z[33];
    z[34]=z[4]*z[34];
    z[35]=z[9]*z[29];
    z[27]=z[30] + z[35] + z[27] + z[32] + z[34];
    z[27]=z[5]*z[27];
    z[30]=5*z[3];
    z[34]=z[30] - z[33];
    z[34]=z[34]*z[25];
    z[35]=n<T>(3,2)*z[31];
    z[36]=npow(z[4],2);
    z[37]= - n<T>(5,2)*z[3] + 3*z[4];
    z[37]=z[9]*z[37];
    z[34]=z[37] + z[34] - z[35] + z[36];
    z[34]=z[9]*z[34];
    z[36]=3*z[3];
    z[37]=z[36] - z[33];
    z[38]=n<T>(1,2)*z[9];
    z[39]= - z[38] + z[25] + z[37];
    z[39]=z[9]*z[39];
    z[40]=z[8] + z[4];
    z[38]= - z[38] + n<T>(1,2)*z[25] - n<T>(3,2)*z[3] + z[40];
    z[38]=z[8]*z[38];
    z[37]= - z[37]*z[25];
    z[37]=z[38] + z[39] - z[35] + z[37];
    z[37]=z[8]*z[37];
    z[38]=z[9] + z[4];
    z[39]= - z[25]*z[38];
    z[39]= - z[14] + z[39];
    z[39]=z[10]*z[39];
    z[38]=z[25] - z[38];
    z[38]=z[13]*z[38];
    z[38]=z[39] + z[38];
    z[28]=z[28] - z[9];
    z[39]=n<T>(1,2)*z[2] + z[28];
    z[29]=z[29]*z[39];
    z[39]=z[3] + z[4];
    z[39]=z[4]*z[39];
    z[29]= - z[32] + z[39] + z[29];
    z[29]=z[2]*z[29];
    z[28]= - z[2] + z[30] + 4*z[4] - z[28];
    z[28]=z[12]*z[28];
    z[30]=z[18]*z[40];
    z[28]=z[28] + z[30];
    z[26]= - z[26] + n<T>(1,3)*z[4];
    z[26]=z[4]*z[26];
    z[26]= - z[31] + z[26];
    z[26]=z[26]*z[33];
    z[30]=z[25]*z[35];
    z[31]=z[36] + 2*z[5];
    z[32]=z[25]*z[31];
    z[32]=2*z[14] + z[32];
    z[32]=z[7]*z[32];
    z[31]=2*z[25] - z[31];
    z[31]=z[11]*z[31];
    z[33]= - z[5] + z[7];
    z[25]=z[25]*z[33];
    z[25]= - z[11] + z[25];
    z[25]=z[6]*z[25];
    z[33]=z[23] - z[17];
    z[35]=npow(z[3],3);
    z[36]=z[5] + 7*z[3] - n<T>(5,3)*z[4];
    z[36]=n<T>(2,3)*z[8] - n<T>(1,3)*z[9] + n<T>(1,2)*z[36];
    z[36]=z[14]*z[36];
    z[39]=z[15]*i;

    r += z[16] - n<T>(7,2)*z[19] - 3*z[20] - 8*z[21] + n<T>(1,2)*z[22] + z[24]
       + z[25] + z[26] + z[27] + 2*z[28] + z[29] + z[30] + z[31] + 
      z[32] - n<T>(3,2)*z[33] + z[34] + n<T>(13,3)*z[35] + z[36] + z[37] + 4*
      z[38] - n<T>(1,3)*z[39];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1172(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1172(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
