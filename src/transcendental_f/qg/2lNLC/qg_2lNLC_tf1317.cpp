#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1317(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[26];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[17];
    z[8]=e[7];
    z[9]=e[9];
    z[10]=c[3];
    z[11]=c[11];
    z[12]=c[15];
    z[13]=f[31];
    z[14]=f[33];
    z[15]=f[35];
    z[16]=f[43];
    z[17]=i*z[1];
    z[18]= - 2*z[17] + z[5] - z[6];
    z[18]=z[18]*z[5];
    z[19]= - z[17] - n<T>(1,2)*z[2] + z[5] + z[6];
    z[19]=z[19]*z[2];
    z[20]=npow(z[6],2);
    z[18]= - z[20] + z[18] + z[19];
    z[19]=2*z[9];
    z[20]=2*z[8];
    z[21]=z[19] - z[20];
    z[22]=2*z[7];
    z[23]= - z[17]*z[22];
    z[24]=z[3] - z[5] + z[2];
    z[24]=z[3]*z[24];
    z[25]=z[17] + z[5];
    z[25]=n<T>(1,6)*z[4] - z[3] + n<T>(1,2)*z[25] + z[2];
    z[25]=z[4]*z[25];
    z[23]=z[25] + z[24] + z[23] + n<T>(7,6)*z[10] - z[21] - z[18];
    z[23]=z[4]*z[23];
    z[24]=2*z[6];
    z[24]=z[17]*z[24];
    z[17]=3*z[17] + z[5];
    z[17]= - n<T>(1,6)*z[3] + n<T>(1,2)*z[17] - 2*z[2];
    z[17]=z[3]*z[17];
    z[17]=z[17] + z[24] + z[10] + z[8] + z[18];
    z[17]=z[3]*z[17];
    z[18]= - n<T>(1,6)*z[10] - z[21];
    z[18]=z[6]*z[18];
    z[21]= - z[6]*z[22];
    z[19]=z[21] + z[19] + z[8];
    z[19]=z[1]*z[19];
    z[19]= - n<T>(1,6)*z[11] + z[19];
    z[19]=i*z[19];
    z[21]= - z[14] + z[12] - z[13];
    z[22]= - z[10]*z[22];
    z[24]= - z[5]*z[8];
    z[20]= - z[2]*z[20];

    r +=  - z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + 2*z[21] + 
      z[22] + z[23] + z[24];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1317(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1317(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
