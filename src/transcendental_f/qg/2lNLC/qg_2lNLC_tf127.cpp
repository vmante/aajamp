#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf127(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[72];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=e[13];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[21];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[0];
    z[34]=e[3];
    z[35]=e[10];
    z[36]=e[6];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[51];
    z[52]=f[55];
    z[53]=f[58];
    z[54]=n<T>(159,4)*z[18] + 9*z[21];
    z[55]=17*z[20];
    z[56]= - n<T>(53,4)*z[13] - 3*z[12];
    z[56]=3*z[56] + n<T>(107,8)*z[4];
    z[56]=z[4]*z[56];
    z[56]=z[56] + n<T>(11995,72)*z[16] - n<T>(5317,36)*z[17] - n<T>(1631,72)*z[19]
    + z[55] - z[54];
    z[57]=n<T>(1,2)*z[15];
    z[58]= - n<T>(3,2)*z[12] + z[15];
    z[58]=z[58]*z[57];
    z[59]=n<T>(1,4)*z[9];
    z[60]= - n<T>(2455,72)*z[9] - z[15] - n<T>(373,18)*z[3];
    z[60]=z[60]*z[59];
    z[61]=z[6] + n<T>(111,16)*z[11];
    z[62]=17*z[14];
    z[63]= - n<T>(143,4)*z[4] - n<T>(123,4)*z[3] - z[62] + 9*z[7];
    z[63]= - n<T>(243,32)*z[8] + n<T>(8687,576)*z[2] + n<T>(943,576)*z[5] - n<T>(5,32)*
    z[9] + n<T>(1,8)*z[63] + z[61];
    z[63]=z[8]*z[63];
    z[64]=npow(z[7],2);
    z[65]=473*z[10] - n<T>(1625,2)*z[3];
    z[65]=z[3]*z[65];
    z[66]= - z[7] + n<T>(13,8)*z[6];
    z[66]=z[6]*z[66];
    z[67]= - z[13] + z[4];
    z[67]= - n<T>(439,32)*z[11] + 15*z[67] + n<T>(4477,48)*z[9];
    z[67]=z[11]*z[67];
    z[68]=n<T>(5317,8)*z[10] + 551*z[3];
    z[68]=n<T>(1,9)*z[68] + n<T>(307,4)*z[4];
    z[68]= - n<T>(5053,192)*z[11] - n<T>(6919,576)*z[9] + n<T>(1,4)*z[68] - 2*z[6];
    z[68]=z[5]*z[68];
    z[69]=n<T>(137,32)*z[3];
    z[70]=z[69] - z[6];
    z[71]=n<T>(2101,144)*z[2] - n<T>(8215,576)*z[5] - n<T>(105,32)*z[9] - n<T>(503,32)*
    z[4] + z[7] - z[70];
    z[71]=z[2]*z[71];
    z[56]=z[63] + z[71] + z[68] + n<T>(1,4)*z[67] + z[60] + z[66] + n<T>(1,576)*
    z[65] + z[58] - n<T>(9,16)*z[64] + z[22] + n<T>(1,8)*z[56];
    z[56]=z[1]*z[56];
    z[58]= - 2159*z[30] - 2551*z[28];
    z[56]=z[56] + n<T>(7315,3456)*z[24] + n<T>(1,144)*z[58] - 3*z[27];
    z[56]=i*z[56];
    z[58]=npow(z[15],2);
    z[60]=z[4]*z[15];
    z[60]=z[58] - z[60];
    z[63]=n<T>(1,2)*z[9];
    z[65]= - 7*z[15] - n<T>(3931,36)*z[3];
    z[65]=z[65]*z[63];
    z[66]=npow(z[3],2);
    z[67]=npow(z[6],2);
    z[68]= - n<T>(18727,432)*z[23] + 9*z[37] + n<T>(4183,36)*z[35];
    z[60]=z[65] - n<T>(3,4)*z[67] + n<T>(1795,288)*z[66] + n<T>(1,2)*z[68] + 3*z[60]
   ;
    z[60]=z[60]*z[63];
    z[63]= - n<T>(3,2)*z[4] + z[6] + z[15];
    z[63]=z[6]*z[63];
    z[63]=z[58] - z[63];
    z[65]=3*z[22] + z[23];
    z[63]=n<T>(1,2)*z[65] - z[64] - n<T>(3,4)*z[63];
    z[63]=z[6]*z[63];
    z[65]=z[21] + z[22];
    z[65]=n<T>(1,6)*z[58] + 3*z[65] - n<T>(47,48)*z[23];
    z[57]=z[65]*z[57];
    z[54]=n<T>(3221,288)*z[23] - n<T>(71,8)*z[33] + z[54];
    z[65]=n<T>(1621,192)*z[4] + z[7] + n<T>(3,4)*z[15];
    z[65]=z[4]*z[65];
    z[54]=n<T>(1,4)*z[54] + z[65];
    z[54]=z[4]*z[54];
    z[65]=z[50] - z[51];
    z[65]= - n<T>(1,2)*z[53] + z[52] + n<T>(3,2)*z[65];
    z[62]=n<T>(5317,36)*z[10] - 9*z[12] - z[62] - n<T>(159,4)*z[13];
    z[62]=z[23]*z[62];
    z[68]=n<T>(595,32)*z[23] - z[64];
    z[68]=z[7]*z[68];
    z[71]=n<T>(3023,9)*z[66] - n<T>(5095,9)*z[23] + n<T>(473,9)*z[17] + n<T>(2579,9)*
    z[35] - z[34];
    z[71]=z[3]*z[71];
    z[54]=z[60] + z[63] + z[54] + n<T>(1,32)*z[71] + z[57] + n<T>(1,3)*z[68] + n<T>(1,4)*z[62]
     - n<T>(3781,576)*z[25]
     - n<T>(2629,216)*z[26]
     + n<T>(2983,144)*z[29]
    + n<T>(2591,144)*z[31] - n<T>(65309,1152)*z[32] + n<T>(375,16)*z[38] - n<T>(279,32)
   *z[39] - n<T>(307,16)*z[40] + n<T>(553,36)*z[41] - n<T>(3415,144)*z[42] - n<T>(943,576)*z[43]
     - n<T>(251,64)*z[44]
     + n<T>(233,64)*z[45]
     + n<T>(143,32)*z[46]
     + n<T>(125,16)*z[47] - n<T>(9,8)*z[48]
     + n<T>(3,4)*z[65]
     + z[49];
    z[57]=npow(z[4],2);
    z[60]=n<T>(5317,4)*z[17] - 1207*z[16];
    z[62]=479*z[3] + n<T>(481,2)*z[9];
    z[62]=z[9]*z[62];
    z[63]=n<T>(123,128)*z[11] - z[6] - n<T>(4477,192)*z[9];
    z[63]=z[11]*z[63];
    z[65]=n<T>(4477,64)*z[11] + n<T>(5767,192)*z[9] - n<T>(9599,96)*z[3] - 73*z[4];
    z[65]=z[5]*z[65];
    z[60]=n<T>(1,6)*z[65] + z[63] + n<T>(1,36)*z[62] - n<T>(1,2)*z[67] - n<T>(315,32)*
    z[57] + n<T>(679,1152)*z[66] + n<T>(14465,3456)*z[23] + n<T>(1,72)*z[60] - z[22]
   ;
    z[60]=z[5]*z[60];
    z[62]= - z[36] + n<T>(485,8)*z[34];
    z[62]=n<T>(1,2)*z[62] + 15*z[18];
    z[63]=npow(z[9],2);
    z[65]= - n<T>(279,8)*z[4] + 5*z[7] - n<T>(141,2)*z[3];
    z[65]=n<T>(3839,576)*z[11] + n<T>(83,6)*z[9] + n<T>(1,4)*z[65] - z[6];
    z[65]=z[11]*z[65];
    z[62]=n<T>(1,2)*z[65] + n<T>(105,16)*z[63] + n<T>(17,16)*z[67] + n<T>(161,32)*z[66]
    - n<T>(5,8)*z[64] - n<T>(21355,2304)*z[23] + n<T>(1,4)*z[62] + z[22];
    z[62]=z[11]*z[62];
    z[63]= - z[15] + n<T>(37,4)*z[7];
    z[65]=z[6] + 3*z[11];
    z[66]= - n<T>(2783,288)*z[2] + n<T>(4715,576)*z[5] - z[65] - z[63];
    z[66]=z[2]*z[66];
    z[68]=n<T>(42089,54)*z[23] - n<T>(2339,9)*z[16] + 71*z[33] + n<T>(5923,9)*z[19];
    z[58]=z[66] + n<T>(1,32)*z[68] - z[58];
    z[66]= - z[7] + n<T>(503,64)*z[4];
    z[66]=z[4]*z[66];
    z[68]=z[6]*z[7];
    z[69]=z[69] - z[15];
    z[69]= - z[9]*z[69];
    z[71]=npow(z[11],2);
    z[65]=n<T>(8621,576)*z[5] + n<T>(41,16)*z[9] + z[65];
    z[65]=z[5]*z[65];
    z[58]=z[65] + n<T>(279,64)*z[71] + z[69] + z[68] + z[66] + n<T>(1,2)*z[58];
    z[58]=z[2]*z[58];
    z[64]=z[67] - z[64];
    z[65]= - 123*z[3] + 59*z[9];
    z[59]=z[65]*z[59];
    z[55]=z[59] - n<T>(801,16)*z[57] - n<T>(23747,432)*z[23] - z[55] - n<T>(1073,18)
   *z[19] - n<T>(9,2)*z[64];
    z[57]= - n<T>(4825,1152)*z[2] - n<T>(6871,288)*z[5] + n<T>(23,32)*z[9] + z[70]
    + z[63];
    z[57]=z[2]*z[57];
    z[59]=z[6] + n<T>(79,32)*z[11];
    z[59]=z[11]*z[59];
    z[63]= - n<T>(7,8)*z[3] + n<T>(149,3)*z[4];
    z[63]= - n<T>(1613,576)*z[5] - n<T>(9,16)*z[9] + n<T>(1,2)*z[63] + z[6];
    z[63]=z[5]*z[63];
    z[61]=n<T>(139,144)*z[8] + n<T>(1235,72)*z[2] + n<T>(1721,576)*z[5] - n<T>(1507,192)
   *z[4] - n<T>(33,4)*z[7] + z[15] - z[61];
    z[61]=z[8]*z[61];
    z[55]=n<T>(1,2)*z[61] + z[57] + z[63] + n<T>(1,8)*z[55] + z[59];
    z[55]=z[8]*z[55];

    r += n<T>(1,2)*z[54] + z[55] + z[56] + z[58] + z[60] + z[62];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf127(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf127(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
