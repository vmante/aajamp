#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf626(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[53];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[8];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[31];
    z[24]=e[3];
    z[25]=e[10];
    z[26]=e[6];
    z[27]=f[3];
    z[28]=f[4];
    z[29]=f[5];
    z[30]=f[11];
    z[31]=f[12];
    z[32]=f[16];
    z[33]=f[17];
    z[34]=f[31];
    z[35]=f[36];
    z[36]=f[39];
    z[37]=127001*z[3];
    z[38]= - n<T>(108247,2)*z[5] - z[37];
    z[39]=z[1]*i;
    z[38]=z[39]*z[38];
    z[38]= - n<T>(108247,2)*z[15] + z[38];
    z[38]=z[8]*z[38];
    z[40]=z[39] - z[5];
    z[37]=n<T>(108247,2)*z[40] - z[37];
    z[37]=z[13]*z[37];
    z[41]=402211*z[39];
    z[42]= - z[41] + 163289*z[5];
    z[42]=n<T>(1,2)*z[42] + 119461*z[2];
    z[42]=z[12]*z[42];
    z[43]=n<T>(253769,2)*z[2] - 271865*z[39] + n<T>(289961,2)*z[6];
    z[43]=z[14]*z[43];
    z[37]=z[38] + z[42] + z[37] + z[43];
    z[38]=2*z[6];
    z[42]= - n<T>(559043,36192)*z[3] + n<T>(84777,12064)*z[2] + n<T>(127001,18096)*
    z[5] - n<T>(72713,36192)*z[39] + z[38];
    z[42]=z[3]*z[42];
    z[43]=2*z[39];
    z[44]=z[43] - z[6];
    z[44]=z[6]*z[44];
    z[45]= - n<T>(124081,4)*z[39] + 29041*z[5];
    z[45]=z[5]*z[45];
    z[46]=z[39] - n<T>(1,2)*z[2];
    z[46]=z[2]*z[46];
    z[42]=z[42] + n<T>(84777,6032)*z[46] + z[44] + n<T>(1,4524)*z[45];
    z[42]=z[3]*z[42];
    z[44]= - z[39] + n<T>(1,2)*z[5];
    z[44]=z[44]*z[5];
    z[45]=npow(z[6],2);
    z[46]= - 7*z[45] - n<T>(108247,2262)*z[44];
    z[38]=n<T>(160273,36192)*z[3] - n<T>(124081,18096)*z[5] + n<T>(160273,18096)*
    z[39] - z[38];
    z[38]=z[3]*z[38];
    z[47]= - n<T>(23,16)*z[2] + z[43] + n<T>(7,8)*z[6];
    z[47]=z[2]*z[47];
    z[48]=n<T>(178369,18096)*z[3] + z[2] - n<T>(501835,72384)*z[5] + n<T>(357067,72384)*z[39]
     + z[6];
    z[48]=z[7]*z[48];
    z[38]=z[48] + z[38] + n<T>(1,16)*z[46] + z[47];
    z[38]=z[7]*z[38];
    z[46]=n<T>(163289,2)*z[6];
    z[47]=n<T>(586187,2)*z[2] - 119461*z[5] - 696271*z[39] - z[46];
    z[47]=z[2]*z[47];
    z[48]=139891*z[39] - n<T>(271865,4)*z[6];
    z[48]=z[6]*z[48];
    z[41]=z[41] + 147455*z[6];
    z[41]=n<T>(1,4)*z[41] - 38843*z[5];
    z[41]=z[5]*z[41];
    z[41]=n<T>(1,4)*z[47] + z[48] + z[41];
    z[41]=z[2]*z[41];
    z[47]= - 93921*z[39] + n<T>(128605,2)*z[6];
    z[47]=z[6]*z[47];
    z[48]=n<T>(152733,2)*z[5] + 425*z[39] - 152733*z[6];
    z[48]=z[5]*z[48];
    z[47]=z[47] + z[48];
    z[48]=n<T>(31,4)*z[39] + z[2];
    z[48]=z[2]*z[48];
    z[49]=118049*z[6] - 425*z[5];
    z[49]=n<T>(1,3016)*z[49] - 39*z[2];
    z[49]=z[4]*z[49];
    z[47]=n<T>(1,8)*z[49] + n<T>(1,12064)*z[47] + z[48];
    z[47]=z[4]*z[47];
    z[48]= - z[39] + n<T>(1,2)*z[6];
    z[48]=z[48]*z[6];
    z[49]=z[39] - z[6];
    z[50]=n<T>(5,4)*z[49] - z[9];
    z[50]=z[9]*z[50];
    z[49]=z[9] + z[49];
    z[49]=z[10]*z[49];
    z[49]=n<T>(39,16)*z[49] + n<T>(9,2)*z[48] + z[50];
    z[49]=z[10]*z[49];
    z[40]= - n<T>(1,2)*z[7] - z[40];
    z[40]=z[7]*z[40];
    z[40]=z[44] - z[40];
    z[40]= - 15*z[48] - n<T>(39601,1508)*z[40];
    z[44]= - n<T>(74221,6032)*z[5] - n<T>(28323,6032)*z[39] + 17*z[6];
    z[44]= - n<T>(13,8)*z[9] - n<T>(70547,24128)*z[7] + n<T>(1,4)*z[44] + 6*z[3];
    z[44]=z[9]*z[44];
    z[40]= - n<T>(13,4)*z[26] + n<T>(3,8)*z[40] + z[44];
    z[40]=z[9]*z[40];
    z[43]= - z[43] + n<T>(3,16)*z[6];
    z[43]=z[43]*z[45];
    z[44]=z[5]*z[46];
    z[44]=805697*z[48] + z[44];
    z[44]=z[5]*z[44];
    z[45]= - z[34] + z[36] - z[35];
    z[46]=z[29] - z[20];
    z[48]=n<T>(1086185,18096)*z[21] + n<T>(1086185,36192)*z[19] - n<T>(278555,108576)
   *z[16];
    z[48]=i*z[48];
    z[50]=n<T>(233411,8)*z[9] + n<T>(65233,3)*z[7] + n<T>(122477,8)*z[3] - n<T>(694571,24)*z[2]
     - n<T>(724265,48)*z[6]
     - 5657*z[5];
    z[50]=n<T>(1,3)*z[50] + n<T>(11703,16)*z[4];
    z[50]=n<T>(1,377)*z[50] - n<T>(59,6)*z[10];
    z[50]=z[15]*z[50];
    z[39]= - 9*z[39] + n<T>(5,2)*z[2];
    z[39]=n<T>(39,8)*z[4] + n<T>(3,4)*z[39] + z[9];
    z[39]=z[11]*z[39];
    z[51]= - z[3] - z[9];
    z[51]=z[24]*z[51];
    z[52]= - 160273*z[3] - 196465*z[7];
    z[52]=z[25]*z[52];

    r += n<T>(362249,36192)*z[17] + n<T>(1086185,434304)*z[18] - n<T>(1086185,36192)
      *z[22] - n<T>(1759123,72384)*z[23] - n<T>(425,24128)*z[27] - n<T>(501835,72384)*z[28]
     + n<T>(805697,72384)*z[30]
     + n<T>(74221,24128)*z[31] - n<T>(70547,24128)*z[32]
     + n<T>(93921,24128)*z[33]
     + n<T>(1,9048)*z[37]
     + z[38]
     +  z[39] + z[40] + n<T>(1,4524)*z[41] + z[42] + z[43] + n<T>(1,36192)*z[44]
       + n<T>(13,16)*z[45] + n<T>(1086185,72384)*z[46] + z[47] + z[48] + z[49]
       + n<T>(1,4)*z[50] + 6*z[51] + n<T>(1,18096)*z[52];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf626(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf626(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
