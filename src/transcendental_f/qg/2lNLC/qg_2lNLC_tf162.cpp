#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf162(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[62];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[11];
    z[11]=d[16];
    z[12]=d[5];
    z[13]=d[9];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=e[12];
    z[19]=c[3];
    z[20]=d[6];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[0];
    z[30]=e[3];
    z[31]=e[10];
    z[32]=e[13];
    z[33]=e[14];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[18];
    z[44]=f[51];
    z[45]=f[55];
    z[46]=f[58];
    z[47]= - n<T>(2,11)*z[4] + 1 + n<T>(4,11)*z[13];
    z[47]=z[13]*z[47];
    z[48]=npow(z[3],2);
    z[49]=npow(z[9],2);
    z[50]=npow(z[12],2);
    z[51]=2*z[50];
    z[52]=n<T>(6427,432)*z[19] - z[51] + 6*z[33] - n<T>(2107,36)*z[31];
    z[53]=z[2]*z[3];
    z[54]= - 9*z[3] + 5*z[2];
    z[54]=z[6]*z[54];
    z[55]=n<T>(1,11)*z[2];
    z[56]=n<T>(13,44)*z[6] + z[55] + n<T>(2179,396)*z[3] - n<T>(3,11)*z[13] - n<T>(5,2)*
    z[9];
    z[56]=z[7]*z[56];
    z[47]=z[56] + n<T>(5,44)*z[54] - n<T>(39,44)*z[53] + n<T>(3629,1584)*z[48] - n<T>(73,33)*z[49]
     + n<T>(1,11)*z[52]
     + z[47];
    z[47]=z[7]*z[47];
    z[52]=npow(z[4],2);
    z[53]= - n<T>(31,4)*z[15] + 7*z[14];
    z[53]=43*z[53] - n<T>(2249,48)*z[19];
    z[54]=n<T>(6247,144)*z[6] + n<T>(763,36)*z[2] - n<T>(149,3)*z[4] + n<T>(3,2)*z[3];
    z[54]=z[6]*z[54];
    z[53]=z[54] - n<T>(47,4)*z[52] + n<T>(1,9)*z[53] + n<T>(5,16)*z[49];
    z[54]=4*z[3] - n<T>(851,144)*z[2];
    z[54]=z[54]*z[55];
    z[56]=z[6] - z[2];
    z[57]=n<T>(1453,8)*z[9];
    z[58]=z[57] - n<T>(403,3)*z[3];
    z[56]= - n<T>(563,18)*z[7] + n<T>(1,3)*z[58] - n<T>(3,2)*z[56];
    z[58]=n<T>(1,11)*z[7];
    z[56]=z[56]*z[58];
    z[57]= - n<T>(1135,24)*z[7] + n<T>(701,12)*z[6] - n<T>(2489,12)*z[2] + n<T>(2747,12)
   *z[3] - z[57] + 149*z[4];
    z[57]=z[5]*z[57];
    z[53]=n<T>(1,66)*z[57] + z[56] + z[54] + n<T>(163,144)*z[48] + n<T>(1,11)*z[53];
    z[53]=z[5]*z[53];
    z[54]=2*z[13];
    z[56]= - z[10]*z[54];
    z[57]=8*z[11] - n<T>(137,16)*z[9];
    z[57]=z[9]*z[57];
    z[59]=4*z[10] + n<T>(1,4)*z[11];
    z[60]=n<T>(49,8)*z[4] - 8*z[9] - z[59];
    z[60]=z[4]*z[60];
    z[51]=z[60] + z[57] + z[56] + z[51] - n<T>(3907,72)*z[14] + n<T>(1333,36)*
    z[15] - n<T>(1,4)*z[16] - 4*z[18] - n<T>(7849,72)*z[17];
    z[56]=n<T>(31,11)*z[4] - 5*z[3];
    z[56]=n<T>(1,2)*z[56] - n<T>(161,9)*z[2];
    z[56]=z[2]*z[56];
    z[57]= - n<T>(69,22)*z[6] + n<T>(10297,396)*z[2] - n<T>(45,22)*z[3] + 3*z[9]
     - 
   n<T>(125,22)*z[4];
    z[57]=z[6]*z[57];
    z[56]=z[56] + z[57];
    z[57]=n<T>(973,36)*z[7] - n<T>(19,4)*z[6] - n<T>(31,4)*z[2] + n<T>(592,9)*z[3]
     + 
    z[54] - n<T>(1453,24)*z[9];
    z[57]=z[57]*z[58];
    z[58]= - 163*z[8] + n<T>(2081,22)*z[3];
    z[58]=z[3]*z[58];
    z[60]= - n<T>(1333,3)*z[8] + n<T>(1453,2)*z[9];
    z[60]=n<T>(1,6)*z[60] + 47*z[4];
    z[60]=n<T>(4303,72)*z[2] + n<T>(1,2)*z[60] - n<T>(403,9)*z[3];
    z[60]=n<T>(1135,792)*z[7] + n<T>(1,11)*z[60] - n<T>(509,72)*z[6];
    z[60]=z[5]*z[60];
    z[51]=z[60] + z[57] + n<T>(1,11)*z[51] + n<T>(1,72)*z[58] + n<T>(1,2)*z[56];
    z[51]=z[1]*z[51];
    z[56]=2939*z[26] + 1609*z[24];
    z[56]=n<T>(1,11)*z[56] - n<T>(5,24)*z[21];
    z[51]=n<T>(1,18)*z[56] + z[51];
    z[51]=i*z[51];
    z[56]=z[44] + z[46];
    z[57]=n<T>(4,3)*z[12] + n<T>(15,8)*z[20] - n<T>(1333,36)*z[8] - z[59];
    z[57]=z[19]*z[57];
    z[58]= - n<T>(359,144)*z[49] + n<T>(11035,288)*z[19] - n<T>(157,8)*z[30] - 8*
    z[16];
    z[58]=z[9]*z[58];
    z[56]=z[58] + z[57] - n<T>(5,16)*z[40] - n<T>(345,16)*z[41] + n<T>(125,8)*z[42]
    - n<T>(31,12)*z[43] + z[45] - 2*z[56];
    z[57]=n<T>(1,11)*z[13];
    z[58]=z[57] - n<T>(7,48)*z[4];
    z[58]=z[4]*z[58];
    z[58]=z[58] + n<T>(31,88)*z[49] + n<T>(2779,3168)*z[19] - n<T>(2,11)*z[50] + n<T>(1,44)*z[16]
     - n<T>(3,8)*z[29]
     + n<T>(4,11)*z[18];
    z[58]=z[4]*z[58];
    z[59]=n<T>(31,11)*z[49];
    z[60]= - n<T>(31,11)*z[52] - z[59] + n<T>(1123,594)*z[19] + n<T>(1499,99)*z[14]
    + 3*z[29] + n<T>(3821,99)*z[17];
    z[61]= - 9*z[20] + n<T>(3131,72)*z[2];
    z[61]=z[61]*z[55];
    z[48]=z[61] + n<T>(1,8)*z[60] - n<T>(2,11)*z[48];
    z[48]=z[2]*z[48];
    z[60]=1007*z[17] - n<T>(6793,24)*z[19];
    z[49]=n<T>(27,8)*z[52] + n<T>(1,99)*z[60] + n<T>(3,2)*z[49];
    z[52]=6*z[20] + n<T>(13,4)*z[3];
    z[52]=3*z[52] - n<T>(3767,144)*z[2];
    z[52]=z[52]*z[55];
    z[55]= - n<T>(3,11)*z[20] - n<T>(1,4)*z[9];
    z[55]=n<T>(95,396)*z[6] - n<T>(1007,198)*z[2] + 3*z[55] + n<T>(1051,528)*z[4];
    z[55]=z[6]*z[55];
    z[49]=z[55] + n<T>(1,2)*z[49] + z[52];
    z[49]=z[6]*z[49];
    z[52]= - z[12] - n<T>(1,3)*z[13];
    z[52]=z[52]*z[54];
    z[50]=z[50] + 2*z[32];
    z[54]=z[18] + z[50];
    z[52]=z[52] + 2*z[54] - n<T>(25,24)*z[19];
    z[52]=z[52]*z[57];
    z[54]= - n<T>(3395,9)*z[31] - 183*z[30];
    z[54]= - n<T>(875,99)*z[19] + n<T>(1,11)*z[54] - n<T>(163,9)*z[15];
    z[54]=n<T>(1,4)*z[54] + z[59];
    z[55]= - n<T>(3515,792)*z[3] - 1 + n<T>(23,44)*z[9];
    z[55]=z[3]*z[55];
    z[54]=n<T>(1,2)*z[54] + z[55];
    z[54]=z[3]*z[54];
    z[55]=z[25] - z[38];
    z[50]=z[12]*z[50];

    r += n<T>(4333,1584)*z[22] + n<T>(1223,1188)*z[23] - n<T>(2939,396)*z[27] - 
      n<T>(38395,3168)*z[28] - n<T>(31,44)*z[34] + n<T>(31,88)*z[35] - n<T>(47,44)*
      z[36] - n<T>(563,198)*z[37] + n<T>(509,144)*z[39] + z[47] + z[48] + z[49]
       + n<T>(2,11)*z[50] + z[51] + z[52] + z[53] + z[54] - n<T>(1609,396)*
      z[55] + n<T>(1,11)*z[56] + z[58];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf162(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf162(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
