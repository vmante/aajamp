#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf624(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[63];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[9];
    z[12]=d[11];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[8];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[19];
    z[21]=c[23];
    z[22]=c[25];
    z[23]=c[26];
    z[24]=c[28];
    z[25]=c[31];
    z[26]=e[3];
    z[27]=e[10];
    z[28]=e[6];
    z[29]=e[13];
    z[30]=e[12];
    z[31]=f[3];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[11];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[17];
    z[38]=f[31];
    z[39]=f[36];
    z[40]=f[39];
    z[41]=f[51];
    z[42]=f[55];
    z[43]=f[58];
    z[44]=5*z[6];
    z[45]=z[1]*i;
    z[46]=n<T>(24405,1508)*z[7] - n<T>(15357,1508)*z[5] + n<T>(22897,1508)*z[45]
     - 
    z[44];
    z[47]=n<T>(1,2)*z[7];
    z[46]=z[46]*z[47];
    z[48]= - z[45] + n<T>(1,2)*z[6];
    z[49]=z[48]*z[6];
    z[50]= - n<T>(5119,8)*z[45] + 687*z[5];
    z[50]=z[5]*z[50];
    z[51]= - z[45] + n<T>(1,2)*z[2];
    z[52]=z[2]*z[51];
    z[53]= - n<T>(72107,1508)*z[3] + n<T>(27067,1508)*z[2] + n<T>(22897,1508)*z[7]
    + n<T>(15557,754)*z[5] - n<T>(6509,1508)*z[45] + 7*z[6];
    z[53]=z[3]*z[53];
    z[46]=n<T>(1,4)*z[53] - n<T>(27067,3016)*z[52] + z[46] - z[49] + n<T>(3,377)*
    z[50];
    z[46]=z[3]*z[46];
    z[50]= - 2269*z[45] + n<T>(3777,2)*z[6];
    z[50]=z[6]*z[50];
    z[52]=n<T>(41547,2)*z[5] - 20281*z[45] - 41547*z[6];
    z[52]=z[5]*z[52];
    z[50]=11*z[50] + z[52];
    z[52]=npow(z[7],2);
    z[53]=npow(z[10],2);
    z[50]=z[53] + n<T>(1,1508)*z[50] - z[52];
    z[53]=15*z[2];
    z[54]=z[45]*z[53];
    z[55]=24959*z[6] + 20281*z[5];
    z[53]=n<T>(1,3016)*z[55] - z[53];
    z[55]=n<T>(1,2)*z[4];
    z[53]=z[53]*z[55];
    z[50]=z[53] + z[54] + n<T>(1,2)*z[50];
    z[50]=z[50]*z[55];
    z[53]= - z[45] + n<T>(1,2)*z[5];
    z[53]=z[53]*z[5];
    z[54]=npow(z[6],2);
    z[56]= - z[54] - n<T>(2517,754)*z[53];
    z[44]=z[7] - n<T>(70399,3016)*z[5] + n<T>(52303,3016)*z[45] + z[44];
    z[44]=z[7]*z[44];
    z[44]=n<T>(7,2)*z[56] + z[44];
    z[44]=z[7]*z[44];
    z[44]=z[42] + z[44] + z[43];
    z[56]=15557*z[3];
    z[57]= - n<T>(17619,2)*z[5] - z[56];
    z[57]=z[45]*z[57];
    z[57]= - n<T>(17619,2)*z[17] + z[57];
    z[57]=z[8]*z[57];
    z[58]=z[45] - z[5];
    z[56]=n<T>(17619,2)*z[58] - z[56];
    z[56]=z[15]*z[56];
    z[58]=n<T>(19881,2)*z[2] - 21389*z[45] + n<T>(22897,2)*z[6];
    z[58]=z[16]*z[58];
    z[56]=z[58] + z[57] + z[56];
    z[57]=n<T>(1,2)*z[10];
    z[58]= - z[45] - z[57];
    z[58]=z[10]*z[58];
    z[59]=z[10] - z[55];
    z[59]=z[4]*z[59];
    z[60]=z[12]*z[45];
    z[58]= - z[30] + z[60] + n<T>(1,6)*z[17] + z[58] + z[59];
    z[58]=z[11]*z[58];
    z[59]=6007*z[45] - n<T>(21389,8)*z[6];
    z[59]=z[6]*z[59];
    z[60]=40439*z[45] + 14603*z[6];
    z[60]=n<T>(1,8)*z[60] - 1684*z[5];
    z[60]=z[5]*z[60];
    z[59]=z[59] + z[60];
    z[47]=z[47] + z[45];
    z[60]=n<T>(7,4)*z[6] + z[47];
    z[60]=z[7]*z[60];
    z[61]= - 4683*z[5] - 23533*z[45] - n<T>(6627,2)*z[6];
    z[61]=n<T>(63059,754)*z[2] + n<T>(3,377)*z[61] - 11*z[7];
    z[61]=z[2]*z[61];
    z[59]=n<T>(1,8)*z[61] + n<T>(1,377)*z[59] + z[60];
    z[59]=z[2]*z[59];
    z[60]=z[45] - z[6];
    z[60]=3*z[60] - z[9];
    z[60]=z[9]*z[60];
    z[52]=z[52] - z[60];
    z[60]=13*z[45] - 15*z[6];
    z[60]=n<T>(15,2)*z[9] + n<T>(1,2)*z[60] + 3*z[7];
    z[60]=n<T>(1,2)*z[60] - z[10];
    z[57]=z[60]*z[57];
    z[60]= - 2*z[45] + z[6];
    z[60]=z[6]*z[60];
    z[52]=z[57] + 2*z[60] - n<T>(1,2)*z[52];
    z[52]=z[10]*z[52];
    z[47]=z[5] - z[47];
    z[47]=z[7]*z[47];
    z[47]=z[53] - z[47];
    z[47]= - 13*z[49] - n<T>(48333,1508)*z[47];
    z[49]= - n<T>(5,4)*z[9] + n<T>(9,2)*z[3] - n<T>(27221,12064)*z[7] - n<T>(27067,12064)
   *z[5] - n<T>(9125,12064)*z[45] + 3*z[6];
    z[49]=z[9]*z[49];
    z[47]= - n<T>(5,2)*z[28] + n<T>(1,4)*z[47] + z[49];
    z[47]=z[9]*z[47];
    z[49]= - z[45] - n<T>(1,8)*z[6];
    z[49]=z[49]*z[54];
    z[51]=z[55] + z[51];
    z[51]=z[13]*z[51];
    z[53]= - z[38] + z[40] - z[39];
    z[54]=z[33] - z[22];
    z[55]=n<T>(105837,3016)*z[23] + n<T>(105837,6032)*z[21] - n<T>(9961,6032)*z[18];
    z[55]=i*z[55];
    z[48]=60597*z[48] + n<T>(12341,2)*z[5];
    z[48]=z[5]*z[6]*z[48];
    z[57]= - n<T>(1593,8)*z[6] - n<T>(535,3)*z[5];
    z[57]=n<T>(87541,48)*z[9] + n<T>(43655,48)*z[3] - n<T>(69491,48)*z[2] + n<T>(7,4)*
    z[57] + n<T>(4700,3)*z[7];
    z[57]= - n<T>(4247,12064)*z[4] + n<T>(1,377)*z[57] - n<T>(25,12)*z[10];
    z[57]=z[17]*z[57];
    z[45]= - 5777*z[45] + 1763*z[5];
    z[45]=n<T>(1,2)*z[45] + 2007*z[2];
    z[45]=z[14]*z[45];
    z[60]= - z[3] - z[9];
    z[60]=z[26]*z[60];
    z[61]= - 25913*z[7] - 22897*z[3];
    z[61]=z[27]*z[61];
    z[62]= - z[10] - z[11];
    z[62]=z[29]*z[62];

    r += n<T>(45717,6032)*z[19] + n<T>(35279,24128)*z[20] - n<T>(105837,6032)*z[24]
       - n<T>(315495,12064)*z[25] + n<T>(20281,12064)*z[31] - n<T>(70399,12064)*
      z[32] + n<T>(60597,12064)*z[34] + n<T>(27067,12064)*z[35] - n<T>(27221,12064)
      *z[36] + n<T>(24959,12064)*z[37] + n<T>(3,4)*z[41] + n<T>(1,4)*z[44] + n<T>(7,1508)*z[45]
     + z[46]
     + z[47]
     + n<T>(1,6032)*z[48]
     + z[49]
     + z[50]
     + n<T>(15,2)*z[51]
     + z[52]
     + n<T>(5,8)*z[53]
     + n<T>(105837,12064)*z[54]
     + z[55]
     +  n<T>(1,1508)*z[56] + z[57] + z[58] + z[59] + n<T>(9,2)*z[60] + n<T>(1,3016)*
      z[61] + z[62];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf624(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf624(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
