#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf316(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[65];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[16];
    z[10]=d[4];
    z[11]=d[12];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[5];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[17];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[0];
    z[31]=e[3];
    z[32]=e[10];
    z[33]=e[6];
    z[34]=d[5];
    z[35]=e[7];
    z[36]=f[0];
    z[37]=f[1];
    z[38]=f[3];
    z[39]=f[4];
    z[40]=f[5];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[16];
    z[44]=f[17];
    z[45]=f[18];
    z[46]=f[32];
    z[47]=f[36];
    z[48]=f[68];
    z[49]=47*z[7];
    z[50]=n<T>(11,4)*z[6];
    z[51]= - z[50] + n<T>(11,2)*z[8] + z[49];
    z[52]=n<T>(1,8)*z[6];
    z[51]=z[51]*z[52];
    z[53]=3*z[5];
    z[54]=z[13] - n<T>(5,2)*z[3];
    z[54]=z[53] + 3*z[54] + n<T>(137,8)*z[2];
    z[55]=n<T>(1,2)*z[5];
    z[54]=z[54]*z[55];
    z[56]=z[12] - n<T>(27,4)*z[7];
    z[56]= - n<T>(41,16)*z[10] + n<T>(1,2)*z[56] - z[53];
    z[56]=z[10]*z[56];
    z[57]=npow(z[7],2);
    z[58]=n<T>(85,16)*z[57] + n<T>(11,8)*z[15];
    z[59]=npow(z[12],2);
    z[60]=n<T>(9,8)*z[16] - n<T>(1,2)*z[19] + z[17];
    z[61]=z[9] - n<T>(1,2)*z[3];
    z[61]=z[3]*z[61];
    z[62]=27*z[3] - 79*z[2];
    z[62]=z[2]*z[62];
    z[63]=n<T>(11,8)*z[8];
    z[64]=n<T>(81,16)*z[10] - n<T>(77,16)*z[5] + n<T>(71,16)*z[2] + n<T>(3,8)*z[3]
     - n<T>(47,8)*z[6]
     - n<T>(13,16)*z[7]
     + 3*z[11] - z[63];
    z[64]=z[4]*z[64];
    z[51]=z[64] + n<T>(3,2)*z[56] + z[54] + n<T>(1,8)*z[62] + n<T>(27,8)*z[61] + 
    z[51] - n<T>(15,8)*z[59] - n<T>(71,16)*z[14] + 3*z[60] - n<T>(137,16)*z[18] + 
    z[58];
    z[51]=z[1]*z[51];
    z[51]=z[51] - n<T>(35,96)*z[21] + n<T>(3,2)*z[23] + 13*z[27] + n<T>(79,8)*z[25];
    z[51]=i*z[51];
    z[54]=npow(z[3],2);
    z[56]=27*z[7] + n<T>(53,2)*z[10];
    z[56]=z[10]*z[56];
    z[56]=z[54] - z[56];
    z[49]= - z[49] - z[50];
    z[49]=z[49]*z[52];
    z[50]=n<T>(13,4)*z[7];
    z[52]=n<T>(33,2)*z[3];
    z[60]=z[52] + z[50] + 29*z[6];
    z[60]= - n<T>(1,2)*z[4] - n<T>(81,32)*z[10] + n<T>(7,16)*z[5] + n<T>(1,8)*z[60]
     - 4*
    z[2];
    z[60]=z[4]*z[60];
    z[61]=npow(z[2],2);
    z[52]=n<T>(77,8)*z[5] - z[52] + 13*z[2];
    z[52]=z[5]*z[52];
    z[49]=z[60] + n<T>(1,4)*z[52] + n<T>(5,32)*z[61] + z[49] + n<T>(3,2)*z[59] - n<T>(49,96)*z[20]
     + n<T>(19,4)*z[14] - 3*z[17] - z[58] - n<T>(3,16)*z[56];
    z[49]=z[4]*z[49];
    z[52]=z[2] - z[3];
    z[56]=n<T>(3,2)*z[5];
    z[50]= - z[56] - z[6] + z[12] - z[50] - n<T>(9,4)*z[52];
    z[50]=3*z[50] - n<T>(59,8)*z[10];
    z[52]=n<T>(1,2)*z[10];
    z[50]=z[50]*z[52];
    z[53]= - z[12] + z[53];
    z[53]=z[53]*z[56];
    z[56]=n<T>(3,4)*z[59];
    z[58]= - z[33] + n<T>(9,8)*z[31];
    z[59]=npow(z[6],2);
    z[50]=z[50] + z[53] - n<T>(15,4)*z[59] - n<T>(39,8)*z[57] - z[56] + 3*z[58]
    - n<T>(49,32)*z[20];
    z[50]=z[50]*z[52];
    z[52]= - z[12] + n<T>(21,8)*z[3];
    z[52]=3*z[52] - n<T>(47,2)*z[2];
    z[52]=z[52]*z[55];
    z[52]=z[52] - n<T>(1,16)*z[61] + n<T>(63,16)*z[54] + z[56] + n<T>(25,48)*z[20]
    + 3*z[19] + n<T>(41,4)*z[18];
    z[52]=z[52]*z[55];
    z[53]=npow(z[34],2);
    z[53]=n<T>(1,2)*z[53];
    z[55]= - 5*z[34] + z[12];
    z[55]=z[12]*z[55];
    z[55]=n<T>(1,2)*z[55] - n<T>(3,2)*z[20] + z[35] + z[53];
    z[55]=z[12]*z[55];
    z[53]= - z[35] + z[53];
    z[53]=z[34]*z[53];
    z[53]= - z[47] + z[53] + z[55];
    z[55]=z[54] + z[30];
    z[55]= - n<T>(217,6)*z[20] - 5*z[14] + 55*z[18] - 27*z[55];
    z[55]=n<T>(1,4)*z[55] + 13*z[61];
    z[55]=z[2]*z[55];
    z[56]= - 31*z[32] + n<T>(125,24)*z[20];
    z[56]=z[7]*z[56];
    z[55]=z[56] + z[55];
    z[56]=n<T>(9,8)*z[9] + n<T>(1,2)*z[13] + z[11];
    z[56]=3*z[56] - z[63];
    z[56]=z[20]*z[56];
    z[58]=11*z[15] - 121*z[32] + 57*z[31];
    z[58]=n<T>(1,2)*z[58] - 9*z[20];
    z[57]=n<T>(1,2)*z[58] + 31*z[57];
    z[58]=n<T>(91,32)*z[7] - 2*z[6];
    z[58]=z[6]*z[58];
    z[57]=n<T>(1,4)*z[57] + z[58];
    z[57]=z[6]*z[57];
    z[58]=n<T>(1,2)*z[30] - z[16];
    z[54]= - n<T>(19,4)*z[54] + 27*z[58] - n<T>(101,8)*z[20];
    z[54]=z[3]*z[54];
    z[58]=z[26] - z[40];

    r +=  - n<T>(91,32)*z[22] + n<T>(241,96)*z[24] - n<T>(13,2)*z[28] + n<T>(1357,64)*
      z[29] - n<T>(27,8)*z[36] + n<T>(27,16)*z[37] - n<T>(3,16)*z[38] - n<T>(85,16)*
      z[39] + n<T>(77,32)*z[41] - n<T>(159,32)*z[42] - n<T>(75,32)*z[43] + n<T>(15,8)*
      z[44] - n<T>(9,8)*z[45] - n<T>(3,2)*z[46] + z[48] + z[49] + z[50] + z[51]
       + z[52] + n<T>(3,4)*z[53] + n<T>(1,8)*z[54] + n<T>(1,4)*z[55] + z[56] + 
      z[57] - n<T>(79,16)*z[58];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf316(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf316(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
