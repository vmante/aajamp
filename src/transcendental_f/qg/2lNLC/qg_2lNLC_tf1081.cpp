#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1081(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[35];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=e[6];
    z[17]=f[31];
    z[18]=f[32];
    z[19]=f[33];
    z[20]=f[36];
    z[21]=f[39];
    z[22]=f[43];
    z[23]=z[1]*i;
    z[24]=n<T>(1,2)*z[23];
    z[25]=z[7] + z[4];
    z[26]=n<T>(3,2)*z[3];
    z[27]= - n<T>(1,2)*z[8] + z[24] - z[26] + z[25];
    z[28]=n<T>(1,2)*z[7];
    z[27]=z[27]*z[28];
    z[26]=z[26] - z[4];
    z[29]= - z[26]*z[23];
    z[26]= - n<T>(1,4)*z[8] + z[24] + z[26];
    z[26]=z[8]*z[26];
    z[30]=npow(z[3],2);
    z[26]=z[27] + z[26] - n<T>(3,4)*z[30] + z[29];
    z[26]=z[7]*z[26];
    z[25]=z[16]*z[25];
    z[25]=z[26] + z[25];
    z[26]= - z[4] + n<T>(9,2)*z[3];
    z[24]=z[26]*z[24];
    z[26]=npow(z[4],2);
    z[27]=z[4] - n<T>(13,8)*z[3];
    z[27]=z[3]*z[27];
    z[29]=z[4] - n<T>(3,4)*z[3];
    z[29]=z[8]*z[29];
    z[24]=n<T>(3,2)*z[29] + z[24] + n<T>(1,4)*z[26] + z[27];
    z[24]=z[8]*z[24];
    z[27]=2*z[3];
    z[29]= - z[8] + z[27];
    z[31]=z[3] - z[4];
    z[29]=z[31]*z[29];
    z[32]= - z[4] - z[3];
    z[32]=z[32]*z[23];
    z[33]=z[23] - z[5];
    z[34]=z[4] + z[33];
    z[34]=z[5]*z[34];
    z[29]=z[34] + 2*z[32] + z[26] + z[29];
    z[29]=z[5]*z[29];
    z[32]= - 5*z[4] + n<T>(31,3)*z[3];
    z[32]=z[3]*z[32];
    z[32]= - 9*z[26] + z[32];
    z[32]=z[3]*z[32];
    z[30]=z[26] + n<T>(13,2)*z[30];
    z[30]=z[30]*z[23];
    z[30]=z[32] + z[30];
    z[32]= - n<T>(1,2)*z[2] + z[8] - z[33];
    z[31]=z[31]*z[32];
    z[27]=z[4] - z[27];
    z[27]=z[3]*z[27];
    z[26]=z[26] + z[27] + z[31];
    z[26]=z[2]*z[26];
    z[27]=z[5] + z[3];
    z[27]=z[23]*z[27];
    z[27]=z[13] + z[27];
    z[27]=z[6]*z[27];
    z[31]= - z[3] + z[33];
    z[31]=z[10]*z[31];
    z[27]= - z[18] + z[27] + z[31];
    z[31]=z[8] + z[4];
    z[31]= - z[23]*z[31];
    z[31]= - z[13] + z[31];
    z[31]=z[9]*z[31];
    z[23]=z[23] - z[8];
    z[32]= - z[4] + z[23];
    z[32]=z[12]*z[32];
    z[31]=z[31] + z[32];
    z[32]=z[2] - z[5];
    z[23]=9*z[4] + 13*z[3] - z[23];
    z[23]=n<T>(1,2)*z[23] - 2*z[32];
    z[23]=z[11]*z[23];
    z[32]=z[14]*i;
    z[32]=z[32] - z[20];
    z[33]=npow(z[4],3);
    z[28]=z[28] + n<T>(1,2)*z[5] - n<T>(5,24)*z[8] - z[4] + n<T>(7,4)*z[3];
    z[28]=z[13]*z[28];

    r += n<T>(1,2)*z[15] - n<T>(17,8)*z[17] - 5*z[19] - n<T>(9,8)*z[21] + z[22] + 
      z[23] + z[24] + n<T>(3,2)*z[25] + z[26] + 2*z[27] + z[28] + z[29] + n<T>(1,4)*z[30]
     + n<T>(5,2)*z[31] - n<T>(3,8)*z[32]
     + n<T>(2,3)*z[33];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1081(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1081(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
