#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf95(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[69];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[16];
    z[11]=d[5];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=d[9];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[21];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[3];
    z[31]=e[10];
    z[32]=e[6];
    z[33]=e[7];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[18];
    z[44]=f[31];
    z[45]=f[36];
    z[46]=f[39];
    z[47]=4*z[13];
    z[48]=3*z[12];
    z[49]=n<T>(1,2)*z[7];
    z[50]=n<T>(1,2)*z[3];
    z[51]= - n<T>(11,2)*z[6] + 13*z[9] + n<T>(131,3)*z[2] - z[50] + z[49] - n<T>(13,2)*z[4]
     - z[47]
     + z[48];
    z[51]=z[6]*z[51];
    z[52]=z[12] + 37*z[7];
    z[53]=2*z[10];
    z[54]=2*z[4];
    z[55]=n<T>(15,2)*z[9] - z[54] + z[53] - z[52];
    z[55]=z[9]*z[55];
    z[56]=73*z[7];
    z[57]=n<T>(55,2)*z[3];
    z[58]=z[57] - 55*z[8] + z[56];
    z[59]=n<T>(1,3)*z[3];
    z[58]=z[58]*z[59];
    z[49]= - n<T>(277,6)*z[2] + z[50] + 6*z[4] + z[49];
    z[49]=z[2]*z[49];
    z[60]=n<T>(76,3)*z[8];
    z[61]=n<T>(73,3)*z[3];
    z[62]= - n<T>(229,6)*z[6] + 38*z[9] + n<T>(113,3)*z[2] - z[61] + n<T>(38,3)*z[7]
    + n<T>(1,2)*z[4] - z[60] - z[11];
    z[62]=z[5]*z[62];
    z[63]=npow(z[11],2);
    z[64]=n<T>(5,2)*z[63];
    z[65]=npow(z[7],2);
    z[66]=n<T>(38,3)*z[15] + z[16] + 2*z[18] - n<T>(67,3)*z[17];
    z[67]=z[11] - n<T>(3,2)*z[12];
    z[67]=z[12]*z[67];
    z[68]=z[10]*z[54];
    z[49]=z[62] + z[51] + z[55] + z[49] + z[58] + n<T>(41,6)*z[65] + z[68]
    + z[67] + z[64] + 2*z[66] - n<T>(113,3)*z[14];
    z[49]=z[1]*z[49];
    z[51]=244*z[27] + n<T>(271,2)*z[25];
    z[49]=z[49] - n<T>(85,36)*z[21] + n<T>(1,3)*z[51] + z[24];
    z[49]=i*z[49];
    z[51]=z[50]*z[7];
    z[55]=npow(z[12],2);
    z[58]=npow(z[4],2);
    z[62]= - z[18] + n<T>(19,3)*z[17];
    z[66]= - z[7] - z[3];
    z[66]=n<T>(1,2)*z[66] - n<T>(29,3)*z[2];
    z[66]=z[2]*z[66];
    z[67]=npow(z[9],2);
    z[68]= - n<T>(1,6)*z[6] - n<T>(13,2)*z[9] - n<T>(67,3)*z[2] + z[12] + n<T>(37,4)*
    z[4];
    z[68]=z[6]*z[68];
    z[62]=z[68] + 7*z[67] + z[66] - z[51] + n<T>(1,2)*z[65] + n<T>(39,4)*z[58]
    + n<T>(3,2)*z[55] - n<T>(247,36)*z[19] + 4*z[62] - z[64];
    z[62]=z[6]*z[62];
    z[56]= - z[56] + z[57];
    z[56]=z[56]*z[59];
    z[57]=z[12] + n<T>(63,2)*z[4];
    z[59]= - n<T>(1,6)*z[5] + n<T>(67,12)*z[6] - n<T>(37,2)*z[9] - n<T>(125,6)*z[2]
     + 
    z[61] + n<T>(1,2)*z[57] - n<T>(19,3)*z[7];
    z[59]=z[5]*z[59];
    z[61]= - 19*z[15] + 16*z[14];
    z[61]=4*z[61] - n<T>(155,12)*z[19];
    z[64]= - z[12]*z[11];
    z[66]=z[11] - n<T>(49,6)*z[2];
    z[66]=z[2]*z[66];
    z[52]= - n<T>(29,2)*z[9] - z[2] + z[52];
    z[52]=z[9]*z[52];
    z[57]=n<T>(229,12)*z[6] + n<T>(61,3)*z[2] + z[3] - z[57];
    z[57]=z[6]*z[57];
    z[52]=z[59] + z[57] + z[52] + z[66] + z[56] - n<T>(22,3)*z[65] - n<T>(1,4)*
    z[58] + n<T>(1,3)*z[61] + z[64];
    z[52]=z[5]*z[52];
    z[48]= - n<T>(11,6)*z[9] - 2*z[2] + 15*z[3] - 12*z[7] + z[48] + z[54];
    z[48]=z[9]*z[48];
    z[54]= - z[16] - 2*z[32] - 9*z[30];
    z[56]=npow(z[3],2);
    z[57]=npow(z[2],2);
    z[48]=z[48] + n<T>(1,2)*z[57] + n<T>(11,2)*z[56] - n<T>(25,2)*z[65] - n<T>(7,2)*
    z[55] + n<T>(155,6)*z[19] + 2*z[54] + n<T>(3,2)*z[63];
    z[48]=z[9]*z[48];
    z[54]= - n<T>(257,12)*z[19] + 58*z[17] + 49*z[14];
    z[55]= - n<T>(1,2)*z[11] + n<T>(64,3)*z[2];
    z[55]=z[2]*z[55];
    z[51]=z[55] + z[51] + n<T>(1,3)*z[54] - 3*z[58];
    z[51]=z[2]*z[51];
    z[47]= - z[60] + z[53] + n<T>(1,12)*z[20] - z[47];
    z[47]=z[19]*z[47];
    z[53]=n<T>(37,3)*z[7] - 51*z[3];
    z[50]=z[53]*z[50];
    z[50]=z[50] + n<T>(58,3)*z[65] + n<T>(11,4)*z[19] - n<T>(55,3)*z[15] - n<T>(22,3)*
    z[31] - 23*z[30];
    z[50]=z[3]*z[50];
    z[53]= - 5*z[44] + 3*z[46] - z[45];
    z[54]=z[33] + n<T>(1,2)*z[63];
    z[55]=z[11]*z[54];
    z[54]=n<T>(41,12)*z[19] + z[54];
    z[54]=z[12]*z[54];
    z[56]=z[58] - z[19];
    z[56]= - 2*z[16] - n<T>(17,6)*z[56];
    z[56]=z[4]*z[56];
    z[57]= - 58*z[31] + n<T>(95,6)*z[19];
    z[57]=z[7]*z[57];

    r += n<T>(131,6)*z[22] + n<T>(421,72)*z[23] - n<T>(277,12)*z[26] - n<T>(247,6)*
      z[28] - n<T>(1003,12)*z[29] - 5*z[34] + 2*z[35] - n<T>(1,4)*z[36] - n<T>(22,3)
      *z[37] + n<T>(283,12)*z[38] + n<T>(229,12)*z[39] + n<T>(29,2)*z[40] - 6*z[41]
       + n<T>(13,4)*z[42] - n<T>(5,3)*z[43] + z[47] + z[48] + z[49] + z[50] + 
      z[51] + z[52] + n<T>(1,2)*z[53] + z[54] + z[55] + z[56] + n<T>(1,3)*z[57]
       + z[62];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf95(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf95(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
