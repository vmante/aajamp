#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf169(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[66];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[16];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=d[9];
    z[13]=d[6];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[0];
    z[28]=e[3];
    z[29]=e[10];
    z[30]=e[7];
    z[31]=e[13];
    z[32]=f[0];
    z[33]=f[1];
    z[34]=f[3];
    z[35]=f[4];
    z[36]=f[5];
    z[37]=f[11];
    z[38]=f[12];
    z[39]=f[16];
    z[40]=f[17];
    z[41]=f[18];
    z[42]=f[31];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[51];
    z[46]=f[55];
    z[47]=f[58];
    z[48]=n<T>(7,3)*z[3];
    z[49]=n<T>(9,4)*z[10];
    z[50]= - n<T>(4,3)*z[7] + z[49] - z[48];
    z[50]=z[7]*z[50];
    z[51]=n<T>(1,2)*z[6];
    z[52]=n<T>(11,12)*z[6];
    z[53]=5*z[4] - z[52];
    z[53]=z[53]*z[51];
    z[54]=npow(z[3],2);
    z[55]=z[12]*z[13];
    z[56]=npow(z[4],2);
    z[57]=npow(z[10],2);
    z[58]= - n<T>(5,2)*z[15] - 4*z[14];
    z[59]= - z[11]*z[13];
    z[60]= - n<T>(23,24)*z[2] + z[11] - n<T>(11,6)*z[6];
    z[60]=z[2]*z[60];
    z[61]= - n<T>(1,12)*z[7] + z[49] + z[12];
    z[62]=n<T>(19,6)*z[3] - n<T>(5,2)*z[4] - z[61];
    z[62]=n<T>(1,6)*z[5] + n<T>(13,12)*z[2] + n<T>(1,2)*z[62] - n<T>(1,3)*z[6];
    z[62]=z[5]*z[62];
    z[50]=z[62] + z[60] + z[53] + z[50] + n<T>(37,24)*z[54] + n<T>(3,4)*z[56] - 
   n<T>(5,8)*z[57] + z[59] + n<T>(7,24)*z[18] + n<T>(1,3)*z[58] + z[55];
    z[50]=z[5]*z[50];
    z[53]=n<T>(3,2)*z[4];
    z[48]=n<T>(19,12)*z[2] + z[52] - z[48] - z[53] - z[11] - n<T>(5,6)*z[8] + 
    z[61];
    z[48]=z[5]*z[48];
    z[52]=3*z[4];
    z[58]=n<T>(1,2)*z[3];
    z[59]= - z[51] - n<T>(1,2)*z[7] - z[58] + z[10] + z[52];
    z[59]=z[6]*z[59];
    z[60]=n<T>(1,2)*z[4];
    z[61]= - z[9] + z[60];
    z[53]=z[61]*z[53];
    z[61]=z[7] + z[3];
    z[52]= - n<T>(35,6)*z[6] - z[52] - z[61];
    z[52]=n<T>(1,2)*z[52] + n<T>(8,3)*z[2];
    z[52]=z[2]*z[52];
    z[62]= - n<T>(7,6)*z[14] + n<T>(5,3)*z[15] - 3*z[16] + n<T>(47,6)*z[17];
    z[63]=z[13] + n<T>(3,2)*z[11];
    z[64]=z[11]*z[63];
    z[65]= - z[8] + z[58];
    z[65]=z[3]*z[65];
    z[49]=n<T>(5,6)*z[7] - z[49] + n<T>(10,3)*z[3];
    z[49]=z[7]*z[49];
    z[48]=z[48] + z[52] + z[59] + z[49] + n<T>(37,12)*z[65] + z[53] - n<T>(7,8)*
    z[57] + z[64] + n<T>(1,2)*z[62] - z[55];
    z[48]=z[1]*z[48];
    z[49]=n<T>(7,24)*z[19] - 10*z[24] - n<T>(19,2)*z[22];
    z[48]=n<T>(1,3)*z[49] + z[48];
    z[48]=i*z[48];
    z[49]= - z[10] - n<T>(11,4)*z[4];
    z[49]=z[49]*z[51];
    z[51]=n<T>(3,2)*z[57];
    z[52]=npow(z[11],2);
    z[53]= - 7*z[17] - n<T>(1,24)*z[18];
    z[59]= - z[7]*z[58];
    z[49]=z[49] + z[59] - n<T>(11,8)*z[56] + z[51] + n<T>(1,3)*z[53] - z[52];
    z[49]=z[6]*z[49];
    z[53]= - n<T>(1,2)*z[27] + z[16];
    z[51]=n<T>(5,12)*z[56] - z[51] - z[52] + 3*z[53] + n<T>(19,24)*z[18];
    z[51]=z[51]*z[60];
    z[53]= - 11*z[29] + n<T>(5,12)*z[18];
    z[53]=n<T>(1,3)*z[53] - z[52];
    z[59]=n<T>(11,6)*z[3] - z[10] + z[60];
    z[59]=z[7]*z[59];
    z[53]=z[59] + n<T>(49,24)*z[54] + n<T>(1,2)*z[53] - z[57];
    z[53]=z[7]*z[53];
    z[54]=n<T>(1,2)*z[12];
    z[59]=z[54] + z[63];
    z[59]=z[11]*z[59];
    z[60]=2*z[30];
    z[55]=z[59] + n<T>(5,6)*z[18] - z[55] + z[31] + z[60];
    z[55]=z[11]*z[55];
    z[59]= - z[28] + n<T>(7,4)*z[18];
    z[52]=n<T>(13,24)*z[57] + n<T>(3,4)*z[59] - z[52];
    z[52]=z[10]*z[52];
    z[56]=z[27] + z[56] + z[57];
    z[56]=n<T>(115,18)*z[18] + n<T>(23,3)*z[14] - n<T>(19,3)*z[17] + 3*z[56];
    z[57]=z[7]*z[3];
    z[59]= - n<T>(7,6)*z[2] - z[11] + n<T>(1,12)*z[6];
    z[59]=z[2]*z[59];
    z[56]=z[59] + n<T>(1,2)*z[56] - z[57];
    z[57]=n<T>(1,2)*z[61] + n<T>(7,3)*z[6];
    z[57]=z[6]*z[57];
    z[56]=z[57] + n<T>(1,2)*z[56];
    z[56]=z[2]*z[56];
    z[57]= - 5*z[18] - n<T>(37,3)*z[15] - n<T>(31,3)*z[29] - 9*z[28];
    z[59]=3*z[10] - n<T>(43,6)*z[3];
    z[59]=z[3]*z[59];
    z[57]=n<T>(1,2)*z[57] + z[59];
    z[57]=z[57]*z[58];
    z[54]=z[54] - n<T>(1,6)*z[13] - 3*z[9] - n<T>(5,3)*z[8];
    z[54]=z[18]*z[54];
    z[54]=z[41] + z[54] - z[45] - z[47] - z[46];
    z[58]=z[33] - z[34];
    z[59]=z[32] - z[40];
    z[61]=z[23] - z[36];
    z[60]=z[13]*z[60];
    z[62]=z[12]*z[31];

    r += n<T>(65,24)*z[20] - n<T>(73,72)*z[21] + n<T>(5,3)*z[25] - n<T>(463,48)*z[26]
       - n<T>(4,3)*z[35] - n<T>(11,24)*z[37] + n<T>(5,8)*z[38] - n<T>(7,8)*z[39]
     - 
      z[42] - z[43] - z[44] + z[48] + z[49] + z[50] + z[51] + z[52] + 
      z[53] + n<T>(1,2)*z[54] + z[55] + z[56] + z[57] - n<T>(3,4)*z[58] + n<T>(3,2)
      *z[59] + z[60] + n<T>(19,12)*z[61] + z[62];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf169(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf169(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
