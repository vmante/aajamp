#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf159(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[47];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[6];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=d[15];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=d[9];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[31];
    z[24]=e[3];
    z[25]=e[10];
    z[26]=f[3];
    z[27]=f[4];
    z[28]=f[5];
    z[29]=f[11];
    z[30]=f[12];
    z[31]=f[16];
    z[32]=f[17];
    z[33]=n<T>(1,3)*z[14];
    z[34]=npow(z[2],2);
    z[35]= - z[12] + z[11];
    z[35]=z[34] - 2*z[35] + z[33];
    z[36]=5*z[9];
    z[37]= - z[36] + 3*z[3];
    z[38]=4*z[10];
    z[39]=2*z[7] + 8*z[2] - z[38] + z[8] - z[37];
    z[39]=z[1]*z[39];
    z[40]=z[6]*z[1];
    z[39]=z[39] - 9*z[40];
    z[39]=i*z[39];
    z[41]=z[2] - z[3];
    z[42]=z[8] - z[9];
    z[41]=5*z[42] - 7*z[41];
    z[41]=z[6] + n<T>(1,2)*z[41] - z[7];
    z[41]=z[4]*z[41];
    z[42]=npow(z[9],2);
    z[43]=n<T>(7,2)*z[42];
    z[44]=npow(z[8],2);
    z[37]=n<T>(3,2)*z[7] - z[37];
    z[37]=z[7]*z[37];
    z[45]=n<T>(9,2)*z[6] - 5*z[8] + 3*z[2];
    z[45]=z[6]*z[45];
    z[35]=z[41] + z[39] + z[45] + z[37] - z[43] - n<T>(1,2)*z[44] - 2*z[35];
    z[35]=z[4]*z[35];
    z[33]=z[13] - z[33];
    z[37]=z[3] - z[5];
    z[39]= - n<T>(5,2)*z[2] - z[37];
    z[39]=z[2]*z[39];
    z[41]=z[3] + z[5];
    z[45]= - z[7] + z[41];
    z[45]=z[7]*z[45];
    z[46]= - n<T>(1,3)*z[6] - 4*z[2] - z[5] + n<T>(3,2)*z[8];
    z[46]=z[6]*z[46];
    z[33]=z[46] + z[45] + z[39] + 5*z[33] + n<T>(3,2)*z[44];
    z[33]=z[6]*z[33];
    z[39]=4*z[11] + 5*z[13];
    z[45]=2*z[12] - z[39];
    z[46]= - 9*z[2] + z[37];
    z[46]=z[2]*z[46];
    z[36]= - n<T>(1,2)*z[7] - z[36] + z[3];
    z[36]=z[7]*z[36];
    z[36]=z[36] + z[46] + 2*z[45] + z[43];
    z[36]=z[1]*z[36];
    z[41]=10*z[2] - z[8] + z[41];
    z[41]=z[1]*z[41];
    z[40]=z[41] - z[40];
    z[40]=z[6]*z[40];
    z[41]=2*z[21] + z[19];
    z[36]=z[40] + z[36] + 9*z[41] - n<T>(2,3)*z[16];
    z[36]=i*z[36];
    z[40]=4*z[24];
    z[41]=npow(z[3],2);
    z[43]= - n<T>(7,6)*z[41] + 4*z[42] + n<T>(13,6)*z[14] + 3*z[25] - z[40];
    z[43]=z[3]*z[43];
    z[41]=z[41] + z[42];
    z[37]=z[2]*z[37];
    z[45]= - n<T>(3,2)*z[9] + z[3];
    z[45]=z[7]*z[45];
    z[37]=z[45] + z[37] - z[25] - n<T>(2,3)*z[14] - n<T>(3,2)*z[41];
    z[37]=z[7]*z[37];
    z[40]=n<T>(1,3)*z[42] - z[40] + n<T>(17,6)*z[14];
    z[40]=z[9]*z[40];
    z[34]=n<T>(9,2)*z[34] - n<T>(1,2)*z[14] + z[39];
    z[34]=z[2]*z[34];
    z[39]=z[28] + z[29];
    z[39]= - z[26] + 3*z[27] + 7*z[30] + z[32] - z[31] + 9*z[39];
    z[38]=n<T>(1,6)*z[15] - z[38];
    z[38]=z[14]*z[38];
    z[41]=z[14] - n<T>(1,3)*z[44];
    z[41]=z[8]*z[41];

    r += 2*z[17] + n<T>(3,4)*z[18] - n<T>(9,2)*z[20] - 9*z[22] - 13*z[23] + 
      z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + n<T>(1,2)*z[39] + 
      z[40] + z[41] + z[43];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf159(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf159(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
