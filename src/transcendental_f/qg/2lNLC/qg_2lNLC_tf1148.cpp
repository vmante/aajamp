#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1148(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[25];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[15];
    z[6]=d[8];
    z[7]=e[2];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[17];
    z[12]=c[31];
    z[13]=e[3];
    z[14]=f[4];
    z[15]=f[12];
    z[16]=f[16];
    z[17]=f[19];
    z[18]= - z[2] + n<T>(1,2)*z[6];
    z[19]=i*z[1];
    z[20]=z[18]*z[19];
    z[21]=npow(z[6],2);
    z[21]=n<T>(3,4)*z[21];
    z[22]=npow(z[2],2);
    z[20]= - 3*z[20] + n<T>(3,2)*z[22] - z[21] - 5*z[13] - n<T>(1,6)*z[8];
    z[23]=n<T>(1,4)*z[6];
    z[24]=n<T>(3,4)*z[19] - z[23] + z[2];
    z[24]=n<T>(3,2)*z[24] + n<T>(1,3)*z[4];
    z[24]=z[4]*z[24];
    z[20]=n<T>(1,2)*z[20] + z[24];
    z[20]=z[4]*z[20];
    z[18]= - n<T>(3,4)*z[4] - n<T>(1,2)*z[19] + z[18];
    z[18]=z[4]*z[18];
    z[18]=z[22] + z[18];
    z[24]= - z[2] + z[5] + n<T>(3,2)*z[6];
    z[24]=z[24]*z[19];
    z[21]=z[21] + z[7];
    z[18]=z[24] - n<T>(5,4)*z[8] + z[21] + 3*z[18];
    z[24]=z[4] - z[6];
    z[19]=n<T>(1,3)*z[3] - z[19] + n<T>(3,8)*z[24];
    z[19]=z[3]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[3]*z[18];
    z[19]= - z[5] - n<T>(1,2)*z[2];
    z[19]=z[2]*z[19];
    z[19]= - n<T>(1,2)*z[21] + z[19];
    z[19]=z[1]*z[19];
    z[21]=9*z[11] - n<T>(1,3)*z[9];
    z[19]=n<T>(1,4)*z[21] + z[19];
    z[19]=i*z[19];
    z[21]=z[14] - z[16];
    z[23]=z[23] + n<T>(1,2)*z[5];
    z[23]=z[8]*z[23];
    z[22]= - n<T>(11,6)*z[22] - n<T>(1,3)*z[8] - n<T>(5,2)*z[13] - z[7];
    z[22]=z[2]*z[22];

    r += n<T>(11,8)*z[10] - n<T>(27,16)*z[12] + n<T>(17,8)*z[15] + z[17] + z[18]
     +  z[19] + z[20] + n<T>(3,8)*z[21] + z[22] + z[23];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1148(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1148(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
