#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf185(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[57];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[16];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=d[6];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[4];
    z[16]=e[8];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[19];
    z[21]=c[23];
    z[22]=c[25];
    z[23]=c[26];
    z[24]=c[28];
    z[25]=c[31];
    z[26]=e[0];
    z[27]=e[3];
    z[28]=e[10];
    z[29]=e[7];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[17];
    z[39]=f[18];
    z[40]=f[31];
    z[41]=f[36];
    z[42]=f[39];
    z[43]=z[1]*i;
    z[44]= - z[43] + n<T>(1,2)*z[4];
    z[44]=z[44]*z[4];
    z[45]=3*z[43] + z[4];
    z[45]=n<T>(1,12)*z[10] + n<T>(1,4)*z[45] - z[5];
    z[45]=z[10]*z[45];
    z[45]= - n<T>(15,2)*z[44] + z[45];
    z[45]=z[10]*z[45];
    z[46]=n<T>(3,2)*z[6];
    z[47]= - 5*z[4] - z[46];
    z[47]=z[43]*z[47];
    z[47]= - 5*z[17] + z[47];
    z[47]=z[8]*z[47];
    z[48]=z[43] - z[4];
    z[46]=5*z[48] - z[46];
    z[46]=z[14]*z[46];
    z[49]= - 23*z[2] + n<T>(83,2)*z[10] - 11*z[4] - 37*z[5];
    z[49]=n<T>(1,3)*z[49] + n<T>(5,2)*z[3];
    z[49]=n<T>(1,3)*z[11] + n<T>(19,12)*z[7] + n<T>(1,4)*z[49] + z[6];
    z[49]=z[17]*z[49];
    z[50]=n<T>(17,2)*z[2] - n<T>(35,2)*z[43] + 9*z[5];
    z[50]=z[16]*z[50];
    z[51]=z[3]*z[43];
    z[51]=z[51] + z[17];
    z[51]=z[9]*z[51];
    z[52]=z[43] - z[3];
    z[52]=z[15]*z[52];
    z[45]=z[52] + z[42] + z[41] + z[40] + z[47] + z[46] + z[50] + z[51]
    - z[30] + z[45] + z[49];
    z[46]=n<T>(3,2)*z[4];
    z[47]=z[43] + z[46];
    z[46]=z[47]*z[46];
    z[47]=n<T>(1,2)*z[2];
    z[49]=z[43]*z[47];
    z[50]=3*z[4];
    z[51]= - n<T>(7,6)*z[3] - z[2] + n<T>(13,2)*z[5] - z[43] - z[50];
    z[51]=z[3]*z[51];
    z[52]=npow(z[10],2);
    z[53]=n<T>(1,4)*z[52];
    z[54]=n<T>(13,8)*z[5] - 2*z[43] - n<T>(9,2)*z[4];
    z[54]=z[5]*z[54];
    z[46]=n<T>(1,4)*z[51] + z[49] + z[53] + z[46] + z[54];
    z[46]=z[3]*z[46];
    z[49]= - z[4] + n<T>(7,4)*z[43];
    z[51]=z[49]*z[50];
    z[50]= - 4*z[5] + n<T>(35,4)*z[43] + z[50];
    z[50]=z[5]*z[50];
    z[54]=n<T>(3,4)*z[4];
    z[55]= - n<T>(5,4)*z[5] - 5*z[43] - z[54];
    z[55]=3*z[55] + 7*z[2];
    z[47]=z[55]*z[47];
    z[47]=z[47] - z[53] + z[51] + z[50];
    z[47]=z[2]*z[47];
    z[50]=z[4]*z[48];
    z[51]=z[43] + z[4];
    z[51]=n<T>(3,4)*z[51] - z[10];
    z[51]=n<T>(1,2)*z[51] - n<T>(5,3)*z[6];
    z[51]=z[6]*z[51];
    z[50]=z[51] - n<T>(5,2)*z[50] + z[52];
    z[50]=z[6]*z[50];
    z[51]= - z[2] + z[3];
    z[51]=z[26]*z[51];
    z[52]= - 3*z[10] - z[6];
    z[52]=z[27]*z[52];
    z[51]=z[31] + z[51] + z[52];
    z[52]= - 9*z[43] + z[4];
    z[52]=z[52]*z[54];
    z[53]= - z[43] + n<T>(27,8)*z[4];
    z[53]=z[5]*z[53];
    z[52]=z[52] + z[53];
    z[52]=z[5]*z[52];
    z[53]= - 3*z[48] - z[10];
    z[53]=z[10]*z[53];
    z[44]= - z[44] + z[53];
    z[53]=n<T>(1,4)*z[6] + z[48];
    z[53]=z[6]*z[53];
    z[44]=n<T>(1,2)*z[44] + z[53];
    z[48]=7*z[48] - 5*z[10];
    z[48]=n<T>(1,4)*z[48] + 3*z[6];
    z[48]=z[7]*z[48];
    z[44]=n<T>(5,2)*z[44] + z[48];
    z[44]=z[7]*z[44];
    z[48]=n<T>(1,2)*z[11];
    z[53]=z[43] - z[5];
    z[54]= - z[11] + z[10] - z[53];
    z[54]=z[54]*z[48];
    z[43]= - z[43] + n<T>(1,2)*z[5];
    z[43]=z[43]*z[5];
    z[54]= - z[43] + z[54];
    z[54]=z[11]*z[54];
    z[48]= - z[48] - z[53];
    z[48]=z[11]*z[48];
    z[43]=n<T>(1,3)*z[17] - z[43] + z[48];
    z[43]=z[12]*z[43];
    z[48]=z[34] - z[22];
    z[53]=14*z[23] + n<T>(15,2)*z[21] - n<T>(1,24)*z[18];
    z[53]=i*z[53];
    z[49]=n<T>(3,4)*z[2] - z[49];
    z[49]=z[13]*z[49];
    z[55]= - n<T>(7,4)*z[6] - 3*z[7];
    z[55]=z[28]*z[55];
    z[56]= - z[11] - z[12];
    z[56]=z[29]*z[56];

    r += n<T>(11,8)*z[19] + n<T>(7,8)*z[20] - 7*z[24] - n<T>(295,48)*z[25] - n<T>(3,4)*
      z[32] - n<T>(7,4)*z[33] + n<T>(27,8)*z[35] - n<T>(1,8)*z[36] - n<T>(5,8)*z[37]
     +  z[38] - n<T>(1,6)*z[39] + z[43] + z[44] + n<T>(1,2)*z[45] + z[46] + z[47]
       + n<T>(15,4)*z[48] + 3*z[49] + z[50] + n<T>(1,4)*z[51] + z[52] + z[53]
       + z[54] + z[55] + z[56];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf185(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf185(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
