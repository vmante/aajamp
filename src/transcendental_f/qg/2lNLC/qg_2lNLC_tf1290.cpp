#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1290(
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[4];
  std::complex<T> r(0,0);

    z[1]=e[7];
    z[2]=e[13];

    r += z[1] - z[2];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1290(
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1290(
  const std::array<std::complex<dd_real>,24>& e
);
#endif
