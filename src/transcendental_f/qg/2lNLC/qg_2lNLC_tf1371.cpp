#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1371(
  const std::array<T, 85>& c
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[1];
  std::complex<T> r(0,0);

    z[1]=c[3];

    r +=  - z[1];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1371(
  const std::array<double,85>& c
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1371(
  const std::array<dd_real,85>& c
);
#endif
