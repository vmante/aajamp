#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf73(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[74];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[16];
    z[13]=d[9];
    z[14]=d[17];
    z[15]=d[11];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=c[3];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[17];
    z[25]=c[19];
    z[26]=c[21];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[0];
    z[33]=e[3];
    z[34]=e[10];
    z[35]=e[6];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=e[12];
    z[39]=f[0];
    z[40]=f[1];
    z[41]=f[3];
    z[42]=f[4];
    z[43]=f[5];
    z[44]=f[6];
    z[45]=f[11];
    z[46]=f[12];
    z[47]=f[14];
    z[48]=f[16];
    z[49]=f[17];
    z[50]=f[18];
    z[51]=f[31];
    z[52]=f[36];
    z[53]=f[39];
    z[54]=f[51];
    z[55]=f[55];
    z[56]=f[58];
    z[57]=f[107];
    z[58]=n<T>(19,8)*z[3];
    z[59]= - z[7] - z[6];
    z[59]= - n<T>(3119,18)*z[2] + n<T>(19,8)*z[9] + z[58] + 2*z[59] + n<T>(145,8)*
    z[4];
    z[59]=z[2]*z[59];
    z[60]=z[13] + n<T>(27,4)*z[7];
    z[60]=z[60]*z[7];
    z[61]=n<T>(25,4)*z[18] - n<T>(19093,72)*z[16] + n<T>(5011,36)*z[17] + 19*z[20]
    - n<T>(27895,72)*z[19];
    z[62]= - z[13]*z[15];
    z[63]=2*z[7];
    z[64]=z[63] + 7*z[6];
    z[64]=z[6]*z[64];
    z[65]=25*z[12] - n<T>(81,2)*z[4];
    z[65]=z[4]*z[65];
    z[66]= - 8447*z[10] + n<T>(8951,2)*z[3];
    z[66]=z[3]*z[66];
    z[67]=z[12] - z[4];
    z[67]=2*z[67] + n<T>(505,32)*z[11];
    z[67]=z[11]*z[67];
    z[68]=n<T>(1357,72)*z[9] - n<T>(4627,48)*z[11] + 2*z[12] + n<T>(2231,36)*z[3];
    z[68]=z[9]*z[68];
    z[69]=n<T>(18049,144)*z[2] + n<T>(5533,144)*z[9] + n<T>(4747,48)*z[11] - n<T>(2357,36)*z[3]
     + n<T>(51,2)*z[4] - n<T>(5011,72)*z[10]
     + z[6];
    z[69]=z[5]*z[69];
    z[70]=19*z[14];
    z[71]= - z[70] + 27*z[7];
    z[71]= - n<T>(207,8)*z[8] - n<T>(22189,144)*z[5] + n<T>(27967,144)*z[2] - n<T>(17,8)
   *z[9] + n<T>(207,4)*z[11] - n<T>(27,8)*z[3] - n<T>(301,8)*z[4] + n<T>(1,2)*z[71]
     - 
    z[6];
    z[71]=z[8]*z[71];
    z[59]=z[71] + z[69] + z[59] + z[68] + z[67] + n<T>(1,144)*z[66] + n<T>(1,8)*
    z[65] + z[64] - z[60] + n<T>(1,2)*z[61] + z[62];
    z[59]=z[1]*z[59];
    z[61]=n<T>(11639,2)*z[29] + 3083*z[27];
    z[59]=z[59] - n<T>(6847,864)*z[22] - n<T>(11,4)*z[24] + n<T>(1,18)*z[61] + 3*
    z[26];
    z[59]=i*z[59];
    z[61]=n<T>(1,2)*z[6];
    z[62]=n<T>(53,72)*z[8] + n<T>(22189,288)*z[5] - n<T>(809,9)*z[2] - n<T>(207,8)*z[11]
    + n<T>(3,2)*z[3] + n<T>(3505,96)*z[4] - z[63] + z[61];
    z[62]=z[8]*z[62];
    z[64]=npow(z[6],2);
    z[65]=npow(z[4],2);
    z[66]= - z[20] + n<T>(97,9)*z[19];
    z[66]=19*z[66] - n<T>(29899,432)*z[21];
    z[67]=npow(z[3],2);
    z[68]= - z[6] + n<T>(219,8)*z[11];
    z[68]=z[11]*z[68];
    z[69]= - 27*z[3] - 11*z[9];
    z[69]=z[9]*z[69];
    z[71]= - n<T>(12269,288)*z[2] + n<T>(7,8)*z[9] - z[58] + 2*z[6];
    z[71]=z[2]*z[71];
    z[72]= - z[6] - n<T>(158,3)*z[4];
    z[72]=n<T>(2489,144)*z[5] + n<T>(5095,72)*z[2] + n<T>(5,4)*z[9] + 2*z[72] + n<T>(11,4)*z[3];
    z[72]=z[5]*z[72];
    z[60]=z[62] + z[72] + z[71] + n<T>(1,8)*z[69] + z[68] + n<T>(3,2)*z[67] + 
   n<T>(1119,32)*z[65] - n<T>(25,4)*z[64] + n<T>(1,2)*z[66] + z[60];
    z[60]=z[8]*z[60];
    z[62]=2*z[18];
    z[66]=npow(z[11],2);
    z[67]= - z[7]*z[13];
    z[68]=2*z[13];
    z[69]= - z[68] + n<T>(5675,288)*z[3];
    z[69]=z[3]*z[69];
    z[68]= - n<T>(125,4)*z[11] + n<T>(3571,72)*z[3] + z[68] + n<T>(3,4)*z[4];
    z[68]=z[9]*z[68];
    z[67]=z[68] - n<T>(781,24)*z[66] + z[69] - 2*z[65] - n<T>(1,4)*z[64] + z[67]
    + n<T>(15505,864)*z[21] - z[62] - z[37] - n<T>(3661,72)*z[34];
    z[67]=z[9]*z[67];
    z[68]=2*z[4];
    z[58]=z[68] + z[58];
    z[58]=z[9]*z[58];
    z[69]= - n<T>(15083,54)*z[21] + n<T>(8381,9)*z[16] - 49*z[32] + n<T>(13151,9)*
    z[19];
    z[71]= - z[6]*z[63];
    z[63]=z[63] - n<T>(145,16)*z[4];
    z[63]=z[4]*z[63];
    z[72]= - z[6] + 3*z[11];
    z[73]=n<T>(11711,72)*z[2] + z[72];
    z[73]=z[2]*z[73];
    z[58]=n<T>(1,2)*z[73] + z[58] - n<T>(65,16)*z[66] + z[63] + n<T>(1,16)*z[69]
     + 
    z[71];
    z[58]=z[2]*z[58];
    z[63]= - n<T>(2117,144)*z[11] + 77*z[3] + n<T>(73,8)*z[4] + 29*z[7] + z[6];
    z[63]=z[11]*z[63];
    z[63]=z[63] - 31*z[35] - n<T>(791,8)*z[33];
    z[66]=npow(z[7],2);
    z[69]= - z[13] + n<T>(25,4)*z[6];
    z[69]=z[6]*z[69];
    z[71]= - z[7] + 22*z[3];
    z[71]=z[3]*z[71];
    z[62]=z[71] + z[69] - n<T>(27,2)*z[66] + n<T>(43489,576)*z[21] - z[62] + n<T>(1,2)*z[63];
    z[62]=z[11]*z[62];
    z[63]= - n<T>(2195,3)*z[3] + n<T>(4627,4)*z[11];
    z[63]=n<T>(1,4)*z[63] - n<T>(164,3)*z[9];
    z[63]=z[9]*z[63];
    z[69]= - n<T>(10073,48)*z[2] - n<T>(4957,96)*z[9] - n<T>(4603,32)*z[11] + 158*
    z[4] + n<T>(9383,48)*z[3];
    z[69]=z[5]*z[69];
    z[63]=z[63] + z[69];
    z[69]= - n<T>(10871,48)*z[21] - n<T>(5011,4)*z[17] + 1339*z[16];
    z[69]=n<T>(1,9)*z[69] - n<T>(51,2)*z[65];
    z[68]=z[68] + n<T>(7799,288)*z[3];
    z[68]=z[3]*z[68];
    z[71]=z[6] - n<T>(1397,32)*z[11];
    z[71]=z[11]*z[71];
    z[72]= - n<T>(8093,288)*z[2] - n<T>(21,4)*z[9] - z[72];
    z[72]=z[2]*z[72];
    z[63]=z[72] + z[71] + n<T>(1,2)*z[69] + z[68] + n<T>(1,3)*z[63];
    z[63]=z[5]*z[63];
    z[68]=n<T>(367,9)*z[21] - n<T>(8447,9)*z[17] - n<T>(3569,9)*z[34] - 1081*z[33];
    z[69]= - z[4] - n<T>(11003,144)*z[3];
    z[69]=z[3]*z[69];
    z[65]=z[69] + n<T>(1,16)*z[68] - z[65];
    z[65]=z[3]*z[65];
    z[68]=z[13] + z[6];
    z[61]=z[68]*z[61];
    z[61]=z[61] + z[36] + z[66];
    z[61]=z[6]*z[61];
    z[68]=n<T>(4825,72)*z[21] + n<T>(49,2)*z[32] - 25*z[18];
    z[64]=n<T>(1,2)*z[68] - 3*z[64];
    z[68]= - n<T>(359,96)*z[4] + n<T>(1,2)*z[13] - z[7];
    z[68]=z[4]*z[68];
    z[64]=n<T>(1,4)*z[64] + z[68];
    z[64]=z[4]*z[64];
    z[68]=z[55] + z[56];
    z[69]=z[51] - z[53];
    z[71]=z[44] + z[52];
    z[70]=n<T>(25,4)*z[12] - z[70] - n<T>(5011,36)*z[10];
    z[70]=z[21]*z[70];
    z[72]=npow(z[13],2);
    z[72]= - n<T>(1,3)*z[72] + n<T>(13,48)*z[21] + z[38] + z[36];
    z[72]=z[13]*z[72];
    z[66]=n<T>(101,8)*z[21] - z[66];
    z[66]=z[7]*z[66];

    r += n<T>(18559,288)*z[23] + n<T>(8029,432)*z[25] - n<T>(3137,36)*z[28] - n<T>(11747,72)*z[30]
     - n<T>(121345,576)*z[31]
     - n<T>(89,8)*z[39]
     + n<T>(65,16)*z[40] - 
      n<T>(47,4)*z[41] - n<T>(355,18)*z[42] + n<T>(3191,36)*z[43] + n<T>(22477,288)*
      z[45] + n<T>(1421,32)*z[46] - n<T>(5,2)*z[47] - n<T>(531,32)*z[48] + n<T>(301,16)
      *z[49] - n<T>(89,24)*z[50] - n<T>(3,4)*z[54] + z[57] + z[58] + z[59] + 
      z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + n<T>(5,6)*z[66] + 
      z[67] - n<T>(1,4)*z[68] - n<T>(25,4)*z[69] + n<T>(1,2)*z[70] - n<T>(3,2)*z[71]
     +  z[72];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf73(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf73(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
