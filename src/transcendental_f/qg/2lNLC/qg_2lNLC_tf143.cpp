#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf143(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[71];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[11];
    z[11]=d[16];
    z[12]=d[6];
    z[13]=d[5];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[0];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[39];
    z[50]=f[51];
    z[51]=f[55];
    z[52]=f[58];
    z[53]=n<T>(829,12)*z[9];
    z[54]=2*z[15];
    z[55]=n<T>(155,9)*z[7] - n<T>(21,2)*z[2] - z[53] + z[54] + n<T>(677,9)*z[3];
    z[55]=z[7]*z[55];
    z[56]=n<T>(769,18)*z[8];
    z[53]=n<T>(643,36)*z[7] + n<T>(2875,36)*z[2] + z[53] - n<T>(461,9)*z[3] - z[56]
    + 34*z[4];
    z[53]=z[5]*z[53];
    z[57]=n<T>(21,2)*z[18];
    z[58]=npow(z[12],2);
    z[59]=npow(z[13],2);
    z[60]=3*z[20];
    z[61]=z[60] - 2*z[21];
    z[62]= - z[10]*z[54];
    z[63]=4*z[10];
    z[64]=n<T>(45,4)*z[4] - n<T>(21,2)*z[11] - z[63];
    z[64]=z[4]*z[64];
    z[65]= - 1565*z[8] + n<T>(1709,2)*z[3];
    z[65]=z[3]*z[65];
    z[66]=z[4] - z[11];
    z[66]=z[12] - 4*z[66];
    z[66]=2*z[66] + n<T>(31,8)*z[9];
    z[66]=z[9]*z[66];
    z[67]= - 5*z[4] - 29*z[3];
    z[67]=n<T>(1,2)*z[67] - n<T>(1054,9)*z[2];
    z[67]=z[2]*z[67];
    z[53]=z[53] + z[55] + z[67] + z[66] + n<T>(1,36)*z[65] + z[64] - z[58]
    + 5*z[59] + z[62] - n<T>(2623,36)*z[16] + n<T>(769,18)*z[17] - z[57] + 2*
    z[61] - n<T>(4981,36)*z[19];
    z[55]=2*z[14];
    z[61]= - n<T>(9,2)*z[3] - z[55] - n<T>(21,2)*z[4];
    z[61]=n<T>(1,11)*z[61] + z[9];
    z[61]= - n<T>(5,2)*z[6] - n<T>(3847,396)*z[5] - n<T>(9,22)*z[7] + 3*z[61] + n<T>(6637,396)*z[2];
    z[61]=z[6]*z[61];
    z[53]=n<T>(1,11)*z[53] + z[61];
    z[53]=z[1]*z[53];
    z[61]= - n<T>(139,24)*z[23] + 1901*z[28] + 928*z[26];
    z[53]=n<T>(1,99)*z[61] + z[53];
    z[53]=i*z[53];
    z[61]=npow(z[9],2);
    z[62]=npow(z[3],2);
    z[64]=npow(z[4],2);
    z[65]=5*z[64] + n<T>(2251,54)*z[22] + n<T>(1271,9)*z[16] + 37*z[31] + n<T>(2621,9)*z[19];
    z[66]= - 14*z[12] + n<T>(1997,36)*z[2];
    z[66]=z[2]*z[66];
    z[65]=z[66] + n<T>(5,4)*z[61] + n<T>(1,4)*z[65] - 2*z[62];
    z[65]=z[2]*z[65];
    z[66]=npow(z[15],2);
    z[67]=z[36] + z[35];
    z[68]=2*z[13];
    z[69]=z[15] + z[68];
    z[69]=z[13]*z[69];
    z[67]=z[69] + 2*z[67] - z[66];
    z[67]=z[67]*z[68];
    z[68]=z[15] - n<T>(3,8)*z[4];
    z[68]=z[4]*z[68];
    z[57]=z[68] + n<T>(2455,144)*z[22] - 2*z[59] + z[57] - n<T>(37,4)*z[31] + 4*
    z[21];
    z[57]=z[4]*z[57];
    z[68]=n<T>(205,72)*z[9] + 24*z[3] + z[12] - n<T>(5,4)*z[4];
    z[68]=z[9]*z[68];
    z[68]=z[68] + n<T>(39,2)*z[62] + n<T>(7759,144)*z[22] - z[59] - 8*z[18] + 2*
    z[34] - n<T>(137,4)*z[32];
    z[68]=z[9]*z[68];
    z[69]= - n<T>(1,3)*z[66] + 2*z[36] + z[21];
    z[54]=z[69]*z[54];
    z[69]=z[50] + z[52];
    z[70]=z[27] - z[42];
    z[54]=z[57] + z[67] + z[54] + n<T>(3625,72)*z[24] + n<T>(793,108)*z[25] - 
   n<T>(1901,18)*z[29] - n<T>(29695,144)*z[30] + n<T>(5,2)*z[38] - n<T>(5,4)*z[39]
     - 17
   *z[40] - n<T>(391,18)*z[41] + n<T>(3847,72)*z[43] + n<T>(171,8)*z[44] - n<T>(177,8)*
    z[45] + n<T>(63,4)*z[46] + n<T>(5,6)*z[47] - 3*z[48] - z[49] + z[51] - n<T>(464,9)*z[70]
     - 2*z[69]
     + z[68]
     + z[65];
    z[57]=n<T>(2,11)*z[59];
    z[65]=z[4]*z[15];
    z[67]=z[2]*z[3];
    z[68]= - n<T>(61,2)*z[9] - 3*z[15] + n<T>(967,18)*z[3];
    z[68]=z[7]*z[68];
    z[65]=n<T>(1,11)*z[68] - n<T>(21,22)*z[67] - n<T>(157,66)*z[61] + n<T>(1997,792)*
    z[62] - n<T>(2,11)*z[65] + n<T>(2587,2376)*z[22] - z[57] + n<T>(4,11)*z[66] + n<T>(6,11)*z[37]
     - n<T>(83,18)*z[33];
    z[65]=z[7]*z[65];
    z[66]= - n<T>(1301,24)*z[22] - n<T>(769,2)*z[17] + 338*z[16];
    z[67]=4*z[3] - n<T>(767,72)*z[2];
    z[67]=z[2]*z[67];
    z[68]=n<T>(829,4)*z[9];
    z[69]= - n<T>(461,3)*z[3] + z[68];
    z[69]= - n<T>(391,18)*z[7] + n<T>(1,3)*z[69] + 3*z[2];
    z[69]=z[7]*z[69];
    z[68]= - n<T>(643,12)*z[7] - n<T>(1379,6)*z[2] - z[68] + 163*z[4] + n<T>(1565,6)
   *z[3];
    z[68]=z[5]*z[68];
    z[61]=n<T>(1,6)*z[68] + z[69] + z[67] - n<T>(171,8)*z[61] + n<T>(1565,72)*z[62]
    + n<T>(1,9)*z[66] - 17*z[64];
    z[62]=n<T>(1,11)*z[5];
    z[61]=z[61]*z[62];
    z[60]= - z[60] + n<T>(295,9)*z[19];
    z[66]= - 2*z[12] + n<T>(35,2)*z[9];
    z[66]=z[9]*z[66];
    z[58]=z[66] + n<T>(161,8)*z[64] + z[58] - n<T>(5473,216)*z[22] + 2*z[60] - 3
   *z[59];
    z[59]=3*z[7];
    z[60]=n<T>(401,36)*z[5] - z[59] + n<T>(325,18)*z[2] - n<T>(163,3)*z[4] + 3*z[3];
    z[60]=z[60]*z[62];
    z[62]=4*z[12] + n<T>(3,2)*z[3];
    z[62]=n<T>(7,11)*z[62] - n<T>(253,72)*z[2];
    z[62]=z[2]*z[62];
    z[59]=z[59] - 9*z[3] + 5*z[2];
    z[59]=z[7]*z[59];
    z[64]=n<T>(41,198)*z[6] + n<T>(4351,792)*z[5] - n<T>(563,99)*z[2] - n<T>(3,2)*z[9]
    - z[12] + n<T>(547,264)*z[4];
    z[64]=z[6]*z[64];
    z[58]=z[64] + z[60] + n<T>(3,22)*z[59] + n<T>(1,11)*z[58] + z[62];
    z[58]=z[6]*z[58];
    z[55]= - z[55] - n<T>(7,2)*z[11];
    z[55]= - z[56] + 3*z[55] - z[63];
    z[55]=n<T>(8,33)*z[13] + n<T>(1,11)*z[55] - n<T>(1,12)*z[15];
    z[55]=z[22]*z[55];
    z[56]= - z[13] - n<T>(1,3)*z[12];
    z[56]=z[12]*z[56];
    z[56]=n<T>(2,11)*z[56] + n<T>(25,44)*z[22] + z[57] + n<T>(4,11)*z[35] + z[15];
    z[56]=z[12]*z[56];
    z[57]= - n<T>(611,99)*z[22] - n<T>(1565,99)*z[17] - n<T>(121,9)*z[33] - n<T>(211,11)
   *z[32];
    z[59]= - 1 - n<T>(2369,396)*z[3];
    z[59]=z[3]*z[59];
    z[57]=n<T>(1,4)*z[57] + z[59];
    z[57]=z[3]*z[57];

    r += z[53] + n<T>(1,11)*z[54] + z[55] + z[56] + z[57] + z[58] + z[61]
       + z[65];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf143(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf143(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
