#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf595(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[87];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[79];
    z[3]=c[11];
    z[4]=d[1];
    z[5]=d[2];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=c[12];
    z[13]=c[13];
    z[14]=c[20];
    z[15]=c[23];
    z[16]=d[0];
    z[17]=f[24];
    z[18]=f[42];
    z[19]=f[44];
    z[20]=f[61];
    z[21]=f[69];
    z[22]=f[70];
    z[23]=f[77];
    z[24]=f[80];
    z[25]=f[22];
    z[26]=f[62];
    z[27]=d[3];
    z[28]=g[24];
    z[29]=g[27];
    z[30]=g[38];
    z[31]=g[40];
    z[32]=g[48];
    z[33]=g[50];
    z[34]=g[79];
    z[35]=g[82];
    z[36]=g[90];
    z[37]=g[92];
    z[38]=g[99];
    z[39]=g[101];
    z[40]=g[128];
    z[41]=g[138];
    z[42]=g[140];
    z[43]=g[145];
    z[44]=g[147];
    z[45]=g[170];
    z[46]=g[173];
    z[47]=g[181];
    z[48]=g[183];
    z[49]=g[189];
    z[50]=g[191];
    z[51]=g[198];
    z[52]=g[200];
    z[53]=g[201];
    z[54]=g[212];
    z[55]=g[214];
    z[56]=g[220];
    z[57]=g[224];
    z[58]=g[243];
    z[59]=g[245];
    z[60]=g[251];
    z[61]=g[254];
    z[62]=g[270];
    z[63]=g[272];
    z[64]=g[276];
    z[65]=g[278];
    z[66]=g[281];
    z[67]=g[324];
    z[68]=g[355];
    z[69]=g[357];
    z[70]=g[358];
    z[71]= - z[14] + n<T>(1,20)*z[15];
    z[72]= - n<T>(3,5)*z[13] + n<T>(1,4)*z[71];
    z[73]= - 29*z[72] - n<T>(11,240)*z[3];
    z[73]=z[8]*z[73];
    z[74]= - 7*z[72] - n<T>(137,2160)*z[3];
    z[74]=z[7]*z[74];
    z[73]=z[73] + z[74];
    z[74]=n<T>(1,2)*z[10];
    z[72]=23*z[72] + n<T>(473,2160)*z[3];
    z[72]=z[72]*z[74];
    z[75]=n<T>(2,27)*z[5] - n<T>(1,18)*z[9];
    z[75]=z[3]*z[75];
    z[71]= - z[71] + n<T>(12,5)*z[13];
    z[71]=3*z[71];
    z[76]= - z[71] + n<T>(53,540)*z[3];
    z[76]=z[11]*z[76];
    z[77]=n<T>(1,4)*z[2];
    z[78]=z[1]*z[77];
    z[71]= - z[71] - n<T>(1,20)*z[3];
    z[71]=z[6]*z[71];
    z[79]= - n<T>(1,4)*z[15] + 5*z[14];
    z[79]= - n<T>(5,144)*z[3] + n<T>(1,4)*z[79] + 3*z[13];
    z[79]=z[4]*z[79];
    z[71]=n<T>(7,2)*z[79] + z[72] + z[71] + z[78] + z[76] + z[75] + n<T>(1,2)*
    z[73];
    z[71]=i*z[71];
    z[72]=21*z[21];
    z[73]= - z[72] + 13*z[25];
    z[73]=n<T>(7,2)*z[19] + n<T>(1,2)*z[73];
    z[75]=3*z[26];
    z[76]= - z[75] - z[73];
    z[78]=n<T>(3,4)*z[20];
    z[76]=z[77] + n<T>(7,4)*z[17] + z[78] - n<T>(7,6)*z[12] + n<T>(13,8)*z[18] + 
    z[22] + n<T>(1,4)*z[76] + z[23];
    z[76]=z[4]*z[76];
    z[77]=z[20] + z[18];
    z[72]= - z[72] + 19*z[19];
    z[79]=5*z[24] - z[72];
    z[79]= - n<T>(19,8)*z[17] + n<T>(7,4)*z[22] + n<T>(1,8)*z[79] + z[23] + z[77];
    z[79]=z[16]*z[79];
    z[80]= - z[17] - n<T>(3,2)*z[20] - n<T>(5,2)*z[26] - z[18] + z[24];
    z[80]=z[11]*z[80];
    z[81]=z[5] - z[16];
    z[81]=n<T>(1,2)*z[9] - z[27] - n<T>(3,4)*z[81];
    z[81]=z[2]*z[81];
    z[76]=z[52] - z[58] - z[64] + z[69] + z[79] + z[80] + z[81] + z[76];
    z[79]=7*z[26];
    z[73]=z[79] + z[73];
    z[80]=z[23] + n<T>(3,4)*z[2];
    z[73]= - n<T>(3,4)*z[17] + z[78] - n<T>(5,6)*z[12] - n<T>(5,8)*z[18] + n<T>(1,4)*
    z[73] - z[24] - z[80];
    z[73]=z[73]*z[74];
    z[74]=z[24] + z[25];
    z[74]= - 13*z[19] + n<T>(13,2)*z[74];
    z[75]=z[75] - z[74];
    z[78]=z[20] + z[22];
    z[75]=n<T>(7,16)*z[17] + n<T>(1,12)*z[12] + n<T>(13,16)*z[18] + n<T>(1,8)*z[75]
     + 
    z[80] + n<T>(3,8)*z[78];
    z[75]=z[7]*z[75];
    z[74]= - n<T>(13,2)*z[18] - 3*z[22] + z[79] + z[74];
    z[74]= - z[2] + n<T>(1,4)*z[17] - n<T>(7,2)*z[20] + n<T>(1,2)*z[74] - n<T>(31,3)*
    z[12];
    z[74]=z[8]*z[74];
    z[74]=z[74] + z[51] + z[61] + z[68];
    z[77]=z[24] - z[77];
    z[78]=n<T>(1,2)*z[27];
    z[77]=z[78]*z[77];
    z[78]=z[26] + z[17] - z[20];
    z[79]=n<T>(1,2)*z[22];
    z[78]=4*z[12] - z[79] - z[23] - n<T>(1,2)*z[78];
    z[78]=z[6]*z[78];
    z[80]=z[34] + z[37] - z[62] - z[65] + z[28] + z[29] + z[30] + z[31];
    z[81]= - z[40] - z[42] + z[46] + z[47];
    z[82]= - z[41] - z[55] + z[60] + z[63];
    z[83]=z[45] + z[48];
    z[84]=z[38] - z[66];
    z[85]=z[36] - z[49];
    z[79]=n<T>(3,2)*z[12] - z[79];
    z[79]=z[9]*z[79];
    z[86]= - z[23]*z[27];
    z[72]= - 13*z[24] + z[72];
    z[72]= - n<T>(3,4)*z[22] + n<T>(1,8)*z[72] + z[23];
    z[72]=n<T>(19,16)*z[17] + n<T>(1,2)*z[72] - 2*z[12];
    z[72]=z[5]*z[72];

    r +=  - n<T>(13,24)*z[32] - n<T>(1,24)*z[33] + n<T>(19,8)*z[35] + n<T>(19,24)*z[39]
       + n<T>(1,12)*z[43] + n<T>(7,12)*z[44] - n<T>(13,12)*z[50] + z[53] - z[54]
     +  z[56] + n<T>(1,3)*z[57] - n<T>(13,4)*z[59] + 2*z[67] + z[70] + z[71] + 
      z[72] + z[73] + n<T>(1,4)*z[74] + z[75] + n<T>(1,2)*z[76] + z[77] + z[78]
       + z[79] + n<T>(13,8)*z[80] - n<T>(3,2)*z[81] + n<T>(3,4)*z[82] - n<T>(15,8)*
      z[83] - n<T>(5,24)*z[84] - n<T>(7,8)*z[85] + z[86];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf595(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf595(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
