#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1441(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[68];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=e[10];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[18];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[29];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=f[0];
    z[30]=f[1];
    z[31]=f[2];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[6];
    z[36]=f[7];
    z[37]=f[8];
    z[38]=f[9];
    z[39]=f[10];
    z[40]=f[11];
    z[41]=f[12];
    z[42]=f[13];
    z[43]=f[14];
    z[44]=f[15];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[23];
    z[49]=f[28];
    z[50]=f[29];
    z[51]=17*z[5];
    z[52]=7*z[3];
    z[53]=z[1]*i;
    z[54]= - 65*z[2] - z[52] + z[51] + 209*z[53];
    z[54]=z[2]*z[54];
    z[55]=2*z[5];
    z[56]= - z[53]*z[55];
    z[57]=npow(z[5],2);
    z[56]=z[57] + z[56];
    z[58]=11*z[5];
    z[59]= - n<T>(79,4)*z[3] + z[58] - n<T>(13,2)*z[53];
    z[60]=n<T>(1,4)*z[3];
    z[59]=z[59]*z[60];
    z[54]=n<T>(1,16)*z[54] + 4*z[56] + z[59];
    z[54]=z[2]*z[54];
    z[56]=z[53]*z[5];
    z[59]= - z[56] + n<T>(1,2)*z[57];
    z[58]=n<T>(71,8)*z[3] - z[58] + n<T>(59,4)*z[53];
    z[58]=z[3]*z[58];
    z[58]=n<T>(31,8)*z[59] + z[58];
    z[61]=z[53] + z[3];
    z[62]= - n<T>(11,16)*z[7] + n<T>(13,4)*z[2] + z[55] - n<T>(43,8)*z[61];
    z[62]=z[7]*z[62];
    z[63]=z[7] - z[2];
    z[64]=z[53] - z[5];
    z[65]= - n<T>(31,2)*z[64] + z[3] - 11*z[63];
    z[65]=n<T>(1,4)*z[65] + n<T>(19,3)*z[8];
    z[65]=z[8]*z[65];
    z[66]=z[64] + z[3];
    z[67]=2*z[66] - n<T>(1,8)*z[2];
    z[67]=z[2]*z[67];
    z[58]=n<T>(1,4)*z[65] + z[62] + n<T>(1,2)*z[58] + z[67];
    z[58]=z[8]*z[58];
    z[62]=z[63] - z[64];
    z[62]=n<T>(35,4)*z[8] + 4*z[3] + n<T>(19,4)*z[62];
    z[62]=z[16]*z[62];
    z[54]=z[62] + z[54] + z[58];
    z[51]=n<T>(11,2)*z[3] - z[51] - z[53];
    z[51]=z[3]*z[51];
    z[51]= - n<T>(23,2)*z[59] + z[51];
    z[58]=z[2]*z[66];
    z[51]=n<T>(1,6)*z[51] + z[58];
    z[52]=n<T>(23,8)*z[64] + z[52];
    z[58]=z[4] - z[2];
    z[52]= - n<T>(7,3)*z[6] - n<T>(11,8)*z[8] + n<T>(1,3)*z[52] + n<T>(3,2)*z[58];
    z[58]=n<T>(1,2)*z[6];
    z[52]=z[52]*z[58];
    z[62]=z[7]*z[61];
    z[63]= - n<T>(11,16)*z[8] - n<T>(19,8)*z[64] - z[3];
    z[63]=z[8]*z[63];
    z[65]= - z[53] + n<T>(1,2)*z[5];
    z[66]=n<T>(1,4)*z[4] - z[7] + z[65];
    z[66]=z[4]*z[66];
    z[51]=z[52] + z[66] + z[63] + n<T>(1,2)*z[51] + z[62];
    z[51]=z[51]*z[58];
    z[52]= - n<T>(181,3)*z[3] + 61*z[5] - 67*z[53];
    z[52]=z[52]*z[60];
    z[52]=z[52] - z[57] - n<T>(17,2)*z[56];
    z[52]=z[3]*z[52];
    z[56]=z[57]*z[53];
    z[57]=npow(z[5],3);
    z[56]= - n<T>(1,3)*z[57] + z[56];
    z[57]=z[5] + z[8] - z[3];
    z[57]= - n<T>(55,6)*z[7] - n<T>(37,6)*z[2] + n<T>(55,3)*z[53] - 3*z[57];
    z[57]=z[15]*z[57];
    z[52]=z[57] + n<T>(1,2)*z[56] + n<T>(1,3)*z[52];
    z[55]=n<T>(43,16)*z[3] - z[55] + n<T>(1,8)*z[53];
    z[55]=z[3]*z[55];
    z[56]=n<T>(1,2)*z[3];
    z[57]= - z[5] - n<T>(7,8)*z[53];
    z[57]= - n<T>(7,8)*z[2] + 5*z[57] + z[56];
    z[57]=z[2]*z[57];
    z[55]=z[57] - n<T>(1,16)*z[59] + z[55];
    z[57]=n<T>(1,3)*z[7] + n<T>(65,6)*z[2] + n<T>(11,6)*z[3] - n<T>(1,12)*z[5] - z[53];
    z[57]=z[7]*z[57];
    z[55]=n<T>(1,3)*z[55] + n<T>(1,8)*z[57];
    z[55]=z[7]*z[55];
    z[57]=z[3]*z[64];
    z[58]=n<T>(23,2)*z[5] - 79*z[53];
    z[58]= - n<T>(17,12)*z[2] + n<T>(1,3)*z[58] - z[3];
    z[58]=z[2]*z[58];
    z[57]=z[58] - n<T>(127,12)*z[59] + z[57];
    z[58]=n<T>(43,32)*z[7];
    z[59]=27*z[5] + 43*z[53];
    z[59]= - z[58] + n<T>(1,16)*z[59] + z[2];
    z[59]=z[7]*z[59];
    z[56]=z[56] - n<T>(127,24)*z[5] - z[53];
    z[56]= - n<T>(1,12)*z[4] - z[58] + n<T>(1,4)*z[56] + n<T>(8,3)*z[2];
    z[56]=z[4]*z[56];
    z[58]=z[61] + z[7];
    z[58]= - z[2] + n<T>(1,2)*z[58];
    z[58]=z[8]*z[58];
    z[56]=z[56] + z[58] + n<T>(1,4)*z[57] + z[59];
    z[56]=z[4]*z[56];
    z[57]=z[6] + z[4];
    z[58]=z[53]*z[57];
    z[58]=z[17] + z[58];
    z[58]=z[10]*z[58];
    z[57]=z[53] - z[57];
    z[57]=z[14]*z[57];
    z[57]= - z[26] + z[58] + z[57];
    z[58]=z[3] + z[5];
    z[58]=z[53]*z[58];
    z[58]=z[17] + z[58];
    z[58]=z[9]*z[58];
    z[59]=z[3] - z[64];
    z[59]=z[13]*z[59];
    z[58]=z[58] + z[59];
    z[59]= - z[43] + z[50] - z[49];
    z[60]=z[38] - z[35];
    z[61]=z[47] + z[31];
    z[62]= - z[3] - z[6];
    z[62]=z[28]*z[62];
    z[62]=z[62] + z[42];
    z[63]= - n<T>(229,12)*z[24] - n<T>(209,48)*z[22] - n<T>(13,96)*z[18];
    z[63]=i*z[63];
    z[64]= - 67*z[2] + n<T>(107,2)*z[5] + 17*z[3];
    z[64]=n<T>(1,2)*z[64] + 7*z[7];
    z[64]=n<T>(91,6)*z[6] - n<T>(77,12)*z[4] + n<T>(1,3)*z[64] - 29*z[8];
    z[64]=z[17]*z[64];
    z[53]=145*z[53] - n<T>(121,2)*z[2];
    z[53]= - z[6] - n<T>(145,24)*z[4] + z[8] + n<T>(1,12)*z[53] - z[7];
    z[53]=z[11]*z[53];
    z[65]= - n<T>(1,2)*z[2] - z[65];
    z[65]=z[12]*z[65];

    r +=  - n<T>(29,24)*z[19] + n<T>(2,3)*z[20] + n<T>(91,144)*z[21] + n<T>(65,48)*
      z[23] + n<T>(187,24)*z[25] + n<T>(31,36)*z[27] - n<T>(23,4)*z[29] + n<T>(5,8)*
      z[30] - n<T>(103,96)*z[32] + n<T>(107,96)*z[33] - n<T>(55,24)*z[34] + n<T>(19,3)*
      z[36] - 3*z[37] + n<T>(11,12)*z[39] - n<T>(13,96)*z[40] + n<T>(23,96)*z[41]
       + n<T>(3,8)*z[44] - n<T>(7,32)*z[45] - n<T>(59,32)*z[46] - n<T>(19,24)*z[48]
     +  z[51] + n<T>(1,4)*z[52] + z[53] + n<T>(1,3)*z[54] + z[55] + z[56] + n<T>(1,2)
      *z[57] + n<T>(7,8)*z[58] + n<T>(1,8)*z[59] - n<T>(7,16)*z[60] - n<T>(7,12)*z[61]
       + n<T>(17,24)*z[62] + z[63] + n<T>(1,24)*z[64] + n<T>(29,12)*z[65];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1441(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1441(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
