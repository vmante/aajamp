#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf278(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[44];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[4];
    z[9]=e[1];
    z[10]=e[2];
    z[11]=e[8];
    z[12]=c[3];
    z[13]=d[2];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[19];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[31];
    z[22]=e[3];
    z[23]=e[10];
    z[24]=f[4];
    z[25]=f[5];
    z[26]=f[11];
    z[27]=f[12];
    z[28]=f[16];
    z[29]=npow(z[3],2);
    z[30]=n<T>(1,2)*z[29];
    z[31]=z[1]*i;
    z[32]=z[31]*z[3];
    z[33]=5*z[31];
    z[34]= - 4*z[3] - z[33];
    z[34]=2*z[34] + z[2];
    z[34]=z[2]*z[34];
    z[35]= - 5*z[3] + 8*z[2];
    z[35]=z[4]*z[35];
    z[34]=z[35] + z[34] - z[30] + 10*z[32];
    z[34]=z[4]*z[34];
    z[35]=2*z[3];
    z[36]= - z[31]*z[35];
    z[36]=z[29] + z[36];
    z[33]=z[35] + z[33];
    z[33]=2*z[33] - 5*z[2];
    z[33]=z[2]*z[33];
    z[33]=5*z[36] + z[33];
    z[33]=z[2]*z[33];
    z[33]=z[33] + z[34];
    z[30]=z[30] - z[32];
    z[34]=z[31] - z[3];
    z[36]=2*z[34];
    z[37]= - z[36] - n<T>(1,2)*z[5];
    z[37]=z[5]*z[37];
    z[38]= - 2*z[5] - z[34];
    z[38]=z[6]*z[38];
    z[37]=z[38] + z[37] + z[30];
    z[37]=z[6]*z[37];
    z[38]=3*z[34] + z[6];
    z[38]=z[6]*z[38];
    z[34]= - n<T>(1,6)*z[8] + z[6] - n<T>(1,2)*z[34] - z[5];
    z[34]=z[8]*z[34];
    z[30]=z[34] + 3*z[30] + z[38];
    z[30]=z[8]*z[30];
    z[34]=3*z[3] - z[4];
    z[34]=z[4]*z[34];
    z[38]= - z[4] + n<T>(1,3)*z[13];
    z[38]=z[13]*z[38];
    z[34]=n<T>(1,2)*z[38] - n<T>(1,12)*z[12] - n<T>(3,2)*z[29] + z[34];
    z[34]=z[13]*z[34];
    z[29]= - z[29] + z[32];
    z[32]= - z[3] - z[31];
    z[32]=n<T>(1,2)*z[32] + n<T>(5,3)*z[5];
    z[32]=z[5]*z[32];
    z[29]=2*z[29] + z[32];
    z[29]=z[5]*z[29];
    z[32]=z[35] + z[5];
    z[32]=z[31]*z[32];
    z[32]=2*z[12] + z[32];
    z[32]=z[7]*z[32];
    z[35]= - z[2] - z[3] + 2*z[31];
    z[35]=z[9]*z[35];
    z[35]= - z[35] + z[26] + z[25] - z[18];
    z[38]=z[28] - z[27];
    z[39]= - n<T>(20,3)*z[19] + n<T>(1,6)*z[14];
    z[39]=i*z[39];
    z[40]=n<T>(2,3)*z[3] - z[5];
    z[40]= - n<T>(17,12)*z[8] + n<T>(1,18)*z[4] + n<T>(4,9)*z[2] + n<T>(1,3)*z[40]
     - n<T>(1,2)*z[6];
    z[40]=z[12]*z[40];
    z[31]= - n<T>(8,3)*z[4] - n<T>(5,3)*z[2] + z[3] + n<T>(10,3)*z[31];
    z[31]=z[11]*z[31];
    z[36]= - z[36] + z[5];
    z[36]=z[10]*z[36];
    z[41]=z[5] + z[8];
    z[41]=z[22]*z[41];
    z[42]=z[5] + 2*z[6];
    z[42]=z[23]*z[42];
    z[43]=n<T>(10,3)*i;
    z[43]= - z[17]*z[43];

    r +=  - n<T>(3,2)*z[15] - n<T>(5,18)*z[16] + n<T>(10,3)*z[20] + n<T>(55,12)*z[21]
       + z[24] + z[29] + z[30] + z[31] + z[32] + n<T>(1,3)*z[33] + z[34] - 
      n<T>(5,3)*z[35] + z[36] + z[37] + n<T>(1,2)*z[38] + z[39] + z[40] + z[41]
       + z[42] + z[43];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf278(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf278(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
