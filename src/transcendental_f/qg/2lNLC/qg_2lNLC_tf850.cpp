#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf850(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[127];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[5];
    z[8]=d[8];
    z[9]=d[9];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[8];
    z[13]=d[6];
    z[14]=d[15];
    z[15]=d[4];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=d[11];
    z[19]=e[2];
    z[20]=e[5];
    z[21]=e[9];
    z[22]=f[0];
    z[23]=f[3];
    z[24]=f[5];
    z[25]=f[8];
    z[26]=f[11];
    z[27]=f[13];
    z[28]=f[17];
    z[29]=f[20];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[17];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[29];
    z[41]=c[30];
    z[42]=c[31];
    z[43]=c[47];
    z[44]=c[51];
    z[45]=c[54];
    z[46]=c[55];
    z[47]=c[59];
    z[48]=c[61];
    z[49]=c[62];
    z[50]=c[66];
    z[51]=c[68];
    z[52]=c[69];
    z[53]=c[72];
    z[54]=c[75];
    z[55]=c[83];
    z[56]=e[3];
    z[57]=e[10];
    z[58]=e[6];
    z[59]=e[7];
    z[60]=e[13];
    z[61]=e[12];
    z[62]=e[14];
    z[63]=f[4];
    z[64]=f[6];
    z[65]=f[12];
    z[66]=f[16];
    z[67]=f[18];
    z[68]=f[31];
    z[69]=f[36];
    z[70]=f[39];
    z[71]=f[51];
    z[72]=f[52];
    z[73]=f[55];
    z[74]=f[56];
    z[75]=f[58];
    z[76]=f[101];
    z[77]=g[0];
    z[78]=g[3];
    z[79]=g[5];
    z[80]=g[8];
    z[81]=g[11];
    z[82]=g[13];
    z[83]=g[17];
    z[84]=g[21];
    z[85]=g[29];
    z[86]=g[30];
    z[87]=g[35];
    z[88]=g[41];
    z[89]=g[42];
    z[90]=g[43];
    z[91]=g[56];
    z[92]=g[57];
    z[93]=g[76];
    z[94]=g[94];
    z[95]=g[107];
    z[96]=g[125];
    z[97]=n<T>(1,2)*z[3];
    z[98]= - n<T>(2121791,6032) + 339*z[3];
    z[98]=z[98]*z[97];
    z[99]=345*z[11];
    z[100]=30*z[6];
    z[101]=12*z[8];
    z[102]= - 404*z[2] + n<T>(1768165,18096) + 536*z[3];
    z[102]=z[2]*z[102];
    z[103]=n<T>(845,6)*z[5] - 1361*z[2] + n<T>(13478539,72384) + 887*z[3];
    z[103]=z[5]*z[103];
    z[98]=z[103] + z[102] + z[98] + z[101] + z[100] - 42*z[13] - n<T>(11999,6)*z[30]
     + 36*z[10]
     + z[99]
     + 1316*z[12];
    z[98]=z[5]*z[98];
    z[102]=z[5]*z[1];
    z[103]=z[2]*z[1];
    z[104]=z[1]*z[3];
    z[104]= - n<T>(3893,2)*z[102] + n<T>(4595,2)*z[104] - 351*z[103];
    z[104]=i*z[104];
    z[105]= - 1 - 464*z[3];
    z[106]=i*z[1];
    z[105]=1643*z[106] + 887*z[5] + 2*z[105] + 41*z[2];
    z[105]=2*z[105] - n<T>(1643,2)*z[4];
    z[105]=z[4]*z[105];
    z[106]= - 2*z[10] - 79*z[11] - 9*z[12];
    z[107]=n<T>(2194175,24128) - 618*z[3];
    z[107]=z[3]*z[107];
    z[108]= - 2242*z[2] - n<T>(606319,4524) + 3236*z[3];
    z[108]=z[2]*z[108];
    z[109]=n<T>(148897,36192) - 257*z[3];
    z[109]=n<T>(1097,2)*z[5] + n<T>(19,2)*z[109] + 1517*z[2];
    z[109]=z[5]*z[109];
    z[104]=n<T>(1,3)*z[105] + z[104] + z[109] + z[108] + z[107] - n<T>(1252429,72384)*z[8]
     + n<T>(407263,4524)*z[6] - n<T>(15,2)*z[9]
     + 16*z[13]
     + n<T>(33839,24)*z[30]
     + 9*z[106] - n<T>(1875233,24128)*z[15];
    z[104]=z[4]*z[104];
    z[105]=18*z[13];
    z[106]=4042*z[10];
    z[107]=npow(z[3],2);
    z[108]=n<T>(3085,3)*z[2] - n<T>(4560911,18096) - 1003*z[3];
    z[108]=z[2]*z[108];
    z[108]=z[108] - 148*z[107] + 47*z[7] - 30*z[8] + z[105] - n<T>(713,6)*
    z[30] + z[106] + 1694*z[11] + 1343*z[12];
    z[108]=z[2]*z[108];
    z[109]= - n<T>(3293437,6032) + 1503*z[3];
    z[97]=z[109]*z[97];
    z[109]=8*z[16];
    z[110]=n<T>(1958173,18096)*z[14];
    z[97]=z[97] - 55*z[7] + n<T>(1903885,36192)*z[8] - n<T>(1842811,18096)*z[6]
    + 21*z[9] - 8*z[13] + n<T>(1875233,12064)*z[15] - 8084*z[10] - 2632*
    z[12] + 2402*z[11] + z[109] - z[110];
    z[97]=z[1]*z[97];
    z[111]= - n<T>(13478539,36192) + 2234*z[3];
    z[111]=z[1]*z[111];
    z[111]= - n<T>(2091,2)*z[102] + z[111] - 40*z[103];
    z[111]=z[5]*z[111];
    z[112]=n<T>(12204209,18096) - 4706*z[3];
    z[112]=z[1]*z[112];
    z[112]=z[112] + 2806*z[103];
    z[112]=z[2]*z[112];
    z[113]=1585*z[38] + 166*z[36];
    z[97]=z[111] + z[112] + z[97] + 6*z[113] - n<T>(8933,12)*z[31];
    z[97]=i*z[97];
    z[111]=6*z[9];
    z[112]=n<T>(3221053,24128) + n<T>(463,3)*z[3];
    z[112]=z[3]*z[112];
    z[112]=z[112] - z[111] + n<T>(8621,6)*z[30] + z[106] - 3019*z[11] + 135*
    z[12];
    z[112]=z[3]*z[112];
    z[113]=3*z[9];
    z[114]=3*z[13];
    z[115]=z[114] + n<T>(5,2)*z[9];
    z[115]=z[115]*z[113];
    z[116]=2*z[13];
    z[117]=z[116] - z[113];
    z[117]=6*z[117] + n<T>(4004459,18096)*z[6];
    z[117]=z[6]*z[117];
    z[118]=6*z[8];
    z[119]=n<T>(1,2)*z[7];
    z[120]= - z[119] - 41*z[13] - z[118];
    z[120]=z[7]*z[120];
    z[121]=8*z[20];
    z[122]=n<T>(1958173,18096)*z[19];
    z[123]=npow(z[15],2);
    z[124]=3*z[15] + z[13];
    z[124]=z[13]*z[124];
    z[125]=24*z[9] + n<T>(1730465,12064)*z[15];
    z[126]= - n<T>(13507945,72384)*z[8] - n<T>(1951387,18096)*z[6] + z[125];
    z[126]=z[8]*z[126];
    z[97]=z[104] + z[97] + z[98] + z[108] + z[112] + z[120] + z[126] + 
    z[117] + z[115] + 4*z[124] - n<T>(721681,27144)*z[30] - n<T>(2009375,24128)*
    z[123] + n<T>(2557603,18096)*z[11] + 3786*z[22] - 926*z[23] + 1520*z[24]
    + 224*z[26] + 1200*z[28] - 54*z[25] + 211*z[27] - 1262*z[29] - n<T>(813,2)*z[32]
     + n<T>(577,2)*z[35] - 1358*z[37] - 6102*z[39] - n<T>(93943,6)*z[42]
    - 1626*z[34] + n<T>(2439,2)*z[40] + 1119*z[41] - z[121] - z[122];
    z[97]=z[4]*z[97];
    z[98]=12*z[9];
    z[104]=n<T>(14015,12)*z[30] + 126*z[10] + 69*z[11] + 62*z[12];
    z[108]=n<T>(230,3)*z[2] - n<T>(3426211,36192) - 1234*z[3];
    z[108]=z[2]*z[108];
    z[104]=z[108] + 933*z[107] + n<T>(1029,8)*z[8] - z[100] - z[98] + 5*
    z[104] - z[105];
    z[104]=z[2]*z[104];
    z[105]= - 30*z[10] + 241*z[11] - 264*z[12];
    z[108]=n<T>(1977023,24128) + 366*z[3];
    z[108]=z[3]*z[108];
    z[112]= - n<T>(307361,18096) + 38*z[3];
    z[112]=11*z[112] - n<T>(1717,2)*z[2];
    z[112]=z[2]*z[112];
    z[115]= - n<T>(1109,24) + 107*z[3];
    z[115]= - n<T>(791,3)*z[5] + n<T>(1,2)*z[115] - n<T>(583,3)*z[2];
    z[115]=z[5]*z[115];
    z[105]=z[115] + z[112] + z[108] + n<T>(13,2)*z[7] - n<T>(1125,16)*z[8] - 17*
    z[6] + z[111] + n<T>(247,2)*z[13] + n<T>(5327,3)*z[30] + 3*z[105] - n<T>(955,16)
   *z[15];
    z[105]=z[5]*z[105];
    z[108]=5*z[8];
    z[112]= - 16*z[6] + n<T>(17,2)*z[8];
    z[112]=z[112]*z[108];
    z[115]=261*z[10] - 723*z[11] + 112*z[12];
    z[117]=n<T>(437355,24128) + 256*z[3];
    z[117]=z[3]*z[117];
    z[115]=z[117] + 2*z[115] - n<T>(50531,12)*z[30];
    z[115]=z[3]*z[115];
    z[117]=241*z[29];
    z[120]=npow(z[9],2);
    z[124]=z[111] + n<T>(91,2)*z[6];
    z[124]=z[6]*z[124];
    z[126]=71*z[15] - n<T>(1553,4)*z[7];
    z[126]=z[7]*z[126];
    z[104]=z[105] + z[104] + z[115] + n<T>(1,4)*z[126] + z[112] + z[124] + 3
   *z[120] - n<T>(4150051,217152)*z[30] + 39*z[123] + n<T>(3534787,18096)*z[12]
    + 435*z[22] - n<T>(303,2)*z[23] + 444*z[24] - n<T>(453,2)*z[26] + n<T>(333,2)*
    z[28] - 1692*z[25] + 96*z[27] - z[117] - n<T>(171,2)*z[32] + 90*z[35] - 
   109*z[37] + 480*z[39] - n<T>(113365,12)*z[42] - 342*z[34] + n<T>(513,2)*
    z[40] - 118*z[21] + 1293*z[41];
    z[104]=z[5]*z[104];
    z[101]= - z[101] + 12*z[7];
    z[105]=z[113] - z[106] + 2125*z[11] - 242*z[12] + z[101];
    z[105]=2*z[105] - n<T>(337,3)*z[107];
    z[105]=z[3]*z[105];
    z[106]=30*z[13] - 35*z[17] - 6*z[15];
    z[106]=z[106]*z[116];
    z[112]= - n<T>(7,2)*z[9] - 2*z[18] - z[114];
    z[112]=z[112]*z[113];
    z[109]=n<T>(1201,16)*z[7] - z[98] + 53*z[13] + z[109] - n<T>(71,4)*z[15];
    z[109]=z[7]*z[109];
    z[114]=59*z[21] + 4*z[20];
    z[98]= - n<T>(2475347,36192)*z[6] + z[98] - n<T>(3904931,9048)*z[14] - 12*
    z[13];
    z[98]=z[6]*z[98];
    z[115]=n<T>(11625961,72384)*z[8] + n<T>(2856187,18096)*z[6] - z[125];
    z[115]=z[8]*z[115];
    z[98]=z[105] + z[109] + z[115] + z[98] + z[112] + z[106] + n<T>(1068383,24128)*z[123]
     - n<T>(1213,4)*z[10]
     - n<T>(3589075,9048)*z[12]
     - n<T>(11570849,18096)*z[11]
     - 4776*z[22]
     + n<T>(2735,2)*z[23] - 2894*z[24] - n<T>(793,2)*
    z[26] - n<T>(4407,2)*z[28] + 1728*z[25] - 992*z[27] + 1928*z[29] + 2*
    z[114] + z[122];
    z[98]=z[1]*z[98];
    z[105]=59*z[17];
    z[99]= - 504*z[10] - 133*z[12] - z[105] - z[99];
    z[106]= - n<T>(97529,6032) - 1735*z[3];
    z[106]=z[3]*z[106];
    z[99]=n<T>(3,2)*z[106] - 13*z[7] + 34*z[6] - 69*z[13] + 2*z[99] + n<T>(955,8)
   *z[15];
    z[99]=z[1]*z[99];
    z[106]=n<T>(2104997,4524) - 770*z[3];
    z[106]=z[1]*z[106];
    z[106]=z[106] + 3125*z[103];
    z[106]=z[2]*z[106];
    z[109]= - 4 - n<T>(1465,2)*z[3];
    z[109]=z[1]*z[109];
    z[109]=z[109] + 1081*z[103];
    z[102]=3*z[109] + n<T>(3164,3)*z[102];
    z[102]=z[5]*z[102];
    z[109]= - 460*z[38] - 141*z[36];
    z[99]=z[102] + z[106] + z[99] + 3*z[109] - n<T>(6725,6)*z[31];
    z[99]=z[5]*z[99];
    z[102]= - 92*z[11] - 193*z[12];
    z[102]=z[111] + 11*z[102] - 4013*z[10];
    z[106]=n<T>(1165,4) + 1851*z[3];
    z[106]=z[3]*z[106];
    z[102]=z[106] + z[7] + 59*z[8] + 2*z[102] + n<T>(2994099,6032)*z[6];
    z[102]=z[1]*z[102];
    z[106]= - n<T>(18089933,18096) + 3142*z[3];
    z[106]=z[1]*z[106];
    z[103]=z[106] - n<T>(20485,3)*z[103];
    z[103]=z[2]*z[103];
    z[102]=z[103] + z[102] + n<T>(6272,3)*z[31] + 3155*z[38] + 1066*z[36];
    z[102]=z[2]*z[102];
    z[103]=n<T>(8279,12)*z[31] + 2477*z[38] + 826*z[36];
    z[103]=z[3]*z[103];
    z[98]=z[99] + z[102] + z[98] + z[103] - n<T>(10065865,108576)*z[31] + 
   n<T>(24128035,36192)*z[36] + n<T>(24236611,18096)*z[38] - n<T>(5,2)*z[33] - n<T>(6272,3)*z[43]
     + 11141*z[50]
     + n<T>(17287,3)*z[47];
    z[98]=i*z[98];
    z[99]=n<T>(73,2)*z[10] - 5*z[11] + 47*z[12];
    z[102]=6 - n<T>(1405,2)*z[3];
    z[102]=z[3]*z[102];
    z[103]=n<T>(8335,6)*z[2] + n<T>(5390083,12064) + n<T>(500,3)*z[3];
    z[103]=z[2]*z[103];
    z[99]=z[103] + z[102] - 24*z[7] - n<T>(1261,16)*z[8] - n<T>(2813139,12064)*
    z[6] + 23*z[99] - n<T>(24523,4)*z[30];
    z[99]=z[2]*z[99];
    z[102]=z[9] - z[13];
    z[102]=12*z[102];
    z[103]=z[102] + z[119];
    z[103]=z[7]*z[103];
    z[106]=npow(z[6],2);
    z[109]=npow(z[13],2);
    z[111]=965*z[41] + n<T>(653,2)*z[40];
    z[100]=z[100] + n<T>(29,2)*z[8];
    z[100]=z[8]*z[100];
    z[112]= - n<T>(1213,8) - n<T>(1231,3)*z[3];
    z[112]=z[3]*z[112];
    z[112]=z[112] + n<T>(8423,4)*z[30] + 1675*z[10] + 215*z[11] + 431*z[12];
    z[112]=z[3]*z[112];
    z[99]=z[99] + z[112] + z[103] + z[100] + n<T>(2813139,12064)*z[106] + 6*
    z[109] - n<T>(18726169,108576)*z[30] + n<T>(1165,8)*z[10] + n<T>(3643363,18096)*
    z[12] + n<T>(4506623,9048)*z[11] + 111*z[22] - 137*z[23] + 478*z[24] + 
   202*z[26] + 468*z[28] + 360*z[25] + 565*z[27] - 184*z[29] - n<T>(653,4)*
    z[32] + n<T>(1043,6)*z[35] - n<T>(1121,2)*z[37] - 1150*z[39] + n<T>(192157,24)*
    z[42] + n<T>(3,2)*z[111] - 653*z[34];
    z[99]=z[2]*z[99];
    z[100]=z[13] + z[8];
    z[100]=z[100]*z[118];
    z[103]=59*z[59];
    z[102]=z[102] + n<T>(1,2)*z[6];
    z[102]=z[6]*z[102];
    z[108]=n<T>(101,6)*z[7] + z[108] - 15*z[9] + n<T>(649,16)*z[15] + 27*z[13];
    z[108]=z[7]*z[108];
    z[100]=z[108] + z[100] + z[102] + n<T>(1,8)*z[30] - n<T>(97,4)*z[123] - 
    z[121] - 24*z[60] + z[103];
    z[100]=z[7]*z[100];
    z[102]= - 17*z[12] + n<T>(205,2)*z[10];
    z[101]=n<T>(337,12)*z[107] + z[113] + 9*z[102] - n<T>(6053,24)*z[30] - 
    z[101];
    z[101]=z[3]*z[101];
    z[102]= - 53*z[41] + n<T>(1,2)*z[40];
    z[102]=n<T>(4481,24)*z[42] + n<T>(3,2)*z[102] - z[34];
    z[107]=npow(z[8],2);
    z[108]=3*z[8] + 11*z[7];
    z[108]=z[7]*z[108];
    z[101]=z[101] + 2*z[108] - 31*z[107] - n<T>(2356969,72384)*z[30] + n<T>(1261,8)*z[10]
     + 444*z[22] - 153*z[23]
     + 452*z[24]
     + 197*z[26]
     + 369*z[28]
    - 342*z[25] + 120*z[27] - z[117] - n<T>(5,4)*z[32] + n<T>(286,3)*z[35] - 
   n<T>(1409,2)*z[37] + 5*z[102] - 1954*z[39];
    z[101]=z[3]*z[101];
    z[102]= - n<T>(3561931,3)*z[57] - n<T>(572321,4)*z[123];
    z[102]=n<T>(1,16)*z[102] + n<T>(352615,9)*z[30];
    z[107]= - n<T>(1440929,12064)*z[15] - 5*z[13];
    z[107]=n<T>(65,6)*z[8] + n<T>(3534787,18096)*z[6] + n<T>(1,2)*z[107] - 9*z[9];
    z[107]=z[8]*z[107];
    z[102]=z[107] + n<T>(3019051,36192)*z[106] + n<T>(1,377)*z[102] - 9*z[120];
    z[102]=z[8]*z[102];
    z[103]= - n<T>(98,3)*z[109] + n<T>(57,8)*z[30] - 6*z[123] - 70*z[21] + 
    z[103] - 12*z[58];
    z[103]=z[13]*z[103];
    z[105]= - z[105] + 4*z[16];
    z[105]=n<T>(4426769,36192)*z[15] - n<T>(50575,4)*z[10] - 3859*z[12] + n<T>(8367,4)*z[11]
     + 2*z[105] - z[110];
    z[105]=z[30]*z[105];
    z[106]= - 4*z[60] + 2*z[62] + z[61];
    z[106]= - n<T>(1,2)*z[109] + 2*z[106] + n<T>(35,8)*z[30];
    z[107]= - 9*z[13] + z[9];
    z[107]=z[9]*z[107];
    z[106]=3*z[106] + n<T>(1,2)*z[107];
    z[106]=z[9]*z[106];
    z[107]= - 4*z[15] - n<T>(1512185,12064)*z[6];
    z[107]=z[6]*z[107];
    z[107]=3*z[107] + n<T>(1198597,12064)*z[30] + 107*z[123] - n<T>(3904931,9048)
   *z[19] - n<T>(3362875,18096)*z[57] - 83*z[56];
    z[107]=z[6]*z[107];
    z[108]=4*z[96] - z[95];
    z[108]= - n<T>(9,2)*z[89] + 63*z[90] + 13*z[91] - 19*z[92] + 23*z[93] + 
   3*z[108] - n<T>(89,2)*z[94];
    z[109]=npow(z[11],2);
    z[110]=npow(z[12],2);
    z[111]=npow(z[10],2);
    z[112]= - n<T>(43,8)*z[123] - n<T>(59,4)*z[58] - 107*z[56];
    z[112]=z[15]*z[112];

    r += 6*z[22] + n<T>(3221053,24128)*z[23] + n<T>(23802307,72384)*z[24] + 
      n<T>(13478539,72384)*z[26] + n<T>(292587,24128)*z[28] + n<T>(12943883,36192)*
      z[32] + n<T>(24888067,434304)*z[35] - n<T>(24019459,72384)*z[37] - 
      n<T>(24236611,36192)*z[39] - n<T>(33477267,24128)*z[42] + n<T>(653,4)*z[44]
       + 653*z[45] + n<T>(71483,12)*z[46] - n<T>(2486,3)*z[48] + 14411*z[49] - 
      n<T>(1311,2)*z[51] + n<T>(1185,4)*z[52] - n<T>(1959,4)*z[53] - n<T>(2895,2)*z[54]
       - n<T>(192157,24)*z[55] - n<T>(13507945,72384)*z[63] + 7*z[64] + n<T>(2009375,24128)*z[65]
     - n<T>(282785,24128)*z[66]
     + 2*z[67] - n<T>(1315,16)*z[68]
       + n<T>(189,16)*z[69] + n<T>(403,16)*z[70] + 13*z[71] - 4*z[72] + 16*
      z[73] + 5*z[74] + 15*z[75] + z[76] - n<T>(213,2)*z[77] - n<T>(603,2)*
      z[78] + n<T>(63,4)*z[79] + n<T>(4353,2)*z[80] - n<T>(4551,2)*z[81] - n<T>(135,2)*
      z[82] + n<T>(243,2)*z[83] - n<T>(259,2)*z[84] + 1696*z[85] + n<T>(7539,4)*
      z[86] - n<T>(1511,2)*z[87] - n<T>(337,2)*z[88] + z[97] + z[98] + z[99] + 
      z[100] + z[101] + z[102] + z[103] + z[104] + z[105] + z[106] + 
      z[107] + 3*z[108] - 447*z[109] + 198*z[110] - n<T>(189,4)*z[111] + 
      z[112];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf850(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf850(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
