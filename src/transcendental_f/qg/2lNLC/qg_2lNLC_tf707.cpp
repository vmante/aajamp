#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf707(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[53];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[8];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[31];
    z[24]=e[10];
    z[25]=e[6];
    z[26]=f[3];
    z[27]=f[4];
    z[28]=f[5];
    z[29]=f[11];
    z[30]=f[12];
    z[31]=f[16];
    z[32]=f[17];
    z[33]=f[31];
    z[34]=f[36];
    z[35]=f[39];
    z[36]=n<T>(283,3)*z[3];
    z[37]= - z[36] + n<T>(2545,2)*z[9];
    z[38]=z[37] + n<T>(6503,12)*z[7];
    z[39]=z[7]*z[38];
    z[40]=n<T>(60037,12)*z[6] - n<T>(28369,2)*z[4] + n<T>(26107,3)*z[2];
    z[40]=z[6]*z[40];
    z[39]=z[39] - z[40];
    z[40]=npow(z[4],2);
    z[41]=npow(z[9],2);
    z[42]=npow(z[3],2);
    z[43]=7069*z[13];
    z[44]=z[43] + 32893*z[12];
    z[44]=n<T>(1,8)*z[44] - 269*z[15];
    z[45]=npow(z[2],2);
    z[46]=n<T>(32893,48)*z[6] - n<T>(7375,3)*z[2] + n<T>(7069,48)*z[7] + n<T>(2545,16)*
    z[9] + n<T>(28369,16)*z[4] - n<T>(919,3)*z[3];
    z[46]=z[5]*z[46];
    z[39]=n<T>(1,2)*z[46] - n<T>(14609,24)*z[45] - n<T>(2545,32)*z[41] + n<T>(7069,24)*
    z[42] + n<T>(1,3)*z[44] + n<T>(21395,32)*z[40] - n<T>(1,8)*z[39];
    z[39]=z[5]*z[39];
    z[44]=n<T>(1,1508)*z[7];
    z[38]=z[38]*z[44];
    z[45]=19*z[11];
    z[46]=npow(z[10],2);
    z[47]=n<T>(3,4)*z[46];
    z[48]= - z[43] - 62111*z[12];
    z[49]= - z[8] - n<T>(1,4)*z[3];
    z[49]=z[3]*z[49];
    z[50]=z[10] - n<T>(3487,6032)*z[9];
    z[50]=z[9]*z[50];
    z[51]= - n<T>(129971,4524)*z[2] + 19*z[4] + n<T>(7069,1508)*z[3];
    z[51]=z[2]*z[51];
    z[38]=z[51] + z[38] + z[50] + n<T>(7069,2262)*z[49] + z[47] - n<T>(41941,2262)*z[14]
     + n<T>(1,4524)*z[48] - z[45];
    z[48]=n<T>(1,4)*z[9];
    z[49]=z[48] - z[10];
    z[50]=n<T>(22667,2262)*z[2] - n<T>(35909,6032)*z[4] + z[49];
    z[50]=z[6]*z[50];
    z[37]= - n<T>(60037,6)*z[6] + n<T>(62111,3)*z[2] - n<T>(7069,6)*z[7] + n<T>(7069,3)*
    z[8] - n<T>(21395,2)*z[4] - z[37];
    z[37]=z[5]*z[37];
    z[37]=n<T>(1,3016)*z[37] + n<T>(1,2)*z[38] + z[50];
    z[37]=z[1]*z[37];
    z[38]=z[21] + n<T>(1,2)*z[19];
    z[38]=231949*z[38] - n<T>(22903,6)*z[16];
    z[37]=n<T>(1,9048)*z[38] + z[37];
    z[37]=i*z[37];
    z[38]=n<T>(1,2)*z[2];
    z[50]=3*z[7];
    z[51]=n<T>(113383,2262)*z[2] - n<T>(7069,754)*z[3] - z[50];
    z[51]=z[51]*z[38];
    z[45]=z[51] + n<T>(7069,1508)*z[42] - 19*z[40] - n<T>(79831,13572)*z[15] + 
   n<T>(32893,2262)*z[14] + n<T>(14609,1131)*z[12] + z[45];
    z[45]=z[2]*z[45];
    z[51]=z[30] + z[31];
    z[52]=z[20] - z[28];
    z[51]=n<T>(7069,2262)*z[17] + n<T>(231949,27144)*z[18] - n<T>(231949,2262)*z[22]
    - n<T>(38927,4524)*z[23] + n<T>(21395,1508)*z[26] - n<T>(6503,4524)*z[27] + 
   n<T>(60037,4524)*z[29] + n<T>(35909,1508)*z[32] - z[33] + z[35] - z[34] - 
   n<T>(231949,4524)*z[52] + n<T>(2545,1508)*z[51];
    z[52]=n<T>(7069,754)*z[8] - 7*z[10];
    z[52]=z[15]*z[52];
    z[45]=z[45] + n<T>(1,2)*z[51] + n<T>(1,3)*z[52];
    z[40]=n<T>(35909,1508)*z[40] - n<T>(14797,13572)*z[15] + n<T>(32893,1131)*z[14]
    - 3*z[46];
    z[46]=z[50] - n<T>(50989,4524)*z[2];
    z[38]=z[46]*z[38];
    z[46]=n<T>(1,4)*z[6];
    z[49]= - z[46] - n<T>(32893,4524)*z[2] - n<T>(3,4)*z[7] + n<T>(28369,6032)*z[4]
    - z[49];
    z[49]=z[6]*z[49];
    z[50]= - z[10] + z[9];
    z[50]=z[9]*z[50];
    z[38]=z[49] + z[38] + n<T>(1,4)*z[40] + z[50];
    z[38]=z[38]*z[46];
    z[40]= - n<T>(283,8)*z[42] + n<T>(283,4)*z[24] - n<T>(919,3)*z[15];
    z[36]= - z[36] + n<T>(2545,4)*z[9];
    z[36]=z[7]*z[36];
    z[36]=n<T>(1,4)*z[36] + n<T>(1,3)*z[40] + n<T>(2545,16)*z[41];
    z[36]=z[36]*z[44];
    z[40]= - n<T>(13855,4)*z[42] + n<T>(2545,4)*z[15] + n<T>(283,2)*z[24] - z[43];
    z[40]=z[3]*z[40];
    z[41]= - n<T>(1,2)*z[41] - n<T>(2545,9048)*z[15] - z[25] + z[47];
    z[41]=z[41]*z[48];
    z[42]= - n<T>(16871,72384)*z[15] + n<T>(19,8)*z[11] + z[14];
    z[42]=z[4]*z[42];

    r += z[36] + n<T>(1,2)*z[37] + z[38] + n<T>(1,754)*z[39] + n<T>(1,9048)*z[40]
       + z[41] + z[42] + n<T>(1,8)*z[45];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf707(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf707(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
