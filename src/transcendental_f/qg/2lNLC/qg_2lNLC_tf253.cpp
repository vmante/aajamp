#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf253(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[45];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[19];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[31];
    z[22]=e[10];
    z[23]=d[5];
    z[24]=f[3];
    z[25]=f[4];
    z[26]=f[5];
    z[27]=f[11];
    z[28]=f[12];
    z[29]=f[16];
    z[30]=f[17];
    z[31]=13*z[2];
    z[32]=z[1]*i;
    z[33]= - n<T>(1,4)*z[2] + n<T>(1,2)*z[3] + z[32];
    z[33]=z[33]*z[31];
    z[34]=n<T>(1,2)*z[4];
    z[31]= - z[31] + 7*z[3];
    z[31]=z[31]*z[34];
    z[35]=z[32]*z[3];
    z[36]=npow(z[3],2);
    z[31]=z[31] + z[33] - n<T>(1,4)*z[36] - 7*z[35];
    z[31]=z[4]*z[31];
    z[33]=n<T>(1,2)*z[36];
    z[37]=z[33] - z[35];
    z[38]=n<T>(1,2)*z[6];
    z[39]=z[32] - z[3];
    z[40]= - n<T>(7,2)*z[39] - 5*z[5];
    z[40]=z[40]*z[38];
    z[41]= - n<T>(1,2)*z[5] - z[39];
    z[41]=z[5]*z[41];
    z[40]=z[40] + n<T>(5,2)*z[41] - z[37];
    z[40]=z[6]*z[40];
    z[41]=z[5] + z[3];
    z[41]=z[32]*z[41];
    z[41]=z[13] + z[41];
    z[41]=z[7]*z[41];
    z[42]=n<T>(1,2)*z[9];
    z[43]=z[8] - z[42] - n<T>(7,6)*z[4] - n<T>(1,12)*z[6] - n<T>(25,12)*z[2] - n<T>(7,12)
   *z[3] + z[5];
    z[43]=z[13]*z[43];
    z[44]=z[5] - z[39];
    z[44]=z[11]*z[44];
    z[31]=z[40] + z[31] + z[43] + z[41] + z[44] - z[15];
    z[38]=z[38] + z[39];
    z[38]=z[6]*z[38];
    z[39]=z[6] + z[39];
    z[39]=z[39]*z[42];
    z[37]=z[39] + z[38] + z[37];
    z[37]=z[37]*z[42];
    z[38]=2*z[3];
    z[39]=z[38]*z[32];
    z[40]=z[32] + z[3];
    z[41]= - 2*z[40] + z[4];
    z[41]=z[4]*z[41];
    z[42]= - z[3] + z[4];
    z[42]=z[8]*z[42];
    z[41]=z[42] + z[41] + z[36] + z[39];
    z[41]=z[8]*z[41];
    z[40]=n<T>(7,2)*z[5] - z[40];
    z[40]=z[5]*z[40];
    z[33]=z[40] - z[33] + 5*z[35];
    z[33]=z[5]*z[33];
    z[35]= - n<T>(11,4)*z[36] - z[39];
    z[36]=n<T>(1,2)*z[2];
    z[39]=z[3] - 11*z[32];
    z[39]=n<T>(11,6)*z[2] + n<T>(1,3)*z[39] + z[5];
    z[39]=z[39]*z[36];
    z[40]= - z[5]*z[3];
    z[35]=z[39] + n<T>(1,3)*z[35] + z[40];
    z[35]=z[2]*z[35];
    z[34]=z[34] - z[32] + z[36];
    z[34]=z[12]*z[34];
    z[32]= - z[2] + z[38] - z[32];
    z[32]= - z[23] + z[5] + n<T>(1,3)*z[32];
    z[32]=z[10]*z[32];
    z[36]= - z[18] + z[27] + z[26];
    z[38]=z[29] + z[28];
    z[39]=n<T>(14,3)*z[19] + n<T>(7,3)*z[17] + n<T>(5,18)*z[14];
    z[39]=i*z[39];
    z[40]=z[5] + z[6];
    z[40]=z[22]*z[40];

    r += n<T>(7,36)*z[16] - n<T>(7,3)*z[20] + n<T>(5,3)*z[21] - z[24] + n<T>(7,12)*
      z[25] + z[30] + n<T>(1,3)*z[31] + z[32] + n<T>(1,6)*z[33] + n<T>(13,3)*z[34]
       + z[35] + n<T>(7,6)*z[36] + z[37] + n<T>(1,4)*z[38] + z[39] + n<T>(5,6)*
      z[40] + z[41];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf253(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf253(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
