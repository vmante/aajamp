#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1106(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[95];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[9];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[10];
    z[26]=e[11];
    z[27]=e[12];
    z[28]=c[3];
    z[29]=c[11];
    z[30]=c[15];
    z[31]=c[19];
    z[32]=c[23];
    z[33]=c[25];
    z[34]=c[31];
    z[35]=e[3];
    z[36]=e[6];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[6];
    z[44]=f[7];
    z[45]=f[11];
    z[46]=f[12];
    z[47]=f[15];
    z[48]=f[16];
    z[49]=f[17];
    z[50]=f[18];
    z[51]=f[23];
    z[52]=f[31];
    z[53]=f[32];
    z[54]=f[33];
    z[55]=f[36];
    z[56]=f[37];
    z[57]=f[39];
    z[58]=f[43];
    z[59]=f[51];
    z[60]=f[52];
    z[61]=f[55];
    z[62]=f[56];
    z[63]=f[58];
    z[64]=f[75];
    z[65]=z[10] + z[5];
    z[66]=n<T>(1,2)*z[6];
    z[67]=z[1]*i;
    z[68]=2*z[67];
    z[65]=n<T>(185,18)*z[3] - n<T>(5,2)*z[7] - n<T>(2,3)*z[2] - z[66] + n<T>(1,6)*z[8]
    - z[68] - n<T>(19,6)*z[65];
    z[65]=z[3]*z[65];
    z[69]=z[7] - z[6];
    z[70]=z[67] - z[5];
    z[69]= - n<T>(77,6)*z[10] - n<T>(19,3)*z[70] + 6*z[69];
    z[69]=z[10]*z[69];
    z[71]=n<T>(1,2)*z[8];
    z[72]= - z[71] + z[70];
    z[73]=n<T>(1,3)*z[8];
    z[72]=z[72]*z[73];
    z[74]=3*z[9];
    z[75]= - z[74] - z[73] - z[6];
    z[75]=z[9]*z[75];
    z[76]=z[70] - z[8];
    z[77]=n<T>(1,2)*z[2];
    z[78]= - z[77] + z[9] - z[76];
    z[79]=n<T>(1,3)*z[2];
    z[78]=z[78]*z[79];
    z[80]=7*z[67] - n<T>(19,3)*z[5];
    z[80]=z[5]*z[80];
    z[81]=npow(z[6],2);
    z[65]=z[65] + z[78] + z[75] + 3*z[81] + z[80] + z[72] + z[69];
    z[65]=z[3]*z[65];
    z[69]=n<T>(1,2)*z[5];
    z[72]=z[69] + 3*z[67];
    z[72]=z[72]*z[5];
    z[75]=z[71] - z[67];
    z[78]=z[5] - z[75];
    z[78]=z[8]*z[78];
    z[78]= - z[72] + z[78];
    z[80]=n<T>(1,3)*z[6];
    z[81]= - 13*z[67] + 17*z[6];
    z[81]=z[81]*z[80];
    z[82]=n<T>(1,3)*z[9];
    z[83]=n<T>(59,6)*z[9] - n<T>(59,3)*z[67] - 35*z[6];
    z[83]=z[83]*z[82];
    z[84]=z[67] - z[4];
    z[85]= - n<T>(1,2)*z[7] - z[77] + n<T>(59,18)*z[9] + n<T>(13,6)*z[6] - n<T>(1,4)*
    z[8] + n<T>(3,4)*z[5] + n<T>(20,9)*z[84];
    z[85]=z[4]*z[85];
    z[86]=z[67]*z[7];
    z[87]=z[2]*z[67];
    z[88]=z[67] - n<T>(1,2)*z[10];
    z[88]=z[10]*z[88];
    z[78]=z[85] + z[86] + z[88] + z[87] + z[83] + n<T>(1,2)*z[78] + z[81];
    z[78]=z[4]*z[78];
    z[69]=z[69] - z[67];
    z[69]=z[69]*z[5];
    z[75]= - z[8]*z[75];
    z[81]= - z[67] + z[6];
    z[80]=z[81]*z[80];
    z[81]=4*z[7];
    z[83]=2*z[6];
    z[85]= - z[83] + z[7];
    z[85]=z[85]*z[81];
    z[66]= - z[66] + z[81];
    z[66]=z[14]*z[66];
    z[81]=z[3] - n<T>(23,9)*z[9] - n<T>(37,9)*z[67] - z[5];
    z[81]=z[9]*z[81];
    z[87]= - n<T>(49,6)*z[4] + z[6] + n<T>(46,3)*z[9];
    z[87]=z[4]*z[87];
    z[66]=z[66] + n<T>(1,3)*z[87] + z[85] + z[80] + z[69] + z[75] + z[81];
    z[66]=z[14]*z[66];
    z[75]=7*z[5];
    z[80]= - 344*z[67] + z[75];
    z[80]=n<T>(1,27)*z[80] + 13*z[8];
    z[81]=4*z[8];
    z[80]=z[80]*z[81];
    z[85]=n<T>(1,27)*z[6];
    z[87]= - n<T>(191,2)*z[5] - 296*z[6];
    z[87]=z[87]*z[85];
    z[88]=3*z[5];
    z[89]=n<T>(344,27)*z[10] - n<T>(688,27)*z[8] + n<T>(769,27)*z[67] - z[88];
    z[89]=z[10]*z[89];
    z[90]= - 103*z[67] + 65*z[5];
    z[90]=z[5]*z[90];
    z[91]=z[70] + z[6];
    z[91]=14*z[2] - 28*z[8] + n<T>(191,2)*z[91];
    z[91]=z[2]*z[91];
    z[92]=367*z[67] + n<T>(331,3)*z[5];
    z[92]= - n<T>(5,18)*z[7] + n<T>(28,3)*z[2] - n<T>(2111,6)*z[6] + n<T>(1,2)*z[92]
     + 
   n<T>(688,3)*z[8];
    z[92]=z[7]*z[92];
    z[80]=n<T>(1,9)*z[92] + 2*z[89] + n<T>(1,27)*z[91] + z[87] + n<T>(5,54)*z[90]
     + 
    z[80];
    z[80]=z[7]*z[80];
    z[87]= - z[77] + n<T>(1,6)*z[9] - n<T>(14,27)*z[6] + z[67] + z[71];
    z[87]=z[2]*z[87];
    z[81]= - n<T>(35,2)*z[70] + z[81];
    z[81]=7*z[81] - 137*z[6];
    z[81]=z[81]*z[85];
    z[85]=z[9] + z[76];
    z[82]=z[85]*z[82];
    z[85]=z[67]*z[5];
    z[89]=z[68] - z[8];
    z[90]= - z[5] - z[89];
    z[90]=z[8]*z[90];
    z[81]=z[87] + z[82] + z[81] + z[85] + z[90];
    z[81]=z[2]*z[81];
    z[69]=n<T>(19,2)*z[69];
    z[82]=z[71] + z[68] + z[5];
    z[82]=z[82]*z[73];
    z[87]= - n<T>(55,9)*z[67] - z[75];
    z[73]= - n<T>(7,3)*z[9] + n<T>(11,6)*z[6] + n<T>(1,4)*z[87] - z[73];
    z[73]=z[9]*z[73];
    z[87]=n<T>(35,6)*z[6] + n<T>(32,3)*z[67] + z[5];
    z[87]=z[6]*z[87];
    z[73]=z[73] + z[87] + z[69] + z[82];
    z[73]=z[9]*z[73];
    z[82]=n<T>(398,9)*z[8];
    z[87]=z[89]*z[82];
    z[69]=z[69] + z[87];
    z[87]=n<T>(362,9)*z[8];
    z[88]= - n<T>(181,9)*z[6] + z[87] - n<T>(389,9)*z[67] + z[88];
    z[83]=z[88]*z[83];
    z[88]=n<T>(719,9)*z[67] + 97*z[5];
    z[82]= - n<T>(326,3)*z[6] + n<T>(1,4)*z[88] - z[82];
    z[77]=n<T>(706,27)*z[10] + z[77] + n<T>(1,3)*z[82] + n<T>(19,4)*z[9];
    z[77]=z[10]*z[77];
    z[82]=n<T>(1,2)*z[9] + z[70];
    z[82]=z[9]*z[82];
    z[69]=z[77] + n<T>(19,2)*z[82] + n<T>(1,3)*z[69] + z[83];
    z[69]=z[10]*z[69];
    z[75]=503*z[67] - z[75];
    z[75]=2*z[75] - 503*z[8];
    z[75]=z[8]*z[75];
    z[77]=n<T>(131,2)*z[67] - 185*z[5];
    z[77]=z[5]*z[77];
    z[75]=z[77] + 2*z[75];
    z[77]= - n<T>(3,4) + n<T>(28,9)*z[67];
    z[77]=n<T>(5392,81)*z[6] - z[87] + 11*z[77] + n<T>(146,27)*z[5];
    z[77]=z[6]*z[77];
    z[75]=n<T>(1,27)*z[75] + z[77];
    z[75]=z[6]*z[75];
    z[74]=n<T>(8,3)*z[3] - z[79] - n<T>(1,3)*z[76] + z[74];
    z[74]=z[25]*z[74];
    z[76]= - n<T>(3,2)*z[67] + n<T>(7,9)*z[85];
    z[77]=n<T>(70,9)*z[6];
    z[79]=z[67]*z[77];
    z[76]=n<T>(77,9)*z[28] + 11*z[76] + z[79];
    z[76]=z[15]*z[76];
    z[79]= - z[8]*z[67];
    z[79]= - z[28] + z[79] - z[86];
    z[79]=z[16]*z[79];
    z[82]=z[67] - z[8];
    z[83]= - z[7] + z[82];
    z[83]=z[24]*z[83];
    z[79]=z[79] + z[83];
    z[83]=z[67]*z[4];
    z[83]=z[83] + z[28];
    z[86]=z[14]*z[67];
    z[86]=z[86] + z[83];
    z[86]=z[12]*z[86];
    z[87]= - z[14] + z[84];
    z[87]=z[27]*z[87];
    z[86]=z[86] + z[87];
    z[87]=z[3]*z[67];
    z[85]=z[28] + z[85] + z[87];
    z[85]=z[11]*z[85];
    z[70]=z[3] - z[70];
    z[70]=z[19]*z[70];
    z[70]=z[85] + z[70];
    z[85]= - n<T>(3,2)*z[5] - z[82];
    z[85]=z[8]*z[85];
    z[72]=z[72] + z[85];
    z[71]=z[72]*z[71];
    z[72]=n<T>(7,9)*z[67];
    z[85]= - n<T>(7,9)*z[5] + n<T>(3,2) + z[72];
    z[77]=11*z[85] - z[77];
    z[77]=z[21]*z[77];
    z[85]= - z[10]*z[67];
    z[83]=z[85] - z[83];
    z[83]=z[13]*z[83];
    z[85]= - z[8] - z[9];
    z[85]=z[67]*z[85];
    z[85]= - z[28] + z[85];
    z[85]=z[17]*z[85];
    z[87]=z[56] + z[62] - z[60] - z[43] + z[42] + z[39] - z[33];
    z[88]=z[45] - z[40];
    z[90]= - n<T>(257,8)*z[5] - n<T>(493,3)*z[8];
    z[90]= - n<T>(401,6)*z[9] + n<T>(1,3)*z[90] + n<T>(1787,4)*z[6];
    z[90]=n<T>(55,12)*z[4] + n<T>(127,12)*z[3] - n<T>(1073,54)*z[7] + n<T>(1043,27)*
    z[10] + n<T>(1,3)*z[90] + n<T>(17,2)*z[2];
    z[90]=z[28]*z[90];
    z[90]=z[50] + z[90] - z[51];
    z[91]= - z[32] - n<T>(1027,324)*z[29];
    z[91]=i*z[91];
    z[72]= - n<T>(28,9)*z[5] + n<T>(33,4) + z[72];
    z[72]=z[72]*npow(z[5],2);
    z[67]=n<T>(5701,2)*z[7] - n<T>(625,2)*z[2] + n<T>(7379,2)*z[6] + n<T>(463,2)*z[8]
     - 
   347*z[67] + 428*z[5];
    z[67]=z[22]*z[67];
    z[68]=z[4] - z[68] + z[2];
    z[68]=z[18]*z[68];
    z[84]=z[10] - z[84];
    z[84]=z[20]*z[84];
    z[89]= - z[2] + z[89];
    z[89]=z[23]*z[89];
    z[82]=z[9] - z[82];
    z[82]=z[26]*z[82];
    z[92]=z[10] + z[3];
    z[92]=z[35]*z[92];
    z[93]=z[10] + z[7];
    z[93]=z[36]*z[93];
    z[94]= - z[9] - z[14];
    z[94]=z[37]*z[94];

    r += n<T>(1931,54)*z[30] - n<T>(7,12)*z[31] - n<T>(235,12)*z[34] + z[38] - n<T>(17,12)*z[41]
     + n<T>(8,3)*z[44] - n<T>(85,12)*z[46] - 6*z[47]
     + n<T>(25,4)*z[48]
       - n<T>(1,4)*z[49] - n<T>(1034,27)*z[52] - n<T>(26,3)*z[53] - n<T>(2372,27)*z[54]
       + n<T>(398,27)*z[55] - n<T>(661,18)*z[57] + n<T>(28,27)*z[58] + n<T>(13,6)*z[59]
       - n<T>(59,18)*z[61] + n<T>(4,3)*z[63] + z[64] + z[65] + z[66] + n<T>(1,27)*
      z[67] + z[68] + z[69] + n<T>(17,3)*z[70] + z[71] + z[72] + z[73] + 2*
      z[74] + z[75] + z[76] + z[77] + z[78] + n<T>(1432,27)*z[79] + z[80]
       + z[81] + z[82] + z[83] + z[84] + z[85] + n<T>(49,9)*z[86] - n<T>(1,2)*
      z[87] - n<T>(3,4)*z[88] + z[89] + n<T>(1,3)*z[90] + z[91] + n<T>(55,3)*z[92]
       + n<T>(1376,27)*z[93] + n<T>(46,9)*z[94];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1106(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1106(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
