#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf112(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[68];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[6];
    z[6]=d[7];
    z[7]=d[1];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=d[4];
    z[12]=d[5];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[19];
    z[23]=c[21];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[0];
    z[30]=e[3];
    z[31]=e[10];
    z[32]=e[6];
    z[33]=e[7];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[18];
    z[44]=f[31];
    z[45]=f[36];
    z[46]=f[37];
    z[47]=f[39];
    z[48]=f[43];
    z[49]=f[48];
    z[50]=f[49];
    z[51]=1129*z[8];
    z[52]=n<T>(551,4)*z[7];
    z[53]=z[52] - n<T>(551,2)*z[9] + z[51];
    z[53]=z[7]*z[53];
    z[54]=n<T>(1,8)*z[3];
    z[55]=91*z[10] - n<T>(155,2)*z[3];
    z[55]=z[55]*z[54];
    z[56]=n<T>(1105,24)*z[9];
    z[57]=2*z[12];
    z[58]= - n<T>(1333,16)*z[6] + n<T>(1105,16)*z[11] + n<T>(1079,16)*z[2] + n<T>(111,8)
   *z[3] - n<T>(1129,24)*z[7] + n<T>(1057,48)*z[8] - z[56] + z[57];
    z[58]=z[4]*z[58];
    z[59]=npow(z[12],2);
    z[60]=n<T>(25,8)*z[59];
    z[61]=npow(z[8],2);
    z[62]= - z[18] + n<T>(13,8)*z[16];
    z[63]= - z[5]*z[57];
    z[64]= - n<T>(999,8)*z[2] + n<T>(283,8)*z[3] + 4*z[5];
    z[64]=z[2]*z[64];
    z[65]=n<T>(85,32)*z[11] - n<T>(1105,16)*z[8] + z[5];
    z[65]=z[11]*z[65];
    z[66]=19*z[11];
    z[67]=8*z[6] - z[66] + n<T>(2105,16)*z[2] - z[5] + 7*z[13] - n<T>(197,4)*
    z[3];
    z[67]=z[6]*z[67];
    z[53]=z[58] + z[67] + z[65] + z[64] + z[63] + z[55] + n<T>(1,24)*z[53]
    + n<T>(1715,48)*z[61] - z[60] - n<T>(1143,16)*z[14] + n<T>(1105,24)*z[15] + 7*
    z[62] - n<T>(2153,16)*z[17];
    z[53]=z[1]*z[53];
    z[53]=z[53] - n<T>(1117,288)*z[20] - z[23] + 207*z[26] + n<T>(1015,8)*z[24];
    z[53]=i*z[53];
    z[55]=5*z[5];
    z[58]=z[66] - n<T>(677,4)*z[2] + n<T>(1151,16)*z[3] - z[55];
    z[58]=n<T>(1,2)*z[58] - n<T>(1,3)*z[6];
    z[58]=z[6]*z[58];
    z[62]=npow(z[5],2);
    z[63]=npow(z[3],2);
    z[64]= - z[5] - n<T>(433,32)*z[2];
    z[64]=z[2]*z[64];
    z[65]= - z[5] - n<T>(77,8)*z[11];
    z[65]=z[11]*z[65];
    z[58]=z[58] + z[65] + z[64] + z[62] + n<T>(1151,32)*z[63] + z[60] - n<T>(55,96)*z[19]
     + 7*z[18]
     + n<T>(657,8)*z[17];
    z[58]=z[6]*z[58];
    z[60]=z[2] - z[3];
    z[55]= - n<T>(169,24)*z[11] + z[55] - n<T>(371,4)*z[8] + 67*z[7] - n<T>(91,4)*
    z[60];
    z[60]=n<T>(1,2)*z[11];
    z[55]=z[55]*z[60];
    z[64]=npow(z[7],2);
    z[55]=z[55] + n<T>(3,2)*z[62] - n<T>(95,4)*z[64] - n<T>(371,8)*z[61] + n<T>(7,4)*
    z[59] + n<T>(4069,96)*z[19] - 7*z[32] - n<T>(173,8)*z[30];
    z[55]=z[55]*z[60];
    z[51]= - z[51] + z[52];
    z[51]=z[7]*z[51];
    z[52]= - n<T>(13,6)*z[15] + 3*z[14];
    z[60]=n<T>(757,2)*z[3];
    z[62]=n<T>(1333,8)*z[6] - z[60] + 259*z[2];
    z[62]=z[6]*z[62];
    z[51]=z[62] - n<T>(111,4)*z[63] + n<T>(1,6)*z[51] - n<T>(1715,12)*z[61] + 85*
    z[52] - n<T>(2803,72)*z[19];
    z[52]= - n<T>(1057,4)*z[8] + 1117*z[7];
    z[52]=n<T>(1,3)*z[52] + z[60];
    z[52]=n<T>(239,4)*z[6] - n<T>(1105,8)*z[11] + n<T>(1,2)*z[52] - 253*z[2];
    z[52]=n<T>(1,4)*z[52] + n<T>(1,3)*z[4];
    z[52]=z[4]*z[52];
    z[60]=z[57] - z[5];
    z[60]=z[5]*z[60];
    z[57]= - z[57] - n<T>(91,32)*z[2];
    z[57]=z[2]*z[57];
    z[62]=1105*z[8] + n<T>(223,2)*z[11];
    z[62]=z[11]*z[62];
    z[51]=z[52] + n<T>(1,16)*z[62] + z[57] + z[60] + n<T>(1,4)*z[51];
    z[51]=z[4]*z[51];
    z[52]= - n<T>(551,3)*z[15] - n<T>(1739,3)*z[31] + 17*z[30];
    z[52]=n<T>(1,2)*z[52] + n<T>(221,3)*z[19];
    z[57]=n<T>(1169,8)*z[8] - 349*z[7];
    z[57]=z[7]*z[57];
    z[52]=n<T>(1,3)*z[57] + n<T>(1,2)*z[52] + n<T>(707,3)*z[61];
    z[52]=z[7]*z[52];
    z[57]=z[13] + n<T>(13,8)*z[10];
    z[56]=7*z[57] - z[56];
    z[56]=z[19]*z[56];
    z[57]=n<T>(1,2)*z[29] - z[16];
    z[57]= - n<T>(139,4)*z[63] + 91*z[57] - n<T>(391,24)*z[19];
    z[54]=z[57]*z[54];
    z[57]= - z[33] - n<T>(1,3)*z[19];
    z[57]=2*z[57] - z[59];
    z[60]=z[12] - n<T>(1,4)*z[5];
    z[60]=z[5]*z[60];
    z[57]=2*z[57] + z[60];
    z[57]=z[5]*z[57];
    z[60]= - 283*z[63] - n<T>(2105,6)*z[19] + 123*z[14] - 91*z[29] + 839*
    z[17];
    z[61]=n<T>(565,12)*z[2] + z[12] - n<T>(3,2)*z[5];
    z[61]=z[2]*z[61];
    z[60]=n<T>(1,16)*z[60] + z[61];
    z[60]=z[2]*z[60];
    z[59]= - n<T>(4,3)*z[59] - 4*z[33] - n<T>(5,6)*z[19];
    z[59]=z[12]*z[59];
    z[61]= - 707*z[31] + n<T>(4423,24)*z[19];
    z[61]=z[8]*z[61];

    r += n<T>(1879,96)*z[21] + n<T>(543,32)*z[22] - n<T>(3145,48)*z[25] - 109*z[27]
       - n<T>(21811,576)*z[28] - n<T>(283,8)*z[34] + n<T>(123,16)*z[35] - n<T>(175,16)*
      z[36] - n<T>(1715,48)*z[37] + n<T>(1011,16)*z[38] + n<T>(1301,32)*z[39] - n<T>(223,32)*z[40]
     - n<T>(379,32)*z[41]
     + n<T>(213,8)*z[42] - n<T>(91,24)*z[43]
     + n<T>(33,8)*z[44]
     + n<T>(5,8)*z[45] - 2*z[46]
     + n<T>(15,8)*z[47] - z[48] - z[49]
       + z[50] + z[51] + n<T>(1,4)*z[52] + z[53] + z[54] + z[55] + z[56] + 
      z[57] + z[58] + z[59] + z[60] + n<T>(1,12)*z[61];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf112(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf112(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
