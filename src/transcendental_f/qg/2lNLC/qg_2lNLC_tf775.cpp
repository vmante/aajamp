#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf775(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[5];
    z[10]=d[9];
    z[11]=d[16];
    z[12]=d[4];
    z[13]=d[11];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[8];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[3];
    z[28]=e[10];
    z[29]=e[4];
    z[30]=e[6];
    z[31]=e[12];
    z[32]=e[14];
    z[33]=f[3];
    z[34]=f[4];
    z[35]=f[5];
    z[36]=f[11];
    z[37]=f[12];
    z[38]=f[16];
    z[39]=f[17];
    z[40]=f[31];
    z[41]=f[36];
    z[42]=f[39];
    z[43]=f[51];
    z[44]=f[58];
    z[45]=27*z[14];
    z[46]=n<T>(1243,2)*z[15] - 19339*z[17] + n<T>(18585,2)*z[16];
    z[47]= - n<T>(15345,1508)*z[2] - n<T>(43691,1508)*z[3] + 27*z[4];
    z[47]=z[2]*z[47];
    z[46]=z[47] + n<T>(1,754)*z[46] - z[45];
    z[47]=n<T>(1,4)*z[12];
    z[48]=3*z[9];
    z[49]=z[48] - n<T>(135679,6032)*z[12];
    z[49]=z[49]*z[47];
    z[50]=z[10] + n<T>(51231,12064)*z[12];
    z[51]=z[50] - n<T>(17831,6032)*z[3];
    z[52]=n<T>(3281,12064)*z[6];
    z[53]=n<T>(27633,12064)*z[7] + z[52] - n<T>(4259,6032)*z[2] - n<T>(795,12064)*
    z[4] - z[11] - n<T>(18585,6032)*z[8] + z[51];
    z[53]=z[5]*z[53];
    z[54]=npow(z[9],2);
    z[55]= - z[10]*z[13];
    z[56]=17077*z[8] + n<T>(12553,4)*z[3];
    z[56]=z[3]*z[56];
    z[57]= - 2*z[4] + z[48] + z[11] + z[10];
    z[57]=z[4]*z[57];
    z[58]= - z[6] + n<T>(7219,1508)*z[2] - n<T>(80637,12064)*z[4] + z[3] - 4*
    z[9] + n<T>(53,8)*z[12];
    z[58]=z[6]*z[58];
    z[50]=z[50] + z[4];
    z[59]=n<T>(8029,24128)*z[7] + n<T>(3,2)*z[2] + n<T>(8783,6032)*z[3] - z[50];
    z[59]=z[7]*z[59];
    z[46]=z[53] + z[59] + z[58] + z[57] + n<T>(1,3016)*z[56] + z[49] + n<T>(23,16)*z[54]
     + z[55]
     + n<T>(1,4)*z[46];
    z[46]=z[1]*z[46];
    z[49]=z[24] + n<T>(1,2)*z[22];
    z[49]=78151*z[49] - n<T>(6623,2)*z[19];
    z[46]=n<T>(1,6032)*z[49] + z[46];
    z[46]=i*z[46];
    z[49]=n<T>(1,2)*z[2];
    z[53]=z[3] + n<T>(1,8)*z[2];
    z[53]=z[53]*z[49];
    z[50]= - z[6] + z[2] + n<T>(11799,3016)*z[3] + z[48] - z[50];
    z[50]=n<T>(1,2)*z[50] - n<T>(1,3)*z[7];
    z[50]=z[7]*z[50];
    z[55]=npow(z[12],2);
    z[56]=npow(z[4],2);
    z[57]=npow(z[3],2);
    z[58]= - n<T>(14815,16)*z[28] + n<T>(1138,3)*z[18];
    z[59]=n<T>(13,16)*z[6] + z[3] - n<T>(9,8)*z[2];
    z[59]=z[6]*z[59];
    z[50]=z[50] + z[59] + z[53] + n<T>(1,2)*z[56] + n<T>(8783,12064)*z[57] - 
   n<T>(51231,24128)*z[55] + n<T>(1,377)*z[58] - z[54];
    z[50]=z[7]*z[50];
    z[53]=n<T>(1,2)*z[6];
    z[49]=n<T>(4035,24128)*z[7] - z[53] - z[49] + z[51];
    z[49]=z[7]*z[49];
    z[51]= - z[52] + n<T>(27633,3016)*z[2] + z[3] - n<T>(79129,6032)*z[4];
    z[51]=z[51]*z[53];
    z[52]= - z[10] + n<T>(12859,24128)*z[4];
    z[52]=z[4]*z[52];
    z[53]=npow(z[2],2);
    z[58]= - n<T>(15569,16)*z[7] + n<T>(23863,16)*z[6] - 6437*z[2] + n<T>(79129,16)*
    z[4] - n<T>(51231,16)*z[12] + 4175*z[3];
    z[58]=z[5]*z[58];
    z[49]=n<T>(1,1508)*z[58] + z[49] + z[51] + n<T>(14061,6032)*z[53] + z[52] - 
   n<T>(17077,6032)*z[57] + n<T>(27103,24128)*z[55] - n<T>(10961,9048)*z[18] + 
   n<T>(26879,6032)*z[15] + z[29] - n<T>(18585,6032)*z[16];
    z[49]=z[5]*z[49];
    z[51]=n<T>(14815,2)*z[17] - 14061*z[15];
    z[45]= - 19*z[56] - n<T>(46707,1508)*z[57] + n<T>(52963,4524)*z[18] + n<T>(1,377)
   *z[51] + z[45];
    z[51]=n<T>(29447,36192)*z[2] + n<T>(46707,12064)*z[3] - z[4];
    z[51]=z[2]*z[51];
    z[45]=n<T>(1,8)*z[45] + z[51];
    z[45]=z[2]*z[45];
    z[51]=23863*z[17] + n<T>(30425,12)*z[18];
    z[51]=n<T>(1,377)*z[51] - 39*z[54];
    z[52]=3*z[12];
    z[53]= - n<T>(1,2)*z[9] + z[52];
    z[52]=z[53]*z[52];
    z[53]= - z[3] - n<T>(14815,6032)*z[2];
    z[53]=z[2]*z[53];
    z[51]=z[53] + n<T>(80637,12064)*z[56] + n<T>(1,8)*z[51] + z[52];
    z[52]=n<T>(7,16)*z[6] - n<T>(20847,6032)*z[2] + n<T>(79129,24128)*z[4] - n<T>(1,2)*
    z[3] + 2*z[9] - n<T>(53,16)*z[12];
    z[52]=z[6]*z[52];
    z[51]=n<T>(1,2)*z[51] + z[52];
    z[51]=z[6]*z[51];
    z[52]= - z[42] + z[40] + z[41];
    z[53]=z[23] - z[35];
    z[56]=z[18]*z[8];
    z[52]= - n<T>(18585,3016)*z[56] - n<T>(9537,6032)*z[20] + n<T>(78151,72384)*
    z[21] - n<T>(78151,6032)*z[25] + n<T>(188907,12064)*z[26] + n<T>(795,12064)*
    z[33] + n<T>(4035,12064)*z[34] - n<T>(3281,12064)*z[36] - n<T>(27103,12064)*
    z[37] - n<T>(51231,12064)*z[38] + n<T>(80637,12064)*z[39] + z[44] - z[43] - 
   n<T>(78151,12064)*z[53] - n<T>(13,8)*z[52];
    z[53]= - 5*z[9] - n<T>(13,2)*z[12];
    z[47]=z[53]*z[47];
    z[47]=z[47] + n<T>(39,16)*z[54] + n<T>(117583,36192)*z[18] - n<T>(13,4)*z[30]
     - 
    z[27];
    z[47]=z[12]*z[47];
    z[53]=n<T>(51985,36192)*z[57] + z[55] - n<T>(36151,36192)*z[18] + n<T>(17077,3016)*z[16]
     - n<T>(8783,6032)*z[28]
     - z[27];
    z[53]=z[3]*z[53];
    z[48]= - n<T>(1,3)*z[4] - z[48] + z[10];
    z[48]=z[4]*z[48];
    z[48]=n<T>(1,2)*z[48] + z[54] - n<T>(135007,72384)*z[18] - z[29] + n<T>(27,8)*
    z[14];
    z[48]=z[4]*z[48];
    z[54]=n<T>(2,3)*z[18] - z[32] + z[31];
    z[54]=z[10]*z[54];
    z[55]=z[9]*z[18];

    r += z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + n<T>(1,2)*
      z[52] + z[53] + z[54] - n<T>(5,8)*z[55];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf775(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf775(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
