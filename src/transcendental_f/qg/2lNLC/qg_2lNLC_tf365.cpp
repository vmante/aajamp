#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf365(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[74];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[0];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[6];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[51];
    z[52]=f[55];
    z[53]=f[56];
    z[54]=f[58];
    z[55]=5*z[14];
    z[56]=n<T>(11,2)*z[3];
    z[57]=n<T>(1,2)*z[8];
    z[58]=n<T>(33,2)*z[7] - n<T>(5591,36)*z[2] + n<T>(3647,36)*z[5] - z[57] - 25*
    z[10] + z[56] + 43*z[4] + z[55] - 7*z[13];
    z[58]=z[7]*z[58];
    z[59]=n<T>(15,4)*z[3];
    z[60]=z[59] - z[6];
    z[61]=n<T>(21,4)*z[4];
    z[62]=n<T>(2125,36)*z[2] - n<T>(2477,72)*z[5] + n<T>(7,4)*z[8] - z[61] + z[60];
    z[62]=z[2]*z[62];
    z[63]= - z[21] + n<T>(9,4)*z[18];
    z[64]=npow(z[13],2);
    z[65]=n<T>(1,2)*z[15];
    z[66]=z[11] - z[65];
    z[66]=z[15]*z[66];
    z[67]=z[13] - z[6];
    z[67]=z[6]*z[67];
    z[68]= - n<T>(7,8)*z[4] - n<T>(9,4)*z[12] + z[11];
    z[68]=z[4]*z[68];
    z[69]=1195*z[9] - n<T>(1267,2)*z[3];
    z[69]=z[3]*z[69];
    z[70]= - z[12] + z[4];
    z[70]=3*z[70] - n<T>(37,16)*z[10];
    z[70]=z[10]*z[70];
    z[71]= - n<T>(377,36)*z[8] + n<T>(959,24)*z[10] + z[15] - n<T>(1259,36)*z[3];
    z[71]=z[8]*z[71];
    z[72]= - n<T>(827,18)*z[8] - n<T>(959,6)*z[10] + n<T>(1025,9)*z[3] + n<T>(827,9)*
    z[9] - 65*z[4];
    z[72]=z[5]*z[72];
    z[58]=n<T>(1,2)*z[58] + z[62] + n<T>(1,4)*z[72] + z[71] + z[70] + n<T>(1,72)*
    z[69] + z[68] + z[67] + z[66] + n<T>(5,4)*z[64] + n<T>(2477,72)*z[16] - n<T>(827,36)*z[17]
     + n<T>(5195,72)*z[19] - n<T>(5,2)*z[20] - z[63];
    z[58]=z[1]*z[58];
    z[62]= - 137*z[28] - n<T>(301,4)*z[26];
    z[62]=7*z[62] - n<T>(97,12)*z[23];
    z[58]=n<T>(1,9)*z[62] + z[58];
    z[58]=i*z[58];
    z[62]=n<T>(5,2)*z[64];
    z[64]=npow(z[3],2);
    z[66]=z[64] - z[62] + n<T>(4991,216)*z[22] + 5*z[20] - n<T>(1373,18)*z[19];
    z[56]=z[56] - 3*z[8];
    z[56]=z[56]*z[57];
    z[57]= - z[15] + 3*z[13];
    z[60]=n<T>(2359,144)*z[2] - n<T>(457,18)*z[5] - n<T>(3,4)*z[8] - z[60] - z[57];
    z[60]=z[2]*z[60];
    z[67]=npow(z[4],2);
    z[68]=npow(z[6],2);
    z[69]=npow(z[10],2);
    z[70]= - z[15]*z[13];
    z[71]=z[8] - z[6];
    z[72]= - n<T>(179,36)*z[5] + n<T>(221,6)*z[4] + z[71];
    z[72]=z[5]*z[72];
    z[73]= - n<T>(17,18)*z[7] + n<T>(1265,36)*z[2] - n<T>(3755,144)*z[5] + n<T>(25,4)*
    z[10] + n<T>(1,2)*z[3] - n<T>(695,48)*z[4] + 2*z[13] - z[65];
    z[73]=z[7]*z[73];
    z[56]=z[73] + z[60] + z[72] + z[56] - n<T>(23,4)*z[69] - n<T>(235,16)*z[67]
    + n<T>(3,4)*z[68] + z[70] + n<T>(1,2)*z[66];
    z[56]=z[7]*z[56];
    z[60]=z[67] + z[69];
    z[66]=n<T>(3019,54)*z[22] - n<T>(973,9)*z[16] + z[31] - n<T>(2449,9)*z[19];
    z[65]=z[13] + z[65];
    z[65]=z[15]*z[65];
    z[70]= - z[6]*z[13];
    z[59]= - 2*z[15] + z[59];
    z[59]=z[8]*z[59];
    z[71]=n<T>(851,36)*z[5] - z[71];
    z[71]=z[5]*z[71];
    z[57]=n<T>(901,72)*z[5] - z[6] + z[57];
    z[57]=n<T>(1,2)*z[57] - n<T>(245,9)*z[2];
    z[57]=z[2]*z[57];
    z[57]=z[57] + z[71] + z[59] + z[70] + n<T>(1,8)*z[66] + z[65] + n<T>(21,8)*
    z[60];
    z[57]=z[2]*z[57];
    z[59]=n<T>(1,4)*z[68];
    z[60]=npow(z[15],2);
    z[65]=z[4]*z[15];
    z[66]=n<T>(1,2)*z[4];
    z[70]= - n<T>(1043,18)*z[3] + 3*z[15] + z[66];
    z[70]=n<T>(1,2)*z[70] + 16*z[10];
    z[70]=z[8]*z[70];
    z[60]=z[70] + n<T>(355,24)*z[69] - n<T>(1591,144)*z[64] + z[65] - z[59] - n<T>(1,2)*z[60]
     - n<T>(3365,432)*z[22]
     - 2*z[36]
     + n<T>(989,36)*z[33];
    z[60]=z[8]*z[60];
    z[65]=n<T>(1651,48)*z[22] + n<T>(827,4)*z[17] - 188*z[16];
    z[70]=n<T>(1025,3)*z[3] - n<T>(959,2)*z[10];
    z[70]=n<T>(1,2)*z[70] + n<T>(211,3)*z[8];
    z[70]=z[8]*z[70];
    z[71]=n<T>(827,48)*z[8] + n<T>(959,16)*z[10] - 56*z[4] - n<T>(463,6)*z[3];
    z[71]=z[5]*z[71];
    z[65]=n<T>(1,3)*z[71] + n<T>(1,6)*z[70] + n<T>(129,16)*z[69] - n<T>(1195,144)*z[64]
    + n<T>(1,9)*z[65] + n<T>(67,8)*z[67];
    z[65]=z[5]*z[65];
    z[67]=z[51] - z[54];
    z[69]=z[48] - z[52];
    z[70]=z[47] - z[50];
    z[71]=z[27] - z[41];
    z[67]= - n<T>(2795,72)*z[24] - n<T>(3241,216)*z[25] + n<T>(959,9)*z[29] + n<T>(28157,144)*z[30]
     + n<T>(21,2)*z[37] - n<T>(21,4)*z[38]
     + n<T>(65,4)*z[39]
     + n<T>(211,9)*
    z[40] - z[42] - n<T>(3647,72)*z[43] - n<T>(129,8)*z[44] + n<T>(173,8)*z[45] - n<T>(43,2)*z[46]
     - z[49]
     - z[53]
     + n<T>(2107,36)*z[71]
     + n<T>(7,2)*z[70]
     + n<T>(3,2)*
    z[69] + n<T>(1,2)*z[67];
    z[61]=n<T>(271,72)*z[10] - n<T>(55,2)*z[3] - 5*z[13] - z[61];
    z[61]=z[10]*z[61];
    z[61]=z[61] + 5*z[34] + n<T>(133,4)*z[32];
    z[61]= - n<T>(41,8)*z[64] - n<T>(7,4)*z[68] + z[62] - n<T>(7835,288)*z[22] + 3*
    z[18] + n<T>(1,2)*z[61];
    z[61]=z[10]*z[61];
    z[62]= - z[15] + n<T>(31,8)*z[4];
    z[62]=z[62]*z[66];
    z[59]=z[62] + z[59] - n<T>(1235,288)*z[22] - n<T>(1,8)*z[31] + z[63];
    z[59]=z[4]*z[59];
    z[62]=z[17] + z[33];
    z[62]=169*z[32] + n<T>(1195,9)*z[62];
    z[62]=n<T>(1,2)*z[62] + n<T>(71,9)*z[22];
    z[62]=n<T>(1,4)*z[62] + n<T>(254,9)*z[64];
    z[62]=z[3]*z[62];
    z[55]=z[55] - n<T>(9,2)*z[12];
    z[55]=n<T>(827,36)*z[9] + n<T>(1,2)*z[55] + z[11];
    z[55]=z[22]*z[55];
    z[63]=z[35] - n<T>(83,24)*z[22];
    z[63]=z[13]*z[63];
    z[64]= - z[21] + n<T>(7,24)*z[22];
    z[64]=z[15]*z[64];
    z[66]=z[13] + z[6];
    z[66]=z[6]*z[66];
    z[66]=n<T>(1,2)*z[66] + z[35] + n<T>(1,6)*z[22];
    z[66]=z[6]*z[66];

    r += z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62]
       + z[63] + z[64] + z[65] + z[66] + n<T>(1,2)*z[67];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf365(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf365(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
