#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1169(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[61];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[4];
    z[9]=d[16];
    z[10]=d[12];
    z[11]=d[17];
    z[12]=d[18];
    z[13]=e[0];
    z[14]=e[4];
    z[15]=e[5];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=d[1];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[31];
    z[27]=d[8];
    z[28]=e[6];
    z[29]=e[11];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[5];
    z[34]=f[11];
    z[35]=f[17];
    z[36]=f[18];
    z[37]=f[31];
    z[38]=f[32];
    z[39]=f[33];
    z[40]=f[36];
    z[41]=f[37];
    z[42]=f[39];
    z[43]=f[43];
    z[44]=z[2] + z[6];
    z[45]=z[1]*i;
    z[46]= - z[45]*z[44];
    z[47]=n<T>(1,2)*z[7];
    z[48]=z[47] - z[45];
    z[49]=z[48]*z[47];
    z[50]=n<T>(1,2)*z[4];
    z[51]=z[50] + 3*z[45] - z[7];
    z[50]=z[51]*z[50];
    z[51]=z[47] + z[45];
    z[44]= - z[3] - n<T>(3,2)*z[4] + z[51] + z[44];
    z[52]=n<T>(1,2)*z[3];
    z[44]=z[44]*z[52];
    z[44]=z[44] + z[50] + z[49] + z[46];
    z[44]=z[3]*z[44];
    z[46]=z[8] + z[3];
    z[49]=z[45]*z[46];
    z[49]=z[19] + z[49];
    z[49]=z[9]*z[49];
    z[46]=z[45] - z[46];
    z[46]=z[14]*z[46];
    z[44]=z[44] + z[49] + z[46] - z[30];
    z[46]=z[48]*z[7];
    z[49]=z[5] - z[6];
    z[49]=n<T>(3,4)*z[2] - n<T>(3,2)*z[51] - n<T>(29,11)*z[49];
    z[49]=z[2]*z[49];
    z[50]=z[45] - z[7];
    z[51]=83*z[50] + n<T>(199,2)*z[6];
    z[51]=z[6]*z[51];
    z[53]=n<T>(83,22)*z[5] - n<T>(3,2)*z[50] - n<T>(58,11)*z[6];
    z[53]=z[4]*z[53];
    z[49]=z[49] + z[53] - 3*z[46] + n<T>(1,22)*z[51];
    z[51]=n<T>(1,11)*z[5];
    z[53]= - n<T>(199,10)*z[5] + n<T>(58,5)*z[6] - 5*z[45] + n<T>(83,10)*z[7];
    z[53]=z[53]*z[51];
    z[49]=z[53] + n<T>(1,5)*z[49];
    z[49]=z[2]*z[49];
    z[53]=n<T>(1,2)*z[2];
    z[52]= - z[52] + z[45] - z[53];
    z[52]=z[13]*z[52];
    z[48]=z[53] + z[48];
    z[48]=z[17]*z[48];
    z[48]=z[52] + z[48];
    z[52]=n<T>(381,11)*z[4] - n<T>(73,2) + n<T>(471,11)*z[5];
    z[53]=z[45]*z[52];
    z[53]=n<T>(381,11)*z[19] + z[53];
    z[53]=z[10]*z[53];
    z[52]=n<T>(381,11)*z[45] - z[52];
    z[52]=z[15]*z[52];
    z[52]=z[53] + z[52];
    z[53]= - 13 - n<T>(1139,22)*z[6];
    z[53]=z[6]*z[53];
    z[53]= - n<T>(1783,22)*z[46] + z[53];
    z[54]=n<T>(1,2)*z[5];
    z[55]= - n<T>(57,11)*z[7] + n<T>(33,5) + n<T>(65,11)*z[45];
    z[55]=n<T>(3749,165)*z[5] + n<T>(3,2)*z[55] - n<T>(619,55)*z[6];
    z[55]=z[55]*z[54];
    z[53]=n<T>(1,5)*z[53] + z[55];
    z[53]=z[53]*z[54];
    z[55]= - n<T>(263,55)*z[45] + 9*z[7];
    z[47]=z[55]*z[47];
    z[55]=339*z[45] + 329*z[7];
    z[55]=n<T>(1,4)*z[55] + n<T>(217,3)*z[6];
    z[55]=z[6]*z[55];
    z[47]=z[47] + n<T>(1,55)*z[55];
    z[47]=z[6]*z[47];
    z[55]=npow(z[7],2);
    z[47]=z[53] - z[55] + z[47];
    z[53]= - n<T>(431,2)*z[45] - 83*z[7];
    z[53]=92*z[5] + n<T>(1,2)*z[53] - 58*z[6];
    z[53]=z[53]*z[51];
    z[55]=n<T>(97,2)*z[6] - 58*z[45] + n<T>(83,2)*z[7];
    z[55]=z[6]*z[55];
    z[53]=z[53] + n<T>(9,4)*z[46] + n<T>(1,11)*z[55];
    z[55]=n<T>(149,22)*z[6] - n<T>(3,4)*z[7] + n<T>(73,8) + n<T>(87,11)*z[45];
    z[51]= - n<T>(37,44)*z[4] + n<T>(1,10)*z[55] - z[51];
    z[51]=z[4]*z[51];
    z[51]=n<T>(1,5)*z[53] + z[51];
    z[51]=z[4]*z[51];
    z[53]=z[6]*z[50];
    z[46]= - n<T>(197,2)*z[46] + 329*z[53];
    z[53]= - z[54] - z[50];
    z[53]=z[5]*z[53];
    z[46]=n<T>(1,5)*z[46] + n<T>(171,2)*z[53];
    z[53]=z[3] - z[2];
    z[54]= - n<T>(987,2)*z[5] + n<T>(197,2)*z[50] + 263*z[6];
    z[53]=n<T>(1,22)*z[54] + 3*z[53];
    z[53]=n<T>(1,5)*z[53] + n<T>(57,22)*z[8];
    z[54]=n<T>(1,2)*z[8];
    z[53]=z[53]*z[54];
    z[55]=z[3]*z[45];
    z[46]=z[53] + n<T>(1,22)*z[46] - n<T>(3,5)*z[55];
    z[46]=z[46]*z[54];
    z[53]= - z[7] - z[6];
    z[53]=z[45]*z[53];
    z[53]= - z[19] + z[53];
    z[53]=z[11]*z[53];
    z[50]= - z[6] + z[50];
    z[50]=z[18]*z[50];
    z[50]=z[53] + z[50];
    z[53]=z[41] + z[33] + z[31] - z[25];
    z[54]=z[34] - z[32];
    z[55]=n<T>(3,10)*z[24] - n<T>(1003,2640)*z[21];
    z[55]=i*z[55];
    z[56]= - n<T>(185,44)*z[7] - n<T>(41,5)*z[6];
    z[56]=n<T>(1,3)*z[56] + n<T>(57,5)*z[5];
    z[56]=n<T>(493,1320)*z[8] + n<T>(1,8)*z[3] - n<T>(13,40)*z[2] + n<T>(1,4)*z[56]
     + n<T>(18,55)*z[4];
    z[56]=z[19]*z[56];
    z[57]=n<T>(1567,5)*z[5] + 259*z[6] - n<T>(281,5)*z[45] + 73*z[7];
    z[57]= - n<T>(199,5)*z[2] + n<T>(1,2)*z[57] + n<T>(157,5)*z[4];
    z[57]=z[16]*z[57];
    z[58]= - z[4] + z[7] - z[6];
    z[58]=z[27]*z[58];
    z[59]=z[27] - z[5] + n<T>(1,40)*z[19];
    z[59]=z[20]*z[59];
    z[60]=z[6] + z[8];
    z[60]=z[28]*z[60];
    z[45]=z[12]*z[45];

    r += n<T>(47,44)*z[22] + n<T>(7,40)*z[23] + n<T>(64,55)*z[26] - z[29] + n<T>(3,40)*
      z[35] - n<T>(1,10)*z[36] - n<T>(2711,880)*z[37] - n<T>(213,110)*z[38] - n<T>(137,22)*z[39]
     + n<T>(197,880)*z[40] - n<T>(987,880)*z[42]
     + n<T>(58,55)*z[43]
     + n<T>(3,10)*z[44]
     + z[45]
     + z[46]
     + n<T>(1,2)*z[47]
     + n<T>(3,5)*z[48]
     + z[49]
     +  n<T>(727,220)*z[50] + z[51] + n<T>(1,20)*z[52] + n<T>(3,20)*z[53] + n<T>(9,40)*
      z[54] + z[55] + z[56] + n<T>(1,22)*z[57] + z[58] + z[59] + n<T>(329,220)*
      z[60];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1169(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1169(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
