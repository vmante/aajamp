#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf383(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[84];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[11];
    z[11]=d[16];
    z[12]=d[5];
    z[13]=d[18];
    z[14]=d[6];
    z[15]=d[9];
    z[16]=d[17];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[17];
    z[27]=c[19];
    z[28]=c[21];
    z[29]=c[23];
    z[30]=c[25];
    z[31]=c[26];
    z[32]=c[28];
    z[33]=c[31];
    z[34]=e[0];
    z[35]=e[3];
    z[36]=e[10];
    z[37]=e[6];
    z[38]=e[11];
    z[39]=e[14];
    z[40]=f[0];
    z[41]=f[1];
    z[42]=f[3];
    z[43]=f[4];
    z[44]=f[5];
    z[45]=f[6];
    z[46]=f[11];
    z[47]=f[12];
    z[48]=f[14];
    z[49]=f[16];
    z[50]=f[17];
    z[51]=f[18];
    z[52]=f[31];
    z[53]=f[36];
    z[54]=f[37];
    z[55]=f[39];
    z[56]=f[51];
    z[57]=f[55];
    z[58]=f[56];
    z[59]=f[58];
    z[60]=f[109];
    z[61]=npow(z[14],2);
    z[62]= - n<T>(127,2)*z[61] + 115*z[21];
    z[63]=n<T>(167,4)*z[19];
    z[64]=n<T>(20063,24)*z[17] - n<T>(1239,4)*z[18] + z[63] + n<T>(39509,24)*z[20]
    + 9*z[22] - z[62];
    z[65]=3*z[15];
    z[66]=z[14] - z[15];
    z[66]=z[66]*z[65];
    z[67]=n<T>(1,2)*z[4];
    z[68]=n<T>(167,4)*z[11] + 9*z[10];
    z[69]= - n<T>(19,8)*z[4] + z[68];
    z[69]=z[69]*z[67];
    z[70]=z[4] - z[11];
    z[71]=n<T>(4309,16)*z[7];
    z[70]= - n<T>(2485,32)*z[9] + z[71] - 2*z[13] - 19*z[70];
    z[70]=z[9]*z[70];
    z[72]=3*z[12];
    z[73]=115*z[16];
    z[74]=z[73] - 127*z[14];
    z[74]=n<T>(1149,8)*z[6] + n<T>(31271,48)*z[5] - n<T>(1021,4)*z[9] - n<T>(42341,48)*
    z[2] + n<T>(83,8)*z[7] + n<T>(117,8)*z[3] + n<T>(1421,8)*z[4] + n<T>(1,2)*z[74]
     - 
    z[72];
    z[74]=z[6]*z[74];
    z[75]=npow(z[12],2);
    z[76]=3511*z[8] - n<T>(3631,2)*z[3];
    z[76]=z[3]*z[76];
    z[77]=n<T>(25,2)*z[7] + z[65] - n<T>(923,4)*z[3];
    z[77]=z[7]*z[77];
    z[78]=57*z[4] + 25*z[3];
    z[78]=7*z[78] + 127*z[7];
    z[78]=n<T>(1,8)*z[78] + n<T>(1799,3)*z[2];
    z[78]=z[2]*z[78];
    z[79]=n<T>(413,8)*z[8] + z[12];
    z[79]= - n<T>(4165,16)*z[9] - n<T>(20771,48)*z[2] - n<T>(1193,16)*z[7] + n<T>(767,4)
   *z[3] + 3*z[79] - n<T>(467,2)*z[4];
    z[79]=z[5]*z[79];
    z[64]=z[74] + z[79] + z[70] + z[78] + z[77] + n<T>(1,16)*z[76] + z[69]
    + z[66] + n<T>(1,2)*z[64] - 27*z[75];
    z[64]=z[1]*z[64];
    z[66]= - n<T>(15025,4)*z[31] - 1778*z[29];
    z[64]=z[64] - n<T>(103,96)*z[24] + n<T>(17,4)*z[26] + n<T>(1,3)*z[66] + 11*z[28]
   ;
    z[64]=i*z[64];
    z[62]=n<T>(117,2)*z[75] + n<T>(54329,144)*z[23] - n<T>(2267,3)*z[20] + z[62];
    z[66]=14*z[14];
    z[69]= - z[66] + z[65];
    z[70]=n<T>(175,8)*z[3];
    z[69]=n<T>(21079,96)*z[2] - n<T>(105,8)*z[7] + 2*z[69] - z[70];
    z[69]=z[2]*z[69];
    z[74]=n<T>(3,2)*z[12];
    z[76]=z[74] - z[65];
    z[77]=n<T>(11,8)*z[6] - n<T>(31943,96)*z[5] + n<T>(1037,8)*z[9] + n<T>(955,3)*z[2]
    + 2*z[3] - n<T>(4267,32)*z[4] + 17*z[14] + z[76];
    z[77]=z[6]*z[77];
    z[78]=npow(z[3],2);
    z[79]=npow(z[4],2);
    z[80]= - z[14]*z[65];
    z[81]=117*z[3] - 29*z[7];
    z[81]=z[7]*z[81];
    z[82]= - n<T>(1009,8)*z[9] - z[72] - 2*z[2];
    z[82]=z[9]*z[82];
    z[83]= - n<T>(2329,12)*z[5] - n<T>(4649,6)*z[2] + 11*z[7] + 1219*z[4] + 13*
    z[3];
    z[83]=z[5]*z[83];
    z[62]=z[77] + n<T>(1,4)*z[83] + z[82] + z[69] + n<T>(1,8)*z[81] + 2*z[78] - 
   n<T>(3451,32)*z[79] + n<T>(1,2)*z[62] + z[80];
    z[62]=z[6]*z[62];
    z[61]=z[61] + z[37];
    z[69]= - z[12]*z[65];
    z[77]=npow(z[7],2);
    z[80]=npow(z[2],2);
    z[81]=n<T>(271,8)*z[4] - 121*z[14] + z[72];
    z[81]= - n<T>(229,96)*z[9] - n<T>(223,16)*z[2] + n<T>(827,8)*z[7] + n<T>(1,2)*z[81]
    - 127*z[3];
    z[81]=z[9]*z[81];
    z[61]=z[81] + n<T>(11,2)*z[80] + n<T>(705,8)*z[77] - n<T>(1029,8)*z[78] + z[69]
    - n<T>(117,4)*z[75] - n<T>(17261,64)*z[23] - 19*z[19] + n<T>(2931,16)*z[35] + 2
   *z[38] + n<T>(121,2)*z[61];
    z[61]=z[9]*z[61];
    z[69]=n<T>(6425,32)*z[9] - 9*z[2] + z[72] - z[71];
    z[69]=z[9]*z[69];
    z[71]= - 6*z[4] - n<T>(3551,32)*z[3];
    z[71]=z[3]*z[71];
    z[77]=777*z[3] - n<T>(83,2)*z[7];
    z[77]=z[7]*z[77];
    z[72]=n<T>(10639,96)*z[2] - z[72] - n<T>(11,4)*z[7];
    z[72]=z[2]*z[72];
    z[78]=n<T>(4309,4)*z[9] + n<T>(9715,6)*z[2] + n<T>(1193,4)*z[7] - 1231*z[4] - 
   n<T>(2751,2)*z[3];
    z[78]=z[5]*z[78];
    z[69]=n<T>(1,8)*z[78] + z[69] + z[72] + n<T>(1,4)*z[77] + z[71] + n<T>(473,4)*
    z[79] + n<T>(7373,288)*z[23] + n<T>(1239,8)*z[18] - n<T>(556,3)*z[17];
    z[69]=z[5]*z[69];
    z[66]= - n<T>(14881,48)*z[2] + z[66] + z[76];
    z[66]=z[2]*z[66];
    z[71]=n<T>(3565,18)*z[23] - n<T>(11167,3)*z[17] + 9*z[34] - n<T>(21373,3)*z[20];
    z[72]=npow(z[15],2);
    z[70]= - 6*z[15] + z[70];
    z[70]=z[7]*z[70];
    z[66]=z[66] + z[70] - n<T>(399,16)*z[79] + n<T>(1,16)*z[71] + 3*z[72];
    z[66]=z[2]*z[66];
    z[70]=n<T>(9,2)*z[75];
    z[71]= - n<T>(1,8)*z[34] - z[22];
    z[63]= - n<T>(103,16)*z[79] - z[70] - n<T>(9887,96)*z[23] + 9*z[71] - z[63];
    z[63]=z[63]*z[67];
    z[67]=n<T>(1397,3)*z[23] + 3511*z[18] + 113*z[36] + 5153*z[35];
    z[71]=3*z[4];
    z[72]=z[71] + n<T>(12661,48)*z[3];
    z[72]=z[3]*z[72];
    z[67]=z[72] + n<T>(1,16)*z[67] + 3*z[79];
    z[67]=z[3]*z[67];
    z[71]= - n<T>(707,4)*z[3] + 13*z[15] + z[71];
    z[71]=z[7]*z[71];
    z[70]=z[71] + z[70] - n<T>(1573,48)*z[23] - 25*z[39] + n<T>(643,4)*z[36];
    z[71]=z[4] + z[14] - 2*z[15];
    z[65]=z[65]*z[71];
    z[71]=z[15] - n<T>(2359,32)*z[3];
    z[71]=z[3]*z[71];
    z[65]=z[71] + z[65] + n<T>(1,2)*z[70];
    z[65]=z[7]*z[65];
    z[68]= - n<T>(3149,24)*z[14] + n<T>(1239,4)*z[8] + z[73] + z[68];
    z[68]=z[23]*z[68];
    z[68]=z[54] + z[68] + z[53];
    z[70]=z[56] - z[59];
    z[71]=z[52] - z[55];
    z[72]=z[74] + n<T>(2,3)*z[15];
    z[72]=z[15]*z[72];
    z[72]=n<T>(21,16)*z[23] + z[72];
    z[72]=z[15]*z[72];

    r +=  - n<T>(7519,32)*z[25] - n<T>(4973,144)*z[27] + n<T>(1745,6)*z[30] + n<T>(14893,24)*z[32]
     + n<T>(179059,192)*z[33] - n<T>(311,8)*z[40]
     + n<T>(223,16)*z[41]
       + n<T>(455,4)*z[42] - n<T>(85,8)*z[43] - n<T>(856,3)*z[44] - n<T>(5,2)*z[45]
     - 
      n<T>(30983,96)*z[46] - n<T>(6329,32)*z[47] + 2*z[48] + n<T>(1819,32)*z[49]
     - 
      n<T>(1421,16)*z[50] - n<T>(311,24)*z[51] - n<T>(9,2)*z[57] - n<T>(3,2)*z[58]
     +  z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + n<T>(1,2)*z[68]
     + z[69] - n<T>(9,4)*z[70]
     + n<T>(117,4)*z[71]
     + z[72];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf383(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf383(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
