#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf163(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[55];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=d[4];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[31];
    z[25]=e[0];
    z[26]=e[3];
    z[27]=e[10];
    z[28]=e[14];
    z[29]=f[0];
    z[30]=f[1];
    z[31]=f[3];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[6];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[18];
    z[39]=f[56];
    z[40]=13*z[5];
    z[41]= - z[4] + n<T>(1,4)*z[5];
    z[41]=z[41]*z[40];
    z[42]=n<T>(1,2)*z[3];
    z[43]=z[1]*i;
    z[44]=z[43] - z[4];
    z[45]= - n<T>(5,2)*z[3] - z[2] + n<T>(13,2)*z[5] - z[44];
    z[45]=z[45]*z[42];
    z[46]=npow(z[11],2);
    z[47]= - z[43] + n<T>(13,2)*z[4];
    z[47]=z[4]*z[47];
    z[48]=z[2]*z[43];
    z[41]=z[45] + z[48] + n<T>(1,2)*z[46] + z[47] + z[41];
    z[41]=z[41]*z[42];
    z[42]=25*z[4];
    z[45]= - z[44]*z[42];
    z[47]=17*z[43] + 29*z[4];
    z[47]=n<T>(1,4)*z[47] - 17*z[6];
    z[47]=z[6]*z[47];
    z[45]=z[45] + z[47];
    z[45]=z[6]*z[45];
    z[47]=n<T>(29,2)*z[6];
    z[42]= - z[42] - z[47];
    z[42]=z[43]*z[42];
    z[42]= - 25*z[16] + z[42];
    z[42]=z[9]*z[42];
    z[48]=25*z[44];
    z[47]=z[48] - z[47];
    z[47]=z[13]*z[47];
    z[49]=n<T>(43,2)*z[2] - n<T>(89,2)*z[43] + 23*z[5];
    z[49]=z[15]*z[49];
    z[42]= - z[38] + z[45] + z[42] + z[47] + z[49];
    z[45]=13*z[4];
    z[47]= - 89*z[43] + z[45];
    z[47]=z[4]*z[47];
    z[49]=n<T>(1,2)*z[6];
    z[50]= - z[43] + z[49];
    z[50]=z[6]*z[50];
    z[49]= - n<T>(2,3)*z[5] + z[49] + z[43] + n<T>(89,24)*z[4];
    z[49]=z[5]*z[49];
    z[47]=z[49] + n<T>(1,12)*z[47] + z[50];
    z[47]=z[5]*z[47];
    z[49]= - z[43] + n<T>(1,2)*z[4];
    z[50]=n<T>(25,2)*z[4];
    z[49]=z[49]*z[50];
    z[50]=npow(z[6],2);
    z[50]= - z[49] - z[50];
    z[51]=n<T>(1,2)*z[5];
    z[52]= - z[43] + z[51];
    z[52]=z[5]*z[52];
    z[53]=9*z[43] - 5*z[4];
    z[51]=n<T>(3,8)*z[11] - z[51] + n<T>(1,8)*z[53] + z[6];
    z[51]=z[11]*z[51];
    z[50]=z[51] + n<T>(1,2)*z[50] + z[52];
    z[50]=z[11]*z[50];
    z[51]= - z[45] + n<T>(95,4)*z[43];
    z[52]=n<T>(43,4)*z[2] - z[51];
    z[52]=z[12]*z[52];
    z[53]=n<T>(23,16)*z[3] + n<T>(67,24)*z[7] - n<T>(109,24)*z[2] + n<T>(107,16)*z[11]
    + n<T>(31,24)*z[5] - n<T>(17,8)*z[4] + z[6];
    z[53]=z[16]*z[53];
    z[54]= - n<T>(29,4)*z[6] - 11*z[7];
    z[54]=z[27]*z[54];
    z[52]=z[54] + z[53] + z[52];
    z[48]=z[48] + n<T>(11,4)*z[6];
    z[48]=z[6]*z[48];
    z[48]= - z[49] + z[48];
    z[49]=3*z[11];
    z[53]= - n<T>(25,2)*z[44] - z[49];
    z[53]=z[11]*z[53];
    z[48]=n<T>(1,3)*z[48] + z[53];
    z[44]=8*z[44] + 11*z[6];
    z[44]=n<T>(1,3)*z[44] - n<T>(3,2)*z[11];
    z[44]=z[7]*z[44];
    z[44]= - z[28] + n<T>(1,2)*z[48] + z[44];
    z[44]=z[7]*z[44];
    z[40]= - z[40] + n<T>(89,4)*z[43] + z[45];
    z[40]=z[5]*z[40];
    z[45]=z[4]*z[51];
    z[40]=z[45] + z[40];
    z[45]=23*z[2] - n<T>(37,4)*z[5] - 49*z[43] - n<T>(43,4)*z[4];
    z[45]=z[2]*z[45];
    z[40]=n<T>(1,6)*z[45] + n<T>(1,3)*z[40] - n<T>(1,4)*z[46];
    z[40]=z[2]*z[40];
    z[45]=z[3]*z[43];
    z[45]=z[45] + z[16];
    z[45]=z[10]*z[45];
    z[46]=z[43] - z[3];
    z[46]=z[14]*z[46];
    z[45]=z[29] - z[45] - z[46] + z[39] + z[34];
    z[46]= - z[2] + z[3];
    z[46]=z[25]*z[46];
    z[48]= - z[6] - z[49];
    z[48]=z[26]*z[48];
    z[46]=z[46] + z[48] + z[31] + z[30];
    z[48]=z[33] - z[21];
    z[49]=n<T>(46,3)*z[22] + n<T>(49,6)*z[20] - n<T>(31,72)*z[17];
    z[49]=i*z[49];
    z[43]=z[7] + z[43] - z[5];
    z[43]=z[6]*z[43];
    z[43]=n<T>(1,6)*z[16] + z[43];
    z[43]=z[8]*z[43];

    r += n<T>(73,24)*z[18] + n<T>(67,72)*z[19] - n<T>(23,3)*z[23] - n<T>(189,16)*z[24]
       - n<T>(8,3)*z[32] + n<T>(89,24)*z[35] + n<T>(5,8)*z[36] + n<T>(1,8)*z[37]
     +  z[40] + z[41] + n<T>(1,6)*z[42] + z[43] + z[44] - n<T>(1,2)*z[45] + n<T>(1,4)
      *z[46] + z[47] + n<T>(49,12)*z[48] + z[49] + z[50] + n<T>(1,3)*z[52];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf163(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf163(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
