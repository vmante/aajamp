#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf651(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[65];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[5];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[8];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[3];
    z[28]=e[10];
    z[29]=e[6];
    z[30]=e[4];
    z[31]=e[12];
    z[32]=f[0];
    z[33]=f[1];
    z[34]=f[3];
    z[35]=f[4];
    z[36]=f[5];
    z[37]=f[11];
    z[38]=f[12];
    z[39]=f[14];
    z[40]=f[16];
    z[41]=f[17];
    z[42]=f[18];
    z[43]=f[31];
    z[44]=f[36];
    z[45]=f[39];
    z[46]=f[51];
    z[47]=f[55];
    z[48]=f[58];
    z[49]=n<T>(1,3016)*z[5];
    z[50]= - n<T>(74507,3)*z[3] + n<T>(73753,2)*z[10];
    z[51]= - n<T>(72245,12)*z[5] + z[50];
    z[51]=z[51]*z[49];
    z[52]= - 3*z[2] + n<T>(73753,6032)*z[10];
    z[53]=n<T>(3,2)*z[7];
    z[54]= - z[53] - n<T>(140105,18096)*z[5] + n<T>(83555,4524)*z[3] - z[9] + n<T>(11,2)*z[4]
     - z[52];
    z[54]=z[7]*z[54];
    z[55]=npow(z[2],2);
    z[56]=npow(z[4],2);
    z[57]=npow(z[10],2);
    z[58]=npow(z[3],2);
    z[59]=npow(z[9],2);
    z[60]= - n<T>(97127,8)*z[28] + n<T>(19351,3)*z[18];
    z[51]=n<T>(1,2)*z[54] + z[51] - n<T>(7,8)*z[55] + n<T>(60935,18096)*z[58] - 
   n<T>(64705,12064)*z[57] - n<T>(5,2)*z[56] + n<T>(1,1131)*z[60] + n<T>(1,4)*z[59];
    z[51]=z[7]*z[51];
    z[54]= - n<T>(101929,2)*z[15] - 110699*z[17] + n<T>(72245,2)*z[16];
    z[60]=z[9] - n<T>(52641,6032)*z[10];
    z[60]=z[10]*z[60];
    z[61]=9*z[4] - n<T>(2467,1508)*z[3];
    z[61]=3*z[61] - n<T>(210505,4524)*z[2];
    z[61]=z[2]*z[61];
    z[54]=z[61] + z[60] + n<T>(1,2262)*z[54] - 31*z[14];
    z[50]=n<T>(101929,3)*z[2] - n<T>(72245,3)*z[8] - n<T>(893,2)*z[4] + z[50];
    z[49]=z[50]*z[49];
    z[50]=z[6] + z[9];
    z[60]=3*z[3];
    z[61]= - n<T>(29,4)*z[10] - z[60] + n<T>(201179,18096)*z[5];
    z[50]=n<T>(26261,1131)*z[2] - n<T>(80539,6032)*z[4] - z[61] - 3*z[50];
    z[50]=z[6]*z[50];
    z[62]= - z[13] + z[12];
    z[62]=z[11]*z[62];
    z[63]= - z[9] - 2*z[4];
    z[63]=z[4]*z[63];
    z[64]= - z[8] - n<T>(1,4)*z[3];
    z[64]=z[3]*z[64];
    z[52]=n<T>(13433,36192)*z[7] + n<T>(72245,18096)*z[5] + n<T>(60935,9048)*z[3]
     + 
   5*z[4] - z[52];
    z[52]=z[7]*z[52];
    z[49]=z[50] + z[52] + z[49] + n<T>(139,4524)*z[64] + z[63] + n<T>(17,8)*
    z[59] + z[62] + n<T>(1,2)*z[54];
    z[49]=z[1]*z[49];
    z[50]= - n<T>(56689,6)*z[19] + 463571*z[24] + n<T>(427379,2)*z[22];
    z[49]=n<T>(1,9048)*z[50] + z[49];
    z[49]=i*z[49];
    z[50]=124271*z[17] - n<T>(354995,12)*z[18];
    z[50]=n<T>(80539,1508)*z[56] + n<T>(1,1131)*z[50] - 15*z[59];
    z[52]= - z[53] + z[60] - n<T>(5,2)*z[2];
    z[52]=z[7]*z[52];
    z[53]=z[9] + n<T>(30365,6032)*z[4];
    z[53]=n<T>(17,12)*z[6] + n<T>(5,4)*z[7] - n<T>(110699,4524)*z[2] + 3*z[53] + 
    z[61];
    z[53]=z[6]*z[53];
    z[54]= - z[9] + 9*z[10];
    z[54]=z[10]*z[54];
    z[60]=n<T>(92603,12)*z[5] - n<T>(91095,2)*z[4] + n<T>(90341,3)*z[2];
    z[60]=z[5]*z[60];
    z[50]=z[53] + z[52] + n<T>(1,1508)*z[60] - n<T>(79031,9048)*z[55] + n<T>(3,2)*
    z[58] + n<T>(1,4)*z[50] + z[54];
    z[50]=z[6]*z[50];
    z[52]=z[57] - z[27];
    z[53]=z[4] - n<T>(136613,9048)*z[3];
    z[53]=z[3]*z[53];
    z[52]=z[53] + n<T>(27283,9048)*z[18] - n<T>(139,2262)*z[16] - n<T>(69983,4524)*
    z[28] + 11*z[52];
    z[52]=z[3]*z[52];
    z[50]=z[39] + z[52] + z[50];
    z[52]= - 72245*z[16] + 92603*z[15];
    z[52]=n<T>(1,4)*z[52] - n<T>(10303,3)*z[18];
    z[52]= - n<T>(4663,12)*z[55] + n<T>(139,12)*z[58] - n<T>(1647,16)*z[57] + n<T>(1,3)*
    z[52] + n<T>(893,16)*z[56];
    z[53]=91095*z[4] - 73753*z[10];
    z[53]= - n<T>(11434,3)*z[2] + n<T>(1,32)*z[53] + n<T>(9172,3)*z[3];
    z[53]=z[5]*z[53];
    z[52]=n<T>(1,2)*z[52] + z[53];
    z[52]=z[5]*z[52];
    z[53]=z[56] - z[14];
    z[54]=n<T>(97127,2)*z[17] + 4663*z[15];
    z[53]= - n<T>(39149,13572)*z[18] + n<T>(1,1131)*z[54] - 27*z[53];
    z[54]=7401*z[3] + n<T>(169789,3)*z[2];
    z[54]=z[2]*z[54];
    z[53]=n<T>(1,6032)*z[54] - n<T>(7401,6032)*z[58] + n<T>(1,4)*z[53] + z[57];
    z[53]=z[2]*z[53];
    z[54]= - z[59] + 35*z[14] - n<T>(41609,9048)*z[18];
    z[55]=n<T>(1,2)*z[9] + n<T>(2,3)*z[4];
    z[55]=z[4]*z[55];
    z[54]=n<T>(1,4)*z[54] + z[55];
    z[54]=z[4]*z[54];
    z[55]=n<T>(15,4)*z[59] + n<T>(131057,9048)*z[18] - 5*z[29] - 13*z[27];
    z[56]= - n<T>(11,12)*z[10] - z[9] - z[4];
    z[56]=z[10]*z[56];
    z[55]=n<T>(1,2)*z[55] + z[56];
    z[55]=z[10]*z[55];
    z[56]=z[23] - z[36];
    z[57]= - n<T>(25,12)*z[9] - n<T>(72245,9048)*z[8];
    z[57]=z[18]*z[57];
    z[58]= - z[45] + z[43] + z[44];
    z[58]=z[46] - z[48] - z[47] - n<T>(5,2)*z[58];
    z[59]= - z[31] + z[30];
    z[59]=z[11]*z[59];

    r += n<T>(72523,18096)*z[20] + n<T>(210227,217152)*z[21] - n<T>(463571,18096)*
      z[25] - n<T>(112419,12064)*z[26] + 2*z[32] - z[33] + n<T>(893,12064)*
      z[34] - n<T>(140105,36192)*z[35] + n<T>(201179,36192)*z[37] + n<T>(1647,12064)
      *z[38] - n<T>(64705,12064)*z[40] + n<T>(80539,12064)*z[41] + n<T>(2,3)*z[42]
       + z[49] + n<T>(1,2)*z[50] + z[51] + n<T>(1,377)*z[52] + z[53] + z[54] + 
      z[55] - n<T>(427379,36192)*z[56] + z[57] + n<T>(1,4)*z[58] + z[59];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf651(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf651(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
