#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf330(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[64];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=d[6];
    z[12]=d[5];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=d[9];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[21];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[0];
    z[31]=e[6];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[7];
    z[35]=f[0];
    z[36]=f[1];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[11];
    z[41]=f[12];
    z[42]=f[16];
    z[43]=f[17];
    z[44]=f[18];
    z[45]=f[31];
    z[46]=z[4] - z[10];
    z[47]=n<T>(67,3)*z[8];
    z[48]=n<T>(1,2)*z[11];
    z[46]= - n<T>(11,4)*z[6] + z[47] - z[48] + 2*z[46];
    z[46]=z[6]*z[46];
    z[49]=npow(z[11],2);
    z[49]=n<T>(1,4)*z[49];
    z[50]=npow(z[12],2);
    z[50]=n<T>(1,2)*z[50];
    z[51]= - z[18] + z[49] - z[50];
    z[52]=n<T>(1,2)*z[4];
    z[53]= - 3*z[10] - z[52];
    z[53]=z[53]*z[52];
    z[54]=npow(z[8],2);
    z[55]= - n<T>(47,2)*z[3] + 47*z[9] - 83*z[8];
    z[55]=z[3]*z[55];
    z[56]=n<T>(1,2)*z[3];
    z[57]=z[56] - 11*z[4] + n<T>(1,2)*z[8];
    z[57]=n<T>(985,36)*z[2] + n<T>(1,2)*z[57] + z[6];
    z[57]=z[2]*z[57];
    z[58]=n<T>(134,9)*z[9];
    z[59]= - n<T>(733,36)*z[2] - n<T>(67,3)*z[6] + n<T>(545,36)*z[3] - n<T>(259,36)*z[8]
    + z[58] + n<T>(1,4)*z[4];
    z[59]=z[5]*z[59];
    z[60]=z[3] - z[8];
    z[61]=n<T>(7,4)*z[7] + n<T>(353,18)*z[5] - n<T>(967,36)*z[2] - n<T>(5,2)*z[6]
     + 
    z[13] + n<T>(21,4)*z[4] + n<T>(3,4)*z[60];
    z[61]=z[7]*z[61];
    z[46]=z[61] + z[59] + z[57] + z[46] + n<T>(7,36)*z[55] - n<T>(541,72)*z[54]
    + z[53] + n<T>(715,36)*z[14] - n<T>(134,9)*z[15] - n<T>(3,2)*z[16] + n<T>(895,36)*
    z[17] + z[51];
    z[46]=z[1]*z[46];
    z[53]= - 787*z[27] - n<T>(931,2)*z[25];
    z[46]=z[46] + n<T>(223,216)*z[21] + n<T>(1,18)*z[53] - z[24];
    z[46]=i*z[46];
    z[53]=npow(z[4],2);
    z[55]=n<T>(503,24)*z[19] + 134*z[15] - n<T>(235,2)*z[14];
    z[57]=545*z[8] - n<T>(329,2)*z[3];
    z[57]=z[3]*z[57];
    z[47]= - z[47] + n<T>(15,4)*z[6];
    z[47]=z[6]*z[47];
    z[59]= - z[8] + n<T>(209,36)*z[2];
    z[59]=z[2]*z[59];
    z[61]= - n<T>(1063,3)*z[3] - 229*z[4] + n<T>(259,3)*z[8];
    z[61]=n<T>(931,12)*z[2] + n<T>(1,4)*z[61] + 67*z[6];
    z[61]=z[5]*z[61];
    z[47]=n<T>(1,6)*z[61] + n<T>(1,2)*z[59] + z[47] + n<T>(1,36)*z[57] + n<T>(595,72)*
    z[54] + n<T>(1,9)*z[55] - n<T>(1,8)*z[53];
    z[47]=z[5]*z[47];
    z[55]=z[3]*z[8];
    z[57]=z[55] - z[54];
    z[59]=n<T>(1,2)*z[6];
    z[61]=z[11] - 3*z[6];
    z[61]=z[61]*z[59];
    z[62]=n<T>(43,9)*z[2] - 2*z[11] - n<T>(1,4)*z[60];
    z[62]=z[2]*z[62];
    z[60]= - n<T>(61,9)*z[5] - n<T>(407,18)*z[2] + n<T>(229,6)*z[4] - z[60];
    z[60]=z[5]*z[60];
    z[63]= - n<T>(1,36)*z[7] - n<T>(371,36)*z[5] + n<T>(515,36)*z[2] + n<T>(5,4)*z[6]
     + 
    z[48] - n<T>(17,3)*z[4];
    z[63]=z[7]*z[63];
    z[51]=z[63] + n<T>(1,2)*z[60] + z[62] + z[61] - n<T>(13,2)*z[53] + n<T>(53,27)*
    z[19] - n<T>(533,36)*z[17] - z[31] - z[51] + n<T>(3,4)*z[57];
    z[51]=z[7]*z[51];
    z[57]=npow(z[3],2);
    z[60]=n<T>(175,12)*z[8] - z[11] - n<T>(7,2)*z[4];
    z[60]=n<T>(47,72)*z[6] + n<T>(1,2)*z[60] - 7*z[3];
    z[60]=z[6]*z[60];
    z[49]=z[60] - z[57] + n<T>(65,8)*z[54] + z[49] - n<T>(1747,144)*z[19] + n<T>(31,4)*z[32]
     + 2*z[16];
    z[49]=z[6]*z[49];
    z[55]=z[55] - z[30];
    z[57]=npow(z[6],2);
    z[59]= - n<T>(841,72)*z[2] + z[11] - z[59];
    z[59]=z[2]*z[59];
    z[55]=z[59] + n<T>(9,4)*z[57] + n<T>(11,4)*z[53] + n<T>(845,216)*z[19] - n<T>(245,36)
   *z[14] - n<T>(181,18)*z[17] + z[31] + n<T>(1,4)*z[55];
    z[55]=z[2]*z[55];
    z[57]=z[12] + n<T>(1,3)*z[11];
    z[57]=z[57]*z[48];
    z[50]=z[50] + z[34];
    z[57]=z[57] - n<T>(13,12)*z[19] - z[50];
    z[48]=z[57]*z[48];
    z[53]=n<T>(15,4)*z[53] - n<T>(217,72)*z[19] + n<T>(1,2)*z[30] + 3*z[16];
    z[52]=z[53]*z[52];
    z[53]= - 89*z[8] + n<T>(1033,4)*z[3];
    z[53]=z[3]*z[53];
    z[53]=n<T>(1,9)*z[53] - n<T>(563,18)*z[54] - n<T>(89,36)*z[19] + n<T>(329,18)*z[15]
    + n<T>(169,9)*z[33] + n<T>(33,2)*z[32];
    z[53]=z[53]*z[56];
    z[50]= - z[12]*z[50];
    z[50]=z[50] - n<T>(775,36)*z[22] - n<T>(1723,216)*z[23] + n<T>(967,36)*z[26]
     + 
   n<T>(805,18)*z[28] + n<T>(5233,72)*z[29] + 9*z[35] - n<T>(7,2)*z[36] - n<T>(1,4)*
    z[37] + n<T>(595,36)*z[38] - n<T>(1003,36)*z[39] - n<T>(353,18)*z[40] - n<T>(15,2)*
    z[41] + n<T>(17,2)*z[42] - n<T>(21,4)*z[43] + z[45] + 3*z[44];
    z[54]= - n<T>(1,6)*z[12] + z[58] - n<T>(3,2)*z[10] - n<T>(1,24)*z[20] + z[13];
    z[54]=z[19]*z[54];
    z[56]=n<T>(563,2)*z[33] - n<T>(247,3)*z[19];
    z[56]=z[8]*z[56];

    r += z[46] + z[47] + z[48] + z[49] + n<T>(1,2)*z[50] + z[51] + z[52] + 
      z[53] + z[54] + z[55] + n<T>(1,18)*z[56];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf330(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf330(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
