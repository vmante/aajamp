#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf872(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[128];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=e[0];
    z[9]=e[1];
    z[10]=e[8];
    z[11]=d[6];
    z[12]=d[15];
    z[13]=d[9];
    z[14]=d[4];
    z[15]=d[5];
    z[16]=d[12];
    z[17]=d[17];
    z[18]=d[11];
    z[19]=e[2];
    z[20]=e[5];
    z[21]=e[9];
    z[22]=f[0];
    z[23]=f[3];
    z[24]=f[5];
    z[25]=f[8];
    z[26]=f[11];
    z[27]=f[13];
    z[28]=f[17];
    z[29]=f[20];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[18];
    z[34]=c[19];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[29];
    z[40]=c[30];
    z[41]=c[31];
    z[42]=c[47];
    z[43]=c[51];
    z[44]=c[54];
    z[45]=c[55];
    z[46]=c[59];
    z[47]=c[61];
    z[48]=c[62];
    z[49]=c[66];
    z[50]=c[68];
    z[51]=c[69];
    z[52]=c[72];
    z[53]=c[75];
    z[54]=c[83];
    z[55]=e[3];
    z[56]=e[10];
    z[57]=e[6];
    z[58]=e[7];
    z[59]=e[13];
    z[60]=e[12];
    z[61]=e[14];
    z[62]=f[1];
    z[63]=f[4];
    z[64]=f[6];
    z[65]=f[12];
    z[66]=f[14];
    z[67]=f[16];
    z[68]=f[18];
    z[69]=f[31];
    z[70]=f[36];
    z[71]=f[37];
    z[72]=f[39];
    z[73]=f[51];
    z[74]=f[52];
    z[75]=f[55];
    z[76]=f[56];
    z[77]=f[58];
    z[78]=g[0];
    z[79]=g[3];
    z[80]=g[5];
    z[81]=g[8];
    z[82]=g[11];
    z[83]=g[13];
    z[84]=g[17];
    z[85]=g[21];
    z[86]=g[29];
    z[87]=g[30];
    z[88]=g[35];
    z[89]=g[41];
    z[90]=g[42];
    z[91]=g[43];
    z[92]=g[56];
    z[93]=g[57];
    z[94]=g[76];
    z[95]=g[94];
    z[96]=g[107];
    z[97]=g[125];
    z[98]=n<T>(1,3)*z[2];
    z[99]= - 1121*z[2] - n<T>(14279,754) + 1618*z[3];
    z[99]=z[99]*z[98];
    z[100]=z[2]*z[1];
    z[101]=z[3]*z[1];
    z[102]=n<T>(4595,6)*z[101] - 117*z[100];
    z[103]=n<T>(1,2)*i;
    z[102]=z[102]*z[103];
    z[104]=i*z[1];
    z[105]=n<T>(1823,754) - 257*z[3];
    z[105]=n<T>(1097,2)*z[5] - n<T>(3893,2)*z[104] + n<T>(19,2)*z[105] + 1517*z[2];
    z[105]=z[5]*z[105];
    z[106]=n<T>(1,2)*z[11];
    z[107]= - 79*z[9] - 9*z[10];
    z[107]=n<T>(1,2)*z[107] - z[8];
    z[108]=n<T>(7493,3016) - 103*z[3];
    z[108]=z[3]*z[108];
    z[109]= - n<T>(1643,12)*z[4] + n<T>(887,3)*z[5] + n<T>(1643,3)*z[104] + n<T>(41,3)*
    z[2] + 1 - n<T>(928,3)*z[3];
    z[109]=z[4]*z[109];
    z[99]=n<T>(1,3)*z[109] + n<T>(1,6)*z[105] + z[102] + z[99] + z[108] + n<T>(1241,754)*z[6]
     + z[106]
     + n<T>(161,754)*z[7] - n<T>(324,377)*z[14] - n<T>(3,2)*z[13]
    + 3*z[107] + n<T>(33839,144)*z[30];
    z[99]=z[4]*z[99];
    z[102]=n<T>(1,4)*z[3];
    z[105]= - n<T>(7493,377) + 113*z[3];
    z[105]=z[105]*z[102];
    z[107]= - 202*z[2] - n<T>(6079,754) + 268*z[3];
    z[107]=z[107]*z[98];
    z[108]= - 20*z[100] - n<T>(57257,1508)*z[1] + 1117*z[101];
    z[108]=i*z[108];
    z[109]= - 1361*z[2] + n<T>(57257,1508) + 887*z[3];
    z[109]=n<T>(845,18)*z[5] + n<T>(1,3)*z[109] - n<T>(697,2)*z[104];
    z[109]=z[5]*z[109];
    z[110]=3*z[11];
    z[111]=n<T>(3,2)*z[6];
    z[105]=n<T>(1,2)*z[109] + n<T>(1,3)*z[108] + z[107] + z[105] + z[111] - 
    z[110] + n<T>(5,2)*z[7] - z[15] - n<T>(11999,36)*z[30] + 6*z[8] + n<T>(115,2)*
    z[9] + n<T>(658,3)*z[10];
    z[105]=z[5]*z[105];
    z[107]=npow(z[3],2);
    z[108]= - n<T>(713,12)*z[30] + 2021*z[8] + 847*z[9] + n<T>(1343,2)*z[10];
    z[109]=n<T>(3085,3)*z[2] - n<T>(54005,754) - 1003*z[3];
    z[109]=z[2]*z[109];
    z[108]=n<T>(1,6)*z[109] - n<T>(74,3)*z[107] + z[110] - n<T>(3,2)*z[7] + n<T>(1,3)*
    z[108] + 6*z[15];
    z[108]=z[2]*z[108];
    z[109]=z[16] + n<T>(1995,754)*z[12];
    z[112]=2*z[13];
    z[113]=3*z[15];
    z[114]= - n<T>(809,377)*z[6] - z[11] - n<T>(538,377)*z[7] - z[113] + n<T>(648,377)*z[14]
     + z[112] - n<T>(4042,3)*z[8] - n<T>(1316,3)*z[10]
     + n<T>(1201,3)*z[9]
    - z[109];
    z[114]=z[1]*z[114];
    z[115]= - n<T>(28699,377)*z[1] + 501*z[101];
    z[115]=z[115]*z[102];
    z[116]=1403*z[100] + n<T>(44321,377)*z[1] - 2353*z[101];
    z[116]=z[116]*z[98];
    z[114]=z[116] + z[115] + z[114] - n<T>(8933,72)*z[31] + 1585*z[37] + 166
   *z[35];
    z[114]=i*z[114];
    z[115]=n<T>(1,2)*z[3];
    z[116]=n<T>(28699,1508) + n<T>(463,9)*z[3];
    z[116]=z[116]*z[115];
    z[117]=z[15] - z[13];
    z[118]= - n<T>(3019,3)*z[9] + 45*z[10];
    z[116]=z[116] + n<T>(8621,36)*z[30] + n<T>(1,2)*z[118] + n<T>(2021,3)*z[8] - 
    z[117];
    z[116]=z[3]*z[116];
    z[118]=n<T>(1,377)*z[7];
    z[119]=271*z[14];
    z[120]=z[119] - n<T>(5117,2)*z[7];
    z[120]=z[120]*z[118];
    z[112]=z[106] - 4*z[15] + z[112] + z[14];
    z[112]=z[11]*z[112];
    z[121]= - z[20] + n<T>(1995,754)*z[19];
    z[122]=npow(z[13],2);
    z[123]=n<T>(1,2)*z[122];
    z[124]=npow(z[14],2);
    z[125]=n<T>(3715,377)*z[6] - z[11] - z[13] - n<T>(809,377)*z[7];
    z[125]=z[6]*z[125];
    z[99]=z[99] + z[105] + z[114] + z[108] + z[116] + z[125] + z[112] + 
    z[120] - n<T>(5113,1508)*z[124] - z[123] + n<T>(21583,27144)*z[30] + n<T>(8836,1131)*z[9]
     + 631*z[22] - n<T>(463,3)*z[23]
     + n<T>(760,3)*z[24]
     + n<T>(112,3)*
    z[26] + 200*z[28] - 9*z[25] + n<T>(211,6)*z[27] - n<T>(631,3)*z[29] - n<T>(271,4)
   *z[32] + n<T>(577,12)*z[34] - n<T>(679,3)*z[36] - 1017*z[38] - n<T>(93943,36)*
    z[41] - 271*z[33] + n<T>(813,4)*z[39] + n<T>(373,2)*z[40] - z[121];
    z[99]=z[4]*z[99];
    z[105]=1 - n<T>(1405,6)*z[3];
    z[105]=z[105]*z[115];
    z[108]=n<T>(8335,12)*z[2] + n<T>(94297,1508) + n<T>(250,3)*z[3];
    z[108]=z[108]*z[98];
    z[112]=6*z[7];
    z[114]=n<T>(73,2)*z[8] - 5*z[9] + 47*z[10];
    z[114]=23*z[114] - n<T>(24523,4)*z[30];
    z[105]=z[108] + z[105] - n<T>(14373,1508)*z[6] - z[112] + n<T>(1,6)*z[114]
    - z[113];
    z[105]=z[2]*z[105];
    z[108]=z[7] + n<T>(4791,754)*z[6];
    z[108]=z[108]*z[111];
    z[114]=n<T>(8423,4)*z[30] + 1675*z[8] + 215*z[9] + 431*z[10];
    z[116]= - 8 - n<T>(1231,18)*z[3];
    z[116]=z[3]*z[116];
    z[114]=z[116] + n<T>(1,6)*z[114] - z[11];
    z[114]=z[3]*z[114];
    z[116]=n<T>(1,2)*z[124];
    z[120]=npow(z[7],2);
    z[125]=965*z[40] + n<T>(653,2)*z[39];
    z[125]=n<T>(192157,72)*z[41] + n<T>(1,2)*z[125] - n<T>(653,3)*z[33];
    z[126]=z[15]*z[13];
    z[127]= - z[15] + z[106];
    z[127]=z[11]*z[127];
    z[105]=z[105] + z[114] + z[108] + z[127] + n<T>(5,4)*z[120] + z[126] - 
    z[116] - z[123] - n<T>(19757,3393)*z[30] + 8*z[8] + n<T>(6574,1131)*z[10] + 
   n<T>(54005,2262)*z[9] + n<T>(37,2)*z[22] - n<T>(137,6)*z[23] + n<T>(239,3)*z[24]
     + 
   n<T>(101,3)*z[26] + 78*z[28] + 60*z[25] + n<T>(565,6)*z[27] - n<T>(92,3)*z[29]
    - n<T>(653,24)*z[32] + n<T>(1043,36)*z[34] - n<T>(1121,12)*z[36] + n<T>(1,2)*z[125]
    - n<T>(575,3)*z[38];
    z[105]=z[2]*z[105];
    z[108]= - z[119] + n<T>(2855,2)*z[7];
    z[108]=z[108]*z[118];
    z[114]=10*z[21];
    z[118]=15*z[8];
    z[119]=z[18] + z[13];
    z[119]=z[13]*z[119];
    z[125]=n<T>(15,2)*z[15] - 3*z[14] - z[16] - z[13];
    z[125]=z[15]*z[125];
    z[126]= - 3*z[17] - z[13];
    z[126]=n<T>(9,2)*z[11] + 5*z[15] + 2*z[126] - z[14];
    z[126]=z[11]*z[126];
    z[127]= - 6676*z[12] + n<T>(2749,2)*z[7];
    z[127]= - n<T>(1453,754)*z[6] + n<T>(1,377)*z[127] + z[11];
    z[127]=z[6]*z[127];
    z[108]=z[127] + z[126] + z[108] + z[125] + n<T>(4359,1508)*z[124] + 
    z[119] - z[118] - n<T>(10886,1131)*z[10] - n<T>(71677,2262)*z[9] - 796*z[22]
    + n<T>(2735,12)*z[23] - n<T>(1447,3)*z[24] - n<T>(793,12)*z[26] - n<T>(1469,4)*
    z[28] + 288*z[25] - n<T>(496,3)*z[27] + n<T>(964,3)*z[29] + z[114] + z[121];
    z[108]=z[1]*z[108];
    z[119]= - n<T>(20485,6)*z[100] - n<T>(107869,754)*z[1] + 1571*z[101];
    z[119]=z[119]*z[98];
    z[121]=n<T>(3136,3)*z[31] + n<T>(3155,2)*z[37] + 533*z[35];
    z[125]= - 92*z[9] - 193*z[10];
    z[125]=11*z[125] - 4013*z[8];
    z[125]=n<T>(7752,377)*z[6] + n<T>(1,3)*z[125] + 4*z[7];
    z[125]=z[1]*z[125];
    z[126]=15*z[1] + n<T>(617,2)*z[101];
    z[126]=z[3]*z[126];
    z[119]=z[119] + z[126] + n<T>(1,3)*z[121] + z[125];
    z[119]=z[2]*z[119];
    z[121]=n<T>(8279,24)*z[31] + n<T>(2477,2)*z[37] + 413*z[35];
    z[125]= - 4042*z[8] + 2125*z[9] - 242*z[10];
    z[125]= - 2*z[7] + n<T>(1,3)*z[125] - z[13];
    z[125]=z[1]*z[125];
    z[126]=2*z[1] - n<T>(337,18)*z[101];
    z[126]=z[3]*z[126];
    z[121]=z[126] + n<T>(1,3)*z[121] + z[125];
    z[121]=z[3]*z[121];
    z[125]=11141*z[49] + n<T>(17287,3)*z[46];
    z[125]=n<T>(131903,1508)*z[35] + n<T>(127379,754)*z[37] + n<T>(1,2)*z[125] - 
   n<T>(3136,3)*z[42];
    z[108]=z[119] + z[121] + z[108] + n<T>(1,3)*z[125] - n<T>(17143,3016)*z[31];
    z[108]=i*z[108];
    z[119]= - n<T>(1717,4)*z[2] - n<T>(13855,1508) + 209*z[3];
    z[119]=z[119]*z[98];
    z[121]=1081*z[100] - z[1] - n<T>(1465,2)*z[101];
    z[103]=z[121]*z[103];
    z[121]= - 23 + 107*z[3];
    z[121]=n<T>(1,2)*z[121] - n<T>(583,3)*z[2];
    z[104]= - n<T>(791,6)*z[5] + n<T>(1,2)*z[121] + n<T>(1582,3)*z[104];
    z[104]=z[5]*z[104];
    z[121]=n<T>(1,2)*z[13];
    z[125]=n<T>(9001,3016) + 61*z[3];
    z[125]=z[3]*z[125];
    z[103]=n<T>(1,3)*z[104] + z[103] + z[119] + z[125] - n<T>(3,4)*z[6] + n<T>(21,2)
   *z[11] - z[112] - z[15] - n<T>(15,4)*z[14] + z[121] + n<T>(5327,18)*z[30] - 
    z[118] + n<T>(241,2)*z[9] - 132*z[10];
    z[103]=z[5]*z[103];
    z[104]=n<T>(115,3)*z[2] - n<T>(7705,754) - 617*z[3];
    z[104]=z[104]*z[98];
    z[112]=n<T>(14015,72)*z[30] + 21*z[8] + n<T>(23,2)*z[9] + n<T>(31,3)*z[10];
    z[104]=z[104] + n<T>(311,2)*z[107] - z[111] - 2*z[11] + 5*z[112] + n<T>(19,2)
   *z[7];
    z[104]=z[2]*z[104];
    z[107]=2*z[15];
    z[112]= - 2*z[17] - 23*z[9];
    z[111]=z[111] - 6*z[11] + z[107] + n<T>(15,2)*z[14] - 168*z[8] + 5*
    z[112] - n<T>(133,3)*z[10];
    z[111]=z[1]*z[111];
    z[112]=n<T>(4571,377)*z[1] - 1735*z[101];
    z[102]=z[112]*z[102];
    z[100]=n<T>(3125,2)*z[100] + n<T>(17672,377)*z[1] - 385*z[101];
    z[98]=z[100]*z[98];
    z[98]=z[98] + z[102] + z[111] - n<T>(6725,36)*z[31] - 230*z[37] - n<T>(141,2)
   *z[35];
    z[98]=i*z[98];
    z[100]=z[14] - n<T>(9,4)*z[15];
    z[100]=z[100]*z[113];
    z[101]= - n<T>(6079,3016) + n<T>(128,3)*z[3];
    z[101]=z[3]*z[101];
    z[101]=z[101] - n<T>(50531,72)*z[30] + 87*z[8] - 241*z[9] + n<T>(112,3)*
    z[10] + z[117];
    z[101]=z[3]*z[101];
    z[102]= - z[116] + n<T>(241,6)*z[29];
    z[111]=n<T>(5,4)*z[6] + z[13] - 3*z[7];
    z[111]=z[6]*z[111];
    z[98]=z[103] + z[98] + z[104] + z[101] + z[111] + n<T>(3,2)*z[120] + 
    z[100] - n<T>(12017,27144)*z[30] + n<T>(4312,1131)*z[10] + n<T>(145,2)*z[22]
     - 
   n<T>(101,4)*z[23] + 74*z[24] - n<T>(151,4)*z[26] + n<T>(111,4)*z[28] - 282*z[25]
    + 16*z[27] - n<T>(57,4)*z[32] + 15*z[34] - n<T>(109,6)*z[36] + 80*z[38] - 
   n<T>(113365,72)*z[41] - 57*z[33] + n<T>(171,4)*z[39] - z[114] + n<T>(431,2)*
    z[40] - z[102];
    z[98]=z[5]*z[98];
    z[100]= - 1 + n<T>(337,36)*z[3];
    z[100]=z[100]*z[115];
    z[101]= - 17*z[10] + n<T>(205,2)*z[8];
    z[101]=3*z[101] - n<T>(6053,72)*z[30];
    z[100]=z[100] + z[106] + n<T>(1,2)*z[101] + z[7];
    z[100]=z[3]*z[100];
    z[101]=npow(z[15],2);
    z[103]=n<T>(3,4)*z[101];
    z[104]= - 53*z[40] + n<T>(1,2)*z[39];
    z[104]=n<T>(4481,72)*z[41] + n<T>(1,2)*z[104] - n<T>(1,3)*z[33];
    z[106]=npow(z[11],2);
    z[111]=npow(z[6],2);
    z[100]=z[100] - z[111] + n<T>(1,2)*z[106] + n<T>(1,4)*z[120] - z[103] + n<T>(707,9048)*z[30]
     + 7*z[8]
     + 74*z[22] - n<T>(51,2)*z[23]
     + n<T>(226,3)*z[24]
     + n<T>(197,6)*z[26]
     + n<T>(123,2)*z[28] - 57*z[25]
     + 20*z[27] - n<T>(5,24)*z[32]
     + n<T>(143,9)*z[34] - n<T>(1409,12)*z[36]
     + n<T>(5,2)*z[104] - n<T>(977,3)*z[38] - z[102];
    z[100]=z[3]*z[100];
    z[102]= - 1940*z[56] + n<T>(1707,4)*z[30];
    z[104]=n<T>(5,12)*z[7] - 3*z[13] - n<T>(271,754)*z[14];
    z[104]=z[7]*z[104];
    z[102]=z[104] + z[103] + n<T>(2097,1508)*z[124] + n<T>(1,377)*z[102] + n<T>(5,2)
   *z[122];
    z[102]=z[7]*z[102];
    z[103]= - z[121] - z[110];
    z[103]=z[11]*z[103];
    z[104]=5*z[58];
    z[101]=z[103] + n<T>(5,2)*z[101] - n<T>(3,2)*z[124] - z[123] + n<T>(1,2)*z[30]
    + z[104] - 6*z[21];
    z[101]=z[11]*z[101];
    z[103]=2*z[124];
    z[106]=z[11]*z[14];
    z[110]= - n<T>(19117,754)*z[6] - z[14] + n<T>(4257,754)*z[7];
    z[110]=z[6]*z[110];
    z[106]=n<T>(1,2)*z[110] + z[106] + n<T>(6629,1508)*z[120] + z[103] + n<T>(9637,2262)*z[30]
     - n<T>(6676,377)*z[19]
     - n<T>(3503,754)*z[56]
     - z[55];
    z[106]=z[6]*z[106];
    z[110]=z[13] + n<T>(7,2)*z[14];
    z[107]=n<T>(1,2)*z[110] + z[107];
    z[107]=z[15]*z[107];
    z[103]=z[107] - z[103] + n<T>(3,2)*z[30] + z[20] - z[59] + z[104];
    z[103]=z[15]*z[103];
    z[104]=z[62] + z[64] + z[71] + z[76];
    z[107]=z[73] - z[77];
    z[110]=z[72] - z[75];
    z[111]=z[24] - z[36];
    z[112]=2*z[97] - n<T>(1,2)*z[96];
    z[113]=npow(z[9],2);
    z[114]=npow(z[10],2);
    z[115]=npow(z[8],2);
    z[109]= - n<T>(50575,24)*z[8] - n<T>(3859,6)*z[10] + n<T>(2789,8)*z[9] - 10*
    z[17] - z[109];
    z[109]=z[30]*z[109];
    z[116]= - n<T>(1,3)*z[122] + n<T>(7,6)*z[30] - z[59] - 3*z[61] - z[60];
    z[116]=z[13]*z[116];
    z[117]=z[124] + n<T>(6519,1508)*z[30] + 2*z[57] - z[55];
    z[117]=z[14]*z[117];

    r +=  - z[22] + n<T>(28699,3016)*z[23] + n<T>(57257,9048)*z[26] - n<T>(4571,3016)*z[28]
     + n<T>(28165,1508)*z[32]
     + n<T>(159047,54288)*z[34] - n<T>(127379,4524)*z[38] - n<T>(234101,3016)*z[41]
     + n<T>(653,24)*z[43]
     + n<T>(653,6)*
      z[44] + n<T>(71483,72)*z[45] - n<T>(1243,9)*z[47] + n<T>(14411,6)*z[48] - n<T>(437,4)*z[50]
     + n<T>(395,8)*z[51] - n<T>(653,8)*z[52] - n<T>(965,4)*z[53] - 
      n<T>(192157,144)*z[54] - n<T>(5117,754)*z[63] + n<T>(5113,1508)*z[65] - z[66]
       + n<T>(2097,1508)*z[67] - n<T>(1,3)*z[68] - n<T>(25,4)*z[69] + n<T>(7,4)*z[70]
       + z[74] - n<T>(71,4)*z[78] - n<T>(201,4)*z[79] + n<T>(21,8)*z[80] + n<T>(1451,4)
      *z[81] - n<T>(1517,4)*z[82] - n<T>(45,4)*z[83] + n<T>(81,4)*z[84] - n<T>(259,12)*
      z[85] + n<T>(848,3)*z[86] + n<T>(2513,8)*z[87] - n<T>(1511,12)*z[88] - n<T>(337,12)*z[89]
     - n<T>(9,4)*z[90]
     + n<T>(63,2)*z[91]
     + n<T>(13,2)*z[92] - n<T>(19,2)*
      z[93] + n<T>(23,2)*z[94] - n<T>(89,4)*z[95] + z[98] + z[99] + z[100] + 
      z[101] + z[102] + z[103] + n<T>(1,2)*z[104] + z[105] + z[106] - n<T>(3,4)
      *z[107] + z[108] + z[109] + n<T>(5,4)*z[110] + n<T>(131903,9048)*z[111]
       + 3*z[112] - n<T>(149,2)*z[113] + 33*z[114] - n<T>(63,8)*z[115] + z[116]
       + z[117];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf872(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf872(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
