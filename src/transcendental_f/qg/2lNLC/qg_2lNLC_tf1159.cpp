#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1159(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[42];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[9];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[17];
    z[16]=c[31];
    z[17]=e[7];
    z[18]=e[6];
    z[19]=f[31];
    z[20]=f[32];
    z[21]=f[33];
    z[22]=f[36];
    z[23]=f[39];
    z[24]=f[40];
    z[25]=f[43];
    z[26]=n<T>(1,2)*z[3];
    z[27]=n<T>(3,2)*z[5];
    z[28]=z[1]*i;
    z[29]=z[27] - n<T>(3,2)*z[28] - z[4] - z[26];
    z[29]=z[5]*z[29];
    z[30]=n<T>(1,2)*z[28];
    z[31]=z[4] + 11*z[3];
    z[31]=z[31]*z[30];
    z[32]= - z[3] + n<T>(1,2)*z[4];
    z[32]=z[32]*z[3];
    z[33]=npow(z[4],2);
    z[34]=z[3] - z[4];
    z[35]= - z[8]*z[34];
    z[29]=z[29] + z[35] + z[31] - z[33] + z[32];
    z[31]=n<T>(1,2)*z[5];
    z[29]=z[29]*z[31];
    z[35]=3*z[4];
    z[36]=n<T>(5,2)*z[3];
    z[37]= - z[35] + z[36];
    z[37]=z[37]*z[28];
    z[38]=n<T>(7,2)*z[28];
    z[39]=n<T>(7,4)*z[8] - z[38] - z[4] - z[36];
    z[40]=n<T>(1,4)*z[8];
    z[39]=z[39]*z[40];
    z[38]= - n<T>(7,9)*z[7] + n<T>(7,2)*z[8] - z[38] - n<T>(13,3)*z[4] + z[36];
    z[38]=z[7]*z[38];
    z[41]=npow(z[3],2);
    z[37]=n<T>(1,8)*z[38] + z[39] + n<T>(1,4)*z[37] + n<T>(1,3)*z[33] + n<T>(5,16)*z[41]
   ;
    z[37]=z[7]*z[37];
    z[38]=n<T>(1,2)*z[2];
    z[35]= - z[38] + z[31] - z[35] - z[36];
    z[35]=z[17]*z[35];
    z[36]=n<T>(1,3)*z[4];
    z[39]= - z[40] - z[36] - n<T>(23,2)*z[3];
    z[27]=n<T>(1,3)*z[39] - z[27];
    z[27]=n<T>(1,2)*z[27] - n<T>(5,9)*z[7];
    z[27]=z[12]*z[27];
    z[27]= - z[21] + z[27] + z[35];
    z[35]=z[5] + z[3];
    z[39]= - z[28]*z[35];
    z[39]= - z[12] + z[39];
    z[39]=z[6]*z[39];
    z[35]= - z[28] + z[35];
    z[35]=z[10]*z[35];
    z[35]=z[20] + z[39] + z[35];
    z[39]= - z[4] + n<T>(1,3)*z[3];
    z[39]=z[3]*z[39];
    z[39]=3*z[33] + z[39];
    z[26]=z[39]*z[26];
    z[39]=n<T>(5,2)*z[41];
    z[40]= - n<T>(19,3)*z[33] - z[39];
    z[30]=z[40]*z[30];
    z[40]=npow(z[4],3);
    z[26]=z[30] + n<T>(5,9)*z[40] + z[26];
    z[30]=z[33] + z[39];
    z[36]=z[36] - n<T>(13,2)*z[3];
    z[36]=z[36]*z[28];
    z[30]=n<T>(1,2)*z[30] + z[36];
    z[36]= - 5*z[4] + n<T>(13,4)*z[3];
    z[36]=n<T>(1,9)*z[8] + n<T>(1,4)*z[36] - n<T>(1,3)*z[28];
    z[36]=z[8]*z[36];
    z[30]=n<T>(1,4)*z[30] + z[36];
    z[30]=z[8]*z[30];
    z[31]=z[2] - z[31] + z[8] - n<T>(5,2)*z[28];
    z[31]=z[34]*z[31];
    z[31]= - n<T>(1,2)*z[33] - z[32] + z[31];
    z[31]=z[31]*z[38];
    z[32]=29*z[8] + 17*z[4];
    z[33]=n<T>(1,12)*z[32] + z[7];
    z[33]=z[28]*z[33];
    z[33]=n<T>(29,12)*z[12] + z[33];
    z[33]=z[9]*z[33];
    z[28]= - 29*z[28] + z[32];
    z[28]=n<T>(1,12)*z[28] + z[7];
    z[28]=z[11]*z[28];
    z[32]=n<T>(3,4)*z[15] + n<T>(47,144)*z[13];
    z[32]=i*z[32];
    z[34]= - z[4] - z[7];
    z[34]=z[18]*z[34];

    r +=  - n<T>(13,24)*z[14] - n<T>(3,16)*z[16] + n<T>(29,16)*z[19] - n<T>(5,48)*z[22]
       + n<T>(5,16)*z[23] + n<T>(1,3)*z[24] - z[25] + n<T>(1,4)*z[26] + n<T>(1,2)*z[27]
       + z[28] + z[29] + z[30] + z[31] + z[32] + z[33] + n<T>(5,12)*z[34]
       + n<T>(3,2)*z[35] + z[37];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1159(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1159(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
