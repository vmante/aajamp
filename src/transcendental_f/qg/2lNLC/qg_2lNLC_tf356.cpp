#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf356(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[80];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[9];
    z[14]=d[5];
    z[15]=d[17];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[0];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[6];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[43];
    z[52]=f[51];
    z[53]=f[55];
    z[54]=f[56];
    z[55]=f[58];
    z[56]=f[66];
    z[57]=n<T>(1,2)*z[2];
    z[58]=n<T>(1193,48)*z[2] - 5*z[6] - n<T>(21,4)*z[3];
    z[58]=z[58]*z[57];
    z[59]=n<T>(1,4)*z[8];
    z[60]=49*z[3] + 23*z[2];
    z[60]=n<T>(1,2)*z[60] - 13*z[8];
    z[60]=z[60]*z[59];
    z[61]=n<T>(1,2)*z[5];
    z[62]= - n<T>(557,24)*z[5] + z[8] - n<T>(961,12)*z[2] + n<T>(261,2)*z[4] - z[3];
    z[62]=z[62]*z[61];
    z[63]=n<T>(1,2)*z[7];
    z[64]=11*z[6];
    z[65]=z[64] - n<T>(713,8)*z[4];
    z[65]= - n<T>(17,6)*z[7] - n<T>(2785,48)*z[5] + n<T>(581,6)*z[2] + n<T>(51,4)*z[10]
    + n<T>(1,2)*z[65] - 3*z[3];
    z[65]=z[65]*z[63];
    z[66]=npow(z[14],2);
    z[67]=n<T>(5,2)*z[66];
    z[68]=npow(z[4],2);
    z[69]=npow(z[6],2);
    z[70]=npow(z[10],2);
    z[71]=npow(z[3],2);
    z[58]=z[65] + z[62] + z[60] + z[58] - n<T>(57,8)*z[70] - n<T>(3,2)*z[71] - 
   n<T>(705,32)*z[68] - n<T>(13,4)*z[69] + n<T>(2305,288)*z[22] + z[67] + n<T>(1,2)*
    z[20] - n<T>(149,3)*z[19];
    z[58]=z[58]*z[63];
    z[60]=59*z[3];
    z[62]=n<T>(1443,16)*z[10];
    z[63]=n<T>(91,2)*z[8] - z[57] + z[60] - z[62];
    z[63]=z[8]*z[63];
    z[65]=n<T>(2359,72)*z[22] + n<T>(455,2)*z[17] - n<T>(503,3)*z[16];
    z[65]=n<T>(1,2)*z[65] - 3*z[69];
    z[62]=n<T>(499,16)*z[8] + z[62] - z[13];
    z[72]=n<T>(2003,24)*z[2] - n<T>(923,8)*z[3] - n<T>(265,4)*z[4] + z[62];
    z[72]=n<T>(1,2)*z[72] + z[5];
    z[72]=z[72]*z[61];
    z[73]=npow(z[13],2);
    z[74]=npow(z[2],2);
    z[63]=z[72] + n<T>(1,2)*z[63] + n<T>(557,192)*z[74] - n<T>(41,64)*z[70] - n<T>(505,64)*z[71]
     + n<T>(5,8)*z[68]
     + n<T>(1,4)*z[65]
     + z[73];
    z[63]=z[5]*z[63];
    z[65]= - n<T>(87,4)*z[18] + 5*z[21];
    z[72]= - 5*z[11] + n<T>(87,4)*z[12];
    z[74]= - n<T>(13,8)*z[4] - z[72];
    z[74]=z[4]*z[74];
    z[75]=n<T>(57,4)*z[7] + n<T>(2689,24)*z[5] - n<T>(27,4)*z[8] - n<T>(3919,24)*z[2]
     - 
   n<T>(51,2)*z[10] + n<T>(49,4)*z[3] + n<T>(187,4)*z[4] + z[15] - 7*z[6];
    z[75]=z[7]*z[75];
    z[74]=z[75] + z[74] + n<T>(19,2)*z[69] - n<T>(39,4)*z[66] + n<T>(3763,24)*z[19]
    + n<T>(2761,24)*z[16] - n<T>(455,4)*z[17] - z[20] + z[65];
    z[75]=n<T>(1,32)*z[3];
    z[76]=505*z[9] - n<T>(457,2)*z[3];
    z[76]=z[76]*z[75];
    z[77]=n<T>(1,2)*z[10];
    z[78]= - z[12] + z[4];
    z[78]=11*z[78] + n<T>(269,32)*z[10];
    z[78]=z[78]*z[77];
    z[79]=n<T>(1081,12)*z[2] + n<T>(21,8)*z[3] - 3*z[6] - n<T>(175,8)*z[4];
    z[79]=z[79]*z[57];
    z[60]= - n<T>(2641,48)*z[2] + z[60] - n<T>(3,2)*z[4] + n<T>(455,8)*z[9] - z[62];
    z[60]=z[60]*z[61];
    z[61]=2*z[11] - n<T>(3,2)*z[13];
    z[61]=z[13]*z[61];
    z[62]= - n<T>(333,2)*z[8] + n<T>(5,2)*z[2] - 271*z[3] + n<T>(1443,4)*z[10];
    z[62]=z[8]*z[62];
    z[60]=z[60] + n<T>(1,8)*z[62] + z[79] + z[78] + z[76] + z[61] + n<T>(1,4)*
    z[74];
    z[60]=z[1]*z[60];
    z[61]=n<T>(125,16)*z[23] - n<T>(1607,2)*z[28] - 533*z[26];
    z[60]=n<T>(1,12)*z[61] + z[60];
    z[60]=i*z[60];
    z[61]=3*z[35];
    z[62]= - z[61] - z[67];
    z[62]=z[14]*z[62];
    z[67]=n<T>(3,2)*z[14] + z[6];
    z[67]=z[6]*z[67];
    z[61]=z[67] - z[61] - n<T>(181,48)*z[22];
    z[61]=z[6]*z[61];
    z[67]=z[54] + z[42] + z[51];
    z[74]=z[52] - z[55];
    z[76]=z[27] - z[41];
    z[72]= - n<T>(1,6)*z[14] + n<T>(455,4)*z[9] + z[15] - z[72];
    z[72]=z[22]*z[72];
    z[61]=z[61] + n<T>(1,2)*z[72] + z[62] - n<T>(1177,32)*z[24] - n<T>(2641,144)*
    z[25] + n<T>(1607,24)*z[29] + n<T>(30029,192)*z[30] + n<T>(175,8)*z[37] - n<T>(175,16)*z[38]
     + n<T>(5,4)*z[39]
     + n<T>(91,2)*z[40] - n<T>(2689,96)*z[43]
     + n<T>(41,32)*
    z[44] + n<T>(725,32)*z[45] - n<T>(195,16)*z[46] + n<T>(175,24)*z[47] + z[48]
     + n<T>(3,4)*z[49] - z[50] - n<T>(7,8)*z[53]
     + z[56]
     + n<T>(533,12)*z[76]
     + n<T>(19,8)*
    z[74] + n<T>(3,2)*z[67];
    z[62]=n<T>(1,4)*z[73] - n<T>(2593,96)*z[22] - n<T>(23,8)*z[66] - z[36] + n<T>(629,8)
   *z[33];
    z[67]= - n<T>(615,2)*z[3] + 5*z[13] + n<T>(7,2)*z[4];
    z[67]= - 3*z[2] + n<T>(1,2)*z[67] + 69*z[10];
    z[59]=z[67]*z[59];
    z[67]=z[4]*z[13];
    z[72]= - n<T>(3,4)*z[2] - z[13] + n<T>(21,16)*z[3];
    z[72]=z[2]*z[72];
    z[59]=z[59] + z[72] + n<T>(133,8)*z[70] - n<T>(805,64)*z[71] + n<T>(1,2)*z[62]
    + z[67];
    z[59]=z[8]*z[59];
    z[62]= - z[6] - n<T>(25,8)*z[4];
    z[62]=n<T>(761,48)*z[10] + 7*z[62] - 43*z[3];
    z[62]=z[62]*z[77];
    z[67]=7*z[34] + n<T>(401,8)*z[32];
    z[62]=z[62] + n<T>(1,8)*z[71] + n<T>(7,2)*z[69] - n<T>(9061,192)*z[22] - z[66]
    + n<T>(1,2)*z[67] + 11*z[18];
    z[62]=z[62]*z[77];
    z[67]=z[68] + z[70];
    z[68]=n<T>(6191,18)*z[22] - n<T>(1379,3)*z[19] + 31*z[31] - n<T>(749,3)*z[16];
    z[64]=z[64] - n<T>(1733,12)*z[2];
    z[64]=z[2]*z[64];
    z[64]=n<T>(1,4)*z[64] + n<T>(1,16)*z[68] + z[73] + n<T>(175,16)*z[67];
    z[57]=z[64]*z[57];
    z[64]= - n<T>(9,8)*z[73] + n<T>(15,32)*z[22] + n<T>(1,4)*z[66] - 2*z[21] + z[19]
   ;
    z[64]=z[13]*z[64];
    z[65]= - n<T>(845,96)*z[22] + n<T>(19,4)*z[66] - n<T>(31,8)*z[31] - z[65];
    z[66]= - z[13] + n<T>(203,64)*z[4];
    z[66]=z[4]*z[66];
    z[65]=n<T>(1,4)*z[65] + z[66];
    z[65]=z[4]*z[65];
    z[66]=947*z[71] - n<T>(107,3)*z[22] + 505*z[17] + 971*z[33] + 283*z[32];
    z[66]=z[66]*z[75];

    r += z[57] + z[58] + z[59] + z[60] + n<T>(1,2)*z[61] + z[62] + z[63] + 
      z[64] + z[65] + z[66];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf356(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf356(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
