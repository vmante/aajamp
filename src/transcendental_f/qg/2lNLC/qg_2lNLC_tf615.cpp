#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf615(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[63];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[15];
    z[10]=d[5];
    z[11]=d[4];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[31];
    z[25]=e[3];
    z[26]=e[10];
    z[27]=e[6];
    z[28]=e[14];
    z[29]=f[3];
    z[30]=f[4];
    z[31]=f[5];
    z[32]=f[6];
    z[33]=f[11];
    z[34]=f[12];
    z[35]=f[16];
    z[36]=f[17];
    z[37]=f[18];
    z[38]=f[31];
    z[39]=f[36];
    z[40]=f[39];
    z[41]=f[51];
    z[42]=f[55];
    z[43]=f[56];
    z[44]=f[58];
    z[45]=f[89];
    z[46]=n<T>(1,2)*z[2];
    z[47]=z[1]*i;
    z[48]=z[47] - z[46];
    z[48]=z[2]*z[48];
    z[49]= - z[47] + n<T>(1,2)*z[6];
    z[49]=z[49]*z[6];
    z[50]=n<T>(106571,4524)*z[7] + n<T>(81689,4524)*z[47] - 11*z[6];
    z[50]=z[7]*z[50];
    z[51]=z[47] + z[7];
    z[52]= - n<T>(6385,4)*z[51] + 4141*z[5];
    z[52]=z[5]*z[52];
    z[53]=n<T>(11567,9048)*z[47];
    z[54]= - n<T>(352615,9048)*z[3] + n<T>(72127,4524)*z[5] + n<T>(35181,3016)*z[2]
    + n<T>(63593,9048)*z[7] + z[53] + 5*z[6];
    z[54]=z[3]*z[54];
    z[48]=z[54] + n<T>(5,1131)*z[52] + n<T>(32165,1508)*z[48] - 3*z[49] + z[50];
    z[48]=z[3]*z[48];
    z[50]= - n<T>(1493,3)*z[5] - n<T>(53357,8)*z[2] - n<T>(82523,16)*z[6] + n<T>(68207,3)
   *z[7];
    z[50]=n<T>(79427,24)*z[11] + n<T>(72641,48)*z[4] + n<T>(1,3)*z[50] + n<T>(30577,8)*
    z[3];
    z[50]=n<T>(7,4)*z[8] + n<T>(1,377)*z[50] + n<T>(31,6)*z[10];
    z[50]=z[16]*z[50];
    z[48]=z[48] + z[50];
    z[50]=npow(z[7],2);
    z[52]=2*z[47];
    z[54]=z[52] - z[2];
    z[54]=z[2]*z[54];
    z[55]=npow(z[3],2);
    z[56]=z[47] - n<T>(1,2)*z[4];
    z[56]=z[4]*z[56];
    z[57]=z[2] - z[51];
    z[57]=3*z[57] + z[8];
    z[57]=z[8]*z[57];
    z[54]=3*z[28] + n<T>(1,2)*z[57] + z[56] + 4*z[55] - n<T>(5,2)*z[50] + z[54];
    z[54]=z[8]*z[54];
    z[55]=46491*z[47] - n<T>(84191,2)*z[6];
    z[55]=z[6]*z[55];
    z[56]= - n<T>(66095,2)*z[5] + 83197*z[47] + 66095*z[6];
    z[56]=z[5]*z[56];
    z[55]=z[55] + z[56];
    z[56]=2*z[7];
    z[57]=3*z[47] + z[56];
    z[57]=z[7]*z[57];
    z[58]= - 43*z[47] + 3*z[2];
    z[58]=z[58]*z[46];
    z[59]=n<T>(1,3)*z[4] - n<T>(83197,12064)*z[5] + n<T>(37,4)*z[2] - n<T>(9,2)*z[7]
     - 
    z[47] - n<T>(28395,12064)*z[6];
    z[59]=z[4]*z[59];
    z[55]=z[59] + z[58] + 3*z[57] + n<T>(1,6032)*z[55];
    z[55]=z[4]*z[55];
    z[57]= - z[52] + z[4];
    z[57]=z[4]*z[57];
    z[50]=z[50] - z[57];
    z[57]= - z[47] + 17*z[6];
    z[56]= - n<T>(17,8)*z[11] - 2*z[4] + n<T>(1,8)*z[57] + z[56];
    z[56]=z[10]*z[56];
    z[57]= - z[47] + z[6];
    z[57]=5*z[57] + n<T>(7,4)*z[11];
    z[57]=z[11]*z[57];
    z[50]=3*z[56] + z[57] - n<T>(27,2)*z[49] - 5*z[50];
    z[50]=z[10]*z[50];
    z[56]=76651*z[3];
    z[57]= - n<T>(133715,2)*z[5] - z[56];
    z[57]=z[47]*z[57];
    z[57]= - n<T>(133715,2)*z[16] + z[57];
    z[57]=z[9]*z[57];
    z[58]=z[47] - z[5];
    z[56]=n<T>(133715,2)*z[58] - z[56];
    z[56]=z[14]*z[56];
    z[56]=z[57] + z[56];
    z[57]= - 8691*z[47] + n<T>(14743,4)*z[6];
    z[57]=z[6]*z[57];
    z[58]= - n<T>(5,2)*z[7] - z[47] - n<T>(19,2)*z[6];
    z[58]=z[7]*z[58];
    z[59]=53871*z[47] + n<T>(20021,2)*z[6];
    z[59]= - n<T>(46331,754)*z[2] + n<T>(1,377)*z[59] + 21*z[7];
    z[59]=z[2]*z[59];
    z[57]=n<T>(1,4)*z[59] + n<T>(1,377)*z[57] + z[58];
    z[46]=z[57]*z[46];
    z[57]= - n<T>(36609,2)*z[5] + n<T>(29743,2)*z[47] + 3433*z[2];
    z[57]=z[13]*z[57];
    z[58]= - n<T>(7957,2)*z[2] + 13235*z[47] - n<T>(18513,2)*z[6];
    z[58]=z[15]*z[58];
    z[57]=z[57] + z[58];
    z[58]= - z[47] - n<T>(1,2)*z[7];
    z[58]=z[7]*z[58];
    z[51]= - n<T>(1,2)*z[5] + z[51];
    z[51]=z[5]*z[51];
    z[51]=z[58] + z[51];
    z[51]=9*z[49] + n<T>(13171,1508)*z[51];
    z[58]= - n<T>(78913,3016)*z[5] - n<T>(23631,3016)*z[7] + n<T>(202569,3016)*z[47]
    - 41*z[6];
    z[58]=n<T>(13,2)*z[11] + n<T>(1,2)*z[58] + 17*z[3];
    z[58]=z[11]*z[58];
    z[51]=n<T>(5,2)*z[51] + z[58];
    z[51]=n<T>(17,2)*z[27] + n<T>(1,2)*z[51];
    z[51]=z[11]*z[51];
    z[58]=npow(z[6],2);
    z[52]=z[52] - n<T>(1,8)*z[6];
    z[52]=z[52]*z[58];
    z[59]=n<T>(1,3)*z[7];
    z[53]=z[53] + 15*z[6];
    z[53]=n<T>(1,4)*z[53] + z[59];
    z[53]=z[7]*z[53];
    z[53]=n<T>(19,8)*z[58] + z[53];
    z[53]=z[7]*z[53];
    z[58]=133715*z[47] - n<T>(319199,2)*z[7];
    z[58]=z[58]*z[59];
    z[49]=5615*z[49] + z[58];
    z[58]=1091*z[2] - 38791*z[47] - 14743*z[6];
    z[58]=z[2]*z[58];
    z[49]=n<T>(1,2)*z[49] + z[58];
    z[58]= - 36609*z[6] - n<T>(133715,3)*z[7];
    z[58]=n<T>(1,16)*z[58] + 6419*z[2];
    z[58]=z[5]*z[58];
    z[49]=n<T>(1,4)*z[49] + z[58];
    z[49]=z[5]*z[49];
    z[47]= - n<T>(13,2)*z[4] + 15*z[47] - n<T>(17,2)*z[2];
    z[47]=z[12]*z[47];
    z[47]=z[32] + z[47] + z[43];
    z[58]= - z[38] + z[40] - z[39];
    z[59]=z[31] - z[21];
    z[60]= - n<T>(124073,3016)*z[22] - n<T>(130105,6032)*z[20] + n<T>(65855,54288)*
    z[17];
    z[60]=i*z[60];
    z[61]= - z[3] - z[11];
    z[61]=z[25]*z[61];
    z[62]= - 131453*z[7] - 117881*z[3];
    z[62]=z[26]*z[62];

    r += n<T>(287017,18096)*z[18] - n<T>(160265,72384)*z[19] + n<T>(124073,6032)*
      z[23] - n<T>(8088793,108576)*z[24] - n<T>(65101,12064)*z[29] - n<T>(337295,36192)*z[30]
     + n<T>(5615,12064)*z[33]
     + n<T>(78913,12064)*z[34] - n<T>(23631,12064)*z[35] - n<T>(58555,12064)*z[36]
     - z[37]
     - z[41]
     - z[42]
     +  z[44] + z[45] + z[46] + n<T>(3,2)*z[47] + n<T>(1,2)*z[48] + n<T>(1,754)*z[49]
       + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + n<T>(1,4524)*z[56]
       + n<T>(1,1508)*z[57] - n<T>(17,8)*z[58] - n<T>(130105,12064)*z[59] + z[60]
       + n<T>(17,2)*z[61] + n<T>(1,9048)*z[62];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf615(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf615(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
