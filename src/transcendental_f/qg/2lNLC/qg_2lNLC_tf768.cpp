#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf768(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[56];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[17];
    z[10]=d[4];
    z[11]=d[6];
    z[12]=d[5];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[8];
    z[17]=e[9];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[3];
    z[28]=e[10];
    z[29]=e[6];
    z[30]=f[3];
    z[31]=f[4];
    z[32]=f[5];
    z[33]=f[11];
    z[34]=f[12];
    z[35]=f[16];
    z[36]=f[17];
    z[37]=f[31];
    z[38]=f[36];
    z[39]=f[39];
    z[40]=7*z[13];
    z[41]= - n<T>(168863,2)*z[14] + 114575*z[16] - n<T>(150013,2)*z[15];
    z[41]=n<T>(1,2262)*z[41] - z[40];
    z[42]=n<T>(1,4)*z[10];
    z[43]=n<T>(281209,6032)*z[10] + z[12] + n<T>(154537,3016)*z[7];
    z[43]=z[43]*z[42];
    z[44]=n<T>(1,2)*z[2];
    z[45]= - n<T>(191483,9048)*z[2] + n<T>(7,2)*z[4] - z[7];
    z[45]=z[45]*z[44];
    z[46]=npow(z[12],2);
    z[46]=n<T>(97,16)*z[46];
    z[47]=z[46] + 5*z[17];
    z[48]=npow(z[7],2);
    z[49]=z[9] - z[11];
    z[49]=z[11]*z[49];
    z[50]=n<T>(161323,18096)*z[7];
    z[51]= - n<T>(150013,36192)*z[3] + n<T>(131917,6032)*z[2] - z[50] - n<T>(127393,9048)*z[8]
     + z[9];
    z[51]=z[3]*z[51];
    z[52]=5*z[9];
    z[53]= - n<T>(53329,4524)*z[2] - n<T>(131,8)*z[10] + 4*z[11] + n<T>(122115,12064)
   *z[4] + z[52] + n<T>(13,2)*z[12];
    z[53]=z[6]*z[53];
    z[54]=n<T>(150013,3)*z[8] - n<T>(143227,2)*z[4];
    z[54]=n<T>(91955,36192)*z[6] + n<T>(152275,18096)*z[3] + n<T>(177911,18096)*z[2]
    - n<T>(154537,12064)*z[10] - n<T>(122869,36192)*z[7] + n<T>(1,6032)*z[54] - 
    z[11];
    z[54]=z[5]*z[54];
    z[41]=z[54] + z[53] + z[51] + z[45] + z[43] - n<T>(181681,72384)*z[48]
    + 3*z[49] + n<T>(1,4)*z[41] - z[47];
    z[41]=z[1]*z[41];
    z[43]= - z[24] - n<T>(1,2)*z[22];
    z[43]=28619*z[43] + n<T>(232265,6)*z[19];
    z[41]=n<T>(1,18096)*z[43] + z[41];
    z[41]=i*z[41];
    z[43]= - z[12] - 25*z[10];
    z[42]=z[43]*z[42];
    z[43]=n<T>(1,8)*z[2];
    z[45]=3*z[7] + n<T>(119099,4524)*z[2];
    z[45]=z[45]*z[43];
    z[49]=npow(z[4],2);
    z[51]=z[3]*z[44];
    z[53]= - 13*z[12] - n<T>(117591,6032)*z[4];
    z[53]=n<T>(1,48)*z[6] + n<T>(110051,18096)*z[2] + n<T>(131,16)*z[10] + n<T>(1,16)*
    z[7] + n<T>(1,4)*z[53] - 5*z[11];
    z[53]=z[6]*z[53];
    z[42]=z[53] + z[51] + z[45] + z[42] - n<T>(122115,24128)*z[49] - n<T>(459973,217152)*z[18]
     - n<T>(110051,18096)*z[16]
     + z[47];
    z[42]=z[6]*z[42];
    z[45]=150013*z[15] - 110051*z[14];
    z[47]= - 154537*z[7] - n<T>(130409,2)*z[10];
    z[47]=z[10]*z[47];
    z[45]=n<T>(1,32)*z[47] + n<T>(181681,192)*z[48] + n<T>(143227,64)*z[49] + n<T>(1,48)
   *z[45] + 640*z[18];
    z[47]=z[7] - n<T>(139457,9048)*z[2];
    z[47]=z[47]*z[44];
    z[51]=n<T>(127393,18096)*z[3] - z[11] + n<T>(170371,18096)*z[7];
    z[51]=z[3]*z[51];
    z[53]=n<T>(1,2)*z[3];
    z[54]= - n<T>(91955,72384)*z[6] - z[53] - n<T>(125885,18096)*z[2] - n<T>(1,2)*
    z[7] + n<T>(117591,12064)*z[4] + z[11];
    z[54]=z[6]*z[54];
    z[55]=154537*z[10] - 117591*z[4] + n<T>(122869,3)*z[7];
    z[55]= - n<T>(101003,48)*z[6] - n<T>(36655,3)*z[3] + n<T>(1,16)*z[55] + n<T>(28361,3)
   *z[2];
    z[55]=z[5]*z[55];
    z[45]=n<T>(1,1508)*z[55] + z[54] + z[51] + n<T>(1,377)*z[45] + z[47];
    z[45]=z[5]*z[45];
    z[47]= - n<T>(119099,2)*z[16] + 139457*z[14];
    z[51]= - z[7] + n<T>(20103,754)*z[2];
    z[51]=z[2]*z[51];
    z[47]=n<T>(3,2)*z[51] - 7*z[49] - n<T>(658831,13572)*z[18] + n<T>(1,1131)*z[47]
    + z[40];
    z[43]=z[47]*z[43];
    z[47]=n<T>(134933,6032)*z[2];
    z[49]= - z[7] - z[47];
    z[44]=z[49]*z[44];
    z[47]= - n<T>(75367,18096)*z[3] + z[47] - z[50] - z[10];
    z[47]=z[47]*z[53];
    z[49]=z[48] - z[28];
    z[50]=z[11] - z[10];
    z[50]=z[10]*z[50];
    z[44]=z[47] + z[44] + z[50] + n<T>(139457,36192)*z[18] + z[17] - n<T>(127393,9048)*z[15]
     + z[27] - n<T>(161323,18096)*z[49];
    z[44]=z[3]*z[44];
    z[47]=n<T>(11,4)*z[10] + n<T>(154537,12064)*z[7] + 7*z[12] - z[11];
    z[47]=z[10]*z[47];
    z[46]=n<T>(1,2)*z[47] + n<T>(154537,24128)*z[48] - n<T>(347561,36192)*z[18] - 
    z[46] + n<T>(11,4)*z[29] + z[27];
    z[46]=z[10]*z[46];
    z[47]=z[37] - z[39];
    z[48]=z[23] - z[32];
    z[47]= - n<T>(58091,2262)*z[20] - n<T>(28619,27144)*z[21] + n<T>(28619,2262)*
    z[25] + n<T>(453385,4524)*z[26] + n<T>(143227,1508)*z[30] + n<T>(181681,4524)*
    z[31] - n<T>(91955,4524)*z[33] + n<T>(130409,1508)*z[34] + n<T>(154537,1508)*
    z[35] - n<T>(122115,1508)*z[36] - 29*z[38] + n<T>(28619,4524)*z[48] + 43*
    z[47];
    z[48]=n<T>(77,24)*z[12] + n<T>(150013,18096)*z[8] + z[52];
    z[48]=z[18]*z[48];
    z[40]=z[40] - n<T>(138703,9048)*z[18];
    z[40]=z[4]*z[40];
    z[49]=npow(z[11],2);
    z[49]=n<T>(3,2)*z[49] - n<T>(2,3)*z[18] - z[29] + 3*z[17];
    z[49]=z[11]*z[49];
    z[50]=n<T>(161323,8)*z[28] - n<T>(18893,3)*z[18];
    z[50]=z[7]*z[50];

    r += n<T>(1,8)*z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + 
      n<T>(1,16)*z[47] + z[48] + z[49] + n<T>(1,2262)*z[50];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf768(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf768(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
