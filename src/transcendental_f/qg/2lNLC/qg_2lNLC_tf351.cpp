#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf351(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[11];
    z[11]=d[16];
    z[12]=d[6];
    z[13]=d[5];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[0];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[14];
    z[46]=f[16];
    z[47]=f[17];
    z[48]=f[18];
    z[49]=f[31];
    z[50]=f[37];
    z[51]=f[39];
    z[52]=f[51];
    z[53]=f[55];
    z[54]=f[58];
    z[55]=n<T>(1,2)*z[6];
    z[56]=z[1]*i;
    z[57]=z[55] - z[56];
    z[57]=z[57]*z[6];
    z[58]=2*z[56];
    z[59]=2*z[4] - z[58] + n<T>(61,2)*z[6];
    z[59]=z[4]*z[59];
    z[60]= - 42*z[56] - n<T>(31,4)*z[9];
    z[60]=z[9]*z[60];
    z[61]=25*z[56] - n<T>(19,4)*z[3];
    z[61]=z[3]*z[61];
    z[62]= - n<T>(59,3)*z[6] - 65*z[4];
    z[62]= - n<T>(55,2)*z[3] + n<T>(1,4)*z[62] + 21*z[9];
    z[62]=z[5]*z[62];
    z[59]=z[62] + z[61] + z[60] - n<T>(203,6)*z[57] + z[59];
    z[59]=z[5]*z[59];
    z[60]= - n<T>(35,2)*z[3] - 57*z[56] + z[55];
    z[61]=n<T>(1,2)*z[3];
    z[60]=z[60]*z[61];
    z[62]=n<T>(1,2)*z[7];
    z[63]= - 169*z[3] + n<T>(129,2)*z[9] - 103*z[56] + z[4];
    z[63]=n<T>(1,2)*z[63] + 51*z[5];
    z[63]=z[63]*z[62];
    z[64]=z[56]*z[6];
    z[65]=40*z[56] + n<T>(121,8)*z[9];
    z[65]=z[9]*z[65];
    z[66]=n<T>(13,2)*z[5] + 27*z[3] - 13*z[56] - 40*z[9];
    z[66]=z[5]*z[66];
    z[60]=z[63] + z[66] + z[60] + n<T>(1,4)*z[64] + z[65];
    z[60]=z[7]*z[60];
    z[63]= - 145*z[56] + n<T>(347,4)*z[6];
    z[63]=z[6]*z[63];
    z[65]=n<T>(191,2)*z[56];
    z[66]=n<T>(127,2)*z[5] - z[65] - 62*z[6];
    z[66]=z[5]*z[66];
    z[67]= - 61*z[2] + n<T>(61,4)*z[5] + 151*z[56] + n<T>(67,4)*z[6];
    z[67]=z[2]*z[67];
    z[63]=z[67] + z[63] + z[66];
    z[66]=n<T>(1,2)*z[4];
    z[67]=z[66] - z[56];
    z[68]=11*z[4];
    z[69]=z[67]*z[68];
    z[70]=n<T>(1,4)*z[7];
    z[71]=z[56] - z[6];
    z[72]=5*z[3] + z[71];
    z[72]=z[72]*z[70];
    z[73]=npow(z[9],2);
    z[74]=z[3]*z[71];
    z[63]=z[72] + n<T>(5,4)*z[74] + n<T>(11,2)*z[73] + z[69] + n<T>(1,3)*z[63];
    z[63]=z[2]*z[63];
    z[66]=z[66] + z[15];
    z[66]=z[56]*z[66];
    z[66]=n<T>(1,2)*z[22] + z[66];
    z[66]=z[10]*z[66];
    z[69]=z[56] - z[4];
    z[72]=n<T>(1,2)*z[69] - z[15];
    z[72]=z[21]*z[72];
    z[74]= - z[12] - z[13];
    z[74]=z[35]*z[74];
    z[75]=z[15] + z[13];
    z[75]=z[36]*z[75];
    z[66]=z[74] + z[75] - z[53] - z[50] + z[45] + z[66] + z[72];
    z[72]=n<T>(5,2)*z[9];
    z[74]=z[15] - z[12] - n<T>(3,2)*z[7] + z[72] - z[55] - z[67];
    z[74]=z[13]*z[74];
    z[75]=npow(z[12],2);
    z[76]=npow(z[15],2);
    z[74]=z[74] + z[75] - z[76];
    z[74]=z[13]*z[74];
    z[75]= - z[56] - z[55];
    z[75]=z[75]*npow(z[6],2);
    z[76]=n<T>(1,6)*z[15];
    z[77]= - n<T>(73,3)*z[6] - 187*z[4];
    z[77]= - n<T>(341,3)*z[3] + n<T>(1,3)*z[77] - 251*z[9];
    z[77]= - z[76] + n<T>(11,6)*z[12] + n<T>(1483,36)*z[2] - n<T>(463,12)*z[7] + n<T>(1,4)*z[77]
     + n<T>(161,9)*z[5];
    z[77]=z[22]*z[77];
    z[78]= - z[64] - z[22];
    z[78]=z[14]*z[78];
    z[74]=z[75] + z[77] + z[74] + z[78];
    z[67]=z[4]*z[67];
    z[75]=n<T>(271,6)*z[3] + n<T>(19,2)*z[9] - n<T>(13,2)*z[56] + z[4];
    z[75]=z[75]*z[61];
    z[64]=z[75] - n<T>(37,4)*z[73] + n<T>(5,4)*z[64] + z[67];
    z[64]=z[3]*z[64];
    z[67]=z[56]*z[61];
    z[73]=z[5]*z[58];
    z[67]=2*z[22] + z[67] + z[73];
    z[67]=z[8]*z[67];
    z[58]=2*z[5] - z[58] + z[61];
    z[58]=z[17]*z[58];
    z[58]=z[67] + z[58];
    z[61]=z[56]*z[68];
    z[57]= - z[57] + z[61];
    z[61]= - z[68] + n<T>(29,2)*z[56] + z[6];
    z[61]=n<T>(1,2)*z[61] + n<T>(10,3)*z[9];
    z[61]=z[9]*z[61];
    z[57]= - n<T>(9,4)*z[34] + n<T>(1,2)*z[57] + z[61];
    z[57]=z[9]*z[57];
    z[55]=z[56]*z[55];
    z[61]=z[72] + z[71];
    z[61]=z[9]*z[61];
    z[67]=n<T>(1,4)*z[71];
    z[68]=n<T>(1,6)*z[12] + z[67] - z[9];
    z[68]=z[12]*z[68];
    z[55]=z[68] + z[55] + z[61];
    z[61]=z[6] - z[4];
    z[61]=z[3]*z[61];
    z[55]=z[61] + n<T>(1,2)*z[55];
    z[55]=z[12]*z[55];
    z[61]=z[62] + z[56] + z[4];
    z[61]=z[7]*z[61];
    z[62]= - z[76] - z[56] + z[2];
    z[62]=z[15]*z[62];
    z[68]=npow(z[4],2);
    z[61]=z[62] - n<T>(1,2)*z[68] + z[61];
    z[62]= - z[2]*z[7];
    z[61]=z[62] + n<T>(1,2)*z[61];
    z[61]=z[15]*z[61];
    z[62]=z[9] + z[4];
    z[62]= - z[56]*z[62];
    z[62]= - z[22] + z[62];
    z[62]=z[11]*z[62];
    z[68]=z[9] - z[69];
    z[68]=z[18]*z[68];
    z[62]= - z[39] + z[62] + z[68];
    z[65]= - n<T>(49,2)*z[2] + z[65] - 71*z[5];
    z[65]=z[16]*z[65];
    z[68]= - n<T>(109,2)*z[2] + n<T>(281,2)*z[56] - 86*z[6];
    z[68]=z[19]*z[68];
    z[65]=z[65] + z[68];
    z[68]=z[56] - n<T>(7,8)*z[6];
    z[68]=z[6]*z[68];
    z[56]=3*z[56] - 83*z[6];
    z[56]=n<T>(1,2)*z[56] + n<T>(31,3)*z[4];
    z[56]=z[4]*z[56];
    z[56]=13*z[68] + n<T>(1,4)*z[56];
    z[56]=z[4]*z[56];
    z[68]=z[42] - z[27];
    z[69]=z[52] + z[49];
    z[71]= - n<T>(236,3)*z[28] - n<T>(151,3)*z[26] + n<T>(91,16)*z[23];
    z[71]=i*z[71];
    z[67]=z[20]*z[67];
    z[72]= - z[4] + z[2];
    z[72]=z[31]*z[72];
    z[73]=5*z[9] - z[3];
    z[73]=z[32]*z[73];
    z[75]=n<T>(3,2)*z[3] + 2*z[7];
    z[75]=z[33]*z[75];
    z[70]= - z[37]*z[70];

    r +=  - n<T>(27,2)*z[24] - n<T>(349,36)*z[25] + n<T>(118,3)*z[29] + n<T>(863,12)*
      z[30] + 11*z[38] + z[40] + n<T>(51,2)*z[41] - n<T>(203,12)*z[43] + n<T>(27,4)
      *z[44] + n<T>(45,4)*z[46] - n<T>(13,2)*z[47] + n<T>(11,3)*z[48] + n<T>(5,8)*z[51]
       - n<T>(3,8)*z[54] + z[55] + z[56] + z[57] + 15*z[58] + z[59] + z[60]
       + z[61] + n<T>(11,2)*z[62] + z[63] + z[64] + n<T>(1,3)*z[65] + n<T>(1,2)*
      z[66] + z[67] - n<T>(151,6)*z[68] - n<T>(1,8)*z[69] + z[70] + z[71] + n<T>(5,2)*z[72]
     + 2*z[73]
     + n<T>(1,4)*z[74]
     + 21*z[75];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf351(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf351(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
