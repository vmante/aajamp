#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1175(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[71];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[4];
    z[9]=d[8];
    z[10]=d[11];
    z[11]=d[16];
    z[12]=d[12];
    z[13]=d[9];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[4];
    z[17]=e[5];
    z[18]=e[6];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[31];
    z[30]=e[14];
    z[31]=f[0];
    z[32]=f[1];
    z[33]=f[3];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[17];
    z[37]=f[18];
    z[38]=f[31];
    z[39]=f[32];
    z[40]=f[33];
    z[41]=f[36];
    z[42]=f[37];
    z[43]=f[39];
    z[44]=f[43];
    z[45]=f[51];
    z[46]=f[55];
    z[47]=f[58];
    z[48]=n<T>(9,2)*z[4];
    z[49]=n<T>(3,2)*z[7];
    z[50]=3*z[2];
    z[51]= - z[48] + 3*z[6] + z[49] + z[50];
    z[52]=3*z[11];
    z[53]=n<T>(1,9)*z[10];
    z[54]=z[52] + z[53];
    z[55]=n<T>(14,9)*z[3];
    z[56]=n<T>(1,9)*z[9];
    z[57]= - z[55] + 3*z[8] - z[56] - z[54] + z[51];
    z[57]=z[3]*z[57];
    z[58]=n<T>(1,2)*z[9];
    z[59]=z[58] + 2*z[13];
    z[59]=z[59]*z[56];
    z[60]=3*z[16];
    z[59]=z[59] + z[60] - z[18];
    z[61]=3368*z[14];
    z[62]=z[61] - n<T>(2011,2)*z[8];
    z[48]=z[48] + n<T>(1,297)*z[62] - 6*z[2];
    z[48]=z[7]*z[48];
    z[62]=n<T>(2545,2)*z[2];
    z[61]=1714*z[7] + n<T>(2545,2)*z[4] - z[62] + z[61] - 2011*z[8];
    z[61]=n<T>(1,9)*z[61] - 67*z[6];
    z[61]=z[6]*z[61];
    z[63]=n<T>(1,3)*z[5];
    z[64]=z[9] + z[13];
    z[65]= - n<T>(1783,132)*z[5] + z[3] - n<T>(8153,198)*z[7] + n<T>(2417,198)*z[4]
    + n<T>(763,198)*z[2] + n<T>(1813,66)*z[8] - n<T>(611,33)*z[12] + z[64];
    z[65]=z[65]*z[63];
    z[66]=z[15] - z[20];
    z[67]=npow(z[2],2);
    z[68]= - z[22] - n<T>(3368,33)*z[21];
    z[53]= - z[13]*z[53];
    z[52]= - z[52] - n<T>(1417,1188)*z[8];
    z[52]=z[8]*z[52];
    z[69]= - n<T>(281,99)*z[4] - n<T>(562,99)*z[12] + z[50];
    z[69]=z[4]*z[69];
    z[48]=z[65] + z[57] + n<T>(1,33)*z[61] + z[48] + z[69] + 3*z[67] + z[52]
    + z[53] + n<T>(3203,594)*z[19] - n<T>(562,99)*z[17] + n<T>(1,9)*z[68] - z[59]
    - 6*z[66];
    z[48]=z[1]*z[48];
    z[48]=z[48] - 3*z[27] + n<T>(2125,1782)*z[24];
    z[48]=i*z[48];
    z[52]= - n<T>(11240,297)*z[5] + z[3] + n<T>(3647,198)*z[6] + n<T>(1813,132)*z[7]
    - n<T>(3551,198)*z[4] + n<T>(1654,99)*z[2] + n<T>(2011,132)*z[8] - z[13] + 
    z[58];
    z[52]=z[52]*z[63];
    z[53]=n<T>(827,594)*z[67];
    z[57]=npow(z[8],2);
    z[58]=47*z[17] - n<T>(665,3)*z[19];
    z[61]=npow(z[9],2);
    z[63]= - 763*z[2] + 179*z[4];
    z[63]=z[4]*z[63];
    z[65]=z[4] - z[2];
    z[68]=n<T>(8153,12)*z[7] - n<T>(1813,2)*z[8] + n<T>(827,3)*z[65];
    z[68]=z[7]*z[68];
    z[69]=n<T>(5809,594)*z[6] + z[13] + n<T>(2545,594)*z[65];
    z[69]=z[6]*z[69];
    z[70]= - n<T>(1,2)*z[3] - z[64];
    z[70]=z[3]*z[70];
    z[52]=z[52] + n<T>(1,3)*z[70] + z[69] + n<T>(1,99)*z[68] + n<T>(1,594)*z[63]
     + 
    z[53] + n<T>(1615,396)*z[57] + n<T>(1,6)*z[61] - n<T>(94,9)*z[23] + n<T>(13,99)*
    z[58] - z[18];
    z[52]=z[5]*z[52];
    z[58]= - 2011*z[18] + 3368*z[21] - n<T>(12605,2)*z[19];
    z[58]=n<T>(1,11)*z[58] + n<T>(292,3)*z[23];
    z[61]= - z[13] - n<T>(560,297)*z[8];
    z[61]=z[8]*z[61];
    z[62]=z[62] - 859*z[4];
    z[62]=z[4]*z[62];
    z[63]=1714*z[8] - 827*z[65];
    z[63]=n<T>(1,33)*z[63] - 77*z[7];
    z[63]=z[7]*z[63];
    z[65]= - n<T>(2518,891)*z[6] - n<T>(857,297)*z[7] - n<T>(1865,594)*z[4] + n<T>(1,2)*
    z[8] - n<T>(827,297)*z[2];
    z[65]=z[6]*z[65];
    z[53]=z[65] + n<T>(1,9)*z[63] + n<T>(1,297)*z[62] - z[53] + n<T>(1,27)*z[58]
     + 
    z[61];
    z[53]=z[6]*z[53];
    z[51]=n<T>(1,9)*z[64] - z[51];
    z[51]=n<T>(1,2)*z[51] + z[55];
    z[51]=z[3]*z[51];
    z[55]=z[4] - n<T>(1,2)*z[7];
    z[49]=z[55]*z[49];
    z[55]=npow(z[4],2);
    z[58]=z[8] - n<T>(1,2)*z[6];
    z[58]=z[6]*z[58];
    z[49]=z[51] + z[58] + z[49] - n<T>(3,4)*z[55] - 2*z[57] - n<T>(49,36)*z[23]
    + 3*z[15] + n<T>(1,9)*z[22] + z[59];
    z[49]=z[3]*z[49];
    z[51]= - z[2] + n<T>(1,4)*z[4];
    z[51]=z[4]*z[51];
    z[51]=z[51] - z[20];
    z[58]=n<T>(3,2)*z[67];
    z[50]= - n<T>(9,4)*z[4] + n<T>(2011,1188)*z[8] + z[50];
    z[50]=z[7]*z[50];
    z[50]=z[50] + z[58] + n<T>(2011,1188)*z[57] + n<T>(6041,3564)*z[23] - n<T>(3011,594)*z[19]
     + n<T>(3368,297)*z[21]
     + 3*z[51];
    z[50]=z[7]*z[50];
    z[51]= - z[58] + n<T>(3,2)*z[57] + n<T>(7,2)*z[23] + 3*z[66] + n<T>(4199,594)*
    z[19];
    z[51]=z[2]*z[51];
    z[57]=n<T>(1684,3)*z[14] - 281*z[12];
    z[54]=n<T>(2,99)*z[57] - z[54];
    z[54]=z[23]*z[54];
    z[57]= - z[30] - n<T>(1,3)*z[23];
    z[58]= - z[9]*z[64];
    z[57]=2*z[57] + z[58];
    z[56]=z[57]*z[56];
    z[57]=n<T>(1,2)*z[13] - n<T>(857,297)*z[8];
    z[57]=z[8]*z[57];
    z[57]=z[57] - n<T>(9529,3564)*z[23] + z[60] - n<T>(2308,297)*z[18];
    z[57]=z[8]*z[57];
    z[58]= - z[28] + z[32] + z[34] + z[42];
    z[59]=z[33] - z[35];
    z[60]= - z[45] + z[47] - n<T>(1,3)*z[46];
    z[60]=n<T>(1813,132)*z[43] + n<T>(1,2)*z[60] - n<T>(827,99)*z[44];
    z[61]= - 2*z[30] + z[22];
    z[61]=n<T>(1,9)*z[61] + z[18];
    z[61]=z[13]*z[61];
    z[55]=281*z[55] - n<T>(265,3)*z[23] + 562*z[17] - n<T>(4391,6)*z[19];
    z[55]=z[4]*z[55];

    r +=  - n<T>(1369,594)*z[25] - n<T>(7,4)*z[26] + n<T>(221,132)*z[29] + 3*z[31]
       - n<T>(3,4)*z[36] + z[37] + n<T>(11461,1188)*z[38] + n<T>(391,66)*z[39] + 
      n<T>(7888,297)*z[40] - n<T>(2011,1188)*z[41] + z[48] + z[49] + z[50] + 
      z[51] + z[52] + z[53] + z[54] + n<T>(1,99)*z[55] + z[56] + z[57] - n<T>(3,2)*z[58]
     + n<T>(9,4)*z[59]
     + n<T>(1,3)*z[60]
     + z[61];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1175(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1175(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
