#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf216(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[45];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[7];
    z[4]=d[1];
    z[5]=d[3];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=e[2];
    z[11]=e[8];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[19];
    z[16]=c[23];
    z[17]=c[25];
    z[18]=c[26];
    z[19]=c[28];
    z[20]=c[31];
    z[21]=e[0];
    z[22]=e[1];
    z[23]=e[3];
    z[24]=e[10];
    z[25]=f[3];
    z[26]=f[4];
    z[27]=f[5];
    z[28]=f[11];
    z[29]=f[12];
    z[30]=f[16];
    z[31]=f[17];
    z[32]=z[1]*i;
    z[33]=z[32]*z[5];
    z[34]=npow(z[5],2);
    z[35]=n<T>(1,2)*z[34];
    z[36]=z[33] - z[35];
    z[37]=z[32] - z[5];
    z[38]=z[4] + 2*z[37];
    z[39]=z[4]*z[38];
    z[40]=n<T>(7,2)*z[37] + 5*z[4];
    z[40]=z[6]*z[40];
    z[39]=z[40] + z[39] + z[36];
    z[39]=z[6]*z[39];
    z[33]=z[34] - z[33];
    z[40]= - z[5] - z[32];
    z[40]=n<T>(1,2)*z[40] - z[4];
    z[40]=z[4]*z[40];
    z[33]=2*z[33] + z[40];
    z[33]=z[4]*z[33];
    z[40]=2*z[5];
    z[41]= - z[40] + z[4];
    z[41]=z[32]*z[41];
    z[41]= - 2*z[12] + z[41];
    z[41]=z[7]*z[41];
    z[38]=z[10]*z[38];
    z[33]=z[38] + z[41] + z[33] + z[39];
    z[38]=npow(z[4],2);
    z[37]=z[37] + n<T>(1,2)*z[6];
    z[39]= - z[6]*z[37];
    z[37]= - n<T>(1,6)*z[9] - z[37];
    z[37]=z[9]*z[37];
    z[36]=z[37] + z[39] - n<T>(1,2)*z[38] + z[36];
    z[36]=z[9]*z[36];
    z[37]=z[40]*z[32];
    z[32]=2*z[32];
    z[38]=z[32] - z[3];
    z[39]= - z[5] - z[38];
    z[39]=z[3]*z[39];
    z[40]=n<T>(1,2)*z[3];
    z[41]=n<T>(1,6)*z[8] - z[5] + z[40];
    z[41]=z[8]*z[41];
    z[39]=z[41] + z[39] + z[35] + z[37];
    z[39]=z[8]*z[39];
    z[41]=z[3]*z[5];
    z[35]=z[41] + z[35] - z[37];
    z[35]=z[3]*z[35];
    z[37]=z[3]*z[38];
    z[32]=n<T>(1,3)*z[2] + z[5] - z[32];
    z[32]=z[2]*z[32];
    z[32]=z[32] - z[34] + 2*z[37];
    z[32]=z[2]*z[32];
    z[34]= - z[22] - z[21];
    z[37]=z[2] - z[5];
    z[34]=z[37]*z[34];
    z[37]=z[2] - z[4];
    z[37]=n<T>(1,4)*z[9] - z[40] + n<T>(5,3)*z[6] - n<T>(2,3)*z[5] - n<T>(1,2)*z[37];
    z[37]=n<T>(1,3)*z[37] + n<T>(1,4)*z[8];
    z[37]=z[12]*z[37];
    z[40]=z[15] + z[14];
    z[38]=z[2] - z[38];
    z[38]=z[11]*z[38];
    z[38]=z[38] - z[19];
    z[41]=4*z[18] - n<T>(1,18)*z[13];
    z[41]=i*z[41];
    z[42]= - z[4] - z[6];
    z[42]=z[24]*z[42];
    z[43]=2*i;
    z[43]=z[16]*z[43];
    z[44]=z[23]*z[4];

    r +=  - z[17] - n<T>(5,4)*z[20] - z[25] - n<T>(7,6)*z[26] + z[27] + z[28]
       - z[29] - n<T>(1,2)*z[30] + z[31] + z[32] + n<T>(1,3)*z[33] + z[34] + 
      z[35] + z[36] + z[37] + 2*z[38] + z[39] + n<T>(1,6)*z[40] + z[41] + n<T>(5,3)*z[42]
     + z[43]
     + z[44];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf216(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf216(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
