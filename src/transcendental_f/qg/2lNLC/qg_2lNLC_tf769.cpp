#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf769(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[54];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[8];
    z[17]=e[9];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[10];
    z[28]=e[6];
    z[29]=e[7];
    z[30]=f[3];
    z[31]=f[4];
    z[32]=f[5];
    z[33]=f[11];
    z[34]=f[12];
    z[35]=f[16];
    z[36]=f[17];
    z[37]=f[31];
    z[38]=f[36];
    z[39]=f[39];
    z[40]=n<T>(1,2)*z[2];
    z[41]=z[1]*i;
    z[42]=z[40] - z[41];
    z[43]=z[2]*z[42];
    z[44]=n<T>(10379,4)*z[41] - 1935*z[5];
    z[44]=z[5]*z[44];
    z[45]=z[41] - z[5];
    z[46]= - z[7] - z[45];
    z[46]=z[7]*z[46];
    z[47]=n<T>(3,2)*z[2] - n<T>(1,2)*z[41] + z[5];
    z[47]=n<T>(177,2)*z[3] + 5101*z[47] - n<T>(10379,2)*z[7];
    z[47]=z[3]*z[47];
    z[43]=n<T>(1,4)*z[47] + n<T>(10379,4)*z[46] + z[44] - n<T>(15303,4)*z[43];
    z[43]=z[3]*z[43];
    z[44]= - 30083*z[41] + n<T>(52249,4)*z[6];
    z[44]=z[6]*z[44];
    z[46]= - 8551*z[41] - 7283*z[6];
    z[46]=n<T>(5,4)*z[46] + 11083*z[5];
    z[46]=z[5]*z[46];
    z[44]=z[44] + z[46];
    z[46]=n<T>(52249,2)*z[6];
    z[47]= - 4747*z[5] + 74423*z[41] + z[46];
    z[47]=n<T>(1,3)*z[47] - n<T>(21289,2)*z[2];
    z[47]=z[2]*z[47];
    z[44]=n<T>(1,3)*z[44] + n<T>(1,4)*z[47];
    z[44]=z[2]*z[44];
    z[43]=z[44] + z[43];
    z[44]=n<T>(1,2)*z[5];
    z[47]=z[44] - z[41];
    z[47]=z[47]*z[5];
    z[48]=npow(z[6],2);
    z[48]=7*z[48] + n<T>(5101,754)*z[47];
    z[49]=7*z[2];
    z[50]= - z[6] + z[40];
    z[50]=z[50]*z[49];
    z[51]=n<T>(1,2)*z[9];
    z[52]=z[51] + z[45];
    z[52]=z[9]*z[52];
    z[53]= - 15657*z[45] + 25859*z[9];
    z[53]=z[7]*z[53];
    z[48]=n<T>(1,3016)*z[53] + n<T>(25859,1508)*z[52] + n<T>(1,2)*z[48] + z[50];
    z[48]=z[7]*z[48];
    z[50]=31137*z[41] - n<T>(41693,2)*z[6];
    z[50]=z[6]*z[50];
    z[52]= - n<T>(41693,2)*z[5] - 10025*z[41] + 41693*z[6];
    z[52]=z[5]*z[52];
    z[50]=z[50] + z[52];
    z[52]= - z[41]*z[49];
    z[53]= - 31137*z[6] + 10025*z[5];
    z[49]=n<T>(1,3016)*z[53] + z[49];
    z[53]=n<T>(1,2)*z[4];
    z[49]=z[49]*z[53];
    z[49]=z[49] + n<T>(1,3016)*z[50] + z[52];
    z[49]=z[4]*z[49];
    z[50]= - z[41] + n<T>(1,2)*z[6];
    z[52]=z[50]*z[6];
    z[47]=29*z[52] + n<T>(25859,1508)*z[47];
    z[51]=z[51] - n<T>(25859,6032)*z[5] + n<T>(31891,6032)*z[41] - z[6];
    z[51]=z[9]*z[51];
    z[47]=n<T>(1,2)*z[47] + z[51];
    z[47]=z[9]*z[47];
    z[46]= - z[5]*z[46];
    z[46]= - 115585*z[52] + z[46];
    z[46]=z[5]*z[46];
    z[51]=npow(z[6],3);
    z[46]=7*z[51] + n<T>(1,754)*z[46];
    z[43]=z[49] + n<T>(1,2)*z[48] + z[47] + n<T>(1,12)*z[46] + n<T>(1,377)*z[43];
    z[46]=z[41] - z[6];
    z[47]=z[9]*z[46];
    z[48]= - 23*z[46] - 15*z[9];
    z[48]=z[10]*z[48];
    z[47]=n<T>(1,8)*z[48] + n<T>(7,12)*z[18] - z[52] + n<T>(3,2)*z[47];
    z[47]=z[10]*z[47];
    z[48]= - n<T>(71237,16)*z[2] - n<T>(38231,32)*z[6] + 2111*z[5];
    z[48]= - n<T>(20581,96)*z[4] + n<T>(25859,48)*z[3] - n<T>(645,2)*z[7] + n<T>(1,9)*
    z[48] - n<T>(24705,16)*z[9];
    z[48]=z[18]*z[48];
    z[43]=z[47] + n<T>(1,2)*z[43] + n<T>(1,377)*z[48];
    z[47]=z[11] + z[6];
    z[47]=z[41]*z[47];
    z[47]=z[18] + z[47];
    z[47]=z[12]*z[47];
    z[48]=z[11] - z[46];
    z[48]=z[17]*z[48];
    z[47]=z[47] + z[48];
    z[44]=z[44] - z[3];
    z[44]=z[41]*z[44];
    z[44]=n<T>(1,2)*z[18] + z[44];
    z[44]=z[8]*z[44];
    z[45]= - n<T>(1,2)*z[45] - z[3];
    z[45]=z[15]*z[45];
    z[44]=z[44] + z[45];
    z[45]=z[6]*z[46];
    z[46]= - z[41] + n<T>(1,3)*z[11];
    z[46]=z[11]*z[46];
    z[45]=z[45] + z[46];
    z[45]=n<T>(1,3)*z[18] + 2*z[45];
    z[45]=z[11]*z[45];
    z[42]= - z[53] - z[42];
    z[42]=z[13]*z[42];
    z[40]= - z[40] - z[50];
    z[40]=z[16]*z[40];
    z[46]=z[32] - z[23];
    z[48]=z[35] + z[34];
    z[49]=z[29] + n<T>(1,4)*z[28];
    z[49]=z[9]*z[49];
    z[50]= - n<T>(178921,18096)*z[24] - n<T>(178921,36192)*z[22] + n<T>(9107,12064)*
    z[19];
    z[50]=i*z[50];
    z[41]=42755*z[41] - 52249*z[5];
    z[41]=n<T>(1,2)*z[41] + 4747*z[2];
    z[41]=z[14]*z[41];
    z[51]=z[7] + z[3];
    z[51]=z[27]*z[51];

    r +=  - n<T>(19027,12064)*z[20] - n<T>(178921,434304)*z[21] + n<T>(178921,36192)
      *z[25] + n<T>(455363,72384)*z[26] + n<T>(10025,24128)*z[30] + n<T>(15657,24128)*z[31]
     - n<T>(115585,72384)*z[33]
     - n<T>(31137,24128)*z[36]
     + n<T>(13,16)*z[37] - n<T>(19,16)*z[38] - n<T>(21,16)*z[39]
     + n<T>(52249,9048)*z[40]
     +  n<T>(1,9048)*z[41] + n<T>(7,4)*z[42] + n<T>(1,2)*z[43] + n<T>(5101,3016)*z[44]
     +  z[45] - n<T>(178921,72384)*z[46] + 2*z[47] + n<T>(25859,24128)*z[48] + 
      z[49] + z[50] + n<T>(10379,6032)*z[51];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf769(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf769(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
