#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf72(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[76];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[5];
    z[10]=d[11];
    z[11]=d[16];
    z[12]=d[4];
    z[13]=d[6];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[21];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[0];
    z[33]=e[3];
    z[34]=e[10];
    z[35]=e[6];
    z[36]=e[7];
    z[37]=e[13];
    z[38]=e[14];
    z[39]=f[0];
    z[40]=f[1];
    z[41]=f[3];
    z[42]=f[4];
    z[43]=f[5];
    z[44]=f[6];
    z[45]=f[11];
    z[46]=f[12];
    z[47]=f[16];
    z[48]=f[17];
    z[49]=f[18];
    z[50]=f[31];
    z[51]=f[36];
    z[52]=f[39];
    z[53]=f[51];
    z[54]=f[55];
    z[55]=f[56];
    z[56]=f[58];
    z[57]=npow(z[3],2);
    z[58]=npow(z[12],2);
    z[59]=n<T>(5629,432)*z[22] + 9*z[38] - n<T>(2101,36)*z[34];
    z[60]=z[13] - n<T>(3,4)*z[9];
    z[60]=z[9]*z[60];
    z[61]=2*z[14];
    z[62]= - z[13] + z[61];
    z[62]=z[14]*z[62];
    z[63]=z[14] - z[9];
    z[64]= - z[4]*z[63];
    z[61]= - z[61] + n<T>(49,8)*z[3];
    z[65]= - z[2]*z[61];
    z[66]=n<T>(2911,48)*z[12];
    z[67]= - n<T>(2317,288)*z[5] + n<T>(9,4)*z[2] - n<T>(401,9)*z[3] - z[9] + z[66];
    z[67]=z[5]*z[67];
    z[68]=n<T>(2281,36)*z[3] - 47*z[12] - z[9] - 5*z[14];
    z[68]= - n<T>(173,36)*z[5] + n<T>(1,2)*z[68] - z[4];
    z[68]=z[7]*z[68];
    z[59]=z[68] + z[67] + z[65] + z[64] + n<T>(5255,288)*z[57] - n<T>(281,12)*
    z[58] + z[62] + n<T>(1,2)*z[59] + z[60];
    z[59]=z[7]*z[59];
    z[60]=npow(z[13],2);
    z[62]= - n<T>(21,2)*z[60] + 21*z[20];
    z[64]=3*z[21];
    z[65]=21*z[15];
    z[67]= - n<T>(219,4)*z[6] - n<T>(5,4)*z[7] - n<T>(16981,72)*z[5] + n<T>(24883,72)*
    z[2] - n<T>(319,4)*z[4] - n<T>(51,4)*z[3] + n<T>(183,2)*z[12] - z[65] + 23*z[13]
   ;
    z[67]=z[6]*z[67];
    z[67]=z[67] - n<T>(10465,72)*z[16] + n<T>(2479,36)*z[17] - n<T>(22723,72)*z[19]
    - z[64] - n<T>(11,4)*z[18] + z[62];
    z[68]=npow(z[9],2);
    z[69]=z[14] - z[10] - z[13];
    z[69]=z[14]*z[69];
    z[70]=n<T>(1,2)*z[3];
    z[71]= - z[8] + z[70];
    z[71]=z[3]*z[71];
    z[72]=3*z[10] + n<T>(11,4)*z[11];
    z[73]=n<T>(15,16)*z[4] - n<T>(1,2)*z[72] + z[9];
    z[73]=z[4]*z[73];
    z[74]= - 49*z[3] - 43*z[4];
    z[74]=n<T>(1,2)*z[74] - n<T>(4207,9)*z[2];
    z[74]=z[2]*z[74];
    z[75]=n<T>(11221,144)*z[2] + n<T>(181,4)*z[4] - n<T>(392,9)*z[3] + n<T>(2815,48)*
    z[12] - n<T>(2479,72)*z[8] - z[9];
    z[75]=z[5]*z[75];
    z[66]=n<T>(211,72)*z[7] + n<T>(2173,144)*z[5] - n<T>(33,8)*z[2] + n<T>(1027,18)*z[3]
    - z[14] - z[66];
    z[66]=z[7]*z[66];
    z[66]=z[66] + z[75] + n<T>(1,4)*z[74] + z[73] + n<T>(6011,144)*z[71] + n<T>(381,32)*z[58]
     + z[69]
     + 5*z[68]
     + n<T>(1,2)*z[67];
    z[66]=z[1]*z[66];
    z[67]=8369*z[29] + 4099*z[27];
    z[66]=z[66] + n<T>(311,864)*z[23] + n<T>(1,36)*z[67] - 2*z[26];
    z[66]=i*z[66];
    z[67]=n<T>(1379,72)*z[5] + n<T>(2461,36)*z[2] + n<T>(5,2)*z[3] - n<T>(347,3)*z[4];
    z[67]=z[5]*z[67];
    z[62]= - z[67] + z[57] + n<T>(19,2)*z[68] + n<T>(29623,432)*z[22] - n<T>(2675,18)
   *z[19] + z[62];
    z[67]=9*z[13];
    z[61]= - n<T>(11429,288)*z[2] + z[67] + z[61];
    z[61]=z[2]*z[61];
    z[69]=npow(z[4],2);
    z[71]=z[14]*z[13];
    z[73]= - 17*z[3] + 5*z[2];
    z[73]=n<T>(23,2)*z[7] + n<T>(3,2)*z[73] - 5*z[5];
    z[73]=z[7]*z[73];
    z[74]=n<T>(107,72)*z[6] + n<T>(17629,288)*z[5] - n<T>(2297,36)*z[2] + n<T>(2449,96)*
    z[4] - z[70] - n<T>(183,8)*z[12] - 5*z[13] + z[14];
    z[74]=z[6]*z[74];
    z[61]=z[74] + n<T>(1,4)*z[73] + z[61] + n<T>(747,32)*z[69] + n<T>(179,8)*z[58]
    + z[71] - n<T>(1,2)*z[62];
    z[61]=z[6]*z[61];
    z[62]=n<T>(1,2)*z[68];
    z[71]=n<T>(5,2)*z[32] + 11*z[18];
    z[63]= - n<T>(31,48)*z[4] + z[63];
    z[63]=z[4]*z[63];
    z[63]=z[63] - n<T>(11,8)*z[58] - z[62] + n<T>(5281,288)*z[22] + n<T>(1,4)*z[71]
    + z[64];
    z[63]=z[4]*z[63];
    z[60]=z[60] + z[35];
    z[64]=21*z[13] - n<T>(365,144)*z[12];
    z[64]=z[12]*z[64];
    z[60]=z[64] + n<T>(23,2)*z[68] + n<T>(31729,288)*z[22] - n<T>(607,8)*z[33] - 21*
    z[60];
    z[60]=z[12]*z[60];
    z[64]= - n<T>(2479,36)*z[8] - z[65] - z[72];
    z[64]=z[22]*z[64];
    z[60]=z[64] - n<T>(3,2)*z[56] + z[55] + z[60] + z[63] + z[44] + z[51];
    z[63]= - n<T>(2435,54)*z[22] + n<T>(5417,9)*z[16] - 5*z[32] + n<T>(12023,9)*
    z[19];
    z[64]=npow(z[14],2);
    z[65]= - z[67] - z[9];
    z[65]=n<T>(8333,144)*z[2] - z[12] + n<T>(1,2)*z[65] + z[14];
    z[65]=z[2]*z[65];
    z[63]=z[65] + n<T>(43,16)*z[69] + n<T>(11,16)*z[58] + n<T>(1,16)*z[63] - z[64];
    z[63]=z[2]*z[63];
    z[64]= - n<T>(4211,48)*z[22] - n<T>(2479,4)*z[17] + 631*z[16];
    z[57]= - n<T>(185,4)*z[69] + n<T>(6155,144)*z[57] + n<T>(1,9)*z[64] - n<T>(1097,16)*
    z[58];
    z[64]= - n<T>(4913,288)*z[2] + z[9] + 2*z[12];
    z[64]=z[2]*z[64];
    z[65]= - n<T>(2911,2)*z[12] + n<T>(5525,3)*z[3];
    z[65]= - n<T>(5615,12)*z[2] + n<T>(1,4)*z[65] + 353*z[4];
    z[65]=z[5]*z[65];
    z[57]=n<T>(1,12)*z[65] + n<T>(1,2)*z[57] + z[64];
    z[57]=z[5]*z[57];
    z[64]=185*z[12] - n<T>(8213,18)*z[3];
    z[64]=z[64]*z[70];
    z[65]= - n<T>(79,9)*z[34] - 43*z[33];
    z[65]= - n<T>(1223,9)*z[22] + 23*z[65] - n<T>(6011,9)*z[17];
    z[58]=z[64] + n<T>(1,4)*z[65] + 107*z[58];
    z[58]=z[3]*z[58];
    z[58]=z[58] - z[53];
    z[64]= - z[9] - n<T>(1,3)*z[14];
    z[64]=z[14]*z[64];
    z[62]=n<T>(1,2)*z[64] + z[62] + n<T>(1,48)*z[22] + z[37] + z[21];
    z[62]=z[14]*z[62];
    z[64]= - z[36] + n<T>(571,48)*z[22];
    z[64]=z[13]*z[64];
    z[65]=z[9]*z[13];
    z[65]= - n<T>(1,2)*z[65] + z[37] - z[36];
    z[65]=z[9]*z[65];

    r += n<T>(13039,288)*z[24] + n<T>(1865,216)*z[25] - n<T>(4027,72)*z[28] - n<T>(8297,72)*z[30]
     - n<T>(122569,576)*z[31]
     + n<T>(27,8)*z[39] - n<T>(11,16)*z[40] - 
      n<T>(181,8)*z[41] - n<T>(173,36)*z[42] + n<T>(3955,72)*z[43] + n<T>(16981,288)*
      z[45] + n<T>(1065,32)*z[46] - n<T>(531,32)*z[47] + n<T>(319,16)*z[48] + n<T>(9,8)
      *z[49] - n<T>(19,4)*z[50] + n<T>(23,4)*z[52] + z[54] + z[57] + n<T>(1,4)*
      z[58] + z[59] + n<T>(1,2)*z[60] + z[61] + z[62] + z[63] + z[64] + 
      z[65] + z[66];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf72(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf72(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
