#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf540(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[14];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[70];
    z[3]=f[79];
    z[4]=d[1];
    z[5]=d[3];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=g[181];
    z[9]=g[243];
    z[10]=g[276];
    z[11]=g[308];
    z[12]= - z[7] + z[6] + z[4] - z[5];
    z[13]=i*z[1];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[13]=z[3] + z[2];
    z[12]=z[13]*z[12];

    r += z[8] + z[9] + z[10] + z[11] + z[12];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf540(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf540(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
