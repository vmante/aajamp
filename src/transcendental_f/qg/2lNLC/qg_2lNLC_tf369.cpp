#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf369(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[70];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[16];
    z[13]=d[9];
    z[14]=d[17];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[4];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[21];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[0];
    z[31]=e[3];
    z[32]=e[10];
    z[33]=e[6];
    z[34]=e[7];
    z[35]=e[14];
    z[36]=f[0];
    z[37]=f[1];
    z[38]=f[3];
    z[39]=f[4];
    z[40]=f[5];
    z[41]=f[6];
    z[42]=f[11];
    z[43]=f[12];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[18];
    z[47]=f[31];
    z[48]=f[36];
    z[49]=f[39];
    z[50]=f[56];
    z[51]= - z[6] + n<T>(33,2)*z[11];
    z[52]=5*z[7];
    z[53]= - n<T>(1,18)*z[8] - n<T>(697,9)*z[5] + n<T>(2033,18)*z[2] + z[3] - n<T>(281,6)
   *z[4] - z[52] + z[51];
    z[53]=z[8]*z[53];
    z[54]=npow(z[3],2);
    z[53]=z[54] + z[53];
    z[55]=n<T>(9,4)*z[9];
    z[56]= - z[3] + z[9];
    z[56]=z[56]*z[55];
    z[57]= - z[6] + n<T>(7,4)*z[3];
    z[58]=n<T>(7,4)*z[9];
    z[59]=n<T>(172,9)*z[2] - z[58] + 8*z[7] + z[57];
    z[59]=z[2]*z[59];
    z[60]=n<T>(1,2)*z[9];
    z[61]=n<T>(973,6)*z[4] - z[3];
    z[61]= - n<T>(104,9)*z[5] - n<T>(2231,36)*z[2] - z[60] + n<T>(1,2)*z[61] + z[6];
    z[61]=z[5]*z[61];
    z[62]=z[13] + n<T>(9,4)*z[7];
    z[62]=z[62]*z[7];
    z[62]=z[62] - 2*z[19];
    z[63]=npow(z[6],2);
    z[64]=npow(z[4],2);
    z[65]=n<T>(1,2)*z[7];
    z[66]= - n<T>(19,2)*z[11] + z[65] + z[6];
    z[66]=z[11]*z[66];
    z[53]=z[61] + z[66] + z[59] + z[56] + n<T>(3,2)*z[63] - n<T>(49,2)*z[64] + 
   n<T>(248,27)*z[20] - n<T>(2177,36)*z[18] - z[62] + n<T>(1,2)*z[53];
    z[53]=z[8]*z[53];
    z[56]=2*z[14];
    z[51]=n<T>(23,4)*z[8] + n<T>(733,9)*z[5] - n<T>(3211,36)*z[2] + z[55] - n<T>(9,4)*
    z[3] - z[52] + z[56] + n<T>(59,4)*z[4] - z[51];
    z[51]=z[8]*z[51];
    z[52]=n<T>(262,3)*z[9];
    z[55]= - z[12] + z[4];
    z[55]= - n<T>(53,4)*z[11] + z[52] + 8*z[55] - z[65];
    z[55]=z[11]*z[55];
    z[59]=1121*z[10] - n<T>(1229,2)*z[3];
    z[59]=z[3]*z[59];
    z[61]= - 1877*z[3] - n<T>(1657,2)*z[9];
    z[61]=z[9]*z[61];
    z[59]=z[59] + z[61];
    z[61]=n<T>(1,2)*z[4];
    z[66]= - 15*z[12] - z[61];
    z[66]=z[66]*z[61];
    z[57]=n<T>(3769,36)*z[2] - z[58] - n<T>(45,2)*z[4] + z[7] - z[57];
    z[57]=z[2]*z[57];
    z[67]= - n<T>(3,2)*z[6] + z[4] - z[7];
    z[67]=z[6]*z[67];
    z[68]=n<T>(548,9)*z[10];
    z[69]= - n<T>(271,3)*z[11] - n<T>(2905,36)*z[2] - n<T>(1231,36)*z[9] - 2*z[6]
     + 
   n<T>(2093,36)*z[3] + z[68] + n<T>(27,4)*z[4];
    z[69]=z[5]*z[69];
    z[51]=z[51] + z[69] + z[55] + z[57] + z[67] + z[66] + n<T>(3175,36)*
    z[15] - n<T>(548,9)*z[16] - n<T>(15,2)*z[17] + n<T>(3463,36)*z[18] + z[62] + n<T>(1,36)*z[59];
    z[51]=z[1]*z[51];
    z[55]= - 3265*z[27] - n<T>(3877,2)*z[25];
    z[51]=z[51] + n<T>(146,27)*z[21] + n<T>(1,18)*z[55] - 3*z[24];
    z[51]=i*z[51];
    z[55]=n<T>(2423,24)*z[20] + 548*z[16] - n<T>(967,2)*z[15];
    z[57]=n<T>(1531,72)*z[9] + n<T>(2021,36)*z[3] - z[6];
    z[57]=z[9]*z[57];
    z[59]=n<T>(1385,72)*z[2] + z[6] + n<T>(7,2)*z[9];
    z[59]=z[2]*z[59];
    z[52]=n<T>(91,4)*z[11] + 3*z[2] - z[6] - z[52];
    z[52]=z[11]*z[52];
    z[62]=n<T>(3751,3)*z[2] + n<T>(1123,3)*z[9] - 973*z[4] - n<T>(4267,3)*z[3];
    z[62]=n<T>(1,8)*z[62] + 131*z[11];
    z[62]=z[5]*z[62];
    z[52]=n<T>(1,3)*z[62] + z[52] + z[59] + z[57] - n<T>(1049,72)*z[54] + n<T>(1,9)*
    z[55] - n<T>(27,8)*z[64];
    z[52]=z[5]*z[52];
    z[55]=npow(z[7],2);
    z[57]=z[55] - z[54];
    z[59]=n<T>(233,8)*z[9] - z[7] + z[6];
    z[59]=z[9]*z[59];
    z[62]=npow(z[2],2);
    z[66]=z[2] - z[4];
    z[66]=n<T>(413,36)*z[11] + n<T>(721,12)*z[9] - z[6] - 69*z[3] - 9*z[7] + n<T>(31,2)*z[66];
    z[66]=z[11]*z[66];
    z[57]=n<T>(1,2)*z[66] - n<T>(3,2)*z[62] + z[59] - 2*z[63] - n<T>(7633,144)*z[20]
    + 8*z[17] + 5*z[33] + n<T>(157,4)*z[31] + n<T>(19,4)*z[57];
    z[57]=z[11]*z[57];
    z[59]= - z[6] - z[4] - n<T>(1877,18)*z[3];
    z[59]=z[59]*z[60];
    z[60]=z[7]*z[13];
    z[62]=z[6]*z[4];
    z[59]=z[59] + z[62] - n<T>(421,36)*z[54] + z[60] - n<T>(1583,108)*z[20] - 
    z[35] + n<T>(1877,36)*z[32];
    z[59]=z[9]*z[59];
    z[60]=z[6] - z[4];
    z[60]=z[7]*z[60];
    z[62]=n<T>(45,2)*z[64] + n<T>(4865,108)*z[20] - n<T>(1241,18)*z[15] + n<T>(3,2)*
    z[30] - n<T>(643,9)*z[18];
    z[58]= - z[3]*z[58];
    z[63]=n<T>(1,2)*z[6];
    z[66]= - n<T>(3433,72)*z[2] - 4*z[7] - z[63];
    z[66]=z[2]*z[66];
    z[58]=z[66] + z[58] + n<T>(1,2)*z[62] + z[60];
    z[58]=z[2]*z[58];
    z[60]=z[6] + z[7];
    z[60]=z[60]*z[63];
    z[60]=z[60] - n<T>(3,2)*z[55] - z[64] + z[34] + z[20];
    z[60]=z[60]*z[63];
    z[56]=n<T>(1,8)*z[13] + z[68] + z[56] - n<T>(15,2)*z[12];
    z[56]=z[20]*z[56];
    z[62]= - n<T>(1,2)*z[30] + 5*z[17];
    z[62]=n<T>(65,4)*z[64] + 3*z[62] - n<T>(895,72)*z[20];
    z[61]=z[62]*z[61];
    z[55]= - n<T>(1,2)*z[55] + z[64] + z[34] - n<T>(53,12)*z[20];
    z[55]=z[55]*z[65];
    z[54]=n<T>(3673,36)*z[54] - n<T>(845,36)*z[20] + n<T>(1121,18)*z[16] + n<T>(403,9)*
    z[32] + n<T>(157,2)*z[31];
    z[54]=z[3]*z[54];
    z[54]=z[54] - z[41] + z[48] - z[50];

    r +=  - n<T>(2827,72)*z[22] - n<T>(7333,432)*z[23] + n<T>(3985,72)*z[26] + n<T>(3319,36)*z[28]
     + n<T>(16765,144)*z[29]
     + n<T>(37,2)*z[36] - n<T>(31,4)*z[37] - n<T>(27,8)*z[38]
     + n<T>(1603,72)*z[39] - n<T>(4093,72)*z[40] - n<T>(733,18)*z[42] - 
      n<T>(95,4)*z[43] + n<T>(31,2)*z[44] - n<T>(59,8)*z[45] + n<T>(37,6)*z[46] + n<T>(3,2)
      *z[47] - 2*z[49] + z[51] + z[52] + z[53] + n<T>(1,2)*z[54] + z[55] + 
      z[56] + z[57] + z[58] + z[59] + z[60] + z[61];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf369(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf369(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
