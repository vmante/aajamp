#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1361(
  const std::array<std::complex<T>,111>& f
) {
    return f[30];
}

template std::complex<double> qg_2lNLC_tf1361(
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1361(
  const std::array<std::complex<dd_real>,111>& f
);
#endif
