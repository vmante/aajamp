#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf589(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[79];
    z[3]=c[11];
    z[4]=d[1];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=c[12];
    z[10]=c[13];
    z[11]=c[20];
    z[12]=c[23];
    z[13]=d[0];
    z[14]=f[24];
    z[15]=f[44];
    z[16]=f[69];
    z[17]=f[80];
    z[18]=f[22];
    z[19]=f[42];
    z[20]=f[70];
    z[21]=d[2];
    z[22]=g[24];
    z[23]=g[27];
    z[24]=g[38];
    z[25]=g[40];
    z[26]=g[48];
    z[27]=g[50];
    z[28]=g[79];
    z[29]=g[82];
    z[30]=g[90];
    z[31]=g[92];
    z[32]=g[99];
    z[33]=g[101];
    z[34]=g[170];
    z[35]=g[173];
    z[36]=g[181];
    z[37]=g[183];
    z[38]=g[189];
    z[39]=g[191];
    z[40]=g[243];
    z[41]=g[251];
    z[42]=g[254];
    z[43]=g[270];
    z[44]=g[272];
    z[45]=g[276];
    z[46]=g[278];
    z[47]=g[281];
    z[48]=g[395];
    z[49]=g[397];
    z[50]=g[398];
    z[51]=z[5] - z[6];
    z[52]=17*z[51];
    z[53]= - z[11] + n<T>(1,20)*z[12];
    z[53]= - n<T>(3,5)*z[10] + n<T>(1,4)*z[53];
    z[54]=z[53] + n<T>(31,2160)*z[3];
    z[52]=z[54]*z[52];
    z[54]=z[3]*z[7];
    z[55]=n<T>(9,2)*z[2];
    z[56]=z[1]*z[55];
    z[53]=17*z[53];
    z[57]= - z[53] - n<T>(1007,2160)*z[3];
    z[57]=z[8]*z[57];
    z[53]=z[53] + n<T>(287,2160)*z[3];
    z[53]=z[4]*z[53];
    z[52]=z[53] + z[57] + n<T>(1,3)*z[54] + z[56] + z[52];
    z[52]=i*z[52];
    z[52]=z[52] + z[49];
    z[53]=z[15] + z[16];
    z[54]=z[18] - z[19];
    z[54]= - n<T>(17,2)*z[54];
    z[53]= - z[54] + 3*z[20] - n<T>(17,2)*z[53];
    z[53]=n<T>(1,2)*z[53];
    z[56]=z[55] - z[53] + n<T>(53,3)*z[9];
    z[56]=z[8]*z[56];
    z[53]=z[55] + z[53] + n<T>(1,3)*z[9];
    z[53]=z[4]*z[53];
    z[53]=z[48] + z[39] + z[56] + z[53];
    z[56]=z[14] + z[17];
    z[54]=z[54] + 9*z[20] - n<T>(17,2)*z[56];
    z[54]=z[55] + n<T>(17,3)*z[9] + n<T>(1,2)*z[54];
    z[51]= - n<T>(1,4)*z[51];
    z[51]=z[54]*z[51];
    z[54]=z[13] - z[21];
    z[54]= - n<T>(17,16)*z[54];
    z[55]= - z[14] - z[15] + z[16] + z[17];
    z[54]=z[55]*z[54];
    z[55]= - z[34] - z[37] - z[43] - z[46] + z[28] + z[29] + z[30] + 
    z[31] + z[22] + z[23] + z[24] + z[25];
    z[56]= - z[26] + z[27] - z[32] + z[33] + z[38] + z[47];
    z[57]= - z[44] + z[35] + z[36];
    z[58]=z[41] - z[42];
    z[59]= - n<T>(9,4)*z[2] - n<T>(9,2)*z[9];
    z[59]=z[7]*z[59];

    r += n<T>(9,2)*z[40] + n<T>(9,4)*z[45] + z[50] + z[51] + n<T>(1,2)*z[52] + n<T>(1,4)
      *z[53] + z[54] - n<T>(17,8)*z[55] - n<T>(17,24)*z[56] + n<T>(3,2)*z[57] + n<T>(3,4)*z[58]
     + z[59];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf589(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf589(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
