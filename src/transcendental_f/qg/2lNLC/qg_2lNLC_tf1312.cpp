#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1312(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[50];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[11];
    z[6]=d[3];
    z[7]=d[6];
    z[8]=d[9];
    z[9]=d[4];
    z[10]=d[7];
    z[11]=d[12];
    z[12]=d[17];
    z[13]=e[9];
    z[14]=e[12];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=e[6];
    z[18]=e[7];
    z[19]=e[13];
    z[20]=e[5];
    z[21]=e[14];
    z[22]=f[30];
    z[23]=f[31];
    z[24]=f[32];
    z[25]=f[36];
    z[26]=f[39];
    z[27]=f[50];
    z[28]=f[51];
    z[29]=f[52];
    z[30]=f[55];
    z[31]=f[58];
    z[32]=z[1]*i;
    z[33]=z[32]*z[8];
    z[34]=z[2]*z[32];
    z[34]=z[15] + z[33] + z[34];
    z[34]=z[5]*z[34];
    z[35]=z[32]*z[7];
    z[36]= - z[10]*z[32];
    z[36]= - z[15] - z[35] + z[36];
    z[36]=z[12]*z[36];
    z[37]=z[32] - z[7];
    z[38]= - z[10] + z[37];
    z[38]=z[13]*z[38];
    z[39]=z[32] - z[8];
    z[40]= - z[2] + z[39];
    z[40]=z[14]*z[40];
    z[41]=z[7] + z[9];
    z[41]=z[17]*z[41];
    z[42]= - z[8] - z[4];
    z[42]=z[21]*z[42];
    z[34]=z[41] + z[42] + z[27] - z[22] + z[34] + z[36] + z[38] + z[40];
    z[36]=n<T>(1,3)*z[32];
    z[38]=z[36] + z[3];
    z[40]=n<T>(1,6)*z[9];
    z[41]=n<T>(2,3)*z[7];
    z[42]= - z[40] - z[41] + z[38];
    z[42]=z[9]*z[42];
    z[43]=n<T>(1,2)*z[3];
    z[40]= - z[40] - z[43] + z[41];
    z[40]=z[10]*z[40];
    z[44]=z[43] - z[32];
    z[44]=z[44]*z[3];
    z[45]=n<T>(1,3)*z[7];
    z[46]= - 2*z[32] + z[7];
    z[46]=z[46]*z[45];
    z[40]=z[40] + z[42] - z[44] + z[46];
    z[40]=z[10]*z[40];
    z[42]=z[36] - z[3];
    z[42]=n<T>(1,2)*z[42];
    z[46]=n<T>(1,3)*z[8];
    z[47]=n<T>(1,6)*z[2];
    z[48]= - n<T>(1,3)*z[4] + z[47] - z[42] - z[46];
    z[48]=z[4]*z[48];
    z[49]=z[43] + z[32];
    z[49]=z[49]*z[3];
    z[38]=z[47] - z[38] + n<T>(2,3)*z[8];
    z[38]=z[2]*z[38];
    z[38]=z[48] + z[38] + z[49] - n<T>(2,3)*z[33];
    z[38]=z[4]*z[38];
    z[41]=z[32]*z[41];
    z[42]=n<T>(1,3)*z[9] + z[42] + z[45];
    z[42]=z[9]*z[42];
    z[41]=z[42] - z[49] + z[41];
    z[41]=z[9]*z[41];
    z[36]= - n<T>(1,3)*z[2] - z[46] + z[36] + z[43];
    z[36]=z[2]*z[36];
    z[36]=z[44] + z[36];
    z[36]=z[2]*z[36];
    z[42]=z[3] + z[7];
    z[42]=z[18]*z[42];
    z[43]= - z[3] - z[8];
    z[43]=z[19]*z[43];
    z[42]=z[42] + z[43];
    z[43]= - z[45] - z[3] - n<T>(2,3)*z[32];
    z[43]=z[43]*npow(z[7],2);
    z[37]= - z[7]*z[37];
    z[39]=z[8]*z[39];
    z[37]=z[37] + z[39];
    z[37]=z[6]*z[37];
    z[33]=z[35] - z[33];
    z[33]=z[11]*z[33];
    z[35]= - z[31] - z[28] + z[26] + z[23];
    z[39]=z[30] - z[25];
    z[32]=z[32] + z[3];
    z[32]=z[32]*npow(z[8],2);
    z[44]=z[9] - z[4];
    z[44]= - n<T>(1,6)*z[10] + n<T>(1,2)*z[2] - z[7] + n<T>(4,3)*z[8] + n<T>(2,3)*z[44];
    z[44]=z[15]*z[44];
    z[45]= - z[7] + z[8];
    z[45]=z[20]*z[45];
    z[46]=z[16]*i;

    r +=  - z[24] + z[29] + z[32] + z[33] + n<T>(2,3)*z[34] - n<T>(1,2)*z[35]
       + z[36] + z[37] + z[38] - n<T>(1,6)*z[39] + z[40] + z[41] + 2*z[42]
       + z[43] + n<T>(1,3)*z[44] + z[45] - n<T>(1,9)*z[46];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1312(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1312(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
