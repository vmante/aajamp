#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1432(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[52];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=e[0];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=e[10];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[18];
    z[18]=c[19];
    z[19]=c[21];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[29];
    z[25]=c[30];
    z[26]=c[31];
    z[27]=f[3];
    z[28]=f[4];
    z[29]=f[6];
    z[30]=f[7];
    z[31]=f[9];
    z[32]=f[10];
    z[33]=f[11];
    z[34]=f[13];
    z[35]=f[17];
    z[36]=f[23];
    z[37]=z[5] - z[2];
    z[38]=z[37]*z[5];
    z[39]=npow(z[2],2);
    z[40]=n<T>(1,2)*z[39];
    z[41]=z[40] - z[38];
    z[42]=n<T>(1,2)*z[2];
    z[43]=z[42] - z[5];
    z[44]=z[1]*i;
    z[45]= - z[43]*z[44];
    z[43]= - n<T>(3,4)*z[3] - n<T>(3,2)*z[44] - z[43];
    z[43]=z[3]*z[43];
    z[46]=z[5] + z[2];
    z[46]= - z[44] + n<T>(1,2)*z[46];
    z[47]=z[3] - z[46];
    z[47]=z[6]*z[47];
    z[48]=z[44] + z[3] - z[5];
    z[48]=n<T>(1,2)*z[48] - n<T>(1,3)*z[7];
    z[48]=z[7]*z[48];
    z[41]=z[48] + z[47] + z[43] + n<T>(1,2)*z[41] + z[45];
    z[41]=z[7]*z[41];
    z[43]=z[2]*z[44];
    z[38]=z[43] + z[40] + z[38];
    z[43]=n<T>(1,2)*z[5];
    z[45]=z[43] - z[2];
    z[47]=n<T>(1,12)*z[3] + z[44] - z[45];
    z[47]=z[3]*z[47];
    z[38]=n<T>(1,2)*z[38] + z[47];
    z[38]=z[3]*z[38];
    z[47]= - n<T>(5,2)*z[7] - z[6] - n<T>(3,2)*z[3] + z[44] - z[37];
    z[47]=z[13]*z[47];
    z[48]=z[3] + z[5];
    z[49]= - z[44]*z[48];
    z[49]= - z[14] + z[49];
    z[49]=z[8]*z[49];
    z[48]=z[44] - z[48];
    z[48]=z[11]*z[48];
    z[50]=z[2] + n<T>(1,3)*z[3];
    z[50]=n<T>(1,12)*z[4] + n<T>(1,2)*z[50] + z[7];
    z[50]=z[14]*z[50];
    z[38]=z[49] + z[48] - z[34] - z[28] + z[38] + z[41] + z[47] + z[50];
    z[41]=n<T>(1,4)*z[5];
    z[47]= - z[2] - z[41];
    z[47]=z[5]*z[47];
    z[47]=z[40] + z[47];
    z[41]=z[41] - z[2];
    z[48]=z[41]*z[44];
    z[49]=n<T>(3,4)*z[6];
    z[50]= - n<T>(1,2)*z[6] + z[5] + z[44];
    z[50]=z[50]*z[49];
    z[41]= - z[49] - z[41];
    z[51]=n<T>(1,2)*z[4];
    z[41]=z[41]*z[51];
    z[41]=z[41] + z[50] + n<T>(1,2)*z[47] + z[48];
    z[41]=z[4]*z[41];
    z[47]=npow(z[5],2);
    z[37]= - z[3] + z[37];
    z[37]=z[3]*z[37];
    z[37]=z[37] + z[39] - n<T>(3,2)*z[47];
    z[47]= - 2*z[2] + n<T>(3,4)*z[5];
    z[47]=z[47]*z[44];
    z[45]= - z[45]*z[49];
    z[37]=z[45] + z[47] + n<T>(1,4)*z[37];
    z[37]=z[6]*z[37];
    z[42]= - z[51] - z[42] + z[44];
    z[42]=z[9]*z[42];
    z[45]=z[5]*z[2];
    z[40]=z[40] + z[45];
    z[40]=z[40]*z[43];
    z[39]=n<T>(9,4)*z[39] - z[45];
    z[39]=z[39]*z[44];
    z[43]= - z[10]*z[46];
    z[45]=z[24] + z[36] - z[32] + z[31] - z[29];
    z[46]=z[18] + z[35] + z[33];
    z[47]= - n<T>(9,2)*z[22] - n<T>(5,2)*z[20] + n<T>(1,2)*z[19] + n<T>(1,12)*z[15];
    z[47]=i*z[47];
    z[48]=npow(z[2],3);
    z[44]= - z[6] - z[2] + 2*z[44];
    z[44]=z[12]*z[44];

    r += n<T>(5,12)*z[16] - n<T>(1,3)*z[17] + n<T>(13,12)*z[21] + n<T>(5,2)*z[23]
     +  z[25] - n<T>(35,24)*z[26] - n<T>(1,8)*z[27] - 2*z[30] + z[37] + n<T>(1,2)*
      z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + n<T>(1,4)*
      z[45] - n<T>(3,8)*z[46] + z[47] - z[48];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1432(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1432(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
