#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1456(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[41];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[8];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[15];
    z[9]=d[9];
    z[10]=e[2];
    z[11]=e[10];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[31];
    z[16]=e[3];
    z[17]=e[14];
    z[18]=f[2];
    z[19]=f[4];
    z[20]=f[6];
    z[21]=f[7];
    z[22]=f[10];
    z[23]=f[12];
    z[24]=f[16];
    z[25]=f[23];
    z[26]=f[56];
    z[27]=z[1]*i;
    z[28]= - z[7] + 2*z[27];
    z[29]=2*z[3];
    z[30]=z[29] + z[4];
    z[30]=z[30]*z[28];
    z[31]=npow(z[3],2);
    z[32]=z[29] - z[4];
    z[32]=z[4]*z[32];
    z[33]=z[4] - z[3];
    z[34]=z[5]*z[33];
    z[30]=z[34] - z[31] + z[32] + z[30];
    z[30]=z[7]*z[30];
    z[32]=n<T>(11,2)*z[4];
    z[34]= - z[29] + z[32];
    z[34]=z[4]*z[34];
    z[35]=7*z[3];
    z[36]= - z[35] + z[32];
    z[36]=z[5]*z[36];
    z[37]=2*z[31];
    z[34]=z[36] - z[37] + z[34];
    z[34]=z[5]*z[34];
    z[36]=z[27] - z[5];
    z[38]=z[36] - z[7];
    z[39]=n<T>(1,2)*z[2] + z[38];
    z[33]=z[33]*z[39];
    z[39]=z[4] + z[3];
    z[40]=z[4]*z[39];
    z[33]= - z[37] + z[40] + z[33];
    z[33]=z[2]*z[33];
    z[40]= - 17*z[3] + n<T>(13,3)*z[4];
    z[40]=z[4]*z[40];
    z[37]= - z[37] + n<T>(1,2)*z[40];
    z[37]=z[4]*z[37];
    z[40]=npow(z[3],3);
    z[30]=z[22] + z[30] + z[34] + n<T>(34,3)*z[40] + z[37] + z[33] - z[25];
    z[33]=n<T>(1,2)*z[5];
    z[34]= - n<T>(1,2)*z[27] + z[33] - z[29] + n<T>(3,2)*z[4];
    z[34]=z[6]*z[34];
    z[37]= - z[29] + 3*z[4];
    z[33]=z[33] - z[37];
    z[33]=z[5]*z[33];
    z[37]= - z[5] + z[37];
    z[37]=z[37]*z[27];
    z[40]=npow(z[4],2);
    z[33]=z[34] + z[37] + z[33] - z[31] + n<T>(3,2)*z[40];
    z[33]=z[6]*z[33];
    z[32]= - z[3] - z[32];
    z[32]=z[4]*z[32];
    z[29]=z[29] - n<T>(11,3)*z[4];
    z[29]=z[5]*z[29];
    z[29]=z[29] - z[31] + n<T>(1,3)*z[32];
    z[29]=z[29]*z[27];
    z[31]=z[5] + z[3];
    z[27]=z[31]*z[27];
    z[27]=z[27] + z[12];
    z[27]=z[8]*z[27];
    z[31]=z[3] - z[36];
    z[31]=z[10]*z[31];
    z[27]=z[27] + z[31] + z[21] - z[14];
    z[31]= - z[2] + z[35] + 8*z[4] - z[38];
    z[31]=z[11]*z[31];
    z[28]= - z[7]*z[28];
    z[28]= - z[12] - z[40] + z[28];
    z[28]=z[9]*z[28];
    z[32]=z[3] + z[6];
    z[32]=z[16]*z[32];
    z[32]=z[32] + z[18];
    z[34]=n<T>(17,9)*z[3] - 5*z[4];
    z[34]= - n<T>(2,3)*z[6] - n<T>(1,6)*z[7] + n<T>(1,2)*z[34] + n<T>(2,9)*z[5];
    z[34]=z[12]*z[34];
    z[35]=z[17]*z[39];
    z[36]=z[13]*i;

    r += 5*z[15] + n<T>(13,6)*z[19] + z[20] - n<T>(1,2)*z[23] + n<T>(3,2)*z[24] + 
      z[26] + n<T>(8,3)*z[27] + z[28] + z[29] + n<T>(1,3)*z[30] + n<T>(2,3)*z[31]
       + 2*z[32] + z[33] + z[34] + z[35] + n<T>(7,18)*z[36];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1456(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1456(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
