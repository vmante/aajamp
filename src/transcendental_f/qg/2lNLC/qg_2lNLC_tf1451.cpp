#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1451(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[61];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[6];
    z[6]=d[7];
    z[7]=d[17];
    z[8]=d[4];
    z[9]=d[16];
    z[10]=d[5];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[4];
    z[14]=e[8];
    z[15]=e[9];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[21];
    z[21]=c[23];
    z[22]=c[25];
    z[23]=c[26];
    z[24]=c[28];
    z[25]=c[31];
    z[26]=e[6];
    z[27]=e[7];
    z[28]=f[0];
    z[29]=f[1];
    z[30]=f[3];
    z[31]=f[5];
    z[32]=f[11];
    z[33]=f[13];
    z[34]=f[17];
    z[35]=f[18];
    z[36]=f[31];
    z[37]=f[35];
    z[38]=f[36];
    z[39]=f[37];
    z[40]=f[39];
    z[41]=f[41];
    z[42]=n<T>(1,2)*z[3];
    z[43]=z[1]*i;
    z[44]= - z[43] + z[42];
    z[44]=z[3]*z[44];
    z[45]=z[43] - z[6];
    z[46]=3*z[8];
    z[47]=z[46] + z[45];
    z[48]=n<T>(1,2)*z[5];
    z[47]=z[47]*z[48];
    z[49]=z[6]*z[45];
    z[50]=5*z[43];
    z[51]=z[50] - 3*z[2];
    z[51]=z[2]*z[51];
    z[44]=z[47] + 3*z[44] + z[49] + z[51];
    z[44]=z[5]*z[44];
    z[47]=2*z[2];
    z[49]=n<T>(1,4)*z[3];
    z[51]=z[43] - 3*z[6];
    z[51]= - z[49] + n<T>(1,2)*z[51] + z[47];
    z[51]=z[3]*z[51];
    z[52]=n<T>(1,2)*z[6];
    z[53]=z[52] - z[43];
    z[54]=z[53]*z[6];
    z[55]=z[6] + n<T>(1,2)*z[2];
    z[56]=2*z[43];
    z[57]=z[56] - z[55];
    z[57]=z[2]*z[57];
    z[49]= - z[49] + n<T>(5,4)*z[6] - z[2];
    z[49]=z[4]*z[49];
    z[49]=z[49] + z[51] + n<T>(5,2)*z[54] + z[57];
    z[49]=z[4]*z[49];
    z[51]=n<T>(1,2)*z[8];
    z[57]=z[51] + z[45];
    z[57]=z[57]*z[46];
    z[46]=z[46] - z[45];
    z[58]=z[10] + z[5];
    z[46]=n<T>(1,2)*z[46] - z[58];
    z[46]=z[10]*z[46];
    z[46]=z[46] + z[54] + z[57];
    z[57]= - z[5]*z[45];
    z[46]=z[57] + n<T>(1,2)*z[46];
    z[46]=z[10]*z[46];
    z[57]=z[8] + z[3];
    z[59]=z[43]*z[57];
    z[59]=z[16] + z[59];
    z[59]=z[9]*z[59];
    z[57]=z[43] - z[57];
    z[57]=z[13]*z[57];
    z[60]= - z[8] - z[5];
    z[60]=z[26]*z[60];
    z[57]= - z[28] + z[59] + z[57] + z[60] + z[41];
    z[47]=z[47] - z[50] + z[52];
    z[47]=z[2]*z[47];
    z[50]=z[56] - z[6];
    z[50]=z[6]*z[50];
    z[47]=2*z[50] + z[47];
    z[47]=z[2]*z[47];
    z[50]= - 3*z[43] + z[52];
    z[59]=z[3] - z[2];
    z[50]=3*z[50] + z[59];
    z[42]=z[50]*z[42];
    z[50]=z[43] - z[2];
    z[50]=z[2]*z[50];
    z[42]=z[42] + n<T>(3,2)*z[54] + z[50];
    z[42]=z[3]*z[42];
    z[50]=z[53]*z[52];
    z[52]= - z[3]*z[43];
    z[50]=z[50] + z[52];
    z[45]= - n<T>(1,2)*z[45] + z[59];
    z[45]=3*z[45] - 5*z[8];
    z[45]=z[45]*z[51];
    z[45]=3*z[50] + z[45];
    z[45]=z[8]*z[45];
    z[50]=z[2] + z[6];
    z[51]=z[50] - z[56];
    z[52]=z[15] + 5*z[14];
    z[51]=z[51]*z[52];
    z[52]= - z[10] - n<T>(71,4)*z[3] - z[55];
    z[48]= - n<T>(1,12)*z[4] + z[48] - n<T>(3,4)*z[8] + n<T>(1,3)*z[52];
    z[48]=z[16]*z[48];
    z[43]=z[43]*z[50];
    z[43]=2*z[16] + z[43];
    z[43]=z[7]*z[43];
    z[50]=z[34] + z[40] - z[38];
    z[52]= - z[22] + z[33] + z[25];
    z[53]=z[36] - z[30];
    z[54]=z[39] - z[29];
    z[55]=13*z[23] + n<T>(13,2)*z[21] - 3*z[20] - n<T>(2,3)*z[17];
    z[55]=i*z[55];
    z[56]=z[56] - z[2];
    z[59]= - z[3] + z[56];
    z[59]=z[11]*z[59];
    z[56]=z[4] - z[56];
    z[56]=z[12]*z[56];
    z[58]= - z[27]*z[58];

    r +=  - 4*z[18] - n<T>(7,12)*z[19] - 5*z[24] - z[31] + n<T>(5,4)*z[32] - 
      z[35] - z[37] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + 
      z[48] + z[49] + n<T>(3,4)*z[50] + z[51] + 2*z[52] + n<T>(1,4)*z[53] - n<T>(3,2)*z[54]
     + z[55]
     + z[56]
     + 3*z[57]
     + z[58]
     + z[59];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1451(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1451(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
