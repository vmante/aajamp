#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf91(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[74];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[11];
    z[11]=d[16];
    z[12]=d[9];
    z[13]=d[5];
    z[14]=d[6];
    z[15]=d[17];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[0];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=e[13];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[11];
    z[43]=f[12];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[18];
    z[47]=f[31];
    z[48]=f[39];
    z[49]=f[51];
    z[50]=f[55];
    z[51]=f[58];
    z[52]=npow(z[13],2);
    z[53]=n<T>(1,2)*z[36] - n<T>(73,9)*z[33];
    z[53]=n<T>(845,54)*z[22] + 7*z[53] - n<T>(3,4)*z[52];
    z[54]=n<T>(1,2)*z[7];
    z[55]= - z[4] - 3*z[12] + n<T>(1049,9)*z[3];
    z[55]=n<T>(1,2)*z[55] - 31*z[9];
    z[55]=z[55]*z[54];
    z[56]=npow(z[12],2);
    z[57]=npow(z[3],2);
    z[58]=npow(z[9],2);
    z[59]=n<T>(1,2)*z[12];
    z[60]= - z[4]*z[59];
    z[61]= - z[12] + n<T>(17,4)*z[3];
    z[62]= - z[2]*z[61];
    z[53]=z[55] + z[62] - n<T>(343,24)*z[58] + z[60] + n<T>(1927,144)*z[57] + n<T>(1,2)*z[53]
     + z[56];
    z[53]=z[7]*z[53];
    z[55]=npow(z[14],2);
    z[60]=z[55] - z[52];
    z[62]=3*z[14];
    z[61]= - n<T>(2299,144)*z[2] + z[62] + z[61];
    z[61]=z[2]*z[61];
    z[63]=n<T>(1,2)*z[6];
    z[64]=n<T>(71,36)*z[6] + n<T>(3599,72)*z[5] - n<T>(1181,18)*z[2] - n<T>(27,2)*z[9]
    - z[62] + n<T>(629,24)*z[4];
    z[64]=z[64]*z[63];
    z[65]=npow(z[4],2);
    z[66]=17*z[3];
    z[67]= - z[66] + 5*z[2];
    z[67]=n<T>(1,4)*z[67] + 2*z[7];
    z[67]=z[7]*z[67];
    z[68]=n<T>(44,9)*z[5] - z[7] + n<T>(785,36)*z[2] + z[12] - n<T>(397,12)*z[4];
    z[68]=z[5]*z[68];
    z[60]=z[64] + z[68] + z[67] + z[61] + n<T>(27,4)*z[58] + n<T>(213,16)*z[65]
    - n<T>(1193,108)*z[22] - n<T>(11,4)*z[20] + n<T>(320,9)*z[19] + n<T>(11,8)*z[60];
    z[60]=z[6]*z[60];
    z[61]=n<T>(1405,16)*z[57] - n<T>(1285,48)*z[22] - n<T>(761,4)*z[17] + 167*z[16];
    z[64]=npow(z[2],2);
    z[67]=n<T>(911,24)*z[9];
    z[68]=z[12] - z[67] + n<T>(493,18)*z[3];
    z[69]= - n<T>(991,72)*z[7] + 2*z[2] - z[68];
    z[69]=z[7]*z[69];
    z[70]= - n<T>(761,6)*z[7] - n<T>(1561,3)*z[2] - n<T>(911,2)*z[9] + n<T>(1747,3)*z[3]
    + 403*z[4];
    z[70]=z[5]*z[70];
    z[61]=n<T>(1,24)*z[70] + z[69] - n<T>(931,144)*z[64] - n<T>(105,16)*z[58] + n<T>(1,9)
   *z[61] - n<T>(33,4)*z[65];
    z[61]=z[5]*z[61];
    z[64]=5*z[18];
    z[69]= - z[64] + 3*z[21];
    z[55]=n<T>(11,2)*z[55];
    z[70]= - z[55] - n<T>(2339,18)*z[16] + n<T>(761,9)*z[17] - n<T>(4913,18)*z[19]
    + 11*z[20] - z[69];
    z[71]= - z[10] + z[12];
    z[71]=z[12]*z[71];
    z[72]= - z[8] + n<T>(1,2)*z[3];
    z[72]=z[3]*z[72];
    z[73]=z[11] - z[4];
    z[73]=5*z[73] - n<T>(3,8)*z[9];
    z[73]=z[9]*z[73];
    z[70]=z[73] + n<T>(1405,36)*z[72] + z[71] + n<T>(1,2)*z[70] + 3*z[52];
    z[67]=n<T>(865,72)*z[7] - n<T>(13,4)*z[2] - z[67] + z[59] + n<T>(323,9)*z[3];
    z[67]=z[7]*z[67];
    z[71]= - z[15] + z[14];
    z[71]= - 79*z[4] + 11*z[71] - z[66];
    z[54]= - n<T>(33,2)*z[6] - n<T>(3491,36)*z[5] - z[54] + n<T>(5453,36)*z[2] + n<T>(1,2)*z[71]
     + 27*z[9];
    z[54]=z[54]*z[63];
    z[63]= - 3*z[10] + 5*z[11];
    z[71]=n<T>(1,4)*z[63] + z[4];
    z[71]=z[4]*z[71];
    z[66]= - z[66] + 15*z[4];
    z[66]=n<T>(1,4)*z[66] - n<T>(496,9)*z[2];
    z[66]=z[2]*z[66];
    z[68]=n<T>(689,72)*z[7] + n<T>(2483,72)*z[2] + 16*z[4] - n<T>(761,36)*z[8] - 
    z[68];
    z[68]=z[5]*z[68];
    z[54]=z[54] + z[68] + z[67] + z[66] + z[71] + n<T>(1,2)*z[70];
    z[54]=z[1]*z[54];
    z[66]=n<T>(517,48)*z[23] + n<T>(1813,2)*z[28] + 487*z[26];
    z[54]=n<T>(1,9)*z[66] + z[54];
    z[54]=i*z[54];
    z[66]= - 11*z[34] - n<T>(115,2)*z[32];
    z[55]=n<T>(51,4)*z[57] + n<T>(7547,144)*z[22] + n<T>(11,4)*z[52] - z[55] + n<T>(1,2)
   *z[66] - z[64];
    z[64]=n<T>(1,4)*z[14] + z[3];
    z[64]= - n<T>(229,144)*z[9] + 11*z[64] + n<T>(15,8)*z[4];
    z[64]=z[9]*z[64];
    z[55]=n<T>(1,2)*z[55] + z[64];
    z[55]=z[9]*z[55];
    z[64]=z[47] - z[48];
    z[52]=n<T>(1,2)*z[52];
    z[66]=z[52] + z[35];
    z[67]=z[13]*z[66];
    z[63]=n<T>(83,6)*z[14] - n<T>(761,9)*z[8] - 11*z[15] + z[63];
    z[63]=n<T>(1,2)*z[63] + n<T>(1,3)*z[13];
    z[63]=z[22]*z[63];
    z[63]=z[63] + z[67] - n<T>(991,36)*z[40] + n<T>(487,9)*z[41] + n<T>(3491,72)*
    z[42] + n<T>(105,8)*z[43] - n<T>(173,8)*z[44] + n<T>(79,4)*z[45] - n<T>(5,2)*z[46]
    - n<T>(1,4)*z[49] - n<T>(3,4)*z[51] + z[50] - n<T>(11,4)*z[64];
    z[64]=z[12] - n<T>(25,4)*z[4];
    z[64]=z[4]*z[64];
    z[52]=z[64] + n<T>(1115,72)*z[22] - z[52] - n<T>(1,2)*z[31] + z[69];
    z[52]=z[4]*z[52];
    z[58]=z[65] + z[58];
    z[62]=n<T>(1861,36)*z[2] - z[62] + z[12];
    z[62]=z[2]*z[62];
    z[56]=z[56] - z[62];
    z[62]=n<T>(1003,9)*z[16] + z[31] + n<T>(2353,9)*z[19];
    z[56]=n<T>(1,8)*z[62] - n<T>(118,27)*z[22] - n<T>(1,2)*z[56] - n<T>(15,8)*z[58];
    z[56]=z[2]*z[56];
    z[58]= - z[13] - n<T>(1,3)*z[12];
    z[58]=z[58]*z[59];
    z[58]=z[58] - n<T>(19,12)*z[22] + z[21] + z[66];
    z[58]=z[58]*z[59];
    z[57]= - n<T>(2203,9)*z[57] - n<T>(617,18)*z[22] - n<T>(1405,9)*z[17] - n<T>(1495,9)
   *z[33] - 163*z[32];
    z[57]=z[3]*z[57];

    r += n<T>(2945,144)*z[24] + n<T>(1379,216)*z[25] - n<T>(487,18)*z[27] - n<T>(1813,36)*z[29]
     - n<T>(26903,288)*z[30]
     - n<T>(15,4)*z[37]
     + n<T>(15,8)*z[38] - 8*
      z[39] + n<T>(1,4)*z[52] + z[53] + z[54] + z[55] + z[56] + n<T>(1,8)*z[57]
       + z[58] + z[60] + z[61] + n<T>(1,2)*z[63];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf91(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf91(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
