#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1320(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[28];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[19];
    z[11]=c[23];
    z[12]=c[25];
    z[13]=c[26];
    z[14]=c[28];
    z[15]=f[3];
    z[16]=f[13];
    z[17]=f[17];
    z[18]=npow(z[2],2);
    z[19]=n<T>(1,2)*z[18];
    z[20]=z[3] + z[2];
    z[21]=z[20]*z[3];
    z[22]= - z[3] + n<T>(1,2)*z[4];
    z[22]=z[4]*z[22];
    z[23]=z[3] - z[2];
    z[24]=z[5]*z[23];
    z[22]=z[24] + 3*z[22] - z[19] + z[21];
    z[22]=z[5]*z[22];
    z[24]=z[3]*z[2];
    z[19]= - z[19] - z[24];
    z[19]=z[3]*z[19];
    z[25]=z[1]*i;
    z[25]=2*z[25];
    z[26]= - z[25] + z[20];
    z[26]=z[6]*z[26];
    z[27]=npow(z[2],3);
    z[19]=z[26] + z[27] + z[19] + z[16] - z[12];
    z[26]= - 2*z[2] + n<T>(1,2)*z[3];
    z[26]=z[4]*z[26];
    z[21]=z[26] - z[18] + 2*z[21];
    z[21]=z[4]*z[21];
    z[26]= - z[5] - 2*z[4];
    z[23]=z[23]*z[26];
    z[18]= - z[18] + z[24];
    z[18]=3*z[18] + z[23];
    z[18]=z[18]*z[25];
    z[23]=z[5] - z[4];
    z[20]= - z[20] + n<T>(1,3)*z[23];
    z[20]=z[9]*z[20];
    z[20]=z[20] + z[10];
    z[23]=z[25] - z[2];
    z[24]=z[4] - z[23];
    z[24]=z[7]*z[24];
    z[24]=z[24] + z[15];
    z[25]=12*z[13] + 6*z[11];
    z[25]=i*z[25];
    z[23]=z[5] - z[23];
    z[23]=z[8]*z[23];

    r +=  - 6*z[14] + z[17] + z[18] + 3*z[19] + n<T>(1,2)*z[20] + z[21] + 
      z[22] + z[23] + 2*z[24] + z[25];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1320(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1320(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
