#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1454(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[70];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=e[10];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[19];
    z[21]=c[23];
    z[22]=c[25];
    z[23]=c[26];
    z[24]=c[28];
    z[25]=c[31];
    z[26]=e[3];
    z[27]=f[0];
    z[28]=f[1];
    z[29]=f[2];
    z[30]=f[3];
    z[31]=f[4];
    z[32]=f[5];
    z[33]=f[6];
    z[34]=f[7];
    z[35]=f[8];
    z[36]=f[9];
    z[37]=f[10];
    z[38]=f[11];
    z[39]=f[12];
    z[40]=f[13];
    z[41]=f[14];
    z[42]=f[15];
    z[43]=f[16];
    z[44]=f[17];
    z[45]=f[18];
    z[46]=f[23];
    z[47]=f[28];
    z[48]=f[29];
    z[49]=z[7] - z[2];
    z[50]=9*z[3];
    z[51]=n<T>(1,2)*z[5];
    z[52]=z[1]*i;
    z[53]=n<T>(1,2)*z[52] + z[50] - z[51] - n<T>(17,3)*z[49];
    z[53]=n<T>(1,4)*z[53] + n<T>(13,9)*z[8];
    z[54]=n<T>(1,2)*z[8];
    z[53]=z[53]*z[54];
    z[55]=z[3]*i;
    z[56]=z[5]*i;
    z[57]=15*z[55] + n<T>(1,2)*z[56];
    z[57]=z[1]*z[57];
    z[58]=z[5] - z[3];
    z[59]=z[58] - z[52];
    z[60]= - n<T>(25,4)*z[2] - z[59];
    z[61]=n<T>(1,3)*z[2];
    z[60]=z[60]*z[61];
    z[62]=npow(z[3],2);
    z[63]=3*z[3];
    z[64]= - z[63] - n<T>(1,16)*z[5];
    z[64]=z[5]*z[64];
    z[65]=z[52] + z[3];
    z[66]= - n<T>(17,8)*z[7] + n<T>(25,2)*z[2] + z[5] - n<T>(25,4)*z[65];
    z[66]=z[7]*z[66];
    z[53]=z[53] + n<T>(1,3)*z[66] + z[60] + n<T>(1,4)*z[57] + n<T>(19,8)*z[62] + 
    z[64];
    z[53]=z[53]*z[54];
    z[57]= - z[55] + n<T>(23,4)*z[56];
    z[57]=z[1]*z[57];
    z[60]=n<T>(3,2)*z[5];
    z[64]= - z[3] - z[60];
    z[64]=n<T>(15,4)*z[2] + 3*z[64] - 19*z[52];
    z[64]=z[2]*z[64];
    z[66]= - z[63] - n<T>(23,8)*z[5];
    z[66]=z[5]*z[66];
    z[57]=z[64] + z[66] + z[57];
    z[64]=z[63] - n<T>(23,4)*z[5];
    z[64]=n<T>(1,2)*z[64] - 5*z[52];
    z[66]=n<T>(41,32)*z[7];
    z[67]=n<T>(1,4)*z[4];
    z[64]=z[67] - z[66] + n<T>(1,4)*z[64] + 2*z[2];
    z[64]=z[4]*z[64];
    z[68]=z[52] + z[5];
    z[66]= - z[66] + z[3] + n<T>(41,16)*z[68];
    z[66]=z[7]*z[66];
    z[68]= - z[7] + z[65];
    z[54]=z[68]*z[54];
    z[54]=z[64] + z[54] + n<T>(1,4)*z[57] + z[66];
    z[54]=z[4]*z[54];
    z[57]=z[52] - z[5];
    z[64]=n<T>(1,2)*z[4];
    z[66]=n<T>(1,2)*z[2];
    z[63]= - n<T>(1,3)*z[6] + z[64] - n<T>(17,8)*z[8] - z[66] + z[63] + n<T>(7,8)*
    z[57];
    z[68]=n<T>(1,2)*z[6];
    z[63]=z[63]*z[68];
    z[69]= - z[55] + n<T>(7,2)*z[56];
    z[69]=z[1]*z[69];
    z[50]= - z[50] - n<T>(7,4)*z[5];
    z[50]=z[5]*z[50];
    z[50]=z[69] + n<T>(3,2)*z[62] + z[50];
    z[59]=z[2]*z[59];
    z[50]=n<T>(1,2)*z[50] - 3*z[59];
    z[59]= - z[67] - z[7] + z[60] - z[52];
    z[59]=z[4]*z[59];
    z[60]=z[7]*z[65];
    z[65]= - n<T>(17,16)*z[8] - z[3] - n<T>(25,8)*z[57];
    z[65]=z[8]*z[65];
    z[50]=z[63] + z[59] + z[65] + n<T>(1,2)*z[50] + z[60];
    z[50]=z[50]*z[68];
    z[49]=z[49] - z[57];
    z[49]=z[3] + n<T>(13,2)*z[49];
    z[49]=n<T>(1,3)*z[49] + n<T>(5,2)*z[8];
    z[49]=z[16]*z[49];
    z[59]=z[6] + z[4];
    z[60]=z[52]*z[59];
    z[60]=z[17] + z[60];
    z[60]=z[10]*z[60];
    z[59]=z[52] - z[59];
    z[59]=z[14]*z[59];
    z[49]=z[59] + z[49] + z[60];
    z[59]=z[3] - z[51];
    z[59]=z[5]*z[59];
    z[59]=n<T>(21,4)*z[62] + z[59];
    z[59]=z[5]*z[59];
    z[60]= - n<T>(19,3)*z[55] + 3*z[56];
    z[60]=z[5]*z[60];
    z[63]=i*z[62];
    z[60]= - n<T>(41,6)*z[63] + z[60];
    z[60]=z[1]*z[60];
    z[58]=z[58] + z[8];
    z[58]= - n<T>(173,6)*z[7] - n<T>(119,6)*z[2] + n<T>(173,3)*z[52] - 9*z[58];
    z[58]=z[15]*z[58];
    z[63]=npow(z[3],3);
    z[58]= - z[27] + z[58] + n<T>(1,2)*z[60] - n<T>(211,36)*z[63] + z[59];
    z[59]=n<T>(5,8)*z[55] - 14*z[56];
    z[59]=z[1]*z[59];
    z[60]=n<T>(11,4)*z[3] + 7*z[5];
    z[60]=z[5]*z[60];
    z[63]= - 115*z[2] + 323*z[52] + 23*z[3] + 11*z[5];
    z[63]=z[2]*z[63];
    z[59]=n<T>(1,16)*z[63] + z[59] - n<T>(61,16)*z[62] + z[60];
    z[59]=z[59]*z[61];
    z[60]= - z[55] + n<T>(31,2)*z[56];
    z[60]=z[1]*z[60];
    z[61]= - z[3] - n<T>(155,16)*z[5];
    z[61]=z[5]*z[61];
    z[60]=n<T>(5,4)*z[60] + n<T>(25,8)*z[62] + z[61];
    z[61]= - 2*z[3] + z[5];
    z[61]= - n<T>(17,8)*z[2] + 2*z[61] - n<T>(121,8)*z[52];
    z[61]=z[2]*z[61];
    z[60]=n<T>(1,2)*z[60] + z[61];
    z[61]=17*z[3] - n<T>(155,2)*z[5];
    z[61]=z[7] + n<T>(139,6)*z[2] + n<T>(1,6)*z[61] - 3*z[52];
    z[61]=z[7]*z[61];
    z[60]=n<T>(1,3)*z[60] + n<T>(1,8)*z[61];
    z[60]=z[7]*z[60];
    z[55]=z[55] + z[56];
    z[55]=z[1]*z[55];
    z[55]=z[55] + z[17];
    z[55]=z[9]*z[55];
    z[56]=z[3] - z[57];
    z[56]=z[13]*z[56];
    z[55]=z[55] + z[56];
    z[52]=z[66] - z[52];
    z[56]= - z[64] - z[52];
    z[56]=z[11]*z[56];
    z[51]= - z[51] - z[52];
    z[51]=z[12]*z[51];
    z[52]= - z[41] + z[48] - z[47];
    z[57]=z[36] - z[33];
    z[61]= - z[3] - z[6];
    z[61]=z[26]*z[61];
    z[61]=z[61] + z[42];
    z[62]= - n<T>(271,12)*z[23] - n<T>(323,48)*z[21] - n<T>(53,288)*z[18];
    z[62]=i*z[62];
    z[63]=n<T>(71,9)*z[2] + n<T>(11,9)*z[3] + n<T>(25,2)*z[5];
    z[63]=n<T>(23,6)*z[6] - n<T>(181,12)*z[4] - n<T>(29,9)*z[8] + n<T>(1,2)*z[63]
     + n<T>(5,9)
   *z[7];
    z[63]=z[17]*z[63];

    r +=  - n<T>(19,24)*z[19] - n<T>(47,144)*z[20] + n<T>(115,48)*z[22] + n<T>(217,24)*
      z[24] + n<T>(7,3)*z[25] + n<T>(7,8)*z[28] - n<T>(3,4)*z[29] + n<T>(1,32)*z[30]
     +  n<T>(49,96)*z[31] - n<T>(29,24)*z[32] + n<T>(13,3)*z[34] - 9*z[35] + n<T>(7,12)*
      z[37] - n<T>(191,96)*z[38] + n<T>(7,32)*z[39] - n<T>(15,8)*z[40] - n<T>(5,32)*
      z[43] - n<T>(89,32)*z[44] - n<T>(1,12)*z[45] - n<T>(13,24)*z[46] + n<T>(1,2)*
      z[49] + z[50] + n<T>(47,12)*z[51] + n<T>(3,8)*z[52] + z[53] + z[54] + n<T>(7,24)*z[55]
     + n<T>(17,4)*z[56]
     + n<T>(3,16)*z[57]
     + n<T>(1,4)*z[58]
     + z[59]
     +  z[60] + n<T>(9,8)*z[61] + z[62] + n<T>(1,8)*z[63];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1454(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1454(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
