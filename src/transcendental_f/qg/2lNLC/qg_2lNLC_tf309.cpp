#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf309(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[46];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[19];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[31];
    z[22]=e[3];
    z[23]=e[10];
    z[24]=f[0];
    z[25]=f[3];
    z[26]=f[4];
    z[27]=f[5];
    z[28]=f[11];
    z[29]=f[12];
    z[30]=f[16];
    z[31]=f[17];
    z[32]=f[20];
    z[33]=z[1]*i;
    z[34]=z[33]*z[3];
    z[35]=npow(z[3],2);
    z[36]= - z[34] + n<T>(1,2)*z[35];
    z[37]=z[33] - z[3];
    z[38]=2*z[37];
    z[39]= - z[38] - n<T>(1,2)*z[5];
    z[39]=z[5]*z[39];
    z[40]= - 2*z[5] - z[37];
    z[40]=z[6]*z[40];
    z[39]=z[40] + z[39] + z[36];
    z[39]=z[6]*z[39];
    z[40]=2*z[3];
    z[41]=z[40] + z[5];
    z[41]=z[33]*z[41];
    z[41]=2*z[13] + z[41];
    z[41]=z[7]*z[41];
    z[38]= - z[38] + z[5];
    z[38]=z[11]*z[38];
    z[42]=z[5] + z[9];
    z[42]=z[22]*z[42];
    z[43]=z[5] + 2*z[6];
    z[43]=z[23]*z[43];
    z[38]=z[24] + z[43] - z[31] + z[26] + z[25] + z[39] + z[41] + z[38]
    + z[42];
    z[39]=z[4] + z[3];
    z[41]=17*z[33];
    z[39]= - 8*z[2] + z[41] + n<T>(7,2)*z[39];
    z[39]=z[2]*z[39];
    z[42]= - 13*z[4] + 23*z[33];
    z[43]= - 10*z[3] - z[42];
    z[43]=z[4]*z[43];
    z[39]=z[39] + z[43] + 7*z[35] - 11*z[34];
    z[39]=z[2]*z[39];
    z[43]= - z[35] - z[34];
    z[40]=z[40] + z[33];
    z[44]=n<T>(3,2)*z[4];
    z[40]=2*z[40] - z[44];
    z[40]=z[4]*z[40];
    z[40]=2*z[43] + z[40];
    z[43]=z[3] - z[44];
    z[43]=3*z[43] + n<T>(1,2)*z[8];
    z[43]=z[8]*z[43];
    z[40]=3*z[40] + z[43];
    z[40]=z[8]*z[40];
    z[34]= - z[35] + z[34];
    z[43]= - z[3] - z[33];
    z[43]=n<T>(3,2)*z[43] + 5*z[5];
    z[43]=z[5]*z[43];
    z[34]=6*z[34] + z[43];
    z[34]=z[5]*z[34];
    z[41]= - n<T>(17,2)*z[4] + z[41];
    z[41]=z[3]*z[41];
    z[35]= - z[35] + z[41];
    z[35]=z[4]*z[35];
    z[41]=3*z[37] + z[6];
    z[41]=z[6]*z[41];
    z[36]=3*z[36] + z[41];
    z[37]=z[6] - n<T>(1,2)*z[37] - z[5];
    z[37]=3*z[37] - n<T>(1,2)*z[9];
    z[37]=z[9]*z[37];
    z[36]=3*z[36] + z[37];
    z[36]=z[9]*z[36];
    z[37]=z[27] - z[18];
    z[41]=z[30] - z[29];
    z[43]= - 34*z[19] + n<T>(1,2)*z[14];
    z[43]=i*z[43];
    z[44]= - n<T>(7,4)*z[8] - n<T>(17,4)*z[9] + n<T>(13,6)*z[2] + n<T>(11,6)*z[4]
     - n<T>(3,2)
   *z[6] + n<T>(4,3)*z[3] - z[5];
    z[44]=z[13]*z[44];
    z[33]= - 4*z[2] - 7*z[3] + 11*z[33];
    z[33]=z[10]*z[33];
    z[42]= - 10*z[2] + z[42];
    z[42]=z[12]*z[42];
    z[45]=17*i;
    z[45]= - z[17]*z[45];

    r +=  - n<T>(9,2)*z[15] - n<T>(5,3)*z[16] + 20*z[20] + n<T>(71,4)*z[21] - n<T>(17,2)
      *z[28] + z[32] + z[33] + z[34] + z[35] + z[36] - 10*z[37] + 3*
      z[38] + z[39] + z[40] + n<T>(3,2)*z[41] + z[42] + z[43] + z[44] + 
      z[45];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf309(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf309(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
