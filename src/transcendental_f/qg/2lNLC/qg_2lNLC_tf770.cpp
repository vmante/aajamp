#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf770(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[55];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[8];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[17];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[31];
    z[25]=e[3];
    z[26]=e[10];
    z[27]=e[6];
    z[28]=f[3];
    z[29]=f[4];
    z[30]=f[5];
    z[31]=f[6];
    z[32]=f[11];
    z[33]=f[12];
    z[34]=f[16];
    z[35]=f[17];
    z[36]=f[31];
    z[37]=f[36];
    z[38]=f[39];
    z[39]=f[78];
    z[40]=z[1]*i;
    z[41]=56267*z[40] - n<T>(87935,2)*z[6];
    z[41]=z[6]*z[41];
    z[42]= - n<T>(17587,2)*z[5] + 11065*z[40] + 17587*z[6];
    z[42]=z[5]*z[42];
    z[41]=z[41] + 5*z[42];
    z[42]=37*z[2];
    z[43]= - z[40]*z[42];
    z[44]= - 56267*z[6] - 55325*z[5];
    z[42]=n<T>(1,3016)*z[44] + z[42];
    z[44]=n<T>(1,2)*z[4];
    z[42]=z[42]*z[44];
    z[41]=z[42] + n<T>(1,3016)*z[41] + z[43];
    z[41]=z[4]*z[41];
    z[42]=n<T>(23657,48)*z[4] - n<T>(149009,24)*z[9] - n<T>(7823,24)*z[3] - n<T>(21811,3)
   *z[7] + n<T>(103267,24)*z[2] - n<T>(11509,16)*z[6] + n<T>(3931,3)*z[5];
    z[42]=n<T>(1,377)*z[42] + n<T>(11,2)*z[10];
    z[42]=z[15]*z[42];
    z[41]=z[41] + z[42];
    z[42]= - 16855*z[40] + n<T>(34841,4)*z[6];
    z[42]=z[6]*z[42];
    z[43]=63179*z[40];
    z[45]= - z[43] - 28055*z[6];
    z[45]=n<T>(1,4)*z[45] + 7485*z[5];
    z[45]=z[5]*z[45];
    z[46]= - n<T>(283033,6)*z[2] + 15677*z[5] + 112943*z[40] + n<T>(25793,2)*
    z[6];
    z[46]=z[2]*z[46];
    z[42]=n<T>(1,4)*z[46] + z[42] + z[45];
    z[42]=z[2]*z[42];
    z[45]= - z[40] + n<T>(1,2)*z[6];
    z[46]=z[45]*z[6];
    z[47]=z[40] - z[6];
    z[48]= - n<T>(3,2)*z[47] + z[9];
    z[48]=z[9]*z[48];
    z[47]= - z[9] - z[47];
    z[47]=z[10]*z[47];
    z[47]=n<T>(21,8)*z[47] - 5*z[46] + z[48];
    z[47]=z[10]*z[47];
    z[48]=npow(z[6],2);
    z[49]= - z[40] + n<T>(7,8)*z[6];
    z[49]=z[49]*z[48];
    z[45]= - 71033*z[45] - n<T>(31825,2)*z[5];
    z[45]=z[5]*z[6]*z[45];
    z[42]=z[47] + n<T>(1,754)*z[42] + z[49] + n<T>(1,6032)*z[45];
    z[45]=7739*z[3];
    z[47]=n<T>(10357,2)*z[5] + z[45];
    z[47]=z[40]*z[47];
    z[47]=n<T>(10357,2)*z[15] + z[47];
    z[47]=z[8]*z[47];
    z[49]=z[40] - z[5];
    z[45]= - n<T>(10357,2)*z[49] + z[45];
    z[45]=z[13]*z[45];
    z[45]=z[47] + z[45];
    z[47]= - z[40] + n<T>(1,2)*z[5];
    z[47]=z[47]*z[5];
    z[48]=5*z[48] + n<T>(31071,754)*z[47];
    z[50]=n<T>(1,2)*z[7];
    z[51]= - z[2] + n<T>(183379,24128)*z[5] - n<T>(135123,24128)*z[40] - z[6];
    z[51]=z[51]*z[50];
    z[52]=n<T>(21,32)*z[2] - z[40] - n<T>(5,16)*z[6];
    z[52]=z[2]*z[52];
    z[48]=z[51] + n<T>(1,32)*z[48] + z[52];
    z[48]=z[7]*z[48];
    z[51]=n<T>(34841,4)*z[40] - 8239*z[5];
    z[51]=z[5]*z[51];
    z[52]= - z[40] + n<T>(1,2)*z[2];
    z[53]=z[2]*z[52];
    z[51]=z[51] + n<T>(27427,4)*z[53];
    z[53]= - n<T>(58969,12064)*z[7] + n<T>(34841,12064)*z[5] - n<T>(46905,12064)*
    z[40] + z[6];
    z[53]=z[7]*z[53];
    z[54]=n<T>(400657,72384)*z[3] - n<T>(58969,24128)*z[7] - n<T>(27427,24128)*z[2]
    - n<T>(23217,12064)*z[5] + n<T>(5121,24128)*z[40] - z[6];
    z[54]=z[3]*z[54];
    z[51]=z[54] + n<T>(1,3016)*z[51] + z[53];
    z[51]=z[3]*z[51];
    z[43]=z[43] - 31825*z[5];
    z[43]=n<T>(1,2)*z[43] - 15677*z[2];
    z[43]=z[12]*z[43];
    z[53]= - n<T>(25793,2)*z[2] + 31825*z[40] - n<T>(37857,2)*z[6];
    z[53]=z[14]*z[53];
    z[43]=z[43] + z[53];
    z[49]=z[50] + z[49];
    z[49]=z[7]*z[49];
    z[47]=z[47] + z[49];
    z[46]=23*z[46] + n<T>(100753,1508)*z[47];
    z[40]=n<T>(76625,6032)*z[7] - n<T>(28369,6032)*z[5] + n<T>(82657,6032)*z[40]
     - 9
   *z[6];
    z[40]=n<T>(7,16)*z[9] + n<T>(1,8)*z[40] - z[3];
    z[40]=z[9]*z[40];
    z[40]=n<T>(7,8)*z[27] + n<T>(1,16)*z[46] + z[40];
    z[40]=z[9]*z[40];
    z[44]= - z[44] - z[52];
    z[44]=z[11]*z[44];
    z[46]= - z[36] + z[38] - z[37];
    z[47]=z[30] - z[21];
    z[49]= - n<T>(182625,12064)*z[22] - n<T>(182625,24128)*z[20] - n<T>(1,4)*z[18]
    + n<T>(42067,72384)*z[16];
    z[49]=i*z[49];
    z[50]=z[3] + z[9];
    z[50]=z[25]*z[50];
    z[52]=z[7] + z[3];
    z[52]=z[26]*z[52];

    r +=  - n<T>(74489,24128)*z[17] - n<T>(60875,96512)*z[19] + n<T>(182625,24128)*
      z[23] + n<T>(305779,48256)*z[24] - n<T>(55325,48256)*z[28] + n<T>(183379,48256)*z[29]
     + n<T>(1,2)*z[31] - n<T>(71033,48256)*z[32]
     + n<T>(28369,48256)*
      z[33] + n<T>(76625,48256)*z[34] - n<T>(56267,48256)*z[35] + z[39] + z[40]
       + n<T>(1,8)*z[41] + n<T>(1,4)*z[42] + n<T>(1,6032)*z[43] + n<T>(37,8)*z[44]
     + n<T>(3,6032)*z[45] - n<T>(7,32)*z[46] - n<T>(182625,48256)*z[47]
     + z[48]
     + z[49]
       + z[50] + z[51] + n<T>(71033,12064)*z[52];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf770(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf770(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
