#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf340(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[64];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[16];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=d[9];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[17];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[0];
    z[31]=e[3];
    z[32]=e[10];
    z[33]=e[6];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[18];
    z[44]=f[31];
    z[45]=f[36];
    z[46]=f[37];
    z[47]=f[39];
    z[48]=f[40];
    z[49]=n<T>(1,4)*z[5];
    z[50]= - n<T>(683,6)*z[11] + n<T>(1399,18)*z[3] - n<T>(325,9)*z[9] + n<T>(695,9)*
    z[10] - n<T>(57,2)*z[4];
    z[50]=z[50]*z[49];
    z[51]=n<T>(13,2)*z[16];
    z[52]=npow(z[6],2);
    z[53]=n<T>(13,8)*z[52];
    z[54]=npow(z[9],2);
    z[55]= - z[6] + n<T>(13,6)*z[7];
    z[55]=z[7]*z[55];
    z[56]=n<T>(9,4)*z[4] + z[13] - n<T>(13,2)*z[12];
    z[56]=z[4]*z[56];
    z[57]= - n<T>(479,2)*z[3] + 479*z[10] - n<T>(1435,2)*z[9];
    z[57]=z[3]*z[57];
    z[58]=z[4] - z[12] + n<T>(1,3)*z[7];
    z[58]= - n<T>(181,48)*z[11] + 2*z[58] + n<T>(683,24)*z[9];
    z[58]=z[11]*z[58];
    z[59]=n<T>(3,8)*z[3];
    z[60]=z[59] - z[6];
    z[61]=n<T>(3845,72)*z[2] - n<T>(653,18)*z[5] - n<T>(17,2)*z[4] + z[7] - z[60]
     - 
   n<T>(3,8)*z[9];
    z[61]=z[2]*z[61];
    z[62]=z[3] - z[9];
    z[62]=n<T>(97,24)*z[8] - n<T>(502,9)*z[2] + n<T>(3035,72)*z[5] - n<T>(83,12)*z[11]
    + n<T>(117,8)*z[4] + n<T>(3,2)*z[13] - n<T>(13,3)*z[7] + n<T>(7,8)*z[62];
    z[62]=z[8]*z[62];
    z[50]=z[62] + z[61] + z[50] + z[58] + n<T>(1,36)*z[57] - n<T>(1151,144)*
    z[54] + z[56] + z[55] - z[53] + n<T>(1261,36)*z[14] - n<T>(695,36)*z[15] - 
    z[51] - n<T>(5,2)*z[18] + n<T>(511,9)*z[17];
    z[50]=z[1]*z[50];
    z[55]= - 661*z[27] - n<T>(769,2)*z[25];
    z[55]=n<T>(239,27)*z[21] + n<T>(5,9)*z[55] + 3*z[23];
    z[50]=n<T>(1,4)*z[55] + z[50];
    z[50]=i*z[50];
    z[55]=13*z[30] - n<T>(1873,18)*z[17];
    z[56]=z[7]*z[6];
    z[57]= - z[7] + n<T>(17,4)*z[4];
    z[57]=z[4]*z[57];
    z[58]= - z[9]*z[59];
    z[59]=npow(z[11],2);
    z[61]=z[6] + n<T>(1,4)*z[9];
    z[62]=n<T>(3107,144)*z[5] - z[61];
    z[62]=z[5]*z[62];
    z[63]= - n<T>(3365,72)*z[2] + n<T>(241,18)*z[5] - z[7] + z[6];
    z[63]=z[2]*z[63];
    z[55]=n<T>(1,2)*z[63] + z[62] + n<T>(15,4)*z[59] + z[58] + z[57] + z[56] + 
   n<T>(1721,216)*z[19] + n<T>(1,4)*z[55] - n<T>(116,9)*z[14];
    z[55]=z[2]*z[55];
    z[56]=n<T>(1,2)*z[11];
    z[57]= - n<T>(683,3)*z[9] + n<T>(119,2)*z[11];
    z[57]=z[57]*z[56];
    z[58]=npow(z[4],2);
    z[59]=n<T>(355,3)*z[19] + 695*z[15] - 797*z[14];
    z[62]=1399*z[9] - 479*z[3];
    z[62]=z[3]*z[62];
    z[63]= - n<T>(151,2)*z[4] + n<T>(65,3)*z[9];
    z[63]=n<T>(683,2)*z[11] + 5*z[63] - n<T>(2699,6)*z[3];
    z[63]=z[5]*z[63];
    z[57]=n<T>(1,6)*z[63] + z[57] + n<T>(1,18)*z[62] + n<T>(1277,36)*z[54] + n<T>(1,9)*
    z[59] + n<T>(57,4)*z[58];
    z[49]=z[57]*z[49];
    z[57]=n<T>(247,36)*z[11] - n<T>(39,2)*z[3] + n<T>(449,24)*z[9] - n<T>(23,3)*z[7]
     - 
   n<T>(15,2)*z[4];
    z[56]=z[57]*z[56];
    z[57]=npow(z[7],2);
    z[57]=n<T>(13,3)*z[57];
    z[59]=npow(z[3],2);
    z[53]=z[56] - n<T>(11,4)*z[59] + n<T>(159,16)*z[54] + z[57] - n<T>(823,48)*z[19]
    - z[53] + 2*z[16] + 4*z[33] + n<T>(23,2)*z[31];
    z[53]=z[11]*z[53];
    z[56]=z[3]*z[9];
    z[56]=z[56] - z[54];
    z[52]= - n<T>(187,8)*z[58] - z[57] + n<T>(3167,216)*z[19] + n<T>(13,4)*z[52]
     + 3
   *z[18] - n<T>(2215,36)*z[17] + n<T>(7,4)*z[56];
    z[56]= - 2*z[7] - 11*z[11];
    z[56]=z[11]*z[56];
    z[57]= - n<T>(421,72)*z[5] - n<T>(5,4)*z[3] + n<T>(755,24)*z[4] + z[61];
    z[57]=z[5]*z[57];
    z[59]=n<T>(1441,144)*z[2] - n<T>(1459,72)*z[5] + n<T>(5,8)*z[9] + z[7] + z[60];
    z[59]=z[2]*z[59];
    z[60]=n<T>(1,2)*z[8] + n<T>(2017,6)*z[2] - n<T>(2999,12)*z[5] + n<T>(83,2)*z[11]
     + 
   11*z[7] - n<T>(545,4)*z[4];
    z[60]=z[8]*z[60];
    z[52]=n<T>(1,12)*z[60] + z[59] + z[57] + n<T>(1,2)*z[52] + n<T>(1,3)*z[56];
    z[52]=z[8]*z[52];
    z[56]= - 859*z[9] + 2789*z[3];
    z[56]=z[3]*z[56];
    z[54]=n<T>(1,72)*z[56] - n<T>(1291,36)*z[54] - n<T>(181,72)*z[19] + n<T>(479,18)*
    z[15] + n<T>(715,36)*z[32] + 27*z[31];
    z[54]=z[3]*z[54];
    z[56]= - n<T>(1,24)*z[20] + z[13];
    z[56]=n<T>(1,3)*z[6] + n<T>(695,18)*z[10] + 5*z[56] - 13*z[12];
    z[56]=z[19]*z[56];
    z[57]= - z[6] - n<T>(7,18)*z[7];
    z[57]=z[7]*z[57];
    z[57]= - n<T>(445,72)*z[19] + z[57];
    z[57]=z[7]*z[57];
    z[54]=z[56] + z[57] + z[54] - z[46];
    z[51]=n<T>(65,24)*z[58] - n<T>(37,36)*z[19] + z[51] - n<T>(13,4)*z[30] + z[18];
    z[51]=z[4]*z[51];
    z[56]=z[44] - z[47];
    z[57]=z[26] - z[38];
    z[58]=1291*z[32] - n<T>(2261,6)*z[19];
    z[58]=z[9]*z[58];

    r +=  - n<T>(931,72)*z[22] - n<T>(7085,864)*z[24] + n<T>(3305,72)*z[28] + n<T>(8251,144)*z[29]
     + n<T>(15,2)*z[34] - n<T>(15,4)*z[35]
     + n<T>(57,16)*z[36]
     + n<T>(1277,144)*z[37] - n<T>(3035,144)*z[39] - n<T>(119,16)*z[40]
     + n<T>(81,16)*z[41] - 
      n<T>(117,16)*z[42] + n<T>(5,2)*z[43] + n<T>(7,8)*z[45] + n<T>(1,3)*z[48] + z[49]
       + z[50] + z[51] + z[52] + z[53] + n<T>(1,2)*z[54] + z[55] + n<T>(13,8)*
      z[56] + n<T>(3845,144)*z[57] + n<T>(1,72)*z[58];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf340(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf340(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
