#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1408(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[69];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[3];
    z[14]=e[8];
    z[15]=e[10];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=f[0];
    z[25]=f[1];
    z[26]=f[2];
    z[27]=f[3];
    z[28]=f[4];
    z[29]=f[5];
    z[30]=f[6];
    z[31]=f[7];
    z[32]=f[8];
    z[33]=f[9];
    z[34]=f[10];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[13];
    z[38]=f[14];
    z[39]=f[15];
    z[40]=f[16];
    z[41]=f[17];
    z[42]=f[18];
    z[43]=f[23];
    z[44]=f[26];
    z[45]=f[27];
    z[46]=f[28];
    z[47]=f[29];
    z[48]=n<T>(1,2)*z[8];
    z[49]=3*z[4];
    z[50]= - n<T>(239,24)*z[8] + z[6] + z[49];
    z[50]=z[50]*z[48];
    z[51]=n<T>(1,2)*z[7];
    z[52]=n<T>(7,12)*z[8];
    z[53]=z[52] + z[6];
    z[54]= - n<T>(13,24)*z[7] - z[53];
    z[54]=z[54]*z[51];
    z[55]=z[4] - z[6];
    z[55]= - n<T>(2,3)*z[8] + n<T>(3,4)*z[55];
    z[56]=n<T>(1,6)*z[7];
    z[57]=n<T>(5,48)*z[2] + z[56] - z[55];
    z[57]=z[2]*z[57];
    z[58]=3*z[9] - n<T>(1,3)*z[6];
    z[56]=z[56] - n<T>(97,6)*z[8] + n<T>(13,2)*z[58] + z[4];
    z[56]=z[1]*z[56];
    z[58]=z[2]*z[1];
    z[56]=z[56] - n<T>(1,6)*z[58];
    z[56]=i*z[56];
    z[59]=npow(z[4],2);
    z[60]=npow(z[6],2);
    z[61]= - n<T>(1,2)*z[14] + n<T>(39,4)*z[12] + n<T>(41,3)*z[15];
    z[62]=i*z[1];
    z[63]=n<T>(959,3)*z[3] - 127*z[62] - 55*z[2] + 7*z[7] - 25*z[6] - 109*
    z[8];
    z[63]=z[3]*z[63];
    z[50]=n<T>(1,48)*z[63] + n<T>(1,4)*z[56] + z[57] + z[54] + z[50] + n<T>(5,8)*
    z[59] - n<T>(71,12)*z[60] + n<T>(107,144)*z[16] + n<T>(223,24)*z[13] + n<T>(1,2)*
    z[61] + z[10];
    z[50]=z[3]*z[50];
    z[54]=n<T>(7,3)*z[15];
    z[56]=n<T>(29,3)*z[11];
    z[57]=39*z[12] + z[56];
    z[61]=3*z[6] - n<T>(5,8)*z[4];
    z[61]=z[4]*z[61];
    z[57]=z[61] + z[14] + n<T>(1,2)*z[57] + z[54];
    z[61]=n<T>(1,3)*z[8];
    z[63]= - z[6] + n<T>(7,2)*z[4];
    z[63]= - n<T>(47,48)*z[2] - n<T>(2,3)*z[7] + n<T>(3,4)*z[63] + z[61];
    z[63]=z[2]*z[63];
    z[64]=n<T>(199,6)*z[8];
    z[65]= - n<T>(143,6)*z[7] - z[64] + n<T>(5,2)*z[4] + 39*z[9] - n<T>(289,6)*z[6];
    z[65]=z[1]*z[65];
    z[65]=n<T>(1,8)*z[65] + n<T>(8,3)*z[58];
    z[65]=i*z[65];
    z[66]=n<T>(13,3)*z[8];
    z[67]=n<T>(7,6)*z[6] - z[49];
    z[67]=n<T>(1,2)*z[67] + z[66];
    z[67]= - n<T>(59,48)*z[3] + n<T>(127,24)*z[62] + n<T>(5,12)*z[2] + n<T>(1,2)*z[67]
    + n<T>(1,3)*z[7];
    z[67]=z[3]*z[67];
    z[68]=n<T>(143,3)*z[7] + n<T>(199,3)*z[8] + n<T>(289,3)*z[6] - 5*z[4];
    z[62]= - n<T>(1,8)*z[5] - n<T>(61,12)*z[3] + n<T>(3,8)*z[62] + n<T>(1,32)*z[68]
     - n<T>(4,3)*z[2];
    z[62]=z[5]*z[62];
    z[68]=n<T>(289,96)*z[60] + 3*z[13];
    z[64]= - 101*z[6] + z[64];
    z[64]=z[8]*z[64];
    z[61]=n<T>(143,96)*z[7] - n<T>(37,16)*z[4] - z[61];
    z[61]=z[7]*z[61];
    z[57]=z[62] + z[67] + z[65] + z[63] + z[61] + n<T>(1,16)*z[64] + n<T>(169,288)*z[16]
     + z[68]
     + n<T>(1,4)*z[57];
    z[57]=z[5]*z[57];
    z[61]= - z[13] + n<T>(5,8)*z[60];
    z[62]=7*z[15];
    z[63]=n<T>(95,2)*z[14] + n<T>(29,2)*z[11] - z[62];
    z[63]=n<T>(1,3)*z[63] - n<T>(1,2)*z[10];
    z[64]= - z[4] - n<T>(13,48)*z[8];
    z[64]=z[8]*z[64];
    z[52]= - n<T>(103,48)*z[7] + z[4] + z[52];
    z[52]=z[7]*z[52];
    z[65]=n<T>(33,2)*z[2] - n<T>(13,3)*z[7] - n<T>(15,2)*z[4] - n<T>(7,3)*z[8];
    z[65]=z[2]*z[65];
    z[52]=n<T>(1,8)*z[65] + z[52] + z[64] - n<T>(1,2)*z[59] - n<T>(11,36)*z[16] + n<T>(1,4)*z[63]
     + z[61];
    z[52]=z[2]*z[52];
    z[54]=9*z[10] - n<T>(101,3)*z[14] - z[54] - n<T>(39,2)*z[12] - z[56];
    z[49]= - n<T>(199,48)*z[8] + n<T>(101,8)*z[6] + z[49];
    z[49]=z[49]*z[48];
    z[53]= - n<T>(11,4)*z[7] - n<T>(21,8)*z[4] - z[53];
    z[51]=z[53]*z[51];
    z[49]=z[51] + z[49] - z[59] + n<T>(1,4)*z[54] - z[68];
    z[49]=z[1]*z[49];
    z[51]=n<T>(121,24)*z[7] - z[55];
    z[51]=z[1]*z[51];
    z[51]=z[51] - n<T>(179,48)*z[58];
    z[51]=z[2]*z[51];
    z[53]=n<T>(23,8)*z[17] + 103*z[22] + n<T>(179,4)*z[20];
    z[49]=z[51] + n<T>(1,12)*z[53] + z[49];
    z[49]=i*z[49];
    z[51]= - z[4] + n<T>(13,24)*z[8];
    z[48]=z[51]*z[48];
    z[51]=z[62] + n<T>(101,2)*z[14];
    z[53]=z[6] + n<T>(21,16)*z[4];
    z[53]=z[4]*z[53];
    z[54]=n<T>(21,2)*z[4] + z[66];
    z[54]=n<T>(1,2)*z[54] + n<T>(11,3)*z[7];
    z[54]=z[7]*z[54];
    z[48]=n<T>(1,8)*z[54] + z[48] + n<T>(1,2)*z[53] - n<T>(205,144)*z[16] + n<T>(1,12)*
    z[51] - z[10];
    z[48]=z[7]*z[48];
    z[51]=n<T>(89,3)*z[15] + z[14];
    z[53]=n<T>(93,4)*z[6] + n<T>(23,9)*z[8];
    z[53]=z[8]*z[53];
    z[51]=n<T>(1,8)*z[53] + n<T>(93,32)*z[60] - n<T>(175,96)*z[16] + n<T>(1,4)*z[51]
     - 2
   *z[13];
    z[51]=z[8]*z[51];
    z[53]= - n<T>(5,8)*z[6] + n<T>(1,3)*z[4];
    z[53]=z[4]*z[53];
    z[53]=z[53] - n<T>(57,32)*z[16] - n<T>(9,8)*z[10] - z[61];
    z[53]=z[4]*z[53];
    z[54]=z[30] - z[33];
    z[55]=z[16]*z[9];
    z[55]=z[55] - z[18];
    z[56]=z[44] - z[46];
    z[56]= - 65*z[39] + n<T>(121,4)*z[40] + n<T>(5,4)*z[41] + n<T>(5,3)*z[42] - n<T>(1,3)
   *z[43] - z[45] + z[47] - 3*z[56];
    z[58]=n<T>(1,8)*z[60] - n<T>(1261,288)*z[16] - z[10] + n<T>(247,24)*z[13];
    z[58]=z[6]*z[58];

    r +=  - n<T>(7,36)*z[19] - n<T>(33,16)*z[21] - n<T>(121,24)*z[23] - n<T>(21,8)*
      z[24] - z[25] + n<T>(17,12)*z[26] - n<T>(13,32)*z[27] + n<T>(191,96)*z[28]
     - 
      n<T>(2,3)*z[29] + n<T>(7,3)*z[31] + n<T>(17,8)*z[32] - n<T>(31,12)*z[34] + n<T>(155,96)*z[35]
     - n<T>(277,96)*z[36]
     + n<T>(3,2)*z[37]
     + z[38]
     + z[48]
     + z[49]
       + z[50] + z[51] + z[52] + z[53] + n<T>(17,16)*z[54] + n<T>(39,8)*z[55]
       + n<T>(1,8)*z[56] + z[57] + z[58];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1408(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1408(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
