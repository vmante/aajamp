#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1079(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[38];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=e[6];
    z[17]=f[31];
    z[18]=f[32];
    z[19]=f[33];
    z[20]=f[36];
    z[21]=f[39];
    z[22]=f[43];
    z[23]=z[1]*i;
    z[24]=2*z[4];
    z[25]= - z[24] + 5*z[3];
    z[25]=z[25]*z[23];
    z[26]=npow(z[3],2);
    z[26]=n<T>(3,2)*z[26];
    z[27]=npow(z[4],2);
    z[28]=3*z[4];
    z[29]=z[28] - n<T>(5,2)*z[3];
    z[29]=z[8]*z[29];
    z[25]=z[29] + z[25] + z[27] - z[26];
    z[25]=z[8]*z[25];
    z[29]=2*z[3];
    z[30]= - z[8] + z[29];
    z[31]=z[3] - z[4];
    z[30]=z[31]*z[30];
    z[32]=2*z[23];
    z[33]= - z[4] - z[3];
    z[33]=z[33]*z[32];
    z[34]=z[23] - z[5];
    z[35]=z[4] + z[34];
    z[35]=z[5]*z[35];
    z[30]=z[35] + z[33] + z[27] + z[30];
    z[30]=z[5]*z[30];
    z[33]=z[7] + z[4];
    z[35]=n<T>(3,2)*z[3];
    z[36]=n<T>(1,2)*z[8];
    z[37]= - z[36] + n<T>(1,2)*z[23] - z[35] + z[33];
    z[37]=z[7]*z[37];
    z[24]= - z[24] + 3*z[3];
    z[36]= - z[36] + z[23] + z[24];
    z[36]=z[8]*z[36];
    z[24]= - z[24]*z[23];
    z[24]=z[37] + z[36] - z[26] + z[24];
    z[24]=z[7]*z[24];
    z[26]=z[5] + z[3];
    z[26]=z[23]*z[26];
    z[26]=z[13] + z[26];
    z[26]=z[6]*z[26];
    z[36]= - z[3] + z[34];
    z[36]=z[10]*z[36];
    z[33]=z[16]*z[33];
    z[26]=z[15] + z[26] + z[36] + z[33] - z[18];
    z[33]=z[8] + z[4];
    z[36]= - z[23]*z[33];
    z[36]= - z[13] + z[36];
    z[36]=z[9]*z[36];
    z[33]=z[23] - z[33];
    z[33]=z[12]*z[33];
    z[33]=z[36] + z[33];
    z[34]= - n<T>(1,2)*z[2] + z[8] - z[34];
    z[31]=z[31]*z[34];
    z[29]=z[4] - z[29];
    z[29]=z[3]*z[29];
    z[29]=z[27] + z[29] + z[31];
    z[29]=z[2]*z[29];
    z[28]= - z[28] + n<T>(23,3)*z[3];
    z[28]=z[3]*z[28];
    z[28]= - 3*z[27] + n<T>(1,2)*z[28];
    z[28]=z[3]*z[28];
    z[31]=z[4] + z[35];
    z[31]=z[3]*z[31];
    z[27]=z[27] + z[31];
    z[23]=z[27]*z[23];
    z[27]= - z[8] + z[2] - z[5];
    z[27]= - z[32] + 7*z[4] + 9*z[3] - 2*z[27];
    z[27]=z[11]*z[27];
    z[31]=npow(z[4],3);
    z[32]=n<T>(2,3)*z[7] + n<T>(1,2)*z[5] - n<T>(1,3)*z[8] - z[4] + n<T>(10,3)*z[3];
    z[32]=z[13]*z[32];
    z[34]=n<T>(1,2)*i;
    z[34]= - z[14]*z[34];

    r +=  - n<T>(7,2)*z[17] - 8*z[19] + n<T>(1,2)*z[20] - n<T>(3,2)*z[21] + z[22]
       + z[23] + z[24] + z[25] + 2*z[26] + z[27] + z[28] + z[29] + 
      z[30] + n<T>(2,3)*z[31] + z[32] + 4*z[33] + z[34];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1079(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1079(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
