#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf172(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[61];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[6];
    z[6]=d[7];
    z[7]=d[1];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=d[4];
    z[12]=d[5];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[19];
    z[23]=c[23];
    z[24]=c[25];
    z[25]=c[26];
    z[26]=c[28];
    z[27]=c[31];
    z[28]=e[0];
    z[29]=e[3];
    z[30]=e[10];
    z[31]=e[7];
    z[32]=f[0];
    z[33]=f[1];
    z[34]=f[3];
    z[35]=f[4];
    z[36]=f[5];
    z[37]=f[11];
    z[38]=f[12];
    z[39]=f[16];
    z[40]=f[17];
    z[41]=f[18];
    z[42]=f[31];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=11*z[4];
    z[47]=z[1]*i;
    z[48]= - n<T>(25,2)*z[6] + z[46] + n<T>(65,2)*z[47];
    z[48]=z[6]*z[48];
    z[49]=z[47]*z[4];
    z[50]=npow(z[4],2);
    z[51]=n<T>(13,2)*z[2] - n<T>(37,4)*z[6] - n<T>(1,4)*z[4] - 10*z[47];
    z[51]=z[2]*z[51];
    z[48]=z[51] + z[48] - 5*z[50] - n<T>(1,2)*z[49];
    z[48]=z[2]*z[48];
    z[51]=n<T>(5,2)*z[7];
    z[52]=z[51] + z[4];
    z[52]= - z[47]*z[52];
    z[52]= - z[19] + z[52];
    z[52]=z[9]*z[52];
    z[53]=n<T>(1,2)*z[2];
    z[54]=z[53] - n<T>(11,2)*z[47] + 5*z[4];
    z[54]=z[14]*z[54];
    z[55]=z[47] - z[4];
    z[51]= - z[51] + z[55];
    z[51]=z[15]*z[51];
    z[56]=npow(z[4],3);
    z[57]=n<T>(31,2)*z[2] - n<T>(59,2)*z[47] + 14*z[6];
    z[57]=z[17]*z[57];
    z[58]=n<T>(19,2)*z[7] + 8*z[8];
    z[58]=z[30]*z[58];
    z[48]=z[51] + z[57] + z[58] + z[41] - z[56] + z[48] + z[52] + z[54];
    z[51]=2*z[6];
    z[52]= - n<T>(9,2)*z[4] + z[47];
    z[52]=n<T>(1,12)*z[3] + z[53] + n<T>(1,2)*z[52] + z[51];
    z[52]=z[3]*z[52];
    z[54]=n<T>(1,2)*z[50];
    z[56]=z[54] + z[49];
    z[57]=7*z[47];
    z[58]= - 9*z[4] - z[57];
    z[51]=n<T>(1,2)*z[58] + z[51];
    z[51]=z[6]*z[51];
    z[58]=npow(z[11],2);
    z[59]= - z[2]*z[47];
    z[51]=z[52] - n<T>(1,2)*z[58] + z[59] + n<T>(9,2)*z[56] + z[51];
    z[51]=z[3]*z[51];
    z[52]=z[54] - z[49];
    z[54]=n<T>(1,2)*z[6];
    z[56]=z[47] - z[54];
    z[56]=z[6]*z[56];
    z[46]= - z[46] + z[57];
    z[46]=n<T>(5,12)*z[11] + n<T>(1,4)*z[46] + z[53] + z[6];
    z[46]=z[11]*z[46];
    z[46]=z[46] + n<T>(1,2)*z[52] + 3*z[56];
    z[46]=z[11]*z[46];
    z[56]= - z[6]*z[47];
    z[56]=z[56] - z[19];
    z[56]=z[13]*z[56];
    z[57]=z[47] - z[6];
    z[57]=z[18]*z[57];
    z[59]= - z[12] - z[5];
    z[59]=z[31]*z[59];
    z[56]=z[59] + z[56] + z[57];
    z[57]= - 2*z[50] + 5*z[49];
    z[59]=z[4] + z[47];
    z[59]=n<T>(5,12)*z[59] + z[11];
    z[59]=z[7]*z[59];
    z[57]=z[59] + n<T>(1,3)*z[57] + z[58];
    z[57]=z[7]*z[57];
    z[58]=z[11] + z[55];
    z[58]=z[11]*z[58];
    z[52]= - n<T>(7,3)*z[52] + z[58];
    z[58]= - 5*z[55] - n<T>(13,4)*z[7];
    z[58]=z[7]*z[58];
    z[55]= - n<T>(13,3)*z[55] + z[11];
    z[55]=n<T>(1,2)*z[55] - n<T>(8,3)*z[7];
    z[55]=z[8]*z[55];
    z[52]=z[55] + n<T>(1,2)*z[52] + n<T>(1,3)*z[58];
    z[52]=z[8]*z[52];
    z[55]=z[47] - z[11];
    z[54]= - n<T>(5,3)*z[12] - z[2] + z[54] + z[4] - n<T>(1,2)*z[55];
    z[54]=z[12]*z[54];
    z[55]=2*z[4];
    z[58]= - z[55] + z[2];
    z[58]=z[2]*z[58];
    z[54]=z[54] + z[50] + z[58];
    z[54]=z[12]*z[54];
    z[58]= - z[6] + 2*z[47];
    z[53]=z[53] - z[58];
    z[53]=z[2]*z[53];
    z[59]=npow(z[6],2);
    z[58]=n<T>(2,3)*z[5] + z[12] - z[4] + z[58];
    z[58]=z[5]*z[58];
    z[53]=z[58] + n<T>(1,6)*z[19] + n<T>(1,2)*z[59] + z[53];
    z[53]=z[5]*z[53];
    z[58]=z[2] - z[3];
    z[58]=z[28]*z[58];
    z[59]= - 3*z[11] - 5*z[7];
    z[59]=z[29]*z[59];
    z[58]= - z[58] - z[59] - z[44] - z[43] + z[42] + z[33];
    z[49]=n<T>(5,4)*z[50] - 19*z[49];
    z[50]=n<T>(19,6)*z[4] - z[47];
    z[50]=z[6]*z[50];
    z[49]=n<T>(1,3)*z[49] + z[50];
    z[49]=z[6]*z[49];
    z[50]=z[36] - z[24];
    z[59]=n<T>(35,3)*z[25] + n<T>(29,6)*z[23] - n<T>(1,36)*z[20];
    z[59]=i*z[59];
    z[55]=n<T>(11,12)*z[2] - z[55] - n<T>(25,6)*z[6];
    z[55]=n<T>(1,6)*z[12] - n<T>(11,36)*z[8] + n<T>(2,3)*z[7] + n<T>(11,8)*z[3] + n<T>(1,3)*
    z[55] + n<T>(13,8)*z[11];
    z[55]=z[19]*z[55];
    z[60]= - z[3]*z[47];
    z[60]=z[60] - z[19];
    z[60]=z[10]*z[60];
    z[47]= - z[47] + z[3];
    z[47]=z[16]*z[47];

    r += n<T>(25,12)*z[21] - n<T>(7,72)*z[22] - n<T>(35,6)*z[26] - n<T>(211,24)*z[27]
       + z[32] - n<T>(9,4)*z[34] + n<T>(13,6)*z[35] + n<T>(19,6)*z[37] + n<T>(11,4)*
      z[38] + n<T>(3,4)*z[39] + n<T>(7,4)*z[40] + z[45] + z[46] + z[47] + n<T>(1,3)
      *z[48] + z[49] + n<T>(29,12)*z[50] + z[51] + z[52] + z[53] + z[54] + 
      z[55] + 2*z[56] + z[57] - n<T>(1,2)*z[58] + z[59] + z[60];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf172(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf172(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
