#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1029(
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[8];
  std::complex<T> r(0,0);

    z[1]=d[1];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[8];
    z[5]= - z[2]*z[1];
    z[6]=2*z[1] - z[3];
    z[6]=z[3]*z[6];
    z[7]= - z[1] + z[2];
    z[7]=z[4]*z[7];

    r += z[5] + z[6] + z[7];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1029(
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1029(
  const std::array<std::complex<dd_real>,25>& d
);
#endif
