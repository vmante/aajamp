#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1098(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[80];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[9];
    z[13]=d[11];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[17];
    z[28]=c[31];
    z[29]=e[3];
    z[30]=e[6];
    z[31]=e[13];
    z[32]=e[14];
    z[33]=f[4];
    z[34]=f[6];
    z[35]=f[7];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[23];
    z[39]=f[31];
    z[40]=f[32];
    z[41]=f[33];
    z[42]=f[36];
    z[43]=f[39];
    z[44]=f[43];
    z[45]=f[51];
    z[46]=f[52];
    z[47]=f[55];
    z[48]=f[56];
    z[49]=f[58];
    z[50]=f[59];
    z[51]=4*z[16];
    z[52]=z[51] + n<T>(139,132)*z[15];
    z[53]=n<T>(259,44)*z[5];
    z[54]=2*z[9];
    z[55]=n<T>(4,3)*z[3];
    z[56]=z[54] - z[55] - z[53] + n<T>(545,88)*z[8] - 4*z[12] + z[52];
    z[57]=n<T>(1,3)*z[9];
    z[56]=z[56]*z[57];
    z[58]=n<T>(8,3)*z[21];
    z[59]=z[58] - n<T>(71,11)*z[18] + 4*z[17];
    z[60]= - n<T>(139,132)*z[20] + 4*z[22];
    z[61]=8*z[23];
    z[62]= - n<T>(199,66)*z[19] - 11*z[14] + z[61] + z[60] + z[59];
    z[63]=2*z[12];
    z[64]=n<T>(4,9)*z[2];
    z[51]=z[51] - n<T>(47,4)*z[11];
    z[51]= - n<T>(7,24)*z[6] - n<T>(8,9)*z[9] - z[64] - n<T>(2,3)*z[8] + n<T>(1,3)*z[51]
    - z[63];
    z[51]=z[6]*z[51];
    z[65]=n<T>(1,3)*z[12];
    z[66]=5*z[13] - z[11];
    z[66]=z[66]*z[65];
    z[67]=n<T>(1,3)*z[8];
    z[68]=n<T>(2,3)*z[3];
    z[69]=z[68] - z[10] - z[67];
    z[69]=z[69]*z[55];
    z[70]=n<T>(71,11)*z[14] + 4*z[10];
    z[71]=2*z[6] - z[55] + n<T>(953,132)*z[5] + n<T>(10,3)*z[8] - z[70];
    z[71]=n<T>(1,3)*z[71] - n<T>(31,22)*z[7];
    z[71]=z[7]*z[71];
    z[72]= - n<T>(71,12)*z[6] + n<T>(1063,264)*z[8];
    z[73]=n<T>(1,2)*z[12];
    z[74]= - n<T>(377,176)*z[4] + n<T>(1015,396)*z[7] - n<T>(359,792)*z[9] + n<T>(953,396)*z[2]
     - z[73]
     - n<T>(13,12)*z[11]
     - n<T>(203,66)*z[14]
     + z[13] - z[72];
    z[74]=z[4]*z[74];
    z[75]=npow(z[8],2);
    z[76]=n<T>(8,3)*z[13] + z[11];
    z[76]=z[11]*z[76];
    z[77]=n<T>(1345,6)*z[5] + n<T>(139,3)*z[15] + 259*z[8];
    z[77]=z[5]*z[77];
    z[78]=4*z[3];
    z[79]= - n<T>(953,44)*z[5] + z[78];
    z[79]=z[2]*z[79];
    z[51]=z[74] + z[71] + z[51] + z[56] + n<T>(1,9)*z[79] + z[69] + n<T>(1,132)*
    z[77] + n<T>(403,1584)*z[75] + z[66] + n<T>(1,3)*z[62] + z[76];
    z[51]=z[1]*z[51];
    z[56]=3*z[27] + n<T>(1015,1188)*z[25];
    z[51]=n<T>(1,4)*z[56] + z[51];
    z[51]=i*z[51];
    z[56]=z[73] - z[11];
    z[62]=n<T>(5,2)*z[12];
    z[56]=z[56]*z[62];
    z[62]=2*z[8] - z[3];
    z[62]=z[62]*z[68];
    z[66]=5*z[12];
    z[69]=n<T>(953,66)*z[5];
    z[71]= - z[66] + z[69];
    z[55]=n<T>(1,2)*z[71] - z[55];
    z[55]=z[2]*z[55];
    z[71]= - z[78] + n<T>(229,22)*z[5];
    z[74]= - z[71]*z[57];
    z[76]=npow(z[5],2);
    z[77]=z[9] - z[2];
    z[77]=z[8] - n<T>(2,3)*z[77];
    z[77]=2*z[77] - 5*z[6];
    z[77]=z[6]*z[77];
    z[66]=11 + z[66];
    z[66]=n<T>(235,66)*z[7] - z[6] + n<T>(8,3)*z[3] - n<T>(181,33)*z[5] + n<T>(1,2)*
    z[66] - n<T>(5,3)*z[8];
    z[66]=z[7]*z[66];
    z[55]=z[66] + z[77] + z[74] + z[55] + z[62] - n<T>(371,66)*z[76] + n<T>(7,3)
   *z[75] + z[56] - n<T>(347,88)*z[24] - n<T>(97,132)*z[19] - z[59];
    z[55]=z[7]*z[55];
    z[59]= - z[8] - n<T>(11,3)*z[3];
    z[59]=z[59]*z[68];
    z[62]= - n<T>(7,3)*z[21] + n<T>(1,3)*z[29] - z[17];
    z[59]=z[59] - n<T>(4,3)*z[75] + 4*z[62] - n<T>(1,9)*z[24];
    z[59]=z[3]*z[59];
    z[52]=8*z[13] + z[52] - z[70];
    z[52]=z[24]*z[52];
    z[62]=npow(z[11],2);
    z[61]= - n<T>(11,3)*z[62] - z[61] + n<T>(13,8)*z[24];
    z[61]=z[11]*z[61];
    z[52]=z[52] + 2*z[48] + z[50] + n<T>(41,8)*z[49] + z[61] + z[59] + z[55]
    - z[37];
    z[55]= - z[11] + z[12];
    z[55]=n<T>(229,66)*z[2] + 5*z[55] - z[69];
    z[55]=z[2]*z[55];
    z[59]=n<T>(19,66)*z[7] + n<T>(229,33)*z[9] - n<T>(623,66)*z[2] + 5*z[11] + z[69]
   ;
    z[59]=z[7]*z[59];
    z[55]=z[55] + z[59];
    z[59]= - n<T>(26,3) - z[11];
    z[59]=n<T>(1,3)*z[59] - z[73];
    z[59]=z[12]*z[59];
    z[61]= - n<T>(1063,264)*z[9] + n<T>(623,99)*z[2] + n<T>(527,396)*z[5] - z[65]
     + 
   n<T>(19,9) + n<T>(29,4)*z[11] - z[72];
    z[61]= - n<T>(2725,2376)*z[4] + n<T>(1,2)*z[61] - n<T>(259,99)*z[7];
    z[61]=z[4]*z[61];
    z[66]=n<T>(359,24)*z[9] + n<T>(1063,4)*z[8] - n<T>(229,3)*z[2];
    z[66]=z[9]*z[66];
    z[69]=n<T>(41,8)*z[6] + n<T>(40,3) - n<T>(71,4)*z[11];
    z[69]=z[6]*z[69];
    z[55]=z[61] + n<T>(1,3)*z[69] + n<T>(1,66)*z[66] + n<T>(295,792)*z[76] - n<T>(491,528)*z[75]
     + z[59]
     + n<T>(1,24)*z[62] - n<T>(97,72)*z[24] - n<T>(365,396)*z[19]
    + n<T>(203,66)*z[18] - n<T>(11,3)*z[31] - z[23] + n<T>(1,6)*z[55];
    z[55]=z[4]*z[55];
    z[54]= - z[54] + z[68] + n<T>(29,12)*z[5] + z[63] - n<T>(545,176)*z[8];
    z[54]=z[9]*z[54];
    z[59]= - z[8] + n<T>(1,2)*z[5];
    z[53]=z[59]*z[53];
    z[59]=n<T>(1,3)*z[2];
    z[61]=z[71]*z[59];
    z[63]=npow(z[3],2);
    z[53]=z[54] + z[61] - n<T>(2,3)*z[63] + z[53] - n<T>(545,176)*z[75] + n<T>(337,1584)*z[24]
     - n<T>(229,33)*z[19]
     - z[60]
     - z[58];
    z[53]=z[53]*z[57];
    z[54]=2*z[3];
    z[57]= - z[8] + n<T>(47,8)*z[11] - 7*z[12];
    z[57]= - n<T>(41,27)*z[6] + n<T>(4,9)*z[9] - z[64] + n<T>(1,3)*z[57] + z[54];
    z[57]=z[6]*z[57];
    z[58]= - z[54] - z[2];
    z[58]=z[2]*z[58];
    z[60]=z[3] + z[2];
    z[60]=2*z[60] - z[9];
    z[60]=z[9]*z[60];
    z[58]=z[58] + z[60];
    z[60]= - n<T>(20,9) + z[11];
    z[60]=2*z[60] + z[65];
    z[60]=z[12]*z[60];
    z[61]= - 2*z[32] - z[22];
    z[61]=n<T>(1,3)*z[61] - z[21];
    z[57]=z[57] - n<T>(1,3)*z[75] + z[60] + n<T>(47,24)*z[62] + 4*z[61] - n<T>(25,27)
   *z[24] + n<T>(2,9)*z[58];
    z[57]=z[6]*z[57];
    z[54]= - n<T>(229,44)*z[5] + z[54];
    z[54]=z[54]*z[59];
    z[58]=z[24] + 8*z[21] + n<T>(1411,44)*z[19];
    z[54]=z[54] + n<T>(8,3)*z[63] - n<T>(229,66)*z[76] + n<T>(1,3)*z[58] - z[56];
    z[54]=z[54]*z[59];
    z[56]=259*z[30] + n<T>(139,3)*z[20];
    z[56]=n<T>(1,2)*z[56] - n<T>(149,3)*z[19];
    z[56]=n<T>(109,99)*z[76] + n<T>(259,44)*z[75] + n<T>(1,11)*z[56] + n<T>(97,18)*z[24]
   ;
    z[56]=z[5]*z[56];
    z[58]= - 2*z[62] - 8*z[32] - 5*z[23];
    z[58]=z[58]*z[65];
    z[59]=n<T>(259,88)*z[75] + n<T>(779,198)*z[24] + n<T>(259,44)*z[30] + n<T>(4,3)*
    z[29];
    z[59]=z[59]*z[67];

    r += n<T>(11,3)*z[18] - n<T>(23,792)*z[26] - n<T>(5975,176)*z[28] - n<T>(19,9)*
      z[33] - n<T>(2,3)*z[34] - n<T>(32,9)*z[35] - n<T>(7,9)*z[36] + n<T>(4,9)*z[38]
     +  n<T>(2191,1584)*z[39] + n<T>(100,33)*z[40] - n<T>(331,198)*z[41] + n<T>(545,528)*
      z[42] - n<T>(491,528)*z[43] - n<T>(229,198)*z[44] + n<T>(13,24)*z[45] - n<T>(5,12)
      *z[46] - n<T>(13,8)*z[47] + z[51] + n<T>(1,3)*z[52] + z[53] + z[54] + 
      z[55] + n<T>(1,6)*z[56] + z[57] + z[58] + z[59];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1098(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1098(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
