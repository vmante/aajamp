#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf751(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[5];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[31];
    z[25]=e[3];
    z[26]=e[10];
    z[27]=e[6];
    z[28]=e[14];
    z[29]=f[3];
    z[30]=f[4];
    z[31]=f[5];
    z[32]=f[11];
    z[33]=f[12];
    z[34]=f[16];
    z[35]=f[17];
    z[36]=f[31];
    z[37]=f[36];
    z[38]=f[39];
    z[39]=f[51];
    z[40]=f[55];
    z[41]=f[58];
    z[42]=n<T>(1,2)*z[6];
    z[43]=z[1]*i;
    z[44]=z[42] - z[43];
    z[45]=z[44]*z[42];
    z[42]= - n<T>(23,12)*z[7] - n<T>(5,3)*z[43] + z[42];
    z[42]=z[7]*z[42];
    z[46]=n<T>(1,2)*z[2];
    z[47]=z[46] - z[43];
    z[48]=z[2]*z[47];
    z[49]=z[43] + z[7];
    z[50]= - z[5] + z[49];
    z[50]=z[5]*z[50];
    z[51]=n<T>(1,3)*z[43] - z[6];
    z[51]=n<T>(13,4)*z[3] - n<T>(5,4)*z[2] - n<T>(19,12)*z[5] + n<T>(1,2)*z[51] - n<T>(4,3)*
    z[7];
    z[51]=z[3]*z[51];
    z[42]=z[51] + n<T>(5,2)*z[48] + n<T>(7,6)*z[50] + z[45] + z[42];
    z[42]=z[3]*z[42];
    z[45]=3*z[6];
    z[48]=z[44]*z[45];
    z[50]=n<T>(1,2)*z[7];
    z[51]=z[50] + z[43];
    z[52]=n<T>(7,2)*z[7];
    z[53]=z[51]*z[52];
    z[54]=n<T>(1,2)*z[5];
    z[55]=z[54] - z[49];
    z[55]=z[5]*z[55];
    z[56]=z[54] + n<T>(3,2)*z[7] + n<T>(9,2)*z[43] - 5*z[6];
    z[56]=z[10] + n<T>(1,2)*z[56] - z[3];
    z[56]=z[10]*z[56];
    z[53]=z[56] + n<T>(7,2)*z[55] + z[48] + z[53];
    z[55]=n<T>(1,2)*z[10];
    z[53]=z[53]*z[55];
    z[44]=z[6]*z[44];
    z[52]= - z[43] + z[52];
    z[56]=7*z[7];
    z[52]=z[52]*z[56];
    z[57]=11*z[6];
    z[56]= - z[57] + z[56];
    z[56]=z[56]*z[54];
    z[44]=z[56] - 41*z[44] + z[52];
    z[44]=z[5]*z[44];
    z[52]=npow(z[6],2);
    z[56]=z[52]*z[43];
    z[45]= - n<T>(25,6)*z[43] - z[45];
    z[45]=n<T>(1,2)*z[45] - n<T>(1,3)*z[7];
    z[45]=z[7]*z[45];
    z[45]=n<T>(1,2)*z[52] + z[45];
    z[45]=z[7]*z[45];
    z[52]=z[3] + z[10];
    z[52]=z[25]*z[52];
    z[44]= - z[39] + z[52] + n<T>(1,6)*z[44] + z[56] + z[45] + z[41] - z[40]
   ;
    z[45]=n<T>(11,2)*z[5];
    z[52]= - z[45] + 14*z[43];
    z[56]=n<T>(11,2)*z[6];
    z[58]= - z[56] - z[52];
    z[58]=z[5]*z[58];
    z[59]= - 17*z[43] + n<T>(31,4)*z[6];
    z[59]=z[6]*z[59];
    z[58]=z[59] + z[58];
    z[51]= - z[6] - z[51];
    z[50]=z[51]*z[50];
    z[51]=7*z[43];
    z[59]=z[51] + z[6];
    z[59]= - n<T>(43,6)*z[2] + n<T>(17,6)*z[5] + n<T>(7,3)*z[59] + z[7];
    z[46]=z[59]*z[46];
    z[46]=z[46] + z[50] + n<T>(1,3)*z[58];
    z[46]=z[2]*z[46];
    z[45]= - z[45] + 5*z[43] + z[57];
    z[45]=z[5]*z[45];
    z[50]=z[51] - z[56];
    z[50]=z[6]*z[50];
    z[45]=z[50] + z[45];
    z[50]=z[7] + 3*z[2];
    z[56]= - z[43]*z[50];
    z[57]=n<T>(1,2)*z[4];
    z[50]= - n<T>(5,4)*z[5] - n<T>(7,4)*z[6] + z[50];
    z[50]=z[50]*z[57];
    z[45]=z[50] + z[56] + n<T>(1,4)*z[45];
    z[45]=z[4]*z[45];
    z[50]= - n<T>(17,2)*z[2] + z[52];
    z[50]=z[13]*z[50];
    z[52]=31*z[43] - 17*z[6];
    z[52]=n<T>(1,2)*z[52] - 7*z[2];
    z[52]=z[15]*z[52];
    z[56]=n<T>(13,2)*z[7] + 8*z[3];
    z[56]=z[26]*z[56];
    z[50]=z[56] + z[50] + z[52];
    z[52]=19*z[3];
    z[56]=z[43]*z[52];
    z[51]=z[5]*z[51];
    z[51]=7*z[16] + z[51] + z[56];
    z[51]=z[8]*z[51];
    z[56]= - z[43] + z[5];
    z[52]=7*z[56] + z[52];
    z[52]=z[14]*z[52];
    z[51]=z[51] + z[52];
    z[52]=z[43] - z[6];
    z[55]=z[55] - z[52];
    z[55]=z[10]*z[55];
    z[56]=npow(z[7],2);
    z[48]=z[55] - z[48] + z[56];
    z[43]=z[57] - z[43];
    z[43]= - z[4]*z[43];
    z[52]= - z[10] - z[52];
    z[52]=z[9]*z[52];
    z[43]=n<T>(3,4)*z[52] + n<T>(1,2)*z[48] + z[43];
    z[43]=z[9]*z[43];
    z[48]=n<T>(1,2)*z[11] - z[7];
    z[49]=z[49] - z[6];
    z[48]=z[49]*z[48];
    z[48]= - 2*z[28] + n<T>(1,6)*z[16] + z[48];
    z[48]=z[11]*z[48];
    z[49]=n<T>(29,4)*z[6] - 37*z[7];
    z[49]=n<T>(1,9)*z[49] + z[54];
    z[49]=n<T>(4,3)*z[9] + n<T>(1,24)*z[4] - n<T>(13,12)*z[10] - n<T>(7,12)*z[3]
     + n<T>(1,2)
   *z[49] + n<T>(11,9)*z[2];
    z[49]=z[16]*z[49];
    z[47]= - z[57] - z[47];
    z[47]=z[12]*z[47];
    z[52]= - z[36] + z[38] - z[37];
    z[54]=z[31] - z[21];
    z[55]= - n<T>(77,6)*z[22] - n<T>(77,12)*z[20] + n<T>(49,72)*z[17];
    z[55]=i*z[55];
    z[56]=z[27]*z[10];

    r +=  - n<T>(13,6)*z[18] - n<T>(77,144)*z[19] + n<T>(77,12)*z[23] + n<T>(41,12)*
      z[24] - n<T>(5,8)*z[29] + n<T>(49,24)*z[30] - n<T>(41,24)*z[32] - n<T>(1,8)*z[33]
       + n<T>(3,8)*z[34] - n<T>(7,8)*z[35] + z[42] + z[43] + n<T>(1,2)*z[44] + 
      z[45] + z[46] + 3*z[47] + z[48] + z[49] + n<T>(1,3)*z[50] + n<T>(1,6)*
      z[51] - n<T>(1,4)*z[52] + z[53] - n<T>(77,24)*z[54] + z[55] + z[56];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf751(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf751(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
