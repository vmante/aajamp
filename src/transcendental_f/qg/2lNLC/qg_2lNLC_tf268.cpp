#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf268(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[48];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[19];
    z[18]=c[23];
    z[19]=c[25];
    z[20]=c[26];
    z[21]=c[28];
    z[22]=c[31];
    z[23]=e[3];
    z[24]=e[10];
    z[25]=f[3];
    z[26]=f[4];
    z[27]=f[5];
    z[28]=f[11];
    z[29]=f[12];
    z[30]=f[16];
    z[31]=f[17];
    z[32]=n<T>(1,2)*z[3];
    z[33]=z[1]*i;
    z[34]= - z[3] + 3*z[4] + z[33];
    z[34]=z[34]*z[32];
    z[35]= - z[2] + n<T>(1,2)*z[4];
    z[36]= - z[32] - z[35];
    z[36]=z[5]*z[36];
    z[37]=z[33]*z[4];
    z[38]=npow(z[4],2);
    z[39]=n<T>(3,2)*z[33];
    z[40]=n<T>(1,4)*z[2] - z[4] - z[39];
    z[40]=z[2]*z[40];
    z[34]=z[36] + z[34] + z[40] - n<T>(1,4)*z[38] + z[37];
    z[34]=z[5]*z[34];
    z[36]= - z[37] + n<T>(1,2)*z[38];
    z[40]=z[33] - z[4];
    z[41]=3*z[40] + z[7];
    z[41]=z[7]*z[41];
    z[42]=z[6] + n<T>(1,2)*z[40];
    z[43]= - n<T>(1,6)*z[9] + z[7] - z[42];
    z[43]=z[9]*z[43];
    z[41]=z[43] + 3*z[36] + z[41];
    z[41]=z[9]*z[41];
    z[43]=z[6] - z[4];
    z[44]=n<T>(1,2)*z[2];
    z[43]= - n<T>(17,12)*z[9] - n<T>(1,12)*z[3] - n<T>(1,2)*z[7] + z[44] - n<T>(1,3)*
    z[43];
    z[43]=z[14]*z[43];
    z[45]=z[6] + z[9];
    z[45]=z[23]*z[45];
    z[41]=z[26] + z[41] + z[43] + z[45] - z[28];
    z[43]= - n<T>(5,6)*z[2] + n<T>(1,4)*z[4] + 2*z[33];
    z[43]=z[2]*z[43];
    z[43]=z[43] + z[38] - n<T>(3,2)*z[37];
    z[43]=z[2]*z[43];
    z[45]=n<T>(1,2)*z[6];
    z[46]= - z[4] - z[33];
    z[46]=n<T>(1,2)*z[46] + n<T>(5,3)*z[6];
    z[46]=z[46]*z[45];
    z[46]=z[46] - z[38] + z[37];
    z[46]=z[6]*z[46];
    z[47]= - n<T>(1,4)*z[6] - z[40];
    z[47]=z[6]*z[47];
    z[42]= - z[7]*z[42];
    z[36]=z[42] + n<T>(1,2)*z[36] + z[47];
    z[36]=z[7]*z[36];
    z[37]= - n<T>(3,2)*z[38] + z[37];
    z[35]=n<T>(1,6)*z[3] - z[35];
    z[35]=z[35]*z[32];
    z[38]= - z[2]*z[33];
    z[35]=z[35] + n<T>(1,2)*z[37] + z[38];
    z[35]=z[3]*z[35];
    z[32]= - z[32] + z[33] - z[44];
    z[32]=z[10]*z[32];
    z[37]=z[45] + z[4];
    z[37]=z[33]*z[37];
    z[37]=z[14] + z[37];
    z[37]=z[8]*z[37];
    z[38]= - z[44] - z[4] + z[39];
    z[38]=z[11]*z[38];
    z[39]=z[45] - z[40];
    z[39]=z[12]*z[39];
    z[40]=z[31] - z[30] + z[29] + z[25];
    z[42]= - 4*z[20] + n<T>(1,12)*z[15];
    z[42]=i*z[42];
    z[33]=3*z[33] - z[2];
    z[33]=n<T>(1,2)*z[33] - z[5];
    z[33]=z[13]*z[33];
    z[44]=z[45] + z[7];
    z[44]=z[24]*z[44];
    z[45]=2*i;
    z[45]= - z[18]*z[45];

    r +=  - n<T>(3,4)*z[16] - n<T>(1,6)*z[17] + z[19] + 2*z[21] + n<T>(21,8)*z[22]
       - z[27] + z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38]
       + z[39] - n<T>(1,4)*z[40] + n<T>(1,2)*z[41] + z[42] + z[43] + z[44] + 
      z[45] + z[46];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf268(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf268(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
