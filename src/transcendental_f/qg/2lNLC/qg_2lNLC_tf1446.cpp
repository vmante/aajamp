#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1446(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[52];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[7];
    z[6]=d[15];
    z[7]=d[18];
    z[8]=d[2];
    z[9]=d[5];
    z[10]=d[8];
    z[11]=d[11];
    z[12]=d[9];
    z[13]=e[2];
    z[14]=e[12];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[31];
    z[18]=e[3];
    z[19]=e[10];
    z[20]=e[11];
    z[21]=e[13];
    z[22]=e[14];
    z[23]=f[2];
    z[24]=f[4];
    z[25]=f[6];
    z[26]=f[12];
    z[27]=f[16];
    z[28]=f[51];
    z[29]=f[54];
    z[30]=f[55];
    z[31]=f[56];
    z[32]=f[58];
    z[33]=npow(z[9],2);
    z[34]=n<T>(1,4)*z[33];
    z[35]=npow(z[4],2);
    z[36]=n<T>(1,4)*z[35];
    z[37]=z[34] - z[36];
    z[38]=n<T>(1,2)*z[8];
    z[39]=z[38] - z[9];
    z[40]=z[39]*z[38];
    z[41]=n<T>(1,2)*z[3];
    z[42]=z[4] - z[41];
    z[41]=z[42]*z[41];
    z[42]=n<T>(1,2)*z[10];
    z[43]= - z[4] + z[9] - z[3] + z[8];
    z[43]=n<T>(1,2)*z[43] - z[10];
    z[43]=z[43]*z[42];
    z[44]=npow(z[12],2);
    z[40]=z[43] + n<T>(1,2)*z[44] + z[41] + z[40] - z[22] - z[19] + z[37];
    z[40]=z[10]*z[40];
    z[41]=n<T>(1,2)*z[9];
    z[43]=z[38] + z[11] - z[41];
    z[43]=z[8]*z[43];
    z[44]= - z[6] + n<T>(1,2)*z[4];
    z[44]=z[3]*z[44];
    z[37]=z[44] + z[43] + z[14] + z[13] - z[37];
    z[43]=z[2] + z[6];
    z[44]=n<T>(1,3)*z[3];
    z[45]=z[44] + z[5];
    z[46]=n<T>(1,3)*z[4];
    z[43]=z[46] + z[7] - z[45] - n<T>(1,3)*z[43];
    z[43]=z[2]*z[43];
    z[47]=n<T>(1,3)*z[9];
    z[48]=n<T>(1,2)*z[12];
    z[49]=z[48] - z[47] + n<T>(1,3)*z[11] + z[5] - z[7];
    z[49]=z[12]*z[49];
    z[50]=z[3] - z[4];
    z[51]=z[9] - z[8] + z[50];
    z[42]=z[51]*z[42];
    z[37]=z[43] + z[42] + n<T>(1,3)*z[37] + z[49];
    z[37]=z[1]*z[37];
    z[37]= - n<T>(2,9)*z[16] + z[37];
    z[37]=i*z[37];
    z[42]=n<T>(1,3)*z[8];
    z[39]= - z[39]*z[42];
    z[43]=z[14] + z[21];
    z[48]= - z[5]*z[48];
    z[39]=z[48] + z[39] - n<T>(1,6)*z[33] + n<T>(7,18)*z[15] + z[20] - z[22] - n<T>(1,3)*z[43];
    z[39]=z[12]*z[39];
    z[43]= - n<T>(5,3)*z[2] + z[46] + z[45];
    z[43]=z[2]*z[43];
    z[45]=npow(z[10],2);
    z[43]= - z[15] + z[45] + z[43];
    z[45]=z[50]*z[44];
    z[35]= - z[18] + z[35] - z[13];
    z[35]=z[45] - z[20] - z[19] + n<T>(1,2)*z[43] + n<T>(1,3)*z[35];
    z[35]=z[2]*z[35];
    z[43]=n<T>(1,3)*z[15];
    z[45]=n<T>(1,2)*z[5] + z[11] - z[6];
    z[45]=z[45]*z[43];
    z[33]= - n<T>(1,2)*z[33] - z[21] - z[43];
    z[33]=z[33]*z[47];
    z[41]=z[41] - z[8];
    z[38]=z[41]*z[38];
    z[34]=z[38] + n<T>(1,4)*z[15] + z[34] - z[14];
    z[34]=z[34]*z[42];
    z[38]= - z[18] + z[43];
    z[38]=z[38]*z[46];
    z[41]=z[3]*z[4];
    z[36]= - n<T>(1,4)*z[41] - n<T>(1,12)*z[15] - z[36] - z[13];
    z[36]=z[36]*z[44];
    z[41]=z[26] + z[28];
    z[42]=z[24] + z[27];
    z[43]=z[23] + z[29];
    z[44]=z[30] - z[32];
    z[44]= - z[25] - z[31] - n<T>(1,2)*z[44];

    r += 2*z[17] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + 
      z[39] + z[40] + n<T>(1,12)*z[41] - n<T>(1,4)*z[42] - n<T>(1,3)*z[43] + n<T>(1,2)*
      z[44] + z[45];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1446(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1446(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
