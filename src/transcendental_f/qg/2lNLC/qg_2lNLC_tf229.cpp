#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf229(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[59];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[16];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=d[6];
    z[18]=d[9];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[0];
    z[28]=e[3];
    z[29]=e[10];
    z[30]=e[14];
    z[31]=f[0];
    z[32]=f[1];
    z[33]=f[3];
    z[34]=f[4];
    z[35]=f[5];
    z[36]=f[11];
    z[37]=f[12];
    z[38]=f[16];
    z[39]=f[17];
    z[40]=f[18];
    z[41]=f[51];
    z[42]=f[55];
    z[43]=f[58];
    z[44]=2*z[4];
    z[45]=n<T>(5,2)*z[2];
    z[46]=n<T>(1,2)*z[10];
    z[47]=n<T>(1,2)*z[7];
    z[48]=n<T>(1,2)*z[6];
    z[49]=z[48] - z[45] - z[47] + z[44] + z[8] - z[46];
    z[49]=z[5]*z[49];
    z[50]=n<T>(1,2)*z[3];
    z[51]=z[8] - z[50];
    z[51]=z[51]*z[50];
    z[52]=z[7] + z[3];
    z[53]=z[52] + z[4];
    z[45]= - z[6] + z[45] + 2*z[10] - z[53];
    z[45]=z[6]*z[45];
    z[54]=npow(z[11],2);
    z[54]=n<T>(1,2)*z[54];
    z[55]=z[54] + z[14];
    z[56]=npow(z[10],2);
    z[57]= - z[9] + n<T>(1,2)*z[4];
    z[57]=z[4]*z[57];
    z[58]= - z[7] + z[46] + 2*z[3];
    z[58]=z[7]*z[58];
    z[53]=2*z[2] - z[53];
    z[53]=z[2]*z[53];
    z[45]=z[49] + z[45] + z[53] + z[58] + z[51] + z[57] - n<T>(1,4)*z[56] + 
   n<T>(5,2)*z[12] - z[13] - n<T>(1,2)*z[15] - z[55];
    z[45]=z[1]*z[45];
    z[49]=n<T>(1,3)*z[19] - z[24] - z[22];
    z[45]=2*z[49] + z[45];
    z[45]=i*z[45];
    z[49]=z[7] + z[10];
    z[49]= - z[48] + n<T>(3,2)*z[2] - z[50] - z[4] + n<T>(1,4)*z[49];
    z[49]=z[5]*z[49];
    z[50]= - z[10] + 3*z[7];
    z[50]=z[50]*z[47];
    z[51]=npow(z[2],2);
    z[53]=z[51] + z[16];
    z[57]=npow(z[3],2);
    z[57]=n<T>(1,4)*z[57];
    z[58]=npow(z[4],2);
    z[44]= - n<T>(1,4)*z[6] + z[44] - z[2];
    z[44]=z[6]*z[44];
    z[44]=z[49] + z[44] + z[50] - z[57] - z[58] - n<T>(3,4)*z[56] + z[13] - 
   2*z[12] + n<T>(1,4)*z[53];
    z[44]=z[5]*z[44];
    z[49]=z[7]*z[3];
    z[50]= - n<T>(3,4)*z[2] + z[52];
    z[50]=z[2]*z[50];
    z[48]=z[48] - z[10] - n<T>(1,4)*z[4];
    z[48]=z[6]*z[48];
    z[48]=z[48] + z[50] - z[49] - n<T>(1,4)*z[58] - n<T>(2,3)*z[16] + z[56];
    z[48]=z[6]*z[48];
    z[50]=z[3] - z[10] - z[4];
    z[47]=z[50]*z[47];
    z[50]=n<T>(1,2)*z[56];
    z[47]=z[47] + z[57] - z[50] - n<T>(1,2)*z[16] + z[30] - z[54];
    z[47]=z[7]*z[47];
    z[50]=n<T>(5,12)*z[58] - z[50] + n<T>(1,8)*z[16] - n<T>(1,2)*z[27] + z[55];
    z[50]=z[4]*z[50];
    z[52]= - 3*z[16] + z[13] + z[29] - 5*z[28];
    z[53]=z[10] - n<T>(2,3)*z[3];
    z[53]=z[3]*z[53];
    z[52]=z[53] + n<T>(1,2)*z[52] + z[56];
    z[52]=z[3]*z[52];
    z[51]=z[51] - z[58] - z[56] - n<T>(7,2)*z[16] + z[12] - z[27] - z[15];
    z[49]= - z[49] - n<T>(1,2)*z[51];
    z[49]=z[2]*z[49];
    z[51]=n<T>(1,6)*z[56] - 3*z[28] + n<T>(31,12)*z[16];
    z[46]=z[51]*z[46];
    z[51]=z[39] - z[32] + z[41] - z[43] + z[42];
    z[53]=z[20] + z[36];
    z[54]=z[18]*z[30];
    z[55]=z[17] - z[18];
    z[55]=z[8] + n<T>(1,6)*z[55] - z[9];
    z[55]=z[16]*z[55];

    r +=  - n<T>(2,3)*z[21] + z[23] + z[25] - n<T>(11,24)*z[26] + z[31] - z[33]
       + n<T>(3,2)*z[34] - z[35] + n<T>(3,4)*z[37] - n<T>(5,4)*z[38] + n<T>(1,3)*z[40]
       + z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + n<T>(1,2)*
      z[51] + z[52] - n<T>(1,4)*z[53] + z[54] + z[55];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf229(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf229(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
