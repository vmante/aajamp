#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf220(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[46];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=d[9];
    z[11]=d[12];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[5];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[31];
    z[25]=e[10];
    z[26]=f[3];
    z[27]=f[4];
    z[28]=f[5];
    z[29]=f[11];
    z[30]=f[12];
    z[31]=f[16];
    z[32]=f[17];
    z[33]=z[1]*i;
    z[34]=z[33]*z[3];
    z[35]=npow(z[3],2);
    z[36]= - z[33] + n<T>(1,2)*z[4];
    z[37]= - n<T>(1,2)*z[3] + z[36];
    z[37]=z[4]*z[37];
    z[38]=n<T>(5,2)*z[33];
    z[39]= - n<T>(5,4)*z[2] + n<T>(1,4)*z[4] + z[3] + z[38];
    z[39]=z[2]*z[39];
    z[37]=z[39] + z[37] + n<T>(5,4)*z[35] - 4*z[34];
    z[37]=z[2]*z[37];
    z[39]=z[33] - z[3];
    z[40]=z[6]*z[39];
    z[40]= - n<T>(23,4)*z[40] - z[35] + 2*z[34];
    z[40]=z[6]*z[40];
    z[41]=n<T>(5,4)*z[4];
    z[38]= - z[41] + z[38];
    z[38]=z[3]*z[38];
    z[38]=z[35] + z[38];
    z[38]=z[4]*z[38];
    z[42]=z[39] + z[6];
    z[43]=z[6]*z[42];
    z[43]=z[34] - z[43];
    z[43]= - n<T>(23,2)*z[35] + 19*z[43];
    z[44]=n<T>(23,4)*z[5] - n<T>(19,4)*z[6] - z[33] - z[3];
    z[44]=z[5]*z[44];
    z[43]=n<T>(1,2)*z[43] + z[44];
    z[43]=z[5]*z[43];
    z[44]=n<T>(5,2)*z[9];
    z[41]=z[41] + z[3] - n<T>(11,4)*z[6];
    z[41]= - z[44] + n<T>(1,12)*z[2] + n<T>(1,3)*z[41] + n<T>(5,2)*z[5];
    z[41]=z[16]*z[41];
    z[45]= - n<T>(1,2)*z[2] - z[36];
    z[45]=z[15]*z[45];
    z[37]=z[45] + z[41] + z[37] + z[43] + z[40] + z[38];
    z[35]=n<T>(1,2)*z[35];
    z[38]=n<T>(1,2)*z[6] + z[39];
    z[38]=z[6]*z[38];
    z[40]=z[9]*z[42];
    z[38]=n<T>(1,2)*z[40] + z[38] + z[35] - z[34];
    z[38]=z[38]*z[44];
    z[40]=n<T>(1,2)*z[8];
    z[41]= - z[3] + z[4];
    z[41]=z[41]*z[40];
    z[36]= - z[3] + z[36];
    z[36]=z[4]*z[36];
    z[35]=z[41] + n<T>(1,6)*z[16] + z[36] + z[35] + z[34];
    z[35]=z[35]*z[40];
    z[36]=z[5] + z[3];
    z[36]=z[33]*z[36];
    z[36]=z[16] + z[36];
    z[36]=z[7]*z[36];
    z[40]= - z[2] - z[3] + 2*z[33];
    z[40]=z[12]*z[40];
    z[41]=z[5] - z[39];
    z[41]=z[13]*z[41];
    z[36]= - z[18] + z[36] + z[40] + z[41] - z[24];
    z[40]=z[10] - z[3];
    z[33]=z[33]*z[40];
    z[33]= - z[16] + z[33];
    z[33]=z[11]*z[33];
    z[40]= - z[21] + z[29] + z[28];
    z[41]=z[31] + z[30];
    z[42]=z[32] - z[26];
    z[43]= - n<T>(5,3)*z[22] - n<T>(5,6)*z[20] + n<T>(2,9)*z[17];
    z[43]=i*z[43];
    z[34]= - z[10]*z[34];
    z[39]= - z[10] - z[39];
    z[39]=z[14]*z[39];
    z[44]=z[6] + z[5];
    z[44]=z[25]*z[44];

    r +=  - n<T>(5,72)*z[19] + n<T>(5,6)*z[23] + n<T>(23,12)*z[27] + z[33] + z[34]
       + z[35] + n<T>(2,3)*z[36] + n<T>(1,3)*z[37] + z[38] + z[39] - n<T>(5,12)*
      z[40] + n<T>(5,4)*z[41] + n<T>(1,4)*z[42] + z[43] + n<T>(19,6)*z[44];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf220(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf220(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
