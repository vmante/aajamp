#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf167(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[48];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[4];
    z[9]=d[9];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=c[3];
    z[14]=d[2];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[19];
    z[18]=c[23];
    z[19]=c[25];
    z[20]=c[26];
    z[21]=c[28];
    z[22]=c[31];
    z[23]=e[3];
    z[24]=e[10];
    z[25]=e[14];
    z[26]=f[4];
    z[27]=f[5];
    z[28]=f[6];
    z[29]=f[11];
    z[30]=f[12];
    z[31]=f[16];
    z[32]=f[56];
    z[33]=n<T>(16,3)*z[2];
    z[34]=3*z[5];
    z[35]= - z[7] + n<T>(3,2)*z[8];
    z[35]= - z[34] + n<T>(3,2)*z[6] + 3*z[35] + z[33];
    z[35]=z[1]*z[35];
    z[36]=z[4]*z[1];
    z[35]=z[35] - n<T>(16,3)*z[36];
    z[35]=i*z[35];
    z[37]=n<T>(8,3)*z[2];
    z[38]=n<T>(2,3)*z[4];
    z[34]=z[38] + z[34] - n<T>(3,4)*z[6] - z[37] + 2*z[14] - n<T>(9,4)*z[8];
    z[34]=z[3]*z[34];
    z[39]=n<T>(1,2)*z[6];
    z[40]=9*z[8];
    z[41]=z[40] - n<T>(19,2)*z[6];
    z[41]=z[41]*z[39];
    z[42]=npow(z[8],2);
    z[43]=n<T>(5,4)*z[42];
    z[44]= - z[43] + 3*z[11];
    z[45]=npow(z[2],2);
    z[46]=n<T>(4,3)*z[45];
    z[47]= - 3*z[6] + 2*z[5];
    z[47]=z[5]*z[47];
    z[38]=z[38] - z[14] + n<T>(2,3)*z[2];
    z[38]=z[4]*z[38];
    z[34]=z[34] + z[35] + 4*z[38] + z[47] + z[41] - z[46] - n<T>(17,36)*
    z[13] + n<T>(8,3)*z[10] - z[44];
    z[34]=z[3]*z[34];
    z[35]=n<T>(1,2)*z[9];
    z[38]=n<T>(1,2)*z[5];
    z[37]= - n<T>(1,3)*z[4] + z[38] - z[37] - z[35] + z[14];
    z[37]=z[4]*z[37];
    z[41]=npow(z[6],2);
    z[47]=npow(z[14],2);
    z[38]= - z[6] + z[38];
    z[38]=z[5]*z[38];
    z[37]=z[37] + z[38] + n<T>(1,2)*z[41] - z[46] - n<T>(7,18)*z[13] + n<T>(8,3)*
    z[12] + z[47];
    z[37]=z[4]*z[37];
    z[38]= - z[40] + n<T>(17,2)*z[6];
    z[38]=z[38]*z[39];
    z[40]=z[45] + z[10] + z[12];
    z[45]= - z[7] + z[6];
    z[45]=4*z[45] + n<T>(3,2)*z[5];
    z[45]=z[5]*z[45];
    z[38]=z[45] + z[38] + z[44] - n<T>(16,3)*z[40];
    z[38]=z[1]*z[38];
    z[33]= - z[5] + z[9] + z[33];
    z[33]=z[33]*z[36];
    z[36]=2*z[20] + z[18];
    z[36]=16*z[36] - z[15];
    z[33]=z[33] + n<T>(1,3)*z[36] + z[38];
    z[33]=i*z[33];
    z[36]=n<T>(5,2)*z[6] - n<T>(13,3)*z[5];
    z[36]=z[5]*z[36];
    z[36]=z[36] + n<T>(9,2)*z[41] - z[42] - n<T>(1,2)*z[13] - 4*z[11] - 6*z[24]
    + z[23];
    z[36]=z[5]*z[36];
    z[38]=npow(z[14],3);
    z[40]= - n<T>(7,3)*z[13] + 8*z[40];
    z[40]=z[2]*z[40];
    z[38]=z[38] - z[40];
    z[40]=z[9] - n<T>(5,2)*z[8];
    z[39]=z[40]*z[39];
    z[39]=z[39] - z[43] + 2*z[13] - z[25] - 5*z[24];
    z[39]=z[6]*z[39];
    z[40]= - z[29] + z[19] - z[27];
    z[41]= - z[28] - n<T>(5,2)*z[30] - z[32] - n<T>(1,2)*z[31];
    z[35]=n<T>(1,2)*z[14] - 3*z[7] + z[35];
    z[35]=z[13]*z[35];
    z[42]=n<T>(1,3)*z[42] + z[23] + n<T>(4,3)*z[13];
    z[42]=z[8]*z[42];

    r += n<T>(7,2)*z[16] + n<T>(4,9)*z[17] - n<T>(16,3)*z[21] - n<T>(121,12)*z[22]
     - n<T>(19,4)*z[26]
     + z[33]
     + z[34]
     + z[35]
     + z[36]
     + z[37] - n<T>(1,3)*z[38]
       + z[39] - n<T>(8,3)*z[40] + n<T>(1,2)*z[41] + z[42];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf167(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf167(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
