#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf372(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[72];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[0];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[6];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[55];
    z[52]=f[56];
    z[53]=n<T>(5,4)*z[7];
    z[54]=z[14] + z[6];
    z[55]=z[53] + z[54];
    z[55]=z[7]*z[55];
    z[56]=n<T>(1,2)*z[9];
    z[57]=3*z[15];
    z[58]=n<T>(85,2)*z[4] + z[57] - 7*z[7];
    z[58]=8*z[8] - n<T>(3025,36)*z[2] + n<T>(529,9)*z[5] - z[56] + n<T>(5,2)*z[3]
     + 
   n<T>(1,2)*z[58] - 12*z[11];
    z[58]=z[8]*z[58];
    z[59]= - z[21] + n<T>(13,2)*z[18];
    z[60]=npow(z[6],2);
    z[61]=z[12] - z[14];
    z[61]=z[14]*z[61];
    z[62]= - n<T>(3,4)*z[4] - n<T>(13,2)*z[13] + z[12];
    z[62]=z[4]*z[62];
    z[63]= - z[13] + z[4];
    z[63]=7*z[63] - n<T>(1,8)*z[11];
    z[63]=z[11]*z[63];
    z[64]=653*z[10] - n<T>(545,2)*z[3];
    z[64]=z[3]*z[64];
    z[65]= - n<T>(188,9)*z[9] - n<T>(1481,36)*z[3] + z[14] + n<T>(619,12)*z[11];
    z[65]=z[9]*z[65];
    z[66]=n<T>(368,9)*z[10];
    z[67]= - n<T>(166,9)*z[9] + n<T>(1121,36)*z[3] - n<T>(643,12)*z[11] + z[66] - n<T>(31,4)*z[4];
    z[67]=z[5]*z[67];
    z[68]=n<T>(7,2)*z[3];
    z[69]=n<T>(2863,36)*z[2] - n<T>(1837,36)*z[5] + n<T>(3,2)*z[9] + z[68] - n<T>(27,2)*
    z[4] - z[6] - z[7];
    z[69]=z[2]*z[69];
    z[55]=z[58] + z[69] + z[67] + z[65] + n<T>(1,36)*z[64] + z[63] + z[62]
    + z[55] + z[61] - n<T>(1,4)*z[60] + n<T>(1837,36)*z[16] - n<T>(368,9)*z[17] + 
   n<T>(2881,36)*z[19] - n<T>(3,2)*z[20] - z[59];
    z[55]=z[1]*z[55];
    z[58]=n<T>(3695,24)*z[23] - 2359*z[28] - n<T>(2845,2)*z[26];
    z[55]=n<T>(1,18)*z[58] + z[55];
    z[55]=i*z[55];
    z[58]=2*z[7];
    z[61]=n<T>(529,36)*z[2] - n<T>(995,36)*z[5] - z[56] - z[68] - z[58] + z[54];
    z[61]=z[2]*z[61];
    z[62]=n<T>(1,2)*z[60];
    z[63]=npow(z[3],2);
    z[64]=z[63] + z[62] + n<T>(791,54)*z[22] + 3*z[20] - n<T>(1625,18)*z[19];
    z[65]=5*z[3] - 3*z[9];
    z[65]=z[65]*z[56];
    z[67]=z[3] - z[14];
    z[58]= - n<T>(13,9)*z[8] + n<T>(1481,36)*z[2] - n<T>(1049,36)*z[5] + 6*z[11]
     - 
   n<T>(47,3)*z[4] + z[58] + n<T>(1,2)*z[67];
    z[58]=z[8]*z[58];
    z[67]=npow(z[4],2);
    z[69]=npow(z[11],2);
    z[53]= - z[14] - z[53];
    z[53]=z[7]*z[53];
    z[70]=z[9] - z[6];
    z[71]= - n<T>(529,72)*z[5] + n<T>(251,6)*z[4] + z[70];
    z[71]=z[5]*z[71];
    z[53]=z[58] + z[61] + z[71] + z[65] - n<T>(11,2)*z[69] - n<T>(127,8)*z[67]
    + z[53] + n<T>(1,2)*z[64];
    z[53]=z[8]*z[53];
    z[58]=z[67] + z[69];
    z[61]=n<T>(9,4)*z[31];
    z[64]=npow(z[14],2);
    z[65]= - z[7]*z[6];
    z[68]= - 2*z[14] + z[68];
    z[68]=z[9]*z[68];
    z[70]=n<T>(2125,72)*z[5] - z[70];
    z[70]=z[5]*z[70];
    z[54]= - n<T>(2497,36)*z[2] + n<T>(707,36)*z[5] + 3*z[7] - z[54];
    z[54]=z[2]*z[54];
    z[54]=n<T>(1,2)*z[54] + z[70] + z[68] + z[65] + z[64] + n<T>(2819,216)*z[22]
    - n<T>(599,36)*z[16] + z[61] - n<T>(314,9)*z[19] + n<T>(27,4)*z[58];
    z[54]=z[2]*z[54];
    z[58]= - n<T>(817,9)*z[3] + 41*z[11] + 5*z[14] + z[4];
    z[56]=z[58]*z[56];
    z[58]=z[4]*z[14];
    z[65]= - z[14] - n<T>(833,72)*z[3];
    z[65]=z[3]*z[65];
    z[56]=z[56] + z[65] + n<T>(463,24)*z[69] + z[58] - z[64] - n<T>(1007,108)*
    z[22] - 3*z[36] + n<T>(404,9)*z[33];
    z[56]=z[9]*z[56];
    z[58]= - 619*z[11] + n<T>(1265,3)*z[3];
    z[58]=n<T>(1,4)*z[58] + n<T>(197,3)*z[9];
    z[58]=z[9]*z[58];
    z[64]= - n<T>(2593,12)*z[3] - 133*z[4] + n<T>(643,4)*z[11];
    z[64]=n<T>(1,2)*z[64] + n<T>(83,3)*z[9];
    z[64]=z[5]*z[64];
    z[58]=z[58] + z[64];
    z[64]=n<T>(1391,24)*z[22] + 368*z[17] - n<T>(619,2)*z[16];
    z[58]= - n<T>(797,72)*z[63] + n<T>(45,8)*z[69] + n<T>(1,9)*z[64] + n<T>(41,8)*z[67]
    + n<T>(1,3)*z[58];
    z[58]=z[5]*z[58];
    z[63]= - z[14] + n<T>(41,6)*z[4];
    z[63]=z[4]*z[63];
    z[59]=n<T>(1,2)*z[63] - n<T>(587,72)*z[22] - z[61] + z[59];
    z[59]=z[4]*z[59];
    z[61]= - z[7] - n<T>(9,2)*z[4];
    z[61]=3*z[61] + n<T>(193,18)*z[11];
    z[61]=z[11]*z[61];
    z[61]=z[61] + 3*z[34] + 47*z[32];
    z[63]=npow(z[7],2);
    z[60]=2*z[63] - n<T>(5,4)*z[60] - n<T>(2051,72)*z[22] + 7*z[18] + n<T>(1,2)*
    z[61];
    z[60]=z[11]*z[60];
    z[61]=z[7]*z[14];
    z[63]= - z[7] - n<T>(79,4)*z[11];
    z[63]=z[11]*z[63];
    z[64]= - 3*z[11] + n<T>(2617,9)*z[3];
    z[64]=z[3]*z[64];
    z[61]=n<T>(1,8)*z[64] + z[63] + z[61] - n<T>(545,72)*z[22] + n<T>(653,36)*z[17]
    + n<T>(251,9)*z[33] + n<T>(71,4)*z[32];
    z[61]=z[3]*z[61];
    z[57]=z[57] - 13*z[13];
    z[57]=z[66] + n<T>(1,2)*z[57] + z[12];
    z[57]=z[22]*z[57];
    z[63]=z[52] + z[42] + z[49];
    z[64]=z[27] - z[41];
    z[62]=z[62] + z[35];
    z[65]=n<T>(1,6)*z[22] + z[62];
    z[65]=z[6]*z[65];
    z[66]= - z[21] + n<T>(1,12)*z[22];
    z[66]=z[14]*z[66];
    z[62]= - n<T>(41,12)*z[22] + z[62];
    z[62]=z[7]*z[62];

    r +=  - n<T>(1765,72)*z[24] - n<T>(5761,432)*z[25] + n<T>(2359,36)*z[29] + 
      n<T>(19411,144)*z[30] + n<T>(27,2)*z[37] - n<T>(27,4)*z[38] + n<T>(31,8)*z[39]
     +  n<T>(206,9)*z[40] - n<T>(529,18)*z[43] - n<T>(53,8)*z[44] + 14*z[45] - n<T>(85,8)
      *z[46] + n<T>(9,2)*z[47] + n<T>(1,4)*z[48] - n<T>(5,4)*z[50] - z[51] + z[53]
       + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61]
       + z[62] - n<T>(1,2)*z[63] + n<T>(2845,72)*z[64] + z[65] + z[66];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf372(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf372(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
