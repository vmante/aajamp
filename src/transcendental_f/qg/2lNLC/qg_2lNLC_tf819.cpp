#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf819(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[173];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=e[21];
    z[4]=d[1];
    z[5]=d[20];
    z[6]=d[22];
    z[7]=d[23];
    z[8]=d[4];
    z[9]=d[24];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[8];
    z[13]=d[9];
    z[14]=d[2];
    z[15]=d[3];
    z[16]=d[7];
    z[17]=f[42];
    z[18]=f[44];
    z[19]=f[61];
    z[20]=f[62];
    z[21]=f[70];
    z[22]=f[71];
    z[23]=f[77];
    z[24]=f[79];
    z[25]=f[81];
    z[26]=c[3];
    z[27]=d[19];
    z[28]=c[11];
    z[29]=c[12];
    z[30]=c[13];
    z[31]=c[20];
    z[32]=c[23];
    z[33]=c[32];
    z[34]=c[33];
    z[35]=c[35];
    z[36]=c[37];
    z[37]=c[38];
    z[38]=c[39];
    z[39]=c[44];
    z[40]=c[47];
    z[41]=c[48];
    z[42]=c[49];
    z[43]=c[50];
    z[44]=c[55];
    z[45]=c[56];
    z[46]=c[57];
    z[47]=c[59];
    z[48]=c[81];
    z[49]=c[84];
    z[50]=e[17];
    z[51]=e[19];
    z[52]=e[20];
    z[53]=e[18];
    z[54]=e[23];
    z[55]=e[15];
    z[56]=e[16];
    z[57]=e[22];
    z[58]=f[24];
    z[59]=f[25];
    z[60]=f[45];
    z[61]=f[63];
    z[62]=f[69];
    z[63]=f[80];
    z[64]=f[82];
    z[65]=f[87];
    z[66]=f[92];
    z[67]=f[22];
    z[68]=f[97];
    z[69]=f[102];
    z[70]=g[24];
    z[71]=g[27];
    z[72]=g[38];
    z[73]=g[40];
    z[74]=g[44];
    z[75]=g[48];
    z[76]=g[50];
    z[77]=g[60];
    z[78]=g[62];
    z[79]=g[63];
    z[80]=g[79];
    z[81]=g[82];
    z[82]=g[90];
    z[83]=g[92];
    z[84]=g[95];
    z[85]=g[99];
    z[86]=g[101];
    z[87]=g[128];
    z[88]=g[131];
    z[89]=g[138];
    z[90]=g[140];
    z[91]=g[141];
    z[92]=g[145];
    z[93]=g[147];
    z[94]=g[170];
    z[95]=g[173];
    z[96]=g[181];
    z[97]=g[183];
    z[98]=g[187];
    z[99]=g[189];
    z[100]=g[191];
    z[101]=g[198];
    z[102]=g[200];
    z[103]=g[201];
    z[104]=g[212];
    z[105]=g[214];
    z[106]=g[218];
    z[107]=g[220];
    z[108]=g[224];
    z[109]=g[243];
    z[110]=g[245];
    z[111]=g[251];
    z[112]=g[254];
    z[113]=g[270];
    z[114]=g[272];
    z[115]=g[276];
    z[116]=g[278];
    z[117]=g[281];
    z[118]=g[285];
    z[119]=g[288];
    z[120]=g[289];
    z[121]=g[292];
    z[122]=g[294];
    z[123]=g[324];
    z[124]=g[329];
    z[125]=g[339];
    z[126]=g[349];
    z[127]=g[359];
    z[128]=g[389];
    z[129]=g[399];
    z[130]=g[430];
    z[131]=g[431];
    z[132]=g[432];
    z[133]=g[434];
    z[134]=2*z[2];
    z[135]=z[134]*z[10];
    z[136]=2*z[4];
    z[137]=z[136] + z[10];
    z[137]=z[137]*z[10];
    z[135]=z[135] + z[137];
    z[138]=z[4]*i;
    z[139]=z[10]*i;
    z[140]=z[138] + z[139];
    z[141]=z[2]*i;
    z[142]=z[141] + z[140];
    z[142]=z[1]*z[142];
    z[143]=z[12] + z[10];
    z[144]=z[1]*i;
    z[145]=2*z[144];
    z[146]= - z[145] - z[143];
    z[147]=2*z[14];
    z[146]=z[146]*z[147];
    z[148]=z[2] + z[4];
    z[149]=z[148] + z[10];
    z[145]= - z[14] + z[145] + z[149];
    z[150]= - 2*z[145] + z[15];
    z[150]=z[15]*z[150];
    z[145]=z[145] - z[15];
    z[151]= - 2*z[145] + z[16];
    z[151]=z[16]*z[151];
    z[152]=2*z[148];
    z[153]=z[152] - z[12];
    z[153]=z[153]*z[12];
    z[142]=z[151] + z[150] + z[146] + 4*z[142] + z[135] + z[153];
    z[142]=z[3]*z[142];
    z[146]=z[10] + z[4];
    z[150]=z[146] - z[8];
    z[151]= - z[13] + 2*z[150];
    z[151]=z[151]*z[13];
    z[154]=2*z[146];
    z[155]=z[154] - z[8];
    z[155]=z[155]*z[8];
    z[151]=z[151] + z[155];
    z[146]=z[146] - z[12];
    z[155]= - z[11] + 2*z[146];
    z[155]=z[155]*z[11];
    z[156]=npow(z[4],2);
    z[157]= - z[154] + z[12];
    z[157]=z[12]*z[157];
    z[157]= - z[157] + z[151] + z[155] + z[156] + z[137];
    z[154]= - z[154] - z[2];
    z[154]=z[2]*z[154];
    z[154]= - z[3] + z[154] - n<T>(1,3)*z[157];
    z[154]=z[26]*z[154];
    z[157]=z[148] - z[12];
    z[158]= - z[14] + 2*z[157];
    z[158]=z[158]*z[14];
    z[135]=z[135] + z[158];
    z[159]= - 2*z[149] + z[13];
    z[159]=z[13]*z[159];
    z[160]=z[149] - z[13];
    z[161]= - 2*z[160] + z[11];
    z[161]=z[11]*z[161];
    z[160]=z[160] - z[11];
    z[162]= - 2*z[160] + z[15];
    z[162]=z[15]*z[162];
    z[159]=z[162] + z[161] + z[159] + z[153] + z[135];
    z[159]=z[55]*z[159];
    z[161]=2*z[12];
    z[162]=z[10]*z[161];
    z[163]=z[157] + z[10];
    z[164]=2*z[163] - z[8];
    z[164]=z[8]*z[164];
    z[163]=z[163] - z[8];
    z[165]=2*z[163] - z[15];
    z[165]=z[15]*z[165];
    z[135]=z[165] + z[164] + z[162] - z[135];
    z[135]=z[57]*z[135];
    z[162]=z[10] - z[4];
    z[134]=z[162]*z[134];
    z[162]=z[153] - z[156];
    z[164]=npow(z[10],2);
    z[134]=z[134] + z[164] + z[162];
    z[164]=z[2] + z[10];
    z[165]= - 2*z[164] + z[13];
    z[165]=z[13]*z[165];
    z[166]=z[164] - z[13];
    z[167]= - 2*z[166] + z[16];
    z[167]=z[16]*z[167];
    z[165]=z[167] + z[165] + z[158] + z[134];
    z[165]=z[53]*z[165];
    z[167]=z[140]*z[1];
    z[168]=z[144]*z[8];
    z[169]=z[167] - z[168];
    z[170]=z[144]*z[13];
    z[169]= - z[170] + 2*z[169];
    z[169]=z[169]*z[13];
    z[167]= - z[168] + 2*z[167];
    z[167]=z[167]*z[8];
    z[167]=z[169] + z[167];
    z[168]=z[12]*i;
    z[169]=z[168] - z[140];
    z[170]=2*z[1];
    z[169]=z[169]*z[170];
    z[170]=z[144]*z[11];
    z[169]=z[169] + z[170];
    z[169]=z[169]*z[11];
    z[140]= - z[168] + 2*z[140];
    z[140]=z[140]*z[12];
    z[170]= - z[1]*z[140];
    z[170]=z[169] + z[170] + z[167];
    z[170]=z[9]*z[170];
    z[152]= - z[152] + z[8];
    z[152]=z[8]*z[152];
    z[171]=z[148] - z[8];
    z[172]= - 2*z[171] + z[16];
    z[172]=z[16]*z[172];
    z[152]=z[172] + z[152] + z[158] + z[153];
    z[152]=z[56]*z[152];
    z[136]=z[136] + z[2];
    z[136]=z[136]*z[2];
    z[136]=z[136] - z[158];
    z[137]=z[136] - z[137];
    z[151]=z[153] - z[151] - z[137];
    z[151]=z[51]*z[151];
    z[153]= - z[10] + z[2];
    z[153]=z[153]*z[161];
    z[137]= - z[155] + z[153] - z[137];
    z[137]=z[52]*z[137];
    z[143]= - z[4] + z[143];
    z[143]=z[143]*z[147];
    z[147]=z[164] - z[14];
    z[153]=2*z[147] - z[11];
    z[153]=z[11]*z[153];
    z[134]=z[153] + z[143] - z[134];
    z[134]=z[54]*z[134];
    z[136]= - z[162] + z[136];
    z[136]=z[50]*z[136];
    z[134]=z[136] + z[152] + z[134] + z[137] + 2*z[170] + z[165] + 
    z[151] + z[135] + z[159] + z[142] + 4*z[154];
    z[135]= - z[16] + z[145];
    z[135]=z[22]*z[135];
    z[136]=z[15] - z[160];
    z[136]=z[59]*z[136];
    z[137]= - z[15] + z[163];
    z[137]=z[64]*z[137];
    z[142]= - z[16] + z[166];
    z[142]=z[60]*z[142];
    z[143]=z[11] - z[147];
    z[143]=z[61]*z[143];
    z[145]=z[16] - z[171];
    z[145]=z[65]*z[145];
    z[147]= - z[14] + z[157];
    z[147]=z[66]*z[147];
    z[150]= - z[13] + z[150];
    z[150]=z[68]*z[150];
    z[146]=z[11] - z[146];
    z[146]=z[69]*z[146];
    z[135]=z[146] + z[143] + z[145] + z[147] + z[150] + z[135] + z[136]
    + z[137] + z[142];
    z[136]=z[15] + z[16];
    z[137]=14*i;
    z[136]=z[137]*z[136];
    z[142]= - 17488*z[138] + 22681*z[139];
    z[143]=z[13]*i;
    z[145]=z[11]*i;
    z[146]=z[8]*i;
    z[136]=z[136] - 1129*z[143] - n<T>(1846,9)*z[145] + n<T>(1,9)*z[142] - 868*
    z[141] + 118*z[168] + n<T>(3106,9)*z[146];
    z[142]=n<T>(1,45)*z[32] - n<T>(16,15)*z[30];
    z[136]=z[136]*z[142];
    z[142]=16858*z[138] - 23311*z[139];
    z[137]=z[14]*z[137];
    z[137]= - n<T>(2602,9)*z[146] + z[137] - 62*z[168] + n<T>(1,9)*z[142] + 742*
    z[141];
    z[137]=n<T>(2350,27)*z[145] + n<T>(1,3)*z[137] + 395*z[143];
    z[137]=z[31]*z[137];
    z[141]= - 1397672*z[141] - 1314368*z[138] + 1463951*z[139];
    z[141]=n<T>(1,3)*z[141] - 325316*z[168];
    z[142]=z[14]*i;
    z[141]=n<T>(1,5)*z[141] + 56362*z[142];
    z[141]= - n<T>(594446,45)*z[145] + n<T>(14509,45)*z[143] + n<T>(1,3)*z[141] - 
   n<T>(23236,5)*z[146];
    z[142]=z[15]*i;
    z[141]=n<T>(1,3)*z[141] + n<T>(1078,5)*z[142];
    z[142]=z[16]*i;
    z[141]=n<T>(1,3)*z[141] + n<T>(14936,5)*z[142];
    z[141]=z[28]*z[141];
    z[142]=21*z[12];
    z[143]=21*z[15];
    z[145]=21*z[16];
    z[146]=n<T>(29972,3)*z[2] + n<T>(8857,3)*z[4] + 12394*z[10];
    z[146]=z[145] + z[143] - n<T>(29972,81)*z[11] - n<T>(8857,81)*z[13] - n<T>(5509,81)*z[8]
     - n<T>(33374,81)*z[14]
     + n<T>(1,27)*z[146]
     + z[142];
    z[146]=z[58]*z[146];
    z[147]=21*z[14];
    z[150]=21*z[8];
    z[151]=799*z[4] + 934*z[10];
    z[151]=13*z[151] - 13843*z[12];
    z[143]=42*z[16] - z[143] - n<T>(10387,81)*z[11] - z[150] + n<T>(1,81)*z[151]
    - z[147];
    z[143]=z[67]*z[143];
    z[138]=z[139] + 2*z[138];
    z[138]=z[138]*z[10];
    z[139]=z[156]*i;
    z[138]=z[138] + z[139];
    z[139]=z[138]*z[1];
    z[139]=z[167] - z[139];
    z[151]=z[6] - z[5];
    z[151]= - n<T>(14,3)*z[151];
    z[139]=z[139]*z[151];
    z[151]=z[16] - z[2];
    z[152]=7*z[4];
    z[151]=n<T>(19892,81)*z[15] + n<T>(21799,81)*z[11] + 21*z[13] - n<T>(14086,27)*
    z[8] + n<T>(23294,81)*z[14] - n<T>(43186,81)*z[144] + n<T>(25201,81)*z[12] + 
    z[152] - n<T>(15598,27)*z[10] - n<T>(20665,81)*z[151];
    z[151]=z[25]*z[151];
    z[138]= - z[140] + z[138];
    z[138]=z[1]*z[138];
    z[138]=z[138] + z[169];
    z[138]=z[7]*z[138];
    z[140]=42*z[144];
    z[153]= - 31862*z[4] - 30215*z[10];
    z[153]=n<T>(33473,9)*z[12] + n<T>(1,9)*z[153] + 554*z[2];
    z[147]= - n<T>(365,9)*z[15] + n<T>(31889,81)*z[11] - n<T>(557,9)*z[13] + n<T>(64,3)*
    z[8] + z[147] + n<T>(1,9)*z[153] - z[140];
    z[147]=z[17]*z[147];
    z[153]=n<T>(32717,3)*z[12] + n<T>(27947,3)*z[2] - 14221*z[4] + n<T>(16984,3)*
    z[10];
    z[153]= - z[145] - n<T>(13079,81)*z[11] + n<T>(3871,81)*z[13] + n<T>(2170,81)*
    z[8] - n<T>(29648,81)*z[14] + n<T>(1,27)*z[153] + z[140];
    z[153]=z[18]*z[153];
    z[154]= - n<T>(25418,3)*z[2] + n<T>(13537,3)*z[4] - 13708*z[10];
    z[142]= - n<T>(15238,81)*z[16] - n<T>(619,9)*z[15] + n<T>(12985,27)*z[11] - n<T>(9667,81)*z[8]
     + n<T>(14275,27)*z[14]
     + z[140]
     + n<T>(1,27)*z[154] - z[142];
    z[142]=z[21]*z[142];
    z[154]= - z[14] - 62*z[12] - n<T>(28,3)*z[2] + 70*z[4] + 13*z[10];
    z[154]= - n<T>(64,3)*z[16] - n<T>(155,9)*z[15] + n<T>(28,9)*z[11] - n<T>(76,3)*z[13]
    + n<T>(137,9)*z[8] + z[140] + n<T>(1,3)*z[154];
    z[154]=z[19]*z[154];
    z[155]=23311*z[4] - 27136*z[10];
    z[155]= - n<T>(17578,9)*z[12] + n<T>(1,9)*z[155] + 173*z[2];
    z[140]= - n<T>(8,3)*z[16] - n<T>(18919,81)*z[11] + n<T>(38531,81)*z[13] + n<T>(199,9)
   *z[8] + n<T>(229,9)*z[14] + n<T>(1,9)*z[155] - z[140];
    z[140]=z[20]*z[140];
    z[155]=54265*z[12] + 3682*z[2] + 65885*z[4] - 78116*z[10];
    z[155]= - 4324*z[16] - n<T>(19325,27)*z[15] + n<T>(61987,27)*z[11] + n<T>(8056,27)*z[8]
     + n<T>(23716,27)*z[14]
     + n<T>(1,27)*z[155]
     + 4025*z[144];
    z[155]=z[24]*z[155];
    z[145]= - z[145] + n<T>(31655,81)*z[15] - n<T>(12898,81)*z[11] + n<T>(29954,81)*
    z[13] - 42*z[8] + n<T>(10063,81)*z[14] - n<T>(31088,81)*z[12] - n<T>(13339,27)*
    z[2] - z[152] + n<T>(14599,81)*z[10];
    z[145]=z[63]*z[145];
    z[148]=28919*z[10] - 18730*z[148];
    z[144]=n<T>(16939,9)*z[16] - n<T>(25517,9)*z[15] - n<T>(15259,3)*z[8] + n<T>(61105,9)
   *z[14] - n<T>(33797,9)*z[144] + n<T>(1,9)*z[148] - 179*z[12];
    z[144]=z[23]*z[144];
    z[148]=7394*z[4] - 9671*z[10];
    z[148]=n<T>(1,9)*z[148] - 253*z[2];
    z[148]= - n<T>(14788,9)*z[14] + 2*z[148] - n<T>(13087,9)*z[12];
    z[148]=n<T>(19342,81)*z[16] + 21*z[11] + n<T>(17641,81)*z[13] + n<T>(1,9)*z[148]
    + z[150];
    z[148]=z[62]*z[148];
    z[149]= - z[27] + z[149];
    z[149]=z[26]*z[149];
    z[150]=z[28]*i;
    z[149]=z[150] + z[149];
    z[149]=z[27]*z[149];
    z[150]=59089*z[4] - 96061*z[10];
    z[150]=n<T>(2,3)*z[150] + 49753*z[2];
    z[150]=n<T>(169054,27)*z[11] - n<T>(19234,9)*z[13] + n<T>(79703,27)*z[8] - 9095*
    z[14] + n<T>(2,9)*z[150] + 11071*z[12];
    z[150]=n<T>(1,3)*z[150] - 4373*z[16];
    z[150]=z[29]*z[150];
    z[134]=z[131] + n<T>(112,3)*z[149] + z[143] + n<T>(4,3)*z[137] + z[148] + n<T>(2,3)*z[150]
     + n<T>(1,9)*z[144]
     + z[145]
     + z[146]
     + n<T>(1,3)*z[155]
     + z[142]
    + z[140] + z[153] + z[147] + n<T>(14,3)*z[138] + n<T>(1,27)*z[141] + z[151]
    + z[154] + z[139] + z[136] + n<T>(7,3)*z[134] + n<T>(7,9)*z[135];
    z[135]=z[125] - z[91] + z[84] - z[74] - z[129] + z[128] + z[127] - 
    z[126];
    z[136]=z[78] + z[119] - z[98];
    z[137]=z[132] - z[120];
    z[138]=z[133] + z[79];
    z[139]=n<T>(44464,27)*z[49] - n<T>(361270,243)*z[48] - n<T>(8836,405)*z[47] + 
   n<T>(5558,9)*z[46] - n<T>(7952,9)*z[45] - n<T>(44464,9)*z[43] - n<T>(6688,3)*z[42]
    + n<T>(4261792,10935)*z[40] + n<T>(111160,27)*z[39] - n<T>(605822,2187)*z[38]
    + n<T>(5124824,1215)*z[37] + n<T>(449248,45)*z[36] - n<T>(123476,81)*z[34];
    z[139]=i*z[139];

    r += n<T>(67517,3645)*z[33] + n<T>(65488,45)*z[35] - n<T>(266912,81)*z[41] - 
      n<T>(4093,135)*z[44] - n<T>(50809,81)*z[70] - n<T>(124343,81)*z[71] - n<T>(126611,81)*z[72]
     - n<T>(48541,81)*z[73]
     + n<T>(43924,243)*z[75] - n<T>(50998,81)*
      z[76] - n<T>(1,3)*z[77] - n<T>(120752,81)*z[80] - n<T>(114056,81)*z[81] - 
      n<T>(53212,27)*z[82] - n<T>(123128,81)*z[83] + n<T>(140804,243)*z[85] - n<T>(98572,243)*z[86]
     + 12*z[87] - n<T>(440,9)*z[88]
     + n<T>(81292,81)*z[89]
     + n<T>(124,3)*z[90] - n<T>(232,27)*z[92] - n<T>(81076,243)*z[93] - n<T>(142091,162)*
      z[94] + n<T>(111809,54)*z[95] + n<T>(113321,54)*z[96] - n<T>(146627,162)*
      z[97] - n<T>(1520,27)*z[99] + n<T>(127727,243)*z[100] - n<T>(1049,18)*z[101]
       - n<T>(1049,9)*z[102] - n<T>(2098,9)*z[103] - n<T>(251224,81)*z[104] + n<T>(88960,81)*z[105]
     + n<T>(160,9)*z[106] - n<T>(72652,81)*z[107]
     + n<T>(58852,81)*
      z[108] + n<T>(17548,3)*z[109] - n<T>(47848,81)*z[110] - n<T>(50780,81)*z[111]
       - n<T>(61936,81)*z[112] + n<T>(44950,81)*z[113] - n<T>(210418,81)*z[114] + 
      n<T>(311782,81)*z[115] + n<T>(47326,81)*z[116] - n<T>(55012,81)*z[117] + 
      z[118] - n<T>(86372,81)*z[121] + n<T>(87196,81)*z[122] + n<T>(58768,27)*
      z[123];
      r+=  - n<T>(58612,27)*z[124] - z[130] + 2*z[134] + n<T>(7,9)*z[135] - n<T>(2,3)*z[136]
     + n<T>(8,3)*z[137] - n<T>(4,3)*z[138]
     + z[139];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf819(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf819(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
