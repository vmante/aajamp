#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1037(
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[6];
  std::complex<T> r(0,0);

    z[1]=d[0];
    z[2]=d[3];
    z[3]=e[0];
    z[4]=npow(z[1],2);
    z[5]= - z[1] + n<T>(1,2)*z[2];
    z[5]=z[2]*z[5];

    r += z[3] + n<T>(1,2)*z[4] + z[5];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1037(
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1037(
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
