#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1199(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[64];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[4];
    z[9]=d[16];
    z[10]=d[12];
    z[11]=d[17];
    z[12]=e[0];
    z[13]=e[4];
    z[14]=e[5];
    z[15]=e[7];
    z[16]=e[8];
    z[17]=e[9];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[30];
    z[27]=c[31];
    z[28]=e[6];
    z[29]=f[0];
    z[30]=f[1];
    z[31]=f[3];
    z[32]=f[5];
    z[33]=f[11];
    z[34]=f[17];
    z[35]=f[18];
    z[36]=f[31];
    z[37]=f[32];
    z[38]=f[33];
    z[39]=f[34];
    z[40]=f[36];
    z[41]=f[39];
    z[42]=f[43];
    z[43]=f[49];
    z[44]=3*z[7];
    z[45]=2*z[6];
    z[46]=n<T>(1,2)*z[4];
    z[47]=z[1]*i;
    z[48]=z[46] + z[45] + z[47] - z[44];
    z[48]=z[4]*z[48];
    z[49]=2*z[47];
    z[50]=z[49] - z[7];
    z[51]=2*z[7];
    z[52]= - z[50]*z[51];
    z[51]=z[51] - z[47];
    z[53]= - n<T>(3,2)*z[6] + z[51];
    z[53]=z[6]*z[53];
    z[54]=n<T>(1,2)*z[2];
    z[55]=z[7] + z[47] - z[6];
    z[55]= - n<T>(7,3)*z[2] + 3*z[55] + z[4];
    z[55]=z[55]*z[54];
    z[48]=z[55] + z[48] + z[52] + z[53];
    z[48]=z[2]*z[48];
    z[52]=n<T>(1,2)*z[7];
    z[53]=z[52] - z[47];
    z[44]= - z[53]*z[44];
    z[55]=npow(z[6],2);
    z[44]=z[44] + 19*z[55];
    z[45]=z[45] + z[51];
    z[45]=z[4]*z[45];
    z[55]=z[47] - z[7];
    z[56]=z[55] - z[6];
    z[57]=2*z[2];
    z[58]=z[56]*z[57];
    z[59]=z[2] - z[4];
    z[60]= - n<T>(41,2)*z[55] + z[6];
    z[60]= - n<T>(73,6)*z[5] + n<T>(1,2)*z[60] + 5*z[59];
    z[61]=n<T>(1,2)*z[5];
    z[60]=z[60]*z[61];
    z[44]=z[60] + z[58] + n<T>(1,4)*z[44] + z[45];
    z[44]=z[5]*z[44];
    z[45]=z[7]*z[53];
    z[53]=3*z[6];
    z[58]= - n<T>(3,2)*z[55] + z[6];
    z[58]=z[58]*z[53];
    z[60]=z[61] + z[55];
    z[61]=n<T>(41,4)*z[5];
    z[60]=z[60]*z[61];
    z[62]= - n<T>(23,2)*z[55] - 11*z[6];
    z[61]= - n<T>(21,2)*z[8] + z[61] + n<T>(1,2)*z[62] - z[2];
    z[62]=n<T>(1,2)*z[8];
    z[61]=z[61]*z[62];
    z[45]=z[61] + z[60] + n<T>(23,4)*z[45] + z[58];
    z[45]=z[8]*z[45];
    z[58]=n<T>(1,2)*z[6];
    z[60]=4*z[47];
    z[46]=z[3] + z[54] + z[46] + z[58] - z[60] - z[7];
    z[46]=z[3]*z[46];
    z[54]= - z[2] - z[6];
    z[54]=z[47]*z[54];
    z[61]= - z[4] + z[51];
    z[61]=z[4]*z[61];
    z[62]= - z[47] + z[62];
    z[62]=z[8]*z[62];
    z[63]=z[50]*z[7];
    z[46]=z[46] + z[62] + z[61] + z[63] + z[54];
    z[46]=z[3]*z[46];
    z[52]=n<T>(5,3)*z[4] - z[58] - z[60] + z[52];
    z[52]=z[4]*z[52];
    z[51]= - z[6] - z[51];
    z[51]=z[6]*z[51];
    z[51]=z[52] + z[63] + z[51];
    z[51]=z[4]*z[51];
    z[52]=z[6] + z[7];
    z[52]=z[47]*z[52];
    z[52]=z[18] + z[52];
    z[52]=z[11]*z[52];
    z[54]=z[17]*z[56];
    z[52]= - z[20] + z[52] - z[54];
    z[54]=n<T>(3,4)*z[55] - z[6];
    z[53]=z[54]*z[53];
    z[54]=z[7]*z[55];
    z[53]=n<T>(5,2)*z[54] + z[53];
    z[53]=z[6]*z[53];
    z[54]= - 7*z[55] - 19*z[6];
    z[54]= - n<T>(27,2)*z[5] + n<T>(1,2)*z[54] + 4*z[59];
    z[54]=z[15]*z[54];
    z[55]=z[3] + z[8];
    z[56]=z[47]*z[55];
    z[56]=z[18] + z[56];
    z[56]=z[9]*z[56];
    z[58]=z[5] + z[4];
    z[59]= - z[47]*z[58];
    z[59]= - z[18] + z[59];
    z[59]=z[10]*z[59];
    z[55]=z[47] - z[55];
    z[55]=z[13]*z[55];
    z[58]= - z[47] + z[58];
    z[58]=z[14]*z[58];
    z[60]=z[35] + z[21];
    z[47]=3*z[47] - z[7];
    z[47]=z[47]*npow(z[7],2);
    z[47]=z[47] + z[34];
    z[61]= - n<T>(3,2)*z[22] - n<T>(7,8)*z[19];
    z[61]=i*z[61];
    z[57]=z[57] - n<T>(91,8)*z[4] + 7*z[7] + n<T>(11,4)*z[6];
    z[57]= - n<T>(39,8)*z[3] - n<T>(23,6)*z[8] + n<T>(1,3)*z[57] - n<T>(5,4)*z[5];
    z[57]=z[18]*z[57];
    z[49]= - z[3] + z[49] - z[2];
    z[49]=z[12]*z[49];
    z[50]= - z[2] + z[50];
    z[50]=z[16]*z[50];
    z[62]= - z[6] - z[8];
    z[62]=z[28]*z[62];
    z[63]=4*i;
    z[63]= - z[24]*z[63];

    r += n<T>(1,6)*z[23] - 4*z[25] - 12*z[26] + n<T>(169,18)*z[27] - z[29] + 
      z[30] - n<T>(7,2)*z[31] - 6*z[32] - n<T>(3,2)*z[33] + n<T>(5,8)*z[36] + 2*
      z[37] + 5*z[38] - z[39] - n<T>(15,8)*z[40] + n<T>(53,8)*z[41] - z[42] + 
      z[43] + z[44] + z[45] + z[46] + n<T>(1,2)*z[47] + z[48] + z[49] + 
      z[50] + z[51] + n<T>(5,2)*z[52] + z[53] + z[54] + z[55] + z[56] + 
      z[57] + z[58] + z[59] - n<T>(1,3)*z[60] + z[61] + n<T>(21,2)*z[62] + 
      z[63];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1199(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1199(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
