#ifndef QG_2LNLC_TF_DECL_H
#define QG_2LNLC_TF_DECL_H

#include <array>
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
std::complex<T> qg_2lNLC_tf1(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf2(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf3(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf4(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf5(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf6(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf7(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf8(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf9(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf10(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf11(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf12(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf13(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf14(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf15(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf16(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf17(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf18(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf19(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf20(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf21(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf22(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf23(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf24(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf25(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf26(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf27(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf28(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf29(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf30(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf31(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf32(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf33(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf34(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf35(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf36(
  const std::array<T,85>&);

template <class T>
std::complex<T> qg_2lNLC_tf37(
  const std::array<T,85>&);

template <class T>
std::complex<T> qg_2lNLC_tf38(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf39(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf40(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf41(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf42(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf43(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf44(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf45(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf46(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf47(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf48(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf49(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf50(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf51(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf52(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf53(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf54(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf55(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf56(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf57(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf58(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf59(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf60(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf61(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf62(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf63(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf64(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf65(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf66(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf67(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf68(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf69(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf70(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf71(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf72(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf73(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf74(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf75(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf76(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf77(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf78(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf79(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf80(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf81(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf82(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf83(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf84(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf85(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf86(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf87(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf88(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf89(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf90(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf91(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf92(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf93(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf94(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf95(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf96(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf97(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf98(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf99(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf100(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf101(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf102(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf103(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf104(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf105(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf106(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf107(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf108(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf109(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf110(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf111(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf112(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf113(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf114(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf115(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf116(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf117(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf118(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf119(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf120(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf121(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf122(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf123(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf124(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf125(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf126(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf127(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf128(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf129(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf130(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf131(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf132(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf133(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf134(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf135(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf136(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf137(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf138(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf139(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf140(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf141(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf142(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf143(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf144(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf145(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf146(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf147(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf148(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf149(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf150(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf151(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf152(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf153(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf154(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf155(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf156(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf157(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf158(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf159(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf160(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf161(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf162(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf163(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf164(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf165(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf166(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf167(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf168(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf169(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf170(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf171(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf172(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf173(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf174(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf175(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf176(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf177(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf178(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf179(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf180(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf181(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf182(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf183(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf184(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf185(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf186(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf187(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf188(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf189(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf190(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf191(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf192(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf193(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf194(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf195(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf196(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf197(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf198(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf199(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf200(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf201(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf202(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf203(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf204(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf205(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf206(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf207(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf208(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf209(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf210(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf211(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf212(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf213(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf214(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf215(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf216(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf217(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf218(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf219(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf220(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf221(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf222(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf223(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf224(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf225(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf226(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf227(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf228(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf229(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf230(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf231(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf232(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf233(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf234(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf235(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf236(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf237(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf238(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf239(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf240(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf241(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf242(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf243(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf244(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf245(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf246(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf247(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf248(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf249(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf250(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf251(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf252(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf253(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf254(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf255(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf256(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf257(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf258(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf259(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf260(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf261(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf262(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf263(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf264(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf265(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf266(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf267(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf268(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf269(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf270(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf271(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf272(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf273(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf274(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf275(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf276(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf277(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf278(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf279(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf280(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf281(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf282(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf283(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf284(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf285(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf286(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf287(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf288(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf289(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf290(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf291(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf292(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf293(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf294(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf295(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf296(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf297(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf298(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf299(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf300(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf301(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf302(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf303(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf304(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf305(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf306(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf307(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf308(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf309(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf310(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf311(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf312(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf313(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf314(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf315(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf316(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf317(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf318(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf319(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf320(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf321(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf322(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf323(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf324(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf325(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf326(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf327(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf328(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf329(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf330(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf331(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf332(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf333(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf334(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf335(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf336(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf337(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf338(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf339(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf340(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf341(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf342(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf343(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf344(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf345(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf346(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf347(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf348(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf349(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf350(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf351(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf352(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf353(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf354(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf355(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf356(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf357(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf358(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf359(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf360(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf361(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf362(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf363(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf364(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf365(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf366(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf367(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf368(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf369(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf370(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf371(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf372(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf373(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf374(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf375(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf376(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf377(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf378(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf379(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf380(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf381(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf382(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf383(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf384(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf385(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf386(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf387(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf388(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf389(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf390(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf391(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf392(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf393(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf394(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf395(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf396(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf397(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf398(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf399(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf400(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf401(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf402(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf403(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf404(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf405(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf406(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf407(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf408(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf409(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf410(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf411(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf412(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf413(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf414(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf415(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf416(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf417(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf418(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf419(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf420(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf421(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf422(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf423(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf424(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf425(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf426(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf427(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf428(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf429(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf430(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf431(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf432(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf433(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf434(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf435(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf436(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf437(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf438(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf439(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf440(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf441(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf442(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf443(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf444(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf445(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf446(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf447(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf448(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf449(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf450(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf451(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf452(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf453(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf454(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf455(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf456(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf457(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf458(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf459(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf460(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf461(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf462(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf463(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf464(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf465(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf466(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf467(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf468(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf469(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf470(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf471(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf472(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf473(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf474(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf475(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf476(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf477(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf478(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf479(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf480(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf481(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf482(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf483(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf484(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf485(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf486(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf487(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf488(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf489(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf490(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf491(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf492(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf493(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf494(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf495(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf496(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf497(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf498(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf499(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf500(
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf501(
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf502(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf503(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf504(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf505(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf506(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf507(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf508(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf509(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf510(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf511(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf512(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf513(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf514(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf515(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf516(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf517(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf518(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf519(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf520(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf521(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf522(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf523(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf524(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf525(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf526(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf527(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf528(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf529(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf530(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf531(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf532(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf533(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf534(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf535(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf536(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf537(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf538(
  const std::array<T,85>&);

template <class T>
std::complex<T> qg_2lNLC_tf539(
  const std::array<T,85>&);

template <class T>
std::complex<T> qg_2lNLC_tf540(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf541(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf542(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf543(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf544(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf545(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf546(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf547(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf548(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf549(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf550(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf551(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf552(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf553(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf554(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf555(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf556(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf557(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf558(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf559(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf560(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf561(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf562(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf563(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf564(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf565(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf566(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf567(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf568(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf569(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf570(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf571(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf572(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf573(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf574(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf575(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf576(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf577(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf578(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf579(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf580(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf581(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf582(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf583(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf584(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf585(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf586(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf587(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf588(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf589(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf590(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf591(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf592(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf593(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf594(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf595(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf596(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf597(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf598(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf599(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf600(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf601(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf602(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf603(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf604(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf605(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf606(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf607(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf608(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf609(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf610(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf611(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf612(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf613(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf614(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf615(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf616(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf617(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf618(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf619(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf620(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf621(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf622(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf623(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf624(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf625(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf626(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf627(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf628(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf629(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf630(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf631(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf632(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf633(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf634(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf635(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf636(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf637(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf638(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf639(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf640(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf641(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf642(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf643(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf644(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf645(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf646(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf647(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf648(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf649(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf650(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf651(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf652(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf653(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf654(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf655(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf656(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf657(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf658(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf659(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf660(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf661(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf662(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf663(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf664(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf665(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf666(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf667(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf668(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf669(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf670(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf671(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf672(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf673(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf674(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf675(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf676(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf677(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf678(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf679(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf680(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf681(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf682(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf683(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf684(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf685(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf686(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf687(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf688(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf689(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf690(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf691(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf692(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf693(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf694(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf695(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf696(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf697(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf698(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf699(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf700(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf701(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf702(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf703(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf704(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf705(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf706(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf707(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf708(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf709(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf710(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf711(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf712(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf713(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf714(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf715(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf716(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf717(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf718(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf719(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf720(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf721(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf722(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf723(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf724(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf725(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf726(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf727(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf728(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf729(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf730(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf731(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf732(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf733(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf734(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf735(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf736(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf737(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf738(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf739(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf740(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf741(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf742(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf743(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf744(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf745(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf746(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf747(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf748(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf749(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf750(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf751(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf752(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf753(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf754(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf755(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf756(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf757(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf758(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf759(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf760(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf761(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf762(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf763(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf764(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf765(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf766(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf767(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf768(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf769(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf770(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf771(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf772(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf773(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf774(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf775(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf776(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf777(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf778(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf779(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf780(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf781(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf782(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf783(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf784(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf785(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf786(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf787(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf788(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf789(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf790(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf791(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf792(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf793(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf794(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf795(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf796(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf797(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf798(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf799(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf800(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf801(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf802(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf803(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf804(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf805(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf806(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf807(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf808(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf809(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf810(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf811(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf812(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf813(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf814(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf815(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf816(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf817(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf818(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf819(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf820(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf821(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf822(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf823(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf824(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf825(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf826(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf827(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf828(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf829(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf830(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf831(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf832(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf833(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf834(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf835(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf836(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf837(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf838(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf839(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf840(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf841(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf842(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf843(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf844(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf845(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf846(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf847(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf848(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf849(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf850(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf851(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf852(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf853(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf854(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf855(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf856(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf857(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf858(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf859(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf860(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf861(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf862(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf863(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf864(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf865(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf866(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf867(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf868(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf869(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf870(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf871(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf872(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf873(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf874(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf875(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf876(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf877(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf878(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf879(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf880(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf881(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf882(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf883(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf884(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf885(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf886(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf887(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf888(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf889(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf890(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf891(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf892(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf893(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf894(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf895(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf896(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf897(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf898(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf899(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf900(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf901(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf902(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf903(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf904(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf905(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf906(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf907(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf908(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf909(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf910(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf911(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf912(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf913(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf914(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf915(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf916(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf917(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf918(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf919(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf920(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf921(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf922(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf923(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf924(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf925(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf926(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf927(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf928(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf929(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf930(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf931(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf932(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf933(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf934(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf935(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf936(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf937(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf938(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf939(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf940(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf941(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf942(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf943(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf944(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf945(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf946(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf947(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf948(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf949(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf950(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf951(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf952(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf953(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf954(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf955(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf956(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf957(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf958(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf959(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf960(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf961(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf962(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf963(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf964(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf965(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf966(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf967(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf968(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf969(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf970(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf971(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf972(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf973(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf974(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf975(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf976(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf977(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf978(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf979(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf980(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf981(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf982(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf983(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf984(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf985(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf986(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf987(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf988(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf989(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf990(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf991(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf992(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf993(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf994(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf995(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf996(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf997(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf998(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf999(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1000(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1001(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1002(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1003(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1004(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1005(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1006(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1007(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1008(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1009(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1010(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1011(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1012(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1013(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1014(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1015(
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1016(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1017(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1018(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1019(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1020(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1021(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1022(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1023(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1024(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1025(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1026(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1027(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1028(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1029(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1030(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1031(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1032(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1033(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1034(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1035(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1036(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1037(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1038(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1039(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1040(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1041(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1042(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1043(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1044(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1045(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1046(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1047(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1048(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1049(
  const std::array<T,85>&);

template <class T>
std::complex<T> qg_2lNLC_tf1050(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1051(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1052(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1053(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1054(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1055(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1056(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1057(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1058(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1059(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1060(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1061(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1062(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1063(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1064(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1065(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1066(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1067(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1068(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1069(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1070(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1071(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1072(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1073(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1074(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1075(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1076(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1077(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1078(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1079(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1080(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1081(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1082(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1083(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1084(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1085(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1086(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1087(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1088(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1089(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1090(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1091(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1092(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1093(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1094(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1095(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1096(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1097(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1098(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1099(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1100(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1101(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1102(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1103(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1104(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1105(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1106(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1107(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1108(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1109(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1110(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1111(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1112(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1113(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1114(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1115(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1116(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1117(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1118(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1119(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1120(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1121(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1122(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1123(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1124(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1125(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1126(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1127(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1128(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1129(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1130(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1131(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1132(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1133(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1134(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1135(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1136(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1137(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1138(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1139(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1140(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1141(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1142(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1143(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1144(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1145(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1146(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1147(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1148(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1149(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1150(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1151(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1152(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1153(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1154(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1155(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1156(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1157(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1158(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1159(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1160(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1161(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1162(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1163(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1164(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1165(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1166(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1167(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1168(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1169(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1170(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1171(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1172(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1173(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1174(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1175(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1176(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1177(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1178(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1179(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1180(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1181(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1182(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1183(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1184(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1185(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1186(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1187(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1188(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1189(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1190(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1191(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1192(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1193(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1194(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1195(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1196(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1197(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1198(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1199(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1200(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1201(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1202(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1203(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1204(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1205(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1206(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1207(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1208(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1209(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1210(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1211(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1212(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1213(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1214(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1215(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1216(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1217(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1218(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1219(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1220(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1221(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1222(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1223(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1224(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1225(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1226(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1227(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1228(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1229(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1230(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1231(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1232(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1233(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1234(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1235(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1236(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1237(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1238(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1239(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1240(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1241(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1242(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1243(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1244(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1245(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1246(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1247(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1248(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1249(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1250(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1251(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1252(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1253(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1254(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1255(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1256(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1257(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1258(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1259(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1260(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1261(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1262(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1263(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1264(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1265(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1266(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1267(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1268(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1269(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1270(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1271(
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1272(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1273(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1274(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1275(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1276(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1277(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1278(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1279(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1280(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1281(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1282(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1283(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1284(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1285(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1286(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1287(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1288(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1289(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1290(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1291(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1292(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1293(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1294(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1295(
  const std::array<T,85>&);

template <class T>
std::complex<T> qg_2lNLC_tf1296(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1297(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1298(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1299(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1300(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1301(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1302(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1303(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1304(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1305(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1306(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1307(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1308(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1309(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1310(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1311(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1312(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1313(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1314(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1315(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1316(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1317(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1318(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1319(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1320(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1321(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1322(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1323(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1324(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1325(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1326(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1327(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1328(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1329(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1330(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1331(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1332(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1333(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1334(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1335(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1336(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1337(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1338(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1339(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1340(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1341(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1342(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1343(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1344(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1345(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1346(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1347(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1348(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1349(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1350(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1351(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1352(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1353(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1354(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1355(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1356(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1357(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1358(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1359(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1360(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1361(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1362(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1363(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1364(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1365(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1366(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1367(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1368(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1369(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1370(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qg_2lNLC_tf1371(
  const std::array<T,85>&);

template <class T>
std::complex<T> qg_2lNLC_tf1372(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1373(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1374(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1375(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1376(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1377(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1378(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1379(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1380(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1381(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1382(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1383(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1384(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1385(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1386(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1387(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1388(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1389(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1390(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1391(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1392(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1393(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1394(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1395(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1396(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1397(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1398(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1399(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_2lNLC_tf1400(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1401(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1402(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1403(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1404(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1405(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1406(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1407(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1408(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1409(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1410(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1411(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1412(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1413(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1414(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1415(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1416(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1417(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1418(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1419(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1420(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1421(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1422(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1423(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1424(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1425(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1426(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1427(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1428(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1429(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1430(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1431(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1432(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1433(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1434(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1435(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1436(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1437(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1438(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1439(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1440(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1441(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1442(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1443(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1444(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1445(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1446(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1447(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1448(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1449(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1450(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1451(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1452(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1453(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1454(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1455(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1456(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1457(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1458(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1459(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1460(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1461(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qg_2lNLC_tf1462(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1463(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1464(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1465(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1466(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1467(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1468(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1469(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1470(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1471(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1472(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1473(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1474(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1475(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1476(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1477(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1478(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1479(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1480(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qg_2lNLC_tf1481(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

#endif /* QG_2LNLC_TF_DECL_H */
