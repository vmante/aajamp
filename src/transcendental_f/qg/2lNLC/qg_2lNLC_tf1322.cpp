#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1322(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[70];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[4];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[1];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=d[18];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[4];
    z[16]=e[8];
    z[17]=e[10];
    z[18]=e[11];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[19];
    z[23]=c[23];
    z[24]=c[25];
    z[25]=c[26];
    z[26]=c[28];
    z[27]=e[3];
    z[28]=f[0];
    z[29]=f[1];
    z[30]=f[3];
    z[31]=f[4];
    z[32]=f[6];
    z[33]=f[7];
    z[34]=f[9];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[13];
    z[38]=f[14];
    z[39]=f[16];
    z[40]=f[17];
    z[41]=f[18];
    z[42]=f[21];
    z[43]=3*z[5];
    z[44]= - z[43] + n<T>(7,3)*z[7];
    z[45]=z[44] + n<T>(8,3)*z[4];
    z[46]=n<T>(8,3)*z[9];
    z[47]=n<T>(3,2)*z[3];
    z[48]=n<T>(3,2)*z[6];
    z[49]= - n<T>(4,3)*z[8] - z[47] + z[48] + z[46] + z[45];
    z[49]=z[8]*z[49];
    z[50]=n<T>(1,2)*z[7];
    z[51]=n<T>(1,2)*z[6];
    z[52]= - z[51] - z[11] + z[50];
    z[52]=z[6]*z[52];
    z[53]=2*z[10];
    z[54]=3*z[6];
    z[55]= - z[3] + z[54] + z[53] - n<T>(5,2)*z[5];
    z[55]=z[3]*z[55];
    z[56]=n<T>(5,2)*z[6];
    z[57]=z[9] - 2*z[7];
    z[57]=n<T>(35,6)*z[3] + n<T>(8,3)*z[57] - z[56];
    z[57]=z[4]*z[57];
    z[58]=n<T>(8,3)*z[14];
    z[59]=2*z[15];
    z[60]=2*z[17];
    z[61]=z[53] - n<T>(7,4)*z[5];
    z[61]=z[5]*z[61];
    z[43]= - z[43] + n<T>(11,12)*z[7];
    z[62]= - z[11] - z[43];
    z[62]=z[7]*z[62];
    z[63]=z[7] + z[5];
    z[64]= - n<T>(53,3)*z[3] - z[6] + z[63];
    z[64]=n<T>(35,6)*z[2] + n<T>(1,2)*z[64] - n<T>(10,3)*z[4];
    z[64]=z[2]*z[64];
    z[49]=z[49] + z[64] + z[57] + z[55] + z[52] + z[62] + z[61] + n<T>(37,3)
   *z[12] + n<T>(4,3)*z[13] - z[58] + z[59] + z[16] - z[60] - z[18];
    z[49]=z[1]*z[49];
    z[52]=n<T>(5,12)*z[20] - 44*z[25] - n<T>(35,2)*z[23];
    z[49]=n<T>(1,3)*z[52] + z[49];
    z[49]=i*z[49];
    z[52]=npow(z[7],2);
    z[55]= - z[60] + n<T>(1,4)*z[52];
    z[57]= - z[7] + z[51];
    z[57]=z[57]*z[51];
    z[60]=n<T>(1,2)*z[3];
    z[61]= - z[5] + n<T>(53,6)*z[3];
    z[61]=z[61]*z[60];
    z[62]=z[4] + z[3];
    z[62]= - n<T>(17,6)*z[2] + n<T>(1,4)*z[63] + n<T>(4,3)*z[62];
    z[62]=z[2]*z[62];
    z[64]=n<T>(1,2)*z[16];
    z[65]=n<T>(37,6)*z[12];
    z[66]=n<T>(5,18)*z[19];
    z[67]=npow(z[5],2);
    z[68]=n<T>(5,4)*z[67];
    z[69]=n<T>(5,3)*z[4] - n<T>(1,2)*z[63] - n<T>(8,3)*z[3];
    z[69]=z[4]*z[69];
    z[57]=z[62] + z[69] + z[61] + z[57] - z[68] + z[66] - z[65] - z[64]
    - n<T>(2,3)*z[13] + z[55];
    z[57]=z[2]*z[57];
    z[61]= - z[7] - z[48];
    z[61]=z[61]*z[51];
    z[47]= - z[5] + z[47];
    z[47]=z[47]*z[60];
    z[60]=z[60] + z[51];
    z[45]= - z[45] - z[60];
    z[45]=z[4]*z[45];
    z[44]=z[44] + z[60];
    z[44]=n<T>(11,3)*z[8] - n<T>(3,2)*z[2] + n<T>(1,2)*z[44] - n<T>(1,3)*z[4];
    z[44]=z[8]*z[44];
    z[60]=n<T>(7,2)*z[27];
    z[62]=z[3] + z[6] + z[63];
    z[62]= - z[2] + n<T>(1,2)*z[62] + z[4];
    z[62]=z[2]*z[62];
    z[44]=z[44] + z[62] + z[45] + z[47] + z[61] - n<T>(29,12)*z[52] - n<T>(13,4)
   *z[67] + n<T>(2,3)*z[19] + n<T>(31,6)*z[17] + z[60] + z[58];
    z[44]=z[8]*z[44];
    z[45]=z[7] + z[56];
    z[45]=z[45]*z[51];
    z[47]=n<T>(1,2)*z[5];
    z[52]= - n<T>(35,12)*z[3] + z[47] + z[54];
    z[52]=z[3]*z[52];
    z[56]=4*z[14] - z[13];
    z[56]=n<T>(1,3)*z[56] + z[17];
    z[43]=z[7]*z[43];
    z[48]= - n<T>(1,6)*z[3] + n<T>(8,3)*z[7] - z[48];
    z[48]=z[4]*z[48];
    z[43]=z[48] + z[52] + z[45] + z[43] + n<T>(7,4)*z[67] + 2*z[56] + z[66];
    z[43]=z[4]*z[43];
    z[45]=z[47] - z[54];
    z[45]=n<T>(1,2)*z[45] - n<T>(1,3)*z[3];
    z[45]=z[3]*z[45];
    z[47]=npow(z[6],2);
    z[45]=z[45] - n<T>(3,2)*z[47] + z[68] - n<T>(25,36)*z[19] - z[59] - z[65];
    z[45]=z[3]*z[45];
    z[47]= - z[67] - n<T>(1,6)*z[19] + z[60] - z[59];
    z[47]=z[5]*z[47];
    z[48]=z[50] + z[6];
    z[48]=z[48]*z[51];
    z[48]=z[48] - n<T>(3,4)*z[19] + z[18] - z[64] - z[55];
    z[48]=z[6]*z[48];
    z[46]=z[46] - z[11] + z[53];
    z[46]=z[19]*z[46];
    z[50]=z[7]*z[5];
    z[50]=z[67] + z[50];
    z[50]= - n<T>(113,36)*z[19] + z[18] + n<T>(43,6)*z[17] + n<T>(3,2)*z[50];
    z[50]=z[7]*z[50];
    z[51]=z[39] - z[40];
    z[52]=z[34] - z[38];
    z[53]=z[29] + z[35];

    r +=  - n<T>(11,3)*z[21] - n<T>(5,18)*z[22] + n<T>(17,6)*z[24] + n<T>(22,3)*z[26]
       - 3*z[28] - n<T>(35,12)*z[30] + n<T>(11,12)*z[31] + n<T>(3,4)*z[32] + 8*
      z[33] - n<T>(7,4)*z[36] - n<T>(8,3)*z[37] - z[41] + z[42] + z[43] + z[44]
       + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + n<T>(3,2)*z[51] + 
      n<T>(1,4)*z[52] + n<T>(5,4)*z[53] + z[57];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1322(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1322(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
