#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf287(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[69];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[11];
    z[11]=d[16];
    z[12]=d[5];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[0];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=e[13];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[11];
    z[43]=f[12];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[18];
    z[47]=f[31];
    z[48]=f[39];
    z[49]=f[51];
    z[50]=f[55];
    z[51]=f[58];
    z[52]=n<T>(47,4)*z[18] + 3*z[21];
    z[53]=npow(z[13],2);
    z[53]=n<T>(5,2)*z[53];
    z[54]= - z[53] + 5*z[20];
    z[55]=npow(z[12],2);
    z[56]=3*z[10] + n<T>(47,4)*z[11];
    z[57]=n<T>(83,8)*z[4] - z[56];
    z[57]=z[4]*z[57];
    z[57]=z[57] - 7*z[55] + n<T>(5147,72)*z[16] - n<T>(965,36)*z[17] + n<T>(3041,72)
   *z[19] - z[54] - z[52];
    z[58]=n<T>(1,4)*z[2];
    z[59]= - 73*z[3] - 23*z[4];
    z[59]=n<T>(1,2)*z[59] + n<T>(1109,9)*z[2];
    z[59]=z[59]*z[58];
    z[60]=n<T>(653,48)*z[9];
    z[61]=n<T>(965,8)*z[8] + 61*z[3];
    z[61]= - z[60] - n<T>(5111,144)*z[2] + n<T>(1,9)*z[61] + n<T>(35,4)*z[4];
    z[61]=z[5]*z[61];
    z[62]=3*z[15];
    z[63]= - n<T>(659,72)*z[7] - n<T>(983,144)*z[5] + z[60] - n<T>(73,8)*z[2] - 
    z[62] + n<T>(211,18)*z[3];
    z[63]=z[7]*z[63];
    z[64]=n<T>(1,2)*z[6];
    z[65]= - n<T>(15,4)*z[3] + z[14] - z[13];
    z[65]= - n<T>(35,4)*z[6] - n<T>(69,4)*z[7] + n<T>(3887,72)*z[5] + n<T>(31,2)*z[9]
     - 
   n<T>(305,72)*z[2] + 5*z[65] - n<T>(47,4)*z[4];
    z[65]=z[65]*z[64];
    z[66]=z[10]*z[62];
    z[67]=z[8] - n<T>(1,2)*z[3];
    z[67]=z[3]*z[67];
    z[68]=z[11] - z[4];
    z[68]=3*z[68] - n<T>(167,32)*z[9];
    z[68]=z[9]*z[68];
    z[57]=z[65] + z[63] + z[61] + z[68] + z[59] + n<T>(1849,144)*z[67] + 
    z[66] + n<T>(1,2)*z[57];
    z[57]=z[1]*z[57];
    z[59]= - 89*z[28] - 49*z[26];
    z[59]=23*z[59] + n<T>(4403,24)*z[23];
    z[57]=n<T>(1,36)*z[59] + z[57];
    z[57]=i*z[57];
    z[59]=npow(z[4],2);
    z[54]= - n<T>(161,16)*z[59] + n<T>(1397,432)*z[22] + n<T>(5,2)*z[55] - n<T>(361,18)*
    z[19] + z[54];
    z[61]=n<T>(359,36)*z[6] - n<T>(3815,144)*z[5] - n<T>(31,4)*z[9] + n<T>(271,18)*z[2]
    - z[13] - n<T>(467,48)*z[4];
    z[61]=z[61]*z[64];
    z[63]=npow(z[9],2);
    z[64]=n<T>(391,288)*z[2] + z[13] + n<T>(73,8)*z[3];
    z[64]=z[2]*z[64];
    z[65]= - n<T>(757,144)*z[5] - n<T>(1127,72)*z[2] + n<T>(1,4)*z[3] + n<T>(77,3)*z[4];
    z[65]=z[5]*z[65];
    z[66]= - 75*z[3] + 71*z[2];
    z[66]=n<T>(3,2)*z[7] + n<T>(1,2)*z[66] - z[5];
    z[66]=z[7]*z[66];
    z[54]=z[61] + n<T>(1,4)*z[66] + z[65] + n<T>(31,8)*z[63] + n<T>(1,2)*z[54] + 
    z[64];
    z[54]=z[6]*z[54];
    z[54]=z[54] + z[57];
    z[57]=npow(z[15],2);
    z[61]=z[4]*z[15];
    z[57]=z[57] + z[61];
    z[60]=n<T>(983,288)*z[5] - z[60] + n<T>(61,9)*z[3] + z[58];
    z[61]=n<T>(1,4)*z[5];
    z[60]=z[60]*z[61];
    z[64]=npow(z[3],2);
    z[65]=19*z[36] + n<T>(47,36)*z[33];
    z[66]=z[2]*z[3];
    z[67]=n<T>(53,9)*z[5] - n<T>(7,8)*z[9] - n<T>(3,2)*z[4] + n<T>(277,144)*z[3]
     - z[12]
    - n<T>(9,4)*z[15];
    z[67]=z[7]*z[67];
    z[57]=n<T>(1,2)*z[67] + z[60] - n<T>(7,24)*z[63] - n<T>(73,32)*z[66] - n<T>(13,1152)
   *z[64] - n<T>(4655,3456)*z[22] - n<T>(5,16)*z[55] + n<T>(1,8)*z[65] + z[35] + n<T>(3,4)*z[57];
    z[57]=z[7]*z[57];
    z[60]=n<T>(1,2)*z[9];
    z[65]=z[2] - z[4];
    z[65]= - n<T>(209,144)*z[9] - 5*z[13] + n<T>(19,2)*z[3] + n<T>(23,8)*z[65];
    z[65]=z[65]*z[60];
    z[66]=5*z[34] - n<T>(107,8)*z[32];
    z[53]=z[65] + n<T>(29,8)*z[64] + n<T>(1045,576)*z[22] - n<T>(5,4)*z[55] + z[53]
    + n<T>(1,2)*z[66] - 3*z[18];
    z[53]=z[53]*z[60];
    z[60]= - n<T>(1849,16)*z[64] + n<T>(2497,48)*z[22] + n<T>(965,4)*z[17] - 383*
    z[16];
    z[65]=npow(z[2],2);
    z[66]=n<T>(653,16)*z[9] + n<T>(2605,24)*z[2] - n<T>(1471,24)*z[3] - 77*z[4];
    z[66]=z[5]*z[66];
    z[60]=n<T>(1,3)*z[66] + n<T>(43,16)*z[63] + n<T>(2155,144)*z[65] + n<T>(1,9)*z[60]
    - n<T>(35,4)*z[59];
    z[60]=z[60]*z[61];
    z[61]= - z[62] + n<T>(325,48)*z[4];
    z[61]=z[4]*z[61];
    z[52]=z[61] + n<T>(277,288)*z[22] + n<T>(9,2)*z[55] - n<T>(71,8)*z[31] + z[52];
    z[52]=z[4]*z[52];
    z[61]=z[47] - z[48];
    z[52]=z[52] - n<T>(1127,36)*z[41] - n<T>(3887,144)*z[42] - n<T>(43,16)*z[43]
     - 
   n<T>(311,16)*z[44] + n<T>(47,8)*z[45] + n<T>(23,12)*z[46] + n<T>(9,2)*z[49] - n<T>(13,2)
   *z[51] + 3*z[50] + n<T>(5,2)*z[61];
    z[55]=z[35] + n<T>(1,2)*z[55];
    z[61]=z[12]*z[55];
    z[56]= - n<T>(15,8)*z[13] + n<T>(965,36)*z[8] + 5*z[14] - z[56];
    z[56]= - n<T>(47,48)*z[15] + n<T>(1,2)*z[56] + n<T>(1,3)*z[12];
    z[56]=z[22]*z[56];
    z[56]=z[61] + z[56];
    z[61]=z[12] - z[15];
    z[61]=z[15]*z[61];
    z[55]=n<T>(1,2)*z[61] - z[21] - z[55];
    z[55]=z[15]*z[55];
    z[59]=23*z[59] + n<T>(12193,54)*z[22] - n<T>(2083,9)*z[16] + 71*z[31] - n<T>(1597,9)*z[19];
    z[61]= - z[13] - n<T>(1903,72)*z[2];
    z[61]=z[2]*z[61];
    z[59]=n<T>(1,8)*z[59] + z[61];
    z[58]=z[59]*z[58];
    z[59]=n<T>(463,9)*z[64] - n<T>(1835,9)*z[22] + n<T>(1849,9)*z[17] + n<T>(499,9)*
    z[33] - 161*z[32];
    z[59]=z[3]*z[59];
    z[52]=z[60] + z[53] + z[58] + n<T>(1,32)*z[59] + n<T>(3,2)*z[55] - n<T>(3941,576)
   *z[24] - n<T>(437,216)*z[25] + n<T>(1127,144)*z[27] + n<T>(2047,144)*z[29] + 
   n<T>(16451,1152)*z[30] + n<T>(23,16)*z[37] - n<T>(23,32)*z[38] - n<T>(35,16)*z[39]
    + n<T>(53,9)*z[40] + n<T>(1,4)*z[52] + n<T>(1,2)*z[56];

    r += n<T>(1,2)*z[52] + n<T>(1,4)*z[54] + z[57];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf287(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf287(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
