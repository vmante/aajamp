#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf685(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[45];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[17];
    z[17]=c[19];
    z[18]=c[23];
    z[19]=c[25];
    z[20]=c[26];
    z[21]=c[28];
    z[22]=c[31];
    z[23]=e[3];
    z[24]=e[10];
    z[25]=f[3];
    z[26]=f[4];
    z[27]=f[5];
    z[28]=f[11];
    z[29]=f[12];
    z[30]=f[16];
    z[31]=f[17];
    z[32]=f[19];
    z[33]=z[1]*i;
    z[34]=z[33]*z[4];
    z[35]=npow(z[4],2);
    z[36]= - n<T>(2459,2)*z[35] - 24685*z[34];
    z[37]=11113*z[2];
    z[38]= - n<T>(1,4)*z[2] + n<T>(1,2)*z[4] + z[33];
    z[38]=z[38]*z[37];
    z[39]=n<T>(1,2)*z[5];
    z[37]=n<T>(24685,4)*z[4] - z[37];
    z[37]=z[37]*z[39];
    z[36]=z[37] + n<T>(1,4)*z[36] + z[38];
    z[36]=z[5]*z[36];
    z[37]=197*z[3];
    z[38]= - n<T>(4327,2)*z[4] - z[37];
    z[38]=z[33]*z[38];
    z[38]= - n<T>(4327,2)*z[13] + z[38];
    z[38]=z[7]*z[38];
    z[40]=z[33] - z[4];
    z[37]=n<T>(4327,2)*z[40] - z[37];
    z[37]=z[11]*z[37];
    z[41]=6589*z[4] - 2459*z[33];
    z[41]=n<T>(1,2)*z[41] - 2065*z[2];
    z[41]=z[10]*z[41];
    z[36]=z[36] + z[38] + z[41] + z[37];
    z[37]=n<T>(1,2)*z[35];
    z[38]=z[37] - z[34];
    z[41]=n<T>(1,2)*z[3];
    z[42]=z[41] + z[40];
    z[43]=17899*z[3];
    z[42]=z[42]*z[43];
    z[43]=n<T>(45043,4)*z[40] + z[43];
    z[43]=z[6]*z[43];
    z[42]=z[43] + n<T>(9245,2)*z[38] + z[42];
    z[42]=z[6]*z[42];
    z[39]=z[39] - z[33];
    z[43]= - z[4] + z[39];
    z[43]=z[5]*z[43];
    z[44]= - z[4] + z[5];
    z[44]=z[8]*z[44];
    z[37]=n<T>(1,2)*z[44] + z[43] + z[37] + z[34];
    z[37]=z[8]*z[37];
    z[35]=8851*z[35];
    z[43]= - z[35] + 2459*z[34];
    z[41]=z[41] + z[33];
    z[41]=z[3]*z[41];
    z[44]=2065*z[4] - 6983*z[33];
    z[44]=n<T>(6983,6)*z[2] + n<T>(1,3)*z[44] + n<T>(3573,2)*z[3];
    z[44]=z[2]*z[44];
    z[41]=z[44] + n<T>(1,3)*z[43] - 3573*z[41];
    z[41]=z[2]*z[41];
    z[34]=z[35] - 13375*z[34];
    z[33]=4721*z[4] + n<T>(2065,2)*z[33];
    z[33]=n<T>(1,3)*z[33] - n<T>(7857,2)*z[3];
    z[33]=z[3]*z[33];
    z[33]=n<T>(1,3)*z[34] + z[33];
    z[33]=z[3]*z[33];
    z[34]=n<T>(2065,6)*z[2] + n<T>(359,3)*z[4] - n<T>(1921,2)*z[3];
    z[34]= - n<T>(24685,12)*z[5] + 5*z[34] + n<T>(4327,3)*z[6];
    z[34]=n<T>(6589,12)*z[8] + n<T>(1,3)*z[34] + n<T>(2699,2)*z[9];
    z[34]=z[13]*z[34];
    z[33]=z[34] + z[33] + z[41];
    z[34]= - n<T>(1,2)*z[6] - z[40];
    z[34]=z[6]*z[34];
    z[34]=z[34] - z[38];
    z[35]= - z[6] - z[40];
    z[35]=n<T>(8851,6032)*z[35] + n<T>(1,3)*z[9];
    z[35]=z[9]*z[35];
    z[34]=n<T>(8851,3016)*z[34] + z[35];
    z[34]=z[9]*z[34];
    z[35]=n<T>(1,2)*z[2] + z[39];
    z[35]=z[12]*z[35];
    z[38]= - z[19] + z[28] + z[27];
    z[39]=z[31] - z[25];
    z[40]=n<T>(24685,4524)*z[20] + n<T>(24685,9048)*z[18] + n<T>(9,4)*z[16] - n<T>(4721,27144)*z[14];
    z[40]=i*z[40];
    z[41]=npow(z[4],3);
    z[43]= - z[3] - z[9];
    z[43]=z[23]*z[43];
    z[44]= - z[3] - z[6];
    z[44]=z[24]*z[44];

    r += n<T>(3725,2262)*z[15] + n<T>(24685,108576)*z[17] - n<T>(24685,9048)*z[21]
       - n<T>(23875,9048)*z[22] - n<T>(45043,18096)*z[26] - n<T>(2819,6032)*z[29]
       - n<T>(8851,6032)*z[30] + z[32] + n<T>(1,1508)*z[33] + z[34] + n<T>(11113,2262)*z[35]
     + n<T>(1,2262)*z[36]
     + n<T>(6589,3016)*z[37]
     + n<T>(24685,18096)*
      z[38] + n<T>(6589,6032)*z[39] + z[40] + n<T>(1,3)*z[41] + n<T>(1,4524)*z[42]
       + z[43] + n<T>(17899,4524)*z[44];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf685(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf685(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
