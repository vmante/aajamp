#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1326(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[38];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[17];
    z[17]=c[31];
    z[18]=e[6];
    z[19]=f[30];
    z[20]=f[31];
    z[21]=f[32];
    z[22]=f[33];
    z[23]=f[35];
    z[24]=f[36];
    z[25]=f[39];
    z[26]=f[43];
    z[27]=f[68];
    z[28]=z[8] + z[4];
    z[29]=z[4] - z[3];
    z[30]= - z[29]*z[28];
    z[31]=2*z[3];
    z[32]=z[1]*i;
    z[33]=z[32]*z[31];
    z[34]=npow(z[3],2);
    z[35]=n<T>(3,2)*z[34];
    z[36]=z[32] - z[5];
    z[37]=z[5]*z[36];
    z[30]= - n<T>(1,2)*z[37] + z[33] - z[35] + z[30];
    z[30]=z[5]*z[30];
    z[33]= - n<T>(1,2)*z[2] + z[8] - z[36];
    z[29]=z[29]*z[33];
    z[33]= - z[3] - z[4];
    z[33]=z[4]*z[33];
    z[29]=2*z[34] + z[33] + z[29];
    z[29]=z[2]*z[29];
    z[33]=z[2] - z[8];
    z[33]= - 8*z[3] - n<T>(13,2)*z[4] + z[36] + 2*z[33];
    z[33]=z[11]*z[33];
    z[37]=z[31] + z[5];
    z[37]= - z[32]*z[37];
    z[37]= - z[13] + z[37];
    z[37]=z[6]*z[37];
    z[31]=z[31] - z[36];
    z[31]=z[10]*z[31];
    z[29]=z[33] + z[37] + z[31] - z[26] + z[23] + z[30] + z[29];
    z[30]= - z[4] + n<T>(3,2)*z[3];
    z[31]=z[30]*z[32];
    z[33]=n<T>(1,2)*z[32];
    z[36]=n<T>(1,4)*z[8] - z[33] - z[30];
    z[36]=z[8]*z[36];
    z[30]= - z[7] + n<T>(1,2)*z[8] - z[33] + z[30];
    z[30]=z[7]*z[30];
    z[30]=n<T>(1,2)*z[30] + z[36] + n<T>(3,4)*z[34] + z[31];
    z[30]=z[7]*z[30];
    z[31]=z[7] + z[4];
    z[31]=z[18]*z[31];
    z[30]=z[30] - z[31] + z[21] + z[19];
    z[31]=z[32]*z[28];
    z[31]=z[13] + z[31];
    z[31]=z[9]*z[31];
    z[28]= - z[32] + z[28];
    z[28]=z[12]*z[28];
    z[28]=z[31] + z[28];
    z[31]=npow(z[4],2);
    z[33]=z[35] - z[31];
    z[35]= - n<T>(17,2)*z[3] + 3*z[4];
    z[35]=z[35]*z[32];
    z[36]=n<T>(17,4)*z[3] - 5*z[4];
    z[36]=z[8]*z[36];
    z[33]=z[36] + n<T>(3,2)*z[33] + z[35];
    z[33]=z[8]*z[33];
    z[35]=n<T>(33,4)*z[3] - 2*z[4];
    z[35]=z[4]*z[35];
    z[35]=n<T>(15,4)*z[34] + z[35];
    z[35]=z[4]*z[35];
    z[31]= - n<T>(13,2)*z[34] - 3*z[31];
    z[31]=z[31]*z[32];
    z[32]=n<T>(3,2)*z[16] + n<T>(7,8)*z[14];
    z[32]=i*z[32];
    z[34]=npow(z[3],3);
    z[36]= - 3*z[7] - z[5] - 17*z[3] + n<T>(7,4)*z[8];
    z[36]=z[13]*z[36];

    r +=  - n<T>(21,4)*z[15] - n<T>(21,8)*z[17] + n<T>(75,8)*z[20] + 21*z[22] - n<T>(9,8)*z[24]
     + n<T>(27,8)*z[25]
     + z[27]
     + n<T>(21,2)*z[28]
     + 3*z[29]
     + n<T>(9,2)*
      z[30] + n<T>(3,4)*z[31] + z[32] + n<T>(3,2)*z[33] - n<T>(37,4)*z[34] + z[35]
       + n<T>(1,2)*z[36];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1326(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1326(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
