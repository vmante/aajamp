#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1406(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[39];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[8];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[15];
    z[9]=d[18];
    z[10]=e[2];
    z[11]=e[10];
    z[12]=e[11];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=e[3];
    z[17]=f[2];
    z[18]=f[4];
    z[19]=f[6];
    z[20]=f[7];
    z[21]=f[10];
    z[22]=f[12];
    z[23]=f[16];
    z[24]=f[23];
    z[25]=n<T>(1,2)*z[7];
    z[26]=z[7] - z[2];
    z[27]= - z[26]*z[25];
    z[28]=n<T>(1,2)*z[2];
    z[29]=z[28] - z[5];
    z[30]=2*z[8];
    z[31]= - z[6] + z[7] + z[30] - z[29];
    z[32]=i*z[1];
    z[31]=z[31]*z[32];
    z[29]= - 2*z[4] + n<T>(1,2)*z[32] + z[29];
    z[29]=z[4]*z[29];
    z[33]=n<T>(1,2)*z[6];
    z[34]=n<T>(23,12)*z[3] + n<T>(1,4)*z[4] - z[32] - z[2] - z[33];
    z[34]=z[3]*z[34];
    z[35]=npow(z[2],2);
    z[35]=n<T>(1,4)*z[35];
    z[36]=2*z[10];
    z[37]=npow(z[6],2);
    z[38]= - n<T>(3,2)*z[5] - n<T>(1,2)*z[26] + z[6];
    z[38]=z[5]*z[38];
    z[27]=z[34] + z[29] + z[31] + z[38] - z[37] + z[27] - z[35] + n<T>(1,2)*
    z[13] + n<T>(7,2)*z[11] + z[16] + z[36];
    z[27]=z[3]*z[27];
    z[29]=n<T>(3,2)*z[6];
    z[31]=n<T>(5,2)*z[5];
    z[34]= - n<T>(5,2)*z[32] + z[31] + z[29] - z[26];
    z[34]=n<T>(1,2)*z[34] + n<T>(1,3)*z[4];
    z[34]=z[4]*z[34];
    z[29]= - z[31] + z[29] + z[7] - z[9] + z[28];
    z[29]=z[29]*z[32];
    z[26]=z[31] - 3*z[6] + z[26];
    z[26]=z[5]*z[26];
    z[28]= - z[7]*z[28];
    z[26]=z[34] + z[29] + n<T>(1,2)*z[26] + n<T>(3,4)*z[37] + z[28] + z[35] - n<T>(5,3)*z[13]
     + z[12]
     + n<T>(9,2)*z[11];
    z[26]=z[4]*z[26];
    z[28]=n<T>(1,4)*z[37] + z[36] + z[11];
    z[29]=z[30] - z[33];
    z[29]=z[5]*z[29];
    z[25]= - z[9] - z[25];
    z[25]=z[7]*z[25];
    z[25]=z[29] + z[25] - z[12] - z[28];
    z[25]=z[1]*z[25];
    z[25]=n<T>(1,4)*z[14] + z[25];
    z[25]=i*z[25];
    z[29]=z[5]*z[6];
    z[28]=n<T>(1,4)*z[29] + n<T>(1,6)*z[13] + z[28];
    z[28]=z[5]*z[28];
    z[29]=z[21] - n<T>(1,2)*z[22] - z[24] + n<T>(3,2)*z[23];
    z[30]= - z[9] + z[30];
    z[30]=z[13]*z[30];
    z[31]= - z[2]*z[11];
    z[32]=npow(z[7],2);
    z[32]=n<T>(1,2)*z[32] - n<T>(1,4)*z[13] + z[11] + z[12];
    z[32]=z[7]*z[32];
    z[33]=z[16] - n<T>(1,3)*z[13];
    z[33]=z[6]*z[33];

    r +=  - z[15] + z[17] + n<T>(7,4)*z[18] + z[19] + 4*z[20] + z[25] + 
      z[26] + z[27] + z[28] + n<T>(1,2)*z[29] + z[30] + z[31] + z[32] + 
      z[33];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1406(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1406(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
