#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf293(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[53];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[16];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[4];
    z[14]=e[8];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[19];
    z[19]=c[21];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[31];
    z[25]=e[0];
    z[26]=e[3];
    z[27]=e[10];
    z[28]=f[0];
    z[29]=f[1];
    z[30]=f[3];
    z[31]=f[4];
    z[32]=f[5];
    z[33]=f[11];
    z[34]=f[12];
    z[35]=f[16];
    z[36]=f[17];
    z[37]=f[18];
    z[38]=z[1]*i;
    z[39]=z[38]*z[4];
    z[40]=npow(z[4],2);
    z[41]= - z[40] + z[39];
    z[42]=npow(z[9],2);
    z[43]= - z[4] - z[38];
    z[43]=7*z[6] + n<T>(41,12)*z[43] - 3*z[9];
    z[43]=z[6]*z[43];
    z[41]=z[43] + n<T>(19,3)*z[41] - 5*z[42];
    z[41]=z[6]*z[41];
    z[42]= - 5*z[38] + 7*z[2];
    z[42]=n<T>(1,2)*z[42] - z[5];
    z[42]=z[14]*z[42];
    z[43]=z[3]*z[38];
    z[43]=z[43] + z[15];
    z[43]=z[10]*z[43];
    z[44]=z[38] - z[3];
    z[44]=z[13]*z[44];
    z[41]=z[28] + z[41] + z[42] + z[43] + z[44];
    z[42]= - z[39] + n<T>(1,2)*z[40];
    z[43]=n<T>(1,2)*z[2];
    z[44]= - z[38]*z[43];
    z[45]=n<T>(1,2)*z[38];
    z[46]=n<T>(11,12)*z[3] + z[43] - 3*z[4] - z[45];
    z[46]=z[3]*z[46];
    z[44]=n<T>(1,2)*z[46] - 3*z[42] + z[44];
    z[44]=z[3]*z[44];
    z[46]=n<T>(41,2)*z[6];
    z[47]=19*z[4] + z[46];
    z[47]=z[38]*z[47];
    z[47]=19*z[15] + z[47];
    z[47]=z[8]*z[47];
    z[48]=z[38] - z[4];
    z[49]=19*z[48];
    z[46]= - z[49] + z[46];
    z[46]=z[12]*z[46];
    z[46]=z[37] + z[47] + z[46];
    z[43]= - z[43] + n<T>(5,8)*z[4] + z[38];
    z[43]=z[2]*z[43];
    z[43]=z[43] + 2*z[40] - n<T>(13,4)*z[39];
    z[43]=z[2]*z[43];
    z[47]=npow(z[2],2);
    z[47]=n<T>(19,2)*z[42] - z[47];
    z[50]= - z[38] + z[2];
    z[50]=z[3]*z[50];
    z[51]=31*z[4] - 27*z[38];
    z[51]= - n<T>(13,6)*z[9] - z[3] + n<T>(1,2)*z[51] - z[2];
    z[51]=z[9]*z[51];
    z[47]=n<T>(1,4)*z[51] + n<T>(1,2)*z[47] + z[50];
    z[47]=z[9]*z[47];
    z[50]=5*z[9];
    z[51]=z[49] + z[50];
    z[51]=z[9]*z[51];
    z[42]=n<T>(19,3)*z[42] + z[51];
    z[51]=n<T>(1,3)*z[6];
    z[49]= - z[49] - n<T>(11,4)*z[6];
    z[49]=z[49]*z[51];
    z[42]=n<T>(1,2)*z[42] + z[49];
    z[48]=n<T>(7,3)*z[48] + z[50];
    z[48]=n<T>(1,4)*z[48] - n<T>(2,3)*z[6];
    z[48]=z[7]*z[48];
    z[42]=n<T>(1,2)*z[42] + z[48];
    z[42]=z[7]*z[42];
    z[48]= - n<T>(3,4)*z[3] - n<T>(1,4)*z[4] + z[2];
    z[48]=z[5]*z[48];
    z[39]=z[48] - z[40] + n<T>(1,2)*z[39];
    z[40]=2*z[4];
    z[48]= - n<T>(1,8)*z[2] - z[40] + n<T>(5,4)*z[38];
    z[48]=z[2]*z[48];
    z[45]= - n<T>(1,8)*z[3] + z[4] - z[45];
    z[45]=z[3]*z[45];
    z[39]=3*z[45] + z[48] + n<T>(1,2)*z[39];
    z[39]=z[5]*z[39];
    z[45]= - n<T>(179,6)*z[9] + n<T>(5,6)*z[3] + n<T>(31,9)*z[4] + 5*z[2];
    z[45]= - n<T>(17,24)*z[5] + n<T>(11,72)*z[7] + n<T>(1,8)*z[45] - z[51];
    z[45]=z[15]*z[45];
    z[38]= - n<T>(5,4)*z[2] - z[40] + n<T>(13,4)*z[38];
    z[38]=z[11]*z[38];
    z[40]= - z[2] + z[3];
    z[40]=z[25]*z[40];
    z[40]=z[21] + z[40] + z[36];
    z[48]=z[18] + z[35] - z[33];
    z[49]=11*z[9] + 21*z[6];
    z[49]=z[26]*z[49];
    z[49]=z[49] + z[29];
    z[50]= - z[22] - z[19] + n<T>(1,72)*z[16];
    z[50]=i*z[50];
    z[51]= - n<T>(19,4)*z[6] + 2*z[7];
    z[51]=z[27]*z[51];
    z[52]=n<T>(1,2)*i;
    z[52]= - z[20]*z[52];

    r +=  - n<T>(85,24)*z[17] + z[23] + n<T>(515,48)*z[24] - n<T>(3,2)*z[30] - n<T>(7,12)*z[31]
     - n<T>(5,4)*z[32]
     - n<T>(31,8)*z[34]
     + z[38]
     + z[39]
     + n<T>(3,4)*
      z[40] + n<T>(1,2)*z[41] + z[42] + z[43] + z[44] + z[45] + n<T>(1,6)*z[46]
       + z[47] + n<T>(1,8)*z[48] + n<T>(1,4)*z[49] + z[50] + n<T>(1,3)*z[51] + 
      z[52];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf293(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf293(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
