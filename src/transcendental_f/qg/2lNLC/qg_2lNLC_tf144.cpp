#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf144(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[74];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[11];
    z[11]=d[16];
    z[12]=d[5];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[17];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[0];
    z[33]=e[3];
    z[34]=e[10];
    z[35]=e[6];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[11];
    z[43]=f[12];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[18];
    z[47]=f[31];
    z[48]=f[39];
    z[49]=f[51];
    z[50]=f[55];
    z[51]=f[58];
    z[52]=f[59];
    z[53]=n<T>(1,4)*z[5];
    z[54]=n<T>(165,4)*z[9];
    z[55]= - n<T>(481,72)*z[5] - n<T>(251,9)*z[3] + z[54];
    z[55]=z[55]*z[53];
    z[56]=n<T>(3,8)*z[6];
    z[57]= - z[3] + z[2];
    z[57]=z[57]*z[56];
    z[58]=npow(z[15],2);
    z[58]= - z[58] + z[36] - n<T>(685,6)*z[34];
    z[59]=npow(z[12],2);
    z[60]=n<T>(1,12)*z[59];
    z[61]=npow(z[9],2);
    z[62]=npow(z[3],2);
    z[63]=n<T>(3,8)*z[2];
    z[64]= - z[3]*z[63];
    z[65]=n<T>(1,3)*z[15];
    z[66]=z[65] - n<T>(1,2)*z[4];
    z[66]=z[4]*z[66];
    z[67]=n<T>(13,2)*z[4] - 5*z[15] + n<T>(691,6)*z[3];
    z[67]= - n<T>(385,72)*z[5] + n<T>(1,12)*z[67] - 4*z[9];
    z[67]=z[7]*z[67];
    z[55]=z[67] + z[57] + z[55] - 4*z[61] + z[66] + z[64] + n<T>(707,288)*
    z[62] + n<T>(2425,864)*z[22] + z[60] - z[21] + n<T>(1,12)*z[58];
    z[55]=z[7]*z[55];
    z[57]=n<T>(9,2)*z[18];
    z[58]=npow(z[13],2);
    z[64]=n<T>(1,6)*z[58];
    z[66]=5*z[21];
    z[67]= - n<T>(1099,12)*z[19] - z[20] - z[66];
    z[68]= - z[8] + n<T>(1,2)*z[3];
    z[68]=z[3]*z[68];
    z[69]=z[14] - z[13];
    z[69]= - n<T>(5,6)*z[6] - n<T>(793,36)*z[5] + n<T>(5,3)*z[9] - n<T>(17,2)*z[4]
     + 
   n<T>(1207,36)*z[2] + n<T>(1,3)*z[69] - n<T>(3,2)*z[3];
    z[69]=z[6]*z[69];
    z[67]=z[69] + n<T>(299,36)*z[68] + z[64] + n<T>(1,6)*z[59] - n<T>(865,36)*z[16]
    + n<T>(481,18)*z[17] + n<T>(1,3)*z[67] + z[57];
    z[56]=n<T>(173,36)*z[7] - z[56] + n<T>(481,144)*z[5] - n<T>(165,16)*z[9] - z[63]
    + n<T>(139,18)*z[3] + z[10] - z[65];
    z[56]=z[7]*z[56];
    z[63]= - n<T>(5,3)*z[10] + n<T>(9,2)*z[11];
    z[65]=n<T>(1,4)*z[4];
    z[68]=z[65] + n<T>(21,2)*z[2] + z[63];
    z[68]=z[68]*z[65];
    z[69]=n<T>(1,2)*z[9];
    z[70]=z[11] - z[4];
    z[70]=3*z[70] - n<T>(113,48)*z[9];
    z[70]=z[70]*z[69];
    z[71]=n<T>(1,2)*z[5];
    z[72]=n<T>(865,4)*z[2] - n<T>(481,2)*z[8] - 251*z[3];
    z[72]=n<T>(165,8)*z[9] + n<T>(1,18)*z[72] - z[4];
    z[72]=z[72]*z[71];
    z[73]= - n<T>(85,9)*z[2] - n<T>(3,8)*z[3];
    z[73]=z[2]*z[73];
    z[56]=z[56] + z[72] + z[70] + z[68] + z[73] + n<T>(1,4)*z[67];
    z[56]=z[1]*z[56];
    z[67]=n<T>(491,4)*z[29] + 85*z[27];
    z[56]=z[56] - n<T>(325,864)*z[23] + n<T>(1,9)*z[67] + n<T>(3,4)*z[25];
    z[56]=i*z[56];
    z[67]=z[4] - z[2];
    z[67]= - n<T>(233,72)*z[9] - n<T>(1,3)*z[13] + 11*z[3] + n<T>(21,4)*z[67];
    z[67]=z[67]*z[69];
    z[68]=z[35] - n<T>(137,4)*z[33];
    z[60]=z[67] - n<T>(7,4)*z[62] + n<T>(2753,288)*z[22] + z[64] - z[60] + n<T>(1,6)
   *z[68] - 3*z[18];
    z[60]=z[60]*z[69];
    z[58]=z[58] - z[59];
    z[58]= - n<T>(91,72)*z[22] + z[20] + n<T>(185,3)*z[19] - n<T>(1,2)*z[58];
    z[64]=npow(z[4],2);
    z[67]=3*z[3] - n<T>(365,36)*z[2];
    z[67]=z[2]*z[67];
    z[68]=27*z[4];
    z[69]=n<T>(155,36)*z[5] + n<T>(331,18)*z[2] - z[68];
    z[69]=z[5]*z[69];
    z[58]=z[69] + n<T>(5,6)*z[61] + n<T>(71,8)*z[64] + n<T>(1,3)*z[58] + n<T>(1,2)*z[67]
   ;
    z[67]=n<T>(11,72)*z[6] + n<T>(793,288)*z[5] - n<T>(5,24)*z[9] - n<T>(47,9)*z[2]
     + n<T>(71,32)*z[4];
    z[67]=z[6]*z[67];
    z[58]=n<T>(1,4)*z[58] + z[67];
    z[58]=z[6]*z[58];
    z[67]=983*z[3] - 641*z[2];
    z[54]= - z[54] + n<T>(1,18)*z[67] + z[68];
    z[54]=z[54]*z[71];
    z[67]=npow(z[2],2);
    z[68]= - n<T>(245,8)*z[67] + n<T>(299,8)*z[62] - n<T>(791,24)*z[22] - n<T>(481,2)*
    z[17] + 155*z[16];
    z[54]=z[54] + n<T>(31,8)*z[61] + n<T>(1,9)*z[68] + z[64];
    z[53]=z[54]*z[53];
    z[54]=z[59] + n<T>(1,4)*z[32] - z[66];
    z[59]= - 21*z[2] - n<T>(185,18)*z[4];
    z[59]=z[59]*z[65];
    z[54]=z[59] + n<T>(115,48)*z[22] - z[57] - n<T>(1,3)*z[54];
    z[54]=z[54]*z[65];
    z[57]= - n<T>(23,3)*z[34] - z[33];
    z[57]=43*z[57] - n<T>(299,3)*z[17];
    z[57]= - n<T>(91,4)*z[62] + n<T>(1,12)*z[57] + 3*z[22];
    z[57]=z[3]*z[57];
    z[59]= - n<T>(1,12)*z[13] - n<T>(13,36)*z[15] - n<T>(481,18)*z[8] + n<T>(1,3)*z[14]
    + z[63];
    z[59]=z[22]*z[59];
    z[57]=z[39] + z[59] + z[57];
    z[59]=z[47] - z[48];
    z[61]=z[28] - z[41];
    z[62]=z[52] + n<T>(1,4)*z[51];
    z[63]=npow(z[15],3);
    z[64]=185*z[67] - n<T>(1865,18)*z[22] + n<T>(245,3)*z[16] + z[32] + n<T>(359,3)*
    z[19];
    z[64]=z[2]*z[64];

    r += n<T>(895,288)*z[24] + n<T>(907,432)*z[26] - n<T>(491,72)*z[30] - n<T>(10657,576)*z[31]
     - n<T>(21,8)*z[37]
     + n<T>(21,16)*z[38] - n<T>(385,72)*z[40]
     + n<T>(793,288)*z[42] - n<T>(31,32)*z[43] - n<T>(91,32)*z[44]
     + n<T>(17,16)*z[45] - n<T>(7,8)
      *z[46] - n<T>(1,12)*z[49] + n<T>(1,8)*z[50] + z[53] + z[54] + z[55] + 
      z[56] + n<T>(1,4)*z[57] + z[58] + n<T>(1,24)*z[59] + z[60] - n<T>(85,18)*
      z[61] + n<T>(1,3)*z[62] - n<T>(1,36)*z[63] + n<T>(1,48)*z[64];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf144(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf144(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
