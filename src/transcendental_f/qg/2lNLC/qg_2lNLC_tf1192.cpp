#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1192(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[20];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[7];
    z[5]=e[7];
    z[6]=c[3];
    z[7]=c[31];
    z[8]=d[6];
    z[9]=f[31];
    z[10]=f[33];
    z[11]=f[36];
    z[12]=f[39];
    z[13]=n<T>(1,2)*z[4];
    z[14]=n<T>(1,2)*z[2];
    z[15]=z[1]*i;
    z[16]=z[3] + z[14] + z[13] - z[8] - n<T>(1,2)*z[15];
    z[16]=z[3]*z[16];
    z[13]=z[13] - z[15];
    z[13]=z[13]*z[4];
    z[15]=z[15] - z[4];
    z[17]= - z[14] + z[15];
    z[17]=z[2]*z[17];
    z[18]=2*z[5];
    z[19]=npow(z[8],2);
    z[16]=z[16] + z[17] - z[13] + z[18] + n<T>(2,3)*z[6] - z[19];
    z[16]=z[3]*z[16];
    z[14]= - z[15]*z[14];
    z[13]=z[14] + z[13] - n<T>(1,2)*z[6] + z[5];
    z[13]=z[2]*z[13];
    z[14]= - z[9] - z[12] - z[11];
    z[15]= - z[5]*z[15];
    z[17]=npow(z[8],3);
    z[18]=z[8]*z[18];

    r += 7*z[7] - 4*z[10] + z[13] + n<T>(1,2)*z[14] + z[15] + z[16] + z[17]
       + z[18];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1192(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1192(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
