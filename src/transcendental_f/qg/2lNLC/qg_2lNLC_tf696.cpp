#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf696(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[44];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[19];
    z[18]=c[23];
    z[19]=c[25];
    z[20]=c[26];
    z[21]=c[28];
    z[22]=c[31];
    z[23]=e[10];
    z[24]=f[3];
    z[25]=f[4];
    z[26]=f[5];
    z[27]=f[11];
    z[28]=f[12];
    z[29]=f[16];
    z[30]=f[17];
    z[31]=n<T>(1,2)*z[2];
    z[32]=z[1]*i;
    z[33]= - z[32] + z[31];
    z[33]=z[2]*z[33];
    z[34]=n<T>(1,2)*z[3];
    z[35]= - n<T>(3,2)*z[2] + n<T>(1,2)*z[32] + z[34] - z[5];
    z[35]=z[3]*z[35];
    z[36]=z[32]*z[5];
    z[37]=npow(z[5],2);
    z[33]=z[35] + 3*z[33] + z[37] - z[36];
    z[33]=z[3]*z[33];
    z[35]= - z[36] + n<T>(1,2)*z[37];
    z[38]=n<T>(1,2)*z[35];
    z[39]=z[32] - z[5];
    z[34]=z[34] + z[39];
    z[34]=z[3]*z[34];
    z[40]=n<T>(1,4)*z[39] + z[3];
    z[40]=z[7]*z[40];
    z[34]=z[40] - z[38] + z[34];
    z[34]=z[7]*z[34];
    z[40]=2*z[32];
    z[31]= - z[31] + z[5] + z[40];
    z[31]=z[2]*z[31];
    z[41]=n<T>(1,4)*z[5] - z[2];
    z[41]=z[6]*z[41];
    z[31]=z[41] + z[38] + z[31];
    z[31]=z[6]*z[31];
    z[31]=z[31] + z[33] + z[34];
    z[33]=561*z[32];
    z[34]=n<T>(561,2)*z[2] - 184*z[5] - z[33];
    z[34]=z[2]*z[34];
    z[34]=z[34] - 193*z[37] + 561*z[36];
    z[34]=z[2]*z[34];
    z[38]=z[9] - z[3];
    z[38]= - n<T>(175,12)*z[4] - n<T>(193,12)*z[6] + n<T>(193,3)*z[7] - n<T>(193,6)*z[5]
    + 98*z[2] + n<T>(193,2)*z[38];
    z[38]=z[14]*z[38];
    z[31]=z[38] + z[34] + 193*z[31];
    z[34]=2*z[3];
    z[38]=z[34] - z[5];
    z[38]=z[32]*z[38];
    z[38]= - z[14] + z[38];
    z[38]=z[8]*z[38];
    z[34]=z[34] + z[39];
    z[34]=z[12]*z[34];
    z[41]=z[40] - z[2];
    z[42]=z[6] - z[41];
    z[42]=z[13]*z[42];
    z[43]= - z[3] - z[7];
    z[43]=z[23]*z[43];
    z[34]=z[38] + z[34] + z[42] + z[43];
    z[36]=n<T>(579,2)*z[37] - 929*z[36];
    z[37]=z[2]*z[40];
    z[32]=n<T>(1,2)*z[6] - z[5] - z[32];
    z[32]=z[6]*z[32];
    z[38]=n<T>(579,1508)*z[6] + n<T>(929,1508)*z[5] - z[2];
    z[38]=z[4]*z[38];
    z[32]=z[38] + n<T>(579,754)*z[32] + n<T>(1,754)*z[36] + z[37];
    z[32]=z[4]*z[32];
    z[36]= - n<T>(1,2)*z[7] - z[39];
    z[36]=z[7]*z[36];
    z[37]= - z[7] - z[39];
    z[37]=z[9]*z[37];
    z[35]=n<T>(1,2)*z[37] + z[36] - z[35];
    z[35]=z[9]*z[35];
    z[36]= - z[28] + z[30] - z[29];
    z[37]=z[26] - z[19];
    z[38]=z[27] - z[25];
    z[39]=n<T>(1701,377)*z[20] + n<T>(1701,754)*z[18] + n<T>(193,2262)*z[15];
    z[39]=i*z[39];
    z[33]= - 9*z[2] + 193*z[5] - z[33];
    z[33]=n<T>(1,377)*z[33] + z[4];
    z[33]=z[11]*z[33];
    z[40]=z[4] - z[41];
    z[40]=z[10]*z[40];

    r +=  - n<T>(193,754)*z[16] + n<T>(567,3016)*z[17] - n<T>(1701,754)*z[21] - 
      n<T>(1665,1508)*z[22] + n<T>(929,1508)*z[24] + n<T>(1,377)*z[31] + z[32] + 
      z[33] + n<T>(193,377)*z[34] + n<T>(579,754)*z[35] + n<T>(579,1508)*z[36] + 
      n<T>(1701,1508)*z[37] + n<T>(193,1508)*z[38] + z[39] + z[40];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf696(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf696(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
