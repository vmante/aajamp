#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf138(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[63];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[16];
    z[11]=d[6];
    z[12]=d[5];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=d[9];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[21];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[0];
    z[31]=e[3];
    z[32]=e[10];
    z[33]=e[6];
    z[34]=e[7];
    z[35]=f[0];
    z[36]=f[1];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[11];
    z[41]=f[12];
    z[42]=f[16];
    z[43]=f[17];
    z[44]=f[18];
    z[45]=f[31];
    z[46]=f[39];
    z[47]=npow(z[12],2);
    z[47]=n<T>(1,2)*z[47];
    z[48]=npow(z[11],2);
    z[48]= - z[18] - z[47] + n<T>(1,2)*z[48];
    z[49]=n<T>(1,8)*z[4];
    z[50]=31*z[10] - n<T>(15,2)*z[4];
    z[50]=z[50]*z[49];
    z[51]=n<T>(1,4)*z[2];
    z[52]=z[3] + 63*z[4] + z[7];
    z[52]=n<T>(1,2)*z[52] - n<T>(613,9)*z[2];
    z[52]=z[52]*z[51];
    z[53]=z[4] - z[10];
    z[54]=n<T>(1045,48)*z[7];
    z[53]= - n<T>(65,32)*z[9] - z[54] - z[11] - 2*z[53];
    z[53]=z[9]*z[53];
    z[55]=n<T>(1045,72)*z[15];
    z[56]=npow(z[7],2);
    z[57]=n<T>(209,32)*z[3] - n<T>(209,16)*z[8] + 125*z[7];
    z[57]=z[3]*z[57];
    z[58]=n<T>(1063,36)*z[7] - n<T>(1045,18)*z[8] - 21*z[4];
    z[58]=n<T>(1093,24)*z[9] + n<T>(1759,72)*z[2] + n<T>(1,2)*z[58] - n<T>(259,9)*z[3];
    z[58]=z[5]*z[58];
    z[59]=z[3] - z[7];
    z[60]=n<T>(19,8)*z[6] - n<T>(1183,144)*z[5] - n<T>(11,4)*z[9] + n<T>(1129,144)*z[2]
    + z[13] - n<T>(21,8)*z[4] + n<T>(3,8)*z[59];
    z[60]=z[6]*z[60];
    z[50]=z[60] + n<T>(1,2)*z[58] + z[53] + z[52] + n<T>(1,9)*z[57] + n<T>(847,72)*
    z[56] + z[50] - n<T>(1939,144)*z[14] + z[55] + n<T>(31,8)*z[16] - n<T>(1561,144)
   *z[17] + z[48];
    z[50]=z[1]*z[50];
    z[52]=839*z[27] + 649*z[25];
    z[50]=z[50] - n<T>(667,864)*z[21] + n<T>(1,36)*z[52] + z[24];
    z[50]=i*z[50];
    z[52]=n<T>(1,18)*z[3];
    z[53]= - 259*z[7] + n<T>(209,16)*z[3];
    z[53]=z[53]*z[52];
    z[57]= - z[7] - n<T>(659,72)*z[2];
    z[51]=z[57]*z[51];
    z[57]=npow(z[4],2);
    z[54]=n<T>(125,32)*z[9] + z[54] - z[2];
    z[54]=z[9]*z[54];
    z[58]= - n<T>(1045,16)*z[9] - n<T>(1505,24)*z[2] + n<T>(2099,24)*z[3] + 49*z[4]
    - n<T>(1063,48)*z[7];
    z[58]=z[5]*z[58];
    z[51]=n<T>(1,6)*z[58] + z[54] + z[51] + z[53] - n<T>(205,18)*z[56] + n<T>(21,8)*
    z[57] - n<T>(2009,864)*z[19] + n<T>(187,18)*z[14] + z[33] - z[55];
    z[51]=z[5]*z[51];
    z[53]=z[3]*z[7];
    z[54]=z[53] - z[56];
    z[55]=3*z[11];
    z[58]=n<T>(1,8)*z[3];
    z[60]= - n<T>(119,288)*z[2] - z[58] - z[55] + n<T>(1,8)*z[7];
    z[60]=z[2]*z[60];
    z[61]=z[11] - n<T>(15,8)*z[9];
    z[61]=z[9]*z[61];
    z[59]=n<T>(329,144)*z[5] + n<T>(955,72)*z[2] - n<T>(49,3)*z[4] - n<T>(1,4)*z[59];
    z[59]=z[5]*z[59];
    z[62]= - n<T>(1,72)*z[6] + n<T>(967,288)*z[5] + n<T>(11,8)*z[9] - n<T>(365,36)*z[2]
    + z[11] + n<T>(427,96)*z[4];
    z[62]=z[6]*z[62];
    z[48]=z[62] + z[59] + z[61] + z[60] + n<T>(161,32)*z[57] + n<T>(1511,864)*
    z[19] + n<T>(347,36)*z[17] - z[33] - z[48] + n<T>(3,8)*z[54];
    z[48]=z[6]*z[48];
    z[54]=npow(z[3],2);
    z[59]=npow(z[2],2);
    z[60]= - z[11] + n<T>(47,8)*z[4];
    z[60]= - n<T>(671,288)*z[9] - n<T>(47,16)*z[2] + n<T>(23,4)*z[3] + n<T>(1,2)*z[60]
    - n<T>(23,3)*z[7];
    z[60]=z[9]*z[60];
    z[54]=z[60] + n<T>(1,2)*z[59] - n<T>(35,8)*z[54] - 8*z[56] + n<T>(4963,576)*
    z[19] - 2*z[16] - n<T>(69,16)*z[31] + z[47] - z[33];
    z[54]=z[9]*z[54];
    z[59]=n<T>(965,2)*z[7] - 1211*z[3];
    z[52]=z[59]*z[52];
    z[59]=n<T>(541,9)*z[19] - n<T>(209,9)*z[15] - n<T>(1811,9)*z[32] + 25*z[31];
    z[52]=z[52] + n<T>(1,2)*z[59] + n<T>(1423,9)*z[56];
    z[52]=z[52]*z[58];
    z[56]= - 63*z[57] - n<T>(5453,54)*z[19] + n<T>(443,9)*z[14] - 15*z[30] + n<T>(173,9)*z[17];
    z[53]=n<T>(1,2)*z[56] + z[53];
    z[55]=z[55] + n<T>(947,72)*z[2];
    z[55]=z[2]*z[55];
    z[53]=n<T>(1,4)*z[53] + z[55];
    z[53]=z[2]*z[53];
    z[53]=z[53] + n<T>(1309,144)*z[22] + n<T>(977,108)*z[23] - n<T>(685,36)*z[26]
     - 
   n<T>(875,36)*z[28] - n<T>(13579,288)*z[29] - n<T>(55,4)*z[35] + n<T>(47,8)*z[36]
     + 
   n<T>(21,4)*z[37] - n<T>(205,9)*z[38] + n<T>(721,36)*z[39] + n<T>(1183,144)*z[40]
     - 
   n<T>(109,16)*z[41] - n<T>(153,16)*z[42] + n<T>(21,8)*z[43] - n<T>(55,12)*z[44]
     + 
    z[46] + z[45];
    z[47]=z[47] + z[34];
    z[55]=z[12] + n<T>(1,3)*z[11];
    z[55]=z[11]*z[55];
    z[55]=n<T>(1,2)*z[55] - n<T>(43,48)*z[19] - z[47];
    z[55]=z[11]*z[55];
    z[56]= - n<T>(71,4)*z[57] - n<T>(221,72)*z[19] + n<T>(15,2)*z[30] - 31*z[16];
    z[49]=z[56]*z[49];
    z[47]= - z[12]*z[47];
    z[56]= - n<T>(1,3)*z[12] - n<T>(1045,72)*z[8] + n<T>(31,8)*z[10] - n<T>(1,48)*z[20]
    + z[13];
    z[56]=z[19]*z[56];
    z[57]= - 1423*z[32] + n<T>(4411,12)*z[19];
    z[57]=z[7]*z[57];

    r += z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + n<T>(1,2)*z[53] + 
      z[54] + z[55] + z[56] + n<T>(1,72)*z[57];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf138(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf138(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
