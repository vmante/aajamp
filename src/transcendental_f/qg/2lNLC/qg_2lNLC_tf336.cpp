#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf336(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[67];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[11];
    z[11]=d[16];
    z[12]=d[5];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[0];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=e[13];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[11];
    z[43]=f[12];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[18];
    z[47]=f[31];
    z[48]=f[39];
    z[49]=f[51];
    z[50]=f[55];
    z[51]=f[58];
    z[52]=n<T>(1,2)*z[9];
    z[53]=n<T>(27,4)*z[4];
    z[54]=n<T>(39,8)*z[9] - z[53] - 5*z[13] - 23*z[3];
    z[54]=z[54]*z[52];
    z[55]=npow(z[3],2);
    z[56]=z[55] - z[18];
    z[57]=npow(z[13],2);
    z[57]=n<T>(5,2)*z[57];
    z[58]=npow(z[12],2);
    z[59]=5*z[34] + n<T>(109,4)*z[32];
    z[54]=z[54] - n<T>(2245,96)*z[22] - n<T>(5,4)*z[58] + z[57] + n<T>(1,2)*z[59]
     - 
   3*z[56];
    z[52]=z[54]*z[52];
    z[54]= - z[57] + 5*z[20];
    z[56]=npow(z[4],2);
    z[57]=npow(z[9],2);
    z[59]= - n<T>(43,4)*z[5] + z[7] - n<T>(99,2)*z[2] - z[3] + 75*z[4];
    z[59]=z[5]*z[59];
    z[59]=z[59] - n<T>(17,2)*z[57] - n<T>(229,8)*z[56] + n<T>(479,24)*z[22] + n<T>(5,2)*
    z[58] - 77*z[19] + z[54];
    z[60]=n<T>(7,8)*z[3];
    z[61]=n<T>(243,32)*z[2] - 2*z[13] - z[60];
    z[61]=z[2]*z[61];
    z[62]=3*z[3] + z[2];
    z[62]=3*z[62] - 7*z[7];
    z[62]=z[7]*z[62];
    z[63]= - n<T>(1,12)*z[6] - n<T>(435,32)*z[5] + 18*z[2] + n<T>(17,8)*z[9] + z[13]
    - n<T>(233,32)*z[4];
    z[63]=z[6]*z[63];
    z[59]=z[63] + n<T>(1,8)*z[62] + z[61] + n<T>(1,4)*z[59];
    z[59]=z[6]*z[59];
    z[61]= - z[21] + n<T>(15,2)*z[18];
    z[62]=n<T>(1,4)*z[4];
    z[63]=z[62] - n<T>(15,2)*z[11] + z[10];
    z[63]=z[4]*z[63];
    z[64]=7*z[3] - 27*z[4];
    z[64]=n<T>(1,2)*z[64] + 129*z[2];
    z[64]=z[2]*z[64];
    z[54]=z[64] + z[63] + n<T>(311,4)*z[16] - n<T>(283,6)*z[17] + n<T>(581,4)*z[19]
    - z[54] - z[61];
    z[63]=z[10] - z[15];
    z[63]=z[15]*z[63];
    z[64]=z[8] - n<T>(1,2)*z[3];
    z[64]=z[3]*z[64];
    z[65]= - n<T>(5,16)*z[9] - z[11] + z[4];
    z[65]=z[9]*z[65];
    z[54]=3*z[65] + n<T>(383,24)*z[64] + z[63] - z[58] + n<T>(1,2)*z[54];
    z[63]=n<T>(311,16)*z[9];
    z[64]=n<T>(283,8)*z[8] + 41*z[3];
    z[53]= - n<T>(277,48)*z[7] - n<T>(315,16)*z[2] - z[63] + n<T>(1,3)*z[64] - z[53]
   ;
    z[53]=z[5]*z[53];
    z[64]= - n<T>(20,3)*z[7] - n<T>(1,8)*z[2] + z[63] + n<T>(1,2)*z[15] - n<T>(47,3)*
    z[3];
    z[64]=z[7]*z[64];
    z[65]=z[14] - z[13];
    z[65]=n<T>(25,2)*z[6] + n<T>(419,4)*z[5] - n<T>(5,2)*z[7] - n<T>(621,4)*z[2]
     - 17*
    z[9] + n<T>(81,2)*z[4] + 5*z[65] + n<T>(9,2)*z[3];
    z[65]=z[6]*z[65];
    z[53]=n<T>(1,4)*z[65] + z[53] + n<T>(1,2)*z[54] + z[64];
    z[53]=z[1]*z[53];
    z[54]=n<T>(109,72)*z[23] - 223*z[28] - 125*z[26];
    z[53]=n<T>(1,4)*z[54] + z[53];
    z[53]=i*z[53];
    z[54]=z[47] - z[48];
    z[64]=z[37] + z[39];
    z[65]=z[27] - z[41];
    z[66]=z[14] - n<T>(3,2)*z[11];
    z[66]= - n<T>(23,4)*z[13] + n<T>(283,6)*z[8] + 5*z[66] + z[10];
    z[66]=n<T>(1,2)*z[66] + n<T>(1,3)*z[12];
    z[66]=z[22]*z[66];
    z[54]=z[66] - n<T>(883,48)*z[24] - n<T>(103,12)*z[25] + n<T>(223,4)*z[29] + n<T>(2807,32)*z[30]
     - n<T>(27,8)*z[38]
     + n<T>(175,12)*z[40] - n<T>(419,16)*z[42] - n<T>(83,16)
   *z[43] + n<T>(149,16)*z[44] - n<T>(81,8)*z[45] + n<T>(9,4)*z[46] - n<T>(1,4)*z[49]
    - n<T>(3,4)*z[51] - z[50] + n<T>(125,4)*z[65] + n<T>(27,4)*z[64] + n<T>(5,4)*z[54];
    z[64]= - n<T>(503,24)*z[55] - n<T>(1279,72)*z[22] - n<T>(3,2)*z[58] - z[36] + 
   n<T>(343,6)*z[33];
    z[65]=z[4]*z[15];
    z[66]=z[4] + z[15] - n<T>(349,6)*z[3];
    z[66]=n<T>(1,2)*z[66] + 15*z[9];
    z[66]=z[7]*z[66];
    z[64]=z[66] + n<T>(55,4)*z[57] + n<T>(1,2)*z[64] + z[65];
    z[60]= - z[15] + z[60];
    z[60]=z[2]*z[60];
    z[60]=z[60] + n<T>(1,2)*z[64];
    z[60]=z[7]*z[60];
    z[64]=npow(z[2],2);
    z[64]=n<T>(119,16)*z[64] + n<T>(83,16)*z[57] + n<T>(29,4)*z[56] - n<T>(383,48)*z[55]
    + n<T>(559,144)*z[22] + n<T>(283,12)*z[17] - 23*z[16];
    z[63]=n<T>(175,24)*z[7] - n<T>(1,4)*z[2] + n<T>(41,3)*z[3] - z[63];
    z[63]=z[7]*z[63];
    z[65]= - n<T>(55,6)*z[3] - 7*z[4];
    z[65]=n<T>(277,12)*z[7] + n<T>(197,2)*z[2] + 11*z[65] + n<T>(311,4)*z[9];
    z[65]=z[5]*z[65];
    z[63]=n<T>(1,8)*z[65] + n<T>(1,2)*z[64] + z[63];
    z[63]=z[5]*z[63];
    z[64]=n<T>(1,4)*z[58];
    z[65]=z[35] - z[21];
    z[66]= - z[12] - n<T>(1,3)*z[15];
    z[66]=z[15]*z[66];
    z[65]=n<T>(1,4)*z[66] + n<T>(1,48)*z[22] + z[64] + n<T>(1,2)*z[65] + z[16];
    z[65]=z[15]*z[65];
    z[66]= - z[15] + n<T>(109,24)*z[4];
    z[66]=z[4]*z[66];
    z[58]=z[66] - n<T>(119,16)*z[22] - n<T>(1,2)*z[58] - n<T>(11,4)*z[31] + z[61];
    z[58]=z[58]*z[62];
    z[56]=z[56] + z[57];
    z[57]=n<T>(403,6)*z[22] - 127*z[16] + 11*z[31] - 273*z[19];
    z[61]=npow(z[15],2);
    z[56]=n<T>(1,8)*z[57] + z[61] + n<T>(27,8)*z[56];
    z[57]=z[13] - n<T>(697,48)*z[2];
    z[57]=z[2]*z[57];
    z[56]=n<T>(1,2)*z[56] + z[57];
    z[56]=z[2]*z[56];
    z[57]=n<T>(383,3)*z[17] + n<T>(461,3)*z[33] + 123*z[32];
    z[55]=n<T>(637,12)*z[55] + n<T>(1,4)*z[57] + z[22];
    z[55]=z[3]*z[55];
    z[57]=z[64] + n<T>(1,2)*z[35] - z[16];
    z[57]=z[12]*z[57];

    r += z[52] + z[53] + n<T>(1,2)*z[54] + n<T>(1,4)*z[55] + z[56] + z[57] + 
      z[58] + z[59] + z[60] + z[63] + z[65];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf336(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf336(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
