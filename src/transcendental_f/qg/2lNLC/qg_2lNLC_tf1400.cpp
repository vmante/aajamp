#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1400(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[50];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[5];
    z[10]=d[11];
    z[11]=d[9];
    z[12]=e[2];
    z[13]=e[12];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=e[3];
    z[17]=e[10];
    z[18]=e[13];
    z[19]=e[14];
    z[20]=f[2];
    z[21]=f[4];
    z[22]=f[6];
    z[23]=f[12];
    z[24]=f[16];
    z[25]=f[51];
    z[26]=f[54];
    z[27]=f[55];
    z[28]=f[56];
    z[29]=f[58];
    z[30]=npow(z[6],2);
    z[31]=n<T>(1,2)*z[30];
    z[32]=z[1]*i;
    z[33]=z[32]*z[6];
    z[34]=z[31] + z[33];
    z[35]=n<T>(3,2)*z[34];
    z[36]=n<T>(1,2)*z[2];
    z[37]= - z[32] - z[36];
    z[37]=z[2]*z[37];
    z[38]=3*z[6];
    z[39]=z[38] + z[32];
    z[39]=n<T>(1,2)*z[39];
    z[40]=n<T>(1,4)*z[3];
    z[41]=z[40] - z[39] + z[2];
    z[41]=z[3]*z[41];
    z[38]=z[38] - z[32];
    z[42]=z[40] + n<T>(1,4)*z[38] - z[2];
    z[42]=z[4]*z[42];
    z[37]=z[42] + z[41] + z[35] + z[37];
    z[37]=z[4]*z[37];
    z[41]=z[9] + z[11];
    z[42]=n<T>(1,2)*z[8];
    z[38]= - z[42] - n<T>(1,2)*z[38] + z[41];
    z[38]=z[9]*z[38];
    z[43]=n<T>(1,4)*z[8];
    z[39]= - z[43] + z[39] - z[11];
    z[39]=z[8]*z[39];
    z[44]=z[11]*z[32];
    z[35]=n<T>(1,2)*z[38] + z[39] - z[35] + z[44];
    z[35]=z[9]*z[35];
    z[38]=2*z[2];
    z[39]= - n<T>(1,2)*z[6] + z[38];
    z[39]=z[2]*z[39];
    z[30]=z[39] - n<T>(3,2)*z[30] - z[33];
    z[30]=z[2]*z[30];
    z[39]=n<T>(1,2)*z[11];
    z[44]=z[32] + z[6];
    z[45]= - z[44]*z[39];
    z[34]=z[45] + z[34];
    z[34]=z[11]*z[34];
    z[31]=z[31] - z[33];
    z[33]=z[36] - z[32];
    z[33]= - z[2]*z[33];
    z[45]=n<T>(3,4)*z[6] - z[2];
    z[45]=z[3]*z[45];
    z[33]=z[45] + n<T>(3,2)*z[31] + z[33];
    z[33]=z[3]*z[33];
    z[45]=z[8] + z[11];
    z[46]=z[45] - z[32];
    z[47]= - n<T>(3,2)*z[6] + z[46];
    z[47]=z[8]*z[47];
    z[31]= - 3*z[31] + z[47];
    z[31]=z[31]*z[42];
    z[36]= - z[36] + z[44];
    z[36]=z[2]*z[36];
    z[39]=z[39] - z[44];
    z[39]=z[11]*z[39];
    z[42]= - z[2] + z[11];
    z[42]=z[5]*z[42];
    z[36]=n<T>(1,2)*z[42] + z[36] + z[39];
    z[36]=z[5]*z[36];
    z[39]=z[6] + z[2];
    z[39]=z[17]*z[39];
    z[42]=z[6] + z[11];
    z[42]=z[19]*z[42];
    z[39]=z[39] + z[42];
    z[38]=z[9] - z[4] + z[40] + z[38] - n<T>(5,2)*z[11];
    z[38]= - z[43] + n<T>(1,3)*z[38];
    z[38]=z[14]*z[38];
    z[40]=z[3] + z[2];
    z[42]=z[32]*z[40];
    z[42]=z[14] + z[42];
    z[42]=z[7]*z[42];
    z[43]= - z[32]*z[45];
    z[43]= - z[14] + z[43];
    z[43]=z[10]*z[43];
    z[32]= - z[32] + z[40];
    z[32]=z[12]*z[32];
    z[40]=z[13]*z[46];
    z[44]= - z[29] + z[27] + z[24] + z[21];
    z[45]=z[25] + z[23];
    z[46]=z[28] + z[22];
    z[47]=npow(z[6],3);
    z[48]=z[2] + z[4];
    z[48]=z[16]*z[48];
    z[41]=z[18]*z[41];
    z[49]=z[15]*i;

    r += z[20] + z[26] + z[30] + z[31] + z[32] + z[33] + z[34] + z[35]
       + z[36] + z[37] + z[38] + 2*z[39] + z[40] + z[41] + z[42] + 
      z[43] + n<T>(3,4)*z[44] - n<T>(1,4)*z[45] + n<T>(1,2)*z[46] + z[47] + z[48]
       + n<T>(1,6)*z[49];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1400(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1400(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
