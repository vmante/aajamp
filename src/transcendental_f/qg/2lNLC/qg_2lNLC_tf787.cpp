#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf787(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[66];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[9];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[12];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[5];
    z[17]=e[8];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[3];
    z[28]=e[10];
    z[29]=e[13];
    z[30]=e[6];
    z[31]=e[14];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[17];
    z[39]=f[31];
    z[40]=f[36];
    z[41]=f[39];
    z[42]=f[51];
    z[43]=f[52];
    z[44]=f[55];
    z[45]=f[58];
    z[46]=npow(z[6],2);
    z[47]=npow(z[2],2);
    z[48]=npow(z[4],2);
    z[49]=npow(z[11],2);
    z[50]=n<T>(64943,2)*z[28] - n<T>(48871,3)*z[18];
    z[51]=n<T>(46847,18096)*z[3];
    z[52]= - z[2] - z[51];
    z[52]=z[3]*z[52];
    z[53]=n<T>(5,8)*z[7] - n<T>(9,4)*z[2] + z[3];
    z[53]=z[7]*z[53];
    z[50]=z[53] + z[52] + n<T>(17,8)*z[47] + n<T>(1,2)*z[46] + n<T>(23473,12064)*
    z[49] + n<T>(1,4524)*z[50] - z[48];
    z[52]=n<T>(1,2)*z[2];
    z[53]=z[9] + n<T>(41569,12064)*z[11];
    z[54]=n<T>(3,2)*z[4] + z[53];
    z[54]=n<T>(1,12)*z[8] - n<T>(1,4)*z[7] - n<T>(55895,18096)*z[3] - z[52] + n<T>(1,2)*
    z[54] - z[6];
    z[54]=z[8]*z[54];
    z[50]=n<T>(1,2)*z[50] + z[54];
    z[50]=z[8]*z[50];
    z[54]=z[12] + n<T>(40061,9048)*z[10];
    z[55]=n<T>(75433,12064)*z[7];
    z[56]=n<T>(1,2)*z[6];
    z[57]= - n<T>(67205,36192)*z[8] + z[55] + n<T>(64943,18096)*z[3] - n<T>(55763,6032)*z[2]
     - z[56]
     + n<T>(42125,12064)*z[4]
     + n<T>(1,2)*z[54] - z[53];
    z[57]=z[5]*z[57];
    z[58]= - z[16] + n<T>(40061,9048)*z[15];
    z[59]=n<T>(47601,12064)*z[49] + n<T>(29,2)*z[13] + n<T>(58779,3016)*z[14] + 
   n<T>(30193,1508)*z[17] - z[58];
    z[51]= - z[53] + z[51] + n<T>(153161,72384)*z[8];
    z[60]= - n<T>(3,2)*z[2] + z[4] - z[51];
    z[60]=z[8]*z[60];
    z[61]= - n<T>(29,16)*z[6] - n<T>(5,4)*z[11] + n<T>(1,2)*z[12] - z[4];
    z[61]=z[6]*z[61];
    z[62]=n<T>(104019,6032)*z[2] - n<T>(29,4)*z[4] + z[6];
    z[62]=z[2]*z[62];
    z[63]=n<T>(50419,36192)*z[3] - n<T>(41371,6032)*z[2] + n<T>(45895,9048)*z[10]
     - 
    z[9];
    z[63]=z[3]*z[63];
    z[64]=5*z[11];
    z[65]=n<T>(6477,1508)*z[4] - z[64];
    z[65]=z[7] - z[3] - n<T>(16039,1508)*z[2] + n<T>(7,8)*z[65] + 4*z[6];
    z[65]=z[7]*z[65];
    z[57]=z[57] + z[60] + z[65] + z[63] + z[62] + n<T>(1,2)*z[59] + z[61];
    z[57]=z[1]*z[57];
    z[59]= - z[24] - n<T>(1,2)*z[22];
    z[59]=162897*z[59] + n<T>(215545,18)*z[19];
    z[57]=n<T>(1,6032)*z[59] + z[57];
    z[57]=i*z[57];
    z[59]=5*z[27];
    z[60]=n<T>(11,4)*z[49] - n<T>(107921,18096)*z[18] + n<T>(11,2)*z[30] + z[59];
    z[60]=z[11]*z[60];
    z[61]= - z[41] + z[39] + z[40];
    z[62]=z[23] - z[34];
    z[63]= - z[42] - 3*z[45] - z[44];
    z[54]=z[18]*z[54];
    z[54]=z[60] + z[54] - n<T>(149947,18096)*z[20] - n<T>(54299,24128)*z[21] + 
   n<T>(162897,6032)*z[25] + n<T>(268947,12064)*z[26] - n<T>(42125,12064)*z[32] + 
   n<T>(153161,36192)*z[33] - n<T>(75433,12064)*z[35] - n<T>(36847,12064)*z[36] + 
   n<T>(23473,12064)*z[37] - n<T>(45339,12064)*z[38] - z[43] + n<T>(1,2)*z[63] + 
   n<T>(162897,12064)*z[62] + n<T>(11,8)*z[61];
    z[60]=n<T>(1,2)*z[3];
    z[61]=n<T>(66319,6032)*z[3] + z[6] - n<T>(38355,6032)*z[2];
    z[61]=z[61]*z[60];
    z[59]=n<T>(46847,9048)*z[28] + z[59];
    z[62]= - z[6]*z[9];
    z[59]=z[61] + n<T>(38355,12064)*z[47] + z[62] - n<T>(5,2)*z[49] - n<T>(14293,12064)*z[18]
     + n<T>(45895,9048)*z[15]
     + n<T>(1,2)*z[59]
     + z[29];
    z[59]=z[3]*z[59];
    z[61]=z[64] + n<T>(33,4)*z[6];
    z[61]=z[61]*z[56];
    z[62]= - n<T>(45339,4)*z[48] - 34717*z[17] + n<T>(6043,4)*z[18];
    z[63]=z[2] - n<T>(3,2)*z[3];
    z[63]=z[3]*z[63];
    z[61]=z[63] + n<T>(25669,6032)*z[47] + z[61] + n<T>(1,3016)*z[62] - 7*z[49];
    z[62]= - n<T>(14195,1508)*z[4] + 7*z[11];
    z[60]= - n<T>(1,16)*z[7] + z[60] + n<T>(31701,6032)*z[2] + n<T>(5,16)*z[62] - 2*
    z[6];
    z[60]=z[7]*z[60];
    z[60]=n<T>(1,2)*z[61] + z[60];
    z[60]=z[7]*z[60];
    z[53]= - n<T>(1,3)*z[5] + n<T>(31013,36192)*z[8] - n<T>(21145,12064)*z[7] - 
   n<T>(23989,4524)*z[3] + n<T>(11515,1508)*z[2] - n<T>(70975,12064)*z[4] + z[53];
    z[53]=z[5]*z[53];
    z[47]=z[53] + n<T>(17309,3016)*z[47] + n<T>(36847,12064)*z[49] - n<T>(42125,12064)*z[48]
     + n<T>(3517,3393)*z[18] - n<T>(24161,3016)*z[14]
     + z[58];
    z[53]=n<T>(1,2)*z[7];
    z[51]=z[53] + z[52] + z[51];
    z[51]=z[8]*z[51];
    z[58]=n<T>(14195,2)*z[4] - 4983*z[2];
    z[55]= - z[55] + n<T>(5,3016)*z[58] - z[3];
    z[53]=z[55]*z[53];
    z[55]=z[9] - n<T>(45895,18096)*z[3];
    z[55]=z[3]*z[55];
    z[47]=z[51] + z[53] + z[55] + n<T>(1,2)*z[47];
    z[47]=z[5]*z[47];
    z[51]= - z[4] - n<T>(11,4)*z[11];
    z[51]=n<T>(3,2)*z[51] + z[6];
    z[51]=z[51]*z[56];
    z[53]=z[48] - z[16];
    z[49]=z[51] + n<T>(3,4)*z[49] + n<T>(17,8)*z[18] + z[29] + n<T>(1,2)*z[53];
    z[49]=z[6]*z[49];
    z[51]=29*z[13];
    z[53]= - n<T>(1351,2)*z[17] - 911*z[14];
    z[48]=29*z[48] + n<T>(31613,1508)*z[18] + n<T>(19,377)*z[53] - z[51];
    z[53]= - z[6] - n<T>(268325,18096)*z[2];
    z[53]=z[2]*z[53];
    z[46]=z[53] + n<T>(1,4)*z[48] + z[46];
    z[46]=z[46]*z[52];
    z[48]= - n<T>(1,6)*z[18] + z[31] + z[29];
    z[48]=z[9]*z[48];
    z[51]= - z[51] + n<T>(34585,9048)*z[18];
    z[51]=z[4]*z[51];

    r += z[46] + z[47] + z[48] + z[49] + z[50] + n<T>(1,8)*z[51] + n<T>(1,2)*
      z[54] + z[57] + z[59] + z[60];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf787(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf787(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
