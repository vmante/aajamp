#include "qg_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qg_2lNLC_tf1121(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[74];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[11];
    z[10]=d[12];
    z[11]=d[4];
    z[12]=d[9];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[5];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[12];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[30];
    z[29]=c[31];
    z[30]=e[1];
    z[31]=e[6];
    z[32]=e[14];
    z[33]=f[0];
    z[34]=f[1];
    z[35]=f[3];
    z[36]=f[5];
    z[37]=f[11];
    z[38]=f[17];
    z[39]=f[18];
    z[40]=f[31];
    z[41]=f[32];
    z[42]=f[33];
    z[43]=f[34];
    z[44]=f[36];
    z[45]=f[37];
    z[46]=f[39];
    z[47]=f[43];
    z[48]=f[46];
    z[49]=f[47];
    z[50]=f[51];
    z[51]=f[55];
    z[52]=f[58];
    z[53]=n<T>(1,2)*z[8] + 2*z[12];
    z[54]=n<T>(1,9)*z[8];
    z[53]=z[53]*z[54];
    z[55]=9*z[3];
    z[56]=n<T>(5737,2)*z[11];
    z[57]= - 1325*z[13] + z[56];
    z[57]=n<T>(1,2376)*z[57] + z[55];
    z[58]= - n<T>(31,2)*z[2] + 11*z[4];
    z[57]=z[7] - n<T>(5737,4752)*z[6] + n<T>(1,2)*z[57] + z[58];
    z[59]=n<T>(1,4)*z[7];
    z[57]=z[57]*z[59];
    z[60]=n<T>(1,3)*z[5];
    z[61]=z[8] + z[12];
    z[62]=n<T>(30313,8448)*z[5] + n<T>(8387,12672)*z[7] + n<T>(23887,6336)*z[2] - 
   n<T>(11923,6336)*z[4] - z[3] - n<T>(18409,4224)*z[11] - n<T>(1429,528)*z[10] - 
    z[61];
    z[62]=z[62]*z[60];
    z[63]= - n<T>(7,4)*z[3] + z[9] + z[8];
    z[63]=z[3]*z[63];
    z[63]=z[63] + z[19] + n<T>(1325,2112)*z[18];
    z[64]=npow(z[11],2);
    z[65]=n<T>(1,9)*z[12];
    z[66]=z[9]*z[65];
    z[67]= - n<T>(53,396)*z[4] - n<T>(53,198)*z[10] - 17*z[3];
    z[67]=z[4]*z[67];
    z[68]=2*z[2] + z[3] + n<T>(3,32)*z[4];
    z[68]=z[2]*z[68];
    z[69]= - 265*z[13] + 4949*z[11];
    z[69]= - 37549*z[2] + 5*z[69] - 467*z[4];
    z[69]=n<T>(1,9)*z[69] + n<T>(3745,2)*z[6];
    z[69]=z[6]*z[69];
    z[57]=z[62] + z[57] + n<T>(1,2112)*z[69] + z[68] + n<T>(1,8)*z[67] + n<T>(5737,76032)*z[64]
     + z[53]
     + z[66]
     + n<T>(5399,4752)*z[16] - n<T>(3,2)*z[14] - n<T>(53,1584)*z[15]
     + n<T>(35,8)*z[17]
     + n<T>(1,9)*z[63];
    z[57]=z[1]*z[57];
    z[57]=z[57] + n<T>(16369,228096)*z[21] - n<T>(23,8)*z[26] - 2*z[24];
    z[57]=i*z[57];
    z[62]=1429*z[15] - n<T>(49981,12)*z[16];
    z[62]=n<T>(1,11)*z[62] - n<T>(49,8)*z[20];
    z[63]=npow(z[8],2);
    z[62]=n<T>(1,24)*z[62] - z[63];
    z[63]=n<T>(1,2)*z[3];
    z[66]=z[63] + z[61];
    z[66]=z[3]*z[66];
    z[67]= - n<T>(5737,4224)*z[11] + 5*z[12] - z[8];
    z[67]= - n<T>(110351,38016)*z[5] - n<T>(5737,8448)*z[7] + n<T>(66535,12672)*z[6]
    + n<T>(1103,1584)*z[2] - n<T>(4127,1584)*z[4] + n<T>(1,2)*z[67] - z[3];
    z[60]=z[67]*z[60];
    z[67]=npow(z[4],2);
    z[68]= - z[12] + n<T>(19607,25344)*z[11];
    z[68]=z[11]*z[68];
    z[69]=z[11] + n<T>(1103,19008)*z[2];
    z[70]= - n<T>(4879,19008)*z[4] + z[69];
    z[70]=z[2]*z[70];
    z[71]=z[2] - z[4];
    z[72]=n<T>(22883,38016)*z[6] - z[12] + n<T>(467,19008)*z[71];
    z[72]=z[6]*z[72];
    z[73]= - n<T>(8387,24)*z[7] + n<T>(5737,4)*z[11] - n<T>(10607,3)*z[71];
    z[73]=z[7]*z[73];
    z[60]=z[60] + n<T>(1,3168)*z[73] + z[72] + z[70] + n<T>(199,594)*z[67] + n<T>(1,3)*z[66]
     + n<T>(1,6)*z[62]
     + z[68];
    z[60]=z[5]*z[60];
    z[62]=13271*z[31];
    z[66]= - z[62] - 1325*z[18];
    z[66]=n<T>(1,2)*z[66] - 12025*z[16];
    z[66]=n<T>(1,11)*z[66] - n<T>(3865,6)*z[20];
    z[68]=z[12] - n<T>(13271,38016)*z[11];
    z[68]=z[11]*z[68];
    z[69]=n<T>(18541,19008)*z[4] - z[69];
    z[69]=z[2]*z[69];
    z[70]= - n<T>(19837,14256)*z[6] - n<T>(5855,4752)*z[2] + z[11] - n<T>(3343,4752)
   *z[4];
    z[70]=z[6]*z[70];
    z[66]=n<T>(1,2)*z[70] + z[69] + n<T>(785,9504)*z[67] + n<T>(1,864)*z[66] + z[68]
   ;
    z[66]=z[6]*z[66];
    z[68]=npow(z[3],2);
    z[69]=n<T>(3,2)*z[30];
    z[70]=z[69] - 35*z[17];
    z[70]=n<T>(45,16)*z[20] + n<T>(1739,4752)*z[16] + n<T>(1,4)*z[70] + 3*z[14];
    z[72]=17*z[4] - n<T>(191,3)*z[2];
    z[72]=z[2]*z[72];
    z[67]=n<T>(1,32)*z[72] + n<T>(13,16)*z[67] + n<T>(1,2)*z[70] - z[68];
    z[70]=n<T>(1,2)*z[2];
    z[67]=z[67]*z[70];
    z[72]= - n<T>(265,1188)*z[18] - 7*z[17];
    z[72]= - 9*z[68] - n<T>(5737,4752)*z[64] + n<T>(145987,14256)*z[20] + 5*
    z[72] - n<T>(3479,297)*z[16];
    z[56]=n<T>(5737,4)*z[6] - z[56] + 10607*z[71];
    z[56]=z[6]*z[56];
    z[71]=z[55] + n<T>(5,4)*z[4];
    z[71]=z[4]*z[71];
    z[70]= - z[4] + z[70];
    z[70]=z[2]*z[70];
    z[56]=n<T>(1,1188)*z[56] + n<T>(23,2)*z[70] + n<T>(1,2)*z[72] + z[71];
    z[70]= - n<T>(5737,4752)*z[11] - z[55];
    z[58]=n<T>(107,72)*z[6] + n<T>(1,2)*z[70] - z[58];
    z[58]=n<T>(1,2)*z[58] - n<T>(1,3)*z[7];
    z[58]=z[7]*z[58];
    z[56]=n<T>(1,2)*z[56] + z[58];
    z[56]=z[56]*z[59];
    z[58]=z[63] - z[61];
    z[58]=z[3]*z[58];
    z[53]=n<T>(1,18)*z[58] - z[53] - n<T>(25,72)*z[20] - n<T>(1,9)*z[19] + n<T>(3,4)*
    z[14];
    z[53]=z[3]*z[53];
    z[55]= - z[55] + n<T>(53,198)*z[4];
    z[55]=z[4]*z[55];
    z[55]=z[55] + 17*z[68] - n<T>(523,2376)*z[20] - n<T>(9419,1188)*z[16] - 
    z[69] + n<T>(53,99)*z[15];
    z[55]=z[4]*z[55];
    z[58]= - z[49] - z[43] - z[34] + z[39] + z[45] + z[48];
    z[59]=z[50] - z[52] + n<T>(1,3)*z[51];
    z[58]=n<T>(8105,9504)*z[22] - n<T>(23,32)*z[23] + n<T>(97,48)*z[25] + n<T>(23,8)*
    z[27] + n<T>(3,8)*z[28] - n<T>(67579,3168)*z[29] + n<T>(3,4)*z[33] + n<T>(21,8)*
    z[35] - n<T>(41,16)*z[36] - n<T>(11,4)*z[37] - n<T>(13,8)*z[38] - n<T>(28075,38016)*
    z[40] + n<T>(181,264)*z[41] + n<T>(28519,4752)*z[42] - n<T>(22775,38016)*z[44]
    + n<T>(6935,12672)*z[46] + n<T>(1273,4752)*z[47] + n<T>(1,3)*z[59] - n<T>(1,4)*
    z[58];
    z[59]=2*z[32] - z[19];
    z[59]=z[59]*z[65];
    z[63]=z[32] + n<T>(1,3)*z[20];
    z[61]=z[8]*z[61];
    z[61]=2*z[63] + z[61];
    z[54]=z[61]*z[54];
    z[61]= - n<T>(18023,2)*z[64] - z[62] - n<T>(1985,3)*z[20];
    z[61]=z[11]*z[61];
    z[62]= - n<T>(25,12)*z[13] - z[10];
    z[62]=n<T>(53,176)*z[62] + z[9];
    z[62]=z[20]*z[62];

    r += z[53] + z[54] + n<T>(1,16)*z[55] + z[56] + z[57] + n<T>(1,2)*z[58] + 
      z[59] + z[60] + n<T>(1,19008)*z[61] + n<T>(1,9)*z[62] + z[66] + z[67];
 
    return r;
}

template std::complex<double> qg_2lNLC_tf1121(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_2lNLC_tf1121(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
