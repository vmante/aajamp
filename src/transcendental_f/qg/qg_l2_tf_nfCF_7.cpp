#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_7(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[37];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[17];
    z[5]=d[1];
    z[6]=d[4];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=e[7];
    z[10]=e[9];
    z[11]=c[3];
    z[12]=c[11];
    z[13]=c[15];
    z[14]=c[17];
    z[15]=c[31];
    z[16]=e[6];
    z[17]=f[30];
    z[18]=f[31];
    z[19]=f[33];
    z[20]=f[35];
    z[21]=f[36];
    z[22]=f[39];
    z[23]=f[40];
    z[24]=z[5] + z[2];
    z[25]=z[1]*i;
    z[26]= - n<T>(17,9)*z[3] - n<T>(8,9)*z[7] - z[8];
    z[26]=4*z[26] + 5*z[25] - z[24];
    z[26]=z[9]*z[26];
    z[27]=5*z[8];
    z[28]=2*z[7];
    z[29]=z[27] + z[28];
    z[30]=n<T>(1,3)*z[29] + z[6];
    z[30]=n<T>(8,3)*z[30] + z[24];
    z[30]=z[4]*z[25]*z[30];
    z[29]=8*z[29] - 49*z[25];
    z[29]=n<T>(1,3)*z[29] + 8*z[6];
    z[29]=n<T>(1,3)*z[29] + z[24];
    z[29]=z[10]*z[29];
    z[26]=z[29] + z[30] + z[26];
    z[29]=4*z[7];
    z[30]= - z[29] + z[27];
    z[30]=z[8]*z[30];
    z[31]=npow(z[7],2);
    z[30]=4*z[31] + z[30];
    z[27]= - z[29] - z[27];
    z[27]=n<T>(1,3)*z[27] + z[3];
    z[32]=2*z[25];
    z[27]=z[27]*z[32];
    z[33]=z[25] - z[8];
    z[33]= - n<T>(2,9)*z[6] + z[3] - z[28] - n<T>(5,3)*z[33];
    z[33]=z[6]*z[33];
    z[34]=2*z[8];
    z[35]=z[34] - z[3];
    z[36]= - z[3]*z[35];
    z[27]=z[33] + z[27] + n<T>(1,3)*z[30] + z[36];
    z[27]=z[6]*z[27];
    z[27]=z[27] + z[22];
    z[30]=z[35] - z[32];
    z[30]=z[30]*z[3];
    z[32]=z[34]*z[7];
    z[30]=z[30] - z[32] + z[31];
    z[24]=z[30]*z[24];
    z[30]= - z[7] + n<T>(2,27)*z[8];
    z[30]=z[30]*z[34];
    z[30]=n<T>(1,3)*z[31] + z[30];
    z[30]=z[8]*z[30];
    z[28]= - z[28] + 13*z[8];
    z[28]=z[8]*z[28];
    z[32]=5*z[7] + z[34];
    z[32]=2*z[32] - 17*z[3];
    z[32]=z[3]*z[32];
    z[28]=z[28] + z[32];
    z[28]=z[31] + n<T>(1,9)*z[28];
    z[28]=z[3]*z[28];
    z[32]=npow(z[7],3);
    z[28]=z[28] - n<T>(16,27)*z[32] + z[30];
    z[30]=41*z[7] - 8*z[8];
    z[30]=z[30]*z[34];
    z[29]=z[29] - 61*z[8];
    z[29]=2*z[29] - 7*z[3];
    z[29]=z[3]*z[29];
    z[29]=z[29] - 53*z[31] + z[30];
    z[25]=z[29]*z[25];
    z[29]= - z[7] - z[6];
    z[29]=z[16]*z[29];
    z[29]=z[29] + z[23] + z[18] + z[17];
    z[30]=4*z[14] + n<T>(2,27)*z[12];
    z[30]=i*z[30];
    z[31]= - n<T>(4,3)*z[7] - z[8];
    z[31]=2*z[31] - n<T>(131,3)*z[3];
    z[31]=n<T>(49,3)*z[4] + n<T>(1,3)*z[31] - 2*z[6];
    z[31]=z[11]*z[31];
    z[24]= - n<T>(4,9)*z[21] + n<T>(8,9)*z[20] + 16*z[19] - 7*z[15] - n<T>(26,9)*
    z[13] + n<T>(2,3)*z[31] + 4*z[28] + n<T>(1,9)*z[25] + z[30] + z[24] + n<T>(4,3)*
    z[27] + n<T>(16,9)*z[29] + 2*z[26];

    r += n<T>(2,3)*z[24];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_7(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_7(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
