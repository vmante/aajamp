#include "qg_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCA_69(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[15];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[9];
    z[3]=d[16];
    z[4]=c[3];
    z[5]=d[0];
    z[6]=d[4];
    z[7]=d[2];
    z[8]=d[8];
    z[9]=e[4];
    z[10]=e[14];
    z[11]=z[1]*i;
    z[12]=z[3]*z[11];
    z[11]= - z[11] - z[8] + z[5];
    z[11]=z[2]*z[11];
    z[11]=z[11] + z[12] + n<T>(1,3)*z[4] + z[10] - z[9];
    z[12]= - z[5] + z[7];
    z[13]=2*z[6];
    z[12]=z[13]*z[12];
    z[13]=npow(z[8],2);
    z[14]=npow(z[7],2);
    z[11]= - z[14] + z[13] + z[12] + 2*z[11];

    r += n<T>(2,9)*z[11];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCA_69(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCA_69(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
