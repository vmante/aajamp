#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_63(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[74];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[12];
    z[10]=d[4];
    z[11]=d[16];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[4];
    z[15]=e[5];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[18];
    z[23]=c[19];
    z[24]=c[21];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[29];
    z[30]=c[30];
    z[31]=c[31];
    z[32]=f[0];
    z[33]=f[1];
    z[34]=f[2];
    z[35]=f[3];
    z[36]=f[5];
    z[37]=f[11];
    z[38]=f[13];
    z[39]=f[17];
    z[40]=f[18];
    z[41]=f[21];
    z[42]=f[30];
    z[43]=f[31];
    z[44]=f[32];
    z[45]=f[33];
    z[46]=f[34];
    z[47]=f[35];
    z[48]=f[37];
    z[49]=f[39];
    z[50]=f[41];
    z[51]=f[43];
    z[52]=z[6] - z[7];
    z[53]=z[4] - z[2];
    z[54]=n<T>(2,3)*z[10];
    z[55]= - z[54] + z[52] + z[53];
    z[55]=z[10]*z[55];
    z[56]= - z[3] - z[8];
    z[56]=z[52]*z[56];
    z[57]=npow(z[7],2);
    z[58]= - z[5]*z[7];
    z[59]=z[7] + z[5];
    z[59]=z[6]*z[59];
    z[60]=z[1]*i;
    z[61]=2*z[60];
    z[62]= - z[4]*z[61];
    z[55]=z[55] + z[62] + z[59] - z[57] + z[58] + z[56];
    z[54]=z[55]*z[54];
    z[55]=z[60]*z[2];
    z[56]=npow(z[2],2);
    z[58]= - z[56] + z[55];
    z[59]=z[7] - z[2];
    z[59]=z[61] + n<T>(5,3)*z[59];
    z[59]=z[7]*z[59];
    z[62]=n<T>(1,3)*z[5];
    z[63]=z[61] - z[2];
    z[64]=z[5] + 2*z[7] - z[63];
    z[64]=z[64]*z[62];
    z[65]= - n<T>(17,9)*z[6] - z[62] + n<T>(4,3)*z[7] + n<T>(1,3)*z[2] - z[60];
    z[65]=z[6]*z[65];
    z[58]=z[65] + z[64] + n<T>(2,3)*z[58] + z[59];
    z[58]=z[6]*z[58];
    z[58]= - z[44] - z[42] - z[41] + z[37] - z[49] - z[58] + z[50] - 
    z[48] + z[46] - z[29] + z[36] + z[35] + z[34] - z[33];
    z[59]=z[60] - z[8];
    z[64]=2*z[2];
    z[65]= - 7*z[6] - 2*z[5] - 5*z[7] + z[64] - z[59];
    z[65]=n<T>(1,3)*z[65] - z[10];
    z[65]=z[16]*z[65];
    z[66]= - z[3]*z[60];
    z[66]=z[19] + z[55] + z[66];
    z[66]=z[9]*z[66];
    z[67]=z[10] + z[4];
    z[68]=z[60]*z[67];
    z[68]=z[19] + z[68];
    z[68]=z[11]*z[68];
    z[67]=z[60] - z[67];
    z[67]=z[14]*z[67];
    z[69]=z[60] - z[2];
    z[70]=z[3] + z[69];
    z[70]=z[15]*z[70];
    z[65]=z[70] - z[39] - z[38] - z[32] + z[65] + z[66] + z[68] + z[67];
    z[66]=4*z[60];
    z[67]=z[2] - z[66];
    z[67]= - z[62] + n<T>(1,3)*z[67] + z[52];
    z[67]=z[6]*z[67];
    z[64]=z[64]*z[60];
    z[68]= - z[64] + n<T>(1,3)*z[56];
    z[69]= - z[5] + 2*z[69];
    z[70]=n<T>(1,3)*z[7];
    z[71]=z[70] + z[69];
    z[71]=z[71]*z[62];
    z[62]=z[62] - z[2];
    z[52]= - z[62] + n<T>(2,9)*z[52];
    z[52]=z[8]*z[52];
    z[72]=n<T>(1,9)*z[7];
    z[73]= - z[2]*z[72];
    z[52]=z[52] + n<T>(1,3)*z[67] + z[71] + z[73] + z[68];
    z[52]=z[8]*z[52];
    z[67]=z[56]*z[60];
    z[71]=npow(z[2],3);
    z[52]=z[52] - z[71] + n<T>(5,3)*z[67];
    z[64]=z[64] - z[56];
    z[67]=z[7]*z[61];
    z[71]=z[5] - z[7];
    z[73]=z[5]*z[71];
    z[71]=z[2] - z[71];
    z[71]=z[6]*z[71];
    z[61]= - z[7] - z[61] + z[4];
    z[61]=z[4]*z[61];
    z[61]=z[61] + z[71] + z[73] + z[67] + z[64];
    z[53]=z[3]*z[53];
    z[53]=2*z[61] + z[53];
    z[53]=z[3]*z[53];
    z[61]= - z[7] + 5*z[2] - z[66];
    z[61]=z[61]*z[70];
    z[66]= - z[70] + z[2] + z[60];
    z[66]=z[5]*z[66];
    z[61]=z[66] - 2*z[64] + z[61];
    z[64]=n<T>(2,3)*z[5];
    z[61]=z[61]*z[64];
    z[66]=z[60] - z[4];
    z[62]= - n<T>(2,3)*z[8] - z[62] + n<T>(1,3)*z[66];
    z[62]=z[4]*z[62];
    z[62]=z[68] + z[62];
    z[64]=z[69]*z[64];
    z[66]=z[60] + z[5];
    z[66]=2*z[66] - z[8];
    z[66]=z[8]*z[66];
    z[57]=n<T>(4,3)*z[66] + z[64] + n<T>(1,3)*z[57] + 2*z[62];
    z[57]=z[4]*z[57];
    z[62]= - z[4] + z[63];
    z[62]=z[13]*z[62];
    z[63]= - z[8] + z[63];
    z[63]=z[17]*z[63];
    z[62]=z[30] + z[62] + z[63];
    z[63]=z[7] + z[8];
    z[63]=z[60]*z[63];
    z[63]=z[19] + z[63];
    z[63]=z[12]*z[63];
    z[59]=z[7] - z[59];
    z[59]=z[18]*z[59];
    z[59]=z[63] + z[59] + z[43] - z[22];
    z[55]=z[56] - 4*z[55];
    z[56]= - n<T>(20,3)*z[7] - z[2] - 8*z[60];
    z[56]=z[7]*z[56];
    z[55]=4*z[55] + z[56];
    z[55]=z[55]*z[72];
    z[56]=z[40] + z[51] - z[47];
    z[60]= - n<T>(28,3)*z[27] - n<T>(8,3)*z[25] - n<T>(4,3)*z[24] - n<T>(10,27)*z[20];
    z[60]=i*z[60];
    z[63]=z[10] - 2*z[3] + 5*z[4] + n<T>(8,3)*z[8] - 8*z[6] - 11*z[2] - n<T>(38,3)*z[7];
    z[63]=z[19]*z[63];
    z[52]=n<T>(32,9)*z[45] - n<T>(35,9)*z[31] + n<T>(20,3)*z[28] + n<T>(20,9)*z[26] + n<T>(10,9)*z[23] - n<T>(22,9)*z[21] + n<T>(1,9)*z[63] + z[54] + n<T>(1,3)*z[53] + z[57]
    + z[61] + z[55] + z[60] + 2*z[52] + n<T>(8,9)*z[59] - n<T>(4,9)*z[56] + n<T>(8,3)*z[62] + n<T>(4,3)*z[65] - n<T>(2,3)*z[58];

    r += 4*z[52];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_63(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_63(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
