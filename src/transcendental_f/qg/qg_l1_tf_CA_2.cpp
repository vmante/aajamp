#include "qg_l1_tf_CA_decl.hpp"

template<class T>
std::complex<T> qg_l1_tf_CA_2(
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[4];
  std::complex<T> r(0,0);

    z[1]=d[8];
    z[2]=d[9];
    z[3]=z[1] - z[2];

    r += 4*z[3];
 
    return r;
}

template std::complex<double> qg_l1_tf_CA_2(
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l1_tf_CA_2(
  const std::array<std::complex<dd_real>,19>& d
);
#endif
