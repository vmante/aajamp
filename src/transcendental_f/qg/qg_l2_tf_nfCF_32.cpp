#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_32(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[38];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=d[1];
    z[11]=c[11];
    z[12]=c[15];
    z[13]=c[18];
    z[14]=c[23];
    z[15]=c[25];
    z[16]=c[26];
    z[17]=c[28];
    z[18]=c[29];
    z[19]=c[30];
    z[20]=c[31];
    z[21]=f[0];
    z[22]=f[3];
    z[23]=f[5];
    z[24]=f[11];
    z[25]=f[13];
    z[26]=f[17];
    z[27]=f[20];
    z[28]=n<T>(1,3)*z[4];
    z[29]= - n<T>(2,3)*z[2] + z[3];
    z[29]=2*z[29] - z[28];
    z[29]=z[4]*z[29];
    z[30]=npow(z[2],2);
    z[31]=npow(z[3],2);
    z[32]= - z[3] - z[28] + n<T>(4,3)*z[2];
    z[33]=z[5]*z[32];
    z[29]=z[33] + z[29] + n<T>(2,3)*z[30] - z[31];
    z[29]=z[5]*z[29];
    z[31]=n<T>(1,3)*z[3];
    z[33]=5*z[2];
    z[34]= - z[33] + 7*z[3];
    z[34]=z[34]*z[31];
    z[35]=z[3]*z[28];
    z[34]=z[35] - z[30] + z[34];
    z[34]=z[4]*z[34];
    z[35]=z[3]*z[2];
    z[36]= - z[2] + z[3];
    z[36]=z[4]*z[36];
    z[36]=z[36] + z[30] - z[35];
    z[36]=z[10]*z[36];
    z[37]=z[30] - 4*z[35];
    z[31]=z[37]*z[31];
    z[37]=npow(z[2],3);
    z[29]=z[36] + z[29] + z[34] + z[37] + z[31];
    z[31]=z[33] - n<T>(17,3)*z[3];
    z[28]=z[31]*z[28];
    z[31]=n<T>(2,3)*z[5];
    z[32]= - z[32]*z[31];
    z[28]=z[32] + z[28] - z[30] + n<T>(11,9)*z[35];
    z[30]=z[1]*i;
    z[28]=z[28]*z[30];
    z[28]=z[28] + n<T>(1,3)*z[29];
    z[29]=13*z[30] - z[33] - 8*z[3];
    z[29]=n<T>(1,3)*z[29] + z[10];
    z[29]=z[6]*z[29];
    z[32]=z[10] + z[2];
    z[33]= - 5*z[30] + z[32] + 4*z[3];
    z[33]=z[7]*z[33];
    z[29]= - z[29] - z[33] + z[26] - z[18];
    z[31]=z[31] + 2*z[4] - n<T>(2,3)*z[3] - z[32];
    z[31]=z[9]*z[31];
    z[30]=2*z[30] - z[2] - z[5];
    z[30]=z[8]*z[30];
    z[30]= - z[13] + z[30] + z[27] + z[25] - z[23];
    z[32]=z[12] + z[24] - z[22];
    z[33]= - 8*z[16] - n<T>(4,3)*z[14] + n<T>(5,9)*z[11];
    z[33]=i*z[33];
    z[28]= - n<T>(8,3)*z[21] - 5*z[20] + n<T>(16,3)*z[19] + 8*z[17] + n<T>(16,9)*
    z[15] + 2*z[28] + n<T>(1,9)*z[31] + z[33] - n<T>(2,9)*z[32] - n<T>(2,3)*z[29] + 
   n<T>(8,9)*z[30];

    r += 2*z[28];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_32(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_32(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
