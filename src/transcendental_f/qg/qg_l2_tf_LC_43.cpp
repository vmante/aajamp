#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_43(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[30];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[15];
    z[11]=c[18];
    z[12]=c[23];
    z[13]=c[25];
    z[14]=c[26];
    z[15]=c[28];
    z[16]=c[29];
    z[17]=c[30];
    z[18]=c[31];
    z[19]=f[0];
    z[20]=f[3];
    z[21]=f[11];
    z[22]=f[17];
    z[23]=f[20];
    z[24]=n<T>(1,2)*z[4];
    z[25]=i*z[1];
    z[26]= - 5*z[25] - z[24];
    z[26]=z[26]*z[24];
    z[27]=n<T>(1,12)*z[9];
    z[28]=z[6] - z[27];
    z[26]=5*z[28] + z[26];
    z[28]=n<T>(1,2)*z[5];
    z[29]=z[28] - z[25] - z[4];
    z[28]=z[29]*z[28];
    z[29]=n<T>(5,3)*z[4] + z[5];
    z[29]=z[3]*z[29];
    z[26]=n<T>(1,4)*z[29] + n<T>(1,3)*z[26] + z[28];
    z[26]=z[3]*z[26];
    z[26]=z[11] + z[26] - 4*z[17] + n<T>(119,24)*z[18] + z[19] + n<T>(5,12)*
    z[20] + n<T>(1,12)*z[21] - n<T>(1,3)*z[23] + n<T>(1,4)*z[22];
    z[28]= - z[25] + z[24];
    z[24]=z[28]*z[24];
    z[28]=z[5]*z[4];
    z[24]=n<T>(1,4)*z[28] + z[24] + z[8] - z[27];
    z[24]=z[5]*z[24];
    z[27]=z[4] + 2*z[25];
    z[28]= - z[5] + z[27];
    z[28]=z[5]*z[28];
    z[28]=z[28] + z[8];
    z[29]=z[4]*z[25];
    z[27]= - z[3] + z[27];
    z[27]=z[3]*z[27];
    z[27]=n<T>(2,3)*z[27] + z[29] - n<T>(1,6)*z[9] + n<T>(5,3)*z[6] + z[7] + n<T>(1,3)*
    z[28];
    z[28]=z[2] - z[4];
    z[25]= - n<T>(1,9)*z[3] - n<T>(1,18)*z[5] - n<T>(1,2)*z[25] + n<T>(1,3)*z[28];
    z[25]=z[2]*z[25];
    z[25]=n<T>(1,3)*z[27] + z[25];
    z[25]=z[2]*z[25];
    z[27]= - n<T>(10,3)*z[6] - z[7] - n<T>(2,3)*z[8];
    z[27]=z[1]*z[27];
    z[27]=n<T>(1,3)*z[27] + n<T>(5,3)*z[14] + n<T>(1,2)*z[12];
    z[27]=i*z[27];
    z[28]=z[4]*z[9];

    r += n<T>(1,12)*z[10] - n<T>(4,9)*z[13] - n<T>(5,3)*z[15] - n<T>(1,4)*z[16] + n<T>(1,9)
      *z[24] + z[25] + n<T>(1,3)*z[26] + z[27] - n<T>(1,36)*z[28];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_43(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_43(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
