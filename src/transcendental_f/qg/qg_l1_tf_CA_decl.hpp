#ifndef QG_L1_TF_CA_DECL_H
#define QG_L1_TF_CA_DECL_H

#include <array>
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
std::complex<T> qg_l1_tf_CA_2(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l1_tf_CA_3(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_4(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l1_tf_CA_5(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_6(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l1_tf_CA_7(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_8(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_9(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_10(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l1_tf_CA_11(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_12(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l1_tf_CA_13(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_14(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_15(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_16(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_17(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_18(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l1_tf_CA_19(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_20(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_21(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_22(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_23(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_24(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_25(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_26(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qg_l1_tf_CA_27(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_28(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_29(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_30(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_31(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_32(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_33(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_34(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_l1_tf_CA_35(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

#endif /* QG_L1_TF_CA_DECL_H */
