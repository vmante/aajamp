#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_9(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[7];
    z[6]=e[6];
    z[7]=e[7];
    z[8]=c[3];
    z[9]=d[0];
    z[10]=d[1];
    z[11]=c[11];
    z[12]=f[30];
    z[13]=f[31];
    z[14]=f[33];
    z[15]=f[35];
    z[16]=f[36];
    z[17]=f[38];
    z[18]=npow(z[3],2);
    z[19]=npow(z[2],2);
    z[20]=z[18] - z[19];
    z[21]=z[1]*i;
    z[22]=z[21] + z[4];
    z[22]= - z[5] + 2*z[22];
    z[23]=z[3] - z[2];
    z[22]=z[23]*z[22];
    z[22]=z[22] - z[20];
    z[22]=z[5]*z[22];
    z[22]= - z[13] + z[22] - z[16];
    z[24]=2*z[2];
    z[25]=z[24] - z[3];
    z[25]=z[25]*z[3];
    z[26]=2*z[7];
    z[25]=z[25] - z[26] - z[19];
    z[27]=z[10] + z[9];
    z[25]= - z[25]*z[27];
    z[18]= - z[19] + n<T>(5,3)*z[18];
    z[18]=z[3]*z[18];
    z[23]=z[23]*z[4];
    z[20]= - n<T>(4,3)*z[20] - z[23];
    z[20]=z[4]*z[20];
    z[28]=npow(z[2],3);
    z[18]=z[20] - n<T>(2,3)*z[28] + z[18];
    z[20]=z[24] + n<T>(1,3)*z[3];
    z[20]=z[3]*z[20];
    z[19]= - n<T>(8,3)*z[23] - n<T>(7,3)*z[19] + z[20];
    z[19]=z[19]*z[21];
    z[20]= - z[2] - z[4];
    z[20]=n<T>(8,3)*z[20] - z[21] + z[27];
    z[20]=z[6]*z[20];
    z[23]= - 4*z[2] + 7*z[3];
    z[23]=n<T>(4,3)*z[23] + z[27];
    z[23]=z[8]*z[23];
    z[24]=z[11]*i;
    z[23]=z[23] - z[24];
    z[24]=n<T>(2,3)*z[4] + z[2] + n<T>(5,3)*z[3];
    z[21]=4*z[24] - z[21];
    z[21]=z[21]*z[26];
    z[24]=z[15] + z[12];
    z[26]=z[17] + z[14];
    z[18]=2*z[20] + z[21] + 4*z[18] + z[19] + z[25] + n<T>(1,3)*z[23] - 16*
    z[26] - n<T>(8,3)*z[24] + n<T>(4,3)*z[22];

    r += n<T>(2,3)*z[18];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_9(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_9(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
