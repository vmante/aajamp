#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_65(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[117];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[12];
    z[12]=d[4];
    z[13]=d[15];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[18];
    z[34]=c[19];
    z[35]=c[21];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[29];
    z[41]=c[30];
    z[42]=c[31];
    z[43]=e[3];
    z[44]=e[6];
    z[45]=e[14];
    z[46]=f[0];
    z[47]=f[1];
    z[48]=f[2];
    z[49]=f[3];
    z[50]=f[4];
    z[51]=f[5];
    z[52]=f[6];
    z[53]=f[7];
    z[54]=f[10];
    z[55]=f[11];
    z[56]=f[12];
    z[57]=f[13];
    z[58]=f[14];
    z[59]=f[16];
    z[60]=f[17];
    z[61]=f[18];
    z[62]=f[21];
    z[63]=f[23];
    z[64]=f[30];
    z[65]=f[31];
    z[66]=f[32];
    z[67]=f[33];
    z[68]=f[34];
    z[69]=f[35];
    z[70]=f[36];
    z[71]=f[39];
    z[72]=f[43];
    z[73]=f[50];
    z[74]=f[51];
    z[75]=f[55];
    z[76]=f[56];
    z[77]=f[58];
    z[78]=f[60];
    z[79]=z[1]*i;
    z[80]=z[79]*z[3];
    z[81]=z[5]*z[79];
    z[81]=z[30] + z[81] + z[80];
    z[81]=z[13]*z[81];
    z[82]=z[79]*z[4];
    z[82]=z[82] + z[30];
    z[83]= - z[12]*z[79];
    z[83]=z[83] - z[82];
    z[83]=z[15]*z[83];
    z[84]=z[79]*z[8];
    z[84]=z[84] + z[30];
    z[85]= - z[9]*z[79];
    z[85]=z[85] - z[84];
    z[85]=z[17]*z[85];
    z[86]=z[79] - z[5];
    z[87]=z[3] - z[86];
    z[87]=z[20]*z[87];
    z[88]=z[79] - z[4];
    z[89]=z[12] - z[88];
    z[89]=z[21]*z[89];
    z[90]=z[79] - z[8];
    z[91]=z[9] - z[90];
    z[91]=z[27]*z[91];
    z[81]=z[78] - z[76] + z[68] - z[66] + z[85] + z[87] + z[89] + z[91]
    + z[81] + z[83] - z[62] + z[52] + z[46] - z[40];
    z[83]=z[2] - z[5];
    z[85]=z[83] + z[79];
    z[87]=2*z[8];
    z[89]=z[87] - z[85];
    z[89]=z[89]*z[87];
    z[91]=2*z[6];
    z[92]= - z[91] + z[83];
    z[92]=z[92]*z[91];
    z[93]=2*z[79];
    z[94]=z[93] + z[5];
    z[95]=2*z[2];
    z[96]=n<T>(7,3)*z[7] - 5*z[6] + z[8] + z[95] + z[94];
    z[96]=z[7]*z[96];
    z[97]=2*z[86];
    z[98]=z[97] + z[2];
    z[98]=z[98]*z[2];
    z[99]=z[93] - z[5];
    z[100]=z[99]*z[5];
    z[101]=2*z[90];
    z[102]=z[101] + z[12];
    z[102]=z[12]*z[102];
    z[89]=z[96] + z[102] + z[92] + z[89] - z[100] + z[98];
    z[89]=z[7]*z[89];
    z[92]=z[99] - z[2];
    z[96]=z[19]*z[92];
    z[102]= - z[3] - z[12];
    z[102]=z[43]*z[102];
    z[89]= - z[63] + z[61] + z[57] - z[54] + z[89] + z[96] + z[102] - 
    z[73];
    z[96]=2*z[5];
    z[102]=z[79]*z[96];
    z[103]=n<T>(2,3)*z[9];
    z[104]=z[103] - n<T>(2,3)*z[79] + z[5];
    z[104]=z[9]*z[104];
    z[105]=z[9] - z[8];
    z[99]= - z[99] - z[105];
    z[99]=z[3]*z[99];
    z[106]=n<T>(1,3)*z[4];
    z[107]= - z[106] + z[6] - n<T>(1,3)*z[9] + z[83];
    z[107]=z[4]*z[107];
    z[92]=z[2]*z[92];
    z[108]=z[93] - z[2];
    z[109]= - z[3] - z[9] - z[108];
    z[109]=z[6]*z[109];
    z[92]=z[107] + z[109] + z[99] + z[104] + z[102] + z[92];
    z[99]=z[108] - z[8];
    z[102]=2*z[9];
    z[104]=z[102] + z[99];
    z[104]=z[10]*z[104];
    z[92]=2*z[92] + z[104];
    z[92]=z[10]*z[92];
    z[104]=7*z[79];
    z[107]=z[95] + z[104] - z[96];
    z[109]=7*z[8];
    z[107]=2*z[107] - z[109];
    z[107]=z[8]*z[107];
    z[110]=z[95] - z[94];
    z[110]=z[2]*z[110];
    z[110]=z[100] + z[110];
    z[107]=2*z[110] + z[107];
    z[110]=n<T>(1,3)*z[5];
    z[111]=n<T>(1,3)*z[2];
    z[112]= - z[111] + z[79] + z[110];
    z[105]=n<T>(34,9)*z[6] + 2*z[112] + z[105];
    z[105]=z[6]*z[105];
    z[112]=2*z[3];
    z[113]=z[9] - z[83];
    z[113]=z[113]*z[112];
    z[114]=z[97] - z[9];
    z[114]=z[9]*z[114];
    z[105]=z[105] + z[113] + n<T>(1,3)*z[107] + z[114];
    z[105]=z[6]*z[105];
    z[92]= - z[77] + z[74] - z[71] + z[58] - z[47] + z[105] + z[92];
    z[105]=n<T>(2,3)*z[3];
    z[107]=2*z[4];
    z[110]=n<T>(8,3)*z[12] - z[107] - z[91] - z[105] + z[9] - n<T>(2,3)*z[8] + 
    z[95] + z[79] - z[110];
    z[110]=z[12]*z[110];
    z[113]=z[93] - z[8];
    z[113]=z[113]*z[87];
    z[113]=z[100] + z[113];
    z[101]= - z[101] - z[6];
    z[101]=z[101]*z[91];
    z[114]=z[97] + z[9];
    z[114]=z[9]*z[114];
    z[97]= - z[97] - z[3];
    z[97]=z[3]*z[97];
    z[115]=4*z[79];
    z[116]=z[4]*z[115];
    z[97]=z[110] + z[116] + z[101] + n<T>(4,3)*z[97] + n<T>(1,3)*z[113] + z[114]
   ;
    z[97]=z[12]*z[97];
    z[101]=4*z[5];
    z[110]=5*z[79] - z[5];
    z[110]=z[110]*z[101];
    z[113]=16*z[2] - 26*z[79] - 11*z[5];
    z[113]=z[2]*z[113];
    z[110]=z[110] + z[113];
    z[110]=z[110]*z[111];
    z[113]= - npow(z[5],2)*z[93];
    z[110]=z[113] + z[110];
    z[94]=2*z[94] - z[2];
    z[94]=z[94]*z[95];
    z[94]= - z[100] + z[94];
    z[113]=n<T>(14,3)*z[8] - n<T>(28,3)*z[2] - 10*z[79] + n<T>(7,3)*z[5];
    z[113]=z[8]*z[113];
    z[94]=n<T>(7,3)*z[94] + z[113];
    z[94]=z[8]*z[94];
    z[113]=z[96] + 25*z[2];
    z[113]=16*z[3] - 46*z[9] + 2*z[113] - 103*z[8];
    z[113]=n<T>(65,3)*z[4] + n<T>(1,3)*z[113] + 16*z[6];
    z[113]= - n<T>(8,9)*z[7] + n<T>(88,9)*z[10] + n<T>(1,3)*z[113] + z[12];
    z[113]=z[30]*z[113];
    z[94]=z[97] + 2*z[110] + z[94] + z[113] + z[59];
    z[97]= - 34*z[2] + 22*z[79] + 13*z[5];
    z[97]= - z[107] + z[91] - z[112] + z[103] + n<T>(1,3)*z[97] + z[109];
    z[97]=z[4]*z[97];
    z[95]= - z[95] + 17*z[79] + z[101];
    z[95]=z[2]*z[95];
    z[95]= - 13*z[100] + 4*z[95];
    z[101]= - z[79] - z[5];
    z[101]=2*z[101] + z[8];
    z[101]=z[101]*z[109];
    z[107]= - z[93] + z[9];
    z[103]=z[107]*z[103];
    z[85]= - 2*z[85] - z[6];
    z[85]=z[85]*z[91];
    z[91]=z[3]*z[115];
    z[85]=z[97] + z[85] + z[91] + z[103] + n<T>(1,3)*z[95] + z[101];
    z[85]=z[85]*z[106];
    z[91]=z[90] + z[83];
    z[95]=5*z[7] - 2*z[91] + 7*z[6];
    z[95]=z[23]*z[95];
    z[97]=z[10]*z[79];
    z[82]=z[97] + z[82];
    z[82]=z[14]*z[82];
    z[97]= - z[10] + z[88];
    z[97]=z[28]*z[97];
    z[101]=z[12] + z[7];
    z[101]=z[44]*z[101];
    z[103]= - z[9] - z[10];
    z[103]=z[45]*z[103];
    z[82]=z[48] + z[103] + z[72] - z[69] - z[64] + z[95] + z[82] + z[97]
    + z[101];
    z[95]= - z[2]*z[79];
    z[80]= - z[30] + z[95] + z[80];
    z[80]=z[11]*z[80];
    z[95]= - z[7]*z[79];
    z[84]=z[95] - z[84];
    z[84]=z[16]*z[84];
    z[95]= - z[3] - z[79] + z[2];
    z[95]=z[22]*z[95];
    z[90]= - z[7] + z[90];
    z[90]=z[25]*z[90];
    z[88]=z[9] + z[88];
    z[88]=z[29]*z[88];
    z[80]=z[88] + z[80] + z[84] + z[95] + z[90];
    z[84]=z[79] - 5*z[5];
    z[84]=z[84]*z[96];
    z[88]=z[115] + z[83];
    z[90]= - z[87] + z[88];
    z[90]=z[90]*z[87];
    z[95]= - z[104] + z[5];
    z[95]=2*z[95] + 5*z[2];
    z[95]=z[2]*z[95];
    z[84]=z[90] + z[84] + z[95];
    z[86]= - z[9] - n<T>(1,3)*z[8] + z[111] + z[86];
    z[86]=z[86]*z[102];
    z[87]= - z[87] - z[88];
    z[87]=n<T>(11,9)*z[3] + n<T>(1,3)*z[87] + z[9];
    z[87]=z[3]*z[87];
    z[84]=z[87] + n<T>(1,3)*z[84] + z[86];
    z[84]=z[84]*z[105];
    z[83]=z[93] - z[83];
    z[83]=2*z[83] + z[8];
    z[83]=z[8]*z[83];
    z[83]=z[98] + z[83];
    z[86]=z[8] - z[2];
    z[79]= - n<T>(4,27)*z[9] - n<T>(11,9)*z[79] + z[5] - n<T>(4,9)*z[86];
    z[79]=z[9]*z[79];
    z[79]=z[79] - z[100] + n<T>(2,9)*z[83];
    z[79]=z[9]*z[79];
    z[83]=n<T>(7,9)*z[3] - n<T>(2,9)*z[91] + z[9];
    z[83]=z[26]*z[83];
    z[83]=z[83] - z[37];
    z[86]=z[50] + z[49];
    z[87]=z[53] + z[32];
    z[88]=z[75] - z[70];
    z[90]=z[24]*z[99];
    z[90]=z[90] - z[51];
    z[91]=n<T>(128,9)*z[38] + n<T>(40,9)*z[36] + n<T>(8,3)*z[35] + n<T>(19,27)*z[31];
    z[91]=i*z[91];
    z[93]=z[4] - z[108];
    z[93]=z[18]*z[93];
    z[79]= - n<T>(64,9)*z[67] - n<T>(22,9)*z[65] + n<T>(7,3)*z[60] + n<T>(1,9)*z[56] + n<T>(7,9)*z[55] + n<T>(70,9)*z[42] - n<T>(16,3)*z[41] - n<T>(100,9)*z[39] - n<T>(44,27)*
    z[34] + n<T>(16,9)*z[33] + n<T>(52,9)*z[93] + z[85] + z[84] + z[79] + z[91]
    + 4*z[83] - n<T>(28,9)*z[90] - n<T>(2,9)*z[88] + n<T>(32,9)*z[87] + n<T>(13,9)*
    z[86] + n<T>(1,3)*z[94] + n<T>(2,3)*z[92] + n<T>(8,3)*z[80] + n<T>(4,9)*z[89] + n<T>(4,3)
   *z[81] + n<T>(8,9)*z[82];

    r += 2*z[79];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_65(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_65(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
