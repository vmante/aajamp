#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_55(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[50];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[8];
    z[14]=e[10];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[18];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[29];
    z[24]=c[30];
    z[25]=c[31];
    z[26]=e[3];
    z[27]=f[3];
    z[28]=f[4];
    z[29]=f[7];
    z[30]=f[11];
    z[31]=f[12];
    z[32]=f[16];
    z[33]=f[17];
    z[34]=f[23];
    z[35]=z[1]*i;
    z[36]=z[35]*z[5];
    z[37]=npow(z[5],2);
    z[38]= - z[36] + n<T>(1,2)*z[37];
    z[39]=z[35] - z[5];
    z[40]=n<T>(1,2)*z[7];
    z[41]=z[40] + z[39];
    z[41]=z[7]*z[41];
    z[42]= - n<T>(1,2)*z[3] - z[39];
    z[42]=z[3]*z[42];
    z[43]=n<T>(3,4)*z[7] - n<T>(1,4)*z[39] - z[3];
    z[43]=z[8]*z[43];
    z[41]=z[43] + n<T>(3,2)*z[41] + n<T>(1,2)*z[38] + z[42];
    z[41]=z[8]*z[41];
    z[42]=z[3] + z[8];
    z[42]=z[26]*z[42];
    z[41]=z[41] + z[42];
    z[42]=n<T>(5,2)*z[37] - 11*z[36];
    z[43]=n<T>(1,2)*z[5];
    z[44]=z[43] + z[35];
    z[45]=n<T>(13,2)*z[44] - 2*z[2];
    z[45]=z[2]*z[45];
    z[42]=n<T>(1,4)*z[42] + z[45];
    z[45]= - n<T>(3,2)*z[35] - z[3];
    z[45]=3*z[45] + n<T>(5,4)*z[7];
    z[40]=z[45]*z[40];
    z[45]=z[7] - z[3];
    z[46]=z[5] - n<T>(23,8)*z[2];
    z[45]=n<T>(1,3)*z[46] + n<T>(9,8)*z[45];
    z[45]=z[6]*z[45];
    z[46]=9*z[35] + n<T>(7,2)*z[3];
    z[46]=z[3]*z[46];
    z[40]=z[45] + z[40] + n<T>(1,3)*z[42] + n<T>(1,4)*z[46];
    z[40]=z[6]*z[40];
    z[42]= - n<T>(1,4)*z[2] + z[44];
    z[42]=z[2]*z[42];
    z[42]=n<T>(1,4)*z[38] + z[42];
    z[44]=z[35] + z[5];
    z[45]=n<T>(1,2)*z[6];
    z[46]=z[45] - z[44];
    z[47]=n<T>(1,4)*z[6];
    z[46]=z[46]*z[47];
    z[48]=n<T>(1,4)*z[5] - z[2];
    z[47]=n<T>(1,3)*z[48] + z[47];
    z[48]=n<T>(1,2)*z[4];
    z[47]=z[47]*z[48];
    z[42]=z[47] + n<T>(1,3)*z[42] + z[46];
    z[42]=z[4]*z[42];
    z[46]=n<T>(33,2) - n<T>(17,3)*z[5];
    z[46]=n<T>(25,24)*z[2] + n<T>(1,8)*z[46] - n<T>(4,3)*z[35];
    z[46]=z[2]*z[46];
    z[47]= - n<T>(33,8) + n<T>(1,3)*z[5];
    z[47]=z[1]*i*z[47];
    z[46]=z[46] - n<T>(5,12)*z[37] + z[47];
    z[46]=z[2]*z[46];
    z[36]= - n<T>(7,2)*z[37] + 17*z[36];
    z[37]=n<T>(9,8)*z[2] - z[5] - n<T>(5,4)*z[35];
    z[37]=z[2]*z[37];
    z[44]=9*z[3] + 7*z[2] - n<T>(33,2) - n<T>(13,3)*z[44];
    z[44]=z[3]*z[44];
    z[36]=n<T>(1,8)*z[44] + n<T>(1,6)*z[36] + z[37];
    z[36]=z[3]*z[36];
    z[37]=n<T>(1,2)*z[2];
    z[44]=z[37] - z[35];
    z[47]=z[2]*z[44];
    z[38]=n<T>(5,3)*z[38] + z[47];
    z[47]=13*z[5] + 17*z[35];
    z[47]=n<T>(13,12)*z[3] + n<T>(1,12)*z[47] - z[2];
    z[47]=z[3]*z[47];
    z[49]= - n<T>(2,3)*z[7] - n<T>(35,24)*z[3] - n<T>(1,3)*z[39] - n<T>(5,8)*z[2];
    z[49]=z[7]*z[49];
    z[38]=z[49] + n<T>(1,4)*z[38] + z[47];
    z[38]=z[7]*z[38];
    z[47]=z[35] - z[6];
    z[37]=n<T>(43,12)*z[7] + n<T>(19,12)*z[3] + z[37] - z[43] - 2*z[47];
    z[37]=z[14]*z[37];
    z[47]=z[3] + z[5];
    z[35]= - z[35]*z[47];
    z[35]= - z[15] + z[35];
    z[35]=z[9]*z[35];
    z[39]= - z[3] + z[39];
    z[39]=z[12]*z[39];
    z[35]=z[35] + z[39];
    z[39]=z[43] + z[44];
    z[39]=n<T>(1,3)*z[39] + z[7];
    z[39]=z[11]*z[39];
    z[43]=z[48] + z[44];
    z[43]=z[10]*z[43];
    z[44]=z[45] + z[44];
    z[44]=z[13]*z[44];
    z[45]=z[20] - z[18];
    z[47]=z[24] + z[22];
    z[48]=n<T>(31,6)*z[21] + n<T>(31,12)*z[19] - n<T>(7,18)*z[16];
    z[48]=i*z[48];
    z[49]= - 7*z[5] + n<T>(25,2)*z[2];
    z[49]= - n<T>(5,6)*z[6] - n<T>(31,3)*z[7] + n<T>(1,3)*z[49] + n<T>(29,2)*z[3];
    z[49]= - n<T>(5,36)*z[4] + n<T>(1,6)*z[49] - z[8];
    z[49]=z[15]*z[49];

    r += n<T>(79,72)*z[17] - n<T>(31,24)*z[23] + n<T>(1615,144)*z[25] + n<T>(5,24)*
      z[27] - n<T>(25,24)*z[28] + 3*z[29] + n<T>(11,24)*z[30] - n<T>(3,8)*z[31] + n<T>(9,8)*z[32]
     + n<T>(5,8)*z[33]
     + z[34]
     + n<T>(2,3)*z[35]
     + z[36]
     + z[37]
     +  z[38] + z[39] + z[40] + n<T>(3,2)*z[41] + 5*z[42] + n<T>(5,3)*z[43] + n<T>(13,6)*z[44]
     - n<T>(31,18)*z[45]
     + z[46] - n<T>(31,6)*z[47]
     + z[48]
     + n<T>(1,2)*
      z[49];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_55(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_55(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
