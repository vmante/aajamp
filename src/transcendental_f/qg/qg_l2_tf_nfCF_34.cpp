#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_34(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[7];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=c[3];
    z[4]=d[1];
    z[5]=npow(z[4],2);
    z[6]=z[1]*i;
    z[6]= - 2*z[6] + z[2];
    z[6]=z[2]*z[6];
    z[5]=z[6] - z[5] - z[3];

    r += 2*z[5];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_34(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_34(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d
);
#endif
