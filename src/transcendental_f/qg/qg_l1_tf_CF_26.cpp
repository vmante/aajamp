#include "qg_l1_tf_CF_decl.hpp"

template<class T>
std::complex<T> qg_l1_tf_CF_26(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[5];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[7];
    z[3]=d[8];
    z[4]= - i*z[1];
    z[4]=z[4] + z[2] - z[3];

    r += 4*z[4];
 
    return r;
}

template std::complex<double> qg_l1_tf_CF_26(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l1_tf_CF_26(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d
);
#endif
