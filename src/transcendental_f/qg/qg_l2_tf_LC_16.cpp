#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_16(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[11];
  std::complex<T> r(0,0);

    z[1]=c[15];
    z[2]=c[18];
    z[3]=c[19];
    z[4]=c[25];
    z[5]=c[28];
    z[6]=c[29];
    z[7]=c[30];
    z[8]=c[31];
    z[9]=f[5];
    z[10]= - z[4] - n<T>(1,2)*z[3] + z[1] + 4*z[2];
    z[10]= - z[9] + n<T>(49,6)*z[8] - 4*z[7] - z[6] + n<T>(1,3)*z[10] - 2*z[5];

    r += n<T>(1,9)*z[10];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_16(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_16(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
