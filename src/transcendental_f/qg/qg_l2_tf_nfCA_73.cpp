#include "qg_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCA_73(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[7];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[4];
    z[4]=d[0];
    z[5]=z[1]*i;
    z[5]=z[5] - z[4];
    z[6]= - 2*z[3] + z[5];
    z[6]=z[3]*z[6];
    z[5]= - z[3] - z[5];
    z[5]=n<T>(1,3)*z[5] + z[2];
    z[5]=z[2]*z[5];
    z[5]=n<T>(1,3)*z[6] + z[5];

    r += n<T>(2,3)*z[5];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCA_73(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCA_73(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d
);
#endif
