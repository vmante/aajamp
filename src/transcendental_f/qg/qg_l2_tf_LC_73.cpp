#include "qg_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_LC_73(
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[7];
  std::complex<T> r(0,0);

    z[1]=d[0];
    z[2]=d[3];
    z[3]=d[2];
    z[4]=npow(z[1],2);
    z[5]= - z[2]*z[1];
    z[6]=z[2] - n<T>(1,2)*z[3];
    z[6]=z[3]*z[6];
    z[4]=z[6] + n<T>(1,2)*z[4] + z[5];

    r += n<T>(1,18)*z[4];
 
    return r;
}

template std::complex<double> qg_l2_tf_LC_73(
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_LC_73(
  const std::array<std::complex<dd_real>,19>& d
);
#endif
