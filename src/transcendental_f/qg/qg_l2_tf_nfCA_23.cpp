#include "qg_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCA_23(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[18];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[16];
    z[5]=d[2];
    z[6]=e[3];
    z[7]=e[4];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[31];
    z[12]=f[14];
    z[13]=z[6] + z[7];
    z[14]=npow(z[5],2);
    z[13]=z[14] + 2*z[13];
    z[14]=i*z[1];
    z[15]=z[2] - z[4] + z[5];
    z[15]=z[15]*z[14];
    z[16]=2*z[2];
    z[17]= - z[5]*z[16];
    z[14]=z[3] + z[14] + z[2] - 2*z[5];
    z[14]=z[3]*z[14];
    z[14]=z[14] + 2*z[15] + z[17] + n<T>(5,3)*z[8] + z[13];
    z[14]=z[3]*z[14];
    z[15]=n<T>(1,3)*z[8];
    z[17]= - z[15] + z[13];
    z[17]=z[2]*z[17];
    z[16]= - z[4]*z[16];
    z[13]=z[16] - z[13];
    z[13]=z[1]*z[13];
    z[13]=z[9] + z[13];
    z[13]=i*z[13];
    z[13]=z[14] + z[17] + z[13];
    z[14]=z[10] - z[12];
    z[15]= - z[4]*z[15];
    z[14]=z[15] + z[11] - n<T>(2,3)*z[14];

    r += n<T>(1,3)*z[13] + 2*z[14];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCA_23(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCA_23(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
