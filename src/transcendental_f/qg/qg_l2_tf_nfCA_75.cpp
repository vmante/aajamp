#include "qg_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCA_75(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[16];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[8];
    z[7]=d[18];
    z[8]=c[3];
    z[9]=d[7];
    z[10]=e[11];
    z[11]=i*z[1];
    z[12]=z[11] - z[2];
    z[13]= - z[3] - z[12];
    z[13]=z[5]*z[13];
    z[14]=2*z[3];
    z[15]=z[14] - z[9];
    z[15]=z[9]*z[15];
    z[13]=z[13] + z[15];
    z[15]=z[11] - z[6];
    z[15]=z[2] - 2*z[15];
    z[15]=z[6]*z[15];
    z[12]=z[6] + z[12];
    z[12]=z[4]*z[12];
    z[12]=z[15] + z[12];
    z[15]=z[7] + z[2];
    z[15]=n<T>(2,3)*z[15];
    z[15]=z[11]*z[15];
    z[11]= - z[11] - z[2];
    z[11]=n<T>(1,3)*z[11] - z[14];
    z[11]=z[3]*z[11];
    z[11]=z[11] + z[15] + n<T>(2,3)*z[12] + n<T>(1,3)*z[13];
    z[11]= - n<T>(4,3)*z[10] + 2*z[11] + n<T>(11,9)*z[8];

    r += n<T>(1,3)*z[11];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCA_75(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCA_75(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
