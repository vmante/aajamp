#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_28(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[11];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[15];
    z[5]=c[3];
    z[6]=d[1];
    z[7]=e[2];
    z[8]=4*z[4] + z[2] + z[3];
    z[8]=i*z[1]*z[8];
    z[9]= - z[3]*z[2];
    z[10]=z[6] - z[3];
    z[10]=z[2] + 5*z[10];
    z[10]=z[6]*z[10];
    z[8]=4*z[7] + n<T>(5,3)*z[5] + z[9] + z[10] + z[8];

    r += n<T>(4,3)*z[8];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_28(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_28(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
