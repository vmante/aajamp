#include "qg_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qg_l2_tf_nfCF_14(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[35];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[11];
    z[5]=d[1];
    z[6]=d[2];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=e[12];
    z[10]=e[13];
    z[11]=c[3];
    z[12]=c[11];
    z[13]=c[15];
    z[14]=c[17];
    z[15]=c[31];
    z[16]=e[14];
    z[17]=f[50];
    z[18]=f[51];
    z[19]=f[53];
    z[20]=f[54];
    z[21]=f[55];
    z[22]=f[58];
    z[23]=f[59];
    z[24]= - z[3] + n<T>(2,9)*z[7];
    z[25]=n<T>(5,3)*z[6];
    z[26]=z[1]*i;
    z[27]=n<T>(5,3)*z[26] - z[25] + z[24];
    z[27]=z[7]*z[27];
    z[28]=2*z[26];
    z[29]=z[25] - z[3];
    z[29]=z[29]*z[28];
    z[30]=npow(z[6],2);
    z[31]= - z[3] + 2*z[6];
    z[32]=z[3]*z[31];
    z[27]=z[27] + z[29] + n<T>(1,3)*z[30] + z[32];
    z[27]=z[7]*z[27];
    z[29]= - 4*z[6] + 17*z[3];
    z[29]=z[3]*z[29];
    z[29]= - 13*z[30] + z[29];
    z[29]=z[3]*z[29];
    z[32]=npow(z[6],3);
    z[29]=n<T>(26,3)*z[32] + z[29];
    z[32]=122*z[6] + 7*z[3];
    z[32]=z[3]*z[32];
    z[32]= - 11*z[30] + n<T>(1,3)*z[32];
    z[32]=z[32]*z[26];
    z[33]=z[5] + z[2];
    z[34]= - 14*z[6] + 131*z[3];
    z[34]=n<T>(1,3)*z[34] + 2*z[7];
    z[34]= - n<T>(98,3)*z[4] + n<T>(2,3)*z[34] - z[33];
    z[34]=z[11]*z[34];
    z[27]=z[34] + 4*z[27] + n<T>(4,3)*z[29] + z[32];
    z[29]=z[6] + n<T>(17,9)*z[3];
    z[29]=n<T>(32,9)*z[8] + 4*z[29] - 5*z[26] + z[33];
    z[29]=z[10]*z[29];
    z[25]= - n<T>(2,3)*z[8] - z[25] - z[7];
    z[25]=n<T>(8,3)*z[25] - z[33];
    z[25]=z[4]*z[26]*z[25];
    z[32]=40*z[6] - 49*z[26];
    z[32]=n<T>(16,3)*z[8] + n<T>(1,3)*z[32] + 8*z[7];
    z[32]=n<T>(1,3)*z[32] + z[33];
    z[32]=z[9]*z[32];
    z[25]=z[29] + z[25] + z[32];
    z[29]=z[31]*z[26];
    z[32]=z[6] - 5*z[3];
    z[32]=z[3]*z[32];
    z[29]=z[29] + z[30] + z[32];
    z[26]=z[26] - z[6];
    z[32]=n<T>(4,3)*z[26] + z[7];
    z[32]=z[7]*z[32];
    z[29]=n<T>(1,3)*z[29] + z[32];
    z[24]=n<T>(28,27)*z[8] + z[24] + n<T>(1,9)*z[26];
    z[24]=z[8]*z[24];
    z[24]=n<T>(2,3)*z[29] + z[24];
    z[24]=z[8]*z[24];
    z[26]=z[28] - z[31];
    z[26]=z[26]*z[3];
    z[26]=z[26] + z[30];
    z[26]=z[26]*z[33];
    z[28]=z[7] + z[8];
    z[28]=z[16]*z[28];
    z[28]= - z[17] - z[18] + z[28] - z[23];
    z[29]= - 4*z[14] + n<T>(47,27)*z[12];
    z[29]=i*z[29];
    z[24]= - n<T>(4,3)*z[22] + n<T>(4,9)*z[21] - n<T>(8,9)*z[20] - 16*z[19] + 7*
    z[15] + n<T>(26,9)*z[13] + 4*z[24] + z[29] + z[26] + n<T>(1,3)*z[27] + 2*
    z[25] + n<T>(16,9)*z[28];

    r += n<T>(2,3)*z[24];
 
    return r;
}

template std::complex<double> qg_l2_tf_nfCF_14(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l2_tf_nfCF_14(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
