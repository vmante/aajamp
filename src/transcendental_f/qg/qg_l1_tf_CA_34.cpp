#include "qg_l1_tf_CA_decl.hpp"

template<class T>
std::complex<T> qg_l1_tf_CA_34(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[13];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[12];
    z[4]=c[3];
    z[5]=d[2];
    z[6]=d[3];
    z[7]=d[5];
    z[8]=e[0];
    z[9]=e[5];
    z[10]=z[5] - z[7];
    z[11]=z[1]*i;
    z[12]=z[6] - z[11] + z[10];
    z[12]=2*z[12] - z[2];
    z[12]=z[2]*z[12];
    z[11]=z[3]*z[11];
    z[11]=z[11] - z[9] - z[8];
    z[10]= - 2*z[10] - z[6];
    z[10]=z[6]*z[10];
    z[10]=z[12] + z[10] + n<T>(1,3)*z[4] + 2*z[11];

    r += 2*z[10];
 
    return r;
}

template std::complex<double> qg_l1_tf_CA_34(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_l1_tf_CA_34(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
