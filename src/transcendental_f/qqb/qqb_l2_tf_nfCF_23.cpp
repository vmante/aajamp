#include "qqb_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCF_23(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[52];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[4];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[16];
    z[9]=d[18];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[4];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=d[1];
    z[16]=d[5];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[18];
    z[20]=c[19];
    z[21]=c[21];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[29];
    z[27]=c[31];
    z[28]=d[8];
    z[29]=e[11];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[5];
    z[34]=f[8];
    z[35]=f[11];
    z[36]=f[13];
    z[37]=f[17];
    z[38]=f[18];
    z[39]=f[30];
    z[40]=f[41];
    z[41]=2*z[7];
    z[42]=z[3] - z[2];
    z[43]=z[42]*z[41];
    z[44]=z[1]*i;
    z[45]=2*z[44];
    z[46]=z[2] - 4*z[3];
    z[46]=z[46]*z[45];
    z[47]= - n<T>(20,3) + z[2];
    z[47]=z[2]*z[47];
    z[48]=z[2] - n<T>(10,3);
    z[49]= - 2*z[48] + z[3];
    z[49]=z[3]*z[49];
    z[50]=2*z[42] - z[5];
    z[50]=z[5]*z[50];
    z[43]=2*z[50] + z[46] + z[43] + z[47] + z[49];
    z[43]=z[5]*z[43];
    z[46]=5 - z[2];
    z[46]=z[2]*z[46];
    z[47]=z[3] + z[48];
    z[47]=z[3]*z[47];
    z[48]= - 5 - 7*z[2];
    z[48]=n<T>(1,3)*z[48] + z[3];
    z[48]=z[7]*z[48];
    z[46]=z[48] + n<T>(2,3)*z[46] + z[47];
    z[41]=z[46]*z[41];
    z[46]= - n<T>(40,3) - 17*z[2];
    z[46]=z[2]*z[46];
    z[47]=n<T>(20,3)*z[2] - z[3];
    z[47]=z[3]*z[47];
    z[48]=n<T>(7,3)*z[2] - z[3];
    z[48]=z[7]*z[48];
    z[46]=4*z[48] + z[46] + z[47];
    z[46]=z[46]*z[44];
    z[47]=2*z[2];
    z[48]=5 - z[47];
    z[48]=z[2]*z[48];
    z[49]= - 1 - z[2];
    z[49]=2*z[49] - z[3];
    z[49]=z[3]*z[49];
    z[48]=4*z[48] + 5*z[49];
    z[48]=z[3]*z[48];
    z[49]=npow(z[2],3);
    z[48]=23*z[49] + z[48];
    z[49]=z[45] - z[7];
    z[50]= - z[5] + z[3] - z[49];
    z[50]=2*z[50] + z[6];
    z[50]=z[6]*z[42]*z[50];
    z[41]=z[50] + z[43] + z[46] + n<T>(1,3)*z[48] + z[41];
    z[43]=n<T>(2,3)*z[4];
    z[46]=z[43] - n<T>(4,3)*z[44];
    z[48]=n<T>(1,3)*z[3];
    z[50]=n<T>(2,3)*z[7] + z[48] - z[2];
    z[46]=z[50]*z[46];
    z[50]=5*z[2] + 2*z[3];
    z[48]=z[50]*z[48];
    z[50]=npow(z[2],2);
    z[48]= - z[50] + z[48];
    z[51]=n<T>(4,9)*z[7] + n<T>(1,9)*z[2] - z[3];
    z[51]=z[7]*z[51];
    z[46]=n<T>(1,3)*z[48] + z[51] + z[46];
    z[46]=z[4]*z[46];
    z[41]= - z[27] + 2*z[46] + n<T>(1,3)*z[41];
    z[46]=z[5] + z[3] + n<T>(10,9);
    z[48]=z[44]*z[46];
    z[48]=z[14] + z[48];
    z[48]=z[8]*z[48];
    z[46]=z[44] - z[46];
    z[46]=z[12]*z[46];
    z[46]=z[48] + z[46];
    z[48]=z[16] + z[15];
    z[45]=z[45] - z[2];
    z[51]=z[3] - z[45];
    z[51]=n<T>(4,3)*z[51] + z[5];
    z[51]=4*z[51] + z[48];
    z[51]=z[10]*z[51];
    z[49]=z[2] - z[49];
    z[49]=n<T>(8,3)*z[49] + z[6];
    z[49]=4*z[49] + z[48];
    z[49]=z[13]*z[49];
    z[49]=z[51] + z[49];
    z[42]=z[42]*z[7];
    z[51]=z[3]*z[2];
    z[42]= - z[42] + z[51] - z[50];
    z[50]=n<T>(1,3)*z[14];
    z[42]=z[50] + 2*z[42];
    z[48]= - n<T>(1,3)*z[48];
    z[42]=z[42]*z[48];
    z[48]= - z[2] + z[7];
    z[48]=z[28]*z[48];
    z[44]=z[9]*z[44];
    z[44]= - z[18] - z[29] + z[48] + z[44];
    z[47]=n<T>(5,9) - z[47];
    z[47]= - n<T>(8,9)*z[7] + 2*z[47] - n<T>(31,9)*z[3];
    z[43]= - n<T>(4,3)*z[6] - z[43] + 2*z[47] + z[5];
    z[43]=z[43]*z[50];
    z[47]=z[40] + z[39] - z[38] + z[37] + z[30] + z[26];
    z[48]=z[35] - z[19];
    z[50]=z[36] + z[20];
    z[45]=z[4] - z[45];
    z[45]=z[11]*z[45];
    z[45]=z[45] + z[31];
    z[51]=n<T>(88,3)*z[24] + n<T>(38,3)*z[22] - n<T>(8,3)*z[21] - n<T>(7,9)*z[17];
    z[51]=i*z[51];
    z[41]=n<T>(16,3)*z[34] + n<T>(4,9)*z[33] + n<T>(8,9)*z[32] - n<T>(32,3)*z[25] - n<T>(34,9)*z[23] + z[43] + z[51] + z[42] + 2*z[41] + n<T>(2,3)*z[49] + 4*z[46]
    + n<T>(8,3)*z[45] + n<T>(20,9)*z[50] + n<T>(16,9)*z[48] + n<T>(40,9)*z[44] + n<T>(4,3)*
    z[47];

    r += 2*z[41];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCF_23(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCF_23(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
