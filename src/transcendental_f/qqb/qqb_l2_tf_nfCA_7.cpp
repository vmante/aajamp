#include "qqb_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCA_7(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[35];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[4];
    z[4]=d[17];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=e[9];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[17];
    z[13]=c[31];
    z[14]=e[6];
    z[15]=e[7];
    z[16]=f[30];
    z[17]=f[31];
    z[18]=f[35];
    z[19]=f[36];
    z[20]=f[39];
    z[21]=f[40];
    z[22]=z[7]*i;
    z[23]=z[5]*i;
    z[24]=n<T>(2,3)*z[6];
    z[25]=z[24] + z[3];
    z[25]=n<T>(8,3)*z[25];
    z[26]= - i*z[25];
    z[26]= - n<T>(4,9)*z[22] + z[26] - z[23];
    z[26]=z[1]*z[26];
    z[27]=z[1]*i;
    z[28]= - z[2]*z[27];
    z[26]=z[26] + z[28];
    z[26]=z[4]*z[26];
    z[28]=z[27] - z[7];
    z[29]=z[2] + z[5];
    z[25]= - z[25] - z[29] + n<T>(4,9)*z[28];
    z[25]=z[8]*z[25];
    z[28]=z[3] + z[6];
    z[28]=n<T>(8,9)*z[28] + z[29];
    z[28]=z[14]*z[28];
    z[25]=z[28] + z[26] + z[25];
    z[26]=2*z[6];
    z[28]=z[3] + z[26];
    z[28]=i*z[28];
    z[22]=4*z[22] + z[28] - z[23];
    z[28]=n<T>(2,3)*z[7];
    z[22]=z[22]*z[28];
    z[29]=4*z[6];
    z[30]=4*z[3];
    z[31]=z[30] + z[6];
    z[31]=z[31]*z[29];
    z[32]=npow(z[3],2);
    z[31]=19*z[32] + z[31];
    z[31]=i*z[31];
    z[33]=5*z[3];
    z[34]=z[33] - n<T>(4,3)*z[6];
    z[34]=i*z[34];
    z[23]=z[34] - n<T>(1,3)*z[23];
    z[23]=z[5]*z[23];
    z[22]=z[22] + n<T>(1,3)*z[31] + z[23];
    z[22]=z[1]*z[22];
    z[22]=z[22] + z[20];
    z[23]=n<T>(2,3)*z[3] + z[6];
    z[23]=z[23]*z[29];
    z[29]= - z[33] + n<T>(13,3)*z[6] + n<T>(1,3)*z[5];
    z[29]=z[5]*z[29];
    z[31]= - n<T>(8,3)*z[7] - z[3] + z[5];
    z[31]=z[7]*z[31];
    z[23]=n<T>(1,3)*z[31] + z[29] - n<T>(19,3)*z[32] + z[23];
    z[23]=z[23]*z[28];
    z[28]=n<T>(2,3)*z[5];
    z[24]= - z[28] + z[3] - z[24];
    z[24]=z[24]*z[28];
    z[28]=npow(z[6],2);
    z[24]=z[24] + n<T>(5,3)*z[32] - z[28];
    z[24]=z[5]*z[24];
    z[29]= - z[3] + z[6];
    z[29]=z[7]*z[29];
    z[27]=z[3]*z[27];
    z[27]=z[29] + z[27];
    z[27]=z[32] - z[28] + 2*z[27];
    z[27]=z[2]*z[27];
    z[28]= - z[30] - n<T>(11,3)*z[6];
    z[28]=z[6]*z[28];
    z[28]=2*z[32] + n<T>(1,3)*z[28];
    z[28]=z[6]*z[28];
    z[29]=npow(z[3],3);
    z[28]=n<T>(2,9)*z[29] + z[28];
    z[29]=z[21] + z[16];
    z[30]= - z[6] - z[5];
    z[30]=z[15]*z[30];
    z[30]=z[30] - z[18];
    z[26]=z[26] - z[5];
    z[26]= - 4*z[4] + n<T>(4,3)*z[26] + 5*z[7];
    z[26]=z[9]*z[26];
    z[26]=z[26] + z[17];
    z[31]= - 4*z[12] - n<T>(2,27)*z[10];
    z[31]=i*z[31];
    z[22]= - n<T>(14,9)*z[19] + z[13] - n<T>(10,9)*z[11] + z[27] + z[23] + n<T>(4,3)
   *z[28] + z[24] + z[31] + n<T>(2,3)*z[22] + n<T>(2,9)*z[26] + n<T>(8,9)*z[30] - 
   n<T>(16,9)*z[29] + 2*z[25];

    r += n<T>(1,3)*z[22];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCA_7(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCA_7(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
