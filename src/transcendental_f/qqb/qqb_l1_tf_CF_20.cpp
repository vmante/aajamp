#include "qqb_l1_tf_CF_decl.hpp"

template<class T>
std::complex<T> qqb_l1_tf_CF_20(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[20];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[4];
    z[4]=d[15];
    z[5]=d[17];
    z[6]=c[3];
    z[7]=d[1];
    z[8]=d[7];
    z[9]=d[6];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[6];
    z[13]=e[9];
    z[14]= - z[2] - z[3] + z[5] + z[4];
    z[14]=z[1]*i*z[14];
    z[14]=z[14] + z[10] + z[11] + z[13] - z[12];
    z[15]=npow(z[9],2);
    z[16]=2*z[8];
    z[17]= - z[9]*z[16];
    z[18]=npow(z[7],2);
    z[16]=z[16] - z[3];
    z[16]=z[3]*z[16];
    z[19]= - 2*z[7] + z[2];
    z[19]=z[2]*z[19];
    z[14]=z[19] + z[16] + z[18] + z[17] + z[15] - n<T>(1,3)*z[6] + 2*z[14];

    r += 4*z[14];
 
    return r;
}

template std::complex<double> qqb_l1_tf_CF_20(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l1_tf_CF_20(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
