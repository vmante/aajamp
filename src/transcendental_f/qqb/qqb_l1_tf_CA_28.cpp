#include "qqb_l1_tf_CA_decl.hpp"

template<class T>
std::complex<T> qqb_l1_tf_CA_28(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[18];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[17];
    z[4]=c[3];
    z[5]=d[6];
    z[6]=d[1];
    z[7]=d[4];
    z[8]=e[3];
    z[9]=e[6];
    z[10]=e[8];
    z[11]=e[9];
    z[12]=z[11] + z[10] + z[9] - z[8];
    z[13]=z[3] - z[2];
    z[13]= - 2*z[13];
    z[13]=z[13]*i*z[1];
    z[14]=npow(z[5],2);
    z[15]=2*z[5];
    z[16]=z[15] - z[2];
    z[16]=z[2]*z[16];
    z[17]= - z[15] + z[6];
    z[17]=z[6]*z[17];
    z[15]=z[15] - z[7];
    z[15]=z[7]*z[15];
    z[12]=n<T>(2,3)*z[4] + z[15] + z[17] - z[14] + z[16] + z[13] - 2*z[12];

    r += 2*z[12];
 
    return r;
}

template std::complex<double> qqb_l1_tf_CA_28(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l1_tf_CA_28(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
