#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_12(
  const std::array<T, 85>& c
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[3];
  std::complex<T> r(0,0);

    z[1]=c[3];

    r += n<T>(1,108)*z[1];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_12(
  const std::array<double,85>& c
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_12(
  const std::array<dd_real,85>& c
);
#endif
