#include "qqb_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCA_36(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[9];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=c[13];
    z[4]=c[20];
    z[5]=c[23];
    z[6]=f[62];
    z[7]= - z[6] + n<T>(2,3)*z[2];
    z[8]=n<T>(1,5)*z[5] - 4*z[4];
    z[8]=n<T>(11,405)*z[1] + n<T>(1,3)*z[8] - n<T>(16,5)*z[3];
    z[8]=i*z[8];
    z[7]=n<T>(2,3)*z[7] + z[8];

    r += 2*z[7];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCA_36(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCA_36(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
