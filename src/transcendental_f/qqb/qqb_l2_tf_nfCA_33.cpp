#include "qqb_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCA_33(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[14];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=c[3];
    z[6]=c[11];
    z[7]=c[31];
    z[8]=d[3];
    z[9]=d[6];
    z[10]=2*z[2];
    z[11]=i*z[1];
    z[12]=n<T>(68,9) - z[5];
    z[12]= - z[10] + n<T>(1,3)*z[12] + 4*z[11];
    z[12]=z[12]*z[10];
    z[11]=z[11] + n<T>(1,9)*z[5];
    z[10]=n<T>(11,9)*z[3] - z[10] + n<T>(20,9)*z[4] + z[8] + z[11];
    z[10]=z[3]*z[10];
    z[11]=n<T>(71,9)*z[4] - z[8] + z[11];
    z[11]=z[4]*z[11];
    z[10]=z[11] + z[10];
    z[11]= - n<T>(13,9) - 16*z[9];
    z[11]=5*z[11] + 13*z[7];
    z[11]=4*z[11] + n<T>(155,3)*z[5];
    z[13]=z[6] - n<T>(68,9)*z[1];
    z[13]=i*z[13];
    z[11]=n<T>(1,3)*z[11] + 2*z[13];

    r += 2*z[10] + n<T>(1,3)*z[11] + z[12];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCA_33(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCA_33(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d
);
#endif
