#ifndef QQB_L2_TF_NFNFAA_DECL_H
#define QQB_L2_TF_NFNFAA_DECL_H

#include <array>
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
std::complex<T> qqb_l2_tf_nfnfaa_1(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfnfaa_2(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfnfaa_3(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfnfaa_4(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfnfaa_5(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfnfaa_6(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfnfaa_7(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

#endif /* QQB_L2_TF_NFNFAA_DECL_H */
