#include "qqb_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCA_14(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[38];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[8];
    z[5]=d[5];
    z[6]=d[7];
    z[7]=d[15];
    z[8]=d[18];
    z[9]=d[3];
    z[10]=e[2];
    z[11]=e[10];
    z[12]=e[11];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[17];
    z[17]=c[31];
    z[18]=f[4];
    z[19]=f[6];
    z[20]=f[7];
    z[21]=f[10];
    z[22]=f[23];
    z[23]=f[78];
    z[24]=z[9] + z[6];
    z[25]=z[24] - z[2];
    z[26]=z[1]*i;
    z[27]=8*z[26];
    z[25]=z[27] - 5*z[3] - 16*z[4] - 8*z[25];
    z[25]=n<T>(1,3)*z[25] + z[5];
    z[25]=z[11]*z[25];
    z[28]=4*z[4];
    z[29]= - z[5] + 4*z[6] + z[28] - z[3];
    z[30]=z[26]*z[29];
    z[30]=4*z[13] + z[30];
    z[30]=z[8]*z[30];
    z[31]=4*z[26];
    z[29]=z[31] - z[29];
    z[29]=z[12]*z[29];
    z[25]=z[29] + z[25] + z[30];
    z[29]=2*z[3];
    z[30]=z[29] - z[4];
    z[32]=2*z[26];
    z[33]=z[32] + z[30];
    z[33]=z[4]*z[33];
    z[34]=z[4] - z[3];
    z[24]= - z[34]*z[24];
    z[35]=npow(z[3],2);
    z[24]= - z[35] + z[24] + z[33];
    z[24]=z[9]*z[24];
    z[24]= - z[21] + z[24] + z[22];
    z[33]=z[4] + z[3];
    z[36]=z[33]*z[28];
    z[37]= - z[3] - 2*z[4];
    z[31]=z[37]*z[31];
    z[31]=z[31] - 11*z[35] + z[36];
    z[36]=7*z[3] - z[28];
    z[27]=n<T>(1,3)*z[36] - z[27];
    z[27]=z[6]*z[27];
    z[27]=n<T>(2,3)*z[31] + z[27];
    z[27]=z[6]*z[27];
    z[31]=z[26] - z[9];
    z[36]=z[6] - z[31];
    z[36]=z[34]*z[36];
    z[33]= - z[4]*z[33];
    z[33]=2*z[35] + z[33] + z[36];
    z[34]= - z[2]*z[34];
    z[33]=2*z[33] + z[34];
    z[33]=z[2]*z[33];
    z[34]=z[26]*z[29];
    z[29]= - z[29] + z[6];
    z[29]=z[6]*z[29];
    z[29]=z[29] + z[35] + z[34];
    z[29]=z[5]*z[29];
    z[34]=z[9] + z[3];
    z[26]= - z[26]*z[34];
    z[26]= - z[13] + z[26];
    z[26]=z[7]*z[26];
    z[31]= - z[3] + z[31];
    z[31]=z[10]*z[31];
    z[26]= - z[18] + z[26] + z[31];
    z[31]=z[3] - n<T>(2,3)*z[4];
    z[31]=z[4]*z[31];
    z[31]= - z[35] + z[31];
    z[31]=z[4]*z[31];
    z[34]=npow(z[3],3);
    z[31]=n<T>(25,3)*z[34] + 8*z[31];
    z[30]=z[4]*z[30];
    z[30]=5*z[35] - n<T>(4,3)*z[30];
    z[30]=z[30]*z[32];
    z[28]= - n<T>(1,3)*z[5] - n<T>(4,9)*z[9] - n<T>(28,3)*z[6] + n<T>(1,9)*z[3] + z[28];
    z[28]=z[13]*z[28];
    z[32]=z[14]*i;
    z[34]=4*i;
    z[34]= - z[16]*z[34];
    z[24]=16*z[23] - n<T>(64,3)*z[20] - 4*z[19] + z[17] + z[34] - n<T>(14,3)*
    z[15] - n<T>(10,9)*z[32] + z[28] + z[29] + n<T>(4,3)*z[33] + z[27] + n<T>(1,3)*
    z[31] + z[30] + n<T>(16,3)*z[26] + 2*z[25] + n<T>(8,3)*z[24];

    r += n<T>(1,3)*z[24];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCA_14(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCA_14(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
