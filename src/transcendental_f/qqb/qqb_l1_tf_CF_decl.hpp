#ifndef QQB_L1_TF_CF_DECL_H
#define QQB_L1_TF_CF_DECL_H

#include <array>
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
std::complex<T> qqb_l1_tf_CF_1(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_2(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_3(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_4(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_5(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_6(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_7(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_8(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_9(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_10(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_11(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_12(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_13(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_14(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_15(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_16(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_17(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_18(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_19(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_20(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_21(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_22(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_23(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_24(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_26(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_27(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_28(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_29(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_30(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_31(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_32(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_33(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_34(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_35(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l1_tf_CF_36(
  const std::array<std::complex<T>,19>&);

#endif /* QQB_L1_TF_CF_DECL_H */
