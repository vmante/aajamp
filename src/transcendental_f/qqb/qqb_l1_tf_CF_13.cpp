#include "qqb_l1_tf_CF_decl.hpp"

template<class T>
std::complex<T> qqb_l1_tf_CF_13(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[21];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[6];
    z[4]=d[12];
    z[5]=d[16];
    z[6]=c[3];
    z[7]=d[5];
    z[8]=d[2];
    z[9]=d[3];
    z[10]=d[4];
    z[11]=e[1];
    z[12]=e[4];
    z[13]=e[5];
    z[14]=e[6];
    z[15]=z[10] - z[8];
    z[15]=z[3]*z[15];
    z[16]=z[2] + z[3] - z[5] - z[4];
    z[16]=z[1]*i*z[16];
    z[15]=z[16] + z[15] - z[11] + z[12] - z[14] + z[13];
    z[16]=npow(z[10],2);
    z[17]=npow(z[9],2);
    z[18]=npow(z[8],2);
    z[19]=2*z[7];
    z[20]= - z[9]*z[19];
    z[19]=z[19] - z[2];
    z[19]=z[2]*z[19];
    z[15]=z[19] + z[20] + z[18] + z[17] - z[16] - n<T>(1,3)*z[6] + 2*z[15];

    r += 4*z[15];
 
    return r;
}

template std::complex<double> qqb_l1_tf_CF_13(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l1_tf_CF_13(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
