#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_43(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,82>& f,
  const std::array<std::complex<T>,330>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[97];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[69];
    z[3]=f[70];
    z[4]=f[77];
    z[5]=f[79];
    z[6]=f[81];
    z[7]=c[11];
    z[8]=d[0];
    z[9]=d[1];
    z[10]=d[3];
    z[11]=d[4];
    z[12]=d[5];
    z[13]=d[6];
    z[14]=d[7];
    z[15]=d[8];
    z[16]=d[9];
    z[17]=c[12];
    z[18]=c[13];
    z[19]=c[20];
    z[20]=c[23];
    z[21]=c[32];
    z[22]=c[33];
    z[23]=c[36];
    z[24]=c[38];
    z[25]=c[40];
    z[26]=c[43];
    z[27]=c[47];
    z[28]=c[48];
    z[29]=f[24];
    z[30]=f[44];
    z[31]=f[80];
    z[32]=f[22];
    z[33]=f[42];
    z[34]=f[62];
    z[35]=d[2];
    z[36]=g[24];
    z[37]=g[27];
    z[38]=g[38];
    z[39]=g[40];
    z[40]=g[48];
    z[41]=g[50];
    z[42]=g[79];
    z[43]=g[82];
    z[44]=g[90];
    z[45]=g[92];
    z[46]=g[99];
    z[47]=g[101];
    z[48]=g[138];
    z[49]=g[147];
    z[50]=g[170];
    z[51]=g[173];
    z[52]=g[181];
    z[53]=g[183];
    z[54]=g[189];
    z[55]=g[191];
    z[56]=g[198];
    z[57]=g[200];
    z[58]=g[201];
    z[59]=g[212];
    z[60]=g[214];
    z[61]=g[220];
    z[62]=g[243];
    z[63]=g[245];
    z[64]=g[251];
    z[65]=g[254];
    z[66]=g[270];
    z[67]=g[272];
    z[68]=g[276];
    z[69]=g[278];
    z[70]=g[281];
    z[71]=g[288];
    z[72]=g[289];
    z[73]=g[290];
    z[74]=g[292];
    z[75]=g[294];
    z[76]=g[308];
    z[77]=z[4] - z[6];
    z[78]=n<T>(7,8)*z[5];
    z[79]=n<T>(1,2)*z[2];
    z[77]=z[78] + z[79] - n<T>(1,2)*z[3] + n<T>(2,3)*z[77];
    z[77]=z[1]*z[77];
    z[80]= - n<T>(7,4)*z[14] + 2*z[10];
    z[81]=n<T>(4,81)*z[8] - n<T>(1,27)*z[80];
    z[81]=z[7]*z[81];
    z[82]= - 2*z[19] + n<T>(1,10)*z[20];
    z[83]=n<T>(13,810)*z[7] - n<T>(24,5)*z[18] + z[82];
    z[83]=z[16]*z[83];
    z[82]= - n<T>(4,135)*z[7] + n<T>(1,3)*z[82] - n<T>(8,5)*z[18];
    z[82]=z[11]*z[82];
    z[84]= - n<T>(1,4)*z[20] + 5*z[19];
    z[84]= - n<T>(1,16)*z[7] + n<T>(1,4)*z[84] + 3*z[18];
    z[84]=z[15]*z[84];
    z[85]= - z[19] + n<T>(1,20)*z[20];
    z[85]= - n<T>(1,5)*z[18] + n<T>(1,12)*z[85];
    z[86]= - 23*z[85] - n<T>(373,6480)*z[7];
    z[86]=z[13]*z[86];
    z[87]=n<T>(469,6480)*z[7] - z[85];
    z[87]=z[12]*z[87];
    z[85]=7*z[85] + n<T>(137,6480)*z[7];
    z[85]=z[9]*z[85];
    z[77]=z[85] + z[87] + z[86] + z[84] + z[82] + z[83] + n<T>(11,24)*z[22]
    - n<T>(4,81)*z[27] - n<T>(21,4)*z[24] + z[77] + z[81];
    z[77]=i*z[77];
    z[81]=z[33] + 7*z[32];
    z[81]=n<T>(1,8)*z[81];
    z[82]=z[81] - z[34];
    z[83]=z[6] + z[82];
    z[84]=n<T>(7,8)*z[31];
    z[85]=n<T>(1,8)*z[29];
    z[78]=z[78] - n<T>(5,12)*z[17] - z[30] + z[85] - z[84] - z[83];
    z[78]=z[13]*z[78];
    z[86]=n<T>(1,2)*z[31];
    z[87]=n<T>(1,2)*z[29];
    z[88]=z[79] + 4*z[17] + n<T>(1,2)*z[30] - z[87] - 2*z[34] - z[86];
    z[88]=z[16]*z[88];
    z[89]=n<T>(1,8)*z[30];
    z[83]=n<T>(19,8)*z[5] + n<T>(5,8)*z[2] + n<T>(7,4)*z[17] - z[89] + z[86] - z[83]
   ;
    z[83]=z[15]*z[83];
    z[86]= - n<T>(1,8)*z[35] - z[14];
    z[86]=z[2]*z[86];
    z[78]= - z[63] + z[86] + z[88] + z[83] + z[78];
    z[83]=n<T>(1,3)*z[4];
    z[86]= - z[83] + n<T>(1,3)*z[6];
    z[88]=n<T>(1,24)*z[30];
    z[90]=n<T>(1,3)*z[3];
    z[91]= - n<T>(1,12)*z[29] + z[90] - n<T>(1,4)*z[31];
    z[91]= - n<T>(1,8)*z[2] - n<T>(4,3)*z[17] - z[88] + n<T>(1,2)*z[91] - z[86];
    z[91]=z[8]*z[91];
    z[81]=z[34] + z[81] + z[84];
    z[81]= - n<T>(11,24)*z[5] + n<T>(1,12)*z[2] - n<T>(73,36)*z[17] + n<T>(1,6)*z[30]
     + 
    z[83] + n<T>(1,3)*z[81] + z[85];
    z[81]=z[12]*z[81];
    z[83]=n<T>(25,3)*z[17] + z[30] - z[3] - z[29];
    z[79]=n<T>(1,3)*z[83] + z[79];
    z[79]=z[11]*z[79];
    z[79]=z[79] - z[76];
    z[83]=z[10] + z[14] + z[35];
    z[83]=z[83]*z[86];
    z[84]=z[8] + z[11];
    z[85]=n<T>(1,2)*z[10] - z[35];
    z[84]=n<T>(1,3)*z[85] - n<T>(7,8)*z[14] + n<T>(1,6)*z[84];
    z[84]=z[5]*z[84];
    z[82]=z[4] + z[87] - z[3] + z[82];
    z[82]=n<T>(1,12)*z[5] - n<T>(5,24)*z[2] - n<T>(1,18)*z[17] + n<T>(1,3)*z[82]
     - z[89]
   ;
    z[82]=z[9]*z[82];
    z[85]=z[48] - z[59] + z[60] - z[61] + z[74] + z[75];
    z[86]= - z[43] - z[45] + z[54] + z[70] - z[37] - z[38] + z[41] - 
    z[42];
    z[87]=z[73] + z[44] - z[58];
    z[89]=z[69] + z[66] + z[68];
    z[92]=z[57] - z[72];
    z[93]=z[56] - z[71];
    z[94]=z[50] + z[53];
    z[95]=z[36] + z[39];
    z[88]=z[88] + n<T>(1,24)*z[29];
    z[88]=z[35]*z[88];
    z[96]= - z[10] + n<T>(7,4)*z[35];
    z[96]=z[31]*z[96];
    z[90]=z[14]*z[90];
    z[80]=z[17]*z[80];
    z[77]=z[77] + z[82] + z[81] + z[84] + z[91] + z[80] + z[90] + n<T>(1,6)*
    z[96] - n<T>(23,108)*z[21] + 3*z[23] - n<T>(5,2)*z[25] - n<T>(3,4)*z[26] + n<T>(4,3)
   *z[28] + n<T>(7,36)*z[40] - n<T>(1,36)*z[46] + n<T>(5,36)*z[47] + n<T>(2,9)*z[49]
     - 
   n<T>(3,16)*z[51] - n<T>(11,16)*z[52] - n<T>(17,72)*z[55] + n<T>(7,4)*z[62] - n<T>(1,24)*
    z[64] - n<T>(7,24)*z[65] - n<T>(67,48)*z[67] + z[88] + z[83] + n<T>(1,2)*z[79]
    - n<T>(7,12)*z[95] + n<T>(17,48)*z[94] + n<T>(1,16)*z[93] + n<T>(1,8)*z[92] + n<T>(19,48)*z[89]
     - n<T>(1,4)*z[87]
     + n<T>(1,3)*z[78] - n<T>(1,12)*z[86] - n<T>(2,3)*z[85];

    r += z[77];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_43(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,82>& f,
  const std::array<std::complex<double>,330>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_43(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,82>& f,
  const std::array<std::complex<dd_real>,330>& g
);
#endif
