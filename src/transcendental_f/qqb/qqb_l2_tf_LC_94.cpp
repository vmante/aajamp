#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_94(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[78];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=e[6];
    z[30]=e[14];
    z[31]=f[4];
    z[32]=f[6];
    z[33]=f[7];
    z[34]=f[12];
    z[35]=f[16];
    z[36]=f[23];
    z[37]=f[30];
    z[38]=f[31];
    z[39]=f[32];
    z[40]=f[33];
    z[41]=f[35];
    z[42]=f[36];
    z[43]=f[39];
    z[44]=f[43];
    z[45]=f[50];
    z[46]=f[51];
    z[47]=f[55];
    z[48]=f[56];
    z[49]=f[58];
    z[50]=n<T>(1,2)*z[6];
    z[51]=z[1]*i;
    z[52]=z[50] + z[51];
    z[53]=z[6]*z[52];
    z[54]=n<T>(1,3)*z[51];
    z[55]=z[54] + z[6];
    z[56]=n<T>(1,6)*z[7];
    z[57]=z[56] - z[55];
    z[57]=z[7]*z[57];
    z[53]=z[53] + z[57];
    z[57]=5*z[6];
    z[58]=n<T>(5,3)*z[7] - n<T>(7,3)*z[51] + z[57];
    z[59]=n<T>(1,3)*z[9];
    z[58]=z[4] + n<T>(1,2)*z[58] + z[59];
    z[60]=n<T>(5,3)*z[3];
    z[61]=n<T>(1,3)*z[8];
    z[58]= - z[61] - n<T>(1,3)*z[5] + n<T>(1,2)*z[58] - z[60];
    z[58]=z[8]*z[58];
    z[62]= - z[51] + n<T>(1,2)*z[9];
    z[63]=z[62]*z[59];
    z[64]=n<T>(1,2)*z[4];
    z[65]=z[51] - z[9];
    z[66]=z[64] + z[65];
    z[66]=z[4]*z[66];
    z[67]=z[51] - z[7];
    z[68]= - n<T>(1,2)*z[3] - z[67];
    z[60]=z[68]*z[60];
    z[68]=n<T>(2,3)*z[5];
    z[69]= - z[65]*z[68];
    z[53]=z[58] + z[69] + z[60] + z[66] + n<T>(5,2)*z[53] + z[63];
    z[53]=z[53]*z[61];
    z[58]=n<T>(1,2)*z[15];
    z[60]=z[6] + z[65];
    z[60]=z[60]*z[58];
    z[61]=z[4]*z[6];
    z[63]=npow(z[6],2);
    z[66]=z[7]*z[6];
    z[62]= - z[9]*z[62];
    z[69]=z[4] + z[9] - z[6] - z[7];
    z[69]=z[3]*z[69];
    z[60]=z[60] + z[69] - z[61] + z[62] + z[63] + z[66];
    z[62]=n<T>(1,3)*z[15];
    z[60]=z[60]*z[62];
    z[69]=z[9] + z[7];
    z[70]=z[69] - z[51];
    z[71]=2*z[4];
    z[72]=z[71] - z[70];
    z[72]=z[4]*z[72];
    z[73]= - 2*z[3] + z[6] + z[70];
    z[73]=z[3]*z[73];
    z[74]= - z[5] - z[4] + z[70];
    z[74]=z[5]*z[74];
    z[75]=z[51] + z[6];
    z[76]= - z[9] + z[75];
    z[76]=z[6]*z[76];
    z[72]=z[74] + z[73] + z[72] - z[66] + z[76];
    z[73]= - z[5] - z[3] + z[6] + z[4];
    z[73]=z[2]*z[73];
    z[72]=2*z[72] + z[73];
    z[72]=z[2]*z[72];
    z[73]=2*z[9];
    z[67]= - z[73] + z[67];
    z[67]=z[67]*z[73];
    z[74]=z[7] + z[71];
    z[71]=z[74]*z[71];
    z[74]=2*z[51];
    z[69]= - n<T>(7,3)*z[5] + 5*z[4] - z[74] - z[69];
    z[69]=z[5]*z[69];
    z[76]=z[74] - z[7];
    z[76]=z[7]*z[76];
    z[67]=z[69] + z[71] + z[76] + z[67];
    z[67]=z[5]*z[67];
    z[67]=z[67] + z[72];
    z[69]=2*z[7];
    z[57]=z[73] + z[69] - z[74] + z[57];
    z[57]= - n<T>(4,3)*z[2] + n<T>(2,3)*z[57] + 5*z[3];
    z[57]=z[21]*z[57];
    z[71]=z[11] + z[15];
    z[72]= - z[51]*z[71];
    z[72]= - z[24] + z[72];
    z[72]=z[12]*z[72];
    z[71]= - z[51] + z[71];
    z[71]=z[23]*z[71];
    z[73]= - z[6] - z[15];
    z[73]=z[30]*z[73];
    z[57]= - z[57] - z[72] - z[71] - z[73] + z[45] - z[39];
    z[71]=z[51]*z[9];
    z[71]=z[71] + z[24];
    z[72]=z[5]*z[51];
    z[72]=z[72] + z[71];
    z[72]=z[14]*z[72];
    z[73]=z[51]*z[6];
    z[71]= - z[73] - z[71];
    z[71]=z[16]*z[71];
    z[76]=z[5] - z[65];
    z[76]=z[20]*z[76];
    z[77]=z[6] - z[65];
    z[77]=z[22]*z[77];
    z[71]=z[72] + z[71] + z[76] + z[77];
    z[72]=5*z[51];
    z[76]= - z[72] - z[50];
    z[76]=z[76]*z[50];
    z[56]= - z[56] + n<T>(4,3)*z[51] + z[6];
    z[56]=z[7]*z[56];
    z[77]=n<T>(7,2)*z[9] - 7*z[51] + z[69];
    z[77]=z[77]*z[59];
    z[56]=z[77] + z[76] + z[56];
    z[55]= - n<T>(17,27)*z[4] + n<T>(1,6)*z[9] + n<T>(1,4)*z[55] - n<T>(4,9)*z[7];
    z[55]=z[4]*z[55];
    z[55]=n<T>(1,3)*z[56] + z[55];
    z[55]=z[4]*z[55];
    z[56]=n<T>(1,3)*z[6];
    z[76]=z[74] + n<T>(7,2)*z[6];
    z[76]=z[76]*z[56];
    z[77]=4*z[51] + 7*z[6];
    z[77]=n<T>(1,3)*z[77] - z[69];
    z[77]=z[7]*z[77];
    z[69]= - n<T>(5,2)*z[9] - z[69] + z[72] - 2*z[6];
    z[69]=z[69]*z[59];
    z[61]= - z[61] + z[69] + z[76] + z[77];
    z[69]=n<T>(11,4) - n<T>(7,9)*z[51];
    z[69]=n<T>(26,27)*z[3] - n<T>(1,18)*z[9] + n<T>(1,2)*z[69] - n<T>(8,9)*z[6];
    z[69]=z[3]*z[69];
    z[61]=n<T>(1,3)*z[61] + z[69];
    z[61]=z[3]*z[61];
    z[52]=z[52] - z[11];
    z[52]=z[62] - z[64] - n<T>(1,3)*z[52];
    z[52]=z[11]*z[52];
    z[50]=z[51] - z[50];
    z[50]=z[50]*z[56];
    z[62]= - z[64] + z[75];
    z[62]=z[4]*z[62];
    z[50]=z[52] + z[50] + z[62];
    z[52]= - z[15]*z[56];
    z[50]=z[52] + n<T>(1,2)*z[50];
    z[50]=z[11]*z[50];
    z[52]=z[74] - z[6];
    z[52]=z[6]*z[52];
    z[52]=z[52] + z[66];
    z[56]=z[56] - z[65];
    z[56]=z[9]*z[56];
    z[52]=n<T>(2,3)*z[52] + z[56];
    z[52]=z[52]*z[59];
    z[56]= - z[4] - z[7];
    z[56]=z[54]*z[56];
    z[59]=n<T>(1,3)*z[24];
    z[56]= - z[59] - n<T>(11,4)*z[51] + z[56];
    z[56]=z[13]*z[56];
    z[62]=z[70] - z[2];
    z[62]= - 5*z[5] - 7*z[4] - 2*z[62];
    z[62]=z[19]*z[62];
    z[64]= - z[5] - z[8];
    z[64]=z[29]*z[64];
    z[62]= - z[36] - z[44] + z[62] + z[64] + z[41] + z[37];
    z[64]= - 19*z[51] - n<T>(29,2)*z[6];
    z[64]=z[6]*z[64];
    z[54]= - z[54] + n<T>(11,4) + n<T>(1,3)*z[7];
    z[65]=n<T>(19,18)*z[6] + z[54];
    z[65]=z[7]*z[65];
    z[64]=n<T>(1,9)*z[64] + z[65];
    z[64]=z[7]*z[64];
    z[64]=z[64] + z[32];
    z[51]=n<T>(8,9)*z[51];
    z[65]= - n<T>(1,54)*z[6] - n<T>(11,4) + z[51];
    z[63]=z[65]*z[63];
    z[65]= - 43*z[6] - n<T>(13,2)*z[7];
    z[65]= - n<T>(7,3)*z[8] + z[68] + n<T>(41,12)*z[3] - n<T>(7,4)*z[4] + n<T>(1,6)*
    z[65] - z[9];
    z[58]= - n<T>(1,4)*z[11] + n<T>(1,3)*z[65] + z[58];
    z[58]=z[58]*z[59];
    z[59]=z[3] + z[7];
    z[65]=z[51]*z[59];
    z[65]=n<T>(8,9)*z[24] - z[73] + z[65];
    z[65]=z[10]*z[65];
    z[51]= - z[51] - z[6] + n<T>(8,9)*z[59];
    z[51]=z[17]*z[51];
    z[59]=z[31] + z[49] - z[46];
    z[66]=z[40] + z[33];
    z[68]=z[48] - z[43];
    z[54]=n<T>(1,3)*z[4] + z[54];
    z[54]=z[18]*z[54];
    z[69]=z[3] + z[8];
    z[69]=z[28]*z[69];
    z[70]=z[25]*i;

    r += n<T>(5,18)*z[26] - n<T>(25,4)*z[27] - n<T>(5,36)*z[34] + n<T>(5,12)*z[35]
     + n<T>(11,18)*z[38] - n<T>(1,18)*z[42]
     + n<T>(1,12)*z[47]
     + z[50]
     + z[51]
     + z[52]
       + z[53] + z[54] + z[55] + z[56] - n<T>(1,3)*z[57] + z[58] + n<T>(1,4)*
      z[59] + z[60] + z[61] + n<T>(2,9)*z[62] + z[63] + n<T>(1,2)*z[64] + z[65]
       + n<T>(16,9)*z[66] + n<T>(1,9)*z[67] - n<T>(1,6)*z[68] + n<T>(5,9)*z[69] + n<T>(29,108)*z[70]
     + n<T>(2,3)*z[71];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_94(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_94(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
