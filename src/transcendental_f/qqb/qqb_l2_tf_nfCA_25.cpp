#include "qqb_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCA_25(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[82];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[4];
    z[10]=d[9];
    z[11]=d[11];
    z[12]=d[15];
    z[13]=d[18];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[8];
    z[18]=e[10];
    z[19]=e[11];
    z[20]=e[12];
    z[21]=c[3];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[17];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[30];
    z[31]=c[31];
    z[32]=e[3];
    z[33]=e[14];
    z[34]=e[13];
    z[35]=f[0];
    z[36]=f[2];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[6];
    z[41]=f[7];
    z[42]=f[10];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[23];
    z[48]=f[50];
    z[49]=f[51];
    z[50]=f[54];
    z[51]=f[55];
    z[52]=f[56];
    z[53]=f[58];
    z[54]=f[59];
    z[55]=z[2] - z[7];
    z[56]=5*z[8];
    z[57]=z[1]*i;
    z[58]= - z[5] + z[55] + z[57] - z[56];
    z[59]=67*z[3];
    z[58]=z[59] - 16*z[58];
    z[58]=n<T>(1,3)*z[58] + z[6];
    z[58]=z[18]*z[58];
    z[60]=z[6] + z[3];
    z[61]=8*z[2];
    z[62]=16*z[57];
    z[63]= - 8*z[4] - 4*z[10] - z[61] + z[62] - z[60];
    z[63]=z[14]*z[63];
    z[64]=n<T>(4,9)*z[10];
    z[65]=z[60] + z[64] - n<T>(8,9)*z[4];
    z[66]= - n<T>(8,3)*z[8] - z[65];
    z[66]=z[57]*z[66];
    z[66]=n<T>(8,9)*z[21] + z[66];
    z[66]=z[11]*z[66];
    z[67]=n<T>(1,3)*z[57];
    z[68]=z[67] + z[8];
    z[65]=n<T>(8,3)*z[68] + z[65];
    z[65]=z[20]*z[65];
    z[69]=z[10] + z[8];
    z[60]= - z[60] - n<T>(64,9)*z[69];
    z[60]=z[33]*z[60];
    z[58]=z[60] + z[58] + z[66] + z[63] + z[65];
    z[60]=z[67] - z[8];
    z[63]=n<T>(2,3)*z[10];
    z[65]=n<T>(1,3)*z[4];
    z[66]= - n<T>(2,3)*z[6] + z[65] - z[63] - z[60];
    z[66]=z[6]*z[66];
    z[62]=z[62] + z[56];
    z[67]=n<T>(1,3)*z[8];
    z[62]=z[62]*z[67];
    z[69]=z[3] - z[8];
    z[70]=z[2] - n<T>(16,9)*z[57] - z[69];
    z[71]=2*z[10];
    z[70]=z[70]*z[71];
    z[72]= - 8*z[68] + n<T>(7,3)*z[10];
    z[72]=2*z[72] + n<T>(17,3)*z[4];
    z[72]=z[72]*z[65];
    z[73]=npow(z[3],2);
    z[74]=2*z[57];
    z[75]=z[74] - z[2];
    z[76]=z[2]*z[75];
    z[62]=n<T>(8,3)*z[66] + z[72] + z[70] + z[76] + z[62] + z[73];
    z[62]=z[6]*z[62];
    z[66]=8*z[8];
    z[70]=z[57] + z[8];
    z[70]=z[70]*z[66];
    z[72]=5*z[57];
    z[66]= - 16*z[3] - z[72] + z[66];
    z[66]=z[3]*z[66];
    z[73]=z[74] - z[7];
    z[76]=z[69] - z[73];
    z[77]=8*z[7];
    z[76]=z[76]*z[77];
    z[66]=z[76] + z[70] + z[66];
    z[70]=17*z[57];
    z[76]=2*z[8];
    z[78]=z[70] + z[76];
    z[77]= - 28*z[2] + z[77] + 4*z[78] - 11*z[3];
    z[77]=z[2]*z[77];
    z[66]=2*z[66] + z[77];
    z[77]=n<T>(1,3)*z[2];
    z[66]=z[66]*z[77];
    z[72]=z[72] - z[76];
    z[72]=2*z[72] - z[3];
    z[72]=z[3]*z[72];
    z[78]=z[74] - z[8];
    z[79]=z[78]*z[8];
    z[72]=4*z[79] + z[72];
    z[67]=z[7] - z[57] + z[67];
    z[67]= - n<T>(5,3)*z[3] + 2*z[67];
    z[67]=z[7]*z[67];
    z[67]=n<T>(1,3)*z[72] + z[67];
    z[72]=4*z[7];
    z[67]=z[67]*z[72];
    z[80]=32*z[57] - 67*z[8];
    z[80]=z[8]*z[80];
    z[81]= - 7*z[57] + 4*z[8];
    z[81]=4*z[81] + n<T>(217,3)*z[3];
    z[81]=z[3]*z[81];
    z[80]=z[80] + z[81];
    z[80]=z[3]*z[80];
    z[70]= - z[70] - n<T>(14,3)*z[8];
    z[70]=z[70]*npow(z[8],2);
    z[70]=n<T>(8,3)*z[70] + z[80];
    z[62]=z[62] + z[66] + n<T>(1,3)*z[70] + z[67];
    z[66]=z[7]*z[73];
    z[67]=4*z[57];
    z[70]= - z[2]*z[67];
    z[73]=z[57] + z[7];
    z[73]=2*z[73] - z[5];
    z[73]=z[5]*z[73];
    z[66]=z[73] + z[70] - n<T>(4,9)*z[79] + z[66];
    z[70]=z[57] - z[76];
    z[70]= - n<T>(20,9)*z[10] + n<T>(8,9)*z[70] - z[3];
    z[70]=z[10]*z[70];
    z[66]=z[70] + 2*z[66];
    z[70]=n<T>(5,3)*z[8];
    z[73]=z[57] + z[70];
    z[61]= - n<T>(40,27)*z[4] - 4*z[5] - z[64] + z[61] - z[72] + n<T>(8,3)*z[73]
    + z[3];
    z[61]=z[4]*z[61];
    z[61]=2*z[66] + z[61];
    z[61]=z[61]*z[65];
    z[64]=n<T>(2,3)*z[3];
    z[65]= - z[74] - z[3];
    z[65]=z[65]*z[64];
    z[64]=z[64] - z[68];
    z[66]=n<T>(1,3)*z[5];
    z[64]=2*z[64] + z[66];
    z[64]=z[5]*z[64];
    z[60]=z[66] - n<T>(4,3)*z[3] - z[60];
    z[60]=z[9]*z[60];
    z[66]=z[74] + z[8];
    z[66]=z[8]*z[66];
    z[60]=z[60] + z[64] + z[66] + z[65];
    z[60]=z[9]*z[60];
    z[60]=z[27] + z[30] + z[60] + z[45];
    z[64]= - z[67] + z[8];
    z[64]=z[77] + n<T>(1,3)*z[64] - z[3];
    z[64]=z[3]*z[64];
    z[65]=n<T>(4,9)*z[8];
    z[66]=n<T>(4,3)*z[57] + z[8];
    z[65]=z[66]*z[65];
    z[66]=z[76] + z[57];
    z[67]=n<T>(1,27)*z[10] + n<T>(10,9)*z[66] - z[3] + z[55];
    z[63]=z[67]*z[63];
    z[67]=z[57] + z[3];
    z[67]=2*z[67] - z[7];
    z[67]=z[7]*z[67];
    z[63]=z[63] + n<T>(2,3)*z[67] + z[65] + z[64];
    z[63]=z[63]*z[71];
    z[56]= - z[78]*z[56];
    z[64]= - z[7] - z[74] + z[69];
    z[64]=2*z[64] + z[2];
    z[64]=z[2]*z[64];
    z[65]=z[57] - z[8];
    z[67]=z[3]*z[65];
    z[56]=z[64] + z[56] + 4*z[67];
    z[64]=2*z[3];
    z[66]= - z[64] + z[66];
    z[66]=2*z[66] - z[7];
    z[66]=z[7]*z[66];
    z[56]=z[66] + 2*z[56];
    z[64]=z[70] - z[64];
    z[64]=n<T>(4,3)*z[2] + 2*z[64] - n<T>(1,3)*z[7];
    z[64]=z[5]*z[64];
    z[56]=n<T>(1,3)*z[56] + z[64];
    z[56]=z[5]*z[56];
    z[56]= - z[37] - z[46] + z[56] - z[52];
    z[64]=z[5] + z[3];
    z[66]=z[57]*z[64];
    z[66]=z[21] + z[66];
    z[66]=z[12]*z[66];
    z[64]= - z[57] + z[64];
    z[64]=z[16]*z[64];
    z[64]=z[66] + z[64];
    z[66]= - z[5] + z[75];
    z[66]=z[15]*z[66];
    z[67]= - z[7] + z[75];
    z[67]=z[17]*z[67];
    z[66]=z[39] - z[66] - z[67] + z[47] - z[42];
    z[67]=z[7] + z[8];
    z[57]= - z[57]*z[67];
    z[57]= - z[21] + z[57];
    z[57]=z[13]*z[57];
    z[65]=z[7] - z[65];
    z[65]=z[19]*z[65];
    z[57]= - z[35] + z[57] + z[65];
    z[55]=z[55] - z[9];
    z[55]= - n<T>(23,3)*z[6] + n<T>(80,3)*z[4] + 20*z[5] + 52*z[10] - 212*z[8]
    + z[59] + 32*z[55];
    z[55]=z[21]*z[55];
    z[59]= - z[10] - z[6];
    z[59]=z[34]*z[59];
    z[59]= - z[50] + z[59] - z[51];
    z[65]=z[49] - z[25];
    z[67]=z[53] - z[44];
    z[68]=z[54] + z[48];
    z[69]=z[3] + z[9];
    z[69]=z[32]*z[69];
    z[69]=z[69] + z[36];
    z[70]= - n<T>(160,9)*z[28] - n<T>(56,9)*z[26] - n<T>(4,3)*z[24] + n<T>(80,81)*z[22];
    z[70]=i*z[70];

    r +=  - n<T>(82,27)*z[23] + n<T>(80,9)*z[29] - 5*z[31] + n<T>(56,9)*z[38] + 4*
      z[40] + n<T>(128,9)*z[41] - n<T>(4,9)*z[43] + n<T>(1,27)*z[55] + n<T>(4,3)*z[56]
       + n<T>(16,3)*z[57] + n<T>(2,3)*z[58] + n<T>(32,27)*z[59] + n<T>(8,3)*z[60] + 
      z[61] + n<T>(1,3)*z[62] + z[63] + n<T>(64,9)*z[64] + n<T>(8,27)*z[65] - n<T>(16,9)
      *z[66] + n<T>(8,9)*z[67] - n<T>(16,27)*z[68] + n<T>(32,9)*z[69] + z[70];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCA_25(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCA_25(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
