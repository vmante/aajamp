#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_45(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,82>& f,
  const std::array<std::complex<T>,330>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[63];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[79];
    z[3]=c[11];
    z[4]=d[1];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[9];
    z[10]=c[12];
    z[11]=c[13];
    z[12]=c[20];
    z[13]=c[23];
    z[14]=c[32];
    z[15]=c[33];
    z[16]=c[36];
    z[17]=c[38];
    z[18]=c[40];
    z[19]=c[43];
    z[20]=d[0];
    z[21]=f[24];
    z[22]=f[44];
    z[23]=f[69];
    z[24]=f[80];
    z[25]=f[22];
    z[26]=f[42];
    z[27]=f[70];
    z[28]=d[2];
    z[29]=g[24];
    z[30]=g[27];
    z[31]=g[38];
    z[32]=g[40];
    z[33]=g[48];
    z[34]=g[50];
    z[35]=g[79];
    z[36]=g[82];
    z[37]=g[90];
    z[38]=g[92];
    z[39]=g[99];
    z[40]=g[101];
    z[41]=g[170];
    z[42]=g[173];
    z[43]=g[181];
    z[44]=g[183];
    z[45]=g[189];
    z[46]=g[243];
    z[47]=g[254];
    z[48]=g[270];
    z[49]=g[272];
    z[50]=g[276];
    z[51]=g[278];
    z[52]=g[281];
    z[53]=g[308];
    z[54]=z[5] - z[6];
    z[55]= - z[12] + n<T>(1,20)*z[13];
    z[55]= - n<T>(6,5)*z[11] + n<T>(1,2)*z[55];
    z[56]=n<T>(31,1080)*z[3] + z[55];
    z[56]= - z[56]*z[54];
    z[57]= - 27*z[17] + n<T>(7,2)*z[15];
    z[58]=z[9] + 2*z[7];
    z[59]=z[3]*z[58];
    z[60]= - z[2]*z[1];
    z[61]=n<T>(71,1080)*z[3] + z[55];
    z[61]=z[8]*z[61];
    z[55]=n<T>(49,1080)*z[3] - z[55];
    z[55]=z[4]*z[55];
    z[55]=z[55] + z[61] + z[60] + n<T>(1,2)*z[57] - n<T>(1,27)*z[59] + z[56];
    z[55]=i*z[55];
    z[56]=z[21] + z[24];
    z[57]=z[25] - z[26];
    z[59]=z[27] - n<T>(1,2)*z[57];
    z[56]=z[59] - n<T>(1,2)*z[56];
    z[60]=n<T>(1,2)*z[2];
    z[56]=z[60] + n<T>(1,3)*z[10] + n<T>(1,2)*z[56];
    z[54]=z[56]*z[54];
    z[56]=z[20] - z[28];
    z[56]= - n<T>(1,4)*z[56];
    z[61]= - z[24] - z[23] + z[21] + z[22];
    z[56]=z[61]*z[56];
    z[61]=z[22] + z[23];
    z[57]= - z[61] + z[57];
    z[57]= - z[60] + n<T>(1,4)*z[57] - n<T>(4,3)*z[10];
    z[57]=z[8]*z[57];
    z[59]=z[59] + n<T>(1,2)*z[61];
    z[59]= - z[60] + n<T>(1,2)*z[59] - n<T>(5,3)*z[10];
    z[59]=z[4]*z[59];
    z[60]=z[27]*z[9];
    z[60]= - z[37] - z[38] + z[41] + z[42] - z[31] - z[32] - z[35] - 
    z[36] - z[30] + z[60] - z[29] - z[43] + z[44] + z[48] + z[51];
    z[61]= - z[33] + z[34] - z[39] + z[40] + z[45] + z[52];
    z[58]=z[10]*z[58];
    z[62]=z[2]*z[7];
    z[54]=z[55] + z[59] + z[57] + z[62] + z[58] + n<T>(5,18)*z[14] - 6*z[16]
    + 5*z[18] + n<T>(3,2)*z[19] - 2*z[46] + n<T>(1,3)*z[47] + z[49] - z[50] + 
    z[53] + z[56] + z[54] + n<T>(1,6)*z[61] - n<T>(1,2)*z[60];

    r += z[54];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_45(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,82>& f,
  const std::array<std::complex<double>,330>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_45(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,82>& f,
  const std::array<std::complex<dd_real>,330>& g
);
#endif
