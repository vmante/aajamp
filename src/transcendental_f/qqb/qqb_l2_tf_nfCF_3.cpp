#include "qqb_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCF_3(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[32];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[5];
    z[4]=d[17];
    z[5]=d[4];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=e[9];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[17];
    z[13]=c[31];
    z[14]=e[7];
    z[15]=e[6];
    z[16]=f[30];
    z[17]=f[31];
    z[18]=f[35];
    z[19]=f[36];
    z[20]=f[39];
    z[21]=f[40];
    z[22]=z[7]*z[1];
    z[23]=z[2] + n<T>(2,3)*z[5];
    z[24]=z[4] - z[23];
    z[24]=z[1]*z[24];
    z[24]=z[24] + n<T>(14,9)*z[22];
    z[24]=i*z[24];
    z[25]=npow(z[5],2);
    z[26]= - 17*z[14] + n<T>(10,3)*z[9];
    z[27]= - n<T>(7,9)*z[7] + z[23];
    z[27]=z[7]*z[27];
    z[24]=z[24] + z[27] - n<T>(1,3)*z[25] + n<T>(1,9)*z[26] + z[8];
    z[26]=i*z[1];
    z[27]=z[26] - z[7];
    z[23]= - n<T>(17,9)*z[3] - n<T>(8,9)*z[6] - z[23] - n<T>(40,9)*z[27];
    z[23]=z[3]*z[23];
    z[27]= - z[7] - 8*z[26];
    z[27]=n<T>(2,9)*z[27] + z[6];
    z[27]=z[6]*z[27];
    z[23]=z[23] + 2*z[24] + z[27];
    z[23]=z[3]*z[23];
    z[24]=z[2]*z[4];
    z[27]=8*z[4] - n<T>(7,3)*z[5];
    z[27]=z[5]*z[27];
    z[24]=n<T>(1,3)*z[27] - n<T>(4,9)*z[8] + z[24];
    z[24]=z[1]*z[24];
    z[27]=7*z[5];
    z[28]=2*z[4] - z[27];
    z[28]=z[1]*z[28];
    z[28]=z[28] - 4*z[22];
    z[29]=n<T>(2,9)*z[7];
    z[28]=z[28]*z[29];
    z[24]=z[28] + z[24] + 2*z[12] + n<T>(1,27)*z[10];
    z[24]=i*z[24];
    z[28]= - z[14] + z[8];
    z[28]=z[2]*z[28];
    z[24]=z[28] + z[24];
    z[28]=4*z[8];
    z[30]=2*z[15] - z[9];
    z[30]=n<T>(7,9)*z[25] + n<T>(1,3)*z[30] + z[28];
    z[30]=z[5]*z[30];
    z[31]=n<T>(8,3)*z[7];
    z[27]=z[27] + z[31];
    z[27]=z[7]*z[27];
    z[27]=z[27] + 7*z[25] - 5*z[9] + z[28];
    z[27]=z[27]*z[29];
    z[28]=z[28] - n<T>(2,3)*z[9] + z[15] - 2*z[14];
    z[25]=n<T>(2,3)*z[28] - z[25];
    z[28]=4*z[4] - z[5];
    z[28]=z[1]*z[28];
    z[22]=z[28] - z[22];
    z[22]=i*z[22];
    z[28]= - z[2] - n<T>(20,9)*z[5];
    z[28]=z[7]*z[28];
    z[22]=n<T>(4,9)*z[22] + n<T>(2,3)*z[25] + z[28];
    z[25]=n<T>(44,27)*z[6] - n<T>(8,9)*z[26] - z[31] + z[2] + n<T>(16,9)*z[5];
    z[25]=z[6]*z[25];
    z[22]=2*z[22] + z[25];
    z[22]=z[6]*z[22];
    z[25]=z[18] - z[21];
    z[25]= - n<T>(4,3)*z[16] - n<T>(7,3)*z[17] + n<T>(1,3)*z[19] - z[20] - n<T>(8,3)*
    z[25];
    z[26]=z[4]*z[9];
    z[22]=z[23] + z[22] + z[27] + n<T>(4,3)*z[30] + n<T>(8,9)*z[26] + n<T>(10,9)*
    z[11] + n<T>(2,3)*z[25] - z[13] + 2*z[24];

    r += n<T>(2,3)*z[22];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCF_3(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCF_3(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
