#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1271(
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[11];
  std::complex<T> r(0,0);

    z[1]=g[170];
    z[2]=g[173];
    z[3]=g[181];
    z[4]=g[183];
    z[5]=g[191];
    z[6]=g[198];
    z[7]=g[200];
    z[8]=g[201];
    z[9]=z[7] - z[5];
    z[10]=z[4] - z[3] + z[1] - z[2];

    r += z[6] + 4*z[8] + 2*z[9] + 3*z[10];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1271(
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1271(
  const std::array<std::complex<dd_real>,472>& g
);
#endif
