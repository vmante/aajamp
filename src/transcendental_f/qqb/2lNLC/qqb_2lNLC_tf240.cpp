#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf240(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[46];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[19];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[31];
    z[22]=e[3];
    z[23]=e[10];
    z[24]=f[4];
    z[25]=f[5];
    z[26]=f[11];
    z[27]=f[12];
    z[28]=f[16];
    z[29]=npow(z[5],2);
    z[30]=n<T>(1,2)*z[29];
    z[31]=z[1]*i;
    z[32]=z[31]*z[5];
    z[33]=z[30] - z[32];
    z[34]=n<T>(1,2)*z[6];
    z[35]=9*z[5] - 5*z[31];
    z[35]= - z[3] + n<T>(1,2)*z[35] - z[2];
    z[35]=z[35]*z[34];
    z[36]= - z[2]*z[31];
    z[37]=npow(z[3],2);
    z[35]=z[35] - z[37] - n<T>(3,2)*z[33] + z[36];
    z[35]=z[6]*z[35];
    z[36]=n<T>(1,2)*z[2];
    z[37]=2*z[31];
    z[38]= - z[36] + z[5] + z[37];
    z[38]=z[2]*z[38];
    z[39]=z[5] - z[2];
    z[39]=z[7]*z[39];
    z[38]=z[39] + z[38] - z[30] - 2*z[32];
    z[39]=n<T>(1,3)*z[7];
    z[38]=z[38]*z[39];
    z[40]= - z[34] - z[31] + z[2];
    z[40]=z[6]*z[40];
    z[41]=z[31] - z[5];
    z[42]=z[41]*z[2];
    z[43]=n<T>(1,3)*z[13];
    z[30]=z[43] + z[40] + z[30] + z[42];
    z[30]=z[4]*z[30];
    z[40]=5*z[32];
    z[37]=z[37] - z[2];
    z[44]= - n<T>(1,2)*z[5] - z[37];
    z[44]=z[2]*z[44];
    z[44]=z[44] - z[29] + z[40];
    z[44]=z[2]*z[44];
    z[45]=z[5] - z[37];
    z[45]=z[10]*z[45];
    z[37]=z[7] - z[37];
    z[37]=z[12]*z[37];
    z[37]=z[37] + z[26] + z[25] - z[18] - z[15] + z[44] + z[45];
    z[44]=2*z[3];
    z[45]=z[31]*z[44];
    z[32]= - z[13] - z[32] + z[45];
    z[32]=z[9]*z[32];
    z[45]=z[44] + z[41];
    z[45]=z[11]*z[45];
    z[32]= - z[32] - z[45] + z[21] + z[20];
    z[29]=2*z[29] - z[40];
    z[31]= - n<T>(1,2)*z[31] - 2*z[5];
    z[31]=n<T>(1,6)*z[3] + n<T>(1,3)*z[31] - z[36];
    z[31]=z[3]*z[31];
    z[29]=z[31] + n<T>(1,3)*z[29] - z[42];
    z[29]=z[3]*z[29];
    z[31]=n<T>(1,2)*z[3] + z[41];
    z[31]=z[3]*z[31];
    z[31]=n<T>(1,2)*z[33] + 5*z[31];
    z[33]= - z[34] - z[41];
    z[33]=z[6]*z[33];
    z[34]=n<T>(23,4)*z[41] + 8*z[3];
    z[34]=n<T>(1,3)*z[34] - n<T>(3,4)*z[6];
    z[34]=z[8]*z[34];
    z[31]=z[34] + n<T>(1,3)*z[31] + n<T>(3,2)*z[33];
    z[31]=z[8]*z[31];
    z[33]= - z[39] + n<T>(5,3)*z[8] - n<T>(5,2)*z[6] - z[3] + n<T>(1,4)*z[5] + n<T>(10,3)
   *z[2];
    z[33]=z[33]*z[43];
    z[34]=n<T>(4,3)*z[19] + n<T>(2,3)*z[17] - n<T>(5,36)*z[14];
    z[34]=i*z[34];
    z[36]=z[44] + z[6];
    z[36]=z[22]*z[36];
    z[39]= - z[3] - z[8];
    z[39]=z[23]*z[39];

    r += n<T>(1,18)*z[16] - n<T>(23,12)*z[24] - n<T>(9,4)*z[27] - n<T>(3,4)*z[28]
     +  z[29] + z[30] + z[31] - n<T>(2,3)*z[32] + z[33] + z[34] + z[35] + 
      z[36] + n<T>(1,3)*z[37] + z[38] + n<T>(8,3)*z[39];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf240(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf240(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
