#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf115(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[77];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=c[3];
    z[21]=d[9];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[3];
    z[31]=e[6];
    z[32]=e[13];
    z[33]=e[14];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[18];
    z[44]=f[20];
    z[45]=f[23];
    z[46]=f[31];
    z[47]=f[32];
    z[48]=f[34];
    z[49]=f[36];
    z[50]=f[37];
    z[51]=f[39];
    z[52]=f[51];
    z[53]=f[55];
    z[54]=f[58];
    z[55]=f[96];
    z[56]=76*z[7];
    z[57]= - 71*z[8] + z[56];
    z[58]=z[1]*i;
    z[57]=z[58]*z[57];
    z[57]= - 71*z[20] + z[57];
    z[57]=z[12]*z[57];
    z[59]=z[58] - z[8];
    z[56]=71*z[59] + z[56];
    z[56]=z[18]*z[56];
    z[60]=76*z[58] - n<T>(661,12)*z[2];
    z[60]=z[60]*npow(z[2],2);
    z[61]=71*z[58];
    z[62]= - n<T>(11,2)*z[5] + z[61] - n<T>(131,2)*z[2];
    z[62]=z[14]*z[62];
    z[63]=24*z[6] + z[59];
    z[63]=z[16]*z[63];
    z[56]=z[62] + z[60] + z[57] + z[63] + z[56];
    z[57]=z[5] - z[2];
    z[60]=4*z[8];
    z[62]=z[60] - 8*z[58] + n<T>(21,10)*z[57];
    z[62]=z[8]*z[62];
    z[63]=n<T>(1,2)*z[2];
    z[64]=n<T>(157,5)*z[58] - z[2];
    z[64]=z[64]*z[63];
    z[65]= - n<T>(21,5)*z[2] - 33 - n<T>(23,15)*z[58];
    z[65]=n<T>(1579,30)*z[9] - z[60] + n<T>(1,2)*z[65] + n<T>(103,15)*z[5];
    z[65]=z[9]*z[65];
    z[66]=n<T>(181,6)*z[5] + n<T>(103,3)*z[58] - 63*z[2];
    z[66]=z[5]*z[66];
    z[67]=13*z[3] + n<T>(37,6)*z[9] - n<T>(122,15)*z[5] + n<T>(37,5)*z[2] + n<T>(33,4)
    - n<T>(278,15)*z[58];
    z[67]=z[3]*z[67];
    z[62]=z[67] + z[65] + z[62] + z[64] + n<T>(1,5)*z[66];
    z[62]=z[3]*z[62];
    z[64]=npow(z[3],2);
    z[65]=npow(z[11],2);
    z[65]=z[64] - z[65];
    z[63]=z[63] - z[58];
    z[63]=z[63]*z[2];
    z[66]=n<T>(1,2)*z[6];
    z[67]=33 + 5*z[6];
    z[67]=z[67]*z[66];
    z[68]= - 46*z[58] + n<T>(117,2)*z[8];
    z[68]=z[8]*z[68];
    z[69]=5*z[5];
    z[70]=n<T>(167,15)*z[7] - z[69] - n<T>(127,5)*z[8];
    z[70]=z[7]*z[70];
    z[65]=n<T>(1,2)*z[70] + z[67] + z[63] + n<T>(1,5)*z[68] - n<T>(3,2)*z[65];
    z[65]=z[7]*z[65];
    z[67]=21*z[2];
    z[68]= - n<T>(299,6)*z[5] + n<T>(299,3)*z[58] + z[67];
    z[68]=z[5]*z[68];
    z[70]= - n<T>(31,5)*z[58] + z[2];
    z[70]=z[2]*z[70];
    z[71]=z[8]*z[57];
    z[72]= - n<T>(896,3)*z[5] + n<T>(734,3)*z[58] - n<T>(1,4)*z[2];
    z[72]=n<T>(1,5)*z[72] - n<T>(7,2)*z[9];
    z[72]=z[9]*z[72];
    z[68]=z[72] - n<T>(21,10)*z[71] + z[70] + n<T>(1,10)*z[68];
    z[68]=z[9]*z[68];
    z[70]=n<T>(1,2)*z[5];
    z[71]=z[58] - z[70];
    z[71]=z[5]*z[71];
    z[72]=n<T>(1,2)*z[8];
    z[73]= - z[58] + z[72];
    z[73]=z[8]*z[73];
    z[74]=z[58] + n<T>(1,2)*z[9];
    z[75]=z[5] - z[74];
    z[75]=z[9]*z[75];
    z[71]=n<T>(31,2)*z[75] + n<T>(21,2)*z[71] + 43*z[73];
    z[59]= - z[66] - z[59];
    z[59]=z[6]*z[59];
    z[66]=2*z[2];
    z[73]=n<T>(467,30)*z[11] - n<T>(201,10)*z[6] - n<T>(116,5)*z[3] + n<T>(11,20)*z[9]
    - n<T>(387,10)*z[8] + n<T>(1263,20)*z[5] - n<T>(449,20)*z[58] - z[66];
    z[73]=z[11]*z[73];
    z[59]=z[73] + n<T>(201,5)*z[59] + n<T>(1,5)*z[71] - 39*z[64];
    z[59]=z[11]*z[59];
    z[64]=z[6] - z[9];
    z[64]= - n<T>(1,2)*z[4] - 2*z[5] + n<T>(3,2)*z[58] + z[66] + n<T>(29,20)*z[64];
    z[64]=z[4]*z[64];
    z[66]=29*z[58] + n<T>(217,2)*z[9];
    z[66]=z[9]*z[66];
    z[71]= - z[58] - z[9];
    z[71]=29*z[71] - n<T>(159,2)*z[6];
    z[71]=z[6]*z[71];
    z[66]=z[66] + z[71];
    z[71]=z[58]*z[57];
    z[64]=z[64] + 4*z[71] + n<T>(1,10)*z[66];
    z[64]=z[4]*z[64];
    z[66]=289*z[3];
    z[71]= - n<T>(253,2)*z[5] + z[66];
    z[71]=z[58]*z[71];
    z[71]= - n<T>(253,2)*z[20] + z[71];
    z[71]=z[10]*z[71];
    z[73]=z[58] - z[5];
    z[66]=n<T>(253,2)*z[73] + z[66];
    z[66]=z[15]*z[66];
    z[66]=z[71] + z[66];
    z[71]= - z[61] + n<T>(33,2)*z[2];
    z[71]=z[2]*z[71];
    z[61]=n<T>(33,2)*z[5] + z[61] - 33*z[2];
    z[61]=z[5]*z[61];
    z[61]=z[71] + z[61];
    z[57]=3*z[8] - 9*z[58] - n<T>(71,5)*z[57];
    z[57]=z[57]*z[72];
    z[57]=n<T>(1,5)*z[61] + z[57];
    z[57]=z[8]*z[57];
    z[61]=2*z[58];
    z[71]=z[61] - z[8];
    z[72]=z[71]*z[60];
    z[63]= - z[63] + z[72];
    z[72]= - 11 - 13*z[58];
    z[69]=n<T>(3,2)*z[72] + z[69];
    z[69]=n<T>(61,15)*z[6] + n<T>(59,20)*z[9] + n<T>(1,2)*z[69] + n<T>(76,5)*z[8];
    z[69]=z[6]*z[69];
    z[72]=z[9]*z[74];
    z[63]=z[69] + 5*z[63] + n<T>(29,10)*z[72];
    z[63]=z[6]*z[63];
    z[69]= - 287*z[58] + n<T>(291,2)*z[2];
    z[69]=z[2]*z[69];
    z[72]= - n<T>(29,15)*z[5] - 7*z[58] + n<T>(31,5)*z[2];
    z[72]=z[5]*z[72];
    z[69]=n<T>(1,5)*z[69] + z[72];
    z[69]=z[69]*z[70];
    z[58]= - z[58] + z[67];
    z[58]= - n<T>(968,15)*z[3] - n<T>(1741,30)*z[9] + n<T>(1,5)*z[58] - z[60];
    z[58]=z[19]*z[58];
    z[60]=z[36] + z[48] - z[47];
    z[67]=z[53] + z[52];
    z[61]= - z[4] + z[61] - z[2];
    z[61]=z[13]*z[61];
    z[61]=z[61] - z[34];
    z[70]= - n<T>(389,5)*z[2] - n<T>(479,6)*z[5];
    z[70]=n<T>(1,3)*z[70] - n<T>(293,10)*z[8];
    z[70]=n<T>(19,30)*z[21] + n<T>(23,3)*z[7] + n<T>(111,40)*z[4] - n<T>(71,6)*z[11]
     + 
   n<T>(92,5)*z[6] + n<T>(329,30)*z[3] + n<T>(1,4)*z[70] + n<T>(863,45)*z[9];
    z[70]=z[20]*z[70];
    z[72]= - n<T>(307,5)*z[27] - n<T>(76,5)*z[25] + n<T>(2309,360)*z[22];
    z[72]=i*z[72];
    z[71]= - z[2] + z[71];
    z[71]=z[17]*z[71];
    z[73]=877*z[3] + 397*z[11];
    z[73]=z[30]*z[73];
    z[74]=n<T>(219,5)*z[11] + 28*z[7];
    z[74]=z[31]*z[74];
    z[75]=z[6] + z[21];
    z[75]=z[32]*z[75];
    z[76]= - z[9] - z[21];
    z[76]=z[33]*z[76];

    r +=  - n<T>(71,12)*z[23] + n<T>(1,40)*z[24] + n<T>(661,60)*z[26] + n<T>(367,10)*
      z[28] - n<T>(25501,360)*z[29] + z[35] - n<T>(866,15)*z[37] - n<T>(257,20)*
      z[38] - n<T>(111,10)*z[39] - n<T>(1263,20)*z[40] + n<T>(11,20)*z[41] + n<T>(3,2)*
      z[42] - n<T>(4,3)*z[43] + 4*z[44] - 2*z[45] + n<T>(153,10)*z[46] + n<T>(74,5)
      *z[49] - z[50] - n<T>(201,10)*z[51] + n<T>(59,20)*z[54] + z[55] + n<T>(1,5)*
      z[56] + z[57] + z[58] + z[59] - n<T>(5,2)*z[60] + 16*z[61] + z[62] + 
      z[63] + z[64] + z[65] + n<T>(1,15)*z[66] - n<T>(159,20)*z[67] + z[68] + 
      z[69] + z[70] + n<T>(38,5)*z[71] + z[72] + n<T>(1,10)*z[73] + z[74] + 5*
      z[75] + n<T>(109,10)*z[76];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf115(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf115(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
