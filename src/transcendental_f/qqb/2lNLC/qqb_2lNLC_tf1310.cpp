#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1310(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[43];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[11];
    z[6]=d[4];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=d[9];
    z[11]=e[9];
    z[12]=e[12];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=d[3];
    z[16]=e[6];
    z[17]=e[7];
    z[18]=e[13];
    z[19]=e[14];
    z[20]=f[30];
    z[21]=f[31];
    z[22]=f[32];
    z[23]=f[36];
    z[24]=f[39];
    z[25]=f[50];
    z[26]=f[51];
    z[27]=f[52];
    z[28]=f[55];
    z[29]=f[58];
    z[30]=z[10] + z[2];
    z[31]=z[1]*i;
    z[30]=z[31]*z[30];
    z[30]=z[13] + z[30];
    z[30]=z[5]*z[30];
    z[32]=z[8] + z[7];
    z[33]= - z[31]*z[32];
    z[33]= - z[13] + z[33];
    z[33]=z[9]*z[33];
    z[32]=z[31] - z[32];
    z[32]=z[11]*z[32];
    z[34]=z[31] - z[2];
    z[35]= - z[10] + z[34];
    z[35]=z[12]*z[35];
    z[36]=z[7] + z[6];
    z[36]=z[16]*z[36];
    z[37]= - z[4] - z[10];
    z[37]=z[19]*z[37];
    z[30]=z[36] + z[37] + z[25] - z[20] + z[30] + z[33] + z[32] + z[35];
    z[32]=n<T>(1,3)*z[31];
    z[33]=z[32] + z[3];
    z[35]=n<T>(1,6)*z[6];
    z[36]=n<T>(2,3)*z[7];
    z[37]= - z[35] - z[36] + z[33];
    z[37]=z[6]*z[37];
    z[38]=n<T>(1,2)*z[3];
    z[35]= - z[35] - z[38] + z[36];
    z[35]=z[8]*z[35];
    z[39]=z[38] - z[31];
    z[39]=z[39]*z[3];
    z[40]=n<T>(1,3)*z[7];
    z[41]= - 2*z[31] + z[7];
    z[41]=z[41]*z[40];
    z[35]=z[35] + z[37] - z[39] + z[41];
    z[35]=z[8]*z[35];
    z[37]=z[38] + z[31];
    z[37]=z[37]*z[3];
    z[36]=z[31]*z[36];
    z[41]=z[32] - z[3];
    z[41]=n<T>(1,3)*z[6] + n<T>(1,2)*z[41] + z[40];
    z[41]=z[6]*z[41];
    z[36]=z[41] - z[37] + z[36];
    z[36]=z[6]*z[36];
    z[32]= - z[32] + n<T>(1,3)*z[2];
    z[38]=z[38] - z[32];
    z[38]=z[2]*z[38];
    z[38]=z[39] + z[38];
    z[38]=z[2]*z[38];
    z[33]=n<T>(1,6)*z[2] - z[33];
    z[33]=z[2]*z[33];
    z[32]=z[3] + z[32];
    z[32]=n<T>(1,2)*z[32] - n<T>(1,3)*z[4];
    z[32]=z[4]*z[32];
    z[32]=z[32] + z[37] + z[33];
    z[32]=z[4]*z[32];
    z[33]=z[3] + z[7];
    z[33]=z[17]*z[33];
    z[37]= - z[3] - z[10];
    z[37]=z[18]*z[37];
    z[33]=z[33] + z[37];
    z[37]= - z[40] - n<T>(2,3)*z[31] - z[3];
    z[39]=npow(z[7],2);
    z[37]=z[37]*z[39];
    z[40]=npow(z[2],2);
    z[34]= - 2*z[34] - z[4];
    z[34]=z[4]*z[34];
    z[34]= - z[40] + z[34];
    z[31]=z[31] + z[3];
    z[31]=z[10]*z[31];
    z[31]=n<T>(1,3)*z[34] + z[31];
    z[31]=z[10]*z[31];
    z[34]=npow(z[10],2);
    z[34]=z[39] - z[34];
    z[34]=z[15]*z[34];
    z[39]= - z[29] - z[26] + z[24] + z[21];
    z[40]=z[28] - z[23];
    z[41]=z[6] - z[4];
    z[41]=n<T>(4,3)*z[10] - n<T>(1,6)*z[8] - z[7] + n<T>(1,2)*z[2] + n<T>(2,3)*z[41];
    z[41]=z[13]*z[41];
    z[42]=z[14]*i;

    r +=  - z[22] + z[27] + n<T>(2,3)*z[30] + z[31] + z[32] + 2*z[33] + 
      z[34] + z[35] + z[36] + z[37] + z[38] - n<T>(1,2)*z[39] - n<T>(1,6)*z[40]
       + n<T>(1,3)*z[41] - n<T>(1,9)*z[42];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1310(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1310(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
