#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1297(
  const std::array<std::complex<T>,24>& e
) {
    return e[21];
}

template std::complex<double> qqb_2lNLC_tf1297(
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1297(
  const std::array<std::complex<dd_real>,24>& e
);
#endif
