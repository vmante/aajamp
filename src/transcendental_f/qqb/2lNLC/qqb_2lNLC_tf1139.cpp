#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1139(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[65];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[7];
    z[7]=d[1];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=d[9];
    z[28]=e[10];
    z[29]=e[6];
    z[30]=e[3];
    z[31]=e[13];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[11];
    z[35]=f[12];
    z[36]=f[13];
    z[37]=f[16];
    z[38]=f[17];
    z[39]=f[30];
    z[40]=f[31];
    z[41]=f[33];
    z[42]=f[36];
    z[43]=2*z[6];
    z[44]=z[1]*i;
    z[45]=z[44] + z[43];
    z[46]=n<T>(2,3)*z[6];
    z[45]=z[45]*z[46];
    z[47]=n<T>(1,3)*z[10];
    z[48]=z[44] - z[6];
    z[49]= - 2*z[48] - z[10];
    z[49]=z[49]*z[47];
    z[50]=2*z[11];
    z[51]=z[44] + z[6];
    z[51]=n<T>(2,3)*z[11] + n<T>(1,3)*z[51] - z[5];
    z[51]=z[51]*z[50];
    z[43]= - z[43] + z[2];
    z[43]=z[2]*z[43];
    z[52]=npow(z[5],2);
    z[43]=z[51] - 2*z[52] + z[49] + z[45] + z[43];
    z[43]=z[11]*z[43];
    z[45]=2*z[4];
    z[49]=z[45]*z[44];
    z[51]=npow(z[4],2);
    z[52]=z[51] - z[49];
    z[53]=z[44] - z[4];
    z[54]=2*z[53];
    z[55]= - z[54] - z[10];
    z[55]=z[10]*z[55];
    z[56]=z[54] + z[7];
    z[56]=z[7]*z[56];
    z[57]=n<T>(8,3)*z[7] + n<T>(5,3)*z[53] - z[10];
    z[57]=z[8]*z[57];
    z[52]=z[57] + n<T>(4,3)*z[56] + n<T>(1,3)*z[52] + z[55];
    z[52]=z[8]*z[52];
    z[43]= - z[35] - z[37] + z[43] + z[52];
    z[52]= - z[4] - z[7];
    z[52]=z[44]*z[52];
    z[52]= - z[19] + z[52];
    z[52]=z[9]*z[52];
    z[55]=2*z[44];
    z[56]= - z[3] + z[55] - z[2];
    z[56]=z[13]*z[56];
    z[57]= - z[7] + z[53];
    z[57]=z[15]*z[57];
    z[58]= - z[10] - z[11];
    z[58]=z[29]*z[58];
    z[52]=z[57] + z[58] - z[42] + z[39] - z[26] + z[52] + z[56];
    z[56]= - z[6]*z[4];
    z[56]=z[56] - z[51] - z[49];
    z[56]=z[6]*z[56];
    z[57]=7*z[51];
    z[58]=z[44]*z[4];
    z[59]= - z[2] - 13*z[4] + 14*z[44];
    z[59]=z[2]*z[59];
    z[59]=z[59] + z[57] + 16*z[58];
    z[60]=z[55] - z[6];
    z[61]=z[60]*z[6];
    z[59]= - 4*z[61] + n<T>(1,3)*z[59];
    z[59]=z[2]*z[59];
    z[62]=npow(z[4],3);
    z[56]= - z[34] + z[59] + n<T>(1,3)*z[62] + z[56];
    z[57]= - z[57] - 10*z[58];
    z[59]=3*z[6];
    z[62]=z[44] + z[4];
    z[63]=2*z[62] - z[6];
    z[63]=z[63]*z[59];
    z[64]= - z[4] - z[55];
    z[64]=2*z[64] + z[2];
    z[64]=z[2]*z[64];
    z[59]=n<T>(4,3)*z[2] + n<T>(5,3)*z[4] - z[59];
    z[59]=z[3]*z[59];
    z[57]=z[59] + n<T>(2,3)*z[64] + n<T>(1,3)*z[57] + z[63];
    z[57]= - z[10] + n<T>(2,11)*z[57];
    z[57]=z[3]*z[57];
    z[46]= - z[47] + z[46] + z[4] - n<T>(5,3)*z[44];
    z[46]=z[10]*z[46];
    z[47]=n<T>(4,11)*z[4];
    z[59]=n<T>(8,11)*z[44] + 1 - z[47];
    z[59]=z[4]*z[59];
    z[46]=n<T>(4,11)*z[46] + n<T>(4,33)*z[61] + z[59];
    z[46]=z[10]*z[46];
    z[59]=2*z[2];
    z[61]= - z[48]*z[59];
    z[54]=z[54] - z[6];
    z[54]=z[6]*z[54];
    z[49]=z[61] + z[49] + z[54];
    z[54]=z[48] - z[2];
    z[47]=n<T>(12,11)*z[5] + n<T>(1,2) + z[47] + n<T>(4,11)*z[54];
    z[47]=z[5]*z[47];
    z[47]=n<T>(4,11)*z[49] + z[47];
    z[47]=z[5]*z[47];
    z[49]=z[50] + 3*z[5] - z[2] + z[6] - z[53];
    z[49]=z[16]*z[49];
    z[50]= - z[2] + z[60];
    z[50]=z[17]*z[50];
    z[49]= - z[49] - z[50] + z[40] - z[20];
    z[50]= - z[6] - z[11];
    z[44]=z[44]*z[50];
    z[44]= - z[19] + z[44];
    z[44]=z[12]*z[44];
    z[48]= - z[11] + z[48];
    z[48]=z[18]*z[48];
    z[44]=z[44] + z[48];
    z[48]=z[51] - 4*z[58];
    z[45]=z[45] - z[2];
    z[45]=z[2]*z[45];
    z[45]=n<T>(2,3)*z[48] + z[45];
    z[48]= - n<T>(20,33)*z[7] + n<T>(1,2) + n<T>(4,33)*z[62];
    z[48]=z[7]*z[48];
    z[45]=z[48] + n<T>(4,11)*z[45] - z[10];
    z[45]=z[7]*z[45];
    z[48]=z[36] - z[23];
    z[50]= - n<T>(56,33)*z[24] - n<T>(28,33)*z[22];
    z[50]=i*z[50];
    z[51]=n<T>(14,3)*z[10] + n<T>(8,3)*z[2] + z[4] - n<T>(5,3)*z[6];
    z[51]=n<T>(1,11)*z[51] + z[5];
    z[51]=n<T>(4,99)*z[8] - n<T>(5,99)*z[3] + n<T>(2,33)*z[11] + n<T>(1,3)*z[51]
     - n<T>(1,11)
   *z[7];
    z[51]=z[19]*z[51];
    z[53]=z[3] - z[5] + z[10] + z[4] - z[59];
    z[53]=z[27]*z[53];
    z[54]=7*z[2] - 5*z[4] - z[55];
    z[54]=z[14]*z[54];
    z[55]= - z[7] - z[8];
    z[55]=z[28]*z[55];

    r +=  - n<T>(7,99)*z[21] + n<T>(28,33)*z[25] + z[30] + z[31] + n<T>(10,33)*
      z[32] - n<T>(20,33)*z[33] - n<T>(6,11)*z[38] - n<T>(32,11)*z[41] + n<T>(4,11)*
      z[43] + n<T>(16,33)*z[44] + z[45] + z[46] + z[47] - n<T>(14,33)*z[48] - n<T>(8,11)*z[49]
     + z[50]
     + 2*z[51]
     + n<T>(8,33)*z[52]
     + z[53]
     + n<T>(4,33)*
      z[54] + n<T>(32,33)*z[55] + n<T>(2,11)*z[56] + z[57];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1139(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1139(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
