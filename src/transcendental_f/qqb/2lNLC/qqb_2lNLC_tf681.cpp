#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf681(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[76];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[9];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[6];
    z[15]=d[17];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=e[14];
    z[36]=f[0];
    z[37]=f[1];
    z[38]=f[3];
    z[39]=f[4];
    z[40]=f[5];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[16];
    z[44]=f[17];
    z[45]=f[18];
    z[46]=f[31];
    z[47]=f[36];
    z[48]=f[39];
    z[49]=f[51];
    z[50]=f[55];
    z[51]=f[58];
    z[52]=z[1]*i;
    z[53]=2*z[52];
    z[54]=z[53] - z[6];
    z[55]=z[6]*z[54];
    z[56]= - 22793*z[52] + 24527*z[5];
    z[56]=z[5]*z[56];
    z[55]= - 289*z[55] + n<T>(1,3)*z[56];
    z[56]=z[7] + z[5];
    z[56]= - n<T>(42727,3)*z[3] + 3640*z[2] + n<T>(9967,6)*z[52] - 578*z[6] + 
   n<T>(28339,6)*z[56];
    z[56]=z[3]*z[56];
    z[57]=npow(z[9],2);
    z[58]=z[52] - z[5];
    z[59]=z[7] + z[58];
    z[59]=z[7]*z[59];
    z[60]=z[2]*z[58];
    z[55]=z[56] + 7280*z[60] + n<T>(45586,3)*z[59] + 2*z[55] + 8061*z[57];
    z[55]=z[3]*z[55];
    z[56]= - n<T>(86923,3)*z[2] + n<T>(49054,3)*z[7] + n<T>(59864,3)*z[9] - n<T>(26402,3)
   *z[4] + n<T>(54349,3)*z[6] + n<T>(54913,2)*z[5];
    z[56]=n<T>(9967,3)*z[10] + n<T>(1,3)*z[56] + n<T>(2687,2)*z[3];
    z[56]=z[24]*z[56];
    z[59]=z[52] - z[6];
    z[60]=578*z[59] - n<T>(22793,3)*z[7];
    z[60]=2*z[60] - n<T>(24871,3)*z[3];
    z[60]=z[22]*z[60];
    z[55]=z[60] + z[55] + z[56];
    z[56]=z[53] - z[4];
    z[56]=z[56]*z[4];
    z[60]= - z[52] + n<T>(1,2)*z[5];
    z[60]=z[60]*z[5];
    z[61]=npow(z[6],2);
    z[62]= - 11701*z[58] - n<T>(10545,2)*z[9];
    z[62]=z[9]*z[62];
    z[63]=n<T>(46883,2)*z[52] - 21145*z[5];
    z[63]=z[7] - n<T>(11701,3062)*z[9] + n<T>(1,4593)*z[63] - 2*z[4];
    z[63]=z[7]*z[63];
    z[62]=z[63] + n<T>(1,1531)*z[62] + z[56] + n<T>(3,2)*z[61] + n<T>(10483,4593)*
    z[60];
    z[62]=z[7]*z[62];
    z[63]=n<T>(41461,2)*z[6];
    z[64]= - 31088*z[52] + z[63];
    z[64]=z[6]*z[64];
    z[65]=2*z[59] + z[9];
    z[65]=z[9]*z[65];
    z[66]= - z[14] + z[52] + z[6];
    z[66]=z[14]*z[66];
    z[64]=n<T>(10373,2)*z[66] - 6905*z[24] + z[64] + 15544*z[65];
    z[64]=z[14]*z[64];
    z[65]=28339*z[3];
    z[66]= - 30682*z[5] - z[65];
    z[66]=z[52]*z[66];
    z[66]= - 30682*z[24] + z[66];
    z[66]=z[8]*z[66];
    z[58]=30682*z[58] - z[65];
    z[58]=z[18]*z[58];
    z[58]=z[58] + z[64] + z[66];
    z[64]=73*z[52] - n<T>(4291,2)*z[6];
    z[64]=z[6]*z[64];
    z[65]= - n<T>(4679,3)*z[5] - n<T>(5288,3)*z[52] + 3765*z[6];
    z[65]=z[5]*z[65];
    z[66]=11467*z[52] + 3437*z[5];
    z[66]=4*z[66] - n<T>(41275,2)*z[4];
    z[66]=z[4]*z[66];
    z[67]=n<T>(25903,6)*z[2] - n<T>(18341,6)*z[4] - n<T>(172,3)*z[5] - n<T>(25903,3)*
    z[52] - 1203*z[6];
    z[67]=z[2]*z[67];
    z[64]=z[67] + n<T>(1,3)*z[66] + 3*z[64] + 4*z[65];
    z[65]=n<T>(1,2)*z[7] + z[59];
    z[65]=z[7]*z[65];
    z[57]=3*z[65] + z[57] + n<T>(1,1531)*z[64];
    z[57]=z[2]*z[57];
    z[64]=n<T>(1,2)*z[6];
    z[65]=z[64] - z[52];
    z[65]=z[65]*z[6];
    z[66]= - n<T>(1,2)*z[9] - z[59];
    z[66]=z[9]*z[66];
    z[66]=z[65] - z[66];
    z[67]=z[52] - z[4];
    z[68]= - 2*z[67] - z[7];
    z[68]=z[7]*z[68];
    z[69]= - 9967*z[52] + 6905*z[6];
    z[69]= - z[7] - n<T>(6905,3062)*z[9] + n<T>(1,3062)*z[69] + z[4];
    z[69]=z[10]*z[69];
    z[56]=z[69] + z[68] + z[56] - n<T>(6905,1531)*z[66];
    z[56]=z[10]*z[56];
    z[66]=n<T>(1281,1531)*z[6];
    z[68]=z[54]*z[66];
    z[66]= - n<T>(2,3)*z[5] + 4*z[52] - z[66];
    z[66]=z[5]*z[66];
    z[66]=z[68] + z[66];
    z[66]=z[5]*z[66];
    z[66]=z[36] + z[66] + z[50];
    z[68]=2484*z[6];
    z[69]= - z[54]*z[68];
    z[68]= - n<T>(4015,3)*z[52] - z[68];
    z[68]=2*z[68] + n<T>(4015,3)*z[5];
    z[68]=z[5]*z[68];
    z[68]=z[69] + z[68];
    z[69]=n<T>(4015,4593)*z[5] - z[52] + n<T>(2484,1531)*z[6];
    z[69]=2*z[69] + n<T>(7,6)*z[4];
    z[69]=z[4]*z[69];
    z[68]=n<T>(2,1531)*z[68] + z[69];
    z[68]=z[4]*z[68];
    z[60]=n<T>(10373,3)*z[65] - 11701*z[60];
    z[63]=29096*z[52] - z[63];
    z[63]=n<T>(1,3)*z[63] - n<T>(5577,2)*z[5];
    z[63]=n<T>(6202,1531)*z[9] + n<T>(1,1531)*z[63] - z[4];
    z[63]=z[9]*z[63];
    z[65]=z[52]*z[4];
    z[60]=z[63] + n<T>(1,1531)*z[60] + z[65];
    z[60]=z[9]*z[60];
    z[63]= - z[14] - z[6];
    z[63]=z[52]*z[63];
    z[63]= - z[24] + z[63];
    z[63]=z[15]*z[63];
    z[59]= - z[14] + z[59];
    z[59]=z[21]*z[59];
    z[59]=z[63] + z[59];
    z[63]=3*z[52] - z[64];
    z[61]=z[63]*z[61];
    z[63]= - z[11] - z[4];
    z[63]=z[52]*z[63];
    z[63]= - z[24] + z[63];
    z[63]=z[12]*z[63];
    z[64]= - z[4] - z[9];
    z[52]=z[52]*z[64];
    z[52]= - z[24] + z[52];
    z[52]=z[13]*z[52];
    z[64]=z[40] - z[29];
    z[69]=z[48] - z[46];
    z[70]=n<T>(70178,4593)*z[30] + n<T>(25903,4593)*z[28] - n<T>(3541,6124)*z[25];
    z[70]=i*z[70];
    z[65]=z[65] + n<T>(1281,3062)*z[24];
    z[65]=z[11]*z[65];
    z[53]=z[53] - z[2];
    z[71]=z[4] - z[53];
    z[71]=z[16]*z[71];
    z[53]=z[5] - z[53];
    z[53]=z[17]*z[53];
    z[72]=z[9] - z[67];
    z[72]=z[19]*z[72];
    z[54]=z[2] - z[54];
    z[54]=z[20]*z[54];
    z[67]=z[11] - z[67];
    z[67]=z[23]*z[67];
    z[73]= - z[9] - z[3];
    z[73]=z[33]*z[73];
    z[74]=z[9] + z[14];
    z[74]=z[34]*z[74];
    z[75]=z[7] + z[11];
    z[75]=z[35]*z[75];

    r += n<T>(32713,3062)*z[26] - n<T>(29213,55116)*z[27] - n<T>(35089,4593)*z[31]
       - n<T>(231427,6124)*z[32] - z[37] + n<T>(8030,4593)*z[38] - n<T>(21145,4593)
      *z[39] - n<T>(2562,1531)*z[41] + n<T>(5577,3062)*z[42] - n<T>(10545,3062)*
      z[43] + n<T>(4968,1531)*z[44] + n<T>(2,3)*z[45] + n<T>(41461,9186)*z[47] + 
      z[49] - z[51] + z[52] + n<T>(344,4593)*z[53] + n<T>(2406,1531)*z[54] + n<T>(1,1531)*z[55]
     + z[56]
     + z[57]
     + n<T>(1,4593)*z[58]
     + n<T>(10373,4593)*z[59]
       + z[60] + n<T>(2687,1531)*z[61] + z[62] + z[63] + n<T>(25903,9186)*z[64]
       + z[65] + 2*z[66] + z[67] + z[68] - n<T>(6905,3062)*z[69] + z[70] + 
      n<T>(27527,4593)*z[71] + z[72] + n<T>(8061,1531)*z[73] + n<T>(31088,4593)*
      z[74] + 3*z[75];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf681(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf681(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
