#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf580(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[14];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[6];
    z[4]=d[7];
    z[5]=d[5];
    z[6]=c[3];
    z[7]=e[6];
    z[8]=e[7];
    z[9]=f[31];
    z[10]=f[36];
    z[11]=z[1]*i;
    z[12]=z[11] - z[4];
    z[13]=z[12]*z[3];
    z[11]= - z[11] + n<T>(1,2)*z[4];
    z[11]=z[11]*z[4];
    z[11]= - n<T>(1,6)*z[6] + z[13] + z[11];
    z[12]=z[12] + n<T>(1,2)*z[3];
    z[13]= - n<T>(1,2)*z[5] - z[12];
    z[13]=z[5]*z[13];
    z[13]=z[13] - z[8] - z[11];
    z[13]=z[5]*z[13];
    z[12]=n<T>(1,2)*z[2] + z[12];
    z[12]=z[2]*z[12];
    z[11]=z[12] + z[7] + z[11];
    z[11]=z[2]*z[11];
    z[12]= - z[8] + z[7];
    z[12]=z[3]*z[12];

    r += z[9] + z[10] + z[11] + z[12] + z[13];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf580(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf580(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
