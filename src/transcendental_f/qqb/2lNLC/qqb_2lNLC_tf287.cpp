#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf287(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[36];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[7];
    z[9]=e[2];
    z[10]=c[3];
    z[11]=d[6];
    z[12]=c[11];
    z[13]=c[15];
    z[14]=c[31];
    z[15]=e[3];
    z[16]=e[10];
    z[17]=e[6];
    z[18]=f[4];
    z[19]=f[12];
    z[20]=f[16];
    z[21]=f[31];
    z[22]=f[36];
    z[23]=f[39];
    z[24]= - z[6] + z[7] + z[11] + 3*z[8];
    z[25]=2*z[4];
    z[26]=i*z[1];
    z[24]=2*z[2] + n<T>(9,2)*z[26] - 6*z[3] + z[25] + n<T>(1,2)*z[24];
    z[24]=z[6]*z[24];
    z[27]=npow(z[8],2);
    z[28]=n<T>(1,3)*z[10];
    z[27]= - z[28] + n<T>(1,2)*z[27];
    z[29]=2*z[3];
    z[30]= - z[25] + z[3];
    z[29]=z[30]*z[29];
    z[30]=z[3] - z[4];
    z[31]=z[8] + z[7] - 4*z[30];
    z[31]=z[31]*z[26];
    z[32]=n<T>(1,2)*z[7];
    z[33]=z[32] - z[8];
    z[33]=z[33]*z[7];
    z[34]=npow(z[4],2);
    z[35]= - z[11] + 3*z[2];
    z[35]=z[2]*z[35];
    z[24]=z[24] + z[35] + z[31] + z[29] + n<T>(3,2)*z[34] + z[33] - z[17] - 
   3*z[15] - z[27];
    z[24]=z[6]*z[24];
    z[29]= - 7*z[5] + 8*z[30];
    z[29]=z[29]*z[26];
    z[26]=z[26] + z[3];
    z[31]=n<T>(1,2)*z[11];
    z[26]=z[31] - n<T>(4,3)*z[4] + n<T>(7,6)*z[26];
    z[26]=z[2]*z[26];
    z[35]=z[34] - z[16];
    z[30]= - z[3]*z[30];
    z[26]=z[26] + n<T>(1,3)*z[29] + n<T>(8,3)*z[30] - z[10] - n<T>(7,3)*z[9] - 7*
    z[15] - n<T>(23,3)*z[35];
    z[26]=z[2]*z[26];
    z[29]=4*z[9] + z[28];
    z[25]=z[3]*z[25];
    z[25]=z[25] + 2*z[29] + n<T>(37,2)*z[34];
    z[25]=z[3]*z[25];
    z[29]=2*z[5] - z[4];
    z[29]=z[3]*z[29];
    z[29]=n<T>(4,3)*z[29] - n<T>(37,6)*z[34] - n<T>(8,3)*z[9] + z[33];
    z[29]=z[1]*z[29];
    z[29]= - n<T>(13,36)*z[12] + z[29];
    z[29]=i*z[29];
    z[30]=8*z[5] - z[31];
    z[30]=z[30]*z[28];
    z[31]= - z[8]*z[32];
    z[27]=z[31] + z[27];
    z[27]=z[7]*z[27];
    z[31]=z[8]*z[10];
    z[31]=z[31] - z[13];
    z[32]=3*z[20] - z[21] + z[23] - z[22];
    z[33]= - z[17] + z[15];
    z[33]=z[11]*z[33];
    z[28]=z[16] - z[28];
    z[28]=z[4]*z[28];

    r += n<T>(33,4)*z[14] + n<T>(37,6)*z[18] + 6*z[19] + z[24] + n<T>(1,3)*z[25]
     +  z[26] + z[27] + n<T>(23,3)*z[28] + z[29] + z[30] + n<T>(1,6)*z[31] + n<T>(1,2)
      *z[32] + z[33];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf287(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf287(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
