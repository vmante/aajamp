#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf377(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[138];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[44];
    z[3]=f[70];
    z[4]=f[77];
    z[5]=f[79];
    z[6]=f[81];
    z[7]=c[11];
    z[8]=d[0];
    z[9]=d[1];
    z[10]=d[2];
    z[11]=d[3];
    z[12]=d[4];
    z[13]=d[5];
    z[14]=d[6];
    z[15]=d[7];
    z[16]=d[8];
    z[17]=d[9];
    z[18]=c[12];
    z[19]=c[13];
    z[20]=c[20];
    z[21]=c[23];
    z[22]=c[32];
    z[23]=c[33];
    z[24]=c[35];
    z[25]=c[36];
    z[26]=c[37];
    z[27]=c[38];
    z[28]=c[39];
    z[29]=c[40];
    z[30]=c[43];
    z[31]=c[44];
    z[32]=c[47];
    z[33]=c[48];
    z[34]=c[49];
    z[35]=c[50];
    z[36]=c[55];
    z[37]=c[56];
    z[38]=c[57];
    z[39]=c[59];
    z[40]=c[81];
    z[41]=c[84];
    z[42]=f[22];
    z[43]=f[24];
    z[44]=f[42];
    z[45]=f[61];
    z[46]=f[62];
    z[47]=f[69];
    z[48]=f[80];
    z[49]=g[24];
    z[50]=g[27];
    z[51]=g[38];
    z[52]=g[40];
    z[53]=g[48];
    z[54]=g[50];
    z[55]=g[79];
    z[56]=g[82];
    z[57]=g[90];
    z[58]=g[92];
    z[59]=g[95];
    z[60]=g[99];
    z[61]=g[101];
    z[62]=g[110];
    z[63]=g[112];
    z[64]=g[128];
    z[65]=g[131];
    z[66]=g[138];
    z[67]=g[140];
    z[68]=g[141];
    z[69]=g[145];
    z[70]=g[147];
    z[71]=g[155];
    z[72]=g[157];
    z[73]=g[170];
    z[74]=g[173];
    z[75]=g[181];
    z[76]=g[183];
    z[77]=g[189];
    z[78]=g[191];
    z[79]=g[198];
    z[80]=g[200];
    z[81]=g[201];
    z[82]=g[212];
    z[83]=g[214];
    z[84]=g[218];
    z[85]=g[220];
    z[86]=g[224];
    z[87]=g[232];
    z[88]=g[233];
    z[89]=g[234];
    z[90]=g[243];
    z[91]=g[249];
    z[92]=g[251];
    z[93]=g[254];
    z[94]=g[261];
    z[95]=g[270];
    z[96]=g[272];
    z[97]=g[276];
    z[98]=g[278];
    z[99]=g[281];
    z[100]=g[288];
    z[101]=g[289];
    z[102]=g[290];
    z[103]=g[292];
    z[104]=g[294];
    z[105]=g[312];
    z[106]=g[324];
    z[107]=g[339];
    z[108]=g[342];
    z[109]=g[347];
    z[110]=g[348];
    z[111]=g[349];
    z[112]=g[355];
    z[113]=g[357];
    z[114]=g[389];
    z[115]=g[392];
    z[116]=g[395];
    z[117]=g[397];
    z[118]= - 4*z[20] + n<T>(1,5)*z[21];
    z[119]= - n<T>(16,5)*z[19] + n<T>(1,3)*z[118];
    z[120]= - 4*z[119] + n<T>(1643,1215)*z[7];
    z[120]=z[15]*z[120];
    z[121]= - 2*z[119] + n<T>(1009,1215)*z[7];
    z[121]=z[9]*z[121];
    z[122]= - 7*z[119] - n<T>(3191,1215)*z[7];
    z[122]=z[16]*z[122];
    z[120]=z[120] + 8*z[41] - n<T>(65,9)*z[40] + z[121] + z[122];
    z[121]=n<T>(16,3)*z[2] - n<T>(8,3)*z[3] + n<T>(451,9)*z[5] + n<T>(44,9)*z[4]
     - n<T>(16,9)
   *z[6];
    z[121]=z[1]*z[121];
    z[122]=z[14]*z[7];
    z[123]= - z[21] + 20*z[20];
    z[123]=16*z[19] + n<T>(1,3)*z[123];
    z[124]= - n<T>(5,81)*z[7] + z[123];
    z[124]=z[17]*z[124];
    z[125]= - n<T>(1,9)*z[7] - z[123];
    z[125]=z[12]*z[125];
    z[126]= - n<T>(31,405)*z[7] - z[119];
    z[126]=z[11]*z[126];
    z[123]= - 5*z[123] + n<T>(229,243)*z[7];
    z[123]=z[10]*z[123];
    z[119]=113*z[119] + n<T>(2261,135)*z[7];
    z[119]=z[13]*z[119];
    z[118]=n<T>(48,5)*z[19] - z[118];
    z[118]=7*z[118] - n<T>(173,1215)*z[7];
    z[118]=z[8]*z[118];
    z[118]=z[118] + n<T>(1,3)*z[119] + z[123] + 4*z[126] + 5*z[125] + n<T>(22,3)
   *z[124] - n<T>(1016,243)*z[122] + n<T>(40,3)*z[23] - n<T>(32,5)*z[26] - n<T>(18896,135)*z[27]
     - n<T>(218,81)*z[28]
     + 40*z[31]
     + n<T>(505,243)*z[32] - n<T>(496,5)*
    z[34] - 48*z[35] - n<T>(92,3)*z[37] + 6*z[38] + n<T>(47,45)*z[39] + z[121]
    + 2*z[120];
    z[118]=i*z[118];
    z[119]=91*z[48];
    z[120]=n<T>(265,3)*z[47];
    z[121]=n<T>(8,3)*z[6];
    z[122]=8*z[4] - n<T>(374,3)*z[43] - 50*z[46] + z[121] + z[119] + z[120];
    z[123]=n<T>(374,9)*z[2];
    z[122]= - z[123] + n<T>(109,9)*z[3] + n<T>(101,9)*z[5] + n<T>(1,3)*z[122] + 4*
    z[18];
    z[122]=z[10]*z[122];
    z[123]=z[123] - n<T>(19,3)*z[3] - n<T>(145,9)*z[5] - n<T>(188,9)*z[18] + n<T>(10,3)*
    z[4] + n<T>(266,9)*z[43] + n<T>(136,9)*z[46] + n<T>(4,9)*z[44] - n<T>(80,9)*z[45]
     - 
   n<T>(8,9)*z[6] - n<T>(14,9)*z[42] - n<T>(317,9)*z[48] - 27*z[47];
    z[123]=z[8]*z[123];
    z[118]=z[122] + z[123] + z[118] - z[71];
    z[122]=n<T>(251,3)*z[44] + n<T>(109,3)*z[45];
    z[123]=91*z[42];
    z[120]= - n<T>(446,3)*z[18] + 10*z[4] - n<T>(35,3)*z[43] + n<T>(79,3)*z[46] - 
    z[120] + z[123] - z[122];
    z[120]= - n<T>(790,3)*z[2] + n<T>(89,3)*z[3] + 2*z[120] + 83*z[5];
    z[120]=z[9]*z[120];
    z[124]= - z[48] - z[42];
    z[124]=n<T>(1016,3)*z[18] - n<T>(329,3)*z[43] - 31*z[46] + n<T>(269,3)*z[44]
     + 
   n<T>(101,3)*z[45] + 91*z[124] - z[121];
    z[124]=n<T>(38,3)*z[2] + n<T>(233,3)*z[3] + 2*z[124] + n<T>(719,3)*z[5];
    z[124]=z[14]*z[124];
    z[120]=z[124] + z[120];
    z[124]= - 44*z[48] + 265*z[47];
    z[121]= - z[121] + n<T>(1,3)*z[124] - z[123];
    z[121]=n<T>(376,3)*z[18] - n<T>(8,9)*z[4] - n<T>(7,3)*z[43] - n<T>(11,3)*z[46]
     + n<T>(91,3)*z[44]
     + n<T>(1,3)*z[121] - z[45];
    z[121]=n<T>(278,3)*z[2] - z[3] + 2*z[121] + n<T>(557,9)*z[5];
    z[121]=z[16]*z[121];
    z[123]=z[46] + z[42];
    z[124]=z[4] + z[47];
    z[123]= - 371*z[18] + 4*z[6] - 11*z[124] + 7*z[123];
    z[123]= - 40*z[3] + 2*z[123] - 403*z[5];
    z[123]=n<T>(1,3)*z[123] - 16*z[2];
    z[123]=z[15]*z[123];
    z[123]= - z[123] + 4*z[117] + z[116] + z[62] + z[82];
    z[119]=z[2] - n<T>(2872,9)*z[18] + n<T>(76,3)*z[4] + 127*z[43] - n<T>(167,3)*
    z[46] + n<T>(295,3)*z[42] + z[119] + n<T>(22,3)*z[47] - z[122];
    z[119]= - n<T>(257,9)*z[3] - 79*z[5] + n<T>(2,3)*z[119];
    z[119]=z[13]*z[119];
    z[122]= - z[110] + z[63] + z[72];
    z[124]= - z[100] + z[79] + z[87];
    z[125]= - z[101] + z[80] + z[88];
    z[126]= - z[102] + z[81] + z[89];
    z[127]=z[112] + z[94] + z[109];
    z[128]=z[107] + z[114];
    z[129]=z[103] + z[104];
    z[130]=z[95] + z[98];
    z[131]=z[73] + z[76];
    z[132]=z[59] + z[68];
    z[133]=z[51] + z[56];
    z[134]=z[49] + z[52];
    z[135]= - z[44] - z[2] - z[42] + 2*z[48] - z[47];
    z[135]= - n<T>(418,3)*z[18] + 28*z[43] + 130*z[46] + 59*z[45] + 11*
    z[135];
    z[135]=z[17]*z[135];
    z[135]=z[135] - z[111];
    z[136]=z[3] + z[43];
    z[136]=n<T>(13,9)*z[2] + n<T>(22,9)*z[5] + n<T>(200,9)*z[18] - 3*z[4] - n<T>(23,9)*
    z[46] + n<T>(4,9)*z[45] - z[44] - n<T>(26,9)*z[136];
    z[136]=z[12]*z[136];
    z[137]= - z[5] - z[44] + 20*z[45] + 11*z[48] + 2*z[6];
    z[137]=n<T>(1,3)*z[3] + n<T>(8,9)*z[18] - n<T>(19,9)*z[4] + 3*z[43] + n<T>(1,9)*
    z[137];
    z[137]=z[11]*z[137];

    r +=  - n<T>(18484,1215)*z[22] + n<T>(1088,5)*z[24] + 256*z[25] - n<T>(640,3)*
      z[29] - 64*z[30] + n<T>(1096,9)*z[33] - n<T>(68,15)*z[36] - n<T>(1136,9)*
      z[50] + n<T>(1108,27)*z[53] - n<T>(448,9)*z[54] - n<T>(1058,9)*z[55] - n<T>(1430,9)*z[57]
     - n<T>(1028,9)*z[58]
     + n<T>(116,3)*z[60] - n<T>(524,9)*z[61] - n<T>(586,9)*z[64] - n<T>(688,9)*z[65]
     + n<T>(526,9)*z[66] - n<T>(508,9)*z[67]
     + n<T>(188,27)*z[69] - n<T>(592,27)*z[70]
     + n<T>(406,9)*z[74]
     + n<T>(394,9)*z[75] - 36*
      z[77] + n<T>(14,3)*z[78] + n<T>(710,9)*z[83] - n<T>(170,3)*z[84] + n<T>(70,3)*
      z[85] + n<T>(184,27)*z[86] + n<T>(1624,9)*z[90] - n<T>(44,3)*z[91] - n<T>(730,9)*
      z[92] - n<T>(710,27)*z[93] - n<T>(1006,9)*z[96] + n<T>(1348,9)*z[97] - n<T>(1268,27)*z[99]
     + n<T>(112,3)*z[105]
     + 24*z[106]
     + z[108] - n<T>(16,3)*z[113]
       + z[115] + 2*z[118] + z[119] + n<T>(1,3)*z[120] + z[121] - n<T>(8,3)*
      z[122] - n<T>(2,3)*z[123] + n<T>(10,3)*z[124] + n<T>(20,3)*z[125] + n<T>(40,3)*
      z[126] - n<T>(4,3)*z[127] + n<T>(1,9)*z[128] - n<T>(32,9)*z[129] + n<T>(334,3)*
      z[130] + n<T>(1150,9)*z[131] - n<T>(2,9)*z[132] - n<T>(1532,9)*z[133] - n<T>(364,3)*z[134]
     + n<T>(4,9)*z[135]
     + 4*z[136]
     + 8*z[137];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf377(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf377(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
