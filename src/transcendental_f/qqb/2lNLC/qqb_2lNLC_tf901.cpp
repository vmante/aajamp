#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf901(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[57];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[44];
    z[3]=c[11];
    z[4]=d[0];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=c[12];
    z[11]=c[13];
    z[12]=c[20];
    z[13]=c[23];
    z[14]=c[32];
    z[15]=c[33];
    z[16]=c[35];
    z[17]=c[36];
    z[18]=c[37];
    z[19]=c[38];
    z[20]=c[39];
    z[21]=c[40];
    z[22]=c[43];
    z[23]=c[44];
    z[24]=c[47];
    z[25]=c[48];
    z[26]=c[49];
    z[27]=c[50];
    z[28]=c[55];
    z[29]=c[56];
    z[30]=c[57];
    z[31]=c[59];
    z[32]=c[81];
    z[33]=c[84];
    z[34]=d[2];
    z[35]=f[62];
    z[36]=f[79];
    z[37]=d[9];
    z[38]=g[82];
    z[39]=g[90];
    z[40]=g[138];
    z[41]=g[218];
    z[42]=g[243];
    z[43]=g[245];
    z[44]=g[254];
    z[45]=g[272];
    z[46]= - n<T>(24,5)*z[11] - 2*z[12] + n<T>(1,10)*z[13];
    z[47]= - z[4]*z[46];
    z[48]=z[8] - z[5];
    z[48]=2*z[48];
    z[49]=z[48] - n<T>(11,10)*z[4];
    z[49]=z[3]*z[49];
    z[50]= - n<T>(1,30)*z[3] + z[46];
    z[50]=z[9]*z[50];
    z[51]= - n<T>(11,270)*z[3] - z[46];
    z[51]=z[7]*z[51];
    z[46]=n<T>(31,270)*z[3] + z[46];
    z[46]=z[6]*z[46];
    z[52]=z[2]*z[1];
    z[46]=z[52] + z[46] + z[51] + z[50] + n<T>(1,27)*z[49] + z[47] + n<T>(23,36)
   *z[15] + 12*z[18] - n<T>(419,18)*z[19] - n<T>(109,324)*z[20] + 5*z[23] + n<T>(73,270)*z[24]
     - n<T>(12,5)*z[26]
     - 6*z[27]
     - z[29]
     + n<T>(3,4)*z[30] - n<T>(1,30)*
    z[31] + 2*z[33] - n<T>(65,36)*z[32];
    z[46]=i*z[46];
    z[47]=z[34] - z[37];
    z[49]=n<T>(1,2)*z[35];
    z[50]=z[47]*z[49];
    z[51]=n<T>(2,3)*z[10];
    z[52]=z[49] - z[51];
    z[52]=z[7]*z[52];
    z[53]=z[42] - z[43] - z[45] - n<T>(1,3)*z[44];
    z[54]=z[36]*z[5];
    z[49]= - z[36] - z[49];
    z[49]=z[8]*z[49];
    z[48]= - z[10]*z[48];
    z[51]= - z[4]*z[51];
    z[55]=z[36] + n<T>(8,3)*z[10];
    z[55]=z[9]*z[55];
    z[56]= - z[36] - n<T>(4,3)*z[10];
    z[56]=z[6]*z[56];
    z[47]= - z[7] - z[8] + z[47];
    z[47]=z[6] + z[9] - z[4] + n<T>(1,2)*z[47];
    z[47]=z[2]*z[47];

    r +=  - n<T>(53,45)*z[14] + n<T>(72,5)*z[16] + 18*z[17] - 15*z[21] - n<T>(9,2)*
      z[22] + n<T>(19,3)*z[25] - n<T>(3,10)*z[28] + z[38] - z[39] - z[40] + 
      z[41] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + 2
      *z[53] + z[54] + z[55] + z[56];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf901(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf901(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
