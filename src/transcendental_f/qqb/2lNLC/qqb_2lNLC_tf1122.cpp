#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1122(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[40];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[11];
    z[5]=d[2];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[6];
    z[10]=d[7];
    z[11]=d[17];
    z[12]=d[9];
    z[13]=e[7];
    z[14]=e[9];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[31];
    z[18]=e[12];
    z[19]=e[6];
    z[20]=e[14];
    z[21]=f[30];
    z[22]=f[31];
    z[23]=f[33];
    z[24]=f[36];
    z[25]=f[39];
    z[26]=f[50];
    z[27]=f[55];
    z[28]=z[8] - z[10];
    z[29]=n<T>(1,2)*z[9];
    z[30]= - z[29] + z[11] - z[28];
    z[30]=z[9]*z[30];
    z[31]=z[6] + z[12];
    z[32]= - z[5] + z[31];
    z[32]=z[6]*z[32];
    z[33]=z[2] - z[7];
    z[34]= - z[12] - z[33];
    z[34]=z[4]*z[34];
    z[35]=z[10]*z[11];
    z[36]=npow(z[8],2);
    z[37]=z[33] + z[8];
    z[38]= - 2*z[10] + z[5] - z[4] + z[37];
    z[38]=z[3]*z[38];
    z[30]=z[38] + z[30] - n<T>(1,2)*z[36] + z[35] + z[34] + z[32] + z[13] - 
    z[14];
    z[30]=z[1]*z[30];
    z[30]= - n<T>(1,6)*z[16] + z[30];
    z[30]=i*z[30];
    z[32]=n<T>(1,2)*z[8];
    z[34]=z[32] - z[10];
    z[34]=z[34]*z[8];
    z[35]=npow(z[10],2);
    z[34]=z[34] + z[35];
    z[35]= - z[5]*z[33];
    z[36]=npow(z[9],2);
    z[37]= - n<T>(3,2)*z[3] + n<T>(1,2)*z[37] + z[9];
    z[37]=z[3]*z[37];
    z[35]=z[37] + z[36] + z[35] - n<T>(7,6)*z[15] + z[18] - 3*z[13] + z[34];
    z[35]=z[3]*z[35];
    z[36]= - z[10] - z[9];
    z[29]=z[36]*z[29];
    z[36]=n<T>(1,6)*z[15];
    z[37]=z[36] + z[14];
    z[29]=z[29] - 2*z[13] - z[19] + z[37] - z[34];
    z[29]=z[9]*z[29];
    z[34]=z[13] + z[18];
    z[34]=z[34]*z[33];
    z[38]=z[31]*z[6];
    z[39]=n<T>(1,2)*z[38] + z[20] - z[36];
    z[39]=z[6]*z[39];
    z[31]=z[31] + z[33];
    z[31]=z[5]*z[31];
    z[31]= - z[38] + n<T>(1,2)*z[31];
    z[31]=z[5]*z[31];
    z[28]= - z[28]*z[32];
    z[28]=z[28] - z[19] - z[36];
    z[28]=z[8]*z[28];
    z[32]=z[25] + z[22] - z[24];
    z[36]=z[20] + z[18];
    z[36]=z[12]*z[36];
    z[33]=z[11] - n<T>(1,3)*z[12] - n<T>(1,6)*z[33];
    z[33]=z[15]*z[33];
    z[37]= - z[13] + z[37];
    z[37]=z[10]*z[37];

    r +=  - 3*z[17] + z[21] + 4*z[23] - z[26] + z[27] + z[28] + z[29]
       + z[30] + z[31] + n<T>(1,2)*z[32] + z[33] + z[34] + z[35] + z[36] + 
      z[37] + z[39];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1122(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1122(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
