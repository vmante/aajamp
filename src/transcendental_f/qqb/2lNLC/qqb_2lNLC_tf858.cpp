#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf858(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[76];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[9];
    z[14]=d[6];
    z[15]=d[17];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=e[13];
    z[36]=e[7];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[51];
    z[52]=f[55];
    z[53]=f[58];
    z[54]=n<T>(11,4)*z[4];
    z[55]= - z[54] + n<T>(11,4)*z[10];
    z[56]=2*z[12];
    z[57]= - n<T>(108457,9186)*z[2] - 2*z[9] + n<T>(1,2)*z[11] + z[56] - z[55];
    z[57]=z[4]*z[57];
    z[58]= - z[13] + n<T>(202519,12248)*z[9];
    z[55]= - n<T>(597029,73488)*z[7] - n<T>(11,4)*z[2] - n<T>(294257,18372)*z[3]
     + 
    z[58] + z[55];
    z[55]=z[7]*z[55];
    z[59]= - z[58] + n<T>(266699,18372)*z[3];
    z[60]=n<T>(64424,4593)*z[8];
    z[61]= - 10*z[5] - n<T>(37415,36744)*z[7] + n<T>(93269,18372)*z[4] + n<T>(71711,6124)*z[2]
     + z[60]
     + z[59];
    z[61]=z[5]*z[61];
    z[62]=n<T>(62893,2)*z[10];
    z[63]=n<T>(113599,2)*z[15] + 151139*z[14];
    z[63]=n<T>(1,3)*z[63] - z[62];
    z[63]= - n<T>(76733,4)*z[6] - n<T>(161243,12)*z[5] + n<T>(38153,4)*z[4] + n<T>(106493,12)*z[2]
     + n<T>(113599,12)*z[9]
     + n<T>(1,2)*z[63]
     + 4991*z[3];
    z[63]=z[6]*z[63];
    z[64]=2*z[19];
    z[65]=npow(z[10],2);
    z[66]=z[23] - n<T>(113599,9186)*z[21];
    z[67]=z[15] - n<T>(1,2)*z[14];
    z[67]=z[14]*z[67];
    z[68]=39071*z[8] - n<T>(41459,8)*z[3];
    z[68]=z[3]*z[68];
    z[69]= - n<T>(1209661,73488)*z[9] + n<T>(62893,6124)*z[10] + z[56] - n<T>(151139,9186)*z[14];
    z[69]=z[9]*z[69];
    z[70]= - 49053*z[3] + 3247*z[2];
    z[70]=z[2]*z[70];
    z[55]=n<T>(1,1531)*z[63] + z[61] + z[55] + z[57] + n<T>(1,6124)*z[70] + 
    z[69] + n<T>(1,4593)*z[68] + n<T>(45991,6124)*z[65] + n<T>(168715,18372)*z[67]
    - n<T>(8451,3062)*z[22] + n<T>(70304,4593)*z[16] - n<T>(11329,3062)*z[17] - 
   n<T>(64424,4593)*z[18] + z[64] + n<T>(1,2)*z[66] - n<T>(11696,4593)*z[20];
    z[55]=z[1]*z[55];
    z[57]=n<T>(32069,8)*z[25] - 27743*z[30] - n<T>(3247,2)*z[28];
    z[55]=n<T>(1,3062)*z[57] + z[55];
    z[55]=i*z[55];
    z[57]=npow(z[14],2);
    z[61]=npow(z[3],2);
    z[63]=n<T>(113599,4)*z[21] + 5848*z[20];
    z[62]= - z[62] + n<T>(151139,3)*z[14];
    z[66]=n<T>(443435,12)*z[9] + z[62];
    z[66]=z[9]*z[66];
    z[63]=n<T>(1,2)*z[66] + n<T>(8451,4)*z[61] - n<T>(53707,8)*z[65] - n<T>(375581,24)*
    z[24] - n<T>(168715,24)*z[57] + n<T>(1,3)*z[63] + n<T>(8451,2)*z[22];
    z[66]=n<T>(1,2)*z[4];
    z[67]= - z[13] - n<T>(41215,6124)*z[4];
    z[67]=z[67]*z[66];
    z[68]=n<T>(1,2)*z[7];
    z[69]= - z[68] + z[4] + z[3] + n<T>(15,2)*z[2];
    z[69]=z[69]*z[68];
    z[70]= - n<T>(2924,4593)*z[2] + n<T>(1,2)*z[13] - z[3];
    z[70]=z[2]*z[70];
    z[71]=n<T>(161243,36744)*z[5] - z[68] + n<T>(38153,6124)*z[4] + n<T>(1,2)*z[3]
    - n<T>(66629,4593)*z[2];
    z[71]=z[5]*z[71];
    z[72]= - n<T>(415877,3)*z[14] + 62893*z[10];
    z[72]= - n<T>(35091,2)*z[4] + n<T>(183415,6)*z[2] - n<T>(113599,6)*z[9] + n<T>(1,2)*
    z[72] - 8451*z[3];
    z[72]=n<T>(76733,9186)*z[6] + n<T>(152057,9186)*z[5] + n<T>(1,1531)*z[72] - n<T>(15,2)*z[7];
    z[72]=z[6]*z[72];
    z[63]=n<T>(1,4)*z[72] + z[71] + z[69] + z[67] + n<T>(1,1531)*z[63] + z[70];
    z[63]=z[6]*z[63];
    z[67]= - 3*z[4] + n<T>(90085,9186)*z[2] + z[13] + n<T>(9,4)*z[10];
    z[67]=z[67]*z[66];
    z[69]=npow(z[9],2);
    z[70]=2*z[69];
    z[71]=npow(z[2],2);
    z[67]=z[67] + n<T>(8390,4593)*z[71] + z[70] - n<T>(25,8)*z[65] + n<T>(36346,13779)*z[24]
     - n<T>(35152,4593)*z[16]
     - n<T>(1,2)*z[23]
     - z[64];
    z[67]=z[4]*z[67];
    z[71]=n<T>(11329,4)*z[17];
    z[72]=n<T>(107597,16)*z[69] - n<T>(39071,6)*z[61] - n<T>(1576381,144)*z[24] + 
   n<T>(64424,3)*z[18] + z[71];
    z[73]=n<T>(1,2)*z[2];
    z[74]= - n<T>(11329,6124)*z[2] + n<T>(58239,3062)*z[3] - z[13] - z[10];
    z[74]=z[74]*z[73];
    z[59]=n<T>(505169,73488)*z[7] - z[73] + z[59];
    z[59]=z[7]*z[59];
    z[73]= - z[13] + z[10];
    z[73]= - n<T>(84083,36744)*z[4] + n<T>(1,2)*z[73] - n<T>(7594,4593)*z[2];
    z[73]=z[4]*z[73];
    z[75]=n<T>(74159,12)*z[7] - n<T>(93269,6)*z[4] + 19291*z[2] - n<T>(354149,3)*
    z[3] + n<T>(202519,4)*z[9];
    z[75]=n<T>(1,6124)*z[75] + n<T>(5,3)*z[5];
    z[75]=z[5]*z[75];
    z[59]=z[75] + z[59] + z[73] + n<T>(1,1531)*z[72] + z[74];
    z[59]=z[5]*z[59];
    z[62]= - n<T>(58545,2)*z[9] - n<T>(1,2)*z[62] - 37601*z[3];
    z[62]=z[9]*z[62];
    z[62]=z[62] - n<T>(151139,3)*z[34] + n<T>(70609,2)*z[33];
    z[62]=n<T>(3,2)*z[61] + n<T>(62893,12248)*z[65] - n<T>(1032803,110232)*z[24]
     - 
    z[64] + n<T>(1,3062)*z[62];
    z[62]=z[9]*z[62];
    z[64]= - 21*z[37] + n<T>(312629,4593)*z[22];
    z[64]=n<T>(156467,12248)*z[69] - n<T>(73549,9186)*z[61] + n<T>(11,4)*z[65] + n<T>(1,2)*z[64]
     - n<T>(197743,13779)*z[24];
    z[54]=z[54] - n<T>(11,2)*z[10] - z[2];
    z[54]=z[54]*z[66];
    z[66]=z[2] - z[10];
    z[58]= - n<T>(23,6)*z[7] + n<T>(27,4)*z[4] - n<T>(158611,4593)*z[3] + z[58] - n<T>(15,4)*z[66];
    z[58]=z[58]*z[68];
    z[66]=z[2]*z[3];
    z[54]=z[58] + z[54] + n<T>(1,2)*z[64] + z[66];
    z[54]=z[7]*z[54];
    z[58]=z[11] + n<T>(113599,9186)*z[15];
    z[56]=n<T>(72079,18372)*z[14] - n<T>(15371,18372)*z[13] + z[60] + n<T>(1,2)*
    z[58] + z[56];
    z[56]=z[24]*z[56];
    z[58]= - n<T>(55177,8)*z[61] + n<T>(26129,2)*z[24] - n<T>(35152,3)*z[16] + n<T>(5848,3)*z[20]
     + z[71];
    z[60]= - n<T>(3247,3062)*z[2] + z[10] - z[3];
    z[60]=z[2]*z[60];
    z[58]=n<T>(1,4)*z[60] + n<T>(1,1531)*z[58] - z[70];
    z[58]=z[2]*z[58];
    z[60]=z[29] - z[42];
    z[64]=5*z[53] - 9*z[52];
    z[64]= - n<T>(53707,1531)*z[48] - n<T>(443435,4593)*z[49] + n<T>(62893,1531)*
    z[50] + 3*z[64] - 25*z[51];
    z[66]= - n<T>(7,2)*z[37] + z[35];
    z[66]=z[13]*z[66];
    z[57]=n<T>(150343,36744)*z[57] + n<T>(168715,18372)*z[21] - n<T>(3,2)*z[36] - 
   n<T>(82459,4593)*z[34];
    z[57]=z[14]*z[57];
    z[65]=n<T>(1,2)*z[65] + z[35] - n<T>(25047,6124)*z[24];
    z[65]=z[10]*z[65];
    z[61]=n<T>(138371,6)*z[61] - n<T>(95105,24)*z[24] + n<T>(61975,6)*z[22] + n<T>(61423,4)*z[33]
     + n<T>(39071,3)*z[18];
    z[61]=z[3]*z[61];

    r +=  - n<T>(32457,1531)*z[26] + n<T>(143729,73488)*z[27] + n<T>(27743,6124)*
      z[31] + n<T>(2914817,36744)*z[32] - 4*z[38] + 2*z[39] - n<T>(93269,36744)
      *z[40] + n<T>(505169,73488)*z[41] + n<T>(161243,36744)*z[43] - n<T>(107597,24496)*z[44]
     + n<T>(156467,24496)*z[45] - n<T>(38153,12248)*z[46] - n<T>(4,3)
      *z[47] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + n<T>(3247,12248)*z[60]
     + n<T>(1,1531)*z[61]
     + z[62]
     + z[63]
     + n<T>(1,8)*z[64]
     +  z[65] + n<T>(3,2)*z[66] + z[67];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf858(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf858(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
