#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1177(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[30];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[7];
    z[6]=d[17];
    z[7]=e[9];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[17];
    z[12]=c[31];
    z[13]=e[6];
    z[14]=f[30];
    z[15]=f[31];
    z[16]=f[36];
    z[17]=f[39];
    z[18]=f[40];
    z[19]=z[1]*i;
    z[20]=3*z[2];
    z[21]= - z[20] - z[5];
    z[21]=z[21]*z[19];
    z[22]=npow(z[2],2);
    z[23]= - z[5]*z[20];
    z[21]=z[21] - n<T>(5,2)*z[22] + z[23];
    z[23]=n<T>(1,2)*z[19];
    z[24]=n<T>(11,12)*z[4] - z[23] + z[2] - n<T>(3,2)*z[5];
    z[24]=z[4]*z[24];
    z[21]=n<T>(1,2)*z[21] + z[24];
    z[21]=z[4]*z[21];
    z[24]=z[5] - z[2];
    z[25]= - z[19] + z[24];
    z[25]=z[3]*z[25];
    z[25]=z[22] - z[25];
    z[24]=z[24]*z[19];
    z[26]=n<T>(1,2)*z[5];
    z[27]=z[2] - z[26];
    z[27]=z[5]*z[27];
    z[24]=z[24] + z[27] - n<T>(1,2)*z[25];
    z[24]=z[3]*z[24];
    z[22]=n<T>(15,8)*z[22];
    z[25]=n<T>(3,8)*z[2] + n<T>(1,3)*z[5];
    z[25]=z[5]*z[25];
    z[25]=z[22] + z[25];
    z[25]=z[5]*z[25];
    z[27]= - n<T>(3,4)*z[2] - z[5];
    z[27]=z[5]*z[27];
    z[22]= - z[22] + z[27];
    z[22]=z[22]*z[19];
    z[20]=2*z[4] + z[26] + z[20];
    z[19]=z[19]*z[20];
    z[19]=n<T>(1,2)*z[8] + z[19];
    z[19]=z[6]*z[19];
    z[20]= - z[23] + z[20];
    z[20]=z[7]*z[20];
    z[23]=z[17] - z[15];
    z[26]= - z[2] - z[4];
    z[26]=z[13]*z[26];
    z[26]=z[26] + z[14];
    z[27]=n<T>(9,4)*z[11] + n<T>(5,12)*z[9];
    z[27]=i*z[27];
    z[28]=npow(z[2],3);
    z[29]=z[3] - n<T>(1,3)*z[2] - n<T>(5,2)*z[5];
    z[29]= - n<T>(1,3)*z[4] + n<T>(1,4)*z[29];
    z[29]=z[8]*z[29];

    r +=  - n<T>(13,8)*z[10] + n<T>(45,16)*z[12] - n<T>(7,8)*z[16] + z[18] + z[19]
       + z[20] + z[21] + z[22] - n<T>(3,8)*z[23] + n<T>(3,4)*z[24] + z[25] + n<T>(1,2)*z[26]
     + z[27]
     + n<T>(1,12)*z[28]
     + z[29];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1177(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1177(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
