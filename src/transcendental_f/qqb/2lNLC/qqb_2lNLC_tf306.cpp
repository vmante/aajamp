#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf306(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[66];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[9];
    z[8]=d[15];
    z[9]=d[2];
    z[10]=d[5];
    z[11]=d[4];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[3];
    z[30]=e[6];
    z[31]=e[14];
    z[32]=e[13];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[23];
    z[39]=f[31];
    z[40]=f[36];
    z[41]=f[39];
    z[42]=f[51];
    z[43]=f[55];
    z[44]=f[58];
    z[45]=f[74];
    z[46]=n<T>(1,2)*z[5];
    z[47]=z[1]*i;
    z[48]=z[46] - z[47];
    z[48]=z[48]*z[5];
    z[49]=n<T>(3,2)*z[6];
    z[50]= - 11 - z[6];
    z[50]=z[50]*z[49];
    z[51]=z[47] - z[5];
    z[52]=2*z[3] + z[51];
    z[52]=z[3]*z[52];
    z[53]=npow(z[10],2);
    z[54]=z[47] - z[4];
    z[54]=n<T>(1,2)*z[54] + z[7];
    z[54]=z[7]*z[54];
    z[50]=z[32] + z[54] + n<T>(1,2)*z[53] + z[52] + z[50] + z[48];
    z[50]=z[7]*z[50];
    z[52]=n<T>(1,2)*z[6];
    z[53]=n<T>(5,12)*z[3] - z[46] + n<T>(5,3)*z[47] - z[52];
    z[53]=z[3]*z[53];
    z[54]=n<T>(5,3)*z[5];
    z[55]= - n<T>(10,3)*z[2] + n<T>(5,3)*z[4] - z[3] + z[54] + n<T>(20,3)*z[47] + 
    z[6];
    z[55]=z[2]*z[55];
    z[56]=5*z[47];
    z[57]= - z[56] + n<T>(1,4)*z[6];
    z[57]=z[6]*z[57];
    z[55]=z[57] + z[55];
    z[57]= - z[52] + n<T>(20,9)*z[47];
    z[58]=n<T>(10,9)*z[5];
    z[59]=z[57] - z[58];
    z[59]=z[59]*z[5];
    z[57]=n<T>(10,9)*z[4] - n<T>(1,2)*z[3] - z[58] - z[57];
    z[57]=z[4]*z[57];
    z[53]=z[57] + z[53] - z[59] + n<T>(1,3)*z[55];
    z[53]=z[2]*z[53];
    z[55]=z[52] + z[47];
    z[57]=z[6]*z[55];
    z[58]=n<T>(14,3)*z[11];
    z[60]= - 2*z[51] - z[11];
    z[60]=z[60]*z[58];
    z[61]=5*z[6];
    z[62]=23*z[47];
    z[63]= - z[61] - 33 - z[62];
    z[63]=n<T>(1,2)*z[63] + 11*z[5];
    z[58]=n<T>(1,6)*z[10] + n<T>(1,2)*z[63] - z[58];
    z[58]=z[10]*z[58];
    z[57]=z[58] + z[60] - n<T>(5,2)*z[57] - n<T>(29,3)*z[48];
    z[57]=z[10]*z[57];
    z[54]= - z[47] + z[54];
    z[54]=z[5]*z[54];
    z[58]=n<T>(1,2)*z[10];
    z[60]=33 - z[10];
    z[60]=z[60]*z[58];
    z[63]=npow(z[3],2);
    z[64]=4*z[3] - n<T>(1,2)*z[11];
    z[64]=z[11]*z[64];
    z[65]= - z[5] + z[4];
    z[65]=n<T>(1,2)*z[65] - n<T>(2,9)*z[12];
    z[65]=z[12]*z[65];
    z[54]=z[65] + z[60] + z[64] + z[54] - n<T>(7,2)*z[63];
    z[54]=z[12]*z[54];
    z[60]=35*z[47] + 41*z[6];
    z[46]= - n<T>(149,12)*z[3] + n<T>(1,3)*z[60] + z[46];
    z[46]=z[3]*z[46];
    z[60]=n<T>(29,3)*z[3];
    z[64]=z[47] + n<T>(17,6)*z[6];
    z[64]= - n<T>(13,18)*z[4] - z[60] + n<T>(1,2)*z[64] + n<T>(5,9)*z[5];
    z[64]=z[4]*z[64];
    z[65]= - 17*z[47] - z[6];
    z[65]=z[6]*z[65];
    z[46]=z[64] + z[46] + n<T>(1,6)*z[65] + z[59];
    z[46]=z[4]*z[46];
    z[59]=z[47] + z[6];
    z[64]=n<T>(1,2)*z[4] - z[59];
    z[64]=z[4]*z[64];
    z[63]=z[63] - z[64];
    z[49]=z[55]*z[49];
    z[48]=z[49] + n<T>(1,3)*z[48] - n<T>(3,2)*z[63];
    z[49]= - n<T>(29,3)*z[47] + 35*z[6];
    z[49]=n<T>(43,9)*z[11] + n<T>(185,12)*z[4] - z[60] + n<T>(1,4)*z[49] - 13*z[5];
    z[49]=z[11]*z[49];
    z[48]=11*z[48] + z[49];
    z[48]=z[11]*z[48];
    z[49]=n<T>(133,2)*z[3];
    z[55]=z[47]*z[49];
    z[60]=z[4]*z[62];
    z[55]=23*z[20] + z[55] + z[60];
    z[55]=z[8]*z[55];
    z[49]=23*z[4] - z[62] + z[49];
    z[49]=z[15]*z[49];
    z[60]= - 127*z[6] - n<T>(83,3)*z[5];
    z[60]=n<T>(233,18)*z[4] + n<T>(1,3)*z[60] + 47*z[3];
    z[60]=n<T>(83,24)*z[12] - n<T>(1,24)*z[7] + n<T>(38,3)*z[10] - n<T>(106,3)*z[11]
     + n<T>(1,4)*z[60]
     + n<T>(5,9)*z[2];
    z[60]=z[20]*z[60];
    z[49]=z[49] + z[60] + z[55];
    z[55]=5*z[59] + z[58];
    z[55]=z[10]*z[55];
    z[58]= - z[56] - n<T>(11,2)*z[6];
    z[58]=z[6]*z[58];
    z[59]=z[6] - z[10];
    z[59]=z[9]*z[59];
    z[55]=n<T>(5,2)*z[59] - n<T>(5,6)*z[20] + z[58] + z[55];
    z[55]=z[9]*z[55];
    z[58]=69*z[3] + n<T>(119,3)*z[11];
    z[58]=z[29]*z[58];
    z[55]=z[33] + z[55] + z[58];
    z[58]= - n<T>(29,3)*z[6] + 33 - n<T>(83,3)*z[47];
    z[52]=z[58]*z[52];
    z[58]=n<T>(1,3)*z[5];
    z[56]=n<T>(5,2)*z[5] - z[56] + 2*z[6];
    z[56]=z[56]*z[58];
    z[58]= - n<T>(33,2) - n<T>(61,3)*z[47];
    z[58]=n<T>(259,9)*z[3] + n<T>(1,2)*z[58] - n<T>(32,3)*z[6];
    z[58]=z[3]*z[58];
    z[52]=z[58] + z[52] + z[56];
    z[52]=z[3]*z[52];
    z[56]= - z[2] + 2*z[47];
    z[58]= - z[4] + z[56];
    z[58]=z[14]*z[58];
    z[56]= - z[5] + z[56];
    z[56]=z[17]*z[56];
    z[56]= - z[25] - z[58] - z[56] + z[35] + z[34];
    z[58]= - z[5]*z[47];
    z[58]=z[58] - z[20];
    z[58]=z[13]*z[58];
    z[59]=z[18]*z[51];
    z[58]=z[58] + z[59];
    z[59]=n<T>(23,3)*z[47] + z[61];
    z[59]=n<T>(1,2)*z[59] - z[5];
    z[59]=z[59]*npow(z[6],2);
    z[60]=z[43] + z[42];
    z[61]= - n<T>(40,9)*z[26] + n<T>(71,24)*z[21];
    z[61]=i*z[61];
    z[47]=z[2] + n<T>(67,6)*z[3] - n<T>(4,3)*z[5] + n<T>(1,3)*z[47] + 6*z[6];
    z[47]=z[19]*z[47];
    z[51]=z[10] - z[51];
    z[51]=n<T>(1,3)*z[51] - z[12];
    z[51]=z[16]*z[51];
    z[62]=9*z[11] + n<T>(31,3)*z[12];
    z[62]=z[30]*z[62];
    z[63]=3*z[6] + z[7];
    z[63]=z[31]*z[63];
    z[64]=n<T>(20,9)*i;
    z[64]= - z[24]*z[64];

    r +=  - n<T>(55,4)*z[22] - n<T>(5,27)*z[23] + n<T>(20,9)*z[27] + n<T>(1081,72)*
      z[28] - n<T>(173,12)*z[36] + n<T>(37,4)*z[37] - n<T>(2,3)*z[38] + n<T>(16,3)*
      z[39] + n<T>(13,2)*z[40] - n<T>(19,6)*z[41] - n<T>(11,4)*z[44] + z[45] + 
      z[46] + z[47] + z[48] + n<T>(1,3)*z[49] + z[50] + z[51] + z[52] + 
      z[53] + z[54] + n<T>(1,2)*z[55] - n<T>(10,9)*z[56] + z[57] + n<T>(7,3)*z[58]
       + z[59] + n<T>(1,4)*z[60] + z[61] + z[62] + n<T>(3,2)*z[63] + z[64];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf306(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf306(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
