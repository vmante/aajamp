#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf379(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[95];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[77];
    z[3]=f[81];
    z[4]=c[11];
    z[5]=d[0];
    z[6]=d[1];
    z[7]=d[2];
    z[8]=d[3];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[8];
    z[13]=d[9];
    z[14]=c[12];
    z[15]=c[13];
    z[16]=c[20];
    z[17]=c[23];
    z[18]=c[32];
    z[19]=c[33];
    z[20]=c[35];
    z[21]=c[36];
    z[22]=c[37];
    z[23]=c[38];
    z[24]=c[39];
    z[25]=c[40];
    z[26]=c[43];
    z[27]=c[44];
    z[28]=c[47];
    z[29]=c[48];
    z[30]=c[49];
    z[31]=c[50];
    z[32]=c[55];
    z[33]=c[56];
    z[34]=c[57];
    z[35]=c[59];
    z[36]=c[81];
    z[37]=c[84];
    z[38]=f[22];
    z[39]=f[24];
    z[40]=f[62];
    z[41]=f[69];
    z[42]=f[70];
    z[43]=f[79];
    z[44]=f[80];
    z[45]=f[42];
    z[46]=f[44];
    z[47]=d[7];
    z[48]=g[24];
    z[49]=g[27];
    z[50]=g[40];
    z[51]=g[48];
    z[52]=g[50];
    z[53]=g[82];
    z[54]=g[90];
    z[55]=g[92];
    z[56]=g[95];
    z[57]=g[101];
    z[58]=g[104];
    z[59]=g[131];
    z[60]=g[138];
    z[61]=g[147];
    z[62]=g[189];
    z[63]=g[191];
    z[64]=g[212];
    z[65]=g[214];
    z[66]=g[220];
    z[67]=g[224];
    z[68]=g[245];
    z[69]=g[251];
    z[70]=g[270];
    z[71]=g[272];
    z[72]=g[276];
    z[73]=g[278];
    z[74]=g[281];
    z[75]=g[292];
    z[76]=g[294];
    z[77]=g[312];
    z[78]=g[324];
    z[79]= - 2*z[5] + z[10];
    z[80]= - 4*z[16] + n<T>(1,5)*z[17];
    z[81]=z[80] - n<T>(48,5)*z[15];
    z[82]=z[81] + n<T>(53,405)*z[4];
    z[79]=z[82]*z[79];
    z[82]=n<T>(8,3)*z[3];
    z[83]=n<T>(4,3)*z[2];
    z[84]= - z[83] - z[82];
    z[84]=z[1]*z[84];
    z[85]= - n<T>(1,15)*z[4] + z[81];
    z[85]=z[8]*z[85];
    z[85]=z[85] + z[34];
    z[86]=8*z[37] - n<T>(65,9)*z[36];
    z[86]=2*z[86] + z[35];
    z[87]=z[7] + n<T>(1,3)*z[11];
    z[87]=z[4]*z[87];
    z[80]= - n<T>(16,5)*z[15] + n<T>(1,3)*z[80];
    z[88]=n<T>(29,405)*z[4] - z[80];
    z[88]=z[12]*z[88];
    z[81]= - n<T>(67,405)*z[4] + z[81];
    z[81]=z[13]*z[81];
    z[89]=n<T>(1,45)*z[4] - z[80];
    z[89]=z[9]*z[89];
    z[80]= - n<T>(37,135)*z[4] - z[80];
    z[80]=z[6]*z[80];
    z[79]=z[80] + z[89] + z[81] + 4*z[88] + n<T>(8,27)*z[87] + n<T>(35,27)*z[19]
    - n<T>(96,5)*z[22] - n<T>(2758,135)*z[23] - n<T>(218,243)*z[24] + n<T>(40,3)*z[27]
    + n<T>(343,405)*z[28] - n<T>(176,5)*z[30] - 16*z[31] - n<T>(28,3)*z[33] + n<T>(1,3)
   *z[86] + z[84] + z[79] + 2*z[85];
    z[79]=i*z[79];
    z[79]= - z[79] + z[32] + z[53] + z[55];
    z[80]=z[39] + n<T>(1,3)*z[43];
    z[81]=n<T>(1,3)*z[38];
    z[84]=n<T>(1,3)*z[3];
    z[85]=n<T>(1,3)*z[40];
    z[86]= - z[46] - z[44];
    z[86]=z[85] - z[84] - n<T>(2,3)*z[14] - z[81] + n<T>(1,3)*z[86] - z[80];
    z[86]=z[11]*z[86];
    z[87]=n<T>(2,3)*z[44];
    z[88]=n<T>(1,3)*z[2];
    z[80]= - z[88] + z[84] + n<T>(8,3)*z[14] - z[87] - z[80];
    z[80]=z[8]*z[80];
    z[84]= - z[70] - z[71] + z[72] - z[73] + z[75] + z[76];
    z[89]= - z[69] + z[64] - z[65];
    z[90]=z[60] - z[66] + z[67] - z[68];
    z[91]=z[63] + z[74];
    z[92]=z[61] + z[62];
    z[80]=z[80] + z[86] - z[59] + z[78] - 2*z[77] - n<T>(1,9)*z[92] + n<T>(2,9)*
    z[91] + n<T>(1,3)*z[90] - n<T>(4,3)*z[89] - n<T>(2,3)*z[84];
    z[84]=n<T>(1,3)*z[41];
    z[86]=n<T>(4,3)*z[43];
    z[89]= - z[45] + n<T>(4,3)*z[39];
    z[90]=n<T>(1,3)*z[46];
    z[81]=n<T>(2,3)*z[2] - n<T>(4,3)*z[40] + z[86] - z[81] + z[87] + z[90] - 
    z[84] + z[89];
    z[81]=z[10]*z[81];
    z[87]=z[89] - z[90];
    z[89]=z[43] - z[42];
    z[89]= - n<T>(2,3)*z[89];
    z[85]= - z[88] + z[85] + n<T>(28,9)*z[14] + n<T>(2,3)*z[38] + z[87] - z[89];
    z[85]=z[6]*z[85];
    z[91]=z[41] + 2*z[42];
    z[92]=2*z[44];
    z[93]=z[92] - z[91];
    z[94]=z[40] + z[38];
    z[86]= - z[88] + z[86] - n<T>(2,3)*z[3] + n<T>(1,3)*z[93] + 2*z[39] + z[94];
    z[86]=z[5]*z[86];
    z[84]=n<T>(2,3)*z[40] + 4*z[14] + z[38] - n<T>(4,3)*z[44] + z[84] - z[87];
    z[84]=z[13]*z[84];
    z[87]= - z[2] - z[40] - n<T>(8,9)*z[14] + n<T>(2,3)*z[39] + z[89] + z[90] + 
    z[45];
    z[87]=z[9]*z[87];
    z[81]=z[81] + z[84] + z[87] + z[85] + z[86];
    z[84]=n<T>(4,3)*z[91] - 4*z[94];
    z[84]=z[47]*z[84];
    z[85]=z[47] + z[7];
    z[82]=z[85]*z[82];
    z[85]=z[47] + 4*z[7];
    z[83]=z[85]*z[83];
    z[85]=z[48] + z[50];
    z[86]=z[40] - z[43] - z[3] - n<T>(28,3)*z[14] + z[92] - z[38];
    z[86]=z[12]*z[86];
    z[86]=z[86] - z[54];
    z[87]=z[43] - z[44];
    z[87]= - 16*z[14] - n<T>(8,3)*z[87];
    z[87]=z[7]*z[87];

    r +=  - n<T>(62,9)*z[18] + 96*z[20] + 112*z[21] - n<T>(280,3)*z[25] - 28*
      z[26] + n<T>(112,3)*z[29] - 16*z[49] + n<T>(16,9)*z[51] - n<T>(32,9)*z[52]
     +  n<T>(1,3)*z[56] + n<T>(8,9)*z[57] + z[58] - 2*z[79] + 8*z[80] + 4*z[81]
       + z[82] + z[83] + z[84] - n<T>(16,3)*z[85] + n<T>(8,3)*z[86] + z[87];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf379(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf379(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
