#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1043(
  const std::array<T, 85>& c
) {
    return -c[3];
}

template std::complex<double> qqb_2lNLC_tf1043(
  const std::array<double,85>& c
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1043(
  const std::array<dd_real,85>& c
);
#endif
