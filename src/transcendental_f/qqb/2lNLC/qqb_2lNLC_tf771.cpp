#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf771(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[56];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[6];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=c[3];
    z[21]=d[9];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[3];
    z[31]=f[3];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[11];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[17];
    z[38]=f[31];
    z[39]=f[36];
    z[40]=f[39];
    z[41]=n<T>(1,2)*z[6];
    z[42]=z[1]*i;
    z[43]=z[41] - z[42];
    z[44]=z[6]*z[43];
    z[45]=1469*z[6];
    z[46]= - n<T>(41309,4)*z[42] + z[45];
    z[46]= - n<T>(1297,3)*z[9] + n<T>(1,3)*z[46] + n<T>(11811,4)*z[5];
    z[46]=z[9]*z[46];
    z[47]= - z[42] + n<T>(1,2)*z[5];
    z[47]=z[47]*z[5];
    z[44]=z[46] - n<T>(4937,3)*z[44] + n<T>(2625,2)*z[47];
    z[44]=z[9]*z[44];
    z[41]= - 3*z[42] + z[41];
    z[41]=z[41]*npow(z[6],2);
    z[46]=2*z[42];
    z[48]=z[46] - z[6];
    z[49]= - z[48] + z[5];
    z[49]=z[5]*z[6]*z[49];
    z[41]=3*z[41] + 4*z[49];
    z[49]=z[42] - z[5];
    z[50]=7*z[49] + n<T>(5,2)*z[9];
    z[50]=z[9]*z[50];
    z[47]=11*z[47] + 3*z[50];
    z[50]= - 5*z[49] - z[3];
    z[50]=z[3]*z[50];
    z[47]=n<T>(1,2)*z[47] + z[50];
    z[50]=2083*z[49] + 875*z[9];
    z[50]=n<T>(1,4)*z[50] + 302*z[3];
    z[50]=z[7]*z[50];
    z[47]=125*z[47] + 3*z[50];
    z[47]=z[7]*z[47];
    z[50]=z[42] - z[6];
    z[51]=z[9] + 2*z[50];
    z[51]=z[51]*z[9];
    z[52]= - z[6]*z[48];
    z[52]=z[52] + z[51];
    z[53]= - z[9] - z[50];
    z[53]=z[10]*z[53];
    z[52]=n<T>(781,2)*z[53] + 375*z[52] - n<T>(2281,3)*z[20];
    z[52]=z[10]*z[52];
    z[53]=1781*z[3];
    z[54]=1000*z[5] + z[53];
    z[54]=z[42]*z[54];
    z[54]=1000*z[20] + z[54];
    z[54]=z[8]*z[54];
    z[53]= - 1000*z[49] + z[53];
    z[53]=z[15]*z[53];
    z[55]=2656*z[9] + 4187*z[3];
    z[55]=z[30]*z[55];
    z[41]=z[47] + 125*z[41] + z[44] + z[52] + z[54] + z[53] + z[55];
    z[44]=14*z[42] - n<T>(5,2)*z[6];
    z[44]=z[6]*z[44];
    z[47]= - 5*z[5] + 13*z[42] - 2*z[6];
    z[47]=z[5]*z[47];
    z[49]= - n<T>(1,2)*z[3] - z[49];
    z[49]=z[3]*z[49];
    z[52]=8*z[2] - n<T>(5,2)*z[5] - 16*z[42] - n<T>(7,2)*z[6];
    z[52]=z[2]*z[52];
    z[44]=z[52] + 3*z[49] + z[44] + z[47];
    z[44]=z[2]*z[44];
    z[47]=3*z[6];
    z[49]= - z[48]*z[47];
    z[52]= - z[42] - z[47];
    z[52]=2*z[52] + z[5];
    z[52]=z[5]*z[52];
    z[53]=z[46] + z[5];
    z[53]=2*z[53] - z[2];
    z[53]=z[2]*z[53];
    z[54]= - 4*z[2] + z[47] + z[5];
    z[54]=z[4]*z[54];
    z[49]=z[54] + 2*z[53] + z[49] + z[52];
    z[49]=z[4]*z[49];
    z[44]=z[31] + z[44] + z[49];
    z[45]=2594*z[42] - z[45];
    z[45]=z[6]*z[45];
    z[49]= - z[6] - z[42] + z[11];
    z[49]=z[11]*z[49];
    z[45]=172*z[49] + z[45] - 1297*z[51];
    z[45]=z[11]*z[45];
    z[49]=z[11] + z[6];
    z[49]=z[42]*z[49];
    z[49]=z[20] + z[49];
    z[49]=z[12]*z[49];
    z[51]=z[11] - z[50];
    z[51]=z[18]*z[51];
    z[49]=z[49] + z[51];
    z[43]= - z[43]*z[47];
    z[47]=5*z[42] - 8*z[5];
    z[47]=z[5]*z[47];
    z[43]=z[43] + z[47];
    z[47]=npow(z[9],2);
    z[43]=125*z[43] - n<T>(3781,2)*z[47];
    z[42]=z[42] + z[5];
    z[42]=375*z[6] - 1781*z[42];
    z[42]=n<T>(6265,4593)*z[3] + n<T>(1,3062)*z[42] - z[9];
    z[42]=z[3]*z[42];
    z[42]=n<T>(1,1531)*z[43] + z[42];
    z[42]=z[3]*z[42];
    z[43]=z[33] - z[26];
    z[47]=z[40] - z[38];
    z[46]=z[46] - z[2];
    z[51]=z[4] - z[46];
    z[51]=z[13]*z[51];
    z[51]=z[51] + z[34];
    z[52]=250*z[11] - n<T>(125,3)*z[4] + 177*z[7] + n<T>(625,3)*z[2] + n<T>(203,3)*
    z[3] - n<T>(7345,18)*z[9] - n<T>(8422,9)*z[6] + n<T>(375,4)*z[5];
    z[52]= - n<T>(125,1531)*z[21] + n<T>(1,1531)*z[52];
    z[52]=z[20]*z[52];
    z[53]=n<T>(4000,1531)*z[27] + n<T>(2000,1531)*z[25] + n<T>(836,13779)*z[22];
    z[53]=i*z[53];
    z[54]=n<T>(1999,4593)*z[11] - n<T>(2594,4593)*z[9] - z[50];
    z[54]=z[16]*z[54];
    z[50]= - 302*z[7] - 125*z[50] - 552*z[3];
    z[50]=z[19]*z[50];
    z[46]=z[5] - z[46];
    z[46]=z[14]*z[46];
    z[48]=z[2] - z[48];
    z[48]=z[17]*z[48];

    r +=  - n<T>(9031,9186)*z[23] + n<T>(500,4593)*z[24] - n<T>(2000,1531)*z[28]
     +  n<T>(31905,6124)*z[29] - n<T>(6249,6124)*z[32] - n<T>(11811,6124)*z[35] + 
      n<T>(1875,6124)*z[36] + n<T>(375,1531)*z[37] + n<T>(1655,9186)*z[39] + n<T>(1,1531)*z[41]
     + z[42]
     + n<T>(1000,1531)*z[43]
     + n<T>(125,1531)*z[44]
     + n<T>(1,4593)*z[45]
     + n<T>(625,1531)*z[46] - n<T>(781,3062)*z[47]
     + n<T>(875,1531)*
      z[48] + n<T>(344,4593)*z[49] + n<T>(3,1531)*z[50] + n<T>(500,1531)*z[51] + 
      z[52] + z[53] + z[54];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf771(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf771(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
