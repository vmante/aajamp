#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf142(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[68];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[9];
    z[8]=d[15];
    z[9]=d[2];
    z[10]=d[5];
    z[11]=d[4];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[3];
    z[30]=e[6];
    z[31]=e[13];
    z[32]=e[14];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[23];
    z[39]=f[31];
    z[40]=f[36];
    z[41]=f[39];
    z[42]=f[51];
    z[43]=f[55];
    z[44]=f[58];
    z[45]=z[1]*i;
    z[46]=2*z[45];
    z[47]=z[4] - z[46] - z[5];
    z[48]=n<T>(5,2)*z[6];
    z[49]=n<T>(5,2)*z[3];
    z[47]=z[49] - z[48] + n<T>(2,3)*z[47];
    z[47]=z[4]*z[47];
    z[50]=n<T>(2,3)*z[5];
    z[51]=z[46] - z[5];
    z[50]=z[50]*z[51];
    z[52]=7*z[45];
    z[53]= - z[52] + n<T>(5,2)*z[5];
    z[48]= - n<T>(11,4)*z[3] + z[48] + z[53];
    z[48]=z[3]*z[48];
    z[54]=n<T>(1,3)*z[4];
    z[55]=4*z[45];
    z[56]=z[55] + z[5];
    z[56]= - n<T>(2,3)*z[2] + z[54] + z[3] + n<T>(1,3)*z[56] - z[6];
    z[56]=z[2]*z[56];
    z[53]=n<T>(1,4)*z[6] - z[53];
    z[53]=z[6]*z[53];
    z[47]=z[56] + z[47] + z[48] - z[50] + z[53];
    z[47]=z[2]*z[47];
    z[48]=n<T>(1,2)*z[10];
    z[53]=4*z[5];
    z[56]=z[48] - n<T>(7,2)*z[11] + n<T>(19,4)*z[6] + n<T>(7,4)*z[45] + z[53];
    z[56]=z[10]*z[56];
    z[57]=n<T>(1,2)*z[6];
    z[58]=z[57] + z[45];
    z[58]=z[58]*z[6];
    z[59]=z[51]*z[53];
    z[60]=n<T>(1,2)*z[11];
    z[61]=z[45] - z[5];
    z[62]= - z[60] - z[61];
    z[62]=z[11]*z[62];
    z[56]=z[56] + 7*z[62] + z[59] + n<T>(19,2)*z[58];
    z[56]=z[10]*z[56];
    z[59]=19*z[45];
    z[62]=z[59] + n<T>(61,2)*z[6];
    z[62]=z[6]*z[62];
    z[63]=z[45] + z[6];
    z[64]= - 19*z[63] - n<T>(23,2)*z[10];
    z[64]=z[10]*z[64];
    z[65]= - z[6] + z[10];
    z[65]=z[9]*z[65];
    z[62]=n<T>(19,2)*z[65] + n<T>(17,6)*z[20] + z[62] + z[64];
    z[62]=z[9]*z[62];
    z[64]=3*z[3];
    z[65]= - z[45]*z[64];
    z[66]= - z[4]*z[59];
    z[65]= - 19*z[20] + z[65] + z[66];
    z[65]=z[8]*z[65];
    z[59]=z[59] - z[64] - 19*z[4];
    z[59]=z[15]*z[59];
    z[64]=53*z[3] + 23*z[11];
    z[64]=z[29]*z[64];
    z[59]=z[62] + z[65] + z[59] + z[64];
    z[62]= - n<T>(1,2)*z[4] + z[63];
    z[62]=z[4]*z[62];
    z[58]=z[58] - z[62];
    z[62]=2*z[5];
    z[63]= - z[51]*z[62];
    z[64]=7*z[5];
    z[65]=n<T>(7,3)*z[11] + n<T>(205,8)*z[4] - 11*z[3] - n<T>(53,8)*z[6] - n<T>(149,8)*
    z[45] - z[64];
    z[65]=z[11]*z[65];
    z[66]=npow(z[3],2);
    z[58]=10*z[30] + z[65] - n<T>(15,2)*z[66] + z[63] - n<T>(33,4)*z[58];
    z[58]=z[11]*z[58];
    z[63]= - z[45] + n<T>(1,2)*z[5];
    z[64]= - z[63]*z[64];
    z[65]=n<T>(41,2)*z[6] - z[45] + z[62];
    z[65]=z[6]*z[65];
    z[66]=n<T>(1,2)*z[3];
    z[67]= - 13*z[3] + 11*z[45] + n<T>(59,2)*z[6];
    z[67]=z[67]*z[66];
    z[64]=z[67] + z[64] + z[65];
    z[64]=z[3]*z[64];
    z[65]=5*z[5];
    z[49]= - z[49] - 7*z[6] - z[52] - z[65];
    z[49]=z[49]*z[66];
    z[52]= - n<T>(233,4)*z[6] + n<T>(19,2)*z[45] + z[65];
    z[52]=z[52]*z[57];
    z[54]= - z[54] + n<T>(13,2)*z[3] + n<T>(1,3)*z[5] - n<T>(19,8)*z[6];
    z[54]=z[4]*z[54];
    z[49]=z[54] + z[49] + z[50] + z[52];
    z[49]=z[4]*z[49];
    z[50]= - z[55] + 3*z[5];
    z[50]=z[50]*z[62];
    z[52]=2*z[12];
    z[54]= - z[53] + n<T>(5,3)*z[12];
    z[54]=z[54]*z[52];
    z[48]=z[48] - z[3];
    z[48]=z[48]*z[10];
    z[55]= - z[3] + z[60];
    z[55]=z[11]*z[55];
    z[50]=z[54] - z[48] + z[50] + z[55];
    z[50]=z[12]*z[50];
    z[54]= - z[5] + z[52];
    z[54]=z[45]*z[54];
    z[54]= - z[20] + z[54];
    z[54]=z[13]*z[54];
    z[52]=z[52] + z[61];
    z[52]=z[18]*z[52];
    z[52]=z[54] + z[52];
    z[54]= - z[5]*z[63];
    z[55]= - z[3]*z[61];
    z[48]=n<T>(1,2)*z[20] + z[48] + z[54] + z[55];
    z[48]=z[7]*z[48];
    z[46]= - z[4] + z[46] - z[2];
    z[46]=z[14]*z[46];
    z[51]= - z[2] + z[51];
    z[51]=z[17]*z[51];
    z[46]= - z[25] - z[46] - z[51] + z[35] + z[34];
    z[51]= - 5*z[2] - n<T>(79,2)*z[3] - 27*z[6] + z[45] + z[53];
    z[51]=z[19]*z[51];
    z[53]= - z[22] + z[41] - z[39];
    z[54]=z[43] + z[42];
    z[55]= - n<T>(8,3)*z[26] - n<T>(4,3)*z[24] - n<T>(11,8)*z[21];
    z[55]=i*z[55];
    z[45]= - n<T>(23,3)*z[6] + n<T>(101,8)*z[45] + z[5];
    z[45]=z[45]*npow(z[6],2);
    z[57]= - n<T>(35,12)*z[4] + n<T>(49,2)*z[3] - n<T>(1,6)*z[5] + 91*z[6];
    z[57]=z[10] - n<T>(73,4)*z[11] + n<T>(1,2)*z[57] + n<T>(1,3)*z[2];
    z[57]=n<T>(1,3)*z[57] + n<T>(1,2)*z[12];
    z[57]=z[20]*z[57];
    z[60]= - z[12] - z[61];
    z[60]=z[16]*z[60];
    z[61]=z[10] + z[7];
    z[61]=z[31]*z[61];
    z[62]= - z[6] - z[7];
    z[62]=z[32]*z[62];

    r +=  - n<T>(1,9)*z[23] + n<T>(4,3)*z[27] - n<T>(193,6)*z[28] - n<T>(249,8)*z[33]
       - n<T>(205,8)*z[36] - n<T>(53,8)*z[37] + 2*z[38] + n<T>(3,2)*z[40] + n<T>(19,4)*
      z[44] + z[45] - n<T>(2,3)*z[46] + z[47] + z[48] + z[49] + z[50] + 
      z[51] + 4*z[52] - n<T>(7,2)*z[53] - n<T>(23,4)*z[54] + z[55] + z[56] + 
      z[57] + z[58] + n<T>(1,2)*z[59] + z[60] + z[61] + n<T>(21,2)*z[62] + 
      z[64];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf142(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf142(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
