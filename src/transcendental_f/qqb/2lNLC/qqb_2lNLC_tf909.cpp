#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf909(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[116];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[69];
    z[3]=f[70];
    z[4]=f[77];
    z[5]=f[79];
    z[6]=f[81];
    z[7]=c[11];
    z[8]=d[0];
    z[9]=d[1];
    z[10]=d[2];
    z[11]=d[4];
    z[12]=d[5];
    z[13]=d[6];
    z[14]=d[7];
    z[15]=d[8];
    z[16]=d[9];
    z[17]=c[12];
    z[18]=c[13];
    z[19]=c[20];
    z[20]=c[23];
    z[21]=c[32];
    z[22]=c[33];
    z[23]=c[35];
    z[24]=c[36];
    z[25]=c[37];
    z[26]=c[38];
    z[27]=c[39];
    z[28]=c[40];
    z[29]=c[43];
    z[30]=c[44];
    z[31]=c[47];
    z[32]=c[48];
    z[33]=c[49];
    z[34]=c[50];
    z[35]=c[55];
    z[36]=c[56];
    z[37]=c[57];
    z[38]=c[59];
    z[39]=c[81];
    z[40]=c[84];
    z[41]=f[22];
    z[42]=f[24];
    z[43]=f[44];
    z[44]=f[80];
    z[45]=f[42];
    z[46]=f[61];
    z[47]=f[62];
    z[48]=d[3];
    z[49]=g[24];
    z[50]=g[27];
    z[51]=g[38];
    z[52]=g[40];
    z[53]=g[48];
    z[54]=g[50];
    z[55]=g[79];
    z[56]=g[82];
    z[57]=g[90];
    z[58]=g[92];
    z[59]=g[99];
    z[60]=g[101];
    z[61]=g[110];
    z[62]=g[112];
    z[63]=g[113];
    z[64]=g[128];
    z[65]=g[138];
    z[66]=g[140];
    z[67]=g[145];
    z[68]=g[147];
    z[69]=g[170];
    z[70]=g[173];
    z[71]=g[181];
    z[72]=g[183];
    z[73]=g[189];
    z[74]=g[191];
    z[75]=g[198];
    z[76]=g[200];
    z[77]=g[201];
    z[78]=g[212];
    z[79]=g[214];
    z[80]=g[218];
    z[81]=g[220];
    z[82]=g[243];
    z[83]=g[245];
    z[84]=g[251];
    z[85]=g[254];
    z[86]=g[270];
    z[87]=g[272];
    z[88]=g[276];
    z[89]=g[278];
    z[90]=g[281];
    z[91]=g[292];
    z[92]=g[294];
    z[93]= - z[14] + z[12] + n<T>(13,2)*z[9];
    z[94]=n<T>(1,2)*z[8];
    z[95]=n<T>(1,6)*z[15];
    z[96]=n<T>(1,3)*z[16];
    z[93]= - z[96] - z[95] - n<T>(13,6)*z[10] + z[94] + n<T>(1,3)*z[93];
    z[93]=z[2]*z[93];
    z[97]=z[14] - z[8];
    z[98]=n<T>(1,6)*z[13];
    z[99]= - 5*z[12] + z[9];
    z[95]=z[16] - z[95] - z[98] + n<T>(1,6)*z[99] + z[97];
    z[95]=z[41]*z[95];
    z[99]=z[8]*i;
    z[100]=z[10]*i;
    z[101]=z[99] - z[100];
    z[102]=z[12]*i;
    z[103]=z[9]*i;
    z[104]= - 7*z[102] + 19*z[103];
    z[105]=n<T>(1,6)*z[104] - z[101];
    z[106]=z[13]*i;
    z[107]=z[15]*i;
    z[108]= - n<T>(1,2)*z[107] + n<T>(1,5)*z[105] - n<T>(1,6)*z[106];
    z[109]=z[16]*i;
    z[110]=z[11]*i;
    z[108]= - n<T>(1,15)*z[110] + n<T>(1,2)*z[108] + n<T>(1,5)*z[109];
    z[108]=z[20]*z[108];
    z[111]=z[96] + n<T>(1,3)*z[11];
    z[112]=z[10] - z[8];
    z[112]= - n<T>(7,6)*z[112];
    z[113]=n<T>(7,6)*z[13] - n<T>(3,2)*z[12] - n<T>(1,3)*z[9] - z[112] + z[111];
    z[113]=z[42]*z[113];
    z[114]=z[9] + z[12];
    z[115]= - z[11] - z[16] + z[114];
    z[115]=z[46]*z[115];
    z[93]=z[54] + z[115] + z[76] + z[73] - z[63] + z[93] + z[108] + 
    z[95] + z[113];
    z[95]= - z[104] + 6*z[101];
    z[95]=n<T>(4,5)*z[110] - n<T>(12,5)*z[109] + 3*z[107] + n<T>(1,5)*z[95] + z[106]
   ;
    z[95]=z[18]*z[95];
    z[101]=n<T>(2,3)*z[110] - 2*z[109] + n<T>(5,2)*z[107] + n<T>(5,6)*z[106] - 
    z[105];
    z[101]=z[19]*z[101];
    z[102]= - 137*z[102] + 269*z[103];
    z[99]=n<T>(1,2)*z[102] + 67*z[99];
    z[99]=n<T>(1,27)*z[99] - z[100];
    z[99]=n<T>(11,6)*z[107] + n<T>(1,5)*z[99] - n<T>(47,54)*z[106];
    z[99]= - n<T>(17,45)*z[110] + n<T>(1,2)*z[99] + n<T>(53,135)*z[109];
    z[99]=z[7]*z[99];
    z[100]=z[15] + z[13];
    z[102]=z[10] + z[97] - z[100];
    z[102]=z[6]*z[102];
    z[99]=z[90] + z[89] + z[86] - z[52] - z[49] + z[99] + z[102];
    z[102]=n<T>(1,3)*z[12];
    z[103]=n<T>(1,3)*z[13];
    z[104]= - n<T>(13,6)*z[15] - z[103] - z[102] + n<T>(3,2)*z[9] - z[112];
    z[104]=n<T>(1,2)*z[104] + z[111];
    z[104]=z[43]*z[104];
    z[105]=z[15] + z[12] - z[9];
    z[106]=n<T>(1,2)*z[10];
    z[98]=n<T>(1,2)*z[14] - n<T>(5,6)*z[16] + z[98] - z[106] + n<T>(2,3)*z[105];
    z[98]=z[47]*z[98];
    z[105]=z[11] - 5*z[15] - n<T>(1,2)*z[13] + z[10] + z[8] + 4*z[12] - n<T>(5,2)
   *z[9];
    z[105]=z[5]*z[105];
    z[107]= - z[44] + n<T>(1,2)*z[6] - 2*z[4] - n<T>(7,2)*z[5];
    z[107]=z[48]*z[107];
    z[105]= - z[81] - z[78] + z[67] + z[65] + z[92] - z[105] - z[107] + 
    z[91] - z[83];
    z[94]= - z[106] + z[94] + n<T>(1,2)*z[12] - z[9];
    z[106]= - z[15] + z[11] - z[16];
    z[94]=n<T>(1,3)*z[94] - n<T>(1,2)*z[106];
    z[94]=z[4]*z[94];
    z[106]=n<T>(1,3)*z[4];
    z[107]=z[7]*i;
    z[107]= - n<T>(4,27)*z[107] + z[106] + n<T>(3,2)*z[5];
    z[107]=z[14]*z[107];
    z[102]= - z[103] + n<T>(1,3)*z[10] + z[102] + z[8];
    z[96]= - z[96] + n<T>(1,4)*z[102] + n<T>(1,3)*z[15];
    z[96]=z[44]*z[96];
    z[100]= - z[100] + z[114];
    z[100]=z[45]*z[100];
    z[102]=z[51] + z[50];
    z[103]=z[58] + z[55];
    z[108]=z[71] + z[70];
    z[109]=z[72] + z[69];
    z[110]=z[75] - z[62];
    z[111]=z[82] - z[32];
    z[112]=z[85] + z[84];
    z[113]=z[88] + z[79];
    z[106]= - n<T>(1,3)*z[6] + z[3] + z[2] + z[106] - z[5];
    z[106]=z[1]*z[106];
    z[106]=z[106] + n<T>(2,3)*z[40] - n<T>(65,108)*z[39] + n<T>(1,45)*z[38] + n<T>(1,4)*
    z[37] - z[36] - 2*z[34] - n<T>(12,5)*z[33] + n<T>(17,405)*z[31] + n<T>(5,3)*
    z[30] - n<T>(109,972)*z[27] - n<T>(121,27)*z[26] + 4*z[25] + n<T>(5,54)*z[22];
    z[106]=i*z[106];
    z[97]=z[97] - z[9];
    z[97]=n<T>(1,6)*z[11] - z[10] - z[13] + z[12] - n<T>(5,6)*z[97];
    z[97]=z[3]*z[97];
    z[114]=z[12] + 5*z[9];
    z[114]=4*z[14] + n<T>(10,9)*z[11] - n<T>(19,3)*z[15] + n<T>(11,9)*z[13] + n<T>(4,3)*
    z[10] + n<T>(1,9)*z[114] - 2*z[8];
    z[114]=z[17]*z[114];

    r +=  - n<T>(131,540)*z[21] + n<T>(12,5)*z[23] + 4*z[24] - n<T>(10,3)*z[28] - 
      z[29] - n<T>(1,20)*z[35] - n<T>(5,18)*z[53] + n<T>(37,24)*z[56] + n<T>(15,8)*
      z[57] - n<T>(7,18)*z[59] + n<T>(31,36)*z[60] - n<T>(1,8)*z[61] + z[64] + 
      z[66] + n<T>(4,9)*z[68] - n<T>(25,18)*z[74] + z[77] - z[80] + n<T>(7,3)*z[87]
       + n<T>(1,2)*z[93] + z[94] + 2*z[95] + z[96] + z[97] + z[98] + n<T>(1,6)*
      z[99] + n<T>(7,12)*z[100] + z[101] + n<T>(7,6)*z[102] + n<T>(19,24)*z[103]
     +  z[104] - n<T>(1,3)*z[105] + z[106] + z[107] - n<T>(11,4)*z[108] - n<T>(5,12)*
      z[109] + n<T>(1,4)*z[110] - 3*z[111] + n<T>(2,3)*z[112] - n<T>(4,3)*z[113]
     +  z[114];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf909(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf909(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
