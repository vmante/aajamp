#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf293(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[38];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[8];
    z[5]=d[3];
    z[6]=d[15];
    z[7]=d[2];
    z[8]=d[5];
    z[9]=d[4];
    z[10]=e[2];
    z[11]=c[3];
    z[12]=d[7];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[31];
    z[16]=e[10];
    z[17]=e[3];
    z[18]=e[14];
    z[19]=d[9];
    z[20]=f[4];
    z[21]=f[12];
    z[22]=f[16];
    z[23]=f[51];
    z[24]=f[55];
    z[25]=f[58];
    z[26]= - z[3] + n<T>(1,4)*z[4];
    z[27]=7*z[5];
    z[28]=z[26]*z[27];
    z[29]=npow(z[4],2);
    z[30]=n<T>(11,4)*z[29];
    z[31]=7*z[4] - n<T>(1,2)*z[3];
    z[31]=z[3]*z[31];
    z[28]=z[28] - z[30] + z[31];
    z[28]=z[5]*z[28];
    z[31]= - n<T>(1,2)*z[4] + z[3];
    z[31]=z[31]*z[27];
    z[32]=4*z[4];
    z[33]= - z[32] + z[3];
    z[33]=z[3]*z[33];
    z[30]=z[31] + z[30] + z[33];
    z[31]=z[1]*i;
    z[30]=z[30]*z[31];
    z[33]=n<T>(5,2)*z[4] + 4*z[3];
    z[33]=z[3]*z[33];
    z[33]= - 4*z[29] + z[33];
    z[33]=z[3]*z[33];
    z[27]=z[27] + z[3];
    z[34]=z[27]*z[31];
    z[34]=z[34] + 7*z[11];
    z[34]=z[6]*z[34];
    z[27]= - 7*z[31] + z[27];
    z[27]=z[10]*z[27];
    z[35]=npow(z[4],3);
    z[36]=n<T>(1,2)*z[7];
    z[37]= - z[36] - n<T>(1,2)*z[12] + z[8] - n<T>(17,2)*z[9] + n<T>(13,12)*z[5]
     + n<T>(8,3)*z[4] - n<T>(5,2)*z[3];
    z[37]=z[11]*z[37];
    z[27]=z[37] + z[30] + z[28] + 2*z[35] + z[33] + z[34] + z[27];
    z[28]=z[31] + z[4];
    z[30]=n<T>(1,2)*z[8];
    z[33]=z[30] + z[28];
    z[33]=z[8]*z[33];
    z[34]=z[4] - z[8];
    z[34]=z[34]*z[36];
    z[35]=z[31]*z[4];
    z[33]=z[34] + z[33] - n<T>(3,2)*z[29] - z[35];
    z[33]=z[7]*z[33];
    z[34]=z[4] - z[5];
    z[34]=z[34]*z[31];
    z[36]=n<T>(1,2)*z[29];
    z[37]= - z[4] + n<T>(1,2)*z[5];
    z[37]=z[5]*z[37];
    z[34]=z[34] + z[36] + z[37];
    z[37]=z[31] - z[5];
    z[26]=z[26] - n<T>(3,4)*z[37];
    z[26]=z[9]*z[26];
    z[26]=n<T>(7,2)*z[34] + 3*z[26];
    z[26]=z[9]*z[26];
    z[31]= - 2*z[31] + z[5];
    z[34]=z[3] - z[4];
    z[31]=z[34]*z[31];
    z[37]=z[4] - n<T>(3,2)*z[3];
    z[37]=z[3]*z[37];
    z[31]=z[36] + z[37] + z[31];
    z[31]=z[2]*z[31];
    z[28]= - z[28]*z[30];
    z[28]=z[28] - z[36] - z[35];
    z[28]=z[8]*z[28];
    z[30]=z[2] - z[5];
    z[30]=z[34]*z[30];
    z[34]= - 2*z[4] + z[3];
    z[34]=z[3]*z[34];
    z[29]=z[29] + z[34] + z[30];
    z[29]=z[12]*z[29];
    z[30]=z[12] - z[2];
    z[32]=z[32] - 5*z[3];
    z[30]=n<T>(1,3)*z[32] + 2*z[30];
    z[30]=z[16]*z[30];
    z[32]= - z[23] + z[25] - z[24];
    z[34]=z[19] + z[4];
    z[34]=z[18]*z[34];
    z[35]=z[3] + z[9];
    z[35]=z[17]*z[35];
    z[36]=z[13]*i;

    r +=  - n<T>(4,3)*z[14] + n<T>(23,2)*z[15] - n<T>(11,12)*z[20] - n<T>(9,4)*z[21]
     +  n<T>(3,4)*z[22] + z[26] + n<T>(1,3)*z[27] + z[28] + z[29] + z[30] + z[31]
       - n<T>(1,2)*z[32] + z[33] + z[34] + 3*z[35] + n<T>(7,36)*z[36];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf293(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf293(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
