#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf584(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[36];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[6];
    z[4]=d[17];
    z[5]=d[8];
    z[6]=d[9];
    z[7]=d[11];
    z[8]=c[3];
    z[9]=d[1];
    z[10]=e[3];
    z[11]=e[10];
    z[12]=d[2];
    z[13]=d[5];
    z[14]=d[3];
    z[15]=d[7];
    z[16]=e[6];
    z[17]=e[7];
    z[18]=e[9];
    z[19]=e[12];
    z[20]=e[13];
    z[21]=e[14];
    z[22]=f[4];
    z[23]=f[12];
    z[24]=f[31];
    z[25]=f[36];
    z[26]=f[51];
    z[27]=f[55];
    z[28]=z[1]*i;
    z[29]=2*z[28];
    z[30]=3*z[9];
    z[31]= - z[30] + 5*z[14];
    z[32]=z[12] - z[31];
    z[32]=n<T>(1,2)*z[32] + z[29];
    z[32]=z[5]*z[32];
    z[33]=npow(z[9],2);
    z[34]=n<T>(1,3)*z[8];
    z[35]= - z[11] + z[34];
    z[32]=z[32] + 2*z[35] + z[33];
    z[32]=z[5]*z[32];
    z[31]= - z[15] + z[31];
    z[31]=n<T>(1,2)*z[31] - z[29];
    z[31]=z[2]*z[31];
    z[35]=z[10] - z[34];
    z[31]=z[31] + 2*z[35] - z[33];
    z[31]=z[2]*z[31];
    z[33]= - z[4]*z[29];
    z[35]= - n<T>(2,3)*z[3] + z[15] + z[28];
    z[35]=z[3]*z[35];
    z[33]=z[35] + z[33] - z[34] + z[16] - 2*z[18] + z[17];
    z[33]=z[3]*z[33];
    z[34]=npow(z[12],2);
    z[29]=z[7]*z[29];
    z[28]= - n<T>(1,3)*z[6] + z[12] - z[28];
    z[28]=z[6]*z[28];
    z[28]=z[28] + z[29] + n<T>(2,3)*z[8] - z[34] - 2*z[19] - z[21] - z[20];
    z[28]=z[6]*z[28];
    z[29]=z[22] + z[23];
    z[34]=z[12] - z[15];
    z[34]=z[34]*npow(z[13],2);
    z[29]= - z[24] + z[25] - z[27] + z[26] + z[34] - 5*z[29];
    z[34]= - z[11] + z[10];
    z[30]=z[34]*z[30];

    r += z[28] + n<T>(1,2)*z[29] + z[30] + z[31] + z[32] + z[33];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf584(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf584(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
