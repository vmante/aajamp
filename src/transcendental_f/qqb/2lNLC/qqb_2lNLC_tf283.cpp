#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf283(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[74];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[16];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[4];
    z[18]=e[7];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=c[3];
    z[23]=d[9];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[3];
    z[33]=e[6];
    z[34]=e[14];
    z[35]=f[0];
    z[36]=f[1];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[11];
    z[41]=f[12];
    z[42]=f[16];
    z[43]=f[18];
    z[44]=f[20];
    z[45]=f[23];
    z[46]=f[31];
    z[47]=f[36];
    z[48]=f[39];
    z[49]=f[51];
    z[50]=f[55];
    z[51]=f[58];
    z[52]=n<T>(1,165)*z[5];
    z[53]=z[1]*i;
    z[54]=101*z[53] + 86*z[7];
    z[54]=4*z[54] - 281*z[5];
    z[54]=z[54]*z[52];
    z[55]=n<T>(1,11)*z[2];
    z[56]=z[6] - n<T>(54,11)*z[5] + n<T>(53,11)*z[53] + z[7];
    z[56]=n<T>(1,5)*z[56] - z[55];
    z[56]=z[2]*z[56];
    z[57]= - n<T>(64,15)*z[7] - 2 - n<T>(317,165)*z[53];
    z[57]=z[7]*z[57];
    z[58]=2*z[53];
    z[59]=z[6] - z[58] - z[7];
    z[59]= - n<T>(1,5)*z[5] + n<T>(4,11)*z[59];
    z[59]=z[6]*z[59];
    z[60]= - n<T>(31,33)*z[3] + n<T>(1,5)*z[2] + n<T>(314,165)*z[5] - n<T>(8,33)*z[7]
     + 
   1 + n<T>(161,165)*z[53];
    z[60]=z[3]*z[60];
    z[54]=z[60] + z[56] + z[59] + z[57] + z[54];
    z[54]=z[3]*z[54];
    z[56]=n<T>(1,5)*z[6];
    z[57]=26*z[53];
    z[59]=z[57] - 21*z[6];
    z[59]=z[59]*z[56];
    z[60]=26*z[6] - n<T>(31,3)*z[12];
    z[60]=z[12]*z[60];
    z[59]=n<T>(1,5)*z[60] + z[59] - n<T>(2,3)*z[22];
    z[59]=z[12]*z[59];
    z[60]=2*z[4] + 5*z[9];
    z[61]=z[53]*z[60];
    z[61]=2*z[22] + z[61];
    z[61]=z[11]*z[61];
    z[60]=z[58] - z[60];
    z[60]=z[17]*z[60];
    z[59]=z[59] + z[61] + z[60] - z[45];
    z[57]=z[57] - z[5];
    z[57]=z[5]*z[57];
    z[60]= - 10*z[53] + 7*z[9];
    z[60]=z[9]*z[60];
    z[57]=z[57] + z[60];
    z[60]= - n<T>(7,110)*z[7] + 1 - n<T>(39,55)*z[53];
    z[60]=z[7]*z[60];
    z[61]= - 13*z[53] + z[5];
    z[61]=z[2]*z[61];
    z[62]=12*z[2] - 13*z[5] + 8*z[53] + n<T>(39,10)*z[7];
    z[62]=n<T>(1,11)*z[62] - n<T>(1,3)*z[4];
    z[62]=z[4]*z[62];
    z[57]=z[62] + n<T>(2,11)*z[61] + z[60] + n<T>(1,11)*z[57];
    z[57]=z[4]*z[57];
    z[60]=z[53] + n<T>(1,2)*z[7];
    z[60]=z[7]*z[60];
    z[61]=z[53] + z[7];
    z[62]=n<T>(1,2)*z[4] - z[61];
    z[62]=z[4]*z[62];
    z[60]=z[60] + z[62];
    z[62]=z[58] - z[6];
    z[63]=z[62]*z[6];
    z[64]=z[53] - z[6];
    z[65]=2*z[64] + z[9];
    z[65]=z[9]*z[65];
    z[66]=25*z[53] + n<T>(21,5)*z[7];
    z[66]= - n<T>(4,5)*z[10] - n<T>(21,10)*z[4] + n<T>(56,5)*z[9] + n<T>(1,2)*z[66]
     - n<T>(52,5)*z[6];
    z[66]=z[10]*z[66];
    z[60]=n<T>(1,11)*z[66] + n<T>(56,55)*z[65] - n<T>(12,11)*z[63] + n<T>(1,5)*z[60];
    z[60]=z[10]*z[60];
    z[65]=13*z[12];
    z[66]= - z[53]*z[65];
    z[67]=z[6]*z[53];
    z[67]=z[67] + z[22];
    z[66]=8*z[67] + z[66];
    z[66]=z[13]*z[66];
    z[65]= - 8*z[64] - z[65];
    z[65]=z[20]*z[65];
    z[65]=z[66] + z[65];
    z[66]=508*z[3];
    z[67]=353*z[5] - z[66];
    z[67]=z[53]*z[67];
    z[67]=353*z[22] + z[67];
    z[67]=z[8]*z[67];
    z[68]= - z[53] + z[5];
    z[66]=353*z[68] - z[66];
    z[66]=z[16]*z[66];
    z[66]=z[67] + z[66];
    z[67]=4*z[53] + n<T>(7,2)*z[7];
    z[67]=z[7]*z[67];
    z[68]=n<T>(19,11)*z[5] - n<T>(83,11)*z[53] - z[7];
    z[68]=z[5]*z[68];
    z[69]= - n<T>(56,11)*z[6] - z[7] + n<T>(112,11)*z[53];
    z[70]=n<T>(56,11)*z[5] + z[69];
    z[70]=z[6]*z[70];
    z[67]=z[70] + n<T>(3,11)*z[67] + z[68];
    z[68]=n<T>(37,10)*z[2] - n<T>(28,5)*z[6] - n<T>(21,10)*z[5] - n<T>(17,5)*z[53]
     + 
    z[7];
    z[55]=z[68]*z[55];
    z[55]=n<T>(1,5)*z[67] + z[55];
    z[55]=z[2]*z[55];
    z[67]=n<T>(1,2)*z[5];
    z[61]=z[67] - z[61];
    z[61]=z[5]*z[61];
    z[68]=161*z[53] + n<T>(181,2)*z[7];
    z[68]=z[7]*z[68];
    z[61]=38*z[63] + z[68] + 161*z[61];
    z[63]=npow(z[3],2);
    z[68]= - 313*z[5] + 49*z[53] + 129*z[7];
    z[68]=n<T>(1,22)*z[68] + 12*z[6];
    z[68]= - n<T>(232,165)*z[9] - n<T>(28,55)*z[3] + n<T>(1,5)*z[68] - n<T>(7,11)*z[2];
    z[68]=z[9]*z[68];
    z[61]=z[68] + n<T>(1,55)*z[61] + 2*z[63];
    z[61]=z[9]*z[61];
    z[63]=z[2] + 18*z[53] - 19*z[5];
    z[63]=z[15]*z[63];
    z[68]= - 147*z[3] - 17*z[9];
    z[68]=z[32]*z[68];
    z[63]=z[63] + z[68];
    z[68]= - 1 - n<T>(413,165)*z[7];
    z[68]=z[7]*z[53]*z[68];
    z[70]= - 79*z[53] + 392*z[7];
    z[70]=z[7]*z[70];
    z[71]=n<T>(79,2)*z[7] + 14*z[5];
    z[71]=z[5]*z[71];
    z[70]=z[70] + z[71];
    z[52]=z[70]*z[52];
    z[69]= - n<T>(28,11)*z[5] - z[69];
    z[56]=z[56]*z[5]*z[69];
    z[62]=z[2] - z[62];
    z[62]=z[19]*z[62];
    z[62]=z[40] + z[62] + z[48];
    z[69]= - z[49] + z[51] - z[50];
    z[70]=z[39] - z[28];
    z[67]= - n<T>(626,55)*z[7] + z[67];
    z[67]= - n<T>(4,165)*z[23] - n<T>(299,330)*z[10] + n<T>(331,330)*z[4] - n<T>(9,22)*
    z[9] - n<T>(61,110)*z[3] - n<T>(157,110)*z[2] + n<T>(1,9)*z[67] - n<T>(21,110)*z[6];
    z[67]=z[22]*z[67];
    z[71]= - n<T>(106,55)*z[29] + n<T>(17,55)*z[27] - n<T>(1009,1980)*z[24];
    z[71]=i*z[71];
    z[58]=z[58] + n<T>(641,3)*z[7];
    z[58]=n<T>(1,5)*z[58] + 4*z[6];
    z[58]=n<T>(476,165)*z[3] + n<T>(1,11)*z[58] - n<T>(2,5)*z[2];
    z[58]=z[21]*z[58];
    z[53]=20*z[53] - 9*z[2];
    z[53]=n<T>(1,11)*z[53] - z[4];
    z[53]=z[14]*z[53];
    z[64]= - z[10] + z[64];
    z[64]=z[18]*z[64];
    z[72]= - n<T>(32,5)*z[9] - 3*z[12];
    z[72]=z[33]*z[72];
    z[73]= - z[7] - z[23];
    z[73]=z[34]*z[73];

    r += n<T>(1,66)*z[25] + n<T>(457,660)*z[26] + n<T>(3,5)*z[30] + n<T>(5077,660)*
      z[31] - n<T>(18,11)*z[35] + n<T>(7,11)*z[36] - n<T>(13,11)*z[37] + n<T>(392,165)*
      z[38] + n<T>(313,110)*z[41] + n<T>(149,110)*z[42] - n<T>(14,33)*z[43] - n<T>(4,33)
      *z[44] - n<T>(48,55)*z[46] - n<T>(38,55)*z[47] + z[52] + 2*z[53] + z[54]
       + z[55] + z[56] + z[57] + z[58] + n<T>(2,11)*z[59] + z[60] + z[61]
       + n<T>(56,55)*z[62] + n<T>(1,55)*z[63] + n<T>(8,55)*z[64] + n<T>(4,55)*z[65]
     + n<T>(1,165)*z[66]
     + z[67]
     + z[68]
     + n<T>(21,110)*z[69]
     + n<T>(37,110)*z[70]
     +  z[71] + n<T>(4,11)*z[72] + n<T>(21,55)*z[73];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf283(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf283(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
