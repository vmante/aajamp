#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1130(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[30];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=e[2];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[17];
    z[12]=c[31];
    z[13]=e[10];
    z[14]=f[4];
    z[15]=f[12];
    z[16]=f[16];
    z[17]=f[19];
    z[18]=z[1]*z[6];
    z[19]=z[5]*z[1];
    z[20]=z[4]*z[1];
    z[21]= - z[20] - z[18] + n<T>(5,2)*z[19];
    z[21]=i*z[21];
    z[22]= - z[4] + n<T>(5,2)*z[5];
    z[23]=i*z[1];
    z[24]= - n<T>(1,2)*z[23] - z[22];
    z[24]=z[3]*z[24];
    z[22]= - n<T>(11,3)*z[2] + 3*z[3] - z[23] + z[22];
    z[22]=z[2]*z[22];
    z[25]=npow(z[5],2);
    z[26]=z[25] - z[13];
    z[27]=n<T>(1,3)*z[8];
    z[28]=npow(z[4],2);
    z[21]=n<T>(1,2)*z[22] + z[24] + z[21] - z[28] - z[27] - z[7] + n<T>(5,2)*
    z[26];
    z[21]=z[2]*z[21];
    z[22]= - n<T>(3,8)*z[5] + n<T>(1,3)*z[4];
    z[22]=z[4]*z[22];
    z[22]=z[22] + z[27] - n<T>(3,8)*z[25];
    z[22]=z[4]*z[22];
    z[24]= - 3*z[19] - n<T>(11,2)*z[20];
    z[26]=n<T>(1,2)*z[4];
    z[24]=z[24]*z[26];
    z[25]= - z[7] + n<T>(17,4)*z[25];
    z[28]=z[1]*z[25];
    z[29]=9*z[11] - n<T>(1,3)*z[9];
    z[24]=z[24] + n<T>(1,2)*z[29] + z[28];
    z[28]=n<T>(1,2)*i;
    z[24]=z[24]*z[28];
    z[28]=3*z[5] + n<T>(11,2)*z[4];
    z[26]=z[28]*z[26];
    z[19]=z[19] - z[20];
    z[18]=z[18] - n<T>(7,2)*z[19];
    z[18]=i*z[18];
    z[18]=z[18] + z[26] - n<T>(5,4)*z[8] - z[25];
    z[19]=z[5] - z[4];
    z[19]=n<T>(1,3)*z[3] + n<T>(7,8)*z[19] - z[23];
    z[19]=z[3]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[3]*z[18];
    z[19]=z[8]*z[6];
    z[20]= - 5*z[13] - z[27];
    z[20]=z[5]*z[20];
    z[19]=z[19] + z[20];
    z[20]=z[15] + z[16];

    r += n<T>(11,8)*z[10] - n<T>(27,16)*z[12] - n<T>(17,8)*z[14] + z[17] + z[18]
     +  n<T>(1,2)*z[19] - n<T>(3,8)*z[20] + z[21] + z[22] + z[24];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1130(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1130(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
