#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf930(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[125];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[77];
    z[3]=f[79];
    z[4]=f[81];
    z[5]=c[11];
    z[6]=d[0];
    z[7]=d[1];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[7];
    z[13]=d[8];
    z[14]=d[9];
    z[15]=c[12];
    z[16]=c[13];
    z[17]=c[20];
    z[18]=c[23];
    z[19]=c[32];
    z[20]=c[33];
    z[21]=c[35];
    z[22]=c[36];
    z[23]=c[37];
    z[24]=c[38];
    z[25]=c[39];
    z[26]=c[40];
    z[27]=c[43];
    z[28]=c[44];
    z[29]=c[47];
    z[30]=c[48];
    z[31]=c[49];
    z[32]=c[50];
    z[33]=c[55];
    z[34]=c[56];
    z[35]=c[57];
    z[36]=c[59];
    z[37]=c[81];
    z[38]=c[84];
    z[39]=f[22];
    z[40]=f[24];
    z[41]=f[42];
    z[42]=f[44];
    z[43]=f[61];
    z[44]=f[62];
    z[45]=f[69];
    z[46]=f[70];
    z[47]=f[80];
    z[48]=d[3];
    z[49]=g[24];
    z[50]=g[27];
    z[51]=g[38];
    z[52]=g[40];
    z[53]=g[48];
    z[54]=g[50];
    z[55]=g[79];
    z[56]=g[82];
    z[57]=g[90];
    z[58]=g[92];
    z[59]=g[99];
    z[60]=g[101];
    z[61]=g[110];
    z[62]=g[112];
    z[63]=g[113];
    z[64]=g[128];
    z[65]=g[131];
    z[66]=g[138];
    z[67]=g[140];
    z[68]=g[145];
    z[69]=g[170];
    z[70]=g[173];
    z[71]=g[181];
    z[72]=g[183];
    z[73]=g[187];
    z[74]=g[189];
    z[75]=g[191];
    z[76]=g[198];
    z[77]=g[200];
    z[78]=g[201];
    z[79]=g[212];
    z[80]=g[214];
    z[81]=g[218];
    z[82]=g[220];
    z[83]=g[224];
    z[84]=g[243];
    z[85]=g[245];
    z[86]=g[251];
    z[87]=g[254];
    z[88]=g[270];
    z[89]=g[272];
    z[90]=g[276];
    z[91]=g[278];
    z[92]=g[281];
    z[93]=g[285];
    z[94]=g[288];
    z[95]=g[289];
    z[96]=g[292];
    z[97]=g[294];
    z[98]=g[399];
    z[99]=g[402];
    z[100]=g[405];
    z[101]=g[407];
    z[102]= - 2*z[17] + n<T>(1,10)*z[18];
    z[102]= - n<T>(8,5)*z[16] + n<T>(1,3)*z[102];
    z[103]=47*z[102] + n<T>(499,270)*z[5];
    z[103]=z[9]*z[103];
    z[104]= - z[17] + n<T>(1,20)*z[18];
    z[105]= - n<T>(4,5)*z[16] + n<T>(1,3)*z[104];
    z[106]=257*z[105] - n<T>(1051,540)*z[5];
    z[106]=z[13]*z[106];
    z[107]= - n<T>(1,4)*z[18] + 5*z[17];
    z[107]=n<T>(1,3)*z[107] + 4*z[16];
    z[107]=13*z[107] - n<T>(163,324)*z[5];
    z[107]=z[11]*z[107];
    z[103]=z[107] + z[103] + z[106];
    z[106]=n<T>(1,3)*z[14];
    z[107]=31*z[102] + n<T>(1,810)*z[5];
    z[107]=z[107]*z[106];
    z[108]= - n<T>(46,9)*z[2] + n<T>(110,9)*z[3] - n<T>(22,3)*z[4];
    z[108]=z[1]*z[108];
    z[109]= - n<T>(2,15)*z[36] - 4*z[38] + n<T>(65,18)*z[37];
    z[109]=4*z[32] + 2*z[34] + n<T>(1,3)*z[109] - n<T>(1,2)*z[35];
    z[109]=n<T>(1,3)*z[109] + n<T>(8,5)*z[31];
    z[110]=z[5]*z[12];
    z[102]=23*z[102];
    z[111]= - z[102] - n<T>(19,2430)*z[5];
    z[111]=z[8]*z[111];
    z[104]=n<T>(12,5)*z[16] - z[104];
    z[104]=39*z[104] - n<T>(907,1620)*z[5];
    z[104]=z[7]*z[104];
    z[105]=n<T>(31,1620)*z[5] + z[105];
    z[105]=z[10]*z[105];
    z[102]=z[102] - n<T>(29,270)*z[5];
    z[102]=z[6]*z[102];
    z[102]=z[102] + z[105] + z[104] + z[111] + z[107] + n<T>(208,243)*z[110]
    + n<T>(281,81)*z[20] - n<T>(184,3)*z[23] + n<T>(1606,81)*z[24] + n<T>(2507,1458)*
    z[25] - n<T>(230,9)*z[28] + 23*z[109] - n<T>(554,405)*z[29] + z[108] + n<T>(1,3)
   *z[103];
    z[102]=i*z[102];
    z[103]=n<T>(1,2)*z[47];
    z[104]=z[103] + n<T>(37,18)*z[41];
    z[105]=n<T>(1,9)*z[43];
    z[107]=n<T>(55,18)*z[40];
    z[108]=n<T>(11,3)*z[4];
    z[109]=n<T>(23,9)*z[44];
    z[110]=n<T>(34,3)*z[3] - n<T>(50,27)*z[15] + z[109] - z[108] - n<T>(13,3)*z[42]
    + n<T>(13,6)*z[39] - z[107] + z[105] + n<T>(62,9)*z[46] + z[104];
    z[110]=z[11]*z[110];
    z[111]=z[3] + z[42];
    z[112]=n<T>(8,9)*z[44];
    z[113]=5*z[40];
    z[114]=n<T>(31,3)*z[43] - z[113];
    z[111]=z[2] - n<T>(200,27)*z[15] - z[112] - n<T>(8,3)*z[4] + n<T>(1,3)*z[114]
     - 
    z[45] - n<T>(7,9)*z[111];
    z[111]=z[9]*z[111];
    z[114]=z[44] - z[47];
    z[115]=n<T>(23,3)*z[39];
    z[116]= - z[115] + n<T>(1,3)*z[41];
    z[114]= - 8*z[2] + n<T>(164,9)*z[15] - n<T>(7,3)*z[42] + n<T>(8,3)*z[45] - 
    z[113] + n<T>(32,3)*z[43] + z[116] - 5*z[114];
    z[106]=z[114]*z[106];
    z[114]=n<T>(110,9)*z[46];
    z[103]= - n<T>(17,9)*z[2] + n<T>(23,9)*z[3] - n<T>(40,3)*z[15] + n<T>(23,3)*z[44]
     + 
    z[4] - n<T>(55,18)*z[42] + n<T>(13,6)*z[45] - z[107] + z[114] - z[103];
    z[103]=z[8]*z[103];
    z[107]=n<T>(13,2)*z[39];
    z[117]=z[46] - n<T>(2,3)*z[43];
    z[113]= - n<T>(49,2)*z[42] - n<T>(7,2)*z[45] - z[107] + z[113] + 16*z[117]
    - n<T>(37,6)*z[41];
    z[113]=n<T>(8,9)*z[2] + n<T>(38,9)*z[3] - n<T>(58,3)*z[15] + n<T>(1,3)*z[113]
     + 6*
    z[44];
    z[113]=z[7]*z[113];
    z[104]=n<T>(16,9)*z[2] - n<T>(92,9)*z[3] - n<T>(2,9)*z[15] - z[112] + n<T>(46,9)*
    z[42] - n<T>(8,9)*z[45] + n<T>(7,18)*z[39] + n<T>(85,18)*z[40] - n<T>(32,9)*z[43]
     - 
    z[114] - z[104];
    z[104]=z[10]*z[104];
    z[112]= - 10*z[48] - 107*z[12];
    z[112]=z[3]*z[112];
    z[114]=8*z[48] + 23*z[12];
    z[114]=z[2]*z[114];
    z[112]=z[112] + z[114] + z[73] + z[98];
    z[114]=z[42] + z[40];
    z[114]=n<T>(32,3)*z[2] - n<T>(16,3)*z[3] + n<T>(148,3)*z[15] - n<T>(46,3)*z[44]
     + 5*
    z[4] + n<T>(23,6)*z[45] - n<T>(1,3)*z[43] - n<T>(62,3)*z[46] - n<T>(7,2)*z[47]
     - 
    z[116] + n<T>(55,6)*z[114];
    z[114]=z[6]*z[114];
    z[114]=z[114] + z[69];
    z[105]=z[108] + n<T>(1,9)*z[41] + z[105] + n<T>(5,3)*z[47];
    z[105]=z[48]*z[105];
    z[108]= - n<T>(31,3)*z[45] - 16*z[46] - z[115];
    z[108]= - n<T>(208,9)*z[15] - z[109] + n<T>(1,3)*z[108] + z[4];
    z[108]=z[12]*z[108];
    z[109]= - z[47] + n<T>(7,6)*z[41];
    z[107]=n<T>(161,6)*z[42] + n<T>(13,2)*z[45] + 5*z[109] + z[107];
    z[107]= - n<T>(55,9)*z[2] + n<T>(23,3)*z[3] + n<T>(1154,27)*z[15] - n<T>(46,9)*z[44]
    + n<T>(1,3)*z[107] - z[4];
    z[107]=z[13]*z[107];
    z[109]=z[94] - z[76] - 4*z[101] - z[100];
    z[115]= - z[91] + z[49] + z[52];
    z[116]=z[96] + z[97];
    z[117]=z[81] + z[90];
    z[118]=z[78] - z[95];
    z[119]=z[70] + z[71];
    z[120]=z[65] - z[85];
    z[121]=z[64] + z[67];
    z[122]=z[55] + z[58];
    z[123]=z[53] - z[74];
    z[124]=z[50] + z[51];

    r += n<T>(10343,2430)*z[19] - n<T>(248,5)*z[21] - 72*z[22] + 60*z[26] + 18*
      z[27] - n<T>(286,9)*z[30] + n<T>(31,30)*z[33] - n<T>(85,27)*z[54] - n<T>(427,36)*
      z[56] - n<T>(265,12)*z[57] + n<T>(35,27)*z[59] - n<T>(115,18)*z[60] + n<T>(23,12)
      *z[61] + n<T>(23,6)*z[62] + n<T>(23,3)*z[63] - n<T>(10,3)*z[66] + n<T>(62,27)*
      z[68] - z[72] + n<T>(160,27)*z[75] - n<T>(4,3)*z[77] + n<T>(34,9)*z[79] + n<T>(16,9)*z[80]
     + n<T>(46,9)*z[82] - n<T>(32,27)*z[83]
     + n<T>(214,9)*z[84] - n<T>(32,9)*
      z[86] - n<T>(152,27)*z[87] - 3*z[88] - n<T>(40,3)*z[89] + z[92] - z[93]
       + z[99] + z[102] + z[103] + z[104] + z[105] + z[106] + z[107] + 
      z[108] + n<T>(2,3)*z[109] + z[110] + z[111] + n<T>(1,9)*z[112] + z[113]
       + n<T>(1,3)*z[114] + n<T>(13,3)*z[115] - 2*z[116] + n<T>(46,3)*z[117] - n<T>(8,3)
      *z[118] + n<T>(190,9)*z[119] + n<T>(92,9)*z[120] - n<T>(64,9)*z[121] + n<T>(59,36)
      *z[122] + n<T>(7,27)*z[123] - n<T>(55,9)*z[124];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf930(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf930(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
