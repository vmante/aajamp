#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf604(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[88];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[77];
    z[3]=f[81];
    z[4]=c[11];
    z[5]=d[0];
    z[6]=d[1];
    z[7]=d[2];
    z[8]=d[4];
    z[9]=d[5];
    z[10]=d[6];
    z[11]=d[7];
    z[12]=d[8];
    z[13]=d[9];
    z[14]=c[12];
    z[15]=c[13];
    z[16]=c[20];
    z[17]=c[23];
    z[18]=c[47];
    z[19]=c[48];
    z[20]=c[49];
    z[21]=c[56];
    z[22]=c[59];
    z[23]=f[24];
    z[24]=f[44];
    z[25]=f[61];
    z[26]=f[62];
    z[27]=f[69];
    z[28]=f[70];
    z[29]=f[79];
    z[30]=f[80];
    z[31]=f[22];
    z[32]=f[42];
    z[33]=d[3];
    z[34]=g[24];
    z[35]=g[27];
    z[36]=g[38];
    z[37]=g[40];
    z[38]=g[48];
    z[39]=g[50];
    z[40]=g[79];
    z[41]=g[82];
    z[42]=g[90];
    z[43]=g[92];
    z[44]=g[99];
    z[45]=g[101];
    z[46]=g[128];
    z[47]=g[131];
    z[48]=g[138];
    z[49]=g[140];
    z[50]=g[147];
    z[51]=g[170];
    z[52]=g[183];
    z[53]=g[189];
    z[54]=g[191];
    z[55]=g[212];
    z[56]=g[214];
    z[57]=g[218];
    z[58]=g[220];
    z[59]=g[243];
    z[60]=g[245];
    z[61]=g[251];
    z[62]=g[254];
    z[63]=g[270];
    z[64]=g[272];
    z[65]=g[276];
    z[66]=g[278];
    z[67]=g[281];
    z[68]=g[292];
    z[69]=g[294];
    z[70]=z[2] - z[3];
    z[71]= - n<T>(2,3)*z[70];
    z[71]=z[1]*z[71];
    z[72]=n<T>(1,5)*z[17] - 4*z[16];
    z[72]=n<T>(1,3)*z[72] - n<T>(16,5)*z[15];
    z[72]=z[8]*z[72];
    z[73]=n<T>(2,3)*z[13] + z[11];
    z[73]=n<T>(4,3)*z[73] - n<T>(1,5)*z[8];
    z[73]=z[4]*z[73];
    z[74]= - z[16] + n<T>(1,20)*z[17];
    z[75]= - n<T>(12,5)*z[15] + z[74];
    z[75]=3*z[75] - n<T>(67,540)*z[4];
    z[75]=z[12]*z[75];
    z[76]= - n<T>(24,5)*z[15] - 2*z[16] + n<T>(1,10)*z[17];
    z[77]=n<T>(1,30)*z[4] - z[76];
    z[77]=z[7]*z[77];
    z[74]= - n<T>(4,5)*z[15] + n<T>(1,3)*z[74];
    z[78]= - 37*z[74] - n<T>(347,1620)*z[4];
    z[78]=z[10]*z[78];
    z[79]=n<T>(1,4)*z[17] - 5*z[16];
    z[79]=n<T>(1,3)*z[79] - 4*z[15];
    z[79]=5*z[79] + n<T>(43,324)*z[4];
    z[79]=z[9]*z[79];
    z[74]=n<T>(49,1620)*z[4] - z[74];
    z[74]=z[6]*z[74];
    z[76]= - n<T>(67,810)*z[4] + z[76];
    z[76]=z[5]*z[76];
    z[71]=z[76] + z[74] + z[79] + z[78] + z[77] + z[75] + n<T>(1,9)*z[73] + 
    z[72] + n<T>(67,810)*z[18] + n<T>(24,5)*z[20] - n<T>(1,10)*z[22] + 2*z[21] + 
    z[71];
    z[71]=i*z[71];
    z[72]= - z[25] + n<T>(1,3)*z[2];
    z[73]=n<T>(1,3)*z[3];
    z[74]=z[72] - z[73];
    z[75]= - z[26] + n<T>(1,2)*z[27];
    z[76]=n<T>(5,6)*z[23];
    z[77]=n<T>(1,3)*z[29];
    z[78]=4*z[14];
    z[79]=n<T>(11,6)*z[24];
    z[80]= - z[79] + z[78] + z[77] - z[76] - n<T>(1,2)*z[30] + n<T>(1,3)*z[28]
    - z[74] + z[75];
    z[80]=z[5]*z[80];
    z[81]=n<T>(2,3)*z[30];
    z[82]=n<T>(2,3)*z[27];
    z[83]=n<T>(1,3)*z[23];
    z[84]=n<T>(8,3)*z[14];
    z[85]= - z[84] - n<T>(7,3)*z[26] - z[83] - z[82] - z[25] + z[81];
    z[85]=z[13]*z[85];
    z[86]=5*z[23] - z[30] + z[27];
    z[86]=n<T>(1,2)*z[86] + z[70];
    z[87]=n<T>(2,3)*z[29];
    z[79]=z[79] - z[84] - z[87] + n<T>(1,3)*z[86] + 2*z[26];
    z[79]=z[7]*z[79];
    z[84]=z[31] + z[32];
    z[86]=z[84] + z[30];
    z[86]=n<T>(1,6)*z[86];
    z[73]= - n<T>(8,3)*z[24] - n<T>(46,9)*z[14] - z[87] + n<T>(5,3)*z[26] + z[73]
     + 
    z[76] - z[25] + z[86];
    z[73]=z[10]*z[73];
    z[76]=n<T>(10,3)*z[24] + n<T>(34,9)*z[14] - z[87] + n<T>(2,3)*z[26] - n<T>(1,2)*
    z[23] + z[82] - z[72] - z[86];
    z[76]=z[9]*z[76];
    z[82]=n<T>(1,2)*z[84];
    z[84]=z[26] - z[82] + z[28];
    z[72]= - n<T>(1,2)*z[24] - n<T>(10,9)*z[14] - z[77] + z[83] - n<T>(1,6)*z[27]
     - 
    z[72] + n<T>(1,3)*z[84];
    z[72]=z[6]*z[72];
    z[74]=n<T>(4,3)*z[29] + z[81] + z[74];
    z[74]=z[33]*z[74];
    z[70]= - z[28] - 2*z[27] + z[70];
    z[70]= - z[78] - z[29] + n<T>(1,3)*z[70] - z[26];
    z[70]=z[11]*z[70];
    z[77]=n<T>(1,3)*z[8];
    z[78]=n<T>(16,3)*z[14] + z[29] - z[28] - z[23];
    z[78]=z[78]*z[77];
    z[75]=4*z[29] + z[3] + z[82] - 2*z[30] + z[75];
    z[75]=n<T>(7,6)*z[24] + n<T>(1,3)*z[75] + 6*z[14];
    z[75]=z[12]*z[75];
    z[81]= - z[62] + z[61] + z[69] + z[68];
    z[81]= - z[43] + z[51] + z[52] + z[53] + z[37] + z[34] - 20*z[60] - 
    z[63] - 8*z[64] - 4*z[65] - z[66] - z[67] + 2*z[81] + z[39] - z[40];
    z[82]=z[46] + z[47] + z[49] + z[59];
    z[83]=z[55] + z[58];
    z[84]=z[38] - z[44];
    z[86]=z[35] + z[36];
    z[87]=z[19] - z[57];
    z[77]=z[77] - z[11] + z[33] - n<T>(5,3)*z[13];
    z[77]=z[24]*z[77];

    r += n<T>(11,3)*z[41] - 3*z[42] + n<T>(7,9)*z[45] - n<T>(10,3)*z[48] + n<T>(4,9)*
      z[50] - n<T>(2,9)*z[54] - n<T>(4,3)*z[56] + z[70] + z[71] + z[72] + z[73]
       + z[74] + z[75] + z[76] + z[77] + z[78] + z[79] + z[80] + n<T>(1,3)*
      z[81] + 2*z[82] - n<T>(2,3)*z[83] - n<T>(1,9)*z[84] + z[85] + n<T>(5,3)*z[86]
       - 4*z[87];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf604(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf604(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
