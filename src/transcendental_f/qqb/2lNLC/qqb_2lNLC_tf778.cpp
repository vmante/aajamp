#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf778(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[28];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[7];
    z[6]=d[17];
    z[7]=e[9];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[31];
    z[12]=e[6];
    z[13]=e[7];
    z[14]=f[36];
    z[15]=f[39];
    z[16]=2*z[6];
    z[17]=i*z[1];
    z[18]=z[17]*z[16];
    z[19]=2*z[5];
    z[20]=z[17] - z[5];
    z[20]=z[20]*z[19];
    z[21]=npow(z[3],2);
    z[21]=z[13] + n<T>(1,2)*z[21];
    z[22]=3*z[12];
    z[23]=n<T>(1,6)*z[8];
    z[24]=2*z[7];
    z[25]=z[4] - z[17] - z[5];
    z[25]=z[4]*z[25];
    z[18]=z[25] + z[20] + z[18] - z[23] + z[24] - z[22] - z[21];
    z[18]=z[4]*z[18];
    z[20]=3*z[3];
    z[25]=n<T>(1,2)*z[5] - z[17] - z[20];
    z[25]=z[5]*z[25];
    z[19]= - 2*z[17] + z[19] + z[3];
    z[26]=z[4]*z[19];
    z[27]=z[2] + z[4];
    z[19]=z[19] - n<T>(3,2)*z[27];
    z[19]=z[2]*z[19];
    z[27]=3*z[17] + z[3];
    z[27]=z[3]*z[27];
    z[19]=z[19] + z[26] + z[25] + z[27] - z[22] - z[8];
    z[19]=z[2]*z[19];
    z[20]= - z[20] + z[16];
    z[17]=z[17]*z[20];
    z[20]=z[5]*z[3];
    z[17]=n<T>(3,2)*z[20] + z[24] + z[23] + z[17];
    z[17]=z[5]*z[17];
    z[20]=z[10] + z[14];
    z[16]=z[8]*z[16];
    z[22]=2*z[1];
    z[22]= - z[7]*z[22];
    z[22]=n<T>(1,6)*z[9] + z[22];
    z[22]=i*z[22];
    z[21]= - n<T>(4,3)*z[8] - z[21];
    z[21]=z[3]*z[21];

    r += 7*z[11] + z[15] + z[16] + z[17] + z[18] + z[19] - 2*z[20] + 
      z[21] + z[22];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf778(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf778(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
