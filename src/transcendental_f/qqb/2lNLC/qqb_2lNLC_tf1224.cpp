#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1224(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[99];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[9];
    z[8]=e[7];
    z[9]=d[2];
    z[10]=d[8];
    z[11]=d[11];
    z[12]=e[12];
    z[13]=d[12];
    z[14]=d[4];
    z[15]=d[17];
    z[16]=e[5];
    z[17]=e[9];
    z[18]=e[13];
    z[19]=e[6];
    z[20]=e[14];
    z[21]=f[30];
    z[22]=f[31];
    z[23]=f[32];
    z[24]=f[33];
    z[25]=f[35];
    z[26]=f[36];
    z[27]=f[39];
    z[28]=f[43];
    z[29]=f[50];
    z[30]=f[51];
    z[31]=f[52];
    z[32]=f[55];
    z[33]=f[58];
    z[34]=f[68];
    z[35]=c[3];
    z[36]=c[11];
    z[37]=c[15];
    z[38]=c[17];
    z[39]=c[31];
    z[40]=c[32];
    z[41]=c[39];
    z[42]=c[43];
    z[43]=f[53];
    z[44]=g[66];
    z[45]=g[67];
    z[46]=g[84];
    z[47]=g[116];
    z[48]=g[117];
    z[49]=g[133];
    z[50]=g[159];
    z[51]=g[160];
    z[52]=g[162];
    z[53]=g[163];
    z[54]=g[165];
    z[55]=g[167];
    z[56]=g[174];
    z[57]=g[175];
    z[58]=g[178];
    z[59]=g[184];
    z[60]=g[185];
    z[61]=g[186];
    z[62]=g[195];
    z[63]=g[209];
    z[64]=n<T>(1,3)*z[4];
    z[65]= - z[64] + n<T>(1,2)*z[3];
    z[66]=155*z[7];
    z[67]=z[65]*z[66];
    z[68]=n<T>(1,4)*z[3];
    z[69]=n<T>(337,3)*z[4];
    z[70]=z[68] - z[69];
    z[71]=z[70]*z[64];
    z[72]=npow(z[3],2);
    z[67]=z[71] - z[67] + n<T>(673,8)*z[72];
    z[71]=n<T>(413,12)*z[5];
    z[73]=z[71] - z[66];
    z[74]=z[69] + n<T>(673,4)*z[3];
    z[75]=z[1]*i;
    z[76]=z[74] + z[73] - n<T>(413,12)*z[75];
    z[77]=z[6]*z[76];
    z[78]=z[7]*i;
    z[79]=155*z[78];
    z[80]=65*z[3] + n<T>(1087,6)*z[4];
    z[80]=i*z[80];
    z[80]=z[80] - z[79];
    z[80]=z[1]*z[80];
    z[81]=n<T>(1,6)*z[75];
    z[82]=z[81] + z[65];
    z[82]=z[82]*z[71];
    z[77]= - n<T>(1,12)*z[77] + z[82] + n<T>(1,6)*z[80] + z[67];
    z[77]=z[6]*z[77];
    z[80]=n<T>(1,3)*z[14] - n<T>(1,6)*z[6];
    z[80]=z[76]*z[80];
    z[82]=181*z[3] + 29*z[4];
    z[82]=i*z[82];
    z[82]=n<T>(1,2)*z[82] - n<T>(155,3)*z[78];
    z[82]=z[1]*z[82];
    z[81]=z[81] - z[65];
    z[81]=z[81]*z[71];
    z[67]=z[81] + n<T>(1,2)*z[82] + z[80] - z[67];
    z[67]=z[14]*z[67];
    z[70]= - z[4]*z[70];
    z[70]= - n<T>(3625,16)*z[72] + z[70];
    z[70]=i*z[70];
    z[79]=z[65]*z[79];
    z[70]=n<T>(1,3)*z[70] + z[79];
    z[70]=z[1]*z[70];
    z[69]=z[66] - z[69];
    z[69]=z[72]*z[69];
    z[79]=npow(z[3],3);
    z[69]= - n<T>(673,4)*z[79] + z[69];
    z[65]= - z[65]*z[75];
    z[65]= - n<T>(1,4)*z[72] + z[65];
    z[65]=z[65]*z[71];
    z[65]=n<T>(1,2)*z[67] + z[77] + z[65] + n<T>(1,4)*z[69] + z[70];
    z[65]=z[14]*z[65];
    z[67]= - 331*z[3] + n<T>(9287,2)*z[4];
    z[67]=z[4]*z[67];
    z[67]= - n<T>(9509,2)*z[72] + z[67];
    z[69]=n<T>(4283,48)*z[7] - n<T>(3155,8)*z[3] + n<T>(188,3)*z[4];
    z[69]=z[7]*z[69];
    z[70]= - n<T>(209,16)*z[5] - n<T>(5677,8)*z[7] + 274*z[3] + n<T>(2843,8)*z[4];
    z[70]=z[5]*z[70];
    z[71]=n<T>(4531,16)*z[3] - 400*z[4];
    z[71]= - n<T>(329,144)*z[5] + n<T>(1,9)*z[71] + n<T>(97,4)*z[7];
    z[71]=z[6]*z[71];
    z[67]=z[71] + n<T>(1,9)*z[70] + n<T>(1,72)*z[67] + z[69];
    z[69]=4*z[9];
    z[70]=n<T>(265,18)*z[7];
    z[71]=2*z[5];
    z[77]=5*z[4];
    z[80]=n<T>(157,9)*z[3] + z[77];
    z[80]= - z[69] + 2*z[80] + z[70] - z[71];
    z[80]=z[9]*z[80];
    z[81]=4*z[5];
    z[82]=10*z[4];
    z[83]= - n<T>(287,9)*z[3] - z[82];
    z[83]=z[81] + 2*z[83] - n<T>(157,9)*z[7];
    z[83]=z[10] + n<T>(1,3)*z[83] + 2*z[9];
    z[83]=z[10]*z[83];
    z[84]=n<T>(107,2)*z[3] + n<T>(2587,3)*z[4];
    z[73]=n<T>(413,16)*z[14] + n<T>(413,8)*z[6] + n<T>(1,4)*z[84] + z[73];
    z[73]=z[14]*z[73];
    z[67]=n<T>(1,9)*z[73] + 2*z[83] + n<T>(1,3)*z[67] + z[80];
    z[67]=z[35]*z[67];
    z[73]=361*z[4] + n<T>(1895,4)*z[3];
    z[73]=n<T>(1,9)*z[73];
    z[80]=97*z[7];
    z[83]=z[73] - z[80];
    z[84]=n<T>(329,36)*z[5];
    z[85]= - z[84] + n<T>(329,36)*z[75] - z[83];
    z[85]=z[6]*z[85];
    z[83]= - z[4]*z[83];
    z[86]=197*z[4] + n<T>(1895,9)*z[3];
    z[87]=n<T>(1,4)*i;
    z[88]=z[86]*z[87];
    z[89]=97*z[78];
    z[88]=z[88] - z[89];
    z[88]=z[1]*z[88];
    z[90]= - z[4] + z[75];
    z[90]=z[90]*z[84];
    z[83]=n<T>(329,36)*z[35] + z[85] + z[90] + z[88] + z[83];
    z[83]=z[17]*z[83];
    z[73]=z[73]*i;
    z[73]=z[73] - z[89];
    z[73]=z[73]*z[1];
    z[85]=z[84]*z[75];
    z[73]=z[73] + z[85];
    z[85]= - z[6] - z[4];
    z[73]=z[73]*z[85];
    z[80]= - n<T>(329,36)*z[6] - n<T>(1,4)*z[86] + z[80] - z[84];
    z[80]=z[35]*z[80];
    z[73]=z[80] + z[73];
    z[73]=z[15]*z[73];
    z[80]=2*z[75];
    z[84]=z[80] - z[71];
    z[85]=n<T>(32,3)*z[3] + 7*z[4];
    z[85]=2*z[85] + n<T>(13,2)*z[7] + z[84];
    z[85]=z[31]*z[85];
    z[86]=n<T>(1141,72)*z[5] - n<T>(1250,9)*z[7] - n<T>(293,8)*z[3] + n<T>(424,9)*z[4];
    z[86]=z[37]*z[86];
    z[65]=z[65] + z[67] + z[73] + z[83] + z[85] + z[86];
    z[67]=z[77] + n<T>(103,9)*z[3];
    z[73]=z[70] + z[84] + 2*z[67];
    z[83]=n<T>(1,3)*z[9];
    z[84]=z[83]*z[73];
    z[85]=z[77]*z[3];
    z[85]=z[85] + n<T>(103,9)*z[72];
    z[86]=n<T>(1,3)*z[7];
    z[88]=40*z[4] + n<T>(29,9)*z[3];
    z[89]=n<T>(530,9)*z[7] + z[88];
    z[89]=z[89]*z[86];
    z[90]= - n<T>(26,9)*z[3] - z[4];
    z[90]=i*z[90];
    z[90]=20*z[90] - n<T>(193,9)*z[78];
    z[91]=n<T>(1,3)*z[1];
    z[90]=z[90]*z[91];
    z[92]=n<T>(1,3)*z[75];
    z[93]=n<T>(2,3)*z[7];
    z[94]=z[93] - z[3];
    z[95]=z[92] - z[94];
    z[95]=z[95]*z[81];
    z[89]=z[84] + z[95] + z[90] - 4*z[85] + z[89];
    z[89]=z[9]*z[89];
    z[90]=n<T>(265,9)*z[7];
    z[95]=z[90] + 20*z[4] + n<T>(29,18)*z[3];
    z[95]=z[95]*z[86];
    z[85]=z[95] - 2*z[85];
    z[67]=4*z[67];
    z[90]=z[67] + z[90];
    z[95]= - z[81] + z[90] + 4*z[75];
    z[96]=z[95]*z[10];
    z[97]=2*i;
    z[98]= - n<T>(76,9)*z[3] - z[77];
    z[98]=z[98]*z[97];
    z[98]=z[98] - n<T>(337,18)*z[78];
    z[98]=z[98]*z[91];
    z[92]=z[92] + z[94];
    z[92]=z[92]*z[71];
    z[84]= - n<T>(1,3)*z[96] + z[84] + z[92] + z[98] - z[85];
    z[84]=z[10]*z[84];
    z[77]=z[77]*z[72];
    z[77]=z[77] + n<T>(103,9)*z[79];
    z[70]=z[70]*z[72];
    z[70]=z[70] + 2*z[77];
    z[77]=2*z[4];
    z[92]=z[3]*z[77];
    z[92]=n<T>(43,9)*z[72] + z[92];
    z[92]=i*z[92];
    z[88]= - i*z[88];
    z[88]=z[88] - n<T>(530,9)*z[78];
    z[88]=z[88]*z[86];
    z[88]=10*z[92] + z[88];
    z[88]=z[1]*z[88];
    z[92]=n<T>(2,3)*z[78];
    z[98]= - i*z[3];
    z[98]=z[98] + z[92];
    z[98]=z[1]*z[98];
    z[98]= - z[72] + 2*z[98];
    z[98]=z[98]*z[71];
    z[84]=z[84] + z[89] + z[98] + z[88] + z[70];
    z[84]=z[10]*z[84];
    z[83]= - z[95]*z[83];
    z[88]=n<T>(233,9)*z[3] + z[82];
    z[88]=z[88]*z[97];
    z[89]=n<T>(229,9)*z[78];
    z[88]=z[88] + z[89];
    z[88]=z[88]*z[91];
    z[94]= - n<T>(2,3)*z[75] + z[94];
    z[94]=z[94]*z[71];
    z[83]=z[83] + z[94] + z[88] - z[85];
    z[83]=z[9]*z[83];
    z[82]= - z[3]*z[82];
    z[82]= - n<T>(197,9)*z[72] + z[82];
    z[82]=z[82]*z[97];
    z[85]=n<T>(265,9)*z[78];
    z[88]= - z[3]*z[85];
    z[82]=z[82] + z[88];
    z[82]=z[1]*z[82];
    z[80]=z[3]*z[80];
    z[80]= - z[72] + z[80];
    z[71]=z[80]*z[71];
    z[70]=z[83] + z[71] + z[82] + z[70];
    z[70]=z[9]*z[70];
    z[71]=z[75] - z[5];
    z[80]=5*z[7];
    z[82]= - z[80] - n<T>(20,3)*z[3] + n<T>(39,2)*z[4] - n<T>(5,9)*z[71];
    z[82]=z[23]*z[82];
    z[70]=z[82] + z[70] + z[84];
    z[82]= - z[9]*z[95];
    z[83]= - z[7]*z[90];
    z[84]=z[67]*i;
    z[88]=z[84] + z[89];
    z[88]=z[1]*z[88];
    z[89]=z[7] - z[75];
    z[89]=z[89]*z[81];
    z[82]= - 4*z[35] + z[82] + z[89] + z[83] + z[88];
    z[82]=z[12]*z[82];
    z[83]=z[85] + z[84];
    z[83]=z[83]*z[1];
    z[84]=z[81]*z[75];
    z[83]=z[83] - z[84];
    z[84]=z[9] + z[7];
    z[83]=z[83]*z[84];
    z[67]= - z[69] + n<T>(229,9)*z[7] + z[67] - z[81];
    z[67]=z[35]*z[67];
    z[67]=z[67] + z[83];
    z[67]=z[11]*z[67];
    z[69]= - z[7]*z[95];
    z[69]= - z[96] + z[69];
    z[69]=z[20]*z[69];
    z[81]=z[29]*z[95];
    z[67]= - z[61] + z[67] + z[82] + z[69] + z[81];
    z[69]=79*z[3] - 529*z[4];
    z[69]=z[4]*z[69];
    z[69]=n<T>(43,2)*z[72] + z[69];
    z[81]= - 73*z[3] - n<T>(139,4)*z[4];
    z[81]=i*z[81];
    z[81]=z[81] - 211*z[78];
    z[81]=z[1]*z[81];
    z[82]= - n<T>(49,36)*z[7] + n<T>(53,9)*z[3] - 28*z[4];
    z[82]=z[7]*z[82];
    z[83]= - n<T>(193,16)*z[5] - n<T>(25,2)*z[75] + 364*z[7] + 82*z[3] + n<T>(1969,4)
   *z[4];
    z[83]=z[5]*z[83];
    z[69]=n<T>(1,27)*z[83] + n<T>(1,9)*z[81] + n<T>(1,36)*z[69] + z[82];
    z[69]=z[5]*z[69];
    z[81]=n<T>(299,8)*z[3] - n<T>(1163,3)*z[4];
    z[81]=z[81]*z[64];
    z[81]=n<T>(101,8)*z[72] + z[81];
    z[81]=z[4]*z[81];
    z[82]=31*z[3] + 83*z[4];
    z[82]=z[4]*z[82];
    z[82]= - n<T>(175,3)*z[72] + z[82];
    z[83]= - 11*z[4] + n<T>(67,3)*z[3];
    z[84]= - n<T>(1639,18)*z[7] - z[83];
    z[84]=z[7]*z[84];
    z[82]=2*z[82] + z[84];
    z[82]=z[7]*z[82];
    z[81]=z[82] + n<T>(3143,72)*z[79] + z[81];
    z[82]= - n<T>(199,3)*z[3] + n<T>(2039,4)*z[4];
    z[82]=z[4]*z[82];
    z[82]=n<T>(2063,24)*z[72] + z[82];
    z[84]=n<T>(1,6)*i;
    z[82]=z[82]*z[84];
    z[85]=n<T>(11,9)*z[3] - z[4];
    z[85]=i*z[85];
    z[85]=46*z[85] + n<T>(967,18)*z[78];
    z[85]=z[7]*z[85];
    z[82]=z[82] + z[85];
    z[82]=z[1]*z[82];
    z[69]=z[69] + n<T>(1,3)*z[81] + z[82];
    z[69]=z[5]*z[69];
    z[81]=n<T>(1,9)*z[7];
    z[82]= - n<T>(359,12)*z[7] - n<T>(193,3)*z[3] - 224*z[4];
    z[82]=z[82]*z[81];
    z[85]=47*z[3] - n<T>(58,3)*z[4];
    z[85]=z[4]*z[85];
    z[82]=z[82] + n<T>(341,9)*z[72] + z[85];
    z[82]=z[7]*z[82];
    z[85]=n<T>(325,2)*z[3] - 151*z[4];
    z[85]=z[4]*z[85];
    z[85]=n<T>(377,2)*z[72] + z[85];
    z[85]=z[4]*z[85];
    z[85]= - n<T>(2725,9)*z[79] + z[85];
    z[82]=n<T>(1,3)*z[85] + z[82];
    z[82]=z[7]*z[82];
    z[85]=2*z[3] - n<T>(623,6)*z[4];
    z[85]=z[4]*z[85];
    z[85]= - n<T>(3217,36)*z[72] + z[85];
    z[85]=i*z[85];
    z[83]=i*z[83];
    z[83]=z[83] + n<T>(1639,18)*z[78];
    z[83]=z[83]*z[86];
    z[83]=z[85] + z[83];
    z[83]=z[7]*z[83];
    z[85]=125*z[3] + n<T>(3409,9)*z[4];
    z[85]=z[4]*z[85];
    z[85]= - n<T>(125,12)*z[72] + z[85];
    z[85]=z[4]*z[85];
    z[85]= - n<T>(19267,72)*z[79] + z[85];
    z[84]=z[85]*z[84];
    z[83]=z[84] + z[83];
    z[83]=z[1]*z[83];
    z[84]=175*z[3] + n<T>(713,2)*z[4];
    z[84]=z[84]*z[64];
    z[84]= - n<T>(3379,8)*z[72] + z[84];
    z[84]=z[4]*z[84];
    z[84]= - n<T>(5873,24)*z[79] + z[84];
    z[84]=z[4]*z[84];
    z[85]=npow(z[3],4);
    z[84]= - n<T>(11351,16)*z[85] + z[84];
    z[69]=z[69] + z[83] + n<T>(1,9)*z[84] + z[82];
    z[74]=z[4]*z[74];
    z[74]= - n<T>(337,2)*z[72] + z[74];
    z[64]=z[74]*z[64];
    z[74]=npow(z[4],2);
    z[74]=n<T>(1,2)*z[72] - n<T>(1,3)*z[74];
    z[66]=z[74]*z[66];
    z[64]=z[66] - n<T>(673,8)*z[79] + z[64];
    z[66]=11*z[3] - n<T>(3109,18)*z[4];
    z[66]=z[4]*z[66];
    z[66]=n<T>(12857,36)*z[72] + z[66];
    z[66]=z[66]*z[87];
    z[74]= - n<T>(737,2)*z[3] + 155*z[4];
    z[74]=z[74]*z[78];
    z[66]=z[66] + n<T>(1,3)*z[74];
    z[66]=z[1]*z[66];
    z[64]=n<T>(1,2)*z[64] + z[66];
    z[66]=4*z[7];
    z[74]=z[4] - z[3];
    z[82]=z[66]*z[74];
    z[83]=z[74]*z[5];
    z[82]=z[82] + n<T>(7,34)*z[83];
    z[84]= - 55*z[3] + n<T>(509,4)*z[4];
    z[84]=z[4]*z[84];
    z[84]= - n<T>(991,8)*z[72] + z[84];
    z[85]=n<T>(47,2)*z[3] - n<T>(53,3)*z[4];
    z[85]=z[85]*z[75];
    z[84]=n<T>(7,68)*z[85] + n<T>(1,102)*z[84] - z[82];
    z[84]=z[5]*z[84];
    z[85]= - n<T>(7,4)*z[71];
    z[68]= - z[68] + n<T>(53,153)*z[4];
    z[68]=z[68]*z[85];
    z[85]=n<T>(449,2)*z[3] + 349*z[4];
    z[85]=z[4]*z[85];
    z[85]= - n<T>(5809,16)*z[72] + z[85];
    z[87]=n<T>(737,4)*z[3] - 223*z[4];
    z[87]=z[7]*z[87];
    z[85]=n<T>(1,3)*z[85] + z[87];
    z[68]=n<T>(1,51)*z[85] + z[68];
    z[68]=z[6]*z[68];
    z[64]=z[68] + n<T>(1,17)*z[64] + n<T>(1,3)*z[84];
    z[64]=z[6]*z[64];
    z[68]=z[5] - z[3];
    z[84]=n<T>(23,306)*z[75];
    z[85]=z[84] - z[77] + 2*z[7] - n<T>(23,306)*z[68];
    z[85]=z[34]*z[85];
    z[87]=n<T>(329,34)*z[6] + n<T>(271,34)*z[5] - n<T>(11725,34)*z[3] - 331*z[4];
    z[87]=i*z[87];
    z[87]=n<T>(6353,17)*z[78] + z[87];
    z[88]=n<T>(329,612)*z[15] + n<T>(8,17)*z[11] - n<T>(29,306)*z[13] - n<T>(413,1836)*
    z[14] - n<T>(8,51)*z[10] + n<T>(2,17)*z[9];
    z[88]=i*z[88];
    z[87]=n<T>(1,216)*z[87] + z[88];
    z[87]=z[36]*z[87];
    z[88]= - n<T>(23,18)*z[3] - z[77];
    z[88]=n<T>(29,17)*z[88] + n<T>(50,3)*z[7] - n<T>(11,34)*z[71];
    z[88]=z[24]*z[88];
    z[64]=z[85] + z[88] + z[87] + n<T>(1,17)*z[69] + z[64];
    z[69]=z[74]*z[75];
    z[85]=4*z[4];
    z[87]=n<T>(55,6)*z[3] - z[85];
    z[87]=z[4]*z[87];
    z[87]= - n<T>(31,6)*z[72] + z[87];
    z[82]= - n<T>(7,34)*z[69] + n<T>(1,17)*z[87] + z[82];
    z[82]=z[6]*z[82];
    z[87]= - n<T>(7,6)*z[3] + z[85];
    z[87]=z[4]*z[87];
    z[87]= - n<T>(79,6)*z[72] + z[87];
    z[87]=z[4]*z[87];
    z[79]=n<T>(31,3)*z[79] + z[87];
    z[87]= - z[3] - z[4];
    z[87]=z[4]*z[87];
    z[87]=2*z[72] + z[87];
    z[87]=z[87]*z[66];
    z[79]=z[82] + n<T>(1,17)*z[79] + z[87];
    z[82]= - n<T>(5,34)*z[4] + n<T>(1,9)*z[3];
    z[82]=z[82]*z[4];
    z[82]=z[82] + n<T>(11,306)*z[72];
    z[87]=z[74]*z[7];
    z[88]=n<T>(7,102)*z[83] - n<T>(7,51)*z[69] + n<T>(4,3)*z[87] + z[82];
    z[88]=z[5]*z[88];
    z[82]= - i*z[82];
    z[89]=n<T>(4,3)*z[74];
    z[89]= - z[78]*z[89];
    z[82]=z[82] + z[89];
    z[82]=z[1]*z[82];
    z[69]=z[69] - z[83];
    z[83]= - n<T>(55,12)*z[3] + z[77];
    z[83]=z[4]*z[83];
    z[83]=n<T>(31,12)*z[72] + z[83];
    z[69]=n<T>(1,17)*z[83] - 2*z[87] + n<T>(7,68)*z[69];
    z[83]=n<T>(1,3)*z[2];
    z[69]=z[69]*z[83];
    z[74]=z[35]*z[74];
    z[69]=z[69] - n<T>(7,102)*z[74] + z[88] + z[82] + n<T>(1,3)*z[79];
    z[69]=z[69]*z[83];
    z[74]=n<T>(7289,12)*z[3] + 194*z[4];
    z[74]=z[4]*z[74];
    z[79]= - 842*z[3] - 553*z[4];
    z[79]=z[7]*z[79];
    z[74]=z[79] + n<T>(2429,6)*z[72] + z[74];
    z[79]= - 8*z[4] + n<T>(31,3)*z[3];
    z[79]=n<T>(1,17)*z[79] + 8*z[7] - n<T>(7,17)*z[71];
    z[82]=z[2] - z[6];
    z[82]=n<T>(1,9)*z[82];
    z[79]=z[79]*z[82];
    z[82]=n<T>(5,4)*z[4] + n<T>(22,27)*z[3];
    z[82]=n<T>(5,17)*z[82];
    z[83]= - i*z[82];
    z[83]=z[83] + n<T>(8,9)*z[78];
    z[83]=z[1]*z[83];
    z[82]= - n<T>(7,153)*z[5] + n<T>(14,153)*z[75] + z[82] - n<T>(8,9)*z[7];
    z[82]=z[5]*z[82];
    z[74]=n<T>(41,153)*z[8] + n<T>(47,1224)*z[35] + z[82] + n<T>(1,153)*z[74] + 
    z[83] + z[79];
    z[74]=z[8]*z[74];
    z[79]= - z[4] + n<T>(47,306)*z[3];
    z[79]=z[79]*z[4];
    z[79]=z[79] - n<T>(13,204)*z[72];
    z[82]=z[3] + n<T>(305,6)*z[4];
    z[82]=n<T>(1,17)*z[82];
    z[83]=z[82]*i;
    z[83]=z[83] + z[78];
    z[83]=z[83]*z[91];
    z[82]=z[82] + z[7];
    z[87]=n<T>(2,51)*z[75] + z[82];
    z[88]=n<T>(3,68)*z[5];
    z[87]=n<T>(1,3)*z[87] - z[88];
    z[87]=z[5]*z[87];
    z[89]=n<T>(2,3)*z[3];
    z[90]=z[89] - z[4];
    z[91]=2*z[90] + z[7];
    z[91]=z[7]*z[91];
    z[84]=z[13]*z[84];
    z[84]= - n<T>(23,612)*z[16] + z[84] + n<T>(29,918)*z[35] + z[87] - z[83] + 
    z[91] - z[79];
    z[84]=z[16]*z[84];
    z[79]=i*z[79];
    z[87]= - z[90]*z[97];
    z[87]=z[87] - z[78];
    z[87]=z[7]*z[87];
    z[79]=z[79] + z[87];
    z[79]=z[1]*z[79];
    z[87]=z[75]*z[88];
    z[83]= - z[83] + z[87];
    z[83]=z[5]*z[83];
    z[82]=n<T>(2,51)*z[5] - z[82];
    z[82]=n<T>(23,612)*z[13] + n<T>(1,3)*z[82];
    z[82]=z[35]*z[82];
    z[79]=z[79] + z[83] + z[82];
    z[79]=z[13]*z[79];
    z[82]=z[89] + z[4];
    z[75]=z[75] + z[93] + z[82];
    z[75]=2*z[75] - z[5];
    z[75]=z[5]*z[75];
    z[83]= - n<T>(52,3)*z[3] - 3*z[4];
    z[83]=z[4]*z[83];
    z[75]=z[83] + z[75];
    z[83]= - n<T>(727,3)*z[3] - 104*z[4];
    z[80]=n<T>(1,17)*z[83] - z[80];
    z[80]=z[80]*z[86];
    z[82]= - i*z[82];
    z[82]=z[82] - z[92];
    z[82]=z[1]*z[82];
    z[72]= - n<T>(103,153)*z[18] + n<T>(31,408)*z[35] + n<T>(4,17)*z[82] + z[80]
     - 
   n<T>(101,18)*z[72] + n<T>(2,17)*z[75];
    z[72]=z[18]*z[72];
    z[75]=z[14] + z[4];
    z[75]=z[19]*z[75];
    z[75]=n<T>(1,153)*z[75] - n<T>(1,204)*z[27] + n<T>(1,612)*z[26] - n<T>(1,153)*z[21];
    z[75]=z[76]*z[75];
    z[76]= - z[85] + n<T>(31,6)*z[3];
    z[66]=z[66] + n<T>(1,17)*z[76] - n<T>(7,34)*z[71];
    z[76]=z[28] - z[25];
    z[76]= - n<T>(1,9)*z[76];
    z[66]=z[66]*z[76];
    z[76]=z[33] + z[30];
    z[76]= - n<T>(1,51)*z[32] + n<T>(1,17)*z[76];
    z[73]=z[73]*z[76];
    z[68]= - z[4] - n<T>(23,612)*z[68];
    z[68]=i*z[68];
    z[68]=z[78] + z[68];
    z[68]=z[38]*z[68];
    z[76]=n<T>(71,9)*z[3] + z[77];
    z[76]=n<T>(1,17)*z[76] - z[81];
    z[76]=z[43]*z[76];
    z[77]=z[62] + z[60];
    z[71]=n<T>(1009,9)*z[7] - n<T>(5561,108)*z[3] - 41*z[4] + n<T>(301,36)*z[71];
    z[71]=z[22]*z[71];
    z[78]= - n<T>(1841,16)*z[3] + 79*z[4];
    z[78]= - n<T>(4793,16)*z[5] + 11*z[78] - 109*z[7];
    z[78]=z[39]*z[78];
    z[80]=z[41]*i;

    r +=  - n<T>(373,88128)*z[40] - n<T>(23,612)*z[42] - n<T>(35,153)*z[44] + n<T>(181,153)*z[45]
     + n<T>(181,459)*z[46]
     + n<T>(11,102)*z[47] - n<T>(137,153)*z[48]
       - n<T>(161,459)*z[49] + n<T>(73,153)*z[50] - n<T>(1,4)*z[51] - n<T>(175,102)*
      z[52] + n<T>(16,51)*z[53] + n<T>(241,612)*z[54] - n<T>(263,918)*z[55] - n<T>(29,459)*z[56]
     - n<T>(37,612)*z[57]
     + n<T>(50,153)*z[58] - n<T>(43,918)*z[59]
     + n<T>(8,51)*z[63]
     + n<T>(1,3)*z[64]
     + n<T>(1,51)*z[65]
     + z[66]
     + n<T>(2,51)*z[67]
     +  z[68] + z[69] + n<T>(1,17)*z[70] + n<T>(1,68)*z[71] + z[72] + z[73] + 
      z[74] + z[75] + 8*z[76] - n<T>(1,34)*z[77] + n<T>(1,1377)*z[78] + z[79]
       - n<T>(1141,3672)*z[80] + z[84];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1224(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1224(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
