#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf82(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[28];
  std::complex<T> r(0,0);

    z[1]=c[32];
    z[2]=c[33];
    z[3]=c[35];
    z[4]=c[36];
    z[5]=c[37];
    z[6]=c[38];
    z[7]=c[40];
    z[8]=c[43];
    z[9]=c[47];
    z[10]=c[48];
    z[11]=c[49];
    z[12]=c[55];
    z[13]=c[56];
    z[14]=c[59];
    z[15]=g[40];
    z[16]=g[79];
    z[17]=g[82];
    z[18]=g[90];
    z[19]=g[99];
    z[20]=g[101];
    z[21]=g[110];
    z[22]=g[112];
    z[23]=g[113];
    z[24]=g[359];
    z[25]=g[362];
    z[26]=n<T>(7,15)*z[14] - 4*z[13];
    z[26]=n<T>(5,9)*z[2] - n<T>(256,5)*z[5] + n<T>(694,45)*z[6] - n<T>(11,135)*z[9]
     + n<T>(1,3)*z[26] - 16*z[11];
    z[26]=i*z[26];
    z[26]=z[26] + z[15] + z[16] - z[21];
    z[27]=z[17] + z[18];

    r +=  - n<T>(406,135)*z[1] + n<T>(288,5)*z[3] + 48*z[4] - 40*z[7] - 12*z[8]
       + n<T>(64,3)*z[10] - n<T>(6,5)*z[12] - n<T>(8,3)*z[19] + n<T>(20,3)*z[20]
     - 4*
      z[22] - 8*z[23] + n<T>(1,3)*z[24] + z[25] + 2*z[26] + 14*z[27];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf82(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf82(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
