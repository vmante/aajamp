#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf538(
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[5];
  std::complex<T> r(0,0);

    z[1]=e[17];
    z[2]=e[19];
    z[3]=e[20];

    r +=  - z[1] + z[2] + z[3];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf538(
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf538(
  const std::array<std::complex<dd_real>,24>& e
);
#endif
