#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf808(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[74];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[16];
    z[12]=d[5];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[21];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[3];
    z[33]=e[6];
    z[34]=e[7];
    z[35]=e[14];
    z[36]=e[13];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[11];
    z[43]=f[12];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[18];
    z[47]=f[31];
    z[48]=f[36];
    z[49]=f[39];
    z[50]=f[51];
    z[51]=f[55];
    z[52]=n<T>(1,2)*z[6];
    z[53]=z[1]*i;
    z[54]=z[52] - z[53];
    z[55]=3031*z[6];
    z[56]= - z[54]*z[55];
    z[57]= - n<T>(5093,2)*z[5] + 5093*z[53] + z[55];
    z[57]=z[5]*z[57];
    z[56]=z[56] + z[57];
    z[57]=n<T>(1,1531)*z[2];
    z[58]= - 1281*z[2] - 4062*z[53] + 1031*z[5];
    z[58]=z[58]*z[57];
    z[59]=z[53] - z[10];
    z[60]=z[10]*z[59];
    z[61]=npow(z[7],2);
    z[55]= - z[55] - 5093*z[5];
    z[55]=n<T>(1,2)*z[55] + 5593*z[2];
    z[55]=n<T>(1,1531)*z[55] - n<T>(5,3)*z[4];
    z[55]=z[4]*z[55];
    z[55]=n<T>(1,2)*z[55] + n<T>(1,4)*z[61] + z[58] + n<T>(1,3062)*z[56] - 2*z[60];
    z[55]=z[4]*z[55];
    z[56]=n<T>(1,1531)*z[10];
    z[58]=z[53] - z[5];
    z[60]=20777*z[58] + n<T>(16153,2)*z[10];
    z[60]=z[60]*z[56];
    z[61]= - z[53] + n<T>(1,2)*z[5];
    z[62]=z[61]*z[5];
    z[63]=npow(z[6],2);
    z[60]=z[60] - z[63] + n<T>(17777,4593)*z[62];
    z[64]=z[53] - z[6];
    z[65]= - z[2]*z[64];
    z[66]= - 22277*z[58] - 9967*z[3];
    z[66]=z[3]*z[66];
    z[67]= - n<T>(63521,3)*z[58] + 20777*z[10];
    z[67]= - n<T>(58333,4593)*z[3] + n<T>(1,3062)*z[67] - z[2];
    z[67]=n<T>(1,2)*z[67] - n<T>(1,3)*z[7];
    z[67]=z[7]*z[67];
    z[60]= - z[35] + z[67] + n<T>(1,4593)*z[66] + n<T>(1,2)*z[60] + z[65];
    z[60]=n<T>(1,2)*z[60];
    z[60]=z[7]*z[60];
    z[65]=2*z[53];
    z[66]=z[65] - z[6];
    z[67]=578*z[6];
    z[66]=z[66]*z[67];
    z[68]=n<T>(1,6)*z[5];
    z[69]=22277*z[53] - 29213*z[5];
    z[69]=z[69]*z[68];
    z[70]=npow(z[10],2);
    z[71]=z[2]*z[58];
    z[66]= - n<T>(3843,2)*z[71] - n<T>(12341,4)*z[70] + z[66] + z[69];
    z[67]= - n<T>(2687,3)*z[5] - n<T>(6155,12)*z[53] + z[67];
    z[69]=n<T>(1,2)*z[10];
    z[67]=n<T>(12107,4593)*z[3] - n<T>(3843,6124)*z[2] + n<T>(1,1531)*z[67] + z[69];
    z[67]=z[3]*z[67];
    z[66]=n<T>(1,1531)*z[66] + z[67];
    z[66]=z[3]*z[66];
    z[67]= - 127067*z[6] - n<T>(54211,2)*z[5];
    z[67]=n<T>(1,2)*z[67] - 29399*z[10];
    z[67]= - n<T>(1031,2)*z[4] - n<T>(38399,3)*z[7] - n<T>(8467,2)*z[3] + n<T>(1,3)*
    z[67] + 281*z[2];
    z[67]=n<T>(1,6)*z[67] + 1281*z[13];
    z[67]=z[22]*z[67];
    z[71]=n<T>(31463,6)*z[7] - 1156*z[64] + n<T>(17591,6)*z[3];
    z[71]=z[21]*z[71];
    z[72]=n<T>(5405,2)*z[10] + 1937*z[3];
    z[72]=z[32]*z[72];
    z[67]=z[72] + z[67] + z[71];
    z[71]=22277*z[13];
    z[72]=13091*z[6] + z[71];
    z[72]=z[53]*z[72];
    z[72]=13091*z[22] + z[72];
    z[72]=z[14]*z[72];
    z[71]= - 13091*z[64] + z[71];
    z[71]=z[20]*z[71];
    z[71]=z[72] + z[71];
    z[72]=2687*z[3];
    z[73]=6155*z[5] + z[72];
    z[73]=z[53]*z[73];
    z[73]=6155*z[22] + z[73];
    z[73]=z[9]*z[73];
    z[58]= - 6155*z[58] + z[72];
    z[58]=z[17]*z[58];
    z[58]=z[73] + z[58];
    z[54]=z[54]*z[6];
    z[68]=z[68] - z[53] + n<T>(5655,6124)*z[6];
    z[68]=z[5]*z[68];
    z[68]=n<T>(5655,3062)*z[54] + z[68];
    z[68]=z[5]*z[68];
    z[72]=4155*z[53] + n<T>(5843,2)*z[6];
    z[52]=z[72]*z[52];
    z[72]=625*z[5] + n<T>(4405,2)*z[53] - 4343*z[6];
    z[72]=z[5]*z[72];
    z[52]=z[52] + z[72];
    z[72]= - 531*z[53] - 164*z[6];
    z[72]=n<T>(3655,4)*z[2] + 2*z[72] - n<T>(281,4)*z[5];
    z[57]=z[72]*z[57];
    z[52]=z[57] + n<T>(1,1531)*z[52] - 2*z[70];
    z[52]=z[2]*z[52];
    z[57]=z[69] + z[64];
    z[57]=z[10]*z[57];
    z[57]= - n<T>(1,3)*z[22] + z[54] + z[57];
    z[69]=z[53] + z[10];
    z[69]= - 1156*z[6] + n<T>(6155,4)*z[69];
    z[69]=n<T>(1,1531)*z[69] - n<T>(1,4)*z[4];
    z[69]=z[12]*z[69];
    z[57]=n<T>(6155,3062)*z[57] + z[69];
    z[57]=z[12]*z[57];
    z[69]=n<T>(1,2)*z[2];
    z[70]= - z[53] + z[69];
    z[70]=z[2]*z[70];
    z[72]=n<T>(1,2)*z[8];
    z[73]=n<T>(1,3)*z[8] + z[53] - z[2];
    z[73]=z[73]*z[72];
    z[70]=z[73] + z[70] - n<T>(6905,9186)*z[22];
    z[70]=z[8]*z[70];
    z[73]=z[4] + z[10];
    z[73]=z[53]*z[73];
    z[73]=z[22] + z[73];
    z[73]=z[11]*z[73];
    z[59]= - z[4] + z[59];
    z[59]=z[18]*z[59];
    z[59]=z[38] + z[73] + z[59];
    z[73]=15778*z[53] - n<T>(44647,4)*z[6];
    z[73]=z[6]*z[73];
    z[64]= - 2*z[64] - z[10];
    z[64]=z[10]*z[64];
    z[64]=z[73] + 7889*z[64];
    z[73]= - z[53] - z[6];
    z[73]=n<T>(22277,3)*z[73] + 6405*z[13];
    z[73]=z[13]*z[73];
    z[64]=n<T>(1,3)*z[64] + n<T>(1,4)*z[73];
    z[64]= - n<T>(1,2)*z[34] + n<T>(1,1531)*z[64];
    z[64]=z[13]*z[64];
    z[73]= - z[53] + n<T>(1,6)*z[6];
    z[63]=z[73]*z[63];
    z[54]= - n<T>(13091,3)*z[54] + n<T>(20777,2)*z[62];
    z[53]= - n<T>(21139,8)*z[53] + 2462*z[6];
    z[53]=n<T>(1,3)*z[53] + n<T>(481,8)*z[5];
    z[53]=5*z[53] - n<T>(15544,3)*z[10];
    z[53]=z[10]*z[53];
    z[53]=n<T>(1,2)*z[54] + z[53];
    z[53]=z[53]*z[56];
    z[54]=z[69] + z[61];
    z[54]=z[16]*z[54];
    z[56]=z[51] + z[50];
    z[61]= - n<T>(13186,1531)*z[29] + n<T>(593,3062)*z[27] + z[26] + n<T>(46897,110232)*z[23];
    z[61]=i*z[61];
    z[62]=z[65] - z[2];
    z[65]= - z[4] + z[62];
    z[65]=z[15]*z[65];
    z[62]=z[6] - z[62];
    z[62]=z[19]*z[62];
    z[69]= - 15778*z[10] - n<T>(36149,2)*z[13];
    z[69]=z[33]*z[69];
    z[72]=z[36]*z[72];

    r +=  - n<T>(26870,4593)*z[24] + n<T>(77143,36744)*z[25] - n<T>(3655,6124)*
      z[28] + n<T>(11655,3062)*z[30] + n<T>(332965,18372)*z[31] - 5*z[37] - 
      n<T>(5093,6124)*z[39] + n<T>(63521,36744)*z[40] + n<T>(6717,6124)*z[41] + 
      n<T>(5655,6124)*z[42] - n<T>(2405,12248)*z[43] + n<T>(16153,12248)*z[44] - 
      n<T>(3031,6124)*z[45] - n<T>(5,3)*z[46] - n<T>(1156,1531)*z[47] - n<T>(12310,4593)
      *z[48] + n<T>(6155,6124)*z[49] + z[52] + z[53] + n<T>(281,1531)*z[54] + 
      z[55] - n<T>(1,4)*z[56] + z[57] + n<T>(2,4593)*z[58] + 2*z[59] + z[60] + 
      z[61] + n<T>(656,1531)*z[62] + n<T>(8467,3062)*z[63] + z[64] + n<T>(6624,1531)
      *z[65] + z[66] + n<T>(1,1531)*z[67] + z[68] + n<T>(1,4593)*z[69] + z[70]
       + n<T>(1,9186)*z[71] + z[72];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf808(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf808(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
