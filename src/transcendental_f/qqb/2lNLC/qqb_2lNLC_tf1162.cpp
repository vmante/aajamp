#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1162(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[50];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[7];
    z[7]=d[4];
    z[8]=d[6];
    z[9]=d[17];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[7];
    z[13]=e[8];
    z[14]=e[9];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[31];
    z[24]=e[6];
    z[25]=f[3];
    z[26]=f[11];
    z[27]=f[13];
    z[28]=f[17];
    z[29]=f[30];
    z[30]=f[31];
    z[31]=f[33];
    z[32]=f[36];
    z[33]=z[2]*i;
    z[34]=i*z[6];
    z[35]=n<T>(1,2)*z[34] - n<T>(2,3)*z[33];
    z[35]=z[1]*z[35];
    z[36]=2*z[6];
    z[37]=z[36] + z[2];
    z[38]=z[1]*i;
    z[39]=z[5] + 2*z[38] - z[37];
    z[39]=z[5]*z[39];
    z[40]=npow(z[2],2);
    z[41]=n<T>(1,3)*z[40];
    z[42]=npow(z[6],2);
    z[43]=n<T>(7,4)*z[6] - n<T>(2,3)*z[2];
    z[43]=z[4]*z[43];
    z[35]=z[43] + z[39] + z[35] - n<T>(5,4)*z[42] - z[41];
    z[35]=z[4]*z[35];
    z[39]=2*z[8];
    z[43]=z[6] + z[38];
    z[43]=n<T>(2,3)*z[8] + n<T>(1,3)*z[43] - z[5];
    z[43]=z[43]*z[39];
    z[44]=z[38]*z[6];
    z[45]= - z[36] + z[2];
    z[45]=z[2]*z[45];
    z[46]=npow(z[5],2);
    z[43]=z[43] - 2*z[46] + n<T>(2,3)*z[44] + n<T>(4,3)*z[42] + z[45];
    z[43]=z[8]*z[43];
    z[45]=z[34] - z[33];
    z[46]=2*z[1];
    z[45]=z[45]*z[46];
    z[46]= - z[2] + 3*z[5];
    z[47]=z[38] - z[6];
    z[48]=z[47] + z[46];
    z[48]=z[5]*z[48];
    z[37]=z[2]*z[37];
    z[37]=z[48] + z[45] - z[42] + z[37];
    z[37]=z[5]*z[37];
    z[45]=n<T>(3,2)*z[6];
    z[48]=n<T>(17,6)*z[4] + z[45] - n<T>(13,3)*z[2];
    z[48]=z[3]*z[48];
    z[40]=z[48] + n<T>(3,2)*z[42] - n<T>(5,3)*z[40];
    z[48]= - n<T>(3,2)*z[34] + n<T>(10,3)*z[33];
    z[48]=z[1]*z[48];
    z[45]= - n<T>(13,12)*z[4] + z[5] - n<T>(11,6)*z[38] - z[45] + n<T>(8,3)*z[2];
    z[45]=z[4]*z[45];
    z[49]= - z[5]*z[2];
    z[40]=z[45] + z[49] + z[48] + n<T>(1,2)*z[40];
    z[40]=z[3]*z[40];
    z[36]=z[38]*z[36];
    z[45]= - z[47]*z[39];
    z[48]=z[7] + z[8];
    z[49]= - 2*z[47] - z[48];
    z[49]=z[7]*z[49];
    z[36]=z[49] + z[45] - z[42] + z[36];
    z[45]=n<T>(1,3)*z[7];
    z[36]=z[36]*z[45];
    z[39]=z[39] + z[4] - z[47] + z[46];
    z[39]=z[12]*z[39];
    z[39]=z[39] - z[30];
    z[33]= - z[34] + n<T>(1,6)*z[33];
    z[33]=z[1]*z[33];
    z[33]=z[33] + n<T>(1,2)*z[42] + z[41];
    z[33]=z[2]*z[33];
    z[34]= - z[8]*z[38];
    z[34]= - z[15] - z[44] + z[34];
    z[34]=z[9]*z[34];
    z[41]= - z[8] + z[47];
    z[41]=z[14]*z[41];
    z[34]=z[17] + z[34] + z[41];
    z[41]=n<T>(31,4)*z[6] - 5*z[2];
    z[41]=z[45] - n<T>(55,24)*z[3] + z[8] - n<T>(17,12)*z[4] + n<T>(1,6)*z[41] + 5*
    z[5];
    z[41]=z[15]*z[41];
    z[42]=2*z[4] - z[38] - z[2];
    z[42]=z[11]*z[42];
    z[41]=z[41] + z[42];
    z[42]=z[2] + z[6];
    z[42]= - n<T>(1,2)*z[42] + z[38];
    z[42]=z[13]*z[42];
    z[44]=z[24]*z[48];
    z[44]= - z[29] + z[44] + z[32];
    z[45]=z[27] - z[20];
    z[46]=n<T>(11,3)*z[21] + n<T>(11,6)*z[19] - n<T>(1,9)*z[16];
    z[46]=i*z[46];
    z[38]=n<T>(7,2)*z[2] - 10*z[38];
    z[38]=n<T>(13,6)*z[3] + n<T>(1,3)*z[38] + z[5];
    z[38]=z[10]*z[38];

    r += n<T>(11,72)*z[18] - n<T>(11,6)*z[22] + n<T>(1,6)*z[23] + n<T>(17,12)*z[25]
     - n<T>(5,4)*z[26]
     + n<T>(3,4)*z[28] - 8*z[31]
     + z[33]
     + n<T>(4,3)*z[34]
     + z[35]
       + z[36] + z[37] + z[38] + 2*z[39] + z[40] + n<T>(1,3)*z[41] + z[42]
       + z[43] - n<T>(2,3)*z[44] + n<T>(11,12)*z[45] + z[46];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1162(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1162(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
