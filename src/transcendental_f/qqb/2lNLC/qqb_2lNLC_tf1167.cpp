#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1167(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[11];
    z[9]=d[4];
    z[10]=d[6];
    z[11]=d[17];
    z[12]=d[9];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[7];
    z[16]=e[8];
    z[17]=e[9];
    z[18]=e[12];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[19];
    z[23]=c[23];
    z[24]=c[25];
    z[25]=c[26];
    z[26]=c[28];
    z[27]=c[31];
    z[28]=e[6];
    z[29]=e[13];
    z[30]=e[14];
    z[31]=f[3];
    z[32]=f[11];
    z[33]=f[13];
    z[34]=f[17];
    z[35]=f[30];
    z[36]=f[31];
    z[37]=f[33];
    z[38]=f[36];
    z[39]=f[50];
    z[40]=f[51];
    z[41]=f[55];
    z[42]=f[58];
    z[43]=z[12] + z[3];
    z[44]=z[1]*i;
    z[43]=z[44]*z[43];
    z[43]=z[19] + z[43];
    z[43]=z[8]*z[43];
    z[45]=z[10] + z[6];
    z[45]=z[44]*z[45];
    z[45]=z[19] + z[45];
    z[45]=z[11]*z[45];
    z[46]=z[44] - z[6];
    z[47]=z[10] - z[46];
    z[47]=z[17]*z[47];
    z[48]=z[44] - z[3];
    z[49]= - z[12] + z[48];
    z[49]=z[18]*z[49];
    z[50]= - z[7] - z[12];
    z[50]=z[30]*z[50];
    z[43]=z[47] + z[49] + z[50] + z[39] + z[27] + z[43] + z[45];
    z[45]=7*z[3];
    z[47]=z[44] - z[45];
    z[47]=n<T>(2,3)*z[2] + n<T>(1,3)*z[47] + z[5];
    z[47]=z[2]*z[47];
    z[49]=2*z[44];
    z[50]= - z[49] + n<T>(3,2)*z[6];
    z[50]=z[6]*z[50];
    z[51]= - n<T>(11,6)*z[3] + n<T>(11,3)*z[44] + z[6];
    z[51]=z[3]*z[51];
    z[52]=z[49] - z[6];
    z[53]= - z[5] - z[52];
    z[53]=z[5]*z[53];
    z[54]=n<T>(1,3)*z[2] - z[6] + n<T>(2,3)*z[3];
    z[54]=z[4]*z[54];
    z[47]=z[54] + z[47] + z[53] + z[50] + z[51];
    z[47]=z[4]*z[47];
    z[50]=n<T>(1,2)*z[6];
    z[51]=z[50] - z[44];
    z[51]=z[51]*z[6];
    z[53]=z[9] + z[10];
    z[54]=z[46] + n<T>(1,2)*z[53];
    z[54]=z[9]*z[54];
    z[55]=z[10]*z[46];
    z[54]=z[54] + z[51] + z[55];
    z[54]=z[9]*z[54];
    z[55]=npow(z[3],2);
    z[56]= - 2*z[48] - z[7];
    z[56]=z[7]*z[56];
    z[55]= - z[55] + z[56];
    z[55]=z[12]*z[55];
    z[56]=z[49] - z[2];
    z[57]= - z[4] + z[56];
    z[57]=z[14]*z[57];
    z[53]=z[28]*z[53];
    z[53]= - z[35] + z[57] + z[54] + z[55] + z[53] + z[38];
    z[54]=n<T>(1,2)*z[5];
    z[46]=z[54] + z[46];
    z[46]=z[5]*z[46];
    z[55]=z[49] + n<T>(7,2)*z[3];
    z[55]= - n<T>(5,6)*z[2] + n<T>(1,3)*z[55] - z[54];
    z[55]=z[2]*z[55];
    z[57]=z[6]*z[52];
    z[49]=z[49] - z[3];
    z[49]=z[3]*z[49];
    z[46]=z[55] + z[46] + z[57] - n<T>(7,3)*z[49];
    z[46]=z[2]*z[46];
    z[49]= - z[44] - 2*z[6];
    z[49]=z[6]*z[49];
    z[55]=npow(z[5],2);
    z[57]=z[6] - n<T>(1,2)*z[2];
    z[57]=z[2]*z[57];
    z[58]= - z[44] - z[6];
    z[58]= - n<T>(2,3)*z[10] + n<T>(1,3)*z[58] + z[5];
    z[58]=z[10]*z[58];
    z[49]=z[58] + z[57] + n<T>(1,3)*z[49] + z[55];
    z[49]=z[10]*z[49];
    z[55]=n<T>(1,3)*z[3];
    z[50]= - z[55] + n<T>(1,3)*z[44] - z[50];
    z[50]=z[3]*z[50];
    z[50]= - z[51] + z[50];
    z[50]=z[3]*z[50];
    z[57]=2*z[5];
    z[58]=z[6] - z[48];
    z[58]=n<T>(1,2)*z[58] - z[57];
    z[58]=z[5]*z[58];
    z[44]= - z[44] + n<T>(1,2)*z[3];
    z[59]=z[3]*z[44];
    z[51]=z[58] + z[51] + z[59];
    z[51]=z[5]*z[51];
    z[44]=z[44]*z[55];
    z[54]=z[54] + z[48];
    z[54]=z[5]*z[54];
    z[48]= - n<T>(1,3)*z[48] + z[5];
    z[48]=n<T>(1,2)*z[48] - n<T>(1,3)*z[7];
    z[48]=z[7]*z[48];
    z[44]=z[48] + z[44] + z[54];
    z[44]=z[7]*z[44];
    z[48]=z[12] + z[9];
    z[45]= - n<T>(5,2)*z[6] + z[45];
    z[45]= - n<T>(2,9)*z[7] + n<T>(4,9)*z[4] + n<T>(7,18)*z[2] + n<T>(1,9)*z[45]
     - z[57]
    - n<T>(1,18)*z[48];
    z[45]=z[19]*z[45];
    z[48]=z[10] + z[4];
    z[48]=z[12] + z[2] - 3*z[5] + z[52] - 2*z[48];
    z[48]=z[15]*z[48];
    z[54]= - z[34] + z[42] + z[40];
    z[55]=z[33] - z[24];
    z[57]= - n<T>(10,3)*z[25] - n<T>(5,3)*z[23] - n<T>(1,6)*z[20];
    z[57]=i*z[57];
    z[56]= - z[3] + z[56];
    z[56]=z[13]*z[56];
    z[52]=z[2] - z[52];
    z[52]=z[16]*z[52];
    z[58]= - z[5] - z[12];
    z[58]=z[29]*z[58];

    r +=  - n<T>(4,3)*z[21] - n<T>(5,36)*z[22] + n<T>(5,3)*z[26] - n<T>(11,6)*z[31]
     + n<T>(3,2)*z[32]
     + z[36]
     + 4*z[37] - n<T>(1,6)*z[41]
     + n<T>(2,3)*z[43]
     + z[44]
       + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52]
       + n<T>(1,3)*z[53] + n<T>(1,2)*z[54] - n<T>(5,6)*z[55] + n<T>(7,3)*z[56] + z[57]
       + z[58];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1167(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1167(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
