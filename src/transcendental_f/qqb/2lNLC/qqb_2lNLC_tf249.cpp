#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf249(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[41];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[4];
    z[9]=e[1];
    z[10]=e[2];
    z[11]=e[8];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[19];
    z[16]=c[23];
    z[17]=c[25];
    z[18]=c[26];
    z[19]=c[28];
    z[20]=c[31];
    z[21]=e[3];
    z[22]=e[10];
    z[23]=f[4];
    z[24]=f[5];
    z[25]=f[11];
    z[26]=f[12];
    z[27]=f[16];
    z[28]=z[2] - z[6];
    z[28]= - n<T>(5,2)*z[8] - n<T>(2,3)*z[5] + z[3] - n<T>(1,4)*z[4] - n<T>(10,3)*z[28];
    z[28]=z[12]*z[28];
    z[29]=z[1]*i;
    z[30]=2*z[29];
    z[31]=z[30] - z[2];
    z[32]= - z[4] + z[31];
    z[32]=z[9]*z[32];
    z[33]= - z[5] + z[31];
    z[33]=z[11]*z[33];
    z[34]=2*z[6] - z[3];
    z[34]=z[22]*z[34];
    z[28]= - z[25] - z[24] + z[17] + z[14] + z[28] + z[32] + z[33] + 
    z[34];
    z[32]=z[29]*z[4];
    z[33]=5*z[32];
    z[34]=npow(z[4],2);
    z[35]=z[29] - z[4];
    z[36]= - 5*z[35] - n<T>(7,2)*z[6];
    z[36]=z[6]*z[36];
    z[36]=z[36] - 2*z[34] + z[33];
    z[37]=2*z[4];
    z[38]=n<T>(1,2)*z[6];
    z[39]=z[38] + z[37] + n<T>(1,2)*z[29];
    z[39]= - n<T>(1,6)*z[3] + n<T>(1,3)*z[39] + n<T>(1,2)*z[2];
    z[39]=z[3]*z[39];
    z[40]=z[2]*z[35];
    z[36]=z[39] + n<T>(1,3)*z[36] + z[40];
    z[36]=z[3]*z[36];
    z[39]=z[29]*z[37];
    z[40]= - z[4] + z[2];
    z[40]=z[5]*z[40];
    z[39]=z[40] - z[34] + z[39];
    z[40]=z[38] + z[29];
    z[40]=z[40]*z[6];
    z[29]=5*z[29];
    z[37]=z[37] - z[29];
    z[37]=n<T>(1,6)*z[2] + n<T>(1,3)*z[37] - z[6];
    z[37]=z[2]*z[37];
    z[37]=z[37] + z[40] + n<T>(1,3)*z[39];
    z[37]=z[5]*z[37];
    z[39]= - z[32] + n<T>(1,2)*z[34];
    z[29]= - 7*z[4] - z[29];
    z[29]=z[29]*z[38];
    z[29]=z[29] - z[39];
    z[29]=z[6]*z[29];
    z[31]=n<T>(1,2)*z[4] + z[31];
    z[31]=z[2]*z[31];
    z[31]=z[31] + z[34] - z[33];
    z[31]=z[40] + n<T>(1,3)*z[31];
    z[31]=z[2]*z[31];
    z[33]=z[38] + z[35];
    z[33]=z[6]*z[33];
    z[33]=z[33] + z[39];
    z[34]=3*z[6] - z[35];
    z[34]=n<T>(1,4)*z[34] - z[3];
    z[34]=z[8]*z[34];
    z[33]=n<T>(3,2)*z[33] + z[34];
    z[33]=z[8]*z[33];
    z[30]= - z[3]*z[30];
    z[30]=z[12] + z[32] + z[30];
    z[30]=z[7]*z[30];
    z[32]= - z[35] - 2*z[3];
    z[32]=z[10]*z[32];
    z[30]=z[30] + z[32] + z[20] + z[19];
    z[32]= - n<T>(4,3)*z[18] - n<T>(2,3)*z[16] + n<T>(5,36)*z[13];
    z[32]=i*z[32];
    z[34]=z[3] + z[8];
    z[34]=z[21]*z[34];

    r +=  - n<T>(1,18)*z[15] - n<T>(7,12)*z[23] - n<T>(1,4)*z[26] + n<T>(3,4)*z[27]
     + n<T>(1,3)*z[28]
     + n<T>(1,6)*z[29]
     + n<T>(2,3)*z[30]
     + z[31]
     + z[32]
     + z[33]
     +  z[34] + z[36] + z[37];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf249(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf249(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
