#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1133(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[27];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[19];
    z[11]=c[23];
    z[12]=c[25];
    z[13]=c[26];
    z[14]=c[28];
    z[15]=c[31];
    z[16]=f[3];
    z[17]=f[11];
    z[18]=f[13];
    z[19]=f[17];
    z[20]=2*z[5];
    z[21]=i*z[1];
    z[22]= - 2*z[21] + z[5];
    z[22]=z[22]*z[20];
    z[23]=n<T>(2,3)*z[4];
    z[24]=z[23] - n<T>(1,3)*z[21] - 4*z[5];
    z[24]=z[4]*z[24];
    z[25]= - n<T>(1,12)*z[9] + z[6];
    z[26]=z[20] + n<T>(1,6)*z[4];
    z[26]=z[3]*z[26];
    z[22]=z[26] + z[24] + n<T>(13,6)*z[25] + z[22];
    z[22]=z[3]*z[22];
    z[24]=5*z[21] - 4*z[4];
    z[23]=z[24]*z[23];
    z[24]= - z[21] + n<T>(1,2)*z[5];
    z[24]=z[24]*z[5];
    z[25]= - n<T>(13,2)*z[3] + 13*z[21] + 8*z[4];
    z[25]=z[3]*z[25];
    z[21]= - 4*z[3] - 22*z[21] - z[4];
    z[21]=n<T>(1,3)*z[21] + 3*z[2];
    z[21]=z[2]*z[21];
    z[21]=z[21] + n<T>(1,3)*z[25] + z[23] - 7*z[24] + n<T>(7,6)*z[6] - n<T>(11,18)*
    z[9] + n<T>(7,2)*z[8] + n<T>(2,3)*z[7];
    z[21]=z[2]*z[21];
    z[23]=4*z[7] - n<T>(1,3)*z[9];
    z[20]=z[4]*z[20];
    z[20]=z[20] + n<T>(2,3)*z[23] + 3*z[24];
    z[20]=z[4]*z[20];
    z[23]=2*z[13] + z[11];
    z[24]=7*z[8];
    z[25]= - n<T>(13,3)*z[6] - z[24] - n<T>(10,3)*z[7];
    z[25]=z[1]*z[25];
    z[23]=n<T>(22,3)*z[23] + z[25];
    z[23]=i*z[23];
    z[25]=z[12] - z[18];
    z[24]=z[24] - n<T>(5,12)*z[9];
    z[24]=n<T>(1,2)*z[24] + z[6];
    z[24]=z[5]*z[24];

    r += n<T>(11,18)*z[10] - n<T>(22,3)*z[14] - n<T>(4,3)*z[15] + n<T>(1,6)*z[16]
     + n<T>(3,2)*z[17]
     + 2*z[19]
     + z[20]
     + z[21]
     + z[22]
     + z[23]
     + z[24] - n<T>(11,3)*z[25];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1133(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1133(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
