#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1268(
  const std::array<std::complex<T>,111>& f
) {
    return f[2];
}

template std::complex<double> qqb_2lNLC_tf1268(
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1268(
  const std::array<std::complex<dd_real>,111>& f
);
#endif
