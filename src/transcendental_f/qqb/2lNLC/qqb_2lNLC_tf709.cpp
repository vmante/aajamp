#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf709(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[69];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=d[12];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=c[3];
    z[21]=d[9];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[3];
    z[31]=e[6];
    z[32]=e[13];
    z[33]=e[7];
    z[34]=e[5];
    z[35]=e[14];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[31];
    z[44]=f[32];
    z[45]=f[36];
    z[46]=f[39];
    z[47]=f[51];
    z[48]=f[55];
    z[49]=n<T>(1,1531)*z[5];
    z[50]=z[1]*i;
    z[51]=885*z[50] + n<T>(2177,2)*z[5];
    z[51]=z[51]*z[49];
    z[52]=n<T>(1,2)*z[6];
    z[53]= - z[5] - z[52];
    z[53]=z[53]*z[52];
    z[54]=n<T>(1,2)*z[2];
    z[55]=n<T>(11491,2)*z[50] - 6511*z[5];
    z[55]=n<T>(11639,6124)*z[3] + n<T>(1,1531)*z[55] + z[54];
    z[55]=z[3]*z[55];
    z[56]=1282*z[50] - n<T>(4095,2)*z[5];
    z[56]= - n<T>(1,3)*z[7] + n<T>(8429,3062)*z[3] + n<T>(1,1531)*z[56] - z[54];
    z[56]=z[7]*z[56];
    z[57]= - z[50] - z[5];
    z[57]=n<T>(1,2)*z[57] + z[6];
    z[57]=z[2]*z[57];
    z[58]= - z[13]*z[50];
    z[51]=z[34] + z[58] + z[56] + z[55] + z[57] + z[51] + z[53];
    z[51]=z[7]*z[51];
    z[53]=n<T>(1,2)*z[5];
    z[55]=z[53] - z[50];
    z[56]=z[55]*z[5];
    z[57]= - n<T>(1383,2)*z[6] + 4149*z[50] + n<T>(5777,6)*z[5];
    z[57]=z[6]*z[57];
    z[57]=n<T>(5777,3)*z[56] + z[57];
    z[57]=z[6]*z[57];
    z[58]=11639*z[3];
    z[59]= - 8281*z[5] - z[58];
    z[59]=z[50]*z[59];
    z[59]= - 8281*z[20] + z[59];
    z[59]=z[8]*z[59];
    z[60]=z[50] - z[5];
    z[58]=8281*z[60] - z[58];
    z[58]=z[16]*z[58];
    z[57]=z[58] + z[57] + z[59];
    z[58]=n<T>(7507,2)*z[50] - 1457*z[5];
    z[58]= - z[54] + n<T>(1,1531)*z[58] - z[52];
    z[58]=z[2]*z[58];
    z[59]= - 6511*z[50] + n<T>(11343,2)*z[5];
    z[49]=z[59]*z[49];
    z[59]=n<T>(1457,3062)*z[6] - n<T>(1457,1531)*z[50] + z[53];
    z[59]=z[6]*z[59];
    z[61]=8577*z[50] + 11639*z[5];
    z[61]=n<T>(1,2)*z[61] - 1457*z[6];
    z[61]= - n<T>(22484,3)*z[3] + n<T>(1,2)*z[61] + 1494*z[2];
    z[61]=z[3]*z[61];
    z[49]=n<T>(1,1531)*z[61] + z[58] + z[49] + z[59];
    z[49]=z[3]*z[49];
    z[58]=z[9] - z[6];
    z[58]=n<T>(1,3)*z[10] + n<T>(74,1531)*z[50] + z[53] - n<T>(1383,3062)*z[58];
    z[58]=z[10]*z[58];
    z[52]=z[52] - z[50];
    z[59]=z[6]*z[52];
    z[61]=z[50] - z[6];
    z[62]=z[61] + n<T>(1,2)*z[9];
    z[63]=z[9]*z[62];
    z[59]=z[59] + z[63];
    z[63]=z[5]*z[50];
    z[64]=z[54] - z[5];
    z[65]=z[2]*z[64];
    z[58]=z[58] + z[65] + z[63] - n<T>(1383,1531)*z[59];
    z[58]=z[10]*z[58];
    z[59]=8395*z[11];
    z[63]= - 9977*z[6] + z[59];
    z[63]=z[50]*z[63];
    z[63]= - 9977*z[20] + z[63];
    z[63]=z[12]*z[63];
    z[59]=9977*z[61] + z[59];
    z[59]=z[18]*z[59];
    z[59]=z[63] + z[59];
    z[63]= - 5731*z[50] - 17581*z[5];
    z[53]=z[63]*z[53];
    z[63]= - n<T>(51263,4)*z[6] + n<T>(43409,2)*z[50] + 13631*z[5];
    z[63]=z[6]*z[63];
    z[65]= - 49829*z[50] - n<T>(8395,2)*z[5];
    z[65]=n<T>(49829,4)*z[2] + n<T>(1,2)*z[65] - 4852*z[6];
    z[65]=z[2]*z[65];
    z[53]=z[65] + z[53] + z[63];
    z[53]=z[2]*z[53];
    z[63]=6841*z[9];
    z[65]=z[63] + 2248*z[11];
    z[65]=z[31]*z[65];
    z[53]=z[53] + z[65];
    z[65]=2*z[50];
    z[66]=z[65] - z[6];
    z[66]=z[6]*z[66];
    z[67]=n<T>(4507,3)*z[50] - 1173*z[5];
    z[67]=n<T>(1,2)*z[67] - n<T>(494,3)*z[6];
    z[67]=n<T>(6841,6)*z[9] - 2204*z[7] + 5*z[67] + n<T>(7211,2)*z[3];
    z[67]=z[9]*z[67];
    z[66]=z[67] - 5865*z[56] - n<T>(1346,3)*z[66];
    z[67]=npow(z[3],2);
    z[60]= - n<T>(1,2)*z[7] - z[60];
    z[60]=z[7]*z[60];
    z[60]=n<T>(5865,1531)*z[60] + z[67] + n<T>(1,1531)*z[66];
    z[60]=z[9]*z[60];
    z[52]= - z[5] + z[52];
    z[66]=3*z[6];
    z[52]=z[52]*z[66];
    z[52]=z[56] + z[52];
    z[56]=z[65] - z[64];
    z[56]=z[2]*z[56];
    z[64]=z[5] + z[66];
    z[64]=n<T>(1,4)*z[64] - z[2];
    z[64]=z[4]*z[64];
    z[52]=z[64] + n<T>(1,2)*z[52] + z[56];
    z[56]=npow(z[7],2);
    z[64]=npow(z[10],2);
    z[52]= - z[64] + z[56] + n<T>(3671,1531)*z[52];
    z[52]=z[4]*z[52];
    z[56]= - n<T>(8429,2)*z[7] + 1457*z[61] - n<T>(5663,2)*z[3];
    z[56]=z[19]*z[56];
    z[61]= - 5902*z[3] - 4371*z[9];
    z[61]=z[30]*z[61];
    z[56]=z[56] + z[61];
    z[61]=z[62]*z[63];
    z[62]= - 6841*z[50] + n<T>(23659,4)*z[6];
    z[62]=z[6]*z[62];
    z[61]=z[62] + z[61];
    z[50]=n<T>(757,3062)*z[11] + z[10] - n<T>(8395,9186)*z[6] + n<T>(791,9186)*z[50]
    - z[5];
    z[50]=z[11]*z[50];
    z[50]= - z[33] + n<T>(1,4593)*z[61] + n<T>(1,2)*z[50];
    z[50]=z[11]*z[50];
    z[54]=z[54] + z[55];
    z[54]=z[15]*z[54];
    z[55]=z[38] - z[26];
    z[61]=z[46] - z[43];
    z[62]=n<T>(38941,9)*z[5] + 6509*z[6];
    z[62]=n<T>(1679,3)*z[11] - n<T>(3671,12)*z[4] + n<T>(4445,3)*z[10] + n<T>(62873,18)*
    z[9] - n<T>(3341,12)*z[7] + n<T>(1383,4)*z[3] + n<T>(1,4)*z[62] - n<T>(25657,9)*z[2]
   ;
    z[62]= - n<T>(74,4593)*z[21] + n<T>(1,1531)*z[62];
    z[62]=z[20]*z[62];
    z[63]=n<T>(49829,4593)*z[27] + n<T>(49829,9186)*z[25] - n<T>(8705,27558)*z[22];
    z[63]=i*z[63];
    z[64]=z[65] - n<T>(1,3)*z[5];
    z[64]=z[64]*npow(z[5],2);
    z[65]=z[65] - z[2];
    z[66]=z[4] - z[65];
    z[66]=z[14]*z[66];
    z[65]=z[6] - z[65];
    z[65]=z[17]*z[65];
    z[67]=z[10] + z[21];
    z[67]=z[32]*z[67];
    z[68]= - z[7] - z[21];
    z[68]=z[35]*z[68];

    r += n<T>(17632,4593)*z[23] + n<T>(49829,110232)*z[24] - n<T>(49829,9186)*z[28]
       - n<T>(159701,9186)*z[29] + n<T>(3671,6124)*z[36] - n<T>(4095,3062)*z[37]
     +  n<T>(5777,18372)*z[39] + n<T>(5865,3062)*z[40] - n<T>(2204,1531)*z[41] + 
      n<T>(11013,6124)*z[42] + n<T>(1,2)*z[44] + n<T>(2470,4593)*z[45] - z[47] - 
      z[48] + z[49] + z[50] + z[51] + z[52] + n<T>(1,4593)*z[53] + n<T>(8395,4593)*z[54]
     + n<T>(49829,18372)*z[55]
     + n<T>(1,1531)*z[56]
     + n<T>(1,3062)*
      z[57] + z[58] + n<T>(1,9186)*z[59] + z[60] - n<T>(1383,3062)*z[61] + 
      z[62] + z[63] + z[64] + n<T>(9704,4593)*z[65] + n<T>(3671,1531)*z[66] + 
      z[67] + z[68];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf709(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf709(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
