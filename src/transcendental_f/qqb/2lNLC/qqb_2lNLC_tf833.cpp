#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf833(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[72];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[5];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=d[9];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[51];
    z[52]=f[55];
    z[53]=f[58];
    z[54]= - z[12] + n<T>(1,2)*z[11];
    z[55]=2*z[6];
    z[56]=n<T>(1,4)*z[8];
    z[57]=n<T>(3,4)*z[4] - z[56] - z[55] - n<T>(1,4)*z[10] - z[54];
    z[57]=z[4]*z[57];
    z[58]=12619*z[13];
    z[59]=n<T>(20207,4)*z[10];
    z[60]=z[58] + z[59];
    z[61]=n<T>(258095,16)*z[6] + z[60];
    z[61]=z[6]*z[61];
    z[59]=n<T>(71501,4)*z[7] + n<T>(425933,12)*z[5] - n<T>(1141529,12)*z[2] + n<T>(230877,4)*z[4]
     - n<T>(76807,4)*z[6]
     - z[59]
     + 4631*z[3] - n<T>(257,4)*z[14] - 
   14150*z[13];
    z[59]=z[7]*z[59];
    z[59]=z[61] + z[59];
    z[61]= - n<T>(1042459,18372)*z[8] + n<T>(122013,3062)*z[6] - n<T>(372793,4593)*
    z[3] + z[10];
    z[61]=z[61]*z[56];
    z[62]= - z[18] + n<T>(1,2)*z[22];
    z[63]=npow(z[10],2);
    z[64]= - 79869*z[14] + n<T>(85993,2)*z[13];
    z[64]=z[13]*z[64];
    z[65]=87343*z[9] - n<T>(363151,8)*z[3];
    z[65]=z[3]*z[65];
    z[66]=n<T>(5,4)*z[8];
    z[67]=n<T>(454917,6124)*z[2] - n<T>(469409,9186)*z[4] + z[66] - n<T>(35365,6124)
   *z[3] + z[6];
    z[67]=z[2]*z[67];
    z[68]=97061*z[9] + n<T>(437095,2)*z[3];
    z[68]= - n<T>(200923,3062)*z[2] + n<T>(255373,9186)*z[4] + n<T>(434663,18372)*
    z[8] - n<T>(122013,6124)*z[6] + n<T>(1,4593)*z[68] - z[10];
    z[68]=n<T>(1,2)*z[68] - 20*z[5];
    z[68]=z[5]*z[68];
    z[57]=z[68] + z[67] + z[57] + z[61] - n<T>(13703,3062)*z[63] + n<T>(1,4593)*
    z[65] + n<T>(1,6124)*z[64] - n<T>(4631,1531)*z[21] + n<T>(241594,4593)*z[15]
     + 
   n<T>(113551,3062)*z[16] - n<T>(97061,9186)*z[17] + n<T>(279641,4593)*z[19] + n<T>(257,6124)*z[20]
     - z[62]
     + n<T>(1,1531)*z[59];
    z[57]=z[1]*z[57];
    z[59]=n<T>(165793,72)*z[25] - 461041*z[30] - n<T>(454917,2)*z[28];
    z[57]=n<T>(1,3062)*z[59] + z[57];
    z[57]=i*z[57];
    z[59]= - z[10] - n<T>(49,2)*z[8];
    z[59]=z[59]*z[56];
    z[61]=npow(z[6],2);
    z[64]=n<T>(1,2)*z[61];
    z[65]= - z[66] + z[13] + n<T>(9,4)*z[10];
    z[65]=n<T>(1,2)*z[65] - n<T>(1,3)*z[4];
    z[65]=z[4]*z[65];
    z[59]=z[65] + z[59] - z[64] + n<T>(47,8)*z[63] + n<T>(38237,13779)*z[23] - 
   n<T>(120797,4593)*z[15] + z[62];
    z[59]=z[4]*z[59];
    z[62]=n<T>(279641,3)*z[19];
    z[65]=n<T>(113551,2)*z[16];
    z[66]= - z[62] - z[65];
    z[67]=npow(z[3],2);
    z[66]= - n<T>(26179,8)*z[67] + n<T>(155905,12)*z[23] + n<T>(1,2)*z[66] - n<T>(120797,3)*z[15];
    z[68]=n<T>(7,8)*z[8] - n<T>(3,2)*z[3] + z[6];
    z[68]=z[8]*z[68];
    z[69]= - z[13] + n<T>(5,2)*z[10];
    z[70]=n<T>(469409,18372)*z[4] + 2*z[8] - z[69];
    z[70]=z[4]*z[70];
    z[71]=z[3] - z[10];
    z[71]= - n<T>(454917,12248)*z[2] + n<T>(5,4)*z[71] + n<T>(58102,4593)*z[4];
    z[71]=z[2]*z[71];
    z[64]=z[71] + z[70] + z[68] + n<T>(1,1531)*z[66] - z[64];
    z[64]=z[2]*z[64];
    z[60]= - n<T>(66043,8)*z[6] - z[60];
    z[60]=z[6]*z[60];
    z[66]=npow(z[13],2);
    z[62]= - n<T>(257,2)*z[20] - z[62];
    z[60]=z[60] - n<T>(14083,8)*z[63] + n<T>(4631,2)*z[67] + n<T>(73745,8)*z[66]
     + 
   n<T>(1509773,72)*z[23] + n<T>(1,2)*z[62] + 4631*z[21];
    z[62]=5*z[10];
    z[68]=n<T>(279641,9186)*z[2] - n<T>(7,2)*z[8] + z[62] - 7*z[24] + 3*z[3];
    z[68]=z[2]*z[68];
    z[62]= - n<T>(471863,18372)*z[5] - n<T>(160907,4593)*z[2] + n<T>(240063,3062)*
    z[4] + 5*z[8] - z[62] - z[13] + z[3];
    z[62]=z[5]*z[62];
    z[56]= - n<T>(71501,18372)*z[7] - n<T>(352445,18372)*z[5] + n<T>(904061,18372)*
    z[2] - n<T>(255373,6124)*z[4] - z[56] + n<T>(76807,6124)*z[6] + n<T>(20207,6124)
   *z[10] - n<T>(7693,1531)*z[3] + z[24] + n<T>(72167,6124)*z[13];
    z[56]=z[7]*z[56];
    z[56]=z[56] + z[68] + z[62];
    z[62]=n<T>(1,2)*z[8];
    z[68]= - z[6] + z[62];
    z[68]=z[8]*z[68];
    z[62]= - n<T>(215567,12248)*z[4] - z[62] + n<T>(5,2)*z[24] - 2*z[13];
    z[62]=z[4]*z[62];
    z[56]=z[62] + n<T>(1,1531)*z[60] + z[68] + n<T>(1,2)*z[56];
    z[56]=z[7]*z[56];
    z[60]= - n<T>(305675,24)*z[23] + 6495*z[34] + n<T>(11173,4)*z[33];
    z[58]=6495*z[6] + n<T>(1835,4)*z[10] + z[58] - n<T>(1987,2)*z[3];
    z[58]=z[6]*z[58];
    z[58]=n<T>(1,3062)*z[58] - n<T>(4289,12248)*z[63] + n<T>(1,1531)*z[60] - 3*z[67]
   ;
    z[58]=z[6]*z[58];
    z[60]=n<T>(95237,13779)*z[23] + 23*z[37] + n<T>(418723,4593)*z[21];
    z[60]=n<T>(158453,12248)*z[61] + n<T>(5,4)*z[63] + n<T>(1,2)*z[60] - n<T>(151645,4593)*z[67];
    z[62]= - n<T>(13150,4593)*z[3] - n<T>(1,8)*z[10];
    z[62]=n<T>(29,12)*z[8] + 7*z[62] + n<T>(146509,24496)*z[6];
    z[62]=z[8]*z[62];
    z[60]=n<T>(1,2)*z[60] + z[62];
    z[60]=z[8]*z[60];
    z[62]= - n<T>(78157,3)*z[67] - n<T>(3349991,72)*z[23] + n<T>(97061,3)*z[17] - 
    z[65];
    z[65]=n<T>(1391527,12)*z[8] + n<T>(400351,3)*z[3] - n<T>(122013,2)*z[6];
    z[65]=z[8]*z[65];
    z[61]=n<T>(1,3062)*z[65] - n<T>(101513,12248)*z[61] + n<T>(1,1531)*z[62] - n<T>(1,2)
   *z[63];
    z[62]= - 5*z[24] + z[10];
    z[55]= - n<T>(301303,36744)*z[4] + n<T>(1,2)*z[62] + z[55];
    z[55]=z[4]*z[55];
    z[62]=n<T>(113551,12248)*z[2] - n<T>(116204,4593)*z[4] - n<T>(3,2)*z[8] - z[6]
    + n<T>(10869,6124)*z[3] + n<T>(7,2)*z[24] + z[69];
    z[62]=z[2]*z[62];
    z[65]=n<T>(174791,3062)*z[2] - n<T>(209443,18372)*z[4] - n<T>(471407,36744)*z[8]
    + n<T>(109765,12248)*z[6] - n<T>(336505,9186)*z[3] - z[24] + n<T>(3,2)*z[13];
    z[65]=n<T>(1,2)*z[65] + n<T>(10,3)*z[5];
    z[65]=z[5]*z[65];
    z[55]=z[65] + z[62] + n<T>(1,2)*z[61] + z[55];
    z[55]=z[5]*z[55];
    z[61]=n<T>(1555,4)*z[33] + n<T>(4597,3)*z[17];
    z[61]=n<T>(112010,3)*z[67] + n<T>(181733,24)*z[23] + 19*z[61] + n<T>(123973,3)*
    z[21];
    z[61]=z[3]*z[61];
    z[62]= - 9*z[36] - z[35];
    z[62]= - n<T>(29,12)*z[63] + n<T>(1,2)*z[62] - n<T>(4108,1531)*z[23];
    z[62]=z[10]*z[62];
    z[63]=z[29] - z[42];
    z[65]=n<T>(29,2)*z[37] - 13*z[36];
    z[65]=z[24]*z[65];
    z[65]=z[65] + z[39];
    z[67]= - n<T>(14083,1531)*z[48] + n<T>(66043,1531)*z[49] - n<T>(22661,1531)*
    z[50] + 47*z[51] - 3*z[53] + 49*z[52];
    z[54]= - n<T>(16993,18372)*z[24] + n<T>(97061,9186)*z[9] - n<T>(257,6124)*z[14]
    - z[54];
    z[54]=z[23]*z[54];
    z[66]= - n<T>(153871,36744)*z[66] - n<T>(14749,3062)*z[23] - n<T>(79869,6124)*
    z[20] + n<T>(13,2)*z[35] + n<T>(15681,1531)*z[34];
    z[66]=z[13]*z[66];

    r +=  - n<T>(156151,18372)*z[26] - n<T>(139391,24496)*z[27] + n<T>(461041,6124)
      *z[31] + n<T>(194749,4593)*z[32] - z[38] - n<T>(255373,36744)*z[40] + 
      n<T>(1391527,73488)*z[41] - n<T>(425933,36744)*z[43] + n<T>(101513,24496)*
      z[44] + n<T>(182949,24496)*z[45] - n<T>(230877,12248)*z[46] - n<T>(1,3)*z[47]
       + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + n<T>(1,1531)*z[61]
     + z[62]
     + n<T>(454917,12248)*z[63]
     + z[64]
     + n<T>(1,2)*z[65]
       + z[66] + n<T>(1,8)*z[67];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf833(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf833(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
