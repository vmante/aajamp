#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf216(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[62];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[16];
    z[11]=d[5];
    z[12]=d[6];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[4];
    z[17]=e[8];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[3];
    z[28]=e[10];
    z[29]=e[6];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[18];
    z[39]=f[31];
    z[40]=f[36];
    z[41]=f[37];
    z[42]=f[39];
    z[43]=n<T>(1,2)*z[2];
    z[44]=n<T>(1,2)*z[9];
    z[45]=z[1]*i;
    z[46]= - n<T>(7,6)*z[3] + z[43] + n<T>(1,6)*z[5] - n<T>(1,3)*z[45] - z[44];
    z[46]=z[3]*z[46];
    z[47]=4*z[5];
    z[48]= - z[45] + z[47];
    z[49]=n<T>(1,3)*z[5];
    z[48]=z[48]*z[49];
    z[50]=npow(z[9],2);
    z[51]=z[45] - z[5];
    z[52]=z[2]*z[51];
    z[46]=z[46] + z[52] + n<T>(1,2)*z[50] + z[48];
    z[46]=z[3]*z[46];
    z[48]= - 4*z[45] - z[5];
    z[48]=z[48]*z[49];
    z[52]=n<T>(1,3)*z[2];
    z[53]=2*z[45];
    z[54]=z[53] + z[5];
    z[54]=2*z[54] - z[2];
    z[54]=z[54]*z[52];
    z[55]=z[2] - z[5];
    z[56]= - z[44] - n<T>(2,3)*z[55];
    z[56]=z[6]*z[56];
    z[57]=z[45] + n<T>(3,2)*z[9];
    z[57]=z[9]*z[57];
    z[48]=z[56] + z[54] + z[57] + z[48];
    z[48]=z[6]*z[48];
    z[54]= - 2*z[55];
    z[54]=z[45]*z[54];
    z[56]= - z[53] + z[44];
    z[56]=z[9]*z[56];
    z[57]=n<T>(1,2)*z[45];
    z[55]= - n<T>(1,2)*z[4] + z[57] + z[55];
    z[55]=z[4]*z[55];
    z[54]=z[55] + z[56] + z[54];
    z[54]=z[4]*z[54];
    z[55]= - z[57] - n<T>(1,3)*z[9];
    z[55]=z[55]*z[50];
    z[56]=z[44] + z[45];
    z[57]=z[56]*z[9];
    z[44]= - z[5]*z[44];
    z[44]=z[57] + z[44];
    z[44]=z[5]*z[44];
    z[44]= - z[41] - z[42] - 7*z[55] - 3*z[44] + z[40] + z[39];
    z[47]= - z[47] - z[3];
    z[47]=z[45]*z[47];
    z[47]= - 4*z[18] + z[47];
    z[47]=z[8]*z[47];
    z[53]=z[53] - z[2];
    z[55]= - z[5] + z[53];
    z[55]=z[14]*z[55];
    z[58]=4*z[51] - z[3];
    z[58]=z[15]*z[58];
    z[47]=z[58] + z[47] + z[55];
    z[55]=n<T>(1,2)*z[5];
    z[43]=z[43] - z[45] + z[55];
    z[43]=z[43]*z[52];
    z[52]= - 5*z[45] + z[5];
    z[49]=z[52]*z[49];
    z[43]=z[43] - z[50] + z[49];
    z[43]=z[2]*z[43];
    z[49]=3*z[9];
    z[50]= - z[56]*z[49];
    z[52]= - n<T>(7,6)*z[5] + n<T>(7,3)*z[45] + z[49];
    z[52]=z[5]*z[52];
    z[50]=z[50] + z[52];
    z[49]= - z[49] + n<T>(19,3)*z[51];
    z[49]=n<T>(1,4)*z[49] + n<T>(7,3)*z[3];
    z[49]=z[7]*z[49];
    z[51]=n<T>(1,2)*z[3] + z[51];
    z[51]=z[3]*z[51];
    z[49]=z[49] + n<T>(1,2)*z[50] + n<T>(1,3)*z[51];
    z[49]=z[7]*z[49];
    z[50]=z[45] + z[9];
    z[51]=n<T>(1,2)*z[6] - z[50];
    z[51]=z[6]*z[51];
    z[52]= - z[6] + z[50];
    z[52]=z[11]*z[52];
    z[51]=n<T>(1,2)*z[52] - n<T>(1,3)*z[18] + z[57] + z[51];
    z[51]=z[11]*z[51];
    z[52]=z[4] + 2*z[9];
    z[56]=z[45]*z[52];
    z[56]=z[18] + z[56];
    z[56]=z[10]*z[56];
    z[45]=z[45] - z[52];
    z[45]=z[16]*z[45];
    z[52]=z[6] - z[53];
    z[52]=z[17]*z[52];
    z[52]=z[52] - z[38] + z[35] - z[20];
    z[57]=z[34] - z[23];
    z[58]=z[37] + z[36];
    z[53]= - z[4] + z[53];
    z[53]=z[13]*z[53];
    z[53]=z[53] - z[30];
    z[50]= - z[50] + z[4];
    z[50]=z[9]*z[50];
    z[50]= - z[29] + z[50];
    z[50]=z[12]*z[50];
    z[59]= - n<T>(10,3)*z[24] + n<T>(1,3)*z[22] - n<T>(7,36)*z[19];
    z[59]=i*z[59];
    z[55]= - n<T>(1,9)*z[6] + n<T>(5,3)*z[3] - n<T>(29,9)*z[2] + z[9] - z[55];
    z[55]=n<T>(2,3)*z[4] + n<T>(1,2)*z[55] + n<T>(10,9)*z[7];
    z[55]=z[18]*z[55];
    z[60]= - z[9] + z[3];
    z[60]=z[27]*z[60];
    z[61]= - z[3] - z[7];
    z[61]=z[28]*z[61];

    r += n<T>(37,36)*z[21] + n<T>(5,3)*z[25] - n<T>(25,6)*z[26] + z[31] - z[32] - 
      n<T>(19,12)*z[33] + z[43] - n<T>(1,2)*z[44] + z[45] + z[46] + n<T>(1,3)*z[47]
       + z[48] + z[49] + z[50] + z[51] + n<T>(2,3)*z[52] + 2*z[53] + z[54]
       + z[55] + z[56] + n<T>(1,6)*z[57] - n<T>(3,4)*z[58] + z[59] + z[60] + n<T>(7,3)*z[61];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf216(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf216(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
