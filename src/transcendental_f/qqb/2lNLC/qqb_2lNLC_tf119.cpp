#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf119(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[75];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[15];
    z[12]=d[16];
    z[13]=d[4];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[3];
    z[33]=e[6];
    z[34]=e[13];
    z[35]=e[14];
    z[36]=f[0];
    z[37]=f[1];
    z[38]=f[3];
    z[39]=f[4];
    z[40]=f[5];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[16];
    z[44]=f[18];
    z[45]=f[23];
    z[46]=f[31];
    z[47]=f[36];
    z[48]=f[39];
    z[49]=f[51];
    z[50]=f[55];
    z[51]=f[58];
    z[52]=n<T>(1,2)*z[9];
    z[53]=z[1]*i;
    z[54]=z[52] + z[53];
    z[55]=z[2] - z[54];
    z[55]=z[9]*z[55];
    z[56]=n<T>(1,2)*z[10];
    z[57]=z[53] - z[5];
    z[58]=n<T>(1,3)*z[10] + z[57];
    z[58]=z[58]*z[56];
    z[59]=z[57]*z[2];
    z[60]=z[53] - z[8];
    z[61]= - z[8]*z[60];
    z[62]=npow(z[5],2);
    z[63]= - z[8] + z[5];
    z[63]=z[3]*z[63];
    z[64]=n<T>(1,2)*z[4];
    z[65]= - z[8] + z[64];
    z[65]=z[4]*z[65];
    z[55]=z[58] + z[65] + z[63] + z[55] + z[59] + z[61] + n<T>(1,2)*z[62];
    z[55]=z[55]*z[56];
    z[56]=n<T>(295,4)*z[3];
    z[58]= - 41*z[5] + z[56];
    z[58]=z[53]*z[58];
    z[58]= - 41*z[23] + z[58];
    z[58]=z[11]*z[58];
    z[61]=n<T>(41,2)*z[7];
    z[62]= - n<T>(46,3)*z[8] + z[61];
    z[62]=z[53]*z[62];
    z[62]= - n<T>(46,3)*z[23] + z[62];
    z[62]=z[14]*z[62];
    z[56]=41*z[57] + z[56];
    z[56]=z[17]*z[56];
    z[61]=n<T>(46,3)*z[60] + z[61];
    z[61]=z[21]*z[61];
    z[56]=z[58] + z[62] + z[56] + z[61];
    z[58]=3*z[8];
    z[61]=n<T>(47,3)*z[5] + n<T>(73,3)*z[53] - z[58];
    z[61]=z[5]*z[61];
    z[62]=n<T>(1,2)*z[5];
    z[63]= - n<T>(5,72)*z[2] + z[62] - n<T>(1,9)*z[53] - n<T>(1,4)*z[8];
    z[63]=z[2]*z[63];
    z[65]= - 5*z[53] + 7*z[8];
    z[65]=z[8]*z[65];
    z[66]= - 13*z[53] + z[8];
    z[66]=n<T>(2,3)*z[66] + n<T>(41,4)*z[5];
    z[66]=n<T>(551,18)*z[9] + n<T>(1,3)*z[66] - n<T>(3,4)*z[2];
    z[66]=z[9]*z[66];
    z[67]=n<T>(3983,6)*z[3] - 115*z[9] + n<T>(13,2)*z[2] - 493*z[53] - n<T>(797,2)*
    z[5];
    z[67]=z[3]*z[67];
    z[61]=n<T>(1,36)*z[67] + z[66] + z[63] + n<T>(1,9)*z[65] + n<T>(1,4)*z[61];
    z[61]=z[3]*z[61];
    z[63]=z[62] - z[53];
    z[63]=z[5]*z[63];
    z[65]=n<T>(1,2)*z[2];
    z[66]=n<T>(4997,24)*z[5] - n<T>(653,24)*z[53] - 181*z[8];
    z[66]=n<T>(1423,54)*z[13] - n<T>(133,9)*z[3] - n<T>(7,24)*z[9] + n<T>(1,3)*z[66]
     - 
    z[65];
    z[67]=n<T>(1,2)*z[13];
    z[66]=z[66]*z[67];
    z[68]= - z[8] + 2*z[53];
    z[69]=z[8]*z[68];
    z[70]= - 13*z[57] + n<T>(17,6)*z[9];
    z[70]=z[9]*z[70];
    z[71]=npow(z[3],2);
    z[63]=z[66] - n<T>(173,6)*z[71] + n<T>(1,8)*z[70] - n<T>(43,9)*z[69] - n<T>(13,8)*
    z[63];
    z[63]=z[13]*z[63];
    z[66]=n<T>(7,2)*z[9];
    z[54]= - z[54]*z[66];
    z[69]= - z[67] - z[60];
    z[69]=z[13]*z[69];
    z[70]=z[53] - n<T>(1,2)*z[8];
    z[70]=z[8]*z[70];
    z[71]=z[2]*z[60];
    z[72]=n<T>(8,9)*z[6] - n<T>(401,36)*z[13] - n<T>(7,4)*z[9] - 11*z[53] + 15*z[8];
    z[72]=z[6]*z[72];
    z[54]=z[72] + n<T>(401,18)*z[69] + z[54] + n<T>(397,18)*z[70] + z[71];
    z[54]=z[6]*z[54];
    z[67]= - z[67] - z[57];
    z[67]=z[13]*z[67];
    z[69]= - 35*z[53] + n<T>(179,6)*z[8];
    z[69]=n<T>(1,6)*z[69] + z[5];
    z[69]=z[8]*z[69];
    z[70]= - n<T>(1,2)*z[6] - z[53] + z[2];
    z[70]=z[6]*z[70];
    z[71]=n<T>(277,108)*z[7] - n<T>(19,3)*z[8] - z[62];
    z[71]=z[7]*z[71];
    z[59]=z[71] + z[70] + z[67] + z[59] + z[69];
    z[59]=z[7]*z[59];
    z[67]=z[5] - z[2];
    z[67]=z[53]*z[67];
    z[69]= - 3*z[53] - z[8];
    z[69]=z[9]*z[69];
    z[70]=npow(z[13],2);
    z[67]=n<T>(1,2)*z[70] + z[69] + z[67];
    z[69]=4*z[6];
    z[66]= - z[69] + z[66] + n<T>(1,2)*z[53] + z[8];
    z[66]=z[6]*z[66];
    z[70]=n<T>(1,4)*z[4];
    z[57]=z[2] + z[57];
    z[57]= - z[70] - n<T>(3,4)*z[6] + n<T>(1,4)*z[57] + z[9];
    z[57]=z[4]*z[57];
    z[57]=z[57] + n<T>(1,2)*z[67] + z[66];
    z[57]=z[4]*z[57];
    z[66]=178*z[8];
    z[67]= - n<T>(739,2)*z[53] + z[66];
    z[67]=z[8]*z[67];
    z[71]=n<T>(739,2)*z[5] - n<T>(1559,2)*z[53] - 329*z[8];
    z[71]=z[71]*z[62];
    z[72]= - n<T>(1451,8)*z[2] + n<T>(739,8)*z[5] + n<T>(1451,4)*z[53] + 89*z[8];
    z[72]=z[2]*z[72];
    z[67]=z[72] + z[67] + z[71];
    z[67]=z[2]*z[67];
    z[66]=z[68]*z[66];
    z[71]=n<T>(329,4)*z[8] - 89*z[5];
    z[71]=z[5]*z[71];
    z[66]=z[66] + z[71];
    z[66]=z[5]*z[66];
    z[66]=z[66] + z[67];
    z[67]=z[4]*z[53];
    z[67]=z[67] + z[23];
    z[67]=z[12]*z[67];
    z[71]=z[53] - z[4];
    z[71]=z[18]*z[71];
    z[67]= - z[36] + z[67] + z[71];
    z[71]=323*z[3] + n<T>(335,3)*z[13];
    z[71]=z[32]*z[71];
    z[72]=179*z[13] + n<T>(377,3)*z[7];
    z[72]=z[33]*z[72];
    z[71]= - z[44] + z[71] + z[72];
    z[58]=n<T>(5,18)*z[2] + z[5] - n<T>(41,9)*z[53] + z[58];
    z[58]=z[2]*z[58];
    z[72]= - n<T>(161,12)*z[5] + n<T>(161,6)*z[53] - z[8];
    z[72]=z[5]*z[72];
    z[58]=z[72] + z[58];
    z[72]= - n<T>(11,9)*z[2] - n<T>(4183,36)*z[5] + n<T>(1421,12)*z[53] - z[8];
    z[72]=n<T>(1,2)*z[72] + n<T>(1,9)*z[9];
    z[72]=z[9]*z[72];
    z[58]=n<T>(1,2)*z[58] + z[72];
    z[58]=z[58]*z[52];
    z[72]=469*z[2] - 757*z[8] - n<T>(3413,2)*z[5];
    z[72]=n<T>(1,3)*z[72] + 1997*z[9];
    z[72]=n<T>(1,9)*z[72] + 313*z[3];
    z[72]=n<T>(212,9)*z[6] + n<T>(1,8)*z[72] - n<T>(253,9)*z[13];
    z[70]=n<T>(485,216)*z[10] + n<T>(263,54)*z[7] + n<T>(1,3)*z[72] - z[70];
    z[70]=z[23]*z[70];
    z[65]=z[65] - z[53];
    z[64]= - z[64] - z[65];
    z[64]=z[15]*z[64];
    z[62]= - z[62] - z[65];
    z[62]=z[16]*z[62];
    z[65]=z[38] - z[37];
    z[72]=z[40] - z[28];
    z[73]=z[50] + z[49];
    z[68]= - z[2] + z[68];
    z[68]=z[20]*z[68];
    z[68]=z[68] - z[41];
    z[74]= - n<T>(1505,54)*z[29] - n<T>(1451,108)*z[27] + n<T>(607,108)*z[24];
    z[74]=i*z[74];
    z[53]= - z[53] - 17*z[8];
    z[53]= - 28*z[3] - n<T>(541,18)*z[9] + n<T>(1,18)*z[53] + z[2];
    z[53]=z[22]*z[53];
    z[60]= - 10*z[6] + z[60];
    z[60]=n<T>(2,9)*z[60] - n<T>(11,2)*z[7];
    z[60]=z[19]*z[60];
    z[69]=z[69] + n<T>(9,2)*z[10];
    z[69]=z[34]*z[69];
    z[52]= - z[52] - z[10];
    z[52]=z[35]*z[52];

    r +=  - n<T>(473,72)*z[25] - n<T>(1127,1296)*z[26] + n<T>(1505,108)*z[30] - 
      n<T>(26509,432)*z[31] - n<T>(4283,144)*z[39] - n<T>(5069,144)*z[42] + n<T>(49,48)
      *z[43] - n<T>(5,36)*z[45] + n<T>(136,9)*z[46] + n<T>(57,4)*z[47] - n<T>(401,36)*
      z[48] - n<T>(7,4)*z[51] + z[52] + z[53] + z[54] + z[55] + n<T>(1,3)*z[56]
       + z[57] + z[58] + z[59] + z[60] + z[61] + n<T>(739,54)*z[62] + z[63]
       + z[64] - n<T>(1,4)*z[65] + n<T>(1,27)*z[66] + n<T>(1,2)*z[67] + n<T>(178,27)*
      z[68] + z[69] + z[70] + n<T>(1,6)*z[71] - n<T>(1451,216)*z[72] - n<T>(5,2)*
      z[73] + z[74];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf119(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf119(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
