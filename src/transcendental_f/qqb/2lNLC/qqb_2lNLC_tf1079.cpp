#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1079(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[76];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[11];
    z[12]=d[4];
    z[13]=d[17];
    z[14]=d[9];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[7];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[6];
    z[33]=e[13];
    z[34]=e[14];
    z[35]=f[3];
    z[36]=f[4];
    z[37]=f[7];
    z[38]=f[11];
    z[39]=f[12];
    z[40]=f[13];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[23];
    z[44]=f[30];
    z[45]=f[31];
    z[46]=f[33];
    z[47]=f[36];
    z[48]=f[39];
    z[49]=f[50];
    z[50]=f[51];
    z[51]=f[55];
    z[52]=f[58];
    z[53]=5*z[5];
    z[54]=z[1]*i;
    z[55]= - n<T>(43,3)*z[54] + z[53];
    z[56]=z[12] + z[7];
    z[57]=4*z[6];
    z[55]= - n<T>(5,2)*z[9] + z[57] + n<T>(1,2)*z[55] + n<T>(14,3)*z[8] - n<T>(13,3)*
    z[56];
    z[55]=z[12]*z[55];
    z[58]= - z[54] + n<T>(1,2)*z[5];
    z[59]= - z[58]*z[53];
    z[60]=z[54] - z[8];
    z[61]=2*z[60] + z[6];
    z[57]=z[61]*z[57];
    z[61]=2*z[54];
    z[62]=z[61] - z[8];
    z[63]=z[62]*z[8];
    z[64]=z[54] - z[5];
    z[65]= - n<T>(1,2)*z[9] - z[64];
    z[65]=z[9]*z[65];
    z[66]=z[7]*z[60];
    z[55]=z[55] - n<T>(26,3)*z[66] + 5*z[65] + z[57] + z[59] + n<T>(1,3)*z[63];
    z[55]=z[12]*z[55];
    z[57]=z[14] + z[4];
    z[57]=z[54]*z[57];
    z[57]=z[23] + z[57];
    z[57]=z[11]*z[57];
    z[59]=z[2] - z[8] + z[64];
    z[59]= - 5*z[7] - 7*z[6] + 2*z[59];
    z[59]=z[18]*z[59];
    z[65]=z[54] - z[4];
    z[66]= - z[14] + z[65];
    z[66]=z[22]*z[66];
    z[67]= - z[6] - z[14];
    z[67]=z[33]*z[67];
    z[68]= - z[9] - z[14];
    z[68]=z[34]*z[68];
    z[57]=z[66] + z[59] + z[57] + z[67] + z[68] + z[49] + z[45];
    z[59]=3*z[6];
    z[66]=z[2] - z[8];
    z[67]=z[9] - z[59] - z[61] - z[66];
    z[67]=z[9]*z[67];
    z[53]=8*z[54] + z[53];
    z[53]=4*z[53] - 13*z[8];
    z[53]=z[8]*z[53];
    z[68]=49*z[54] + n<T>(17,2)*z[5];
    z[68]=z[5]*z[68];
    z[53]=n<T>(1,3)*z[68] + z[53];
    z[68]=z[6] - z[5];
    z[69]= - z[61] + z[68];
    z[69]=z[6]*z[69];
    z[70]= - 145*z[54] - 59*z[5];
    z[70]=n<T>(59,18)*z[2] + n<T>(1,9)*z[70] + z[6];
    z[70]=z[2]*z[70];
    z[65]=z[9] + n<T>(145,18)*z[2] + z[6] - n<T>(13,3)*z[8] - n<T>(67,18)*z[5] + 
    z[65];
    z[65]=z[4]*z[65];
    z[53]=z[65] + z[67] + z[70] + n<T>(1,3)*z[53] + z[69];
    z[53]=z[4]*z[53];
    z[65]=z[61] + z[9];
    z[67]=n<T>(1,2)*z[2];
    z[69]=n<T>(1,2)*z[8];
    z[70]= - n<T>(17,3)*z[3] + z[67] - z[69] + z[5] + z[65];
    z[70]=z[3]*z[70];
    z[71]=9*z[9] + 2*z[64] - z[66];
    z[71]=z[9]*z[71];
    z[72]=5*z[54];
    z[73]= - z[72] + 4*z[5];
    z[73]=z[5]*z[73];
    z[74]=z[61] - z[2];
    z[74]=z[2]*z[74];
    z[63]=z[70] + z[71] + z[74] + z[73] - z[63];
    z[63]=z[3]*z[63];
    z[70]= - z[72] + z[69];
    z[70]=z[8]*z[70];
    z[71]=2*z[6];
    z[62]=z[71] + z[62];
    z[62]=z[6]*z[62];
    z[72]=2*z[8];
    z[73]=67*z[54] - 20*z[5];
    z[73]= - n<T>(7,2)*z[2] - z[6] + n<T>(1,9)*z[73] + z[72];
    z[73]=z[2]*z[73];
    z[74]=29*z[54] + 35*z[5];
    z[74]=z[5]*z[74];
    z[62]=z[73] + z[62] + n<T>(1,9)*z[74] + z[70];
    z[62]=z[2]*z[62];
    z[70]=13*z[5];
    z[73]=11*z[54] - z[70];
    z[73]= - n<T>(1,3)*z[9] + z[6] + n<T>(1,2)*z[73] + z[66];
    z[73]=z[9]*z[73];
    z[58]=z[2] - 3*z[58];
    z[58]=z[5]*z[58];
    z[74]=z[8]*z[5];
    z[75]=z[6] + 4*z[54] - z[8];
    z[75]=z[6]*z[75];
    z[58]=z[73] + z[75] - z[74] + z[58];
    z[58]=z[9]*z[58];
    z[72]=z[2] - z[72] - z[54] - z[68];
    z[72]=z[2]*z[72];
    z[73]= - z[54] - z[8];
    z[71]= - n<T>(1,3)*z[7] + n<T>(5,3)*z[73] + z[71];
    z[71]=z[7]*z[71];
    z[59]=z[9] + z[5] + z[59];
    z[59]=z[6]*z[59];
    z[73]= - n<T>(20,3)*z[8] + n<T>(29,3)*z[54] - z[5];
    z[73]=z[8]*z[73];
    z[59]=2*z[71] + z[72] + z[73] + z[59];
    z[59]=z[7]*z[59];
    z[71]=z[2] + z[6];
    z[68]=z[68] + z[8];
    z[72]= - z[68]*z[71];
    z[65]= - z[65] + z[71];
    z[65]=z[9]*z[65];
    z[71]=z[68] - z[4] + z[9];
    z[71]=z[4]*z[71];
    z[68]= - z[9] + z[68];
    z[68]=z[7]*z[68];
    z[65]=z[68] + z[71] + z[65] + z[72];
    z[65]=z[14]*z[65];
    z[61]= - z[5]*z[61];
    z[68]=6*z[8] - 14*z[54] + 3*z[5];
    z[68]=z[8]*z[68];
    z[71]= - 8*z[6] - z[54] - 2*z[5];
    z[71]=z[6]*z[71];
    z[61]=z[71] + z[61] + z[68];
    z[61]=z[6]*z[61];
    z[68]=z[8] + z[7];
    z[68]=z[54]*z[68];
    z[68]=z[23] + z[68];
    z[68]=z[13]*z[68];
    z[60]=z[7] - z[60];
    z[60]=z[20]*z[60];
    z[60]=z[68] + z[60];
    z[68]= - z[5] - z[3];
    z[68]=z[54]*z[68];
    z[68]= - z[23] + z[68];
    z[68]=z[10]*z[68];
    z[71]= - z[3] + z[64];
    z[71]=z[17]*z[71];
    z[68]=z[68] + z[71];
    z[71]= - 20*z[54] - n<T>(23,2)*z[5];
    z[71]=z[5]*z[71];
    z[71]=z[71] + n<T>(23,2)*z[74];
    z[71]=z[8]*z[71];
    z[72]=npow(z[5],3);
    z[71]= - z[72] + z[71];
    z[64]=z[66] + z[64];
    z[64]= - 13*z[3] + 3*z[64] - 16*z[9];
    z[64]=z[21]*z[64];
    z[66]= - n<T>(1,2)*z[4] + z[54] - z[67];
    z[66]=z[15]*z[66];
    z[67]=14*z[2] - z[54] - z[70];
    z[67]=z[16]*z[67];
    z[54]= - n<T>(3,2)*z[2] + z[54] + z[69];
    z[54]=z[19]*z[54];
    z[69]=z[40] - z[28];
    z[70]=z[41] + z[39];
    z[56]= - z[32]*z[56];
    z[56]=z[56] + z[44];
    z[72]= - n<T>(152,9)*z[29] - n<T>(76,9)*z[27] + n<T>(7,18)*z[24];
    z[72]=i*z[72];
    z[73]=n<T>(8,3)*z[5] - n<T>(23,8)*z[8];
    z[73]= - n<T>(49,18)*z[2] + n<T>(1,3)*z[73] - 29*z[6];
    z[73]=n<T>(4,9)*z[12] - n<T>(4,3)*z[3] + z[14] + n<T>(5,6)*z[7] + n<T>(481,216)*z[4]
    + n<T>(1,3)*z[73] + 3*z[9];
    z[73]=z[23]*z[73];

    r +=  - n<T>(17,3)*z[25] - n<T>(19,27)*z[26] + n<T>(76,9)*z[30] - n<T>(529,18)*
      z[31] - n<T>(67,18)*z[35] - n<T>(11,2)*z[36] - 12*z[37] + n<T>(23,6)*z[38]
     - 
      n<T>(13,3)*z[42] - z[43] + 16*z[46] - n<T>(14,3)*z[47] + 4*z[48] + z[50]
       - z[51] + z[52] + z[53] + z[54] + z[55] + n<T>(26,3)*z[56] + 2*z[57]
       + z[58] + z[59] + n<T>(20,3)*z[60] + z[61] + z[62] + z[63] + z[64]
       + z[65] + n<T>(145,9)*z[66] + n<T>(2,9)*z[67] + 3*z[68] - n<T>(38,9)*z[69]
       - n<T>(5,2)*z[70] + n<T>(1,3)*z[71] + z[72] + z[73];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1079(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1079(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
