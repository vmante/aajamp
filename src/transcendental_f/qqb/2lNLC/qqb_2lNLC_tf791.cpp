#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf791(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=d[18];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=c[3];
    z[21]=d[9];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[3];
    z[31]=e[6];
    z[32]=e[11];
    z[33]=f[3];
    z[34]=f[4];
    z[35]=f[5];
    z[36]=f[6];
    z[37]=f[11];
    z[38]=f[12];
    z[39]=f[16];
    z[40]=f[17];
    z[41]=f[31];
    z[42]=f[36];
    z[43]=f[39];
    z[44]=z[1]*i;
    z[45]=2*z[44];
    z[46]=z[45] - z[6];
    z[47]=578*z[6];
    z[48]=z[46]*z[47];
    z[49]=3593*z[5];
    z[50]=2437*z[44] - z[49];
    z[50]=z[5]*z[50];
    z[51]=z[44] - z[5];
    z[52]=z[2]*z[51];
    z[53]=npow(z[9],2);
    z[54]=z[44] - n<T>(3,2)*z[6];
    z[54]=n<T>(7561,2)*z[3] - 578*z[2] + 125*z[54] - n<T>(1281,2)*z[5];
    z[54]=z[3]*z[54];
    z[48]=z[54] - 3468*z[53] - 1156*z[52] + z[48] + z[50];
    z[48]=z[3]*z[48];
    z[47]=86*z[5] + 695*z[44] - z[47];
    z[47]=z[5]*z[47];
    z[50]= - 6781*z[44] + n<T>(7999,2)*z[6];
    z[50]=z[6]*z[50];
    z[52]= - 1469*z[2] + 172*z[5] + 2938*z[44] + 547*z[6];
    z[52]=z[2]*z[52];
    z[47]=z[52] + z[50] + 4*z[47];
    z[47]=z[2]*z[47];
    z[50]= - z[44] + n<T>(1,2)*z[6];
    z[50]=z[50]*z[6];
    z[52]= - z[44] + n<T>(1,2)*z[5];
    z[52]=z[52]*z[5];
    z[53]= - n<T>(41365,2)*z[44] + 10841*z[6];
    z[53]=n<T>(1,3)*z[53] + n<T>(6561,2)*z[5];
    z[53]=n<T>(1,2)*z[53] - n<T>(3296,3)*z[9];
    z[53]=z[9]*z[53];
    z[53]=z[53] - n<T>(4249,3)*z[50] + n<T>(9623,2)*z[52];
    z[53]=z[9]*z[53];
    z[54]=npow(z[6],2);
    z[55]= - z[44] + n<T>(1,6)*z[6];
    z[55]=z[55]*z[54];
    z[56]=1281*z[3];
    z[49]=z[49] + z[56];
    z[49]=z[44]*z[49];
    z[49]=3593*z[20] + z[49];
    z[49]=z[8]*z[49];
    z[56]= - 3593*z[51] + z[56];
    z[56]=z[16]*z[56];
    z[57]= - z[46] + z[5];
    z[57]=z[5]*z[6]*z[57];
    z[47]=z[56] + z[49] + z[48] + z[53] + n<T>(1,3)*z[47] + 1937*z[55] + n<T>(31,3)*z[57];
    z[48]=z[44] - z[6];
    z[49]=z[2]*z[48];
    z[53]=9623*z[51] + n<T>(7311,2)*z[9];
    z[53]=z[9]*z[53];
    z[55]= - 2437*z[51] - n<T>(1281,2)*z[3];
    z[55]=z[3]*z[55];
    z[51]= - n<T>(6405,1531)*z[3] + n<T>(9623,3062)*z[9] + z[2] - z[6] - n<T>(3187,3062)*z[51];
    z[51]=z[7]*z[51];
    z[56]= - z[13]*z[44];
    z[49]=z[32] + z[56] + n<T>(1,2)*z[51] + n<T>(1,1531)*z[55] + n<T>(1,3062)*z[53]
    + z[49] + z[54] + n<T>(4749,3062)*z[52];
    z[49]=z[7]*z[49];
    z[51]=3*z[6];
    z[52]=z[46]*z[51];
    z[53]=z[44] + z[51];
    z[53]=2*z[53] - z[5];
    z[53]=z[5]*z[53];
    z[54]= - z[45] - z[5];
    z[54]=2*z[54] + z[2];
    z[54]=z[2]*z[54];
    z[51]=4*z[2] - z[51] - z[5];
    z[51]=z[4]*z[51];
    z[51]=z[51] + 2*z[54] + z[52] + z[53];
    z[51]=z[4]*z[51];
    z[51]=z[51] - z[33];
    z[52]=6592*z[44] - n<T>(10841,2)*z[6];
    z[52]=z[6]*z[52];
    z[53]= - 2*z[48] - z[9];
    z[53]=z[9]*z[53];
    z[54]= - z[6] - z[44] + z[11];
    z[54]=z[11]*z[54];
    z[52]=n<T>(4249,2)*z[54] + z[52] + 3296*z[53];
    z[52]=z[11]*z[52];
    z[53]=n<T>(1,2)*z[9] + z[48];
    z[53]=z[9]*z[53];
    z[54]=z[9] + z[48];
    z[54]=z[10]*z[54];
    z[50]=n<T>(1,2)*z[54] - n<T>(1,3)*z[20] + z[50] + z[53];
    z[50]=z[10]*z[50];
    z[53]=z[11] + z[6];
    z[44]=z[44]*z[53];
    z[44]=z[20] + z[44];
    z[44]=z[12]*z[44];
    z[53]=z[11] - z[48];
    z[53]=z[18]*z[53];
    z[44]=z[44] + z[53];
    z[53]=z[35] - z[26];
    z[54]=z[43] - z[41];
    z[55]=n<T>(781,3)*z[11] + n<T>(125,3)*z[4] - n<T>(3593,3)*z[7] - n<T>(1937,6)*z[3]
    - n<T>(47771,18)*z[9] - n<T>(344,9)*z[2] - 1786*z[6] + n<T>(18059,36)*z[5];
    z[55]= - n<T>(1156,4593)*z[21] + n<T>(1,1531)*z[55];
    z[55]=z[20]*z[55];
    z[56]= - n<T>(5876,4593)*z[27] - n<T>(2938,4593)*z[25] + n<T>(3757,13779)*z[22];
    z[56]=i*z[56];
    z[48]=992*z[7] - 289*z[48] + 414*z[3];
    z[48]=z[19]*z[48];
    z[45]=z[45] - z[2];
    z[57]= - z[4] + z[45];
    z[57]=z[14]*z[57];
    z[45]= - z[5] + z[45];
    z[45]=z[15]*z[45];
    z[46]= - z[2] + z[46];
    z[46]=z[17]*z[46];
    z[58]=z[9] + z[3];
    z[58]=z[30]*z[58];
    z[59]= - z[9] - z[11];
    z[59]=z[31]*z[59];

    r +=  - n<T>(18527,9186)*z[23] - n<T>(1469,27558)*z[24] + n<T>(2938,4593)*z[28]
       + n<T>(213547,18372)*z[29] + n<T>(3187,6124)*z[34] + n<T>(1,2)*z[36] + n<T>(31,4593)*z[37]
     - n<T>(6561,6124)*z[38]
     + n<T>(7311,6124)*z[39] - n<T>(375,1531)*
      z[40] - n<T>(10841,9186)*z[42] + n<T>(4249,4593)*z[44] + n<T>(344,4593)*z[45]
       + n<T>(1094,4593)*z[46] + n<T>(1,1531)*z[47] + n<T>(4,1531)*z[48] + z[49]
     +  n<T>(781,1531)*z[50] + n<T>(125,1531)*z[51] + n<T>(1,4593)*z[52] - n<T>(1469,4593)
      *z[53] + n<T>(781,3062)*z[54] + z[55] + z[56] + n<T>(500,1531)*z[57] + 
      n<T>(3468,1531)*z[58] + n<T>(6592,4593)*z[59];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf791(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf791(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
