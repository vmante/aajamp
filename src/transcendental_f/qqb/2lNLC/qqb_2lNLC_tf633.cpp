#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf633(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[9];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[5];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[17];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[51];
    z[52]=f[55];
    z[53]=f[58];
    z[54]=z[1]*i;
    z[55]=n<T>(25,2)*z[54];
    z[56]=3*z[2];
    z[57]=3*z[5];
    z[58]= - n<T>(25,4)*z[4] + z[56] + z[55] - z[57];
    z[58]=z[4]*z[58];
    z[59]=n<T>(1,2)*z[7];
    z[60]=z[59] - z[54];
    z[61]=z[60]*z[7];
    z[62]=z[54] + z[7];
    z[63]=z[62]*z[57];
    z[64]=n<T>(1,2)*z[2];
    z[65]=z[64] - z[7] - z[5];
    z[65]=z[65]*z[56];
    z[66]=z[54] - z[4];
    z[67]=n<T>(1,2)*z[8];
    z[68]= - z[67] - z[66];
    z[68]=z[8]*z[68];
    z[69]=z[54] - z[7];
    z[70]= - 198295*z[69] - n<T>(179923,2)*z[11];
    z[70]=z[11]*z[70];
    z[58]=n<T>(1,3062)*z[70] + n<T>(25,2)*z[68] + z[58] + z[65] - n<T>(198295,3062)*
    z[61] + z[63];
    z[63]= - 28423*z[54] + n<T>(179923,8)*z[7];
    z[63]=n<T>(5,4)*z[12] - n<T>(198295,12248)*z[11] - n<T>(37,8)*z[8] + n<T>(25,8)*z[4]
    + n<T>(1,1531)*z[63] + n<T>(3,4)*z[5];
    z[63]=z[12]*z[63];
    z[58]=n<T>(1,2)*z[58] + z[63];
    z[58]=z[12]*z[58];
    z[63]=3*z[7];
    z[65]=n<T>(1569397,9186)*z[5] - n<T>(1384483,9186)*z[54] - z[63];
    z[68]=n<T>(1,2)*z[5];
    z[65]=z[65]*z[68];
    z[70]=z[54] - z[5];
    z[71]= - n<T>(3,2)*z[2] + z[63] + n<T>(210145,3062)*z[70];
    z[71]=z[71]*z[64];
    z[72]=1412041*z[54] - 1384483*z[5];
    z[56]=n<T>(699131,4593)*z[8] + n<T>(1,9186)*z[72] - z[56];
    z[56]=z[56]*z[67];
    z[72]=2*z[54];
    z[73]= - z[72] + z[7];
    z[73]=z[7]*z[73];
    z[74]=npow(z[11],2);
    z[75]=n<T>(284965,6)*z[8] + n<T>(219331,8)*z[2] + n<T>(257407,6)*z[5] + n<T>(584107,24)*z[54]
     - 8853*z[7];
    z[75]= - n<T>(252791,3062)*z[3] + n<T>(1,1531)*z[75] + 3*z[11];
    z[75]=z[3]*z[75];
    z[56]=z[75] + n<T>(269119,6124)*z[74] + z[56] + z[71] + n<T>(8853,1531)*
    z[73] + z[65];
    z[56]=z[3]*z[56];
    z[65]=n<T>(3,2)*z[5];
    z[71]=z[7] - z[68];
    z[71]=z[71]*z[65];
    z[73]= - 69515*z[54] + n<T>(363949,8)*z[7];
    z[73]=z[7]*z[73];
    z[74]=z[2]*z[54];
    z[75]=z[2] - z[7];
    z[76]= - z[4]*z[75];
    z[77]=2*z[69] + z[11];
    z[77]=z[11]*z[77];
    z[62]=64455*z[62] - 76703*z[6];
    z[62]=z[6]*z[62];
    z[62]=n<T>(1,12248)*z[62] + n<T>(33992,1531)*z[77] + z[76] + z[74] + n<T>(1,1531)
   *z[73] + z[71];
    z[62]=z[6]*z[62];
    z[71]=n<T>(220679,36744)*z[5] - n<T>(220679,18372)*z[54] - z[63];
    z[71]=z[5]*z[71];
    z[73]=43*z[54] - 49*z[7];
    z[73]=n<T>(1,2)*z[73] + z[57];
    z[73]=z[2]*z[73];
    z[55]= - n<T>(19,4)*z[4] + z[55] - z[63];
    z[55]=z[4]*z[55];
    z[63]=2759995*z[54] - 2925343*z[5];
    z[63]= - 31*z[4] + n<T>(1,9186)*z[63] + 49*z[2];
    z[63]=n<T>(1,2)*z[63] + n<T>(31,3)*z[8];
    z[63]=z[63]*z[67];
    z[74]=npow(z[7],2);
    z[55]=z[63] + z[55] + z[73] + n<T>(61,4)*z[74] + z[71];
    z[55]=z[55]*z[67];
    z[63]=z[54]*z[4];
    z[71]=z[63] + z[24];
    z[73]=z[13]*z[71];
    z[76]=z[23]*z[66];
    z[77]=z[12] - z[6];
    z[77]=z[35]*z[77];
    z[78]=z[12] + z[9];
    z[78]=z[36]*z[78];
    z[73]= - z[73] - z[76] + z[77] + z[78];
    z[68]=z[68] - z[54];
    z[76]=z[5]*z[68];
    z[76]=24547*z[61] - n<T>(283143,2)*z[76];
    z[67]= - z[67] - z[70];
    z[67]=z[8]*z[67];
    z[77]=z[4] - z[2];
    z[78]= - n<T>(442183,2)*z[5] + n<T>(1133337,2)*z[54] - 345577*z[7];
    z[77]=n<T>(117286,4593)*z[11] - n<T>(707781,24496)*z[8] + n<T>(1,12248)*z[78]
     - 
   5*z[77];
    z[77]=z[11]*z[77];
    z[63]=z[77] + n<T>(849429,12248)*z[67] + n<T>(3,6124)*z[76] + 5*z[63];
    z[63]=z[11]*z[63];
    z[65]= - z[65] + z[54] + z[59];
    z[65]=z[2]*z[65];
    z[67]=z[54]*z[7];
    z[57]=z[7] + z[57];
    z[57]=n<T>(1,2)*z[57] - z[4];
    z[57]=z[4]*z[57];
    z[75]=z[6]*z[75];
    z[57]=n<T>(72355,18372)*z[24] + z[75] + z[57] - z[67] + z[65];
    z[57]=z[9]*z[57];
    z[65]=n<T>(2727841,3)*z[7] + n<T>(2743889,2)*z[5];
    z[65]=n<T>(1469453,2)*z[11] + n<T>(1119283,3)*z[8] - n<T>(408379,3)*z[4] + n<T>(1,2)
   *z[65] - n<T>(2330641,3)*z[2];
    z[65]= - n<T>(38474,3)*z[6] + n<T>(177259,24)*z[3] + n<T>(1,12)*z[65] + 22521*
    z[12];
    z[65]=z[24]*z[65];
    z[75]= - n<T>(218068,3)*z[3] + 17706*z[69] - n<T>(1439599,12)*z[8];
    z[75]=z[22]*z[75];
    z[76]=67984*z[11] + 63391*z[6];
    z[76]=z[34]*z[76];
    z[65]=z[76] + z[65] + z[75];
    z[75]=21485*z[6];
    z[76]= - z[54]*z[75];
    z[67]=z[67] + z[24];
    z[67]=z[76] - 33733*z[67];
    z[67]=z[15]*z[67];
    z[69]=33733*z[69] - z[75];
    z[69]=z[21]*z[69];
    z[67]=z[67] + z[69];
    z[69]=257407*z[3];
    z[75]= - n<T>(548159,2)*z[5] - z[69];
    z[75]=z[54]*z[75];
    z[75]= - n<T>(548159,2)*z[24] + z[75];
    z[75]=z[10]*z[75];
    z[69]=n<T>(548159,2)*z[70] - z[69];
    z[69]=z[18]*z[69];
    z[69]=z[75] + z[69];
    z[70]=476693*z[54] - n<T>(1297829,2)*z[7];
    z[59]=z[70]*z[59];
    z[70]=n<T>(999499,2)*z[2] - n<T>(66905,2)*z[5] - 999499*z[54] - 173141*z[7];
    z[70]=z[70]*z[64];
    z[75]= - n<T>(553763,2)*z[5] - n<T>(524183,2)*z[54] + 583709*z[7];
    z[75]=z[5]*z[75];
    z[59]=z[70] + z[59] + z[75];
    z[59]=z[2]*z[59];
    z[70]= - z[11]*z[54];
    z[70]=z[70] - z[71];
    z[70]=z[14]*z[70];
    z[66]=z[11] - z[66];
    z[66]=z[19]*z[66];
    z[66]= - z[39] + z[70] + z[66];
    z[70]=z[54] - n<T>(1,6)*z[7];
    z[70]=z[70]*z[74];
    z[71]= - n<T>(53,6)*z[5] + 53*z[54] - n<T>(237427,36744)*z[7];
    z[71]=z[5]*z[71];
    z[71]= - n<T>(264985,18372)*z[61] + z[71];
    z[71]=z[5]*z[71];
    z[74]=n<T>(371237,6)*z[5] - n<T>(398795,3)*z[54] - 309997*z[7];
    z[74]=z[5]*z[74];
    z[61]=319183*z[61] + z[74];
    z[74]= - 73289*z[2] + n<T>(682765,2)*z[54] + 132799*z[5];
    z[74]=z[2]*z[74];
    z[61]=n<T>(1,4)*z[61] + n<T>(1,3)*z[74];
    z[54]= - n<T>(655207,4593)*z[2] + n<T>(426353,9186)*z[5] - 23*z[54] + n<T>(309997,3062)*z[7];
    z[54]=n<T>(1,4)*z[54] + n<T>(11,3)*z[4];
    z[54]=z[4]*z[54];
    z[54]=n<T>(1,1531)*z[61] + z[54];
    z[54]=z[4]*z[54];
    z[61]=z[64] + z[68];
    z[61]=z[17]*z[61];
    z[60]=z[64] + z[60];
    z[60]=z[20]*z[60];
    z[64]=z[42] - z[29];
    z[68]=z[50] - z[48];
    z[74]=n<T>(1183219,9186)*z[30] + n<T>(999499,18372)*z[28] - n<T>(1298869,220464)
   *z[25];
    z[74]=i*z[74];
    z[72]=z[4] - z[72] + z[2];
    z[72]=z[16]*z[72];
    z[75]= - 278305*z[11] - 296677*z[3];
    z[75]=z[33]*z[75];
    z[76]=37*z[8] + 31*z[9];
    z[76]=z[37]*z[76];

    r += n<T>(1417093,18372)*z[26] - n<T>(102821,220464)*z[27] - n<T>(1183219,18372)
      *z[31] - n<T>(5454353,18372)*z[32] + 10*z[38] + n<T>(398795,36744)*z[40]
       - n<T>(2925343,73488)*z[41] - n<T>(264985,36744)*z[43] + n<T>(442183,24496)*
      z[44] - n<T>(707781,24496)*z[45] + n<T>(319183,12248)*z[46] + n<T>(10,3)*
      z[47] + n<T>(345577,12248)*z[49] + n<T>(25,8)*z[51] + n<T>(31,8)*z[52] - n<T>(37,8)*z[53]
     + z[54]
     + z[55]
     + z[56]
     + z[57]
     + z[58]
     + n<T>(1,9186)*z[59]
       + n<T>(173141,4593)*z[60] + n<T>(66905,9186)*z[61] + z[62] + z[63] + 
      n<T>(999499,36744)*z[64] + n<T>(1,1531)*z[65] + 5*z[66] + n<T>(3,6124)*z[67]
       - n<T>(179923,12248)*z[68] + n<T>(1,4593)*z[69] + n<T>(201755,6124)*z[70]
     +  z[71] + n<T>(192508,4593)*z[72] + n<T>(3,2)*z[73] + z[74] + n<T>(1,6124)*
      z[75] + n<T>(1,4)*z[76];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf633(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf633(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
