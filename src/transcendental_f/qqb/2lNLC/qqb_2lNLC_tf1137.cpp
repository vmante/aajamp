#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1137(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[31];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[19];
    z[11]=c[23];
    z[12]=c[25];
    z[13]=c[26];
    z[14]=c[28];
    z[15]=c[30];
    z[16]=c[31];
    z[17]=f[3];
    z[18]=f[11];
    z[19]=f[13];
    z[20]=f[17];
    z[21]=f[20];
    z[22]=n<T>(1,2)*z[4];
    z[23]= - 5*z[2] + n<T>(7,2)*z[3];
    z[23]=z[23]*z[22];
    z[24]= - z[2] + n<T>(1,4)*z[4] + n<T>(3,4)*z[3];
    z[24]=z[24]*z[5];
    z[25]=z[3]*z[2];
    z[26]=npow(z[2],2);
    z[23]=3*z[24] + z[23] + n<T>(19,4)*z[26] - 4*z[25];
    z[27]=z[1]*i;
    z[23]=z[23]*z[27];
    z[28]= - z[2] - n<T>(1,4)*z[3];
    z[28]=z[3]*z[28];
    z[29]=z[2] + n<T>(5,4)*z[3];
    z[29]=z[4]*z[29];
    z[28]=z[29] + 5*z[26] + 7*z[28];
    z[28]=z[28]*z[22];
    z[29]=npow(z[3],2);
    z[30]=z[3] - z[22];
    z[30]=z[4]*z[30];
    z[29]= - n<T>(1,2)*z[29] + z[30];
    z[24]=n<T>(3,2)*z[29] - z[24];
    z[24]=z[5]*z[24];
    z[24]=z[24] + z[15];
    z[29]= - z[2] - z[5];
    z[29]=n<T>(1,2)*z[29] + z[27];
    z[29]=z[8]*z[29];
    z[30]=npow(z[2],3);
    z[29]=z[30] - z[29];
    z[25]=n<T>(7,4)*z[26] + 2*z[25];
    z[25]=z[3]*z[25];
    z[26]= - z[2] - z[3];
    z[26]=n<T>(1,2)*z[26] + z[27];
    z[26]=z[6]*z[26];
    z[27]=n<T>(5,2)*z[27] - z[22] - 2*z[2];
    z[27]=z[7]*z[27];
    z[22]=z[22] + z[2] + n<T>(1,2)*z[3];
    z[22]=n<T>(7,3)*z[22] + n<T>(1,2)*z[5];
    z[22]=z[9]*z[22];
    z[22]=z[22] - z[16];
    z[30]= - n<T>(25,2)*z[13] - n<T>(19,4)*z[11];
    z[30]=i*z[30];

    r +=  - n<T>(25,48)*z[10] + n<T>(29,8)*z[12] + n<T>(37,4)*z[14] - n<T>(7,8)*z[17]
       - n<T>(3,8)*z[18] - n<T>(25,8)*z[19] - n<T>(9,8)*z[20] + z[21] + n<T>(1,4)*z[22]
       + z[23] + n<T>(3,2)*z[24] + z[25] + 7*z[26] + z[27] + z[28] - 3*
      z[29] + z[30];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1137(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1137(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
