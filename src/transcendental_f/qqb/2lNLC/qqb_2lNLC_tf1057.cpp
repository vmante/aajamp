#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1057(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[45];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[11];
    z[5]=d[4];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[17];
    z[10]=d[9];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=e[12];
    z[14]=e[13];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=d[0];
    z[18]=d[3];
    z[19]=e[6];
    z[20]=e[14];
    z[21]=f[30];
    z[22]=f[31];
    z[23]=f[33];
    z[24]=f[36];
    z[25]=f[39];
    z[26]=f[50];
    z[27]=f[51];
    z[28]=f[53];
    z[29]=f[55];
    z[30]=f[58];
    z[31]=z[5] - z[7];
    z[32]=n<T>(1,2)*z[6];
    z[33]=z[32] - z[9] + z[31];
    z[33]=z[6]*z[33];
    z[34]=z[11] - z[14];
    z[35]=n<T>(1,2)*z[8];
    z[36]=z[35] + z[10];
    z[37]=z[36]*z[8];
    z[38]=z[10]*z[4];
    z[39]= - z[7]*z[9];
    z[40]=npow(z[5],2);
    z[41]=n<T>(1,2)*z[2];
    z[42]=z[4] + z[41];
    z[42]=z[2]*z[42];
    z[43]=z[2] - z[7];
    z[43]= - z[5] + z[8] - 2*z[43];
    z[43]=z[3]*z[43];
    z[33]=z[43] + z[33] + z[42] + n<T>(1,2)*z[40] + z[39] - z[37] + z[38] + 
    z[13] + z[12] - z[34];
    z[33]=z[1]*z[33];
    z[33]= - n<T>(1,6)*z[16] + z[33];
    z[33]=i*z[33];
    z[38]=n<T>(1,2)*z[5];
    z[39]=z[38] - z[7];
    z[39]=z[39]*z[5];
    z[40]=npow(z[7],2);
    z[39]=z[39] + z[40];
    z[36]= - z[6] - z[38] + z[36];
    z[36]=z[3]*z[36];
    z[40]=npow(z[10],2);
    z[42]=npow(z[8],2);
    z[43]= - z[8] + z[2];
    z[43]=z[2]*z[43];
    z[44]=npow(z[6],2);
    z[36]=z[36] - z[44] + z[43] + n<T>(1,2)*z[42] + 3*z[34] + z[40] - z[39];
    z[36]=z[3]*z[36];
    z[42]= - z[10] - z[2];
    z[41]=z[42]*z[41];
    z[42]= - z[13] + n<T>(1,3)*z[15];
    z[37]=z[41] + z[37] - z[14] + z[42];
    z[37]=z[2]*z[37];
    z[41]=z[7] + z[6];
    z[32]=z[41]*z[32];
    z[41]=n<T>(1,6)*z[15];
    z[43]=z[41] + z[12];
    z[32]=z[32] + 2*z[11] + z[19] - z[43] + z[39];
    z[32]=z[6]*z[32];
    z[39]=z[17] - z[18];
    z[34]= - z[39]*z[34];
    z[39]= - z[40] - 2*z[14] - z[20] + z[42];
    z[39]=z[10]*z[39];
    z[40]= - z[10] - z[8];
    z[35]=z[40]*z[35];
    z[35]=z[35] - z[20] - z[41];
    z[35]=z[8]*z[35];
    z[31]=z[31]*z[38];
    z[31]=z[31] + z[19] + z[41];
    z[31]=z[5]*z[31];
    z[38]=z[27] - z[22] + z[30] - z[29] + z[24] - z[25];
    z[40]=z[23] - z[28];
    z[41]= - z[9] + z[4];
    z[41]=z[15]*z[41];
    z[42]=z[11] - z[43];
    z[42]=z[7]*z[42];

    r +=  - z[21] + z[26] + z[31] + z[32] + z[33] + z[34] + z[35] + 
      z[36] + z[37] + n<T>(1,2)*z[38] + z[39] - 4*z[40] + z[41] + z[42];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1057(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1057(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
