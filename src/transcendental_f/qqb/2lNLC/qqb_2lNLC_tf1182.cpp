#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1182(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[43];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[1];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[7];
    z[9]=d[4];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[19];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[31];
    z[22]=e[10];
    z[23]=f[3];
    z[24]=f[4];
    z[25]=f[11];
    z[26]=f[12];
    z[27]=f[13];
    z[28]=f[16];
    z[29]=f[17];
    z[30]=2*z[4];
    z[31]=z[1]*i;
    z[32]=z[30]*z[31];
    z[33]=npow(z[4],2);
    z[32]=z[32] - z[33];
    z[34]=z[31] - z[4];
    z[35]=2*z[34];
    z[36]= - z[35] - z[5];
    z[36]=z[5]*z[36];
    z[37]= - 5*z[34] - 8*z[5];
    z[37]=z[6]*z[37];
    z[36]=z[37] + 4*z[36] + z[32];
    z[36]=z[6]*z[36];
    z[37]=z[31]*z[4];
    z[38]= - n<T>(5,2)*z[33] - 19*z[37];
    z[39]=7*z[2];
    z[40]=2*z[31];
    z[41]= - n<T>(1,2)*z[2] + z[4] + z[40];
    z[41]=z[41]*z[39];
    z[39]=n<T>(19,4)*z[4] - z[39];
    z[39]=z[3]*z[39];
    z[38]=z[39] + n<T>(1,2)*z[38] + z[41];
    z[38]=z[3]*z[38];
    z[39]= - z[33] + 4*z[37];
    z[41]=z[31] + z[4];
    z[42]=5*z[5] - z[41];
    z[42]=z[5]*z[42];
    z[39]=2*z[39] + z[42];
    z[39]=z[5]*z[39];
    z[36]=z[38] + z[39] + z[36];
    z[38]= - z[33] - z[37];
    z[39]= - z[30] - n<T>(13,2)*z[31];
    z[39]=n<T>(4,3)*z[2] + n<T>(1,3)*z[39] + z[5];
    z[39]=z[2]*z[39];
    z[30]= - z[5]*z[30];
    z[30]=z[39] + n<T>(1,3)*z[38] + z[30];
    z[30]=z[2]*z[30];
    z[35]=z[35] + z[6];
    z[35]=z[6]*z[35];
    z[38]=z[6] + z[34];
    z[38]=z[9]*z[38];
    z[32]=z[38] + z[35] - z[32];
    z[32]=z[9]*z[32];
    z[35]= - z[4] + z[3];
    z[35]=z[8]*z[35];
    z[33]=z[33] + z[35];
    z[35]=n<T>(1,2)*z[3] - z[41];
    z[35]=z[3]*z[35];
    z[33]=z[35] + z[37] + n<T>(1,2)*z[33];
    z[33]=z[8]*z[33];
    z[35]=z[5] + z[4];
    z[31]=z[31]*z[35];
    z[31]=z[13] + z[31];
    z[31]=z[7]*z[31];
    z[34]=z[5] - z[34];
    z[34]=z[12]*z[34];
    z[31]= - z[15] + z[31] + z[34];
    z[34]= - z[18] + z[27] + z[23];
    z[35]=z[29] - z[25];
    z[37]=n<T>(19,3)*z[19] + n<T>(19,6)*z[17] + n<T>(1,18)*z[14];
    z[37]=i*z[37];
    z[38]= - n<T>(11,18)*z[4] + z[5];
    z[38]=n<T>(1,4)*z[8] - n<T>(2,3)*z[9] - n<T>(19,36)*z[3] - n<T>(2,9)*z[6] + n<T>(1,2)*
    z[38] - n<T>(5,9)*z[2];
    z[38]=z[13]*z[38];
    z[39]=z[2] + z[4] - z[40];
    z[39]=z[5] + n<T>(1,3)*z[39];
    z[39]=z[11]*z[39];
    z[40]=z[3] - z[40] + z[2];
    z[40]=z[10]*z[40];
    z[41]=z[5] + z[6];
    z[41]=z[22]*z[41];

    r += n<T>(19,72)*z[16] - n<T>(19,6)*z[20] + n<T>(23,6)*z[21] + n<T>(5,3)*z[24]
     +  z[26] + z[28] + z[30] + n<T>(2,3)*z[31] + z[32] + n<T>(3,2)*z[33] + n<T>(19,12)*z[34]
     + n<T>(3,4)*z[35]
     + n<T>(1,3)*z[36]
     + z[37]
     + z[38]
     + z[39]
     + n<T>(7,3)*z[40]
     + n<T>(8,3)*z[41];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1182(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1182(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
