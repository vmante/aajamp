#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1251(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[109];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[9];
    z[8]=e[7];
    z[9]=d[2];
    z[10]=d[8];
    z[11]=d[11];
    z[12]=e[12];
    z[13]=d[12];
    z[14]=d[4];
    z[15]=d[17];
    z[16]=e[5];
    z[17]=e[9];
    z[18]=e[13];
    z[19]=e[6];
    z[20]=e[14];
    z[21]=f[30];
    z[22]=f[31];
    z[23]=f[32];
    z[24]=f[33];
    z[25]=f[35];
    z[26]=f[36];
    z[27]=f[39];
    z[28]=f[43];
    z[29]=f[50];
    z[30]=f[51];
    z[31]=f[52];
    z[32]=f[55];
    z[33]=f[58];
    z[34]=f[68];
    z[35]=c[3];
    z[36]=c[11];
    z[37]=c[15];
    z[38]=c[17];
    z[39]=c[31];
    z[40]=c[32];
    z[41]=c[39];
    z[42]=c[43];
    z[43]=c[45];
    z[44]=c[46];
    z[45]=c[78];
    z[46]=c[81];
    z[47]=f[53];
    z[48]=g[66];
    z[49]=g[67];
    z[50]=g[84];
    z[51]=g[116];
    z[52]=g[117];
    z[53]=g[133];
    z[54]=g[159];
    z[55]=g[160];
    z[56]=g[162];
    z[57]=g[163];
    z[58]=g[165];
    z[59]=g[167];
    z[60]=g[174];
    z[61]=g[175];
    z[62]=g[178];
    z[63]=g[184];
    z[64]=g[185];
    z[65]=g[186];
    z[66]=g[195];
    z[67]=g[209];
    z[68]=g[221];
    z[69]=n<T>(8017,3)*z[7];
    z[70]=1845*z[4];
    z[71]=z[69] + z[70];
    z[72]=n<T>(20677,6)*z[3];
    z[73]=z[72] - z[71];
    z[73]=z[73]*z[7];
    z[74]=5535*z[4];
    z[75]=z[74]*z[3];
    z[76]=npow(z[3],2);
    z[77]=z[75] + 1687*z[76];
    z[73]=z[73] + n<T>(1,2)*z[77];
    z[77]=z[70] + n<T>(1687,3)*z[3];
    z[69]=z[77] + z[69];
    z[78]=107*z[5];
    z[79]=z[1]*i;
    z[80]=z[69] + z[78] - 107*z[79];
    z[81]=z[80]*z[10];
    z[82]=z[80]*z[9];
    z[83]=n<T>(1,2)*z[82];
    z[84]=z[7]*i;
    z[85]= - n<T>(265,3)*z[3] - n<T>(369,2)*z[4];
    z[85]=i*z[85];
    z[85]=z[85] - n<T>(1475,6)*z[84];
    z[85]=z[1]*z[85];
    z[86]=n<T>(3,2)*z[3];
    z[87]=z[86] - z[7];
    z[88]=n<T>(1,2)*z[79];
    z[89]= - z[88] + z[87];
    z[89]=z[89]*z[78];
    z[85]= - z[81] + z[83] + z[89] + 5*z[85] + z[73];
    z[85]=z[10]*z[85];
    z[73]=n<T>(1,2)*z[73];
    z[89]= - n<T>(181,3)*z[3] - n<T>(1845,4)*z[4];
    z[89]=i*z[89];
    z[89]=z[89] - n<T>(8659,12)*z[84];
    z[89]=z[1]*z[89];
    z[90]= - z[88] - z[87];
    z[91]=n<T>(107,2)*z[5];
    z[90]=z[90]*z[91];
    z[82]=n<T>(1,8)*z[82] + z[90] - z[73] + z[89];
    z[82]=z[9]*z[82];
    z[75]=n<T>(3053,2)*z[76] + z[75];
    z[89]=n<T>(1,2)*i;
    z[75]=z[75]*z[89];
    z[72]=z[72] - z[70];
    z[72]=i*z[72];
    z[90]=n<T>(8017,3)*z[84];
    z[72]=z[72] - z[90];
    z[72]=z[7]*z[72];
    z[72]=z[75] + z[72];
    z[72]=z[1]*z[72];
    z[74]=z[74] + 8017*z[7];
    z[75]=z[74]*z[76];
    z[92]=npow(z[3],3);
    z[75]=z[75] + 1687*z[92];
    z[93]=i*z[86];
    z[93]=z[93] - z[84];
    z[93]=z[1]*z[93];
    z[93]=n<T>(3,4)*z[76] + z[93];
    z[93]=z[93]*z[78];
    z[72]=z[93] + n<T>(1,4)*z[75] + z[72];
    z[72]=n<T>(1,4)*z[85] + n<T>(1,2)*z[72] + z[82];
    z[72]=z[10]*z[72];
    z[82]=z[86] - z[4];
    z[82]=n<T>(1409,2)*z[82];
    z[85]=z[82]*z[7];
    z[86]= - n<T>(683,6)*z[4] + 87*z[3];
    z[86]=z[86]*z[4];
    z[85]=z[86] - z[85] + n<T>(1005,8)*z[76];
    z[93]=1409*z[7];
    z[94]=z[93] + n<T>(1813,6)*z[5];
    z[95]=n<T>(683,3)*z[4] + n<T>(335,2)*z[3];
    z[96]= - n<T>(1813,6)*z[79] + z[94] - z[95];
    z[97]=n<T>(1,8)*z[6];
    z[98]= - z[96]*z[97];
    z[99]= - 537*z[3] + n<T>(565,3)*z[4];
    z[99]=i*z[99];
    z[99]=z[99] + n<T>(1409,2)*z[84];
    z[100]=n<T>(1,2)*z[1];
    z[99]=z[99]*z[100];
    z[101]=n<T>(1,2)*z[3];
    z[102]=n<T>(1,3)*z[4];
    z[103]=z[101] - z[102];
    z[104]=n<T>(1,6)*z[79];
    z[105]=z[104] + z[103];
    z[106]=n<T>(1813,4)*z[5];
    z[105]=z[105]*z[106];
    z[98]=z[98] + z[105] + z[99] - z[85];
    z[98]=z[6]*z[98];
    z[86]=n<T>(3823,16)*z[76] + z[86];
    z[86]=i*z[86];
    z[82]= - z[84]*z[82];
    z[82]=z[86] + z[82];
    z[82]=z[1]*z[82];
    z[86]= - 4227*z[7] + 683*z[4];
    z[99]=z[76]*z[86];
    z[99]=n<T>(1005,2)*z[92] + z[99];
    z[105]= - z[103]*z[79];
    z[105]= - n<T>(1,4)*z[76] + z[105];
    z[105]=z[105]*z[106];
    z[82]=z[98] + z[105] + n<T>(1,8)*z[99] + z[82];
    z[98]=n<T>(1,8)*z[14] - n<T>(1,16)*z[6];
    z[98]=z[96]*z[98];
    z[99]=n<T>(739,16)*z[3] - 52*z[4];
    z[99]=i*z[99];
    z[99]=z[99] + n<T>(1409,16)*z[84];
    z[99]=z[1]*z[99];
    z[103]=z[104] - z[103];
    z[103]=z[5]*z[103];
    z[85]=n<T>(1813,16)*z[103] + n<T>(1,4)*z[85] + z[99] + z[98];
    z[85]=z[14]*z[85];
    z[82]=n<T>(1,2)*z[82] + z[85];
    z[82]=z[14]*z[82];
    z[85]=n<T>(3635,2)*z[3] - 3409*z[4];
    z[85]=z[4]*z[85];
    z[85]=n<T>(7391,4)*z[76] + z[85];
    z[98]=n<T>(175,3)*z[3] - 23*z[4];
    z[98]=7*z[98] - n<T>(2269,6)*z[7];
    z[98]=z[7]*z[98];
    z[85]=n<T>(1,6)*z[85] + z[98];
    z[98]=1519*z[3] - n<T>(3767,4)*z[4];
    z[98]=z[98]*z[89];
    z[98]=z[98] - 397*z[84];
    z[98]=z[1]*z[98];
    z[99]=n<T>(1,9)*z[5];
    z[103]=n<T>(20125,32)*z[5] - 4625*z[79] + n<T>(641,2)*z[7] - 4391*z[3] + 
   n<T>(29621,8)*z[4];
    z[103]=z[103]*z[99];
    z[85]=z[103] + n<T>(1,2)*z[85] + n<T>(1,3)*z[98];
    z[85]=z[5]*z[85];
    z[98]= - 299*z[3] + n<T>(591,4)*z[4];
    z[98]=z[4]*z[98];
    z[103]=n<T>(215,4)*z[4] + n<T>(557,3)*z[3];
    z[104]= - n<T>(3803,36)*z[7] + z[103];
    z[104]=z[7]*z[104];
    z[98]=z[104] + n<T>(845,6)*z[76] + z[98];
    z[98]=z[7]*z[98];
    z[104]= - n<T>(3185,3)*z[3] + n<T>(12163,4)*z[4];
    z[104]=z[4]*z[104];
    z[104]= - n<T>(94517,24)*z[76] + z[104];
    z[104]=i*z[104];
    z[105]= - n<T>(2297,6)*z[3] + 212*z[4];
    z[105]=i*z[105];
    z[105]=z[105] + n<T>(1058,3)*z[84];
    z[105]=z[7]*z[105];
    z[104]=n<T>(1,4)*z[104] + z[105];
    z[104]=z[1]*z[104];
    z[105]=n<T>(28327,4)*z[3] + n<T>(2611,3)*z[4];
    z[105]=z[105]*z[102];
    z[105]= - n<T>(24319,4)*z[76] + z[105];
    z[105]=z[4]*z[105];
    z[105]=n<T>(173059,36)*z[92] + z[105];
    z[85]=z[85] + z[104] + n<T>(1,4)*z[105] + z[98];
    z[85]=z[5]*z[85];
    z[98]= - n<T>(140269,2)*z[3] + 14207*z[4];
    z[98]=z[4]*z[98];
    z[98]=n<T>(180707,8)*z[76] + z[98];
    z[104]=10321*z[3] + n<T>(11663,2)*z[4];
    z[104]= - n<T>(120629,8)*z[5] + n<T>(1,2)*z[104] - 3571*z[7];
    z[99]=z[104]*z[99];
    z[104]=n<T>(96131,2)*z[3] - 631*z[4];
    z[104]= - n<T>(9217,18)*z[5] + n<T>(1,9)*z[104] - 991*z[7];
    z[97]=z[104]*z[97];
    z[71]=z[78] - n<T>(239,3)*z[3] + z[71];
    z[71]=n<T>(1,2)*z[71] + 107*z[9];
    z[104]=n<T>(1,2)*z[9];
    z[71]=z[71]*z[104];
    z[104]= - 3295*z[3] + n<T>(4387,3)*z[4];
    z[104]=n<T>(1,2)*z[104] + n<T>(2627,3)*z[7];
    z[104]=z[7]*z[104];
    z[71]=z[71] + z[97] + z[99] + n<T>(1,18)*z[98] + z[104];
    z[97]= - n<T>(6109,2)*z[3] + n<T>(4073,3)*z[4];
    z[94]=n<T>(1813,8)*z[14] + n<T>(1813,4)*z[6] + n<T>(1,2)*z[97] + z[94];
    z[94]=z[14]*z[94];
    z[97]= - n<T>(97,18)*z[3] - 123*z[4];
    z[97]=n<T>(1,2)*z[97] - n<T>(898,9)*z[7];
    z[97]= - n<T>(107,8)*z[10] - n<T>(107,4)*z[9] + 5*z[97] - n<T>(107,6)*z[5];
    z[97]=z[10]*z[97];
    z[71]=n<T>(1,12)*z[94] + n<T>(1,2)*z[71] + z[97];
    z[71]=z[35]*z[71];
    z[94]=n<T>(1,2)*z[7];
    z[69]= - z[69]*z[94];
    z[97]=z[77]*z[89];
    z[98]=n<T>(4169,3)*z[84];
    z[97]=z[97] + z[98];
    z[97]=z[1]*z[97];
    z[99]= - z[7] + z[79];
    z[99]=z[99]*z[91];
    z[69]=n<T>(107,2)*z[35] - z[83] + z[99] + z[69] + z[97];
    z[69]=z[12]*z[69];
    z[97]=n<T>(4425,4)*z[3] - 397*z[4];
    z[97]=z[4]*z[97];
    z[99]= - n<T>(8039,3)*z[7] + n<T>(37955,12)*z[3] - 1892*z[4];
    z[99]=z[7]*z[99];
    z[97]=n<T>(1,3)*z[99] + n<T>(9317,12)*z[76] + z[97];
    z[97]=z[7]*z[97];
    z[99]=n<T>(1787,2)*z[3] - n<T>(997,3)*z[4];
    z[99]=z[4]*z[99];
    z[99]= - n<T>(4553,2)*z[76] + z[99];
    z[99]=z[4]*z[99];
    z[99]= - n<T>(79349,18)*z[92] + z[99];
    z[97]=n<T>(1,4)*z[99] + z[97];
    z[97]=z[7]*z[97];
    z[99]=9*z[4];
    z[104]=23*z[3] + n<T>(199,4)*z[4];
    z[104]=z[104]*z[99];
    z[104]= - n<T>(23413,24)*z[76] + z[104];
    z[104]=z[104]*z[89];
    z[103]= - i*z[103];
    z[103]=z[103] + n<T>(3803,36)*z[84];
    z[103]=z[7]*z[103];
    z[103]=z[104] + z[103];
    z[103]=z[7]*z[103];
    z[104]=673*z[3] - n<T>(35963,36)*z[4];
    z[104]=z[4]*z[104];
    z[104]= - n<T>(13457,12)*z[76] + z[104];
    z[104]=z[4]*z[104];
    z[104]= - n<T>(99623,144)*z[92] + z[104];
    z[104]=z[104]*z[89];
    z[103]=z[104] + z[103];
    z[103]=z[1]*z[103];
    z[104]=z[77]*i;
    z[90]=z[104] + z[90];
    z[90]=z[90]*z[1];
    z[78]=z[78]*z[79];
    z[78]=z[90] + z[78];
    z[90]=z[9] + z[7];
    z[78]=z[78]*z[90];
    z[77]=n<T>(107,2)*z[9] + z[91] + n<T>(1,2)*z[77] + n<T>(4169,3)*z[7];
    z[77]=z[35]*z[77];
    z[77]=n<T>(1,2)*z[78] + z[77];
    z[77]=z[11]*z[77];
    z[78]=56501*z[3] + 3059*z[4];
    z[78]=z[78]*z[102];
    z[78]= - n<T>(52613,2)*z[76] + z[78];
    z[78]=z[4]*z[78];
    z[78]= - n<T>(240571,6)*z[92] + z[78];
    z[78]=z[4]*z[78];
    z[90]=npow(z[3],4);
    z[78]= - n<T>(26543,4)*z[90] + z[78];
    z[69]=z[69] + z[82] + z[72] + z[85] + z[103] + n<T>(1,24)*z[78] + z[97]
    + z[71] + z[77];
    z[71]=n<T>(9217,6)*z[5];
    z[72]=z[71]*z[79];
    z[77]=2973*z[84];
    z[78]= - 10247*z[4] + n<T>(25351,2)*z[3];
    z[82]=n<T>(1,3)*z[78];
    z[85]= - i*z[82];
    z[85]=z[85] - z[77];
    z[85]=z[1]*z[85];
    z[85]=z[85] - z[72];
    z[85]=z[6]*z[85];
    z[78]=z[78]*z[102];
    z[90]= - i*z[78];
    z[97]= - z[4]*z[77];
    z[90]=z[90] + z[97];
    z[90]=z[1]*z[90];
    z[72]= - z[4]*z[72];
    z[97]=2973*z[7];
    z[102]=z[97] + z[71];
    z[103]= - 3759*z[4] + n<T>(25351,3)*z[3];
    z[104]= - n<T>(9217,6)*z[6] - n<T>(1,2)*z[103] - z[102];
    z[104]=z[35]*z[104];
    z[72]=z[104] + z[85] + z[90] + z[72];
    z[72]=z[15]*z[72];
    z[82]=n<T>(9217,6)*z[79] - z[82] - z[102];
    z[82]=z[6]*z[82];
    z[85]=z[103]*z[89];
    z[77]=z[85] + z[77];
    z[77]=z[1]*z[77];
    z[85]= - z[4] + z[79];
    z[71]=z[85]*z[71];
    z[85]= - z[4]*z[97];
    z[71]=n<T>(9217,6)*z[35] + z[82] + z[71] + z[77] - z[78] + z[85];
    z[71]=z[17]*z[71];
    z[77]=z[79] - z[5];
    z[78]=151*z[7];
    z[82]= - z[78] - 1465*z[3] + 543*z[4];
    z[82]=3*z[82] + 709*z[77];
    z[82]=z[23]*z[82];
    z[85]=555*z[7] - 89*z[3] + 705*z[4] - 759*z[77];
    z[85]=z[31]*z[85];
    z[71]=z[72] + z[71] + z[82] + z[85];
    z[72]=797*z[4];
    z[82]= - z[72] + n<T>(11371,6)*z[3];
    z[82]=z[82]*z[4];
    z[82]=z[82] - n<T>(6589,6)*z[76];
    z[85]=23*z[7];
    z[90]=z[85] + n<T>(617,34)*z[5];
    z[97]=z[4] - z[3];
    z[102]=z[97]*z[90];
    z[103]=n<T>(617,34)*z[79];
    z[104]=z[103]*z[97];
    z[82]=z[102] - z[104] + n<T>(1,17)*z[82];
    z[105]=n<T>(1,2)*z[6];
    z[106]=n<T>(1,4)*z[2] - z[105];
    z[82]=z[82]*z[106];
    z[106]= - n<T>(2211,68)*z[4] + n<T>(140,3)*z[3];
    z[106]=z[106]*z[4];
    z[106]=z[106] - n<T>(2887,204)*z[76];
    z[107]= - n<T>(617,68)*z[5] - n<T>(23,2)*z[7];
    z[107]=z[97]*z[107];
    z[104]=z[104] + z[107] - z[106];
    z[104]=z[5]*z[104];
    z[106]=i*z[106];
    z[107]=z[97]*z[84];
    z[106]=z[106] + n<T>(23,2)*z[107];
    z[106]=z[1]*z[106];
    z[107]=n<T>(1807,6)*z[3] - z[72];
    z[107]=z[4]*z[107];
    z[107]=n<T>(16153,6)*z[76] + z[107];
    z[107]=z[4]*z[107];
    z[107]= - n<T>(6589,3)*z[92] + z[107];
    z[108]=z[3] + z[4];
    z[108]=z[4]*z[108];
    z[108]= - z[76] + n<T>(1,2)*z[108];
    z[85]=z[108]*z[85];
    z[97]=z[35]*z[97];
    z[82]=n<T>(617,68)*z[97] + z[104] + z[106] + n<T>(1,34)*z[107] + z[85] + 
    z[82];
    z[82]=z[2]*z[82];
    z[70]=n<T>(2411,6)*z[3] + z[70];
    z[70]=z[70]*z[89];
    z[70]=z[70] + z[98];
    z[70]=z[1]*z[70];
    z[85]=z[79] + z[87];
    z[85]=z[85]*z[91];
    z[70]= - z[83] + z[85] + z[73] + z[70];
    z[70]=z[9]*z[70];
    z[73]=z[4]*z[3];
    z[83]= - n<T>(739,2)*z[76] - 1107*z[73];
    z[83]=i*z[83];
    z[85]=z[3]*z[84];
    z[83]=5*z[83] - 8017*z[85];
    z[83]=z[1]*z[83];
    z[85]= - z[3]*z[79];
    z[85]=n<T>(1,2)*z[76] + z[85];
    z[87]=321*z[5];
    z[85]=z[85]*z[87];
    z[75]=z[85] + n<T>(1,2)*z[75] + z[83];
    z[70]=n<T>(1,2)*z[75] + z[70];
    z[70]=z[9]*z[70];
    z[75]= - z[7]*z[80];
    z[75]= - z[81] + z[75];
    z[75]=z[20]*z[75];
    z[70]=z[70] + z[75];
    z[75]= - z[4]*z[95];
    z[75]=n<T>(683,2)*z[76] + z[75];
    z[75]=z[4]*z[75];
    z[81]=npow(z[4],2);
    z[81]= - n<T>(3,2)*z[76] + z[81];
    z[81]=z[81]*z[93];
    z[75]=z[81] + n<T>(1005,4)*z[92] + z[75];
    z[81]= - 1681*z[3] + n<T>(919,24)*z[4];
    z[81]=z[4]*z[81];
    z[81]=n<T>(104833,48)*z[76] + z[81];
    z[81]=i*z[81];
    z[83]=n<T>(1,2)*z[84];
    z[85]=n<T>(7355,2)*z[3] - 1409*z[4];
    z[85]=z[85]*z[83];
    z[81]=z[81] + z[85];
    z[81]=z[1]*z[81];
    z[75]=n<T>(1,4)*z[75] + z[81];
    z[81]=11371*z[3] - n<T>(17315,4)*z[4];
    z[81]=z[4]*z[81];
    z[81]= - n<T>(58151,8)*z[76] + z[81];
    z[85]=n<T>(9217,2)*z[3] - n<T>(5515,3)*z[4];
    z[85]=z[85]*z[79];
    z[81]=n<T>(1,68)*z[85] + n<T>(1,102)*z[81] + z[102];
    z[81]=z[5]*z[81];
    z[85]=n<T>(1,2)*z[5];
    z[88]= - z[85] + z[88];
    z[89]= - n<T>(5515,51)*z[4] + n<T>(397,4)*z[3];
    z[88]=z[89]*z[88];
    z[89]=n<T>(45523,4)*z[3] - 5465*z[4];
    z[89]=z[4]*z[89];
    z[89]= - n<T>(49697,8)*z[76] + z[89];
    z[91]= - n<T>(7355,4)*z[3] + 2191*z[4];
    z[91]=z[7]*z[91];
    z[89]=n<T>(1,3)*z[89] + z[91];
    z[88]=n<T>(1,17)*z[89] + z[88];
    z[88]=z[88]*z[105];
    z[75]=z[88] + n<T>(1,17)*z[75] + z[81];
    z[75]=z[75]*z[105];
    z[72]= - z[72] + n<T>(6589,6)*z[3];
    z[72]=z[90] - z[103] + n<T>(1,17)*z[72];
    z[81]= - z[2] + z[6];
    z[81]=z[72]*z[81];
    z[88]=n<T>(26977,6)*z[3] - 2645*z[4];
    z[88]=z[4]*z[88];
    z[89]=1583*z[3] + n<T>(4513,4)*z[4];
    z[89]=z[7]*z[89];
    z[88]=z[89] + n<T>(2758,3)*z[76] + n<T>(1,4)*z[88];
    z[89]=n<T>(461,8)*z[4] + n<T>(7295,3)*z[3];
    z[90]= - n<T>(617,17)*z[79] + n<T>(1,17)*z[89] + z[90];
    z[90]=z[5]*z[90];
    z[91]=n<T>(1,17)*i;
    z[89]= - z[89]*z[91];
    z[89]=z[89] - 23*z[84];
    z[89]=z[1]*z[89];
    z[81]= - n<T>(2509,68)*z[8] - n<T>(2687,136)*z[35] + z[90] + n<T>(1,17)*z[88]
     + 
    z[89] + z[81];
    z[81]=z[8]*z[81];
    z[73]= - 767*z[73] + n<T>(8013,2)*z[76];
    z[73]=n<T>(1,68)*z[73];
    z[88]=z[101] + z[4];
    z[89]= - z[94] + z[88];
    z[90]=3*z[7];
    z[89]=z[89]*z[90];
    z[91]= - n<T>(4309,2)*z[4] + 2421*z[3];
    z[91]=n<T>(1,17)*z[91];
    z[92]=z[91]*i;
    z[92]=z[92] + 3*z[84];
    z[93]=z[92]*z[100];
    z[91]=z[91] + z[90];
    z[91]=n<T>(1,2)*z[91];
    z[94]= - n<T>(1875,136)*z[5] - z[91] + n<T>(77,17)*z[79];
    z[94]=z[5]*z[94];
    z[95]=z[13]*z[79];
    z[89]= - n<T>(1567,136)*z[16] + n<T>(1567,68)*z[95] + n<T>(2131,204)*z[35] + 
    z[94] + z[93] - z[73] + z[89];
    z[89]=z[16]*z[89];
    z[73]=i*z[73];
    z[88]= - i*z[88];
    z[83]=z[88] + z[83];
    z[83]=z[83]*z[90];
    z[73]=z[73] + z[83];
    z[73]=z[1]*z[73];
    z[83]=n<T>(1567,136)*z[13] + z[91] + n<T>(77,17)*z[5];
    z[83]=z[35]*z[83];
    z[88]=z[1]*z[92];
    z[90]=z[5]*z[79];
    z[88]=z[88] + n<T>(1875,68)*z[90];
    z[85]=z[88]*z[85];
    z[73]=z[73] + z[85] + z[83];
    z[73]=z[13]*z[73];
    z[83]=n<T>(9217,34)*z[6] + n<T>(196205,34)*z[5] - n<T>(119057,34)*z[3] - 35*z[4]
   ;
    z[83]=i*z[83];
    z[83]= - n<T>(12455,17)*z[84] + z[83];
    z[85]=z[9]*i;
    z[83]=n<T>(1,18)*z[83] - n<T>(107,17)*z[85];
    z[85]=n<T>(9217,204)*z[15] - n<T>(107,17)*z[11] - n<T>(2131,102)*z[13] - n<T>(1813,612)*z[14]
     + n<T>(107,51)*z[10];
    z[85]=i*z[85];
    z[83]=n<T>(1,4)*z[83] + z[85];
    z[83]=z[36]*z[83];
    z[85]= - n<T>(15455,6)*z[3] + 1103*z[4];
    z[85]=n<T>(1,17)*z[85] - 199*z[7] + n<T>(6299,34)*z[77];
    z[85]=z[24]*z[85];
    z[88]= - 5*z[7] + n<T>(1567,102)*z[3] + 3*z[4] + n<T>(1363,102)*z[77];
    z[88]=z[34]*z[88];
    z[83]=z[88] + z[83] + z[85];
    z[85]=z[14] + z[4];
    z[85]=z[19]*z[85];
    z[85]=n<T>(1,68)*z[85] + n<T>(1,272)*z[26] - n<T>(1,68)*z[21];
    z[85]=z[96]*z[85];
    z[74]= - 321*z[79] + 1687*z[3] + z[74] + z[87];
    z[87]=z[33] + z[30];
    z[87]=n<T>(1,136)*z[87];
    z[74]=z[74]*z[87];
    z[87]= - n<T>(1,136)*z[32] + n<T>(1,34)*z[29];
    z[80]=z[80]*z[87];
    z[87]=z[28] - z[25];
    z[87]=n<T>(1,2)*z[87];
    z[72]=z[72]*z[87];
    z[87]=122*z[4];
    z[88]= - z[87] + 151*z[3];
    z[78]= - 61*z[5] + 122*z[79] - z[78] - z[88];
    z[78]=z[5]*z[78];
    z[79]=i*z[88];
    z[79]=z[79] + 151*z[84];
    z[79]=z[1]*z[79];
    z[78]=z[79] + z[78];
    z[79]= - 275*z[3] - 61*z[4];
    z[79]=z[4]*z[79];
    z[76]= - 4*z[76] + n<T>(1,17)*z[79];
    z[79]= - 4589*z[3] - 2475*z[4];
    z[79]=n<T>(1,17)*z[79] - n<T>(355,2)*z[7];
    z[79]=z[7]*z[79];
    z[76]=n<T>(331,34)*z[18] + n<T>(869,136)*z[35] + 9*z[76] + z[79] + n<T>(3,17)*
    z[78];
    z[76]=z[18]*z[76];
    z[78]= - n<T>(1363,34)*z[5] + n<T>(1567,34)*z[3] + z[99];
    z[78]=i*z[78];
    z[78]= - 15*z[84] + z[78];
    z[78]=z[38]*z[78];
    z[78]=z[78] + z[44];
    z[79]=n<T>(1005,2)*z[3] + z[86] + n<T>(1813,2)*z[77];
    z[79]=z[27]*z[79];
    z[77]= - 10483*z[7] - n<T>(102409,6)*z[3] + 13435*z[4] + n<T>(11685,2)*z[77]
   ;
    z[77]=z[22]*z[77];
    z[77]=z[77] + z[79];
    z[79]= - n<T>(21,2)*z[46] + z[43] - n<T>(9317,816)*z[41];
    z[79]=i*z[79];
    z[84]=n<T>(12377,6)*z[5] - n<T>(14383,3)*z[7] - n<T>(3967,2)*z[3] - n<T>(28171,3)*
    z[4];
    z[84]=z[37]*z[84];
    z[86]=n<T>(1150127,2)*z[5] + 457799*z[7] - n<T>(302483,2)*z[3] - 28705*z[4];
    z[86]=z[39]*z[86];
    z[87]=77*z[3] + z[87];
    z[87]=n<T>(1,17)*z[87] + 17*z[7];
    z[87]=z[47]*z[87];

    r +=  - n<T>(288649,97920)*z[40] - n<T>(1295,136)*z[42] + 6*z[45] - n<T>(1259,68)*z[48]
     + n<T>(262,17)*z[49] - n<T>(146,51)*z[50]
     + n<T>(87,17)*z[51] - n<T>(166,17)*z[52] - n<T>(410,51)*z[53] - n<T>(703,34)*z[54]
     - 3*z[55]
     - n<T>(3467,68)
      *z[56] + n<T>(12,17)*z[57] + n<T>(1603,68)*z[58] + n<T>(509,51)*z[59] - n<T>(1009,102)*z[60]
     - n<T>(215,34)*z[61]
     + n<T>(59,34)*z[62] - n<T>(559,102)*z[63] - 
      n<T>(183,68)*z[64] + n<T>(759,68)*z[65] - n<T>(15,34)*z[66] + n<T>(244,17)*z[67]
       - n<T>(9,4)*z[68] + n<T>(1,17)*z[69] + n<T>(1,34)*z[70] + n<T>(1,68)*z[71]
     +  z[72] + z[73] + z[74] + z[75] + z[76] + n<T>(1,272)*z[77] + n<T>(1,4)*
      z[78] + z[79] + z[80] + z[81] + z[82] + n<T>(1,2)*z[83] + n<T>(1,136)*
      z[84] + z[85] + n<T>(1,2448)*z[86] + 12*z[87] + z[89];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1251(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1251(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
