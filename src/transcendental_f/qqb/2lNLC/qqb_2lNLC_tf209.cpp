#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf209(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[35];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[2];
    z[7]=d[5];
    z[8]=d[4];
    z[9]=e[2];
    z[10]=e[14];
    z[11]=c[3];
    z[12]=d[7];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[31];
    z[16]=e[3];
    z[17]=e[10];
    z[18]=d[9];
    z[19]=f[4];
    z[20]=f[12];
    z[21]=f[16];
    z[22]=f[51];
    z[23]=f[55];
    z[24]=f[58];
    z[25]=z[1]*i;
    z[26]=z[25] + z[4];
    z[27]=2*z[2];
    z[28]=z[27] - z[26];
    z[28]=z[28]*z[27];
    z[29]=z[25]*z[4];
    z[30]=npow(z[4],2);
    z[31]= - n<T>(1,2)*z[4] + z[27];
    z[31]=z[3]*z[31];
    z[28]=z[31] + z[28] - n<T>(25,2)*z[30] + z[29];
    z[28]=z[3]*z[28];
    z[31]=z[2] - z[4];
    z[31]=4*z[25] - n<T>(17,2)*z[31];
    z[31]=z[2]*z[31];
    z[31]=z[31] + n<T>(13,2)*z[30] + 2*z[29];
    z[31]=z[2]*z[31];
    z[32]= - z[30]*z[25];
    z[33]=npow(z[4],3);
    z[34]= - 11*z[4] - 17*z[2];
    z[34]=z[17]*z[34];
    z[28]=z[34] + z[28] + z[31] - n<T>(13,2)*z[33] + z[32];
    z[31]=n<T>(1,2)*z[7];
    z[32]= - z[31] - z[26];
    z[32]=z[7]*z[32];
    z[33]=n<T>(1,2)*z[6];
    z[34]= - z[4] + z[7];
    z[34]=z[34]*z[33];
    z[32]=z[34] + z[32] + n<T>(3,2)*z[30] + z[29];
    z[32]=z[6]*z[32];
    z[29]=z[29] + n<T>(1,2)*z[30];
    z[30]=z[26]*z[31];
    z[30]=z[30] + z[29];
    z[30]=z[7]*z[30];
    z[30]=z[30] + z[32];
    z[31]=z[3] + 4*z[2];
    z[32]= - z[25]*z[31];
    z[32]= - z[11] + z[32];
    z[32]=z[5]*z[32];
    z[31]=z[25] - z[31];
    z[31]=z[9]*z[31];
    z[31]=z[32] + z[31];
    z[32]= - z[4] - 5*z[25];
    z[27]=n<T>(5,2)*z[3] + n<T>(1,2)*z[32] - z[27];
    z[27]=z[8]*z[27];
    z[26]= - n<T>(1,2)*z[3] + z[26];
    z[26]=z[3]*z[26];
    z[26]=z[27] + z[26] - z[29];
    z[26]=z[8]*z[26];
    z[27]=z[8] + z[2];
    z[29]= - n<T>(1,6)*z[3] + n<T>(25,6)*z[4] - z[27];
    z[29]=z[33] + n<T>(1,3)*z[29] - z[7];
    z[29]=z[11]*z[29];
    z[32]= - z[22] + z[24] - z[23];
    z[25]= - 3*z[18] - 4*z[4] - z[25];
    z[25]=z[10]*z[25];
    z[33]=n<T>(1,6)*z[11] + z[10];
    z[33]=z[12]*z[33];
    z[27]=z[16]*z[27];
    z[34]=z[13]*i;

    r += n<T>(5,3)*z[14] - n<T>(9,2)*z[15] - n<T>(25,6)*z[19] - n<T>(5,2)*z[20] - n<T>(1,2)
      *z[21] + z[25] + z[26] + 2*z[27] + n<T>(1,3)*z[28] + z[29] + 3*z[30]
       + n<T>(2,3)*z[31] + n<T>(3,2)*z[32] + z[33] - n<T>(8,9)*z[34];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf209(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf209(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
