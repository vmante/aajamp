#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf232(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[39];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=d[2];
    z[8]=d[5];
    z[9]=d[4];
    z[10]=d[9];
    z[11]=e[2];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[31];
    z[16]=e[3];
    z[17]=e[10];
    z[18]=e[14];
    z[19]=f[4];
    z[20]=f[12];
    z[21]=f[16];
    z[22]=f[51];
    z[23]=f[55];
    z[24]=f[58];
    z[25]=2*z[2];
    z[26]=z[1]*i;
    z[27]=z[26] + z[5];
    z[28]= - z[2] - z[27];
    z[28]=z[28]*z[25];
    z[29]=z[26]*z[5];
    z[30]=npow(z[5],2);
    z[31]= - n<T>(1,2)*z[5] + z[25];
    z[31]=z[3]*z[31];
    z[28]=z[31] + z[28] + n<T>(11,2)*z[30] + z[29];
    z[28]=z[3]*z[28];
    z[31]=z[30]*z[26];
    z[32]=z[5] + 7*z[2];
    z[32]=z[17]*z[32];
    z[28]= - z[31] + z[28] + z[32] - z[14];
    z[31]=n<T>(1,2)*z[8];
    z[32]=z[31] + z[27];
    z[32]=z[8]*z[32];
    z[33]=z[5] - z[8];
    z[33]=z[7]*z[33];
    z[32]=n<T>(1,2)*z[33] + z[32] - n<T>(3,2)*z[30] - z[29];
    z[32]=z[7]*z[32];
    z[33]=z[25] - z[3];
    z[34]=z[26]*z[33];
    z[34]= - z[12] + z[34];
    z[34]=z[6]*z[34];
    z[33]=z[26] + z[33];
    z[33]=z[11]*z[33];
    z[33]=z[34] + z[33];
    z[34]=2*z[30] + 5*z[29];
    z[35]=n<T>(1,2)*z[2];
    z[36]= - n<T>(7,2)*z[5] - 2*z[26];
    z[36]=n<T>(1,3)*z[36] + z[35];
    z[36]=z[2]*z[36];
    z[34]=n<T>(1,3)*z[34] + z[36];
    z[34]=z[2]*z[34];
    z[29]=z[29] + n<T>(1,2)*z[30];
    z[26]= - z[5] + 3*z[26];
    z[25]= - n<T>(3,2)*z[3] + n<T>(1,2)*z[26] + z[25];
    z[25]=z[9]*z[25];
    z[26]= - n<T>(1,2)*z[3] + z[27];
    z[26]=z[3]*z[26];
    z[25]=z[25] + z[26] - z[29];
    z[25]=z[9]*z[25];
    z[26]= - z[27]*z[31];
    z[26]=z[26] - z[29];
    z[26]=z[8]*z[26];
    z[29]=n<T>(1,2)*z[12] - z[29];
    z[29]=z[10]*z[29];
    z[27]= - n<T>(1,2)*z[4] + z[27];
    z[30]=z[10] - z[2];
    z[27]=z[4]*z[30]*z[27];
    z[30]=npow(z[5],3);
    z[30]= - z[21] + z[30] - z[24] + z[23] + z[22];
    z[31]=z[20] - z[15];
    z[35]=z[8] - n<T>(1,6)*z[3] - n<T>(4,3)*z[5] - z[35];
    z[35]= - n<T>(1,6)*z[7] + z[9] + n<T>(1,3)*z[35];
    z[35]=z[12]*z[35];
    z[36]= - z[2] - z[9];
    z[36]=z[16]*z[36];
    z[37]=z[5] + z[10];
    z[37]=z[18]*z[37];
    z[38]=z[13]*i;

    r += n<T>(11,6)*z[19] + z[25] + z[26] + z[27] + n<T>(1,3)*z[28] + z[29] + n<T>(1,2)*z[30]
     + n<T>(3,2)*z[31]
     + z[32]
     + n<T>(2,3)*z[33]
     + z[34]
     + z[35]
     + 2
      *z[36] + z[37] + n<T>(5,18)*z[38];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf232(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf232(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
