#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1135(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[30];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[19];
    z[11]=c[23];
    z[12]=c[25];
    z[13]=c[26];
    z[14]=c[28];
    z[15]=c[30];
    z[16]=c[31];
    z[17]=f[3];
    z[18]=f[8];
    z[19]=f[11];
    z[20]=f[13];
    z[21]=f[17];
    z[22]=npow(z[2],2);
    z[23]= - z[2] - n<T>(13,4)*z[3];
    z[23]=z[3]*z[23];
    z[24]=n<T>(1,4)*z[3];
    z[25]=z[2] - z[24];
    z[25]=z[4]*z[25];
    z[23]=z[25] - z[22] + z[23];
    z[25]=n<T>(1,2)*z[4];
    z[23]=z[23]*z[25];
    z[22]=n<T>(1,4)*z[22];
    z[26]=z[3]*z[2];
    z[27]=z[22] + 2*z[26];
    z[27]=z[3]*z[27];
    z[28]=z[1]*i;
    z[29]= - n<T>(1,2)*z[28] + z[2] - z[25];
    z[29]=z[7]*z[29];
    z[23]=z[29] + z[27] + z[23];
    z[27]=z[2] + n<T>(13,2)*z[3];
    z[29]=z[27]*z[25];
    z[22]=z[29] + z[22] - 4*z[26];
    z[24]= - z[2] - z[24] + n<T>(5,4)*z[4];
    z[24]=z[24]*z[5];
    z[22]=n<T>(1,3)*z[22] - z[24];
    z[22]=z[22]*z[28];
    z[26]=npow(z[3],2);
    z[29]=z[3] - z[25];
    z[29]=z[4]*z[29];
    z[26]= - n<T>(1,2)*z[26] + z[29];
    z[24]=n<T>(1,2)*z[26] + z[24];
    z[24]=z[5]*z[24];
    z[24]=z[24] - z[15];
    z[25]=z[25] + z[27];
    z[25]=n<T>(1,3)*z[25] - n<T>(5,2)*z[5];
    z[25]=z[9]*z[25];
    z[26]=z[21] - z[12];
    z[27]=n<T>(5,6)*z[13] - n<T>(1,12)*z[11];
    z[27]=i*z[27];
    z[28]= - z[2] + 2*z[28];
    z[29]= - z[3] + z[28];
    z[29]=z[6]*z[29];
    z[28]=z[5] - z[28];
    z[28]=z[8]*z[28];

    r +=  - n<T>(7,144)*z[10] - n<T>(5,12)*z[14] - n<T>(5,4)*z[16] - n<T>(13,24)*z[17]
       + z[18] + n<T>(5,8)*z[19] - n<T>(7,24)*z[20] + z[22] + n<T>(1,3)*z[23] + n<T>(1,2)*z[24]
     + n<T>(1,12)*z[25] - n<T>(1,8)*z[26]
     + z[27]
     + z[28]
     + n<T>(2,3)*
      z[29];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1135(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1135(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
