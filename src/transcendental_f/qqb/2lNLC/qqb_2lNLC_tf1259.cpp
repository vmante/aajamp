#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1259(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[120];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=f[0];
    z[10]=f[3];
    z[11]=f[5];
    z[12]=f[8];
    z[13]=f[11];
    z[14]=f[13];
    z[15]=f[17];
    z[16]=f[20];
    z[17]=c[3];
    z[18]=d[1];
    z[19]=d[4];
    z[20]=d[8];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[18];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[29];
    z[30]=c[30];
    z[31]=c[31];
    z[32]=c[32];
    z[33]=c[34];
    z[34]=c[43];
    z[35]=c[46];
    z[36]=c[47];
    z[37]=c[51];
    z[38]=c[54];
    z[39]=c[55];
    z[40]=c[58];
    z[41]=c[59];
    z[42]=c[60];
    z[43]=c[61];
    z[44]=c[62];
    z[45]=c[66];
    z[46]=c[68];
    z[47]=c[69];
    z[48]=c[72];
    z[49]=c[75];
    z[50]=c[76];
    z[51]=c[78];
    z[52]=c[79];
    z[53]=c[80];
    z[54]=c[83];
    z[55]=g[0];
    z[56]=g[2];
    z[57]=g[3];
    z[58]=g[5];
    z[59]=g[6];
    z[60]=g[7];
    z[61]=g[8];
    z[62]=g[9];
    z[63]=g[10];
    z[64]=g[11];
    z[65]=g[13];
    z[66]=g[15];
    z[67]=g[16];
    z[68]=g[17];
    z[69]=g[18];
    z[70]=g[19];
    z[71]=g[20];
    z[72]=g[21];
    z[73]=g[22];
    z[74]=g[25];
    z[75]=g[26];
    z[76]=g[28];
    z[77]=g[29];
    z[78]=g[30];
    z[79]=g[31];
    z[80]=g[32];
    z[81]=g[33];
    z[82]=g[34];
    z[83]=g[35];
    z[84]=g[36];
    z[85]=g[37];
    z[86]=g[41];
    z[87]=g[43];
    z[88]=g[45];
    z[89]=g[46];
    z[90]=g[49];
    z[91]=g[51];
    z[92]=g[52];
    z[93]=g[54];
    z[94]=g[55];
    z[95]=g[56];
    z[96]=g[57];
    z[97]=g[58];
    z[98]=g[59];
    z[99]=g[61];
    z[100]=g[76];
    z[101]=g[93];
    z[102]=g[94];
    z[103]=g[107];
    z[104]=g[125];
    z[105]= - z[98] - z[92] + z[93] - z[94] - z[97] + z[69] + z[76] + 
    z[84] - z[91];
    z[106]=z[90] + z[56] + z[67] + z[80] + z[89];
    z[107]= - z[85] + z[59] + z[62] + z[74] - z[75];
    z[108]= - z[88] + z[99] - z[100] + z[103] + z[60] + z[70] + z[73] + 
    z[81];
    z[109]=z[63] + z[71] + z[79] - z[82];
    z[110]=z[58] + z[101];
    z[111]= - 5*z[19] + n<T>(17,9) - 25*z[20];
    z[111]=n<T>(1,2)*z[111] + 10*z[18];
    z[111]=z[31]*z[111];
    z[105]=z[111] - n<T>(2407,192)*z[55] + n<T>(395,64)*z[57] - n<T>(3059,64)*z[61]
    + n<T>(21233,576)*z[64] + n<T>(1175,576)*z[65] + n<T>(15,16)*z[66] + n<T>(1207,288)
   *z[68] + n<T>(1513,576)*z[72] - n<T>(47615,1728)*z[77] - n<T>(18443,576)*z[78]
    + n<T>(21161,1728)*z[83] + n<T>(3481,1728)*z[86] - n<T>(763,144)*z[87] - n<T>(203,144)*z[95]
     + n<T>(53,36)*z[96]
     + n<T>(59,96)*z[102]
     + n<T>(2,3)*z[104]
     + n<T>(3,8)*
    z[110] + n<T>(15,32)*z[109] + n<T>(5,16)*z[108] - n<T>(15,8)*z[107] + n<T>(5,8)*
    z[106] - n<T>(5,32)*z[105];
    z[106]=n<T>(1,117)*z[3];
    z[107]=i*z[1];
    z[108]=n<T>(487,8)*z[3] + n<T>(785,6)*z[5] - 2 + n<T>(16339,24)*z[107];
    z[108]=z[108]*z[106];
    z[109]= - n<T>(14819,27)*z[6] - 271*z[7] - n<T>(11413,54)*z[8];
    z[109]=n<T>(1,13)*z[109] - n<T>(137,108)*z[17];
    z[110]=2641*z[107] + n<T>(10073,4)*z[5];
    z[110]=z[5]*z[110];
    z[111]=n<T>(1657,18)*z[4] - 1051*z[3] - n<T>(5845,8)*z[5] + 1 - n<T>(2887,24)*
    z[107];
    z[111]=z[4]*z[111];
    z[108]=n<T>(1,234)*z[111] + z[108] + n<T>(1,1404)*z[110] + n<T>(1,8)*z[109] - n<T>(1,117)*z[107];
    z[108]=z[4]*z[108];
    z[109]= - n<T>(28489,156)*z[17] - n<T>(2141,13)*z[6] + n<T>(977,117)*z[7] - 17*
    z[8];
    z[110]=n<T>(9721,6)*z[107] - 479*z[5];
    z[110]=z[5]*z[110];
    z[111]=n<T>(491,72)*z[3] - n<T>(12115,312)*z[5] + 1 - n<T>(14029,312)*z[107];
    z[111]=z[3]*z[111];
    z[109]=n<T>(1,6)*z[111] + n<T>(1,156)*z[110] + n<T>(1,16)*z[109] - n<T>(1,3)*z[107];
    z[109]=z[3]*z[109];
    z[110]= - 46159*z[107] + n<T>(19777,2)*z[5];
    z[110]=z[5]*z[110];
    z[110]=z[110] + n<T>(102383,2)*z[17] - n<T>(31693,2)*z[6] - 4099*z[7] - 
   18047*z[8];
    z[110]=5*z[107] + n<T>(1,24)*z[110];
    z[111]=n<T>(2501,24)*z[5] + 1 - n<T>(5779,8)*z[107];
    z[111]=n<T>(1,3)*z[111] + n<T>(1871,32)*z[3];
    z[111]=z[3]*z[111];
    z[112]=n<T>(6503,16)*z[4] + n<T>(941,16)*z[3] - 20*z[5] + 1 - n<T>(2693,6)*
    z[107];
    z[112]=z[4]*z[112];
    z[110]=n<T>(1,3)*z[112] + n<T>(1,6)*z[110] + z[111];
    z[111]=n<T>(5585,54)*z[5] - 1 + n<T>(195815,216)*z[107];
    z[111]= - n<T>(52207,2808)*z[2] - n<T>(19337,2808)*z[4] + n<T>(1,13)*z[111] + 
   n<T>(385,108)*z[3];
    z[111]=z[2]*z[111];
    z[110]=n<T>(1,13)*z[110] + n<T>(1,2)*z[111];
    z[110]=z[2]*z[110];
    z[109]=z[109] + z[110];
    z[110]=n<T>(1,2)*z[5];
    z[111]= - n<T>(8833,648)*z[5] - 1 - n<T>(15269,216)*z[107];
    z[111]=z[111]*z[110];
    z[112]= - n<T>(184643,8)*z[17] - 1147*z[6] + n<T>(1595,2)*z[7] - 1163*z[8];
    z[111]=z[111] + n<T>(1,216)*z[112] + z[107];
    z[111]=z[5]*z[111];
    z[112]= - z[8] + n<T>(1,9)*z[7];
    z[113]= - 943*z[30] - n<T>(3061,6)*z[29];
    z[113]=n<T>(3061,36)*z[22] + n<T>(1,2)*z[113] + n<T>(3061,9)*z[23];
    z[113]= - n<T>(12319,324)*z[24] + n<T>(16151,108)*z[26] + n<T>(1,2)*z[113] + 
   n<T>(13363,27)*z[28];
    z[113]= - n<T>(935375,2592)*z[31] - n<T>(125,108)*z[10] - n<T>(803,27)*z[13]
     - 
   n<T>(6281,108)*z[14] - n<T>(371,6)*z[15] - n<T>(1189,12)*z[9] - n<T>(3019,54)*z[11]
    - 123*z[12] + n<T>(1,2)*z[113] + n<T>(157,9)*z[16];
    z[113]=n<T>(1,2)*z[113] - z[112];
    z[114]=n<T>(1,9)*z[6];
    z[113]=n<T>(1,13)*z[113] - z[114];
    z[115]= - 10015*z[25] - n<T>(22967,3)*z[21] - n<T>(69697,2)*z[27];
    z[116]=n<T>(41447,18)*z[6] + n<T>(5065,9)*z[7] + n<T>(2879,2)*z[8];
    z[116]=z[1]*z[116];
    z[115]=n<T>(1,18)*z[115] + z[116];
    z[115]=i*z[115];
    z[108]=z[108] + n<T>(1,13)*z[111] + n<T>(1,156)*z[115] + n<T>(1,2)*z[113] + n<T>(1,351)*z[17]
     + n<T>(1,3)*z[109];
    z[108]=z[2]*z[108];
    z[109]=n<T>(1,3)*z[5];
    z[111]=n<T>(175,24)*z[5] - 1 + n<T>(12929,24)*z[107];
    z[111]=z[111]*z[109];
    z[113]=15*z[8];
    z[115]=n<T>(17939,432)*z[17] + z[113] - n<T>(10307,54)*z[6];
    z[116]= - n<T>(3157,72)*z[3] + n<T>(3157,18)*z[107] - 527*z[5];
    z[116]=z[3]*z[116];
    z[111]=n<T>(1,36)*z[116] + n<T>(1,4)*z[115] + z[111];
    z[111]=z[3]*z[111];
    z[115]= - 119*z[30] - n<T>(137,2)*z[29];
    z[115]=n<T>(137,12)*z[22] + n<T>(1,2)*z[115] + n<T>(137,3)*z[23];
    z[115]=n<T>(19,2)*z[115] + n<T>(3145,3)*z[28];
    z[115]=n<T>(1,13)*z[115] + n<T>(77,4)*z[26];
    z[115]=n<T>(1585,156)*z[16] + n<T>(1,2)*z[115] - n<T>(329,117)*z[24];
    z[115]=n<T>(109189,33696)*z[31] + n<T>(1883,1404)*z[10] - n<T>(493,468)*z[13]
     - 
   n<T>(133,351)*z[14] - n<T>(305,468)*z[15] - n<T>(815,234)*z[9] - n<T>(175,117)*z[11]
    + n<T>(1,9)*z[115] + n<T>(15,26)*z[12];
    z[115]=n<T>(1,2)*z[115] - z[114];
    z[116]= - n<T>(40823,8)*z[21] - 10879*z[27];
    z[116]=n<T>(1,2)*z[116] - 511*z[25];
    z[117]=n<T>(15593,36)*z[6] - n<T>(15965,72)*z[7] + 22*z[8];
    z[117]=z[1]*z[117];
    z[116]=n<T>(1,72)*z[116] + z[117];
    z[116]=i*z[116];
    z[117]=n<T>(1007,24)*z[5] - 1 + n<T>(8807,24)*z[107];
    z[117]=z[5]*z[117];
    z[117]=n<T>(1,52)*z[117] + n<T>(1,26)*z[107] + n<T>(51067,2808)*z[17] - n<T>(1579,936)*z[6]
     + n<T>(133,24)*z[7] - n<T>(11,13)*z[8];
    z[117]=z[117]*z[109];
    z[111]=n<T>(1,52)*z[111] + z[117] + n<T>(1,39)*z[116] + n<T>(1,2)*z[115] + n<T>(2,351)*z[17];
    z[111]=z[3]*z[111];
    z[115]=1 - n<T>(2299,9)*z[107];
    z[115]=n<T>(1,13)*z[115] - n<T>(107,8)*z[5];
    z[109]=z[115]*z[109];
    z[115]= - n<T>(6121,24)*z[5] - 23 - n<T>(10729,24)*z[107];
    z[115]=n<T>(1,2)*z[115] - n<T>(205,9)*z[3];
    z[106]=z[115]*z[106];
    z[115]=n<T>(1583,27)*z[7] - n<T>(15,13)*z[8];
    z[115]= - n<T>(10403,936)*z[17] + n<T>(1,2)*z[115] - n<T>(14819,351)*z[6];
    z[106]=z[106] + z[109] + n<T>(1,4)*z[115] + n<T>(23,117)*z[107];
    z[106]=z[3]*z[106];
    z[109]=n<T>(3583,27)*z[7] + z[113];
    z[109]= - n<T>(41815,864)*z[17] + n<T>(1,8)*z[109] - z[6];
    z[113]= - n<T>(1957,468)*z[5] - n<T>(1,13) + n<T>(259,8)*z[107];
    z[113]=z[5]*z[113];
    z[115]=n<T>(2381,8)*z[3] + n<T>(43271,24)*z[5] + 1 - n<T>(24529,24)*z[107];
    z[115]=z[3]*z[115];
    z[116]= - n<T>(23975,2)*z[107] - 7669*z[5];
    z[116]=n<T>(23975,104)*z[4] + n<T>(1,13)*z[116] + 335*z[3];
    z[116]=z[4]*z[116];
    z[109]=n<T>(1,324)*z[116] + n<T>(1,234)*z[115] + n<T>(1,13)*z[109] + n<T>(1,6)*
    z[113];
    z[109]=z[4]*z[109];
    z[106]=z[106] + z[109];
    z[109]=n<T>(1379,16)*z[7] - 688*z[8];
    z[113]=n<T>(8603,216)*z[5] + 7 + n<T>(4117,72)*z[107];
    z[113]=z[5]*z[113];
    z[109]=n<T>(1,12)*z[113] - n<T>(7,6)*z[107] + n<T>(56873,1728)*z[17] + n<T>(1,27)*
    z[109] + z[6];
    z[109]=z[5]*z[109];
    z[113]= - n<T>(6485,4)*z[25] + n<T>(2123,3)*z[21] - n<T>(37171,4)*z[27];
    z[115]=n<T>(14819,36)*z[6] - n<T>(805,8)*z[7] + n<T>(1376,9)*z[8];
    z[115]=z[1]*z[115];
    z[113]=n<T>(1,18)*z[113] + z[115];
    z[115]=n<T>(1,3)*i;
    z[113]=z[113]*z[115];
    z[115]= - n<T>(1181,3)*z[30] - n<T>(645,2)*z[29];
    z[115]=n<T>(22181,27)*z[28] + n<T>(215,4)*z[22] + n<T>(1,2)*z[115] + 215*z[23];
    z[115]=n<T>(15,2)*z[12] + n<T>(1367,18)*z[16] - n<T>(15395,648)*z[24] + n<T>(1,2)*
    z[115] + n<T>(2963,27)*z[26];
    z[109]=z[109] + z[113] + n<T>(1,54)*z[17] - n<T>(1,18)*z[7] + n<T>(349543,1296)*
    z[31] + n<T>(5333,432)*z[10] - n<T>(1669,432)*z[13] - n<T>(3283,432)*z[14] - 
   n<T>(3383,144)*z[15] - n<T>(1367,24)*z[9] + n<T>(1,4)*z[115] - n<T>(757,27)*z[11];
    z[106]=n<T>(1,13)*z[109] + n<T>(1,2)*z[106];
    z[106]=z[4]*z[106];
    z[109]=n<T>(35335,96)*z[21] + 2029*z[27];
    z[109]=5*z[109] + n<T>(17701,4)*z[25];
    z[113]= - n<T>(1487,3)*z[7] - 2519*z[8];
    z[113]=n<T>(1,2)*z[113] + n<T>(1579,3)*z[6];
    z[113]=z[1]*z[113];
    z[109]=n<T>(1,3)*z[109] + z[113];
    z[109]=i*z[109];
    z[113]= - 7789*z[30] - n<T>(3463,2)*z[29];
    z[113]=n<T>(3463,12)*z[22] + n<T>(1,2)*z[113] + n<T>(3463,3)*z[23];
    z[113]= - n<T>(2447,4)*z[26] + n<T>(1,2)*z[113] - n<T>(6265,3)*z[28];
    z[113]=n<T>(1873,48)*z[16] + n<T>(1,4)*z[113] - n<T>(14,9)*z[24];
    z[109]=n<T>(1,36)*z[109] - n<T>(1,12)*z[17] + n<T>(1,2)*z[8] + n<T>(983009,5184)*
    z[31] + n<T>(3593,864)*z[10] + n<T>(9755,864)*z[13] + n<T>(305,54)*z[14] + n<T>(1451,288)*z[15]
     - n<T>(2449,144)*z[9]
     - n<T>(361,54)*z[11]
     + n<T>(1,9)*z[113]
     + 42*
    z[12];
    z[113]= - n<T>(133,8)*z[7] + n<T>(1877,39)*z[8];
    z[107]= - z[107] + n<T>(1,4)*z[5];
    z[107]=z[5]*z[107];
    z[107]=n<T>(43379,16848)*z[107] - n<T>(622487,134784)*z[17] + n<T>(1,9)*z[113]
    - n<T>(3,13)*z[6];
    z[107]=z[107]*z[110];
    z[107]=n<T>(1,13)*z[109] + z[107];
    z[107]=z[5]*z[107];
    z[109]=npow(z[20],2);
    z[110]=npow(z[19],2);
    z[109]=5*z[109] + z[110];
    z[110]=npow(z[18],2);
    z[109]= - n<T>(12329,54)*z[7] + n<T>(1,4)*z[109] - z[110];
    z[109]=n<T>(140909,468)*z[6] + n<T>(5,26)*z[109] + n<T>(2407,27)*z[8];
    z[109]=z[17]*z[109];
    z[110]= - n<T>(1975,96)*z[10] + n<T>(2965,864)*z[13] + n<T>(319,18)*z[14] + n<T>(1153,32)*z[15]
     + n<T>(1981,18)*z[9]
     + n<T>(11569,216)*z[11] - n<T>(1693,54)*z[16] - 
   15*z[12] + z[112];
    z[110]=n<T>(1,13)*z[110] + z[114];
    z[110]=z[1]*z[110];
    z[112]= - 85279*z[45] - n<T>(135725,3)*z[41];
    z[112]=n<T>(1,2)*z[112] + n<T>(22967,3)*z[36];
    z[112]= - n<T>(5,2)*z[25] + n<T>(1,24)*z[112] - 5*z[27];
    z[110]=n<T>(1,117)*z[112] + z[110];
    z[110]=i*z[110];
    z[112]=z[40] + z[53];
    z[113]=z[14] - z[26];
    z[114]=npow(z[20],4);
    z[115]=npow(z[19],4);
    z[116]=npow(z[18],4);
    z[117]=npow(z[7],2);
    z[118]=npow(z[8],2);
    z[119]=npow(z[6],2);

    r +=  - n<T>(23,468)*z[10] + n<T>(7,156)*z[13] - n<T>(1,156)*z[15] - n<T>(5,2808)*
      z[24] + n<T>(5,234)*z[28] + n<T>(109,37440)*z[32] + n<T>(5,117)*z[33] - n<T>(19,156)*z[34]
     + n<T>(19,39)*z[35] - n<T>(1309,7488)*z[37] - n<T>(5053,1872)*
      z[38] - n<T>(74585,8424)*z[39] - n<T>(1,6)*z[42] + n<T>(4637,2592)*z[43] - 
      n<T>(81197,3744)*z[44] + n<T>(8807,3744)*z[46] + n<T>(5663,7488)*z[47] + n<T>(3805,2496)*z[48]
     + n<T>(943,416)*z[49]
     + n<T>(19,52)*z[50] - n<T>(28,13)*z[51]
     +  z[52] + n<T>(70019,10368)*z[54] + n<T>(1,13)*z[105] + z[106] + z[107] + 
      z[108] + n<T>(1,16)*z[109] + z[110] + z[111] + n<T>(1,2)*z[112] - n<T>(5,468)
      *z[113] - n<T>(25,832)*z[114] - n<T>(5,832)*z[115] + n<T>(5,208)*z[116] + n<T>(751,1872)*z[117]
     - n<T>(69,104)*z[118]
     - n<T>(123,832)*z[119];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1259(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1259(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
