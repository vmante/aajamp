#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf862(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[76];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=d[9];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[51];
    z[52]=f[55];
    z[53]=f[58];
    z[54]=z[11] - z[12];
    z[55]=3*z[7];
    z[56]=3*z[10];
    z[57]=3*z[4] - z[55] + z[9] - z[56] + z[54];
    z[57]=z[4]*z[57];
    z[58]=n<T>(1,1531)*z[3];
    z[59]=7345*z[8] + n<T>(2151,4)*z[3];
    z[59]=z[59]*z[58];
    z[60]=n<T>(1,1531)*z[9];
    z[61]= - 2563*z[10] + n<T>(16393,3)*z[13];
    z[61]=8*z[61];
    z[62]= - z[61] - n<T>(1229095,24)*z[9];
    z[62]=z[62]*z[60];
    z[63]=n<T>(145059,6124)*z[9];
    z[64]=n<T>(13469,12248)*z[7] + z[63] + z[56] - n<T>(22190,1531)*z[3];
    z[64]=z[7]*z[64];
    z[63]=z[63] + n<T>(68547,6124)*z[7];
    z[65]=n<T>(51105,1531)*z[8] - z[10];
    z[65]= - 2*z[5] + n<T>(347695,9186)*z[2] - n<T>(7035,3062)*z[4] + n<T>(1,2)*
    z[65] + n<T>(19128,1531)*z[3] - z[63];
    z[65]=z[5]*z[65];
    z[66]=z[18] - z[22];
    z[67]=npow(z[10],2);
    z[68]=143857*z[14] - n<T>(153043,2)*z[13];
    z[68]=z[13]*z[68];
    z[69]=n<T>(7,2)*z[7];
    z[70]= - n<T>(188528,4593)*z[2] + n<T>(23256,1531)*z[4] - n<T>(14845,1531)*z[3]
    - z[69];
    z[70]=z[2]*z[70];
    z[71]=n<T>(88741,2)*z[14] + 135737*z[13];
    z[71]= - n<T>(78353,2)*z[6] - n<T>(232927,6)*z[5] + n<T>(198644,3)*z[2] - n<T>(42539,2)*z[4]
     + n<T>(69632,3)*z[9]
     + n<T>(18973,2)*z[3]
     + n<T>(1,3)*z[71] - 20504*
    z[10];
    z[71]=z[6]*z[71];
    z[57]=n<T>(1,1531)*z[71] + z[65] + z[70] + z[57] + z[64] + z[62] + z[59]
    + n<T>(67035,6124)*z[67] + n<T>(1,9186)*z[68] - n<T>(7190,1531)*z[21] - n<T>(26318,1531)*z[15]
     - n<T>(127016,4593)*z[16]
     - n<T>(51105,3062)*z[17]
     - n<T>(180272,4593)*z[19]
     - n<T>(88741,9186)*z[20]
     - z[66];
    z[57]=z[1]*z[57];
    z[59]=193121*z[30] + 94264*z[28];
    z[59]=2*z[59] + n<T>(107249,12)*z[25];
    z[57]=n<T>(1,4593)*z[59] + z[57];
    z[57]=i*z[57];
    z[59]=6376*z[3] - n<T>(48353,4)*z[9];
    z[59]=3*z[59] - n<T>(44089,8)*z[7];
    z[59]=z[7]*z[59];
    z[59]=z[59] - n<T>(6839,72)*z[23] + n<T>(51105,2)*z[17] + n<T>(63508,3)*z[16];
    z[62]=npow(z[9],2);
    z[64]=npow(z[3],2);
    z[65]=n<T>(13159,6124)*z[4] + z[24] - z[9];
    z[65]=z[4]*z[65];
    z[68]=2*z[24];
    z[70]= - z[13] + z[68] - z[7];
    z[71]= - n<T>(31754,4593)*z[2] + n<T>(17752,1531)*z[4] + n<T>(34283,3062)*z[3]
    - z[10] - z[70];
    z[71]=z[2]*z[71];
    z[63]=n<T>(1221,1531)*z[4] - n<T>(60291,1531)*z[3] + z[24] - n<T>(1,2)*z[13]
     + 
    z[63];
    z[63]=n<T>(1,3)*z[5] + n<T>(1,2)*z[63] - n<T>(58915,4593)*z[2];
    z[63]=z[5]*z[63];
    z[59]=z[63] + z[71] + z[65] + n<T>(175679,12248)*z[62] - n<T>(7345,3062)*
    z[64] - n<T>(1,4)*z[67] + n<T>(1,1531)*z[59];
    z[59]=z[5]*z[59];
    z[63]=npow(z[13],2);
    z[65]=n<T>(88741,2)*z[20] + 90136*z[19];
    z[61]=z[61] + n<T>(341843,12)*z[9];
    z[61]=z[9]*z[61];
    z[61]=z[61] + 3595*z[64] - n<T>(36415,4)*z[67] - n<T>(134671,12)*z[63] - 
   n<T>(644731,18)*z[23] + n<T>(1,3)*z[65] + 7190*z[21];
    z[65]=n<T>(1,2)*z[3];
    z[71]=n<T>(3,2)*z[10];
    z[68]= - n<T>(45068,4593)*z[2] + 4*z[7] - z[65] + z[68] - z[71];
    z[68]=z[2]*z[68];
    z[72]=n<T>(3,2)*z[7];
    z[73]=z[3] - n<T>(1,2)*z[7];
    z[73]=z[73]*z[72];
    z[74]=2*z[7];
    z[75]= - n<T>(45601,3)*z[13] + 5126*z[10];
    z[75]= - n<T>(74225,6)*z[9] + 2*z[75] - 3595*z[3];
    z[75]=n<T>(78353,18372)*z[6] + n<T>(104981,9186)*z[5] - n<T>(125485,18372)*z[2]
    + n<T>(25097,3062)*z[4] + n<T>(1,1531)*z[75] - z[74];
    z[75]=z[6]*z[75];
    z[70]=n<T>(36415,6124)*z[4] - z[70];
    z[70]=z[4]*z[70];
    z[56]= - z[13] + z[56];
    z[56]=n<T>(123353,9186)*z[5] - n<T>(91531,9186)*z[2] - n<T>(22035,1531)*z[4]
     - 
    z[7] + z[9] + n<T>(1,2)*z[56] - z[3];
    z[56]=z[5]*z[56];
    z[56]=z[75] + z[56] + z[68] + z[70] + n<T>(1,1531)*z[61] + z[73];
    z[56]=z[6]*z[56];
    z[61]= - n<T>(174167,9186)*z[23] - 13*z[37] + n<T>(33663,1531)*z[21];
    z[68]=z[10] - n<T>(9564,1531)*z[3];
    z[68]= - n<T>(7,3)*z[7] + 2*z[68] + n<T>(145059,12248)*z[9];
    z[68]=z[7]*z[68];
    z[61]=z[68] + n<T>(97927,12248)*z[62] + n<T>(5213,6124)*z[64] + n<T>(1,2)*z[61]
    + z[67];
    z[61]=z[7]*z[61];
    z[68]=22534*z[19] + 15877*z[16];
    z[68]= - n<T>(31221,4)*z[64] + n<T>(515833,36)*z[23] + n<T>(4,3)*z[68] + 13159*
    z[15];
    z[65]=z[65] - z[74];
    z[65]=z[7]*z[65];
    z[70]= - n<T>(14690,1531)*z[4] - z[72] - z[13] + z[71];
    z[70]=z[4]*z[70];
    z[72]=z[10] - z[3];
    z[72]=n<T>(94264,4593)*z[2] + n<T>(1,2)*z[72] - n<T>(5814,1531)*z[4];
    z[72]=z[2]*z[72];
    z[62]=z[72] + z[70] + z[65] + n<T>(1,1531)*z[68] + n<T>(1,2)*z[62];
    z[62]=z[2]*z[62];
    z[65]= - z[10] + n<T>(7,4)*z[7];
    z[55]=z[65]*z[55];
    z[65]= - z[4] + z[69] + z[24] + z[71];
    z[65]=z[4]*z[65];
    z[55]=n<T>(1,2)*z[65] + z[55] - n<T>(19,4)*z[67] + n<T>(9341,4593)*z[23] + n<T>(13159,1531)*z[15]
     + z[66];
    z[55]=z[4]*z[55];
    z[65]=z[53] - n<T>(7,2)*z[52];
    z[66]=n<T>(13,6)*z[67] - n<T>(95795,9186)*z[23] + 5*z[36] - z[35];
    z[66]=z[10]*z[66];
    z[65]= - z[39] + z[66] - n<T>(36415,3062)*z[48] - n<T>(341843,9186)*z[49] + 
   n<T>(25097,1531)*z[50] + 3*z[65] - n<T>(19,2)*z[51];
    z[64]=n<T>(255835,12)*z[64] - n<T>(25097,4)*z[23] - 10562*z[21] + n<T>(69167,2)*
    z[33] + 7345*z[17];
    z[58]=z[64]*z[58];
    z[64]= - n<T>(5044,3)*z[13] + n<T>(1695,2)*z[10];
    z[64]= - n<T>(57917,3)*z[9] + 13*z[64] - n<T>(136803,4)*z[3];
    z[64]=z[9]*z[64];
    z[64]=z[64] + 11783*z[67] - n<T>(195854,9)*z[23] - n<T>(117365,3)*z[34] + 
   33818*z[33];
    z[60]=z[64]*z[60];
    z[64]=z[29] - z[42];
    z[66]= - 7*z[37] + 4*z[36];
    z[66]=z[24]*z[66];
    z[54]= - n<T>(7190,4593)*z[24] + n<T>(51105,3062)*z[8] + n<T>(88741,9186)*z[14]
    + z[54];
    z[54]=z[23]*z[54];
    z[63]=n<T>(122423,18372)*z[63] + n<T>(46221,6124)*z[23] + n<T>(143857,9186)*
    z[20] - 4*z[35] - n<T>(257695,9186)*z[34];
    z[63]=z[13]*z[63];

    r +=  - n<T>(429983,18372)*z[26] + n<T>(80485,27558)*z[27] - n<T>(193121,4593)*
      z[31] + n<T>(1242741,12248)*z[32] + z[38] + n<T>(7035,6124)*z[40] - n<T>(44089,12248)*z[41]
     + n<T>(232927,18372)*z[43] - n<T>(175679,12248)*z[44]
     +  n<T>(97927,12248)*z[45] + n<T>(42539,6124)*z[46] + n<T>(1,3)*z[47] + z[54]
     +  z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + 
      z[63] - n<T>(94264,4593)*z[64] + n<T>(1,2)*z[65] + z[66];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf862(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf862(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
