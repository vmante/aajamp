#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf757(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[71];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[16];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[4];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=e[10];
    z[21]=c[3];
    z[22]=d[9];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[3];
    z[32]=e[6];
    z[33]=e[13];
    z[34]=e[7];
    z[35]=e[14];
    z[36]=f[0];
    z[37]=f[1];
    z[38]=f[3];
    z[39]=f[4];
    z[40]=f[5];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[16];
    z[44]=f[17];
    z[45]=f[18];
    z[46]=f[31];
    z[47]=f[36];
    z[48]=f[39];
    z[49]=f[51];
    z[50]=f[55];
    z[51]=f[58];
    z[52]=z[9] - z[11];
    z[53]=n<T>(1,2)*z[4];
    z[54]=n<T>(1,2)*z[10];
    z[55]=n<T>(1,2)*z[7];
    z[52]=z[53] - z[55] - z[54] + 3*z[52];
    z[52]=z[4]*z[52];
    z[56]=n<T>(405683,18372)*z[7] + n<T>(1307,3062)*z[9] + z[10] + n<T>(93551,4593)*
    z[3];
    z[56]=z[56]*z[55];
    z[57]=n<T>(445009,9186)*z[2];
    z[58]=n<T>(7687,1531)*z[3] + 5*z[7];
    z[58]= - z[57] + n<T>(1,2)*z[58] + n<T>(169813,4593)*z[4];
    z[58]=z[2]*z[58];
    z[59]=3*z[17];
    z[60]=npow(z[10],2);
    z[61]=n<T>(1815,2)*z[19];
    z[62]= - z[61] - n<T>(27526,3)*z[18];
    z[63]=28015*z[13] - n<T>(31077,2)*z[12];
    z[63]=z[12]*z[63];
    z[64]= - 53617*z[8] + n<T>(75083,4)*z[3];
    z[64]=z[3]*z[64];
    z[65]= - z[11] - n<T>(4068,1531)*z[12];
    z[65]= - n<T>(168355,12248)*z[9] + 4*z[65] + n<T>(4529,3062)*z[10];
    z[65]=z[9]*z[65];
    z[66]= - 12376*z[8] - n<T>(102737,2)*z[3];
    z[66]=n<T>(206269,6)*z[2] - n<T>(45898,3)*z[4] - n<T>(209395,12)*z[7] + n<T>(1,3)*
    z[66] - n<T>(1307,4)*z[9];
    z[66]=n<T>(1,1531)*z[66] + 14*z[5];
    z[66]=z[5]*z[66];
    z[67]=n<T>(4529,2)*z[10];
    z[68]= - n<T>(35117,2)*z[6] - n<T>(68767,3)*z[5] + n<T>(362399,6)*z[2] - 41305*
    z[4] + n<T>(28015,2)*z[9] + 3046*z[3] - z[67] + n<T>(12705,2)*z[13] + 17803*
    z[12];
    z[68]=z[6]*z[68];
    z[52]=n<T>(1,1531)*z[68] + z[66] + z[58] + z[52] + z[56] + z[65] + n<T>(1,4593)*z[64]
     + n<T>(4577,1531)*z[60]
     + n<T>(1,3062)*z[63] - n<T>(1515,1531)*z[20]
    - n<T>(201964,4593)*z[14] - n<T>(114665,4593)*z[15] + n<T>(12376,4593)*z[16]
     + 
   n<T>(7,1531)*z[62] - z[59];
    z[52]=z[1]*z[52];
    z[56]=n<T>(40921,24)*z[23] + 509311*z[28] + n<T>(445009,2)*z[26];
    z[52]=n<T>(1,4593)*z[56] + z[52];
    z[52]=i*z[52];
    z[56]=npow(z[12],2);
    z[58]=npow(z[3],2);
    z[61]=z[61] + n<T>(13763,3)*z[18];
    z[62]=n<T>(48311,4)*z[9] + 16272*z[12] - z[67];
    z[62]=z[9]*z[62];
    z[61]=z[62] + n<T>(1515,2)*z[58] - n<T>(7591,4)*z[60] - n<T>(24953,4)*z[56] - 
   n<T>(337567,18)*z[21] + 7*z[61] + 1515*z[20];
    z[62]=n<T>(7,2)*z[7];
    z[63]= - 7339*z[12] + 647*z[10];
    z[63]= - n<T>(31077,2)*z[9] + n<T>(7,2)*z[63] - 1515*z[3];
    z[63]=n<T>(35117,9186)*z[6] + n<T>(59581,4593)*z[5] - n<T>(280013,9186)*z[2]
     + 
   n<T>(44367,1531)*z[4] + n<T>(1,1531)*z[63] + z[62];
    z[63]=z[6]*z[63];
    z[64]=z[3] - z[55];
    z[64]=z[7]*z[64];
    z[65]=2*z[22];
    z[66]=n<T>(38243,3062)*z[4] + z[9] - z[65] + z[12];
    z[66]=z[4]*z[66];
    z[67]=z[10] - z[22];
    z[68]= - n<T>(96341,9186)*z[2] - 2*z[67] - n<T>(5,2)*z[7];
    z[68]=z[2]*z[68];
    z[69]=2*z[10];
    z[70]=n<T>(77953,9186)*z[5] + n<T>(55148,4593)*z[2] - n<T>(41305,1531)*z[4] - 
    z[7] + z[69] - z[3];
    z[70]=z[5]*z[70];
    z[61]=n<T>(1,2)*z[63] + z[70] + z[68] + z[66] + n<T>(1,1531)*z[61] + z[64];
    z[61]=z[6]*z[61];
    z[62]= - z[10] + z[62];
    z[62]=z[62]*z[55];
    z[63]=n<T>(7,3)*z[4] + n<T>(3,2)*z[7] + z[22] - z[54];
    z[53]=z[63]*z[53];
    z[63]=npow(z[9],2);
    z[53]=z[53] + z[62] - 4*z[63] - n<T>(7,4)*z[60] - n<T>(41273,55116)*z[21] + 
    z[59] + n<T>(100982,4593)*z[14];
    z[53]=z[4]*z[53];
    z[59]= - n<T>(64382,3)*z[21] + 100982*z[14] + 96341*z[18] + n<T>(114665,2)*
    z[15];
    z[59]=n<T>(1,3)*z[59] + n<T>(7687,4)*z[58];
    z[62]=z[7] + z[12];
    z[64]= - n<T>(178999,9186)*z[4] + z[69] - z[62];
    z[64]=z[4]*z[64];
    z[66]=npow(z[7],2);
    z[57]=z[57] - n<T>(68831,4593)*z[4] + z[10] - z[3];
    z[57]=z[2]*z[57];
    z[57]=n<T>(1,2)*z[57] + z[64] + n<T>(5,4)*z[66] + n<T>(1,1531)*z[59] + n<T>(7,2)*
    z[63];
    z[57]=z[2]*z[57];
    z[59]=n<T>(1307,6124)*z[9];
    z[54]= - z[7] + z[59] + z[54] + n<T>(102737,4593)*z[3];
    z[54]=z[7]*z[54];
    z[54]=z[54] - n<T>(17001,6124)*z[63] + n<T>(81175,4593)*z[58] - n<T>(3,2)*z[60]
    - n<T>(16585,13779)*z[21] - z[35] - n<T>(121109,4593)*z[20];
    z[54]=z[54]*z[55];
    z[64]= - 728*z[16] + n<T>(6745,2)*z[15];
    z[64]=n<T>(53617,2)*z[58] + 17*z[64] + n<T>(1229105,24)*z[21];
    z[66]= - n<T>(497543,12)*z[7] - n<T>(102737,3)*z[3] - n<T>(1307,2)*z[9];
    z[55]=z[66]*z[55];
    z[55]=z[55] + n<T>(1,3)*z[64] + n<T>(77857,8)*z[63];
    z[62]= - n<T>(114665,18372)*z[2] + n<T>(78017,4593)*z[4] - n<T>(4625,3062)*z[3]
    - z[10] - z[65] + z[62];
    z[62]=z[2]*z[62];
    z[63]=n<T>(27542,4593)*z[4] - z[67];
    z[63]=z[4]*z[63];
    z[59]=n<T>(209395,18372)*z[7] + z[59] + n<T>(75275,4593)*z[3] + z[22] - 
    z[12];
    z[59]= - n<T>(7,3)*z[5] - n<T>(178967,9186)*z[2] + n<T>(1,2)*z[59] + n<T>(18356,4593)
   *z[4];
    z[59]=z[5]*z[59];
    z[55]=z[59] + z[62] + n<T>(1,1531)*z[55] + z[63];
    z[55]=z[5]*z[55];
    z[59]= - 2712*z[12] + n<T>(3551,4)*z[10];
    z[59]= - 3543*z[9] + 3*z[59] - n<T>(16745,2)*z[3];
    z[59]=z[9]*z[59];
    z[59]=z[59] - 14741*z[32] + n<T>(16745,2)*z[31];
    z[59]=n<T>(1,2)*z[58] + n<T>(10653,6124)*z[60] - n<T>(8569,4593)*z[21] + 4*z[17]
    + n<T>(1,1531)*z[59];
    z[59]=z[9]*z[59];
    z[62]=z[27] - z[40];
    z[63]=z[49] + z[50];
    z[63]= - n<T>(7591,1531)*z[46] - n<T>(48311,1531)*z[47] + n<T>(16777,1531)*z[48]
    - 3*z[51] - 7*z[63];
    z[64]= - n<T>(1,2)*z[35] + z[33];
    z[64]=z[22]*z[64];
    z[65]=n<T>(4235,3062)*z[13] - z[11];
    z[65]= - n<T>(1499,9186)*z[22] + 3*z[65] - n<T>(12376,4593)*z[8];
    z[65]=z[21]*z[65];
    z[56]=n<T>(21891,6124)*z[56] + n<T>(9675,3062)*z[21] + n<T>(28015,3062)*z[19]
     - 
   3*z[34] - n<T>(17803,1531)*z[32];
    z[56]=z[12]*z[56];
    z[60]=z[60] + 2*z[33] - n<T>(489,3062)*z[21];
    z[60]=z[10]*z[60];
    z[58]= - 17386*z[58] - n<T>(10685,4)*z[21] - n<T>(81127,3)*z[20] + n<T>(13683,2)
   *z[31] - n<T>(53617,3)*z[16];
    z[58]=z[3]*z[58];

    r += n<T>(18532,4593)*z[24] + n<T>(59197,110232)*z[25] - n<T>(509311,9186)*
      z[29] + n<T>(10703,6124)*z[30] + 7*z[36] - n<T>(7,2)*z[37] + n<T>(22949,4593)
      *z[38] - n<T>(497543,36744)*z[39] + n<T>(68767,9186)*z[41] - n<T>(77857,12248)
      *z[42] - n<T>(17001,12248)*z[43] + n<T>(41305,3062)*z[44] + n<T>(7,3)*z[45]
       + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + n<T>(1,1531)*z[58]
       + z[59] + z[60] + z[61] - n<T>(445009,18372)*z[62] + n<T>(1,4)*z[63] + 3
      *z[64] + z[65];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf757(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf757(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
