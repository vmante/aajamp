#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf318(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[74];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=d[16];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[3];
    z[33]=e[6];
    z[34]=e[14];
    z[35]=e[13];
    z[36]=f[0];
    z[37]=f[1];
    z[38]=f[3];
    z[39]=f[4];
    z[40]=f[5];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[16];
    z[44]=f[18];
    z[45]=f[20];
    z[46]=f[23];
    z[47]=f[31];
    z[48]=f[36];
    z[49]=f[39];
    z[50]=f[51];
    z[51]=f[55];
    z[52]=f[58];
    z[53]=n<T>(139,18)*z[11];
    z[54]=n<T>(1,2)*z[8];
    z[55]=n<T>(164,3)*z[14] + n<T>(61,2)*z[13];
    z[55]= - n<T>(2491,270)*z[2] - n<T>(53,45)*z[10] + n<T>(38,9)*z[3] - z[53] + n<T>(1,15)*z[55]
     - z[54];
    z[55]=z[6]*z[55];
    z[56]=n<T>(71,5)*z[11];
    z[57]=n<T>(1,2)*z[4];
    z[58]= - z[57] - z[12] - z[56];
    z[58]=z[58]*z[57];
    z[59]=z[4] - z[12];
    z[60]= - z[13] + n<T>(703,90)*z[11];
    z[59]= - n<T>(1691,720)*z[10] + z[60] + 2*z[59];
    z[59]=z[10]*z[59];
    z[61]=n<T>(64,15)*z[9];
    z[62]=n<T>(469,40)*z[10];
    z[63]=n<T>(1178,135)*z[6] - n<T>(1337,540)*z[2] - z[62] + n<T>(583,60)*z[3] + 
    z[61] - n<T>(11,2)*z[4];
    z[63]=z[5]*z[63];
    z[64]=npow(z[8],2);
    z[65]=n<T>(1,4)*z[64];
    z[66]=npow(z[11],2);
    z[67]=n<T>(589,3)*z[20];
    z[68]= - 82*z[21] + z[67];
    z[69]=z[13]*z[14];
    z[70]= - 671*z[9] + n<T>(1751,3)*z[3];
    z[70]=z[3]*z[70];
    z[71]=11*z[4];
    z[72]=z[8] + z[71];
    z[72]=n<T>(2417,540)*z[2] + n<T>(1,2)*z[72] - n<T>(349,45)*z[3];
    z[72]=z[2]*z[72];
    z[73]=n<T>(61,5)*z[4] - z[8] + n<T>(81,5)*z[11];
    z[73]= - n<T>(4189,240)*z[7] - n<T>(361,120)*z[5] + n<T>(1261,180)*z[2] + n<T>(509,40)*z[10]
     + n<T>(1,2)*z[73] - n<T>(578,45)*z[3];
    z[73]=z[7]*z[73];
    z[55]=z[73] + z[63] + z[55] + z[72] + z[59] + n<T>(1,60)*z[70] + z[58]
    + n<T>(17,2)*z[66] + z[65] - n<T>(31,30)*z[69] + n<T>(83,90)*z[22] - n<T>(4,45)*
    z[19] - 8*z[15] + n<T>(871,270)*z[16] - n<T>(64,15)*z[17] + n<T>(2,45)*z[68]
     - n<T>(1,2)*z[18];
    z[55]=z[1]*z[55];
    z[58]= - n<T>(1601,2)*z[24] - 1067*z[29] - n<T>(2417,2)*z[27];
    z[55]=n<T>(1,270)*z[58] + z[55];
    z[55]=i*z[55];
    z[58]=npow(z[3],2);
    z[59]= - n<T>(2707,108)*z[23] - 101*z[34] + n<T>(1253,9)*z[22];
    z[63]= - z[13] + n<T>(91,20)*z[11];
    z[63]=z[11]*z[63];
    z[68]= - 27*z[11] - z[71];
    z[68]=z[4]*z[68];
    z[69]= - z[4] + n<T>(863,240)*z[10];
    z[69]=z[10]*z[69];
    z[70]= - n<T>(29,36)*z[2] + z[8] + n<T>(47,10)*z[3];
    z[70]=z[2]*z[70];
    z[57]= - n<T>(67,20)*z[2] + z[57] + n<T>(28,9)*z[3];
    z[57]=z[6]*z[57];
    z[71]=z[6] - z[2];
    z[71]=n<T>(361,12)*z[5] + n<T>(443,3)*z[3] - n<T>(469,2)*z[10] + 57*z[71];
    z[71]=z[5]*z[71];
    z[72]=n<T>(119,5)*z[4] + n<T>(91,10)*z[11] + z[13] - z[54];
    z[72]= - n<T>(62,9)*z[7] + n<T>(3623,720)*z[5] - n<T>(3,4)*z[6] + n<T>(229,180)*z[2]
    + n<T>(1727,240)*z[10] + n<T>(1,2)*z[72] - n<T>(866,45)*z[3];
    z[72]=z[7]*z[72];
    z[57]=z[72] + n<T>(1,20)*z[71] + z[57] + n<T>(1,2)*z[70] + z[69] + n<T>(145,36)*
    z[58] + n<T>(3,10)*z[68] + n<T>(1,10)*z[59] + z[63];
    z[57]=z[7]*z[57];
    z[59]=z[54] - z[4];
    z[62]= - n<T>(229,135)*z[5] + n<T>(1043,270)*z[6] + n<T>(601,270)*z[2] + z[62]
    - n<T>(457,30)*z[3] + z[59];
    z[62]=z[5]*z[62];
    z[63]=npow(z[13],2);
    z[62]=z[63] - z[62];
    z[68]=n<T>(1,2)*z[3];
    z[69]=z[8] + n<T>(2053,180)*z[3];
    z[68]=z[69]*z[68];
    z[69]=npow(z[4],2);
    z[70]=64*z[17] - n<T>(601,36)*z[16];
    z[70]=n<T>(1,5)*z[70] + n<T>(377,432)*z[23];
    z[71]=z[13] - n<T>(5413,720)*z[10];
    z[71]=z[10]*z[71];
    z[59]=n<T>(1681,1080)*z[2] + n<T>(41,10)*z[3] - z[59];
    z[59]=z[2]*z[59];
    z[72]= - n<T>(57,2)*z[3] - n<T>(1043,27)*z[2];
    z[72]=n<T>(1,2)*z[72] - n<T>(589,27)*z[6];
    z[72]=z[6]*z[72];
    z[59]=n<T>(1,5)*z[72] + z[59] + z[71] + z[68] + n<T>(11,4)*z[69] - z[65] + n<T>(1,3)*z[70]
     - n<T>(1,2)*z[62];
    z[59]=z[5]*z[59];
    z[62]= - z[3] - z[4];
    z[54]=z[54]*z[62];
    z[62]=4*z[19];
    z[65]=z[62] + 164*z[21] - z[67];
    z[65]=n<T>(6293,360)*z[23] + n<T>(1,5)*z[65] + n<T>(77,2)*z[22];
    z[65]=n<T>(1,3)*z[65] + n<T>(23,5)*z[63];
    z[67]= - n<T>(703,3)*z[11] + 281*z[10];
    z[67]=z[10]*z[67];
    z[68]=n<T>(3,2)*z[3] + n<T>(31,27)*z[2];
    z[68]=z[2]*z[68];
    z[53]=z[53] - n<T>(511,90)*z[13] + z[8];
    z[53]=n<T>(589,135)*z[2] + n<T>(53,90)*z[10] + n<T>(1,2)*z[53] - n<T>(19,9)*z[3];
    z[53]=z[6]*z[53];
    z[53]=z[53] + n<T>(19,10)*z[68] + n<T>(1,30)*z[67] + n<T>(1,3)*z[65] - n<T>(47,10)*
    z[66] + z[54];
    z[53]=z[6]*z[53];
    z[54]= - 589*z[20] - n<T>(1141,4)*z[16];
    z[65]=npow(z[10],2);
    z[67]=29*z[3] - n<T>(2957,15)*z[2];
    z[67]=z[2]*z[67];
    z[54]=n<T>(1,72)*z[67] + n<T>(5,4)*z[65] - n<T>(1529,360)*z[58] - n<T>(13,4)*z[69]
    + n<T>(6811,3240)*z[23] - n<T>(26,5)*z[22] + n<T>(1,135)*z[54] + n<T>(7,2)*z[15];
    z[54]=z[2]*z[54];
    z[65]= - 349*z[33] + n<T>(257,3)*z[32];
    z[60]= - n<T>(1991,270)*z[10] - n<T>(517,45)*z[3] - n<T>(3,2)*z[4] + z[60];
    z[60]=z[10]*z[60];
    z[60]=n<T>(1,2)*z[60] + n<T>(25,3)*z[58] + n<T>(703,180)*z[66] - n<T>(469,54)*z[23]
    + z[34] + n<T>(1,30)*z[65] + 2*z[18];
    z[60]=z[10]*z[60];
    z[65]=n<T>(11,30)*z[23] + z[18] + 9*z[15];
    z[56]=n<T>(5,3)*z[4] + z[8] + z[56];
    z[56]=z[4]*z[56];
    z[56]=n<T>(1,4)*z[56] + n<T>(1,2)*z[65] - n<T>(19,5)*z[66];
    z[56]=z[4]*z[56];
    z[65]= - n<T>(149,3)*z[33] - n<T>(31,5)*z[21];
    z[63]=n<T>(49,540)*z[63] - n<T>(35,54)*z[23] + n<T>(3,2)*z[19] + n<T>(1,6)*z[65]
     + 
    z[34];
    z[63]=z[13]*z[63];
    z[62]=z[62] - n<T>(2101,12)*z[23];
    z[65]=n<T>(1,2)*z[13] + n<T>(2,45)*z[11];
    z[65]=z[11]*z[65];
    z[62]=n<T>(1,45)*z[62] + z[65];
    z[62]=z[11]*z[62];
    z[65]= - 241*z[32] - n<T>(671,2)*z[17];
    z[65]= - n<T>(31,24)*z[23] + n<T>(1,6)*z[65] + 31*z[22];
    z[58]=n<T>(1,5)*z[65] - n<T>(623,216)*z[58];
    z[58]=z[3]*z[58];
    z[65]=z[28] - z[40];
    z[66]=z[50] + z[51];
    z[66]= - n<T>(427,9)*z[47] - n<T>(121,2)*z[48] + n<T>(703,18)*z[49] + n<T>(91,2)*
    z[52] - 43*z[66];
    z[66]=n<T>(1,5)*z[66] + n<T>(29,18)*z[46];
    z[61]=z[61] + n<T>(164,45)*z[14] - n<T>(1,2)*z[12];
    z[61]=z[23]*z[61];
    z[64]=n<T>(1,12)*z[64] + n<T>(281,1080)*z[23] - n<T>(1,2)*z[35] - n<T>(48,5)*z[34];
    z[64]=z[8]*z[64];

    r += n<T>(245,72)*z[25] - n<T>(11057,6480)*z[26] + n<T>(1607,540)*z[30] + n<T>(53981,2160)*z[31]
     + n<T>(7,2)*z[36] - n<T>(5,4)*z[37]
     + n<T>(11,4)*z[38]
     + n<T>(2563,720)*z[39] - n<T>(589,135)*z[41]
     + n<T>(5053,720)*z[42]
     + n<T>(1063,240)*
      z[43] + n<T>(5,6)*z[44] + n<T>(1,3)*z[45] + z[53] + z[54] + z[55] + z[56]
       + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64]
       + n<T>(2957,1080)*z[65] + n<T>(1,2)*z[66];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf318(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf318(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
