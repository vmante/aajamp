#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf245(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[58];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[5];
    z[10]=d[4];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[7];
    z[16]=e[8];
    z[17]=e[9];
    z[18]=e[10];
    z[19]=c[3];
    z[20]=d[9];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[3];
    z[30]=e[6];
    z[31]=e[14];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[11];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[23];
    z[38]=f[31];
    z[39]=f[36];
    z[40]=f[39];
    z[41]=f[51];
    z[42]=f[55];
    z[43]=f[58];
    z[44]=z[1]*i;
    z[45]= - z[6] + n<T>(8,9)*z[44];
    z[46]=n<T>(4,9)*z[5];
    z[47]=z[45] - z[46];
    z[48]=7*z[5];
    z[47]=z[47]*z[48];
    z[49]=n<T>(1,2)*z[6];
    z[50]= - 62*z[44] - z[49];
    z[50]=z[6]*z[50];
    z[51]=n<T>(43,6)*z[3] - z[48] + n<T>(62,3)*z[44] - 7*z[6];
    z[51]=z[3]*z[51];
    z[45]=n<T>(4,9)*z[4] - z[3] - z[46] - z[45];
    z[45]=z[4]*z[45];
    z[45]=7*z[45] + z[51] + n<T>(1,3)*z[50] - z[47];
    z[46]=z[4] + z[5];
    z[46]= - n<T>(14,15)*z[2] + n<T>(28,15)*z[44] - z[3] + z[6] + n<T>(7,15)*z[46];
    z[46]=z[2]*z[46];
    z[45]=n<T>(1,5)*z[45] + n<T>(2,3)*z[46];
    z[45]=z[2]*z[45];
    z[46]=4*z[44] - n<T>(7,3)*z[5];
    z[46]=z[5]*z[46];
    z[50]=4*z[5];
    z[51]=z[50] - n<T>(17,9)*z[11];
    z[51]=z[11]*z[51];
    z[46]=z[46] + z[51];
    z[46]=n<T>(5,9)*z[19] + n<T>(4,5)*z[46];
    z[46]=z[11]*z[46];
    z[51]= - z[44] - 31*z[6];
    z[50]=n<T>(1,5)*z[51] - z[50];
    z[50]=n<T>(14,5)*z[2] + n<T>(2,3)*z[50] - n<T>(9,5)*z[3];
    z[50]=z[18]*z[50];
    z[51]=n<T>(113,20)*z[44] + z[6];
    z[51]=z[51]*npow(z[6],2);
    z[45]=z[51] + z[45] + z[46] + z[50];
    z[46]=2*z[44];
    z[50]=z[46] - z[5];
    z[51]=z[50]*z[5];
    z[52]=n<T>(31,220)*z[6] + 1 + n<T>(31,110)*z[44];
    z[52]=z[6]*z[52];
    z[53]=npow(z[3],2);
    z[54]=z[44] + z[6];
    z[55]=n<T>(1,2)*z[4] - z[54];
    z[55]=z[4]*z[55];
    z[56]=n<T>(217,3)*z[44] + 59*z[6];
    z[56]=n<T>(232,9)*z[10] + n<T>(371,12)*z[4] - n<T>(47,3)*z[3] + n<T>(1,4)*z[56]
     - 49
   *z[5];
    z[56]=z[10]*z[56];
    z[52]=n<T>(1,55)*z[56] + n<T>(31,110)*z[55] - n<T>(6,11)*z[53] + z[52] - n<T>(28,165)
   *z[51];
    z[52]=z[10]*z[52];
    z[53]=13*z[44];
    z[55]= - z[53] - n<T>(271,6)*z[6];
    z[55]=z[55]*z[49];
    z[47]=z[55] + z[47];
    z[48]=9*z[54] + z[48];
    z[48]=n<T>(1,11)*z[48] - n<T>(1,6)*z[3];
    z[48]=z[3]*z[48];
    z[55]=n<T>(13,4)*z[6] + n<T>(14,9)*z[5];
    z[55]= - n<T>(14,99)*z[4] + n<T>(1,11)*z[55] - z[3];
    z[55]=z[4]*z[55];
    z[47]=z[55] + n<T>(1,11)*z[47] + z[48];
    z[47]=z[4]*z[47];
    z[48]= - n<T>(7,11)*z[44] - z[49];
    z[48]=z[6]*z[48];
    z[55]=n<T>(1,11)*z[9];
    z[54]=7*z[54] - n<T>(3,2)*z[9];
    z[54]=z[54]*z[55];
    z[56]=z[6] - z[9];
    z[56]=z[8]*z[56];
    z[48]=n<T>(7,22)*z[56] - n<T>(7,66)*z[19] + z[48] + z[54];
    z[48]=z[8]*z[48];
    z[47]=z[47] + z[48];
    z[48]= - z[44] - z[49];
    z[48]=z[6]*z[48];
    z[49]=z[44] - z[5];
    z[54]= - 2*z[49] - z[10];
    z[54]=z[10]*z[54];
    z[56]= - 7*z[44] + n<T>(3,5)*z[6];
    z[56]= - n<T>(1,15)*z[9] - n<T>(56,15)*z[10] + n<T>(1,2)*z[56] + n<T>(19,5)*z[5];
    z[56]=z[9]*z[56];
    z[48]=z[56] + n<T>(41,15)*z[54] + n<T>(7,5)*z[48] + n<T>(8,3)*z[51];
    z[48]=z[48]*z[55];
    z[51]=17*z[3];
    z[54]=z[44]*z[51];
    z[55]=z[4]*z[53];
    z[54]=13*z[19] + z[54] + z[55];
    z[54]=z[7]*z[54];
    z[51]= - z[53] + z[51] + 13*z[4];
    z[51]=z[14]*z[51];
    z[53]=49*z[3] + n<T>(127,3)*z[10];
    z[53]=z[29]*z[53];
    z[51]=z[53] + z[54] + z[51];
    z[53]= - z[4] + z[46] - z[2];
    z[53]=z[13]*z[53];
    z[54]= - z[2] + z[50];
    z[54]=z[16]*z[54];
    z[53]= - z[25] - z[53] - z[54] + z[34] + z[33];
    z[54]=z[5]*z[44];
    z[54]=z[54] + z[19];
    z[46]= - z[11]*z[46];
    z[46]=n<T>(1,3)*z[54] + z[46];
    z[46]=z[12]*z[46];
    z[54]= - n<T>(1,3)*z[49] - 2*z[11];
    z[54]=z[17]*z[54];
    z[46]=z[46] + z[54];
    z[54]=n<T>(29,165)*z[6] - 1 + n<T>(17,165)*z[44];
    z[54]=z[6]*z[54];
    z[50]= - z[6] - z[50];
    z[50]=z[5]*z[50];
    z[50]=z[54] + n<T>(4,33)*z[50];
    z[44]=n<T>(38,99)*z[3] - n<T>(7,66)*z[6] + 1 - n<T>(67,165)*z[44];
    z[44]=z[3]*z[44];
    z[44]=2*z[50] + z[44];
    z[44]=z[3]*z[44];
    z[50]= - z[41] + z[43] - z[42];
    z[54]= - 83*z[6] - n<T>(271,6)*z[5];
    z[54]=n<T>(1,9)*z[54] - n<T>(13,2)*z[3];
    z[54]=n<T>(92,45)*z[9] - n<T>(25,18)*z[10] + n<T>(14,135)*z[2] + n<T>(1,5)*z[54]
     - 
   n<T>(13,108)*z[4];
    z[54]=n<T>(19,495)*z[20] + n<T>(1,11)*z[54];
    z[54]=z[19]*z[54];
    z[55]= - n<T>(112,495)*z[26] - n<T>(56,495)*z[24] + n<T>(313,1980)*z[21];
    z[55]=i*z[55];
    z[49]= - z[9] + z[49];
    z[49]=z[15]*z[49];
    z[56]=n<T>(9,5)*z[10] + n<T>(8,3)*z[11];
    z[56]=z[30]*z[56];
    z[57]= - z[6] - z[20];
    z[57]=z[31]*z[57];

    r +=  - n<T>(5,33)*z[22] - n<T>(14,1485)*z[23] + n<T>(56,495)*z[27] + n<T>(22,45)*
      z[28] - n<T>(191,660)*z[32] - n<T>(371,660)*z[35] + n<T>(59,220)*z[36] - n<T>(4,33)*z[37]
     + n<T>(58,165)*z[38]
     + n<T>(26,55)*z[39] - n<T>(56,165)*z[40]
     +  z[44] + n<T>(1,11)*z[45] + n<T>(8,55)*z[46] + n<T>(1,5)*z[47] + z[48] + n<T>(2,165)*z[49]
     + n<T>(3,110)*z[50]
     + n<T>(1,55)*z[51]
     + z[52] - n<T>(28,495)*
      z[53] + z[54] + z[55] + n<T>(4,11)*z[56] + n<T>(3,55)*z[57];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf245(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf245(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
