#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf192(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[15];
    z[8]=d[4];
    z[9]=d[16];
    z[10]=d[8];
    z[11]=d[5];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[4];
    z[16]=e[8];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[19];
    z[21]=c[23];
    z[22]=c[25];
    z[23]=c[26];
    z[24]=c[28];
    z[25]=c[31];
    z[26]=e[3];
    z[27]=e[10];
    z[28]=e[6];
    z[29]=d[6];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[14];
    z[38]=f[16];
    z[39]=f[18];
    z[40]=f[31];
    z[41]=f[36];
    z[42]=f[39];
    z[43]=n<T>(1,3)*z[5];
    z[44]=z[1]*i;
    z[45]= - 4*z[44] - z[5];
    z[45]=z[45]*z[43];
    z[46]=n<T>(1,3)*z[2];
    z[47]=2*z[44];
    z[48]=z[47] + z[5];
    z[48]=2*z[48] - z[2];
    z[48]=z[48]*z[46];
    z[49]=z[47] + 3*z[8];
    z[49]=z[8]*z[49];
    z[50]=z[2] - z[5];
    z[51]= - z[8] - n<T>(2,3)*z[50];
    z[51]=z[6]*z[51];
    z[45]=z[51] + z[48] + z[49] + z[45];
    z[45]=z[6]*z[45];
    z[48]=z[47] + z[8];
    z[48]=z[48]*z[8];
    z[49]=z[44] + z[8];
    z[51]=2*z[49];
    z[52]= - z[51] + z[6];
    z[52]=z[6]*z[52];
    z[49]= - z[6] + z[49];
    z[49]=z[11]*z[49];
    z[49]=z[49] + z[48] + z[52];
    z[49]=z[11]*z[49];
    z[52]=z[5] + 3*z[3];
    z[53]= - z[44]*z[52];
    z[53]= - z[17] + z[53];
    z[53]=z[7]*z[53];
    z[54]=2*z[8];
    z[55]=z[54] + z[4];
    z[55]=z[44]*z[55];
    z[55]=z[17] + z[55];
    z[55]=z[9]*z[55];
    z[52]=z[44] - z[52];
    z[52]=z[14]*z[52];
    z[56]=z[44] - z[4];
    z[54]= - z[54] + z[56];
    z[54]=z[15]*z[54];
    z[57]=4*z[8];
    z[58]= - z[57] - 5*z[3];
    z[58]=z[26]*z[58];
    z[59]=z[3] + z[10];
    z[59]=z[27]*z[59];
    z[45]= - z[41] - z[40] - z[32] + z[31] + z[54] + z[58] + z[59] + 
    z[42] + z[53] + z[45] + z[49] + z[55] + z[52];
    z[49]=npow(z[8],2);
    z[52]=npow(z[5],2);
    z[53]=z[44] - z[5];
    z[53]=z[2]*z[53];
    z[52]=z[53] + 2*z[49] + z[52];
    z[53]=3*z[5];
    z[54]= - n<T>(20,3)*z[3] + z[2] + z[53] + z[44] + z[57];
    z[54]=z[3]*z[54];
    z[52]=2*z[52] + z[54];
    z[52]=z[3]*z[52];
    z[54]= - 5*z[44] + z[5];
    z[43]=z[54]*z[43];
    z[43]= - z[49] + z[43];
    z[54]=z[47] - z[2];
    z[55]=z[54] - z[5];
    z[46]= - z[55]*z[46];
    z[43]=2*z[43] + z[46];
    z[43]=z[2]*z[43];
    z[46]= - 2*z[50];
    z[50]=z[44]*z[46];
    z[58]= - z[47] + z[8];
    z[58]=z[8]*z[58];
    z[50]=z[58] + z[50];
    z[46]=z[56] - z[46];
    z[46]=z[4]*z[46];
    z[56]=npow(z[3],2);
    z[46]=z[46] + 2*z[50] + z[56];
    z[46]=z[4]*z[46];
    z[50]= - 2*z[3] + z[53] - 3*z[44] - z[8];
    z[50]=z[10]*z[50];
    z[51]=z[51] - z[5];
    z[51]=z[5]*z[51];
    z[48]=z[50] - z[48] + z[51];
    z[48]=z[10]*z[48];
    z[44]=z[44] - z[57];
    z[44]=z[44]*z[49];
    z[49]=z[11] - z[4];
    z[49]=n<T>(2,3)*z[6] + z[3] - n<T>(29,3)*z[2] + 11*z[8] - n<T>(4,3)*z[5] - 4*
    z[49];
    z[49]=z[17]*z[49];
    z[49]= - z[22] + z[49] + z[34];
    z[50]=z[6] - z[54];
    z[50]=z[16]*z[50];
    z[50]=z[35] + z[50] - z[39];
    z[51]= - z[4] + z[54];
    z[51]=z[12]*z[51];
    z[51]=z[51] - z[30];
    z[53]= - n<T>(20,3)*z[23] + n<T>(2,3)*z[21] - n<T>(7,6)*z[18];
    z[53]=i*z[53];
    z[47]= - z[5] + z[47] - 7*z[8];
    z[47]=z[5]*z[8]*z[47];
    z[54]=z[13]*z[55];
    z[55]= - z[28]*z[57];
    z[56]=4*z[28];
    z[56]= - z[29]*z[56];

    r += z[19] + n<T>(37,18)*z[20] + n<T>(10,3)*z[24] - n<T>(41,6)*z[25] + 3*z[33]
       + 7*z[36] + z[37] - z[38] + z[43] + z[44] + 2*z[45] + z[46] + 
      z[47] + z[48] + n<T>(1,3)*z[49] + n<T>(4,3)*z[50] + 4*z[51] + z[52] + 
      z[53] + n<T>(2,3)*z[54] + z[55] + z[56];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf192(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf192(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
