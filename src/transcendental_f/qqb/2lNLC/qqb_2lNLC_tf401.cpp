#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf401(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[143];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[42];
    z[3]=f[61];
    z[4]=f[77];
    z[5]=f[79];
    z[6]=f[80];
    z[7]=f[81];
    z[8]=c[11];
    z[9]=d[0];
    z[10]=d[1];
    z[11]=d[2];
    z[12]=d[4];
    z[13]=d[5];
    z[14]=d[6];
    z[15]=d[7];
    z[16]=d[8];
    z[17]=d[9];
    z[18]=c[12];
    z[19]=c[13];
    z[20]=c[20];
    z[21]=c[23];
    z[22]=c[32];
    z[23]=c[33];
    z[24]=c[35];
    z[25]=c[36];
    z[26]=c[37];
    z[27]=c[38];
    z[28]=c[39];
    z[29]=c[40];
    z[30]=c[43];
    z[31]=c[44];
    z[32]=c[47];
    z[33]=c[48];
    z[34]=c[49];
    z[35]=c[50];
    z[36]=c[55];
    z[37]=c[56];
    z[38]=c[57];
    z[39]=c[59];
    z[40]=c[81];
    z[41]=c[84];
    z[42]=f[22];
    z[43]=f[24];
    z[44]=f[44];
    z[45]=f[62];
    z[46]=f[69];
    z[47]=f[70];
    z[48]=d[3];
    z[49]=g[24];
    z[50]=g[27];
    z[51]=g[38];
    z[52]=g[40];
    z[53]=g[44];
    z[54]=g[48];
    z[55]=g[50];
    z[56]=g[60];
    z[57]=g[62];
    z[58]=g[63];
    z[59]=g[79];
    z[60]=g[82];
    z[61]=g[90];
    z[62]=g[92];
    z[63]=g[95];
    z[64]=g[99];
    z[65]=g[101];
    z[66]=g[110];
    z[67]=g[112];
    z[68]=g[113];
    z[69]=g[128];
    z[70]=g[131];
    z[71]=g[138];
    z[72]=g[140];
    z[73]=g[145];
    z[74]=g[147];
    z[75]=g[170];
    z[76]=g[173];
    z[77]=g[181];
    z[78]=g[183];
    z[79]=g[187];
    z[80]=g[189];
    z[81]=g[191];
    z[82]=g[198];
    z[83]=g[200];
    z[84]=g[201];
    z[85]=g[212];
    z[86]=g[214];
    z[87]=g[218];
    z[88]=g[220];
    z[89]=g[224];
    z[90]=g[232];
    z[91]=g[233];
    z[92]=g[234];
    z[93]=g[243];
    z[94]=g[245];
    z[95]=g[249];
    z[96]=g[251];
    z[97]=g[254];
    z[98]=g[270];
    z[99]=g[272];
    z[100]=g[276];
    z[101]=g[278];
    z[102]=g[281];
    z[103]=g[285];
    z[104]=g[289];
    z[105]=g[290];
    z[106]=g[292];
    z[107]=g[294];
    z[108]=g[324];
    z[109]=g[333];
    z[110]=g[335];
    z[111]=g[339];
    z[112]=g[345];
    z[113]=g[347];
    z[114]=g[348];
    z[115]=g[378];
    z[116]=g[424];
    z[117]=g[427];
    z[118]=g[428];
    z[119]=g[430];
    z[120]=g[431];
    z[121]=g[433];
    z[122]=g[434];
    z[123]=g[435];
    z[124]= - 5*z[20] + n<T>(1,4)*z[21];
    z[125]= - n<T>(1,12)*z[124] + z[19];
    z[125]=85*z[125] - n<T>(3211,1296)*z[8];
    z[125]=z[10]*z[125];
    z[124]=n<T>(1,4)*z[124] - 3*z[19];
    z[124]=71*z[124] + n<T>(2495,1296)*z[8];
    z[124]=z[11]*z[124];
    z[126]= - z[20] + n<T>(1,20)*z[21];
    z[127]= - n<T>(3,5)*z[19] + n<T>(1,4)*z[126];
    z[128]= - 157*z[127] - n<T>(2927,2160)*z[8];
    z[128]=z[9]*z[128];
    z[124]=z[128] + z[125] + z[124];
    z[125]=n<T>(1,2)*z[12];
    z[126]= - n<T>(1,5)*z[19] + n<T>(1,12)*z[126];
    z[128]= - 59*z[126] - n<T>(143,2160)*z[8];
    z[128]=z[128]*z[125];
    z[129]=9*z[2] + n<T>(143,24)*z[4] - n<T>(169,24)*z[5] - 7*z[3] - n<T>(3,2)*z[6]
    - n<T>(5,4)*z[7];
    z[129]=z[1]*z[129];
    z[130]=z[14]*z[8];
    z[131]=n<T>(1,10)*z[21] - 2*z[20];
    z[131]=n<T>(1,3)*z[131] - n<T>(8,5)*z[19];
    z[131]=31*z[131] + n<T>(3877,1620)*z[8];
    z[131]=z[16]*z[131];
    z[132]= - 377*z[126] - n<T>(2177,6480)*z[8];
    z[132]=z[17]*z[132];
    z[127]=29*z[127] - n<T>(3743,6480)*z[8];
    z[127]=z[15]*z[127];
    z[126]= - n<T>(1261,6480)*z[8] - z[126];
    z[126]=z[13]*z[126];
    z[124]=13*z[126] + z[127] + z[128] + z[132] + z[131] + n<T>(55,27)*
    z[130] + n<T>(63,4)*z[23] - 24*z[26] - n<T>(1147,18)*z[27] + n<T>(109,162)*z[28]
    - 10*z[31] + n<T>(149,1440)*z[32] - n<T>(471,10)*z[34] + 12*z[35] - n<T>(157,8)
   *z[37] - n<T>(3,2)*z[38] + n<T>(551,480)*z[39] - 4*z[41] + n<T>(65,18)*z[40] + 
    z[129] + n<T>(1,2)*z[124];
    z[124]=i*z[124];
    z[126]=z[2] + z[3];
    z[127]=z[4] + z[5];
    z[128]=n<T>(3,8)*z[7];
    z[126]=z[128] + n<T>(35,24)*z[127] - n<T>(11,6)*z[126];
    z[126]=z[48]*z[126];
    z[129]=n<T>(5,8)*z[7];
    z[130]=n<T>(4849,12)*z[18] + n<T>(53,2)*z[44] - n<T>(421,8)*z[45] - n<T>(1045,8)*
    z[42] - n<T>(1201,8)*z[43] + n<T>(89,2)*z[47] + 25*z[46];
    z[130]=n<T>(587,12)*z[2] - n<T>(65,12)*z[4] + n<T>(355,12)*z[5] + n<T>(359,24)*z[3]
    - n<T>(463,12)*z[6] + n<T>(1,3)*z[130] + z[129];
    z[130]=z[13]*z[130];
    z[131]=z[115] + z[120];
    z[130]= - z[113] - z[57] - z[130] + n<T>(5,8)*z[116] - n<T>(5,4)*z[117] - 
    z[118] - n<T>(3,4)*z[119] + n<T>(29,8)*z[121] - 7*z[123] - n<T>(13,4)*z[122]
     + n<T>(1,2)*z[131]
     + z[83] - z[105];
    z[131]=n<T>(31,8)*z[45] + n<T>(215,24)*z[42] - n<T>(25,3)*z[46] - n<T>(1,8)*z[43];
    z[127]=n<T>(209,48)*z[2] - n<T>(223,24)*z[3] - n<T>(1,16)*z[6] - n<T>(1345,72)*z[18]
    + n<T>(1,2)*z[131] - n<T>(14,3)*z[44] + n<T>(1,8)*z[127];
    z[127]=z[17]*z[127];
    z[131]= - n<T>(127,3)*z[45] - 5*z[47] - z[43];
    z[131]=n<T>(1,2)*z[131] + n<T>(65,3)*z[44];
    z[131]=n<T>(7,4)*z[2] + n<T>(95,8)*z[4] + n<T>(295,24)*z[5] - n<T>(277,24)*z[3]
     + n<T>(5,8)*z[6] - n<T>(7,8)*z[7]
     + n<T>(1,4)*z[131] - n<T>(23,9)*z[18];
    z[125]=z[131]*z[125];
    z[131]=53*z[46];
    z[132]=n<T>(301,4)*z[43] - n<T>(89,4)*z[47] - z[131];
    z[129]= - n<T>(39,16)*z[2] - n<T>(169,48)*z[4] - n<T>(563,48)*z[5] + n<T>(29,16)*
    z[3] - n<T>(409,24)*z[6] + z[129] + n<T>(317,24)*z[18] + n<T>(203,6)*z[44] + n<T>(1,3)*z[132]
     - n<T>(147,16)*z[45];
    z[129]=z[11]*z[129];
    z[132]=n<T>(23,4)*z[18] - n<T>(635,4)*z[44] + n<T>(145,2)*z[45] - n<T>(215,8)*z[42]
    - n<T>(301,2)*z[43] + n<T>(371,8)*z[47] + 131*z[46];
    z[128]=n<T>(221,6)*z[6] + n<T>(1,3)*z[132] - z[128];
    z[128]= - n<T>(67,24)*z[2] - n<T>(61,24)*z[4] + n<T>(137,24)*z[5] + n<T>(1,2)*z[128]
    + n<T>(16,3)*z[3];
    z[128]=z[9]*z[128];
    z[132]= - z[56] + z[82] + z[104] - z[112];
    z[133]=z[76] + z[77];
    z[134]=z[75] + z[78];
    z[135]=z[69] + z[72];
    z[136]=z[66] - z[90];
    z[137]=z[50] + z[51];
    z[138]=z[49] + z[52];
    z[139]=n<T>(3,8)*z[48];
    z[139]=z[6]*z[139];
    z[140]=n<T>(415,2)*z[42] - 89*z[47] + 301*z[43];
    z[140]=n<T>(1,2)*z[140] + 41*z[45];
    z[140]=n<T>(1,3)*z[140] + n<T>(1,2)*z[44];
    z[140]= - n<T>(1027,48)*z[2] + n<T>(9,8)*z[4] - n<T>(131,8)*z[5] - n<T>(337,48)*z[3]
    + n<T>(827,48)*z[6] - n<T>(3,16)*z[7] + n<T>(1,2)*z[140] - 55*z[18];
    z[140]=z[14]*z[140];
    z[131]=n<T>(143,8)*z[45] - z[131] + n<T>(415,8)*z[42];
    z[131]= - n<T>(89,4)*z[2] + n<T>(241,48)*z[4] - n<T>(43,16)*z[5] + n<T>(5,16)*z[6]
    - n<T>(15,16)*z[7] - n<T>(1669,36)*z[18] + n<T>(1,3)*z[131] - n<T>(93,4)*z[44];
    z[131]=z[16]*z[131];
    z[141]=n<T>(551,24)*z[2] - n<T>(85,24)*z[4] - n<T>(217,24)*z[5] + n<T>(365,48)*z[3]
    + n<T>(641,36)*z[18] + n<T>(493,24)*z[44] - n<T>(53,16)*z[45] - n<T>(415,24)*z[42]
    + n<T>(1,16)*z[43] + n<T>(5,16)*z[47] + n<T>(53,3)*z[46];
    z[141]=z[10]*z[141];
    z[142]=n<T>(43,24)*z[42] - n<T>(1,8)*z[47] - n<T>(5,3)*z[46];
    z[142]= - n<T>(59,4)*z[44] + 5*z[142] - n<T>(139,24)*z[45];
    z[142]= - n<T>(31,16)*z[2] - n<T>(65,48)*z[4] + n<T>(557,48)*z[5] + n<T>(27,16)*z[3]
    - n<T>(1,4)*z[6] + n<T>(7,16)*z[7] + n<T>(1,2)*z[142] + 22*z[18];
    z[142]=z[15]*z[142];

    r += n<T>(1417,405)*z[22] - n<T>(312,5)*z[24] - 22*z[25] + n<T>(55,3)*z[29] + 
      n<T>(11,2)*z[30] - n<T>(599,24)*z[33] + n<T>(13,10)*z[36] - n<T>(1,8)*z[53]
     - 
      n<T>(1045,72)*z[54] + n<T>(1165,72)*z[55] + z[58] + n<T>(3445,96)*z[59] + 
      n<T>(5575,96)*z[60] + n<T>(6071,96)*z[61] + n<T>(3541,96)*z[62] - n<T>(1,24)*
      z[63] - n<T>(129,8)*z[64] + n<T>(3295,144)*z[65] - n<T>(63,16)*z[67] - n<T>(63,8)
      *z[68] - n<T>(299,12)*z[70] + n<T>(27,16)*z[71] - n<T>(95,36)*z[73] - n<T>(35,48)
      *z[74] - n<T>(1,48)*z[79] + n<T>(131,9)*z[80] - n<T>(335,72)*z[81] - z[84]
     +  n<T>(1195,96)*z[85] + n<T>(1225,96)*z[86] - n<T>(423,32)*z[87] + n<T>(19,96)*
      z[88] - n<T>(1,36)*z[89] + n<T>(67,16)*z[91] + n<T>(59,8)*z[92] - n<T>(557,24)*
      z[93] - n<T>(115,12)*z[94] - n<T>(59,4)*z[95] + n<T>(161,12)*z[96] + n<T>(97,18)*
      z[97] - n<T>(809,24)*z[98] + n<T>(37,8)*z[99] - n<T>(125,4)*z[100] - n<T>(833,24)
      *z[101] + n<T>(833,72)*z[102] + n<T>(3,16)*z[103] - n<T>(5,4)*z[106] - n<T>(3,4)*
      z[107] - n<T>(83,8)*z[108] - n<T>(3,8)*z[109] + z[110] + n<T>(1,12)*z[111]
     +  z[114] + z[124] + z[125] + z[126] + z[127] + z[128] + z[129] - n<T>(1,2)*z[130]
     + z[131] - n<T>(1,4)*z[132] - n<T>(169,12)*z[133] - n<T>(433,12)*
      z[134];
      r+=  n<T>(223,12)*z[135] - n<T>(63,32)*z[136] + n<T>(593,12)*z[137] + n<T>(106,3)*z[138]
     + z[139]
     + z[140]
     + z[141]
     + z[142];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf401(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf401(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
