#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf904(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[62];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[9];
    z[7]=c[12];
    z[8]=c[13];
    z[9]=c[20];
    z[10]=c[23];
    z[11]=c[32];
    z[12]=c[33];
    z[13]=c[35];
    z[14]=c[36];
    z[15]=c[37];
    z[16]=c[38];
    z[17]=c[39];
    z[18]=c[40];
    z[19]=c[43];
    z[20]=c[44];
    z[21]=c[47];
    z[22]=c[48];
    z[23]=c[49];
    z[24]=c[50];
    z[25]=c[55];
    z[26]=c[56];
    z[27]=c[57];
    z[28]=c[59];
    z[29]=c[81];
    z[30]=c[84];
    z[31]=f[44];
    z[32]=f[61];
    z[33]=f[62];
    z[34]=f[79];
    z[35]=d[1];
    z[36]=f[24];
    z[37]=d[3];
    z[38]=g[38];
    z[39]=g[82];
    z[40]=g[101];
    z[41]=g[128];
    z[42]=g[131];
    z[43]=g[138];
    z[44]=g[140];
    z[45]=g[147];
    z[46]=g[214];
    z[47]=g[218];
    z[48]=g[245];
    z[49]=g[251];
    z[50]=g[276];
    z[51]=g[312];
    z[52]=z[3] - z[2];
    z[53]=z[52] - z[4];
    z[54]= - i*z[53];
    z[55]=z[5]*i;
    z[56]=n<T>(13,9)*z[55] + z[54];
    z[57]=z[6]*i;
    z[56]=n<T>(1,2)*z[56] - n<T>(11,9)*z[57];
    z[56]=z[1]*z[56];
    z[58]=z[5] - z[4];
    z[59]= - z[52] + z[58];
    z[59]=z[31]*z[59];
    z[60]=z[52] + z[58];
    z[60]=z[34]*z[60];
    z[58]=z[6] - z[2] + z[58];
    z[58]=z[32]*z[58];
    z[61]= - z[3] + z[6];
    z[61]=z[36]*z[61];
    z[56]=z[59] + z[58] + n<T>(1,15)*z[56] + z[60] + z[61];
    z[54]=z[54] - 3*z[55];
    z[55]= - n<T>(1,2)*z[54] - z[57];
    z[55]=z[10]*z[55];
    z[55]=z[55] - z[25];
    z[54]=2*z[57] + z[54];
    z[57]=z[9] + n<T>(12,5)*z[8];
    z[54]=z[54]*z[57];
    z[57]=z[37] - z[35];
    z[57]=n<T>(1,2)*z[57];
    z[58]=z[36] + z[32];
    z[57]=z[58]*z[57];
    z[58]=z[45] + z[40];
    z[53]= - n<T>(1,3)*z[6] + n<T>(2,3)*z[53] + z[5];
    z[53]=z[7]*z[53];
    z[53]=z[53] - z[19];
    z[59]=n<T>(4,3)*z[30] - n<T>(65,54)*z[29] - n<T>(1,180)*z[28] + n<T>(1,2)*z[27]
     - 
    z[26] - 4*z[24] - n<T>(12,5)*z[23] + n<T>(3,20)*z[21] + n<T>(10,3)*z[20] - n<T>(109,486)*z[17]
     - n<T>(242,27)*z[16]
     + 8*z[15]
     + n<T>(5,27)*z[12];
    z[59]=i*z[59];
    z[52]= - z[5] - z[4] - z[52];
    z[52]=n<T>(1,2)*z[52] + z[6];
    z[52]=z[33]*z[52];

    r +=  - n<T>(131,270)*z[11] + n<T>(24,5)*z[13] + 8*z[14] - n<T>(20,3)*z[18] + 
      n<T>(10,3)*z[22] - z[38] - z[39] - z[41] - z[42] + z[43] - z[44] + 
      z[46] - z[47] + z[48] - z[49] + z[50] + z[51] + z[52] + 2*z[53]
       + z[54] + n<T>(1,10)*z[55] + n<T>(1,2)*z[56] + z[57] - n<T>(1,3)*z[58] + 
      z[59];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf904(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf904(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
