#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf685(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[75];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[16];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[4];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=e[10];
    z[21]=c[3];
    z[22]=d[9];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[3];
    z[32]=e[6];
    z[33]=e[14];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[14];
    z[42]=f[16];
    z[43]=f[17];
    z[44]=f[18];
    z[45]=f[31];
    z[46]=f[36];
    z[47]=f[39];
    z[48]=f[51];
    z[49]=f[55];
    z[50]=f[58];
    z[51]=z[1]*i;
    z[52]=2*z[51];
    z[53]=z[52] - z[6];
    z[54]=1156*z[6];
    z[55]= - z[53]*z[54];
    z[56]=n<T>(40649,3)*z[7];
    z[57]=z[51] - z[5];
    z[58]=z[7] + z[57];
    z[58]=z[58]*z[56];
    z[59]=npow(z[9],2);
    z[60]= - 40649*z[51] + 47585*z[5];
    z[60]=z[5]*z[60];
    z[61]=z[2]*z[57];
    z[55]=6905*z[61] + z[58] + 9998*z[59] + z[55] + n<T>(1,3)*z[60];
    z[54]=n<T>(14560,3)*z[5] + n<T>(6155,6)*z[51] - z[54];
    z[54]= - n<T>(47179,4593)*z[3] + n<T>(6905,3062)*z[2] + n<T>(14560,4593)*z[7]
     + 
   n<T>(1,1531)*z[54] + z[4];
    z[54]=z[3]*z[54];
    z[54]=n<T>(1,1531)*z[55] + z[54];
    z[54]=z[3]*z[54];
    z[55]=z[9] - z[6];
    z[55]= - 3843*z[51] - n<T>(6155,2)*z[55];
    z[58]=n<T>(1,2)*z[4];
    z[60]=n<T>(1,2)*z[7];
    z[55]= - z[60] + n<T>(1,1531)*z[55] + z[58];
    z[55]=z[10]*z[55];
    z[58]=z[58] - z[51];
    z[58]=z[58]*z[4];
    z[61]= - z[51] + n<T>(1,2)*z[6];
    z[61]=z[61]*z[6];
    z[62]=z[51] - z[6];
    z[63]= - n<T>(1,2)*z[9] - z[62];
    z[63]=z[9]*z[63];
    z[63]= - z[61] + z[63];
    z[64]=z[4] - z[60] - z[51];
    z[64]=z[7]*z[64];
    z[55]=z[55] + z[64] + n<T>(6155,1531)*z[63] - z[58];
    z[55]=z[10]*z[55];
    z[63]=1969*z[51] - n<T>(11967,2)*z[6];
    z[63]=z[6]*z[63];
    z[64]= - 2489*z[51] + 3916*z[6];
    z[64]=3*z[64] - 4312*z[5];
    z[64]=z[5]*z[64];
    z[65]=n<T>(8593,2)*z[2] - 3562*z[4] + n<T>(281,2)*z[5] - 8593*z[51] - 875*
    z[6];
    z[65]=z[2]*z[65];
    z[63]=z[65] + z[63] + z[64];
    z[64]=z[4] - z[5];
    z[64]=5593*z[51] - 2031*z[64];
    z[64]=z[4]*z[64];
    z[60]=z[60] + z[62];
    z[60]=z[7]*z[60];
    z[59]=z[60] + n<T>(2,1531)*z[64] - z[59] + n<T>(1,1531)*z[63];
    z[59]=z[2]*z[59];
    z[60]=z[4] + z[9];
    z[63]=z[51]*z[60];
    z[63]=z[21] + z[63];
    z[63]=z[11]*z[63];
    z[60]=z[51] - z[60];
    z[60]=z[17]*z[60];
    z[60]= - z[34] + z[63] + z[60];
    z[63]=14560*z[3];
    z[64]= - 16903*z[5] - z[63];
    z[64]=z[51]*z[64];
    z[64]= - 16903*z[21] + z[64];
    z[64]=z[8]*z[64];
    z[63]=16903*z[57] - z[63];
    z[63]=z[16]*z[63];
    z[63]=z[64] + z[63];
    z[64]= - z[51] + n<T>(1,2)*z[5];
    z[64]=z[64]*z[5];
    z[65]=n<T>(142439,2)*z[51] - 44647*z[6];
    z[65]=n<T>(1,3)*z[65] - n<T>(17715,2)*z[5];
    z[65]=n<T>(1,2)*z[65] + n<T>(12716,3)*z[9];
    z[65]=z[9]*z[65];
    z[61]=z[65] + n<T>(13091,3)*z[61] - n<T>(26901,2)*z[64];
    z[65]=n<T>(1,1531)*z[9];
    z[61]=z[61]*z[65];
    z[66]=3812*z[6];
    z[67]= - z[53]*z[66];
    z[66]= - 1781*z[51] - z[66];
    z[66]=2*z[66] + 1781*z[5];
    z[66]=z[5]*z[66];
    z[66]=z[67] + z[66];
    z[67]= - z[52] + z[9];
    z[67]=z[9]*z[67];
    z[68]= - n<T>(2,3)*z[4] + n<T>(1781,1531)*z[5] - z[51] + n<T>(3812,1531)*z[6];
    z[68]=z[4]*z[68];
    z[66]=z[68] + n<T>(1,1531)*z[66] + z[67];
    z[66]=z[4]*z[66];
    z[67]= - 26901*z[57] - n<T>(22277,2)*z[9];
    z[65]=z[67]*z[65];
    z[67]=npow(z[6],2);
    z[64]=z[65] + z[67] + n<T>(595,4593)*z[64];
    z[57]=n<T>(72707,3)*z[57] - 26901*z[9];
    z[57]=n<T>(1,3062)*z[57] - z[4];
    z[57]=n<T>(1,2)*z[57] + n<T>(1,3)*z[7];
    z[57]=z[7]*z[57];
    z[57]=z[57] + n<T>(1,2)*z[64] - z[58];
    z[57]=z[7]*z[57];
    z[58]= - 31556*z[51] + n<T>(44647,2)*z[6];
    z[58]=z[6]*z[58];
    z[64]=2*z[62] + z[9];
    z[64]=z[9]*z[64];
    z[65]=z[6] + z[51] - z[12];
    z[65]=z[12]*z[65];
    z[58]=n<T>(13091,2)*z[65] - 6155*z[21] + z[58] + 15778*z[64];
    z[58]=z[12]*z[58];
    z[64]= - z[12] - z[6];
    z[64]=z[51]*z[64];
    z[64]= - z[21] + z[64];
    z[64]=z[13]*z[64];
    z[65]= - z[12] + z[62];
    z[65]=z[19]*z[65];
    z[64]=z[64] + z[65];
    z[65]=z[51] - n<T>(1,6)*z[6];
    z[65]=z[65]*z[67];
    z[67]=z[6]*z[53];
    z[51]=3*z[51] - n<T>(1031,1531)*z[6];
    z[51]=2*z[51] - z[5];
    z[51]=z[5]*z[51];
    z[51]=n<T>(2062,1531)*z[67] + z[51];
    z[51]=z[5]*z[51];
    z[56]= - n<T>(22184,3)*z[3] + 2312*z[62] - z[56];
    z[56]=z[20]*z[56];
    z[62]= - z[48] + z[50] - z[49];
    z[67]=z[38] - z[27];
    z[68]=z[47] - z[45];
    z[69]=5567*z[6] + n<T>(20795,4)*z[5];
    z[69]=11*z[69] + 75329*z[9];
    z[69]=n<T>(33806,3)*z[7] + n<T>(1,3)*z[69] - n<T>(14279,2)*z[4];
    z[69]=2562*z[10] + n<T>(5405,6)*z[3] + n<T>(1,3)*z[69] - 9790*z[2];
    z[69]=n<T>(2312,4593)*z[22] + n<T>(1,1531)*z[69];
    z[69]=z[21]*z[69];
    z[70]=n<T>(11062,1531)*z[28] + n<T>(8593,1531)*z[26] - n<T>(56083,55116)*z[23];
    z[70]=i*z[70];
    z[52]=z[52] - z[2];
    z[71]=z[4] - z[52];
    z[71]=z[14]*z[71];
    z[52]= - z[5] + z[52];
    z[52]=z[15]*z[52];
    z[53]=z[2] - z[53];
    z[53]=z[18]*z[53];
    z[72]= - z[9] - z[3];
    z[72]=z[31]*z[72];
    z[73]=z[9] + z[12];
    z[73]=z[32]*z[73];
    z[74]=z[7] + z[22];
    z[74]=z[33]*z[74];

    r += n<T>(35368,4593)*z[24] + n<T>(26965,18372)*z[25] - n<T>(5531,1531)*z[29]
       - n<T>(311531,9186)*z[30] + z[35] + n<T>(1781,1531)*z[36] - n<T>(72707,18372)
      *z[37] - n<T>(2062,1531)*z[39] + n<T>(17715,6124)*z[40] + z[41] - n<T>(22277,6124)*z[42]
     + n<T>(3812,1531)*z[43] - n<T>(2,3)*z[44]
     + n<T>(44647,9186)*
      z[46] + z[51] + n<T>(281,1531)*z[52] + n<T>(1750,1531)*z[53] + z[54] + 
      z[55] + n<T>(1,1531)*z[56] + z[57] + n<T>(1,4593)*z[58] + z[59] + 2*z[60]
       + z[61] - n<T>(1,2)*z[62] + n<T>(2,4593)*z[63] + n<T>(13091,4593)*z[64] + 
      n<T>(8467,1531)*z[65] + z[66] + n<T>(8593,3062)*z[67] - n<T>(6155,3062)*z[68]
       + z[69] + z[70] + n<T>(4062,1531)*z[71] + n<T>(11529,1531)*z[72] + n<T>(31556,4593)*z[73]
     + z[74];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf685(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf685(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
