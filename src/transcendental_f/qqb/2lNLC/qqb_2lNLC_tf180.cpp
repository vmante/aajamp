#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf180(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[37];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[7];
    z[9]=d[6];
    z[10]=d[17];
    z[11]=e[2];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[31];
    z[17]=e[3];
    z[18]=e[10];
    z[19]=e[6];
    z[20]=f[4];
    z[21]=f[12];
    z[22]=f[16];
    z[23]=f[31];
    z[24]=f[36];
    z[25]=f[39];
    z[26]=z[1]*i;
    z[27]=z[26]*z[6];
    z[28]=npow(z[6],2);
    z[29]=z[28] + 2*z[27];
    z[30]=n<T>(2,3)*z[2];
    z[31]=z[2] + 2*z[26];
    z[31]=z[31]*z[30];
    z[30]= - z[30] + z[6] + n<T>(1,3)*z[26];
    z[30]=2*z[30] - n<T>(1,3)*z[3];
    z[30]=z[3]*z[30];
    z[32]=z[26] - z[3];
    z[32]=n<T>(19,3)*z[2] - z[6] + n<T>(16,3)*z[32];
    z[32]=z[4]*z[32];
    z[30]=z[32] + z[30] + z[31] - z[29];
    z[30]=z[4]*z[30];
    z[31]= - z[8]*z[26];
    z[31]=z[31] - z[13];
    z[31]=z[10]*z[31];
    z[32]=z[26] - z[8];
    z[33]=z[12]*z[32];
    z[34]=3*z[6] + 8*z[2];
    z[34]=z[17]*z[34];
    z[35]=3*z[9] + 4*z[6];
    z[35]=z[19]*z[35];
    z[30]=z[30] + z[31] + z[33] + z[34] + z[35] - z[22];
    z[31]=11*z[2];
    z[33]= - 8*z[26] - z[31];
    z[33]=z[2]*z[33];
    z[34]= - z[6] + n<T>(4,3)*z[2];
    z[34]=z[3]*z[34];
    z[33]=2*z[34] + n<T>(1,3)*z[33] + 13*z[28] + 4*z[27];
    z[33]=z[3]*z[33];
    z[34]=z[26] + z[6];
    z[34]=2*z[34] - z[8];
    z[34]=z[8]*z[34];
    z[35]= - z[6] - z[32];
    z[35]=z[7]*z[35];
    z[29]=z[35] + z[34] - z[29];
    z[29]=z[7]*z[29];
    z[29]=z[23] + z[29] - z[25];
    z[31]= - z[31] + 4*z[3];
    z[34]= - z[26]*z[31];
    z[34]= - 4*z[13] + z[34];
    z[34]=z[5]*z[34];
    z[31]= - z[31] + 4*z[26];
    z[31]=z[11]*z[31];
    z[31]=z[34] + z[31];
    z[34]= - z[28]*z[26];
    z[35]=npow(z[6],3);
    z[34]=n<T>(2,3)*z[35] + z[34];
    z[26]=z[26] - z[2];
    z[26]= - 8*z[6] - n<T>(11,3)*z[26];
    z[26]=z[2]*z[26];
    z[26]= - 3*z[28] + z[26];
    z[26]=z[2]*z[26];
    z[27]= - 4*z[28] - z[27];
    z[28]=z[8]*z[6];
    z[27]=2*z[27] + z[28];
    z[27]=z[8]*z[27];
    z[28]=z[9] - 2*z[8];
    z[28]=z[32]*z[28];
    z[28]=n<T>(1,3)*z[13] + z[28];
    z[28]=z[9]*z[28];
    z[32]=2*z[7] - n<T>(2,3)*z[8] + n<T>(38,9)*z[4] - n<T>(2,9)*z[3] - z[6] + n<T>(5,3)*
    z[2];
    z[32]=z[13]*z[32];
    z[35]= - z[2] - z[4];
    z[35]=z[18]*z[35];
    z[36]=z[14]*i;

    r +=  - n<T>(4,3)*z[15] - 12*z[16] - n<T>(32,3)*z[20] - 13*z[21] + 4*z[24]
       + z[26] + z[27] + z[28] + 3*z[29] + 2*z[30] + n<T>(2,3)*z[31] + 
      z[32] + z[33] + 5*z[34] + n<T>(38,3)*z[35] + n<T>(10,9)*z[36];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf180(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf180(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
