#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf568(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[9];
    z[4]=d[2];
    z[5]=d[6];
    z[6]=d[5];
    z[7]=d[8];
    z[8]=i*z[1];
    z[8]=z[8] - z[4];
    z[9]=z[3] - z[2];
    z[8]=z[9]*z[8];
    z[9]= - z[5]*z[2];
    z[10]=z[5] - n<T>(1,2)*z[6];
    z[10]=z[6]*z[10];
    z[11]=z[3] - n<T>(1,2)*z[7];
    z[11]=z[7]*z[11];

    r += z[8] + z[9] + z[10] + z[11];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf568(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf568(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d
);
#endif
