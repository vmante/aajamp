#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf145(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[87];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[9];
    z[10]=d[15];
    z[11]=d[16];
    z[12]=d[4];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=d[11];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[10];
    z[32]=e[3];
    z[33]=e[6];
    z[34]=e[7];
    z[35]=e[13];
    z[36]=e[14];
    z[37]=e[12];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[20];
    z[49]=f[23];
    z[50]=f[31];
    z[51]=f[32];
    z[52]=f[34];
    z[53]=f[36];
    z[54]=f[39];
    z[55]=f[51];
    z[56]=f[55];
    z[57]=f[58];
    z[58]=f[91];
    z[59]=n<T>(1,2)*z[2];
    z[60]=z[1]*i;
    z[61]=z[59] - z[60];
    z[61]=z[61]*z[2];
    z[62]=n<T>(1,2)*z[8];
    z[63]=33 + z[8];
    z[63]=z[63]*z[62];
    z[64]=2*z[60];
    z[65]=z[64] - z[7];
    z[66]=z[65]*z[7];
    z[67]=npow(z[3],2);
    z[68]=npow(z[4],2);
    z[69]=npow(z[9],2);
    z[70]=z[15]*z[60];
    z[63]= - z[37] + z[70] + n<T>(1,3)*z[69] - n<T>(1,2)*z[68] - n<T>(3,2)*z[67]
     + 
    z[61] - z[66] + z[63];
    z[63]=z[9]*z[63];
    z[68]=2*z[8];
    z[69]=11*z[6];
    z[70]= - 8*z[60] - n<T>(13,2)*z[7];
    z[70]=n<T>(26,3)*z[12] - z[69] - 21*z[3] - z[59] + n<T>(87,2)*z[5] + 3*z[70]
    - z[68];
    z[70]=z[12]*z[70];
    z[71]=z[62] + z[60];
    z[71]=z[71]*z[8];
    z[72]=n<T>(1,2)*z[7];
    z[73]= - z[60] + z[72];
    z[73]=z[7]*z[73];
    z[74]=n<T>(1,2)*z[5];
    z[75]=z[60] + z[8];
    z[76]= - z[74] + z[75];
    z[76]=z[5]*z[76];
    z[77]=z[60] - z[7];
    z[78]= - 2*z[77] - z[6];
    z[69]=z[78]*z[69];
    z[67]=z[70] + z[69] - n<T>(41,2)*z[67] + z[76] + 5*z[73] - z[71];
    z[67]=z[12]*z[67];
    z[68]=z[68] + z[64] - z[72];
    z[68]=3*z[68] + n<T>(7,2)*z[5];
    z[68]=z[5]*z[68];
    z[69]=5*z[5];
    z[70]=2*z[2];
    z[73]=n<T>(17,4)*z[8] + n<T>(11,2) - z[60];
    z[73]=n<T>(19,4)*z[3] + z[70] + 3*z[73] - z[69];
    z[73]=z[3]*z[73];
    z[76]=z[7] + z[75];
    z[76]=n<T>(5,2)*z[2] + 3*z[76] - 11*z[5];
    z[59]=z[76]*z[59];
    z[76]=n<T>(57,2)*z[8] + z[7] - 33 - n<T>(17,2)*z[60];
    z[76]=z[8]*z[76];
    z[59]=z[73] + z[59] + z[68] + 2*z[66] + z[76];
    z[59]=z[3]*z[59];
    z[68]= - z[70] + 2*z[5];
    z[68]=z[60]*z[68];
    z[70]=7*z[8];
    z[73]=3*z[60];
    z[76]=z[73] + z[70];
    z[78]=3*z[8];
    z[76]=z[76]*z[78];
    z[79]=z[4] + z[2];
    z[80]=n<T>(11,2)*z[6] - z[5] - 4*z[60] - n<T>(9,2)*z[8] + z[79];
    z[80]=z[4]*z[80];
    z[75]= - 11*z[75] - 10*z[6];
    z[75]=z[6]*z[75];
    z[81]=npow(z[12],2);
    z[68]=z[80] + n<T>(1,2)*z[81] + z[75] + z[76] + z[68];
    z[68]=z[4]*z[68];
    z[75]=8*z[7];
    z[70]=n<T>(5,2)*z[6] + n<T>(3,2)*z[5] + z[70] + n<T>(1,2)*z[60] + z[75];
    z[70]=z[6]*z[70];
    z[61]=z[61] - z[34];
    z[66]=z[66] + z[71];
    z[61]=z[70] + 11*z[66] - 3*z[61];
    z[61]=z[6]*z[61];
    z[66]=z[74] - z[78] - n<T>(7,2)*z[60] + n<T>(8,3)*z[7];
    z[66]=z[5]*z[66];
    z[70]=n<T>(13,3)*z[7];
    z[70]=z[70]*z[65];
    z[71]= - 14*z[8] + z[64] + z[72];
    z[71]=z[71]*z[78];
    z[66]=z[66] + z[70] + z[71];
    z[66]=z[5]*z[66];
    z[71]=13*z[60];
    z[72]=z[71] - 3*z[7];
    z[62]=z[72]*z[62];
    z[72]=n<T>(55,2)*z[60] + z[75];
    z[72]= - 8*z[2] + n<T>(73,6)*z[5] + n<T>(1,3)*z[72] - n<T>(7,4)*z[8];
    z[72]=z[2]*z[72];
    z[74]= - 53*z[60] - 16*z[7];
    z[74]= - n<T>(2,3)*z[5] + n<T>(1,3)*z[74] - n<T>(3,2)*z[8];
    z[74]=z[5]*z[74];
    z[62]=z[72] + z[74] - z[70] + z[62];
    z[62]=z[2]*z[62];
    z[70]=z[3] - z[5];
    z[72]=z[60]*z[70];
    z[72]= - z[22] + z[72];
    z[72]=z[10]*z[72];
    z[70]=z[60] + z[70];
    z[70]=z[18]*z[70];
    z[70]=z[53] + z[72] + z[70];
    z[72]= - 10*z[60] + n<T>(19,2)*z[7];
    z[72]=z[7]*z[72];
    z[74]=npow(z[6],2);
    z[75]= - 23*z[7] - 3*z[5];
    z[75]=n<T>(1,2)*z[75] + 5*z[13];
    z[75]=z[13]*z[75];
    z[72]=z[75] + n<T>(15,4)*z[22] + z[72] + n<T>(3,2)*z[74];
    z[72]=z[13]*z[72];
    z[69]= - 31*z[2] + 26*z[60] + z[69];
    z[69]=z[17]*z[69];
    z[69]=z[69] - z[47];
    z[74]= - z[7]*z[60];
    z[74]=z[74] - z[22];
    z[71]=z[13]*z[71];
    z[71]=9*z[74] + z[71];
    z[71]=z[14]*z[71];
    z[64]=z[64] - z[79];
    z[64]=z[16]*z[64];
    z[74]= - z[52] + z[51] + z[49] + z[46];
    z[75]=z[44] + z[41];
    z[76]=z[50] + z[27];
    z[78]=z[6] + z[9];
    z[78]=z[35]*z[78];
    z[78]=z[78] - z[24];
    z[73]=z[73] - z[7];
    z[73]=z[73]*npow(z[7],2);
    z[73]=z[73] + z[39];
    z[79]= - n<T>(130,3)*z[28] - n<T>(55,6)*z[26] + n<T>(11,12)*z[23];
    z[79]=i*z[79];
    z[80]=21*z[60] - n<T>(71,6)*z[8];
    z[80]=z[80]*npow(z[8],2);
    z[81]= - n<T>(343,18)*z[5] + n<T>(59,18)*z[7] + 73*z[8];
    z[81]= - n<T>(19,12)*z[9] - n<T>(65,24)*z[4] - n<T>(71,6)*z[12] + n<T>(31,4)*z[6]
     + 
   n<T>(33,4)*z[3] + n<T>(1,4)*z[81] - n<T>(31,9)*z[2];
    z[81]=z[22]*z[81];
    z[82]= - z[2] + z[7] - n<T>(23,2)*z[8];
    z[82]=3*z[82] - n<T>(101,2)*z[3];
    z[82]=z[31]*z[82];
    z[65]= - z[2] + z[65];
    z[65]=z[20]*z[65];
    z[77]=9*z[77] + 13*z[13];
    z[77]=z[21]*z[77];
    z[83]=z[4]*z[60];
    z[83]=z[83] + z[22];
    z[83]=z[11]*z[83];
    z[60]=z[60] - z[4];
    z[60]=z[19]*z[60];
    z[84]=57*z[3] + 26*z[12];
    z[84]=z[32]*z[84];
    z[85]=25*z[12] + 9*z[13];
    z[85]=z[33]*z[85];
    z[86]= - 16*z[8] - 15*z[9];
    z[86]=z[36]*z[86];

    r +=  - n<T>(35,36)*z[25] + n<T>(83,3)*z[29] - n<T>(274,9)*z[30] - 13*z[38] - 5
      *z[40] - n<T>(59,6)*z[42] - n<T>(29,6)*z[43] - 2*z[45] + 4*z[48] - 11*
      z[54] - 10*z[55] - n<T>(21,2)*z[56] + 7*z[57] + z[58] + z[59] + z[60]
       + z[61] + z[62] + z[63] + 14*z[64] + n<T>(10,3)*z[65] + z[66] + 
      z[67] + z[68] + n<T>(1,3)*z[69] + 6*z[70] + z[71] + z[72] + n<T>(1,2)*
      z[73] + n<T>(3,2)*z[74] - n<T>(87,2)*z[75] + 8*z[76] + z[77] + 3*z[78] + 
      z[79] + z[80] + z[81] + z[82] + z[83] + z[84] + z[85] + z[86];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf145(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf145(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
