#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf295(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[51];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[7];
    z[9]=d[6];
    z[10]=d[17];
    z[11]=d[9];
    z[12]=d[11];
    z[13]=e[2];
    z[14]=e[9];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[31];
    z[19]=e[3];
    z[20]=e[10];
    z[21]=d[2];
    z[22]=e[6];
    z[23]=e[7];
    z[24]=e[12];
    z[25]=e[13];
    z[26]=e[14];
    z[27]=f[4];
    z[28]=f[12];
    z[29]=f[16];
    z[30]=f[31];
    z[31]=f[36];
    z[32]=f[39];
    z[33]=f[51];
    z[34]=f[55];
    z[35]=2*z[4];
    z[36]=2*z[3];
    z[37]= - z[36] + z[4];
    z[37]=z[37]*z[35];
    z[38]= - n<T>(7,2)*z[3] + z[7] + n<T>(5,2)*z[8];
    z[38]= - n<T>(10,3)*z[6] + n<T>(3,2)*z[2] + 3*z[38] + z[35];
    z[38]=z[6]*z[38];
    z[39]=npow(z[7],2);
    z[40]=3*z[39];
    z[41]=npow(z[3],2);
    z[42]=n<T>(1,3)*z[15];
    z[43]= - 2*z[22] - z[19];
    z[44]= - 6*z[7] - z[8];
    z[44]=z[8]*z[44];
    z[45]=npow(z[2],2);
    z[37]=z[38] + 7*z[45] + z[37] + 2*z[41] + z[44] + z[40] + 4*z[43] + 
    z[42];
    z[37]=z[6]*z[37];
    z[38]=3*z[7];
    z[35]=z[35] - z[36] + z[38] + z[8];
    z[35]=2*z[35] + 3*z[6];
    z[35]=z[6]*z[35];
    z[43]=11*z[2];
    z[44]=z[4] - z[3];
    z[44]= - 11*z[5] - 4*z[44];
    z[44]=2*z[44] + z[43];
    z[45]=n<T>(1,3)*z[2];
    z[44]=z[44]*z[45];
    z[46]= - z[38] + z[10] + z[9];
    z[46]=z[8]*z[46];
    z[46]=z[46] - z[14] - n<T>(4,3)*z[13];
    z[47]=2*z[12] - z[11];
    z[47]=z[11]*z[47];
    z[48]=2*z[9];
    z[49]= - z[10]*z[48];
    z[50]=z[3]*z[5];
    z[36]= - z[36] - 13*z[4];
    z[36]=z[4]*z[36];
    z[35]=z[35] + z[44] + n<T>(2,3)*z[36] + n<T>(8,3)*z[50] + z[40] + z[49] + 
    z[47] + 2*z[46];
    z[35]=z[1]*z[35];
    z[35]= - n<T>(10,9)*z[16] + z[35];
    z[35]=i*z[35];
    z[36]=npow(z[9],2);
    z[40]=z[42] + z[14] + z[36];
    z[38]= - z[48] + z[38];
    z[38]=z[8]*z[38];
    z[38]=z[38] + 2*z[40] - n<T>(7,2)*z[39];
    z[38]=z[8]*z[38];
    z[39]= - z[43] + 11*z[3] - z[4];
    z[39]=z[39]*z[45];
    z[40]=8*z[3] - n<T>(67,2)*z[4];
    z[40]=z[4]*z[40];
    z[39]=z[39] + n<T>(1,3)*z[40] - n<T>(8,3)*z[41] - n<T>(5,3)*z[15] - n<T>(22,3)*z[13]
    + n<T>(29,3)*z[20] - 13*z[19];
    z[39]=z[2]*z[39];
    z[40]=npow(z[21],2);
    z[43]=n<T>(1,3)*z[11];
    z[44]=z[21] - z[43];
    z[44]=z[11]*z[44];
    z[40]=z[44] - z[40] - 2*z[24] - z[26] - z[25];
    z[40]=z[11]*z[40];
    z[44]=2*z[15];
    z[43]= - n<T>(1,3)*z[9] + z[43] + z[10] + n<T>(4,3)*z[5];
    z[43]=z[43]*z[44];
    z[45]=z[7]*z[21];
    z[44]= - z[44] + n<T>(1,2)*z[45];
    z[44]=z[7]*z[44];
    z[45]=z[20] - z[42];
    z[41]=16*z[45] + z[41];
    z[45]=z[21] + n<T>(49,3)*z[3];
    z[45]=z[4]*z[45];
    z[41]=n<T>(2,3)*z[41] + n<T>(1,2)*z[45];
    z[41]=z[4]*z[41];
    z[45]=z[30] + z[31];
    z[46]= - z[34] + z[33];
    z[36]= - n<T>(2,3)*z[36] - 2*z[14] + z[23] - 5*z[22];
    z[36]=z[9]*z[36];
    z[42]=4*z[13] + z[42];
    z[42]=z[3]*z[42];

    r += n<T>(4,3)*z[17] + 12*z[18] + n<T>(49,6)*z[27] + n<T>(21,2)*z[28] + 2*z[29]
       + 3*z[32] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + 
      z[41] + n<T>(2,3)*z[42] + z[43] + z[44] - n<T>(7,2)*z[45] + n<T>(1,2)*z[46];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf295(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf295(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
