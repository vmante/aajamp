#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf20(
  const std::array<std::complex<T>,25>& d
) {
    return d[8];
}

template std::complex<double> qqb_2lNLC_tf20(
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf20(
  const std::array<std::complex<dd_real>,25>& d
);
#endif
