#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1085(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[64];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[4];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[1];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[6];
    z[17]=e[7];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[10];
    z[30]=f[3];
    z[31]=f[4];
    z[32]=f[11];
    z[33]=f[12];
    z[34]=f[13];
    z[35]=f[16];
    z[36]=f[17];
    z[37]=f[30];
    z[38]=f[31];
    z[39]=f[33];
    z[40]=f[36];
    z[41]=f[38];
    z[42]=f[39];
    z[43]=n<T>(1,2)*z[2];
    z[44]=z[1]*i;
    z[45]=z[43] - z[44];
    z[46]=n<T>(1,2)*z[4];
    z[47]=n<T>(1,2)*z[7];
    z[48]=z[47] + z[46] - z[45];
    z[48]=z[2]*z[48];
    z[49]=z[44] - z[7];
    z[50]=n<T>(1,2)*z[5];
    z[51]=z[49] + z[50];
    z[51]=z[51]*z[5];
    z[52]=2*z[44];
    z[53]=z[52] - z[7];
    z[54]= - z[46] - z[53];
    z[54]=z[7]*z[54];
    z[55]= - z[4]*z[44];
    z[56]=z[2] - z[4];
    z[57]= - z[56] + 3*z[6];
    z[58]=z[5] - z[57];
    z[58]=z[6]*z[58];
    z[48]=n<T>(1,2)*z[58] + z[51] + z[48] + z[55] + z[54];
    z[48]=z[6]*z[48];
    z[52]=z[52] - 5*z[7];
    z[52]=z[7]*z[52];
    z[54]=z[7] - z[43];
    z[54]=z[2]*z[54];
    z[55]=z[5] - z[49];
    z[55]=z[5]*z[55];
    z[58]=npow(z[6],2);
    z[59]= - z[44] - z[7];
    z[59]= - n<T>(7,6)*z[11] + z[6] + n<T>(5,6)*z[59] + z[5];
    z[59]=z[11]*z[59];
    z[52]=z[59] + z[58] + n<T>(2,3)*z[55] + n<T>(1,3)*z[52] + z[54];
    z[52]=z[11]*z[52];
    z[54]= - 7*z[4] + z[47];
    z[54]=z[54]*z[47];
    z[55]=4*z[44];
    z[58]=z[55] + n<T>(25,8)*z[4];
    z[58]=z[4]*z[58];
    z[54]=z[58] + z[54];
    z[54]=z[7]*z[54];
    z[58]=z[4] + z[8];
    z[58]=z[44]*z[58];
    z[58]=z[20] + z[58];
    z[58]=z[10]*z[58];
    z[59]=npow(z[4],3);
    z[60]=z[44] - z[4];
    z[61]=z[8] - z[60];
    z[61]=z[15]*z[61];
    z[54]=z[61] + z[58] - n<T>(1,4)*z[59] + z[54];
    z[58]=n<T>(2,3)*z[7];
    z[59]= - n<T>(1,3)*z[44] - z[4];
    z[59]= - n<T>(11,6)*z[5] + z[43] + n<T>(1,2)*z[59] + z[58];
    z[59]=z[5]*z[59];
    z[46]=z[46] - z[44];
    z[61]=z[4]*z[46];
    z[53]= - z[53]*z[58];
    z[58]=z[2]*z[49];
    z[53]=z[59] + z[58] + z[61] + z[53];
    z[53]=z[5]*z[53];
    z[58]=z[47] + 11*z[44] - 13*z[4];
    z[58]=z[58]*z[47];
    z[59]= - 7*z[44] - n<T>(1,4)*z[4];
    z[59]=z[4]*z[59];
    z[58]=n<T>(13,3)*z[59] + z[58];
    z[59]= - 2*z[2] + n<T>(185,4)*z[44] + 4*z[4];
    z[59]=z[2]*z[59];
    z[58]=n<T>(1,2)*z[58] + n<T>(1,3)*z[59];
    z[56]= - z[6]*z[56];
    z[59]= - n<T>(185,6)*z[2] + n<T>(109,3)*z[4] + z[47];
    z[59]=n<T>(1,2)*z[59] - z[3];
    z[59]=z[3]*z[59];
    z[51]=n<T>(1,6)*z[59] + z[56] + n<T>(1,3)*z[58] - z[51];
    z[51]=z[3]*z[51];
    z[56]=n<T>(1,3)*z[4];
    z[55]=z[55] - z[4];
    z[55]=z[55]*z[56];
    z[58]= - z[4] + z[7];
    z[58]=z[58]*z[43];
    z[59]= - z[4]*z[47];
    z[61]=5*z[8] - z[44] - z[4];
    z[61]=z[8]*z[61];
    z[55]=n<T>(1,6)*z[61] + z[58] + z[55] + z[59];
    z[55]=z[8]*z[55];
    z[46]= - z[46]*z[56];
    z[50]=z[50] + z[60];
    z[50]=z[5]*z[50];
    z[58]= - 2*z[60] - z[8];
    z[58]=z[8]*z[58];
    z[59]= - n<T>(5,3)*z[60] + z[5];
    z[59]=n<T>(1,2)*z[59] - n<T>(4,3)*z[8];
    z[59]=z[9]*z[59];
    z[46]=z[59] + n<T>(2,3)*z[58] + z[46] + z[50];
    z[46]=z[9]*z[46];
    z[50]= - 5*z[44] + n<T>(7,2)*z[7];
    z[50]=z[50]*z[47];
    z[58]= - 173*z[44] - 53*z[4];
    z[58]=9*z[2] + n<T>(1,9)*z[58] - z[7];
    z[58]=z[58]*z[43];
    z[59]=67*z[44];
    z[60]=z[59] - 11*z[4];
    z[60]=z[4]*z[60];
    z[50]=z[58] + n<T>(1,9)*z[60] + z[50];
    z[43]=z[50]*z[43];
    z[50]=z[7] + z[11];
    z[44]=z[44]*z[50];
    z[44]=z[20] + z[44];
    z[44]=z[12]*z[44];
    z[50]=z[11] - z[49];
    z[50]=z[19]*z[50];
    z[44]=z[44] + z[50];
    z[50]= - 2*z[11] + z[49] - z[57];
    z[50]=z[17]*z[50];
    z[57]=n<T>(1,2)*z[3] + z[45];
    z[57]=z[13]*z[57];
    z[45]= - z[47] - z[45];
    z[45]=z[18]*z[45];
    z[47]=z[42] + z[38] + z[35] + z[33];
    z[58]=z[34] - z[25];
    z[60]=z[40] + z[32];
    z[61]=z[41] - z[39];
    z[62]=n<T>(137,18)*z[26] + n<T>(137,36)*z[24] + n<T>(1,6)*z[21];
    z[62]=i*z[62];
    z[56]= - z[56] + n<T>(349,8)*z[7];
    z[56]= - z[9] - n<T>(25,2)*z[5] + n<T>(1,4)*z[56] - n<T>(20,3)*z[2];
    z[56]=n<T>(1,4)*z[8] - n<T>(803,864)*z[3] - n<T>(5,4)*z[6] + n<T>(1,9)*z[56];
    z[56]=z[20]*z[56];
    z[49]= - n<T>(8,3)*z[11] - z[3] - n<T>(11,3)*z[5] + z[2] + z[49];
    z[49]=z[16]*z[49];
    z[59]=n<T>(103,2)*z[2] - z[59] + n<T>(31,2)*z[4];
    z[59]=z[14]*z[59];
    z[63]=z[8] + z[9];
    z[63]=z[29]*z[63];

    r +=  - 2*z[22] + n<T>(137,432)*z[23] - n<T>(137,36)*z[27] - n<T>(617,36)*z[28]
       + n<T>(109,36)*z[30] + n<T>(5,6)*z[31] + n<T>(1,24)*z[36] + n<T>(2,3)*z[37]
     +  z[43] + n<T>(5,3)*z[44] + n<T>(9,4)*z[45] + z[46] + n<T>(1,2)*z[47] + z[48]
       + z[49] + z[50] + z[51] + z[52] + z[53] + n<T>(1,3)*z[54] + z[55] + 
      z[56] + n<T>(221,36)*z[57] + n<T>(137,72)*z[58] + n<T>(1,18)*z[59] - n<T>(7,6)*
      z[60] - 4*z[61] + z[62] + n<T>(4,3)*z[63];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1085(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1085(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
