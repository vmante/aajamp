#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf67(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[30];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[79];
    z[3]=c[11];
    z[4]=d[1];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=c[12];
    z[8]=f[70];
    z[9]=d[5];
    z[10]=d[6];
    z[11]=g[173];
    z[12]=g[181];
    z[13]=g[191];
    z[14]=g[243];
    z[15]=g[251];
    z[16]=g[254];
    z[17]=g[272];
    z[18]=g[276];
    z[19]=g[395];
    z[20]=g[397];
    z[21]=g[398];
    z[22]=z[9] - z[10];
    z[23]=z[22] - z[6];
    z[24]=n<T>(1,2)*z[4];
    z[25]=i*z[1];
    z[23]=z[24] + z[25] - z[5] - n<T>(1,2)*z[23];
    z[23]=z[2]*z[23];
    z[23]=z[23] + z[18];
    z[25]=i*z[3];
    z[26]= - 3*z[7] + n<T>(1,9)*z[25];
    z[27]=n<T>(3,4)*z[8] - z[26];
    z[24]=z[27]*z[24];
    z[27]= - z[17] + z[11] + z[12];
    z[28]=z[15] - z[16];
    z[29]=z[13] + z[19];
    z[25]=n<T>(1,6)*z[25] - n<T>(9,2)*z[7];
    z[25]=z[5]*z[25];
    z[22]=z[8]*z[22];
    z[26]= - n<T>(3,8)*z[8] - z[26];
    z[26]=z[6]*z[26];

    r += n<T>(9,2)*z[14] + n<T>(1,2)*z[20] + z[21] - n<T>(9,8)*z[22] + n<T>(9,4)*z[23]
       + z[24] + z[25] + z[26] + n<T>(3,2)*z[27] + n<T>(3,4)*z[28] + n<T>(1,4)*
      z[29];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf67(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf67(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
