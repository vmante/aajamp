#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf77(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[84];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[77];
    z[3]=f[79];
    z[4]=c[11];
    z[5]=d[1];
    z[6]=d[2];
    z[7]=d[4];
    z[8]=d[5];
    z[9]=d[6];
    z[10]=d[7];
    z[11]=d[8];
    z[12]=d[9];
    z[13]=c[12];
    z[14]=c[13];
    z[15]=c[20];
    z[16]=c[23];
    z[17]=d[0];
    z[18]=f[22];
    z[19]=f[24];
    z[20]=f[42];
    z[21]=f[44];
    z[22]=f[61];
    z[23]=f[62];
    z[24]=f[69];
    z[25]=f[70];
    z[26]=f[80];
    z[27]=d[3];
    z[28]=g[24];
    z[29]=g[27];
    z[30]=g[38];
    z[31]=g[40];
    z[32]=g[48];
    z[33]=g[50];
    z[34]=g[79];
    z[35]=g[82];
    z[36]=g[90];
    z[37]=g[92];
    z[38]=g[99];
    z[39]=g[101];
    z[40]=g[128];
    z[41]=g[131];
    z[42]=g[140];
    z[43]=g[145];
    z[44]=g[147];
    z[45]=g[170];
    z[46]=g[173];
    z[47]=g[181];
    z[48]=g[183];
    z[49]=g[189];
    z[50]=g[191];
    z[51]=g[212];
    z[52]=g[214];
    z[53]=g[220];
    z[54]=g[224];
    z[55]=g[243];
    z[56]=g[245];
    z[57]=g[251];
    z[58]=g[254];
    z[59]=g[270];
    z[60]=g[276];
    z[61]=g[278];
    z[62]=g[281];
    z[63]=g[324];
    z[64]=g[405];
    z[65]=g[407];
    z[66]=g[408];
    z[67]=n<T>(3,2)*z[2];
    z[68]=n<T>(3,4)*z[3];
    z[69]=z[67] - z[68];
    z[69]=z[1]*z[69];
    z[70]= - n<T>(1,2)*z[10] - z[6];
    z[70]=n<T>(1,6)*z[9] + n<T>(1,9)*z[70];
    z[70]=z[4]*z[70];
    z[71]= - 5*z[15] + n<T>(1,4)*z[16];
    z[71]= - 3*z[14] + n<T>(1,4)*z[71];
    z[71]=3*z[71];
    z[72]=z[71] + n<T>(7,144)*z[4];
    z[72]=z[12]*z[72];
    z[73]= - z[15] + n<T>(1,20)*z[16];
    z[74]= - n<T>(12,5)*z[14] + z[73];
    z[74]=3*z[74] + n<T>(41,180)*z[4];
    z[74]=z[11]*z[74];
    z[73]=n<T>(1,4)*z[73] - n<T>(3,5)*z[14];
    z[73]=3*z[73] + n<T>(71,720)*z[4];
    z[73]=z[7]*z[73];
    z[75]= - z[71] - n<T>(23,144)*z[4];
    z[75]=z[5]*z[75];
    z[71]= - z[71] - n<T>(31,144)*z[4];
    z[71]=z[8]*z[71];
    z[69]=z[71] + z[75] + z[73] + z[74] + z[72] + z[69] + z[70];
    z[69]=i*z[69];
    z[70]=z[21] + z[20];
    z[71]=n<T>(5,2)*z[18];
    z[72]=z[71] + n<T>(1,2)*z[23];
    z[73]=n<T>(5,2)*z[24];
    z[70]= - z[73] + z[72] - 3*z[70];
    z[74]=n<T>(3,4)*z[2];
    z[70]=z[74] + n<T>(3,2)*z[70] - 7*z[13];
    z[70]=z[11]*z[70];
    z[75]=n<T>(7,2)*z[20];
    z[76]=n<T>(5,2)*z[26];
    z[77]=z[75] - z[76];
    z[78]= - z[22] + n<T>(1,2)*z[24];
    z[79]= - z[25] + n<T>(7,2)*z[19];
    z[80]=z[3] + z[21] - n<T>(7,2)*z[18] + z[23] - z[78] - z[79] + z[77];
    z[74]= - z[74] + 5*z[13] + n<T>(3,2)*z[80];
    z[74]=z[8]*z[74];
    z[70]= - z[65] - z[70] - z[74] + z[50] + z[54];
    z[74]=z[2] + z[3];
    z[76]= - z[76] + n<T>(7,2)*z[21];
    z[80]= - z[18] + n<T>(1,2)*z[20];
    z[81]= - z[79] + n<T>(1,2)*z[22];
    z[74]=n<T>(7,2)*z[24] - z[23] + z[81] + z[80] - z[76] + n<T>(1,2)*z[74];
    z[74]=z[17]*z[74];
    z[74]=z[57] + z[74] + z[44];
    z[72]= - n<T>(3,2)*z[21] + z[72] - z[81] - z[77];
    z[72]= - z[68] + n<T>(1,2)*z[72] - 3*z[13];
    z[72]=z[9]*z[72];
    z[72]= - z[42] + z[46] + z[47] - z[49] - z[72] + z[38] - z[40] - 
    z[41] + z[63] + z[52] - z[53] + z[55] + z[56];
    z[73]= - z[73] + z[79] + z[76];
    z[73]=n<T>(1,4)*z[73] + z[13];
    z[73]=z[6]*z[73];
    z[73]=z[73] + z[51];
    z[76]=n<T>(1,2)*z[27];
    z[77]= - z[6] + z[76] + z[10];
    z[77]=z[77]*z[68];
    z[76]=z[76] - z[10];
    z[76]=n<T>(1,2)*z[76] - z[6];
    z[67]=z[76]*z[67];
    z[76]=n<T>(1,2)*z[21] - n<T>(3,2)*z[23] - z[80] + z[78];
    z[78]=2*z[13];
    z[76]=n<T>(3,4)*z[76] + z[78];
    z[76]=z[12]*z[76];
    z[79]=z[3] + z[21] - z[24] - z[23] - z[22];
    z[78]=n<T>(9,8)*z[2] - z[78] + n<T>(3,8)*z[79];
    z[78]=z[7]*z[78];
    z[71]=n<T>(5,2)*z[21] + 3*z[24] - z[71] + z[22] + z[75];
    z[68]=n<T>(3,8)*z[2] - z[68] + n<T>(3,4)*z[71] + z[13];
    z[68]=z[5]*z[68];
    z[71]= - z[61] + z[28] + z[31] + z[36] - z[59];
    z[75]=z[37] + z[29] + z[30] + z[34] + z[35];
    z[79]= - z[64] + z[43] - z[58];
    z[80]=z[45] + z[48];
    z[81]=z[32] - z[33];
    z[82]=z[20] + z[22];
    z[82]= - n<T>(3,8)*z[82];
    z[82]=z[27]*z[82];
    z[83]= - z[24] + z[23] + z[18];
    z[83]=n<T>(3,2)*z[13] + n<T>(3,4)*z[83];
    z[83]=z[10]*z[83];

    r += 2*z[39] - n<T>(9,4)*z[60] + n<T>(5,4)*z[62] + z[66] + z[67] + z[68] + 
      z[69] - n<T>(1,2)*z[70] + n<T>(15,4)*z[71] - n<T>(3,2)*z[72] + 3*z[73] + n<T>(3,4)
      *z[74] + n<T>(21,4)*z[75] + z[76] + z[77] + z[78] - n<T>(1,4)*z[79] - n<T>(9,2)*z[80]
     - n<T>(7,4)*z[81]
     + z[82]
     + z[83];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf77(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf77(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
