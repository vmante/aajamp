#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf176(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[38];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[7];
    z[9]=d[6];
    z[10]=d[17];
    z[11]=e[2];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[31];
    z[17]=e[3];
    z[18]=e[10];
    z[19]=e[6];
    z[20]=f[4];
    z[21]=f[12];
    z[22]=f[16];
    z[23]=f[31];
    z[24]=f[33];
    z[25]=f[36];
    z[26]=f[39];
    z[27]=z[1]*i;
    z[28]=z[27]*z[6];
    z[29]=npow(z[6],2);
    z[28]=z[28] + n<T>(1,2)*z[29];
    z[30]=3*z[6];
    z[31]=z[30] + z[27];
    z[31]= - n<T>(1,4)*z[3] + n<T>(1,2)*z[31] - z[2];
    z[31]=z[3]*z[31];
    z[30]= - z[30] + 11*z[27];
    z[30]= - n<T>(11,2)*z[3] + n<T>(1,2)*z[30] + 7*z[2];
    z[30]=z[4]*z[30];
    z[32]=z[27] + n<T>(1,2)*z[2];
    z[32]=z[2]*z[32];
    z[30]=n<T>(1,2)*z[30] + z[31] - n<T>(3,2)*z[28] + z[32];
    z[30]=z[4]*z[30];
    z[31]=n<T>(1,2)*z[6];
    z[31]=z[27]*z[31];
    z[31]=z[29] + z[31];
    z[32]= - z[27] - n<T>(3,4)*z[2];
    z[32]=z[2]*z[32];
    z[33]= - n<T>(3,4)*z[6] + z[2];
    z[33]=z[3]*z[33];
    z[31]=z[33] + 3*z[31] + z[32];
    z[31]=z[3]*z[31];
    z[32]= - z[3] + n<T>(3,2)*z[2];
    z[33]=z[27]*z[32];
    z[33]= - z[13] + z[33];
    z[33]=z[5]*z[33];
    z[32]=z[27] + z[32];
    z[32]=z[11]*z[32];
    z[34]=z[6] + n<T>(7,2)*z[2];
    z[34]=z[17]*z[34];
    z[35]=2*z[6] + z[9];
    z[35]=z[19]*z[35];
    z[30]=z[31] + z[30] + z[33] + z[32] + z[34] + z[35];
    z[31]= - z[27] + n<T>(1,2)*z[8];
    z[32]=z[6] - z[31];
    z[32]=z[8]*z[32];
    z[28]=z[32] - z[28];
    z[32]=2*z[8];
    z[33]=z[32] - n<T>(5,2)*z[6] - 2*z[27];
    z[33]=z[7]*z[33];
    z[28]=5*z[28] + z[33];
    z[28]=z[7]*z[28];
    z[33]=z[27] - z[8];
    z[34]= - z[33]*z[32];
    z[35]=npow(z[7],2);
    z[32]= - z[32] + n<T>(1,2)*z[9];
    z[32]=z[9]*z[32];
    z[32]=z[32] + z[34] + n<T>(1,2)*z[35];
    z[32]=z[9]*z[32];
    z[34]=z[9] - z[8];
    z[34]=z[27]*z[34];
    z[34]= - z[13] + z[34];
    z[34]=z[10]*z[34];
    z[33]=z[9] + z[33];
    z[33]=z[12]*z[33];
    z[33]=z[34] + z[33] + z[25] + z[23];
    z[34]=z[29]*z[27];
    z[27]= - 7*z[6] - 3*z[27];
    z[27]=n<T>(3,4)*z[27] + z[2];
    z[27]=z[2]*z[27];
    z[27]= - n<T>(3,2)*z[29] + z[27];
    z[27]=z[2]*z[27];
    z[31]=z[6]*z[31];
    z[29]= - 5*z[29] + z[31];
    z[29]=z[8]*z[29];
    z[31]=npow(z[6],3);
    z[31]=z[31] - z[22];
    z[35]= - z[3] + n<T>(1,6)*z[6] + 5*z[2];
    z[35]=n<T>(17,12)*z[9] + n<T>(11,3)*z[7] - z[8] + n<T>(1,2)*z[35] + 7*z[4];
    z[35]=z[13]*z[35];
    z[36]= - z[2] - z[4];
    z[36]=z[18]*z[36];
    z[37]=z[14]*i;

    r +=  - n<T>(3,4)*z[15] - n<T>(111,8)*z[16] - n<T>(33,4)*z[20] - 9*z[21] + 
      z[24] - n<T>(5,2)*z[26] + z[27] + z[28] + z[29] + 3*z[30] + n<T>(9,4)*
      z[31] + z[32] + 2*z[33] - 4*z[34] + n<T>(1,2)*z[35] + n<T>(21,2)*z[36] + 
      n<T>(7,8)*z[37];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf176(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf176(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
