#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf250(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[31];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[15];
    z[4]=d[2];
    z[5]=d[5];
    z[6]=d[8];
    z[7]=d[4];
    z[8]=e[14];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[31];
    z[13]=d[3];
    z[14]=e[2];
    z[15]=e[3];
    z[16]=e[10];
    z[17]=d[9];
    z[18]=f[4];
    z[19]=f[12];
    z[20]=f[51];
    z[21]=f[55];
    z[22]=f[58];
    z[23]=i*z[1];
    z[24]=z[23] + n<T>(1,2)*z[5];
    z[25]=3*z[5];
    z[24]=z[24]*z[25];
    z[25]=3*z[4];
    z[26]= - n<T>(1,2)*z[4] + z[23] - z[5];
    z[26]=z[26]*z[25];
    z[27]=3*z[13];
    z[25]=z[2] + z[25] - z[27] + z[5];
    z[25]=n<T>(3,2)*z[25] - 2*z[6];
    z[25]=z[6]*z[25];
    z[28]=npow(z[2],2);
    z[28]=z[28] - z[16];
    z[25]=z[25] + z[26] + z[24] + n<T>(5,3)*z[9] - 4*z[8] + 3*z[28];
    z[25]=z[6]*z[25];
    z[26]=z[13] + z[23];
    z[26]=3*z[26] - 5*z[2];
    z[26]=z[2]*z[26];
    z[26]=z[26] - z[9];
    z[28]=z[3]*z[23];
    z[28]= - z[28] + z[15] - z[14] - 2*z[16];
    z[29]=npow(z[7],2);
    z[29]=3*z[29];
    z[26]= - z[29] + 3*z[28] + n<T>(1,2)*z[26];
    z[26]=z[2]*z[26];
    z[28]=z[11] - z[20] + z[22] - z[21];
    z[30]= - z[8]*z[17];
    z[28]=z[30] + n<T>(1,4)*z[12] - n<T>(3,2)*z[18] - z[19] + n<T>(1,2)*z[28];
    z[30]=n<T>(3,2)*z[5];
    z[23]=z[23]*z[30];
    z[23]= - z[9] + z[23];
    z[23]=z[5]*z[23];
    z[30]=z[4]*z[30];
    z[24]=z[30] - z[24] + z[8] + n<T>(1,3)*z[9];
    z[24]=z[4]*z[24];
    z[27]=z[7]*z[27];
    z[27]=z[27] + 3*z[15] - z[9];
    z[27]=z[7]*z[27];
    z[29]= - z[8] - z[29];
    z[29]=z[1]*z[29];
    z[29]= - n<T>(7,12)*z[10] + z[29];
    z[29]=i*z[29];

    r += z[23] + z[24] + z[25] + z[26] + z[27] + 3*z[28] + z[29];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf250(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf250(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
