#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1054(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[40];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[2];
    z[7]=d[5];
    z[8]=d[11];
    z[9]=d[4];
    z[10]=d[9];
    z[11]=e[2];
    z[12]=e[12];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=d[7];
    z[16]=e[10];
    z[17]=e[14];
    z[18]=f[4];
    z[19]=f[6];
    z[20]=f[12];
    z[21]=f[16];
    z[22]=f[50];
    z[23]=f[51];
    z[24]=f[55];
    z[25]=f[56];
    z[26]=f[58];
    z[27]=2*z[4];
    z[28]=z[1]*i;
    z[29]=z[27]*z[28];
    z[30]=npow(z[4],2);
    z[31]=5*z[2] - z[4] + 2*z[28];
    z[31]=z[2]*z[31];
    z[31]=z[31] - 5*z[30] - z[29];
    z[31]=z[2]*z[31];
    z[32]=z[28]*z[4];
    z[33]= - z[32] + n<T>(7,2)*z[30];
    z[34]=z[28] + z[4];
    z[35]=2*z[34] - z[2];
    z[35]=z[2]*z[35];
    z[36]=n<T>(1,2)*z[4];
    z[37]=z[36] - 2*z[2];
    z[37]=z[3]*z[37];
    z[35]=z[37] + z[35] + z[33];
    z[35]=z[3]*z[35];
    z[37]=z[28] - z[6];
    z[36]= - z[36] - z[37];
    z[36]=z[6]*z[36];
    z[33]=z[36] - z[33];
    z[33]=z[6]*z[33];
    z[36]=npow(z[4],3);
    z[38]=n<T>(1,2)*z[6];
    z[39]= - z[9] + z[7] - n<T>(4,3)*z[10] - z[38] + z[2] + n<T>(1,6)*z[3];
    z[39]=z[13]*z[39];
    z[31]=z[39] + z[33] + z[35] + 4*z[36] + z[31];
    z[33]=z[3] + z[2];
    z[35]=z[28]*z[33];
    z[35]=z[13] + z[35];
    z[35]=z[5]*z[35];
    z[36]=z[10] + z[6];
    z[36]= - z[28]*z[36];
    z[36]= - z[13] + z[36];
    z[36]=z[8]*z[36];
    z[28]= - z[28] + z[33];
    z[28]=z[11]*z[28];
    z[33]=z[10] - z[37];
    z[33]=z[12]*z[33];
    z[28]= - z[22] + z[35] + z[36] + z[28] + z[33];
    z[27]= - z[27] + z[6];
    z[27]=z[6]*z[27];
    z[27]=z[27] + z[30] + z[29];
    z[29]= - z[10]*z[34];
    z[27]=n<T>(1,3)*z[27] + z[29];
    z[27]=z[10]*z[27];
    z[29]=z[32] + n<T>(1,2)*z[30];
    z[30]= - z[38] + z[34];
    z[30]=z[6]*z[30];
    z[32]=z[6] - z[34];
    z[32]=z[7]*z[32];
    z[30]=n<T>(1,2)*z[32] + z[30] - z[29];
    z[30]=z[7]*z[30];
    z[32]=n<T>(1,2)*z[3] - z[34];
    z[32]=z[3]*z[32];
    z[33]= - z[3] + z[34];
    z[33]=z[9]*z[33];
    z[29]=n<T>(1,2)*z[33] + z[32] + z[29];
    z[29]=z[9]*z[29];
    z[32]=z[4] + z[2];
    z[32]=z[16]*z[32];
    z[33]=z[4] + z[10];
    z[33]=z[17]*z[33];
    z[32]=z[32] + z[33];
    z[33]=npow(z[2],2);
    z[34]=npow(z[10],2);
    z[33]= - z[33] + z[34];
    z[33]=z[15]*z[33];
    z[34]= - z[26] + z[23] + z[21] + z[20];
    z[35]=z[24] + z[18];
    z[36]=z[14]*i;

    r += z[19] + z[25] + z[27] + n<T>(2,3)*z[28] + z[29] + z[30] + n<T>(1,3)*
      z[31] + n<T>(8,3)*z[32] + z[33] + n<T>(1,2)*z[34] + n<T>(7,6)*z[35] + n<T>(1,9)*
      z[36];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1054(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1054(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
