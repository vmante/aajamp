#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf215(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[46];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[2];
    z[7]=d[5];
    z[8]=d[4];
    z[9]=e[2];
    z[10]=c[3];
    z[11]=d[0];
    z[12]=d[7];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[31];
    z[16]=e[1];
    z[17]=e[3];
    z[18]=e[10];
    z[19]=e[14];
    z[20]=d[9];
    z[21]=f[4];
    z[22]=f[12];
    z[23]=f[16];
    z[24]=f[51];
    z[25]=f[55];
    z[26]=f[58];
    z[27]=z[1]*i;
    z[28]= - z[4] + z[3];
    z[28]=z[28]*z[27];
    z[29]=npow(z[4],2);
    z[30]=n<T>(1,2)*z[29];
    z[31]=n<T>(1,2)*z[3];
    z[32]=z[4] - z[31];
    z[32]=z[3]*z[32];
    z[28]=z[28] - z[30] + z[32];
    z[32]=z[27] - z[3];
    z[32]= - n<T>(3,4)*z[4] - z[2] - n<T>(7,4)*z[32];
    z[32]=z[8]*z[32];
    z[28]=n<T>(3,2)*z[28] + z[32];
    z[28]=z[8]*z[28];
    z[32]=z[11] - z[3];
    z[33]=z[2] - z[4];
    z[32]=z[32]*z[33];
    z[34]=n<T>(1,6)*z[10];
    z[35]=z[34] + z[32];
    z[35]=z[12]*z[35];
    z[36]=z[3] + 3*z[2];
    z[37]= - z[36]*z[27];
    z[37]=z[37] - z[10];
    z[37]=z[5]*z[37];
    z[36]=z[27] - z[36];
    z[36]=z[9]*z[36];
    z[38]=z[2] + z[8];
    z[38]=z[17]*z[38];
    z[28]=z[38] + z[28] + z[35] + z[37] + z[36];
    z[35]=z[27] + z[4];
    z[36]=n<T>(1,2)*z[7];
    z[37]= - z[36] - z[35];
    z[37]=z[7]*z[37];
    z[38]=n<T>(1,2)*z[6];
    z[39]= - z[4] + z[7];
    z[39]=z[39]*z[38];
    z[40]=n<T>(3,2)*z[29];
    z[41]=z[27]*z[4];
    z[34]=z[39] + z[34] + z[37] + z[40] + z[41];
    z[34]=z[34]*z[38];
    z[37]=z[8] - z[2];
    z[38]=2*z[4];
    z[39]=n<T>(1,2)*z[11];
    z[37]= - z[36] - z[39] + n<T>(1,8)*z[3] + z[38] + n<T>(1,4)*z[37];
    z[37]=z[10]*z[37];
    z[42]=npow(z[4],3);
    z[37]=z[42] - z[37];
    z[42]= - z[4] + n<T>(3,2)*z[2];
    z[42]=z[2]*z[42];
    z[43]=n<T>(1,4)*z[4];
    z[44]= - z[43] + z[2];
    z[44]=z[3]*z[44];
    z[42]=z[44] - n<T>(15,4)*z[29] + z[42];
    z[42]=z[42]*z[31];
    z[44]=n<T>(1,2)*z[4] - z[2];
    z[31]=z[44]*z[31];
    z[44]=n<T>(1,2)*z[2];
    z[45]=z[4] + z[44];
    z[45]=z[2]*z[45];
    z[29]=z[31] + n<T>(7,8)*z[29] + z[45];
    z[27]=z[29]*z[27];
    z[29]= - z[4] + z[44];
    z[29]=z[2]*z[29];
    z[29]=z[30] + z[29] - z[32];
    z[29]=z[29]*z[39];
    z[31]=z[35]*z[36];
    z[30]=z[31] + z[30] + z[41];
    z[30]=z[30]*z[36];
    z[31]= - z[20] - z[4];
    z[32]=n<T>(1,2)*z[19];
    z[31]=z[32]*z[31];
    z[32]=z[43] - n<T>(1,3)*z[2];
    z[32]=z[2]*z[32];
    z[32]=z[40] + 5*z[32];
    z[32]=z[2]*z[32];
    z[35]= - z[24] + z[26] - z[25];
    z[33]= - z[16]*z[33];
    z[36]= - z[38] - n<T>(5,2)*z[2];
    z[36]=z[18]*z[36];
    z[38]=z[13]*i;

    r += z[14] - n<T>(17,4)*z[15] - n<T>(15,8)*z[21] - n<T>(7,8)*z[22] - n<T>(3,8)*
      z[23] + z[27] + n<T>(1,2)*z[28] + z[29] + z[30] + z[31] + z[32] + 
      z[33] + z[34] + n<T>(1,4)*z[35] + z[36] - n<T>(1,3)*z[37] - n<T>(5,24)*z[38]
       + z[42];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf215(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf215(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
