#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1108(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[69];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[7];
    z[7]=d[1];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[19];
    z[23]=c[23];
    z[24]=c[25];
    z[25]=c[26];
    z[26]=c[28];
    z[27]=c[30];
    z[28]=c[31];
    z[29]=e[10];
    z[30]=e[6];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[11];
    z[35]=f[12];
    z[36]=f[13];
    z[37]=f[16];
    z[38]=f[17];
    z[39]=f[18];
    z[40]=f[30];
    z[41]=f[31];
    z[42]=f[33];
    z[43]=f[36];
    z[44]=f[37];
    z[45]=f[39];
    z[46]=f[43];
    z[47]=f[46];
    z[48]=n<T>(1,2)*z[6];
    z[49]=z[1]*i;
    z[50]=z[48] - z[49];
    z[51]=z[50]*z[6];
    z[52]=z[2] - z[4];
    z[53]=3*z[49];
    z[54]=z[53] - z[52];
    z[54]=z[2]*z[54];
    z[55]=z[49] - z[6];
    z[56]= - n<T>(37,3)*z[5] + z[55] + 3*z[52];
    z[57]=n<T>(1,2)*z[5];
    z[56]=z[56]*z[57];
    z[53]= - z[4]*z[53];
    z[53]=z[56] + z[54] + z[53] + 7*z[51];
    z[53]=z[53]*z[57];
    z[54]=n<T>(1,2)*z[4];
    z[56]=z[54] - z[49];
    z[57]=z[4]*z[56];
    z[54]= - n<T>(1,4)*z[10] + z[5] + n<T>(7,6)*z[6] - n<T>(2,3)*z[49] - z[54];
    z[54]=z[10]*z[54];
    z[58]=2*z[55] + z[5];
    z[58]=z[5]*z[58];
    z[54]=z[54] + z[58] + z[57] - n<T>(1,6)*z[51];
    z[54]=z[10]*z[54];
    z[57]=n<T>(1,3)*z[4];
    z[58]=4*z[49] - z[4];
    z[58]=z[58]*z[57];
    z[59]=n<T>(1,2)*z[2];
    z[60]= - z[4] + z[6];
    z[60]=z[60]*z[59];
    z[61]= - z[4]*z[48];
    z[62]=5*z[7] - z[49] - z[4];
    z[62]=z[7]*z[62];
    z[58]=n<T>(1,6)*z[62] + z[60] + z[58] + z[61];
    z[58]=z[7]*z[58];
    z[60]=5*z[6];
    z[61]=n<T>(13,2)*z[49] - z[60];
    z[61]=z[6]*z[61];
    z[62]=n<T>(1,2)*z[10];
    z[63]= - z[62] - z[55];
    z[63]=z[10]*z[63];
    z[64]=3*z[5];
    z[65]=7*z[49];
    z[66]= - z[65] - 13*z[6];
    z[66]= - n<T>(7,6)*z[11] + z[64] + n<T>(1,6)*z[66] + z[2];
    z[66]=z[11]*z[66];
    z[67]=npow(z[5],2);
    z[61]=n<T>(1,2)*z[66] + n<T>(13,6)*z[63] + n<T>(1,3)*z[61] + n<T>(3,2)*z[67];
    z[61]=z[11]*z[61];
    z[56]= - z[56]*z[57];
    z[63]=z[49] - z[4];
    z[62]=z[62] + z[63];
    z[62]=z[10]*z[62];
    z[66]= - 2*z[63] - z[7];
    z[66]=z[7]*z[66];
    z[67]= - n<T>(5,3)*z[63] + z[10];
    z[67]=n<T>(1,2)*z[67] - n<T>(4,3)*z[7];
    z[67]=z[8]*z[67];
    z[56]=z[67] + n<T>(2,3)*z[66] + z[56] + z[62];
    z[56]=z[8]*z[56];
    z[62]=n<T>(7,4)*z[49] + 2*z[4];
    z[57]=z[62]*z[57];
    z[62]= - n<T>(13,12)*z[4] + z[50];
    z[48]=z[62]*z[48];
    z[48]=z[57] + z[48];
    z[48]=z[6]*z[48];
    z[57]=n<T>(1,3)*z[2];
    z[62]= - n<T>(61,2)*z[49] - 14*z[4];
    z[62]=n<T>(1,3)*z[62] + n<T>(11,2)*z[2];
    z[62]=z[62]*z[57];
    z[65]=z[65] + z[4];
    z[65]=z[4]*z[65];
    z[51]=z[62] + n<T>(2,9)*z[65] - n<T>(1,4)*z[51];
    z[51]=z[2]*z[51];
    z[62]= - z[4] + z[50];
    z[60]=z[62]*z[60];
    z[62]= - 59*z[49] - 23*z[4];
    z[62]=z[4]*z[62];
    z[60]=n<T>(1,6)*z[62] + z[60];
    z[62]= - 5*z[2] + n<T>(89,4)*z[49] + 10*z[4];
    z[57]=z[62]*z[57];
    z[57]=n<T>(1,2)*z[60] + z[57];
    z[60]= - z[5]*z[52];
    z[62]= - z[49] + n<T>(1,2)*z[3];
    z[65]= - n<T>(89,72)*z[2] + n<T>(5,12)*z[6] + n<T>(95,72)*z[4] - z[62];
    z[65]=z[3]*z[65];
    z[57]=z[65] + n<T>(1,3)*z[57] + z[60];
    z[57]=z[3]*z[57];
    z[52]= - z[64] + z[52] + z[55];
    z[52]=n<T>(1,2)*z[52] - z[11];
    z[52]=z[16]*z[52];
    z[60]=z[4] + z[7];
    z[60]=z[49]*z[60];
    z[60]=z[19] + z[60];
    z[60]=z[9]*z[60];
    z[63]=z[7] - z[63];
    z[63]=z[15]*z[63];
    z[60]=z[39] + z[43] + z[60] + z[63];
    z[63]=z[6] + z[11];
    z[63]=z[49]*z[63];
    z[63]=z[19] + z[63];
    z[63]=z[12]*z[63];
    z[55]=z[11] - z[55];
    z[55]=z[18]*z[55];
    z[55]=z[63] + z[55];
    z[62]=z[59] + z[62];
    z[62]=z[13]*z[62];
    z[50]=z[59] + z[50];
    z[50]=z[17]*z[50];
    z[59]=z[37] - z[46] + z[44] + z[35] - z[31];
    z[63]=z[41] - z[21];
    z[64]= - z[10] - z[11];
    z[64]=z[30]*z[64];
    z[64]=z[64] + z[40];
    z[65]=n<T>(25,9)*z[25] + n<T>(26,9)*z[23] + n<T>(1,6)*z[20];
    z[65]=i*z[65];
    z[66]=npow(z[4],3);
    z[67]=n<T>(67,3)*z[4] + n<T>(13,4)*z[6];
    z[67]=n<T>(1,2)*z[67] - n<T>(155,3)*z[2];
    z[67]=n<T>(337,72)*z[3] - n<T>(61,6)*z[10] + n<T>(1,3)*z[67] - n<T>(43,2)*z[5];
    z[67]=n<T>(1,3)*z[67] + z[7];
    z[67]= - n<T>(1,9)*z[8] + n<T>(1,4)*z[67] + n<T>(1,3)*z[11];
    z[67]=z[19]*z[67];
    z[49]= - n<T>(17,4)*z[2] + 22*z[49] - n<T>(71,4)*z[4];
    z[49]=z[14]*z[49];
    z[68]=z[7] + z[8];
    z[68]=z[29]*z[68];

    r += n<T>(169,216)*z[22] - n<T>(49,36)*z[24] - n<T>(25,18)*z[26] - n<T>(5,2)*z[27]
       - n<T>(215,36)*z[28] + n<T>(59,72)*z[32] + n<T>(5,6)*z[33] - n<T>(13,24)*z[34]
       + n<T>(115,36)*z[36] + n<T>(17,12)*z[38] + 6*z[42] + z[45] + z[47] + 
      z[48] + n<T>(1,9)*z[49] + n<T>(7,4)*z[50] + z[51] + 3*z[52] + z[53] + 
      z[54] + n<T>(7,6)*z[55] + z[56] + z[57] + z[58] + n<T>(1,2)*z[59] + n<T>(1,3)
      *z[60] + z[61] + n<T>(125,36)*z[62] + n<T>(3,2)*z[63] + n<T>(13,6)*z[64] + 
      z[65] - n<T>(1,12)*z[66] + z[67] + n<T>(4,3)*z[68];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1108(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1108(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
