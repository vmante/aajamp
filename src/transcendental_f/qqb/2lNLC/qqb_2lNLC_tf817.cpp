#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf817(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[69];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[12];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[5];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=e[10];
    z[21]=c[3];
    z[22]=d[9];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[3];
    z[32]=e[6];
    z[33]=e[13];
    z[34]=e[7];
    z[35]=e[14];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[31];
    z[44]=f[32];
    z[45]=f[36];
    z[46]=f[39];
    z[47]=f[51];
    z[48]=f[55];
    z[49]=z[1]*i;
    z[50]= - z[49] + n<T>(1,2)*z[5];
    z[50]=z[50]*z[5];
    z[51]=npow(z[6],2);
    z[51]=n<T>(11653,4593)*z[50] - z[51];
    z[52]=z[49] - z[6];
    z[53]= - z[2]*z[52];
    z[54]=z[49] - z[5];
    z[55]=20777*z[54] + n<T>(16153,2)*z[9];
    z[55]=z[9]*z[55];
    z[56]= - 25339*z[54] - 6905*z[3];
    z[56]=z[3]*z[56];
    z[57]= - 57397*z[49] + 48211*z[5];
    z[57]=n<T>(20777,3062)*z[9] + n<T>(1,9186)*z[57] - z[2];
    z[57]= - n<T>(1,3)*z[7] + n<T>(1,2)*z[57] - n<T>(29932,4593)*z[3];
    z[57]=z[7]*z[57];
    z[51]=z[57] + n<T>(1,4593)*z[56] + n<T>(1,3062)*z[55] + n<T>(1,2)*z[51] + z[53];
    z[51]=z[7]*z[51];
    z[53]=n<T>(10779,3062)*z[6];
    z[55]=n<T>(10779,3062)*z[9];
    z[56]=z[55] - z[53] + n<T>(13841,3062)*z[49] + z[5];
    z[56]=n<T>(1,2)*z[56] + n<T>(1,3)*z[10];
    z[56]=z[10]*z[56];
    z[57]= - z[49] + n<T>(1,2)*z[6];
    z[53]=z[57]*z[53];
    z[58]=z[52] + n<T>(1,2)*z[9];
    z[55]=z[58]*z[55];
    z[59]=z[49]*z[5];
    z[60]=n<T>(1,2)*z[2];
    z[61]= - z[5] + z[60];
    z[61]=z[2]*z[61];
    z[53]=z[56] + z[55] + z[61] + z[59] + z[53];
    z[53]=z[10]*z[53];
    z[55]=2*z[49];
    z[56]=z[55] - z[5];
    z[61]=n<T>(9248,3)*z[5];
    z[62]= - z[56]*z[61];
    z[61]=n<T>(8467,6)*z[6] - 8467*z[49] + z[61];
    z[61]=z[6]*z[61];
    z[61]=z[62] + z[61];
    z[61]=z[6]*z[61];
    z[57]=z[6]*z[57];
    z[50]=20777*z[50] - n<T>(27713,3)*z[57];
    z[57]= - n<T>(30025,6)*z[9] + n<T>(96949,12)*z[6] - n<T>(30634,3)*z[49] + n<T>(8529,4)*z[5];
    z[57]=z[9]*z[57];
    z[50]=n<T>(1,2)*z[50] + z[57];
    z[50]=z[9]*z[50];
    z[57]=n<T>(29932,3)*z[7] - 2312*z[52] + n<T>(11467,3)*z[3];
    z[57]=z[20]*z[57];
    z[62]=6936*z[9] + 5405*z[3];
    z[62]=z[31]*z[62];
    z[50]=z[61] + z[50] + z[57] + z[62];
    z[57]=37711*z[49] - n<T>(3217,2)*z[5];
    z[57]=z[5]*z[57];
    z[61]=n<T>(11405,2)*z[6] + 18589*z[49] - 22996*z[5];
    z[61]=z[6]*z[61];
    z[62]=6248*z[2] - 3499*z[6] - 12496*z[49] - 4249*z[5];
    z[62]=z[2]*z[62];
    z[57]=z[62] + z[57] + z[61];
    z[57]=z[2]*z[57];
    z[61]=13810*z[3];
    z[62]=23089*z[5] + z[61];
    z[62]=z[49]*z[62];
    z[62]=23089*z[21] + z[62];
    z[62]=z[8]*z[62];
    z[61]= - 23089*z[54] + z[61];
    z[61]=z[16]*z[61];
    z[63]= - 30025*z[9] - 34618*z[12];
    z[63]=z[32]*z[63];
    z[57]=z[57] + z[62] + z[61] + z[63];
    z[61]=z[55] - z[6];
    z[62]=1156*z[6];
    z[61]=z[61]*z[62];
    z[63]=25339*z[49] - 36868*z[5];
    z[63]=z[5]*z[63];
    z[61]=n<T>(1,3)*z[63] + z[61];
    z[63]= - 6905*z[49] + 8436*z[5];
    z[60]=n<T>(1,1531)*z[63] - z[60];
    z[60]=z[2]*z[60];
    z[63]= - n<T>(31,2)*z[49] - 6905*z[5];
    z[62]= - n<T>(6905,2)*z[2] + n<T>(1,3)*z[63] + z[62];
    z[62]=n<T>(25745,4593)*z[3] + n<T>(1,1531)*z[62] + z[9];
    z[62]=z[3]*z[62];
    z[63]=npow(z[9],2);
    z[60]=z[62] - n<T>(15403,3062)*z[63] + n<T>(1,1531)*z[61] + z[60];
    z[60]=z[3]*z[60];
    z[61]=3*z[6];
    z[62]=z[49] + z[5];
    z[62]=2*z[62] - z[6];
    z[62]=z[62]*z[61];
    z[56]=z[5]*z[56];
    z[63]= - z[55] - z[5];
    z[63]=2*z[63] + z[2];
    z[63]=z[2]*z[63];
    z[61]=4*z[2] - z[5] - z[61];
    z[61]=z[4]*z[61];
    z[56]=z[61] + 2*z[63] + z[56] + z[62];
    z[61]=npow(z[7],2);
    z[62]=npow(z[10],2);
    z[56]= - z[62] + z[61] + n<T>(250,1531)*z[56];
    z[56]=z[4]*z[56];
    z[61]=10373*z[12];
    z[62]=5780*z[6] + z[61];
    z[62]=z[49]*z[62];
    z[62]=5780*z[21] + z[62];
    z[62]=z[13]*z[62];
    z[52]= - 5780*z[52] + z[61];
    z[52]=z[19]*z[52];
    z[52]=z[62] + z[52];
    z[61]=6005*z[49] - n<T>(8317,2)*z[6];
    z[61]=z[6]*z[61];
    z[58]=z[9]*z[58];
    z[58]=z[61] - 6005*z[58];
    z[61]=z[10] - n<T>(16153,4593)*z[49] - z[5];
    z[61]=n<T>(8842,4593)*z[12] - n<T>(10373,4593)*z[6] + n<T>(1,2)*z[61];
    z[61]=z[12]*z[61];
    z[58]= - z[34] + n<T>(5,4593)*z[58] + z[61];
    z[58]=z[12]*z[58];
    z[61]=z[38] - z[27];
    z[62]=z[46] - z[43];
    z[63]= - 109327*z[5] - 258727*z[6];
    z[63]= - n<T>(106259,2)*z[9] + n<T>(1,4)*z[63] + 63614*z[2];
    z[63]=n<T>(16903,2)*z[12] + 250*z[4] - 1562*z[10] - n<T>(32275,3)*z[7] + n<T>(1,3)*z[63]
     - 6530*z[3];
    z[63]= - n<T>(2312,4593)*z[22] + n<T>(1,4593)*z[63];
    z[63]=z[21]*z[63];
    z[64]=n<T>(24992,4593)*z[28] + n<T>(12496,4593)*z[26] + n<T>(19621,27558)*z[23];
    z[64]=i*z[64];
    z[49]= - n<T>(7,2)*z[49] + z[5];
    z[49]=z[49]*npow(z[5],2);
    z[55]=z[55] - z[2];
    z[65]= - z[4] + z[55];
    z[65]=z[14]*z[65];
    z[66]=z[5] - z[55];
    z[66]=z[15]*z[66];
    z[55]=z[6] - z[55];
    z[55]=z[18]*z[55];
    z[59]= - z[59] - z[21];
    z[59]=z[11]*z[59];
    z[54]= - z[17]*z[54];
    z[67]=z[10] + z[22];
    z[67]=z[33]*z[67];
    z[68]= - z[7] - z[22];
    z[68]=z[35]*z[68];

    r +=  - n<T>(32306,4593)*z[24] + n<T>(3124,13779)*z[25] - n<T>(12496,4593)*
      z[29] + n<T>(122849,4593)*z[30] - n<T>(250,1531)*z[36] + n<T>(48211,18372)*
      z[37] + n<T>(9248,4593)*z[39] - n<T>(8529,6124)*z[40] + n<T>(16153,6124)*
      z[41] - n<T>(750,1531)*z[42] + n<T>(1,2)*z[44] - n<T>(96949,18372)*z[45] - 
      z[47] - z[48] + z[49] + n<T>(1,1531)*z[50] + z[51] + n<T>(2,4593)*z[52]
       + z[53] + z[54] + n<T>(6998,4593)*z[55] + z[56] + n<T>(1,4593)*z[57] + 
      z[58] + z[59] + z[60] + n<T>(6248,4593)*z[61] + n<T>(10779,6124)*z[62] + 
      z[63] + z[64] + n<T>(1000,1531)*z[65] + n<T>(8498,4593)*z[66] + z[67] + 
      z[68];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf817(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf817(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
