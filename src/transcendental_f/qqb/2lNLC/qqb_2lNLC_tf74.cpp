#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf74(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[41];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[1];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[8];
    z[6]=c[12];
    z[7]=c[13];
    z[8]=c[20];
    z[9]=c[23];
    z[10]=d[0];
    z[11]=f[24];
    z[12]=f[44];
    z[13]=f[69];
    z[14]=f[80];
    z[15]=f[22];
    z[16]=f[42];
    z[17]=d[2];
    z[18]=g[24];
    z[19]=g[27];
    z[20]=g[38];
    z[21]=g[40];
    z[22]=g[48];
    z[23]=g[50];
    z[24]=g[79];
    z[25]=g[82];
    z[26]=g[90];
    z[27]=g[92];
    z[28]=g[99];
    z[29]=g[101];
    z[30]=g[170];
    z[31]=g[183];
    z[32]=g[189];
    z[33]=g[270];
    z[34]=g[278];
    z[35]=g[281];
    z[36]=z[10] - z[17];
    z[36]=n<T>(1,2)*z[36];
    z[37]= - z[14] - z[13] + z[11] + z[12];
    z[36]=z[37]*z[36];
    z[37]=z[15] - z[16];
    z[38]= - z[37] + z[12] + z[13];
    z[39]=n<T>(2,3)*z[6];
    z[38]=z[39] + n<T>(1,2)*z[38];
    z[40]=z[2] - z[5];
    z[38]= - z[38]*z[40];
    z[37]=z[37] + z[11] + z[14];
    z[37]= - z[39] + n<T>(1,2)*z[37];
    z[39]=z[3] - z[4];
    z[37]=z[37]*z[39];
    z[39]=z[40] + z[39];
    z[40]= - n<T>(12,5)*z[7] + n<T>(1,20)*z[9] - z[8] + n<T>(31,540)*z[1];
    z[39]=i*z[40]*z[39];
    z[40]= - z[22] + z[23] - z[28] + z[29] + z[32] + z[35];

    r +=  - z[18] - z[19] - z[20] - z[21] - z[24] - z[25] - z[26] - 
      z[27] + z[30] + z[31] + z[33] + z[34] + z[36] + z[37] + z[38] + 
      z[39] - n<T>(1,3)*z[40];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf74(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf74(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
