#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf877(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[76];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=d[16];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=d[11];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[3];
    z[33]=e[6];
    z[34]=e[7];
    z[35]=e[13];
    z[36]=e[14];
    z[37]=e[12];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[6];
    z[44]=f[11];
    z[45]=f[12];
    z[46]=f[16];
    z[47]=f[17];
    z[48]=f[18];
    z[49]=f[31];
    z[50]=f[36];
    z[51]=f[39];
    z[52]=f[51];
    z[53]=f[55];
    z[54]=f[56];
    z[55]=f[58];
    z[56]=f[64];
    z[57]=n<T>(11,2)*z[11];
    z[58]=7*z[12];
    z[59]=5*z[4] - 7*z[10] + z[58] - z[57];
    z[59]=z[4]*z[59];
    z[60]=n<T>(293723,6124)*z[10];
    z[57]= - n<T>(956689,36744)*z[7] - n<T>(11,2)*z[4] + z[60] - n<T>(251549,4593)*
    z[3] + z[57];
    z[57]=z[7]*z[57];
    z[61]=98356*z[9] + 128071*z[3];
    z[61]= - n<T>(83,2)*z[5] + n<T>(1934,1531)*z[2] + n<T>(143399,18372)*z[7] + 
   n<T>(99329,4593)*z[4] - z[60] + n<T>(2,4593)*z[61] - z[11];
    z[61]=z[5]*z[61];
    z[62]=27744*z[11];
    z[63]=85135*z[14] + 224041*z[13];
    z[63]= - 27837*z[6] + n<T>(24539,6)*z[5] - n<T>(183949,3)*z[2] + 70240*z[4]
    + n<T>(57577,6)*z[10] - z[62] + n<T>(1,6)*z[63] + 12341*z[3];
    z[63]=z[6]*z[63];
    z[64]=7*z[19];
    z[65]=npow(z[11],2);
    z[66]= - 85135*z[21] + 445979*z[20];
    z[67]= - z[14] + n<T>(1,2)*z[13];
    z[67]=z[13]*z[67];
    z[68]=2*z[8];
    z[69]=z[15]*z[68];
    z[70]=205340*z[9] - n<T>(240739,4)*z[3];
    z[70]=z[3]*z[70];
    z[71]= - n<T>(1122895,36744)*z[10] + n<T>(27744,1531)*z[11] + z[58] - n<T>(224041,9186)*z[13];
    z[71]=z[10]*z[71];
    z[72]=n<T>(190383,3062)*z[2] - n<T>(13,2)*z[7] - n<T>(310049,4593)*z[4] - z[8]
    - n<T>(81329,3062)*z[3];
    z[72]=z[2]*z[72];
    z[57]=n<T>(1,1531)*z[63] + z[61] + z[72] + z[57] + z[59] + z[71] + n<T>(60081,6124)*z[65]
     + n<T>(1,4593)*z[70]
     + z[69]
     + n<T>(6725,9186)*z[67] - n<T>(13872,1531)*z[22]
     + n<T>(392723,4593)*z[16]
     + n<T>(86647,3062)*z[17] - n<T>(196712,4593)*z[18]
     + n<T>(1,9186)*z[66]
     + z[64];
    z[57]=z[1]*z[57];
    z[59]=n<T>(414257,72)*z[24] - 248561*z[29] - n<T>(190383,2)*z[27];
    z[57]=n<T>(1,1531)*z[59] + z[57];
    z[57]=i*z[57];
    z[59]=3*z[7];
    z[61]=z[4] - n<T>(5,4)*z[7];
    z[61]=z[61]*z[59];
    z[63]=npow(z[10],2);
    z[66]=7*z[63];
    z[67]=npow(z[8],2);
    z[67]=n<T>(1,2)*z[67];
    z[69]=npow(z[3],2);
    z[70]=41899*z[23] - n<T>(189472,3)*z[16] - n<T>(229879,6)*z[20] - 22810*
    z[17];
    z[71]=3*z[11];
    z[72]= - z[71] + n<T>(157321,4593)*z[4];
    z[72]=z[4]*z[72];
    z[73]=n<T>(1,2)*z[8];
    z[74]= - n<T>(565025,18372)*z[2] - n<T>(1,2)*z[7] + n<T>(57992,4593)*z[4] - 
    z[11] + z[73] + z[3];
    z[74]=z[2]*z[74];
    z[61]=z[74] + z[61] + z[72] - z[66] - n<T>(1,2)*z[65] - n<T>(81329,6124)*
    z[69] + n<T>(1,1531)*z[70] + z[67];
    z[61]=z[2]*z[61];
    z[70]=n<T>(8343,1531)*z[2] - z[59] - n<T>(120577,4593)*z[4] + 2*z[11] + 
    z[68] + n<T>(75205,3062)*z[3];
    z[70]=z[2]*z[70];
    z[72]=n<T>(993433,24)*z[7] + n<T>(256142,3)*z[3] - n<T>(293723,4)*z[10];
    z[72]=z[7]*z[72];
    z[72]=z[72] - n<T>(3211895,72)*z[23] + n<T>(196712,3)*z[18] - n<T>(41027,2)*
    z[17];
    z[74]= - z[8] + z[11];
    z[74]=2*z[74] - n<T>(108515,9186)*z[4];
    z[74]=z[4]*z[74];
    z[60]=z[60] + z[13] - n<T>(1140509,9186)*z[3];
    z[60]=n<T>(43,6)*z[5] + n<T>(196879,6124)*z[2] - n<T>(143399,36744)*z[7] + n<T>(1,2)
   *z[60] - n<T>(47368,4593)*z[4];
    z[60]=z[5]*z[60];
    z[60]=z[60] + z[70] + z[74] + n<T>(204925,12248)*z[63] - n<T>(102670,4593)*
    z[69] - z[67] + n<T>(1,1531)*z[72];
    z[60]=z[5]*z[60];
    z[70]=z[4] + z[11];
    z[70]= - n<T>(5,6)*z[7] + n<T>(293723,12248)*z[10] - z[73] - n<T>(237770,4593)*
    z[3] + n<T>(7,4)*z[70];
    z[70]=z[7]*z[70];
    z[67]= - z[67] + n<T>(9,2)*z[36];
    z[72]= - 11*z[11] + n<T>(9,2)*z[4];
    z[72]=z[4]*z[72];
    z[63]=z[70] + n<T>(1,2)*z[72] + n<T>(250483,12248)*z[63] + n<T>(15,4)*z[65] - 
   n<T>(424459,18372)*z[69] - n<T>(1265965,110232)*z[23] + n<T>(246956,4593)*z[22]
    - z[67];
    z[63]=z[7]*z[63];
    z[70]=npow(z[13],2);
    z[72]=n<T>(17027,2)*z[21] - 21610*z[20];
    z[62]=n<T>(63515,3)*z[10] + n<T>(224041,6)*z[13] - z[62];
    z[62]=z[10]*z[62];
    z[62]=z[62] - n<T>(29275,2)*z[65] + n<T>(12341,2)*z[69] + n<T>(6725,12)*z[70]
     - 
   n<T>(159206,9)*z[23] + n<T>(5,3)*z[72] + 13872*z[22];
    z[72]=n<T>(220693,18372)*z[2] + n<T>(15,2)*z[7] - z[68] + z[71];
    z[72]=z[2]*z[72];
    z[59]= - n<T>(33725,18372)*z[5] - n<T>(461203,9186)*z[2] + z[59] + n<T>(74833,1531)*z[4]
     - z[71]
     - z[13]
     + z[3];
    z[59]=z[5]*z[59];
    z[68]=z[68] - n<T>(33589,1531)*z[4];
    z[68]=z[4]*z[68];
    z[74]=z[7] - z[3] - 2*z[4];
    z[74]=z[7]*z[74];
    z[75]= - n<T>(77895,2)*z[4] - n<T>(57577,12)*z[10] + 13872*z[11] - n<T>(149995,6)
   *z[13] - 6936*z[3];
    z[75]=n<T>(57205,18372)*z[6] + n<T>(21391,18372)*z[5] + n<T>(152102,4593)*z[2]
    + n<T>(1,1531)*z[75] - n<T>(17,4)*z[7];
    z[75]=z[6]*z[75];
    z[59]=z[75] + z[59] + z[72] + z[74] + n<T>(1,1531)*z[62] + z[68];
    z[59]=z[6]*z[59];
    z[62]= - n<T>(224041,3)*z[33] + 106197*z[32];
    z[68]= - n<T>(103259,4)*z[10] + 12341*z[11] - n<T>(224041,12)*z[13] - 49271*
    z[3];
    z[68]=z[10]*z[68];
    z[62]=n<T>(1,1531)*z[68] + n<T>(13872,1531)*z[65] - 5*z[69] - n<T>(749317,27558)
   *z[23] + n<T>(1,3062)*z[62] - z[64];
    z[62]=z[10]*z[62];
    z[68]= - n<T>(25,6)*z[4] - z[8] + n<T>(13,4)*z[11];
    z[68]=z[4]*z[68];
    z[64]=z[68] + z[66] + n<T>(1,4)*z[65] + n<T>(732605,110232)*z[23] - z[64] - 
   n<T>(203251,4593)*z[16];
    z[64]=z[4]*z[64];
    z[65]=z[73] - z[71];
    z[65]=z[11]*z[65];
    z[65]=z[65] - n<T>(321467,36744)*z[23] - 3*z[35] - z[34];
    z[65]=z[11]*z[65];
    z[66]=z[43] + z[54];
    z[58]=n<T>(196712,4593)*z[9] + n<T>(85135,9186)*z[14] + z[58];
    z[58]=z[23]*z[58];
    z[68]=n<T>(7965,6124)*z[70] + n<T>(10841,3062)*z[23] - n<T>(6725,9186)*z[21]
     + 4
   *z[34] - n<T>(178111,9186)*z[33];
    z[68]=z[13]*z[68];
    z[67]= - n<T>(29275,9186)*z[23] - 4*z[35] - 2*z[37] - z[67];
    z[67]=z[8]*z[67];
    z[69]=n<T>(385087,4)*z[69] - n<T>(57205,12)*z[23] + n<T>(304483,6)*z[22] + 
   n<T>(121507,2)*z[32] + n<T>(205340,3)*z[18];
    z[69]=z[3]*z[69];

    r +=  - n<T>(514745,9186)*z[25] + n<T>(37585,12248)*z[26] + n<T>(565025,18372)*
      z[28] + n<T>(248561,3062)*z[30] + n<T>(1907461,9186)*z[31] - 20*z[38] + 7
      *z[39] - n<T>(99329,9186)*z[40] + n<T>(993433,36744)*z[41] - n<T>(172011,6124)
      *z[42] - n<T>(33725,18372)*z[44] - n<T>(204925,12248)*z[45] + n<T>(250483,12248)*z[46]
     - n<T>(33589,1531)*z[47]
     - n<T>(14,3)*z[48]
     - n<T>(29275,3062)*
      z[49] - n<T>(63515,4593)*z[50] + n<T>(12341,1531)*z[51] + n<T>(3,4)*z[52] - n<T>(1,4)*z[53]
     + n<T>(11,4)*z[55]
     + z[56]
     + z[57]
     + z[58]
     + z[59]
     + z[60]
       + z[61] + z[62] + z[63] + z[64] + z[65] + n<T>(1,2)*z[66] + z[67] + 
      z[68] + n<T>(1,1531)*z[69];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf877(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf877(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
