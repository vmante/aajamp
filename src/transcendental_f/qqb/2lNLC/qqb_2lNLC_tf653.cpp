#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf653(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[73];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[16];
    z[12]=d[5];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[3];
    z[32]=e[6];
    z[33]=e[7];
    z[34]=e[14];
    z[35]=e[13];
    z[36]=f[0];
    z[37]=f[1];
    z[38]=f[3];
    z[39]=f[4];
    z[40]=f[5];
    z[41]=f[6];
    z[42]=f[9];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[51];
    z[52]=f[55];
    z[53]=8779*z[6];
    z[54]=11841*z[13];
    z[55]= - z[53] - z[54];
    z[56]=z[1]*i;
    z[55]=z[56]*z[55];
    z[55]= - 8779*z[22] + z[55];
    z[55]=z[14]*z[55];
    z[57]=z[56] - z[6];
    z[54]=8779*z[57] - z[54];
    z[54]=z[20]*z[54];
    z[58]= - n<T>(15434,3)*z[13] - n<T>(2531,3)*z[4] + n<T>(26213,6)*z[3] + n<T>(87763,9)
   *z[7] - n<T>(47054,3)*z[2] + n<T>(62674,3)*z[10] + 15819*z[6] + n<T>(484769,36)*
    z[5];
    z[58]=z[22]*z[58];
    z[59]= - 26213*z[10] - 24682*z[3];
    z[59]=z[31]*z[59];
    z[60]=22682*z[10] + 24213*z[13];
    z[60]=z[32]*z[60];
    z[54]=z[54] + z[58] + z[55] + z[59] + z[60];
    z[55]=n<T>(1,3)*z[7];
    z[58]=z[56] - z[5];
    z[59]=105949*z[58] + n<T>(234863,2)*z[7];
    z[59]=z[59]*z[55];
    z[60]=2*z[56];
    z[61]=z[60] - z[6];
    z[62]=z[61]*z[6];
    z[63]=npow(z[10],2);
    z[64]= - 105949*z[56] + 133693*z[5];
    z[64]=z[5]*z[64];
    z[65]=z[2]*z[58];
    z[59]=z[59] + 19965*z[65] + n<T>(53957,2)*z[63] - 4624*z[62] + n<T>(1,3)*
    z[64];
    z[64]=n<T>(36806,3)*z[5] + n<T>(32275,6)*z[56] - 3093*z[6];
    z[64]= - n<T>(40961,1531)*z[3] + n<T>(41399,4593)*z[7] + n<T>(19965,3062)*z[2]
    + n<T>(1,1531)*z[64] - z[10];
    z[64]=z[3]*z[64];
    z[59]=n<T>(1,1531)*z[59] + z[64];
    z[59]=z[3]*z[59];
    z[64]=10655*z[6];
    z[61]= - z[61]*z[64];
    z[65]= - 5593*z[56] - z[64];
    z[66]=5593*z[5];
    z[65]=2*z[65] + z[66];
    z[65]=z[5]*z[65];
    z[61]=z[61] + z[65];
    z[65]=n<T>(1,1531)*z[2];
    z[67]=8124*z[56] + 2531*z[5];
    z[67]=4*z[67] - 3531*z[2];
    z[67]=z[67]*z[65];
    z[64]= - 17779*z[2] + z[64] + z[66];
    z[64]=n<T>(1,1531)*z[64] + n<T>(5,3)*z[4];
    z[64]=z[4]*z[64];
    z[66]=z[56] - z[10];
    z[68]=z[10]*z[66];
    z[69]=npow(z[7],2);
    z[61]=z[64] - n<T>(1,2)*z[69] + z[67] + n<T>(1,1531)*z[61] + 4*z[68];
    z[61]=z[4]*z[61];
    z[64]=5258*z[3];
    z[67]= - 7253*z[5] - z[64];
    z[67]=z[56]*z[67];
    z[67]= - 7253*z[22] + z[67];
    z[67]=z[9]*z[67];
    z[64]=7253*z[58] - z[64];
    z[64]=z[17]*z[64];
    z[64]=z[67] + z[64];
    z[67]=n<T>(1,1531)*z[10];
    z[68]= - 81577*z[58] - n<T>(63081,2)*z[10];
    z[68]=z[68]*z[67];
    z[69]= - z[56] + n<T>(1,2)*z[5];
    z[69]=z[69]*z[5];
    z[70]=npow(z[6],2);
    z[68]=z[68] + 3*z[70] - n<T>(32833,4593)*z[69];
    z[55]=z[55] + n<T>(3,2)*z[2] - n<T>(81577,6124)*z[10] - z[6] + n<T>(234181,18372)
   *z[58];
    z[55]=z[7]*z[55];
    z[58]=z[2] + z[56] - 3*z[6];
    z[58]=z[2]*z[58];
    z[55]=z[34] + z[55] + n<T>(1,2)*z[68] + z[58];
    z[55]=z[7]*z[55];
    z[58]= - 22682*z[56] + n<T>(31461,2)*z[6];
    z[58]=z[6]*z[58];
    z[68]=2*z[57] + z[10];
    z[68]=z[10]*z[68];
    z[71]=z[56] + z[6];
    z[71]=11841*z[71] - n<T>(32461,3)*z[13];
    z[71]=z[13]*z[71];
    z[58]=n<T>(1,2)*z[71] + z[58] + 11341*z[68];
    z[58]=z[33] + n<T>(1,1531)*z[58];
    z[58]=z[13]*z[58];
    z[68]= - z[56] + n<T>(1,2)*z[6];
    z[53]=z[68]*z[53];
    z[71]=n<T>(49333,3)*z[10] - n<T>(38709,4)*z[5] + n<T>(104693,4)*z[56] - 16496*
    z[6];
    z[71]=z[10]*z[71];
    z[53]=z[71] + z[53] - n<T>(81577,2)*z[69];
    z[53]=z[53]*z[67];
    z[67]=6345*z[56] - n<T>(31027,2)*z[6];
    z[67]=z[6]*z[67];
    z[69]= - 11124*z[5] - 19151*z[56] + 31682*z[6];
    z[69]=z[5]*z[69];
    z[67]=z[67] + z[69];
    z[69]= - 2067*z[56] - 500*z[6];
    z[69]=n<T>(14469,2)*z[2] + 7*z[69] - n<T>(407,2)*z[5];
    z[65]=z[69]*z[65];
    z[63]=z[65] + n<T>(1,1531)*z[67] + 4*z[63];
    z[63]=z[2]*z[63];
    z[65]=z[4] + z[10];
    z[65]= - z[56]*z[65];
    z[65]= - z[22] + z[65];
    z[65]=z[11]*z[65];
    z[66]=z[4] - z[66];
    z[66]=z[18]*z[66];
    z[65]= - z[37] + z[65] + z[66];
    z[60]=z[60] - z[2];
    z[66]=z[2]*z[60];
    z[67]= - n<T>(1,3)*z[8] - z[56] + z[2];
    z[67]=z[8]*z[67];
    z[66]= - z[35] + z[67] + z[66] + n<T>(13841,4593)*z[22];
    z[66]=z[8]*z[66];
    z[67]=z[56] - n<T>(1,6)*z[6];
    z[67]=z[67]*z[70];
    z[69]=7*z[56] - n<T>(2593,1531)*z[6];
    z[69]=2*z[69] - n<T>(7,3)*z[5];
    z[69]=z[5]*z[69];
    z[62]=n<T>(5186,1531)*z[62] + z[69];
    z[62]=z[5]*z[62];
    z[68]= - z[6]*z[68];
    z[69]= - n<T>(1,2)*z[10] - z[57];
    z[69]=z[10]*z[69];
    z[68]=n<T>(1,3)*z[22] + z[68] + z[69];
    z[56]=z[56] + z[10];
    z[56]=6186*z[6] - n<T>(13903,2)*z[56];
    z[56]=n<T>(1,1531)*z[56] + n<T>(1,2)*z[4];
    z[56]=z[12]*z[56];
    z[56]=n<T>(13903,1531)*z[68] + z[56];
    z[56]=z[12]*z[56];
    z[68]=z[40] - z[27];
    z[69]=z[52] + z[51];
    z[70]=n<T>(59558,1531)*z[28] + n<T>(14469,1531)*z[26] - n<T>(109507,55116)*z[23]
   ;
    z[70]=i*z[70];
    z[57]= - n<T>(4049,3)*z[3] + 544*z[57] - n<T>(7313,3)*z[7];
    z[57]=z[21]*z[57];
    z[71]=z[4] - z[60];
    z[71]=z[15]*z[71];
    z[72]=z[5] - z[60];
    z[72]=z[16]*z[72];
    z[60]=z[6] - z[60];
    z[60]=z[19]*z[60];

    r += n<T>(136879,4593)*z[24] - n<T>(25797,6124)*z[25] - n<T>(29779,1531)*z[29]
       - n<T>(508237,4593)*z[30] + 10*z[36] + n<T>(5593,1531)*z[38] - n<T>(234181,18372)*z[39]
     - z[41]
     + z[42] - n<T>(5186,1531)*z[43]
     + n<T>(38709,6124)*
      z[44] - n<T>(63081,6124)*z[45] + n<T>(10655,1531)*z[46] + n<T>(10,3)*z[47]
     +  n<T>(6186,1531)*z[48] + n<T>(16496,1531)*z[49] - n<T>(13903,3062)*z[50] + 
      z[53] + n<T>(1,1531)*z[54] + z[55] + z[56] + n<T>(17,1531)*z[57] + z[58]
       + z[59] + n<T>(3938,1531)*z[60] + z[61] + z[62] + z[63] + n<T>(14,4593)*
      z[64] + 4*z[65] + z[66] + n<T>(20089,1531)*z[67] + n<T>(14469,3062)*z[68]
       + n<T>(1,2)*z[69] + z[70] + n<T>(25434,1531)*z[71] + n<T>(407,1531)*z[72];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf653(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf653(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
