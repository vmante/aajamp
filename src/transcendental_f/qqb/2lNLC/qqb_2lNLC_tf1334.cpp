#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1334(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[43];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[9];
    z[7]=d[12];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[17];
    z[11]=e[5];
    z[12]=e[7];
    z[13]=e[9];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[31];
    z[18]=e[6];
    z[19]=f[30];
    z[20]=f[31];
    z[21]=f[32];
    z[22]=f[33];
    z[23]=f[35];
    z[24]=f[36];
    z[25]=f[39];
    z[26]=f[43];
    z[27]=z[1]*i;
    z[28]=2*z[3];
    z[29]=z[28] + z[4];
    z[29]=z[29]*z[27];
    z[30]=z[27] - z[5];
    z[31]=z[4] - z[3];
    z[32]= - n<T>(1,2)*z[31] - z[30];
    z[32]=z[5]*z[32];
    z[33]=npow(z[3],2);
    z[34]=2*z[33];
    z[35]=2*z[4];
    z[36]=z[3] - z[35];
    z[36]=z[4]*z[36];
    z[37]= - z[9]*z[31];
    z[29]=z[32] + z[37] + z[29] - z[34] + z[36];
    z[29]=z[5]*z[29];
    z[32]=5*z[3];
    z[36]= - z[32] + z[35];
    z[36]=z[36]*z[27];
    z[37]=n<T>(3,2)*z[33];
    z[38]=npow(z[4],2);
    z[39]=n<T>(5,2)*z[3] - 3*z[4];
    z[39]=z[9]*z[39];
    z[36]=z[39] + z[36] + z[37] - z[38];
    z[36]=z[9]*z[36];
    z[38]=3*z[3];
    z[39]=z[38] - z[35];
    z[40]=n<T>(1,2)*z[9];
    z[41]=z[40] - z[27] - z[39];
    z[41]=z[9]*z[41];
    z[42]=z[8] + z[4];
    z[40]=z[40] - n<T>(1,2)*z[27] + n<T>(3,2)*z[3] - z[42];
    z[40]=z[8]*z[40];
    z[39]=z[39]*z[27];
    z[39]=z[40] + z[41] + z[37] + z[39];
    z[39]=z[8]*z[39];
    z[40]=z[9] + z[4];
    z[41]=z[27]*z[40];
    z[41]=z[14] + z[41];
    z[41]=z[10]*z[41];
    z[40]= - z[27] + z[40];
    z[40]=z[13]*z[40];
    z[40]=z[41] + z[40];
    z[30]=z[30] - z[9];
    z[41]= - n<T>(1,2)*z[2] - z[30];
    z[31]=z[31]*z[41];
    z[41]= - z[3] - z[4];
    z[41]=z[4]*z[41];
    z[31]=z[34] + z[41] + z[31];
    z[31]=z[2]*z[31];
    z[30]=z[2] - z[32] - 4*z[4] + z[30];
    z[30]=z[12]*z[30];
    z[32]=z[18]*z[42];
    z[30]=z[19] + z[30] - z[32];
    z[28]=z[28] - n<T>(1,3)*z[4];
    z[28]=z[4]*z[28];
    z[28]=z[33] + z[28];
    z[28]=z[28]*z[35];
    z[32]= - z[27]*z[37];
    z[33]=z[38] + 2*z[5];
    z[34]= - z[27]*z[33];
    z[34]= - 2*z[14] + z[34];
    z[34]=z[7]*z[34];
    z[33]= - 2*z[27] + z[33];
    z[33]=z[11]*z[33];
    z[35]=z[5] - z[7];
    z[27]=z[27]*z[35];
    z[27]=z[11] + z[27];
    z[27]=z[6]*z[27];
    z[35]=z[25] - z[17];
    z[37]=npow(z[3],3);
    z[38]= - z[5] - 7*z[3] + n<T>(5,3)*z[4];
    z[38]= - n<T>(2,3)*z[8] + n<T>(1,3)*z[9] + n<T>(1,2)*z[38];
    z[38]=z[14]*z[38];
    z[41]=z[15]*i;

    r +=  - z[16] + n<T>(7,2)*z[20] + 3*z[21] + 8*z[22] + z[23] - n<T>(1,2)*
      z[24] - z[26] + z[27] + z[28] + z[29] + 2*z[30] + z[31] + z[32]
       + z[33] + z[34] + n<T>(3,2)*z[35] + z[36] - n<T>(13,3)*z[37] + z[38] + 
      z[39] + 4*z[40] + n<T>(1,3)*z[41];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1334(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1334(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
