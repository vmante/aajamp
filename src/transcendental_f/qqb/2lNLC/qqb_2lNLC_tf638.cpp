#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf638(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[77];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[5];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=d[9];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[51];
    z[52]=f[55];
    z[53]=f[58];
    z[54]=n<T>(167125,12248)*z[6];
    z[55]=n<T>(1,2)*z[2];
    z[56]=z[1]*i;
    z[57]= - 4324*z[56] + n<T>(20813,8)*z[7];
    z[57]= - n<T>(7,3)*z[10] - n<T>(17,8)*z[8] - z[54] + n<T>(67,8)*z[4] + n<T>(7,1531)*
    z[57] - z[55];
    z[57]=z[10]*z[57];
    z[58]=n<T>(1,2)*z[7];
    z[59]=z[58] - z[56];
    z[60]=z[59]*z[7];
    z[61]=3*z[5] - z[55];
    z[61]=z[61]*z[55];
    z[62]=z[56] - z[7];
    z[54]= - z[54] + z[2] - n<T>(167125,6124)*z[62] - z[5];
    z[54]=z[6]*z[54];
    z[63]= - z[5]*z[56];
    z[64]=n<T>(17,2)*z[56];
    z[65]=z[64] - z[5];
    z[65]= - n<T>(11,8)*z[4] + n<T>(1,2)*z[65] - z[2];
    z[65]=z[4]*z[65];
    z[66]=z[56] - z[4];
    z[67]= - 17*z[66] - n<T>(29,2)*z[8];
    z[67]=z[8]*z[67];
    z[54]=z[57] + n<T>(1,4)*z[67] + z[54] + z[65] + z[61] - n<T>(167125,6124)*
    z[60] + z[63];
    z[54]=z[10]*z[54];
    z[57]= - 211999*z[56] + n<T>(1139501,8)*z[7];
    z[57]=z[7]*z[57];
    z[61]=n<T>(1,2)*z[6] + z[62];
    z[61]=z[6]*z[61];
    z[63]=z[56] + z[7];
    z[63]=493597*z[63] - 426233*z[13];
    z[63]=z[13]*z[63];
    z[57]=n<T>(1,8)*z[63] - n<T>(213055,4)*z[23] + z[57] + 211999*z[61];
    z[57]=z[13]*z[57];
    z[61]=106466*z[3];
    z[63]= - n<T>(369463,2)*z[5] - z[61];
    z[63]=z[56]*z[63];
    z[63]= - n<T>(369463,2)*z[23] + z[63];
    z[63]=z[9]*z[63];
    z[65]=z[56] - z[5];
    z[61]=n<T>(369463,2)*z[65] - z[61];
    z[61]=z[17]*z[61];
    z[67]=3456901*z[7] + n<T>(4074415,2)*z[5];
    z[67]=n<T>(1,12)*z[67] - 253483*z[2];
    z[67]=n<T>(309631,8)*z[3] + n<T>(164063,4)*z[10] + n<T>(1074215,12)*z[8] + 
   n<T>(2915351,24)*z[6] + n<T>(1,2)*z[67] - 16448*z[4];
    z[67]=z[23]*z[67];
    z[68]=202813*z[6] + n<T>(456149,2)*z[13];
    z[68]=z[34]*z[68];
    z[57]=z[68] + z[67] + z[57] + z[63] + z[61];
    z[61]=n<T>(1,2)*z[5];
    z[63]=576517*z[56] - n<T>(588765,2)*z[5];
    z[63]=z[63]*z[61];
    z[60]=n<T>(346621,3)*z[60] + z[63];
    z[63]=5*z[4];
    z[67]=n<T>(3631927,2)*z[56] - 1222175*z[7];
    z[67]=n<T>(1,3)*z[67] - n<T>(371363,2)*z[5];
    z[67]=n<T>(230371,9186)*z[6] - z[63] + n<T>(1,12248)*z[67] + 4*z[2];
    z[67]=z[6]*z[67];
    z[68]=z[2]*z[65];
    z[69]=3*z[56];
    z[70]=z[69] + 2*z[5];
    z[70]=z[4]*z[70];
    z[60]=z[67] + z[70] + n<T>(1,6124)*z[60] + z[68];
    z[60]=z[6]*z[60];
    z[61]=z[61] - z[56];
    z[67]=z[5]*z[61];
    z[68]=npow(z[7],2);
    z[67]=51*z[68] - n<T>(252437,4593)*z[67];
    z[70]=3*z[2];
    z[71]=13*z[56] - 15*z[7];
    z[71]=z[71]*z[70];
    z[67]=n<T>(1,2)*z[67] + z[71];
    z[71]=3*z[7];
    z[64]= - n<T>(17,4)*z[4] + z[70] + z[64] - z[71];
    z[64]=z[4]*z[64];
    z[72]=17*z[8] - n<T>(588765,6124)*z[6] - n<T>(73,2)*z[4] + n<T>(45,2)*z[2] - 
   n<T>(1215491,18372)*z[5] + n<T>(1417583,18372)*z[56] + z[71];
    z[73]=n<T>(1,2)*z[8];
    z[72]=z[72]*z[73];
    z[65]= - 117753*z[65] - n<T>(88921,2)*z[6];
    z[65]=z[6]*z[65];
    z[64]=z[72] + n<T>(5,6124)*z[65] + n<T>(1,2)*z[67] + z[64];
    z[64]=z[64]*z[73];
    z[65]=13427*z[7];
    z[67]= - 31447*z[56] + z[65];
    z[67]=z[7]*z[67];
    z[72]= - 756929*z[56] + 1000727*z[5];
    z[72]=z[5]*z[72];
    z[67]=z[67] + n<T>(1,6)*z[72];
    z[72]=n<T>(52095,3062)*z[56] + z[7];
    z[72]=z[55] + 3*z[72] - n<T>(168533,3062)*z[5];
    z[72]=z[2]*z[72];
    z[73]=npow(z[6],2);
    z[67]=n<T>(115775,1531)*z[73] + n<T>(1,1531)*z[67] + z[72];
    z[70]= - z[70] - n<T>(756929,9186)*z[5] + n<T>(812045,9186)*z[56] - z[71];
    z[70]=n<T>(1,2)*z[70] + n<T>(215642,4593)*z[8];
    z[70]=z[8]*z[70];
    z[65]=n<T>(90575,12)*z[56] - z[65];
    z[65]=n<T>(165471,8)*z[2] + n<T>(1,2)*z[65] + n<T>(53233,3)*z[5];
    z[65]= - n<T>(256709,6124)*z[3] + n<T>(180781,18372)*z[8] + n<T>(1,1531)*z[65]
    - n<T>(9,2)*z[6];
    z[65]=z[3]*z[65];
    z[65]=z[65] + n<T>(1,2)*z[67] + z[70];
    z[65]=z[3]*z[65];
    z[67]=493597*z[13];
    z[70]= - z[56]*z[67];
    z[72]= - z[7]*z[56];
    z[72]=z[72] - z[23];
    z[70]=291505*z[72] + z[70];
    z[70]=z[14]*z[70];
    z[67]=291505*z[62] - z[67];
    z[67]=z[20]*z[67];
    z[67]=z[70] + z[67];
    z[70]= - 19821*z[56] + n<T>(16759,2)*z[7];
    z[70]=z[70]*z[71];
    z[71]=n<T>(50441,2)*z[5] - 50441*z[56] - 59463*z[7];
    z[71]=z[5]*z[71];
    z[70]=z[70] + z[71];
    z[71]= - 3807*z[2] + n<T>(56483,2)*z[56] + 4552*z[5];
    z[71]=z[2]*z[71];
    z[70]=n<T>(1,4)*z[70] + z[71];
    z[71]= - 5*z[56] + n<T>(22883,3062)*z[7];
    z[71]= - n<T>(50359,1531)*z[2] + 3*z[71] + n<T>(41255,3062)*z[5];
    z[63]=n<T>(1,2)*z[71] + z[63];
    z[63]=z[4]*z[63];
    z[63]=n<T>(1,1531)*z[70] + n<T>(1,2)*z[63];
    z[63]=z[4]*z[63];
    z[70]= - z[4]*z[56];
    z[70]=z[70] - z[23];
    z[70]=z[11]*z[70];
    z[71]=z[22]*z[66];
    z[70]=z[70] - z[71];
    z[71]= - 195011*z[56] - n<T>(103903,2)*z[7];
    z[71]=z[7]*z[71];
    z[72]= - n<T>(100171,2)*z[2] + n<T>(67569,2)*z[5] + 100171*z[56] + 31529*
    z[7];
    z[72]=z[2]*z[72];
    z[71]=z[71] + z[72];
    z[72]= - n<T>(15105,4)*z[5] - n<T>(291423,4)*z[56] + 58964*z[7];
    z[72]=z[5]*z[72];
    z[71]=z[72] + n<T>(1,4)*z[71];
    z[71]=z[2]*z[71];
    z[62]= - n<T>(66185,3)*z[3] + 13427*z[62] - n<T>(848789,12)*z[8];
    z[62]=z[21]*z[62];
    z[62]=z[71] + z[62];
    z[58]=z[69] - z[58];
    z[58]=z[58]*z[68];
    z[68]=185579*z[56] - n<T>(176393,2)*z[7];
    z[68]=z[7]*z[68];
    z[69]= - n<T>(9,2)*z[5] + 27*z[56] - n<T>(185579,12248)*z[7];
    z[69]=z[5]*z[69];
    z[68]=n<T>(1,6124)*z[68] + z[69];
    z[68]=z[5]*z[68];
    z[69]=z[2] - z[4];
    z[71]=z[5] - z[7];
    z[69]=z[71]*z[69];
    z[69]=3*z[69] + n<T>(58301,9186)*z[23];
    z[69]=z[24]*z[69];
    z[71]=5*z[6];
    z[72]= - 3*z[4] - z[71];
    z[72]=z[56]*z[72];
    z[72]= - 3*z[23] + z[72];
    z[72]=z[12]*z[72];
    z[61]= - z[55] - z[61];
    z[61]=z[16]*z[61];
    z[66]= - 3*z[66] + z[71];
    z[66]=z[18]*z[66];
    z[55]= - z[55] - z[59];
    z[55]=z[19]*z[55];
    z[59]=z[42] - z[29];
    z[71]= - n<T>(51179,3062)*z[30] - n<T>(100171,6124)*z[28] - n<T>(322043,73488)*
    z[25];
    z[71]=i*z[71];
    z[56]=z[4] - 2*z[56] + z[2];
    z[56]=z[15]*z[56];
    z[73]= - 214709*z[6] - 193275*z[3];
    z[73]=z[33]*z[73];
    z[74]= - z[10] + n<T>(9,2)*z[13];
    z[74]=z[35]*z[74];
    z[75]= - 4*z[10] - n<T>(11,2)*z[24];
    z[75]=z[36]*z[75];
    z[76]=z[8] + z[24];
    z[76]=z[37]*z[76];

    r += n<T>(354071,6124)*z[26] - n<T>(394123,73488)*z[27] + n<T>(51179,6124)*
      z[31] - n<T>(4011827,18372)*z[32] + 8*z[38] - 4*z[39] + n<T>(50441,12248)
      *z[40] - n<T>(1215491,73488)*z[41] - n<T>(185579,12248)*z[43] + n<T>(371363,24496)*z[44]
     - n<T>(444605,24496)*z[45]
     + n<T>(59463,12248)*z[46]
     + n<T>(8,3)
      *z[47] + n<T>(145691,12248)*z[48] + n<T>(1222175,36744)*z[49] - n<T>(167125,12248)*z[50]
     + n<T>(67,8)*z[51]
     + n<T>(73,8)*z[52] - n<T>(29,8)*z[53]
     + z[54]
       + n<T>(31529,1531)*z[55] + n<T>(19862,1531)*z[56] + n<T>(1,4593)*z[57] + 
      n<T>(76673,6124)*z[58] - n<T>(100171,12248)*z[59] + z[60] + n<T>(67569,3062)*
      z[61] + n<T>(1,1531)*z[62] + z[63] + z[64] + z[65] + z[66] + n<T>(1,18372)
      *z[67] + z[68] + n<T>(1,2)*z[69] + n<T>(3,2)*z[70] + z[71] + z[72] + n<T>(1,6124)*z[73]
     + z[74]
     + z[75]
     + n<T>(51,4)*z[76];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf638(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf638(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
