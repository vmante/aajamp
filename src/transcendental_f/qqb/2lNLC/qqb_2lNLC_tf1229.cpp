#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1229(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[131];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[62];
    z[3]=f[70];
    z[4]=f[79];
    z[5]=f[81];
    z[6]=c[11];
    z[7]=d[0];
    z[8]=d[1];
    z[9]=d[2];
    z[10]=d[3];
    z[11]=d[4];
    z[12]=d[5];
    z[13]=d[6];
    z[14]=d[7];
    z[15]=d[8];
    z[16]=d[9];
    z[17]=c[12];
    z[18]=c[13];
    z[19]=c[20];
    z[20]=c[23];
    z[21]=c[32];
    z[22]=c[33];
    z[23]=c[35];
    z[24]=c[36];
    z[25]=c[37];
    z[26]=c[38];
    z[27]=c[39];
    z[28]=c[40];
    z[29]=c[43];
    z[30]=c[44];
    z[31]=c[47];
    z[32]=c[48];
    z[33]=c[49];
    z[34]=c[50];
    z[35]=c[55];
    z[36]=c[56];
    z[37]=c[57];
    z[38]=c[59];
    z[39]=c[81];
    z[40]=c[84];
    z[41]=f[22];
    z[42]=f[24];
    z[43]=f[42];
    z[44]=f[44];
    z[45]=f[61];
    z[46]=f[69];
    z[47]=f[77];
    z[48]=f[80];
    z[49]=g[24];
    z[50]=g[27];
    z[51]=g[38];
    z[52]=g[40];
    z[53]=g[48];
    z[54]=g[50];
    z[55]=g[79];
    z[56]=g[82];
    z[57]=g[90];
    z[58]=g[92];
    z[59]=g[95];
    z[60]=g[99];
    z[61]=g[101];
    z[62]=g[110];
    z[63]=g[112];
    z[64]=g[113];
    z[65]=g[128];
    z[66]=g[131];
    z[67]=g[138];
    z[68]=g[140];
    z[69]=g[145];
    z[70]=g[147];
    z[71]=g[170];
    z[72]=g[173];
    z[73]=g[181];
    z[74]=g[183];
    z[75]=g[189];
    z[76]=g[191];
    z[77]=g[198];
    z[78]=g[200];
    z[79]=g[201];
    z[80]=g[212];
    z[81]=g[214];
    z[82]=g[218];
    z[83]=g[220];
    z[84]=g[224];
    z[85]=g[228];
    z[86]=g[233];
    z[87]=g[234];
    z[88]=g[243];
    z[89]=g[245];
    z[90]=g[251];
    z[91]=g[254];
    z[92]=g[270];
    z[93]=g[272];
    z[94]=g[276];
    z[95]=g[278];
    z[96]=g[281];
    z[97]=g[288];
    z[98]=g[289];
    z[99]=g[290];
    z[100]=g[292];
    z[101]=g[294];
    z[102]=g[308];
    z[103]=g[312];
    z[104]=g[339];
    z[105]=g[342];
    z[106]=g[347];
    z[107]=g[348];
    z[108]=n<T>(17,6)*z[4];
    z[109]=z[2] - 2*z[3] + z[108] - n<T>(2,3)*z[5];
    z[109]=z[1]*z[109];
    z[110]= - n<T>(61,120)*z[38] - 4*z[40] + n<T>(65,18)*z[39];
    z[111]=n<T>(1,5)*z[20] - 4*z[19];
    z[111]=n<T>(1,3)*z[111] - n<T>(16,5)*z[18];
    z[111]=z[11]*z[111];
    z[112]=2*z[10] + n<T>(17,3)*z[14];
    z[113]=n<T>(1,3)*z[112] - n<T>(1,5)*z[11];
    z[113]=z[6]*z[113];
    z[114]= - z[19] + n<T>(1,20)*z[20];
    z[115]=n<T>(1,6)*z[114] - n<T>(2,5)*z[18];
    z[115]=17*z[115] + n<T>(89,1080)*z[6];
    z[115]=z[16]*z[115];
    z[116]= - n<T>(4,5)*z[18] + n<T>(1,3)*z[114];
    z[117]= - n<T>(103,540)*z[6] + z[116];
    z[117]=z[15]*z[117];
    z[118]= - 7*z[116] + n<T>(163,1620)*z[6];
    z[118]=z[12]*z[118];
    z[114]= - n<T>(6,5)*z[18] + n<T>(1,2)*z[114];
    z[119]= - 7*z[114] - n<T>(311,3240)*z[6];
    z[119]=z[9]*z[119];
    z[120]= - n<T>(673,3240)*z[6] - z[114];
    z[120]=z[13]*z[120];
    z[116]= - 11*z[116] - n<T>(181,1620)*z[6];
    z[116]=z[8]*z[116];
    z[114]=11*z[114] + n<T>(523,3240)*z[6];
    z[114]=z[7]*z[114];
    z[109]=z[114] + z[116] + z[120] + z[119] + z[118] + z[117] + z[115]
    + n<T>(1,9)*z[113] + z[111] - n<T>(7,54)*z[22] - 8*z[25] + n<T>(107,27)*z[26]
    + n<T>(109,486)*z[27] - n<T>(10,3)*z[30] - n<T>(877,3240)*z[31] + n<T>(54,5)*z[33]
    + 4*z[34] + n<T>(9,2)*z[36] + n<T>(1,3)*z[110] - n<T>(1,2)*z[37] + z[109];
    z[109]=i*z[109];
    z[110]=n<T>(1,2)*z[46] + 2*z[47];
    z[111]=z[110] + n<T>(7,2)*z[44];
    z[113]=z[43] + z[111] + z[41];
    z[114]=z[3] + z[45];
    z[115]= - z[4] + n<T>(1,2)*z[17];
    z[116]=n<T>(1,2)*z[48];
    z[117]=n<T>(1,3)*z[5];
    z[113]= - n<T>(29,12)*z[2] + n<T>(1,6)*z[42] - z[117] - z[116] + z[115] - n<T>(2,3)*z[114]
     + n<T>(1,3)*z[113];
    z[113]=z[7]*z[113];
    z[110]= - n<T>(1,6)*z[41] - n<T>(1,3)*z[110] + n<T>(1,2)*z[44];
    z[114]=n<T>(1,3)*z[2];
    z[118]=n<T>(1,2)*z[43];
    z[119]= - z[114] + n<T>(49,9)*z[17] + n<T>(13,6)*z[4] - z[117] + z[118] - n<T>(1,3)*z[48]
     + z[110];
    z[119]=z[15]*z[119];
    z[120]=z[48] + n<T>(13,2)*z[44];
    z[118]= - z[118] + n<T>(1,2)*z[120];
    z[120]=2*z[45];
    z[121]=n<T>(1,2)*z[41];
    z[122]= - z[2] + n<T>(31,2)*z[17] + z[3] + n<T>(19,2)*z[4] - n<T>(1,2)*z[42]
     - 
    z[5] - z[121] + z[120] - z[118];
    z[122]=z[13]*z[122];
    z[123]=z[43] - z[48];
    z[124]=4*z[45];
    z[125]= - n<T>(5,4)*z[2] + n<T>(5,6)*z[17] + z[42] - z[41] + z[124] - z[123]
   ;
    z[125]=z[16]*z[125];
    z[122]=z[96] + z[52] - z[53] - z[60] - z[83] - z[125] - z[122] + 
    z[49] + z[50];
    z[118]= - z[124] + z[118];
    z[124]=n<T>(4,3)*z[3];
    z[118]=n<T>(3,4)*z[2] - n<T>(43,9)*z[17] - z[124] - n<T>(19,6)*z[4] + n<T>(5,6)*
    z[42] + n<T>(1,3)*z[118] + z[121];
    z[118]=z[12]*z[118];
    z[111]= - n<T>(7,2)*z[42] + z[5] + z[116] - z[111];
    z[111]=n<T>(7,4)*z[2] + z[124] + n<T>(1,3)*z[111] - z[115];
    z[111]=z[9]*z[111];
    z[115]=z[3] + z[42];
    z[116]=z[2] + z[4];
    z[110]= - n<T>(2,9)*z[17] - n<T>(1,6)*z[43] - n<T>(4,3)*z[45] - z[110] + n<T>(2,3)*
    z[116] - n<T>(1,3)*z[115];
    z[110]=z[8]*z[110];
    z[108]= - z[108] + z[117];
    z[108]=z[14]*z[108];
    z[115]=z[5] + z[120] - z[123];
    z[115]=n<T>(1,3)*z[115] + z[42];
    z[115]=z[10]*z[115];
    z[116]=n<T>(2,3)*z[11];
    z[117]=z[45] - z[42];
    z[117]=z[117]*z[116];
    z[120]=n<T>(1,3)*z[14];
    z[116]= - z[116] + z[120] + z[10];
    z[116]=z[3]*z[116];
    z[121]= - z[14] + 2*z[11];
    z[114]=z[121]*z[114];
    z[121]= - z[98] + z[63] - n<T>(1,24)*z[104] - n<T>(3,8)*z[105] - z[107] + n<T>(1,2)*z[106]
     + z[78] - z[87];
    z[123]= - z[67] + z[71] + z[74] - z[92] - z[95] + z[55] + z[58];
    z[124]=z[62] + z[77] + z[86] - z[97];
    z[125]=z[102] - z[103];
    z[126]=z[100] + z[101];
    z[127]=z[75] - z[84];
    z[128]=z[65] + z[68];
    z[129]=z[57] + z[90];
    z[130]=z[51] + z[56];
    z[120]= - z[41]*z[120];
    z[112]=n<T>(16,9)*z[11] - z[112];
    z[112]=z[17]*z[112];

    r +=  - n<T>(3,10)*z[21] + n<T>(24,5)*z[23] + 4*z[24] - n<T>(10,3)*z[28] - 
      z[29] + n<T>(5,6)*z[32] - n<T>(1,10)*z[35] - n<T>(5,9)*z[54] + n<T>(1,48)*z[59]
       - n<T>(25,36)*z[61] + z[64] + n<T>(23,6)*z[66] + n<T>(4,9)*z[69] + n<T>(5,36)*
      z[70] + n<T>(23,12)*z[72] - n<T>(1,12)*z[73] - n<T>(1,18)*z[76] + z[79] + n<T>(7,12)*z[80]
     - n<T>(3,2)*z[81]
     + n<T>(11,4)*z[82] - n<T>(3,16)*z[85]
     + n<T>(17,3)*
      z[88] - n<T>(17,12)*z[89] - n<T>(17,18)*z[91] - n<T>(43,12)*z[93] + n<T>(67,12)*
      z[94] - z[99] + z[108] + z[109] + z[110] + z[111] + z[112] + 
      z[113] + z[114] + z[115] + z[116] + z[117] + z[118] + z[119] + 
      z[120] + n<T>(1,2)*z[121] - n<T>(1,3)*z[122] + n<T>(5,12)*z[123] + n<T>(1,4)*
      z[124] - 2*z[125] - n<T>(2,3)*z[126] + n<T>(1,9)*z[127] - n<T>(8,3)*z[128]
     - 
      n<T>(7,2)*z[129] - n<T>(7,3)*z[130];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1229(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1229(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
