#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf118(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[62];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[15];
    z[10]=d[2];
    z[11]=d[4];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[3];
    z[30]=e[6];
    z[31]=e[14];
    z[32]=e[13];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[23];
    z[39]=f[31];
    z[40]=f[36];
    z[41]=f[39];
    z[42]=f[51];
    z[43]=f[55];
    z[44]=f[58];
    z[45]=z[4] - z[6];
    z[46]=n<T>(106,45)*z[2];
    z[47]=n<T>(223,10)*z[11];
    z[48]=n<T>(191,5)*z[3] - z[47] + z[12] - z[8];
    z[45]= - n<T>(159,20)*z[7] + n<T>(1,2)*z[48] + z[46] - n<T>(53,45)*z[45];
    z[45]=z[4]*z[45];
    z[48]=n<T>(57,10)*z[3];
    z[49]=z[48] + n<T>(53,45)*z[2];
    z[50]=z[5] + z[8];
    z[51]= - z[11] + z[50] + z[49];
    z[51]=z[2]*z[51];
    z[48]= - n<T>(106,45)*z[6] + n<T>(67,10)*z[7] - z[46] - z[12] - z[48];
    z[48]=z[6]*z[48];
    z[52]=npow(z[5],2);
    z[53]=n<T>(1,2)*z[52];
    z[54]=npow(z[8],2);
    z[55]=n<T>(3,2)*z[54];
    z[56]=npow(z[12],2);
    z[57]= - 72*z[15] - n<T>(53,9)*z[14];
    z[58]=z[12] + n<T>(1757,60)*z[11];
    z[58]=z[11]*z[58];
    z[59]=3*z[8];
    z[60]= - z[59] + n<T>(103,30)*z[3];
    z[60]=z[3]*z[60];
    z[61]= - n<T>(2947,12)*z[7] - n<T>(67,2)*z[2] + n<T>(223,2)*z[11] - 22*z[3];
    z[61]=z[7]*z[61];
    z[45]=z[45] + z[48] + n<T>(1,5)*z[61] + z[51] + z[60] + z[58] - z[53] + 
    z[55] - n<T>(1,2)*z[56] + n<T>(2,5)*z[57] - n<T>(25,27)*z[20];
    z[45]=z[4]*z[45];
    z[48]=z[59] + n<T>(1,3)*z[5];
    z[51]=n<T>(47,15)*z[11] + n<T>(343,15)*z[12] - z[48];
    z[46]=z[46] + n<T>(1,2)*z[51] - n<T>(34,3)*z[3];
    z[46]=z[6]*z[46];
    z[51]= - 31*z[16] + 79*z[18];
    z[57]= - n<T>(106,3)*z[17] - z[51];
    z[57]=n<T>(1381,180)*z[20] + n<T>(1,5)*z[57] + 25*z[19];
    z[58]= - n<T>(56,3)*z[5] + n<T>(89,2)*z[11];
    z[58]=z[11]*z[58];
    z[59]=z[3]*z[59];
    z[49]=z[11] + z[49];
    z[49]=z[2]*z[49];
    z[60]=n<T>(5,2)*z[7] + n<T>(22,3)*z[3] - n<T>(67,10)*z[2];
    z[60]=z[7]*z[60];
    z[46]=z[46] + z[60] + z[49] + z[59] + n<T>(1,5)*z[58] - n<T>(9,10)*z[52] + n<T>(1,3)*z[57]
     - n<T>(171,10)*z[56];
    z[46]=z[6]*z[46];
    z[49]=n<T>(212,3)*z[17] + z[51];
    z[51]=z[12]*z[13];
    z[57]= - 106*z[9] + n<T>(727,6)*z[3];
    z[57]=z[3]*z[57];
    z[49]=z[57] + 88*z[51] + n<T>(76,3)*z[19] + n<T>(212,9)*z[14] + n<T>(1,3)*z[49]
    + 144*z[15];
    z[51]=n<T>(212,45)*z[2];
    z[57]=n<T>(79,3)*z[13];
    z[58]= - z[57] - 83*z[12];
    z[48]= - z[51] + n<T>(68,3)*z[3] - n<T>(62,15)*z[11] + n<T>(1,5)*z[58] + z[48];
    z[48]=z[6]*z[48];
    z[50]=z[51] - n<T>(527,30)*z[3] - z[50];
    z[50]=z[2]*z[50];
    z[58]=n<T>(156,5)*z[10];
    z[47]=n<T>(41,20)*z[7] + n<T>(587,30)*z[2] - n<T>(319,30)*z[3] - z[47] + n<T>(146,5)
   *z[5] + z[58] + z[8];
    z[47]=z[7]*z[47];
    z[58]= - z[58] + 17*z[5];
    z[58]=z[5]*z[58];
    z[59]= - n<T>(2291,60)*z[11] - z[12] + n<T>(56,15)*z[5];
    z[59]=z[11]*z[59];
    z[51]=n<T>(212,45)*z[6] + n<T>(159,10)*z[7] - z[51] - n<T>(47,5)*z[3] + n<T>(233,10)
   *z[11] - n<T>(144,5)*z[9] - z[5];
    z[51]=z[4]*z[51];
    z[47]=z[51] + z[48] + z[47] + z[50] + z[59] + z[58] - z[55] + n<T>(1,5)*
    z[49];
    z[47]=z[1]*z[47];
    z[48]= - 2*z[26] - z[24];
    z[48]=212*z[48] - 401*z[21];
    z[47]=n<T>(1,45)*z[48] + z[47];
    z[47]=i*z[47];
    z[48]=npow(z[3],2);
    z[49]= - z[17] - z[14];
    z[49]=n<T>(61,54)*z[20] + n<T>(106,9)*z[49] - 67*z[19];
    z[50]=n<T>(37,4)*z[3] - n<T>(106,15)*z[2];
    z[50]=z[2]*z[50];
    z[49]=n<T>(1,3)*z[50] - n<T>(97,15)*z[48] + n<T>(1,5)*z[49] - z[53];
    z[49]=z[2]*z[49];
    z[50]=npow(z[10],2);
    z[50]=78*z[50];
    z[51]=npow(z[11],2);
    z[53]= - 2*z[10] + z[5];
    z[53]=z[5]*z[53];
    z[53]= - n<T>(327,4)*z[51] + 73*z[53] + n<T>(6241,36)*z[20] - z[50] - 146*
    z[31] - n<T>(1303,6)*z[19];
    z[55]= - n<T>(37,12)*z[2] - z[8] + n<T>(77,10)*z[3];
    z[55]=z[2]*z[55];
    z[58]=n<T>(443,5)*z[10] + z[8];
    z[58]= - 22*z[7] + n<T>(4,15)*z[2] + n<T>(767,30)*z[3] - n<T>(273,20)*z[11] + n<T>(1,2)*z[58]
     + n<T>(73,5)*z[5];
    z[58]=z[7]*z[58];
    z[48]=z[58] + z[55] + n<T>(1,5)*z[53] + n<T>(433,12)*z[48];
    z[48]=z[7]*z[48];
    z[53]= - 151*z[10] + n<T>(11,3)*z[5];
    z[53]=z[5]*z[53];
    z[50]=n<T>(1,2)*z[53] - n<T>(421,9)*z[20] + n<T>(1,3)*z[16] + z[50];
    z[50]=z[5]*z[50];
    z[53]=z[42] + z[43];
    z[55]=n<T>(47,2)*z[10] - z[57] - 144*z[9];
    z[55]=z[20]*z[55];
    z[50]=z[50] + z[55] - n<T>(44,3)*z[39] - n<T>(91,2)*z[40] + n<T>(28,3)*z[41]
     + 
   73*z[44] - n<T>(141,2)*z[53];
    z[53]= - 3*z[30] + n<T>(34,3)*z[29];
    z[55]= - n<T>(326,45)*z[11] - n<T>(1,2)*z[12] + n<T>(28,15)*z[5];
    z[55]=z[11]*z[55];
    z[52]=z[55] + n<T>(28,15)*z[52] + n<T>(1,5)*z[53] - n<T>(347,36)*z[20];
    z[52]=z[11]*z[52];
    z[53]= - n<T>(8,3)*z[30] + n<T>(11,5)*z[18];
    z[53]=n<T>(713,90)*z[56] + 8*z[53] + n<T>(23,36)*z[20];
    z[53]=z[12]*z[53];
    z[51]= - n<T>(373,6)*z[51] + n<T>(176,3)*z[20] - n<T>(731,2)*z[19] + 93*z[29]
     - 
   106*z[15];
    z[55]=2*z[11] - n<T>(1177,36)*z[3];
    z[55]=z[3]*z[55];
    z[51]=n<T>(1,5)*z[51] + z[55];
    z[51]=z[3]*z[51];
    z[55]= - z[35] + z[25] - z[34];
    z[54]= - n<T>(1,2)*z[54] + n<T>(187,180)*z[20] - z[32] - n<T>(141,5)*z[31];
    z[54]=z[8]*z[54];

    r += n<T>(113,6)*z[22] - n<T>(53,135)*z[23] + n<T>(212,45)*z[27] - n<T>(11899,180)*
      z[28] - n<T>(3257,60)*z[33] - n<T>(1757,60)*z[36] - n<T>(377,20)*z[37] + n<T>(37,6)*z[38]
     + z[45]
     + z[46]
     + z[47]
     + z[48]
     + z[49]
     + n<T>(1,5)*z[50]
     +  z[51] + z[52] + z[53] + z[54] + n<T>(106,45)*z[55];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf118(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf118(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
