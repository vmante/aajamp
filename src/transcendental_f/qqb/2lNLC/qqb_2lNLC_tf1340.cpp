#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1340(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[93];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[1];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[15];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[9];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[12];
    z[26]=c[3];
    z[27]=c[11];
    z[28]=c[15];
    z[29]=c[19];
    z[30]=c[23];
    z[31]=c[25];
    z[32]=c[26];
    z[33]=c[28];
    z[34]=c[31];
    z[35]=e[10];
    z[36]=e[13];
    z[37]=e[6];
    z[38]=e[14];
    z[39]=f[0];
    z[40]=f[1];
    z[41]=f[3];
    z[42]=f[4];
    z[43]=f[5];
    z[44]=f[11];
    z[45]=f[12];
    z[46]=f[14];
    z[47]=f[16];
    z[48]=f[17];
    z[49]=f[18];
    z[50]=f[30];
    z[51]=f[31];
    z[52]=f[32];
    z[53]=f[33];
    z[54]=f[36];
    z[55]=f[37];
    z[56]=f[39];
    z[57]=f[43];
    z[58]=f[50];
    z[59]=f[51];
    z[60]=f[53];
    z[61]=f[54];
    z[62]=f[55];
    z[63]=f[58];
    z[64]=n<T>(1,2)*z[4];
    z[65]=z[1]*i;
    z[66]=z[64] - z[65];
    z[67]=z[66]*z[4];
    z[68]=n<T>(1,3)*z[4];
    z[69]=n<T>(5,24)*z[5] + n<T>(1,4)*z[65] - z[68];
    z[69]=z[5]*z[69];
    z[70]=n<T>(1,2)*z[3];
    z[71]=z[70] - z[65];
    z[72]= - z[4] + z[71];
    z[72]=z[72]*z[70];
    z[73]=z[5] - z[4];
    z[74]= - n<T>(1,12)*z[6] + n<T>(7,6)*z[9] + n<T>(1,6)*z[65] - z[73];
    z[75]=n<T>(1,3)*z[6];
    z[74]=z[74]*z[75];
    z[76]=n<T>(1,2)*z[5];
    z[77]= - n<T>(11,18)*z[9] + z[3] + z[68] - z[76];
    z[77]=n<T>(1,2)*z[77] + n<T>(5,9)*z[6];
    z[78]=n<T>(1,2)*z[7];
    z[77]=z[77]*z[78];
    z[79]= - n<T>(11,6)*z[9] + n<T>(11,3)*z[65] - z[5];
    z[79]=z[9]*z[79];
    z[69]=z[77] + z[74] + n<T>(1,12)*z[79] + z[72] + n<T>(1,6)*z[67] + z[69];
    z[69]=z[7]*z[69];
    z[72]=n<T>(1,2)*z[9];
    z[74]=z[72] - z[70];
    z[77]=n<T>(1,2)*z[65] - z[4];
    z[77]=n<T>(5,3)*z[8] + n<T>(1,3)*z[77] + z[74];
    z[77]=z[8]*z[77];
    z[79]=npow(z[5],2);
    z[77]=z[79] - z[77];
    z[80]=z[71]*z[70];
    z[81]=z[65] - z[4];
    z[82]=z[81]*z[68];
    z[83]=z[73] + z[65];
    z[84]=z[72] + z[83];
    z[84]=z[84]*z[72];
    z[85]=n<T>(1,2)*z[6];
    z[73]= - z[9] + z[3] + z[73];
    z[73]=z[73]*z[85];
    z[73]=z[73] + z[84] - z[80] + z[82] - n<T>(1,2)*z[77];
    z[73]=z[8]*z[73];
    z[77]=2*z[65];
    z[82]=z[77] - z[2];
    z[78]= - z[78] + z[85] - z[76] - z[64] - z[82];
    z[78]=z[2]*z[78];
    z[84]=z[5] + z[4];
    z[86]=z[7] + z[6];
    z[87]=z[77] - z[86] + z[84];
    z[87]=z[7]*z[87];
    z[88]=2*z[5];
    z[89]= - z[88] - z[81];
    z[89]=z[5]*z[89];
    z[90]=z[77] - z[4];
    z[90]=z[4]*z[90];
    z[78]=z[78] + z[87] + z[90] + z[89];
    z[71]= - z[3]*z[71];
    z[83]=z[6] + z[83];
    z[75]=z[83]*z[75];
    z[83]=npow(z[9],2);
    z[71]=z[75] + n<T>(1,2)*z[83] + z[71] + n<T>(1,3)*z[78];
    z[71]=z[2]*z[71];
    z[75]=z[66]*z[68];
    z[78]=n<T>(1,3)*z[3];
    z[83]=z[78] + z[5];
    z[87]=z[83]*z[78];
    z[89]= - z[68] - z[5];
    z[89]=z[5]*z[89];
    z[90]=z[65] - z[3];
    z[91]=n<T>(1,2)*z[10] + z[90];
    z[91]=z[10]*z[91];
    z[92]= - z[5] + z[14];
    z[92]=z[14]*z[92];
    z[75]=z[92] + n<T>(5,9)*z[91] + z[87] + z[75] + z[89];
    z[75]=z[14]*z[75];
    z[87]=z[4] + z[8];
    z[87]=z[65]*z[87];
    z[87]=z[26] + z[87];
    z[87]=z[11]*z[87];
    z[89]=z[4] - z[82];
    z[89]=z[18]*z[89];
    z[91]=z[8] - z[81];
    z[91]=z[19]*z[91];
    z[92]=z[7] - z[82];
    z[92]=z[23]*z[92];
    z[87]= - z[87] - z[89] - z[57] - z[49] - z[43] + z[31] - z[91] - 
    z[92] + z[61] + z[59];
    z[66]=z[66]*z[64];
    z[89]=n<T>(1,3)*z[5];
    z[91]=z[77] - z[5];
    z[91]=z[91]*z[89];
    z[92]= - n<T>(11,9)*z[65] + z[64];
    z[89]=n<T>(11,18)*z[3] + n<T>(1,2)*z[92] - z[89];
    z[89]=z[3]*z[89];
    z[66]=z[89] + z[66] + z[91];
    z[66]=z[3]*z[66];
    z[64]=n<T>(1,18)*z[9] - z[76] - n<T>(8,9)*z[65] + z[64];
    z[64]=z[9]*z[64];
    z[89]= - 5*z[5] + n<T>(7,3)*z[65] + z[4];
    z[74]=n<T>(7,6)*z[6] + n<T>(1,3)*z[89] + z[74];
    z[74]=z[74]*z[85];
    z[64]=z[74] + z[64] - n<T>(2,3)*z[79] - z[80];
    z[64]=z[6]*z[64];
    z[74]= - z[65] - z[76];
    z[74]=z[5]*z[74];
    z[76]= - n<T>(2,3)*z[65] + z[83];
    z[76]=z[3]*z[76];
    z[74]=z[76] - n<T>(7,4)*z[67] + z[74];
    z[76]= - n<T>(77,3)*z[65] + 23*z[4];
    z[76]=z[78] + n<T>(1,8)*z[76] - z[88];
    z[76]=n<T>(5,18)*z[10] - n<T>(4,3)*z[8] + n<T>(1,3)*z[76] + n<T>(3,8)*z[9];
    z[76]=z[10]*z[76];
    z[72]=z[72] + z[81];
    z[72]=z[9]*z[72];
    z[78]= - 2*z[81] - z[8];
    z[78]=z[8]*z[78];
    z[72]=z[76] + n<T>(2,3)*z[78] + n<T>(1,3)*z[74] + n<T>(3,4)*z[72];
    z[72]=z[10]*z[72];
    z[74]= - z[65]*z[86];
    z[74]= - z[26] + z[74];
    z[74]=z[16]*z[74];
    z[76]=z[65] - z[86];
    z[76]=z[24]*z[76];
    z[74]=z[74] + z[76];
    z[76]= - z[2] + z[88] - z[81] + z[86];
    z[76]=z[22]*z[76];
    z[76]=z[76] - z[63];
    z[78]=n<T>(7,12)*z[5] + n<T>(7,6)*z[65] - z[4];
    z[78]=z[5]*z[78];
    z[67]=n<T>(5,2)*z[67] + z[78];
    z[70]=n<T>(5,36)*z[9] - z[70] + n<T>(7,24)*z[5] + n<T>(7,9)*z[65] - n<T>(5,8)*z[4];
    z[70]=z[9]*z[70];
    z[78]=z[65]*z[3];
    z[67]=z[70] + n<T>(1,2)*z[67] + z[78];
    z[67]=z[9]*z[67];
    z[70]=z[65]*z[84];
    z[70]=z[26] + z[70];
    z[70]=z[15]*z[70];
    z[79]= - z[5] + z[81];
    z[79]=z[21]*z[79];
    z[70]= - z[70] - z[79] + z[52] + z[40];
    z[78]=z[78] + z[26];
    z[79]= - z[14]*z[65];
    z[79]=z[79] - z[78];
    z[79]=z[12]*z[79];
    z[80]=z[14] - z[90];
    z[80]=z[25]*z[80];
    z[79]=z[79] + z[80];
    z[77]= - z[77] + n<T>(1,4)*z[4];
    z[68]=z[77]*z[68];
    z[77]=n<T>(35,18)*z[5] + n<T>(1,8)*z[65] + n<T>(2,3)*z[4];
    z[77]=z[5]*z[77];
    z[68]=z[68] + z[77];
    z[68]=z[5]*z[68];
    z[65]= - z[9]*z[65];
    z[65]=z[65] - z[78];
    z[65]=z[13]*z[65];
    z[77]=z[81]*npow(z[4],2);
    z[77]=z[41] + z[77] - z[55] + z[48] - z[46];
    z[78]=z[10] + z[14];
    z[78]=z[38]*z[78];
    z[78]=z[78] - z[58];
    z[80]= - z[9] - z[6];
    z[80]=z[37]*z[80];
    z[80]=z[80] + z[50];
    z[81]=n<T>(10,3)*z[32] + n<T>(2,3)*z[30] - n<T>(23,108)*z[27];
    z[81]=i*z[81];
    z[83]=n<T>(3,2)*z[4] + n<T>(7,3)*z[5];
    z[83]=n<T>(1,2)*z[83] - n<T>(7,9)*z[3];
    z[83]= - n<T>(11,54)*z[14] - n<T>(19,108)*z[10] + n<T>(1,6)*z[8] + n<T>(4,9)*z[2]
     - 
   n<T>(17,216)*z[7] + n<T>(2,9)*z[6] + n<T>(1,2)*z[83] - n<T>(8,27)*z[9];
    z[83]=z[26]*z[83];
    z[84]=2*z[14] + z[10] - z[8] + 3*z[5] + z[6];
    z[84]=z[36]*z[84];
    z[82]=z[3] - z[82];
    z[82]=z[17]*z[82];
    z[85]=z[9] - z[90];
    z[85]=z[20]*z[85];
    z[86]=z[8] + z[10];
    z[86]=z[35]*z[86];

    r += z[28] - n<T>(4,9)*z[29] - n<T>(5,3)*z[33] + n<T>(11,2)*z[34] + z[39] + n<T>(23,24)*z[42]
     + n<T>(1,12)*z[44]
     + n<T>(5,8)*z[45]
     + n<T>(3,8)*z[47] - n<T>(11,24)*
      z[51] - n<T>(8,3)*z[53] + n<T>(11,72)*z[54] + n<T>(7,24)*z[56] - 4*z[60] - n<T>(1,9)*z[62]
     + z[64]
     + z[65]
     + z[66]
     + z[67]
     + z[68]
     + z[69] - n<T>(1,2)*
      z[70] + z[71] + z[72] + z[73] + n<T>(11,18)*z[74] + z[75] + n<T>(2,3)*
      z[76] + n<T>(1,4)*z[77] + n<T>(5,9)*z[78] + n<T>(2,9)*z[79] + n<T>(7,18)*z[80]
     +  z[81] + z[82] + z[83] + z[84] + z[85] + n<T>(4,3)*z[86] - n<T>(1,3)*z[87]
      ;
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1340(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1340(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
