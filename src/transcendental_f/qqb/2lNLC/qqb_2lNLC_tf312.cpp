#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf312(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[46];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[2];
    z[7]=d[5];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[6];
    z[11]=d[17];
    z[12]=e[2];
    z[13]=e[9];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[31];
    z[18]=e[3];
    z[19]=e[10];
    z[20]=e[6];
    z[21]=e[14];
    z[22]=d[9];
    z[23]=f[4];
    z[24]=f[12];
    z[25]=f[16];
    z[26]=f[31];
    z[27]=f[36];
    z[28]=f[39];
    z[29]=f[51];
    z[30]=f[55];
    z[31]=f[58];
    z[32]=z[2] - z[3];
    z[33]=3*z[8];
    z[34]=z[33] - z[7];
    z[35]=i*z[1];
    z[32]=n<T>(4,3)*z[4] - 11*z[35] - 3*z[6] - z[10] + z[34] - 14*z[32];
    z[32]=z[4]*z[32];
    z[36]=z[3] + 2*z[2];
    z[34]= - z[6] - z[36] + z[34];
    z[34]=z[34]*z[35];
    z[34]=z[34] + z[21] + 8*z[19];
    z[35]=2*z[3];
    z[37]= - z[35] + z[8];
    z[33]=z[37]*z[33];
    z[37]=npow(z[6],2);
    z[38]=npow(z[10],2);
    z[39]=npow(z[3],2);
    z[40]=2*z[6];
    z[41]=z[40] - z[7];
    z[41]=z[7]*z[41];
    z[42]=4*z[3] - 5*z[2];
    z[42]=z[2]*z[42];
    z[32]=z[32] + z[33] + z[42] + z[41] + z[39] + z[38] - n<T>(16,3)*z[14]
    + z[37] + 2*z[34];
    z[32]=z[4]*z[32];
    z[33]=5*z[7];
    z[34]= - z[9] + n<T>(1,2)*z[7];
    z[34]=z[34]*z[33];
    z[41]=npow(z[2],2);
    z[41]=z[41] - z[20] - z[18];
    z[42]=n<T>(1,3)*z[14];
    z[43]=npow(z[9],2);
    z[44]= - n<T>(7,3)*z[8] + 4*z[2] + n<T>(5,2)*z[7] + n<T>(11,2)*z[9] - 13*z[3];
    z[44]=z[8]*z[44];
    z[34]=z[44] + z[34] + 3*z[39] - n<T>(1,2)*z[43] + z[42] + 6*z[41];
    z[34]=z[8]*z[34];
    z[41]= - z[10]*z[11];
    z[44]=z[11] + z[10];
    z[44]=z[9]*z[44];
    z[45]=z[5]*z[35];
    z[41]=z[45] + z[44] + z[41] - z[13] - 2*z[12];
    z[40]=n<T>(3,2)*z[7] + z[40] - 5*z[9];
    z[40]=z[7]*z[40];
    z[35]= - z[5] + z[35];
    z[35]=2*z[35] + z[2];
    z[35]=z[2]*z[35];
    z[33]=n<T>(15,2)*z[8] + z[33] + z[9] - 6*z[3];
    z[33]=z[8]*z[33];
    z[33]=z[33] + z[35] + 2*z[41] + z[40];
    z[33]=z[1]*z[33];
    z[33]= - n<T>(1,3)*z[15] + z[33];
    z[33]=i*z[33];
    z[35]=z[6] - n<T>(5,2)*z[9];
    z[35]=z[7]*z[35];
    z[35]=z[35] + n<T>(5,2)*z[43] - z[37] - z[14];
    z[35]=z[7]*z[35];
    z[37]= - z[12] + 9*z[19] - 7*z[18];
    z[36]=z[2]*z[36];
    z[36]=z[36] - 4*z[39] + 2*z[37] - z[14];
    z[36]=z[2]*z[36];
    z[37]= - 2*z[20] - z[13];
    z[37]= - z[38] + 2*z[37] - z[14];
    z[37]=z[10]*z[37];
    z[39]=z[9]*z[10];
    z[38]=z[13] + z[38] - z[39];
    z[38]=n<T>(1,2)*z[14] + 2*z[38];
    z[38]=z[9]*z[38];
    z[39]= - z[28] + z[26] + z[27];
    z[40]=z[21]*z[22];
    z[41]=z[11] + 2*z[5];
    z[41]=2*z[41] - n<T>(1,3)*z[6];
    z[41]=z[14]*z[41];
    z[42]=4*z[12] + z[42];
    z[42]=z[3]*z[42];

    r +=  - z[16] + n<T>(33,2)*z[17] + 14*z[23] + 13*z[24] + 3*z[25] + 
      z[29] + z[30] - z[31] + z[32] + z[33] + z[34] + z[35] + z[36] + 
      z[37] + z[38] - n<T>(5,2)*z[39] + 2*z[40] + z[41] + z[42];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf312(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf312(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
