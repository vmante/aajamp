#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf179(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[77];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[9];
    z[8]=d[1];
    z[9]=d[15];
    z[10]=d[5];
    z[11]=d[16];
    z[12]=d[4];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[4];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=c[3];
    z[21]=d[6];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[10];
    z[31]=e[3];
    z[32]=e[6];
    z[33]=e[7];
    z[34]=e[14];
    z[35]=e[13];
    z[36]=f[0];
    z[37]=f[1];
    z[38]=f[3];
    z[39]=f[4];
    z[40]=f[5];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[16];
    z[44]=f[18];
    z[45]=f[23];
    z[46]=f[31];
    z[47]=f[36];
    z[48]=f[39];
    z[49]=f[51];
    z[50]=f[55];
    z[51]=f[58];
    z[52]=z[1]*i;
    z[53]=2*z[52];
    z[54]=z[53] - z[5];
    z[55]=n<T>(1,2)*z[2];
    z[56]=n<T>(1,2)*z[4];
    z[57]= - z[6] - z[55] + z[56] - z[54];
    z[57]=z[5]*z[57];
    z[58]=n<T>(1,4)*z[2];
    z[59]=n<T>(1,2)*z[6];
    z[60]= - n<T>(1,12)*z[8] - z[59] - z[58] - z[53] - n<T>(5,4)*z[4];
    z[60]=z[8]*z[60];
    z[61]=7*z[52];
    z[62]= - z[61] + 5*z[4];
    z[62]=z[62]*z[56];
    z[63]=5*z[52];
    z[64]=n<T>(29,4)*z[6] - z[55] + z[63] - n<T>(7,2)*z[4];
    z[64]=z[6]*z[64];
    z[58]=z[4] - z[58];
    z[58]=z[2]*z[58];
    z[57]=z[60] + z[57] + z[64] + z[62] + z[58];
    z[57]=z[8]*z[57];
    z[58]=z[56] - z[52];
    z[60]=z[58]*z[4];
    z[62]=z[52] - z[3];
    z[64]=z[2] - z[4];
    z[65]=n<T>(3,2)*z[6] - z[64] - z[62];
    z[66]=n<T>(1,2)*z[3];
    z[65]=z[65]*z[66];
    z[67]=z[53] - z[4];
    z[67]=z[2]*z[67];
    z[68]= - n<T>(1,2)*z[52] - z[6];
    z[68]=z[6]*z[68];
    z[69]=npow(z[12],2);
    z[65]=z[65] - n<T>(1,2)*z[69] + 3*z[68] + z[60] + z[67];
    z[65]=z[3]*z[65];
    z[67]=z[59] + z[52];
    z[68]=z[4] - z[67];
    z[68]=z[6]*z[68];
    z[68]= - z[60] + z[68];
    z[69]=3*z[12];
    z[70]=n<T>(7,2)*z[8];
    z[71]= - z[52] + 19*z[4];
    z[71]=z[69] + z[70] - n<T>(9,2)*z[5] - n<T>(13,4)*z[6] + n<T>(1,4)*z[71] + z[2];
    z[71]=z[12]*z[71];
    z[72]=z[52] - n<T>(1,2)*z[5];
    z[72]=z[5]*z[72];
    z[73]=npow(z[8],2);
    z[68]=n<T>(1,2)*z[71] - n<T>(5,2)*z[73] + n<T>(17,4)*z[68] + z[72];
    z[68]=z[12]*z[68];
    z[67]=z[6]*z[67];
    z[71]= - z[66] + z[52] + z[6];
    z[71]=z[3]*z[71];
    z[67]=z[67] - z[71];
    z[71]=z[5] - z[6];
    z[71]= - 9*z[52] + 5*z[71];
    z[71]=z[3] + n<T>(1,4)*z[71] - z[12];
    z[71]=z[10]*z[71];
    z[72]=z[5]*z[54];
    z[73]=z[52] - z[5];
    z[74]= - 2*z[73] - z[12];
    z[74]=z[12]*z[74];
    z[67]=z[71] + z[74] + z[72] - n<T>(5,2)*z[67];
    z[67]=z[10]*z[67];
    z[56]= - n<T>(5,2)*z[2] + z[63] + z[56];
    z[56]=z[2]*z[56];
    z[61]=z[61] + z[4];
    z[61]=z[4]*z[61];
    z[56]=z[61] + z[56];
    z[56]=z[56]*z[55];
    z[58]= - z[55] - z[58];
    z[58]=z[15]*z[58];
    z[61]=npow(z[4],3);
    z[56]=z[44] + z[58] - z[61] + z[56];
    z[58]=n<T>(1,3)*z[7] + z[52] - z[2];
    z[58]=z[7]*z[58];
    z[58]=z[58] + z[35];
    z[61]= - z[2]*z[52];
    z[63]=z[2] - z[66];
    z[63]=z[3]*z[63];
    z[58]=z[61] + z[63] + n<T>(1,2)*z[58];
    z[58]=z[7]*z[58];
    z[55]=z[55] - 3*z[52] + z[4];
    z[55]=z[2]*z[55];
    z[55]= - n<T>(3,2)*z[60] + z[55];
    z[60]=15*z[52] - 7*z[4];
    z[60]=n<T>(3,8)*z[60] + n<T>(5,3)*z[6];
    z[60]=z[6]*z[60];
    z[55]=n<T>(1,2)*z[55] + z[60];
    z[55]=z[6]*z[55];
    z[59]=n<T>(2,3)*z[5] + z[59];
    z[59]=z[64]*z[59];
    z[60]=4*z[52] + z[4];
    z[60]=z[4]*z[60];
    z[61]= - z[53] - z[4];
    z[61]=2*z[61] + z[2];
    z[61]=z[2]*z[61];
    z[60]=z[60] + z[61];
    z[59]=n<T>(1,3)*z[60] + z[59];
    z[59]=z[5]*z[59];
    z[60]=npow(z[5],2);
    z[61]=npow(z[21],2);
    z[60]= - z[33] - n<T>(1,3)*z[61] + z[60] + n<T>(5,3)*z[20];
    z[61]=n<T>(1,2)*z[21];
    z[60]=z[61]*z[60];
    z[61]= - 3*z[4] + z[70];
    z[61]=z[52]*z[61];
    z[61]= - 3*z[20] + z[61];
    z[61]=z[9]*z[61];
    z[63]=z[52] - z[4];
    z[63]=3*z[63] + z[70];
    z[63]=z[16]*z[63];
    z[64]= - z[5] - n<T>(17,2)*z[4] + 73*z[2];
    z[64]=n<T>(17,6)*z[10] - n<T>(5,6)*z[3] + n<T>(13,3)*z[12] + z[6] + n<T>(1,18)*z[64]
   ;
    z[64]=z[20]*z[64];
    z[64]=z[64] - z[45] + z[38] - z[37];
    z[66]=z[40] - z[26];
    z[71]=z[51] - z[46];
    z[54]= - z[2] + z[54];
    z[54]=z[18]*z[54];
    z[54]=z[54] - z[41];
    z[72]=n<T>(1,3)*z[27] - n<T>(5,6)*z[25] + n<T>(3,4)*z[22];
    z[72]=i*z[72];
    z[70]= - z[70] - z[5] + z[2] - 6*z[6];
    z[70]=z[30]*z[70];
    z[53]=z[3] - z[53] + z[2];
    z[53]=z[14]*z[53];
    z[74]= - z[3]*z[52];
    z[74]=z[74] - z[20];
    z[74]=z[11]*z[74];
    z[52]= - z[5]*z[52];
    z[52]=z[52] - z[20];
    z[52]=z[13]*z[52];
    z[62]= - z[17]*z[62];
    z[73]=z[19]*z[73];
    z[75]=n<T>(5,2)*z[8] - z[12];
    z[75]=z[31]*z[75];
    z[69]=z[69] + n<T>(5,2)*z[21];
    z[69]=z[32]*z[69];
    z[76]=2*z[6] + n<T>(5,2)*z[7];
    z[76]=z[34]*z[76];

    r += n<T>(3,4)*z[23] - n<T>(41,72)*z[24] - n<T>(1,6)*z[28] - n<T>(103,8)*z[29]
     +  z[36] - n<T>(17,8)*z[39] - n<T>(19,8)*z[42] - n<T>(13,8)*z[43] + n<T>(7,4)*z[47]
       - z[48] + z[49] + z[50] + z[52] + z[53] + n<T>(2,3)*z[54] + z[55] + 
      n<T>(1,3)*z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + 
      z[63] + n<T>(1,2)*z[64] + z[65] - n<T>(5,12)*z[66] + z[67] + z[68] + 
      z[69] + z[70] - n<T>(5,4)*z[71] + z[72] + z[73] + z[74] + z[75] + 
      z[76];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf179(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf179(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
