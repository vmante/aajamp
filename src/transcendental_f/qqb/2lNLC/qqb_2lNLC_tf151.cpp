#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf151(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[65];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[3];
    z[4]=d[5];
    z[5]=d[8];
    z[6]=d[1];
    z[7]=d[7];
    z[8]=d[15];
    z[9]=d[2];
    z[10]=d[4];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=d[18];
    z[14]=e[2];
    z[15]=e[8];
    z[16]=e[9];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[19];
    z[21]=c[23];
    z[22]=c[25];
    z[23]=c[26];
    z[24]=c[28];
    z[25]=c[31];
    z[26]=e[3];
    z[27]=e[10];
    z[28]=e[6];
    z[29]=e[7];
    z[30]=e[13];
    z[31]=e[11];
    z[32]=e[14];
    z[33]=d[9];
    z[34]=f[4];
    z[35]=f[5];
    z[36]=f[9];
    z[37]=f[11];
    z[38]=f[12];
    z[39]=f[16];
    z[40]=f[31];
    z[41]=f[32];
    z[42]=f[34];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[51];
    z[46]=f[55];
    z[47]=f[58];
    z[48]=npow(z[9],2);
    z[49]=npow(z[2],2);
    z[48]=z[49] + 3*z[48];
    z[50]=3*z[4];
    z[51]=2*z[9] - z[4];
    z[51]=z[51]*z[50];
    z[52]=2*z[7];
    z[53]=z[2] - z[6];
    z[53]=z[53]*z[52];
    z[54]=3*z[10];
    z[55]= - z[54] + n<T>(5,3)*z[6];
    z[56]=n<T>(2,3)*z[3];
    z[57]= - z[56] - z[55];
    z[58]=2*z[3];
    z[57]=z[57]*z[58];
    z[50]=z[50] + z[54];
    z[59]=n<T>(10,3)*z[5] - n<T>(34,3)*z[3] + n<T>(61,3)*z[6] - 7*z[9] - z[50] - 
    z[7] - z[2];
    z[59]=z[5]*z[59];
    z[60]=npow(z[6],2);
    z[61]=npow(z[10],2);
    z[62]= - n<T>(23,3)*z[27] + z[31] + 2*z[32];
    z[51]=z[59] + z[57] + z[53] + z[51] - 3*z[61] - n<T>(10,3)*z[60] + n<T>(22,9)
   *z[17] + 2*z[62] + z[48];
    z[51]=z[5]*z[51];
    z[53]= - z[56] + z[7] + n<T>(13,3)*z[6] - z[54] + z[2];
    z[53]=z[3]*z[53];
    z[54]=npow(z[11],2);
    z[56]=npow(z[4],2);
    z[57]=2*z[2];
    z[59]= - z[57] - z[7];
    z[59]=z[7]*z[59];
    z[53]=z[53] + z[59] + z[56] + 17*z[61] - n<T>(25,3)*z[60] + z[49] - n<T>(7,9)
   *z[17] - n<T>(16,3)*z[14] - z[54];
    z[53]=z[3]*z[53];
    z[56]=3*z[16];
    z[59]=z[11]*z[12];
    z[59]=z[49] + 4*z[59] + n<T>(8,3)*z[14] + z[56] + 2*z[15];
    z[62]=3*z[9];
    z[50]=n<T>(4,3)*z[3] + n<T>(8,3)*z[6] - z[57] - z[13] - z[62] - z[50];
    z[50]=2*z[50] + n<T>(61,3)*z[5];
    z[50]=z[5]*z[50];
    z[57]=7*z[10];
    z[62]= - z[57] + z[62] + z[2];
    z[63]=7*z[4];
    z[62]=2*z[62] - z[63];
    z[62]=z[4]*z[62];
    z[63]= - z[6] + z[63] - z[10];
    z[64]= - z[12] - z[11];
    z[64]=3*z[64] + z[63];
    z[52]=z[64]*z[52];
    z[64]=n<T>(8,3)*z[8];
    z[55]= - z[64] - z[2] + z[7] - z[55];
    z[55]=z[55]*z[58];
    z[58]=2*z[8] - z[6];
    z[58]=z[6]*z[58];
    z[50]=z[50] + z[55] + z[52] + z[62] - 5*z[61] + 2*z[59] + n<T>(25,3)*
    z[58];
    z[50]=z[1]*z[50];
    z[52]= - 2*z[23] - z[21];
    z[50]=2*z[52] + n<T>(67,18)*z[18] + z[50];
    z[50]=i*z[50];
    z[52]=n<T>(2,3)*z[17];
    z[55]= - z[56] - z[15];
    z[56]=14*z[10] + 5*z[4];
    z[56]=z[4]*z[56];
    z[58]=6*z[11] + z[2] - z[63];
    z[58]=z[7]*z[58];
    z[55]=z[58] + z[56] - 12*z[61] - z[52] + 2*z[55] - 7*z[54];
    z[55]=z[7]*z[55];
    z[56]=z[29] + z[30];
    z[57]=n<T>(5,3)*z[4] - z[57] + z[9] + z[11];
    z[57]=z[4]*z[57];
    z[48]=z[57] - 7*z[61] + 9*z[17] + 2*z[56] - z[48];
    z[48]=z[4]*z[48];
    z[56]=4*z[28] + z[26];
    z[57]= - z[6] + n<T>(17,3)*z[10];
    z[57]=z[10]*z[57];
    z[56]=z[57] - 13*z[60] + 4*z[56] - n<T>(5,6)*z[17];
    z[56]=z[10]*z[56];
    z[57]= - n<T>(5,3)*z[27] + 3*z[26];
    z[57]=5*z[60] + n<T>(5,3)*z[17] + 4*z[57] + n<T>(25,3)*z[14];
    z[57]=z[6]*z[57];
    z[57]=z[57] + z[24];
    z[58]=3*z[28] + 4*z[16];
    z[54]=2*z[58] + 3*z[54];
    z[54]=z[11]*z[54];
    z[52]= - z[15] + z[52];
    z[49]=2*z[52] - z[49];
    z[49]=z[2]*z[49];
    z[52]=z[39] + z[47];
    z[58]=2*z[30] + 4*z[32];
    z[58]=z[33]*z[58];
    z[59]= - 3*z[12] - z[64];
    z[59]=n<T>(17,6)*z[11] + 2*z[59] - z[9];
    z[59]=z[17]*z[59];

    r +=  - n<T>(17,3)*z[19] - n<T>(1,6)*z[20] + z[22] - n<T>(55,2)*z[25] - n<T>(34,3)*
      z[34] - z[35] + z[36] - z[37] - 17*z[38] + 5*z[40] + z[41] - 
      z[42] + 4*z[43] - 7*z[44] + z[45] + z[46] + z[48] + z[49] + z[50]
       + z[51] - 3*z[52] + z[53] + z[54] + z[55] + z[56] + 2*z[57] + 
      z[58] + z[59];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf151(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf151(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
