#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf668(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[66];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[16];
    z[9]=d[15];
    z[10]=d[5];
    z[11]=d[4];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=c[3];
    z[21]=d[9];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[4];
    z[31]=e[3];
    z[32]=e[6];
    z[33]=e[14];
    z[34]=f[3];
    z[35]=f[4];
    z[36]=f[5];
    z[37]=f[11];
    z[38]=f[12];
    z[39]=f[16];
    z[40]=f[17];
    z[41]=f[31];
    z[42]=f[36];
    z[43]=f[39];
    z[44]=f[51];
    z[45]=f[55];
    z[46]=f[58];
    z[47]=n<T>(66604,3)*z[7];
    z[48]=z[1]*i;
    z[49]=z[48] - z[5];
    z[50]=z[7] + z[49];
    z[50]=z[50]*z[47];
    z[51]=z[7] + z[5];
    z[51]= - n<T>(276221,12)*z[3] + 6048*z[2] + n<T>(10565,3)*z[48] - n<T>(7351,4)*
    z[6] + n<T>(92783,12)*z[51];
    z[51]=z[3]*z[51];
    z[52]= - z[48] + n<T>(1,2)*z[6];
    z[53]=z[52]*z[6];
    z[54]= - 66604*z[48] + n<T>(155261,2)*z[5];
    z[54]=z[5]*z[54];
    z[55]=z[2]*z[49];
    z[56]=npow(z[11],2);
    z[50]=z[51] + n<T>(28177,2)*z[56] + 12096*z[55] + z[50] + n<T>(7351,2)*z[53]
    + n<T>(1,3)*z[54];
    z[50]=z[3]*z[50];
    z[51]= - z[48] + n<T>(1,2)*z[5];
    z[51]=z[51]*z[5];
    z[54]= - n<T>(1,2)*z[7] - z[49];
    z[54]=z[7]*z[54];
    z[54]=z[51] - z[54];
    z[54]=14293*z[53] - 40273*z[54];
    z[55]=12353*z[6];
    z[56]=n<T>(35119,4)*z[11] - n<T>(16461,2)*z[7] - 5858*z[5] + 18211*z[48] - 
    z[55];
    z[56]=z[11]*z[56];
    z[54]=n<T>(1,2)*z[54] + z[56];
    z[54]=z[11]*z[54];
    z[55]= - n<T>(35119,2)*z[48] + z[55];
    z[55]=z[6]*z[55];
    z[56]=z[48] - z[6];
    z[57]=z[56] + n<T>(1,2)*z[11];
    z[57]=z[57]*z[11];
    z[58]=z[6] + z[48] - z[12];
    z[58]=z[12]*z[58];
    z[55]=n<T>(14293,4)*z[58] + z[55] + n<T>(35119,2)*z[57];
    z[55]=z[12]*z[55];
    z[47]= - n<T>(35365,3)*z[3] + n<T>(7351,2)*z[56] - z[47];
    z[47]=z[19]*z[47];
    z[47]=z[54] + z[50] + z[55] + z[47];
    z[50]=35821*z[6];
    z[54]=z[52]*z[50];
    z[50]=n<T>(60317,6)*z[5] - n<T>(60317,3)*z[48] - z[50];
    z[50]=z[5]*z[50];
    z[50]=z[54] + z[50];
    z[54]=2*z[48];
    z[55]=z[54] - z[7];
    z[55]=z[7]*z[55];
    z[58]= - n<T>(41945,2)*z[2] + 88483*z[48] + 23573*z[5];
    z[58]=z[2]*z[58];
    z[59]= - n<T>(32759,4593)*z[2] - z[7] + n<T>(60317,18372)*z[5] - 3*z[48] + 
   n<T>(35821,6124)*z[6];
    z[59]=z[4]*z[59];
    z[50]=z[59] + n<T>(1,4593)*z[58] + n<T>(1,3062)*z[50] + z[55];
    z[50]=z[4]*z[50];
    z[55]=z[57] + z[53];
    z[57]= - z[54] - z[7];
    z[57]=z[7]*z[57];
    z[58]=z[48] + z[7];
    z[58]=2*z[58] - z[4];
    z[58]=z[4]*z[58];
    z[59]= - 13475*z[48] + 10413*z[6];
    z[59]= - n<T>(10413,3062)*z[11] + z[4] + n<T>(1,3062)*z[59] - z[7];
    z[59]=z[10]*z[59];
    z[55]=z[59] + z[58] + z[57] - n<T>(10413,1531)*z[55];
    z[55]=z[10]*z[55];
    z[57]=n<T>(92783,2)*z[3];
    z[58]= - 52369*z[5] - z[57];
    z[58]=z[48]*z[58];
    z[58]= - 52369*z[20] + z[58];
    z[58]=z[9]*z[58];
    z[57]=52369*z[49] - z[57];
    z[57]=z[16]*z[57];
    z[57]=z[58] + z[57];
    z[58]=npow(z[6],2);
    z[49]=n<T>(127225,6124)*z[49] + 2*z[7];
    z[49]=z[7]*z[49];
    z[49]=n<T>(1,3)*z[49] + z[58] + n<T>(12389,9186)*z[51];
    z[49]=z[7]*z[49];
    z[51]=9385*z[48] - n<T>(53071,4)*z[6];
    z[51]=z[6]*z[51];
    z[59]= - n<T>(11260,3)*z[48] + 11687*z[6];
    z[59]=2*z[59] - n<T>(64291,6)*z[5];
    z[59]=z[5]*z[59];
    z[60]= - n<T>(134999,3)*z[48] - n<T>(12447,2)*z[6];
    z[60]=n<T>(134999,12)*z[2] + n<T>(1,2)*z[60] - n<T>(3442,3)*z[5];
    z[60]=z[2]*z[60];
    z[51]=z[60] + z[51] + z[59];
    z[59]=2*z[56] + z[7];
    z[59]=z[7]*z[59];
    z[51]=z[59] + n<T>(1,1531)*z[51];
    z[51]=z[2]*z[51];
    z[59]=z[8]*z[48];
    z[59]= - z[30] + z[59];
    z[60]=z[4] - z[2];
    z[59]=z[60]*z[59];
    z[60]= - z[6] - z[12];
    z[60]=z[48]*z[60];
    z[60]= - z[20] + z[60];
    z[60]=z[13]*z[60];
    z[56]= - z[12] + z[56];
    z[56]=z[18]*z[56];
    z[56]=z[60] + z[56];
    z[60]=z[48] - n<T>(1,6)*z[6];
    z[58]=z[60]*z[58];
    z[60]= - z[11] - z[3];
    z[60]=z[31]*z[60];
    z[58]=z[58] + z[60];
    z[48]= - n<T>(11,6)*z[5] + 11*z[48] - n<T>(10927,6124)*z[6];
    z[48]=z[5]*z[48];
    z[48]= - n<T>(10927,3062)*z[53] + z[48];
    z[48]=z[5]*z[48];
    z[53]=z[36] - z[26];
    z[60]=z[43] - z[41];
    z[61]=13475*z[10] + n<T>(28177,4)*z[3] + n<T>(140371,4)*z[11] - n<T>(170549,12)*
    z[4] - n<T>(135499,3)*z[2] + n<T>(109331,6)*z[7] + 32951*z[6] + 35327*z[5];
    z[61]=n<T>(1,3)*z[61] - 3471*z[12];
    z[61]=n<T>(3471,3062)*z[21] + n<T>(1,1531)*z[61];
    z[61]=z[20]*z[61];
    z[62]=n<T>(134999,4593)*z[27] + n<T>(134999,9186)*z[25] - n<T>(156803,110232)*
    z[22];
    z[62]=i*z[62];
    z[54]=z[54] - z[2];
    z[63]=z[4] - z[54];
    z[63]=z[14]*z[63];
    z[54]=z[5] - z[54];
    z[54]=z[15]*z[54];
    z[52]=n<T>(1,2)*z[2] + z[52];
    z[52]=z[17]*z[52];
    z[64]=z[11] + z[12];
    z[64]=z[32]*z[64];
    z[65]=z[7] + z[21];
    z[65]=z[33]*z[65];

    r += n<T>(283279,18372)*z[23] + n<T>(134999,110232)*z[24] - n<T>(134999,9186)*
      z[28] - n<T>(2284985,36744)*z[29] + n<T>(60317,18372)*z[34] - n<T>(127225,18372)*z[35]
     - n<T>(10927,6124)*z[37]
     + n<T>(5858,1531)*z[38] - n<T>(16461,3062)*z[39]
     + n<T>(35821,6124)*z[40]
     + n<T>(12353,1531)*z[42]
     + z[44]
     +  z[45] - z[46] + n<T>(1,1531)*z[47] + z[48] + z[49] + z[50] + z[51] + 
      n<T>(12447,1531)*z[52] + n<T>(134999,18372)*z[53] + n<T>(6884,4593)*z[54] + 
      z[55] + n<T>(14293,3062)*z[56] + n<T>(1,4593)*z[57] + n<T>(28177,3062)*z[58]
       + z[59] - n<T>(10413,3062)*z[60] + z[61] + z[62] + n<T>(41945,4593)*
      z[63] + n<T>(35119,3062)*z[64] + 2*z[65];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf668(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf668(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
