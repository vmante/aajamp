#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf289(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[62];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[5];
    z[10]=d[4];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[7];
    z[16]=e[8];
    z[17]=e[9];
    z[18]=e[10];
    z[19]=c[3];
    z[20]=d[9];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[3];
    z[30]=e[6];
    z[31]=e[14];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[11];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[23];
    z[38]=f[31];
    z[39]=f[36];
    z[40]=f[39];
    z[41]=f[51];
    z[42]=f[55];
    z[43]=f[58];
    z[44]=z[4] + z[6];
    z[45]=6*z[5];
    z[46]=z[1]*i;
    z[47]=17*z[46];
    z[44]= - n<T>(13,2)*z[3] + z[45] - z[47] + 6*z[44];
    z[44]=z[3]*z[44];
    z[48]=3*z[6] + n<T>(2,3)*z[46];
    z[49]=n<T>(1,3)*z[5];
    z[50]=z[48] - z[49];
    z[51]=2*z[5];
    z[50]=z[50]*z[51];
    z[52]=n<T>(1,2)*z[6];
    z[47]=z[47] + z[52];
    z[47]=z[6]*z[47];
    z[49]= - z[49] + n<T>(1,3)*z[4];
    z[48]= - z[48] + z[49];
    z[48]=z[4]*z[48];
    z[44]=z[44] + 2*z[48] + z[47] - z[50];
    z[47]=z[4] + z[5];
    z[48]=n<T>(1,2)*z[3];
    z[47]= - n<T>(2,15)*z[2] + z[48] + n<T>(4,15)*z[46] - z[52] + n<T>(1,15)*z[47];
    z[47]=z[2]*z[47];
    z[44]=n<T>(1,5)*z[44] + z[47];
    z[44]=z[2]*z[44];
    z[47]=2*z[46];
    z[53]=z[6] + z[47] - z[5];
    z[53]=z[53]*z[51];
    z[54]=z[46] + z[6];
    z[45]= - 7*z[4] + 13*z[54] - z[45];
    z[55]=n<T>(1,5)*z[4];
    z[45]=z[45]*z[55];
    z[56]= - 29*z[46] - 53*z[6];
    z[56]=z[6]*z[56];
    z[57]= - n<T>(14,3)*z[3] + n<T>(31,10)*z[4] + n<T>(32,5)*z[46] + n<T>(5,2)*z[6];
    z[57]=z[3]*z[57];
    z[45]=z[57] + z[45] + n<T>(1,5)*z[56] + z[53];
    z[45]=z[3]*z[45];
    z[53]= - z[4] + 41*z[3];
    z[56]= - z[46]*z[53];
    z[56]=z[19] + z[56];
    z[56]=z[7]*z[56];
    z[53]= - z[46] - z[53];
    z[53]=z[14]*z[53];
    z[57]= - 52*z[3] - 17*z[10];
    z[57]=z[29]*z[57];
    z[53]=z[57] + z[56] + z[53];
    z[56]= - z[46] + n<T>(91,2)*z[6];
    z[56]=z[56]*z[52];
    z[49]=n<T>(1,4)*z[6] - z[49];
    z[49]=z[4]*z[49];
    z[49]=z[49] + z[56] + z[50];
    z[49]=z[49]*z[55];
    z[50]=z[52] + z[46];
    z[50]=z[50]*z[6];
    z[52]=n<T>(1,2)*z[4] - z[54];
    z[52]=z[4]*z[52];
    z[52]=z[50] + z[52];
    z[55]= - 37*z[46] + 13*z[6];
    z[48]= - 24*z[10] - z[48] - n<T>(151,4)*z[4] + n<T>(1,4)*z[55] + 47*z[5];
    z[48]=z[10]*z[48];
    z[55]= - z[46] + n<T>(1,2)*z[5];
    z[55]=z[55]*z[5];
    z[48]=z[48] - 11*z[55] + n<T>(27,2)*z[52];
    z[52]=npow(z[3],2);
    z[48]=7*z[52] + n<T>(1,5)*z[48];
    z[48]=z[10]*z[48];
    z[52]=z[47] - z[2];
    z[56]= - z[4] + z[52];
    z[56]=z[13]*z[56];
    z[52]= - z[5] + z[52];
    z[52]=z[16]*z[52];
    z[52]= - z[25] - z[56] - z[52] + z[34] + z[33];
    z[56]=z[5]*z[46];
    z[56]=z[56] + z[19];
    z[57]= - z[11]*z[46];
    z[56]=6*z[56] + z[57];
    z[56]=z[12]*z[56];
    z[57]=z[46] - z[5];
    z[58]= - 6*z[57] - z[11];
    z[58]=z[17]*z[58];
    z[56]=z[56] + z[58];
    z[58]=2*z[57] + z[10];
    z[58]=z[10]*z[58];
    z[50]=z[50] + z[58];
    z[50]=3*z[55] + n<T>(7,5)*z[50];
    z[55]=11*z[46] + n<T>(21,5)*z[6];
    z[55]= - n<T>(3,10)*z[9] + n<T>(37,10)*z[10] + n<T>(1,2)*z[55] - n<T>(17,5)*z[5];
    z[55]=z[9]*z[55];
    z[50]=3*z[50] + z[55];
    z[50]=z[9]*z[50];
    z[55]=z[46] + n<T>(3,2)*z[6];
    z[55]=z[6]*z[55];
    z[54]= - n<T>(1,2)*z[9] - z[54];
    z[54]=z[9]*z[54];
    z[54]=z[55] + z[54];
    z[55]= - z[6] + z[9];
    z[55]=z[8]*z[55];
    z[54]=n<T>(3,2)*z[55] + 3*z[54] + n<T>(1,2)*z[19];
    z[54]=z[8]*z[54];
    z[47]=z[47] - 7*z[5];
    z[47]=z[5]*z[47];
    z[51]=z[51] + z[11];
    z[51]=z[11]*z[51];
    z[47]=z[47] + z[51];
    z[47]= - n<T>(7,6)*z[19] + n<T>(1,5)*z[47];
    z[47]=z[11]*z[47];
    z[51]= - z[41] + z[43] - z[42];
    z[55]= - 2*z[6] + n<T>(91,6)*z[5];
    z[55]=n<T>(1,15)*z[2] + n<T>(7,10)*z[3] + n<T>(1,5)*z[55] + n<T>(7,12)*z[4];
    z[55]= - n<T>(1,10)*z[20] - n<T>(67,15)*z[9] + n<T>(1,3)*z[55] - z[10];
    z[55]=z[19]*z[55];
    z[58]= - n<T>(8,15)*z[26] - n<T>(4,15)*z[24] - n<T>(143,60)*z[21];
    z[58]=i*z[58];
    z[59]= - n<T>(219,20)*z[46] - n<T>(10,3)*z[6];
    z[59]=z[59]*npow(z[6],2);
    z[46]=z[46] + 21*z[6];
    z[46]= - n<T>(6,5)*z[2] + n<T>(11,5)*z[3] + n<T>(1,5)*z[46] + z[5];
    z[46]=z[18]*z[46];
    z[57]= - z[9] + z[57];
    z[57]=z[15]*z[57];
    z[60]= - 7*z[11] - n<T>(48,5)*z[10] + z[9];
    z[60]=z[30]*z[60];
    z[61]= - z[6] - z[20];
    z[61]=z[31]*z[61];

    r += 3*z[22] - n<T>(1,45)*z[23] + n<T>(4,15)*z[27] + n<T>(133,15)*z[28] + n<T>(71,20)*z[32]
     + n<T>(151,20)*z[35]
     + n<T>(13,20)*z[36]
     + z[37] - n<T>(31,10)*
      z[38] - n<T>(41,10)*z[39] + n<T>(37,10)*z[40] + z[44] + z[45] + 2*z[46]
       + z[47] + z[48] + z[49] + z[50] + n<T>(21,10)*z[51] - n<T>(2,15)*z[52]
       + n<T>(1,5)*z[53] + n<T>(7,5)*z[54] + z[55] + n<T>(2,5)*z[56] + n<T>(3,5)*z[57]
       + z[58] + z[59] + z[60] + n<T>(21,5)*z[61];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf289(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf289(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
