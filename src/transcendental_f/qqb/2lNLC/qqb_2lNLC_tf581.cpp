#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf581(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[37];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[15];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[17];
    z[7]=d[8];
    z[8]=c[3];
    z[9]=d[3];
    z[10]=d[4];
    z[11]=e[2];
    z[12]=e[3];
    z[13]=e[10];
    z[14]=d[2];
    z[15]=d[7];
    z[16]=d[9];
    z[17]=e[13];
    z[18]=e[6];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[14];
    z[22]=f[4];
    z[23]=f[12];
    z[24]=f[31];
    z[25]=f[36];
    z[26]=f[51];
    z[27]=f[55];
    z[28]=npow(z[10],2);
    z[29]=npow(z[7],2);
    z[29]=z[28] + z[29];
    z[30]=n<T>(1,3)*z[8];
    z[31]=z[1]*i;
    z[32]=2*z[31];
    z[33]=z[3]*z[32];
    z[34]=n<T>(5,3)*z[2] - z[7] - z[31] - z[9] - z[10];
    z[34]=z[2]*z[34];
    z[29]=z[34] + z[33] + z[30] + 2*z[11] + z[13] + z[12] + n<T>(1,2)*z[29];
    z[29]=z[2]*z[29];
    z[33]= - z[6]*z[32];
    z[31]= - n<T>(2,3)*z[5] + z[15] + z[31];
    z[31]=z[5]*z[31];
    z[31]=z[31] + z[33] - z[30] + z[18] - 2*z[20] + z[19];
    z[31]=z[5]*z[31];
    z[32]=z[32] + z[16];
    z[33]=5*z[14];
    z[34]=z[33] - z[15];
    z[34]= - z[4] + n<T>(1,2)*z[34] - z[32];
    z[34]=z[4]*z[34];
    z[35]=npow(z[16],2);
    z[36]= - z[17] - z[30];
    z[34]=z[34] + 2*z[36] + z[35];
    z[34]=z[4]*z[34];
    z[33]= - z[33] + z[9];
    z[32]=z[7] + n<T>(1,2)*z[33] + z[32];
    z[32]=z[7]*z[32];
    z[30]=z[21] + z[30];
    z[30]=z[32] + 2*z[30] - z[35];
    z[30]=z[7]*z[30];
    z[32]= - z[15] + z[9];
    z[28]=z[32]*z[28];
    z[32]=z[27] + z[26];
    z[28]=z[28] + z[22] - z[23] - z[24] + 5*z[32] + z[25];
    z[32]=z[21] - z[17];
    z[32]=z[16]*z[32];

    r += n<T>(1,2)*z[28] + z[29] + z[30] + z[31] + 3*z[32] + z[34];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf581(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf581(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
