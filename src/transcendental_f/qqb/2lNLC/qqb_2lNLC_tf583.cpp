#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf583(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[31];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[15];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[17];
    z[7]=d[8];
    z[8]=c[3];
    z[9]=d[3];
    z[10]=d[4];
    z[11]=e[2];
    z[12]=e[3];
    z[13]=e[10];
    z[14]=d[7];
    z[15]=e[6];
    z[16]=e[7];
    z[17]=e[9];
    z[18]=f[4];
    z[19]=f[12];
    z[20]=f[31];
    z[21]=f[36];
    z[22]=npow(z[10],2);
    z[23]=n<T>(1,3)*z[8];
    z[22]=z[22] - z[23];
    z[24]= - 2*z[15] + z[17] + z[16];
    z[25]=z[1]*i;
    z[26]=2*z[25];
    z[27]=z[6]*z[26];
    z[28]=npow(z[4],2);
    z[29]=n<T>(2,3)*z[5] + z[10] - z[4] - z[14] - z[25];
    z[29]=z[5]*z[29];
    z[24]=z[29] + z[28] + z[27] + 2*z[24] - z[22];
    z[24]=z[5]*z[24];
    z[27]=npow(z[7],2);
    z[28]=z[27] + 2*z[12] + z[11] - z[13];
    z[29]= - z[3]*z[26];
    z[30]= - n<T>(5,3)*z[2] + 2*z[10] + z[9] + z[25];
    z[30]=z[2]*z[30];
    z[22]=z[30] + z[29] + z[22] - 2*z[28];
    z[22]=z[2]*z[22];
    z[28]=z[7]*z[9];
    z[28]=z[28] + z[13] - z[23];
    z[28]=z[7]*z[28];
    z[28]= - z[20] + z[28] + z[18];
    z[26]= - z[27]*z[26];
    z[23]=z[16] + z[23];
    z[25]= - z[14] + z[25];
    z[25]=2*z[25] + z[4];
    z[25]=z[4]*z[25];
    z[23]=2*z[23] + z[25];
    z[23]=z[4]*z[23];
    z[25]= - z[15] - z[12];
    z[27]=z[14] - z[9];
    z[27]=3*z[27] - z[10];
    z[27]=z[10]*z[27];
    z[25]=2*z[25] + z[27];
    z[25]=z[10]*z[25];
    z[27]=z[19] - z[21];

    r += z[22] + z[23] + z[24] + z[25] + z[26] + 3*z[27] + 2*z[28];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf583(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf583(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
