#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1053(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[37];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[17];
    z[11]=e[2];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=d[2];
    z[15]=e[10];
    z[16]=e[6];
    z[17]=f[4];
    z[18]=f[12];
    z[19]=f[14];
    z[20]=f[16];
    z[21]=f[30];
    z[22]=f[31];
    z[23]=f[36];
    z[24]=f[37];
    z[25]=f[39];
    z[26]=z[3] + z[2];
    z[27]=i*z[1];
    z[28]=z[27]*z[26];
    z[28]=z[13] + z[28];
    z[28]=z[6]*z[28];
    z[29]=z[9] + z[8];
    z[30]= - z[27]*z[29];
    z[30]= - z[13] + z[30];
    z[30]=z[10]*z[30];
    z[26]= - z[27] + z[26];
    z[26]=z[11]*z[26];
    z[29]=z[27] - z[29];
    z[29]=z[12]*z[29];
    z[26]= - z[21] + z[28] + z[30] + z[26] + z[29];
    z[28]=z[27]*z[4];
    z[29]=npow(z[4],2);
    z[30]=z[27] + z[4];
    z[31]= - 2*z[30] + z[8];
    z[31]=z[8]*z[31];
    z[32]= - n<T>(1,2)*z[4] + 2*z[8];
    z[32]=z[9]*z[32];
    z[31]=z[32] + z[31] - n<T>(7,2)*z[29] + z[28];
    z[31]=z[9]*z[31];
    z[32]=z[9] - z[3];
    z[32]=z[7] - z[8] + n<T>(1,3)*z[5] - n<T>(4,3)*z[4] + z[2] - n<T>(1,6)*z[32];
    z[32]=z[13]*z[32];
    z[31]=z[31] + z[32];
    z[28]=z[28] + n<T>(1,2)*z[29];
    z[32]=n<T>(8,3)*z[2];
    z[33]= - n<T>(5,6)*z[3] + z[32] - z[4] + n<T>(5,3)*z[27];
    z[33]=z[3]*z[33];
    z[34]=z[4] - n<T>(13,3)*z[27];
    z[32]=n<T>(13,6)*z[3] + n<T>(1,2)*z[34] - z[32];
    z[32]=z[5]*z[32];
    z[34]=2*z[27];
    z[35]= - z[34] - z[2];
    z[35]=z[2]*z[35];
    z[32]=z[32] + z[33] + n<T>(4,3)*z[35] + z[28];
    z[32]=z[5]*z[32];
    z[33]= - z[4] + n<T>(1,3)*z[27];
    z[33]=2*z[33] - n<T>(1,3)*z[2];
    z[33]=z[2]*z[33];
    z[35]=n<T>(3,2)*z[4] - n<T>(2,3)*z[2];
    z[35]=z[3]*z[35];
    z[33]=z[35] - 3*z[28] + z[33];
    z[33]=z[3]*z[33];
    z[35]= - n<T>(1,2)*z[9] + z[30];
    z[35]=z[9]*z[35];
    z[30]=z[9] - z[30];
    z[30]=z[7]*z[30];
    z[28]=n<T>(1,2)*z[30] + z[35] - z[28];
    z[28]=z[7]*z[28];
    z[30]=z[2] + z[5];
    z[30]=z[15]*z[30];
    z[35]=z[4] + z[8];
    z[35]=z[16]*z[35];
    z[30]=z[30] + z[35];
    z[34]=z[29]*z[34];
    z[35]=npow(z[4],3);
    z[34]=z[35] + z[34];
    z[35]=2*z[4];
    z[35]=z[35]*z[27];
    z[29]=z[35] + z[29];
    z[27]=z[4] + n<T>(2,3)*z[27];
    z[35]=n<T>(5,3)*z[2] + z[27];
    z[35]=z[2]*z[35];
    z[35]=z[35] + z[29];
    z[35]=z[2]*z[35];
    z[27]= - n<T>(1,3)*z[8] - z[27];
    z[27]=z[8]*z[27];
    z[27]=n<T>(1,3)*z[29] + z[27];
    z[27]=z[8]*z[27];
    z[29]=npow(z[2],2);
    z[36]=npow(z[8],2);
    z[29]= - z[29] + z[36];
    z[29]=z[14]*z[29];
    z[36]= - z[20] + z[25] - z[22];

    r += n<T>(13,6)*z[17] + n<T>(3,2)*z[18] - z[19] + n<T>(7,6)*z[23] + z[24] + n<T>(2,3)*z[26]
     + z[27]
     + z[28]
     + z[29]
     + n<T>(8,3)*z[30]
     + n<T>(1,3)*z[31]
     +  z[32] + z[33] + n<T>(4,3)*z[34] + z[35] - n<T>(1,2)*z[36];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1053(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1053(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
