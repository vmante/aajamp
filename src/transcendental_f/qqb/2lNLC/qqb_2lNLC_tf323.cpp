#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf323(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[51];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[3];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=d[7];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=e[2];
    z[14]=e[9];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[31];
    z[19]=e[3];
    z[20]=e[10];
    z[21]=e[6];
    z[22]=e[7];
    z[23]=e[13];
    z[24]=e[14];
    z[25]=d[9];
    z[26]=f[4];
    z[27]=f[12];
    z[28]=f[16];
    z[29]=f[31];
    z[30]=f[36];
    z[31]=f[39];
    z[32]=f[51];
    z[33]=f[55];
    z[34]=f[58];
    z[35]=2*z[3];
    z[36]= - z[35] + 3*z[5];
    z[37]=z[1]*i;
    z[38]=z[37]*z[36];
    z[38]=3*z[15] + z[38];
    z[38]=z[7]*z[38];
    z[36]= - 3*z[37] + z[36];
    z[36]=z[13]*z[36];
    z[39]=5*z[9];
    z[40]= - z[39] - 12*z[3];
    z[40]=z[19]*z[40];
    z[41]=13*z[6] + 15*z[3];
    z[41]=z[20]*z[41];
    z[42]= - z[23] + 2*z[24];
    z[42]=z[25]*z[42];
    z[36]=z[42] + z[38] + z[36] + z[40] + z[41];
    z[38]=8*z[6];
    z[40]=z[35] + z[38] + 7*z[37] - z[4];
    z[40]=z[3]*z[40];
    z[41]=2*z[9];
    z[42]= - z[37] - z[6];
    z[42]=5*z[42] - 11*z[9];
    z[42]=z[42]*z[41];
    z[43]= - z[4]*z[37];
    z[44]=2*z[37];
    z[45]= - z[44] + 23*z[6];
    z[45]=z[6]*z[45];
    z[46]= - 6*z[3] + z[6] + z[39];
    z[46]=z[5]*z[46];
    z[40]=z[46] + z[40] + z[42] + z[43] + z[45];
    z[40]=z[5]*z[40];
    z[42]=5*z[6];
    z[43]= - z[37] + z[4];
    z[43]=2*z[43] - z[42];
    z[43]=z[6]*z[43];
    z[45]=n<T>(1,3)*z[15];
    z[46]=z[44] + 3*z[4];
    z[46]=z[4]*z[46];
    z[47]= - z[4] + z[6];
    z[47]=z[8]*z[47];
    z[43]=z[47] - z[45] + z[46] + z[43];
    z[43]=z[8]*z[43];
    z[46]=z[37] - z[10];
    z[47]=z[10]*z[46];
    z[48]=z[10] - n<T>(1,3)*z[11];
    z[48]=z[11]*z[48];
    z[45]=z[48] + z[47] - z[45];
    z[45]=z[11]*z[45];
    z[47]=z[11] - z[10];
    z[47]= - z[37]*z[47];
    z[47]=z[15] + z[47];
    z[47]=z[12]*z[47];
    z[46]= - z[11] - z[46];
    z[46]=z[14]*z[46];
    z[45]= - z[29] + z[45] + z[47] + z[46] - z[30];
    z[46]=5*z[4];
    z[42]= - n<T>(14,3)*z[9] + z[42] + 12*z[37] + z[46];
    z[42]=z[9]*z[42];
    z[47]=z[44] + z[4];
    z[47]=z[47]*z[4];
    z[48]=z[44] + z[6];
    z[48]=z[6]*z[48];
    z[48]=z[47] + z[48];
    z[42]=5*z[48] + z[42];
    z[42]=z[9]*z[42];
    z[48]= - z[37] - 3*z[6];
    z[48]=z[48]*z[38];
    z[39]=z[39] + z[37];
    z[49]=n<T>(4,3)*z[3] - 4*z[6] + z[39];
    z[35]=z[49]*z[35];
    z[49]=npow(z[9],2);
    z[35]=z[35] + z[48] + 7*z[49];
    z[35]=z[3]*z[35];
    z[48]=2*z[4];
    z[49]= - 5*z[37] - z[48];
    z[49]=z[4]*z[49];
    z[39]= - z[46] + z[39];
    z[39]=z[9]*z[39];
    z[39]=z[49] + z[39];
    z[46]=z[46] - z[9];
    z[46]=z[10]*z[46];
    z[39]=2*z[39] + z[46];
    z[39]=z[10]*z[39];
    z[46]=2*z[6] - 18*z[37] - z[4];
    z[46]=z[6]*z[46];
    z[46]=4*z[24] - z[47] + z[46];
    z[46]=z[6]*z[46];
    z[47]= - z[22] - z[23];
    z[47]=z[48]*z[47];
    z[37]= - z[5] + z[37] + z[4];
    z[48]=z[3] - z[4];
    z[37]=z[48]*z[37];
    z[37]= - n<T>(1,6)*z[15] + z[37];
    z[37]=z[2]*z[37];
    z[48]=z[31] + z[28];
    z[49]=z[33] + z[32];
    z[44]=z[44] - n<T>(5,3)*z[4];
    z[44]=z[44]*npow(z[4],2);
    z[38]=z[10] + n<T>(5,6)*z[5] - n<T>(7,6)*z[3] + n<T>(2,3)*z[9] - n<T>(9,2)*z[4]
     - 
    z[38];
    z[38]=z[15]*z[38];
    z[41]= - z[41] - z[11];
    z[41]=z[21]*z[41];
    z[50]=z[16]*i;

    r +=  - z[17] + n<T>(59,2)*z[18] + 23*z[26] + 22*z[27] - z[34] + z[35]
       + 2*z[36] + z[37] + z[38] + z[39] + z[40] + 6*z[41] + z[42] + 
      z[43] + z[44] + 4*z[45] + z[46] + z[47] + 5*z[48] + 3*z[49] - n<T>(7,6)*z[50];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf323(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf323(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
