#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf81(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[49];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[79];
    z[3]=c[11];
    z[4]=d[1];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=c[12];
    z[10]=c[32];
    z[11]=c[33];
    z[12]=c[36];
    z[13]=c[38];
    z[14]=c[40];
    z[15]=c[43];
    z[16]=f[80];
    z[17]=g[38];
    z[18]=g[170];
    z[19]=g[173];
    z[20]=g[181];
    z[21]=g[183];
    z[22]=g[191];
    z[23]=g[198];
    z[24]=g[200];
    z[25]=g[201];
    z[26]=g[243];
    z[27]=g[251];
    z[28]=g[254];
    z[29]=g[270];
    z[30]=g[272];
    z[31]=g[276];
    z[32]=g[278];
    z[33]=g[281];
    z[34]=g[288];
    z[35]=g[289];
    z[36]=g[290];
    z[37]=g[359];
    z[38]=g[374];
    z[39]=n<T>(58,9)*z[13] - z[11];
    z[40]=z[5] - z[6];
    z[41]=z[40] + z[7];
    z[42]=n<T>(16,3)*z[4] + 11*z[8] - n<T>(49,3)*z[41];
    z[42]=z[3]*z[42];
    z[43]=z[2]*z[1];
    z[39]= - n<T>(49,2)*z[43] + 8*z[39] + n<T>(1,9)*z[42];
    z[39]=i*z[39];
    z[42]=7*z[16];
    z[42]= - z[42]*z[40];
    z[43]=z[30] - z[31];
    z[44]=z[21] - z[20] + z[18] - z[19];
    z[45]=z[35] + z[22] - z[24];
    z[46]=z[23] - z[34];
    z[40]=z[40] - z[8];
    z[40]=7*z[7] + 5*z[40];
    z[40]=n<T>(1,2)*z[40] - z[4];
    z[40]=z[2]*z[40];
    z[40]=z[36] + z[40] - z[25];
    z[47]=z[17] + z[33];
    z[48]=z[4] - z[8];
    z[48]=z[16]*z[48];
    z[41]= - 16*z[4] - 33*z[8] + 49*z[41];
    z[41]=z[9]*z[41];

    r += n<T>(80,27)*z[10] - 64*z[12] + n<T>(160,3)*z[14] + 16*z[15] - 49*z[26]
       + n<T>(21,2)*z[27] + n<T>(49,6)*z[28] - n<T>(3,4)*z[29] - n<T>(11,4)*z[32]
     - n<T>(1,3)*z[37]
     + z[38]
     + z[39]
     + 7*z[40]
     + z[41]
     + z[42]
     + n<T>(119,4)*
      z[43] - n<T>(21,4)*z[44] + n<T>(7,2)*z[45] - n<T>(7,4)*z[46] + 2*z[47] + 
      z[48];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf81(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf81(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
