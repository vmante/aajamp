#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf174(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[65];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[9];
    z[8]=d[15];
    z[9]=d[2];
    z[10]=d[5];
    z[11]=d[4];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=c[3];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[3];
    z[30]=e[6];
    z[31]=e[13];
    z[32]=e[14];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[23];
    z[39]=f[31];
    z[40]=f[36];
    z[41]=f[39];
    z[42]=f[51];
    z[43]=f[55];
    z[44]=f[58];
    z[45]=n<T>(1,2)*z[6];
    z[46]=z[1]*i;
    z[47]= - 39*z[46] - n<T>(137,6)*z[6];
    z[47]=z[47]*z[45];
    z[48]=2*z[46];
    z[49]=z[48] - z[5];
    z[50]=z[49]*z[5];
    z[47]= - n<T>(14,9)*z[50] + z[47];
    z[51]=npow(z[3],2);
    z[52]=z[46] + z[6];
    z[53]= - n<T>(1,2)*z[4] + z[52];
    z[53]=z[4]*z[53];
    z[54]=z[46] - z[5];
    z[55]=2*z[54] + z[10];
    z[55]=z[10]*z[55];
    z[56]= - n<T>(184,9)*z[11] + n<T>(2,3)*z[10] + n<T>(1693,12)*z[4] - n<T>(241,3)*z[3]
    - n<T>(203,4)*z[6] - n<T>(1969,12)*z[46] + 23*z[5];
    z[56]=z[11]*z[56];
    z[47]=n<T>(1,15)*z[56] + n<T>(2,45)*z[55] + n<T>(39,10)*z[53] + n<T>(1,5)*z[47]
     - n<T>(1,3)*z[51];
    z[47]=z[11]*z[47];
    z[53]=n<T>(104,27)*z[5];
    z[53]=z[49]*z[53];
    z[55]=8*z[5];
    z[56]= - n<T>(2663,36)*z[6] + n<T>(91,6)*z[46] + z[55];
    z[56]=z[6]*z[56];
    z[57]=n<T>(91,9)*z[3] - n<T>(8,3)*z[6] - n<T>(13,3)*z[46] - z[55];
    z[57]=z[3]*z[57];
    z[58]=z[4] - z[5];
    z[58]=4*z[3] - n<T>(7,4)*z[6] - n<T>(4,9)*z[58];
    z[58]=z[4]*z[58];
    z[56]=n<T>(13,3)*z[58] + z[57] + z[53] + z[56];
    z[56]=z[4]*z[56];
    z[57]=19*z[46] + 31*z[6];
    z[57]=z[6]*z[57];
    z[52]= - 19*z[52] - 12*z[10];
    z[52]=z[10]*z[52];
    z[52]=z[57] + z[52];
    z[57]= - z[6] + z[10];
    z[57]=z[9]*z[57];
    z[52]=19*z[57] + 2*z[52] + n<T>(11,2)*z[20];
    z[52]=z[9]*z[52];
    z[52]=z[56] + z[52];
    z[55]= - z[55] + n<T>(209,9)*z[46];
    z[56]=n<T>(7,18)*z[6] + z[55];
    z[56]=z[6]*z[56];
    z[55]= - n<T>(151,18)*z[3] + 8*z[6] - z[55];
    z[55]=z[3]*z[55];
    z[57]=z[4] - z[48] - z[5];
    z[57]=z[3] - z[6] + n<T>(13,27)*z[57];
    z[57]=z[4]*z[57];
    z[53]=8*z[57] + z[55] - z[53] + z[56];
    z[55]=z[4] + 4*z[46] + z[5];
    z[45]= - n<T>(8,15)*z[2] + n<T>(1,2)*z[3] - z[45] + n<T>(4,15)*z[55];
    z[45]=z[2]*z[45];
    z[45]=n<T>(1,5)*z[53] + n<T>(13,9)*z[45];
    z[45]=z[2]*z[45];
    z[53]=n<T>(1,5)*z[5];
    z[55]=n<T>(19,5)*z[6];
    z[56]=n<T>(59,90)*z[10] + z[55] + 5*z[46] - z[53];
    z[56]=z[10]*z[56];
    z[57]=z[48] + z[6];
    z[55]=z[57]*z[55];
    z[50]=z[56] + n<T>(1,2)*z[51] + n<T>(1,9)*z[50] + z[55];
    z[50]=z[10]*z[50];
    z[51]=31*z[12];
    z[55]=z[46]*z[51];
    z[56]= - z[5]*z[46];
    z[56]=z[56] - z[20];
    z[55]=n<T>(28,3)*z[56] + z[55];
    z[55]=z[13]*z[55];
    z[51]=n<T>(28,3)*z[54] + z[51];
    z[51]=z[18]*z[51];
    z[51]=z[55] + z[51];
    z[55]=91*z[4] + 109*z[3];
    z[56]= - z[46]*z[55];
    z[56]= - 91*z[20] + z[56];
    z[56]=z[8]*z[56];
    z[55]=91*z[46] - z[55];
    z[55]=z[15]*z[55];
    z[55]=z[56] + z[55];
    z[56]= - z[46] + n<T>(1,2)*z[5];
    z[56]=z[56]*z[5];
    z[57]=n<T>(11,5)*z[46];
    z[58]= - z[57] + z[5];
    z[58]=8*z[58] + n<T>(299,5)*z[6];
    z[58]=z[6]*z[58];
    z[59]=n<T>(623,5)*z[46] + 193*z[6];
    z[59]=n<T>(1,2)*z[59] - n<T>(236,3)*z[3];
    z[59]=z[3]*z[59];
    z[58]=z[59] - 43*z[56] + z[58];
    z[58]=z[3]*z[58];
    z[59]= - z[3]*z[54];
    z[60]= - z[3] + n<T>(1,2)*z[10];
    z[60]=z[10]*z[60];
    z[56]=n<T>(139,270)*z[20] + z[60] - z[56] + z[59];
    z[56]=z[7]*z[56];
    z[48]= - z[4] + z[48] - z[2];
    z[48]=z[14]*z[48];
    z[49]= - z[2] + z[49];
    z[49]=z[17]*z[49];
    z[48]= - z[25] - z[48] - z[49] + z[35] + z[34];
    z[49]= - 62*z[46] + n<T>(121,3)*z[5];
    z[49]=z[49]*z[53];
    z[53]= - 62*z[5] + n<T>(251,9)*z[12];
    z[53]=z[12]*z[53];
    z[49]=n<T>(1,5)*z[53] + z[49] - n<T>(1,18)*z[20];
    z[49]=z[12]*z[49];
    z[53]=z[43] + z[42];
    z[59]= - n<T>(416,135)*z[26] - n<T>(208,135)*z[24] - n<T>(1081,540)*z[21];
    z[59]=i*z[59];
    z[46]= - n<T>(55,9)*z[6] + n<T>(79,60)*z[46] + z[5];
    z[46]=z[46]*npow(z[6],2);
    z[60]=n<T>(61,3)*z[5] + 331*z[6];
    z[60]=n<T>(7,27)*z[60] + 17*z[3];
    z[60]=n<T>(1,5)*z[60] - n<T>(119,162)*z[4];
    z[60]= - n<T>(179,54)*z[11] - n<T>(179,135)*z[10] + n<T>(1,2)*z[60] + n<T>(52,405)*
    z[2];
    z[60]=z[20]*z[60];
    z[57]=z[57] + 5*z[5];
    z[57]=4*z[57] - n<T>(541,5)*z[6];
    z[57]= - n<T>(16,5)*z[2] + n<T>(1,9)*z[57] - n<T>(104,5)*z[3];
    z[57]=z[19]*z[57];
    z[54]=z[10] - z[54];
    z[54]=z[16]*z[54];
    z[61]=n<T>(146,45)*z[11] + n<T>(107,15)*z[3] + z[10];
    z[61]=z[29]*z[61];
    z[62]=n<T>(4,5)*z[11] - n<T>(19,3)*z[12];
    z[62]=z[30]*z[62];
    z[63]=z[10] + z[7];
    z[63]=z[31]*z[63];
    z[64]= - z[6] - z[7];
    z[64]=z[32]*z[64];

    r += n<T>(47,9)*z[22] - n<T>(52,405)*z[23] + n<T>(208,135)*z[27] - n<T>(1994,135)*
      z[28] - n<T>(2893,180)*z[33] - n<T>(1693,180)*z[36] - n<T>(223,60)*z[37] + n<T>(13,9)*z[38]
     - n<T>(16,45)*z[39]
     - n<T>(9,5)*z[40]
     + n<T>(2,45)*z[41]
     + n<T>(19,5)*
      z[44] + z[45] + z[46] + z[47] - n<T>(104,135)*z[48] + n<T>(1,3)*z[49] + 
      z[50] + n<T>(2,15)*z[51] + n<T>(1,5)*z[52] - n<T>(24,5)*z[53] + n<T>(14,45)*z[54]
       + n<T>(1,15)*z[55] + z[56] + z[57] + n<T>(1,9)*z[58] + z[59] + z[60] + 
      z[61] + n<T>(2,3)*z[62] + z[63] + n<T>(43,5)*z[64];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf174(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf174(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
