#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf251(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[30];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[15];
    z[4]=d[4];
    z[5]=d[5];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=e[6];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[31];
    z[13]=d[3];
    z[14]=e[2];
    z[15]=e[3];
    z[16]=e[10];
    z[17]=d[6];
    z[18]=f[4];
    z[19]=f[12];
    z[20]=f[31];
    z[21]=f[36];
    z[22]=f[39];
    z[23]=i*z[1];
    z[24]=z[13] + z[23];
    z[24]=3*z[24] - 5*z[2];
    z[24]=z[2]*z[24];
    z[24]=z[24] - z[9];
    z[25]=z[3]*z[23];
    z[25]= - z[25] - 2*z[15] - z[14] + z[16];
    z[26]=npow(z[7],2);
    z[26]=3*z[26];
    z[24]= - z[26] + 3*z[25] + n<T>(1,2)*z[24];
    z[24]=z[2]*z[24];
    z[25]=z[23] - z[6];
    z[27]=n<T>(1,2)*z[5] + z[25];
    z[27]=z[5]*z[27];
    z[28]=npow(z[2],2);
    z[27]= - z[15] + z[27] + z[28];
    z[23]= - z[23] + n<T>(1,2)*z[6];
    z[28]=3*z[6];
    z[23]=z[23]*z[28];
    z[28]= - z[13] + z[6];
    z[28]=z[2] + 3*z[28] + z[5];
    z[28]=n<T>(3,2)*z[28] - 2*z[4];
    z[28]=z[4]*z[28];
    z[27]=z[28] - z[23] + n<T>(5,3)*z[9] - 4*z[8] + 3*z[27];
    z[27]=z[4]*z[27];
    z[28]=z[11] + 3*z[19] - z[20] + z[22] - z[21];
    z[29]= - z[8]*z[17];
    z[28]=z[29] + n<T>(1,4)*z[12] + z[18] + n<T>(1,2)*z[28];
    z[25]=z[5]*z[25];
    z[23]=n<T>(3,2)*z[25] - z[9] + z[23];
    z[23]=z[5]*z[23];
    z[25]=z[7]*z[13];
    z[25]=z[25] + z[16];
    z[25]= - z[9] + 3*z[25];
    z[25]=z[7]*z[25];
    z[26]= - z[8] - z[26];
    z[26]=z[1]*z[26];
    z[26]= - n<T>(7,12)*z[10] + z[26];
    z[26]=i*z[26];
    z[29]=z[8] + n<T>(1,3)*z[9];
    z[29]=z[6]*z[29];

    r += z[23] + z[24] + z[25] + z[26] + z[27] + 3*z[28] + z[29];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf251(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf251(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
