#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf915(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[32];
  std::complex<T> r(0,0);

    z[1]=c[32];
    z[2]=c[33];
    z[3]=c[35];
    z[4]=c[36];
    z[5]=c[37];
    z[6]=c[38];
    z[7]=c[39];
    z[8]=c[40];
    z[9]=c[43];
    z[10]=c[44];
    z[11]=c[47];
    z[12]=c[48];
    z[13]=c[50];
    z[14]=c[55];
    z[15]=c[57];
    z[16]=c[59];
    z[17]=c[81];
    z[18]=c[84];
    z[19]=d[2];
    z[20]=f[44];
    z[21]=f[62];
    z[22]=d[6];
    z[23]=d[7];
    z[24]=d[9];
    z[25]=g[90];
    z[26]=g[138];
    z[27]=g[218];
    z[28]=g[249];
    z[29]=z[24] + z[23] - z[19] - z[22];
    z[30]=z[21] + z[20];
    z[29]=z[30]*z[29];
    z[29]=n<T>(131,135)*z[1] + z[29];
    z[30]=z[12] - z[9];
    z[31]= - n<T>(4,3)*z[18] + n<T>(65,54)*z[17] + n<T>(1,18)*z[16] - n<T>(1,2)*z[15]
     + 
   4*z[13] - n<T>(1,6)*z[11] - n<T>(10,3)*z[10] + n<T>(109,486)*z[7] + n<T>(242,27)*
    z[6] - 8*z[5] - n<T>(5,27)*z[2];
    z[31]=i*z[31];

    r +=  - n<T>(24,5)*z[3] - 8*z[4] + n<T>(20,3)*z[8] + n<T>(1,10)*z[14] - z[25]
       + z[26] - z[27] + z[28] + n<T>(1,2)*z[29] - 2*z[30] + z[31];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf915(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf915(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
