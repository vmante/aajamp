#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf217(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[32];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[4];
    z[7]=d[6];
    z[8]=d[17];
    z[9]=e[2];
    z[10]=c[3];
    z[11]=c[11];
    z[12]=c[15];
    z[13]=c[31];
    z[14]=e[3];
    z[15]=e[10];
    z[16]=d[7];
    z[17]=e[6];
    z[18]=e[9];
    z[19]=f[4];
    z[20]=f[12];
    z[21]=f[16];
    z[22]=f[36];
    z[23]=n<T>(2,3)*z[4];
    z[24]=i*z[1];
    z[25]=2*z[24];
    z[26]=z[25] + 5*z[4];
    z[26]=z[26]*z[23];
    z[27]=z[6] + n<T>(1,3)*z[24];
    z[23]= - n<T>(2,3)*z[2] - n<T>(1,3)*z[3] + z[23] - z[27];
    z[23]=z[2]*z[23];
    z[28]=z[24]*z[5];
    z[29]=n<T>(1,3)*z[10];
    z[30]=n<T>(1,3)*z[9] + z[14] - n<T>(5,3)*z[15];
    z[31]=z[3] - z[24] - z[4];
    z[31]=z[3]*z[31];
    z[23]=z[23] + n<T>(4,3)*z[31] + z[26] + n<T>(2,3)*z[28] + 2*z[30] + z[29];
    z[23]=z[2]*z[23];
    z[26]=z[28] + z[9];
    z[26]= - z[29] - 4*z[26];
    z[28]=z[24] + z[6];
    z[28]=z[6]*z[28];
    z[27]=2*z[27] - n<T>(7,3)*z[4];
    z[27]=z[4]*z[27];
    z[30]= - z[6] - n<T>(1,3)*z[4];
    z[30]=z[3]*z[30];
    z[26]=z[30] + z[27] + n<T>(1,3)*z[26] + 2*z[28];
    z[26]=z[3]*z[26];
    z[27]=n<T>(1,2)*z[7];
    z[28]= - n<T>(1,3)*z[6] - z[27] - z[25];
    z[28]=z[6]*z[28];
    z[30]=n<T>(1,6)*z[10];
    z[31]= - z[24] + z[16];
    z[31]=z[7]*z[31];
    z[28]=z[28] + z[30] + z[31];
    z[28]=z[6]*z[28];
    z[29]= - z[15] + z[29];
    z[25]= - z[25] - z[6];
    z[25]=z[6]*z[25];
    z[24]=n<T>(7,3)*z[24] - z[6];
    z[24]=z[4]*z[24];
    z[24]=z[24] + n<T>(10,3)*z[29] + z[25];
    z[24]=z[4]*z[24];
    z[25]= - z[16] + z[27];
    z[25]=z[7]*z[25];
    z[25]=z[25] + z[30] + z[18] - z[17];
    z[25]=z[7]*z[25];
    z[27]=z[10]*z[5];
    z[29]=z[7]*z[8];
    z[29]=n<T>(4,3)*z[9] + z[29];
    z[29]=z[1]*z[29];
    z[29]=n<T>(5,36)*z[11] + z[29];
    z[29]=i*z[29];

    r +=  - n<T>(1,6)*z[12] - n<T>(15,4)*z[13] - n<T>(7,3)*z[19] - 2*z[20] - z[21]
       - n<T>(1,2)*z[22] + z[23] + z[24] + z[25] + z[26] - n<T>(4,3)*z[27] + 
      z[28] + z[29];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf217(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf217(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
