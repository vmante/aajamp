#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1049(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[20];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[7];
    z[5]=d[3];
    z[6]=e[0];
    z[7]=e[8];
    z[8]=c[3];
    z[9]=d[1];
    z[10]=d[5];
    z[11]=f[3];
    z[12]=f[11];
    z[13]=z[1]*i;
    z[14]= - z[9] - z[10] + 3*z[13];
    z[15]=n<T>(1,2)*z[2];
    z[16]=z[15] + z[14];
    z[16]=z[16]*z[2];
    z[14]= - z[5] - z[14];
    z[14]=z[14]*z[5];
    z[14]=z[14] + z[16];
    z[16]=n<T>(31,12)*z[8];
    z[17]=z[16] - 5*z[7];
    z[18]= - 2*z[2] + n<T>(5,2)*z[5];
    z[19]=n<T>(1,6)*z[4] - z[18];
    z[19]=z[4]*z[19];
    z[17]=z[19] + n<T>(1,2)*z[17] - z[14];
    z[17]=z[4]*z[17];
    z[16]= - z[16] + 5*z[6];
    z[18]= - n<T>(1,6)*z[3] + z[18];
    z[18]=z[3]*z[18];
    z[14]=z[18] + n<T>(1,2)*z[16] + z[14];
    z[14]=z[3]*z[14];
    z[13]=z[15] - z[13];
    z[15]=z[6] - z[7];
    z[13]=z[15]*z[13];
    z[15]= - z[12] + z[11];
    z[13]=n<T>(1,2)*z[15] + z[13];

    r += 5*z[13] + z[14] + z[17];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1049(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1049(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
