#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf292(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[37];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[8];
    z[6]=d[9];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[5];
    z[10]=d[4];
    z[11]=e[2];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[31];
    z[16]=e[3];
    z[17]=e[10];
    z[18]=e[14];
    z[19]=f[4];
    z[20]=f[12];
    z[21]=f[16];
    z[22]=f[51];
    z[23]=f[55];
    z[24]=f[58];
    z[25]=5*z[3];
    z[26]= - z[10] + n<T>(1,6)*z[3];
    z[26]=z[26]*z[25];
    z[27]=n<T>(5,3)*z[3];
    z[28]=z[8] + z[9];
    z[28]= - z[27] + 2*z[28] + 5*z[10];
    z[29]=i*z[1];
    z[28]=z[28]*z[29];
    z[27]= - n<T>(8,3)*z[29] + z[4] + z[27];
    z[27]=2*z[27] - n<T>(7,6)*z[2];
    z[27]=z[2]*z[27];
    z[30]=n<T>(5,2)*z[10];
    z[31]= - n<T>(4,3)*z[5] - n<T>(13,3)*z[2] - n<T>(17,6)*z[29] + n<T>(5,6)*z[3]
     + 
    z[30] + z[9] + 3*z[8] - z[4];
    z[31]=z[5]*z[31];
    z[32]=npow(z[10],2);
    z[33]=z[32] - z[18] + n<T>(5,3)*z[17];
    z[34]= - z[9] + 2*z[8];
    z[34]=z[34]*z[9];
    z[35]=npow(z[8],2);
    z[26]=z[31] + z[27] + z[28] + z[26] - z[34] - z[35] - n<T>(23,18)*z[12]
    + 2*z[33];
    z[26]=z[5]*z[26];
    z[27]=n<T>(10,3)*z[3] + z[4] + n<T>(1,3)*z[7] + z[6];
    z[27]=z[27]*z[29];
    z[28]=3*z[16];
    z[31]=n<T>(1,2)*z[4];
    z[33]= - z[6] - z[31];
    z[33]=z[4]*z[33];
    z[36]=npow(z[3],2);
    z[29]=z[29] + z[3];
    z[29]=n<T>(17,3)*z[2] + z[6] - n<T>(1,3)*z[29];
    z[29]=z[2]*z[29];
    z[27]=n<T>(1,2)*z[29] + z[27] - n<T>(10,3)*z[36] - 3*z[32] + z[33] + n<T>(1,3)*
    z[11] + z[28] + n<T>(4,3)*z[17];
    z[27]=z[2]*z[27];
    z[29]=n<T>(2,3)*z[7] - z[10];
    z[25]=z[29]*z[25];
    z[29]=z[4]*z[6];
    z[25]=z[25] - z[32] - z[34] - n<T>(10,3)*z[11] - z[29];
    z[25]=z[1]*z[25];
    z[25]= - n<T>(17,36)*z[13] + z[25];
    z[25]=i*z[25];
    z[33]=2*z[11] + n<T>(1,6)*z[12];
    z[30]=z[3]*z[30];
    z[30]=z[30] + n<T>(5,3)*z[33] + z[32];
    z[30]=z[3]*z[30];
    z[32]=n<T>(1,3)*z[12];
    z[29]=z[32] + z[29];
    z[29]=z[29]*z[31];
    z[31]= - z[9]*z[8];
    z[31]=z[31] - n<T>(2,3)*z[12] + z[35];
    z[31]=z[9]*z[31];
    z[33]=z[12]*z[7];
    z[32]=z[8]*z[32];
    z[34]= - n<T>(1,2)*z[12] - 2*z[18] + z[17];
    z[34]=z[6]*z[34];
    z[28]=z[28] - n<T>(8,3)*z[12];
    z[28]=z[10]*z[28];

    r +=  - n<T>(11,6)*z[14] + n<T>(43,4)*z[15] + n<T>(5,6)*z[19] - z[20] + 2*z[21]
       - z[22] - z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29]
       + z[30] + z[31] + z[32] + n<T>(10,3)*z[33] + z[34];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf292(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf292(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
