#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1187(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[69];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[18];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=d[9];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[7];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[11];
    z[32]=e[6];
    z[33]=f[3];
    z[34]=f[4];
    z[35]=f[6];
    z[36]=f[7];
    z[37]=f[11];
    z[38]=f[12];
    z[39]=f[13];
    z[40]=f[16];
    z[41]=f[17];
    z[42]=f[23];
    z[43]=f[30];
    z[44]=f[31];
    z[45]=f[33];
    z[46]=f[36];
    z[47]=f[39];
    z[48]=n<T>(1,2)*z[7];
    z[49]=z[1]*i;
    z[50]= - z[49] + z[48];
    z[50]=z[7]*z[50];
    z[51]=3*z[5];
    z[52]=2*z[49];
    z[53]= - z[52] + z[5];
    z[53]=z[53]*z[51];
    z[54]=n<T>(1,2)*z[6];
    z[55]=z[49] - z[7];
    z[56]= - z[54] - z[55];
    z[57]=n<T>(3,2)*z[6];
    z[56]=z[56]*z[57];
    z[58]=49*z[49] - 13*z[7];
    z[51]=n<T>(11,12)*z[11] - n<T>(3,4)*z[6] + n<T>(1,12)*z[58] - z[51];
    z[51]=z[11]*z[51];
    z[50]=z[51] + z[56] + n<T>(1,3)*z[50] + z[53];
    z[50]=z[11]*z[50];
    z[51]=n<T>(1,2)*z[5];
    z[53]= - z[49] + z[51];
    z[53]=z[5]*z[53];
    z[56]=3*z[11];
    z[58]=z[49] - z[5];
    z[59]=2*z[58] + z[11];
    z[59]=z[59]*z[56];
    z[60]= - n<T>(55,3)*z[3] - z[2] + z[7] - n<T>(37,3)*z[58];
    z[56]= - n<T>(1,3)*z[9] + n<T>(1,2)*z[60] + z[56];
    z[56]=z[9]*z[56];
    z[60]=z[49]*z[7];
    z[61]= - n<T>(1,2)*z[3] - z[58];
    z[61]=z[3]*z[61];
    z[53]=z[56] + z[59] + n<T>(17,3)*z[61] - z[60] + n<T>(1,3)*z[53];
    z[53]=z[9]*z[53];
    z[56]=n<T>(1,2)*z[2];
    z[57]=z[57] - z[56] + z[51] - z[55];
    z[54]=z[57]*z[54];
    z[57]=z[56] - z[49];
    z[59]=n<T>(3,2)*z[7];
    z[61]= - z[51] - z[59] + z[57];
    z[61]=z[61]*z[56];
    z[59]=z[49] + z[59];
    z[51]=z[59]*z[51];
    z[59]=z[52] - z[7];
    z[59]=z[59]*z[7];
    z[51]=z[54] + z[61] + z[59] + z[51];
    z[51]=z[6]*z[51];
    z[54]=n<T>(7,4)*z[7];
    z[61]= - n<T>(19,6)*z[5] + n<T>(23,3)*z[49] - z[54];
    z[61]=z[5]*z[61];
    z[52]=n<T>(5,4)*z[2] - n<T>(9,4)*z[5] - z[52] + z[54];
    z[52]=z[2]*z[52];
    z[54]=3*z[7];
    z[62]= - z[2] - n<T>(5,3)*z[5] - n<T>(17,3)*z[49] + z[54];
    z[62]=n<T>(1,2)*z[62] + 5*z[3];
    z[62]=z[3]*z[62];
    z[52]=z[62] + z[52] + z[59] + z[61];
    z[52]=z[3]*z[52];
    z[59]=npow(z[6],2);
    z[61]=z[49] + z[7];
    z[61]=n<T>(1,6)*z[12] + n<T>(5,6)*z[61] - z[6];
    z[61]=z[12]*z[61];
    z[59]=z[59] - z[61];
    z[61]= - 11*z[49] + 5*z[7];
    z[61]=z[7]*z[61];
    z[62]=z[7] - z[56];
    z[62]=z[2]*z[62];
    z[63]=n<T>(1,2)*z[11] + z[55];
    z[63]=z[11]*z[63];
    z[59]=n<T>(11,6)*z[63] + n<T>(1,6)*z[61] + z[62] - n<T>(1,2)*z[59];
    z[59]=z[12]*z[59];
    z[61]=21*z[49] - n<T>(23,2)*z[7];
    z[61]=z[61]*z[48];
    z[54]=n<T>(187,3)*z[2] - n<T>(205,9)*z[5] - n<T>(1075,9)*z[49] - z[54];
    z[54]=z[54]*z[56];
    z[62]=43*z[49] - 35*z[5];
    z[62]=z[5]*z[62];
    z[54]=z[54] + z[61] + n<T>(5,9)*z[62];
    z[54]=z[54]*z[56];
    z[56]=29*z[49] - n<T>(35,2)*z[7];
    z[56]=z[7]*z[56];
    z[61]=n<T>(227,2)*z[7] + z[5];
    z[61]=z[5]*z[61];
    z[56]=z[56] + n<T>(1,4)*z[61];
    z[56]=z[5]*z[56];
    z[61]=npow(z[7],3);
    z[56]=n<T>(1,4)*z[61] + z[56];
    z[54]=z[42] + n<T>(1,3)*z[56] + z[54];
    z[56]= - 155*z[49] + n<T>(203,2)*z[7];
    z[56]=z[56]*z[48];
    z[61]= - n<T>(19,3)*z[49] - n<T>(7,2)*z[7];
    z[61]=29*z[61] + n<T>(1,12)*z[5];
    z[61]=z[5]*z[61];
    z[56]=z[56] + z[61];
    z[61]= - 47*z[2] + n<T>(1639,8)*z[49] + 85*z[5];
    z[61]=z[2]*z[61];
    z[56]=n<T>(1,4)*z[56] + n<T>(1,3)*z[61];
    z[61]=z[2] - z[5];
    z[62]=z[6]*z[61];
    z[63]= - n<T>(1567,6)*z[2] + n<T>(179,2)*z[7] + n<T>(515,3)*z[5];
    z[63]=z[4]*z[63];
    z[56]=n<T>(1,24)*z[63] + n<T>(1,3)*z[56] + z[62];
    z[56]=z[4]*z[56];
    z[62]=z[49]*z[8];
    z[62]=z[62] - z[31];
    z[63]= - z[2] + z[4] - z[9];
    z[63]= - z[63]*z[62];
    z[64]= - z[12]*z[49];
    z[64]= - z[22] - z[60] + z[64];
    z[64]=z[13]*z[64];
    z[65]= - z[12] + z[55];
    z[65]=z[20]*z[65];
    z[64]=z[64] + z[65];
    z[65]=z[5] + z[3];
    z[65]=z[49]*z[65];
    z[65]=z[22] + z[65];
    z[65]=z[10]*z[65];
    z[58]=z[3] - z[58];
    z[58]=z[17]*z[58];
    z[58]=z[65] + z[58];
    z[55]=z[55] + z[61];
    z[61]=n<T>(19,3)*z[9] + n<T>(16,3)*z[3] - z[55];
    z[61]=z[21]*z[61];
    z[61]=z[61] - z[45];
    z[55]=3*z[6] - z[55];
    z[55]=n<T>(1,2)*z[55] + z[12];
    z[55]=z[18]*z[55];
    z[60]= - z[60] + z[62];
    z[60]=z[14]*z[60];
    z[62]=n<T>(1,2)*z[4] + z[57];
    z[62]=z[15]*z[62];
    z[48]=z[48] + z[57];
    z[48]=z[19]*z[48];
    z[57]=z[39] - z[27];
    z[65]=z[40] + z[38];
    z[66]=z[11] + z[12];
    z[66]=z[32]*z[66];
    z[66]=z[66] - z[43];
    z[67]=n<T>(1147,36)*z[28] + n<T>(1147,72)*z[26] + n<T>(4,9)*z[23];
    z[67]=i*z[67];
    z[68]=n<T>(647,8)*z[7] - n<T>(203,3)*z[5];
    z[68]=n<T>(1,8)*z[68] - n<T>(29,3)*z[2];
    z[68]=n<T>(1,3)*z[68] + n<T>(23,8)*z[6];
    z[68]= - n<T>(5,12)*z[12] - n<T>(3577,1728)*z[4] - n<T>(83,36)*z[9] - n<T>(65,36)*
    z[11] + n<T>(1,3)*z[68] + n<T>(13,8)*z[3];
    z[68]=z[22]*z[68];
    z[49]=n<T>(269,2)*z[2] - 305*z[49] + n<T>(341,2)*z[5];
    z[49]=z[16]*z[49];

    r +=  - n<T>(17,6)*z[24] + n<T>(1147,864)*z[25] - n<T>(1147,72)*z[29] + n<T>(359,72)
      *z[30] + n<T>(515,72)*z[33] + n<T>(17,3)*z[34] - z[35] + 8*z[36] - n<T>(35,12)
      *z[37] + n<T>(179,48)*z[41] + n<T>(1,4)*z[44] + n<T>(13,12)*z[46] - n<T>(3,4)*
      z[47] + n<T>(13,8)*z[48] + n<T>(1,36)*z[49] + z[50] + z[51] + z[52] + 
      z[53] + n<T>(1,2)*z[54] + z[55] + z[56] + n<T>(1147,144)*z[57] + n<T>(8,3)*
      z[58] + z[59] + z[60] + 2*z[61] + n<T>(1567,72)*z[62] + z[63] + n<T>(5,6)
      *z[64] + 3*z[65] + n<T>(11,6)*z[66] + z[67] + z[68];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1187(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1187(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
