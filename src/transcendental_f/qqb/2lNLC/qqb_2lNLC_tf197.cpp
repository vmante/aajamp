#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf197(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[41];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[2];
    z[7]=d[5];
    z[8]=d[11];
    z[9]=d[4];
    z[10]=e[2];
    z[11]=e[12];
    z[12]=c[3];
    z[13]=d[9];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[31];
    z[17]=e[3];
    z[18]=e[10];
    z[19]=e[14];
    z[20]=f[4];
    z[21]=f[12];
    z[22]=f[16];
    z[23]=f[51];
    z[24]=f[55];
    z[25]=f[58];
    z[26]=n<T>(11,2)*z[2];
    z[27]=z[1]*i;
    z[28]=z[27] + z[4];
    z[29]= - 4*z[28] - z[26];
    z[29]=z[2]*z[29];
    z[30]=z[27]*z[4];
    z[31]=2*z[30];
    z[32]=npow(z[4],2);
    z[33]=4*z[2];
    z[34]= - z[4] + z[33];
    z[34]=z[3]*z[34];
    z[29]=z[34] + z[29] + n<T>(31,2)*z[32] + z[31];
    z[29]=z[3]*z[29];
    z[26]=z[26] - 10*z[4] - n<T>(11,2)*z[27];
    z[26]=z[2]*z[26];
    z[34]=n<T>(1,2)*z[32];
    z[26]=z[26] - z[34] + 4*z[30];
    z[26]=z[2]*z[26];
    z[35]=z[32]*z[27];
    z[36]=4*z[3];
    z[37]= - z[36] + 11*z[2];
    z[38]=z[27]*z[37];
    z[38]= - 4*z[12] + z[38];
    z[38]=z[5]*z[38];
    z[39]=4*z[27];
    z[37]=z[39] + z[37];
    z[37]=z[10]*z[37];
    z[40]=npow(z[4],3);
    z[26]=z[37] + z[38] + z[29] + z[26] + 5*z[40] - n<T>(7,2)*z[35];
    z[29]=5*z[2];
    z[35]=z[29] - z[4] + z[39] - z[36];
    z[35]=z[9]*z[35];
    z[36]=2*z[28] - z[3];
    z[36]=z[3]*z[36];
    z[31]=z[35] + z[36] - z[32] - z[31];
    z[31]=z[9]*z[31];
    z[35]=n<T>(1,2)*z[6];
    z[36]= - z[35] + z[28];
    z[36]=z[6]*z[36];
    z[28]=z[6] - z[28];
    z[28]=z[7]*z[28];
    z[28]=n<T>(1,2)*z[28] + z[36] - z[34] - z[30];
    z[28]=z[7]*z[28];
    z[34]=z[27] - z[6];
    z[36]=z[4] - z[34];
    z[36]=z[36]*z[35];
    z[30]=z[36] - 4*z[32] - z[30];
    z[30]=z[6]*z[30];
    z[29]= - n<T>(13,3)*z[4] + z[29];
    z[29]=n<T>(1,2)*z[29] - n<T>(1,3)*z[3];
    z[29]=n<T>(1,6)*z[13] + z[7] + n<T>(7,3)*z[9] + n<T>(1,3)*z[29] - z[35];
    z[29]=z[12]*z[29];
    z[32]=z[25] - z[23];
    z[27]= - z[6]*z[27];
    z[27]=z[27] - z[12];
    z[27]=z[8]*z[27];
    z[34]= - z[11]*z[34];
    z[35]= - z[2] - z[9];
    z[35]=z[17]*z[35];
    z[33]=z[4] + z[33];
    z[33]=z[18]*z[33];
    z[36]=3*z[13] + 4*z[4];
    z[36]=z[19]*z[36];
    z[37]=z[14]*i;

    r +=  - n<T>(2,3)*z[15] - 6*z[16] + n<T>(31,6)*z[20] + 4*z[21] - z[22] + 2*
      z[24] + n<T>(1,3)*z[26] + z[27] + 3*z[28] + z[29] + z[30] + z[31] - n<T>(3,2)*z[32]
     + n<T>(5,3)*z[33]
     + z[34]
     + 5*z[35]
     + z[36]
     + n<T>(13,18)*z[37]
      ;
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf197(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf197(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
