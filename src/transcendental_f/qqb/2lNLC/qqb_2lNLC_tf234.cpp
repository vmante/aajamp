#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf234(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[24];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[4];
    z[7]=e[2];
    z[8]=e[10];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[31];
    z[13]=e[3];
    z[14]=f[4];
    z[15]=f[12];
    z[16]=f[16];
    z[17]=z[3] - z[2];
    z[18]=2*z[4];
    z[19]= - z[17]*z[18];
    z[20]=npow(z[2],2);
    z[21]=n<T>(1,2)*z[20];
    z[22]= - z[3] + 2*z[2];
    z[23]= - z[3]*z[22];
    z[19]=z[19] - z[21] + z[23];
    z[19]=z[4]*z[19];
    z[17]=z[4] - z[17];
    z[17]=z[17]*z[18];
    z[18]=z[3]*z[2];
    z[17]=z[17] - z[21] + z[18];
    z[21]=z[1]*i;
    z[17]=z[17]*z[21];
    z[18]=n<T>(5,2)*z[20] - z[18];
    z[18]=z[3]*z[18];
    z[20]= - z[22]*z[21];
    z[20]=z[20] + z[9];
    z[20]=z[5]*z[20];
    z[22]= - z[21] - z[22];
    z[22]=z[7]*z[22];
    z[17]=z[22] + z[20] + z[17] + z[18] + z[19];
    z[18]=z[4] + z[2];
    z[19]=z[21] - z[3];
    z[18]= - z[19] - n<T>(2,3)*z[18];
    z[18]=z[8]*z[18];
    z[20]=z[16] - z[15];
    z[21]= - n<T>(5,3)*z[4] - z[2] - n<T>(1,3)*z[3];
    z[21]=z[9]*z[21];
    z[21]=z[21] + z[11];
    z[19]=z[19] - z[4];
    z[19]= - z[2] - n<T>(1,2)*z[19];
    z[19]=z[19]*npow(z[6],2);
    z[22]=z[2] + z[6];
    z[22]=z[13]*z[22];
    z[23]=z[10]*i;

    r +=  - n<T>(5,4)*z[12] - n<T>(2,3)*z[14] + n<T>(1,3)*z[17] + z[18] + z[19] + n<T>(1,2)*z[20]
     + n<T>(1,6)*z[21]
     + z[22]
     + n<T>(1,36)*z[23];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf234(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf234(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
