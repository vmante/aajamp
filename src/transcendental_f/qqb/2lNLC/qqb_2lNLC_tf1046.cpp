#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1046(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[10];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[16];
    z[4]=d[2];
    z[5]=d[9];
    z[6]=e[4];
    z[7]= - z[4]*z[5];
    z[8]=z[1]*i;
    z[9]= - z[3]*z[8];
    z[8]= - z[2] + z[8] + z[5] + z[4];
    z[8]=z[2]*z[8];

    r += z[6] + z[7] + z[8] + z[9];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1046(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1046(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
