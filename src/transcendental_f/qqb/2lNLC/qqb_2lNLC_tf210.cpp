#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf210(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[36];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[7];
    z[9]=e[2];
    z[10]=e[6];
    z[11]=c[3];
    z[12]=d[2];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[31];
    z[16]=e[3];
    z[17]=e[10];
    z[18]=d[6];
    z[19]=f[4];
    z[20]=f[12];
    z[21]=f[16];
    z[22]=f[31];
    z[23]=f[36];
    z[24]=f[39];
    z[25]= - z[8] + n<T>(1,2)*z[7];
    z[26]=3*z[7];
    z[25]=z[25]*z[26];
    z[26]=z[3] - z[4];
    z[27]=z[8] + z[7];
    z[27]=3*z[27] + z[26];
    z[28]=i*z[1];
    z[27]=z[27]*z[28];
    z[29]=z[2] + 3*z[8] + z[7];
    z[29]= - 7*z[3] - z[4] + 3*z[29];
    z[28]= - n<T>(13,6)*z[6] + n<T>(1,2)*z[29] - z[28];
    z[28]=z[6]*z[28];
    z[29]=npow(z[8],2);
    z[29]=n<T>(3,2)*z[29];
    z[30]=npow(z[4],2);
    z[31]=n<T>(1,2)*z[3];
    z[32]=z[4] - z[31];
    z[32]=z[3]*z[32];
    z[33]=npow(z[2],2);
    z[27]=z[28] + z[27] + n<T>(5,2)*z[33] + z[32] - n<T>(1,2)*z[30] + z[25] - 
    z[29] + n<T>(3,2)*z[11] - 3*z[16] - 4*z[10];
    z[27]=z[6]*z[27];
    z[28]=z[30] - z[17];
    z[32]=n<T>(1,3)*z[11];
    z[33]=z[3]*z[26];
    z[34]= - n<T>(17,2)*z[2] + z[4] + 4*z[3];
    z[34]=z[2]*z[34];
    z[28]=n<T>(1,3)*z[34] + n<T>(2,3)*z[33] - z[32] - n<T>(8,3)*z[9] - 5*z[16] - n<T>(4,3)*z[28];
    z[28]=z[2]*z[28];
    z[33]=n<T>(1,3)*z[3];
    z[34]=2*z[5];
    z[35]= - z[34] + z[4];
    z[35]=z[35]*z[33];
    z[26]=2*z[2] - 4*z[5] - z[26];
    z[26]=z[2]*z[26];
    z[26]=z[26] + z[9];
    z[25]=z[35] - n<T>(11,6)*z[30] + z[25] - z[10] + n<T>(2,3)*z[26];
    z[25]=z[1]*z[25];
    z[25]= - n<T>(8,9)*z[13] + z[25];
    z[25]=i*z[25];
    z[26]= - z[22] + z[24] - z[23];
    z[35]=z[8]*z[11];
    z[26]=z[35] - 9*z[15] + n<T>(11,3)*z[19] + 7*z[20] + 3*z[26] - z[21];
    z[34]=n<T>(1,2)*z[12] - z[34];
    z[34]=z[34]*z[32];
    z[35]=z[7]*z[8];
    z[29]= - n<T>(3,2)*z[35] - z[11] + z[29];
    z[29]=z[7]*z[29];
    z[31]= - z[4]*z[31];
    z[30]=z[31] + n<T>(11,2)*z[30] - 2*z[9] - n<T>(1,6)*z[11];
    z[30]=z[30]*z[33];
    z[31]= - 3*z[18] + z[12];
    z[31]=z[10]*z[31];
    z[32]=z[17] - z[32];
    z[32]=z[4]*z[32];

    r += n<T>(5,3)*z[14] + z[25] + n<T>(1,2)*z[26] + z[27] + z[28] + z[29] + 
      z[30] + z[31] + n<T>(4,3)*z[32] + z[34];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf210(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf210(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
