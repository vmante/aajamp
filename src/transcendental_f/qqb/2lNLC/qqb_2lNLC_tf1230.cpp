#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1230(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[130];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[42];
    z[3]=f[77];
    z[4]=f[79];
    z[5]=f[80];
    z[6]=f[81];
    z[7]=c[11];
    z[8]=d[0];
    z[9]=d[1];
    z[10]=d[2];
    z[11]=d[4];
    z[12]=d[5];
    z[13]=d[6];
    z[14]=d[7];
    z[15]=d[8];
    z[16]=d[9];
    z[17]=c[12];
    z[18]=c[13];
    z[19]=c[20];
    z[20]=c[23];
    z[21]=c[32];
    z[22]=c[33];
    z[23]=c[35];
    z[24]=c[36];
    z[25]=c[37];
    z[26]=c[38];
    z[27]=c[39];
    z[28]=c[40];
    z[29]=c[43];
    z[30]=c[44];
    z[31]=c[47];
    z[32]=c[48];
    z[33]=c[49];
    z[34]=c[50];
    z[35]=c[55];
    z[36]=c[56];
    z[37]=c[57];
    z[38]=c[59];
    z[39]=c[81];
    z[40]=c[84];
    z[41]=f[22];
    z[42]=f[24];
    z[43]=f[44];
    z[44]=f[61];
    z[45]=f[62];
    z[46]=f[69];
    z[47]=f[70];
    z[48]=d[3];
    z[49]=g[24];
    z[50]=g[27];
    z[51]=g[38];
    z[52]=g[40];
    z[53]=g[44];
    z[54]=g[48];
    z[55]=g[50];
    z[56]=g[79];
    z[57]=g[82];
    z[58]=g[90];
    z[59]=g[92];
    z[60]=g[99];
    z[61]=g[101];
    z[62]=g[110];
    z[63]=g[112];
    z[64]=g[113];
    z[65]=g[128];
    z[66]=g[131];
    z[67]=g[138];
    z[68]=g[140];
    z[69]=g[145];
    z[70]=g[147];
    z[71]=g[170];
    z[72]=g[173];
    z[73]=g[181];
    z[74]=g[183];
    z[75]=g[189];
    z[76]=g[191];
    z[77]=g[198];
    z[78]=g[200];
    z[79]=g[201];
    z[80]=g[212];
    z[81]=g[214];
    z[82]=g[218];
    z[83]=g[220];
    z[84]=g[224];
    z[85]=g[232];
    z[86]=g[233];
    z[87]=g[234];
    z[88]=g[243];
    z[89]=g[245];
    z[90]=g[249];
    z[91]=g[251];
    z[92]=g[254];
    z[93]=g[270];
    z[94]=g[272];
    z[95]=g[276];
    z[96]=g[278];
    z[97]=g[281];
    z[98]=g[288];
    z[99]=g[289];
    z[100]=g[290];
    z[101]=g[292];
    z[102]=g[294];
    z[103]=g[298];
    z[104]=g[301];
    z[105]=g[302];
    z[106]=g[334];
    z[107]=g[349];
    z[108]=g[352];
    z[109]=g[357];
    z[110]=g[358];
    z[111]=g[378];
    z[112]=g[384];
    z[113]= - z[19] + n<T>(1,20)*z[20];
    z[114]= - n<T>(3,5)*z[18] + n<T>(1,4)*z[113];
    z[115]= - 33*z[114] - n<T>(589,6480)*z[7];
    z[115]=z[16]*z[115];
    z[116]= - n<T>(1,5)*z[18] + n<T>(1,12)*z[113];
    z[117]= - 41*z[116] + n<T>(89,6480)*z[7];
    z[117]=z[9]*z[117];
    z[118]=37*z[116] + n<T>(707,6480)*z[7];
    z[118]=z[13]*z[118];
    z[119]=17*z[114] - n<T>(91,720)*z[7];
    z[119]=z[10]*z[119];
    z[114]=n<T>(1307,6480)*z[7] - z[114];
    z[114]=z[8]*z[114];
    z[114]=z[115] + z[117] + z[118] + z[119] + z[114] - z[37];
    z[115]=n<T>(1,6)*z[3];
    z[117]=z[2] - n<T>(7,8)*z[4] - n<T>(1,8)*z[5] - z[115] - n<T>(1,12)*z[6];
    z[117]=z[1]*z[117];
    z[118]=n<T>(161,480)*z[38] - 4*z[40] + n<T>(65,18)*z[39];
    z[116]= - n<T>(1,720)*z[7] + z[116];
    z[116]=z[11]*z[116];
    z[119]=z[14]*z[7];
    z[120]=n<T>(1,2)*z[113] - n<T>(6,5)*z[18];
    z[120]=3*z[120] + n<T>(83,1080)*z[7];
    z[120]=z[15]*z[120];
    z[113]= - n<T>(221,3240)*z[7] - n<T>(1,6)*z[113] + n<T>(2,5)*z[18];
    z[113]=z[12]*z[113];
    z[113]=z[113] + z[120] - n<T>(5,108)*z[119] + n<T>(23,2)*z[116] - n<T>(7,54)*
    z[22] - 8*z[25] + n<T>(107,27)*z[26] + n<T>(109,486)*z[27] - n<T>(10,3)*z[30]
     - 
   n<T>(2723,12960)*z[31] - n<T>(27,10)*z[33] + 4*z[34] - n<T>(9,8)*z[36] + n<T>(1,3)*
    z[118] + z[117] + n<T>(1,2)*z[114];
    z[113]=i*z[113];
    z[114]=5*z[47];
    z[116]=n<T>(31,12)*z[43];
    z[117]= - n<T>(9,4)*z[42] + z[116] - n<T>(59,12)*z[41] + z[114] + n<T>(13,6)*
    z[46];
    z[118]=z[2] - z[45];
    z[119]=n<T>(1,8)*z[44];
    z[115]=n<T>(13,24)*z[4] - z[119] - n<T>(35,24)*z[5] + n<T>(61,18)*z[17] - z[115]
    + n<T>(1,2)*z[117] + z[6] + n<T>(61,24)*z[118];
    z[115]=z[12]*z[115];
    z[117]= - z[106] - z[98] - z[112] - z[111];
    z[118]=z[109] + z[99] + z[104];
    z[120]=z[103] + z[108];
    z[121]=z[97] - z[102];
    z[122]=n<T>(1,2)*z[47];
    z[123]= - n<T>(1,2)*z[42] - z[122] + 11*z[43];
    z[123]=n<T>(1,3)*z[123] - n<T>(7,2)*z[45];
    z[123]=n<T>(11,12)*z[4] - n<T>(7,8)*z[44] + n<T>(1,4)*z[123] + n<T>(23,9)*z[17];
    z[123]=z[11]*z[123];
    z[115]=z[123] - n<T>(13,6)*z[95] - n<T>(11,3)*z[96] - z[100] - n<T>(7,6)*z[101]
    - z[105] + n<T>(1,24)*z[107] + z[110] + n<T>(11,12)*z[121] + n<T>(3,8)*z[120]
    - n<T>(1,2)*z[118] + n<T>(1,4)*z[117] + z[115] - z[64] + z[79] + z[87];
    z[117]=n<T>(9,2)*z[41];
    z[118]= - z[122] - 13*z[46];
    z[118]=n<T>(1,3)*z[118] + z[117];
    z[120]=z[4] + z[17];
    z[121]=n<T>(5,16)*z[2];
    z[123]=n<T>(1,24)*z[6];
    z[124]=n<T>(1,12)*z[3];
    z[118]= - z[121] - n<T>(5,16)*z[44] + n<T>(1,16)*z[5] - n<T>(7,16)*z[45] + n<T>(1,8)
   *z[118] - z[43] + z[123] + z[124] + n<T>(5,4)*z[120];
    z[118]=z[14]*z[118];
    z[120]=z[123] + n<T>(1,24)*z[5];
    z[123]=n<T>(1,16)*z[3];
    z[125]=n<T>(3,16)*z[44];
    z[126]=2*z[41];
    z[127]= - n<T>(11,8)*z[43] - n<T>(17,8)*z[46] + z[126];
    z[127]= - n<T>(55,48)*z[2] + n<T>(1,48)*z[4] + z[125] - n<T>(3,4)*z[17] + n<T>(7,6)*
    z[45] - z[123] + n<T>(1,3)*z[127] - n<T>(3,8)*z[42] - z[120];
    z[127]=z[15]*z[127];
    z[119]= - n<T>(1,8)*z[2] + n<T>(1,12)*z[4] - z[119] + z[124] + z[120];
    z[119]=z[48]*z[119];
    z[120]=z[122] + 17*z[46];
    z[120]= - n<T>(61,12)*z[17] - n<T>(35,16)*z[45] - z[123] + n<T>(19,16)*z[42]
     + n<T>(1,8)*z[120] - z[126];
    z[122]=z[2] - z[4];
    z[120]=n<T>(1,3)*z[120] + n<T>(3,4)*z[44] + n<T>(61,48)*z[122];
    z[120]=z[9]*z[120];
    z[122]=z[4] - z[45];
    z[123]=n<T>(2,3)*z[5];
    z[122]= - n<T>(23,24)*z[2] + z[125] + z[123] - n<T>(1,9)*z[17] - n<T>(13,24)*
    z[6] + n<T>(7,12)*z[42] - n<T>(5,48)*z[43] - n<T>(5,4)*z[47] + n<T>(2,3)*z[41]
     - n<T>(17,48)*z[122];
    z[122]=z[13]*z[122];
    z[124]=z[124] + n<T>(13,6)*z[6] + n<T>(47,6)*z[42];
    z[114]=n<T>(43,3)*z[17] - n<T>(25,4)*z[45] + n<T>(79,12)*z[43] - z[114] - n<T>(17,6)
   *z[46] + z[124];
    z[114]= - z[121] - n<T>(7,24)*z[4] - n<T>(1,2)*z[44] + n<T>(1,4)*z[114] - z[123]
   ;
    z[114]=z[10]*z[114];
    z[121]=z[43] + z[46];
    z[117]= - n<T>(1,6)*z[42] + z[117] - n<T>(13,3)*z[121];
    z[117]=n<T>(9,4)*z[2] - n<T>(5,2)*z[44] - n<T>(1,12)*z[5] - n<T>(29,3)*z[17]
     + n<T>(1,2)
   *z[117] + n<T>(11,3)*z[45];
    z[117]=z[16]*z[117];
    z[117]=z[117] - z[63] + z[78] + z[86];
    z[116]= - z[2] - z[116] - n<T>(9,4)*z[41] + n<T>(61,12)*z[47] + 5*z[46] - 
    z[124];
    z[116]= - n<T>(1,6)*z[4] + n<T>(15,16)*z[44] + n<T>(11,16)*z[5] - n<T>(17,6)*z[17]
    + 2*z[45] + n<T>(1,4)*z[116];
    z[116]=z[8]*z[116];
    z[121]= - z[85] + z[62] - z[77];
    z[123]=z[83] + z[94];
    z[124]=z[72] + z[73];
    z[125]=z[71] + z[74];
    z[126]=z[58] - z[88];
    z[128]=z[49] + z[52];
    z[129]=z[24] - z[66];

    r +=  - n<T>(3,10)*z[21] + n<T>(24,5)*z[23] - n<T>(10,3)*z[28] - z[29] + n<T>(25,6)
      *z[32] - n<T>(1,10)*z[35] + n<T>(19,6)*z[50] + n<T>(35,12)*z[51] - n<T>(1,48)*
      z[53] - n<T>(59,72)*z[54] + n<T>(17,24)*z[55] + n<T>(37,24)*z[56] + n<T>(5,3)*
      z[57] + n<T>(43,24)*z[59] - n<T>(8,9)*z[60] + n<T>(71,72)*z[61] + n<T>(5,4)*z[65]
       + n<T>(11,12)*z[67] + n<T>(3,2)*z[68] - n<T>(5,24)*z[69] - n<T>(11,36)*z[70]
     + n<T>(5,6)*z[75] - n<T>(79,72)*z[76]
     + n<T>(11,24)*z[80]
     + n<T>(13,6)*z[81] - n<T>(11,4)
      *z[82] - n<T>(1,24)*z[84] - n<T>(31,24)*z[89] - 2*z[90] - n<T>(13,12)*z[91]
       + n<T>(13,24)*z[92] - n<T>(41,24)*z[93] + z[113] + z[114] + n<T>(1,2)*z[115]
       + z[116] + n<T>(1,4)*z[117] + z[118] + z[119] + z[120] - n<T>(1,8)*
      z[121] + z[122] + n<T>(1,3)*z[123] - n<T>(23,8)*z[124] - n<T>(25,24)*z[125]
       + n<T>(19,8)*z[126] + z[127] + n<T>(4,3)*z[128] + 4*z[129];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1230(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1230(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
