#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf113(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[72];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[9];
    z[10]=d[15];
    z[11]=d[16];
    z[12]=d[4];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[3];
    z[33]=e[6];
    z[34]=e[13];
    z[35]=e[14];
    z[36]=f[0];
    z[37]=f[1];
    z[38]=f[3];
    z[39]=f[4];
    z[40]=f[5];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[16];
    z[44]=f[18];
    z[45]=f[23];
    z[46]=f[31];
    z[47]=f[36];
    z[48]=f[39];
    z[49]=f[51];
    z[50]=f[55];
    z[51]=f[58];
    z[52]=npow(z[13],2);
    z[53]=3*z[9];
    z[54]= - z[53] - n<T>(487,60)*z[3];
    z[54]=z[3]*z[54];
    z[54]=z[52] + z[54];
    z[55]=n<T>(1313,20)*z[12];
    z[53]= - n<T>(137,45)*z[7] - n<T>(519,20)*z[8] - n<T>(319,45)*z[2] + n<T>(473,5)*
    z[3] - z[53] - z[55];
    z[53]=n<T>(1,4)*z[53] + n<T>(38,45)*z[5];
    z[53]=z[5]*z[53];
    z[56]=npow(z[6],2);
    z[57]=n<T>(1,2)*z[56];
    z[58]=npow(z[4],2);
    z[59]=n<T>(1,4)*z[58];
    z[60]=npow(z[9],2);
    z[61]=n<T>(3,4)*z[60];
    z[62]= - 126*z[17] + n<T>(319,36)*z[16];
    z[63]= - z[13] + n<T>(11947,240)*z[12];
    z[63]=z[12]*z[63];
    z[64]= - n<T>(319,360)*z[2] + n<T>(13,10)*z[3] + n<T>(1,2)*z[9] - z[6];
    z[64]=z[2]*z[64];
    z[65]= - n<T>(14657,12)*z[8] - 11*z[2] + n<T>(1313,2)*z[12] - 337*z[3];
    z[65]=z[8]*z[65];
    z[66]=n<T>(91,45)*z[7] + n<T>(11,20)*z[8] + n<T>(137,90)*z[2] + z[9] - n<T>(31,20)*
    z[3];
    z[66]=z[7]*z[66];
    z[53]=z[53] + z[66] + n<T>(1,20)*z[65] + z[64] + z[63] + z[57] - z[59]
    + z[61] + n<T>(1,5)*z[62] - n<T>(407,432)*z[23] + n<T>(1,2)*z[54];
    z[53]=z[5]*z[53];
    z[54]=n<T>(126,5)*z[10];
    z[62]=n<T>(683,180)*z[2];
    z[63]=n<T>(1,2)*z[4];
    z[64]= - n<T>(182,45)*z[7] + n<T>(519,40)*z[8] + z[62] - n<T>(437,20)*z[3] + 
   n<T>(1313,40)*z[12] + z[6] - z[54] + z[63];
    z[64]=z[5]*z[64];
    z[65]=z[11] + z[63];
    z[65]=z[65]*z[63];
    z[66]= - z[9] - z[4];
    z[62]= - z[62] - n<T>(104,15)*z[3] + n<T>(1,2)*z[66] + z[6];
    z[62]=z[2]*z[62];
    z[66]=z[13]*z[14];
    z[67]= - n<T>(223,10)*z[4] + 6*z[6];
    z[67]=z[6]*z[67];
    z[68]= - z[13] + n<T>(457,30)*z[6];
    z[69]= - n<T>(9211,240)*z[12] - z[68];
    z[69]=z[12]*z[69];
    z[70]= - 71*z[10] + n<T>(451,3)*z[3];
    z[70]=z[3]*z[70];
    z[55]= - z[55] + n<T>(193,5)*z[6] + z[9] + n<T>(213,5)*z[4];
    z[55]=n<T>(2371,80)*z[8] + n<T>(341,60)*z[2] + n<T>(1,2)*z[55] + n<T>(167,15)*z[3];
    z[55]=z[8]*z[55];
    z[71]= - n<T>(146,3)*z[14] - n<T>(179,2)*z[13];
    z[71]=n<T>(409,90)*z[2] + n<T>(43,3)*z[3] - n<T>(73,15)*z[12] + n<T>(97,6)*z[6]
     + n<T>(1,5)*z[71]
     + n<T>(3,2)*z[9];
    z[71]=z[7]*z[71];
    z[55]=z[64] + z[71] + z[55] + z[62] + n<T>(1,20)*z[70] + z[69] + z[67]
    + z[65] - z[61] + n<T>(169,10)*z[66] + n<T>(13,30)*z[22] - n<T>(14,15)*z[19]
     + 
    z[15] - n<T>(319,90)*z[16] + n<T>(126,5)*z[17] - n<T>(182,45)*z[20] + n<T>(1,2)*
    z[18] + n<T>(146,15)*z[21];
    z[55]=z[1]*z[55];
    z[61]= - n<T>(571,2)*z[24] + 593*z[29] + n<T>(683,2)*z[27];
    z[55]=n<T>(1,90)*z[61] + z[55];
    z[55]=i*z[55];
    z[61]=n<T>(3,2)*z[3] - z[63];
    z[61]=z[9]*z[61];
    z[62]=14*z[19] - 146*z[21] + n<T>(91,3)*z[20];
    z[62]= - n<T>(707,360)*z[23] + n<T>(1,5)*z[62] + n<T>(13,2)*z[22];
    z[64]=n<T>(457,6)*z[6] - 57*z[12];
    z[64]=z[12]*z[64];
    z[65]=n<T>(31,2)*z[3] - n<T>(91,9)*z[2];
    z[65]=z[2]*z[65];
    z[66]=n<T>(7,4)*z[8] - n<T>(21,20)*z[2] + z[63] + n<T>(14,3)*z[3];
    z[66]=z[8]*z[66];
    z[67]= - n<T>(91,45)*z[2] - n<T>(43,6)*z[3] + n<T>(73,30)*z[12] - n<T>(97,12)*z[6]
    + n<T>(829,60)*z[13] - z[9];
    z[67]=z[7]*z[67];
    z[61]=z[67] + z[66] + n<T>(1,10)*z[65] + n<T>(1,5)*z[64] + n<T>(27,5)*z[56] + n<T>(1,3)*z[62]
     - n<T>(87,5)*z[52]
     + z[61];
    z[61]=z[7]*z[61];
    z[62]=npow(z[3],2);
    z[64]=npow(z[12],2);
    z[65]=91*z[20] + n<T>(319,4)*z[16];
    z[66]=49*z[3] + n<T>(683,15)*z[2];
    z[66]=z[2]*z[66];
    z[57]=n<T>(1,24)*z[66] + n<T>(11,120)*z[62] - n<T>(1,4)*z[64] + z[57] + z[59]
     - 
   n<T>(589,1080)*z[23] - n<T>(13,5)*z[22] + n<T>(1,45)*z[65] - n<T>(1,2)*z[15];
    z[57]=z[2]*z[57];
    z[59]=n<T>(629,90)*z[12] + z[63] - z[68];
    z[59]=z[12]*z[59];
    z[65]=201*z[33] + n<T>(427,3)*z[32];
    z[56]=z[59] - n<T>(457,30)*z[56] + n<T>(1,5)*z[65] - n<T>(5,9)*z[23];
    z[56]=z[12]*z[56];
    z[56]=z[56] - z[36];
    z[59]= - z[4] + n<T>(1,2)*z[6];
    z[59]=z[6]*z[59];
    z[59]= - n<T>(1417,8)*z[64] + 193*z[59] - 109*z[58] + n<T>(11563,36)*z[23]
    - 213*z[35] - n<T>(1937,3)*z[22];
    z[65]= - n<T>(49,12)*z[2] - z[9] + n<T>(21,10)*z[3];
    z[65]=z[2]*z[65];
    z[59]=z[65] + n<T>(1,5)*z[59] + n<T>(365,6)*z[62];
    z[62]= - 15*z[8] - n<T>(91,60)*z[2] + n<T>(1633,30)*z[3] - n<T>(1473,80)*z[12]
    + n<T>(193,20)*z[6] + n<T>(1,4)*z[9] + n<T>(156,5)*z[4];
    z[62]=z[8]*z[62];
    z[59]=n<T>(1,2)*z[59] + z[62];
    z[59]=z[8]*z[59];
    z[62]= - n<T>(1,3)*z[33] + n<T>(13,5)*z[21];
    z[62]=13*z[62] + z[19];
    z[52]=n<T>(1229,180)*z[52] + n<T>(1,2)*z[62] + n<T>(4,9)*z[23];
    z[52]=z[13]*z[52];
    z[62]=z[9] - z[4];
    z[62]=z[62]*z[63];
    z[62]=z[62] + n<T>(193,30)*z[23] - z[18] - z[15];
    z[62]=z[62]*z[63];
    z[63]= - 62*z[4] + n<T>(79,6)*z[6];
    z[63]=z[6]*z[63];
    z[58]=n<T>(1,5)*z[63] + n<T>(223,20)*z[58] + n<T>(169,180)*z[23] + 2*z[34] + n<T>(59,15)*z[19];
    z[58]=z[6]*z[58];
    z[63]=449*z[32] - n<T>(71,2)*z[17];
    z[63]= - n<T>(467,6)*z[64] + n<T>(827,24)*z[23] + n<T>(1,2)*z[63] - 427*z[22];
    z[64]= - 14*z[12] - n<T>(1543,72)*z[3];
    z[64]=z[3]*z[64];
    z[63]=n<T>(1,5)*z[63] + z[64];
    z[63]=z[3]*z[63];
    z[64]=z[37] - z[38];
    z[65]=z[28] - z[40];
    z[66]=z[49] + z[50];
    z[66]=n<T>(27,2)*z[47] - n<T>(457,6)*z[48] + n<T>(193,2)*z[51] - 109*z[66];
    z[66]=n<T>(1,2)*z[66] + n<T>(74,3)*z[46];
    z[54]= - z[54] + n<T>(1,2)*z[11] - n<T>(146,15)*z[14];
    z[54]=z[23]*z[54];
    z[60]= - n<T>(1,4)*z[60] - n<T>(509,360)*z[23] - n<T>(99,5)*z[35] + n<T>(1,2)*z[34];
    z[60]=z[9]*z[60];

    r += n<T>(253,24)*z[25] + n<T>(1223,2160)*z[26] - n<T>(593,180)*z[30] - n<T>(57419,720)*z[31]
     - n<T>(15277,240)*z[39]
     + n<T>(91,45)*z[41] - n<T>(11827,240)*
      z[42] - n<T>(1577,80)*z[43] - n<T>(1,6)*z[44] + n<T>(49,12)*z[45] + z[52] + 
      z[53] + z[54] + z[55] + n<T>(1,2)*z[56] + z[57] + z[58] + z[59] + 
      z[60] + z[61] + z[62] + z[63] + n<T>(1,4)*z[64] - n<T>(683,360)*z[65] + n<T>(1,5)*z[66];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf113(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf113(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
