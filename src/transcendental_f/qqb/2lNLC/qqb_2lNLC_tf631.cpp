#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf631(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[76];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[6];
    z[15]=d[17];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[14];
    z[46]=f[16];
    z[47]=f[17];
    z[48]=f[18];
    z[49]=f[31];
    z[50]=f[36];
    z[51]=f[37];
    z[52]=f[39];
    z[53]=f[51];
    z[54]=f[55];
    z[55]=f[58];
    z[56]=f[94];
    z[57]=n<T>(204779,12248)*z[7];
    z[58]=5*z[11];
    z[59]= - 107241*z[9] - n<T>(478615,4)*z[3];
    z[59]=n<T>(53,2)*z[5] - n<T>(1452007,18372)*z[2] + n<T>(389087,18372)*z[4] + 
    z[57] + n<T>(1137513,12248)*z[10] + n<T>(1,1531)*z[59] + z[58];
    z[59]=z[5]*z[59];
    z[60]=n<T>(1,2)*z[10];
    z[61]=n<T>(6086011,36744)*z[10] - n<T>(220677,3062)*z[11] + 37*z[13] + 
   n<T>(542177,4593)*z[14];
    z[61]=z[61]*z[60];
    z[62]=n<T>(1,4)*z[7];
    z[63]= - n<T>(16111,6124)*z[7] - n<T>(1137513,3062)*z[10] + n<T>(472491,1531)*
    z[3] + z[11];
    z[63]=z[63]*z[62];
    z[64]= - n<T>(33,2)*z[13] + 4*z[12];
    z[65]=n<T>(23,4)*z[4] - z[62] - n<T>(33,2)*z[10] - n<T>(1,4)*z[11] - z[64];
    z[65]=z[4]*z[65];
    z[66]= - n<T>(33,2)*z[19] + 4*z[23];
    z[67]=npow(z[8],2);
    z[68]=npow(z[11],2);
    z[69]=z[15] - n<T>(1,2)*z[14];
    z[69]=z[14]*z[69];
    z[70]= - 224327*z[9] + n<T>(434875,4)*z[3];
    z[70]=z[3]*z[70];
    z[71]=2*z[8];
    z[72]=n<T>(1252531,18372)*z[2] - n<T>(452071,9186)*z[4] + n<T>(35,4)*z[7] - 
    z[71] + n<T>(137131,6124)*z[3];
    z[72]=z[2]*z[72];
    z[73]= - n<T>(302905,2)*z[15] - 542177*z[14];
    z[73]=n<T>(261355,4)*z[6] + n<T>(706643,12)*z[5] - n<T>(1253849,12)*z[2] + 
   n<T>(147189,4)*z[4] - n<T>(422323,12)*z[10] + n<T>(220677,4)*z[11] + n<T>(1,6)*z[73]
    - 29307*z[3];
    z[73]=z[6]*z[73];
    z[59]=n<T>(1,1531)*z[73] + z[59] + z[72] + z[65] + z[63] + z[61] - n<T>(94263,6124)*z[68]
     + n<T>(1,3062)*z[70]
     + n<T>(211511,18372)*z[69]
     + z[67]
     + n<T>(27776,1531)*z[22]
     + n<T>(764395,9186)*z[16]
     + n<T>(520307,9186)*z[17]
     + n<T>(107241,1531)*z[18]
     + n<T>(555733,9186)*z[20]
     + n<T>(302905,18372)*z[21] - z[66];
    z[59]=z[1]*z[59];
    z[61]= - n<T>(1672471,24)*z[25] - 1840435*z[30] - n<T>(1252531,2)*z[28];
    z[59]=n<T>(1,9186)*z[61] + z[59];
    z[59]=i*z[59];
    z[61]=3*z[7];
    z[63]=z[61] - z[58];
    z[65]=n<T>(104980,4593)*z[4] - 6*z[14] - z[63];
    z[65]=z[4]*z[65];
    z[69]=npow(z[14],2);
    z[70]=npow(z[10],2);
    z[72]=npow(z[3],2);
    z[73]= - n<T>(2054369,6)*z[24] - 389087*z[16] - 270977*z[20] - n<T>(520307,2)
   *z[17];
    z[74]=npow(z[7],2);
    z[75]=z[11] - z[3];
    z[75]= - n<T>(1252531,36744)*z[2] + n<T>(40019,18372)*z[4] - z[7] + z[8] + 3
   *z[75];
    z[75]=z[2]*z[75];
    z[65]=z[75] + z[65] + n<T>(35,8)*z[74] - n<T>(35,2)*z[70] + n<T>(143255,12248)*
    z[72] + n<T>(1,2)*z[69] + n<T>(1,9186)*z[73] - z[67];
    z[65]=z[2]*z[65];
    z[73]= - z[11] - z[8] + z[14];
    z[61]=n<T>(575423,36744)*z[2] - n<T>(3934,4593)*z[4] + z[61] - n<T>(100387,6124)
   *z[3] + 6*z[73];
    z[61]=z[2]*z[61];
    z[73]= - z[14] + n<T>(101409,3062)*z[3];
    z[57]= - n<T>(517691,18372)*z[4] - z[57] + 5*z[73] - n<T>(1113017,12248)*
    z[10];
    z[57]= - n<T>(53,12)*z[5] + n<T>(1,2)*z[57] + n<T>(76109,4593)*z[2];
    z[57]=z[5]*z[57];
    z[73]=n<T>(215141,4)*z[72] + n<T>(3839555,144)*z[24] - 107241*z[18] - n<T>(520307,12)*z[17];
    z[74]= - n<T>(204353,4)*z[7] - 466367*z[3] + n<T>(1137513,2)*z[10];
    z[74]=z[7]*z[74];
    z[75]= - z[10] + 3*z[8] - 2*z[11];
    z[75]=2*z[75] - n<T>(278855,36744)*z[4];
    z[75]=z[4]*z[75];
    z[57]=z[57] + z[61] + z[75] + n<T>(1,6124)*z[74] - n<T>(1538635,24496)*z[70]
    + n<T>(1,1531)*z[73] + n<T>(5,2)*z[68];
    z[57]=z[5]*z[57];
    z[61]= - n<T>(771797,12)*z[10] - n<T>(542177,3)*z[14] + n<T>(220677,2)*z[11];
    z[60]=z[61]*z[60];
    z[61]= - n<T>(302905,4)*z[21] - 142378*z[20];
    z[60]=z[60] + n<T>(232925,8)*z[68] - 13888*z[72] - n<T>(229883,24)*z[69] + 
   n<T>(457073,8)*z[24] + n<T>(1,3)*z[61] - 27776*z[22];
    z[61]= - n<T>(578039,36744)*z[5] + n<T>(70862,4593)*z[2] + n<T>(141065,6124)*
    z[4] - z[14] - z[3] - z[63];
    z[61]=z[5]*z[61];
    z[63]=n<T>(1,2)*z[7];
    z[73]= - z[3] + z[63];
    z[73]=z[7]*z[73];
    z[71]= - z[7] - z[71] + 3*z[14];
    z[71]=2*z[71] - n<T>(183933,12248)*z[4];
    z[71]=z[4]*z[71];
    z[58]=n<T>(298535,18372)*z[2] - n<T>(27,4)*z[7] + 6*z[8] - z[58];
    z[58]=z[2]*z[58];
    z[74]= - n<T>(88139,12248)*z[6] - n<T>(835247,36744)*z[5] + n<T>(373331,36744)*
    z[2] - n<T>(104321,12248)*z[4] + n<T>(47,8)*z[7] + n<T>(422323,36744)*z[10] - 
   n<T>(220677,12248)*z[11] + n<T>(15419,1531)*z[3] - z[8] + n<T>(1295399,36744)*
    z[14];
    z[74]=z[6]*z[74];
    z[58]=z[74] + z[61] + z[58] + z[71] + n<T>(1,1531)*z[60] + z[73];
    z[58]=z[6]*z[58];
    z[60]= - 7*z[7] - n<T>(1186505,6124)*z[10] + n<T>(304081,1531)*z[3] + n<T>(9,2)*
    z[11];
    z[60]=z[60]*z[63];
    z[61]= - z[37] - n<T>(343887,1531)*z[22];
    z[60]=z[60] - n<T>(939801,12248)*z[70] - n<T>(23,4)*z[68] + n<T>(230451,3062)*
    z[72] + n<T>(1,2)*z[61] + n<T>(139463,4593)*z[24];
    z[60]=z[60]*z[63];
    z[61]= - z[11] + n<T>(33,2)*z[7];
    z[61]=z[61]*z[62];
    z[62]= - n<T>(37,6)*z[4] + n<T>(21,8)*z[7] - z[8] - n<T>(3,8)*z[11];
    z[62]=z[4]*z[62];
    z[61]=z[62] + z[61] + n<T>(37,2)*z[70] - n<T>(49,8)*z[68] + n<T>(673853,110232)*
    z[24] - n<T>(187654,4593)*z[16] + z[66];
    z[61]=z[4]*z[61];
    z[62]=n<T>(487061,3)*z[34] - n<T>(558369,2)*z[33];
    z[62]=n<T>(7250483,55116)*z[24] + n<T>(1,1531)*z[62] - 37*z[19];
    z[63]=n<T>(36053,3)*z[14] + 34001*z[3];
    z[63]=n<T>(248225,3)*z[10] + 13*z[63] - n<T>(159437,2)*z[11];
    z[63]=z[10]*z[63];
    z[62]=n<T>(1,6124)*z[63] - n<T>(269669,12248)*z[68] + n<T>(1,2)*z[62] + 31*z[72]
   ;
    z[62]=z[10]*z[62];
    z[63]= - 70791*z[22] - n<T>(754337,2)*z[33] - 224327*z[18];
    z[63]=n<T>(1,2)*z[63] - n<T>(28762,3)*z[24];
    z[66]=4*z[14] - n<T>(341337,3062)*z[3];
    z[66]=z[3]*z[66];
    z[63]=n<T>(1,1531)*z[63] + z[66];
    z[63]=z[3]*z[63];
    z[66]=z[29] - z[42];
    z[64]= - n<T>(107241,1531)*z[9] - n<T>(302905,18372)*z[15] - z[64];
    z[64]=z[24]*z[64];
    z[67]=n<T>(1,3)*z[67] + n<T>(8238,1531)*z[24] - n<T>(17,4)*z[37] + 9*z[36];
    z[67]=z[8]*z[67];
    z[69]=n<T>(58411,36744)*z[69] - n<T>(130277,12248)*z[24] + n<T>(211511,18372)*
    z[21] - 9*z[35] + n<T>(284969,9186)*z[34];
    z[69]=z[14]*z[69];
    z[68]=n<T>(31,6)*z[68] + n<T>(80048,4593)*z[24] + 7*z[36] + 5*z[35];
    z[68]=z[11]*z[68];

    r += n<T>(538679,9186)*z[26] + n<T>(2293265,220464)*z[27] + n<T>(1840435,18372)
      *z[31] - n<T>(34966115,110232)*z[32] - 33*z[38] + n<T>(33,2)*z[39] - 
      n<T>(389087,36744)*z[40] - n<T>(204353,24496)*z[41] - n<T>(651527,36744)*
      z[43] + n<T>(1550883,24496)*z[44] - n<T>(1,2)*z[45] - n<T>(988793,24496)*
      z[46] - n<T>(159437,12248)*z[47] - 12*z[48] + n<T>(232925,12248)*z[49] + 
      n<T>(790169,36744)*z[50] + n<T>(3,2)*z[51] - n<T>(208429,12248)*z[52] - n<T>(49,8)
      *z[53] - n<T>(33,8)*z[54] - n<T>(15,8)*z[55] + z[56] + z[57] + z[58] + 
      z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + n<T>(1252531,36744)*z[66]
     + z[67]
     + z[68]
     + z[69];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf631(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf631(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
