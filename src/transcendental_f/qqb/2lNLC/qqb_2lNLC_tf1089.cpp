#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1089(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[68];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=d[18];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[7];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=e[10];
    z[21]=e[11];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[6];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[6];
    z[35]=f[7];
    z[36]=f[11];
    z[37]=f[12];
    z[38]=f[13];
    z[39]=f[16];
    z[40]=f[17];
    z[41]=f[23];
    z[42]=f[30];
    z[43]=f[31];
    z[44]=f[33];
    z[45]=f[36];
    z[46]=f[39];
    z[47]=z[1]*i;
    z[48]=z[47] - z[7];
    z[49]=n<T>(1,2)*z[10];
    z[50]= - z[49] - z[48];
    z[50]=z[10]*z[50];
    z[51]=z[7]*z[48];
    z[52]=npow(z[6],2);
    z[53]= - z[11] - z[47] - z[7];
    z[53]=z[6] + n<T>(1,2)*z[53];
    z[53]=z[11]*z[53];
    z[50]=z[53] + z[50] + z[51] + z[52];
    z[50]=z[11]*z[50];
    z[51]=z[7] + z[11];
    z[51]=z[47]*z[51];
    z[51]=z[22] + z[51];
    z[51]=z[12]*z[51];
    z[52]=z[11] - z[48];
    z[52]=z[19]*z[52];
    z[53]= - z[10] - z[11];
    z[53]=z[31]*z[53];
    z[50]=z[42] + z[50] + z[51] + z[52] + z[53];
    z[51]=n<T>(1,4)*z[7];
    z[52]=n<T>(1,4)*z[5];
    z[53]= - n<T>(1,4)*z[2] - z[52] + z[47] - z[51];
    z[53]=z[2]*z[53];
    z[54]=n<T>(1,2)*z[7];
    z[55]=z[54] - z[47];
    z[56]=z[7]*z[55];
    z[51]=n<T>(7,6)*z[5] - n<T>(2,3)*z[47] + z[51];
    z[51]=z[5]*z[51];
    z[57]=5*z[47];
    z[58]=z[57] - z[5];
    z[58]= - n<T>(4,3)*z[3] + n<T>(1,6)*z[58] + z[2];
    z[58]=z[3]*z[58];
    z[51]=z[58] + z[53] + z[56] + z[51];
    z[51]=z[3]*z[51];
    z[53]=n<T>(5,3)*z[5];
    z[56]=n<T>(1,2)*z[2];
    z[58]= - n<T>(1,3)*z[8] + n<T>(13,6)*z[3] - z[56] - z[53] + n<T>(5,3)*z[47] + 
    z[54];
    z[58]=z[8]*z[58];
    z[59]= - z[7]*z[47];
    z[60]=2*z[47] - z[5];
    z[60]=z[5]*z[60];
    z[61]=z[47] - z[5];
    z[62]= - n<T>(1,2)*z[3] - z[61];
    z[62]=z[3]*z[62];
    z[58]=z[58] + n<T>(1,3)*z[62] + z[59] + n<T>(2,3)*z[60];
    z[58]=z[8]*z[58];
    z[59]=n<T>(1,2)*z[5];
    z[60]=z[7] + z[59];
    z[60]=z[55]*z[60];
    z[62]=z[55] + z[56];
    z[63]=z[59] - z[62];
    z[63]=z[63]*z[56];
    z[64]=z[2] - z[5];
    z[65]=3*z[6];
    z[66]=z[64] - z[65];
    z[67]=z[6]*z[66];
    z[60]=n<T>(1,4)*z[67] + z[63] + z[60];
    z[60]=z[60]*z[65];
    z[59]=z[47] - z[59];
    z[59]=z[5]*z[59];
    z[63]=n<T>(3,2)*z[6];
    z[65]=n<T>(1,2)*z[6] + z[48];
    z[65]=z[65]*z[63];
    z[57]= - z[57] + 3*z[7];
    z[57]= - n<T>(3,2)*z[10] + z[63] - z[8] + n<T>(1,2)*z[57] + z[5];
    z[49]=z[57]*z[49];
    z[57]= - n<T>(1,2)*z[8] - z[61];
    z[57]=z[8]*z[57];
    z[49]=z[49] + z[65] + z[59] + z[57];
    z[49]=z[10]*z[49];
    z[57]=z[47] - n<T>(3,2)*z[7];
    z[54]=z[57]*z[54];
    z[53]= - n<T>(65,3)*z[2] + z[53] + n<T>(131,3)*z[47] + z[7];
    z[53]=z[53]*z[56];
    z[57]= - 19*z[47] + 35*z[5];
    z[57]=z[5]*z[57];
    z[53]=z[53] + z[54] + n<T>(1,3)*z[57];
    z[53]=z[53]*z[56];
    z[54]= - n<T>(43,2)*z[7] - z[5];
    z[52]=z[54]*z[52];
    z[54]= - 9*z[47] + n<T>(11,2)*z[7];
    z[54]=z[7]*z[54];
    z[52]=z[54] + z[52];
    z[52]=z[5]*z[52];
    z[54]=z[47] - n<T>(5,4)*z[7];
    z[54]=z[54]*npow(z[7],2);
    z[52]= - z[41] - z[53] - z[54] - z[52] + z[39] + z[37];
    z[53]=n<T>(5,2)*z[7];
    z[54]= - z[55]*z[53];
    z[55]=n<T>(1,12)*z[5] + n<T>(13,3)*z[47] + z[53];
    z[55]=z[5]*z[55];
    z[54]=z[54] + z[55];
    z[55]=z[2] - n<T>(41,8)*z[47] - 2*z[5];
    z[55]=z[2]*z[55];
    z[53]=n<T>(41,6)*z[2] - z[53] - n<T>(13,3)*z[5];
    z[53]=z[4]*z[53];
    z[53]=n<T>(1,8)*z[53] + n<T>(1,4)*z[54] + n<T>(1,3)*z[55];
    z[53]=z[4]*z[53];
    z[54]= - z[5] - z[3];
    z[54]=z[47]*z[54];
    z[54]= - z[22] + z[54];
    z[54]=z[9]*z[54];
    z[55]= - z[3] + z[61];
    z[55]=z[16]*z[55];
    z[54]=z[54] + z[55];
    z[55]=z[48] + z[66];
    z[55]=n<T>(1,2)*z[55] - z[11];
    z[55]=z[17]*z[55];
    z[57]= - n<T>(14,3)*z[8] - n<T>(11,3)*z[3] + z[64] + z[48];
    z[57]=z[20]*z[57];
    z[59]=z[7] + z[8];
    z[59]=z[47]*z[59];
    z[59]=z[22] + z[59];
    z[59]=z[13]*z[59];
    z[56]= - n<T>(1,2)*z[4] - z[56] + z[47];
    z[56]=z[14]*z[56];
    z[61]=z[18]*z[62];
    z[62]=z[43] + z[46] - z[45];
    z[63]=z[38] - z[27];
    z[64]= - n<T>(155,12)*z[28] - n<T>(155,24)*z[26] - n<T>(1,18)*z[23];
    z[64]=i*z[64];
    z[65]= - n<T>(103,24)*z[7] + 3*z[5];
    z[65]=n<T>(761,576)*z[4] + n<T>(1,4)*z[11] + n<T>(1,12)*z[10] - n<T>(15,8)*z[6]
     + n<T>(53,36)*z[8] - n<T>(3,8)*z[3]
     + n<T>(1,8)*z[65] - n<T>(2,9)*z[2];
    z[65]=z[22]*z[65];
    z[47]=n<T>(11,2)*z[2] + 25*z[47] - n<T>(61,2)*z[5];
    z[47]=z[15]*z[47];
    z[48]= - z[8] + z[48];
    z[48]=z[21]*z[48];

    r +=  - n<T>(5,6)*z[24] - n<T>(155,288)*z[25] + n<T>(155,24)*z[29] - n<T>(343,24)*
      z[30] - n<T>(91,24)*z[32] - n<T>(13,6)*z[33] - z[34] - 4*z[35] + n<T>(11,4)*
      z[36] - n<T>(35,16)*z[40] + 6*z[44] + n<T>(1,12)*z[47] + z[48] + z[49] + 
      n<T>(3,2)*z[50] + z[51] - n<T>(1,2)*z[52] + 7*z[53] + n<T>(5,3)*z[54] + 3*
      z[55] + n<T>(287,24)*z[56] + z[57] + z[58] + z[59] + z[60] + n<T>(9,8)*
      z[61] + n<T>(3,4)*z[62] - n<T>(155,48)*z[63] + z[64] + z[65];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1089(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1089(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
