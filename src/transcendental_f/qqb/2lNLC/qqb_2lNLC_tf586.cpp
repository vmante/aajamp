#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf586(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[35];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[7];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=d[4];
    z[8]=d[5];
    z[9]=d[6];
    z[10]=d[17];
    z[11]=e[2];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=e[10];
    z[15]=e[6];
    z[16]=f[4];
    z[17]=f[12];
    z[18]=f[16];
    z[19]=f[31];
    z[20]=f[36];
    z[21]=f[39];
    z[22]=z[3] + z[2];
    z[23]=i*z[1];
    z[24]=z[23]*z[22];
    z[24]=z[13] + z[24];
    z[24]=z[6]*z[24];
    z[25]=z[9] + z[4];
    z[25]= - z[23]*z[25];
    z[25]= - z[13] + z[25];
    z[25]=z[10]*z[25];
    z[22]= - z[23] + z[22];
    z[22]=z[11]*z[22];
    z[26]=z[23] - z[9];
    z[27]= - z[4] + z[26];
    z[27]=z[12]*z[27];
    z[22]=z[24] + z[25] + z[22] + z[27];
    z[24]=n<T>(1,3)*z[2];
    z[25]=z[23] + z[5];
    z[27]=2*z[25] - z[2];
    z[27]=z[27]*z[24];
    z[28]=z[23] + n<T>(1,2)*z[7];
    z[29]=z[28]*z[7];
    z[30]=n<T>(7,6)*z[5] - n<T>(1,3)*z[23] - z[7];
    z[30]=z[5]*z[30];
    z[31]=z[7] + n<T>(1,3)*z[5];
    z[31]=n<T>(1,2)*z[31] - n<T>(2,3)*z[2];
    z[31]=z[3]*z[31];
    z[27]=z[31] + z[27] - z[29] + z[30];
    z[27]=z[3]*z[27];
    z[30]= - z[23] + n<T>(1,2)*z[4];
    z[31]=z[30]*z[4];
    z[32]= - n<T>(7,3)*z[23] + z[7];
    z[32]=z[5]*z[32];
    z[32]=n<T>(1,2)*z[32] + z[29] - z[31];
    z[32]=z[5]*z[32];
    z[33]=n<T>(7,2)*z[2] - z[23] - n<T>(5,2)*z[5];
    z[24]=z[33]*z[24];
    z[25]=z[4] - n<T>(5,3)*z[25];
    z[25]=z[5]*z[25];
    z[24]=z[24] - z[31] + z[25];
    z[24]=z[2]*z[24];
    z[25]=5*z[7];
    z[31]=z[28]*z[25];
    z[33]=z[23] + z[7];
    z[34]= - 5*z[33] + n<T>(7,2)*z[4];
    z[34]=z[4]*z[34];
    z[26]=z[4] + z[26];
    z[26]=z[9]*z[26];
    z[26]=z[26] + z[31] + z[34];
    z[31]=n<T>(1,3)*z[9];
    z[26]=z[26]*z[31];
    z[30]=z[7] - z[30];
    z[30]=z[4]*z[30];
    z[33]=z[4] - z[33];
    z[33]=z[8]*z[33];
    z[29]=n<T>(1,2)*z[33] - z[29] + z[30];
    z[29]=z[8]*z[29];
    z[30]=z[5] + z[2];
    z[30]=z[14]*z[30];
    z[33]=z[7] + z[9];
    z[33]=z[15]*z[33];
    z[30]=z[30] + z[33];
    z[28]=z[28]*npow(z[7],2);
    z[23]=z[4] - 2*z[23] - n<T>(7,2)*z[7];
    z[23]=z[4]*z[7]*z[23];
    z[23]=5*z[28] + z[23];
    z[25]= - z[25] - z[4];
    z[25]=n<T>(1,2)*z[25] - 2*z[5];
    z[25]=n<T>(1,3)*z[8] - z[31] + n<T>(1,18)*z[3] + n<T>(1,9)*z[25] + n<T>(1,2)*z[2];
    z[25]=z[13]*z[25];
    z[28]= - z[17] - z[18] + z[21] - z[19];
    z[31]=z[20] + z[16];

    r += n<T>(2,3)*z[22] + n<T>(1,3)*z[23] + z[24] + z[25] + z[26] + z[27] - n<T>(1,2)*z[28]
     + z[29]
     + n<T>(5,3)*z[30]
     + n<T>(7,6)*z[31]
     + z[32];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf586(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf586(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
