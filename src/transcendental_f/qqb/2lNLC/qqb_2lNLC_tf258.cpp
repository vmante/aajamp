#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf258(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[47];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[4];
    z[9]=e[0];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[19];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[30];
    z[22]=c[31];
    z[23]=e[10];
    z[24]=d[2];
    z[25]=f[0];
    z[26]=f[4];
    z[27]=f[5];
    z[28]=f[11];
    z[29]=f[12];
    z[30]=f[16];
    z[31]=f[20];
    z[32]=z[1]*i;
    z[33]=2*z[32];
    z[34]=n<T>(1,2)*z[2];
    z[35]=z[34] - z[4] - z[33];
    z[35]=z[2]*z[35];
    z[36]=npow(z[4],2);
    z[37]=n<T>(1,2)*z[36];
    z[38]=z[4]*z[33];
    z[39]= - z[4] + z[2];
    z[39]=z[5]*z[39];
    z[35]=z[39] + z[35] + z[37] + z[38];
    z[35]=z[5]*z[35];
    z[38]=z[32]*z[4];
    z[39]= - z[36] - 7*z[38];
    z[40]= - n<T>(7,4)*z[2] + n<T>(11,4)*z[4] + z[33];
    z[40]=z[2]*z[40];
    z[39]=n<T>(1,2)*z[39] + z[40];
    z[39]=z[2]*z[39];
    z[40]=n<T>(1,2)*z[3];
    z[41]=z[40] - z[4];
    z[41]= - z[32]*z[41];
    z[41]=z[13] + z[41];
    z[41]=z[7]*z[41];
    z[42]=n<T>(1,2)*z[4];
    z[43]= - n<T>(5,2)*z[2] + z[42] + z[33];
    z[43]=z[10]*z[43];
    z[44]=z[32] - z[4];
    z[40]=z[44] + z[40];
    z[45]=z[11]*z[40];
    z[33]=z[33] - z[2];
    z[46]= - z[5] + z[33];
    z[46]=z[12]*z[46];
    z[35]= - z[27] - z[45] + z[46] + z[31] - z[28] + z[39] + z[35] + 
    z[41] + z[43];
    z[36]= - z[36] + n<T>(5,2)*z[38];
    z[39]=z[44]*z[34];
    z[32]=z[42] - z[32];
    z[32]=n<T>(1,3)*z[32] + z[34];
    z[32]=n<T>(1,2)*z[32] + n<T>(1,3)*z[3];
    z[32]=z[3]*z[32];
    z[32]=z[32] + n<T>(1,3)*z[36] + z[39];
    z[32]=z[3]*z[32];
    z[34]=z[37] - z[38];
    z[36]=5*z[3];
    z[37]= - z[40]*z[36];
    z[36]= - n<T>(11,4)*z[44] - z[36];
    z[36]=z[6]*z[36];
    z[36]=z[36] - n<T>(1,2)*z[34] + z[37];
    z[36]=z[6]*z[36];
    z[37]=n<T>(1,2)*z[6] + z[44];
    z[37]=z[6]*z[37];
    z[38]=z[6] + z[44];
    z[38]=z[8]*z[38];
    z[34]=n<T>(1,2)*z[38] + z[37] + z[34];
    z[34]=z[8]*z[34];
    z[37]=z[5] - z[6];
    z[38]=z[42] - 7*z[2];
    z[38]= - z[8] + n<T>(1,9)*z[38] + z[3];
    z[37]=n<T>(1,4)*z[38] + n<T>(1,9)*z[37];
    z[37]=z[13]*z[37];
    z[38]=z[30] + z[29];
    z[33]= - z[24] + z[33];
    z[33]=z[9]*z[33];
    z[39]= - n<T>(10,3)*z[19] - n<T>(2,3)*z[17] + n<T>(1,36)*z[14];
    z[39]=i*z[39];
    z[40]=z[3] + z[6];
    z[40]=z[23]*z[40];

    r +=  - n<T>(1,12)*z[15] - n<T>(1,18)*z[16] + n<T>(2,3)*z[18] + n<T>(8,3)*z[20]
     +  z[21] + n<T>(41,24)*z[22] - z[25] + n<T>(11,24)*z[26] + z[32] + z[33] + n<T>(3,4)*z[34]
     + n<T>(1,3)*z[35]
     + n<T>(1,6)*z[36]
     + z[37]
     + n<T>(3,8)*z[38]
     +  z[39] + n<T>(5,6)*z[40];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf258(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf258(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
