#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf265(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[42];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[12];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[5];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=d[5];
    z[16]=c[15];
    z[17]=c[19];
    z[18]=c[23];
    z[19]=c[25];
    z[20]=c[26];
    z[21]=c[28];
    z[22]=c[31];
    z[23]=e[10];
    z[24]=f[4];
    z[25]=f[5];
    z[26]=f[11];
    z[27]=f[12];
    z[28]=f[16];
    z[29]=2*z[4];
    z[30]=z[1]*i;
    z[29]=z[29]*z[30];
    z[31]=npow(z[4],2);
    z[32]=n<T>(1,2)*z[31];
    z[29]=z[29] + z[32];
    z[33]=2*z[30];
    z[34]= - n<T>(1,2)*z[2] + z[4] + z[33];
    z[34]=z[2]*z[34];
    z[35]=z[4] - z[2];
    z[35]=z[5]*z[35];
    z[34]=z[35] + z[34] - z[29];
    z[34]=z[5]*z[34];
    z[33]=z[33] - z[2];
    z[35]= - n<T>(1,2)*z[4] - z[33];
    z[35]=z[2]*z[35];
    z[29]=z[35] + z[29];
    z[29]=z[2]*z[29];
    z[35]=z[4] - z[33];
    z[35]=z[10]*z[35];
    z[33]=z[5] - z[33];
    z[33]=z[13]*z[33];
    z[29]=z[35] + z[33] + z[26] + z[25] - z[19] + z[29] + z[34];
    z[33]=z[30]*z[4];
    z[34]=z[30] - z[4];
    z[35]=z[2]*z[34];
    z[36]=z[30] - z[2];
    z[37]= - n<T>(1,2)*z[36] + z[3];
    z[37]=z[3]*z[37];
    z[35]=z[37] + z[35] - z[31] + z[33];
    z[35]=z[3]*z[35];
    z[32]=z[32] - z[33];
    z[37]= - 2*z[34] - z[3];
    z[37]=z[3]*z[37];
    z[38]= - n<T>(5,4)*z[34] - 2*z[3];
    z[38]=z[7]*z[38];
    z[37]=z[38] - n<T>(1,2)*z[32] + z[37];
    z[37]=z[7]*z[37];
    z[38]=n<T>(1,2)*z[8];
    z[39]=z[7] + z[34];
    z[39]=z[39]*z[38];
    z[40]=n<T>(1,2)*z[7] + z[34];
    z[40]=z[7]*z[40];
    z[32]=z[39] + z[40] + z[32];
    z[32]=z[8]*z[32];
    z[31]= - z[31]*z[30];
    z[39]=npow(z[4],3);
    z[31]= - z[16] + n<T>(1,3)*z[39] + z[31];
    z[39]=n<T>(17,2)*z[4] - 7*z[2];
    z[39]= - n<T>(1,3)*z[7] + n<T>(1,9)*z[39] + z[3];
    z[38]= - n<T>(1,6)*z[15] - z[38] + n<T>(1,2)*z[39] - n<T>(1,9)*z[5];
    z[38]=z[14]*z[38];
    z[39]=z[3] - z[2];
    z[30]=z[30]*z[39];
    z[30]= - z[14] + z[30];
    z[30]=z[6]*z[30];
    z[39]=z[28] + z[27];
    z[40]=n<T>(4,3)*z[20] + n<T>(2,3)*z[18];
    z[40]=i*z[40];
    z[36]= - z[3] - z[36];
    z[36]=z[12]*z[36];
    z[33]=z[33] + z[14];
    z[33]=z[9]*z[33];
    z[34]= - z[11]*z[34];
    z[41]=z[3] + z[7];
    z[41]=z[23]*z[41];

    r += n<T>(1,18)*z[17] - n<T>(2,3)*z[21] + n<T>(31,12)*z[22] + n<T>(5,4)*z[24]
     + n<T>(1,3)*z[29]
     + z[30]
     + n<T>(1,2)*z[31]
     + n<T>(3,2)*z[32]
     + z[33]
     + z[34]
     +  z[35] + z[36] + z[37] + z[38] + n<T>(3,4)*z[39] + z[40] + 2*z[41];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf265(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf265(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
