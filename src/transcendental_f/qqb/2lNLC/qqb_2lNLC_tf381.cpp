#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf381(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[142];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[42];
    z[3]=f[61];
    z[4]=f[77];
    z[5]=f[79];
    z[6]=f[81];
    z[7]=c[11];
    z[8]=d[0];
    z[9]=d[1];
    z[10]=d[2];
    z[11]=d[4];
    z[12]=d[5];
    z[13]=d[6];
    z[14]=d[7];
    z[15]=d[8];
    z[16]=d[9];
    z[17]=c[12];
    z[18]=c[13];
    z[19]=c[20];
    z[20]=c[23];
    z[21]=c[32];
    z[22]=c[33];
    z[23]=c[35];
    z[24]=c[36];
    z[25]=c[37];
    z[26]=c[38];
    z[27]=c[39];
    z[28]=c[40];
    z[29]=c[43];
    z[30]=c[44];
    z[31]=c[47];
    z[32]=c[48];
    z[33]=c[49];
    z[34]=c[50];
    z[35]=c[55];
    z[36]=c[56];
    z[37]=c[57];
    z[38]=c[59];
    z[39]=c[81];
    z[40]=c[84];
    z[41]=f[22];
    z[42]=f[24];
    z[43]=f[44];
    z[44]=f[62];
    z[45]=f[69];
    z[46]=f[70];
    z[47]=f[80];
    z[48]=d[3];
    z[49]=g[24];
    z[50]=g[27];
    z[51]=g[38];
    z[52]=g[40];
    z[53]=g[44];
    z[54]=g[48];
    z[55]=g[50];
    z[56]=g[79];
    z[57]=g[82];
    z[58]=g[90];
    z[59]=g[92];
    z[60]=g[95];
    z[61]=g[99];
    z[62]=g[101];
    z[63]=g[110];
    z[64]=g[112];
    z[65]=g[113];
    z[66]=g[128];
    z[67]=g[131];
    z[68]=g[138];
    z[69]=g[140];
    z[70]=g[145];
    z[71]=g[147];
    z[72]=g[170];
    z[73]=g[173];
    z[74]=g[181];
    z[75]=g[183];
    z[76]=g[187];
    z[77]=g[189];
    z[78]=g[191];
    z[79]=g[198];
    z[80]=g[200];
    z[81]=g[201];
    z[82]=g[212];
    z[83]=g[214];
    z[84]=g[218];
    z[85]=g[220];
    z[86]=g[224];
    z[87]=g[232];
    z[88]=g[233];
    z[89]=g[234];
    z[90]=g[243];
    z[91]=g[245];
    z[92]=g[249];
    z[93]=g[251];
    z[94]=g[254];
    z[95]=g[270];
    z[96]=g[272];
    z[97]=g[276];
    z[98]=g[278];
    z[99]=g[281];
    z[100]=g[285];
    z[101]=g[288];
    z[102]=g[289];
    z[103]=g[290];
    z[104]=g[292];
    z[105]=g[294];
    z[106]=g[301];
    z[107]=g[324];
    z[108]=g[339];
    z[109]=g[345];
    z[110]=g[347];
    z[111]=g[348];
    z[112]=g[384];
    z[113]=g[424];
    z[114]=g[427];
    z[115]=g[428];
    z[116]=g[430];
    z[117]=g[431];
    z[118]=g[433];
    z[119]=g[434];
    z[120]=g[435];
    z[121]= - 4*z[19] + n<T>(1,5)*z[20];
    z[122]= - n<T>(48,5)*z[18] + z[121];
    z[122]=2*z[122] + n<T>(271,405)*z[7];
    z[122]=z[14]*z[122];
    z[122]=z[122] + z[37];
    z[123]=12*z[2] - n<T>(11,6)*z[4] + n<T>(91,6)*z[5] - 20*z[3] + 8*z[6];
    z[123]=z[1]*z[123];
    z[124]=8*z[40] - n<T>(65,9)*z[39];
    z[124]=2*z[124] + n<T>(757,120)*z[38];
    z[125]=z[13]*z[7];
    z[121]=n<T>(1,3)*z[121] - n<T>(16,5)*z[18];
    z[121]=46*z[121] + n<T>(1301,405)*z[7];
    z[121]=z[15]*z[121];
    z[126]= - z[19] + n<T>(1,20)*z[20];
    z[127]= - n<T>(4,5)*z[18] + n<T>(1,3)*z[126];
    z[128]= - 187*z[127] - n<T>(667,1620)*z[7];
    z[128]=z[16]*z[128];
    z[129]= - n<T>(2,5)*z[18] + n<T>(1,6)*z[126];
    z[130]= - 67*z[129] - n<T>(893,360)*z[7];
    z[130]=z[11]*z[130];
    z[129]= - 349*z[129] - n<T>(12739,3240)*z[7];
    z[129]=z[9]*z[129];
    z[126]= - n<T>(6,5)*z[18] + n<T>(1,2)*z[126];
    z[131]=189*z[126] + n<T>(8117,3240)*z[7];
    z[131]=z[10]*z[131];
    z[127]=19*z[127] - n<T>(5141,1620)*z[7];
    z[127]=z[12]*z[127];
    z[126]= - 93*z[126] - n<T>(743,1080)*z[7];
    z[126]=z[8]*z[126];
    z[121]=z[126] + z[127] + z[131] + z[129] + z[130] + z[128] + z[121]
    + n<T>(98,27)*z[125] + n<T>(1217,27)*z[22] + 32*z[25] - n<T>(3578,27)*z[26] - 
   n<T>(218,243)*z[27] + n<T>(40,3)*z[30] + n<T>(2423,1080)*z[31] - n<T>(558,5)*z[33]
    - 16*z[34] - n<T>(93,2)*z[36] + n<T>(1,3)*z[124] + z[123] + 2*z[122];
    z[121]=i*z[121];
    z[122]= - n<T>(175,6)*z[41] + n<T>(47,3)*z[45];
    z[123]=n<T>(23,2)*z[42];
    z[124]=n<T>(23,2)*z[47];
    z[125]=n<T>(31,6)*z[2] + z[4] + 5*z[5] - n<T>(131,3)*z[3] - n<T>(791,9)*z[17]
     - 
   n<T>(71,3)*z[43] + n<T>(15,2)*z[44] + z[124] + z[123] - z[122];
    z[125]=z[16]*z[125];
    z[125]=z[125] + z[116];
    z[126]=z[4] + z[5];
    z[127]=z[2] + z[3];
    z[126]=z[6] + z[47] - n<T>(14,3)*z[127] + n<T>(11,3)*z[126];
    z[126]=z[48]*z[126];
    z[127]=n<T>(7,4)*z[6];
    z[128]=179*z[42];
    z[129]=50*z[44] + n<T>(565,4)*z[47] + n<T>(269,2)*z[41] - n<T>(83,2)*z[46] + 
    z[128];
    z[129]= - n<T>(677,12)*z[2] + n<T>(3,2)*z[4] - 16*z[5] - n<T>(227,12)*z[3] + 
    z[127] - 98*z[17] + n<T>(1,3)*z[129] - 6*z[43];
    z[129]=z[13]*z[129];
    z[130]=275*z[45];
    z[131]= - z[130] + 269*z[41];
    z[124]=n<T>(61,3)*z[44] + n<T>(1,3)*z[131] - z[124];
    z[124]= - 51*z[2] + n<T>(83,12)*z[4] + n<T>(47,4)*z[5] + n<T>(35,4)*z[6] - n<T>(293,9)*z[17]
     + n<T>(1,2)*z[124] - 55*z[43];
    z[124]=z[15]*z[124];
    z[131]=n<T>(27,2)*z[46];
    z[122]= - n<T>(17,6)*z[44] - z[131] - z[122];
    z[122]= - n<T>(11,4)*z[2] - n<T>(7,12)*z[4] - n<T>(77,12)*z[5] + n<T>(21,4)*z[3]
     - 
   n<T>(27,4)*z[6] - 22*z[17] + n<T>(1,2)*z[122] - 16*z[43];
    z[122]=z[14]*z[122];
    z[123]=n<T>(305,3)*z[43] - n<T>(1,2)*z[44] - n<T>(269,3)*z[41] - z[123] + z[131]
    + n<T>(275,3)*z[45];
    z[123]=n<T>(173,3)*z[2] - n<T>(35,6)*z[4] - n<T>(47,6)*z[5] + n<T>(199,12)*z[3]
     + n<T>(1,2)*z[123]
     + n<T>(493,9)*z[17];
    z[123]=z[9]*z[123];
    z[131]=83*z[46];
    z[130]= - z[131] - z[130];
    z[130]= - n<T>(299,2)*z[47] + n<T>(1,2)*z[130] + z[128];
    z[130]= - n<T>(13,4)*z[2] + n<T>(43,12)*z[4] - n<T>(217,12)*z[5] + n<T>(23,4)*z[3]
    - 4*z[6] + n<T>(95,6)*z[17] + n<T>(236,3)*z[43] + n<T>(1,3)*z[130] - n<T>(77,4)*
    z[44];
    z[130]=z[10]*z[130];
    z[128]= - 188*z[43] + 62*z[44] + n<T>(293,2)*z[47] - n<T>(175,4)*z[41] - 
    z[128] + n<T>(247,4)*z[46] + 161*z[45];
    z[127]= - n<T>(4,3)*z[2] - n<T>(29,6)*z[4] + n<T>(17,3)*z[5] + n<T>(41,3)*z[3]
     + 
    z[127] + n<T>(1,3)*z[128] - n<T>(45,2)*z[17];
    z[127]=z[8]*z[127];
    z[128]=z[115] - z[120];
    z[132]=z[80] - z[111];
    z[133]=z[79] - z[110];
    z[134]=z[73] + z[74];
    z[135]=z[72] + z[75];
    z[136]=z[63] - z[87];
    z[137]=z[56] + z[59];
    z[138]=z[53] - z[108];
    z[139]=z[50] + z[51];
    z[140]=z[49] + z[52];
    z[141]= - n<T>(119,3)*z[44] - 29*z[47] - 27*z[46] + 23*z[42];
    z[141]=n<T>(1,2)*z[141] + n<T>(25,3)*z[43];
    z[141]=2*z[2] + n<T>(3,4)*z[4] + n<T>(179,12)*z[5] - n<T>(143,12)*z[3] + n<T>(9,4)*
    z[6] + n<T>(1,2)*z[141] + n<T>(514,9)*z[17];
    z[141]=z[11]*z[141];
    z[131]=n<T>(1643,3)*z[17] + 107*z[43] - n<T>(245,2)*z[44] - 239*z[47] - n<T>(713,2)*z[41]
     - n<T>(785,2)*z[42]
     + z[131]
     + 47*z[45];
    z[131]=n<T>(59,3)*z[5] + n<T>(193,6)*z[3] + n<T>(1,3)*z[131] - n<T>(19,2)*z[6];
    z[131]=n<T>(343,6)*z[2] + n<T>(1,2)*z[131] - n<T>(17,3)*z[4];
    z[131]=z[12]*z[131];

    r +=  - n<T>(868,135)*z[21] - n<T>(96,5)*z[23] + 200*z[24] - n<T>(500,3)*z[28]
       - 50*z[29] - n<T>(19,2)*z[32] + n<T>(2,5)*z[35] - n<T>(713,18)*z[54] + n<T>(785,18)*z[55]
     + n<T>(3305,24)*z[57]
     + n<T>(3289,24)*z[58]
     + n<T>(1,6)*z[60] - n<T>(223,6)*z[61]
     + n<T>(1901,36)*z[62] - n<T>(25,4)*z[64] - n<T>(25,2)*z[65]
     + n<T>(131,3)*z[66] - n<T>(115,3)*z[67]
     + n<T>(49,4)*z[68]
     + n<T>(125,3)*z[69] - n<T>(43,9)*
      z[70] + n<T>(19,12)*z[71] - n<T>(5,12)*z[76] + n<T>(322,9)*z[77] - n<T>(319,18)*
      z[78] + 8*z[81] + n<T>(125,24)*z[82] + n<T>(647,24)*z[83] - n<T>(209,8)*z[84]
       - n<T>(43,24)*z[85] + n<T>(4,9)*z[86] + n<T>(21,4)*z[88] + n<T>(29,2)*z[89]
     + n<T>(71,6)*z[90] - n<T>(98,3)*z[91] - 32*z[92]
     + n<T>(52,3)*z[93]
     + n<T>(7,9)*z[94]
       - n<T>(565,6)*z[95] - n<T>(47,2)*z[96] - 31*z[97] - n<T>(559,6)*z[98] + n<T>(421,18)*z[99]
     + n<T>(15,4)*z[100] - 5*z[101] - 15*z[102] - 10*z[103]
     + 7*
      z[104] + 12*z[105] + z[106] - n<T>(15,2)*z[107] - z[109] + z[112] + n<T>(7,4)*z[113]
     - n<T>(3,2)*z[114]
     - z[117]
     - n<T>(13,4)*z[118]
     + n<T>(5,2)*z[119]
       + z[121] + z[122] + z[123] + z[124] + n<T>(1,2)*z[125] + z[126] + 
      z[127] - 6*z[128] + z[129] + z[130] + z[131] + 4*z[132] + 2*
      z[133];
      r+=  - n<T>(101,3)*z[134] - n<T>(257,3)*z[135] - n<T>(25,8)*z[136] + n<T>(2387,24)*z[137]
     - n<T>(1,3)*z[138]
     + n<T>(358,3)*z[139]
     + n<T>(269,3)*z[140]
     +  z[141];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf381(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf381(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
