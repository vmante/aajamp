#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf28(
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[7];
  std::complex<T> r(0,0);

    z[1]=d[1];
    z[2]=d[8];
    z[3]=d[9];
    z[4]=npow(z[1],2);
    z[5]=2*z[2];
    z[6]=z[1]*z[5];
    z[5]= - z[5] + z[3];
    z[5]=z[3]*z[5];

    r +=  - z[4] + z[5] + z[6];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf28(
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf28(
  const std::array<std::complex<dd_real>,25>& d
);
#endif
