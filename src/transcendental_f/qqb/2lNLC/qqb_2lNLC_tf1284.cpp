#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1284(
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=d[1];
    z[2]=d[6];
    z[3]=d[9];
    z[4]=d[4];
    z[5]=d[5];
    z[6]=d[8];
    z[7]=e[6];
    z[8]=z[2] - z[3];
    z[8]=z[1]*z[8];
    z[9]=z[3] - z[5];
    z[9]=z[6]*z[9];
    z[8]=z[7] + z[8] + z[9];
    z[9]= - z[3]*z[2];
    z[10]=z[2] + z[3];
    z[10]=z[5]*z[10];
    z[11]= - 2*z[2] + z[4];
    z[11]=z[4]*z[11];

    r += 2*z[8] + z[9] + z[10] + z[11];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1284(
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1284(
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
