#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf918(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[46];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[5];
    z[5]=d[9];
    z[6]=c[12];
    z[7]=c[13];
    z[8]=c[20];
    z[9]=c[23];
    z[10]=c[32];
    z[11]=c[33];
    z[12]=c[35];
    z[13]=c[36];
    z[14]=c[37];
    z[15]=c[38];
    z[16]=c[39];
    z[17]=c[40];
    z[18]=c[43];
    z[19]=c[44];
    z[20]=c[47];
    z[21]=c[48];
    z[22]=c[49];
    z[23]=c[50];
    z[24]=c[55];
    z[25]=c[56];
    z[26]=c[57];
    z[27]=c[59];
    z[28]=c[81];
    z[29]=c[84];
    z[30]=f[62];
    z[31]=g[131];
    z[32]=g[138];
    z[33]=g[147];
    z[34]=g[212];
    z[35]=g[214];
    z[36]=g[218];
    z[37]=g[220];
    z[38]=g[232];
    z[39]=g[233];
    z[40]=g[234];
    z[41]=n<T>(3,20)*z[9] - 3*z[8] - n<T>(36,5)*z[7] + n<T>(11,180)*z[1];
    z[41]=z[41]*i;
    z[41]=z[41] - n<T>(3,2)*z[30] + z[6];
    z[42]= - z[5] + z[4] + z[3] - z[2];
    z[41]=z[42]*z[41];
    z[42]=z[36] - z[35];
    z[43]=z[37] + z[34];
    z[44]=z[39] - z[33];
    z[45]= - n<T>(4,3)*z[29] + n<T>(65,54)*z[28] + n<T>(37,180)*z[27] - n<T>(1,2)*z[26]
    - 3*z[25] + 4*z[23] - n<T>(36,5)*z[22] - n<T>(19,180)*z[20] - n<T>(10,3)*z[19]
    + n<T>(109,486)*z[16] + n<T>(242,27)*z[15] - 8*z[14] - n<T>(5,27)*z[11];
    z[45]=i*z[45];

    r += n<T>(131,270)*z[10] - n<T>(24,5)*z[12] - 8*z[13] + n<T>(20,3)*z[17] + 2*
      z[18] - z[21] + n<T>(1,10)*z[24] - 3*z[31] + n<T>(3,2)*z[32] + n<T>(1,4)*
      z[38] + z[40] + z[41] - n<T>(9,4)*z[42] + n<T>(3,4)*z[43] + n<T>(1,2)*z[44]
       + z[45];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf918(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf918(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
