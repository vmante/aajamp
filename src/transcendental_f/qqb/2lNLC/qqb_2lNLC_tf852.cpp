#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf852(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[69];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[16];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[4];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=e[10];
    z[21]=c[3];
    z[22]=d[9];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[3];
    z[32]=e[14];
    z[33]=e[6];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[11];
    z[40]=f[12];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[18];
    z[44]=f[31];
    z[45]=f[36];
    z[46]=f[39];
    z[47]=f[51];
    z[48]=f[55];
    z[49]=f[58];
    z[50]=z[1]*i;
    z[51]=156329*z[50] - 177557*z[5];
    z[51]=z[5]*z[51];
    z[52]=z[50] - z[5];
    z[53]= - 156329*z[52] - 174701*z[7];
    z[53]=z[7]*z[53];
    z[51]=z[51] + z[53];
    z[53]=2*z[50];
    z[54]=z[53] - z[6];
    z[55]=1769*z[6];
    z[54]=z[54]*z[55];
    z[55]=n<T>(179191,6)*z[3] - n<T>(23917,4)*z[2] - n<T>(153473,12)*z[7] - n<T>(116729,12)*z[5]
     - n<T>(33103,6)*z[50]
     + z[55];
    z[55]=z[3]*z[55];
    z[56]=npow(z[9],2);
    z[57]=z[2]*z[52];
    z[51]=z[55] - n<T>(23917,2)*z[57] - 13676*z[56] + z[54] + n<T>(1,6)*z[51];
    z[51]=z[3]*z[51];
    z[54]=n<T>(1,2)*z[6];
    z[55]=z[54] - z[50];
    z[57]=z[55]*z[6];
    z[58]= - z[50] + n<T>(1,2)*z[5];
    z[59]=z[58]*z[5];
    z[60]=n<T>(20443,4)*z[5] - n<T>(118375,4)*z[50] + 24483*z[6];
    z[60]=n<T>(1,2)*z[60] - n<T>(33238,3)*z[9];
    z[60]=z[9]*z[60];
    z[60]=z[60] - 6407*z[57] + n<T>(103117,4)*z[59];
    z[60]=z[9]*z[60];
    z[61]=18076*z[50] - n<T>(24483,2)*z[6];
    z[61]=z[6]*z[61];
    z[62]=z[50] - z[6];
    z[63]= - 2*z[62] - z[9];
    z[63]=z[9]*z[63];
    z[64]= - z[6] - z[50] + z[12];
    z[64]=z[12]*z[64];
    z[61]=n<T>(6407,2)*z[64] + n<T>(11669,3)*z[21] + z[61] + 9038*z[63];
    z[61]=z[12]*z[61];
    z[63]= - n<T>(156097,3)*z[9] - 47881*z[6] - n<T>(862571,18)*z[5];
    z[63]= - 4400*z[10] - n<T>(12145,6)*z[3] + n<T>(127922,9)*z[2] + n<T>(57805,36)*
    z[4] + n<T>(1,4)*z[63] - n<T>(100261,9)*z[7];
    z[63]=z[21]*z[63];
    z[64]=n<T>(132245,6)*z[3] - 3538*z[62] + n<T>(193073,6)*z[7];
    z[64]=z[20]*z[64];
    z[51]=z[64] + z[60] + z[51] + z[63] + z[61];
    z[60]=10897*z[50] + n<T>(11965,2)*z[6];
    z[60]=z[6]*z[60];
    z[61]=n<T>(14461,3)*z[5] + n<T>(114953,6)*z[50] - 21241*z[6];
    z[61]=z[5]*z[61];
    z[60]=z[60] + z[61];
    z[61]=n<T>(1,2)*z[7];
    z[63]= - z[61] - z[62];
    z[63]=z[7]*z[63];
    z[64]=21061*z[4] - 42122*z[50] - 11875*z[5];
    z[64]=z[4]*z[64];
    z[65]=n<T>(7577,6)*z[2] + n<T>(11875,3)*z[4] - n<T>(21601,6)*z[5] - n<T>(7577,3)*
    z[50] - 1621*z[6];
    z[65]=z[2]*z[65];
    z[56]=n<T>(1,3062)*z[65] + n<T>(1,4593)*z[64] + 5*z[63] + n<T>(1,1531)*z[60]
     - 2
   *z[56];
    z[56]=z[2]*z[56];
    z[60]=17999*z[6];
    z[55]= - z[55]*z[60];
    z[60]= - n<T>(30247,6)*z[5] + n<T>(30247,3)*z[50] + z[60];
    z[60]=z[5]*z[60];
    z[55]=z[55] + z[60];
    z[60]=z[61] + z[50];
    z[61]= - z[4] - n<T>(30247,18372)*z[5] - n<T>(17999,6124)*z[6] + z[60];
    z[61]=z[4]*z[61];
    z[63]=z[50] - z[9];
    z[64]=z[9]*z[63];
    z[65]= - z[50] + z[7];
    z[65]=z[7]*z[65];
    z[55]=z[61] + z[65] + n<T>(1,3062)*z[55] - 2*z[64];
    z[55]=z[4]*z[55];
    z[61]=n<T>(1,2)*z[9] + z[62];
    z[61]=z[9]*z[61];
    z[61]=z[57] + z[61];
    z[60]=z[7]*z[60];
    z[64]=z[9] - z[6];
    z[64]=14731*z[50] + 11669*z[64];
    z[64]= - z[4] + n<T>(1,3062)*z[64] + z[7];
    z[64]=z[10]*z[64];
    z[65]=n<T>(1,2)*z[4] - z[50] - z[7];
    z[65]=z[4]*z[65];
    z[60]=z[64] + z[65] + n<T>(11669,1531)*z[61] + z[60];
    z[60]=z[10]*z[60];
    z[61]=z[4] + z[9];
    z[61]=z[50]*z[61];
    z[61]=z[21] + z[61];
    z[61]=z[11]*z[61];
    z[63]= - z[4] + z[63];
    z[63]=z[17]*z[63];
    z[64]= - n<T>(1769,4593)*z[21] - z[32];
    z[64]=z[22]*z[64];
    z[61]=z[61] + z[63] + z[64] + z[35];
    z[63]=n<T>(116729,2)*z[3];
    z[64]=63517*z[5] + z[63];
    z[64]=z[50]*z[64];
    z[64]=63517*z[21] + z[64];
    z[64]=z[8]*z[64];
    z[63]= - 63517*z[52] + z[63];
    z[63]=z[16]*z[63];
    z[63]=z[64] + z[63];
    z[64]=npow(z[6],2);
    z[52]=103117*z[52] + n<T>(88965,2)*z[9];
    z[52]=z[9]*z[52];
    z[52]=n<T>(1,3062)*z[52] - 5*z[64] - n<T>(3307,9186)*z[59];
    z[59]= - 371081*z[50] + 426197*z[5];
    z[59]=n<T>(1,3)*z[59] + 103117*z[9];
    z[59]=n<T>(1,12248)*z[59] - n<T>(2,3)*z[7];
    z[59]=z[7]*z[59];
    z[52]=n<T>(1,2)*z[52] + z[59];
    z[52]=z[7]*z[52];
    z[59]=z[12] + z[6];
    z[59]=z[50]*z[59];
    z[59]=z[21] + z[59];
    z[59]=z[13]*z[59];
    z[62]=z[12] - z[62];
    z[62]=z[19]*z[62];
    z[59]=z[59] + z[62];
    z[54]= - 3*z[50] + z[54];
    z[54]=z[54]*z[64];
    z[50]=n<T>(11,6)*z[5] - 11*z[50] + n<T>(24483,6124)*z[6];
    z[50]=z[5]*z[50];
    z[50]=n<T>(24483,3062)*z[57] + z[50];
    z[50]=z[5]*z[50];
    z[57]=z[38] - z[27];
    z[62]=z[46] - z[44];
    z[64]= - n<T>(29167,4593)*z[28] + n<T>(7577,9186)*z[26] + n<T>(18388,13779)*
    z[23];
    z[64]=i*z[64];
    z[53]=z[53] - z[2];
    z[65]= - z[4] + z[53];
    z[65]=z[14]*z[65];
    z[58]=n<T>(1,2)*z[2] + z[58];
    z[58]=z[15]*z[58];
    z[53]=z[6] - z[53];
    z[53]=z[18]*z[53];
    z[66]= - 3*z[7] + z[3];
    z[66]=z[32]*z[66];
    z[67]=z[9] + z[3];
    z[67]=z[31]*z[67];
    z[68]= - z[9] - z[12];
    z[68]=z[33]*z[68];

    r +=  - n<T>(357391,18372)*z[24] + n<T>(228041,110232)*z[25] + n<T>(29167,9186)
      *z[29] + n<T>(839821,12248)*z[30] - 4*z[34] - n<T>(30247,18372)*z[36] + 
      n<T>(426197,36744)*z[37] + n<T>(24483,6124)*z[39] - n<T>(20443,12248)*z[40]
       + n<T>(88965,12248)*z[41] - n<T>(17999,6124)*z[42] - n<T>(4,3)*z[43] - n<T>(24483,3062)*z[45]
     - z[47]
     - z[48]
     + z[49]
     + z[50]
     + n<T>(1,1531)*z[51]
     +  z[52] + n<T>(1621,1531)*z[53] + n<T>(5069,1531)*z[54] + z[55] + z[56] + 
      n<T>(7577,18372)*z[57] + n<T>(21601,4593)*z[58] + n<T>(6407,1531)*z[59] + 
      z[60] + 2*z[61] + n<T>(11669,3062)*z[62] + n<T>(1,4593)*z[63] + z[64] + 
      n<T>(30247,4593)*z[65] + z[66] + n<T>(13676,1531)*z[67] + n<T>(18076,1531)*
      z[68];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf852(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf852(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
