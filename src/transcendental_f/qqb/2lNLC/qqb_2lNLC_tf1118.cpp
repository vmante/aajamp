#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1118(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[19];
    z[11]=c[23];
    z[12]=c[25];
    z[13]=c[26];
    z[14]=c[28];
    z[15]=c[31];
    z[16]=f[3];
    z[17]=f[11];
    z[18]=f[13];
    z[19]=f[17];
    z[20]=2*z[4];
    z[21]=z[2] + z[4];
    z[21]=z[21]*z[20];
    z[22]= - 13*z[4] + 19*z[2];
    z[22]=z[22]*z[3];
    z[23]=npow(z[2],2);
    z[21]= - n<T>(1,2)*z[22] - z[23] + z[21];
    z[21]=z[3]*z[21];
    z[24]=z[4]*z[2];
    z[24]=2*z[23] - 5*z[24];
    z[24]=z[4]*z[24];
    z[25]= - n<T>(19,8)*z[3] - n<T>(5,2)*z[2] - z[20];
    z[25]=n<T>(1,3)*z[25] + n<T>(5,8)*z[5];
    z[25]=z[9]*z[25];
    z[26]=z[1]*i;
    z[27]= - 4*z[26] - z[2] + 5*z[4];
    z[27]=z[7]*z[27];
    z[21]=z[24] + z[21] + z[25] + z[27];
    z[24]=npow(z[4],2);
    z[25]= - z[20] + z[3];
    z[25]=z[3]*z[25];
    z[27]= - z[2] + 3*z[4];
    z[28]= - n<T>(1,2)*z[27] + z[3];
    z[28]=z[5]*z[28];
    z[24]=z[28] + z[24] + z[25];
    z[24]=z[5]*z[24];
    z[20]=z[2]*z[20];
    z[20]= - 5*z[23] + z[20];
    z[20]=2*z[20] + z[22];
    z[22]= - 2*z[3] + z[27];
    z[22]=z[5]*z[22];
    z[20]=n<T>(1,3)*z[20] + z[22];
    z[20]=z[20]*z[26];
    z[22]=z[2] + z[3];
    z[22]=n<T>(1,2)*z[22] - z[26];
    z[22]=z[6]*z[22];
    z[23]=z[15] + z[14];
    z[25]=z[18] - z[12];
    z[27]=n<T>(20,3)*z[13] + n<T>(10,3)*z[11];
    z[27]=i*z[27];
    z[28]=npow(z[2],3);
    z[26]=z[26] - n<T>(1,2)*z[5] - n<T>(3,2)*z[2] + z[4];
    z[26]=z[8]*z[26];

    r += n<T>(5,18)*z[10] + n<T>(13,6)*z[16] - n<T>(3,2)*z[17] + z[19] + z[20] + n<T>(1,3)*z[21]
     + n<T>(19,3)*z[22] - n<T>(10,3)*z[23]
     + z[24]
     + n<T>(5,3)*z[25]
     +  z[26] + z[27] + z[28];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1118(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1118(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
