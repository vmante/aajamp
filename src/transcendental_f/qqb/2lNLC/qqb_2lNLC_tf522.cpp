#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf522(
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[9];
  std::complex<T> r(0,0);

    z[1]=d[4];
    z[2]=d[6];
    z[3]=d[9];
    z[4]=d[5];
    z[5]=d[8];
    z[6]= - z[2] + z[3];
    z[6]=z[1]*z[6];
    z[7]=z[2] - n<T>(1,2)*z[4];
    z[7]=z[4]*z[7];
    z[8]= - z[3] + n<T>(1,2)*z[5];
    z[8]=z[5]*z[8];

    r += z[6] + z[7] + z[8];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf522(
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf522(
  const std::array<std::complex<dd_real>,25>& d
);
#endif
