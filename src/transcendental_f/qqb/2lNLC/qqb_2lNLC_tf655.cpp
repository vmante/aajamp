#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf655(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[78];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=e[7];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[36];
    z[50]=f[39];
    z[51]=f[51];
    z[52]=f[55];
    z[53]=f[58];
    z[54]=n<T>(1,2)*z[6];
    z[55]=z[1]*i;
    z[56]=z[54] - z[55];
    z[56]=z[56]*z[6];
    z[57]=z[55] - z[4];
    z[58]=n<T>(1,2)*z[7];
    z[59]= - z[58] - z[57];
    z[59]=z[7]*z[59];
    z[60]=n<T>(1,2)*z[10];
    z[61]= - 65629*z[55] + n<T>(99107,2)*z[6];
    z[61]= - n<T>(19,3)*z[10] - n<T>(15,2)*z[7] + n<T>(19,2)*z[4] + n<T>(1,1531)*z[61]
    + z[5];
    z[61]=z[61]*z[60];
    z[62]=z[55] + z[6];
    z[62]=z[5]*z[62];
    z[63]=n<T>(1,2)*z[2];
    z[64]=z[63] - z[6];
    z[65]= - z[5] + z[64];
    z[65]=z[2]*z[65];
    z[66]=n<T>(11,2)*z[55];
    z[67]= - n<T>(11,4)*z[4] + z[2] + z[66] - z[5];
    z[67]=z[4]*z[67];
    z[59]=z[61] + n<T>(11,2)*z[59] + z[67] + z[65] - n<T>(68487,3062)*z[56] + 
    z[62];
    z[59]=z[59]*z[60];
    z[61]=1009133*z[55] - 1027505*z[5];
    z[61]= - 21*z[4] + n<T>(1,9186)*z[61] + 23*z[2];
    z[61]=n<T>(1,2)*z[61] + n<T>(17,3)*z[7];
    z[61]=z[61]*z[58];
    z[62]=npow(z[6],2);
    z[65]=n<T>(12961,36744)*z[5] - n<T>(12961,18372)*z[55] - z[6];
    z[65]=z[5]*z[65];
    z[67]=21*z[55] - 23*z[6];
    z[67]=n<T>(1,2)*z[67] + z[5];
    z[67]=z[2]*z[67];
    z[66]= - n<T>(9,4)*z[4] + z[66] - z[6];
    z[66]=z[4]*z[66];
    z[61]=z[61] + z[66] + z[67] + n<T>(27,4)*z[62] + z[65];
    z[61]=z[61]*z[58];
    z[65]=n<T>(1,2)*z[5];
    z[66]=z[65] - z[55];
    z[67]=z[5]*z[66];
    z[67]=n<T>(138301,3)*z[56] - n<T>(312427,2)*z[67];
    z[68]=z[55] - z[5];
    z[58]= - z[58] - z[68];
    z[58]=z[7]*z[58];
    z[69]=z[55] - z[6];
    z[60]= - z[60] - z[69];
    z[60]=z[10]*z[60];
    z[70]=z[4] - z[2];
    z[71]=n<T>(1414549,2)*z[55] - 500435*z[6];
    z[71]=n<T>(1,3)*z[71] - n<T>(137893,2)*z[5];
    z[70]=n<T>(65459,6124)*z[9] - n<T>(68487,12248)*z[10] - n<T>(257719,24496)*z[7]
    + n<T>(1,12248)*z[71] - 2*z[70];
    z[70]=z[9]*z[70];
    z[71]=z[55]*z[4];
    z[58]=z[70] + n<T>(68487,6124)*z[60] + n<T>(312427,12248)*z[58] + n<T>(1,6124)*
    z[67] + 2*z[71];
    z[58]=z[9]*z[58];
    z[60]= - z[64] + n<T>(80939,3062)*z[68];
    z[60]=z[2]*z[60];
    z[64]=n<T>(566369,9186)*z[5] - n<T>(475121,9186)*z[55] - z[6];
    z[64]=z[5]*z[64];
    z[67]=484307*z[55] - 475121*z[5];
    z[67]=n<T>(258229,4593)*z[7] + n<T>(1,9186)*z[67] - z[2];
    z[67]=z[7]*z[67];
    z[70]=npow(z[9],2);
    z[60]=n<T>(105027,3062)*z[70] + z[67] + z[60] + n<T>(13677,1531)*z[56] + 
    z[64];
    z[64]=n<T>(175861,3)*z[7] + n<T>(84001,2)*z[2] + n<T>(157489,3)*z[5] + n<T>(145037,6)
   *z[55] - 13677*z[6];
    z[64]= - n<T>(502373,18372)*z[3] + n<T>(1,6124)*z[64] - z[9];
    z[64]=z[3]*z[64];
    z[60]=n<T>(1,2)*z[60] + z[64];
    z[60]=z[3]*z[60];
    z[64]=z[6] - z[5];
    z[64]=z[64]*z[63];
    z[67]=n<T>(1,2)*z[4];
    z[70]= - z[4] + z[5] + z[6];
    z[70]=z[70]*z[67];
    z[72]=z[55]*z[6];
    z[73]=npow(z[10],2);
    z[74]= - z[6] + z[10];
    z[74]=z[13]*z[74];
    z[64]=z[74] - n<T>(1,2)*z[73] + z[70] - z[72] + z[64];
    z[64]=z[15]*z[64];
    z[70]=157489*z[3];
    z[74]= - 202807*z[5] - z[70];
    z[74]=z[55]*z[74];
    z[74]= - 202807*z[24] + z[74];
    z[74]=z[8]*z[74];
    z[68]=202807*z[68] - z[70];
    z[68]=z[18]*z[68];
    z[70]=171881*z[9] + 181067*z[13];
    z[70]=z[34]*z[70];
    z[68]=z[70] + z[74] + z[68];
    z[70]=z[71] + z[24];
    z[71]=z[11]*z[70];
    z[74]=z[23]*z[57];
    z[75]= - z[10] - z[15];
    z[75]=z[36]*z[75];
    z[71]= - z[75] + z[71] + z[74];
    z[74]= - 6703*z[55] - n<T>(124351,2)*z[6];
    z[54]=z[74]*z[54];
    z[74]= - n<T>(137279,6)*z[5] - n<T>(308141,6)*z[55] + 71889*z[6];
    z[74]=z[5]*z[74];
    z[75]=n<T>(170143,12)*z[2] + n<T>(28069,12)*z[5] - n<T>(170143,6)*z[55] - 3181*
    z[6];
    z[75]=z[2]*z[75];
    z[54]=z[75] + z[54] + z[74];
    z[54]=z[2]*z[54];
    z[74]=n<T>(121357,6)*z[5] - n<T>(130543,3)*z[55] - 84613*z[6];
    z[74]=z[5]*z[74];
    z[74]=87675*z[56] + z[74];
    z[75]= - 20005*z[2] + n<T>(201377,2)*z[55] + 30824*z[5];
    z[75]=z[2]*z[75];
    z[74]=n<T>(1,4)*z[74] + n<T>(1,3)*z[75];
    z[75]= - n<T>(183005,4593)*z[2] + n<T>(139729,9186)*z[5] - 11*z[55] + n<T>(84613,3062)*z[6];
    z[75]=n<T>(1,2)*z[75] + 3*z[4];
    z[67]=z[75]*z[67];
    z[67]=n<T>(1,1531)*z[74] + z[67];
    z[67]=z[4]*z[67];
    z[74]= - 181067*z[55] + n<T>(454505,4)*z[6];
    z[74]=z[6]*z[74];
    z[65]=z[6] - z[65];
    z[65]=z[5]*z[65];
    z[75]=138301*z[55] + 101557*z[6];
    z[75]= - n<T>(107681,18372)*z[13] + n<T>(1,18372)*z[75] + z[10];
    z[75]=z[13]*z[75];
    z[65]=z[75] + n<T>(1,4593)*z[74] + z[65];
    z[74]=n<T>(1,2)*z[9] + z[69];
    z[74]=z[9]*z[74];
    z[65]=n<T>(171881,9186)*z[74] - z[73] + n<T>(1,2)*z[65];
    z[65]=z[13]*z[65];
    z[73]= - z[9]*z[55];
    z[70]=z[73] - z[70];
    z[70]=z[12]*z[70];
    z[57]=z[9] - z[57];
    z[57]=z[19]*z[57];
    z[57]= - z[39] + z[70] + z[57];
    z[70]=1470481*z[6] + n<T>(2756105,2)*z[5];
    z[70]=n<T>(1,2)*z[70] - 863995*z[2];
    z[70]=n<T>(502067,2)*z[7] + n<T>(1,2)*z[70] - 84307*z[4];
    z[70]=n<T>(7587,4)*z[15] - n<T>(44195,6)*z[13] + n<T>(117275,24)*z[3] + n<T>(1542743,72)*z[9]
     + n<T>(1,18)*z[70]
     + 6090*z[10];
    z[70]=z[24]*z[70];
    z[73]=13677*z[69] - n<T>(530237,6)*z[7];
    z[73]=n<T>(1,2)*z[73] - n<T>(72008,3)*z[3];
    z[73]=z[22]*z[73];
    z[70]=z[70] + z[73];
    z[73]=z[55] - n<T>(1,6)*z[6];
    z[62]=z[73]*z[62];
    z[73]= - 3*z[5] + 18*z[55] - n<T>(59165,12248)*z[6];
    z[73]=z[5]*z[73];
    z[56]= - n<T>(62227,6124)*z[56] + z[73];
    z[56]=z[5]*z[56];
    z[73]=119929*z[13];
    z[74]= - z[55]*z[73];
    z[72]= - 101557*z[72] + z[74];
    z[74]=z[15]*z[55];
    z[72]= - n<T>(101557,18372)*z[24] + n<T>(1,18372)*z[72] + z[74];
    z[72]=z[14]*z[72];
    z[69]=101557*z[69] - z[73];
    z[69]=n<T>(1,18372)*z[69] + z[15];
    z[69]=z[21]*z[69];
    z[63]= - z[63] - z[66];
    z[63]=z[17]*z[63];
    z[66]=z[42] - z[29];
    z[73]=n<T>(243631,9186)*z[30] + n<T>(170143,18372)*z[28] - n<T>(51249,24496)*
    z[25];
    z[73]=i*z[73];
    z[55]= - z[2] + 2*z[55];
    z[74]=z[4] - z[55];
    z[74]=z[16]*z[74];
    z[55]=z[6] - z[55];
    z[55]=z[20]*z[55];
    z[75]= - 101965*z[9] - 95841*z[3];
    z[75]=z[33]*z[75];
    z[76]= - z[10] - z[13];
    z[76]=z[35]*z[76];
    z[77]=19*z[7] + 17*z[15];
    z[77]=z[37]*z[77];

    r += n<T>(83865,3062)*z[26] - n<T>(270785,220464)*z[27] - n<T>(243631,18372)*
      z[31] - n<T>(3894979,36744)*z[32] + 4*z[38] + n<T>(130543,36744)*z[40] - 
      n<T>(1027505,73488)*z[41] - n<T>(62227,12248)*z[43] + n<T>(137893,24496)*
      z[44] - n<T>(257719,24496)*z[45] + n<T>(87675,12248)*z[46] + n<T>(4,3)*z[47]
       + n<T>(99107,12248)*z[48] + n<T>(500435,36744)*z[49] - n<T>(68487,12248)*
      z[50] + n<T>(19,8)*z[51] + n<T>(21,8)*z[52] - n<T>(15,8)*z[53] + n<T>(1,3062)*
      z[54] + n<T>(3181,1531)*z[55] + z[56] + 2*z[57] + z[58] + z[59] + 
      z[60] + z[61] + n<T>(98903,6124)*z[62] + n<T>(28069,9186)*z[63] + z[64]
       + z[65] + n<T>(170143,36744)*z[66] + z[67] + n<T>(1,9186)*z[68] + z[69]
       + n<T>(1,1531)*z[70] - n<T>(1,2)*z[71] + z[72] + z[73] + n<T>(58382,4593)*
      z[74] + n<T>(1,6124)*z[75] + n<T>(5,2)*z[76] + n<T>(1,4)*z[77];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf655(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf655(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
