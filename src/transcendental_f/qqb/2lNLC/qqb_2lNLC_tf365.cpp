#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf365(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[9];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[16];
    z[13]=d[6];
    z[14]=d[17];
    z[15]=d[11];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[7];
    z[21]=e[8];
    z[22]=e[9];
    z[23]=e[10];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=e[13];
    z[36]=e[14];
    z[37]=e[12];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[18];
    z[47]=f[20];
    z[48]=f[23];
    z[49]=f[31];
    z[50]=f[36];
    z[51]=f[37];
    z[52]=f[39];
    z[53]=f[51];
    z[54]=f[55];
    z[55]=f[58];
    z[56]=n<T>(1,2)*z[3];
    z[57]=z[9] + n<T>(7681,180)*z[3];
    z[57]=z[57]*z[56];
    z[58]=n<T>(1,2)*z[9];
    z[59]=3*z[6];
    z[60]=n<T>(1057,1080)*z[2] + n<T>(6,5)*z[3] + z[59] - z[58] + z[4];
    z[60]=z[2]*z[60];
    z[61]=z[13] + n<T>(1,4)*z[9];
    z[62]=n<T>(1,2)*z[4];
    z[63]=n<T>(17,270)*z[5] + n<T>(1817,240)*z[8] + n<T>(191,540)*z[7] - n<T>(23,540)*
    z[2] - n<T>(2159,60)*z[3] + n<T>(2173,80)*z[11] - z[62] + z[61];
    z[63]=z[5]*z[63];
    z[64]=npow(z[6],2);
    z[65]=n<T>(3,2)*z[64];
    z[66]=npow(z[9],2);
    z[67]=npow(z[13],2);
    z[68]=npow(z[4],2);
    z[69]=518*z[18] + n<T>(23,36)*z[17];
    z[69]=n<T>(1,5)*z[69] + n<T>(4133,432)*z[24];
    z[70]=z[13] - n<T>(74701,720)*z[11];
    z[70]=z[11]*z[70];
    z[71]= - n<T>(433,135)*z[7] - n<T>(191,270)*z[2] - 2*z[13] - n<T>(99,20)*z[3];
    z[71]=z[7]*z[71];
    z[72]=n<T>(73631,36)*z[8] + 139*z[7] - 99*z[2] - n<T>(2173,2)*z[11] + 717*
    z[3];
    z[72]=z[8]*z[72];
    z[57]=z[63] + n<T>(1,20)*z[72] + z[71] + z[60] + z[57] + z[70] - z[65]
    + n<T>(11,4)*z[68] - n<T>(1,4)*z[66] + n<T>(1,3)*z[69] - n<T>(1,2)*z[67];
    z[57]=z[5]*z[57];
    z[60]=n<T>(1169,540)*z[2];
    z[63]=z[9] + 11*z[4];
    z[63]=z[60] - n<T>(701,90)*z[3] + n<T>(1,2)*z[63] - z[59];
    z[63]=z[2]*z[63];
    z[69]= - n<T>(529,45)*z[11] + n<T>(1009,18)*z[6];
    z[70]=n<T>(878,3)*z[14] + n<T>(547,2)*z[13];
    z[70]= - n<T>(2407,270)*z[2] + n<T>(86,9)*z[3] + n<T>(1,15)*z[70] - z[58] - 
    z[69];
    z[70]=z[7]*z[70];
    z[71]=n<T>(518,15)*z[10];
    z[60]= - n<T>(2057,120)*z[8] + n<T>(866,135)*z[7] - z[60] + n<T>(2471,60)*z[3]
    - n<T>(2173,40)*z[11] - z[59] + z[71] - n<T>(11,2)*z[4];
    z[60]=z[5]*z[60];
    z[72]=n<T>(1,10)*z[4] + 8*z[6];
    z[59]=z[72]*z[59];
    z[72]= - z[12] - z[62];
    z[72]=z[4]*z[72];
    z[72]=z[72] - z[19];
    z[73]=n<T>(433,3)*z[21];
    z[74]= - 439*z[22] + z[73];
    z[75]=z[13]*z[14];
    z[61]= - z[15] + z[61];
    z[61]=z[9]*z[61];
    z[76]=z[4] - z[12];
    z[76]=n<T>(28213,720)*z[11] + n<T>(5281,90)*z[6] - z[13] + 2*z[76];
    z[76]=z[11]*z[76];
    z[77]= - 2467*z[10] + n<T>(4367,3)*z[3];
    z[77]=z[3]*z[77];
    z[78]=z[6] + z[4];
    z[78]= - n<T>(23573,120)*z[8] + n<T>(2527,90)*z[2] - n<T>(4237,45)*z[3] + n<T>(2173,20)*z[11]
     - z[9]
     + n<T>(7,5)*z[78];
    z[78]=z[8]*z[78];
    z[59]=z[60] + n<T>(1,2)*z[78] + z[70] + z[63] + n<T>(1,60)*z[77] + z[76] + 
    z[59] + z[61] - n<T>(517,30)*z[75] - n<T>(49,90)*z[23] + n<T>(62,45)*z[20] - 8*
    z[16] + n<T>(247,270)*z[17] - n<T>(518,15)*z[18] + n<T>(2,45)*z[74] + n<T>(3,2)*
    z[72];
    z[59]=z[1]*z[59];
    z[60]= - n<T>(5417,2)*z[25] + 181*z[30] - n<T>(1169,2)*z[28];
    z[59]=n<T>(1,270)*z[60] + z[59];
    z[59]=i*z[59];
    z[60]=z[9]*z[13];
    z[61]=npow(z[3],2);
    z[63]=npow(z[11],2);
    z[70]= - n<T>(39439,216)*z[24] + n<T>(53,2)*z[36] + n<T>(5653,9)*z[23];
    z[72]=n<T>(17,20)*z[6];
    z[74]=z[72] - z[13] - n<T>(7,10)*z[4];
    z[74]=z[6]*z[74];
    z[75]= - n<T>(59,36)*z[2] + z[9] + n<T>(89,10)*z[3];
    z[75]=z[2]*z[75];
    z[76]= - n<T>(149,20)*z[2] + z[62] + n<T>(61,9)*z[3];
    z[76]=z[7]*z[76];
    z[72]=n<T>(1,9)*z[8] - n<T>(7,4)*z[7] + n<T>(523,180)*z[2] - n<T>(11239,90)*z[3]
     + 
   n<T>(6719,240)*z[11] + z[72] - n<T>(3,4)*z[9] - n<T>(26,5)*z[4];
    z[72]=z[8]*z[72];
    z[70]=z[72] + z[76] + n<T>(1,2)*z[75] - n<T>(215,9)*z[61] + n<T>(5471,240)*z[63]
    + z[74] - n<T>(3,5)*z[68] + n<T>(1,5)*z[70] + z[60];
    z[70]=z[8]*z[70];
    z[72]= - 62*z[20] + 878*z[22] - z[73];
    z[72]=n<T>(20471,360)*z[24] + n<T>(1,5)*z[72] + n<T>(179,2)*z[23];
    z[72]=n<T>(1,3)*z[72] + n<T>(266,5)*z[67];
    z[62]= - z[9]*z[62];
    z[73]= - n<T>(5281,3)*z[6] + 1937*z[11];
    z[73]=z[11]*z[73];
    z[58]= - z[3]*z[58];
    z[74]=n<T>(99,2)*z[3] + n<T>(433,27)*z[2];
    z[74]=z[2]*z[74];
    z[69]= - n<T>(3217,90)*z[13] + z[9] + z[69];
    z[69]=n<T>(433,135)*z[2] + n<T>(1,2)*z[69] - n<T>(43,9)*z[3];
    z[69]=z[7]*z[69];
    z[58]=z[69] + n<T>(1,10)*z[74] + z[58] + n<T>(1,30)*z[73] - n<T>(269,10)*z[64]
    + n<T>(1,3)*z[72] + z[62];
    z[58]=z[7]*z[58];
    z[62]= - 433*z[21] - n<T>(517,4)*z[17];
    z[69]=59*z[3] - n<T>(1709,15)*z[2];
    z[69]=z[2]*z[69];
    z[61]=n<T>(1,72)*z[69] - n<T>(1613,360)*z[61] + n<T>(5,4)*z[63] - z[65] - n<T>(13,4)
   *z[68] - n<T>(1373,3240)*z[24] - n<T>(47,5)*z[23] + n<T>(1,135)*z[62] + n<T>(7,2)*
    z[16];
    z[61]=z[2]*z[61];
    z[62]=z[19] + 3*z[16];
    z[62]=3*z[62] - n<T>(43,30)*z[24];
    z[65]=3*z[9] + n<T>(11,3)*z[4];
    z[65]=z[4]*z[65];
    z[60]=n<T>(1,4)*z[65] + n<T>(1,2)*z[62] - z[60];
    z[60]=z[4]*z[60];
    z[62]= - n<T>(1574,135)*z[24] - 3*z[35] - n<T>(121,45)*z[20];
    z[65]=z[13] + n<T>(59,5)*z[4];
    z[65]=n<T>(1,2)*z[65] - n<T>(211,45)*z[6];
    z[65]=z[6]*z[65];
    z[62]=z[65] + 2*z[62] - n<T>(3,20)*z[68];
    z[62]=z[6]*z[62];
    z[65]= - 2173*z[34] - n<T>(3871,3)*z[33];
    z[68]= - n<T>(16277,135)*z[11] - 5*z[4] + n<T>(5281,45)*z[6];
    z[68]=z[11]*z[68];
    z[64]=n<T>(1,4)*z[68] + n<T>(5281,180)*z[64] - n<T>(233,27)*z[24] + n<T>(1,30)*z[65]
    + 2*z[19];
    z[64]=z[11]*z[64];
    z[65]= - n<T>(827,3)*z[34] - n<T>(517,5)*z[22];
    z[65]=n<T>(1,3)*z[65] + 9*z[20];
    z[65]= - n<T>(2897,540)*z[67] + n<T>(1,2)*z[65] - n<T>(136,27)*z[24];
    z[65]=z[13]*z[65];
    z[67]=3827*z[23] - 3487*z[33] - n<T>(2467,2)*z[18];
    z[63]=n<T>(1871,9)*z[63] + n<T>(1,3)*z[67] - n<T>(219,4)*z[24];
    z[67]=353*z[11] - n<T>(329,36)*z[3];
    z[67]=z[3]*z[67];
    z[63]=n<T>(1,5)*z[63] + n<T>(1,3)*z[67];
    z[56]=z[63]*z[56];
    z[63]=z[29] - z[42];
    z[67]=n<T>(17,2)*z[55] + 59*z[54];
    z[67]=n<T>(5281,36)*z[52] + n<T>(1,2)*z[67] + 27*z[53];
    z[68]=z[71] + n<T>(878,45)*z[14] - n<T>(3,2)*z[12];
    z[68]=z[24]*z[68];
    z[66]=n<T>(1,12)*z[66] - n<T>(103,1080)*z[24] - n<T>(13,2)*z[35] + z[37] + n<T>(24,5)
   *z[36];
    z[66]=z[9]*z[66];

    r += n<T>(317,72)*z[26] - n<T>(9809,6480)*z[27] + n<T>(359,540)*z[31] + n<T>(325397,2160)*z[32]
     + n<T>(7,2)*z[38] - n<T>(5,4)*z[39]
     + n<T>(11,4)*z[40]
     + n<T>(71971,720)*z[41] - n<T>(433,135)*z[43]
     + n<T>(74341,720)*z[44]
     + n<T>(5671,240)*
      z[45] + n<T>(5,6)*z[46] + n<T>(1,3)*z[47] + n<T>(59,36)*z[48] - n<T>(2539,90)*
      z[49] - n<T>(587,20)*z[50] - n<T>(1,2)*z[51] + z[56] + z[57] + z[58] + 
      z[59] + z[60] + z[61] + z[62] + n<T>(1709,1080)*z[63] + z[64] + z[65]
       + z[66] + n<T>(1,5)*z[67] + z[68] + z[70];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf365(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf365(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
