#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf83(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[23];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[79];
    z[3]=c[11];
    z[4]=d[7];
    z[5]=d[8];
    z[6]=c[12];
    z[7]=c[32];
    z[8]=c[33];
    z[9]=c[36];
    z[10]=c[38];
    z[11]=c[40];
    z[12]=c[43];
    z[13]=g[141];
    z[14]=g[243];
    z[15]=g[245];
    z[16]=g[251];
    z[17]=g[254];
    z[18]=g[258];
    z[19]=g[272];
    z[20]=z[5] - z[4];
    z[21]=z[1]*i;
    z[21]=z[21] + z[20];
    z[21]=z[2]*z[21];
    z[22]= - z[3]*z[20];
    z[22]=n<T>(2,27)*z[22] - n<T>(58,9)*z[10] + z[8];
    z[22]=i*z[22];
    z[20]=z[6]*z[20];
    z[20]=z[20] - z[12];
    z[20]= - z[19] - n<T>(20,3)*z[11] + 8*z[9] - n<T>(10,27)*z[7] + z[21] + 2*
    z[20] + z[22];

    r +=  - n<T>(1,3)*z[13] + 16*z[14] - 2*z[15] + 6*z[16] - n<T>(8,3)*z[17] + 
      z[18] + 8*z[20];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf83(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf83(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
