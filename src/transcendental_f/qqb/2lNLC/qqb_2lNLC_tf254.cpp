#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf254(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[44];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[12];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=e[1];
    z[11]=e[2];
    z[12]=e[8];
    z[13]=e[10];
    z[14]=c[3];
    z[15]=d[5];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[31];
    z[24]=e[3];
    z[25]=e[5];
    z[26]=f[4];
    z[27]=f[5];
    z[28]=f[11];
    z[29]=f[12];
    z[30]=f[16];
    z[31]=2*z[4];
    z[32]=z[1]*i;
    z[33]=z[31]*z[32];
    z[34]=npow(z[4],2);
    z[35]=n<T>(1,2)*z[34];
    z[36]=2*z[32];
    z[37]=n<T>(1,2)*z[2] - z[4] - z[36];
    z[37]=z[2]*z[37];
    z[38]= - z[4] + z[2];
    z[38]=z[5]*z[38];
    z[37]=z[38] + z[37] + z[35] + z[33];
    z[37]=z[5]*z[37];
    z[38]=4*z[3];
    z[39]=5*z[4] - z[38];
    z[39]=z[32]*z[39];
    z[39]=5*z[14] + z[39];
    z[39]=z[8]*z[39];
    z[40]=z[32] - z[4];
    z[38]= - 5*z[40] - z[38];
    z[38]=z[11]*z[38];
    z[36]=z[36] - z[2];
    z[41]= - z[4] + z[36];
    z[41]=z[10]*z[41];
    z[42]= - z[5] + z[36];
    z[42]=z[12]*z[42];
    z[37]=z[42] - z[28] - z[27] + z[20] + z[37] + z[39] + z[41] + z[38];
    z[38]=z[32]*z[4];
    z[39]=11*z[38];
    z[41]= - 5*z[34] + z[39];
    z[31]=z[31] - z[32];
    z[31]=n<T>(1,3)*z[31] + z[3];
    z[31]=z[3]*z[31];
    z[31]=n<T>(1,3)*z[41] + z[31];
    z[31]=z[3]*z[31];
    z[33]=z[34] - z[33];
    z[34]=n<T>(1,3)*z[2];
    z[36]=n<T>(1,2)*z[4] + z[36];
    z[36]=z[36]*z[34];
    z[41]=z[40] + z[3];
    z[42]=z[3]*z[41];
    z[33]=z[36] + n<T>(1,3)*z[33] + z[42];
    z[33]=z[2]*z[33];
    z[36]= - z[35] - z[39];
    z[39]= - n<T>(5,2)*z[3] + 8*z[4] - 5*z[32];
    z[39]=z[3]*z[39];
    z[42]=n<T>(1,4)*z[40];
    z[43]=z[42] - 5*z[3];
    z[43]=z[6]*z[43];
    z[36]=z[43] + n<T>(1,2)*z[36] + z[39];
    z[39]= - z[2]*z[41];
    z[36]=z[39] + n<T>(1,3)*z[36];
    z[36]=z[6]*z[36];
    z[39]=n<T>(1,2)*z[6] + z[40];
    z[39]=z[6]*z[39];
    z[35]=z[39] + z[35] - z[38];
    z[38]=n<T>(7,4)*z[6] - z[42] - 2*z[3];
    z[38]=z[9]*z[38];
    z[35]=n<T>(7,2)*z[35] + z[38];
    z[35]=z[9]*z[35];
    z[38]=z[7]*z[32];
    z[38]= - z[25] + z[38];
    z[39]=z[6] - z[3];
    z[38]=z[39]*z[38];
    z[39]=npow(z[4],3);
    z[39]=z[39] + z[17];
    z[34]= - n<T>(8,3)*z[6] + n<T>(1,4)*z[4] - z[34];
    z[34]=n<T>(1,6)*z[15] + n<T>(1,9)*z[5] + n<T>(1,3)*z[34] - n<T>(3,2)*z[9];
    z[34]=z[14]*z[34];
    z[40]= - n<T>(4,3)*z[21] - n<T>(2,3)*z[19] + n<T>(1,18)*z[16];
    z[40]=i*z[40];
    z[32]=n<T>(5,3)*z[6] + z[2] - z[32] + n<T>(2,3)*z[3];
    z[32]=z[13]*z[32];
    z[41]=z[3] + z[9];
    z[41]=z[24]*z[41];

    r +=  - n<T>(1,18)*z[18] + n<T>(2,3)*z[22] + n<T>(17,12)*z[23] - n<T>(1,12)*z[26]
       - n<T>(1,4)*z[29] + n<T>(7,4)*z[30] + z[31] + z[32] + z[33] + z[34] + 
      z[35] + z[36] + n<T>(1,3)*z[37] + z[38] - n<T>(1,6)*z[39] + z[40] + 2*
      z[41];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf254(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf254(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
