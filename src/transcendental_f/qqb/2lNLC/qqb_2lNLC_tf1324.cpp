#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1324(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[92];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[1];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[5];
    z[22]=e[7];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[12];
    z[26]=c[3];
    z[27]=c[11];
    z[28]=c[15];
    z[29]=c[19];
    z[30]=c[23];
    z[31]=c[25];
    z[32]=c[26];
    z[33]=c[28];
    z[34]=c[31];
    z[35]=e[10];
    z[36]=e[6];
    z[37]=e[13];
    z[38]=e[14];
    z[39]=f[0];
    z[40]=f[1];
    z[41]=f[3];
    z[42]=f[4];
    z[43]=f[5];
    z[44]=f[11];
    z[45]=f[12];
    z[46]=f[14];
    z[47]=f[16];
    z[48]=f[17];
    z[49]=f[18];
    z[50]=f[30];
    z[51]=f[31];
    z[52]=f[32];
    z[53]=f[33];
    z[54]=f[35];
    z[55]=f[36];
    z[56]=f[37];
    z[57]=f[39];
    z[58]=f[43];
    z[59]=f[50];
    z[60]=f[51];
    z[61]=f[53];
    z[62]=f[54];
    z[63]=f[55];
    z[64]=f[58];
    z[65]=f[72];
    z[66]=n<T>(4,3)*z[4];
    z[67]=z[1]*i;
    z[68]=2*z[67];
    z[69]=z[68] - z[4];
    z[66]=z[66]*z[69];
    z[70]=z[67] - z[4];
    z[71]=n<T>(1,2)*z[6];
    z[72]=z[71] + z[70];
    z[72]=z[6]*z[72];
    z[73]=z[5] - z[4];
    z[74]=z[73] + z[67];
    z[75]= - n<T>(1,2)*z[8] - z[74];
    z[75]=z[8]*z[75];
    z[76]=n<T>(17,36)*z[10] - n<T>(35,18)*z[3] + z[8] + n<T>(53,18)*z[67] + z[73];
    z[76]=z[10]*z[76];
    z[77]=n<T>(1,2)*z[11];
    z[78]=n<T>(1,2)*z[5];
    z[79]= - n<T>(3,2)*z[11] + z[10] - z[8] + z[78] + z[6];
    z[79]=z[79]*z[77];
    z[80]= - n<T>(5,3)*z[4] + n<T>(3,4)*z[5];
    z[80]=z[5]*z[80];
    z[81]=z[5] + n<T>(1,12)*z[3];
    z[81]=z[3]*z[81];
    z[72]=z[79] + z[76] + n<T>(5,3)*z[81] + z[75] + z[72] - z[66] + z[80];
    z[72]=z[11]*z[72];
    z[75]=4*z[67];
    z[76]= - z[7] - z[75] - z[4];
    z[76]=n<T>(4,3)*z[2] + z[71] - z[78] + n<T>(2,3)*z[76];
    z[76]=z[2]*z[76];
    z[79]=2*z[5];
    z[80]= - z[79] - z[70];
    z[80]=z[5]*z[80];
    z[74]=z[6] + z[74];
    z[74]=z[6]*z[74];
    z[81]=z[68] - z[3];
    z[82]=2*z[3];
    z[81]=z[81]*z[82];
    z[83]= - z[7] + z[68] + z[4];
    z[84]=z[6] - z[5];
    z[83]= - z[84] + n<T>(4,3)*z[83];
    z[83]=z[7]*z[83];
    z[85]=npow(z[9],2);
    z[66]=z[76] + z[83] + 2*z[85] + z[81] + z[74] + z[66] + z[80];
    z[66]=z[2]*z[66];
    z[74]= - z[67] + n<T>(1,2)*z[4];
    z[76]=7*z[4];
    z[80]=z[74]*z[76];
    z[81]=n<T>(3,2)*z[6] + n<T>(43,18)*z[67] - z[73];
    z[81]=z[6]*z[81];
    z[83]=n<T>(3,2)*z[8] + 3*z[70] - z[84];
    z[83]=z[8]*z[83];
    z[76]=3*z[8] + n<T>(79,18)*z[6] - n<T>(49,12)*z[5] + n<T>(313,36)*z[67] - z[76];
    z[76]=n<T>(91,36)*z[9] + n<T>(1,2)*z[76] - z[82];
    z[76]=z[9]*z[76];
    z[82]=z[67]*z[3];
    z[84]= - n<T>(49,24)*z[5] - n<T>(49,12)*z[67] - z[4];
    z[84]=z[5]*z[84];
    z[76]=z[76] + 4*z[82] + z[83] + z[81] + z[80] + z[84];
    z[76]=z[9]*z[76];
    z[80]=2*z[9];
    z[81]=2*z[70];
    z[83]=z[81] + z[9];
    z[83]=z[83]*z[80];
    z[84]= - n<T>(41,36)*z[67] + z[4];
    z[84]=n<T>(55,36)*z[3] - 15*z[8] + 11*z[84] - n<T>(29,12)*z[5];
    z[80]= - n<T>(43,36)*z[10] + n<T>(1,2)*z[84] + z[80];
    z[80]=z[10]*z[80];
    z[74]=z[4]*z[74];
    z[84]= - n<T>(17,24)*z[5] - n<T>(17,12)*z[67] + z[4];
    z[84]=z[5]*z[84];
    z[85]= - n<T>(7,2)*z[8] - 7*z[70] - z[5];
    z[85]=z[8]*z[85];
    z[86]=n<T>(11,6)*z[3] - n<T>(11,3)*z[67] + z[5];
    z[86]=z[3]*z[86];
    z[74]=z[80] + z[83] + n<T>(5,12)*z[86] + z[85] - 3*z[74] + z[84];
    z[74]=z[10]*z[74];
    z[80]=n<T>(1,3)*z[6];
    z[83]=n<T>(1,3)*z[67] + z[5];
    z[83]= - n<T>(1,12)*z[9] + n<T>(1,2)*z[83] - z[80];
    z[83]=z[9]*z[83];
    z[84]=z[69]*z[4];
    z[85]= - n<T>(61,24)*z[5] + n<T>(85,12)*z[67] - z[4];
    z[85]=z[5]*z[85];
    z[86]=n<T>(61,36)*z[6] - n<T>(61,18)*z[67] + z[4];
    z[86]=z[6]*z[86];
    z[87]= - z[67] - z[4];
    z[87]=2*z[87] + z[3];
    z[87]=z[3]*z[87];
    z[88]=n<T>(79,6)*z[6] + z[4] - n<T>(85,8)*z[5];
    z[88]= - n<T>(61,72)*z[9] + n<T>(1,3)*z[88] + z[3];
    z[88]=z[7]*z[88];
    z[83]=z[88] + n<T>(61,6)*z[83] + z[87] + z[86] - n<T>(1,3)*z[84] + z[85];
    z[83]=z[7]*z[83];
    z[73]= - z[71] - z[67] + z[73];
    z[73]=z[6]*z[73];
    z[85]=2*z[4];
    z[86]=3*z[67] - z[85];
    z[86]=z[4]*z[86];
    z[87]=z[78] + z[70];
    z[87]=z[5]*z[87];
    z[71]=n<T>(31,6)*z[8] - z[71] + n<T>(3,2)*z[70] + z[5];
    z[71]=z[8]*z[71];
    z[71]=z[71] + z[73] + z[86] + z[87];
    z[71]=z[8]*z[71];
    z[73]=z[67] - z[78];
    z[73]=z[5]*z[73];
    z[78]=z[68] - z[6];
    z[78]=z[6]*z[78];
    z[86]=z[67] + z[6];
    z[86]=2*z[86] - z[8];
    z[86]=z[8]*z[86];
    z[87]=z[67] - z[3];
    z[88]= - z[8] - z[6] - n<T>(25,24)*z[5] + z[4] - n<T>(77,36)*z[87];
    z[88]=z[3]*z[88];
    z[73]=z[88] + z[86] + z[78] - z[84] + n<T>(25,12)*z[73];
    z[73]=z[3]*z[73];
    z[78]=z[82] + z[26];
    z[82]= - z[9]*z[67];
    z[82]=z[82] - z[78];
    z[82]=z[14]*z[82];
    z[68]=z[68] - z[2];
    z[86]=z[3] - z[68];
    z[86]=z[17]*z[86];
    z[88]=z[9] - z[87];
    z[88]=z[20]*z[88];
    z[82]=z[39] + z[88] + z[82] + z[86];
    z[86]=z[7] + z[6];
    z[88]= - z[67]*z[86];
    z[88]= - z[26] + z[88];
    z[88]=z[16]*z[88];
    z[86]=z[67] - z[86];
    z[86]=z[24]*z[86];
    z[86]=z[88] + z[86];
    z[88]= - n<T>(7,4)*z[5] + n<T>(7,12)*z[67] + z[4];
    z[88]=7*z[88] - n<T>(10,3)*z[6];
    z[80]=z[88]*z[80];
    z[88]= - z[4] - n<T>(5,4)*z[5];
    z[88]=z[5]*z[88];
    z[80]=z[80] - z[84] + z[88];
    z[80]=z[6]*z[80];
    z[69]=z[2] - z[69];
    z[69]=z[18]*z[69];
    z[68]=z[7] - z[68];
    z[68]=z[23]*z[68];
    z[68]= - z[31] + z[69] + z[68] + z[49] + z[43];
    z[69]=z[67]*z[4];
    z[69]=z[69] + z[26];
    z[84]=z[5]*z[67];
    z[84]=z[84] + z[69];
    z[84]=z[15]*z[84];
    z[88]= - z[5] + z[70];
    z[88]=z[21]*z[88];
    z[84]=z[28] - z[52] + z[84] + z[88];
    z[88]= - z[11]*z[67];
    z[78]=z[88] - z[78];
    z[78]=z[13]*z[78];
    z[87]=z[11] - z[87];
    z[87]=z[25]*z[87];
    z[78]=z[78] + z[87];
    z[85]=n<T>(5,3)*z[67] - z[85];
    z[85]=z[85]*npow(z[4],2);
    z[75]= - z[75] - n<T>(1,6)*z[4];
    z[75]=z[4]*z[75];
    z[87]=n<T>(9,2)*z[5] + n<T>(19,12)*z[67] + 4*z[4];
    z[87]=z[5]*z[87];
    z[75]=z[75] + z[87];
    z[75]=z[5]*z[75];
    z[87]=z[2] - z[7];
    z[81]=n<T>(55,6)*z[6] - z[81] + n<T>(29,3)*z[5] - 2*z[87];
    z[81]=z[22]*z[81];
    z[67]=z[8]*z[67];
    z[67]=z[67] + z[69];
    z[67]=z[12]*z[67];
    z[69]= - z[79] - z[77];
    z[69]=z[37]*z[69];
    z[77]=z[47] - z[40];
    z[79]=z[53] - z[45];
    z[87]=z[8] + z[10];
    z[87]=z[35]*z[87];
    z[87]=z[87] - z[34];
    z[88]=n<T>(40,3)*z[32] + n<T>(8,3)*z[30] - n<T>(10,27)*z[27];
    z[88]=i*z[88];
    z[89]= - n<T>(1,9)*z[6] + n<T>(49,18)*z[4] + 23*z[5];
    z[89]=n<T>(16,9)*z[2] - n<T>(149,108)*z[11] - n<T>(121,216)*z[7] - n<T>(193,216)*
    z[10] - n<T>(359,216)*z[9] - n<T>(101,72)*z[3] + n<T>(1,4)*z[89] + n<T>(4,3)*z[8];
    z[89]=z[26]*z[89];
    z[70]=z[8] - z[70];
    z[70]=z[19]*z[70];
    z[90]=z[6] + z[9];
    z[90]=z[36]*z[90];
    z[91]= - z[10] - z[11];
    z[91]=z[38]*z[91];

    r +=  - n<T>(16,9)*z[29] - n<T>(20,3)*z[33] + z[41] + 6*z[42] + n<T>(1,3)*z[44]
       - z[46] + z[48] - n<T>(61,18)*z[50] - n<T>(109,24)*z[51] - z[54] + n<T>(61,72)*z[55]
     - z[56]
     - n<T>(37,24)*z[57]
     + z[58] - n<T>(35,18)*z[59] - n<T>(25,24)*z[60]
     + 3*z[61] - n<T>(5,3)*z[62] - n<T>(55,72)*z[63] - n<T>(17,24)*z[64]
       + z[65] + z[66] + z[67] + n<T>(4,3)*z[68] + z[69] + z[70] + z[71] + 
      z[72] + z[73] + z[74] + z[75] + z[76] + 2*z[77] + n<T>(5,18)*z[78] - 
      5*z[79] + z[80] + z[81] + 4*z[82] + z[83] + n<T>(13,3)*z[84] + z[85]
       + n<T>(97,18)*z[86] + 8*z[87] + z[88] + z[89] + n<T>(25,18)*z[90] + n<T>(1,18)*z[91];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1324(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1324(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
