#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf840(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[16];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=d[9];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[17];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[3];
    z[33]=e[6];
    z[34]=e[7];
    z[35]=e[13];
    z[36]=e[14];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[6];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[31];
    z[49]=f[32];
    z[50]=f[36];
    z[51]=f[39];
    z[52]=f[51];
    z[53]=f[55];
    z[54]=f[56];
    z[55]=f[58];
    z[56]=f[100];
    z[57]=n<T>(1,2)*z[7];
    z[58]=z[1]*i;
    z[59]= - 9225*z[58] + n<T>(7811,4)*z[6];
    z[59]= - n<T>(5,2)*z[10] + n<T>(25,4)*z[4] - n<T>(39,1531)*z[9] - z[57] + n<T>(1,1531)*z[59]
     - n<T>(3,2)*z[5];
    z[59]=z[10]*z[59];
    z[60]=2*z[58];
    z[61]=z[60] - z[6];
    z[61]=z[6]*z[61];
    z[62]=z[58] + z[7];
    z[63]= - z[7]*z[62];
    z[64]=z[58] - z[6];
    z[65]= - 78*z[64] - n<T>(1609,2)*z[9];
    z[65]=z[9]*z[65];
    z[62]= - n<T>(1,2)*z[4] + z[62];
    z[62]=z[4]*z[62];
    z[59]=z[59] + z[62] + n<T>(1,1531)*z[65] + n<T>(39,1531)*z[61] + z[63];
    z[59]=z[10]*z[59];
    z[61]= - z[58] + n<T>(1,2)*z[6];
    z[62]=z[61]*z[6];
    z[63]=149843*z[58] - n<T>(294859,2)*z[5];
    z[63]=z[5]*z[63];
    z[63]= - n<T>(10639,2)*z[62] + n<T>(1,3)*z[63];
    z[65]=npow(z[9],2);
    z[66]= - n<T>(512105,18372)*z[7] + n<T>(149843,4593)*z[5] - n<T>(145250,4593)*
    z[58] - z[6];
    z[66]=z[7]*z[66];
    z[67]=z[58] - z[5];
    z[68]=z[2]*z[67];
    z[69]= - n<T>(129979,3)*z[58] - 4671*z[6];
    z[69]= - n<T>(145367,6)*z[7] + n<T>(1,4)*z[69] - n<T>(74980,3)*z[5];
    z[69]=n<T>(240029,6124)*z[3] - n<T>(8401,1531)*z[2] + n<T>(3,2)*z[10] + n<T>(1,1531)
   *z[69] - n<T>(13,2)*z[9];
    z[69]=z[3]*z[69];
    z[63]=z[69] - n<T>(16802,1531)*z[68] - n<T>(71489,6124)*z[65] + n<T>(1,1531)*
    z[63] + z[66];
    z[63]=z[3]*z[63];
    z[66]=139477*z[6];
    z[68]= - z[61]*z[66];
    z[66]= - n<T>(127229,6)*z[5] + n<T>(127229,3)*z[58] + z[66];
    z[66]=z[5]*z[66];
    z[66]=z[68] + z[66];
    z[68]=z[4] - n<T>(127229,18372)*z[5] - n<T>(139477,6124)*z[6] - z[57] - 
    z[58];
    z[68]=z[4]*z[68];
    z[69]=z[58] - n<T>(25,4)*z[7];
    z[69]=z[7]*z[69];
    z[70]=z[58] - z[9];
    z[71]=z[9]*z[70];
    z[66]=z[68] + 2*z[71] + n<T>(1,3062)*z[66] + z[69];
    z[66]=z[4]*z[66];
    z[68]= - 348083*z[58] + n<T>(571453,4)*z[6];
    z[68]=z[6]*z[68];
    z[69]= - 20753*z[58] - n<T>(48953,3)*z[6];
    z[69]=2*z[69] + n<T>(93521,2)*z[5];
    z[69]=z[5]*z[69];
    z[71]=262217*z[58] + n<T>(320525,6)*z[6];
    z[71]= - n<T>(262217,4)*z[2] + n<T>(145601,6)*z[4] + n<T>(1,2)*z[71] + 14577*
    z[5];
    z[71]=z[2]*z[71];
    z[68]=z[71] + n<T>(1,3)*z[68] + z[69];
    z[69]=2*z[64] + z[7];
    z[69]=z[7]*z[69];
    z[71]=136415*z[4] - 272830*z[58] - 145601*z[5];
    z[71]=z[4]*z[71];
    z[65]=n<T>(1,4593)*z[71] + 2*z[65] + 3*z[69] + n<T>(1,1531)*z[68];
    z[65]=z[2]*z[65];
    z[68]=49927*z[6] - n<T>(2047649,18)*z[5];
    z[68]= - n<T>(222619,24)*z[12] + n<T>(9225,2)*z[3] + n<T>(43335,2)*z[2] - n<T>(19747,6)*z[10]
     + n<T>(99671,36)*z[4] - n<T>(849647,36)*z[9]
     + n<T>(1,4)*z[68]
     + n<T>(52366,9)*z[7];
    z[68]=z[22]*z[68];
    z[69]=n<T>(122519,3)*z[3] - n<T>(13701,2)*z[64] + n<T>(140657,3)*z[7];
    z[69]=z[21]*z[69];
    z[71]=22848*z[9] + n<T>(65599,2)*z[3];
    z[71]=z[32]*z[71];
    z[68]=z[71] + z[68] + z[69];
    z[69]=136181*z[3];
    z[71]=94610*z[5] + z[69];
    z[71]=z[58]*z[71];
    z[71]=94610*z[22] + z[71];
    z[71]=z[8]*z[71];
    z[69]= - 94610*z[67] + z[69];
    z[69]=z[17]*z[69];
    z[72]=n<T>(63629,2)*z[9] + 61669*z[12];
    z[72]=z[33]*z[72];
    z[69]=z[72] + z[71] + z[69];
    z[71]=136649*z[12];
    z[72]=10327*z[6] - z[71];
    z[72]=z[58]*z[72];
    z[72]=10327*z[22] + z[72];
    z[72]=z[13]*z[72];
    z[71]= - 10327*z[64] - z[71];
    z[71]=z[20]*z[71];
    z[71]=z[72] + z[71];
    z[57]=z[57] + z[67];
    z[57]=z[7]*z[57];
    z[72]= - z[58] + n<T>(1,2)*z[5];
    z[72]=z[72]*z[5];
    z[57]=z[72] + z[57];
    z[73]=20171*z[58] - n<T>(177313,2)*z[6];
    z[73]=n<T>(29375,2)*z[9] + 32819*z[7] + n<T>(1,3)*z[73] + n<T>(45657,2)*z[5];
    z[73]=z[9]*z[73];
    z[57]=z[73] + n<T>(63161,3)*z[62] + 79339*z[57];
    z[57]=z[9]*z[57];
    z[62]=npow(z[6],2);
    z[73]=20137*z[58] - n<T>(17075,6)*z[6];
    z[73]=z[73]*z[62];
    z[57]=z[73] + z[57];
    z[73]=n<T>(222619,9186)*z[6];
    z[74]= - z[61]*z[73];
    z[73]= - 51*z[58] - z[73];
    z[73]=n<T>(1,2)*z[73] + n<T>(14,3)*z[5];
    z[73]=z[5]*z[73];
    z[73]=z[74] + z[73];
    z[73]=z[5]*z[73];
    z[74]=n<T>(1,4)*z[7] + n<T>(361355,18372)*z[5] - n<T>(91487,4593)*z[58] + n<T>(7,2)*
    z[6];
    z[74]=z[7]*z[74];
    z[62]=z[74] + 3*z[62] - n<T>(70855,9186)*z[72];
    z[62]=z[7]*z[62];
    z[72]=npow(z[7],2);
    z[74]= - 63629*z[58] + 26651*z[6];
    z[74]=z[6]*z[74];
    z[64]=n<T>(1,2)*z[9] + z[64];
    z[64]=z[9]*z[64];
    z[64]=n<T>(63629,4593)*z[64] + n<T>(1,4593)*z[74] - 3*z[72];
    z[74]=z[10] - z[5];
    z[75]=72347*z[58] + 164207*z[6];
    z[74]= - n<T>(93781,18372)*z[12] + n<T>(1,18372)*z[75] - 2*z[74];
    z[74]=z[12]*z[74];
    z[75]=npow(z[10],2);
    z[64]=z[74] + n<T>(1,2)*z[64] - z[75];
    z[64]=z[12]*z[64];
    z[67]= - n<T>(1,3)*z[14] + z[7] + z[67];
    z[67]=z[14]*z[67];
    z[67]=z[67] - z[72] - z[75];
    z[72]=z[12]*z[10];
    z[67]= - n<T>(47149,36744)*z[22] + z[72] + n<T>(1,2)*z[67];
    z[67]=z[14]*z[67];
    z[72]=z[4] + z[9];
    z[58]= - z[58]*z[72];
    z[58]= - z[22] + z[58];
    z[58]=z[11]*z[58];
    z[70]=z[4] - z[70];
    z[70]=z[18]*z[70];
    z[58]= - z[58] - z[70] + z[49] + z[38];
    z[70]=z[41] - z[28];
    z[72]=z[53] + z[52];
    z[74]= - n<T>(249969,1531)*z[29] - n<T>(262217,3062)*z[27] + n<T>(17,4)*z[25]
     + 
   n<T>(51035,27558)*z[23];
    z[74]=i*z[74];
    z[60]=z[60] - z[2];
    z[75]= - z[4] + z[60];
    z[75]=z[15]*z[75];
    z[60]= - z[5] + z[60];
    z[60]=z[16]*z[60];
    z[61]= - n<T>(1,2)*z[2] - z[61];
    z[61]=z[19]*z[61];
    z[76]= - z[10] + n<T>(9,2)*z[12];
    z[76]=z[34]*z[76];
    z[77]= - 5*z[10] - n<T>(11,2)*z[14];
    z[77]=z[35]*z[77];
    z[78]=n<T>(13,2)*z[7] + 6*z[14];
    z[78]=z[36]*z[78];

    r +=  - n<T>(518951,36744)*z[24] - n<T>(335705,36744)*z[26] + n<T>(249969,3062)
      *z[30] + n<T>(4019231,73488)*z[31] + 4*z[37] - n<T>(127229,18372)*z[39]
       + n<T>(324611,18372)*z[40] + n<T>(5,2)*z[42] - n<T>(222619,18372)*z[43] - 
      n<T>(45657,6124)*z[44] + n<T>(32819,3062)*z[45] - n<T>(139477,6124)*z[46] + n<T>(4,3)*z[47]
     + n<T>(16997,6124)*z[48]
     + n<T>(177313,18372)*z[50] - n<T>(1609,3062)*z[51] - z[54] - z[55]
     + z[56]
     + n<T>(1,3062)*z[57] - 2*z[58]
     +  z[59] + n<T>(29154,1531)*z[60] + n<T>(320525,4593)*z[61] + z[62] + z[63]
       + z[64] + z[65] + z[66] + z[67] + n<T>(1,1531)*z[68] + n<T>(1,4593)*
      z[69] - n<T>(262217,6124)*z[70] + n<T>(1,9186)*z[71] + n<T>(25,4)*z[72] + 
      z[73] + z[74] + n<T>(127229,4593)*z[75] + z[76] + z[77] + z[78];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf840(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf840(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
