#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf230(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[62];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[16];
    z[11]=d[5];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[4];
    z[16]=e[8];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[17];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[3];
    z[28]=e[10];
    z[29]=e[6];
    z[30]=d[6];
    z[31]=f[0];
    z[32]=f[1];
    z[33]=f[3];
    z[34]=f[4];
    z[35]=f[5];
    z[36]=f[11];
    z[37]=f[12];
    z[38]=f[16];
    z[39]=f[18];
    z[40]=f[31];
    z[41]=f[36];
    z[42]=f[39];
    z[43]=f[76];
    z[44]=n<T>(1,2)*z[2];
    z[45]=z[1]*i;
    z[46]=z[44] - z[45];
    z[47]=n<T>(1,2)*z[5];
    z[48]=z[46] + z[47];
    z[49]=z[2]*z[48];
    z[50]=npow(z[9],2);
    z[51]= - 5*z[45] + z[5];
    z[51]=z[5]*z[51];
    z[49]=z[49] - 3*z[50] + z[51];
    z[44]=z[49]*z[44];
    z[49]=n<T>(5,8)*z[45] - n<T>(7,3)*z[9];
    z[49]=z[49]*z[50];
    z[51]=z[47] - z[45] - n<T>(29,2)*z[9];
    z[52]=n<T>(1,4)*z[5];
    z[51]=z[52]*z[9]*z[51];
    z[48]=z[13]*z[48];
    z[44]= - z[42] + z[48] - z[44] - z[49] - z[51] + z[41] + z[40] + 
    z[39] - z[36];
    z[48]=n<T>(3,2)*z[9];
    z[49]=z[45] + z[48];
    z[49]=z[9]*z[49];
    z[51]= - z[45] - z[52];
    z[51]=z[5]*z[51];
    z[47]= - n<T>(1,4)*z[2] + z[45] + z[47];
    z[47]=z[2]*z[47];
    z[52]=n<T>(1,2)*z[6];
    z[53]=z[2] - z[5];
    z[54]= - z[9] - z[53];
    z[54]=z[54]*z[52];
    z[47]=z[54] + z[47] + z[49] + z[51];
    z[47]=z[6]*z[47];
    z[49]=n<T>(17,2)*z[3];
    z[51]= - z[5] - z[49];
    z[51]=z[45]*z[51];
    z[51]= - z[17] + z[51];
    z[51]=z[8]*z[51];
    z[54]=z[45] - z[5];
    z[49]= - z[49] + z[54];
    z[49]=z[14]*z[49];
    z[49]=z[51] + z[49];
    z[51]=n<T>(1,2)*z[9];
    z[55]=z[51] + z[45];
    z[55]=z[55]*z[9];
    z[56]= - n<T>(11,6)*z[5] + n<T>(11,3)*z[45] - z[9];
    z[56]=z[5]*z[56];
    z[56]=z[55] + z[56];
    z[57]= - n<T>(1,2)*z[3] - z[54];
    z[57]=z[3]*z[57];
    z[58]=z[9] - n<T>(49,3)*z[54];
    z[58]=n<T>(1,4)*z[58] - n<T>(13,3)*z[3];
    z[58]=z[7]*z[58];
    z[56]=z[58] + n<T>(1,2)*z[56] + n<T>(7,3)*z[57];
    z[56]=z[7]*z[56];
    z[57]= - z[45]*z[53];
    z[51]= - z[45] + z[51];
    z[51]=z[9]*z[51];
    z[51]=z[51] + z[57];
    z[53]= - z[4] - z[45] + 3*z[53];
    z[57]=n<T>(1,2)*z[4];
    z[53]=z[53]*z[57];
    z[51]=3*z[51] + z[53];
    z[51]=z[51]*z[57];
    z[53]=z[45] + z[9];
    z[58]=z[52] - z[53];
    z[58]=z[6]*z[58];
    z[53]= - z[6] + z[53];
    z[53]=z[11]*z[53];
    z[53]=n<T>(1,2)*z[53] + z[55] + z[58];
    z[53]=z[11]*z[53];
    z[54]=z[2]*z[54];
    z[50]=z[50] + z[54];
    z[54]=n<T>(7,2)*z[45] + z[5];
    z[54]=z[5]*z[54];
    z[50]=n<T>(1,3)*z[54] + n<T>(3,2)*z[50];
    z[54]= - n<T>(13,12)*z[3] + n<T>(3,8)*z[2] + n<T>(17,24)*z[5] + n<T>(1,3)*z[45]
     + 
    z[9];
    z[54]=z[3]*z[54];
    z[50]=n<T>(1,2)*z[50] + z[54];
    z[50]=z[3]*z[50];
    z[54]=z[48] + z[4];
    z[55]=z[45]*z[54];
    z[55]=z[17] + z[55];
    z[55]=z[10]*z[55];
    z[58]= - z[57] - z[46];
    z[58]=z[12]*z[58];
    z[45]=z[45] - z[54];
    z[45]=z[15]*z[45];
    z[46]=z[52] + z[46];
    z[46]=z[16]*z[46];
    z[52]=z[33] - z[32];
    z[54]=z[35] - z[23];
    z[57]= - z[11] - z[57] - n<T>(1,3)*z[7] + n<T>(7,8)*z[3] - n<T>(29,8)*z[2] + 2*
    z[9] - n<T>(17,48)*z[5];
    z[57]=z[17]*z[57];
    z[57]=z[57] - z[19];
    z[59]= - z[30] - z[9];
    z[59]=z[29]*z[59];
    z[60]=z[22] - z[20];
    z[60]= - n<T>(5,2)*z[24] - n<T>(25,72)*z[18] + n<T>(1,4)*z[60];
    z[60]=i*z[60];
    z[48]= - z[48] - 2*z[3];
    z[48]=z[27]*z[48];
    z[61]=z[3] + z[7];
    z[61]=z[28]*z[61];

    r += n<T>(37,48)*z[21] + n<T>(5,4)*z[25] - n<T>(7,4)*z[26] - n<T>(3,2)*z[31]
     + n<T>(49,48)*z[34]
     + n<T>(29,16)*z[37]
     + n<T>(1,16)*z[38]
     + z[43] - n<T>(1,2)*z[44]
     +  z[45] + z[46] + z[47] + z[48] + n<T>(1,6)*z[49] + z[50] + z[51] - n<T>(3,4)*z[52]
     + z[53]
     + n<T>(1,8)*z[54]
     + z[55]
     + n<T>(1,4)*z[56]
     + n<T>(1,3)*
      z[57] + 3*z[58] + z[59] + z[60] + n<T>(13,12)*z[61];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf230(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf230(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
