#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf238(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[59];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=d[16];
    z[11]=d[5];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[31];
    z[25]=e[2];
    z[26]=e[3];
    z[27]=e[10];
    z[28]=e[6];
    z[29]=d[6];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[3];
    z[33]=f[4];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[18];
    z[39]=f[31];
    z[40]=f[36];
    z[41]=f[39];
    z[42]=z[4] + z[9];
    z[43]=z[1]*i;
    z[42]=z[43]*z[42];
    z[42]=z[16] + z[42];
    z[42]=z[10]*z[42];
    z[44]= - z[4] + z[43] - z[9];
    z[44]=z[14]*z[44];
    z[45]= - z[9] - z[3];
    z[45]=z[26]*z[45];
    z[46]=z[3] + z[7];
    z[46]=z[27]*z[46];
    z[42]=z[31] + z[45] + z[42] + z[44] + z[46] - z[32];
    z[44]=z[43] - z[5];
    z[45]=2*z[2];
    z[46]=z[44]*z[45];
    z[47]=npow(z[9],2);
    z[48]=2*z[43];
    z[49]=z[5]*z[48];
    z[50]=z[2] + z[5];
    z[51]= - z[3] + z[9] + z[50];
    z[51]=z[3]*z[51];
    z[46]=z[51] + z[46] + z[47] + z[49];
    z[46]=z[3]*z[46];
    z[49]=2*z[3];
    z[51]=z[9] - 3*z[44];
    z[51]=n<T>(1,2)*z[51] - z[49];
    z[51]=z[7]*z[51];
    z[52]=z[43] + n<T>(1,2)*z[9];
    z[52]=z[9]*z[52];
    z[53]= - z[43] + n<T>(1,2)*z[5];
    z[54]= - z[9] - z[53];
    z[54]=z[5]*z[54];
    z[44]= - 2*z[44] - z[3];
    z[44]=z[3]*z[44];
    z[44]=z[51] + z[44] + z[52] + z[54];
    z[44]=z[7]*z[44];
    z[51]= - 5*z[43] + z[5];
    z[51]=z[5]*z[51];
    z[51]= - z[47] + n<T>(1,3)*z[51];
    z[50]=z[48] - z[50];
    z[52]=z[2]*z[50];
    z[51]=2*z[51] - n<T>(1,3)*z[52];
    z[51]=z[2]*z[51];
    z[52]=4*z[43];
    z[54]= - z[52] - z[5];
    z[54]=z[5]*z[54];
    z[55]=z[48] + z[5];
    z[55]=2*z[55] - z[2];
    z[55]=z[2]*z[55];
    z[54]=z[54] + z[55];
    z[55]=z[48] + 3*z[9];
    z[55]=z[9]*z[55];
    z[56]=z[2] - z[5];
    z[57]= - z[9] - n<T>(4,3)*z[56];
    z[57]=z[6]*z[57];
    z[54]=z[57] + z[55] + n<T>(2,3)*z[54];
    z[54]=z[6]*z[54];
    z[55]= - 4*z[56];
    z[55]=z[43]*z[55];
    z[56]=z[43] + z[9];
    z[45]= - z[4] + z[45] + z[56] - 2*z[5];
    z[45]=z[4]*z[45];
    z[52]= - z[52] + z[9];
    z[52]=z[9]*z[52];
    z[45]=z[45] + z[52] + z[55];
    z[45]=z[4]*z[45];
    z[52]=z[48] + z[9];
    z[52]=z[9]*z[52];
    z[55]= - 2*z[56] + z[6];
    z[55]=z[6]*z[55];
    z[56]= - z[6] + z[56];
    z[56]=z[11]*z[56];
    z[52]=z[56] + z[52] + z[55];
    z[52]=z[11]*z[52];
    z[55]= - z[8]*z[43];
    z[55]= - z[25] + z[55];
    z[49]=z[49]*z[55];
    z[43]=n<T>(1,2)*z[43] - n<T>(8,3)*z[9];
    z[43]=z[43]*z[47];
    z[47]=z[13]*z[50];
    z[48]=z[48] - z[2];
    z[50]=z[6] - z[48];
    z[50]=z[15]*z[50];
    z[50]=z[35] + z[50] - z[38];
    z[55]=z[34] - z[21];
    z[48]= - z[4] + z[48];
    z[48]=z[12]*z[48];
    z[48]=z[48] - z[30];
    z[56]=z[29] + z[9];
    z[56]= - 2*z[56];
    z[56]=z[28]*z[56];
    z[57]=z[20] - z[17];
    z[57]= - n<T>(20,3)*z[22] + n<T>(2,3)*z[57];
    z[57]=i*z[57];
    z[53]= - n<T>(5,2)*z[9] + z[53];
    z[53]=z[5]*z[9]*z[53];
    z[58]= - z[6] - n<T>(5,2)*z[5] - 29*z[2];
    z[58]= - n<T>(2,3)*z[11] + n<T>(4,3)*z[4] + z[3] + n<T>(1,9)*z[58];
    z[58]=z[16]*z[58];

    r +=  - z[18] + n<T>(37,18)*z[19] + n<T>(10,3)*z[23] - n<T>(5,6)*z[24] + n<T>(3,2)*
      z[33] + n<T>(5,2)*z[36] + n<T>(1,2)*z[37] - z[39] - z[40] + z[41] + 2*
      z[42] + z[43] + z[44] + z[45] + z[46] + n<T>(2,3)*z[47] + 4*z[48] + 
      z[49] + n<T>(4,3)*z[50] + z[51] + z[52] + z[53] + z[54] + n<T>(1,3)*z[55]
       + z[56] + z[57] + z[58];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf238(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf238(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
