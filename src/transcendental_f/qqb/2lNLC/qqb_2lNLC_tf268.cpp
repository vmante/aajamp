#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf268(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[50];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[2];
    z[7]=d[5];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[6];
    z[11]=d[17];
    z[12]=e[2];
    z[13]=e[9];
    z[14]=c[3];
    z[15]=d[9];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[31];
    z[19]=e[3];
    z[20]=e[10];
    z[21]=e[6];
    z[22]=e[7];
    z[23]=e[14];
    z[24]=e[13];
    z[25]=f[4];
    z[26]=f[12];
    z[27]=f[16];
    z[28]=f[31];
    z[29]=f[36];
    z[30]=f[39];
    z[31]=f[51];
    z[32]=f[55];
    z[33]=f[57];
    z[34]=f[58];
    z[35]=3*z[7];
    z[36]=15*z[9] - 31*z[3];
    z[36]=n<T>(11,2)*z[2] + n<T>(1,2)*z[36] + z[35];
    z[36]=n<T>(1,2)*z[36] - n<T>(5,3)*z[8];
    z[37]=n<T>(1,2)*z[8];
    z[36]=z[36]*z[37];
    z[38]=npow(z[9],2);
    z[39]=z[38] - z[14];
    z[40]=npow(z[3],2);
    z[41]= - z[21] - z[19];
    z[42]=n<T>(1,2)*z[7];
    z[43]= - z[9] + z[42];
    z[43]=z[7]*z[43];
    z[44]=npow(z[2],2);
    z[36]=z[36] + n<T>(7,4)*z[44] + n<T>(3,2)*z[43] + n<T>(3,4)*z[40] + 2*z[41] - n<T>(1,4)*z[39];
    z[36]=z[8]*z[36];
    z[39]=n<T>(1,2)*z[9];
    z[41]=z[11] + z[10];
    z[41]=z[41]*z[39];
    z[43]=n<T>(1,2)*z[10];
    z[44]= - z[11]*z[43];
    z[45]=z[3]*z[5];
    z[46]=n<T>(1,4)*z[7];
    z[47]=z[46] + z[6] - n<T>(3,2)*z[9];
    z[47]=z[7]*z[47];
    z[48]=n<T>(1,4)*z[2] - n<T>(1,2)*z[5] + z[3];
    z[48]=z[2]*z[48];
    z[35]=z[35] + z[9] - 3*z[3];
    z[35]=n<T>(1,2)*z[35] + 2*z[8];
    z[35]=z[8]*z[35];
    z[35]=z[35] + z[48] + z[47] + z[45] + z[41] + z[44] - n<T>(1,2)*z[13] - 
    z[12];
    z[35]=z[1]*z[35];
    z[35]= - n<T>(1,24)*z[16] + z[35];
    z[35]=i*z[35];
    z[41]=n<T>(3,2)*z[8];
    z[44]=z[41] - z[7];
    z[45]=i*z[1];
    z[47]=n<T>(13,6)*z[4] - 5*z[45] - n<T>(31,4)*z[2] + n<T>(33,4)*z[3] - z[15] - n<T>(13,4)*z[6]
     + z[44];
    z[47]=z[4]*z[47];
    z[48]=npow(z[6],2);
    z[47]=z[48] + z[47];
    z[37]= - z[3] + z[37];
    z[37]=z[37]*z[41];
    z[41]=z[2] + n<T>(1,2)*z[3];
    z[44]= - z[6] + z[44] - z[41];
    z[44]=z[44]*z[45];
    z[45]=z[6] - z[42];
    z[45]=z[7]*z[45];
    z[49]=z[3] - n<T>(3,2)*z[2];
    z[49]=z[2]*z[49];
    z[37]=z[44] + z[37] + z[49] + z[45] + n<T>(1,4)*z[40] - n<T>(37,24)*z[14] + 
   2*z[23] + n<T>(9,2)*z[20] + n<T>(1,2)*z[47];
    z[37]=z[4]*z[37];
    z[44]=5*z[6] - 7*z[9];
    z[44]=z[44]*z[46];
    z[45]=n<T>(1,3)*z[14];
    z[38]=z[44] + n<T>(3,2)*z[38] - z[48] - z[45];
    z[38]=z[38]*z[42];
    z[42]=npow(z[10],2);
    z[44]= - z[9]*z[10];
    z[44]=z[44] + z[45] + z[13] + z[42];
    z[39]=z[44]*z[39];
    z[44]= - z[24] + 5*z[23];
    z[45]=npow(z[15],2);
    z[44]=n<T>(1,2)*z[44] + n<T>(1,3)*z[45];
    z[44]=z[15]*z[44];
    z[44]=z[44] - z[34];
    z[45]=z[22] - 5*z[21];
    z[42]= - n<T>(1,3)*z[42] + n<T>(1,2)*z[45] - z[13];
    z[42]=z[42]*z[43];
    z[43]= - z[14] + 21*z[20] - 17*z[19];
    z[41]=z[2]*z[41];
    z[41]=z[41] - z[12] + n<T>(1,2)*z[43];
    z[40]= - z[40] + n<T>(1,2)*z[41];
    z[40]=z[2]*z[40];
    z[41]=z[31] + z[32];
    z[43]=z[28] + z[29];
    z[45]=z[27] + z[30];
    z[46]=z[10] + z[6];
    z[46]= - n<T>(1,24)*z[15] + z[5] + n<T>(1,2)*z[11] - n<T>(1,6)*z[46];
    z[46]=z[14]*z[46];
    z[47]=z[12] + n<T>(1,12)*z[14];
    z[47]=z[3]*z[47];

    r +=  - n<T>(1,4)*z[17] + n<T>(39,8)*z[18] + n<T>(33,8)*z[25] + n<T>(31,8)*z[26]
     +  z[33] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + n<T>(5,8)*
      z[41] + z[42] - n<T>(7,8)*z[43] + n<T>(1,2)*z[44] + n<T>(3,4)*z[45] + z[46]
       + z[47];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf268(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf268(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
