#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1100(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[48];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[7];
    z[5]=d[1];
    z[6]=d[3];
    z[7]=d[8];
    z[8]=d[15];
    z[9]=d[4];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[19];
    z[18]=c[23];
    z[19]=c[25];
    z[20]=c[26];
    z[21]=c[28];
    z[22]=c[31];
    z[23]=e[10];
    z[24]=f[3];
    z[25]=f[4];
    z[26]=f[11];
    z[27]=f[12];
    z[28]=f[13];
    z[29]=f[16];
    z[30]=f[17];
    z[31]=z[1]*i;
    z[32]=z[31]*z[6];
    z[33]=npow(z[6],2);
    z[34]=n<T>(3,2)*z[33] + z[32];
    z[35]=z[31] - z[2];
    z[36]=2*z[6];
    z[37]= - z[36] - z[35];
    z[37]=z[2]*z[37];
    z[38]=z[31] + z[6];
    z[39]=n<T>(1,2)*z[4];
    z[40]= - z[39] + z[38];
    z[40]=z[40]*z[39];
    z[41]= - z[2] + n<T>(1,2)*z[6];
    z[42]= - z[39] - z[41];
    z[43]=n<T>(1,2)*z[3];
    z[42]=z[42]*z[43];
    z[34]=z[42] + z[40] + n<T>(1,2)*z[34] + z[37];
    z[34]=z[3]*z[34];
    z[37]= - n<T>(1,2)*z[33] + z[32];
    z[40]=z[5] + 2*z[31];
    z[42]=n<T>(1,2)*z[2];
    z[44]=z[42] - z[40];
    z[44]=z[2]*z[44];
    z[41]= - z[41]*z[39];
    z[45]=z[5]*z[6];
    z[37]=z[41] + z[44] + n<T>(3,2)*z[37] + z[45];
    z[37]=z[4]*z[37];
    z[36]=z[36]*z[31];
    z[36]=z[36] - z[33];
    z[41]=z[31] - z[6];
    z[44]=2*z[41];
    z[46]=z[44] + z[5];
    z[46]=z[5]*z[46];
    z[47]=5*z[41] + 8*z[5];
    z[47]=z[7]*z[47];
    z[46]=z[47] + 4*z[46] - z[36];
    z[46]=z[7]*z[46];
    z[32]=z[33] - 4*z[32];
    z[33]= - 5*z[5] + z[38];
    z[33]=z[5]*z[33];
    z[32]=2*z[32] + z[33];
    z[32]=z[5]*z[32];
    z[32]=z[32] + z[46];
    z[33]= - z[44] - z[7];
    z[33]=z[7]*z[33];
    z[38]= - z[7] - z[41];
    z[38]=z[9]*z[38];
    z[33]=z[38] + z[33] + z[36];
    z[33]=z[9]*z[33];
    z[36]= - z[43] + z[31] - z[42];
    z[36]=z[10]*z[36];
    z[38]=z[5] + z[6];
    z[38]= - z[31]*z[38];
    z[38]= - z[14] + z[38];
    z[38]=z[8]*z[38];
    z[41]= - z[5] + z[41];
    z[41]=z[12]*z[41];
    z[38]=z[16] + z[38] + z[41];
    z[31]= - n<T>(4,3)*z[2] + z[6] + n<T>(3,2)*z[31];
    z[31]=z[2]*z[31];
    z[31]=z[45] + z[31];
    z[31]=z[2]*z[31];
    z[41]=n<T>(7,12)*z[6] - 2*z[5];
    z[41]=n<T>(2,3)*z[9] + n<T>(13,24)*z[3] + n<T>(2,9)*z[7] - n<T>(5,24)*z[4] + n<T>(1,3)*
    z[41] + z[42];
    z[41]=z[14]*z[41];
    z[39]= - z[39] - n<T>(3,2)*z[2] + z[40];
    z[39]=z[13]*z[39];
    z[40]=z[24] + z[30] + z[26];
    z[42]=z[28] - z[19];
    z[43]= - 3*z[20] - n<T>(3,2)*z[18] - n<T>(2,9)*z[15];
    z[43]=i*z[43];
    z[35]=z[11]*z[35];
    z[44]= - z[5] - z[7];
    z[44]=z[23]*z[44];

    r +=  - n<T>(1,8)*z[17] + n<T>(3,2)*z[21] - n<T>(19,2)*z[22] - n<T>(5,3)*z[25]
     - 
      z[27] - z[29] + z[31] + n<T>(1,3)*z[32] + z[33] + z[34] + z[35] + 
      z[36] + z[37] + n<T>(2,3)*z[38] + z[39] - n<T>(1,4)*z[40] + z[41] - n<T>(3,4)
      *z[42] + z[43] + n<T>(8,3)*z[44];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1100(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1100(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
