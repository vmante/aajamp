#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf415(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[133];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[1];
    z[6]=d[3];
    z[7]=d[7];
    z[8]=d[9];
    z[9]=d[8];
    z[10]=e[7];
    z[11]=d[15];
    z[12]=d[2];
    z[13]=d[11];
    z[14]=e[12];
    z[15]=d[12];
    z[16]=d[4];
    z[17]=d[17];
    z[18]=e[5];
    z[19]=e[9];
    z[20]=e[13];
    z[21]=e[6];
    z[22]=e[14];
    z[23]=e[1];
    z[24]=e[2];
    z[25]=e[8];
    z[26]=e[10];
    z[27]=f[30];
    z[28]=f[31];
    z[29]=f[32];
    z[30]=f[33];
    z[31]=f[35];
    z[32]=f[36];
    z[33]=f[39];
    z[34]=f[43];
    z[35]=f[50];
    z[36]=f[51];
    z[37]=f[52];
    z[38]=f[55];
    z[39]=f[58];
    z[40]=f[68];
    z[41]=c[3];
    z[42]=c[11];
    z[43]=c[15];
    z[44]=c[17];
    z[45]=c[19];
    z[46]=c[23];
    z[47]=c[25];
    z[48]=c[26];
    z[49]=c[28];
    z[50]=c[31];
    z[51]=c[32];
    z[52]=c[39];
    z[53]=c[43];
    z[54]=c[46];
    z[55]=c[78];
    z[56]=e[3];
    z[57]=f[53];
    z[58]=f[4];
    z[59]=f[5];
    z[60]=f[11];
    z[61]=f[12];
    z[62]=f[16];
    z[63]=f[23];
    z[64]=g[66];
    z[65]=g[67];
    z[66]=g[84];
    z[67]=g[116];
    z[68]=g[117];
    z[69]=g[133];
    z[70]=g[159];
    z[71]=g[162];
    z[72]=g[163];
    z[73]=g[165];
    z[74]=g[167];
    z[75]=g[174];
    z[76]=g[175];
    z[77]=g[178];
    z[78]=g[184];
    z[79]=g[185];
    z[80]=g[186];
    z[81]=g[195];
    z[82]=g[209];
    z[83]=134*z[4];
    z[84]= - z[83] + n<T>(169,3)*z[3];
    z[84]=z[84]*z[4];
    z[85]=npow(z[3],2);
    z[84]=z[84] + n<T>(233,3)*z[85];
    z[86]=z[4]*i;
    z[87]=n<T>(23,17)*z[3];
    z[88]=n<T>(94,45) - z[87];
    z[88]=i*z[88];
    z[88]=z[88] + n<T>(23,17)*z[86];
    z[88]=z[1]*z[88];
    z[89]=n<T>(23,17)*z[4];
    z[87]=z[87] - z[89];
    z[90]=n<T>(47,90) + z[87];
    z[90]=z[6]*z[90];
    z[91]=z[4] - z[3];
    z[92]=7*z[8];
    z[93]= - z[91]*z[92];
    z[88]= - n<T>(47,45)*z[2] - n<T>(1,24)*z[9] + n<T>(47,90)*z[7] + z[93] + z[90]
    + n<T>(1,17)*z[84] + z[88];
    z[90]=n<T>(1,3)*z[2];
    z[88]=z[88]*z[90];
    z[93]= - n<T>(53,90) + n<T>(92,17)*z[3];
    z[93]=i*z[93];
    z[93]=z[93] - n<T>(92,17)*z[86];
    z[94]=n<T>(1,3)*z[1];
    z[93]=z[93]*z[94];
    z[95]=n<T>(1,3)*z[6];
    z[96]=n<T>(46,17)*z[3];
    z[97]= - z[96] + n<T>(46,17)*z[4];
    z[98]=n<T>(47,45) + z[97];
    z[98]=z[98]*z[95];
    z[99]=n<T>(1,2) + n<T>(28,9)*z[3];
    z[100]=n<T>(74,17)*z[4] - z[99];
    z[100]=z[4]*z[100];
    z[93]=z[98] + z[93] - n<T>(190,153)*z[85] + z[100];
    z[93]=z[6]*z[93];
    z[98]=i*z[3];
    z[100]=z[98] - z[86];
    z[100]=z[1]*z[100];
    z[101]=z[91]*z[6];
    z[102]= - z[3] - z[4];
    z[102]=z[4]*z[102];
    z[100]=z[101] + z[100] + 2*z[85] + z[102];
    z[100]=z[8]*z[100];
    z[96]= - n<T>(53,90) + z[96];
    z[96]=i*z[96];
    z[96]=z[96] - n<T>(46,17)*z[86];
    z[96]=z[1]*z[96];
    z[102]=14*z[8];
    z[91]=z[91]*z[102];
    z[103]= - n<T>(229,90) + z[97];
    z[103]=z[6]*z[103];
    z[84]=n<T>(47,45)*z[7] + z[91] + z[103] - n<T>(2,17)*z[84] + z[96];
    z[91]=n<T>(1,3)*z[7];
    z[84]=z[84]*z[91];
    z[96]= - n<T>(1,4) + n<T>(932,153)*z[3];
    z[96]=z[96]*z[85];
    z[103]=n<T>(1,2) + n<T>(1142,153)*z[3];
    z[103]=z[3]*z[103];
    z[104]= - n<T>(635,3)*z[3] - z[83];
    z[104]=z[4]*z[104];
    z[103]=z[103] + n<T>(2,51)*z[104];
    z[103]=z[4]*z[103];
    z[99]=i*z[99];
    z[99]=z[99] - n<T>(74,17)*z[86];
    z[99]=z[4]*z[99];
    z[104]=z[85]*i;
    z[99]=n<T>(190,153)*z[104] + z[99];
    z[99]=z[1]*z[99];
    z[105]=z[1]*i;
    z[106]= - n<T>(41,72)*z[9] + z[7] - n<T>(67,36)*z[105] + z[6];
    z[106]=z[9]*z[106];
    z[84]=z[88] + n<T>(1,5)*z[106] + z[84] + n<T>(14,3)*z[100] + z[93] + z[99]
    + z[96] + z[103];
    z[84]=z[84]*z[90];
    z[88]=z[105] - z[6];
    z[90]=n<T>(1,5) - n<T>(466,17)*z[3];
    z[90]= - z[102] + n<T>(1,3)*z[90] - n<T>(268,17)*z[4] + n<T>(46,17)*z[88];
    z[90]=z[7]*z[90];
    z[93]= - n<T>(1,5) + n<T>(982,17)*z[3];
    z[93]=i*z[93];
    z[93]=n<T>(1,9)*z[93] + n<T>(82,17)*z[86];
    z[93]=z[1]*z[93];
    z[96]= - 2389*z[3] - n<T>(3673,2)*z[4];
    z[96]=n<T>(1,17)*z[96] + 28*z[88];
    z[99]=n<T>(1,3)*z[8];
    z[96]=z[96]*z[99];
    z[83]= - n<T>(233,3)*z[3] - z[83] + 23*z[88];
    z[83]= - z[92] + n<T>(1,17)*z[83];
    z[100]=z[2]*z[83];
    z[102]= - n<T>(43,5) - n<T>(17189,34)*z[3];
    z[102]=z[3]*z[102];
    z[103]= - n<T>(2281,102)*z[4] - n<T>(1,2) - n<T>(8552,153)*z[3];
    z[103]=z[4]*z[103];
    z[106]= - n<T>(23,3)*z[6] + n<T>(46,3)*z[105] - n<T>(491,9)*z[3] - 41*z[4];
    z[106]=z[6]*z[106];
    z[90]= - n<T>(649,102)*z[10] + n<T>(727,408)*z[41] - n<T>(4,3)*z[100] + n<T>(2,3)*
    z[90] + z[96] + n<T>(4,17)*z[106] + 2*z[93] + n<T>(1,9)*z[102] + z[103];
    z[90]=z[10]*z[90];
    z[93]=n<T>(1,4)*z[85];
    z[96]= - 1 - n<T>(18857,459)*z[3];
    z[96]=z[96]*z[93];
    z[100]=n<T>(1,3)*z[4];
    z[102]=n<T>(1,5) + n<T>(8129,34)*z[3];
    z[102]=n<T>(1,2)*z[102] + n<T>(161,17)*z[4];
    z[102]=z[102]*z[100];
    z[102]=n<T>(10253,68)*z[85] + z[102];
    z[103]=n<T>(1,9)*z[4];
    z[102]=z[102]*z[103];
    z[96]=z[96] + z[102];
    z[96]=z[4]*z[96];
    z[102]=npow(z[3],3);
    z[106]= - n<T>(161,5) - n<T>(22069,17)*z[3];
    z[106]=z[106]*z[102];
    z[107]=n<T>(499,68)*z[3];
    z[108]=z[107] + n<T>(26,45);
    z[109]=n<T>(1349,408)*z[4];
    z[110]=z[108] + z[109];
    z[111]=n<T>(1109,136)*z[8] + n<T>(439,204)*z[88];
    z[112]=z[110] + z[111];
    z[112]=z[33]*z[112];
    z[113]=n<T>(1823,20)*z[9] - n<T>(13,5)*z[105] + n<T>(7,2)*z[7];
    z[113]=n<T>(413,30)*z[5] + n<T>(1,9)*z[113] - n<T>(1,10)*z[2];
    z[113]=z[26]*z[113];
    z[114]=z[8] + n<T>(326,153)*z[3] + z[4] - n<T>(592,153)*z[88];
    z[114]=z[40]*z[114];
    z[115]= - n<T>(11,9) - n<T>(73,17)*z[3];
    z[115]=7*z[115] - n<T>(8419,306)*z[4];
    z[115]= - n<T>(9607,612)*z[8] + n<T>(1,2)*z[115] - n<T>(38,153)*z[6];
    z[115]=z[43]*z[115];
    z[90]=z[112] + z[90] + n<T>(1,36)*z[106] + z[96] + z[113] + z[114] + 
    z[115] - z[54];
    z[96]=n<T>(94,45) + n<T>(163,34)*z[3];
    z[96]=i*z[96];
    z[96]=z[96] - n<T>(301,51)*z[86];
    z[96]=z[96]*z[94];
    z[97]=n<T>(229,180) - z[97];
    z[97]=z[97]*z[95];
    z[106]= - n<T>(1169,306)*z[4] + n<T>(1,2) + n<T>(338,153)*z[3];
    z[106]=z[4]*z[106];
    z[96]=z[97] + z[96] + n<T>(547,612)*z[85] + z[106];
    z[96]=z[96]*z[95];
    z[97]=1 + n<T>(1271,68)*z[3];
    z[97]=z[3]*z[97];
    z[106]= - n<T>(13,5) - n<T>(475,34)*z[3];
    z[106]=n<T>(11,2)*z[106] - n<T>(2957,17)*z[4];
    z[106]=z[4]*z[106];
    z[97]=5*z[97] + n<T>(1,2)*z[106];
    z[106]=n<T>(5,4)*z[98] - n<T>(301,153)*z[86];
    z[106]=z[1]*z[106];
    z[112]=n<T>(301,153)*z[4] - n<T>(47,135) - n<T>(5,4)*z[3];
    z[112]=z[6]*z[112];
    z[113]=1 + n<T>(5231,102)*z[3];
    z[114]=n<T>(1,2)*z[113] - n<T>(1585,51)*z[4];
    z[114]=z[8]*z[114];
    z[97]=n<T>(1,2)*z[114] + z[112] + n<T>(1,9)*z[97] + z[106];
    z[97]=z[97]*z[91];
    z[106]= - 10 - n<T>(11393,68)*z[3];
    z[106]=z[106]*z[98];
    z[112]=n<T>(4,15) - z[107];
    z[112]=i*z[112];
    z[112]=z[112] + n<T>(455,153)*z[86];
    z[112]=z[4]*z[112];
    z[106]=n<T>(1,9)*z[106] + z[112];
    z[106]=z[106]*z[94];
    z[112]=n<T>(1,2)*z[85];
    z[114]=npow(z[4],2);
    z[114]=z[112] - n<T>(1,3)*z[114];
    z[113]= - i*z[113];
    z[113]=z[113] + n<T>(1109,51)*z[86];
    z[113]=z[1]*z[113];
    z[113]=n<T>(1109,34)*z[114] + z[113];
    z[101]=n<T>(1,2)*z[113] - n<T>(14,3)*z[101];
    z[101]=z[101]*z[99];
    z[113]=n<T>(1,5) + n<T>(499,51)*z[3];
    z[113]=z[113]*z[93];
    z[114]=n<T>(1349,204)*z[4];
    z[115]=n<T>(499,34)*z[3];
    z[116]= - z[114] + n<T>(4,5) - z[115];
    z[116]=z[4]*z[116];
    z[116]=n<T>(1349,136)*z[85] + z[116];
    z[116]=z[116]*z[103];
    z[96]=z[97] + z[101] + z[96] + z[106] + z[113] + z[116];
    z[96]=z[7]*z[96];
    z[97]=z[115] + n<T>(52,45);
    z[101]=n<T>(439,102)*z[105];
    z[106]= - n<T>(1109,68)*z[8] + n<T>(439,102)*z[6] - z[101] - z[114] - z[97];
    z[106]=z[16]*z[106];
    z[113]=n<T>(1349,3)*z[4] + n<T>(647,2)*z[3];
    z[113]=z[113]*z[4];
    z[108]=z[3]*z[108];
    z[108]=z[108] - n<T>(1,204)*z[113];
    z[114]=n<T>(1,3)*i;
    z[115]=n<T>(3947,720) - n<T>(5,17)*z[3];
    z[115]=z[115]*z[114];
    z[115]=z[115] - n<T>(115,136)*z[86];
    z[115]=z[1]*z[115];
    z[116]= - n<T>(4439,60) - n<T>(439,17)*z[3];
    z[101]=z[101] + n<T>(1,2)*z[116] + n<T>(439,51)*z[4];
    z[101]=z[6]*z[101];
    z[116]= - z[100] + n<T>(1,2)*z[3];
    z[117]=n<T>(1,6)*z[105];
    z[118]= - z[117] + z[116];
    z[118]=z[8]*z[118];
    z[119]=n<T>(1109,34)*z[8];
    z[120]= - z[119] + n<T>(439,51)*z[6];
    z[121]=n<T>(1349,102)*z[4] + n<T>(499,17)*z[3] - z[120] + n<T>(439,51)*z[105];
    z[122]=n<T>(41,5) + z[121];
    z[122]=z[7]*z[122];
    z[101]=n<T>(1,9)*z[106] + n<T>(469,720)*z[9] + n<T>(1,36)*z[122] + n<T>(1109,204)*
    z[118] + n<T>(1,18)*z[101] + n<T>(1,3)*z[108] + z[115];
    z[101]=z[16]*z[101];
    z[106]=z[109] + n<T>(2,15) + z[107] + z[111];
    z[91]=z[106]*z[91];
    z[106]=z[116] + z[117];
    z[106]=z[106]*z[120];
    z[107]= - n<T>(4,15) - n<T>(469,17)*z[3];
    z[107]=i*z[107];
    z[107]=z[107] + n<T>(407,204)*z[86];
    z[107]=z[107]*z[94];
    z[97]= - z[3]*z[97];
    z[91]=z[91] + z[107] + z[97] + n<T>(1,102)*z[113] + z[106];
    z[91]=z[7]*z[91];
    z[97]=n<T>(52,15) + n<T>(3433,68)*z[3];
    z[97]=z[97]*z[98];
    z[106]= - n<T>(647,2)*z[98] - n<T>(1349,3)*z[86];
    z[106]=z[4]*z[106];
    z[97]=z[97] + n<T>(1,34)*z[106];
    z[97]=z[97]*z[94];
    z[106]=z[85]*z[110];
    z[107]= - n<T>(167,20) - n<T>(439,51)*z[3];
    z[107]=i*z[107];
    z[107]=n<T>(1,2)*z[107] + n<T>(439,153)*z[86];
    z[107]=z[1]*z[107];
    z[107]=n<T>(167,80)*z[6] - n<T>(439,204)*z[85] + z[107];
    z[107]=z[6]*z[107];
    z[108]=n<T>(1,2)*z[98] - n<T>(1,3)*z[86];
    z[108]=z[1]*z[108];
    z[108]=z[93] + z[108];
    z[108]=z[108]*z[119];
    z[109]=167*z[88] + n<T>(481,6)*z[9];
    z[109]=z[9]*z[109];
    z[91]=n<T>(1,40)*z[109] + z[91] + z[108] + z[107] + z[97] + z[106];
    z[91]=n<T>(1,3)*z[91] + z[101];
    z[91]=z[16]*z[91];
    z[97]= - n<T>(554,27)*z[8] + n<T>(31,2)*z[6];
    z[101]= - n<T>(107,54)*z[3] - 7*z[4];
    z[101]=5*z[101] - n<T>(31,2)*z[105] + z[97];
    z[101]=z[12]*z[101];
    z[106]=n<T>(535,51)*z[3];
    z[107]=z[106] - n<T>(59,10);
    z[108]=n<T>(1,6)*z[3];
    z[107]=z[107]*z[108];
    z[108]=n<T>(105,17)*z[4];
    z[109]=z[108]*z[3];
    z[107]=z[107] + z[109];
    z[109]=n<T>(1,2)*z[107];
    z[110]=n<T>(3581,108)*z[98] + 35*z[86];
    z[111]=n<T>(1,17)*z[1];
    z[110]=z[110]*z[111];
    z[113]=n<T>(31,34)*z[6];
    z[115]= - n<T>(3,2)*z[3] - z[105];
    z[115]=z[115]*z[113];
    z[116]= - 5*z[4] + n<T>(161,54)*z[3];
    z[97]=z[97] + 7*z[116];
    z[116]=n<T>(271,54)*z[105] + z[97];
    z[117]=n<T>(1,17)*z[8];
    z[116]=z[116]*z[117];
    z[118]= - z[113] + n<T>(35,17)*z[4];
    z[119]=z[118] + n<T>(31,34)*z[105];
    z[122]=n<T>(535,153)*z[3];
    z[123]=n<T>(59,10) + z[122];
    z[123]=n<T>(1,6)*z[123] + z[119];
    z[124]=n<T>(277,459)*z[8];
    z[123]=n<T>(1,2)*z[123] + z[124];
    z[123]=z[9]*z[123];
    z[101]=n<T>(1,17)*z[101] + z[123] + z[116] + z[115] + z[109] + z[110];
    z[101]=z[12]*z[101];
    z[110]=n<T>(1108,27)*z[8];
    z[115]=31*z[6];
    z[116]=z[110] - z[115];
    z[123]= - n<T>(161,27)*z[3] + 10*z[4];
    z[123]=7*z[123] + n<T>(283,27)*z[105] + z[116];
    z[123]=z[123]*z[117];
    z[125]= - n<T>(59,20) - n<T>(1523,153)*z[3];
    z[114]=z[125]*z[114];
    z[114]=z[114] - n<T>(35,17)*z[86];
    z[114]=z[1]*z[114];
    z[125]=3*z[3] + z[105];
    z[113]=z[125]*z[113];
    z[125]= - n<T>(227,10) + z[122];
    z[125]=n<T>(1,6)*z[125] + z[119];
    z[125]=n<T>(1,2)*z[125] + z[124];
    z[125]=z[9]*z[125];
    z[107]=z[125] + z[123] + z[113] + z[114] - z[107];
    z[107]=z[9]*z[107];
    z[113]=z[108]*z[85];
    z[106]=z[106] + n<T>(109,10);
    z[106]=n<T>(1,6)*z[106];
    z[114]=z[85]*z[106];
    z[114]=z[114] + z[113];
    z[123]=n<T>(1,12)*z[98];
    z[125]=n<T>(59,5) - n<T>(233,51)*z[3];
    z[125]=z[125]*z[123];
    z[126]=z[86]*z[3];
    z[127]=n<T>(105,17)*z[126];
    z[125]=z[125] - z[127];
    z[125]=z[1]*z[125];
    z[128]=z[3]*z[105];
    z[128]= - z[112] + z[128];
    z[129]=n<T>(93,34)*z[6];
    z[128]=z[128]*z[129];
    z[130]=2*z[105];
    z[131]= - z[3]*z[130];
    z[131]=z[85] + z[131];
    z[132]=n<T>(277,153)*z[8];
    z[131]=z[131]*z[132];
    z[101]=z[101] + z[107] + z[131] + z[128] + n<T>(1,2)*z[114] + z[125];
    z[101]=z[12]*z[101];
    z[107]=n<T>(331,3)*z[98] - 116*z[86];
    z[107]=z[1]*z[107];
    z[114]= - n<T>(331,3)*z[3] + 116*z[4];
    z[114]= - n<T>(491,12)*z[6] + 2*z[114] + n<T>(1409,6)*z[105];
    z[114]=z[114]*z[95];
    z[125]=n<T>(983,3)*z[3] - 554*z[4];
    z[125]= - n<T>(1549,12)*z[8] + 2*z[125] + n<T>(3815,6)*z[88];
    z[125]=z[8]*z[125];
    z[128]=166*z[3] - n<T>(541,6)*z[4];
    z[128]=z[4]*z[128];
    z[107]=n<T>(1,9)*z[125] + z[114] + n<T>(2,3)*z[107] + n<T>(1072,9)*z[85] + 
    z[128];
    z[107]=z[107]*z[117];
    z[114]=487*z[3] + 149*z[4];
    z[114]=z[4]*z[114];
    z[114]= - n<T>(1540,3)*z[85] + z[114];
    z[125]=n<T>(1951,9)*z[98] + 328*z[86];
    z[125]=z[1]*z[125];
    z[128]=n<T>(3713,27)*z[6] - n<T>(1571,9)*z[105] + n<T>(191,18)*z[3] - 215*z[4];
    z[128]=z[6]*z[128];
    z[114]=z[128] + n<T>(1,3)*z[114] + z[125];
    z[114]=z[6]*z[114];
    z[125]= - 1 - n<T>(45971,459)*z[3];
    z[93]=z[125]*z[93];
    z[125]=n<T>(3625,4)*z[3] - 338*z[4];
    z[125]=z[4]*z[125];
    z[125]=n<T>(1757,4)*z[85] + z[125];
    z[128]=n<T>(1,51)*z[4];
    z[125]=z[125]*z[128];
    z[131]= - 83*z[98] - n<T>(2657,12)*z[86];
    z[131]=z[4]*z[131];
    z[131]= - n<T>(15733,72)*z[104] + z[131];
    z[111]=z[131]*z[111];
    z[93]=z[107] + n<T>(1,17)*z[114] + z[111] + z[93] + z[125];
    z[93]=z[93]*z[99];
    z[107]= - n<T>(113,80) + n<T>(494,459)*z[3];
    z[107]=i*z[107];
    z[107]=z[107] - n<T>(35,34)*z[86];
    z[107]=z[1]*z[107];
    z[111]=n<T>(93,17)*z[3];
    z[114]=n<T>(31,17)*z[105];
    z[125]=z[114] + n<T>(7399,540) - z[111];
    z[125]=z[6]*z[125];
    z[97]= - n<T>(1391,54)*z[105] + z[97];
    z[97]=z[97]*z[117];
    z[131]=n<T>(83,2) - n<T>(535,17)*z[3];
    z[131]= - n<T>(554,459)*z[8] + n<T>(1,54)*z[131] - z[119];
    z[131]=z[9]*z[131];
    z[97]=z[131] - n<T>(1,12)*z[7] + z[97] + n<T>(1,4)*z[125] + z[109] + z[107];
    z[97]=z[9]*z[97];
    z[107]=z[122] - n<T>(23,10);
    z[109]=z[107]*z[112];
    z[109]=z[109] + z[113];
    z[110]=z[110]*z[105];
    z[112]=z[115]*z[105];
    z[110]=z[110] - z[112];
    z[113]=n<T>(161,27)*z[98] - 10*z[86];
    z[113]=z[1]*z[113];
    z[113]=n<T>(277,9)*z[85] + 7*z[113] - z[110];
    z[113]=z[113]*z[117];
    z[111]= - n<T>(61,60) - z[111];
    z[111]=z[111]*z[105];
    z[111]=n<T>(61,120)*z[6] - n<T>(93,34)*z[85] + z[111];
    z[111]= - n<T>(1,15)*z[7] + n<T>(1,2)*z[111];
    z[111]=z[6]*z[111];
    z[117]= - n<T>(59,5) + n<T>(1907,51)*z[3];
    z[117]=z[117]*z[123];
    z[117]=z[117] + z[127];
    z[117]=z[1]*z[117];
    z[97]=z[97] + z[113] + n<T>(1,2)*z[109] + z[117] + z[111];
    z[97]=z[9]*z[97];
    z[109]= - n<T>(32,5) - n<T>(23725,68)*z[3];
    z[109]=z[3]*z[109];
    z[111]= - n<T>(12473,68)*z[4] - 17 - n<T>(86555,34)*z[3];
    z[111]=z[4]*z[111];
    z[113]=n<T>(47153,272)*z[6] - n<T>(58391,136)*z[4] + n<T>(319,48) - n<T>(1579,17)*
    z[3];
    z[113]=z[6]*z[113];
    z[109]=z[113] + z[109] + n<T>(1,4)*z[111];
    z[111]=n<T>(6595,204)*z[8] - n<T>(31925,306)*z[6] + n<T>(9541,51)*z[4] - n<T>(121,45)
    - n<T>(9445,34)*z[3];
    z[111]=z[8]*z[111];
    z[109]=n<T>(1,9)*z[109] + n<T>(1,4)*z[111];
    z[111]= - n<T>(163,68)*z[6] - n<T>(5971,136)*z[4] + n<T>(188,15) + n<T>(7019,68)*
    z[3];
    z[111]=n<T>(1,27)*z[111] + n<T>(229,136)*z[8];
    z[111]=z[7]*z[111];
    z[109]=n<T>(1,3)*z[109] + z[111];
    z[111]= - n<T>(49,10) + n<T>(5557,51)*z[3];
    z[111]=n<T>(1,18)*z[111] + z[118];
    z[113]=z[12] - z[9];
    z[111]=n<T>(1,2)*z[111] + z[124] - n<T>(31,34)*z[113];
    z[111]=z[12]*z[111];
    z[113]=n<T>(31,17)*z[6];
    z[117]= - n<T>(289,10) - n<T>(1229,17)*z[3];
    z[117]=n<T>(1,54)*z[117] - n<T>(10,17)*z[4];
    z[117]=n<T>(1403,459)*z[8] + 7*z[117] + z[113];
    z[117]=n<T>(1,3)*z[117] + n<T>(31,68)*z[9];
    z[117]=z[9]*z[117];
    z[118]=n<T>(1285,51)*z[4] - n<T>(11,12) - n<T>(2315,17)*z[3];
    z[118]=n<T>(439,68)*z[16] + n<T>(439,34)*z[7] + n<T>(1,2)*z[118] + z[120];
    z[118]=z[16]*z[118];
    z[87]=n<T>(113,135) + z[87];
    z[87]=z[2]*z[87];
    z[87]=z[111] + n<T>(2,9)*z[87] + n<T>(1,27)*z[118] + n<T>(1,3)*z[109] + z[117];
    z[87]=z[41]*z[87];
    z[109]=n<T>(1,10)*z[6];
    z[111]=n<T>(53,3)*z[105] - 19*z[6];
    z[111]=z[111]*z[109];
    z[117]=z[3] + z[105];
    z[99]=z[117]*z[99];
    z[99]=z[111] + z[99];
    z[109]= - n<T>(1,54)*z[7] - n<T>(1,6)*z[8] + n<T>(1,27)*z[105] - z[109];
    z[109]=z[7]*z[109];
    z[111]=n<T>(1,3)*z[9];
    z[117]=n<T>(149,15)*z[6] + 11 - n<T>(511,45)*z[105];
    z[117]= - n<T>(328,45)*z[9] + n<T>(1,4)*z[117] + n<T>(5,9)*z[7];
    z[117]=z[117]*z[111];
    z[118]=4*z[6];
    z[120]= - n<T>(203,36)*z[105] + z[118];
    z[111]= - z[111] + n<T>(1,3)*z[120] + n<T>(1,2)*z[7];
    z[111]=n<T>(1,5)*z[111] + n<T>(1,216)*z[2];
    z[111]=z[2]*z[111];
    z[120]= - 11 - z[3];
    z[120]=n<T>(1475,108)*z[5] - n<T>(157,180)*z[2] + n<T>(11,6)*z[16] - n<T>(223,18)*
    z[9] - n<T>(961,180)*z[6] + n<T>(1,2)*z[120] - n<T>(211,45)*z[105];
    z[120]=z[5]*z[120];
    z[123]=npow(z[16],2);
    z[99]=n<T>(1,6)*z[120] + n<T>(7,360)*z[41] + z[111] + n<T>(152,135)*z[123] + 
    z[117] + n<T>(1,2)*z[99] + z[109];
    z[99]=z[5]*z[99];
    z[109]=n<T>(107,27)*z[98] + 14*z[86];
    z[111]=5*z[1];
    z[109]=z[109]*z[111];
    z[111]=14*z[4] + n<T>(107,27)*z[3];
    z[111]=5*z[111];
    z[116]=z[111] + z[116];
    z[117]=n<T>(271,27)*z[105] - z[116];
    z[117]=z[8]*z[117];
    z[116]=z[116] + 31*z[105];
    z[120]= - z[12]*z[116];
    z[112]= - 31*z[41] + z[120] + z[117] + z[109] - z[112];
    z[112]=z[14]*z[112];
    z[109]=z[109] + z[110];
    z[110]=z[8] + z[12];
    z[109]=z[109]*z[110];
    z[110]= - 31*z[12] + n<T>(271,27)*z[8] + z[111] - z[115];
    z[110]=z[41]*z[110];
    z[109]=z[110] + z[109];
    z[109]=z[13]*z[109];
    z[110]=z[35]*z[116];
    z[111]=n<T>(79,3)*z[3] + 107*z[4] + n<T>(721,9)*z[88];
    z[111]=n<T>(1,2)*z[111] + 46*z[8];
    z[111]=z[29]*z[111];
    z[109]=z[109] + z[112] + z[111] + z[110];
    z[110]=n<T>(229,34)*z[8];
    z[111]= - z[110] + n<T>(163,459)*z[6];
    z[112]=z[105] - z[4];
    z[112]=z[112]*z[111];
    z[115]= - n<T>(4,5) + n<T>(2429,51)*z[3];
    z[116]=n<T>(4565,102)*z[4] + z[115];
    z[103]=z[116]*z[103];
    z[116]=n<T>(37,5) + n<T>(2429,17)*z[3];
    z[117]=z[116]*i;
    z[120]= - n<T>(1,27)*z[117] - n<T>(157,34)*z[86];
    z[120]=z[1]*z[120];
    z[123]=n<T>(4565,34)*z[4] + z[116] + n<T>(163,17)*z[88];
    z[110]=n<T>(1,27)*z[123] + z[110];
    z[110]=z[7]*z[110];
    z[103]=n<T>(163,459)*z[41] + z[110] + z[103] + z[120] + z[112];
    z[103]=z[19]*z[103];
    z[110]=218*z[3] - n<T>(4903,3)*z[4];
    z[100]=z[110]*z[100];
    z[100]= - 151*z[85] + z[100];
    z[100]=z[4]*z[100];
    z[110]=n<T>(1643,3)*z[98] - 151*z[86];
    z[110]=z[4]*z[110];
    z[110]=n<T>(793,3)*z[104] + z[110];
    z[110]=z[1]*z[110];
    z[100]=z[110] + n<T>(1262,9)*z[102] + z[100];
    z[102]= - 71*z[3] + n<T>(1069,2)*z[4];
    z[102]=z[4]*z[102];
    z[102]=797*z[85] + z[102];
    z[110]= - 371*z[98] - 677*z[86];
    z[112]=4*z[1];
    z[110]=z[110]*z[112];
    z[102]=n<T>(1,2)*z[102] + z[110];
    z[110]= - n<T>(16153,136)*z[6] + n<T>(7081,34)*z[105] + n<T>(3344,17)*z[4] - n<T>(139,20)
     + n<T>(1340,17)*z[3];
    z[110]=z[110]*z[95];
    z[102]=n<T>(1,17)*z[102] + z[110];
    z[95]=z[102]*z[95];
    z[95]=n<T>(1,17)*z[100] + z[95];
    z[95]=z[6]*z[95];
    z[100]= - n<T>(7,6) - z[121];
    z[100]=z[4]*z[100];
    z[102]= - n<T>(16,5) - z[121];
    z[102]=z[16]*z[102];
    z[100]=z[102] + z[100];
    z[100]=z[21]*z[100];
    z[95]=z[95] + z[100];
    z[100]= - n<T>(89,3)*z[98] - 2*z[86];
    z[100]=z[1]*z[100];
    z[102]=n<T>(89,3)*z[3] + 2*z[4] + z[130] - z[6];
    z[102]=z[6]*z[102];
    z[100]=z[100] + z[102];
    z[102]= - n<T>(23,3)*z[3] - z[4];
    z[89]=z[102]*z[89];
    z[102]= - 1 - n<T>(109,6)*z[3];
    z[102]=z[3]*z[102];
    z[110]= - n<T>(478,17)*z[4] - 1 - n<T>(1526,51)*z[3] - n<T>(178,17)*z[88];
    z[110]=n<T>(1,3)*z[110] - 6*z[8];
    z[110]=z[8]*z[110];
    z[89]= - n<T>(86,153)*z[20] - n<T>(37,408)*z[41] + z[110] + n<T>(1,3)*z[102]
     + 
    z[89] + n<T>(2,17)*z[100];
    z[89]=z[20]*z[89];
    z[100]=i*z[115];
    z[100]=z[100] + n<T>(4565,102)*z[86];
    z[100]=z[1]*z[100];
    z[102]=z[105]*z[6];
    z[100]=z[100] - n<T>(163,51)*z[102];
    z[110]=z[105]*z[8];
    z[115]=n<T>(229,34)*z[110];
    z[100]=n<T>(1,9)*z[100] + z[115];
    z[100]=z[4]*z[100];
    z[117]=z[117] + n<T>(4565,34)*z[86];
    z[117]=z[1]*z[117];
    z[117]=z[117] - n<T>(163,17)*z[102];
    z[115]=n<T>(1,27)*z[117] + z[115];
    z[115]=z[7]*z[115];
    z[111]= - n<T>(163,459)*z[7] + n<T>(1,27)*z[116] + n<T>(157,34)*z[4] - z[111];
    z[111]=z[41]*z[111];
    z[100]=z[111] + z[115] + z[100];
    z[100]=z[17]*z[100];
    z[111]= - 101*z[104] - n<T>(478,3)*z[126];
    z[111]=z[111]*z[94];
    z[115]= - 4*z[98] + n<T>(53,3)*z[86];
    z[94]= - z[115]*z[94];
    z[94]=z[94] + 14*z[102];
    z[94]=z[94]*z[118];
    z[94]=z[111] + z[94];
    z[111]= - n<T>(53,3)*z[4] + 4*z[3];
    z[116]=n<T>(247,3)*z[6] + z[111];
    z[92]=n<T>(2,17)*z[116] - z[92];
    z[92]=n<T>(10,153)*z[15] + n<T>(2,3)*z[92];
    z[92]=z[41]*z[92];
    z[116]= - n<T>(7,3)*z[98] + z[86];
    z[116]=z[1]*z[116];
    z[116]=z[116] - n<T>(7,3)*z[102];
    z[110]=2*z[116] - z[110];
    z[110]=z[8]*z[110];
    z[92]=n<T>(1,17)*z[94] + z[110] + z[92];
    z[92]=z[15]*z[92];
    z[94]=n<T>(163,68)*z[7] + n<T>(1373,80) - n<T>(1025,17)*z[3];
    z[94]=i*z[94];
    z[110]=z[8]*i;
    z[116]=z[6]*i;
    z[94]=n<T>(11039,68)*z[110] - n<T>(12929,136)*z[116] + n<T>(413,4)*z[86] + z[94]
   ;
    z[117]= - n<T>(439,459)*z[16] - n<T>(31,17)*z[9];
    z[117]=i*z[117];
    z[94]=n<T>(1,27)*z[94] + z[117];
    z[117]=n<T>(31,17)*z[13] - n<T>(3157,918)*z[15] + n<T>(163,459)*z[17] + n<T>(31,68)*
    z[12];
    z[117]=i*z[117];
    z[94]=n<T>(1,3)*z[94] + z[117];
    z[94]=z[42]*z[94];
    z[117]=n<T>(109,3)*z[5];
    z[120]=z[105]*z[117];
    z[102]=z[102] + z[41];
    z[102]=17*z[102] + z[120];
    z[102]=z[11]*z[102];
    z[117]= - 17*z[88] + z[117];
    z[117]=z[24]*z[117];
    z[102]=z[102] + z[117];
    z[117]= - 1 - n<T>(20899,612)*z[3];
    z[117]=z[117]*z[98];
    z[120]= - 419*z[98] + n<T>(5917,18)*z[86];
    z[120]=z[120]*z[128];
    z[117]=z[117] + z[120];
    z[117]=z[4]*z[117];
    z[120]= - n<T>(7,8) - n<T>(8630,1377)*z[3];
    z[104]=z[120]*z[104];
    z[104]=z[104] + n<T>(1,6)*z[117];
    z[104]=z[1]*z[104];
    z[112]= - z[115]*z[112];
    z[115]=z[4]*z[3];
    z[85]=z[112] + 101*z[85] + n<T>(478,3)*z[115];
    z[111]=n<T>(247,3)*z[105] - z[111];
    z[111]=n<T>(1,3)*z[111] - 14*z[6];
    z[111]=z[111]*z[118];
    z[85]=n<T>(1,3)*z[85] + z[111];
    z[111]=z[88] - z[3];
    z[111]= - z[4] - n<T>(7,3)*z[111];
    z[111]=2*z[111] + z[8];
    z[111]=z[8]*z[111];
    z[112]=z[15]*z[105];
    z[85]= - n<T>(10,153)*z[18] + n<T>(20,153)*z[112] + n<T>(3157,918)*z[41] + n<T>(1,17)
   *z[85] + z[111];
    z[85]=z[18]*z[85];
    z[111]=z[122] - n<T>(89,20);
    z[111]=z[114] + n<T>(1,3)*z[111] + n<T>(70,17)*z[4] - z[113] + n<T>(1108,459)*
    z[8];
    z[112]= - z[8] - z[9];
    z[111]=z[22]*z[111]*z[112];
    z[105]=z[108] - z[129] + n<T>(93,34)*z[105];
    z[106]=z[106] + z[105];
    z[106]=n<T>(1,2)*z[106] + z[132];
    z[106]=z[36]*z[106];
    z[105]=n<T>(1,2)*z[107] + z[105];
    z[105]=n<T>(1,2)*z[105] + z[132];
    z[105]=z[39]*z[105];
    z[86]=z[110] + z[86];
    z[86]=n<T>(296,153)*z[116] + n<T>(163,153)*z[98] + n<T>(1,2)*z[86];
    z[86]=z[44]*z[86];
    z[98]=z[130] - z[2];
    z[107]= - z[6] + z[98];
    z[107]=z[23]*z[107];
    z[98]= - z[7] + z[98];
    z[98]=z[25]*z[98];
    z[98]= - z[47] - z[107] - z[98] + z[60] + z[59];
    z[107]=z[34] - z[31];
    z[107]=n<T>(2,9)*z[107];
    z[83]=z[83]*z[107];
    z[107]=n<T>(23,5) + n<T>(8219,17)*z[3];
    z[107]=n<T>(7135,102)*z[8] + n<T>(1,9)*z[107] + n<T>(1879,34)*z[4] + n<T>(71,51)*
    z[88];
    z[107]=z[28]*z[107];
    z[108]=n<T>(109,10) - z[122];
    z[108]=n<T>(1,6)*z[108] - z[119];
    z[108]=n<T>(1,2)*z[108] - z[124];
    z[108]=z[38]*z[108];
    z[110]=z[81] + z[79];
    z[112]=n<T>(1147,918)*z[52] - n<T>(188,405)*z[48] - n<T>(94,405)*z[46];
    z[112]=i*z[112];
    z[113]= - n<T>(1109,306)*z[8] - n<T>(1349,918)*z[4] - n<T>(1,5) - n<T>(499,153)*z[3]
    - n<T>(439,459)*z[88];
    z[113]=z[32]*z[113];
    z[114]=n<T>(1109,2)*z[8] + 499*z[3] + n<T>(1349,6)*z[4] + n<T>(439,3)*z[88];
    z[114]=z[27]*z[114];
    z[115]=n<T>(8600,27)*z[3] + 111*z[4] - n<T>(320,3)*z[88];
    z[115]=n<T>(1,17)*z[115] + n<T>(121,9)*z[8];
    z[115]=z[30]*z[115];
    z[88]=n<T>(43,3)*z[3] + 113*z[4] + 89*z[88];
    z[88]=n<T>(1,2)*z[88] + 49*z[8];
    z[88]=z[37]*z[88];
    z[116]=n<T>(6935,34)*z[4] + n<T>(3539,10) - n<T>(101423,17)*z[3];
    z[116]=n<T>(55007,136)*z[8] + n<T>(1,4)*z[116] - n<T>(970,17)*z[6];
    z[116]=z[50]*z[116];
    z[117]=n<T>(278,9)*z[3] + 21*z[4];
    z[117]=n<T>(1,17)*z[117] + n<T>(8,9)*z[8];
    z[117]=z[57]*z[117];
    z[118]= - n<T>(643,3)*z[16] - 301*z[5];
    z[118]=z[56]*z[118];

    r +=  - n<T>(47,2430)*z[45] + n<T>(94,405)*z[49] - n<T>(8707,110160)*z[51] + n<T>(41,153)*z[53]
     - 8*z[55]
     + n<T>(7319,2160)*z[58]
     + n<T>(4439,2160)*z[61]
     +  n<T>(449,720)*z[62] + n<T>(1,108)*z[63] - n<T>(461,306)*z[64] + n<T>(215,153)*
      z[65] + n<T>(215,459)*z[66] - n<T>(227,102)*z[67] + n<T>(356,153)*z[68] + n<T>(26,459)*z[69]
     - n<T>(857,306)*z[70]
     - n<T>(100,17)*z[71]
     + n<T>(49,102)*z[72]
     +  n<T>(1091,612)*z[73] + n<T>(217,459)*z[74] - n<T>(2183,918)*z[75] - n<T>(877,306)
      *z[76] + n<T>(118,153)*z[77] - n<T>(64,459)*z[78] - n<T>(89,102)*z[80] + n<T>(25,51)*z[82]
     + z[83]
     + z[84]
     + z[85]
     + z[86]
     + z[87]
     + n<T>(1,51)*z[88]
       + z[89] + n<T>(1,3)*z[90] + z[91] + z[92] + z[93] + z[94] + n<T>(1,9)*
      z[95] + z[96] + z[97] - n<T>(47,405)*z[98] + z[99] + z[100] + z[101]
       + n<T>(1,20)*z[102] + z[103] + z[104] + z[105] + z[106] + n<T>(1,12)*
      z[107] + z[108] + n<T>(1,17)*z[109] - n<T>(1,34)*z[110] + z[111] + z[112]
       + n<T>(1,4)*z[113] + n<T>(1,153)*z[114] + z[115] + n<T>(1,81)*z[116] + 4*
      z[117] + n<T>(1,180)*z[118];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf415(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf415(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
