#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1028(
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[7];
  std::complex<T> r(0,0);

    z[1]=d[0];
    z[2]=d[4];
    z[3]=d[9];
    z[4]=d[2];
    z[5]=z[4] - z[1];
    z[6]=z[3] - z[2];

    r += z[6]*z[5];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1028(
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1028(
  const std::array<std::complex<dd_real>,25>& d
);
#endif
