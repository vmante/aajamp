#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf706(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[59];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[4];
    z[11]=d[5];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[2];
    z[16]=e[8];
    z[17]=e[9];
    z[18]=e[10];
    z[19]=c[3];
    z[20]=d[9];
    z[21]=c[11];
    z[22]=c[15];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[3];
    z[30]=e[6];
    z[31]=f[3];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[11];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[17];
    z[38]=f[31];
    z[39]=f[36];
    z[40]=f[39];
    z[41]=n<T>(1,2)*z[5];
    z[42]=n<T>(1,2)*z[2];
    z[43]=z[1]*i;
    z[44]=z[42] + z[41] - n<T>(1,2)*z[43] - z[7];
    z[44]=z[2]*z[44];
    z[45]=npow(z[7],2);
    z[46]= - n<T>(39679,18372)*z[5] + n<T>(39679,9186)*z[43] - z[7];
    z[46]=z[5]*z[46];
    z[46]=n<T>(3,2)*z[45] + z[46];
    z[47]=7139*z[43] - n<T>(9685,2)*z[5];
    z[47]=n<T>(1937,9186)*z[3] + n<T>(1,4593)*z[47] - z[42];
    z[47]=z[3]*z[47];
    z[48]=z[43] - z[5];
    z[49]= - 6561*z[48] - n<T>(4249,2)*z[10];
    z[49]=z[10]*z[49];
    z[50]= - n<T>(1937,8)*z[48] + 7139*z[3];
    z[50]=n<T>(1,3)*z[50] - n<T>(19683,8)*z[10];
    z[50]=z[8]*z[50];
    z[44]=n<T>(1,1531)*z[50] + n<T>(3,6124)*z[49] + z[47] + n<T>(1,2)*z[46] + z[44];
    z[44]=z[8]*z[44];
    z[46]=n<T>(1,2)*z[7];
    z[47]= - 11591*z[43] - n<T>(1875,2)*z[7];
    z[47]=z[47]*z[46];
    z[49]= - 3015*z[43] + 1406*z[7];
    z[49]=3*z[49] + n<T>(4249,2)*z[5];
    z[49]=z[5]*z[49];
    z[50]=1359*z[5] + 13903*z[43] + 2515*z[7];
    z[50]=n<T>(1,2)*z[50] - 3093*z[2];
    z[50]=z[2]*z[50];
    z[47]=z[50] + z[47] + z[49];
    z[47]=z[2]*z[47];
    z[49]=z[46] - z[43];
    z[50]=z[49]*z[7];
    z[41]=z[41] - z[43];
    z[51]=z[5]*z[41];
    z[52]=n<T>(228203,4)*z[43] - 35399*z[7];
    z[52]=n<T>(1,3)*z[52] - n<T>(28869,4)*z[5];
    z[52]=n<T>(12599,3)*z[10] + n<T>(1,2)*z[52] + 5202*z[3];
    z[52]=z[10]*z[52];
    z[51]=z[52] + n<T>(10201,3)*z[50] - n<T>(19683,4)*z[51];
    z[51]=z[10]*z[51];
    z[52]=z[43] - z[7];
    z[53]= - n<T>(7139,3)*z[8] + 1734*z[52] + n<T>(3265,3)*z[3];
    z[53]=z[18]*z[53];
    z[47]=z[53] + z[47] + z[51];
    z[46]=z[42] - n<T>(8061,3062)*z[5] + n<T>(1734,1531)*z[43] + z[46];
    z[46]=z[2]*z[46];
    z[51]=2*z[43];
    z[53]=z[51] - z[7];
    z[53]=z[7]*z[53];
    z[54]= - n<T>(9685,4593)*z[43] - z[7];
    z[54]=n<T>(1,2)*z[54] + n<T>(16934,4593)*z[5];
    z[54]=z[5]*z[54];
    z[55]= - n<T>(24073,6)*z[3] + n<T>(4999,4)*z[2] + n<T>(3265,3)*z[5] + n<T>(8467,12)*
    z[43] - 867*z[7];
    z[55]=z[3]*z[55];
    z[46]=n<T>(1,1531)*z[55] + z[46] - n<T>(867,1531)*z[53] + z[54];
    z[46]=z[3]*z[46];
    z[53]= - 25198*z[43] + n<T>(57019,4)*z[7];
    z[53]=n<T>(1,4593)*z[53] + z[5];
    z[53]=z[7]*z[53];
    z[54]=z[2]*z[48];
    z[55]=2*z[52] + z[10];
    z[55]=z[10]*z[55];
    z[56]=15809*z[43] + 24995*z[7];
    z[56]= - n<T>(24995,9186)*z[6] + n<T>(1,9186)*z[56] + z[2];
    z[56]=z[6]*z[56];
    z[53]=n<T>(1,2)*z[56] + n<T>(12599,4593)*z[55] + z[54] + z[53];
    z[53]=z[6]*z[53];
    z[54]=24995*z[6];
    z[55]= - 15809*z[7] - z[54];
    z[55]=z[43]*z[55];
    z[55]= - 15809*z[19] + z[55];
    z[55]=z[12]*z[55];
    z[54]=15809*z[52] - z[54];
    z[54]=z[17]*z[54];
    z[54]=z[55] + z[54];
    z[55]=1306*z[3];
    z[56]= - n<T>(5855,2)*z[5] - z[55];
    z[56]=z[43]*z[56];
    z[56]= - n<T>(5855,2)*z[19] + z[56];
    z[56]=z[9]*z[56];
    z[48]=n<T>(5855,2)*z[48] - z[55];
    z[48]=z[15]*z[48];
    z[48]=z[56] + z[48];
    z[55]=3*z[7];
    z[49]= - z[49]*z[55];
    z[41]=z[55] - z[41];
    z[41]=z[5]*z[41];
    z[41]=z[49] + z[41];
    z[42]=z[42] - z[51] - z[5];
    z[42]=z[2]*z[42];
    z[49]= - z[55] - z[5];
    z[49]=n<T>(1,4)*z[49] + z[2];
    z[49]=z[4]*z[49];
    z[41]=z[49] + n<T>(1,2)*z[41] + z[42];
    z[41]=z[4]*z[41];
    z[42]=z[51] - z[2];
    z[49]= - z[4] + z[42];
    z[49]=z[13]*z[49];
    z[41]=z[41] + z[49];
    z[49]= - n<T>(1,2)*z[10] - z[52];
    z[49]=z[10]*z[49];
    z[51]= - z[10] - z[52];
    z[51]=z[11]*z[51];
    z[49]=n<T>(1,2)*z[51] + n<T>(1,3)*z[19] - z[50] + z[49];
    z[49]=z[11]*z[49];
    z[51]=z[43] - n<T>(1,6)*z[7];
    z[45]=z[51]*z[45];
    z[43]= - n<T>(1,6)*z[5] + z[43] - n<T>(10779,6124)*z[7];
    z[43]=z[5]*z[43];
    z[43]= - n<T>(7717,3062)*z[50] + z[43];
    z[43]=z[5]*z[43];
    z[50]=z[33] - z[25];
    z[51]=z[40] - z[38];
    z[52]=186773*z[7] + n<T>(40745,2)*z[5];
    z[52]=n<T>(1,12)*z[52] - 9014*z[2];
    z[52]=n<T>(781,12)*z[4] - n<T>(8467,6)*z[6] + n<T>(44771,36)*z[8] + n<T>(131065,36)*
    z[10] + n<T>(1,3)*z[52] + n<T>(4999,4)*z[3];
    z[52]=n<T>(578,1531)*z[20] + n<T>(1,1531)*z[52];
    z[52]=z[19]*z[52];
    z[55]= - n<T>(10841,1531)*z[26] - n<T>(10841,3062)*z[24] - n<T>(3015,6124)*z[21]
   ;
    z[55]=i*z[55];
    z[56]= - z[5] + z[42];
    z[56]=n<T>(2890,1531)*z[56] + z[6];
    z[56]=z[14]*z[56];
    z[42]= - z[7] + z[42];
    z[42]=z[16]*z[42];
    z[57]= - z[3] - z[10];
    z[57]=z[29]*z[57];
    z[58]=z[10] + z[6];
    z[58]=z[30]*z[58];

    r += n<T>(27713,6124)*z[22] - n<T>(10841,36744)*z[23] + n<T>(10841,3062)*z[27]
       - n<T>(234981,12248)*z[28] - n<T>(781,6124)*z[31] + n<T>(1937,36744)*z[32]
       - n<T>(7717,6124)*z[34] + n<T>(28869,12248)*z[35] - n<T>(12747,12248)*z[36]
       - n<T>(2343,6124)*z[37] + n<T>(35399,9186)*z[39] + n<T>(781,1531)*z[41] + 
      n<T>(2515,1531)*z[42] + z[43] + z[44] + n<T>(11935,3062)*z[45] + z[46] + 
      n<T>(1,1531)*z[47] + n<T>(5,4593)*z[48] + n<T>(4999,1531)*z[49] - n<T>(10841,6124)
      *z[50] - n<T>(4999,3062)*z[51] + z[52] + z[53] + n<T>(1,9186)*z[54] + 
      z[55] + z[56] + n<T>(5202,1531)*z[57] + n<T>(25198,4593)*z[58];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf706(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf706(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
