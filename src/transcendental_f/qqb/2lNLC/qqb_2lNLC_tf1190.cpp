#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf1190(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[26];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=e[2];
    z[8]=e[10];
    z[9]=c[3];
    z[10]=c[15];
    z[11]=c[31];
    z[12]=f[4];
    z[13]=f[12];
    z[14]=f[16];
    z[15]=2*z[5];
    z[16]=z[1]*i;
    z[17]=z[16] + z[15];
    z[17]=z[5]*z[17];
    z[18]= - z[4] - 3*z[16] - z[15];
    z[18]=z[4]*z[18];
    z[19]=3*z[5];
    z[20]=z[19] - z[4];
    z[21]= - z[2] + z[16] + z[20];
    z[21]=z[2]*z[21];
    z[22]=z[8] - z[7];
    z[23]=n<T>(1,6)*z[9];
    z[24]=z[6]*z[16];
    z[25]= - z[5] + 3*z[4];
    z[25]=n<T>(1,2)*z[25] - z[2];
    z[25]=z[3]*z[25];
    z[17]=z[25] + z[21] + z[18] + z[17] + z[24] + z[23] - z[22];
    z[17]=z[3]*z[17];
    z[18]=z[4] + z[6];
    z[18]=z[16]*z[18];
    z[21]=z[16] + z[5];
    z[19]= - z[21]*z[19];
    z[24]=3*z[8];
    z[20]= - n<T>(1,2)*z[20] + 2*z[2];
    z[20]=z[2]*z[20];
    z[18]=z[20] + z[19] + z[24] + z[7] - z[23] + z[18];
    z[18]=z[2]*z[18];
    z[19]=z[22]*z[16];
    z[15]= - z[16]*z[15];
    z[15]=z[24] + z[15];
    z[15]=z[5]*z[15];
    z[16]=2*z[16] + z[5];
    z[16]=z[5]*z[16];
    z[20]=z[4]*z[21];
    z[16]=z[20] + z[16] - n<T>(4,3)*z[9] + z[8];
    z[16]=z[4]*z[16];
    z[20]=z[9]*z[6];

    r +=  - z[10] + n<T>(11,2)*z[11] + 2*z[12] + z[13] + z[14] + z[15] + 
      z[16] + z[17] + z[18] + z[19] + z[20];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf1190(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf1190(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
