#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf304(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[63];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[2];
    z[9]=d[5];
    z[10]=d[4];
    z[11]=d[6];
    z[12]=d[17];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[3];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=c[3];
    z[21]=d[9];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[6];
    z[31]=e[14];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[11];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[23];
    z[38]=f[31];
    z[39]=f[36];
    z[40]=f[39];
    z[41]=f[51];
    z[42]=f[55];
    z[43]=f[58];
    z[44]=n<T>(1,2)*z[6];
    z[45]=z[1]*i;
    z[46]= - z[44] + n<T>(8,3)*z[45];
    z[47]=n<T>(4,3)*z[5];
    z[48]=z[47] - z[46];
    z[49]=7*z[5];
    z[48]=z[48]*z[49];
    z[50]=n<T>(1,4)*z[6];
    z[51]=4*z[45] - z[50];
    z[52]=3*z[6];
    z[51]=z[51]*z[52];
    z[46]= - n<T>(4,3)*z[4] + z[47] + z[46];
    z[46]=z[4]*z[46];
    z[47]=z[4] + z[5];
    z[53]=z[47] + z[6];
    z[53]= - n<T>(11,4)*z[3] - 12*z[45] + n<T>(7,2)*z[53];
    z[53]=z[3]*z[53];
    z[46]=z[53] + 7*z[46] + z[51] - z[48];
    z[51]=n<T>(1,2)*z[3];
    z[47]=n<T>(28,15)*z[2] + z[51] - n<T>(56,15)*z[45] - z[44] - n<T>(14,15)*z[47];
    z[47]=z[2]*z[47];
    z[46]=n<T>(1,5)*z[46] + z[47];
    z[46]=z[2]*z[46];
    z[47]=2*z[5];
    z[53]=2*z[45];
    z[54]=z[53] - z[5];
    z[55]=z[6] + z[54];
    z[55]=z[55]*z[47];
    z[56]=z[45] + z[6];
    z[57]= - n<T>(17,3)*z[4] - n<T>(37,3)*z[56] - z[49];
    z[57]=z[4]*z[57];
    z[58]=n<T>(181,10)*z[4] + n<T>(167,5)*z[45] + n<T>(47,2)*z[6];
    z[58]=n<T>(1,3)*z[58] - 11*z[3];
    z[51]=z[58]*z[51];
    z[58]= - 22*z[45] - n<T>(263,2)*z[6];
    z[58]=z[6]*z[58];
    z[51]=z[51] + n<T>(1,10)*z[57] + n<T>(1,15)*z[58] + z[55];
    z[51]=z[3]*z[51];
    z[44]=z[44] + z[45];
    z[50]= - z[44]*z[50];
    z[55]=z[54]*z[5];
    z[57]= - n<T>(1,2)*z[4] + z[56];
    z[57]=z[4]*z[57];
    z[58]= - 69*z[45] - 29*z[6];
    z[58]= - n<T>(43,2)*z[10] + 2*z[3] - n<T>(267,8)*z[4] + n<T>(1,8)*z[58] + 42*
    z[5];
    z[58]=z[10]*z[58];
    z[50]=z[58] + n<T>(1,4)*z[57] + z[50] + 3*z[55];
    z[57]=npow(z[3],2);
    z[50]=7*z[57] + n<T>(1,5)*z[50];
    z[50]=z[10]*z[50];
    z[44]=z[6]*z[44];
    z[57]=z[45] - z[5];
    z[58]=n<T>(1,2)*z[10] + z[57];
    z[58]=z[10]*z[58];
    z[59]=21*z[45] + n<T>(37,5)*z[6];
    z[59]= - n<T>(3,10)*z[9] + n<T>(37,10)*z[10] + n<T>(1,4)*z[59] - n<T>(17,5)*z[5];
    z[59]=z[9]*z[59];
    z[44]=z[59] + n<T>(37,5)*z[58] + n<T>(37,10)*z[44] - 4*z[55];
    z[44]=z[9]*z[44];
    z[55]=241*z[3];
    z[58]=71*z[4] - z[55];
    z[58]=z[45]*z[58];
    z[58]=71*z[20] + z[58];
    z[58]=z[7]*z[58];
    z[59]= - z[45] + z[4];
    z[55]=71*z[59] - z[55];
    z[55]=z[14]*z[55];
    z[55]=z[58] + z[55];
    z[58]= - 71*z[45] + n<T>(581,2)*z[6];
    z[58]=z[6]*z[58];
    z[59]=z[4] - z[5];
    z[59]=n<T>(71,8)*z[6] + 14*z[59];
    z[59]=z[4]*z[59];
    z[48]=n<T>(1,3)*z[59] + n<T>(1,12)*z[58] + z[48];
    z[48]=z[4]*z[48];
    z[58]=z[4] - z[53] + z[2];
    z[58]=z[13]*z[58];
    z[54]=z[2] - z[54];
    z[54]=z[17]*z[54];
    z[54]=z[34] + z[58] + z[54] + z[33] - z[26];
    z[58]=z[5]*z[45];
    z[58]=z[58] + z[20];
    z[59]= - z[11]*z[45];
    z[58]=6*z[58] + z[59];
    z[58]=z[12]*z[58];
    z[59]= - 6*z[57] - z[11];
    z[59]=z[18]*z[59];
    z[58]=z[58] + z[59];
    z[52]= - n<T>(1229,120)*z[45] - z[52];
    z[52]=z[52]*npow(z[6],2);
    z[59]=z[45] + n<T>(3,2)*z[6];
    z[59]=z[6]*z[59];
    z[56]= - n<T>(1,2)*z[9] - z[56];
    z[56]=z[9]*z[56];
    z[56]=z[59] + z[56];
    z[59]= - z[6] + z[9];
    z[59]=z[8]*z[59];
    z[56]=n<T>(37,2)*z[59] + 37*z[56] + n<T>(47,6)*z[20];
    z[56]=z[8]*z[56];
    z[49]=z[53] - z[49];
    z[49]=z[5]*z[49];
    z[47]=z[47] + z[11];
    z[47]=z[11]*z[47];
    z[47]=z[49] + z[47];
    z[47]= - n<T>(11,6)*z[20] + n<T>(1,5)*z[47];
    z[47]=z[11]*z[47];
    z[49]= - z[41] + z[43] - z[42];
    z[53]= - z[6] - z[21];
    z[53]=z[31]*z[53];
    z[53]=z[53] + z[40];
    z[59]= - 17*z[6] + n<T>(47,2)*z[5];
    z[59]= - n<T>(53,15)*z[3] + n<T>(1,45)*z[59] + n<T>(5,4)*z[4];
    z[59]= - n<T>(23,30)*z[21] - n<T>(253,60)*z[9] + n<T>(5,12)*z[10] + n<T>(1,2)*z[59]
    - n<T>(14,45)*z[2];
    z[59]=z[20]*z[59];
    z[60]=n<T>(112,15)*z[27] + n<T>(56,15)*z[25] - n<T>(823,360)*z[22];
    z[60]=i*z[60];
    z[61]=z[45] + n<T>(53,3)*z[6];
    z[61]=n<T>(1,5)*z[61] + z[5];
    z[61]= - n<T>(12,5)*z[2] + 2*z[61] + n<T>(107,30)*z[3];
    z[61]=z[19]*z[61];
    z[45]= - n<T>(39,10)*z[10] + z[2] - z[45] - n<T>(119,10)*z[3];
    z[45]=z[15]*z[45];
    z[57]= - z[9] + z[57];
    z[57]=z[16]*z[57];
    z[62]= - n<T>(43,5)*z[10] - 7*z[11];
    z[62]=z[30]*z[62];

    r += n<T>(11,6)*z[23] + n<T>(14,45)*z[24] - n<T>(56,15)*z[28] + n<T>(223,15)*z[29]
       + n<T>(461,120)*z[32] + n<T>(267,40)*z[35] - n<T>(29,40)*z[36] + z[37] - n<T>(31,10)*z[38]
     - n<T>(41,10)*z[39]
     + z[44]
     + z[45]
     + z[46]
     + z[47]
     + n<T>(1,5)
      *z[48] + n<T>(37,20)*z[49] + z[50] + z[51] + z[52] + n<T>(37,10)*z[53] + 
      n<T>(28,15)*z[54] + n<T>(1,30)*z[55] + n<T>(1,10)*z[56] + n<T>(3,5)*z[57] + n<T>(2,5)
      *z[58] + z[59] + z[60] + z[61] + z[62];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf304(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf304(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
