#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf92(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[77];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[9];
    z[10]=d[15];
    z[11]=d[4];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=d[11];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[7];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[3];
    z[32]=e[6];
    z[33]=e[13];
    z[34]=e[14];
    z[35]=e[12];
    z[36]=f[0];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[11];
    z[41]=f[12];
    z[42]=f[16];
    z[43]=f[17];
    z[44]=f[23];
    z[45]=f[31];
    z[46]=f[32];
    z[47]=f[34];
    z[48]=f[36];
    z[49]=f[39];
    z[50]=f[51];
    z[51]=f[55];
    z[52]=f[58];
    z[53]=f[89];
    z[54]=z[2] - z[5];
    z[55]=z[1]*i;
    z[56]= - z[8] + z[55] - z[54];
    z[56]=z[2]*z[56];
    z[57]=2*z[55];
    z[58]=n<T>(1,2)*z[5];
    z[59]= - n<T>(1,6)*z[9] + n<T>(3,2)*z[2] - z[57] + z[58];
    z[59]=z[9]*z[59];
    z[60]=npow(z[5],2);
    z[61]=4*z[55] - n<T>(5,2)*z[7];
    z[61]=z[7]*z[61];
    z[62]= - 2*z[8] - 44 + z[55];
    z[62]=z[8]*z[62];
    z[63]=4*z[3] - z[5] + z[7];
    z[63]=z[3]*z[63];
    z[64]=z[14]*z[55];
    z[56]= - z[35] + z[64] + z[59] + z[63] + z[56] + z[62] - n<T>(1,2)*z[60]
    + z[61];
    z[56]=z[9]*z[56];
    z[59]=892*z[3];
    z[61]= - n<T>(1639,2)*z[5] + z[59];
    z[61]=z[55]*z[61];
    z[61]= - n<T>(1639,2)*z[22] + z[61];
    z[61]=z[10]*z[61];
    z[62]=299*z[12];
    z[63]= - n<T>(932,3)*z[7] + z[62];
    z[63]=z[55]*z[63];
    z[63]= - n<T>(932,3)*z[22] + z[63];
    z[63]=z[13]*z[63];
    z[64]=z[55] - z[5];
    z[59]=n<T>(1639,2)*z[64] + z[59];
    z[59]=z[17]*z[59];
    z[65]=z[55] - z[7];
    z[62]=n<T>(932,3)*z[65] + z[62];
    z[62]=z[20]*z[62];
    z[66]=1364*z[3] + n<T>(1189,6)*z[11];
    z[66]=z[31]*z[66];
    z[59]=z[66] + z[61] + z[63] + z[59] + z[62];
    z[61]= - 881*z[55] + n<T>(1573,2)*z[5];
    z[61]=z[5]*z[61];
    z[62]=n<T>(77,18)*z[7] - n<T>(77,9)*z[55] + n<T>(53,10)*z[5];
    z[62]=z[7]*z[62];
    z[63]=n<T>(25547,180)*z[8] - n<T>(61,9)*z[7] - n<T>(257,5)*z[5] + 88 + n<T>(5983,90)
   *z[55];
    z[63]=z[8]*z[63];
    z[66]= - n<T>(83,2)*z[8] - n<T>(53,2)*z[7] + n<T>(269,18)*z[55] + 16*z[5];
    z[66]=n<T>(1,5)*z[66] - n<T>(4,9)*z[2];
    z[66]=z[2]*z[66];
    z[67]=n<T>(125,54)*z[3] + n<T>(473,90)*z[2] + n<T>(169,18)*z[8] - n<T>(1463,45)*z[5]
    - 44 - n<T>(2899,90)*z[55];
    z[67]=z[3]*z[67];
    z[61]=z[67] + z[66] + z[63] + n<T>(1,15)*z[61] + z[62];
    z[61]=z[3]*z[61];
    z[62]=2*z[5];
    z[63]=n<T>(1,2)*z[2];
    z[66]=z[63] + z[55] - z[62];
    z[67]=3*z[2];
    z[66]=z[66]*z[67];
    z[68]=z[57] - z[7];
    z[69]=z[68]*z[7];
    z[70]=z[5]*z[55];
    z[70]=3*z[70] + n<T>(125,9)*z[69];
    z[71]=n<T>(1,2)*z[8];
    z[72]= - z[55] - z[71];
    z[72]=z[8]*z[72];
    z[67]=n<T>(683,90)*z[6] + z[67] - n<T>(129,10)*z[8] + n<T>(507,20)*z[7] - 34*
    z[55] + n<T>(3,2)*z[5];
    z[67]=z[6]*z[67];
    z[66]=z[67] + z[66] + 2*z[70] + n<T>(89,5)*z[72];
    z[66]=z[6]*z[66];
    z[67]=7*z[54];
    z[67]=z[55]*z[67];
    z[54]=n<T>(1,3)*z[4] - z[9] - n<T>(79,10)*z[6] + n<T>(42,5)*z[8] - z[55] - n<T>(7,2)
   *z[54];
    z[54]=z[4]*z[54];
    z[70]= - n<T>(419,20)*z[8] - n<T>(79,5)*z[55] - z[7];
    z[70]=z[8]*z[70];
    z[72]=n<T>(83,4)*z[6] + 79*z[55] + 89*z[8];
    z[72]=z[6]*z[72];
    z[73]=z[9]*z[7];
    z[54]=z[54] + z[73] + n<T>(1,5)*z[72] + z[70] + z[67];
    z[54]=z[4]*z[54];
    z[62]= - n<T>(187,15)*z[55] + z[62];
    z[62]=2*z[62] + n<T>(937,45)*z[7];
    z[62]=z[7]*z[62];
    z[67]=n<T>(1,2)*z[12];
    z[70]=n<T>(1759,135)*z[12] + 5*z[5] - n<T>(673,15)*z[7];
    z[70]=z[70]*z[67];
    z[72]=npow(z[6],2);
    z[73]=2*z[64] + z[11];
    z[73]=z[11]*z[73];
    z[60]=z[70] + z[73] - n<T>(3,2)*z[72] - 2*z[60] + z[62];
    z[60]=z[12]*z[60];
    z[62]=z[58] - z[55];
    z[70]=z[5]*z[62];
    z[64]= - 783*z[64] - n<T>(2429,6)*z[8];
    z[64]=z[64]*z[71];
    z[64]=z[64] - n<T>(783,2)*z[70] - n<T>(233,9)*z[69];
    z[69]=npow(z[3],2);
    z[70]= - 2*z[65] - z[6];
    z[70]=z[6]*z[70];
    z[72]= - n<T>(4063,4)*z[55] + 3919*z[5];
    z[72]=n<T>(8179,18)*z[11] - n<T>(1381,3)*z[6] + n<T>(1487,12)*z[3] - n<T>(1961,4)*
    z[8] + n<T>(1,3)*z[72] - n<T>(3871,4)*z[7];
    z[72]=z[11]*z[72];
    z[64]=n<T>(1,15)*z[72] + n<T>(1381,45)*z[70] + n<T>(1,5)*z[64] - n<T>(181,3)*z[69];
    z[64]=z[11]*z[64];
    z[69]=2983*z[55] - 479*z[5];
    z[58]=z[69]*z[58];
    z[69]=233*z[7] + 209*z[55] - 773*z[5];
    z[69]=z[7]*z[69];
    z[58]=z[58] + z[69];
    z[69]= - n<T>(937,36)*z[8] + n<T>(103,2)*z[7] - n<T>(877,9)*z[55] + n<T>(93,2)*z[5];
    z[69]=z[8]*z[69];
    z[58]=n<T>(1,27)*z[58] + z[69];
    z[69]= - 283*z[55] - n<T>(479,2)*z[5];
    z[69]=n<T>(1,2)*z[69] + 49*z[7];
    z[69]=n<T>(283,60)*z[2] + n<T>(1,15)*z[69] - z[71];
    z[69]=z[2]*z[69];
    z[58]=n<T>(1,5)*z[58] + n<T>(1,9)*z[69];
    z[58]=z[2]*z[58];
    z[69]=466*z[55] + n<T>(773,2)*z[5];
    z[69]=z[5]*z[69];
    z[70]= - n<T>(1,2)*z[7] + n<T>(3,2)*z[55] - n<T>(233,135)*z[5];
    z[70]=z[7]*z[70];
    z[69]=n<T>(1,135)*z[69] + z[70];
    z[69]=z[7]*z[69];
    z[70]= - n<T>(647,3)*z[62] - 93*z[7];
    z[70]=z[5]*z[70];
    z[72]=6563*z[55] - n<T>(15611,3)*z[5];
    z[72]=n<T>(229,9)*z[8] + n<T>(1,30)*z[72] + 7*z[7];
    z[72]=z[8]*z[72];
    z[70]=n<T>(1,5)*z[70] + z[72];
    z[70]=z[70]*z[71];
    z[65]= - 14*z[65] + 59*z[6];
    z[65]=n<T>(1,45)*z[65] - z[67];
    z[65]=z[18]*z[65];
    z[62]=z[63] + z[62];
    z[62]=z[16]*z[62];
    z[63]=z[39] - z[27];
    z[67]=z[47] - z[46];
    z[71]=n<T>(1363,135)*z[28] + n<T>(283,270)*z[26] + n<T>(15721,1080)*z[23];
    z[71]=i*z[71];
    z[72]=npow(z[5],3);
    z[73]= - 1247*z[5] - n<T>(7529,10)*z[7];
    z[73]=n<T>(1,3)*z[73] + n<T>(17753,5)*z[8];
    z[73]=n<T>(1,2)*z[73] + n<T>(7253,15)*z[2];
    z[73]=n<T>(427,18)*z[12] - n<T>(323,10)*z[4] + n<T>(2795,18)*z[11] + n<T>(1687,180)*
    z[9] + n<T>(16363,90)*z[6] + n<T>(1,9)*z[73] + n<T>(1123,20)*z[3];
    z[73]=z[22]*z[73];
    z[55]= - n<T>(12179,10)*z[8] - n<T>(7,5)*z[55] - 121*z[7];
    z[55]= - n<T>(3353,30)*z[3] + n<T>(1,9)*z[55] + n<T>(68,5)*z[2];
    z[55]=z[21]*z[55];
    z[57]=z[4] - z[57] + z[2];
    z[57]=z[15]*z[57];
    z[68]= - z[2] + z[68];
    z[68]=z[19]*z[68];
    z[74]=n<T>(1106,5)*z[11] + n<T>(809,6)*z[12];
    z[74]=z[32]*z[74];
    z[75]=9*z[6] + n<T>(13,2)*z[9];
    z[75]=z[33]*z[75];
    z[76]=n<T>(223,2)*z[8] + 84*z[9];
    z[76]=z[34]*z[76];

    r +=  - n<T>(91,36)*z[24] - n<T>(5927,3240)*z[25] - n<T>(1363,270)*z[29] - 
      n<T>(222529,1080)*z[30] + 3*z[36] + 5*z[37] - n<T>(15061,180)*z[38] - n<T>(233,135)*z[40]
     - n<T>(3874,45)*z[41]
     - n<T>(2041,60)*z[42]
     - z[43]
     - n<T>(8,9)*
      z[44] + n<T>(5087,180)*z[45] + n<T>(573,20)*z[48] - n<T>(1381,45)*z[49] + n<T>(103,20)*z[50]
     + n<T>(93,20)*z[51] - n<T>(129,10)*z[52]
     + z[53]
     + z[54]
     +  z[55] + z[56] + 6*z[57] + z[58] + n<T>(1,15)*z[59] + z[60] + z[61] + 
      n<T>(479,135)*z[62] + n<T>(283,540)*z[63] + z[64] + 7*z[65] + z[66] + n<T>(3,2)*z[67]
     + n<T>(368,135)*z[68]
     + z[69]
     + z[70]
     + z[71] - n<T>(49,135)*
      z[72] + n<T>(1,6)*z[73] + n<T>(1,3)*z[74] + z[75] + n<T>(1,5)*z[76];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf92(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf92(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
