#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf916(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[49];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=d[9];
    z[5]=c[12];
    z[6]=c[13];
    z[7]=c[20];
    z[8]=c[23];
    z[9]=c[32];
    z[10]=c[33];
    z[11]=c[35];
    z[12]=c[36];
    z[13]=c[37];
    z[14]=c[38];
    z[15]=c[39];
    z[16]=c[40];
    z[17]=c[43];
    z[18]=c[44];
    z[19]=c[47];
    z[20]=c[48];
    z[21]=c[50];
    z[22]=c[55];
    z[23]=c[57];
    z[24]=c[59];
    z[25]=c[81];
    z[26]=c[84];
    z[27]=d[0];
    z[28]=f[44];
    z[29]=f[62];
    z[30]=d[2];
    z[31]=g[82];
    z[32]=g[90];
    z[33]=g[101];
    z[34]=g[131];
    z[35]=g[138];
    z[36]=g[147];
    z[37]=g[214];
    z[38]=g[218];
    z[39]=g[245];
    z[40]=g[345];
    z[41]=g[347];
    z[42]=g[348];
    z[43]=n<T>(1,6)*z[24] - 4*z[26] + n<T>(65,18)*z[25];
    z[44]= - n<T>(11,135)*z[1] + n<T>(48,5)*z[6] - n<T>(1,5)*z[8] + 4*z[7];
    z[44]=z[4]*z[44];
    z[45]= - z[7] - n<T>(12,5)*z[6] + n<T>(1,20)*z[8];
    z[46]=n<T>(11,540)*z[1] + z[45];
    z[46]=z[3]*z[46];
    z[45]= - 3*z[45] - n<T>(11,180)*z[1];
    z[45]=z[2]*z[45];
    z[43]=z[45] + 7*z[46] + z[44] - n<T>(5,27)*z[10] - 8*z[13] + n<T>(242,27)*
    z[14] + n<T>(109,486)*z[15] - n<T>(10,3)*z[18] - n<T>(1,6)*z[19] + 4*z[21] + n<T>(1,3)*z[43]
     - n<T>(1,2)*z[23];
    z[43]=i*z[43];
    z[44]=z[29] + z[28];
    z[45]=z[27] - z[30];
    z[44]=z[45]*z[44];
    z[45]= - n<T>(2,3)*z[5] + z[29];
    z[45]=z[4]*z[45];
    z[45]=z[38] - z[45] - z[17] + z[20] - z[35];
    z[46]= - z[37] + z[31] + z[34];
    z[47]=n<T>(5,2)*z[28];
    z[48]= - z[29] + n<T>(7,3)*z[5] + z[47];
    z[48]=z[3]*z[48];
    z[47]= - z[29] - z[5] - z[47];
    z[47]=z[2]*z[47];

    r += n<T>(131,270)*z[9] - n<T>(24,5)*z[11] - 8*z[12] + n<T>(20,3)*z[16] + n<T>(1,10)
      *z[22] + n<T>(3,2)*z[32] - n<T>(7,6)*z[33] - n<T>(2,3)*z[36] + n<T>(17,4)*z[39]
       + n<T>(1,4)*z[40] + n<T>(1,2)*z[41] + z[42] + z[43] + z[44] - 2*z[45] - 
      n<T>(11,4)*z[46] + z[47] + z[48];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf916(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf916(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
