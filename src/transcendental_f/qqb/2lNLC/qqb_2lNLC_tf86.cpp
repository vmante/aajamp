#include "qqb_2lNLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lNLC_tf86(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[80];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=d[4];
    z[12]=d[6];
    z[13]=d[17];
    z[14]=d[9];
    z[15]=d[11];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[7];
    z[21]=e[8];
    z[22]=e[9];
    z[23]=e[10];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=e[13];
    z[36]=e[14];
    z[37]=e[12];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[20];
    z[49]=f[23];
    z[50]=f[31];
    z[51]=f[36];
    z[52]=f[39];
    z[53]=f[51];
    z[54]=f[55];
    z[55]=f[58];
    z[56]=f[88];
    z[57]=2*z[12];
    z[58]=n<T>(188,135)*z[2];
    z[59]=n<T>(1,2)*z[14];
    z[60]= - n<T>(274,135)*z[5] + n<T>(634,135)*z[7] - n<T>(259,20)*z[8] + z[58]
     + 
   n<T>(293,5)*z[3] - n<T>(863,20)*z[11] - z[57] - z[59];
    z[60]=z[5]*z[60];
    z[61]= - n<T>(7229,9)*z[8] + n<T>(73,2)*z[2] + n<T>(863,2)*z[11] - n<T>(826,3)*z[3];
    z[61]=z[8]*z[61];
    z[61]=z[61] - n<T>(643,2)*z[18] - n<T>(458,27)*z[17];
    z[62]=npow(z[4],2);
    z[63]=n<T>(3,2)*z[62];
    z[64]=npow(z[6],2);
    z[65]=3*z[64];
    z[66]=npow(z[14],2);
    z[67]=npow(z[12],2);
    z[57]= - z[57] + n<T>(28591,180)*z[11];
    z[57]=z[11]*z[57];
    z[68]= - z[14] - n<T>(1583,45)*z[3];
    z[68]=z[3]*z[68];
    z[69]=6*z[6];
    z[70]=n<T>(1051,540)*z[2] + n<T>(31,5)*z[3] + z[14] - z[69];
    z[70]=z[2]*z[70];
    z[71]= - n<T>(188,135)*z[7] - n<T>(73,10)*z[8] - n<T>(1268,135)*z[2] + 4*z[12]
    + n<T>(33,10)*z[3];
    z[71]=z[7]*z[71];
    z[57]=z[60] + z[71] + z[70] + z[68] + z[57] + z[65] - z[63] + n<T>(1,2)*
    z[66] + z[67] - n<T>(359,81)*z[24] + n<T>(1,5)*z[61];
    z[57]=z[5]*z[57];
    z[60]=n<T>(643,10)*z[9];
    z[61]=n<T>(863,10)*z[11];
    z[68]=n<T>(376,135)*z[7] + n<T>(259,10)*z[8] + n<T>(733,270)*z[2] - n<T>(312,5)*z[3]
    + z[61] + z[69] - z[60] + 3*z[4];
    z[68]=z[5]*z[68];
    z[69]=n<T>(1,2)*z[6];
    z[70]=n<T>(3,5)*z[4] - n<T>(127,2)*z[6];
    z[70]=z[70]*z[69];
    z[71]= - n<T>(278,45)*z[11] + n<T>(376,9)*z[6];
    z[72]= - n<T>(368,3)*z[13] - 131*z[12];
    z[72]=n<T>(2,15)*z[72] - z[14] + z[71];
    z[72]= - n<T>(1,2)*z[7] + n<T>(299,135)*z[2] + 2*z[72] - n<T>(59,9)*z[3];
    z[72]=z[7]*z[72];
    z[73]=z[4]*z[10];
    z[73]= - z[73] + n<T>(53,135)*z[21] - z[19] + n<T>(736,45)*z[22];
    z[74]=z[12]*z[13];
    z[75]= - z[15] - z[59];
    z[75]=z[14]*z[75];
    z[76]= - z[12] + n<T>(2011,45)*z[6];
    z[77]= - 2*z[76] - n<T>(12253,180)*z[11];
    z[77]=z[11]*z[77];
    z[78]=324*z[9] - n<T>(2779,18)*z[3];
    z[78]=z[3]*z[78];
    z[79]= - z[4] + 2*z[6];
    z[79]=n<T>(781,135)*z[2] + 3*z[79] - n<T>(271,90)*z[3];
    z[79]=z[2]*z[79];
    z[61]=n<T>(754,5)*z[8] - n<T>(742,45)*z[2] + n<T>(6313,90)*z[3] - z[61] - n<T>(23,10)
   *z[6] + z[14] + n<T>(37,10)*z[4];
    z[61]=z[8]*z[61];
    z[61]=z[68] + z[72] + z[61] + z[79] + n<T>(1,5)*z[78] + z[77] + z[70] + 
    z[75] + n<T>(494,15)*z[74] - n<T>(7,45)*z[23] - n<T>(98,45)*z[20] + 7*z[16] + 
   n<T>(1321,135)*z[17] + n<T>(643,10)*z[18] + 2*z[73];
    z[61]=z[1]*z[61];
    z[68]=n<T>(15571,8)*z[25] - 2372*z[30] - 781*z[28];
    z[61]=n<T>(1,135)*z[68] + z[61];
    z[61]=i*z[61];
    z[68]=n<T>(7,2)*z[16];
    z[70]=npow(z[3],2);
    z[72]=npow(z[11],2);
    z[73]= - 53*z[21] - 863*z[17];
    z[74]= - n<T>(1787,270)*z[2] - z[14] + n<T>(1,9)*z[3];
    z[74]=z[2]*z[74];
    z[63]=n<T>(1,2)*z[74] + n<T>(113,90)*z[70] + z[72] + z[65] + z[63] + n<T>(5993,810)*z[24]
     + n<T>(48,5)*z[23]
     + n<T>(1,135)*z[73] - z[68];
    z[63]=z[2]*z[63];
    z[58]=n<T>(1,6)*z[7] + z[58] + n<T>(59,18)*z[3] + n<T>(1432,45)*z[12] + z[59]
     - 
    z[71];
    z[58]=z[7]*z[58];
    z[59]=98*z[20] - 1472*z[22] - n<T>(53,3)*z[21];
    z[59]= - n<T>(25709,360)*z[24] + n<T>(1,5)*z[59] - 85*z[23];
    z[59]=n<T>(1,3)*z[59] - n<T>(509,5)*z[67];
    z[65]=z[3] + z[4];
    z[65]=z[14]*z[65];
    z[71]=n<T>(4022,3)*z[6] - n<T>(2723,2)*z[11];
    z[71]=z[11]*z[71];
    z[73]= - 33*z[3] + n<T>(593,27)*z[2];
    z[73]=z[2]*z[73];
    z[74]=n<T>(7,2)*z[8] + n<T>(83,10)*z[2] - z[4] - n<T>(79,9)*z[3];
    z[74]=z[8]*z[74];
    z[58]=z[58] + z[74] + n<T>(1,10)*z[73] + n<T>(1,15)*z[71] + n<T>(371,10)*z[64]
    + n<T>(1,3)*z[59] + z[65];
    z[58]=z[7]*z[58];
    z[59]=z[4] - z[69];
    z[59]=z[6]*z[59];
    z[59]= - n<T>(2321,6)*z[72] + 23*z[59] - n<T>(27,2)*z[62] + n<T>(32183,54)*z[24]
    - 57*z[36] - n<T>(17459,9)*z[23];
    z[65]=n<T>(4,9)*z[2] - z[14] - n<T>(63,10)*z[3];
    z[65]=z[2]*z[65];
    z[69]= - n<T>(4,9)*z[8] - n<T>(667,180)*z[2] + n<T>(8723,45)*z[3] - n<T>(2669,60)*
    z[11] - n<T>(23,20)*z[6] - z[14] + n<T>(201,20)*z[4];
    z[69]=z[8]*z[69];
    z[59]=z[69] + z[65] + n<T>(1,10)*z[59] + n<T>(353,9)*z[70];
    z[59]=z[8]*z[59];
    z[65]=3*z[35] + n<T>(137,45)*z[20];
    z[69]= - n<T>(1,4)*z[4] + n<T>(2,9)*z[6];
    z[69]=z[6]*z[69];
    z[65]=n<T>(227,5)*z[69] - n<T>(3,20)*z[62] + 4*z[65] + n<T>(21133,540)*z[24];
    z[65]=z[6]*z[65];
    z[69]= - z[10] - n<T>(736,45)*z[13];
    z[60]=2*z[69] - z[60];
    z[60]=z[24]*z[60];
    z[69]=n<T>(553,3)*z[34] + n<T>(494,5)*z[22];
    z[67]=n<T>(1487,135)*z[67] + n<T>(551,54)*z[24] + n<T>(1,3)*z[69] - 5*z[20];
    z[67]=z[12]*z[67];
    z[62]=n<T>(2,3)*z[62] + n<T>(31,30)*z[24] + 2*z[19] - z[68];
    z[62]=z[4]*z[62];
    z[68]=1616*z[34] + n<T>(5239,6)*z[33];
    z[64]= - n<T>(2011,15)*z[64] + n<T>(1,5)*z[68] + n<T>(269,9)*z[24];
    z[68]=n<T>(5867,135)*z[11] - z[4] - z[76];
    z[68]=z[11]*z[68];
    z[64]=n<T>(1,3)*z[64] + z[68];
    z[64]=z[11]*z[64];
    z[68]= - n<T>(1327,9)*z[72] + n<T>(1733,24)*z[24] - n<T>(3014,3)*z[23] + n<T>(5323,6)
   *z[33] + 324*z[18];
    z[69]= - 265*z[11] + n<T>(4,9)*z[3];
    z[69]=z[3]*z[69];
    z[68]=n<T>(1,5)*z[68] + n<T>(1,3)*z[69];
    z[68]=z[3]*z[68];
    z[66]=n<T>(1,3)*z[66] - n<T>(1553,1080)*z[24] + 11*z[35] + z[37] - n<T>(77,10)*
    z[36];
    z[66]=z[14]*z[66];

    r += n<T>(41,36)*z[26] - n<T>(1157,3240)*z[27] + n<T>(1787,540)*z[29] + n<T>(2777,270)*z[31]
     - n<T>(288289,1080)*z[32]
     - n<T>(5,2)*z[38]
     - z[39]
     - 2*z[40]
       - n<T>(14183,90)*z[41] - n<T>(347,540)*z[42] - n<T>(188,135)*z[43] - n<T>(28411,180)*z[44]
     - n<T>(2401,60)*z[45]
     + z[46]
     + n<T>(2,3)*z[47]
     + n<T>(1,2)*z[48]
       + n<T>(1,9)*z[49] + n<T>(3601,90)*z[50] + n<T>(399,10)*z[51] - n<T>(2011,45)*
      z[52] - n<T>(207,20)*z[53] - n<T>(197,20)*z[54] - n<T>(23,20)*z[55] + z[56]
       + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64]
       + z[65] + z[66] + z[67] + z[68];
 
    return r;
}

template std::complex<double> qqb_2lNLC_tf86(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lNLC_tf86(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
