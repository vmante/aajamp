#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_54(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[41];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[31];
    z[17]=e[6];
    z[18]=f[30];
    z[19]=f[31];
    z[20]=f[32];
    z[21]=f[33];
    z[22]=f[35];
    z[23]=f[36];
    z[24]=f[38];
    z[25]=f[39];
    z[26]=f[43];
    z[27]=2*z[3];
    z[28]= - z[8] + z[27];
    z[29]=z[3] - z[4];
    z[28]=z[29]*z[28];
    z[30]=z[1]*i;
    z[31]= - z[4] - z[3];
    z[31]=z[31]*z[30];
    z[32]=npow(z[4],2);
    z[33]=z[30] - z[5];
    z[34]=z[4] + z[33];
    z[34]=z[5]*z[34];
    z[28]=z[34] + 2*z[31] + z[32] + z[28];
    z[28]=z[5]*z[28];
    z[31]=2*z[4];
    z[34]=z[31] - n<T>(7,4)*z[3];
    z[34]=z[3]*z[34];
    z[34]=n<T>(1,2)*z[32] + z[34];
    z[35]= - n<T>(1,3)*z[4] + n<T>(5,2)*z[3];
    z[35]=z[35]*z[30];
    z[36]=z[4] - n<T>(5,4)*z[3];
    z[36]=z[8]*z[36];
    z[34]=z[36] + n<T>(1,3)*z[34] + z[35];
    z[34]=z[8]*z[34];
    z[35]= - z[4] + n<T>(5,3)*z[3];
    z[35]=z[3]*z[35];
    z[35]= - z[32] + n<T>(2,3)*z[35];
    z[35]=z[3]*z[35];
    z[36]=npow(z[4],3);
    z[35]=n<T>(2,9)*z[36] + z[35];
    z[36]=npow(z[3],2);
    z[37]=z[32] + n<T>(7,2)*z[36];
    z[37]=z[37]*z[30];
    z[28]= - z[18] + n<T>(2,3)*z[28] + z[34] + 2*z[35] + n<T>(1,6)*z[37];
    z[34]= - z[4] + n<T>(1,2)*z[3];
    z[35]=n<T>(1,4)*z[8];
    z[37]=z[35] - n<T>(1,2)*z[30] + z[34];
    z[38]=n<T>(1,3)*z[8];
    z[37]=z[37]*z[38];
    z[36]=n<T>(1,3)*z[32] - n<T>(1,2)*z[36];
    z[39]=n<T>(1,3)*z[30];
    z[34]= - z[34]*z[39];
    z[40]=z[30] + z[3];
    z[35]= - n<T>(1,2)*z[7] + z[35] + z[4] - n<T>(1,4)*z[40];
    z[40]=n<T>(1,3)*z[7];
    z[35]=z[35]*z[40];
    z[34]=z[35] + z[37] + n<T>(1,2)*z[36] + z[34];
    z[34]=z[7]*z[34];
    z[35]=z[8] - z[33];
    z[35]=z[29]*z[35];
    z[27]=z[4] - z[27];
    z[27]=z[3]*z[27];
    z[27]=z[32] + z[27] + z[35];
    z[29]= - z[2]*z[29];
    z[27]=2*z[27] + z[29];
    z[27]=z[2]*z[27];
    z[29]= - z[7] + z[5] - n<T>(5,12)*z[8] - z[31] + n<T>(11,2)*z[3];
    z[29]=z[13]*z[29];
    z[27]=z[15] + z[27] + z[29];
    z[29]=z[8] + z[4];
    z[31]= - z[30]*z[29];
    z[31]= - z[13] + z[31];
    z[31]=z[9]*z[31];
    z[29]=z[30] - z[29];
    z[29]=z[12]*z[29];
    z[29]=z[31] + z[29];
    z[31]=z[2] - z[5];
    z[31]=z[38] - z[39] + z[4] + n<T>(4,3)*z[3] - n<T>(1,3)*z[31];
    z[31]=z[11]*z[31];
    z[32]=z[5] + z[3];
    z[30]=z[30]*z[32];
    z[30]=z[13] + z[30];
    z[30]=z[6]*z[30];
    z[32]= - z[3] + z[33];
    z[32]=z[10]*z[32];
    z[30]= - z[30] - z[32] + z[22] + z[20];
    z[32]=z[24] - z[16];
    z[33]=z[14]*i;
    z[33]=z[33] + z[23];
    z[35]= - z[17]*z[40];

    r +=  - n<T>(23,36)*z[19] - n<T>(16,9)*z[21] - n<T>(1,4)*z[25] + n<T>(2,9)*z[26]
     +  n<T>(1,9)*z[27] + n<T>(1,3)*z[28] + n<T>(5,9)*z[29] - n<T>(4,9)*z[30] + n<T>(4,3)*
      z[31] - n<T>(2,3)*z[32] - n<T>(1,12)*z[33] + z[34] + z[35];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_54(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_54(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
