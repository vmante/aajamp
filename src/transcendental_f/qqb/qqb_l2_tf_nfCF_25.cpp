#include "qqb_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCF_25(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[13];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=c[3];
    z[6]=c[11];
    z[7]=c[31];
    z[8]=d[3];
    z[9]=z[1]*i;
    z[10]=n<T>(17,27) - z[2];
    z[10]=4*z[10] - z[4];
    z[10]=z[10]*z[9];
    z[9]= - z[9] + 2*z[2] - z[4];
    z[9]=z[3]*z[9];
    z[11]=z[4] - z[3];
    z[11]=z[8]*z[11];
    z[9]=z[11] + z[10] + z[9];
    z[10]= - n<T>(19,3) + z[2];
    z[10]=z[5]*z[10];
    z[11]=z[6]*i;
    z[10]=z[10] - z[11];
    z[11]= - n<T>(34,27) + z[2];
    z[11]=z[2]*z[11];
    z[12]=n<T>(85,9) - 6*z[4];
    z[12]=z[4]*z[12];
    z[9]=n<T>(4,9)*z[7] + 4*z[11] + z[12] + n<T>(2,3)*z[10] + 2*z[9];

    r += 2*z[9];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCF_25(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCF_25(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d
);
#endif
