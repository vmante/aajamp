#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_72(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=d[9];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=e[6];
    z[30]=e[14];
    z[31]=e[13];
    z[32]=f[4];
    z[33]=f[6];
    z[34]=f[7];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[23];
    z[38]=f[30];
    z[39]=f[31];
    z[40]=f[32];
    z[41]=f[33];
    z[42]=f[35];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=f[50];
    z[47]=f[51];
    z[48]=f[55];
    z[49]=f[56];
    z[50]=f[58];
    z[51]=z[9] + z[7];
    z[52]=z[4] + z[6];
    z[51]=z[52]*z[51];
    z[53]=z[1]*i;
    z[54]=z[53] + z[6];
    z[55]= - z[6]*z[54];
    z[56]= - z[53] - 2*z[4];
    z[56]=z[4]*z[56];
    z[57]=z[53] - z[7];
    z[58]=z[57] - z[9];
    z[59]=2*z[3] - z[6] + z[58];
    z[59]=z[3]*z[59];
    z[58]=z[5] + z[4] + z[58];
    z[58]=z[5]*z[58];
    z[60]=z[5] + z[3] - z[52];
    z[60]=z[2]*z[60];
    z[51]=n<T>(1,2)*z[60] + z[58] + z[59] + z[55] + z[56] + z[51];
    z[51]=z[2]*z[51];
    z[55]=n<T>(1,2)*z[7];
    z[56]=n<T>(1,2)*z[9];
    z[58]=n<T>(7,6)*z[5] + z[56] + z[55] + z[53] - n<T>(5,2)*z[4];
    z[58]=z[5]*z[58];
    z[55]=z[55] - z[53] - z[4];
    z[55]=z[7]*z[55];
    z[59]=2*z[9];
    z[60]=z[59] - z[57];
    z[60]=z[9]*z[60];
    z[61]=npow(z[4],2);
    z[55]=z[58] + z[60] - 2*z[61] + z[55];
    z[55]=z[5]*z[55];
    z[58]=2*z[53];
    z[59]=2*z[7] + z[59] - z[58];
    z[60]= - 2*z[2] + 5*z[5] + 7*z[4] + z[59];
    z[60]=z[19]*z[60];
    z[61]=z[58] - n<T>(17,12)*z[6];
    z[61]=z[61]*npow(z[6],2);
    z[62]=z[5] + z[8];
    z[62]=z[29]*z[62];
    z[51]=z[26] + z[45] - z[42] - z[38] + z[37] + z[51] + z[61] + z[55]
    + z[60] + z[62];
    z[55]=n<T>(1,3)*z[5];
    z[60]=n<T>(5,3)*z[3];
    z[61]= - z[60] + z[9] - n<T>(11,12)*z[7] + n<T>(17,3)*z[6] + n<T>(7,2)*z[4];
    z[61]=n<T>(7,6)*z[8] + n<T>(1,2)*z[61] - z[55];
    z[62]=n<T>(1,2)*z[15];
    z[61]=n<T>(1,8)*z[11] + n<T>(1,3)*z[61] - z[62];
    z[61]=z[24]*z[61];
    z[59]= - n<T>(19,2)*z[6] - z[59];
    z[59]=n<T>(2,3)*z[2] + n<T>(1,3)*z[59] - n<T>(5,2)*z[3];
    z[59]=z[21]*z[59];
    z[63]=z[53]*z[9];
    z[63]=z[63] + z[24];
    z[64]= - z[5]*z[53];
    z[64]=z[64] - z[63];
    z[64]=z[14]*z[64];
    z[65]=z[6]*z[53];
    z[63]=z[65] + z[63];
    z[63]=z[16]*z[63];
    z[65]=z[53] - z[9];
    z[66]= - z[5] + z[65];
    z[66]=z[20]*z[66];
    z[67]= - z[6] + z[65];
    z[67]=z[22]*z[67];
    z[59]=z[61] + z[59] + z[64] + z[63] + z[66] + z[67];
    z[61]=n<T>(1,3)*z[9];
    z[63]=n<T>(7,3)*z[53];
    z[64]=z[63] - 5*z[6];
    z[64]= - z[61] - n<T>(5,6)*z[7] + n<T>(1,2)*z[64] - z[4];
    z[66]=n<T>(1,3)*z[8];
    z[64]=z[66] + z[55] + n<T>(1,2)*z[64] + z[60];
    z[64]=z[8]*z[64];
    z[67]=n<T>(1,2)*z[6];
    z[68]=z[67] + z[53];
    z[69]=z[68]*z[6];
    z[70]=n<T>(1,3)*z[53];
    z[71]=z[70] + z[6];
    z[72]=n<T>(1,6)*z[7];
    z[73]= - z[72] + z[71];
    z[73]=z[7]*z[73];
    z[73]=z[69] - z[73];
    z[74]=n<T>(1,2)*z[4];
    z[75]= - z[53] - z[74];
    z[75]=z[4]*z[75];
    z[76]=n<T>(1,6)*z[9];
    z[77]= - z[76] + z[70] + z[4];
    z[77]=z[9]*z[77];
    z[78]=n<T>(1,2)*z[3] + z[57];
    z[60]=z[78]*z[60];
    z[60]=z[64] + z[60] + z[77] + z[75] - n<T>(5,2)*z[73];
    z[55]=z[65]*z[55];
    z[55]=z[55] + n<T>(1,2)*z[60];
    z[55]=z[55]*z[66];
    z[60]=n<T>(1,3)*z[4];
    z[64]=n<T>(1,3)*z[7];
    z[66]=n<T>(1,3)*z[3];
    z[70]=z[70] - z[6];
    z[70]=z[66] + z[76] - z[64] + n<T>(1,2)*z[70] + z[60];
    z[70]=z[70]*z[62];
    z[73]=n<T>(1,3)*z[6];
    z[75]= - z[53] - n<T>(1,4)*z[6];
    z[75]=z[75]*z[73];
    z[77]= - z[53] + z[56];
    z[76]=z[77]*z[76];
    z[77]=z[73] - n<T>(1,4)*z[4];
    z[77]=z[4]*z[77];
    z[72]=z[6]*z[72];
    z[78]= - z[9] + z[7] - z[52];
    z[78]=z[3]*z[78];
    z[70]=z[70] + n<T>(1,6)*z[78] + z[76] + z[72] + z[75] + z[77];
    z[70]=z[15]*z[70];
    z[58]=z[58] - z[6];
    z[72]= - z[58]*z[73];
    z[63]=z[63] - z[74];
    z[63]=z[63]*z[74];
    z[52]= - z[52]*z[64];
    z[65]= - n<T>(7,6)*z[4] - z[73] + z[65];
    z[65]=z[65]*z[56];
    z[52]=z[65] + z[52] + z[72] + z[63];
    z[52]=z[52]*z[61];
    z[63]= - z[53] + n<T>(17,4)*z[6];
    z[63]=z[63]*z[73];
    z[65]=n<T>(5,4)*z[9] + z[7] - n<T>(5,2)*z[53] + z[6];
    z[61]=z[65]*z[61];
    z[56]=z[56] + n<T>(7,2)*z[53] - z[6];
    z[56]=n<T>(1,2)*z[56] - n<T>(13,3)*z[3];
    z[56]=z[56]*z[66];
    z[65]=z[6]*z[74];
    z[58]= - n<T>(1,3)*z[58] + z[7];
    z[58]=z[7]*z[58];
    z[56]=z[56] + z[61] + z[58] + z[63] + z[65];
    z[56]=z[56]*z[66];
    z[58]=z[11] + z[15];
    z[61]=z[53]*z[58];
    z[61]=z[24] + z[61];
    z[61]=z[12]*z[61];
    z[63]=z[53]*z[7];
    z[63]=z[63] + z[24];
    z[65]=z[4]*z[53];
    z[65]=z[65] + z[63];
    z[65]=z[13]*z[65];
    z[66]=z[53] - z[58];
    z[66]=z[23]*z[66];
    z[72]= - z[4] + z[57];
    z[72]=z[18]*z[72];
    z[61]=z[72] + z[66] + z[46] - z[40] + z[27] + z[61] + z[65];
    z[65]=z[67] - z[53];
    z[66]=z[6]*z[65];
    z[72]=z[53] - z[4];
    z[67]= - z[67] - n<T>(2,3)*z[72];
    z[67]=z[4]*z[67];
    z[60]=z[60] - n<T>(19,6)*z[6] + z[57];
    z[60]=z[7]*z[60];
    z[60]=n<T>(1,4)*z[60] - n<T>(19,12)*z[66] + z[67];
    z[60]=z[60]*z[64];
    z[64]=z[65]*z[73];
    z[54]=z[74] - z[54];
    z[54]=z[4]*z[54];
    z[54]=z[64] + z[54];
    z[58]=z[58] - z[68];
    z[58]=z[74] - n<T>(1,3)*z[58];
    z[64]=n<T>(1,2)*z[11];
    z[58]=z[58]*z[64];
    z[65]=z[15]*z[73];
    z[54]=z[58] + n<T>(1,2)*z[54] + z[65];
    z[54]=z[54]*z[64];
    z[53]= - z[3]*z[53];
    z[53]=z[53] - z[63];
    z[53]=z[10]*z[53];
    z[57]= - z[3] + z[57];
    z[57]=z[17]*z[57];
    z[53]=z[53] + z[57];
    z[57]= - n<T>(1,4)*z[71] + n<T>(17,27)*z[4];
    z[57]=z[4]*z[57];
    z[57]=n<T>(5,6)*z[69] + z[57];
    z[57]=z[57]*z[74];
    z[58]=z[41] + z[34];
    z[63]=z[49] - z[44];
    z[64]=z[50] + z[47];
    z[65]= - z[3] - z[8];
    z[65]=z[28]*z[65];
    z[66]=z[25]*i;
    z[67]=n<T>(1,6)*z[6];
    z[67]= - z[30]*z[67];
    z[62]= - z[31]*z[62];

    r +=  - n<T>(3,8)*z[32] - n<T>(1,4)*z[33] + n<T>(5,72)*z[35] - n<T>(5,24)*z[36]
     - 
      n<T>(11,36)*z[39] + n<T>(1,36)*z[43] - n<T>(1,24)*z[48] + n<T>(1,9)*z[51] + z[52]
       + n<T>(4,9)*z[53] + z[54] + z[55] + z[56] + z[57] - n<T>(8,9)*z[58] + n<T>(1,3)*z[59]
     + z[60]
     + n<T>(1,6)*z[61]
     + z[62]
     + n<T>(1,12)*z[63]
     + n<T>(1,8)*
      z[64] + n<T>(5,18)*z[65] - n<T>(7,108)*z[66] + z[67] + z[70];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_72(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_72(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
