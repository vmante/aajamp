#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_35(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[28];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[11];
    z[6]=d[9];
    z[7]=e[12];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=e[13];
    z[12]=e[14];
    z[13]=f[50];
    z[14]=f[51];
    z[15]=f[53];
    z[16]=f[54];
    z[17]=f[55];
    z[18]=f[58];
    z[19]=npow(z[3],2);
    z[20]=n<T>(5,6)*z[4];
    z[21]=z[3] - z[20];
    z[21]=z[4]*z[21];
    z[22]=z[3] + 2*z[4];
    z[22]=n<T>(1,3)*z[22];
    z[23]=z[22] - n<T>(1,2)*z[2];
    z[23]=z[2]*z[23];
    z[24]=z[3] - z[6];
    z[24]=z[6]*z[24];
    z[21]=n<T>(1,2)*z[24] + z[23] - n<T>(1,6)*z[19] + z[21];
    z[21]=z[6]*z[21];
    z[23]=5*z[4];
    z[24]=n<T>(1,2)*z[3] - n<T>(1,3)*z[4];
    z[24]=z[24]*z[23];
    z[24]=n<T>(1,2)*z[19] + z[24];
    z[24]=z[4]*z[24];
    z[20]= - z[20] + 3*z[3];
    z[20]=z[20]*z[4];
    z[19]=z[20] - n<T>(7,6)*z[19];
    z[20]=z[23] + 7*z[3];
    z[23]=n<T>(1,6)*z[20] - z[2];
    z[23]=z[2]*z[23];
    z[23]=z[23] - z[19];
    z[23]=z[2]*z[23];
    z[25]=npow(z[3],3);
    z[23]=z[23] - n<T>(7,3)*z[25] + z[24];
    z[24]= - n<T>(1,3)*z[20] + z[2];
    z[24]=z[2]*z[24];
    z[19]=z[24] + z[19];
    z[22]= - z[6]*z[22];
    z[19]=n<T>(1,2)*z[19] + z[22];
    z[22]=z[1]*i;
    z[19]=z[19]*z[22];
    z[24]=z[6] + z[2];
    z[25]=z[24]*z[22];
    z[25]=z[25] + z[8];
    z[25]=z[5]*z[25];
    z[22]=z[22] - z[24];
    z[22]=z[7]*z[22];
    z[20]= - n<T>(1,9)*z[20] + n<T>(1,4)*z[2];
    z[20]=z[8]*z[20];
    z[24]=z[6] + z[3];
    z[24]= - z[4] - n<T>(7,3)*z[24];
    z[24]=z[11]*z[24];
    z[26]= - z[4] - z[6];
    z[26]=z[12]*z[26];
    z[27]=z[9]*i;

    r +=  - z[10] + n<T>(2,3)*z[13] + n<T>(7,12)*z[14] + 2*z[15] - n<T>(1,3)*z[16]
       - n<T>(5,12)*z[17] + n<T>(5,4)*z[18] + z[19] + z[20] + z[21] + z[22] + n<T>(1,2)*z[23]
     + z[24]
     + z[25]
     + n<T>(5,3)*z[26] - n<T>(1,12)*z[27];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_35(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_35(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
