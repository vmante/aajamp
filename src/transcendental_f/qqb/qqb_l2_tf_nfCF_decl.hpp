#ifndef QQB_L2_TF_NFCF_DECL_H
#define QQB_L2_TF_NFCF_DECL_H

#include <array>
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
std::complex<T> qqb_l2_tf_nfCF_1(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_2(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_3(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_4(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_5(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_6(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_7(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_8(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_9(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_10(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_11(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_12(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_13(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_14(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_15(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_16(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_17(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_18(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_19(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_20(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_21(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_22(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_23(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_24(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_25(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_26(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_27(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_28(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_29(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_30(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_31(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_32(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_33(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_34(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_35(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_36(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_37(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_38(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_39(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_40(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_41(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_42(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_43(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_44(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_45(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_46(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_47(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_48(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_49(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_50(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_51(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_52(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_53(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_54(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_55(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_56(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_57(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_58(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_59(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_60(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_61(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_62(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_63(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_64(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_65(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_66(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_67(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_68(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_69(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_70(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_71(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_72(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_73(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCF_74(
  const std::array<std::complex<T>,19>&);

#endif /* QQB_L2_TF_NFCF_DECL_H */
