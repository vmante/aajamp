#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_87(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[75];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[12];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=e[6];
    z[30]=e[13];
    z[31]=e[14];
    z[32]=f[4];
    z[33]=f[6];
    z[34]=f[7];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[23];
    z[38]=f[30];
    z[39]=f[31];
    z[40]=f[32];
    z[41]=f[33];
    z[42]=f[35];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=f[50];
    z[47]=f[51];
    z[48]=f[52];
    z[49]=f[54];
    z[50]=f[55];
    z[51]=f[56];
    z[52]=f[58];
    z[53]=npow(z[4],2);
    z[54]=z[1]*i;
    z[55]=n<T>(1,3)*z[54] + n<T>(5,11)*z[53];
    z[56]=n<T>(1,11)*z[9];
    z[57]=2*z[54];
    z[58]=z[57] - n<T>(5,3)*z[4];
    z[58]= - n<T>(5,3)*z[9] + n<T>(1,3)*z[58] - z[7];
    z[58]=z[58]*z[56];
    z[59]=5*z[7];
    z[60]= - z[59] - z[54] + 14*z[4];
    z[60]= - 4*z[5] + n<T>(2,3)*z[60] - z[9];
    z[60]=z[5]*z[60];
    z[61]= - n<T>(10,33)*z[7] + n<T>(14,33)*z[4] - n<T>(1,2) + n<T>(20,33)*z[54];
    z[61]=z[7]*z[61];
    z[62]=n<T>(1,33)*z[4];
    z[63]= - z[3]*z[62];
    z[55]=n<T>(1,33)*z[60] + z[63] + z[58] + n<T>(1,2)*z[55] + n<T>(1,3)*z[61];
    z[55]=z[5]*z[55];
    z[58]=2*z[4];
    z[60]=z[54] - z[4];
    z[60]=z[60]*z[58];
    z[61]=n<T>(1,3)*z[6];
    z[63]=n<T>(43,12)*z[6] - n<T>(43,6)*z[54] - z[4];
    z[63]=z[63]*z[61];
    z[60]=z[60] + z[63];
    z[63]=n<T>(1,2) + n<T>(19,33)*z[7];
    z[64]=n<T>(19,33)*z[54];
    z[65]=z[63] - z[64];
    z[62]=n<T>(43,66)*z[6] + z[62] + z[65];
    z[62]=z[7]*z[62];
    z[60]=n<T>(1,11)*z[60] + n<T>(1,6)*z[62];
    z[60]=z[7]*z[60];
    z[57]=z[57] - z[6];
    z[62]=2*z[6];
    z[66]=z[57]*z[62];
    z[67]= - 32*z[54] + 7*z[4];
    z[67]=z[4]*z[67];
    z[66]=z[67] + z[66];
    z[67]=z[54] - z[9];
    z[68]=z[61] + n<T>(16,3)*z[4] - z[67];
    z[69]=n<T>(1,3)*z[9];
    z[68]=z[68]*z[69];
    z[70]=n<T>(2,9)*z[6];
    z[71]=z[70] + z[4];
    z[72]=z[7]*z[71];
    z[66]=z[68] + n<T>(1,9)*z[66] + z[72];
    z[56]=z[66]*z[56];
    z[66]=2*z[7];
    z[68]=5*z[54];
    z[62]= - n<T>(5,2)*z[9] - z[66] + z[68] - z[62];
    z[62]=z[62]*z[69];
    z[72]= - n<T>(35,6)*z[6] + n<T>(5,3)*z[54] + z[58];
    z[72]=z[6]*z[72];
    z[57]=n<T>(5,3)*z[57] - 4*z[7];
    z[57]=z[7]*z[57];
    z[57]=z[62] + z[72] + z[57];
    z[62]=n<T>(97,99)*z[3] - n<T>(1,33)*z[9] - n<T>(7,33)*z[6] + n<T>(1,2) - z[64];
    z[62]=z[3]*z[62];
    z[57]=n<T>(1,11)*z[57] + n<T>(1,2)*z[62];
    z[57]=z[3]*z[57];
    z[62]= - n<T>(1,2) - z[64];
    z[62]=n<T>(1,3)*z[62] - n<T>(7,11)*z[4];
    z[53]=z[62]*z[53];
    z[62]=4*z[54] + z[4];
    z[62]=z[4]*z[62];
    z[64]=1 - n<T>(19,11)*z[54];
    z[64]= - n<T>(25,198)*z[6] + n<T>(1,4)*z[64] + n<T>(2,11)*z[4];
    z[64]=z[6]*z[64];
    z[62]=n<T>(1,11)*z[62] + z[64];
    z[61]=z[62]*z[61];
    z[53]=z[55] + n<T>(1,3)*z[57] + z[56] + z[60] + n<T>(1,2)*z[53] + z[61];
    z[55]=n<T>(5,6)*z[4];
    z[56]=z[54] + z[55];
    z[56]=z[4]*z[56];
    z[57]=n<T>(1,2)*z[6];
    z[60]=z[57] + z[54];
    z[60]=z[60]*z[57];
    z[61]=n<T>(1,2)*z[7];
    z[62]=n<T>(5,54)*z[7] - n<T>(5,27)*z[54] - z[6];
    z[62]=z[62]*z[61];
    z[64]= - n<T>(1,9)*z[9] + n<T>(2,9)*z[54] - z[4];
    z[64]=z[64]*z[69];
    z[69]=n<T>(1,9)*z[3];
    z[72]=z[4] + n<T>(8,3)*z[54];
    z[73]= - n<T>(5,6)*z[3] + n<T>(8,3)*z[7] - z[72];
    z[73]=z[73]*z[69];
    z[74]=n<T>(1,6)*z[54] + z[4];
    z[74]= - n<T>(1,54)*z[7] + n<T>(5,9)*z[74] + z[57];
    z[74]= - n<T>(5,54)*z[8] - n<T>(7,27)*z[5] - n<T>(13,54)*z[3] + n<T>(1,2)*z[74]
     - n<T>(1,27)*z[9];
    z[74]=z[8]*z[74];
    z[56]=z[74] + z[73] + z[64] + z[62] + n<T>(1,3)*z[56] + z[60];
    z[60]= - n<T>(1,3)*z[67] + n<T>(1,11)*z[3];
    z[60]=z[5]*z[60];
    z[56]=n<T>(1,9)*z[60] + n<T>(1,11)*z[56];
    z[56]=z[8]*z[56];
    z[60]= - z[54] + n<T>(1,2)*z[9];
    z[62]=n<T>(1,2)*z[4];
    z[57]= - z[61] + z[57] + z[62] - z[60];
    z[57]=z[14]*z[57];
    z[61]=z[4] + z[6];
    z[61]=z[61]*z[66];
    z[64]= - z[68] - 4*z[4];
    z[64]=z[4]*z[64];
    z[66]= - n<T>(5,6)*z[6] - z[72];
    z[66]=z[6]*z[66];
    z[60]= - z[9]*z[60];
    z[72]= - z[7] + z[9];
    z[72]=z[3]*z[72];
    z[57]=z[57] + z[72] + z[60] + z[61] + n<T>(2,3)*z[64] + z[66];
    z[57]=z[14]*z[57];
    z[60]= - n<T>(43,2)*z[7] - 161*z[4] - 125*z[6];
    z[60]=n<T>(1,3)*z[60] + z[9];
    z[60]=4*z[14] - n<T>(4,3)*z[8] + n<T>(59,6)*z[5] + n<T>(1,2)*z[60] + n<T>(14,3)*z[3]
   ;
    z[60]=n<T>(1,3)*z[60] + n<T>(1,2)*z[11];
    z[60]=z[24]*z[60];
    z[57]= - z[48] + z[57] + z[60];
    z[60]= - z[9] - z[7];
    z[60]=z[71]*z[60];
    z[61]=z[54] + z[58];
    z[61]=z[4]*z[61];
    z[64]=z[54] + z[6];
    z[64]=z[64]*z[70];
    z[66]= - n<T>(1,2)*z[5] - z[69] + z[62] + n<T>(1,9)*z[6];
    z[66]=z[2]*z[66];
    z[69]=z[67] - z[7];
    z[70]= - 2*z[3] + z[6] - z[69];
    z[70]=z[3]*z[70];
    z[71]= - z[5] - z[4] - z[69];
    z[71]=z[5]*z[71];
    z[60]=z[66] + z[71] + n<T>(2,9)*z[70] + z[61] + z[64] + z[60];
    z[60]=z[2]*z[60];
    z[61]= - z[54] + z[62];
    z[61]=z[4]*z[61];
    z[62]=z[11] + z[14];
    z[64]=z[62] - z[54];
    z[55]=n<T>(7,6)*z[6] + z[55] - z[64];
    z[55]=z[11]*z[55];
    z[66]=n<T>(7,18)*z[6] - n<T>(7,9)*z[54] - z[4];
    z[66]=z[6]*z[66];
    z[58]=z[58] + z[6];
    z[58]=z[14]*z[58];
    z[55]=n<T>(1,3)*z[55] + n<T>(2,9)*z[58] + n<T>(5,9)*z[61] + z[66];
    z[55]=z[11]*z[55];
    z[55]= - z[60] - z[55] + z[45] - z[44];
    z[58]=2*z[9] + z[59] - z[68] + 17*z[6];
    z[58]= - n<T>(2,3)*z[2] + n<T>(1,3)*z[58] + 5*z[3];
    z[58]=z[21]*z[58];
    z[59]=z[54]*z[62];
    z[59]=z[24] + z[59];
    z[59]=z[12]*z[59];
    z[60]=z[54]*z[9];
    z[60]=z[60] + z[24];
    z[61]= - z[6]*z[54];
    z[61]=z[61] - z[60];
    z[61]=z[16]*z[61];
    z[62]=z[23]*z[64];
    z[64]=z[6] - z[67];
    z[64]=z[22]*z[64];
    z[58]= - z[62] + z[58] + z[59] + z[61] + z[64];
    z[59]=z[3] + z[7];
    z[61]=z[54]*z[59];
    z[61]=z[24] + z[61];
    z[61]=z[10]*z[61];
    z[59]= - z[54] + z[59];
    z[59]=z[17]*z[59];
    z[59]=z[42] + z[61] + z[59];
    z[61]=n<T>(19,33)*z[4];
    z[62]= - z[61] - z[63];
    z[62]=z[54]*z[62];
    z[62]= - n<T>(19,33)*z[24] + z[62];
    z[62]=z[13]*z[62];
    z[61]=z[61] + z[65];
    z[61]=z[18]*z[61];
    z[61]=z[62] + z[61];
    z[62]= - z[5] - n<T>(5,11)*z[8];
    z[62]=z[29]*z[62];
    z[63]= - z[6] - n<T>(8,11)*z[14];
    z[63]=z[31]*z[63];
    z[62]=z[38] + z[62] + z[63];
    z[54]=z[5]*z[54];
    z[54]=z[54] + z[60];
    z[54]=z[15]*z[54];
    z[60]=z[5] - z[67];
    z[60]=z[20]*z[60];
    z[54]=z[54] + z[60];
    z[60]=z[46] - z[37];
    z[63]=z[52] + z[33];
    z[64]= - z[4] - 7*z[14];
    z[64]=z[30]*z[64];
    z[64]=z[64] + z[43];
    z[65]=z[2] - n<T>(28,9)*z[5] - n<T>(37,9)*z[4] + z[69];
    z[65]=z[19]*z[65];
    z[66]=z[3] + z[8];
    z[66]=z[28]*z[66];
    z[67]=z[25]*i;

    r +=  - n<T>(19,297)*z[26] - n<T>(4,33)*z[27] + n<T>(7,132)*z[32] + n<T>(28,297)*
      z[34] + n<T>(7,1188)*z[35] + n<T>(1,44)*z[36] + n<T>(25,297)*z[39] + n<T>(2,27)*
      z[40] + n<T>(8,33)*z[41] + n<T>(5,594)*z[47] - n<T>(4,297)*z[49] - n<T>(7,594)*
      z[50] - n<T>(1,198)*z[51] + n<T>(1,3)*z[53] + n<T>(8,99)*z[54] - n<T>(1,33)*z[55]
       + z[56] + n<T>(1,99)*z[57] + n<T>(2,99)*z[58] + n<T>(14,297)*z[59] + n<T>(2,297)
      *z[60] + n<T>(1,9)*z[61] + n<T>(1,27)*z[62] + n<T>(1,66)*z[63] + n<T>(1,297)*
      z[64] + n<T>(2,33)*z[65] + n<T>(5,297)*z[66] + n<T>(19,1188)*z[67];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_87(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_87(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
