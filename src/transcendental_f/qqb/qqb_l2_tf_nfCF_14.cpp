#include "qqb_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCF_14(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[73];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[12];
    z[9]=d[16];
    z[10]=d[3];
    z[11]=d[4];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[4];
    z[15]=e[5];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[17];
    z[23]=c[19];
    z[24]=c[25];
    z[25]=c[26];
    z[26]=c[28];
    z[27]=c[30];
    z[28]=c[31];
    z[29]=e[1];
    z[30]=e[6];
    z[31]=f[0];
    z[32]=f[1];
    z[33]=f[3];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[13];
    z[37]=f[17];
    z[38]=f[18];
    z[39]=f[20];
    z[40]=f[30];
    z[41]=f[31];
    z[42]=f[32];
    z[43]=f[33];
    z[44]=f[36];
    z[45]=f[39];
    z[46]=f[43];
    z[47]=f[68];
    z[48]=f[76];
    z[49]=17*z[5];
    z[50]=z[1]*i;
    z[51]=7*z[50];
    z[52]=z[51] + z[49];
    z[52]=z[5]*z[52];
    z[53]=4*z[6];
    z[54]=z[50] + z[5];
    z[55]= - z[6] - z[54];
    z[55]=z[55]*z[53];
    z[56]=z[6] - z[5];
    z[57]=2*z[50];
    z[58]=z[57] - z[7];
    z[59]= - z[58] + z[56];
    z[59]=z[7]*z[59];
    z[52]=4*z[59] + z[52] + z[55];
    z[55]=n<T>(1,3)*z[5];
    z[59]=n<T>(4,3)*z[6];
    z[60]=4*z[2];
    z[61]=4*z[50];
    z[62]= - z[60] + n<T>(4,3)*z[7] - z[59] + z[61] + z[55];
    z[62]=z[2]*z[62];
    z[52]=n<T>(2,3)*z[52] + z[62];
    z[52]=z[2]*z[52];
    z[62]=2*z[5];
    z[63]=2*z[6];
    z[64]= - z[63] + z[50] + z[62];
    z[64]=2*z[64] - z[7];
    z[64]=z[7]*z[64];
    z[65]=8*z[50];
    z[49]=z[65] - z[49];
    z[49]=z[5]*z[49];
    z[66]=z[61] + z[5];
    z[67]= - z[63] + z[66];
    z[67]=z[67]*z[53];
    z[49]=z[64] + z[49] + z[67];
    z[56]= - z[7] + z[56];
    z[56]=n<T>(2,3)*z[56] + z[2];
    z[56]=z[2]*z[56];
    z[49]=n<T>(1,3)*z[49] + 2*z[56];
    z[56]=4*z[10];
    z[59]= - n<T>(2,3)*z[7] - z[59] + n<T>(7,3)*z[5] + z[56] - z[61];
    z[59]=z[10]*z[59];
    z[49]=2*z[49] + z[59];
    z[49]=z[10]*z[49];
    z[59]=n<T>(4,3)*z[2];
    z[64]= - z[50] + z[2];
    z[64]=z[64]*z[59];
    z[67]=z[59] - z[7];
    z[68]=n<T>(1,3)*z[50];
    z[69]= - z[68] - z[67];
    z[69]=2*z[69] + n<T>(1,3)*z[10];
    z[69]=z[10]*z[69];
    z[70]=z[63] + z[5];
    z[71]= - z[6]*z[70];
    z[72]=z[7]*z[58];
    z[64]=z[69] + z[64] + z[71] + z[72];
    z[59]=n<T>(2,3)*z[10] + z[59] - 2*z[7] - z[65] + z[5];
    z[59]=z[4]*z[59];
    z[59]=2*z[64] + z[59];
    z[59]=z[4]*z[59];
    z[64]=13*z[5];
    z[65]=n<T>(2,3)*z[6];
    z[51]= - z[65] + z[51] + z[64];
    z[51]=z[6]*z[51];
    z[68]= - z[68] + z[5];
    z[68]=z[5]*z[68];
    z[51]=z[68] + n<T>(1,3)*z[51];
    z[51]=z[51]*z[63];
    z[68]=5*z[5];
    z[69]= - 26*z[50] + z[68];
    z[55]=z[69]*z[55];
    z[69]=z[50] + n<T>(2,3)*z[5];
    z[69]=2*z[69] - z[6];
    z[69]=z[69]*z[63];
    z[64]=z[64] - 20*z[6];
    z[64]=z[7]*z[64];
    z[55]=n<T>(1,3)*z[64] + z[55] + z[69];
    z[55]=z[7]*z[55];
    z[64]=n<T>(1,3)*z[3];
    z[69]= - 59*z[5] + 32*z[6];
    z[67]= - z[64] - n<T>(22,9)*z[11] - n<T>(74,9)*z[4] - 2*z[10] + n<T>(1,9)*z[69]
    - z[67];
    z[67]=z[19]*z[67];
    z[69]= - 5*z[50] - n<T>(62,3)*z[5];
    z[69]=z[69]*npow(z[5],2);
    z[49]=z[67] + z[59] + z[49] + z[52] + z[55] + n<T>(1,3)*z[69] + z[51];
    z[51]=z[53] + z[56] + z[68] + z[3];
    z[52]= - z[50]*z[51];
    z[53]=4*z[19];
    z[52]= - z[53] + z[52];
    z[52]=z[8]*z[52];
    z[51]= - z[61] + z[51];
    z[51]=z[15]*z[51];
    z[55]=4*z[4];
    z[56]=z[55] - z[3];
    z[59]= - z[68] - z[60] + z[56];
    z[59]=z[29]*z[59];
    z[56]=z[56] + 4*z[11];
    z[61]= - z[5] + z[56];
    z[61]=z[50]*z[61];
    z[53]=z[53] + z[61];
    z[53]=z[9]*z[53];
    z[56]= - z[56] + z[66];
    z[56]=z[14]*z[56];
    z[61]=z[11] + z[6];
    z[61]= - z[3] - z[5] - n<T>(14,3)*z[61];
    z[61]=z[30]*z[61];
    z[51]=z[56] + z[51] + z[59] + z[61] - z[37] + z[52] + z[53];
    z[52]=z[50] - z[7];
    z[52]= - n<T>(22,3)*z[11] + z[55] - z[60] - z[65] + z[62] - n<T>(7,3)*z[52];
    z[52]=z[11]*z[52];
    z[53]= - n<T>(2,3)*z[50] + z[70];
    z[53]=z[53]*z[65];
    z[55]=z[57] + z[5];
    z[55]=z[5]*z[55];
    z[56]=n<T>(2,9)*z[6] - n<T>(7,9)*z[50] - z[5];
    z[56]=2*z[56] + n<T>(7,9)*z[7];
    z[56]=z[7]*z[56];
    z[59]=z[4]*z[50];
    z[52]=n<T>(1,3)*z[52] - n<T>(8,3)*z[59] + z[56] + z[55] + z[53];
    z[52]=z[11]*z[52];
    z[53]=z[50]*z[63];
    z[54]=2*z[54] - z[2];
    z[54]=z[2]*z[54];
    z[55]= - z[62] + z[10];
    z[55]=z[10]*z[55];
    z[56]= - z[63] + z[4];
    z[56]=z[4]*z[56];
    z[59]=z[63] - z[11];
    z[59]=z[11]*z[59];
    z[53]=z[59] + z[56] + z[55] + z[53] + z[54];
    z[53]=z[53]*z[64];
    z[54]=z[7] + z[6];
    z[55]=z[50]*z[54];
    z[55]=z[19] + z[55];
    z[55]=z[12]*z[55];
    z[50]= - z[50] + z[54];
    z[50]=z[18]*z[50];
    z[50]=z[55] + z[50];
    z[54]=z[57] - z[68];
    z[55]=z[7] + z[10] - z[2];
    z[54]=n<T>(2,9)*z[54] - z[6] - n<T>(4,9)*z[55];
    z[54]=z[16]*z[54];
    z[55]= - z[2] + z[58];
    z[55]=z[17]*z[55];
    z[55]=z[38] - z[36] + z[34] - z[24] - z[55] + z[47] + z[46] - z[39];
    z[56]= - z[4] + z[57] - z[2];
    z[56]=z[13]*z[56];
    z[56]= - z[21] + z[56] + z[43];
    z[57]=z[32] + z[23];
    z[58]=z[35] - z[33];
    z[59]=z[42] + z[27];
    z[60]=z[48] - z[31];
    z[61]= - n<T>(32,3)*z[25] - n<T>(8,3)*z[22] + n<T>(7,27)*z[20];
    z[61]=i*z[61];
    z[49]=z[45] - n<T>(7,9)*z[44] + n<T>(7,3)*z[41] + n<T>(4,9)*z[40] - 2*z[28] + 8*
    z[26] + 4*z[54] + z[53] + z[52] + z[61] + n<T>(1,3)*z[49] + n<T>(28,9)*z[50]
    + n<T>(16,3)*z[60] + n<T>(8,3)*z[59] - n<T>(2,9)*z[58] + n<T>(4,3)*z[57] + n<T>(40,9)*
    z[56] + n<T>(2,3)*z[51] - n<T>(8,9)*z[55];

    r += 2*z[49];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCF_14(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCF_14(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
