#ifndef QQB_L2_TF_NFCA_DECL_H
#define QQB_L2_TF_NFCA_DECL_H

#include <array>
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
std::complex<T> qqb_l2_tf_nfCA_1(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_2(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_3(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_4(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_5(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_6(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_7(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_8(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_9(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_10(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_11(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_12(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_13(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_14(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_15(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_16(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_17(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_18(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_19(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_20(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_21(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_22(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_23(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_24(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_25(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_26(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_27(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_28(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_29(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_30(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_31(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_32(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_33(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_34(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_35(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_36(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_37(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_38(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_39(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_40(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_41(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_42(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_43(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_44(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_45(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_46(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_47(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_48(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_49(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_50(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_51(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_52(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_53(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_54(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_55(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_56(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_57(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_58(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_59(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_60(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_61(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_62(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_63(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_64(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_65(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_66(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_67(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_68(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_69(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_70(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_71(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_72(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_73(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_74(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_75(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_76(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_77(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_78(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_79(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_80(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_81(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_82(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_83(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_84(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_85(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_86(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_87(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_l2_tf_nfCA_88(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

#endif /* QQB_L2_TF_NFCA_DECL_H */
