#include "qqb_l2_tf_nfCA_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCA_56(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[11];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[8];
    z[3]=d[16];
    z[4]=c[3];
    z[5]=d[2];
    z[6]=e[4];
    z[7]=e[14];
    z[8]=z[1]*i;
    z[9]=z[3]*z[8];
    z[9]= - z[9] + z[7] + z[6];
    z[10]=npow(z[5],2);
    z[8]= - z[5] + z[8];
    z[8]=2*z[8] + z[2];
    z[8]=z[2]*z[8];
    z[8]=z[8] + z[10] - n<T>(1,3)*z[4] + 2*z[9];

    r += n<T>(2,9)*z[8];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCA_56(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCA_56(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
