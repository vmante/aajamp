#include "qqb_l2_tf_nfnfaa_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfnfaa_4(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[18];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[7];
    z[5]=e[0];
    z[6]=e[8];
    z[7]=c[3];
    z[8]=d[1];
    z[9]=d[5];
    z[10]=c[11];
    z[11]=z[2] - n<T>(5,3);
    z[11]=4*z[11];
    z[12]=z[9] + z[8];
    z[13]=z[1]*i;
    z[13]=4*z[13];
    z[14]= - z[11] + z[13] - z[12];
    z[15]=z[3] - z[2];
    z[15]=z[15]*z[4];
    z[16]=z[3]*z[2];
    z[17]=npow(z[2],2);
    z[15]=z[15] - z[16] + z[17];
    z[14]=z[15]*z[14];
    z[11]=z[12] + z[11];
    z[12]=z[11] - z[13];
    z[13]= - z[6] - z[5];
    z[12]=z[12]*z[13];
    z[12]=z[12] + z[14];
    z[11]=z[7]*z[11];
    z[13]=z[10]*i;
    z[11]= - n<T>(4,3)*z[13] + 2*z[12] + n<T>(1,3)*z[11];

    r += n<T>(4,3)*z[11];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfnfaa_4(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfnfaa_4(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
