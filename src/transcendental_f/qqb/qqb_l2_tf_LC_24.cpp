#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_24(
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[3];
  std::complex<T> r(0,0);

    z[1]=d[8];

    r += n<T>(1,36)*z[1];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_24(
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_24(
  const std::array<std::complex<dd_real>,19>& d
);
#endif
