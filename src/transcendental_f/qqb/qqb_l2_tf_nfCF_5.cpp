#include "qqb_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCF_5(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[32];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[5];
    z[4]=d[11];
    z[5]=d[2];
    z[6]=d[8];
    z[7]=d[9];
    z[8]=e[12];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[17];
    z[13]=c[31];
    z[14]=e[13];
    z[15]=e[14];
    z[16]=f[50];
    z[17]=f[51];
    z[18]=f[54];
    z[19]=f[55];
    z[20]=f[58];
    z[21]=f[59];
    z[22]=2*z[3];
    z[23]=z[22] - z[5];
    z[23]=z[23]*z[5];
    z[24]=npow(z[3],2);
    z[23]=z[23] - z[24];
    z[25]=z[1]*i;
    z[26]=2*z[25];
    z[27]= - z[3] - z[5];
    z[27]=z[27]*z[26];
    z[27]=z[27] + z[23];
    z[28]=z[25] - z[5];
    z[29]=2*z[6];
    z[30]=n<T>(1,3)*z[7];
    z[31]= - z[30] - z[29] - z[28];
    z[30]=z[31]*z[30];
    z[28]= - n<T>(2,3)*z[28] - z[6];
    z[28]=z[6]*z[28];
    z[27]=z[30] + n<T>(2,3)*z[27] + z[28];
    z[27]=z[7]*z[27];
    z[28]=n<T>(7,3)*z[3] - z[5];
    z[28]=z[5]*z[28];
    z[28]= - n<T>(10,3)*z[24] + z[28];
    z[28]=z[28]*z[25];
    z[27]=z[28] + z[27];
    z[28]=n<T>(7,3)*z[5];
    z[30]= - z[3] - z[28];
    z[26]=z[30]*z[26];
    z[28]=n<T>(14,9)*z[6] - n<T>(7,3)*z[25] - z[3] + z[28];
    z[28]=z[6]*z[28];
    z[30]=z[22] - n<T>(5,3)*z[5];
    z[30]=z[5]*z[30];
    z[26]=z[28] + z[26] - z[24] + z[30];
    z[26]=z[26]*z[29];
    z[28]= - 23*z[3] + n<T>(4,3)*z[5];
    z[28]=z[5]*z[28];
    z[24]=40*z[24] + z[28];
    z[24]=z[5]*z[24];
    z[28]=npow(z[3],3);
    z[24]= - 17*z[28] + z[24];
    z[28]=29*z[3] - 26*z[5];
    z[28]=n<T>(1,3)*z[28] + 4*z[6];
    z[28]=n<T>(8,3)*z[4] + n<T>(1,3)*z[28] + z[2];
    z[28]=z[9]*z[28];
    z[24]=z[28] + z[26] + n<T>(1,3)*z[24] + 4*z[27];
    z[26]=z[2] + n<T>(16,9)*z[7] + n<T>(8,3)*z[6] + z[3] + n<T>(4,9)*z[5];
    z[27]=z[4]*z[25]*z[26];
    z[26]=n<T>(4,9)*z[25] - z[26];
    z[26]=z[8]*z[26];
    z[28]= - 17*z[3] - 8*z[7];
    z[28]=n<T>(1,9)*z[28] - z[2];
    z[28]=z[14]*z[28];
    z[26]=z[28] + z[27] + z[26];
    z[22]= - z[25]*z[22];
    z[22]=z[22] + z[23];
    z[22]=z[2]*z[22];
    z[23]=z[21] - z[18];
    z[25]=z[6] + z[7];
    z[25]=z[15]*z[25];
    z[25]=z[25] - z[16];
    z[27]=z[10]*i;
    z[28]=4*i;
    z[28]=z[12]*z[28];
    z[22]= - n<T>(2,3)*z[20] + n<T>(2,9)*z[19] - n<T>(14,9)*z[17] - z[13] + z[28] + 
   n<T>(10,9)*z[11] - n<T>(2,27)*z[27] + z[22] + n<T>(1,3)*z[24] + n<T>(8,9)*z[25] + n<T>(16,9)*z[23] + 2*z[26];

    r += n<T>(2,3)*z[22];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCF_5(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCF_5(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
