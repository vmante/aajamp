#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_55(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[87];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[12];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[3];
    z[19]=e[5];
    z[20]=e[7];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[11];
    z[24]=e[12];
    z[25]=c[3];
    z[26]=c[11];
    z[27]=c[15];
    z[28]=c[31];
    z[29]=e[6];
    z[30]=e[13];
    z[31]=e[14];
    z[32]=f[4];
    z[33]=f[6];
    z[34]=f[7];
    z[35]=f[12];
    z[36]=f[15];
    z[37]=f[16];
    z[38]=f[23];
    z[39]=f[30];
    z[40]=f[31];
    z[41]=f[32];
    z[42]=f[33];
    z[43]=f[35];
    z[44]=f[36];
    z[45]=f[39];
    z[46]=f[43];
    z[47]=f[50];
    z[48]=f[51];
    z[49]=f[52];
    z[50]=f[54];
    z[51]=f[55];
    z[52]=f[56];
    z[53]=f[58];
    z[54]=n<T>(13,3)*z[3] + n<T>(29,4)*z[8];
    z[55]=n<T>(3,4)*z[11];
    z[56]=7*z[16];
    z[57]=n<T>(7,3)*z[2];
    z[58]=2*z[14];
    z[59]=n<T>(20,3)*z[6] + n<T>(127,12)*z[7] - n<T>(14,3)*z[9] + z[58] - z[57] + 
    z[56] + z[55] - z[54];
    z[59]=z[6]*z[59];
    z[60]=9*z[15];
    z[56]=z[56] - z[60];
    z[61]=n<T>(7,2)*z[14];
    z[62]=n<T>(7,2)*z[9];
    z[63]=z[62] - n<T>(35,6)*z[3] - n<T>(7,3)*z[5] - n<T>(5,6)*z[8] - z[61] + z[56];
    z[63]=z[9]*z[63];
    z[64]=n<T>(10,3)*z[2];
    z[65]=z[64] + n<T>(7,2)*z[8];
    z[66]=13*z[13] - n<T>(7,2)*z[11];
    z[66]=n<T>(27,8)*z[4] - n<T>(7,4)*z[6] - n<T>(20,3)*z[7] + n<T>(73,6)*z[9] + z[58]
    + n<T>(1,2)*z[66] - z[65];
    z[66]=z[4]*z[66];
    z[67]=7*z[23];
    z[68]=9*z[21];
    z[69]=z[67] + z[68];
    z[70]=n<T>(1,2)*z[11];
    z[71]=z[12] + z[70];
    z[71]=z[71]*z[70];
    z[72]=n<T>(1,2)*z[14];
    z[73]=z[12] - n<T>(5,2)*z[14];
    z[73]=z[73]*z[72];
    z[60]=n<T>(5,6)*z[5] + n<T>(13,3)*z[8] - z[60] + z[64];
    z[60]=z[5]*z[60];
    z[74]=n<T>(1,3)*z[3];
    z[75]= - 4*z[10] + z[2];
    z[75]=n<T>(67,4)*z[3] + 7*z[75] + n<T>(29,2)*z[8];
    z[75]=z[75]*z[74];
    z[76]= - n<T>(28,3)*z[10] + n<T>(13,2)*z[13];
    z[77]=n<T>(13,4)*z[7];
    z[78]=z[77] - n<T>(14,3)*z[3] - n<T>(19,3)*z[5] + n<T>(41,12)*z[8] + z[76];
    z[78]=z[7]*z[78];
    z[79]=n<T>(13,2)*z[19];
    z[80]=n<T>(28,3)*z[17] + z[79] + z[18] - n<T>(11,3)*z[20] + n<T>(20,3)*z[22];
    z[81]=n<T>(1,2)*z[24];
    z[82]=npow(z[8],2);
    z[83]=n<T>(5,24)*z[82];
    z[59]=z[66] + z[59] + z[78] + z[63] + z[75] + z[60] - z[83] + z[73]
    + z[71] + z[81] + z[69] + z[80];
    z[59]=z[1]*z[59];
    z[59]= - n<T>(67,36)*z[26] + z[59];
    z[59]=i*z[59];
    z[60]=n<T>(3,2)*z[3];
    z[55]= - n<T>(3,8)*z[6] + n<T>(5,2)*z[7] - z[60] - z[55] + z[58];
    z[55]=z[6]*z[55];
    z[58]=n<T>(1,3)*z[5];
    z[63]=10*z[2];
    z[66]=z[63] - n<T>(53,2)*z[5];
    z[66]=z[66]*z[58];
    z[71]=npow(z[2],2);
    z[73]=n<T>(5,3)*z[71];
    z[75]=npow(z[11],2);
    z[78]= - z[11] + z[72];
    z[78]=z[14]*z[78];
    z[84]= - z[72] + 2*z[8];
    z[84]=z[3]*z[84];
    z[65]= - n<T>(73,12)*z[9] + 2*z[5] + z[65];
    z[65]=z[9]*z[65];
    z[64]=n<T>(1,12)*z[7] - n<T>(10,3)*z[9] - n<T>(16,3)*z[5] + z[64] - z[14];
    z[64]=z[7]*z[64];
    z[85]=n<T>(17,3)*z[5];
    z[86]=n<T>(161,18)*z[4] + n<T>(1,8)*z[6] + n<T>(31,6)*z[7] - n<T>(11,4)*z[9]
     - z[85]
    - n<T>(15,4)*z[8] + n<T>(3,4)*z[14] + n<T>(7,8)*z[11] - n<T>(20,3)*z[2];
    z[86]=z[4]*z[86];
    z[55]=z[86] + z[55] + z[64] + z[65] + z[84] + z[66] - n<T>(11,4)*z[82]
    + z[78] - z[73] + n<T>(7,8)*z[75] + n<T>(91,12)*z[25] + n<T>(76,3)*z[20] - 3*
    z[30] - z[79];
    z[55]=z[4]*z[55];
    z[64]=n<T>(1,3)*z[2];
    z[65]=z[64] - z[72];
    z[65]=7*z[65];
    z[66]=n<T>(7,3)*z[9];
    z[54]= - n<T>(127,24)*z[7] - z[66] + z[65] + z[54];
    z[54]=z[7]*z[54];
    z[61]=3*z[11] - z[61];
    z[61]=z[61]*z[72];
    z[78]= - n<T>(5,3)*z[3] - z[57] + z[72];
    z[78]=z[3]*z[78];
    z[79]= - n<T>(1,2)*z[9] + z[2] + z[3];
    z[79]=z[79]*z[66];
    z[57]= - n<T>(29,36)*z[6] - n<T>(151,24)*z[7] + z[66] + n<T>(119,12)*z[3] - n<T>(29,8)*z[8]
     - n<T>(1,4)*z[14]
     - n<T>(3,8)*z[11]
     - z[57];
    z[57]=z[6]*z[57];
    z[66]=n<T>(7,6)*z[71];
    z[54]=z[57] + z[54] + z[79] + z[78] - n<T>(29,8)*z[82] + z[61] - z[66]
    - n<T>(3,8)*z[75] + n<T>(161,18)*z[25] - n<T>(139,6)*z[22] + n<T>(3,2)*z[31]
     - 
    z[67];
    z[54]=z[6]*z[54];
    z[57]=7*z[3];
    z[61]= - n<T>(41,4)*z[8] + 19*z[5];
    z[61]= - z[77] + n<T>(1,6)*z[61] + z[57];
    z[61]=z[7]*z[61];
    z[67]= - z[63] + 11*z[5];
    z[67]=z[67]*z[58];
    z[60]= - z[60] - z[65] - n<T>(29,6)*z[8];
    z[60]=z[3]*z[60];
    z[65]=npow(z[14],2);
    z[71]=10*z[5] + z[57];
    z[71]=z[9]*z[71];
    z[60]=z[61] + n<T>(1,3)*z[71] + z[60] + z[67] - z[83] - n<T>(1,2)*z[65] + n<T>(1,72)*z[25]
     - z[80];
    z[60]=z[7]*z[60];
    z[61]=7*z[14] + n<T>(5,3)*z[8];
    z[61]= - z[62] + n<T>(35,12)*z[3] + n<T>(1,4)*z[61] + z[85];
    z[61]=z[9]*z[61];
    z[62]=n<T>(7,2)*z[5] - z[63] - 13*z[8];
    z[58]=z[62]*z[58];
    z[62]=n<T>(1,12)*z[3] - z[64] - z[72];
    z[57]=z[62]*z[57];
    z[57]=z[61] + z[57] + z[58] + n<T>(5,12)*z[82] + n<T>(7,4)*z[65] + z[25] + 
   n<T>(20,3)*z[20] - n<T>(14,3)*z[22] - z[69];
    z[57]=z[9]*z[57];
    z[58]=5*z[2] + n<T>(13,3)*z[5];
    z[58]=z[5]*z[58];
    z[58]=n<T>(2,3)*z[58] + n<T>(13,6)*z[82] + z[73] - n<T>(53,18)*z[25] + n<T>(59,3)*
    z[20] + n<T>(13,3)*z[29] - z[68];
    z[58]=z[5]*z[58];
    z[61]= - n<T>(155,6)*z[3] + 14*z[2] + n<T>(23,4)*z[8];
    z[61]=z[61]*z[74];
    z[62]=n<T>(29,2)*z[18];
    z[63]= - 28*z[17] - z[62];
    z[61]=z[61] + n<T>(23,6)*z[82] + z[66] - n<T>(47,18)*z[25] + n<T>(1,3)*z[63]
     - 
   n<T>(37,2)*z[22];
    z[61]=z[3]*z[61];
    z[56]=n<T>(1,2)*z[12] + z[56] + z[76];
    z[56]=z[25]*z[56];
    z[63]= - n<T>(1,2)*z[75] - z[24] + n<T>(1,4)*z[25];
    z[63]=z[63]*z[70];
    z[64]= - n<T>(1,4)*z[75] - n<T>(13,6)*z[25] - z[81] + z[31] - n<T>(5,2)*z[30];
    z[64]=z[14]*z[64];
    z[62]=n<T>(7,2)*z[82] + n<T>(11,3)*z[25] + 7*z[29] - z[62];
    z[62]=z[8]*z[62];
    z[65]=z[27] - z[39];
    z[66]=n<T>(1,2)*z[53] + z[52];
    z[66]=7*z[66] + n<T>(3,2)*z[51];
    z[67]=7*z[22] - 10*z[20];
    z[67]=z[2]*z[67];

    r += n<T>(45,2)*z[28] - n<T>(69,8)*z[32] - n<T>(21,4)*z[33] - n<T>(68,3)*z[34]
     + n<T>(17,24)*z[35]
     + 2*z[36] - n<T>(29,8)*z[37]
     + n<T>(7,3)*z[38] - n<T>(113,12)*
      z[40] - n<T>(15,2)*z[41] - n<T>(62,3)*z[42] - n<T>(16,3)*z[43] - n<T>(5,12)*z[44]
       - n<T>(15,4)*z[45] + n<T>(10,3)*z[46] + n<T>(3,2)*z[47] + n<T>(7,8)*z[48]
     +  z[49] + z[50] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + 
      z[60] + z[61] + n<T>(1,3)*z[62] + z[63] + z[64] + n<T>(13,3)*z[65] + n<T>(1,4)
      *z[66] + n<T>(2,3)*z[67];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_55(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_55(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
