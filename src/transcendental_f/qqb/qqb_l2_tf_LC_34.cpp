#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_34(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[11];
    z[6]=d[9];
    z[7]=e[12];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[31];
    z[12]=e[13];
    z[13]=e[14];
    z[14]=f[50];
    z[15]=f[51];
    z[16]=f[54];
    z[17]=f[55];
    z[18]=f[57];
    z[19]=f[58];
    z[20]=i*z[1];
    z[21]=z[20]*z[5];
    z[21]=z[21] - z[7];
    z[22]= - 2*z[13] - z[12];
    z[23]=npow(z[6],2);
    z[22]= - n<T>(1,2)*z[23] + n<T>(1,6)*z[8] + n<T>(2,3)*z[22] + z[21];
    z[22]=z[6]*z[22];
    z[23]=2*z[6];
    z[24]= - z[20]*z[23];
    z[25]= - z[3] - n<T>(5,4)*z[20] - z[6];
    z[25]=z[3]*z[25];
    z[24]=z[25] + z[24] - 2*z[12] - n<T>(7,6)*z[8];
    z[25]=n<T>(1,3)*z[3];
    z[24]=z[24]*z[25];
    z[26]=z[11] - z[18];
    z[27]=z[8]*z[5];
    z[28]=z[1]*z[7];
    z[28]= - n<T>(1,12)*z[9] + z[28];
    z[28]=i*z[28];
    z[22]=z[24] + z[22] + z[28] + z[27] - z[10] + n<T>(1,3)*z[14] + n<T>(5,12)*
    z[15] - n<T>(2,3)*z[16] - n<T>(7,12)*z[17] + n<T>(1,4)*z[19] + 2*z[26];
    z[24]= - z[13] - n<T>(1,3)*z[8];
    z[26]= - n<T>(1,3)*z[20] + n<T>(1,2)*z[6];
    z[26]=z[6]*z[26];
    z[24]=n<T>(7,3)*z[24] + z[26];
    z[26]=n<T>(1,2)*z[3];
    z[27]=z[20] + n<T>(1,6)*z[3];
    z[27]=z[27]*z[26];
    z[28]= - n<T>(7,4)*z[20] + z[6];
    z[28]= - n<T>(7,18)*z[4] + n<T>(1,9)*z[28] + n<T>(1,4)*z[3];
    z[28]=z[4]*z[28];
    z[24]=z[28] + n<T>(1,3)*z[24] + z[27];
    z[24]=z[4]*z[24];
    z[23]=n<T>(5,4)*z[3] - n<T>(5,2)*z[20] + z[23];
    z[23]=z[23]*z[25];
    z[21]=z[23] + n<T>(1,4)*z[8] + z[21];
    z[23]= - n<T>(7,2)*z[20] + z[6];
    z[23]=n<T>(7,36)*z[4] + n<T>(1,9)*z[23] - z[26];
    z[23]=z[4]*z[23];
    z[20]= - z[2] + n<T>(7,6)*z[4] + n<T>(5,6)*z[3] + z[20] - z[6];
    z[20]=z[2]*z[20];
    z[20]=n<T>(1,6)*z[20] + n<T>(1,3)*z[21] + z[23];
    z[20]=z[2]*z[20];

    r += z[20] + n<T>(1,3)*z[22] + z[24];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_34(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_34(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
