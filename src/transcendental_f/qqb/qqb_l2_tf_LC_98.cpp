#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_98(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[15];
    z[10]=d[2];
    z[11]=d[11];
    z[12]=d[3];
    z[13]=d[12];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=e[6];
    z[30]=e[13];
    z[31]=e[14];
    z[32]=f[4];
    z[33]=f[6];
    z[34]=f[7];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[23];
    z[38]=f[30];
    z[39]=f[31];
    z[40]=f[32];
    z[41]=f[33];
    z[42]=f[35];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=f[50];
    z[47]=f[51];
    z[48]=f[52];
    z[49]=f[55];
    z[50]=f[56];
    z[51]=f[58];
    z[52]=n<T>(1,6)*z[12];
    z[53]=3*z[4];
    z[54]=z[1]*i;
    z[55]= - n<T>(7,3)*z[54] + z[6];
    z[55]=z[8] + z[52] + n<T>(1,2)*z[55] + z[53];
    z[56]=n<T>(1,3)*z[3];
    z[57]=z[56] + z[5];
    z[55]= - z[7] + n<T>(1,2)*z[55] - z[57];
    z[55]=z[7]*z[55];
    z[58]=n<T>(1,2)*z[6];
    z[59]=z[58] + z[54];
    z[59]=z[59]*z[58];
    z[60]=n<T>(1,2)*z[4];
    z[61]=z[54] + z[60];
    z[61]=z[61]*z[53];
    z[52]=z[52] - n<T>(1,3)*z[54] - z[6];
    z[62]=n<T>(1,2)*z[12];
    z[52]=z[52]*z[62];
    z[63]=n<T>(1,2)*z[8];
    z[64]=z[63] - z[54];
    z[65]= - z[53] + z[64];
    z[65]=z[8]*z[65];
    z[66]=n<T>(1,2)*z[3];
    z[67]=z[54] - z[12];
    z[68]= - z[66] - z[67];
    z[68]=z[68]*z[56];
    z[52]=z[55] + z[68] + z[65] + z[52] + z[59] + z[61];
    z[55]=z[54] - z[8];
    z[59]= - z[5]*z[55];
    z[52]=z[59] + n<T>(1,2)*z[52];
    z[52]=z[7]*z[52];
    z[59]= - z[8] - z[12];
    z[61]=n<T>(1,3)*z[6];
    z[65]=z[61] + z[4];
    z[59]=z[65]*z[59];
    z[68]=z[54] + z[6];
    z[69]=z[68]*z[61];
    z[70]=2*z[4];
    z[71]=z[54] + z[70];
    z[71]=z[4]*z[71];
    z[72]=z[12] + z[6];
    z[73]= - 2*z[3] - z[55] + z[72];
    z[56]=z[73]*z[56];
    z[57]=z[65] - z[57];
    z[57]=z[2]*z[57];
    z[73]= - z[5] + z[12] - z[4] - z[55];
    z[73]=z[5]*z[73];
    z[56]=n<T>(1,2)*z[57] + z[73] + z[56] + z[69] + z[71] + z[59];
    z[56]=z[2]*z[56];
    z[57]=n<T>(3,2)*z[6];
    z[59]=z[60] - z[54] - z[57];
    z[59]=z[4]*z[59];
    z[64]= - z[64]*z[63];
    z[69]=z[8] + z[4] - z[72];
    z[66]=z[69]*z[66];
    z[69]=npow(z[6],2);
    z[57]= - z[57] + z[4];
    z[57]=z[12]*z[57];
    z[71]=z[6] + z[55];
    z[71]=z[14]*z[71];
    z[57]=n<T>(1,4)*z[71] + z[66] + z[64] + z[57] + z[69] + z[59];
    z[57]=z[14]*z[57];
    z[59]=z[54] - n<T>(5,4)*z[6];
    z[59]=z[59]*z[61];
    z[64]=n<T>(1,3)*z[12];
    z[66]= - z[72]*z[64];
    z[71]= - n<T>(5,4)*z[8] + n<T>(5,2)*z[54] - z[72];
    z[71]=z[8]*z[71];
    z[64]=n<T>(5,18)*z[3] - n<T>(1,12)*z[8] + z[64] - n<T>(1,4)*z[54] + n<T>(2,3)*z[6];
    z[64]=z[3]*z[64];
    z[72]= - z[4]*z[58];
    z[59]=z[64] + n<T>(1,3)*z[71] + z[66] + z[59] + z[72];
    z[59]=z[3]*z[59];
    z[64]=z[10] + z[14];
    z[66]=z[64] - z[54];
    z[71]=n<T>(3,2)*z[4];
    z[72]=z[71] + z[58] - z[66];
    z[72]=z[10]*z[72];
    z[73]=z[58] - z[54];
    z[73]=z[73]*z[6];
    z[68]=z[60] - z[68];
    z[53]=z[68]*z[53];
    z[53]=z[72] + z[73] + z[53];
    z[68]=z[14]*z[6];
    z[53]=z[68] + n<T>(1,2)*z[53];
    z[53]=z[10]*z[53];
    z[64]=z[54]*z[64];
    z[64]=z[24] + z[64];
    z[64]=z[11]*z[64];
    z[66]=z[23]*z[66];
    z[53]= - z[40] - z[53] - z[64] + z[66] - z[46];
    z[64]=2*z[54];
    z[66]=z[64] - z[6];
    z[66]=z[66]*z[61];
    z[68]=7*z[54];
    z[72]= - z[68] + z[71];
    z[72]=z[72]*z[60];
    z[61]=n<T>(7,2)*z[4] + z[61] - z[55];
    z[61]=z[61]*z[63];
    z[65]=z[12]*z[65];
    z[61]=z[61] + z[65] + z[66] + z[72];
    z[61]=z[8]*z[61];
    z[63]= - n<T>(7,6)*z[5] - z[63] - z[62] - z[54] + n<T>(5,2)*z[4];
    z[63]=z[5]*z[63];
    z[62]= - z[62] + z[54] + z[4];
    z[62]=z[12]*z[62];
    z[65]=2*z[8];
    z[66]= - z[65] + z[67];
    z[66]=z[8]*z[66];
    z[72]=npow(z[4],2);
    z[62]=z[63] + z[66] + 2*z[72] + z[62];
    z[62]=z[5]*z[62];
    z[63]= - z[70] + z[64] + z[58];
    z[63]=z[4]*z[63];
    z[66]= - n<T>(11,2) + 3*z[54];
    z[72]=3*z[12] - z[4] + n<T>(19,6)*z[6] - z[66];
    z[72]=z[12]*z[72];
    z[63]=n<T>(1,4)*z[72] + n<T>(7,12)*z[73] + z[63];
    z[63]=z[12]*z[63];
    z[72]=z[8] + z[5];
    z[72]=z[54]*z[72];
    z[72]=z[24] + z[72];
    z[72]=z[15]*z[72];
    z[73]=z[5] - z[55];
    z[73]=z[20]*z[73];
    z[72]=z[72] + z[73];
    z[73]=z[12] + z[3];
    z[73]=z[54]*z[73];
    z[73]=z[24] + z[73];
    z[73]=z[9]*z[73];
    z[67]=z[3] - z[67];
    z[67]=z[17]*z[67];
    z[67]=z[73] + z[67];
    z[73]= - n<T>(1,36)*z[6] - n<T>(11,4) - n<T>(2,3)*z[54];
    z[69]=z[73]*z[69];
    z[68]=z[68] - z[58];
    z[58]=z[68]*z[58];
    z[68]=7*z[6] + 11 - 15*z[54];
    z[68]=n<T>(1,4)*z[68] - n<T>(23,3)*z[4];
    z[68]=z[4]*z[68];
    z[58]=z[58] + z[68];
    z[58]=z[58]*z[60];
    z[60]= - z[64] + z[65] + 2*z[12];
    z[64]=7*z[4];
    z[65]=2*z[2] - 5*z[5] - z[64] - z[60];
    z[65]=z[19]*z[65];
    z[60]=n<T>(13,2)*z[6] + z[60];
    z[60]= - n<T>(2,3)*z[2] + n<T>(1,3)*z[60] + n<T>(3,2)*z[3];
    z[60]=z[21]*z[60];
    z[68]=z[6] + z[71] + n<T>(3,2)*z[12];
    z[71]= - n<T>(11,4) - z[68];
    z[71]=z[54]*z[71];
    z[71]= - n<T>(3,2)*z[24] + z[71];
    z[71]=z[13]*z[71];
    z[66]= - n<T>(1,2)*z[66] + z[68];
    z[66]=z[18]*z[66];
    z[68]= - z[6] - z[8];
    z[54]=z[54]*z[68];
    z[54]= - z[24] + z[54];
    z[54]=z[16]*z[54];
    z[68]=z[44] + z[33];
    z[73]=z[50] + z[43];
    z[74]=z[51] - z[47];
    z[75]= - z[6] + 3*z[14];
    z[75]=z[31]*z[75];
    z[75]=z[75] + z[48];
    z[64]=n<T>(7,18)*z[3] - n<T>(17,36)*z[12] - n<T>(17,9)*z[6] - z[64];
    z[64]=n<T>(3,8)*z[10] + n<T>(13,12)*z[14] - n<T>(7,18)*z[7] + n<T>(1,2)*z[64]
     + n<T>(1,3)
   *z[5];
    z[64]=z[24]*z[64];
    z[55]=z[6] - z[55];
    z[55]=z[22]*z[55];
    z[76]=z[3] + z[7];
    z[76]=z[28]*z[76];
    z[77]=z[7] + z[5];
    z[77]= - z[29]*z[77];
    z[70]= - z[70] + z[14];
    z[70]=z[30]*z[70];
    z[78]=z[25]*i;

    r +=  - n<T>(13,6)*z[26] + n<T>(17,4)*z[27] + n<T>(5,8)*z[32] + n<T>(8,3)*z[34]
     - n<T>(1,24)*z[35]
     + n<T>(1,8)*z[36] - n<T>(1,3)*z[37]
     + z[38]
     + n<T>(11,4)*z[39]
     + 8
      *z[41] + z[42] - z[45] - n<T>(3,8)*z[49] + z[52] - n<T>(3,2)*z[53] + 
      z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + 
      z[62] + z[63] + z[64] + z[65] + z[66] + n<T>(2,3)*z[67] + n<T>(3,4)*z[68]
       + z[69] + z[70] + z[71] + 3*z[72] - n<T>(1,4)*z[73] - n<T>(9,8)*z[74] + 
      n<T>(1,2)*z[75] + n<T>(1,6)*z[76] + z[77] + n<T>(2,9)*z[78];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_98(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_98(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
