#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf218(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[28];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[9];
    z[6]=d[11];
    z[7]=e[12];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[17];
    z[12]=c[31];
    z[13]=e[13];
    z[14]=e[14];
    z[15]=f[51];
    z[16]=f[55];
    z[17]=f[58];
    z[18]=f[59];
    z[19]=npow(z[4],2);
    z[20]=i*z[1];
    z[21]=n<T>(1,6)*z[5] + z[4] + n<T>(1,2)*z[20];
    z[21]=z[5]*z[21];
    z[21]=z[19] + z[21];
    z[22]=n<T>(1,2)*z[8];
    z[23]=npow(z[3],2);
    z[24]=n<T>(3,2)*z[3];
    z[25]=z[24] - z[6];
    z[25]=z[1]*z[25];
    z[26]= - i*z[25];
    z[21]=z[26] - n<T>(3,4)*z[23] + z[22] - z[7] - z[14] - n<T>(3,2)*z[13] + n<T>(1,2)*z[21];
    z[21]=z[5]*z[21];
    z[23]=z[24] - z[4];
    z[23]=z[23]*z[24];
    z[23]=z[23] + z[7] - n<T>(3,4)*z[19];
    z[26]=n<T>(3,2)*z[1];
    z[26]= - z[4]*z[26];
    z[25]=z[26] + z[25];
    z[25]=i*z[25];
    z[25]=z[25] + n<T>(13,12)*z[8] + z[23];
    z[26]= - n<T>(1,4)*z[5] + z[24] + z[20];
    z[26]=z[5]*z[26];
    z[27]=z[4] - z[3];
    z[20]=n<T>(1,2)*z[27] + z[20];
    z[20]= - n<T>(1,12)*z[2] + n<T>(3,4)*z[20] - z[5];
    z[20]=z[2]*z[20];
    z[20]=z[20] + n<T>(1,2)*z[25] + z[26];
    z[20]=z[2]*z[20];
    z[25]=n<T>(1,2)*z[4] - z[3];
    z[25]=z[3]*z[25];
    z[25]=n<T>(1,2)*z[25] - z[13] + n<T>(1,4)*z[19];
    z[24]=z[25]*z[24];
    z[23]= - z[1]*z[23];
    z[23]= - n<T>(9,2)*z[11] + n<T>(1,3)*z[9] + z[23];
    z[25]=n<T>(1,2)*i;
    z[23]=z[23]*z[25];
    z[22]= - z[6]*z[22];
    z[19]= - n<T>(5,6)*z[19] - z[14] - n<T>(1,6)*z[8];
    z[19]=z[4]*z[19];

    r +=  - n<T>(11,8)*z[10] + n<T>(27,16)*z[12] + n<T>(9,8)*z[15] - n<T>(5,8)*z[16]
     +  n<T>(3,8)*z[17] - z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + 
      z[24];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf218(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf218(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
