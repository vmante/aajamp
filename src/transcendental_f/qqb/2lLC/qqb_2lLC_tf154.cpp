#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf154(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[78];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[9];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=e[6];
    z[30]=e[13];
    z[31]=e[14];
    z[32]=f[2];
    z[33]=f[4];
    z[34]=f[6];
    z[35]=f[10];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[23];
    z[39]=f[31];
    z[40]=f[32];
    z[41]=f[33];
    z[42]=f[36];
    z[43]=f[38];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=f[51];
    z[47]=f[52];
    z[48]=f[53];
    z[49]=f[55];
    z[50]=f[58];
    z[51]=f[72];
    z[52]=f[73];
    z[53]=z[9] + z[5];
    z[54]=n<T>(1,2)*z[6];
    z[55]=z[1]*i;
    z[56]= - n<T>(3,2)*z[55] - z[4];
    z[53]=z[54] + 3*z[56] - n<T>(5,2)*z[3] + n<T>(9,2)*z[53];
    z[56]= - z[13] + 3*z[8];
    z[53]=n<T>(1,2)*z[53] - z[56];
    z[53]=z[8]*z[53];
    z[57]=z[55] - z[7];
    z[58]=z[57] + z[54];
    z[58]=z[58]*z[54];
    z[59]=13*z[55];
    z[60]=n<T>(1,2)*z[4];
    z[61]= - z[59] - z[60];
    z[62]=n<T>(1,4)*z[4];
    z[61]=z[61]*z[62];
    z[63]=n<T>(1,2)*z[3];
    z[64]=n<T>(9,4)*z[5] + z[63] + z[57];
    z[64]=z[5]*z[64];
    z[65]=z[55] + z[4];
    z[66]=n<T>(1,2)*z[9];
    z[67]= - z[66] + z[65];
    z[67]=z[9]*z[67];
    z[68]=n<T>(1,2)*z[55];
    z[69]= - z[68] + z[4];
    z[69]=z[3]*z[69];
    z[70]=z[4] + z[63];
    z[70]=z[7]*z[70];
    z[53]=z[53] + n<T>(9,4)*z[67] + z[58] + z[64] + z[70] + z[61] + z[69];
    z[61]=z[13]*z[4];
    z[53]= - z[61] + n<T>(1,2)*z[53];
    z[53]=z[8]*z[53];
    z[64]=z[68] + z[4];
    z[67]=z[4]*z[64];
    z[69]=z[4] - z[63];
    z[69]=z[3]*z[69];
    z[70]= - 5*z[55] - z[4];
    z[70]=n<T>(5,4)*z[7] + n<T>(1,2)*z[70] - z[3];
    z[70]=z[7]*z[70];
    z[67]=z[70] + n<T>(5,2)*z[67] + z[69];
    z[69]=n<T>(1,2)*z[5];
    z[70]=z[69] + z[57];
    z[70]=z[5]*z[70];
    z[71]=z[55] + z[3];
    z[72]= - z[66] + z[71];
    z[72]=z[72]*z[66];
    z[73]=z[7] - z[55] - n<T>(3,2)*z[4];
    z[73]= - z[3] + n<T>(5,2)*z[73];
    z[73]= - n<T>(3,8)*z[13] + z[54] + n<T>(1,2)*z[73] + z[5];
    z[74]=n<T>(1,2)*z[13];
    z[73]=z[73]*z[74];
    z[58]=z[73] + z[72] - z[58] + n<T>(1,2)*z[67] + z[70];
    z[58]=z[58]*z[74];
    z[67]=n<T>(1,4)*z[2] - z[66];
    z[70]=n<T>(5,3)*z[4];
    z[72]=z[70] - z[3];
    z[73]= - z[6] - z[72] + n<T>(5,3)*z[5];
    z[67]=z[73]*z[67];
    z[64]= - z[64]*z[70];
    z[73]= - z[6] - z[3] - z[57];
    z[54]=z[73]*z[54];
    z[72]=z[72]*z[7];
    z[73]=z[68] + z[3];
    z[73]=z[3]*z[73];
    z[74]=z[5] + z[4] + z[57];
    z[74]=z[5]*z[74];
    z[54]=z[54] + n<T>(5,6)*z[74] + n<T>(1,2)*z[72] + z[64] + z[73] + z[67];
    z[64]=n<T>(1,2)*z[2];
    z[54]=z[54]*z[64];
    z[67]=n<T>(1,4)*z[3];
    z[73]=n<T>(13,24)*z[7];
    z[74]=n<T>(25,8)*z[55] - 7*z[4];
    z[74]= - n<T>(137,72)*z[5] + z[73] + n<T>(1,3)*z[74] - z[67];
    z[74]=z[5]*z[74];
    z[75]=3*z[55];
    z[70]=z[75] - z[70];
    z[70]=z[70]*z[62];
    z[67]= - z[67] - z[65];
    z[67]=z[3]*z[67];
    z[59]= - z[59] - z[4];
    z[59]=z[73] + n<T>(1,12)*z[59] + z[3];
    z[59]=z[7]*z[59];
    z[59]=z[74] + z[59] + z[70] + z[67];
    z[59]=z[59]*z[69];
    z[67]= - z[63] + z[55] - z[60];
    z[67]=z[67]*z[63];
    z[69]=n<T>(1,3)*z[4];
    z[70]= - 4*z[55] + n<T>(17,8)*z[4];
    z[70]=z[70]*z[69];
    z[69]= - n<T>(13,3)*z[7] + z[75] + z[69];
    z[69]=z[7]*z[69];
    z[67]=n<T>(1,8)*z[69] + z[70] + z[67];
    z[67]=z[7]*z[67];
    z[69]=n<T>(1,3)*z[55];
    z[70]=n<T>(1,24)*z[3] + z[69] + z[62];
    z[70]=z[3]*z[70];
    z[73]=n<T>(1,4)*z[7];
    z[74]= - z[4] + z[3];
    z[74]=z[74]*z[73];
    z[76]= - z[55] - n<T>(9,2)*z[4];
    z[76]=z[4]*z[76];
    z[77]= - 41*z[55] - 17*z[4];
    z[77]= - n<T>(17,3)*z[6] + n<T>(1,8)*z[77] - z[3];
    z[77]=z[6]*z[77];
    z[70]=n<T>(1,8)*z[77] + z[74] + n<T>(1,32)*z[76] + z[70];
    z[70]=z[6]*z[70];
    z[74]=n<T>(29,3)*z[55] - 15*z[4];
    z[74]=z[74]*z[60];
    z[76]=n<T>(1,3)*z[3];
    z[71]= - z[71]*z[76];
    z[77]=n<T>(3,2)*z[5] - 3*z[65] + n<T>(5,3)*z[7];
    z[77]=z[5]*z[77];
    z[71]=z[77] - z[72] + z[74] + z[71];
    z[68]=n<T>(1,4)*z[6] - z[73] - z[68] - z[76];
    z[68]=z[6]*z[68];
    z[63]=z[63] + z[55] - n<T>(29,4)*z[4];
    z[63]= - z[66] + z[6] + n<T>(1,2)*z[63] + 7*z[5];
    z[63]=z[9]*z[63];
    z[63]=n<T>(1,6)*z[63] + n<T>(1,4)*z[71] + z[68];
    z[63]=z[9]*z[63];
    z[66]=z[55]*z[7];
    z[66]=z[66] + z[24];
    z[68]= - z[3]*z[55];
    z[68]=z[68] - z[66];
    z[68]=z[10]*z[68];
    z[71]= - z[3] + z[57];
    z[71]=z[17]*z[71];
    z[72]= - z[6] - z[13];
    z[72]=z[31]*z[72];
    z[68]=z[72] + z[68] + z[71];
    z[71]=z[11] + z[13];
    z[72]= - z[55]*z[71];
    z[72]= - z[24] + z[72];
    z[72]=z[12]*z[72];
    z[71]=z[71] - z[55];
    z[73]=z[23]*z[71];
    z[72]=z[72] + z[73];
    z[73]=z[75] - n<T>(53,2)*z[4];
    z[73]=z[4]*z[73];
    z[74]=9*z[65] + n<T>(41,2)*z[6];
    z[74]=z[6]*z[74];
    z[73]=z[73] + z[74];
    z[71]= - n<T>(3,2)*z[6] - z[60] + z[71];
    z[71]=z[11]*z[71];
    z[61]=n<T>(3,2)*z[71] + n<T>(1,2)*z[73] - 3*z[61];
    z[61]=z[11]*z[61];
    z[71]=n<T>(181,4)*z[55] + n<T>(347,9)*z[4];
    z[71]=z[71]*npow(z[4],2);
    z[61]=z[71] + z[61];
    z[64]=z[64] + n<T>(1,3)*z[6] - n<T>(1,2)*z[65] + z[76];
    z[64]=z[21]*z[64];
    z[65]=z[55]*z[9];
    z[65]=z[65] + z[24];
    z[71]=z[6]*z[55];
    z[71]=z[71] + z[65];
    z[71]=z[16]*z[71];
    z[73]=z[55] - z[9];
    z[74]= - z[6] + z[73];
    z[74]=z[22]*z[74];
    z[71]= - z[34] + z[71] + z[74];
    z[74]= - z[5]*z[55];
    z[65]=z[74] - z[65];
    z[65]=z[15]*z[65];
    z[73]= - z[5] + z[73];
    z[73]=z[20]*z[73];
    z[65]=z[65] + z[73];
    z[73]=z[4]*z[55];
    z[66]=z[73] + z[66];
    z[66]=z[14]*z[66];
    z[73]= - z[4] + z[57];
    z[73]=z[18]*z[73];
    z[66]=z[66] + z[73];
    z[55]=z[60] + z[55];
    z[55]=z[55]*z[62];
    z[60]=z[69] + n<T>(3,8)*z[3];
    z[60]=z[3]*z[60];
    z[55]=z[55] + z[60];
    z[55]=z[3]*z[55];
    z[60]= - n<T>(3,8)*z[11] + n<T>(1,3)*z[8] - n<T>(31,12)*z[13] - n<T>(7,18)*z[9]
     - n<T>(7,9)*z[6]
     + n<T>(1,18)*z[5] - n<T>(1,6)*z[7]
     + n<T>(45,4)*z[4]
     + n<T>(11,9)*z[3];
    z[60]=z[24]*z[60];
    z[60]= - z[33] + z[60] - z[52] + z[51] - z[44];
    z[62]=z[3] + z[8];
    z[62]=z[28]*z[62];
    z[62]= - z[35] + z[62] + z[38];
    z[69]=z[48] + z[36];
    z[57]=5*z[9] + n<T>(83,4)*z[5] + n<T>(47,2)*z[4] + z[57];
    z[57]= - n<T>(5,6)*z[2] + n<T>(1,6)*z[57] + z[13];
    z[57]=z[19]*z[57];
    z[56]= - 2*z[5] - z[4] - z[3] - z[56];
    z[56]=z[29]*z[56];
    z[73]=n<T>(13,16)*z[4] + z[13];
    z[73]=z[30]*z[73];
    z[74]=z[25]*i;

    r += n<T>(41,48)*z[26] - n<T>(19,6)*z[27] + n<T>(3,4)*z[32] + n<T>(5,8)*z[37]
     - n<T>(65,24)*z[39]
     - n<T>(15,16)*z[40]
     - n<T>(31,12)*z[41]
     - n<T>(9,8)*z[42]
     - 4*z[43]
       + n<T>(5,12)*z[45] - n<T>(53,64)*z[46] - n<T>(5,16)*z[47] - n<T>(41,64)*z[49]
     - 
      n<T>(9,64)*z[50] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + 
      z[59] + n<T>(1,8)*z[60] + n<T>(1,16)*z[61] + n<T>(1,4)*z[62] + z[63] + z[64]
       + n<T>(19,12)*z[65] + n<T>(5,4)*z[66] + z[67] + n<T>(1,2)*z[68] + n<T>(3,8)*
      z[69] + z[70] + n<T>(1,6)*z[71] + n<T>(3,16)*z[72] + z[73] - n<T>(61,192)*
      z[74];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf154(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf154(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
