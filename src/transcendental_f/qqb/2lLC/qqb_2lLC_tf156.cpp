#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf156(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[85];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[15];
    z[10]=d[2];
    z[11]=d[11];
    z[12]=d[3];
    z[13]=d[12];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=e[6];
    z[30]=e[13];
    z[31]=e[14];
    z[32]=f[4];
    z[33]=f[6];
    z[34]=f[7];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[23];
    z[38]=f[30];
    z[39]=f[31];
    z[40]=f[32];
    z[41]=f[33];
    z[42]=f[35];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=f[50];
    z[47]=f[51];
    z[48]=f[52];
    z[49]=f[54];
    z[50]=f[55];
    z[51]=f[56];
    z[52]=f[58];
    z[53]=n<T>(2,3)*z[12];
    z[54]=n<T>(1,4)*z[7];
    z[55]=2*z[2];
    z[56]= - n<T>(17,8)*z[10] - z[55];
    z[56]=n<T>(29,18)*z[4] + z[53] - n<T>(1,4)*z[8] - n<T>(1,12)*z[14] - n<T>(2,3)*z[5]
    + n<T>(1,3)*z[56] - z[54];
    z[56]=z[4]*z[56];
    z[57]=n<T>(1,3)*z[5];
    z[58]=z[2] - n<T>(5,2)*z[5];
    z[58]=z[58]*z[57];
    z[59]=n<T>(1,2)*z[7];
    z[60]=n<T>(1,3)*z[2];
    z[61]= - n<T>(7,12)*z[8] + z[59] + z[60];
    z[61]=z[8]*z[61];
    z[62]=n<T>(1,2)*z[18];
    z[63]=npow(z[2],2);
    z[63]=n<T>(1,6)*z[63];
    z[64]=npow(z[10],2);
    z[65]=npow(z[7],2);
    z[66]=n<T>(1,2)*z[14];
    z[67]= - n<T>(1,3)*z[10] - z[66];
    z[67]=z[14]*z[67];
    z[68]= - z[3]*z[66];
    z[69]=n<T>(1,4)*z[12];
    z[70]=z[69] - z[8] + z[2] - z[5];
    z[70]=z[12]*z[70];
    z[56]=z[56] + n<T>(1,3)*z[70] + z[61] + z[68] + z[67] + z[58] - n<T>(1,4)*
    z[65] - z[63] - n<T>(17,24)*z[64] + n<T>(13,9)*z[24] + n<T>(7,3)*z[19] + n<T>(4,3)*
    z[30] - z[62];
    z[56]=z[4]*z[56];
    z[58]=z[15] - z[16];
    z[61]=n<T>(5,6)*z[3];
    z[67]=n<T>(1,2)*z[8];
    z[68]=z[67] - z[61] - z[66] - z[57] + n<T>(1,6)*z[7] - z[58];
    z[68]=z[8]*z[68];
    z[70]= - z[60] + n<T>(1,3)*z[14];
    z[71]=z[13] + n<T>(17,6)*z[10];
    z[53]=n<T>(23,24)*z[4] - z[53] + n<T>(7,6)*z[8] - z[59] + n<T>(1,2)*z[71] + 
    z[70];
    z[53]=z[4]*z[53];
    z[71]=n<T>(1,3)*z[7];
    z[72]=z[57] + z[71] - z[15] + z[60];
    z[72]=z[5]*z[72];
    z[73]= - n<T>(1,2)*z[13] + n<T>(2,3)*z[9];
    z[74]=z[69] - z[57] + n<T>(1,12)*z[7] - z[73];
    z[74]=z[12]*z[74];
    z[75]=z[19] - z[21];
    z[76]= - n<T>(2,3)*z[75];
    z[62]=n<T>(2,3)*z[17] + z[76] + z[62];
    z[77]=n<T>(7,6)*z[10];
    z[78]= - z[11] - n<T>(1,2)*z[10];
    z[78]=z[78]*z[77];
    z[79]=n<T>(1,3)*z[3];
    z[80]=z[79] + z[54];
    z[70]=n<T>(3,4)*z[6] - n<T>(5,4)*z[4] + n<T>(7,12)*z[12] - n<T>(2,3)*z[8] + z[16]
     + 
   n<T>(11,12)*z[10] + z[70] - z[80];
    z[70]=z[6]*z[70];
    z[81]=z[22] + n<T>(1,8)*z[65];
    z[82]=n<T>(7,6)*z[11];
    z[83]= - n<T>(3,4)*z[14] - z[82] + z[13];
    z[83]=z[14]*z[83];
    z[59]=z[59] + z[2];
    z[84]= - 2*z[9] + z[59];
    z[84]=n<T>(1,3)*z[84] + n<T>(1,4)*z[3];
    z[84]=z[3]*z[84];
    z[53]=z[70] + z[53] + z[74] + z[68] + z[84] + z[83] + z[72] + z[78]
    + z[20] - n<T>(7,6)*z[23] + z[81] + z[62];
    z[53]=z[1]*z[53];
    z[53]=n<T>(1,12)*z[25] + z[53];
    z[53]=i*z[53];
    z[68]=z[4] + z[7];
    z[70]=n<T>(5,12)*z[3];
    z[72]=n<T>(7,24)*z[12];
    z[74]=n<T>(1,3)*z[8];
    z[78]= - n<T>(11,8)*z[10] - z[2];
    z[68]=n<T>(7,36)*z[6] - z[72] + z[74] + z[70] - n<T>(7,12)*z[14] + n<T>(1,3)*
    z[78] - n<T>(1,8)*z[68];
    z[68]=z[6]*z[68];
    z[78]=z[60] + z[66];
    z[72]= - z[72] - z[74] + z[80] + z[78];
    z[72]=z[12]*z[72];
    z[80]= - n<T>(5,3)*z[10] - z[66];
    z[80]=z[80]*z[66];
    z[60]= - n<T>(2,3)*z[3] - z[60] + z[66];
    z[60]=z[3]*z[60];
    z[83]= - z[67] + z[2] + z[3];
    z[83]=z[83]*z[74];
    z[84]=z[12] - z[3];
    z[84]= - n<T>(5,8)*z[4] + n<T>(7,4)*z[10] + z[14] - n<T>(1,2)*z[84];
    z[84]=z[4]*z[84];
    z[60]=z[68] + z[84] + z[72] + z[83] + z[60] + z[80] - z[63] - n<T>(11,24)
   *z[64] + n<T>(8,9)*z[24] - n<T>(13,6)*z[21] + n<T>(5,6)*z[31] - z[81];
    z[60]=z[6]*z[60];
    z[59]= - z[79] - n<T>(1,3)*z[59] + z[66];
    z[59]=z[3]*z[59];
    z[66]= - z[54] + z[5];
    z[66]=n<T>(1,3)*z[66] - z[14];
    z[66]= - z[69] + n<T>(1,2)*z[66] + z[79];
    z[66]=z[12]*z[66];
    z[68]= - z[2] + n<T>(1,2)*z[5];
    z[69]=z[68]*z[57];
    z[72]=z[5] + z[3];
    z[72]=z[72]*z[74];
    z[74]=npow(z[14],2);
    z[59]=z[66] + z[72] + z[59] + n<T>(1,2)*z[74] + z[69] - n<T>(1,24)*z[65] - n<T>(7,72)*z[24]
     - z[62];
    z[59]=z[12]*z[59];
    z[62]= - z[54] + 2*z[5];
    z[62]= - z[67] + z[70] + n<T>(1,3)*z[62] + n<T>(1,4)*z[14];
    z[62]=z[8]*z[62];
    z[66]= - z[7] + z[68];
    z[66]=z[66]*z[57];
    z[67]=n<T>(1,12)*z[3] - z[78];
    z[67]=z[3]*z[67];
    z[62]=z[62] + z[67] + n<T>(1,4)*z[74] + z[66] - n<T>(1,12)*z[65] + n<T>(1,6)*
    z[24] - z[22] - z[76] - z[20];
    z[62]=z[8]*z[62];
    z[58]= - z[82] - z[58] - z[73];
    z[58]=z[24]*z[58];
    z[63]=z[63] + n<T>(1,6)*z[65];
    z[66]=z[2] + n<T>(7,6)*z[5];
    z[57]=z[66]*z[57];
    z[57]=z[57] - n<T>(1,9)*z[24] + n<T>(5,3)*z[19] + n<T>(1,3)*z[29] - z[20] + 
    z[63];
    z[57]=z[5]*z[57];
    z[54]= - z[61] + z[55] + z[54];
    z[54]=z[54]*z[79];
    z[55]=n<T>(1,2)*z[28];
    z[61]= - z[55] - 2*z[17];
    z[54]=z[54] - n<T>(7,36)*z[24] + n<T>(1,3)*z[61] - n<T>(3,2)*z[21] + z[63];
    z[54]=z[3]*z[54];
    z[61]=n<T>(1,2)*z[64] + z[23] - n<T>(1,4)*z[24];
    z[61]=z[61]*z[77];
    z[63]=z[65] + z[24];
    z[55]=z[29] - z[55] + n<T>(1,2)*z[63];
    z[55]=z[55]*z[71];
    z[63]=n<T>(7,2)*z[23] - 2*z[31] - n<T>(1,2)*z[30];
    z[63]=n<T>(7,12)*z[64] - n<T>(4,9)*z[24] + n<T>(1,3)*z[63] - z[18];
    z[63]=z[14]*z[63];
    z[64]= - z[42] + z[37] - z[38] + z[45] + z[49];
    z[65]=z[40] + z[48];
    z[66]=z[34] + z[41];
    z[67]=z[26] - z[46];
    z[68]= - z[44] + n<T>(11,6)*z[50] + n<T>(5,2)*z[52] + z[51];
    z[69]= - z[2]*z[75];

    r +=  - n<T>(9,4)*z[27] - n<T>(5,8)*z[32] - n<T>(3,4)*z[33] + n<T>(1,24)*z[35]
     - n<T>(1,8)*z[36]
     - n<T>(11,12)*z[39]
     + n<T>(1,12)*z[43] - n<T>(17,24)*z[47]
     + z[53]
       + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61]
       + z[62] + z[63] + n<T>(1,3)*z[64] - n<T>(1,2)*z[65] - n<T>(8,3)*z[66] + n<T>(5,6)
      *z[67] + n<T>(1,4)*z[68] + n<T>(2,3)*z[69];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf156(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf156(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
