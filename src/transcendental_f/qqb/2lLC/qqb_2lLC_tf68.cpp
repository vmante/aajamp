#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf68(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[11];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=c[3];
    z[6]=d[3];
    z[7]=z[3] + 3*z[4];
    z[7]=z[4]*z[7];
    z[8]= - z[3] - z[2];
    z[8]=z[2]*z[8];
    z[9]=4*z[2] + z[3] + z[4];
    z[9]=i*z[1]*z[9];
    z[10]=z[3] - z[4];
    z[10]=z[6]*z[10];
    z[7]=n<T>(19,9)*z[5] + z[10] + z[9] + z[7] + 2*z[8];

    r += 4*z[7];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf68(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf68(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d
);
#endif
