#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf120(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[56];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[4];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[2];
    z[14]=e[4];
    z[15]=e[8];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=e[3];
    z[25]=e[10];
    z[26]=f[0];
    z[27]=f[1];
    z[28]=f[3];
    z[29]=f[4];
    z[30]=f[5];
    z[31]=f[10];
    z[32]=f[11];
    z[33]=f[12];
    z[34]=f[14];
    z[35]=f[16];
    z[36]=f[17];
    z[37]=f[18];
    z[38]=n<T>(1,2)*z[3];
    z[39]=z[1]*i;
    z[40]=z[38] - z[39];
    z[41]= - z[3]*z[40];
    z[42]=z[39] - z[4];
    z[43]=z[4]*z[42];
    z[44]=z[39] - z[3];
    z[45]= - n<T>(3,2)*z[7] - z[44];
    z[45]=z[7]*z[45];
    z[46]=z[7] + z[3];
    z[47]= - z[4] - z[46];
    z[47]=n<T>(1,2)*z[47] + 2*z[6];
    z[47]=z[6]*z[47];
    z[41]=z[47] + z[45] + z[43] + z[41];
    z[41]=z[6]*z[41];
    z[43]=3*z[3];
    z[45]= - z[40]*z[43];
    z[47]=2*z[39];
    z[48]=z[47] - z[2];
    z[49]=n<T>(1,2)*z[4];
    z[50]= - z[49] - z[48];
    z[50]=z[2]*z[50];
    z[51]=z[47] - z[4];
    z[51]=z[4]*z[51];
    z[52]=npow(z[7],2);
    z[45]=z[50] + n<T>(3,2)*z[52] + z[51] + z[45];
    z[45]=z[2]*z[45];
    z[50]= - z[39]*z[46];
    z[50]= - z[16] + z[50];
    z[50]=z[10]*z[50];
    z[46]= - z[39] + z[46];
    z[46]=z[14]*z[46];
    z[51]=z[7] + z[6];
    z[51]=z[24]*z[51];
    z[46]=z[51] + z[50] + z[46];
    z[49]=z[49] - z[39];
    z[50]=n<T>(3,2)*z[4];
    z[50]=z[49]*z[50];
    z[38]=3*z[39] - z[38];
    z[38]=z[3]*z[38];
    z[51]= - n<T>(1,2)*z[42] - z[3];
    z[51]=n<T>(3,2)*z[51] + z[7];
    z[51]=z[7]*z[51];
    z[38]=z[51] + z[50] + z[38];
    z[38]=z[7]*z[38];
    z[49]=z[49]*z[4];
    z[51]=3*z[7];
    z[52]=n<T>(1,2)*z[7] + z[42];
    z[52]=z[52]*z[51];
    z[52]=z[49] + z[52];
    z[51]=z[51] - z[42];
    z[51]=n<T>(1,4)*z[51] - z[6];
    z[51]=z[8]*z[51];
    z[53]= - n<T>(1,2)*z[6] - z[42];
    z[53]=z[6]*z[53];
    z[51]=z[51] + n<T>(1,2)*z[52] + z[53];
    z[51]=z[8]*z[51];
    z[40]= - z[4] + z[40];
    z[40]=z[40]*z[43];
    z[40]=z[49] + z[40];
    z[47]= - n<T>(1,2)*z[2] + z[47] + z[4];
    z[47]=z[2]*z[47];
    z[49]=z[4] + z[43];
    z[49]=n<T>(1,4)*z[49] - z[2];
    z[49]=z[5]*z[49];
    z[40]=z[49] + n<T>(1,2)*z[40] + z[47];
    z[40]=z[5]*z[40];
    z[44]=n<T>(3,4)*z[4] - z[44];
    z[44]=z[3]*z[44];
    z[44]=z[50] + z[44];
    z[44]=z[3]*z[44];
    z[47]=z[6] + z[4];
    z[39]=z[39]*z[47];
    z[39]=z[16] + z[39];
    z[39]=z[9]*z[39];
    z[47]= - z[36] - z[35] + z[33] - z[28];
    z[49]=z[32] + z[29];
    z[50]=z[3] - z[48];
    z[50]=z[11]*z[50];
    z[50]=z[50] + z[26];
    z[52]=10*z[22] + n<T>(1,4)*z[17];
    z[52]=i*z[52];
    z[43]= - z[43] - n<T>(13,3)*z[7];
    z[43]= - n<T>(1,12)*z[5] - n<T>(1,3)*z[8] + n<T>(4,3)*z[2] + n<T>(1,4)*z[43]
     + n<T>(2,3)
   *z[6];
    z[43]=z[16]*z[43];
    z[53]=z[4] - z[48];
    z[53]=z[12]*z[53];
    z[42]=z[6] - z[42];
    z[42]=z[13]*z[42];
    z[48]=z[5] - z[48];
    z[48]=z[15]*z[48];
    z[54]=z[6] + z[8];
    z[54]=z[25]*z[54];
    z[55]=2*i;
    z[55]=z[20]*z[55];

    r += z[18] - n<T>(4,3)*z[19] - z[21] - 5*z[23] - n<T>(3,2)*z[27] + z[30] - 
      z[31] - n<T>(1,2)*z[34] + z[37] + z[38] + z[39] + z[40] + z[41] + 
      z[42] + z[43] + z[44] + z[45] + 2*z[46] - n<T>(3,4)*z[47] + z[48] + n<T>(1,4)*z[49]
     + 3*z[50]
     + z[51]
     + z[52]
     + z[53]
     + z[54]
     + z[55];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf120(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf120(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
