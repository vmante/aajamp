#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf213(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[19];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[7];
    z[4]=d[8];
    z[5]=d[9];
    z[6]=d[18];
    z[7]=c[3];
    z[8]=c[11];
    z[9]=c[15];
    z[10]=c[31];
    z[11]=e[10];
    z[12]=e[11];
    z[13]=f[6];
    z[14]=z[2] + z[4];
    z[15]=i*z[1];
    z[16]=z[15] - z[3];
    z[14]= - z[16] - n<T>(1,2)*z[14];
    z[14]=z[2]*z[14];
    z[17]=npow(z[3],2);
    z[17]= - n<T>(1,2)*z[17] - z[11] + n<T>(1,6)*z[7];
    z[18]=z[3]*z[15];
    z[16]= - z[4]*z[16];
    z[14]=z[14] + z[16] + z[18] + z[17];
    z[14]=z[2]*z[14];
    z[15]=z[6]*z[15];
    z[15]=z[15] - z[12] + z[17];
    z[15]=z[4]*z[15];
    z[16]= - z[5]*z[12];
    z[17]=z[6] - z[3];
    z[17]=z[1]*z[5]*z[17];
    z[17]=n<T>(1,6)*z[8] + z[17];
    z[17]=i*z[17];

    r +=  - z[9] + n<T>(3,2)*z[10] - z[13] + z[14] + z[15] + z[16] + z[17];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf213(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf213(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
