#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf146(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[44];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[16];
    z[9]=d[4];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[4];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[17];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[30];
    z[24]=c[31];
    z[25]=f[0];
    z[26]=f[1];
    z[27]=f[3];
    z[28]=f[5];
    z[29]=f[11];
    z[30]=f[17];
    z[31]=f[18];
    z[32]=f[76];
    z[33]=z[1]*i;
    z[34]=z[33]*z[2];
    z[35]=npow(z[2],2);
    z[34]= - z[35] + 4*z[34];
    z[36]=2*z[33];
    z[37]=z[36] - z[4];
    z[38]=z[37]*z[4];
    z[34]= - z[38] + n<T>(2,3)*z[34];
    z[38]=2*z[2];
    z[39]= - z[38] + z[33];
    z[39]=n<T>(1,3)*z[39] + z[4];
    z[40]=n<T>(1,3)*z[5];
    z[39]=2*z[39] - z[40];
    z[39]=z[5]*z[39];
    z[41]= - z[4] + n<T>(4,3)*z[2];
    z[40]= - z[40] + z[41];
    z[40]=z[7]*z[40];
    z[39]=z[40] + z[39] - z[34];
    z[39]=z[7]*z[39];
    z[40]=z[35]*z[33];
    z[38]=z[38]*z[33];
    z[42]=z[2] - n<T>(2,3)*z[33];
    z[42]=z[4]*z[42];
    z[42]= - z[38] + z[42];
    z[42]=z[4]*z[42];
    z[43]=npow(z[2],3);
    z[40]=z[42] - z[43] + n<T>(7,3)*z[40];
    z[41]=z[5]*z[41];
    z[34]=z[41] - z[34];
    z[34]=z[5]*z[34];
    z[41]= - z[4]*z[33];
    z[42]=npow(z[9],2);
    z[41]=z[41] - n<T>(1,3)*z[42];
    z[41]=z[9]*z[41];
    z[42]=z[6] + z[3];
    z[43]=n<T>(1,3)*z[42];
    z[37]= - n<T>(1,3)*z[9] - z[2] + z[37];
    z[37]=4*z[37] - z[43];
    z[37]=z[10]*z[37];
    z[34]= - z[27] - z[30] + z[37] + n<T>(4,3)*z[41] + z[39] + 2*z[40] + 
    z[34];
    z[37]=z[4] - z[2];
    z[39]=2*z[9];
    z[37]=z[37]*z[39];
    z[39]=npow(z[4],2);
    z[35]= - z[37] - z[35] + z[38] + z[39];
    z[35]=z[35]*z[43];
    z[37]=z[9] + z[4];
    z[38]=4*z[37] - z[42];
    z[38]=z[8]*z[33]*z[38];
    z[33]=z[33] - z[37];
    z[33]=4*z[33] + z[42];
    z[33]=z[12]*z[33];
    z[37]=z[9] + z[7] - 11*z[4] + z[5];
    z[37]=n<T>(1,3)*z[37] + 4*z[8];
    z[37]=z[14]*z[37];
    z[33]= - z[29] + z[37] + z[38] + z[33];
    z[36]=z[36] - z[2];
    z[37]= - z[5] + z[36];
    z[37]=z[11]*z[37];
    z[36]= - z[7] + z[36];
    z[36]=z[13]*z[36];
    z[36]= - z[37] - z[36] + z[28] - z[23];
    z[37]=z[31] - z[18];
    z[38]= - n<T>(80,3)*z[21] - 8*z[19] - n<T>(4,3)*z[17] - n<T>(2,9)*z[15];
    z[38]=i*z[38];
    z[33]=n<T>(16,3)*z[32] + n<T>(4,3)*z[26] - 8*z[25] - n<T>(7,3)*z[24] + n<T>(40,3)*
    z[22] + n<T>(32,9)*z[20] - n<T>(10,3)*z[16] + z[38] + z[35] - n<T>(8,9)*z[37]
     - 
   n<T>(8,3)*z[36] + 2*z[34] + n<T>(2,3)*z[33];

    r += 2*z[33];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf146(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf146(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
