#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf195(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=d[5];
    z[8]=e[2];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[17];
    z[13]=c[31];
    z[14]=e[3];
    z[15]=e[10];
    z[16]=f[2];
    z[17]=f[4];
    z[18]=f[10];
    z[19]=f[12];
    z[20]=f[16];
    z[21]=f[19];
    z[22]=npow(z[5],2);
    z[23]= - n<T>(1,3)*z[14] - z[8];
    z[24]=i*z[1];
    z[25]=z[24] - z[3];
    z[25]= - n<T>(8,9)*z[4] + z[5] + n<T>(19,3)*z[25];
    z[25]=z[4]*z[25];
    z[23]=z[25] + 8*z[23] + z[22];
    z[25]=z[7] + n<T>(2,3)*z[5];
    z[26]=n<T>(1,9)*z[3];
    z[27]= - z[26] + n<T>(2,9)*z[24] - z[25];
    z[27]=z[3]*z[27];
    z[25]= - n<T>(8,3)*z[6] + z[25];
    z[25]=z[25]*z[24];
    z[23]=z[27] + z[25] + n<T>(1,3)*z[23];
    z[23]=z[4]*z[23];
    z[25]= - 25*z[6] - 4*z[5];
    z[25]=z[25]*z[24];
    z[27]=z[22] - z[15];
    z[25]=z[25] - 25*z[8] + n<T>(8,3)*z[9] - 17*z[14] - 4*z[27];
    z[26]=n<T>(16,9)*z[4] - z[26] + z[7] + n<T>(25,9)*z[24];
    z[26]=z[4]*z[26];
    z[27]=n<T>(4,9)*z[24];
    z[28]=z[27] + z[7] + n<T>(4,9)*z[5];
    z[28]=z[3]*z[28];
    z[27]= - n<T>(55,27)*z[2] + n<T>(5,9)*z[4] + n<T>(7,3)*z[3] + z[27] - z[7] - n<T>(2,9)*z[5];
    z[27]=z[2]*z[27];
    z[25]=z[27] + z[26] + n<T>(1,9)*z[25] + z[28];
    z[25]=z[2]*z[25];
    z[26]= - z[14] - z[8];
    z[26]=z[7]*z[26];
    z[23]=z[25] + z[26] + z[23];
    z[25]= - 2*z[6] - z[5];
    z[25]=z[25]*z[24];
    z[24]= - n<T>(8,3)*z[3] + z[5] + 8*z[24];
    z[24]=z[3]*z[24];
    z[24]=z[24] + 2*z[25] + z[22] + 5*z[9] - 4*z[8];
    z[24]=z[3]*z[24];
    z[25]=z[6]*z[9];
    z[26]=z[15] - n<T>(1,3)*z[9];
    z[26]=z[5]*z[26];
    z[25]=z[25] - z[26];
    z[26]= - z[7]*z[6];
    z[22]= - n<T>(1,9)*z[22] + n<T>(4,9)*z[8] + z[26];
    z[22]=z[1]*z[22];
    z[22]=z[22] - 2*z[12] - n<T>(1,27)*z[10];
    z[26]=2*i;
    z[22]=z[22]*z[26];
    z[26]=z[16] + z[21];
    z[26]=n<T>(1,3)*z[17] - n<T>(4,3)*z[18] - n<T>(7,3)*z[19] + z[20] - n<T>(8,3)*z[26];
    z[22]=n<T>(2,9)*z[24] + z[22] - n<T>(10,9)*z[11] + n<T>(2,3)*z[26] + z[13] - n<T>(8,9)*z[25]
     + 2*z[23];

    r += n<T>(2,3)*z[22];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf195(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf195(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
