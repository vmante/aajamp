#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf118(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[71];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[9];
    z[10]=d[8];
    z[11]=d[15];
    z[12]=d[16];
    z[13]=d[18];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[2];
    z[17]=e[4];
    z[18]=e[8];
    z[19]=e[10];
    z[20]=e[11];
    z[21]=c[3];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=e[3];
    z[30]=f[0];
    z[31]=f[1];
    z[32]=f[2];
    z[33]=f[3];
    z[34]=f[4];
    z[35]=f[5];
    z[36]=f[6];
    z[37]=f[7];
    z[38]=f[9];
    z[39]=f[10];
    z[40]=f[11];
    z[41]=f[12];
    z[42]=f[13];
    z[43]=f[16];
    z[44]=f[17];
    z[45]=f[18];
    z[46]=f[23];
    z[47]=f[30];
    z[48]=f[41];
    z[49]=f[50];
    z[50]=f[60];
    z[51]= - z[5] - z[3];
    z[52]=z[1]*i;
    z[51]=z[52]*z[51];
    z[51]= - z[21] + z[51];
    z[51]=z[11]*z[51];
    z[53]=z[6] + z[4];
    z[54]= - z[52]*z[53];
    z[54]= - z[21] + z[54];
    z[54]=z[12]*z[54];
    z[55]=z[10] + z[8];
    z[56]=z[52]*z[55];
    z[56]=z[21] + z[56];
    z[56]=z[13]*z[56];
    z[53]= - z[52] + z[53];
    z[53]=z[17]*z[53];
    z[55]=z[52] - z[55];
    z[55]=z[20]*z[55];
    z[57]=z[52] - z[5];
    z[58]= - z[3] + z[57];
    z[58]=z[16]*z[58];
    z[51]=z[53] + z[55] + z[30] + z[23] + z[51] + z[54] + z[56] + z[58];
    z[53]=2*z[5];
    z[54]=z[8] - z[2];
    z[55]=z[53] - z[52] + z[54];
    z[55]=z[55]*z[53];
    z[56]=2*z[2];
    z[58]=z[56]*z[52];
    z[59]=npow(z[2],2);
    z[60]=z[58] + z[59];
    z[61]=z[52] + z[2];
    z[62]= - 2*z[61] + z[8];
    z[62]=z[8]*z[62];
    z[61]= - n<T>(11,3)*z[3] - z[5] + 4*z[61] - z[8];
    z[61]=z[3]*z[61];
    z[55]=z[61] + z[55] + z[62] + z[60];
    z[55]=z[3]*z[55];
    z[61]=2*z[52];
    z[62]=z[61] - z[54];
    z[63]= - z[4] + z[62];
    z[63]=z[4]*z[63];
    z[64]=z[8]*z[2];
    z[65]=z[4] - z[2];
    z[66]=z[6]*z[65];
    z[63]=z[66] + z[63] - z[58] + z[64];
    z[64]= - z[7]*z[65];
    z[63]=2*z[63] + z[64];
    z[63]=z[7]*z[63];
    z[64]= - z[8]*z[62];
    z[65]=z[54]*z[4];
    z[66]= - z[10]*z[54];
    z[58]=z[66] + z[65] + z[58] + z[64];
    z[64]=z[9]*z[54];
    z[58]=2*z[58] + z[64];
    z[58]=z[9]*z[58];
    z[55]= - z[38] + z[58] + z[55] + z[63];
    z[58]= - z[54] + z[57];
    z[58]= - 7*z[10] + 2*z[58] - 5*z[3];
    z[58]=z[19]*z[58];
    z[63]=z[9] - z[6];
    z[64]=z[61] - z[2];
    z[66]=z[4] - z[64];
    z[63]=n<T>(11,3)*z[66] + 2*z[63];
    z[63]=z[14]*z[63];
    z[66]=z[7] - z[10];
    z[67]=z[8] - z[64];
    z[66]=n<T>(1,3)*z[67] - 2*z[66];
    z[66]=z[18]*z[66];
    z[64]=z[5] - z[64];
    z[64]=z[15]*z[64];
    z[67]= - z[3] - z[6];
    z[67]=z[29]*z[67];
    z[58]= - z[49] + z[48] + z[47] - z[46] - z[66] - z[64] - z[67] - 
    z[50] - z[58] - z[63] - z[45] + z[39] + z[32] + z[26];
    z[63]=4*z[52];
    z[54]=z[63] - z[54];
    z[54]=2*z[54] - z[4];
    z[54]=z[4]*z[54];
    z[64]=z[8]*z[56];
    z[66]=2*z[57];
    z[67]=z[66] + z[3];
    z[67]=z[3]*z[67];
    z[54]=z[67] + z[54] + z[64] - z[60];
    z[60]=8*z[2];
    z[64]=4*z[3];
    z[67]=4*z[6] + z[64] - 8*z[4] + z[60] + z[57];
    z[67]=z[6]*z[67];
    z[68]=z[61] - z[5];
    z[68]=z[5]*z[68];
    z[54]=z[67] + z[68] + 2*z[54];
    z[54]=z[6]*z[54];
    z[67]=z[52]*z[2];
    z[68]=5*z[67] - 2*z[59];
    z[69]=z[8] + z[2];
    z[69]= - z[52] - n<T>(5,3)*z[69];
    z[69]=z[8]*z[69];
    z[68]=n<T>(2,3)*z[68] + z[69];
    z[68]=z[8]*z[68];
    z[69]= - z[59]*z[61];
    z[70]=npow(z[2],3);
    z[69]=z[70] + z[69];
    z[68]=2*z[69] + z[68];
    z[69]= - z[59] + 4*z[67];
    z[60]=z[60] - z[52];
    z[60]=2*z[60] + z[8];
    z[70]=n<T>(1,3)*z[8];
    z[60]=z[60]*z[70];
    z[70]= - 4*z[2] + z[70];
    z[70]=z[5]*z[70];
    z[60]=z[70] + 2*z[69] + z[60];
    z[60]=z[5]*z[60];
    z[69]=z[9] - z[7];
    z[64]=z[6] - z[64] + n<T>(43,3)*z[4] - 4*z[5] + 16*z[2] - n<T>(55,3)*z[8];
    z[64]=n<T>(1,3)*z[64] + 10*z[10] - n<T>(4,3)*z[69];
    z[64]=z[21]*z[64];
    z[54]=z[41] + z[64] + z[54] + 2*z[68] + z[60];
    z[53]= - z[3] + z[53] - z[62];
    z[53]=z[3]*z[53];
    z[53]=z[65] + z[53];
    z[60]= - z[56] + 2*z[8];
    z[62]=7*z[52] - z[60];
    z[62]=2*z[62] - 7*z[5];
    z[62]=z[5]*z[62];
    z[57]= - n<T>(8,9)*z[10] - z[6] + n<T>(10,3)*z[3] + z[60] + n<T>(7,3)*z[57];
    z[57]=z[10]*z[57];
    z[60]= - z[8]*z[63];
    z[63]= - z[66] - z[6];
    z[63]=z[6]*z[63];
    z[53]=z[57] + z[63] + z[60] + n<T>(1,3)*z[62] + n<T>(4,3)*z[53];
    z[53]=z[10]*z[53];
    z[57]=z[59] + 20*z[67];
    z[56]= - z[56] - 11*z[52];
    z[56]=n<T>(1,9)*z[56] - z[8];
    z[59]=n<T>(11,9)*z[5];
    z[56]=2*z[56] + z[59];
    z[56]=z[5]*z[56];
    z[52]= - n<T>(10,3)*z[2] + z[52];
    z[52]=n<T>(10,9)*z[4] + z[59] + n<T>(2,3)*z[52] + z[8];
    z[52]=z[4]*z[52];
    z[59]=z[61] - z[8];
    z[59]= - z[8]*z[59];
    z[52]=z[52] + z[56] + n<T>(2,9)*z[57] + z[59];
    z[52]=z[4]*z[52];
    z[56]=z[35] - z[24];
    z[57]=n<T>(40,3)*z[27] + n<T>(8,3)*z[25] - n<T>(1,3)*z[22];
    z[57]=i*z[57];

    r +=  - n<T>(20,3)*z[28] - n<T>(8,3)*z[31] + n<T>(11,9)*z[33] - n<T>(11,3)*z[34]
     - 
      2*z[36] - n<T>(32,3)*z[37] + n<T>(1,9)*z[40] - n<T>(4,9)*z[42] - z[43] + 
      z[44] + 4*z[51] + z[52] + z[53] + n<T>(1,3)*z[54] + n<T>(2,3)*z[55] + n<T>(16,9)*z[56]
     + z[57] - n<T>(4,3)*z[58];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf118(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf118(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
