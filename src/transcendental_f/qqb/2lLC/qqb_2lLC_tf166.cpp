#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf166(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[21];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[7];
    z[5]=d[6];
    z[6]=d[17];
    z[7]=e[6];
    z[8]=e[9];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[31];
    z[13]=e[7];
    z[14]=f[31];
    z[15]=npow(z[2],2);
    z[15]=z[7] + n<T>(1,2)*z[15];
    z[16]=z[3] + z[5];
    z[17]=z[16] - z[2];
    z[17]=z[17]*z[3];
    z[17]= - z[17] - z[8] + z[15];
    z[18]=npow(z[5],2);
    z[18]=n<T>(1,2)*z[18];
    z[19]= - z[5] - z[6] + 2*z[2];
    z[19]=i*z[1]*z[19];
    z[20]=z[5] - z[2];
    z[20]=z[4]*z[20];
    z[19]=z[20] + z[19] + z[18] + z[17];
    z[19]=z[4]*z[19];
    z[16]=z[3]*z[16];
    z[15]=n<T>(1,2)*z[16] + z[13] - z[15];
    z[15]=z[3]*z[15];
    z[16]= - z[6] + n<T>(1,2)*z[5];
    z[16]=z[5]*z[16];
    z[16]=z[16] - z[17];
    z[16]=z[1]*z[16];
    z[16]= - n<T>(1,6)*z[10] + z[16];
    z[16]=i*z[16];
    z[17]=z[2] - z[6];
    z[17]=z[9]*z[17];
    z[18]= - z[18] + z[13] - z[8];
    z[18]=z[5]*z[18];

    r += z[11] - n<T>(3,2)*z[12] - z[14] + z[15] + z[16] + z[17] + z[18] + 
      z[19];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf166(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf166(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
