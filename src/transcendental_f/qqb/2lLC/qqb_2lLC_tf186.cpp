#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf186(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[12];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=e[6];
    z[30]=e[13];
    z[31]=e[14];
    z[32]=f[4];
    z[33]=f[6];
    z[34]=f[7];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[23];
    z[38]=f[30];
    z[39]=f[31];
    z[40]=f[32];
    z[41]=f[33];
    z[42]=f[35];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=f[50];
    z[47]=f[51];
    z[48]=f[52];
    z[49]=f[54];
    z[50]=f[55];
    z[51]=f[56];
    z[52]=f[58];
    z[53]= - z[9] - z[7];
    z[54]=n<T>(2,9)*z[6];
    z[55]=z[54] + z[4];
    z[53]=z[55]*z[53];
    z[56]=2*z[4];
    z[57]=z[1]*i;
    z[58]=z[57] + z[56];
    z[58]=z[4]*z[58];
    z[59]=z[57] + z[6];
    z[54]=z[59]*z[54];
    z[59]=n<T>(1,9)*z[3];
    z[60]=n<T>(1,2)*z[4];
    z[61]= - n<T>(1,2)*z[5] - z[59] + z[60] + n<T>(1,9)*z[6];
    z[61]=z[2]*z[61];
    z[62]=z[57] - z[9];
    z[63]=z[62] - z[7];
    z[64]= - 2*z[3] + z[6] - z[63];
    z[64]=z[3]*z[64];
    z[65]= - z[5] - z[4] - z[63];
    z[65]=z[5]*z[65];
    z[53]=z[61] + z[65] + n<T>(2,9)*z[64] + z[58] + z[54] + z[53];
    z[53]=z[2]*z[53];
    z[54]=7*z[4];
    z[58]=5*z[7];
    z[61]= - z[58] + 10*z[57] + z[54];
    z[61]=z[7]*z[61];
    z[64]= - z[58] - z[57] + 14*z[4];
    z[64]= - 4*z[5] + n<T>(2,3)*z[64] - z[9];
    z[64]=z[5]*z[64];
    z[65]=npow(z[4],2);
    z[66]=2*z[57];
    z[67]=z[66] - n<T>(5,3)*z[4];
    z[67]= - n<T>(5,3)*z[9] + n<T>(1,3)*z[67] - z[7];
    z[67]=z[9]*z[67];
    z[68]=n<T>(1,3)*z[3];
    z[69]= - z[4]*z[68];
    z[61]=n<T>(1,3)*z[64] + z[69] + z[67] + n<T>(5,2)*z[65] + n<T>(2,9)*z[61];
    z[61]=z[5]*z[61];
    z[64]=2*z[6];
    z[67]=2*z[7];
    z[69]=5*z[57];
    z[70]= - n<T>(5,2)*z[9] - z[67] + z[69] - z[64];
    z[71]=n<T>(1,3)*z[9];
    z[70]=z[70]*z[71];
    z[72]= - n<T>(35,6)*z[6] + n<T>(5,3)*z[57] + z[56];
    z[72]=z[6]*z[72];
    z[66]=z[66] - z[6];
    z[73]=n<T>(5,3)*z[66] - 4*z[7];
    z[73]=z[7]*z[73];
    z[74]=19*z[57];
    z[75]=n<T>(97,3)*z[3] - z[9] - z[74] - 7*z[6];
    z[75]=z[3]*z[75];
    z[70]=n<T>(1,6)*z[75] + z[70] + z[72] + z[73];
    z[68]=z[70]*z[68];
    z[70]= - z[57] + z[60];
    z[70]=z[4]*z[70];
    z[72]=z[11] + z[14];
    z[73]=z[72] - z[57];
    z[75]=n<T>(5,6)*z[4];
    z[76]=n<T>(7,6)*z[6] + z[75] - z[73];
    z[76]=z[11]*z[76];
    z[77]=n<T>(7,18)*z[6] - n<T>(7,9)*z[57] - z[4];
    z[77]=z[6]*z[77];
    z[78]=z[56] + z[6];
    z[78]=z[14]*z[78];
    z[70]=n<T>(1,3)*z[76] + n<T>(2,9)*z[78] + n<T>(5,9)*z[70] + z[77];
    z[70]=z[11]*z[70];
    z[76]=z[57] - z[4];
    z[76]=z[76]*z[56];
    z[77]=n<T>(1,3)*z[6];
    z[78]=n<T>(43,12)*z[6] - n<T>(43,6)*z[57] - z[4];
    z[78]=z[78]*z[77];
    z[74]=19*z[7] + n<T>(43,2)*z[6] - z[74] + z[4];
    z[74]=z[7]*z[74];
    z[74]=n<T>(1,18)*z[74] + z[76] + z[78];
    z[74]=z[7]*z[74];
    z[76]= - 32*z[57] + z[54];
    z[76]=z[4]*z[76];
    z[64]=z[66]*z[64];
    z[64]=z[76] + z[64];
    z[66]=z[77] + n<T>(16,3)*z[4] - z[62];
    z[66]=z[66]*z[71];
    z[55]=z[7]*z[55];
    z[55]=z[66] + n<T>(1,9)*z[64] + z[55];
    z[55]=z[9]*z[55];
    z[54]= - n<T>(19,9)*z[57] - z[54];
    z[54]=z[54]*z[65];
    z[56]= - n<T>(25,18)*z[6] - n<T>(19,4)*z[57] + z[56];
    z[56]=z[6]*z[56];
    z[64]=4*z[57] + z[4];
    z[64]=z[4]*z[64];
    z[56]=z[64] + z[56];
    z[56]=z[56]*z[77];
    z[53]=z[44] + z[53] + z[61] + z[68] + z[55] + z[74] + n<T>(1,2)*z[54] + 
    z[56] + z[70] - z[45];
    z[54]=z[57] + z[75];
    z[55]=n<T>(1,3)*z[4];
    z[54]=z[54]*z[55];
    z[55]=n<T>(1,2)*z[6];
    z[56]=z[55] + z[57];
    z[56]=z[56]*z[55];
    z[61]=n<T>(1,2)*z[7];
    z[64]=n<T>(5,54)*z[7] - n<T>(5,27)*z[57] - z[6];
    z[64]=z[64]*z[61];
    z[65]= - n<T>(1,9)*z[9] + n<T>(2,9)*z[57] - z[4];
    z[65]=z[65]*z[71];
    z[66]=z[4] + n<T>(8,3)*z[57];
    z[68]= - n<T>(5,6)*z[3] + n<T>(8,3)*z[7] - z[66];
    z[59]=z[68]*z[59];
    z[68]=n<T>(1,6)*z[57] + z[4];
    z[68]= - n<T>(1,54)*z[7] + n<T>(5,9)*z[68] + z[55];
    z[68]= - n<T>(5,54)*z[8] - n<T>(7,27)*z[5] - n<T>(13,54)*z[3] + n<T>(1,2)*z[68]
     - n<T>(1,27)*z[9];
    z[68]=z[8]*z[68];
    z[54]=z[68] + z[59] + z[65] + z[64] + z[54] + z[56];
    z[56]= - n<T>(1,3)*z[62] + n<T>(1,11)*z[3];
    z[56]=z[5]*z[56];
    z[54]=n<T>(1,9)*z[56] + n<T>(1,11)*z[54];
    z[54]=z[8]*z[54];
    z[56]= - z[57] + n<T>(1,2)*z[9];
    z[55]= - z[61] + z[55] + z[60] - z[56];
    z[55]=z[14]*z[55];
    z[59]=z[4] + z[6];
    z[59]=z[59]*z[67];
    z[60]= - z[69] - 4*z[4];
    z[60]=z[4]*z[60];
    z[61]= - n<T>(5,6)*z[6] - z[66];
    z[61]=z[6]*z[61];
    z[56]= - z[9]*z[56];
    z[64]= - z[7] + z[9];
    z[64]=z[3]*z[64];
    z[55]=z[55] + z[64] + z[56] + z[59] + n<T>(2,3)*z[60] + z[61];
    z[55]=z[14]*z[55];
    z[56]= - n<T>(43,2)*z[7] - 161*z[4] - 125*z[6];
    z[56]=n<T>(1,3)*z[56] + z[9];
    z[56]=4*z[14] - n<T>(4,3)*z[8] + n<T>(59,6)*z[5] + n<T>(1,2)*z[56] + n<T>(14,3)*z[3]
   ;
    z[56]=n<T>(1,3)*z[56] + n<T>(1,2)*z[11];
    z[56]=z[24]*z[56];
    z[55]= - z[48] + z[55] + z[56];
    z[56]=2*z[9] + z[58] - z[69] + 17*z[6];
    z[56]= - n<T>(2,3)*z[2] + n<T>(1,3)*z[56] + 5*z[3];
    z[56]=z[21]*z[56];
    z[58]=z[57]*z[72];
    z[58]=z[24] + z[58];
    z[58]=z[12]*z[58];
    z[59]=z[57]*z[9];
    z[59]=z[59] + z[24];
    z[60]= - z[6]*z[57];
    z[60]=z[60] - z[59];
    z[60]=z[16]*z[60];
    z[61]=z[23]*z[73];
    z[64]=z[6] - z[62];
    z[64]=z[22]*z[64];
    z[56]= - z[61] + z[56] + z[58] + z[60] + z[64];
    z[58]=z[57]*z[7];
    z[58]=z[58] + z[24];
    z[60]=z[3]*z[57];
    z[60]=z[60] + z[58];
    z[60]=z[10]*z[60];
    z[61]=z[57] - z[7];
    z[64]=z[3] - z[61];
    z[64]=z[17]*z[64];
    z[60]=z[42] + z[60] + z[64];
    z[64]= - z[4]*z[57];
    z[58]=z[64] - z[58];
    z[58]=z[13]*z[58];
    z[61]=z[4] - z[61];
    z[61]=z[18]*z[61];
    z[58]= - z[26] + z[58] + z[61];
    z[61]= - z[5] - n<T>(5,11)*z[8];
    z[61]=z[29]*z[61];
    z[64]= - z[6] - n<T>(8,11)*z[14];
    z[64]=z[31]*z[64];
    z[61]=z[38] + z[61] + z[64];
    z[57]=z[5]*z[57];
    z[57]=z[57] + z[59];
    z[57]=z[15]*z[57];
    z[59]=z[5] - z[62];
    z[59]=z[20]*z[59];
    z[57]=z[57] + z[59];
    z[59]=z[46] - z[37];
    z[62]=z[52] + z[33];
    z[64]= - z[4] - 7*z[14];
    z[64]=z[30]*z[64];
    z[64]=z[64] + z[43];
    z[63]=z[2] - n<T>(28,9)*z[5] - n<T>(37,9)*z[4] + z[63];
    z[63]=z[19]*z[63];
    z[65]=z[3] + z[8];
    z[65]=z[28]*z[65];
    z[66]=z[25]*i;

    r +=  - n<T>(4,33)*z[27] + n<T>(7,132)*z[32] + n<T>(28,297)*z[34] + n<T>(7,1188)*
      z[35] + n<T>(1,44)*z[36] + n<T>(25,297)*z[39] + n<T>(2,27)*z[40] + n<T>(8,33)*
      z[41] + n<T>(5,594)*z[47] - n<T>(4,297)*z[49] - n<T>(7,594)*z[50] - n<T>(1,198)*
      z[51] + n<T>(1,33)*z[53] + z[54] + n<T>(1,99)*z[55] + n<T>(2,99)*z[56] + n<T>(8,99)*z[57]
     + n<T>(19,297)*z[58]
     + n<T>(2,297)*z[59]
     + n<T>(14,297)*z[60]
     + n<T>(1,27)*z[61]
     + n<T>(1,66)*z[62]
     + n<T>(2,33)*z[63]
     + n<T>(1,297)*z[64]
     + n<T>(5,297)
      *z[65] + n<T>(19,1188)*z[66];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf186(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf186(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
