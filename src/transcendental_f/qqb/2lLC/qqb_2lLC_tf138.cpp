#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf138(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[36];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[18];
    z[9]=e[2];
    z[10]=e[11];
    z[11]=e[14];
    z[12]=c[3];
    z[13]=d[9];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[31];
    z[17]=e[3];
    z[18]=e[10];
    z[19]=f[2];
    z[20]=f[4];
    z[21]=f[6];
    z[22]=f[12];
    z[23]=f[16];
    z[24]=i*z[1];
    z[25]=2*z[3];
    z[26]=2*z[4];
    z[27]=z[24] + n<T>(5,2)*z[6] + z[26] + z[25] + z[5];
    z[27]=n<T>(1,3)*z[27] - n<T>(5,2)*z[2];
    z[27]=z[2]*z[27];
    z[28]=npow(z[4],2);
    z[29]= - z[4] + z[3];
    z[29]=z[3]*z[29];
    z[29]=z[28] + z[29];
    z[30]=7*z[18];
    z[31]=npow(z[5],2);
    z[32]=z[31] - z[30] - 4*z[17];
    z[25]=z[25] - z[26];
    z[33]=2*z[7];
    z[34]=z[6] - z[33] - z[5] - z[25];
    z[24]=z[34]*z[24];
    z[34]=n<T>(4,3)*z[9];
    z[35]= - n<T>(2,3)*z[5] + n<T>(3,2)*z[6];
    z[35]=z[6]*z[35];
    z[24]=z[27] + n<T>(2,3)*z[24] + z[35] - n<T>(5,18)*z[12] - z[34] - z[11] + n<T>(1,3)*z[32]
     + n<T>(4,3)*z[29];
    z[24]=z[2]*z[24];
    z[27]=n<T>(1,2)*z[5];
    z[29]=z[8] + z[27];
    z[29]=z[5]*z[29];
    z[29]=z[28] + z[29];
    z[25]=n<T>(1,2)*z[6] + n<T>(1,3)*z[8] + z[25];
    z[25]=z[6]*z[25];
    z[32]= - z[11] + n<T>(1,3)*z[10];
    z[33]= - z[33] + z[4];
    z[33]=z[3]*z[33];
    z[25]=z[25] + n<T>(2,3)*z[33] + z[34] + z[32] + n<T>(1,3)*z[29];
    z[25]=z[1]*z[25];
    z[25]= - n<T>(1,18)*z[14] + z[25];
    z[25]=i*z[25];
    z[29]= - n<T>(1,2)*z[31] + n<T>(14,3)*z[12] - z[30] - z[10];
    z[26]=z[26] - z[3];
    z[26]=z[3]*z[26];
    z[27]= - z[3] + z[27] - z[4];
    z[27]=z[6]*z[27];
    z[26]=z[27] + z[26] + n<T>(1,3)*z[29] - z[28];
    z[26]=z[6]*z[26];
    z[27]=n<T>(1,3)*z[12];
    z[29]= - z[3]*z[4];
    z[28]=z[29] - z[28] - 4*z[9] - z[27];
    z[28]=z[3]*z[28];
    z[28]=z[22] + z[28] - z[21];
    z[29]= - 4*z[7] - n<T>(1,2)*z[13] + z[8];
    z[29]=z[29]*z[27];
    z[30]= - n<T>(1,6)*z[31] - z[32];
    z[30]=z[5]*z[30];
    z[27]= - z[17] + z[27];
    z[27]=z[4]*z[27];
    z[27]=z[27] - z[19];

    r += z[15] - n<T>(11,2)*z[16] - z[20] - z[23] + z[24] + z[25] + z[26]
       + n<T>(4,3)*z[27] + n<T>(1,3)*z[28] + z[29] + z[30];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf138(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf138(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
