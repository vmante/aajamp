#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf91(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[43];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[15];
    z[6]=d[2];
    z[7]=d[5];
    z[8]=d[8];
    z[9]=d[11];
    z[10]=d[9];
    z[11]=e[2];
    z[12]=e[12];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=d[7];
    z[16]=e[3];
    z[17]=e[10];
    z[18]=e[13];
    z[19]=e[14];
    z[20]=f[2];
    z[21]=f[4];
    z[22]=f[6];
    z[23]=f[12];
    z[24]=f[16];
    z[25]=f[51];
    z[26]=f[55];
    z[27]=f[56];
    z[28]=f[58];
    z[29]=z[3] + z[2];
    z[30]=z[1]*i;
    z[31]=z[30]*z[29];
    z[31]=z[13] + z[31];
    z[31]=z[5]*z[31];
    z[32]=z[10] + z[6];
    z[32]= - z[30]*z[32];
    z[32]= - z[13] + z[32];
    z[32]=z[9]*z[32];
    z[29]= - z[30] + z[29];
    z[29]=z[11]*z[29];
    z[33]=z[30] - z[6];
    z[34]=z[10] - z[33];
    z[34]=z[12]*z[34];
    z[35]=z[2] + z[4];
    z[35]=z[16]*z[35];
    z[36]=z[7] + z[10];
    z[36]=z[18]*z[36];
    z[29]=z[29] + z[34] + z[35] + z[36] + z[20] + z[31] + z[32];
    z[31]=n<T>(1,3)*z[30];
    z[32]=z[31] + z[8];
    z[34]=n<T>(2,3)*z[2];
    z[35]=n<T>(1,6)*z[3];
    z[36]=z[35] + z[34] - z[32];
    z[36]=z[3]*z[36];
    z[37]=z[31] - z[8];
    z[37]=z[35] - n<T>(1,2)*z[37] - z[34];
    z[37]=z[4]*z[37];
    z[38]=n<T>(1,2)*z[8];
    z[39]=z[38] + z[30];
    z[39]=z[39]*z[8];
    z[40]=n<T>(1,3)*z[2];
    z[41]=2*z[30];
    z[42]= - z[41] - z[2];
    z[42]=z[42]*z[40];
    z[36]=z[37] + z[36] + z[39] + z[42];
    z[36]=z[4]*z[36];
    z[37]=z[38] - z[30];
    z[37]=z[37]*z[8];
    z[42]=z[41] - z[2];
    z[42]=z[42]*z[40];
    z[34]=z[38] - z[34];
    z[34]=z[3]*z[34];
    z[34]=z[34] + z[37] + z[42];
    z[34]=z[3]*z[34];
    z[31]=z[31] - n<T>(1,3)*z[6];
    z[38]= - z[38] - z[31];
    z[38]=z[6]*z[38];
    z[37]= - z[37] + z[38];
    z[37]=z[6]*z[37];
    z[32]= - n<T>(1,6)*z[6] + z[32];
    z[32]=z[6]*z[32];
    z[31]= - z[8] + z[31];
    z[31]=n<T>(1,2)*z[31] + n<T>(1,3)*z[7];
    z[31]=z[7]*z[31];
    z[31]=z[31] - z[39] + z[32];
    z[31]=z[7]*z[31];
    z[32]=z[8] + z[2];
    z[32]=z[17]*z[32];
    z[38]=z[8] + z[10];
    z[38]=z[19]*z[38];
    z[32]=z[32] + z[38];
    z[38]=z[41] + 5*z[2];
    z[38]=z[38]*z[40];
    z[39]=npow(z[8],2);
    z[38]= - z[39] + z[38];
    z[38]=z[2]*z[38];
    z[39]=npow(z[6],2);
    z[33]=2*z[33] + z[7];
    z[33]=z[7]*z[33];
    z[33]=z[39] + z[33];
    z[30]= - z[30] - z[8];
    z[30]=z[10]*z[30];
    z[30]=n<T>(1,3)*z[33] + z[30];
    z[30]=z[10]*z[30];
    z[33]=z[7] - z[4];
    z[33]= - n<T>(4,3)*z[10] - n<T>(1,2)*z[6] + z[2] + z[35] + n<T>(2,3)*z[33];
    z[33]=z[13]*z[33];
    z[35]=npow(z[2],2);
    z[39]=npow(z[10],2);
    z[35]= - z[35] + z[39];
    z[35]=z[15]*z[35];
    z[39]= - z[28] + z[26] + z[24] + z[21];
    z[40]=z[25] + z[23];
    z[41]=npow(z[8],3);
    z[42]=z[14]*i;

    r += z[22] + z[27] + n<T>(2,3)*z[29] + z[30] + z[31] + 2*z[32] + n<T>(1,3)*
      z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + n<T>(1,2)*z[39] - n<T>(1,6)*z[40]
     + z[41]
     + n<T>(1,9)*z[42];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf91(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf91(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
