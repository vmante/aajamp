#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf233(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[51];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[11];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[8];
    z[14]=e[12];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[17];
    z[19]=c[19];
    z[20]=c[23];
    z[21]=c[25];
    z[22]=c[26];
    z[23]=c[28];
    z[24]=c[30];
    z[25]=c[31];
    z[26]=e[13];
    z[27]=e[14];
    z[28]=f[0];
    z[29]=f[3];
    z[30]=f[5];
    z[31]=f[11];
    z[32]=f[17];
    z[33]=f[50];
    z[34]=f[51];
    z[35]=f[54];
    z[36]=f[55];
    z[37]=f[58];
    z[38]=f[59];
    z[39]=z[1]*i;
    z[40]= - z[2] + 4*z[39];
    z[41]=n<T>(2,3)*z[2];
    z[40]=z[40]*z[41];
    z[42]=2*z[39];
    z[43]=z[42]*z[4];
    z[44]=npow(z[4],2);
    z[43]=z[43] - z[44];
    z[40]=z[40] - z[43];
    z[41]=z[41] - z[4] - n<T>(1,3)*z[39];
    z[45]=n<T>(1,3)*z[5];
    z[41]=2*z[41] + z[45];
    z[41]=z[5]*z[41];
    z[46]= - z[4] + n<T>(4,3)*z[2];
    z[45]=z[45] - z[46];
    z[45]=z[7]*z[45];
    z[41]=z[45] + z[41] + z[40];
    z[41]=z[7]*z[41];
    z[45]= - z[5]*z[46];
    z[40]=z[45] + z[40];
    z[40]=z[5]*z[40];
    z[45]= - 17*z[39] + 7*z[2];
    z[45]=z[2]*z[45];
    z[43]=2*z[43] + n<T>(1,3)*z[45];
    z[43]=z[2]*z[43];
    z[45]=z[39]*z[4];
    z[46]=4*z[45];
    z[47]=5*z[44];
    z[48]= - z[47] - z[46];
    z[49]=10*z[4] - z[39];
    z[49]= - n<T>(1,27)*z[10] + n<T>(1,9)*z[49] - z[2];
    z[49]=z[10]*z[49];
    z[48]=n<T>(1,9)*z[48] + z[49];
    z[48]=z[10]*z[48];
    z[49]=npow(z[4],3);
    z[40]=z[29] + z[40] + z[48] - n<T>(8,27)*z[49] + z[43] + z[41] + z[32];
    z[41]=z[3] + z[6];
    z[43]=z[41] + n<T>(8,3)*z[9];
    z[48]=z[39]*z[43];
    z[49]=7*z[10];
    z[50]=z[39]*z[49];
    z[46]=z[46] + z[50];
    z[46]=n<T>(16,9)*z[15] + n<T>(4,9)*z[46] + z[48];
    z[46]=z[8]*z[46];
    z[42]=z[42] - z[2];
    z[48]=z[4] - z[42];
    z[48]=2*z[48] + z[10];
    z[41]=4*z[48] + z[41];
    z[41]=z[11]*z[41];
    z[48]=z[39] - z[4];
    z[50]=4*z[48];
    z[49]=z[50] - z[49];
    z[43]=n<T>(4,9)*z[49] - z[43];
    z[43]=z[14]*z[43];
    z[41]=z[43] + z[46] + z[41];
    z[43]= - z[47] - 8*z[45];
    z[46]=2*z[10];
    z[39]=17*z[4] - 8*z[39];
    z[39]=n<T>(1,9)*z[39] - z[2];
    z[39]=z[39]*z[46];
    z[47]= - 2*z[6] - z[46] - z[48];
    z[47]=z[6]*z[47];
    z[49]=z[42]*z[2];
    z[39]=n<T>(4,9)*z[47] + z[39] + n<T>(1,9)*z[43] - z[49];
    z[39]=z[6]*z[39];
    z[43]= - z[50] - z[10];
    z[43]=z[43]*z[46];
    z[43]=z[43] - z[44] - 10*z[45];
    z[45]= - n<T>(2,9)*z[9] + z[6] - n<T>(5,3)*z[48] - z[46];
    z[45]=z[9]*z[45];
    z[47]=2*z[48] + z[6];
    z[47]=z[6]*z[47];
    z[43]=z[45] + n<T>(1,3)*z[43] + z[47];
    z[43]=z[9]*z[43];
    z[43]=z[31] + z[43] + z[37];
    z[45]=z[4] - z[2];
    z[45]=z[45]*z[46];
    z[44]=z[45] - z[44] - z[49];
    z[44]=z[3]*z[44];
    z[45]= - z[10] - z[6];
    z[45]=z[26]*z[45];
    z[46]= - z[10] - z[9];
    z[46]=z[27]*z[46];
    z[45]=z[33] + z[45] + z[46] + z[38] - z[35];
    z[46]=z[5] - z[42];
    z[46]=z[12]*z[46];
    z[42]=z[7] - z[42];
    z[42]=z[13]*z[42];
    z[42]=z[30] + z[46] + z[42];
    z[46]= - z[7] - z[5] - z[9] - n<T>(4,3)*z[6] + z[10] - n<T>(11,3)*z[4] - 8*
    z[2];
    z[46]=z[15]*z[46];
    z[46]=z[34] + z[46] - z[36];
    z[47]=z[24] + z[21];
    z[48]=n<T>(160,3)*z[22] + n<T>(56,3)*z[20] - n<T>(8,27)*z[16];
    z[48]=i*z[48];
    z[49]=4*i;
    z[49]=z[18]*z[49];
    z[39]=16*z[28] + 7*z[25] - n<T>(80,3)*z[23] + n<T>(8,9)*z[19] + z[49] - n<T>(14,9)*z[17]
     + z[44]
     + z[39]
     + z[48] - 8*z[47]
     + n<T>(16,3)*z[42]
     + 2*z[41]
    + n<T>(4,9)*z[46] + n<T>(4,3)*z[43] + 4*z[40] + n<T>(16,9)*z[45];

    r += n<T>(2,3)*z[39];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf233(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf233(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
