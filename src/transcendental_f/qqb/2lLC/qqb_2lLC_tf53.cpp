#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf53(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[6];
    z[3]=d[12];
    z[4]=d[1];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[8];
    z[8]=e[5];
    z[9]=npow(z[4],2);
    z[10]=npow(z[6],2);
    z[11]=npow(z[7],2);
    z[9]=z[11] + z[9] - z[10];
    z[10]= - z[3] + z[2];
    z[10]=z[10]*i*z[1];
    z[11]= - z[2] + n<T>(1,2)*z[5];
    z[11]=z[5]*z[11];
    z[9]=z[8] + z[11] + z[10] + n<T>(1,2)*z[9];

    r += n<T>(1,18)*z[9];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf53(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf53(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
