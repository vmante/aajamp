#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf228(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[68];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[2];
    z[11]=d[11];
    z[12]=d[9];
    z[13]=d[17];
    z[14]=d[18];
    z[15]=e[2];
    z[16]=e[3];
    z[17]=e[7];
    z[18]=e[9];
    z[19]=e[11];
    z[20]=e[12];
    z[21]=c[3];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[31];
    z[25]=e[10];
    z[26]=e[13];
    z[27]=e[14];
    z[28]=f[2];
    z[29]=f[4];
    z[30]=f[6];
    z[31]=f[10];
    z[32]=f[12];
    z[33]=f[15];
    z[34]=f[16];
    z[35]=f[31];
    z[36]=f[33];
    z[37]=f[36];
    z[38]=f[39];
    z[39]=f[51];
    z[40]=f[53];
    z[41]=f[55];
    z[42]=f[58];
    z[43]=f[72];
    z[44]=n<T>(1,2)*z[2];
    z[45]=z[5] - z[44];
    z[45]=z[45]*z[44];
    z[46]=n<T>(1,4)*z[6];
    z[47]=z[1]*i;
    z[48]=n<T>(1,2)*z[47];
    z[49]=z[46] - z[48] - z[5];
    z[49]=z[6]*z[49];
    z[50]=n<T>(3,2)*z[8];
    z[51]= - z[47] - z[50];
    z[52]=n<T>(1,2)*z[8];
    z[51]=z[51]*z[52];
    z[53]=n<T>(1,2)*z[3];
    z[54]=z[6] - z[5];
    z[55]=z[54] + z[8];
    z[56]= - z[2] + z[55];
    z[56]=z[56]*z[53];
    z[57]= - n<T>(1,4)*z[47] + z[5];
    z[57]=z[5]*z[57];
    z[58]= - z[5] + z[8];
    z[58]=z[4]*z[58];
    z[59]=n<T>(1,2)*z[5];
    z[60]= - n<T>(3,2)*z[12] + z[59] - z[2] + z[8] + z[6];
    z[60]=z[12]*z[60];
    z[45]=n<T>(1,4)*z[60] + z[58] + z[56] + z[51] + z[49] + z[57] + z[45];
    z[45]=z[12]*z[45];
    z[49]=n<T>(1,4)*z[5];
    z[51]= - 15*z[47] - n<T>(7,2)*z[5];
    z[51]=z[51]*z[49];
    z[56]=7*z[2];
    z[57]= - 5*z[47] - z[56];
    z[58]=n<T>(1,3)*z[2];
    z[57]=z[57]*z[58];
    z[51]=z[51] + z[57];
    z[57]=n<T>(5,6)*z[2];
    z[55]=n<T>(1,12)*z[3] + z[57] - n<T>(1,6)*z[47] - z[55];
    z[55]=z[3]*z[55];
    z[60]=n<T>(7,3)*z[2];
    z[61]= - n<T>(13,3)*z[47] - 7*z[5];
    z[61]=z[6] + n<T>(1,4)*z[61] - z[60];
    z[62]=n<T>(1,6)*z[3];
    z[63]=n<T>(1,2)*z[4];
    z[61]=z[63] + z[62] + n<T>(1,2)*z[61] + z[8];
    z[61]=z[61]*z[63];
    z[46]=z[46] + z[47] + n<T>(3,2)*z[2];
    z[46]=z[6]*z[46];
    z[63]=z[52] + z[47] - z[6];
    z[63]=z[8]*z[63];
    z[46]=z[61] + z[55] + z[63] + n<T>(1,2)*z[51] + z[46];
    z[46]=z[4]*z[46];
    z[51]=n<T>(1,6)*z[8];
    z[55]=z[62] + z[51] - z[58] - z[48] - z[54];
    z[53]=z[55]*z[53];
    z[55]= - z[57] + n<T>(2,3)*z[47] - z[59];
    z[55]=z[2]*z[55];
    z[57]=n<T>(5,3)*z[2];
    z[51]=z[51] + z[57] - n<T>(1,3)*z[47] - z[5];
    z[51]=z[51]*z[52];
    z[52]=z[59] + z[47];
    z[58]=n<T>(1,2)*z[6];
    z[61]= - z[58] + z[2] + z[52];
    z[61]=z[6]*z[61];
    z[62]= - z[5]*z[47];
    z[51]=z[53] + z[51] + z[61] + z[62] + z[55];
    z[51]=z[3]*z[51];
    z[53]=z[3] + z[2];
    z[53]= - z[47]*z[53];
    z[53]= - z[21] + z[53];
    z[53]=z[9]*z[53];
    z[55]=z[47]*z[7];
    z[55]=z[55] + z[21];
    z[61]= - z[8]*z[47];
    z[61]=z[61] - z[55];
    z[61]=z[14]*z[61];
    z[62]=z[47] - z[3];
    z[63]= - z[2] + z[62];
    z[63]=z[15]*z[63];
    z[64]=z[47] - z[7];
    z[65]=z[8] - z[64];
    z[65]=z[19]*z[65];
    z[53]=z[63] + z[53] + z[61] + z[65] - z[31] + z[30] + z[29];
    z[61]=z[8] + z[2];
    z[48]=z[48] - z[61];
    z[48]=z[2]*z[48];
    z[63]=z[47] + z[5];
    z[65]=z[58] - z[63];
    z[65]=z[6]*z[65];
    z[66]=7*z[47] - n<T>(37,2)*z[5];
    z[66]=z[5]*z[66];
    z[48]=n<T>(7,4)*z[65] + n<T>(1,8)*z[66] + z[48];
    z[65]=n<T>(1,2)*z[12];
    z[66]=z[47] + z[2];
    z[66]=z[66]*z[65];
    z[44]=z[7] + n<T>(7,2)*z[6] - z[44] - n<T>(7,8)*z[5] - z[47] + z[8];
    z[44]= - z[65] - n<T>(7,8)*z[4] + n<T>(1,3)*z[44];
    z[44]=z[7]*z[44];
    z[67]=7*z[63] + n<T>(3,2)*z[4];
    z[67]=z[4]*z[67];
    z[44]=n<T>(1,2)*z[44] + z[66] + n<T>(1,3)*z[48] + n<T>(1,8)*z[67];
    z[44]=z[7]*z[44];
    z[48]=z[10] + z[12];
    z[66]=z[47]*z[48];
    z[66]=z[21] + z[66];
    z[66]=z[11]*z[66];
    z[67]=z[47] - z[48];
    z[67]=z[20]*z[67];
    z[66]=z[66] + z[67];
    z[48]=z[50] + z[52] - z[48];
    z[48]=z[10]*z[48];
    z[50]= - 3*z[47] - n<T>(11,2)*z[5];
    z[50]=z[5]*z[50];
    z[67]= - 9*z[63] + n<T>(23,2)*z[8];
    z[67]=z[8]*z[67];
    z[50]=z[50] + z[67];
    z[67]=z[12]*z[5];
    z[48]=n<T>(3,2)*z[48] + n<T>(1,2)*z[50] + 3*z[67];
    z[48]=z[10]*z[48];
    z[50]= - z[59] + z[56];
    z[50]=n<T>(3,4)*z[10] - n<T>(19,36)*z[7] - z[65] + n<T>(17,36)*z[4] - n<T>(31,36)*
    z[3] - n<T>(47,12)*z[8] + n<T>(1,9)*z[50] + n<T>(9,2)*z[6];
    z[50]=z[21]*z[50];
    z[48]=z[48] + z[50];
    z[50]=z[52]*z[59];
    z[52]=z[57] + n<T>(3,2)*z[47] + z[5];
    z[52]=z[2]*z[52];
    z[50]=z[50] + z[52];
    z[50]=z[2]*z[50];
    z[52]=5*z[5];
    z[56]=n<T>(19,3)*z[47] + z[52];
    z[56]=n<T>(1,2)*z[56] - z[2];
    z[56]=n<T>(1,2)*z[56] - n<T>(5,3)*z[6];
    z[56]=z[56]*z[58];
    z[57]=z[47] + 11*z[5];
    z[57]=z[5]*z[57];
    z[58]= - n<T>(3,4)*z[2] - z[63];
    z[58]=z[2]*z[58];
    z[56]=z[56] + n<T>(1,12)*z[57] + z[58];
    z[56]=z[6]*z[56];
    z[57]=13*z[47] + n<T>(9,2)*z[5];
    z[49]=z[57]*z[49];
    z[57]= - z[2]*z[63];
    z[49]=z[49] + z[57];
    z[52]= - n<T>(73,3)*z[47] + z[52];
    z[52]= - n<T>(19,3)*z[8] + n<T>(1,4)*z[52] - z[60];
    z[52]=z[8]*z[52];
    z[57]=z[6]*z[5];
    z[49]=n<T>(1,4)*z[52] + n<T>(1,2)*z[49] + z[57];
    z[49]=z[8]*z[49];
    z[52]= - z[6]*z[47];
    z[52]=z[52] - z[55];
    z[52]=z[13]*z[52];
    z[55]= - z[6] + z[64];
    z[55]=z[18]*z[55];
    z[52]=z[52] + z[55];
    z[54]= - n<T>(7,6)*z[4] - n<T>(1,6)*z[2] - z[54] + z[62];
    z[54]=z[16]*z[54];
    z[55]=z[23] + z[43] + z[34];
    z[57]=z[38] + z[37];
    z[58]=z[40] + z[36];
    z[47]=n<T>(7,2)*z[47] - z[5];
    z[47]=z[47]*npow(z[5],2);
    z[59]=z[62] + z[12];
    z[59]=n<T>(1,3)*z[6] - n<T>(5,12)*z[5] + 2*z[59];
    z[59]=z[17]*z[59];
    z[60]=z[25]*z[61];
    z[61]= - n<T>(7,4)*z[5] - z[12];
    z[61]=z[26]*z[61];
    z[62]= - z[8] - z[12];
    z[62]=z[27]*z[62];
    z[63]=z[22]*i;

    r += n<T>(7,2)*z[24] + n<T>(5,6)*z[28] + n<T>(2,3)*z[32] + 4*z[33] - n<T>(37,48)*
      z[35] - n<T>(11,16)*z[39] - n<T>(23,16)*z[41] + n<T>(9,16)*z[42] + z[44] + 
      z[45] + z[46] + n<T>(5,12)*z[47] + n<T>(1,4)*z[48] + z[49] + z[50] + 
      z[51] + n<T>(7,12)*z[52] + n<T>(1,3)*z[53] + z[54] + n<T>(1,2)*z[55] + z[56]
       - n<T>(3,16)*z[57] + n<T>(3,2)*z[58] + z[59] + n<T>(7,6)*z[60] + z[61] + 2*
      z[62] - n<T>(1,9)*z[63] + n<T>(3,4)*z[66];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf228(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf228(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
