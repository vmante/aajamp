#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf260(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,82>& f,
  const std::array<std::complex<T>,330>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[60];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[79];
    z[3]=c[11];
    z[4]=d[1];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=c[12];
    z[10]=c[13];
    z[11]=c[20];
    z[12]=c[23];
    z[13]=c[32];
    z[14]=c[33];
    z[15]=c[36];
    z[16]=c[38];
    z[17]=c[40];
    z[18]=c[43];
    z[19]=d[0];
    z[20]=f[24];
    z[21]=f[44];
    z[22]=f[69];
    z[23]=f[80];
    z[24]=f[22];
    z[25]=f[42];
    z[26]=f[70];
    z[27]=d[2];
    z[28]=g[24];
    z[29]=g[27];
    z[30]=g[38];
    z[31]=g[40];
    z[32]=g[48];
    z[33]=g[50];
    z[34]=g[79];
    z[35]=g[82];
    z[36]=g[90];
    z[37]=g[92];
    z[38]=g[99];
    z[39]=g[101];
    z[40]=g[170];
    z[41]=g[173];
    z[42]=g[183];
    z[43]=g[189];
    z[44]=g[243];
    z[45]=g[254];
    z[46]=g[270];
    z[47]=g[272];
    z[48]=g[276];
    z[49]=g[278];
    z[50]=g[281];
    z[51]=g[308];
    z[52]=z[5] - z[6];
    z[53]=z[8] - z[52];
    z[54]= - z[11] + n<T>(1,20)*z[12];
    z[54]= - n<T>(6,5)*z[10] + n<T>(1,2)*z[54];
    z[55]=n<T>(31,1080)*z[3] + z[54];
    z[53]=z[55]*z[53];
    z[55]= - 27*z[16] + n<T>(7,2)*z[14];
    z[56]=z[3]*z[7];
    z[57]= - z[2]*z[1];
    z[54]=n<T>(49,1080)*z[3] - z[54];
    z[54]=z[4]*z[54];
    z[53]=z[54] + z[57] + n<T>(1,2)*z[55] - n<T>(2,27)*z[56] + z[53];
    z[53]=i*z[53];
    z[54]=z[20] + z[23];
    z[55]=z[24] - z[25];
    z[55]=z[26] - n<T>(1,2)*z[55];
    z[54]= - z[55] + n<T>(1,2)*z[54];
    z[56]=n<T>(1,2)*z[2];
    z[57]=z[56] + n<T>(1,3)*z[9];
    z[54]= - z[57] + n<T>(1,2)*z[54];
    z[52]= - z[54]*z[52];
    z[54]=z[19] - z[27];
    z[54]=n<T>(1,4)*z[54];
    z[58]= - z[20] - z[21] + z[22] + z[23];
    z[54]=z[58]*z[54];
    z[58]=z[21] + z[22];
    z[55]=z[55] + n<T>(1,2)*z[58];
    z[55]=n<T>(1,2)*z[55];
    z[57]= - z[55] - z[57];
    z[57]=z[8]*z[57];
    z[55]= - z[56] + z[55] - n<T>(5,3)*z[9];
    z[55]=z[4]*z[55];
    z[56]= - z[40] - z[42] - z[46] - z[49] + z[34] + z[35] + z[36] + 
    z[37] + z[28] + z[29] + z[30] + z[31];
    z[58]= - z[32] + z[33] - z[38] + z[39] + z[43] + z[50];
    z[59]=z[2] + 2*z[9];
    z[59]=z[7]*z[59];

    r += n<T>(5,18)*z[13] - 6*z[15] + 5*z[17] + n<T>(3,2)*z[18] - z[41] - 2*
      z[44] + n<T>(1,3)*z[45] + z[47] - z[48] + z[51] + z[52] + z[53] + 
      z[54] + z[55] + n<T>(1,2)*z[56] + z[57] + n<T>(1,6)*z[58] + z[59];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf260(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,82>& f,
  const std::array<std::complex<double>,330>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf260(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,82>& f,
  const std::array<std::complex<dd_real>,330>& g
);
#endif
