#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf163(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[23];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[8];
    z[4]=d[11];
    z[5]=d[5];
    z[6]=d[9];
    z[7]=e[12];
    z[8]=e[14];
    z[9]=c[3];
    z[10]=c[15];
    z[11]=c[31];
    z[12]=e[13];
    z[13]=f[51];
    z[14]=z[1]*i;
    z[15]= - z[4] + 2*z[3];
    z[15]=z[15]*z[14];
    z[16]=z[14] - z[2];
    z[17]=n<T>(1,2)*z[6];
    z[16]=z[17] - z[3] - n<T>(1,2)*z[16];
    z[16]=z[2]*z[16];
    z[18]=npow(z[3],2);
    z[18]=z[8] + n<T>(1,2)*z[18];
    z[19]=z[7] + z[18];
    z[20]=n<T>(1,6)*z[9];
    z[21]=z[3] - z[6];
    z[22]= - z[5] + z[21];
    z[22]=z[5]*z[22];
    z[15]=z[16] + z[22] + z[15] - z[20] + z[19];
    z[15]=z[2]*z[15];
    z[16]= - z[21]*z[14];
    z[17]=n<T>(1,2)*z[5] + z[17] + z[14];
    z[17]=z[5]*z[17];
    z[16]=z[17] + z[16] + z[12] - z[18];
    z[16]=z[5]*z[16];
    z[17]= - z[6]*z[4];
    z[17]=z[17] - z[19];
    z[14]=z[17]*z[14];
    z[17]=z[3] - z[4];
    z[17]=z[9]*z[17];
    z[18]= - z[20] + z[12] + z[7];
    z[18]=z[6]*z[18];

    r += z[10] - n<T>(3,2)*z[11] - z[13] + z[14] + z[15] + z[16] + z[17] + 
      z[18];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf163(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf163(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
