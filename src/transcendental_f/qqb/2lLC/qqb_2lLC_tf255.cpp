#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf255(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,82>& f,
  const std::array<std::complex<T>,330>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[10];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[4];
    z[3]=d[6];
    z[4]=c[12];
    z[5]=f[69];
    z[6]=g[170];
    z[7]=g[183];
    z[8]=i*z[1];
    z[8]=n<T>(1,27)*z[8] - z[4] - n<T>(1,2)*z[5];
    z[9]=z[3] - z[2];
    z[8]=z[9]*z[8];
    z[9]=z[7] + z[6];

    r += z[8] - n<T>(1,2)*z[9];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf255(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,82>& f,
  const std::array<std::complex<double>,330>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf255(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,82>& f,
  const std::array<std::complex<dd_real>,330>& g
);
#endif
