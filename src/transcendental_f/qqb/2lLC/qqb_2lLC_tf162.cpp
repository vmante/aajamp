#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf162(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[11];
  std::complex<T> r(0,0);

    z[1]=c[3];
    z[2]=d[9];
    z[3]=c[31];
    z[4]=d[5];
    z[5]=d[8];
    z[6]=e[13];
    z[7]=e[14];
    z[8]=f[58];
    z[9]=z[5] - n<T>(1,2)*z[4];
    z[9]=z[4]*z[9];
    z[9]=z[9] - z[7] - z[6];
    z[10]=npow(z[5],2);
    z[9]= - n<T>(3,2)*z[10] - n<T>(1,2)*z[1] + 3*z[9];
    z[9]=z[2]*z[9];
    z[10]=n<T>(1,2)*z[8] - z[3];
    z[9]=3*z[10] + z[9];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf162(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf162(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
