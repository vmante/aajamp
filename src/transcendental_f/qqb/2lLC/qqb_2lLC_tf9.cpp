#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf9(
  const std::array<std::complex<T>,19>& d
) {
    return d[9];
}

template std::complex<double> qqb_2lLC_tf9(
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf9(
  const std::array<std::complex<dd_real>,19>& d
);
#endif
