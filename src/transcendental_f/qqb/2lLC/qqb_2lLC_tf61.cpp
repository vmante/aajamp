#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf61(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[7];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[11];
    z[3]=d[16];
    z[4]=e[4];
    z[5]=e[12];
    z[6]=z[2] - z[3];
    z[6]=z[6]*i*z[1];

    r += z[4] - z[5] + z[6];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf61(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf61(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
