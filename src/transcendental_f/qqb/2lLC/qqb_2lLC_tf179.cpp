#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf179(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[71];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[12];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[31];
    z[28]=e[3];
    z[29]=e[6];
    z[30]=e[13];
    z[31]=e[14];
    z[32]=f[4];
    z[33]=f[6];
    z[34]=f[7];
    z[35]=f[12];
    z[36]=f[16];
    z[37]=f[23];
    z[38]=f[30];
    z[39]=f[31];
    z[40]=f[32];
    z[41]=f[33];
    z[42]=f[35];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=f[50];
    z[47]=f[51];
    z[48]=f[52];
    z[49]=f[54];
    z[50]=f[55];
    z[51]=f[56];
    z[52]=f[58];
    z[53]=25*z[6];
    z[54]=64*z[4];
    z[55]=z[53] + z[54];
    z[56]=n<T>(1,3)*z[7];
    z[57]=z[55]*z[56];
    z[58]=25*z[9];
    z[59]=z[1]*i;
    z[60]= - z[59] + n<T>(1,3)*z[6];
    z[60]=z[58] + 25*z[60] + n<T>(233,3)*z[4];
    z[61]=n<T>(1,2)*z[9];
    z[60]=z[60]*z[61];
    z[62]=2*z[59];
    z[63]=z[62] - z[6];
    z[63]=z[6]*z[63];
    z[64]= - n<T>(233,3)*z[59] + n<T>(35,2)*z[4];
    z[64]=z[4]*z[64];
    z[57]=z[60] + z[57] + n<T>(25,3)*z[63] + z[64];
    z[57]=z[9]*z[57];
    z[60]=n<T>(1,2)*z[6];
    z[63]=z[59] + z[60];
    z[63]=z[6]*z[63];
    z[64]=23*z[59] + n<T>(47,2)*z[4];
    z[64]=z[4]*z[64];
    z[65]= - n<T>(13,6)*z[9] + n<T>(13,3)*z[59] - 23*z[4];
    z[65]=z[9]*z[65];
    z[63]=z[65] + n<T>(113,4)*z[63] + z[64];
    z[64]=n<T>(7,6)*z[7] - n<T>(7,3)*z[59] - n<T>(113,11)*z[6];
    z[64]=z[7]*z[64];
    z[63]=n<T>(1,4)*z[64] + n<T>(1,11)*z[63];
    z[64]=z[59] - z[7];
    z[65]= - n<T>(95,36)*z[3] - 4*z[4] - n<T>(149,18)*z[64];
    z[65]=z[3]*z[65];
    z[66]=z[59] - z[9];
    z[67]=z[5]*z[66];
    z[68]=n<T>(155,3)*z[59] + 113*z[6];
    z[68]= - n<T>(59,3)*z[3] - n<T>(13,3)*z[9] - n<T>(103,12)*z[7] + n<T>(1,4)*z[68]
     + 
   35*z[4];
    z[68]= - n<T>(23,3)*z[8] + n<T>(1,2)*z[68] - n<T>(41,3)*z[5];
    z[68]=z[8]*z[68];
    z[63]=n<T>(1,33)*z[68] - n<T>(82,99)*z[67] + n<T>(1,3)*z[63] + n<T>(1,11)*z[65];
    z[63]=z[8]*z[63];
    z[65]=z[59] - z[11];
    z[67]=n<T>(5,66)*z[4];
    z[68]=n<T>(1,3)*z[14];
    z[65]= - z[68] + z[67] + n<T>(13,22)*z[6] + n<T>(1,3)*z[65];
    z[65]=z[11]*z[65];
    z[69]= - z[59] + z[60];
    z[69]=z[6]*z[69];
    z[67]=z[67] - n<T>(5,33)*z[59] - z[6];
    z[67]=z[4]*z[67];
    z[65]=z[65] + n<T>(13,11)*z[69] + z[67];
    z[60]= - z[60] + n<T>(7,3)*z[4];
    z[60]=z[14]*z[60];
    z[60]=n<T>(1,11)*z[60] + n<T>(1,4)*z[65];
    z[60]=z[11]*z[60];
    z[65]= - 571*z[59] + n<T>(703,2)*z[6];
    z[65]=z[6]*z[65];
    z[67]=n<T>(128,3)*z[59] - n<T>(23,2)*z[6];
    z[67]=n<T>(1,11)*z[67] - n<T>(10,3)*z[4];
    z[67]=z[4]*z[67];
    z[69]= - n<T>(1,3)*z[4] + n<T>(571,6)*z[6] - 85*z[64];
    z[69]=z[7]*z[69];
    z[65]=n<T>(1,44)*z[69] + n<T>(1,132)*z[65] + z[67];
    z[56]=z[65]*z[56];
    z[65]=npow(z[4],2);
    z[67]= - n<T>(127,2)*z[7] + 127*z[59] + 100*z[4];
    z[67]=z[7]*z[67];
    z[69]= - n<T>(197,3)*z[5] - 23*z[9] - 68*z[7] - 19*z[59] + 178*z[4];
    z[69]=z[5]*z[69];
    z[65]=z[69] + n<T>(247,2)*z[65] + z[67];
    z[67]=2*z[4];
    z[69]= - n<T>(32,9)*z[7] + n<T>(23,9)*z[59] - z[67];
    z[69]=n<T>(1,11)*z[69] - n<T>(5,9)*z[9];
    z[69]=z[9]*z[69];
    z[65]=2*z[69] + n<T>(1,99)*z[65];
    z[65]=z[5]*z[65];
    z[69]=79*z[59] + n<T>(59,2)*z[6];
    z[69]=z[6]*z[69];
    z[70]= - n<T>(193,3)*z[59] + 5*z[6];
    z[70]=n<T>(1,8)*z[70] - n<T>(589,27)*z[4];
    z[70]=z[4]*z[70];
    z[69]=n<T>(1,12)*z[69] + z[70];
    z[69]=z[4]*z[69];
    z[70]= - 205*z[59] - n<T>(61,6)*z[6];
    z[70]=z[70]*npow(z[6],2);
    z[69]=n<T>(1,18)*z[70] + z[69];
    z[56]=z[65] + n<T>(1,33)*z[57] + n<T>(1,11)*z[69] + z[56] + z[63] + z[60];
    z[57]= - z[9] - z[7];
    z[55]=z[55]*z[57];
    z[57]=z[59] + z[6];
    z[53]=z[57]*z[53];
    z[57]=z[59] + z[67];
    z[54]=z[57]*z[54];
    z[57]=z[3] - z[6];
    z[60]=z[5] - z[4];
    z[57]= - 32*z[60] - n<T>(25,2)*z[57];
    z[57]=z[2]*z[57];
    z[60]=z[66] - z[7];
    z[63]= - 2*z[3] + z[6] - z[60];
    z[63]=z[3]*z[63];
    z[60]= - z[5] - z[4] - z[60];
    z[60]=z[5]*z[60];
    z[53]=z[57] + 64*z[60] + 25*z[63] + z[53] + z[54] + z[55];
    z[53]=z[2]*z[53];
    z[54]= - 64*z[9] - 251*z[4] + 46*z[64];
    z[54]=128*z[2] + 2*z[54] - 383*z[5];
    z[54]=z[19]*z[54];
    z[53]=z[53] + z[54];
    z[54]= - n<T>(14,11)*z[59] - n<T>(1,4)*z[6];
    z[54]=z[6]*z[54];
    z[55]= - z[62] - z[6];
    z[55]=8*z[55] - n<T>(41,4)*z[4];
    z[55]=z[4]*z[55];
    z[54]=z[54] + n<T>(1,11)*z[55];
    z[55]=z[59] - z[61];
    z[55]=z[9]*z[55];
    z[57]=n<T>(31,18)*z[6] + z[4];
    z[57]=z[7]*z[57];
    z[58]=z[58] - 25*z[7] - z[6] + z[4];
    z[58]=z[3]*z[58];
    z[60]=z[6] + z[66];
    z[60]=z[14]*z[60];
    z[54]=n<T>(25,396)*z[60] + n<T>(1,198)*z[58] + n<T>(25,198)*z[55] + n<T>(1,9)*z[54]
    + n<T>(1,11)*z[57];
    z[54]=z[54]*z[68];
    z[55]=50*z[9] + n<T>(571,2)*z[6] - 86*z[64];
    z[55]= - n<T>(50,3)*z[2] + n<T>(1,3)*z[55] + n<T>(151,2)*z[3];
    z[55]=z[21]*z[55];
    z[57]=8*z[4] + n<T>(1,2)*z[14];
    z[57]=z[30]*z[57];
    z[58]= - n<T>(29,2)*z[6] - 10*z[14];
    z[58]=z[31]*z[58];
    z[55]=z[58] + z[55] + z[57];
    z[57]=z[11] + z[14];
    z[58]=z[59]*z[57];
    z[58]=z[24] + z[58];
    z[58]=z[12]*z[58];
    z[57]=z[59] - z[57];
    z[57]=z[23]*z[57];
    z[57]=z[58] + z[57];
    z[58]=67*z[59] - n<T>(521,4)*z[6];
    z[58]=n<T>(1,3)*z[58] + n<T>(23,2)*z[4];
    z[58]=z[6]*z[58];
    z[60]=56*z[59] - 67*z[6];
    z[60]=n<T>(1,81)*z[60] - z[7];
    z[60]=z[7]*z[60];
    z[61]= - n<T>(5,4)*z[9] + n<T>(5,2)*z[59] - z[7] - z[6];
    z[61]=z[9]*z[61];
    z[58]=n<T>(25,81)*z[61] + n<T>(1,27)*z[58] + z[60];
    z[60]= - n<T>(259,2)*z[59] + 31*z[6];
    z[60]= - n<T>(25,12)*z[9] + n<T>(1,6)*z[60] + 5*z[7];
    z[60]=n<T>(1,11)*z[60] + n<T>(55,18)*z[3];
    z[60]=z[3]*z[60];
    z[58]=n<T>(1,11)*z[58] + n<T>(1,27)*z[60];
    z[58]=z[3]*z[58];
    z[60]=z[9] + z[6];
    z[60]= - z[59]*z[60];
    z[60]= - z[24] + z[60];
    z[60]=z[16]*z[60];
    z[61]=z[66] - z[6];
    z[61]=z[22]*z[61];
    z[60]=z[60] - z[61];
    z[61]=z[9] + z[5];
    z[61]=z[59]*z[61];
    z[61]=z[24] + z[61];
    z[61]=z[15]*z[61];
    z[62]=z[5] - z[66];
    z[62]=z[20]*z[62];
    z[61]=z[61] + z[62];
    z[62]= - z[4] - z[7];
    z[62]=z[59]*z[62];
    z[62]= - z[24] + z[62];
    z[62]=z[13]*z[62];
    z[63]=z[4] - z[64];
    z[63]=z[18]*z[63];
    z[62]=z[62] + z[63];
    z[63]=z[7] + z[3];
    z[59]=z[59]*z[63];
    z[59]=z[24] + z[59];
    z[59]=z[10]*z[59];
    z[63]=z[3] - z[64];
    z[63]=z[17]*z[63];
    z[59]=z[59] + z[63];
    z[63]=n<T>(172,9)*z[5] + n<T>(91,9)*z[3] - n<T>(17,12)*z[9] - n<T>(349,72)*z[7]
     - 
   n<T>(397,9)*z[6] - n<T>(223,4)*z[4];
    z[63]=n<T>(1,8)*z[11] + n<T>(10,11)*z[14] + n<T>(1,11)*z[63] + n<T>(1,18)*z[8];
    z[63]=z[24]*z[63];
    z[64]=7*z[3] + n<T>(59,11)*z[8];
    z[64]=z[28]*z[64];
    z[65]= - 41*z[5] - 23*z[8];
    z[65]=z[29]*z[65];
    z[66]=z[25]*i;

    r +=  - n<T>(94,891)*z[26] - n<T>(67,198)*z[27] + n<T>(305,2376)*z[32] + n<T>(25,396)*z[33]
     + n<T>(272,891)*z[34]
     + n<T>(67,7128)*z[35]
     + n<T>(113,2376)*z[36]
       - n<T>(25,891)*z[37] + n<T>(82,891)*z[38] + n<T>(361,1782)*z[39] + n<T>(97,594)*
      z[40] + n<T>(40,81)*z[41] + n<T>(100,891)*z[42] + n<T>(13,1782)*z[43] + n<T>(47,594)*z[44]
     - n<T>(64,891)*z[45]
     - n<T>(1,198)*z[46]
     + n<T>(5,2376)*z[47] - n<T>(2,99)*z[48] - n<T>(7,297)*z[49] - n<T>(13,792)*z[50]
     - n<T>(25,1188)*z[51]
     + n<T>(1,264)*z[52]
     + n<T>(1,891)*z[53]
     + z[54]
     + n<T>(1,297)*z[55]
     + n<T>(1,9)*z[56]
       + n<T>(1,54)*z[57] + z[58] + n<T>(106,891)*z[59] + n<T>(25,297)*z[60] + n<T>(58,297)*z[61]
     + n<T>(85,594)*z[62]
     + n<T>(1,27)*z[63]
     + n<T>(1,162)*z[64]
     + n<T>(2,891)*z[65]
     + n<T>(91,2673)*z[66];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf179(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf179(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
