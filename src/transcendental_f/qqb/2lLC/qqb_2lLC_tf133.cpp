#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf133(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[83];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[9];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[3];
    z[19]=e[5];
    z[20]=e[7];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[11];
    z[24]=e[12];
    z[25]=c[3];
    z[26]=c[11];
    z[27]=c[15];
    z[28]=c[31];
    z[29]=e[14];
    z[30]=e[6];
    z[31]=e[13];
    z[32]=f[2];
    z[33]=f[4];
    z[34]=f[6];
    z[35]=f[10];
    z[36]=f[12];
    z[37]=f[15];
    z[38]=f[16];
    z[39]=f[23];
    z[40]=f[31];
    z[41]=f[32];
    z[42]=f[33];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=f[51];
    z[47]=f[52];
    z[48]=f[53];
    z[49]=f[55];
    z[50]=f[57];
    z[51]=f[58];
    z[52]=f[72];
    z[53]=f[73];
    z[54]=13*z[4];
    z[55]=z[1]*i;
    z[56]=3*z[55];
    z[57]= - z[56] - z[54];
    z[58]=z[6] - z[5];
    z[59]=n<T>(1,4)*z[9];
    z[60]=n<T>(1,2)*z[3];
    z[57]=z[8] - z[59] + z[60] + n<T>(1,4)*z[57] + z[7] - n<T>(3,2)*z[58];
    z[57]=z[8]*z[57];
    z[58]=n<T>(5,2)*z[4];
    z[56]= - z[56] - z[58];
    z[61]=n<T>(1,2)*z[4];
    z[56]=z[56]*z[61];
    z[62]=z[55] + z[4];
    z[63]=n<T>(1,2)*z[7];
    z[64]=z[62] - z[63];
    z[65]= - z[7]*z[64];
    z[56]=z[56] + z[65];
    z[65]=n<T>(5,2)*z[7];
    z[66]=n<T>(5,2)*z[55];
    z[67]= - z[65] + z[66] + z[4];
    z[67]=n<T>(1,2)*z[67] + z[3];
    z[67]=z[3]*z[67];
    z[68]=z[55] - z[7];
    z[69]=n<T>(1,4)*z[5];
    z[70]= - z[69] - n<T>(5,2)*z[3] - z[68];
    z[71]=n<T>(1,2)*z[5];
    z[70]=z[70]*z[71];
    z[72]=n<T>(1,2)*z[6];
    z[73]= - z[72] - z[68];
    z[73]=z[6]*z[73];
    z[74]=n<T>(1,2)*z[9];
    z[75]= - z[74] + z[62];
    z[75]=z[9]*z[75];
    z[56]=n<T>(1,4)*z[57] + n<T>(5,4)*z[75] + n<T>(3,4)*z[73] + z[70] + n<T>(1,2)*z[56]
    + z[67];
    z[56]=z[8]*z[56];
    z[57]=n<T>(5,3)*z[4];
    z[67]=n<T>(5,3)*z[5];
    z[70]=z[57] - z[67] + z[6] - z[3];
    z[73]= - n<T>(1,4)*z[2] + z[74];
    z[70]=z[70]*z[73];
    z[73]=n<T>(1,2)*z[55];
    z[75]= - z[73] - z[4];
    z[75]=z[4]*z[75];
    z[76]=z[7]*z[61];
    z[75]=z[75] + z[76];
    z[76]=z[7] - z[4];
    z[77]=z[76] - z[55];
    z[78]=z[5] - z[77];
    z[78]=z[5]*z[78];
    z[79]=z[6] + z[3];
    z[80]= - z[68] - z[79];
    z[80]=z[80]*z[72];
    z[81]=n<T>(1,2)*z[68] + z[3];
    z[81]=z[3]*z[81];
    z[70]=z[80] + n<T>(5,6)*z[78] + n<T>(5,3)*z[75] + z[81] + z[70];
    z[70]=z[2]*z[70];
    z[75]=z[2] - z[62] - n<T>(7,3)*z[79];
    z[75]=z[22]*z[75];
    z[78]=z[55]*z[9];
    z[78]=z[78] + z[25];
    z[79]=z[6]*z[55];
    z[79]=z[79] + z[78];
    z[79]=z[16]*z[79];
    z[80]=z[55] - z[9];
    z[81]= - z[6] + z[80];
    z[81]=z[23]*z[81];
    z[70]=z[79] + z[70] + z[75] + z[81] - z[34];
    z[75]=5*z[55];
    z[79]=z[75] - n<T>(37,12)*z[4];
    z[79]=z[4]*z[79];
    z[57]= - z[7]*z[57];
    z[81]=z[68] - z[3];
    z[82]= - z[3]*z[81];
    z[57]=z[82] + z[79] + z[57];
    z[63]=z[72] - z[55] - z[63];
    z[63]=z[6]*z[63];
    z[58]=5*z[5] + z[60] - z[58] + z[80];
    z[58]=z[58]*z[74];
    z[64]=z[71] - z[64];
    z[64]=z[64]*z[67];
    z[57]=z[58] + z[63] + n<T>(1,2)*z[57] + z[64];
    z[57]=z[57]*z[74];
    z[58]=n<T>(1,3)*z[4];
    z[63]= - z[66] - 17*z[4];
    z[63]=z[63]*z[58];
    z[64]=z[65] - z[75] + 3*z[4];
    z[64]=z[7]*z[64];
    z[63]=z[63] + z[64];
    z[64]=z[60] + z[76];
    z[64]=z[3]*z[64];
    z[65]=n<T>(9,2)*z[6] + z[68];
    z[65]=z[6]*z[65];
    z[63]=z[65] + n<T>(1,2)*z[63] + z[64];
    z[64]= - z[5]*z[77];
    z[65]=z[74] - z[55] - z[3];
    z[59]=z[65]*z[59];
    z[65]= - z[75] + 5*z[7];
    z[66]=n<T>(19,2)*z[4];
    z[67]= - z[66] + z[65];
    z[67]= - n<T>(23,4)*z[13] + 7*z[6] + n<T>(1,2)*z[67] + z[3];
    z[67]=z[13]*z[67];
    z[59]=n<T>(1,8)*z[67] + z[59] + z[64] + n<T>(1,4)*z[63];
    z[59]=z[13]*z[59];
    z[54]=n<T>(37,2)*z[7] - 37*z[55] - z[54];
    z[54]=z[7]*z[54];
    z[63]=23*z[55] - 19*z[4];
    z[63]=z[4]*z[63];
    z[54]=z[63] + z[54];
    z[63]=n<T>(5,4)*z[3] - z[77];
    z[63]=z[3]*z[63];
    z[64]=n<T>(79,18)*z[5] + z[3] + n<T>(37,6)*z[7] + z[73] - n<T>(43,3)*z[4];
    z[64]=z[64]*z[69];
    z[54]=z[64] + n<T>(1,12)*z[54] + z[63];
    z[54]=z[54]*z[71];
    z[63]=z[55] - n<T>(7,2)*z[4];
    z[63]=z[63]*z[58];
    z[62]=z[72] + z[62];
    z[62]=z[6]*z[62];
    z[64]=z[61] + z[55];
    z[67]=z[11] + z[13];
    z[69]=z[67] - z[64];
    z[69]= - z[72] + n<T>(1,3)*z[69];
    z[69]=z[11]*z[69];
    z[62]=z[69] + z[63] + z[62];
    z[63]= - z[13]*z[58];
    z[62]=z[63] + n<T>(1,2)*z[62];
    z[63]=n<T>(13,16)*z[11];
    z[62]=z[62]*z[63];
    z[69]= - z[55]*z[67];
    z[69]= - z[25] + z[69];
    z[69]=z[12]*z[69];
    z[67]= - z[55] + z[67];
    z[67]=z[24]*z[67];
    z[67]=z[69] + z[67];
    z[64]= - z[64]*z[61];
    z[69]=n<T>(1,3)*z[55];
    z[71]=n<T>(1,3)*z[7] - z[69] + z[61];
    z[71]=z[7]*z[71];
    z[64]=z[64] + z[71];
    z[71]= - n<T>(31,24)*z[3] + n<T>(7,12)*z[7] - n<T>(7,6)*z[55] - z[4];
    z[71]=z[3]*z[71];
    z[64]=n<T>(1,2)*z[64] + z[71];
    z[64]=z[3]*z[64];
    z[66]= - 21*z[55] + z[66];
    z[66]=z[4]*z[66];
    z[61]= - n<T>(5,6)*z[7] + n<T>(5,3)*z[55] + z[61];
    z[61]=z[7]*z[61];
    z[69]=n<T>(5,12)*z[3] + n<T>(1,6)*z[7] + z[69] + n<T>(3,2)*z[4];
    z[69]=z[3]*z[69];
    z[61]=z[69] + n<T>(1,16)*z[66] + z[61];
    z[66]=5*z[4];
    z[69]=n<T>(41,3)*z[55] - z[66];
    z[69]=n<T>(7,6)*z[3] + n<T>(1,16)*z[69] - n<T>(5,3)*z[7];
    z[69]= - n<T>(9,4)*z[6] + n<T>(1,2)*z[69] + z[5];
    z[69]=z[69]*z[72];
    z[71]= - z[5]*z[4];
    z[61]=z[69] + n<T>(1,2)*z[61] + z[71];
    z[61]=z[6]*z[61];
    z[69]=z[9] + n<T>(409,8)*z[4] + 7*z[7];
    z[60]= - z[63] + n<T>(23,4)*z[8] - n<T>(121,24)*z[13] + n<T>(47,12)*z[6] - n<T>(113,12)*z[5]
     - z[60]
     + n<T>(1,3)*z[69];
    z[60]=z[25]*z[60];
    z[63]=z[2] - z[9];
    z[63]= - z[13] + n<T>(91,24)*z[5] + z[66] - n<T>(11,6)*z[68] - n<T>(5,6)*z[63];
    z[63]=z[20]*z[63];
    z[66]= - z[5]*z[55];
    z[66]=z[66] - z[78];
    z[66]=z[15]*z[66];
    z[69]= - z[5] + z[80];
    z[69]=z[21]*z[69];
    z[66]=z[66] + z[69];
    z[69]=z[55]*z[7];
    z[69]=z[69] + z[25];
    z[71]=z[4]*z[55];
    z[71]=z[71] + z[69];
    z[71]=z[14]*z[71];
    z[68]= - z[4] + z[68];
    z[68]=z[19]*z[68];
    z[68]=z[71] + z[68];
    z[71]= - z[3]*z[55];
    z[69]=z[71] - z[69];
    z[69]=z[10]*z[69];
    z[71]=z[17]*z[81];
    z[69]=z[69] + z[71];
    z[71]= - z[55] + n<T>(17,8)*z[4];
    z[58]=z[71]*z[58];
    z[65]= - n<T>(11,3)*z[4] - z[65];
    z[65]=z[7]*z[65];
    z[58]=z[58] + n<T>(1,8)*z[65];
    z[58]=z[7]*z[58];
    z[65]=n<T>(7,4)*z[8] + z[5] + n<T>(3,4)*z[3] + z[77];
    z[65]=z[18]*z[65];
    z[71]=z[44] + z[41];
    z[72]=z[50] + z[37];
    z[73]=z[53] - z[38];
    z[55]=n<T>(263,4)*z[55] + n<T>(407,3)*z[4];
    z[55]=z[55]*npow(z[4],2);
    z[74]= - n<T>(3,2)*z[13] - n<T>(5,2)*z[6] + z[5] - z[4] - z[3];
    z[74]=z[29]*z[74];
    z[75]=z[5] + z[8];
    z[75]=z[30]*z[75];
    z[76]=n<T>(91,16)*z[4] + 4*z[13];
    z[76]=z[31]*z[76];
    z[77]=z[26]*i;

    r += n<T>(17,48)*z[27] - n<T>(20,3)*z[28] - n<T>(3,4)*z[32] - n<T>(19,24)*z[33]
     - n<T>(7,12)*z[35]
     - n<T>(5,8)*z[36]
     + n<T>(1,4)*z[39] - n<T>(77,48)*z[40] - n<T>(49,12)*
      z[42] + n<T>(1,16)*z[43] + n<T>(5,12)*z[45] - n<T>(91,192)*z[46] - n<T>(5,16)*
      z[47] - n<T>(9,8)*z[48] - n<T>(13,64)*z[49] + n<T>(19,64)*z[51] - n<T>(3,8)*z[52]
       + z[54] + n<T>(1,48)*z[55] + z[56] + z[57] + z[58] + z[59] + n<T>(1,12)*
      z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + n<T>(5,3)*z[66] + n<T>(13,48)*z[67]
     + n<T>(5,4)*z[68]
     + n<T>(1,6)*z[69]
     + n<T>(1,2)*z[70] - n<T>(15,16)*
      z[71] - 4*z[72] - n<T>(1,8)*z[73] + z[74] + z[75] + n<T>(1,3)*z[76] - n<T>(61,192)*z[77];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf133(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf133(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
