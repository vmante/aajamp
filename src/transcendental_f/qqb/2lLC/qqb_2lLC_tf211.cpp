#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf211(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[16];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=d[17];
    z[5]=c[11];
    z[6]=c[15];
    z[7]=c[31];
    z[8]=d[7];
    z[9]=e[7];
    z[10]=e[9];
    z[11]=f[31];
    z[12]=n<T>(1,2)*z[3];
    z[13]=i*z[1];
    z[14]= - z[13] + z[12];
    z[14]=z[3]*z[14];
    z[12]=z[12] - z[8];
    z[15]= - n<T>(1,2)*z[2] - z[13] - z[12];
    z[15]=z[2]*z[15];
    z[13]=z[13]*z[4];
    z[13]=z[13] - z[9] + z[10];
    z[14]=z[15] + z[14] + z[13];
    z[14]=z[2]*z[14];
    z[12]=z[3]*z[12];
    z[12]=z[12] + z[13];
    z[12]=z[3]*z[12];
    z[13]=i*z[5];

    r +=  - z[6] + n<T>(3,2)*z[7] + z[11] + z[12] + n<T>(1,6)*z[13] + z[14];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf211(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf211(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
