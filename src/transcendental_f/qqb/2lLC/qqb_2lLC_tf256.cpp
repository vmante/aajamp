#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf256(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,82>& f,
  const std::array<std::complex<T>,330>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[23];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[79];
    z[3]=c[11];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=c[12];
    z[9]=g[243];
    z[10]=g[251];
    z[11]=g[254];
    z[12]=g[270];
    z[13]=g[272];
    z[14]=g[276];
    z[15]=g[278];
    z[16]=g[288];
    z[17]=g[289];
    z[18]=g[290];
    z[19]=z[4] - z[5] + z[6] - z[7];
    z[20]= - i*z[1];
    z[20]=z[20] + z[19];
    z[20]=z[2]*z[20];
    z[20]=z[20] + z[10];
    z[21]=i*z[3];
    z[21]= - n<T>(2,9)*z[21] + 6*z[8];
    z[19]=z[19]*z[21];
    z[21]=z[13] - z[14];
    z[22]=z[12] + z[15];

    r +=  - 6*z[9] + z[11] + n<T>(1,2)*z[16] + z[17] + 2*z[18] + z[19] + 3*
      z[20] + n<T>(9,2)*z[21] + n<T>(3,2)*z[22];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf256(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,82>& f,
  const std::array<std::complex<double>,330>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf256(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,82>& f,
  const std::array<std::complex<dd_real>,330>& g
);
#endif
