#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf142(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[78];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[9];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[3];
    z[19]=e[5];
    z[20]=e[7];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[11];
    z[24]=e[12];
    z[25]=c[3];
    z[26]=c[11];
    z[27]=c[15];
    z[28]=c[31];
    z[29]=e[6];
    z[30]=e[13];
    z[31]=f[2];
    z[32]=f[4];
    z[33]=f[6];
    z[34]=f[10];
    z[35]=f[12];
    z[36]=f[15];
    z[37]=f[16];
    z[38]=f[23];
    z[39]=f[31];
    z[40]=f[32];
    z[41]=f[33];
    z[42]=f[36];
    z[43]=f[39];
    z[44]=f[43];
    z[45]=f[51];
    z[46]=f[52];
    z[47]=f[53];
    z[48]=f[55];
    z[49]=f[58];
    z[50]=f[72];
    z[51]=f[73];
    z[52]=n<T>(1,2)*z[5];
    z[53]=n<T>(1,3)*z[9];
    z[54]=z[1]*i;
    z[55]=n<T>(1,3)*z[54];
    z[56]=n<T>(1,2)*z[4];
    z[57]=z[53] - z[52] - n<T>(1,6)*z[6] + n<T>(1,12)*z[3] - z[55] + z[56];
    z[57]=z[9]*z[57];
    z[58]=z[54] + z[4];
    z[59]=n<T>(1,2)*z[7];
    z[60]=n<T>(1,2)*z[6];
    z[61]= - z[60] - n<T>(2,3)*z[3] + z[59] + z[58];
    z[61]=z[6]*z[61];
    z[62]=z[59] - z[54];
    z[63]=2*z[4];
    z[52]= - z[52] - z[63] - z[62];
    z[64]=n<T>(1,3)*z[5];
    z[52]=z[52]*z[64];
    z[65]=n<T>(1,6)*z[7];
    z[66]=z[65] - z[54] - n<T>(25,24)*z[4];
    z[66]=z[4]*z[66];
    z[67]=n<T>(1,2)*z[3];
    z[68]= - n<T>(1,3)*z[3] - z[55] - z[7];
    z[68]=z[68]*z[67];
    z[52]=z[57] + z[52] + z[61] + z[68] + z[66];
    z[52]=z[9]*z[52];
    z[57]=2*z[54];
    z[61]=z[7] - z[4];
    z[66]=z[57] - z[61];
    z[68]=z[7]*z[66];
    z[69]=n<T>(1,2)*z[54];
    z[70]=z[69] + z[4];
    z[59]= - z[3] + z[59] - z[70];
    z[59]=z[3]*z[59];
    z[71]=z[54] - z[7];
    z[72]= - z[60] - z[71];
    z[72]=z[6]*z[72];
    z[73]=n<T>(1,4)*z[5];
    z[74]=z[73] + n<T>(5,2)*z[3] + z[71];
    z[74]=z[5]*z[74];
    z[75]=z[5] + z[6];
    z[76]= - z[54] + z[4];
    z[75]= - n<T>(1,2)*z[8] + n<T>(11,8)*z[9] + n<T>(7,4)*z[3] + n<T>(3,8)*z[76]
     - z[7]
    - n<T>(3,4)*z[75];
    z[75]=z[8]*z[75];
    z[76]=z[54]*z[4];
    z[59]=z[75] + z[74] + n<T>(3,2)*z[72] + z[59] - z[76] + z[68];
    z[59]=z[8]*z[59];
    z[68]=n<T>(2,3)*z[7];
    z[72]= - z[68] + n<T>(4,3)*z[54] - z[56];
    z[72]=z[7]*z[72];
    z[74]=n<T>(11,2)*z[4];
    z[75]= - 19*z[54] - z[74];
    z[75]=z[4]*z[75];
    z[55]=n<T>(11,12)*z[3] - z[65] + z[55] - n<T>(3,2)*z[4];
    z[55]=z[3]*z[55];
    z[77]=n<T>(31,3)*z[54] - 19*z[4];
    z[68]= - n<T>(1,4)*z[6] + n<T>(17,12)*z[3] + n<T>(1,32)*z[77] - z[68];
    z[68]=z[6]*z[68];
    z[55]=z[68] + z[55] + n<T>(1,16)*z[75] + z[72];
    z[55]=z[6]*z[55];
    z[68]=n<T>(1,3)*z[4];
    z[72]=n<T>(53,2)*z[54] + 23*z[4];
    z[72]=z[72]*z[68];
    z[62]= - 3*z[4] - z[62];
    z[62]=z[7]*z[62];
    z[62]=z[72] + z[62];
    z[72]= - z[67] - z[61];
    z[72]=z[3]*z[72];
    z[75]=z[60] - z[71];
    z[75]=z[6]*z[75];
    z[62]=z[75] + n<T>(1,2)*z[62] + z[72];
    z[66]= - z[6] - z[66];
    z[66]=z[5]*z[66];
    z[67]= - n<T>(1,4)*z[9] + z[5] + z[67] + z[69] - z[4];
    z[67]=z[9]*z[67];
    z[69]=z[74] + z[71];
    z[72]=z[6] - z[3];
    z[69]= - n<T>(9,4)*z[13] + n<T>(1,2)*z[69] + z[72];
    z[69]=z[13]*z[69];
    z[62]=n<T>(1,4)*z[69] + z[67] + n<T>(1,2)*z[62] + z[66];
    z[62]=z[13]*z[62];
    z[64]=z[68] + z[72] - z[64];
    z[66]=n<T>(1,4)*z[2] - n<T>(1,2)*z[9];
    z[64]=z[64]*z[66];
    z[66]=z[4]*z[70];
    z[67]= - z[7]*z[56];
    z[66]=z[66] + z[67];
    z[67]=z[6] + z[3];
    z[69]=z[71] + z[67];
    z[69]=z[69]*z[60];
    z[61]=z[61] - z[54];
    z[70]= - z[5] + z[61];
    z[70]=z[5]*z[70];
    z[72]= - n<T>(1,2)*z[71] - z[3];
    z[72]=z[3]*z[72];
    z[64]=n<T>(1,6)*z[70] + z[69] + n<T>(1,3)*z[66] + z[72] + z[64];
    z[64]=z[2]*z[64];
    z[66]=z[54]*z[9];
    z[66]=z[66] + z[25];
    z[69]=z[5]*z[54];
    z[69]=z[69] + z[66];
    z[69]=z[15]*z[69];
    z[70]= - z[6]*z[54];
    z[66]=z[70] - z[66];
    z[66]=z[16]*z[66];
    z[70]=z[54] - z[9];
    z[72]=z[5] - z[70];
    z[72]=z[21]*z[72];
    z[70]=z[6] - z[70];
    z[70]=z[23]*z[70];
    z[66]=z[33] + z[69] + z[66] + z[72] + z[70];
    z[69]=z[54] - n<T>(7,2)*z[4];
    z[69]=z[69]*z[68];
    z[70]=z[60] + z[58];
    z[70]=z[6]*z[70];
    z[72]=z[56] + z[54];
    z[74]=z[11] + z[13];
    z[75]=z[74] - z[72];
    z[60]= - z[60] + n<T>(1,3)*z[75];
    z[60]=z[11]*z[60];
    z[60]=z[60] + z[69] + z[70];
    z[69]= - z[13]*z[68];
    z[60]=z[69] + n<T>(1,2)*z[60];
    z[60]=z[11]*z[60];
    z[60]=z[60] - z[42];
    z[65]= - n<T>(13,12)*z[3] - z[65] + 3*z[54] + z[63];
    z[65]=z[3]*z[65];
    z[69]=z[72]*z[56];
    z[56]= - z[56] - n<T>(5,3)*z[71];
    z[56]=z[7]*z[56];
    z[56]=z[65] + z[69] + z[56];
    z[56]=z[3]*z[56];
    z[65]=5*z[54] + 17*z[4];
    z[65]=z[4]*z[65];
    z[69]= - n<T>(17,2)*z[7] + 17*z[54] + 5*z[4];
    z[69]=z[7]*z[69];
    z[65]=z[65] + z[69];
    z[69]= - n<T>(5,4)*z[3] + z[61];
    z[69]=z[3]*z[69];
    z[70]= - n<T>(47,18)*z[5] - z[3] - n<T>(17,6)*z[7] + n<T>(3,2)*z[54] + n<T>(17,3)*
    z[4];
    z[70]=z[70]*z[73];
    z[72]=z[6]*z[4];
    z[65]=z[70] + z[72] + n<T>(1,12)*z[65] + z[69];
    z[65]=z[5]*z[65];
    z[69]=2*z[13];
    z[53]=n<T>(1,3)*z[2] + z[69] - z[53] - n<T>(11,12)*z[5] - z[63] + n<T>(7,3)*
    z[71];
    z[53]=z[20]*z[53];
    z[63]= - z[54]*z[74];
    z[63]= - z[25] + z[63];
    z[63]=z[12]*z[63];
    z[70]= - z[54] + z[74];
    z[70]=z[24]*z[70];
    z[63]=z[63] + z[70];
    z[70]=z[54]*z[7];
    z[70]=z[70] + z[25];
    z[72]= - z[76] - z[70];
    z[72]=z[14]*z[72];
    z[73]=z[4] - z[71];
    z[73]=z[19]*z[73];
    z[72]= - z[72] - z[73] + z[38] + z[31];
    z[73]= - z[3]*z[54];
    z[70]=z[73] - z[70];
    z[70]=z[10]*z[70];
    z[73]= - z[3] + z[71];
    z[73]=z[17]*z[73];
    z[70]=z[70] + z[73];
    z[57]= - z[57] - n<T>(1,4)*z[4];
    z[57]=z[57]*z[68];
    z[68]=n<T>(7,3)*z[4] - z[71];
    z[68]=z[7]*z[68];
    z[57]=z[57] + n<T>(1,4)*z[68];
    z[57]=z[7]*z[57];
    z[61]=z[61] + z[5];
    z[61]= - n<T>(11,2)*z[8] - n<T>(7,2)*z[3] - 2*z[61];
    z[61]=z[18]*z[61];
    z[58]= - z[2] + z[58] - z[67];
    z[58]=z[22]*z[58];
    z[67]=z[37] - z[35];
    z[68]=z[49] + z[48];
    z[71]= - n<T>(11,16)*z[11] + n<T>(13,4)*z[8] + n<T>(25,24)*z[13] - n<T>(25,6)*z[9]
    + n<T>(73,12)*z[5] + n<T>(125,12)*z[6] - n<T>(1,6)*z[3] - n<T>(11,8)*z[4] - n<T>(1,3)*
    z[7];
    z[71]=z[25]*z[71];
    z[71]=z[71] - z[44];
    z[54]=n<T>(51,4)*z[54] - n<T>(79,9)*z[4];
    z[54]=z[54]*npow(z[4],2);
    z[54]=z[54] + z[46];
    z[73]= - z[5] - z[8];
    z[73]=z[29]*z[73];
    z[69]= - n<T>(43,8)*z[4] - z[69];
    z[69]=z[30]*z[69];
    z[74]=z[26]*i;

    r += n<T>(21,8)*z[27] - n<T>(14,3)*z[28] + n<T>(1,12)*z[32] + n<T>(7,6)*z[34]
     + 8*
      z[36] - n<T>(17,24)*z[39] + n<T>(3,8)*z[40] + n<T>(17,6)*z[41] + n<T>(5,8)*z[43]
       - n<T>(77,96)*z[45] + n<T>(9,4)*z[47] + n<T>(3,4)*z[50] + n<T>(1,4)*z[51]
     +  z[52] + z[53] + n<T>(1,8)*z[54] + z[55] + z[56] + z[57] + z[58] + 
      z[59] + n<T>(11,8)*z[60] + z[61] + z[62] + n<T>(11,24)*z[63] + z[64] + 
      z[65] + n<T>(2,3)*z[66] - n<T>(7,4)*z[67] - n<T>(11,32)*z[68] + n<T>(1,3)*z[69]
       + n<T>(5,3)*z[70] + n<T>(1,6)*z[71] - n<T>(1,2)*z[72] + 2*z[73] + n<T>(61,96)*
      z[74];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf142(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf142(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
