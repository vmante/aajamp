#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf49(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[13];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[12];
    z[3]=d[1];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[8];
    z[7]=e[5];
    z[8]=i*z[2]*z[1];
    z[8]=z[8] - z[7];
    z[9]=npow(z[3],2);
    z[10]=npow(z[4],2);
    z[11]=npow(z[5],2);
    z[12]=npow(z[6],2);
    z[8]= - n<T>(43,2)*z[12] + n<T>(71129,9360)*z[11] + n<T>(38071,9360)*z[9] + 8*
    z[10] - 16*z[8];

    r += 11*z[8];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf49(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf49(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
