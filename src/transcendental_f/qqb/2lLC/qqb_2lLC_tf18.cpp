#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf18(
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[9];
  std::complex<T> r(0,0);

    z[1]=d[1];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=npow(z[1],2);
    z[6]=npow(z[2],2);
    z[7]=npow(z[3],2);
    z[8]=npow(z[4],2);
    z[5]= - z[8] - z[7] + z[5] + z[6];

    r += n<T>(1,36)*z[5];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf18(
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf18(
  const std::array<std::complex<dd_real>,19>& d
);
#endif
