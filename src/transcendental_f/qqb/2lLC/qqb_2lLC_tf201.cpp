#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf201(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[7];
  std::complex<T> r(0,0);

    z[1]=c[3];
    z[2]=d[0];
    z[3]=c[11];
    z[4]=c[31];
    z[5]=z[2]*z[1];
    z[6]= - i*z[3];
    z[5]=z[6] + z[5] + n<T>(2,3)*z[4];

    r += n<T>(4,3)*z[5];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf201(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf201(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d
);
#endif
