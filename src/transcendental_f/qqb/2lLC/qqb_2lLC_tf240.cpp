#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf240(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[26];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[11];
    z[6]=d[9];
    z[7]=e[12];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[31];
    z[12]=e[13];
    z[13]=e[14];
    z[14]=f[51];
    z[15]=f[58];
    z[16]=z[6] + z[2];
    z[17]=z[1]*i;
    z[18]=z[16]*z[17];
    z[18]=z[18] + z[8];
    z[18]=z[5]*z[18];
    z[16]=z[17] - z[16];
    z[16]=z[7]*z[16];
    z[16]=z[18] + z[16] + z[14] - z[10];
    z[18]= - z[3] + n<T>(1,2)*z[4];
    z[18]=z[18]*z[4];
    z[19]=npow(z[3],2);
    z[20]= - z[3] + n<T>(1,2)*z[2];
    z[21]=z[2]*z[20];
    z[21]= - z[18] + z[19] + 3*z[21];
    z[21]=z[4]*z[21];
    z[22]=2*z[3];
    z[23]=z[22] - z[2];
    z[23]=z[2]*z[23];
    z[18]= - z[18] - n<T>(3,2)*z[19] + z[23];
    z[18]=z[6]*z[18];
    z[23]= - 3*z[4] + z[2];
    z[24]=z[2] - z[3];
    z[23]=z[24]*z[23];
    z[19]=2*z[19];
    z[22]= - z[6]*z[22];
    z[22]=z[22] - z[19] + z[23];
    z[17]=z[22]*z[17];
    z[22]=n<T>(1,2)*z[3] - z[2];
    z[22]=z[2]*z[22];
    z[19]=z[19] + z[22];
    z[19]=z[2]*z[19];
    z[22]=npow(z[3],3);
    z[20]=n<T>(1,6)*z[6] - n<T>(4,3)*z[4] + z[20];
    z[20]=z[8]*z[20];
    z[23]= - z[3] - z[6];
    z[23]=z[12]*z[23];
    z[24]= - z[4] - z[6];
    z[24]=z[13]*z[24];
    z[25]=z[9]*i;

    r += 7*z[11] + z[15] + 2*z[16] + z[17] + z[18] + z[19] + z[20] + 
      z[21] - n<T>(3,2)*z[22] + 3*z[23] + z[24] - n<T>(1,6)*z[25];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf240(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf240(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
