#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf177(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[56];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[5];
    z[8]=d[8];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=d[4];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[4];
    z[16]=e[8];
    z[17]=c[3];
    z[18]=c[11];
    z[19]=c[15];
    z[20]=c[17];
    z[21]=c[19];
    z[22]=c[23];
    z[23]=c[25];
    z[24]=c[26];
    z[25]=c[28];
    z[26]=c[31];
    z[27]=e[3];
    z[28]=e[10];
    z[29]=f[0];
    z[30]=f[1];
    z[31]=f[3];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[10];
    z[35]=f[11];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[17];
    z[39]=f[18];
    z[40]=f[76];
    z[41]=z[1]*i;
    z[42]=2*z[41];
    z[43]=z[42] - z[4];
    z[44]=n<T>(1,3)*z[4];
    z[45]=z[43]*z[44];
    z[46]=n<T>(4,3)*z[2];
    z[44]= - z[46] + z[44] + z[3];
    z[44]=z[5]*z[44];
    z[47]= - z[41] - z[4];
    z[47]=2*z[47] + z[3];
    z[47]=z[3]*z[47];
    z[48]=z[42] + z[4];
    z[48]=2*z[48] - z[2];
    z[48]=z[2]*z[48];
    z[44]=z[44] + n<T>(2,3)*z[48] - z[45] + z[47];
    z[44]=z[5]*z[44];
    z[47]=4*z[3];
    z[48]=z[41]*z[47];
    z[49]=z[41] - z[4];
    z[50]=2*z[6];
    z[51]=n<T>(4,3)*z[11] - 2*z[3] + z[50] + z[49];
    z[51]=z[11]*z[51];
    z[52]=z[43]*z[4];
    z[48]=z[51] + z[52] + z[48];
    z[48]=z[11]*z[48];
    z[51]=2*z[49];
    z[53]=z[51] + z[6];
    z[53]=z[53]*z[50];
    z[53]=z[52] + z[53];
    z[51]= - z[51] - z[11];
    z[51]=z[11]*z[51];
    z[54]=4*z[6] + z[49];
    z[54]=n<T>(1,3)*z[54] - z[11];
    z[54]=z[8]*z[54];
    z[51]=z[54] + n<T>(1,3)*z[53] + z[51];
    z[51]=z[8]*z[51];
    z[53]=4*z[11];
    z[47]= - z[6] - z[7] + z[47] + z[53];
    z[54]= - z[41]*z[47];
    z[54]= - 4*z[17] + z[54];
    z[54]=z[10]*z[54];
    z[55]=4*z[41];
    z[47]= - z[55] + z[47];
    z[47]=z[15]*z[47];
    z[53]= - z[7] - 5*z[6] - z[53];
    z[53]=z[27]*z[53];
    z[44]=z[48] + z[51] + z[38] - z[37] + z[36] + z[31] + z[44] + z[54]
    + z[47] + z[53];
    z[47]= - z[4] - z[6];
    z[47]=z[41]*z[47];
    z[47]= - z[17] + z[47];
    z[47]=z[9]*z[47];
    z[43]=z[2] - z[43];
    z[43]=z[13]*z[43];
    z[48]= - z[6] + z[49];
    z[48]=z[14]*z[48];
    z[51]=z[42] - z[2];
    z[53]=z[5] - z[51];
    z[53]=z[16]*z[53];
    z[54]= - z[6] - z[8];
    z[54]=z[28]*z[54];
    z[43]= - z[23] + z[54] + z[39] + z[34] + z[33] + z[47] + z[43] + 
    z[48] + z[53];
    z[47]=z[42] - z[3];
    z[47]=z[3]*z[47];
    z[48]=npow(z[11],2);
    z[53]=z[55] + z[4];
    z[54]=2*z[2] - z[53];
    z[54]=z[2]*z[54];
    z[47]=n<T>(1,3)*z[54] + z[48] + n<T>(2,3)*z[52] + z[47];
    z[46]=z[47]*z[46];
    z[47]=n<T>(1,3)*z[17];
    z[42]= - z[42] - z[6];
    z[42]=z[6]*z[42];
    z[48]=z[50] - z[3];
    z[48]=z[3]*z[48];
    z[42]=z[47] + z[42] + z[48];
    z[42]=z[7]*z[42];
    z[42]=z[42] - z[26];
    z[48]=z[4]*z[49];
    z[41]= - 7*z[41] + 2*z[4];
    z[41]=2*z[41] - 23*z[6];
    z[41]=z[6]*z[41];
    z[41]= - 8*z[48] + z[41];
    z[41]=z[6]*z[41];
    z[48]=npow(z[6],2);
    z[45]= - z[45] + z[48];
    z[48]=2*z[53] - z[6];
    z[48]=z[3]*z[48];
    z[45]=2*z[45] + n<T>(1,3)*z[48];
    z[45]=z[3]*z[45];
    z[48]= - n<T>(2,9)*z[5] + n<T>(8,9)*z[8] + n<T>(32,9)*z[2] + 2*z[11] + n<T>(26,3)*
    z[3] - n<T>(4,9)*z[4] - z[6];
    z[47]=z[48]*z[47];
    z[48]=z[35] - z[32];
    z[49]=z[3] - z[51];
    z[49]=z[12]*z[49];
    z[49]=z[49] + z[29];
    z[50]=n<T>(80,9)*z[24] + n<T>(16,9)*z[22] + n<T>(4,3)*z[20] + n<T>(4,27)*z[18];
    z[50]=i*z[50];
    z[41]= - n<T>(16,3)*z[40] - n<T>(4,3)*z[30] - n<T>(40,9)*z[25] - n<T>(32,27)*z[21]
    + n<T>(38,9)*z[19] + z[47] + z[46] + n<T>(1,9)*z[41] + z[45] + z[50] + n<T>(1,3)
   *z[42] + n<T>(8,3)*z[49] + n<T>(2,9)*z[48] + n<T>(8,9)*z[43] + n<T>(2,3)*z[44];

    r += 2*z[41];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf177(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf177(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
