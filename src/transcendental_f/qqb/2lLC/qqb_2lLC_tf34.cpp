#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf34(
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[8];
  std::complex<T> r(0,0);

    z[1]=d[1];
    z[2]=d[4];
    z[3]=d[6];
    z[4]=d[8];
    z[5]=e[14];
    z[6]= - z[1]*z[2];
    z[7]= - z[2] + z[1];
    z[7]=z[3]*z[7];
    z[6]=z[6] + z[7];
    z[7]=z[2] - n<T>(1,2)*z[4];
    z[7]=z[4]*z[7];

    r +=  - z[5] + n<T>(1,2)*z[6] + z[7];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf34(
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf34(
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
