#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf183(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[38];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[15];
    z[8]=d[18];
    z[9]=e[2];
    z[10]=e[11];
    z[11]=c[3];
    z[12]=c[11];
    z[13]=c[15];
    z[14]=c[17];
    z[15]=c[31];
    z[16]=e[3];
    z[17]=e[10];
    z[18]=f[2];
    z[19]=f[4];
    z[20]=f[6];
    z[21]=f[12];
    z[22]=f[16];
    z[23]=f[78];
    z[24]=n<T>(1,2)*z[6];
    z[25]=z[4] + z[3];
    z[25]=z[25]*z[24];
    z[26]=npow(z[4],2);
    z[27]=npow(z[5],2);
    z[28]=n<T>(1,2)*z[27];
    z[29]= - z[28] - n<T>(11,6)*z[11] + 5*z[17] - z[10];
    z[30]=n<T>(1,2)*z[3];
    z[31]= - z[4] + z[30];
    z[31]=z[3]*z[31];
    z[25]=z[25] + z[31] + n<T>(1,3)*z[29] + n<T>(1,2)*z[26];
    z[25]=z[25]*z[24];
    z[29]=z[9] + n<T>(1,4)*z[26];
    z[31]=n<T>(1,2)*z[10];
    z[32]=n<T>(1,2)*z[8];
    z[33]=z[32] - 2*z[5];
    z[33]=z[5]*z[33];
    z[34]=n<T>(1,2)*z[4];
    z[35]=z[7] - z[34];
    z[35]=z[3]*z[35];
    z[33]=z[35] + z[33] + z[31] - z[29];
    z[35]=z[3] - z[4];
    z[36]= - z[24] + n<T>(1,3)*z[8] - z[35];
    z[36]=z[36]*z[24];
    z[33]=n<T>(1,3)*z[33] + z[36];
    z[33]=z[1]*z[33];
    z[36]= - z[14] - n<T>(1,9)*z[12];
    z[33]=n<T>(1,4)*z[36] + z[33];
    z[33]=i*z[33];
    z[36]= - z[24] + z[7] + n<T>(1,2)*z[5] + z[35];
    z[37]=i*z[1];
    z[36]=z[36]*z[37];
    z[35]= - z[3]*z[35];
    z[26]=z[36] + z[35] - z[26] - n<T>(1,4)*z[27] + n<T>(7,12)*z[11] + z[9] + n<T>(5,2)*z[17]
     + z[16];
    z[27]=n<T>(1,2)*z[37] - n<T>(1,4)*z[6] - z[30] - z[5] - z[34];
    z[27]=n<T>(1,3)*z[27] + n<T>(3,4)*z[2];
    z[27]=z[2]*z[27];
    z[30]=n<T>(1,3)*z[5];
    z[34]=z[30] - z[6];
    z[24]=z[34]*z[24];
    z[24]=z[27] + z[24] + n<T>(1,3)*z[26];
    z[24]=z[2]*z[24];
    z[26]=z[3]*z[4];
    z[26]=n<T>(1,4)*z[26] + n<T>(1,12)*z[11] + z[29];
    z[26]=z[3]*z[26];
    z[27]=n<T>(1,3)*z[11];
    z[29]=z[16] - z[27];
    z[29]=z[4]*z[29];
    z[26]=z[29] + z[26] + z[18] + z[20];
    z[29]=z[32] + z[7];
    z[27]=z[29]*z[27];
    z[28]=z[28] - z[31] - 2*z[11];
    z[28]=z[28]*z[30];
    z[29]=z[19] + z[22];

    r +=  - n<T>(3,8)*z[13] - n<T>(5,16)*z[15] - n<T>(1,12)*z[21] + z[23] + z[24]
       + z[25] + n<T>(1,3)*z[26] + z[27] + z[28] + n<T>(1,4)*z[29] + z[33];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf183(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf183(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
