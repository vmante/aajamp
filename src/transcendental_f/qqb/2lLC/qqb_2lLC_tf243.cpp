#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf243(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[89];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[9];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[3];
    z[19]=e[5];
    z[20]=e[7];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[11];
    z[24]=e[12];
    z[25]=e[13];
    z[26]=c[3];
    z[27]=c[11];
    z[28]=c[15];
    z[29]=c[31];
    z[30]=e[6];
    z[31]=e[14];
    z[32]=f[2];
    z[33]=f[4];
    z[34]=f[6];
    z[35]=f[10];
    z[36]=f[12];
    z[37]=f[15];
    z[38]=f[16];
    z[39]=f[23];
    z[40]=f[31];
    z[41]=f[32];
    z[42]=f[33];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=f[51];
    z[47]=f[52];
    z[48]=f[53];
    z[49]=f[55];
    z[50]=f[58];
    z[51]=f[72];
    z[52]=f[73];
    z[53]=f[74];
    z[54]=f[75];
    z[55]=9*z[14];
    z[56]=n<T>(41,12)*z[9];
    z[57]=n<T>(67,24)*z[11];
    z[58]=n<T>(5,2)*z[2];
    z[59]= - n<T>(79,8)*z[6] - n<T>(11,3)*z[5] + z[56] + n<T>(43,12)*z[13] + n<T>(15,4)*
    z[8] - z[58] + z[55] + z[57];
    z[59]=n<T>(479,96)*z[4] - n<T>(3,4)*z[3] + n<T>(1,2)*z[59] - 2*z[7];
    z[59]=z[4]*z[59];
    z[60]=3*z[8];
    z[61]=5*z[2];
    z[62]= - n<T>(23,3)*z[15] + z[61];
    z[62]=n<T>(1,2)*z[62] - z[60];
    z[63]=4*z[13];
    z[64]=n<T>(7,12)*z[9];
    z[62]= - n<T>(25,24)*z[5] + z[64] + n<T>(1,2)*z[62] + z[63];
    z[62]=z[5]*z[62];
    z[65]=3*z[13];
    z[66]=z[65] + n<T>(101,12)*z[8];
    z[67]=n<T>(23,12)*z[6];
    z[68]=n<T>(14,3)*z[10];
    z[69]=n<T>(11,4)*z[7] - z[67] - 4*z[5] + n<T>(9,2)*z[14] + z[68] - z[66];
    z[69]=z[7]*z[69];
    z[70]= - z[12] - n<T>(1,2)*z[11];
    z[70]=z[70]*z[57];
    z[71]=n<T>(67,12)*z[12];
    z[65]= - z[71] - z[65];
    z[72]=n<T>(1,2)*z[13];
    z[65]=z[65]*z[72];
    z[73]= - n<T>(23,2)*z[15] + 7*z[16];
    z[73]=n<T>(1,3)*z[73];
    z[74]=n<T>(3,2)*z[13];
    z[75]=n<T>(7,6)*z[9] - z[74] + z[73] - n<T>(3,4)*z[8];
    z[76]=n<T>(1,2)*z[9];
    z[75]=z[75]*z[76];
    z[77]=n<T>(25,2)*z[8];
    z[78]=z[74] - z[2];
    z[79]=z[77] + n<T>(7,3)*z[16] + n<T>(67,8)*z[11] + z[78];
    z[79]= - n<T>(29,96)*z[6] + n<T>(1,2)*z[79] - z[9];
    z[79]=z[6]*z[79];
    z[80]= - n<T>(21,4)*z[3] + n<T>(17,3)*z[7] - 4*z[6] + n<T>(3,2)*z[5] - z[64]
     + 
   n<T>(13,6)*z[8] + z[68] + n<T>(1,2)*z[2];
    z[80]=z[3]*z[80];
    z[81]=n<T>(1,2)*z[25];
    z[82]= - z[81] + 4*z[18] + n<T>(14,3)*z[17];
    z[83]=n<T>(5,2)*z[20];
    z[84]=npow(z[8],2);
    z[85]=n<T>(23,2)*z[21] + 7*z[23];
    z[86]= - n<T>(67,4)*z[24] + z[85];
    z[86]=n<T>(1,3)*z[86] + 9*z[19];
    z[59]=z[59] + z[80] + z[69] + z[79] + z[62] + z[75] + z[65] - n<T>(307,48)*z[84]
     + z[70] - z[83] - z[22]
     + n<T>(1,2)*z[86] - z[82];
    z[59]=z[1]*z[59];
    z[59]= - n<T>(1,96)*z[27] + z[59];
    z[59]=i*z[59];
    z[62]=2*z[18];
    z[65]=npow(z[2],2);
    z[69]=z[62] + n<T>(5,8)*z[65];
    z[56]= - z[56] + z[61] - n<T>(3,2)*z[8];
    z[56]=z[9]*z[56];
    z[61]=n<T>(5,4)*z[2];
    z[70]= - n<T>(39,8)*z[5] + z[64] + z[61] + 2*z[13];
    z[70]=z[5]*z[70];
    z[58]=z[58] - z[60];
    z[58]= - n<T>(5,4)*z[9] + n<T>(1,2)*z[58];
    z[60]= - n<T>(5,4)*z[7] + n<T>(3,4)*z[6] + z[13] + z[58];
    z[60]=z[7]*z[60];
    z[75]=n<T>(1,2)*z[3];
    z[72]=z[8] - z[72];
    z[72]= - n<T>(17,4)*z[3] + n<T>(3,2)*z[7] + n<T>(5,2)*z[6] + 3*z[72] - z[5];
    z[72]=z[72]*z[75];
    z[79]= - n<T>(41,48)*z[11] - z[2];
    z[75]=n<T>(181,12)*z[4] + z[75] + n<T>(7,2)*z[7] - n<T>(67,16)*z[6] - n<T>(61,12)*
    z[5] - n<T>(101,24)*z[9] + n<T>(5,12)*z[13] + 5*z[79] + n<T>(3,8)*z[8];
    z[75]=z[4]*z[75];
    z[79]=n<T>(9,2)*z[19];
    z[80]=npow(z[11],2);
    z[86]= - n<T>(67,3)*z[11] - n<T>(41,2)*z[13];
    z[86]=z[13]*z[86];
    z[87]=2*z[5];
    z[88]= - n<T>(51,32)*z[6] + n<T>(67,16)*z[11] - z[87];
    z[88]=z[6]*z[88];
    z[56]=n<T>(1,2)*z[75] + z[72] + z[60] + z[88] + z[70] + n<T>(1,4)*z[56] + n<T>(1,8)*z[86]
     - n<T>(3,16)*z[84]
     - n<T>(67,96)*z[80]
     + n<T>(559,144)*z[26]
     + n<T>(61,6)*
    z[20] + n<T>(199,24)*z[25] - z[79] - z[22] - z[69];
    z[56]=z[4]*z[56];
    z[60]=npow(z[13],2);
    z[65]=z[81] + n<T>(1,4)*z[65] + n<T>(3,4)*z[60];
    z[70]=z[74] + z[2];
    z[64]=z[64] - z[70];
    z[64]=z[64]*z[76];
    z[70]=n<T>(1,3)*z[9] - z[70];
    z[70]= - n<T>(49,12)*z[6] + n<T>(1,2)*z[70] + z[87];
    z[70]=z[6]*z[70];
    z[72]=14*z[17] + 11*z[22];
    z[74]=n<T>(1,2)*z[8];
    z[75]=z[5]*z[74];
    z[78]=z[78] + z[9];
    z[81]= - 3*z[5] - n<T>(13,3)*z[8] + z[78];
    z[81]= - n<T>(31,6)*z[7] + n<T>(1,2)*z[81] + n<T>(13,3)*z[6];
    z[81]=z[7]*z[81];
    z[86]=n<T>(2,3)*z[9];
    z[88]=n<T>(51,8)*z[3] - n<T>(13,12)*z[7] - n<T>(13,4)*z[6] - n<T>(1,4)*z[5] + z[86]
    + n<T>(3,4)*z[13] + z[2] + n<T>(1,12)*z[8];
    z[88]=z[3]*z[88];
    z[64]=z[88] + z[81] + z[70] + z[75] + z[64] - n<T>(22,3)*z[84] + n<T>(19,36)
   *z[26] + z[20] + n<T>(37,3)*z[18] + n<T>(1,3)*z[72] + z[65];
    z[64]=z[3]*z[64];
    z[70]=z[2] - n<T>(1,6)*z[9];
    z[70]=z[70]*z[76];
    z[72]=n<T>(1,2)*z[6];
    z[75]=n<T>(7,2)*z[6] + n<T>(25,4)*z[8] - n<T>(21,16)*z[11] + z[78];
    z[75]=z[75]*z[72];
    z[81]=3*z[31];
    z[62]=z[75] + z[70] + n<T>(25,8)*z[84] - n<T>(67,32)*z[80] + n<T>(11,24)*z[26]
    - z[62] + n<T>(11,3)*z[22] + z[81] - n<T>(7,6)*z[23] - z[65];
    z[62]=z[6]*z[62];
    z[58]=z[87] - z[63] - z[58];
    z[58]=z[5]*z[58];
    z[63]=z[67] - z[77] - z[78];
    z[63]=z[63]*z[72];
    z[65]= - n<T>(29,12)*z[7] + n<T>(23,24)*z[6] + n<T>(1,2)*z[66] + z[87];
    z[65]=z[7]*z[65];
    z[58]=z[65] + z[63] + z[58] + n<T>(3,2)*z[60] + n<T>(101,24)*z[84] + n<T>(17,9)*
    z[26] + n<T>(7,2)*z[20] - z[79] + z[82];
    z[58]=z[7]*z[58];
    z[63]= - z[61] + z[86];
    z[63]=z[9]*z[63];
    z[65]=2*z[30];
    z[61]=n<T>(7,3)*z[5] - n<T>(7,24)*z[9] + z[61] + z[13];
    z[61]=z[5]*z[61];
    z[61]=z[61] + z[63] + z[60] - z[84] - n<T>(91,48)*z[26] + n<T>(95,12)*z[20]
    - z[65] - n<T>(23,12)*z[21] + z[69];
    z[61]=z[5]*z[61];
    z[55]= - z[71] + z[73] + z[55];
    z[55]=n<T>(1,2)*z[55] + z[68];
    z[55]=z[26]*z[55];
    z[60]=n<T>(9,16)*z[60] + n<T>(67,48)*z[80] - n<T>(385,144)*z[26] - 2*z[20] + n<T>(23,3)*z[25]
     + z[81]
     + n<T>(67,24)*z[24];
    z[60]=z[13]*z[60];
    z[63]=z[74] + z[13];
    z[63]=n<T>(3,2)*z[63] - n<T>(7,3)*z[9];
    z[63]=z[63]*z[76];
    z[63]=z[63] + n<T>(35,8)*z[84] + n<T>(19,72)*z[26] - n<T>(1,3)*z[85] + 3*z[20];
    z[63]=z[63]*z[76];
    z[66]=n<T>(1,2)*z[80] + z[24] - n<T>(1,4)*z[26];
    z[57]=z[66]*z[57];
    z[65]= - n<T>(4,3)*z[84] - n<T>(485,72)*z[26] - z[65] + n<T>(43,3)*z[18];
    z[65]=z[8]*z[65];
    z[66]= - z[48] + n<T>(21,8)*z[49] - n<T>(75,8)*z[50] - 3*z[51] - z[52] + 
    z[54] + z[53];
    z[66]=z[39] - 15*z[42] - n<T>(35,8)*z[43] - n<T>(3,8)*z[44] + n<T>(5,2)*z[45]
     - 
   n<T>(205,48)*z[46] + n<T>(1,2)*z[66] - 3*z[47];
    z[67]=z[22] - z[83];
    z[67]=z[2]*z[67];

    r +=  - n<T>(45,8)*z[28] + n<T>(35,4)*z[29] - n<T>(5,12)*z[32] + n<T>(5,24)*z[33]
       - n<T>(7,6)*z[34] - n<T>(61,12)*z[35] - n<T>(107,24)*z[36] - n<T>(47,4)*z[37]
     +  n<T>(37,8)*z[38] - n<T>(197,48)*z[40] - 3*z[41] + z[55] + z[56] + z[57]
       + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65]
       + n<T>(1,2)*z[66] + z[67];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf243(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf243(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
