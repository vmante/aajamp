#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf158(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[4];
    z[3]=d[5];
    z[4]=d[7];
    z[5]=d[6];
    z[6]=d[17];
    z[7]=e[9];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[17];
    z[12]=c[31];
    z[13]=e[6];
    z[14]=e[7];
    z[15]=f[31];
    z[16]=f[36];
    z[17]=f[39];
    z[18]=f[40];
    z[19]=n<T>(3,2)*z[2];
    z[20]=n<T>(1,2)*z[2];
    z[21]=z[3] + z[20];
    z[21]=z[21]*z[19];
    z[22]=z[1]*z[6];
    z[23]=z[3]*z[1];
    z[22]= - z[22] + n<T>(3,2)*z[23];
    z[24]=z[1]*z[19];
    z[24]=z[24] - z[22];
    z[24]=i*z[24];
    z[25]=z[5] - z[3];
    z[26]=i*z[1];
    z[25]= - z[26] + 3*z[25];
    z[25]=z[5]*z[25];
    z[27]=npow(z[3],2);
    z[28]= - z[7] + n<T>(9,4)*z[27];
    z[21]=z[25] + z[24] + z[21] - n<T>(5,4)*z[8] - z[28];
    z[24]=z[3] - z[2];
    z[24]=n<T>(1,3)*z[4] + n<T>(3,8)*z[24] - z[26];
    z[24]=z[4]*z[24];
    z[21]=n<T>(1,2)*z[21] + z[24];
    z[21]=z[4]*z[21];
    z[24]= - z[1]*z[20];
    z[23]= - z[23] + z[24];
    z[19]=z[23]*z[19];
    z[23]=z[1]*z[28];
    z[24]=9*z[11] - n<T>(1,3)*z[9];
    z[19]=z[19] + n<T>(1,2)*z[24] + z[23];
    z[19]=i*z[19];
    z[23]=z[8]*z[6];
    z[19]=z[23] + z[19];
    z[23]=npow(z[2],2);
    z[24]= - n<T>(7,6)*z[5] - z[2] - z[26];
    z[24]=z[5]*z[24];
    z[23]=z[23] - z[24];
    z[22]=i*z[22];
    z[22]=z[22] + n<T>(3,4)*z[27] - n<T>(1,3)*z[8] - z[7] + n<T>(3,2)*z[14] + z[13]
    - n<T>(1,2)*z[23];
    z[22]=z[5]*z[22];
    z[23]=n<T>(3,2)*z[3];
    z[24]=z[14] + n<T>(1,2)*z[27];
    z[23]=z[24]*z[23];
    z[24]= - n<T>(3,4)*z[3] + n<T>(5,3)*z[2];
    z[20]=z[24]*z[20];
    z[20]=z[20] - n<T>(3,8)*z[27] + z[13] + n<T>(1,6)*z[8];
    z[20]=z[2]*z[20];

    r += n<T>(11,8)*z[10] - n<T>(27,16)*z[12] - n<T>(9,8)*z[15] + n<T>(5,8)*z[16]
     - n<T>(3,8)*z[17]
     + z[18]
     + n<T>(1,2)*z[19]
     + z[20]
     + z[21]
     + z[22]
     + z[23];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf158(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf158(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
