#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf80(
  const std::array<std::complex<T>,82>& f
) {
    return f[2];
}

template std::complex<double> qqb_2lLC_tf80(
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf80(
  const std::array<std::complex<dd_real>,82>& f
);
#endif
