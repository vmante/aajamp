#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf239(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[48];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[9];
    z[8]=d[15];
    z[9]=d[2];
    z[10]=d[5];
    z[11]=d[11];
    z[12]=d[18];
    z[13]=e[2];
    z[14]=e[11];
    z[15]=e[12];
    z[16]=c[3];
    z[17]=c[11];
    z[18]=c[15];
    z[19]=c[31];
    z[20]=e[3];
    z[21]=e[10];
    z[22]=e[13];
    z[23]=e[14];
    z[24]=f[2];
    z[25]=f[4];
    z[26]=f[6];
    z[27]=f[12];
    z[28]=f[16];
    z[29]=f[51];
    z[30]=f[55];
    z[31]=f[58];
    z[32]=z[3] + z[2];
    z[33]=z[1]*i;
    z[34]=z[33]*z[32];
    z[34]=z[16] + z[34];
    z[34]=z[8]*z[34];
    z[35]=z[9] + z[7];
    z[36]=z[33]*z[35];
    z[36]=z[16] + z[36];
    z[36]=z[11]*z[36];
    z[37]=z[6] + z[5];
    z[37]=z[33]*z[37];
    z[37]=z[16] + z[37];
    z[37]=z[12]*z[37];
    z[32]= - z[33] + z[32];
    z[32]=z[13]*z[32];
    z[35]=z[33] - z[35];
    z[35]=z[15]*z[35];
    z[38]=z[33] - z[6];
    z[39]= - z[5] + z[38];
    z[39]=z[14]*z[39];
    z[40]=z[2] + z[4];
    z[40]=z[20]*z[40];
    z[41]= - z[7] - z[10];
    z[41]=z[22]*z[41];
    z[32]=z[34] + z[36] + z[40] + z[41] - z[26] + z[24] + z[37] + z[32]
    + z[39] + z[35];
    z[34]=n<T>(1,3)*z[33];
    z[35]=z[34] + z[6];
    z[36]=n<T>(1,6)*z[3];
    z[37]=n<T>(2,3)*z[2];
    z[39]=z[36] + z[37] - z[35];
    z[39]=z[3]*z[39];
    z[40]=z[34] - z[6];
    z[40]=n<T>(1,2)*z[40];
    z[36]=z[36] - z[40] - z[37];
    z[36]=z[4]*z[36];
    z[41]=n<T>(1,2)*z[6];
    z[42]=z[41] + z[33];
    z[43]=z[42]*z[6];
    z[44]=n<T>(1,3)*z[2];
    z[45]=2*z[33];
    z[46]= - z[45] - z[2];
    z[46]=z[46]*z[44];
    z[36]=z[36] + z[39] + z[43] + z[46];
    z[36]=z[4]*z[36];
    z[39]=z[41] - z[33];
    z[39]=z[39]*z[6];
    z[45]=z[45] - z[2];
    z[45]=z[45]*z[44];
    z[37]=z[41] - z[37];
    z[37]=z[3]*z[37];
    z[37]=z[37] + z[39] + z[45];
    z[37]=z[3]*z[37];
    z[45]=n<T>(1,3)*z[7];
    z[46]=n<T>(1,6)*z[9];
    z[40]= - n<T>(1,3)*z[10] + z[46] - z[40] - z[45];
    z[40]=z[10]*z[40];
    z[35]=z[46] - z[35] + n<T>(2,3)*z[7];
    z[35]=z[9]*z[35];
    z[46]=n<T>(2,3)*z[33];
    z[47]= - z[7]*z[46];
    z[35]=z[40] + z[35] + z[43] + z[47];
    z[35]=z[10]*z[35];
    z[34]= - n<T>(1,3)*z[9] - z[45] + z[34] + z[41];
    z[34]=z[9]*z[34];
    z[34]=z[39] + z[34];
    z[34]=z[9]*z[34];
    z[39]=2*z[2];
    z[40]=z[39] - z[33] + 2*z[6];
    z[40]=z[40]*z[44];
    z[41]= - z[33] - z[2];
    z[41]=z[7]*z[41];
    z[38]= - z[5] + n<T>(1,2)*z[2] + z[38];
    z[38]=n<T>(1,2)*z[7] + n<T>(1,3)*z[38];
    z[38]=z[5]*z[38];
    z[38]=z[38] + z[40] + z[41];
    z[38]=z[5]*z[38];
    z[40]=z[4] + z[7];
    z[39]=n<T>(1,2)*z[3] - n<T>(25,2)*z[6] - z[39] - 2*z[40];
    z[40]=z[5] + z[9];
    z[39]= - n<T>(2,3)*z[10] + n<T>(1,3)*z[39] + n<T>(1,2)*z[40];
    z[39]=z[16]*z[39];
    z[40]=z[6] + z[2];
    z[40]=z[21]*z[40];
    z[39]=z[39] + z[40];
    z[40]=npow(z[6],2);
    z[41]= - z[42]*z[40];
    z[42]=z[2] - z[6];
    z[42]= - z[33] + n<T>(5,6)*z[42];
    z[42]=z[2]*z[42];
    z[43]= - z[46] - z[6];
    z[43]=z[6]*z[43];
    z[42]=z[43] + z[42];
    z[42]=z[2]*z[42];
    z[33]=z[33] + z[6];
    z[33]=z[2]*z[33];
    z[33]= - n<T>(1,2)*z[40] + z[33];
    z[33]=z[7]*z[33];
    z[40]=z[31] - z[30] + z[28] + z[25];
    z[43]=z[29] - z[27];
    z[44]= - z[6] - z[7];
    z[44]=z[23]*z[44];
    z[45]=z[17]*i;

    r +=  - 2*z[18] + 7*z[19] + n<T>(2,3)*z[32] + z[33] + z[34] + z[35] + 
      z[36] + z[37] + z[38] + n<T>(1,3)*z[39] + n<T>(1,2)*z[40] + z[41] + z[42]
       + n<T>(1,6)*z[43] + z[44] - n<T>(1,18)*z[45];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf239(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf239(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
