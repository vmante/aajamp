#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf88(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[44];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[8];
    z[5]=d[15];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[7];
    z[9]=d[6];
    z[10]=d[17];
    z[11]=e[2];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=d[2];
    z[15]=e[3];
    z[16]=e[10];
    z[17]=e[6];
    z[18]=e[7];
    z[19]=f[4];
    z[20]=f[10];
    z[21]=f[12];
    z[22]=f[14];
    z[23]=f[16];
    z[24]=f[31];
    z[25]=f[36];
    z[26]=f[37];
    z[27]=f[39];
    z[28]=z[3] + z[2];
    z[29]=i*z[1];
    z[30]=z[29]*z[28];
    z[30]=z[13] + z[30];
    z[30]=z[5]*z[30];
    z[31]=z[8] + z[9];
    z[32]= - z[29]*z[31];
    z[32]= - z[13] + z[32];
    z[32]=z[10]*z[32];
    z[28]= - z[29] + z[28];
    z[28]=z[11]*z[28];
    z[31]=z[29] - z[31];
    z[31]=z[12]*z[31];
    z[33]=z[2] + z[4];
    z[33]=z[16]*z[33];
    z[34]=z[9] + z[7];
    z[34]=z[18]*z[34];
    z[28]=z[28] + z[31] + z[33] + z[34] - z[20] + z[30] + z[32];
    z[30]=n<T>(1,3)*z[29];
    z[31]=z[30] + z[6];
    z[32]=n<T>(2,3)*z[2];
    z[33]=n<T>(1,6)*z[3];
    z[34]=z[33] + z[32] - z[31];
    z[34]=z[3]*z[34];
    z[30]=z[30] - z[6];
    z[30]=n<T>(1,2)*z[30];
    z[35]=z[33] - z[30] - z[32];
    z[35]=z[4]*z[35];
    z[36]=n<T>(1,2)*z[6];
    z[37]=z[36] + z[29];
    z[37]=z[37]*z[6];
    z[38]=n<T>(1,3)*z[2];
    z[39]=2*z[29];
    z[40]= - z[39] - z[2];
    z[40]=z[40]*z[38];
    z[34]=z[35] + z[34] + z[37] + z[40];
    z[34]=z[4]*z[34];
    z[35]=n<T>(1,6)*z[7];
    z[40]=n<T>(2,3)*z[9];
    z[31]= - z[35] - z[40] + z[31];
    z[31]=z[7]*z[31];
    z[35]= - z[35] - z[36] + z[40];
    z[35]=z[8]*z[35];
    z[41]=z[36] - z[29];
    z[41]=z[41]*z[6];
    z[42]=n<T>(1,3)*z[9];
    z[43]= - z[39] + z[9];
    z[43]=z[43]*z[42];
    z[31]=z[35] + z[31] - z[41] + z[43];
    z[31]=z[8]*z[31];
    z[35]=z[39] - z[2];
    z[35]=z[35]*z[38];
    z[32]=z[36] - z[32];
    z[32]=z[3]*z[32];
    z[32]=z[32] + z[41] + z[35];
    z[32]=z[3]*z[32];
    z[35]=z[29]*z[40];
    z[30]=n<T>(1,3)*z[7] + z[30] + z[42];
    z[30]=z[7]*z[30];
    z[30]=z[30] - z[37] + z[35];
    z[30]=z[7]*z[30];
    z[35]=z[6] + z[2];
    z[35]=z[15]*z[35];
    z[36]=z[6] + z[9];
    z[36]=z[17]*z[36];
    z[35]=z[35] + z[36];
    z[36]=z[39] + 5*z[2];
    z[36]=z[36]*z[38];
    z[37]=npow(z[6],2);
    z[36]= - z[37] + z[36];
    z[36]=z[2]*z[36];
    z[29]= - z[42] - n<T>(2,3)*z[29] - z[6];
    z[37]=npow(z[9],2);
    z[29]=z[29]*z[37];
    z[38]=z[7] - z[4];
    z[33]= - n<T>(1,6)*z[8] + z[33] + z[2] - z[9] + n<T>(2,3)*z[38];
    z[33]=z[13]*z[33];
    z[38]=npow(z[2],2);
    z[37]= - z[38] + z[37];
    z[37]=z[14]*z[37];
    z[38]= - z[27] + z[25] + z[23] - z[21];
    z[39]=z[24] - z[19];
    z[40]=npow(z[6],3);

    r +=  - z[22] + z[26] + n<T>(2,3)*z[28] + z[29] + z[30] + z[31] + z[32]
       + n<T>(1,3)*z[33] + z[34] + 2*z[35] + z[36] + z[37] + n<T>(1,2)*z[38] - 
      n<T>(1,6)*z[39] + z[40];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf88(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf88(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
