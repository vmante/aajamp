#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf157(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[44];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[16];
    z[8]=e[0];
    z[9]=e[4];
    z[10]=c[3];
    z[11]=d[5];
    z[12]=c[11];
    z[13]=c[15];
    z[14]=c[18];
    z[15]=c[19];
    z[16]=c[21];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[29];
    z[22]=c[31];
    z[23]=e[1];
    z[24]=f[0];
    z[25]=f[1];
    z[26]=f[2];
    z[27]=f[3];
    z[28]=f[13];
    z[29]=f[18];
    z[30]=f[20];
    z[31]=f[21];
    z[32]=z[3] - z[5];
    z[33]= - z[1]*z[32];
    z[34]=z[1]*z[7];
    z[35]=z[6]*z[1];
    z[34]= - n<T>(4,3)*z[35] + z[34] + n<T>(2,3)*z[33];
    z[34]=i*z[34];
    z[36]= - z[9] + n<T>(2,3)*z[8];
    z[34]=z[34] - n<T>(5,9)*z[10] - n<T>(2,3)*z[23] + z[36];
    z[37]= - z[32] + 2*z[6];
    z[38]=n<T>(2,3)*z[6];
    z[37]=z[37]*z[38];
    z[39]=n<T>(1,3)*z[5];
    z[40]= - z[11] + 2*z[5];
    z[39]=z[39]*z[40];
    z[41]=n<T>(1,3)*z[3];
    z[42]=z[41] + z[5];
    z[42]=z[42]*z[3];
    z[37]= - z[37] + z[39] - z[42];
    z[39]=i*z[1];
    z[42]= - z[11] + 4*z[5];
    z[38]= - n<T>(2,3)*z[2] - n<T>(4,3)*z[39] - z[38] + n<T>(1,3)*z[42] - z[3];
    z[38]=z[2]*z[38];
    z[39]=z[39] - z[6];
    z[42]=2*z[2];
    z[32]= - n<T>(5,3)*z[4] + z[42] + 2*z[32] - z[39];
    z[32]=z[4]*z[32];
    z[32]=n<T>(1,3)*z[32] + z[38] - z[37] + 2*z[34];
    z[32]=z[4]*z[32];
    z[34]=2*z[25] + z[26] - z[27] - z[28] - z[29] + z[31] - n<T>(2,3)*z[30];
    z[34]=n<T>(1,3)*z[34] + z[24];
    z[38]=z[8] + z[23];
    z[43]=z[11]*z[38];
    z[32]=z[32] + n<T>(1,3)*z[43] - n<T>(20,9)*z[13] - n<T>(8,9)*z[14] + n<T>(7,9)*z[15]
    + n<T>(1,9)*z[18] - n<T>(4,3)*z[20] + n<T>(2,3)*z[21] + 2*z[34] - z[22];
    z[33]= - 2*z[33] + z[35];
    z[33]=i*z[33];
    z[34]=n<T>(5,3)*z[2] + z[3] - z[40] - z[39];
    z[34]=z[2]*z[34];
    z[33]=n<T>(1,3)*z[34] + n<T>(2,3)*z[33] + n<T>(4,3)*z[38] - z[10] + z[37];
    z[33]=z[33]*z[42];
    z[34]=4*z[7];
    z[35]=z[35]*z[34];
    z[37]= - n<T>(4,3)*z[16] + 4*z[19] + z[17];
    z[38]=z[9] - n<T>(4,3)*z[8];
    z[38]=z[1]*z[38];
    z[35]=z[35] + 4*z[38] + 2*z[37] - n<T>(7,9)*z[12];
    z[35]=i*z[35];
    z[37]=5*z[23] + z[8];
    z[37]=2*z[37] - n<T>(5,3)*z[10];
    z[37]=z[37]*z[41];
    z[38]=npow(z[6],2);
    z[36]= - n<T>(4,3)*z[38] + 4*z[36] + n<T>(1,3)*z[10];
    z[36]=z[6]*z[36];
    z[34]= - n<T>(2,9)*z[5] + z[34] - n<T>(1,9)*z[11];
    z[34]=z[10]*z[34];

    r += 2*z[32] + z[33] + z[34] + z[35] + z[36] + z[37];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf157(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf157(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
