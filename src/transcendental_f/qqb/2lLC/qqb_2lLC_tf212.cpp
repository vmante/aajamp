#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf212(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[16];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[8];
    z[4]=d[18];
    z[5]=c[3];
    z[6]=c[11];
    z[7]=c[15];
    z[8]=c[31];
    z[9]=d[7];
    z[10]=e[10];
    z[11]=e[11];
    z[12]=f[6];
    z[13]=i*z[1];
    z[14]=z[13]*z[4];
    z[15]=npow(z[9],2);
    z[14]= - z[14] + n<T>(1,2)*z[15] - n<T>(1,6)*z[5] + z[10] + z[11];
    z[15]=z[2] + z[3];
    z[13]=z[13] - z[9];
    z[15]= - z[13] - n<T>(1,2)*z[15];
    z[15]=z[2]*z[15];
    z[13]= - z[3]*z[13];
    z[13]=z[15] + z[13] - z[14];
    z[13]=z[2]*z[13];
    z[14]= - z[3]*z[14];
    z[15]=i*z[6];

    r +=  - z[7] + n<T>(3,2)*z[8] - z[12] + z[13] + z[14] + n<T>(1,6)*z[15];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf212(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf212(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
