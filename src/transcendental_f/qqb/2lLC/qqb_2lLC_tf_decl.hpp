#ifndef QQB_2LLC_TF_DECL_H
#define QQB_2LLC_TF_DECL_H

#include <array>
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
std::complex<T> qqb_2lLC_tf1(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf2(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf3(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf4(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf5(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf6(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf7(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf8(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf9(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf10(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf11(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf12(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf13(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf14(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf15(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf16(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf17(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf18(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf19(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf20(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf21(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf22(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf23(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf24(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf25(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf26(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf27(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf28(
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf29(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf30(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf31(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf32(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf33(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf34(
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf35(
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf36(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf37(
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf38(
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf39(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf40(
  const std::array<T,85>&);

template <class T>
std::complex<T> qqb_2lLC_tf41(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf42(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf43(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf44(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf45(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf46(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf47(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf48(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf49(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf50(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf51(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf52(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf53(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf54(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf55(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf56(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf57(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf58(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf59(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf60(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf61(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf62(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf63(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf64(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf65(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf66(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf67(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf68(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf69(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf70(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf71(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf72(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf73(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf74(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf75(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf76(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf77(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf78(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf79(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf80(
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf81(
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf82(
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf83(
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf84(
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf85(
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf86(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf87(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf88(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf89(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf90(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf91(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf92(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf93(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lLC_tf94(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf95(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf96(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf97(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf98(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf99(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf100(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf101(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf102(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf103(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf104(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf105(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf106(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf107(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf108(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf109(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf110(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf111(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf112(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf113(
  const std::array<T,85>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf114(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf115(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf116(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf117(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf118(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf119(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf120(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf121(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf122(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf123(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf124(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf125(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf126(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf127(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf128(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf129(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf130(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf131(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf132(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf133(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf134(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf135(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf136(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf137(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf138(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf139(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf140(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf141(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf142(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf143(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf144(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf145(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf146(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf147(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf148(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf149(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf150(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf151(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf152(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf153(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf154(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf155(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf156(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf157(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf158(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf159(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf160(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf161(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf162(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf163(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf164(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf165(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf166(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf167(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf168(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf169(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf170(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf171(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf172(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf173(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf174(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf175(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf176(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf177(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf178(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf179(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf180(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf181(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf182(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf183(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf184(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf185(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf186(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf187(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf188(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf189(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf190(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf191(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf192(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf193(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf194(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf195(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf196(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf197(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf198(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf199(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf200(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf201(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf202(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf203(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf204(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf205(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf206(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf207(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf208(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf209(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf210(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf211(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf212(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf213(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf214(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf215(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf216(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf217(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf218(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf219(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf220(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf221(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf222(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf223(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf224(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf225(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf226(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf227(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf228(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf229(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf230(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf231(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf232(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf233(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf234(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf235(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&);

template <class T>
std::complex<T> qqb_2lLC_tf236(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf237(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf238(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf239(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf240(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf241(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf242(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf243(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf244(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf245(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf246(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf247(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf248(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf249(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf250(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf251(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf252(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf253(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf254(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&);

template <class T>
std::complex<T> qqb_2lLC_tf255(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf256(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf257(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf258(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf259(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf260(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf261(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf262(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf263(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf264(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf265(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf266(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf267(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf268(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf269(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf270(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf271(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf272(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf273(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf274(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf275(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf276(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf277(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf278(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf279(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf280(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf281(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf282(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf283(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf284(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf285(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf286(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf287(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf288(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf289(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf290(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf291(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf292(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf293(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf294(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf295(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf296(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

template <class T>
std::complex<T> qqb_2lLC_tf297(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&);

#endif /* QQB_2LLC_TF_DECL_H */
