#include "qqb_2lLC_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lLC_tf115(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[39];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=e[6];
    z[17]=f[30];
    z[18]=f[31];
    z[19]=f[32];
    z[20]=f[33];
    z[21]=f[35];
    z[22]=f[36];
    z[23]=f[39];
    z[24]=f[43];
    z[25]=n<T>(1,2)*z[8];
    z[26]=z[25] - z[3];
    z[27]=z[3] - z[4];
    z[26]=z[27]*z[26];
    z[28]=z[1]*i;
    z[29]=z[4] + z[3];
    z[29]=z[29]*z[28];
    z[30]=n<T>(1,2)*z[5];
    z[31]=z[28] - z[5];
    z[32]= - z[4] - z[31];
    z[32]=z[32]*z[30];
    z[33]=npow(z[4],2);
    z[34]=n<T>(1,2)*z[33];
    z[26]=z[32] + z[29] - z[34] + z[26];
    z[26]=z[5]*z[26];
    z[29]=n<T>(1,2)*z[28];
    z[30]=n<T>(1,4)*z[2] - z[30] - z[25] + z[29];
    z[27]=z[27]*z[30];
    z[30]=n<T>(1,2)*z[4];
    z[32]= - z[30] + z[3];
    z[32]=z[3]*z[32];
    z[27]= - z[34] + z[32] + z[27];
    z[27]=z[2]*z[27];
    z[32]= - n<T>(1,3)*z[7] + n<T>(1,6)*z[8];
    z[30]= - n<T>(1,4)*z[5] + z[30] - n<T>(5,3)*z[3] + z[32];
    z[30]=z[13]*z[30];
    z[26]=z[30] + z[26] + z[27];
    z[27]=n<T>(1,3)*z[33];
    z[30]=npow(z[3],2);
    z[34]= - z[27] + n<T>(1,2)*z[30];
    z[35]=z[4] - n<T>(5,2)*z[3];
    z[35]=z[35]*z[28];
    z[36]= - z[4] + n<T>(5,6)*z[3];
    z[25]=z[36]*z[25];
    z[25]=z[25] + n<T>(1,2)*z[34] + n<T>(1,3)*z[35];
    z[25]=z[8]*z[25];
    z[34]=n<T>(1,3)*z[4];
    z[35]=n<T>(1,2)*z[3];
    z[36]=z[34] - z[35];
    z[37]=n<T>(1,6)*z[28];
    z[32]= - z[37] - z[36] + z[32];
    z[32]=z[7]*z[32];
    z[38]= - z[36]*z[28];
    z[36]=n<T>(1,12)*z[8] - z[37] + z[36];
    z[36]=z[8]*z[36];
    z[30]=n<T>(1,2)*z[32] + z[36] + n<T>(1,4)*z[30] + z[38];
    z[30]=z[7]*z[30];
    z[32]=z[4] - n<T>(23,9)*z[3];
    z[32]=z[32]*z[35];
    z[32]=z[33] + z[32];
    z[32]=z[32]*z[35];
    z[33]= - z[34] - z[35];
    z[33]=z[3]*z[33];
    z[27]= - z[27] + z[33];
    z[27]=z[27]*z[29];
    z[29]=npow(z[4],3);
    z[25]=z[30] + z[25] + z[27] - n<T>(1,9)*z[29] + z[32] + n<T>(1,3)*z[26];
    z[26]=z[5] + z[3];
    z[26]= - z[28]*z[26];
    z[26]= - z[13] + z[26];
    z[26]=z[6]*z[26];
    z[27]=z[3] - z[31];
    z[27]=z[10]*z[27];
    z[29]= - z[4] - z[7];
    z[29]=z[16]*z[29];
    z[26]= - z[15] + z[29] + z[26] + z[27] + z[19] + z[17];
    z[27]=z[8] + z[4];
    z[29]=z[28]*z[27];
    z[29]=z[13] + z[29];
    z[29]=z[9]*z[29];
    z[27]= - z[28] + z[27];
    z[27]=z[12]*z[27];
    z[27]=z[29] + z[27];
    z[28]= - z[8] + z[31] + z[2];
    z[29]= - n<T>(7,9)*z[4] - z[3];
    z[28]=n<T>(1,2)*z[29] + n<T>(1,9)*z[28];
    z[28]=z[11]*z[28];
    z[29]=z[24] - z[21];
    z[30]=z[14]*i;
    z[30]=z[30] - z[22];

    r += n<T>(7,36)*z[18] + n<T>(4,9)*z[20] + n<T>(1,12)*z[23] + n<T>(1,3)*z[25]
     + n<T>(1,9)
      *z[26] + n<T>(2,9)*z[27] + z[28] - n<T>(1,18)*z[29] + n<T>(1,36)*z[30];
 
    return r;
}

template std::complex<double> qqb_2lLC_tf115(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lLC_tf115(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
