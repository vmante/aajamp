#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf425(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[111];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[18];
    z[33]=c[19];
    z[34]=c[21];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[29];
    z[40]=c[31];
    z[41]=e[3];
    z[42]=e[6];
    z[43]=e[13];
    z[44]=e[14];
    z[45]=f[0];
    z[46]=f[1];
    z[47]=f[2];
    z[48]=f[3];
    z[49]=f[4];
    z[50]=f[5];
    z[51]=f[6];
    z[52]=f[7];
    z[53]=f[8];
    z[54]=f[10];
    z[55]=f[11];
    z[56]=f[12];
    z[57]=f[13];
    z[58]=f[14];
    z[59]=f[16];
    z[60]=f[17];
    z[61]=f[18];
    z[62]=f[21];
    z[63]=f[23];
    z[64]=f[30];
    z[65]=f[31];
    z[66]=f[32];
    z[67]=f[33];
    z[68]=f[34];
    z[69]=f[35];
    z[70]=f[36];
    z[71]=f[37];
    z[72]=f[39];
    z[73]=f[41];
    z[74]=f[43];
    z[75]=f[50];
    z[76]=f[51];
    z[77]=f[52];
    z[78]=f[53];
    z[79]=f[54];
    z[80]=f[55];
    z[81]=f[56];
    z[82]=f[58];
    z[83]=f[60];
    z[84]=f[85];
    z[85]=f[86];
    z[86]=n<T>(1,2)*z[5];
    z[87]=z[1]*i;
    z[88]=z[86] - z[87];
    z[89]=z[88]*z[5];
    z[90]=z[87] - z[5];
    z[91]=n<T>(1,2)*z[2];
    z[92]= - z[91] - z[90];
    z[92]=z[2]*z[92];
    z[92]=n<T>(59,2)*z[89] + z[92];
    z[93]=2*z[87];
    z[94]=n<T>(1,2)*z[9];
    z[95]=z[2] - z[5];
    z[96]= - z[94] - z[93] + z[95];
    z[97]=n<T>(1,3)*z[9];
    z[96]=z[96]*z[97];
    z[98]=n<T>(1,2)*z[4];
    z[99]=z[98] - z[87];
    z[100]=z[4]*z[99];
    z[101]=n<T>(1,2)*z[7];
    z[102]=z[87] - z[4];
    z[103]= - z[101] - z[102];
    z[103]=z[7]*z[103];
    z[104]= - n<T>(149,4)*z[87] + 59*z[5];
    z[104]=n<T>(1,4)*z[104] - z[2];
    z[97]=n<T>(245,72)*z[10] + n<T>(65,4)*z[6] + z[97] - n<T>(95,16)*z[7] + n<T>(1,3)*
    z[104] - n<T>(29,16)*z[4];
    z[97]=z[10]*z[97];
    z[104]=n<T>(1,2)*z[6] + z[90];
    z[104]=z[6]*z[104];
    z[92]=z[97] + n<T>(65,2)*z[104] + z[96] + n<T>(95,8)*z[103] + n<T>(1,3)*z[92]
     - 
   n<T>(29,8)*z[100];
    z[92]=z[10]*z[92];
    z[96]=13*z[87] - z[86];
    z[96]=z[96]*z[86];
    z[97]= - n<T>(7,2)*z[2] + 4*z[87] - z[86];
    z[97]=z[2]*z[97];
    z[100]=2*z[2];
    z[103]= - n<T>(373,48)*z[4] + z[100] + n<T>(229,24)*z[87] + 4*z[5];
    z[103]=z[4]*z[103];
    z[104]= - n<T>(281,12)*z[87] - 35*z[5];
    z[104]= - n<T>(143,24)*z[7] - n<T>(157,48)*z[4] + n<T>(1,4)*z[104] + 8*z[2];
    z[104]=z[7]*z[104];
    z[96]=z[104] + z[103] + z[96] + z[97];
    z[96]=z[7]*z[96];
    z[97]=z[9] + z[10];
    z[97]=z[87]*z[97];
    z[97]=z[29] + z[97];
    z[97]=z[17]*z[97];
    z[103]= - n<T>(49,6)*z[10] + 19*z[8] - n<T>(289,18)*z[6] - n<T>(713,36)*z[9]
     - 
   n<T>(187,9)*z[7] + n<T>(13333,72)*z[4] - n<T>(263,9)*z[5] - 113*z[2];
    z[103]= - n<T>(343,72)*z[11] + n<T>(1,2)*z[103] + n<T>(98,9)*z[3];
    z[103]=z[29]*z[103];
    z[104]=883*z[87];
    z[105]=n<T>(895,2)*z[4] - z[104] + n<T>(871,2)*z[2];
    z[105]=n<T>(1,3)*z[105] - 2*z[6];
    z[105]=z[18]*z[105];
    z[106]=z[87] - z[9];
    z[107]= - z[10] + z[106];
    z[107]=z[27]*z[107];
    z[92]=z[105] + z[97] + z[107] + z[83] + z[39] + z[103] + z[96] + 
    z[92];
    z[96]=z[4] - z[2];
    z[97]=n<T>(25,8)*z[7];
    z[103]= - n<T>(11,2)*z[87] - 5*z[5];
    z[103]=n<T>(13,4)*z[6] + n<T>(49,24)*z[9] + z[97] + n<T>(7,36)*z[103] - 10*z[96]
   ;
    z[103]=z[6]*z[103];
    z[105]=2*z[5];
    z[107]=z[87] + z[105];
    z[107]=n<T>(1,3)*z[107] - z[91];
    z[107]=z[2]*z[107];
    z[108]=28*z[87] - z[5];
    z[108]=n<T>(2,3)*z[108] + z[98];
    z[108]=z[4]*z[108];
    z[97]=z[97] + n<T>(33,4)*z[87] - z[4];
    z[97]=z[7]*z[97];
    z[109]=n<T>(49,6)*z[9] - n<T>(49,3)*z[87] - 29*z[7];
    z[109]=z[9]*z[109];
    z[97]=z[103] + n<T>(1,4)*z[109] + z[97] + z[108] - n<T>(35,18)*z[89] + 
    z[107];
    z[97]=z[6]*z[97];
    z[103]=n<T>(1,4)*z[5];
    z[107]= - 791*z[87] + n<T>(743,2)*z[5];
    z[107]=z[107]*z[103];
    z[108]= - n<T>(41,4)*z[2] + 50*z[87] + n<T>(41,2)*z[5];
    z[108]=z[2]*z[108];
    z[107]=z[107] + 13*z[108];
    z[99]= - z[5] + z[99];
    z[99]=z[4]*z[99];
    z[108]=n<T>(79,12)*z[7] - n<T>(91,6)*z[87] + z[95];
    z[101]=z[108]*z[101];
    z[108]=n<T>(1,9)*z[9];
    z[109]=n<T>(791,8)*z[5] - 325*z[2];
    z[109]= - z[108] + n<T>(91,24)*z[7] + n<T>(1,9)*z[109] + n<T>(201,8)*z[4];
    z[109]=z[9]*z[109];
    z[99]=z[109] + z[101] + n<T>(1,9)*z[107] + n<T>(201,4)*z[99];
    z[99]=z[9]*z[99];
    z[101]= - z[91] + 3*z[87] - z[5];
    z[101]=z[2]*z[101];
    z[89]= - 5*z[89] + z[101];
    z[101]=n<T>(2,3)*z[7];
    z[107]=z[101] - z[4] + z[91] - n<T>(8,3)*z[87] + n<T>(5,2)*z[5];
    z[107]=z[7]*z[107];
    z[109]=n<T>(23,3)*z[87] - z[95];
    z[101]= - n<T>(35,6)*z[9] + n<T>(1,2)*z[109] + z[101];
    z[101]=z[9]*z[101];
    z[109]= - n<T>(25,12)*z[6] + n<T>(19,6)*z[9] + z[7] - n<T>(25,6)*z[87] + z[4];
    z[109]=z[6]*z[109];
    z[110]= - n<T>(23,6)*z[9] + n<T>(17,2)*z[7] - z[4] - 5*z[2] - n<T>(1,3)*z[87]
     - 
   n<T>(11,2)*z[5];
    z[110]=n<T>(1,4)*z[8] + n<T>(1,2)*z[110] + z[6];
    z[110]=z[8]*z[110];
    z[89]=z[110] + z[109] + z[101] + n<T>(1,2)*z[89] + z[107];
    z[89]=z[8]*z[89];
    z[101]=n<T>(181,3)*z[87] - z[105];
    z[101]=z[5]*z[101];
    z[105]=n<T>(1,3)*z[2];
    z[107]=n<T>(325,2)*z[2] - 161*z[87] - 169*z[5];
    z[107]=z[107]*z[105];
    z[109]= - n<T>(40,3)*z[6] + 2*z[4] - n<T>(115,3)*z[90] - z[100];
    z[109]=z[6]*z[109];
    z[101]=z[109] + z[101] + z[107];
    z[107]=n<T>(1,3)*z[4];
    z[86]=n<T>(109,54)*z[3] - n<T>(34,9)*z[10] - n<T>(115,18)*z[6] - n<T>(1,18)*z[9]
     - 
    z[107] + n<T>(80,9)*z[2] - n<T>(77,9)*z[87] - z[86];
    z[86]=z[3]*z[86];
    z[105]=z[105] + 9*z[87] - n<T>(1,3)*z[5];
    z[105]=2*z[105] - 9*z[4];
    z[105]=z[4]*z[105];
    z[109]=z[95] + z[87];
    z[110]=z[94] - z[109];
    z[108]=z[110]*z[108];
    z[110]= - n<T>(127,2)*z[10] + z[9] - 68*z[90] - z[2];
    z[110]=z[10]*z[110];
    z[86]=z[86] + n<T>(1,9)*z[110] + z[108] + z[105] + n<T>(1,3)*z[101];
    z[86]=z[3]*z[86];
    z[95]=z[87] - z[95];
    z[95]=z[95]*z[100];
    z[100]=z[107] + n<T>(2,3)*z[87] - z[2];
    z[100]=2*z[100] - n<T>(11,24)*z[7];
    z[100]=z[7]*z[100];
    z[94]= - z[87] + z[94];
    z[94]=z[9]*z[94];
    z[93]= - z[93] - z[5];
    z[93]=2*z[93] + n<T>(115,24)*z[4];
    z[93]=z[4]*z[93];
    z[93]=z[94] + z[100] + z[95] + z[93];
    z[94]=n<T>(1,2)*z[8];
    z[95]=z[94] - z[6] - z[7] - z[102];
    z[95]=z[8]*z[95];
    z[94]=n<T>(3,8)*z[11] + n<T>(1,6)*z[10] + z[94] - n<T>(19,8)*z[7] + z[4] - n<T>(7,6)
   *z[2] - n<T>(4,3)*z[87] + n<T>(3,2)*z[5];
    z[94]=z[11]*z[94];
    z[96]=z[7] - z[96];
    z[96]=z[6]*z[96];
    z[100]=n<T>(1,2)*z[10] + z[102];
    z[100]=z[10]*z[100];
    z[93]=z[94] + n<T>(11,4)*z[100] + z[95] + n<T>(1,3)*z[93] + z[96];
    z[93]=z[11]*z[93];
    z[94]= - z[104] + n<T>(907,2)*z[5];
    z[94]=z[94]*z[103];
    z[95]= - n<T>(451,4)*z[2] + 679*z[87] + n<T>(463,2)*z[5];
    z[95]=z[2]*z[95];
    z[94]=z[94] + z[95];
    z[95]=1769*z[87] + 859*z[5];
    z[95]=n<T>(1,4)*z[95] - 685*z[2];
    z[95]=n<T>(1,9)*z[95] - n<T>(9,4)*z[4];
    z[95]=z[95]*z[98];
    z[94]=n<T>(1,9)*z[94] + z[95];
    z[94]=z[4]*z[94];
    z[95]=z[8] - z[7];
    z[96]=3*z[5];
    z[95]=n<T>(767,18)*z[9] + n<T>(713,18)*z[2] - n<T>(767,9)*z[87] + z[96] + 3*
    z[95];
    z[95]=z[24]*z[95];
    z[98]=z[5] + z[3];
    z[98]=z[87]*z[98];
    z[98]=z[29] + z[98];
    z[98]=z[12]*z[98];
    z[100]=z[3] - z[90];
    z[100]=z[20]*z[100];
    z[98]= - z[79] + z[98] + z[100];
    z[100]=z[9] + z[8];
    z[100]=z[87]*z[100];
    z[100]=z[29] + z[100];
    z[100]=z[16]*z[100];
    z[101]=z[8] - z[106];
    z[101]=z[25]*z[101];
    z[100]=z[100] + z[101];
    z[101]= - z[5] - z[7];
    z[101]=z[87]*z[101];
    z[101]= - z[29] + z[101];
    z[101]=z[15]*z[101];
    z[90]=z[7] - z[90];
    z[90]=z[22]*z[90];
    z[90]=z[101] + z[90];
    z[101]= - z[4] - z[6];
    z[101]=z[87]*z[101];
    z[101]= - z[29] + z[101];
    z[101]=z[14]*z[101];
    z[103]=z[6] - z[102];
    z[103]=z[21]*z[103];
    z[101]=z[101] + z[103];
    z[103]= - z[4] - z[11];
    z[103]=z[87]*z[103];
    z[103]= - z[29] + z[103];
    z[103]=z[13]*z[103];
    z[102]=z[11] - z[102];
    z[102]=z[28]*z[102];
    z[102]=z[103] + z[102];
    z[103]=z[109] - z[9];
    z[103]=2*z[103];
    z[104]=61*z[3] + 59*z[10] + z[103];
    z[104]=z[26]*z[104];
    z[104]=z[104] + z[63];
    z[96]= - n<T>(11,3)*z[87] + z[96];
    z[96]=z[96]*npow(z[5],2);
    z[96]=z[84] + z[96] - z[85];
    z[105]=167*z[87] - n<T>(275,2)*z[5];
    z[105]=z[5]*z[105];
    z[87]=n<T>(539,12)*z[2] - n<T>(355,4)*z[87] - n<T>(56,3)*z[5];
    z[87]=z[2]*z[87];
    z[87]=n<T>(1,6)*z[105] + z[87];
    z[87]=z[2]*z[87];
    z[103]= - n<T>(67,6)*z[8] - n<T>(79,6)*z[7] + z[103];
    z[103]=z[23]*z[103];
    z[88]=z[91] + z[88];
    z[88]=z[19]*z[88];
    z[91]=z[78] + z[77];
    z[105]=z[81] + z[51];
    z[106]=n<T>(545,3)*z[37] + n<T>(985,12)*z[35] + n<T>(40,3)*z[34] + n<T>(907,144)*
    z[30];
    z[106]=i*z[106];
    z[107]= - z[6] - z[3];
    z[107]=z[41]*z[107];
    z[108]= - z[6] - z[8];
    z[108]=z[42]*z[108];
    z[109]=107*z[7] + n<T>(187,2)*z[11];
    z[109]=z[43]*z[109];
    z[110]=z[10] + z[11];
    z[110]=z[44]*z[110];

    r += n<T>(505,36)*z[31] - n<T>(4,9)*z[32] + n<T>(49,4)*z[33] - n<T>(179,4)*z[36]
     - 
      n<T>(563,6)*z[38] + n<T>(137,6)*z[40] + n<T>(65,3)*z[45] - 10*z[46] + n<T>(139,9)
      *z[47] + n<T>(787,72)*z[48] + n<T>(55,36)*z[49] + n<T>(419,18)*z[50] - n<T>(8,9)*
      z[52] + 12*z[53] - n<T>(67,9)*z[54] + n<T>(827,72)*z[55] + n<T>(35,36)*z[56]
       + n<T>(475,18)*z[57] + n<T>(25,3)*z[58] + n<T>(65,12)*z[59] + n<T>(217,8)*z[60]
       + n<T>(19,3)*z[61] - n<T>(52,3)*z[62] + n<T>(19,6)*z[64] + n<T>(139,24)*z[65]
     +  n<T>(21,4)*z[66] + 8*z[67] - n<T>(13,12)*z[68] - n<T>(7,6)*z[69] - n<T>(49,24)*
      z[70] - z[71] + n<T>(25,8)*z[72] + z[73] - n<T>(5,2)*z[74] - n<T>(11,4)*z[75]
       - n<T>(85,144)*z[76] + n<T>(29,48)*z[80] - n<T>(95,48)*z[82] + z[86] + z[87]
       + n<T>(37,3)*z[88] + z[89] + n<T>(8,3)*z[90] - n<T>(3,2)*z[91] + n<T>(1,3)*z[92]
       + z[93] + z[94] + z[95] + n<T>(1,2)*z[96] + z[97] + n<T>(5,9)*z[98] + 
      z[99] + n<T>(47,6)*z[100] + 19*z[101] + n<T>(43,36)*z[102] + z[103] + n<T>(1,9)*z[104]
     - n<T>(1,6)*z[105]
     + z[106]
     + n<T>(35,9)*z[107]
     + n<T>(37,6)*z[108]
       + n<T>(1,18)*z[109] + n<T>(29,12)*z[110];
 
    return r;
}

template std::complex<double> qqb_2lha_tf425(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf425(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
