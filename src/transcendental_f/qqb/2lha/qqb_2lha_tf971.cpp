#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf971(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[135];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[22];
    z[3]=f[69];
    z[4]=f[70];
    z[5]=f[77];
    z[6]=f[79];
    z[7]=f[81];
    z[8]=c[11];
    z[9]=d[0];
    z[10]=d[1];
    z[11]=d[2];
    z[12]=d[3];
    z[13]=d[4];
    z[14]=d[5];
    z[15]=d[6];
    z[16]=d[7];
    z[17]=d[8];
    z[18]=d[9];
    z[19]=c[12];
    z[20]=c[13];
    z[21]=c[20];
    z[22]=c[23];
    z[23]=c[32];
    z[24]=c[33];
    z[25]=c[35];
    z[26]=c[36];
    z[27]=c[37];
    z[28]=c[38];
    z[29]=c[39];
    z[30]=c[40];
    z[31]=c[43];
    z[32]=c[44];
    z[33]=c[47];
    z[34]=c[48];
    z[35]=c[49];
    z[36]=c[50];
    z[37]=c[55];
    z[38]=c[56];
    z[39]=c[57];
    z[40]=c[59];
    z[41]=c[81];
    z[42]=c[84];
    z[43]=f[24];
    z[44]=f[42];
    z[45]=f[44];
    z[46]=f[61];
    z[47]=f[62];
    z[48]=f[80];
    z[49]=g[24];
    z[50]=g[27];
    z[51]=g[38];
    z[52]=g[40];
    z[53]=g[48];
    z[54]=g[50];
    z[55]=g[79];
    z[56]=g[82];
    z[57]=g[90];
    z[58]=g[92];
    z[59]=g[95];
    z[60]=g[99];
    z[61]=g[101];
    z[62]=g[110];
    z[63]=g[112];
    z[64]=g[113];
    z[65]=g[128];
    z[66]=g[131];
    z[67]=g[138];
    z[68]=g[140];
    z[69]=g[145];
    z[70]=g[147];
    z[71]=g[170];
    z[72]=g[173];
    z[73]=g[181];
    z[74]=g[183];
    z[75]=g[189];
    z[76]=g[191];
    z[77]=g[198];
    z[78]=g[200];
    z[79]=g[201];
    z[80]=g[212];
    z[81]=g[214];
    z[82]=g[218];
    z[83]=g[220];
    z[84]=g[224];
    z[85]=g[232];
    z[86]=g[233];
    z[87]=g[234];
    z[88]=g[243];
    z[89]=g[245];
    z[90]=g[251];
    z[91]=g[254];
    z[92]=g[270];
    z[93]=g[272];
    z[94]=g[276];
    z[95]=g[278];
    z[96]=g[281];
    z[97]=g[288];
    z[98]=g[289];
    z[99]=g[290];
    z[100]=g[292];
    z[101]=g[294];
    z[102]=g[308];
    z[103]=g[312];
    z[104]=g[324];
    z[105]=g[329];
    z[106]=g[336];
    z[107]=g[337];
    z[108]=g[338];
    z[109]=g[359];
    z[110]=g[362];
    z[111]=g[365];
    z[112]=g[367];
    z[113]=g[368];
    z[114]=z[7] + z[4];
    z[115]=n<T>(9,8)*z[6];
    z[116]=n<T>(1,9)*z[5];
    z[117]=n<T>(1,16)*z[2] - n<T>(1,3)*z[3];
    z[118]=z[8]*i;
    z[114]= - z[116] - n<T>(9,4)*z[19] + n<T>(1,12)*z[118] + n<T>(1,3)*z[117] - 
    z[115] + n<T>(13,144)*z[114];
    z[114]=z[16]*z[114];
    z[117]=7*z[12] - n<T>(25,9)*z[10] - n<T>(47,9)*z[9] - z[14];
    z[117]=n<T>(47,18)*z[15] + n<T>(1,2)*z[117] - n<T>(19,9)*z[13];
    z[117]=n<T>(25,144)*z[18] + n<T>(1,8)*z[117] - n<T>(1,9)*z[11];
    z[117]=z[43]*z[117];
    z[114]=z[114] + z[117];
    z[117]=z[9]*i;
    z[118]=z[14]*i;
    z[119]=z[10]*i;
    z[120]=z[12]*i;
    z[121]= - n<T>(29,9)*z[119] - n<T>(19,3)*z[120] - 7*z[117] + n<T>(131,9)*z[118];
    z[122]=z[13]*i;
    z[123]=z[15]*i;
    z[121]= - n<T>(17,9)*z[122] + n<T>(19,18)*z[123] + n<T>(1,2)*z[121];
    z[124]=z[11]*i;
    z[125]=z[17]*i;
    z[126]=z[18]*i;
    z[121]=z[124] + n<T>(1,3)*z[125] - n<T>(7,8)*z[126] + n<T>(1,4)*z[121];
    z[127]= - n<T>(1,32)*z[22] + n<T>(5,8)*z[21];
    z[121]=z[121]*z[127];
    z[127]=859*z[119] + 1033*z[117] + 539*z[118];
    z[127]=n<T>(1,9)*z[127] + 263*z[120];
    z[127]= - n<T>(3749,18)*z[123] + n<T>(1,2)*z[127] + 23*z[122];
    z[127]=n<T>(1513,2592)*z[126] - n<T>(151,108)*z[125] + n<T>(1,144)*z[127] - 
    z[124];
    z[127]=z[8]*z[127];
    z[128]=11*z[9];
    z[129]=n<T>(13,3)*z[10];
    z[130]= - z[129] + z[128] + n<T>(25,3)*z[14];
    z[131]=z[13] + z[12];
    z[130]=n<T>(1,3)*z[130] - z[131];
    z[130]= - n<T>(17,18)*z[17] + n<T>(1,2)*z[130] + n<T>(11,9)*z[11];
    z[130]=n<T>(1,8)*z[130] - n<T>(1,9)*z[18];
    z[130]=z[3]*z[130];
    z[132]=n<T>(19,4)*z[12] - n<T>(1,12)*z[10] - z[9] - n<T>(11,6)*z[14];
    z[132]=n<T>(1,3)*z[132] + n<T>(1,4)*z[13];
    z[132]= - n<T>(1,8)*z[18] - n<T>(17,72)*z[17] + n<T>(1,2)*z[132] - n<T>(1,9)*z[15];
    z[132]=z[2]*z[132];
    z[130]=n<T>(1,2)*z[132] + z[130];
    z[132]= - n<T>(5,2)*z[10] - n<T>(31,2)*z[9] - 83*z[14];
    z[131]=n<T>(167,32)*z[17] - n<T>(1,32)*z[11] + n<T>(67,16)*z[15] + n<T>(1,16)*z[132]
    + z[131];
    z[131]=z[6]*z[131];
    z[130]=n<T>(1,2)*z[130] + n<T>(1,9)*z[131];
    z[127]=5*z[130] + n<T>(1,6)*z[127];
    z[117]= - 19*z[120] - n<T>(29,3)*z[119] - 21*z[117] + n<T>(131,3)*z[118];
    z[117]=n<T>(19,6)*z[123] + n<T>(1,2)*z[117] - n<T>(17,3)*z[122];
    z[117]= - n<T>(21,8)*z[126] + z[125] + n<T>(1,4)*z[117] + 3*z[124];
    z[117]=z[20]*z[117];
    z[118]=z[9] + z[17] + z[15];
    z[119]=z[13] + z[14];
    z[120]=z[11] + z[12];
    z[118]= - n<T>(53,6)*z[120] + 11*z[119] - n<T>(13,6)*z[118];
    z[118]=z[7]*z[118];
    z[119]=z[12] + z[18] - z[17];
    z[120]=z[15] - z[14];
    z[122]=z[120] - z[11];
    z[119]= - 7*z[9] - n<T>(17,6)*z[122] + n<T>(25,6)*z[119];
    z[119]=z[48]*z[119];
    z[118]=z[118] + z[119] + z[107] + z[54];
    z[119]=z[12] - z[9];
    z[122]=n<T>(1,3)*z[10];
    z[120]= - z[122] + n<T>(1,3)*z[120] - 19*z[119];
    z[120]=n<T>(1,2)*z[120] + n<T>(5,3)*z[17];
    z[120]=n<T>(1,3)*z[120] - n<T>(1,2)*z[18];
    z[120]=z[44]*z[120];
    z[123]=z[12] - z[14];
    z[123]=11*z[10] + z[128] + n<T>(47,2)*z[123];
    z[123]= - n<T>(85,18)*z[11] + n<T>(1,9)*z[123] + n<T>(7,2)*z[13] - n<T>(1,3)*z[17];
    z[123]=z[5]*z[123];
    z[120]= - z[97] + z[123] + z[120] + z[77] + z[62];
    z[119]=z[119] + z[15];
    z[123]=n<T>(1,2)*z[13];
    z[119]=z[18] + z[123] - z[10] - z[14] + n<T>(1,2)*z[119];
    z[119]=z[46]*z[119];
    z[119]= - z[119] + z[111] + z[85] - z[75];
    z[124]= - 5*z[9] + n<T>(17,3)*z[14];
    z[122]=n<T>(1,2)*z[124] - z[122];
    z[122]=n<T>(7,6)*z[15] + 5*z[122] + z[123];
    z[122]=n<T>(1,24)*z[16] - n<T>(23,18)*z[18] + n<T>(7,72)*z[17] + n<T>(1,12)*z[122]
    + z[11];
    z[122]=z[47]*z[122];
    z[122]=z[122] - z[99] + z[79] + z[64];
    z[123]= - z[2] - n<T>(5,8)*z[3];
    z[115]=n<T>(1,3)*z[123] + n<T>(5,24)*z[4] + z[115];
    z[115]=n<T>(1,2)*z[115] + n<T>(53,144)*z[7] + z[116];
    z[115]=z[1]*i*z[115];
    z[116]=z[65] + z[113] - z[108] + z[86] + z[68];
    z[123]= - n<T>(5,6)*z[12] - n<T>(13,18)*z[10] + n<T>(13,9)*z[9] + 3*z[14];
    z[123]= - n<T>(3,2)*z[11] - n<T>(13,12)*z[15] + n<T>(1,2)*z[123] + n<T>(7,9)*z[13];
    z[123]=z[4]*z[123];
    z[123]= - z[98] + z[123] - z[112] + z[78] + z[63];
    z[124]=z[52] + z[49];
    z[125]=z[81] - z[80];
    z[126]=z[95] + z[92];
    z[128]=z[104] - z[103];
    z[130]=z[105] - z[73];
    z[131]=z[109] - z[59];
    z[132]=z[110] - z[106];
    z[133]=z[18] - z[9] + n<T>(11,8)*z[14] + z[11] + z[13];
    z[133]=n<T>(5,24)*z[17] + n<T>(19,24)*z[15] + n<T>(1,8)*z[10] - n<T>(1,3)*z[133];
    z[133]=z[45]*z[133];
    z[133]=z[133] - z[87];
    z[129]=n<T>(7,3)*z[13] - 17*z[12] - z[129] + z[9] - n<T>(107,3)*z[14];
    z[129]=n<T>(1,16)*z[129] + n<T>(8,3)*z[15];
    z[129]= - n<T>(5,48)*z[18] + n<T>(5,6)*z[17] + n<T>(1,3)*z[129] + n<T>(11,32)*z[11];
    z[129]=z[19]*z[129];
    z[129]=z[129] + z[26];
    z[134]=n<T>(10,9)*z[42] - n<T>(325,324)*z[41] - n<T>(437,6912)*z[40] + n<T>(5,12)*
    z[39] + n<T>(65,192)*z[38] - n<T>(10,3)*z[36] + n<T>(13,16)*z[35] + n<T>(7391,62208)
   *z[33] + n<T>(25,9)*z[32] - n<T>(545,2916)*z[29] - n<T>(755,324)*z[28] + n<T>(20,3)*
    z[27] - n<T>(5,648)*z[24];
    z[134]=i*z[134];

    r +=  - n<T>(23,432)*z[23] - z[25] - n<T>(25,18)*z[30] - n<T>(5,12)*z[31] - n<T>(25,144)*z[34]
     + n<T>(1,48)*z[37]
     + n<T>(235,576)*z[50] - n<T>(5,36)*z[51] - n<T>(185,1728)*z[53]
     + n<T>(65,288)*z[55] - n<T>(215,576)*z[56] - n<T>(35,192)*z[57]
       + n<T>(5,18)*z[58] + n<T>(85,864)*z[60] - n<T>(215,864)*z[61] + n<T>(125,192)*
      z[66] - n<T>(115,288)*z[67] + n<T>(5,288)*z[69] + n<T>(115,864)*z[70] + n<T>(25,72)*z[71]
     - n<T>(45,64)*z[72]
     + n<T>(85,288)*z[74] - n<T>(25,108)*z[76]
     + n<T>(15,32)*z[82]
     + n<T>(5,144)*z[83] - n<T>(115,576)*z[84]
     + n<T>(45,32)*z[88]
     + n<T>(55,288)*z[89] - n<T>(425,576)*z[90] - n<T>(15,64)*z[91] - n<T>(175,144)*z[93]
     +  n<T>(535,576)*z[94] - n<T>(35,288)*z[96] + n<T>(265,576)*z[100] - n<T>(65,576)*
      z[101] + n<T>(25,192)*z[102] + n<T>(5,8)*z[114] + n<T>(5,4)*z[115] - n<T>(5,48)*
      z[116] + n<T>(1,2)*z[117] + n<T>(5,192)*z[118] - n<T>(5,96)*z[119] + n<T>(5,64)*
      z[120] + z[121] + n<T>(5,16)*z[122] + n<T>(5,32)*z[123] + n<T>(5,576)*z[124]
       - n<T>(335,576)*z[125] - n<T>(25,288)*z[126] + n<T>(1,4)*z[127] - n<T>(35,64)*
      z[128] + n<T>(5,3)*z[129] + n<T>(55,96)*z[130] + n<T>(5,1152)*z[131] + n<T>(5,128)
      *z[132] + n<T>(5,24)*z[133] + z[134];
 
    return r;
}

template std::complex<double> qqb_2lha_tf971(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf971(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
