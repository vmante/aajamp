#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf958(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[114];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[18];
    z[33]=c[19];
    z[34]=c[21];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[29];
    z[40]=c[31];
    z[41]=e[3];
    z[42]=e[6];
    z[43]=e[13];
    z[44]=e[14];
    z[45]=f[0];
    z[46]=f[1];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[10];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[13];
    z[56]=f[14];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[18];
    z[60]=f[21];
    z[61]=f[23];
    z[62]=f[30];
    z[63]=f[31];
    z[64]=f[32];
    z[65]=f[33];
    z[66]=f[35];
    z[67]=f[36];
    z[68]=f[37];
    z[69]=f[38];
    z[70]=f[39];
    z[71]=f[41];
    z[72]=f[43];
    z[73]=f[50];
    z[74]=f[51];
    z[75]=f[52];
    z[76]=f[54];
    z[77]=f[55];
    z[78]=f[56];
    z[79]=f[58];
    z[80]=f[60];
    z[81]=f[95];
    z[82]=f[96];
    z[83]=n<T>(1,2)*z[5];
    z[84]=z[1]*i;
    z[85]=z[83] - z[84];
    z[85]=z[85]*z[5];
    z[86]=n<T>(1,2)*z[2];
    z[87]=z[86] - n<T>(23,4)*z[84] + z[5];
    z[87]=z[2]*z[87];
    z[88]=n<T>(1,2)*z[9];
    z[89]=z[88] - z[84];
    z[90]=n<T>(17,4)*z[9];
    z[91]= - z[89]*z[90];
    z[92]=n<T>(1,2)*z[6];
    z[93]= - z[2] + z[4];
    z[93]=z[93]*z[92];
    z[94]=n<T>(23,2)*z[84];
    z[95]=3*z[5];
    z[96]=z[94] - z[95];
    z[97]=n<T>(25,8)*z[7] - z[92] - n<T>(13,4)*z[4] + n<T>(1,2)*z[96] - z[2];
    z[97]=z[7]*z[97];
    z[98]=n<T>(1,2)*z[8];
    z[99]=z[84] - z[4];
    z[100]=z[6] + z[99];
    z[100]=z[100]*z[98];
    z[101]=z[10] - z[2];
    z[96]= - 3*z[7] - z[96] - n<T>(17,2)*z[101];
    z[96]=z[11]*z[96];
    z[101]=n<T>(9,4)*z[4] + n<T>(15,4)*z[2] - n<T>(11,4)*z[84] - z[5];
    z[101]=z[4]*z[101];
    z[102]=2*z[99] + z[10];
    z[102]=z[10]*z[102];
    z[87]=n<T>(1,4)*z[96] + 2*z[102] + z[100] + z[97] + z[93] + z[91] + 
    z[101] + n<T>(3,2)*z[85] + z[87];
    z[87]=z[11]*z[87];
    z[91]=n<T>(17,12)*z[9];
    z[93]=11*z[84];
    z[96]= - z[93] + n<T>(7,6)*z[5];
    z[96]= - z[98] - n<T>(9,4)*z[6] + z[91] + n<T>(1,2)*z[96] - n<T>(23,3)*z[2];
    z[96]=n<T>(391,72)*z[3] + n<T>(1,2)*z[96] + n<T>(2,3)*z[10];
    z[96]=z[3]*z[96];
    z[97]=n<T>(1,2)*z[4];
    z[100]=z[97] - z[84];
    z[101]=z[100]*z[4];
    z[102]=z[84] - z[5];
    z[103]=z[102] + z[2];
    z[104]= - z[88] + z[103];
    z[91]=z[104]*z[91];
    z[104]= - 9*z[84] + 11*z[5];
    z[104]= - n<T>(13,2)*z[6] + n<T>(1,2)*z[104] - z[4];
    z[104]=z[104]*z[92];
    z[105]= - z[98] - z[99];
    z[98]=z[105]*z[98];
    z[90]= - n<T>(121,8)*z[10] - z[90] + 4*z[102] + n<T>(17,4)*z[2];
    z[90]=z[10]*z[90];
    z[105]= - n<T>(47,12)*z[5] - 11 + n<T>(17,4)*z[84];
    z[105]=z[5]*z[105];
    z[106]= - n<T>(11,24)*z[2] - n<T>(7,12)*z[5] + 11 + n<T>(1,12)*z[84];
    z[106]=z[2]*z[106];
    z[90]=z[96] + n<T>(1,3)*z[90] + z[98] + z[104] + z[91] + n<T>(3,2)*z[101] + 
    z[105] + z[106];
    z[90]=z[3]*z[90];
    z[91]= - n<T>(37,4)*z[84] - n<T>(13,3)*z[5];
    z[91]= - n<T>(355,48)*z[7] - z[92] + n<T>(37,8)*z[9] + z[97] + n<T>(1,2)*z[91]
    + n<T>(5,3)*z[2];
    z[91]=z[7]*z[91];
    z[96]=4*z[84] - z[5];
    z[96]=5*z[96] - n<T>(7,2)*z[2];
    z[96]=z[2]*z[96];
    z[96]=n<T>(13,2)*z[85] + z[96];
    z[98]=n<T>(19,4)*z[4] - n<T>(19,2)*z[84] - z[2];
    z[98]=z[98]*z[97];
    z[104]= - n<T>(13,2)*z[2] + n<T>(107,8)*z[84] + 5*z[5];
    z[104]=n<T>(41,24)*z[9] + n<T>(1,3)*z[104] + z[97];
    z[104]=z[9]*z[104];
    z[105]=n<T>(1,3)*z[9];
    z[106]=n<T>(19,24)*z[6] - z[105] - n<T>(3,2)*z[4] + n<T>(5,6)*z[84] + z[2];
    z[106]=z[106]*z[92];
    z[107]=13*z[5];
    z[108]=47*z[2] + n<T>(239,4)*z[84] + z[107];
    z[108]=n<T>(359,18)*z[8] - n<T>(77,3)*z[7] + n<T>(41,4)*z[6] - n<T>(107,12)*z[9]
     + n<T>(1,3)*z[108]
     + z[97];
    z[108]=z[8]*z[108];
    z[91]=n<T>(1,4)*z[108] + z[91] + z[106] + z[104] + n<T>(1,3)*z[96] + z[98];
    z[91]=z[8]*z[91];
    z[96]=4*z[5];
    z[98]=2*z[2] - z[93] - z[96];
    z[98]=z[2]*z[98];
    z[104]=19*z[84];
    z[106]= - z[104] + n<T>(23,4)*z[5];
    z[106]=z[5]*z[106];
    z[98]=z[106] + z[98];
    z[106]=z[2] - z[5];
    z[108]= - n<T>(39,16)*z[4] + n<T>(39,8)*z[84] + z[106];
    z[108]=z[4]*z[108];
    z[106]= - n<T>(313,32)*z[9] + n<T>(313,16)*z[84] + 8*z[106];
    z[105]=z[106]*z[105];
    z[106]=n<T>(31,16)*z[6] - n<T>(39,8)*z[9] + n<T>(31,8)*z[84] + z[4];
    z[106]=z[106]*z[92];
    z[109]=n<T>(1,2)*z[7];
    z[110]=n<T>(121,8)*z[84] + n<T>(97,3)*z[5];
    z[110]=n<T>(989,72)*z[7] + n<T>(47,16)*z[6] - n<T>(35,16)*z[9] - n<T>(47,8)*z[4]
     + n<T>(1,2)*z[110] - n<T>(47,3)*z[2];
    z[110]=z[110]*z[109];
    z[98]=z[110] + z[106] + z[105] + n<T>(1,3)*z[98] + z[108];
    z[98]=z[7]*z[98];
    z[105]=17*z[2];
    z[106]= - z[104] + n<T>(137,2)*z[5];
    z[106]=n<T>(1,2)*z[106] + z[105];
    z[108]=n<T>(17,3)*z[9];
    z[110]=n<T>(35,4)*z[6];
    z[111]=n<T>(65,4)*z[7];
    z[106]=n<T>(365,18)*z[10] - z[111] + z[110] - z[108] + n<T>(1,3)*z[106] - n<T>(33,4)*z[4];
    z[112]=n<T>(1,2)*z[10];
    z[106]=z[106]*z[112];
    z[113]=z[86] + z[102];
    z[105]=z[113]*z[105];
    z[105]=n<T>(137,2)*z[85] + z[105];
    z[101]=n<T>(1,3)*z[105] - n<T>(33,2)*z[101];
    z[83]=n<T>(1,4)*z[9] - z[86] + z[84] + z[83];
    z[83]=z[83]*z[108];
    z[92]=z[92] + z[102];
    z[92]=z[92]*z[110];
    z[105]= - z[109] - z[99];
    z[105]=z[105]*z[111];
    z[83]=z[106] + z[105] + z[92] + n<T>(1,2)*z[101] + z[83];
    z[83]=z[83]*z[112];
    z[92]=n<T>(1,3)*z[2];
    z[101]=n<T>(3,4)*z[4];
    z[105]=43*z[84] - n<T>(13,6)*z[5];
    z[105]= - n<T>(3,2)*z[9] + z[101] + n<T>(1,2)*z[105] + z[92];
    z[105]=z[105]*z[88];
    z[106]=13*z[84] + n<T>(107,2)*z[5];
    z[106]=z[5]*z[106];
    z[108]=n<T>(49,4)*z[2] - z[84] - n<T>(49,2)*z[5];
    z[108]=z[2]*z[108];
    z[106]=n<T>(1,4)*z[106] + z[108];
    z[100]= - z[5] + z[100];
    z[100]=z[100]*z[101];
    z[100]=z[105] + n<T>(1,3)*z[106] + z[100];
    z[88]=z[100]*z[88];
    z[95]=n<T>(7,4)*z[2] + n<T>(1,2)*z[84] - z[95];
    z[95]=z[2]*z[95];
    z[94]= - n<T>(1,4)*z[4] - z[94] + z[5];
    z[94]=z[4]*z[94];
    z[94]=z[94] + n<T>(17,4)*z[85] + z[95];
    z[95]=n<T>(149,96)*z[9] - z[97] - z[86] - n<T>(149,48)*z[84] + z[5];
    z[95]=z[9]*z[95];
    z[97]= - n<T>(215,6)*z[84] + z[107];
    z[97]= - n<T>(233,48)*z[6] + n<T>(125,96)*z[9] + n<T>(25,8)*z[4] + n<T>(1,16)*z[97]
    - 3*z[2];
    z[97]=z[6]*z[97];
    z[94]=z[97] + n<T>(1,2)*z[94] + z[95];
    z[94]=z[6]*z[94];
    z[95]=z[84]*z[9];
    z[95]=z[95] + z[29];
    z[97]= - z[10]*z[84];
    z[97]=z[97] - z[95];
    z[97]=z[17]*z[97];
    z[100]=z[84] - z[9];
    z[101]=z[10] - z[100];
    z[101]=z[27]*z[101];
    z[105]=z[6] + z[3];
    z[105]=z[41]*z[105];
    z[97]= - z[80] + z[105] + z[97] + z[101];
    z[101]= - n<T>(45,8)*z[2] - 2*z[84] + 7*z[5];
    z[101]=z[2]*z[101];
    z[93]=n<T>(19,6)*z[4] + n<T>(25,8)*z[2] - z[93] - n<T>(53,16)*z[5];
    z[93]=z[4]*z[93];
    z[85]=z[93] - n<T>(41,8)*z[85] + z[101];
    z[85]=z[4]*z[85];
    z[93]=z[84]*z[4];
    z[93]=z[93] + z[29];
    z[101]= - z[11]*z[84];
    z[101]=z[101] - z[93];
    z[101]=z[13]*z[101];
    z[105]=z[11] - z[99];
    z[105]=z[28]*z[105];
    z[101]=z[71] + z[101] + z[105];
    z[105]= - z[8]*z[84];
    z[95]=z[105] - z[95];
    z[95]=z[16]*z[95];
    z[100]= - z[8] + z[100];
    z[100]=z[25]*z[100];
    z[95]=z[95] + z[100];
    z[100]=z[84]*z[5];
    z[100]=z[100] + z[29];
    z[105]=z[7]*z[84];
    z[105]=z[105] + z[100];
    z[105]=z[15]*z[105];
    z[106]= - z[7] + z[102];
    z[106]=z[22]*z[106];
    z[105]=z[105] + z[106];
    z[106]=z[6]*z[84];
    z[93]=z[106] + z[93];
    z[93]=z[14]*z[93];
    z[99]= - z[6] + z[99];
    z[99]=z[21]*z[99];
    z[93]=z[93] + z[99];
    z[99]=z[3]*z[84];
    z[99]=z[99] + z[100];
    z[99]=z[12]*z[99];
    z[100]=z[3] - z[102];
    z[100]=z[20]*z[100];
    z[99]=z[99] + z[100];
    z[96]=n<T>(7,2)*z[84] - z[96];
    z[96]=z[96]*npow(z[5],2);
    z[100]=2*z[5];
    z[102]= - z[104] + z[100];
    z[100]=z[102]*z[100];
    z[102]=35*z[84] + 17*z[5];
    z[102]=n<T>(5,2)*z[102] - 29*z[2];
    z[102]=z[2]*z[102];
    z[100]=z[100] + n<T>(1,4)*z[102];
    z[92]=z[100]*z[92];
    z[86]=z[86] + z[89];
    z[86]=z[24]*z[86];
    z[89]=z[50] + z[78] - z[68];
    z[100]=z[56] + z[75] + z[69];
    z[102]=z[82] - z[81];
    z[104]= - 61*z[6] - n<T>(113,2)*z[8];
    z[104]=z[42]*z[104];
    z[104]=z[104] - z[52];
    z[106]= - n<T>(337,12)*z[37] - n<T>(31,6)*z[35] + n<T>(1,4)*z[34] - n<T>(127,288)*
    z[30];
    z[106]=i*z[106];
    z[107]=n<T>(71,16)*z[5] - 7*z[2];
    z[107]=n<T>(35,12)*z[3] - n<T>(31,8)*z[10] + n<T>(4,3)*z[8] + n<T>(199,8)*z[7]
     - n<T>(22,3)*z[6]
     + n<T>(1913,96)*z[9]
     + n<T>(1,3)*z[107] - n<T>(521,16)*z[4];
    z[107]=n<T>(1,3)*z[107] - 3*z[11];
    z[107]=z[29]*z[107];
    z[108]= - 23*z[84] + 29*z[5];
    z[108]=n<T>(1,2)*z[108] - 13*z[2];
    z[108]=n<T>(1,2)*z[11] + n<T>(209,24)*z[8] + n<T>(337,24)*z[7] + z[6] + n<T>(13,3)*
    z[9] + n<T>(1,3)*z[108] - z[4];
    z[108]=z[23]*z[108];
    z[103]=z[9] - z[103];
    z[103]=n<T>(103,2)*z[3] + 17*z[103] + n<T>(137,2)*z[10];
    z[103]=z[26]*z[103];
    z[109]=109*z[84] - 45*z[2];
    z[109]= - n<T>(19,4)*z[11] + n<T>(1,4)*z[109] - 16*z[4];
    z[109]=z[18]*z[109];
    z[84]= - n<T>(79,2)*z[2] + 31*z[84] + n<T>(17,2)*z[5];
    z[84]=n<T>(1,3)*z[84] + 3*z[6];
    z[84]=z[19]*z[84];
    z[110]=z[7] + z[11];
    z[110]=z[43]*z[110];
    z[111]=z[10] + z[11];
    z[111]=z[44]*z[111];

    r +=  - n<T>(37,16)*z[31] + n<T>(7,12)*z[32] + n<T>(245,288)*z[33] + n<T>(59,48)*
      z[36] + n<T>(277,24)*z[38] - n<T>(7,16)*z[39] + n<T>(4417,288)*z[40] - n<T>(33,2)
      *z[45] + n<T>(23,8)*z[46] - n<T>(57,16)*z[47] + n<T>(205,48)*z[48] - n<T>(301,48)
      *z[49] + n<T>(34,3)*z[51] - n<T>(61,48)*z[53] - n<T>(21,16)*z[54] + 5*z[55]
       + n<T>(35,16)*z[57] + n<T>(11,16)*z[58] - n<T>(7,3)*z[59] + n<T>(3,2)*z[60]
     - n<T>(17,12)*z[61]
     + n<T>(1,3)*z[62] - n<T>(857,96)*z[63] - n<T>(27,4)*z[64] - n<T>(64,3)
      *z[65] - n<T>(151,24)*z[66] - n<T>(101,96)*z[67] + n<T>(31,32)*z[70] + n<T>(17,3)
      *z[72] - 4*z[73] - n<T>(39,16)*z[74] + n<T>(19,4)*z[76] + n<T>(33,16)*z[77]
       - n<T>(65,16)*z[79] + z[83] + n<T>(1,2)*z[84] + z[85] + n<T>(47,6)*z[86] + 
      z[87] + z[88] + n<T>(17,8)*z[89] + z[90] + z[91] + z[92] + 6*z[93] + 
      z[94] + n<T>(63,8)*z[95] + z[96] + n<T>(17,4)*z[97] + z[98] + n<T>(67,12)*
      z[99] - n<T>(3,4)*z[100] + n<T>(9,2)*z[101] + n<T>(1,4)*z[102] + n<T>(1,6)*z[103]
       + n<T>(1,12)*z[104] + n<T>(15,2)*z[105] + z[106] + z[107] + z[108] + 
      z[109] + n<T>(29,4)*z[110] + n<T>(33,4)*z[111];
 
    return r;
}

template std::complex<double> qqb_2lha_tf958(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf958(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
