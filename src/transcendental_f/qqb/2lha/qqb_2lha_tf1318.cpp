#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1318(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[15];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[12];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=e[12];
    z[24]=e[14];
    z[25]=c[3];
    z[26]=c[11];
    z[27]=c[15];
    z[28]=e[3];
    z[29]=e[6];
    z[30]=e[13];
    z[31]=f[2];
    z[32]=f[4];
    z[33]=f[6];
    z[34]=f[7];
    z[35]=f[10];
    z[36]=f[12];
    z[37]=f[16];
    z[38]=f[23];
    z[39]=f[30];
    z[40]=f[31];
    z[41]=f[32];
    z[42]=f[33];
    z[43]=f[35];
    z[44]=f[36];
    z[45]=f[39];
    z[46]=f[43];
    z[47]=f[51];
    z[48]=f[52];
    z[49]=f[54];
    z[50]=f[55];
    z[51]=f[56];
    z[52]=f[57];
    z[53]=f[58];
    z[54]=n<T>(1,2)*z[7];
    z[55]=n<T>(1,2)*z[9];
    z[56]= - z[55] - z[54];
    z[57]=z[4] + z[6];
    z[56]=z[57]*z[56];
    z[58]=n<T>(1,2)*z[6];
    z[59]=z[1]*i;
    z[60]=z[59] + z[6];
    z[60]=z[60]*z[58];
    z[61]=n<T>(1,2)*z[59];
    z[62]=z[61] + z[4];
    z[62]=z[4]*z[62];
    z[63]=z[59] - z[7];
    z[64]=z[9] - z[63];
    z[65]=z[6] + z[64];
    z[65]=n<T>(1,2)*z[65] - z[3];
    z[65]=z[3]*z[65];
    z[66]= - z[5] - z[4] + z[64];
    z[66]=z[5]*z[66];
    z[67]= - z[5] - z[3] + z[57];
    z[67]=z[2]*z[67];
    z[56]=n<T>(1,4)*z[67] + n<T>(1,2)*z[66] + z[65] + z[60] + z[62] + z[56];
    z[60]=n<T>(1,3)*z[2];
    z[56]=z[56]*z[60];
    z[62]= - n<T>(3,2)*z[6] - z[63];
    z[65]=n<T>(1,4)*z[6];
    z[62]=z[65]*z[62];
    z[66]=n<T>(1,6)*z[4];
    z[67]=n<T>(1,3)*z[59];
    z[68]=z[66] + z[67];
    z[69]= - z[65] + z[68];
    z[69]=z[4]*z[69];
    z[70]=z[55] - z[59];
    z[71]=n<T>(1,4)*z[9];
    z[72]= - z[70]*z[71];
    z[73]=3*z[6];
    z[74]=z[7] - z[4];
    z[75]=z[73] - z[74];
    z[76]= - z[9] - z[75];
    z[76]=n<T>(1,4)*z[76] + z[14];
    z[76]=z[14]*z[76];
    z[74]= - z[6] - z[74] + z[9];
    z[74]=z[3]*z[74];
    z[62]=n<T>(1,2)*z[76] + n<T>(1,4)*z[74] + z[72] + z[69] + z[62];
    z[62]=z[14]*z[62];
    z[69]=z[58] - z[59];
    z[72]=n<T>(1,3)*z[6];
    z[74]= - z[69]*z[72];
    z[76]=n<T>(1,6)*z[7];
    z[57]=z[57]*z[76];
    z[77]=z[59] - z[9];
    z[78]=n<T>(17,12)*z[4] + z[72] - z[77];
    z[71]=z[78]*z[71];
    z[78]= - n<T>(17,3)*z[59] + n<T>(3,2)*z[4];
    z[78]=z[4]*z[78];
    z[57]=z[71] + z[57] + z[74] + n<T>(1,8)*z[78];
    z[57]=z[9]*z[57];
    z[71]= - z[55] - n<T>(7,2)*z[59] + z[6];
    z[71]=n<T>(1,2)*z[71] + n<T>(13,3)*z[3];
    z[71]=z[3]*z[71];
    z[74]= - n<T>(5,4)*z[9] - z[7] + n<T>(5,2)*z[59] - z[6];
    z[74]=z[9]*z[74];
    z[71]=z[74] + z[71];
    z[74]=z[59] - n<T>(11,4)*z[6];
    z[72]=z[74]*z[72];
    z[74]= - z[4]*z[58];
    z[72]=z[72] + z[74];
    z[74]= - n<T>(1,3)*z[69] - z[54];
    z[74]=z[7]*z[74];
    z[71]=n<T>(1,2)*z[72] + z[74] + n<T>(1,6)*z[71];
    z[71]=z[3]*z[71];
    z[72]= - z[54] + z[59] + z[4];
    z[54]=z[72]*z[54];
    z[72]=npow(z[4],2);
    z[54]=z[72] + z[54];
    z[61]= - n<T>(5,6)*z[9] + z[61] - n<T>(1,3)*z[7];
    z[61]=z[61]*z[55];
    z[72]= - n<T>(5,2)*z[7] - z[59] + n<T>(13,2)*z[4];
    z[55]= - n<T>(11,18)*z[5] + n<T>(1,3)*z[72] - z[55];
    z[72]=n<T>(1,4)*z[5];
    z[55]=z[55]*z[72];
    z[54]=z[55] + n<T>(1,3)*z[54] + z[61];
    z[54]=z[5]*z[54];
    z[55]=z[59] + z[58];
    z[55]=z[6]*z[55];
    z[61]=z[67] + z[6];
    z[74]=z[76] - z[61];
    z[74]=z[7]*z[74];
    z[55]=z[55] + z[74];
    z[74]=3*z[4];
    z[76]=z[59] + n<T>(1,2)*z[4];
    z[76]=z[76]*z[74];
    z[70]= - z[74] + z[70];
    z[70]=z[9]*z[70];
    z[55]=z[70] + z[76] + 5*z[55];
    z[70]= - n<T>(1,2)*z[3] - z[63];
    z[70]=z[3]*z[70];
    z[74]= - z[5]*z[77];
    z[55]=z[74] + n<T>(1,2)*z[55] + n<T>(5,3)*z[70];
    z[70]= - n<T>(1,4)*z[8] - z[72] - n<T>(5,6)*z[3] + n<T>(1,8)*z[9] + n<T>(5,24)*z[7]
    + n<T>(3,8)*z[4] - z[67] + n<T>(5,8)*z[6];
    z[70]=z[8]*z[70];
    z[55]=n<T>(1,2)*z[55] + z[70];
    z[55]=z[8]*z[55];
    z[70]=z[11] + z[14];
    z[68]= - z[58] - z[68] + n<T>(1,3)*z[70];
    z[68]=z[11]*z[68];
    z[69]= - z[6]*z[69];
    z[61]= - z[66] + z[61];
    z[61]=z[4]*z[61];
    z[61]=z[68] + z[69] + z[61];
    z[66]=n<T>(1,3)*z[4];
    z[68]= - z[14]*z[66];
    z[61]=z[68] + n<T>(1,2)*z[61];
    z[61]=z[11]*z[61];
    z[58]= - 3*z[59] - z[58];
    z[58]=z[6]*z[58];
    z[68]= - n<T>(5,6)*z[59] - z[73];
    z[68]=n<T>(1,2)*z[68] - n<T>(11,9)*z[4];
    z[68]=z[4]*z[68];
    z[58]=z[58] + z[68];
    z[58]=z[4]*z[58];
    z[68]=n<T>(5,4)*z[59] + n<T>(31,3)*z[6];
    z[68]=z[68]*npow(z[6],2);
    z[58]=n<T>(1,3)*z[68] + z[58];
    z[65]= - z[66] + z[67] + z[65];
    z[65]=z[4]*z[65];
    z[66]= - z[66] + n<T>(19,6)*z[6] - z[63];
    z[66]=z[7]*z[66];
    z[67]= - 19*z[59] + n<T>(7,2)*z[6];
    z[67]=z[6]*z[67];
    z[65]=n<T>(1,8)*z[66] + n<T>(1,24)*z[67] + z[65];
    z[65]=z[7]*z[65];
    z[66]=n<T>(19,4)*z[6] + z[64];
    z[60]= - z[60] + n<T>(1,3)*z[66] + n<T>(5,4)*z[3];
    z[60]=z[21]*z[60];
    z[66]=z[3] + z[59] + z[75];
    z[66]=n<T>(1,2)*z[66] + z[14];
    z[66]=z[24]*z[66];
    z[54]=z[66] + z[60] + z[61] + z[62] + z[56] + n<T>(1,2)*z[55] + z[54] + 
    z[71] + z[57] + n<T>(1,4)*z[58] + z[65];
    z[55]= - z[4] - z[7];
    z[55]=z[59]*z[55];
    z[55]= - z[25] + z[55];
    z[55]=z[13]*z[55];
    z[56]=z[4] - z[63];
    z[56]=z[18]*z[56];
    z[57]= - z[5] - z[8];
    z[57]=z[29]*z[57];
    z[55]=z[55] + z[56] + z[57] + z[50] + z[39] + z[35];
    z[56]=z[7] + z[3];
    z[56]=z[59]*z[56];
    z[56]=z[25] + z[56];
    z[56]=z[10]*z[56];
    z[57]= - z[6] - n<T>(5,2)*z[4];
    z[57]= - n<T>(11,4)*z[9] + 5*z[57] - n<T>(1,4)*z[7];
    z[57]= - n<T>(1,16)*z[14] - z[8] + n<T>(7,16)*z[5] + n<T>(1,8)*z[57] + z[3];
    z[57]=n<T>(1,3)*z[57] - n<T>(1,8)*z[11];
    z[57]=z[25]*z[57];
    z[58]=z[3] - z[63];
    z[58]=z[17]*z[58];
    z[56]=z[31] + z[58] + z[57] + z[56];
    z[57]=z[59]*z[9];
    z[57]=z[57] + z[25];
    z[58]=z[5]*z[59];
    z[58]=z[58] + z[57];
    z[58]=z[15]*z[58];
    z[60]=z[5] - z[77];
    z[60]=z[20]*z[60];
    z[61]=z[4] + z[14];
    z[61]=z[30]*z[61];
    z[58]=z[61] + z[58] + z[60];
    z[60]= - z[59]*z[70];
    z[60]= - z[25] + z[60];
    z[60]=z[12]*z[60];
    z[61]= - z[59] + z[70];
    z[61]=z[23]*z[61];
    z[62]=z[2] - n<T>(13,4)*z[5] - n<T>(17,4)*z[4] - z[64];
    z[62]=z[19]*z[62];
    z[60]=z[62] + z[60] + z[61] + z[49];
    z[59]= - z[6]*z[59];
    z[57]=z[59] - z[57];
    z[57]=z[16]*z[57];
    z[59]=z[6] - z[77];
    z[59]=z[22]*z[59];
    z[57]=z[57] + z[59];
    z[59]= - z[33] + z[53] - z[41];
    z[61]=z[46] - z[43] + z[38] + z[27];
    z[62]=z[42] + z[34];
    z[63]=z[51] + z[48];
    z[64]=z[3] + z[8];
    z[64]=z[28]*z[64];
    z[65]=z[26]*i;

    r += n<T>(9,32)*z[32] - n<T>(5,96)*z[36] + n<T>(5,32)*z[37] + n<T>(25,96)*z[40]
     - n<T>(1,32)*z[44]
     + n<T>(3,32)*z[45] - n<T>(1,24)*z[47]
     + z[52]
     + n<T>(1,2)*z[54]
     +  n<T>(1,8)*z[55] + n<T>(1,3)*z[56] + n<T>(1,4)*z[57] + n<T>(7,24)*z[58] - n<T>(3,16)*
      z[59] + n<T>(1,6)*z[60] - n<T>(1,12)*z[61] + n<T>(2,3)*z[62] - n<T>(1,16)*z[63]
       + n<T>(5,24)*z[64] + n<T>(7,72)*z[65];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1318(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1318(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
