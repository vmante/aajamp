#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf210(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[35];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[15];
    z[11]=c[18];
    z[12]=c[19];
    z[13]=c[23];
    z[14]=c[25];
    z[15]=c[26];
    z[16]=c[28];
    z[17]=c[29];
    z[18]=c[31];
    z[19]=f[0];
    z[20]=f[3];
    z[21]=f[5];
    z[22]=f[11];
    z[23]=f[13];
    z[24]=f[17];
    z[25]=f[20];
    z[26]=i*z[1];
    z[27]= - z[2] + n<T>(3,2)*z[26] + z[4];
    z[28]=n<T>(1,2)*z[5];
    z[27]=z[3] + z[28] + 3*z[27];
    z[27]=z[2]*z[27];
    z[29]=3*z[7];
    z[30]=z[4]*z[26];
    z[31]=z[4] + 2*z[26];
    z[32]=z[5] - z[31];
    z[32]=z[5]*z[32];
    z[31]=z[3] - z[31];
    z[31]=z[3]*z[31];
    z[27]=z[27] + 2*z[31] + z[32] - 3*z[30] + n<T>(1,2)*z[9] - 5*z[6] - 
    z[29] - z[8];
    z[27]=z[2]*z[27];
    z[30]=n<T>(1,2)*z[4];
    z[31]=5*z[26] + z[30];
    z[31]=z[31]*z[30];
    z[28]= - z[28] + z[26] + z[4];
    z[28]=z[5]*z[28];
    z[32]=n<T>(1,12)*z[9];
    z[33]= - z[6] + z[32];
    z[34]= - 5*z[4] - 3*z[5];
    z[34]=z[3]*z[34];
    z[28]=n<T>(1,4)*z[34] + n<T>(3,2)*z[28] + 5*z[33] + z[31];
    z[28]=z[3]*z[28];
    z[26]=z[26] - z[30];
    z[26]=z[26]*z[30];
    z[30]=n<T>(1,4)*z[4];
    z[31]= - z[5]*z[30];
    z[26]=z[31] + z[26] - z[8] + z[32];
    z[26]=z[5]*z[26];
    z[31]= - 5*z[15] - n<T>(3,2)*z[13];
    z[29]=10*z[6] + z[29] + 2*z[8];
    z[29]=z[1]*z[29];
    z[29]=3*z[31] + z[29];
    z[29]=i*z[29];
    z[31]=z[17] + z[24];
    z[32]=z[14] - z[19];
    z[33]=z[10] - z[22];
    z[30]=z[9]*z[30];

    r += z[11] - n<T>(1,2)*z[12] + 9*z[16] + n<T>(77,8)*z[18] - n<T>(5,4)*z[20] - 
      z[21] - 2*z[23] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30]
       - n<T>(3,4)*z[31] + 3*z[32] + n<T>(1,4)*z[33];
 
    return r;
}

template std::complex<double> qqb_2lha_tf210(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf210(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
