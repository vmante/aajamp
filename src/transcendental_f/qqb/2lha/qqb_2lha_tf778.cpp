#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf778(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[97];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[9];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[5];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[18];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[10];
    z[21]=e[11];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[18];
    z[27]=c[19];
    z[28]=c[21];
    z[29]=c[23];
    z[30]=c[25];
    z[31]=c[26];
    z[32]=c[28];
    z[33]=c[29];
    z[34]=c[31];
    z[35]=d[6];
    z[36]=e[3];
    z[37]=e[13];
    z[38]=e[14];
    z[39]=f[0];
    z[40]=f[1];
    z[41]=f[2];
    z[42]=f[3];
    z[43]=f[4];
    z[44]=f[5];
    z[45]=f[6];
    z[46]=f[7];
    z[47]=f[8];
    z[48]=f[10];
    z[49]=f[11];
    z[50]=f[12];
    z[51]=f[14];
    z[52]=f[15];
    z[53]=f[16];
    z[54]=f[17];
    z[55]=f[18];
    z[56]=f[20];
    z[57]=f[23];
    z[58]=f[50];
    z[59]=f[51];
    z[60]=f[54];
    z[61]=f[55];
    z[62]=f[56];
    z[63]=f[57];
    z[64]=f[58];
    z[65]=f[60];
    z[66]=f[90];
    z[67]=f[91];
    z[68]=n<T>(5,4)*z[7];
    z[69]=z[1]*i;
    z[70]=n<T>(5,2)*z[69];
    z[71]=n<T>(1,2)*z[9];
    z[72]=z[68] + z[71] - z[70] + z[3];
    z[73]=n<T>(1,2)*z[7];
    z[72]=z[72]*z[73];
    z[74]=n<T>(1,2)*z[3];
    z[75]= - z[69] - z[74];
    z[75]=z[75]*z[74];
    z[76]=n<T>(1,2)*z[2];
    z[77]=z[69] - z[76];
    z[77]=z[2]*z[77];
    z[78]= - n<T>(3,8)*z[8] - n<T>(3,4)*z[7] - z[2] + n<T>(29,8)*z[9] - z[74] + n<T>(11,4)*z[69]
     - z[4];
    z[78]=z[8]*z[78];
    z[79]=z[69] - n<T>(67,48)*z[4];
    z[79]=z[4]*z[79];
    z[80]= - n<T>(25,8)*z[9] - n<T>(17,2)*z[69] + 7*z[4];
    z[80]=z[9]*z[80];
    z[81]=z[2] - z[4];
    z[82]=z[6]*z[81];
    z[72]=n<T>(1,2)*z[78] + z[82] + z[72] + 3*z[77] + n<T>(1,6)*z[80] + z[79] + 
    z[75];
    z[72]=z[8]*z[72];
    z[75]=3*z[3];
    z[77]=9*z[4];
    z[78]=3*z[9];
    z[79]=z[78] - z[75] + n<T>(25,3)*z[69] + z[77];
    z[80]=n<T>(1,3)*z[2];
    z[79]= - n<T>(25,12)*z[7] + n<T>(1,2)*z[79] - z[80];
    z[79]=z[79]*z[73];
    z[82]=n<T>(5,2)*z[3];
    z[83]=n<T>(7,2)*z[9];
    z[84]=11*z[69] - 37*z[4];
    z[84]=n<T>(19,3)*z[2] - z[83] + n<T>(1,3)*z[84] + z[82];
    z[84]=z[84]*z[76];
    z[85]= - z[69] + n<T>(1,2)*z[4];
    z[86]=z[85]*z[4];
    z[87]= - z[4] + n<T>(13,6)*z[69];
    z[88]= - n<T>(13,12)*z[3] + z[87];
    z[88]=z[3]*z[88];
    z[88]=n<T>(47,6)*z[86] + z[88];
    z[89]=n<T>(11,16)*z[9] - z[74] - n<T>(15,8)*z[69] + z[4];
    z[89]=z[9]*z[89];
    z[90]= - z[69] + n<T>(15,4)*z[9];
    z[91]=n<T>(1,3)*z[5] - n<T>(25,6)*z[7] - n<T>(11,3)*z[2] - n<T>(7,3)*z[3] + z[90]
     + 
   n<T>(47,6)*z[4];
    z[91]=z[5]*z[91];
    z[79]=n<T>(1,4)*z[91] + z[79] + z[84] + n<T>(1,2)*z[88] + z[89];
    z[79]=z[5]*z[79];
    z[84]=3*z[69];
    z[88]=z[84] + z[83];
    z[89]=n<T>(1,4)*z[9];
    z[88]=z[88]*z[89];
    z[91]= - z[81] + n<T>(17,24)*z[5];
    z[92]= - n<T>(7,4)*z[9] + n<T>(19,6)*z[3] + n<T>(17,12)*z[69] - z[91];
    z[92]=z[5]*z[92];
    z[87]= - n<T>(19,12)*z[3] - z[87];
    z[87]=z[3]*z[87];
    z[87]=z[92] + z[87] + z[88];
    z[88]= - z[74] + z[69] - z[81];
    z[88]=z[2]*z[88];
    z[92]=z[9] - z[3];
    z[93]=z[92] - z[81];
    z[94]=z[93]*z[73];
    z[91]=n<T>(7,8)*z[9] - n<T>(1,6)*z[3] + n<T>(17,24)*z[69] - z[91];
    z[91]=n<T>(1,2)*z[91] + n<T>(2,3)*z[6];
    z[91]=z[6]*z[91];
    z[87]=z[91] + z[94] + z[88] + n<T>(1,2)*z[87];
    z[87]=z[6]*z[87];
    z[70]= - z[70] + z[4];
    z[70]=z[70]*z[74];
    z[83]=z[3] - z[83];
    z[83]=z[83]*z[89];
    z[88]=23*z[69] + 31*z[4];
    z[82]= - n<T>(65,6)*z[2] + n<T>(9,2)*z[9] + n<T>(1,3)*z[88] - z[82];
    z[82]=z[2]*z[82];
    z[88]=8*z[69] - n<T>(5,2)*z[4];
    z[88]=z[4]*z[88];
    z[70]=n<T>(1,4)*z[82] + z[83] + n<T>(1,3)*z[88] + z[70];
    z[70]=z[2]*z[70];
    z[82]= - 25*z[69] - z[77] + z[92];
    z[68]=z[68] + n<T>(1,4)*z[82] + n<T>(13,3)*z[2];
    z[68]=z[68]*z[73];
    z[73]= - z[85]*z[77];
    z[77]=z[69] + n<T>(7,2)*z[3];
    z[77]=z[3]*z[77];
    z[73]=z[73] + z[77];
    z[75]= - n<T>(61,3)*z[69] + z[75];
    z[75]=n<T>(11,12)*z[2] + n<T>(1,2)*z[75] - z[9];
    z[75]=z[75]*z[76];
    z[76]=z[9] - n<T>(5,4)*z[3] - n<T>(3,4)*z[69] - z[4];
    z[76]=z[9]*z[76];
    z[68]=z[68] + z[75] + n<T>(1,4)*z[73] + z[76];
    z[68]=z[7]*z[68];
    z[73]=z[5] + z[3];
    z[75]=z[69]*z[73];
    z[75]=z[23] + z[75];
    z[75]=z[10]*z[75];
    z[73]= - z[69] + z[73];
    z[73]=z[17]*z[73];
    z[76]= - 65*z[9] - n<T>(121,2)*z[8];
    z[76]=z[38]*z[76];
    z[73]=z[41] + z[76] + z[75] + z[73];
    z[75]= - z[6] + z[7];
    z[75]=z[93]*z[75];
    z[76]=z[4] + z[92];
    z[76]=z[3]*z[76];
    z[77]= - z[9] + z[81];
    z[77]=z[2]*z[77];
    z[75]=z[77] + z[75] + z[76];
    z[75]=z[35]*z[75];
    z[75]=z[40] - z[75] + z[51] + z[39] + z[33];
    z[76]=z[7] + z[9];
    z[77]=z[69]*z[76];
    z[77]=z[23] + z[77];
    z[77]=z[14]*z[77];
    z[76]=z[69] - z[76];
    z[76]=z[21]*z[76];
    z[76]=z[77] + z[76];
    z[77]=z[69] - z[4];
    z[81]=z[71] + z[77];
    z[81]=z[81]*z[78];
    z[78]=z[78] - z[77];
    z[82]=z[11] + z[8];
    z[78]=n<T>(1,2)*z[78] - z[82];
    z[78]=z[11]*z[78];
    z[78]=z[78] + z[86] + z[81];
    z[81]= - z[8]*z[77];
    z[78]=z[81] + n<T>(1,2)*z[78];
    z[78]=z[11]*z[78];
    z[81]=z[37]*z[82];
    z[78]= - z[60] + z[78] - z[81];
    z[81]= - 211*z[69] + 133*z[4];
    z[81]= - n<T>(51,2)*z[9] + n<T>(1,12)*z[81] - 11*z[3];
    z[81]=z[81]*z[89];
    z[82]=z[84] + z[3];
    z[82]=z[3]*z[82];
    z[81]=z[81] + n<T>(157,24)*z[86] + z[82];
    z[71]=z[81]*z[71];
    z[81]=z[69]*z[4];
    z[81]=z[81] + z[23];
    z[82]=z[8]*z[69];
    z[82]=z[82] + z[81];
    z[82]=z[12]*z[82];
    z[83]= - z[8] + z[77];
    z[83]=z[22]*z[83];
    z[82]=z[82] + z[83];
    z[83]=n<T>(41,12)*z[3] + n<T>(1,6)*z[69] - z[4];
    z[74]=z[83]*z[74];
    z[74]= - z[86] + z[74];
    z[74]=z[3]*z[74];
    z[83]=n<T>(7,6)*z[5] - n<T>(11,6)*z[2] - n<T>(7,3)*z[69] + 3*z[4] + n<T>(3,2)*z[92];
    z[83]=z[16]*z[83];
    z[84]= - 5*z[69] + z[4];
    z[80]=z[5] + z[80] + n<T>(1,3)*z[84] - z[3];
    z[80]=z[15]*z[80];
    z[84]= - z[6]*z[69];
    z[81]=z[84] - z[81];
    z[81]=z[13]*z[81];
    z[84]=z[48] + z[67] - z[66];
    z[85]=z[30] - z[25];
    z[86]=z[52] + z[46];
    z[88]=z[53] + z[43];
    z[89]=z[54] + z[45];
    z[91]=z[55] + z[26];
    z[92]=z[63] - z[57];
    z[93]= - n<T>(19,12)*z[31] + n<T>(5,24)*z[29] - n<T>(9,4)*z[28] - n<T>(257,288)*
    z[24];
    z[93]=i*z[93];
    z[94]= - n<T>(29,3)*z[69] - z[4];
    z[94]=z[94]*npow(z[4],2);
    z[95]= - n<T>(119,24)*z[4] + 5*z[3];
    z[95]=n<T>(211,48)*z[8] + n<T>(7,6)*z[6] - n<T>(7,12)*z[5] - n<T>(565,48)*z[7]
     + n<T>(55,6)*z[2]
     + n<T>(1,4)*z[95] - n<T>(29,3)*z[9];
    z[95]=n<T>(1,3)*z[95] - n<T>(7,8)*z[11];
    z[95]=z[23]*z[95];
    z[69]=n<T>(53,3)*z[69] - 5*z[4];
    z[69]=z[35] + n<T>(3,2)*z[5] - n<T>(41,12)*z[7] - n<T>(53,12)*z[2] + n<T>(1,2)*z[69]
    + z[3];
    z[69]=z[19]*z[69];
    z[90]=z[5] + z[7] - z[2] + n<T>(11,4)*z[3] + z[90];
    z[90]=z[20]*z[90];
    z[96]= - z[35] - n<T>(17,12)*z[6] - z[5] - n<T>(5,12)*z[3] + z[7];
    z[96]=z[36]*z[96];
    z[77]=z[6] - z[77];
    z[77]=z[18]*z[77];

    r +=  - n<T>(205,72)*z[27] + n<T>(47,12)*z[32] + n<T>(721,72)*z[34] + n<T>(23,24)*
      z[42] - n<T>(77,24)*z[44] - 3*z[47] - n<T>(31,24)*z[49] + n<T>(17,48)*z[50]
       + n<T>(3,2)*z[56] + n<T>(7,6)*z[58] + n<T>(21,32)*z[59] - n<T>(109,96)*z[61]
     - 
      n<T>(11,8)*z[62] + n<T>(63,32)*z[64] + 2*z[65] + z[68] + z[69] + z[70] + 
      z[71] + z[72] + n<T>(1,12)*z[73] + z[74] - n<T>(1,2)*z[75] + n<T>(5,4)*z[76]
       + z[77] + n<T>(21,8)*z[78] + z[79] + z[80] + z[81] + n<T>(43,24)*z[82]
       + z[83] + n<T>(1,4)*z[84] + n<T>(47,24)*z[85] + 4*z[86] + z[87] + n<T>(7,16)
      *z[88] - n<T>(5,8)*z[89] + z[90] + n<T>(2,3)*z[91] - n<T>(3,4)*z[92] + z[93]
       + n<T>(1,16)*z[94] + z[95] + z[96];
 
    return r;
}

template std::complex<double> qqb_2lha_tf778(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf778(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
