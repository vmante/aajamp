#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1498(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[106];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[11];
    z[27]=e[12];
    z[28]=e[13];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[19];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[31];
    z[38]=e[10];
    z[39]=e[3];
    z[40]=e[6];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[11];
    z[50]=f[12];
    z[51]=f[14];
    z[52]=f[15];
    z[53]=f[16];
    z[54]=f[17];
    z[55]=f[18];
    z[56]=f[23];
    z[57]=f[30];
    z[58]=f[31];
    z[59]=f[32];
    z[60]=f[33];
    z[61]=f[35];
    z[62]=f[36];
    z[63]=f[37];
    z[64]=f[39];
    z[65]=f[43];
    z[66]=f[50];
    z[67]=f[51];
    z[68]=f[52];
    z[69]=f[53];
    z[70]=f[54];
    z[71]=f[55];
    z[72]=f[56];
    z[73]=f[58];
    z[74]=f[72];
    z[75]=f[73];
    z[76]=f[74];
    z[77]=f[75];
    z[78]=n<T>(1,2)*z[3];
    z[79]=z[1]*i;
    z[80]=z[78] + z[79];
    z[81]=n<T>(1,4)*z[5];
    z[82]=n<T>(1,4)*z[6];
    z[83]=z[81] - z[82];
    z[84]= - z[83] + z[80];
    z[84]=z[3]*z[84];
    z[85]=n<T>(1,2)*z[5];
    z[86]=z[85] - z[79];
    z[87]=z[86]*z[5];
    z[88]=n<T>(1,2)*z[8];
    z[89]=z[88] - z[79];
    z[90]= - z[3] + z[89];
    z[91]=n<T>(3,4)*z[8];
    z[90]=z[90]*z[91];
    z[92]=n<T>(1,4)*z[9];
    z[93]= - n<T>(7,4)*z[9] - z[3] - z[6] - n<T>(11,2)*z[79] + z[5];
    z[93]=z[93]*z[92];
    z[94]=n<T>(1,2)*z[4];
    z[95]= - n<T>(7,24)*z[4] - n<T>(5,3)*z[6] + n<T>(9,4)*z[9];
    z[95]=z[95]*z[94];
    z[91]= - n<T>(3,8)*z[11] - z[7] - n<T>(3,4)*z[9] + z[91] + n<T>(15,8)*z[6] + n<T>(1,4)*z[79]
     - z[5];
    z[91]=z[11]*z[91];
    z[96]= - n<T>(31,48)*z[6] - n<T>(1,6)*z[79] + z[5];
    z[96]=z[6]*z[96];
    z[97]=n<T>(1,2)*z[7];
    z[98]=z[6] - z[97];
    z[98]=z[7]*z[98];
    z[84]=n<T>(1,2)*z[91] + z[95] + z[98] + z[93] + z[90] + z[84] - z[87] + 
    z[96];
    z[84]=z[11]*z[84];
    z[90]=5*z[5];
    z[91]=z[90]*z[86];
    z[93]=n<T>(1,2)*z[6];
    z[95]= - z[79] + z[93];
    z[96]=n<T>(61,12)*z[6];
    z[95]=z[95]*z[96];
    z[98]=z[78] - z[79];
    z[99]=5*z[3];
    z[98]=z[98]*z[99];
    z[89]=z[5] - z[89];
    z[100]=5*z[8];
    z[89]=z[89]*z[100];
    z[101]=7*z[6];
    z[102]=11*z[79];
    z[103]= - n<T>(11,2)*z[9] + z[102] - z[101];
    z[103]=z[103]*z[92];
    z[97]=z[97] - z[79] - z[3];
    z[104]=5*z[7];
    z[97]=z[97]*z[104];
    z[89]=z[97] + z[103] + z[89] + z[98] - z[91] + z[95];
    z[90]=z[104] - n<T>(11,4)*z[9] - z[100] + z[99] + z[96] + n<T>(67,6)*z[79]
    - z[90];
    z[95]=5*z[2];
    z[90]= - n<T>(67,12)*z[4] + n<T>(1,2)*z[90] + z[95];
    z[90]=z[90]*z[94];
    z[95]= - z[79]*z[95];
    z[96]=n<T>(1,2)*z[10];
    z[97]= - z[79] + z[96];
    z[97]=z[10]*z[97];
    z[89]=z[90] + 5*z[97] + n<T>(1,2)*z[89] + z[95];
    z[89]=z[89]*z[94];
    z[81]=z[81] + z[79];
    z[81]=5*z[81] - z[82];
    z[90]=n<T>(1,4)*z[3];
    z[81]= - n<T>(5,6)*z[2] + n<T>(1,12)*z[7] - z[92] + n<T>(5,12)*z[8] + n<T>(1,3)*
    z[81] + z[90];
    z[81]=z[2]*z[81];
    z[92]=z[79] - z[5];
    z[95]=n<T>(1,2)*z[92];
    z[97]= - z[95] - z[6];
    z[97]=z[6]*z[97];
    z[91]=z[91] + z[97];
    z[97]=z[85] + z[79];
    z[97]= - 5*z[97] + z[93];
    z[97]=n<T>(5,6)*z[8] + n<T>(1,3)*z[97] - z[78];
    z[97]=z[8]*z[97];
    z[95]=z[95] + z[3];
    z[95]=z[3]*z[95];
    z[98]=z[92] + z[9];
    z[100]=z[8] - z[3] - z[98];
    z[100]=z[9]*z[100];
    z[103]=z[7] + z[6] + z[92] - z[8];
    z[103]=z[7]*z[103];
    z[81]=z[81] + n<T>(1,6)*z[103] + n<T>(1,2)*z[100] + z[97] + n<T>(1,3)*z[91] + 
    z[95];
    z[81]=z[2]*z[81];
    z[85]=z[78] - z[85];
    z[91]=n<T>(19,6)*z[8];
    z[95]= - z[91] + z[93] + n<T>(11,3)*z[79] + z[85];
    z[95]=n<T>(1,2)*z[95] - z[7];
    z[95]=z[7]*z[95];
    z[97]= - n<T>(21,4)*z[6] - n<T>(21,2)*z[79] + z[5];
    z[97]=z[6]*z[97];
    z[97]= - 15*z[87] + z[97];
    z[100]= - z[82] + z[92];
    z[100]=z[3]*z[100];
    z[103]= - n<T>(1,6)*z[8] + n<T>(1,3)*z[79] + z[6];
    z[103]=z[8]*z[103];
    z[104]= - n<T>(23,8)*z[9] - n<T>(19,4)*z[92] + z[3];
    z[104]=z[9]*z[104];
    z[105]= - n<T>(31,2)*z[6] + n<T>(109,6)*z[79] - 15*z[5];
    z[105]=z[10] - n<T>(23,2)*z[9] - n<T>(19,12)*z[8] + n<T>(1,2)*z[105] + 19*z[3];
    z[105]= - n<T>(5,4)*z[2] + n<T>(2,3)*z[7] + n<T>(1,4)*z[105];
    z[105]=z[10]*z[105];
    z[95]=z[105] + z[95] + z[104] + n<T>(19,8)*z[103] + n<T>(1,4)*z[97] + z[100]
   ;
    z[95]=z[10]*z[95];
    z[97]=n<T>(11,8)*z[6] + n<T>(11,4)*z[79] - z[5];
    z[82]=z[97]*z[82];
    z[97]=n<T>(7,2)*z[3] + 11*z[92] + z[6];
    z[97]=z[97]*z[90];
    z[85]= - n<T>(1,4)*z[8] - z[79] + z[85];
    z[85]=z[8]*z[85];
    z[100]=n<T>(3,16)*z[9] + z[88] + n<T>(31,8)*z[3] + n<T>(27,32)*z[6] + n<T>(43,32)*
    z[79] - z[5];
    z[100]=z[9]*z[100];
    z[103]=2*z[79];
    z[104]=z[103] - z[5];
    z[104]=z[5]*z[104];
    z[82]=z[100] + z[85] + z[97] + z[104] + z[82];
    z[82]=z[9]*z[82];
    z[80]=z[83] + z[80];
    z[80]=z[3]*z[80];
    z[83]= - n<T>(19,2)*z[79] + z[5];
    z[83]=n<T>(1,3)*z[83] + n<T>(7,2)*z[8];
    z[83]=z[83]*z[88];
    z[85]=3*z[79];
    z[91]=n<T>(23,18)*z[7] + z[91] + n<T>(3,2)*z[3] - n<T>(43,6)*z[6] - z[85] + n<T>(13,3)*z[5];
    z[91]=z[7]*z[91];
    z[97]= - n<T>(17,24)*z[6] + z[103] - n<T>(13,6)*z[5];
    z[97]=z[6]*z[97];
    z[80]=n<T>(1,4)*z[91] + z[83] + z[80] + n<T>(13,6)*z[87] + z[97];
    z[80]=z[7]*z[80];
    z[83]=z[90] - n<T>(5,2)*z[79] + z[5];
    z[83]=z[3]*z[83];
    z[90]= - 3*z[8] + n<T>(5,2)*z[3] - n<T>(65,12)*z[6] + z[85] - n<T>(5,6)*z[5];
    z[90]=z[90]*z[88];
    z[91]=n<T>(65,4)*z[79] - z[5];
    z[91]=n<T>(1,3)*z[91] - n<T>(19,8)*z[6];
    z[91]=z[6]*z[91];
    z[83]=z[90] + z[83] - n<T>(5,6)*z[87] + z[91];
    z[83]=z[8]*z[83];
    z[87]= - z[101] - 13*z[79] + 27*z[5];
    z[87]=n<T>(1,2)*z[87] - n<T>(143,3)*z[3];
    z[87]=z[3]*z[87];
    z[90]= - 25*z[79] + 23*z[5];
    z[90]=z[5]*z[90];
    z[91]= - 3*z[92] - z[6];
    z[91]=z[6]*z[91];
    z[87]=z[87] + z[90] + z[91];
    z[87]=z[87]*z[78];
    z[90]=n<T>(347,6)*z[6] + n<T>(41,4)*z[79] + 11*z[5];
    z[90]=z[90]*z[93];
    z[91]=z[102] - n<T>(25,4)*z[5];
    z[91]=z[5]*z[91];
    z[90]=z[91] + z[90];
    z[90]=z[6]*z[90];
    z[85]= - z[85] + n<T>(1,3)*z[5];
    z[85]=z[85]*npow(z[5],2);
    z[85]=z[87] + n<T>(1,4)*z[85] + n<T>(1,3)*z[90];
    z[83]=z[56] + z[68] + n<T>(1,2)*z[85] + z[83];
    z[85]=z[79]*z[5];
    z[85]=z[85] + z[29];
    z[87]=z[6]*z[79];
    z[87]=z[87] + z[85];
    z[87]=z[15]*z[87];
    z[90]= - z[6] + z[92];
    z[90]=z[22]*z[90];
    z[91]= - z[9] - z[11];
    z[91]=z[41]*z[91];
    z[87]= - z[87] - z[90] - z[91] + z[77] + z[76] - z[31];
    z[90]=n<T>(1,2)*z[2];
    z[86]= - z[90] - z[86];
    z[86]=z[19]*z[86];
    z[90]=z[90] - z[79];
    z[88]= - z[88] - z[90];
    z[88]=z[24]*z[88];
    z[86]=z[86] + z[88];
    z[88]=z[2] - z[8];
    z[91]=z[88] + n<T>(17,2)*z[92] - 20*z[6];
    z[78]= - 2*z[11] + z[96] + n<T>(61,12)*z[7] - z[78] - n<T>(1,3)*z[91];
    z[78]=z[23]*z[78];
    z[91]=z[79]*z[8];
    z[91]=z[91] + z[29];
    z[93]=z[9]*z[79];
    z[93]=z[93] + z[91];
    z[93]=z[17]*z[93];
    z[96]=z[79] - z[8];
    z[97]= - z[9] + z[96];
    z[97]=z[26]*z[97];
    z[93]=z[93] + z[97];
    z[97]= - z[7]*z[79];
    z[91]=z[97] - z[91];
    z[91]=z[16]*z[91];
    z[96]= - z[7] + z[96];
    z[96]=z[25]*z[96];
    z[91]=z[91] + z[96];
    z[96]=z[79]*z[4];
    z[96]=z[96] + z[29];
    z[97]=z[10]*z[79];
    z[97]=z[97] + z[96];
    z[97]=z[14]*z[97];
    z[100]=z[79] - z[4];
    z[101]= - z[10] + z[100];
    z[101]=z[21]*z[101];
    z[97]= - z[42] + z[97] + z[101];
    z[101]=z[11]*z[79];
    z[96]=z[101] + z[96];
    z[96]=z[13]*z[96];
    z[100]= - z[11] + z[100];
    z[100]=z[27]*z[100];
    z[96]=z[96] + z[100];
    z[79]= - z[3]*z[79];
    z[79]=z[79] - z[85];
    z[79]=z[12]*z[79];
    z[85]=z[92] - z[3];
    z[85]=z[20]*z[85];
    z[79]=z[79] + z[85];
    z[85]=z[11] - 5*z[9] - z[6] - z[99] + z[88];
    z[85]=z[38]*z[85];
    z[88]= - z[94] - z[90];
    z[88]=z[18]*z[88];
    z[90]=z[45] - z[63] + z[59] + z[54] - z[51];
    z[92]= - z[70] + z[55] + z[47] - z[34];
    z[94]=z[65] - z[61];
    z[99]=z[66] - z[48];
    z[100]=z[72] - z[44];
    z[101]=z[75] + z[69];
    z[102]= - n<T>(25,3)*z[35] - n<T>(5,3)*z[33] - n<T>(179,288)*z[30];
    z[102]=i*z[102];
    z[103]= - 107*z[5] + 223*z[6];
    z[103]= - n<T>(601,18)*z[7] + 29*z[9] + n<T>(41,9)*z[8] + n<T>(1,9)*z[103] - 27*
    z[3];
    z[103]=n<T>(25,36)*z[11] + n<T>(29,32)*z[4] + n<T>(529,144)*z[10] + n<T>(1,16)*
    z[103] - n<T>(10,9)*z[2];
    z[103]=z[29]*z[103];
    z[98]=z[3] - n<T>(5,6)*z[6] - z[98];
    z[98]= - n<T>(25,24)*z[11] + n<T>(1,2)*z[98] - z[7];
    z[98]=z[28]*z[98];
    z[104]= - n<T>(37,4)*z[10] - z[7] - z[9] + z[6] - n<T>(33,4)*z[3];
    z[104]=z[39]*z[104];
    z[105]=z[7] + z[10];
    z[105]=z[40]*z[105];

    r += n<T>(10,9)*z[32] + n<T>(25,6)*z[36] - 2*z[37] + n<T>(5,4)*z[43] - n<T>(7,4)*
      z[46] - n<T>(5,24)*z[49] + n<T>(7,8)*z[50] + n<T>(47,8)*z[52] - n<T>(17,8)*z[53]
       - n<T>(19,12)*z[57] - n<T>(73,48)*z[58] - n<T>(29,6)*z[60] + n<T>(19,48)*z[62]
       - n<T>(37,16)*z[64] + n<T>(61,96)*z[67] + n<T>(11,32)*z[71] + n<T>(27,32)*z[73]
       - n<T>(3,4)*z[74] + z[78] + n<T>(21,8)*z[79] + z[80] + z[81] + z[82] + n<T>(1,2)*z[83]
     + z[84]
     + z[85]
     + n<T>(5,3)*z[86] - n<T>(1,8)*z[87]
     + 5*z[88]
       + z[89] - n<T>(5,8)*z[90] + n<T>(23,12)*z[91] - n<T>(5,6)*z[92] + n<T>(3,2)*
      z[93] + n<T>(1,6)*z[94] + z[95] + n<T>(7,24)*z[96] + n<T>(5,2)*z[97] + z[98]
       + n<T>(9,8)*z[99] + n<T>(3,8)*z[100] - n<T>(1,4)*z[101] + z[102] + z[103]
     +  z[104] + n<T>(10,3)*z[105];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1498(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1498(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
