#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1090(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[42];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[17];
    z[17]=c[31];
    z[18]=e[6];
    z[19]=f[30];
    z[20]=f[31];
    z[21]=f[32];
    z[22]=f[33];
    z[23]=f[35];
    z[24]=f[36];
    z[25]=f[39];
    z[26]=f[43];
    z[27]=f[68];
    z[28]=z[1]*i;
    z[29]=2*z[28];
    z[30]= - 2*z[4] - z[3];
    z[30]=z[30]*z[29];
    z[31]=z[28] - z[5];
    z[32]=5*z[4];
    z[33]=z[32] + z[3];
    z[33]=n<T>(1,2)*z[33] + 3*z[31];
    z[33]=z[5]*z[33];
    z[34]=npow(z[4],2);
    z[35]=2*z[3];
    z[36]= - n<T>(5,2)*z[4] + z[35];
    z[36]=z[3]*z[36];
    z[37]=z[3] - z[4];
    z[38]= - z[8]*z[37];
    z[30]=n<T>(1,2)*z[33] + z[38] + z[30] + 2*z[34] + z[36];
    z[30]=z[5]*z[30];
    z[33]= - z[4] + n<T>(3,2)*z[3];
    z[36]= - z[33]*z[28];
    z[38]=n<T>(1,2)*z[28];
    z[39]= - n<T>(1,4)*z[8] + z[38] + z[33];
    z[39]=z[8]*z[39];
    z[33]=z[7] - n<T>(1,2)*z[8] + z[38] - z[33];
    z[38]=n<T>(1,2)*z[7];
    z[33]=z[33]*z[38];
    z[40]=npow(z[3],2);
    z[33]=z[33] + z[39] - n<T>(3,4)*z[40] + z[36];
    z[33]=z[7]*z[33];
    z[36]=z[7] + z[4];
    z[36]=z[18]*z[36];
    z[33]= - z[19] + z[33] + z[36];
    z[36]=z[34] - n<T>(3,2)*z[40];
    z[39]= - 3*z[4] + n<T>(17,2)*z[3];
    z[39]=z[39]*z[28];
    z[40]=z[32] - n<T>(17,4)*z[3];
    z[40]=z[8]*z[40];
    z[36]=z[40] + n<T>(3,2)*z[36] + z[39];
    z[36]=z[8]*z[36];
    z[39]= - z[4] + n<T>(17,3)*z[3];
    z[39]=z[3]*z[39];
    z[39]= - n<T>(13,2)*z[34] + z[39];
    z[39]=z[3]*z[39];
    z[40]=z[4] + n<T>(3,4)*z[3];
    z[40]=z[3]*z[40];
    z[40]= - n<T>(1,2)*z[34] + 3*z[40];
    z[40]=z[40]*z[28];
    z[41]=npow(z[4],3);
    z[36]=z[36] + z[40] + n<T>(1,3)*z[41] + z[39];
    z[39]=z[3] + z[4];
    z[39]=n<T>(1,2)*z[39];
    z[40]=z[39] + z[5];
    z[40]=z[28]*z[40];
    z[40]=z[13] + z[40];
    z[40]=z[6]*z[40];
    z[39]= - z[39] + z[31];
    z[39]=z[10]*z[39];
    z[39]= - z[21] + z[40] + z[39];
    z[40]=z[8] + z[4];
    z[41]= - z[28]*z[40];
    z[41]= - z[13] + z[41];
    z[41]=z[9]*z[41];
    z[28]=z[28] - z[40];
    z[28]=z[12]*z[28];
    z[28]=z[41] + z[28];
    z[31]= - n<T>(1,2)*z[2] + z[8] - z[31];
    z[31]=z[37]*z[31];
    z[35]=z[4] - z[35];
    z[35]=z[3]*z[35];
    z[31]=z[34] + z[35] + z[31];
    z[31]=z[2]*z[31];
    z[34]= - z[8] + z[2] - z[5];
    z[29]= - z[29] + z[32] + n<T>(11,2)*z[3] - 2*z[34];
    z[29]=z[11]*z[29];
    z[32]= - n<T>(7,6)*z[8] - n<T>(19,3)*z[4] + 11*z[3];
    z[32]=z[38] + n<T>(1,4)*z[32] + z[5];
    z[32]=z[13]*z[32];
    z[34]=z[24] - z[17];
    z[35]=n<T>(3,2)*z[16] - n<T>(13,24)*z[14];
    z[35]=i*z[35];

    r += n<T>(5,4)*z[15] - n<T>(25,8)*z[20] - 5*z[22] - z[23] - n<T>(9,8)*z[25] + 
      z[26] + z[27] + n<T>(7,2)*z[28] + z[29] + z[30] + z[31] + z[32] + n<T>(3,2)*z[33]
     + n<T>(3,8)*z[34]
     + z[35]
     + n<T>(1,2)*z[36]
     + 3*z[39];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1090(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1090(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
