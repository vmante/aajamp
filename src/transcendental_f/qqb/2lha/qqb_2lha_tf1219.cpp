#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1219(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[19];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[15];
    z[3]=d[18];
    z[4]=d[0];
    z[5]=d[1];
    z[6]=d[2];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[5];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[2];
    z[13]=e[8];
    z[14]=e[11];
    z[15]=npow(z[5],2);
    z[16]= - z[3] + z[2];
    z[16]=z[1]*i*z[16];
    z[17]=z[6] + z[8];
    z[18]=n<T>(1,2)*z[7] - z[17];
    z[18]=z[7]*z[18];
    z[17]= - z[4] + z[7] - z[5] + z[17];
    z[17]=z[4]*z[17];

    r +=  - n<T>(11,2)*z[9] - z[10] + z[11] + z[12] - 2*z[13] + z[14] + n<T>(1,2)*z[15]
     + z[16]
     + z[17]
     + z[18];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1219(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1219(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
