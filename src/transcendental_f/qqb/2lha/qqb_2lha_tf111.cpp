#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf111(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=f[22];
    z[4]=f[42];
    z[5]=f[44];
    z[6]=f[69];
    z[7]=f[70];
    z[8]=f[79];
    z[9]=f[80];
    z[10]=z[8] + z[7];
    z[11]=i*z[1];

    r += n<T>(5,2)*z[2] + 7*z[3] - 4*z[4] - 11*z[5] + n<T>(1,4)*z[6] - z[9] - n<T>(1,2)*z[10]
     - n<T>(5,54)*z[11];
 
    return r;
}

template std::complex<double> qqb_2lha_tf111(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf111(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
