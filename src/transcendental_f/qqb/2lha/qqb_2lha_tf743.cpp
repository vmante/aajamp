#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf743(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[87];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[17];
    z[12]=d[15];
    z[13]=d[12];
    z[14]=d[18];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[11];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[17];
    z[28]=c[18];
    z[29]=c[19];
    z[30]=c[21];
    z[31]=c[23];
    z[32]=c[25];
    z[33]=c[26];
    z[34]=c[28];
    z[35]=c[29];
    z[36]=c[31];
    z[37]=e[3];
    z[38]=e[6];
    z[39]=f[2];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[6];
    z[44]=f[7];
    z[45]=f[8];
    z[46]=f[10];
    z[47]=f[11];
    z[48]=f[12];
    z[49]=f[14];
    z[50]=f[16];
    z[51]=f[17];
    z[52]=f[21];
    z[53]=f[23];
    z[54]=f[30];
    z[55]=f[31];
    z[56]=f[32];
    z[57]=f[33];
    z[58]=f[35];
    z[59]=f[36];
    z[60]=f[37];
    z[61]=f[39];
    z[62]=f[40];
    z[63]=f[43];
    z[64]=n<T>(1,2)*z[9];
    z[65]=z[1]*i;
    z[66]=z[64] - z[65];
    z[67]=z[66]*z[9];
    z[68]=z[65] + z[64];
    z[69]=n<T>(1,9)*z[5];
    z[68]=z[68]*z[69];
    z[70]=n<T>(1,2)*z[2];
    z[71]=z[70] - z[5];
    z[72]=z[65] - z[9];
    z[73]=z[72] + z[71];
    z[73]=z[2]*z[73];
    z[71]= - z[64] - z[71];
    z[71]=n<T>(1,3)*z[71] + n<T>(1,2)*z[8];
    z[74]=n<T>(1,3)*z[8];
    z[71]=z[71]*z[74];
    z[75]=n<T>(1,2)*z[6];
    z[76]=z[75] + z[72];
    z[76]=z[6]*z[76];
    z[77]=z[2] - z[5];
    z[78]=z[8] - n<T>(5,8)*z[72] + z[77];
    z[78]= - n<T>(5,9)*z[7] + n<T>(1,3)*z[78] + n<T>(1,8)*z[6];
    z[78]=z[7]*z[78];
    z[68]=n<T>(1,3)*z[78] + n<T>(1,12)*z[76] + z[71] + n<T>(1,18)*z[73] + n<T>(1,4)*
    z[67] + z[68];
    z[68]=z[7]*z[68];
    z[71]=n<T>(1,2)*z[5];
    z[73]=z[71] - z[65];
    z[76]=z[9] - z[73];
    z[76]=z[5]*z[76];
    z[78]= - 3*z[65] + z[70];
    z[78]=z[2]*z[78];
    z[79]=z[65]*z[3];
    z[80]=n<T>(3,2)*z[8] - z[9] - z[65] - z[2];
    z[80]=z[8]*z[80];
    z[81]= - z[8] - z[72];
    z[81]=z[6]*z[81];
    z[76]=z[79] + z[81] + z[80] + z[78] - z[67] + z[76];
    z[78]=z[3] - z[8] + z[5] + z[9];
    z[78]= - n<T>(1,3)*z[4] + z[70] + z[65] - n<T>(1,4)*z[78];
    z[78]=z[4]*z[78];
    z[76]=n<T>(1,2)*z[76] + z[78];
    z[76]=z[4]*z[76];
    z[69]=z[69] - n<T>(1,9)*z[65] - z[64];
    z[69]=z[5]*z[69];
    z[67]= - z[67] + z[69];
    z[67]=z[5]*z[67];
    z[69]=z[9] + z[10];
    z[69]=z[65]*z[69];
    z[69]=z[24] + z[69];
    z[69]=z[14]*z[69];
    z[78]= - 7*z[65] - n<T>(11,3)*z[9];
    z[78]=z[78]*npow(z[9],2);
    z[80]=z[8] + z[65] - z[2];
    z[80]=z[20]*z[80];
    z[81]= - z[10] + z[72];
    z[81]=z[23]*z[81];
    z[67]= - z[43] + z[80] + z[69] + n<T>(1,18)*z[78] + z[67] + z[81] - 
    z[52];
    z[69]=n<T>(17,4)*z[2] + z[71] + 4*z[65] + z[64];
    z[78]=n<T>(1,3)*z[2];
    z[69]=z[69]*z[78];
    z[80]=n<T>(7,3)*z[65] - z[9];
    z[80]=z[80]*z[64];
    z[71]= - z[71] - z[66];
    z[71]=z[5]*z[71];
    z[81]= - z[5] - n<T>(19,2)*z[65] + 5*z[9];
    z[81]= - n<T>(7,6)*z[8] + n<T>(1,2)*z[81] - 5*z[2];
    z[81]=z[81]*z[74];
    z[69]=z[81] + z[69] + z[80] + n<T>(1,3)*z[71];
    z[69]=z[69]*z[74];
    z[71]=2*z[5];
    z[74]=2*z[9];
    z[80]=n<T>(7,4)*z[2] - z[71] + n<T>(1,2)*z[65] - z[74];
    z[78]=z[80]*z[78];
    z[80]=n<T>(1,3)*z[9];
    z[81]= - n<T>(77,36)*z[3] + n<T>(13,12)*z[2] + n<T>(1,12)*z[5] - z[80] + z[75]
    + z[65];
    z[81]=z[3]*z[81];
    z[82]=2*z[65];
    z[83]= - z[82] + z[9];
    z[83]=z[83]*z[80];
    z[84]=n<T>(5,3)*z[5] - n<T>(3,2)*z[65] + n<T>(2,3)*z[9];
    z[84]=z[5]*z[84];
    z[85]=z[65] - z[5];
    z[86]=n<T>(5,4)*z[6] + z[85];
    z[86]=z[6]*z[86];
    z[78]=z[81] + z[86] + z[78] + z[83] + z[84];
    z[78]=z[3]*z[78];
    z[81]= - z[66]*z[64];
    z[83]=n<T>(31,4)*z[65];
    z[84]= - n<T>(31,8)*z[5] + z[83] - z[74];
    z[84]=z[5]*z[84];
    z[86]=z[82] - z[64];
    z[71]= - n<T>(1,4)*z[2] + z[71] - z[86];
    z[71]=z[2]*z[71];
    z[74]= - n<T>(5,4)*z[3] - 2*z[2] + z[74] - n<T>(5,2)*z[85];
    z[74]=z[3]*z[74];
    z[71]=z[74] + z[71] + z[81] + z[84];
    z[74]= - z[75] - z[85];
    z[75]=n<T>(7,4)*z[6];
    z[74]=z[74]*z[75];
    z[81]= - n<T>(31,4)*z[5] + z[83] - z[2] + z[9];
    z[75]=n<T>(13,3)*z[3] + n<T>(1,3)*z[81] - z[75];
    z[75]=n<T>(1,2)*z[75] - n<T>(1,9)*z[10];
    z[75]=z[10]*z[75];
    z[71]=z[75] + z[74] + n<T>(1,3)*z[71];
    z[71]=z[10]*z[71];
    z[73]=z[73]*z[5];
    z[74]= - z[9]*z[86];
    z[64]= - n<T>(11,12)*z[2] + z[82] + z[64];
    z[64]=z[2]*z[64];
    z[64]=z[64] + z[74] + z[73];
    z[64]=z[2]*z[64];
    z[74]=z[77] + z[72];
    z[75]= - n<T>(4,3)*z[7] + n<T>(1,3)*z[74] - z[8];
    z[75]=z[19]*z[75];
    z[74]= - n<T>(31,2)*z[10] + 4*z[74] - n<T>(23,2)*z[3];
    z[74]=z[22]*z[74];
    z[77]= - n<T>(47,3)*z[9] - 7*z[5];
    z[77]=n<T>(1,6)*z[77] - 7*z[2];
    z[77]= - n<T>(1,2)*z[7] + n<T>(13,4)*z[4] + n<T>(11,2)*z[10] - n<T>(7,6)*z[3]
     + n<T>(65,36)*z[6]
     + n<T>(1,4)*z[77] - n<T>(7,9)*z[8];
    z[77]=z[24]*z[77];
    z[74]=z[74] + z[77] + z[75];
    z[66]=z[66]*z[80];
    z[66]=z[66] - 3*z[73];
    z[73]=z[2]*z[72];
    z[66]=n<T>(1,2)*z[66] + z[73];
    z[73]= - n<T>(11,36)*z[8] - n<T>(1,3)*z[65] + z[70];
    z[73]=z[8]*z[73];
    z[75]=z[65] + n<T>(1,8)*z[9];
    z[75]=n<T>(49,108)*z[6] - n<T>(7,18)*z[8] + n<T>(1,3)*z[75] - n<T>(3,8)*z[5];
    z[75]=z[6]*z[75];
    z[66]=z[75] + n<T>(1,2)*z[66] + z[73];
    z[66]=z[6]*z[66];
    z[73]=z[65]*z[5];
    z[73]=z[73] + z[24];
    z[75]= - z[7]*z[65];
    z[75]=z[75] - z[73];
    z[75]=z[13]*z[75];
    z[77]=z[7] - z[85];
    z[77]=z[18]*z[77];
    z[75]=z[75] + z[77] + z[58] + z[56];
    z[73]= - z[79] - z[73];
    z[73]=z[12]*z[73];
    z[77]= - z[3] + z[85];
    z[77]=z[17]*z[77];
    z[73]=z[73] + z[77];
    z[77]=n<T>(1,3)*z[6] + n<T>(1,9)*z[8] + z[4] - z[2];
    z[79]=n<T>(4,9)*z[9] + z[77];
    z[79]=z[65]*z[79];
    z[79]=n<T>(4,9)*z[24] + z[79];
    z[79]=z[11]*z[79];
    z[72]= - n<T>(4,9)*z[72] + z[77];
    z[72]=z[21]*z[72];
    z[65]= - n<T>(1,2)*z[4] + z[65] - z[70];
    z[65]=z[15]*z[65];
    z[70]= - z[40] + z[60] - z[51] + z[49] - z[47];
    z[77]= - z[28] + z[53] - z[46];
    z[80]=z[41] - z[26];
    z[81]=z[62] + z[57];
    z[83]= - n<T>(7,2)*z[33] - n<T>(7,4)*z[31] + n<T>(1,2)*z[30] + z[27] - n<T>(23,216)*
    z[25];
    z[83]=i*z[83];
    z[82]= - z[2] + z[82] - z[5];
    z[82]=z[16]*z[82];
    z[84]= - z[6] - z[3];
    z[84]=z[37]*z[84];
    z[85]=z[8] + z[6];
    z[85]=z[38]*z[85];

    r += n<T>(13,48)*z[29] + n<T>(19,24)*z[32] + n<T>(7,4)*z[34] + n<T>(1,8)*z[35]
     + n<T>(7,48)*z[36] - z[39] - n<T>(5,8)*z[42] - n<T>(16,3)*z[44]
     + z[45]
     + n<T>(3,8)*
      z[48] - n<T>(7,8)*z[50] + n<T>(7,18)*z[54] + n<T>(13,72)*z[55] + n<T>(29,72)*
      z[59] + n<T>(1,24)*z[61] - n<T>(1,18)*z[63] + z[64] + z[65] + z[66] + n<T>(1,2)*z[67]
     + z[68]
     + z[69]
     + n<T>(1,4)*z[70]
     + z[71]
     + z[72]
     + n<T>(11,6)*
      z[73] + n<T>(1,3)*z[74] + n<T>(1,9)*z[75] + z[76] + n<T>(1,6)*z[77] + z[78]
       + z[79] - n<T>(35,24)*z[80] + n<T>(4,9)*z[81] + z[82] + z[83] + n<T>(3,2)*
      z[84] + n<T>(11,18)*z[85];
 
    return r;
}

template std::complex<double> qqb_2lha_tf743(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf743(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
