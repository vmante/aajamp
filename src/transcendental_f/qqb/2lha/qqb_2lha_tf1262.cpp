#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1262(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[45];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[9];
    z[7]=d[12];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[17];
    z[11]=e[5];
    z[12]=e[7];
    z[13]=e[9];
    z[14]=e[13];
    z[15]=c[3];
    z[16]=c[11];
    z[17]=c[15];
    z[18]=c[31];
    z[19]=e[6];
    z[20]=f[10];
    z[21]=f[30];
    z[22]=f[31];
    z[23]=f[32];
    z[24]=f[33];
    z[25]=f[35];
    z[26]=f[36];
    z[27]=f[39];
    z[28]=f[43];
    z[29]=5*z[3];
    z[30]=2*z[4];
    z[31]= - z[29] + z[30];
    z[32]=z[1]*i;
    z[31]=z[31]*z[32];
    z[33]=npow(z[3],2);
    z[34]=n<T>(3,2)*z[33];
    z[35]=npow(z[4],2);
    z[36]=z[4] - z[3];
    z[37]= - z[5]*z[36];
    z[38]=n<T>(5,2)*z[3] - 3*z[4];
    z[38]=z[9]*z[38];
    z[31]=z[38] + z[37] + z[31] + z[34] - z[35];
    z[31]=z[9]*z[31];
    z[35]=z[4] + z[3];
    z[37]=z[35]*z[32];
    z[38]=n<T>(5,2)*z[33];
    z[39]=z[3] - z[30];
    z[39]=z[4]*z[39];
    z[40]=z[32] - z[5];
    z[41]=n<T>(1,2)*z[3] - z[4] - n<T>(3,2)*z[40];
    z[41]=z[5]*z[41];
    z[39]=z[41] + 2*z[37] - z[38] + z[39];
    z[39]=z[5]*z[39];
    z[41]= - z[30] + 3*z[3];
    z[42]=n<T>(1,2)*z[9];
    z[43]=z[42] - z[32] - z[41];
    z[43]=z[9]*z[43];
    z[44]=z[8] + z[4];
    z[42]=z[42] - n<T>(1,2)*z[32] + n<T>(3,2)*z[3] - z[44];
    z[42]=z[8]*z[42];
    z[41]=z[41]*z[32];
    z[34]=z[42] + z[43] + z[34] + z[41];
    z[34]=z[8]*z[34];
    z[41]=z[9] + z[4];
    z[42]=z[32]*z[41];
    z[42]=z[15] + z[42];
    z[42]=z[10]*z[42];
    z[41]= - z[32] + z[41];
    z[41]=z[13]*z[41];
    z[41]=z[42] + z[41];
    z[42]=z[40] - z[9];
    z[43]= - n<T>(1,2)*z[2] - z[42];
    z[36]=z[36]*z[43];
    z[43]= - z[4]*z[35];
    z[36]=2*z[33] + z[43] + z[36];
    z[36]=z[2]*z[36];
    z[35]= - z[5] + z[35] + 2*z[32];
    z[35]=z[5]*z[35];
    z[43]= - z[4]*z[3];
    z[35]=z[35] + z[43] - z[37];
    z[35]=z[6]*z[35];
    z[29]=z[2] - z[29] - 4*z[4] + z[42];
    z[29]=z[12]*z[29];
    z[37]=z[19]*z[44];
    z[29]=z[21] + z[29] - z[37];
    z[37]= - z[3] - z[5];
    z[37]=z[32]*z[37];
    z[37]= - z[15] + z[37];
    z[37]=z[7]*z[37];
    z[42]=z[3] - z[40];
    z[42]=z[11]*z[42];
    z[37]=z[23] + z[37] + z[42];
    z[42]=2*z[3] - n<T>(1,3)*z[4];
    z[30]=z[42]*z[30];
    z[30]=z[38] + z[30];
    z[30]=z[4]*z[30];
    z[32]= - z[33]*z[32];
    z[33]=z[27] - z[18];
    z[38]=npow(z[3],3);
    z[42]=z[8] + z[5];
    z[42]=z[6] + n<T>(1,3)*z[9] - n<T>(7,2)*z[3] + z[4] - n<T>(2,3)*z[42];
    z[42]=z[15]*z[42];
    z[40]=z[4] + z[40];
    z[40]=z[14]*z[40];
    z[43]=n<T>(1,2)*i;
    z[43]=z[16]*z[43];

    r +=  - z[17] + z[20] + n<T>(7,2)*z[22] + 8*z[24] + z[25] - n<T>(1,2)*z[26]
       - z[28] + 2*z[29] + z[30] + z[31] + z[32] + n<T>(3,2)*z[33] + z[34]
       + z[35] + z[36] + 3*z[37] - n<T>(13,3)*z[38] + z[39] + z[40] + 4*
      z[41] + z[42] + z[43];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1262(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1262(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
