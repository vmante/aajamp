#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1332(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[50];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[11];
    z[6]=d[4];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[9];
    z[10]=d[12];
    z[11]=d[17];
    z[12]=e[9];
    z[13]=e[12];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[31];
    z[18]=d[3];
    z[19]=e[6];
    z[20]=e[5];
    z[21]=e[7];
    z[22]=e[13];
    z[23]=e[14];
    z[24]=f[2];
    z[25]=f[30];
    z[26]=f[31];
    z[27]=f[32];
    z[28]=f[36];
    z[29]=f[39];
    z[30]=f[50];
    z[31]=f[51];
    z[32]=f[55];
    z[33]=f[58];
    z[34]=z[1]*i;
    z[35]=z[34]*z[9];
    z[36]= - z[2]*z[34];
    z[36]= - z[14] - z[35] + z[36];
    z[36]=z[5]*z[36];
    z[37]=z[8] + z[7];
    z[38]=z[34]*z[37];
    z[38]=z[14] + z[38];
    z[38]=z[11]*z[38];
    z[37]= - z[34] + z[37];
    z[37]=z[12]*z[37];
    z[39]=z[2] - z[34] + z[9];
    z[39]=z[13]*z[39];
    z[40]= - z[7] - z[6];
    z[40]=z[19]*z[40];
    z[41]=z[9] + z[4];
    z[41]=z[23]*z[41];
    z[36]=z[40] + z[41] - z[30] + z[25] + z[36] + z[38] + z[37] + z[39];
    z[37]=n<T>(1,3)*z[34];
    z[38]=z[37] + z[3];
    z[39]=n<T>(1,6)*z[6];
    z[40]=n<T>(2,3)*z[7];
    z[41]=z[39] + z[40] - z[38];
    z[41]=z[6]*z[41];
    z[42]=n<T>(1,2)*z[3];
    z[39]=z[39] + z[42] - z[40];
    z[39]=z[8]*z[39];
    z[43]=npow(z[3],2);
    z[43]=n<T>(1,2)*z[43];
    z[44]=z[34]*z[3];
    z[45]=z[43] - z[44];
    z[46]=n<T>(1,3)*z[7];
    z[47]=2*z[34] - z[7];
    z[47]=z[47]*z[46];
    z[39]=z[39] + z[41] + z[47] + z[45];
    z[39]=z[8]*z[39];
    z[41]=z[37] - z[3];
    z[41]=n<T>(1,2)*z[41];
    z[47]=n<T>(1,3)*z[9];
    z[48]=n<T>(1,6)*z[2];
    z[49]=n<T>(1,3)*z[4] - z[48] + z[41] + z[47];
    z[49]=z[4]*z[49];
    z[43]=z[43] + z[44];
    z[38]= - z[48] - n<T>(2,3)*z[9] + z[38];
    z[38]=z[2]*z[38];
    z[38]=z[49] + z[38] + n<T>(2,3)*z[35] - z[43];
    z[38]=z[4]*z[38];
    z[40]= - z[34]*z[40];
    z[41]= - n<T>(1,3)*z[6] - z[41] - z[46];
    z[41]=z[6]*z[41];
    z[40]=z[41] + z[40] + z[43];
    z[40]=z[6]*z[40];
    z[37]=n<T>(1,3)*z[2] + z[47] - z[42] - z[37];
    z[37]=z[2]*z[37];
    z[37]=z[37] - z[45];
    z[37]=z[2]*z[37];
    z[41]=npow(z[7],2);
    z[45]= - z[9]*z[3];
    z[47]=z[9] + z[3];
    z[48]=z[18]*z[47];
    z[45]=n<T>(1,2)*z[48] - z[41] + z[45];
    z[45]=z[18]*z[45];
    z[48]=z[22] + z[20];
    z[47]=z[47]*z[48];
    z[34]=z[46] + z[3] + n<T>(2,3)*z[34];
    z[34]=z[34]*z[41];
    z[41]=z[9]*z[43];
    z[43]=z[6] - z[4];
    z[42]=n<T>(1,6)*z[8] - n<T>(1,2)*z[2] - n<T>(11,6)*z[9] - z[42] + z[7] - n<T>(2,3)*
    z[43];
    z[42]=z[14]*z[42];
    z[35]= - z[44] - z[35];
    z[35]=z[10]*z[35];
    z[43]=npow(z[3],3);
    z[43]= - z[26] + z[43] + z[33] + z[31] - z[29];
    z[44]=z[32] - z[28];
    z[46]= - z[3] - z[7];
    z[46]=z[21]*z[46];
    z[46]=z[46] + z[24];
    z[48]=z[15]*i;

    r += z[16] - n<T>(3,2)*z[17] + z[27] + z[34] + z[35] + n<T>(2,3)*z[36] + 
      z[37] + z[38] + z[39] + z[40] + z[41] + n<T>(1,3)*z[42] - n<T>(1,2)*z[43]
       + n<T>(1,6)*z[44] + z[45] + 2*z[46] + z[47] - n<T>(1,18)*z[48];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1332(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1332(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
