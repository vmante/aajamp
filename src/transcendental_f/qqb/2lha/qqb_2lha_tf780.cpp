#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf780(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[71];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=d[18];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[4];
    z[16]=e[8];
    z[17]=e[10];
    z[18]=e[11];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[18];
    z[23]=c[19];
    z[24]=c[21];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[29];
    z[30]=c[31];
    z[31]=e[3];
    z[32]=f[0];
    z[33]=f[1];
    z[34]=f[2];
    z[35]=f[3];
    z[36]=f[4];
    z[37]=f[5];
    z[38]=f[6];
    z[39]=f[7];
    z[40]=f[8];
    z[41]=f[10];
    z[42]=f[11];
    z[43]=f[12];
    z[44]=f[14];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[18];
    z[48]=f[20];
    z[49]=f[21];
    z[50]=f[23];
    z[51]=f[26];
    z[52]=7*z[3];
    z[53]=z[1]*i;
    z[54]=z[53] - z[5];
    z[55]=2*z[54] + z[3];
    z[55]=z[55]*z[52];
    z[56]=2*z[2];
    z[57]=z[3] + z[54];
    z[57]=13*z[57] + z[56];
    z[57]=z[2]*z[57];
    z[58]=13*z[5];
    z[59]=4*z[53];
    z[60]=2*z[6] - 4*z[2] - 13*z[3] - z[59] + z[58];
    z[60]=z[6]*z[60];
    z[61]=z[6] - z[2];
    z[62]= - n<T>(5,3)*z[7] - n<T>(103,2)*z[54] - 89*z[3] - 5*z[61];
    z[63]=n<T>(1,2)*z[7];
    z[62]=z[62]*z[63];
    z[64]= - z[53] + n<T>(1,2)*z[5];
    z[64]=z[64]*z[5];
    z[55]=z[62] + z[60] + z[57] + n<T>(103,2)*z[64] + z[55];
    z[55]=z[7]*z[55];
    z[57]=z[61] - z[54];
    z[57]=103*z[7] + 77*z[3] + 26*z[57];
    z[57]=z[17]*z[57];
    z[55]=z[41] + z[55] + z[57];
    z[57]=n<T>(1,3)*z[2];
    z[60]=n<T>(23,2)*z[2] - 52*z[53] - 23*z[5];
    z[60]=z[60]*z[57];
    z[61]=n<T>(1,2)*z[6];
    z[62]= - z[61] + z[53] + z[5];
    z[62]=z[6]*z[62];
    z[65]= - z[3]*z[59];
    z[66]=n<T>(1,3)*z[4] - n<T>(25,4)*z[6] + n<T>(26,3)*z[2] + 2*z[3] - 5*z[53] - n<T>(29,12)*z[5];
    z[66]=z[4]*z[66];
    z[60]=z[66] + n<T>(25,2)*z[62] + z[60] - n<T>(29,6)*z[64] + z[65];
    z[60]=z[4]*z[60];
    z[62]=z[54] + n<T>(1,2)*z[3];
    z[52]= - z[62]*z[52];
    z[63]=z[63] + z[54];
    z[63]=z[7]*z[63];
    z[56]= - n<T>(5,6)*z[8] + 2*z[4] + n<T>(25,4)*z[7] - z[56] - n<T>(11,4)*z[54]
     - 
   9*z[3];
    z[56]=z[8]*z[56];
    z[59]= - z[4]*z[59];
    z[52]=z[56] + z[59] + n<T>(25,2)*z[63] + n<T>(11,2)*z[64] + z[52];
    z[52]=z[8]*z[52];
    z[56]=n<T>(13,3)*z[3];
    z[59]=z[62]*z[56];
    z[62]= - 7*z[53] - 4*z[5];
    z[62]=n<T>(5,2)*z[2] + 2*z[62] + z[56];
    z[62]=z[2]*z[62];
    z[63]=z[6] - z[53] - n<T>(3,2)*z[5];
    z[56]=17*z[2] - z[56] + 3*z[63];
    z[56]=z[56]*z[61];
    z[56]=z[56] + z[62] - n<T>(9,2)*z[64] + z[59];
    z[56]=z[6]*z[56];
    z[59]=z[7] + z[6];
    z[61]= - z[53]*z[59];
    z[61]= - z[19] + z[61];
    z[61]=z[11]*z[61];
    z[59]= - z[53] + z[59];
    z[59]=z[18]*z[59];
    z[59]=z[38] + z[61] + z[59];
    z[61]=z[8] + z[4];
    z[62]=z[53]*z[61];
    z[62]=z[19] + z[62];
    z[62]=z[10]*z[62];
    z[61]=z[53] - z[61];
    z[61]=z[15]*z[61];
    z[61]=z[49] + z[62] + z[61];
    z[58]= - 20*z[3] - z[53] + z[58];
    z[58]=z[3]*z[58];
    z[62]=25*z[3];
    z[63]= - z[62] + 119*z[53] + 47*z[5];
    z[63]=n<T>(1,2)*z[63] - 34*z[2];
    z[63]=z[2]*z[63];
    z[64]=2*z[53];
    z[65]=z[64] - z[5];
    z[66]=z[5]*z[65];
    z[58]=z[63] - 14*z[66] + z[58];
    z[57]=z[58]*z[57];
    z[58]=z[5] + z[3];
    z[58]=z[53]*z[58];
    z[58]=z[19] + z[58];
    z[58]=z[9]*z[58];
    z[54]=z[3] - z[54];
    z[54]=z[14]*z[54];
    z[54]=z[58] + z[54];
    z[58]=11*z[53] - n<T>(34,3)*z[5];
    z[58]=z[5]*z[58];
    z[63]=n<T>(275,18)*z[3] - 6*z[53] - n<T>(7,6)*z[5];
    z[63]=z[3]*z[63];
    z[58]=z[58] + z[63];
    z[58]=z[3]*z[58];
    z[63]=z[47] + z[50] - z[48];
    z[66]=z[40] - z[34];
    z[67]=z[44] - z[33];
    z[68]= - n<T>(185,3)*z[27] - n<T>(55,3)*z[25] - 3*z[24] + n<T>(5,36)*z[20];
    z[68]=i*z[68];
    z[53]= - z[53] + n<T>(1,3)*z[5];
    z[53]=z[53]*npow(z[5],2);
    z[62]=n<T>(115,4)*z[2] + n<T>(41,8)*z[5] + z[62];
    z[62]= - n<T>(83,24)*z[8] - n<T>(49,9)*z[4] - n<T>(95,8)*z[7] + n<T>(1,9)*z[62]
     + n<T>(3,4)*z[6];
    z[62]=z[19]*z[62];
    z[65]= - 11*z[2] - z[65];
    z[65]=n<T>(1,3)*z[65] + 4*z[4];
    z[65]=z[13]*z[65];
    z[64]=z[64] - z[2];
    z[69]= - z[4] + z[64];
    z[69]=z[12]*z[69];
    z[64]= - z[6] + z[64];
    z[64]=z[16]*z[64];
    z[70]=z[3] + z[8];
    z[70]=z[31]*z[70];

    r +=  - 12*z[21] + n<T>(8,3)*z[22] - n<T>(79,36)*z[23] + 10*z[26] + n<T>(97,3)*
      z[28] - 2*z[29] + n<T>(23,2)*z[30] - 15*z[32] - n<T>(17,12)*z[35] + n<T>(125,12)*z[36]
     - n<T>(49,6)*z[37]
     + n<T>(104,3)*z[39] - n<T>(9,4)*z[42] - n<T>(5,4)*
      z[43] + n<T>(25,4)*z[45] - n<T>(27,4)*z[46] + z[51] + z[52] + n<T>(1,2)*z[53]
       + n<T>(35,3)*z[54] + n<T>(1,3)*z[55] + z[56] + z[57] + z[58] + 3*z[59]
       + z[60] + 4*z[61] + z[62] - n<T>(4,3)*z[63] + 14*z[64] + z[65] - 7*
      z[66] - n<T>(3,2)*z[67] + z[68] + n<T>(56,3)*z[69] + 11*z[70];
 
    return r;
}

template std::complex<double> qqb_2lha_tf780(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf780(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
