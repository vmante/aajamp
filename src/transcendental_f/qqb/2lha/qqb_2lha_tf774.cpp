#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf774(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[113];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[12];
    z[15]=d[9];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[18];
    z[33]=c[19];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=c[29];
    z[39]=c[31];
    z[40]=e[3];
    z[41]=e[6];
    z[42]=e[13];
    z[43]=e[14];
    z[44]=f[0];
    z[45]=f[1];
    z[46]=f[2];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[10];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[14];
    z[56]=f[15];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[20];
    z[60]=f[23];
    z[61]=f[30];
    z[62]=f[31];
    z[63]=f[32];
    z[64]=f[33];
    z[65]=f[34];
    z[66]=f[35];
    z[67]=f[36];
    z[68]=f[37];
    z[69]=f[39];
    z[70]=f[43];
    z[71]=f[50];
    z[72]=f[51];
    z[73]=f[54];
    z[74]=f[55];
    z[75]=f[56];
    z[76]=f[58];
    z[77]=z[1]*i;
    z[78]=n<T>(1,3)*z[77];
    z[79]=n<T>(1,3)*z[2];
    z[80]=n<T>(1,2)*z[5];
    z[81]=n<T>(1,4)*z[4];
    z[82]=n<T>(11,36)*z[7] - n<T>(7,6)*z[6] - z[81] + z[79] + z[80] + z[78] + n<T>(1,4)*z[8];
    z[82]=z[7]*z[82];
    z[83]=2*z[2];
    z[84]= - z[83] + n<T>(5,2)*z[5];
    z[85]=n<T>(5,2)*z[8];
    z[86]=z[77] - z[85] - z[84];
    z[86]=z[86]*z[79];
    z[87]=n<T>(1,2)*z[4];
    z[88]=z[87] - z[77];
    z[88]=z[88]*z[87];
    z[89]=n<T>(1,2)*z[8];
    z[90]= - z[77] + n<T>(5,3)*z[8];
    z[90]=z[90]*z[89];
    z[91]=z[80] - z[77];
    z[92]=n<T>(5,6)*z[8] + z[91];
    z[92]=z[5]*z[92];
    z[93]=z[2] - z[8];
    z[94]=n<T>(1,2)*z[9];
    z[95]= - z[93]*z[94];
    z[96]= - n<T>(2,3)*z[6] + n<T>(5,6)*z[2] + n<T>(1,6)*z[8] - z[5];
    z[96]=z[6]*z[96];
    z[82]=z[82] + z[96] - z[88] + z[95] + z[86] + z[90] + z[92];
    z[82]=z[7]*z[82];
    z[86]=z[89] - z[77];
    z[90]= - z[86]*z[89];
    z[92]=11*z[5];
    z[91]= - z[91]*z[92];
    z[90]=z[90] + z[91];
    z[91]=n<T>(2,3)*z[77];
    z[95]=n<T>(1,2)*z[7];
    z[96]=n<T>(1,12)*z[3] + z[95] - n<T>(1,6)*z[5] + z[91] - z[89];
    z[96]=z[3]*z[96];
    z[97]=2*z[5];
    z[98]=5*z[77];
    z[99]= - z[98] + z[8];
    z[99]= - z[9] + n<T>(1,2)*z[99] + z[97];
    z[99]=z[9]*z[99];
    z[100]=n<T>(1,2)*z[6];
    z[101]=z[77] - z[8];
    z[102]= - z[100] - z[101];
    z[102]=z[6]*z[102];
    z[103]=n<T>(1,4)*z[7] + n<T>(2,3)*z[101] - z[94];
    z[103]=z[7]*z[103];
    z[104]=23*z[77] - z[8];
    z[92]=n<T>(1,2)*z[104] - z[92];
    z[92]=n<T>(1,6)*z[92] + z[2];
    z[87]=n<T>(1,12)*z[10] + n<T>(23,12)*z[3] + n<T>(1,3)*z[7] - n<T>(3,8)*z[6] - z[87]
    + n<T>(1,2)*z[92] - z[9];
    z[87]=z[10]*z[87];
    z[92]=z[77]*z[4];
    z[87]=z[87] + z[96] + z[103] + n<T>(3,4)*z[102] + z[92] + n<T>(1,6)*z[90] + 
    z[99];
    z[87]=z[10]*z[87];
    z[90]=n<T>(17,4)*z[5];
    z[96]=2*z[8];
    z[99]= - n<T>(31,12)*z[9] - z[83] - z[90] + n<T>(29,8)*z[77] + z[96];
    z[99]=z[9]*z[99];
    z[90]= - z[90] + n<T>(17,2)*z[77] - z[96];
    z[90]=z[5]*z[90];
    z[102]=z[89] + z[77];
    z[102]=5*z[102];
    z[103]=z[83] - z[102] + z[97];
    z[103]=z[2]*z[103];
    z[104]=2*z[77];
    z[105]=z[104] + z[89];
    z[105]=z[8]*z[105];
    z[90]=z[99] + z[103] + z[105] + z[90];
    z[99]=n<T>(1,3)*z[9];
    z[90]=z[90]*z[99];
    z[103]=n<T>(3,2)*z[5];
    z[105]=n<T>(1,2)*z[2];
    z[106]=z[81] + n<T>(5,24)*z[9] - z[105] + z[103] - n<T>(1,4)*z[77] - z[8];
    z[106]=z[4]*z[106];
    z[103]=z[103] - 3*z[77] + z[96];
    z[103]=z[5]*z[103];
    z[107]=5*z[5];
    z[108]=n<T>(5,2)*z[2];
    z[109]=z[108] + z[77] - z[107];
    z[109]=z[2]*z[109];
    z[93]=n<T>(5,12)*z[9] - n<T>(5,6)*z[77] + z[93];
    z[93]=z[93]*z[94];
    z[110]=z[104] - z[8];
    z[111]=z[110]*z[8];
    z[93]=z[106] + z[93] + z[109] + z[111] + z[103];
    z[93]=z[4]*z[93];
    z[83]= - z[83] + 4*z[5] + z[104] - z[89];
    z[83]=z[2]*z[83];
    z[103]=z[86]*z[8];
    z[102]= - z[102] + z[80];
    z[102]=z[5]*z[102];
    z[83]=z[83] - n<T>(19,4)*z[103] + z[102];
    z[102]=n<T>(3,4)*z[9];
    z[80]=n<T>(7,24)*z[4] - z[102] - z[105] - z[80] - n<T>(7,12)*z[77] + z[8];
    z[80]=z[4]*z[80];
    z[106]=7*z[5];
    z[109]=7*z[2];
    z[112]= - z[109] + z[106] + z[77] - n<T>(11,4)*z[8];
    z[112]=n<T>(17,9)*z[6] + n<T>(7,12)*z[4] + n<T>(1,3)*z[112] + z[102];
    z[100]=z[112]*z[100];
    z[94]=z[77] + z[94];
    z[94]=z[94]*z[102];
    z[80]=z[100] + z[80] + n<T>(1,3)*z[83] + z[94];
    z[80]=z[6]*z[80];
    z[83]=z[106] - z[98] + z[96];
    z[83]=z[5]*z[83];
    z[84]=n<T>(29,4)*z[9] + z[86] + z[84];
    z[84]=z[9]*z[84];
    z[86]=z[101] - z[5];
    z[94]=2*z[86] + z[2];
    z[94]=z[2]*z[94];
    z[83]=z[84] + z[94] + n<T>(7,2)*z[103] + z[83];
    z[84]=11*z[77] + z[85];
    z[85]=4*z[2];
    z[84]= - n<T>(5,4)*z[9] + z[85] + n<T>(1,2)*z[84] - z[5];
    z[84]= - n<T>(20,9)*z[3] - z[95] + n<T>(1,3)*z[84] - z[81];
    z[84]=z[3]*z[84];
    z[94]=z[4] - z[8] + z[9];
    z[94]=z[94]*z[95];
    z[83]=z[84] + z[94] + n<T>(1,3)*z[83] - z[88];
    z[83]=z[3]*z[83];
    z[78]= - n<T>(1,6)*z[9] - z[105] - z[78] + z[89];
    z[78]=z[9]*z[78];
    z[84]=z[5] - z[8];
    z[88]= - z[81] + n<T>(1,2)*z[84] + z[99];
    z[88]=z[4]*z[88];
    z[89]= - z[2]*z[84];
    z[89]= - z[103] + z[89];
    z[91]= - n<T>(1,3)*z[6] + n<T>(1,6)*z[4] - z[91] + z[105];
    z[91]=z[6]*z[91];
    z[94]=z[9] + z[101];
    z[94]=z[15]*z[94];
    z[78]=n<T>(1,4)*z[94] + z[91] + z[88] + n<T>(1,2)*z[89] + z[78];
    z[78]=z[15]*z[78];
    z[88]=z[92] + z[29];
    z[89]=z[15]*z[77];
    z[89]=z[89] + z[88];
    z[89]=z[12]*z[89];
    z[91]=z[101]*npow(z[8],2);
    z[92]=z[77] - z[4];
    z[94]= - z[15] + z[92];
    z[94]=z[28]*z[94];
    z[89]= - z[45] + z[91] + z[89] + z[94] - z[65];
    z[91]= - z[97] + z[110];
    z[91]=z[5]*z[91];
    z[91]=z[111] + z[91];
    z[91]=z[5]*z[91];
    z[94]=n<T>(59,2)*z[9] + z[108] - n<T>(29,4)*z[8] + z[107];
    z[81]=n<T>(17,3)*z[6] + n<T>(1,3)*z[94] + z[81];
    z[81]=n<T>(5,4)*z[15] + n<T>(55,12)*z[10] - n<T>(17,12)*z[3] + n<T>(1,2)*z[81]
     - n<T>(2,3)*z[7];
    z[81]=z[29]*z[81];
    z[86]=z[86] + z[2];
    z[94]= - 13*z[3] + 4*z[86] - 17*z[9];
    z[94]=z[26]*z[94];
    z[81]= - z[53] + z[91] + z[81] + z[94] + z[70];
    z[91]=7*z[77];
    z[94]=z[91] - z[96];
    z[95]= - z[94]*z[96];
    z[91]= - n<T>(7,2)*z[5] + z[91] - 4*z[8];
    z[91]=z[5]*z[91];
    z[96]= - 8*z[2] + n<T>(19,2)*z[5] + z[104] + 5*z[8];
    z[96]=z[2]*z[96];
    z[91]=z[96] + z[95] + z[91];
    z[79]=z[91]*z[79];
    z[85]= - z[3] - 3*z[4] - z[85] + z[98] + z[97];
    z[85]=z[18]*z[85];
    z[91]=z[77]*z[5];
    z[91]=z[91] + z[29];
    z[95]=z[6]*z[77];
    z[95]=z[95] + z[91];
    z[95]=z[14]*z[95];
    z[96]=z[77] - z[5];
    z[97]= - z[6] + z[96];
    z[97]=z[22]*z[97];
    z[95]=z[59] + z[95] + z[97];
    z[97]=z[77]*z[8];
    z[97]=z[97] + z[29];
    z[98]= - z[7]*z[77];
    z[98]=z[98] - z[97];
    z[98]=z[16]*z[98];
    z[99]= - z[7] + z[101];
    z[99]=z[25]*z[99];
    z[98]= - z[98] - z[99] + z[61] + z[46];
    z[99]= - z[3]*z[77];
    z[91]=z[99] - z[91];
    z[91]=z[11]*z[91];
    z[96]= - z[3] + z[96];
    z[96]=z[20]*z[96];
    z[91]=z[91] + z[96];
    z[94]=z[3] + z[7] - z[4] - n<T>(10,3)*z[2] + n<T>(2,3)*z[94] + z[5];
    z[94]=z[24]*z[94];
    z[96]= - z[104] + z[5];
    z[96]=5*z[96] - z[109];
    z[96]=n<T>(1,3)*z[96] + 4*z[4];
    z[96]=z[19]*z[96];
    z[99]= - z[10]*z[77];
    z[88]=z[99] - z[88];
    z[88]=z[13]*z[88];
    z[77]=z[9]*z[77];
    z[77]=z[77] + z[97];
    z[77]=z[17]*z[77];
    z[97]=z[7] + z[10];
    z[97]=z[41]*z[97];
    z[97]= - z[52] + z[97] - z[71];
    z[99]= - z[6] - z[15];
    z[99]=z[42]*z[99];
    z[99]=z[60] + z[99] - z[73];
    z[100]=z[55] + z[75] + z[68];
    z[102]=z[49] - z[35];
    z[103]=z[76] - z[69];
    z[104]= - z[9] - z[15];
    z[104]=z[43]*z[104];
    z[104]=z[104] - z[63];
    z[105]= - n<T>(16,3)*z[36] + n<T>(1,3)*z[34] - n<T>(1,2)*z[30];
    z[105]=i*z[105];
    z[86]=n<T>(7,3)*z[7] - n<T>(2,3)*z[86] + 3*z[6];
    z[86]=z[23]*z[86];
    z[84]= - n<T>(11,3)*z[10] - n<T>(8,3)*z[3] - z[7] - z[84];
    z[84]=z[40]*z[84];
    z[92]=z[10] - z[92];
    z[92]=z[21]*z[92];
    z[101]= - z[9] + z[101];
    z[101]=z[27]*z[101];

    r += n<T>(31,12)*z[31] + z[32] - n<T>(13,72)*z[33] + n<T>(31,6)*z[37] - n<T>(3,4)*
      z[38] + n<T>(151,24)*z[39] - 2*z[44] + n<T>(3,2)*z[47] - n<T>(25,12)*z[48]
     - 
      n<T>(5,4)*z[50] - n<T>(16,3)*z[51] + n<T>(11,12)*z[54] + 4*z[56] - z[57] - 
      z[58] - n<T>(9,8)*z[62] - n<T>(8,3)*z[64] - z[66] + n<T>(1,24)*z[67] + n<T>(7,24)
      *z[72] - n<T>(5,24)*z[74] + z[77] + z[78] + z[79] + z[80] + n<T>(1,3)*
      z[81] + z[82] + z[83] + z[84] + z[85] + z[86] + z[87] + z[88] + n<T>(1,2)*z[89]
     + z[90]
     + 3*z[91]
     + z[92]
     + z[93]
     + z[94]
     + n<T>(4,3)*z[95]
       + z[96] + n<T>(1,6)*z[97] - n<T>(7,6)*z[98] + n<T>(2,3)*z[99] - n<T>(1,4)*z[100]
       + z[101] - n<T>(13,12)*z[102] + n<T>(3,8)*z[103] + n<T>(5,6)*z[104] + z[105]
      ;
 
    return r;
}

template std::complex<double> qqb_2lha_tf774(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf774(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
