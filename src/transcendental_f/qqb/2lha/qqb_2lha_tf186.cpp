#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf186(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[95];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[18];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[16];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[5];
    z[21]=e[8];
    z[22]=e[9];
    z[23]=e[10];
    z[24]=e[11];
    z[25]=c[3];
    z[26]=c[11];
    z[27]=c[15];
    z[28]=c[18];
    z[29]=c[19];
    z[30]=c[23];
    z[31]=c[25];
    z[32]=c[26];
    z[33]=c[28];
    z[34]=c[29];
    z[35]=c[31];
    z[36]=e[3];
    z[37]=e[6];
    z[38]=e[7];
    z[39]=f[0];
    z[40]=f[1];
    z[41]=f[2];
    z[42]=f[3];
    z[43]=f[4];
    z[44]=f[5];
    z[45]=f[6];
    z[46]=f[7];
    z[47]=f[8];
    z[48]=f[10];
    z[49]=f[11];
    z[50]=f[12];
    z[51]=f[13];
    z[52]=f[14];
    z[53]=f[15];
    z[54]=f[16];
    z[55]=f[17];
    z[56]=f[18];
    z[57]=f[21];
    z[58]=f[23];
    z[59]=f[28];
    z[60]=f[29];
    z[61]=f[30];
    z[62]=f[31];
    z[63]=f[32];
    z[64]=f[35];
    z[65]=f[36];
    z[66]=f[37];
    z[67]=f[39];
    z[68]=f[41];
    z[69]=n<T>(7,2)*z[5];
    z[70]=n<T>(3,2)*z[8];
    z[71]=n<T>(1,2)*z[7];
    z[72]=z[1]*i;
    z[73]= - 2*z[3] - z[71] - z[2] + z[69] - 4*z[72] + z[70];
    z[73]=z[3]*z[73];
    z[74]=n<T>(3,8)*z[5];
    z[75]=z[2] - z[8];
    z[76]= - n<T>(1,2)*z[9] + n<T>(9,8)*z[11] - n<T>(3,4)*z[3] - z[74] + n<T>(3,8)*z[72]
    - z[75];
    z[76]=z[9]*z[76];
    z[77]=z[72] + z[70];
    z[78]=n<T>(1,4)*z[8];
    z[77]=z[77]*z[78];
    z[74]= - z[74] + n<T>(3,4)*z[72] - z[8];
    z[74]=z[5]*z[74];
    z[75]= - z[75]*z[71];
    z[79]=n<T>(1,2)*z[11];
    z[80]=n<T>(9,4)*z[11] + z[3] + z[7] - n<T>(11,2)*z[5] + n<T>(13,2)*z[72] - z[8];
    z[80]=z[80]*z[79];
    z[81]=3*z[72];
    z[82]= - z[81] - n<T>(5,2)*z[8];
    z[82]=n<T>(7,8)*z[2] + n<T>(1,2)*z[82] + z[5];
    z[82]=z[2]*z[82];
    z[73]=z[76] + z[80] + z[73] + z[75] + z[82] + z[77] + z[74];
    z[73]=z[9]*z[73];
    z[74]=n<T>(1,2)*z[8];
    z[75]=z[74] - z[72];
    z[76]=z[75]*z[8];
    z[77]=n<T>(95,6)*z[5] - n<T>(167,3)*z[72] - 113*z[8];
    z[77]=z[5]*z[77];
    z[77]=113*z[76] + z[77];
    z[80]=n<T>(1,3)*z[2];
    z[82]= - n<T>(61,2)*z[2] + n<T>(253,2)*z[72] + 61*z[5];
    z[82]=z[82]*z[80];
    z[83]=7*z[72];
    z[84]= - z[83] - z[7];
    z[84]=z[84]*z[71];
    z[85]=5*z[72];
    z[86]=n<T>(1,2)*z[3];
    z[87]= - z[86] + z[85] + z[7];
    z[87]=z[3]*z[87];
    z[88]=z[72] - z[79];
    z[88]=z[11]*z[88];
    z[89]=n<T>(1,2)*z[4];
    z[83]=7*z[7] - n<T>(253,3)*z[2] + n<T>(167,6)*z[5] - z[83] + n<T>(113,2)*z[8];
    z[83]=n<T>(13,6)*z[4] + n<T>(1,2)*z[83] - 5*z[3];
    z[83]=z[83]*z[89];
    z[77]=z[83] + n<T>(3,2)*z[88] + z[87] + z[84] + n<T>(1,4)*z[77] + z[82];
    z[77]=z[77]*z[89];
    z[82]=z[72] + z[74];
    z[82]=z[82]*z[74];
    z[83]=n<T>(1,4)*z[5];
    z[84]=14*z[72] + z[83];
    z[87]=n<T>(1,3)*z[5];
    z[84]=z[84]*z[87];
    z[88]=4*z[5];
    z[90]=n<T>(27,8)*z[2] - z[88] - n<T>(7,4)*z[72] - z[8];
    z[90]=z[2]*z[90];
    z[75]=z[7]*z[75];
    z[91]=n<T>(1,6)*z[72] - 3*z[8];
    z[91]=n<T>(11,4)*z[3] + n<T>(21,4)*z[2] + n<T>(1,2)*z[91] - n<T>(5,3)*z[5];
    z[91]=z[91]*z[86];
    z[75]=z[91] + z[75] + z[90] + z[82] + z[84];
    z[75]=z[3]*z[75];
    z[82]=n<T>(3,2)*z[72] + 2*z[8];
    z[83]=z[83] + z[82];
    z[83]=z[5]*z[83];
    z[69]=n<T>(7,2)*z[2] - z[69] - z[82];
    z[69]=z[2]*z[69];
    z[82]=35*z[72] - 33*z[8];
    z[82]=z[71] - n<T>(3,2)*z[2] + n<T>(1,4)*z[82] + z[5];
    z[82]=z[7]*z[82];
    z[84]=z[72] - z[8];
    z[79]= - z[79] - z[84];
    z[79]=z[11]*z[79];
    z[90]=31*z[6] - n<T>(99,2)*z[11] + n<T>(33,2)*z[84] + 35*z[7];
    z[90]=z[6]*z[90];
    z[69]=n<T>(1,8)*z[90] + n<T>(99,8)*z[79] + z[82] + z[69] - n<T>(33,8)*z[76] + 
    z[83];
    z[69]=z[6]*z[69];
    z[79]= - 55*z[72] + n<T>(51,2)*z[8];
    z[78]=z[79]*z[78];
    z[79]=n<T>(59,8)*z[8] + z[87];
    z[79]=z[5]*z[79];
    z[78]=z[78] + z[79];
    z[78]=z[5]*z[78];
    z[79]=z[72]*z[5];
    z[79]=z[79] + z[25];
    z[82]= - z[6]*z[72];
    z[82]=z[82] - z[79];
    z[82]=z[14]*z[82];
    z[83]= - z[72] - n<T>(13,3)*z[8];
    z[83]=z[83]*npow(z[8],2);
    z[90]=z[72] - z[5];
    z[91]=z[6] - z[90];
    z[91]=z[20]*z[91];
    z[78]=z[82] + n<T>(1,4)*z[83] + z[78] + z[91] + z[63];
    z[82]= - z[72] + n<T>(1,2)*z[5];
    z[83]= - z[70] - z[82];
    z[83]=z[5]*z[83];
    z[91]=n<T>(1,2)*z[2];
    z[92]=z[72] + 61*z[8];
    z[92]= - 17*z[2] + n<T>(1,2)*z[92] + 3*z[5];
    z[92]=z[92]*z[91];
    z[93]=n<T>(89,4)*z[72] + 11*z[8];
    z[93]= - n<T>(11,3)*z[7] - n<T>(9,4)*z[2] + n<T>(1,3)*z[93] - z[5];
    z[93]=z[93]*z[71];
    z[94]= - n<T>(11,2)*z[72] + z[8];
    z[94]=z[8]*z[94];
    z[83]=z[93] + z[92] + n<T>(19,6)*z[94] + z[83];
    z[83]=z[7]*z[83];
    z[71]= - n<T>(4,3)*z[3] - z[71] + n<T>(13,6)*z[5] - n<T>(8,3)*z[72] + z[74];
    z[71]=z[3]*z[71];
    z[74]=z[5]*z[82];
    z[74]= - n<T>(11,2)*z[76] + z[74];
    z[76]=n<T>(5,3)*z[84] + n<T>(1,4)*z[7];
    z[76]=z[7]*z[76];
    z[74]=n<T>(7,6)*z[74] + 11*z[76];
    z[76]=z[81] - n<T>(11,3)*z[8];
    z[76]=n<T>(1,2)*z[76] + z[87];
    z[76]=7*z[76] + 3*z[2];
    z[76]=n<T>(83,6)*z[11] - n<T>(17,3)*z[3] + n<T>(1,2)*z[76] + n<T>(55,3)*z[7];
    z[76]=z[11]*z[76];
    z[71]=n<T>(1,4)*z[76] + n<T>(1,2)*z[74] + z[71];
    z[71]=z[11]*z[71];
    z[74]=z[4] + z[11];
    z[76]= - z[72]*z[74];
    z[76]= - z[25] + z[76];
    z[76]=z[13]*z[76];
    z[74]= - z[72] + z[74];
    z[74]=z[19]*z[74];
    z[74]=z[76] + z[74] + z[58] + z[53] + z[52] + z[39];
    z[70]=z[85] - z[70];
    z[70]=z[8]*z[70];
    z[76]= - n<T>(17,3)*z[5] + n<T>(25,3)*z[72] + n<T>(29,4)*z[8];
    z[76]=z[5]*z[76];
    z[81]= - n<T>(173,6)*z[5] - n<T>(337,3)*z[72] - n<T>(77,2)*z[8];
    z[81]=n<T>(1,4)*z[81] + n<T>(50,3)*z[2];
    z[81]=z[2]*z[81];
    z[70]=z[81] + n<T>(13,2)*z[70] + z[76];
    z[70]=z[2]*z[70];
    z[76]= - 13*z[72] + n<T>(7,2)*z[8];
    z[76]=n<T>(7,2)*z[6] + n<T>(3,4)*z[9] + z[86] - n<T>(31,4)*z[7] + n<T>(35,4)*z[2]
     + 
   n<T>(1,2)*z[76] - z[88];
    z[76]=z[21]*z[76];
    z[81]=z[89] - z[72] + z[91];
    z[81]=z[16]*z[81];
    z[85]= - z[8] - z[7];
    z[85]=z[72]*z[85];
    z[85]= - z[25] + z[85];
    z[85]=z[15]*z[85];
    z[86]= - z[7] + z[84];
    z[86]=z[22]*z[86];
    z[85]=z[85] + z[86];
    z[86]= - z[3]*z[72];
    z[79]=z[86] - z[79];
    z[79]=z[12]*z[79];
    z[86]= - z[3] + z[90];
    z[86]=z[18]*z[86];
    z[79]=z[79] + z[86];
    z[86]=n<T>(271,12)*z[8] - 13*z[5];
    z[80]=n<T>(1,4)*z[86] - z[80];
    z[80]= - n<T>(275,144)*z[4] + n<T>(25,12)*z[6] + n<T>(23,24)*z[9] + n<T>(73,48)*
    z[11] + n<T>(35,24)*z[3] + n<T>(1,3)*z[80] - n<T>(11,4)*z[7];
    z[80]=z[25]*z[80];
    z[86]= - z[2] + z[7] + n<T>(5,4)*z[9];
    z[87]=n<T>(5,4)*z[8] + z[86];
    z[87]=z[72]*z[87];
    z[87]=n<T>(1,4)*z[25] + z[87];
    z[87]=z[10]*z[87];
    z[72]=z[72] - 5*z[8];
    z[72]=n<T>(1,4)*z[72] - z[86];
    z[72]=z[24]*z[72];
    z[82]=z[91] + z[82];
    z[82]=n<T>(41,6)*z[82] + 2*z[6];
    z[82]=z[17]*z[82];
    z[86]=z[56] + z[60] - z[59];
    z[88]=z[48] + z[34];
    z[89]=z[66] - z[54];
    z[90]=n<T>(205,6)*z[32] + n<T>(331,12)*z[30] - n<T>(19,24)*z[26];
    z[90]=i*z[90];
    z[84]=z[2] - z[5] + z[84];
    z[84]= - n<T>(5,2)*z[9] + n<T>(3,2)*z[84] - z[3];
    z[84]=z[23]*z[84];
    z[91]=z[3] + z[11];
    z[91]=z[36]*z[91];
    z[92]=z[7] + z[11];
    z[92]=z[37]*z[92];
    z[93]=z[7] + z[6];
    z[93]=z[38]*z[93];

    r += n<T>(31,6)*z[27] + n<T>(8,3)*z[28] + n<T>(191,72)*z[29] - n<T>(197,12)*z[31]
       - n<T>(271,12)*z[33] + n<T>(19,18)*z[35] + n<T>(1,8)*z[40] + n<T>(35,12)*z[41]
       + n<T>(191,48)*z[42] - n<T>(9,8)*z[43] + n<T>(9,2)*z[44] - n<T>(5,4)*z[45]
     - 6*
      z[46] - 22*z[47] + n<T>(47,16)*z[49] - n<T>(7,24)*z[50] + n<T>(137,12)*z[51]
       + n<T>(97,16)*z[55] - n<T>(5,2)*z[57] - n<T>(26,3)*z[61] - n<T>(33,16)*z[62]
     +  n<T>(39,4)*z[64] + n<T>(77,48)*z[65] - n<T>(99,16)*z[67] + n<T>(9,4)*z[68]
     +  z[69] + z[70] + z[71] + z[72] + z[73] + n<T>(3,4)*z[74] + z[75] + 
      z[76] + z[77] + n<T>(1,2)*z[78] + n<T>(5,6)*z[79] + z[80] + n<T>(131,6)*z[81]
       + z[82] + z[83] + z[84] + n<T>(11,3)*z[85] + n<T>(1,4)*z[86] + z[87] - 2
      *z[88] - n<T>(11,8)*z[89] + z[90] + n<T>(1,6)*z[91] + n<T>(77,12)*z[92] + n<T>(31,4)*z[93];
 
    return r;
}

template std::complex<double> qqb_2lha_tf186(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf186(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
