#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf686(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[6];
    z[3]=d[17];
    z[4]=d[0];
    z[5]=d[5];
    z[6]=d[3];
    z[7]=e[9];
    z[8]=e[13];
    z[9]=z[4] - z[6];
    z[10]=z[1]*i;
    z[11]=n<T>(1,2)*z[2] - z[5] - z[10] - z[9];
    z[11]=z[2]*z[11];
    z[10]=z[3]*z[10];
    z[9]=n<T>(1,2)*z[5] + z[9];
    z[9]=z[5]*z[9];

    r += z[7] + z[8] + z[9] + z[10] + z[11];
 
    return r;
}

template std::complex<double> qqb_2lha_tf686(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf686(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
