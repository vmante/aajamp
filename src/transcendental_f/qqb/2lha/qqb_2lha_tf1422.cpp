#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1422(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[112];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[5];
    z[10]=d[6];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[3];
    z[22]=e[4];
    z[23]=e[5];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[10];
    z[28]=e[11];
    z[29]=e[12];
    z[30]=e[13];
    z[31]=c[3];
    z[32]=c[11];
    z[33]=c[15];
    z[34]=c[19];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[31];
    z[40]=e[6];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[7];
    z[50]=f[10];
    z[51]=f[11];
    z[52]=f[12];
    z[53]=f[14];
    z[54]=f[15];
    z[55]=f[16];
    z[56]=f[17];
    z[57]=f[18];
    z[58]=f[23];
    z[59]=f[30];
    z[60]=f[31];
    z[61]=f[32];
    z[62]=f[33];
    z[63]=f[35];
    z[64]=f[36];
    z[65]=f[37];
    z[66]=f[39];
    z[67]=f[50];
    z[68]=f[51];
    z[69]=f[52];
    z[70]=f[53];
    z[71]=f[54];
    z[72]=f[55];
    z[73]=f[56];
    z[74]=f[58];
    z[75]=f[74];
    z[76]=f[75];
    z[77]=n<T>(3,8)*z[6];
    z[78]=n<T>(1,8)*z[7];
    z[79]=n<T>(1,2)*z[3];
    z[80]=z[1]*i;
    z[81]=n<T>(1,8)*z[80] + z[5];
    z[81]=z[10] - n<T>(17,6)*z[9] - z[78] - z[77] + n<T>(1,3)*z[81] + z[79];
    z[81]=n<T>(1,2)*z[81] + z[11];
    z[81]=z[11]*z[81];
    z[82]=n<T>(1,8)*z[3];
    z[78]=z[82] - z[78];
    z[83]=n<T>(1,4)*z[9];
    z[84]=n<T>(1,3)*z[5];
    z[85]= - z[83] - z[84] - z[78];
    z[85]=z[9]*z[85];
    z[86]=n<T>(1,2)*z[5];
    z[87]=z[86] - z[80];
    z[88]=z[87]*z[84];
    z[89]=z[79] + z[80];
    z[90]=n<T>(1,4)*z[5];
    z[91]=z[90] - z[89];
    z[91]=z[91]*z[79];
    z[92]=n<T>(1,2)*z[6];
    z[93]=z[92] - z[80];
    z[94]=z[3] - z[93];
    z[77]=z[94]*z[77];
    z[82]=n<T>(17,24)*z[7] + z[82] + n<T>(5,3)*z[80] - n<T>(1,8)*z[5];
    z[82]=z[7]*z[82];
    z[94]=n<T>(1,2)*z[10];
    z[95]= - z[9] + z[94];
    z[95]=z[10]*z[95];
    z[96]=n<T>(29,16)*z[4] - n<T>(37,8)*z[7] + z[9];
    z[96]=z[4]*z[96];
    z[77]=z[81] + n<T>(1,3)*z[96] + z[95] + z[85] + z[82] + z[77] + z[88] + 
    z[91];
    z[77]=z[11]*z[77];
    z[81]=n<T>(1,2)*z[2];
    z[82]=z[81] - z[80];
    z[85]=n<T>(1,4)*z[6];
    z[78]= - z[85] - z[90] + z[82] - z[78];
    z[78]=z[2]*z[78];
    z[88]=n<T>(1,4)*z[3];
    z[90]=z[88] + z[86] - z[93];
    z[90]=z[6]*z[90];
    z[91]=z[87]*z[5];
    z[95]=z[80] - z[5];
    z[96]= - n<T>(1,2)*z[95] - z[3];
    z[96]=z[96]*z[79];
    z[97]=z[3] - z[5];
    z[98]=z[80] - z[6];
    z[99]=z[7] + z[97] + z[98];
    z[100]=n<T>(1,4)*z[7];
    z[99]=z[99]*z[100];
    z[101]=n<T>(1,2)*z[4];
    z[102]=z[80] - z[101];
    z[102]=z[4]*z[102];
    z[103]=npow(z[8],2);
    z[78]=z[78] + n<T>(3,2)*z[102] + n<T>(3,4)*z[103] + z[99] + z[90] - z[91] + 
    z[96];
    z[78]=z[2]*z[78];
    z[88]= - z[88] + n<T>(5,2)*z[80] - z[5];
    z[88]=z[3]*z[88];
    z[90]=3*z[6];
    z[96]=3*z[80];
    z[99]=z[90] - n<T>(5,2)*z[3] - z[96] + z[5];
    z[99]=z[99]*z[92];
    z[88]=z[99] + z[91] + z[88];
    z[88]=z[6]*z[88];
    z[99]=z[93]*z[6];
    z[102]=z[95] + z[79];
    z[103]=z[3]*z[102];
    z[103]= - 13*z[99] + n<T>(193,2)*z[91] + 11*z[103];
    z[104]=n<T>(1,2)*z[7];
    z[105]=z[104] + z[95];
    z[105]=z[7]*z[105];
    z[103]=n<T>(1,3)*z[103] + n<T>(57,2)*z[105];
    z[105]= - n<T>(9,4)*z[9] + n<T>(11,2)*z[6] - n<T>(9,2)*z[80] + z[97];
    z[105]=z[9]*z[105];
    z[106]=n<T>(1,2)*z[8];
    z[107]= - 167*z[80] + 193*z[5];
    z[107]= - n<T>(13,2)*z[6] + n<T>(1,4)*z[107] - 91*z[3];
    z[107]=5*z[8] - n<T>(5,2)*z[9] + n<T>(1,3)*z[107] + n<T>(57,4)*z[7];
    z[107]=z[107]*z[106];
    z[103]=z[107] + n<T>(1,2)*z[103] + z[105];
    z[103]=z[8]*z[103];
    z[105]=z[5]*z[95];
    z[107]=5*z[80];
    z[108]= - z[107] - n<T>(53,4)*z[5];
    z[108]=n<T>(1,3)*z[108] + n<T>(33,2)*z[3];
    z[108]=z[3]*z[108];
    z[105]=n<T>(59,6)*z[105] + z[108];
    z[105]=z[3]*z[105];
    z[108]=z[80]*z[5];
    z[109]=z[108] + z[31];
    z[110]=z[9]*z[80];
    z[110]=z[110] + z[109];
    z[110]=z[15]*z[110];
    z[96]=z[96] - n<T>(5,3)*z[5];
    z[96]=z[96]*npow(z[5],2);
    z[111]= - z[9] + z[95];
    z[111]=z[23]*z[111];
    z[88]=z[110] + z[103] + z[88] + n<T>(1,2)*z[96] + z[105] + z[111] - 
    z[58];
    z[96]= - z[102]*z[79];
    z[102]= - n<T>(43,4)*z[7] - n<T>(31,2)*z[80] - z[97];
    z[102]=z[102]*z[104];
    z[103]=n<T>(29,3)*z[9] - n<T>(31,8)*z[7] - n<T>(31,12)*z[6] + n<T>(47,8)*z[80]
     - 
    z[97];
    z[103]=z[9]*z[103];
    z[96]=z[103] + z[102] - n<T>(31,6)*z[99] - z[108] + z[96];
    z[96]=z[96]*z[83];
    z[79]=z[79] - z[80];
    z[99]= - z[3]*z[79];
    z[102]= - z[5] + z[93];
    z[102]=z[6]*z[102];
    z[99]=z[102] + z[91] + z[99];
    z[102]=z[80] - z[104];
    z[103]=n<T>(13,12)*z[7];
    z[102]=z[102]*z[103];
    z[104]= - n<T>(103,6)*z[9] + n<T>(103,3)*z[80] + 29*z[7];
    z[83]=z[104]*z[83];
    z[83]=z[83] + 3*z[99] + z[102];
    z[99]=z[97] + z[10];
    z[102]=z[80] - z[4];
    z[90]= - n<T>(103,12)*z[9] - z[103] + z[90] - n<T>(65,6)*z[102] - 3*z[99];
    z[90]=z[4]*z[90];
    z[99]=z[80] - z[106];
    z[99]=z[8]*z[99];
    z[103]= - z[94] + z[80] + z[3];
    z[104]=n<T>(3,2)*z[10];
    z[103]=z[103]*z[104];
    z[83]=n<T>(1,4)*z[90] + z[103] + n<T>(1,2)*z[83] + 3*z[99];
    z[83]=z[83]*z[101];
    z[79]=z[85] + z[86] - z[79];
    z[79]=z[79]*z[92];
    z[85]= - 2*z[95] - z[3];
    z[85]=z[3]*z[85];
    z[85]=n<T>(43,16)*z[91] + 4*z[85];
    z[90]= - n<T>(107,3)*z[3] - z[107] + n<T>(43,6)*z[5];
    z[90]=n<T>(15,4)*z[7] + n<T>(1,4)*z[90] - z[6];
    z[90]=z[90]*z[100];
    z[79]=z[90] + n<T>(1,3)*z[85] + z[79];
    z[79]=z[7]*z[79];
    z[85]= - z[86] - z[89];
    z[85]=z[3]*z[85];
    z[86]=z[6]*z[98];
    z[85]= - n<T>(11,6)*z[86] - n<T>(1,6)*z[91] + z[85];
    z[84]=z[84] + z[3];
    z[86]=n<T>(11,3)*z[6];
    z[89]=n<T>(1,3)*z[9] + z[86] + 7*z[80] - z[84];
    z[89]= - z[104] + n<T>(1,4)*z[89] + z[8];
    z[89]=z[89]*z[94];
    z[90]= - z[9] + n<T>(7,3)*z[80] - z[97];
    z[90]=n<T>(13,12)*z[8] - n<T>(5,3)*z[6] + n<T>(1,2)*z[90];
    z[90]=z[90]*z[106];
    z[84]= - n<T>(1,3)*z[6] + z[84];
    z[84]=n<T>(1,4)*z[84] + z[9];
    z[84]=z[9]*z[84];
    z[84]=z[89] + z[90] + n<T>(1,2)*z[85] + z[84];
    z[84]=z[10]*z[84];
    z[85]=z[97] + z[80];
    z[89]=n<T>(11,6)*z[10] - z[8] + n<T>(5,6)*z[9] + z[85];
    z[89]=z[24]*z[89];
    z[85]=z[7] - z[85];
    z[85]=3*z[85] + n<T>(55,3)*z[9];
    z[85]=n<T>(1,2)*z[85] + n<T>(23,3)*z[11];
    z[85]=z[30]*z[85];
    z[85]= - z[36] + z[89] + z[85] + z[57] + z[47];
    z[89]=z[80]*z[4];
    z[89]=z[89] + z[31];
    z[90]= - z[8]*z[80];
    z[90]=z[90] - z[89];
    z[90]=z[14]*z[90];
    z[91]=z[8] - z[102];
    z[91]=z[22]*z[91];
    z[90]=z[42] + z[90] + z[91];
    z[91]=z[80]*z[6];
    z[91]=z[91] + z[31];
    z[92]= - z[7]*z[80];
    z[92]=z[92] - z[91];
    z[92]=z[17]*z[92];
    z[94]=z[7] - z[98];
    z[94]=z[28]*z[94];
    z[92]= - z[43] + z[92] + z[94];
    z[82]=z[101] + z[82];
    z[82]=z[18]*z[82];
    z[82]=z[82] - z[70];
    z[94]= - z[10]*z[80];
    z[91]=z[94] - z[91];
    z[91]=z[16]*z[91];
    z[94]= - z[10] + z[98];
    z[94]=z[26]*z[94];
    z[91]=z[91] + z[94];
    z[94]= - z[11]*z[80];
    z[89]=z[94] - z[89];
    z[89]=z[13]*z[89];
    z[94]=z[11] - z[102];
    z[94]=z[29]*z[94];
    z[89]=z[89] + z[94];
    z[80]=z[3]*z[80];
    z[80]=z[80] + z[109];
    z[80]=z[12]*z[80];
    z[94]=z[3] - z[95];
    z[94]=z[20]*z[94];
    z[80]=z[80] + z[94];
    z[94]=n<T>(149,6)*z[5] + 47*z[3];
    z[86]= - n<T>(19,3)*z[11] - n<T>(77,8)*z[4] + n<T>(61,24)*z[10] - n<T>(379,12)*z[8]
    + n<T>(61,2)*z[9] - n<T>(7,2)*z[7] + n<T>(1,4)*z[94] - z[86];
    z[86]=n<T>(1,4)*z[86] + 2*z[2];
    z[86]=z[31]*z[86];
    z[86]=z[86] - z[71];
    z[94]=n<T>(43,12)*z[7] + z[6] + n<T>(55,12)*z[3] + z[95];
    z[94]= - z[81] - z[11] + n<T>(1,2)*z[94] + z[9];
    z[94]=z[27]*z[94];
    z[87]=z[81] + z[87];
    z[87]=z[19]*z[87];
    z[81]=z[81] + z[93];
    z[81]=z[25]*z[81];
    z[93]=z[51] + z[76] + z[75];
    z[97]=z[8] + z[10];
    z[97]=z[40]*z[97];
    z[97]= - z[97] - z[63] + z[61] + z[44];
    z[98]=z[65] - z[56] + z[53] - z[45];
    z[99]=z[62] - z[49];
    z[100]=z[73] + z[66];
    z[101]=z[7] + z[11];
    z[101]=z[41]*z[101];
    z[101]=z[101] - z[67];
    z[102]=5*z[37] + z[35] + n<T>(19,48)*z[32];
    z[102]=i*z[102];
    z[95]=z[10] + n<T>(187,24)*z[8] - z[9] + n<T>(163,24)*z[3] - z[95];
    z[95]=z[21]*z[95];

    r += n<T>(5,3)*z[33] - n<T>(2,3)*z[34] - n<T>(5,2)*z[38] - n<T>(19,3)*z[39] + n<T>(67,96)*z[46]
     + n<T>(9,16)*z[48] - n<T>(43,24)*z[50] - n<T>(169,96)*z[52] - n<T>(47,8)
      *z[54] + n<T>(65,32)*z[55] - n<T>(5,6)*z[59] - n<T>(31,48)*z[60] + n<T>(13,48)*
      z[64] - n<T>(103,96)*z[68] - n<T>(1,6)*z[69] + n<T>(13,96)*z[72] - n<T>(51,32)*
      z[74] + z[77] + z[78] + z[79] + n<T>(59,24)*z[80] + z[81] + 3*z[82]
       + z[83] + z[84] + n<T>(1,2)*z[85] + n<T>(1,3)*z[86] + z[87] + n<T>(1,4)*
      z[88] + n<T>(29,24)*z[89] + n<T>(3,2)*z[90] + n<T>(11,12)*z[91] + n<T>(3,4)*z[92]
       + n<T>(1,8)*z[93] + z[94] + z[95] + z[96] - n<T>(1,12)*z[97] - n<T>(3,8)*
      z[98] + 2*z[99] - n<T>(3,16)*z[100] + n<T>(37,24)*z[101] + z[102];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1422(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1422(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
