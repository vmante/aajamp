#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1103(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[82];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[12];
    z[13]=d[17];
    z[14]=d[18];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[11];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=c[31];
    z[33]=e[3];
    z[34]=e[6];
    z[35]=f[2];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[5];
    z[39]=f[6];
    z[40]=f[7];
    z[41]=f[9];
    z[42]=f[11];
    z[43]=f[12];
    z[44]=f[13];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[23];
    z[48]=f[30];
    z[49]=f[31];
    z[50]=f[32];
    z[51]=f[33];
    z[52]=f[34];
    z[53]=f[35];
    z[54]=f[36];
    z[55]=f[39];
    z[56]=f[43];
    z[57]=z[8] + z[5];
    z[58]=z[1]*i;
    z[59]=z[57] - z[58];
    z[60]=z[59] - z[2];
    z[61]=5*z[7];
    z[62]= - 7*z[6] - 2*z[60] - z[61];
    z[62]=z[19]*z[62];
    z[63]= - z[5] - z[6];
    z[63]=z[58]*z[63];
    z[63]= - z[24] + z[63];
    z[63]=z[12]*z[63];
    z[64]=z[58]*z[8];
    z[64]=z[64] + z[24];
    z[65]=z[9]*z[58];
    z[65]=z[65] + z[64];
    z[65]=z[14]*z[65];
    z[66]=2*z[58];
    z[67]=z[66] - z[5];
    z[68]= - z[2] + z[67];
    z[68]=z[16]*z[68];
    z[69]=z[58] - z[5];
    z[70]=z[6] - z[69];
    z[70]=z[18]*z[70];
    z[71]=z[58] - z[8];
    z[72]= - z[9] + z[71];
    z[72]=z[23]*z[72];
    z[73]= - z[3] - z[10];
    z[73]=z[33]*z[73];
    z[74]= - z[7] - z[10];
    z[74]=z[34]*z[74];
    z[62]= - z[65] - z[68] - z[70] - z[72] + z[35] - z[62] - z[63] - 
    z[48] - z[47] + z[42] + z[38] - z[73] - z[74] + z[56] - z[53];
    z[63]=4*z[3];
    z[65]=3*z[9];
    z[68]=2*z[7];
    z[70]= - 2*z[10] - z[65] - z[68] + z[63] - z[5] + z[8];
    z[70]=z[10]*z[70];
    z[72]=2*z[69];
    z[73]= - z[72] - z[9];
    z[65]=z[73]*z[65];
    z[73]=z[67]*z[5];
    z[74]= - z[66] + z[8];
    z[74]=z[8]*z[74];
    z[75]=z[72] + z[3];
    z[75]=z[3]*z[75];
    z[76]=z[7]*z[71];
    z[65]=z[70] + z[65] - 4*z[76] + 2*z[75] + z[73] + z[74];
    z[65]=z[10]*z[65];
    z[70]=2*z[2];
    z[74]=2*z[5];
    z[75]=z[70] - z[74] - z[71];
    z[76]=3*z[10];
    z[75]= - n<T>(34,3)*z[6] + z[76] + 3*z[75] + 8*z[7];
    z[75]=z[6]*z[75];
    z[77]= - 7*z[58] + z[74];
    z[77]=2*z[77] + 7*z[8];
    z[77]=z[8]*z[77];
    z[78]=4*z[2];
    z[79]=z[71]*z[78];
    z[80]=z[5] - z[2];
    z[61]=2*z[80] + z[61];
    z[61]=z[61]*z[68];
    z[80]=2*z[71] + z[10];
    z[76]=z[80]*z[76];
    z[61]=z[75] + z[76] + z[61] + z[77] + z[79];
    z[61]=z[6]*z[61];
    z[75]=3*z[8];
    z[76]=z[58] + z[5];
    z[76]=2*z[76] - z[8];
    z[76]=z[76]*z[75];
    z[77]= - z[66] - z[5];
    z[77]=2*z[77] + z[2];
    z[77]=z[77]*z[70];
    z[75]=z[78] - z[5] - z[75];
    z[75]=z[4]*z[75];
    z[75]=z[75] + z[77] + z[73] + z[76];
    z[75]=z[4]*z[75];
    z[76]=z[67]*z[74];
    z[77]= - z[74] + z[71];
    z[77]=z[8]*z[77];
    z[76]=z[76] + z[77];
    z[76]=z[8]*z[76];
    z[77]=3*z[5];
    z[79]= - 4*z[58] + z[77];
    z[79]=z[5]*z[79];
    z[80]=6*z[58];
    z[81]= - 3*z[2] + z[80] + z[57];
    z[81]=z[81]*z[70];
    z[80]= - z[80] - z[5];
    z[80]=2*z[80] + 5*z[8];
    z[80]=z[8]*z[80];
    z[79]=z[81] + z[79] + z[80];
    z[79]=z[2]*z[79];
    z[80]= - z[69]*npow(z[5],2);
    z[75]= - z[75] - z[79] - z[80] - z[76] + z[52] - z[41] + z[36] + 
    z[27];
    z[59]= - z[2] + 2*z[59];
    z[59]=z[59]*z[2];
    z[76]= - z[58] + z[74];
    z[74]=z[76]*z[74];
    z[72]= - z[72] + z[8];
    z[72]=z[8]*z[72];
    z[72]= - z[59] + z[74] + z[72];
    z[74]=z[58] + z[2];
    z[76]=2*z[8];
    z[74]= - n<T>(19,3)*z[3] - z[76] - z[77] + 8*z[74];
    z[74]=z[3]*z[74];
    z[72]=2*z[72] + z[74];
    z[72]=z[3]*z[72];
    z[57]= - n<T>(7,3)*z[7] - z[70] - z[66] - z[57];
    z[57]=z[7]*z[57];
    z[74]= - z[76] + z[69];
    z[74]=z[74]*z[76];
    z[57]=z[57] + z[59] + z[73] + z[74];
    z[57]=z[57]*z[68];
    z[59]=4*z[8];
    z[63]= - z[63] - z[78] + z[59] - 9*z[58] + 8*z[5];
    z[63]=z[3]*z[63];
    z[59]=7*z[67] - z[59];
    z[59]=z[5]*z[59];
    z[67]= - z[69]*z[78];
    z[68]=3*z[69] - z[2] + z[8];
    z[68]= - n<T>(8,3)*z[9] + 2*z[68] + 9*z[3];
    z[68]=z[9]*z[68];
    z[59]=z[68] + z[63] + z[67] + z[59];
    z[59]=z[9]*z[59];
    z[63]=z[7]*z[58];
    z[63]=z[63] + z[64];
    z[63]=z[13]*z[63];
    z[64]=z[66] - z[2];
    z[66]= - z[8] + z[64];
    z[66]=z[20]*z[66];
    z[67]=z[7] - z[71];
    z[67]=z[21]*z[67];
    z[63]=z[63] + z[66] + z[67] + z[29];
    z[66]=z[7] - z[3];
    z[67]=z[6] - z[9];
    z[66]=n<T>(2,3)*z[4] + z[70] - z[5] + n<T>(7,3)*z[8] - n<T>(32,3)*z[67] + n<T>(4,3)*
    z[66];
    z[66]=z[24]*z[66];
    z[60]= - 27*z[9] - 8*z[60] - 19*z[3];
    z[60]=z[22]*z[60];
    z[67]= - z[9] + 11*z[3];
    z[68]= - 12*z[5] - z[67];
    z[58]=z[58]*z[68];
    z[58]= - 12*z[24] + z[58];
    z[58]=z[11]*z[58];
    z[67]=12*z[69] - z[67];
    z[67]=z[17]*z[67];
    z[68]= - z[39] + z[50] - z[46];
    z[69]=z[51] - z[40];
    z[70]=z[55] - z[45];
    z[64]= - z[4] + z[64];
    z[64]=z[15]*z[64];
    z[64]=z[64] - z[44];
    z[71]= - 48*z[30] - 24*z[28] + n<T>(1,6)*z[25];
    z[71]=i*z[71];

    r +=  - z[26] + 24*z[31] + n<T>(3,2)*z[32] - 10*z[37] + z[43] + 11*
      z[49] - z[54] + z[57] + z[58] + z[59] + z[60] + z[61] - 4*z[62]
       + 12*z[63] + 8*z[64] + z[65] + z[66] + z[67] + 6*z[68] + 32*
      z[69] + 3*z[70] + z[71] + z[72] - 2*z[75];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1103(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1103(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
