#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1277(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[178];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[1];
    z[6]=d[2];
    z[7]=d[3];
    z[8]=d[7];
    z[9]=d[9];
    z[10]=d[8];
    z[11]=e[7];
    z[12]=d[4];
    z[13]=d[15];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=e[12];
    z[17]=d[12];
    z[18]=d[17];
    z[19]=e[5];
    z[20]=e[9];
    z[21]=e[13];
    z[22]=e[6];
    z[23]=d[18];
    z[24]=e[14];
    z[25]=e[0];
    z[26]=e[1];
    z[27]=e[2];
    z[28]=e[4];
    z[29]=e[8];
    z[30]=e[10];
    z[31]=e[11];
    z[32]=f[30];
    z[33]=f[31];
    z[34]=f[32];
    z[35]=f[33];
    z[36]=f[35];
    z[37]=f[36];
    z[38]=f[39];
    z[39]=f[43];
    z[40]=f[50];
    z[41]=f[51];
    z[42]=f[52];
    z[43]=f[55];
    z[44]=f[58];
    z[45]=f[68];
    z[46]=c[3];
    z[47]=c[11];
    z[48]=c[15];
    z[49]=c[17];
    z[50]=c[19];
    z[51]=c[23];
    z[52]=c[25];
    z[53]=c[26];
    z[54]=c[28];
    z[55]=c[31];
    z[56]=c[32];
    z[57]=c[39];
    z[58]=c[43];
    z[59]=c[45];
    z[60]=c[46];
    z[61]=c[78];
    z[62]=c[81];
    z[63]=e[3];
    z[64]=f[53];
    z[65]=f[0];
    z[66]=f[1];
    z[67]=f[2];
    z[68]=f[3];
    z[69]=f[4];
    z[70]=f[5];
    z[71]=f[6];
    z[72]=f[7];
    z[73]=f[10];
    z[74]=f[11];
    z[75]=f[12];
    z[76]=f[14];
    z[77]=f[16];
    z[78]=f[17];
    z[79]=f[18];
    z[80]=f[23];
    z[81]=f[37];
    z[82]=f[54];
    z[83]=f[56];
    z[84]=f[72];
    z[85]=f[73];
    z[86]=g[66];
    z[87]=g[67];
    z[88]=g[84];
    z[89]=g[116];
    z[90]=g[117];
    z[91]=g[133];
    z[92]=g[159];
    z[93]=g[160];
    z[94]=g[162];
    z[95]=g[163];
    z[96]=g[165];
    z[97]=g[167];
    z[98]=g[174];
    z[99]=g[175];
    z[100]=g[178];
    z[101]=g[184];
    z[102]=g[185];
    z[103]=g[186];
    z[104]=g[195];
    z[105]=g[209];
    z[106]=g[221];
    z[107]= - 31 + n<T>(523,17)*z[3];
    z[108]=n<T>(3397,51)*z[4];
    z[109]=z[1]*i;
    z[110]= - z[108] - n<T>(608,51)*z[109] + z[107];
    z[111]=n<T>(1,9)*z[4];
    z[110]=z[110]*z[111];
    z[112]=n<T>(1,3)*z[109];
    z[113]=z[112] + z[3];
    z[114]=n<T>(1,3)*z[4];
    z[115]= - z[114] + n<T>(1,2)*z[113];
    z[116]=n<T>(4847,51)*z[9];
    z[117]=z[116] + n<T>(4613,306)*z[7];
    z[115]= - z[115]*z[117];
    z[118]=z[109] - z[7];
    z[119]=n<T>(2351,136)*z[3];
    z[120]= - n<T>(4847,68)*z[9] + n<T>(3397,204)*z[4] + z[119] + n<T>(4613,408)*
    z[118];
    z[121]=z[120] + 7;
    z[122]=n<T>(1,9)*z[8];
    z[123]=z[121]*z[122];
    z[124]=n<T>(1,9)*z[1];
    z[125]=z[3]*i;
    z[126]=14*i + n<T>(1741,17)*z[125];
    z[126]=z[126]*z[124];
    z[127]=5 + n<T>(2351,204)*z[3];
    z[128]=z[3]*z[127];
    z[110]= - z[123] + z[110] + z[128] + z[126] + z[115];
    z[110]=z[8]*z[110];
    z[115]=n<T>(3397,153)*z[4];
    z[107]=z[115] - n<T>(1,3)*z[107] + n<T>(445,17)*z[109];
    z[107]=z[4]*z[107];
    z[115]=z[115] + n<T>(4613,306)*z[109] + 13 + n<T>(2351,102)*z[3] - z[117];
    z[115]=z[12]*z[115];
    z[126]=i - n<T>(377,34)*z[125];
    z[126]=z[1]*z[126];
    z[107]=z[115] + z[126] + z[107];
    z[112]=z[112] - z[3];
    z[115]= - n<T>(1,2)*z[112] - z[114];
    z[115]=z[9]*z[115];
    z[126]= - 2 - n<T>(2351,408)*z[3];
    z[126]=z[3]*z[126];
    z[128]= - n<T>(4613,102)*z[4] - n<T>(4613,204)*z[109] + 11 + n<T>(4613,68)*z[3];
    z[128]=z[7]*z[128];
    z[107]= - n<T>(2,3)*z[6] + n<T>(7,6)*z[10] - z[123] + n<T>(4847,102)*z[115] + n<T>(1,18)*z[128]
     + z[126]
     + n<T>(1,6)*z[107];
    z[107]=z[12]*z[107];
    z[115]=11*i;
    z[123]= - z[115] + n<T>(4613,68)*z[125];
    z[123]=z[1]*z[123];
    z[126]=z[109]*z[4];
    z[128]=npow(z[3],2);
    z[123]=n<T>(11,2)*z[7] - n<T>(4613,102)*z[126] + n<T>(4613,136)*z[128] + z[123];
    z[129]=n<T>(1,9)*z[7];
    z[123]=z[123]*z[129];
    z[130]=n<T>(1,2)*z[128];
    z[131]= - z[127]*z[130];
    z[132]=z[1]*z[3];
    z[133]= - 5*i - n<T>(18719,1224)*z[125];
    z[133]=z[133]*z[132];
    z[134]=31*i - n<T>(523,17)*z[125];
    z[134]=z[1]*z[134];
    z[134]= - n<T>(3397,68)*z[128] + z[134];
    z[135]=2 + n<T>(3397,153)*z[109];
    z[135]=z[4]*z[135];
    z[134]=n<T>(1,3)*z[134] + z[135];
    z[134]=z[134]*z[114];
    z[135]=z[109]*z[3];
    z[136]=z[130] + z[135];
    z[137]=n<T>(1,2)*z[136] - n<T>(1,3)*z[126];
    z[116]=z[137]*z[116];
    z[137]=n<T>(1,2)*z[10] + z[118];
    z[137]=z[10]*z[137];
    z[138]=z[6]*z[109];
    z[107]=z[107] + n<T>(4,3)*z[138] + n<T>(7,3)*z[137] + z[110] + z[116] + 
    z[123] + z[134] + z[131] + z[133];
    z[107]=z[12]*z[107];
    z[110]=2*z[7];
    z[116]= - n<T>(8,3)*z[7] - z[3] + n<T>(7,3)*z[109];
    z[116]=z[116]*z[110];
    z[123]=2*z[109];
    z[131]= - z[123] - z[4];
    z[131]=z[4]*z[131];
    z[116]=z[116] + z[131] + z[128] + 2*z[135];
    z[131]=n<T>(1,3)*z[3];
    z[133]=n<T>(1,3)*z[7];
    z[134]=z[133] - z[131] - z[109];
    z[137]=n<T>(1,3)*z[9];
    z[134]=2*z[134] - z[137];
    z[134]=z[9]*z[134];
    z[138]=5*z[109] - z[110];
    z[138]=n<T>(1,3)*z[138] + z[9];
    z[138]=2*z[138] - n<T>(5,3)*z[8];
    z[139]=n<T>(2,3)*z[8];
    z[138]=z[138]*z[139];
    z[140]=7*z[10];
    z[141]=n<T>(4,3)*z[8];
    z[142]=z[141] - z[9];
    z[143]= - z[3] - z[142];
    z[143]=2*z[143] - z[140];
    z[144]=n<T>(1,3)*z[10];
    z[143]=z[143]*z[144];
    z[145]=n<T>(1,3)*z[6];
    z[146]=z[109] + z[4];
    z[147]=2*z[146] - z[6];
    z[147]=z[147]*z[145];
    z[148]=z[10] + z[8];
    z[149]= - z[118] + z[148];
    z[149]=2*z[149] - z[2];
    z[149]=z[2]*z[149];
    z[150]=z[3] + n<T>(2,3)*z[109];
    z[150]= - n<T>(5,3)*z[7] + 4*z[150] - z[4];
    z[150]=n<T>(101,27)*z[5] - n<T>(16,9)*z[2] - n<T>(5,9)*z[12] - z[145] - n<T>(2,9)*
    z[8] + n<T>(1,3)*z[150] - z[9];
    z[150]=z[5]*z[150];
    z[151]= - 5*z[118] - 8*z[12];
    z[151]=z[12]*z[151];
    z[116]=z[150] + n<T>(38,27)*z[46] + n<T>(4,9)*z[149] + n<T>(2,9)*z[151] + z[147]
    + z[143] + z[138] + n<T>(1,3)*z[116] + z[134];
    z[116]=z[5]*z[116];
    z[134]= - 23 + n<T>(73159,68)*z[3];
    z[134]=z[3]*z[134];
    z[138]= - 23*i + n<T>(9157,51)*z[125];
    z[138]=z[1]*z[138];
    z[143]= - n<T>(32411,34)*z[109] + 35 - n<T>(84383,102)*z[3];
    z[143]=n<T>(1,2)*z[143] - n<T>(12485,153)*z[4];
    z[143]=z[4]*z[143];
    z[134]=z[143] + z[134] + z[138];
    z[134]=z[4]*z[134];
    z[138]=47 - n<T>(512891,612)*z[3];
    z[138]=z[138]*z[128];
    z[143]= - 56*i + n<T>(287677,408)*z[125];
    z[143]=z[143]*z[132];
    z[134]=z[134] + z[138] + z[143];
    z[138]=n<T>(1,3)*z[1];
    z[143]=25*i - n<T>(9406,51)*z[125];
    z[143]=z[143]*z[138];
    z[147]=n<T>(11023,51)*z[109] + 23 - n<T>(10747,51)*z[3];
    z[147]=n<T>(1,2)*z[147] + n<T>(9698,51)*z[4];
    z[147]=z[147]*z[114];
    z[149]= - n<T>(16985,306)*z[4] + n<T>(11090,153)*z[109] - 5 + n<T>(10604,153)*
    z[3];
    z[149]=5*z[149] - n<T>(60761,1224)*z[7];
    z[149]=z[149]*z[133];
    z[150]=1 - n<T>(22087,612)*z[3];
    z[150]=z[3]*z[150];
    z[143]=z[149] + z[147] + z[150] + z[143];
    z[143]=z[7]*z[143];
    z[134]=n<T>(1,3)*z[134] + z[143];
    z[134]=z[134]*z[133];
    z[143]=n<T>(180839,102)*z[7] - n<T>(16073,51)*z[4] + n<T>(85,2) - n<T>(29867,51)*
    z[3];
    z[143]=z[143]*z[129];
    z[147]= - n<T>(5,2) + n<T>(3323,17)*z[3];
    z[147]= - n<T>(58765,204)*z[9] + n<T>(37175,306)*z[7] + n<T>(5,2)*z[147] - n<T>(12409,51)*z[4];
    z[147]=z[147]*z[137];
    z[149]=163 - n<T>(561139,408)*z[3];
    z[149]=z[3]*z[149];
    z[150]= - n<T>(161197,1836)*z[4] - n<T>(1,2) + n<T>(210238,459)*z[3];
    z[150]=z[4]*z[150];
    z[151]=n<T>(3361,34)*z[9] + n<T>(26993,612)*z[7] - n<T>(4507,306)*z[4] - 25 - 
   n<T>(279331,612)*z[3];
    z[151]=z[8]*z[151];
    z[143]=n<T>(1,6)*z[151] + z[147] + z[143] + n<T>(1,9)*z[149] + z[150];
    z[147]=n<T>(3530,17)*z[4];
    z[149]= - n<T>(281,4) - n<T>(3137,51)*z[3];
    z[149]=n<T>(1,3)*z[149] + z[147];
    z[150]=n<T>(82,17)*z[7];
    z[149]=n<T>(52624,459)*z[9] + n<T>(1,3)*z[149] + z[150];
    z[149]=n<T>(1,3)*z[149] + n<T>(41,34)*z[10];
    z[149]=z[10]*z[149];
    z[151]=n<T>(1765,17)*z[4];
    z[152]= - n<T>(11,4) + n<T>(3229,51)*z[3];
    z[152]=n<T>(1,3)*z[152] - z[151];
    z[153]=n<T>(41,17)*z[7];
    z[154]=z[153] + n<T>(22991,459)*z[9];
    z[152]=n<T>(1,3)*z[152] - z[154];
    z[155]=z[6] - z[10];
    z[152]=n<T>(1,2)*z[152] - n<T>(41,17)*z[155];
    z[152]=z[6]*z[152];
    z[155]=n<T>(1,9)*z[2];
    z[156]=z[4] - z[3];
    z[157]= - n<T>(1865,17)*z[156];
    z[158]=n<T>(16,3) + z[157];
    z[158]=z[158]*z[155];
    z[117]= - n<T>(4613,408)*z[12] - n<T>(4613,204)*z[8] - n<T>(7045,306)*z[4] - 2
    + n<T>(18541,204)*z[3] - z[117];
    z[117]=z[12]*z[117];
    z[117]=z[158] + n<T>(1,9)*z[117] + z[152] + n<T>(1,3)*z[143] + z[149];
    z[117]=z[46]*z[117];
    z[143]= - 79 + n<T>(709967,306)*z[3];
    z[143]=z[143]*z[130];
    z[149]=z[128]*z[109];
    z[143]=z[143] + n<T>(20123,51)*z[149];
    z[149]= - 19 + n<T>(151081,612)*z[3];
    z[149]=z[149]*z[131];
    z[152]=7 - n<T>(12931,51)*z[3];
    z[152]= - n<T>(6325,51)*z[4] + 13*z[152] + n<T>(121525,51)*z[109];
    z[152]=z[4]*z[152];
    z[158]=n<T>(5,2)*i - n<T>(3788,153)*z[125];
    z[158]=z[1]*z[158];
    z[149]=n<T>(1,54)*z[152] + z[149] + z[158];
    z[149]=z[4]*z[149];
    z[143]=n<T>(1,9)*z[143] + z[149];
    z[143]=z[4]*z[143];
    z[149]=n<T>(7,2)*i;
    z[152]= - z[149] + n<T>(6826,153)*z[125];
    z[152]=z[152]*z[1];
    z[158]=n<T>(3530,17)*z[126];
    z[152]=z[152] + z[158];
    z[159]=z[150]*z[109];
    z[152]=z[159] + n<T>(1,3)*z[152];
    z[159]=n<T>(45982,459)*z[9];
    z[160]=z[159] + z[150];
    z[161]= - n<T>(7,2) + n<T>(6826,153)*z[3];
    z[162]=z[161] + z[147];
    z[163]= - n<T>(48196,153)*z[109] + z[162];
    z[163]=n<T>(1,3)*z[163] + z[160];
    z[163]=z[9]*z[163];
    z[164]=z[160] - n<T>(82,17)*z[109] + n<T>(3530,51)*z[4];
    z[161]=n<T>(1,3)*z[161] + z[164];
    z[161]=z[6]*z[161];
    z[161]= - n<T>(82,17)*z[46] + z[161] + z[163] - z[152];
    z[161]=z[16]*z[161];
    z[159]=z[159]*z[109];
    z[152]=z[152] + z[159];
    z[159]= - z[9] - z[6];
    z[152]=z[152]*z[159];
    z[150]= - n<T>(82,17)*z[6] - n<T>(48196,459)*z[9] - n<T>(1,3)*z[162] - z[150];
    z[150]=z[46]*z[150];
    z[150]=z[150] + z[152];
    z[150]=z[14]*z[150];
    z[152]= - n<T>(26993,102)*z[8] + n<T>(50071,51)*z[9] - n<T>(588073,102)*z[7];
    z[152]=i*z[152];
    z[159]=z[4]*i;
    z[162]= - 113*i + n<T>(333721,51)*z[125];
    z[152]=n<T>(1,2)*z[162] - n<T>(557,3)*z[159] + z[152];
    z[162]=z[10]*i;
    z[152]=n<T>(1,36)*z[152] - n<T>(82,17)*z[162];
    z[162]= - n<T>(26993,918)*z[18] + n<T>(82,17)*z[14] + n<T>(6335,459)*z[17] + 
   n<T>(4613,2754)*z[12] + n<T>(41,34)*z[6];
    z[162]=i*z[162];
    z[152]=n<T>(1,3)*z[152] + z[162];
    z[152]=z[47]*z[152];
    z[162]=17*i + n<T>(260335,1377)*z[125];
    z[162]=z[1]*z[162]*z[130];
    z[163]=643 + n<T>(56927,34)*z[3];
    z[163]=z[163]*npow(z[3],3);
    z[162]=n<T>(1,27)*z[163] + z[162];
    z[163]=n<T>(6826,51)*z[3];
    z[165]=z[163] - n<T>(41,2);
    z[165]=z[164] + n<T>(1,9)*z[165];
    z[166]=z[9] + z[10];
    z[165]=z[24]*z[165]*z[166];
    z[166]= - 158 + n<T>(45031,51)*z[3];
    z[166]=n<T>(1394,9)*z[9] - n<T>(2438,51)*z[4] + n<T>(1,9)*z[166] - n<T>(2107,17)*
    z[118];
    z[166]=z[35]*z[166];
    z[163]= - n<T>(7,2) - z[163];
    z[163]=n<T>(1,9)*z[163] - z[164];
    z[163]=z[40]*z[163];
    z[164]= - n<T>(529,51)*z[9] - n<T>(649,51)*z[4] - n<T>(1,2) + n<T>(523,153)*z[3]
     + 
   n<T>(767,51)*z[118];
    z[164]=z[42]*z[164];
    z[107]=z[152] + z[166] + z[163] + z[164] + z[116] + z[150] + z[161]
    + z[165] + z[84] + z[117] + z[107] + z[134] + n<T>(1,4)*z[162] + z[143]
   ;
    z[116]=n<T>(1865,17)*z[109];
    z[117]=z[116] + n<T>(4798,17)*z[4];
    z[134]=154*z[9];
    z[143]=z[134] - z[117] + n<T>(1865,17)*z[7];
    z[150]=n<T>(19829,51)*z[3];
    z[152]=z[150] - 22;
    z[161]=z[143] + z[152];
    z[139]=n<T>(2,3)*z[2] - z[139];
    z[139]=z[161]*z[139];
    z[162]=155 - n<T>(28238,51)*z[3];
    z[162]=z[3]*z[162];
    z[163]= - i + n<T>(4355,51)*z[125];
    z[163]=z[1]*z[163];
    z[162]=z[162] + 20*z[163];
    z[163]=n<T>(8711,51)*z[4] + n<T>(311,34)*z[109] + 40 - n<T>(66353,306)*z[3];
    z[163]=z[4]*z[163];
    z[164]=n<T>(373,17)*z[109] + 1 - n<T>(4355,51)*z[3];
    z[164]= - n<T>(3730,51)*z[7] + n<T>(20,3)*z[164] - n<T>(311,34)*z[4];
    z[164]=z[7]*z[164];
    z[165]=n<T>(77,3)*z[109] + 2 - n<T>(5591,51)*z[3];
    z[165]= - n<T>(308,3)*z[7] + 4*z[165] - n<T>(15751,51)*z[4];
    z[165]=z[9]*z[165];
    z[139]=n<T>(7691,51)*z[11] + n<T>(4054,51)*z[46] + z[165] + z[164] + n<T>(1,3)*
    z[162] + z[163] + z[139];
    z[139]=z[11]*z[139];
    z[162]= - n<T>(72263,102)*z[125] + 74*i;
    z[163]=z[162]*z[1];
    z[163]=z[163] + n<T>(32185,51)*z[126];
    z[164]= - z[4]*z[163];
    z[165]=n<T>(26993,102)*z[7];
    z[166]=z[126]*z[165];
    z[164]=z[164] + z[166];
    z[166]=z[109]*z[7];
    z[163]=n<T>(26993,102)*z[166] - z[163];
    z[167]=n<T>(3361,17)*z[9];
    z[168]=z[109]*z[167];
    z[163]=n<T>(1,3)*z[163] + z[168];
    z[163]=z[8]*z[163];
    z[168]=z[126]*z[167];
    z[169]=z[167] + n<T>(26993,306)*z[7];
    z[170]= - 74 + n<T>(72263,102)*z[3];
    z[171]=n<T>(1,3)*z[170];
    z[172]=n<T>(26993,306)*z[8] + z[171] - n<T>(4153,34)*z[4] + z[169];
    z[172]=z[46]*z[172];
    z[163]=z[172] + z[163] + n<T>(1,3)*z[164] + z[168];
    z[163]=z[18]*z[163];
    z[164]=z[165] - n<T>(32185,51)*z[4] - n<T>(26993,102)*z[109] + z[170];
    z[164]=n<T>(1,3)*z[164] + z[167];
    z[164]=z[8]*z[164];
    z[165]=z[109] - z[4];
    z[165]= - z[165]*z[169];
    z[162]=z[162]*z[138];
    z[167]= - n<T>(32185,153)*z[4] + z[171] + n<T>(4153,34)*z[109];
    z[167]=z[4]*z[167];
    z[162]= - n<T>(26993,306)*z[46] + z[164] + z[162] + z[167] + z[165];
    z[162]=z[20]*z[162];
    z[120]= - 5 - z[120];
    z[120]=z[38]*z[120];
    z[164]= - n<T>(34453,136)*z[109] - 67 + n<T>(296105,408)*z[3];
    z[164]=n<T>(35485,204)*z[9] + n<T>(34453,408)*z[7] + n<T>(1,3)*z[164] - n<T>(13927,68)*z[4];
    z[164]=z[33]*z[164];
    z[120]= - z[81] + z[78] - z[76] + z[68] + z[162] + z[164] + z[120]
    + z[85] - z[60] + z[139] + z[163];
    z[139]=4*i;
    z[162]= - z[139] - n<T>(383,8)*z[125];
    z[162]=z[162]*z[138];
    z[163]=n<T>(15803,102)*z[109];
    z[164]=4 - n<T>(10229,204)*z[3];
    z[164]=n<T>(17791,51)*z[4] + 13*z[164] + z[163];
    z[111]=z[164]*z[111];
    z[164]= - n<T>(15803,306)*z[4] + n<T>(1,3) + n<T>(383,8)*z[3];
    z[164]=z[164]*z[133];
    z[165]= - n<T>(7465,51)*z[4] - 2 + n<T>(25013,204)*z[3];
    z[165]=z[165]*z[137];
    z[167]= - 5 + n<T>(137473,3672)*z[3];
    z[167]=z[3]*z[167];
    z[111]=z[141] + z[165] + z[164] + z[111] + z[167] + z[162];
    z[111]=z[8]*z[111];
    z[141]=z[139] - n<T>(25013,102)*z[125];
    z[141]=z[1]*z[141];
    z[162]=z[156]*z[7];
    z[164]=2*z[9];
    z[165]=z[109] - n<T>(1,2)*z[4];
    z[165]=z[4]*z[165];
    z[141]= - z[164] - n<T>(154,3)*z[162] + n<T>(4847,51)*z[165] + n<T>(4847,68)*
    z[128] + z[141];
    z[141]=z[141]*z[137];
    z[165]=2*i;
    z[167]= - z[165] - n<T>(26993,68)*z[125];
    z[167]=z[1]*z[167];
    z[168]= - 22 + n<T>(34223,51)*z[3];
    z[163]=n<T>(52963,204)*z[4] + z[163] - z[168];
    z[163]=z[4]*z[163];
    z[169]= - 22 + n<T>(172471,408)*z[3];
    z[169]=z[3]*z[169];
    z[170]=1 + z[157];
    z[170]=z[7]*z[170];
    z[163]=z[170] + z[163] + z[169] + z[167];
    z[163]=z[163]*z[129];
    z[167]=1 - n<T>(3397,68)*z[3];
    z[167]=z[167]*z[131];
    z[127]=n<T>(3397,306)*z[4] - n<T>(8975,612)*z[109] + z[127];
    z[127]=z[4]*z[127];
    z[169]= - 10*i + n<T>(3373,17)*z[125];
    z[169]=z[1]*z[169];
    z[127]=z[127] + z[167] + z[169];
    z[127]=z[127]*z[114];
    z[119]= - n<T>(23,3) - z[119];
    z[119]=z[119]*z[128];
    z[165]=z[165] - n<T>(57757,3672)*z[125];
    z[165]=z[165]*z[132];
    z[111]=z[111] + z[141] + z[163] + z[127] + n<T>(1,3)*z[119] + 5*z[165];
    z[119]=n<T>(1,3)*z[8];
    z[111]=z[111]*z[119];
    z[127]=n<T>(3413,17)*z[3];
    z[141]=z[127] - n<T>(7,4);
    z[131]= - z[141]*z[131];
    z[163]=n<T>(1,2)*i;
    z[165]= - z[163] - n<T>(3505,153)*z[125];
    z[165]=z[1]*z[165];
    z[131]=z[131] + z[165];
    z[165]= - n<T>(1765,51)*z[109] - n<T>(1,3) - n<T>(1765,34)*z[3];
    z[165]=z[4]*z[165];
    z[131]=n<T>(1,6)*z[131] + z[165];
    z[165]= - n<T>(7,2) - n<T>(62147,153)*z[3];
    z[165]=z[151] + n<T>(1,2)*z[165] - n<T>(24098,153)*z[109];
    z[165]=n<T>(1,3)*z[165] + z[154];
    z[165]=z[165]*z[137];
    z[167]=n<T>(41,17)*z[109];
    z[169]= - z[167] + z[154] + n<T>(1765,51)*z[4];
    z[170]=n<T>(3413,51)*z[3];
    z[171]=z[170] - n<T>(77,4);
    z[171]=z[169] + n<T>(1,9)*z[171];
    z[171]=n<T>(1,6)*z[171];
    z[172]=z[171]*z[10];
    z[173]=n<T>(41,34)*z[3];
    z[174]=n<T>(41,51)*z[109];
    z[175]= - z[174] + n<T>(1,9) - z[173];
    z[175]=z[7]*z[175];
    z[176]=n<T>(3413,153)*z[3];
    z[177]=n<T>(1,4) + z[176];
    z[177]=n<T>(1,3)*z[177] + z[169];
    z[145]=z[177]*z[145];
    z[131]=z[145] - z[172] + z[122] + z[165] + n<T>(1,3)*z[131] + z[175];
    z[131]=z[6]*z[131];
    z[145]= - n<T>(77,4)*i + n<T>(92,51)*z[125];
    z[124]=z[145]*z[124];
    z[145]=z[113]*z[151];
    z[165]= - n<T>(7,4) + z[176];
    z[165]=z[3]*z[165];
    z[124]=z[145] + z[165] + z[124];
    z[145]=n<T>(62147,51)*z[3];
    z[165]=n<T>(25205,51)*z[109] - n<T>(7,2) + z[145];
    z[147]=n<T>(1,3)*z[165] - z[147];
    z[147]=n<T>(1,3)*z[147] - z[160];
    z[147]=z[147]*z[137];
    z[113]=z[113]*z[153];
    z[113]= - z[172] + z[147] + n<T>(1,3)*z[124] + z[113];
    z[113]=z[10]*z[113];
    z[124]= - z[141]*z[128];
    z[147]= - z[149] + n<T>(7933,17)*z[125];
    z[147]=z[147]*z[132];
    z[124]=z[124] + z[147];
    z[147]=n<T>(2,3)*i + n<T>(1765,17)*z[125];
    z[147]=z[1]*z[147];
    z[114]= - z[114] - n<T>(1765,34)*z[128] + z[147];
    z[114]=z[4]*z[114];
    z[114]=n<T>(1,18)*z[124] + z[114];
    z[124]=n<T>(41,17)*z[125];
    z[147]= - n<T>(2,9)*i + z[124];
    z[147]=z[1]*z[147];
    z[129]=z[129] - n<T>(41,34)*z[128] + z[147];
    z[129]=z[7]*z[129];
    z[147]=14 - n<T>(22991,34)*z[3];
    z[147]=z[3]*z[147];
    z[147]=z[147] + n<T>(22991,17)*z[135];
    z[147]=z[9]*z[147];
    z[149]= - z[109] - z[7];
    z[149]=2*z[149] + z[8];
    z[122]=z[149]*z[122];
    z[113]=z[131] + z[113] + z[122] + n<T>(1,27)*z[147] + n<T>(1,3)*z[114] + 
    z[129];
    z[113]=z[6]*z[113];
    z[114]=8*i;
    z[122]= - z[114] - n<T>(1865,34)*z[125];
    z[122]=z[1]*z[122];
    z[129]=z[2] + z[10];
    z[131]= - 11 + n<T>(19829,102)*z[3];
    z[131]=z[3]*z[131];
    z[147]=n<T>(2399,17)*z[4] + n<T>(1865,34)*z[109] + 11 - n<T>(34223,102)*z[3];
    z[147]=z[4]*z[147];
    z[149]= - 2 - n<T>(1865,34)*z[156];
    z[149]=z[7]*z[149];
    z[153]=z[9]*z[156];
    z[122]= - 2*z[8] - 77*z[153] + z[149] + z[147] + z[131] + z[122] + 4
   *z[129];
    z[122]=z[2]*z[122];
    z[129]=2*z[128];
    z[131]=z[152]*z[129];
    z[147]= - 22*i + n<T>(8639,51)*z[125];
    z[147]=z[147]*z[132];
    z[122]=z[122] + z[131] + z[147];
    z[131]=n<T>(1865,17)*z[125];
    z[114]=z[114] + z[131];
    z[114]=z[1]*z[114];
    z[117]= - z[117] + z[168];
    z[117]=z[4]*z[117];
    z[134]=z[156]*z[134];
    z[147]= - z[3]*z[152];
    z[149]=4 - z[157];
    z[149]=z[7]*z[149];
    z[114]= - 4*z[8] + z[134] + z[149] + z[117] + z[147] + z[114];
    z[114]=z[114]*z[119];
    z[117]= - z[3] - z[146];
    z[117]=z[4]*z[117];
    z[117]=z[162] + z[117] + z[129] + z[135];
    z[117]=z[9]*z[117];
    z[115]=z[115] - n<T>(842,3)*z[125];
    z[119]=2*z[1];
    z[115]=z[115]*z[119];
    z[129]=22 - n<T>(48617,51)*z[3];
    z[129]=z[3]*z[129];
    z[115]=z[129] + z[115];
    z[129]=22 - n<T>(5435,51)*z[3];
    z[129]=n<T>(4798,51)*z[4] + n<T>(1,3)*z[129] + n<T>(2221,17)*z[109];
    z[129]=z[4]*z[129];
    z[115]=n<T>(1,3)*z[115] + z[129];
    z[115]=z[4]*z[115];
    z[129]=z[139] + z[131];
    z[129]=z[129]*z[119];
    z[131]=22 - n<T>(8639,51)*z[3];
    z[131]=z[3]*z[131];
    z[129]=z[131] + z[129];
    z[131]= - 4 - z[157];
    z[131]=z[131]*z[133];
    z[116]= - z[116] - 11 + n<T>(842,3)*z[3];
    z[116]=n<T>(2,3)*z[116] - n<T>(2221,17)*z[4];
    z[116]=z[4]*z[116];
    z[116]=z[131] + n<T>(1,3)*z[129] + z[116];
    z[116]=z[7]*z[116];
    z[129]=z[123] - z[6];
    z[129]=z[6]*z[129];
    z[131]=npow(z[12],2);
    z[129]=z[129] + z[131];
    z[131]=z[10] - z[8] + z[118];
    z[131]=z[10]*z[131];
    z[114]=n<T>(8,3)*z[131] + z[114] + n<T>(154,3)*z[117] + z[116] + z[115] + n<T>(1,3)*z[122]
     + 2*z[129];
    z[114]=z[114]*z[155];
    z[108]= - n<T>(4847,17)*z[9] - n<T>(4613,102)*z[7] + z[108] + n<T>(4613,102)*
    z[109] + n<T>(2351,34)*z[3];
    z[115]=19 + z[108];
    z[116]=z[12] + z[4];
    z[115]=z[22]*z[115]*z[116];
    z[108]= - 31 - z[108];
    z[108]=z[32]*z[108];
    z[116]= - 23 + z[150] + z[143];
    z[116]=z[36]*z[116];
    z[117]=z[37]*z[121];
    z[121]=z[39]*z[161];
    z[122]= - n<T>(1375141,612)*z[9] - n<T>(3459967,1224)*z[7] + n<T>(100019,612)*
    z[4] + 52 + n<T>(866947,1224)*z[3];
    z[122]=z[55]*z[122];
    z[108]=z[116] + z[117] - z[121] + z[122] + z[74] + z[115] + z[108];
    z[115]=n<T>(41,2) + n<T>(216247,153)*z[3];
    z[115]=z[115]*z[130];
    z[116]=7*i + n<T>(57371,204)*z[125];
    z[116]=z[116]*z[132];
    z[115]=z[115] + z[116];
    z[116]=n<T>(523,6)*z[128] - 14*z[135];
    z[117]= - 4061*z[3] - 7865*z[109];
    z[117]=n<T>(1,6)*z[117] + 131*z[4];
    z[117]=z[4]*z[117];
    z[116]=29*z[116] + z[117];
    z[117]=n<T>(1,17)*z[4];
    z[116]=z[116]*z[117];
    z[115]=n<T>(1,3)*z[115] + z[116];
    z[116]= - 7 - n<T>(6470,153)*z[3];
    z[116]=z[3]*z[116];
    z[121]= - i + n<T>(2258,153)*z[125];
    z[121]=z[1]*z[121];
    z[116]=z[116] + 7*z[121];
    z[121]=n<T>(959,9)*z[3] - 86*z[109];
    z[121]=4*z[121] - n<T>(1109,9)*z[4];
    z[117]=z[121]*z[117];
    z[121]=n<T>(3920,153)*z[109] + n<T>(7,2) - n<T>(7138,153)*z[3];
    z[121]= - n<T>(2390,1377)*z[7] + n<T>(1,3)*z[121] + n<T>(70,17)*z[4];
    z[121]=z[7]*z[121];
    z[116]=z[121] + n<T>(1,3)*z[116] + z[117];
    z[116]=z[7]*z[116];
    z[115]=n<T>(1,3)*z[115] + z[116];
    z[116]=z[163] + n<T>(6952,153)*z[125];
    z[116]=z[116]*z[138];
    z[117]=n<T>(1,4) - n<T>(26587,459)*z[3];
    z[117]=z[3]*z[117];
    z[121]=n<T>(4532,3)*z[4] - 4237*z[3] + n<T>(601,3)*z[109];
    z[121]=z[4]*z[121];
    z[122]=n<T>(6758,459)*z[7] - n<T>(601,153)*z[4] - n<T>(10762,459)*z[109] + n<T>(1,2)
    - n<T>(6952,459)*z[3];
    z[122]=z[7]*z[122];
    z[116]=z[122] + n<T>(1,51)*z[121] + z[117] + z[116];
    z[117]=n<T>(96109,4131)*z[9] + n<T>(21808,1377)*z[4] - n<T>(1,4) - n<T>(114637,4131)
   *z[3] - n<T>(8131,4131)*z[118];
    z[117]=z[9]*z[117];
    z[116]=n<T>(1,3)*z[116] + z[117];
    z[116]=z[9]*z[116];
    z[115]=n<T>(1,3)*z[115] + z[116];
    z[115]=z[9]*z[115];
    z[116]=n<T>(5,4) - z[170];
    z[116]=z[3]*z[116];
    z[117]= - n<T>(23,8)*i + n<T>(481,51)*z[125];
    z[117]=z[1]*z[117];
    z[116]=n<T>(1,2)*z[116] + n<T>(7,3)*z[117];
    z[117]=z[4]*z[112];
    z[116]=n<T>(1,3)*z[116] + n<T>(1765,34)*z[117];
    z[117]=n<T>(20777,51)*z[109] - n<T>(5,2) - z[145];
    z[117]=n<T>(1,6)*z[117] + z[151];
    z[117]=n<T>(1,3)*z[117] + z[154];
    z[117]=z[117]*z[137];
    z[121]= - n<T>(143,4) + z[127];
    z[121]=n<T>(1,27)*z[121] + z[169];
    z[121]=z[121]*z[144];
    z[122]=z[174] + n<T>(7,9) - n<T>(41,17)*z[3];
    z[122]=z[7]*z[122];
    z[116]=z[121] - n<T>(8,27)*z[8] + z[117] + n<T>(1,3)*z[116] + n<T>(1,2)*z[122];
    z[116]=z[10]*z[116];
    z[117]=z[170] - n<T>(13,4);
    z[121]= - z[117]*z[128];
    z[122]=n<T>(13,2)*i - n<T>(5719,51)*z[125];
    z[122]=z[122]*z[132];
    z[121]=z[121] + z[122];
    z[122]= - z[136]*z[151];
    z[121]=n<T>(1,6)*z[121] + z[122];
    z[122]=n<T>(19,2)*i - n<T>(62147,51)*z[125];
    z[122]=z[122]*z[138];
    z[127]=2 - n<T>(22991,102)*z[3];
    z[127]=z[3]*z[127];
    z[122]=z[158] + z[127] + z[122];
    z[127]= - n<T>(1,3) + z[167];
    z[110]=z[127]*z[110];
    z[127]=1 + n<T>(45982,459)*z[109];
    z[127]=z[9]*z[127];
    z[110]=z[127] + n<T>(1,3)*z[122] + z[110];
    z[110]=z[110]*z[137];
    z[122]=n<T>(2,9) - z[173];
    z[122]=z[3]*z[122];
    z[124]= - n<T>(7,9)*i - z[124];
    z[124]=z[1]*z[124];
    z[122]=n<T>(7,18)*z[7] + z[122] + z[124];
    z[122]=z[7]*z[122];
    z[124]=z[123] + z[7];
    z[124]=2*z[124] + z[8];
    z[124]=z[8]*z[124];
    z[110]=z[116] + n<T>(4,27)*z[124] + z[110] + n<T>(1,3)*z[121] + z[122];
    z[110]=z[10]*z[110];
    z[116]=n<T>(50,3)*i - n<T>(8039,34)*z[125];
    z[116]=z[116]*z[132];
    z[121]=n<T>(2395,153)*z[135] - 4*z[126];
    z[121]=z[4]*z[121];
    z[122]= - n<T>(2423,17)*z[125] + n<T>(25,3)*i;
    z[119]=z[122]*z[119];
    z[122]=z[119] + n<T>(12317,51)*z[126];
    z[122]=n<T>(1,3)*z[122] - n<T>(619,34)*z[166];
    z[122]=z[7]*z[122];
    z[116]=z[122] + n<T>(1,3)*z[116] + z[121];
    z[121]=z[166] + z[135];
    z[122]= - z[9]*z[109];
    z[121]=z[122] + 2*z[126] - n<T>(5,9)*z[121];
    z[121]=z[121]*z[164];
    z[122]= - n<T>(25,3) + n<T>(2423,17)*z[3];
    z[124]=n<T>(12317,51)*z[4];
    z[126]= - 10*z[9] - n<T>(916,51)*z[7] - 2*z[122] + z[124];
    z[126]= - n<T>(4655,918)*z[17] + n<T>(1,9)*z[126];
    z[126]=z[46]*z[126];
    z[116]=n<T>(1,3)*z[116] + z[121] + z[126];
    z[116]=z[17]*z[116];
    z[121]= - 7 + 46*z[3];
    z[121]=z[3]*z[121];
    z[121]=n<T>(5,3)*z[121] - n<T>(1844,17)*z[135];
    z[126]= - n<T>(43,2) + n<T>(52160,17)*z[3];
    z[126]=n<T>(9484,17)*z[4] + n<T>(1,3)*z[126] - n<T>(1844,17)*z[118];
    z[126]=n<T>(1,3)*z[126] + 230*z[9];
    z[126]=z[126]*z[137];
    z[127]=118*z[109];
    z[128]=n<T>(2371,3)*z[3] + z[127];
    z[128]=n<T>(1,3)*z[128] + 59*z[4];
    z[128]=z[4]*z[128];
    z[127]=59*z[7] - 118*z[4] + n<T>(461,3)*z[3] - z[127];
    z[127]=z[7]*z[127];
    z[121]= - n<T>(2398,459)*z[21] - n<T>(419,153)*z[46] + z[126] + n<T>(4,51)*
    z[127] + n<T>(1,9)*z[121] + n<T>(4,17)*z[128];
    z[121]=z[21]*z[121];
    z[112]=z[133] - z[112] + z[142];
    z[112]=n<T>(19,3)*z[5] - n<T>(8,3)*z[2] + 2*z[112] + z[140];
    z[112]=z[30]*z[112];
    z[112]= - z[66] + z[112] - z[83];
    z[126]=z[12] + z[6];
    z[127]= - z[109]*z[126];
    z[127]= - z[46] + z[127];
    z[127]=z[15]*z[127];
    z[126]= - z[109] + z[126];
    z[126]=z[28]*z[126];
    z[123]=z[123] - z[2];
    z[128]=z[6] - z[123];
    z[128]=z[25]*z[128];
    z[126]=z[127] + z[128] + z[126] + z[65];
    z[127]= - z[109]*z[148];
    z[127]= - z[46] + z[127];
    z[127]=z[23]*z[127];
    z[128]= - z[109] + z[148];
    z[128]=z[31]*z[128];
    z[127]=z[127] + z[128];
    z[128]= - n<T>(1,27)*z[141] - z[169];
    z[128]=z[41]*z[128];
    z[117]= - n<T>(1,9)*z[117] - z[169];
    z[117]=z[44]*z[117];
    z[117]=z[128] + z[117];
    z[128]= - n<T>(50,3) + n<T>(8039,34)*z[3];
    z[128]=z[3]*z[128];
    z[119]=z[128] + z[119];
    z[122]= - n<T>(458,51)*z[109] + z[122];
    z[122]=2*z[122] - z[124];
    z[122]=n<T>(1,3)*z[122] + n<T>(619,34)*z[7];
    z[122]=z[7]*z[122];
    z[124]= - 2395*z[3] + 12317*z[109];
    z[124]=n<T>(1,153)*z[124] + 4*z[4];
    z[124]=z[4]*z[124];
    z[119]=z[122] + n<T>(1,3)*z[119] + z[124];
    z[122]=z[7] + z[3] - z[109];
    z[122]=z[9] - 2*z[4] + n<T>(5,9)*z[122];
    z[122]=z[122]*z[164];
    z[124]=z[17]*z[109];
    z[119]=n<T>(4655,918)*z[19] - n<T>(4655,459)*z[124] - n<T>(6335,1377)*z[46] + n<T>(1,3)*z[119]
     + z[122];
    z[119]=z[19]*z[119];
    z[122]=z[7] - z[123];
    z[122]=z[26]*z[122];
    z[123]=z[8] - z[123];
    z[123]=z[29]*z[123];
    z[122]= - z[52] + z[122] + z[123] + z[79] + z[70];
    z[123]=z[5]*z[109];
    z[123]=z[123] + z[166] + z[46];
    z[123]=z[13]*z[123];
    z[118]=z[5] - z[118];
    z[118]=z[27]*z[118];
    z[118]=z[71] + z[123] + z[118];
    z[123]=z[43]*z[171];
    z[124]= - 4655*z[3] - 4043*z[109];
    z[124]=n<T>(4043,153)*z[7] + n<T>(1,153)*z[124] - 14*z[4];
    z[124]=n<T>(1,9)*z[124] + z[164];
    z[124]=z[45]*z[124];
    z[125]= - n<T>(665,306)*z[125] - z[159];
    z[128]=z[7]*i;
    z[125]=7*z[125] + n<T>(4043,306)*z[128];
    z[128]=z[9]*i;
    z[125]=n<T>(1,3)*z[125] + 3*z[128];
    z[125]=z[49]*z[125];
    z[128]=z[73] + z[67];
    z[129]=n<T>(14,3)*z[62] - n<T>(4,9)*z[59] + n<T>(25669,5508)*z[57] + n<T>(40,27)*
    z[53] + n<T>(8,27)*z[51];
    z[129]=i*z[129];
    z[109]= - n<T>(2147,51)*z[109] - n<T>(91,6) + n<T>(4315,17)*z[3];
    z[109]=n<T>(131,51)*z[9] + n<T>(2147,459)*z[7] + n<T>(1,9)*z[109] - n<T>(155,17)*
    z[4];
    z[109]=z[34]*z[109];
    z[130]=1 + n<T>(161,34)*z[3];
    z[130]=n<T>(33149,459)*z[9] - n<T>(34849,918)*z[7] + 7*z[130] + n<T>(87905,459)*
    z[4];
    z[130]=z[48]*z[130];
    z[131]= - n<T>(2480,27)*z[9] - n<T>(1888,51)*z[4] + 1 - n<T>(8816,459)*z[3];
    z[131]=z[64]*z[131];
    z[132]=z[12] + z[5];
    z[132]=z[63]*z[132];

    r +=  - n<T>(16,81)*z[50] - n<T>(20,27)*z[54] + n<T>(862217,660960)*z[56] + 
      n<T>(3839,918)*z[58] - n<T>(8,3)*z[61] + n<T>(43,54)*z[69] + n<T>(16,27)*z[72]
     +  n<T>(7,54)*z[75] - n<T>(1,18)*z[77] - n<T>(8,27)*z[80] - n<T>(14,27)*z[82]
     + n<T>(3637,459)*z[86] - n<T>(2420,459)*z[87]
     + n<T>(2476,1377)*z[88] - n<T>(326,153)*
      z[89] + n<T>(1444,459)*z[90] + n<T>(4276,1377)*z[91] + n<T>(4510,459)*z[92]
       + z[93] + n<T>(1039,51)*z[94] + n<T>(16,153)*z[95] - n<T>(4568,459)*z[96]
     - 
      n<T>(6634,1377)*z[97] + n<T>(5938,1377)*z[98] + n<T>(1253,459)*z[99] - n<T>(154,459)*z[100]
     + n<T>(3268,1377)*z[101]
     + n<T>(59,51)*z[102] - n<T>(767,153)*
      z[103] + n<T>(8,51)*z[104] - n<T>(944,153)*z[105] + z[106] + n<T>(1,3)*z[107]
       + n<T>(1,27)*z[108] + z[109] + z[110] + z[111] + n<T>(2,9)*z[112] + 
      z[113] + z[114] + z[115] + z[116] + n<T>(1,2)*z[117] + n<T>(2,3)*z[118]
       + z[119] + n<T>(1,9)*z[120] + z[121] + n<T>(4,27)*z[122] + z[123] + 
      z[124] + z[125] + n<T>(4,9)*z[126] + n<T>(8,9)*z[127] + n<T>(10,27)*z[128]
     +  z[129] + n<T>(1,6)*z[130] + z[131] + n<T>(22,27)*z[132];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1277(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1277(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
