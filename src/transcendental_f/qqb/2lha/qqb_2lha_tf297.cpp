#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf297(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=f[22];
    z[4]=f[24];
    z[5]=f[42];
    z[6]=f[61];
    z[7]=f[62];
    z[8]=f[70];
    z[9]=f[81];
    z[10]=z[5] - z[9];
    z[11]=z[1]*i;

    r += 10*z[2] + n<T>(16,3)*z[3] + n<T>(26,3)*z[4] + n<T>(20,3)*z[6] + n<T>(22,3)*
      z[7] - z[8] - 4*z[10] - n<T>(10,27)*z[11];
 
    return r;
}

template std::complex<double> qqb_2lha_tf297(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf297(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
