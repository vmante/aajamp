#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf984(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[164];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[1];
    z[7]=d[4];
    z[8]=d[5];
    z[9]=d[6];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[8];
    z[15]=d[15];
    z[16]=d[11];
    z[17]=d[16];
    z[18]=d[12];
    z[19]=d[17];
    z[20]=d[18];
    z[21]=e[2];
    z[22]=e[4];
    z[23]=e[5];
    z[24]=e[7];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=f[0];
    z[30]=f[3];
    z[31]=f[5];
    z[32]=f[8];
    z[33]=f[11];
    z[34]=f[13];
    z[35]=f[17];
    z[36]=f[20];
    z[37]=c[3];
    z[38]=c[11];
    z[39]=c[15];
    z[40]=c[18];
    z[41]=c[19];
    z[42]=c[21];
    z[43]=c[23];
    z[44]=c[25];
    z[45]=c[26];
    z[46]=c[28];
    z[47]=c[29];
    z[48]=c[30];
    z[49]=c[31];
    z[50]=c[32];
    z[51]=c[43];
    z[52]=c[46];
    z[53]=c[47];
    z[54]=c[51];
    z[55]=c[54];
    z[56]=c[55];
    z[57]=c[58];
    z[58]=c[59];
    z[59]=c[60];
    z[60]=c[61];
    z[61]=c[62];
    z[62]=c[66];
    z[63]=c[68];
    z[64]=c[69];
    z[65]=c[72];
    z[66]=c[75];
    z[67]=c[76];
    z[68]=c[77];
    z[69]=c[78];
    z[70]=c[79];
    z[71]=c[80];
    z[72]=c[83];
    z[73]=e[3];
    z[74]=e[6];
    z[75]=e[13];
    z[76]=e[14];
    z[77]=f[1];
    z[78]=f[4];
    z[79]=f[6];
    z[80]=f[7];
    z[81]=f[10];
    z[82]=f[12];
    z[83]=f[14];
    z[84]=f[16];
    z[85]=f[18];
    z[86]=f[21];
    z[87]=f[23];
    z[88]=f[30];
    z[89]=f[31];
    z[90]=f[32];
    z[91]=f[33];
    z[92]=f[34];
    z[93]=f[35];
    z[94]=f[36];
    z[95]=f[37];
    z[96]=f[39];
    z[97]=f[41];
    z[98]=f[43];
    z[99]=f[50];
    z[100]=f[51];
    z[101]=f[52];
    z[102]=f[54];
    z[103]=f[55];
    z[104]=f[56];
    z[105]=f[58];
    z[106]=f[60];
    z[107]=g[0];
    z[108]=g[3];
    z[109]=g[5];
    z[110]=g[8];
    z[111]=g[11];
    z[112]=g[13];
    z[113]=g[17];
    z[114]=g[21];
    z[115]=g[29];
    z[116]=g[30];
    z[117]=g[35];
    z[118]=g[41];
    z[119]=g[43];
    z[120]=g[56];
    z[121]=g[57];
    z[122]=g[76];
    z[123]=g[93];
    z[124]=g[94];
    z[125]=g[107];
    z[126]=g[125];
    z[127]=z[5]*i;
    z[128]=z[2]*i;
    z[129]=z[4]*i;
    z[130]= - n<T>(34,3)*z[127] - n<T>(7,4)*z[129] - 4*z[128];
    z[130]=z[1]*z[130];
    z[131]=npow(z[4],2);
    z[132]=n<T>(7,3)*z[4];
    z[133]= - z[132] + 5*z[2];
    z[133]=z[2]*z[133];
    z[133]=n<T>(7,6)*z[131] + z[133];
    z[134]=n<T>(1,3)*z[5];
    z[135]=z[2] - z[4];
    z[136]= - n<T>(7,44)*z[135] + n<T>(2,3)*z[5];
    z[136]=z[136]*z[134];
    z[137]=n<T>(1,2)*z[3];
    z[138]=z[1]*i;
    z[139]=z[137] - z[138];
    z[140]=z[3]*z[139];
    z[141]=z[138] - z[5];
    z[142]= - n<T>(5,2)*z[8] + 7*z[135] + 23*z[141];
    z[142]=z[8]*z[142];
    z[143]=n<T>(1,2)*z[7];
    z[144]=z[143] + z[141];
    z[144]=z[7]*z[144];
    z[145]=z[132] - 3*z[2];
    z[145]= - n<T>(5,18)*z[9] - n<T>(23,24)*z[7] - n<T>(35,24)*z[8] - n<T>(25,36)*z[138]
    + n<T>(1,8)*z[145] + n<T>(17,9)*z[5];
    z[145]=z[9]*z[145];
    z[130]=n<T>(1,11)*z[145] + n<T>(67,396)*z[144] + n<T>(1,132)*z[142] - n<T>(23,132)*
    z[140] + n<T>(1,33)*z[130] + n<T>(1,44)*z[133] + z[136];
    z[130]=z[9]*z[130];
    z[133]=z[138] - z[4];
    z[136]=n<T>(1,4)*z[10];
    z[140]=n<T>(1,2)*z[2];
    z[142]=n<T>(1,2)*z[5];
    z[144]= - z[136] - z[142] + z[140] + 2*z[133];
    z[144]=z[10]*z[144];
    z[145]=z[127] - n<T>(7,3)*z[129] - z[128];
    z[145]=z[1]*z[145];
    z[146]=z[142] - z[135];
    z[147]= - z[146]*z[142];
    z[148]= - z[135]*z[137];
    z[149]=n<T>(1,3)*z[4];
    z[150]= - z[3] + n<T>(1,3)*z[138] - z[149] + z[2];
    z[150]=n<T>(1,2)*z[150] + n<T>(2,3)*z[7];
    z[150]=z[7]*z[150];
    z[151]=z[4] - n<T>(1,4)*z[2];
    z[151]=z[2]*z[151];
    z[144]=z[150] + z[144] + z[148] + n<T>(1,2)*z[145] + z[147] - n<T>(1,3)*
    z[131] + z[151];
    z[145]=z[134] + n<T>(7,9)*z[4] - z[2];
    z[145]=n<T>(1,6)*z[3] + n<T>(1,2)*z[145] - n<T>(7,9)*z[138];
    z[145]= - n<T>(1,4)*z[6] + n<T>(1,36)*z[7] + n<T>(1,2)*z[145] + n<T>(1,3)*z[10];
    z[145]=z[6]*z[145];
    z[144]=n<T>(1,3)*z[144] + z[145];
    z[144]=z[6]*z[144];
    z[145]=npow(z[5],2);
    z[147]=z[2]*z[4];
    z[148]= - n<T>(17,3)*z[145] - n<T>(7,12)*z[131] - z[147];
    z[150]=n<T>(7,6)*z[129];
    z[151]=z[150] + z[128];
    z[151]=n<T>(1,2)*z[151] + n<T>(17,3)*z[127];
    z[151]=z[1]*z[151];
    z[148]=n<T>(1,2)*z[148] + z[151];
    z[132]= - z[132] + 23*z[2];
    z[132]= - n<T>(23,4)*z[3] + n<T>(25,4)*z[138] + n<T>(1,4)*z[132] - n<T>(17,3)*z[5];
    z[151]=n<T>(15,4)*z[8];
    z[132]=n<T>(29,6)*z[7] - z[136] + n<T>(1,3)*z[132] - z[151];
    z[132]=z[132]*z[143];
    z[143]=z[149] + n<T>(7,2)*z[138];
    z[137]=z[143]*z[137];
    z[143]=n<T>(1,2)*z[8];
    z[149]= - z[143] - z[141];
    z[149]=z[149]*z[151];
    z[151]=n<T>(1,2)*z[10];
    z[152]= - z[151] - z[133];
    z[136]=z[152]*z[136];
    z[132]=z[132] + z[136] + z[149] + n<T>(1,3)*z[148] + z[137];
    z[132]=z[7]*z[132];
    z[136]=z[5]*z[146];
    z[137]= - z[4] + z[140];
    z[137]=z[2]*z[137];
    z[136]=z[136] + n<T>(5,4)*z[131] + z[137];
    z[137]= - n<T>(5,2)*z[129] + z[128];
    z[137]=n<T>(1,2)*z[137] + z[127];
    z[137]=z[1]*z[137];
    z[146]=n<T>(5,8)*z[3];
    z[139]= - z[139]*z[146];
    z[136]=z[139] + n<T>(1,2)*z[136] + z[137];
    z[137]=z[138] - z[3];
    z[139]= - z[143] - z[137];
    z[143]=n<T>(1,8)*z[8];
    z[139]=z[139]*z[143];
    z[146]= - z[146] - n<T>(5,8)*z[138] - z[5] + n<T>(5,4)*z[4] + z[2];
    z[143]=n<T>(23,36)*z[10] + n<T>(1,3)*z[146] - z[143];
    z[143]=z[143]*z[151];
    z[136]=z[143] + n<T>(1,3)*z[136] + z[139];
    z[136]=z[10]*z[136];
    z[139]=z[141] + z[135];
    z[143]=n<T>(29,6)*z[9] - n<T>(7,6)*z[139] + 6*z[8];
    z[143]=z[24]*z[143];
    z[139]=n<T>(5,2)*z[10] - z[139];
    z[139]=n<T>(1,3)*z[139] + n<T>(1,2)*z[6];
    z[139]=z[26]*z[139];
    z[146]= - n<T>(101,18)*z[3] + n<T>(963,8)*z[138] + n<T>(1369,72)*z[5] - n<T>(167,3)*
    z[2] - n<T>(23,16) - n<T>(703,9)*z[4];
    z[146]=z[35]*z[146];
    z[132]=z[146] + z[144] + z[136] + z[132] + z[143] + z[139];
    z[136]=229*i - n<T>(3857,3)*z[129];
    z[136]=n<T>(11689,2)*z[127] + n<T>(1,2)*z[136] - n<T>(15605,3)*z[128];
    z[136]=z[1]*z[136];
    z[139]= - n<T>(5,8) + n<T>(373,11)*z[4];
    z[139]=z[4]*z[139];
    z[143]=n<T>(131,88) + 20*z[4];
    z[143]=n<T>(1,9)*z[143] + n<T>(541,88)*z[2];
    z[143]=z[2]*z[143];
    z[144]= - n<T>(23,2) - n<T>(4199,27)*z[4];
    z[144]= - n<T>(71,9)*z[5] + n<T>(1,2)*z[144] - n<T>(9869,27)*z[2];
    z[144]=z[5]*z[144];
    z[146]= - n<T>(3131,288)*z[3] + n<T>(3131,72)*z[138] - n<T>(571,4)*z[5] + n<T>(8227,36)*z[2]
     - n<T>(29,8)
     - n<T>(772,9)*z[4];
    z[146]=z[3]*z[146];
    z[136]=n<T>(1,99)*z[146] + n<T>(1,396)*z[136] + n<T>(1,44)*z[144] + n<T>(1,18)*
    z[139] + z[143];
    z[136]=z[3]*z[136];
    z[139]= - n<T>(5,16) + n<T>(1784,99)*z[4];
    z[139]=z[139]*z[131];
    z[143]= - 16 - n<T>(3539,2)*z[4];
    z[143]=z[4]*z[143];
    z[144]=n<T>(43,2) + 1003*z[4];
    z[144]=n<T>(1,4)*z[144] + n<T>(1879,9)*z[2];
    z[144]=z[2]*z[144];
    z[143]=z[143] + z[144];
    z[143]=z[2]*z[143];
    z[139]=z[139] + n<T>(1,11)*z[143];
    z[143]=n<T>(1,8)*z[4];
    z[144]=23 + n<T>(36733,27)*z[4];
    z[144]=z[144]*z[143];
    z[146]=n<T>(631,36)*z[5] - n<T>(725,9)*z[2] - n<T>(23,8) - n<T>(571,3)*z[4];
    z[142]=z[146]*z[142];
    z[146]=320*z[4] + 221*z[2];
    z[146]=z[2]*z[146];
    z[142]=z[142] + z[144] + n<T>(4,27)*z[146];
    z[144]=n<T>(1,11)*z[5];
    z[142]=z[142]*z[144];
    z[146]=n<T>(8143,36)*z[127] + n<T>(8123,27)*z[128] + n<T>(23,4)*i - n<T>(8549,27)*
    z[129];
    z[146]=z[5]*z[146];
    z[148]= - i + n<T>(2605,33)*z[129];
    z[148]=n<T>(5,2)*z[148] - n<T>(2389,11)*z[128];
    z[148]=z[2]*z[148];
    z[149]=31*i - n<T>(17489,3)*z[129];
    z[149]=z[4]*z[149];
    z[148]=n<T>(1,88)*z[149] + z[148];
    z[146]=n<T>(1,9)*z[148] + n<T>(1,22)*z[146];
    z[146]=z[1]*z[146];
    z[136]=z[136] + z[146] + n<T>(1,9)*z[139] + z[142];
    z[136]=z[3]*z[136];
    z[139]=n<T>(2117,3)*z[127] + n<T>(36307,6)*z[128] + n<T>(149,4)*i + n<T>(12733,3)*
    z[129];
    z[139]=z[1]*z[139];
    z[142]=n<T>(1,11)*z[4];
    z[146]=n<T>(1,6) - 4*z[4];
    z[146]=z[146]*z[142];
    z[148]= - 12*z[5] + 8*z[4] - n<T>(1253,54)*z[2];
    z[148]=z[148]*z[144];
    z[149]= - 2 - n<T>(12733,66)*z[4];
    z[149]= - n<T>(12655,2376)*z[3] + n<T>(15064,297)*z[138] - n<T>(2117,594)*z[5]
    + n<T>(1,9)*z[149] - n<T>(663,44)*z[2];
    z[149]=z[3]*z[149];
    z[152]= - n<T>(67,2) - n<T>(12733,3)*z[4];
    z[152]=n<T>(1,11)*z[152] - n<T>(2431,12)*z[2];
    z[152]=z[2]*z[152];
    z[139]= - n<T>(87,176)*z[12] + n<T>(128299,1584)*z[37] - n<T>(1,12)*z[11] + n<T>(1,66)*z[8]
     + z[149]
     + n<T>(1,99)*z[139]
     + z[148]
     + z[146]
     + n<T>(1,18)*z[152];
    z[139]=z[12]*z[139];
    z[146]=n<T>(1,2)*i + n<T>(1004,99)*z[129];
    z[146]= - n<T>(1537,66)*z[127] + 5*z[146] + n<T>(247,2)*z[128];
    z[146]=z[1]*z[146];
    z[148]=n<T>(1,3)*z[2];
    z[149]= - n<T>(16705,99)*z[2] - 5 - n<T>(10121,99)*z[4];
    z[149]=z[149]*z[148];
    z[149]=n<T>(3,11)*z[131] + z[149];
    z[152]= - n<T>(371,198)*z[2] - n<T>(1,4) - n<T>(502,99)*z[4];
    z[152]=5*z[152] + n<T>(6139,198)*z[5];
    z[152]=z[152]*z[134];
    z[153]= - 3*z[4] - n<T>(185,3)*z[2];
    z[153]=n<T>(3,44)*z[3] + n<T>(8,3)*z[138] + n<T>(1,44)*z[153] - n<T>(4,3)*z[5];
    z[153]=z[3]*z[153];
    z[146]= - n<T>(63,22)*z[14] + n<T>(31679,1188)*z[37] + z[153] + n<T>(1,3)*z[146]
    + n<T>(1,4)*z[149] + z[152];
    z[146]=z[14]*z[146];
    z[149]=z[3]*i;
    z[152]= - n<T>(391,8)*i - n<T>(3023,3)*z[129];
    z[152]= - n<T>(499,33)*z[149] + n<T>(15335,132)*z[127] + n<T>(1,11)*z[152] - n<T>(913,6)*z[128];
    z[152]=z[43]*z[152];
    z[153]= - n<T>(66317,66)*z[128] - n<T>(53,2)*i - n<T>(32765,33)*z[129];
    z[153]= - n<T>(14903,132)*z[149] + n<T>(1,2)*z[153] + n<T>(716,3)*z[127];
    z[153]=z[45]*z[153];
    z[154]=n<T>(1153,2)*z[4] + 98*z[2];
    z[154]=n<T>(1085,132)*z[3] - n<T>(2710,33)*z[138] + n<T>(1,11)*z[154] + n<T>(151,12)
   *z[5];
    z[154]=z[36]*z[154];
    z[155]= - n<T>(1,32) + n<T>(1747,9)*z[4];
    z[155]=n<T>(881047,144)*z[5] + 35*z[155] - n<T>(781603,288)*z[2];
    z[155]=n<T>(1,11)*z[155] + n<T>(16783,288)*z[3];
    z[155]=z[49]*z[155];
    z[152]=z[154] + z[152] + z[153] + z[155];
    z[153]=13 + n<T>(3169,18)*z[4];
    z[153]=z[153]*z[131];
    z[154]= - n<T>(91303,18)*z[2] - n<T>(169,2) - n<T>(18619,9)*z[4];
    z[154]=z[154]*z[140];
    z[155]=n<T>(103,4) + 2909*z[4];
    z[155]=z[4]*z[155];
    z[154]=z[155] + z[154];
    z[154]=z[154]*z[140];
    z[153]=z[153] + z[154];
    z[153]=z[153]*z[148];
    z[154]= - n<T>(2813,6)*z[5] - n<T>(1037,2)*z[2] + n<T>(1,2) - 1153*z[4];
    z[154]= - n<T>(1303,6)*z[3] + n<T>(1,2)*z[154] + n<T>(3862,3)*z[138];
    z[154]=z[29]*z[154];
    z[155]= - n<T>(5,8) - n<T>(691,9)*z[4];
    z[155]= - n<T>(379,6)*z[3] + n<T>(9749,18)*z[138] - n<T>(1307,18)*z[5] + n<T>(7,2)*
    z[155] - n<T>(1234,9)*z[2];
    z[155]=z[31]*z[155];
    z[156]=1 + n<T>(18103,108)*z[4];
    z[156]=z[156]*npow(z[4],3);
    z[153]=z[155] + z[154] + n<T>(1,4)*z[156] + z[153];
    z[154]=n<T>(1,2)*z[131];
    z[155]= - n<T>(17,8) - n<T>(6737,81)*z[4];
    z[155]=z[155]*z[154];
    z[156]= - 3 - n<T>(4955,9)*z[4];
    z[156]=z[4]*z[156];
    z[157]=n<T>(9689,162)*z[2] + n<T>(3,8) - n<T>(37,9)*z[4];
    z[157]=z[2]*z[157];
    z[156]=n<T>(1,4)*z[156] + z[157];
    z[156]=z[2]*z[156];
    z[155]=z[155] + z[156];
    z[156]= - n<T>(59,2) - n<T>(3919,9)*z[4];
    z[156]=z[156]*z[143];
    z[157]=n<T>(17363,72)*z[2] + 8 + n<T>(8119,36)*z[4];
    z[157]=z[2]*z[157];
    z[156]=z[156] + z[157];
    z[157]=n<T>(1,4)*z[5];
    z[158]= - 1 + n<T>(7015,54)*z[4];
    z[158]=n<T>(32341,2376)*z[5] + n<T>(1,11)*z[158] - n<T>(577,27)*z[2];
    z[158]=z[158]*z[157];
    z[156]=n<T>(1,11)*z[156] + z[158];
    z[134]=z[156]*z[134];
    z[134]=n<T>(1,11)*z[155] + z[134];
    z[134]=z[5]*z[134];
    z[155]= - n<T>(32341,162)*z[127] - n<T>(14785,27)*z[128] + 3*i + n<T>(5141,54)*
    z[129];
    z[155]=z[155]*z[157];
    z[156]=n<T>(59,3)*i + 1029*z[129];
    z[143]=z[156]*z[143];
    z[156]= - n<T>(39833,36)*z[128] - 16*i + n<T>(2315,9)*z[129];
    z[156]=z[156]*z[148];
    z[143]=z[155] + z[143] + z[156];
    z[143]=z[143]*z[144];
    z[155]= - 29*i - n<T>(5147,12)*z[129];
    z[142]=z[155]*z[142];
    z[155]=n<T>(174331,198)*z[128] + n<T>(167,22)*i - n<T>(857,3)*z[129];
    z[140]=z[155]*z[140];
    z[140]=z[142] + z[140];
    z[140]=z[140]*z[148];
    z[142]= - i - n<T>(18103,27)*z[129];
    z[142]=z[142]*z[131];
    z[140]=n<T>(1,44)*z[142] + z[140];
    z[140]=n<T>(1,3)*z[140] + z[143];
    z[140]=z[1]*z[140];
    z[142]=3*z[127] + z[150] - z[128];
    z[142]=z[1]*z[142];
    z[131]= - z[131] + z[147];
    z[143]=n<T>(7,6)*z[135] - 3*z[5];
    z[143]=z[5]*z[143];
    z[131]=n<T>(1,3)*z[131] + z[143];
    z[143]=n<T>(7,36)*z[3] - n<T>(7,18)*z[138] + z[135];
    z[143]=z[3]*z[143];
    z[131]=n<T>(1,4)*z[143] + n<T>(1,2)*z[131] + z[142];
    z[142]=n<T>(91,88)*z[8] + n<T>(7,528)*z[3] + n<T>(125,528)*z[138] - n<T>(1,11)*
    z[135] - z[157];
    z[143]=n<T>(1,3)*z[8];
    z[142]=z[142]*z[143];
    z[131]=n<T>(1,11)*z[131] + z[142];
    z[131]=z[8]*z[131];
    z[142]= - z[154] + z[147];
    z[142]=n<T>(5,3)*z[142] - z[145];
    z[145]=n<T>(5,11)*z[129] - z[128];
    z[145]=n<T>(1,6)*z[145] + n<T>(1,11)*z[127];
    z[145]=z[1]*z[145];
    z[147]=n<T>(1,11)*z[3];
    z[148]= - z[2] + n<T>(1,6)*z[138] + n<T>(5,6)*z[4];
    z[150]=n<T>(1,18)*z[3] - z[148];
    z[150]=z[150]*z[147];
    z[142]=z[150] + n<T>(1,22)*z[142] + z[145];
    z[135]=z[143] - n<T>(5,4)*z[135] + n<T>(2,3)*z[137];
    z[135]=z[8]*z[135];
    z[143]= - z[151] - z[137];
    z[143]=z[10]*z[143];
    z[145]= - z[10] + n<T>(5,6)*z[8] - z[148];
    z[145]=z[11]*z[145];
    z[135]=n<T>(1,44)*z[145] + n<T>(1,132)*z[143] + n<T>(1,2)*z[142] + n<T>(1,33)*z[135]
   ;
    z[135]=z[11]*z[135];
    z[142]=n<T>(25,33)*i - 41*z[129];
    z[142]= - n<T>(1171,198)*z[127] + n<T>(1,2)*z[142] + n<T>(5300,99)*z[128];
    z[142]=z[1]*z[142];
    z[143]=1481*z[5];
    z[145]=1279*z[4] + 1063*z[2];
    z[145]=n<T>(1,3)*z[145] - z[143];
    z[145]=z[5]*z[145];
    z[148]= - 19 + n<T>(2309,3)*z[4];
    z[148]=z[4]*z[148];
    z[145]=z[148] + z[145];
    z[148]= - n<T>(3629,18)*z[2] - n<T>(31,6) - 809*z[4];
    z[148]=z[2]*z[148];
    z[145]=z[148] + n<T>(1,6)*z[145];
    z[142]=n<T>(1,22)*z[145] + z[142];
    z[145]=16783*z[4] - 443*z[2];
    z[143]= - n<T>(12289,3)*z[138] + n<T>(1,6)*z[145] + z[143];
    z[143]=n<T>(1,18)*z[143] - 3*z[3];
    z[143]=z[143]*z[147];
    z[142]=n<T>(767,396)*z[13] - n<T>(30341,4752)*z[37] + n<T>(1,3)*z[142] + z[143];
    z[142]=z[13]*z[142];
    z[127]= - n<T>(34993,16)*z[149] + n<T>(130813,48)*z[127] - n<T>(17923,3)*z[128]
    + 19*i + n<T>(22681,12)*z[129];
    z[127]=z[38]*z[127];
    z[128]= - n<T>(13,44) - 691*z[4];
    z[128]=z[4]*z[128];
    z[129]=n<T>(188927,2)*z[2] - 395 - 9217*z[4];
    z[129]=z[2]*z[129];
    z[128]=z[128] + n<T>(1,22)*z[129];
    z[129]= - n<T>(475705,192)*z[5] - n<T>(166123,16)*z[2] + n<T>(569,16) + 3340*
    z[4];
    z[129]=z[129]*z[144];
    z[128]=n<T>(1,4)*z[128] + z[129];
    z[129]=n<T>(17557,576)*z[3] + n<T>(360427,432)*z[5] - n<T>(29351,48)*z[2] + n<T>(239,18)
     - 109*z[4];
    z[129]=z[129]*z[147];
    z[128]=n<T>(1,36)*z[11] - n<T>(5,22)*z[9] - n<T>(1,33)*z[6] + n<T>(185,528)*z[7]
     - n<T>(1,22)*z[10]
     + n<T>(91,198)*z[8]
     + n<T>(1,9)*z[128]
     + z[129];
    z[128]=z[37]*z[128];
    z[129]= - n<T>(59,8) - n<T>(883,9)*z[4];
    z[129]=n<T>(7789,72)*z[5] + n<T>(1,2)*z[129] - n<T>(793,9)*z[2];
    z[129]= - n<T>(95,33)*z[3] + n<T>(1,11)*z[129] + n<T>(397,72)*z[138];
    z[129]=z[33]*z[129];
    z[128]=z[128] + z[129];
    z[129]=z[138]*z[4];
    z[129]=z[129] + z[37];
    z[143]= - z[8]*z[138];
    z[143]=z[143] - z[129];
    z[143]=z[18]*z[143];
    z[144]=z[8] - z[133];
    z[144]=z[23]*z[144];
    z[143]= - z[143] - z[144] + z[89] + z[87] + z[86] - z[83];
    z[144]=z[138]*z[5];
    z[144]=z[144] + z[37];
    z[145]= - z[10]*z[138];
    z[145]=z[145] - z[144];
    z[145]=z[20]*z[145];
    z[147]=z[10] - z[141];
    z[147]=z[27]*z[147];
    z[145]= - z[145] - z[147] + z[106] - z[81];
    z[147]=z[138]*z[3];
    z[147]=z[147] + z[37];
    z[148]= - z[7]*z[138];
    z[148]=z[148] - z[147];
    z[148]=z[17]*z[148];
    z[149]=z[7] - z[137];
    z[149]=z[22]*z[149];
    z[148]= - z[148] - z[149] + z[97] + z[51];
    z[149]= - z[9]*z[138];
    z[144]=z[149] - z[144];
    z[144]=z[19]*z[144];
    z[141]= - z[9] + z[141];
    z[141]=z[25]*z[141];
    z[141]=z[144] + z[141];
    z[144]= - z[11]*z[138];
    z[144]=z[144] - z[147];
    z[144]=z[16]*z[144];
    z[137]=z[11] - z[137];
    z[137]=z[28]*z[137];
    z[137]=z[144] + z[137];
    z[144]=z[6]*z[138];
    z[129]=z[144] + z[129];
    z[129]=z[15]*z[129];
    z[133]=z[6] - z[133];
    z[133]=z[21]*z[133];
    z[129]=z[129] + z[133];
    z[133]=n<T>(3115,594)*z[3] + n<T>(3761,297)*z[5] + n<T>(211,18)*z[2];
    z[144]=149*z[4];
    z[147]=n<T>(509,18) + z[144];
    z[147]=n<T>(1,11)*z[147] + z[133];
    z[147]=z[39]*z[147];
    z[144]=z[144] + n<T>(1,2);
    z[149]= - n<T>(3115,198)*z[3] - n<T>(3761,99)*z[5] - n<T>(3,11)*z[144] - n<T>(211,6)
   *z[2];
    z[149]=z[47]*z[149];
    z[147]=z[147] + z[149];
    z[149]= - n<T>(5557,3)*z[2] - 61 - n<T>(2447,3)*z[4];
    z[149]=n<T>(10,3)*z[3] + 502*z[138] + n<T>(1,4)*z[149] + n<T>(485,3)*z[5];
    z[149]=z[34]*z[149];
    z[150]=n<T>(13121,3)*z[2] + n<T>(529,4) + n<T>(20725,3)*z[4];
    z[150]=n<T>(3017,6)*z[3] + n<T>(1,2)*z[150] - n<T>(5513,3)*z[5];
    z[150]=z[46]*z[150];
    z[149]=z[149] + z[150];
    z[150]=n<T>(3439,264)*z[5] - n<T>(859,132)*z[2] - n<T>(5,16) + n<T>(1039,33)*z[4];
    z[150]=n<T>(17,27)*z[3] + n<T>(1,9)*z[150] - n<T>(427,88)*z[138];
    z[150]=z[30]*z[150];
    z[151]=n<T>(1,2)*z[4] - 41*z[2];
    z[138]=n<T>(1,22)*z[3] - n<T>(4,11)*z[138] + n<T>(1,11)*z[151] + 4*z[5];
    z[138]=z[32]*z[138];
    z[133]=n<T>(1,11)*z[144] + z[133];
    z[133]=z[40]*z[133];
    z[144]=z[125] - z[122] + z[104] + z[79];
    z[151]= - z[80] + z[98] - z[93];
    z[154]=z[71] + z[57];
    z[155]=z[95] - z[77];
    z[156]= - n<T>(76403,1188)*z[62] - n<T>(114073,3564)*z[58] + n<T>(17923,1782)*
    z[53] + n<T>(3,44)*z[42];
    z[156]=i*z[156];
    z[157]= - n<T>(10493,3)*z[2] + n<T>(1565,4) - n<T>(12121,3)*z[4];
    z[157]= - n<T>(1117,3)*z[3] + n<T>(1,2)*z[157] - n<T>(337,3)*z[5];
    z[157]=z[41]*z[157];
    z[158]=n<T>(625,44)*z[3] - n<T>(2329,22)*z[5] + n<T>(15775,132)*z[2] + n<T>(31,8)
     + 
   n<T>(5483,33)*z[4];
    z[158]=z[44]*z[158];
    z[159]= - n<T>(2593,198)*z[3] - n<T>(7661,99)*z[5] - n<T>(35,3)*z[4] - n<T>(579,22)*
    z[2];
    z[159]=z[48]*z[159];
    z[160]= - z[7] - z[6];
    z[160]=z[73]*z[160];
    z[161]=z[7] + z[9];
    z[161]=z[74]*z[161];
    z[162]= - z[8] - z[11];
    z[162]=z[75]*z[162];
    z[163]=z[10] + z[11];
    z[163]=z[76]*z[163];

    r += n<T>(2,495)*z[50] + n<T>(169,264)*z[52] - n<T>(1865,1584)*z[54] - n<T>(2837,396)*z[55]
     - n<T>(49765,1296)*z[56]
     - n<T>(1,6)*z[59]
     + n<T>(89711,14256)*
      z[60] - n<T>(20065,198)*z[61] + n<T>(5263,792)*z[63] + n<T>(257,144)*z[64]
     +  n<T>(2513,528)*z[65] + n<T>(579,88)*z[66] + n<T>(41,88)*z[67] - n<T>(5,22)*z[68]
       - n<T>(21,11)*z[69] + z[70] + n<T>(779659,28512)*z[72] + n<T>(3,88)*z[78]
     +  n<T>(7,792)*z[82] - n<T>(1,88)*z[84] + n<T>(19,396)*z[85] - n<T>(67,396)*z[88]
     +  n<T>(25,264)*z[90] - n<T>(14,33)*z[91] - n<T>(13,132)*z[92] + n<T>(17,198)*z[94]
       - n<T>(15,88)*z[96] + n<T>(1,132)*z[99] + n<T>(7,1584)*z[100] + n<T>(5,264)*
      z[101] + n<T>(23,396)*z[102] + n<T>(5,528)*z[103] - n<T>(1,176)*z[105] - n<T>(2525,528)*z[107]
     + n<T>(259,176)*z[108]
     + n<T>(3,22)*z[109] - n<T>(2521,176)*
      z[110] + n<T>(16921,1584)*z[111] + n<T>(1141,1584)*z[112] + n<T>(727,396)*
      z[113] + n<T>(917,1584)*z[114] - n<T>(37915,4752)*z[115] - n<T>(14653,1584)*
      z[116] + n<T>(1535,432)*z[117] + n<T>(3293,4752)*z[118] - n<T>(146,99)*z[119]
       - n<T>(31,99)*z[120] + n<T>(71,198)*z[121] + n<T>(3,44)*z[123] + n<T>(73,264)*
      z[124] + n<T>(16,33)*z[126] + n<T>(1,594)*z[127] + n<T>(1,3)*z[128] + n<T>(5,198)
      *z[129];
      r+=  + z[130] + z[131] + n<T>(1,11)*z[132] + n<T>(1,2)*z[133] + z[134]
       + z[135] + z[136] + n<T>(1,198)*z[137] + 3*z[138] + z[139] + z[140]
       + n<T>(10,99)*z[141] + z[142] - n<T>(1,66)*z[143] + n<T>(1,44)*z[144] - n<T>(1,22)*z[145]
     + z[146]
     + n<T>(1,8)*z[147] - n<T>(23,132)*z[148]
     + n<T>(1,99)*
      z[149] + z[150] - n<T>(4,33)*z[151] + n<T>(1,9)*z[152] + n<T>(1,33)*z[153]
     +  n<T>(27,44)*z[154] + n<T>(23,264)*z[155] + z[156] + n<T>(1,1188)*z[157] + n<T>(1,18)*z[158]
     + n<T>(1,4)*z[159]
     + n<T>(7,198)*z[160]
     + n<T>(34,99)*z[161]
     + n<T>(7,396)*z[162]
     + n<T>(5,132)*z[163];
 
    return r;
}

template std::complex<double> qqb_2lha_tf984(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf984(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
