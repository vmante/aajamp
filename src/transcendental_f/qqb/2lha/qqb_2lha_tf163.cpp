#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf163(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[78];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[12];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[5];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[17];
    z[23]=c[19];
    z[24]=c[23];
    z[25]=c[25];
    z[26]=c[26];
    z[27]=c[28];
    z[28]=c[31];
    z[29]=e[3];
    z[30]=e[10];
    z[31]=e[6];
    z[32]=f[2];
    z[33]=f[3];
    z[34]=f[4];
    z[35]=f[5];
    z[36]=f[10];
    z[37]=f[11];
    z[38]=f[12];
    z[39]=f[13];
    z[40]=f[14];
    z[41]=f[16];
    z[42]=f[17];
    z[43]=f[21];
    z[44]=f[30];
    z[45]=f[31];
    z[46]=f[32];
    z[47]=f[35];
    z[48]=f[36];
    z[49]=f[37];
    z[50]=f[39];
    z[51]=f[41];
    z[52]=f[43];
    z[53]=f[68];
    z[54]=3*z[5];
    z[55]=3*z[2];
    z[56]=z[54] - z[55];
    z[57]=z[1]*i;
    z[58]=17*z[57];
    z[59]= - n<T>(17,2)*z[8] + z[58] - z[56];
    z[59]=z[8]*z[59];
    z[60]=n<T>(3,2)*z[2];
    z[61]=z[60] + n<T>(35,2)*z[57] - 11*z[5];
    z[61]=n<T>(1,8)*z[7] + n<T>(1,4)*z[61] - 2*z[8];
    z[61]=z[7]*z[61];
    z[62]=z[57] - z[8];
    z[63]=z[62] + z[6];
    z[56]=n<T>(35,4)*z[7] + z[56] + n<T>(11,4)*z[63];
    z[56]=z[6]*z[56];
    z[63]= - n<T>(3,4)*z[57] + z[5];
    z[63]=z[5]*z[63];
    z[64]=z[57] - z[5];
    z[65]= - n<T>(1,2)*z[2] - z[64];
    z[65]=z[2]*z[65];
    z[56]=n<T>(1,4)*z[56] + z[61] + n<T>(1,8)*z[59] + z[63] + n<T>(3,8)*z[65];
    z[56]=z[6]*z[56];
    z[59]=n<T>(7,8)*z[8];
    z[61]=n<T>(27,8)*z[6];
    z[63]= - n<T>(7,4)*z[9] - z[61] + n<T>(17,4)*z[7] + z[59] + n<T>(1,8)*z[57] - 
    z[5];
    z[65]=n<T>(1,2)*z[9];
    z[63]=z[63]*z[65];
    z[66]= - z[57] + n<T>(1,2)*z[8];
    z[59]=z[66]*z[59];
    z[66]=3*z[7];
    z[67]=n<T>(17,4)*z[62] + z[66];
    z[67]=z[7]*z[67];
    z[68]=n<T>(1,2)*z[6];
    z[69]= - z[68] - z[62];
    z[61]=z[69]*z[61];
    z[69]= - z[57] + n<T>(1,2)*z[5];
    z[69]=z[69]*z[5];
    z[59]=z[63] + z[61] + z[67] - z[69] + z[59];
    z[59]=z[9]*z[59];
    z[58]= - n<T>(31,2)*z[2] + z[58] - z[5];
    z[58]=z[58]*z[60];
    z[60]=n<T>(5,2)*z[8] + n<T>(45,2)*z[2] - 25*z[57] + n<T>(3,2)*z[5];
    z[60]=z[8]*z[60];
    z[61]=z[8] - 21*z[2] + 29*z[57] - z[5];
    z[61]=n<T>(1,2)*z[61] - z[7];
    z[61]=z[7]*z[61];
    z[58]=z[61] + z[60] + 11*z[69] + z[58];
    z[58]=z[7]*z[58];
    z[60]=5*z[6];
    z[61]= - z[57]*z[60];
    z[63]=z[57]*z[5];
    z[67]=z[63] + z[19];
    z[61]=z[61] + 7*z[67];
    z[61]=z[11]*z[61];
    z[60]=7*z[64] + z[60];
    z[60]=z[15]*z[60];
    z[58]= - z[46] + z[60] + z[58] + z[61];
    z[60]=z[65] + z[64];
    z[61]=3*z[9];
    z[60]=z[60]*z[61];
    z[61]=z[61] - z[64];
    z[61]=n<T>(1,2)*z[61] - 2*z[3];
    z[61]=z[10]*z[61];
    z[65]= - 2*z[64] - z[3];
    z[65]=z[3]*z[65];
    z[60]=z[61] + z[65] + z[69] + z[60];
    z[60]=z[10]*z[60];
    z[61]=3*z[57];
    z[65]=2*z[5];
    z[67]=z[61] - z[65];
    z[67]=z[67]*z[65];
    z[54]=8*z[2] - 16*z[57] - z[54];
    z[54]=z[2]*z[54];
    z[54]=z[67] + z[54];
    z[54]=z[54]*z[55];
    z[55]=2*z[57];
    z[67]=z[55] - z[5];
    z[67]=z[67]*z[5];
    z[69]=23*z[57] + 4*z[5];
    z[69]=2*z[69] - 7*z[2];
    z[69]=z[2]*z[69];
    z[65]=z[65] - 5*z[2];
    z[65]=z[8]*z[65];
    z[65]=4*z[65] - 8*z[67] + z[69];
    z[65]=z[8]*z[65];
    z[69]=z[57] - z[2];
    z[70]= - z[5] - z[69];
    z[70]=z[2]*z[70];
    z[63]=z[63] + z[70];
    z[70]= - 4*z[64] - z[9];
    z[70]=z[9]*z[70];
    z[69]= - 3*z[69] - 2*z[9];
    z[69]=z[3]*z[69];
    z[63]=z[69] + 6*z[63] + z[70];
    z[63]=z[3]*z[63];
    z[69]=3*z[8];
    z[70]=z[57] + z[5];
    z[70]= - 2*z[70] + z[8];
    z[70]=z[70]*z[69];
    z[71]=z[55] + z[5];
    z[71]=2*z[71] - z[2];
    z[71]=z[2]*z[71];
    z[67]=z[70] - z[67] + 2*z[71];
    z[70]= - z[57]*z[66];
    z[61]=z[3]*z[61];
    z[61]=z[61] + 2*z[67] + z[70];
    z[67]=4*z[2];
    z[69]=z[69] + z[5] - z[67];
    z[66]=4*z[69] + z[66] - 3*z[3];
    z[66]=z[4]*z[66];
    z[61]=2*z[61] + z[66];
    z[61]=z[4]*z[61];
    z[66]= - z[9] - z[3];
    z[66]=z[29]*z[66];
    z[69]=z[3] + z[10];
    z[69]=z[30]*z[69];
    z[66]= - z[36] + z[66] + z[69];
    z[68]=z[68] + n<T>(3,2)*z[7] + z[8] - z[2] - z[64];
    z[68]=z[16]*z[68];
    z[69]= - z[8] - z[7];
    z[57]=z[57]*z[69];
    z[57]= - z[19] + z[57];
    z[57]=z[12]*z[57];
    z[69]=z[23] + z[33] + z[32];
    z[70]=z[37] + z[35];
    z[71]=z[38] + z[34];
    z[72]=z[41] + z[21];
    z[73]=z[49] - z[40];
    z[55]=z[55] - z[2];
    z[74]=z[5] - z[55];
    z[74]=z[14]*z[74];
    z[74]= - z[43] + z[74] + z[51];
    z[75]=z[4] - z[55];
    z[75]=z[13]*z[75];
    z[75]=z[75] + z[39];
    z[76]=96*z[26] + 48*z[24] + n<T>(3,2)*z[22] - n<T>(23,48)*z[20];
    z[76]=i*z[76];
    z[64]=z[64]*npow(z[5],2);
    z[67]= - n<T>(2,3)*z[10] - n<T>(4,3)*z[4] + z[3] + n<T>(1,12)*z[9] + n<T>(13,12)*
    z[6] - n<T>(53,24)*z[7] - n<T>(5,4)*z[8] - n<T>(13,16)*z[5] - z[67];
    z[67]=z[19]*z[67];
    z[55]=z[8] - z[55];
    z[55]=z[17]*z[55];
    z[62]= - z[7] + z[62];
    z[62]=z[18]*z[62];
    z[77]= - z[7] - z[9];
    z[77]=z[31]*z[77];

    r +=  - 24*z[25] - 48*z[27] - n<T>(21,8)*z[28] + 12*z[42] - n<T>(17,4)*
      z[44] - n<T>(23,16)*z[45] + n<T>(13,8)*z[47] - n<T>(7,16)*z[48] - n<T>(27,16)*
      z[50] + n<T>(3,8)*z[52] + z[53] + z[54] + 26*z[55] + z[56] + z[57] + 
      n<T>(1,4)*z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + n<T>(7,8)*z[64]
       + z[65] + 2*z[66] + z[67] + n<T>(3,4)*z[68] + 4*z[69] + 8*z[70] + n<T>(1,2)*z[71]
     + n<T>(3,2)*z[72] - 3*z[73]
     + 6*z[74]
     + 16*z[75]
     + z[76]
     + n<T>(7,4)*z[77];
 
    return r;
}

template std::complex<double> qqb_2lha_tf163(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf163(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
