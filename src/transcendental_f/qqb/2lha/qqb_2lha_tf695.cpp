#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf695(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[20];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[13];
    z[3]=c[20];
    z[4]=c[23];
    z[5]=f[22];
    z[6]=f[42];
    z[7]=f[80];
    z[8]=g[24];
    z[9]=g[27];
    z[10]=g[38];
    z[11]=g[40];
    z[12]=g[50];
    z[13]=g[60];
    z[14]=g[62];
    z[15]=g[63];
    z[16]= - z[11] + z[10] + z[9] - z[8];
    z[17]= - n<T>(53,432)*z[1] + 9*z[2];
    z[17]=n<T>(1,5)*z[17] - n<T>(3,80)*z[4] + n<T>(3,4)*z[3];
    z[17]=i*z[17];
    z[18]=z[7] - z[6];
    z[19]=z[14] - z[12];

    r +=  - n<T>(1,4)*z[5] + n<T>(9,32)*z[13] + n<T>(9,8)*z[15] - n<T>(27,32)*z[16]
     +  z[17] - n<T>(1,8)*z[18] + n<T>(9,16)*z[19];
 
    return r;
}

template std::complex<double> qqb_2lha_tf695(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf695(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
