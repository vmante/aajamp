#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1274(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[126];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[11];
    z[27]=e[12];
    z[28]=e[13];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[19];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[31];
    z[38]=e[10];
    z[39]=e[3];
    z[40]=e[6];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[10];
    z[50]=f[11];
    z[51]=f[12];
    z[52]=f[14];
    z[53]=f[15];
    z[54]=f[16];
    z[55]=f[17];
    z[56]=f[18];
    z[57]=f[23];
    z[58]=f[30];
    z[59]=f[31];
    z[60]=f[32];
    z[61]=f[33];
    z[62]=f[35];
    z[63]=f[36];
    z[64]=f[37];
    z[65]=f[39];
    z[66]=f[43];
    z[67]=f[50];
    z[68]=f[51];
    z[69]=f[52];
    z[70]=f[53];
    z[71]=f[54];
    z[72]=f[55];
    z[73]=f[56];
    z[74]=f[58];
    z[75]=f[72];
    z[76]=f[73];
    z[77]=f[74];
    z[78]=f[75];
    z[79]=6*z[17];
    z[80]=n<T>(77,3)*z[16];
    z[81]=z[79] + z[80];
    z[82]=3*z[11];
    z[83]=6*z[2];
    z[84]=3*z[8];
    z[85]=n<T>(7,2)*z[10];
    z[86]=n<T>(9,2)*z[4];
    z[87]= - z[84] + 4*z[9] - 10*z[7] + z[83] + z[85] - z[86] + z[82] - 
    z[81];
    z[87]=z[8]*z[87];
    z[88]=n<T>(17,6)*z[10];
    z[89]=2*z[2];
    z[90]=n<T>(29,6)*z[9] + z[88] + z[89];
    z[91]=n<T>(83,12)*z[12];
    z[92]=5*z[7];
    z[93]=5*z[11];
    z[94]=n<T>(97,24)*z[3] + 5*z[8] - z[92] + z[86] + z[91] - z[93] - z[90];
    z[94]=z[3]*z[94];
    z[95]=z[93] + z[86];
    z[96]=z[95] + n<T>(101,12)*z[10];
    z[97]=n<T>(28,3)*z[7];
    z[98]=69*z[15] + n<T>(83,3)*z[12];
    z[83]=n<T>(73,8)*z[5] - n<T>(121,6)*z[6] + n<T>(95,12)*z[3] - n<T>(3,2)*z[8]
     - n<T>(77,12)*z[9]
     - z[97]
     + z[83]
     + n<T>(1,4)*z[98] - z[96];
    z[83]=z[5]*z[83];
    z[98]=n<T>(47,6)*z[2];
    z[80]=n<T>(113,12)*z[7] + z[98] + 12*z[10] - z[80] + z[86];
    z[80]=z[7]*z[80];
    z[86]=6*z[26];
    z[99]=n<T>(77,3)*z[25];
    z[100]=z[86] - z[99];
    z[101]=3*z[2];
    z[102]=3*z[4] - z[89];
    z[102]=z[102]*z[101];
    z[103]=z[89] + n<T>(45,4)*z[10];
    z[79]= - n<T>(23,4)*z[9] - n<T>(61,12)*z[4] - z[79] + n<T>(23,6)*z[11] + z[103];
    z[79]=z[9]*z[79];
    z[104]= - z[98] + n<T>(7,4)*z[3];
    z[105]=n<T>(69,4)*z[15];
    z[106]=n<T>(71,8)*z[6] + n<T>(193,6)*z[8] + n<T>(5,4)*z[9] - n<T>(31,2)*z[10]
     + n<T>(3,4)
   *z[4] + z[105] + z[11] + z[104];
    z[106]=z[6]*z[106];
    z[107]=z[19] + z[24];
    z[108]=2*z[28];
    z[109]= - z[108] + n<T>(20,3)*z[23];
    z[110]=9*z[21];
    z[111]= - z[110] + n<T>(13,6)*z[27];
    z[112]=n<T>(13,6)*z[13];
    z[113]=z[11]*z[112];
    z[112]= - z[112] + 9*z[14];
    z[114]= - n<T>(41,12)*z[4] - z[112];
    z[114]=z[4]*z[114];
    z[115]= - z[14] + z[4];
    z[115]=9*z[115] - n<T>(59,24)*z[10];
    z[115]=z[10]*z[115];
    z[79]=z[83] + z[106] + z[94] + z[87] + z[79] + z[80] + z[102] + 
    z[115] + z[114] + z[113] - 18*z[18] - n<T>(83,12)*z[20] + n<T>(69,4)*z[22]
    - z[100] + z[111] - z[109] - 6*z[107];
    z[79]=z[1]*z[79];
    z[80]=5*z[35] + z[33];
    z[79]=z[79] + 6*z[80] - n<T>(85,72)*z[30];
    z[79]=i*z[79];
    z[80]=n<T>(1,2)*z[10];
    z[83]=2*z[11];
    z[87]= - n<T>(119,6)*z[7] - z[83] + z[80] + z[98];
    z[87]=z[7]*z[87];
    z[94]=2*z[9];
    z[102]=z[80] + z[94];
    z[106]=n<T>(1,2)*z[7];
    z[113]=z[106] + z[83];
    z[114]=n<T>(31,8)*z[3] - z[113] - z[102];
    z[114]=z[3]*z[114];
    z[115]=n<T>(47,3)*z[2];
    z[116]=z[93] - n<T>(1,2)*z[4];
    z[116]=3*z[116] - 31*z[10];
    z[116]=n<T>(703,36)*z[6] + n<T>(5,4)*z[3] - n<T>(33,4)*z[8] + n<T>(5,8)*z[9]
     - n<T>(73,6)
   *z[7] + n<T>(1,4)*z[116] - z[115];
    z[116]=z[6]*z[116];
    z[117]=npow(z[2],2);
    z[118]=n<T>(47,12)*z[117];
    z[119]=4*z[38];
    z[120]=npow(z[11],2);
    z[121]=npow(z[10],2);
    z[122]= - n<T>(23,4)*z[22] - 3*z[28];
    z[123]=4*z[11] - n<T>(3,8)*z[4];
    z[123]=z[4]*z[123];
    z[124]= - n<T>(3,8)*z[9] + z[83] - n<T>(13,4)*z[4];
    z[124]=z[9]*z[124];
    z[125]= - n<T>(193,12)*z[8] + n<T>(3,2)*z[7] + 15*z[10] + z[98];
    z[125]=z[8]*z[125];
    z[87]=z[116] + z[114] + z[125] + z[124] + z[87] - z[118] - n<T>(23,4)*
    z[121] + z[123] - n<T>(1,4)*z[120] + n<T>(485,24)*z[29] + n<T>(160,3)*z[23] + 3*
    z[122] + z[119];
    z[87]=z[6]*z[87];
    z[93]=n<T>(185,12)*z[6] - n<T>(47,6)*z[8] - z[97] - z[93] - z[104] + z[102];
    z[93]=z[6]*z[93];
    z[97]=n<T>(77,24)*z[9];
    z[102]=n<T>(3,4)*z[8];
    z[96]= - n<T>(211,24)*z[5] + n<T>(35,24)*z[6] - n<T>(89,12)*z[3] + z[102] + 
    z[97] + n<T>(14,3)*z[7] + n<T>(1,2)*z[96] - z[101];
    z[96]=z[5]*z[96];
    z[83]=z[97] - z[83] - z[103];
    z[83]=z[9]*z[83];
    z[97]=n<T>(3,2)*z[4];
    z[103]= - z[97] + z[2];
    z[102]=z[102] + z[94] + 3*z[103] + n<T>(47,6)*z[7];
    z[102]=z[8]*z[102];
    z[90]= - n<T>(101,24)*z[3] - 2*z[8] + z[113] + z[90];
    z[90]=z[3]*z[90];
    z[103]=npow(z[4],2);
    z[104]=n<T>(9,4)*z[103];
    z[113]= - z[104] + n<T>(3,2)*z[117];
    z[114]=n<T>(3,2)*z[120];
    z[116]= - 69*z[22] + n<T>(83,3)*z[20];
    z[122]= - z[10] - z[115];
    z[122]=n<T>(1,2)*z[122] + n<T>(17,3)*z[7];
    z[122]=z[7]*z[122];
    z[83]=z[96] + z[93] + z[90] + z[102] + z[83] + z[122] + n<T>(101,24)*
    z[121] + z[114] + n<T>(191,36)*z[29] + n<T>(1,4)*z[116] + 3*z[19] - z[113]
    + z[109];
    z[83]=z[5]*z[83];
    z[82]= - n<T>(5,2)*z[8] - z[94] + z[82] + z[89];
    z[82]=z[8]*z[82];
    z[90]=9*z[4];
    z[93]=n<T>(5,2)*z[7];
    z[96]= - z[93] + z[90] + z[10];
    z[96]=z[96]*z[106];
    z[88]= - z[8] - z[88] - z[95];
    z[88]=n<T>(193,12)*z[3] - n<T>(29,12)*z[9] - z[93] - 4*z[2] + n<T>(1,2)*z[88];
    z[88]=z[3]*z[88];
    z[93]=n<T>(77,2)*z[38] + 49*z[39] + n<T>(83,4)*z[20];
    z[95]=z[11] + z[2];
    z[95]=2*z[95] - n<T>(53,6)*z[9];
    z[95]=z[9]*z[95];
    z[82]=z[88] + z[82] + z[95] + z[96] - z[117] - n<T>(59,6)*z[121] - 
    z[104] - n<T>(1,2)*z[120] + n<T>(133,24)*z[29] + n<T>(1,3)*z[93] + z[23];
    z[82]=z[3]*z[82];
    z[88]=n<T>(191,36)*z[7] + z[98] + z[85] + z[11] - n<T>(9,4)*z[4];
    z[88]=z[7]*z[88];
    z[93]=4*z[40];
    z[88]=z[88] + z[118] + n<T>(11,2)*z[121] + z[120] - n<T>(67,144)*z[29] + n<T>(125,3)*z[23]
     + z[93] - z[99]
     + z[108] - z[104];
    z[88]=z[7]*z[88];
    z[95]= - z[11] + z[97];
    z[85]=3*z[95] - z[85];
    z[84]=z[84] + z[9] + n<T>(107,6)*z[7] + n<T>(1,2)*z[85] - z[101];
    z[84]=z[8]*z[84];
    z[85]= - 23*z[10] - z[115];
    z[85]=n<T>(1,2)*z[85] + z[92];
    z[85]=z[7]*z[85];
    z[92]= - z[2] - z[9];
    z[92]=z[92]*z[94];
    z[94]=z[119] + n<T>(47,3)*z[23];
    z[84]=z[84] + z[92] + z[85] - n<T>(7,4)*z[121] - z[114] - n<T>(35,9)*z[29]
    + 3*z[24] - z[113] + z[100] + z[94];
    z[84]=z[8]*z[84];
    z[85]= - z[11] + n<T>(61,2)*z[4];
    z[85]=n<T>(1,3)*z[85] + n<T>(45,2)*z[10];
    z[85]= - n<T>(37,12)*z[9] + n<T>(1,4)*z[85] + z[89];
    z[85]=z[9]*z[85];
    z[89]= - 11*z[11] + n<T>(61,4)*z[4];
    z[89]=z[4]*z[89];
    z[85]=z[85] + z[117] + n<T>(45,8)*z[121] + n<T>(1,6)*z[89] + 2*z[120] - n<T>(77,12)*z[29]
     + n<T>(77,6)*z[38] - n<T>(25,6)*z[41]
     + z[86];
    z[85]=z[9]*z[85];
    z[81]=z[91] + z[105] - z[81] - z[112];
    z[81]=z[29]*z[81];
    z[86]= - 25*z[41] - 13*z[27];
    z[86]=n<T>(1,3)*z[86] - 9*z[28];
    z[86]= - n<T>(7,4)*z[120] - n<T>(151,72)*z[29] + 8*z[23] + n<T>(1,2)*z[86] - 
    z[119];
    z[86]=z[11]*z[86];
    z[89]= - z[90] + n<T>(43,3)*z[10];
    z[80]=z[89]*z[80];
    z[80]=z[80] - n<T>(365,72)*z[29] - z[23] + z[110] + z[93] + n<T>(49,3)*z[39]
   ;
    z[80]=z[10]*z[80];
    z[89]=z[117] + 3*z[18] + z[107];
    z[90]=z[103] - z[121];
    z[89]=4*z[29] - z[94] - n<T>(9,2)*z[90] + 3*z[89];
    z[89]=z[2]*z[89];
    z[90]= - 13*z[11] + 41*z[4];
    z[90]=z[4]*z[90];
    z[90]=n<T>(1,12)*z[90] - n<T>(59,24)*z[29] + 9*z[18] - z[111];
    z[90]=z[4]*z[90];
    z[91]= - z[75] - z[56] + z[34] - z[47];
    z[92]=z[65] + z[43] - z[48];
    z[93]=z[45] - z[52] + z[55] - z[64];
    z[94]=z[69] + z[73];
    z[95]=z[42] + z[70];
    z[96]=z[32] + z[71];
    z[97]=z[78] + z[77];

    r += n<T>(43,3)*z[31] - 15*z[36] + n<T>(47,3)*z[37] + n<T>(31,12)*z[44] + n<T>(149,24)*z[46]
     - n<T>(1,3)*z[49]
     + n<T>(3,4)*z[50] - n<T>(17,24)*z[51] - n<T>(15,4)*
      z[53] + n<T>(17,8)*z[54] - 2*z[57] - n<T>(23,2)*z[58] - n<T>(287,12)*z[59]
     - 
      n<T>(63,4)*z[60] - n<T>(158,3)*z[61] - n<T>(28,3)*z[62] + n<T>(7,4)*z[63] + n<T>(47,6)
      *z[66] - n<T>(11,6)*z[67] - n<T>(3,8)*z[68] - n<T>(61,24)*z[72] + n<T>(1,8)*z[74]
       + z[76] + z[79] + z[80] + z[81] + z[82] + z[83] + z[84] + z[85]
       + z[86] + z[87] + z[88] + z[89] + z[90] - 3*z[91] - n<T>(9,2)*z[92]
       + n<T>(9,4)*z[93] - n<T>(3,2)*z[94] + 9*z[95] - 4*z[96] + n<T>(1,4)*z[97];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1274(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1274(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
