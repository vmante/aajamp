#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf786(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[132];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[61];
    z[3]=f[77];
    z[4]=f[79];
    z[5]=c[11];
    z[6]=d[0];
    z[7]=d[1];
    z[8]=d[2];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[7];
    z[13]=d[8];
    z[14]=d[9];
    z[15]=c[12];
    z[16]=c[13];
    z[17]=c[20];
    z[18]=c[23];
    z[19]=c[32];
    z[20]=c[33];
    z[21]=c[35];
    z[22]=c[36];
    z[23]=c[37];
    z[24]=c[38];
    z[25]=c[39];
    z[26]=c[40];
    z[27]=c[43];
    z[28]=c[44];
    z[29]=c[47];
    z[30]=c[48];
    z[31]=c[49];
    z[32]=c[50];
    z[33]=c[55];
    z[34]=c[56];
    z[35]=c[57];
    z[36]=c[59];
    z[37]=c[81];
    z[38]=c[84];
    z[39]=f[22];
    z[40]=f[24];
    z[41]=f[42];
    z[42]=f[44];
    z[43]=f[62];
    z[44]=f[69];
    z[45]=f[70];
    z[46]=f[80];
    z[47]=f[81];
    z[48]=d[3];
    z[49]=g[24];
    z[50]=g[27];
    z[51]=g[38];
    z[52]=g[40];
    z[53]=g[44];
    z[54]=g[48];
    z[55]=g[50];
    z[56]=g[79];
    z[57]=g[82];
    z[58]=g[90];
    z[59]=g[92];
    z[60]=g[99];
    z[61]=g[101];
    z[62]=g[110];
    z[63]=g[112];
    z[64]=g[113];
    z[65]=g[128];
    z[66]=g[131];
    z[67]=g[138];
    z[68]=g[140];
    z[69]=g[145];
    z[70]=g[147];
    z[71]=g[170];
    z[72]=g[173];
    z[73]=g[181];
    z[74]=g[183];
    z[75]=g[189];
    z[76]=g[191];
    z[77]=g[198];
    z[78]=g[200];
    z[79]=g[201];
    z[80]=g[212];
    z[81]=g[214];
    z[82]=g[218];
    z[83]=g[220];
    z[84]=g[224];
    z[85]=g[232];
    z[86]=g[233];
    z[87]=g[234];
    z[88]=g[243];
    z[89]=g[245];
    z[90]=g[249];
    z[91]=g[251];
    z[92]=g[254];
    z[93]=g[270];
    z[94]=g[272];
    z[95]=g[276];
    z[96]=g[278];
    z[97]=g[281];
    z[98]=g[288];
    z[99]=g[289];
    z[100]=g[290];
    z[101]=g[292];
    z[102]=g[294];
    z[103]=g[298];
    z[104]=g[301];
    z[105]=g[302];
    z[106]=g[349];
    z[107]=g[352];
    z[108]=g[357];
    z[109]=g[358];
    z[110]= - z[17] + n<T>(1,20)*z[18];
    z[111]= - n<T>(3,5)*z[16] + n<T>(1,4)*z[110];
    z[112]=n<T>(379,6480)*z[5];
    z[113]= - 17*z[111] + z[112];
    z[113]=z[8]*z[113];
    z[114]= - n<T>(1,4)*z[18] + 5*z[17];
    z[114]=n<T>(1,12)*z[114] + z[16];
    z[114]=7*z[114] - n<T>(169,1296)*z[5];
    z[114]=z[11]*z[114];
    z[115]=n<T>(271,2160)*z[5] + z[111];
    z[115]=z[10]*z[115];
    z[116]= - n<T>(409,2160)*z[5] + z[111];
    z[116]=z[6]*z[116];
    z[113]=z[35] + z[114] + z[113] + z[115] + z[116];
    z[114]=n<T>(1,12)*z[110] - n<T>(1,5)*z[16];
    z[112]=49*z[114] + z[112];
    z[112]=z[14]*z[112];
    z[114]=n<T>(1,24)*z[4];
    z[115]=z[2] - z[114] - n<T>(2,3)*z[3];
    z[115]=z[1]*z[115];
    z[116]= - n<T>(161,480)*z[36] + 4*z[38] - n<T>(65,18)*z[37];
    z[117]= - n<T>(1,5)*z[18] + 4*z[17];
    z[117]=n<T>(1,3)*z[117] + n<T>(16,5)*z[16];
    z[117]=z[9]*z[117];
    z[118]=n<T>(23,36)*z[12] + n<T>(1,5)*z[9];
    z[118]=z[5]*z[118];
    z[111]=7*z[111] - n<T>(1,720)*z[5];
    z[111]=z[7]*z[111];
    z[110]= - n<T>(1,6)*z[110] + n<T>(2,5)*z[16];
    z[110]=7*z[110] - n<T>(89,1080)*z[5];
    z[110]=z[13]*z[110];
    z[110]=z[110] + z[111] + z[112] + n<T>(1,9)*z[118] + z[117] + n<T>(7,54)*
    z[20] + 8*z[23] - n<T>(107,27)*z[24] - n<T>(109,486)*z[25] + n<T>(10,3)*z[28]
     + 
   n<T>(881,4320)*z[29] + n<T>(27,10)*z[31] - 4*z[32] + n<T>(9,8)*z[34] + n<T>(1,3)*
    z[116] + z[115] + n<T>(1,2)*z[113];
    z[110]=i*z[110];
    z[111]= - z[47] + n<T>(11,8)*z[46];
    z[112]=n<T>(49,24)*z[45];
    z[113]= - n<T>(41,12)*z[40] + z[111] + z[112];
    z[115]= - n<T>(49,24)*z[44] - z[113];
    z[116]=n<T>(1,3)*z[41];
    z[117]=n<T>(19,48)*z[42];
    z[118]=n<T>(1,6)*z[3];
    z[119]=n<T>(8,3)*z[15];
    z[114]= - n<T>(41,48)*z[2] - z[114] + z[119] - n<T>(107,48)*z[43] - z[118]
    + z[117] + n<T>(1,3)*z[39] + n<T>(1,2)*z[115] + z[116];
    z[114]=z[6]*z[114];
    z[115]=n<T>(1,3)*z[3];
    z[120]=z[39] - z[44];
    z[121]=3*z[40] - n<T>(11,2)*z[120];
    z[122]= - n<T>(49,6)*z[41] - z[121];
    z[122]= - n<T>(25,24)*z[2] + n<T>(49,24)*z[4] + n<T>(19,6)*z[15] + n<T>(11,8)*z[43]
    + n<T>(1,4)*z[122] - z[115];
    z[122]=z[7]*z[122];
    z[123]=z[103] + z[107];
    z[124]=z[99] + z[108];
    z[125]=z[98] + z[104];
    z[122]= - z[64] - z[122] - n<T>(7,2)*z[96] + n<T>(11,12)*z[97] - z[100] - n<T>(5,4)*z[101]
     - n<T>(3,4)*z[102]
     - z[105]
     + n<T>(1,24)*z[106]
     + z[109] - n<T>(1,4)*
    z[125] - n<T>(1,2)*z[124] + n<T>(3,8)*z[123] + z[79] + z[87];
    z[123]=n<T>(3,16)*z[2];
    z[121]=n<T>(65,6)*z[41] + z[121];
    z[118]= - z[123] - n<T>(1,16)*z[4] + n<T>(43,36)*z[15] - n<T>(65,48)*z[43] - 
    z[118] + n<T>(1,8)*z[121] + n<T>(2,3)*z[42];
    z[118]=z[13]*z[118];
    z[113]=n<T>(25,8)*z[43] + z[115] - n<T>(67,24)*z[42] + n<T>(11,8)*z[44] + z[113]
   ;
    z[113]=z[123] + n<T>(17,24)*z[4] + n<T>(1,2)*z[113] - z[119];
    z[113]=z[8]*z[113];
    z[111]=z[111] - z[112];
    z[111]= - n<T>(49,48)*z[41] + n<T>(1,3)*z[40] + n<T>(1,2)*z[111];
    z[112]=n<T>(7,24)*z[2] - n<T>(31,48)*z[4] - n<T>(19,12)*z[15] + n<T>(37,24)*z[43]
     - 
    z[115] - z[117] + n<T>(49,48)*z[39] - n<T>(1,3)*z[44] + z[111];
    z[112]=z[10]*z[112];
    z[117]= - z[48] + 2*z[9];
    z[121]= - n<T>(23,8)*z[12] - z[117];
    z[121]=z[4]*z[121];
    z[117]=z[2]*z[117];
    z[117]=z[70] + z[121] + z[117];
    z[121]=z[48] + z[12];
    z[115]=z[121]*z[115];
    z[111]=n<T>(1,24)*z[2] + n<T>(5,16)*z[4] + n<T>(17,36)*z[15] - n<T>(13,24)*z[43]
     + n<T>(1,16)*z[42] - n<T>(11,16)*z[39] - z[111];
    z[111]=z[11]*z[111];
    z[121]= - z[73] + z[51] - z[72];
    z[123]= - z[85] + z[62] - z[77];
    z[124]= - z[63] + z[78] + z[86] + z[94];
    z[125]=z[80] + z[83];
    z[126]=z[65] + z[68];
    z[127]=z[56] + z[59];
    z[128]=z[54] - z[75];
    z[129]=z[49] + z[52];
    z[130]=z[22] + z[30];
    z[119]= - z[119] - z[42] + z[43];
    z[119]=z[9]*z[119];
    z[119]=z[119] + z[91];
    z[131]= - n<T>(23,12)*z[15] + n<T>(2,3)*z[43] - n<T>(1,3)*z[120] + z[42];
    z[131]=z[12]*z[131];
    z[116]= - z[48]*z[116];
    z[120]=z[42] - z[41] - z[120];
    z[120]=n<T>(17,24)*z[2] + n<T>(73,36)*z[15] + n<T>(1,3)*z[120] - z[43];
    z[120]=z[14]*z[120];

    r += n<T>(3,10)*z[19] - n<T>(24,5)*z[21] + n<T>(10,3)*z[26] + z[27] + n<T>(1,10)*
      z[33] - n<T>(8,3)*z[50] + n<T>(1,48)*z[53] - n<T>(5,9)*z[55] - n<T>(7,6)*z[57]
     - 
      n<T>(19,8)*z[58] + n<T>(65,72)*z[60] - n<T>(23,24)*z[61] + n<T>(107,24)*z[66]
     - 
      z[67] + n<T>(17,72)*z[69] + z[71] + z[74] + n<T>(67,72)*z[76] - n<T>(65,24)*
      z[81] + n<T>(11,4)*z[82] - n<T>(1,9)*z[84] + n<T>(23,12)*z[88] + n<T>(19,24)*
      z[89] + 2*z[90] - n<T>(47,72)*z[92] + n<T>(7,4)*z[93] + z[95] + z[110] + 
      z[111] + z[112] + z[113] + z[114] + z[115] + z[116] + n<T>(1,3)*
      z[117] + z[118] + n<T>(2,3)*z[119] + z[120] - n<T>(29,12)*z[121] - n<T>(1,2)*
      z[122] + n<T>(1,8)*z[123] - n<T>(1,4)*z[124] - n<T>(17,24)*z[125] - n<T>(17,12)*
      z[126] - n<T>(5,3)*z[127] + n<T>(49,72)*z[128] - n<T>(11,8)*z[129] - 4*z[130]
       + z[131];
 
    return r;
}

template std::complex<double> qqb_2lha_tf786(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf786(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
