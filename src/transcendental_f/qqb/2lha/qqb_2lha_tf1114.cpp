#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1114(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[83];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[1];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[12];
    z[13]=d[17];
    z[14]=d[18];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[11];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[17];
    z[28]=c[19];
    z[29]=c[23];
    z[30]=c[25];
    z[31]=c[26];
    z[32]=c[28];
    z[33]=c[31];
    z[34]=e[3];
    z[35]=e[6];
    z[36]=f[2];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[6];
    z[41]=f[7];
    z[42]=f[11];
    z[43]=f[12];
    z[44]=f[13];
    z[45]=f[16];
    z[46]=f[17];
    z[47]=f[19];
    z[48]=f[23];
    z[49]=f[30];
    z[50]=f[31];
    z[51]=f[32];
    z[52]=f[33];
    z[53]=f[35];
    z[54]=f[36];
    z[55]=f[39];
    z[56]=f[43];
    z[57]=3*z[4];
    z[58]=z[1]*i;
    z[59]= - z[58] + z[57];
    z[60]=12*z[6];
    z[61]=3*z[2];
    z[59]=z[60] - z[61] + 2*z[59] - z[7];
    z[59]=z[6]*z[59];
    z[62]=5*z[7];
    z[63]=9*z[2];
    z[64]=5*z[58];
    z[65]=8*z[6] + z[63] + z[62] - z[64] - 9*z[4];
    z[65]=2*z[65] - 29*z[5];
    z[65]=z[5]*z[65];
    z[66]=3*z[58];
    z[67]=n<T>(1,2)*z[4];
    z[68]=z[66] - z[67];
    z[68]=z[68]*z[57];
    z[69]=6*z[4];
    z[70]= - 19*z[58] + z[69];
    z[70]=2*z[70] + 19*z[7];
    z[70]=z[7]*z[70];
    z[71]=z[58] - z[7];
    z[72]=n<T>(1,2)*z[2];
    z[73]=z[72] - z[4] + 4*z[71];
    z[73]=z[73]*z[61];
    z[59]=z[65] + 2*z[59] + z[73] + z[68] + z[70];
    z[59]=z[5]*z[59];
    z[65]=7*z[4];
    z[68]= - 79*z[58] + z[65];
    z[70]=9*z[5];
    z[73]=3*z[7];
    z[68]= - n<T>(53,9)*z[10] - n<T>(1,8)*z[8] + z[70] - 6*z[6] + n<T>(1,24)*z[68]
    + z[73];
    z[68]=z[10]*z[68];
    z[74]=z[67] - z[58];
    z[74]=z[74]*z[4];
    z[75]=2*z[58];
    z[76]= - z[75] + z[7];
    z[76]=z[76]*z[73];
    z[60]= - z[71]*z[60];
    z[77]=2*z[71] + z[5];
    z[70]=z[77]*z[70];
    z[77]=z[58] - z[4];
    z[78]= - n<T>(1,2)*z[8] - z[77];
    z[78]=z[8]*z[78];
    z[60]=z[68] + n<T>(1,4)*z[78] + z[70] + z[60] + n<T>(7,12)*z[74] + z[76];
    z[60]=z[10]*z[60];
    z[68]=6*z[2];
    z[70]= - n<T>(1,9)*z[9] + n<T>(5,12)*z[10] - n<T>(25,6)*z[8] + z[68] - z[73]
     + 
   n<T>(19,3)*z[58] - z[57];
    z[70]=z[9]*z[70];
    z[76]=z[73] + n<T>(11,6)*z[58] + 4*z[4];
    z[76]=z[4]*z[76];
    z[78]=n<T>(3,2)*z[2];
    z[79]=2*z[4];
    z[80]=z[78] - z[79] - z[7];
    z[80]=z[80]*z[61];
    z[68]=n<T>(11,3)*z[8] - z[68] - n<T>(25,3)*z[77] + 6*z[7];
    z[68]=z[8]*z[68];
    z[81]=5*z[4];
    z[82]= - z[10] - z[58] - z[81];
    z[82]=z[10]*z[82];
    z[68]=z[70] + n<T>(1,6)*z[82] + z[68] + z[80] + z[76];
    z[68]=z[9]*z[68];
    z[70]=z[75] - z[4];
    z[70]=z[70]*z[57];
    z[64]= - 11*z[7] + z[64] - z[57];
    z[64]=z[7]*z[64];
    z[64]=z[70] + z[64];
    z[70]=2*z[77];
    z[76]= - z[78] - z[70] + z[73];
    z[76]=z[76]*z[61];
    z[63]= - n<T>(17,2)*z[6] - z[63] - n<T>(13,2)*z[7] - n<T>(19,2)*z[58] - z[69];
    z[63]=z[6]*z[63];
    z[63]=z[63] + 2*z[64] + z[76];
    z[63]=z[6]*z[63];
    z[64]=n<T>(1,2)*z[7];
    z[69]=z[64] + z[58] - z[79];
    z[69]=z[69]*z[73];
    z[76]= - 2*z[8] - z[61] + n<T>(85,24)*z[77] + z[73];
    z[76]=z[8]*z[76];
    z[70]=z[72] - z[70] - z[7];
    z[61]=z[70]*z[61];
    z[61]=z[76] + z[61] - n<T>(103,12)*z[74] + z[69];
    z[61]=z[8]*z[61];
    z[69]= - z[66] + z[79];
    z[69]=z[69]*z[57];
    z[70]=7*z[7];
    z[72]=z[70] - 23*z[58] - z[4];
    z[72]=z[7]*z[72];
    z[74]=8*z[58];
    z[76]=z[74] + z[4];
    z[62]= - 12*z[2] + 3*z[76] + z[62];
    z[62]=z[2]*z[62];
    z[62]=z[62] + z[69] + z[72];
    z[62]=z[2]*z[62];
    z[67]=7*z[58] - z[67];
    z[67]=z[4]*z[67];
    z[69]= - z[64] + z[58] + z[4];
    z[72]=9*z[7];
    z[69]=z[69]*z[72];
    z[65]= - z[65] - z[72];
    z[65]=n<T>(1,2)*z[65] + 8*z[2];
    z[65]=z[3]*z[65];
    z[72]= - z[75] - z[4];
    z[72]=2*z[72] + z[2];
    z[72]=z[2]*z[72];
    z[65]=z[65] + 4*z[72] + z[67] + z[69];
    z[65]=z[3]*z[65];
    z[67]=z[9] + z[6];
    z[57]= - 10*z[2] - z[70] + 20*z[58] - z[57] + 3*z[67];
    z[57]=z[20]*z[57];
    z[66]= - z[73] + z[66] - z[81];
    z[64]=z[66]*z[64];
    z[66]=z[74] - n<T>(11,2)*z[4];
    z[66]=z[4]*z[66];
    z[64]=z[66] + z[64];
    z[64]=z[7]*z[64];
    z[66]=z[58]*z[7];
    z[66]=z[66] + z[24];
    z[67]=z[8]*z[58];
    z[67]=z[67] + z[66];
    z[67]=z[14]*z[67];
    z[69]= - z[8] + z[71];
    z[69]=z[23]*z[69];
    z[67]= - z[54] + z[67] + z[69] + z[48] - z[40];
    z[69]= - 47*z[58] + n<T>(137,3)*z[4];
    z[69]=z[69]*npow(z[4],2);
    z[70]= - z[10] - z[9];
    z[70]=z[34]*z[70];
    z[69]= - z[36] + z[69] + z[70];
    z[70]= - z[4] - z[5];
    z[70]=z[58]*z[70];
    z[70]= - z[24] + z[70];
    z[70]=z[12]*z[70];
    z[72]=z[5] - z[77];
    z[72]=z[18]*z[72];
    z[70]=z[51] + z[70] + z[72];
    z[72]=z[6]*z[58];
    z[66]=z[72] + z[66];
    z[66]=z[13]*z[66];
    z[71]=z[6] - z[71];
    z[71]=z[21]*z[71];
    z[66]=z[66] + z[71];
    z[71]= - z[10] + n<T>(19,3)*z[9];
    z[72]= - n<T>(41,6)*z[4] - z[71];
    z[58]=z[58]*z[72];
    z[58]= - n<T>(41,6)*z[24] + z[58];
    z[58]=z[11]*z[58];
    z[71]=n<T>(41,6)*z[77] - z[71];
    z[71]=z[17]*z[71];
    z[72]=z[56] - z[55];
    z[73]= - z[6] - z[10];
    z[73]=z[35]*z[73];
    z[73]=z[73] + z[49];
    z[74]=z[75] - z[2];
    z[75]= - z[3] + z[74];
    z[75]=z[15]*z[75];
    z[75]=z[75] + z[53];
    z[76]= - 42*z[31] - 21*z[29] + n<T>(3,4)*z[27] + n<T>(95,36)*z[25];
    z[76]=i*z[76];
    z[78]= - n<T>(73,8)*z[4] + 8*z[7];
    z[78]=n<T>(7,6)*z[3] + n<T>(7,18)*z[9] - n<T>(49,12)*z[10] + n<T>(233,36)*z[8]
     - n<T>(79,3)*z[5]
     + 5*z[6]
     + n<T>(1,3)*z[78]
     + 2*z[2];
    z[78]=z[24]*z[78];
    z[77]= - z[7] + z[77] + z[2];
    z[79]= - 35*z[5] + 9*z[77] - 26*z[6];
    z[79]=z[19]*z[79];
    z[77]= - n<T>(29,3)*z[9] + 6*z[77] - n<T>(47,3)*z[8];
    z[77]=z[22]*z[77];
    z[74]= - z[4] + z[74];
    z[74]=z[16]*z[74];

    r +=  - n<T>(301,24)*z[26] - n<T>(7,4)*z[28] + n<T>(21,2)*z[30] + 21*z[32] + n<T>(63,16)*z[33]
     - n<T>(7,2)*z[37]
     - n<T>(157,24)*z[38]
     - z[39]
     - 24*z[41]
     - n<T>(5,2)*z[42]
     + n<T>(1,24)*z[43] - n<T>(19,2)*z[44] - n<T>(1,8)*z[45] - n<T>(9,2)*
      z[46] + n<T>(1,3)*z[47] + 28*z[50] + 72*z[52] + z[57] + z[58] + z[59]
       + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + 31*z[66] + 3*
      z[67] + z[68] + n<T>(1,6)*z[69] + 15*z[70] + z[71] - 9*z[72] + 12*
      z[73] + 6*z[74] + 8*z[75] + z[76] + z[77] + z[78] + 2*z[79];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1114(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1114(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
