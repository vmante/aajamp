#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf587(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[33];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[15];
    z[11]=c[18];
    z[12]=c[19];
    z[13]=c[23];
    z[14]=c[25];
    z[15]=c[26];
    z[16]=c[28];
    z[17]=c[29];
    z[18]=c[31];
    z[19]=f[0];
    z[20]=f[5];
    z[21]=f[11];
    z[22]=f[17];
    z[23]=f[20];
    z[24]=i*z[1];
    z[25]= - n<T>(2,3)*z[24] - z[3] + n<T>(1,3)*z[4];
    z[26]=2*z[4];
    z[25]=z[25]*z[26];
    z[26]= - z[3] + 2*z[24];
    z[26]=z[26]*z[3];
    z[25]=z[25] - z[26];
    z[26]=n<T>(1,3)*z[5];
    z[27]= - 5*z[24] - z[4];
    z[27]=2*z[27] + 5*z[5];
    z[27]=z[27]*z[26];
    z[24]= - 2*z[2] + z[26] + n<T>(4,3)*z[4] + n<T>(10,3)*z[24] + z[3];
    z[24]=z[2]*z[24];
    z[26]=3*z[6];
    z[28]=n<T>(1,6)*z[9];
    z[29]= - z[7] - z[8] + z[28];
    z[24]=z[24] + z[27] + n<T>(5,3)*z[29] - z[26] + z[25];
    z[24]=z[2]*z[24];
    z[27]=2*z[7];
    z[29]= - z[26] + z[28] + z[27];
    z[29]=z[3]*z[29];
    z[30]=5*z[8];
    z[31]= - z[30] + n<T>(2,3)*z[9];
    z[32]= - z[3] - n<T>(2,3)*z[4];
    z[32]=z[5]*z[32];
    z[25]=z[32] - z[6] + n<T>(1,3)*z[31] - z[7] - z[25];
    z[25]=z[5]*z[25];
    z[30]=z[30] + z[27];
    z[26]=n<T>(1,3)*z[30] + z[26];
    z[26]=z[1]*z[26];
    z[30]= - 16*z[15] - 5*z[13];
    z[26]=n<T>(1,3)*z[30] + z[26];
    z[30]=2*i;
    z[26]=z[26]*z[30];
    z[27]=z[28] - z[27];
    z[27]=n<T>(1,3)*z[27] + z[6];
    z[27]=z[4]*z[27];
    z[28]=z[14] - z[20];
    z[30]=z[11] - z[21];

    r += n<T>(1,6)*z[10] - n<T>(5,18)*z[12] + n<T>(16,3)*z[16] - n<T>(1,2)*z[17]
     + n<T>(15,4)*z[18] - 3*z[19] - z[22]
     + n<T>(1,3)*z[23]
     + z[24]
     + z[25]
     + z[26]
       + z[27] + n<T>(5,3)*z[28] + z[29] + n<T>(2,3)*z[30];
 
    return r;
}

template std::complex<double> qqb_2lha_tf587(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf587(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
