#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1329(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[57];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[2];
    z[6]=d[8];
    z[7]=d[11];
    z[8]=d[3];
    z[9]=d[9];
    z[10]=d[12];
    z[11]=d[4];
    z[12]=d[7];
    z[13]=d[17];
    z[14]=e[5];
    z[15]=e[7];
    z[16]=e[9];
    z[17]=e[12];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[31];
    z[22]=e[6];
    z[23]=e[13];
    z[24]=e[14];
    z[25]=f[30];
    z[26]=f[31];
    z[27]=f[32];
    z[28]=f[33];
    z[29]=f[35];
    z[30]=f[36];
    z[31]=f[39];
    z[32]=f[43];
    z[33]=f[50];
    z[34]=f[51];
    z[35]=f[55];
    z[36]=f[58];
    z[37]=n<T>(1,3)*z[4];
    z[38]=z[1]*i;
    z[39]= - 2*z[38] + z[4];
    z[39]=z[39]*z[37];
    z[40]=npow(z[3],2);
    z[41]=n<T>(1,2)*z[40];
    z[42]=z[4] - z[3];
    z[43]=z[42]*z[8];
    z[44]=z[38]*z[3];
    z[45]= - n<T>(3,2)*z[3] + n<T>(5,3)*z[4];
    z[45]=z[12]*z[45];
    z[39]=z[45] + z[43] + z[39] - z[41] + 3*z[44];
    z[39]=z[12]*z[39];
    z[45]=n<T>(1,3)*z[5];
    z[46]= - z[3] + n<T>(2,3)*z[9];
    z[47]=n<T>(1,3)*z[38];
    z[48]=n<T>(2,3)*z[6] - z[45] + z[47] + z[46];
    z[48]=z[6]*z[48];
    z[49]=2*z[3];
    z[50]=z[49]*z[38];
    z[51]=z[47] - z[46];
    z[45]=2*z[51] - z[45];
    z[45]=z[5]*z[45];
    z[51]=z[38]*z[9];
    z[45]=z[48] + z[45] + n<T>(4,3)*z[51] - z[40] - z[50];
    z[45]=z[6]*z[45];
    z[48]= - z[3] + n<T>(2,3)*z[4];
    z[52]=n<T>(1,6)*z[12];
    z[53]= - z[52] + z[47] - z[48];
    z[53]=z[12]*z[53];
    z[47]= - z[3] + z[47];
    z[37]=n<T>(1,3)*z[11] - z[52] + n<T>(1,2)*z[47] + z[37];
    z[37]=z[11]*z[37];
    z[47]=z[38]*z[4];
    z[37]=z[37] + z[53] + n<T>(2,3)*z[47] - z[41] - z[44];
    z[37]=z[11]*z[37];
    z[41]=z[38] - z[8];
    z[52]= - z[42] + z[41];
    z[53]=n<T>(1,2)*z[8];
    z[52]=z[52]*z[53];
    z[54]=n<T>(3,2)*z[40];
    z[52]=z[52] + z[47] + z[54] - z[50];
    z[52]=z[8]*z[52];
    z[55]=z[38] - z[4];
    z[49]=z[53] - z[49] - z[55];
    z[49]=z[8]*z[49];
    z[53]=z[38] + z[3];
    z[56]= - z[4]*z[53];
    z[49]=z[49] + z[56] + z[54] + z[50];
    z[49]=z[9]*z[49];
    z[54]=z[44] + 2*z[40];
    z[56]=n<T>(1,2)*z[2] - z[12];
    z[42]=z[42]*z[56];
    z[53]=z[4] + z[53];
    z[53]=z[4]*z[53];
    z[42]= - z[43] + z[53] + z[42] - z[54];
    z[42]=z[2]*z[42];
    z[43]= - z[5]*z[38];
    z[43]= - z[18] - z[51] + z[43];
    z[43]=z[7]*z[43];
    z[51]=z[5] - z[38] + z[9];
    z[51]=z[17]*z[51];
    z[53]=z[9] + z[6];
    z[53]=z[24]*z[53];
    z[43]=z[43] + z[51] + z[53] - z[33];
    z[51]=z[40]*z[38];
    z[53]=npow(z[3],3);
    z[56]=z[3] + z[9];
    z[56]=z[23]*z[56];
    z[51]=z[56] + n<T>(5,3)*z[53] + z[51];
    z[53]=n<T>(2,3)*z[38];
    z[48]=z[53] + z[48];
    z[48]=z[4]*z[48];
    z[48]=2*z[48] - z[54];
    z[48]=z[4]*z[48];
    z[46]=n<T>(2,3)*z[5] - z[53] + z[46];
    z[46]=z[5]*z[46];
    z[40]=z[46] - z[40] + z[50];
    z[40]=z[5]*z[40];
    z[46]= - z[12]*z[38];
    z[46]= - z[18] - z[47] + z[46];
    z[46]=z[13]*z[46];
    z[47]= - z[12] + z[55];
    z[47]=z[16]*z[47];
    z[46]=z[46] + z[47];
    z[47]=z[11] - z[12];
    z[50]=19*z[3] + 5*z[4];
    z[47]=n<T>(4,3)*z[6] - z[5] - n<T>(19,6)*z[9] + n<T>(1,2)*z[50] + z[8] + n<T>(2,3)*
    z[47];
    z[47]=z[18]*z[47];
    z[47]=z[47] + z[35];
    z[50]=z[2] - z[12];
    z[50]=z[9] + 4*z[4] + 6*z[3] - z[41] - 2*z[50];
    z[50]=z[15]*z[50];
    z[38]=z[8]*z[38];
    z[38]=z[18] + z[44] + z[38];
    z[38]=z[10]*z[38];
    z[44]=z[4] + z[11];
    z[44]=z[22]*z[44];
    z[44]=z[44] - z[25];
    z[41]= - z[3] + z[41];
    z[41]=z[14]*z[41];
    z[53]=z[19]*i;

    r += 3*z[20] - n<T>(3,2)*z[21] - n<T>(5,2)*z[26] - z[27] - 8*z[28] - z[29]
       + n<T>(1,6)*z[30] - n<T>(1,2)*z[31] + z[32] - z[34] - z[36] + z[37] + 
      z[38] + z[39] + z[40] + z[41] + z[42] + n<T>(4,3)*z[43] + n<T>(2,3)*z[44]
       + z[45] + n<T>(8,3)*z[46] + n<T>(1,3)*z[47] + z[48] + z[49] + z[50] + 2*
      z[51] + z[52] - n<T>(5,18)*z[53];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1329(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1329(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
