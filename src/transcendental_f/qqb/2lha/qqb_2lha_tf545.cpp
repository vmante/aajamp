#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf545(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[89];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[4];
    z[6]=d[5];
    z[7]=d[7];
    z[8]=d[9];
    z[9]=d[16];
    z[10]=d[8];
    z[11]=d[11];
    z[12]=d[6];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[4];
    z[18]=e[5];
    z[19]=e[6];
    z[20]=e[7];
    z[21]=e[8];
    z[22]=e[9];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[18];
    z[28]=c[19];
    z[29]=c[21];
    z[30]=c[23];
    z[31]=c[25];
    z[32]=c[26];
    z[33]=c[28];
    z[34]=c[29];
    z[35]=c[31];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[5];
    z[42]=f[11];
    z[43]=f[17];
    z[44]=f[18];
    z[45]=f[20];
    z[46]=f[30];
    z[47]=f[31];
    z[48]=f[32];
    z[49]=f[33];
    z[50]=f[34];
    z[51]=f[35];
    z[52]=f[36];
    z[53]=f[37];
    z[54]=f[38];
    z[55]=f[39];
    z[56]=f[43];
    z[57]=f[48];
    z[58]=f[49];
    z[59]=f[50];
    z[60]=f[51];
    z[61]=f[52];
    z[62]=f[54];
    z[63]=f[55];
    z[64]=f[58];
    z[65]=n<T>(1,3)*z[6];
    z[66]=z[1]*i;
    z[67]= - z[66] + z[6];
    z[67]=z[67]*z[65];
    z[65]=n<T>(2,3)*z[3] + n<T>(1,2)*z[66] + z[65];
    z[65]=z[3]*z[65];
    z[68]=z[3] + z[6];
    z[69]=n<T>(1,2)*z[12];
    z[70]= - z[68]*z[69];
    z[71]=z[66] - z[3];
    z[72]=n<T>(1,2)*z[2];
    z[73]=z[71]*z[72];
    z[74]=n<T>(1,2)*z[4];
    z[75]= - z[74] + z[66] + z[12];
    z[74]=z[75]*z[74];
    z[75]=z[2] - z[3];
    z[76]=z[12] - z[6];
    z[77]=z[76] + z[75];
    z[77]=z[5]*z[77];
    z[78]=z[66] - z[4];
    z[79]=z[6] + z[78];
    z[79]=z[8]*z[79];
    z[65]=n<T>(1,4)*z[79] + n<T>(1,2)*z[77] + z[74] + z[73] + z[70] + z[67] + 
    z[65];
    z[65]=z[8]*z[65];
    z[67]=n<T>(121,12)*z[7];
    z[70]=n<T>(1,2)*z[6];
    z[73]=z[4] + z[67] - n<T>(37,2)*z[2] - z[69] + n<T>(101,12)*z[3] + 3*z[66]
    - z[70];
    z[73]=z[4]*z[73];
    z[67]=z[67] + n<T>(19,3)*z[2] - n<T>(53,2)*z[3] - n<T>(121,6)*z[66] + z[76];
    z[67]=z[7]*z[67];
    z[74]=n<T>(1,2)*z[3];
    z[77]=z[74] - z[66];
    z[77]=z[77]*z[3];
    z[79]=n<T>(3,2)*z[6];
    z[80]=7*z[66];
    z[81]=z[80] - z[79];
    z[81]=z[6]*z[81];
    z[82]= - z[69] + z[66] - 3*z[6];
    z[82]=z[12]*z[82];
    z[67]=z[73] + z[67] + z[82] + z[81] + n<T>(101,6)*z[77];
    z[73]=n<T>(29,3)*z[3];
    z[81]=37*z[66];
    z[76]=z[73] + z[81] - z[76];
    z[76]=n<T>(1,8)*z[76] - z[2];
    z[76]=z[2]*z[76];
    z[67]=z[76] + n<T>(1,8)*z[67];
    z[67]=z[4]*z[67];
    z[76]=3*z[3];
    z[82]=3*z[2];
    z[81]=z[81] - 103*z[6];
    z[81]=n<T>(161,12)*z[5] - n<T>(37,8)*z[7] + z[82] + n<T>(27,4)*z[12] + n<T>(1,8)*
    z[81] - z[76];
    z[81]=z[5]*z[81];
    z[83]=z[70] + z[66];
    z[84]= - n<T>(1,8)*z[3] + z[83];
    z[84]=z[3]*z[84];
    z[74]= - n<T>(9,4)*z[12] - z[74] + n<T>(33,4)*z[66] + z[6];
    z[74]=z[74]*z[69];
    z[76]= - n<T>(5,2)*z[2] - z[12] + z[66] + z[76];
    z[85]=n<T>(1,4)*z[2];
    z[76]=z[76]*z[85];
    z[86]=95*z[6];
    z[87]=53*z[66] + z[86];
    z[75]= - n<T>(53,8)*z[7] - n<T>(29,2)*z[12] + n<T>(1,4)*z[87] + z[75];
    z[87]=n<T>(1,4)*z[7];
    z[75]=z[75]*z[87];
    z[88]= - 103*z[66] - n<T>(87,2)*z[6];
    z[88]=z[6]*z[88];
    z[74]=n<T>(1,4)*z[81] + z[75] + z[76] + z[74] + n<T>(1,16)*z[88] + z[84];
    z[74]=z[5]*z[74];
    z[75]=z[66] - z[70];
    z[75]=z[6]*z[75];
    z[76]=n<T>(7,4)*z[3] + z[66] - n<T>(7,8)*z[6];
    z[76]=z[3]*z[76];
    z[75]=n<T>(7,4)*z[75] + z[76];
    z[75]=z[3]*z[75];
    z[76]= - z[3] - z[8];
    z[76]=z[66]*z[76];
    z[76]= - z[24] + z[76];
    z[76]=z[11]*z[76];
    z[81]=n<T>(169,2)*z[66] + 185*z[6];
    z[81]=z[81]*npow(z[6],2);
    z[84]=z[8] - z[71];
    z[84]=z[23]*z[84];
    z[75]=z[84] + z[76] + n<T>(1,16)*z[81] + z[75];
    z[76]=n<T>(1,2)*z[7];
    z[81]= - 25*z[66] + z[6];
    z[81]=n<T>(25,4)*z[4] + z[76] + n<T>(27,4)*z[2] - z[69] + n<T>(1,2)*z[81] - z[3]
   ;
    z[81]=z[16]*z[81];
    z[84]=z[86] - n<T>(13,4)*z[3];
    z[84]=n<T>(1,3)*z[84] - 13*z[12];
    z[82]=n<T>(13,9)*z[8] + n<T>(15,2)*z[5] + n<T>(29,24)*z[4] - n<T>(31,18)*z[7]
     + n<T>(1,3)
   *z[84] - z[82];
    z[82]=n<T>(1,2)*z[82] + n<T>(1,9)*z[10];
    z[82]=z[24]*z[82];
    z[84]=z[7] - z[2] + n<T>(43,4)*z[12] + n<T>(47,4)*z[6] - z[78];
    z[84]=z[20]*z[84];
    z[86]= - 131*z[66] + n<T>(125,2)*z[3];
    z[86]=z[7] + n<T>(125,12)*z[2] + n<T>(1,6)*z[86] - z[12];
    z[86]=z[15]*z[86];
    z[81]=z[82] + z[81] + z[84] + z[86];
    z[79]= - z[66] + z[79];
    z[79]=z[79]*z[70];
    z[82]=n<T>(1,3)*z[3];
    z[84]=65*z[66] - n<T>(77,2)*z[3];
    z[84]=z[84]*z[82];
    z[73]= - z[73] - 155*z[66] - z[6];
    z[73]=n<T>(199,6)*z[2] + n<T>(1,2)*z[73] - 7*z[12];
    z[73]=z[73]*z[72];
    z[70]=z[70] + z[3];
    z[70]=3*z[70] - n<T>(19,4)*z[12];
    z[70]=z[12]*z[70];
    z[70]=z[73] + z[70] + z[79] + z[84];
    z[70]=z[70]*z[85];
    z[73]=55*z[66] - n<T>(47,2)*z[6];
    z[73]=z[6]*z[73];
    z[73]=z[73] + 53*z[77];
    z[77]=n<T>(1,3)*z[7] - n<T>(35,3)*z[2] + n<T>(27,2)*z[12] + n<T>(53,8)*z[3] - z[66]
    - n<T>(55,8)*z[6];
    z[77]=z[77]*z[87];
    z[79]= - n<T>(17,8)*z[66] - z[6];
    z[79]=n<T>(41,16)*z[12] + 3*z[79] + n<T>(1,4)*z[3];
    z[79]=z[12]*z[79];
    z[84]= - n<T>(55,48)*z[2] + n<T>(9,8)*z[12] + n<T>(22,3)*z[66] + n<T>(1,8)*z[6];
    z[84]=z[2]*z[84];
    z[73]=z[77] + z[84] + n<T>(1,16)*z[73] + z[79];
    z[73]=z[7]*z[73];
    z[77]= - z[6]*z[83];
    z[79]=n<T>(1,3)*z[66];
    z[83]= - n<T>(1,6)*z[3] + z[79] + z[6];
    z[83]=z[3]*z[83];
    z[77]=z[77] + z[83];
    z[83]=n<T>(1,3)*z[8];
    z[71]=z[71]*z[83];
    z[79]= - z[82] + z[79] - z[6];
    z[79]=n<T>(1,3)*z[10] + n<T>(1,2)*z[79] + z[83];
    z[82]=n<T>(1,2)*z[10];
    z[79]=z[79]*z[82];
    z[71]=z[79] + n<T>(1,2)*z[77] + z[71];
    z[71]=z[71]*z[82];
    z[77]=z[80] + n<T>(5,2)*z[6];
    z[77]=z[6]*z[77];
    z[68]= - z[3]*z[68];
    z[79]=z[80] - 15*z[6];
    z[79]= - n<T>(7,3)*z[12] + n<T>(1,2)*z[79] + z[3];
    z[79]=z[12]*z[79];
    z[68]=n<T>(1,4)*z[79] + z[77] + z[68];
    z[68]=z[68]*z[69];
    z[69]=z[76] - z[66] + z[72];
    z[69]=z[21]*z[69];
    z[72]=z[6] + z[8];
    z[72]=z[36]*z[72];
    z[76]=z[8] + z[10];
    z[76]=z[37]*z[76];
    z[72]= - z[72] - z[76] - z[62] + z[59] + z[45] - z[27];
    z[76]= - z[6] - z[4];
    z[76]=z[66]*z[76];
    z[76]= - z[24] + z[76];
    z[76]=z[13]*z[76];
    z[77]=z[6] - z[78];
    z[77]=z[18]*z[77];
    z[76]=z[44] + z[76] + z[77];
    z[77]= - z[12] - z[7];
    z[77]=z[66]*z[77];
    z[77]= - z[24] + z[77];
    z[77]=z[14]*z[77];
    z[78]=z[66] - z[7];
    z[79]= - z[12] + z[78];
    z[79]=z[22]*z[79];
    z[77]=z[54] + z[77] + z[79];
    z[79]= - z[2] + z[8] + n<T>(5,4)*z[5];
    z[80]= - n<T>(5,4)*z[3] - z[79];
    z[80]=z[66]*z[80];
    z[80]= - n<T>(1,4)*z[24] + z[80];
    z[80]=z[9]*z[80];
    z[66]= - z[66] + 5*z[3];
    z[66]=n<T>(1,4)*z[66] + z[79];
    z[66]=z[17]*z[66];
    z[79]=z[5] + z[12];
    z[78]= - z[6] - z[78] + n<T>(43,8)*z[79];
    z[78]=z[19]*z[78];
    z[79]=z[64] + z[58] + z[57] + z[34];
    z[82]=z[46] - z[41];
    z[83]=z[53] + z[39];
    z[84]=n<T>(99,4)*z[32] + n<T>(167,16)*z[30] - n<T>(5,2)*z[29] - n<T>(121,288)*z[25];
    z[84]=i*z[84];

    r += n<T>(17,4)*z[26] + n<T>(13,16)*z[28] - n<T>(193,48)*z[31] - n<T>(89,8)*z[33]
       - n<T>(1153,144)*z[35] + n<T>(15,4)*z[38] + n<T>(149,96)*z[40] + n<T>(121,96)*
      z[42] + n<T>(49,32)*z[43] - n<T>(15,32)*z[47] + n<T>(17,16)*z[48] - 2*z[49]
       - n<T>(9,16)*z[50] + n<T>(21,8)*z[51] + n<T>(45,32)*z[52] - n<T>(95,32)*z[55]
     - 
      n<T>(5,4)*z[56] - n<T>(7,24)*z[60] + n<T>(1,4)*z[61] + n<T>(1,24)*z[63] + z[65]
       + z[66] + z[67] + z[68] + n<T>(67,12)*z[69] + z[70] + z[71] - n<T>(1,6)*
      z[72] + z[73] + z[74] + n<T>(1,3)*z[75] + n<T>(3,4)*z[76] + n<T>(15,8)*z[77]
       + z[78] - n<T>(1,8)*z[79] + z[80] + n<T>(1,2)*z[81] - 3*z[82] - n<T>(5,8)*
      z[83] + z[84];
 
    return r;
}

template std::complex<double> qqb_2lha_tf545(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf545(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
