#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf157(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[84];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[4];
    z[10]=d[15];
    z[11]=d[16];
    z[12]=d[5];
    z[13]=d[17];
    z[14]=d[18];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[9];
    z[21]=e[10];
    z[22]=e[11];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=c[31];
    z[32]=e[3];
    z[33]=e[6];
    z[34]=e[7];
    z[35]=f[0];
    z[36]=f[2];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[6];
    z[41]=f[7];
    z[42]=f[8];
    z[43]=f[10];
    z[44]=f[11];
    z[45]=f[12];
    z[46]=f[13];
    z[47]=f[14];
    z[48]=f[16];
    z[49]=f[17];
    z[50]=f[18];
    z[51]=f[23];
    z[52]=f[26];
    z[53]=f[27];
    z[54]=f[30];
    z[55]=f[31];
    z[56]=f[35];
    z[57]=f[36];
    z[58]=f[37];
    z[59]=f[39];
    z[60]=f[41];
    z[61]=2*z[5];
    z[62]=2*z[7];
    z[63]=z[1]*i;
    z[64]= - z[61] + n<T>(23,3)*z[63] + z[62];
    z[65]=z[4] + z[2];
    z[64]=2*z[64] - n<T>(23,3)*z[65];
    z[64]=z[15]*z[64];
    z[65]=z[5] + z[3];
    z[65]=z[63]*z[65];
    z[65]=z[23] + z[65];
    z[65]=z[10]*z[65];
    z[66]=z[4] + z[9];
    z[67]=z[63]*z[66];
    z[67]=z[23] + z[67];
    z[67]=z[11]*z[67];
    z[68]= - z[7] - z[8];
    z[68]=z[63]*z[68];
    z[68]= - z[23] + z[68];
    z[68]=z[14]*z[68];
    z[66]=z[63] - z[66];
    z[66]=z[18]*z[66];
    z[69]=z[63] - z[5];
    z[70]=z[3] - z[69];
    z[70]=z[17]*z[70];
    z[71]=z[63] - z[7];
    z[72]=z[8] - z[71];
    z[72]=z[22]*z[72];
    z[73]= - z[6] - z[12];
    z[73]=z[34]*z[73];
    z[64]=z[72] + z[73] + z[60] - z[56] + z[67] + z[68] + z[70] + z[66]
    + z[40] + z[64] + z[65];
    z[65]=n<T>(13,3)*z[5];
    z[66]=n<T>(1,3)*z[4];
    z[67]=4*z[7];
    z[68]= - z[66] + z[6] + n<T>(25,3)*z[2] - z[65] - z[63] - z[67];
    z[68]=z[4]*z[68];
    z[70]=2*z[63];
    z[72]=z[70] - z[7];
    z[73]=z[72]*z[67];
    z[67]=n<T>(13,3)*z[63] + z[67];
    z[65]=2*z[67] - z[65];
    z[65]=z[5]*z[65];
    z[67]= - 25*z[63] + z[5];
    z[67]=2*z[67] - z[2];
    z[67]=z[2]*z[67];
    z[74]= - z[70] + z[9];
    z[74]=z[9]*z[74];
    z[75]= - z[6]*z[70];
    z[65]=z[68] + z[75] + z[74] + n<T>(1,3)*z[67] + z[73] + z[65];
    z[65]=z[65]*z[66];
    z[66]= - z[61] + z[71];
    z[66]=z[66]*z[61];
    z[67]=z[5] + z[7];
    z[68]=z[67] - z[63];
    z[73]=2*z[68] - z[2];
    z[73]=z[2]*z[73];
    z[74]=z[63] + z[2];
    z[67]=n<T>(11,3)*z[3] - z[9] + z[67] - 4*z[74];
    z[67]=z[3]*z[67];
    z[75]=z[72]*z[7];
    z[76]= - z[9] - z[69];
    z[76]=z[9]*z[76];
    z[66]=z[67] + 2*z[76] + z[73] + z[75] + z[66];
    z[66]=z[3]*z[66];
    z[67]=z[12] - z[6];
    z[73]=n<T>(2,3)*z[3];
    z[76]=7*z[2] - 25*z[7] + 11*z[5];
    z[76]=n<T>(1,3)*z[76] - n<T>(17,2)*z[9];
    z[67]=n<T>(17,6)*z[4] - n<T>(17,4)*z[8] + n<T>(1,2)*z[76] + z[73] - 2*z[67];
    z[67]=z[23]*z[67];
    z[66]= - z[46] + z[66] + z[67];
    z[67]=n<T>(7,2)*z[5];
    z[76]=z[67] - 7*z[63] + z[62];
    z[76]=z[5]*z[76];
    z[77]=z[72]*z[62];
    z[78]= - z[2] + z[62] + z[69];
    z[78]=z[2]*z[78];
    z[76]=2*z[78] + z[77] + z[76];
    z[77]=5*z[7];
    z[67]=5*z[2] + z[67] - n<T>(7,2)*z[63] - z[77];
    z[78]=n<T>(1,2)*z[9];
    z[67]= - n<T>(5,9)*z[8] - n<T>(5,3)*z[3] + n<T>(1,3)*z[67] + z[78];
    z[67]=z[8]*z[67];
    z[69]=z[78] + z[69];
    z[69]=z[9]*z[69];
    z[79]=z[3] + z[2] - z[61] + z[72];
    z[73]=z[79]*z[73];
    z[67]=z[67] + z[73] + n<T>(1,3)*z[76] + z[69];
    z[69]=n<T>(1,3)*z[8];
    z[67]=z[67]*z[69];
    z[73]= - z[63] + n<T>(1,2)*z[7];
    z[76]=n<T>(1,3)*z[7];
    z[79]=z[73]*z[76];
    z[78]=z[78] + z[71];
    z[78]=z[9]*z[78];
    z[80]=n<T>(2,3)*z[6];
    z[81]= - z[71]*z[80];
    z[82]=n<T>(1,3)*z[6];
    z[83]= - n<T>(1,3)*z[71] + z[9];
    z[83]= - n<T>(1,3)*z[12] + n<T>(1,2)*z[83] - z[82];
    z[83]=z[12]*z[83];
    z[78]=z[83] + z[81] + z[79] + z[78];
    z[78]=z[12]*z[78];
    z[79]=n<T>(8,9)*z[5];
    z[81]=z[79] - n<T>(16,9)*z[63] - z[7];
    z[61]=z[81]*z[61];
    z[79]= - n<T>(23,9)*z[2] + z[79] + n<T>(53,9)*z[63] + z[7];
    z[79]=z[2]*z[79];
    z[61]=z[79] - n<T>(5,3)*z[75] + z[61];
    z[61]=z[2]*z[61];
    z[75]=5*z[63] - z[62];
    z[75]=z[75]*z[76];
    z[76]=n<T>(1,3)*z[9];
    z[79]= - 2*z[71] - z[9];
    z[79]=z[79]*z[76];
    z[81]=z[2]*z[71];
    z[75]=z[79] + z[75] + z[81];
    z[62]=z[63] - z[62];
    z[62]=z[80] + z[9] + n<T>(1,3)*z[62] - z[2];
    z[62]=z[6]*z[62];
    z[62]=2*z[75] + z[62];
    z[62]=z[62]*z[82];
    z[75]= - z[2] + z[72];
    z[69]=z[82] + 2*z[75] - z[69];
    z[69]=z[19]*z[69];
    z[75]= - z[71]*npow(z[7],2);
    z[72]=z[72] - z[5];
    z[72]=z[5]*z[7]*z[72];
    z[72]=z[39] + z[53] - z[58] + z[75] + z[72] + z[47] - z[44];
    z[75]=z[7] + z[6];
    z[75]=z[63]*z[75];
    z[75]=z[23] + z[75];
    z[75]=z[13]*z[75];
    z[71]=z[6] - z[71];
    z[71]=z[20]*z[71];
    z[71]=z[54] + z[75] + z[71];
    z[70]= - z[5] + z[70] - z[2];
    z[70]=z[16]*z[70];
    z[75]= - z[9] - z[6];
    z[75]=z[33]*z[75];
    z[70]= - z[25] + z[70] + z[75];
    z[68]= - z[2] + z[68];
    z[68]=7*z[8] + 2*z[68] + 5*z[3];
    z[68]=z[21]*z[68];
    z[75]=z[9] + z[3];
    z[75]=z[32]*z[75];
    z[68]=z[36] + z[68] + z[75];
    z[73]=z[73]*z[77];
    z[63]= - z[63] + n<T>(1,2)*z[5];
    z[63]=z[5]*z[63];
    z[63]=z[73] + z[63];
    z[73]= - n<T>(4,3)*z[9] + n<T>(1,6)*z[5] + n<T>(5,6)*z[7] - z[74];
    z[73]=z[9]*z[73];
    z[63]=n<T>(1,3)*z[63] + z[73];
    z[63]=z[63]*z[76];
    z[73]= - n<T>(184,9)*z[29] - n<T>(53,9)*z[27] + n<T>(17,54)*z[24];
    z[73]=i*z[73];

    r += n<T>(5,54)*z[26] + n<T>(23,9)*z[28] + n<T>(92,9)*z[30] - n<T>(124,27)*z[31]
     - 
      n<T>(11,3)*z[35] - n<T>(7,9)*z[37] + n<T>(29,18)*z[38] + n<T>(16,9)*z[41] - 3*
      z[42] + n<T>(26,9)*z[43] + n<T>(17,18)*z[45] - n<T>(7,6)*z[48] - n<T>(4,3)*z[49]
       - n<T>(5,9)*z[50] - n<T>(8,9)*z[51] + z[52] + n<T>(1,6)*z[55] - n<T>(5,18)*z[57]
       + n<T>(1,2)*z[59] + z[61] + z[62] + z[63] + n<T>(2,3)*z[64] + z[65] + n<T>(1,9)*z[66]
     + z[67]
     + n<T>(2,9)*z[68]
     + 2*z[69]
     + n<T>(10,9)*z[70]
     + n<T>(4,9)*
      z[71] + n<T>(1,3)*z[72] + z[73] + z[78];
 
    return r;
}

template std::complex<double> qqb_2lha_tf157(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf157(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
