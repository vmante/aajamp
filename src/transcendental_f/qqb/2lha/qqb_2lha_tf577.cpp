#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf577(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[30];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[4];
    z[5]=d[16];
    z[6]=e[0];
    z[7]=e[4];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[18];
    z[12]=c[19];
    z[13]=c[21];
    z[14]=c[23];
    z[15]=c[25];
    z[16]=c[26];
    z[17]=c[28];
    z[18]=c[29];
    z[19]=c[31];
    z[20]=f[0];
    z[21]=f[1];
    z[22]=f[5];
    z[23]=f[18];
    z[24]=n<T>(1,2)*z[3];
    z[25]=z[4] - z[3];
    z[25]=z[25]*z[24];
    z[26]=npow(z[2],2);
    z[26]=z[6] - z[7] + n<T>(1,2)*z[26];
    z[27]= - z[2] + z[4];
    z[27]=z[4]*z[27];
    z[25]=z[25] + z[27] + z[26];
    z[25]=z[3]*z[25];
    z[27]=z[2] - z[5];
    z[24]=z[24] - z[27];
    z[24]=z[1]*z[24];
    z[28]=z[4]*z[1];
    z[24]= - z[28] + z[24];
    z[24]=z[3]*z[24];
    z[29]= - z[1]*z[26];
    z[28]=z[5]*z[28];
    z[24]=z[24] + z[28] + z[29] - n<T>(1,3)*z[9] - z[13] + 3*z[16] + 2*z[14]
   ;
    z[24]=i*z[24];
    z[28]= - z[2] - n<T>(1,3)*z[4];
    z[28]=z[4]*z[28];
    z[26]=z[28] + n<T>(1,12)*z[8] + z[26];
    z[26]=z[4]*z[26];
    z[28]=z[22] + z[15] - z[18];
    z[29]=z[11] + z[23];
    z[27]= - z[8]*z[27];

    r +=  - n<T>(13,12)*z[10] + n<T>(25,24)*z[12] - n<T>(1,2)*z[17] + n<T>(35,24)*z[19]
       + z[20] + z[21] + z[24] + z[25] + z[26] + z[27] - n<T>(1,4)*z[28] - 
      n<T>(1,3)*z[29];
 
    return r;
}

template std::complex<double> qqb_2lha_tf577(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf577(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
