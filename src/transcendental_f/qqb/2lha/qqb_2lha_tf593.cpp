#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf593(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[16];
    z[12]=d[17];
    z[13]=e[0];
    z[14]=e[1];
    z[15]=e[4];
    z[16]=e[7];
    z[17]=e[8];
    z[18]=e[9];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[18];
    z[23]=c[19];
    z[24]=c[21];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[29];
    z[30]=c[31];
    z[31]=e[3];
    z[32]=e[10];
    z[33]=e[6];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[3];
    z[37]=f[4];
    z[38]=f[6];
    z[39]=f[8];
    z[40]=f[9];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[14];
    z[44]=f[17];
    z[45]=f[18];
    z[46]=f[21];
    z[47]=f[30];
    z[48]=f[31];
    z[49]=f[32];
    z[50]=f[33];
    z[51]=f[34];
    z[52]=f[35];
    z[53]=f[36];
    z[54]=f[39];
    z[55]=f[43];
    z[56]=n<T>(1,6)*z[5];
    z[57]=z[1]*i;
    z[58]=n<T>(1,3)*z[57];
    z[59]=z[2] - z[8];
    z[60]=z[56] - z[58] + z[59];
    z[60]=z[5]*z[60];
    z[61]=n<T>(1,2)*z[2];
    z[62]=z[61] - z[8] - z[57];
    z[62]=z[2]*z[62];
    z[63]=n<T>(1,3)*z[5];
    z[58]=z[58] - z[63];
    z[64]=n<T>(1,6)*z[3] - z[59] + z[58];
    z[64]=z[3]*z[64];
    z[58]= - n<T>(1,3)*z[3] - z[59] - z[58];
    z[58]=z[9]*z[58];
    z[65]=npow(z[8],2);
    z[66]=n<T>(1,2)*z[65];
    z[67]=z[57]*z[8];
    z[58]=n<T>(1,2)*z[58] + z[64] + z[60] + z[62] + z[66] + z[67];
    z[58]=z[9]*z[58];
    z[60]= - z[2] + 2*z[8];
    z[62]=z[57] + z[60];
    z[62]=z[2]*z[62];
    z[64]=n<T>(1,2)*z[5];
    z[68]=z[64] - z[57];
    z[69]=z[68] - z[59];
    z[70]=z[69]*z[63];
    z[56]= - n<T>(1,9)*z[7] + z[56] - n<T>(2,3)*z[2] + z[8] + n<T>(1,6)*z[57];
    z[56]=z[7]*z[56];
    z[56]=z[56] + z[70] + n<T>(1,3)*z[62] + n<T>(4,3)*z[65] - 3*z[67];
    z[56]=z[7]*z[56];
    z[62]=z[66] - z[67];
    z[65]=z[57] - z[8];
    z[61]= - z[61] - z[65];
    z[61]=z[2]*z[61];
    z[69]= - z[5]*z[69];
    z[70]=z[5] - z[2];
    z[71]= - n<T>(5,2)*z[7] + 4*z[65] - z[70];
    z[71]=z[7]*z[71];
    z[61]=z[71] + z[69] - n<T>(11,2)*z[62] + z[61];
    z[69]=n<T>(1,2)*z[10];
    z[71]= - z[69] - z[65];
    z[72]=n<T>(5,2)*z[10];
    z[71]=z[71]*z[72];
    z[70]=n<T>(7,2)*z[65] + z[70];
    z[70]=n<T>(41,9)*z[6] + n<T>(1,3)*z[70] - z[72];
    z[70]=z[6]*z[70];
    z[61]=n<T>(1,2)*z[70] + n<T>(1,3)*z[61] + z[71];
    z[61]=z[6]*z[61];
    z[70]=z[69] - z[57];
    z[71]=z[5] + z[70];
    z[72]=n<T>(1,3)*z[10];
    z[71]=z[71]*z[72];
    z[72]= - z[72] + z[59];
    z[72]=z[3]*z[72];
    z[73]=2*z[57];
    z[74]=2*z[2] - z[8] - z[73];
    z[74]=z[2]*z[74];
    z[75]= - z[5]*z[59];
    z[71]=n<T>(1,2)*z[72] + z[71] + z[75] + z[74] - z[62];
    z[71]=z[3]*z[71];
    z[72]= - z[2] + 3*z[57];
    z[74]=n<T>(1,2)*z[8];
    z[75]= - n<T>(1,3)*z[4] - z[3] + z[64] + z[74] + z[72];
    z[75]=z[4]*z[75];
    z[75]=z[62] + z[75];
    z[76]=z[3] + z[2];
    z[76]=z[57]*z[76];
    z[77]= - z[8] + z[68];
    z[77]=z[77]*z[64];
    z[70]= - z[10]*z[70];
    z[70]=z[70] + z[77] + z[76] + n<T>(1,2)*z[75];
    z[70]=z[4]*z[70];
    z[75]= - z[7]*z[57];
    z[75]= - z[19] - z[67] + z[75];
    z[75]=z[12]*z[75];
    z[76]= - z[7] + z[65];
    z[76]=z[18]*z[76];
    z[77]= - z[10] - z[3];
    z[77]=z[31]*z[77];
    z[78]=z[3] + z[9];
    z[78]=z[32]*z[78];
    z[75]=z[77] + z[78] - z[55] - z[26] + z[22] + z[75] + z[76];
    z[60]= - z[57] + z[60];
    z[60]=z[2]*z[60];
    z[76]= - n<T>(3,2)*z[8] + z[2];
    z[64]=z[76]*z[64];
    z[60]=z[64] - n<T>(3,2)*z[62] + z[60];
    z[60]=z[5]*z[60];
    z[64]=n<T>(1,6)*z[2] - z[74] + z[57];
    z[64]=z[2]*z[64];
    z[64]=z[64] + z[66] - 2*z[67];
    z[64]=z[2]*z[64];
    z[66]= - z[5]*z[68];
    z[65]=z[7]*z[65];
    z[62]=5*z[65] - n<T>(5,2)*z[62] + z[66];
    z[65]= - 5*z[8] + 7*z[57];
    z[63]=n<T>(7,3)*z[10] + n<T>(5,3)*z[7] - z[63] + n<T>(1,6)*z[65] + z[2];
    z[63]=z[63]*z[69];
    z[62]=n<T>(1,3)*z[62] + z[63];
    z[62]=z[10]*z[62];
    z[59]=z[5] - z[59] - z[57];
    z[59]=n<T>(11,3)*z[6] + n<T>(2,3)*z[59] + 3*z[7];
    z[59]=z[16]*z[59];
    z[63]=z[4] + z[10];
    z[65]= - z[57]*z[63];
    z[65]= - z[19] + z[65];
    z[65]=z[11]*z[65];
    z[57]= - z[57] + z[63];
    z[57]=z[15]*z[57];
    z[63]= - z[43] + z[40] - z[38] + z[35] + z[29] + z[51] - z[49];
    z[66]= - z[36] + z[48] - z[44];
    z[67]=z[42] + z[37];
    z[68]=z[7] + z[10];
    z[68]=z[33]*z[68];
    z[68]=z[68] - z[47];
    z[69]=n<T>(23,4)*z[4] - n<T>(2,3)*z[9] + n<T>(10,3)*z[6] + n<T>(25,12)*z[10]
     - n<T>(11,6)
   *z[7] + n<T>(1,4)*z[5] + n<T>(1,3)*z[8] - n<T>(5,2)*z[2];
    z[69]=z[19]*z[69];
    z[69]=z[69] + z[45];
    z[74]= - 3*z[27] + n<T>(3,2)*z[25] - z[24] - n<T>(7,18)*z[20];
    z[74]=i*z[74];
    z[72]=z[3] - z[5] - z[8] + z[72];
    z[72]=z[17]*z[72];
    z[73]=z[73] - z[2];
    z[76]=z[4] - z[73];
    z[76]=z[13]*z[76];
    z[73]= - z[5] + z[73];
    z[73]=z[14]*z[73];

    r += n<T>(11,6)*z[21] + n<T>(17,12)*z[23] + z[28] + n<T>(21,4)*z[30] + z[34]
     - 
      4*z[39] - n<T>(3,4)*z[41] - z[46] - n<T>(8,3)*z[50] + z[52] + n<T>(5,12)*
      z[53] - n<T>(5,4)*z[54] + z[56] + z[57] + z[58] + z[59] + z[60] + 
      z[61] + z[62] - n<T>(1,2)*z[63] + z[64] + z[65] - n<T>(1,4)*z[66] + n<T>(1,6)
      *z[67] + n<T>(5,3)*z[68] + n<T>(1,3)*z[69] + z[70] + z[71] + 2*z[72] + 
      z[73] + z[74] + n<T>(2,3)*z[75] + z[76];
 
    return r;
}

template std::complex<double> qqb_2lha_tf593(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf593(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
