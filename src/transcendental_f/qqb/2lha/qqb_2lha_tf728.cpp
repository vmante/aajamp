#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf728(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[35];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[8];
    z[4]=d[15];
    z[5]=d[1];
    z[6]=d[3];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=e[2];
    z[10]=e[10];
    z[11]=c[3];
    z[12]=c[11];
    z[13]=c[15];
    z[14]=c[17];
    z[15]=c[31];
    z[16]=e[3];
    z[17]=f[2];
    z[18]=f[4];
    z[19]=f[7];
    z[20]=f[10];
    z[21]=f[12];
    z[22]=f[16];
    z[23]=f[19];
    z[24]=n<T>(5,4)*z[6];
    z[25]=2*z[5];
    z[26]=z[1]*i;
    z[27]=n<T>(1,3)*z[7] - n<T>(5,4)*z[26] + z[24] - z[25] + n<T>(3,4)*z[3];
    z[27]=z[7]*z[27];
    z[28]=npow(z[5],2);
    z[29]=npow(z[3],2);
    z[29]=z[28] + n<T>(3,2)*z[29];
    z[30]=n<T>(3,2)*z[3];
    z[24]=z[24] - z[5] - z[30];
    z[24]=z[6]*z[24];
    z[30]= - n<T>(5,2)*z[6] - z[25] + z[30];
    z[30]=z[30]*z[26];
    z[24]=z[27] + z[30] + n<T>(1,2)*z[29] + z[24];
    z[24]=z[7]*z[24];
    z[27]=z[8] - z[2];
    z[27]= - n<T>(9,4)*z[27];
    z[25]=z[27] - 3*z[7] - z[25] - n<T>(11,4)*z[6] + n<T>(9,4)*z[3];
    z[29]= - z[4]*z[25];
    z[30]=n<T>(11,4)*z[5] - z[3];
    z[30]=z[3]*z[30];
    z[31]= - z[6] + n<T>(7,4)*z[5] - 2*z[3];
    z[31]=z[6]*z[31];
    z[29]=z[29] + z[31] - n<T>(11,4)*z[28] + z[30];
    z[29]=z[26]*z[29];
    z[30]=z[3] - z[5];
    z[30]=z[30]*z[6];
    z[31]=z[26]*z[3];
    z[32]=z[3]*z[5];
    z[30]= - z[30] + z[31] + z[32] - z[28];
    z[30]=z[30]*z[27];
    z[31]= - n<T>(1,2)*z[5] + z[3];
    z[31]=z[3]*z[31];
    z[33]=n<T>(1,3)*z[6] - n<T>(9,4)*z[5] + z[3];
    z[33]=z[6]*z[33];
    z[31]=z[33] + n<T>(3,4)*z[28] + z[31];
    z[31]=z[6]*z[31];
    z[33]=z[26] - z[6];
    z[34]=n<T>(25,4)*z[3];
    z[27]=4*z[5] + z[34] - z[27] - n<T>(9,4)*z[33];
    z[27]=z[10]*z[27];
    z[28]=z[28] - 7*z[32];
    z[28]=z[3]*z[28];
    z[32]=npow(z[5],3);
    z[28]=z[20] + n<T>(7,3)*z[32] + n<T>(1,2)*z[28];
    z[25]= - n<T>(11,4)*z[26] - z[25];
    z[25]=z[9]*z[25];
    z[26]= - z[5] - z[34];
    z[26]=n<T>(11,4)*z[4] - n<T>(3,4)*z[7] + n<T>(1,3)*z[26] - n<T>(1,4)*z[6];
    z[26]=z[11]*z[26];
    z[32]=z[5] + z[7];
    z[32]=z[16]*z[32];
    z[33]=z[12]*i;
    z[34]=n<T>(9,4)*i;
    z[34]=z[14]*z[34];

    r +=  - n<T>(13,8)*z[13] - n<T>(63,16)*z[15] + z[17] + z[18] + 9*z[19] - n<T>(1,4)*z[21]
     + n<T>(3,4)*z[22]
     + z[23]
     + z[24]
     + z[25]
     + z[26]
     + z[27]
     +  n<T>(1,2)*z[28] + z[29] + z[30] + z[31] + z[32] + n<T>(1,24)*z[33] + 
      z[34];
 
    return r;
}

template std::complex<double> qqb_2lha_tf728(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf728(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
