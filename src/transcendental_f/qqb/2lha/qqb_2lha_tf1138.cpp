#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1138(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[112];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[19];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=c[30];
    z[39]=c[31];
    z[40]=e[3];
    z[41]=e[6];
    z[42]=e[14];
    z[43]=f[0];
    z[44]=f[1];
    z[45]=f[2];
    z[46]=f[3];
    z[47]=f[4];
    z[48]=f[5];
    z[49]=f[6];
    z[50]=f[7];
    z[51]=f[8];
    z[52]=f[9];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[13];
    z[56]=f[14];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[18];
    z[60]=f[21];
    z[61]=f[23];
    z[62]=f[30];
    z[63]=f[31];
    z[64]=f[32];
    z[65]=f[33];
    z[66]=f[34];
    z[67]=f[35];
    z[68]=f[36];
    z[69]=f[37];
    z[70]=f[39];
    z[71]=f[41];
    z[72]=f[43];
    z[73]=f[46];
    z[74]=f[47];
    z[75]=f[50];
    z[76]=f[51];
    z[77]=f[52];
    z[78]=f[53];
    z[79]=f[54];
    z[80]=f[55];
    z[81]=f[56];
    z[82]=f[58];
    z[83]=f[60];
    z[84]=f[85];
    z[85]=f[86];
    z[86]=43*z[5];
    z[87]=z[1]*i;
    z[88]=2*z[87];
    z[89]=z[88] - z[5];
    z[89]=z[89]*z[86];
    z[90]=2*z[8];
    z[91]= - n<T>(215,3)*z[8] - n<T>(13,3)*z[87] - 32*z[5];
    z[91]=z[91]*z[90];
    z[89]=z[89] + z[91];
    z[91]=z[6] - z[5];
    z[92]=n<T>(17,3)*z[2];
    z[91]= - z[92] - z[9] + n<T>(65,3)*z[8] - 21*z[87] - n<T>(32,3)*z[91];
    z[91]=z[2]*z[91];
    z[93]=z[88] + z[6];
    z[94]=7*z[8];
    z[95]=7*z[9];
    z[96]=z[95] - z[94] + 5*z[5] + z[93];
    z[96]=n<T>(1,3)*z[96] - z[10];
    z[97]=2*z[10];
    z[96]=z[96]*z[97];
    z[98]= - n<T>(43,3)*z[8] + n<T>(34,3)*z[87] + 19*z[5];
    z[98]=n<T>(295,3)*z[6] + 2*z[98] - z[95];
    z[99]=n<T>(2,3)*z[6];
    z[98]=z[98]*z[99];
    z[86]= - 62*z[2] + 217*z[6] - n<T>(74,3)*z[8] - n<T>(299,3)*z[87] - z[86];
    z[86]= - n<T>(328,9)*z[7] + n<T>(1,3)*z[86] - z[97];
    z[86]=z[7]*z[86];
    z[97]=z[9]*z[90];
    z[86]=z[86] + z[96] + 2*z[91] + z[98] + n<T>(1,3)*z[89] + z[97];
    z[86]=z[7]*z[86];
    z[89]= - z[87] + n<T>(1,2)*z[5];
    z[89]=z[89]*z[5];
    z[91]= - z[87] + n<T>(1,2)*z[8];
    z[96]=z[8]*z[91];
    z[96]= - 79*z[89] + 13*z[96];
    z[97]=n<T>(1,2)*z[9];
    z[98]=z[87] - z[5];
    z[100]= - z[97] - z[98];
    z[100]=z[9]*z[100];
    z[101]=n<T>(1,2)*z[6];
    z[102]=z[87] - z[8];
    z[103]=z[101] + z[102];
    z[103]=z[6]*z[103];
    z[104]= - n<T>(85,3)*z[102] - 14*z[7];
    z[104]=z[7]*z[104];
    z[105]=2*z[98] + z[3];
    z[105]=z[3]*z[105];
    z[106]=z[4]*z[87];
    z[96]=34*z[106] + n<T>(112,3)*z[105] + 2*z[104] + 61*z[103] + n<T>(1,3)*
    z[96] + 101*z[100];
    z[92]=4*z[11] - n<T>(17,3)*z[4] + n<T>(191,9)*z[3] - n<T>(85,9)*z[7] + z[92]
     + 
   n<T>(61,6)*z[6] - n<T>(101,6)*z[9] - n<T>(11,18)*z[8] + 5*z[87] - n<T>(79,18)*z[5];
    z[92]=z[11]*z[92];
    z[92]=n<T>(1,3)*z[96] + z[92];
    z[92]=z[11]*z[92];
    z[96]=10039*z[87] - n<T>(7063,2)*z[5];
    z[96]=z[5]*z[96];
    z[100]=7*z[87];
    z[103]= - n<T>(137,3)*z[8] + z[100] - n<T>(9895,72)*z[5];
    z[103]=z[8]*z[103];
    z[96]=n<T>(1,36)*z[96] + z[103];
    z[96]=z[8]*z[96];
    z[103]=4*z[5];
    z[104]= - 4*z[8] + n<T>(1855,9)*z[87] + z[103];
    z[105]=z[4] + z[2];
    z[104]=2*z[104] - n<T>(1855,9)*z[105];
    z[104]=z[18]*z[104];
    z[105]=8*z[87];
    z[106]= - z[105] + n<T>(25,3)*z[5];
    z[106]=z[106]*npow(z[5],2);
    z[107]= - 2183*z[5] - n<T>(161,3)*z[8];
    z[107]=n<T>(1,8)*z[107] + 2359*z[9];
    z[107]=n<T>(155,12)*z[11] + n<T>(15113,216)*z[4] - n<T>(50,3)*z[3] + n<T>(44,3)*z[7]
    + n<T>(17,18)*z[10] + n<T>(2545,36)*z[2] + n<T>(1,9)*z[107] - n<T>(799,4)*z[6];
    z[107]=z[30]*z[107];
    z[108]=n<T>(1645,3)*z[87] - 139*z[5];
    z[108]=n<T>(1,2)*z[108] - n<T>(614,3)*z[2];
    z[108]=z[19]*z[108];
    z[96]=z[104] + z[107] + 4*z[106] + z[96] + z[108] + z[74];
    z[104]=7*z[5];
    z[106]= - 22*z[87] + z[104];
    z[107]=n<T>(1,3)*z[5];
    z[106]=z[106]*z[107];
    z[108]=n<T>(895,18)*z[8] - n<T>(823,9)*z[87] + 28*z[5];
    z[108]=z[8]*z[108];
    z[109]=n<T>(5,12)*z[9] + n<T>(14,3)*z[8] - n<T>(9,2)*z[87] - n<T>(4,3)*z[5];
    z[109]=z[9]*z[109];
    z[110]= - n<T>(437,2)*z[6] + n<T>(29,4)*z[9] + n<T>(271,6)*z[8] - n<T>(175,4)*z[87]
    - 109*z[5];
    z[110]=z[6]*z[110];
    z[106]=n<T>(1,3)*z[110] + z[109] + z[106] + z[108];
    z[106]=z[6]*z[106];
    z[108]=2*z[5];
    z[109]=z[88] + z[5];
    z[109]=z[109]*z[108];
    z[110]=13*z[87];
    z[111]= - z[110] - z[103];
    z[111]=2*z[111] + 17*z[8];
    z[111]=z[8]*z[111];
    z[103]=n<T>(41,6)*z[9] - 14*z[8] + n<T>(17,3)*z[87] + z[103];
    z[103]=z[9]*z[103];
    z[103]=z[103] + z[109] + z[111];
    z[101]=z[101] + 11*z[9] + z[8] + z[110] - z[108];
    z[101]=n<T>(1,3)*z[101] + n<T>(5,2)*z[10];
    z[101]=z[10]*z[101];
    z[104]=11*z[8] - n<T>(20,3)*z[87] - z[104];
    z[104]=n<T>(1,3)*z[104] - 3*z[9];
    z[104]=2*z[104] - n<T>(25,18)*z[6];
    z[104]=z[6]*z[104];
    z[108]=z[88] - z[2];
    z[108]=z[2]*z[108];
    z[101]=z[101] + 4*z[108] + n<T>(1,3)*z[103] + z[104];
    z[101]=z[10]*z[101];
    z[103]=n<T>(334,3)*z[9] - n<T>(365,3)*z[98] + 62*z[8];
    z[99]=z[99] + n<T>(1,3)*z[103];
    z[99]=z[9]*z[99];
    z[100]= - z[100] + n<T>(29,3)*z[5];
    z[94]=2*z[100] + z[94];
    z[94]=z[8]*z[94];
    z[100]=n<T>(272,9)*z[5] - 22 - n<T>(59,9)*z[87];
    z[100]=z[5]*z[100];
    z[94]=z[100] + z[94] + z[99];
    z[99]=z[8] + z[5];
    z[93]= - z[9] + z[99] - z[93];
    z[93]=z[10]*z[93];
    z[100]=2 + n<T>(5,3)*z[87];
    z[100]= - n<T>(62,3)*z[9] - n<T>(58,3)*z[8] + 11*z[100] - n<T>(74,3)*z[5];
    z[100]=2*z[100] + n<T>(77,3)*z[2];
    z[100]=z[2]*z[100];
    z[103]=662*z[87] - 199*z[5];
    z[103]= - n<T>(365,3)*z[9] + n<T>(1,3)*z[103] - 64*z[8];
    z[103]= - n<T>(383,9)*z[3] + n<T>(1,3)*z[103] + 67*z[2];
    z[103]=z[3]*z[103];
    z[93]=z[103] + n<T>(4,3)*z[93] + 2*z[94] + z[100];
    z[93]=z[3]*z[93];
    z[88]=z[88] - 31*z[5];
    z[88]=n<T>(2,3)*z[88] - z[8];
    z[88]=z[88]*z[90];
    z[90]=n<T>(2113,2)*z[87] - 1033*z[5];
    z[90]= - n<T>(449,6)*z[9] + n<T>(1,6)*z[90] + 67*z[8];
    z[90]=z[9]*z[90];
    z[88]=n<T>(1,3)*z[90] - n<T>(1033,9)*z[89] + z[88];
    z[88]=z[9]*z[88];
    z[89]= - n<T>(2335,6)*z[87] + 151*z[5];
    z[89]=z[5]*z[89];
    z[90]=n<T>(5353,2)*z[8] - 6001*z[87] - n<T>(2011,2)*z[5];
    z[90]=z[8]*z[90];
    z[94]=n<T>(3307,18)*z[8] + n<T>(9193,6)*z[87] + 413*z[5];
    z[94]= - n<T>(781,2)*z[2] + n<T>(1,2)*z[94] - z[6];
    z[94]=z[2]*z[94];
    z[89]=z[94] + z[89] + n<T>(1,9)*z[90];
    z[90]= - 65*z[87] + 62*z[5];
    z[90]=n<T>(1,3)*z[90] + z[8];
    z[90]=2*z[90] - n<T>(67,3)*z[9];
    z[90]=z[9]*z[90];
    z[94]=n<T>(56,3)*z[6] + n<T>(55,3)*z[87] - 18*z[8];
    z[94]=z[6]*z[94];
    z[89]=2*z[94] + z[90] + n<T>(1,3)*z[89];
    z[89]=z[2]*z[89];
    z[90]=5555*z[87] - n<T>(3233,2)*z[5];
    z[90]=z[90]*z[107];
    z[91]=z[5] - z[91];
    z[91]=z[8]*z[91];
    z[90]=z[90] + 3647*z[91];
    z[91]=z[87] - z[97];
    z[91]=z[9]*z[91];
    z[90]=n<T>(1,2)*z[90] + 47*z[91];
    z[91]= - n<T>(53,6)*z[6] - n<T>(19,3)*z[87] + z[95];
    z[91]=z[6]*z[91];
    z[90]=n<T>(1,3)*z[90] + z[91];
    z[91]=n<T>(1927,2)*z[2] - 4124*z[87] - 1927*z[5];
    z[91]=z[2]*z[91];
    z[94]=n<T>(19,2)*z[6] - n<T>(47,2)*z[9] - n<T>(3647,4)*z[8] + 131*z[87] - n<T>(5555,12)*z[5];
    z[94]= - n<T>(29,2)*z[10] + n<T>(1,2)*z[94] + n<T>(2062,3)*z[2];
    z[94]=n<T>(53,6)*z[4] + 11*z[3] + n<T>(1,3)*z[94] - 28*z[7];
    z[94]=z[4]*z[94];
    z[90]=z[94] + n<T>(1,2)*z[90] + n<T>(1,9)*z[91];
    z[91]= - n<T>(22,3)*z[3] + n<T>(56,3)*z[7];
    z[91]=z[87]*z[91];
    z[94]= - 4*z[10] + n<T>(16,9)*z[6] + z[105] + n<T>(13,9)*z[9];
    z[94]=z[10]*z[94];
    z[90]=z[94] + z[91] + n<T>(1,3)*z[90];
    z[90]=z[4]*z[90];
    z[91]= - z[2] + z[99] - z[87];
    z[94]= - n<T>(583,9)*z[7] - n<T>(835,9)*z[6] - 28*z[91];
    z[94]=z[23]*z[94];
    z[94]= - z[84] + z[94] + z[85];
    z[95]=z[8] + z[9];
    z[95]=z[87]*z[95];
    z[95]=z[30] + z[95];
    z[95]=z[17]*z[95];
    z[97]= - z[9] + z[102];
    z[97]=z[27]*z[97];
    z[95]=z[95] + z[97];
    z[97]=z[8] + z[7];
    z[97]=z[87]*z[97];
    z[97]=z[30] + z[97];
    z[97]=z[16]*z[97];
    z[99]=z[7] - z[102];
    z[99]=z[25]*z[99];
    z[97]=z[97] + z[99];
    z[99]= - z[5] - z[6];
    z[99]=z[87]*z[99];
    z[99]= - z[30] + z[99];
    z[99]=z[15]*z[99];
    z[100]=z[6] - z[98];
    z[100]=z[22]*z[100];
    z[99]=z[99] + z[100];
    z[100]=z[11] + z[4];
    z[100]= - z[87]*z[100];
    z[100]= - z[30] + z[100];
    z[100]=z[14]*z[100];
    z[103]=z[87] - z[4];
    z[104]=z[11] - z[103];
    z[104]=z[21]*z[104];
    z[100]=z[100] + z[104];
    z[104]=z[4] + z[10];
    z[104]= - z[87]*z[104];
    z[104]= - z[30] + z[104];
    z[104]=z[13]*z[104];
    z[103]=z[10] - z[103];
    z[103]=z[28]*z[103];
    z[103]=z[104] + z[103];
    z[104]= - z[5] - z[3];
    z[104]=z[87]*z[104];
    z[104]= - z[30] + z[104];
    z[104]=z[12]*z[104];
    z[98]= - z[3] + z[98];
    z[98]=z[20]*z[98];
    z[98]=z[104] + z[98];
    z[91]= - n<T>(715,3)*z[3] - n<T>(1033,3)*z[9] - 106*z[91];
    z[91]=z[26]*z[91];
    z[91]=z[91] + z[77];
    z[104]=z[3] - z[6];
    z[87]= - n<T>(7195,6)*z[8] + n<T>(6817,3)*z[87] + 116*z[5];
    z[87]= - 12*z[4] - n<T>(10,3)*z[7] - n<T>(6487,54)*z[2] + n<T>(1,9)*z[87] + 4*
    z[104];
    z[87]=z[24]*z[87];
    z[104]=z[81] + z[56];
    z[105]= - n<T>(8723,18)*z[36] - n<T>(8785,36)*z[34] - n<T>(323,108)*z[31];
    z[105]=i*z[105];
    z[102]=z[9] + z[102];
    z[102]=2*z[102] + n<T>(13,9)*z[6];
    z[102]=4*z[7] + 2*z[102] + n<T>(17,9)*z[10];
    z[102]=z[29]*z[102];
    z[107]= - z[3] - z[11];
    z[107]=z[40]*z[107];
    z[108]= - z[7] - z[11];
    z[108]=z[41]*z[108];
    z[109]= - z[9] - z[10];
    z[109]=z[42]*z[109];

    r += n<T>(193,9)*z[32] - n<T>(3157,144)*z[33] + n<T>(26057,216)*z[35] + n<T>(8291,36)*z[37]
     - n<T>(415,18)*z[38]
     + n<T>(7063,108)*z[39]
     + n<T>(25,3)*z[43] - 6*
      z[44] - n<T>(224,9)*z[45] - n<T>(4691,216)*z[46] - n<T>(1669,18)*z[47] - n<T>(1297,54)*z[48]
     - n<T>(148,3)*z[49]
     - n<T>(848,3)*z[50]
     - n<T>(44,9)*z[51]
     + 13*
      z[52] - n<T>(10327,216)*z[53] + n<T>(79,18)*z[54] - n<T>(22865,216)*z[55] - 
      n<T>(101,6)*z[57] - n<T>(4175,72)*z[58] + n<T>(31,9)*z[59] + n<T>(22,3)*z[60]
     +  n<T>(106,3)*z[61] + n<T>(170,9)*z[62] + n<T>(1405,18)*z[63] + n<T>(128,3)*z[64]
       + 224*z[65] - 18*z[66] + n<T>(406,9)*z[67] + n<T>(29,18)*z[68] + n<T>(29,3)*
      z[69] + n<T>(53,6)*z[70] - n<T>(56,3)*z[71] - n<T>(86,3)*z[72] + z[73] + n<T>(13,9)*z[75]
     - n<T>(125,36)*z[76]
     - 10*z[78]
     - n<T>(16,9)*z[79]
     + n<T>(47,36)*
      z[80] + n<T>(5,12)*z[82] + 8*z[83] + z[86] + z[87] + z[88] + z[89] + 
      z[90] + n<T>(2,3)*z[91] + z[92] + z[93] + 2*z[94] + n<T>(122,3)*z[95] + n<T>(1,3)*z[96]
     + n<T>(688,9)*z[97]
     + n<T>(874,9)*z[98]
     + n<T>(76,3)*z[99]
     + n<T>(34,3)
      *z[100] + z[101] + z[102] + n<T>(43,9)*z[103] - n<T>(11,3)*z[104] + 
      z[105] + z[106] + n<T>(158,9)*z[107] + n<T>(2,9)*z[108] + n<T>(25,9)*z[109];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1138(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1138(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
