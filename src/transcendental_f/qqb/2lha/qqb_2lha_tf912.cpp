#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf912(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[39];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[4];
    z[7]=d[16];
    z[8]=e[0];
    z[9]=e[1];
    z[10]=e[4];
    z[11]=e[8];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[19];
    z[16]=c[21];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[31];
    z[22]=f[0];
    z[23]=f[1];
    z[24]=f[3];
    z[25]=f[5];
    z[26]=f[11];
    z[27]=f[17];
    z[28]=f[18];
    z[29]=3*z[4];
    z[30]= - z[5] + n<T>(1,2)*z[4];
    z[30]=z[30]*z[29];
    z[31]=i*z[1];
    z[29]= - z[6] + z[29] + 3*z[5];
    z[32]= - z[7] - z[29];
    z[32]=z[32]*z[31];
    z[29]=z[3] - z[31] + z[29];
    z[33]=3*z[2];
    z[29]= - z[33] + n<T>(1,2)*z[29];
    z[29]=z[3]*z[29];
    z[34]=5*z[8];
    z[35]=npow(z[6],2);
    z[36]=npow(z[5],2);
    z[37]= - n<T>(1,2)*z[2] + z[6] + 7*z[31];
    z[37]=z[2]*z[37];
    z[29]=z[29] + z[37] + z[32] + z[30] + n<T>(3,2)*z[36] - z[35] - n<T>(1,2)*
    z[12] + z[10] + z[34];
    z[29]=z[3]*z[29];
    z[30]=z[9] + z[11];
    z[32]=z[5] - z[4];
    z[32]=z[4]*z[32];
    z[32]= - z[30] + z[36] - z[32];
    z[37]=z[4] + z[5];
    z[33]=z[33] - n<T>(13,2)*z[31] - n<T>(1,2)*z[6] - z[37];
    z[33]=z[2]*z[33];
    z[31]=z[37]*z[31];
    z[31]=z[33] + 4*z[31] + z[35] + n<T>(5,6)*z[12] + z[34] - 2*z[32];
    z[31]=z[2]*z[31];
    z[32]=z[4]*z[5];
    z[33]= - z[6]*z[7];
    z[30]= - z[32] + z[33] - 10*z[8] - z[10] - 4*z[30];
    z[30]=z[1]*z[30];
    z[30]=z[30] - n<T>(1,6)*z[13] - z[16] + 19*z[19] + 7*z[17];
    z[30]=i*z[30];
    z[32]=z[32] + z[36];
    z[33]=n<T>(1,6)*z[12];
    z[32]=2*z[9] - z[33] + n<T>(1,2)*z[32];
    z[32]=z[4]*z[32];
    z[34]=z[24] + z[27];
    z[36]=z[18] - z[22];
    z[37]=z[15] - z[26];
    z[38]= - z[12]*z[7];
    z[35]=z[35] + z[10] - n<T>(5,12)*z[12];
    z[35]=z[6]*z[35];
    z[33]=2*z[11] - z[33];
    z[33]=z[5]*z[33];

    r += z[14] - 9*z[20] - n<T>(7,6)*z[21] - z[23] + n<T>(5,2)*z[25] + z[28] + 
      z[29] + z[30] + z[31] + z[32] + z[33] + n<T>(3,2)*z[34] + z[35] - 3*
      z[36] - n<T>(1,2)*z[37] + z[38];
 
    return r;
}

template std::complex<double> qqb_2lha_tf912(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf912(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
