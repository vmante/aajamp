#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1132(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[115];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[19];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=c[30];
    z[39]=c[31];
    z[40]=e[3];
    z[41]=e[6];
    z[42]=e[14];
    z[43]=f[0];
    z[44]=f[1];
    z[45]=f[2];
    z[46]=f[3];
    z[47]=f[4];
    z[48]=f[5];
    z[49]=f[6];
    z[50]=f[7];
    z[51]=f[8];
    z[52]=f[9];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[13];
    z[56]=f[14];
    z[57]=f[16];
    z[58]=f[17];
    z[59]=f[18];
    z[60]=f[21];
    z[61]=f[23];
    z[62]=f[30];
    z[63]=f[31];
    z[64]=f[32];
    z[65]=f[33];
    z[66]=f[34];
    z[67]=f[35];
    z[68]=f[36];
    z[69]=f[37];
    z[70]=f[39];
    z[71]=f[41];
    z[72]=f[43];
    z[73]=f[50];
    z[74]=f[51];
    z[75]=f[52];
    z[76]=f[53];
    z[77]=f[54];
    z[78]=f[55];
    z[79]=f[56];
    z[80]=f[57];
    z[81]=f[58];
    z[82]=f[60];
    z[83]=f[90];
    z[84]=f[91];
    z[85]=n<T>(1,2)*z[8];
    z[86]=z[1]*i;
    z[87]=z[85] - z[86];
    z[88]=n<T>(1,2)*z[2];
    z[89]=n<T>(1,2)*z[5];
    z[90]= - z[88] + z[89] - z[87];
    z[90]=z[2]*z[90];
    z[91]=z[9] - z[8];
    z[92]=z[2] - z[5];
    z[93]= - n<T>(21,8)*z[6] - n<T>(13,4)*z[86] - z[92] - z[91];
    z[94]=n<T>(1,2)*z[6];
    z[93]=z[93]*z[94];
    z[95]=z[89] - z[86];
    z[96]=z[95]*z[5];
    z[87]=z[87]*z[8];
    z[97]=n<T>(1,2)*z[9];
    z[98]=z[86] - 3*z[8];
    z[98]=n<T>(5,8)*z[9] + n<T>(1,2)*z[98] + z[2];
    z[98]=z[98]*z[97];
    z[99]= - n<T>(9,16)*z[4] + n<T>(9,8)*z[6] + z[85] - z[95];
    z[99]=z[4]*z[99];
    z[100]=z[10] + z[9];
    z[101]=7*z[86];
    z[102]=z[101] + z[8];
    z[100]= - z[4] - z[6] + n<T>(1,4)*z[102] - z[5] + n<T>(13,8)*z[100];
    z[102]=n<T>(1,2)*z[10];
    z[100]=z[100]*z[102];
    z[90]=z[100] + z[99] + z[93] + z[98] + z[90] + n<T>(5,4)*z[87] - z[96];
    z[90]=z[10]*z[90];
    z[93]=n<T>(5,2)*z[5];
    z[98]=2*z[2];
    z[99]=n<T>(1,2)*z[86];
    z[100]=z[98] - z[93] - z[99] - z[8];
    z[100]=z[2]*z[100];
    z[103]=n<T>(5,2)*z[8];
    z[95]=z[103] + z[95];
    z[104]=n<T>(1,3)*z[5];
    z[95]=z[95]*z[104];
    z[105]=z[2] - z[8];
    z[105]= - z[105]*z[97];
    z[106]= - n<T>(5,3)*z[92];
    z[107]=z[86] - z[8];
    z[108]=z[107] - z[9];
    z[109]=n<T>(1,6)*z[6] + z[108] - z[106];
    z[109]=z[109]*z[94];
    z[110]=n<T>(1,2)*z[4];
    z[111]=z[110] - z[86];
    z[110]= - z[111]*z[110];
    z[91]= - z[6] + z[5] + z[91];
    z[91]=z[91]*z[102];
    z[112]=n<T>(7,6)*z[86];
    z[113]= - z[112] + z[8];
    z[113]=z[8]*z[113];
    z[114]=n<T>(2,9)*z[7] - n<T>(5,6)*z[6] + n<T>(7,12)*z[2] + n<T>(1,6)*z[5] + n<T>(1,4)*
    z[86] + n<T>(1,3)*z[8];
    z[114]=z[7]*z[114];
    z[91]=z[114] + z[91] + z[110] + z[109] + z[105] + n<T>(1,3)*z[100] + 
    z[113] + z[95];
    z[91]=z[7]*z[91];
    z[95]= - 11 + n<T>(35,12)*z[8];
    z[100]=n<T>(25,4)*z[5] + z[112] + z[95];
    z[100]=z[5]*z[100];
    z[95]=n<T>(4,3)*z[2] - n<T>(77,12)*z[5] + n<T>(20,3)*z[86] - z[95];
    z[95]=z[2]*z[95];
    z[105]=z[4]*z[111];
    z[109]= - z[102] - z[107];
    z[102]=z[109]*z[102];
    z[109]=3*z[5];
    z[110]=26*z[86] - n<T>(53,8)*z[8];
    z[110]= - n<T>(83,36)*z[3] - n<T>(1,4)*z[10] - n<T>(133,24)*z[9] + n<T>(161,24)*z[2]
    + n<T>(1,3)*z[110] - z[109];
    z[110]=z[3]*z[110];
    z[112]=n<T>(175,2)*z[9] - 53*z[2] + 139*z[5] - 133*z[86] + 47*z[8];
    z[112]=z[9]*z[112];
    z[95]=z[110] + z[102] + n<T>(11,4)*z[105] + n<T>(1,12)*z[112] + z[95] + n<T>(47,12)*z[87]
     + z[100];
    z[95]=z[3]*z[95];
    z[100]=z[4] - z[2];
    z[102]=n<T>(7,4)*z[9];
    z[105]=n<T>(5,4)*z[6];
    z[110]=z[86] - n<T>(7,3)*z[8];
    z[100]= - z[105] - z[102] + n<T>(1,4)*z[110] + z[104] + n<T>(9,4)*z[100];
    z[100]= - n<T>(1,6)*z[11] + n<T>(17,24)*z[3] + n<T>(1,2)*z[100] + n<T>(1,3)*z[7];
    z[100]=z[11]*z[100];
    z[96]= - n<T>(7,4)*z[87] + z[96];
    z[104]=z[86] - z[5];
    z[110]= - z[97] - z[104];
    z[102]=z[110]*z[102];
    z[110]= - z[94] - z[107];
    z[105]=z[110]*z[105];
    z[110]=z[86]*z[4];
    z[112]=n<T>(2,3)*z[107] - n<T>(1,4)*z[7];
    z[112]=z[7]*z[112];
    z[113]=n<T>(1,2)*z[3] + z[104];
    z[113]=z[3]*z[113];
    z[96]=z[100] + n<T>(25,12)*z[113] + z[112] - n<T>(9,4)*z[110] + z[105] + n<T>(1,3)*z[96]
     + z[102];
    z[96]=z[11]*z[96];
    z[100]= - n<T>(77,2)*z[5] + 77*z[86] - n<T>(47,2)*z[8];
    z[100]=z[100]*z[89];
    z[102]=n<T>(7,2)*z[5] - 5*z[86] + z[85];
    z[102]=n<T>(5,2)*z[102] + z[98];
    z[102]=z[2]*z[102];
    z[105]=z[99] - z[8];
    z[105]=z[8]*z[105];
    z[112]=n<T>(583,16)*z[86] + 13*z[8];
    z[112]= - n<T>(523,48)*z[9] - n<T>(49,8)*z[2] + n<T>(1,2)*z[112] - 20*z[5];
    z[112]=z[9]*z[112];
    z[100]=z[112] + z[102] + z[105] + z[100];
    z[100]=z[9]*z[100];
    z[102]=z[107] + z[92];
    z[105]=n<T>(13,2)*z[7] - 2*z[102] + n<T>(17,2)*z[6];
    z[105]=z[23]*z[105];
    z[102]= - 52*z[3] + 25*z[102] - 77*z[9];
    z[102]=z[26]*z[102];
    z[100]=z[72] + z[102] + z[100] + z[105];
    z[93]=z[93] - 2*z[86] + z[85];
    z[93]=z[5]*z[93];
    z[102]=n<T>(5,2)*z[2] - 5*z[5] - z[86] - z[85];
    z[102]=z[2]*z[102];
    z[93]=z[102] - n<T>(17,4)*z[87] + z[93];
    z[102]=n<T>(7,16)*z[9] + n<T>(7,8)*z[86] + z[8];
    z[97]=z[102]*z[97];
    z[102]=n<T>(31,4)*z[86] - 7*z[8];
    z[102]=n<T>(271,144)*z[6] + n<T>(23,32)*z[9] + n<T>(1,8)*z[102] + z[106];
    z[102]=z[6]*z[102];
    z[93]=z[102] + n<T>(1,3)*z[93] + z[97];
    z[93]=z[6]*z[93];
    z[97]=n<T>(9,2)*z[5];
    z[102]=z[97] + 55*z[86] + 27*z[8];
    z[102]=z[5]*z[102];
    z[102]= - 27*z[87] + z[102];
    z[105]=n<T>(9,2)*z[2] - n<T>(41,2)*z[86] - 9*z[5];
    z[105]=z[2]*z[105];
    z[102]=n<T>(1,4)*z[102] + z[105];
    z[105]= - n<T>(23,8)*z[86] - z[8];
    z[105]=n<T>(15,32)*z[9] - z[88] + n<T>(1,2)*z[105] + z[5];
    z[105]=z[9]*z[105];
    z[92]= - n<T>(3,16)*z[6] - n<T>(15,8)*z[9] + n<T>(3,8)*z[86] + z[92];
    z[92]=z[92]*z[94];
    z[94]= - 23*z[86] - 9*z[8];
    z[94]=3*z[94] - 55*z[5];
    z[94]=n<T>(31,6)*z[4] - n<T>(3,4)*z[6] + n<T>(23,4)*z[9] + n<T>(1,2)*z[94] + 41*z[2]
   ;
    z[94]=z[4]*z[94];
    z[92]=n<T>(1,8)*z[94] + z[92] + n<T>(1,2)*z[102] + z[105];
    z[92]=z[4]*z[92];
    z[94]= - z[109] + z[86] - n<T>(37,4)*z[8];
    z[94]=z[5]*z[94];
    z[87]= - n<T>(25,2)*z[87] + z[94];
    z[87]=z[5]*z[87];
    z[94]= - 7*z[9] - n<T>(11,2)*z[10];
    z[94]=z[42]*z[94];
    z[87]= - z[84] - z[87] - z[94] + z[83] - z[69];
    z[94]=n<T>(11,2)*z[5] - 31*z[86] + z[103];
    z[89]=z[94]*z[89];
    z[85]=9*z[86] + z[85];
    z[85]= - n<T>(121,3)*z[2] + 9*z[85] + n<T>(53,2)*z[5];
    z[85]=z[2]*z[85];
    z[94]= - 10*z[86] + n<T>(13,4)*z[8];
    z[94]=z[8]*z[94];
    z[85]=n<T>(1,4)*z[85] + z[94] + z[89];
    z[85]=z[2]*z[85];
    z[89]=3*z[9] - n<T>(23,2)*z[2] + z[101] + z[97];
    z[89]=z[19]*z[89];
    z[89]= - z[89] - z[75] - z[73] + z[71] + z[64] - z[48];
    z[94]=z[86]*z[8];
    z[94]=z[94] + z[30];
    z[97]=z[9]*z[86];
    z[97]=z[97] + z[94];
    z[97]=z[17]*z[97];
    z[101]=z[27]*z[108];
    z[97]= - z[38] + z[97] + z[101];
    z[101]=z[110] + z[30];
    z[102]=z[11]*z[86];
    z[102]=z[102] + z[101];
    z[102]=z[14]*z[102];
    z[103]=z[86] - z[4];
    z[105]= - z[11] + z[103];
    z[105]=z[21]*z[105];
    z[102]=z[102] + z[105] + z[52] - z[43];
    z[105]= - z[7]*z[86];
    z[94]=z[105] - z[94];
    z[94]=z[16]*z[94];
    z[105]= - z[7] + z[107];
    z[105]=z[25]*z[105];
    z[94]=z[94] + z[105];
    z[105]=z[10]*z[86];
    z[101]=z[105] + z[101];
    z[101]=z[13]*z[101];
    z[103]= - z[10] + z[103];
    z[103]=z[28]*z[103];
    z[101]=z[101] + z[103];
    z[103]=z[86]*z[5];
    z[103]=z[103] + z[30];
    z[105]= - z[3]*z[86];
    z[105]=z[105] - z[103];
    z[105]=z[12]*z[105];
    z[106]= - z[3] + z[104];
    z[106]=z[20]*z[106];
    z[105]=z[105] + z[106];
    z[106]= - n<T>(19,8)*z[8] - 4*z[5];
    z[98]=n<T>(1,3)*z[106] + z[98];
    z[98]= - n<T>(443,32)*z[4] + n<T>(31,8)*z[6] + 5*z[98] + 28*z[9];
    z[98]=n<T>(17,48)*z[11] - n<T>(5,36)*z[3] - n<T>(5,18)*z[7] + n<T>(1,3)*z[98]
     + n<T>(11,16)*z[10];
    z[98]=z[30]*z[98];
    z[106]=z[6]*z[86];
    z[103]=z[106] + z[103];
    z[103]=z[15]*z[103];
    z[88]= - z[88] - z[111];
    z[88]=z[18]*z[88];
    z[106]=z[67] - z[54];
    z[108]=z[77] + z[70];
    z[109]=z[80] + z[59];
    z[110]=z[81] - z[78];
    z[111]=z[3] + z[11];
    z[111]=z[40]*z[111];
    z[111]=z[111] - z[62];
    z[112]= - 22*z[36] - n<T>(61,4)*z[34] - n<T>(155,96)*z[31];
    z[112]=i*z[112];
    z[99]=z[99] - n<T>(5,3)*z[8];
    z[99]=z[99]*npow(z[8],2);
    z[86]= - z[3] + n<T>(1,2)*z[7] - n<T>(19,4)*z[2] + z[5] + 8*z[86] - n<T>(17,4)*
    z[8];
    z[86]=z[24]*z[86];
    z[107]=z[7] + n<T>(3,8)*z[10] + n<T>(11,8)*z[6] + z[9] + z[107];
    z[107]=z[29]*z[107];
    z[104]= - z[6] + z[104];
    z[104]=z[22]*z[104];
    z[113]=z[7] + z[11];
    z[113]=z[41]*z[113];

    r += n<T>(29,8)*z[32] - n<T>(223,48)*z[33] + n<T>(179,24)*z[35] + n<T>(19,2)*z[37]
       + n<T>(965,36)*z[39] + n<T>(9,8)*z[44] - n<T>(25,12)*z[45] - n<T>(71,16)*z[46]
       - n<T>(31,3)*z[47] - n<T>(49,8)*z[49] - n<T>(100,3)*z[50] + 3*z[51] - n<T>(29,16)
      *z[53] - n<T>(75,8)*z[55] - n<T>(11,8)*z[56] - n<T>(7,8)*z[57] - n<T>(19,16)*
      z[58] + n<T>(11,4)*z[60] + n<T>(25,6)*z[61] - n<T>(25,24)*z[63] - n<T>(8,3)*z[65]
       - z[66] + n<T>(7,24)*z[68] - n<T>(3,32)*z[74] - 4*z[76] - n<T>(3,8)*z[79]
     +  z[82] + z[85] + z[86] - n<T>(1,4)*z[87] + n<T>(23,2)*z[88] - n<T>(1,2)*z[89]
       + z[90] + z[91] + z[92] + z[93] + n<T>(4,3)*z[94] + z[95] + z[96] + 
      n<T>(9,2)*z[97] + z[98] + z[99] + n<T>(1,3)*z[100] + n<T>(1,8)*z[101] + n<T>(9,4)
      *z[102] + z[103] + z[104] + n<T>(61,6)*z[105] + n<T>(1,6)*z[106] + z[107]
       - n<T>(5,8)*z[108] - n<T>(3,4)*z[109] + n<T>(7,32)*z[110] + n<T>(2,3)*z[111]
     +  z[112] + n<T>(7,6)*z[113];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1132(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1132(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
