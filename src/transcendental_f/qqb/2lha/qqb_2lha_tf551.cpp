#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf551(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[36];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[15];
    z[11]=c[18];
    z[12]=c[19];
    z[13]=c[23];
    z[14]=c[25];
    z[15]=c[26];
    z[16]=c[28];
    z[17]=c[29];
    z[18]=c[31];
    z[19]=f[0];
    z[20]=f[3];
    z[21]=f[5];
    z[22]=f[8];
    z[23]=f[11];
    z[24]=f[17];
    z[25]=f[20];
    z[26]=i*z[1];
    z[27]=2*z[26];
    z[28]=z[27] - z[5];
    z[29]=z[28]*z[5];
    z[27]=z[27] - z[3];
    z[27]=z[3]*z[27];
    z[27]=z[29] + z[27];
    z[30]=z[3] + z[5];
    z[31]=z[26] + z[30];
    z[31]=2*z[31] - z[4];
    z[32]=2*z[4];
    z[31]=z[31]*z[32];
    z[33]=7*z[6];
    z[34]= - z[33] + n<T>(5,6)*z[9];
    z[35]=7*z[8];
    z[27]=z[31] + z[35] + 5*z[7] - z[34] + 4*z[27];
    z[31]= - z[32] - 5*z[26] - z[30];
    z[31]=n<T>(1,3)*z[31] + z[2];
    z[31]=z[2]*z[31];
    z[27]=n<T>(1,3)*z[27] + 2*z[31];
    z[27]=z[2]*z[27];
    z[31]=2*z[7];
    z[32]= - z[31] - z[35] - z[33];
    z[32]=z[1]*z[32];
    z[32]=z[32] + 16*z[15] + 5*z[13];
    z[32]=i*z[32];
    z[32]=z[32] - z[11];
    z[30]=z[4]*z[30];
    z[29]= - z[30] + z[29] + n<T>(1,6)*z[9] - z[31];
    z[26]= - n<T>(1,3)*z[26] - z[5];
    z[26]=2*z[26] + n<T>(1,3)*z[3];
    z[26]=z[3]*z[26];
    z[26]=z[26] - z[8] - n<T>(1,3)*z[29];
    z[26]=z[4]*z[26];
    z[28]=z[3] - z[28];
    z[28]=z[5]*z[28];
    z[28]= - n<T>(1,3)*z[34] + z[8] + z[28];
    z[28]=z[3]*z[28];
    z[29]= - z[25] + z[20] + z[23];
    z[30]=z[14] - z[21];
    z[31]=n<T>(7,3)*z[8] - n<T>(1,9)*z[9] - z[7];
    z[31]=z[5]*z[31];

    r +=  - n<T>(1,6)*z[10] + n<T>(5,18)*z[12] - n<T>(16,3)*z[16] + n<T>(1,2)*z[17]
     - 
      n<T>(15,4)*z[18] + z[19] + 2*z[22] + z[24] + z[26] + z[27] + z[28] + 
      n<T>(1,3)*z[29] - n<T>(5,3)*z[30] + z[31] + n<T>(2,3)*z[32];
 
    return r;
}

template std::complex<double> qqb_2lha_tf551(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf551(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
