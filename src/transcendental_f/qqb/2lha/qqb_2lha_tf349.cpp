#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf349(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[31];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[19];
    z[11]=c[23];
    z[12]=c[25];
    z[13]=c[26];
    z[14]=c[28];
    z[15]=c[31];
    z[16]=f[3];
    z[17]=f[5];
    z[18]=f[11];
    z[19]=f[13];
    z[20]=npow(z[5],2);
    z[21]= - z[4] - 2*z[3];
    z[21]=z[3]*z[21];
    z[21]= - z[21] + z[20] + n<T>(1,2)*z[9] - z[8];
    z[22]=n<T>(2,3)*z[3];
    z[23]=n<T>(1,3)*z[5];
    z[24]=z[22] + z[23];
    z[25]=z[1]*z[24];
    z[26]=z[4]*z[1];
    z[25]=z[26] + z[25];
    z[27]=2*i;
    z[25]=z[25]*z[27];
    z[28]=z[3] - z[5];
    z[29]= - z[1]*z[27];
    z[28]=z[2] + z[29] - z[4] + n<T>(1,6)*z[28];
    z[28]=z[2]*z[28];
    z[29]=z[7] + n<T>(2,3)*z[6];
    z[30]=z[4]*z[23];
    z[21]=z[28] + z[25] + z[30] + z[29] - n<T>(1,3)*z[21];
    z[21]=z[2]*z[21];
    z[25]=npow(z[4],2);
    z[22]=z[4]*z[22];
    z[22]=z[22] + n<T>(1,6)*z[25] - n<T>(1,18)*z[9] + z[29];
    z[22]=z[3]*z[22];
    z[25]= - n<T>(1,3)*z[9] + z[8];
    z[23]=z[25]*z[23];
    z[25]=z[4]*z[5];
    z[20]=z[20] - n<T>(1,2)*z[25];
    z[25]=n<T>(1,3)*z[4];
    z[20]=z[20]*z[25];
    z[24]= - z[26]*z[24];
    z[25]= - n<T>(1,3)*z[8] - z[29];
    z[25]=z[1]*z[25];
    z[24]=z[25] + 2*z[13] + z[11] + z[24];
    z[24]=z[24]*z[27];
    z[25]=z[14] + z[15];
    z[26]=2*z[16] + 4*z[17] - z[19] + z[18];

    r += n<T>(1,6)*z[10] - z[12] + z[20] + z[21] + z[22] + z[23] + z[24] - 
      2*z[25] + n<T>(1,3)*z[26];
 
    return r;
}

template std::complex<double> qqb_2lha_tf349(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf349(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
