#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf708(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[15];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[13];
    z[3]=c[20];
    z[4]=c[23];
    z[5]=f[22];
    z[6]=f[24];
    z[7]=f[42];
    z[8]=f[61];
    z[9]=f[62];
    z[10]=f[77];
    z[11]=f[79];
    z[12]=f[80];
    z[13]=f[81];
    z[14]= - n<T>(12,5)*z[2] + n<T>(1,20)*z[4] - z[3];
    z[14]=9*z[14] + n<T>(53,180)*z[1];
    z[14]=i*z[14];

    r += 6*z[5] + 10*z[6] + n<T>(1,2)*z[7] + 4*z[8] - 5*z[9] + z[10] - 
      z[11] + n<T>(3,2)*z[12] + 2*z[13] + z[14];
 
    return r;
}

template std::complex<double> qqb_2lha_tf708(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf708(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
