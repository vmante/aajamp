#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf563(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[100];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=d[5];
    z[11]=d[11];
    z[12]=d[16];
    z[13]=d[6];
    z[14]=d[9];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[8];
    z[22]=e[9];
    z[23]=e[10];
    z[24]=e[11];
    z[25]=e[12];
    z[26]=c[3];
    z[27]=c[11];
    z[28]=c[15];
    z[29]=c[17];
    z[30]=c[19];
    z[31]=c[23];
    z[32]=c[25];
    z[33]=c[26];
    z[34]=c[28];
    z[35]=c[31];
    z[36]=e[3];
    z[37]=e[6];
    z[38]=e[7];
    z[39]=e[13];
    z[40]=e[14];
    z[41]=f[0];
    z[42]=f[1];
    z[43]=f[3];
    z[44]=f[4];
    z[45]=f[5];
    z[46]=f[6];
    z[47]=f[7];
    z[48]=f[11];
    z[49]=f[12];
    z[50]=f[16];
    z[51]=f[17];
    z[52]=f[18];
    z[53]=f[23];
    z[54]=f[31];
    z[55]=f[35];
    z[56]=f[36];
    z[57]=f[37];
    z[58]=f[39];
    z[59]=f[51];
    z[60]=f[54];
    z[61]=f[55];
    z[62]=f[56];
    z[63]=f[58];
    z[64]=f[76];
    z[65]=f[78];
    z[66]=n<T>(1,2)*z[6];
    z[67]=z[1]*i;
    z[68]=z[66] - z[67];
    z[69]=n<T>(1,3)*z[68];
    z[70]=n<T>(1,2)*z[8];
    z[71]=n<T>(1,2)*z[4];
    z[72]=n<T>(1,6)*z[13] + z[70] - z[69] - z[71];
    z[72]=z[13]*z[72];
    z[73]=n<T>(1,2)*z[10];
    z[74]=z[67] - z[6];
    z[75]= - z[73] - z[74];
    z[75]=z[10]*z[75];
    z[76]=z[6]*z[74];
    z[75]=z[76] + z[75];
    z[72]=n<T>(1,3)*z[75] + z[72];
    z[72]=z[13]*z[72];
    z[75]= - z[5] - z[3];
    z[75]=z[67]*z[75];
    z[75]= - z[26] + z[75];
    z[75]=z[9]*z[75];
    z[76]=z[67]*z[4];
    z[77]=z[76] + z[26];
    z[78]=z[8]*z[67];
    z[78]=z[78] + z[77];
    z[78]=z[12]*z[78];
    z[79]=z[67]*z[6];
    z[79]=z[79] + z[26];
    z[80]=z[7]*z[67];
    z[80]=z[80] + z[79];
    z[80]=z[16]*z[80];
    z[81]=npow(z[6],2)*z[67];
    z[82]=z[67] - z[5];
    z[83]= - z[3] + z[82];
    z[83]=z[19]*z[83];
    z[84]=z[67] - z[4];
    z[85]= - z[8] + z[84];
    z[85]=z[20]*z[85];
    z[86]=z[74] - z[7];
    z[86]=z[24]*z[86];
    z[87]= - z[8] - z[13];
    z[87]=z[37]*z[87];
    z[88]= - z[7] - z[14];
    z[88]=z[40]*z[88];
    z[72]= - z[85] - z[86] - z[87] - z[88] - z[75] - z[78] - z[80] - 
    z[83] + z[41] + z[81] - z[72];
    z[75]=z[14]*z[67];
    z[75]=z[75] + z[77];
    z[75]=z[11]*z[75];
    z[77]=z[13]*z[67];
    z[77]=z[77] + z[79];
    z[77]=z[15]*z[77];
    z[78]=z[13] - z[74];
    z[78]=z[22]*z[78];
    z[79]= - z[14] + z[84];
    z[79]=z[25]*z[79];
    z[80]= - z[3] - z[8];
    z[80]=z[36]*z[80];
    z[81]= - z[10] - z[13];
    z[81]=z[38]*z[81];
    z[83]= - z[10] - z[14];
    z[83]=z[39]*z[83];
    z[75]= - z[80] - z[81] - z[83] + z[60] - z[75] - z[77] - z[78] - 
    z[79] - z[32] + z[55] - z[53] + z[52] + z[45];
    z[77]=n<T>(1,2)*z[7];
    z[78]=z[77] - z[66];
    z[79]=n<T>(1,2)*z[5];
    z[80]= - n<T>(1,3)*z[4] - z[79] - n<T>(5,3)*z[67] + z[78];
    z[80]=z[4]*z[80];
    z[81]=z[68]*z[6];
    z[83]=z[77] - z[67];
    z[85]=z[7]*z[83];
    z[86]=z[79] - z[67];
    z[87]=z[6] - z[86];
    z[87]=z[5]*z[87];
    z[80]=z[80] + z[87] - z[81] + z[85];
    z[80]=z[4]*z[80];
    z[80]= - z[42] - z[80] + z[62] + z[57] + z[46];
    z[85]=z[78] + z[86];
    z[85]=z[5]*z[85];
    z[86]=n<T>(1,4)*z[6];
    z[87]=n<T>(1,4)*z[5];
    z[88]=z[86] + z[87];
    z[89]=n<T>(1,2)*z[2];
    z[90]=z[89] - z[67];
    z[91]=n<T>(1,4)*z[7];
    z[92]=n<T>(1,4)*z[3] - z[91] - z[90] + z[88];
    z[92]=z[2]*z[92];
    z[93]=z[74] + z[7];
    z[94]= - z[93]*z[77];
    z[95]=z[74] - z[5];
    z[96]= - z[7] + z[95];
    z[96]=n<T>(1,2)*z[96] + z[3];
    z[96]=z[3]*z[96];
    z[85]=z[92] + z[96] + z[85] + z[81] + z[94];
    z[92]= - z[67] + z[71];
    z[92]=z[92]*z[71];
    z[94]=npow(z[8],2);
    z[85]= - n<T>(1,4)*z[94] + z[92] + n<T>(1,3)*z[85];
    z[85]=z[2]*z[85];
    z[92]=z[77] + z[67];
    z[92]=z[92]*z[7];
    z[94]=n<T>(1,3)*z[67];
    z[96]=z[94] + z[7];
    z[97]=n<T>(1,6)*z[4];
    z[98]=z[97] - z[96];
    z[98]=z[4]*z[98];
    z[99]=z[70] + z[74];
    z[99]=z[8]*z[99];
    z[98]=z[99] + z[98] + n<T>(1,3)*z[81] + z[92];
    z[69]=z[70] + z[97] + z[69] + z[77];
    z[69]=n<T>(1,2)*z[69] - n<T>(1,3)*z[10];
    z[69]=z[10]*z[69];
    z[69]=n<T>(1,2)*z[98] + z[69];
    z[69]=z[69]*z[73];
    z[86]= - z[67] - z[86];
    z[86]=z[6]*z[86];
    z[97]= - n<T>(13,6)*z[7] + z[67] + z[6];
    z[77]=z[97]*z[77];
    z[77]=z[86] + z[77];
    z[77]=z[7]*z[77];
    z[79]= - z[79] - z[90];
    z[79]=z[18]*z[79];
    z[86]= - z[89] - z[68];
    z[86]=z[21]*z[86];
    z[89]=z[2] - n<T>(5,2)*z[3] - n<T>(7,2)*z[7] + z[95];
    z[89]=z[23]*z[89];
    z[77]=z[77] + z[89] + z[79] + z[86];
    z[79]= - n<T>(11,12)*z[3] - z[83] - z[88];
    z[79]=z[3]*z[79];
    z[66]=z[68]*z[66];
    z[68]=n<T>(5,4)*z[7] + z[68];
    z[68]=z[7]*z[68];
    z[74]= - n<T>(1,2)*z[74] + z[5] + z[7];
    z[74]=z[5]*z[74];
    z[68]=z[79] + z[74] + z[66] + z[68];
    z[74]=n<T>(1,3)*z[3];
    z[68]=z[68]*z[74];
    z[79]=n<T>(1,6)*z[5];
    z[83]= - z[79] + z[96];
    z[83]=z[5]*z[83];
    z[81]=z[83] + z[81] - z[92];
    z[78]= - z[79] - z[94] - z[78];
    z[78]= - n<T>(5,6)*z[8] + z[71] + n<T>(1,2)*z[78] + z[74];
    z[78]=z[8]*z[78];
    z[82]=n<T>(1,2)*z[3] + z[82];
    z[74]=z[82]*z[74];
    z[74]=z[78] - z[76] + n<T>(1,2)*z[81] + z[74];
    z[70]=z[74]*z[70];
    z[74]= - z[6] - 7*z[7];
    z[74]=z[74]*z[87];
    z[67]= - n<T>(7,4)*z[7] + n<T>(7,2)*z[67] - z[6];
    z[67]=z[7]*z[67];
    z[66]=z[74] - z[66] + z[67];
    z[66]=z[66]*z[79];
    z[67]=n<T>(1,12)*z[13] - n<T>(1,9)*z[10] - n<T>(2,9)*z[2] - n<T>(5,72)*z[8] - z[71]
    - n<T>(1,18)*z[3] - n<T>(1,36)*z[5] - n<T>(5,9)*z[6] + z[91];
    z[67]=z[26]*z[67];
    z[73]= - z[73] - z[84];
    z[73]=z[10]*z[73];
    z[74]=npow(z[4],2);
    z[73]= - n<T>(1,2)*z[74] + z[73];
    z[73]=n<T>(1,2)*z[73] + n<T>(1,3)*z[26];
    z[74]=z[14]*z[93];
    z[73]=n<T>(1,3)*z[73] + n<T>(1,4)*z[74];
    z[73]=z[14]*z[73];
    z[71]= - z[71] - z[90];
    z[71]=z[17]*z[71];
    z[74]=z[58] - z[56] - z[51] - z[50] - z[43] + z[63] - z[61];
    z[76]=z[59] + z[54] + z[49] - z[48];
    z[78]= - n<T>(5,3)*z[33] - n<T>(1,2)*z[29] - n<T>(5,24)*z[27];
    z[78]=i*z[78];
    z[79]=n<T>(1,3)*i;
    z[79]= - z[31]*z[79];

    r +=  - n<T>(7,12)*z[28] + n<T>(2,9)*z[30] + n<T>(5,6)*z[34] - n<T>(5,8)*z[35]
     - n<T>(11,24)*z[44]
     - n<T>(4,3)*z[47]
     + z[64]
     + z[65]
     + z[66]
     + z[67]
     + z[68]
       + z[69] + z[70] + z[71] - n<T>(1,2)*z[72] + z[73] + n<T>(1,8)*z[74] - n<T>(1,6)*z[75]
     + n<T>(1,24)*z[76]
     + n<T>(1,3)*z[77]
     + z[78]
     + z[79] - n<T>(1,4)*
      z[80] + z[85];
 
    return r;
}

template std::complex<double> qqb_2lha_tf563(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf563(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
