#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf722(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[87];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[9];
    z[10]=d[15];
    z[11]=d[5];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[18];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[10];
    z[21]=e[11];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[18];
    z[27]=c[19];
    z[28]=c[21];
    z[29]=c[23];
    z[30]=c[25];
    z[31]=c[26];
    z[32]=c[28];
    z[33]=c[29];
    z[34]=c[31];
    z[35]=e[3];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[2];
    z[41]=f[3];
    z[42]=f[4];
    z[43]=f[5];
    z[44]=f[6];
    z[45]=f[7];
    z[46]=f[10];
    z[47]=f[11];
    z[48]=f[12];
    z[49]=f[14];
    z[50]=f[16];
    z[51]=f[17];
    z[52]=f[18];
    z[53]=f[20];
    z[54]=f[21];
    z[55]=f[23];
    z[56]=f[50];
    z[57]=f[51];
    z[58]=f[54];
    z[59]=f[55];
    z[60]=f[56];
    z[61]=f[58];
    z[62]=f[88];
    z[63]=f[89];
    z[64]=n<T>(2,3)*z[7];
    z[65]=z[1]*i;
    z[66]=3*z[65];
    z[67]=z[66] - z[64];
    z[68]=n<T>(4,3)*z[3];
    z[69]=n<T>(1,3)*z[2];
    z[70]=n<T>(11,12)*z[5];
    z[67]= - n<T>(14,3)*z[6] + z[70] + z[69] + z[68] + 2*z[67] - n<T>(5,4)*z[4];
    z[67]=z[6]*z[67];
    z[71]=16*z[7];
    z[72]= - n<T>(11,2)*z[65] + z[71];
    z[73]=n<T>(5,2)*z[4];
    z[70]=z[70] - n<T>(13,3)*z[2] - n<T>(16,3)*z[3] + n<T>(1,3)*z[72] + z[73];
    z[70]=z[5]*z[70];
    z[72]=n<T>(8,3)*z[7];
    z[74]= - 5*z[65] + z[7];
    z[74]=z[74]*z[72];
    z[75]=n<T>(1,2)*z[4];
    z[76]=z[75] - z[65];
    z[73]= - z[76]*z[73];
    z[77]=2*z[7];
    z[78]=z[77] + z[65];
    z[79]= - 2*z[78] + 5*z[3];
    z[68]=z[79]*z[68];
    z[79]=4*z[7];
    z[80]=11*z[65];
    z[81]=8*z[3] + z[80] + z[79];
    z[81]=2*z[81] - n<T>(11,2)*z[2];
    z[69]=z[81]*z[69];
    z[67]=z[67] + z[70] + z[69] + z[68] + z[74] + z[73];
    z[67]=z[6]*z[67];
    z[68]=n<T>(4,3)*z[7];
    z[69]=n<T>(11,6)*z[4];
    z[66]=z[69] + z[66] + z[68];
    z[66]=z[4]*z[66];
    z[70]=4*z[3];
    z[73]=4*z[6];
    z[74]=z[73] + z[2] + z[70] - 4*z[78] - z[4];
    z[74]=z[9]*z[74];
    z[78]=2*z[65];
    z[81]=z[78] + z[7];
    z[64]= - z[81]*z[64];
    z[82]=z[78] + z[3];
    z[70]=z[82]*z[70];
    z[83]= - z[65] - z[3];
    z[83]=2*z[83] + z[6];
    z[73]=z[83]*z[73];
    z[83]= - z[4] + n<T>(1,2)*z[2];
    z[83]=z[2]*z[83];
    z[64]=z[74] + z[73] + z[83] + z[70] + z[64] + z[66];
    z[64]=z[9]*z[64];
    z[66]=z[5] + z[3];
    z[70]=z[65]*z[66];
    z[70]=z[23] + z[70];
    z[70]=z[10]*z[70];
    z[73]=z[6] + z[7];
    z[74]=z[65]*z[73];
    z[74]=z[23] + z[74];
    z[74]=z[14]*z[74];
    z[66]= - z[65] + z[66];
    z[66]=z[17]*z[66];
    z[73]=z[65] - z[73];
    z[73]=z[21]*z[73];
    z[66]=z[73] + z[66] + z[70] + z[74];
    z[70]=z[78] - z[7];
    z[73]=7*z[7];
    z[74]= - z[70]*z[73];
    z[76]=z[4]*z[76];
    z[83]=n<T>(17,3)*z[65] - z[77];
    z[83]=2*z[83] - n<T>(1,3)*z[3];
    z[83]=z[3]*z[83];
    z[72]=n<T>(8,3)*z[3] - n<T>(4,3)*z[4] + z[65] - z[72];
    z[72]=2*z[72] + n<T>(7,2)*z[2];
    z[72]=z[2]*z[72];
    z[84]= - z[2] - n<T>(29,3)*z[3] + z[73] + n<T>(1,12)*z[4];
    z[84]=z[5]*z[84];
    z[72]=z[84] + z[72] + z[83] + z[74] + n<T>(1,6)*z[76];
    z[72]=z[5]*z[72];
    z[74]=n<T>(7,3)*z[3];
    z[76]=n<T>(8,3)*z[65];
    z[83]=5*z[7];
    z[84]=n<T>(4,3)*z[5] + z[74] - z[76] - z[83];
    z[84]=z[5]*z[84];
    z[76]= - z[76] + n<T>(8,3)*z[5];
    z[85]=3*z[4];
    z[86]= - 2*z[8] - 3*z[2] - n<T>(23,3)*z[3] + z[85] + z[83] + z[76];
    z[86]=z[8]*z[86];
    z[83]=z[81]*z[83];
    z[74]= - z[82]*z[74];
    z[82]=z[65]*z[4];
    z[74]=z[86] + 2*z[84] + z[74] + z[83] - 6*z[82];
    z[74]=z[8]*z[74];
    z[83]=3*z[7];
    z[84]=z[65] - z[83];
    z[79]=z[84]*z[79];
    z[84]= - z[78] + z[4];
    z[84]=z[84]*z[85];
    z[77]=n<T>(91,9)*z[3] - n<T>(23,3)*z[65] + z[77];
    z[77]=z[3]*z[77];
    z[77]=z[77] + z[79] + z[84];
    z[77]=z[3]*z[77];
    z[70]=z[70]*z[7];
    z[79]= - 10*z[65] + n<T>(7,2)*z[4];
    z[79]=z[4]*z[79];
    z[84]= - 10*z[3] + z[65] + 8*z[7];
    z[84]=z[3]*z[84];
    z[79]=2*z[84] + 8*z[70] + z[79];
    z[68]= - n<T>(5,6)*z[2] - n<T>(17,3)*z[3] + z[69] - z[65] - z[68];
    z[68]=z[2]*z[68];
    z[68]=n<T>(1,3)*z[79] + z[68];
    z[68]=z[2]*z[68];
    z[69]=z[11] + z[9];
    z[79]=z[65] - z[4];
    z[84]= - z[83] + z[79] + 2*z[69];
    z[84]=z[11]*z[84];
    z[81]= - z[81]*z[83];
    z[83]=z[65] + z[83];
    z[83]=2*z[83] - z[4];
    z[83]=z[4]*z[83];
    z[85]=z[9]*z[79];
    z[81]=z[84] + 4*z[85] + z[81] + z[83];
    z[81]=z[11]*z[81];
    z[83]=4*z[2];
    z[75]=n<T>(7,2)*z[5] + z[83] - 7*z[65] - z[75];
    z[75]=z[16]*z[75];
    z[75]=z[39] - z[49] + z[75] - z[61];
    z[84]=z[6] - z[2];
    z[73]=n<T>(13,3)*z[3] + z[73] + z[76] + n<T>(8,3)*z[84];
    z[73]=z[20]*z[73];
    z[69]=z[36]*z[69];
    z[69]=z[58] + z[73] + z[69];
    z[73]=z[82] + z[23];
    z[76]=z[8]*z[65];
    z[76]=z[76] + z[73];
    z[76]=z[13]*z[76];
    z[82]= - z[8] + z[79];
    z[82]=z[18]*z[82];
    z[76]=z[54] + z[76] + z[82];
    z[82]= - z[9]*z[65];
    z[73]=z[82] - z[73];
    z[73]=z[12]*z[73];
    z[79]=z[9] - z[79];
    z[79]=z[22]*z[79];
    z[73]=z[73] + z[79];
    z[79]= - n<T>(83,2)*z[65] - 11*z[7];
    z[79]=n<T>(1,3)*z[79] + n<T>(9,2)*z[4];
    z[79]=z[4]*z[79];
    z[70]=n<T>(11,3)*z[70] + z[79];
    z[70]=z[4]*z[70];
    z[79]=z[83] - z[80] + 7*z[4];
    z[79]=n<T>(1,3)*z[79] + z[9];
    z[79]=z[15]*z[79];
    z[80]=z[56] - z[46];
    z[82]=z[59] + z[40];
    z[71]=4*z[11] - n<T>(55,6)*z[8] - n<T>(52,3)*z[9] + n<T>(211,12)*z[6] - z[5]
     + 
   n<T>(83,4)*z[2] - n<T>(65,12)*z[3] - z[71] - n<T>(145,3)*z[4];
    z[71]=z[23]*z[71];
    z[71]=z[71] - z[26];
    z[83]=30*z[31] + n<T>(13,2)*z[29] - 9*z[28] - n<T>(29,18)*z[24];
    z[83]=i*z[83];
    z[65]= - z[65] + n<T>(5,3)*z[7];
    z[65]=z[65]*npow(z[7],2);
    z[78]= - z[6] + z[78] - z[2];
    z[78]=z[19]*z[78];
    z[84]=z[3] + z[8];
    z[84]=z[35]*z[84];
    z[85]=z[7] + z[9];
    z[85]=z[37]*z[85];

    r +=  - n<T>(197,12)*z[25] - n<T>(85,24)*z[27] + n<T>(7,12)*z[30] - n<T>(23,2)*
      z[32] + n<T>(1,4)*z[33] - n<T>(407,72)*z[34] + n<T>(7,2)*z[38] + n<T>(25,12)*
      z[41] + n<T>(16,3)*z[42] - n<T>(27,4)*z[43] - z[44] + n<T>(128,3)*z[45] + n<T>(11,12)*z[47]
     - n<T>(8,3)*z[48]
     + 5*z[50] - n<T>(13,4)*z[51] - 2*z[52] - n<T>(1,2)
      *z[53] + n<T>(2,3)*z[55] - z[57] + 7*z[60] - z[62] + z[63] + z[64] + 
      n<T>(10,3)*z[65] + 8*z[66] + z[67] + z[68] + 4*z[69] + z[70] + n<T>(1,3)*
      z[71] + z[72] + n<T>(17,3)*z[73] + z[74] + 3*z[75] + 6*z[76] + z[77]
       + n<T>(7,3)*z[78] + z[79] + n<T>(4,3)*z[80] + z[81] + n<T>(14,3)*z[82] + 
      z[83] + n<T>(32,3)*z[84] + n<T>(44,3)*z[85];
 
    return r;
}

template std::complex<double> qqb_2lha_tf722(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf722(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
