#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf758(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[43];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[4];
    z[7]=d[16];
    z[8]=e[0];
    z[9]=e[1];
    z[10]=e[4];
    z[11]=e[8];
    z[12]=c[3];
    z[13]=c[11];
    z[14]=c[15];
    z[15]=c[19];
    z[16]=c[21];
    z[17]=c[23];
    z[18]=c[25];
    z[19]=c[26];
    z[20]=c[28];
    z[21]=c[31];
    z[22]=f[0];
    z[23]=f[1];
    z[24]=f[3];
    z[25]=f[5];
    z[26]=f[11];
    z[27]=f[17];
    z[28]=f[18];
    z[29]=npow(z[2],2);
    z[30]=npow(z[3],2);
    z[29]= - 2*z[29] + 3*z[30];
    z[31]=3*z[3];
    z[32]= - z[31] + 4*z[2];
    z[33]=z[1]*i;
    z[34]=2*z[33];
    z[35]= - z[32]*z[34];
    z[31]=z[33] + z[31] - 2*z[2];
    z[31]=2*z[31] - z[4];
    z[31]=z[4]*z[31];
    z[36]= - z[4] + z[32];
    z[36]=z[5]*z[36];
    z[31]=z[36] + z[31] + z[35] - z[29];
    z[31]=z[5]*z[31];
    z[35]=2*z[3];
    z[36]= - z[33]*z[35];
    z[35]= - z[35] + z[2];
    z[35]=z[2]*z[35];
    z[37]= - z[6] + z[3] - z[2];
    z[37]=z[6]*z[37];
    z[35]=2*z[37] + z[36] + z[30] + z[35];
    z[35]=z[6]*z[35];
    z[36]=z[6] + z[3];
    z[37]=z[33]*z[36];
    z[37]=z[12] + z[37];
    z[37]=z[7]*z[37];
    z[36]=z[33] - z[36];
    z[36]=z[10]*z[36];
    z[36]=z[14] - z[37] - z[36] + z[28] - z[23];
    z[37]=z[34] - z[4];
    z[32]= - z[32]*z[37];
    z[29]=z[32] - z[29];
    z[29]=z[4]*z[29];
    z[32]=z[34] - z[2];
    z[34]= - z[4] + z[32];
    z[34]=z[9]*z[34];
    z[37]= - z[5] + z[32];
    z[37]=z[11]*z[37];
    z[34]=z[34] + z[37];
    z[37]=z[3] - 6*z[2];
    z[37]=z[2]*z[37];
    z[37]=6*z[30] + z[37];
    z[37]=z[2]*z[37];
    z[38]= - 14*z[3] + 13*z[2];
    z[38]=z[2]*z[38];
    z[30]=z[30] + z[38];
    z[30]=z[30]*z[33];
    z[33]=z[22] - z[18];
    z[38]=z[27] + z[24];
    z[39]= - 38*z[19] - 14*z[17] + n<T>(1,3)*z[13];
    z[39]=i*z[39];
    z[40]=npow(z[3],3);
    z[41]=z[5] + z[4];
    z[41]=n<T>(5,6)*z[6] + z[3] - n<T>(5,3)*z[2] + n<T>(1,3)*z[41];
    z[41]=z[12]*z[41];
    z[32]= - z[3] + z[32];
    z[32]=z[8]*z[32];
    z[42]=2*i;
    z[42]=z[16]*z[42];

    r += z[15] + 18*z[20] + n<T>(7,3)*z[21] - 5*z[25] - z[26] + z[29] + 
      z[30] + z[31] + 10*z[32] - 6*z[33] + 4*z[34] + z[35] - 2*z[36] + 
      z[37] - 3*z[38] + z[39] - z[40] + z[41] + z[42];
 
    return r;
}

template std::complex<double> qqb_2lha_tf758(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf758(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
