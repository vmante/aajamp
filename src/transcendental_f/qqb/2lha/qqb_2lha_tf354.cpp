#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf354(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=e[2];
    z[8]=e[10];
    z[9]=c[3];
    z[10]=c[11];
    z[11]=c[15];
    z[12]=c[31];
    z[13]=e[3];
    z[14]=f[2];
    z[15]=f[4];
    z[16]=f[10];
    z[17]=f[12];
    z[18]=z[4] + z[2];
    z[19]=2*z[3];
    z[20]=z[19] + z[18];
    z[21]=n<T>(1,3)*z[4];
    z[20]=z[20]*z[21];
    z[22]=npow(z[2],2);
    z[23]= - z[3]*z[2];
    z[24]=z[3] + z[2];
    z[25]= - z[6]*z[24];
    z[20]=z[25] + z[20] + z[22] + z[23];
    z[23]=z[1]*i;
    z[20]=z[23]*z[20];
    z[19]=z[2] - z[19];
    z[19]=z[3]*z[19];
    z[19]=z[22] + z[19];
    z[25]= - z[2] + 4*z[3];
    z[25]=n<T>(1,3)*z[25] - z[4];
    z[25]=z[25]*z[23];
    z[26]=z[3] - z[2];
    z[27]=z[23] - z[26];
    z[28]=n<T>(2,3)*z[5];
    z[27]=z[27]*z[28];
    z[26]=z[26]*z[4];
    z[19]=z[27] + z[25] + n<T>(1,3)*z[19] + z[26];
    z[19]=z[5]*z[19];
    z[25]= - z[3]*z[24];
    z[22]= - z[26] + 2*z[22] + z[25];
    z[21]=z[22]*z[21];
    z[22]= - z[28] + z[23] + z[4] - n<T>(2,3)*z[2] - z[3];
    z[22]=z[8]*z[22];
    z[23]=z[23] - z[24];
    z[23]=z[7]*z[23];
    z[24]=z[14] + z[16] - z[15];
    z[18]=z[13]*z[18];
    z[18]=z[18] - z[17];
    z[25]=npow(z[2],3);
    z[26]=z[2]*npow(z[3],2);
    z[27]=z[4] + 8*z[5];
    z[27]=n<T>(1,9)*z[27] - z[6];
    z[27]=z[9]*z[27];
    z[28]=z[10]*i;

    r += z[11] - n<T>(3,2)*z[12] - n<T>(1,3)*z[18] + z[19] + z[20] + z[21] + 
      z[22] + z[23] + n<T>(2,3)*z[24] - z[25] + z[26] + z[27] - n<T>(1,6)*z[28]
      ;
 
    return r;
}

template std::complex<double> qqb_2lha_tf354(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf354(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
