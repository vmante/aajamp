#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf956(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[117];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[18];
    z[33]=c[19];
    z[34]=c[21];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[29];
    z[40]=c[31];
    z[41]=e[3];
    z[42]=e[6];
    z[43]=e[13];
    z[44]=e[14];
    z[45]=f[0];
    z[46]=f[1];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[8];
    z[53]=f[9];
    z[54]=f[10];
    z[55]=f[11];
    z[56]=f[12];
    z[57]=f[13];
    z[58]=f[14];
    z[59]=f[15];
    z[60]=f[16];
    z[61]=f[17];
    z[62]=f[18];
    z[63]=f[21];
    z[64]=f[23];
    z[65]=f[28];
    z[66]=f[29];
    z[67]=f[30];
    z[68]=f[31];
    z[69]=f[32];
    z[70]=f[33];
    z[71]=f[34];
    z[72]=f[35];
    z[73]=f[36];
    z[74]=f[37];
    z[75]=f[39];
    z[76]=f[41];
    z[77]=f[43];
    z[78]=f[50];
    z[79]=f[51];
    z[80]=f[52];
    z[81]=f[54];
    z[82]=f[55];
    z[83]=f[56];
    z[84]=f[58];
    z[85]=f[60];
    z[86]=n<T>(1,2)*z[5];
    z[87]=z[1]*i;
    z[88]=z[86] - z[87];
    z[89]=n<T>(1,3)*z[5];
    z[90]=z[88]*z[89];
    z[91]=n<T>(1,4)*z[2];
    z[92]=z[87] - z[5];
    z[93]=z[92]*z[91];
    z[94]=n<T>(1,2)*z[9];
    z[95]=z[94] - z[87];
    z[96]=z[9]*z[95];
    z[97]=3*z[87];
    z[98]= - z[97] + z[86];
    z[99]=n<T>(1,2)*z[4];
    z[98]=z[98]*z[99];
    z[100]=z[87] - z[9];
    z[101]=n<T>(1,2)*z[7];
    z[102]= - z[101] - z[100];
    z[102]=z[7]*z[102];
    z[103]=n<T>(1,2)*z[10];
    z[104]=z[103] + z[92];
    z[104]=z[10]*z[104];
    z[105]=z[4] - z[2];
    z[106]=n<T>(17,8)*z[87] + 2*z[5];
    z[105]=n<T>(13,6)*z[6] + n<T>(3,4)*z[10] - n<T>(15,4)*z[7] - n<T>(11,8)*z[9]
     + n<T>(1,3)
   *z[106] + n<T>(5,8)*z[105];
    z[105]=z[6]*z[105];
    z[90]=z[105] + n<T>(3,2)*z[104] + n<T>(15,2)*z[102] + z[98] - n<T>(11,4)*z[96]
    + z[90] + z[93];
    z[90]=z[6]*z[90];
    z[93]=n<T>(1,2)*z[2];
    z[96]=z[94] - z[93];
    z[98]=n<T>(39,4)*z[7];
    z[102]= - n<T>(65,6)*z[10] + z[98] + n<T>(53,12)*z[4] - n<T>(65,12)*z[87] + z[5]
    - z[96];
    z[102]=z[102]*z[103];
    z[104]=3*z[5];
    z[105]=z[88]*z[104];
    z[106]= - z[87] - z[93];
    z[106]=z[2]*z[106];
    z[94]= - z[94] + n<T>(1,2)*z[87] + z[2];
    z[94]=z[9]*z[94];
    z[107]=z[9] - z[2];
    z[108]=n<T>(53,24)*z[4] - n<T>(53,12)*z[87] - z[107];
    z[108]=z[4]*z[108];
    z[109]=z[87] - z[4];
    z[110]=z[101] + z[109];
    z[98]=z[110]*z[98];
    z[94]=z[102] + z[98] + z[108] + z[94] + z[105] + z[106];
    z[94]=z[94]*z[103];
    z[98]=5*z[2];
    z[102]=n<T>(17,2)*z[5];
    z[103]=z[98] + 7*z[87] - z[102];
    z[103]=z[2]*z[103];
    z[106]=z[88]*z[102];
    z[103]=z[106] + z[103];
    z[106]= - n<T>(17,2)*z[2] - 11*z[87] + z[102];
    z[106]=n<T>(1,2)*z[106] + 9*z[9];
    z[106]=z[9]*z[106];
    z[108]=z[87] - z[99];
    z[108]=z[4]*z[108];
    z[110]=z[2] - z[5];
    z[111]= - 17*z[110];
    z[112]= - n<T>(65,2)*z[7] - 3*z[9] + z[97] - z[111];
    z[113]=n<T>(1,4)*z[7];
    z[112]=z[112]*z[113];
    z[114]=n<T>(1,2)*z[6];
    z[115]=z[114] + z[100];
    z[115]=z[6]*z[115];
    z[102]=n<T>(55,3)*z[8] - n<T>(3,2)*z[6] - n<T>(85,2)*z[7] + 11*z[9] + n<T>(31,2)*
    z[2] + 15*z[87] + z[102];
    z[102]=z[8]*z[102];
    z[102]=n<T>(1,4)*z[102] + n<T>(19,4)*z[115] + z[112] + n<T>(3,4)*z[108] + n<T>(1,2)*
    z[103] + z[106];
    z[102]=z[8]*z[102];
    z[103]=n<T>(11,2)*z[5];
    z[106]=13*z[87] - z[103];
    z[89]=z[106]*z[89];
    z[106]=z[93] - z[5];
    z[108]=5*z[87];
    z[112]=z[108] - z[106];
    z[112]=z[112]*z[93];
    z[89]=z[89] + z[112];
    z[99]= - n<T>(17,6)*z[6] - z[99] - n<T>(7,3)*z[92] + z[93];
    z[99]=z[99]*z[114];
    z[112]=n<T>(1,8)*z[4];
    z[114]=n<T>(7,8)*z[87] - z[5];
    z[114]=n<T>(15,8)*z[3] - n<T>(7,12)*z[6] + z[112] + n<T>(1,3)*z[114] - z[91];
    z[114]=z[3]*z[114];
    z[91]=z[4] + z[9] - z[91] - 2*z[87] - n<T>(3,4)*z[5];
    z[91]=z[4]*z[91];
    z[115]= - z[9]*z[2];
    z[116]=npow(z[10],2);
    z[89]=z[114] + z[99] - z[116] + z[91] + n<T>(1,2)*z[89] + z[115];
    z[89]=z[3]*z[89];
    z[91]=z[9] + n<T>(5,2)*z[2];
    z[99]=n<T>(17,6)*z[4];
    z[114]=n<T>(5,2)*z[87];
    z[115]= - z[99] + z[114] + z[5] - z[91];
    z[115]=z[4]*z[115];
    z[99]= - n<T>(41,12)*z[7] + z[99] + z[2] - n<T>(41,6)*z[87] + z[104];
    z[99]=z[7]*z[99];
    z[114]=z[114] - z[5];
    z[114]=z[2]*z[114];
    z[116]=n<T>(3,4)*z[9] - n<T>(3,2)*z[87] + z[2];
    z[116]=z[9]*z[116];
    z[99]=z[99] + z[115] + z[116] - z[105] + z[114];
    z[91]=n<T>(7,2)*z[10] + 3*z[7] + n<T>(13,2)*z[87] - z[104] - z[91];
    z[91]=z[11]*z[91];
    z[96]= - n<T>(4,3)*z[10] + z[96] - n<T>(8,3)*z[109];
    z[96]=z[10]*z[96];
    z[91]=n<T>(1,4)*z[91] + n<T>(1,2)*z[99] + z[96];
    z[91]=z[11]*z[91];
    z[96]= - 77*z[87] + n<T>(5,2)*z[5];
    z[96]=z[96]*z[86];
    z[99]=z[93] + 25*z[87] - n<T>(13,2)*z[5];
    z[99]=z[2]*z[99];
    z[96]=z[96] + z[99];
    z[96]=z[2]*z[96];
    z[99]=19*z[87] - z[103];
    z[99]=z[5]*z[99];
    z[103]=n<T>(19,3)*z[9] - 7*z[2] - 21*z[87] - n<T>(19,2)*z[5];
    z[103]=z[9]*z[103];
    z[99]=z[99] + z[103];
    z[103]= - n<T>(11,2)*z[2] + n<T>(7,2)*z[87] + 11*z[5];
    z[103]=z[2]*z[103];
    z[99]=z[103] + n<T>(1,4)*z[99];
    z[99]=z[9]*z[99];
    z[103]=z[108] - n<T>(13,3)*z[5];
    z[103]=z[103]*npow(z[5],2);
    z[105]=n<T>(53,2)*z[4] - n<T>(91,2)*z[87] + 19*z[2];
    z[105]=n<T>(1,3)*z[105] + n<T>(5,2)*z[11];
    z[105]=z[18]*z[105];
    z[96]= - z[48] - z[54] + z[105] + z[99] + n<T>(1,2)*z[103] + n<T>(1,3)*z[96]
   ;
    z[99]= - 35*z[87] + n<T>(23,2)*z[5];
    z[99]=z[5]*z[99];
    z[103]= - n<T>(7,4)*z[2] + n<T>(55,4)*z[87] + 8*z[5];
    z[103]=z[2]*z[103];
    z[99]=n<T>(1,8)*z[99] + z[103];
    z[95]= - z[5] + z[95];
    z[95]=z[9]*z[95];
    z[103]= - 73*z[2] + z[87] + n<T>(47,2)*z[5];
    z[103]= - 11*z[4] + n<T>(1,3)*z[103] + n<T>(33,2)*z[9];
    z[103]=z[103]*z[112];
    z[95]=z[103] + n<T>(1,3)*z[99] + n<T>(33,8)*z[95];
    z[95]=z[4]*z[95];
    z[99]=z[108] - z[86];
    z[99]=z[99]*z[104];
    z[97]= - z[97] + z[106];
    z[97]=z[97]*z[98];
    z[98]= - n<T>(61,2)*z[9] + 61*z[87] - z[111];
    z[98]=z[9]*z[98];
    z[97]=z[98] + z[99] + z[97];
    z[98]=n<T>(83,24)*z[4] - n<T>(83,12)*z[87] - z[110];
    z[98]=z[4]*z[98];
    z[99]=n<T>(251,3)*z[7] + n<T>(83,6)*z[4] - 27*z[9] + n<T>(79,6)*z[87] - 39*
    z[110];
    z[99]=z[99]*z[113];
    z[97]=z[99] + n<T>(1,2)*z[97] + z[98];
    z[97]=z[97]*z[101];
    z[98]=z[87]*z[5];
    z[98]=z[98] + z[29];
    z[99]=z[7]*z[87];
    z[99]=z[99] + z[98];
    z[99]=z[15]*z[99];
    z[101]= - z[7] + z[92];
    z[101]=z[22]*z[101];
    z[99]=z[51] + z[99] + z[101];
    z[101]=z[87]*z[4];
    z[101]=z[101] + z[29];
    z[103]=z[6]*z[87];
    z[103]=z[103] + z[101];
    z[103]=z[14]*z[103];
    z[104]= - z[6] + z[109];
    z[104]=z[21]*z[104];
    z[103]=z[103] + z[104] + z[85] + z[63];
    z[104]=z[87]*z[9];
    z[104]=z[104] + z[29];
    z[105]=z[10]*z[87];
    z[105]=z[105] + z[104];
    z[105]=z[17]*z[105];
    z[106]= - z[10] + z[100];
    z[106]=z[27]*z[106];
    z[105]=z[105] + z[106] + z[66] - z[65];
    z[106]= - z[8]*z[87];
    z[104]=z[106] - z[104];
    z[104]=z[16]*z[104];
    z[100]= - z[8] + z[100];
    z[100]=z[25]*z[100];
    z[100]=z[104] + z[100];
    z[104]=z[11]*z[87];
    z[101]=z[104] + z[101];
    z[101]=z[13]*z[101];
    z[104]= - z[11] + z[109];
    z[104]=z[28]*z[104];
    z[101]=z[101] + z[104];
    z[87]=z[3]*z[87];
    z[87]=z[87] + z[98];
    z[87]=z[12]*z[87];
    z[98]=z[3] - z[92];
    z[98]=z[20]*z[98];
    z[87]=z[87] + z[98];
    z[92]=z[107] - z[92];
    z[98]=17*z[92] + 61*z[7];
    z[98]=n<T>(1,2)*z[98] + 22*z[8];
    z[98]=z[23]*z[98];
    z[92]=n<T>(3,2)*z[3] + n<T>(1,2)*z[92] + 2*z[10];
    z[92]=z[26]*z[92];
    z[88]= - z[93] - z[88];
    z[88]=z[19]*z[88];
    z[93]= - z[80] + z[76] - z[64] - z[59] + z[58] - z[45];
    z[104]=z[53] + z[46];
    z[106]=z[61] + z[49];
    z[107]=z[77] - z[72];
    z[109]= - n<T>(25,3)*z[37] - n<T>(20,3)*z[35] + n<T>(13,2)*z[34] - n<T>(1,18)*z[30];
    z[109]=i*z[109];
    z[86]= - n<T>(145,18)*z[4] - 15*z[9] + z[86] + n<T>(95,9)*z[2];
    z[86]=n<T>(5,24)*z[3] - n<T>(5,3)*z[8] + n<T>(20,9)*z[11] + n<T>(235,144)*z[6]
     - n<T>(157,72)*z[10]
     + n<T>(1,4)*z[86]
     + n<T>(88,9)*z[7];
    z[86]=z[29]*z[86];
    z[108]=z[3] - z[10] - n<T>(5,2)*z[9] - n<T>(3,2)*z[2] + z[108] - z[5];
    z[108]=z[24]*z[108];
    z[110]=z[6] + z[3];
    z[110]=z[41]*z[110];
    z[111]=z[6] + z[8];
    z[111]=z[42]*z[111];
    z[112]= - z[7] - z[11];
    z[112]=z[43]*z[112];
    z[113]= - z[10] - z[11];
    z[113]=z[44]*z[113];

    r += n<T>(283,48)*z[31] - n<T>(5,12)*z[32] - n<T>(1333,288)*z[33] - n<T>(19,48)*
      z[36] + n<T>(1,24)*z[38] + n<T>(5,16)*z[39] + n<T>(3025,288)*z[40] + n<T>(71,48)*
      z[47] - z[50] - 6*z[52] - n<T>(23,16)*z[55] - n<T>(2,3)*z[56] + n<T>(29,12)*
      z[57] + z[60] - n<T>(7,12)*z[62] - n<T>(19,4)*z[67] - n<T>(89,8)*z[68] - n<T>(45,8)*z[69]
     - 34*z[70]
     + n<T>(23,8)*z[71]
     + n<T>(11,8)*z[73]
     + n<T>(3,8)*z[74]
       - n<T>(15,4)*z[75] + n<T>(13,6)*z[78] + n<T>(83,48)*z[79] - n<T>(23,12)*z[81]
     - 
      n<T>(53,48)*z[82] - n<T>(7,8)*z[83] + n<T>(39,16)*z[84] + z[86] + n<T>(1,6)*z[87]
       + n<T>(19,6)*z[88] + z[89] + z[90] + z[91] + z[92] - n<T>(3,4)*z[93] + 
      z[94] + z[95] + n<T>(1,2)*z[96] + z[97] + z[98] + 2*z[99] + n<T>(25,2)*
      z[100] + n<T>(17,6)*z[101] + z[102] + n<T>(5,4)*z[103] + n<T>(9,8)*z[104] + n<T>(1,4)*z[105]
     + n<T>(17,16)*z[106]
     + n<T>(7,2)*z[107]
     + n<T>(3,2)*z[108]
     +  z[109] + n<T>(5,3)*z[110] + n<T>(11,2)*z[111] + n<T>(59,12)*z[112] + n<T>(53,12)*
      z[113];
 
    return r;
}

template std::complex<double> qqb_2lha_tf956(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf956(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
