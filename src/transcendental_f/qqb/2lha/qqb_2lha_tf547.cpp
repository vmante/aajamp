#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf547(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[37];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[16];
    z[8]=e[0];
    z[9]=e[1];
    z[10]=e[4];
    z[11]=c[3];
    z[12]=c[11];
    z[13]=c[15];
    z[14]=c[18];
    z[15]=c[19];
    z[16]=c[23];
    z[17]=c[25];
    z[18]=c[26];
    z[19]=c[28];
    z[20]=c[29];
    z[21]=c[31];
    z[22]=f[0];
    z[23]=f[3];
    z[24]=f[13];
    z[25]=f[18];
    z[26]=f[21];
    z[27]= - z[3] + z[5] + z[6];
    z[27]=z[27]*z[5];
    z[28]=z[6]*z[3];
    z[29]=npow(z[3],2);
    z[27]=z[27] - z[28] + n<T>(1,2)*z[29];
    z[28]=2*z[5];
    z[29]=2*z[3];
    z[30]=z[28] + z[29];
    z[31]= - 2*z[6] + z[7] - z[30];
    z[32]=i*z[1];
    z[31]=z[31]*z[32];
    z[28]=4*z[32] - z[28] + z[3] + z[6];
    z[28]=z[2]*z[28];
    z[33]= - z[5] + n<T>(1,2)*z[6];
    z[34]=n<T>(1,6)*z[4] - z[2] - n<T>(3,2)*z[32] + z[3] - z[33];
    z[34]=z[4]*z[34];
    z[35]=2*z[8];
    z[28]=z[34] + z[28] + z[31] + z[35] - z[10] - 2*z[11] + z[27];
    z[28]=z[4]*z[28];
    z[31]=n<T>(5,6)*z[2] - n<T>(7,2)*z[32] - z[29] - z[33];
    z[31]=z[2]*z[31];
    z[30]=z[6] + z[30];
    z[30]=z[30]*z[32];
    z[32]=z[35] + 2*z[9];
    z[27]=z[31] + z[30] + n<T>(1,2)*z[11] + z[32] - z[27];
    z[27]=z[2]*z[27];
    z[30]=z[20] + z[24];
    z[31]=z[19] - z[22];
    z[33]=z[17] + z[25];
    z[34]=z[11]*z[7];
    z[29]= - z[8]*z[29];
    z[35]=npow(z[6],2);
    z[35]= - n<T>(1,3)*z[35] - z[10] + n<T>(1,12)*z[11];
    z[35]=z[6]*z[35];
    z[32]= - n<T>(1,6)*z[11] + z[32];
    z[32]=z[5]*z[32];
    z[36]=z[6]*z[7];
    z[36]=z[36] - 6*z[8] + z[10] - 4*z[9];
    z[36]=z[1]*z[36];
    z[36]=z[36] + n<T>(1,12)*z[12] + 8*z[18] + n<T>(3,2)*z[16];
    z[36]=i*z[36];

    r +=  - n<T>(7,6)*z[13] - n<T>(2,3)*z[14] - n<T>(3,4)*z[15] - n<T>(77,12)*z[21]
     +  z[23] + z[26] + z[27] + z[28] + z[29] + n<T>(1,2)*z[30] - 3*z[31] + 
      z[32] - n<T>(1,3)*z[33] + z[34] + z[35] + z[36];
 
    return r;
}

template std::complex<double> qqb_2lha_tf547(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf547(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
