#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1096(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[85];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[12];
    z[13]=d[17];
    z[14]=d[18];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[8];
    z[21]=e[9];
    z[22]=e[10];
    z[23]=e[11];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[17];
    z[28]=c[19];
    z[29]=c[23];
    z[30]=c[25];
    z[31]=c[26];
    z[32]=c[28];
    z[33]=c[31];
    z[34]=e[3];
    z[35]=e[6];
    z[36]=f[2];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[6];
    z[41]=f[7];
    z[42]=f[9];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[13];
    z[46]=f[16];
    z[47]=f[17];
    z[48]=f[23];
    z[49]=f[30];
    z[50]=f[31];
    z[51]=f[32];
    z[52]=f[33];
    z[53]=f[34];
    z[54]=f[35];
    z[55]=f[36];
    z[56]=f[39];
    z[57]=f[40];
    z[58]=f[43];
    z[59]=z[1]*i;
    z[60]=z[59]*z[5];
    z[60]=z[60] + z[24];
    z[61]=z[6]*z[59];
    z[61]=z[61] + z[60];
    z[61]=z[12]*z[61];
    z[62]=z[59]*z[8];
    z[63]= - z[9]*z[59];
    z[63]= - z[24] - z[62] + z[63];
    z[63]=z[14]*z[63];
    z[64]=z[59] - z[5];
    z[65]= - z[6] + z[64];
    z[65]=z[18]*z[65];
    z[66]=z[59] - z[8];
    z[67]=z[9] - z[66];
    z[67]=z[23]*z[67];
    z[68]=z[3] + z[10];
    z[68]=z[34]*z[68];
    z[61]=z[61] + z[63] - z[48] + z[43] + z[39] + z[36] + z[65] + z[67]
    + z[68] + z[58];
    z[63]=n<T>(1,2)*z[5];
    z[65]=z[63] - z[59];
    z[67]=n<T>(15,2)*z[5];
    z[68]=z[65]*z[67];
    z[69]=15*z[8] - n<T>(23,3)*z[59] + z[67];
    z[69]=z[8]*z[69];
    z[70]=z[5] - z[66];
    z[71]=n<T>(1,2)*z[2];
    z[72]=z[70] - z[71];
    z[73]=n<T>(15,2)*z[2];
    z[74]= - z[72]*z[73];
    z[75]=n<T>(163,18)*z[7] + z[73] + n<T>(13,4)*z[8] + n<T>(22,3)*z[59] + n<T>(15,4)*
    z[5];
    z[75]=z[7]*z[75];
    z[69]=z[75] + z[74] + z[68] + z[69];
    z[69]=z[7]*z[69];
    z[74]=z[5]*z[65];
    z[75]=n<T>(1,2)*z[8];
    z[76]=z[75] - z[59];
    z[77]= - z[5] + z[76];
    z[78]=3*z[8];
    z[77]=z[77]*z[78];
    z[74]=z[74] + z[77];
    z[77]=2*z[59];
    z[79]= - z[71] + z[77] + z[5];
    z[79]=z[2]*z[79];
    z[78]=z[5] + z[78];
    z[78]=n<T>(1,4)*z[78] - z[2];
    z[78]=z[4]*z[78];
    z[74]=z[78] + n<T>(1,2)*z[74] + z[79];
    z[74]=z[4]*z[74];
    z[78]=z[71] + z[65];
    z[78]=z[16]*z[78];
    z[70]=z[70] - z[2];
    z[79]=n<T>(7,2)*z[9] + n<T>(5,2)*z[3] + z[70];
    z[79]=z[22]*z[79];
    z[77]=z[4] - z[77] + z[2];
    z[77]=z[15]*z[77];
    z[74]=z[77] + z[74] + z[79] + z[78] + z[45];
    z[77]=n<T>(19,3)*z[8];
    z[78]= - z[76]*z[77];
    z[79]=n<T>(1,2)*z[3];
    z[80]= - z[79] - z[64];
    z[81]=15*z[3];
    z[80]=z[80]*z[81];
    z[68]=z[80] + z[68] + z[78];
    z[78]=15*z[5];
    z[80]= - n<T>(7,3)*z[59] + z[78];
    z[77]=n<T>(1,2)*z[80] - z[77];
    z[77]=n<T>(71,9)*z[10] + 7*z[7] + n<T>(1,2)*z[77] - z[81];
    z[80]=n<T>(1,2)*z[10];
    z[77]=z[77]*z[80];
    z[81]=z[7] + 22*z[59] - 25*z[8];
    z[81]=z[7]*z[81];
    z[68]=z[77] + n<T>(1,2)*z[68] + n<T>(1,3)*z[81];
    z[68]=z[10]*z[68];
    z[77]=z[2] - z[5];
    z[81]=n<T>(23,2)*z[10];
    z[77]=n<T>(253,6)*z[6] - z[81] - n<T>(91,3)*z[7] + n<T>(31,3)*z[66] - n<T>(45,2)*
    z[77];
    z[77]=z[6]*z[77];
    z[82]= - n<T>(161,6)*z[8] + n<T>(161,3)*z[59] - z[78];
    z[82]=z[8]*z[82];
    z[83]=15*z[2];
    z[84]= - z[66]*z[83];
    z[77]=z[77] + z[82] + z[84];
    z[67]= - n<T>(75,4)*z[7] + z[73] - z[67] - n<T>(1,3)*z[66];
    z[67]=z[7]*z[67];
    z[73]= - z[80] - z[66];
    z[73]=z[73]*z[81];
    z[67]=z[73] + z[67] + n<T>(1,2)*z[77];
    z[67]=z[6]*z[67];
    z[73]=z[64] - z[75];
    z[77]=z[79] + z[71] + z[73];
    z[77]=z[3]*z[77];
    z[79]=n<T>(7,2)*z[65] + z[8];
    z[79]=z[5]*z[79];
    z[81]=z[2]*z[64];
    z[79]=z[81] + z[79];
    z[80]=z[80] + z[64];
    z[80]=z[10]*z[80];
    z[77]=n<T>(3,4)*z[80] + n<T>(1,2)*z[79] + z[77];
    z[79]=5*z[3];
    z[80]=n<T>(3,2)*z[10] - z[79] - n<T>(7,2)*z[64] + z[2] - z[8];
    z[80]=n<T>(3,4)*z[80] + z[9];
    z[80]=z[9]*z[80];
    z[77]=3*z[77] + z[80];
    z[77]=z[9]*z[77];
    z[65]=z[65]*z[78];
    z[78]=n<T>(139,18)*z[8] - n<T>(49,6)*z[59] + z[78];
    z[78]=z[78]*z[75];
    z[65]=z[65] + z[78];
    z[65]=z[8]*z[65];
    z[63]=z[63] + 3*z[59];
    z[78]= - n<T>(5,4)*z[8] + z[63];
    z[78]=z[8]*z[78];
    z[63]=n<T>(3,2)*z[2] - z[75] - z[63];
    z[63]=z[2]*z[63];
    z[80]=z[59] - n<T>(3,4)*z[5];
    z[80]=z[5]*z[80];
    z[63]=z[63] + z[80] + z[78];
    z[63]=z[63]*z[83];
    z[73]=z[73]*z[75];
    z[72]=z[72]*z[71];
    z[75]=n<T>(1,2)*z[59] - z[5];
    z[75]=z[5]*z[75];
    z[72]=z[72] + z[75] + z[73];
    z[73]=z[8] + z[5];
    z[73]= - z[59] - z[2] + n<T>(1,4)*z[73];
    z[73]=3*z[73] + n<T>(11,4)*z[3];
    z[73]=z[3]*z[73];
    z[72]=3*z[72] + z[73];
    z[72]=z[72]*z[79];
    z[73]=z[3]*z[59];
    z[60]=z[73] + z[60];
    z[60]=z[11]*z[60];
    z[73]=z[3] - z[64];
    z[73]=z[17]*z[73];
    z[60]= - z[30] + z[60] + z[73];
    z[71]=z[71] + z[76];
    z[71]=z[20]*z[71];
    z[71]=z[71] - z[32];
    z[73]=n<T>(131,2)*z[7];
    z[75]= - z[59]*z[73];
    z[62]= - 67*z[62] + z[75];
    z[59]=z[10]*z[59];
    z[59]= - n<T>(67,3)*z[24] + n<T>(1,3)*z[62] + z[59];
    z[59]=z[13]*z[59];
    z[62]=67*z[66] - z[73];
    z[62]=n<T>(1,3)*z[62] + z[10];
    z[62]=z[21]*z[62];
    z[66]= - z[40] + z[51] - z[47];
    z[64]=z[64]*npow(z[5],2);
    z[64]= - z[42] + z[64] + z[53] + z[37] + z[28];
    z[73]=z[52] - z[41];
    z[75]=z[7] + z[10];
    z[75]=z[35]*z[75];
    z[75]=z[75] - z[49];
    z[76]=90*z[31] + 45*z[29] + n<T>(3,4)*z[27] + n<T>(5,36)*z[25];
    z[76]=i*z[76];
    z[78]= - 3*z[2] + n<T>(3,2)*z[5] - n<T>(11,3)*z[8];
    z[78]=n<T>(1,2)*z[78] + z[3];
    z[78]=n<T>(725,18)*z[6] - n<T>(1,6)*z[10] + 5*z[78] - n<T>(47,9)*z[7];
    z[78]= - n<T>(5,4)*z[4] + n<T>(1,2)*z[78] - 20*z[9];
    z[78]=z[24]*z[78];
    z[70]=n<T>(313,6)*z[6] + 15*z[70] + n<T>(223,6)*z[7];
    z[70]=z[19]*z[70];

    r +=  - n<T>(13,24)*z[26] + n<T>(15,16)*z[33] + n<T>(165,8)*z[38] - n<T>(15,8)*
      z[44] + n<T>(45,8)*z[46] - n<T>(121,6)*z[50] - n<T>(47,6)*z[54] + n<T>(23,12)*
      z[55] - n<T>(23,4)*z[56] + n<T>(1,3)*z[57] + z[59] + n<T>(45,2)*z[60] + n<T>(15,2)
      *z[61] + z[62] + z[63] + n<T>(15,4)*z[64] + z[65] - n<T>(45,4)*z[66] + 
      z[67] + z[68] + z[69] + z[70] + 45*z[71] + z[72] - 60*z[73] + 15*
      z[74] + n<T>(23,3)*z[75] + z[76] + 5*z[77] + z[78];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1096(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1096(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
