#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1488(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[29];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[2];
    z[3]=d[5];
    z[4]=d[8];
    z[5]=d[9];
    z[6]=e[13];
    z[7]=e[14];
    z[8]=c[3];
    z[9]=d[1];
    z[10]=d[3];
    z[11]=c[11];
    z[12]=f[50];
    z[13]=f[51];
    z[14]=f[53];
    z[15]=f[54];
    z[16]=f[55];
    z[17]=f[57];
    z[18]=npow(z[4],2);
    z[19]=npow(z[3],2);
    z[18]=z[18] - z[19];
    z[20]=z[1]*i;
    z[21]= - n<T>(1,2)*z[2] + z[20] + z[5];
    z[22]=z[4] - z[3];
    z[21]=z[22]*z[21];
    z[21]= - n<T>(1,2)*z[18] + z[21];
    z[21]=z[2]*z[21];
    z[21]=z[12] + z[21] + z[15];
    z[23]= - z[3] + n<T>(5,6)*z[4];
    z[23]=z[4]*z[23];
    z[23]=n<T>(1,6)*z[19] + z[23];
    z[24]=n<T>(1,3)*z[5];
    z[25]= - z[22]*z[24];
    z[23]=n<T>(1,2)*z[23] + z[25];
    z[23]=z[23]*z[20];
    z[20]=n<T>(1,4)*z[20];
    z[25]= - n<T>(7,3)*z[3] - z[4];
    z[25]=z[20] + n<T>(1,4)*z[25] - z[24];
    z[25]=z[6]*z[25];
    z[26]=z[3] + n<T>(7,3)*z[4];
    z[20]=z[20] + n<T>(1,4)*z[26] + z[24];
    z[20]=z[7]*z[20];
    z[24]=z[10] - z[9];
    z[27]=n<T>(11,3)*z[22] - z[24];
    z[27]=z[8]*z[27];
    z[28]=z[11]*i;
    z[27]=z[27] + z[28];
    z[28]= - z[3] + n<T>(1,2)*z[4];
    z[28]=z[28]*z[4];
    z[28]=z[28] + z[7] + z[6] + n<T>(1,2)*z[19];
    z[24]= - n<T>(1,4)*z[24];
    z[24]=z[28]*z[24];
    z[26]=z[4]*z[26];
    z[19]= - z[19] + z[26];
    z[19]=z[4]*z[19];
    z[26]=npow(z[3],3);
    z[19]= - n<T>(7,3)*z[26] + z[19];
    z[22]=z[5]*z[22];
    z[18]= - n<T>(1,3)*z[18] - n<T>(1,4)*z[22];
    z[18]=z[5]*z[18];
    z[22]=z[16] + z[13];

    r += z[14] + z[17] + z[18] + n<T>(1,8)*z[19] + z[20] + n<T>(1,6)*z[21] + n<T>(1,12)*z[22]
     + n<T>(1,2)*z[23]
     + z[24]
     + z[25]
     + n<T>(1,24)*z[27];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1488(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1488(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
