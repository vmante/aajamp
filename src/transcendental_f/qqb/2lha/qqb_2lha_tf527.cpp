#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf527(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[11];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[13];
    z[3]=c[20];
    z[4]=c[23];
    z[5]=f[22];
    z[6]=f[42];
    z[7]=f[44];
    z[8]=f[80];
    z[9]=z[8] - z[7];
    z[10]= - n<T>(12,5)*z[2] + n<T>(1,20)*z[4] - z[3];
    z[10]=3*z[10] + n<T>(53,540)*z[1];
    z[10]=i*z[10];

    r += z[5] - z[6] + n<T>(1,2)*z[9] + z[10];
 
    return r;
}

template std::complex<double> qqb_2lha_tf527(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf527(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
