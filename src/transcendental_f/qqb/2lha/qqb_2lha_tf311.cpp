#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf311(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[11];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=f[22];
    z[4]=f[24];
    z[5]=f[61];
    z[6]=f[62];
    z[7]=f[77];
    z[8]=z[4] - z[6];
    z[9]=z[3] - z[5];
    z[10]=z[1]*i;

    r +=  - 2*z[2] + z[7] - n<T>(1,3)*z[8] + n<T>(4,3)*z[9] + n<T>(2,27)*z[10];
 
    return r;
}

template std::complex<double> qqb_2lha_tf311(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf311(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
