#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1355(
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[4];
  std::complex<T> r(0,0);

    z[1]=e[1];
    z[2]=e[8];

    r +=  - z[1] + z[2];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1355(
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1355(
  const std::array<std::complex<dd_real>,24>& e
);
#endif
