#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf613(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[113];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=f[42];
    z[3]=f[77];
    z[4]=f[79];
    z[5]=c[11];
    z[6]=d[0];
    z[7]=d[2];
    z[8]=d[4];
    z[9]=d[5];
    z[10]=d[6];
    z[11]=d[7];
    z[12]=d[8];
    z[13]=d[9];
    z[14]=c[12];
    z[15]=c[13];
    z[16]=c[20];
    z[17]=c[23];
    z[18]=c[32];
    z[19]=c[33];
    z[20]=c[35];
    z[21]=c[36];
    z[22]=c[37];
    z[23]=c[38];
    z[24]=c[39];
    z[25]=c[40];
    z[26]=c[43];
    z[27]=c[44];
    z[28]=c[47];
    z[29]=c[48];
    z[30]=c[49];
    z[31]=c[50];
    z[32]=c[55];
    z[33]=c[56];
    z[34]=c[57];
    z[35]=c[59];
    z[36]=c[81];
    z[37]=c[84];
    z[38]=f[22];
    z[39]=f[24];
    z[40]=f[44];
    z[41]=f[61];
    z[42]=f[62];
    z[43]=f[69];
    z[44]=f[70];
    z[45]=d[1];
    z[46]=d[3];
    z[47]=g[27];
    z[48]=g[38];
    z[49]=g[48];
    z[50]=g[50];
    z[51]=g[79];
    z[52]=g[82];
    z[53]=g[90];
    z[54]=g[92];
    z[55]=g[95];
    z[56]=g[99];
    z[57]=g[101];
    z[58]=g[104];
    z[59]=g[110];
    z[60]=g[112];
    z[61]=g[113];
    z[62]=g[128];
    z[63]=g[131];
    z[64]=g[138];
    z[65]=g[140];
    z[66]=g[145];
    z[67]=g[147];
    z[68]=g[173];
    z[69]=g[181];
    z[70]=g[189];
    z[71]=g[191];
    z[72]=g[212];
    z[73]=g[214];
    z[74]=g[218];
    z[75]=g[220];
    z[76]=g[224];
    z[77]=g[232];
    z[78]=g[233];
    z[79]=g[234];
    z[80]=g[243];
    z[81]=g[245];
    z[82]=g[249];
    z[83]=g[251];
    z[84]=g[254];
    z[85]=g[276];
    z[86]=g[339];
    z[87]=g[345];
    z[88]=g[347];
    z[89]=g[348];
    z[90]=g[377];
    z[91]=g[378];
    z[92]=g[379];
    z[93]= - z[16] + n<T>(1,20)*z[17];
    z[94]=n<T>(1,12)*z[93] - n<T>(1,5)*z[15];
    z[94]=11*z[94] + n<T>(761,6480)*z[5];
    z[94]=z[13]*z[94];
    z[95]= - n<T>(1,4)*z[17] + 5*z[16];
    z[95]=n<T>(1,12)*z[95] + z[15];
    z[95]=55*z[95] - n<T>(577,1296)*z[5];
    z[95]=z[10]*z[95];
    z[94]= - z[34] + z[94] + z[95];
    z[95]=z[8] - z[12];
    z[96]= - n<T>(4,5)*z[15] + n<T>(1,3)*z[93];
    z[96]=z[96]*z[95];
    z[97]=n<T>(1,3)*z[11];
    z[98]=n<T>(53,20) + z[97];
    z[95]=n<T>(1,3)*z[98] - n<T>(1,20)*z[95];
    z[95]=z[5]*z[95];
    z[98]=n<T>(1,3)*z[4];
    z[99]= - n<T>(1,3)*z[3] + n<T>(1,2)*z[2] - z[98];
    z[99]=z[1]*z[99];
    z[100]= - n<T>(61,240)*z[35] - 2*z[37] + n<T>(65,36)*z[36];
    z[101]= - n<T>(3,5)*z[15] + n<T>(1,4)*z[93];
    z[102]= - n<T>(19,3240)*z[5] - z[101];
    z[102]=z[7]*z[102];
    z[93]=n<T>(1,2)*z[93] - n<T>(6,5)*z[15];
    z[93]=9*z[93] + n<T>(133,2160)*z[5];
    z[93]=z[9]*z[93];
    z[101]=11*z[101] + n<T>(53,1080)*z[5];
    z[101]=z[6]*z[101];
    z[93]=z[101] + z[93] + 7*z[102] + n<T>(1,9)*z[95] - n<T>(36,5)*z[15] - 3*
    z[16] + n<T>(3,20)*z[17] - n<T>(7,108)*z[19] - 4*z[22] + n<T>(107,54)*z[23] + 
   n<T>(109,972)*z[24] - n<T>(5,3)*z[27] - n<T>(14,135)*z[28] + n<T>(27,5)*z[30]
     + 2*
    z[31] + n<T>(9,4)*z[33] + n<T>(1,3)*z[100] + z[99] + z[96] + n<T>(1,4)*z[94];
    z[93]=i*z[93];
    z[94]=n<T>(1,2)*z[41];
    z[95]=z[39] - z[44];
    z[96]=z[94] - z[95];
    z[99]=n<T>(1,2)*z[4];
    z[100]=z[38] - z[43];
    z[101]=n<T>(7,32)*z[3] - n<T>(41,32)*z[2] + z[99] + n<T>(31,32)*z[40] - n<T>(11,8)*
    z[42] + n<T>(53,16)*z[14] - z[100] + z[96];
    z[101]=z[6]*z[101];
    z[102]=n<T>(1,2)*z[42];
    z[103]=n<T>(1,2)*z[40];
    z[104]=z[103] - z[102] + n<T>(4,3)*z[14];
    z[105]=z[12]*z[104];
    z[94]= - z[94] + z[104];
    z[94]=z[8]*z[94];
    z[94]= - z[105] + z[94] + z[101] + z[83];
    z[101]=z[100] - z[41];
    z[104]= - z[95] - z[101];
    z[105]=n<T>(7,96)*z[2];
    z[106]=n<T>(7,96)*z[3];
    z[104]= - z[106] - z[105] + z[98] + n<T>(221,96)*z[40] + n<T>(11,24)*z[42]
    + n<T>(1,3)*z[104] + n<T>(37,16)*z[14];
    z[104]=z[9]*z[104];
    z[107]=z[12] + z[11];
    z[108]=n<T>(1,3)*z[45];
    z[109]=n<T>(1,3)*z[46];
    z[110]=1 + z[109];
    z[107]=n<T>(1,2)*z[110] - z[108] + n<T>(1,6)*z[107];
    z[107]=z[3]*z[107];
    z[95]= - z[106] + n<T>(3,32)*z[2] - z[98] + n<T>(17,96)*z[40] + n<T>(5,8)*z[42]
    + n<T>(1,3)*z[95] - n<T>(7,16)*z[14];
    z[95]=z[7]*z[95];
    z[96]=n<T>(31,32)*z[42] - n<T>(37,6)*z[14] - z[96];
    z[96]= - n<T>(3,32)*z[3] + z[105] - z[99] + n<T>(1,3)*z[96] - n<T>(63,32)*z[40];
    z[96]=z[10]*z[96];
    z[99]= - n<T>(3,4) - z[109];
    z[99]=n<T>(1,2)*z[99] + z[108];
    z[99]=z[41]*z[99];
    z[100]= - z[14] + z[100];
    z[100]=z[100]*z[97];
    z[97]=1 - z[97];
    z[97]=z[97]*z[102];
    z[102]=n<T>(19,4) - z[11];
    z[102]=z[102]*z[103];
    z[103]= - z[45] + n<T>(1,2)*z[46];
    z[105]=n<T>(1,2)*z[8] + z[11] + z[103];
    z[98]=z[105]*z[98];
    z[105]=n<T>(1,3)*z[2];
    z[103]= - n<T>(1,2)*z[12] - z[103];
    z[103]=z[103]*z[105];
    z[101]= - z[40] - n<T>(85,48)*z[14] + z[101];
    z[101]=z[105] - n<T>(25,32)*z[42] + n<T>(1,3)*z[101];
    z[101]=z[13]*z[101];
    z[105]=z[62] + z[65] - z[68] - z[69] - z[80] + z[47] + z[48];
    z[106]= - z[49] + z[50] + z[70] - z[71];
    z[108]=z[66] - z[84];
    z[109]=z[59] - z[77];
    z[110]=z[56] - z[76];
    z[111]=z[51] + z[54];
    z[112]=n<T>(1,24)*z[86] - n<T>(7,4)*z[87] - n<T>(7,2)*z[88] - 7*z[89] - n<T>(3,8)*
    z[90] + z[92] + n<T>(1,4)*z[91];

    r +=  - n<T>(3,20)*z[18] + n<T>(12,5)*z[20] + 2*z[21] - n<T>(5,3)*z[25] - n<T>(1,2)
      *z[26] - n<T>(7,16)*z[29] - n<T>(1,20)*z[32] + n<T>(155,192)*z[52] - n<T>(87,64)*
      z[53] - n<T>(1,96)*z[55] + n<T>(29,32)*z[57] - n<T>(3,32)*z[58] + n<T>(9,32)*
      z[60] + n<T>(1,16)*z[61] + n<T>(107,48)*z[63] - n<T>(25,32)*z[64] + n<T>(25,96)*
      z[67] - n<T>(53,192)*z[72] - n<T>(383,192)*z[73] + n<T>(65,64)*z[74] - n<T>(77,192)*z[75]
     - n<T>(5,32)*z[78]
     - n<T>(5,16)*z[79]
     - n<T>(79,24)*z[81]
     - z[82]
       - z[85] + z[93] + n<T>(1,3)*z[94] + z[95] + z[96] + z[97] + z[98] + 
      z[99] + z[100] + z[101] + z[102] + z[103] + z[104] + n<T>(2,3)*z[105]
       + n<T>(2,9)*z[106] + z[107] - n<T>(1,9)*z[108] + n<T>(5,64)*z[109] - n<T>(1,144)
      *z[110] - n<T>(19,192)*z[111] + n<T>(1,4)*z[112];
 
    return r;
}

template std::complex<double> qqb_2lha_tf613(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf613(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
