#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf387(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[86];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[1];
    z[8]=d[4];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[7];
    z[12]=d[16];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[2];
    z[17]=e[4];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=c[3];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[21];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[1];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=d[9];
    z[36]=e[13];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[2];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[10];
    z[44]=f[11];
    z[45]=f[12];
    z[46]=f[13];
    z[47]=f[14];
    z[48]=f[16];
    z[49]=f[17];
    z[50]=f[18];
    z[51]=f[21];
    z[52]=f[30];
    z[53]=f[31];
    z[54]=f[32];
    z[55]=f[33];
    z[56]=f[34];
    z[57]=f[35];
    z[58]=f[36];
    z[59]=f[39];
    z[60]=f[43];
    z[61]=n<T>(2,3)*z[10];
    z[62]=z[61] - z[3];
    z[63]=n<T>(1,3)*z[5];
    z[64]=n<T>(1,2)*z[9];
    z[65]=n<T>(1,3)*z[6];
    z[66]=n<T>(1,2)*z[4] + z[63] - n<T>(5,6)*z[8] - 7*z[2] - z[65] + 4*z[11] + n<T>(8,3)*z[7]
     - z[64]
     + z[13]
     + z[62];
    z[66]=z[4]*z[66];
    z[67]=n<T>(3,2)*z[9];
    z[68]= - z[65] - z[67] + n<T>(1,3)*z[8];
    z[69]= - z[3] + n<T>(1,6)*z[11];
    z[70]=n<T>(2,3)*z[7];
    z[71]= - z[70] + z[12] - z[68] + z[69];
    z[71]=z[8]*z[71];
    z[72]=n<T>(2,3)*z[19];
    z[73]=npow(z[9],2);
    z[74]= - z[18] + z[72] + n<T>(1,4)*z[73];
    z[75]=n<T>(1,3)*z[11];
    z[76]=z[65] - z[14] - z[75];
    z[76]=z[6]*z[76];
    z[77]=n<T>(3,2)*z[2];
    z[78]=z[77] + 5*z[3] + z[65];
    z[78]=z[2]*z[78];
    z[79]=n<T>(4,3)*z[2];
    z[80]=n<T>(1,2)*z[8];
    z[81]=n<T>(1,4)*z[5] - z[80] - z[79] + z[13] + n<T>(7,6)*z[11];
    z[81]=z[5]*z[81];
    z[82]=n<T>(2,3)*z[16];
    z[83]=n<T>(3,2)*z[3];
    z[84]=z[12] - z[83];
    z[84]=z[3]*z[84];
    z[62]=n<T>(1,6)*z[7] - z[9] + z[62];
    z[62]=z[7]*z[62];
    z[85]= - z[14] - 4*z[3];
    z[85]=z[11]*z[85];
    z[62]=z[66] + z[81] + z[71] + z[78] + z[76] + z[85] + z[62] + z[84]
    - 4*z[15] + z[17] + z[20] - z[82] - z[74];
    z[62]=z[1]*z[62];
    z[62]= - n<T>(25,36)*z[22] - 2*z[25] + 6*z[28] + n<T>(7,2)*z[26] + z[62];
    z[62]=i*z[62];
    z[66]= - z[2] - n<T>(7,3)*z[7] - z[68] - z[69];
    z[66]=z[66]*z[80];
    z[68]=npow(z[7],2);
    z[69]=z[68] - z[34] - 5*z[32];
    z[71]=npow(z[11],2);
    z[76]= - z[11]*z[65];
    z[66]=z[66] + z[76] - n<T>(1,12)*z[71] + n<T>(3,4)*z[73] - n<T>(13,36)*z[21]
     - 
    z[17] - n<T>(1,3)*z[69];
    z[66]=z[8]*z[66];
    z[69]=npow(z[3],2);
    z[71]=2*z[11];
    z[76]=z[3]*z[71];
    z[72]=z[76] + 2*z[69] + n<T>(7,12)*z[21] + z[72] - z[20];
    z[72]=z[11]*z[72];
    z[76]=z[57] - z[60];
    z[78]=z[54] + z[56];
    z[61]=z[12] - z[14] + z[61] + z[13];
    z[61]=z[21]*z[61];
    z[81]=n<T>(1,3)*z[21];
    z[84]=z[33] - z[81];
    z[84]=z[9]*z[84];
    z[85]=n<T>(1,6)*z[69] - n<T>(13,6)*z[21] - z[17] + 2*z[15];
    z[85]=z[3]*z[85];
    z[61]=z[61] - n<T>(8,3)*z[55] + n<T>(1,12)*z[58] - n<T>(1,4)*z[59] - n<T>(1,2)*z[78]
    - n<T>(1,3)*z[76] + z[62] - z[37] - z[43] + z[51] + z[84] + z[85] + 
    z[72] + z[66];
    z[62]=z[64] + z[3];
    z[66]=n<T>(1,6)*z[6];
    z[72]=n<T>(5,12)*z[8];
    z[62]= - n<T>(5,6)*z[4] + z[63] + z[72] - z[77] + z[66] + z[71] + n<T>(1,2)*
    z[62] - z[70];
    z[62]=z[4]*z[62];
    z[63]=z[72] - z[67] + z[70];
    z[63]=z[8]*z[63];
    z[67]=z[82] + n<T>(1,2)*z[69];
    z[70]= - 2*z[3] - z[11];
    z[70]=z[70]*z[71];
    z[71]=z[11] + n<T>(1,2)*z[6];
    z[71]=z[71]*z[65];
    z[72]=z[9] - n<T>(1,3)*z[7];
    z[72]=z[7]*z[72];
    z[76]=n<T>(7,2)*z[2] - z[65] + 3*z[3] - 2*z[7];
    z[76]=z[2]*z[76];
    z[62]=z[62] + z[63] + z[76] + z[71] + z[70] + z[72] - n<T>(43,36)*z[21]
    + 4*z[31] + z[67] + z[74];
    z[63]=1 + n<T>(1,33)*z[6];
    z[70]=n<T>(1,11)*z[7];
    z[71]=n<T>(1,33)*z[11];
    z[72]=n<T>(7,66)*z[5] - n<T>(5,33)*z[2] - z[71] + z[70] - z[63];
    z[72]=z[5]*z[72];
    z[62]=z[72] + n<T>(1,11)*z[62];
    z[62]=z[4]*z[62];
    z[65]=z[79] + z[65] - z[7] + z[75];
    z[72]=n<T>(1,11)*z[2];
    z[65]=z[65]*z[72];
    z[74]=1 - n<T>(7,132)*z[11];
    z[74]=z[11]*z[74];
    z[75]= - 1 - n<T>(5,66)*z[6];
    z[75]=z[6]*z[75];
    z[76]=z[11] - z[80];
    z[76]=z[8]*z[76];
    z[77]=z[8] + z[11];
    z[77]=n<T>(17,198)*z[5] - n<T>(7,66)*z[2] - n<T>(2,33)*z[6] + 1 - n<T>(1,44)*z[77];
    z[77]=z[5]*z[77];
    z[65]=z[77] + n<T>(1,22)*z[76] + z[65] + z[75] + z[74] + n<T>(5,66)*z[21] + 
   n<T>(7,33)*z[19] - z[35] - n<T>(1,11)*z[18];
    z[65]=z[5]*z[65];
    z[64]= - z[64] + n<T>(5,3)*z[7];
    z[64]=z[7]*z[64];
    z[64]=z[64] - z[73] + z[81] + z[33] + n<T>(5,3)*z[32] + z[67];
    z[64]=z[64]*z[70];
    z[67]= - n<T>(1,3)*z[19] - 2*z[31] + z[15];
    z[67]= - n<T>(1,2)*z[68] - n<T>(5,2)*z[69] + 2*z[67] + n<T>(1,6)*z[21];
    z[63]= - z[71] + z[63];
    z[63]=z[6]*z[63];
    z[66]= - n<T>(7,6)*z[2] + z[66] - z[83] + z[7];
    z[66]=z[66]*z[72];
    z[63]=z[66] + n<T>(1,11)*z[67] + z[63];
    z[63]=z[2]*z[63];
    z[66]= - 1 + n<T>(2,33)*z[11];
    z[66]=z[11]*z[66];
    z[67]=z[11] + n<T>(7,3)*z[6];
    z[67]=z[6]*z[67];
    z[66]=n<T>(1,66)*z[67] + z[66] - n<T>(1,99)*z[21] + n<T>(5,33)*z[19] - n<T>(1,11)*
    z[20] + z[35] + n<T>(1,33)*z[34];
    z[66]=z[6]*z[66];
    z[67]= - z[52] + z[24] - z[50];
    z[68]= - z[49] + z[29] + z[44];
    z[69]= - z[47] + z[38] + z[40];
    z[70]=z[42] - z[46];

    r +=  - n<T>(5,33)*z[23] - n<T>(5,66)*z[27] + n<T>(41,33)*z[30] + 2*z[36] + n<T>(2,33)*z[39]
     + n<T>(1,44)*z[41] - n<T>(5,132)*z[45]
     + n<T>(3,44)*z[48] - n<T>(1,12)*
      z[53] + n<T>(1,11)*z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + n<T>(1,33)*z[67]
     - n<T>(2,11)*z[68]
     + n<T>(1,22)*z[69] - n<T>(3,11)*z[70];
 
    return r;
}

template std::complex<double> qqb_2lha_tf387(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf387(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
