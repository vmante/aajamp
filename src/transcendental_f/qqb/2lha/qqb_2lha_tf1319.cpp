#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1319(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[79];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=d[6];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[7];
    z[10]=d[9];
    z[11]=d[2];
    z[12]=d[11];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=d[18];
    z[16]=e[5];
    z[17]=e[7];
    z[18]=e[9];
    z[19]=e[10];
    z[20]=e[11];
    z[21]=e[12];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=e[3];
    z[26]=e[6];
    z[27]=e[13];
    z[28]=e[14];
    z[29]=f[4];
    z[30]=f[6];
    z[31]=f[7];
    z[32]=f[10];
    z[33]=f[12];
    z[34]=f[16];
    z[35]=f[23];
    z[36]=f[30];
    z[37]=f[31];
    z[38]=f[32];
    z[39]=f[33];
    z[40]=f[35];
    z[41]=f[36];
    z[42]=f[39];
    z[43]=f[43];
    z[44]=f[51];
    z[45]=f[52];
    z[46]=f[54];
    z[47]=f[55];
    z[48]=f[56];
    z[49]=f[58];
    z[50]=z[6] + z[4];
    z[51]=z[50]*z[9];
    z[52]=z[1]*i;
    z[53]=z[52] + 2*z[4];
    z[53]=z[4]*z[53];
    z[54]=z[52] + z[6];
    z[54]=z[6]*z[54];
    z[55]= - z[7]*z[50];
    z[56]=z[52] - z[9];
    z[57]=z[56] - z[7];
    z[58]= - 2*z[3] + z[6] - z[57];
    z[58]=z[3]*z[58];
    z[57]= - z[5] - z[4] - z[57];
    z[57]=z[5]*z[57];
    z[50]= - z[5] - z[3] + z[50];
    z[50]=z[2]*z[50];
    z[50]=n<T>(1,2)*z[50] + z[57] + z[58] + z[55] - z[51] + z[53] + z[54];
    z[50]=z[2]*z[50];
    z[53]=z[2] - z[9];
    z[54]=z[53] - z[7];
    z[55]=2*z[52];
    z[54]= - n<T>(13,2)*z[5] + z[55] - n<T>(17,2)*z[4] + 2*z[54];
    z[54]=z[17]*z[54];
    z[57]=z[23]*i;
    z[50]=z[57] - z[43] + z[40] - z[35] - z[24] + z[50] + z[54];
    z[54]=npow(z[4],2);
    z[57]= - n<T>(13,4)*z[52] - n<T>(23,3)*z[4];
    z[57]=z[57]*z[54];
    z[58]=n<T>(1,2)*z[4];
    z[59]=z[58] + z[52];
    z[60]=z[59]*z[4];
    z[61]=n<T>(7,6)*z[52];
    z[62]= - z[61] + z[4];
    z[62]=n<T>(1,2)*z[62] - n<T>(5,9)*z[6];
    z[62]=z[6]*z[62];
    z[62]=z[60] + z[62];
    z[62]=z[6]*z[62];
    z[63]= - z[4] - z[7];
    z[63]=z[52]*z[63];
    z[63]= - z[22] + z[63];
    z[63]=z[13]*z[63];
    z[64]=z[52] - z[7];
    z[65]=z[4] - z[64];
    z[65]=z[16]*z[65];
    z[66]= - z[3] - z[8];
    z[66]=z[25]*z[66];
    z[67]= - z[5] - z[8];
    z[67]=z[26]*z[67];
    z[57]=z[66] + z[67] - z[47] + z[36] + z[32] + z[65] + z[63] + n<T>(1,3)*
    z[57] + z[62];
    z[62]=n<T>(1,2)*z[7];
    z[63]=n<T>(1,2)*z[3];
    z[65]=n<T>(1,2)*z[9];
    z[66]=n<T>(1,2)*z[6];
    z[67]= - z[63] + z[62] + z[65] + z[66] - z[59];
    z[67]=z[3]*z[67];
    z[68]= - z[66] + z[52] + z[4];
    z[69]= - z[7] + z[68];
    z[69]=z[66]*z[69];
    z[70]=n<T>(1,3)*z[4];
    z[71]= - z[55] - z[4];
    z[71]=z[71]*z[70];
    z[72]=z[65] - z[52];
    z[73]= - z[72]*z[65];
    z[74]=z[6] - z[4];
    z[75]=z[7] - z[9];
    z[76]=z[75] + z[74];
    z[76]=z[10]*z[76];
    z[67]=n<T>(1,4)*z[76] + z[67] + z[73] + z[71] + z[69];
    z[67]=z[10]*z[67];
    z[69]=n<T>(1,3)*z[6];
    z[71]=n<T>(1,3)*z[7];
    z[61]= - n<T>(7,9)*z[3] + z[71] - n<T>(1,6)*z[9] + z[69] + z[61] + z[4];
    z[61]=z[61]*z[63];
    z[73]=n<T>(1,3)*z[52];
    z[76]=n<T>(7,12)*z[6] + z[73] - z[58];
    z[76]=z[6]*z[76];
    z[77]=n<T>(1,3)*z[9];
    z[78]= - n<T>(5,4)*z[9] + n<T>(5,2)*z[52] - z[6];
    z[77]=z[78]*z[77];
    z[78]= - z[55] - z[6] + z[75];
    z[78]=z[78]*z[71];
    z[61]=z[61] + z[78] + z[76] + z[77];
    z[61]=z[3]*z[61];
    z[76]=n<T>(5,6)*z[6];
    z[53]= - z[10] - z[63] - z[71] - z[76] + z[73] + z[4] - n<T>(2,3)*z[53];
    z[53]=z[19]*z[53];
    z[73]= - z[76] - z[70] - z[64];
    z[73]=z[7]*z[73];
    z[76]=n<T>(2,3)*z[4];
    z[77]=z[52] - z[4];
    z[77]=z[77]*z[76];
    z[78]= - n<T>(5,12)*z[6] + n<T>(5,6)*z[52] + z[4];
    z[78]=z[78]*z[66];
    z[51]=n<T>(1,4)*z[73] + n<T>(1,3)*z[51] + z[77] + z[78];
    z[51]=z[7]*z[51];
    z[73]=z[52] - n<T>(5,3)*z[9];
    z[73]=z[73]*z[65];
    z[77]= - z[62] + z[56] + z[4];
    z[71]=z[77]*z[71];
    z[77]= - z[52] + n<T>(13,2)*z[4];
    z[77]= - n<T>(11,18)*z[5] - n<T>(5,6)*z[7] + n<T>(1,3)*z[77] - z[65];
    z[78]=n<T>(1,2)*z[5];
    z[77]=z[77]*z[78];
    z[54]=z[77] + z[71] + n<T>(2,3)*z[54] + z[73];
    z[54]=z[5]*z[54];
    z[71]= - z[52] - z[66];
    z[71]=z[6]*z[71];
    z[60]=z[60] + z[71];
    z[71]= - 3*z[4] + z[72];
    z[71]=z[9]*z[71];
    z[62]=3*z[6] - z[62] + z[52];
    z[62]=z[7]*z[62];
    z[60]=z[62] + 3*z[60] + z[71];
    z[62]= - 3*z[74] - z[75];
    z[71]=n<T>(1,2)*z[8];
    z[62]= - z[71] - z[78] + n<T>(1,4)*z[62] + z[3];
    z[62]=z[8]*z[62];
    z[63]=z[63] + z[64];
    z[63]=z[3]*z[63];
    z[64]= - z[5]*z[56];
    z[60]=z[62] + z[64] + n<T>(1,2)*z[60] + z[63];
    z[60]=z[60]*z[71];
    z[58]= - z[52] + z[58];
    z[58]=z[58]*z[70];
    z[62]= - z[6]*z[68];
    z[63]=z[11] + z[10];
    z[59]=z[63] - z[59];
    z[59]=z[66] - n<T>(1,3)*z[59];
    z[59]=z[11]*z[59];
    z[64]=z[10]*z[76];
    z[58]=z[59] + z[64] + z[58] + z[62];
    z[58]=z[11]*z[58];
    z[59]=z[52]*z[63];
    z[59]=z[22] + z[59];
    z[59]=z[12]*z[59];
    z[62]=z[52] - z[63];
    z[62]=z[21]*z[62];
    z[59]= - z[46] + z[59] + z[62];
    z[55]=z[55] - z[6];
    z[55]=z[55]*z[69];
    z[62]=z[69] + n<T>(17,12)*z[4] - z[56];
    z[62]=z[62]*z[65];
    z[63]= - n<T>(17,3)*z[52] + n<T>(3,2)*z[4];
    z[63]=z[4]*z[63];
    z[55]=z[62] + n<T>(1,4)*z[63] + z[55];
    z[55]=z[9]*z[55];
    z[62]= - n<T>(7,2)*z[4] - z[6];
    z[62]=5*z[62] - n<T>(11,4)*z[9];
    z[62]=z[11] + n<T>(13,6)*z[10] + n<T>(7,6)*z[5] + n<T>(5,3)*z[3] + n<T>(1,3)*z[62]
    - n<T>(7,4)*z[7];
    z[62]=z[22]*z[62];
    z[63]= - z[4] - z[10];
    z[63]=z[27]*z[63];
    z[62]=z[44] + z[62] + z[63];
    z[63]=z[52]*z[9];
    z[63]=z[63] + z[22];
    z[64]=z[5]*z[52];
    z[64]=z[64] + z[63];
    z[64]=z[14]*z[64];
    z[65]=z[5] - z[56];
    z[65]=z[18]*z[65];
    z[64]=z[64] + z[65];
    z[52]= - z[6]*z[52];
    z[52]=z[52] - z[63];
    z[52]=z[15]*z[52];
    z[63]= - z[45] + z[49] - z[48];
    z[65]= - z[29] + z[41] - z[33];
    z[66]=z[38] + z[30];
    z[68]=z[42] - z[34];
    z[56]=z[56] - z[6];
    z[56]= - z[20]*z[56];
    z[69]= - z[6] - z[10];
    z[69]=z[28]*z[69];

    r +=  - n<T>(4,3)*z[31] + n<T>(25,24)*z[37] + n<T>(8,3)*z[39] + n<T>(1,3)*z[50]
     +  z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + n<T>(1,2)*z[57] + 
      z[58] + n<T>(2,3)*z[59] + z[60] + z[61] + n<T>(1,6)*z[62] + n<T>(1,4)*z[63]
       + n<T>(7,6)*z[64] - n<T>(1,8)*z[65] + n<T>(3,4)*z[66] + z[67] + n<T>(3,8)*z[68]
       + z[69];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1319(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1319(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
