#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf598(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[54];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[2];
    z[6]=d[8];
    z[7]=d[11];
    z[8]=d[3];
    z[9]=d[9];
    z[10]=d[12];
    z[11]=d[4];
    z[12]=d[7];
    z[13]=d[17];
    z[14]=e[5];
    z[15]=e[7];
    z[16]=e[9];
    z[17]=e[12];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[31];
    z[22]=e[6];
    z[23]=e[14];
    z[24]=f[30];
    z[25]=f[31];
    z[26]=f[32];
    z[27]=f[33];
    z[28]=f[35];
    z[29]=f[36];
    z[30]=f[39];
    z[31]=f[43];
    z[32]=f[50];
    z[33]=f[51];
    z[34]=f[52];
    z[35]=f[55];
    z[36]=f[58];
    z[37]=z[12] + z[8];
    z[38]=z[4] - z[3];
    z[37]= - z[38]*z[37];
    z[39]=z[1]*i;
    z[40]=z[39]*z[3];
    z[41]=npow(z[3],2);
    z[42]=z[39] + z[3];
    z[43]=z[4] + z[42];
    z[43]=z[4]*z[43];
    z[37]=z[43] - 2*z[41] - z[40] + z[37];
    z[43]=z[2]*z[38];
    z[37]=2*z[37] + z[43];
    z[37]=z[2]*z[37];
    z[43]= - 13*z[3] + n<T>(17,3)*z[4];
    z[43]=z[4]*z[43];
    z[43]= - 11*z[41] + z[43];
    z[44]=n<T>(1,2)*z[4];
    z[43]=z[43]*z[44];
    z[45]=z[5] + z[9];
    z[46]=z[39]*z[45];
    z[46]=z[18] + z[46];
    z[46]=z[7]*z[46];
    z[45]=z[39] - z[45];
    z[45]=z[17]*z[45];
    z[47]=npow(z[3],3);
    z[48]=z[4] + z[11];
    z[48]=z[22]*z[48];
    z[49]= - z[9] - z[6];
    z[49]=z[23]*z[49];
    z[37]=z[45] + z[48] + z[49] + z[32] - z[24] + z[46] + z[37] + n<T>(17,3)
   *z[47] + z[43];
    z[43]=n<T>(1,3)*z[39];
    z[45]=z[43] + z[3];
    z[45]=n<T>(1,2)*z[45];
    z[46]=n<T>(1,12)*z[5];
    z[47]=n<T>(1,3)*z[9];
    z[48]=z[46] - z[45] + z[47];
    z[48]=z[5]*z[48];
    z[49]=n<T>(1,2)*z[41];
    z[50]=z[49] + z[40];
    z[50]=n<T>(1,2)*z[50];
    z[51]= - z[39]*z[47];
    z[52]=z[43] - z[3];
    z[52]=n<T>(1,2)*z[52];
    z[53]= - n<T>(1,3)*z[6] + n<T>(1,6)*z[5] - z[52] - z[47];
    z[53]=z[6]*z[53];
    z[48]=n<T>(1,2)*z[53] + z[48] + z[50] + z[51];
    z[48]=z[6]*z[48];
    z[51]=n<T>(1,3)*z[4];
    z[53]=z[39]*z[51];
    z[45]= - n<T>(1,12)*z[12] + z[45] - z[51];
    z[45]=z[12]*z[45];
    z[52]=n<T>(1,3)*z[11] - n<T>(1,6)*z[12] + z[52] + z[51];
    z[52]=z[11]*z[52];
    z[45]=n<T>(1,2)*z[52] + z[45] - z[50] + z[53];
    z[45]=z[11]*z[45];
    z[50]= - z[49] + n<T>(11,3)*z[40];
    z[44]= - z[39] + z[44];
    z[44]=z[44]*z[51];
    z[38]=z[8]*z[38];
    z[51]= - n<T>(11,12)*z[3] + z[4];
    z[51]=z[12]*z[51];
    z[38]=z[51] + n<T>(2,3)*z[38] + n<T>(1,2)*z[50] + z[44];
    z[38]=z[12]*z[38];
    z[42]=z[4]*z[42];
    z[44]=n<T>(1,2)*z[8] - z[39] - z[4];
    z[44]=z[8]*z[44];
    z[50]=z[39] - z[8];
    z[51]= - z[3] - z[50];
    z[51]=z[9]*z[51];
    z[42]=n<T>(1,2)*z[51] + z[44] - z[49] + z[42];
    z[42]=z[9]*z[42];
    z[44]=z[12] + z[4];
    z[51]= - z[39]*z[44];
    z[51]= - z[18] + z[51];
    z[51]=z[13]*z[51];
    z[44]=z[39] - z[44];
    z[44]=z[16]*z[44];
    z[44]=z[51] + z[44];
    z[43]= - n<T>(1,3)*z[5] - z[47] + n<T>(1,2)*z[3] + z[43];
    z[43]=z[5]*z[43];
    z[43]=z[43] + z[49] - z[40];
    z[43]=z[5]*z[43];
    z[43]=z[43] - z[34];
    z[47]=z[11] - z[6];
    z[49]=5*z[3] - n<T>(13,3)*z[4];
    z[49]= - n<T>(5,18)*z[12] + n<T>(1,3)*z[49] + z[8];
    z[46]=z[46] + n<T>(1,2)*z[49] - n<T>(4,9)*z[9] + n<T>(1,9)*z[47];
    z[46]=z[18]*z[46];
    z[47]=z[3] + z[8];
    z[47]=z[39]*z[47];
    z[47]=z[18] + z[47];
    z[47]=z[10]*z[47];
    z[49]= - z[3] + z[50];
    z[49]=z[14]*z[49];
    z[47]=z[47] + z[49];
    z[40]=z[41] - z[40];
    z[39]=n<T>(5,2)*z[4] - 2*z[3] - 5*z[39];
    z[39]=z[4]*z[39];
    z[39]=4*z[40] + z[39];
    z[40]=n<T>(5,6)*z[4] - n<T>(1,3)*z[3] + z[50];
    z[40]=z[8]*z[40];
    z[39]=n<T>(1,3)*z[39] + z[40];
    z[39]=z[8]*z[39];
    z[40]= - z[30] + z[36] + z[33];
    z[41]= - z[12] + z[50] + z[2];
    z[41]=5*z[4] + 7*z[3] - 2*z[41];
    z[41]=z[15]*z[41];
    z[41]= - z[41] - z[31] + z[28] + z[20];
    z[49]=z[35] - z[29];
    z[50]=n<T>(1,3)*i;
    z[50]= - z[19]*z[50];

    r += 7*z[21] - n<T>(19,12)*z[25] - n<T>(3,2)*z[26] - n<T>(16,3)*z[27] + n<T>(1,3)*
      z[37] + z[38] + z[39] + n<T>(1,4)*z[40] - n<T>(2,3)*z[41] + z[42] + n<T>(1,2)
      *z[43] + n<T>(5,3)*z[44] + z[45] + z[46] + 2*z[47] + z[48] - n<T>(1,12)*
      z[49] + z[50];
 
    return r;
}

template std::complex<double> qqb_2lha_tf598(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf598(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
