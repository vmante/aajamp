#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1339(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[114];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[19];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[31];
    z[38]=e[3];
    z[39]=e[6];
    z[40]=e[13];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[7];
    z[50]=f[10];
    z[51]=f[11];
    z[52]=f[12];
    z[53]=f[14];
    z[54]=f[16];
    z[55]=f[17];
    z[56]=f[18];
    z[57]=f[23];
    z[58]=f[30];
    z[59]=f[31];
    z[60]=f[32];
    z[61]=f[33];
    z[62]=f[35];
    z[63]=f[36];
    z[64]=f[37];
    z[65]=f[39];
    z[66]=f[43];
    z[67]=f[50];
    z[68]=f[51];
    z[69]=f[52];
    z[70]=f[53];
    z[71]=f[54];
    z[72]=f[55];
    z[73]=f[56];
    z[74]=f[58];
    z[75]=f[72];
    z[76]=f[73];
    z[77]=z[1]*i;
    z[78]=z[77] - z[4];
    z[79]=2*z[2];
    z[80]=z[79] + z[7];
    z[81]=n<T>(3,8)*z[6];
    z[82]=z[8] + z[5];
    z[83]= - n<T>(29,24)*z[9] + z[3] + z[81] - z[82] + z[80] + n<T>(19,12)*z[78]
   ;
    z[83]=z[4]*z[83];
    z[84]=n<T>(1,2)*z[6];
    z[85]= - z[77] + z[84];
    z[85]=z[6]*z[85];
    z[86]=5*z[6];
    z[87]= - n<T>(29,6)*z[9] + n<T>(29,3)*z[77] + z[86];
    z[87]=z[9]*z[87];
    z[88]=2*z[77];
    z[89]=z[88] - z[5];
    z[90]=z[89]*z[5];
    z[91]=z[77] + z[5];
    z[92]=2*z[91] - z[8];
    z[92]=z[8]*z[92];
    z[93]= - z[88] + z[3];
    z[93]=z[3]*z[93];
    z[94]=4*z[77];
    z[95]= - z[2]*z[94];
    z[96]= - z[77] - z[3];
    z[96]=2*z[96] + z[7];
    z[96]=z[7]*z[96];
    z[83]=z[83] + n<T>(1,4)*z[87] + z[96] + z[95] + z[93] + n<T>(3,4)*z[85] + 
    z[90] + z[92];
    z[83]=z[4]*z[83];
    z[85]=7*z[5];
    z[87]= - z[10] + n<T>(43,4)*z[77] - z[85];
    z[92]=n<T>(5,4)*z[6];
    z[93]=5*z[9];
    z[80]=2*z[4] - z[93] + n<T>(22,3)*z[3] + z[92] - n<T>(5,4)*z[8] - z[80] + n<T>(1,3)*z[87];
    z[80]=z[10]*z[80];
    z[87]=z[77] - z[8];
    z[95]= - z[84] - z[87];
    z[95]=z[95]*z[84];
    z[96]=2*z[7];
    z[97]= - z[7] - z[87];
    z[97]=z[97]*z[96];
    z[98]=z[77] - z[5];
    z[99]=2*z[98];
    z[100]= - z[99] - z[9];
    z[93]=z[100]*z[93];
    z[100]=z[77]*z[4];
    z[101]=z[77] - n<T>(1,2)*z[8];
    z[101]=z[8]*z[101];
    z[102]=z[99] + z[3];
    z[102]=z[3]*z[102];
    z[80]=z[80] - 4*z[100] + z[93] + z[97] + n<T>(8,3)*z[102] + z[95] + n<T>(7,3)
   *z[90] + n<T>(5,2)*z[101];
    z[80]=z[10]*z[80];
    z[93]=2*z[5];
    z[95]=3*z[77] - z[93];
    z[97]=n<T>(9,4)*z[6];
    z[95]=2*z[95] + z[97];
    z[95]=z[6]*z[95];
    z[101]= - z[99] - z[7];
    z[101]=z[101]*z[96];
    z[102]=n<T>(7,6)*z[9];
    z[103]= - z[77] - n<T>(1,2)*z[9];
    z[103]=z[103]*z[102];
    z[104]=2*z[6];
    z[102]=n<T>(5,12)*z[4] - z[104] + z[102];
    z[102]=z[4]*z[102];
    z[96]=n<T>(3,4)*z[11] - 2*z[9] - z[96] + z[3] + z[97] - z[5] + z[8];
    z[96]=z[11]*z[96];
    z[97]=z[88] - z[8];
    z[105]= - z[8]*z[97];
    z[106]=2*z[87] + z[3];
    z[106]=z[3]*z[106];
    z[95]=z[96] + z[102] + z[103] + z[101] + z[106] + z[105] + z[95];
    z[95]=z[11]*z[95];
    z[96]=n<T>(2,3)*z[5];
    z[101]=n<T>(5,3)*z[2];
    z[102]=n<T>(3,2)*z[77];
    z[103]=n<T>(43,9)*z[7] + z[101] + z[3] - n<T>(13,6)*z[6] + n<T>(1,6)*z[8] + 
    z[102] - z[96];
    z[103]=z[7]*z[103];
    z[105]=2*z[8];
    z[106]= - z[77] + 5*z[5];
    z[106]=n<T>(1,3)*z[106] + z[105];
    z[106]=z[8]*z[106];
    z[107]=n<T>(1,3)*z[6];
    z[108]= - n<T>(25,2)*z[6] + 2*z[89] - 7*z[8];
    z[108]=z[108]*z[107];
    z[109]=z[82] - z[77];
    z[110]=n<T>(1,2)*z[2] + z[6] - z[109];
    z[101]=z[110]*z[101];
    z[110]=z[88] + z[3];
    z[110]=z[3]*z[110];
    z[101]=z[103] + z[101] + z[110] + z[108] + n<T>(5,3)*z[90] + z[106];
    z[101]=z[7]*z[101];
    z[100]=z[100] + z[29];
    z[103]=z[10]*z[77];
    z[103]=z[103] + z[100];
    z[103]=z[14]*z[103];
    z[106]=z[77]*z[8];
    z[106]=z[106] + z[29];
    z[108]=z[9]*z[77];
    z[108]=z[108] + z[106];
    z[108]=z[17]*z[108];
    z[110]=z[88] - z[2];
    z[111]= - z[4] + z[110];
    z[111]=z[18]*z[111];
    z[112]= - z[10] + z[78];
    z[112]=z[21]*z[112];
    z[113]= - z[9] + z[87];
    z[113]=z[27]*z[113];
    z[103]=z[103] + z[108] + z[111] + z[112] + z[113] - z[42];
    z[108]= - z[3] + z[109];
    z[108]=2*z[108] - z[2];
    z[108]=z[2]*z[108];
    z[111]=z[8] + z[98];
    z[111]=2*z[111] + z[3];
    z[111]=z[3]*z[111];
    z[108]=z[111] + z[108];
    z[111]= - z[88] - z[5];
    z[111]=2*z[111] - z[8];
    z[111]=z[111]*z[105];
    z[111]=13*z[90] + z[111];
    z[84]= - z[77] - z[84];
    z[84]=z[84]*z[92];
    z[92]=4*z[8] + n<T>(133,8)*z[77] - 13*z[5];
    z[81]=n<T>(103,36)*z[9] - n<T>(4,3)*z[2] + n<T>(28,3)*z[3] + n<T>(1,3)*z[92]
     + z[81]
   ;
    z[81]=z[9]*z[81];
    z[81]=z[81] + n<T>(1,3)*z[111] + z[84] + n<T>(2,3)*z[108];
    z[81]=z[9]*z[81];
    z[84]=2*z[3];
    z[92]=z[84] - z[109];
    z[92]=z[3]*z[92];
    z[97]= - z[5] - z[97];
    z[97]=z[8]*z[97];
    z[90]=z[92] - z[90] + z[97];
    z[92]= - z[104] + z[109];
    z[86]=z[92]*z[86];
    z[82]=z[94] + z[82];
    z[82]= - 4*z[2] + z[84] + 2*z[82] - n<T>(5,2)*z[6];
    z[82]=z[2]*z[82];
    z[82]=z[82] + z[86] + 4*z[90];
    z[84]=n<T>(1,3)*z[2];
    z[82]=z[82]*z[84];
    z[79]= - 13*z[9] + z[79] - 2*z[109] - 11*z[3];
    z[79]=z[26]*z[79];
    z[86]= - z[2] + z[89];
    z[86]=z[19]*z[86];
    z[90]= - z[8] + z[110];
    z[90]=z[24]*z[90];
    z[79]= - z[90] - z[79] - z[86] - z[57] + z[56] + z[47] - z[34];
    z[86]=z[77]*z[5];
    z[86]=z[86] + z[29];
    z[90]= - z[6]*z[77];
    z[90]=z[90] - z[86];
    z[90]=z[15]*z[90];
    z[92]=z[6] - z[98];
    z[92]=z[22]*z[92];
    z[94]=z[7] + z[10];
    z[94]=z[39]*z[94];
    z[90]=z[90] + z[92] + z[94] + z[71] + z[58] + z[43];
    z[92]=n<T>(1,3)*z[5];
    z[88]= - z[105] + z[88] - z[92];
    z[88]=z[8]*z[88];
    z[89]=z[89]*z[92];
    z[88]=z[89] + z[88];
    z[88]=z[8]*z[88];
    z[89]=z[91]*z[96];
    z[91]=n<T>(101,12)*z[6] + n<T>(11,4)*z[8] + n<T>(41,8)*z[77] + z[5];
    z[91]=z[91]*z[107];
    z[94]= - n<T>(3,4)*z[8] + z[102] - n<T>(5,3)*z[5];
    z[94]=z[8]*z[94];
    z[89]=z[91] + z[89] + z[94];
    z[89]=z[6]*z[89];
    z[91]= - 11*z[77] + 10*z[5];
    z[91]=z[91]*z[93];
    z[93]= - 5*z[77] + z[93];
    z[93]=2*z[93] + 5*z[8];
    z[93]=z[8]*z[93];
    z[91]=z[91] + z[93];
    z[93]=z[85] - z[87];
    z[93]= - n<T>(115,9)*z[3] + n<T>(1,3)*z[93] - z[104];
    z[93]=z[3]*z[93];
    z[94]= - z[99] - z[6];
    z[94]=z[6]*z[94];
    z[91]=z[93] + n<T>(1,3)*z[91] + z[94];
    z[91]=z[3]*z[91];
    z[93]= - z[7]*z[77];
    z[93]=z[93] - z[106];
    z[93]=z[16]*z[93];
    z[87]= - z[7] + z[87];
    z[87]=z[25]*z[87];
    z[87]=z[93] + z[87];
    z[93]= - z[11]*z[77];
    z[93]=z[93] - z[100];
    z[93]=z[13]*z[93];
    z[78]=z[11] - z[78];
    z[78]=z[28]*z[78];
    z[78]=z[93] + z[78];
    z[93]= - z[3]*z[77];
    z[86]=z[93] - z[86];
    z[86]=z[12]*z[86];
    z[93]= - z[3] + z[98];
    z[93]=z[20]*z[93];
    z[86]=z[86] + z[93];
    z[77]=z[77] + z[92];
    z[77]=z[77]*npow(z[5],2);
    z[84]= - z[84] + n<T>(1,3)*z[109] + z[6];
    z[84]=10*z[84] + n<T>(11,3)*z[7];
    z[84]=z[23]*z[84];
    z[92]=z[6] + n<T>(1,2)*z[11];
    z[92]=z[40]*z[92];
    z[92]= - z[48] + z[92] - z[70];
    z[93]=z[46] - z[36];
    z[94]=z[62] - z[52];
    z[96]= - n<T>(40,3)*z[35] - n<T>(8,3)*z[33] - n<T>(41,72)*z[30];
    z[96]=i*z[96];
    z[85]=n<T>(241,4)*z[9] + n<T>(17,2)*z[7] - 16*z[2] - 28*z[3] + 13*z[6] + 
    z[85] + n<T>(29,4)*z[8];
    z[85]= - n<T>(41,36)*z[11] + n<T>(83,18)*z[10] + n<T>(1,9)*z[85] + n<T>(9,8)*z[4];
    z[85]=z[29]*z[85];
    z[97]= - z[3] - z[10];
    z[97]=z[38]*z[97];
    z[98]=z[9] + z[11];
    z[98]=z[41]*z[98];

    r += n<T>(9,2)*z[31] + n<T>(16,9)*z[32] + n<T>(16,3)*z[37] - n<T>(22,3)*z[44]
     - 
      z[45] - n<T>(32,3)*z[49] + n<T>(8,3)*z[50] - n<T>(1,3)*z[51] + z[53] - 9*
      z[54] - z[55] - n<T>(29,12)*z[59] + z[60] - n<T>(58,3)*z[61] + n<T>(5,4)*
      z[63] + z[64] + n<T>(1,4)*z[65] + n<T>(5,3)*z[66] + n<T>(7,6)*z[67] + n<T>(3,8)*
      z[68] + z[69] + n<T>(29,24)*z[72] + z[73] - n<T>(5,8)*z[74] - z[75] + 
      z[76] + z[77] + n<T>(5,6)*z[78] - n<T>(4,3)*z[79] + z[80] + z[81] + z[82]
       + z[83] + z[84] + z[85] + 6*z[86] + n<T>(11,3)*z[87] + z[88] + z[89]
       + 2*z[90] + z[91] + 3*z[92] - n<T>(20,3)*z[93] + n<T>(2,3)*z[94] + z[95]
       + z[96] + n<T>(28,3)*z[97] + n<T>(17,6)*z[98] + z[101] + 4*z[103];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1339(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1339(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
