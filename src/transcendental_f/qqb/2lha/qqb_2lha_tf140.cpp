#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf140(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[103];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=d[5];
    z[11]=d[6];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[9];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[6];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[14];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[19];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=e[3];
    z[39]=e[7];
    z[40]=e[13];
    z[41]=f[0];
    z[42]=f[1];
    z[43]=f[2];
    z[44]=f[3];
    z[45]=f[4];
    z[46]=f[5];
    z[47]=f[6];
    z[48]=f[7];
    z[49]=f[11];
    z[50]=f[12];
    z[51]=f[14];
    z[52]=f[16];
    z[53]=f[17];
    z[54]=f[18];
    z[55]=f[23];
    z[56]=f[31];
    z[57]=f[32];
    z[58]=f[35];
    z[59]=f[36];
    z[60]=f[37];
    z[61]=f[38];
    z[62]=f[39];
    z[63]=f[50];
    z[64]=f[51];
    z[65]=f[52];
    z[66]=f[54];
    z[67]=f[55];
    z[68]=f[56];
    z[69]=f[57];
    z[70]=f[58];
    z[71]=n<T>(1,2)*z[6];
    z[72]=z[1]*i;
    z[73]=z[71] - z[72];
    z[74]=z[73]*z[6];
    z[75]=n<T>(1,2)*z[5];
    z[76]=z[75] - z[72];
    z[77]=z[76]*z[5];
    z[78]=z[72] - z[5];
    z[79]=n<T>(1,2)*z[3];
    z[80]=z[79] + z[78];
    z[80]=z[3]*z[80];
    z[81]=3*z[72] - z[5];
    z[81]=n<T>(3,4)*z[10] + z[79] + n<T>(1,2)*z[81] - z[6];
    z[81]=z[10]*z[81];
    z[82]=n<T>(1,2)*z[4];
    z[83]= - 7*z[72] + z[5];
    z[83]=n<T>(1,6)*z[83] + z[6];
    z[83]= - n<T>(11,6)*z[8] + z[82] + n<T>(1,4)*z[10] + n<T>(11,12)*z[3] + n<T>(1,2)*
    z[83] - z[7];
    z[83]=z[8]*z[83];
    z[84]=z[72]*z[4];
    z[85]= - 2*z[78] - z[7];
    z[85]=z[7]*z[85];
    z[80]=z[83] - z[84] + z[81] + n<T>(7,6)*z[80] + z[85] - n<T>(5,6)*z[77] + 
    z[74];
    z[80]=z[8]*z[80];
    z[73]=z[5] - z[73];
    z[73]=z[6]*z[73];
    z[81]=n<T>(1,2)*z[7];
    z[83]= - z[72] + z[81];
    z[83]=z[7]*z[83];
    z[85]= - z[72] + z[79];
    z[85]=z[3]*z[85];
    z[86]=3*z[7];
    z[87]=n<T>(5,6)*z[10] - n<T>(5,3)*z[72] - z[86];
    z[87]=z[10]*z[87];
    z[73]=z[87] + z[85] + n<T>(7,3)*z[83] - z[77] + z[73];
    z[83]=z[3] - z[5];
    z[85]=z[83] - z[6];
    z[87]=n<T>(5,12)*z[10];
    z[88]=z[72] - z[4];
    z[85]=z[87] + n<T>(7,12)*z[7] + z[88] + n<T>(1,4)*z[85];
    z[85]=z[4]*z[85];
    z[73]=n<T>(1,2)*z[73] + z[85];
    z[73]=z[4]*z[73];
    z[76]= - z[76]*z[75];
    z[85]=2*z[6];
    z[89]=z[72] - z[6];
    z[90]=z[89]*z[85];
    z[76]=z[76] + z[90];
    z[90]= - 5*z[72] + z[5];
    z[85]=n<T>(1,2)*z[90] + z[85];
    z[85]= - z[87] + n<T>(1,3)*z[85] - z[79];
    z[85]=z[10]*z[85];
    z[87]=z[72] + z[3];
    z[90]=z[82] - z[87];
    z[90]=z[90]*z[82];
    z[91]=z[10] - z[5];
    z[87]=z[87] + z[91];
    z[92]=n<T>(3,2)*z[8] + z[87];
    z[92]=z[8]*z[92];
    z[93]=n<T>(1,6)*z[5];
    z[94]= - z[72] - z[93];
    z[95]=n<T>(1,3)*z[6];
    z[94]= - n<T>(2,3)*z[11] + n<T>(3,4)*z[8] + n<T>(1,4)*z[4] + n<T>(1,12)*z[10]
     + n<T>(1,2)
   *z[94] - z[95];
    z[94]=z[11]*z[94];
    z[96]=z[3]*z[75];
    z[76]=z[94] + n<T>(1,2)*z[92] + z[90] + z[85] + n<T>(1,3)*z[76] + z[96];
    z[76]=z[11]*z[76];
    z[85]=z[8] + z[5];
    z[90]=z[7] + z[6];
    z[85]=z[14] - n<T>(4,3)*z[2] - n<T>(1,6)*z[11] + n<T>(7,4)*z[4] - n<T>(5,2)*z[10]
     - 
   n<T>(7,3)*z[3] + z[90] - n<T>(7,12)*z[85];
    z[85]=z[30]*z[85];
    z[92]=z[72]*z[5];
    z[92]=z[92] + z[30];
    z[94]= - z[10]*z[72];
    z[94]=z[94] - z[92];
    z[94]=z[15]*z[94];
    z[96]=2*z[72];
    z[97]=z[96] - z[2];
    z[98]= - z[5] + z[97];
    z[98]=z[19]*z[98];
    z[99]=z[10] - z[78];
    z[99]=z[22]*z[99];
    z[100]= - z[6] + z[97];
    z[100]=z[24]*z[100];
    z[85]=z[35] + z[63] + z[55] - z[54] - z[46] + z[98] + z[85] + z[94]
    + z[99] + z[100];
    z[75]=z[81] - z[75];
    z[81]=z[79] + z[71] + z[97] - z[75];
    z[81]=z[2]*z[81];
    z[94]= - z[96] + z[5];
    z[94]=z[5]*z[94];
    z[98]=z[96] + z[5];
    z[99]=z[6] - z[98];
    z[99]=z[6]*z[99];
    z[100]=z[78] - z[6];
    z[101]= - z[7] - z[100];
    z[101]=z[7]*z[101];
    z[102]=2*z[3] - z[7] + z[100];
    z[102]=z[3]*z[102];
    z[81]=z[81] + z[102] + z[101] + z[94] + z[99];
    z[94]= - z[72] + z[82];
    z[94]=z[4]*z[94];
    z[99]=npow(z[8],2);
    z[81]= - n<T>(1,2)*z[99] + z[94] + n<T>(1,3)*z[81];
    z[81]=z[2]*z[81];
    z[83]= - n<T>(7,6)*z[10] + z[7] - n<T>(7,3)*z[72] - z[83];
    z[83]=z[10]*z[83];
    z[94]=n<T>(7,6)*z[7] + n<T>(1,3)*z[72] - z[5];
    z[94]=z[7]*z[94];
    z[99]=z[7] - z[6] + z[5];
    z[99]=z[3]*z[99];
    z[83]=z[83] + z[99] + z[94] - z[77] + z[74];
    z[94]=z[7] + 2*z[10];
    z[82]=n<T>(1,3)*z[94] - z[82];
    z[82]=z[4]*z[82];
    z[86]=z[86] + z[6] + z[91];
    z[86]=n<T>(1,4)*z[86] - z[14];
    z[86]=z[14]*z[86];
    z[82]=z[86] + n<T>(1,2)*z[83] + z[82];
    z[82]=z[14]*z[82];
    z[83]= - z[3]*z[72];
    z[83]=z[83] - z[92];
    z[83]=z[9]*z[83];
    z[86]= - z[3] + z[78];
    z[86]=z[20]*z[86];
    z[91]= - z[3] - z[8];
    z[91]=z[38]*z[91];
    z[92]= - z[10] - z[14];
    z[92]=z[40]*z[92];
    z[83]=z[83] + z[86] + z[91] + z[92] + z[37] - z[32];
    z[86]=n<T>(5,4)*z[6] - n<T>(5,2)*z[72] + z[5];
    z[86]=z[86]*z[95];
    z[91]=n<T>(1,3)*z[7];
    z[92]=n<T>(17,4)*z[7] + n<T>(1,2)*z[78] + z[6];
    z[92]=z[92]*z[91];
    z[90]= - n<T>(41,9)*z[3] + z[72] + n<T>(1,3)*z[5] + n<T>(1,6)*z[90];
    z[90]=z[90]*z[79];
    z[94]= - z[72] + n<T>(4,3)*z[5];
    z[94]=z[5]*z[94];
    z[86]=z[90] + z[92] + z[94] + z[86];
    z[86]=z[3]*z[86];
    z[90]= - z[93] + z[89];
    z[90]=z[6]*z[90];
    z[90]= - n<T>(1,3)*z[77] + z[90];
    z[90]=z[6]*z[90];
    z[92]=npow(z[5],2);
    z[78]=z[78]*z[92];
    z[78]=z[42] - z[59] - n<T>(1,3)*z[78] + z[90];
    z[71]= - z[71] - z[98];
    z[71]=z[6]*z[71];
    z[90]= - z[72] - n<T>(5,2)*z[5];
    z[90]= - n<T>(17,3)*z[7] + n<T>(1,2)*z[90] + z[6];
    z[90]=z[7]*z[90];
    z[71]=z[90] - n<T>(11,2)*z[77] + z[71];
    z[71]=z[71]*z[91];
    z[75]=z[79] + z[96] + z[75];
    z[75]=z[7]*z[75];
    z[74]=n<T>(1,2)*z[92] + z[74];
    z[77]= - n<T>(7,2)*z[72] + z[6];
    z[77]= - n<T>(4,3)*z[10] + n<T>(1,6)*z[77] + z[7];
    z[77]=z[10]*z[77];
    z[74]=z[77] + n<T>(1,3)*z[74] + z[75];
    z[74]=z[10]*z[74];
    z[75]=z[72]*z[6];
    z[75]=z[75] + z[30];
    z[77]=z[11]*z[72];
    z[77]=z[77] + z[75];
    z[77]=z[16]*z[77];
    z[79]=z[11] - z[89];
    z[79]=z[25]*z[79];
    z[77]=z[43] - z[77] - z[79] + z[66] + z[58];
    z[79]= - 2*z[11] - 3*z[8] - z[87];
    z[79]=z[23]*z[79];
    z[87]= - n<T>(7,3)*z[14] - n<T>(10,3)*z[7] - z[87];
    z[87]=z[29]*z[87];
    z[84]=z[84] + z[30];
    z[90]=z[14]*z[72];
    z[90]=z[90] + z[84];
    z[90]=z[12]*z[90];
    z[91]=z[8]*z[72];
    z[84]=z[91] + z[84];
    z[84]=z[13]*z[84];
    z[72]=z[7]*z[72];
    z[72]=z[72] + z[75];
    z[72]=z[17]*z[72];
    z[75]=z[68] + z[65] + z[60] - z[53] + z[51] - z[44];
    z[91]=z[57] - z[49];
    z[92]=z[62] - z[47];
    z[93]=z[64] + z[50];
    z[94]=z[69] + z[61];
    z[95]= - n<T>(10,3)*z[36] - n<T>(2,3)*z[34] - n<T>(11,36)*z[31];
    z[95]=i*z[95];
    z[96]=2*z[100] - 11*z[7];
    z[96]=n<T>(2,3)*z[2] + n<T>(1,3)*z[96] - 3*z[3];
    z[96]=z[26]*z[96];
    z[97]= - z[4] + z[97];
    z[97]=z[18]*z[97];
    z[98]= - z[8] + z[88];
    z[98]=z[21]*z[98];
    z[89]= - z[7] + z[89];
    z[89]=z[27]*z[89];
    z[88]= - z[14] + z[88];
    z[88]=z[28]*z[88];
    z[99]= - z[10] - z[11];
    z[99]=z[39]*z[99];

    r += n<T>(4,9)*z[33] - z[41] - n<T>(5,4)*z[45] - n<T>(8,3)*z[48] - z[52] + n<T>(1,6)
      *z[56] - n<T>(7,12)*z[67] + z[70] + z[71] + z[72] + z[73] + z[74] + n<T>(1,4)*z[75]
     + z[76] - n<T>(2,3)*z[77]
     + n<T>(1,2)*z[78]
     + z[79]
     + z[80]
     +  z[81] + z[82] + n<T>(5,3)*z[83] + z[84] + n<T>(1,3)*z[85] + z[86] + z[87]
       + z[88] + z[89] + z[90] + n<T>(1,12)*z[91] + n<T>(3,4)*z[92] + n<T>(5,12)*
      z[93] - 4*z[94] + z[95] + z[96] + z[97] + z[98] + z[99];
 
    return r;
}

template std::complex<double> qqb_2lha_tf140(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf140(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
