#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf607(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[56];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[4];
    z[9]=d[16];
    z[10]=e[0];
    z[11]=e[1];
    z[12]=e[4];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=c[11];
    z[16]=c[15];
    z[17]=c[18];
    z[18]=c[19];
    z[19]=c[23];
    z[20]=c[25];
    z[21]=c[26];
    z[22]=c[28];
    z[23]=c[29];
    z[24]=c[31];
    z[25]=e[3];
    z[26]=e[10];
    z[27]=f[0];
    z[28]=f[1];
    z[29]=f[3];
    z[30]=f[4];
    z[31]=f[6];
    z[32]=f[8];
    z[33]=f[9];
    z[34]=f[11];
    z[35]=f[12];
    z[36]=f[13];
    z[37]=f[14];
    z[38]=f[18];
    z[39]=f[21];
    z[40]=2*z[3];
    z[41]=2*z[5];
    z[42]=z[4] - 13*z[6];
    z[42]= - z[41] - z[40] + n<T>(1,3)*z[42] - z[7];
    z[43]=i*z[1];
    z[42]=z[42]*z[43];
    z[44]=z[4] - z[6];
    z[43]= - n<T>(5,3)*z[2] + 7*z[43] - z[41] + 4*z[3] + n<T>(1,3)*z[44] + z[7];
    z[43]=z[2]*z[43];
    z[44]=npow(z[4],2);
    z[45]=n<T>(1,3)*z[44];
    z[46]=npow(z[6],2);
    z[47]= - z[45] + n<T>(7,3)*z[46];
    z[48]= - z[4] + 7*z[6];
    z[49]=z[5] - z[3] + n<T>(1,3)*z[48] + z[7];
    z[41]=z[49]*z[41];
    z[49]=npow(z[8],2);
    z[50]= - n<T>(4,3)*z[13] - z[11] + n<T>(1,3)*z[10];
    z[51]=2*z[6];
    z[52]= - z[51] - z[7];
    z[52]=z[7]*z[52];
    z[53]= - z[6] - z[7];
    z[53]=2*z[53] + z[3];
    z[53]=z[3]*z[53];
    z[41]=z[43] + 2*z[42] + z[41] + z[53] + z[52] + z[49] + 4*z[50] - 
    z[14] + z[47];
    z[41]=z[2]*z[41];
    z[42]=n<T>(1,3)*z[7];
    z[43]=z[51] - z[42];
    z[43]=z[43]*z[7];
    z[50]=z[43] + n<T>(1,3)*z[49];
    z[51]=n<T>(1,3)*z[8];
    z[42]=z[51] - z[42];
    z[52]=z[42] + z[6];
    z[53]=z[52]*z[40];
    z[48]=z[8] + z[48] - z[7];
    z[48]=z[48]*z[5];
    z[54]=n<T>(1,3)*z[14];
    z[55]= - z[11] - z[13];
    z[47]= - n<T>(1,3)*z[48] + z[53] + 4*z[55] + z[54] - z[47] - z[50];
    z[47]=z[5]*z[47];
    z[53]=2*z[9];
    z[51]= - z[53] + z[51];
    z[51]=z[8]*z[51];
    z[42]=z[6] + z[4] - z[42];
    z[40]=z[42]*z[40];
    z[42]=n<T>(22,3)*z[13] - n<T>(4,3)*z[10] - z[12] + 4*z[11];
    z[55]= - z[9] + z[8];
    z[55]=2*z[55] + 3*z[4];
    z[55]=z[4]*z[55];
    z[40]=n<T>(2,3)*z[48] + z[40] + z[43] + z[55] + 2*z[42] + z[51];
    z[40]=z[1]*z[40];
    z[40]= - n<T>(1,6)*z[15] - 16*z[21] - 3*z[19] + z[40];
    z[40]=i*z[40];
    z[42]= - z[3]*z[52];
    z[43]=z[26] - z[25];
    z[43]=n<T>(1,3)*z[43] + z[13];
    z[42]=z[42] - z[46] - z[44] + 4*z[43] + z[50];
    z[42]=z[3]*z[42];
    z[43]=z[12] + n<T>(2,3)*z[10];
    z[43]= - z[45] - z[49] + 2*z[43] + n<T>(35,9)*z[14];
    z[43]=z[4]*z[43];
    z[44]=z[26] - z[54];
    z[45]=z[7]*z[6];
    z[44]=z[45] + n<T>(4,3)*z[44] + z[46];
    z[44]=z[7]*z[44];
    z[45]= - n<T>(2,3)*z[25] + z[12];
    z[45]=n<T>(2,3)*z[49] + 2*z[45] + n<T>(5,18)*z[14];
    z[45]=z[8]*z[45];
    z[46]= - 16*z[13] + z[54];
    z[46]=z[6]*z[46];
    z[46]=z[46] + z[29] + z[30] + z[35];
    z[48]=z[16] - z[34];
    z[49]=z[27] - z[39] + n<T>(1,3)*z[38];
    z[50]= - z[14]*z[53];

    r += n<T>(4,3)*z[17] + n<T>(3,2)*z[18] + n<T>(2,3)*z[20] + 6*z[22] - z[23] + n<T>(77,6)*z[24]
     - z[28]
     + z[31] - 8*z[32] - z[33] - z[36]
     + z[37]
     +  z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + n<T>(1,3)*z[46] + 
      z[47] + n<T>(7,3)*z[48] + 2*z[49] + z[50];
 
    return r;
}

template std::complex<double> qqb_2lha_tf607(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf607(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
