#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf931(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[76];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[11];
    z[10]=d[9];
    z[11]=d[12];
    z[12]=d[4];
    z[13]=d[17];
    z[14]=e[0];
    z[15]=e[1];
    z[16]=e[5];
    z[17]=e[7];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=e[12];
    z[21]=c[3];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[23];
    z[26]=c[25];
    z[27]=c[26];
    z[28]=c[28];
    z[29]=c[31];
    z[30]=e[6];
    z[31]=e[13];
    z[32]=e[14];
    z[33]=f[3];
    z[34]=f[5];
    z[35]=f[11];
    z[36]=f[13];
    z[37]=f[17];
    z[38]=f[30];
    z[39]=f[31];
    z[40]=f[32];
    z[41]=f[33];
    z[42]=f[35];
    z[43]=f[36];
    z[44]=f[39];
    z[45]=f[43];
    z[46]=f[50];
    z[47]=f[51];
    z[48]=f[52];
    z[49]=f[55];
    z[50]=f[58];
    z[51]=2*z[2];
    z[52]=n<T>(1,2)*z[5];
    z[53]=z[1]*i;
    z[54]=z[51] - n<T>(1,3)*z[7] - z[4] - 4*z[53] - z[52];
    z[54]=z[2]*z[54];
    z[55]=2*z[5];
    z[56]= - z[53] - z[55];
    z[56]=z[5]*z[56];
    z[57]=2*z[53];
    z[58]=z[4] - z[5];
    z[59]=z[57] - z[58];
    z[59]=z[4]*z[59];
    z[60]=n<T>(2,3)*z[4];
    z[61]= - n<T>(5,3)*z[7] + z[60] + n<T>(10,3)*z[53] + z[5];
    z[61]=z[7]*z[61];
    z[54]=z[54] + z[61] + z[56] + z[59];
    z[54]=z[2]*z[54];
    z[56]=n<T>(3,2)*z[5];
    z[59]= - z[53] - z[56];
    z[61]=n<T>(1,3)*z[4];
    z[62]=n<T>(4,3)*z[2];
    z[63]=n<T>(1,2)*z[3];
    z[59]=z[63] - z[62] + z[7] + n<T>(1,2)*z[59] + z[61];
    z[59]=z[3]*z[59];
    z[64]=z[53] - z[52];
    z[64]=z[64]*z[56];
    z[65]=z[57] - z[4];
    z[61]= - z[65]*z[61];
    z[65]= - z[53] - z[4];
    z[65]=2*z[65] + z[7];
    z[65]=z[7]*z[65];
    z[66]=z[57] + z[4];
    z[66]=2*z[66] - z[2];
    z[66]=z[2]*z[66];
    z[59]=z[59] + n<T>(2,3)*z[66] + z[65] + z[64] + z[61];
    z[59]=z[3]*z[59];
    z[61]=n<T>(1,2)*z[7];
    z[64]=n<T>(1,2)*z[4];
    z[65]=n<T>(7,6)*z[6] + z[2] + z[61] + z[64] + z[53] - n<T>(5,2)*z[5];
    z[65]=z[6]*z[65];
    z[66]= - z[5] + z[64] - z[53];
    z[66]=z[66]*z[4];
    z[67]=2*z[7];
    z[68]=z[53] - z[4];
    z[69]=z[67] - z[68];
    z[69]=z[7]*z[69];
    z[70]=z[53] - z[7];
    z[71]=n<T>(1,2)*z[2] - z[58] + z[70];
    z[71]=z[2]*z[71];
    z[72]=npow(z[5],2);
    z[65]=z[65] + z[71] + z[69] - 2*z[72] + z[66];
    z[65]=z[6]*z[65];
    z[58]= - z[53] + z[58];
    z[58]=z[10]*z[58];
    z[69]=npow(z[3],2);
    z[58]=z[69] + z[58];
    z[69]=z[52] + z[53];
    z[71]=z[5]*z[69];
    z[58]=z[71] + z[66] + n<T>(1,2)*z[58];
    z[58]=z[10]*z[58];
    z[66]=3*z[5];
    z[69]=z[69]*z[66];
    z[71]=z[66] + z[53];
    z[63]= - z[63] + z[71];
    z[63]=z[3]*z[63];
    z[73]=z[53] - z[3];
    z[74]= - z[66] + z[73];
    z[75]=z[8] + z[10];
    z[74]=n<T>(1,2)*z[74] + z[75];
    z[74]=z[8]*z[74];
    z[63]=z[74] - z[69] + z[63];
    z[74]=z[10]*z[73];
    z[63]=z[74] + n<T>(1,2)*z[63];
    z[63]=z[8]*z[63];
    z[61]= - z[61] + z[71];
    z[61]=z[7]*z[61];
    z[66]= - z[66] + z[70];
    z[71]=z[12] + z[6];
    z[66]=n<T>(1,2)*z[66] + z[71];
    z[66]=z[12]*z[66];
    z[61]=z[66] - z[69] + z[61];
    z[66]=z[6]*z[70];
    z[61]=z[66] + n<T>(1,2)*z[61];
    z[61]=z[12]*z[61];
    z[66]=z[5] + z[4];
    z[66]=z[53]*z[66];
    z[66]=z[21] + z[66];
    z[66]=z[11]*z[66];
    z[69]= - z[5] + z[68];
    z[69]=z[16]*z[69];
    z[74]=z[5] + z[10];
    z[74]=z[31]*z[74];
    z[66]= - z[66] - z[69] - z[74] - z[29] + z[26] - z[23];
    z[56]=7*z[53] - z[56];
    z[52]=z[56]*z[52];
    z[56]=z[60] - n<T>(4,3)*z[53] - z[5];
    z[56]=z[4]*z[56];
    z[60]= - n<T>(7,4)*z[5] + z[60];
    z[60]=z[7]*z[60];
    z[52]=z[60] + z[52] + z[56];
    z[52]=z[7]*z[52];
    z[51]=5*z[6] - z[51] + z[67] + 2*z[4] - z[57] + 7*z[5];
    z[51]=z[17]*z[51];
    z[56]= - z[7] - z[6];
    z[56]=z[53]*z[56];
    z[56]= - z[21] + z[56];
    z[56]=z[13]*z[56];
    z[60]= - z[6] + z[70];
    z[60]=z[19]*z[60];
    z[56]=z[56] + z[60];
    z[60]=3*z[53] + n<T>(23,3)*z[5];
    z[60]=z[60]*z[72];
    z[60]=z[60] - z[48];
    z[67]= - z[53] + z[5];
    z[55]=z[67]*z[55];
    z[67]=z[4]*z[68];
    z[55]=z[55] + z[67];
    z[55]=z[4]*z[55];
    z[67]= - z[12] - z[8] + z[6] + z[2];
    z[64]= - n<T>(5,6)*z[10] - n<T>(7,36)*z[3] - n<T>(23,36)*z[7] + n<T>(10,3)*z[5]
     + 
    z[64] - n<T>(1,3)*z[67];
    z[64]=z[21]*z[64];
    z[62]=n<T>(4,3)*z[3] + z[62] + z[7] - n<T>(8,3)*z[53] - z[4];
    z[62]=z[14]*z[62];
    z[67]= - z[3] - z[10];
    z[53]=z[53]*z[67];
    z[53]= - z[21] + z[53];
    z[53]=z[9]*z[53];
    z[67]=z[44] + z[50] + z[47];
    z[68]=z[24] + z[36] + z[33];
    z[69]=z[49] + z[43];
    z[57]=z[57] - z[2];
    z[70]=z[7] - z[57];
    z[70]=z[18]*z[70];
    z[70]=z[70] + z[34];
    z[72]=8*z[27] + 4*z[25] - n<T>(1,3)*z[22];
    z[72]=i*z[72];
    z[57]=z[4] - z[57];
    z[57]=z[15]*z[57];
    z[73]=z[10] - z[73];
    z[73]=z[20]*z[73];
    z[71]=z[30]*z[71];
    z[74]=z[32]*z[75];

    r +=  - 4*z[28] + n<T>(2,3)*z[35] + z[37] - z[38] - n<T>(11,4)*z[39] - n<T>(3,2)
      *z[40] - 8*z[41] - z[42] + z[45] - z[46] + z[51] + z[52] + z[53]
       + z[54] + z[55] + 3*z[56] + z[57] + z[58] + z[59] + n<T>(1,2)*z[60]
       + z[61] + z[62] + z[63] + z[64] + z[65] - 2*z[66] - n<T>(3,4)*z[67]
       + n<T>(1,3)*z[68] + n<T>(1,4)*z[69] + n<T>(5,3)*z[70] + z[71] + z[72] + 
      z[73] + z[74];
 
    return r;
}

template std::complex<double> qqb_2lha_tf931(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf931(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
