#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf891(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[112];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=c[3];
    z[30]=c[11];
    z[31]=c[15];
    z[32]=c[18];
    z[33]=c[19];
    z[34]=c[21];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[29];
    z[40]=c[31];
    z[41]=e[3];
    z[42]=e[6];
    z[43]=e[13];
    z[44]=e[14];
    z[45]=f[0];
    z[46]=f[1];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[9];
    z[53]=f[10];
    z[54]=f[11];
    z[55]=f[12];
    z[56]=f[13];
    z[57]=f[14];
    z[58]=f[16];
    z[59]=f[17];
    z[60]=f[18];
    z[61]=f[21];
    z[62]=f[23];
    z[63]=f[30];
    z[64]=f[31];
    z[65]=f[32];
    z[66]=f[33];
    z[67]=f[34];
    z[68]=f[35];
    z[69]=f[36];
    z[70]=f[37];
    z[71]=f[39];
    z[72]=f[41];
    z[73]=f[43];
    z[74]=f[50];
    z[75]=f[51];
    z[76]=f[52];
    z[77]=f[54];
    z[78]=f[55];
    z[79]=f[56];
    z[80]=f[58];
    z[81]=f[60];
    z[82]=2*z[2];
    z[83]=z[1]*i;
    z[84]=13*z[83];
    z[85]=n<T>(17,2)*z[5];
    z[86]= - z[82] - z[84] + z[85];
    z[86]=z[2]*z[86];
    z[87]=n<T>(1,2)*z[4];
    z[88]=z[87] - z[83];
    z[89]=z[88]*z[4];
    z[90]=n<T>(1,2)*z[5];
    z[91]=z[90] - z[83];
    z[92]= - z[91]*z[85];
    z[93]= - 14*z[9] + n<T>(17,2)*z[2] + z[83] - z[85];
    z[93]=z[9]*z[93];
    z[94]=z[2] - z[5];
    z[95]= - 17*z[94];
    z[96]=9*z[83];
    z[97]=n<T>(77,2)*z[7] - 9*z[9] + z[96] + z[95];
    z[98]=n<T>(1,2)*z[7];
    z[97]=z[97]*z[98];
    z[99]=n<T>(1,2)*z[6];
    z[100]=z[83] - z[9];
    z[101]= - z[99] - z[100];
    z[101]=z[6]*z[101];
    z[85]= - n<T>(73,3)*z[8] - n<T>(9,2)*z[6] + n<T>(85,2)*z[7] - z[9] - n<T>(43,2)*z[2]
    - 23*z[83] - z[85];
    z[85]=z[8]*z[85];
    z[85]=n<T>(1,2)*z[85] + n<T>(11,2)*z[101] + z[97] - n<T>(9,2)*z[89] + z[93] + 
    z[92] + z[86];
    z[85]=z[8]*z[85];
    z[86]=n<T>(1,2)*z[9];
    z[92]=z[86] - z[83];
    z[93]=z[92]*z[9];
    z[91]=z[91]*z[5];
    z[97]=n<T>(3,2)*z[2];
    z[101]=n<T>(7,6)*z[4] + z[97] - n<T>(1,2)*z[83] - z[5];
    z[101]=z[4]*z[101];
    z[102]= - z[5] + n<T>(5,2)*z[83];
    z[97]= - n<T>(3,2)*z[10] - z[7] + z[97] - z[102];
    z[97]=z[11]*z[97];
    z[102]= - z[2]*z[102];
    z[103]=n<T>(17,12)*z[7] - n<T>(5,6)*z[4] + n<T>(17,6)*z[83] - z[2] - z[5];
    z[103]=z[7]*z[103];
    z[104]=z[83] - z[4];
    z[105]=2*z[104] + z[10];
    z[105]=z[10]*z[105];
    z[97]=n<T>(1,2)*z[97] + z[105] + z[103] + z[101] - n<T>(3,2)*z[93] + z[91]
    + z[102];
    z[97]=z[11]*z[97];
    z[101]=z[83] - z[5];
    z[102]=z[2]*z[101];
    z[93]=z[93] - n<T>(11,6)*z[91] + z[102];
    z[102]=5*z[83];
    z[103]=z[102] + z[90];
    z[103]=z[4]*z[103];
    z[105]=3*z[7];
    z[106]=2*z[100] + z[7];
    z[106]=z[106]*z[105];
    z[107]=n<T>(1,2)*z[10];
    z[108]= - z[107] - z[101];
    z[109]=n<T>(11,4)*z[10];
    z[108]=z[108]*z[109];
    z[110]=z[4] - z[2];
    z[111]=29*z[83] - 35*z[5];
    z[111]=z[9] + n<T>(1,6)*z[111] - 11*z[110];
    z[105]=z[6] - n<T>(11,8)*z[10] + n<T>(1,4)*z[111] + z[105];
    z[105]=z[6]*z[105];
    z[93]=z[105] + z[108] + z[106] + n<T>(1,2)*z[93] + z[103];
    z[93]=z[6]*z[93];
    z[103]=n<T>(1,2)*z[2];
    z[86]=z[109] - z[86] + 5*z[101] + z[103];
    z[86]=z[10]*z[86];
    z[105]=n<T>(5,4)*z[2] - n<T>(7,2)*z[83] + 2*z[5];
    z[105]=z[2]*z[105];
    z[86]=z[105] + z[86];
    z[105]=z[103] - z[90];
    z[88]= - z[88] - z[105];
    z[88]=z[4]*z[88];
    z[106]=n<T>(1,6)*z[9];
    z[87]= - n<T>(115,18)*z[3] + n<T>(11,6)*z[6] + n<T>(5,3)*z[10] + z[87] + z[106]
    - n<T>(7,6)*z[2] - n<T>(7,3)*z[83] + n<T>(5,2)*z[5];
    z[108]=n<T>(1,2)*z[3];
    z[87]=z[87]*z[108];
    z[109]=z[94] - z[92];
    z[106]=z[109]*z[106];
    z[109]=n<T>(17,3)*z[6] + n<T>(11,3)*z[101] - z[110];
    z[99]=z[109]*z[99];
    z[109]= - n<T>(19,6)*z[83] + z[5];
    z[109]=z[5]*z[109];
    z[86]=z[87] + z[99] + z[88] + z[106] + z[109] + n<T>(1,3)*z[86];
    z[86]=z[3]*z[86];
    z[87]=z[103] + z[101];
    z[87]=z[2]*z[87];
    z[87]= - n<T>(13,2)*z[91] + z[87];
    z[88]=n<T>(1,4)*z[9];
    z[91]=z[88] + 4*z[83] - z[105];
    z[91]=z[9]*z[91];
    z[87]=n<T>(1,2)*z[87] + z[91];
    z[91]= - z[98] - z[104];
    z[91]=z[7]*z[91];
    z[98]=z[102] + n<T>(11,2)*z[5];
    z[82]= - 2*z[9] + n<T>(1,4)*z[98] + z[82];
    z[82]=n<T>(91,36)*z[10] - n<T>(15,8)*z[7] + n<T>(1,3)*z[82] - n<T>(7,8)*z[4];
    z[82]=z[10]*z[82];
    z[82]=z[82] + n<T>(15,4)*z[91] + n<T>(1,3)*z[87] - n<T>(7,4)*z[89];
    z[82]=z[10]*z[82];
    z[87]=3*z[83];
    z[89]= - z[87] - z[90];
    z[89]=z[5]*z[89];
    z[87]= - z[103] + z[87] + z[5];
    z[87]=z[2]*z[87];
    z[91]=55*z[83];
    z[95]=n<T>(55,2)*z[9] - z[91] + z[95];
    z[95]=z[9]*z[95];
    z[87]=z[95] + z[89] + 5*z[87];
    z[89]= - n<T>(35,24)*z[4] + n<T>(35,12)*z[83] + z[94];
    z[89]=z[4]*z[89];
    z[95]= - n<T>(203,3)*z[7] - n<T>(35,6)*z[4] + 21*z[9] - n<T>(91,6)*z[83] + 51*
    z[94];
    z[95]=z[7]*z[95];
    z[87]=n<T>(1,4)*z[95] + n<T>(1,2)*z[87] + z[89];
    z[87]=z[7]*z[87];
    z[89]=z[91] - n<T>(67,2)*z[5];
    z[89]=z[89]*z[90];
    z[90]=n<T>(37,2)*z[2] - 98*z[83] - 37*z[5];
    z[90]=z[2]*z[90];
    z[89]=z[89] + z[90];
    z[90]=z[96] - n<T>(55,3)*z[5];
    z[88]= - z[88] + n<T>(1,4)*z[90] + n<T>(49,3)*z[2];
    z[88]=z[9]*z[88];
    z[88]=n<T>(1,3)*z[89] + z[88];
    z[88]=z[9]*z[88];
    z[89]=n<T>(79,2)*z[2] - 185*z[83] - 91*z[5];
    z[89]=z[89]*z[103];
    z[90]=25*z[83] - 11*z[5];
    z[90]=z[5]*z[90];
    z[89]=z[90] + z[89];
    z[90]=z[5] - z[92];
    z[90]=z[9]*z[90];
    z[91]=n<T>(197,4)*z[2] + n<T>(97,4)*z[83] - 14*z[5];
    z[91]= - n<T>(1,12)*z[4] + n<T>(1,3)*z[91] - n<T>(47,4)*z[9];
    z[91]=z[4]*z[91];
    z[89]=z[91] + n<T>(1,3)*z[89] + n<T>(47,2)*z[90];
    z[89]=z[4]*z[89];
    z[90]= - z[9] - z[10];
    z[90]=z[83]*z[90];
    z[90]= - z[29] + z[90];
    z[90]=z[17]*z[90];
    z[91]=z[10] - z[100];
    z[91]=z[27]*z[91];
    z[90]= - z[53] + z[81] - z[90] - z[91] + z[61] - z[60];
    z[91]= - z[6] - z[3];
    z[91]=z[41]*z[91];
    z[92]=z[7] + z[11];
    z[92]=z[43]*z[92];
    z[91]=z[91] + z[92];
    z[92]= - z[4] - z[6];
    z[92]=z[83]*z[92];
    z[92]= - z[29] + z[92];
    z[92]=z[14]*z[92];
    z[95]=z[6] - z[104];
    z[95]=z[21]*z[95];
    z[92]=z[63] + z[92] + z[95];
    z[95]=z[9] + z[8];
    z[95]=z[83]*z[95];
    z[95]=z[29] + z[95];
    z[95]=z[16]*z[95];
    z[96]=z[8] - z[100];
    z[96]=z[25]*z[96];
    z[95]=z[95] + z[96];
    z[96]= - z[5] - z[7];
    z[96]=z[83]*z[96];
    z[96]= - z[29] + z[96];
    z[96]=z[15]*z[96];
    z[98]=z[7] - z[101];
    z[98]=z[22]*z[98];
    z[96]=z[96] + z[98];
    z[98]= - z[4] - z[11];
    z[98]=z[83]*z[98];
    z[98]= - z[29] + z[98];
    z[98]=z[13]*z[98];
    z[99]=z[11] - z[104];
    z[99]=z[28]*z[99];
    z[98]=z[98] + z[99];
    z[99]=z[5] + z[3];
    z[99]=z[83]*z[99];
    z[99]=z[29] + z[99];
    z[99]=z[12]*z[99];
    z[102]=z[3] - z[101];
    z[102]=z[20]*z[102];
    z[99]=z[99] + z[102];
    z[102]= - 61*z[2] + 101*z[83] + 31*z[5];
    z[102]=z[102]*z[103];
    z[84]= - z[84] + n<T>(37,2)*z[5];
    z[84]=z[5]*z[84];
    z[84]=z[84] + z[102];
    z[84]=z[84]*z[103];
    z[94]=z[100] + z[94];
    z[100]= - 38*z[8] + 17*z[94] - 55*z[7];
    z[100]=z[23]*z[100];
    z[94]= - z[107] - z[94];
    z[94]=n<T>(1,3)*z[94] - z[108];
    z[94]=z[26]*z[94];
    z[102]=z[45] + z[76] + z[52];
    z[103]=z[73] - z[68];
    z[104]=z[75] - z[55];
    z[105]= - z[2] + 2*z[83];
    z[106]= - z[5] + z[105];
    z[106]=z[19]*z[106];
    z[106]=z[106] - z[74];
    z[107]= - 72*z[37] - n<T>(87,4)*z[35] - 10*z[34] + n<T>(31,24)*z[30];
    z[107]=i*z[107];
    z[101]=z[101]*npow(z[5],2);
    z[108]=n<T>(1,6)*z[5] - 73*z[2];
    z[108]= - n<T>(14,3)*z[11] + 4*z[8] - n<T>(7,3)*z[3] + n<T>(23,24)*z[6] + n<T>(49,12)
   *z[10] - n<T>(188,3)*z[7] + n<T>(967,24)*z[4] + n<T>(1,4)*z[108] + n<T>(61,3)*z[9];
    z[108]=z[29]*z[108];
    z[83]=209*z[83] - 97*z[2];
    z[83]=n<T>(1,2)*z[83] - 56*z[4];
    z[83]=n<T>(1,3)*z[83] - n<T>(5,2)*z[11];
    z[83]=z[18]*z[83];
    z[105]= - z[9] + z[105];
    z[105]=z[24]*z[105];
    z[109]= - z[6] - z[8];
    z[109]=z[42]*z[109];
    z[110]=z[10] + z[11];
    z[110]=z[44]*z[110];

    r +=  - n<T>(77,8)*z[31] + n<T>(5,6)*z[32] + n<T>(475,48)*z[33] + n<T>(127,8)*z[36]
       + n<T>(159,4)*z[38] - n<T>(5,8)*z[39] - n<T>(643,48)*z[40] - n<T>(11,4)*z[46]
     - 
      n<T>(14,3)*z[47] + n<T>(13,8)*z[48] - n<T>(155,24)*z[49] + n<T>(5,4)*z[50]
     + n<T>(4,3)
      *z[51] - n<T>(55,12)*z[54] - n<T>(47,3)*z[56] + z[57] - n<T>(11,8)*z[58] - n<T>(47,4)*z[59]
     - n<T>(7,6)*z[62]
     + n<T>(107,4)*z[64]
     + n<T>(69,4)*z[65]
     + 68*z[66]
       - n<T>(35,4)*z[67] - n<T>(1,4)*z[69] + n<T>(9,4)*z[70] + 3*z[71] - n<T>(9,2)*
      z[72] + n<T>(11,6)*z[77] + n<T>(7,8)*z[78] + n<T>(3,4)*z[79] - n<T>(15,8)*z[80]
       + z[82] + z[83] + z[84] + z[85] + z[86] + z[87] + z[88] + z[89]
       - n<T>(3,2)*z[90] + n<T>(23,6)*z[91] + n<T>(11,2)*z[92] + z[93] + z[94] + 27
      *z[95] + 8*z[96] + z[97] + n<T>(7,3)*z[98] + n<T>(1,6)*z[99] + z[100] - 4
      *z[101] - n<T>(1,2)*z[102] - 13*z[103] - n<T>(35,24)*z[104] + n<T>(61,3)*
      z[105] + 2*z[106] + z[107] + n<T>(1,3)*z[108] + z[109] + n<T>(7,2)*z[110]
      ;
 
    return r;
}

template std::complex<double> qqb_2lha_tf891(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf891(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
