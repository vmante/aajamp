#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1264(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[42];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[31];
    z[17]=e[6];
    z[18]=f[30];
    z[19]=f[31];
    z[20]=f[32];
    z[21]=f[33];
    z[22]=f[35];
    z[23]=f[36];
    z[24]=f[39];
    z[25]=f[43];
    z[26]=z[1]*i;
    z[27]=2*z[26];
    z[28]=z[4] + z[3];
    z[28]=z[28]*z[27];
    z[29]=z[26] - z[5];
    z[30]=z[3] - z[4];
    z[31]=n<T>(1,2)*z[30] - z[29];
    z[31]=z[5]*z[31];
    z[32]= - z[4] + 2*z[3];
    z[32]=z[32]*z[3];
    z[33]=npow(z[4],2);
    z[34]=z[8]*z[30];
    z[28]=z[31] + z[34] + z[28] - 2*z[33] - z[32];
    z[28]=z[5]*z[28];
    z[31]=5*z[3];
    z[34]=2*z[4];
    z[35]=z[34] - z[31];
    z[35]=z[35]*z[26];
    z[36]=npow(z[3],2);
    z[36]=n<T>(3,2)*z[36];
    z[37]= - 3*z[4] + n<T>(5,2)*z[3];
    z[37]=z[8]*z[37];
    z[35]=z[37] + z[35] - z[33] + z[36];
    z[35]=z[8]*z[35];
    z[37]=3*z[3];
    z[38]=z[34] - z[37];
    z[39]=n<T>(1,2)*z[8];
    z[40]=z[39] - z[26] + z[38];
    z[40]=z[8]*z[40];
    z[41]=z[7] + z[4];
    z[39]=z[39] - n<T>(1,2)*z[26] + n<T>(3,2)*z[3] - z[41];
    z[39]=z[7]*z[39];
    z[38]= - z[38]*z[26];
    z[38]=z[39] + z[40] + z[36] + z[38];
    z[38]=z[7]*z[38];
    z[39]=z[8] + z[4];
    z[40]=z[26]*z[39];
    z[40]=z[13] + z[40];
    z[40]=z[9]*z[40];
    z[39]= - z[26] + z[39];
    z[39]=z[12]*z[39];
    z[39]=z[40] + z[39];
    z[29]=z[29] - z[8];
    z[40]=n<T>(1,2)*z[2] + z[29];
    z[30]=z[30]*z[40];
    z[30]= - z[33] + z[32] + z[30];
    z[30]=z[2]*z[30];
    z[29]=z[2] - 4*z[4] - z[31] + z[29];
    z[29]=z[11]*z[29];
    z[31]=z[17]*z[41];
    z[29]=z[18] + z[29] - z[31];
    z[31]=z[34] - n<T>(13,3)*z[3];
    z[31]=z[3]*z[31];
    z[31]=4*z[33] + z[31];
    z[31]=z[3]*z[31];
    z[32]= - z[26]*z[36];
    z[33]=2*z[5] + z[37] + z[4];
    z[26]= - z[26]*z[33];
    z[26]= - 2*z[13] + z[26];
    z[26]=z[6]*z[26];
    z[27]= - z[27] + z[33];
    z[27]=z[10]*z[27];
    z[33]=z[24] - z[16];
    z[34]=npow(z[4],3);
    z[36]= - z[5] + n<T>(5,3)*z[4] - 7*z[3];
    z[36]= - n<T>(2,3)*z[7] + n<T>(1,3)*z[8] + n<T>(1,2)*z[36];
    z[36]=z[13]*z[36];
    z[37]=z[14]*i;

    r +=  - z[15] + n<T>(7,2)*z[19] + 3*z[20] + 8*z[21] + z[22] - n<T>(1,2)*
      z[23] - z[25] + z[26] + z[27] + z[28] + 2*z[29] + z[30] + z[31]
       + z[32] + n<T>(3,2)*z[33] - n<T>(2,3)*z[34] + z[35] + z[36] + n<T>(1,3)*
      z[37] + z[38] + 4*z[39];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1264(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1264(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
