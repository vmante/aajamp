#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1335(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[42];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[12];
    z[7]=d[4];
    z[8]=d[7];
    z[9]=d[17];
    z[10]=e[5];
    z[11]=e[7];
    z[12]=e[9];
    z[13]=c[3];
    z[14]=c[11];
    z[15]=c[15];
    z[16]=c[17];
    z[17]=c[31];
    z[18]=e[6];
    z[19]=f[2];
    z[20]=f[30];
    z[21]=f[31];
    z[22]=f[32];
    z[23]=f[33];
    z[24]=f[35];
    z[25]=f[36];
    z[26]=f[39];
    z[27]=f[43];
    z[28]=f[68];
    z[29]=npow(z[3],2);
    z[30]=n<T>(3,2)*z[29];
    z[31]=npow(z[4],2);
    z[32]=z[31] - z[30];
    z[33]=z[1]*i;
    z[34]= - z[4] + n<T>(17,6)*z[3];
    z[34]=z[34]*z[33];
    z[35]=5*z[4];
    z[36]=z[35] - n<T>(17,4)*z[3];
    z[36]=z[8]*z[36];
    z[32]=n<T>(1,3)*z[36] + n<T>(1,2)*z[32] + z[34];
    z[32]=z[8]*z[32];
    z[34]= - z[35] + n<T>(43,3)*z[3];
    z[34]=z[3]*z[34];
    z[34]= - 19*z[31] + z[34];
    z[34]=z[3]*z[34];
    z[35]=npow(z[4],3);
    z[34]=n<T>(1,3)*z[35] + n<T>(1,2)*z[34];
    z[30]= - n<T>(7,3)*z[31] + z[30];
    z[35]=n<T>(1,2)*z[33];
    z[30]=z[30]*z[35];
    z[36]=z[7] + z[4];
    z[36]=z[18]*z[36];
    z[30]= - z[20] + z[36] + z[32] + n<T>(1,3)*z[34] + z[30];
    z[32]=2*z[33];
    z[34]= - 2*z[4] - z[3];
    z[34]=z[34]*z[32];
    z[36]= - z[4] + 2*z[3];
    z[36]=z[36]*z[3];
    z[37]=z[3] - z[4];
    z[38]= - z[8]*z[37];
    z[34]=z[38] + z[34] + n<T>(7,2)*z[31] + z[36];
    z[38]=z[33] - z[5];
    z[39]= - n<T>(1,3)*z[37] + z[38];
    z[39]=z[5]*z[39];
    z[34]=n<T>(1,3)*z[34] + n<T>(1,2)*z[39];
    z[34]=z[5]*z[34];
    z[39]= - z[4] + n<T>(3,2)*z[3];
    z[40]= - z[39]*z[33];
    z[41]= - n<T>(1,4)*z[8] + z[35] + z[39];
    z[41]=z[8]*z[41];
    z[35]=z[7] - n<T>(1,2)*z[8] + z[35] - z[39];
    z[39]=n<T>(1,2)*z[7];
    z[35]=z[35]*z[39];
    z[29]=z[35] + z[41] - n<T>(3,4)*z[29] + z[40];
    z[29]=z[29]*z[39];
    z[35]= - n<T>(1,2)*z[2] + z[8] - z[38];
    z[35]=z[37]*z[35];
    z[31]=z[31] - z[36] + z[35];
    z[31]=z[2]*z[31];
    z[35]= - z[8] + z[2] - z[5];
    z[32]= - z[32] + n<T>(13,2)*z[4] + 7*z[3] - 2*z[35];
    z[32]=z[11]*z[32];
    z[31]= - z[24] + z[31] + z[32] + z[28] + z[27];
    z[32]=z[8] + z[4];
    z[35]= - z[33]*z[32];
    z[35]= - z[13] + z[35];
    z[35]=z[9]*z[35];
    z[32]=z[33] - z[32];
    z[32]=z[12]*z[32];
    z[32]=z[35] + z[32];
    z[35]=z[3] + z[4];
    z[36]=z[35] + z[5];
    z[33]=z[33]*z[36];
    z[33]=z[13] + z[33];
    z[33]=z[6]*z[33];
    z[35]=z[38] - z[35];
    z[35]=z[10]*z[35];
    z[36]=n<T>(1,6)*z[7] + n<T>(1,3)*z[5] - n<T>(7,72)*z[8] - n<T>(4,9)*z[4] + z[3];
    z[36]=z[13]*z[36];
    z[37]=z[14]*i;
    z[38]=n<T>(1,2)*i;
    z[38]=z[16]*z[38];

    r +=  - n<T>(1,12)*z[15] + n<T>(5,8)*z[17] - z[19] - n<T>(25,24)*z[21] - n<T>(3,2)*
      z[22] - n<T>(5,3)*z[23] + n<T>(1,8)*z[25] - n<T>(3,8)*z[26] + z[29] + n<T>(1,2)*
      z[30] + n<T>(1,3)*z[31] + n<T>(7,6)*z[32] + z[33] + z[34] + z[35] + z[36]
       - n<T>(7,72)*z[37] + z[38];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1335(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1335(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
