#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1467(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[65];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[1];
    z[6]=d[4];
    z[7]=d[8];
    z[8]=d[2];
    z[9]=d[11];
    z[10]=d[3];
    z[11]=d[9];
    z[12]=d[12];
    z[13]=d[7];
    z[14]=d[17];
    z[15]=e[5];
    z[16]=e[7];
    z[17]=e[9];
    z[18]=e[12];
    z[19]=c[3];
    z[20]=c[11];
    z[21]=c[15];
    z[22]=c[31];
    z[23]=e[3];
    z[24]=e[10];
    z[25]=e[6];
    z[26]=e[13];
    z[27]=e[14];
    z[28]=f[4];
    z[29]=f[10];
    z[30]=f[12];
    z[31]=f[30];
    z[32]=f[31];
    z[33]=f[32];
    z[34]=f[33];
    z[35]=f[35];
    z[36]=f[36];
    z[37]=f[39];
    z[38]=f[43];
    z[39]=f[50];
    z[40]=f[51];
    z[41]=f[52];
    z[42]=f[55];
    z[43]=f[58];
    z[44]=z[1]*i;
    z[45]=z[44] - z[10];
    z[46]= - z[6] - z[45];
    z[46]=z[6]*z[46];
    z[47]=z[7] + z[45];
    z[47]=z[7]*z[47];
    z[48]= - z[6] + z[7];
    z[48]=z[5]*z[48];
    z[46]=n<T>(1,2)*z[48] + z[46] + z[47];
    z[46]=z[5]*z[46];
    z[47]=z[8] + z[11];
    z[48]= - z[44]*z[47];
    z[48]= - z[19] + z[48];
    z[48]=z[9]*z[48];
    z[47]= - z[44] + z[47];
    z[47]=z[18]*z[47];
    z[49]=z[6] + z[5];
    z[49]=z[23]*z[49];
    z[50]= - z[7] - z[5];
    z[50]=z[24]*z[50];
    z[51]=z[7] + z[11];
    z[51]=z[27]*z[51];
    z[46]=z[46] + z[48] - z[39] - z[30] + z[29] - z[28] + z[47] + z[49]
    + z[50] + z[51];
    z[47]=n<T>(1,2)*z[10];
    z[48]=z[47] - z[44];
    z[49]= - z[4] + z[48];
    z[49]=z[10]*z[49];
    z[50]=n<T>(1,3)*z[7];
    z[51]=z[44] + n<T>(1,2)*z[7];
    z[50]=z[51]*z[50];
    z[51]=npow(z[3],2);
    z[52]=n<T>(1,2)*z[51];
    z[53]=z[44] + z[3];
    z[54]=z[4]*z[53];
    z[55]= - z[3] - z[45];
    z[55]=z[11]*z[55];
    z[49]=n<T>(1,2)*z[55] + z[50] + z[49] - z[52] + z[54];
    z[49]=z[11]*z[49];
    z[50]=n<T>(5,2)*z[51];
    z[54]=z[44]*z[3];
    z[55]= - z[50] + 9*z[54];
    z[56]=n<T>(5,3)*z[4];
    z[57]= - z[44] + n<T>(1,2)*z[4];
    z[57]=z[57]*z[56];
    z[58]=z[4] - z[3];
    z[59]=z[10]*z[58];
    z[60]=n<T>(1,3)*z[44];
    z[61]=z[60] + z[3];
    z[62]= - n<T>(1,12)*z[6] + n<T>(1,2)*z[61] - n<T>(1,3)*z[4];
    z[62]=z[6]*z[62];
    z[63]= - n<T>(5,12)*z[6] - n<T>(9,4)*z[3] + n<T>(8,3)*z[4];
    z[63]=z[13]*z[63];
    z[55]=z[63] + 5*z[62] + z[59] + n<T>(1,2)*z[55] + z[57];
    z[55]=z[13]*z[55];
    z[57]=3*z[10];
    z[59]=3*z[44];
    z[62]= - z[57] + 3*z[4] - z[3] + z[59];
    z[47]=z[62]*z[47];
    z[59]=n<T>(3,2)*z[4] - 2*z[3] - z[59];
    z[59]=z[4]*z[59];
    z[47]=z[47] + z[59] + z[50] - 2*z[54];
    z[47]=z[10]*z[47];
    z[50]=n<T>(1,3)*z[10];
    z[48]=z[48]*z[50];
    z[59]=z[52] + z[54];
    z[59]=n<T>(1,2)*z[59];
    z[62]=z[4]*z[60];
    z[62]= - z[59] + z[62];
    z[63]= - 5*z[3] + z[60];
    z[63]=n<T>(1,2)*z[63] + z[56];
    z[63]=n<T>(5,6)*z[6] + n<T>(1,2)*z[63] + z[50];
    z[63]=z[6]*z[63];
    z[62]=z[63] + 5*z[62] - z[48];
    z[62]=z[6]*z[62];
    z[63]=n<T>(1,6)*z[7];
    z[64]= - z[3] + n<T>(5,3)*z[44];
    z[50]=z[63] + n<T>(1,4)*z[64] - z[50];
    z[50]=z[7]*z[50];
    z[48]=z[50] - z[59] + z[48];
    z[48]=z[7]*z[48];
    z[50]=n<T>(1,3)*z[11];
    z[59]=n<T>(1,3)*z[8] + z[50] - z[63] - n<T>(1,2)*z[3] - z[60];
    z[59]=z[8]*z[59];
    z[61]= - z[63] + z[61];
    z[61]=z[7]*z[61];
    z[52]=z[59] + z[61] - z[52] + z[54];
    z[50]= - z[7]*z[50];
    z[50]=z[50] + n<T>(1,2)*z[52];
    z[50]=z[8]*z[50];
    z[52]=n<T>(1,2)*z[2] - z[13] - z[10];
    z[52]=z[58]*z[52];
    z[58]=2*z[51];
    z[53]=z[4] + z[53];
    z[53]=z[4]*z[53];
    z[52]=z[53] - z[58] - z[54] + z[52];
    z[52]=z[2]*z[52];
    z[53]=z[13] + z[4];
    z[59]= - z[44]*z[53];
    z[59]= - z[19] + z[59];
    z[59]=z[14]*z[59];
    z[53]=z[44] - z[53];
    z[53]=z[17]*z[53];
    z[53]=z[59] + z[53];
    z[59]=n<T>(7,2)*z[3];
    z[60]=n<T>(5,6)*z[4] - z[59] + z[60];
    z[60]=z[4]*z[60];
    z[58]=z[60] - z[58] + z[54];
    z[58]=z[4]*z[58];
    z[60]=z[2] - z[13];
    z[61]=3*z[3] - z[44];
    z[57]= - z[11] + z[57] + 3*z[61] + 7*z[4] - 2*z[60];
    z[57]=z[16]*z[57];
    z[60]=z[10]*z[44];
    z[54]=z[19] + z[54] + z[60];
    z[54]=z[12]*z[54];
    z[45]= - z[3] + z[45];
    z[45]=z[15]*z[45];
    z[45]=z[54] + z[45];
    z[44]=z[51]*z[44];
    z[51]= - n<T>(1,12)*z[8] - n<T>(11,36)*z[13] - n<T>(7,18)*z[11] + n<T>(1,18)*z[7]
     + 
   n<T>(11,18)*z[6] + n<T>(2,3)*z[10] + z[59] - z[56];
    z[51]=z[19]*z[51];
    z[54]=z[43] + z[40];
    z[56]=z[4] + z[6];
    z[56]=z[25]*z[56];
    z[56]=z[56] - z[31];
    z[59]=npow(z[3],3);
    z[60]=z[3] + z[11];
    z[60]=z[26]*z[60];
    z[61]=z[20]*i;

    r += z[21] + n<T>(3,2)*z[22] - n<T>(13,4)*z[32] - n<T>(5,2)*z[33] - 8*z[34] - 
      z[35] + n<T>(5,12)*z[36] - n<T>(5,4)*z[37] + z[38] - n<T>(1,2)*z[41] + n<T>(1,12)
      *z[42] + z[44] + 3*z[45] + n<T>(1,3)*z[46] + z[47] + z[48] + z[49] + 
      z[50] + z[51] + z[52] + n<T>(11,3)*z[53] - n<T>(1,4)*z[54] + z[55] + n<T>(5,3)
      *z[56] + z[57] + z[58] + n<T>(13,3)*z[59] + z[60] - n<T>(4,9)*z[61] + 
      z[62];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1467(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1467(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
