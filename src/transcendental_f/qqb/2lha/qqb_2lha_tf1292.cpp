#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1292(
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[10];
  std::complex<T> r(0,0);

    z[1]=d[4];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=d[9];
    z[5]=e[6];
    z[6]=e[13];
    z[7]= - z[3]*z[4];
    z[8]=npow(z[1],2);
    z[9]= - n<T>(1,2)*z[2] - z[1] + z[4] + z[3];
    z[9]=z[2]*z[9];

    r += z[5] - z[6] + z[7] + n<T>(1,2)*z[8] + z[9];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1292(
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1292(
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
