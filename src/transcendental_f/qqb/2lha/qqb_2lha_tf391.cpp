#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf391(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[116];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[12];
    z[13]=d[15];
    z[14]=d[11];
    z[15]=d[16];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[11];
    z[27]=e[12];
    z[28]=c[3];
    z[29]=c[11];
    z[30]=c[15];
    z[31]=c[18];
    z[32]=c[19];
    z[33]=c[21];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=c[29];
    z[39]=c[31];
    z[40]=e[3];
    z[41]=e[10];
    z[42]=e[6];
    z[43]=e[13];
    z[44]=e[14];
    z[45]=f[0];
    z[46]=f[1];
    z[47]=f[2];
    z[48]=f[3];
    z[49]=f[4];
    z[50]=f[5];
    z[51]=f[6];
    z[52]=f[8];
    z[53]=f[9];
    z[54]=f[10];
    z[55]=f[11];
    z[56]=f[12];
    z[57]=f[13];
    z[58]=f[14];
    z[59]=f[15];
    z[60]=f[16];
    z[61]=f[17];
    z[62]=f[18];
    z[63]=f[21];
    z[64]=f[23];
    z[65]=f[28];
    z[66]=f[29];
    z[67]=f[30];
    z[68]=f[31];
    z[69]=f[32];
    z[70]=f[33];
    z[71]=f[34];
    z[72]=f[35];
    z[73]=f[36];
    z[74]=f[39];
    z[75]=f[43];
    z[76]=f[50];
    z[77]=f[51];
    z[78]=f[52];
    z[79]=f[54];
    z[80]=f[55];
    z[81]=f[56];
    z[82]=f[60];
    z[83]=n<T>(1,2)*z[5];
    z[84]=z[1]*i;
    z[85]=z[83] - z[84];
    z[86]=z[85]*z[5];
    z[87]=n<T>(1,4)*z[2];
    z[88]=n<T>(1,2)*z[84];
    z[89]= - z[87] - z[88] + z[5];
    z[89]=z[2]*z[89];
    z[90]=n<T>(1,2)*z[9];
    z[91]=z[90] - z[84];
    z[92]=z[9]*z[91];
    z[93]=n<T>(1,2)*z[4];
    z[94]=11*z[84];
    z[95]= - z[94] + 13*z[5];
    z[95]= - n<T>(11,8)*z[3] + z[93] + n<T>(1,4)*z[95] - z[2];
    z[95]=z[3]*z[95];
    z[96]=n<T>(3,2)*z[84];
    z[97]=n<T>(1,4)*z[4] + z[96] - z[5];
    z[97]=z[4]*z[97];
    z[98]= - n<T>(13,3)*z[84] + z[5];
    z[98]= - z[4] + n<T>(5,6)*z[9] + n<T>(1,4)*z[98] + z[2];
    z[98]= - n<T>(1,12)*z[6] + n<T>(1,4)*z[98] - 2*z[3];
    z[98]=z[6]*z[98];
    z[89]=z[98] + z[95] + z[97] + n<T>(5,12)*z[92] + n<T>(1,8)*z[86] + z[89];
    z[89]=z[6]*z[89];
    z[92]=3*z[84];
    z[95]=z[2] - z[5];
    z[97]=z[90] - z[92] - z[95];
    z[97]=z[9]*z[97];
    z[98]=n<T>(1,2)*z[2];
    z[99]=z[98] - z[5];
    z[100]=z[99] + z[84];
    z[100]=z[100]*z[2];
    z[101]=z[85]*z[83];
    z[97]=z[97] + z[101] + z[100];
    z[101]=n<T>(3,4)*z[9];
    z[102]=3*z[5];
    z[103]= - n<T>(5,4)*z[3] - z[93] - z[101] + n<T>(3,4)*z[2] - n<T>(5,2)*z[84]
     + 
    z[102];
    z[103]=z[3]*z[103];
    z[104]=n<T>(31,8)*z[6];
    z[105]=z[3] - z[4];
    z[106]=23*z[84];
    z[107]=z[106] - 27*z[5];
    z[107]=z[104] + n<T>(1,4)*z[107] - z[105];
    z[108]=n<T>(1,2)*z[6];
    z[107]=z[107]*z[108];
    z[109]=n<T>(1,3)*z[84];
    z[110]= - z[109] + z[102];
    z[111]=z[9] - z[2];
    z[110]=n<T>(1,2)*z[110] + 9*z[111];
    z[111]=n<T>(1,3)*z[4];
    z[104]=z[104] - n<T>(17,4)*z[3] + n<T>(1,4)*z[110] - z[111];
    z[104]=n<T>(1,2)*z[104] - n<T>(2,3)*z[10];
    z[104]=z[10]*z[104];
    z[110]=z[84] - z[93];
    z[110]=z[110]*z[111];
    z[97]=z[104] + z[107] + z[103] + n<T>(3,4)*z[97] + z[110];
    z[97]=z[10]*z[97];
    z[103]=n<T>(1,4)*z[5];
    z[104]=27*z[84] - z[5];
    z[104]=z[104]*z[103];
    z[107]=z[95] + z[91];
    z[101]=z[107]*z[101];
    z[107]=2*z[84];
    z[110]=n<T>(11,4)*z[2] - z[107] - n<T>(17,4)*z[5];
    z[110]=z[2]*z[110];
    z[96]= - n<T>(5,4)*z[4] + z[96] + z[5];
    z[96]=z[4]*z[96];
    z[112]=7*z[84];
    z[113]=5*z[9] + 19*z[2] - z[112] - 9*z[5];
    z[113]=n<T>(1,2)*z[113] + z[4];
    z[113]=n<T>(1,4)*z[113] + n<T>(5,3)*z[3];
    z[113]=z[3]*z[113];
    z[96]=z[113] + z[96] + z[101] + z[104] + z[110];
    z[96]=z[3]*z[96];
    z[101]=z[112] - z[83];
    z[101]=z[101]*z[83];
    z[104]=4*z[84] + z[99];
    z[104]=z[2]*z[104];
    z[110]=n<T>(7,4)*z[9] - n<T>(7,2)*z[84] - z[95];
    z[113]=n<T>(5,2)*z[9];
    z[110]=z[110]*z[113];
    z[101]=z[110] + z[101] + z[104];
    z[104]=z[111] - z[98] - n<T>(2,3)*z[84] + z[83];
    z[104]=z[4]*z[104];
    z[110]=z[84] - z[9];
    z[114]=z[110] + z[108];
    z[115]=z[6]*z[114];
    z[88]= - z[88] + z[95];
    z[88]=n<T>(23,3)*z[88] + z[113];
    z[88]= - n<T>(25,9)*z[7] + n<T>(5,8)*z[6] + n<T>(1,4)*z[88] + z[111];
    z[88]=z[7]*z[88];
    z[88]=z[88] + n<T>(5,4)*z[115] + n<T>(1,3)*z[101] + z[104];
    z[88]=z[7]*z[88];
    z[99]=z[7] - z[10] + z[90] + z[99];
    z[99]=z[11]*z[99];
    z[101]=z[84] - z[5];
    z[104]=z[2]*z[101];
    z[111]=n<T>(5,6)*z[4] - z[92] + z[5];
    z[111]=z[4]*z[111];
    z[99]=z[99] + z[104] + z[111];
    z[104]= - z[93] - z[91];
    z[104]=z[3]*z[104];
    z[111]=z[2] + z[105];
    z[111]=z[111]*z[108];
    z[113]=z[84] - z[4];
    z[115]= - n<T>(1,6)*z[10] - z[108] - n<T>(1,3)*z[113] + n<T>(1,2)*z[3];
    z[115]=z[10]*z[115];
    z[109]=z[109] + z[2];
    z[109]=n<T>(1,12)*z[7] + n<T>(1,2)*z[109] - n<T>(2,3)*z[4];
    z[109]=z[7]*z[109];
    z[99]=z[109] + z[115] + z[111] + z[104] + n<T>(1,2)*z[99];
    z[99]=z[11]*z[99];
    z[83]= - n<T>(7,6)*z[8] + n<T>(5,2)*z[7] - z[90] - z[2] - z[84] - z[83];
    z[83]=z[8]*z[83];
    z[83]=z[83] - z[86] - z[100];
    z[86]=z[84] + z[95];
    z[86]=n<T>(1,2)*z[86] - z[9];
    z[86]=z[9]*z[86];
    z[90]= - z[114]*z[108];
    z[100]= - n<T>(1,2)*z[95] + z[7];
    z[100]=z[7]*z[100];
    z[83]=z[100] + z[90] + z[86] + n<T>(1,2)*z[83];
    z[83]=z[8]*z[83];
    z[86]= - n<T>(5,2)*z[8] - n<T>(7,2)*z[7] + z[95] + z[110];
    z[86]=z[23]*z[86];
    z[83]=z[83] + z[86];
    z[86]=z[10] - z[3];
    z[86]=n<T>(23,2)*z[9] + n<T>(17,2)*z[2] - z[106] + z[102] + 3*z[86];
    z[86]=z[24]*z[86];
    z[90]=65*z[2] - 127*z[84] - 19*z[5];
    z[87]=z[90]*z[87];
    z[90]= - z[94] - n<T>(13,2)*z[5];
    z[90]=z[5]*z[90];
    z[87]=z[90] + z[87];
    z[87]=z[2]*z[87];
    z[90]= - z[92] + n<T>(11,3)*z[5];
    z[90]=z[90]*npow(z[5],2);
    z[86]=z[86] + n<T>(1,2)*z[90] + n<T>(1,3)*z[87] - z[82] + z[81] + z[78] - 
    z[50];
    z[87]= - z[5] - z[3];
    z[87]=z[84]*z[87];
    z[87]= - z[28] + z[87];
    z[87]=z[13]*z[87];
    z[90]= - z[3] + z[101];
    z[90]=z[20]*z[90];
    z[92]=z[3] + z[6];
    z[92]=z[40]*z[92];
    z[87]=z[58] + z[87] + z[90] + z[92] + z[69];
    z[90]= - 15*z[84] + n<T>(23,2)*z[5];
    z[90]=z[90]*z[103];
    z[92]= - n<T>(3,2)*z[2] + n<T>(25,2)*z[84] + z[102];
    z[92]=z[2]*z[92];
    z[90]=z[90] + z[92];
    z[92]= - n<T>(7,6)*z[9] - n<T>(25,8)*z[2] + z[107] + n<T>(15,16)*z[5];
    z[92]=z[9]*z[92];
    z[90]=n<T>(1,2)*z[90] + z[92];
    z[90]=z[9]*z[90];
    z[92]= - z[112] - n<T>(17,2)*z[5];
    z[92]=z[92]*z[103];
    z[94]= - n<T>(31,4)*z[2] + 25*z[84] + n<T>(49,2)*z[5];
    z[94]=z[2]*z[94];
    z[92]=z[92] + z[94];
    z[91]= - z[5] + z[91];
    z[91]=z[9]*z[91];
    z[94]=43*z[84] + n<T>(7,4)*z[5];
    z[94]=n<T>(1,2)*z[94] - 17*z[2];
    z[94]= - n<T>(3,2)*z[4] + n<T>(1,3)*z[94] + n<T>(35,8)*z[9];
    z[94]=z[4]*z[94];
    z[91]=z[94] + n<T>(1,3)*z[92] + n<T>(35,4)*z[91];
    z[91]=z[91]*z[93];
    z[92]= - z[6] - z[8];
    z[92]=z[42]*z[92];
    z[93]= - z[7] - z[11];
    z[93]=z[43]*z[93];
    z[92]= - z[67] - z[92] - z[93] + z[75] - z[72];
    z[93]=z[84]*z[4];
    z[93]=z[93] + z[28];
    z[94]=z[11]*z[84];
    z[94]=z[94] + z[93];
    z[94]=z[14]*z[94];
    z[95]= - z[11] + z[113];
    z[95]=z[27]*z[95];
    z[94]= - z[94] - z[95] - z[80] - z[79] + z[76] - z[30];
    z[95]=z[84]*z[9];
    z[95]=z[95] + z[28];
    z[100]=z[10]*z[84];
    z[100]=z[100] + z[95];
    z[100]=z[17]*z[100];
    z[101]= - z[10] + z[110];
    z[101]=z[26]*z[101];
    z[100]=z[100] + z[101] + z[64] - z[63];
    z[101]=z[8]*z[84];
    z[95]=z[101] + z[95];
    z[95]=z[16]*z[95];
    z[101]=z[8] - z[110];
    z[101]=z[25]*z[101];
    z[95]=z[95] + z[101];
    z[101]=z[7] + z[5];
    z[101]=z[105] + 2*z[101];
    z[102]= - z[84]*z[101];
    z[102]= - z[28] + z[102];
    z[102]=z[12]*z[102];
    z[101]= - z[84] + z[101];
    z[101]=z[22]*z[101];
    z[103]= - z[6]*z[84];
    z[93]=z[103] - z[93];
    z[93]=z[15]*z[93];
    z[85]= - z[98] - z[85];
    z[85]=z[19]*z[85];
    z[98]=z[31] + z[77] + z[62];
    z[103]=z[71] - z[66] + z[65] - z[38];
    z[104]=z[59] + z[46];
    z[105]=n<T>(31,3)*z[36] + n<T>(97,24)*z[34] + n<T>(5,2)*z[33] + n<T>(133,144)*z[29];
    z[105]=i*z[105];
    z[106]= - n<T>(277,2)*z[5] - 235*z[2];
    z[106]=n<T>(11,6)*z[3] + n<T>(653,36)*z[4] + n<T>(1,18)*z[106] + 7*z[9];
    z[106]=n<T>(5,18)*z[8] - n<T>(1,18)*z[11] - n<T>(22,9)*z[7] + n<T>(7,18)*z[10]
     + n<T>(1,4)*z[106] - n<T>(7,9)*z[6];
    z[106]=z[28]*z[106];
    z[84]= - 19*z[84] + n<T>(25,2)*z[2];
    z[84]= - z[6] + z[3] + n<T>(25,12)*z[4] + n<T>(1,6)*z[84] - z[9];
    z[84]=z[18]*z[84];
    z[107]=z[6] - z[113];
    z[107]=z[21]*z[107];
    z[108]=z[3] + z[10];
    z[108]=z[41]*z[108];
    z[109]=z[10] + z[11];
    z[109]=z[44]*z[109];

    r += n<T>(97,72)*z[32] - n<T>(65,24)*z[35] - n<T>(65,12)*z[37] + n<T>(101,72)*z[39]
       - z[45] + n<T>(7,2)*z[47] - n<T>(17,48)*z[48] - n<T>(21,16)*z[49] - n<T>(13,8)*
      z[51] + 6*z[52] + n<T>(1,8)*z[53] - n<T>(9,4)*z[54] + n<T>(19,16)*z[55] - n<T>(1,16)*z[56]
     + n<T>(55,12)*z[57]
     + n<T>(27,16)*z[60]
     + n<T>(51,16)*z[61]
     + n<T>(55,24)*z[68]
     + n<T>(20,3)*z[70] - n<T>(5,24)*z[73]
     + n<T>(5,8)*z[74]
     + n<T>(5,3)*
      z[83] + z[84] + n<T>(11,6)*z[85] + n<T>(1,2)*z[86] + n<T>(5,4)*z[87] + z[88]
       + z[89] + z[90] + z[91] - n<T>(5,6)*z[92] + z[93] - n<T>(1,6)*z[94] + n<T>(5,2)*z[95]
     + z[96]
     + z[97]
     + n<T>(1,3)*z[98]
     + z[99]
     + n<T>(3,2)*z[100]
     +  z[101] + z[102] + n<T>(1,4)*z[103] - n<T>(3,4)*z[104] + z[105] + z[106]
       + z[107] + n<T>(7,4)*z[108] + n<T>(2,3)*z[109];
 
    return r;
}

template std::complex<double> qqb_2lha_tf391(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf391(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
