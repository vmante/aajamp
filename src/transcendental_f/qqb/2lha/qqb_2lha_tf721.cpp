#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf721(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[108];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[1];
    z[10]=d[4];
    z[11]=d[15];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[9];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[18];
    z[34]=c[19];
    z[35]=c[21];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[29];
    z[41]=c[31];
    z[42]=e[3];
    z[43]=e[14];
    z[44]=f[1];
    z[45]=f[2];
    z[46]=f[3];
    z[47]=f[4];
    z[48]=f[5];
    z[49]=f[6];
    z[50]=f[7];
    z[51]=f[10];
    z[52]=f[11];
    z[53]=f[12];
    z[54]=f[14];
    z[55]=f[15];
    z[56]=f[16];
    z[57]=f[17];
    z[58]=f[18];
    z[59]=f[20];
    z[60]=f[21];
    z[61]=f[30];
    z[62]=f[31];
    z[63]=f[32];
    z[64]=f[33];
    z[65]=f[35];
    z[66]=f[37];
    z[67]=f[39];
    z[68]=f[43];
    z[69]=f[50];
    z[70]=f[51];
    z[71]=f[52];
    z[72]=f[53];
    z[73]=f[54];
    z[74]=f[55];
    z[75]=f[56];
    z[76]=f[58];
    z[77]=n<T>(5,6)*z[8];
    z[78]=n<T>(7,6)*z[5];
    z[79]=3*z[7];
    z[80]=n<T>(1,2)*z[6];
    z[81]=z[1]*i;
    z[82]= - z[80] - z[78] - z[77] - n<T>(7,3)*z[4] - 11*z[81] - z[79];
    z[83]=n<T>(8,3)*z[2];
    z[82]=n<T>(3,2)*z[3] + n<T>(5,4)*z[9] + n<T>(1,2)*z[82] + z[83];
    z[82]=z[3]*z[82];
    z[84]=n<T>(1,2)*z[7];
    z[85]=z[84] - z[81];
    z[86]= - z[85]*z[79];
    z[87]=n<T>(7,3)*z[81];
    z[88]= - n<T>(7,6)*z[4] + z[87] + z[79];
    z[88]=z[4]*z[88];
    z[89]=n<T>(1,2)*z[8];
    z[90]=z[81] - z[89];
    z[90]=z[90]*z[77];
    z[91]= - z[78] + z[87] + 3*z[8];
    z[92]=n<T>(1,2)*z[5];
    z[91]=z[91]*z[92];
    z[93]=z[81] - z[80];
    z[93]=z[93]*z[80];
    z[94]=n<T>(1,2)*z[9];
    z[95]=5*z[81];
    z[96]= - z[94] - z[95] + z[6];
    z[94]=z[96]*z[94];
    z[96]= - 8*z[81] - z[4];
    z[96]=2*z[96] + z[2];
    z[96]=z[2]*z[96];
    z[82]=z[82] + z[94] + n<T>(1,3)*z[96] + z[93] + z[91] + z[90] + z[86] + 
    z[88];
    z[82]=z[3]*z[82];
    z[86]= - n<T>(11,4)*z[9] + z[80] + n<T>(11,2)*z[4] - z[95] - z[84];
    z[86]=z[9]*z[86];
    z[88]=z[81] - z[4];
    z[90]=z[6] - z[5];
    z[90]= - n<T>(1,4)*z[90];
    z[91]=4*z[8];
    z[93]= - z[10] + z[3] - n<T>(21,4)*z[9] - z[2] - n<T>(5,4)*z[88] + z[91] - 
    z[90];
    z[93]=z[10]*z[93];
    z[94]=n<T>(1,2)*z[4];
    z[95]=z[94] - z[81];
    z[95]=z[95]*z[4];
    z[96]=15*z[81] + z[7];
    z[91]=z[91] + n<T>(1,2)*z[96] - 8*z[4];
    z[91]=z[8]*z[91];
    z[96]=z[81] - z[7];
    z[97]= - z[92] - z[96];
    z[97]=z[97]*z[92];
    z[98]=z[96] - z[8];
    z[99]=z[80] + z[98];
    z[99]=z[99]*z[80];
    z[100]=2*z[81];
    z[101]= - z[3]*z[100];
    z[86]=z[93] + z[101] + z[86] + z[99] + z[97] + n<T>(5,2)*z[95] + z[91];
    z[86]=z[10]*z[86];
    z[91]=z[85]*z[7];
    z[93]=7*z[81];
    z[97]= - 7*z[4] + z[93] - z[79];
    z[97]=z[4]*z[97];
    z[99]=n<T>(7,2)*z[7];
    z[101]=4*z[81];
    z[102]= - n<T>(39,4)*z[8] - n<T>(7,2)*z[4] + z[101] - z[99];
    z[102]=z[8]*z[102];
    z[103]=z[8] - z[7];
    z[104]=z[103]*z[80];
    z[93]= - z[93] + n<T>(9,2)*z[7];
    z[93]=10*z[9] - n<T>(9,2)*z[2] - z[80] + n<T>(7,4)*z[8] + n<T>(1,2)*z[93] - z[4]
   ;
    z[93]=z[9]*z[93];
    z[105]=z[4] + z[7];
    z[106]= - z[2] + z[8] + z[105];
    z[106]=z[2]*z[106];
    z[93]=z[93] + 3*z[106] + z[104] + z[102] - n<T>(5,2)*z[91] + z[97];
    z[93]=z[9]*z[93];
    z[97]=n<T>(1,6)*z[4] - z[81] - z[84];
    z[97]=z[4]*z[97];
    z[84]=z[100] - z[84];
    z[102]= - z[8] - z[84];
    z[102]=z[8]*z[102];
    z[104]=n<T>(1,3)*z[7];
    z[106]=n<T>(1,2)*z[81] - z[104];
    z[106]=n<T>(13,4)*z[5] - z[89] + n<T>(7,2)*z[106] + z[4];
    z[106]=z[5]*z[106];
    z[97]=z[106] + z[102] - n<T>(7,3)*z[91] + z[97];
    z[97]=z[5]*z[97];
    z[102]=4*z[4];
    z[106]=n<T>(5,2)*z[7] - z[102];
    z[92]= - z[92] + n<T>(1,3)*z[106] - z[89];
    z[92]=z[5]*z[92];
    z[99]= - z[100] + z[99];
    z[99]=z[99]*z[104];
    z[104]=n<T>(5,6)*z[4] - n<T>(5,3)*z[81] + z[7];
    z[94]=z[104]*z[94];
    z[100]= - n<T>(17,4)*z[5] + n<T>(5,4)*z[4] + z[100] + z[7];
    z[100]=n<T>(1,3)*z[100] + z[80];
    z[100]=z[6]*z[100];
    z[92]=z[100] + z[92] + z[99] + z[94];
    z[92]=z[6]*z[92];
    z[94]=5*z[7];
    z[99]= - z[101] + z[94];
    z[99]=z[7]*z[99];
    z[100]= - z[81] - z[7];
    z[100]=2*z[100] + z[4];
    z[100]=z[100]*z[102];
    z[99]=z[99] + z[100];
    z[100]=5*z[4] + n<T>(31,2)*z[81] + z[7];
    z[89]= - n<T>(5,2)*z[2] + n<T>(1,3)*z[100] + z[89] - z[90];
    z[89]=z[2]*z[89];
    z[90]=z[105] - z[81];
    z[100]=n<T>(1,2)*z[90] - z[5];
    z[100]=z[5]*z[100];
    z[101]=z[6] + z[5] - z[90];
    z[80]=z[101]*z[80];
    z[101]= - z[7] + 3*z[88];
    z[101]=z[8]*z[101];
    z[80]=z[89] + z[80] + z[100] + n<T>(1,3)*z[99] + z[101];
    z[80]=z[2]*z[80];
    z[78]= - z[78] - n<T>(1,3)*z[81] - z[103];
    z[78]=z[5]*z[78];
    z[87]=n<T>(7,6)*z[8] + z[87] - z[7];
    z[87]=z[8]*z[87];
    z[89]=z[103] + z[4];
    z[99]= - z[5] + z[89];
    z[99]=z[6]*z[99];
    z[78]=z[99] + z[78] + z[87] + z[91] - z[95];
    z[87]= - 2*z[8] - z[5];
    z[87]=n<T>(1,3)*z[87] + n<T>(1,2)*z[3];
    z[87]=z[3]*z[87];
    z[89]= - 3*z[5] - z[89];
    z[89]=n<T>(1,4)*z[89] + z[14];
    z[89]=z[14]*z[89];
    z[78]=z[89] + n<T>(1,2)*z[78] + z[87];
    z[78]=z[14]*z[78];
    z[87]=n<T>(23,4)*z[4];
    z[79]=z[87] - n<T>(23,2)*z[81] + z[79];
    z[79]=z[4]*z[79];
    z[77]=z[77] - n<T>(16,3)*z[81] + z[87];
    z[77]=z[8]*z[77];
    z[84]= - z[7]*z[84];
    z[77]=z[77] + z[84] + z[79];
    z[77]=z[8]*z[77];
    z[79]=z[81]*z[7];
    z[79]=z[79] + z[30];
    z[84]= - z[6]*z[81];
    z[84]=z[84] - z[79];
    z[84]=z[16]*z[84];
    z[87]= - z[6] + z[96];
    z[87]=z[25]*z[87];
    z[89]=z[8] + z[14];
    z[89]=z[43]*z[89];
    z[84]=z[84] + z[87] + z[89] - z[62];
    z[85]= - z[4] - z[85];
    z[85]=z[4]*z[85];
    z[85]= - z[91] + z[85];
    z[85]=z[4]*z[85];
    z[85]= - z[59] + z[85] + z[73];
    z[87]=z[9] - z[4];
    z[89]= - n<T>(8,3)*z[3] - z[83] + n<T>(13,3)*z[81] - z[87];
    z[89]=z[18]*z[89];
    z[91]=z[81]*z[4];
    z[91]=z[91] + z[30];
    z[95]=z[5]*z[81];
    z[95]=z[95] + z[91];
    z[95]=z[15]*z[95];
    z[99]= - z[5] + z[88];
    z[99]=z[22]*z[99];
    z[95]= - z[95] - z[99] + z[69] + z[33];
    z[99]=z[81]*z[3];
    z[99]=z[99] + z[30];
    z[100]=z[10]*z[81];
    z[100]=z[100] + z[99];
    z[100]=z[13]*z[100];
    z[101]=z[81] - z[3];
    z[102]= - z[10] + z[101];
    z[102]=z[21]*z[102];
    z[100]=z[100] + z[102];
    z[102]=z[9]*z[81];
    z[91]=z[102] + z[91];
    z[91]=z[11]*z[91];
    z[88]=z[9] - z[88];
    z[88]=z[20]*z[88];
    z[88]=z[91] + z[88];
    z[90]=z[90] - z[2];
    z[91]=n<T>(17,6)*z[6] + n<T>(23,6)*z[5] + z[90];
    z[91]=z[23]*z[91];
    z[90]=17*z[9] + 23*z[8] + 6*z[90];
    z[90]=z[26]*z[90];
    z[102]=n<T>(7,3)*z[14] + z[6] + n<T>(10,3)*z[5] + z[81] + z[103];
    z[102]=z[29]*z[102];
    z[94]=13*z[81] - z[94];
    z[87]= - n<T>(5,3)*z[2] + n<T>(1,3)*z[94] + z[87];
    z[87]=z[24]*z[87];
    z[83]=z[83] + n<T>(5,3)*z[4] - n<T>(10,3)*z[81] - z[7];
    z[83]=z[19]*z[83];
    z[94]= - z[14]*z[81];
    z[94]=z[94] - z[99];
    z[94]=z[12]*z[94];
    z[81]= - z[8]*z[81];
    z[79]=z[81] - z[79];
    z[79]=z[17]*z[79];
    z[81]=z[96]*npow(z[7],2);
    z[81]= - z[40] - z[51] + z[81] - z[68];
    z[96]= - z[75] - z[71] + z[67] + z[66];
    z[99]= - z[72] - z[64] + z[56] + z[55];
    z[103]=z[53] - z[49];
    z[104]= - n<T>(10,3)*z[38] - n<T>(25,6)*z[36] - n<T>(7,36)*z[31];
    z[104]=i*z[104];
    z[105]= - n<T>(277,6)*z[8] + z[7] + n<T>(11,3)*z[4];
    z[105]= - z[14] + n<T>(23,3)*z[2] - n<T>(1,6)*z[6] + n<T>(1,2)*z[105] + n<T>(22,3)*
    z[5];
    z[105]= - n<T>(7,6)*z[10] - n<T>(113,18)*z[3] + n<T>(7,4)*z[9] + n<T>(1,3)*z[105];
    z[105]=z[30]*z[105];
    z[106]=5*z[10] + 6*z[9] - z[6] + z[7] - z[4];
    z[106]=z[42]*z[106];
    z[98]= - z[27]*z[98];
    z[101]=z[14] - z[101];
    z[101]=z[28]*z[101];
    z[107]=2*i;
    z[107]= - z[35]*z[107];

    r +=  - n<T>(37,6)*z[32] - n<T>(17,9)*z[34] + n<T>(17,6)*z[37] + n<T>(8,3)*z[39]
     - 
      n<T>(73,12)*z[41] + z[44] + n<T>(9,2)*z[45] - n<T>(7,6)*z[46] + n<T>(23,4)*z[47]
       - n<T>(10,3)*z[48] + 24*z[50] - n<T>(1,6)*z[52] - n<T>(7,4)*z[54] - n<T>(3,2)*
      z[57] - z[58] + 3*z[60] - z[61] - n<T>(11,12)*z[63] - n<T>(5,6)*z[65] - n<T>(7,12)*z[70]
     + n<T>(5,12)*z[74] - z[76]
     + z[77]
     + z[78]
     + z[79]
     + z[80]
       - n<T>(1,2)*z[81] + z[82] + z[83] + n<T>(5,3)*z[84] + n<T>(1,3)*z[85] + 
      z[86] + z[87] + 7*z[88] + z[89] + z[90] + z[91] + z[92] + z[93]
       + z[94] - n<T>(2,3)*z[95] - n<T>(1,4)*z[96] + z[97] + z[98] + 4*z[99] + 
      2*z[100] + z[101] + z[102] - n<T>(5,4)*z[103] + z[104] + z[105] + 
      z[106] + z[107];
 
    return r;
}

template std::complex<double> qqb_2lha_tf721(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf721(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
