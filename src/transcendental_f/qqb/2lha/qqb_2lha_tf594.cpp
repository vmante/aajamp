#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf594(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[112];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[12];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[5];
    z[21]=e[7];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=e[10];
    z[25]=e[11];
    z[26]=e[12];
    z[27]=c[3];
    z[28]=c[11];
    z[29]=c[15];
    z[30]=c[18];
    z[31]=c[19];
    z[32]=c[21];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[29];
    z[38]=c[31];
    z[39]=e[3];
    z[40]=e[6];
    z[41]=e[13];
    z[42]=e[14];
    z[43]=f[0];
    z[44]=f[1];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[6];
    z[48]=f[7];
    z[49]=f[8];
    z[50]=f[9];
    z[51]=f[11];
    z[52]=f[12];
    z[53]=f[13];
    z[54]=f[16];
    z[55]=f[17];
    z[56]=f[20];
    z[57]=f[23];
    z[58]=f[30];
    z[59]=f[31];
    z[60]=f[32];
    z[61]=f[33];
    z[62]=f[35];
    z[63]=f[36];
    z[64]=f[37];
    z[65]=f[39];
    z[66]=f[43];
    z[67]=f[50];
    z[68]=f[51];
    z[69]=f[52];
    z[70]=f[54];
    z[71]=f[55];
    z[72]=f[57];
    z[73]=f[58];
    z[74]=f[66];
    z[75]=f[67];
    z[76]=z[4] - z[2];
    z[77]=n<T>(1,4)*z[6];
    z[78]=z[1]*i;
    z[79]=n<T>(1,2)*z[78];
    z[80]=n<T>(1,2)*z[9];
    z[81]= - z[77] + z[80] - z[79] + z[76];
    z[82]=n<T>(1,2)*z[6];
    z[81]=z[81]*z[82];
    z[83]=7*z[2];
    z[84]=n<T>(11,4)*z[78] + z[83];
    z[85]=n<T>(5,12)*z[5];
    z[86]=n<T>(1,2)*z[4];
    z[84]=n<T>(7,18)*z[8] + z[82] - n<T>(1,12)*z[7] - z[86] + z[85] + n<T>(1,3)*
    z[84] - n<T>(3,2)*z[9];
    z[84]=z[8]*z[84];
    z[87]=13*z[78];
    z[88]= - z[87] + z[83];
    z[89]=n<T>(1,12)*z[2];
    z[88]=z[88]*z[89];
    z[90]=n<T>(1,3)*z[2];
    z[91]=n<T>(7,2)*z[78];
    z[92]=z[91] - z[90];
    z[92]=n<T>(1,2)*z[92] - n<T>(1,3)*z[9];
    z[92]=z[9]*z[92];
    z[93]=z[9] - z[2];
    z[94]=z[93] - z[78];
    z[95]=n<T>(1,2)*z[5];
    z[96]=z[95] + z[94];
    z[96]=z[96]*z[85];
    z[97]= - z[86] - z[94];
    z[97]=z[97]*z[86];
    z[98]=z[5] - z[2];
    z[99]=z[78] - z[9];
    z[100]= - n<T>(1,6)*z[7] - z[99] - n<T>(5,6)*z[98];
    z[101]=n<T>(1,2)*z[7];
    z[100]=z[100]*z[101];
    z[102]=z[10]*z[93];
    z[81]=n<T>(1,2)*z[84] + z[81] + z[102] + z[100] + z[97] + z[96] + z[88]
    + z[92];
    z[81]=z[8]*z[81];
    z[84]=n<T>(1,2)*z[2];
    z[88]=z[84] + z[95];
    z[92]=z[99] + z[101];
    z[96]= - z[4] + z[92] + z[88];
    z[96]= - n<T>(5,12)*z[11] + n<T>(1,2)*z[96] + z[10];
    z[96]=z[11]*z[96];
    z[97]=z[78] + z[2];
    z[100]= - z[97]*z[84];
    z[102]=z[84] + z[78];
    z[103]= - z[80] + z[102];
    z[103]=z[9]*z[103];
    z[104]=5*z[2];
    z[105]=z[78] + z[104];
    z[105]= - n<T>(1,8)*z[5] + n<T>(1,4)*z[105] - z[9];
    z[105]=z[5]*z[105];
    z[88]= - n<T>(7,12)*z[4] + z[9] - z[88];
    z[88]=z[88]*z[86];
    z[106]=z[78] - z[4];
    z[107]=n<T>(1,4)*z[5];
    z[108]= - n<T>(1,3)*z[7] - z[107] + n<T>(1,4)*z[2] - n<T>(2,3)*z[106];
    z[108]=z[7]*z[108];
    z[109]= - n<T>(5,12)*z[10] + z[93] - n<T>(5,6)*z[106];
    z[110]=n<T>(1,2)*z[10];
    z[109]=z[109]*z[110];
    z[88]=n<T>(1,2)*z[96] + z[109] + z[108] + z[88] + z[105] + z[100] + 
    z[103];
    z[88]=z[11]*z[88];
    z[96]=n<T>(5,4)*z[7];
    z[100]=n<T>(3,2)*z[10];
    z[76]= - n<T>(3,2)*z[6] + z[100] + z[96] + z[95] + n<T>(3,4)*z[9] - n<T>(5,4)*
    z[78] + z[76];
    z[76]=z[76]*z[82];
    z[103]= - z[78] + z[84];
    z[103]=z[2]*z[103];
    z[105]= - z[78] + z[95];
    z[105]=z[105]*z[95];
    z[108]=z[86] - z[2] + z[99];
    z[108]=z[4]*z[108];
    z[92]=z[92]*z[96];
    z[96]=z[78] - z[5];
    z[109]=z[110] + z[96];
    z[109]=z[109]*z[100];
    z[111]=n<T>(3,8)*z[9] - n<T>(3,4)*z[78] + z[2];
    z[111]=z[9]*z[111];
    z[76]=z[76] + z[109] + z[92] + z[108] + z[105] + z[103] + z[111];
    z[76]=z[76]*z[82];
    z[92]=n<T>(17,12)*z[4];
    z[100]= - z[100] + n<T>(9,4)*z[7] + z[92] + n<T>(7,2)*z[5] - n<T>(59,12)*z[78]
    - 3*z[93];
    z[100]=z[10]*z[100];
    z[91]=n<T>(7,4)*z[5] - z[91] + z[93];
    z[91]=z[91]*z[95];
    z[103]=n<T>(17,24)*z[4] - n<T>(17,12)*z[78] - z[93];
    z[103]=z[103]*z[86];
    z[105]=z[101] + z[106];
    z[105]=z[7]*z[105];
    z[108]= - z[78] + z[2];
    z[108]=z[2]*z[108];
    z[109]=n<T>(5,2)*z[78];
    z[111]=z[109] - z[2];
    z[111]=z[9]*z[111];
    z[91]=n<T>(1,4)*z[100] + n<T>(9,8)*z[105] + z[103] + z[91] + z[108] + z[111]
   ;
    z[91]=z[10]*z[91];
    z[100]=n<T>(1,4)*z[9];
    z[77]=n<T>(11,12)*z[3] - z[77] + z[110] + z[107] + z[100] - z[97];
    z[77]=z[3]*z[77];
    z[103]= - n<T>(5,4)*z[10] - z[80] + z[84] + z[96];
    z[103]=z[10]*z[103];
    z[105]=z[80] - z[78];
    z[107]= - z[2] - z[105];
    z[107]=z[9]*z[107];
    z[108]= - z[78] + n<T>(3,2)*z[2];
    z[108]=z[2]*z[108];
    z[107]=z[108] + z[107];
    z[94]= - n<T>(1,2)*z[94] - z[5];
    z[94]=z[5]*z[94];
    z[108]= - z[6] - z[96];
    z[82]=z[108]*z[82];
    z[108]=z[4]*z[93];
    z[77]=z[77] + z[82] + z[103] + z[108] + n<T>(1,2)*z[107] + z[94];
    z[77]=z[3]*z[77];
    z[82]= - z[109] - z[2];
    z[80]=z[85] + n<T>(1,3)*z[82] - z[80];
    z[82]=n<T>(5,2)*z[5];
    z[80]=z[80]*z[82];
    z[85]=7*z[78] + n<T>(11,4)*z[2];
    z[85]=z[85]*z[90];
    z[90]=z[9]*z[105];
    z[94]=n<T>(1,8)*z[78] - z[104];
    z[94]= - n<T>(5,24)*z[4] + n<T>(25,24)*z[5] + n<T>(1,3)*z[94] + n<T>(5,8)*z[9];
    z[94]=z[4]*z[94];
    z[80]=z[94] + z[80] + z[85] + n<T>(5,4)*z[90];
    z[80]=z[4]*z[80];
    z[85]=z[78]*z[5];
    z[85]=z[85] + z[27];
    z[90]=z[3]*z[78];
    z[90]=z[90] + z[85];
    z[90]=z[12]*z[90];
    z[94]=z[78]*z[9];
    z[94]=z[94] + z[27];
    z[103]= - z[10]*z[78];
    z[103]=z[103] - z[94];
    z[103]=z[16]*z[103];
    z[105]=z[3] - z[96];
    z[105]=z[19]*z[105];
    z[107]=z[10] - z[99];
    z[107]=z[25]*z[107];
    z[90]=z[105] + z[90] + z[103] + z[107] - z[37];
    z[103]=11*z[78];
    z[105]=n<T>(7,2)*z[9];
    z[82]=z[82] + z[105] - z[103] - z[104];
    z[82]=z[5]*z[82];
    z[107]=z[102]*z[104];
    z[79]=z[100] - z[79];
    z[100]= - z[2] + z[79];
    z[100]=z[100]*z[105];
    z[82]=z[82] + z[107] + z[100];
    z[98]=n<T>(11,12)*z[4] - n<T>(11,6)*z[78] + z[98];
    z[98]=z[98]*z[86];
    z[100]= - n<T>(7,4)*z[78] - z[104];
    z[100]= - n<T>(55,18)*z[7] + n<T>(11,24)*z[4] + n<T>(5,3)*z[5] + n<T>(1,3)*z[100]
     + 
   n<T>(1,8)*z[9];
    z[100]=z[7]*z[100];
    z[82]=z[100] + n<T>(1,3)*z[82] + z[98];
    z[82]=z[82]*z[101];
    z[79]=z[83] + z[79];
    z[79]=z[9]*z[79];
    z[83]=z[87] - z[2];
    z[83]=z[2]*z[83];
    z[79]=z[83] + z[79];
    z[83]= - n<T>(7,3)*z[5] + n<T>(1,6)*z[9] + 3*z[78] - n<T>(13,3)*z[2];
    z[83]=z[83]*z[95];
    z[79]=n<T>(1,3)*z[79] + z[83];
    z[79]=z[79]*z[95];
    z[83]= - z[8]*z[78];
    z[83]=z[83] - z[94];
    z[83]=z[15]*z[83];
    z[94]= - z[8] + z[99];
    z[94]=z[23]*z[94];
    z[83]=z[83] + z[94];
    z[94]=z[11] + z[4];
    z[94]=z[78]*z[94];
    z[94]=z[27] + z[94];
    z[94]=z[13]*z[94];
    z[98]= - z[11] + z[106];
    z[98]=z[26]*z[98];
    z[94]=z[94] + z[98];
    z[93]=z[96] - z[93];
    z[98]= - n<T>(5,2)*z[8] - n<T>(7,2)*z[7] + z[93];
    z[98]=z[21]*z[98];
    z[98]=z[98] + z[67];
    z[84]= - z[103] + z[84];
    z[84]=z[84]*npow(z[2],2);
    z[89]=z[102]*z[89];
    z[99]=n<T>(5,12)*z[9] + n<T>(1,4)*z[78] - n<T>(2,3)*z[2];
    z[99]=z[9]*z[99];
    z[89]=z[89] + z[99];
    z[89]=z[9]*z[89];
    z[95]=z[95] - z[97];
    z[86]=n<T>(1,3)*z[95] + z[86];
    z[95]=z[11] - z[7];
    z[86]=5*z[86] - n<T>(3,2)*z[95];
    z[86]=z[18]*z[86];
    z[93]=n<T>(5,2)*z[3] + n<T>(7,2)*z[10] - z[93];
    z[93]=z[24]*z[93];
    z[87]= - z[105] + z[87] - n<T>(19,2)*z[2];
    z[87]=n<T>(1,6)*z[87] + z[3];
    z[87]=z[22]*z[87];
    z[95]=z[7]*z[78];
    z[85]=z[95] + z[85];
    z[85]=z[14]*z[85];
    z[95]= - z[75] + z[74] - z[64] - z[58] + z[44] - z[29];
    z[97]= - z[6] - z[8];
    z[97]=z[40]*z[97];
    z[97]=z[97] - z[72] + z[50] + z[47];
    z[99]=z[49] - z[30];
    z[100]=z[55] - z[52];
    z[101]=z[60] - z[46];
    z[102]=z[69] + z[54];
    z[103]=z[70] + z[36];
    z[104]=z[73] + z[65];
    z[105]= - z[7] - z[11];
    z[105]=z[41]*z[105];
    z[105]=z[105] - z[62];
    z[106]=z[6] + z[3];
    z[106]=z[39]*z[106];
    z[106]=z[106] - z[57];
    z[92]=n<T>(5,3)*z[11] + n<T>(11,24)*z[8] - n<T>(5,4)*z[6] - n<T>(31,6)*z[10]
     - n<T>(11,12)*z[7]
     - z[92]
     + n<T>(107,48)*z[5]
     + n<T>(4,3)*z[2]
     + n<T>(13,16)*z[9];
    z[92]=z[27]*z[92];
    z[92]=z[92] + z[56];
    z[107]= - n<T>(5,12)*z[35] + n<T>(67,24)*z[33] + n<T>(1,4)*z[32] + n<T>(1,9)*z[28];
    z[107]=i*z[107];
    z[78]= - z[4] + 5*z[78] - 4*z[2];
    z[78]=n<T>(1,3)*z[78] + z[3];
    z[78]=z[17]*z[78];
    z[96]= - z[7] + z[96];
    z[96]=z[20]*z[96];
    z[108]= - z[10] - z[11];
    z[108]=z[42]*z[108];

    r += n<T>(2,9)*z[31] - n<T>(15,8)*z[34] + n<T>(431,72)*z[38] - 3*z[43] + n<T>(31,24)
      *z[45] + 4*z[48] + n<T>(25,24)*z[51] + n<T>(59,24)*z[53] - n<T>(49,48)*z[59]
       + n<T>(2,3)*z[61] - n<T>(3,16)*z[63] + n<T>(7,6)*z[66] + n<T>(11,48)*z[68]
     - n<T>(17,48)*z[71]
     + z[76]
     + z[77]
     + z[78]
     + z[79]
     + z[80]
     + z[81]
     + z[82]
       + n<T>(5,6)*z[83] + n<T>(1,12)*z[84] + z[85] + z[86] + z[87] + z[88] + 
      z[89] + n<T>(3,2)*z[90] + z[91] + n<T>(1,3)*z[92] + z[93] + n<T>(7,12)*z[94]
       + n<T>(1,4)*z[95] + z[96] + n<T>(3,4)*z[97] + n<T>(1,6)*z[98] - 2*z[99] + n<T>(1,8)*z[100]
     - n<T>(11,8)*z[101]
     + n<T>(3,8)*z[102] - n<T>(5,12)*z[103]
     + n<T>(5,16)
      *z[104] + n<T>(11,12)*z[105] + n<T>(1,2)*z[106] + z[107] + n<T>(17,12)*z[108]
      ;
 
    return r;
}

template std::complex<double> qqb_2lha_tf594(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf594(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
