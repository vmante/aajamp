#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf762(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[97];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[17];
    z[16]=d[18];
    z[17]=e[0];
    z[18]=e[1];
    z[19]=e[2];
    z[20]=e[4];
    z[21]=e[7];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=e[10];
    z[25]=e[11];
    z[26]=e[12];
    z[27]=c[3];
    z[28]=c[11];
    z[29]=c[15];
    z[30]=c[18];
    z[31]=c[19];
    z[32]=c[21];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[29];
    z[38]=c[31];
    z[39]=e[3];
    z[40]=e[6];
    z[41]=e[13];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[7];
    z[50]=f[10];
    z[51]=f[11];
    z[52]=f[12];
    z[53]=f[14];
    z[54]=f[16];
    z[55]=f[17];
    z[56]=f[18];
    z[57]=f[21];
    z[58]=f[23];
    z[59]=f[30];
    z[60]=f[31];
    z[61]=f[32];
    z[62]=f[33];
    z[63]=f[34];
    z[64]=f[35];
    z[65]=f[36];
    z[66]=f[39];
    z[67]=f[43];
    z[68]=f[51];
    z[69]=f[54];
    z[70]=n<T>(1,2)*z[5];
    z[71]=z[1]*i;
    z[72]=z[70] - z[71];
    z[73]=z[72]*z[5];
    z[74]=z[71] - z[8];
    z[75]=z[5] - z[74];
    z[76]=n<T>(1,2)*z[8];
    z[77]= - z[75]*z[76];
    z[77]= - z[73] + z[77];
    z[77]=z[8]*z[77];
    z[78]=n<T>(1,2)*z[2];
    z[79]=43*z[71] - 5*z[5];
    z[79]= - n<T>(13,3)*z[2] + n<T>(1,3)*z[79] + z[8];
    z[79]=z[79]*z[78];
    z[80]= - 3*z[71] + z[8];
    z[80]=z[8]*z[80];
    z[79]=z[79] + n<T>(11,3)*z[73] + z[80];
    z[79]=z[2]*z[79];
    z[80]=z[71]*z[4];
    z[80]=z[80] + z[27];
    z[81]= - z[11]*z[71];
    z[81]=z[81] - z[80];
    z[81]=z[14]*z[81];
    z[82]=z[71]*z[8];
    z[82]=z[82] + z[27];
    z[83]= - z[7]*z[71];
    z[83]=z[83] - z[82];
    z[83]=z[15]*z[83];
    z[84]=z[9]*z[71];
    z[82]=z[84] + z[82];
    z[82]=z[16]*z[82];
    z[84]=z[71] - z[4];
    z[85]=z[11] - z[84];
    z[85]=z[20]*z[85];
    z[86]= - z[7] + z[74];
    z[86]=z[23]*z[86];
    z[87]= - z[9] + z[74];
    z[87]=z[25]*z[87];
    z[77]=z[81] + z[77] + z[79] + z[87] - z[57] - z[48] + z[30] + z[83]
    + z[82] + z[85] + z[86];
    z[79]=n<T>(1,3)*z[71];
    z[81]=n<T>(1,6)*z[6];
    z[82]= - z[81] - z[78] + z[79] + z[70];
    z[82]=z[6]*z[82];
    z[83]=z[71] - z[5];
    z[85]=n<T>(1,4)*z[8];
    z[86]= - n<T>(1,4)*z[3] - z[81] + n<T>(11,12)*z[2] + n<T>(2,3)*z[83] - z[85];
    z[86]=z[4]*z[86];
    z[87]=z[76] - z[71];
    z[88]=z[5] - z[87];
    z[88]=z[88]*z[76];
    z[89]= - z[78] - 7*z[71] + n<T>(5,2)*z[5];
    z[90]=n<T>(1,3)*z[2];
    z[89]=z[89]*z[90];
    z[91]=2*z[71];
    z[92]=z[91] - z[5];
    z[93]=z[5]*z[92];
    z[94]=n<T>(1,2)*z[71];
    z[95]=z[3]*z[94];
    z[82]=z[86] + z[95] + z[82] + z[89] + n<T>(2,3)*z[93] + z[88];
    z[82]=z[4]*z[82];
    z[86]=n<T>(7,6)*z[7] - n<T>(5,2)*z[6] + z[2] + z[76] + z[71] + z[70];
    z[86]=z[7]*z[86];
    z[70]=z[72]*z[70];
    z[72]=z[78] - z[75];
    z[72]=z[72]*z[78];
    z[88]=n<T>(1,2)*z[11];
    z[89]=z[88] + z[74];
    z[89]=z[89]*z[88];
    z[93]=n<T>(1,2)*z[83];
    z[95]= - z[93] + z[8];
    z[95]=z[8]*z[95];
    z[96]= - z[5] + z[2];
    z[96]=n<T>(1,2)*z[96] - z[6];
    z[96]=z[6]*z[96];
    z[70]=n<T>(1,2)*z[86] + z[89] + z[96] + z[72] + z[70] + z[95];
    z[72]=n<T>(1,3)*z[7];
    z[70]=z[70]*z[72];
    z[86]=z[3] - z[6];
    z[89]= - n<T>(1,3)*z[5] + z[8];
    z[89]=n<T>(1,2)*z[89] + n<T>(7,3)*z[2];
    z[72]= - z[72] + 11*z[9] + n<T>(37,12)*z[11] + n<T>(41,6)*z[4] + n<T>(1,2)*z[89]
    - n<T>(7,3)*z[86];
    z[72]=z[27]*z[72];
    z[86]= - z[10]*z[71];
    z[80]=z[86] - z[80];
    z[80]=z[13]*z[80];
    z[84]=z[10] - z[84];
    z[84]=z[26]*z[84];
    z[86]=z[11] + z[7];
    z[86]=z[40]*z[86];
    z[89]=z[6] + z[10];
    z[89]=z[41]*z[89];
    z[72]=z[89] + z[69] - z[68] + z[67] + z[72] + z[80] + z[84] + z[86]
    - z[50] - z[64] - z[59] + z[58] + z[56];
    z[80]=n<T>(1,3)*z[8];
    z[84]= - z[87]*z[80];
    z[86]=n<T>(1,2)*z[6];
    z[74]= - z[86] - z[74];
    z[74]=z[6]*z[74];
    z[86]=z[11] - z[4] + 5*z[3] - z[86] + z[2] - n<T>(1,6)*z[8] + n<T>(5,3)*
    z[71] - n<T>(3,2)*z[5];
    z[86]=z[11]*z[86];
    z[74]=z[86] + z[74] - 3*z[73] + z[84];
    z[84]=n<T>(1,2)*z[3] + z[83];
    z[84]=z[3]*z[84];
    z[86]=z[4]*z[94];
    z[74]=z[86] + z[84] + n<T>(1,4)*z[74];
    z[74]=z[11]*z[74];
    z[84]=2*z[8];
    z[86]=2*z[2];
    z[89]= - n<T>(5,4)*z[3] - z[86] - n<T>(5,2)*z[83] + z[84];
    z[89]=z[3]*z[89];
    z[94]= - z[94] + 2*z[5];
    z[85]= - z[85] - z[94];
    z[85]=z[8]*z[85];
    z[95]=2*z[83];
    z[96]= - n<T>(1,4)*z[2] - z[95] + z[76];
    z[96]=z[2]*z[96];
    z[85]=z[89] + z[96] - n<T>(31,4)*z[73] + z[85];
    z[88]= - z[88] - z[83];
    z[89]=n<T>(7,4)*z[11];
    z[88]=z[88]*z[89];
    z[96]=13*z[3] - z[2] + n<T>(31,4)*z[83] + z[8];
    z[89]=n<T>(1,3)*z[96] - z[89];
    z[89]=n<T>(1,2)*z[89] - n<T>(1,9)*z[9];
    z[89]=z[9]*z[89];
    z[85]=z[89] + n<T>(1,3)*z[85] + z[88];
    z[85]=z[9]*z[85];
    z[86]=z[86] + z[76] - z[91] - z[5];
    z[86]=z[2]*z[86];
    z[88]=n<T>(7,2)*z[71];
    z[89]= - n<T>(7,4)*z[8] + z[88] - z[5];
    z[89]=z[8]*z[89];
    z[73]= - z[73] + z[89];
    z[73]=n<T>(1,2)*z[73] + z[86];
    z[86]=z[88] + z[5];
    z[76]= - z[90] + n<T>(1,3)*z[86] - z[76];
    z[76]=n<T>(1,4)*z[76] + n<T>(5,9)*z[6];
    z[76]=z[6]*z[76];
    z[73]=n<T>(1,3)*z[73] + z[76];
    z[73]=z[6]*z[73];
    z[76]= - z[95] + z[8];
    z[76]=z[76]*z[80];
    z[84]=n<T>(7,4)*z[2] - z[84] - z[94];
    z[84]=z[84]*z[90];
    z[80]= - n<T>(77,36)*z[3] + n<T>(13,12)*z[2] - z[80] + z[71] + n<T>(1,12)*z[5];
    z[80]=z[3]*z[80];
    z[86]= - n<T>(3,2)*z[71] + n<T>(5,3)*z[5];
    z[86]=z[5]*z[86];
    z[76]=z[80] + z[84] + z[86] + z[76];
    z[76]=z[3]*z[76];
    z[80]= - z[5] - z[3];
    z[80]=z[71]*z[80];
    z[80]= - z[27] + z[80];
    z[80]=z[12]*z[80];
    z[84]= - z[3] + z[83];
    z[84]=z[19]*z[84];
    z[80]=z[80] + z[84];
    z[75]=z[75] - z[2];
    z[84]=n<T>(5,2)*z[7] + n<T>(7,2)*z[6] + z[75];
    z[84]=z[21]*z[84];
    z[75]= - n<T>(31,2)*z[9] - 4*z[75] - n<T>(23,2)*z[3];
    z[75]=z[24]*z[75];
    z[75]=z[84] + z[75];
    z[79]=z[81] + z[79] - z[2];
    z[79]=z[6]*z[79];
    z[81]= - z[2]*z[83];
    z[79]=z[81] + z[79];
    z[81]=n<T>(1,12)*z[4] + z[93] + n<T>(1,3)*z[6];
    z[81]=z[4]*z[81];
    z[79]= - n<T>(1,36)*z[27] + n<T>(1,2)*z[79] + z[81];
    z[79]=z[10]*z[79];
    z[78]= - z[78] - z[87];
    z[78]=z[22]*z[78];
    z[81]= - z[63] + z[61] + z[55] - z[53] + z[51] + z[43];
    z[83]=z[52] - z[37];
    z[84]= - n<T>(29,3)*z[35] - n<T>(43,12)*z[33] + z[32] - n<T>(1,72)*z[28];
    z[84]=i*z[84];
    z[71]= - n<T>(1,2)*z[10] - n<T>(13,6)*z[4] + z[6] - n<T>(5,3)*z[2] + n<T>(29,6)*
    z[71] - z[5];
    z[71]=z[17]*z[71];
    z[86]= - z[2] + z[92];
    z[86]=z[18]*z[86];
    z[87]= - z[3] - z[11];
    z[87]=z[39]*z[87];

    r += n<T>(21,8)*z[29] - n<T>(37,144)*z[31] + n<T>(23,24)*z[34] + n<T>(43,12)*z[36]
       + n<T>(49,16)*z[38] - n<T>(5,2)*z[42] - z[44] - n<T>(2,3)*z[45] - n<T>(35,24)*
      z[46] - n<T>(13,24)*z[47] - n<T>(16,3)*z[49] - n<T>(7,8)*z[54] - n<T>(11,24)*
      z[60] - n<T>(4,3)*z[62] + n<T>(1,24)*z[65] - n<T>(1,8)*z[66] + z[70] + z[71]
       + n<T>(1,6)*z[72] + z[73] + z[74] + n<T>(1,3)*z[75] + z[76] + n<T>(1,2)*
      z[77] + z[78] + z[79] + n<T>(11,6)*z[80] - n<T>(1,4)*z[81] + z[82] + n<T>(3,8)
      *z[83] + z[84] + z[85] + n<T>(5,3)*z[86] + n<T>(3,2)*z[87];
 
    return r;
}

template std::complex<double> qqb_2lha_tf762(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf762(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
