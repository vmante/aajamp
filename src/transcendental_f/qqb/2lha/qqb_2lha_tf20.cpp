#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf20(
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[5];
  std::complex<T> r(0,0);

    z[1]=f[22];
    z[2]=f[42];
    z[3]=f[44];
    z[4]= - 2*z[3] + z[1] - z[2];

    r += 2*z[4];
 
    return r;
}

template std::complex<double> qqb_2lha_tf20(
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf20(
  const std::array<std::complex<dd_real>,111>& f
);
#endif
