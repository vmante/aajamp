#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf83(
  const std::array<std::complex<T>,24>& e
) {
    return e[16];
}

template std::complex<double> qqb_2lha_tf83(
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf83(
  const std::array<std::complex<dd_real>,24>& e
);
#endif
