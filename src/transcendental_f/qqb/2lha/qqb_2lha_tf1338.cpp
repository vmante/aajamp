#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1338(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[103];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[3];
    z[22]=e[4];
    z[23]=e[5];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[10];
    z[28]=e[11];
    z[29]=e[12];
    z[30]=e[13];
    z[31]=c[3];
    z[32]=c[11];
    z[33]=c[15];
    z[34]=c[19];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[31];
    z[40]=e[6];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[7];
    z[50]=f[10];
    z[51]=f[11];
    z[52]=f[12];
    z[53]=f[14];
    z[54]=f[15];
    z[55]=f[16];
    z[56]=f[17];
    z[57]=f[18];
    z[58]=f[23];
    z[59]=f[30];
    z[60]=f[31];
    z[61]=f[32];
    z[62]=f[33];
    z[63]=f[35];
    z[64]=f[36];
    z[65]=f[37];
    z[66]=f[39];
    z[67]=f[43];
    z[68]=f[50];
    z[69]=f[51];
    z[70]=f[52];
    z[71]=f[53];
    z[72]=f[54];
    z[73]=f[55];
    z[74]=f[56];
    z[75]=f[58];
    z[76]=f[74];
    z[77]=f[75];
    z[78]=z[9] - z[3];
    z[79]=z[7] - z[6];
    z[80]=z[1]*i;
    z[81]=2*z[80];
    z[82]=z[81] - z[2];
    z[83]=n<T>(1,2)*z[8];
    z[84]=n<T>(1,2)*z[5];
    z[78]=z[83] + z[84] + z[82] - n<T>(1,48)*z[79] + n<T>(7,24)*z[78];
    z[78]=z[2]*z[78];
    z[79]=n<T>(7,6)*z[3];
    z[85]=z[80] - z[5];
    z[86]=n<T>(1,2)*z[85];
    z[87]= - z[86] - z[3];
    z[87]=z[87]*z[79];
    z[88]=n<T>(1,12)*z[6];
    z[86]=z[86] + z[6];
    z[86]=z[86]*z[88];
    z[89]=z[8] - z[5];
    z[90]= - n<T>(1,24)*z[6] + n<T>(7,12)*z[3] - z[81] + z[89];
    z[90]=z[8]*z[90];
    z[91]=z[80] - z[8];
    z[92]=z[3] - z[5];
    z[93]=z[9] + z[92] + z[91];
    z[93]=z[9]*z[93];
    z[94]= - z[81] + z[5];
    z[94]=z[5]*z[94];
    z[95]= - z[7] - z[6] - z[85] + z[8];
    z[95]=z[7]*z[95];
    z[78]=z[78] + n<T>(1,24)*z[95] + n<T>(7,12)*z[93] + z[90] + z[86] + z[94] + 
    z[87];
    z[78]=z[2]*z[78];
    z[86]=z[84] - z[80];
    z[86]=z[86]*z[5];
    z[87]=n<T>(5,2)*z[80] - z[5] - n<T>(1,4)*z[3];
    z[79]=z[87]*z[79];
    z[87]= - n<T>(119,4)*z[6] + n<T>(115,2)*z[80] + z[5];
    z[87]=z[87]*z[88];
    z[88]= - n<T>(115,24)*z[6] - n<T>(35,12)*z[3] - z[5] - n<T>(7,2)*z[91];
    z[88]=z[88]*z[83];
    z[79]=z[88] + z[87] - z[86] + z[79];
    z[79]=z[8]*z[79];
    z[87]=n<T>(1,2)*z[3];
    z[88]=z[85] + z[87];
    z[88]=z[88]*z[3];
    z[90]=n<T>(1,3)*z[5];
    z[93]= - z[80] - n<T>(7,4)*z[5];
    z[93]=z[93]*z[90];
    z[94]=n<T>(133,36)*z[6] + n<T>(15,8)*z[80] - z[90];
    z[95]=n<T>(1,2)*z[6];
    z[94]=z[94]*z[95];
    z[93]=z[94] + z[93] - n<T>(1,4)*z[88];
    z[93]=z[6]*z[93];
    z[94]=n<T>(113,4)*z[86] + 41*z[88];
    z[96]=7*z[80];
    z[97]=n<T>(7,2)*z[6] + z[96] - n<T>(5,2)*z[92];
    z[97]=z[6]*z[97];
    z[94]=n<T>(1,3)*z[94] + z[97];
    z[87]=z[87] - z[80];
    z[97]=n<T>(1,4)*z[8] + z[84] - z[87];
    z[98]=n<T>(7,3)*z[8];
    z[97]=z[97]*z[98];
    z[99]= - 191*z[80] + 113*z[5];
    z[99]=n<T>(1,2)*z[99] - 31*z[3];
    z[99]= - n<T>(89,36)*z[9] - n<T>(7,6)*z[8] + n<T>(1,24)*z[99] + 3*z[6];
    z[99]=z[9]*z[99];
    z[94]=z[99] + n<T>(1,2)*z[94] + z[97];
    z[94]=z[9]*z[94];
    z[96]= - z[96] + z[90];
    z[96]=z[5]*z[96];
    z[97]= - n<T>(73,9)*z[3] - 9*z[80] + n<T>(43,6)*z[5];
    z[97]=z[3]*z[97];
    z[96]=z[96] + z[97];
    z[96]=z[3]*z[96];
    z[97]=z[85]*npow(z[5],2);
    z[96]=3*z[97] + z[96];
    z[97]= - z[9] + z[80] + z[92];
    z[97]= - n<T>(31,6)*z[11] - n<T>(23,3)*z[6] + n<T>(5,2)*z[97];
    z[97]=z[30]*z[97];
    z[79]=z[97] + z[94] + z[79] + n<T>(1,4)*z[96] + z[93];
    z[81]=z[3] + z[81] - z[84];
    z[81]=z[3]*z[81];
    z[84]=n<T>(1,4)*z[6];
    z[93]=19*z[80];
    z[94]=z[93] - n<T>(11,2)*z[5];
    z[94]= - z[84] + n<T>(1,6)*z[94] + z[3];
    z[94]=z[94]*z[95];
    z[90]= - z[93] - z[90];
    z[90]=n<T>(1,2)*z[90] - n<T>(13,3)*z[6];
    z[90]=n<T>(1,4)*z[90] + z[98];
    z[90]=z[8]*z[90];
    z[93]=n<T>(1,2)*z[7];
    z[96]=z[80] + n<T>(11,8)*z[5];
    z[96]= - n<T>(19,72)*z[7] + n<T>(19,8)*z[8] - n<T>(79,24)*z[6] + n<T>(1,3)*z[96]
     + n<T>(5,4)*z[3];
    z[96]=z[96]*z[93];
    z[81]=z[96] + z[90] + z[94] + n<T>(35,24)*z[86] + z[81];
    z[81]=z[7]*z[81];
    z[88]= - n<T>(39,2)*z[86] - 11*z[88];
    z[90]= - n<T>(49,16)*z[6] - n<T>(49,8)*z[80] + z[92];
    z[90]=z[6]*z[90];
    z[94]=n<T>(17,8)*z[9];
    z[96]= - n<T>(1,2)*z[9] - z[85];
    z[96]=z[96]*z[94];
    z[97]= - n<T>(11,4)*z[7] - n<T>(31,12)*z[8] - z[6] + n<T>(19,12)*z[80] - z[92];
    z[97]=z[7]*z[97];
    z[98]=n<T>(113,3)*z[80] - n<T>(39,2)*z[5];
    z[94]=n<T>(43,12)*z[7] - z[94] - n<T>(109,24)*z[8] - n<T>(65,8)*z[6] + n<T>(1,4)*
    z[98] + 7*z[3];
    z[98]=3*z[2];
    z[94]=n<T>(19,8)*z[10] + n<T>(1,2)*z[94] - z[98];
    z[94]=z[10]*z[94];
    z[99]= - n<T>(109,6)*z[8] + n<T>(109,3)*z[80] + 57*z[6];
    z[99]=z[8]*z[99];
    z[88]=z[94] + z[97] + z[96] + n<T>(1,8)*z[99] + n<T>(1,4)*z[88] + z[90];
    z[90]=n<T>(1,2)*z[10];
    z[88]=z[88]*z[90];
    z[87]=z[3]*z[87];
    z[87]= - z[86] + z[87];
    z[94]= - z[80] + z[95];
    z[95]=n<T>(37,12)*z[6];
    z[94]=z[94]*z[95];
    z[83]=z[83] - z[80];
    z[96]=z[5] - z[83];
    z[97]=3*z[8];
    z[96]=z[96]*z[97];
    z[99]=n<T>(1,4)*z[9];
    z[100]=n<T>(13,2)*z[9] - 13*z[80] - 19*z[6];
    z[100]=z[100]*z[99];
    z[101]=z[93] - z[80] - z[3];
    z[102]=3*z[7];
    z[101]=z[101]*z[102];
    z[87]=z[101] + z[100] + z[96] + 3*z[87] + z[94];
    z[92]=z[102] + n<T>(13,4)*z[9] - z[97] + z[95] + n<T>(55,6)*z[80] + 3*z[92];
    z[92]= - n<T>(55,12)*z[4] + n<T>(1,2)*z[92] + z[98];
    z[94]=n<T>(1,2)*z[4];
    z[92]=z[92]*z[94];
    z[95]= - z[80]*z[98];
    z[90]= - z[80] + z[90];
    z[90]=z[10]*z[90];
    z[87]=z[92] + 3*z[90] + n<T>(1,2)*z[87] + z[95];
    z[87]=z[4]*z[87];
    z[90]=z[6] - z[5];
    z[92]=5*z[3];
    z[90]= - n<T>(11,2)*z[9] + z[92] - z[80] + 5*z[90];
    z[90]=z[90]*z[99];
    z[92]=n<T>(13,6)*z[6] - z[92] - n<T>(47,3)*z[80] + 9*z[5];
    z[84]=z[92]*z[84];
    z[83]=z[3] - z[83];
    z[83]=z[8]*z[83];
    z[92]= - n<T>(3,2)*z[3] - 3*z[80] + n<T>(5,4)*z[5];
    z[92]=z[3]*z[92];
    z[89]= - n<T>(17,6)*z[11] + z[7] + n<T>(15,8)*z[9] + n<T>(5,8)*z[6] - z[3] - n<T>(7,8)*z[89];
    z[89]=z[11]*z[89];
    z[83]=z[89] + z[90] + n<T>(7,4)*z[83] + z[84] - n<T>(1,4)*z[86] + z[92];
    z[84]=z[93] + z[85];
    z[84]=z[7]*z[84];
    z[86]= - n<T>(19,12)*z[4] + n<T>(5,3)*z[6] + n<T>(3,2)*z[9];
    z[86]=z[86]*z[94];
    z[83]=z[86] + z[84] + n<T>(1,2)*z[83];
    z[83]=z[11]*z[83];
    z[84]=z[80]*z[4];
    z[84]=z[84] + z[31];
    z[86]=z[10]*z[80];
    z[86]=z[86] + z[84];
    z[86]=z[14]*z[86];
    z[89]= - z[4] + z[82];
    z[89]=z[18]*z[89];
    z[90]=z[80] - z[4];
    z[92]= - z[10] + z[90];
    z[92]=z[22]*z[92];
    z[86]=z[86] + z[89] + z[92] - z[42];
    z[89]=z[80]*z[5];
    z[89]=z[89] + z[31];
    z[92]=z[6]*z[80];
    z[92]=z[92] + z[89];
    z[92]=z[15]*z[92];
    z[93]= - z[6] + z[85];
    z[93]=z[23]*z[93];
    z[92]=z[68] + z[65] - z[56] + z[53] - z[45] + z[92] + z[93];
    z[93]=z[80]*z[8];
    z[93]=z[93] + z[31];
    z[94]= - z[9]*z[80];
    z[94]=z[94] - z[93];
    z[94]=z[17]*z[94];
    z[95]=z[9] - z[91];
    z[95]=z[28]*z[95];
    z[94]=z[94] + z[95];
    z[95]= - z[7]*z[80];
    z[93]=z[95] - z[93];
    z[93]=z[16]*z[93];
    z[91]= - z[7] + z[91];
    z[91]=z[26]*z[91];
    z[91]=z[93] + z[91];
    z[93]=z[11]*z[80];
    z[84]=z[93] + z[84];
    z[84]=z[13]*z[84];
    z[90]= - z[11] + z[90];
    z[90]=z[29]*z[90];
    z[84]=z[84] + z[90];
    z[80]=z[3]*z[80];
    z[80]=z[80] + z[89];
    z[80]=z[12]*z[80];
    z[89]=z[3] - z[85];
    z[89]=z[20]*z[89];
    z[80]=z[80] + z[89];
    z[89]=z[2] - z[8];
    z[90]=z[10] + n<T>(39,8)*z[7] + n<T>(139,24)*z[6] - n<T>(11,12)*z[85] - z[3]
     + n<T>(1,12)*z[89];
    z[90]=z[24]*z[90];
    z[89]=z[89] + z[85];
    z[89]=n<T>(89,4)*z[9] + n<T>(61,4)*z[3] - 7*z[89];
    z[89]=z[27]*z[89];
    z[93]=z[10] + z[3];
    z[85]=z[9] + z[85] - n<T>(37,8)*z[93];
    z[85]=z[21]*z[85];
    z[93]=z[71] + z[38];
    z[95]= - 10*z[37] - n<T>(37,144)*z[32];
    z[95]=i*z[95];
    z[96]=n<T>(79,2)*z[5] - 53*z[3];
    z[96]=n<T>(67,24)*z[11] + n<T>(67,16)*z[4] + n<T>(515,48)*z[10] - 4*z[2] - n<T>(545,96)*z[7]
     - n<T>(35,4)*z[9]
     - n<T>(157,96)*z[8]
     + n<T>(1,48)*z[96]
     + 2*z[6];
    z[96]=z[31]*z[96];
    z[97]= - z[5] + z[82];
    z[97]=z[19]*z[97];
    z[82]= - z[8] + z[82];
    z[82]=z[25]*z[82];
    z[98]=z[7] + z[10];
    z[98]=z[40]*z[98];
    z[99]= - z[9] - z[11];
    z[99]=z[41]*z[99];
    z[100]=2*i;
    z[100]= - z[35]*z[100];

    r +=  - n<T>(25,12)*z[33] + n<T>(4,3)*z[34] + z[36] + n<T>(13,6)*z[39] + n<T>(3,2)*
      z[43] + z[44] + n<T>(169,96)*z[46] - z[47] + n<T>(21,16)*z[48] + n<T>(14,3)*
      z[49] + n<T>(47,24)*z[50] - n<T>(1,4)*z[51] + n<T>(31,32)*z[52] + n<T>(21,8)*
      z[54] - n<T>(17,32)*z[55] - z[57] - n<T>(7,12)*z[58] - n<T>(31,24)*z[59] - n<T>(37,32)*z[60]
     - n<T>(9,16)*z[61]
     - n<T>(11,3)*z[62]
     + n<T>(9,8)*z[63]
     + n<T>(109,96)
      *z[64] - n<T>(85,32)*z[66] - n<T>(1,24)*z[67] + n<T>(37,48)*z[69] - n<T>(3,16)*
      z[70] - n<T>(5,6)*z[72] - n<T>(13,16)*z[73] - n<T>(7,16)*z[74] + 2*z[75] - n<T>(3,8)*z[76]
     + n<T>(1,8)*z[77]
     + z[78]
     + n<T>(1,2)*z[79]
     + n<T>(19,24)*z[80]
     +  z[81] + z[82] + z[83] + n<T>(19,12)*z[84] + z[85] + 3*z[86] + z[87]
       + z[88] + n<T>(1,6)*z[89] + z[90] + n<T>(55,24)*z[91] + n<T>(3,4)*z[92] + 5*
      z[93] + n<T>(7,4)*z[94] + z[95] + n<T>(1,3)*z[96] + z[97] + n<T>(109,24)*
      z[98] + n<T>(13,4)*z[99] + z[100];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1338(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1338(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
