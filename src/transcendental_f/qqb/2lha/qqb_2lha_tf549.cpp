#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf549(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[91];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[4];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[9];
    z[10]=d[16];
    z[11]=d[8];
    z[12]=d[11];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[4];
    z[18]=e[5];
    z[19]=e[6];
    z[20]=e[7];
    z[21]=e[8];
    z[22]=e[9];
    z[23]=e[12];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[18];
    z[28]=c[19];
    z[29]=c[21];
    z[30]=c[23];
    z[31]=c[25];
    z[32]=c[26];
    z[33]=c[28];
    z[34]=c[29];
    z[35]=c[31];
    z[36]=e[13];
    z[37]=e[14];
    z[38]=f[0];
    z[39]=f[1];
    z[40]=f[3];
    z[41]=f[11];
    z[42]=f[13];
    z[43]=f[17];
    z[44]=f[18];
    z[45]=f[30];
    z[46]=f[31];
    z[47]=f[32];
    z[48]=f[33];
    z[49]=f[35];
    z[50]=f[36];
    z[51]=f[37];
    z[52]=f[38];
    z[53]=f[39];
    z[54]=f[43];
    z[55]=f[48];
    z[56]=f[49];
    z[57]=f[50];
    z[58]=f[51];
    z[59]=f[52];
    z[60]=f[54];
    z[61]=f[55];
    z[62]=f[58];
    z[63]=z[6] + z[3];
    z[64]=z[1]*i;
    z[65]= - z[64] + z[63];
    z[65]=z[6]*z[65];
    z[66]=z[64] - z[3];
    z[67]=n<T>(1,2)*z[2];
    z[68]=z[66]*z[67];
    z[69]=n<T>(1,2)*z[7];
    z[70]= - z[63]*z[69];
    z[71]=n<T>(1,2)*z[4];
    z[72]=z[71] - z[7];
    z[73]=z[64] - z[72];
    z[73]=z[73]*z[71];
    z[74]=z[7] + z[2];
    z[63]= - z[63] + z[74];
    z[63]=z[5]*z[63];
    z[75]=n<T>(1,2)*z[64];
    z[76]=z[75] + n<T>(2,3)*z[3];
    z[76]=z[3]*z[76];
    z[77]=z[64] - z[4];
    z[78]=z[6] + z[77];
    z[78]=z[9]*z[78];
    z[63]=n<T>(1,4)*z[78] + n<T>(1,2)*z[63] + z[73] + z[70] + z[68] + z[76] + n<T>(1,3)*z[65];
    z[63]=z[9]*z[63];
    z[65]= - z[69] + z[64] + z[67];
    z[65]=z[65]*z[69];
    z[68]=n<T>(1,2)*z[3];
    z[70]=z[68] - z[64];
    z[73]=z[70]*z[3];
    z[76]=z[64] - n<T>(3,4)*z[6];
    z[76]=z[6]*z[76];
    z[76]=n<T>(23,6)*z[73] + z[76];
    z[78]=n<T>(1,2)*z[6];
    z[79]=17*z[64] - z[3];
    z[79]=n<T>(1,3)*z[79] - z[78];
    z[79]=n<T>(1,2)*z[79] - n<T>(1,3)*z[2];
    z[79]=z[2]*z[79];
    z[80]=7*z[3];
    z[81]=z[6] - n<T>(11,3)*z[64] - z[80];
    z[81]=n<T>(11,12)*z[8] - z[69] + n<T>(1,2)*z[81] + n<T>(5,3)*z[2];
    z[82]=n<T>(1,2)*z[8];
    z[81]=z[81]*z[82];
    z[83]=3*z[64];
    z[84]=z[6] + z[83] + n<T>(23,3)*z[3];
    z[72]=n<T>(11,6)*z[8] + n<T>(1,2)*z[84] - n<T>(17,3)*z[2] + z[72];
    z[84]=n<T>(1,4)*z[4];
    z[72]=z[72]*z[84];
    z[65]=z[72] + z[81] + z[65] + n<T>(1,2)*z[76] + z[79];
    z[65]=z[4]*z[65];
    z[72]=z[64] - z[8];
    z[76]=3*z[3];
    z[74]=n<T>(29,3)*z[5] - n<T>(29,4)*z[6] - z[76] + n<T>(11,4)*z[72] + 3*z[74];
    z[74]=z[5]*z[74];
    z[79]= - n<T>(21,8)*z[6] - n<T>(29,4)*z[64] + z[3];
    z[79]=z[79]*z[78];
    z[76]= - n<T>(5,2)*z[2] + z[64] + z[76];
    z[81]=n<T>(1,4)*z[2];
    z[76]=z[76]*z[81];
    z[85]=9*z[64] - z[3];
    z[85]= - n<T>(9,4)*z[7] - z[67] + n<T>(1,2)*z[85] + z[6];
    z[85]=z[85]*z[69];
    z[86]=n<T>(1,4)*z[8];
    z[87]= - n<T>(19,4)*z[8] - 7*z[7] + z[2] + n<T>(25,2)*z[6] + n<T>(19,2)*z[64]
     - 
    z[3];
    z[87]=z[87]*z[86];
    z[88]=z[64] - n<T>(1,8)*z[3];
    z[88]=z[3]*z[88];
    z[74]=n<T>(1,4)*z[74] + z[87] + z[85] + z[76] + z[88] + z[79];
    z[74]=z[5]*z[74];
    z[70]=z[70]*z[80];
    z[76]=31*z[6] + n<T>(53,2)*z[64] - z[80];
    z[76]=z[76]*z[78];
    z[76]= - z[70] + z[76];
    z[79]=n<T>(1,4)*z[6];
    z[76]=z[76]*z[79];
    z[85]=npow(z[3],2);
    z[87]=z[64] + n<T>(7,4)*z[3];
    z[87]=z[87]*z[85];
    z[88]= - z[3] - z[9];
    z[88]=z[64]*z[88];
    z[88]= - z[24] + z[88];
    z[88]=z[12]*z[88];
    z[89]=z[9] - z[66];
    z[89]=z[23]*z[89];
    z[76]=z[89] + z[88] + z[87] + z[76];
    z[87]=11*z[64];
    z[80]=z[87] - z[80];
    z[88]=n<T>(1,3)*z[3];
    z[80]=z[80]*z[88];
    z[88]=n<T>(3,2)*z[6];
    z[89]=z[64] + z[88];
    z[89]=z[89]*z[79];
    z[90]= - n<T>(125,2)*z[64] + z[3];
    z[90]=n<T>(97,12)*z[2] + n<T>(1,3)*z[90] + z[78];
    z[81]=z[90]*z[81];
    z[80]=z[81] + z[80] + z[89];
    z[80]=z[2]*z[80];
    z[81]=n<T>(5,4)*z[6] + 2*z[64] - z[68];
    z[81]=z[6]*z[81];
    z[75]= - z[75] + z[3];
    z[75]=n<T>(3,2)*z[75] - z[2];
    z[75]=z[75]*z[67];
    z[89]= - n<T>(7,4)*z[2] + z[64] + z[68];
    z[89]=n<T>(1,2)*z[89] - n<T>(1,3)*z[7];
    z[89]=z[89]*z[69];
    z[75]=z[89] + z[75] - n<T>(1,2)*z[85] + z[81];
    z[75]=z[7]*z[75];
    z[81]= - z[78] - z[66];
    z[81]=z[6]*z[81];
    z[73]= - n<T>(1,3)*z[73] + z[81];
    z[81]=n<T>(1,3)*z[9];
    z[85]=z[66]*z[81];
    z[66]=n<T>(1,3)*z[66] - z[6];
    z[66]=n<T>(1,3)*z[11] + n<T>(1,2)*z[66] + z[81];
    z[81]=n<T>(1,2)*z[11];
    z[66]=z[66]*z[81];
    z[66]=z[66] + n<T>(1,2)*z[73] + z[85];
    z[66]=z[66]*z[81];
    z[68]=n<T>(5,3)*z[4] - z[86] - n<T>(1,4)*z[7] + n<T>(17,12)*z[2] + z[79] - n<T>(10,3)
   *z[64] + z[68];
    z[68]=z[16]*z[68];
    z[73]=z[87] - n<T>(13,2)*z[6];
    z[73]=z[73]*z[78];
    z[78]= - n<T>(29,12)*z[2] + n<T>(73,6)*z[64] - z[6];
    z[78]=z[2]*z[78];
    z[70]=z[78] + z[70] + z[73];
    z[73]=n<T>(23,16)*z[7] + n<T>(3,8)*z[2] - z[88] - z[83] + n<T>(1,4)*z[3];
    z[73]=z[7]*z[73];
    z[78]= - n<T>(11,4)*z[6] - z[64] + n<T>(7,2)*z[3];
    z[78]=n<T>(1,12)*z[8] + n<T>(15,8)*z[7] + n<T>(1,4)*z[78] - n<T>(4,3)*z[2];
    z[78]=z[8]*z[78];
    z[70]=z[78] + n<T>(1,4)*z[70] + z[73];
    z[70]=z[8]*z[70];
    z[73]=z[2] + z[64] + 7*z[6];
    z[73]= - z[84] - z[86] + n<T>(1,4)*z[73] + 2*z[7];
    z[73]=z[20]*z[73];
    z[78]=z[2] + z[3];
    z[78]= - n<T>(43,2)*z[64] + 10*z[78];
    z[69]=z[71] + n<T>(1,3)*z[78] - z[69];
    z[69]=z[15]*z[69];
    z[67]=z[82] - z[64] + z[67];
    z[67]=z[21]*z[67];
    z[71]=z[4] + z[6];
    z[71]= - z[64]*z[71];
    z[71]= - z[24] + z[71];
    z[71]=z[13]*z[71];
    z[77]=z[6] - z[77];
    z[77]=z[18]*z[77];
    z[71]=z[43] + z[44] + z[71] + z[77];
    z[77]=z[8] + z[7];
    z[77]= - z[64]*z[77];
    z[77]= - z[24] + z[77];
    z[77]=z[14]*z[77];
    z[78]= - z[7] + z[72];
    z[78]=z[22]*z[78];
    z[77]= - z[45] + z[77] + z[78];
    z[78]=z[6] + z[9];
    z[78]=z[36]*z[78];
    z[79]=z[9] + z[11];
    z[79]=z[37]*z[79];
    z[78]=z[78] + z[79] + z[60] - z[57];
    z[79]= - z[2] + z[9] + n<T>(5,4)*z[5];
    z[81]= - n<T>(5,4)*z[3] - z[79];
    z[81]=z[64]*z[81];
    z[81]= - n<T>(1,4)*z[24] + z[81];
    z[81]=z[10]*z[81];
    z[64]= - z[64] + 5*z[3];
    z[64]=n<T>(1,4)*z[64] + z[79];
    z[64]=z[17]*z[64];
    z[79]=z[5] + z[7];
    z[72]= - z[6] - z[72] + n<T>(7,2)*z[79];
    z[72]=z[19]*z[72];
    z[79]= - n<T>(17,4)*z[2] + n<T>(7,16)*z[3] + 25*z[6];
    z[79]=n<T>(61,24)*z[4] - n<T>(55,24)*z[8] + n<T>(1,3)*z[79] - n<T>(19,8)*z[7];
    z[79]=n<T>(1,9)*z[11] + n<T>(13,18)*z[9] + n<T>(1,3)*z[79] + n<T>(5,2)*z[5];
    z[79]=z[24]*z[79];
    z[79]=z[27] + z[47] + z[79] - z[54];
    z[82]=z[55] + z[62] + z[56];
    z[83]=z[51] + z[39];
    z[84]=n<T>(331,24)*z[32] + n<T>(259,48)*z[30] - n<T>(11,8)*z[29] - n<T>(11,144)*
    z[25];
    z[84]=i*z[84];

    r += n<T>(43,12)*z[26] - n<T>(11,144)*z[28] - n<T>(33,16)*z[31] - n<T>(149,24)*
      z[33] - n<T>(3,8)*z[34] - n<T>(347,72)*z[35] + n<T>(9,4)*z[38] + n<T>(35,24)*
      z[40] + n<T>(11,24)*z[41] + n<T>(77,48)*z[42] - n<T>(3,16)*z[46] + z[48] + n<T>(3,2)*z[49]
     + n<T>(15,16)*z[50]
     + n<T>(15,8)*z[52] - n<T>(25,16)*z[53] - n<T>(7,24)*
      z[58] + n<T>(1,4)*z[59] + n<T>(1,24)*z[61] + z[63] + z[64] + z[65] + 
      z[66] + n<T>(29,12)*z[67] + z[68] + z[69] + z[70] + n<T>(3,4)*z[71] + 
      z[72] + z[73] + z[74] + z[75] + n<T>(1,3)*z[76] + n<T>(9,8)*z[77] + n<T>(1,6)
      *z[78] + n<T>(1,2)*z[79] + z[80] + z[81] - n<T>(1,8)*z[82] - n<T>(5,8)*z[83]
       + z[84];
 
    return r;
}

template std::complex<double> qqb_2lha_tf549(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf549(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
