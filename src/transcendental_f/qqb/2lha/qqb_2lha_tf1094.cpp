#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1094(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[90];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[12];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[16];
    z[14]=d[17];
    z[15]=d[18];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[5];
    z[21]=e[7];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=e[10];
    z[25]=e[11];
    z[26]=c[3];
    z[27]=c[11];
    z[28]=c[15];
    z[29]=c[17];
    z[30]=c[19];
    z[31]=c[23];
    z[32]=c[25];
    z[33]=c[26];
    z[34]=c[28];
    z[35]=c[31];
    z[36]=e[6];
    z[37]=f[0];
    z[38]=f[1];
    z[39]=f[2];
    z[40]=f[3];
    z[41]=f[4];
    z[42]=f[5];
    z[43]=f[6];
    z[44]=f[7];
    z[45]=f[9];
    z[46]=f[11];
    z[47]=f[13];
    z[48]=f[14];
    z[49]=f[16];
    z[50]=f[17];
    z[51]=f[18];
    z[52]=f[21];
    z[53]=f[23];
    z[54]=f[30];
    z[55]=f[31];
    z[56]=f[32];
    z[57]=f[33];
    z[58]=f[35];
    z[59]=f[36];
    z[60]=f[39];
    z[61]=f[43];
    z[62]=f[68];
    z[63]=z[1]*i;
    z[64]=z[63] - z[8];
    z[65]=n<T>(11,4)*z[6];
    z[66]=n<T>(11,6)*z[7];
    z[67]=z[3] - z[2];
    z[68]=n<T>(5,2)*z[11] + z[66] - z[65] + n<T>(11,12)*z[64] - z[67];
    z[69]=n<T>(1,2)*z[11];
    z[68]=z[68]*z[69];
    z[70]=n<T>(1,2)*z[8];
    z[71]=z[70] - z[63];
    z[72]=z[8]*z[71];
    z[73]=n<T>(1,2)*z[3];
    z[74]=z[63] - z[5];
    z[75]= - z[73] - z[74];
    z[75]=z[3]*z[75];
    z[76]= - n<T>(1,2)*z[6] - z[64];
    z[65]=z[76]*z[65];
    z[66]=z[64]*z[66];
    z[65]=z[68] + z[66] + z[65] - n<T>(11,12)*z[72] + z[75];
    z[65]=z[11]*z[65];
    z[66]=n<T>(1,2)*z[5];
    z[68]=2*z[63];
    z[72]= - z[68] + z[66];
    z[72]=z[5]*z[72];
    z[71]= - z[5] + z[71];
    z[71]=z[8]*z[71];
    z[75]=2*z[5];
    z[76]=5*z[63];
    z[77]= - z[2] + z[76] + z[75];
    z[77]=z[2]*z[77];
    z[78]=z[63] - z[69];
    z[78]=z[11]*z[78];
    z[79]=z[63] + z[8];
    z[73]= - n<T>(1,6)*z[4] - z[73] - n<T>(5,2)*z[2] + z[5] + n<T>(3,2)*z[79];
    z[73]=z[4]*z[73];
    z[79]=z[63]*z[3];
    z[71]=z[73] + z[78] + z[79] + z[77] + z[72] + 3*z[71];
    z[71]=z[4]*z[71];
    z[66]= - z[63] + z[66];
    z[72]=7*z[5];
    z[66]=z[66]*z[72];
    z[73]=4*z[5];
    z[77]=n<T>(19,2)*z[8] - n<T>(11,2)*z[63] + z[73];
    z[77]=z[8]*z[77];
    z[78]=z[8] + z[5];
    z[80]=z[78] - z[63];
    z[81]= - 2*z[80] + z[2];
    z[81]=z[2]*z[81];
    z[72]=n<T>(11,2)*z[8] + n<T>(13,2)*z[63] + z[72];
    z[72]=n<T>(19,6)*z[7] - n<T>(43,4)*z[6] + n<T>(1,2)*z[72] + 4*z[2];
    z[72]=z[7]*z[72];
    z[66]=z[72] + 2*z[81] + z[66] + z[77];
    z[72]= - n<T>(23,12)*z[6] + n<T>(4,3)*z[2] + z[63] - n<T>(7,3)*z[5];
    z[72]=z[6]*z[72];
    z[66]=z[72] + n<T>(1,3)*z[66];
    z[66]=z[7]*z[66];
    z[72]=n<T>(1,2)*z[2];
    z[77]=n<T>(2,3)*z[9] + z[69] - n<T>(13,6)*z[3] + z[72] - n<T>(5,3)*z[74] - z[70]
   ;
    z[77]=z[9]*z[77];
    z[69]=z[69] + z[74];
    z[69]=z[11]*z[69];
    z[81]=z[68] - z[5];
    z[82]= - n<T>(5,3)*z[81] + z[8];
    z[82]=z[5]*z[82];
    z[83]=z[2]*z[74];
    z[84]=n<T>(7,6)*z[3] + z[2] + n<T>(7,3)*z[74] - z[8];
    z[84]=z[3]*z[84];
    z[69]=z[77] + z[69] + z[84] + z[83] + z[82];
    z[69]=z[9]*z[69];
    z[77]= - z[70] + z[74];
    z[77]=z[8]*z[77];
    z[78]= - z[68] + z[78];
    z[78]=z[2]*z[78];
    z[75]= - 7*z[63] + z[75];
    z[75]=z[3] - n<T>(3,2)*z[2] + n<T>(1,3)*z[75] + z[70];
    z[75]=z[3]*z[75];
    z[82]= - z[63] - 5*z[5];
    z[82]=z[5]*z[82];
    z[75]=z[75] + z[78] + n<T>(1,3)*z[82] + z[77];
    z[75]=z[3]*z[75];
    z[77]=4*z[63];
    z[78]=z[77] - n<T>(3,2)*z[5];
    z[78]=z[5]*z[78];
    z[82]= - n<T>(5,2)*z[8] + 6*z[63] + z[5];
    z[82]=z[8]*z[82];
    z[83]= - z[76] - z[5];
    z[83]=n<T>(11,3)*z[2] + n<T>(3,2)*z[83] - z[8];
    z[83]=z[2]*z[83];
    z[78]=z[83] + z[78] + z[82];
    z[78]=z[2]*z[78];
    z[73]= - n<T>(65,8)*z[8] + n<T>(65,4)*z[63] - z[73];
    z[73]=z[8]*z[73];
    z[72]= - z[72] + 4*z[8] - z[77] + z[5];
    z[72]=z[2]*z[72];
    z[76]= - z[76] + z[5];
    z[76]=z[5]*z[76];
    z[72]=z[72] + z[76] + z[73];
    z[73]=z[2] - z[5];
    z[73]=n<T>(145,36)*z[6] + n<T>(11,8)*z[64] - n<T>(8,3)*z[73];
    z[73]=z[6]*z[73];
    z[72]=n<T>(1,3)*z[72] + z[73];
    z[72]=z[6]*z[72];
    z[73]=z[80] - z[2];
    z[76]=2*z[73] + 7*z[6];
    z[76]=4*z[76] + n<T>(43,2)*z[7];
    z[76]=z[21]*z[76];
    z[77]=z[3] - 7*z[2] + n<T>(25,3)*z[5] - n<T>(17,4)*z[8];
    z[77]=5*z[4] - n<T>(23,3)*z[9] + n<T>(19,12)*z[11] - n<T>(10,3)*z[7] + n<T>(1,2)*
    z[77] + 11*z[6];
    z[77]=z[26]*z[77];
    z[76]=z[51] + z[62] + z[77] + z[76];
    z[77]=z[68] - z[2];
    z[80]=z[4] - z[77];
    z[80]=z[16]*z[80];
    z[82]=z[8] - z[77];
    z[82]=z[22]*z[82];
    z[80]=z[80] + z[82];
    z[82]=z[63]*z[8];
    z[82]=z[82] + z[26];
    z[83]= - z[7]*z[63];
    z[83]=z[83] - z[82];
    z[83]=z[14]*z[83];
    z[84]= - z[7] + z[64];
    z[84]=z[23]*z[84];
    z[83]=z[83] + z[84];
    z[84]=z[5]*z[63];
    z[79]=z[26] + z[84] + z[79];
    z[79]=z[12]*z[79];
    z[74]=z[3] - z[74];
    z[74]=z[18]*z[74];
    z[74]=z[41] + z[79] + z[74];
    z[70]=z[70] - n<T>(1,2)*z[63] + z[5];
    z[70]=z[8]*z[70];
    z[79]= - z[5]*z[81];
    z[70]=z[79] + z[70];
    z[70]=z[8]*z[70];
    z[73]=n<T>(10,3)*z[9] + n<T>(7,3)*z[3] + z[73];
    z[73]=z[24]*z[73];
    z[67]=z[67] + 2*z[6] + 3*z[5];
    z[79]=z[63]*z[67];
    z[79]=2*z[26] + z[79];
    z[79]=z[10]*z[79];
    z[67]=z[68] - z[67];
    z[67]=z[20]*z[67];
    z[68]=z[4] + z[11];
    z[81]= - z[63]*z[68];
    z[81]= - z[26] + z[81];
    z[81]=z[13]*z[81];
    z[84]= - z[9]*z[63];
    z[82]=z[84] - z[82];
    z[82]=z[15]*z[82];
    z[68]= - z[63] + z[68];
    z[68]=z[19]*z[68];
    z[84]= - z[49] - z[48] + z[45] + z[38];
    z[85]=z[50] + z[43];
    z[86]=z[56] - z[47];
    z[87]=z[61] - z[58];
    z[88]=z[7] + z[11];
    z[88]=z[36]*z[88];
    z[88]=z[88] - z[54];
    z[89]=14*z[33] + n<T>(15,2)*z[31] + n<T>(1,2)*z[29] - n<T>(29,72)*z[27];
    z[89]=i*z[89];
    z[63]=z[63] - n<T>(4,3)*z[5];
    z[63]=z[63]*npow(z[5],2);
    z[77]=z[5] - z[77];
    z[77]=z[17]*z[77];
    z[64]=z[9] - z[64];
    z[64]=z[25]*z[64];

    r += n<T>(19,12)*z[28] + n<T>(17,12)*z[30] - n<T>(11,3)*z[32] - 7*z[34] + n<T>(5,8)
      *z[35] + z[37] + z[39] + z[40] + z[42] + 8*z[44] + z[46] - z[52]
       - z[53] - n<T>(97,24)*z[55] - n<T>(29,3)*z[57] + n<T>(11,24)*z[59] - n<T>(11,8)*
      z[60] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + 
      z[70] + z[71] + z[72] + 2*z[73] + n<T>(8,3)*z[74] + z[75] + n<T>(1,3)*
      z[76] + z[77] + z[78] + z[79] + 3*z[80] + z[81] + z[82] + n<T>(9,2)*
      z[83] - n<T>(1,2)*z[84] + n<T>(3,2)*z[85] - n<T>(5,2)*z[86] + n<T>(4,3)*z[87]
     +  n<T>(11,6)*z[88] + z[89];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1094(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1094(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
