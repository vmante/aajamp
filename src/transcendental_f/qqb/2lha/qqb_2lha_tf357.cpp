#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf357(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[86];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[1];
    z[8]=d[4];
    z[9]=d[8];
    z[10]=d[15];
    z[11]=d[7];
    z[12]=d[16];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[2];
    z[17]=e[4];
    z[18]=e[5];
    z[19]=e[7];
    z[20]=e[9];
    z[21]=c[3];
    z[22]=c[11];
    z[23]=c[15];
    z[24]=c[19];
    z[25]=c[21];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=c[26];
    z[29]=c[28];
    z[30]=c[31];
    z[31]=e[1];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=e[6];
    z[35]=e[13];
    z[36]=f[0];
    z[37]=f[1];
    z[38]=f[2];
    z[39]=f[3];
    z[40]=f[4];
    z[41]=f[5];
    z[42]=f[10];
    z[43]=f[11];
    z[44]=f[12];
    z[45]=f[13];
    z[46]=f[14];
    z[47]=f[16];
    z[48]=f[17];
    z[49]=f[18];
    z[50]=f[21];
    z[51]=f[30];
    z[52]=f[31];
    z[53]=f[32];
    z[54]=f[33];
    z[55]=f[34];
    z[56]=f[35];
    z[57]=f[36];
    z[58]=f[39];
    z[59]=f[43];
    z[60]=2*z[11];
    z[61]=n<T>(1,2)*z[7];
    z[62]=z[1]*i;
    z[63]=3*z[62];
    z[64]=5*z[2] - z[4] + z[63];
    z[64]= - n<T>(1,6)*z[3] - z[61] + n<T>(1,2)*z[64] - z[60];
    z[64]=z[3]*z[64];
    z[65]=n<T>(1,2)*z[8];
    z[66]=z[65] - z[62];
    z[66]=z[66]*z[8];
    z[67]=z[62]*z[4];
    z[68]=npow(z[4],2);
    z[69]= - z[67] + n<T>(1,2)*z[68];
    z[70]=z[4] + z[62];
    z[70]=2*z[70] - z[11];
    z[60]=z[70]*z[60];
    z[70]=z[62]*z[7];
    z[71]=n<T>(3,2)*z[2] - 3*z[4] - 5*z[62];
    z[71]=z[2]*z[71];
    z[60]=z[64] + z[70] + z[60] + z[71] - z[66] - z[69];
    z[60]=z[3]*z[60];
    z[64]=n<T>(7,3)*z[2];
    z[63]=z[64] - 7*z[4] - z[63];
    z[71]=n<T>(1,2)*z[2];
    z[63]=z[63]*z[71];
    z[72]=npow(z[8],2);
    z[63]=z[63] + n<T>(1,2)*z[72] + n<T>(3,2)*z[68] + 7*z[67];
    z[63]=z[2]*z[63];
    z[72]=z[68] - 4*z[67];
    z[73]=z[62] - z[4];
    z[74]=2*z[73] + n<T>(7,2)*z[8];
    z[74]=z[8]*z[74];
    z[72]=2*z[72] + z[74];
    z[74]=n<T>(1,2)*z[62];
    z[75]=z[8] + z[4] - z[74];
    z[71]= - n<T>(5,3)*z[7] + n<T>(1,3)*z[75] + z[71];
    z[71]=z[7]*z[71];
    z[75]=2*z[4];
    z[76]=z[75] - z[2];
    z[76]=z[2]*z[76];
    z[77]=z[2] - z[4];
    z[78]=z[5]*z[77];
    z[71]=z[71] + z[78] + n<T>(1,3)*z[72] + z[76];
    z[71]=z[7]*z[71];
    z[72]= - z[65] - z[73];
    z[76]=3*z[8];
    z[72]=z[72]*z[76];
    z[72]=z[72] - z[69];
    z[61]=z[61] + z[73];
    z[61]=z[7]*z[61];
    z[76]= - z[76] + z[73];
    z[76]=n<T>(1,4)*z[76] + z[7];
    z[76]=z[9]*z[76];
    z[61]=z[76] + n<T>(1,2)*z[72] + z[61];
    z[61]=z[9]*z[61];
    z[72]=z[65] + z[62];
    z[76]= - n<T>(5,4)*z[4] + z[72];
    z[76]=z[8]*z[76];
    z[76]= - n<T>(5,2)*z[69] + z[76];
    z[76]=z[8]*z[76];
    z[78]= - z[68]*z[62];
    z[79]=npow(z[4],3);
    z[78]=n<T>(5,3)*z[79] + z[78];
    z[79]=z[3] + z[8];
    z[80]= - z[62]*z[79];
    z[80]= - z[21] + z[80];
    z[80]=z[12]*z[80];
    z[67]=z[67] + z[21];
    z[81]= - z[5]*z[62];
    z[81]=z[81] - z[67];
    z[81]=z[13]*z[81];
    z[82]=z[11] + z[6];
    z[83]=z[62]*z[82];
    z[83]=z[21] + z[83];
    z[83]=z[14]*z[83];
    z[79]= - z[62] + z[79];
    z[79]=z[17]*z[79];
    z[82]= - z[62] + z[82];
    z[82]=z[20]*z[82];
    z[84]=z[5] - z[73];
    z[84]=z[18]*z[84];
    z[85]= - z[7] - z[9];
    z[85]=z[33]*z[85];
    z[60]= - z[81] - z[83] - z[79] - z[84] - z[60] - z[71] - z[63] - n<T>(1,2)*z[78]
     - n<T>(1,3)*z[76]
     - z[61]
     - z[80]
     - z[36]
     - z[82]
     - z[85]
     + 
    z[50] - z[42];
    z[61]= - n<T>(17,9)*z[5] + z[64] + z[65] - n<T>(7,3)*z[4] - z[74];
    z[61]=z[5]*z[61];
    z[63]=z[72]*z[8];
    z[61]=z[63] + z[61];
    z[64]=n<T>(1,33)*z[4];
    z[65]= - z[62]*z[64];
    z[71]=n<T>(1,33)*z[2];
    z[72]=z[62] - z[2];
    z[72]=5*z[4] + 4*z[72];
    z[72]=z[72]*z[71];
    z[64]=z[64] - 1;
    z[74]= - z[4]*z[64];
    z[61]=z[72] + z[74] + z[65] + n<T>(1,22)*z[61];
    z[61]=z[5]*z[61];
    z[65]=2*z[5] - z[77];
    z[65]=z[5]*z[65];
    z[72]=z[62] + z[2];
    z[74]= - n<T>(7,6)*z[6] + n<T>(5,2)*z[5] - n<T>(1,2)*z[4] - z[72];
    z[74]=z[6]*z[74];
    z[63]=z[74] + z[65] - z[63] - z[69];
    z[65]= - z[64] + n<T>(1,33)*z[62];
    z[69]= - n<T>(1,66)*z[2] - z[65];
    z[69]=z[2]*z[69];
    z[63]=z[69] + n<T>(1,33)*z[63];
    z[63]=z[6]*z[63];
    z[69]= - z[62]*z[75];
    z[68]= - z[68] + z[69];
    z[69]= - n<T>(2,3)*z[6] + n<T>(7,12)*z[5] + z[75] + n<T>(1,12)*z[8];
    z[69]=z[11]*z[69];
    z[66]=z[69] + 2*z[68] + n<T>(1,6)*z[66];
    z[64]=n<T>(1,44)*z[5] - z[71] - n<T>(7,66)*z[62] + z[64] - n<T>(1,22)*z[8];
    z[64]=z[5]*z[64];
    z[65]= - n<T>(1,66)*z[6] + z[71] + n<T>(1,33)*z[8] + z[65];
    z[65]=z[6]*z[65];
    z[64]=z[65] + z[64] + n<T>(1,11)*z[66];
    z[64]=z[11]*z[64];
    z[65]=43*z[4] + 13*z[8];
    z[65]= - 5*z[5] + n<T>(1,6)*z[65] - z[2];
    z[65]=z[9] + n<T>(13,2)*z[3] - z[7] - n<T>(7,4)*z[11] + n<T>(1,2)*z[65] + n<T>(1,3)*
    z[6];
    z[65]=z[21]*z[65];
    z[66]= - z[8] - z[6];
    z[66]=z[34]*z[66];
    z[65]= - z[59] + z[56] + z[51] + z[49] - z[24] + z[65] + z[66];
    z[66]= - z[70] - z[67];
    z[66]=z[10]*z[66];
    z[67]= - z[7] + z[73];
    z[67]=z[16]*z[67];
    z[66]= - z[38] + z[66] + z[67];
    z[67]= - z[37] + z[55] + z[53] + z[46] - z[39];
    z[62]= - z[3] + 2*z[62] - z[2];
    z[62]=z[15]*z[62];
    z[62]=z[62] - z[48] + z[43] + z[29];
    z[68]=z[45] - z[41];
    z[69]=z[58] - z[40];
    z[70]= - z[8] - z[7];
    z[70]=z[32]*z[70];
    z[70]=z[70] + z[23];
    z[71]= - n<T>(6,11)*z[28] - n<T>(7,22)*z[26] + n<T>(2,11)*z[25] + n<T>(25,396)*z[22]
   ;
    z[71]=i*z[71];
    z[72]= - z[4] + z[72] - z[11];
    z[72]= - n<T>(5,33)*z[6] - n<T>(7,33)*z[5] + 1 + n<T>(2,33)*z[72];
    z[72]=z[19]*z[72];
    z[73]=z[31]*z[77];

    r += n<T>(5,66)*z[27] - n<T>(41,33)*z[30] - z[35] + n<T>(5,132)*z[44] - n<T>(3,44)*
      z[47] + n<T>(1,12)*z[52] + n<T>(8,33)*z[54] - n<T>(1,132)*z[57] - n<T>(1,11)*
      z[60] + z[61] + n<T>(2,11)*z[62] + z[63] + z[64] + n<T>(1,33)*z[65] + n<T>(2,33)*z[66]
     + n<T>(1,22)*z[67] - n<T>(3,11)*z[68]
     + n<T>(1,44)*z[69]
     + n<T>(5,33)*
      z[70] + z[71] + z[72] + n<T>(4,11)*z[73];
 
    return r;
}

template std::complex<double> qqb_2lha_tf357(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf357(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
