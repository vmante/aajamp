#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1276(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[109];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[3];
    z[6]=d[7];
    z[7]=d[9];
    z[8]=e[7];
    z[9]=d[2];
    z[10]=d[8];
    z[11]=d[11];
    z[12]=e[12];
    z[13]=d[12];
    z[14]=d[4];
    z[15]=d[17];
    z[16]=e[5];
    z[17]=e[9];
    z[18]=e[13];
    z[19]=e[6];
    z[20]=e[14];
    z[21]=f[30];
    z[22]=f[31];
    z[23]=f[32];
    z[24]=f[33];
    z[25]=f[35];
    z[26]=f[36];
    z[27]=f[39];
    z[28]=f[43];
    z[29]=f[50];
    z[30]=f[51];
    z[31]=f[52];
    z[32]=f[55];
    z[33]=f[58];
    z[34]=f[68];
    z[35]=c[3];
    z[36]=c[11];
    z[37]=c[15];
    z[38]=c[17];
    z[39]=c[31];
    z[40]=c[32];
    z[41]=c[39];
    z[42]=c[43];
    z[43]=c[45];
    z[44]=c[46];
    z[45]=c[78];
    z[46]=c[81];
    z[47]=f[53];
    z[48]=g[66];
    z[49]=g[67];
    z[50]=g[84];
    z[51]=g[116];
    z[52]=g[117];
    z[53]=g[133];
    z[54]=g[159];
    z[55]=g[160];
    z[56]=g[162];
    z[57]=g[163];
    z[58]=g[165];
    z[59]=g[167];
    z[60]=g[174];
    z[61]=g[175];
    z[62]=g[178];
    z[63]=g[184];
    z[64]=g[185];
    z[65]=g[186];
    z[66]=g[195];
    z[67]=g[209];
    z[68]=g[221];
    z[69]=z[1]*i;
    z[70]=z[69] - z[5];
    z[71]= - 77*z[7] + n<T>(1865,34)*z[70];
    z[72]=z[4] - z[3];
    z[71]=z[72]*z[71];
    z[73]=npow(z[3],2);
    z[74]= - n<T>(34223,6)*z[3] + 2399*z[4];
    z[74]=z[4]*z[74];
    z[74]=n<T>(19829,6)*z[73] + z[74];
    z[71]=n<T>(1,17)*z[74] + z[71];
    z[71]=z[2]*z[71];
    z[74]=4798*z[4];
    z[75]= - n<T>(5435,3)*z[3] + z[74];
    z[75]=z[4]*z[75];
    z[75]= - n<T>(48617,3)*z[73] + z[75];
    z[75]=z[4]*z[75];
    z[76]=npow(z[3],3);
    z[75]=n<T>(39658,3)*z[76] + z[75];
    z[77]= - z[3] - z[4];
    z[77]=z[4]*z[77];
    z[77]=2*z[73] + z[77];
    z[78]=154*z[7];
    z[77]=z[77]*z[78];
    z[71]=z[71] + n<T>(1,17)*z[75] + z[77];
    z[75]= - n<T>(2221,17)*z[4] + n<T>(1684,9)*z[3];
    z[75]=z[75]*z[4];
    z[75]=z[75] - n<T>(8639,153)*z[73];
    z[77]= - i*z[75];
    z[79]=z[7]*i;
    z[80]=z[72]*z[79];
    z[77]=z[77] - n<T>(154,3)*z[80];
    z[77]=z[1]*z[77];
    z[80]=n<T>(1865,51)*z[5] - n<T>(3730,51)*z[69] + n<T>(154,3)*z[7];
    z[80]=z[72]*z[80];
    z[75]=z[80] + z[75];
    z[75]=z[5]*z[75];
    z[78]=z[78] + n<T>(1865,17)*z[5];
    z[80]=n<T>(1865,17)*z[69] - z[78];
    z[81]= - z[72]*z[80];
    z[82]=n<T>(34223,3)*z[3] - z[74];
    z[82]=z[4]*z[82];
    z[82]= - n<T>(19829,3)*z[73] + z[82];
    z[81]=n<T>(1,17)*z[82] + z[81];
    z[82]=n<T>(1,3)*z[6];
    z[81]=z[81]*z[82];
    z[83]=z[35]*z[72];
    z[71]= - n<T>(1865,51)*z[83] + z[81] + z[75] + z[77] + n<T>(1,3)*z[71];
    z[71]=z[2]*z[71];
    z[75]= - n<T>(66353,6)*z[3] + 8711*z[4];
    z[75]=z[4]*z[75];
    z[77]= - 22364*z[3] - 15751*z[4];
    z[77]=z[7]*z[77];
    z[75]=z[77] - n<T>(28238,3)*z[73] + z[75];
    z[74]= - z[74] + n<T>(19829,3)*z[3];
    z[74]= - z[80] + n<T>(1,17)*z[74];
    z[77]=z[2] - z[6];
    z[77]=n<T>(2,3)*z[77];
    z[77]=z[74]*z[77];
    z[80]=n<T>(311,2)*z[4] + n<T>(87100,9)*z[3];
    z[80]=n<T>(1,17)*z[80];
    z[81]=i*z[80];
    z[81]=z[81] + n<T>(308,3)*z[79];
    z[81]=z[1]*z[81];
    z[80]= - n<T>(3730,51)*z[5] + n<T>(7460,51)*z[69] - z[80] - n<T>(308,3)*z[7];
    z[80]=z[5]*z[80];
    z[75]=n<T>(7691,51)*z[8] + n<T>(4054,51)*z[35] + z[80] + n<T>(1,51)*z[75] + 
    z[81] + z[77];
    z[75]=z[8]*z[75];
    z[71]= - z[44] + z[71] + z[75];
    z[75]=41*z[5];
    z[77]=z[75]*z[69];
    z[80]=n<T>(22991,9)*z[79];
    z[81]=1765*z[4];
    z[83]=z[81] + n<T>(3413,9)*z[3];
    z[84]=z[83]*i;
    z[85]=z[80] + z[84];
    z[86]=n<T>(1,3)*z[1];
    z[87]= - z[85]*z[86];
    z[87]=z[87] - z[77];
    z[87]=z[9]*z[87];
    z[88]=41*z[9];
    z[89]= - n<T>(24098,9)*z[7] - z[83];
    z[89]= - z[88] + n<T>(1,3)*z[89] - z[75];
    z[89]=z[35]*z[89];
    z[90]=n<T>(1,3)*z[7];
    z[85]= - z[1]*z[85]*z[90];
    z[77]= - z[7]*z[77];
    z[77]=z[89] + z[87] + z[85] + z[77];
    z[77]=z[11]*z[77];
    z[85]= - z[75] + 41*z[69];
    z[87]=n<T>(22991,9)*z[7];
    z[83]=z[83] + z[87];
    z[89]= - z[85] + n<T>(1,3)*z[83];
    z[91]=z[89]*z[9];
    z[92]=z[7]*z[83];
    z[93]=n<T>(24098,9)*z[79];
    z[84]= - z[84] - z[93];
    z[84]=z[1]*z[84];
    z[84]=z[92] + z[84];
    z[92]=z[7] - z[69];
    z[92]=z[92]*z[75];
    z[84]= - 41*z[35] + z[91] + n<T>(1,3)*z[84] + z[92];
    z[84]=z[12]*z[84];
    z[92]=z[89]*z[10];
    z[85]= - z[7]*z[85];
    z[83]=z[83]*z[90];
    z[83]=z[92] + z[83] + z[85];
    z[83]=z[20]*z[83];
    z[77]=z[83] + z[77] + z[84];
    z[83]=n<T>(3397,3)*z[4];
    z[84]= - z[83] + 523*z[3];
    z[85]=n<T>(1,3)*z[4];
    z[94]=z[84]*z[85];
    z[95]=n<T>(1,2)*z[3];
    z[96]=z[95] - z[85];
    z[97]=4847*z[7];
    z[98]=z[96]*z[97];
    z[94]= - z[98] + z[94] + n<T>(2351,4)*z[73];
    z[98]=n<T>(4613,6)*z[5];
    z[99]=z[98] + z[97];
    z[100]=z[83] + n<T>(2351,2)*z[3];
    z[101]= - z[100] + z[99] - n<T>(4613,6)*z[69];
    z[102]=z[6]*z[101];
    z[103]=1741*z[3] - n<T>(608,3)*z[4];
    z[103]=i*z[103];
    z[103]=z[103] - n<T>(4847,2)*z[79];
    z[103]=z[103]*z[86];
    z[104]=n<T>(1,6)*z[69];
    z[105]= - z[104] - z[96];
    z[105]=z[105]*z[98];
    z[102]=n<T>(1,12)*z[102] + z[105] + z[103] + z[94];
    z[102]=z[6]*z[102];
    z[103]= - n<T>(1,3)*z[14] + n<T>(1,6)*z[6];
    z[103]=z[101]*z[103];
    z[105]= - n<T>(377,2)*z[3] + 445*z[4];
    z[105]=i*z[105];
    z[105]=z[105] - n<T>(4847,6)*z[79];
    z[105]=z[1]*z[105];
    z[106]= - z[104] + z[96];
    z[106]=z[106]*z[98];
    z[94]=z[106] + z[105] + z[103] - z[94];
    z[94]=z[14]*z[94];
    z[84]= - z[4]*z[84];
    z[84]= - n<T>(18719,8)*z[73] + z[84];
    z[103]=n<T>(1,3)*i;
    z[84]=z[84]*z[103];
    z[105]=z[96]*z[79];
    z[84]=z[84] + 4847*z[105];
    z[84]=z[1]*z[84];
    z[83]=z[97] - z[83];
    z[83]=z[73]*z[83];
    z[83]= - n<T>(2351,2)*z[76] + z[83];
    z[96]=z[96]*z[69];
    z[96]=n<T>(1,4)*z[73] + z[96];
    z[96]=z[96]*z[98];
    z[83]=n<T>(1,2)*z[94] + z[102] + z[96] + n<T>(1,4)*z[83] + z[84];
    z[83]=z[14]*z[83];
    z[84]=3361*z[7];
    z[94]=n<T>(26993,18)*z[5];
    z[96]=z[84] + z[94];
    z[98]= - 32185*z[4] + n<T>(72263,2)*z[3];
    z[98]=n<T>(1,9)*z[98];
    z[102]= - n<T>(26993,18)*z[69] + z[98] + z[96];
    z[102]=z[6]*z[102];
    z[84]=z[98] + z[84];
    z[84]=z[4]*z[84];
    z[105]=3361*z[79];
    z[106]= - 4153*z[4] + n<T>(72263,9)*z[3];
    z[106]=n<T>(1,2)*z[106];
    z[107]= - i*z[106];
    z[107]=z[107] - z[105];
    z[107]=z[1]*z[107];
    z[108]=z[4] - z[69];
    z[108]=z[108]*z[94];
    z[84]= - n<T>(26993,18)*z[35] + z[102] + z[108] + z[107] + z[84];
    z[84]=z[17]*z[84];
    z[98]=z[98]*i;
    z[98]=z[98] + z[105];
    z[98]=z[98]*z[1];
    z[94]=z[94]*z[69];
    z[94]=z[98] + z[94];
    z[98]=z[6] + z[4];
    z[94]=z[94]*z[98];
    z[98]=n<T>(26993,18)*z[6] + z[106] + z[96];
    z[98]=z[35]*z[98];
    z[94]=z[98] + z[94];
    z[94]=z[15]*z[94];
    z[98]= - 529*z[7] + n<T>(523,3)*z[3] - 649*z[4] + 767*z[70];
    z[98]=z[31]*z[98];
    z[83]=z[83] + z[94] + z[84] + z[98];
    z[84]=3836*z[3] - 1109*z[4];
    z[84]=z[4]*z[84];
    z[94]=601*z[4] + n<T>(6952,3)*z[3];
    z[98]=n<T>(8131,9)*z[7] - z[94];
    z[98]=z[7]*z[98];
    z[84]=z[98] - n<T>(6470,3)*z[73] + z[84];
    z[84]=z[7]*z[84];
    z[98]= - n<T>(84383,4)*z[3] - n<T>(12485,3)*z[4];
    z[98]=z[98]*z[85];
    z[98]=n<T>(73159,4)*z[73] + z[98];
    z[98]=z[4]*z[98];
    z[84]=z[84] - n<T>(512891,36)*z[76] + z[98];
    z[98]=n<T>(9157,3)*z[3] - n<T>(32411,4)*z[4];
    z[98]=z[4]*z[98];
    z[98]=n<T>(287677,24)*z[73] + z[98];
    z[102]=n<T>(1,9)*i;
    z[98]=z[98]*z[102];
    z[102]=n<T>(7903,27)*z[3] - 172*z[4];
    z[102]=i*z[102];
    z[102]=z[102] - n<T>(5381,27)*z[79];
    z[105]=2*z[7];
    z[102]=z[102]*z[105];
    z[98]=z[98] + z[102];
    z[98]=z[1]*z[98];
    z[102]= - 9406*z[3] + n<T>(11023,2)*z[4];
    z[102]=i*z[102];
    z[102]=z[102] + 3920*z[79];
    z[102]=z[1]*z[102];
    z[106]= - n<T>(10747,2)*z[3] + 9698*z[4];
    z[106]=z[4]*z[106];
    z[102]=z[102] - n<T>(22087,4)*z[73] + z[106];
    z[106]=n<T>(3379,27)*z[7] - n<T>(3569,27)*z[3] + 35*z[4];
    z[106]=z[106]*z[105];
    z[107]=11090*z[69] - 478*z[7] + 10604*z[3] - n<T>(16985,2)*z[4];
    z[107]=5*z[107] - n<T>(60761,8)*z[5];
    z[107]=z[5]*z[107];
    z[102]=n<T>(1,81)*z[107] + z[106] + n<T>(1,27)*z[102];
    z[102]=z[5]*z[102];
    z[84]=z[102] + n<T>(1,9)*z[84] + z[98];
    z[84]=z[5]*z[84];
    z[98]= - 4237*z[3] + n<T>(4532,3)*z[4];
    z[98]=z[4]*z[98];
    z[102]= - n<T>(3953,3)*z[3] + 752*z[4];
    z[102]=29*z[102] + n<T>(96109,3)*z[7];
    z[102]=z[7]*z[102];
    z[98]=n<T>(1,9)*z[102] - n<T>(26587,9)*z[73] + z[98];
    z[98]=z[7]*z[98];
    z[102]= - n<T>(31,6)*z[3] + z[4];
    z[102]=z[4]*z[102];
    z[102]=n<T>(15167,6)*z[73] + 131*z[102];
    z[102]=z[4]*z[102];
    z[98]=z[98] + n<T>(216247,54)*z[76] + z[102];
    z[98]=z[7]*z[98];
    z[102]= - 406*z[3] - n<T>(7865,6)*z[4];
    z[102]=z[4]*z[102];
    z[102]=n<T>(57371,36)*z[73] + z[102];
    z[102]=i*z[102];
    z[94]=i*z[94];
    z[94]=z[94] - n<T>(8131,9)*z[79];
    z[94]=z[94]*z[90];
    z[94]=z[102] + z[94];
    z[94]=z[7]*z[94];
    z[102]= - 3788*z[3] + n<T>(121525,18)*z[4];
    z[102]=z[4]*z[102];
    z[102]=n<T>(20123,3)*z[73] + z[102];
    z[102]=z[4]*z[102];
    z[102]=n<T>(260335,72)*z[76] + z[102];
    z[102]=z[102]*z[103];
    z[94]=z[102] + z[94];
    z[94]=z[1]*z[94];
    z[102]= - 168103*z[3] - 6325*z[4];
    z[102]=z[102]*z[85];
    z[102]=n<T>(151081,2)*z[73] + z[102];
    z[102]=z[4]*z[102];
    z[102]=n<T>(709967,6)*z[76] + z[102];
    z[102]=z[4]*z[102];
    z[106]=npow(z[3],4);
    z[102]=n<T>(56927,4)*z[106] + z[102];
    z[94]=z[94] + n<T>(1,18)*z[102] + z[98];
    z[84]=n<T>(1,3)*z[94] + z[84];
    z[94]=z[4]*z[100];
    z[94]= - n<T>(3397,2)*z[73] + z[94];
    z[85]=z[94]*z[85];
    z[94]=n<T>(1,2)*z[73];
    z[98]=npow(z[4],2);
    z[98]=z[94] - n<T>(1,3)*z[98];
    z[97]=z[98]*z[97];
    z[85]=z[97] - n<T>(2351,4)*z[76] + z[85];
    z[97]=3373*z[3] - n<T>(8975,36)*z[4];
    z[97]=z[4]*z[97];
    z[97]= - n<T>(288785,72)*z[73] + z[97];
    z[97]=i*z[97];
    z[98]= - n<T>(25013,2)*z[3] + 4847*z[4];
    z[98]=z[98]*z[79];
    z[97]=z[97] + n<T>(1,3)*z[98];
    z[97]=z[1]*z[97];
    z[85]=n<T>(1,2)*z[85] + z[97];
    z[72]= - z[72]*z[78];
    z[78]= - 34223*z[3] + n<T>(52963,4)*z[4];
    z[78]=z[4]*z[78];
    z[78]=n<T>(172471,8)*z[73] + z[78];
    z[97]= - n<T>(26993,2)*z[3] + n<T>(15803,3)*z[4];
    z[97]=z[97]*z[69];
    z[72]=n<T>(1,34)*z[97] + n<T>(1,51)*z[78] + z[72];
    z[78]=n<T>(1,3)*z[5];
    z[72]=z[72]*z[78];
    z[97]= - n<T>(1,2)*z[70];
    z[98]= - n<T>(15803,153)*z[4] + n<T>(383,4)*z[3];
    z[97]=z[98]*z[97];
    z[98]= - n<T>(132977,4)*z[3] + 17791*z[4];
    z[98]=z[4]*z[98];
    z[98]=n<T>(137473,8)*z[73] + z[98];
    z[100]=n<T>(25013,4)*z[3] - 7465*z[4];
    z[100]=z[7]*z[100];
    z[98]=n<T>(1,3)*z[98] + z[100];
    z[97]=n<T>(1,51)*z[98] + z[97];
    z[97]=z[6]*z[97];
    z[72]=z[97] + n<T>(1,17)*z[85] + z[72];
    z[72]=z[72]*z[82];
    z[82]= - n<T>(26993,34)*z[6] - n<T>(588073,34)*z[5] + n<T>(333721,34)*z[3] - 557
   *z[4];
    z[82]=i*z[82];
    z[82]=n<T>(50071,17)*z[79] + z[82];
    z[85]=z[9]*i;
    z[82]=n<T>(1,162)*z[82] + n<T>(41,17)*z[85];
    z[85]= - n<T>(26993,918)*z[15] + n<T>(82,17)*z[11] + n<T>(6335,459)*z[13] + n<T>(4613,2754)*z[14]
     - n<T>(82,51)*z[10];
    z[85]=i*z[85];
    z[82]=n<T>(1,2)*z[82] + z[85];
    z[82]=z[36]*z[82];
    z[85]=n<T>(45031,9)*z[3] - 2438*z[4];
    z[85]=n<T>(1,17)*z[85] + n<T>(1394,3)*z[7];
    z[85]=n<T>(1,3)*z[85] - n<T>(2107,17)*z[70];
    z[85]=z[24]*z[85];
    z[72]=z[85] + z[82] + n<T>(1,17)*z[84] + z[72];
    z[82]=z[81]*z[3];
    z[84]=z[82] + n<T>(3413,9)*z[73];
    z[85]=3530*z[4];
    z[97]= - z[85] + n<T>(62147,9)*z[3];
    z[98]= - n<T>(45982,9)*z[7] + z[97];
    z[98]=z[98]*z[90];
    z[100]=n<T>(92,9)*z[3] + z[81];
    z[100]=i*z[100];
    z[100]=z[100] + n<T>(25205,9)*z[79];
    z[100]=z[100]*z[86];
    z[98]=z[100] + z[98] + z[84];
    z[100]=n<T>(1,6)*z[91];
    z[102]=n<T>(1,3)*z[69];
    z[106]=z[102] + z[3] - n<T>(2,3)*z[7];
    z[106]=z[106]*z[75];
    z[98]= - z[100] + n<T>(1,3)*z[98] + z[106];
    z[98]=z[9]*z[98];
    z[81]=z[87] + z[81];
    z[87]= - z[81] + n<T>(62147,18)*z[3];
    z[87]=z[87]*z[90];
    z[84]=z[87] + n<T>(1,2)*z[84];
    z[87]=n<T>(3367,9)*z[3] + n<T>(1765,2)*z[4];
    z[87]=i*z[87];
    z[87]=z[87] + n<T>(20777,18)*z[79];
    z[87]=z[87]*z[86];
    z[87]=z[92] + z[87] - z[84];
    z[92]=z[95] - z[90];
    z[95]=z[104] - z[92];
    z[95]=z[95]*z[75];
    z[87]= - z[100] + z[95] + n<T>(1,3)*z[87];
    z[87]=z[10]*z[87];
    z[95]= - n<T>(5719,18)*z[73] - z[82];
    z[95]=i*z[95];
    z[97]= - i*z[97];
    z[97]=z[97] + n<T>(45982,9)*z[79];
    z[97]=z[97]*z[90];
    z[95]=z[95] + z[97];
    z[95]=z[1]*z[95];
    z[97]=z[81]*z[73];
    z[76]=z[97] + n<T>(3413,9)*z[76];
    z[76]=n<T>(1,2)*z[76];
    z[95]= - z[76] + z[95];
    z[97]= - i*z[3];
    z[97]=z[97] + n<T>(2,3)*z[79];
    z[97]=z[1]*z[97];
    z[97]= - z[94] + z[97];
    z[97]=z[97]*z[75];
    z[87]=z[87] + z[98] + n<T>(1,3)*z[95] + z[97];
    z[87]=z[10]*z[87];
    z[95]= - n<T>(701,18)*z[3] - 353*z[4];
    z[95]=i*z[95];
    z[93]=5*z[95] - z[93];
    z[93]=z[93]*z[86];
    z[84]=z[91] + z[93] - z[84];
    z[91]= - z[102] - z[92];
    z[91]=z[91]*z[75];
    z[84]=z[91] + n<T>(1,3)*z[84];
    z[84]=z[9]*z[84];
    z[82]=n<T>(7933,18)*z[73] + z[82];
    z[82]=i*z[82];
    z[80]=z[3]*z[80];
    z[80]=z[82] + z[80];
    z[80]=z[1]*z[80];
    z[76]= - z[76] + z[80];
    z[80]=z[3]*z[69];
    z[80]= - z[94] + z[80];
    z[80]=z[80]*z[75];
    z[76]=z[84] + n<T>(1,3)*z[76] + z[80];
    z[76]=z[9]*z[76];
    z[80]=n<T>(863,9)*z[3] - 31*z[4];
    z[80]=5*z[80] + n<T>(131,3)*z[7] - n<T>(2147,27)*z[70];
    z[80]=z[23]*z[80];
    z[76]=z[80] + z[76] + z[87];
    z[80]=210238*z[3] - n<T>(161197,4)*z[4];
    z[80]=z[4]*z[80];
    z[82]=n<T>(180839,2)*z[5] + n<T>(37175,2)*z[7] - 29867*z[3] - 16073*z[4];
    z[82]=z[5]*z[82];
    z[80]=z[82] - n<T>(561139,8)*z[73] + z[80];
    z[82]= - n<T>(279331,2)*z[3] - 4507*z[4];
    z[82]=n<T>(1,9)*z[82] + z[96];
    z[82]=z[6]*z[82];
    z[84]= - n<T>(58765,12)*z[7] + n<T>(16615,2)*z[3] - n<T>(12409,3)*z[4];
    z[84]=z[7]*z[84];
    z[80]=n<T>(1,4)*z[82] + z[84] + n<T>(1,9)*z[80];
    z[81]=n<T>(3229,9)*z[3] - z[81];
    z[75]=n<T>(1,3)*z[81] - z[75];
    z[75]=n<T>(1,2)*z[75] - z[88];
    z[75]=z[9]*z[75];
    z[81]=n<T>(52624,9)*z[7] - n<T>(3137,9)*z[3] + z[85];
    z[81]=n<T>(1,3)*z[81] + 82*z[5];
    z[81]=n<T>(41,2)*z[10] + n<T>(1,3)*z[81] + z[88];
    z[81]=z[10]*z[81];
    z[82]=n<T>(18541,2)*z[3] - n<T>(7045,3)*z[4];
    z[82]= - n<T>(4613,8)*z[14] - n<T>(4613,4)*z[6] + n<T>(1,2)*z[82] - z[99];
    z[82]=z[14]*z[82];
    z[75]=n<T>(1,27)*z[82] + z[81] + n<T>(1,9)*z[80] + z[75];
    z[75]=z[35]*z[75];
    z[80]= - 4*z[4] + n<T>(2395,153)*z[3];
    z[80]=z[80]*z[4];
    z[80]=z[80] - n<T>(8039,102)*z[73];
    z[81]=2*z[4];
    z[82]= - z[81] + n<T>(5,9)*z[3];
    z[84]=z[7] + z[82];
    z[84]=z[84]*z[105];
    z[85]= - n<T>(12317,3)*z[4] + 4846*z[3];
    z[85]=n<T>(1,17)*z[85];
    z[87]=z[85]*i;
    z[87]=z[87] + 10*z[79];
    z[88]=z[1]*z[87];
    z[85]=z[85] + 10*z[7];
    z[91]= - n<T>(916,51)*z[69] + z[85];
    z[92]=n<T>(619,34)*z[5];
    z[91]=n<T>(1,3)*z[91] + z[92];
    z[91]=z[91]*z[78];
    z[93]=z[13]*z[69];
    z[84]=n<T>(4655,918)*z[16] - n<T>(4655,459)*z[93] - n<T>(6335,1377)*z[35] + 
    z[91] - n<T>(1,9)*z[88] - n<T>(1,3)*z[80] + z[84];
    z[84]=z[16]*z[84];
    z[80]=z[80]*z[103];
    z[82]= - i*z[82];
    z[82]=z[82] - z[79];
    z[82]=z[82]*z[105];
    z[80]=z[80] + z[82];
    z[80]=z[1]*z[80];
    z[82]= - z[87]*z[86];
    z[86]= - z[69]*z[92];
    z[82]=z[82] + z[86];
    z[78]=z[82]*z[78];
    z[82]= - n<T>(916,51)*z[5] - z[85];
    z[82]= - n<T>(4655,918)*z[13] + n<T>(1,9)*z[82];
    z[82]=z[35]*z[82];
    z[78]=z[80] + z[78] + z[82];
    z[78]=z[13]*z[78];
    z[80]=118*z[4];
    z[82]= - z[80] + n<T>(461,3)*z[3];
    z[85]= - i*z[82];
    z[85]=z[85] - n<T>(461,3)*z[79];
    z[85]=z[1]*z[85];
    z[69]=59*z[5] - 118*z[69] + n<T>(461,3)*z[7] + z[82];
    z[69]=z[5]*z[69];
    z[69]=z[85] + z[69];
    z[82]=n<T>(13040,3)*z[3] + 2371*z[4];
    z[82]=n<T>(2,51)*z[82] + 115*z[7];
    z[82]=z[82]*z[90];
    z[85]=n<T>(2371,9)*z[3] + 59*z[4];
    z[85]=z[4]*z[85];
    z[69]=z[82] + n<T>(115,27)*z[73] + n<T>(2,17)*z[85] + n<T>(2,51)*z[69];
    z[69]= - n<T>(2398,459)*z[18] + 2*z[69] - n<T>(419,153)*z[35];
    z[69]=z[18]*z[69];
    z[73]= - z[14] - z[4];
    z[73]=z[19]*z[73];
    z[73]=n<T>(1,459)*z[73] + n<T>(1,612)*z[27] - n<T>(1,1836)*z[26] + n<T>(1,459)*z[21]
   ;
    z[73]=z[101]*z[73];
    z[82]=z[33] + z[30];
    z[82]=n<T>(1,102)*z[32] - n<T>(2,51)*z[29] - n<T>(1,34)*z[82];
    z[82]=z[89]*z[82];
    z[81]= - n<T>(665,153)*z[3] - z[81];
    z[81]=n<T>(7,9)*z[81] + z[105] - n<T>(4043,1377)*z[70];
    z[81]=z[34]*z[81];
    z[85]=z[28] - z[25];
    z[85]= - n<T>(1,27)*z[85];
    z[74]=z[74]*z[85];
    z[85]= - n<T>(665,306)*z[3] - z[4];
    z[85]=n<T>(4043,918)*z[5] + n<T>(7,3)*z[85];
    z[85]=i*z[85];
    z[79]=3*z[79] + z[85];
    z[79]=z[38]*z[79];
    z[85]=n<T>(14,3)*z[46] - n<T>(4,9)*z[43] + n<T>(25669,5508)*z[41];
    z[85]=i*z[85];
    z[70]=n<T>(35485,3)*z[7] + n<T>(296105,18)*z[3] - 13927*z[4] - n<T>(34453,6)*
    z[70];
    z[70]=z[22]*z[70];
    z[86]= - n<T>(34849,54)*z[5] + n<T>(33149,27)*z[7] + n<T>(1127,2)*z[3] + n<T>(87905,27)*z[4];
    z[86]=z[37]*z[86];
    z[87]= - n<T>(3459967,2)*z[5] - 1375141*z[7] + n<T>(866947,2)*z[3] + 100019*
    z[4];
    z[87]=z[39]*z[87];
    z[80]= - n<T>(551,9)*z[3] - z[80];
    z[80]=n<T>(1,17)*z[80] - n<T>(155,9)*z[7];
    z[80]=z[47]*z[80];

    r += n<T>(862217,660960)*z[40] + n<T>(3839,918)*z[42] - n<T>(8,3)*z[45] + n<T>(3637,459)*z[48]
     - n<T>(2420,459)*z[49]
     + n<T>(2476,1377)*z[50] - n<T>(326,153)*
      z[51] + n<T>(1444,459)*z[52] + n<T>(4276,1377)*z[53] + n<T>(4510,459)*z[54]
       + z[55] + n<T>(1039,51)*z[56] + n<T>(16,153)*z[57] - n<T>(4568,459)*z[58]
     - 
      n<T>(6634,1377)*z[59] + n<T>(5938,1377)*z[60] + n<T>(1253,459)*z[61] - n<T>(154,459)*z[62]
     + n<T>(3268,1377)*z[63]
     + n<T>(59,51)*z[64] - n<T>(767,153)*z[65]
       + n<T>(8,51)*z[66] - n<T>(944,153)*z[67] + z[68] + z[69] + n<T>(1,612)*z[70]
       + n<T>(1,9)*z[71] + n<T>(1,3)*z[72] + z[73] + z[74] + n<T>(1,51)*z[75] + n<T>(1,17)*z[76]
     + n<T>(2,51)*z[77]
     + z[78]
     + z[79]
     + n<T>(16,3)*z[80]
     + z[81]
       + z[82] + n<T>(1,153)*z[83] + z[84] + z[85] + n<T>(1,102)*z[86] + n<T>(1,16524)*z[87];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1276(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1276(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
