#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1118(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=c[15];
    z[2]=c[18];
    z[3]=c[19];
    z[4]=c[25];
    z[5]=c[28];
    z[6]=c[29];
    z[7]=c[30];
    z[8]=c[31];
    z[9]=f[13];
    z[10]=z[4] - z[1];
    z[11]=z[9] + z[6];

    r += z[2] - n<T>(1,8)*z[3] - n<T>(3,2)*z[5] - 3*z[7] + n<T>(49,8)*z[8] - n<T>(1,4)*
      z[10] - n<T>(3,4)*z[11];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1118(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1118(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
