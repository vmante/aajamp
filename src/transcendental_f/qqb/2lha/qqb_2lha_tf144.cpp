#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf144(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[94];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[15];
    z[12]=d[16];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=d[18];
    z[16]=e[0];
    z[17]=e[1];
    z[18]=e[2];
    z[19]=e[4];
    z[20]=e[5];
    z[21]=e[6];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=e[10];
    z[25]=e[11];
    z[26]=c[3];
    z[27]=c[11];
    z[28]=c[15];
    z[29]=c[18];
    z[30]=c[19];
    z[31]=c[23];
    z[32]=c[25];
    z[33]=c[26];
    z[34]=c[28];
    z[35]=c[29];
    z[36]=c[31];
    z[37]=e[3];
    z[38]=e[7];
    z[39]=f[0];
    z[40]=f[1];
    z[41]=f[2];
    z[42]=f[3];
    z[43]=f[4];
    z[44]=f[5];
    z[45]=f[6];
    z[46]=f[7];
    z[47]=f[8];
    z[48]=f[10];
    z[49]=f[11];
    z[50]=f[12];
    z[51]=f[13];
    z[52]=f[14];
    z[53]=f[16];
    z[54]=f[17];
    z[55]=f[18];
    z[56]=f[21];
    z[57]=f[23];
    z[58]=f[30];
    z[59]=f[31];
    z[60]=f[32];
    z[61]=f[35];
    z[62]=f[36];
    z[63]=f[37];
    z[64]=f[38];
    z[65]=f[39];
    z[66]=f[41];
    z[67]=f[95];
    z[68]=f[96];
    z[69]=z[1]*i;
    z[70]=z[69] - z[9];
    z[71]=2*z[6];
    z[72]=4*z[2];
    z[73]=4*z[5];
    z[74]=2*z[8] - z[72] + z[71] + z[73] - n<T>(55,2)*z[70];
    z[74]=z[8]*z[74];
    z[75]=n<T>(59,8)*z[9] - n<T>(59,4)*z[69] - z[73];
    z[75]=z[9]*z[75];
    z[76]=2*z[5];
    z[77]=n<T>(161,8)*z[6] - n<T>(169,4)*z[9] + n<T>(177,4)*z[69] - z[76];
    z[77]=z[6]*z[77];
    z[78]=2*z[69];
    z[79]=z[78] + 11;
    z[80]=2*z[9];
    z[81]=z[80] - z[79];
    z[82]=2*z[2];
    z[81]=z[81]*z[82];
    z[79]=2*z[79] + z[5];
    z[79]=z[5]*z[79];
    z[83]=z[6] - z[8];
    z[83]=z[3]*z[83];
    z[84]=3*z[6] - z[70];
    z[84]= - 63*z[7] + n<T>(59,2)*z[84] - 55*z[8];
    z[84]=z[7]*z[84];
    z[74]=n<T>(1,4)*z[84] + 2*z[83] + z[74] + z[81] + z[77] + z[79] + z[75];
    z[74]=z[7]*z[74];
    z[75]=n<T>(1,2)*z[5];
    z[77]=z[75] - z[69];
    z[77]=z[77]*z[5];
    z[79]=5*z[69];
    z[81]= - z[79] + z[76];
    z[81]=2*z[81] - z[9];
    z[81]=z[81]*z[80];
    z[83]=z[69] - z[5];
    z[84]=n<T>(1,2)*z[6];
    z[85]= - z[84] - z[83];
    z[85]=z[6]*z[85];
    z[86]=2*z[83];
    z[87]=z[86] + z[9];
    z[87]=2*z[87] - z[2];
    z[87]=z[87]*z[82];
    z[88]=4*z[9];
    z[72]=z[88] - z[72];
    z[89]=9*z[83] - z[72];
    z[89]=2*z[89] + 9*z[3];
    z[89]=z[3]*z[89];
    z[72]= - n<T>(8,3)*z[10] + 42*z[3] - n<T>(51,2)*z[6] + n<T>(33,2)*z[83] + z[72];
    z[72]=z[10]*z[72];
    z[72]=z[72] + z[89] + z[87] + 51*z[85] - 33*z[77] + z[81];
    z[72]=z[10]*z[72];
    z[81]=16*z[9];
    z[85]=5*z[2];
    z[87]=3*z[69];
    z[89]=z[85] - z[6] - z[81] + z[87] + z[76];
    z[89]=z[89]*z[82];
    z[90]=z[6] - z[9];
    z[90]=n<T>(11,2)*z[8] - 8*z[2] - n<T>(9,4)*z[69] - z[76] + n<T>(27,4)*z[90];
    z[90]=z[8]*z[90];
    z[91]=z[78] - z[5];
    z[91]=z[91]*z[76];
    z[92]= - n<T>(31,2)*z[9] + n<T>(103,2)*z[69] - z[73];
    z[92]=z[9]*z[92];
    z[76]= - n<T>(37,4)*z[6] + 24*z[9] - 21*z[69] - z[76];
    z[76]=z[6]*z[76];
    z[76]=z[90] + z[89] + z[76] + z[91] + z[92];
    z[76]=z[8]*z[76];
    z[89]=z[78] + z[6];
    z[81]=z[2] - z[81] + 13*z[5] + z[89];
    z[81]=z[2]*z[81];
    z[90]=11*z[69] + 23*z[5];
    z[88]= - n<T>(176,3)*z[3] + z[8] + n<T>(33,2)*z[2] + n<T>(41,3)*z[6] + n<T>(1,3)*
    z[90] + z[88];
    z[88]=z[3]*z[88];
    z[90]= - 32*z[69] + 17*z[5];
    z[90]=z[5]*z[90];
    z[86]= - z[86] + z[9];
    z[86]=z[9]*z[86];
    z[91]=53*z[6] + 41*z[69] - 44*z[5];
    z[91]=z[6]*z[91];
    z[92]=z[69] + z[5];
    z[93]=z[6] + z[92];
    z[93]=2*z[93] + z[8];
    z[93]=z[8]*z[93];
    z[81]=z[88] + z[93] + z[81] + n<T>(2,3)*z[91] + n<T>(5,3)*z[90] + 8*z[86];
    z[81]=z[3]*z[81];
    z[86]=z[84] + z[80] - z[87] - z[5];
    z[86]=z[6]*z[86];
    z[82]=n<T>(3,2)*z[8] + z[82] - 15*z[69] - z[80] + z[6];
    z[82]=z[8]*z[82];
    z[87]=19*z[69];
    z[88]=z[87] + n<T>(9,2)*z[5];
    z[88]=z[5]*z[88];
    z[90]=n<T>(1,2)*z[9] - z[92];
    z[90]=z[9]*z[90];
    z[88]=z[88] + 9*z[90];
    z[85]=15*z[8] + z[85] + n<T>(9,2)*z[9] + 13*z[69] - n<T>(19,2)*z[5];
    z[85]= - n<T>(19,6)*z[4] + n<T>(1,2)*z[85] - 6*z[3];
    z[85]=z[4]*z[85];
    z[79]= - z[79] - z[6];
    z[79]=z[2]*z[79];
    z[90]=12*z[69];
    z[91]=n<T>(3,2)*z[3] - 4*z[8] - z[2] + z[90] + z[6] + z[5];
    z[91]=z[3]*z[91];
    z[79]=z[85] + z[91] + z[82] + z[79] + n<T>(1,2)*z[88] + z[86];
    z[79]=z[4]*z[79];
    z[73]=n<T>(65,8)*z[9] - n<T>(65,4)*z[69] - z[73];
    z[73]=z[9]*z[73];
    z[82]=n<T>(5,4)*z[69] - 53*z[5];
    z[82]= - n<T>(259,6)*z[6] + n<T>(1,3)*z[82] + n<T>(73,4)*z[9];
    z[82]=z[82]*z[84];
    z[73]=z[82] - n<T>(71,3)*z[77] + z[73];
    z[73]=z[6]*z[73];
    z[77]=5*z[5];
    z[80]=z[77] + z[80] - z[89];
    z[80]=z[6]*z[80];
    z[77]= - 13*z[9] - z[90] - z[77];
    z[77]=z[9]*z[77];
    z[71]=n<T>(2,3)*z[2] - z[71] + n<T>(43,2)*z[9] - 16*z[69] - n<T>(15,2)*z[5];
    z[71]=z[2]*z[71];
    z[82]=26*z[69] - 3*z[5];
    z[82]=z[5]*z[82];
    z[71]=z[71] + z[80] + z[82] + z[77];
    z[71]=z[2]*z[71];
    z[77]=z[4] + z[6];
    z[80]=z[69]*z[77];
    z[80]=z[26] + z[80];
    z[80]=z[12]*z[80];
    z[77]=z[69] - z[77];
    z[77]=z[19]*z[77];
    z[82]=4*z[10] + 6*z[8] + 9*z[2] - 28*z[69] + 19*z[9];
    z[82]=z[22]*z[82];
    z[77]= - z[39] + z[77] + z[82] + z[80];
    z[80]=z[69]*z[5];
    z[80]=z[80] + z[26];
    z[82]= - z[7]*z[69];
    z[82]=z[82] - z[80];
    z[82]=z[13]*z[82];
    z[84]=z[2] - z[9] + z[83];
    z[84]= - 33*z[10] + 4*z[84] - 29*z[3];
    z[84]=z[24]*z[84];
    z[85]=z[7] - z[83];
    z[85]=z[20]*z[85];
    z[82]=z[60] + z[85] + z[84] + z[82];
    z[84]=z[69]*z[9];
    z[84]=z[84] + z[26];
    z[85]=z[10]*z[69];
    z[85]=z[85] + z[84];
    z[85]=z[15]*z[85];
    z[86]= - z[10] + z[70];
    z[86]=z[25]*z[86];
    z[85]=z[85] + z[86] + z[66] - z[45];
    z[86]=z[8]*z[69];
    z[84]=z[86] + z[84];
    z[84]=z[14]*z[84];
    z[70]=z[8] - z[70];
    z[70]=z[23]*z[70];
    z[70]=z[84] + z[70];
    z[84]= - z[3]*z[69];
    z[80]=z[84] - z[80];
    z[80]=z[11]*z[80];
    z[84]= - z[3] + z[83];
    z[84]=z[18]*z[84];
    z[80]=z[80] + z[84];
    z[84]= - z[87] - n<T>(5,2)*z[5];
    z[75]=z[84]*z[75];
    z[84]= - n<T>(22,3)*z[9] + 10*z[69] + n<T>(43,4)*z[5];
    z[84]=z[9]*z[84];
    z[75]=z[75] + z[84];
    z[75]=z[9]*z[75];
    z[84]=z[30] - z[29];
    z[86]=z[63] - z[35];
    z[78]= - z[4] + z[78] - z[2];
    z[78]=z[16]*z[78];
    z[78]=z[78] + z[51];
    z[87]=npow(z[5],3);
    z[87]=z[87] + z[28];
    z[88]=84*z[33] + 20*z[31] - n<T>(221,72)*z[27];
    z[88]=i*z[88];
    z[89]=n<T>(109,6)*z[6] + n<T>(73,3)*z[5] - n<T>(35,4)*z[9];
    z[89]= - 49*z[3] + 28*z[8] + n<T>(1,2)*z[89] - 26*z[2];
    z[89]=n<T>(68,3)*z[10] - n<T>(61,6)*z[7] + n<T>(1,3)*z[89] + 7*z[4];
    z[89]=z[26]*z[89];
    z[83]=z[3] + z[83] + z[7];
    z[83]= - n<T>(77,2)*z[8] - 41*z[6] - 4*z[83];
    z[83]=z[21]*z[83];
    z[69]=z[2] - 6*z[6] + 14*z[69] - 15*z[5];
    z[69]=z[17]*z[69];
    z[90]= - z[6] - z[3];
    z[90]=z[37]*z[90];
    z[91]= - z[8] - z[7];
    z[91]=z[38]*z[91];

    r +=  - n<T>(8,3)*z[32] - 27*z[34] - n<T>(911,36)*z[36] + n<T>(3,2)*z[40] - n<T>(79,3)*z[41]
     - n<T>(15,4)*z[42]
     - n<T>(41,2)*z[43]
     - z[44]
     - 32*z[46]
     + 60*
      z[47] + 34*z[48] + n<T>(59,4)*z[49] + n<T>(77,6)*z[50] + 9*z[52] - n<T>(51,2)
      *z[53] + n<T>(1,4)*z[54] - z[55] - 15*z[56] + 4*z[57] + 22*z[58] + n<T>(59,8)*z[59]
     - n<T>(47,2)*z[61]
     - n<T>(81,8)*z[62]
     - 13*z[64]
     + n<T>(177,8)*
      z[65] + z[67] - z[68] + z[69] + n<T>(35,2)*z[70] + z[71] + z[72] + 
      z[73] + z[74] + z[75] + z[76] + 3*z[77] + 5*z[78] + z[79] + n<T>(100,3)*z[80]
     + z[81]
     + 2*z[82]
     + z[83]
     + 6*z[84]
     + 12*z[85] - n<T>(9,2)*
      z[86] + n<T>(4,3)*z[87] + z[88] + z[89] + n<T>(142,3)*z[90] + n<T>(63,2)*
      z[91];
 
    return r;
}

template std::complex<double> qqb_2lha_tf144(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf144(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
