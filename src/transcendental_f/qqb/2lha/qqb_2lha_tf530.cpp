#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf530(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,111>& f,
  const std::array<std::complex<T>,472>& g
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[23];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=d[5];
    z[3]=d[6];
    z[4]=c[12];
    z[5]=c[13];
    z[6]=c[20];
    z[7]=c[23];
    z[8]=f[44];
    z[9]=g[82];
    z[10]=g[90];
    z[11]=g[101];
    z[12]=g[131];
    z[13]=g[214];
    z[14]=g[245];
    z[15]=g[345];
    z[16]=g[347];
    z[17]=g[348];
    z[18]=z[3] - z[2];
    z[19]= - i*z[18];
    z[20]=z[1]*z[19];
    z[21]=z[4]*z[18];
    z[20]=z[17] - n<T>(11,180)*z[20] + z[21];
    z[21]= - n<T>(1,20)*z[7] + z[6] + n<T>(12,5)*z[5];
    z[19]=z[19]*z[21];
    z[21]= - z[9] + z[13] - z[12];
    z[22]=z[16] - z[11];
    z[18]=z[8]*z[18];
    z[18]=z[18] + z[10];

    r += n<T>(3,4)*z[14] + n<T>(1,12)*z[15] + n<T>(1,2)*z[18] + z[19] + n<T>(1,3)*z[20]
       + n<T>(1,4)*z[21] + n<T>(1,6)*z[22];
 
    return r;
}

template std::complex<double> qqb_2lha_tf530(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,111>& f,
  const std::array<std::complex<double>,472>& g
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf530(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,111>& f,
  const std::array<std::complex<dd_real>,472>& g
);
#endif
