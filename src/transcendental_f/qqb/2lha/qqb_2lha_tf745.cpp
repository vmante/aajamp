#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf745(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[36];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=d[4];
    z[11]=c[11];
    z[12]=c[15];
    z[13]=c[18];
    z[14]=c[19];
    z[15]=c[21];
    z[16]=c[23];
    z[17]=c[25];
    z[18]=c[26];
    z[19]=c[28];
    z[20]=c[29];
    z[21]=c[31];
    z[22]=f[0];
    z[23]=f[3];
    z[24]=f[5];
    z[25]=f[11];
    z[26]=f[17];
    z[27]=f[18];
    z[28]=2*z[4];
    z[29]=3*z[3];
    z[30]=z[28] + z[29];
    z[30]=z[1]*z[30];
    z[31]=z[5]*z[1];
    z[30]=2*z[31] + z[30];
    z[30]=i*z[30];
    z[32]=z[7] + z[8];
    z[33]=npow(z[5],2);
    z[30]=z[30] + z[32] - z[33];
    z[33]=z[5] - z[4];
    z[28]=z[33]*z[28];
    z[33]=n<T>(1,6)*z[9];
    z[34]=npow(z[3],2);
    z[35]=i*z[1];
    z[35]=3*z[2] - 7*z[35] - z[5] - z[4];
    z[35]=z[2]*z[35];
    z[28]=z[35] + z[28] - 3*z[34] - z[33] + 5*z[6] + 2*z[30];
    z[28]=z[2]*z[28];
    z[30]= - z[33] + n<T>(3,2)*z[34];
    z[33]= - z[29] + n<T>(1,2)*z[5];
    z[33]=z[5]*z[33];
    z[34]=z[29] + z[5];
    z[34]=z[4]*z[34];
    z[33]=n<T>(1,2)*z[34] + z[33] + 2*z[7] + z[30];
    z[33]=z[4]*z[33];
    z[34]= - z[29]*z[31];
    z[29]= - z[1]*z[29];
    z[29]=z[29] - z[31];
    z[29]=z[4]*z[29];
    z[31]= - 4*z[32] - 11*z[6];
    z[31]=z[1]*z[31];
    z[29]=z[29] + z[34] + z[31] - n<T>(1,2)*z[11] - 2*z[15] + 22*z[18] + 9*
    z[16];
    z[29]=i*z[29];
    z[31]=z[5]*z[3];
    z[30]=n<T>(3,2)*z[31] + 2*z[8] + z[30];
    z[30]=z[5]*z[30];
    z[31]=z[23] + z[26];
    z[32]=npow(z[10],3);
    z[32]=z[32] + z[27];
    z[34]= - n<T>(1,3)*z[9] + z[6];
    z[34]=z[10]*z[34];
    z[35]=6*z[6] - n<T>(1,2)*z[9];
    z[35]=z[3]*z[35];

    r +=  - n<T>(1,12)*z[12] - n<T>(1,3)*z[13] + n<T>(13,24)*z[14] - n<T>(13,4)*z[17]
       - n<T>(19,2)*z[19] + n<T>(1,4)*z[20] + n<T>(7,24)*z[21] + 4*z[22] + n<T>(9,4)*
      z[24] + n<T>(1,2)*z[25] + z[28] + z[29] + z[30] + n<T>(3,2)*z[31] + n<T>(2,3)
      *z[32] + z[33] + z[34] + z[35];
 
    return r;
}

template std::complex<double> qqb_2lha_tf745(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf745(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
