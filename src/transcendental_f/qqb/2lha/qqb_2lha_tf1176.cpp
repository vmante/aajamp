#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1176(
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[21];
  std::complex<T> r(0,0);

    z[1]=d[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[6];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[2];
    z[8]=d[4];
    z[9]=d[5];
    z[10]=d[9];
    z[11]=e[6];
    z[12]=e[7];
    z[13]=e[10];
    z[14]=e[13];
    z[15]=z[3] - z[5];
    z[16]=z[8] + z[7];
    z[16]=z[16]*z[15];
    z[17]=z[9] - z[10];
    z[18]=z[9]*z[17];
    z[19]= - z[6] + n<T>(1,2)*z[8];
    z[19]=z[8]*z[19];
    z[17]=z[5] - z[17];
    z[17]=z[4]*z[17];
    z[20]= - n<T>(1,2)*z[2] + z[6] - z[3];
    z[20]=z[2]*z[20];
    z[15]=z[2] - z[4] - z[15];
    z[15]=z[1]*z[15];

    r += z[11] + z[12] - z[13] + z[14] + z[15] + z[16] + z[17] + z[18]
       + z[19] + z[20];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1176(
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1176(
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
