#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1325(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[76];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[1];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[15];
    z[12]=d[16];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[5];
    z[20]=e[7];
    z[21]=e[8];
    z[22]=e[9];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[19];
    z[27]=c[23];
    z[28]=c[25];
    z[29]=c[26];
    z[30]=c[28];
    z[31]=e[3];
    z[32]=e[10];
    z[33]=e[6];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[2];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[10];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[14];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[18];
    z[47]=f[30];
    z[48]=f[31];
    z[49]=f[32];
    z[50]=f[33];
    z[51]=f[35];
    z[52]=f[36];
    z[53]=f[37];
    z[54]=f[39];
    z[55]=f[43];
    z[56]=n<T>(1,2)*z[4];
    z[57]=z[1]*i;
    z[58]=z[56] - z[57];
    z[59]=n<T>(1,2)*z[7];
    z[60]=n<T>(1,2)*z[6];
    z[61]=n<T>(1,2)*z[8];
    z[62]= - z[3] + z[61] + z[2] - z[59] + z[60] - z[58];
    z[62]=z[3]*z[62];
    z[58]=z[58]*z[4];
    z[63]= - z[57] + z[60];
    z[63]=z[6]*z[63];
    z[59]= - z[59] + z[57] + z[4];
    z[59]=z[7]*z[59];
    z[64]=z[61] - z[57] - z[6];
    z[64]=z[8]*z[64];
    z[65]=2*z[57];
    z[66]= - z[65] + z[9];
    z[66]=z[9]*z[66];
    z[67]= - z[2]*z[65];
    z[59]=z[62] + z[64] + z[67] + z[59] + z[63] - z[58] + z[66];
    z[59]=z[3]*z[59];
    z[62]= - z[4] - z[8];
    z[62]=z[57]*z[62];
    z[62]= - z[23] + z[62];
    z[62]=z[11]*z[62];
    z[63]=z[65] - z[2];
    z[64]= - z[4] + z[63];
    z[64]=z[16]*z[64];
    z[66]=z[57] - z[4];
    z[67]= - z[8] + z[66];
    z[67]=z[17]*z[67];
    z[68]= - z[7] + z[63];
    z[68]=z[21]*z[68];
    z[62]=z[67] + z[68] - z[46] - z[39] + z[28] + z[62] + z[64];
    z[56]=z[65] + z[56];
    z[56]=z[4]*z[56];
    z[64]=n<T>(9,4)*z[9] + n<T>(5,2)*z[57] + z[4];
    z[64]=z[9]*z[64];
    z[67]=z[57] - z[9];
    z[68]=n<T>(10,3)*z[6] + n<T>(4,3)*z[4] - z[67];
    z[68]=z[6]*z[68];
    z[69]= - n<T>(89,18)*z[5] + n<T>(13,6)*z[6] + n<T>(5,4)*z[9] - n<T>(3,4)*z[57]
     - n<T>(5,3)*z[4];
    z[69]=z[5]*z[69];
    z[56]=z[69] + z[68] + n<T>(1,3)*z[56] + z[64];
    z[56]=z[5]*z[56];
    z[64]=n<T>(1,2)*z[9];
    z[68]= - z[57] + z[64];
    z[69]=n<T>(7,2)*z[9];
    z[68]=z[68]*z[69];
    z[70]=z[57] + z[9];
    z[70]= - n<T>(7,2)*z[6] - z[4] + 7*z[70];
    z[70]=z[6]*z[70];
    z[68]=z[70] - z[58] + z[68];
    z[70]= - n<T>(25,2)*z[57] + z[4];
    z[70]=n<T>(7,4)*z[5] + n<T>(1,3)*z[70] - z[69];
    z[70]=z[5]*z[70];
    z[71]=n<T>(1,3)*z[7];
    z[72]= - z[4] + z[69];
    z[72]=n<T>(25,4)*z[5] + n<T>(1,2)*z[72] - 8*z[6];
    z[72]=z[72]*z[71];
    z[68]=z[72] + n<T>(1,3)*z[68] + z[70];
    z[68]=z[7]*z[68];
    z[60]=z[7] + n<T>(1,2)*z[5] - z[60] + 4*z[57] + z[4] - 2*z[2];
    z[60]=z[2]*z[60];
    z[70]= - z[6] - z[66];
    z[70]=z[6]*z[70];
    z[72]=2*z[5] - z[6] + z[66];
    z[72]=z[5]*z[72];
    z[60]=z[60] + z[70] + z[72];
    z[70]=z[65] - z[4];
    z[72]=n<T>(2,3)*z[4];
    z[70]=z[70]*z[72];
    z[65]=z[65] + z[4];
    z[72]=z[65] - z[7];
    z[73]=z[5] - z[6];
    z[72]= - z[73] - 2*z[72];
    z[71]=z[72]*z[71];
    z[72]=npow(z[9],2);
    z[60]=z[71] - z[70] - z[72] + n<T>(1,3)*z[60];
    z[60]=z[2]*z[60];
    z[71]= - z[64] - z[66];
    z[72]=3*z[9];
    z[71]=z[71]*z[72];
    z[71]= - z[58] + z[71];
    z[61]=z[61] + z[66];
    z[61]=z[8]*z[61];
    z[72]= - z[72] + z[66];
    z[72]=n<T>(1,4)*z[72] + z[8];
    z[72]=z[10]*z[72];
    z[61]=z[72] + n<T>(1,2)*z[71] + z[61];
    z[61]=z[10]*z[61];
    z[71]=z[3] + z[9];
    z[71]=z[57]*z[71];
    z[71]=z[23] + z[71];
    z[71]=z[12]*z[71];
    z[63]= - z[3] + z[63];
    z[63]=z[15]*z[63];
    z[67]= - z[3] + z[67];
    z[67]=z[18]*z[67];
    z[63]=z[71] + z[63] + z[67] - z[34];
    z[67]=z[7] + z[6];
    z[71]=z[57]*z[67];
    z[71]=z[23] + z[71];
    z[71]=z[14]*z[71];
    z[67]= - z[57] + z[67];
    z[67]=z[22]*z[67];
    z[67]=z[71] + z[67];
    z[64]= - n<T>(25,18)*z[6] - n<T>(2,3)*z[65] - z[64];
    z[64]=z[6]*z[64];
    z[65]= - n<T>(5,3)*z[9] - n<T>(4,3)*z[57] - z[4];
    z[65]=z[9]*z[65];
    z[64]=z[64] + z[70] + z[65];
    z[64]=z[6]*z[64];
    z[65]=2*z[66];
    z[69]=z[65] + z[69];
    z[69]=z[9]*z[69];
    z[70]=z[9] + z[4];
    z[71]= - 5*z[8] - n<T>(1,2)*z[57] + z[70];
    z[71]=z[8]*z[71];
    z[65]= - z[4]*z[65];
    z[65]=z[71] + z[65] + z[69];
    z[69]=z[6]*z[70];
    z[70]= - z[9] + z[73];
    z[70]=z[5]*z[70];
    z[65]=z[70] + z[69] + n<T>(1,3)*z[65];
    z[65]=z[8]*z[65];
    z[69]= - 11*z[9] - z[57] - n<T>(5,2)*z[4];
    z[69]=z[9]*z[69];
    z[58]= - 5*z[58] + z[69];
    z[69]=n<T>(1,3)*z[9];
    z[58]=z[58]*z[69];
    z[70]= - z[66]*npow(z[4],2);
    z[58]=z[37] - z[53] - z[70] - z[58] + z[45] - z[43];
    z[70]=z[2] - z[7];
    z[71]=z[8] + n<T>(4,3)*z[66] - z[9];
    z[70]= - n<T>(31,3)*z[5] - n<T>(23,3)*z[6] + 2*z[71] + n<T>(2,3)*z[70];
    z[70]=z[20]*z[70];
    z[71]= - z[4] - z[5];
    z[57]=z[57]*z[71];
    z[57]= - z[23] + z[57];
    z[57]=z[13]*z[57];
    z[69]= - n<T>(7,2)*z[4] - z[69];
    z[69]=z[10] + 2*z[3] - z[8] - n<T>(8,3)*z[2] + n<T>(11,12)*z[7] - n<T>(19,2)*
    z[5] + n<T>(1,2)*z[69] + n<T>(4,3)*z[6];
    z[69]=z[23]*z[69];
    z[69]=z[69] - z[55] + z[51] + z[36];
    z[71]=z[30] - z[25];
    z[72]=z[6] + z[9];
    z[72]=z[33]*z[72];
    z[72]=z[72] - z[47];
    z[73]= - n<T>(20,3)*z[29] - n<T>(4,3)*z[27] + n<T>(4,9)*z[24];
    z[73]=i*z[73];
    z[66]=z[5] - z[66];
    z[66]=z[19]*z[66];
    z[74]= - z[9] - z[8];
    z[74]=z[31]*z[74];
    z[75]= - z[8] - z[10];
    z[75]=z[32]*z[75];

    r += n<T>(8,9)*z[26] + z[35] - n<T>(1,4)*z[38] + z[40] - n<T>(1,6)*z[41] + n<T>(5,12)*z[42]
     - n<T>(3,4)*z[44]
     + n<T>(29,12)*z[48]
     + z[49]
     + n<T>(32,3)*z[50] - 
      n<T>(7,12)*z[52] + n<T>(9,4)*z[54] + z[56] + z[57] - n<T>(1,2)*z[58] + z[59]
       + z[60] + z[61] + n<T>(2,3)*z[62] + 2*z[63] + z[64] + z[65] + z[66]
       + 3*z[67] + z[68] + n<T>(1,3)*z[69] + z[70] + n<T>(10,3)*z[71] - n<T>(7,3)*
      z[72] + z[73] + n<T>(5,3)*z[74] + z[75];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1325(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1325(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
