#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf705(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[16];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[13];
    z[3]=c[20];
    z[4]=c[23];
    z[5]=f[22];
    z[6]=f[24];
    z[7]=f[42];
    z[8]=f[62];
    z[9]=f[77];
    z[10]=f[79];
    z[11]=f[80];
    z[12]=f[81];
    z[13]= - z[8] + z[10] + z[9];
    z[14]=z[11] - z[7];
    z[15]=n<T>(3,16)*z[4] - n<T>(15,4)*z[3] + n<T>(53,432)*z[1] - 9*z[2];
    z[15]=i*z[15];

    r +=  - n<T>(3,2)*z[5] - n<T>(7,2)*z[6] - n<T>(1,2)*z[12] - n<T>(1,4)*z[13] + n<T>(1,8)
      *z[14] + z[15];
 
    return r;
}

template std::complex<double> qqb_2lha_tf705(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf705(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
