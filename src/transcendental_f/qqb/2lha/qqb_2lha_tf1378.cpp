#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1378(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[28];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[7];
    z[5]=d[3];
    z[6]=e[0];
    z[7]=e[8];
    z[8]=c[3];
    z[9]=c[19];
    z[10]=c[23];
    z[11]=c[25];
    z[12]=c[26];
    z[13]=c[28];
    z[14]=f[3];
    z[15]=f[5];
    z[16]=f[11];
    z[17]=f[17];
    z[18]=i*z[1];
    z[19]=2*z[18];
    z[20]=z[19] - z[5];
    z[21]=n<T>(1,3)*z[5];
    z[22]= - z[20]*z[21];
    z[23]=z[19] + z[5];
    z[24]=n<T>(1,2)*z[4];
    z[25]=z[24] - z[23];
    z[25]=z[4]*z[25];
    z[26]=4*z[6];
    z[27]= - n<T>(1,3)*z[8] + z[26];
    z[21]=z[21] + z[4];
    z[21]=z[3]*z[21];
    z[21]=z[21] + z[25] + z[22] + n<T>(1,3)*z[27] + z[7];
    z[21]=z[3]*z[21];
    z[22]=2*z[7];
    z[23]= - z[4] + z[23];
    z[23]=z[4]*z[23];
    z[18]= - 4*z[3] + 8*z[18] + z[5];
    z[18]=z[3]*z[18];
    z[18]=z[18] + 2*z[23] + z[22] - n<T>(1,2)*z[8] + z[26];
    z[23]=n<T>(1,3)*z[4];
    z[19]=z[2] - n<T>(1,6)*z[3] - z[23] - z[19] - n<T>(1,2)*z[5];
    z[19]=z[2]*z[19];
    z[18]=n<T>(1,3)*z[18] + z[19];
    z[18]=z[2]*z[18];
    z[19]=2*i;
    z[25]= - 2*z[6] - z[7];
    z[25]=z[1]*z[25];
    z[25]=n<T>(2,3)*z[25] + 2*z[12] + z[10];
    z[19]=z[25]*z[19];
    z[20]=z[24] + z[20];
    z[20]=z[5]*z[20];
    z[20]= - n<T>(1,6)*z[8] + z[22] + z[20];
    z[20]=z[20]*z[23];
    z[22]=z[14] - z[16];
    z[23]= - z[5]*z[7];

    r += n<T>(1,6)*z[9] - z[11] - 2*z[13] + z[15] + z[17] + z[18] + z[19]
       + z[20] + z[21] + n<T>(1,3)*z[22] + z[23];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1378(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1378(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
