#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1050(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[76];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[6];
    z[6]=d[7];
    z[7]=d[8];
    z[8]=d[3];
    z[9]=d[4];
    z[10]=d[15];
    z[11]=d[16];
    z[12]=d[5];
    z[13]=d[17];
    z[14]=d[18];
    z[15]=e[0];
    z[16]=e[2];
    z[17]=e[4];
    z[18]=e[8];
    z[19]=e[9];
    z[20]=e[10];
    z[21]=e[11];
    z[22]=c[3];
    z[23]=c[11];
    z[24]=c[15];
    z[25]=c[19];
    z[26]=c[23];
    z[27]=c[25];
    z[28]=e[3];
    z[29]=e[6];
    z[30]=e[7];
    z[31]=f[0];
    z[32]=f[1];
    z[33]=f[2];
    z[34]=f[3];
    z[35]=f[4];
    z[36]=f[5];
    z[37]=f[6];
    z[38]=f[7];
    z[39]=f[9];
    z[40]=f[11];
    z[41]=f[12];
    z[42]=f[13];
    z[43]=f[16];
    z[44]=f[18];
    z[45]=f[23];
    z[46]=f[30];
    z[47]=f[31];
    z[48]=f[35];
    z[49]=f[36];
    z[50]=f[37];
    z[51]=f[39];
    z[52]=f[41];
    z[53]=n<T>(5,3)*z[2];
    z[54]=n<T>(1,2)*z[9];
    z[55]=z[1]*i;
    z[56]=n<T>(7,2)*z[55];
    z[57]=z[56] + 5*z[6];
    z[57]=n<T>(5,3)*z[3] - z[53] + n<T>(1,3)*z[57] - z[54];
    z[57]=n<T>(1,2)*z[57] - n<T>(2,9)*z[7];
    z[57]=z[7]*z[57];
    z[58]=2*z[55];
    z[59]=z[58] - z[6];
    z[60]=n<T>(1,3)*z[6];
    z[59]= - z[59]*z[60];
    z[61]= - z[55] - z[54];
    z[61]=z[61]*z[54];
    z[62]=2*z[6];
    z[63]=z[2] - z[55] - z[62];
    z[64]=n<T>(1,3)*z[2];
    z[63]=z[63]*z[64];
    z[65]=z[2] - z[6];
    z[66]= - z[3] - z[58] - z[65];
    z[67]=n<T>(1,3)*z[3];
    z[66]=z[66]*z[67];
    z[57]=z[57] + z[66] + z[63] + z[59] + z[61];
    z[57]=z[7]*z[57];
    z[59]=n<T>(1,3)*z[8];
    z[61]= - n<T>(1,3)*z[4] - z[5] + z[59] + 3*z[55] - z[64];
    z[61]=z[4]*z[61];
    z[63]=z[54] - z[55];
    z[66]= - z[9]*z[63];
    z[68]=n<T>(1,2)*z[2];
    z[69]=z[55] + z[68];
    z[69]=z[69]*z[64];
    z[70]=n<T>(1,2)*z[8] - z[55] - z[2];
    z[70]=z[70]*z[59];
    z[71]=z[55]*z[5];
    z[61]=n<T>(1,2)*z[61] + z[71] + z[70] + z[66] + z[69];
    z[61]=z[4]*z[61];
    z[66]=n<T>(1,2)*z[6];
    z[69]= - n<T>(11,6)*z[3] + 2*z[2] + z[54] + z[58] - z[66];
    z[69]=z[3]*z[69];
    z[66]=z[66] - z[55];
    z[70]=z[66]*z[6];
    z[72]=z[55] - z[6];
    z[73]=z[68] + z[72];
    z[73]=z[2]*z[73];
    z[74]=z[55] + z[9];
    z[74]=z[9]*z[74];
    z[69]=z[69] + z[73] + z[70] + z[74];
    z[67]=z[69]*z[67];
    z[69]=z[2] - z[66];
    z[69]=z[6]*z[69];
    z[63]= - z[63]*z[54];
    z[65]=z[65] + z[55];
    z[73]= - n<T>(1,2)*z[3] - z[9] - z[65];
    z[73]=z[3]*z[73];
    z[63]=z[73] + z[63] + z[69];
    z[56]=z[56] - z[6];
    z[56]= - n<T>(7,12)*z[7] + n<T>(2,3)*z[3] + z[64] + n<T>(1,3)*z[56] + z[54];
    z[56]=z[7]*z[56];
    z[69]= - z[6] - z[54];
    z[69]= - n<T>(7,4)*z[7] + n<T>(1,2)*z[69] + 2*z[3];
    z[59]=z[69]*z[59];
    z[56]=z[59] + n<T>(1,3)*z[63] + z[56];
    z[56]=z[8]*z[56];
    z[59]=n<T>(1,3)*z[5];
    z[63]= - n<T>(1,2)*z[55] + z[6];
    z[63]= - z[59] + z[68] + n<T>(1,3)*z[63] - z[54];
    z[63]=z[5]*z[63];
    z[62]= - 5*z[55] + z[62];
    z[62]=z[6]*z[62];
    z[69]=2*z[72] + z[9];
    z[69]=z[9]*z[69];
    z[62]=z[62] + z[69];
    z[69]= - z[2]*z[72];
    z[62]=z[63] + n<T>(1,3)*z[62] + z[69];
    z[62]=z[5]*z[62];
    z[54]= - z[54] - z[72];
    z[63]=3*z[9];
    z[54]=z[54]*z[63];
    z[63]= - z[63] + z[72];
    z[69]=z[12] + z[5];
    z[63]=n<T>(1,2)*z[63] + z[69];
    z[63]=z[12]*z[63];
    z[54]=z[63] - z[70] + z[54];
    z[63]=z[5]*z[72];
    z[54]=z[63] + n<T>(1,2)*z[54];
    z[54]=z[12]*z[54];
    z[63]=n<T>(7,3)*z[9];
    z[73]=z[63] + z[55] - n<T>(5,6)*z[6];
    z[73]=z[9]*z[73];
    z[70]= - n<T>(5,3)*z[70] + z[73];
    z[70]=z[9]*z[70];
    z[73]=z[72]*npow(z[6],2);
    z[70]=z[32] - z[50] - z[73] - z[70] + z[39] + z[37];
    z[73]=z[55]*z[6];
    z[73]=z[73] + z[22];
    z[71]= - z[71] - z[73];
    z[71]=z[13]*z[71];
    z[58]=z[58] - z[2];
    z[74]=z[4] - z[58];
    z[74]=z[15]*z[74];
    z[75]= - z[5] + z[72];
    z[75]=z[19]*z[75];
    z[71]=z[71] + z[74] + z[75] - z[46];
    z[64]=z[64] - z[55] - z[60];
    z[64]=z[64]*z[68];
    z[60]=z[66]*z[60];
    z[66]=npow(z[9],2);
    z[60]=z[64] + z[60] + n<T>(1,2)*z[66];
    z[60]=z[2]*z[60];
    z[64]=z[65] - z[8];
    z[64]= - 7*z[7] - 5*z[3] + 2*z[64];
    z[64]=z[20]*z[64];
    z[65]= - z[9] - z[3];
    z[65]=z[28]*z[65];
    z[64]=z[45] + z[44] - z[42] + z[36] - z[33] + z[64] + z[65];
    z[63]=z[6] + z[63];
    z[53]=n<T>(1,2)*z[63] - z[53];
    z[53]=n<T>(1,3)*z[12] + n<T>(35,18)*z[4] - z[59] - n<T>(1,12)*z[8] + n<T>(5,6)*z[7]
    + n<T>(1,2)*z[53] - n<T>(1,9)*z[3];
    z[53]=z[22]*z[53];
    z[59]=z[8] + z[3];
    z[63]= - z[55]*z[59];
    z[63]= - z[22] + z[63];
    z[63]=z[10]*z[63];
    z[65]=z[4] + z[9];
    z[66]= - z[55]*z[65];
    z[66]= - z[22] + z[66];
    z[66]=z[11]*z[66];
    z[68]=z[7]*z[55];
    z[68]=z[68] + z[73];
    z[68]=z[14]*z[68];
    z[59]=z[55] - z[59];
    z[59]=z[16]*z[59];
    z[55]= - z[55] + z[65];
    z[55]=z[17]*z[55];
    z[65]=z[27] + z[40] - z[34];
    z[73]=z[47] + z[43];
    z[74]=z[9] + z[5];
    z[74]=z[29]*z[74];
    z[74]=z[74] + z[24];
    z[75]=n<T>(1,2)*z[26] - n<T>(5,36)*z[23];
    z[75]=i*z[75];
    z[58]= - z[6] + z[58];
    z[58]= - z[5] + n<T>(2,3)*z[58] + z[7];
    z[58]=z[18]*z[58];
    z[72]= - z[7] + z[72];
    z[72]=z[21]*z[72];
    z[69]=z[30]*z[69];

    r += n<T>(5,6)*z[25] + z[31] - n<T>(11,12)*z[35] - n<T>(8,3)*z[38] + n<T>(1,12)*
      z[41] + z[48] + n<T>(5,12)*z[49] - n<T>(3,4)*z[51] - z[52] + z[53] + 
      z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + 
      z[62] + z[63] + n<T>(1,3)*z[64] - n<T>(1,6)*z[65] + z[66] + z[67] + z[68]
       + z[69] - n<T>(1,2)*z[70] + n<T>(2,3)*z[71] + z[72] - n<T>(1,4)*z[73] + n<T>(5,3)
      *z[74] + z[75];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1050(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1050(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
