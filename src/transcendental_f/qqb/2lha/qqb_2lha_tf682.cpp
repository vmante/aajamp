#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf682(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[16];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[6];
    z[3]=d[11];
    z[4]=d[0];
    z[5]=d[2];
    z[6]=d[5];
    z[7]=d[9];
    z[8]=d[3];
    z[9]=e[12];
    z[10]=e[13];
    z[11]=z[5]*z[7];
    z[12]=z[1]*i;
    z[13]=z[3]*z[12];
    z[12]=z[8] - z[12];
    z[12]=z[2]*z[12];
    z[14]=n<T>(1,2)*z[6] - z[8] - z[2];
    z[14]=z[6]*z[14];
    z[15]=n<T>(1,2)*z[4] + z[6] - z[7] - z[5];
    z[15]=z[4]*z[15];

    r +=  - z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15];
 
    return r;
}

template std::complex<double> qqb_2lha_tf682(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf682(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
