#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1501(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[111];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[4];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[6];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[19];
    z[34]=c[23];
    z[35]=c[25];
    z[36]=c[26];
    z[37]=c[28];
    z[38]=c[31];
    z[39]=e[10];
    z[40]=e[3];
    z[41]=e[14];
    z[42]=f[0];
    z[43]=f[1];
    z[44]=f[2];
    z[45]=f[3];
    z[46]=f[4];
    z[47]=f[5];
    z[48]=f[6];
    z[49]=f[11];
    z[50]=f[12];
    z[51]=f[14];
    z[52]=f[15];
    z[53]=f[16];
    z[54]=f[17];
    z[55]=f[18];
    z[56]=f[23];
    z[57]=f[30];
    z[58]=f[31];
    z[59]=f[32];
    z[60]=f[33];
    z[61]=f[35];
    z[62]=f[36];
    z[63]=f[37];
    z[64]=f[38];
    z[65]=f[39];
    z[66]=f[43];
    z[67]=f[50];
    z[68]=f[51];
    z[69]=f[52];
    z[70]=f[53];
    z[71]=f[54];
    z[72]=f[55];
    z[73]=f[56];
    z[74]=f[58];
    z[75]=f[72];
    z[76]=f[73];
    z[77]=f[74];
    z[78]=f[75];
    z[79]=n<T>(1,2)*z[3];
    z[80]=z[1]*i;
    z[81]=z[79] + z[80];
    z[82]=n<T>(1,4)*z[5];
    z[83]=n<T>(1,4)*z[6];
    z[84]=z[83] - z[82] + z[81];
    z[84]=z[3]*z[84];
    z[85]=n<T>(1,2)*z[5];
    z[86]=z[85] - z[80];
    z[87]=z[86]*z[5];
    z[88]=z[80] + z[3];
    z[89]=n<T>(1,2)*z[8];
    z[90]=z[89] - z[88];
    z[91]=n<T>(3,4)*z[8];
    z[90]=z[90]*z[91];
    z[92]=z[6] - z[5];
    z[93]= - n<T>(7,4)*z[9] - n<T>(11,2)*z[80] - z[3] - z[92];
    z[94]=n<T>(1,4)*z[9];
    z[93]=z[93]*z[94];
    z[95]=n<T>(1,2)*z[4];
    z[96]= - n<T>(7,24)*z[4] - n<T>(5,3)*z[6] + n<T>(9,4)*z[9];
    z[96]=z[96]*z[95];
    z[91]= - n<T>(3,8)*z[11] - z[7] - n<T>(3,4)*z[9] + z[91] + n<T>(15,8)*z[6] + n<T>(1,4)*z[80]
     - z[5];
    z[97]=n<T>(1,2)*z[11];
    z[91]=z[91]*z[97];
    z[98]= - n<T>(31,48)*z[6] - n<T>(1,6)*z[80] + z[5];
    z[98]=z[6]*z[98];
    z[99]=n<T>(1,2)*z[7];
    z[100]=z[6] - z[99];
    z[100]=z[7]*z[100];
    z[84]=z[91] + z[96] + z[100] + z[93] + z[90] + z[84] - z[87] + z[98]
   ;
    z[84]=z[11]*z[84];
    z[82]=z[80] + z[82];
    z[82]=5*z[82] - z[83];
    z[90]=n<T>(1,4)*z[3];
    z[82]= - n<T>(5,6)*z[2] + n<T>(1,12)*z[7] - z[94] + n<T>(5,12)*z[8] + n<T>(1,3)*
    z[82] + z[90];
    z[82]=z[2]*z[82];
    z[91]=5*z[5];
    z[93]=z[91]*z[86];
    z[96]=z[80] - z[5];
    z[98]=n<T>(1,2)*z[96];
    z[100]= - z[98] - z[6];
    z[100]=z[6]*z[100];
    z[100]=z[93] + z[100];
    z[101]=z[85] + z[80];
    z[102]=n<T>(1,2)*z[6];
    z[101]= - 5*z[101] + z[102];
    z[101]=n<T>(5,6)*z[8] + n<T>(1,3)*z[101] - z[79];
    z[101]=z[8]*z[101];
    z[103]=n<T>(1,2)*z[9];
    z[104]=z[96] + z[9];
    z[105]=z[8] - z[3] - z[104];
    z[105]=z[105]*z[103];
    z[98]=z[98] + z[3];
    z[98]=z[3]*z[98];
    z[106]=z[96] + z[6];
    z[107]=z[7] - z[8] + z[106];
    z[107]=z[7]*z[107];
    z[82]=z[82] + n<T>(1,6)*z[107] + z[105] + z[101] + n<T>(1,3)*z[100] + z[98];
    z[82]=z[2]*z[82];
    z[98]=z[9] + z[3];
    z[100]=z[2] - z[8];
    z[98]=z[11] - z[6] + z[100] - n<T>(13,3)*z[98];
    z[98]=z[39]*z[98];
    z[101]=z[3] - n<T>(5,6)*z[6] - z[104];
    z[101]= - n<T>(25,24)*z[11] + n<T>(1,2)*z[101] - z[7];
    z[101]=z[29]*z[101];
    z[82]=z[98] + z[101] + z[82] + z[84];
    z[84]= - z[80] + z[102];
    z[98]=n<T>(61,12)*z[6];
    z[84]=z[84]*z[98];
    z[101]=z[79] - z[80];
    z[104]=5*z[3];
    z[105]=z[101]*z[104];
    z[107]=z[5] - z[89] + z[80];
    z[108]=5*z[8];
    z[107]=z[107]*z[108];
    z[109]=11*z[80];
    z[110]= - n<T>(11,2)*z[9] + z[109] - 7*z[6];
    z[94]=z[110]*z[94];
    z[88]=z[99] - z[88];
    z[99]=5*z[7];
    z[88]=z[88]*z[99];
    z[84]=z[88] + z[94] + z[107] + z[105] - z[93] + z[84];
    z[88]=z[99] - n<T>(11,4)*z[9] - z[108] + z[104] + z[98] + n<T>(67,6)*z[80]
    - z[91];
    z[88]= - n<T>(67,12)*z[4] + n<T>(1,2)*z[88] + 5*z[2];
    z[88]=z[88]*z[95];
    z[91]=n<T>(1,2)*z[10];
    z[93]= - z[80] + z[91];
    z[93]=z[10]*z[93];
    z[94]=5*z[80];
    z[98]= - z[2]*z[94];
    z[84]=z[88] + 5*z[93] + n<T>(1,2)*z[84] + z[98];
    z[84]=z[4]*z[84];
    z[88]=n<T>(1,3)*z[5];
    z[93]= - n<T>(73,24)*z[6] + n<T>(27,4)*z[80] - z[88];
    z[93]=z[6]*z[93];
    z[98]=z[90] - n<T>(5,2)*z[80] + z[5];
    z[98]=z[3]*z[98];
    z[99]=3*z[80];
    z[104]= - 3*z[8] + n<T>(5,2)*z[3] - n<T>(27,4)*z[6] + z[99] - n<T>(5,6)*z[5];
    z[104]=z[104]*z[89];
    z[93]=z[104] + z[98] - n<T>(5,6)*z[87] + z[93];
    z[93]=z[8]*z[93];
    z[98]= - 67*z[80] + 61*z[5];
    z[98]=z[98]*z[88];
    z[104]= - 3*z[96] - z[6];
    z[104]=z[6]*z[104];
    z[105]= - 47*z[80] + 73*z[5];
    z[105]=n<T>(1,3)*z[105] - 15*z[6];
    z[105]=n<T>(1,2)*z[105] - 41*z[3];
    z[105]=z[3]*z[105];
    z[98]=z[105] + z[98] + z[104];
    z[79]=z[98]*z[79];
    z[88]= - z[99] + z[88];
    z[88]=z[88]*npow(z[5],2);
    z[98]=n<T>(443,6)*z[6] + n<T>(73,4)*z[80] + 11*z[5];
    z[98]=z[98]*z[102];
    z[99]=z[109] - n<T>(25,4)*z[5];
    z[99]=z[5]*z[99];
    z[98]=z[99] + z[98];
    z[98]=z[6]*z[98];
    z[79]=z[79] + n<T>(1,4)*z[88] + n<T>(1,3)*z[98];
    z[79]=z[56] + z[69] + z[84] + n<T>(1,2)*z[79] + z[93];
    z[84]=n<T>(11,8)*z[6] + n<T>(11,4)*z[80] - z[5];
    z[84]=z[84]*z[83];
    z[88]=n<T>(25,6)*z[3] + n<T>(25,3)*z[96] + z[6];
    z[88]=z[88]*z[90];
    z[85]= - n<T>(1,4)*z[8] - z[85] + z[101];
    z[85]=z[8]*z[85];
    z[84]=z[85] + z[88] - n<T>(5,3)*z[87] + z[84];
    z[85]=n<T>(1,8)*z[8];
    z[88]=n<T>(161,128)*z[80] - z[5];
    z[88]=n<T>(3,64)*z[9] + z[85] + n<T>(77,96)*z[3] + n<T>(1,3)*z[88] + n<T>(27,128)*
    z[6];
    z[88]=z[9]*z[88];
    z[84]=n<T>(1,4)*z[84] + z[88];
    z[84]=z[9]*z[84];
    z[88]= - n<T>(37,4)*z[6] - n<T>(37,2)*z[80] + z[5];
    z[88]=z[6]*z[88];
    z[88]= - n<T>(41,3)*z[87] + z[88];
    z[83]=n<T>(1,6)*z[3] + n<T>(1,3)*z[96] - z[83];
    z[83]=z[3]*z[83];
    z[90]= - n<T>(67,6)*z[8] + n<T>(67,3)*z[80] + 35*z[6];
    z[85]=z[90]*z[85];
    z[90]= - z[103] - z[96];
    z[90]=z[9]*z[90];
    z[83]=n<T>(15,4)*z[90] + z[85] + n<T>(1,4)*z[88] + z[83];
    z[85]=n<T>(3,2)*z[3];
    z[88]= - n<T>(19,6)*z[8] - z[85] + n<T>(5,3)*z[80] - n<T>(3,2)*z[92];
    z[88]=n<T>(1,4)*z[88] - z[7];
    z[88]=z[7]*z[88];
    z[90]=n<T>(149,2)*z[80] - 41*z[5];
    z[90]=n<T>(1,3)*z[90] - n<T>(39,2)*z[6];
    z[90]= - n<T>(15,2)*z[9] - n<T>(67,12)*z[8] + n<T>(1,2)*z[90] + n<T>(43,3)*z[3];
    z[90]=n<T>(9,2)*z[10] - n<T>(5,2)*z[2] + n<T>(1,2)*z[90] + n<T>(1,3)*z[7];
    z[90]=z[10]*z[90];
    z[83]=n<T>(1,4)*z[90] + n<T>(1,2)*z[83] + z[88];
    z[83]=z[83]*z[91];
    z[88]=z[10] - z[3];
    z[88]= - z[97] + n<T>(77,48)*z[7] - n<T>(17,24)*z[96] + 2*z[6] - n<T>(1,12)*
    z[100] + n<T>(1,8)*z[88];
    z[88]=z[24]*z[88];
    z[90]=z[80]*z[5];
    z[90]=z[90] + z[30];
    z[91]=z[6]*z[80];
    z[91]=z[91] + z[90];
    z[91]=z[15]*z[91];
    z[93]= - z[6] + z[96];
    z[93]=z[22]*z[93];
    z[97]= - z[9] - z[11];
    z[97]=z[41]*z[97];
    z[91]= - z[77] + z[91] + z[93] + z[97] - z[78];
    z[93]=n<T>(1,2)*z[2];
    z[86]= - z[93] - z[86];
    z[86]=z[19]*z[86];
    z[93]=z[93] - z[80];
    z[89]= - z[89] - z[93];
    z[89]=z[25]*z[89];
    z[86]=z[86] + z[89];
    z[89]= - n<T>(1,16)*z[6] + z[94] - n<T>(13,4)*z[5];
    z[89]=z[6]*z[89];
    z[87]=n<T>(13,4)*z[87] + z[89];
    z[81]=z[81] + n<T>(3,8)*z[92];
    z[81]=z[3]*z[81];
    z[81]=n<T>(1,3)*z[87] + z[81];
    z[87]= - n<T>(43,2)*z[6] - z[80] + 13*z[5];
    z[85]=n<T>(47,18)*z[7] + n<T>(35,6)*z[8] + n<T>(1,3)*z[87] + z[85];
    z[85]=z[7]*z[85];
    z[87]= - n<T>(35,2)*z[80] + z[5];
    z[87]=n<T>(37,16)*z[8] + n<T>(1,8)*z[87] - z[6];
    z[87]=z[8]*z[87];
    z[81]=n<T>(1,16)*z[85] + n<T>(1,2)*z[81] + n<T>(1,3)*z[87];
    z[81]=z[7]*z[81];
    z[85]=z[80]*z[4];
    z[85]=z[85] + z[30];
    z[87]=z[10]*z[80];
    z[87]=z[87] + z[85];
    z[87]=z[14]*z[87];
    z[89]=z[80] - z[4];
    z[92]= - z[10] + z[89];
    z[92]=z[21]*z[92];
    z[87]= - z[42] + z[87] + z[92];
    z[92]=z[80]*z[8];
    z[92]=z[92] + z[30];
    z[94]=z[9]*z[80];
    z[94]=z[94] + z[92];
    z[94]=z[17]*z[94];
    z[97]=z[80] - z[8];
    z[98]= - z[9] + z[97];
    z[98]=z[27]*z[98];
    z[94]=z[94] + z[98];
    z[98]= - z[7]*z[80];
    z[92]=z[98] - z[92];
    z[92]=z[16]*z[92];
    z[97]= - z[7] + z[97];
    z[97]=z[26]*z[97];
    z[92]=z[92] + z[97];
    z[97]=z[11]*z[80];
    z[85]=z[97] + z[85];
    z[85]=z[13]*z[85];
    z[89]= - z[11] + z[89];
    z[89]=z[28]*z[89];
    z[85]=z[85] + z[89];
    z[80]= - z[3]*z[80];
    z[80]=z[80] - z[90];
    z[80]=z[12]*z[80];
    z[89]=z[96] - z[3];
    z[89]=z[20]*z[89];
    z[80]=z[80] + z[89];
    z[89]= - 67*z[5] + 263*z[6];
    z[89]= - n<T>(649,6)*z[7] + n<T>(205,3)*z[9] + n<T>(25,3)*z[8] + n<T>(1,3)*z[89]
     - 
   73*z[3];
    z[89]=n<T>(665,96)*z[10] + n<T>(1,32)*z[89] - n<T>(5,3)*z[2];
    z[89]=n<T>(25,72)*z[11] + n<T>(1,3)*z[89] + n<T>(29,64)*z[4];
    z[89]=z[30]*z[89];
    z[90]=n<T>(19,3)*z[7] + z[3] + z[106];
    z[90]=n<T>(1,2)*z[90] + n<T>(11,3)*z[10];
    z[90]=z[23]*z[90];
    z[89]=z[89] + z[90];
    z[90]= - z[95] - z[93];
    z[90]=z[18]*z[90];
    z[93]=z[45] - z[63] + z[59] + z[54] - z[51];
    z[95]= - z[67] + z[53] + z[48] - z[32];
    z[96]= - z[71] + z[55] + z[47] - z[35];
    z[97]=z[50] - z[49];
    z[98]=z[76] + z[70];
    z[99]= - n<T>(25,12)*z[36] - n<T>(5,12)*z[34] - n<T>(275,1152)*z[31];
    z[99]=i*z[99];
    z[100]= - z[3] - z[10];
    z[100]=z[40]*z[100];

    r += n<T>(5,18)*z[33] + n<T>(25,24)*z[37] - n<T>(7,8)*z[38] + n<T>(5,16)*z[43]
     - n<T>(17,96)*z[44]
     - n<T>(25,48)*z[46]
     + n<T>(15,32)*z[52] - n<T>(19,48)*z[57] - n<T>(89,192)*z[58] - n<T>(29,24)*z[60]
     + n<T>(7,24)*z[61]
     + n<T>(67,192)*z[62]
     +  z[64] - n<T>(53,64)*z[65] + n<T>(1,24)*z[66] + n<T>(61,384)*z[68] + n<T>(11,128)*
      z[72] + n<T>(3,32)*z[73] + n<T>(27,128)*z[74] - n<T>(3,16)*z[75] + n<T>(1,8)*
      z[79] + n<T>(55,96)*z[80] + z[81] + n<T>(1,4)*z[82] + z[83] + z[84] + n<T>(7,96)*z[85]
     + n<T>(5,12)*z[86]
     + n<T>(5,8)*z[87]
     + z[88]
     + n<T>(1,2)*z[89]
     + n<T>(5,4)*z[90]
     + n<T>(1,32)*z[91]
     + n<T>(13,16)*z[92] - n<T>(5,32)*z[93]
     + n<T>(3,8)*
      z[94] - n<T>(9,32)*z[95] - n<T>(5,24)*z[96] + n<T>(5,96)*z[97] - n<T>(1,16)*z[98]
       + z[99] + n<T>(67,48)*z[100];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1501(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1501(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
