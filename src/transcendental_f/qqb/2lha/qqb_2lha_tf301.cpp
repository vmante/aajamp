#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf301(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[12];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=f[22];
    z[4]=f[24];
    z[5]=f[42];
    z[6]=f[61];
    z[7]=f[62];
    z[8]=f[70];
    z[9]=f[81];
    z[10]=z[7] + z[3];
    z[11]=i*z[1];

    r += n<T>(3,2)*z[2] + n<T>(8,3)*z[4] - z[5] + n<T>(5,3)*z[6] + n<T>(1,4)*z[8]
     +  z[9] + n<T>(4,3)*z[10] - n<T>(1,18)*z[11];
 
    return r;
}

template std::complex<double> qqb_2lha_tf301(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf301(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
