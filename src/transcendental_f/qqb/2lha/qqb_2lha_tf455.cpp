#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf455(
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[4];
  std::complex<T> r(0,0);

    z[1]=f[44];
    z[2]=f[61];

    r += z[1] - z[2];
 
    return r;
}

template std::complex<double> qqb_2lha_tf455(
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf455(
  const std::array<std::complex<dd_real>,111>& f
);
#endif
