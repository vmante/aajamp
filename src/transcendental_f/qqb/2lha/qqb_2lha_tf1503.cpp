#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1503(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[31];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=e[2];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[17];
    z[12]=c[31];
    z[13]=e[3];
    z[14]=e[10];
    z[15]=f[2];
    z[16]=f[4];
    z[17]=f[12];
    z[18]=f[16];
    z[19]=f[19];
    z[20]=npow(z[2],2);
    z[21]=n<T>(1,3)*z[4];
    z[22]= - n<T>(1,2)*z[2] + z[21];
    z[22]=z[4]*z[22];
    z[22]=n<T>(5,4)*z[20] + z[22];
    z[22]=z[4]*z[22];
    z[23]=n<T>(7,4)*z[4];
    z[24]= - z[2] - z[23];
    z[24]=z[4]*z[24];
    z[24]= - z[20] + z[24];
    z[25]= - z[2] - n<T>(7,2)*z[4];
    z[25]=n<T>(1,2)*z[25] - z[3];
    z[25]=z[3]*z[25];
    z[24]=n<T>(1,2)*z[24] + z[25];
    z[25]=z[1]*i;
    z[24]=z[24]*z[25];
    z[26]=npow(z[2],3);
    z[27]=n<T>(1,3)*z[2];
    z[28]=z[27] + n<T>(1,4)*z[4];
    z[29]=n<T>(5,12)*z[5] - n<T>(5,8)*z[3] - z[28];
    z[29]=z[8]*z[29];
    z[30]=z[2] + z[5];
    z[30]=z[14]*z[30];
    z[22]=z[19] + z[24] + n<T>(7,6)*z[26] + z[22] + z[29] + z[30];
    z[24]=npow(z[4],2);
    z[24]= - n<T>(1,3)*z[20] - n<T>(1,4)*z[24];
    z[26]=n<T>(7,12)*z[3] - z[28];
    z[26]=z[26]*z[25];
    z[29]=z[25] - z[3];
    z[27]= - z[27] - n<T>(1,8)*z[4] - n<T>(11,24)*z[29];
    z[27]=z[5]*z[27];
    z[28]= - n<T>(7,24)*z[3] + z[28];
    z[28]=z[3]*z[28];
    z[24]=z[27] + z[26] + n<T>(1,2)*z[24] + z[28];
    z[24]=z[5]*z[24];
    z[23]= - 5*z[2] + z[23];
    z[21]=z[23]*z[21];
    z[20]= - z[20] + z[21];
    z[21]=n<T>(1,3)*z[3];
    z[23]=n<T>(7,8)*z[4] + z[21];
    z[21]=z[23]*z[21];
    z[20]=n<T>(1,2)*z[20] + z[21];
    z[20]=z[3]*z[20];
    z[21]=z[4] + n<T>(1,6)*z[3] + n<T>(2,3)*z[2];
    z[23]=z[21]*z[25];
    z[23]=z[23] + n<T>(1,6)*z[8];
    z[23]=z[6]*z[23];
    z[21]= - n<T>(1,6)*z[25] + z[21];
    z[21]=z[7]*z[21];
    z[25]= - z[2] - z[4];
    z[25]=z[13]*z[25];
    z[25]=z[25] - z[15];
    z[26]=n<T>(3,4)*z[11] + n<T>(5,36)*z[9];
    z[26]=i*z[26];

    r +=  - n<T>(13,24)*z[10] + n<T>(15,16)*z[12] + n<T>(11,24)*z[16] + n<T>(1,24)*
      z[17] - n<T>(1,8)*z[18] + z[20] + z[21] + n<T>(1,3)*z[22] + z[23] + z[24]
       + n<T>(1,6)*z[25] + z[26];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1503(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1503(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
