#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf755(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[64];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[7];
    z[6]=d[8];
    z[7]=d[3];
    z[8]=d[4];
    z[9]=d[15];
    z[10]=d[16];
    z[11]=d[18];
    z[12]=e[0];
    z[13]=e[1];
    z[14]=e[2];
    z[15]=e[4];
    z[16]=e[10];
    z[17]=e[11];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[19];
    z[22]=c[21];
    z[23]=c[23];
    z[24]=c[25];
    z[25]=c[26];
    z[26]=c[31];
    z[27]=e[3];
    z[28]=f[0];
    z[29]=f[1];
    z[30]=f[2];
    z[31]=f[3];
    z[32]=f[4];
    z[33]=f[5];
    z[34]=f[6];
    z[35]=f[7];
    z[36]=f[10];
    z[37]=f[11];
    z[38]=f[12];
    z[39]=f[14];
    z[40]=f[16];
    z[41]=f[17];
    z[42]=f[18];
    z[43]=f[21];
    z[44]=f[23];
    z[45]=z[4] + z[8];
    z[46]=z[1]*i;
    z[47]= - z[46]*z[45];
    z[47]= - z[18] + z[47];
    z[47]=z[10]*z[47];
    z[48]=z[5] + z[6];
    z[49]=z[46]*z[48];
    z[49]=z[18] + z[49];
    z[49]=z[11]*z[49];
    z[45]= - z[46] + z[45];
    z[45]=z[15]*z[45];
    z[48]=z[46] - z[48];
    z[48]=z[17]*z[48];
    z[45]= - z[45] - z[47] - z[49] - z[48] + z[43] + z[34] - z[28];
    z[47]=z[46] - z[7];
    z[48]= - n<T>(3,2)*z[47];
    z[49]=2*z[3];
    z[50]=n<T>(1,2)*z[6];
    z[51]=z[50] - z[49] + z[48];
    z[51]=z[5]*z[51];
    z[52]=n<T>(1,2)*z[3];
    z[53]=z[46] + n<T>(13,2)*z[3];
    z[53]=z[53]*z[52];
    z[54]= - z[3] - z[47];
    z[54]=2*z[54] - z[50];
    z[54]=z[6]*z[54];
    z[55]= - z[7]*z[49];
    z[56]=z[2] - z[6] + 7*z[3] - 3*z[7];
    z[56]=z[2]*z[56];
    z[51]=n<T>(1,4)*z[56] + z[51] + z[54] + z[53] + z[55];
    z[51]=z[2]*z[51];
    z[53]=n<T>(1,2)*z[7];
    z[54]=z[53] - z[46];
    z[54]=z[54]*z[53];
    z[55]=n<T>(1,2)*z[5];
    z[56]=z[55] - z[46] - z[7];
    z[56]=z[56]*z[55];
    z[57]=n<T>(1,2)*z[8];
    z[58]=z[46] - z[57];
    z[58]=z[8]*z[58];
    z[59]=z[46]*z[3];
    z[60]=z[2]*z[46];
    z[54]=z[58] + z[60] + z[56] + z[59] + z[54];
    z[56]=3*z[46];
    z[55]= - z[2] + z[55] + z[53] + z[56] - z[3];
    z[55]=3*z[55] - z[4];
    z[58]=n<T>(1,2)*z[4];
    z[55]=z[55]*z[58];
    z[54]=3*z[54] + z[55];
    z[54]=z[54]*z[58];
    z[55]=z[50] - 2*z[7] + n<T>(1,2)*z[46] + z[49];
    z[55]=z[6]*z[55];
    z[60]= - n<T>(3,4)*z[46] + n<T>(3,8)*z[7];
    z[49]=z[49] - z[60];
    z[49]=z[7]*z[49];
    z[61]= - 2*z[46] - z[3];
    z[61]=z[3]*z[61];
    z[62]= - n<T>(3,4)*z[5] - n<T>(1,4)*z[6] + z[3] - z[60];
    z[62]=z[5]*z[62];
    z[49]=z[62] + z[55] + z[61] + z[49];
    z[49]=z[5]*z[49];
    z[55]= - z[46] + n<T>(1,2)*z[2];
    z[58]=z[58] + z[55];
    z[58]=z[12]*z[58];
    z[53]= - z[53] - z[55];
    z[53]=z[13]*z[53];
    z[53]= - z[30] + z[58] + z[53];
    z[55]=n<T>(31,4)*z[7];
    z[58]=5*z[3];
    z[61]= - z[55] + n<T>(31,2)*z[46] + z[58];
    z[61]=z[7]*z[61];
    z[62]=z[52] + z[46];
    z[63]= - z[62]*z[58];
    z[61]=z[63] + z[61];
    z[55]= - z[55] + n<T>(31,4)*z[46] + 13*z[3];
    z[55]=n<T>(1,2)*z[55] - n<T>(1,3)*z[6];
    z[55]=z[6]*z[55];
    z[55]=n<T>(1,2)*z[61] + z[55];
    z[55]=z[6]*z[55];
    z[61]=z[3]*z[62];
    z[50]= - z[50] - z[47];
    z[50]=z[6]*z[50];
    z[60]= - z[3] - z[60];
    z[60]=z[7]*z[60];
    z[50]=n<T>(7,4)*z[50] + z[61] + z[60];
    z[48]=z[2] - n<T>(7,2)*z[6] + z[58] - z[48];
    z[48]=n<T>(3,2)*z[48] + z[8];
    z[48]=z[48]*z[57];
    z[48]=3*z[50] + z[48];
    z[48]=z[8]*z[48];
    z[50]= - z[7]*z[46];
    z[50]= - z[18] - z[59] + z[50];
    z[50]=z[9]*z[50];
    z[57]= - z[3] + z[47];
    z[57]=z[14]*z[57];
    z[50]=z[20] + z[50] + z[57];
    z[57]= - z[25] + z[22];
    z[59]=n<T>(3,2)*i;
    z[57]=z[59]*z[57];
    z[46]= - 9*z[46] + z[52];
    z[46]=z[46]*z[52];
    z[52]=z[7]*z[58];
    z[46]=z[46] + z[52];
    z[46]=z[7]*z[46];
    z[52]=z[31] + z[41] - z[37];
    z[58]= - z[29] + z[39] + z[33];
    z[59]= - z[44] - z[42] + z[36] - z[21];
    z[56]=z[56] - n<T>(77,12)*z[3];
    z[56]=z[56]*npow(z[3],2);
    z[60]= - n<T>(7,2)*z[3] - z[7];
    z[60]=n<T>(23,8)*z[4] + n<T>(11,8)*z[8] - z[2] + n<T>(1,8)*z[5] + n<T>(1,3)*z[60]
     + 
   n<T>(11,2)*z[6];
    z[60]=z[18]*z[60];
    z[47]= - z[5] + z[47] + z[2];
    z[47]= - n<T>(31,2)*z[6] - n<T>(23,2)*z[3] + 4*z[47];
    z[47]=z[16]*z[47];
    z[61]= - z[3] - z[8];
    z[61]=z[27]*z[61];
    z[62]=z[19]*i;
    z[63]=n<T>(3,4)*i;
    z[63]= - z[23]*z[63];

    r +=  - n<T>(1,4)*z[24] + n<T>(7,4)*z[26] - n<T>(35,8)*z[32] - 16*z[35] + n<T>(9,8)
      *z[38] - n<T>(21,8)*z[40] - n<T>(3,2)*z[45] + z[46] + z[47] + z[48] + 
      z[49] + n<T>(11,2)*z[50] + z[51] + n<T>(3,8)*z[52] + 3*z[53] + z[54] + 
      z[55] + z[56] + z[57] + n<T>(3,4)*z[58] - n<T>(1,2)*z[59] + z[60] + n<T>(9,2)
      *z[61] - n<T>(1,12)*z[62] + z[63];
 
    return r;
}

template std::complex<double> qqb_2lha_tf755(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf755(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
