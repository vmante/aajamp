#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1334(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[31];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=e[2];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[17];
    z[12]=c[31];
    z[13]=e[3];
    z[14]=e[10];
    z[15]=f[2];
    z[16]=f[4];
    z[17]=f[10];
    z[18]=f[12];
    z[19]=f[16];
    z[20]=f[19];
    z[21]=n<T>(3,2)*z[4];
    z[22]=n<T>(1,2)*z[4];
    z[23]=z[3] - z[22];
    z[23]=z[23]*z[21];
    z[24]=z[2] + n<T>(1,2)*z[3];
    z[25]= - z[21] + z[24];
    z[26]=z[1]*i;
    z[25]=z[25]*z[26];
    z[27]=n<T>(1,4)*z[3];
    z[28]=n<T>(1,4)*z[26] - n<T>(3,4)*z[4] + z[2] - z[27];
    z[28]=z[5]*z[28];
    z[29]=npow(z[2],2);
    z[30]=n<T>(1,2)*z[29];
    z[27]= - z[2] - z[27];
    z[27]=z[3]*z[27];
    z[23]=z[28] + z[25] + z[23] + z[30] + z[27];
    z[23]=z[5]*z[23];
    z[25]=n<T>(1,2)*z[2];
    z[21]= - z[3] + z[25] - z[21];
    z[27]= - z[26] - z[21];
    z[27]=z[7]*z[27];
    z[28]= - z[2] - z[5];
    z[28]=z[14]*z[28];
    z[23]=z[23] + z[27] + z[28] + z[17];
    z[27]=n<T>(1,2)*z[6];
    z[21]= - z[21]*z[27];
    z[25]= - z[25] - z[3];
    z[25]=z[3]*z[25];
    z[22]= - z[22] + z[2] - z[3];
    z[22]=z[4]*z[22];
    z[21]=z[21] + n<T>(1,4)*z[22] - z[30] + z[25];
    z[21]=z[26]*z[21];
    z[22]= - z[2] + n<T>(1,8)*z[3];
    z[22]=z[3]*z[22];
    z[24]=n<T>(1,4)*z[24] + n<T>(1,3)*z[4];
    z[24]=z[4]*z[24];
    z[22]=z[24] + z[30] + z[22];
    z[22]=z[4]*z[22];
    z[24]=npow(z[3],2);
    z[24]=n<T>(3,4)*z[29] + n<T>(1,3)*z[24];
    z[24]=z[3]*z[24];
    z[25]=n<T>(9,4)*z[11] + n<T>(1,24)*z[9];
    z[25]=i*z[25];
    z[26]=npow(z[2],3);
    z[27]=z[27] + n<T>(1,6)*z[5] - n<T>(1,3)*z[2] - n<T>(5,8)*z[3];
    z[27]=z[8]*z[27];
    z[28]= - z[2] - z[4];
    z[28]=z[13]*z[28];

    r += n<T>(5,8)*z[10] - n<T>(9,16)*z[12] + z[15] - n<T>(1,8)*z[16] + n<T>(7,8)*z[18]
       - n<T>(3,8)*z[19] + z[20] + z[21] + z[22] + n<T>(1,2)*z[23] + z[24] + 
      z[25] - n<T>(13,12)*z[26] + z[27] + n<T>(5,4)*z[28];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1334(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1334(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
