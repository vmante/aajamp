#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf691(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[15];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[13];
    z[3]=c[20];
    z[4]=c[23];
    z[5]=f[22];
    z[6]=f[24];
    z[7]=f[42];
    z[8]=f[62];
    z[9]=f[77];
    z[10]=f[79];
    z[11]=f[80];
    z[12]=f[81];
    z[13]= - z[10] + z[8] + z[9];
    z[13]=z[5] - n<T>(1,4)*z[7] - z[12] - n<T>(3,4)*z[11] - n<T>(1,2)*z[13];
    z[13]=z[6] + n<T>(1,4)*z[13];
    z[14]= - n<T>(1,20)*z[4] + z[3];
    z[14]=n<T>(1,4)*z[14] + n<T>(3,5)*z[2];
    z[14]=27*z[14] - n<T>(53,240)*z[1];
    z[14]=i*z[14];

    r += 3*z[13] + n<T>(1,2)*z[14];
 
    return r;
}

template std::complex<double> qqb_2lha_tf691(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf691(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
