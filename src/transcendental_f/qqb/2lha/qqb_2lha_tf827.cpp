#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf827(
  const std::array<std::complex<T>,25>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[3];
  std::complex<T> r(0,0);

    z[1]=d[1];

    r += 1 - n<T>(99,80)*z[1];
 
    return r;
}

template std::complex<double> qqb_2lha_tf827(
  const std::array<std::complex<double>,25>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf827(
  const std::array<std::complex<dd_real>,25>& d
);
#endif
