#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf611(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[71];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=d[5];
    z[7]=d[8];
    z[8]=d[9];
    z[9]=d[4];
    z[10]=d[6];
    z[11]=e[0];
    z[12]=e[1];
    z[13]=e[8];
    z[14]=c[3];
    z[15]=c[15];
    z[16]=c[18];
    z[17]=c[19];
    z[18]=c[23];
    z[19]=c[25];
    z[20]=c[26];
    z[21]=c[28];
    z[22]=c[29];
    z[23]=c[31];
    z[24]=e[6];
    z[25]=e[7];
    z[26]=e[13];
    z[27]=e[14];
    z[28]=f[0];
    z[29]=f[3];
    z[30]=f[8];
    z[31]=f[11];
    z[32]=f[13];
    z[33]=f[17];
    z[34]=f[20];
    z[35]=f[30];
    z[36]=f[31];
    z[37]=f[32];
    z[38]=f[35];
    z[39]=f[36];
    z[40]=f[39];
    z[41]=f[43];
    z[42]=f[50];
    z[43]=f[51];
    z[44]=f[52];
    z[45]=f[54];
    z[46]=f[55];
    z[47]=f[58];
    z[48]=f[83];
    z[49]=z[1]*i;
    z[50]=z[49]*z[3];
    z[51]=2*z[50];
    z[52]=npow(z[3],2);
    z[53]= - z[52] + z[51];
    z[54]=3*z[5];
    z[55]=2*z[49];
    z[56]=3*z[3] + z[55] - z[5];
    z[56]=z[56]*z[54];
    z[57]= - 23*z[3] - 22*z[49];
    z[57]=n<T>(16,3)*z[2] + n<T>(1,3)*z[57] - z[54];
    z[57]=z[2]*z[57];
    z[54]=n<T>(1,6)*z[4] + n<T>(11,3)*z[2] - z[54] - n<T>(2,3)*z[3] - n<T>(1,2)*z[49];
    z[54]=z[4]*z[54];
    z[53]=z[54] + z[57] + n<T>(2,3)*z[53] + z[56];
    z[53]=z[4]*z[53];
    z[54]=n<T>(1,2)*z[6];
    z[56]=z[54] + z[49];
    z[57]=z[56] - z[3];
    z[58]=z[6]*z[57];
    z[59]=n<T>(1,2)*z[52];
    z[60]=n<T>(1,2)*z[2];
    z[61]=z[3] - z[60];
    z[61]=z[2]*z[61];
    z[62]=z[8] + z[3] - z[2];
    z[62]=z[8]*z[62];
    z[51]=n<T>(1,2)*z[62] + z[58] + z[61] + z[59] - z[51];
    z[51]=z[8]*z[51];
    z[58]=n<T>(1,2)*z[5];
    z[61]=z[58] - z[49];
    z[61]=z[61]*z[5];
    z[59]=z[59] - z[50];
    z[62]= - z[3] + z[5];
    z[62]=z[62]*z[54];
    z[62]=z[62] + z[61] - z[59];
    z[54]=z[62]*z[54];
    z[62]=z[55] - z[58];
    z[62]=z[5]*z[62];
    z[56]=z[56] - z[5];
    z[63]= - z[6]*z[56];
    z[60]= - z[5] + z[60];
    z[60]=z[2]*z[60];
    z[64]= - z[10] - z[5] + z[2];
    z[64]=z[10]*z[64];
    z[60]=n<T>(1,2)*z[64] + z[63] + z[62] + z[60];
    z[60]=z[10]*z[60];
    z[62]=3*z[6];
    z[57]= - z[57]*z[62];
    z[63]=z[49] - z[3];
    z[64]= - z[62] + z[63];
    z[65]=z[7] + z[8];
    z[64]=n<T>(1,2)*z[64] + z[65];
    z[64]=z[7]*z[64];
    z[57]=z[64] + z[57] - z[59];
    z[63]=z[8]*z[63];
    z[57]=z[63] + n<T>(1,2)*z[57];
    z[57]=z[7]*z[57];
    z[56]=z[56]*z[62];
    z[63]=z[49] - z[5];
    z[62]=z[62] - z[63];
    z[64]=z[9] + z[10];
    z[62]=n<T>(1,2)*z[62] - z[64];
    z[62]=z[9]*z[62];
    z[56]=z[62] + z[61] + z[56];
    z[61]= - z[10]*z[63];
    z[56]=z[61] + n<T>(1,2)*z[56];
    z[56]=z[9]*z[56];
    z[50]=n<T>(31,2)*z[52] - 28*z[50];
    z[61]= - 16*z[49] + n<T>(15,2)*z[5];
    z[61]=z[5]*z[61];
    z[62]=5*z[3] + 23*z[49];
    z[62]= - n<T>(13,3)*z[2] + n<T>(1,3)*z[62] + z[5];
    z[62]=z[2]*z[62];
    z[50]=2*z[62] + n<T>(1,3)*z[50] + z[61];
    z[50]=z[2]*z[50];
    z[52]=z[52]*z[49];
    z[58]= - z[3]*z[58];
    z[58]=z[58] - z[59];
    z[58]=z[5]*z[58];
    z[59]=z[10] - z[8];
    z[61]=z[9] - z[7];
    z[59]= - n<T>(11,72)*z[4] + n<T>(1,36)*z[2] + n<T>(20,9)*z[3] + n<T>(3,4)*z[5]
     - n<T>(1,3)*z[61]
     + n<T>(5,8)*z[59];
    z[59]=z[14]*z[59];
    z[61]= - z[46] - z[43] + z[39] + z[36];
    z[62]=z[44] - z[37];
    z[63]=z[47] - z[40];
    z[66]=npow(z[3],3);
    z[66]=z[66] - z[29];
    z[67]= - n<T>(170,3)*z[20] - n<T>(46,3)*z[18];
    z[67]=i*z[67];
    z[49]= - n<T>(2,3)*z[4] - n<T>(5,3)*z[2] + z[3] + n<T>(4,3)*z[49];
    z[49]=z[12]*z[49];
    z[55]=z[55] - z[2];
    z[68]= - z[3] + z[55];
    z[68]=z[11]*z[68];
    z[55]= - z[5] + z[55];
    z[55]=z[13]*z[55];
    z[64]= - z[24]*z[64];
    z[69]= - z[6] - z[10];
    z[69]=z[25]*z[69];
    z[70]=z[6] + z[8];
    z[70]=z[26]*z[70];
    z[65]=z[27]*z[65];

    r += n<T>(2,3)*z[15] + n<T>(8,3)*z[16] - n<T>(1,36)*z[17] + n<T>(22,3)*z[19]
     + n<T>(85,3)*z[21] - 2*z[22]
     + 15*z[23] - 11*z[28] - 7*z[30] - 3*z[31] - n<T>(14,3)*z[32] - 5*z[33]
     + n<T>(4,3)*z[34]
     + z[35] - z[38]
     + z[41] - z[42]
       + z[45] + z[48] + 4*z[49] + z[50] + z[51] + z[52] + z[53] + 
      z[54] + 13*z[55] + z[56] + z[57] + 9*z[58] + z[59] + z[60] - n<T>(1,4)
      *z[61] + n<T>(3,2)*z[62] - n<T>(3,4)*z[63] + z[64] + z[65] - n<T>(1,3)*z[66]
       + z[67] + n<T>(38,3)*z[68] + z[69] + z[70];
 
    return r;
}

template std::complex<double> qqb_2lha_tf611(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf611(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
