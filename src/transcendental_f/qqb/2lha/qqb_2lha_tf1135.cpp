#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1135(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[109];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[5];
    z[8]=d[6];
    z[9]=d[7];
    z[10]=d[8];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[6];
    z[24]=e[7];
    z[25]=e[8];
    z[26]=e[9];
    z[27]=e[10];
    z[28]=e[11];
    z[29]=e[12];
    z[30]=e[13];
    z[31]=c[3];
    z[32]=c[11];
    z[33]=c[15];
    z[34]=c[18];
    z[35]=c[19];
    z[36]=c[23];
    z[37]=c[25];
    z[38]=c[26];
    z[39]=c[28];
    z[40]=c[29];
    z[41]=c[31];
    z[42]=e[3];
    z[43]=e[14];
    z[44]=f[0];
    z[45]=f[1];
    z[46]=f[2];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[8];
    z[53]=f[9];
    z[54]=f[11];
    z[55]=f[12];
    z[56]=f[13];
    z[57]=f[14];
    z[58]=f[16];
    z[59]=f[17];
    z[60]=f[18];
    z[61]=f[23];
    z[62]=f[30];
    z[63]=f[31];
    z[64]=f[32];
    z[65]=f[33];
    z[66]=f[35];
    z[67]=f[36];
    z[68]=f[37];
    z[69]=f[38];
    z[70]=f[39];
    z[71]=f[41];
    z[72]=f[43];
    z[73]=f[48];
    z[74]=f[49];
    z[75]=f[50];
    z[76]=f[51];
    z[77]=f[52];
    z[78]=f[53];
    z[79]=f[54];
    z[80]=f[55];
    z[81]=f[56];
    z[82]=f[58];
    z[83]=f[60];
    z[84]=f[85];
    z[85]=f[86];
    z[86]=z[1]*i;
    z[87]=9*z[86];
    z[88]=n<T>(1,2)*z[2];
    z[89]=n<T>(1,2)*z[8];
    z[90]=n<T>(1,2)*z[9];
    z[91]= - n<T>(9,2)*z[6] + z[88] + z[89] + z[87] - z[90];
    z[91]=z[6]*z[91];
    z[92]=n<T>(7,12)*z[10];
    z[93]=n<T>(1,2)*z[6];
    z[94]= - n<T>(769,12)*z[5] + n<T>(167,3)*z[86] - n<T>(233,4)*z[9];
    z[94]= - n<T>(17,6)*z[4] + n<T>(9,4)*z[7] - z[93] - z[92] + n<T>(367,6)*z[2]
     + n<T>(1,2)*z[94] - 9*z[8];
    z[95]=n<T>(1,2)*z[4];
    z[94]=z[94]*z[95];
    z[96]=z[90] - z[86];
    z[96]=z[96]*z[9];
    z[97]=n<T>(131,6)*z[5] + n<T>(769,3)*z[86] + 233*z[9];
    z[97]=z[5]*z[97];
    z[97]= - 233*z[96] + z[97];
    z[98]=n<T>(1,4)*z[8] + z[87] + z[90];
    z[98]=z[8]*z[98];
    z[99]= - 367*z[86] - n<T>(415,2)*z[5];
    z[99]=n<T>(415,12)*z[2] + n<T>(1,3)*z[99] - z[8];
    z[99]=z[99]*z[88];
    z[100]=n<T>(1,2)*z[10];
    z[101]=z[86] - z[100];
    z[92]=z[101]*z[92];
    z[101]=n<T>(1,2)*z[7];
    z[87]=z[101] - z[87] - 5*z[10];
    z[87]=z[7]*z[87];
    z[87]=z[94] + n<T>(1,4)*z[87] + z[91] + z[92] + z[99] + n<T>(1,8)*z[97] + 
    z[98];
    z[87]=z[87]*z[95];
    z[91]=n<T>(21,4)*z[9];
    z[92]= - 1 + n<T>(25,24)*z[86];
    z[92]=n<T>(23,12)*z[5] + 11*z[92] + z[91];
    z[92]=z[5]*z[92];
    z[91]=n<T>(49,4)*z[2] - n<T>(119,8)*z[5] - z[91] + 11 - n<T>(35,8)*z[86];
    z[91]=z[2]*z[91];
    z[94]=n<T>(179,3)*z[5] - n<T>(185,3)*z[86] + n<T>(107,2)*z[9];
    z[94]= - n<T>(41,24)*z[10] - n<T>(103,4)*z[2] + n<T>(1,2)*z[94] - z[8];
    z[94]=z[94]*z[100];
    z[97]=z[8] - z[9];
    z[98]= - n<T>(127,12)*z[6] + n<T>(121,12)*z[5] - n<T>(109,12)*z[86] + z[97] + 
    z[10];
    z[98]=z[98]*z[93];
    z[99]=z[95] - z[86] - z[8];
    z[95]=z[99]*z[95];
    z[99]=107*z[9];
    z[102]=n<T>(445,3)*z[86] - z[99];
    z[102]=n<T>(101,12)*z[3] + n<T>(1,4)*z[4] - n<T>(133,48)*z[6] - n<T>(185,24)*z[10]
    + n<T>(17,2)*z[2] + n<T>(1,16)*z[102] - n<T>(17,3)*z[5];
    z[102]=z[3]*z[102];
    z[90]=z[8]*z[90];
    z[90]=z[102] + z[95] + z[98] + z[94] + z[91] + z[90] - n<T>(39,8)*z[96]
    + z[92];
    z[90]=z[3]*z[90];
    z[91]=3*z[5];
    z[92]=41*z[86] + n<T>(101,2)*z[9];
    z[92]=n<T>(1,2)*z[92] - z[91];
    z[92]=z[5]*z[92];
    z[94]=n<T>(1,4)*z[2];
    z[95]=n<T>(35,2)*z[2] - 45*z[8] - 33*z[5] + 107*z[86] - 109*z[9];
    z[95]=z[95]*z[94];
    z[98]=3*z[9];
    z[102]=n<T>(15,4)*z[10] - 3*z[8] - n<T>(1,2)*z[86] + z[98];
    z[103]=n<T>(1,4)*z[10];
    z[102]=z[102]*z[103];
    z[104]=z[86] - z[9];
    z[93]=z[93] + z[104];
    z[93]=z[6]*z[93];
    z[99]= - n<T>(217,2)*z[86] + z[99];
    z[99]=n<T>(1,12)*z[99] - 15*z[5];
    z[99]=n<T>(11,8)*z[10] + n<T>(151,2)*z[2] + 5*z[99] + n<T>(401,6)*z[8];
    z[99]= - n<T>(755,12)*z[7] + n<T>(1,2)*z[99] + 18*z[6];
    z[99]=z[7]*z[99];
    z[105]= - 979*z[86] + n<T>(1003,2)*z[9];
    z[105]=z[9]*z[105];
    z[106]= - 23*z[86] - n<T>(97,4)*z[9];
    z[106]=n<T>(413,8)*z[8] + n<T>(1,3)*z[106] + 27*z[5];
    z[106]=z[8]*z[106];
    z[92]=z[99] + 37*z[93] + z[102] + z[95] + z[106] + n<T>(1,12)*z[105] + 
    z[92];
    z[92]=z[7]*z[92];
    z[93]=n<T>(3,4)*z[9];
    z[95]= - z[89] - n<T>(1,4)*z[5] + z[86] - z[93];
    z[95]=z[8]*z[95];
    z[98]=z[86] + z[98];
    z[99]=z[7] - z[10];
    z[91]=n<T>(5,2)*z[8] + n<T>(1,2)*z[98] - z[91] + n<T>(3,2)*z[99];
    z[91]=z[91]*z[101];
    z[98]= - z[86] + n<T>(1,2)*z[5];
    z[98]=z[5]*z[98];
    z[96]=11*z[96] + 7*z[98];
    z[98]=n<T>(3,2)*z[9];
    z[99]= - n<T>(1,6)*z[10] + n<T>(3,2)*z[8] - n<T>(1,3)*z[86] - z[98];
    z[99]=z[99]*z[100];
    z[102]=2*z[86];
    z[101]= - n<T>(17,24)*z[4] + z[101] + z[102] + n<T>(11,12)*z[10];
    z[101]=z[4]*z[101];
    z[98]=n<T>(7,2)*z[5] - z[86] + z[98];
    z[98]=z[7] + n<T>(5,4)*z[10] + n<T>(1,2)*z[98] - z[8];
    z[98]= - n<T>(7,8)*z[11] + n<T>(1,2)*z[98] - z[4];
    z[98]=z[11]*z[98];
    z[102]=z[102] - z[2];
    z[102]=z[2]*z[102];
    z[91]=z[98] + z[101] + z[91] + z[99] + z[102] + n<T>(1,4)*z[96] + z[95];
    z[91]=z[11]*z[91];
    z[95]=n<T>(1,3)*z[9];
    z[96]= - 40*z[86] + n<T>(37,2)*z[9];
    z[96]=z[96]*z[95];
    z[98]=z[97] + z[86];
    z[88]= - z[88] - z[98];
    z[88]=z[88]*z[94];
    z[94]=n<T>(121,16)*z[10] - n<T>(129,8)*z[5] + n<T>(137,8)*z[86] + z[97];
    z[94]=z[94]*z[100];
    z[99]=n<T>(145,96)*z[5] - n<T>(193,48)*z[86] + z[9];
    z[99]=z[5]*z[99];
    z[101]= - 74*z[86] + 77*z[9];
    z[101]= - n<T>(13,8)*z[8] + n<T>(1,3)*z[101] - z[5];
    z[101]=z[8]*z[101];
    z[102]= - n<T>(311,8)*z[8] + n<T>(145,32)*z[5] - n<T>(689,32)*z[86] + 17*z[9];
    z[102]= - n<T>(233,24)*z[6] + n<T>(121,32)*z[10] + n<T>(1,3)*z[102] + n<T>(9,4)*z[2]
   ;
    z[102]=z[6]*z[102];
    z[88]=z[102] + z[94] + z[88] + z[101] + z[96] + z[99];
    z[88]=z[6]*z[88];
    z[94]= - n<T>(319,12)*z[4] - z[6] - n<T>(307,12)*z[2] + n<T>(319,6)*z[86] + 
    z[97];
    z[94]=z[18]*z[94];
    z[96]= - n<T>(5,2)*z[11] - 5*z[7] - z[10] - z[98];
    z[96]=z[30]*z[96];
    z[97]=z[2] + z[104] - z[5];
    z[98]= - n<T>(1781,6)*z[7] - n<T>(1331,6)*z[8] + 75*z[97];
    z[98]=z[24]*z[98];
    z[97]= - n<T>(125,12)*z[3] + 17*z[97] - n<T>(329,12)*z[10];
    z[97]=z[27]*z[97];
    z[94]= - z[97] - z[94] - z[98] - z[96] - z[85] + z[84] + z[79];
    z[96]=55*z[86] + n<T>(59,2)*z[9];
    z[96]=z[9]*z[96];
    z[97]= - n<T>(329,12)*z[5] + n<T>(329,6)*z[86] - 103*z[9];
    z[97]=z[5]*z[97];
    z[96]=z[96] + z[97];
    z[97]=z[2] - z[9];
    z[97]= - n<T>(281,6)*z[5] + n<T>(295,6)*z[86] - 117*z[97];
    z[97]=n<T>(1,2)*z[97] - n<T>(85,3)*z[10];
    z[97]=z[97]*z[100];
    z[98]=z[8]*z[9];
    z[99]=103*z[5] - 105*z[86] - 61*z[9];
    z[99]=n<T>(63,4)*z[2] + n<T>(1,2)*z[99] - z[8];
    z[99]=z[2]*z[99];
    z[96]=z[97] + z[99] + n<T>(1,2)*z[96] + z[98];
    z[96]=z[96]*z[103];
    z[97]= - 377*z[86] + n<T>(249,2)*z[9];
    z[97]=z[9]*z[97];
    z[98]=n<T>(2581,3)*z[86] + 85*z[9];
    z[98]= - n<T>(1219,6)*z[2] - 71*z[8] + n<T>(1,2)*z[98] + n<T>(203,3)*z[5];
    z[98]=z[2]*z[98];
    z[97]=z[97] + z[98];
    z[98]= - 139*z[86] + 183*z[9];
    z[98]= - n<T>(263,8)*z[8] + n<T>(1,4)*z[98] + 25*z[5];
    z[89]=z[98]*z[89];
    z[98]=n<T>(73,6)*z[5] - n<T>(88,3)*z[86] + n<T>(43,8)*z[9];
    z[98]=z[5]*z[98];
    z[89]=z[89] + z[98] + n<T>(1,8)*z[97];
    z[89]=z[2]*z[89];
    z[97]=z[86]*z[4];
    z[97]=z[97] + z[31];
    z[98]= - z[6]*z[86];
    z[98]=z[98] - z[97];
    z[98]=z[14]*z[98];
    z[99]=z[86] - z[4];
    z[100]=z[6] - z[99];
    z[100]=z[21]*z[100];
    z[98]=z[98] + z[100] + z[61] + z[49];
    z[100]=z[86]*z[9];
    z[100]=z[100] + z[31];
    z[101]=z[10]*z[86];
    z[101]=z[101] + z[100];
    z[101]=z[17]*z[101];
    z[102]= - z[10] + z[104];
    z[102]=z[28]*z[102];
    z[101]=z[101] + z[102];
    z[102]=z[8]*z[86];
    z[100]=z[102] + z[100];
    z[100]=z[16]*z[100];
    z[102]=z[8] - z[104];
    z[102]=z[26]*z[102];
    z[100]=z[100] + z[102];
    z[102]=z[86]*z[5];
    z[102]=z[102] + z[31];
    z[103]= - z[7]*z[86];
    z[103]=z[103] - z[102];
    z[103]=z[15]*z[103];
    z[105]=z[86] - z[5];
    z[106]=z[7] - z[105];
    z[106]=z[22]*z[106];
    z[103]=z[103] + z[106];
    z[106]= - z[11]*z[86];
    z[97]=z[106] - z[97];
    z[97]=z[13]*z[97];
    z[99]=z[11] - z[99];
    z[99]=z[29]*z[99];
    z[97]=z[97] + z[99];
    z[99]= - z[3]*z[86];
    z[99]=z[99] - z[102];
    z[99]=z[12]*z[99];
    z[102]= - z[3] + z[105];
    z[102]=z[20]*z[102];
    z[99]=z[99] + z[102];
    z[102]=121*z[86] - n<T>(83,2)*z[9];
    z[93]=z[102]*z[93];
    z[102]= - n<T>(433,8)*z[9] - 63*z[105];
    z[102]=z[5]*z[102];
    z[93]=z[93] + z[102];
    z[93]=z[5]*z[93];
    z[102]= - 11*z[86] - n<T>(43,3)*z[9];
    z[102]=z[102]*npow(z[9],2);
    z[93]=z[57] + n<T>(1,2)*z[102] + z[93];
    z[102]=101*z[86] - n<T>(1139,4)*z[9];
    z[95]=z[102]*z[95];
    z[102]= - n<T>(109,4)*z[5] + n<T>(105,2)*z[86] - 23*z[9];
    z[102]=z[5]*z[102];
    z[95]=z[95] + z[102];
    z[102]= - 68*z[86] - n<T>(701,16)*z[9];
    z[102]= - n<T>(431,24)*z[8] + n<T>(1,3)*z[102] - n<T>(103,8)*z[5];
    z[102]=z[8]*z[102];
    z[95]=n<T>(1,2)*z[95] + z[102];
    z[95]=z[8]*z[95];
    z[102]=z[6] + z[8];
    z[102]=z[7] + z[104] - n<T>(269,12)*z[102];
    z[102]=z[23]*z[102];
    z[104]= - z[10] - z[11];
    z[104]=z[43]*z[104];
    z[104]=z[104] + z[60];
    z[105]=z[6] + z[3];
    z[105]=z[42]*z[105];
    z[105]=z[105] + z[46];
    z[106]= - n<T>(1313,12)*z[38] - n<T>(2131,48)*z[36] + n<T>(593,96)*z[32];
    z[106]=i*z[106];
    z[107]= - n<T>(97,24)*z[11] + n<T>(139,16)*z[3] + n<T>(431,12)*z[4] - n<T>(2015,12)*
    z[7] - n<T>(103,3)*z[6] + n<T>(409,24)*z[10] + n<T>(343,48)*z[2] + n<T>(229,8)*z[8]
    + n<T>(497,96)*z[9] - 17*z[5];
    z[107]=z[31]*z[107];
    z[108]=n<T>(77,8)*z[3] + z[7] - n<T>(113,4)*z[2] + n<T>(25,8)*z[8] - n<T>(85,8)*z[5]
    + 65*z[86] - n<T>(209,8)*z[9];
    z[108]=z[25]*z[108];
    z[86]=91*z[86] - n<T>(67,2)*z[5];
    z[86]= - n<T>(115,6)*z[2] + n<T>(1,3)*z[86] - n<T>(5,4)*z[8];
    z[86]=z[19]*z[86];

    r +=  - n<T>(727,24)*z[33] + n<T>(5,3)*z[34] - n<T>(71,72)*z[35] + n<T>(973,48)*
      z[37] + n<T>(1199,24)*z[39] - n<T>(5,4)*z[40] + n<T>(797,24)*z[41] + n<T>(9,4)*
      z[44] - n<T>(23,8)*z[45] - n<T>(625,96)*z[47] - n<T>(689,96)*z[48] - n<T>(29,16)*
      z[50] - 34*z[51] - 17*z[52] - n<T>(49,16)*z[53] - n<T>(257,32)*z[54] - 
      n<T>(145,96)*z[55] - n<T>(619,24)*z[56] + n<T>(121,32)*z[58] - n<T>(309,32)*z[59]
       + n<T>(607,24)*z[62] + n<T>(1453,24)*z[63] + n<T>(259,8)*z[64] + 150*z[65]
       + n<T>(247,12)*z[66] - n<T>(71,12)*z[67] + n<T>(31,8)*z[68] - n<T>(21,8)*z[69]
       + 18*z[70] - n<T>(11,2)*z[71] - n<T>(75,4)*z[72] + n<T>(3,8)*z[73] - n<T>(1,8)*
      z[74] + n<T>(11,12)*z[75] - n<T>(7,16)*z[76] - n<T>(7,8)*z[77] + n<T>(7,2)*z[78]
       + n<T>(7,48)*z[80] - n<T>(5,8)*z[81] + n<T>(15,16)*z[82] + 2*z[83] + z[86]
       + z[87] + z[88] + z[89] + z[90] + z[91] + z[92] + n<T>(1,4)*z[93] - 
      n<T>(1,2)*z[94] + z[95] + z[96] + n<T>(7,12)*z[97] + n<T>(17,4)*z[98] + n<T>(17,3)
      *z[99] + n<T>(1595,24)*z[100] + n<T>(25,4)*z[101] + z[102] + n<T>(63,2)*
      z[103] + n<T>(17,12)*z[104] + n<T>(121,24)*z[105] + z[106] + n<T>(1,3)*z[107]
       + z[108];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1135(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1135(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
