#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf143(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[109];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[8];
    z[24]=e[9];
    z[25]=e[10];
    z[26]=e[11];
    z[27]=e[12];
    z[28]=c[3];
    z[29]=c[11];
    z[30]=c[15];
    z[31]=c[18];
    z[32]=c[19];
    z[33]=c[23];
    z[34]=c[25];
    z[35]=c[26];
    z[36]=c[28];
    z[37]=c[29];
    z[38]=c[31];
    z[39]=e[3];
    z[40]=e[6];
    z[41]=e[7];
    z[42]=e[13];
    z[43]=e[14];
    z[44]=f[0];
    z[45]=f[1];
    z[46]=f[2];
    z[47]=f[3];
    z[48]=f[4];
    z[49]=f[5];
    z[50]=f[6];
    z[51]=f[7];
    z[52]=f[8];
    z[53]=f[10];
    z[54]=f[11];
    z[55]=f[12];
    z[56]=f[13];
    z[57]=f[14];
    z[58]=f[16];
    z[59]=f[17];
    z[60]=f[18];
    z[61]=f[21];
    z[62]=f[23];
    z[63]=f[30];
    z[64]=f[31];
    z[65]=f[32];
    z[66]=f[35];
    z[67]=f[36];
    z[68]=f[37];
    z[69]=f[39];
    z[70]=f[41];
    z[71]=f[43];
    z[72]=f[50];
    z[73]=f[51];
    z[74]=f[52];
    z[75]=f[53];
    z[76]=f[54];
    z[77]=f[55];
    z[78]=f[56];
    z[79]=f[58];
    z[80]=f[60];
    z[81]=f[83];
    z[82]=f[84];
    z[83]=f[85];
    z[84]=f[86];
    z[85]=z[1]*i;
    z[86]=z[85] - z[5];
    z[87]=7*z[8];
    z[88]= - n<T>(29,2)*z[9] + z[87] - 22*z[86];
    z[89]=n<T>(5,3)*z[9];
    z[88]=z[88]*z[89];
    z[90]=35*z[9];
    z[91]=n<T>(299,2)*z[2] - z[90] - 179*z[5] - 133*z[85] + 13*z[8];
    z[92]=n<T>(1,3)*z[2];
    z[91]=z[91]*z[92];
    z[93]=n<T>(82,3)*z[2];
    z[94]=2*z[85];
    z[95]= - z[94] - n<T>(5,6)*z[8];
    z[95]=n<T>(359,18)*z[3] - n<T>(125,6)*z[11] + z[93] - n<T>(55,3)*z[9] + 7*z[95]
    - n<T>(43,6)*z[5];
    z[95]=z[3]*z[95];
    z[96]= - z[85] + n<T>(1,2)*z[8];
    z[97]=z[96]*z[8];
    z[98]=71*z[85];
    z[99]= - n<T>(10,3)*z[5] + z[98] - n<T>(13,3)*z[8];
    z[99]=z[5]*z[99];
    z[100]=z[94] - z[4];
    z[100]=z[4]*z[100];
    z[101]= - 25*z[86] - 13*z[11];
    z[101]=z[11]*z[101];
    z[88]=z[95] + n<T>(5,3)*z[101] + 20*z[100] + z[91] + z[88] - n<T>(13,3)*
    z[97] + z[99];
    z[88]=z[3]*z[88];
    z[91]=2*z[8];
    z[95]=z[94] - z[8];
    z[95]=z[95]*z[91];
    z[99]=n<T>(1,2)*z[5];
    z[100]= - z[85] + z[99];
    z[99]=z[100]*z[99];
    z[95]=z[95] + z[99];
    z[99]=n<T>(1,2)*z[9];
    z[100]=z[99] + z[86];
    z[100]=z[9]*z[100];
    z[95]=n<T>(1,3)*z[95] + n<T>(17,2)*z[100];
    z[100]=n<T>(1,4)*z[5];
    z[101]=z[100] + n<T>(7,4)*z[85] - z[91];
    z[101]=n<T>(1,3)*z[101] + n<T>(17,4)*z[9];
    z[102]=29*z[6];
    z[101]=n<T>(22,3)*z[11] - z[4] + n<T>(77,3)*z[7] + z[2] + 5*z[101] - z[102];
    z[101]=z[11]*z[101];
    z[103]=z[85] - z[8];
    z[104]= - 2*z[103] - z[6];
    z[102]=z[104]*z[102];
    z[104]=n<T>(154,3)*z[103] + 19*z[7];
    z[104]=z[7]*z[104];
    z[105]=z[4]*z[94];
    z[95]=z[101] + z[105] + z[104] + 5*z[95] + z[102];
    z[95]=z[11]*z[95];
    z[100]=z[100] - n<T>(1,2)*z[85] - z[8];
    z[101]=35*z[5];
    z[100]=z[100]*z[101];
    z[102]=26*z[85] - n<T>(59,2)*z[8];
    z[102]=z[8]*z[102];
    z[100]=z[102] + z[100];
    z[102]=n<T>(11,3)*z[8];
    z[104]= - n<T>(11,18)*z[9] + n<T>(35,12)*z[5] + z[94] + z[102];
    z[104]=z[9]*z[104];
    z[100]=n<T>(1,3)*z[100] + z[104];
    z[100]=z[9]*z[100];
    z[104]=22*z[2];
    z[105]= - z[8] + z[86];
    z[90]=z[104] + 22*z[105] + z[90];
    z[90]=n<T>(1,3)*z[90] + 19*z[3];
    z[90]=z[25]*z[90];
    z[105]= - 367*z[85] + n<T>(343,2)*z[8];
    z[105]=z[8]*z[105];
    z[106]= - 20*z[5] + 28*z[85] + n<T>(391,2)*z[8];
    z[106]=z[5]*z[106];
    z[105]=z[105] + z[106];
    z[105]=z[5]*z[105];
    z[106]= - 41*z[85] + z[8];
    z[106]=z[106]*npow(z[8],2);
    z[88]= - z[45] + z[88] + z[95] + z[100] + n<T>(1,2)*z[106] + n<T>(1,3)*
    z[105] + z[90] + z[82];
    z[90]=z[6] - z[5];
    z[87]= - 5*z[7] - z[104] + n<T>(55,3)*z[85] + z[87] - n<T>(14,3)*z[90];
    z[87]=z[7]*z[87];
    z[90]=n<T>(8,3)*z[9];
    z[95]=z[90] - 34*z[85] + z[102];
    z[95]=z[8]*z[95];
    z[100]=2*z[6];
    z[102]=7*z[85];
    z[104]=2*z[5];
    z[105]=z[100] - n<T>(4,3)*z[9] - z[104] + z[102] + n<T>(38,3)*z[8];
    z[105]=2*z[105] - n<T>(53,3)*z[2];
    z[105]=z[2]*z[105];
    z[106]= - n<T>(1,9)*z[85] + z[8];
    z[106]=2*z[106] + n<T>(1,9)*z[5];
    z[106]=z[106]*z[104];
    z[107]=2*z[9];
    z[108]=4*z[85] - n<T>(19,3)*z[8];
    z[108]=4*z[6] - z[107] + 2*z[108] + n<T>(5,3)*z[5];
    z[108]=z[6]*z[108];
    z[87]=n<T>(1,3)*z[87] + z[105] + n<T>(4,3)*z[108] + z[106] + z[95];
    z[87]=z[7]*z[87];
    z[95]=n<T>(5,3)*z[85];
    z[91]=z[95] - z[91];
    z[89]=2*z[91] + z[89];
    z[89]=z[89]*z[107];
    z[91]=n<T>(1,3)*z[6];
    z[102]=z[102] + n<T>(2,3)*z[9];
    z[102]= - n<T>(5,6)*z[4] + 2*z[102] + z[91];
    z[102]=z[4]*z[102];
    z[105]=z[94] - z[5];
    z[106]=z[5]*z[105];
    z[89]=z[102] + z[89] + 19*z[97] - 10*z[106];
    z[97]=n<T>(1,2)*z[2];
    z[102]= - n<T>(1,3)*z[10] - n<T>(7,3)*z[4] + z[97] - z[6] + z[99] + z[104]
    - n<T>(3,2)*z[85] + n<T>(4,3)*z[8];
    z[102]=z[10]*z[102];
    z[106]=4*z[5];
    z[107]=n<T>(11,3)*z[85] + 8*z[8];
    z[90]=n<T>(29,18)*z[6] - z[90] + n<T>(1,3)*z[107] - z[106];
    z[90]=z[6]*z[90];
    z[97]=z[85] - z[97];
    z[97]=z[2]*z[97];
    z[107]=z[6] + z[9] - z[8] + z[105];
    z[107]=z[7]*z[107];
    z[89]=z[102] + n<T>(8,3)*z[107] + n<T>(11,3)*z[97] + z[90] + n<T>(1,3)*z[89];
    z[89]=z[10]*z[89];
    z[90]=n<T>(11,3)*z[9] - n<T>(625,3)*z[8] - n<T>(77,2)*z[5];
    z[90]=n<T>(133,3)*z[3] + n<T>(67,6)*z[11] - n<T>(331,6)*z[10] - n<T>(305,4)*z[4]
     - 
   n<T>(575,12)*z[7] - z[93] + n<T>(1,2)*z[90] + n<T>(179,3)*z[6];
    z[90]=z[28]*z[90];
    z[93]=z[85]*z[5];
    z[93]=z[93] + z[28];
    z[97]= - z[3]*z[85];
    z[97]=z[97] - z[93];
    z[97]=z[12]*z[97];
    z[102]= - z[3] + z[86];
    z[102]=z[20]*z[102];
    z[107]=11*z[6] + 29*z[10];
    z[107]=z[42]*z[107];
    z[90]= - z[90] - z[97] - z[102] - z[107] + z[76] + z[30];
    z[97]=35*z[8];
    z[101]= - 11*z[9] + z[101] - 59*z[85] + z[97];
    z[101]=z[9]*z[101];
    z[102]=103*z[85] + 44*z[8];
    z[102]=2*z[102] - 139*z[5];
    z[102]=z[102]*z[104];
    z[104]=1126*z[85] - 419*z[8];
    z[104]=z[8]*z[104];
    z[101]=z[101] + z[104] + z[102];
    z[102]=z[5] - z[8];
    z[104]=n<T>(4,3)*z[85] + z[102];
    z[104]=2*z[104] - z[91];
    z[100]=z[104]*z[100];
    z[104]= - n<T>(11,2)*z[9] - 205*z[5] - n<T>(2213,2)*z[85] - 232*z[8];
    z[104]=n<T>(385,2)*z[2] + n<T>(1,3)*z[104] - 8*z[6];
    z[92]=z[104]*z[92];
    z[92]=z[92] + n<T>(1,9)*z[101] + z[100];
    z[92]=z[2]*z[92];
    z[100]=181*z[8];
    z[96]=z[96]*z[100];
    z[98]=n<T>(55,2)*z[5] - z[98] - z[100];
    z[98]=z[5]*z[98];
    z[99]=z[85] - z[99];
    z[101]=n<T>(59,6)*z[9];
    z[99]=z[99]*z[101];
    z[95]= - n<T>(53,6)*z[6] + z[95] + 17*z[9];
    z[95]=z[6]*z[95];
    z[95]=n<T>(1,2)*z[95] + z[99] + z[96] + z[98];
    z[96]= - n<T>(5,6)*z[6] - z[101] + 71*z[5] - n<T>(103,3)*z[85] + z[100];
    z[96]=n<T>(7,2)*z[4] + n<T>(19,3)*z[7] + n<T>(1,6)*z[96] - 42*z[2];
    z[96]=z[4]*z[96];
    z[98]=z[85]*z[7];
    z[99]=z[94] + z[5];
    z[99]=2*z[99] - z[2];
    z[99]=z[2]*z[99];
    z[95]=z[96] - n<T>(38,3)*z[98] + n<T>(1,3)*z[95] + 21*z[99];
    z[95]=z[4]*z[95];
    z[96]=z[4] + z[2];
    z[96]= - 32*z[85] - z[102] + 16*z[96];
    z[96]=z[18]*z[96];
    z[99]=z[85]*z[8];
    z[99]=z[99] + z[28];
    z[98]= - z[98] - z[99];
    z[98]=z[16]*z[98];
    z[100]= - z[7] + z[103];
    z[100]=z[24]*z[100];
    z[96]=z[100] + z[96] + z[98];
    z[98]=z[85]*z[4];
    z[98]=z[98] + z[28];
    z[100]= - z[11]*z[85];
    z[100]=z[100] - z[98];
    z[100]=z[14]*z[100];
    z[101]= - 470*z[85] + 211*z[8];
    z[101]=n<T>(259,3)*z[2] + n<T>(1,3)*z[101] - 8*z[9];
    z[101]=z[23]*z[101];
    z[102]=z[85] - z[4];
    z[104]=z[11] - z[102];
    z[104]=z[21]*z[104];
    z[100]=z[101] + z[100] + z[104] + z[71];
    z[94]= - n<T>(4,3)*z[5] - z[94] - z[8];
    z[94]=z[94]*z[106];
    z[101]=46*z[85] + z[8];
    z[101]=z[8]*z[101];
    z[94]=n<T>(1,3)*z[101] + z[94];
    z[97]=n<T>(169,4)*z[85] - z[97];
    z[97]=n<T>(137,6)*z[6] - n<T>(1,4)*z[9] + n<T>(1,3)*z[97] + z[106];
    z[91]=z[97]*z[91];
    z[97]= - n<T>(1,12)*z[9] - n<T>(11,2)*z[85] + n<T>(8,3)*z[8];
    z[97]=z[9]*z[97];
    z[91]=z[91] + n<T>(1,3)*z[94] + z[97];
    z[91]=z[6]*z[91];
    z[94]=z[9]*z[85];
    z[94]=z[94] + z[99];
    z[94]=z[17]*z[94];
    z[97]= - z[9] + z[103];
    z[97]=z[26]*z[97];
    z[94]=z[80] + z[94] + z[97];
    z[97]=z[6]*z[85];
    z[93]=z[97] + z[93];
    z[93]=z[15]*z[93];
    z[86]= - z[6] + z[86];
    z[86]=z[22]*z[86];
    z[86]=z[93] + z[86];
    z[85]= - z[10]*z[85];
    z[85]=z[85] - z[98];
    z[85]=z[13]*z[85];
    z[93]=z[10] - z[102];
    z[93]=z[27]*z[93];
    z[85]=z[85] + z[93];
    z[93]= - z[84] + z[83] + z[44] - z[37];
    z[97]=z[78] + z[50];
    z[98]=z[9] + z[10];
    z[98]=z[43]*z[98];
    z[98]=z[98] + z[62];
    z[99]=n<T>(2012,9)*z[35] + n<T>(2165,18)*z[33] - n<T>(23,108)*z[29];
    z[99]=i*z[99];
    z[101]=z[2] - z[105];
    z[101]=z[19]*z[101];
    z[102]=z[11] + z[3];
    z[102]=z[39]*z[102];
    z[103]=z[7] + z[11];
    z[103]=z[40]*z[103];
    z[104]=z[6] + z[7];
    z[104]=z[41]*z[104];

    r +=  - n<T>(16,9)*z[31] + n<T>(178,27)*z[32] - n<T>(377,6)*z[34] - n<T>(1054,9)*
      z[36] - n<T>(698,27)*z[38] + n<T>(125,9)*z[46] + n<T>(91,6)*z[47] - n<T>(1,4)*
      z[48] + n<T>(161,9)*z[49] - n<T>(88,9)*z[51] - n<T>(73,3)*z[52] - n<T>(25,3)*
      z[53] + n<T>(319,18)*z[54] - n<T>(5,36)*z[55] + n<T>(137,3)*z[56] + n<T>(20,3)*
      z[57] + n<T>(85,12)*z[58] + n<T>(149,6)*z[59] + n<T>(2,9)*z[60] - n<T>(40,3)*
      z[61] - n<T>(154,9)*z[63] - n<T>(14,9)*z[64] - n<T>(23,9)*z[65] + n<T>(140,9)*
      z[66] + n<T>(10,9)*z[67] - n<T>(19,3)*z[68] - n<T>(29,3)*z[69] + n<T>(38,3)*z[70]
       + n<T>(4,9)*z[72] - n<T>(89,36)*z[73] - z[74] + 4*z[75] + n<T>(59,36)*z[77]
       - n<T>(1,12)*z[79] + z[81] + n<T>(37,9)*z[85] + n<T>(32,9)*z[86] + z[87] + n<T>(1,3)*z[88]
     + z[89] - n<T>(1,9)*z[90]
     + z[91]
     + z[92] - n<T>(4,3)*z[93]
     +  n<T>(11,3)*z[94] + z[95] + n<T>(8,3)*z[96] - n<T>(1,2)*z[97] + n<T>(11,9)*z[98]
       + z[99] + n<T>(2,3)*z[100] + n<T>(176,9)*z[101] + n<T>(5,9)*z[102] + n<T>(40,9)*
      z[103] + n<T>(124,9)*z[104];
 
    return r;
}

template std::complex<double> qqb_2lha_tf143(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf143(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
