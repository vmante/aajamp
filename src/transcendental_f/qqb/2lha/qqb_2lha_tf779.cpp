#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf779(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[94];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[4];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[9];
    z[10]=d[15];
    z[11]=d[5];
    z[12]=d[11];
    z[13]=d[16];
    z[14]=d[18];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[8];
    z[20]=e[10];
    z[21]=e[11];
    z[22]=e[12];
    z[23]=c[3];
    z[24]=c[11];
    z[25]=c[15];
    z[26]=c[18];
    z[27]=c[19];
    z[28]=c[21];
    z[29]=c[23];
    z[30]=c[25];
    z[31]=c[26];
    z[32]=c[28];
    z[33]=c[29];
    z[34]=c[31];
    z[35]=d[6];
    z[36]=e[3];
    z[37]=e[13];
    z[38]=e[14];
    z[39]=f[0];
    z[40]=f[1];
    z[41]=f[2];
    z[42]=f[3];
    z[43]=f[4];
    z[44]=f[5];
    z[45]=f[6];
    z[46]=f[7];
    z[47]=f[8];
    z[48]=f[10];
    z[49]=f[11];
    z[50]=f[12];
    z[51]=f[14];
    z[52]=f[15];
    z[53]=f[16];
    z[54]=f[17];
    z[55]=f[18];
    z[56]=f[20];
    z[57]=f[23];
    z[58]=f[50];
    z[59]=f[51];
    z[60]=f[54];
    z[61]=f[55];
    z[62]=f[56];
    z[63]=f[58];
    z[64]=f[60];
    z[65]= - n<T>(2,3)*z[9] + n<T>(3,2)*z[11];
    z[66]=n<T>(1,2)*z[7];
    z[67]= - z[66] + n<T>(1,2)*z[2];
    z[68]=n<T>(1,2)*z[3];
    z[69]=n<T>(5,12)*z[8];
    z[70]=n<T>(1,2)*z[6];
    z[71]=z[69] + z[68] - z[70] + z[67] - z[65];
    z[71]=z[8]*z[71];
    z[72]=n<T>(1,3)*z[9];
    z[73]=n<T>(1,6)*z[4];
    z[68]=z[73] + z[69] + n<T>(23,6)*z[2] - z[68] - n<T>(13,4)*z[7] - n<T>(7,12)*
    z[5] + n<T>(1,4)*z[11] - z[72];
    z[68]=z[4]*z[68];
    z[69]=z[9] + z[35];
    z[74]=z[69] + z[6];
    z[75]=z[74]*z[70];
    z[76]=n<T>(13,2)*z[7];
    z[77]=z[9] - z[35];
    z[78]= - z[76] + z[6] + 13*z[5] - z[77];
    z[66]=z[78]*z[66];
    z[69]=n<T>(1,2)*z[69];
    z[78]=n<T>(8,3)*z[2] + z[70] - z[69] - n<T>(16,3)*z[5];
    z[78]=z[2]*z[78];
    z[79]=npow(z[11],2);
    z[80]=z[18] + n<T>(1,4)*z[79];
    z[81]=n<T>(2,3)*z[22];
    z[82]=n<T>(16,3)*z[15];
    z[83]=2*z[19];
    z[84]=npow(z[5],2);
    z[85]=z[9]*z[11];
    z[74]=n<T>(1,2)*z[74] - z[3];
    z[74]=z[3]*z[74];
    z[66]=z[68] + z[71] + z[78] + z[74] + z[66] - z[75] - n<T>(7,12)*z[84]
    + z[85] + n<T>(11,18)*z[23] - z[20] - z[83] - z[82] + 2*z[16] - z[81]
    + z[80];
    z[66]=z[4]*z[66];
    z[68]=z[6] + z[2] + z[3];
    z[71]=4*z[14];
    z[74]=6*z[5];
    z[65]=n<T>(31,12)*z[8] - z[7] + z[74] + z[71] + z[65] - 3*z[68];
    z[65]=z[8]*z[65];
    z[68]=n<T>(2,3)*z[12];
    z[78]=z[68] - z[13];
    z[73]= - z[73] - n<T>(5,6)*z[8] - n<T>(23,3)*z[2] + z[3] + z[76] + n<T>(7,6)*
    z[5] + z[9] - n<T>(1,2)*z[11] + z[78];
    z[73]=z[4]*z[73];
    z[76]= - z[9] + n<T>(29,6)*z[5];
    z[85]=z[71] + z[76];
    z[85]=z[7]*z[85];
    z[86]=2*z[3];
    z[87]=15*z[2] + z[86] - n<T>(43,3)*z[7] - z[74] + z[6] + z[9];
    z[87]=z[2]*z[87];
    z[88]=z[16] + z[17];
    z[89]=4*z[20];
    z[90]=n<T>(1,3)*z[22] + 2*z[21];
    z[68]=n<T>(1,2)*z[9] + z[68] - z[11];
    z[68]=z[9]*z[68];
    z[74]= - z[10]*z[74];
    z[91]=2*z[5];
    z[92]=z[6] - z[13] + z[91];
    z[92]=z[6]*z[92];
    z[93]= - 3*z[10] - z[5];
    z[93]=4*z[3] - 3*z[7] + 2*z[93] + z[6];
    z[93]=z[3]*z[93];
    z[65]=z[73] + z[65] + z[87] + z[93] + z[85] + z[92] + z[74] + z[68]
    + z[89] + n<T>(55,3)*z[19] + n<T>(26,3)*z[15] + 2*z[90] - z[80] + 6*z[88];
    z[65]=z[1]*z[65];
    z[65]= - n<T>(5,9)*z[24] + z[28] - 33*z[31] - n<T>(29,2)*z[29] + z[65];
    z[65]=i*z[65];
    z[68]=z[2] - z[7];
    z[73]=n<T>(3,2)*z[6];
    z[74]=3*z[5];
    z[68]= - n<T>(13,6)*z[8] + n<T>(9,2)*z[3] - z[73] - z[74] + n<T>(3,4)*z[11] - 
    z[72] - 2*z[68];
    z[68]=z[8]*z[68];
    z[72]=n<T>(1,2)*z[77];
    z[73]= - z[73] + z[72] + z[74];
    z[73]=z[6]*z[73];
    z[80]=n<T>(3,2)*z[7];
    z[74]= - n<T>(3,2)*z[3] + z[80] - z[72] + z[74];
    z[74]=z[3]*z[74];
    z[69]=z[69] - z[91];
    z[67]= - z[86] - z[69] + z[67];
    z[67]=z[2]*z[67];
    z[85]=npow(z[9],2);
    z[85]= - n<T>(1,2)*z[85] + 3*z[84];
    z[69]=z[69] + z[70];
    z[87]=z[7]*z[69];
    z[88]=n<T>(5,3)*z[38];
    z[90]=4*z[21];
    z[67]=z[68] + z[67] + z[74] + z[87] + z[73] + n<T>(3,4)*z[79] + n<T>(65,18)*
    z[23] - 12*z[20] - z[88] - z[90] - z[85];
    z[67]=z[8]*z[67];
    z[68]=z[80] - z[69];
    z[68]=z[7]*z[68];
    z[69]= - n<T>(11,3)*z[3] + z[70] + z[72] - z[5];
    z[69]=z[3]*z[69];
    z[73]=2*z[15];
    z[74]=2*z[17];
    z[80]= - z[74] - z[36];
    z[87]=n<T>(5,2)*z[6] - z[72] - z[5];
    z[87]=z[6]*z[87];
    z[68]=z[69] + z[68] + z[87] + 4*z[84] - n<T>(2,3)*z[23] - 9*z[20] + 
    z[83] + 3*z[80] - z[73];
    z[68]=z[3]*z[68];
    z[69]=n<T>(17,3)*z[7] - z[70] + z[72] - n<T>(5,3)*z[5];
    z[69]=z[7]*z[69];
    z[70]=z[86] - z[5] - z[7];
    z[70]=z[70]*z[86];
    z[72]= - z[6] + 7*z[5] - z[77];
    z[72]= - n<T>(47,6)*z[2] + z[3] + n<T>(1,2)*z[72] + n<T>(7,3)*z[7];
    z[72]=z[2]*z[72];
    z[69]=z[72] + z[70] + z[69] + z[75] + n<T>(11,6)*z[23] + z[89] - n<T>(26,3)*
    z[19] - 5*z[16] - z[82] + z[85];
    z[69]=z[2]*z[69];
    z[70]= - n<T>(1,2)*z[76] - n<T>(4,3)*z[7];
    z[70]=z[7]*z[70];
    z[70]=z[70] - n<T>(29,12)*z[84] - n<T>(49,36)*z[23] - z[89] - n<T>(23,3)*z[19]
    - z[90] + z[36];
    z[70]=z[7]*z[70];
    z[72]=z[37] + n<T>(1,2)*z[79];
    z[75]=n<T>(4,9)*z[23] + z[20] - z[81] - z[88] - z[72];
    z[75]=z[9]*z[75];
    z[71]= - 6*z[10] + z[71] + z[78];
    z[71]=z[23]*z[71];
    z[74]= - z[20] - z[74] - z[16];
    z[73]= - n<T>(1,4)*z[23] + z[73] - z[36] + 3*z[74];
    z[73]=z[5]*z[73];
    z[74]= - z[5] - n<T>(1,3)*z[6];
    z[74]=z[6]*z[74];
    z[74]=z[74] - z[84] + n<T>(23,12)*z[23] + z[18] - 4*z[36];
    z[74]=z[6]*z[74];
    z[76]=z[62] + z[40] + z[51];
    z[77]= - z[57] + z[41] + z[48];
    z[78]=z[56] + z[58];
    z[79]=z[47] - z[52];
    z[80]=z[33] - z[63];
    z[72]= - n<T>(1,3)*z[23] - z[72];
    z[72]=z[11]*z[72];
    z[81]= - z[36] + z[19];
    z[81]=z[35]*z[81];

    r += n<T>(31,12)*z[25] + z[26] - n<T>(59,24)*z[27] + n<T>(83,12)*z[30] + n<T>(33,2)
      *z[32] + n<T>(271,24)*z[34] - z[39] - n<T>(7,12)*z[42] - 5*z[43] - n<T>(25,4)
      *z[44] - n<T>(7,2)*z[45] - 12*z[46] - n<T>(29,12)*z[49] + z[50] - n<T>(3,2)*
      z[53] - n<T>(13,4)*z[54] - n<T>(1,3)*z[55] + n<T>(1,4)*z[59] - z[60] - n<T>(5,12)
      *z[61] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + 
      z[71] + z[72] + z[73] + z[74] + z[75] - n<T>(1,2)*z[76] - 2*z[77] + n<T>(2,3)*z[78]
     - 4*z[79]
     - n<T>(3,4)*z[80]
     + z[81];
 
    return r;
}

template std::complex<double> qqb_2lha_tf779(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf779(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
