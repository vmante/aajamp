#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1255(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[78];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[5];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[1];
    z[9]=d[4];
    z[10]=d[8];
    z[11]=d[15];
    z[12]=d[16];
    z[13]=d[12];
    z[14]=d[17];
    z[15]=e[0];
    z[16]=e[1];
    z[17]=e[2];
    z[18]=e[4];
    z[19]=e[5];
    z[20]=e[6];
    z[21]=e[7];
    z[22]=e[8];
    z[23]=e[9];
    z[24]=c[3];
    z[25]=c[11];
    z[26]=c[15];
    z[27]=c[19];
    z[28]=c[23];
    z[29]=c[25];
    z[30]=c[26];
    z[31]=c[28];
    z[32]=e[3];
    z[33]=e[10];
    z[34]=f[0];
    z[35]=f[1];
    z[36]=f[2];
    z[37]=f[3];
    z[38]=f[4];
    z[39]=f[5];
    z[40]=f[10];
    z[41]=f[11];
    z[42]=f[12];
    z[43]=f[14];
    z[44]=f[16];
    z[45]=f[17];
    z[46]=f[18];
    z[47]=f[30];
    z[48]=f[31];
    z[49]=f[32];
    z[50]=f[33];
    z[51]=f[35];
    z[52]=f[36];
    z[53]=f[37];
    z[54]=f[38];
    z[55]=f[39];
    z[56]=f[43];
    z[57]=n<T>(1,2)*z[4];
    z[58]=z[1]*i;
    z[59]=z[58] + z[57];
    z[60]=n<T>(1,2)*z[5];
    z[61]=n<T>(1,2)*z[6];
    z[59]= - n<T>(1,6)*z[7] - z[60] + n<T>(1,3)*z[59] + z[61];
    z[59]=z[7]*z[59];
    z[62]=z[5] - z[6];
    z[63]=n<T>(1,12)*z[7];
    z[64]= - z[58] - n<T>(1,4)*z[4];
    z[62]=n<T>(1,6)*z[2] - z[63] + n<T>(1,3)*z[64] + n<T>(1,4)*z[62];
    z[62]=z[2]*z[62];
    z[64]=z[57] - z[58];
    z[65]=z[64]*z[4];
    z[66]=z[6] - z[4];
    z[67]= - z[58] - z[66];
    z[67]=z[67]*z[61];
    z[68]=npow(z[9],2);
    z[69]=z[58] - z[4];
    z[70]= - z[6] + z[69];
    z[70]=n<T>(1,2)*z[70] + z[5];
    z[70]=z[5]*z[70];
    z[59]=z[62] + z[59] + z[70] + n<T>(1,4)*z[68] - n<T>(1,3)*z[65] + z[67];
    z[59]=z[2]*z[59];
    z[62]=n<T>(1,2)*z[7];
    z[67]=n<T>(1,2)*z[8];
    z[68]=z[3] - z[67] - z[2] + z[62] - z[61] + z[64];
    z[70]=n<T>(1,2)*z[3];
    z[68]=z[68]*z[70];
    z[71]=z[58] - z[61];
    z[71]=z[6]*z[71];
    z[71]=z[65] + z[71];
    z[72]=n<T>(1,2)*z[9];
    z[73]=z[58] - z[72];
    z[73]=z[9]*z[73];
    z[74]= - z[4] + z[62] - z[58];
    z[74]=z[74]*z[62];
    z[75]= - z[67] + z[58] + z[6];
    z[75]=z[75]*z[67];
    z[76]=z[2]*z[58];
    z[68]=z[68] + z[75] + z[76] + z[74] + n<T>(1,2)*z[71] + z[73];
    z[68]=z[68]*z[70];
    z[71]=n<T>(1,3)*z[69];
    z[73]= - n<T>(1,12)*z[9] - z[71] - z[61];
    z[73]=z[9]*z[73];
    z[74]=z[6] - z[9];
    z[60]=z[74]*z[60];
    z[71]=z[4]*z[71];
    z[74]= - z[6]*z[57];
    z[75]=n<T>(1,2)*z[58];
    z[76]=5*z[8] - z[9] + z[75] - z[4];
    z[76]=z[8]*z[76];
    z[60]=n<T>(1,6)*z[76] + z[60] + z[73] + z[71] + z[74];
    z[60]=z[60]*z[67];
    z[71]=n<T>(1,6)*z[6];
    z[73]= - z[71] - z[58] - n<T>(5,3)*z[4];
    z[61]=z[73]*z[61];
    z[61]= - n<T>(5,3)*z[65] + z[61];
    z[61]=z[6]*z[61];
    z[73]= - n<T>(3,4)*z[6] - n<T>(7,3)*z[58] + z[57];
    z[73]=z[6]*z[73];
    z[74]=n<T>(11,4)*z[9] - 5*z[6] + z[58] - n<T>(7,8)*z[4];
    z[74]=z[9]*z[74];
    z[73]=n<T>(1,3)*z[74] + n<T>(5,12)*z[65] + z[73];
    z[73]=z[9]*z[73];
    z[74]=z[69]*npow(z[4],2);
    z[61]=z[73] - n<T>(11,12)*z[74] + z[61];
    z[73]=5*z[4] + n<T>(17,2)*z[6];
    z[73]=z[73]*z[71];
    z[74]=n<T>(9,4)*z[9] + n<T>(5,2)*z[58] - z[66];
    z[76]=n<T>(1,4)*z[9];
    z[74]=z[74]*z[76];
    z[66]= - n<T>(19,12)*z[5] + n<T>(5,16)*z[9] - n<T>(29,48)*z[58] + z[66];
    z[66]=z[5]*z[66];
    z[77]=z[58] - n<T>(1,24)*z[4];
    z[77]=z[4]*z[77];
    z[66]=z[66] + z[74] + z[77] + z[73];
    z[66]=z[5]*z[66];
    z[72]=z[72] + z[69];
    z[73]=3*z[9];
    z[72]=z[72]*z[73];
    z[72]=z[65] + z[72];
    z[67]= - z[67] - z[69];
    z[67]=z[8]*z[67];
    z[73]=z[73] - z[69];
    z[73]=n<T>(1,4)*z[73] - z[8];
    z[73]=z[10]*z[73];
    z[67]=z[73] + n<T>(1,2)*z[72] + z[67];
    z[67]=z[10]*z[67];
    z[72]= - z[76] + z[75] + 11*z[6];
    z[72]=z[9]*z[72];
    z[65]=z[65] + z[72];
    z[72]= - n<T>(7,12)*z[6] + n<T>(7,6)*z[58] - z[4];
    z[72]=z[6]*z[72];
    z[65]=z[72] + n<T>(1,6)*z[65];
    z[57]=n<T>(53,4)*z[5] - z[76] + z[57] - 13*z[6];
    z[57]=z[57]*z[63];
    z[63]= - n<T>(53,12)*z[58] + z[4];
    z[63]=n<T>(29,48)*z[5] - n<T>(7,8)*z[9] + n<T>(1,2)*z[63] - n<T>(1,3)*z[6];
    z[63]=z[5]*z[63];
    z[57]=z[57] + n<T>(1,2)*z[65] + z[63];
    z[57]=z[7]*z[57];
    z[63]=n<T>(1,2)*z[2];
    z[65]=z[63] - z[58];
    z[70]=z[70] + z[65];
    z[70]=z[15]*z[70];
    z[72]=z[2] - z[7] - n<T>(23,6)*z[5] - n<T>(17,6)*z[6] + z[69];
    z[72]=z[21]*z[72];
    z[57]=z[70] + z[72] + n<T>(1,4)*z[67] + z[68] + z[60] + z[59] + z[57] + 
   n<T>(1,2)*z[61] + z[66];
    z[59]=z[8] + z[5] + n<T>(7,6)*z[9] + z[71] + z[69];
    z[59]=z[20]*z[59];
    z[60]=z[3] + z[9];
    z[61]= - z[58]*z[60];
    z[61]= - z[24] + z[61];
    z[61]=z[12]*z[61];
    z[60]= - z[58] + z[60];
    z[60]=z[18]*z[60];
    z[59]=z[34] + z[59] + z[61] + z[60] - z[56];
    z[60]=z[63] + z[64];
    z[60]=z[16]*z[60];
    z[61]=z[62] + z[65];
    z[61]=z[22]*z[61];
    z[60]= - z[26] + z[60] + z[61];
    z[61]=z[7] + z[6];
    z[62]=z[58]*z[61];
    z[62]=z[24] + z[62];
    z[62]=z[14]*z[62];
    z[61]= - z[58] + z[61];
    z[61]=z[23]*z[61];
    z[61]=z[62] + z[61];
    z[62]=z[58]*z[4];
    z[62]=z[62] + z[24];
    z[63]=z[8]*z[58];
    z[63]=z[63] + z[62];
    z[63]=z[11]*z[63];
    z[64]=z[8] - z[69];
    z[64]=z[17]*z[64];
    z[63]= - z[29] + z[63] + z[64] + z[46] + z[39];
    z[58]= - z[5]*z[58];
    z[58]=z[58] - z[62];
    z[58]=z[13]*z[58];
    z[62]=z[5] - z[69];
    z[62]=z[19]*z[62];
    z[58]=z[58] + z[62] + z[49] + z[47];
    z[62]=z[3] - z[8];
    z[64]= - z[10] + n<T>(17,12)*z[7] - n<T>(35,2)*z[5] - n<T>(5,6)*z[9] - n<T>(11,4)*
    z[4] + n<T>(17,3)*z[6];
    z[62]=n<T>(1,3)*z[2] + n<T>(1,8)*z[64] - n<T>(1,4)*z[62];
    z[62]=z[24]*z[62];
    z[64]=z[8] + z[10];
    z[64]=z[33]*z[64];
    z[64]= - z[35] + z[64] - z[40];
    z[65]=z[53] - z[45] + z[43] - z[37];
    z[66]=z[51] - z[31];
    z[67]=n<T>(5,6)*z[30] + n<T>(1,6)*z[28] + n<T>(19,144)*z[25];
    z[67]=i*z[67];
    z[68]=z[9] + z[8];
    z[68]=z[32]*z[68];

    r +=  - n<T>(1,9)*z[27] - n<T>(1,24)*z[36] + n<T>(1,32)*z[38] + n<T>(1,48)*z[41]
     - 
      n<T>(5,96)*z[42] + n<T>(3,32)*z[44] + n<T>(77,96)*z[48] + 2*z[50] + n<T>(1,96)*
      z[52] + z[54] + n<T>(5,32)*z[55] + n<T>(1,2)*z[57] + n<T>(11,24)*z[58] + n<T>(1,4)
      *z[59] + n<T>(1,6)*z[60] + n<T>(19,24)*z[61] + n<T>(1,3)*z[62] + n<T>(1,12)*z[63]
       + n<T>(1,8)*z[64] - n<T>(1,16)*z[65] + n<T>(5,12)*z[66] + z[67] + n<T>(5,24)*
      z[68];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1255(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1255(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
