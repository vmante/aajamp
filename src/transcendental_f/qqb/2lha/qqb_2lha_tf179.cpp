#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf179(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[32];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[3];
    z[4]=d[4];
    z[5]=d[8];
    z[6]=d[15];
    z[7]=e[2];
    z[8]=c[3];
    z[9]=c[11];
    z[10]=c[15];
    z[11]=c[17];
    z[12]=c[31];
    z[13]=e[3];
    z[14]=e[10];
    z[15]=f[2];
    z[16]=f[4];
    z[17]=f[10];
    z[18]=f[12];
    z[19]=f[16];
    z[20]=f[19];
    z[21]=z[3]*z[1];
    z[22]=z[1]*z[6];
    z[23]=z[5]*z[1];
    z[24]=n<T>(7,3)*z[21] - 4*z[22] + z[23];
    z[24]=i*z[24];
    z[25]=npow(z[5],2);
    z[26]=2*z[13];
    z[27]=z[26] + z[8];
    z[28]= - z[5] - n<T>(7,6)*z[3];
    z[28]=z[3]*z[28];
    z[29]=i*z[1];
    z[30]=z[29] - z[3];
    z[30]=z[5] + n<T>(7,3)*z[30];
    z[30]=n<T>(1,2)*z[30] - n<T>(4,9)*z[4];
    z[30]=z[4]*z[30];
    z[24]=z[30] + z[24] + z[28] + n<T>(1,2)*z[25] + n<T>(1,3)*z[27] - 4*z[7];
    z[24]=z[4]*z[24];
    z[27]=2*z[21] + z[22] - 5*z[23];
    z[27]=i*z[27];
    z[28]=z[25] - z[14];
    z[30]=4*z[3];
    z[30]= - z[5]*z[30];
    z[31]=z[4] + 5*z[3] + z[29];
    z[31]=z[4]*z[31];
    z[26]=2*z[31] + z[27] + z[30] + z[7] + n<T>(4,3)*z[8] + z[26] - 5*z[28];
    z[27]=z[29] + z[5];
    z[27]=n<T>(13,9)*z[2] - n<T>(5,3)*z[4] - z[3] + n<T>(2,3)*z[27];
    z[27]=z[2]*z[27];
    z[26]=n<T>(1,3)*z[26] + z[27];
    z[26]=z[2]*z[26];
    z[27]=z[17] - z[20];
    z[24]=z[26] + z[24] + n<T>(2,3)*z[15] + n<T>(7,6)*z[16] - n<T>(1,6)*z[18] + n<T>(1,2)
   *z[19] + n<T>(4,3)*z[27];
    z[21]=4*z[21] - 2*z[22] - 7*z[23];
    z[26]=n<T>(1,9)*z[3];
    z[21]=z[21]*z[26];
    z[22]=z[22] - n<T>(7,18)*z[23];
    z[22]=z[5]*z[22];
    z[23]=z[1]*z[7];
    z[21]=z[21] + z[22] + n<T>(2,9)*z[23] - z[11] - n<T>(1,54)*z[9];
    z[21]=i*z[21];
    z[22]=n<T>(7,2)*z[5] - n<T>(4,3)*z[3];
    z[22]=z[3]*z[22];
    z[22]=z[22] + n<T>(7,2)*z[25] + n<T>(5,2)*z[8] - 2*z[7];
    z[22]=z[22]*z[26];
    z[23]=z[6]*z[8];
    z[25]=z[14] - n<T>(1,3)*z[8];
    z[25]=n<T>(5,9)*z[25] + z[7];
    z[25]=z[5]*z[25];

    r +=  - n<T>(5,18)*z[10] + n<T>(1,4)*z[12] + z[21] + z[22] - n<T>(2,9)*z[23]
     +  n<T>(1,3)*z[24] + z[25];
 
    return r;
}

template std::complex<double> qqb_2lha_tf179(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf179(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
