#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf918(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[110];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[5];
    z[7]=d[6];
    z[8]=d[7];
    z[9]=d[8];
    z[10]=d[9];
    z[11]=d[4];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[18];
    z[34]=c[19];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[29];
    z[40]=c[31];
    z[41]=e[3];
    z[42]=e[6];
    z[43]=e[14];
    z[44]=f[0];
    z[45]=f[1];
    z[46]=f[3];
    z[47]=f[4];
    z[48]=f[5];
    z[49]=f[6];
    z[50]=f[7];
    z[51]=f[8];
    z[52]=f[10];
    z[53]=f[11];
    z[54]=f[12];
    z[55]=f[13];
    z[56]=f[16];
    z[57]=f[17];
    z[58]=f[18];
    z[59]=f[23];
    z[60]=f[30];
    z[61]=f[31];
    z[62]=f[32];
    z[63]=f[33];
    z[64]=f[35];
    z[65]=f[36];
    z[66]=f[37];
    z[67]=f[39];
    z[68]=f[41];
    z[69]=f[43];
    z[70]=f[50];
    z[71]=f[51];
    z[72]=f[52];
    z[73]=f[53];
    z[74]=f[54];
    z[75]=f[55];
    z[76]=f[56];
    z[77]=f[58];
    z[78]=f[60];
    z[79]=f[85];
    z[80]=f[86];
    z[81]=n<T>(1,8)*z[5];
    z[82]=z[1]*i;
    z[83]= - z[82] + n<T>(1,2)*z[5];
    z[84]=z[83]*z[81];
    z[85]=n<T>(1,8)*z[2];
    z[86]=3*z[82];
    z[87]=z[85] - z[86] + n<T>(11,8)*z[5];
    z[87]=z[2]*z[87];
    z[88]=z[2] - z[5];
    z[89]= - n<T>(11,2)*z[88];
    z[90]=5*z[82];
    z[91]= - z[90] - z[89];
    z[92]=n<T>(1,4)*z[8];
    z[91]=z[91]*z[92];
    z[93]=n<T>(1,8)*z[6];
    z[94]=n<T>(125,6)*z[6] - n<T>(17,3)*z[8] - 11*z[2] + n<T>(53,3)*z[82] - z[5];
    z[94]=z[94]*z[93];
    z[95]=n<T>(1,2)*z[4];
    z[96]=z[82] - z[95];
    z[97]=n<T>(13,8)*z[4];
    z[96]=z[96]*z[97];
    z[86]= - n<T>(11,4)*z[7] + n<T>(23,8)*z[6] + n<T>(5,4)*z[8] - n<T>(7,8)*z[2]
     - z[86]
    + z[81];
    z[98]=n<T>(1,2)*z[7];
    z[86]=z[86]*z[98];
    z[84]=z[86] + z[96] + z[94] + z[91] + z[84] + z[87];
    z[84]=z[7]*z[84];
    z[86]=n<T>(13,8)*z[6];
    z[87]=n<T>(17,4)*z[82] - z[5];
    z[87]=n<T>(13,4)*z[11] + n<T>(13,24)*z[7] - z[97] - z[9] - z[86] - n<T>(13,12)*
    z[8] + n<T>(1,3)*z[87] + n<T>(13,8)*z[2];
    z[87]=z[11]*z[87];
    z[91]=z[83]*z[5];
    z[94]= - z[82] + n<T>(1,2)*z[8];
    z[96]=z[8]*z[94];
    z[96]= - z[91] - n<T>(13,4)*z[96];
    z[99]=n<T>(1,2)*z[6];
    z[100]=z[82] - z[8];
    z[101]= - z[99] - z[100];
    z[86]=z[101]*z[86];
    z[101]=n<T>(1,2)*z[9];
    z[102]=z[82] - z[5];
    z[103]= - z[101] - z[102];
    z[103]=z[9]*z[103];
    z[97]=z[82]*z[97];
    z[98]=n<T>(1,3)*z[100] - z[98];
    z[98]=z[7]*z[98];
    z[86]=n<T>(1,2)*z[87] + n<T>(13,8)*z[98] + z[97] + z[103] + n<T>(1,3)*z[96] + 
    z[86];
    z[86]=z[11]*z[86];
    z[87]=2*z[5];
    z[96]= - z[82] + z[87];
    z[87]=z[96]*z[87];
    z[96]=z[88] + z[82];
    z[96]= - 2*z[96] + z[8];
    z[96]=z[8]*z[96];
    z[97]=4*z[2];
    z[98]= - n<T>(11,3)*z[3] + z[11] - 2*z[9] - z[8] + z[97] + 4*z[82] - 
    z[5];
    z[98]=z[3]*z[98];
    z[103]=z[2] + 2*z[102];
    z[104]=z[103]*z[2];
    z[103]=z[8] - z[103];
    z[103]=2*z[103] + 5*z[9];
    z[103]=z[9]*z[103];
    z[105]=z[11] + z[102];
    z[105]=z[11]*z[105];
    z[87]=z[98] + 2*z[105] + z[103] + z[96] + z[87] + z[104];
    z[87]=z[3]*z[87];
    z[96]= - n<T>(851,4)*z[8] - n<T>(157,4)*z[5] + 85*z[2];
    z[96]=n<T>(233,12)*z[11] + n<T>(667,12)*z[10] - 25*z[7] + n<T>(1559,24)*z[4]
     + n<T>(5,2)*z[9]
     + n<T>(1,3)*z[96] - n<T>(149,2)*z[6];
    z[96]=n<T>(1,8)*z[96] - n<T>(2,3)*z[3];
    z[96]=z[30]*z[96];
    z[87]=z[87] + z[96];
    z[96]=3*z[5];
    z[98]= - n<T>(5,2)*z[2] + 11*z[82] - z[96];
    z[98]=z[2]*z[98];
    z[98]= - n<T>(13,2)*z[91] + z[98];
    z[103]=13*z[5];
    z[105]= - n<T>(77,3)*z[82] + z[103];
    z[106]=3*z[2];
    z[105]= - n<T>(91,24)*z[6] + n<T>(1,2)*z[105] + z[106];
    z[107]=n<T>(1,4)*z[6];
    z[105]=z[105]*z[107];
    z[101]= - z[82] - z[101];
    z[101]=z[9]*z[101];
    z[108]=z[96] - z[106];
    z[109]=z[90] + z[108];
    z[109]= - n<T>(169,48)*z[4] + n<T>(43,8)*z[9] + n<T>(1,2)*z[109] + n<T>(5,3)*z[6];
    z[95]=z[109]*z[95];
    z[103]=29*z[82] - z[103];
    z[103]=n<T>(47,4)*z[6] + n<T>(1,2)*z[103] - z[106];
    z[103]= - n<T>(71,96)*z[10] - n<T>(5,8)*z[4] + n<T>(1,8)*z[103] + z[9];
    z[103]=z[10]*z[103];
    z[106]=2*z[82];
    z[109]= - z[106] + z[8];
    z[109]=z[8]*z[109];
    z[95]=z[103] + z[95] + n<T>(43,16)*z[101] + z[105] + n<T>(1,4)*z[98] + 
    z[109];
    z[95]=z[10]*z[95];
    z[81]=z[82] + z[81];
    z[81]=z[5]*z[81];
    z[85]=z[85] + z[82] - n<T>(5,8)*z[5];
    z[85]=z[2]*z[85];
    z[89]=n<T>(13,6)*z[8] - n<T>(13,3)*z[82] + z[89];
    z[89]=z[89]*z[92];
    z[92]= - n<T>(133,4)*z[6] - n<T>(11,3)*z[8] + n<T>(23,2)*z[2] - n<T>(109,8)*z[82]
     - 
   11*z[5];
    z[92]=z[92]*z[93];
    z[81]=z[92] + z[89] + z[81] + z[85];
    z[81]=z[6]*z[81];
    z[85]=z[97] + n<T>(121,8)*z[82] - 8*z[5];
    z[85]=z[2]*z[85];
    z[85]=n<T>(185,16)*z[91] + z[85];
    z[89]=n<T>(403,48)*z[6] - n<T>(331,24)*z[82] - z[108];
    z[89]=z[89]*z[107];
    z[92]= - z[5] + z[94];
    z[92]=z[8]*z[92];
    z[93]=n<T>(75,2)*z[9] - 75*z[82] - 161*z[6];
    z[93]=z[9]*z[93];
    z[94]=307*z[82] + 185*z[5];
    z[94]=n<T>(1,2)*z[94] - 121*z[2];
    z[94]= - n<T>(41,2)*z[4] + n<T>(75,4)*z[9] + n<T>(331,12)*z[6] + n<T>(1,3)*z[94]
     + 
   n<T>(19,2)*z[8];
    z[94]=z[4]*z[94];
    z[85]=n<T>(1,16)*z[94] + n<T>(1,32)*z[93] + z[89] + n<T>(1,3)*z[85] + n<T>(19,16)*
    z[92];
    z[85]=z[4]*z[85];
    z[89]=z[82]*z[5];
    z[89]=z[89] + z[30];
    z[92]= - z[3]*z[82];
    z[92]=z[92] - z[89];
    z[92]=z[12]*z[92];
    z[93]=z[82]*z[8];
    z[93]=z[93] + z[30];
    z[94]=z[9]*z[82];
    z[94]=z[94] + z[93];
    z[94]=z[17]*z[94];
    z[97]= - z[3] + z[102];
    z[97]=z[20]*z[97];
    z[98]= - z[9] + z[100];
    z[98]=z[27]*z[98];
    z[92]=z[78] + z[92] + z[94] + z[97] + z[98];
    z[94]=z[10] - z[6];
    z[94]=n<T>(185,12)*z[4] + n<T>(149,12)*z[2] - n<T>(185,6)*z[82] + z[96] + 3*
    z[94];
    z[94]=z[18]*z[94];
    z[96]= - 47*z[82] - n<T>(13,2)*z[5];
    z[96]=n<T>(1,4)*z[96] + 5*z[2];
    z[96]=z[2]*z[96];
    z[96]= - 11*z[91] + z[96];
    z[96]=z[2]*z[96];
    z[97]= - n<T>(77,8)*z[2] - z[90] + n<T>(77,4)*z[5];
    z[97]=z[2]*z[97];
    z[97]= - n<T>(97,8)*z[91] + z[97];
    z[90]=n<T>(1,3)*z[8] + n<T>(5,6)*z[2] - z[90] - n<T>(97,48)*z[5];
    z[90]=z[8]*z[90];
    z[90]=n<T>(1,3)*z[97] + z[90];
    z[90]=z[8]*z[90];
    z[97]=z[102]*npow(z[5],2);
    z[90]=z[39] - z[56] + z[94] + z[90] - n<T>(5,4)*z[97] + n<T>(1,3)*z[96];
    z[94]= - z[6]*z[82];
    z[89]=z[94] - z[89];
    z[89]=z[15]*z[89];
    z[94]=z[7]*z[82];
    z[93]=z[94] + z[93];
    z[93]=z[16]*z[93];
    z[94]=z[6] - z[102];
    z[94]=z[22]*z[94];
    z[96]=z[7] - z[100];
    z[96]=z[25]*z[96];
    z[89]=z[89] + z[93] + z[94] + z[96];
    z[93]= - z[106] + z[88];
    z[93]=2*z[93] - z[8];
    z[93]=z[8]*z[93];
    z[91]=z[93] - 7*z[91] - z[104];
    z[93]=z[82] + z[99];
    z[93]=z[6]*z[93];
    z[94]=z[8] - z[2];
    z[96]= - n<T>(1,32)*z[82] - 7*z[5];
    z[94]=n<T>(1,2)*z[96] + 2*z[94];
    z[94]= - n<T>(803,288)*z[9] + n<T>(1,3)*z[94] + n<T>(161,64)*z[6];
    z[94]=z[9]*z[94];
    z[91]=z[94] + n<T>(1,3)*z[91] + n<T>(161,32)*z[93];
    z[91]=z[9]*z[91];
    z[93]=z[82]*z[4];
    z[93]=z[93] + z[30];
    z[94]= - z[11]*z[82];
    z[94]=z[94] - z[93];
    z[94]=z[14]*z[94];
    z[96]=z[82] - z[4];
    z[97]=z[11] - z[96];
    z[97]=z[21]*z[97];
    z[94]= - z[68] + z[94] + z[97];
    z[88]=z[100] + z[88];
    z[97]= - 5*z[3] + 2*z[88] - 7*z[9];
    z[97]=z[26]*z[97];
    z[98]= - z[11] - z[3];
    z[98]=z[41]*z[98];
    z[97]= - z[59] - z[97] - z[98] + z[52] + z[33];
    z[98]=z[10]*z[82];
    z[93]=z[98] + z[93];
    z[93]=z[13]*z[93];
    z[96]= - z[10] + z[96];
    z[96]=z[28]*z[96];
    z[93]=z[93] + z[96];
    z[88]=3*z[88] - n<T>(19,3)*z[6];
    z[88]=n<T>(1,2)*z[88] - n<T>(5,3)*z[7];
    z[88]=z[23]*z[88];
    z[96]=z[5] + z[7] - z[6];
    z[82]= - n<T>(97,6)*z[8] - n<T>(67,6)*z[2] + n<T>(97,3)*z[82] - 5*z[96];
    z[82]=z[24]*z[82];
    z[96]=z[58] + z[65] - z[60];
    z[98]=z[67] - z[66] + z[45] + z[32];
    z[83]=n<T>(1,2)*z[2] + z[83];
    z[83]=z[19]*z[83];
    z[83]=z[83] + z[64];
    z[99]=n<T>(113,12)*z[37] + n<T>(47,24)*z[35] - n<T>(103,576)*z[31];
    z[99]=i*z[99];
    z[100]= - n<T>(283,48)*z[10] + z[7] - n<T>(173,24)*z[6] + z[102];
    z[100]=z[29]*z[100];
    z[101]=z[7] + z[11];
    z[101]=z[42]*z[101];
    z[102]= - z[9] - z[10];
    z[102]=z[43]*z[102];

    r +=  - n<T>(85,72)*z[34] - n<T>(5,6)*z[36] - n<T>(107,24)*z[38] + n<T>(1,36)*z[40]
       + n<T>(61,8)*z[44] + n<T>(221,96)*z[46] - n<T>(11,6)*z[47] + n<T>(31,12)*z[48]
       - z[49] - n<T>(16,3)*z[50] - 5*z[51] - n<T>(121,96)*z[53] + n<T>(1,6)*z[54]
       - n<T>(23,12)*z[55] + n<T>(3,32)*z[57] + n<T>(43,24)*z[61] + n<T>(11,16)*z[62]
       + 6*z[63] - n<T>(3,4)*z[69] + n<T>(43,16)*z[70] + n<T>(211,192)*z[71] + n<T>(9,16)*z[72]
     + n<T>(21,8)*z[73] - n<T>(19,12)*z[74] - n<T>(75,64)*z[75] - z[76]
       + n<T>(161,64)*z[77] - n<T>(3,8)*z[79] + n<T>(1,8)*z[80] + z[81] + n<T>(1,4)*
      z[82] + n<T>(25,12)*z[83] + z[84] + z[85] + z[86] + n<T>(1,3)*z[87] + 
      z[88] + n<T>(5,4)*z[89] + n<T>(1,2)*z[90] + z[91] + 2*z[92] + n<T>(109,48)*
      z[93] + n<T>(13,8)*z[94] + z[95] + n<T>(13,24)*z[96] - n<T>(2,3)*z[97] - n<T>(13,16)*z[98]
     + z[99]
     + z[100]
     + n<T>(13,6)*z[101]
     + n<T>(75,16)*z[102];
 
    return r;
}

template std::complex<double> qqb_2lha_tf918(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf918(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
