#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1333(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[59];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[2];
    z[6]=d[8];
    z[7]=d[11];
    z[8]=d[3];
    z[9]=d[9];
    z[10]=d[12];
    z[11]=d[4];
    z[12]=d[7];
    z[13]=d[17];
    z[14]=e[5];
    z[15]=e[7];
    z[16]=e[9];
    z[17]=e[12];
    z[18]=c[3];
    z[19]=c[11];
    z[20]=c[15];
    z[21]=c[17];
    z[22]=c[31];
    z[23]=e[6];
    z[24]=e[13];
    z[25]=e[14];
    z[26]=f[30];
    z[27]=f[31];
    z[28]=f[32];
    z[29]=f[33];
    z[30]=f[35];
    z[31]=f[36];
    z[32]=f[39];
    z[33]=f[43];
    z[34]=f[50];
    z[35]=f[51];
    z[36]=f[55];
    z[37]=f[58];
    z[38]=f[68];
    z[39]=z[4] - z[3];
    z[40]=z[39]*z[8];
    z[41]=z[1]*i;
    z[42]=z[41]*z[3];
    z[43]=npow(z[3],2);
    z[44]=z[41] + z[3];
    z[45]=z[4] + z[44];
    z[45]=z[4]*z[45];
    z[46]= - z[12]*z[39];
    z[45]=z[46] - z[40] + z[45] - 2*z[43] - z[42];
    z[46]=z[2]*z[39];
    z[45]=2*z[45] + z[46];
    z[45]=z[2]*z[45];
    z[46]=n<T>(1,2)*z[8];
    z[47]=n<T>(5,6)*z[11] - n<T>(11,6)*z[9] + n<T>(2,3)*z[6] - n<T>(1,2)*z[5] - n<T>(13,24)*
    z[12] + z[46] + 7*z[3] - n<T>(1,6)*z[4];
    z[47]=z[18]*z[47];
    z[48]= - z[41] - z[2] + z[12] + z[8];
    z[48]=n<T>(23,2)*z[4] + 17*z[3] + 4*z[48];
    z[48]=z[15]*z[48];
    z[45]=z[45] + z[47] + z[48] - z[38];
    z[47]=n<T>(1,2)*z[43];
    z[48]=z[47] + z[42];
    z[44]=z[46] - z[44];
    z[44]=z[8]*z[44];
    z[49]=n<T>(1,3)*z[6];
    z[50]=z[41] - z[5];
    z[51]=2*z[50] + z[6];
    z[51]=z[51]*z[49];
    z[52]=npow(z[5],2);
    z[44]=z[51] + n<T>(1,3)*z[52] + z[44] + z[48];
    z[44]=z[9]*z[44];
    z[51]=n<T>(1,3)*z[41];
    z[52]=z[51] - z[3];
    z[53]=n<T>(1,3)*z[4];
    z[52]=n<T>(1,3)*z[11] - n<T>(1,6)*z[12] + n<T>(1,2)*z[52] + z[53];
    z[52]=z[11]*z[52];
    z[52]=z[48] - z[52];
    z[54]=z[41]*z[53];
    z[55]=z[51] + z[3];
    z[56]= - n<T>(1,12)*z[12] + n<T>(1,2)*z[55] - z[53];
    z[56]=z[12]*z[56];
    z[52]=z[56] + z[54] - n<T>(1,2)*z[52];
    z[52]=z[11]*z[52];
    z[54]= - z[5] - z[9];
    z[54]=z[41]*z[54];
    z[54]= - z[18] + z[54];
    z[54]=z[7]*z[54];
    z[50]=z[9] - z[50];
    z[50]=z[17]*z[50];
    z[56]=z[6] + z[9];
    z[56]=z[25]*z[56];
    z[50]=z[54] + z[50] + z[56] - z[34] + z[33] - z[30];
    z[54]=z[43]*z[41];
    z[56]=npow(z[3],3);
    z[54]=n<T>(95,9)*z[56] + n<T>(9,2)*z[54];
    z[56]= - n<T>(17,3)*z[3] + 5*z[41];
    z[56]=n<T>(1,2)*z[56] + n<T>(17,9)*z[4];
    z[56]=z[4]*z[56];
    z[56]= - n<T>(19,6)*z[43] + z[56];
    z[56]=z[4]*z[56];
    z[54]=z[37] - n<T>(1,2)*z[54] - z[56] + z[35] + z[28];
    z[56]=z[12] + z[4];
    z[57]= - z[41]*z[56];
    z[57]= - z[18] + z[57];
    z[57]=z[13]*z[57];
    z[56]=z[41] - z[56];
    z[56]=z[16]*z[56];
    z[56]=z[57] + z[56];
    z[57]=n<T>(1,3)*z[3] + z[41];
    z[46]= - z[46] + n<T>(1,2)*z[57] + z[53];
    z[46]=z[8]*z[46];
    z[53]=z[43] - z[42];
    z[57]= - z[41] + n<T>(1,2)*z[4];
    z[58]= - 2*z[3] - z[57];
    z[58]=z[4]*z[58];
    z[53]=4*z[53] + z[58];
    z[46]=n<T>(1,3)*z[53] + z[46];
    z[46]=z[8]*z[46];
    z[43]= - n<T>(5,2)*z[43] + n<T>(31,3)*z[42];
    z[53]=z[4]*z[57];
    z[57]= - n<T>(31,12)*z[3] + 3*z[4];
    z[57]=z[12]*z[57];
    z[43]=z[57] + n<T>(1,2)*z[43] + n<T>(5,3)*z[53];
    z[40]=n<T>(2,3)*z[40] + n<T>(1,2)*z[43];
    z[40]=z[12]*z[40];
    z[43]= - z[51] + n<T>(1,3)*z[5];
    z[51]= - n<T>(1,2)*z[3] + z[43];
    z[51]=z[5]*z[51];
    z[42]=z[51] - z[47] + z[42];
    z[42]=z[5]*z[42];
    z[43]= - z[3] - z[43];
    z[43]=n<T>(1,2)*z[43] + z[49];
    z[43]=z[6]*z[43];
    z[47]= - n<T>(1,6)*z[5] + z[55];
    z[47]=z[5]*z[47];
    z[43]=z[43] + z[47] - z[48];
    z[43]=z[6]*z[43];
    z[39]=z[39] - z[8];
    z[47]= - z[41]*z[39];
    z[47]=z[18] + z[47];
    z[47]=z[10]*z[47];
    z[39]=z[41] + z[39];
    z[39]=z[14]*z[39];
    z[41]=z[32] + z[22];
    z[48]=z[4] + z[11];
    z[48]=z[23]*z[48];
    z[48]=z[48] - z[26];
    z[49]= - n<T>(1,2)*z[21] - n<T>(7,24)*z[19];
    z[49]=i*z[49];
    z[51]=z[3] + z[9];
    z[51]=z[24]*z[51];

    r += n<T>(25,12)*z[20] - n<T>(47,24)*z[27] - n<T>(19,3)*z[29] + n<T>(5,24)*z[31]
     +  n<T>(1,6)*z[36] + z[39] + z[40] - n<T>(5,8)*z[41] + z[42] + z[43] + z[44]
       + n<T>(1,3)*z[45] + z[46] + z[47] + n<T>(5,6)*z[48] + z[49] + n<T>(2,3)*
      z[50] + z[51] + n<T>(5,2)*z[52] - n<T>(1,2)*z[54] + n<T>(13,6)*z[56];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1333(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1333(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
