#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1430(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[114];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[2];
    z[5]=d[3];
    z[6]=d[6];
    z[7]=d[7];
    z[8]=d[8];
    z[9]=d[4];
    z[10]=d[5];
    z[11]=d[9];
    z[12]=d[15];
    z[13]=d[11];
    z[14]=d[16];
    z[15]=d[12];
    z[16]=d[17];
    z[17]=d[18];
    z[18]=e[0];
    z[19]=e[1];
    z[20]=e[2];
    z[21]=e[4];
    z[22]=e[5];
    z[23]=e[7];
    z[24]=e[8];
    z[25]=e[9];
    z[26]=e[10];
    z[27]=e[11];
    z[28]=e[12];
    z[29]=e[13];
    z[30]=c[3];
    z[31]=c[11];
    z[32]=c[15];
    z[33]=c[19];
    z[34]=c[21];
    z[35]=c[23];
    z[36]=c[25];
    z[37]=c[26];
    z[38]=c[28];
    z[39]=c[31];
    z[40]=e[3];
    z[41]=e[6];
    z[42]=e[14];
    z[43]=f[0];
    z[44]=f[1];
    z[45]=f[2];
    z[46]=f[3];
    z[47]=f[4];
    z[48]=f[5];
    z[49]=f[6];
    z[50]=f[7];
    z[51]=f[10];
    z[52]=f[11];
    z[53]=f[12];
    z[54]=f[14];
    z[55]=f[15];
    z[56]=f[16];
    z[57]=f[17];
    z[58]=f[18];
    z[59]=f[21];
    z[60]=f[23];
    z[61]=f[30];
    z[62]=f[31];
    z[63]=f[32];
    z[64]=f[33];
    z[65]=f[34];
    z[66]=f[35];
    z[67]=f[36];
    z[68]=f[37];
    z[69]=f[39];
    z[70]=f[43];
    z[71]=f[50];
    z[72]=f[51];
    z[73]=f[52];
    z[74]=f[53];
    z[75]=f[54];
    z[76]=f[55];
    z[77]=f[56];
    z[78]=f[58];
    z[79]=f[74];
    z[80]=f[75];
    z[81]=n<T>(1,8)*z[3];
    z[82]=n<T>(1,8)*z[10];
    z[83]=z[1]*i;
    z[84]= - n<T>(17,24)*z[8] - z[82] - z[81] - n<T>(5,3)*z[83] + n<T>(1,8)*z[5];
    z[84]=z[8]*z[84];
    z[85]=n<T>(3,8)*z[7];
    z[86]=n<T>(1,2)*z[3];
    z[87]= - n<T>(1,8)*z[83] - z[5];
    z[87]= - z[6] + n<T>(1,8)*z[8] + n<T>(17,6)*z[10] + z[85] + n<T>(1,3)*z[87] - 
    z[86];
    z[87]=n<T>(1,2)*z[87] - z[11];
    z[87]=z[11]*z[87];
    z[88]=n<T>(1,2)*z[5];
    z[89]=z[88] - z[83];
    z[90]=n<T>(1,3)*z[5];
    z[91]=z[89]*z[90];
    z[92]= - n<T>(1,4)*z[5] + z[86] + z[83];
    z[92]=z[92]*z[86];
    z[93]=n<T>(1,2)*z[7];
    z[94]=z[93] - z[83];
    z[95]= - z[3] + z[94];
    z[85]=z[95]*z[85];
    z[81]=n<T>(1,4)*z[10] + z[90] + z[81];
    z[81]=z[10]*z[81];
    z[95]=n<T>(1,2)*z[6];
    z[96]=z[10] - z[95];
    z[96]=z[6]*z[96];
    z[97]= - n<T>(29,16)*z[4] - z[10] + n<T>(37,8)*z[8];
    z[97]=z[4]*z[97];
    z[81]=z[87] + n<T>(1,3)*z[97] + z[96] + z[84] + z[81] + z[85] - z[91] + 
    z[92];
    z[81]=z[11]*z[81];
    z[84]=z[89]*z[5];
    z[85]=3*z[3];
    z[87]=3*z[83];
    z[92]=z[85] - z[87] - z[5];
    z[92]=z[92]*z[86];
    z[92]=n<T>(5,3)*z[84] + z[92];
    z[96]=z[88] + z[83];
    z[97]=n<T>(1,4)*z[3];
    z[98]=n<T>(1,6)*z[7];
    z[99]=z[98] - n<T>(1,3)*z[96] - z[97];
    z[99]=z[7]*z[99];
    z[100]=n<T>(1,4)*z[8];
    z[101]=n<T>(7,2)*z[83] - z[5];
    z[101]= - n<T>(1,6)*z[2] - z[100] + z[10] + z[98] + n<T>(1,3)*z[101] + n<T>(5,4)
   *z[3];
    z[102]=n<T>(1,2)*z[2];
    z[101]=z[101]*z[102];
    z[103]=n<T>(1,2)*z[10];
    z[104]=n<T>(5,2)*z[10] - z[7] - z[5] - z[3];
    z[104]=z[104]*z[103];
    z[105]=z[83] - z[7];
    z[106]=z[3] - z[5];
    z[107]= - z[8] - z[106] - z[105];
    z[107]=z[107]*z[100];
    z[92]=z[101] + z[107] + z[104] + n<T>(1,2)*z[92] + z[99];
    z[92]=z[2]*z[92];
    z[88]=n<T>(3,2)*z[3] + z[87] - z[88];
    z[88]=z[3]*z[88];
    z[99]= - n<T>(7,3)*z[7] + n<T>(13,3)*z[5] + z[3];
    z[99]=z[99]*z[103];
    z[101]=z[83] - z[5];
    z[104]= - z[102] - z[10] + z[7] - z[101];
    z[104]=z[2]*z[104];
    z[107]= - z[5] + n<T>(5,2)*z[83];
    z[108]= - n<T>(7,2)*z[7] + z[107];
    z[108]=z[7]*z[108];
    z[109]= - 29*z[83] - 13*z[5];
    z[109]=n<T>(37,3)*z[10] - 5*z[7] + n<T>(1,3)*z[109] + z[3];
    z[109]=n<T>(1,6)*z[6] + n<T>(1,4)*z[109] - z[2];
    z[109]=z[6]*z[109];
    z[88]=z[109] + z[104] + z[99] + z[108] - n<T>(13,6)*z[84] + z[88];
    z[88]=z[88]*z[95];
    z[99]=3*z[5];
    z[104]= - z[89]*z[99];
    z[108]=n<T>(5,2)*z[3];
    z[109]= - z[87] + z[108];
    z[109]=z[3]*z[109];
    z[110]=z[5] - z[94];
    z[110]=z[7]*z[110];
    z[111]= - z[83] + z[103];
    z[112]=n<T>(103,12)*z[10];
    z[111]=z[111]*z[112];
    z[113]=n<T>(13,6)*z[8] - n<T>(13,3)*z[83] - 29*z[10];
    z[113]=z[113]*z[100];
    z[104]=z[113] + z[111] + z[110] + z[104] + z[109];
    z[85]=n<T>(13,12)*z[8] + z[112] - z[7] + z[85] + n<T>(125,6)*z[83] - z[99];
    z[109]=n<T>(5,4)*z[6];
    z[85]= - n<T>(31,8)*z[4] + z[109] + n<T>(1,4)*z[85] + z[2];
    z[110]=n<T>(1,2)*z[4];
    z[85]=z[85]*z[110];
    z[111]= - n<T>(1,4)*z[2] + z[89];
    z[111]=z[2]*z[111];
    z[95]=z[95] - z[83] - z[3];
    z[95]=z[95]*z[109];
    z[85]=z[85] + z[95] + n<T>(1,4)*z[104] + z[111];
    z[85]=z[4]*z[85];
    z[87]= - 3*z[7] + z[108] + z[87] - z[90];
    z[87]=z[87]*z[93];
    z[95]=z[97] - z[107];
    z[95]=z[3]*z[95];
    z[87]=z[87] - z[91] + z[95];
    z[87]=z[7]*z[87];
    z[91]=z[5]*z[101];
    z[95]=11*z[83];
    z[97]= - n<T>(95,2)*z[3] - z[95] + n<T>(61,4)*z[5];
    z[97]=z[3]*z[97];
    z[91]= - n<T>(43,2)*z[91] + z[97];
    z[91]=z[3]*z[91];
    z[97]=n<T>(25,3)*z[83];
    z[104]= - z[97] + 7*z[5];
    z[104]=z[104]*npow(z[5],2);
    z[87]=z[87] + n<T>(1,2)*z[104] + n<T>(1,3)*z[91];
    z[90]=7*z[83] - z[90];
    z[90]=z[5]*z[90];
    z[91]= - n<T>(7,2)*z[3] + z[83] + z[99];
    z[91]=z[91]*z[86];
    z[90]=z[90] + z[91];
    z[91]= - n<T>(25,3)*z[10] + n<T>(59,24)*z[7] + z[86] - n<T>(197,48)*z[83] - 
    z[99];
    z[91]=z[10]*z[91];
    z[99]=n<T>(83,24)*z[7] - n<T>(83,12)*z[83] + z[5];
    z[99]=z[7]*z[99];
    z[90]=z[91] + n<T>(1,2)*z[90] + z[99];
    z[90]=z[10]*z[90];
    z[91]= - z[8] + z[83] + z[106];
    z[91]= - n<T>(55,3)*z[10] + 3*z[91];
    z[91]=n<T>(1,2)*z[91] - n<T>(23,3)*z[11];
    z[91]=z[29]*z[91];
    z[99]= - n<T>(71,6)*z[5] - 21*z[3];
    z[99]=n<T>(19,18)*z[11] + n<T>(33,8)*z[9] + n<T>(197,48)*z[4] + n<T>(19,144)*z[6]
     - 
   n<T>(29,18)*z[2] + n<T>(13,36)*z[8] - n<T>(101,12)*z[10] + n<T>(1,8)*z[99] + n<T>(7,9)*
    z[7];
    z[99]=z[30]*z[99];
    z[87]= - z[59] + z[99] + n<T>(1,2)*z[87] + z[90] + z[91] - z[70];
    z[90]=z[101] + z[86];
    z[90]=z[90]*z[3];
    z[91]= - n<T>(5,16)*z[84] + z[90];
    z[86]= - n<T>(1,4)*z[7] + z[86] - z[96];
    z[86]=z[86]*z[93];
    z[93]=n<T>(31,4)*z[10] + n<T>(31,2)*z[83] + z[106];
    z[82]=z[93]*z[82];
    z[93]=91*z[3] + z[95] - n<T>(35,2)*z[5];
    z[93]= - n<T>(15,4)*z[8] + n<T>(43,8)*z[10] + n<T>(1,12)*z[93] + z[7];
    z[93]=z[93]*z[100];
    z[82]=z[93] + z[82] + n<T>(7,3)*z[91] + z[86];
    z[82]=z[8]*z[82];
    z[86]=z[7]*z[94];
    z[84]=z[86] - n<T>(137,2)*z[84] + 5*z[90];
    z[86]=n<T>(17,4)*z[10] - n<T>(15,2)*z[7] + n<T>(17,2)*z[83] + z[106];
    z[86]=z[10]*z[86];
    z[90]=n<T>(49,4)*z[8];
    z[91]= - n<T>(1,2)*z[8] - z[101];
    z[91]=z[91]*z[90];
    z[84]=z[91] + n<T>(1,6)*z[84] + z[86];
    z[86]= - z[97] - z[106];
    z[86]= - n<T>(3,2)*z[6] - z[103] + n<T>(1,2)*z[86] + n<T>(11,3)*z[7];
    z[86]=z[6]*z[86];
    z[84]=n<T>(1,2)*z[84] + z[86];
    z[86]=45*z[83] - n<T>(137,3)*z[5];
    z[86]= - z[90] + n<T>(13,2)*z[10] + z[98] + n<T>(1,4)*z[86] + n<T>(71,3)*z[3];
    z[90]=z[9] + z[6];
    z[86]=z[4] + n<T>(1,8)*z[86] - z[2] - n<T>(19,24)*z[90];
    z[86]=z[9]*z[86];
    z[91]=z[83]*z[4];
    z[84]=z[86] + n<T>(1,2)*z[84] - 2*z[91];
    z[84]=z[9]*z[84];
    z[86]= - z[110] - z[102] + z[83];
    z[86]=z[18]*z[86];
    z[86]=z[86] + z[74];
    z[91]=z[91] + z[30];
    z[93]=z[9]*z[83];
    z[93]=z[93] + z[91];
    z[93]=z[14]*z[93];
    z[95]=z[83] - z[4];
    z[96]= - z[9] + z[95];
    z[96]=z[21]*z[96];
    z[93]= - z[43] + z[93] + z[96] + z[64] + z[50];
    z[94]= - z[102] - z[94];
    z[94]=z[24]*z[94];
    z[94]=z[48] + z[94] + z[75];
    z[96]=z[83]*z[7];
    z[96]=z[96] + z[30];
    z[97]=z[8]*z[83];
    z[97]=z[97] + z[96];
    z[97]=z[17]*z[97];
    z[98]= - z[8] + z[105];
    z[98]=z[27]*z[98];
    z[97]=z[97] + z[98];
    z[98]=z[6]*z[83];
    z[96]=z[98] + z[96];
    z[96]=z[16]*z[96];
    z[98]=z[6] - z[105];
    z[98]=z[25]*z[98];
    z[96]=z[96] + z[98];
    z[98]=z[83]*z[5];
    z[98]=z[98] + z[30];
    z[99]= - z[10]*z[83];
    z[99]=z[99] - z[98];
    z[99]=z[15]*z[99];
    z[100]=z[10] - z[101];
    z[100]=z[22]*z[100];
    z[99]=z[99] + z[100];
    z[100]=z[11]*z[83];
    z[91]=z[100] + z[91];
    z[91]=z[13]*z[91];
    z[95]= - z[11] + z[95];
    z[95]=z[28]*z[95];
    z[91]=z[91] + z[95];
    z[83]= - z[3]*z[83];
    z[83]=z[83] - z[98];
    z[83]=z[12]*z[83];
    z[95]=z[101] - z[3];
    z[98]=z[20]*z[95];
    z[83]=z[83] + z[98];
    z[98]= - z[7] - n<T>(47,12)*z[3] - z[101];
    z[98]=z[11] + z[102] - n<T>(35,24)*z[8] + n<T>(1,2)*z[98] - z[10];
    z[98]=z[26]*z[98];
    z[89]= - z[102] - z[89];
    z[89]=z[19]*z[89];
    z[90]= - z[41]*z[90];
    z[90]=z[36] + z[90] - z[45];
    z[100]=z[57] + z[80] + z[79];
    z[101]=z[38] - z[32];
    z[102]=z[65] + z[60];
    z[103]= - z[8] - z[11];
    z[103]=z[42]*z[103];
    z[103]=z[103] + z[71];
    z[104]= - n<T>(20,3)*z[37] - n<T>(13,12)*z[35] + z[34] - n<T>(11,144)*z[31];
    z[104]=i*z[104];
    z[95]=z[95] + z[9];
    z[95]= - n<T>(49,12)*z[6] + z[2] - n<T>(55,12)*z[10] - z[7] + n<T>(1,2)*z[95];
    z[95]=z[23]*z[95];
    z[105]= - z[3] - z[9];
    z[105]=z[40]*z[105];

    r += n<T>(41,36)*z[33] + n<T>(31,6)*z[39] + z[44] - n<T>(3,8)*z[46] - n<T>(59,96)*
      z[47] - n<T>(9,16)*z[49] + n<T>(35,24)*z[51] - n<T>(1,24)*z[52] + n<T>(113,96)*
      z[53] + n<T>(7,8)*z[54] + n<T>(15,8)*z[55] - n<T>(57,32)*z[56] - n<T>(2,3)*z[58]
       + n<T>(11,6)*z[61] + n<T>(107,48)*z[62] + n<T>(7,6)*z[63] + n<T>(13,12)*z[66]
     - 
      n<T>(1,48)*z[67] + n<T>(5,8)*z[68] + n<T>(11,16)*z[69] + n<T>(103,96)*z[72]
     + n<T>(1,6)*z[73] - n<T>(13,96)*z[76]
     + n<T>(3,16)*z[77]
     + n<T>(51,32)*z[78]
     + z[81]
       + z[82] + n<T>(43,24)*z[83] + z[84] + z[85] + 3*z[86] + n<T>(1,2)*z[87]
       + z[88] + n<T>(7,3)*z[89] + n<T>(1,12)*z[90] + n<T>(29,24)*z[91] + z[92] + 2
      *z[93] + n<T>(1,3)*z[94] + z[95] + n<T>(9,4)*z[96] + n<T>(3,4)*z[97] + z[98]
       + n<T>(19,12)*z[99] - n<T>(1,8)*z[100] + n<T>(17,6)*z[101] + n<T>(1,4)*z[102]
     +  n<T>(37,24)*z[103] + z[104] + n<T>(131,24)*z[105];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1430(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1430(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
