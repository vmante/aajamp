#ifndef QQB_2LHA_TF_DECL_H
#define QQB_2LHA_TF_DECL_H

#include <array>
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
std::complex<T> qqb_2lha_tf1(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf2(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf3(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf4(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf5(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf6(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf7(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf8(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf9(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf10(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf11(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf12(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf13(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf14(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf15(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf16(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf17(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf18(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf19(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf20(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf21(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf22(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf23(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf24(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf25(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf26(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf27(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf28(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf29(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf30(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf31(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf32(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf33(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf34(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf35(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf36(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf37(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf38(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf39(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf40(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf41(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf42(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf43(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf44(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf45(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf46(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf47(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf48(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf49(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf50(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf51(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf52(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf53(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf54(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf55(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf56(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf57(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf58(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf59(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf60(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf61(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf62(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf63(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf64(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf65(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf66(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf67(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf68(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf69(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf70(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf71(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf72(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf73(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf74(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf75(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf76(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf77(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf78(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf79(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf80(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf81(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf82(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf83(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf84(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf85(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf86(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf87(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf88(
  const std::array<T,85>&);

template <class T>
std::complex<T> qqb_2lha_tf89(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf90(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf91(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf92(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf93(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf94(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf95(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf96(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf97(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf98(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf99(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf100(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf101(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf102(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf103(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf104(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf105(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf106(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf107(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf108(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf109(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf110(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf111(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf112(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf113(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf114(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf115(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf116(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf117(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf118(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf119(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf120(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf121(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf122(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf123(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf124(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf125(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf126(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf127(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf128(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf129(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf130(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf131(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf132(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf133(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf134(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf135(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf136(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf137(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf138(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf139(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf140(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf141(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf142(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf143(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf144(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf145(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf146(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf147(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf148(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf149(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf150(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf151(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf152(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf153(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf154(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf155(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf156(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf157(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf158(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf159(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf160(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf161(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf162(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf163(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf164(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf165(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf166(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf167(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf168(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf169(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf170(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf171(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf172(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf173(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf174(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf175(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf176(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf177(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf178(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf179(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf180(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf181(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf182(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf183(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf184(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf185(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf186(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf187(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf188(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf189(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf190(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf191(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf192(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf193(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf194(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf195(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf196(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf197(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf198(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf199(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf200(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf201(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf202(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf203(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf204(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf205(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf206(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf207(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf208(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf209(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf210(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf211(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf212(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf213(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf214(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf215(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf216(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf217(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf218(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf219(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf220(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf221(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf222(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf223(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf224(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf225(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf226(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf227(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf228(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf229(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf230(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf231(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf232(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf233(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf234(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf235(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf236(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf237(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf238(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf239(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf240(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf241(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf242(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf243(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf244(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf245(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf246(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf247(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf248(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf249(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf250(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf251(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf252(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf253(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf254(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf255(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf256(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf257(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf258(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf259(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf260(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf261(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf262(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf263(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf264(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf265(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf266(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf267(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf268(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf269(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf270(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf271(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf272(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf273(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf274(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf275(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf276(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf277(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf278(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf279(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf280(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf281(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf282(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf283(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf284(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf285(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf286(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf287(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf288(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf289(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf290(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf291(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf292(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf293(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf294(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf295(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf296(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf297(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf298(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf299(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf300(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf301(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf302(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf303(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf304(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf305(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf306(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf307(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf308(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf309(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf310(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf311(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf312(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf313(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf314(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf315(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf316(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf317(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf318(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf319(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf320(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf321(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf322(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf323(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf324(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf325(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf326(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf327(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf328(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf329(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf330(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf331(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf332(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf333(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf334(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf335(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf336(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf337(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf338(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf339(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf340(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf341(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf342(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf343(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf344(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf345(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf346(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf347(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf348(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf349(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf350(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf351(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf352(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf353(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf354(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf355(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf356(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf357(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf358(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf359(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf360(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf361(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf362(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf363(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf364(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf365(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf366(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf367(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf368(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf369(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf370(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf371(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf372(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf373(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf374(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf375(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf376(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf377(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf378(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf379(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf380(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf381(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf382(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf383(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf384(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf385(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf386(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf387(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf388(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf389(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf390(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf391(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf392(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf393(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf394(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf395(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf396(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf397(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf398(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf399(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf400(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf401(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf402(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf403(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf404(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf405(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf406(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf407(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf408(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf409(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf410(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf411(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf412(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf413(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf414(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf415(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf416(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf417(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf418(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf419(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf420(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf421(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf422(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf423(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf424(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf425(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf426(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf427(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf428(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf429(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf430(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf431(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf432(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf433(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf434(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf435(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf436(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf437(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf438(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf439(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf440(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf441(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf442(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf443(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf444(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf445(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf446(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf447(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf448(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf449(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf450(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf451(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf452(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf453(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf454(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf455(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf456(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf457(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf458(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf459(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf460(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf461(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf462(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf463(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf464(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf465(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf466(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf467(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf468(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf469(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf470(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf471(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf472(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf473(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf474(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf475(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf476(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf477(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf478(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf479(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf480(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf481(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf482(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf483(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf484(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf485(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf486(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf487(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf488(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf489(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf490(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf491(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf492(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf493(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf494(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf495(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf496(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf497(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf498(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf499(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf500(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf501(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf502(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf503(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf504(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf505(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf506(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf507(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf508(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf509(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf510(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf511(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf512(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf513(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf514(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf515(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf516(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf517(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf518(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf519(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf520(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf521(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf522(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf523(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf524(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf525(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf526(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf527(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf528(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf529(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf530(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf531(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf532(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf533(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf534(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf535(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf536(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf537(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf538(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf539(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf540(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf541(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf542(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf543(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf544(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf545(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf546(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf547(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf548(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf549(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf550(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf551(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf552(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf553(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf554(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf555(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf556(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf557(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf558(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf559(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf560(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf561(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf562(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf563(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf564(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf565(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf566(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf567(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf568(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf569(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf570(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf571(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf572(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf573(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf574(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf575(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf576(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf577(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf578(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf579(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf580(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf581(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf582(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf583(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf584(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf585(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf586(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf587(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf588(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf589(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf590(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf591(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf592(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf593(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf594(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf595(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf596(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf597(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf598(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf599(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf600(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf601(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf602(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf603(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf604(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf605(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf606(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf607(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf608(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf609(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf610(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf611(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf612(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf613(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf614(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf615(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf616(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf617(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf618(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf619(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf620(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf621(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf622(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf623(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf624(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf625(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf626(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf627(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf628(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf629(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf630(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf631(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf632(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf633(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf634(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf635(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf636(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf637(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf638(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf639(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf640(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf641(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf642(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf643(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf644(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf645(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf646(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf647(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf648(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf649(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf650(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf651(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf652(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf653(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf654(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf655(
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf656(
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf657(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf658(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf659(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf660(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf661(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf662(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf663(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf664(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf665(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf666(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf667(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf668(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf669(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf670(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf671(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf672(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf673(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf674(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf675(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf676(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf677(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf678(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf679(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf680(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf681(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf682(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf683(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf684(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf685(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf686(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf687(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf688(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf689(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf690(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf691(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf692(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf693(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf694(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf695(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf696(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf697(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf698(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf699(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf700(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf701(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf702(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf703(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf704(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf705(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf706(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf707(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf708(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf709(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf710(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf711(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf712(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf713(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf714(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf715(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf716(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf717(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf718(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf719(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf720(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf721(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf722(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf723(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf724(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf725(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf726(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf727(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf728(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf729(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf730(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf731(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf732(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf733(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf734(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf735(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf736(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf737(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf738(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf739(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf740(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf741(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf742(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf743(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf744(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf745(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf746(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf747(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf748(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf749(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf750(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf751(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf752(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf753(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf754(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf755(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf756(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf757(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf758(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf759(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf760(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf761(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf762(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf763(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf764(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf765(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf766(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf767(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf768(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf769(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf770(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf771(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf772(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf773(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf774(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf775(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf776(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf777(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf778(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf779(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf780(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf781(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf782(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf783(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf784(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf785(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf786(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf787(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf788(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf789(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf790(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf791(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf792(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf793(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf794(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf795(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf796(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf797(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf798(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf799(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf800(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf801(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf802(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf803(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf804(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf805(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf806(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf807(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf808(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf809(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf810(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf811(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf812(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf813(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf814(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf815(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf816(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf817(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf818(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf819(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf820(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf821(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf822(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf823(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf824(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf825(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf826(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf827(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf828(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf829(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf830(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf831(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf832(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf833(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf834(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf835(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf836(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf837(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf838(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf839(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf840(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf841(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf842(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf843(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf844(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf845(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf846(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf847(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf848(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf849(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf850(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf851(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf852(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf853(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf854(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf855(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf856(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf857(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf858(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf859(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf860(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf861(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf862(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf863(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf864(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf865(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf866(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf867(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf868(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf869(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf870(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf871(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf872(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf873(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf874(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf875(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf876(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf877(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf878(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf879(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf880(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf881(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf882(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf883(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf884(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf885(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf886(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf887(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf888(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf889(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf890(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf891(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf892(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf893(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf894(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf895(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf896(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf897(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf898(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf899(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf900(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf901(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf902(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf903(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf904(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf905(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf906(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf907(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf908(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf909(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf910(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf911(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf912(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf913(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf914(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf915(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf916(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf917(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf918(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf919(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf920(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf921(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf922(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf923(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf924(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf925(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf926(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf927(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf928(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf929(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf930(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf931(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf932(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf933(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf934(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf935(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf936(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf937(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf938(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf939(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf940(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf941(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf942(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf943(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf944(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf945(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf946(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf947(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf948(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf949(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf950(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf951(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf952(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf953(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf954(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf955(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf956(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf957(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf958(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf959(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf960(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf961(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf962(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf963(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf964(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf965(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf966(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf967(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf968(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf969(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf970(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf971(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf972(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf973(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf974(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf975(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf976(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf977(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf978(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf979(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf980(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf981(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf982(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf983(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf984(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf985(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf986(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf987(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf988(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf989(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf990(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf991(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf992(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf993(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf994(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf995(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf996(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf997(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf998(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf999(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1000(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1001(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1002(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1003(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1004(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1005(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1006(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1007(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1008(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1009(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1010(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1011(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1012(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1013(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1014(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1015(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1016(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1017(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1018(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1019(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1020(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1021(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1022(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1023(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1024(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1025(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1026(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1027(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1028(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1029(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1030(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1031(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1032(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1033(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1034(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1035(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1036(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1037(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1038(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1039(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1040(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1041(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1042(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1043(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1044(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1045(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1046(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1047(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1048(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1049(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1050(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1051(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1052(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1053(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1054(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1055(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1056(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1057(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1058(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1059(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1060(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1061(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1062(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1063(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1064(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1065(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1066(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1067(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1068(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1069(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1070(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1071(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1072(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1073(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1074(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1075(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1076(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1077(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1078(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1079(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1080(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1081(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1082(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1083(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1084(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1085(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1086(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1087(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1088(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1089(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1090(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1091(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1092(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1093(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1094(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1095(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1096(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1097(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1098(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1099(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1100(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1101(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1102(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1103(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1104(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1105(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1106(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1107(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1108(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1109(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1110(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1111(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1112(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1113(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1114(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1115(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1116(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1117(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1118(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1119(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1120(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1121(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1122(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1123(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1124(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1125(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1126(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1127(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1128(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1129(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1130(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1131(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1132(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1133(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1134(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1135(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1136(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1137(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1138(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1139(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1140(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1141(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1142(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1143(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1144(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1145(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1146(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1147(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1148(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1149(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1150(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1151(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1152(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1153(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1154(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1155(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1156(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1157(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1158(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1159(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1160(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1161(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1162(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1163(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1164(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1165(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1166(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1167(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1168(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1169(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1170(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1171(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1172(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1173(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1174(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1175(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1176(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1177(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1178(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1179(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1180(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1181(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1182(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1183(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1184(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1185(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1186(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1187(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1188(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1189(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1190(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1191(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1192(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1193(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1194(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1195(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1196(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1197(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1198(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1199(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1200(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1201(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1202(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1203(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1204(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1205(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1206(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1207(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1208(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1209(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1210(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1211(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1212(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1213(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1214(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1215(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1216(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1217(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1218(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1219(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1220(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1221(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1222(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1223(
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1224(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1225(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1226(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1227(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1228(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1229(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1230(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1231(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1232(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1233(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1234(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1235(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1236(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1237(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1238(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1239(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1240(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1241(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1242(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1243(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1244(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1245(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1246(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1247(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1248(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1249(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1250(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1251(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1252(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1253(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1254(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1255(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1256(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1257(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1258(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1259(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1260(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1261(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1262(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1263(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1264(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1265(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1266(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1267(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1268(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1269(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1270(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1271(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1272(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1273(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1274(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1275(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1276(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1277(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1278(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1279(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1280(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1281(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1282(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1283(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1284(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1285(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1286(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1287(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1288(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1289(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1290(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1291(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1292(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1293(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1294(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1295(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1296(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1297(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1298(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1299(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1300(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1301(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1302(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1303(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1304(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1305(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1306(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1307(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1308(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1309(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1310(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1311(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1312(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1313(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1314(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1315(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1316(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1317(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1318(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1319(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1320(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1321(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1322(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1323(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1324(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1325(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1326(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1327(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1328(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1329(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1330(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1331(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1332(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1333(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1334(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1335(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1336(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1337(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1338(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1339(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1340(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1341(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1342(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1343(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1344(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1345(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1346(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1347(
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1348(
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1349(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1350(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1351(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1352(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1353(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1354(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1355(
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1356(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1357(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1358(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1359(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1360(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1361(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1362(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1363(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1364(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1365(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1366(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1367(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1368(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1369(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1370(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1371(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1372(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1373(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1374(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1375(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1376(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1377(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1378(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1379(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1380(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1381(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1382(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1383(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1384(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1385(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1386(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1387(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1388(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1389(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1390(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1391(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1392(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1393(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1394(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1395(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1396(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1397(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1398(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1399(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1400(
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1401(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1402(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1403(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1404(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1405(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1406(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1407(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1408(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1409(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1410(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1411(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1412(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1413(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1414(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1415(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1416(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1417(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1418(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1419(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1420(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1421(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1422(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1423(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1424(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1425(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1426(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1427(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1428(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1429(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1430(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1431(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1432(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1433(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1434(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1435(
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1436(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1437(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1438(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1439(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1440(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1441(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1442(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1443(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1444(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1445(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1446(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1447(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1448(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1449(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1450(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1451(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1452(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1453(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1454(
  const std::array<T,85>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1455(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1456(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1457(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1458(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1459(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1460(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1461(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1462(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1463(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1464(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1465(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1466(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1467(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1468(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1469(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1470(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1471(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1472(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1473(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1474(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1475(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1476(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1477(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1478(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1479(
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1480(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1481(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1482(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1483(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1484(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1485(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1486(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1487(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1488(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1489(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1490(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1491(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1492(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1493(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1494(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1495(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1496(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1497(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1498(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1499(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1500(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1501(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1502(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1503(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1504(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1505(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1506(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1507(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1508(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&);

template <class T>
std::complex<T> qqb_2lha_tf1509(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1510(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1511(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1512(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&);

template <class T>
std::complex<T> qqb_2lha_tf1513(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1514(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1515(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1516(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1517(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1518(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1519(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1520(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1521(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1522(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1523(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1524(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1525(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&);

template <class T>
std::complex<T> qqb_2lha_tf1526(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1527(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1528(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1529(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_2lha_tf1530(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&);

#endif /* QQB_2LHA_TF_DECL_H */
