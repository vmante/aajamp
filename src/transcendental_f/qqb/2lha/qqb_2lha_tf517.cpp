#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf517(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[10];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[13];
    z[3]=c[20];
    z[4]=c[23];
    z[5]=f[44];
    z[6]=f[77];
    z[7]=f[79];
    z[8]=f[81];
    z[9]= - n<T>(53,54)*z[1] + 72*z[2];
    z[9]=n<T>(1,5)*z[9] - n<T>(3,10)*z[4] + 6*z[3];
    z[9]=i*z[9];

    r +=  - 3*z[5] + z[6] + z[7] + z[8] + z[9];
 
    return r;
}

template std::complex<double> qqb_2lha_tf517(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf517(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
