#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf167(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e,
  const std::array<std::complex<T>,111>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[30];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[2];
    z[4]=d[3];
    z[5]=d[7];
    z[6]=e[0];
    z[7]=e[1];
    z[8]=e[8];
    z[9]=c[3];
    z[10]=c[19];
    z[11]=c[23];
    z[12]=c[25];
    z[13]=c[26];
    z[14]=c[28];
    z[15]=c[31];
    z[16]=f[3];
    z[17]=f[5];
    z[18]=f[11];
    z[19]=f[13];
    z[20]=i*z[2];
    z[21]=z[4]*i;
    z[20]=z[20] - z[21];
    z[22]=z[5]*z[20];
    z[23]=npow(z[2],2);
    z[24]= - i*z[23];
    z[21]=z[2]*z[21];
    z[21]=n<T>(2,3)*z[22] + z[24] + z[21];
    z[22]=2*z[1];
    z[21]=z[21]*z[22];
    z[24]= - z[2] + n<T>(1,2)*z[4];
    z[24]=z[24]*z[4];
    z[24]=z[24] + n<T>(1,2)*z[23];
    z[20]=z[20]*z[22];
    z[25]=z[4] - z[2];
    z[26]=z[3]*z[25];
    z[20]=z[26] + z[20] - z[24];
    z[26]=n<T>(1,3)*z[3];
    z[20]=z[20]*z[26];
    z[27]=n<T>(1,3)*z[5];
    z[28]= - z[2] - z[27];
    z[26]=n<T>(1,2)*z[28] - z[26];
    z[26]=z[9]*z[26];
    z[22]=z[22]*i;
    z[22]=z[22] - z[2];
    z[28]=z[3] - z[22];
    z[28]=z[6]*z[28];
    z[26]= - z[28] - z[26] + z[17] - z[16];
    z[28]= - n<T>(2,3)*z[8] - z[7];
    z[22]= - z[5] + z[22];
    z[22]=z[22]*z[28];
    z[25]=z[5]*z[25];
    z[24]=2*z[25] + z[24];
    z[24]=z[24]*z[27];
    z[25]=z[15] + z[14];
    z[27]=npow(z[2],3);
    z[23]= - z[4]*z[23];
    z[28]=2*i;
    z[28]=z[11]*z[28];
    z[29]=z[13]*i;

    r += n<T>(1,6)*z[10] - z[12] + n<T>(2,3)*z[18] + n<T>(4,3)*z[19] + z[20] + 
      z[21] + z[22] + z[23] + z[24] - 2*z[25] - n<T>(1,3)*z[26] + z[27] + 
      z[28] + 4*z[29];
 
    return r;
}

template std::complex<double> qqb_2lha_tf167(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e,
  const std::array<std::complex<double>,111>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf167(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e,
  const std::array<std::complex<dd_real>,111>& f
);
#endif
