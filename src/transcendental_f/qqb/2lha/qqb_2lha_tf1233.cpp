#include "qqb_2lha_tf_decl.hpp"

template<class T>
std::complex<T> qqb_2lha_tf1233(
  const std::array<std::complex<T>,25>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[7];
  std::complex<T> r(0,0);

    z[1]=d[4];
    z[2]=d[5];
    z[3]=e[6];
    z[4]=e[7];
    z[5]=npow(z[1],2);
    z[6]= - z[1] + n<T>(1,2)*z[2];
    z[6]=z[2]*z[6];

    r += z[3] + z[4] + n<T>(1,2)*z[5] + z[6];
 
    return r;
}

template std::complex<double> qqb_2lha_tf1233(
  const std::array<std::complex<double>,25>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_2lha_tf1233(
  const std::array<std::complex<dd_real>,25>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
