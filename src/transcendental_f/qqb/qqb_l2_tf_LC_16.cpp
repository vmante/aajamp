#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_16(
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[3];
  std::complex<T> r(0,0);

    z[1]=f[2];

    r += n<T>(1,54)*z[1];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_16(
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_16(
  const std::array<std::complex<dd_real>,82>& f
);
#endif
