#include "qqb_l2_tf_nfnfaa_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfnfaa_1(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[6];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[0];
    z[3]=d[1];
    z[4]=d[5];
    z[5]=i*z[1];
    z[5]= - z[5] - n<T>(5,3) + z[2];
    z[5]=z[4] + z[3] + 4*z[5];

    r += n<T>(8,3)*z[5];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfnfaa_1(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfnfaa_1(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d
);
#endif
