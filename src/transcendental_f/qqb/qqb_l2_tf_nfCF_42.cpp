#include "qqb_l2_tf_nfCF_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_nfCF_42(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,19>& d,
  const std::array<std::complex<T>,24>& e
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[13];
  std::complex<T> r(0,0);

    z[1]=c[0];
    z[2]=d[1];
    z[3]=d[5];
    z[4]=d[6];
    z[5]=d[18];
    z[6]=d[3];
    z[7]=d[7];
    z[8]=e[11];
    z[9]=z[4]*z[6];
    z[9]=z[9] - z[8];
    z[10]=npow(z[7],2);
    z[9]= - z[10] + 2*z[9];
    z[10]=z[1]*i;
    z[11]=z[5] - z[4];
    z[11]=4*z[11] - z[2];
    z[11]=z[11]*z[10];
    z[12]=z[2]*z[6];
    z[9]=z[11] + 2*z[9] + z[12];
    z[11]=z[4] - z[7];
    z[10]= - 5*z[10] - z[2] + z[6] - 4*z[11];
    z[10]=n<T>(1,3)*z[10] - z[3];
    z[10]=z[3]*z[10];
    z[9]=n<T>(1,3)*z[9] + z[10];

    r += n<T>(2,3)*z[9];
 
    return r;
}

template std::complex<double> qqb_l2_tf_nfCF_42(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,19>& d,
  const std::array<std::complex<double>,24>& e
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_nfCF_42(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,19>& d,
  const std::array<std::complex<dd_real>,24>& e
);
#endif
