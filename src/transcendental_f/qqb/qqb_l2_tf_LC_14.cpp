#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_14(
  const std::array<T, 85>& c,
  const std::array<std::complex<T>,82>& f
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[5];
  std::complex<T> r(0,0);

    z[1]=c[11];
    z[2]=c[12];
    z[3]=f[69];
    z[4]=i*z[1];
    z[4]= - n<T>(1,27)*z[4] + z[2] + n<T>(1,2)*z[3];

    r += n<T>(1,3)*z[4];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_14(
  const std::array<double,85>& c,
  const std::array<std::complex<double>,82>& f
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_14(
  const std::array<dd_real,85>& c,
  const std::array<std::complex<dd_real>,82>& f
);
#endif
