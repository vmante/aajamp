#include "qqb_l2_tf_LC_decl.hpp"

template<class T>
std::complex<T> qqb_l2_tf_LC_38(
  const std::array<std::complex<T>,19>& d
) {
  const std::complex<T> i(0,1);
  std::complex<T> z[5];
  std::complex<T> r(0,0);

    z[1]=d[1];
    z[2]=d[5];
    z[3]=npow(z[1],2);
    z[4]=z[1] - n<T>(1,2)*z[2];
    z[4]=z[2]*z[4];
    z[3]= - n<T>(1,2)*z[3] + z[4];

    r += n<T>(1,9)*z[3];
 
    return r;
}

template std::complex<double> qqb_l2_tf_LC_38(
  const std::array<std::complex<double>,19>& d
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_l2_tf_LC_38(
  const std::array<std::complex<dd_real>,19>& d
);
#endif
