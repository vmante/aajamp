#ifndef TRANFUNS_EVALUATOR_H
#define TRANFUNS_EVALUATOR_H

#include <array>
#include <complex>

template <class T>
void evaluate_2lLC_tf_qqgaa(
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,82>&,
  const std::array<std::complex<T>,330>&,
  std::array<std::complex<T>,409>&);

#ifdef FULLCOL_ENABLED
template <class T>
void evaluate_2lNLC_tf_qqgaa(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&,
  std::array<std::complex<T>,1361>&);

template <class T>
void evaluate_2lNLC_tf_qgqaa(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&,
  std::array<std::complex<T>,1482>&);
#endif

#ifdef LOOPSQ_ENABLED
template <class T>
void evaluate_2lHA_tf_qqgaa(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&,
  std::array<std::complex<T>,1531>&);

template <class T>
void evaluate_2lHA_tf_qgqaa(
  const std::array<T,85>&,
  const std::array<std::complex<T>,25>&,
  const std::array<std::complex<T>,24>&,
  const std::array<std::complex<T>,111>&,
  const std::array<std::complex<T>,472>&,
  std::array<std::complex<T>,1466>&);
#endif

#endif /* TRANFUNS_EVALUATOR_H */
