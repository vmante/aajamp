# File: CMakeLists.txt
# Authors: F. Buccioni
# Description:
# Last Modified: November 9, 2024

set(AAJAMP_FILES "")

# default compiled files
set(AAJAMP_FILES ${AAJAMP_FILES} ${AAJAMP_SOURCE_PATH}/aajamp.cpp)
set(AAJAMP_FILES ${AAJAMP_FILES} ${AAJAMP_SOURCE_PATH}/helamplitudes.cpp)
set(AAJAMP_FILES ${AAJAMP_FILES} ${AAJAMP_SOURCE_PATH}/interferences.cpp)
set(AAJAMP_FILES ${AAJAMP_FILES} ${AAJAMP_SOURCE_PATH}/wrapper.cpp)
set(AAJAMP_FILES ${AAJAMP_FILES} ${AAJAMP_SOURCE_PATH}/rational_f/ratfuns_evaluator.cpp)
set(AAJAMP_FILES ${AAJAMP_FILES} ${AAJAMP_SOURCE_PATH}/transcendental_f/tranfuns_evaluator.cpp)
# rational functions-qqb
file(GLOB_RECURSE RATFUNSQQB0L ${AAJAMP_SOURCE_PATH}/rational_f/qqb/0l/qqb_0l_r*.cpp)
file(GLOB_RECURSE RATFUNSQQB1L ${AAJAMP_SOURCE_PATH}/rational_f/qqb/1l/qqb_1l_r*cpp)
file(GLOB_RECURSE RATFUNSQQB1LHA ${AAJAMP_SOURCE_PATH}/rational_f/qqb/1lha/qqb_1lha_r*cpp)
file(GLOB_RECURSE RATFUNSQQB2LLC ${AAJAMP_SOURCE_PATH}/rational_f/qqb/2lLC/qqb_2lLC_r*cpp)
set(AAJAMP_FILES ${AAJAMP_FILES} ${RATFUNSQQB0L} ${RATFUNSQQB1L} ${RATFUNSQQB1LHA} ${RATFUNSQQB2LLC})
# rational functions-qg
file(GLOB_RECURSE RATFUNSQG0L ${AAJAMP_SOURCE_PATH}/rational_f/qg/0l/qg_0l_r*.cpp)
file(GLOB_RECURSE RATFUNSQG1L ${AAJAMP_SOURCE_PATH}/rational_f/qg/1l/qg_1l_r*cpp)
file(GLOB_RECURSE RATFUNSQG1LHA ${AAJAMP_SOURCE_PATH}/rational_f/qg/1lha/qg_1lha_r*cpp)
file(GLOB_RECURSE RATFUNSQG2LLC ${AAJAMP_SOURCE_PATH}/rational_f/qg/2lLC/qg_2lLC_r*cpp)
set(AAJAMP_FILES ${AAJAMP_FILES} ${RATFUNSQG0L} ${RATFUNSQG1L} ${RATFUNSQG1LHA} ${RATFUNSQG2LLC})
# One loop transcendental functions
file(GLOB_RECURSE TRANSFUNSONEQQB ${AAJAMP_SOURCE_PATH}/transcendental_f/qqb/qqb_l1_tf_*.cpp)
file(GLOB_RECURSE TRANSFUNSONEQG ${AAJAMP_SOURCE_PATH}/transcendental_f/qg/qg_l1_tf_*.cpp)
set(AAJAMP_FILES ${AAJAMP_FILES} ${TRANSFUNSONEQQB} ${TRANSFUNSONEQG})
# Two loop transcendental functions: LC
file(GLOB_RECURSE TRFNTQQB2LLC ${AAJAMP_SOURCE_PATH}/transcendental_f/qqb/2lLC/qqb_2lLC_*.cpp)
file(GLOB_RECURSE TRANSFUNSTWOQG ${AAJAMP_SOURCE_PATH}/transcendental_f/qg/qg_l2_tf_*.cpp)
set(AAJAMP_FILES ${AAJAMP_FILES} ${TRFNTQQB2LLC} ${TRANSFUNSTWOQG})
# Two-loop NLC contributions: rational + transcendental functions
if (FULL_COLOUR)
  file(GLOB_RECURSE RATFUNSQQB2LNLC ${AAJAMP_SOURCE_PATH}/rational_f/qqb/2lNLC/qqb_2lNLC_r*cpp)
  file(GLOB_RECURSE RATFUNSQG2LNLC ${AAJAMP_SOURCE_PATH}/rational_f/qg/2lNLC/qg_2lNLC_r*cpp)
  set(AAJAMP_FILES ${AAJAMP_FILES} ${RATFUNSQQB2LNLC} ${RATFUNSQG2LNLC})
  file(GLOB_RECURSE TRFNTQQB2LNLC ${AAJAMP_SOURCE_PATH}/transcendental_f/qqb/2lNLC/qqb_2lNLC_*.cpp)
  file(GLOB_RECURSE TRFNTQG2LNLC ${AAJAMP_SOURCE_PATH}/transcendental_f/qg/2lNLC/qg_2lNLC_*.cpp)
  set(AAJAMP_FILES ${AAJAMP_FILES} ${TRFNTQQB2LNLC} ${TRFNTQG2LNLC})
endif (FULL_COLOUR)
# Two-loop helicity amplitudes with closed fermionic loops
if (FERMIONIC_LOOPS)
  file(GLOB_RECURSE RATFUNSQQB2LHA ${AAJAMP_SOURCE_PATH}/rational_f/qqb/2lha/qqb_2lha_r*cpp)
  file(GLOB_RECURSE RATFUNSQG2LHA ${AAJAMP_SOURCE_PATH}/rational_f/qg/2lha/qg_2lha_r*cpp)
  file(GLOB_RECURSE TRFNTQQB2LHA ${AAJAMP_SOURCE_PATH}/transcendental_f/qqb/2lha/qqb_2lha_*.cpp)
  file(GLOB_RECURSE TRFNTQG2LHA ${AAJAMP_SOURCE_PATH}/transcendental_f/qg/2lha/qg_2lha_*.cpp)
  set(AAJAMP_FILES ${AAJAMP_FILES} ${RATFUNSQQB2LHA} ${RATFUNSQG2LHA} ${TRFNTQQB2LHA} ${TRFNTQG2LHA})
endif (FERMIONIC_LOOPS)

string(TOLOWER "${CMAKE_BUILD_TYPE}" CBT)
enable_language(Fortran)

set(SOURCE ${AAJAMP_FILES})
add_library (aajamp SHARED ${SOURCE})

#------------------------------------------------
#  PentagonFunctions-cpp
#------------------------------------------------
if (PENTAGONFUNCTIONS_FOUND)
  target_link_libraries (aajamp ${PENTAGONFUNCTIONS_LIBRARIES})
  include_directories (${PENTAGONFUNCTIONS_INCLUDE_DIR})
endif()

#------------------------------------------------
#  QD
#------------------------------------------------
if (USE_QD)
  if(QD_FOUND)
    target_link_libraries (aajamp ${QD_LIBRARIES})
    include_directories (${QD_INCLUDE_DIR})
  endif()
endif(USE_QD)

################################
#  installation configuration  #
################################

set(AAJAMP_HEADERS "")
set(AAJAMP_HEADERS ${AAJAMP_HEADERS} "${AAJAMP_INCLUDE_PATH}/aajamp.hpp")
set(AAJAMP_HEADERS ${AAJAMP_HEADERS} "${AAJAMP_INCLUDE_PATH}/helamplitudes.hpp")
set(AAJAMP_HEADERS ${AAJAMP_HEADERS} "${AAJAMP_INCLUDE_PATH}/interferences.hpp")
set(AAJAMP_HEADERS ${AAJAMP_HEADERS} "${AAJAMP_INCLUDE_PATH}/wrapper.hpp")
set(AAJAMP_HEADERS ${AAJAMP_HEADERS} "${AAJAMP_INCLUDE_PATH}/version.hpp")
set(AAJAMP_HEADERS ${AAJAMP_HEADERS} "${AAJAMP_INCLUDE_PATH}/aajamp.f90")
set_target_properties(aajamp PROPERTIES PUBLIC_HEADER "${AAJAMP_HEADERS}")

message("")
if (CBT MATCHES debug)
  message("Build type: Debug")
elseif(CBT MATCHES release)
  message("Build type: Release")
elseif(CBT MATCHES profiling)
  message("Build type: Profiling")
else()
  message("Build type: Default")
endif ()

get_filename_component (CXX_COMPILER_NAME ${CMAKE_CXX_COMPILER} NAME)
get_filename_component (Fortran_COMPILER_NAME ${CMAKE_Fortran_COMPILER} NAME)
message ("aajamp:  CXX compiler      = ${CXX_COMPILER_NAME}")
message ("         CXX compiler ID   = ${CMAKE_CXX_COMPILER_ID}")
message ("         Fortran compiler  = ${Fortran_COMPILER_NAME}")
#message ("         Fortran compiler ID = ${CMAKE_Fortran_COMPILER_ID}")
if (CBT MATCHES "debug")
  message ("         CXX flags         = ${CMAKE_CXX_FLAGS_DEBUG}")
  #message ("         Fortran flags     = ${CMAKE_Fortran_FLAGS_DEBUG}")
elseif (CBT MATCHES "release")
  message ("         CXX flags         = ${CMAKE_CXX_FLAGS_RELEASE}")
  #message ("         Fortran flags      = ${CMAKE_Fortran_FLAGS_RELEASE}")
  message ("         Library install prefix = ${LIB_INSTALL_DIR}")
  message ("         Include install prefix = ${INCLUDE_INSTALL_DIR}\n")
elseif (CBT MATCHES "profiling")
  message ("         CXX flags         = ${CMAKE_CXX_FLAGS_RELEASE}")
  #message ("         Fortran flags      = ${CMAKE_Fortran_FLAGS_RELEASE}")
else ()
  message ("         CXX flags         = ${CMAKE_CXX_FLAGS}")
  #message ("         Fortran flags      = ${CMAKE_Fortran_FLAGS}")
endif ()

include(PackageConfigInstall)
PACKAGE_CONFIG_INSTALL(aajamp ${AAJAMP_VERSION})
