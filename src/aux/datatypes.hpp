#ifndef DATATYPES
#define DATATYPES

#include <complex>

/* Overload of operators for complex numbers */
template<typename T>
inline std::complex<T> operator+(int a, const std::complex<T>& b) {
  return static_cast<T>(a)+b;
}

template<typename T>
inline std::complex<T> operator+(long int a, const std::complex<T>& b) {
  return static_cast<T>(static_cast<double>(a))+b;
}

template<typename T>
inline std::complex<T> operator-(int a, const std::complex<T>& b) {
  return static_cast<T>(a)-b;
}

template<typename T>
inline std::complex<T> operator-(long int a, const std::complex<T>& b) {
  return static_cast<T>(static_cast<double>(a))-b;
}

template<typename T>
inline std::complex<T> operator+(const std::complex<T>& a, int b) {
  return a+static_cast<T>(b);
}

template<typename T>
inline std::complex<T> operator+(const std::complex<T>& a, long int b) {
  return a+static_cast<T>(static_cast<double>(b));
}

template<typename T>
inline std::complex<T> operator-(const std::complex<T>& a, int b) {
  return a-static_cast<T>(b);
}

template<typename T>
inline std::complex<T> operator*(int a, const std::complex<T>& b) {
  return static_cast<T>(a)*b;
}

template<typename T>
inline std::complex<T> operator*(long int a, const std::complex<T>& b) {
  return static_cast<T>(static_cast<double>(a))*b;
}

template<typename T>
inline std::complex<T> operator/(const std::complex<T>& a, int b) {
  return a/static_cast<T>(b);
}

template<typename T>
inline std::complex<T> operator/(const std::complex<T>& a, long int b) {
  return a/static_cast<T>(static_cast<double>(b));
}

/* Ratonal numbers */
template<typename T>
inline T n(int a, int b) {
 return static_cast<T>(a)/static_cast<T>(b);
}

template<typename T>
inline T n(long int a, int b) {
 return static_cast<T>(static_cast<double>(a))/static_cast<T>(b);
}

template<typename T>
inline T n(long long int a, int b) {
 return static_cast<T>(static_cast<double>(a))/static_cast<T>(b);
}

template<typename T>
inline T n(int a, long int b) {
 return static_cast<T>(a)/static_cast<T>(static_cast<double>(b));
}

template<typename T>
inline T n(long int a, long int b) {
 return static_cast<T>(static_cast<double>(a))/static_cast<T>(static_cast<double>(b));
}

template<typename T>
inline T n(long long int a, long int b) {
 return static_cast<T>(static_cast<double>(a))/static_cast<T>(static_cast<double>(b));
}

template<typename T>
inline T n(int a, long long int b) {
 return static_cast<T>(a)/static_cast<T>(static_cast<double>(b));
}

template<typename T>
inline T n(long int a, long long int b) {
 return static_cast<T>(static_cast<double>(a))/static_cast<T>(static_cast<double>(b));
}

template<typename T>
inline T n(long long int a, long long int b) {
 return static_cast<T>(static_cast<double>(a))/static_cast<T>(static_cast<double>(b));
}

#endif /* DATATYPES */
