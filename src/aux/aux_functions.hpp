#ifndef AUX_FUNCTIONS_H
#define AUX_FUNCTIONS_H

template <typename T>
inline T npow(T x, unsigned int n)
{
    T res(1);
    for (unsigned int i = 0; i < n; i++) { res *= x; }
    return res;
}

#endif
