#ifndef UTIL_H
#define UTIL_H

namespace aajamp{

  inline void OUT2STDERR(const std::string& msg) {
    fprintf(stderr, "%s", "aajamp::ERROR. ");
    fprintf(stderr, "%s", msg.c_str());
    fflush(stderr);
  }

  inline void OUT2STDOUT(const std::string& msg) {
    fprintf(stdout, "%s", msg.c_str());
    fflush(stdout);
  }

}

#endif
