#include <cmath>
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif
#include "aux/util.hpp"
#include "aux/datatypes.hpp"
#include "interferences.hpp"
#include "helamplitudes.hpp"
#include "PentagonFunctions/Constants.h"
#include "rational_f/ratfuns_evaluator.hpp"
#include "transcendental_f/tranfuns_evaluator.hpp"
#include "transcendental_f/qqb/qqb_l1_tf_CA_decl.hpp"
#include "transcendental_f/qqb/qqb_l1_tf_CF_decl.hpp"
#include "transcendental_f/qqb/qqb_l1_tf_nfaa_decl.hpp"
#include "transcendental_f/qg/qg_l1_tf_CA_decl.hpp"
#include "transcendental_f/qg/qg_l1_tf_CF_decl.hpp"
#include "transcendental_f/qg/qg_l1_tf_nfaa_decl.hpp"
#include "transcendental_f/qg/qg_l2_tf_nfnfaa_decl.hpp"
#include "transcendental_f/qg/qg_l2_tf_nfCA_decl.hpp"
#include "transcendental_f/qg/qg_l2_tf_nfCF_decl.hpp"
#include "transcendental_f/qg/qg_l2_tf_LC_decl.hpp"

// ----------------------------------------------------------------------------
// Process registration
// ----------------------------------------------------------------------------
template <class T>
void Interference<T>::register_interf_process(int proc_id){
    process_id = proc_id;
    if(proc_id == 1){
        qqb_active = true;
    } else if (proc_id == 2){
        qg_active = true;
    }
    else{
      aajamp::OUT2STDERR("Unknown process_id.\n Proc ids:\n - 1: q qb -> g a a\n - 2: q g -> q a a\n");
    };
}

template <class T>
void Interference<T>::register_interf_loop_order(int ord){
    loop_ord = ord;
}

// ----------------------------------------------------------------------------
// Event registration
// ----------------------------------------------------------------------------
template <class T>
void Interference<T>::register_interf_event(const std::array<T,5>& sij_in, const T musq_in){

  if(!qqb_active and !qg_active){
    aajamp::OUT2STDERR("No process has been registered.\n");
  }

  std::complex<T> CI(0,1);

  musq = musq_in;

  sij[0] = sij_in[0]; // s_{12}
  sij[1] = sij_in[1]; // s_{23}
  sij[2] = sij_in[2]; // s_{34}
  sij[3] = sij_in[3]; // s_{45}
  sij[4] = sij_in[4]; // s_{51}

  // dimensionless variables for the Pentagon functions
  v[0] = sij_in[0]/musq;
  v[1] = sij_in[1]/musq;
  v[2] = sij_in[2]/musq;
  v[3] = sij_in[3]/musq;
  v[4] = sij_in[4]/musq;

  // evaluate eps5
  gram  = (sij[0]*sij[1] + sij[1]*sij[2] - sij[2]*sij[3] +
              sij[3]*sij[4] - sij[4]*sij[0]);
  gram *= gram;
  gram += -4*sij[0]*sij[1]*sij[2]*(sij[1] - sij[3] - sij[4]);
  im_eps5 = sqrt(-gram);

  /* The sign of the imaginary part of eps5 is forced to be positive
   * in order to match the convention of 2009.07803. See eq. (4.15)
   * and sec. 5.8 of the same reference for further details
   */
  eps5  = CI*im_eps5;
  eps5t = eps5/(sij[0]*sij[0]);

  /* tr5 normalised by s12^2
   * Not careful about sign of tr5. It's irrelevant anyways for a squared ME
   * summed over all helicities
   */
  tr5 = CI*im_eps5/(sij[0]*sij[0]);

  // Setting values of k (invariants + denominators)
  this->set_k();

  // Computing rational functions
  this->set_rational_functions();

  /* Evaluate the pentagon functions at the given kinematic point
     and store them in arrays */
  if(loop_ord > 0){
    if(!pentagon_functions_w1_initialised){this->initialise_PentagonFunctions_w1();}
    this->set_fw1();

    if(!pentagon_functions_w2_initialised){this->initialise_PentagonFunctions_w2();}
    this->set_fw2();}

  if(loop_ord == 2 || loop_ord == 12){

    if(!pentagon_functions_w3_initialised){this->initialise_PentagonFunctions_w3();}
    this->set_fw3();

    if(!pentagon_functions_w4_initialised){this->initialise_PentagonFunctions_w4();}
    this->set_fw4();
  }

  // Computing transcendental functions
  this->set_transcendental_functions();
    
  if(loop_ord == 11){
    // Computing squared of spinor phases
    this->set_spinor_phases();

    // Computing rational functions
    this->set_1lHAMP_rational_functions();

    // evaluate 1loop helicity amplitudes
    this->evaluate_1loop_helamplitudes();
  }

#ifdef LOOPSQ_ENABLED
  if(loop_ord == 12){
    // Computing squared of spinor phases
    this->set_spinor_phases();

    // Computing rational functions
    this->set_1lHAMP_rational_functions();
    this->set_2lHAMP_rational_functions();

    // Computing transcendental functions
    this->set_2lHAMP_transcendental_functions();

    // evaluate 1- and 2-loop helicity amplitudes
    this->evaluate_1loop_helamplitudes();
    this->evaluate_2loop_helamplitudes();
  }
#endif
}

// ----------------------------------------------------------------------------
// Variables of the rational functions
// ----------------------------------------------------------------------------
template <class T>
void Interference<T>::set_k(){
   k[0]=static_cast<T>(1);
   k[1]=sij[0];
   k[2]=sij[1];
   k[3]=sij[2];
   k[4]=sij[3];
   k[5]=sij[4];
   k[6]=1/sij[0];
   k[7]=1/sij[1];
   k[8]=1/sij[2];
   k[9]=1/sij[3];
   k[10]=1/sij[4];
   k[11]=1/(sij[0]+sij[1]-sij[3]);
   k[12]=1/(sij[1]-sij[3]-sij[4]);
   k[13]=1/(sij[1]+sij[2]-sij[4]);
   k[14]=1/(sij[0]-sij[2]+sij[4]);
   k[15]=1/(sij[0]-sij[2]-sij[3]);
   k[16]=1/(sij[0]+sij[1]);
   k[17]=1/(sij[0]-sij[2]);
   k[18]=1/(sij[1]+sij[2]);
   k[19]=1/(sij[0]-sij[3]);
   k[20]=1/(sij[1]-sij[3]);
   k[21]=1/(sij[0]+sij[1]-sij[2]-sij[3]);
   k[22]=1/(sij[2]+sij[3]);
   k[23]=1/(sij[1]-sij[4]);
   k[24]=1/(sij[2]-sij[4]);
   k[25]=1/(sij[0]+sij[1]-sij[3]-sij[4]);
   k[26]=1/(sij[1]+sij[2]-sij[3]-sij[4]);
   k[27]=1/(sij[0]+sij[4]);
   k[28]=1/(sij[0]-sij[1]-sij[2]+sij[4]);
   k[29]=1/(sij[0]-sij[2]-sij[3]+sij[4]);
   k[30]=1/(sij[3]+sij[4]);

   // dimensionless sij and denominator factors
   for(int i = 1; i < 5; i++){
      kk[i] = k[i+1]/k[1];
   }
   for(int i = 5; i < 29; i++){
      kk[i] = k[i+2]*k[1];
   }
   kk[29] = (k[1]*k[1]*k[1]*k[1])/gram;
}

// ----------------------------------------------------------------------------
// Transcendental constants
// ----------------------------------------------------------------------------
template <class T>
void Interference<T>::set_tcs(){

    tcs[0]  = PentagonFunctions::bc<T>[0];
    tcs[1]  = PentagonFunctions::bc<T>[2];
    tcs[2]  = PentagonFunctions::bc<T>[1];
    tcs[3]  = tcs[0]*tcs[0];
    tcs[4]  = PentagonFunctions::bc<T>[3];
    tcs[5]  = tcs[0]*tcs[1];
    tcs[6]  = tcs[1]*tcs[1],
    tcs[7]  = tcs[0]*tcs[2];
    tcs[8]  = tcs[1]*tcs[2];
    tcs[9]  = tcs[2]*tcs[2];
    tcs[10] = PentagonFunctions::bc<T>[4];
    tcs[11] = tcs[0]*tcs[0]*tcs[0];
    tcs[12] = tcs[0]*tcs[4];
    tcs[13] = PentagonFunctions::bc<T>[5];
    tcs[14] = PentagonFunctions::bc<T>[6];
    tcs[15] = tcs[3]*tcs[1];
    tcs[16] = tcs[1]*tcs[4];
    tcs[17] = tcs[0]*tcs[6];
    tcs[18] = tcs[1]*tcs[1]*tcs[1];
    tcs[19] = tcs[3]*tcs[2];
    tcs[20] = tcs[2]*tcs[4];
    tcs[21] = tcs[0]*tcs[1]*tcs[2];
    tcs[22] = tcs[6]*tcs[2];
    tcs[23] = tcs[0]*tcs[9];
    tcs[24] = tcs[1]*tcs[9];
    tcs[25] = tcs[2]*tcs[2]*tcs[2];
    tcs[26] = tcs[0]*tcs[10];
    tcs[27] = tcs[1]*tcs[10];
    tcs[28] = tcs[2]*tcs[10];
    tcs[29] = PentagonFunctions::bc<T>[8];
    tcs[30] = PentagonFunctions::bc<T>[7];
    tcs[31] = PentagonFunctions::bc<T>[9];
    tcs[32] = tcs[0]*tcs[0]*tcs[0]*tcs[0];
    tcs[33] = tcs[3]*tcs[4];
    tcs[34] = tcs[4]*tcs[4];
    tcs[35] = tcs[0]*tcs[13];
    tcs[36] = tcs[0]*tcs[14];
    tcs[37] = PentagonFunctions::bc<T>[11];
    tcs[38] = PentagonFunctions::bc<T>[10];
    tcs[39] = tcs[11]*tcs[1];
    tcs[40] = tcs[0]*tcs[1]*tcs[4];
    tcs[41] = tcs[1]*tcs[13];
    tcs[42] = tcs[1]*tcs[14];
    tcs[43] = tcs[3]*tcs[6];
    tcs[44] = tcs[6]*tcs[4];
    tcs[45] = tcs[0]*tcs[18];
    tcs[46] = tcs[1]*tcs[1]*tcs[1]*tcs[1];
    tcs[47] = tcs[11]*tcs[2];
    tcs[48] = tcs[0]*tcs[2]*tcs[4];
    tcs[49] = tcs[2]*tcs[13];
    tcs[50] = tcs[2]*tcs[14];
    tcs[51] = tcs[3]*tcs[1]*tcs[2];
    tcs[52] = tcs[1]*tcs[2]*tcs[4];
    tcs[53] = tcs[0]*tcs[6]*tcs[2];
    tcs[54] = tcs[18]*tcs[2];
    tcs[55] = tcs[3]*tcs[9];
    tcs[56] = tcs[9]*tcs[4];
    tcs[57] = tcs[0]*tcs[1]*tcs[9],
    tcs[58] = tcs[6]*tcs[9];
    tcs[59] = tcs[0]*tcs[25];
    tcs[60] = tcs[1]*tcs[25];
    tcs[61] = tcs[2]*tcs[2]*tcs[2]*tcs[2],
    tcs[62] = tcs[3]*tcs[10];
    tcs[63] = tcs[4]*tcs[10];
    tcs[64] = tcs[0]*tcs[1]*tcs[10];
    tcs[65] = tcs[6]*tcs[10];
    tcs[66] = tcs[0]*tcs[2]*tcs[10];
    tcs[67] = tcs[1]*tcs[2]*tcs[10];
    tcs[68] = tcs[9]*tcs[10];
    tcs[69] = tcs[10]*tcs[10];
    tcs[70] = tcs[0]*tcs[29];
    tcs[71] = tcs[1]*tcs[29];
    tcs[72] = tcs[2]*tcs[29];
    tcs[73] = tcs[0]*tcs[30];
    tcs[74] = tcs[1]*tcs[30];
    tcs[75] = tcs[2]*tcs[30];
    tcs[76] = PentagonFunctions::bc<T>[15];
    tcs[77] = PentagonFunctions::bc<T>[13];
    tcs[78] = PentagonFunctions::bc<T>[14];
    tcs[79] = PentagonFunctions::bc<T>[16];
    tcs[80] = PentagonFunctions::bc<T>[17];
    tcs[81] = tcs[0]*tcs[31];
    tcs[82] = tcs[1]*tcs[31];
    tcs[83] = tcs[2]*tcs[31];
    tcs[84] = PentagonFunctions::bc<T>[12];
}

// --------------------------------------------------------
// Pentagon functions
// --------------------------------------------------------

// initialise pentagon functions of weight 1
template <class T>
void Interference<T>::initialise_PentagonFunctions_w1(){

    std::vector<PentagonFunctions::FunctionID> needed_functions = {
      {1,1,1}, {1,1,2}, {1,1,3},
      {1,1,4}, {1,1,5}, {1,1,6},
      {1,1,7}, {1,1,8}, {1,1,9}, {1,1,10},
      {1,2,1}, {1,2,2}, {1,2,3},
      {1,2,4}, {1,2,5}, {1,2,6},
      {1,2,7}, {1,2,8}, {1,2,9}
#if defined (FULLCOL_ENABLED) || defined (LOOPSQ_ENABLED)
      ,
      {1,2,10}, {1,3,1}, {1,3,2},
      {1,3,3}, {1,3,4}, {1,3,5}
#endif
    };

    for (auto pf : needed_functions) {
        PF_evaluators_w1.push_back(pf.get_evaluator<T>());
    }

    pentagon_functions_w1_initialised = true;
}

// initialise pentagon functions of weight 2
template <class T>
void Interference<T>::initialise_PentagonFunctions_w2(){

    std::vector<PentagonFunctions::FunctionID> needed_functions = {
      {2,1,1}, {2,1,2}, {2,1,3},
      {2,1,4}, {2,1,5}, {2,1,6},
      {2,1,7}, {2,1,8}, {2,1,9},
      {2,1,10}, {2,1,11}, {2,1,12},
      {2,1,13}, {2,1,14}, {2,1,15},
      {2,2,1}, {2,2,2}, {2,2,3},
      {2,2,4}, {2,2,5}, {2,2,6},
      {2,2,7}, {2,2,8}, {2,2,9}
    };

    for (auto pf : needed_functions) {
        PF_evaluators_w2.push_back(pf.get_evaluator<T>());
    }

    pentagon_functions_w2_initialised = true;
}

// initialise pentagon functions of weight 3
template <class T>
void Interference<T>::initialise_PentagonFunctions_w3(){

    std::vector<PentagonFunctions::FunctionID> needed_functions = {
      {3,1}, {3,2}, {3,3}, {3,4}, {3,5}, {3,6}, {3,7}, {3,8},
      {3,9}, {3,10}, {3,11}, {3,12}, {3,13}, {3,14}, {3,15}, {3,16},
      {3,17}, {3,18}, {3,19}, {3,20}, {3,21}, {3,22}, {3,23}, {3,24},
      {3,25}, {3,26}, {3,27}, {3,28}, {3,29}, {3,30}, {3,31}, {3,32},
      {3,33}, {3,34}, {3,35}, {3,36}, {3,37}, {3,38}, {3,39}, {3,40},
      {3,41}, {3,42}, {3,43}, {3,44}, {3,45}, {3,46}, {3,47}, {3,48},
      {3,49}, {3,50}, {3,51}, {3,52}, {3,53}, {3,54}, {3,55}, {3,56},
      {3,57}, {3,58}, {3,59}, {3,60}, {3,61}, {3,62}, {3,63}, {3,64},
      {3,65}, {3,66}, {3,67}, {3,68}, {3,69}, {3,70}, {3,71}, {3,72},
      {3,73}, {3,74}, {3,75}, {3,76}, {3,77}, {3,78}, {3,79}, {3,80},
      {3,81}, {3,82}
#if defined (FULLCOL_ENABLED) || defined (LOOPSQ_ENABLED)
      ,
      {3,83}, {3,84}, {3,85}, {3,86}, {3,87}, {3,88}, {3,89}, {3,90},
      {3,91}, {3,92}, {3,93}, {3,94}, {3,95}, {3,96}, {3,97}, {3,98},
      {3,99}, {3,100}, {3,101}, {3,102}, {3,103}, {3,104}, {3,105},
      {3,106}, {3,107}, {3,108}, {3,109}, {3,110}, {3,111}
#endif
    };

    for (auto pf : needed_functions) {
        PF_evaluators_w3.push_back(pf.get_evaluator<T>());
    }

    pentagon_functions_w3_initialised = true;
}

// initialise pentagon functions of weight 4
template <class T>
void Interference<T>::initialise_PentagonFunctions_w4(){

    std::vector<PentagonFunctions::FunctionID> needed_functions = {
      {4,1}, {4,2}, {4,3}, {4,4}, {4,5}, {4,6}, {4,7},
      {4,8}, {4,9}, {4,10}, {4,11}, {4,12}, {4,13}, {4,14},
      {4,15}, {4,16}, {4,17}, {4,18}, {4,19}, {4,20}, {4,21},
      {4,22}, {4,23}, {4,24}, {4,25}, {4,26}, {4,27}, {4,28},
      {4,29}, {4,30}, {4,31}, {4,32}, {4,33}, {4,34}, {4,35},
      {4,36}, {4,37}, {4,38}, {4,39}, {4,40}, {4,41}, {4,42},
      {4,43}, {4,44}, {4,45}, {4,46}, {4,47}, {4,48}, {4,49},
      {4,50}, {4,51}, {4,52}, {4,53}, {4,54}, {4,55}, {4,56},
      {4,57}, {4,58}, {4,59}, {4,60}, {4,61}, {4,62}, {4,63},
      {4,64}, {4,65}, {4,66}, {4,67}, {4,68}, {4,69}, {4,70},
      {4,71}, {4,72}, {4,73}, {4,74}, {4,75}, {4,76}, {4,77},
      {4,78}, {4,79}, {4,80}, {4,81}, {4,82}, {4,83}, {4,84},
      {4,85}, {4,86}, {4,87}, {4,88}, {4,89}, {4,90}, {4,91},
      {4,92}, {4,93}, {4,94}, {4,95}, {4,96}, {4,97}, {4,98}, {4,99},
      {4,100}, {4,101}, {4,102}, {4,103}, {4,104}, {4,105},
      {4,106}, {4,107}, {4,108}, {4,109}, {4,110}, {4,111},
      {4,112}, {4,113}, {4,114}, {4,115}, {4,116}, {4,117},
      {4,118}, {4,119}, {4,120}, {4,121}, {4,122}, {4,123},
      {4,124}, {4,125}, {4,126}, {4,127}, {4,128}, {4,129},
      {4,130}, {4,131}, {4,132}, {4,133}, {4,134}, {4,135},
      {4,136}, {4,137}, {4,138}, {4,139}, {4,140}, {4,141},
      {4,142}, {4,143}, {4,144}, {4,145}, {4,146}, {4,147},
      {4,148}, {4,149}, {4,150}, {4,151}, {4,152}, {4,153},
      {4,154}, {4,155}, {4,156}, {4,157}, {4,158}, {4,159},
      {4,160}, {4,161}, {4,162}, {4,163}, {4,164}, {4,165},
      {4,166}, {4,167}, {4,168}, {4,169}, {4,170}, {4,171},
      {4,172}, {4,173}, {4,174}, {4,175}, {4,176}, {4,177},
      {4,178}, {4,179}, {4,180}, {4,181}, {4,182}, {4,183},
      {4,184}, {4,185}, {4,186}, {4,187}, {4,188}, {4,189},
      {4,190}, {4,191}, {4,192}, {4,193}, {4,194}, {4,195},
      {4,196}, {4,197}, {4,198}, {4,199}, {4,200}, {4,201},
      {4,202}, {4,203}, {4,204}, {4,205}, {4,206}, {4,207},
      {4,208}, {4,209}, {4,210}, {4,211}, {4,212}, {4,213},
      {4,214}, {4,215}, {4,216}, {4,217}, {4,218}, {4,219},
      {4,220}, {4,221}, {4,222}, {4,223}, {4,224}, {4,225},
      {4,226}, {4,227}, {4,228}, {4,229}, {4,230}, {4,231},
      {4,232}, {4,233}, {4,234}, {4,235}, {4,236}, {4,237},
      {4,238}, {4,239}, {4,240}, {4,241}, {4,242}, {4,243},
      {4,244}, {4,245}, {4,246}, {4,247}, {4,248}, {4,249},
      {4,250}, {4,251}, {4,252}, {4,253}, {4,254}, {4,255},
      {4,256}, {4,257}, {4,258}, {4,259}, {4,260}, {4,261},
      {4,262}, {4,263}, {4,264}, {4,265}, {4,266}, {4,267},
      {4,268}, {4,269}, {4,270}, {4,271}, {4,272}, {4,273},
      {4,274}, {4,275}, {4,276}, {4,277}, {4,278}, {4,279},
      {4,280}, {4,281}, {4,282}, {4,283}, {4,284}, {4,285},
      {4,286}, {4,287}, {4,288}, {4,289}, {4,290}, {4,291},
      {4,292}, {4,293}, {4,294}, {4,295}, {4,296}, {4,297},
      {4,298}, {4,299}, {4,300}, {4,301}, {4,302}, {4,303},
      {4,304}, {4,305}, {4,306}, {4,307}, {4,308}, {4,309},
      {4,310}, {4,311}, {4,312}, {4,313}, {4,314}, {4,315},
      {4,316}, {4,317}, {4,318}, {4,319}, {4,320}, {4,321},
      {4,322}, {4,323}, {4,324}, {4,325}, {4,326}, {4,327},
      {4,328}, {4,329}, {4,330}
#if defined (FULLCOL_ENABLED) || defined (LOOPSQ_ENABLED)
      ,
      {4,331}, {4,332}, {4,333}, {4,334}, {4,335}, {4,336}, {4,337},
      {4,338}, {4,339}, {4,340}, {4,341}, {4,342}, {4,343}, {4,344},
      {4,345}, {4,346}, {4,347}, {4,348}, {4,349}, {4,350}, {4,351},
      {4,352}, {4,353}, {4,354}, {4,355}, {4,356}, {4,357}, {4,358},
      {4,359}, {4,360}, {4,361}, {4,362}, {4,363}, {4,364}, {4,365},
      {4,366}, {4,367}, {4,368}, {4,369}, {4,370}, {4,371}, {4,372},
      {4,373}, {4,374}, {4,375}, {4,376}, {4,377}, {4,378}, {4,379},
      {4,380}, {4,381}, {4,382}, {4,383}, {4,384}, {4,385}, {4,386},
      {4,387}, {4,388}, {4,389}, {4,390}, {4,391}, {4,392}, {4,393},
      {4,394}, {4,395}, {4,396}, {4,397}, {4,398}, {4,399}, {4,400},
      {4,401}, {4,402}, {4,403}, {4,404}, {4,405}, {4,406}, {4,407},
      {4,408}, {4,409}, {4,410}, {4,411}, {4,412}, {4,413}, {4,414},
      {4,415}, {4,416}, {4,417}, {4,418}, {4,419}, {4,420}, {4,421},
      {4,422}, {4,423}, {4,424}, {4,425}, {4,426}, {4,427}, {4,428},
      {4,429}, {4,430}, {4,431}, {4,432}, {4,433}, {4,434}, {4,435},
      {4,436}, {4,437}, {4,438}, {4,439}, {4,440}, {4,441}, {4,442},
      {4,443}, {4,444}, {4,445}, {4,446}, {4,447}, {4,448}, {4,449},
      {4,450}, {4,451}, {4,452}, {4,453}, {4,454}, {4,455}, {4,456},
      {4,457}, {4,458}, {4,459}, {4,460}, {4,461}, {4,462}, {4,463},
      {4,464}, {4,465}, {4,466}, {4,467}, {4,468}, {4,469}, {4,470},
      {4,471}, {4,472}
#endif
    };

    for (auto pf : needed_functions) {
        PF_evaluators_w4.push_back(pf.get_evaluator<T>());
    }

    pentagon_functions_w4_initialised = true;
}


// evaluate pentagon functions of weight 1
template <class T>
void Interference<T>::set_fw1(){

    PentagonFunctions::Kin<T> k(v);

    for (size_t i = 0; i < PF_evaluators_w1.size(); ++i) {
#if defined (FULLCOL_ENABLED) || defined (LOOPSQ_ENABLED)
        if(i < 19){
            fw1[i] = PF_evaluators_w1.at(i)(k);
            fw1np[i] = fw1[i];
        } else {
            fw1np[i] = PF_evaluators_w1.at(i)(k);            
        }
#else
        fw1[i] = PF_evaluators_w1.at(i)(k);
#endif
    }
}

// evaluate pentagon functions of weight 2
template <class T>
void Interference<T>::set_fw2(){

    PentagonFunctions::Kin<T> k(v);

    for (size_t i = 0; i < PF_evaluators_w2.size(); ++i) {
        fw2[i] = PF_evaluators_w2.at(i)(k);
    }
}

// evaluate pentagon functions of weight 3
template <class T>
void Interference<T>::set_fw3(){

    PentagonFunctions::Kin<T> k(v);

    for (size_t i = 0; i < PF_evaluators_w3.size(); ++i) {
#if defined (FULLCOL_ENABLED) || defined (LOOPSQ_ENABLED)
        if(i < 82){
            fw3[i] = PF_evaluators_w3.at(i)(k);
            fw3np[i] = fw3[i];
        } else {
            fw3np[i] = PF_evaluators_w3.at(i)(k);            
        }
#else
        fw3[i] = PF_evaluators_w3.at(i)(k);
#endif
    }
}

// evaluate pentagon functions of weight 4
template <class T>
void Interference<T>::set_fw4(){

    PentagonFunctions::Kin<T> k(v);

    for (size_t i = 0; i < PF_evaluators_w4.size(); ++i) {
#if defined (FULLCOL_ENABLED) || defined (LOOPSQ_ENABLED)
        if(i < 330){
            fw4[i] = PF_evaluators_w4.at(i)(k);
            fw4np[i] = fw4[i];
        } else {
            fw4np[i] = PF_evaluators_w4.at(i)(k);
        }
#else
        fw4[i] = PF_evaluators_w4.at(i)(k);
#endif
    }
}

// --------------------------------------------------------
// Evaluate rational functions
// --------------------------------------------------------
template <class T>
void Interference<T>::set_rational_functions(){

  // q qb -> g a a @0-loop
  if(qqb_active){
      evaluate_0l_rf_qqgaa(k, rat0l_qqgaa);
  }
  // q g -> q a a @0-loop
  if(qg_active){
      evaluate_0l_rf_qgqaa(k, rat0l_qgqaa);
  }

  if(loop_ord >= 1){
    // q qb -> g a a @1-loop
    if(qqb_active){
        evaluate_1l_rf_qqgaa(k, rat1l_qqgaa);
    }
    // q g -> q a a @1-loop
    if(qg_active){
        evaluate_1l_rf_qgqaa(k, rat1l_qgqaa);
    }
  }

  if(loop_ord == 2){
    // q qb -> g a a @2-loop
    if(qqb_active){
        evaluate_2lLC_rf_qqgaa(k, rat2lLC_qqgaa);
#ifdef FULLCOL_ENABLED
        evaluate_2lNLC_rf_qqgaa(k, rat2lNLC_qqgaa);
#endif
    }
    // q g -> q a a @2-loop
    if(qg_active){
        evaluate_2lLC_rf_qgqaa(k, rat2lLC_qgqaa);
#ifdef FULLCOL_ENABLED
        evaluate_2lNLC_rf_qgqaa(k, rat2lNLC_qgqaa);
#endif
    }
  }
}

template <class T>
void Interference<T>::set_1lHAMP_rational_functions(){
  // q qb -> g a a
  if(qqb_active){
      evaluate_1lha_rf_qqgaa(k, hrat_qqgaa);
  }
  // q g -> q a a
  if(qg_active){
      evaluate_1lha_rf_qgqaa(k, hrat_qgqaa);
  }
}

#ifdef LOOPSQ_ENABLED
template <class T>
void Interference<T>::set_2lHAMP_rational_functions(){
  // q qb -> g a a
  if(qqb_active){
      evaluate_2lha_rf_qqgaa(kk, hrat2l_qqgaa);
  }
  // q g -> q a a
  if(qg_active){
      evaluate_2lha_rf_qgqaa(kk, hrat2l_qgqaa);
  }
}
#endif

// --------------------------------------------------------
// Evaluate transcendental functions
// --------------------------------------------------------
template <class T>
void Interference<T>::set_transcendental_functions(){

  if(loop_ord == 2){
    // q qb -> g a a @2-loop
    if(qqb_active){
        evaluate_2lLC_tf_qqgaa(tcs,fw1,fw2,fw3,fw4,tf2lLC_qqgaa);
#ifdef FULLCOL_ENABLED
        evaluate_2lNLC_tf_qqgaa(tcs,fw1np,fw2,fw3np,fw4np,tf2lNLC_qqgaa);

        std::array<int,186> eps5qqb;
        eps5qqb = {38, 39, 40, 41, 42, 43, 44, 49, 57, 58, 61, 62, 63, 64, 65, 
                   66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 
                   81, 82, 83, 84, 374, 375, 376, 377, 378, 379, 380, 381, 382, 
                   383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 
                   395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 
                   407, 408, 409, 410,
                   535, 536, 537, 538, 539, 540, 541, 546, 578, 579, 588, 589, 
                   590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 
                   602, 603, 604, 605, 606, 888, 889, 890, 891, 892, 893, 894, 
                   895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 
                   907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 
                   919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 
                   931, 932, 933, 934,
                   1011, 1037, 1038, 1039, 1040, 1041, 1042, 1058, 1059, 1060, 
                   1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070,
                   1071, 1072, 1073, 1074, 1075, 1076, 1225, 1226, 1227, 1228, 
                   1229, 1230, 1231, 1232, 1233,
                   1271, 1297, 1311, 1312};
        for(int i=0; i<186; i++){
            int loc = eps5qqb[i];
            tf2lNLC_qqgaa[loc] *= eps5;
        }
#endif
    }
    // q g -> q a a @2-loop
    if(qg_active){
//         evaluate_2lLC_rf_qgqaa(k, rat2lLC_qgqaa);
#ifdef FULLCOL_ENABLED
        evaluate_2lNLC_tf_qgqaa(tcs,fw1np,fw2,fw3np,fw4np,tf2lNLC_qgqaa);

        std::array<int,193> eps5qg;
        eps5qg = {25, 26, 27, 28, 29, 30, 31, 36, 39, 40, 42, 43, 44, 45, 46,
                  47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61,
                  384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395,
                  396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407,
                  408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419,
                  420, 421, 422, 423, 424,
                  500, 501, 531, 532, 533, 534, 535, 536, 537, 538, 540, 571,
                  572, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586,
                  587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598,
                  599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610,
                  611, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829,
                  830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841,
                  842, 843, 844, 845,
                  1015, 1045, 1046, 1047, 1048, 1061, 1062, 1065, 1066, 1067,
                  1068, 1069, 1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077,
                  1078, 1082, 1083, 1084, 1085, 1224, 1225, 1226, 1227, 1228,
                  1229, 1230, 1231, 1232, 1233,
                  1271, 1294, 1313, 1314,
                  1367, 1401, 1402, 1403, 1404, 1462, 1463};
        for(int i=0; i<193; i++){
            int loc = eps5qg[i];
            tf2lNLC_qgqaa[loc] *= eps5;
        }
#endif
    }
  }
}

#ifdef LOOPSQ_ENABLED
template <class T>
void Interference<T>::set_2lHAMP_transcendental_functions(){

    // q qb -> g a a @2-loop
    if(qqb_active){
        evaluate_2lHA_tf_qqgaa(tcs,fw1np,fw2,fw3np,fw4np,tf2lha_qqgaa);
    }
    // q g -> q a a @2-loop
    if(qg_active){
        evaluate_2lHA_tf_qgqaa(tcs,fw1np,fw2,fw3np,fw4np,tf2lha_qgqaa);
    }
}
#endif

// --------------------------------------------------------
// Evaluate square of spinor phases
// --------------------------------------------------------
template <class T>
void Interference<T>::set_spinor_phases(){
  // q qb -> g a a
  if(qqb_active){
      SpPh_qqb[0] = 8*k[1]*k[5]*k[5]*k[7]*k[11]*k[12]*k[13];      // Lmmp
      SpPh_qqb[1] = 8*k[1]*k[7]*k[10]*k[11]*k[14]/(k[12]*k[12]);  // Lmpm
      SpPh_qqb[2] = 8*k[1]*k[10]*k[12]*k[13]*k[14]/(k[11]*k[11]); // Lpmm
      SpPh_qqb[3] = 8*k[1]*k[2]*k[2]*k[10]*k[12]*k[13]*k[14];     // Lmpp
      SpPh_qqb[4] = 8*k[1]*k[7]*k[10]*k[11]*k[14]/(k[13]*k[13]);  // Lpmp
      SpPh_qqb[5] = 8*k[1]*k[7]*k[11]*k[12]*k[13]/(k[14]*k[14]);  // Lppm
      SpPh_qqb[6] = k[1]*k[7]*k[10]*k[12]/k[11];                  // Lppp
      SpPh_qqb[6] *=SpPh_qqb[6];
      SpPh_qqb[6] *=8*k[1];
      SpPh_qqb[7] = k[1]*k[2]*k[11]*k[13]*k[14];                  // Lmmm
      SpPh_qqb[7] *=SpPh_qqb[7]; 
      SpPh_qqb[7] *=8*k[1];
  }

  // q g -> q a a
  if(qg_active){
      SpPh_qg[0] = 8*k[5]*k[5]*k[6]*k[7]*k[8]*k[12]/k[11];      // Lmmp
      SpPh_qg[1] = 8*k[6]*k[7]*k[10]*k[15]/(k[11]*k[12]*k[12]); // Lmpm
      SpPh_qg[2] = 8*k[1]*k[1]*k[8]*k[10]*k[12]*k[15]/k[11];    // Lpmm
      SpPh_qg[3] = 8*k[2]*k[2]*k[8]*k[10]*k[12]*k[15]/k[11];    // Lmpp
      SpPh_qg[4] = 8*k[3]*k[3]*k[6]*k[7]*k[10]*k[15]/k[11];     // Lpmp
      SpPh_qg[5] = 8*k[6]*k[7]*k[8]*k[12]/(k[11]*k[15]*k[15]);  // Lppm
      SpPh_qg[6] = k[1]*k[7]*k[10]*k[12]/k[11];                 // Lppp
      SpPh_qg[6] *=SpPh_qg[6];
      SpPh_qg[6] *=8/k[11];
      SpPh_qg[7] = k[2]*k[6]*k[8]*k[15]/k[11];                  // Lmmm
      SpPh_qg[7] *=SpPh_qg[7];
      SpPh_qg[7] *=8/k[11];
  }
}

// --------------------------------------------------------
// Evaluate 1-loop helicity amplitudes
// --------------------------------------------------------
template <class T>
void Interference<T>::evaluate_1loop_helamplitudes(){

  using TC = std::complex<T>;

  std::array<TC,8> AE;
  std::array<TC,8> AO;

  // q qb -> g a a
  if(qqb_active){

    // qqb channel - CA contribution
    //-------------------------------------------------------------------------
    AE[0] = qqb_HA1L_LmmpE_CA(hrat_qqgaa,tcs,fw1,fw2);
    AO[0] = qqb_HA1L_LmmpO_CA(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CA[0] = AE[0] + tr5*AO[0];
    Aqqb_1l_CA[8] = AE[0] - tr5*AO[0];

    AE[1] = qqb_HA1L_LmpmE_CA(hrat_qqgaa,tcs,fw1,fw2);
    AO[1] = qqb_HA1L_LmpmO_CA(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CA[1] = AE[1] + tr5*AO[1];
    Aqqb_1l_CA[9] = AE[1] - tr5*AO[1];

    AE[2] = qqb_HA1L_LpmmE_CA(hrat_qqgaa,tcs,fw1,fw2);
    AO[2] = qqb_HA1L_LpmmO_CA(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CA[2]  = AE[2] + tr5*AO[2];
    Aqqb_1l_CA[10] = AE[2] - tr5*AO[2];

    AE[3] = qqb_HA1L_LmppE_CA(hrat_qqgaa,tcs,fw1,fw2);
    AO[3] = qqb_HA1L_LmppO_CA(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CA[3]  = AE[3] + tr5*AO[3];
    Aqqb_1l_CA[11] = AE[3] - tr5*AO[3];

    AE[4] = qqb_HA1L_LpmpE_CA(hrat_qqgaa,tcs,fw1,fw2);
    AO[4] = qqb_HA1L_LpmpO_CA(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CA[4]  = AE[4] + tr5*AO[4];
    Aqqb_1l_CA[12] = AE[4] - tr5*AO[4];

    AE[5] = qqb_HA1L_LppmE_CA(hrat_qqgaa,tcs,fw1,fw2);
    AO[5] = qqb_HA1L_LppmO_CA(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CA[5]  = AE[5] + tr5*AO[5];
    Aqqb_1l_CA[13] = AE[5] - tr5*AO[5];

    AE[6] = qqb_HA1L_LpppE_CA(hrat_qqgaa);
    AO[6] = qqb_HA1L_LpppO_CA(hrat_qqgaa);
    Aqqb_1l_CA[6]  = AE[6] + tr5*AO[6];
    Aqqb_1l_CA[14] = AE[6] - tr5*AO[6];

    AE[7] = qqb_HA1L_LmmmE_CA(hrat_qqgaa);
    AO[7] = qqb_HA1L_LmmmO_CA(hrat_qqgaa);
    Aqqb_1l_CA[7]  = AE[7] + tr5*AO[7];
    Aqqb_1l_CA[15] = AE[7] - tr5*AO[7];

    // qqb channel - CF contribution
    //-------------------------------------------------------------------------
    AE[0] = qqb_HA1L_LmmpE_CF(hrat_qqgaa,tcs,fw1,fw2);
    AO[0] = qqb_HA1L_LmmpO_CF(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CF[0] = AE[0] + tr5*AO[0];
    Aqqb_1l_CF[8] = AE[0] - tr5*AO[0];

    AE[1] = qqb_HA1L_LmpmE_CF(hrat_qqgaa,tcs,fw1,fw2);
    AO[1] = qqb_HA1L_LmpmO_CF(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CF[1] = AE[1] + tr5*AO[1];
    Aqqb_1l_CF[9] = AE[1] - tr5*AO[1];

    AE[2] = qqb_HA1L_LpmmE_CF(hrat_qqgaa,tcs,fw1,fw2);
    AO[2] = qqb_HA1L_LpmmO_CF(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CF[2]  = AE[2] + tr5*AO[2];
    Aqqb_1l_CF[10] = AE[2] - tr5*AO[2];

    AE[3] = qqb_HA1L_LmppE_CF(hrat_qqgaa,tcs,fw1,fw2);
    AO[3] = qqb_HA1L_LmppO_CF(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CF[3]  = AE[3] + tr5*AO[3];
    Aqqb_1l_CF[11] = AE[3] - tr5*AO[3];

    AE[4] = qqb_HA1L_LpmpE_CF(hrat_qqgaa,tcs,fw1,fw2);
    AO[4] = qqb_HA1L_LpmpO_CF(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CF[4]  = AE[4] + tr5*AO[4];
    Aqqb_1l_CF[12] = AE[4] - tr5*AO[4];

    AE[5] = qqb_HA1L_LppmE_CF(hrat_qqgaa,tcs,fw1,fw2);
    AO[5] = qqb_HA1L_LppmO_CF(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_CF[5]  = AE[5] + tr5*AO[5];
    Aqqb_1l_CF[13] = AE[5] - tr5*AO[5];

    AE[6] = qqb_HA1L_LpppE_CF(hrat_qqgaa);
    AO[6] = qqb_HA1L_LpppO_CF(hrat_qqgaa);
    Aqqb_1l_CF[6]  = AE[6] + tr5*AO[6];
    Aqqb_1l_CF[14] = AE[6] - tr5*AO[6];

    AE[7] = qqb_HA1L_LmmmE_CF(hrat_qqgaa);
    AO[7] = qqb_HA1L_LmmmO_CF(hrat_qqgaa);
    Aqqb_1l_CF[7]  = AE[7] + tr5*AO[7];
    Aqqb_1l_CF[15] = AE[7] - tr5*AO[7];

    // qqb channel - nfaa contribution
    //-------------------------------------------------------------------------
    AE[0] = qqb_HA1L_LmmpE_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    AO[0] = qqb_HA1L_LmmpO_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_nfaa[0] = AE[0] + tr5*AO[0];
    Aqqb_1l_nfaa[8] = AE[0] - tr5*AO[0];

    AE[1] = qqb_HA1L_LmpmE_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    AO[1] = qqb_HA1L_LmpmO_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_nfaa[1] = AE[1] + tr5*AO[1];
    Aqqb_1l_nfaa[9] = AE[1] - tr5*AO[1];

    AE[2] = qqb_HA1L_LpmmE_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    AO[2] = qqb_HA1L_LpmmO_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_nfaa[2]  = AE[2] + tr5*AO[2];
    Aqqb_1l_nfaa[10] = AE[2] - tr5*AO[2];

    AE[3] = qqb_HA1L_LmppE_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    AO[3] = qqb_HA1L_LmppO_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_nfaa[3]  = AE[3] + tr5*AO[3];
    Aqqb_1l_nfaa[11] = AE[3] - tr5*AO[3];

    AE[4] = qqb_HA1L_LpmpE_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    AO[4] = qqb_HA1L_LpmpO_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_nfaa[4]  = AE[4] + tr5*AO[4];
    Aqqb_1l_nfaa[12] = AE[4] - tr5*AO[4];

    AE[5] = qqb_HA1L_LppmE_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    AO[5] = qqb_HA1L_LppmO_nfaa(hrat_qqgaa,tcs,fw1,fw2);
    Aqqb_1l_nfaa[5]  = AE[5] + tr5*AO[5];
    Aqqb_1l_nfaa[13] = AE[5] - tr5*AO[5];

    AE[6] = qqb_HA1L_LpppE_nfaa(hrat_qqgaa);
    AO[6] = qqb_HA1L_LpppO_nfaa(hrat_qqgaa);
    Aqqb_1l_nfaa[6]  = AE[6] + tr5*AO[6];
    Aqqb_1l_nfaa[14] = AE[6] - tr5*AO[6];

    AE[7] = qqb_HA1L_LmmmE_nfaa(hrat_qqgaa);
    AO[7] = qqb_HA1L_LmmmO_nfaa(hrat_qqgaa);
    Aqqb_1l_nfaa[7]  = AE[7] + tr5*AO[7];
    Aqqb_1l_nfaa[15] = AE[7] - tr5*AO[7];
  }

  // q g -> q a a
  if(qg_active){

    // qg channel - CA contribution
    //-------------------------------------------------------------------------
    AE[0] = qg_HA1L_LmmpE_CA(hrat_qgqaa,tcs,fw1,fw2);
    AO[0] = qg_HA1L_LmmpO_CA(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CA[0] = AE[0] + tr5*AO[0];
    Aqg_1l_CA[8] = AE[0] - tr5*AO[0];

    AE[1] = qg_HA1L_LmpmE_CA(hrat_qgqaa,tcs,fw1,fw2);
    AO[1] = qg_HA1L_LmpmO_CA(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CA[1] = AE[1] + tr5*AO[1];
    Aqg_1l_CA[9] = AE[1] - tr5*AO[1];

    AE[2] = qg_HA1L_LpmmE_CA(hrat_qgqaa,tcs,fw1,fw2);
    AO[2] = qg_HA1L_LpmmO_CA(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CA[2]  = AE[2] + tr5*AO[2];
    Aqg_1l_CA[10] = AE[2] - tr5*AO[2];

    AE[3] = qg_HA1L_LmppE_CA(hrat_qgqaa,tcs,fw1,fw2);
    AO[3] = qg_HA1L_LmppO_CA(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CA[3]  = AE[3] + tr5*AO[3];
    Aqg_1l_CA[11] = AE[3] - tr5*AO[3];

    AE[4] = qg_HA1L_LpmpE_CA(hrat_qgqaa,tcs,fw1,fw2);
    AO[4] = qg_HA1L_LpmpO_CA(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CA[4]  = AE[4] + tr5*AO[4];
    Aqg_1l_CA[12] = AE[4] - tr5*AO[4];

    AE[5] = qg_HA1L_LppmE_CA(hrat_qgqaa,tcs,fw1,fw2);
    AO[5] = qg_HA1L_LppmO_CA(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CA[5]  = AE[5] + tr5*AO[5];
    Aqg_1l_CA[13] = AE[5] - tr5*AO[5];

    AE[6] = qg_HA1L_LpppE_CA(hrat_qgqaa);
    AO[6] = qg_HA1L_LpppO_CA(hrat_qgqaa);
    Aqg_1l_CA[6]  = AE[6] + tr5*AO[6];
    Aqg_1l_CA[14] = AE[6] - tr5*AO[6];

    AE[7] = qg_HA1L_LmmmE_CA(hrat_qgqaa);
    AO[7] = qg_HA1L_LmmmO_CA(hrat_qgqaa);
    Aqg_1l_CA[7]  = AE[7] + tr5*AO[7];
    Aqg_1l_CA[15] = AE[7] - tr5*AO[7];

    // qg channel - CF contribution
    //-------------------------------------------------------------------------
    AE[0] = qg_HA1L_LmmpE_CF(hrat_qgqaa,tcs,fw1,fw2);
    AO[0] = qg_HA1L_LmmpO_CF(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CF[0] = AE[0] + tr5*AO[0];
    Aqg_1l_CF[8] = AE[0] - tr5*AO[0];

    AE[1] = qg_HA1L_LmpmE_CF(hrat_qgqaa,tcs,fw1,fw2);
    AO[1] = qg_HA1L_LmpmO_CF(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CF[1] = AE[1] + tr5*AO[1];
    Aqg_1l_CF[9] = AE[1] - tr5*AO[1];

    AE[2] = qg_HA1L_LpmmE_CF(hrat_qgqaa,tcs,fw1,fw2);
    AO[2] = qg_HA1L_LpmmO_CF(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CF[2]  = AE[2] + tr5*AO[2];
    Aqg_1l_CF[10] = AE[2] - tr5*AO[2];

    AE[3] = qg_HA1L_LmppE_CF(hrat_qgqaa,tcs,fw1,fw2);
    AO[3] = qg_HA1L_LmppO_CF(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CF[3]  = AE[3] + tr5*AO[3];
    Aqg_1l_CF[11] = AE[3] - tr5*AO[3];

    AE[4] = qg_HA1L_LpmpE_CF(hrat_qgqaa,tcs,fw1,fw2);
    AO[4] = qg_HA1L_LpmpO_CF(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CF[4]  = AE[4] + tr5*AO[4];
    Aqg_1l_CF[12] = AE[4] - tr5*AO[4];

    AE[5] = qg_HA1L_LppmE_CF(hrat_qgqaa,tcs,fw1,fw2);
    AO[5] = qg_HA1L_LppmO_CF(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_CF[5]  = AE[5] + tr5*AO[5];
    Aqg_1l_CF[13] = AE[5] - tr5*AO[5];

    AE[6] = qg_HA1L_LpppE_CF(hrat_qgqaa);
    AO[6] = qg_HA1L_LpppO_CF(hrat_qgqaa);
    Aqg_1l_CF[6]  = AE[6] + tr5*AO[6];
    Aqg_1l_CF[14] = AE[6] - tr5*AO[6];

    AE[7] = qg_HA1L_LmmmE_CF(hrat_qgqaa);
    AO[7] = qg_HA1L_LmmmO_CF(hrat_qgqaa);
    Aqg_1l_CF[7]  = AE[7] + tr5*AO[7];
    Aqg_1l_CF[15] = AE[7] - tr5*AO[7];

    // qg channel - nfaa contribution
    //-------------------------------------------------------------------------
    AE[0] = qg_HA1L_LmmpE_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    AO[0] = qg_HA1L_LmmpO_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_nfaa[0] = AE[0] + tr5*AO[0];
    Aqg_1l_nfaa[8] = AE[0] - tr5*AO[0];

    AE[1] = qg_HA1L_LmpmE_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    AO[1] = qg_HA1L_LmpmO_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_nfaa[1] = AE[1] + tr5*AO[1];
    Aqg_1l_nfaa[9] = AE[1] - tr5*AO[1];

    AE[2] = qg_HA1L_LpmmE_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    AO[2] = qg_HA1L_LpmmO_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_nfaa[2]  = AE[2] + tr5*AO[2];
    Aqg_1l_nfaa[10] = AE[2] - tr5*AO[2];

    AE[3] = qg_HA1L_LmppE_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    AO[3] = qg_HA1L_LmppO_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_nfaa[3]  = AE[3] + tr5*AO[3];
    Aqg_1l_nfaa[11] = AE[3] - tr5*AO[3];

    AE[4] = qg_HA1L_LpmpE_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    AO[4] = qg_HA1L_LpmpO_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_nfaa[4]  = AE[4] + tr5*AO[4];
    Aqg_1l_nfaa[12] = AE[4] - tr5*AO[4];

    AE[5] = qg_HA1L_LppmE_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    AO[5] = qg_HA1L_LppmO_nfaa(hrat_qgqaa,tcs,fw1,fw2);
    Aqg_1l_nfaa[5]  = AE[5] + tr5*AO[5];
    Aqg_1l_nfaa[13] = AE[5] - tr5*AO[5];

    AE[6] = qg_HA1L_LpppE_nfaa(hrat_qgqaa);
    AO[6] = qg_HA1L_LpppO_nfaa(hrat_qgqaa);
    Aqg_1l_nfaa[6]  = AE[6] + tr5*AO[6];
    Aqg_1l_nfaa[14] = AE[6] - tr5*AO[6];

    AE[7] = qg_HA1L_LmmmE_nfaa(hrat_qgqaa);
    AO[7] = qg_HA1L_LmmmO_nfaa(hrat_qgqaa);
    Aqg_1l_nfaa[7]  = AE[7] + tr5*AO[7];
    Aqg_1l_nfaa[15] = AE[7] - tr5*AO[7];
  }

}

#ifdef LOOPSQ_ENABLED
// --------------------------------------------------------
// Evaluate 2-loop helicity amplitudes.
// Only fermionic loops with two photons attached for now
// --------------------------------------------------------
template <class T>
void Interference<T>::evaluate_2loop_helamplitudes(){

  using TC = std::complex<T>;

  std::array<TC,8> AE;
  std::array<TC,8> AO;

  // q qb -> g a a
  if(qqb_active){

    // qqb channel - nfaa*Nc contribution
    //------------------------------------------------------------------------
    AE[0] = qqb_HA2L_LmmpE_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[0] = qqb_HA2L_LmmpO_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcp1[0] = AE[0] + tr5*AO[0];
    Aqqb_2l_nfaaNcp1[8] = AE[0] - tr5*AO[0];

    AE[1] = qqb_HA2L_LmpmE_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[1] = qqb_HA2L_LmpmO_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcp1[1] = AE[1] + tr5*AO[1];
    Aqqb_2l_nfaaNcp1[9] = AE[1] - tr5*AO[1];

    AE[2] = qqb_HA2L_LpmmE_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[2] = qqb_HA2L_LpmmO_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcp1[2]  = AE[2] + tr5*AO[2];
    Aqqb_2l_nfaaNcp1[10] = AE[2] - tr5*AO[2];

    AE[3] = qqb_HA2L_LmppE_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[3] = qqb_HA2L_LmppO_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcp1[3]  = AE[3] + tr5*AO[3];
    Aqqb_2l_nfaaNcp1[11] = AE[3] - tr5*AO[3];

    AE[4] = qqb_HA2L_LpmpE_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[4] = qqb_HA2L_LpmpO_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcp1[4]  = AE[4] + tr5*AO[4];
    Aqqb_2l_nfaaNcp1[12] = AE[4] - tr5*AO[4];

    AE[5] = qqb_HA2L_LppmE_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[5] = qqb_HA2L_LppmO_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcp1[5]  = AE[5] + tr5*AO[5];
    Aqqb_2l_nfaaNcp1[13] = AE[5] - tr5*AO[5];

    AE[6] = qqb_HA2L_LpppE_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[6] = qqb_HA2L_LpppO_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcp1[6]  = AE[6] + tr5*AO[6];
    Aqqb_2l_nfaaNcp1[14] = AE[6] - tr5*AO[6];

    AE[7] = qqb_HA2L_LmmmE_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[7] = qqb_HA2L_LmmmO_nfaaNcp1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcp1[7]  = AE[7] + tr5*AO[7];
    Aqqb_2l_nfaaNcp1[15] = AE[7] - tr5*AO[7];

    // qqb channel - nfaa/Nc contribution
    //------------------------------------------------------------------------
    AE[0] = qqb_HA2L_LmmpE_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[0] = qqb_HA2L_LmmpO_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcm1[0] = AE[0] + tr5*AO[0];
    Aqqb_2l_nfaaNcm1[8] = AE[0] - tr5*AO[0];

    AE[1] = qqb_HA2L_LmpmE_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[1] = qqb_HA2L_LmpmO_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcm1[1] = AE[1] + tr5*AO[1];
    Aqqb_2l_nfaaNcm1[9] = AE[1] - tr5*AO[1];

    AE[2] = qqb_HA2L_LpmmE_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[2] = qqb_HA2L_LpmmO_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcm1[2]  = AE[2] + tr5*AO[2];
    Aqqb_2l_nfaaNcm1[10] = AE[2] - tr5*AO[2];

    AE[3] = qqb_HA2L_LmppE_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[3] = qqb_HA2L_LmppO_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcm1[3]  = AE[3] + tr5*AO[3];
    Aqqb_2l_nfaaNcm1[11] = AE[3] - tr5*AO[3];

    AE[4] = qqb_HA2L_LpmpE_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[4] = qqb_HA2L_LpmpO_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcm1[4]  = AE[4] + tr5*AO[4];
    Aqqb_2l_nfaaNcm1[12] = AE[4] - tr5*AO[4];

    AE[5] = qqb_HA2L_LppmE_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[5] = qqb_HA2L_LppmO_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcm1[5]  = AE[5] + tr5*AO[5];
    Aqqb_2l_nfaaNcm1[13] = AE[5] - tr5*AO[5];

    AE[6] = qqb_HA2L_LpppE_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[6] = qqb_HA2L_LpppO_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcm1[6]  = AE[6] + tr5*AO[6];
    Aqqb_2l_nfaaNcm1[14] = AE[6] - tr5*AO[6];

    AE[7] = qqb_HA2L_LmmmE_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    AO[7] = qqb_HA2L_LmmmO_nfaaNcm1(hrat2l_qqgaa,tf2lha_qqgaa,eps5t);
    Aqqb_2l_nfaaNcm1[7]  = AE[7] + tr5*AO[7];
    Aqqb_2l_nfaaNcm1[15] = AE[7] - tr5*AO[7];

    // qqb channel - nfaa*nf contribution
    //------------------------------------------------------------------------
    AE[0] = qqb_HA2L_LmmpE_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    AO[0] = qqb_HA2L_LmmpO_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    Aqqb_2l_nfnfaa[0] = AE[0] + tr5*AO[0];
    Aqqb_2l_nfnfaa[8] = AE[0] - tr5*AO[0];

    AE[1] = qqb_HA2L_LmpmE_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    AO[1] = qqb_HA2L_LmpmO_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    Aqqb_2l_nfnfaa[1] = AE[1] + tr5*AO[1];
    Aqqb_2l_nfnfaa[9] = AE[1] - tr5*AO[1];

    AE[2] = qqb_HA2L_LpmmE_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    AO[2] = qqb_HA2L_LpmmO_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    Aqqb_2l_nfnfaa[2]  = AE[2] + tr5*AO[2];
    Aqqb_2l_nfnfaa[10] = AE[2] - tr5*AO[2];

    AE[3] = qqb_HA2L_LmppE_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    AO[3] = qqb_HA2L_LmppO_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    Aqqb_2l_nfnfaa[3]  = AE[3] + tr5*AO[3];
    Aqqb_2l_nfnfaa[11] = AE[3] - tr5*AO[3];

    AE[4] = qqb_HA2L_LpmpE_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    AO[4] = qqb_HA2L_LpmpO_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    Aqqb_2l_nfnfaa[4]  = AE[4] + tr5*AO[4];
    Aqqb_2l_nfnfaa[12] = AE[4] - tr5*AO[4];

    AE[5] = qqb_HA2L_LppmE_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    AO[5] = qqb_HA2L_LppmO_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    Aqqb_2l_nfnfaa[5]  = AE[5] + tr5*AO[5];
    Aqqb_2l_nfnfaa[13] = AE[5] - tr5*AO[5];

    AE[6] = qqb_HA2L_LpppE_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    AO[6] = qqb_HA2L_LpppO_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    Aqqb_2l_nfnfaa[6]  = AE[6] + tr5*AO[6];
    Aqqb_2l_nfnfaa[14] = AE[6] - tr5*AO[6];

    AE[7] = qqb_HA2L_LmmmE_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    AO[7] = qqb_HA2L_LmmmO_nfnfaa(hrat2l_qqgaa,tf2lha_qqgaa);
    Aqqb_2l_nfnfaa[7]  = AE[7] + tr5*AO[7];
    Aqqb_2l_nfnfaa[15] = AE[7] - tr5*AO[7];
  }

  // q g -> q a a
  if(qg_active){

    // qg channel - nfaa*Nc contribution
    //-------------------------------------------------------------------------
    AO[0] = qg_HA2L_LmmpO_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AE[0] = qg_HA2L_LmmpE_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcp1[0] = AE[0] + tr5*AO[0];
    Aqg_2l_nfaaNcp1[8] = AE[0] - tr5*AO[0];

    AE[1] = qg_HA2L_LmpmE_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[1] = qg_HA2L_LmpmO_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcp1[1] = AE[1] + tr5*AO[1];
    Aqg_2l_nfaaNcp1[9] = AE[1] - tr5*AO[1];

    AE[2] = qg_HA2L_LpmmE_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[2] = qg_HA2L_LpmmO_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcp1[2]  = AE[2] + tr5*AO[2];
    Aqg_2l_nfaaNcp1[10] = AE[2] - tr5*AO[2];

    AE[3] = qg_HA2L_LmppE_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[3] = qg_HA2L_LmppO_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcp1[3]  = AE[3] + tr5*AO[3];
    Aqg_2l_nfaaNcp1[11] = AE[3] - tr5*AO[3];

    AE[4] = qg_HA2L_LpmpE_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[4] = qg_HA2L_LpmpO_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcp1[4]  = AE[4] + tr5*AO[4];
    Aqg_2l_nfaaNcp1[12] = AE[4] - tr5*AO[4];

    AE[5] = qg_HA2L_LppmE_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[5] = qg_HA2L_LppmO_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcp1[5]  = AE[5] + tr5*AO[5];
    Aqg_2l_nfaaNcp1[13] = AE[5] - tr5*AO[5];

    AE[6] = qg_HA2L_LpppE_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[6] = qg_HA2L_LpppO_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcp1[6]  = AE[6] + tr5*AO[6];
    Aqg_2l_nfaaNcp1[14] = AE[6] - tr5*AO[6];

    AE[7] = qg_HA2L_LmmmE_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[7] = qg_HA2L_LmmmO_nfaaNcp1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcp1[7]  = AE[7] + tr5*AO[7];
    Aqg_2l_nfaaNcp1[15] = AE[7] - tr5*AO[7];

    // qg channel - nfaa/Nc contribution
    //-------------------------------------------------------------------------
    AE[0] = qg_HA2L_LmmpE_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[0] = qg_HA2L_LmmpO_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcm1[0] = AE[0] + tr5*AO[0];
    Aqg_2l_nfaaNcm1[8] = AE[0] - tr5*AO[0];

    AE[1] = qg_HA2L_LmpmE_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[1] = qg_HA2L_LmpmO_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcm1[1] = AE[1] + tr5*AO[1];
    Aqg_2l_nfaaNcm1[9] = AE[1] - tr5*AO[1];

    AE[2] = qg_HA2L_LpmmE_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[2] = qg_HA2L_LpmmO_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcm1[2]  = AE[2] + tr5*AO[2];
    Aqg_2l_nfaaNcm1[10] = AE[2] - tr5*AO[2];

    AE[3] = qg_HA2L_LmppE_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[3] = qg_HA2L_LmppO_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcm1[3]  = AE[3] + tr5*AO[3];
    Aqg_2l_nfaaNcm1[11] = AE[3] - tr5*AO[3];

    AE[4] = qg_HA2L_LpmpE_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[4] = qg_HA2L_LpmpO_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcm1[4]  = AE[4] + tr5*AO[4];
    Aqg_2l_nfaaNcm1[12] = AE[4] - tr5*AO[4];

    AE[5] = qg_HA2L_LppmE_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[5] = qg_HA2L_LppmO_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcm1[5]  = AE[5] + tr5*AO[5];
    Aqg_2l_nfaaNcm1[13] = AE[5] - tr5*AO[5];

    AE[6] = qg_HA2L_LpppE_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[6] = qg_HA2L_LpppO_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcm1[6]  = AE[6] + tr5*AO[6];
    Aqg_2l_nfaaNcm1[14] = AE[6] - tr5*AO[6];

    AE[7] = qg_HA2L_LmmmE_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    AO[7] = qg_HA2L_LmmmO_nfaaNcm1(hrat2l_qgqaa,tf2lha_qgqaa,eps5t);
    Aqg_2l_nfaaNcm1[7]  = AE[7] + tr5*AO[7];
    Aqg_2l_nfaaNcm1[15] = AE[7] - tr5*AO[7];

    // qg channel - nfaa*nf contribution
    //-------------------------------------------------------------------------
    AE[0] = qg_HA2L_LmmpE_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    AO[0] = qg_HA2L_LmmpO_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    Aqg_2l_nfnfaa[0] = AE[0] + tr5*AO[0];
    Aqg_2l_nfnfaa[8] = AE[0] - tr5*AO[0];

    AE[1] = qg_HA2L_LmpmE_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    AO[1] = qg_HA2L_LmpmO_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    Aqg_2l_nfnfaa[1] = AE[1] + tr5*AO[1];
    Aqg_2l_nfnfaa[9] = AE[1] - tr5*AO[1];

    AE[2] = qg_HA2L_LpmmE_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    AO[2] = qg_HA2L_LpmmO_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    Aqg_2l_nfnfaa[2]  = AE[2] + tr5*AO[2];
    Aqg_2l_nfnfaa[10] = AE[2] - tr5*AO[2];

    AE[3] = qg_HA2L_LmppE_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    AO[3] = qg_HA2L_LmppO_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    Aqg_2l_nfnfaa[3]  = AE[3] + tr5*AO[3];
    Aqg_2l_nfnfaa[11] = AE[3] - tr5*AO[3];

    AE[4] = qg_HA2L_LpmpE_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    AO[4] = qg_HA2L_LpmpO_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    Aqg_2l_nfnfaa[4]  = AE[4] + tr5*AO[4];
    Aqg_2l_nfnfaa[12] = AE[4] - tr5*AO[4];

    AE[5] = qg_HA2L_LppmE_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    AO[5] = qg_HA2L_LppmO_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    Aqg_2l_nfnfaa[5]  = AE[5] + tr5*AO[5];
    Aqg_2l_nfnfaa[13] = AE[5] - tr5*AO[5];

    AE[6] = qg_HA2L_LpppE_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    AO[6] = qg_HA2L_LpppO_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    Aqg_2l_nfnfaa[6]  = AE[6] + tr5*AO[6];
    Aqg_2l_nfnfaa[14] = AE[6] - tr5*AO[6];

    AE[7] = qg_HA2L_LmmmE_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    AO[7] = qg_HA2L_LmmmO_nfnfaa(hrat2l_qgqaa,tf2lha_qgqaa);
    Aqg_2l_nfnfaa[7]  = AE[7] + tr5*AO[7];
    Aqg_2l_nfnfaa[15] = AE[7] - tr5*AO[7];
  }

}
#endif

// ----------------------------------------------------------------------------
// Interferences
// ----------------------------------------------------------------------------

// |tree|^2 interference
// ----------------------------------------------------------------------------
template <class T>
T Interference<T>::qqgaa_interf_W00() {
  return (-8*rat0l_qqgaa[0]);
}

template <class T>
T Interference<T>::qgqaa_interf_W00() {
  return (8*rat0l_qgqaa[0]);
}

// 2 Re[tree* x 1-loop] interferences
// ----------------------------------------------------------------------------

// CA colour structure
template <class T>
T Interference<T>::qqgaa_interf_W01_CA() {

  std::complex<T> res(0,0);

  res+= rat1l_qqgaa[8]*4;
  res+= rat1l_qqgaa[9]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  res+= rat1l_qqgaa[10]*qqb_l1_tf_CA_3(fw1);
  res+= rat1l_qqgaa[11]*qqb_l1_tf_CA_4(fw1);
  res+= rat1l_qqgaa[12]*qqb_l1_tf_CA_5(tcs,fw1);
  res+= rat1l_qqgaa[13]*qqb_l1_tf_CA_6(tcs,fw1,fw2);
  res+= rat1l_qqgaa[14]*qqb_l1_tf_CA_7(tcs,fw1,fw2);
  res+= rat1l_qqgaa[15]*qqb_l1_tf_CA_8(fw1);
  res+= rat1l_qqgaa[16]*qqb_l1_tf_CA_9(tcs,fw1,fw2);
  res+= rat1l_qqgaa[17]*qqb_l1_tf_CA_10(tcs,fw1,fw2);
  res+= rat1l_qqgaa[18]*qqb_l1_tf_CA_11(fw1);
  res+= rat1l_qqgaa[19]*qqb_l1_tf_CA_12(tcs,fw1,fw2);
  res+= rat1l_qqgaa[20]*qqb_l1_tf_CA_13(tcs,fw1,fw2);
  res+= rat1l_qqgaa[21]*qqb_l1_tf_CA_14(tcs,fw1,fw2);
  res+= rat1l_qqgaa[22]*qqb_l1_tf_CA_15(tcs,fw1);
  res+= rat1l_qqgaa[23]*qqb_l1_tf_CA_16(tcs,fw1,fw2);
  res+= rat1l_qqgaa[24]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  res+= rat1l_qqgaa[25]*qqb_l1_tf_CA_18(tcs,fw1,fw2);
  res+= rat1l_qqgaa[26]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  res+= rat1l_qqgaa[27]*qqb_l1_tf_CA_20(fw1);
  res+= rat1l_qqgaa[28]*qqb_l1_tf_CA_21(tcs,fw1,fw2);
  res+= rat1l_qqgaa[29]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  res+= rat1l_qqgaa[30]*qqb_l1_tf_CA_23(tcs,fw1,fw2);
  res+= rat1l_qqgaa[31]*qqb_l1_tf_CA_24(tcs,fw1,fw2);
  res+= rat1l_qqgaa[32]*qqb_l1_tf_CA_25(tcs,fw1,fw2);
  res+= rat1l_qqgaa[33]*qqb_l1_tf_CA_26(tcs,fw1,fw2);
  res+= rat1l_qqgaa[34]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  res+= rat1l_qqgaa[35]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  res+= rat1l_qqgaa[36]*qqb_l1_tf_CA_29(tcs,fw1,fw2);
  res+= rat1l_qqgaa[37]*qqb_l1_tf_CA_30(tcs,fw1,fw2);
  res+= rat1l_qqgaa[38]*qqb_l1_tf_CA_31(tcs,fw1,fw2);
  res+= rat1l_qqgaa[39]*qqb_l1_tf_CA_32(tcs,fw1,fw2);
  res+= rat1l_qqgaa[40]*qqb_l1_tf_CA_33(tcs,fw1,fw2);
  res+= rat1l_qqgaa[41]*qqb_l1_tf_CA_34(tcs,fw1,fw2);
  res+= rat1l_qqgaa[42]*qqb_l1_tf_CA_35(tcs,fw1);
  res /= 2;

  return res.real();
}

// CA colour structure
template <class T>
T Interference<T>::qgqaa_interf_W01_CA() {

  std::complex<T> res(0,0);

  res+= rat1l_qgqaa[8]*4;
  res+= rat1l_qgqaa[9]*qg_l1_tf_CA_2(fw1);
  res+= rat1l_qgqaa[10]*qg_l1_tf_CA_3(tcs,fw1,fw2);
  res+= rat1l_qgqaa[11]*qg_l1_tf_CA_4(tcs,fw1);
  res+= rat1l_qgqaa[12]*qg_l1_tf_CA_5(tcs,fw1,fw2);
  res+= rat1l_qgqaa[13]*qg_l1_tf_CA_6(fw1);
  res+= rat1l_qgqaa[14]*qg_l1_tf_CA_7(tcs,fw1,fw2);
  res+= rat1l_qgqaa[15]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  res+= rat1l_qgqaa[16]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  res+= rat1l_qgqaa[17]*qg_l1_tf_CA_10(fw1);
  res+= rat1l_qgqaa[18]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  res+= rat1l_qgqaa[19]*qg_l1_tf_CA_12(fw1);
  res+= rat1l_qgqaa[20]*qg_l1_tf_CA_13(tcs,fw1,fw2);
  res+= rat1l_qgqaa[21]*qg_l1_tf_CA_14(tcs,fw1,fw2);
  res+= rat1l_qgqaa[22]*qg_l1_tf_CA_15(tcs,fw1,fw2);
  res+= rat1l_qgqaa[23]*qg_l1_tf_CA_16(tcs,fw1,fw2);
  res+= rat1l_qgqaa[24]*qg_l1_tf_CA_17(tcs,fw1,fw2);
  res+= rat1l_qgqaa[25]*qg_l1_tf_CA_18(tcs,fw1);
  res+= rat1l_qgqaa[26]*qg_l1_tf_CA_19(tcs,fw1,fw2);
  res+= rat1l_qgqaa[27]*qg_l1_tf_CA_20(tcs,fw1,fw2);
  res+= rat1l_qgqaa[28]*qg_l1_tf_CA_21(tcs,fw1,fw2);
  res+= rat1l_qgqaa[29]*qg_l1_tf_CA_22(tcs,fw1,fw2);
  res+= rat1l_qgqaa[30]*qg_l1_tf_CA_23(tcs,fw1,fw2);
  res+= rat1l_qgqaa[31]*qg_l1_tf_CA_24(tcs,fw1,fw2);
  res+= rat1l_qgqaa[32]*qg_l1_tf_CA_25(tcs,fw1,fw2);
  res+= rat1l_qgqaa[33]*qg_l1_tf_CA_26(fw1);
  res+= rat1l_qgqaa[34]*qg_l1_tf_CA_27(tcs,fw1,fw2);
  res+= rat1l_qgqaa[35]*qg_l1_tf_CA_28(tcs,fw1,fw2);
  res+= rat1l_qgqaa[36]*qg_l1_tf_CA_29(tcs,fw1,fw2);
  res+= rat1l_qgqaa[37]*qg_l1_tf_CA_30(tcs,fw1,fw2);
  res+= rat1l_qgqaa[38]*qg_l1_tf_CA_31(tcs,fw1,fw2);
  res+= rat1l_qgqaa[39]*qg_l1_tf_CA_32(tcs,fw1,fw2);
  res+= rat1l_qgqaa[40]*qg_l1_tf_CA_33(tcs,fw1,fw2);
  res+= rat1l_qgqaa[41]*qg_l1_tf_CA_34(tcs,fw1,fw2);
  res+= rat1l_qgqaa[42]*qg_l1_tf_CA_35(tcs,fw1);
  res /= 2;

  return res.real();
}

// CF colour structure
template <class T>
T Interference<T>::qqgaa_interf_W01_CF() {

  std::complex<T> res(0,0);

  res+= rat1l_qqgaa[9]*qqb_l1_tf_CF_1(tcs,fw1,fw2);
  res+= rat1l_qqgaa[12]*qqb_l1_tf_CF_2(tcs,fw1);
  res+= rat1l_qqgaa[13]*qqb_l1_tf_CF_3(tcs,fw1,fw2);
  res+= rat1l_qqgaa[16]*qqb_l1_tf_CF_4(tcs,fw1,fw2);
  res+= rat1l_qqgaa[19]*qqb_l1_tf_CF_5(tcs,fw1,fw2);
  res+= rat1l_qqgaa[20]*qqb_l1_tf_CF_6(tcs,fw1,fw2);
  res+= rat1l_qqgaa[21]*qqb_l1_tf_CF_7(tcs,fw1,fw2);
  res+= rat1l_qqgaa[23]*qqb_l1_tf_CF_8(tcs,fw1,fw2);
  res+= rat1l_qqgaa[25]*qqb_l1_tf_CF_9(tcs,fw1,fw2);
  res+= rat1l_qqgaa[28]*qqb_l1_tf_CF_10(tcs,fw1,fw2);
  res+= rat1l_qqgaa[29]*qqb_l1_tf_CF_11(tcs,fw1,fw2);
  res+= rat1l_qqgaa[30]*qqb_l1_tf_CF_12(tcs,fw1,fw2);
  res+= rat1l_qqgaa[31]*qqb_l1_tf_CF_13(tcs,fw1,fw2);
  res+= rat1l_qqgaa[32]*qqb_l1_tf_CF_14(tcs,fw1,fw2);
  res+= rat1l_qqgaa[33]*qqb_l1_tf_CF_15(tcs,fw1,fw2);
  res+= rat1l_qqgaa[34]*qqb_l1_tf_CF_16(tcs,fw1,fw2);
  res+= rat1l_qqgaa[35]*qqb_l1_tf_CF_17(tcs,fw1,fw2);
  res+= rat1l_qqgaa[36]*qqb_l1_tf_CF_18(tcs,fw1,fw2);
  res+= rat1l_qqgaa[37]*qqb_l1_tf_CF_19(tcs,fw1,fw2);
  res+= rat1l_qqgaa[38]*qqb_l1_tf_CF_20(tcs,fw1,fw2);
  res+= rat1l_qqgaa[39]*qqb_l1_tf_CF_21(tcs,fw1,fw2);
  res+= rat1l_qqgaa[40]*qqb_l1_tf_CF_22(tcs,fw1,fw2);
  res+= rat1l_qqgaa[41]*qqb_l1_tf_CF_23(tcs,fw1,fw2);
  res+= rat1l_qqgaa[42]*qqb_l1_tf_CF_24(tcs,fw1);
  res+= rat1l_qqgaa[43]*4;
  res+= rat1l_qqgaa[44]*qqb_l1_tf_CF_26(fw1);
  res+= rat1l_qqgaa[45]*qqb_l1_tf_CF_27(fw1);
  res+= rat1l_qqgaa[46]*qqb_l1_tf_CF_28(fw1);
  res+= rat1l_qqgaa[47]*qqb_l1_tf_CF_29(fw1);
  res+= rat1l_qqgaa[48]*qqb_l1_tf_CF_30(tcs,fw1,fw2);
  res+= rat1l_qqgaa[49]*qqb_l1_tf_CF_31(tcs,fw1,fw2);
  res+= rat1l_qqgaa[50]*qqb_l1_tf_CF_32(tcs,fw1);
  res+= rat1l_qqgaa[51]*qqb_l1_tf_CF_33(tcs,fw1);
  res+= rat1l_qqgaa[52]*qqb_l1_tf_CF_34(tcs,fw1,fw2);
  res+= rat1l_qqgaa[53]*qqb_l1_tf_CF_35(tcs,fw1,fw2);
  res+= rat1l_qqgaa[54]*qqb_l1_tf_CF_36(fw1);
  res /= 2;

  return res.real();
}

// CF colour structure
template <class T>
T Interference<T>::qgqaa_interf_W01_CF() {

  std::complex<T> res(0,0);

  res+= rat1l_qgqaa[9]*qg_l1_tf_CF_1(fw1);
  res+= rat1l_qgqaa[10]*qg_l1_tf_CF_2(tcs,fw1,fw2);
  res+= rat1l_qgqaa[12]*qg_l1_tf_CF_3(tcs,fw1,fw2);
  res+= rat1l_qgqaa[14]*qg_l1_tf_CF_4(tcs,fw1,fw2);
  res+= rat1l_qgqaa[15]*qg_l1_tf_CF_5(tcs,fw1,fw2);
  res+= rat1l_qgqaa[16]*qg_l1_tf_CF_6(tcs,fw1,fw2);
  res+= rat1l_qgqaa[17]*qg_l1_tf_CF_7(fw1);
  res+= rat1l_qgqaa[18]*qg_l1_tf_CF_8(tcs,fw1,fw2);
  res+= rat1l_qgqaa[20]*qg_l1_tf_CF_9(tcs,fw1,fw2);
  res+= rat1l_qgqaa[21]*qg_l1_tf_CF_10(tcs,fw1,fw2);
  res+= rat1l_qgqaa[22]*qg_l1_tf_CF_11(tcs,fw1,fw2);
  res+= rat1l_qgqaa[23]*qg_l1_tf_CF_12(tcs,fw1,fw2);
  res+= rat1l_qgqaa[24]*qg_l1_tf_CF_13(tcs,fw1,fw2);
  res+= rat1l_qgqaa[26]*qg_l1_tf_CF_14(tcs,fw1,fw2);
  res+= rat1l_qgqaa[27]*qg_l1_tf_CF_15(tcs,fw1,fw2);
  res+= rat1l_qgqaa[28]*qg_l1_tf_CF_16(tcs,fw1,fw2);
  res+= rat1l_qgqaa[29]*qg_l1_tf_CF_17(tcs,fw1,fw2);
  res+= rat1l_qgqaa[30]*qg_l1_tf_CF_18(tcs,fw1,fw2);
  res+= rat1l_qgqaa[34]*qg_l1_tf_CF_19(tcs,fw1,fw2);
  res+= rat1l_qgqaa[35]*qg_l1_tf_CF_20(tcs,fw1,fw2);
  res+= rat1l_qgqaa[36]*qg_l1_tf_CF_21(tcs,fw1,fw2);
  res+= rat1l_qgqaa[37]*qg_l1_tf_CF_22(tcs,fw1,fw2);
  res+= rat1l_qgqaa[39]*qg_l1_tf_CF_23(tcs,fw1,fw2);
  res+= rat1l_qgqaa[41]*qg_l1_tf_CF_24(tcs,fw1,fw2);
  res+= rat1l_qgqaa[43]*4;
  res+= rat1l_qgqaa[44]*qg_l1_tf_CF_26(tcs,fw1);
  res+= rat1l_qgqaa[45]*qg_l1_tf_CF_27(fw1);
  res+= rat1l_qgqaa[46]*qg_l1_tf_CF_28(fw1);
  res+= rat1l_qgqaa[47]*qg_l1_tf_CF_29(tcs,fw1);
  res+= rat1l_qgqaa[48]*qg_l1_tf_CF_30(tcs,fw1);
  res+= rat1l_qgqaa[49]*qg_l1_tf_CF_31(tcs,fw1,fw2);
  res+= rat1l_qgqaa[50]*qg_l1_tf_CF_32(tcs,fw1,fw2);
  res+= rat1l_qgqaa[51]*qg_l1_tf_CF_33(fw1);
  res+= rat1l_qgqaa[52]*qg_l1_tf_CF_34(tcs,fw1,fw2);
  res+= rat1l_qgqaa[53]*qg_l1_tf_CF_35(tcs,fw1,fw2);
  res+= rat1l_qgqaa[54]*qg_l1_tf_CF_36(tcs,fw1);
  res /= 2;

  return res.real();
}


// nfaa colour structure
template <class T>
T Interference<T>::qqgaa_interf_W01_nfaa() {

  std::complex<T> res(0,0);

  res += rat1l_qqgaa[1]*16;
  res += rat1l_qqgaa[2]*qqb_l1_tf_nfaa_2(tcs,fw1,fw2);
  res += rat1l_qqgaa[3]*qqb_l1_tf_nfaa_3(tcs,fw1,fw2);
  res += rat1l_qqgaa[4]*qqb_l1_tf_nfaa_4(tcs,fw1,fw2);
  res += rat1l_qqgaa[5]*qqb_l1_tf_nfaa_5(fw1);
  res += rat1l_qqgaa[6]*qqb_l1_tf_nfaa_6(fw1);
  res += rat1l_qqgaa[7]*qqb_l1_tf_nfaa_7(fw1);
  res /= 2;

  // Tr
  res /= 2;

  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W01_nfaa() {

  std::complex<T> res(0,0);

  res += rat1l_qgqaa[1]*16;
  res += rat1l_qgqaa[2]*qg_l1_tf_nfaa_2(tcs,fw1,fw2);
  res += rat1l_qgqaa[3]*qg_l1_tf_nfaa_3(fw1);
  res += rat1l_qgqaa[4]*qg_l1_tf_nfaa_4(fw1);
  res += rat1l_qgqaa[5]*qg_l1_tf_nfaa_5(tcs,fw1,fw2);
  res += rat1l_qgqaa[6]*qg_l1_tf_nfaa_6(tcs,fw1,fw2);
  res += rat1l_qgqaa[7]*qg_l1_tf_nfaa_7(tcs,fw1);
  res /= 2;

  // Tr
  res /= 2;

  return res.real();
}

// nf colour structure
template <class T>
T Interference<T>::qqgaa_interf_W01_nf() {
  return (-2*rat0l_qqgaa[0]*(fw1[1] + fw1[5]).real()/3);
}

template <class T>
T Interference<T>::qgqaa_interf_W01_nf() {
  const std::complex<T> i(0,1);
  return (2*rat0l_qgqaa[0]*(-i*tcs[0] + fw1[0] + fw1[1]).real()/3);
}

// [1-loop* x 1-loop] interference: qqb
//---------------------------------------------------------------------------
template <class T>
T Interference<T>::qqgaa_interf_W11_CACA() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qqb[i]*(npow(Aqqb_1l_CA[i].real(),2) + npow(Aqqb_1l_CA[i].imag(),2));
      res += SpPh_qqb[i]*(npow(Aqqb_1l_CA[i+8].real(),2) + npow(Aqqb_1l_CA[i+8].imag(),2));
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qqgaa_interf_W11_CFCF() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qqb[i]*(npow(Aqqb_1l_CF[i].real(),2) + npow(Aqqb_1l_CF[i].imag(),2));
      res += SpPh_qqb[i]*(npow(Aqqb_1l_CF[i+8].real(),2) + npow(Aqqb_1l_CF[i+8].imag(),2));
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qqgaa_interf_W11_nfaanfaa() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qqb[i]*(npow(Aqqb_1l_nfaa[i].real(),2) + npow(Aqqb_1l_nfaa[i].imag(),2));
      res += SpPh_qqb[i]*(npow(Aqqb_1l_nfaa[i+8].real(),2) + npow(Aqqb_1l_nfaa[i+8].imag(),2));
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qqgaa_interf_W11_nfnf() {
  T res=0;
  res = (-rat0l_qqgaa[0]*(fw1[1]*fw1[1] + 2*fw1[1]*fw1[5] + fw1[5]*fw1[5]).real()/9);
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qqgaa_interf_W11_CACF() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += 2*SpPh_qqb[i]*(Aqqb_1l_CA[i].real()*Aqqb_1l_CF[i].real()+Aqqb_1l_CA[i].imag()*Aqqb_1l_CF[i].imag());
      res += 2*SpPh_qqb[i]*(Aqqb_1l_CA[i+8].real()*Aqqb_1l_CF[i+8].real()+Aqqb_1l_CA[i+8].imag()*Aqqb_1l_CF[i+8].imag());
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qqgaa_interf_W11_nfaaCA() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += 2*SpPh_qqb[i]*(Aqqb_1l_CA[i].real()*Aqqb_1l_nfaa[i].real()+Aqqb_1l_CA[i].imag()*Aqqb_1l_nfaa[i].imag());
      res += 2*SpPh_qqb[i]*(Aqqb_1l_CA[i+8].real()*Aqqb_1l_nfaa[i+8].real()+Aqqb_1l_CA[i+8].imag()*Aqqb_1l_nfaa[i+8].imag());
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qqgaa_interf_W11_nfCA() {

  T res=0;

  for (size_t i = 0; i < 6; ++i) {
      res += SpPh_qqb[i]*Aqqb_1l_CA[i].real();
      res += SpPh_qqb[i]*Aqqb_1l_CA[i+8].real();
  }
  res*=2*((fw1[1]+fw1[5])/12).real();
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qqgaa_interf_W11_nfaaCF() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += 2*SpPh_qqb[i]*(Aqqb_1l_CF[i].real()*Aqqb_1l_nfaa[i].real()+Aqqb_1l_CF[i].imag()*Aqqb_1l_nfaa[i].imag());
      res += 2*SpPh_qqb[i]*(Aqqb_1l_CF[i+8].real()*Aqqb_1l_nfaa[i+8].real()+Aqqb_1l_CF[i+8].imag()*Aqqb_1l_nfaa[i+8].imag());
  }
  res /= 2;
  return res;
}


template <class T>
T Interference<T>::qqgaa_interf_W11_nfCF() {

  T res=0;

  for (size_t i = 0; i < 6; ++i) {
      res += SpPh_qqb[i]*Aqqb_1l_CF[i].real();
      res += SpPh_qqb[i]*Aqqb_1l_CF[i+8].real();
  }
  res*=2*((fw1[1]+fw1[5])/12).real();
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qqgaa_interf_W11_nfnfaa() {

  T res=0;

  for (size_t i = 0; i < 6; ++i) {
      res += SpPh_qqb[i]*Aqqb_1l_nfaa[i].real();
      res += SpPh_qqb[i]*Aqqb_1l_nfaa[i+8].real();
  }
  res*=2*((fw1[1]+fw1[5])/12).real();
  res /= 2;
  return res;
}

// [1-loop* x 1-loop] interference: qg
//---------------------------------------------------------------------------
template <class T>
T Interference<T>::qgqaa_interf_W11_CACA() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qg[i]*(npow(Aqg_1l_CA[i].real(),2) + npow(Aqg_1l_CA[i].imag(),2));
      res += SpPh_qg[i]*(npow(Aqg_1l_CA[i+8].real(),2) + npow(Aqg_1l_CA[i+8].imag(),2));
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qgqaa_interf_W11_CFCF() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qg[i]*(npow(Aqg_1l_CF[i].real(),2) + npow(Aqg_1l_CF[i].imag(),2));
      res += SpPh_qg[i]*(npow(Aqg_1l_CF[i+8].real(),2) + npow(Aqg_1l_CF[i+8].imag(),2));
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qgqaa_interf_W11_nfaanfaa() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qg[i]*(npow(Aqg_1l_nfaa[i].real(),2) + npow(Aqg_1l_nfaa[i].imag(),2));
      res += SpPh_qg[i]*(npow(Aqg_1l_nfaa[i+8].real(),2) + npow(Aqg_1l_nfaa[i+8].imag(),2));
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qgqaa_interf_W11_nfnf() {
  T res=0;
  res = (fw1[0]*fw1[0] + 2*fw1[0]*fw1[1] + fw1[1]*fw1[1] + tcs[3]).real();
  res *= rat0l_qgqaa[0]/9;
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qgqaa_interf_W11_CACF() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += 2*SpPh_qg[i]*(Aqg_1l_CA[i].real()*Aqg_1l_CF[i].real()+Aqg_1l_CA[i].imag()*Aqg_1l_CF[i].imag());
      res += 2*SpPh_qg[i]*(Aqg_1l_CA[i+8].real()*Aqg_1l_CF[i+8].real()+Aqg_1l_CA[i+8].imag()*Aqg_1l_CF[i+8].imag());
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qgqaa_interf_W11_nfaaCA() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += 2*SpPh_qg[i]*(Aqg_1l_CA[i].real()*Aqg_1l_nfaa[i].real()+Aqg_1l_CA[i].imag()*Aqg_1l_nfaa[i].imag());
      res += 2*SpPh_qg[i]*(Aqg_1l_CA[i+8].real()*Aqg_1l_nfaa[i+8].real()+Aqg_1l_CA[i+8].imag()*Aqg_1l_nfaa[i+8].imag());
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qgqaa_interf_W11_nfCA() {

  T res=0;
  T re,im;
  re = (fw1[0]+fw1[1]).real()/12;
  im = -tcs[0]/12;

  for (size_t i = 0; i < 6; ++i) {
      res += 2*SpPh_qg[i]*(Aqg_1l_CA[i].real()*re + Aqg_1l_CA[i].imag()*im);
      res += 2*SpPh_qg[i]*(Aqg_1l_CA[i+8].real()*re + Aqg_1l_CA[i+8].imag()*im);
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qgqaa_interf_W11_nfaaCF() {

  T res=0;

  for (size_t i = 0; i < 8; ++i) {
      res += 2*SpPh_qg[i]*(Aqg_1l_CF[i].real()*Aqg_1l_nfaa[i].real()+Aqg_1l_CF[i].imag()*Aqg_1l_nfaa[i].imag());
      res += 2*SpPh_qg[i]*(Aqg_1l_CF[i+8].real()*Aqg_1l_nfaa[i+8].real()+Aqg_1l_CF[i+8].imag()*Aqg_1l_nfaa[i+8].imag());
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qgqaa_interf_W11_nfCF() {

  T res=0;
  T re,im;
  re = (fw1[0]+fw1[1]).real()/12;
  im = -tcs[0]/12;

  for (size_t i = 0; i < 6; ++i) {
      res += 2*SpPh_qg[i]*(Aqg_1l_CF[i].real()*re + Aqg_1l_CF[i].imag()*im);
      res += 2*SpPh_qg[i]*(Aqg_1l_CF[i+8].real()*re + Aqg_1l_CF[i+8].imag()*im);
  }
  res /= 2;
  return res;
}

template <class T>
T Interference<T>::qgqaa_interf_W11_nfnfaa() {

  T res=0;
  T re,im;
  re = (fw1[0]+fw1[1]).real()/12;
  im = -tcs[0]/12;

  for (size_t i = 0; i < 6; ++i) {
      res += 2*SpPh_qg[i]*(Aqg_1l_nfaa[i].real()*re + Aqg_1l_nfaa[i].imag()*im);
      res += 2*SpPh_qg[i]*(Aqg_1l_nfaa[i+8].real()*re + Aqg_1l_nfaa[i+8].imag()*im);
  }
  res /= 2;
  return res;
}

// 2 Re[tree* x 2-loop] interference.
//---------------------------------------------------------------------------

// LC colour structure
template <class T>
T Interference<T>::qqgaa_interf_W02_Nc2() {

  std::complex<T> res(0,0);

  res+= rat2lLC_qqgaa[13] *tf2lLC_qqgaa[1];
  res+= rat2lLC_qqgaa[15] *tf2lLC_qqgaa[2];
  res+= rat2lLC_qqgaa[22] *tf2lLC_qqgaa[3];
  res+= rat2lLC_qqgaa[24] *tf2lLC_qqgaa[4];
  res+= rat2lLC_qqgaa[29] *tf2lLC_qqgaa[5];
  res+= rat2lLC_qqgaa[32] *tf2lLC_qqgaa[6];
  res+= rat2lLC_qqgaa[33] *tf2lLC_qqgaa[7];
  res+= rat2lLC_qqgaa[34] *tf2lLC_qqgaa[8];
  res+= rat2lLC_qqgaa[35] *tf2lLC_qqgaa[9];
  res+= rat2lLC_qqgaa[36] *tf2lLC_qqgaa[10];
  res+= rat2lLC_qqgaa[40] *tf2lLC_qqgaa[11];
  res+= rat2lLC_qqgaa[115]*tf2lLC_qqgaa[12];
  res+= rat2lLC_qqgaa[116]*tf2lLC_qqgaa[13]*eps5;
  res+= rat2lLC_qqgaa[117]*tf2lLC_qqgaa[14]*eps5;
  res+= rat2lLC_qqgaa[118]*tf2lLC_qqgaa[15];
  res+= rat2lLC_qqgaa[119]*tf2lLC_qqgaa[16];
  res+= rat2lLC_qqgaa[120]*tf2lLC_qqgaa[17];
  res+= rat2lLC_qqgaa[121]*tf2lLC_qqgaa[18];
  res+= rat2lLC_qqgaa[122]*tf2lLC_qqgaa[19];
  res+= rat2lLC_qqgaa[123]*tf2lLC_qqgaa[20];
  res+= rat2lLC_qqgaa[124]*tf2lLC_qqgaa[21];
  res+= rat2lLC_qqgaa[125]*tf2lLC_qqgaa[22];
  res+= rat2lLC_qqgaa[126]*tf2lLC_qqgaa[23];
  res+= rat2lLC_qqgaa[127]*tf2lLC_qqgaa[24];
  res+= rat2lLC_qqgaa[128]*tf2lLC_qqgaa[25];
  res+= rat2lLC_qqgaa[129]*tf2lLC_qqgaa[26];
  res+= rat2lLC_qqgaa[130]*tf2lLC_qqgaa[27];
  res+= rat2lLC_qqgaa[131]*tf2lLC_qqgaa[28]*eps5;
  res+= rat2lLC_qqgaa[132]*tf2lLC_qqgaa[29];
  res+= rat2lLC_qqgaa[133]*tf2lLC_qqgaa[30];
  res+= rat2lLC_qqgaa[134]*tf2lLC_qqgaa[31];
  res+= rat2lLC_qqgaa[135]*tf2lLC_qqgaa[32];
  res+= rat2lLC_qqgaa[136]*tf2lLC_qqgaa[33];
  res+= rat2lLC_qqgaa[137]*tf2lLC_qqgaa[34];
  res+= rat2lLC_qqgaa[138]*tf2lLC_qqgaa[35];
  res+= rat2lLC_qqgaa[139]*tf2lLC_qqgaa[36];
  res+= rat2lLC_qqgaa[140]*tf2lLC_qqgaa[37];
  res+= rat2lLC_qqgaa[141]*tf2lLC_qqgaa[38];
  res+= rat2lLC_qqgaa[142]*tf2lLC_qqgaa[39];
  res+= rat2lLC_qqgaa[143]*tf2lLC_qqgaa[40];
  res+= rat2lLC_qqgaa[144]*tf2lLC_qqgaa[41];
  res+= rat2lLC_qqgaa[145]*tf2lLC_qqgaa[42]*eps5;
  res+= rat2lLC_qqgaa[146]*tf2lLC_qqgaa[43]*eps5;
  res+= rat2lLC_qqgaa[147]*tf2lLC_qqgaa[44]*eps5;
  res+= rat2lLC_qqgaa[148]*tf2lLC_qqgaa[45]*eps5;
  res+= rat2lLC_qqgaa[149]*tf2lLC_qqgaa[46]*eps5;
  res+= rat2lLC_qqgaa[150]*tf2lLC_qqgaa[47]*eps5;
  res+= rat2lLC_qqgaa[151]*tf2lLC_qqgaa[48]*eps5;
  res+= rat2lLC_qqgaa[152]*tf2lLC_qqgaa[49]*eps5;
  res+= rat2lLC_qqgaa[153]*tf2lLC_qqgaa[50];
  res+= rat2lLC_qqgaa[154]*tf2lLC_qqgaa[51];
  res+= rat2lLC_qqgaa[155]*tf2lLC_qqgaa[52];
  res+= rat2lLC_qqgaa[156]*tf2lLC_qqgaa[53];
  res+= rat2lLC_qqgaa[157]*tf2lLC_qqgaa[54];
  res+= rat2lLC_qqgaa[158]*tf2lLC_qqgaa[55];
  res+= rat2lLC_qqgaa[159]*tf2lLC_qqgaa[56];
  res+= rat2lLC_qqgaa[160]*tf2lLC_qqgaa[57];
  res+= rat2lLC_qqgaa[161]*tf2lLC_qqgaa[58];
  res+= rat2lLC_qqgaa[162]*tf2lLC_qqgaa[59];
  res+= rat2lLC_qqgaa[163]*tf2lLC_qqgaa[60];
  res+= rat2lLC_qqgaa[164]*tf2lLC_qqgaa[61];
  res+= rat2lLC_qqgaa[165]*tf2lLC_qqgaa[62];
  res+= rat2lLC_qqgaa[166]*tf2lLC_qqgaa[63];
  res+= rat2lLC_qqgaa[167]*tf2lLC_qqgaa[64];
  res+= rat2lLC_qqgaa[168]*tf2lLC_qqgaa[65];
  res+= rat2lLC_qqgaa[169]*tf2lLC_qqgaa[66];
  res+= rat2lLC_qqgaa[170]*tf2lLC_qqgaa[67];
  res+= rat2lLC_qqgaa[171]*tf2lLC_qqgaa[68];
  res+= rat2lLC_qqgaa[172]*tf2lLC_qqgaa[69];
  res+= rat2lLC_qqgaa[173]*tf2lLC_qqgaa[70];
  res+= rat2lLC_qqgaa[174]*tf2lLC_qqgaa[71];
  res+= rat2lLC_qqgaa[175]*tf2lLC_qqgaa[72];
  res+= rat2lLC_qqgaa[176]*tf2lLC_qqgaa[73];
  res+= rat2lLC_qqgaa[177]*tf2lLC_qqgaa[74];
  res+= rat2lLC_qqgaa[178]*tf2lLC_qqgaa[75];
  res+= rat2lLC_qqgaa[179]*tf2lLC_qqgaa[76];
  res+= rat2lLC_qqgaa[180]*tf2lLC_qqgaa[77];
  res+= rat2lLC_qqgaa[181]*tf2lLC_qqgaa[78];
  res+= rat2lLC_qqgaa[182]*tf2lLC_qqgaa[79];
  res+= rat2lLC_qqgaa[183]*tf2lLC_qqgaa[80];
  res+= rat2lLC_qqgaa[184]*tf2lLC_qqgaa[81];
  res+= rat2lLC_qqgaa[185]*tf2lLC_qqgaa[82];
  res+= rat2lLC_qqgaa[186]*tf2lLC_qqgaa[83];
  res+= rat2lLC_qqgaa[187]*tf2lLC_qqgaa[84];
  res+= rat2lLC_qqgaa[188]*tf2lLC_qqgaa[85];
  res+= rat2lLC_qqgaa[189]*tf2lLC_qqgaa[86];
  res+= rat2lLC_qqgaa[190]*tf2lLC_qqgaa[87];
  res+= rat2lLC_qqgaa[191]*tf2lLC_qqgaa[88];
  res+= rat2lLC_qqgaa[192]*tf2lLC_qqgaa[89];
  res+= rat2lLC_qqgaa[193]*tf2lLC_qqgaa[90];
  res+= rat2lLC_qqgaa[194]*tf2lLC_qqgaa[91];
  res+= rat2lLC_qqgaa[195]*tf2lLC_qqgaa[92];
  res+= rat2lLC_qqgaa[196]*tf2lLC_qqgaa[93];
  res+= rat2lLC_qqgaa[197]*tf2lLC_qqgaa[94];
  res+= rat2lLC_qqgaa[198]*tf2lLC_qqgaa[95];
  res+= rat2lLC_qqgaa[199]*tf2lLC_qqgaa[96];
  res+= rat2lLC_qqgaa[200]*tf2lLC_qqgaa[97];
  res+= rat2lLC_qqgaa[201]*tf2lLC_qqgaa[98];
  res+= rat2lLC_qqgaa[202]*tf2lLC_qqgaa[99];
  res+= rat2lLC_qqgaa[203]*tf2lLC_qqgaa[100];
  res+= rat2lLC_qqgaa[204]*tf2lLC_qqgaa[101];
  res+= rat2lLC_qqgaa[205]*tf2lLC_qqgaa[102];
  res+= rat2lLC_qqgaa[206]*tf2lLC_qqgaa[103];
  res+= rat2lLC_qqgaa[207]*tf2lLC_qqgaa[104];
  res+= rat2lLC_qqgaa[208]*tf2lLC_qqgaa[105];
  res+= rat2lLC_qqgaa[209]*tf2lLC_qqgaa[106];
  res+= rat2lLC_qqgaa[210]*tf2lLC_qqgaa[107];
  res+= rat2lLC_qqgaa[211]*tf2lLC_qqgaa[108];
  res+= rat2lLC_qqgaa[212]*tf2lLC_qqgaa[109];
  res+= rat2lLC_qqgaa[213]*tf2lLC_qqgaa[110];
  res+= rat2lLC_qqgaa[214]*tf2lLC_qqgaa[111];
  res+= rat2lLC_qqgaa[215]*tf2lLC_qqgaa[112];
  res+= rat2lLC_qqgaa[216]*tf2lLC_qqgaa[113];
  res+= rat2lLC_qqgaa[217]*tf2lLC_qqgaa[114];
  res+= rat2lLC_qqgaa[218]*tf2lLC_qqgaa[115];
  res+= rat2lLC_qqgaa[219]*tf2lLC_qqgaa[116];
  res+= rat2lLC_qqgaa[220]*tf2lLC_qqgaa[117];
  res+= rat2lLC_qqgaa[221]/648;
  res /= 2;

  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W02_Nc2() {

  std::complex<T> res(0,0);

  res+= rat2lLC_qgqaa[17]*qg_l2_tf_LC_1(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[28]*qg_l2_tf_LC_2(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[29]*qg_l2_tf_LC_3(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[30]*qg_l2_tf_LC_4(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[31]*qg_l2_tf_LC_5(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[32]*qg_l2_tf_LC_6(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[36]*qg_l2_tf_LC_7(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[37]*qg_l2_tf_LC_8(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[38]*qg_l2_tf_LC_9(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[39]*qg_l2_tf_LC_10(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[116]*qg_l2_tf_LC_11(tcs);
  res+= rat2lLC_qgqaa[117]*(qg_l2_tf_LC_12(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[118]*(qg_l2_tf_LC_13(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[119]*qg_l2_tf_LC_14(tcs,fw3);
  res+= rat2lLC_qgqaa[120]*qg_l2_tf_LC_15(fw3);
  res+= rat2lLC_qgqaa[121]*qg_l2_tf_LC_16(tcs,fw3);
  res+= rat2lLC_qgqaa[122]*qg_l2_tf_LC_17(fw3);
  res+= rat2lLC_qgqaa[123]*qg_l2_tf_LC_18(fw2);
  res+= rat2lLC_qgqaa[124]*qg_l2_tf_LC_19(fw2);
  res+= rat2lLC_qgqaa[125]*qg_l2_tf_LC_20(fw2);
  res+= rat2lLC_qgqaa[126]*qg_l2_tf_LC_21(fw2);
  res+= rat2lLC_qgqaa[127]*qg_l2_tf_LC_22(fw2);
  res+= rat2lLC_qgqaa[128]*qg_l2_tf_LC_23(fw1);
  res+= rat2lLC_qgqaa[129]*qg_l2_tf_LC_24(tcs,fw1);
  res+= rat2lLC_qgqaa[130]*qg_l2_tf_LC_25(fw1);
  res+= rat2lLC_qgqaa[131]*qg_l2_tf_LC_26(tcs,fw1);
  res+= rat2lLC_qgqaa[132]*qg_l2_tf_LC_27(tcs,fw1);
  res+= rat2lLC_qgqaa[133]*qg_l2_tf_LC_28(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[134]*qg_l2_tf_LC_29(fw1);
  res+= rat2lLC_qgqaa[135]*(qg_l2_tf_LC_30(tcs,fw1,fw3,fw4)*eps5);
  res+= rat2lLC_qgqaa[136]*(qg_l2_tf_LC_31(tcs,fw1,fw3,fw4)*eps5);
  res+= rat2lLC_qgqaa[137]*(qg_l2_tf_LC_32(tcs,fw1,fw3,fw4)*eps5);
  res+= rat2lLC_qgqaa[138]*(qg_l2_tf_LC_33(tcs,fw1,fw3,fw4)*eps5);
  res+= rat2lLC_qgqaa[139]*(qg_l2_tf_LC_34(tcs,fw1,fw3,fw4)*eps5);
  res+= rat2lLC_qgqaa[140]*(qg_l2_tf_LC_35(tcs,fw1,fw3,fw4)*eps5);
  res+= rat2lLC_qgqaa[141]*(qg_l2_tf_LC_36(tcs,fw1,fw3,fw4)*eps5);
  res+= rat2lLC_qgqaa[142]*(qg_l2_tf_LC_37(tcs,fw1,fw3,fw4)*eps5);
  res+= rat2lLC_qgqaa[143]*(qg_l2_tf_LC_38(tcs,fw1,fw3,fw4)*eps5);
  res+= rat2lLC_qgqaa[144]*qg_l2_tf_LC_39(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[145]*qg_l2_tf_LC_40(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[146]*qg_l2_tf_LC_41(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[147]*qg_l2_tf_LC_42(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[148]*qg_l2_tf_LC_43(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[149]*qg_l2_tf_LC_44(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[150]*qg_l2_tf_LC_45(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[151]*qg_l2_tf_LC_46(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[152]*qg_l2_tf_LC_47(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[153]*qg_l2_tf_LC_48(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[154]*qg_l2_tf_LC_49(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[155]*qg_l2_tf_LC_50(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[156]*qg_l2_tf_LC_51(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[157]*qg_l2_tf_LC_52(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[158]*qg_l2_tf_LC_53(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[159]*qg_l2_tf_LC_54(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[160]*qg_l2_tf_LC_55(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[161]*qg_l2_tf_LC_56(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[162]*qg_l2_tf_LC_57(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[163]*qg_l2_tf_LC_58(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[164]*qg_l2_tf_LC_59(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[165]*qg_l2_tf_LC_60(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[166]*qg_l2_tf_LC_61(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[167]*qg_l2_tf_LC_62(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[168]*qg_l2_tf_LC_63(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[169]*qg_l2_tf_LC_64(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[170]*qg_l2_tf_LC_65(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[171]*qg_l2_tf_LC_66(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[172]*qg_l2_tf_LC_67(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[173]*qg_l2_tf_LC_68(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[174]*qg_l2_tf_LC_69(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[175]*qg_l2_tf_LC_70(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[176]*qg_l2_tf_LC_71(tcs,fw1);
  res+= rat2lLC_qgqaa[177]*qg_l2_tf_LC_72(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[178]*qg_l2_tf_LC_73(fw1);
  res+= rat2lLC_qgqaa[179]*qg_l2_tf_LC_74(fw1);
  res+= rat2lLC_qgqaa[180]*qg_l2_tf_LC_75(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[181]*qg_l2_tf_LC_76(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[182]*qg_l2_tf_LC_77(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[183]*qg_l2_tf_LC_78(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[184]*qg_l2_tf_LC_79(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[185]*qg_l2_tf_LC_80(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[186]*qg_l2_tf_LC_81(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[187]*qg_l2_tf_LC_82(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[188]*qg_l2_tf_LC_83(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[189]*qg_l2_tf_LC_84(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[190]*qg_l2_tf_LC_85(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[191]*qg_l2_tf_LC_86(fw1);
  res+= rat2lLC_qgqaa[192]*qg_l2_tf_LC_87(fw1);
  res+= rat2lLC_qgqaa[193]*qg_l2_tf_LC_88(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[194]*qg_l2_tf_LC_89(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[195]*qg_l2_tf_LC_90(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[196]*qg_l2_tf_LC_91(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[197]*qg_l2_tf_LC_92(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[198]*qg_l2_tf_LC_93(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[199]*qg_l2_tf_LC_94(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[200]*qg_l2_tf_LC_95(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[201]*qg_l2_tf_LC_96(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[202]*qg_l2_tf_LC_97(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[203]*qg_l2_tf_LC_98(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[204]*qg_l2_tf_LC_99(fw1);
  res+= rat2lLC_qgqaa[205]*qg_l2_tf_LC_100(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[206]*qg_l2_tf_LC_101(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[207]*qg_l2_tf_LC_102(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[208]*qg_l2_tf_LC_103(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[209]*qg_l2_tf_LC_104(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[210]*qg_l2_tf_LC_105(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[211]*qg_l2_tf_LC_106(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[212]*qg_l2_tf_LC_107(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[213]*qg_l2_tf_LC_108(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[214]*qg_l2_tf_LC_109(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[215]*qg_l2_tf_LC_110(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[216]*qg_l2_tf_LC_111(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[217]*qg_l2_tf_LC_112(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[218]*qg_l2_tf_LC_113(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[219]*qg_l2_tf_LC_114(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[220]*qg_l2_tf_LC_115(tcs,fw1,fw2,fw3,fw4);
  res+= rat2lLC_qgqaa[221]*qg_l2_tf_LC_116(tcs,fw1);
  res+= -rat2lLC_qgqaa[222]/648;
  res /= 2;

  return res.real();
}


// nf*CA colour structure
template <class T>
T Interference<T>::qqgaa_interf_W02_nfCA() {

  std::complex<T> res(0,0);

  res+= rat2lLC_qqgaa[8] *tf2lLC_qqgaa[119];
  res+= rat2lLC_qqgaa[9] *tf2lLC_qqgaa[120];
  res+= rat2lLC_qqgaa[10]*tf2lLC_qqgaa[121];
  res+= rat2lLC_qqgaa[11]*tf2lLC_qqgaa[122];
  res+= rat2lLC_qqgaa[12]*tf2lLC_qqgaa[123];
  res+= rat2lLC_qqgaa[13]*tf2lLC_qqgaa[124];
  res+= rat2lLC_qqgaa[14]*tf2lLC_qqgaa[125];
  res+= rat2lLC_qqgaa[15]*tf2lLC_qqgaa[126];
  res+= rat2lLC_qqgaa[16]*tf2lLC_qqgaa[127];
  res+= rat2lLC_qqgaa[17]*tf2lLC_qqgaa[128];
  res+= rat2lLC_qqgaa[18]*tf2lLC_qqgaa[129];
  res+= rat2lLC_qqgaa[19]*tf2lLC_qqgaa[130];
  res+= rat2lLC_qqgaa[20]*tf2lLC_qqgaa[131];
  res+= rat2lLC_qqgaa[21]*tf2lLC_qqgaa[132];
  res+= rat2lLC_qqgaa[22]*tf2lLC_qqgaa[133];
  res+= rat2lLC_qqgaa[23]*tf2lLC_qqgaa[134];
  res+= rat2lLC_qqgaa[24]*tf2lLC_qqgaa[135];
  res+= rat2lLC_qqgaa[25]*tf2lLC_qqgaa[136];
  res+= rat2lLC_qqgaa[26]*tf2lLC_qqgaa[137];
  res+= rat2lLC_qqgaa[27]*tf2lLC_qqgaa[138];
  res+= rat2lLC_qqgaa[28]*tf2lLC_qqgaa[139];
  res+= rat2lLC_qqgaa[29]*tf2lLC_qqgaa[140];
  res+= rat2lLC_qqgaa[30]*tf2lLC_qqgaa[141];
  res+= rat2lLC_qqgaa[31]*tf2lLC_qqgaa[142];
  res+= rat2lLC_qqgaa[32]*tf2lLC_qqgaa[143];
  res+= rat2lLC_qqgaa[33]*tf2lLC_qqgaa[144];
  res+= rat2lLC_qqgaa[34]*tf2lLC_qqgaa[145];
  res+= rat2lLC_qqgaa[35]*tf2lLC_qqgaa[146];
  res+= rat2lLC_qqgaa[36]*tf2lLC_qqgaa[147];
  res+= rat2lLC_qqgaa[37]*tf2lLC_qqgaa[148];
  res+= rat2lLC_qqgaa[38]*tf2lLC_qqgaa[149];
  res+= rat2lLC_qqgaa[39]*tf2lLC_qqgaa[150];
  res+= rat2lLC_qqgaa[40]*tf2lLC_qqgaa[151];
  res+= rat2lLC_qqgaa[45]*tf2lLC_qqgaa[152]*eps5;
  res+= rat2lLC_qqgaa[46]*tf2lLC_qqgaa[153]*eps5;
  res+= rat2lLC_qqgaa[47]*tf2lLC_qqgaa[154]*eps5;
  res+= rat2lLC_qqgaa[48]*tf2lLC_qqgaa[155]*eps5;
  res+= rat2lLC_qqgaa[49]*tf2lLC_qqgaa[156]*eps5;
  res+= rat2lLC_qqgaa[50]*tf2lLC_qqgaa[157]*eps5;
  res+= rat2lLC_qqgaa[51]*tf2lLC_qqgaa[158]*eps5;
  res+= rat2lLC_qqgaa[52]*tf2lLC_qqgaa[159]*eps5;
  res+= rat2lLC_qqgaa[53]*tf2lLC_qqgaa[160];
  res+= rat2lLC_qqgaa[54]*tf2lLC_qqgaa[161];
  res+= rat2lLC_qqgaa[55]*tf2lLC_qqgaa[162];
  res+= rat2lLC_qqgaa[56]*tf2lLC_qqgaa[163];
  res+= rat2lLC_qqgaa[57]*tf2lLC_qqgaa[164];
  res+= rat2lLC_qqgaa[58]*tf2lLC_qqgaa[165];
  res+= rat2lLC_qqgaa[59]*tf2lLC_qqgaa[166];
  res+= rat2lLC_qqgaa[60]*tf2lLC_qqgaa[167];
  res+= rat2lLC_qqgaa[61]*tf2lLC_qqgaa[168];
  res+= rat2lLC_qqgaa[62]*tf2lLC_qqgaa[169];
  res+= rat2lLC_qqgaa[63]*tf2lLC_qqgaa[170];
  res+= rat2lLC_qqgaa[64]*tf2lLC_qqgaa[171];
  res+= rat2lLC_qqgaa[65]*tf2lLC_qqgaa[172];
  res+= rat2lLC_qqgaa[66]*tf2lLC_qqgaa[173];
  res+= rat2lLC_qqgaa[67]*tf2lLC_qqgaa[174];
  res+= rat2lLC_qqgaa[68]*tf2lLC_qqgaa[175];
  res+= rat2lLC_qqgaa[69]*tf2lLC_qqgaa[176];
  res+= rat2lLC_qqgaa[70]*tf2lLC_qqgaa[177];
  res+= rat2lLC_qqgaa[71]*tf2lLC_qqgaa[178];
  res+= rat2lLC_qqgaa[72]*tf2lLC_qqgaa[179];
  res+= rat2lLC_qqgaa[73]*tf2lLC_qqgaa[180];
  res+= rat2lLC_qqgaa[74]*tf2lLC_qqgaa[181];
  res+= rat2lLC_qqgaa[75]*tf2lLC_qqgaa[182];
  res+= rat2lLC_qqgaa[76]*tf2lLC_qqgaa[183];
  res+= rat2lLC_qqgaa[77]*tf2lLC_qqgaa[184];
  res+= rat2lLC_qqgaa[78]*tf2lLC_qqgaa[185];
  res+= rat2lLC_qqgaa[79]*tf2lLC_qqgaa[186];
  res+= rat2lLC_qqgaa[80]*tf2lLC_qqgaa[187];
  res+= rat2lLC_qqgaa[81]*tf2lLC_qqgaa[188];
  res+= rat2lLC_qqgaa[82]*tf2lLC_qqgaa[189];
  res+= rat2lLC_qqgaa[83]*tf2lLC_qqgaa[190];
  res+= rat2lLC_qqgaa[84]*tf2lLC_qqgaa[191];
  res+= rat2lLC_qqgaa[85]*tf2lLC_qqgaa[192];
  res+= rat2lLC_qqgaa[86]*tf2lLC_qqgaa[193];
  res+= rat2lLC_qqgaa[87]*tf2lLC_qqgaa[194];
  res+= rat2lLC_qqgaa[88]*tf2lLC_qqgaa[195];
  res+= rat2lLC_qqgaa[89]*tf2lLC_qqgaa[196];
  res+= rat2lLC_qqgaa[90]*tf2lLC_qqgaa[197];
  res+= rat2lLC_qqgaa[91]*tf2lLC_qqgaa[198];
  res+= rat2lLC_qqgaa[92]*tf2lLC_qqgaa[199];
  res+= rat2lLC_qqgaa[93]*tf2lLC_qqgaa[200];
  res+= rat2lLC_qqgaa[94]*tf2lLC_qqgaa[201];
  res+= rat2lLC_qqgaa[95]*tf2lLC_qqgaa[202];
  res+= rat2lLC_qqgaa[96]*tf2lLC_qqgaa[203];
  res+= rat2lLC_qqgaa[97]*tf2lLC_qqgaa[204];
  res+= rat2lLC_qqgaa[98]*tf2lLC_qqgaa[205];
  res+= rat2lLC_qqgaa[99]*tf2lLC_qqgaa[206];
  res /= 2;

  // Tr
  res /= 2;

  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W02_nfCA() {

  std::complex<T> res(0,0);

  res+= rat2lLC_qgqaa[8]*qg_l2_tf_nfCA_1(fw1);
  res+= rat2lLC_qgqaa[9]*qg_l2_tf_nfCA_2(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[10]*qg_l2_tf_nfCA_3(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[11]*qg_l2_tf_nfCA_4(tcs,fw1);
  res+= rat2lLC_qgqaa[12]*qg_l2_tf_nfCA_5(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[13]*qg_l2_tf_nfCA_6(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[14]*qg_l2_tf_nfCA_7(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[15]*qg_l2_tf_nfCA_8(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[16]*qg_l2_tf_nfCA_9(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[17]*qg_l2_tf_nfCA_10(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[18]*qg_l2_tf_nfCA_11(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[19]*qg_l2_tf_nfCA_12(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[20]*qg_l2_tf_nfCA_13(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[21]*qg_l2_tf_nfCA_14(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[22]*qg_l2_tf_nfCA_15(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[23]*qg_l2_tf_nfCA_16(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[24]*qg_l2_tf_nfCA_17(tcs,fw1);
  res+= rat2lLC_qgqaa[25]*qg_l2_tf_nfCA_18(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[26]*qg_l2_tf_nfCA_19(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[27]*qg_l2_tf_nfCA_20(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[28]*qg_l2_tf_nfCA_21(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[29]*qg_l2_tf_nfCA_22(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[30]*qg_l2_tf_nfCA_23(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[31]*qg_l2_tf_nfCA_24(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[32]*qg_l2_tf_nfCA_25(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[33]*qg_l2_tf_nfCA_26(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[34]*qg_l2_tf_nfCA_27(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[35]*qg_l2_tf_nfCA_28(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[36]*qg_l2_tf_nfCA_29(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[37]*qg_l2_tf_nfCA_30(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[38]*qg_l2_tf_nfCA_31(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[39]*qg_l2_tf_nfCA_32(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[40]*qg_l2_tf_nfCA_33(tcs,fw1);
  res+= rat2lLC_qgqaa[50 ]*(qg_l2_tf_nfCA_34(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[51 ]*(qg_l2_tf_nfCA_35(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[52 ]*(qg_l2_tf_nfCA_36(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[53 ]*(qg_l2_tf_nfCA_37(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[54 ]*(qg_l2_tf_nfCA_38(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[55 ]*(qg_l2_tf_nfCA_39(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[56 ]*(qg_l2_tf_nfCA_40(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[57 ]*(qg_l2_tf_nfCA_41(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[58 ]*qg_l2_tf_nfCA_42(fw1);
  res+= rat2lLC_qgqaa[59 ]*qg_l2_tf_nfCA_43(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[60 ]*qg_l2_tf_nfCA_44(tcs,fw1);
  res+= rat2lLC_qgqaa[61 ]*qg_l2_tf_nfCA_45(fw1);
  res+= rat2lLC_qgqaa[62 ]*qg_l2_tf_nfCA_46(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[63 ]*qg_l2_tf_nfCA_47(tcs,fw1);
  res+= rat2lLC_qgqaa[64 ]*qg_l2_tf_nfCA_48(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[65 ]*qg_l2_tf_nfCA_49(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[66 ]*qg_l2_tf_nfCA_50(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[67 ]*qg_l2_tf_nfCA_51(fw1);
  res+= rat2lLC_qgqaa[68 ]*qg_l2_tf_nfCA_52(tcs,fw1);
  res+= rat2lLC_qgqaa[69 ]*qg_l2_tf_nfCA_53(tcs,fw1);
  res+= rat2lLC_qgqaa[70 ]*qg_l2_tf_nfCA_54(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[71 ]*qg_l2_tf_nfCA_55(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[72 ]*qg_l2_tf_nfCA_56(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[73 ]*qg_l2_tf_nfCA_57(tcs,fw1);
  res+= rat2lLC_qgqaa[74 ]*qg_l2_tf_nfCA_58(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[75 ]*qg_l2_tf_nfCA_59(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[76 ]*qg_l2_tf_nfCA_60(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[77 ]*qg_l2_tf_nfCA_61(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[78 ]*qg_l2_tf_nfCA_62(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[79 ]*qg_l2_tf_nfCA_63(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[80 ]*qg_l2_tf_nfCA_64(fw1);
  res+= rat2lLC_qgqaa[81 ]*qg_l2_tf_nfCA_65(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[82 ]*qg_l2_tf_nfCA_66(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[83 ]*qg_l2_tf_nfCA_67(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[84 ]*qg_l2_tf_nfCA_68(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[85 ]*qg_l2_tf_nfCA_69(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[86 ]*qg_l2_tf_nfCA_70(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[87 ]*qg_l2_tf_nfCA_71(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[88 ]*qg_l2_tf_nfCA_72(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[89 ]*qg_l2_tf_nfCA_73(tcs,fw1);
  res+= rat2lLC_qgqaa[90 ]*qg_l2_tf_nfCA_74(tcs,fw1);
  res+= rat2lLC_qgqaa[91 ]*qg_l2_tf_nfCA_75(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[92 ]*qg_l2_tf_nfCA_76(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[93 ]*qg_l2_tf_nfCA_77(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[94]*qg_l2_tf_nfCA_78(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[95]*qg_l2_tf_nfCA_79(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[96]*qg_l2_tf_nfCA_80(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[97]*qg_l2_tf_nfCA_81(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[98]*qg_l2_tf_nfCA_82(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[99]*qg_l2_tf_nfCA_83(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[100]*qg_l2_tf_nfCA_84(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[101]*qg_l2_tf_nfCA_85(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[102]*qg_l2_tf_nfCA_86(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[103]*qg_l2_tf_nfCA_87(tcs,fw1);
  res+= rat2lLC_qgqaa[104]*qg_l2_tf_nfCA_88(tcs,fw1);
  res /= 2;

  // Tr
  res /= 2;

  return res.real();
}

// nf*CF colour structure
template <class T>
T Interference<T>::qqgaa_interf_W02_nfCF() {

  std::complex<T> res(0,0);

  res += rat2lLC_qqgaa[9]*tf2lLC_qqgaa[207];
  res += rat2lLC_qqgaa[11]*tf2lLC_qqgaa[208];
  res += rat2lLC_qqgaa[12]*tf2lLC_qqgaa[209];
  res += rat2lLC_qqgaa[14]*tf2lLC_qqgaa[210];
  res += rat2lLC_qqgaa[17]*tf2lLC_qqgaa[211];
  res += rat2lLC_qqgaa[18]*tf2lLC_qqgaa[212];
  res += rat2lLC_qqgaa[19]*tf2lLC_qqgaa[213];
  res += rat2lLC_qqgaa[20]*tf2lLC_qqgaa[214];
  res += rat2lLC_qqgaa[21]*tf2lLC_qqgaa[215];
  res += rat2lLC_qqgaa[23]*tf2lLC_qqgaa[216];
  res += rat2lLC_qqgaa[26]*tf2lLC_qqgaa[217];
  res += rat2lLC_qqgaa[27]*tf2lLC_qqgaa[218];
  res += rat2lLC_qqgaa[28]*tf2lLC_qqgaa[219];
  res += rat2lLC_qqgaa[29]*tf2lLC_qqgaa[220];
  res += rat2lLC_qqgaa[30]*tf2lLC_qqgaa[221];
  res += rat2lLC_qqgaa[31]*tf2lLC_qqgaa[222];
  res += rat2lLC_qqgaa[32]*tf2lLC_qqgaa[223];
  res += rat2lLC_qqgaa[33]*tf2lLC_qqgaa[224];
  res += rat2lLC_qqgaa[34]*tf2lLC_qqgaa[225];
  res += rat2lLC_qqgaa[35]*tf2lLC_qqgaa[226];
  res += rat2lLC_qqgaa[36]*tf2lLC_qqgaa[227];
  res += rat2lLC_qqgaa[37]*tf2lLC_qqgaa[228];
  res += rat2lLC_qqgaa[38]*tf2lLC_qqgaa[229];
  res += rat2lLC_qqgaa[39]*tf2lLC_qqgaa[230];
  res += rat2lLC_qqgaa[40]*tf2lLC_qqgaa[231];
  res += rat2lLC_qqgaa[41]*tf2lLC_qqgaa[232];
  res += rat2lLC_qqgaa[42]*tf2lLC_qqgaa[233];
  res += rat2lLC_qqgaa[43]*tf2lLC_qqgaa[234];
  res += rat2lLC_qqgaa[44]*tf2lLC_qqgaa[235];
  res += rat2lLC_qqgaa[47]*tf2lLC_qqgaa[236]*eps5;
  res += rat2lLC_qqgaa[48]*tf2lLC_qqgaa[237]*eps5;
  res += rat2lLC_qqgaa[49]*tf2lLC_qqgaa[238]*eps5;
  res += rat2lLC_qqgaa[50]*tf2lLC_qqgaa[239]*eps5;
  res += rat2lLC_qqgaa[51]*tf2lLC_qqgaa[240]*eps5;
  res += rat2lLC_qqgaa[52]*tf2lLC_qqgaa[241]*eps5;
  res += rat2lLC_qqgaa[53]*tf2lLC_qqgaa[242];
  res += rat2lLC_qqgaa[54]*tf2lLC_qqgaa[243];
  res += rat2lLC_qqgaa[55]*tf2lLC_qqgaa[244];
  res += rat2lLC_qqgaa[58]*tf2lLC_qqgaa[245];
  res += rat2lLC_qqgaa[61]*tf2lLC_qqgaa[246];
  res += rat2lLC_qqgaa[62]*tf2lLC_qqgaa[247];
  res += rat2lLC_qqgaa[63]*tf2lLC_qqgaa[248];
  res += rat2lLC_qqgaa[67]*tf2lLC_qqgaa[249];
  res += rat2lLC_qqgaa[68]*tf2lLC_qqgaa[250];
  res += rat2lLC_qqgaa[69]*tf2lLC_qqgaa[251];
  res += rat2lLC_qqgaa[70]*tf2lLC_qqgaa[252];
  res += rat2lLC_qqgaa[71]*tf2lLC_qqgaa[253];
  res += rat2lLC_qqgaa[72]*tf2lLC_qqgaa[254];
  res += rat2lLC_qqgaa[73]*tf2lLC_qqgaa[255];
  res += rat2lLC_qqgaa[79]*tf2lLC_qqgaa[256];
  res += rat2lLC_qqgaa[88]*tf2lLC_qqgaa[257];
  res += rat2lLC_qqgaa[89]*tf2lLC_qqgaa[258];
  res += rat2lLC_qqgaa[90]*tf2lLC_qqgaa[259];
  res += rat2lLC_qqgaa[93]*tf2lLC_qqgaa[260];
  res += rat2lLC_qqgaa[95]*tf2lLC_qqgaa[261];
  res += rat2lLC_qqgaa[96]*tf2lLC_qqgaa[262];
  res += rat2lLC_qqgaa[97]*tf2lLC_qqgaa[263];
  res += rat2lLC_qqgaa[98]*tf2lLC_qqgaa[264];
  res += rat2lLC_qqgaa[99]*tf2lLC_qqgaa[265];
  res += rat2lLC_qqgaa[100]*tf2lLC_qqgaa[266];
  res += rat2lLC_qqgaa[101]*tf2lLC_qqgaa[267];
  res += rat2lLC_qqgaa[102]*tf2lLC_qqgaa[268];
  res += rat2lLC_qqgaa[103]*tf2lLC_qqgaa[269];
  res += rat2lLC_qqgaa[104]*tf2lLC_qqgaa[270];
  res += rat2lLC_qqgaa[105]*tf2lLC_qqgaa[271];
  res += rat2lLC_qqgaa[106]*tf2lLC_qqgaa[272];
  res += rat2lLC_qqgaa[107]*tf2lLC_qqgaa[273];
  res += rat2lLC_qqgaa[108]*tf2lLC_qqgaa[274];
  res += rat2lLC_qqgaa[109]*tf2lLC_qqgaa[275];
  res += rat2lLC_qqgaa[110]*tf2lLC_qqgaa[276];
  res += rat2lLC_qqgaa[111]*tf2lLC_qqgaa[277];
  res += rat2lLC_qqgaa[112]*tf2lLC_qqgaa[278];
  res += rat2lLC_qqgaa[113]*tf2lLC_qqgaa[279];
  res += rat2lLC_qqgaa[114]*tf2lLC_qqgaa[280];
  res /= 2;

  // Tr
  res /= 2;

  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W02_nfCF() {

  std::complex<T> res(0,0);

  res+= rat2lLC_qgqaa[9]*qg_l2_tf_nfCF_1(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[10]*qg_l2_tf_nfCF_2(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[12]*qg_l2_tf_nfCF_3(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[13]*qg_l2_tf_nfCF_4(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[14]*qg_l2_tf_nfCF_5(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[15]*qg_l2_tf_nfCF_6(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[16]*qg_l2_tf_nfCF_7(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[17]*qg_l2_tf_nfCF_8(tcs,fw1);
  res+= rat2lLC_qgqaa[18]*qg_l2_tf_nfCF_9(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[19]*qg_l2_tf_nfCF_10(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[20]*qg_l2_tf_nfCF_11(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[21]*qg_l2_tf_nfCF_12(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[22]*qg_l2_tf_nfCF_13(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[23]*qg_l2_tf_nfCF_14(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[25]*qg_l2_tf_nfCF_15(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[26]*qg_l2_tf_nfCF_16(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[27]*qg_l2_tf_nfCF_17(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[28]*qg_l2_tf_nfCF_18(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[29]*qg_l2_tf_nfCF_19(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[32]*qg_l2_tf_nfCF_20(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[33]*qg_l2_tf_nfCF_21(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[34]*qg_l2_tf_nfCF_22(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[35]*qg_l2_tf_nfCF_23(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[37]*qg_l2_tf_nfCF_24(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[39]*qg_l2_tf_nfCF_25(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[41]*qg_l2_tf_nfCF_26(tcs,fw1);
  res+= rat2lLC_qgqaa[42]*qg_l2_tf_nfCF_27(tcs,fw1);
  res+= rat2lLC_qgqaa[43]*qg_l2_tf_nfCF_28(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[44]*qg_l2_tf_nfCF_29(tcs,fw1);
  res+= rat2lLC_qgqaa[45]*qg_l2_tf_nfCF_30(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[46]*qg_l2_tf_nfCF_31(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[47]*qg_l2_tf_nfCF_32(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[48]*qg_l2_tf_nfCF_33(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[49]*qg_l2_tf_nfCF_34(tcs,fw1);
  res+= rat2lLC_qgqaa[50]*(qg_l2_tf_nfCF_35(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[51]*(qg_l2_tf_nfCF_36(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[52]*(qg_l2_tf_nfCF_37(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[53]*(qg_l2_tf_nfCF_38(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[54]*(qg_l2_tf_nfCF_39(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[55]*(qg_l2_tf_nfCF_40(tcs,fw3)*eps5);
  res+= rat2lLC_qgqaa[58]*qg_l2_tf_nfCF_41(fw1);
  res+= rat2lLC_qgqaa[59]*qg_l2_tf_nfCF_42(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[61]*qg_l2_tf_nfCF_43(fw1);
  res+= rat2lLC_qgqaa[62]*qg_l2_tf_nfCF_44(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[63]*qg_l2_tf_nfCF_45(tcs,fw1);
  res+= rat2lLC_qgqaa[64]*qg_l2_tf_nfCF_46(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[65]*qg_l2_tf_nfCF_47(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[66]*qg_l2_tf_nfCF_48(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[69]*qg_l2_tf_nfCF_49(tcs,fw1);
  res+= rat2lLC_qgqaa[70]*qg_l2_tf_nfCF_50(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[71]*qg_l2_tf_nfCF_51(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[72]*qg_l2_tf_nfCF_52(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[74]*qg_l2_tf_nfCF_53(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[75]*qg_l2_tf_nfCF_54(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[76]*qg_l2_tf_nfCF_55(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[77]*qg_l2_tf_nfCF_56(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[81]*qg_l2_tf_nfCF_57(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[82]*qg_l2_tf_nfCF_58(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[83]*qg_l2_tf_nfCF_59(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[85]*qg_l2_tf_nfCF_60(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[87]*qg_l2_tf_nfCF_61(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[91]*qg_l2_tf_nfCF_62(tcs,fw1);
  res+= rat2lLC_qgqaa[96]*qg_l2_tf_nfCF_63(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[97]*qg_l2_tf_nfCF_64(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[98]*qg_l2_tf_nfCF_65(tcs,fw1,fw2,fw3);
  res+= rat2lLC_qgqaa[105]*qg_l2_tf_nfCF_66(tcs,fw1);
  res+= rat2lLC_qgqaa[106]*qg_l2_tf_nfCF_67(fw1);
  res+= rat2lLC_qgqaa[107]*qg_l2_tf_nfCF_68(fw1);
  res+= rat2lLC_qgqaa[108]*qg_l2_tf_nfCF_69(tcs,fw1);
  res+= rat2lLC_qgqaa[109]*qg_l2_tf_nfCF_70(tcs,fw1);
  res+= rat2lLC_qgqaa[110]*qg_l2_tf_nfCF_71(tcs,fw1);
  res+= rat2lLC_qgqaa[111]*qg_l2_tf_nfCF_72(tcs,fw1);
  res+= rat2lLC_qgqaa[112]*qg_l2_tf_nfCF_73(tcs,fw1);
  res+= rat2lLC_qgqaa[113]*qg_l2_tf_nfCF_74(tcs,fw1,fw2);
  res+= rat2lLC_qgqaa[114]*qg_l2_tf_nfCF_75(tcs,fw1);
  res+= rat2lLC_qgqaa[115]*qg_l2_tf_nfCF_76(fw1);
  res /= 2;

  // Tr
  res /= 2;

  return res.real();
}

// nf*nfaa colour structure
template <class T>
T Interference<T>::qqgaa_interf_W02_nfnfaa() {

  std::complex<T> res(0,0);

  res += rat2lLC_qqgaa[1]*tf2lLC_qqgaa[281];
  res += rat2lLC_qqgaa[2]*tf2lLC_qqgaa[282];
  res += rat2lLC_qqgaa[3]*tf2lLC_qqgaa[283];
  res += rat2lLC_qqgaa[4]*tf2lLC_qqgaa[284];
  res += rat2lLC_qqgaa[5]*tf2lLC_qqgaa[285];
  res += rat2lLC_qqgaa[6]*tf2lLC_qqgaa[286];
  res += rat2lLC_qqgaa[7]*tf2lLC_qqgaa[287];
  res /= 2;

  // Tr^2
  res /= 4;

  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W02_nfnfaa() {

  std::complex<T> res(0,0);

  res += rat2lLC_qgqaa[1]*qg_l2_tf_nfnfaa_1(tcs,fw1);
  res += rat2lLC_qgqaa[2]*qg_l2_tf_nfnfaa_2(tcs,fw1,fw2);
  res += rat2lLC_qgqaa[3]*qg_l2_tf_nfnfaa_3(tcs,fw1);
  res += rat2lLC_qgqaa[4]*qg_l2_tf_nfnfaa_4(tcs,fw1);
  res += rat2lLC_qgqaa[5]*qg_l2_tf_nfnfaa_5(tcs,fw1,fw2);
  res += rat2lLC_qgqaa[6]*qg_l2_tf_nfnfaa_6(tcs,fw1,fw2);
  res += rat2lLC_qgqaa[7]*qg_l2_tf_nfnfaa_7(tcs,fw1);
  res /= 2;

  // Tr
  res /= 4;

  return res.real();
}

template <class T>
T Interference<T>::qqgaa_interf_W02_nfa() {

  std::complex<T> res(0,0);

  res+= rat2lLC_qqgaa[222]*tf2lLC_qqgaa[288];
  res+= rat2lLC_qqgaa[223]*tf2lLC_qqgaa[289];
  res+= rat2lLC_qqgaa[224]*tf2lLC_qqgaa[290];
  res+= rat2lLC_qqgaa[225]*tf2lLC_qqgaa[291];
  res+= rat2lLC_qqgaa[226]*tf2lLC_qqgaa[292];
  res+= rat2lLC_qqgaa[227]*tf2lLC_qqgaa[293];
  res+= rat2lLC_qqgaa[228]*tf2lLC_qqgaa[294];
  res+= rat2lLC_qqgaa[229]*tf2lLC_qqgaa[295];
  res+= rat2lLC_qqgaa[230]*tf2lLC_qqgaa[296];
  res+= rat2lLC_qqgaa[231]*tf2lLC_qqgaa[297];
  res+= rat2lLC_qqgaa[232]*tf2lLC_qqgaa[298];
  res+= rat2lLC_qqgaa[233]*tf2lLC_qqgaa[299];
  res+= rat2lLC_qqgaa[234]*tf2lLC_qqgaa[300];
  res+= rat2lLC_qqgaa[235]*tf2lLC_qqgaa[301];
  res+= rat2lLC_qqgaa[236]*tf2lLC_qqgaa[302];
  res+= rat2lLC_qqgaa[237]*tf2lLC_qqgaa[303];
  res+= rat2lLC_qqgaa[238]*tf2lLC_qqgaa[304];
  res+= rat2lLC_qqgaa[239]*tf2lLC_qqgaa[305];
  res+= rat2lLC_qqgaa[240]*tf2lLC_qqgaa[306];
  res+= rat2lLC_qqgaa[241]*tf2lLC_qqgaa[307];
  res+= rat2lLC_qqgaa[242]*tf2lLC_qqgaa[308];
  res+= rat2lLC_qqgaa[243]*tf2lLC_qqgaa[309]*eps5;
  res+= rat2lLC_qqgaa[244]*tf2lLC_qqgaa[310];
  res+= rat2lLC_qqgaa[245]*tf2lLC_qqgaa[311];
  res+= rat2lLC_qqgaa[246]*tf2lLC_qqgaa[312];
  res+= rat2lLC_qqgaa[247]*tf2lLC_qqgaa[313];
  res+= rat2lLC_qqgaa[248]*tf2lLC_qqgaa[314];
  res+= rat2lLC_qqgaa[249]*tf2lLC_qqgaa[315];
  res+= rat2lLC_qqgaa[250]*tf2lLC_qqgaa[316];
  res+= rat2lLC_qqgaa[251]*tf2lLC_qqgaa[317];
  res+= rat2lLC_qqgaa[252]*tf2lLC_qqgaa[318];
  res+= rat2lLC_qqgaa[253]*tf2lLC_qqgaa[319];
  res+= rat2lLC_qqgaa[254]*tf2lLC_qqgaa[320];
  res+= rat2lLC_qqgaa[255]*tf2lLC_qqgaa[321];
  res+= rat2lLC_qqgaa[256]*tf2lLC_qqgaa[322];
  res+= rat2lLC_qqgaa[257]*tf2lLC_qqgaa[323];
  res+= rat2lLC_qqgaa[258]*tf2lLC_qqgaa[324];
  res+= rat2lLC_qqgaa[259]*tf2lLC_qqgaa[325];
  res+= rat2lLC_qqgaa[260]*tf2lLC_qqgaa[326];
  res+= rat2lLC_qqgaa[261]*tf2lLC_qqgaa[327];
  res+= rat2lLC_qqgaa[262]*tf2lLC_qqgaa[328];
  res+= rat2lLC_qqgaa[263]*tf2lLC_qqgaa[329];
  res+= rat2lLC_qqgaa[264]*tf2lLC_qqgaa[330];
  res+= rat2lLC_qqgaa[265]*tf2lLC_qqgaa[331];
  res+= rat2lLC_qqgaa[266]*tf2lLC_qqgaa[332];
  res+= rat2lLC_qqgaa[267]*tf2lLC_qqgaa[333];
  res+= rat2lLC_qqgaa[268]*tf2lLC_qqgaa[334]*eps5;
  res+= rat2lLC_qqgaa[269]*tf2lLC_qqgaa[335]*eps5;
  res+= rat2lLC_qqgaa[270]*tf2lLC_qqgaa[336]*eps5;
  res+= rat2lLC_qqgaa[271]*tf2lLC_qqgaa[337]*eps5;
  res+= rat2lLC_qqgaa[272]*tf2lLC_qqgaa[338]*eps5;
  res+= rat2lLC_qqgaa[273]*tf2lLC_qqgaa[339]*eps5;
  res+= rat2lLC_qqgaa[274]*tf2lLC_qqgaa[340];
  res+= rat2lLC_qqgaa[275]*tf2lLC_qqgaa[341];
  res+= rat2lLC_qqgaa[276]*tf2lLC_qqgaa[342];
  res+= rat2lLC_qqgaa[277]*tf2lLC_qqgaa[343];
  res+= rat2lLC_qqgaa[278]*tf2lLC_qqgaa[344];
  res+= rat2lLC_qqgaa[279]*tf2lLC_qqgaa[345];
  res+= rat2lLC_qqgaa[280]*tf2lLC_qqgaa[346];
  res+= rat2lLC_qqgaa[281]*tf2lLC_qqgaa[347];
  res+= rat2lLC_qqgaa[282]*tf2lLC_qqgaa[348];
  res+= rat2lLC_qqgaa[283]*tf2lLC_qqgaa[349];
  res+= rat2lLC_qqgaa[284]*tf2lLC_qqgaa[350];
  res+= rat2lLC_qqgaa[285]*tf2lLC_qqgaa[351];
  res+= rat2lLC_qqgaa[286]*tf2lLC_qqgaa[352];
  res+= rat2lLC_qqgaa[287]*tf2lLC_qqgaa[353];
  res+= rat2lLC_qqgaa[288]*tf2lLC_qqgaa[354];
  res+= rat2lLC_qqgaa[289]*tf2lLC_qqgaa[355];
  res+= rat2lLC_qqgaa[290]*tf2lLC_qqgaa[356];
  res+= rat2lLC_qqgaa[291]*tf2lLC_qqgaa[357];
  res+= rat2lLC_qqgaa[292]*tf2lLC_qqgaa[358];
  res+= rat2lLC_qqgaa[293]*tf2lLC_qqgaa[359];
  res+= rat2lLC_qqgaa[294]*tf2lLC_qqgaa[360];
  res+= rat2lLC_qqgaa[295]*tf2lLC_qqgaa[361];
  res+= rat2lLC_qqgaa[296]*tf2lLC_qqgaa[362];
  res+= rat2lLC_qqgaa[297]*tf2lLC_qqgaa[363];
  res+= rat2lLC_qqgaa[298]*tf2lLC_qqgaa[364];
  res+= rat2lLC_qqgaa[299]*tf2lLC_qqgaa[365];
  res+= rat2lLC_qqgaa[300]*tf2lLC_qqgaa[366];
  res+= rat2lLC_qqgaa[301]*tf2lLC_qqgaa[367];
  res+= rat2lLC_qqgaa[302]*tf2lLC_qqgaa[368];
  res+= rat2lLC_qqgaa[303]*tf2lLC_qqgaa[369];
  res+= rat2lLC_qqgaa[304]*tf2lLC_qqgaa[370];
  res+= rat2lLC_qqgaa[305]*tf2lLC_qqgaa[371];
  res+= rat2lLC_qqgaa[306]*tf2lLC_qqgaa[372];
  res+= rat2lLC_qqgaa[307]*tf2lLC_qqgaa[373];
  res+= rat2lLC_qqgaa[308]*tf2lLC_qqgaa[374];
  res+= rat2lLC_qqgaa[309]*tf2lLC_qqgaa[375];
  res+= rat2lLC_qqgaa[310]*tf2lLC_qqgaa[376];
  res+= rat2lLC_qqgaa[311]*tf2lLC_qqgaa[377];
  res+= rat2lLC_qqgaa[312]*tf2lLC_qqgaa[378];
  res+= rat2lLC_qqgaa[313]*tf2lLC_qqgaa[379];
  res+= rat2lLC_qqgaa[314]*tf2lLC_qqgaa[380];
  res+= rat2lLC_qqgaa[315]*tf2lLC_qqgaa[381];
  res+= rat2lLC_qqgaa[316]*tf2lLC_qqgaa[382];
  res+= rat2lLC_qqgaa[317]*tf2lLC_qqgaa[383];
  res+= rat2lLC_qqgaa[318]*tf2lLC_qqgaa[384];
  res+= rat2lLC_qqgaa[319]*tf2lLC_qqgaa[385];
  res+= rat2lLC_qqgaa[320]*tf2lLC_qqgaa[386];
  res+= rat2lLC_qqgaa[321]*tf2lLC_qqgaa[387];
  res+= rat2lLC_qqgaa[322]*tf2lLC_qqgaa[388];
  res+= rat2lLC_qqgaa[323]*tf2lLC_qqgaa[389];
  res+= rat2lLC_qqgaa[324]*tf2lLC_qqgaa[390];
  res+= rat2lLC_qqgaa[325]*tf2lLC_qqgaa[391];
  res+= rat2lLC_qqgaa[326]*tf2lLC_qqgaa[392];
  res+= rat2lLC_qqgaa[327]*tf2lLC_qqgaa[393];
  res+= rat2lLC_qqgaa[328]*tf2lLC_qqgaa[394];
  res+= rat2lLC_qqgaa[329]*tf2lLC_qqgaa[395];
  res+= rat2lLC_qqgaa[330]*tf2lLC_qqgaa[396];
  res+= rat2lLC_qqgaa[331]*tf2lLC_qqgaa[397];
  res+= rat2lLC_qqgaa[332]*tf2lLC_qqgaa[398];
  res+= rat2lLC_qqgaa[333]*tf2lLC_qqgaa[399];
  res+= rat2lLC_qqgaa[334]*tf2lLC_qqgaa[400];
  res+= rat2lLC_qqgaa[335]*tf2lLC_qqgaa[401];
  res+= rat2lLC_qqgaa[336]*tf2lLC_qqgaa[402];
  res+= rat2lLC_qqgaa[337]*tf2lLC_qqgaa[403];
  res+= rat2lLC_qqgaa[338]*tf2lLC_qqgaa[404];
  res+= rat2lLC_qqgaa[339]*tf2lLC_qqgaa[405];
  res+= rat2lLC_qqgaa[340]*tf2lLC_qqgaa[406];
  res+= rat2lLC_qqgaa[341]*tf2lLC_qqgaa[407];
  res+= rat2lLC_qqgaa[342]*tf2lLC_qqgaa[408];
  res /= sij[0];
  res /= 2;

  return res.real();
}

// nf^2 colour structure
template <class T>
T Interference<T>::qqgaa_interf_W02_nfnf() {

  std::complex<T> res(0,0);

  res += -rat0l_qqgaa[0]*(tcs[3]/54 - 10*fw1[1]/27 - 10*fw1[5]/27 +
         5*fw1[5]*fw1[5]/36 + 5*fw1[1]*fw1[1]/36 +
         fw1[1]*fw1[5]/18);
  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W02_nfnf() {

  std::complex<T> res(0,0);
  const std::complex<T> i(0,1);

  res += rat0l_qgqaa[0]*(-13*tcs[3]/108 - 10*fw1[0]/27 +
         5*fw1[0]*fw1[0]/36 + i*tcs[0]*(static_cast<T>(10)/27 -
         5*fw1[0]/18 - fw1[1]/18) - 10*fw1[1]/27 + fw1[0]*fw1[1]/18 +
         5*fw1[1]*fw1[1]/36);
  return res.real();
}

#ifdef FULLCOL_ENABLED
/*------------------------ qqb ------------------------- */
template <class T>
T Interference<T>::qqgaa_interf_W02_Nc0() {

  std::complex<T> res(0,0);

  for(int i = 1; i < 493; i++){
      res += rat2lNLC_qqgaa[i]*tf2lNLC_qqgaa[i];
  }
  res += rat2lNLC_qqgaa[493];
  res /= sij[0];
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qqgaa_interf_W02_Ncm2() {

  std::complex<T> res(0,0);

  for(int i = 493; i < 1008; i++){
      res += rat2lNLC_qqgaa[i+1]*tf2lNLC_qqgaa[i];
  }
  res += rat2lNLC_qqgaa[1009];
  res /= sij[0];
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qqgaa_interf_W02_nfaaNc() {

  std::complex<T> res(0,0);

  for(int i = 1008; i < 1268; i++){
      res += rat2lNLC_qqgaa[i+2]*tf2lNLC_qqgaa[i];
  }
  res += rat2lNLC_qqgaa[1270];
  res /= sij[0];
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qqgaa_interf_W02_nfaaNcm1() {

  std::complex<T> res(0,0);

  for(int i = 1268; i < 1361; i++){
      res += rat2lNLC_qqgaa[i+3]*tf2lNLC_qqgaa[i];
  }
  res += rat2lNLC_qqgaa[1364];
  res /= sij[0];
  res /= 2;
  return res.real();
}

/*------------------------- qg ------------------------- */
template <class T>
T Interference<T>::qgqaa_interf_W02_Nc0() {

  std::complex<T> res(0,0);

  for(int i = 1; i < 493; i++){
      res += rat2lNLC_qgqaa[i]*tf2lNLC_qgqaa[i];
  }
  res += rat2lNLC_qgqaa[493];
  res /= sij[0];
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W02_Ncm2() {

  std::complex<T> res(0,0);

  for(int i = 493; i < 1008; i++){
      res += rat2lNLC_qgqaa[i+1]*tf2lNLC_qgqaa[i];
  }
  res += rat2lNLC_qgqaa[1009];
  res /= sij[0];
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W02_nfaaNc() {

  std::complex<T> res(0,0);

  for(int i = 1008; i < 1268; i++){
      res += rat2lNLC_qgqaa[i+2]*tf2lNLC_qgqaa[i];
  }
  res += rat2lNLC_qgqaa[1270];
  res /= sij[0];
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W02_nfaaNcm1() {

  std::complex<T> res(0,0);

  for(int i = 1268; i < 1361; i++){
      res += rat2lNLC_qgqaa[i+3]*tf2lNLC_qgqaa[i];
  }
  res += rat2lNLC_qgqaa[1364];
  res /= sij[0];
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W02_nfa() {

  std::complex<T> res(0,0);

  for(int i = 1361; i < 1482; i++){
      res += rat2lNLC_qgqaa[i+4]*tf2lNLC_qgqaa[i];
  }
  res /= sij[0];
  res /= 2;
  return res.real();
}
#endif

#ifdef LOOPSQ_ENABLED
// [1-loop* x 2-loop] fermionic loops interference: qqb
//---------------------------------------------------------------------------
template <class T>
T Interference<T>::qqgaa_interf_W12_nfaa2Ncp1() {

  std::complex<T> res(0,0);

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qqb[i]*(Aqqb_1l_nfaa[i]  * std::conj(Aqqb_2l_nfaaNcp1[i]));
      res += SpPh_qqb[i]*(Aqqb_1l_nfaa[i+8]* std::conj(Aqqb_2l_nfaaNcp1[i+8]));
  }
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qqgaa_interf_W12_nfaa2Ncm1() {

  std::complex<T> res(0,0);

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qqb[i]*(Aqqb_1l_nfaa[i]  * std::conj(Aqqb_2l_nfaaNcm1[i]));
      res += SpPh_qqb[i]*(Aqqb_1l_nfaa[i+8]* std::conj(Aqqb_2l_nfaaNcm1[i+8]));
  }
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qqgaa_interf_W12_nfaa2nf() {

  std::complex<T> res(0,0);

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qqb[i]*(Aqqb_1l_nfaa[i]  * std::conj(Aqqb_2l_nfnfaa[i]));
      res += SpPh_qqb[i]*(Aqqb_1l_nfaa[i+8]* std::conj(Aqqb_2l_nfnfaa[i+8]));
  }
  res /= 2;
  return res.real();
}

// [1-loop* x 2-loop] fermionic loops interference: qg
//---------------------------------------------------------------------------
template <class T>
T Interference<T>::qgqaa_interf_W12_nfaa2Ncp1() {

  std::complex<T> res(0,0);

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qg[i]*(Aqg_1l_nfaa[i]  * std::conj(Aqg_2l_nfaaNcp1[i]));
      res += SpPh_qg[i]*(Aqg_1l_nfaa[i+8]* std::conj(Aqg_2l_nfaaNcp1[i+8]));
  }
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W12_nfaa2Ncm1() {

  std::complex<T> res(0,0);

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qg[i]*(Aqg_1l_nfaa[i]  * std::conj(Aqg_2l_nfaaNcm1[i]));
      res += SpPh_qg[i]*(Aqg_1l_nfaa[i+8]* std::conj(Aqg_2l_nfaaNcm1[i+8]));
  }
  res /= 2;
  return res.real();
}

template <class T>
T Interference<T>::qgqaa_interf_W12_nfaa2nf() {

  std::complex<T> res(0,0);

  for (size_t i = 0; i < 8; ++i) {
      res += SpPh_qg[i]*(Aqg_1l_nfaa[i]  * std::conj(Aqg_2l_nfnfaa[i]));
      res += SpPh_qg[i]*(Aqg_1l_nfaa[i+8]* std::conj(Aqg_2l_nfnfaa[i+8]));
  }
  res /= 2;
  return res.real();
}
#endif

/*---------------------------------------------------------------------------*/

template class Interference<double>;
#ifdef HAVE_QD
template class Interference<dd_real>;
#endif
