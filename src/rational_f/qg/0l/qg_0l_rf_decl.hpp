#ifndef QG_0L_RF_DECL
#define QG_0L_RF_DECL

#include <array>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
T qg_0l_r1(const std::array<T,31>&);

#endif /* QG_0L_RF_DECL_H */
