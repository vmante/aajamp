#include "qg_0l_rf_decl.hpp"

template<class T>
T qg_0l_r1(const std::array<T,31>& k) {

  T s12, s13, s14, s51, s23, s35, s34, s45;
  T r;

  s12 = k[1];
  s23 = k[2];
  s34 = k[3];
  s45 = k[4];
  s51 = k[5];

  s13 = s45 - s12 - s23;
  s14 = s23 - s51 - s45;
  s35 = s12 - s34 - s45;

  r = -s13*((s12*s12 + s23*s23)/(s14*s51*s34*s35) +
            (s14*s14 + s34*s34)/(s12*s51*s23*s35) +
            (s51*s51 + s35*s35)/(s12*s14*s23*s34));

  return r;
}

template double qg_0l_r1(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_0l_r1(const std::array<dd_real,31>&);
#endif
