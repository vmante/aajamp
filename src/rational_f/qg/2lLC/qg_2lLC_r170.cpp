#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r170(const std::array<T,31>& k) {
  T z[59];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[8];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=k[3];
    z[11]=k[2];
    z[12]=k[6];
    z[13]=k[15];
    z[14]=k[14];
    z[15]=k[16];
    z[16]=npow(z[2],2);
    z[17]=2*z[16];
    z[18]=npow(z[13],3);
    z[19]=z[18]*z[17];
    z[20]=npow(z[2],3);
    z[21]=npow(z[13],2);
    z[22]=z[20]*z[21];
    z[22]=z[2] + z[22];
    z[23]=z[9]*z[13];
    z[22]=z[22]*z[23];
    z[19]=z[19] + 3*z[22];
    z[19]=z[12]*z[19];
    z[22]=z[21]*z[16];
    z[24]= - static_cast<T>(1)- z[22];
    z[24]=z[12]*z[24];
    z[24]=z[24] + z[7];
    z[24]=z[23]*z[24];
    z[25]=z[5]*z[13];
    z[26]=z[12]*z[25];
    z[27]=npow(z[5],2);
    z[28]=z[27]*z[13];
    z[29]=z[28]*z[12];
    z[30]= - z[14]*z[29];
    z[26]=2*z[26] + z[30];
    z[26]=z[14]*z[26];
    z[30]=z[12]*z[13];
    z[26]= - z[30] + z[26];
    z[30]=2*z[14];
    z[26]=z[26]*z[30];
    z[24]=z[26] + z[24];
    z[24]=z[11]*z[24];
    z[26]=z[13]*z[2];
    z[31]=z[26] - 1;
    z[32]=2*z[21];
    z[33]= - z[31]*z[32];
    z[34]=z[9]*z[16]*z[18];
    z[35]=static_cast<T>(1)+ z[26];
    z[35]=z[9]*z[35];
    z[35]=z[13] + z[35];
    z[35]=z[7]*z[35];
    z[36]=3*z[5];
    z[37]=z[14]*z[36];
    z[37]= - static_cast<T>(1)+ z[37];
    z[37]=z[37]*npow(z[14],2);
    z[19]=z[24] + 2*z[37] + z[35] + z[19] + z[33] - 3*z[34];
    z[19]=z[11]*z[19];
    z[24]=z[16]*z[13];
    z[33]=2*z[2];
    z[34]= - z[33] + z[24];
    z[34]=z[34]*z[32];
    z[35]=z[21]*z[5];
    z[37]= - static_cast<T>(2)+ z[26];
    z[37]=z[37]*z[35];
    z[34]=z[34] + z[37];
    z[34]=z[6]*z[34];
    z[37]=2*z[26];
    z[38]=static_cast<T>(1)- z[37];
    z[38]=z[38]*z[32];
    z[39]=npow(z[4],2);
    z[40]=z[5] - z[2];
    z[41]=z[4]*z[40];
    z[41]=static_cast<T>(19)+ 15*z[41];
    z[41]=z[41]*z[39];
    z[42]=z[33] + z[5];
    z[43]= - z[6]*z[18]*z[42];
    z[44]=npow(z[4],3);
    z[43]=z[43] - 15*z[44];
    z[43]=z[10]*z[43];
    z[45]= - z[5]*z[18];
    z[34]=z[43] + z[41] + z[34] + z[38] + z[45];
    z[34]=z[10]*z[34];
    z[32]= - z[20]*z[32];
    z[32]=z[2] + z[32];
    z[32]=z[13]*z[32];
    z[38]=z[5]*z[2];
    z[41]= - z[16] + z[38];
    z[43]=5*z[4];
    z[41]=z[41]*z[43];
    z[41]=z[41] + 5*z[2] + z[5];
    z[41]=z[5]*z[41];
    z[41]= - z[17] + z[41];
    z[41]=z[4]*z[41];
    z[45]=3*z[2];
    z[41]=z[45] + z[41];
    z[41]=z[4]*z[41];
    z[32]=z[41] + static_cast<T>(1)+ z[32];
    z[41]= - z[5] + 4*z[2];
    z[46]= - z[5]*z[41];
    z[46]=z[16] + z[46];
    z[46]=z[46]*z[43];
    z[47]= - z[33] + z[5];
    z[46]=6*z[47] + z[46];
    z[46]=z[4]*z[46];
    z[46]=static_cast<T>(3)+ z[46];
    z[46]=z[4]*z[46];
    z[47]= - z[40]*z[43];
    z[47]= - static_cast<T>(4)+ z[47];
    z[39]=z[47]*z[39];
    z[47]=z[44]*z[10];
    z[39]=2*z[39] + 5*z[47];
    z[39]=z[10]*z[39];
    z[39]=z[46] + z[39];
    z[39]=z[10]*z[39];
    z[46]=z[22] + 2;
    z[48]= - z[46]*z[25];
    z[49]=npow(z[2],4);
    z[50]=z[49]*z[21];
    z[51]=5*z[16];
    z[50]= - z[51] - 3*z[50];
    z[50]=z[50]*z[23];
    z[32]=z[50] + z[39] + z[48] + 2*z[32];
    z[32]=z[12]*z[32];
    z[39]=z[16]*z[27]*z[43];
    z[48]=z[17] + z[38];
    z[50]=2*z[5];
    z[48]=z[48]*z[50];
    z[39]=z[48] + z[39];
    z[39]=z[4]*z[39];
    z[48]=z[36] - z[33];
    z[52]=z[5]*z[48];
    z[39]=z[39] + z[51] + z[52];
    z[39]=z[12]*z[39];
    z[38]=z[38]*z[4];
    z[51]=z[38] + z[40];
    z[39]=z[39] + 2*z[51];
    z[39]=z[4]*z[39];
    z[51]=2*z[9];
    z[52]=z[2] + z[24];
    z[52]=z[52]*z[51];
    z[53]=z[6]*z[40];
    z[37]=z[52] + 7*z[53] + z[25] + static_cast<T>(1)+ z[37] + z[39];
    z[37]=z[7]*z[37];
    z[39]=2*z[13];
    z[52]=z[39]*z[16];
    z[45]=z[45] + z[52];
    z[53]=3*z[26];
    z[54]=z[25] - static_cast<T>(8)+ z[53];
    z[54]=z[5]*z[54];
    z[55]=z[20]*z[13];
    z[56]=z[16] + z[55];
    z[51]=z[56]*z[51];
    z[56]= - z[12]*z[40]*z[36];
    z[57]=z[27]*z[6];
    z[45]=z[56] + z[51] + 12*z[57] + 2*z[45] + z[54];
    z[45]=z[7]*z[45];
    z[51]=z[2] + z[52];
    z[52]=z[53] + 1;
    z[53]=z[10]*z[13];
    z[54]=z[53] + z[52];
    z[54]=z[10]*z[54];
    z[51]=2*z[51] + z[54];
    z[51]=z[9]*z[51];
    z[54]=z[53] + z[25];
    z[56]= - z[2] + z[36];
    z[56]=3*z[56] - 10*z[57];
    z[56]=z[4]*z[56];
    z[45]=z[45] + z[51] + z[56] + 2*z[52] + 3*z[54];
    z[45]=z[3]*z[45];
    z[48]=3*z[57] - z[48];
    z[48]=z[4]*z[48];
    z[51]= - z[6]*z[36];
    z[48]=z[48] + static_cast<T>(2)+ z[51];
    z[48]=z[4]*z[48];
    z[51]=z[6]*z[5];
    z[52]=9*z[51];
    z[56]=z[9]*z[2];
    z[56]=z[56] - static_cast<T>(5)+ z[52];
    z[56]=z[7]*z[56];
    z[48]=z[56] + 3*z[48] + z[9];
    z[48]=z[3]*z[48];
    z[56]=9*z[6];
    z[52]= - static_cast<T>(14)+ z[52];
    z[52]=z[4]*z[52];
    z[52]= - z[56] + z[52];
    z[52]=z[4]*z[52];
    z[57]= - z[57] + z[40];
    z[43]=z[57]*z[43];
    z[57]= - static_cast<T>(1)+ z[51];
    z[43]=3*z[57] + z[43];
    z[43]=z[4]*z[43];
    z[57]=3*z[6];
    z[43]= - z[57] + z[43];
    z[43]=z[4]*z[43];
    z[58]=z[57] + z[9];
    z[58]=z[7]*z[58];
    z[43]=z[43] + z[58];
    z[43]=z[3]*z[43];
    z[43]=z[43] - 5*z[44];
    z[43]=z[1]*z[43];
    z[44]=z[56] + z[9];
    z[44]=z[7]*z[44];
    z[43]=z[43] + z[48] + z[44] + z[52] + 15*z[47];
    z[43]=z[1]*z[43];
    z[44]=z[55] - z[16];
    z[44]=z[44]*z[13];
    z[47]= - z[13]*z[49];
    z[47]=z[20] + z[47];
    z[47]=z[13]*z[47];
    z[47]= - 4*z[16] + z[47];
    z[47]=z[6]*z[47];
    z[47]=z[47] + 7*z[2] + 3*z[44];
    z[47]=z[13]*z[47];
    z[17]= - z[17] + z[55];
    z[17]=z[13]*z[17];
    z[17]=z[33] + z[17];
    z[17]=z[13]*z[17];
    z[33]= - z[46]*z[53];
    z[17]=z[33] + z[17];
    z[17]=z[6]*z[17];
    z[22]= - static_cast<T>(1)- 3*z[22];
    z[22]=z[13]*z[22];
    z[17]=z[22] + z[17];
    z[17]=z[10]*z[17];
    z[17]=z[17] + static_cast<T>(1)+ z[47];
    z[17]=z[9]*z[17];
    z[22]=z[42]*z[5];
    z[22]=z[22] + z[16];
    z[33]=z[6]*z[22];
    z[46]=z[10]*z[6];
    z[42]=z[42]*z[46];
    z[47]=z[5] + z[2];
    z[33]=z[42] + 3*z[47] - 2*z[33];
    z[33]=z[10]*z[33];
    z[36]= - z[47]*z[36];
    z[22]=z[22]*z[51];
    z[42]=z[16]*z[6];
    z[47]=z[9]*npow(z[10],2)*z[42];
    z[22]=z[47] + z[33] + z[36] + z[22];
    z[22]=z[14]*z[22];
    z[33]=z[46] + 1;
    z[33]=z[2]*z[33];
    z[33]= - z[42] + z[33];
    z[33]=z[9]*z[33];
    z[36]=z[6]*z[2];
    z[36]=z[36] - 1;
    z[33]=z[33] + z[46] - z[36];
    z[33]=z[10]*z[33];
    z[27]=z[16] - z[27];
    z[27]=z[6]*z[27];
    z[22]=z[22] + z[27] - z[2] + z[50] + z[33];
    z[22]=z[14]*z[22];
    z[27]= - z[10]*z[36];
    z[27]=z[27] - z[2] + z[42];
    z[27]=z[9]*z[27];
    z[22]=z[22] + z[29] + z[27] + z[54];
    z[22]=z[22]*z[30];
    z[21]=z[21]*npow(z[2],5);
    z[20]=z[21] + 3*z[20];
    z[20]=z[20]*z[23];
    z[18]=z[49]*z[18];
    z[21]=z[5]*npow(z[26],3);
    z[18]=z[20] + z[21] + 2*z[18];
    z[20]=z[12] + z[6];
    z[18]=z[18]*z[20];
    z[21]= - z[2] + 6*z[5];
    z[21]=z[21]*z[5];
    z[23]=z[3]*npow(z[5],3);
    z[16]=6*z[23] + z[21] + z[16];
    z[16]=z[7]*z[20]*z[16];
    z[16]=z[16] + z[18];
    z[16]=z[15]*z[16];
    z[18]=z[53] + 1;
    z[18]=z[10]*z[18];
    z[18]=z[28] + z[18];
    z[18]=z[3]*z[18];
    z[20]=z[7]*z[9];
    z[21]= - z[1]*z[20];
    z[21]=z[3] + z[21];
    z[21]=z[1]*z[21];
    z[20]= - z[11]*z[20];
    z[20]=z[13] + z[20];
    z[20]=z[11]*z[20];
    z[18]=z[20] + z[21] + z[25] + z[18];
    z[18]=z[8]*z[18];
    z[20]= - z[2] - z[44];
    z[20]=z[20]*z[39];
    z[21]=z[24] - z[2];
    z[23]= - z[13]*z[21];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[25];
    z[20]=z[20] + z[23];
    z[20]=z[6]*z[20];
    z[23]= - z[40]*z[57];
    z[23]= - static_cast<T>(2)+ z[23];
    z[24]=4*z[41] + 15*z[38];
    z[24]=z[4]*z[24];
    z[23]=3*z[23] + z[24];
    z[23]=z[4]*z[23];
    z[21]=z[21]*z[39];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[39];
    z[24]=z[31]*z[35];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[23]
       + z[24] + z[32] + z[34] + z[37] + z[43] + z[45];
 
    return r;
}

template double qg_2lLC_r170(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r170(const std::array<dd_real,31>&);
#endif
