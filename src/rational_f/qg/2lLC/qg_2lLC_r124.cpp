#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r124(const std::array<T,31>& k) {
  T z[59];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[15];
    z[9]=k[5];
    z[10]=k[14];
    z[11]=k[17];
    z[12]=k[3];
    z[13]=k[7];
    z[14]=k[12];
    z[15]=k[16];
    z[16]=z[9]*z[12];
    z[17]=npow(z[12],2);
    z[18]=z[17] - z[16];
    z[19]=z[10]*z[9];
    z[20]=z[17]*z[19];
    z[18]=2*z[18] + z[20];
    z[18]=z[10]*z[18];
    z[20]=2*z[8];
    z[21]= - z[17]*z[20];
    z[22]=npow(z[8],3);
    z[23]=z[22]*z[12];
    z[24]=npow(z[8],2);
    z[25]=2*z[24];
    z[26]=z[23] - z[25];
    z[27]=z[26]*z[16];
    z[28]=z[12]*z[8];
    z[27]=z[28] + z[27];
    z[27]=z[9]*z[27];
    z[18]=z[18] + z[21] + z[27];
    z[18]=z[14]*z[18];
    z[21]=z[10]*npow(z[9],3);
    z[27]=npow(z[9],2);
    z[29]= - z[27] + z[21];
    z[29]=z[10]*z[29];
    z[27]=z[27]*z[10];
    z[30]=2*z[9];
    z[31]=z[30] - z[27];
    z[31]=z[10]*z[31];
    z[31]=static_cast<T>(1)+ z[31];
    z[32]= - static_cast<T>(3)+ z[19];
    z[32]=z[10]*z[32];
    z[32]=z[32] - z[5];
    z[32]=z[2]*z[32];
    z[31]=2*z[31] + z[32];
    z[31]=z[2]*z[31];
    z[29]=z[31] - z[9] + z[29];
    z[29]=z[3]*z[29];
    z[31]=3*z[8];
    z[32]=5*z[24];
    z[33]= - z[12]*z[32];
    z[33]= - z[31] + z[33];
    z[33]=z[9]*z[33];
    z[34]=z[9] - z[12];
    z[35]=3*z[19];
    z[36]= - z[34]*z[35];
    z[36]=z[12] + z[36];
    z[36]=z[10]*z[36];
    z[37]=z[28] + 2;
    z[35]= - static_cast<T>(4)+ z[35];
    z[35]=z[10]*z[35];
    z[35]=7*z[8] + z[35];
    z[35]=z[2]*z[35];
    z[18]=z[29] + z[18] + z[35] + z[36] + z[33] + z[37];
    z[29]=2*z[3];
    z[33]= - z[2]*z[5];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[1]*z[33]*z[29];
    z[18]=z[33] + 2*z[18];
    z[18]=z[4]*z[18];
    z[33]=2*z[6];
    z[35]= - z[33] + z[12];
    z[35]=z[12]*z[35];
    z[36]=2*z[12];
    z[38]=z[36] - z[6];
    z[39]=z[9] - z[38];
    z[39]=z[9]*z[39];
    z[35]=z[35] + z[39];
    z[35]=z[9]*z[35];
    z[39]=z[17]*z[6];
    z[35]=z[39] + z[35];
    z[35]=z[10]*z[35];
    z[40]=npow(z[6],2);
    z[41]=17*z[6] - 7*z[12];
    z[41]=z[12]*z[41];
    z[38]=6*z[38] - 5*z[9];
    z[38]=z[9]*z[38];
    z[35]=2*z[35] + z[38] - 3*z[40] + z[41];
    z[35]=z[10]*z[35];
    z[38]=3*z[6];
    z[41]=z[38] - z[36];
    z[42]=3*z[9];
    z[41]=2*z[41] + z[42];
    z[35]=2*z[41] + z[35];
    z[35]=z[10]*z[35];
    z[41]=z[12]*z[6];
    z[43]=z[41] + z[40];
    z[44]= - z[12]*z[43];
    z[45]=z[17]*z[40];
    z[46]= - z[10]*z[45];
    z[44]=z[44] + z[46];
    z[45]=z[45]*z[5];
    z[44]=2*z[44] - z[45];
    z[44]=z[5]*z[44];
    z[46]=7*z[40] - 11*z[41];
    z[46]=z[10]*z[46];
    z[46]=z[46] - 11*z[6] - 4*z[12];
    z[46]=z[12]*z[46];
    z[44]=2*z[44] + 6*z[40] + z[46];
    z[44]=z[10]*z[44];
    z[46]=z[38]*z[22];
    z[47]=4*z[24];
    z[48]=z[47] - z[46];
    z[49]=z[48]*z[6];
    z[49]=z[49] + 8*z[8];
    z[50]= - z[6]*z[49];
    z[51]=z[25] + z[46];
    z[51]=z[51]*z[6];
    z[52]= - z[51] + 4*z[8];
    z[53]=z[12]*z[52];
    z[50]=z[50] + z[53];
    z[50]=z[12]*z[50];
    z[53]=z[46] - z[24];
    z[54]=z[53]*z[6];
    z[55]=z[31] - z[54];
    z[55]=z[55]*z[40];
    z[44]=z[55] + z[50] + z[44];
    z[44]=z[5]*z[44];
    z[50]=z[22]*z[6];
    z[55]= - z[24] - z[50];
    z[56]=3*z[12];
    z[55]=z[55]*z[56];
    z[49]=z[55] - z[49];
    z[49]=z[12]*z[49];
    z[55]= - z[53]*z[40];
    z[49]=z[55] + z[49];
    z[55]= - z[22]*z[56];
    z[48]=z[55] - z[48];
    z[48]=z[12]*z[48];
    z[48]= - z[54] + z[48];
    z[48]=z[9]*z[48];
    z[35]=z[44] + z[35] + 2*z[49] + z[48];
    z[35]=z[13]*z[35];
    z[44]=5*z[8];
    z[48]=z[47]*z[9];
    z[49]= - z[44] + z[48];
    z[49]=z[49]*z[42];
    z[54]=z[8] - z[48];
    z[54]=z[9]*z[54];
    z[54]= - static_cast<T>(1)+ z[54];
    z[54]=z[54]*z[19];
    z[49]=z[54] - static_cast<T>(10)+ z[49];
    z[49]=z[10]*z[49];
    z[54]=z[30]*z[24];
    z[55]=z[54] - z[8];
    z[57]= - z[55]*z[42];
    z[21]=z[25]*z[21];
    z[21]=z[57] + z[21];
    z[21]=z[10]*z[21];
    z[57]= - z[31] + z[54];
    z[21]=3*z[57] + z[21];
    z[21]=z[10]*z[21];
    z[57]=z[51] - z[8];
    z[58]=z[57]*z[5];
    z[21]= - z[58] - z[25] + z[21];
    z[21]=z[2]*z[21];
    z[46]=z[47] + z[46];
    z[46]=z[6]*z[46];
    z[46]= - z[48] + 10*z[8] + z[46];
    z[48]=z[38]*z[58];
    z[21]=z[21] + z[48] + 2*z[46] + z[49];
    z[21]=z[2]*z[21];
    z[22]= - z[22]*z[40];
    z[22]=z[8] + z[22];
    z[22]=3*z[22] - z[54];
    z[22]=z[9]*z[22];
    z[46]=6*z[50];
    z[48]= - z[32] - z[46];
    z[48]=z[6]*z[48];
    z[48]= - z[31] + z[48];
    z[48]=z[48]*z[33];
    z[49]=z[9]*z[55];
    z[49]= - static_cast<T>(5)+ z[49];
    z[49]=z[9]*z[49];
    z[49]= - z[6] + z[49];
    z[49]=z[10]*z[49];
    z[51]=z[20] - z[51];
    z[51]=z[6]*z[51];
    z[51]=static_cast<T>(1)+ z[51];
    z[51]=z[51]*z[38];
    z[51]=z[51] - z[12];
    z[51]=z[5]*z[51];
    z[21]=z[21] + z[51] + z[49] + z[22] + static_cast<T>(1)+ z[48];
    z[21]=z[7]*z[21];
    z[22]=z[43]*z[36];
    z[22]=z[22] + z[45];
    z[22]=z[5]*z[22];
    z[43]=z[38]*z[12];
    z[48]=z[43] - z[40];
    z[22]=3*z[48] + z[22];
    z[22]=z[5]*z[22];
    z[49]=static_cast<T>(6)+ z[28];
    z[49]=z[12]*z[49];
    z[49]= - z[38] + z[49];
    z[51]= - z[12]*z[25];
    z[44]= - z[44] + z[51];
    z[44]=z[12]*z[44];
    z[44]= - static_cast<T>(3)+ z[44];
    z[44]=z[9]*z[44];
    z[22]=z[22] + 2*z[49] + z[44];
    z[22]=z[13]*z[22];
    z[44]=z[12]*z[48];
    z[44]=z[44] + z[45];
    z[44]=z[5]*z[44];
    z[45]= - z[33] + z[56];
    z[45]=z[12]*z[45];
    z[48]=z[28] + 1;
    z[49]= - z[48]*z[16];
    z[44]=z[44] + z[45] + z[49];
    z[44]=z[13]*z[44];
    z[45]=z[41]*z[5];
    z[49]=z[9]*z[48];
    z[49]=z[45] + z[6] + z[49];
    z[44]=3*z[49] + z[44];
    z[44]=z[11]*z[44];
    z[49]=z[56]*z[24];
    z[49]=z[49] - z[20];
    z[51]=z[9]*z[49];
    z[43]=z[5]*z[43];
    z[43]=z[43] + 14*z[6] - z[56];
    z[43]=z[5]*z[43];
    z[55]= - z[8] - z[5];
    z[55]=z[2]*z[55];
    z[22]=z[44] + 3*z[55] + z[22] + z[43] + z[51] + static_cast<T>(13)+ 6*z[28];
    z[43]=2*z[11];
    z[22]=z[22]*z[43];
    z[44]= - z[12]*z[24];
    z[44]=z[8] + z[44];
    z[16]=z[44]*z[16];
    z[44]=3*z[28];
    z[51]= - static_cast<T>(2)+ z[44];
    z[51]=z[12]*z[51];
    z[16]=z[51] + z[16];
    z[16]=z[9]*z[16];
    z[51]= - z[6] - z[56];
    z[51]=z[12]*z[51];
    z[39]= - z[5]*z[39];
    z[16]=z[39] + z[51] + z[16];
    z[16]=z[11]*z[16];
    z[39]=z[48]*z[56];
    z[26]= - z[12]*z[26];
    z[26]= - z[20] + z[26];
    z[26]=z[9]*z[26];
    z[26]=z[26] + static_cast<T>(4)- z[44];
    z[26]=z[9]*z[26];
    z[44]=z[12] - z[6];
    z[44]=z[44]*z[12];
    z[48]= - z[5]*z[44];
    z[16]=z[16] + z[48] + z[26] + z[33] + z[39];
    z[16]=z[16]*z[43];
    z[26]= - z[40]*z[20];
    z[39]=z[6]*z[8];
    z[48]= - z[39] + z[28];
    z[48]=z[12]*z[48];
    z[17]=z[17]*z[10];
    z[51]=z[6]*z[17];
    z[44]=z[44] + z[51];
    z[44]=z[10]*z[44];
    z[26]=z[44] + z[26] + z[48];
    z[26]=z[5]*z[26];
    z[44]= - static_cast<T>(3)+ z[28];
    z[44]=z[12]*z[44];
    z[48]= - z[9]*z[28];
    z[17]=2*z[17] + z[44] + z[48];
    z[17]=z[10]*z[17];
    z[44]=z[9]*z[8];
    z[48]= - static_cast<T>(1)- 5*z[39];
    z[16]=z[16] + z[26] + z[17] - 10*z[44] + 2*z[48] - 7*z[28];
    z[16]=z[14]*z[16];
    z[17]=z[24]*z[9];
    z[26]=z[8] - z[17];
    z[26]=z[26]*z[19];
    z[26]=z[26] - z[20] + 3*z[17];
    z[26]=z[10]*z[26];
    z[48]=z[6]*z[24];
    z[20]=z[20] + z[48];
    z[20]=z[5]*z[20];
    z[20]=z[20] - z[25] + z[26];
    z[20]=z[2]*z[20];
    z[25]= - static_cast<T>(1)- z[44];
    z[25]=z[25]*z[19];
    z[25]=z[25] + static_cast<T>(2)+ z[44];
    z[25]=z[10]*z[25];
    z[26]= - static_cast<T>(1)- z[39];
    z[26]=z[5]*z[26];
    z[25]=z[25] + z[26];
    z[20]=2*z[25] + z[20];
    z[20]=z[2]*z[20];
    z[25]=z[7] - z[11];
    z[26]=z[9]*z[6];
    z[25]=z[29]*z[26]*z[25];
    z[29]=z[6] + z[30];
    z[29]=z[29]*z[19];
    z[29]= - z[30] + z[29];
    z[29]=z[10]*z[29];
    z[30]=5*z[6];
    z[39]= - 8*z[2] + z[30] + z[42];
    z[39]=z[39]*z[43];
    z[30]=4*z[2] - z[30] - z[9];
    z[30]=z[7]*z[30];
    z[20]=z[25] + z[39] + 2*z[30] + z[29] + z[20];
    z[20]=z[3]*z[20];
    z[25]=z[54] + z[49];
    z[25]=z[9]*z[25];
    z[29]=4*z[37] - z[44];
    z[29]=z[9]*z[29];
    z[19]=z[34]*z[19];
    z[19]= - 6*z[19] + z[29] + 7*z[6] - 13*z[12];
    z[19]=z[10]*z[19];
    z[19]=z[19] + z[25] - static_cast<T>(18)+ z[28];
    z[19]=z[10]*z[19];
    z[25]=z[41]*z[10];
    z[25]= - 6*z[45] - 10*z[25] - 19*z[6] + z[36];
    z[25]=z[10]*z[25];
    z[28]=z[53]*z[38];
    z[28]= - 16*z[8] + z[28];
    z[28]=z[6]*z[28];
    z[29]=9*z[50];
    z[30]= - z[32] - z[29];
    z[30]=z[6]*z[30];
    z[30]=z[31] + z[30];
    z[30]=z[12]*z[30];
    z[25]=z[30] - static_cast<T>(2)+ z[28] + z[25];
    z[25]=z[5]*z[25];
    z[28]=12*z[8] - 5*z[17];
    z[28]=z[9]*z[28];
    z[27]=z[8]*z[27];
    z[27]= - 6*z[27] - static_cast<T>(8)+ z[28];
    z[27]=z[10]*z[27];
    z[28]=11*z[8];
    z[17]=z[27] - z[28] + 12*z[17];
    z[17]=z[10]*z[17];
    z[27]= - z[47] - z[29];
    z[27]=z[6]*z[27];
    z[27]=5*z[10] + z[8] + z[27];
    z[27]=z[5]*z[27];
    z[17]=z[27] + z[17] - 7*z[24] - z[46];
    z[17]=z[2]*z[17];
    z[23]= - z[23] - z[24] + z[50];
    z[23]=z[23]*z[42];
    z[26]=z[26] + 2*z[40];
    z[26]=z[26]*z[57];
    z[27]=z[52]*z[6];
    z[27]=z[27] + 3;
    z[27]=z[27]*z[40];
    z[27]=z[27] - z[41];
    z[27]=z[27]*z[5];
    z[26]=z[27] - z[26];
    z[27]= - z[13] - z[7];
    z[26]=z[15]*z[26]*z[27];
    z[27]= - z[24] + z[46];
    z[27]=z[27]*z[33];
    z[24]= - 3*z[24] - 4*z[50];
    z[24]=z[24]*z[56];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[23]
       + z[24] + z[25] + z[26] + z[27] - z[28] + z[35];
 
    return r;
}

template double qg_2lLC_r124(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r124(const std::array<dd_real,31>&);
#endif
