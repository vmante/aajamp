#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r3(const std::array<T,31>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[8];
    z[7]=k[3];
    z[8]=k[6];
    z[9]=k[4];
    z[10]=2*z[6];
    z[11]= - z[3]*z[9];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[11]*z[10];
    z[12]=z[3]*z[2];
    z[13]=z[12] - 1;
    z[14]=z[2]*z[10];
    z[14]=z[14] + z[13];
    z[14]=z[4]*z[14];
    z[11]=z[11] + z[14];
    z[11]=z[8]*z[11];
    z[14]=z[2] - z[9];
    z[15]= - z[9]*z[6]*z[2];
    z[15]=z[15] + z[7] - z[14];
    z[15]=z[8]*z[15];
    z[13]=z[4]*z[1]*z[13];
    z[13]=z[15] - static_cast<T>(2)+ z[13];
    z[13]=z[5]*z[13];
    z[15]=z[3]*z[1];
    z[16]=static_cast<T>(1)+ z[15];
    z[16]=z[4]*z[16];
    z[14]= - z[6]*z[14];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[8]*z[14];
    z[13]=z[13] + z[14] + z[16] + z[3] - z[6];
    z[13]=z[5]*z[13];
    z[14]=z[10]*z[3];
    z[15]=static_cast<T>(2)+ z[15];
    z[15]=z[6]*z[15];
    z[15]=z[15] - 2*z[3];
    z[15]=z[4]*z[15];
    z[11]=2*z[13] + z[11] + z[14] + z[15];
    z[11]=z[5]*z[11];
    z[10]= - z[10]*z[12];
    z[10]=z[3] + z[10];
    z[10]=z[4]*z[10];
    z[10]= - z[14] + z[10];
    z[10]=z[8]*z[10];
    z[12]= - z[4]*z[14];

    r += z[10] + z[11] + z[12];
 
    return r;
}

template double qg_2lLC_r3(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r3(const std::array<dd_real,31>&);
#endif
