#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r39(const std::array<T,31>& k) {
  T z[64];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[8];
    z[7]=k[4];
    z[8]=k[10];
    z[9]=k[2];
    z[10]=k[6];
    z[11]=k[15];
    z[12]=k[14];
    z[13]=k[3];
    z[14]=k[16];
    z[15]=4*z[2];
    z[16]=8*z[2];
    z[17]= - 9*z[7] + z[16];
    z[17]=z[10]*z[17]*z[15];
    z[18]=npow(z[2],3);
    z[19]=z[18]*z[10];
    z[20]=npow(z[2],2);
    z[21]= - 2*z[20] - z[19];
    z[21]=z[3]*z[21];
    z[22]=z[2]*z[7];
    z[23]=2*z[22];
    z[24]=z[20]*z[10];
    z[25]= - z[7]*z[24];
    z[25]= - z[23] + z[25];
    z[26]=16*z[4];
    z[25]=z[25]*z[26];
    z[17]=z[25] + 20*z[21] + z[17] - 42*z[7] + 53*z[2];
    z[17]=z[4]*z[17];
    z[21]=6*z[5];
    z[25]= - z[22]*z[21];
    z[27]=8*z[7];
    z[28]=5*z[2];
    z[25]=z[25] + z[27] - z[28];
    z[25]=z[5]*z[25];
    z[29]=npow(z[7],2);
    z[30]=3*z[29];
    z[31]=z[30] - 4*z[22];
    z[32]=z[5]*z[2];
    z[33]=6*z[32];
    z[31]=z[31]*z[33];
    z[34]=17*z[7] - 32*z[2];
    z[34]=z[2]*z[34];
    z[31]=z[31] - 16*z[29] + z[34];
    z[31]=z[5]*z[31];
    z[31]= - 3*z[7] + z[31];
    z[31]=z[10]*z[31];
    z[34]= - z[29] + z[23];
    z[35]=2*z[5];
    z[34]=z[34]*z[35];
    z[36]=5*z[7];
    z[34]=z[34] + z[36] - z[15];
    z[34]=z[34]*z[32];
    z[37]=3*z[2];
    z[34]= - z[37] + z[34];
    z[38]=9*z[12];
    z[34]=z[34]*z[38];
    z[39]=2*z[2];
    z[40]=z[7] - z[28];
    z[40]=z[40]*z[39];
    z[40]= - 5*z[29] + z[40];
    z[41]=z[29]*z[2];
    z[42]=3*z[10];
    z[43]=z[42]*z[41];
    z[40]=2*z[40] + z[43];
    z[40]=z[10]*z[40];
    z[43]=9*z[2];
    z[44]=10*z[7];
    z[45]=z[44] - z[43];
    z[40]=3*z[45] + z[40];
    z[40]=z[3]*z[40];
    z[45]=2*z[29];
    z[46]=z[29]*z[10];
    z[47]= - z[2]*z[46];
    z[47]=z[47] + z[45] + z[22];
    z[47]=z[10]*z[47];
    z[32]= - z[32] + 1;
    z[32]=z[29]*z[32];
    z[32]=z[22] + z[32];
    z[32]=z[32]*z[35];
    z[32]=z[47] + z[7] + z[32];
    z[47]=3*z[14];
    z[32]=z[32]*z[47];
    z[17]=z[32] + z[17] + z[40] + z[34] + z[31] + static_cast<T>(18)+ z[25];
    z[17]=z[6]*z[17];
    z[25]=z[24] + z[2];
    z[31]= - z[13]*z[25];
    z[31]=z[31] + 5*z[20] + z[19];
    z[31]=z[10]*z[31];
    z[32]=npow(z[7],3);
    z[34]=z[41] + z[32];
    z[40]= - z[5]*z[34];
    z[40]=z[40] - z[29] + z[22];
    z[40]=z[40]*z[35];
    z[31]=z[40] - z[7] + z[15] + z[31];
    z[31]=z[4]*z[31];
    z[40]=z[10]*z[34];
    z[40]=z[40] + z[45] - z[22];
    z[40]=z[10]*z[40];
    z[41]=4*z[20];
    z[48]=z[18]*z[5];
    z[49]= - z[41] - z[48];
    z[49]=z[5]*z[49];
    z[49]=z[49] + z[7] - z[37];
    z[40]=2*z[49] + z[40];
    z[40]=z[11]*z[40];
    z[49]=npow(z[5],2);
    z[50]=2*z[49];
    z[51]= - npow(z[10],2);
    z[51]=z[50] + z[51];
    z[52]=z[2] - z[7];
    z[51]=z[52]*z[51];
    z[53]=z[20]*z[5];
    z[54]=z[2] + z[53];
    z[54]=z[11]*z[54]*z[35];
    z[51]=z[54] + z[51];
    z[51]=z[13]*z[51];
    z[54]=z[35]*z[29];
    z[55]=z[29]*z[5];
    z[56]=z[7] + z[55];
    z[56]=z[56]*z[35];
    z[57]=z[46] + z[7];
    z[58]=z[10]*z[57];
    z[56]=z[56] + z[58];
    z[56]=z[13]*z[56];
    z[56]=z[56] + 2*z[46] - z[7] + z[54];
    z[56]=z[8]*z[56];
    z[58]=z[52]*z[2];
    z[59]=z[58] + z[29];
    z[60]= - z[5]*z[59];
    z[60]=z[60] + 2*z[7] - z[37];
    z[60]=z[60]*z[35];
    z[59]=z[59]*z[10];
    z[61]=z[59] + z[7] + z[15];
    z[61]=z[10]*z[61];
    z[31]=z[56] + z[31] + z[51] + z[40] + z[60] + z[61];
    z[31]=z[31]*z[47];
    z[40]= - z[52]*z[35];
    z[40]=static_cast<T>(9)+ z[40];
    z[40]=z[5]*z[40];
    z[47]=8*z[5];
    z[51]=z[52]*z[47];
    z[51]=static_cast<T>(3)+ z[51];
    z[51]=z[12]*z[51];
    z[40]=z[40] + 4*z[51];
    z[51]=3*z[12];
    z[40]=z[40]*z[51];
    z[56]=z[10]*z[52];
    z[56]=static_cast<T>(1)+ z[56];
    z[56]=z[56]*z[42];
    z[60]=z[7] + z[39];
    z[60]=z[60]*z[51];
    z[60]= - static_cast<T>(16)+ z[60];
    z[61]=2*z[12];
    z[60]=z[60]*z[61];
    z[56]=z[56] + z[60];
    z[56]=z[3]*z[56];
    z[60]= - z[20]*z[35];
    z[60]= - 23*z[2] + z[60];
    z[62]=3*z[5];
    z[60]=z[60]*z[62];
    z[37]=z[37] + 8*z[53];
    z[63]=12*z[12];
    z[37]=z[37]*z[63];
    z[37]=z[37] - static_cast<T>(16)+ z[60];
    z[37]=z[11]*z[12]*z[37];
    z[33]= - z[11]*z[33];
    z[33]=z[33] - z[3];
    z[60]=npow(z[12],2);
    z[33]=z[60]*z[33];
    z[60]= - z[60]*z[62];
    z[50]= - z[10]*z[50];
    z[50]=z[50] + z[60];
    z[33]=2*z[50] + z[33];
    z[33]=z[13]*z[33];
    z[50]=7*z[7];
    z[16]= - z[50] + z[16];
    z[16]=z[16]*z[62];
    z[16]=static_cast<T>(10)+ z[16];
    z[16]=z[10]*z[5]*z[16];
    z[16]=9*z[49] + z[16];
    z[16]=6*z[33] + z[37] + z[56] + 2*z[16] + z[40];
    z[16]=z[13]*z[16];
    z[33]= - z[50] - z[15];
    z[33]=z[2]*z[33];
    z[23]=z[30] + z[23];
    z[23]=z[2]*z[23];
    z[23]=z[32] + z[23];
    z[23]=z[23]*z[35];
    z[23]=z[23] - z[30] + z[33];
    z[23]=z[5]*z[23];
    z[33]=z[2] + z[7];
    z[23]=z[23] + z[33];
    z[23]=z[23]*z[38];
    z[19]= - z[41] - z[19];
    z[19]=z[19]*z[42];
    z[19]=z[19] + z[43] + 32*z[53];
    z[19]=z[3]*z[19];
    z[37]=4*z[11];
    z[38]=z[3]*z[10]*z[25];
    z[38]=z[38] - z[37];
    z[38]=z[13]*z[38];
    z[40]= - z[36] - z[2];
    z[40]=z[11]*z[40];
    z[38]=z[40] + z[38];
    z[40]= - z[29] - 3*z[22];
    z[40]=z[5]*z[40];
    z[40]=z[40] + z[33];
    z[40]=z[5]*z[40];
    z[25]= - z[25]*z[26];
    z[41]=z[10]*z[2];
    z[19]=z[25] + z[19] + z[23] - 32*z[41] - static_cast<T>(13)+ 12*z[40] + 3*z[38];
    z[19]=z[4]*z[19];
    z[23]=z[46] - z[7];
    z[25]= - z[23]*z[42];
    z[38]= - z[12]*z[30];
    z[38]= - z[44] + z[38];
    z[38]=z[38]*z[61];
    z[25]=z[25] + z[38];
    z[25]=z[3]*z[25];
    z[38]= - z[36] - z[54];
    z[38]=z[38]*z[62];
    z[40]= - z[7] + z[55];
    z[40]=z[12]*z[40];
    z[38]=24*z[40] + static_cast<T>(47)+ z[38];
    z[38]=z[12]*z[38];
    z[40]=z[12]*z[7];
    z[40]= - static_cast<T>(1)+ z[40];
    z[40]=z[5]*z[40];
    z[43]= - z[7]*z[51];
    z[43]=static_cast<T>(5)+ z[43];
    z[43]=z[3]*z[43];
    z[40]=18*z[40] + z[43];
    z[40]=z[12]*z[40];
    z[43]=5*z[3];
    z[50]= - z[11]*z[43];
    z[40]=z[50] + z[40];
    z[40]=z[13]*z[40];
    z[50]= - z[62] - z[10];
    z[44]=z[3]*z[44];
    z[44]= - static_cast<T>(9)+ z[44];
    z[44]=z[11]*z[44];
    z[25]=2*z[40] + z[44] + z[25] + 3*z[50] + z[38];
    z[25]=z[13]*z[25];
    z[38]= - z[29]*z[42];
    z[38]=z[7] + z[38];
    z[32]=z[32]*z[10];
    z[32]=z[32] - z[29];
    z[40]=z[32]*z[43];
    z[38]=2*z[38] + z[40];
    z[37]=z[38]*z[37];
    z[32]= - z[11]*z[32];
    z[38]=z[11]*z[7];
    z[40]=z[38]*z[13];
    z[32]= - z[40] - z[46] + z[32];
    z[44]=16*z[8];
    z[32]=z[32]*z[44];
    z[46]=13*z[29];
    z[50]=z[12]*z[46];
    z[50]= - 3*z[57] + z[50];
    z[50]=z[3]*z[50];
    z[53]= - z[11]*z[29];
    z[40]= - z[40] - z[7] + z[53];
    z[40]=z[4]*z[40];
    z[53]= - 37*z[7] - 6*z[55];
    z[53]=z[12]*z[53];
    z[25]=z[32] + 6*z[40] + z[25] + z[37] + 2*z[50] + static_cast<T>(38)+ z[53];
    z[25]=z[8]*z[25];
    z[32]=z[42]*z[2];
    z[37]=z[12]*z[32];
    z[37]= - 7*z[10] + z[37];
    z[37]=z[11]*z[37]*z[61];
    z[40]=z[11]*z[42];
    z[38]= - static_cast<T>(1)- z[38];
    z[38]=z[8]*z[10]*z[38];
    z[38]=z[40] + 4*z[38];
    z[38]=z[8]*z[38];
    z[37]=z[37] + z[38];
    z[37]=z[9]*z[37];
    z[38]=z[10]*z[7];
    z[23]=z[11]*z[23];
    z[23]=z[38] + z[23];
    z[23]= - static_cast<T>(1)+ 2*z[23];
    z[23]=z[23]*z[44];
    z[38]=static_cast<T>(13)- 8*z[38];
    z[38]=z[11]*z[38];
    z[23]= - 3*z[6] + z[23] + 8*z[12] + z[38];
    z[23]=z[8]*z[23];
    z[38]=6*z[12];
    z[32]= - static_cast<T>(8)+ z[32];
    z[32]=z[32]*z[38];
    z[32]= - 97*z[10] + z[32];
    z[32]=z[12]*z[32];
    z[40]=z[20]*z[42];
    z[28]= - z[28] + z[40];
    z[28]=z[28]*z[38];
    z[28]=z[28] - static_cast<T>(11)- 101*z[41];
    z[28]=z[12]*z[28];
    z[28]=123*z[10] + z[28];
    z[28]=z[11]*z[28];
    z[23]=4*z[37] + z[32] + z[28] + z[23];
    z[23]=z[9]*z[23];
    z[28]=7*z[29] + z[58];
    z[28]=z[28]*z[35];
    z[32]=19*z[7];
    z[28]=z[28] - z[32] + 15*z[2];
    z[28]=z[5]*z[28];
    z[28]= - static_cast<T>(10)+ z[28];
    z[37]=2*z[10];
    z[32]=z[32] - 12*z[2];
    z[32]=z[32]*z[37];
    z[37]= - z[45] - 5*z[58];
    z[37]=z[5]*z[37];
    z[37]= - 5*z[52] + z[37];
    z[37]=z[37]*z[63];
    z[28]=z[37] + 3*z[28] + z[32];
    z[28]=z[12]*z[28];
    z[32]=z[36] + z[15];
    z[29]=z[29] - z[20];
    z[29]=z[29]*z[51];
    z[29]=4*z[32] + z[29];
    z[29]=z[29]*z[61];
    z[27]= - z[27] + 11*z[2];
    z[27]=z[27]*z[35];
    z[32]=z[7] - z[39];
    z[32]=2*z[32] - z[59];
    z[32]=z[32]*z[42];
    z[27]=z[29] + z[32] - static_cast<T>(1)+ z[27];
    z[27]=z[3]*z[27];
    z[18]=z[18]*z[35];
    z[18]=35*z[20] + z[18];
    z[18]=z[18]*z[62];
    z[29]= - z[20] - z[48];
    z[29]=z[12]*z[29];
    z[18]=60*z[29] - 34*z[24] + 71*z[2] + z[18];
    z[18]=z[12]*z[18];
    z[22]=z[46] + 8*z[22];
    z[24]= - z[34]*z[42];
    z[22]=2*z[22] + z[24];
    z[22]=z[10]*z[22];
    z[22]=z[22] - 23*z[7] - 13*z[2];
    z[22]=z[3]*z[22];
    z[24]=z[62]*z[2];
    z[29]=static_cast<T>(2)- z[24];
    z[32]= - 99*z[7] + 14*z[2];
    z[32]=z[10]*z[32];
    z[18]=z[22] + z[18] + 9*z[29] + z[32];
    z[18]=z[11]*z[18];
    z[22]=static_cast<T>(5)+ z[24];
    z[22]=z[3]*z[5]*z[22];
    z[29]= - z[6]*z[43];
    z[22]=z[29] - 3*z[49] + z[22];
    z[22]=z[1]*z[22];
    z[29]=z[33]*z[35];
    z[32]=static_cast<T>(1)+ z[29];
    z[32]=z[32]*z[62];
    z[20]= - z[20]*z[62];
    z[20]=z[15] + z[20];
    z[33]=z[35]*z[3];
    z[20]=z[20]*z[33];
    z[20]=2*z[22] + z[32] + z[20];
    z[20]=z[4]*z[20];
    z[22]=z[3]*z[2];
    z[22]=static_cast<T>(7)- 10*z[22];
    z[26]= - z[7]*z[26];
    z[22]=3*z[22] + z[26];
    z[22]=z[4]*z[22];
    z[26]= - z[47] - 15*z[3];
    z[32]= - z[4]*z[7];
    z[32]= - static_cast<T>(2)+ z[32];
    z[32]=z[8]*z[32];
    z[22]=6*z[32] + 2*z[26] + z[22];
    z[22]=z[6]*z[22];
    z[24]=static_cast<T>(4)- z[24];
    z[24]=z[24]*z[33];
    z[20]=z[22] + z[24] + z[20];
    z[20]=z[1]*z[20];
    z[15]=11*z[7] - z[15];
    z[15]=z[2]*z[15];
    z[15]= - z[30] + z[15];
    z[15]=z[15]*z[21];
    z[15]=z[15] - z[36] + z[39];
    z[15]=z[5]*z[15];
    z[15]= - static_cast<T>(18)+ z[15];
    z[15]=z[10]*z[15];
    z[21]=static_cast<T>(3)- z[29];
    z[21]=z[5]*z[21];

    r += z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + 9*z[21] + 
      z[23] + z[25] + z[27] + z[28] + z[31];
 
    return r;
}

template double qg_2lLC_r39(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r39(const std::array<dd_real,31>&);
#endif
