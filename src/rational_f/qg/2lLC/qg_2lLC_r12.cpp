#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r12(const std::array<T,31>& k) {
  T z[51];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[12];
    z[6]=k[5];
    z[7]=k[7];
    z[8]=k[18];
    z[9]=k[2];
    z[10]=k[9];
    z[11]=k[3];
    z[12]=k[6];
    z[13]=k[15];
    z[14]=k[14];
    z[15]=k[25];
    z[16]=k[13];
    z[17]=2*z[2];
    z[18]=npow(z[2],2);
    z[19]=z[18]*z[7];
    z[20]=z[17] - z[19];
    z[21]=z[19] - z[2];
    z[22]=z[6]*z[7];
    z[23]= - z[21]*z[22];
    z[23]=z[23] + z[20];
    z[23]=z[12]*z[23];
    z[24]=npow(z[7],2);
    z[25]=z[24]*z[6];
    z[26]=z[7] - 2*z[25];
    z[26]=z[6]*z[26];
    z[27]=z[7] - z[25];
    z[27]=z[1]*z[27];
    z[26]=z[27] + z[26];
    z[26]=z[5]*z[26];
    z[27]=2*z[4];
    z[26]= - z[27] + z[7] - 3*z[25] + z[26];
    z[26]=z[1]*z[26];
    z[28]=z[7]*z[2];
    z[29]= - static_cast<T>(2)+ 3*z[28];
    z[29]=z[7]*z[29];
    z[29]=z[29] - z[25];
    z[29]=z[6]*z[29];
    z[30]= - z[5]*z[24]*npow(z[6],3);
    z[31]=z[4]*z[17];
    z[27]=z[27] + z[12];
    z[27]=z[11]*z[27];
    z[23]=z[27] + z[26] + z[23] + z[31] + z[30] + z[29] + static_cast<T>(1)+ z[28];
    z[23]=z[8]*z[23];
    z[26]=z[22] - 2;
    z[27]=z[3]*z[2];
    z[29]=z[27] - z[26];
    z[30]=npow(z[6],2);
    z[31]=z[30]*z[12];
    z[29]=z[29]*z[31];
    z[31]=z[7]*z[31];
    z[31]= - z[22] + z[31];
    z[31]=z[11]*z[31];
    z[32]=z[6]*z[27];
    z[29]=z[31] + z[32] + z[29];
    z[29]=z[12]*z[29];
    z[31]=z[6]*z[3];
    z[29]=static_cast<T>(2)- z[31] + z[29];
    z[29]=z[15]*z[29];
    z[23]=z[23] + z[29];
    z[29]=19*z[7];
    z[32]=npow(z[2],3);
    z[33]= - z[32]*z[29];
    z[33]=65*z[18] + z[33];
    z[34]=2*z[7];
    z[33]=z[33]*z[34];
    z[35]=z[29]*z[18];
    z[36]=16*z[2] - z[35];
    z[36]=z[36]*z[22];
    z[37]=6*z[32];
    z[38]=npow(z[2],4);
    z[39]=z[38]*z[7];
    z[40]=z[37] - z[39];
    z[40]=z[40]*z[29];
    z[41]=155*z[18];
    z[40]= - z[41] + z[40];
    z[40]=z[4]*z[40];
    z[33]=z[40] + z[36] - 67*z[2] + z[33];
    z[33]=z[13]*z[33];
    z[36]=2*z[32];
    z[39]=z[36] - z[39];
    z[39]=z[4]*z[39];
    z[40]=z[32]*z[7];
    z[39]=z[39] + z[18] - z[40];
    z[39]=z[13]*z[39];
    z[42]=z[22] - z[28];
    z[42]= - z[6]*z[42];
    z[21]=z[39] + z[42] - z[21];
    z[39]=3*z[22];
    z[42]= - z[39] - static_cast<T>(13)- 16*z[27];
    z[43]=z[30]*z[5];
    z[42]=z[42]*z[43];
    z[44]=2*z[18] - z[40];
    z[45]=19*z[4];
    z[44]=z[44]*z[45];
    z[21]=z[44] + z[42] + 19*z[21];
    z[21]=z[12]*z[21];
    z[42]=z[27] + 1;
    z[44]=19*z[5];
    z[30]=z[42]*z[30]*z[44];
    z[27]=static_cast<T>(11)- 4*z[27];
    z[27]=4*z[27] - z[39];
    z[27]=z[6]*z[27];
    z[27]=z[27] + z[30];
    z[27]=z[5]*z[27];
    z[30]=z[18]*z[3];
    z[39]=3*z[30];
    z[46]= - z[24]*z[39];
    z[46]= - z[29] + z[46];
    z[46]=z[6]*z[46];
    z[47]=5*z[18];
    z[40]=z[40] - z[47];
    z[29]= - z[40]*z[29];
    z[29]= - 117*z[2] + z[29];
    z[29]=z[4]*z[29];
    z[21]=z[21] + z[33] + z[29] + z[27] + z[46] + static_cast<T>(6)+ 35*z[28];
    z[21]=z[12]*z[21];
    z[27]=4*z[2];
    z[29]= - z[6]*z[42];
    z[29]=z[29] - z[27] - z[39];
    z[29]=z[6]*z[29];
    z[33]=z[32]*z[3];
    z[29]=z[29] - z[47] - 3*z[33];
    z[29]=z[29]*z[44];
    z[42]=19*z[3];
    z[32]= - z[32]*z[42];
    z[32]= - 106*z[18] + z[32];
    z[32]=z[3]*z[32];
    z[46]=z[42]*z[18];
    z[48]= - 98*z[2] - z[46];
    z[48]=z[3]*z[48];
    z[48]= - static_cast<T>(88)+ z[48];
    z[48]=z[6]*z[48];
    z[29]=z[29] + z[48] - 243*z[2] + 2*z[32];
    z[29]=z[5]*z[29];
    z[32]=z[7]*z[40];
    z[40]=z[33] + z[47];
    z[47]= - z[3]*z[40];
    z[32]=z[32] + z[47];
    z[38]=z[38]*z[3];
    z[37]= - z[37] - z[38];
    z[37]=z[37]*z[42];
    z[36]= - z[36] - z[38];
    z[36]=z[36]*z[44];
    z[36]=z[36] - z[41] + z[37];
    z[36]=z[5]*z[36];
    z[32]=19*z[32] + z[36];
    z[32]=z[4]*z[32];
    z[36]=z[46] + 79*z[2];
    z[36]=z[36]*z[3];
    z[37]=19*z[28];
    z[38]= - static_cast<T>(16)+ z[37];
    z[38]=z[38]*z[22];
    z[19]= - 111*z[2] + 38*z[19];
    z[19]=z[7]*z[19];
    z[19]=z[32] + z[29] + z[38] - z[36] - static_cast<T>(12)+ z[19];
    z[19]=z[13]*z[19];
    z[29]=60*z[2];
    z[32]=z[29] + z[46];
    z[32]=z[32]*z[3];
    z[38]= - z[5]*z[46];
    z[38]= - z[32] + z[38];
    z[38]=z[5]*z[38];
    z[41]=z[42]*z[2];
    z[46]= - static_cast<T>(44)- z[41];
    z[46]=z[3]*z[46];
    z[38]=z[46] + z[38];
    z[38]=z[4]*z[38];
    z[46]= - z[5]*z[2];
    z[46]= - static_cast<T>(1)+ z[46];
    z[47]=npow(z[3],2);
    z[46]=z[45]*z[47]*z[46];
    z[48]=z[42]*z[7];
    z[48]= - z[48] + 16*z[24];
    z[48]=z[48]*z[31];
    z[49]= - 16*z[7] + z[42];
    z[49]=z[3]*z[49];
    z[49]=z[49] + z[48];
    z[49]=z[5]*z[49];
    z[46]=z[49] + z[46];
    z[46]=z[1]*z[46];
    z[49]=z[3]*z[7];
    z[49]=29*z[24] - 38*z[49];
    z[31]=z[49]*z[31];
    z[49]= - 5*z[7] + z[42];
    z[49]=z[3]*z[49];
    z[49]=z[49] + z[48];
    z[49]=z[6]*z[49];
    z[50]= - static_cast<T>(11)- z[41];
    z[50]=z[3]*z[50];
    z[49]=z[50] + z[49];
    z[49]=z[5]*z[49];
    z[50]=28*z[7] + z[42];
    z[50]=z[3]*z[50];
    z[31]=z[46] + z[38] + 2*z[49] + z[50] + z[31];
    z[31]=z[1]*z[31];
    z[35]=z[29] - z[35];
    z[35]=z[7]*z[35];
    z[38]= - z[18]*z[44];
    z[38]= - z[29] + z[38];
    z[38]=z[5]*z[38];
    z[35]=z[38] - static_cast<T>(152)+ z[35];
    z[35]=z[4]*z[35];
    z[37]=static_cast<T>(30)- z[37];
    z[37]=z[37]*z[34];
    z[38]= - z[17] - z[6];
    z[38]=z[38]*z[44];
    z[38]= - static_cast<T>(66)+ z[38];
    z[38]=z[5]*z[38];
    z[25]=z[35] + z[38] + z[37] - 19*z[25];
    z[25]=z[13]*z[25];
    z[35]=z[28] - 1;
    z[22]=z[22] - z[35];
    z[37]=3*z[7];
    z[38]=z[43]*z[37];
    z[20]= - z[20]*z[45];
    z[20]=z[20] + 19*z[22] + z[38];
    z[20]=z[12]*z[20];
    z[22]=z[4]*z[35];
    z[20]= - 16*z[22] + z[20];
    z[20]=z[12]*z[20];
    z[20]=z[25] + z[20];
    z[20]=z[11]*z[20];
    z[22]= - z[4]*z[28];
    z[22]= - z[7] + z[22];
    z[22]=z[11]*z[22];
    z[25]=z[13]*z[6];
    z[35]=z[9]*z[12]*z[25];
    z[22]=z[35] + z[22] + z[28] + z[26];
    z[22]=z[14]*z[22];
    z[26]=z[1]*z[3];
    z[35]= - z[11]*z[5]*z[25];
    z[38]=static_cast<T>(1)- z[26];
    z[38]=z[9]*z[4]*z[38];
    z[26]=z[38] + z[35] + static_cast<T>(1)+ z[26];
    z[26]=z[10]*z[26];
    z[22]=z[22] + z[26];
    z[26]=6*z[7] + z[42];
    z[26]=z[3]*z[26];
    z[26]=z[26] + z[48];
    z[26]=z[6]*z[26];
    z[35]= - static_cast<T>(22)- z[41];
    z[35]=z[3]*z[35];
    z[26]=z[26] - z[37] + z[35];
    z[26]=z[6]*z[26];
    z[35]= - static_cast<T>(14)- z[36];
    z[17]= - z[17] - z[39];
    z[17]=z[17]*z[44];
    z[17]=z[17] + 2*z[35] + z[26];
    z[17]=z[5]*z[17];
    z[26]= - static_cast<T>(152)- z[32];
    z[26]=z[13]*z[26];
    z[32]= - static_cast<T>(41)- z[41];
    z[32]=z[3]*z[32];
    z[26]=z[32] + z[26];
    z[26]=z[4]*z[26];
    z[32]= - z[1]*z[47];
    z[35]=z[11]*npow(z[12],2);
    z[32]=z[35] + z[32];
    z[32]=z[45]*z[32];
    z[35]= - z[13]*z[18];
    z[35]= - z[2] + z[35];
    z[35]=z[12]*z[35];
    z[35]=19*z[35] + 41;
    z[35]=z[4]*z[35];
    z[36]=z[4]*z[2];
    z[36]= - static_cast<T>(1)+ 10*z[36];
    z[36]=z[13]*z[36];
    z[35]=6*z[36] + z[35];
    z[35]=z[12]*z[35];
    z[26]=z[35] + z[32] + z[26];
    z[26]=z[9]*z[26];
    z[32]=static_cast<T>(11)- 5*z[28];
    z[32]=z[32]*z[34];
    z[34]=z[42]*z[28];
    z[32]=z[32] + z[34];
    z[32]=z[3]*z[32];
    z[24]=z[48] - 19*z[24] + z[32];
    z[24]=z[6]*z[24];
    z[32]= - z[40]*z[42];
    z[18]= - z[18] - z[33];
    z[18]=z[18]*z[44];
    z[18]=z[18] - z[29] + z[32];
    z[18]=z[5]*z[18];
    z[27]= - z[27] - z[30];
    z[27]=z[27]*z[42];
    z[29]= - static_cast<T>(39)+ z[28];
    z[18]=z[18] + 3*z[29] + z[27];
    z[18]=z[4]*z[18];
    z[27]= - static_cast<T>(3)- 22*z[28];
    z[27]=2*z[27] - z[41];
    z[27]=z[3]*z[27];
    z[29]=z[15]*z[6];
    z[25]= - z[25] + z[29];
    z[25]=z[16]*z[25];
    z[28]=static_cast<T>(35)- 38*z[28];
    z[28]=z[7]*z[28];

    r += z[17] + z[18] + z[19] + z[20] + z[21] + 6*z[22] + 16*z[23] + 
      z[24] + 32*z[25] + z[26] + z[27] + z[28] + z[31];
 
    return r;
}

template double qg_2lLC_r12(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r12(const std::array<dd_real,31>&);
#endif
