#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r116(const std::array<T,31>& k) {
  T z[120];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[5];
    z[9]=k[8];
    z[10]=k[7];
    z[11]=k[2];
    z[12]=k[23];
    z[13]=k[6];
    z[14]=k[15];
    z[15]=k[14];
    z[16]=k[17];
    z[17]=k[16];
    z[18]=2*z[8];
    z[19]=npow(z[2],2);
    z[20]=z[18]*z[19];
    z[21]=npow(z[8],2);
    z[22]=z[21]*z[7];
    z[23]=z[22]*z[19];
    z[24]=z[20] - z[23];
    z[24]=z[24]*z[7];
    z[25]=z[2] - z[8];
    z[26]= - z[2]*z[25];
    z[26]= - z[21] + z[26];
    z[26]=2*z[26] - z[24];
    z[26]=z[7]*z[26];
    z[27]=2*z[2];
    z[28]=z[27]*z[22];
    z[29]=z[2]*z[8];
    z[30]=5*z[29];
    z[31]=z[30] + z[28];
    z[32]=z[23]*z[14];
    z[33]=z[32] - z[31];
    z[33]=z[14]*z[33];
    z[34]=z[29] - z[21];
    z[35]=z[21]*z[2];
    z[36]=z[5]*z[35];
    z[36]= - 2*z[34] + z[36];
    z[36]=z[5]*z[36];
    z[37]=3*z[8];
    z[38]=z[37] - z[2];
    z[26]=z[33] + z[26] + z[36] - z[38];
    z[26]=z[14]*z[26];
    z[33]=z[21]*z[15];
    z[36]=z[18] - z[33];
    z[36]=z[15]*z[36];
    z[36]=static_cast<T>(1)+ z[36];
    z[39]=2*z[9];
    z[36]=z[36]*z[39];
    z[40]=z[4]*z[9];
    z[41]=z[15]*z[8];
    z[42]= - static_cast<T>(3)+ z[41];
    z[42]=z[9]*z[15]*z[42];
    z[42]=z[42] - z[40];
    z[42]=z[11]*z[42];
    z[43]=z[37]*z[15];
    z[44]= - static_cast<T>(4)+ z[43];
    z[44]=z[15]*z[44];
    z[45]=z[1]*z[40];
    z[36]=z[42] - 2*z[45] + z[4] + z[36] + z[44] + 7*z[14];
    z[36]=z[11]*z[36];
    z[42]=z[25]*z[27];
    z[44]=z[19]*z[15];
    z[45]=z[8]*z[44];
    z[42]=z[42] + z[45];
    z[42]=z[15]*z[42];
    z[45]=3*z[2];
    z[46]=z[45] - z[8];
    z[30]= - z[7]*z[30];
    z[30]=z[30] + z[42] - z[46];
    z[30]=z[7]*z[30];
    z[42]=z[25]*z[45];
    z[47]= - z[5]*z[42];
    z[47]=z[8] + z[47];
    z[47]=z[5]*z[47];
    z[48]=3*z[15];
    z[34]=z[34]*z[48];
    z[34]=z[2] + z[34];
    z[34]=z[15]*z[34];
    z[49]=npow(z[8],3);
    z[50]=z[49]*z[15];
    z[51]= - z[21] + z[50];
    z[51]=z[15]*z[51];
    z[51]= - z[8] + z[51];
    z[51]=z[9]*z[51];
    z[52]=npow(z[2],3);
    z[53]=z[5]*z[52];
    z[53]= - z[19] + z[53];
    z[53]=z[5]*z[53];
    z[53]= - z[2] + z[53];
    z[53]=z[4]*z[53];
    z[54]=z[19]*z[5];
    z[55]=z[27] - z[54];
    z[55]=z[5]*z[55];
    z[55]=static_cast<T>(1)+ z[55];
    z[56]=z[5]*z[2];
    z[57]= - static_cast<T>(3)+ z[56];
    z[57]=z[5]*z[57];
    z[57]=z[57] - z[9];
    z[57]=z[1]*z[57];
    z[55]=z[57] + 2*z[55];
    z[55]=z[4]*z[55];
    z[57]=z[5]*z[45];
    z[57]= - static_cast<T>(4)+ z[57];
    z[57]=z[5]*z[57];
    z[55]=z[9] + z[57] + 7*z[7] + z[55];
    z[55]=z[1]*z[55];
    z[26]=z[36] + z[55] + z[53] + z[51] + z[26] + z[30] + z[34] + static_cast<T>(4)+ 
    z[47];
    z[26]=z[3]*z[26];
    z[30]=3*z[29];
    z[32]=z[32] - z[30];
    z[34]=z[21] + z[30];
    z[36]=z[7]*z[2];
    z[34]=z[34]*z[36];
    z[34]=z[34] - z[32];
    z[34]=z[14]*z[34];
    z[47]=npow(z[6],2);
    z[51]=z[19]*z[47];
    z[53]=z[51]*z[4];
    z[55]=z[2]*z[6];
    z[57]=3*z[55];
    z[58]=z[57] - z[47];
    z[59]=z[2]*z[58];
    z[59]=z[59] + z[53];
    z[59]=z[4]*z[59];
    z[60]=2*z[6];
    z[61]=z[60] + z[8];
    z[62]=z[45] - z[61];
    z[62]=z[2]*z[62];
    z[63]= - z[14]*z[8]*z[19];
    z[59]=z[59] + z[62] + z[63];
    z[59]=z[10]*z[59];
    z[62]=z[18] + z[6];
    z[63]= - z[45] - z[62];
    z[63]=z[63]*z[36];
    z[64]=z[7]*z[6];
    z[65]= - z[19]*z[64];
    z[65]=z[57] + z[65];
    z[65]=z[4]*z[65];
    z[66]=z[6] + z[8];
    z[34]=z[59] + z[65] + z[34] + 3*z[66] + z[63];
    z[34]=z[16]*z[34];
    z[59]=z[2] - z[6];
    z[36]= - z[59]*z[36];
    z[63]=14*z[6];
    z[65]=z[4]*z[57];
    z[36]=z[65] + z[36] + z[63] - z[45];
    z[36]=z[4]*z[36];
    z[65]=5*z[8];
    z[67]= - z[65] + z[27];
    z[67]=z[2]*z[67];
    z[20]= - z[14]*z[20];
    z[20]=z[67] + z[20];
    z[20]=z[14]*z[20];
    z[67]=z[55] + z[47];
    z[68]=z[67]*z[27];
    z[68]=z[68] + z[53];
    z[68]=z[4]*z[68];
    z[58]=3*z[58] + z[68];
    z[58]=z[4]*z[58];
    z[68]=4*z[2];
    z[69]=z[68] - z[61];
    z[20]=z[58] + 3*z[69] + z[20];
    z[20]=z[10]*z[20];
    z[28]=z[28] - z[32];
    z[28]=z[14]*z[28];
    z[32]=2*z[21];
    z[42]= - z[32] + z[42];
    z[42]=z[7]*z[42];
    z[28]=z[28] + 2*z[46] + z[42];
    z[28]=z[14]*z[28];
    z[42]=z[6]*z[8];
    z[46]=z[42]*z[9];
    z[58]=5*z[6];
    z[69]= - z[46] + z[37] + z[58];
    z[69]=z[9]*z[69];
    z[70]=8*z[9];
    z[71]=3*z[4];
    z[72]=3*z[14];
    z[73]= - z[71] - z[72] - z[70];
    z[73]=z[11]*z[73];
    z[74]=2*z[62] + z[45];
    z[74]=z[7]*z[74];
    z[20]=z[34] + z[73] + z[20] + z[36] + z[69] + z[28] + static_cast<T>(13)+ z[74];
    z[20]=z[16]*z[20];
    z[28]=z[9]*z[47]*z[21];
    z[34]=3*z[21];
    z[36]=z[34] - z[42];
    z[36]=z[6]*z[36];
    z[36]=z[36] + z[28];
    z[36]=z[9]*z[36];
    z[69]= - z[2]*z[22];
    z[36]=z[36] + z[69] - z[29] + z[34] - 2*z[42];
    z[36]=z[13]*z[36];
    z[69]=z[34] + z[29];
    z[69]=z[2]*z[69];
    z[23]=z[69] - z[23];
    z[23]=z[7]*z[23];
    z[69]=z[34] + z[42];
    z[23]=z[23] - 2*z[29] - z[69];
    z[23]=z[14]*z[23];
    z[73]=z[2] + z[6];
    z[74]=z[7]*z[29];
    z[74]=z[74] + z[73];
    z[75]=3*z[42];
    z[76]=z[21]*z[6];
    z[77]= - z[14]*z[76];
    z[77]=z[75] + z[77];
    z[77]=z[9]*z[77];
    z[23]=z[36] + z[77] + 3*z[74] + z[23];
    z[23]=z[12]*z[23];
    z[36]=z[42] - z[21];
    z[74]=z[14]*z[36];
    z[77]=3*z[9];
    z[78]=z[42]*z[77];
    z[74]=z[78] + z[74] - z[37] + z[63];
    z[74]=z[9]*z[74];
    z[78]= - z[37] - z[27];
    z[78]=z[2]*z[78];
    z[24]=z[24] + z[34] + z[78];
    z[24]=z[7]*z[24];
    z[78]=z[60] + z[37];
    z[24]=z[24] + z[68] + z[78];
    z[24]=z[14]*z[24];
    z[68]=z[55]*z[4];
    z[79]= - z[68] + z[58] + z[45];
    z[79]=z[4]*z[79];
    z[80]=z[42] + z[21];
    z[81]=z[80]*z[60];
    z[28]=z[81] + z[28];
    z[28]=z[9]*z[28];
    z[81]=3*z[6];
    z[82]=z[37] - z[6];
    z[82]=z[82]*z[81];
    z[28]=z[82] + z[28];
    z[28]=z[9]*z[28];
    z[31]=z[32] - z[31];
    z[31]=z[7]*z[31];
    z[82]=z[18] - z[6];
    z[82]=2*z[82] - z[2];
    z[28]=z[28] + 3*z[82] + z[31];
    z[28]=z[13]*z[28];
    z[31]=z[7]*z[30];
    z[31]=2*z[38] + z[31];
    z[31]=z[7]*z[31];
    z[38]= - z[7] - z[9];
    z[38]=3*z[38] - 8*z[4];
    z[38]=z[1]*z[38];
    z[23]=z[23] + z[38] + z[28] + z[79] + z[74] + z[24] + static_cast<T>(13)+ z[31];
    z[23]=z[12]*z[23];
    z[20]=z[20] + z[26] + z[23];
    z[23]=z[47]*z[5];
    z[24]=z[23]*z[21];
    z[26]= - 69*z[21] + 41*z[42];
    z[26]=z[6]*z[26];
    z[26]=z[26] + 61*z[24];
    z[26]=z[5]*z[26];
    z[28]=10*z[21];
    z[31]=9*z[8];
    z[38]= - z[31] + 22*z[6];
    z[38]=z[6]*z[38];
    z[26]=z[26] + z[28] + z[38];
    z[26]=z[5]*z[26];
    z[38]= - 28*z[49] + 15*z[76];
    z[74]=npow(z[8],4);
    z[79]=z[6]*z[49];
    z[79]= - z[74] + z[79];
    z[82]=11*z[7];
    z[79]=z[79]*z[82];
    z[38]=2*z[38] + z[79];
    z[38]=z[7]*z[38];
    z[79]= - 23*z[21] + 9*z[42];
    z[38]=6*z[79] + z[38];
    z[38]=z[7]*z[38];
    z[79]= - z[6]*z[80];
    z[79]=z[79] - z[24];
    z[83]=2*z[5];
    z[79]=z[79]*z[83];
    z[84]= - z[9]*z[24];
    z[79]=z[84] + z[42] + z[79];
    z[84]=6*z[9];
    z[79]=z[79]*z[84];
    z[85]= - z[8] - z[58];
    z[26]=z[79] + z[38] + 6*z[85] + z[26];
    z[26]=z[9]*z[26];
    z[38]=61*z[6];
    z[79]= - 256*z[8] + z[38];
    z[79]=z[6]*z[79];
    z[85]= - 67*z[8] + 64*z[6];
    z[85]=2*z[85] + 67*z[2];
    z[85]=z[2]*z[85];
    z[79]=z[85] + 67*z[21] + z[79];
    z[79]=z[2]*z[79];
    z[85]=61*z[42];
    z[86]=64*z[21] - z[85];
    z[86]=z[86]*z[60];
    z[79]=z[86] + z[79];
    z[79]=z[5]*z[79];
    z[86]=101*z[2];
    z[87]=82*z[8];
    z[88]=z[87] - 59*z[6];
    z[88]=2*z[88] - z[86];
    z[88]=z[2]*z[88];
    z[89]=199*z[8] - 23*z[6];
    z[89]=z[6]*z[89];
    z[79]=z[79] + z[88] - 57*z[21] + z[89];
    z[79]=z[5]*z[79];
    z[88]=19*z[2];
    z[89]= - 24*z[8] + 7*z[6];
    z[79]=z[79] + 2*z[89] + z[88];
    z[79]=z[5]*z[79];
    z[89]=2*z[15];
    z[90]=z[49]*z[89];
    z[91]=z[72]*z[47];
    z[92]=4*z[6];
    z[93]=z[92] + z[8];
    z[94]= - z[93]*z[91];
    z[95]=10*z[6];
    z[96]= - z[8] - z[95];
    z[96]=z[6]*z[96];
    z[90]=z[94] + z[90] - z[32] + z[96];
    z[90]=z[90]*z[72];
    z[90]=z[90] + 55*z[33] - 58*z[8] + 43*z[6];
    z[90]=z[14]*z[90];
    z[94]=8*z[8];
    z[96]=z[33] + z[94] - z[58];
    z[96]=z[96]*z[48];
    z[97]=z[76] + z[49];
    z[98]=z[15]*z[97];
    z[98]=z[98] - z[21] + z[75];
    z[98]=z[15]*z[98];
    z[98]=31*z[8] + z[98];
    z[99]=z[82]*z[49];
    z[100]=36*z[21] + z[99];
    z[100]=z[7]*z[100];
    z[98]=3*z[98] + 2*z[100];
    z[98]=z[7]*z[98];
    z[73]=z[73]*z[45];
    z[100]=z[60] + z[2];
    z[101]= - z[2]*z[100];
    z[101]= - z[47] + z[101];
    z[101]=z[101]*z[56];
    z[73]=z[73] + z[101];
    z[101]=6*z[5];
    z[73]=z[73]*z[101];
    z[102]=npow(z[6],4);
    z[103]=9*z[14];
    z[104]= - z[102]*z[103];
    z[105]=npow(z[6],3);
    z[104]= - 2*z[105] + z[104];
    z[104]=z[104]*z[72];
    z[104]= - 55*z[47] + z[104];
    z[104]=z[14]*z[104];
    z[73]=z[104] + z[73] - 37*z[6] + z[88];
    z[73]=z[4]*z[73];
    z[26]=z[73] + z[26] + z[90] + z[98] + z[96] + static_cast<T>(28)+ z[79];
    z[26]=z[13]*z[26];
    z[73]=z[51]*z[15];
    z[79]= - z[2]*z[67];
    z[79]=z[79] - z[73];
    z[79]=z[79]*z[89];
    z[53]= - z[15]*z[53];
    z[53]=z[53] + z[55] + z[79];
    z[53]=z[4]*z[53];
    z[53]=z[53] - z[58] - z[2];
    z[57]=z[57] + z[47];
    z[79]=z[2]*z[57];
    z[73]=z[79] - 61*z[73];
    z[73]=z[15]*z[73];
    z[79]= - 57*z[6] - 34*z[2];
    z[79]=z[2]*z[79];
    z[73]=z[73] + 14*z[47] + z[79];
    z[73]=z[15]*z[73];
    z[79]=6*z[47];
    z[90]= - z[79] + z[55];
    z[90]=z[2]*z[90];
    z[96]=z[47]*z[2];
    z[98]=z[96] - z[105];
    z[104]= - z[2]*z[98];
    z[102]= - z[102] + z[104];
    z[102]=z[102]*z[72];
    z[90]=z[102] + 3*z[105] + z[90];
    z[90]=z[90]*z[72];
    z[102]=34*z[6] - z[88];
    z[102]=z[2]*z[102];
    z[90]=z[90] - 44*z[47] + z[102];
    z[90]=z[14]*z[90];
    z[53]=z[90] + z[73] + 6*z[53];
    z[53]=z[4]*z[53];
    z[73]= - 116*z[21] - z[85];
    z[73]=z[6]*z[73];
    z[38]=116*z[8] + z[38];
    z[38]=z[6]*z[38];
    z[38]=55*z[21] + z[38];
    z[85]=116*z[6];
    z[90]= - 55*z[8] - z[85];
    z[90]=z[2]*z[90];
    z[38]=2*z[38] + z[90];
    z[38]=z[2]*z[38];
    z[38]=z[38] - 55*z[49] + z[73];
    z[38]=z[15]*z[38];
    z[73]=z[87] + z[58];
    z[73]=z[6]*z[73];
    z[87]=15*z[2] - 92*z[8] - 97*z[6];
    z[87]=z[2]*z[87];
    z[38]=z[38] + z[87] + 71*z[21] + z[73];
    z[38]=z[15]*z[38];
    z[38]=z[38] + 17*z[8] + 58*z[6];
    z[38]=z[15]*z[38];
    z[73]=z[49]*z[101];
    z[87]= - 4*z[49] + z[35];
    z[87]=z[87]*z[82];
    z[73]=z[87] + z[73] - 192*z[21] + 43*z[29];
    z[73]=z[7]*z[73];
    z[87]=35*z[2];
    z[90]=z[21]*z[5];
    z[73]=z[73] - 61*z[90] - 306*z[8] + z[87];
    z[73]=z[7]*z[73];
    z[97]= - z[5]*z[97];
    z[102]=5*z[42];
    z[97]=z[97] - z[32] - z[102];
    z[97]=z[5]*z[97];
    z[104]=z[59]*z[2];
    z[106]= - z[47] - z[104];
    z[106]=z[14]*z[61]*z[106];
    z[107]=z[81] + z[8];
    z[108]=z[107] + z[2];
    z[108]=z[108]*z[2];
    z[106]=z[106] + z[47] - z[108];
    z[106]=z[106]*z[72];
    z[109]= - 5*z[2] - z[66];
    z[97]=z[106] + 2*z[109] + z[97];
    z[97]=z[97]*z[72];
    z[106]=z[32] + z[42];
    z[109]=z[6]*z[106];
    z[109]=z[49] + z[109];
    z[109]=z[15]*z[109];
    z[109]= - 3*z[80] + z[109];
    z[109]=z[109]*z[89];
    z[110]=53*z[6];
    z[109]=z[109] + 95*z[8] - z[110];
    z[111]=z[74]*z[82];
    z[111]=54*z[49] + z[111];
    z[112]=3*z[7];
    z[111]=z[111]*z[112];
    z[111]=425*z[21] + z[111];
    z[111]=z[7]*z[111];
    z[109]=3*z[109] + z[111];
    z[109]=z[9]*z[109];
    z[111]= - z[90] - 20*z[8] + z[81];
    z[113]=3*z[5];
    z[111]=z[111]*z[113];
    z[38]=z[53] + z[109] + z[97] + z[73] + z[38] - static_cast<T>(170)+ z[111];
    z[38]=z[10]*z[38];
    z[53]=13*z[90];
    z[73]= - 23*z[8] + z[53];
    z[73]=z[73]*z[113];
    z[97]=npow(z[5],2);
    z[109]=z[49]*z[97];
    z[109]= - 22*z[22] - 71*z[8] - 12*z[109];
    z[109]=z[7]*z[109];
    z[73]=z[109] - static_cast<T>(76)+ z[73];
    z[73]=z[7]*z[73];
    z[109]=z[8]*z[101];
    z[109]= - static_cast<T>(19)+ z[109];
    z[109]=z[109]*z[113];
    z[99]=41*z[21] + z[99];
    z[99]=z[7]*z[99];
    z[99]=85*z[8] + z[99];
    z[99]=z[7]*z[99];
    z[99]=static_cast<T>(53)+ z[99];
    z[99]=z[99]*z[77];
    z[73]=24*z[4] + z[99] + z[109] + z[73];
    z[73]=z[10]*z[73];
    z[99]=7*z[8];
    z[53]=z[99] - z[53];
    z[53]=z[5]*z[53];
    z[53]= - static_cast<T>(31)+ z[53];
    z[53]=z[5]*z[53];
    z[109]=npow(z[5],3);
    z[111]=z[109]*z[7];
    z[111]=6*z[111];
    z[114]=z[49]*z[111];
    z[115]=28*z[8] + 11*z[22];
    z[115]=z[7]*z[115];
    z[115]=static_cast<T>(53)+ z[115];
    z[115]=z[9]*z[115];
    z[53]=z[115] + z[53] + z[114];
    z[53]=z[10]*z[53];
    z[114]=25*z[6];
    z[115]=19*z[8];
    z[116]=z[115] - z[114];
    z[116]=z[5]*z[116];
    z[116]= - static_cast<T>(7)+ z[116];
    z[116]=z[116]*z[97];
    z[117]=z[6] - z[8];
    z[118]=z[117]*z[6];
    z[118]=z[118] + z[21];
    z[111]= - z[118]*z[111];
    z[53]=z[53] + z[116] + z[111];
    z[53]=z[7]*z[53];
    z[111]=z[117]*z[82];
    z[111]= - static_cast<T>(17)+ z[111];
    z[111]=z[9]*z[111]*npow(z[7],2);
    z[116]= - z[5]*z[81];
    z[119]= - z[6] - z[23];
    z[119]=z[7]*z[119];
    z[116]=z[119] - static_cast<T>(2)+ z[116];
    z[116]=z[5]*z[116];
    z[119]=static_cast<T>(2)+ z[64];
    z[119]=z[9]*z[119];
    z[116]=z[119] + z[116];
    z[116]=z[7]*z[116];
    z[116]= - 2*z[97] + z[116];
    z[116]=z[116]*z[71];
    z[53]=z[116] + z[111] - 19*z[109] + z[53];
    z[53]=z[1]*z[53];
    z[109]= - z[65] + z[60];
    z[111]=z[36]*z[82];
    z[109]=6*z[109] + z[111];
    z[109]=z[7]*z[109];
    z[109]= - static_cast<T>(40)+ z[109];
    z[109]=z[7]*z[109];
    z[109]=10*z[5] + z[109];
    z[109]=z[109]*z[77];
    z[111]=z[6] - z[23];
    z[111]=z[111]*z[83];
    z[116]=z[64] + 1;
    z[111]=z[111] - z[116];
    z[111]=z[7]*z[111];
    z[39]= - z[116]*z[39];
    z[116]= - z[60] + z[2];
    z[116]=z[5]*z[116];
    z[116]=static_cast<T>(3)+ z[116];
    z[116]=z[5]*z[116];
    z[39]=z[39] + z[116] + z[111];
    z[39]=z[39]*z[71];
    z[71]= - static_cast<T>(73)+ 39*z[56];
    z[71]=z[71]*z[97];
    z[97]= - 4*z[8] - z[6];
    z[97]=z[6]*z[97];
    z[97]=4*z[21] + z[97];
    z[97]=z[5]*z[97];
    z[97]= - z[60] + z[97];
    z[97]=z[5]*z[97];
    z[97]=static_cast<T>(3)+ z[97];
    z[111]=z[7]*z[8];
    z[97]=3*z[97] + 22*z[111];
    z[97]=z[7]*z[97];
    z[111]= - 51*z[8] - z[114];
    z[111]=z[5]*z[111];
    z[111]=static_cast<T>(68)+ z[111];
    z[111]=z[5]*z[111];
    z[97]=z[111] + z[97];
    z[97]=z[7]*z[97];
    z[39]=z[53] + z[73] + z[39] + z[109] + z[71] + z[97];
    z[39]=z[1]*z[39];
    z[53]= - z[6]*z[78];
    z[71]= - z[2]*z[66];
    z[53]=z[71] - z[21] + z[53];
    z[53]=z[7]*z[53];
    z[59]= - z[72]*z[93]*z[59];
    z[30]=z[32] + z[30];
    z[30]=z[15]*z[30];
    z[71]=6*z[2];
    z[30]=z[59] + z[53] + z[30] - z[71] - z[37] - z[92];
    z[30]=z[30]*z[72];
    z[53]= - 109*z[21] + 25*z[29];
    z[53]=z[2]*z[53];
    z[59]= - z[35] + 2*z[49];
    z[59]=z[59]*z[2];
    z[59]=z[59] - z[74];
    z[73]=z[5]*z[59];
    z[53]= - 47*z[73] + 84*z[49] + z[53];
    z[53]=z[5]*z[53];
    z[73]= - 3*z[49] + z[35];
    z[73]=z[2]*z[73];
    z[73]=3*z[74] + z[73];
    z[73]=z[2]*z[73];
    z[78]=npow(z[8],5);
    z[73]= - z[78] + z[73];
    z[93]=11*z[5];
    z[73]=z[73]*z[93];
    z[59]=25*z[59] + z[73];
    z[59]=z[5]*z[59];
    z[73]=z[35] - z[49];
    z[59]=14*z[73] + z[59];
    z[59]=z[59]*z[89];
    z[97]=12*z[21] - 7*z[29];
    z[53]=z[59] + 3*z[97] + z[53];
    z[53]=z[53]*z[48];
    z[59]=z[5]*z[73];
    z[59]=31*z[59] - 44*z[21] + 29*z[29];
    z[59]=z[59]*z[113];
    z[53]=z[53] + z[59] - z[31] + 61*z[2];
    z[53]=z[15]*z[53];
    z[18]= - z[18] - z[81];
    z[59]=z[5]*z[80];
    z[73]= - z[6]*z[61];
    z[73]=z[73] - z[108];
    z[73]=z[7]*z[73];
    z[80]=z[15]*z[25];
    z[80]=z[80] - 15;
    z[80]=z[2]*z[80];
    z[18]=z[73] + z[59] + 8*z[18] + z[80];
    z[18]=z[18]*z[112];
    z[59]=z[99] + 9*z[90];
    z[59]=z[5]*z[59];
    z[59]= - static_cast<T>(22)+ z[59];
    z[18]=z[30] + z[18] + 3*z[59] + z[53];
    z[18]=z[14]*z[18];
    z[30]=z[6]*z[90];
    z[30]=z[30] - z[36];
    z[30]=z[5]*z[30];
    z[53]=z[21] - z[47];
    z[53]=z[7]*z[53];
    z[59]=z[62]*z[6];
    z[59]=z[59] + z[21];
    z[62]= - z[59]*z[64];
    z[64]= - z[6]*z[66];
    z[62]=z[64] + z[62];
    z[62]=z[14]*z[62];
    z[30]=z[62] + 4*z[53] + z[30] + z[37] - z[60];
    z[30]=z[14]*z[30];
    z[53]= - z[69]*z[81];
    z[24]=z[53] - 16*z[24];
    z[24]=z[5]*z[24];
    z[53]=25*z[21];
    z[62]=z[6]*z[107];
    z[24]=z[24] + z[53] + z[62];
    z[24]=z[5]*z[24];
    z[62]= - z[21]*z[81];
    z[64]=44*z[49];
    z[62]=z[64] + z[62];
    z[62]=z[6]*z[62];
    z[69]=22*z[49];
    z[73]= - z[23]*z[69];
    z[62]=z[62] + z[73];
    z[62]=z[5]*z[62];
    z[73]=z[32] - z[42];
    z[73]=z[73]*z[81];
    z[62]=z[62] - z[69] + z[73];
    z[62]=z[5]*z[62];
    z[32]=z[62] - z[32] + z[75];
    z[32]=z[15]*z[32];
    z[24]=z[32] + z[24] + z[65] - z[92];
    z[24]=z[15]*z[24];
    z[32]=z[76] - z[49];
    z[62]=z[32]*z[82];
    z[73]=43*z[21];
    z[62]=z[62] - z[73] + 21*z[42];
    z[62]=z[7]*z[62];
    z[75]=13*z[6];
    z[62]=z[62] - 84*z[8] + z[75];
    z[62]=z[7]*z[62];
    z[76]= - z[5]*z[102];
    z[76]=z[76] - z[115] - 24*z[6];
    z[76]=z[5]*z[76];
    z[46]= - z[101]*z[46];
    z[24]=z[46] + z[30] + z[62] + z[24] - static_cast<T>(6)+ z[76];
    z[24]=z[24]*z[77];
    z[30]=z[50] - z[34];
    z[30]=z[30]*z[15];
    z[46]=z[14]*z[79];
    z[46]=z[46] - 4*z[30] - z[94] + 9*z[6];
    z[46]=z[14]*z[46];
    z[50]=z[115] - 11*z[33];
    z[50]=z[15]*z[50];
    z[46]=z[46] - static_cast<T>(1)+ z[50];
    z[46]=z[14]*z[46];
    z[50]=z[105]*z[103];
    z[50]=4*z[47] + z[50];
    z[50]=z[14]*z[50];
    z[50]=z[95] + z[50];
    z[50]=z[14]*z[50];
    z[50]=static_cast<T>(2)+ z[50];
    z[50]=z[4]*z[50];
    z[62]=static_cast<T>(5)- 8*z[41];
    z[62]=z[15]*z[62];
    z[46]=z[50] + z[70] + z[62] + z[46];
    z[46]=z[13]*z[46];
    z[50]= - static_cast<T>(5)- 13*z[41];
    z[50]=z[50]*npow(z[15],2);
    z[46]=z[50] + z[46];
    z[50]=15*z[8];
    z[62]= - z[50] + 13*z[33];
    z[62]=z[15]*z[62];
    z[62]= - static_cast<T>(3)+ z[62];
    z[62]=z[15]*z[62];
    z[30]=z[37] + z[30];
    z[30]=z[15]*z[30];
    z[30]= - static_cast<T>(1)+ z[30];
    z[70]=6*z[14];
    z[30]=z[30]*z[70];
    z[70]= - z[60] - z[91];
    z[70]=z[70]*z[72];
    z[70]= - static_cast<T>(10)+ z[70];
    z[70]=z[4]*z[70];
    z[30]=z[70] + z[62] + z[30];
    z[30]=z[13]*z[30];
    z[62]=z[37] - z[33];
    z[62]=z[15]*z[62];
    z[62]= - static_cast<T>(2)+ z[62];
    z[62]=z[14]*z[62];
    z[41]= - static_cast<T>(2)+ z[41];
    z[41]=z[15]*z[41];
    z[41]=z[41] + z[62];
    z[41]=z[9]*z[41];
    z[62]=z[14]*z[6];
    z[70]=static_cast<T>(2)+ z[62];
    z[40]=z[70]*z[40];
    z[40]=z[41] + z[40];
    z[30]=3*z[40] + z[30];
    z[30]=z[11]*z[14]*z[30];
    z[40]=z[99] - 18*z[33];
    z[40]=z[40]*z[89];
    z[40]= - static_cast<T>(13)+ z[40];
    z[40]=z[15]*z[40];
    z[41]=12*z[8] - 5*z[33];
    z[41]=z[15]*z[41];
    z[41]= - 6*z[62] - static_cast<T>(7)+ z[41];
    z[41]=z[41]*z[72];
    z[40]=z[40] + z[41];
    z[40]=z[14]*z[40];
    z[41]= - z[47]*z[103];
    z[41]= - z[6] + z[41];
    z[41]=z[41]*z[72];
    z[41]= - static_cast<T>(19)+ z[41];
    z[41]=z[14]*z[41];
    z[62]=z[62] + 1;
    z[70]= - z[62]*z[84];
    z[41]=z[41] + z[70];
    z[41]=z[4]*z[41];
    z[33]=z[8] - z[33];
    z[33]=z[33]*z[89];
    z[33]=z[33] - z[62];
    z[33]=z[14]*z[33];
    z[43]=static_cast<T>(5)- z[43];
    z[43]=z[15]*z[43];
    z[33]=z[43] + z[33];
    z[33]=z[33]*z[77];
    z[30]=z[30] + z[41] + z[33] + z[40] + 3*z[46];
    z[30]=z[11]*z[30];
    z[33]=z[78]*z[82];
    z[33]=z[33] + 67*z[74];
    z[33]=z[33]*z[7];
    z[33]=z[33] + 223*z[49];
    z[33]=z[33]*z[7];
    z[40]= - z[110] + 99*z[8];
    z[40]=z[40]*z[6];
    z[33]=z[33] - z[40] + 167*z[21];
    z[33]=z[33]*z[9];
    z[40]=z[49]*z[2];
    z[40]= - z[40] + 2*z[74];
    z[40]=z[40]*z[82];
    z[35]=z[40] - 54*z[35] + 121*z[49];
    z[35]=z[35]*z[7];
    z[29]= - z[73] + 13*z[29];
    z[29]=z[35] - 6*z[29];
    z[29]=z[29]*z[7];
    z[35]=npow(z[14],2)*npow(z[6],5);
    z[35]=9*z[35] + 35*z[105];
    z[35]=z[35]*z[14];
    z[40]=z[55] - z[47];
    z[35]=z[35] - 31*z[40];
    z[35]=z[35]*z[4];
    z[40]=z[14]*z[61]*z[105];
    z[41]=z[66]*z[47];
    z[40]=z[40] + z[41];
    z[40]=z[40]*z[72];
    z[41]=z[58] - z[8];
    z[41]=z[41]*z[6];
    z[40]=z[40] - z[41];
    z[40]=z[40]*z[72];
    z[29]= - z[29] + z[33] + z[87] + 82*z[6] - 159*z[8] + z[35] + z[40];
    z[33]=z[10] + z[13];
    z[29]=z[17]*z[29]*z[33];
    z[33]=41*z[6];
    z[35]=113*z[8] + z[33];
    z[35]=z[6]*z[35];
    z[35]= - z[34] + z[35];
    z[35]=z[6]*z[35];
    z[40]= - z[115] - z[63];
    z[40]=z[6]*z[40];
    z[40]= - 14*z[21] + z[40];
    z[41]=22*z[2] - z[115] + 85*z[6];
    z[41]=z[2]*z[41];
    z[40]=2*z[40] + z[41];
    z[40]=z[2]*z[40];
    z[41]=z[37] + z[6];
    z[43]= - z[41] + z[2];
    z[43]=z[5]*z[105]*z[43];
    z[46]=25*z[49];
    z[35]=22*z[43] + z[40] + z[46] + z[35];
    z[35]=z[5]*z[35];
    z[40]=11*z[6];
    z[43]= - 36*z[8] - z[40];
    z[43]=z[6]*z[43];
    z[43]=17*z[21] + z[43];
    z[58]=25*z[2];
    z[31]= - z[58] - z[31] + 19*z[6];
    z[31]=z[2]*z[31];
    z[31]=z[35] + 2*z[43] + z[31];
    z[31]=z[31]*z[113];
    z[35]= - 25*z[8] - 8*z[6];
    z[35]=z[6]*z[35];
    z[35]=z[53] + z[35];
    z[43]=z[117]*z[58];
    z[35]=2*z[35] + z[43];
    z[35]=z[2]*z[35];
    z[36]=z[6]*z[36];
    z[36]=z[49] + z[36];
    z[43]= - z[2]*z[117];
    z[43]= - 3*z[118] + z[43];
    z[43]=z[2]*z[43];
    z[36]=3*z[36] + z[43];
    z[36]=z[2]*z[36];
    z[32]= - z[6]*z[32];
    z[32]=z[36] - z[74] + z[32];
    z[32]=z[32]*z[93];
    z[36]=z[53] + 8*z[42];
    z[36]=z[6]*z[36];
    z[32]=z[32] + z[35] - z[46] + z[36];
    z[32]=z[32]*z[83];
    z[35]=11*z[8] - z[75];
    z[35]=z[2]*z[35];
    z[32]=z[32] + z[35] - 11*z[21] + 13*z[42];
    z[32]=z[32]*z[48];
    z[35]= - z[81] + 11*z[2];
    z[36]= - z[65] + z[35];
    z[31]=z[32] + 4*z[36] + z[31];
    z[31]=z[15]*z[31];
    z[32]= - z[88] + z[37] + 47*z[6];
    z[32]=z[2]*z[32];
    z[36]=66*z[8] + z[114];
    z[36]=z[36]*z[47];
    z[36]=z[36] - 33*z[96];
    z[36]=z[36]*z[83];
    z[42]= - 107*z[8] - 66*z[6];
    z[42]=z[6]*z[42];
    z[32]=z[36] + z[32] - 6*z[21] + z[42];
    z[32]=z[5]*z[32];
    z[36]=10*z[2] - z[65] + z[75];
    z[32]=2*z[36] + z[32];
    z[32]=z[32]*z[113];
    z[31]=z[31] - static_cast<T>(56)+ z[32];
    z[31]=z[15]*z[31];
    z[32]=21*z[8] + z[40];
    z[32]=z[6]*z[32];
    z[36]=z[2] - z[66];
    z[36]=z[2]*z[36];
    z[28]=z[36] + z[28] + z[32];
    z[32]=63*z[6];
    z[36]=170*z[8] + z[32];
    z[36]=z[6]*z[36];
    z[36]=151*z[21] + z[36];
    z[36]=z[6]*z[36];
    z[36]=z[64] + z[36];
    z[36]=z[6]*z[36];
    z[41]= - z[6]*z[41];
    z[34]= - z[34] + z[41];
    z[34]=z[6]*z[34];
    z[34]= - z[49] + z[34];
    z[34]=z[34]*z[23];
    z[34]=z[36] + 22*z[34];
    z[34]=z[5]*z[34];
    z[32]= - 145*z[8] - z[32];
    z[32]=z[6]*z[32];
    z[32]= - 104*z[21] + z[32];
    z[32]=z[6]*z[32];
    z[32]=z[34] - z[69] + z[32];
    z[32]=z[5]*z[32];
    z[28]=2*z[28] + z[32];
    z[28]=z[15]*z[28];
    z[32]= - 191*z[8] - z[85];
    z[32]=z[6]*z[32];
    z[21]= - 75*z[21] + z[32];
    z[21]=z[6]*z[21];
    z[32]=z[59]*z[23];
    z[21]=z[21] + 50*z[32];
    z[21]=z[5]*z[21];
    z[32]=111*z[8] + 92*z[6];
    z[32]=z[6]*z[32];
    z[21]=z[21] + z[53] + z[32];
    z[21]=z[5]*z[21];
    z[32]= - 10*z[8] - z[40];
    z[21]=z[28] + z[21] + 2*z[32] - z[2];
    z[21]=z[21]*z[48];
    z[28]=z[37] - z[33];
    z[32]=z[66]*z[23];
    z[33]=27*z[8];
    z[34]=z[33] + 46*z[6];
    z[34]=z[6]*z[34];
    z[32]=z[34] - 28*z[32];
    z[32]=z[32]*z[113];
    z[28]=2*z[28] + z[32];
    z[28]=z[5]*z[28];
    z[32]= - z[5]*z[106];
    z[32]=z[32] + z[33] - z[100];
    z[22]=3*z[32] + 44*z[22];
    z[22]=z[7]*z[22];
    z[21]=z[22] + z[21] + static_cast<T>(31)+ z[28];
    z[21]=z[7]*z[21];
    z[22]=z[52]*z[23];
    z[22]=8*z[51] + 11*z[22];
    z[22]=z[22]*z[89];
    z[23]=z[57]*z[45];
    z[28]=3*z[47];
    z[32]=z[28] - 44*z[55];
    z[32]=z[32]*z[54];
    z[22]=z[22] + z[23] + z[32];
    z[22]=z[5]*z[22];
    z[22]= - 15*z[55] + z[22];
    z[22]=z[15]*z[22];
    z[23]=z[35]*z[27];
    z[23]=z[28] + z[23];
    z[23]=z[23]*z[56];
    z[32]= - z[6] - z[58];
    z[32]=z[2]*z[32];
    z[23]=z[23] - z[28] + z[32];
    z[23]=z[5]*z[23];
    z[22]=z[22] + z[23] - z[63] + 23*z[2];
    z[22]=z[15]*z[22];
    z[23]=z[6]*z[44];
    z[23]=z[104] + z[23];
    z[23]=z[15]*z[23];
    z[28]= - z[7]*z[67];
    z[23]=z[28] + z[23] - z[60] + z[45];
    z[23]=z[7]*z[23];
    z[28]= - z[6] + z[71];
    z[28]=z[28]*z[56];
    z[28]=z[28] + z[92] - 9*z[2];
    z[28]=z[5]*z[28];
    z[22]=z[23] + z[28] + z[22];
    z[19]= - z[47] + z[19];
    z[23]= - 2*z[47] - z[55];
    z[23]=z[2]*z[23];
    z[23]= - z[105] + z[23];
    z[23]=z[7]*z[23];
    z[19]=4*z[19] + z[23];
    z[19]=z[19]*z[112];
    z[23]= - z[98]*z[103];
    z[23]=z[23] - 7*z[47] + z[55];
    z[23]=z[23]*z[72];
    z[27]=z[81] - z[27];
    z[19]=z[23] + 11*z[27] + z[19];
    z[19]=z[14]*z[19];
    z[23]=z[15]*z[68];
    z[19]= - 18*z[23] + 3*z[22] + z[19];
    z[19]=z[4]*z[19];
    z[22]= - z[50] - 28*z[6];
    z[22]=z[6]*z[22];
    z[23]=15*z[6] - 29*z[25];
    z[23]=z[2]*z[23];
    z[22]=z[22] + z[23];
    z[22]=z[22]*z[113];
    z[23]= - 61*z[8] + 54*z[6];
    z[22]=z[22] + 2*z[23] + z[86];
    z[22]=z[5]*z[22];
    z[22]= - static_cast<T>(23)+ z[22];
    z[22]=z[5]*z[22];

    r += z[18] + z[19] + 6*z[20] + z[21] + z[22] + z[24] + z[26] + 
      z[29] + z[30] + z[31] + z[38] + z[39];
 
    return r;
}

template double qg_2lLC_r116(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r116(const std::array<dd_real,31>&);
#endif
