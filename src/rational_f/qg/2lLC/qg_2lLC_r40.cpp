#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r40(const std::array<T,31>& k) {
  T z[65];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[24];
    z[7]=k[8];
    z[8]=k[18];
    z[9]=k[2];
    z[10]=k[10];
    z[11]=k[9];
    z[12]=k[4];
    z[13]=k[6];
    z[14]=k[14];
    z[15]=k[15];
    z[16]=k[3];
    z[17]=k[23];
    z[18]=z[12]*z[2];
    z[19]=3*z[18];
    z[20]=npow(z[2],2);
    z[21]=z[20] - z[19];
    z[21]=z[3]*z[21];
    z[22]=2*z[2];
    z[21]=z[21] + z[22] - z[12];
    z[21]=z[3]*z[21];
    z[23]=npow(z[12],2);
    z[24]=z[3]*z[2];
    z[25]=z[23]*z[24];
    z[26]=z[12] - z[2];
    z[27]=z[26]*z[12];
    z[25]=z[27] + z[25];
    z[25]=z[3]*z[25];
    z[28]=2*z[12];
    z[25]= - z[28] + z[25];
    z[25]=z[13]*z[25];
    z[29]=z[4]*npow(z[3],2);
    z[30]=npow(z[2],3);
    z[31]=z[30]*z[29];
    z[32]=z[20]*z[3];
    z[33]=2*z[32];
    z[34]=z[24] - 1;
    z[35]=z[1]*z[34];
    z[35]=z[35] - z[2] + z[33];
    z[36]=z[4]*z[3];
    z[35]=z[36]*z[35];
    z[37]=3*z[24];
    z[38]= - static_cast<T>(1)+ z[37];
    z[38]=z[3]*z[38];
    z[35]=z[38] + z[35];
    z[35]=z[1]*z[35];
    z[38]=2*z[10];
    z[39]= - z[12] + z[1];
    z[39]=z[39]*z[38];
    z[38]= - z[13] - z[38];
    z[38]=z[16]*z[38];
    z[21]=z[38] + z[39] + z[35] + z[31] + z[25] - static_cast<T>(1)+ z[21];
    z[21]=z[8]*z[21];
    z[25]=2*z[7];
    z[31]=z[13]*z[2];
    z[35]=static_cast<T>(2)- 5*z[31];
    z[35]=z[13]*z[35];
    z[35]=z[35] - z[25];
    z[38]=z[20]*z[13];
    z[39]= - z[22] + 3*z[38];
    z[39]=z[15]*z[39];
    z[40]=static_cast<T>(1)+ z[31];
    z[39]=3*z[40] + z[39];
    z[40]=3*z[6];
    z[39]=z[39]*z[40];
    z[41]=27*z[2] - 14*z[38];
    z[41]=z[13]*z[41];
    z[41]= - static_cast<T>(6)+ z[41];
    z[41]=z[15]*z[41];
    z[35]=z[39] + 3*z[35] + z[41];
    z[35]=z[6]*z[35];
    z[39]=10*z[2];
    z[41]=z[39] - 7*z[38];
    z[42]=z[15]*z[13];
    z[41]=z[41]*z[42];
    z[43]=z[15]*z[38];
    z[43]= - 11*z[31] - 16*z[43];
    z[43]=z[14]*z[43];
    z[44]= - static_cast<T>(1)- 14*z[31];
    z[44]=z[13]*z[44];
    z[41]=z[43] + z[44] + 2*z[41];
    z[41]=z[14]*z[41];
    z[43]= - z[38] + 4*z[2];
    z[44]=2*z[13];
    z[45]= - z[43]*z[44];
    z[46]=z[38] - z[2];
    z[47]= - z[46]*z[40];
    z[45]=z[47] + static_cast<T>(3)+ z[45];
    z[42]=z[6]*z[42]*z[45];
    z[45]=2*z[15];
    z[43]= - z[43]*z[45];
    z[47]=z[20]*z[15];
    z[48]=5*z[47];
    z[49]=z[14]*z[48];
    z[43]=z[43] + z[49];
    z[49]=npow(z[13],2);
    z[43]=z[14]*z[49]*z[43];
    z[42]=z[42] + z[43];
    z[42]=z[9]*z[42];
    z[43]=z[1]*npow(z[7],2);
    z[50]=z[40]*z[43];
    z[35]=z[42] + z[41] + z[50] - z[49] + z[35];
    z[35]=z[9]*z[35];
    z[41]=3*z[20];
    z[42]=z[41] - z[18];
    z[49]=z[13]*z[12];
    z[42]=z[42]*z[49];
    z[50]=z[23]*z[7];
    z[51]=z[50]*z[38];
    z[42]=z[42] + z[51];
    z[52]= - z[19] - z[42];
    z[52]=z[7]*z[52];
    z[53]=2*z[18];
    z[54]= - z[41] + z[53];
    z[54]=z[13]*z[54];
    z[52]=z[52] - 3*z[12] + z[54];
    z[52]=z[7]*z[52];
    z[54]=z[20]*z[12];
    z[55]=z[54]*z[7];
    z[41]=z[55] + z[41] + z[18];
    z[41]=z[7]*z[41];
    z[41]=z[22] + z[41];
    z[41]=z[15]*z[41];
    z[41]=z[41] + z[52] - static_cast<T>(3)+ z[31];
    z[41]=z[17]*z[41];
    z[52]=z[16]*z[15];
    z[56]= - z[22]*z[52];
    z[57]=3*z[47];
    z[46]=z[56] - z[57] + z[46];
    z[46]=z[4]*z[46];
    z[52]=z[52] + 3;
    z[52]=z[2]*z[52];
    z[52]=z[57] - z[38] + z[52];
    z[52]=z[17]*z[52];
    z[56]= - z[4] + z[17];
    z[56]=z[11]*z[47]*z[16]*z[56];
    z[57]=z[1]*z[7];
    z[58]=static_cast<T>(1)- z[57];
    z[58]=z[10]*z[9]*z[58];
    z[46]=z[56] + z[52] + z[58] + static_cast<T>(2)+ z[46];
    z[46]=z[11]*z[46];
    z[52]=2*z[3];
    z[56]=z[26]*z[52];
    z[27]= - z[20] - z[27];
    z[27]=z[3]*z[27];
    z[27]=z[27] - z[2] + z[28];
    z[27]=z[13]*z[27];
    z[27]=z[27] - static_cast<T>(1)+ z[56];
    z[27]=z[13]*z[27];
    z[21]=z[46] + z[35] - z[52] + z[27] + z[21] + z[41];
    z[27]=z[54] + z[30];
    z[35]=z[27]*z[3];
    z[41]=2*z[20];
    z[46]=z[35] - z[41] + z[18];
    z[46]=z[15]*z[46];
    z[27]=z[27]*z[13];
    z[27]=z[27] + z[18];
    z[41]=z[41] - z[27];
    z[41]=z[4]*z[41];
    z[41]=z[46] + z[41] + z[32] - z[38];
    z[41]=z[41]*z[40];
    z[46]= - z[22] + 7*z[32];
    z[52]=9*z[20];
    z[27]= - z[52] - z[27];
    z[27]=z[13]*z[27];
    z[27]=3*z[46] + z[27];
    z[27]=z[4]*z[27];
    z[35]=5*z[18] - z[35];
    z[35]=z[3]*z[35];
    z[46]=6*z[2];
    z[56]=z[46] - z[12];
    z[35]=21*z[38] - 3*z[56] + z[35];
    z[35]=z[15]*z[35];
    z[58]=9*z[12];
    z[59]= - z[32] + 20*z[2] - z[58];
    z[59]=z[3]*z[59];
    z[60]= - z[38] + 14*z[2] - z[58];
    z[60]=z[13]*z[60];
    z[61]=z[7]*z[12];
    z[27]=z[41] + z[35] + z[27] + 6*z[61] + z[60] + static_cast<T>(24)+ z[59];
    z[27]=z[27]*z[40];
    z[35]=4*z[18] + z[42];
    z[41]=3*z[7];
    z[35]=z[35]*z[41];
    z[42]=10*z[12];
    z[59]= - 23*z[2] + z[42];
    z[59]=z[12]*z[59];
    z[59]=35*z[20] + z[59];
    z[44]=z[59]*z[44];
    z[35]=z[35] + z[44] + 22*z[2] - 31*z[12];
    z[35]=z[7]*z[35];
    z[44]=18*z[20] - z[18];
    z[44]=z[44]*z[49];
    z[44]=z[51] + 7*z[18] + z[44];
    z[44]=z[7]*z[44];
    z[51]=17*z[20] - 18*z[18];
    z[51]=z[13]*z[51];
    z[59]=z[4]*z[18];
    z[44]=24*z[59] + z[44] + 15*z[2] + z[51];
    z[51]=3*z[5];
    z[44]=z[44]*z[51];
    z[59]= - 50*z[2] + 29*z[12];
    z[59]=z[3]*z[59];
    z[60]= - 19*z[2] + 40*z[12];
    z[60]=z[13]*z[60];
    z[62]= - 3*z[2] - 41*z[32];
    z[62]=z[4]*z[62];
    z[63]=z[15]*z[32];
    z[35]=z[44] + 18*z[63] + 2*z[62] + z[35] + z[60] - static_cast<T>(78)+ z[59];
    z[35]=z[5]*z[35];
    z[44]= - static_cast<T>(1)- 5*z[24];
    z[44]=z[3]*z[44];
    z[25]=z[44] - z[25];
    z[44]=9*z[2];
    z[59]=z[44] - 14*z[32];
    z[59]=z[59]*z[36];
    z[60]=3*z[3];
    z[62]=z[60]*z[20];
    z[63]=z[22] - z[62];
    z[63]=z[4]*z[63];
    z[64]= - static_cast<T>(1)- z[24];
    z[63]=3*z[64] + z[63];
    z[63]=z[63]*z[40];
    z[25]=z[63] + 3*z[25] + z[59];
    z[25]=z[25]*z[40];
    z[59]=z[40]*z[36];
    z[29]=2*z[29] + z[59];
    z[59]=z[32] - z[2];
    z[29]=z[40]*z[59]*z[29];
    z[34]=z[34]*z[41];
    z[37]=static_cast<T>(23)- z[37];
    z[37]=z[3]*z[37];
    z[34]=z[37] + z[34];
    z[34]=z[4]*z[7]*z[34];
    z[37]= - z[59]*z[60];
    z[37]= - static_cast<T>(10)+ z[37];
    z[40]=z[5]*z[59];
    z[37]=2*z[37] - 15*z[40];
    z[37]=z[5]*z[36]*z[37];
    z[29]=z[37] + z[34] + z[29];
    z[29]=z[1]*z[29];
    z[34]=11*z[2];
    z[37]=z[34] - z[32];
    z[37]=z[3]*z[37];
    z[37]= - static_cast<T>(7)+ z[37];
    z[40]=z[22] + z[12];
    z[33]=z[33] - z[40];
    z[33]=z[7]*z[33];
    z[33]=2*z[37] + z[33];
    z[33]=z[33]*z[41];
    z[37]=32*z[4];
    z[63]=z[61]*z[37];
    z[33]=z[33] + z[63];
    z[33]=z[4]*z[33];
    z[32]= - 32*z[2] + 21*z[32];
    z[32]=z[32]*z[36];
    z[36]= - static_cast<T>(11)+ 21*z[24];
    z[36]=z[3]*z[36];
    z[32]=z[32] + z[36] + 19*z[7];
    z[36]=z[4]*z[59];
    z[36]=11*z[24] + 8*z[36];
    z[36]=z[36]*z[51];
    z[32]=2*z[32] + z[36];
    z[32]=z[5]*z[32];
    z[22]=z[22]*z[3];
    z[36]=static_cast<T>(19)- z[22];
    z[36]=z[3]*z[36];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[7]*z[22];
    z[22]=z[36] + z[22];
    z[22]=z[22]*z[41];
    z[22]=z[29] + z[32] + z[25] + z[22] + z[33];
    z[22]=z[1]*z[22];
    z[25]=4*z[20];
    z[29]=z[25] - z[18];
    z[29]=z[7]*z[29];
    z[19]=z[25] + z[19];
    z[19]=z[4]*z[19];
    z[25]= - z[20] - z[18];
    z[25]=z[4]*z[12]*z[25];
    z[25]= - z[55] + z[25];
    z[25]=z[5]*z[25];
    z[19]=4*z[25] - z[48] + z[19] + z[29] + z[26];
    z[19]=z[5]*z[19];
    z[25]=z[4]*z[38];
    z[19]=z[25] - z[19];
    z[25]=z[39] + z[12];
    z[25]=z[12]*z[25];
    z[25]=z[52] + z[25];
    z[25]=z[3]*z[25];
    z[29]=z[18] - z[20];
    z[32]=z[15]*z[30];
    z[32]=z[32] - z[29];
    z[33]=24*z[5];
    z[32]=z[32]*z[33];
    z[25]=z[32] + 24*z[47] + z[25] + 25*z[2] - 7*z[12];
    z[25]=z[14]*z[25];
    z[32]=3*z[13];
    z[36]=z[56]*z[32];
    z[39]= - z[46] + 11*z[38];
    z[39]=z[39]*z[45];
    z[45]=z[7]*z[46];
    z[19]=z[25] + z[39] + z[45] + z[36] + static_cast<T>(7)- 12*z[24] - 6*z[19];
    z[25]=3*z[14];
    z[19]=z[19]*z[25];
    z[36]=4*z[49];
    z[39]=z[36] - z[61];
    z[39]=z[15]*z[39];
    z[45]=z[13] - z[7];
    z[39]=3*z[43] + 4*z[45] + z[39];
    z[39]=z[9]*z[39];
    z[43]=z[4] - z[13];
    z[43]=z[23]*z[43];
    z[43]=z[28] + z[43];
    z[43]=z[15]*z[43];
    z[45]=z[3] + 4*z[4];
    z[48]= - z[14] + z[45];
    z[48]=z[12]*z[48];
    z[36]=z[39] - z[57] + 4*z[43] + 3*z[61] - z[36] + static_cast<T>(4)+ z[48];
    z[39]=3*z[10];
    z[36]=z[36]*z[39];
    z[40]=z[40]*z[12];
    z[40]=z[40] + z[20];
    z[43]= - z[3]*z[40];
    z[48]=z[12] + z[2];
    z[43]=4*z[48] + z[43];
    z[43]=z[3]*z[43];
    z[48]=z[3]*z[48]*z[23];
    z[49]= - z[2] - z[28];
    z[49]=z[12]*z[49];
    z[48]=z[49] + z[48];
    z[48]=z[3]*z[48];
    z[48]= - z[2] + z[48];
    z[48]=z[13]*z[48];
    z[28]= - z[28] - z[50];
    z[28]=z[7]*z[28];
    z[49]= - static_cast<T>(3)- z[61];
    z[49]=z[7]*z[40]*z[49];
    z[46]=z[49] - z[46] + 5*z[12];
    z[46]=z[4]*z[46];
    z[28]=z[46] + z[28] + z[48] - static_cast<T>(3)+ z[43];
    z[43]=3*z[15];
    z[28]=z[28]*z[43];
    z[46]=z[29]*z[3];
    z[34]=z[34] - z[58];
    z[34]=2*z[34] + z[46];
    z[34]=z[3]*z[34];
    z[34]= - static_cast<T>(8)+ z[34];
    z[48]= - 29*z[2] + 20*z[12];
    z[48]=z[12]*z[48];
    z[48]=49*z[20] + z[48];
    z[48]=z[3]*z[48];
    z[29]=z[12]*z[29];
    z[29]=z[30] + z[29];
    z[29]=z[29]*z[60]*z[13];
    z[29]=z[29] + 6*z[12] + z[48];
    z[29]=z[13]*z[29];
    z[46]= - z[12] - z[46];
    z[46]=z[46]*z[41];
    z[29]=z[46] + 3*z[34] + z[29];
    z[29]=z[7]*z[29];
    z[34]=z[30]*z[3];
    z[18]=7*z[34] - 10*z[20] + 9*z[18];
    z[46]=z[3]*npow(z[2],4);
    z[46]=z[46] - z[30] + z[54];
    z[32]=z[46]*z[32];
    z[18]=7*z[18] + z[32];
    z[18]=z[13]*z[18];
    z[30]=z[60]*z[30];
    z[20]=89*z[20] - z[30];
    z[20]=z[3]*z[20];
    z[32]=z[34] - z[40];
    z[32]=z[32]*z[41];
    z[34]= - 59*z[2] + 27*z[12];
    z[18]=z[32] + z[18] + 2*z[34] + z[20];
    z[18]=z[7]*z[18];
    z[20]=26*z[2] - z[62];
    z[30]= - z[13]*z[30];
    z[20]=2*z[20] + z[30];
    z[20]=z[13]*z[20];
    z[30]=z[12]*z[38];
    z[30]=z[53] + z[30];
    z[30]=z[7]*z[30];
    z[30]=z[30] + z[2] + z[38];
    z[30]=z[30]*z[37];
    z[18]=z[30] + z[18] + z[20] + static_cast<T>(17)- 18*z[24];
    z[18]=z[4]*z[18];
    z[20]= - z[44] - z[42];
    z[20]=z[3]*z[20];
    z[24]= - z[47] + z[26];
    z[24]=z[24]*z[33];
    z[20]=z[20] + z[24];
    z[20]=z[14]*z[20];
    z[24]=z[3] - 8*z[5];
    z[20]=3*z[24] + z[20];
    z[20]=z[20]*z[25];
    z[24]=z[3] + 7*z[4];
    z[24]=z[24]*z[43];
    z[25]=z[15]*z[12]*z[45];
    z[23]= - z[14]*z[23];
    z[23]= - z[12] + z[23];
    z[23]=z[14]*z[3]*z[23];
    z[23]=z[25] + z[23];
    z[23]=z[23]*z[39];
    z[25]=z[5]*z[31];
    z[25]=20*z[13] - 51*z[25];
    z[25]=z[5]*z[25];
    z[20]=z[23] + z[20] + z[24] + z[25];
    z[20]=z[16]*z[20];

    r += z[18] + z[19] + z[20] + 3*z[21] + z[22] + z[27] + z[28] + 
      z[29] + z[35] + z[36];
 
    return r;
}

template double qg_2lLC_r40(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r40(const std::array<dd_real,31>&);
#endif
