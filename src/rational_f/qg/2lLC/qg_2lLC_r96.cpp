#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r96(const std::array<T,31>& k) {
  T z[69];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[30];
    z[7]=k[4];
    z[8]=k[12];
    z[9]=k[13];
    z[10]=k[5];
    z[11]=k[7];
    z[12]=k[3];
    z[13]=k[6];
    z[14]=k[15];
    z[15]=k[14];
    z[16]=k[28];
    z[17]=k[16];
    z[18]=2*z[7];
    z[19]=z[18] + z[10];
    z[20]=z[19]*z[10];
    z[21]=npow(z[7],2);
    z[20]=z[20] + z[21];
    z[19]=z[19]*z[12];
    z[19]= - z[19] + 2*z[20];
    z[19]=z[19]*z[12];
    z[22]=npow(z[12],2);
    z[23]=z[22]*z[5];
    z[24]=z[23]*z[21];
    z[25]=z[20]*z[10];
    z[19]=z[19] - z[24] - z[25];
    z[24]=z[19]*z[15];
    z[24]=57*z[24];
    z[26]=25*z[16];
    z[19]=z[19]*z[26];
    z[27]=51*z[21];
    z[28]=z[12]*z[7];
    z[29]=z[27] - 7*z[28];
    z[30]=z[5]*z[12];
    z[29]=z[29]*z[30];
    z[31]=33*z[7];
    z[32]=z[31] + 109*z[10];
    z[32]=z[10]*z[32];
    z[33]= - 7*z[12] - 76*z[7] - 127*z[10];
    z[33]=z[12]*z[33];
    z[19]=z[24] + z[19] + z[29] + z[33] - 76*z[21] + z[32];
    z[19]=z[16]*z[19];
    z[29]=2*z[10];
    z[32]=z[29] - z[12];
    z[19]=96*z[32] + z[19];
    z[19]=z[15]*z[19];
    z[33]=npow(z[10],2);
    z[34]= - z[21] + z[33];
    z[35]=z[21] + z[28];
    z[35]=z[35]*z[30];
    z[36]=3*z[7];
    z[37]=z[12] - z[36] - 4*z[10];
    z[37]=z[12]*z[37];
    z[34]=z[35] + 2*z[34] + z[37];
    z[34]=z[34]*z[26];
    z[35]=45*z[21];
    z[37]= - z[35] - 19*z[28];
    z[37]=z[5]*z[37];
    z[38]=2*z[12];
    z[39]= - 5*z[10] - z[38];
    z[34]=z[34] + 13*z[39] + z[37];
    z[34]=z[16]*z[34];
    z[37]=13*z[30];
    z[39]= - static_cast<T>(21)- z[37];
    z[19]=z[19] + 2*z[39] + z[34];
    z[19]=z[15]*z[19];
    z[34]=13*z[11];
    z[39]=13*z[6];
    z[40]=z[34] - z[39];
    z[41]=z[12]*z[10];
    z[41]=z[41] - z[33];
    z[40]=z[41]*z[40];
    z[41]=z[10]*z[7];
    z[42]=z[41] + z[21];
    z[43]=z[10] + z[7];
    z[43]=z[43]*z[12];
    z[43]=z[43] + z[42];
    z[44]= - z[16] + z[14];
    z[43]=z[43]*z[44];
    z[40]=25*z[43] + z[40];
    z[40]=z[8]*z[40];
    z[43]=11*z[7];
    z[44]= - z[43] - z[29];
    z[44]=3*z[44] - 58*z[12];
    z[44]=z[14]*z[44];
    z[45]=3*z[12];
    z[46]= - z[29] - z[45];
    z[46]=z[46]*z[39];
    z[47]=z[12] + z[10];
    z[48]=z[33]*z[9];
    z[49]=2*z[47] + z[48];
    z[34]=z[49]*z[34];
    z[49]=8*z[12];
    z[50]= - z[49] - 9*z[7] - 8*z[10];
    z[50]=z[16]*z[50];
    z[51]=13*z[10];
    z[52]=z[7] - z[51];
    z[52]=z[9]*z[52];
    z[34]=z[40] + z[34] + z[46] + z[44] + z[52] - static_cast<T>(45)+ 4*z[50];
    z[34]=z[8]*z[34];
    z[18]=z[18] + z[12];
    z[18]=z[18]*z[12];
    z[18]=z[18] + z[21];
    z[18]=z[18]*z[30];
    z[32]=z[32]*z[38];
    z[38]=z[36] - z[10];
    z[38]=z[10]*z[38];
    z[32]= - z[18] + z[32] + 2*z[21] + z[38];
    z[32]=z[32]*z[26];
    z[38]=26*z[12];
    z[40]=19*z[7] - z[38];
    z[40]=z[12]*z[40];
    z[40]=z[35] + z[40];
    z[40]=z[5]*z[40];
    z[32]=z[32] + z[40] - 56*z[10] - 23*z[12];
    z[32]=z[16]*z[32];
    z[40]=4*z[7];
    z[44]=z[40] - z[10];
    z[44]=z[44]*z[10];
    z[46]=z[10] - z[7];
    z[50]=2*z[46];
    z[52]=z[50] - z[12];
    z[52]=z[52]*z[12];
    z[44]= - z[21] + z[44] + z[52];
    z[44]=z[44]*z[12];
    z[52]=z[41] - z[21];
    z[29]=z[52]*z[29];
    z[29]=z[44] - z[29];
    z[44]=51*z[9];
    z[44]=z[44]*z[29];
    z[18]= - z[44] - 19*z[18] + 70*z[21];
    z[29]= - z[29]*z[26];
    z[44]= - 46*z[7] - z[51];
    z[44]=z[10]*z[44];
    z[52]=91*z[12] + 21*z[7] - 97*z[10];
    z[52]=z[12]*z[52];
    z[29]=z[29] + z[52] + z[44] - z[18];
    z[29]=z[16]*z[29];
    z[29]= - 78*z[12] + z[29];
    z[29]=z[9]*z[29];
    z[44]=static_cast<T>(7)- 2*z[30];
    z[29]=z[29] + 3*z[44] + z[32];
    z[29]=z[9]*z[29];
    z[32]=5*z[5];
    z[44]=z[2]*z[13];
    z[40]= - z[5]*z[40];
    z[40]= - static_cast<T>(1)+ z[40];
    z[40]=z[40]*z[44]*z[32];
    z[52]=z[5]*z[7];
    z[53]= - static_cast<T>(1)- 6*z[52];
    z[32]=z[53]*z[32];
    z[53]=z[15]*z[10];
    z[54]= - static_cast<T>(25)+ 48*z[53];
    z[54]=z[15]*z[54];
    z[32]=z[32] + z[54];
    z[54]=11*z[10];
    z[55]=z[33]*z[15];
    z[56]=z[54] - 4*z[55];
    z[56]=z[15]*z[56];
    z[57]=z[21]*z[5];
    z[57]= - z[7] + 20*z[57];
    z[57]=z[5]*z[57];
    z[57]= - static_cast<T>(18)+ z[57];
    z[56]=3*z[57] + 8*z[56];
    z[56]=z[13]*z[56];
    z[32]=z[40] + 2*z[32] + z[56];
    z[32]=z[2]*z[32];
    z[40]=z[33]*z[13];
    z[56]=4*z[40];
    z[57]=z[15]*z[56];
    z[57]=23*z[53] + z[57];
    z[58]=19*z[10] - 8*z[55];
    z[58]=z[15]*z[58]*z[44];
    z[57]=3*z[57] + 4*z[58];
    z[57]=z[2]*z[57];
    z[58]=npow(z[2],2);
    z[59]=z[58]*z[14];
    z[60]=z[59]*z[40];
    z[61]=19*z[60];
    z[62]= - z[15]*z[61];
    z[57]=z[57] + z[62];
    z[57]=z[14]*z[57];
    z[62]=z[28] - z[21];
    z[63]=z[62]*z[5];
    z[64]= - 60*z[63] - 111*z[7] + 46*z[12];
    z[64]=z[5]*z[64];
    z[65]=10*z[5];
    z[66]=npow(z[7],3);
    z[67]= - z[66]*z[65];
    z[67]=11*z[21] + z[67];
    z[68]=2*z[5];
    z[67]=z[67]*z[68];
    z[46]=7*z[46] + z[67];
    z[46]=3*z[46] - z[55];
    z[46]=z[13]*z[46];
    z[55]=3*z[10];
    z[67]=z[55] + z[12];
    z[67]=z[15]*z[67];
    z[32]=z[57] + z[32] + z[46] + 21*z[67] + static_cast<T>(59)+ z[64];
    z[32]=z[14]*z[32];
    z[46]=23*z[10];
    z[56]= - z[46] - z[56];
    z[57]=z[10]*z[44];
    z[56]=3*z[56] - 76*z[57];
    z[56]=z[2]*z[56];
    z[56]=z[56] + z[61];
    z[56]=z[14]*z[56];
    z[46]=z[13]*z[46];
    z[46]=75*z[44] + static_cast<T>(72)+ z[46];
    z[46]=z[2]*z[46];
    z[46]=z[56] + z[46] - 20*z[40] - 122*z[10] - z[45];
    z[46]=z[14]*z[46];
    z[56]= - z[5] + z[13];
    z[57]=z[30] + 2;
    z[56]=z[2]*z[57]*z[56];
    z[57]=z[47]*z[13];
    z[61]=static_cast<T>(1)- z[30];
    z[56]=z[56] + 2*z[61] + z[57];
    z[56]=z[2]*z[56];
    z[61]= - z[44] - 1;
    z[55]=z[55]*z[61];
    z[40]= - z[40] + z[55];
    z[40]=z[2]*z[40];
    z[40]=z[40] + z[60];
    z[40]=z[14]*z[40];
    z[40]=z[40] - z[45] + z[56];
    z[40]=z[40]*z[39];
    z[55]=npow(z[5],2);
    z[37]= - static_cast<T>(20)- z[37];
    z[37]=z[37]*z[55];
    z[56]=z[13]*z[5];
    z[60]= - static_cast<T>(1)+ 4*z[30];
    z[60]=z[60]*z[56];
    z[37]=z[37] + 5*z[60];
    z[37]=z[2]*z[37];
    z[60]=static_cast<T>(54)+ 7*z[30];
    z[60]=z[5]*z[60];
    z[61]=static_cast<T>(2)+ 27*z[30];
    z[61]=z[13]*z[61];
    z[37]=z[37] + z[60] + z[61];
    z[37]=z[2]*z[37];
    z[57]=z[57] + z[30];
    z[37]=z[40] + z[46] + z[37] + static_cast<T>(63)+ 20*z[57];
    z[37]=z[6]*z[37];
    z[40]= - 5*z[7] - z[10];
    z[40]=z[10]*z[40];
    z[40]= - 4*z[21] + z[40];
    z[46]=13*z[12];
    z[54]= - z[46] + 28*z[7] + z[54];
    z[45]=z[54]*z[45];
    z[28]=17*z[21] - 13*z[28];
    z[54]=3*z[30];
    z[28]=z[28]*z[54];
    z[24]=z[24] + z[28] + 19*z[40] + z[45];
    z[24]=z[15]*z[24];
    z[28]=z[43] - z[46];
    z[28]=z[12]*z[28];
    z[28]= - 15*z[21] + z[28];
    z[28]=z[5]*z[28];
    z[24]=z[24] + 3*z[28] + z[49] - 34*z[7] + 69*z[10];
    z[24]=z[15]*z[24];
    z[28]= - z[12]*z[62];
    z[28]= - z[66] + z[28];
    z[28]=z[28]*z[65];
    z[40]=32*z[12];
    z[43]= - 29*z[7] + z[40];
    z[43]=z[12]*z[43];
    z[28]=z[28] + 29*z[21] + z[43];
    z[28]=z[5]*z[28];
    z[43]=z[47] + z[48];
    z[28]=z[28] + 3*z[43];
    z[43]=2*z[14];
    z[28]=z[28]*z[43];
    z[45]=static_cast<T>(10)+ z[54];
    z[47]=z[9]*z[10];
    z[24]=z[28] + 6*z[47] + 4*z[45] + z[24];
    z[24]=z[11]*z[24];
    z[28]=13*z[7];
    z[45]= - z[28] + 38*z[10];
    z[45]=z[45]*z[10];
    z[45]=z[45] - z[27];
    z[25]=z[26]*z[25];
    z[25]=z[25] - z[45];
    z[25]=z[16]*z[25];
    z[20]=z[20]*z[53];
    z[45]=25*z[20] - z[45];
    z[45]=z[11]*z[45];
    z[20]=z[26]*z[20];
    z[20]=z[45] + z[25] + z[20];
    z[20]=z[15]*z[20];
    z[25]=13*z[41];
    z[41]=z[25] - z[27];
    z[45]=z[41]*z[10];
    z[47]=z[48]*z[27];
    z[45]=z[45] + z[47];
    z[47]=z[21]*z[33]*z[26];
    z[47]=z[47] - z[45];
    z[47]=z[9]*z[47];
    z[42]= - z[10]*z[42]*z[26];
    z[25]=z[47] + z[42] - z[27] - z[25];
    z[25]=z[16]*z[25];
    z[27]=z[9]*z[45];
    z[27]=z[27] - z[41];
    z[27]=z[13]*z[27];
    z[25]=z[27] + z[25];
    z[25]=z[9]*z[25];
    z[27]=13*z[2];
    z[42]=z[27]*z[5];
    z[45]= - 45*z[10] - 26*z[2];
    z[45]=z[14]*z[45];
    z[45]=z[45] + static_cast<T>(45)+ z[42];
    z[45]=z[2]*z[45];
    z[47]=z[10]*z[59];
    z[47]= - z[58] + z[47];
    z[47]=z[47]*z[39];
    z[45]=z[47] + z[45];
    z[45]=z[6]*z[45];
    z[47]=static_cast<T>(1)- z[52];
    z[42]=45*z[47] + z[42];
    z[42]=z[14]*z[42];
    z[47]=45*z[5];
    z[42]=z[42] - z[47];
    z[42]=z[2]*z[42];
    z[48]=z[33]*npow(z[16],2);
    z[20]=z[45] - 25*z[48] + z[42] + z[25] + z[20];
    z[20]=z[3]*z[20];
    z[25]=30*z[63] + z[31] - z[38];
    z[25]=z[25]*z[68];
    z[31]=z[36] + z[10];
    z[31]=19*z[31] - z[46];
    z[31]=z[12]*z[31];
    z[36]= - 32*z[7] + z[51];
    z[36]=z[10]*z[36];
    z[18]=z[31] + z[36] + z[18];
    z[18]=z[9]*z[18];
    z[31]= - 7*z[7] + 38*z[12];
    z[31]=z[12]*z[31];
    z[31]= - z[35] + z[31];
    z[31]=z[5]*z[31];
    z[28]=z[28] - 19*z[12];
    z[18]=z[18] + 2*z[28] + z[31];
    z[18]=z[9]*z[18];
    z[18]=z[18] + 12*z[53] - static_cast<T>(33)+ z[25];
    z[18]=z[13]*z[18];
    z[25]=npow(z[5],3);
    z[28]=z[25]*z[27];
    z[28]= - z[28] + 58*z[55];
    z[31]=z[55]*z[2];
    z[35]=z[5] - z[31];
    z[35]=z[35]*z[39];
    z[35]=z[35] + z[28];
    z[35]=z[2]*z[35];
    z[35]= - z[47] + z[35];
    z[35]=z[1]*z[35];
    z[36]=z[5]*z[58]*z[39];
    z[38]= - z[2]*z[47];
    z[35]=z[35] + z[38] + z[36];
    z[35]=z[3]*z[35];
    z[25]=z[25]*z[2];
    z[36]= - 5*z[55] + 2*z[25];
    z[27]=z[36]*z[27];
    z[31]=z[5] + 2*z[31];
    z[31]=z[2]*z[31];
    z[31]=static_cast<T>(3)+ z[31];
    z[31]=z[31]*z[39];
    z[27]=z[35] + z[31] - 50*z[5] + z[27];
    z[27]=z[6]*z[27];
    z[31]=npow(z[9],2);
    z[35]= - z[31]*z[50];
    z[36]=z[6] - z[11];
    z[36]=z[8]*z[10]*z[36];
    z[35]=z[36] + z[35] - 5*z[6];
    z[33]=z[33]*z[31];
    z[33]=static_cast<T>(10)+ 13*z[33];
    z[33]=z[11]*z[33];
    z[33]=2*z[33] + 13*z[35];
    z[33]=z[8]*z[33];
    z[27]=z[33] + 26*z[31] + z[27];
    z[27]=z[1]*z[27];
    z[21]=z[21]*z[12];
    z[21]=z[21] - z[66];
    z[31]=20*z[5];
    z[21]=z[21]*z[31];
    z[21]=z[21] - 13*z[62];
    z[33]=z[65]*npow(z[7],4);
    z[33]= - z[33] + 29*z[66];
    z[35]= - z[33]*z[43];
    z[35]= - z[21] + z[35];
    z[35]=z[56]*z[35];
    z[21]= - z[5]*z[21];
    z[33]= - z[14]*z[33]*z[68];
    z[21]=z[21] + z[33];
    z[21]=z[11]*z[21];
    z[33]=z[13] + z[11];
    z[33]=z[3]*z[41]*z[33];
    z[21]=z[33] + z[21] + z[35];
    z[21]=z[17]*z[21];
    z[33]=45*z[55] - 26*z[25];
    z[33]=z[2]*z[33];
    z[28]= - z[2]*z[28];
    z[28]=z[47] + z[28];
    z[28]=z[1]*z[3]*z[28];
    z[28]=z[28] + z[47] + z[33];
    z[28]=z[1]*z[28];
    z[25]=z[55] + z[25];
    z[25]=z[2]*z[12]*z[25];
    z[25]=z[30] + z[25];
    z[22]=z[14]*z[22];
    z[22]= - z[40] - 45*z[22];
    z[22]=z[8]*z[22];
    z[22]=z[28] + 13*z[25] + z[22];
    z[22]=z[4]*z[22];
    z[25]=z[7] - z[12];
    z[25]=z[25]*z[31];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[56];
    z[28]=z[55]*z[44];
    z[25]=3*z[25] - 20*z[28];
    z[25]=z[2]*z[25];
    z[28]= - z[10] + z[12];
    z[23]=6*z[28] + z[23];
    z[23]=z[23]*z[26];
    z[23]=static_cast<T>(28)+ z[23];
    z[23]=z[16]*z[23];
    z[26]= - static_cast<T>(79)- 40*z[30];
    z[26]=z[5]*z[26];

    r += z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25]
       + z[26] + z[27] + z[29] + z[32] + z[34] + z[37];
 
    return r;
}

template double qg_2lLC_r96(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r96(const std::array<dd_real,31>&);
#endif
