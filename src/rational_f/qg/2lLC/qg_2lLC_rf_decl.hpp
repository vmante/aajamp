#ifndef QG_2LLC_RF_DECL
#define QG_2LLC_RF_DECL

#include <array>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
T qg_2lLC_r1(const std::array<T,31>&);

template <class T>
T qg_2lLC_r2(const std::array<T,31>&);

template <class T>
T qg_2lLC_r3(const std::array<T,31>&);

template <class T>
T qg_2lLC_r4(const std::array<T,31>&);

template <class T>
T qg_2lLC_r5(const std::array<T,31>&);

template <class T>
T qg_2lLC_r6(const std::array<T,31>&);

template <class T>
T qg_2lLC_r7(const std::array<T,31>&);

template <class T>
T qg_2lLC_r8(const std::array<T,31>&);

template <class T>
T qg_2lLC_r9(const std::array<T,31>&);

template <class T>
T qg_2lLC_r10(const std::array<T,31>&);

template <class T>
T qg_2lLC_r11(const std::array<T,31>&);

template <class T>
T qg_2lLC_r12(const std::array<T,31>&);

template <class T>
T qg_2lLC_r13(const std::array<T,31>&);

template <class T>
T qg_2lLC_r14(const std::array<T,31>&);

template <class T>
T qg_2lLC_r15(const std::array<T,31>&);

template <class T>
T qg_2lLC_r16(const std::array<T,31>&);

template <class T>
T qg_2lLC_r17(const std::array<T,31>&);

template <class T>
T qg_2lLC_r18(const std::array<T,31>&);

template <class T>
T qg_2lLC_r19(const std::array<T,31>&);

template <class T>
T qg_2lLC_r20(const std::array<T,31>&);

template <class T>
T qg_2lLC_r21(const std::array<T,31>&);

template <class T>
T qg_2lLC_r22(const std::array<T,31>&);

template <class T>
T qg_2lLC_r23(const std::array<T,31>&);

template <class T>
T qg_2lLC_r24(const std::array<T,31>&);

template <class T>
T qg_2lLC_r25(const std::array<T,31>&);

template <class T>
T qg_2lLC_r26(const std::array<T,31>&);

template <class T>
T qg_2lLC_r27(const std::array<T,31>&);

template <class T>
T qg_2lLC_r28(const std::array<T,31>&);

template <class T>
T qg_2lLC_r29(const std::array<T,31>&);

template <class T>
T qg_2lLC_r30(const std::array<T,31>&);

template <class T>
T qg_2lLC_r31(const std::array<T,31>&);

template <class T>
T qg_2lLC_r32(const std::array<T,31>&);

template <class T>
T qg_2lLC_r33(const std::array<T,31>&);

template <class T>
T qg_2lLC_r34(const std::array<T,31>&);

template <class T>
T qg_2lLC_r35(const std::array<T,31>&);

template <class T>
T qg_2lLC_r36(const std::array<T,31>&);

template <class T>
T qg_2lLC_r37(const std::array<T,31>&);

template <class T>
T qg_2lLC_r38(const std::array<T,31>&);

template <class T>
T qg_2lLC_r39(const std::array<T,31>&);

template <class T>
T qg_2lLC_r40(const std::array<T,31>&);

template <class T>
T qg_2lLC_r41(const std::array<T,31>&);

template <class T>
T qg_2lLC_r42(const std::array<T,31>&);

template <class T>
T qg_2lLC_r43(const std::array<T,31>&);

template <class T>
T qg_2lLC_r44(const std::array<T,31>&);

template <class T>
T qg_2lLC_r45(const std::array<T,31>&);

template <class T>
T qg_2lLC_r46(const std::array<T,31>&);

template <class T>
T qg_2lLC_r47(const std::array<T,31>&);

template <class T>
T qg_2lLC_r48(const std::array<T,31>&);

template <class T>
T qg_2lLC_r49(const std::array<T,31>&);

template <class T>
T qg_2lLC_r50(const std::array<T,31>&);

template <class T>
T qg_2lLC_r51(const std::array<T,31>&);

template <class T>
T qg_2lLC_r52(const std::array<T,31>&);

template <class T>
T qg_2lLC_r53(const std::array<T,31>&);

template <class T>
T qg_2lLC_r54(const std::array<T,31>&);

template <class T>
T qg_2lLC_r55(const std::array<T,31>&);

template <class T>
T qg_2lLC_r56(const std::array<T,31>&);

template <class T>
T qg_2lLC_r57(const std::array<T,31>&);

template <class T>
T qg_2lLC_r58(const std::array<T,31>&);

template <class T>
T qg_2lLC_r59(const std::array<T,31>&);

template <class T>
T qg_2lLC_r60(const std::array<T,31>&);

template <class T>
T qg_2lLC_r61(const std::array<T,31>&);

template <class T>
T qg_2lLC_r62(const std::array<T,31>&);

template <class T>
T qg_2lLC_r63(const std::array<T,31>&);

template <class T>
T qg_2lLC_r64(const std::array<T,31>&);

template <class T>
T qg_2lLC_r65(const std::array<T,31>&);

template <class T>
T qg_2lLC_r66(const std::array<T,31>&);

template <class T>
T qg_2lLC_r67(const std::array<T,31>&);

template <class T>
T qg_2lLC_r68(const std::array<T,31>&);

template <class T>
T qg_2lLC_r69(const std::array<T,31>&);

template <class T>
T qg_2lLC_r70(const std::array<T,31>&);

template <class T>
T qg_2lLC_r71(const std::array<T,31>&);

template <class T>
T qg_2lLC_r72(const std::array<T,31>&);

template <class T>
T qg_2lLC_r73(const std::array<T,31>&);

template <class T>
T qg_2lLC_r74(const std::array<T,31>&);

template <class T>
T qg_2lLC_r75(const std::array<T,31>&);

template <class T>
T qg_2lLC_r76(const std::array<T,31>&);

template <class T>
T qg_2lLC_r77(const std::array<T,31>&);

template <class T>
T qg_2lLC_r78(const std::array<T,31>&);

template <class T>
T qg_2lLC_r79(const std::array<T,31>&);

template <class T>
T qg_2lLC_r80(const std::array<T,31>&);

template <class T>
T qg_2lLC_r81(const std::array<T,31>&);

template <class T>
T qg_2lLC_r82(const std::array<T,31>&);

template <class T>
T qg_2lLC_r83(const std::array<T,31>&);

template <class T>
T qg_2lLC_r84(const std::array<T,31>&);

template <class T>
T qg_2lLC_r85(const std::array<T,31>&);

template <class T>
T qg_2lLC_r86(const std::array<T,31>&);

template <class T>
T qg_2lLC_r87(const std::array<T,31>&);

template <class T>
T qg_2lLC_r88(const std::array<T,31>&);

template <class T>
T qg_2lLC_r89(const std::array<T,31>&);

template <class T>
T qg_2lLC_r90(const std::array<T,31>&);

template <class T>
T qg_2lLC_r91(const std::array<T,31>&);

template <class T>
T qg_2lLC_r92(const std::array<T,31>&);

template <class T>
T qg_2lLC_r93(const std::array<T,31>&);

template <class T>
T qg_2lLC_r94(const std::array<T,31>&);

template <class T>
T qg_2lLC_r95(const std::array<T,31>&);

template <class T>
T qg_2lLC_r96(const std::array<T,31>&);

template <class T>
T qg_2lLC_r97(const std::array<T,31>&);

template <class T>
T qg_2lLC_r98(const std::array<T,31>&);

template <class T>
T qg_2lLC_r99(const std::array<T,31>&);

template <class T>
T qg_2lLC_r100(const std::array<T,31>&);

template <class T>
T qg_2lLC_r101(const std::array<T,31>&);

template <class T>
T qg_2lLC_r102(const std::array<T,31>&);

template <class T>
T qg_2lLC_r103(const std::array<T,31>&);

template <class T>
T qg_2lLC_r104(const std::array<T,31>&);

template <class T>
T qg_2lLC_r105(const std::array<T,31>&);

template <class T>
T qg_2lLC_r106(const std::array<T,31>&);

template <class T>
T qg_2lLC_r107(const std::array<T,31>&);

template <class T>
T qg_2lLC_r108(const std::array<T,31>&);

template <class T>
T qg_2lLC_r109(const std::array<T,31>&);

template <class T>
T qg_2lLC_r110(const std::array<T,31>&);

template <class T>
T qg_2lLC_r111(const std::array<T,31>&);

template <class T>
T qg_2lLC_r112(const std::array<T,31>&);

template <class T>
T qg_2lLC_r113(const std::array<T,31>&);

template <class T>
T qg_2lLC_r114(const std::array<T,31>&);

template <class T>
T qg_2lLC_r115(const std::array<T,31>&);

template <class T>
T qg_2lLC_r116(const std::array<T,31>&);

template <class T>
T qg_2lLC_r117(const std::array<T,31>&);

template <class T>
T qg_2lLC_r118(const std::array<T,31>&);

template <class T>
T qg_2lLC_r119(const std::array<T,31>&);

template <class T>
T qg_2lLC_r120(const std::array<T,31>&);

template <class T>
T qg_2lLC_r121(const std::array<T,31>&);

template <class T>
T qg_2lLC_r122(const std::array<T,31>&);

template <class T>
T qg_2lLC_r123(const std::array<T,31>&);

template <class T>
T qg_2lLC_r124(const std::array<T,31>&);

template <class T>
T qg_2lLC_r125(const std::array<T,31>&);

template <class T>
T qg_2lLC_r126(const std::array<T,31>&);

template <class T>
T qg_2lLC_r127(const std::array<T,31>&);

template <class T>
T qg_2lLC_r128(const std::array<T,31>&);

template <class T>
T qg_2lLC_r129(const std::array<T,31>&);

template <class T>
T qg_2lLC_r130(const std::array<T,31>&);

template <class T>
T qg_2lLC_r131(const std::array<T,31>&);

template <class T>
T qg_2lLC_r132(const std::array<T,31>&);

template <class T>
T qg_2lLC_r133(const std::array<T,31>&);

template <class T>
T qg_2lLC_r134(const std::array<T,31>&);

template <class T>
T qg_2lLC_r135(const std::array<T,31>&);

template <class T>
T qg_2lLC_r136(const std::array<T,31>&);

template <class T>
T qg_2lLC_r137(const std::array<T,31>&);

template <class T>
T qg_2lLC_r138(const std::array<T,31>&);

template <class T>
T qg_2lLC_r139(const std::array<T,31>&);

template <class T>
T qg_2lLC_r140(const std::array<T,31>&);

template <class T>
T qg_2lLC_r141(const std::array<T,31>&);

template <class T>
T qg_2lLC_r142(const std::array<T,31>&);

template <class T>
T qg_2lLC_r143(const std::array<T,31>&);

template <class T>
T qg_2lLC_r144(const std::array<T,31>&);

template <class T>
T qg_2lLC_r145(const std::array<T,31>&);

template <class T>
T qg_2lLC_r146(const std::array<T,31>&);

template <class T>
T qg_2lLC_r147(const std::array<T,31>&);

template <class T>
T qg_2lLC_r148(const std::array<T,31>&);

template <class T>
T qg_2lLC_r149(const std::array<T,31>&);

template <class T>
T qg_2lLC_r150(const std::array<T,31>&);

template <class T>
T qg_2lLC_r151(const std::array<T,31>&);

template <class T>
T qg_2lLC_r152(const std::array<T,31>&);

template <class T>
T qg_2lLC_r153(const std::array<T,31>&);

template <class T>
T qg_2lLC_r154(const std::array<T,31>&);

template <class T>
T qg_2lLC_r155(const std::array<T,31>&);

template <class T>
T qg_2lLC_r156(const std::array<T,31>&);

template <class T>
T qg_2lLC_r157(const std::array<T,31>&);

template <class T>
T qg_2lLC_r158(const std::array<T,31>&);

template <class T>
T qg_2lLC_r159(const std::array<T,31>&);

template <class T>
T qg_2lLC_r160(const std::array<T,31>&);

template <class T>
T qg_2lLC_r161(const std::array<T,31>&);

template <class T>
T qg_2lLC_r162(const std::array<T,31>&);

template <class T>
T qg_2lLC_r163(const std::array<T,31>&);

template <class T>
T qg_2lLC_r164(const std::array<T,31>&);

template <class T>
T qg_2lLC_r165(const std::array<T,31>&);

template <class T>
T qg_2lLC_r166(const std::array<T,31>&);

template <class T>
T qg_2lLC_r167(const std::array<T,31>&);

template <class T>
T qg_2lLC_r168(const std::array<T,31>&);

template <class T>
T qg_2lLC_r169(const std::array<T,31>&);

template <class T>
T qg_2lLC_r170(const std::array<T,31>&);

template <class T>
T qg_2lLC_r171(const std::array<T,31>&);

template <class T>
T qg_2lLC_r172(const std::array<T,31>&);

template <class T>
T qg_2lLC_r173(const std::array<T,31>&);

#endif /* QG_2LLC_RF_DECL_H */
