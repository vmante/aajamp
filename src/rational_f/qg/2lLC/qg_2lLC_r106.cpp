#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r106(const std::array<T,31>& k) {
  T z[35];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[2];
    z[8]=k[10];
    z[9]=k[3];
    z[10]=k[6];
    z[11]=k[15];
    z[12]=k[16];
    z[13]=3*z[3];
    z[14]=z[13]*z[1];
    z[15]=z[14] - 4;
    z[16]=z[12]*z[9];
    z[17]=z[15] + z[16];
    z[17]=z[17]*z[6];
    z[18]=z[16] + 2;
    z[18]=z[18]*z[10];
    z[19]=z[2]*z[3];
    z[20]=z[19]*z[10];
    z[17]=z[17] - z[13] + z[18] + z[20];
    z[21]=2*z[12];
    z[22]=z[21] - z[13];
    z[22]=z[22]*z[6];
    z[23]=z[21] + z[3];
    z[23]=z[23]*z[10];
    z[22]=z[22] + z[23];
    z[23]=z[6]*z[12];
    z[24]=z[10]*z[12];
    z[23]=z[24] + z[23];
    z[23]=z[5]*z[3]*z[23];
    z[25]=5*z[23];
    z[26]=4*z[22] - z[25];
    z[26]=z[5]*z[26];
    z[26]= - 3*z[17] + z[26];
    z[26]=z[5]*z[26];
    z[27]=2*z[1];
    z[28]=npow(z[1],2);
    z[29]=z[28]*z[3];
    z[30]=z[27] - z[29];
    z[31]=z[30] - z[9];
    z[31]=z[31]*z[6];
    z[32]=z[19] + 1;
    z[33]=z[3]*z[1];
    z[34]=z[33] - z[32];
    z[34]=3*z[34] + z[31];
    z[26]=2*z[34] + z[26];
    z[26]=z[5]*z[26];
    z[22]= - z[23] + z[22];
    z[22]=z[5]*z[22];
    z[17]=z[22] - z[17];
    z[17]=z[5]*z[17];
    z[22]= - z[2]*z[13];
    z[15]=z[17] + z[31] + z[22] + z[15];
    z[15]=z[5]*z[15];
    z[17]=z[14]*z[2];
    z[15]=z[15] - z[17] - z[30];
    z[15]=z[5]*z[15];
    z[22]= - z[28]*z[19];
    z[15]=z[22] + z[15];
    z[15]=z[4]*z[15];
    z[15]=z[15] + z[26] + z[29] - z[17];
    z[15]=z[4]*z[15];
    z[17]=8*z[12];
    z[22]=5*z[3] + z[17];
    z[22]=z[10]*z[22];
    z[17]= - 15*z[3] + z[17];
    z[17]=z[6]*z[17];
    z[17]= - 9*z[23] + z[22] + z[17];
    z[17]=z[5]*z[17];
    z[14]=static_cast<T>(2)- z[14];
    z[14]=2*z[14] - z[16];
    z[14]=z[6]*z[14];
    z[14]=z[17] + z[14] - 2*z[20] + 6*z[3] - z[18];
    z[14]=z[5]*z[14];
    z[17]=z[27] - z[9];
    z[17]= - z[6]*z[17];
    z[14]=z[15] + z[14] + z[17] - 2*z[19] + static_cast<T>(2)+ z[33];
    z[14]=z[4]*z[14];
    z[15]=z[11]*z[7];
    z[17]=z[9]*z[11];
    z[18]= - z[15] - z[17];
    z[19]=3*z[9];
    z[20]=npow(z[7],2);
    z[22]= - z[11]*z[20];
    z[22]=z[19] + z[22] + 3*z[7];
    z[22]=z[10]*z[22];
    z[18]=3*z[18] + z[22];
    z[22]=npow(z[8],2);
    z[18]=z[18]*z[22];
    z[23]=z[11] - z[21];
    z[23]=z[10]*z[23];
    z[18]=z[23] + z[18];
    z[18]=z[8]*z[18];
    z[23]=npow(z[8],3);
    z[26]=3*z[11];
    z[27]= - z[26] + z[12];
    z[27]=z[10]*z[27]*z[23];
    z[28]=z[24]*z[11];
    z[29]=z[23]*z[2];
    z[30]=z[29]*z[28];
    z[27]=z[27] + z[30];
    z[27]=z[2]*z[27];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=3*z[15] - z[16];
    z[15]=z[10]*z[15];
    z[15]=z[26] + z[15];
    z[15]=z[15]*z[22];
    z[15]= - z[28] + z[15];
    z[15]=z[8]*z[15];
    z[15]=z[15] + z[27];
    z[15]=z[2]*z[15];
    z[15]=z[18] + z[15];
    z[15]=z[2]*z[15];
    z[18]=2*z[10];
    z[26]=static_cast<T>(1)+ z[16];
    z[18]=z[26]*z[18];
    z[19]= - z[7]*z[19];
    z[19]= - z[20] + z[19];
    z[19]=z[10]*z[19];
    z[19]= - 2*z[9] + z[19];
    z[19]=z[8]*z[19];
    z[19]= - static_cast<T>(2)+ z[19];
    z[19]=z[8]*z[19];
    z[18]=z[19] - z[11] + z[18];
    z[18]=z[8]*z[18];
    z[15]=z[15] + 2*z[24] + z[18];
    z[15]=z[2]*z[15];
    z[18]=z[10]*z[9];
    z[19]=z[18] - 1;
    z[19]=z[20]*z[19];
    z[20]=z[9] - z[1];
    z[20]=z[7]*z[20];
    z[19]=z[19] + z[20];
    z[19]=z[8]*z[19];
    z[20]=z[7] + z[1];
    z[19]=2*z[20] + z[19];
    z[19]=z[8]*z[19];
    z[20]=z[17] - 1;
    z[18]=z[19] - 2*z[18] + z[20];
    z[18]=z[8]*z[18];
    z[19]= - z[11] + z[12];
    z[19]=z[19]*z[23];
    z[23]=z[12]*z[11];
    z[26]=z[29]*z[23];
    z[19]=z[19] + z[26];
    z[19]=z[2]*z[19];
    z[26]=z[17] - z[16];
    z[26]=z[26]*z[22];
    z[23]= - z[23] + z[26];
    z[23]=z[8]*z[23];
    z[19]=z[23] + z[19];
    z[19]=z[2]*z[19];
    z[22]= - npow(z[9],2)*z[22];
    z[22]=z[22] + 1;
    z[22]=z[11]*z[22];
    z[22]= - z[21] + z[22];
    z[22]=z[8]*z[22];
    z[19]=z[22] + z[19];
    z[19]=z[2]*z[19];
    z[17]=2*z[16] + static_cast<T>(2)- z[17];
    z[17]=z[8]*z[17];
    z[17]=z[19] + z[21] + z[17];
    z[17]=z[2]*z[17];
    z[19]=z[9]*z[20];
    z[19]= - z[1] + z[19];
    z[19]=z[8]*z[19];
    z[17]=z[17] + z[19] + z[16] - 4;
    z[17]=z[6]*z[17];
    z[19]=z[24]*z[32];
    z[20]=z[12]*z[32];
    z[13]= - z[13] + z[20];
    z[13]=z[6]*z[13];
    z[13]=z[13] + z[19];
    z[13]=2*z[13] - z[25];
    z[13]=z[5]*z[13];
    z[16]=z[10]*z[16];

    r += z[13] + z[14] + z[15] + z[16] + z[17] + z[18];
 
    return r;
}

template double qg_2lLC_r106(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r106(const std::array<dd_real,31>&);
#endif
