#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r6(const std::array<T,31>& k) {
  T z[20];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[14];
    z[5]=k[15];
    z[6]=k[9];
    z[7]=k[10];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[7];
    z[11]=z[6]*z[1];
    z[12]=z[11] - 2;
    z[12]=z[12]*z[3];
    z[13]=2*z[6];
    z[12]=z[12] + z[13];
    z[14]=z[3]*z[9];
    z[15]=z[6]*z[8];
    z[14]=z[15] + z[14] - 1;
    z[15]=2*z[10];
    z[15]= - z[14]*z[15];
    z[15]=z[15] - z[12];
    z[15]=z[7]*z[15];
    z[11]= - static_cast<T>(2)- z[11];
    z[11]=z[3]*z[11];
    z[11]= - z[13] + z[11];
    z[11]=z[5]*z[11];
    z[16]=z[3]*z[2];
    z[13]= - z[8]*z[13];
    z[13]=z[13] + z[16];
    z[17]=z[10]*z[5];
    z[13]=z[13]*z[17];
    z[18]=z[8] - z[9];
    z[19]= - z[7]*z[8]*z[9];
    z[19]=z[19] + z[2] - z[18];
    z[19]=z[10]*z[19];
    z[16]=z[5]*z[1]*z[16];
    z[16]= - static_cast<T>(2)+ z[16] + z[19];
    z[16]=z[4]*z[16];
    z[18]= - z[10]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[7]*z[18];
    z[16]=z[16] + z[18] + z[10] + z[3] + z[5];
    z[16]=z[4]*z[16];
    z[11]=2*z[16] + z[15] + z[11] + z[13];
    z[11]=z[4]*z[11];
    z[12]=z[5]*z[12];
    z[13]=z[14]*z[17];
    z[12]=z[12] + 2*z[13];
    z[12]=z[7]*z[12];

    r += z[11] + z[12];
 
    return r;
}

template double qg_2lLC_r6(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r6(const std::array<dd_real,31>&);
#endif
