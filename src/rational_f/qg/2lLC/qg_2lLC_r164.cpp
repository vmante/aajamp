#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r164(const std::array<T,31>& k) {
  T z[67];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[5];
    z[9]=k[8];
    z[10]=k[7];
    z[11]=k[2];
    z[12]=k[23];
    z[13]=k[15];
    z[14]=k[6];
    z[15]=k[16];
    z[16]=z[9]*npow(z[6],2);
    z[17]=npow(z[8],2);
    z[18]=z[16]*z[17];
    z[19]=3*z[17];
    z[20]=z[6]*z[8];
    z[21]=z[19] - z[20];
    z[21]=z[6]*z[21];
    z[21]=z[21] + z[18];
    z[21]=z[9]*z[21];
    z[22]=z[17]*z[7];
    z[23]=z[22] + z[8];
    z[24]= - z[2]*z[23];
    z[21]=z[24] + z[21] + z[19] - 2*z[20];
    z[21]=z[14]*z[21];
    z[24]=3*z[7];
    z[25]=z[24]*z[17];
    z[26]=z[2]*z[7];
    z[27]=z[8] - z[22];
    z[27]=z[27]*z[26];
    z[28]=2*z[8];
    z[27]=z[27] - z[28] + z[25];
    z[27]=z[2]*z[27];
    z[29]=z[9]*z[6];
    z[30]= - z[17]*z[29];
    z[27]=z[27] + z[30] - z[19] - z[20];
    z[27]=z[13]*z[27];
    z[30]=z[20]*z[9];
    z[31]=z[7]*z[8];
    z[32]=static_cast<T>(1)+ z[31];
    z[32]=z[2]*z[32];
    z[32]=z[32] + z[6] + z[30];
    z[21]=z[27] + 3*z[32] + z[21];
    z[21]=z[12]*z[21];
    z[27]=6*z[8];
    z[32]=3*z[6];
    z[33]= - z[32] + z[27] + z[22];
    z[34]=z[17] + z[20];
    z[35]=2*z[6];
    z[34]=z[34]*z[35];
    z[18]=z[34] + z[18];
    z[18]=z[9]*z[18];
    z[34]=3*z[8];
    z[36]=z[34] - z[6];
    z[36]=z[36]*z[32];
    z[18]=z[36] + z[18];
    z[18]=z[9]*z[18];
    z[36]=2*z[7];
    z[37]=z[36]*z[17];
    z[38]=5*z[8];
    z[39]=z[38] + z[37];
    z[40]= - z[7]*z[39];
    z[40]= - static_cast<T>(3)+ z[40];
    z[40]=z[2]*z[40];
    z[18]=z[40] + 2*z[33] + z[18];
    z[18]=z[14]*z[18];
    z[20]= - z[17] + z[20];
    z[20]=z[9]*z[20];
    z[33]=z[28] - z[22];
    z[40]=z[7]*z[33];
    z[40]= - static_cast<T>(2)+ z[40];
    z[40]=z[40]*z[26];
    z[41]=3*z[31];
    z[40]=z[40] + static_cast<T>(4)- z[41];
    z[40]=z[2]*z[40];
    z[20]=z[40] + z[20] + 3*z[23] + z[35];
    z[20]=z[13]*z[20];
    z[23]=14*z[6];
    z[40]=3*z[30] - z[34] + z[23];
    z[40]=z[9]*z[40];
    z[42]=3*z[2];
    z[43]=z[6]*z[4]*z[2];
    z[44]=5*z[6];
    z[45]= - z[43] + z[42] + z[44] - 8*z[1];
    z[45]=z[4]*z[45];
    z[46]= - static_cast<T>(2)+ z[41];
    z[46]=z[46]*z[26];
    z[47]= - z[7] - z[9];
    z[47]=z[1]*z[47];
    z[18]=z[21] + z[45] + z[20] + z[18] + z[46] + 3*z[47] + z[40] + 13
    + 6*z[31];
    z[18]=z[12]*z[18];
    z[20]=npow(z[7],2);
    z[21]=z[20]*z[32];
    z[40]= - static_cast<T>(1)+ z[41];
    z[40]=z[7]*z[40];
    z[40]=z[40] - z[21];
    z[45]=npow(z[5],3);
    z[40]=z[35]*z[45]*z[40];
    z[25]= - z[28] - z[25];
    z[25]=z[7]*z[25];
    z[25]=static_cast<T>(2)+ z[25];
    z[46]=2*z[5];
    z[25]=z[25]*z[46];
    z[25]=z[7] + z[25];
    z[47]=npow(z[5],2);
    z[25]=z[25]*z[47];
    z[48]= - static_cast<T>(7)- z[31];
    z[48]=z[48]*z[20];
    z[49]=z[6]*npow(z[7],3);
    z[48]=z[48] + z[49];
    z[48]=z[9]*z[48];
    z[25]=z[48] + z[25] + z[40];
    z[25]=z[1]*z[25];
    z[40]=z[32]*z[47];
    z[48]=6*z[5];
    z[49]= - z[7]*z[48];
    z[49]= - z[20] + z[49];
    z[49]=z[49]*z[40];
    z[50]=18*z[5];
    z[51]= - static_cast<T>(23)- 12*z[31];
    z[51]=z[7]*z[51];
    z[51]=z[51] - z[50];
    z[51]=z[5]*z[51];
    z[51]= - 6*z[20] + z[51];
    z[51]=z[5]*z[51];
    z[49]=z[51] + z[49];
    z[49]=z[6]*z[49];
    z[51]=8*z[8];
    z[52]=z[51] + z[22];
    z[53]= - z[52]*z[24];
    z[53]= - static_cast<T>(37)+ z[53];
    z[53]=z[7]*z[53];
    z[54]=static_cast<T>(3)+ z[31];
    z[54]=z[54]*z[21];
    z[53]=z[54] + z[53] + 15*z[5];
    z[53]=z[9]*z[53];
    z[54]= - static_cast<T>(3)+ z[31];
    z[54]=z[54]*z[20];
    z[38]=z[38] + 4*z[22];
    z[38]=z[38]*z[24];
    z[38]=static_cast<T>(1)+ z[38];
    z[38]=z[5]*z[38];
    z[38]= - z[36] + z[38];
    z[38]=z[5]*z[38];
    z[25]=z[25] + z[53] + z[49] + 2*z[54] + z[38];
    z[25]=z[1]*z[25];
    z[38]=npow(z[8],3);
    z[49]=z[38]*z[7];
    z[19]=z[19] + z[49];
    z[19]=z[7]*z[19];
    z[19]=z[34] + z[19];
    z[19]=z[7]*z[19];
    z[53]=z[6]*z[5];
    z[54]=z[17]*z[5];
    z[55]=z[8] - 10*z[54];
    z[55]=z[5]*z[55];
    z[55]=static_cast<T>(8)+ z[55];
    z[55]=z[55]*z[53];
    z[56]=z[54] + z[8];
    z[57]=z[5]*z[56];
    z[19]=z[55] + z[19] - 33*z[57];
    z[19]=z[6]*z[19];
    z[55]= - z[56]*z[53];
    z[55]= - z[54] + z[55];
    z[55]=z[55]*z[35];
    z[56]= - z[54]*z[16];
    z[55]=z[55] + z[56];
    z[55]=z[9]*z[55];
    z[56]=npow(z[8],4);
    z[57]=z[56]*z[7];
    z[57]=z[57] + 10*z[38];
    z[58]=z[57]*z[7];
    z[59]= - 24*z[17] - z[58];
    z[59]=z[7]*z[59];
    z[19]=6*z[55] + z[19] + z[59] - 13*z[54];
    z[19]=z[9]*z[19];
    z[55]=z[5]*z[8];
    z[59]=static_cast<T>(11)+ 14*z[55];
    z[59]=z[59]*z[47];
    z[60]= - z[45]*z[44];
    z[59]=z[59] + z[60];
    z[59]=z[6]*z[59];
    z[60]=4*z[8] - z[54];
    z[60]=z[5]*z[60];
    z[60]=static_cast<T>(2)+ z[60];
    z[60]=z[60]*z[46];
    z[59]=z[60] + z[59];
    z[23]= - 4*z[2] - z[23];
    z[23]=z[45]*z[23];
    z[60]=static_cast<T>(5)+ 8*z[55];
    z[60]=z[60]*z[47];
    z[23]=z[60] + z[23];
    z[23]=z[2]*z[23];
    z[23]=2*z[59] + z[23];
    z[23]=z[2]*z[23];
    z[59]=11*z[8];
    z[60]=z[59] - 14*z[54];
    z[60]=z[5]*z[60];
    z[60]=static_cast<T>(16)+ z[60];
    z[60]=z[5]*z[60];
    z[61]=z[47]*z[6];
    z[62]=static_cast<T>(11)+ 20*z[55];
    z[62]=z[62]*z[61];
    z[60]=z[60] + z[62];
    z[60]=z[6]*z[60];
    z[62]=z[38]*z[20];
    z[27]= - z[27] + z[62];
    z[27]=z[27]*z[36];
    z[51]= - z[51] - 7*z[54];
    z[62]=3*z[5];
    z[51]=z[51]*z[62];
    z[19]=z[23] + z[19] + z[60] + z[27] + z[51];
    z[19]=z[14]*z[19];
    z[23]=z[38]*z[24];
    z[27]=5*z[17] + z[23];
    z[27]=z[5]*z[27]*z[36];
    z[27]= - z[31] + z[27];
    z[27]=z[5]*z[27];
    z[27]= - 8*z[7] + z[27];
    z[27]=z[5]*z[27];
    z[51]=z[52]*z[7];
    z[52]=static_cast<T>(10)+ z[51];
    z[52]=z[9]*z[7]*z[52];
    z[27]=z[27] + z[52];
    z[27]=z[1]*z[27];
    z[52]= - static_cast<T>(4)- z[51];
    z[52]=z[52]*z[36];
    z[60]=9*z[17];
    z[63]= - z[60] - 4*z[49];
    z[63]=z[7]*z[63];
    z[59]= - z[59] + z[63];
    z[59]=z[5]*z[59];
    z[63]=z[41] + 1;
    z[59]=z[59] - z[63];
    z[59]=z[59]*z[62];
    z[49]=z[60] + z[49];
    z[60]=z[49]*z[7];
    z[64]=18*z[8];
    z[65]=z[60] + z[64];
    z[65]=z[65]*z[7];
    z[65]=z[65] + 10;
    z[66]=z[9]*z[65];
    z[27]=z[27] + 3*z[66] + z[52] + z[59];
    z[27]=z[1]*z[27];
    z[52]= - 57*z[8] - 4*z[60];
    z[52]=z[7]*z[52];
    z[23]=14*z[17] + z[23];
    z[23]=z[23]*z[36];
    z[23]=15*z[8] + z[23];
    z[23]=z[5]*z[23];
    z[57]=z[57]*z[24];
    z[57]=91*z[17] + z[57];
    z[57]=z[7]*z[57];
    z[59]=30*z[6];
    z[57]= - z[59] + 60*z[8] + z[57];
    z[57]=z[9]*z[57];
    z[51]=static_cast<T>(7)+ z[51];
    z[51]=z[51]*z[26];
    z[23]=z[51] + z[27] + z[57] - 33*z[53] + z[23] - static_cast<T>(28)+ z[52];
    z[23]=z[10]*z[23];
    z[27]=z[6]*z[7];
    z[51]= - z[47]*z[27];
    z[52]=z[7] - z[5];
    z[52]=z[5]*z[52];
    z[51]=z[52] + z[51];
    z[51]=z[51]*z[32];
    z[52]=5*z[7];
    z[53]=z[27] + 1;
    z[57]=z[9]*z[53];
    z[51]= - 8*z[57] + z[51] - z[52] + z[48];
    z[57]= - z[5]*z[24];
    z[57]= - z[20] + z[57];
    z[57]=z[5]*z[57];
    z[20]= - z[20]*z[61];
    z[20]=z[57] + z[20];
    z[20]=z[6]*z[20];
    z[57]= - z[7] - z[5];
    z[46]=z[57]*z[46];
    z[20]=z[46] + z[20];
    z[46]=4*z[7];
    z[21]= - z[46] + z[21];
    z[21]=z[9]*z[21];
    z[20]=3*z[20] + z[21];
    z[20]=z[1]*z[20];
    z[20]=2*z[51] + z[20];
    z[20]=z[1]*z[20];
    z[21]=4*z[1];
    z[43]=z[43] - z[2] - z[44] + z[21];
    z[43]=6*z[43];
    z[43]=z[10]*z[43];
    z[51]= - z[52] - z[62];
    z[52]=z[47]*z[2];
    z[40]=6*z[52] + 2*z[51] + z[40];
    z[40]=z[2]*z[40];
    z[51]=11*z[53];
    z[16]= - z[16]*z[51];
    z[53]= - z[6]*z[24];
    z[53]= - z[26] - static_cast<T>(1)+ z[53];
    z[53]=z[2]*z[53];
    z[57]= - static_cast<T>(21)- 31*z[27];
    z[57]=z[6]*z[57];
    z[16]=10*z[53] + z[57] + z[16];
    z[16]=z[13]*z[16];
    z[51]= - z[29]*z[51];
    z[53]= - z[6]*z[36];
    z[53]= - static_cast<T>(1)+ z[53];
    z[16]=z[16] + z[40] + z[20] + 10*z[53] + z[51] + z[43];
    z[16]=z[4]*z[16];
    z[20]= - z[8] + z[54];
    z[20]=z[20]*z[62];
    z[20]= - 19*z[27] - 2*z[63] + z[20];
    z[20]=z[6]*z[20];
    z[20]=z[20] + 3*z[54] - z[8] + z[37];
    z[20]=z[9]*z[20];
    z[27]=2*z[54] - z[33];
    z[27]=z[27]*z[62];
    z[40]=z[5]*z[31];
    z[40]= - 21*z[7] + z[40];
    z[40]=z[40]*z[32];
    z[20]= - 60*z[26] + z[20] + z[40] + z[27] - static_cast<T>(8)- 27*z[31];
    z[20]=z[13]*z[20];
    z[27]=z[47]*z[1];
    z[40]=z[8]*z[47];
    z[40]=z[40] + z[27] - z[52];
    z[43]= - static_cast<T>(14)- 15*z[31];
    z[43]=z[7]*z[43];
    z[40]=z[43] + 9*z[40];
    z[40]=z[2]*z[40];
    z[43]=z[55] + static_cast<T>(2)+ z[31];
    z[46]=z[46] - z[62];
    z[21]=z[46]*z[21];
    z[46]=z[52] - z[5] - 2*z[27];
    z[46]=z[2]*z[46];
    z[27]=4*z[5] + z[27];
    z[27]=z[1]*z[27];
    z[27]=z[46] - static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[42];
    z[42]= - 9*z[5] + 2*z[9];
    z[42]=z[1]*z[42];
    z[42]=static_cast<T>(6)+ z[42];
    z[42]=z[1]*z[42];
    z[27]=z[42] + z[27];
    z[27]=z[4]*z[27];
    z[21]=z[27] + z[40] + 3*z[43] + z[21];
    z[27]= - z[28] + z[54];
    z[27]=z[5]*z[27];
    z[27]=z[31] + z[27];
    z[28]= - z[33]*z[24];
    z[28]= - static_cast<T>(5)+ z[28];
    z[26]=z[28]*z[26];
    z[26]=3*z[27] + z[26];
    z[26]=z[2]*z[26];
    z[26]=2*z[26] + 12*z[54] - z[8] - 13*z[22];
    z[26]=z[13]*z[26];
    z[21]=z[26] + 2*z[21];
    z[21]=z[3]*z[21];
    z[26]= - z[49]*z[24];
    z[26]= - 55*z[8] + z[26];
    z[26]=z[7]*z[26];
    z[22]=z[34] + z[22];
    z[22]=z[7]*z[22];
    z[22]= - static_cast<T>(7)+ z[22];
    z[22]=z[7]*z[22];
    z[27]= - static_cast<T>(19)- 10*z[55];
    z[27]=z[5]*z[27];
    z[22]=z[22] + z[27];
    z[22]=z[22]*z[32];
    z[27]= - z[30]*z[50];
    z[22]=z[27] + z[22] + 6*z[55] - static_cast<T>(7)+ z[26];
    z[22]=z[9]*z[22];
    z[26]=z[7]*npow(z[8],5);
    z[26]=z[26] + 11*z[56];
    z[26]=z[26]*z[7];
    z[26]=z[26] + 47*z[38];
    z[26]=z[26]*z[7];
    z[27]= - z[44] + 12*z[8];
    z[27]=z[27]*z[35];
    z[26]=z[26] - z[27] + 37*z[17];
    z[26]=z[26]*z[9];
    z[17]=z[58] + 27*z[17];
    z[17]=z[17]*z[7];
    z[17]=z[17] + z[64] - 10*z[6];
    z[27]=z[65]*z[2];
    z[17]=z[26] + z[27] - 2*z[17];
    z[26]=z[14] + z[10];
    z[17]=z[15]*z[17]*z[26];
    z[26]= - 5*z[55] - static_cast<T>(7)- 2*z[31];
    z[26]=z[26]*z[48];
    z[27]=static_cast<T>(19)- z[41];
    z[27]=z[7]*z[27];
    z[26]=z[27] + z[26];
    z[26]=z[5]*z[26];
    z[27]= - z[24]*z[61];
    z[26]=z[26] + z[27];
    z[26]=z[6]*z[26];
    z[27]=z[2] - z[1];
    z[27]=z[59] + 12*z[27];
    z[27]=z[45]*z[27];
    z[28]= - static_cast<T>(23)- 12*z[55];
    z[28]=z[28]*z[47];
    z[27]=z[28] + z[27];
    z[27]=z[2]*z[27];
    z[28]= - static_cast<T>(1)- z[29];
    z[28]=z[13]*z[28];
    z[29]= - z[13] + z[3];
    z[29]=z[11]*z[29]*z[9];
    z[28]=z[29] - z[9] + z[28];
    z[28]=z[4]*z[28];
    z[29]= - z[13]*z[9];
    z[30]= - z[1]*z[9];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[4]*z[30];
    z[30]= - z[13] + 6*z[30];
    z[30]=z[3]*z[30];
    z[28]=z[30] + z[29] + z[28];
    z[28]=z[11]*z[28];
    z[29]= - z[34] + z[37];
    z[29]=z[29]*z[36];
    z[29]= - static_cast<T>(63)+ z[29];
    z[29]=z[7]*z[29];
    z[24]= - z[39]*z[24];
    z[24]= - 31*z[55] - static_cast<T>(13)+ z[24];
    z[24]=z[5]*z[24];

    r += z[16] + z[17] + 6*z[18] + z[19] + z[20] + z[21] + z[22] + 
      z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29];
 
    return r;
}

template double qg_2lLC_r164(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r164(const std::array<dd_real,31>&);
#endif
