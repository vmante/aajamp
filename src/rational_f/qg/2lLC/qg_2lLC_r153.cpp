#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r153(const std::array<T,31>& k) {
  T z[66];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[13];
    z[8]=k[2];
    z[9]=k[10];
    z[10]=k[6];
    z[11]=k[15];
    z[12]=k[14];
    z[13]=k[11];
    z[14]=k[3];
    z[15]=k[16];
    z[16]=z[9]*z[6];
    z[17]=npow(z[2],2);
    z[18]=z[16]*z[17];
    z[19]=2*z[5];
    z[20]=3*z[2];
    z[21]=z[20] + z[19];
    z[21]=z[6]*z[21];
    z[22]= - z[2]*z[16];
    z[22]= - z[6] + z[22];
    z[22]=z[14]*z[22];
    z[21]=z[22] + z[18] - static_cast<T>(2)+ z[21];
    z[21]=z[14]*z[21];
    z[22]=2*z[2];
    z[23]=z[22] + z[5];
    z[24]= - z[5]*z[23];
    z[24]= - z[17] + z[24];
    z[24]=z[6]*z[24];
    z[25]=z[11]*z[5];
    z[26]=npow(z[5],2);
    z[27]=z[26]*z[11];
    z[28]=z[5] + z[27];
    z[28]=z[10]*z[28];
    z[28]=z[25] + z[28];
    z[28]=z[8]*z[28];
    z[29]=z[5] + z[2];
    z[21]=z[28] + z[21] + 2*z[29] + z[24];
    z[21]=z[12]*z[21];
    z[24]=2*z[11];
    z[28]=3*z[5];
    z[30]= - z[11]*z[28];
    z[30]= - static_cast<T>(2)+ z[30];
    z[30]=z[10]*z[30];
    z[30]= - z[24] + z[30];
    z[30]=z[8]*z[30];
    z[21]=z[30] + z[21];
    z[30]=z[6]*z[23];
    z[18]=z[18] + z[30];
    z[30]=z[5] - z[2];
    z[27]= - z[27] - z[30];
    z[27]=z[10]*z[27];
    z[25]=z[25] + 2;
    z[27]=z[25] - z[27];
    z[31]=4*z[2];
    z[32]= - z[14] - z[31];
    z[32]=z[16]*z[32];
    z[33]=2*z[6];
    z[32]= - z[11] - z[33] + z[32];
    z[32]=z[14]*z[32];
    z[18]=z[32] - 5*z[27] + 3*z[18] + 2*z[21];
    z[18]=z[12]*z[18];
    z[21]=z[3]*z[5];
    z[27]=z[21] - 1;
    z[27]=z[17]*z[27];
    z[32]=z[5]*z[2];
    z[27]=z[32] + z[27];
    z[27]=z[10]*z[27];
    z[34]=z[26]*z[6];
    z[34]=z[34] - z[30];
    z[35]=z[6]*z[5];
    z[36]= - static_cast<T>(1)+ z[35];
    z[36]=z[1]*z[36];
    z[36]=z[36] + z[34];
    z[36]=z[4]*z[36];
    z[35]= - static_cast<T>(2)+ z[35] + z[36];
    z[35]=z[1]*z[35];
    z[36]=z[32]*z[3];
    z[37]= - z[10]*z[2];
    z[37]=static_cast<T>(1)+ z[37];
    z[37]=z[14]*z[37];
    z[27]=z[37] + z[35] + z[27] - z[36] + z[20] - z[5];
    z[27]=z[7]*z[27];
    z[35]= - 3*z[17] + 2*z[32];
    z[35]=z[10]*z[35];
    z[37]=z[22] - z[5];
    z[35]=z[35] + 2*z[37];
    z[35]=z[3]*z[35];
    z[34]=z[4]*z[34];
    z[37]=z[6]*z[30];
    z[38]= - z[1]*z[4]*z[6];
    z[38]= - z[3] + z[38];
    z[38]=z[1]*z[38];
    z[27]=2*z[27] + z[38] + z[34] + z[37] + z[35];
    z[27]=z[7]*z[27];
    z[34]= - z[11]*z[32];
    z[34]=z[34] + z[36];
    z[34]=z[10]*z[34];
    z[35]=z[8]*z[3];
    z[25]=z[35] + z[34] - z[21] + z[25];
    z[25]=z[13]*z[25];
    z[18]=z[25] + z[18] + z[27];
    z[25]=z[30]*z[21];
    z[25]=z[5] + 5*z[25];
    z[25]=z[3]*z[25];
    z[25]= - static_cast<T>(7)+ z[25];
    z[25]=z[3]*z[25];
    z[27]=npow(z[5],3);
    z[34]=z[3]*z[27];
    z[34]= - z[26] - 5*z[34];
    z[34]=z[3]*z[34];
    z[35]=12*z[5];
    z[34]=z[35] + z[34];
    z[34]=z[3]*z[34];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[6]*z[34];
    z[37]=3*z[3];
    z[38]=z[26]*z[37];
    z[38]= - z[5] + z[38];
    z[38]=z[6]*z[38];
    z[39]=z[28] - z[2];
    z[40]= - z[3]*z[39];
    z[38]=z[40] + z[38];
    z[38]=z[4]*z[38];
    z[25]=z[38] + z[25] + z[34];
    z[34]=6*z[4];
    z[25]=z[25]*z[34];
    z[38]=10*z[3];
    z[40]=z[30]*z[38];
    z[40]=static_cast<T>(3)+ z[40];
    z[41]=npow(z[3],2);
    z[40]=z[40]*z[41];
    z[42]=z[38]*z[26];
    z[43]= - z[28] - z[42];
    z[43]=z[3]*z[43];
    z[43]=static_cast<T>(21)+ z[43];
    z[43]=z[6]*z[3]*z[43];
    z[44]=z[6]*z[21];
    z[44]= - z[3] + z[44];
    z[44]=z[44]*z[34];
    z[40]=z[44] + z[40] + z[43];
    z[40]=z[4]*z[40];
    z[43]=npow(z[3],3);
    z[43]=10*z[43];
    z[40]= - z[43] + z[40];
    z[40]=z[1]*z[40];
    z[44]=static_cast<T>(17)+ 10*z[21];
    z[44]=z[44]*z[41];
    z[45]= - z[5] - z[42];
    z[45]=z[3]*z[45];
    z[45]=static_cast<T>(18)+ z[45];
    z[45]=z[6]*z[45]*z[37];
    z[46]=6*z[9];
    z[33]=z[33] - z[9];
    z[33]=z[33]*z[46];
    z[25]=z[40] + z[33] + z[25] + z[44] + z[45];
    z[25]=z[1]*z[25];
    z[33]=3*z[11];
    z[40]=z[11]*z[2];
    z[44]=z[40] + 1;
    z[45]=z[3]*z[44];
    z[45]= - z[33] + z[45];
    z[45]=z[45]*z[37];
    z[47]=npow(z[11],2);
    z[48]=10*z[47];
    z[49]= - z[17]*z[48];
    z[49]=static_cast<T>(19)+ z[49];
    z[50]=z[10]*z[11];
    z[49]=z[49]*z[50];
    z[45]=z[45] + z[49];
    z[45]=z[8]*z[45];
    z[49]=10*z[11];
    z[51]=z[49]*z[17];
    z[52]=z[2] - z[51];
    z[52]=z[52]*z[33];
    z[52]=static_cast<T>(7)+ z[52];
    z[52]=z[11]*z[52];
    z[53]= - static_cast<T>(3)+ 5*z[40];
    z[53]=z[53]*z[37];
    z[54]=npow(z[2],3);
    z[48]=z[54]*z[48];
    z[48]= - 17*z[2] + z[48];
    z[55]=z[33]*z[10];
    z[48]=z[48]*z[55];
    z[56]=z[17]*z[11];
    z[57]=z[2] + z[56];
    z[57]=z[10]*z[57];
    z[44]=z[57] - z[44];
    z[44]=z[44]*z[46];
    z[44]=z[45] + z[44] + z[48] + z[52] + z[53];
    z[44]=z[9]*z[44];
    z[45]=static_cast<T>(1)- z[40];
    z[45]=z[45]*z[47];
    z[46]=static_cast<T>(5)+ z[40];
    z[46]=z[46]*z[33];
    z[46]=z[46] + 14*z[3];
    z[46]=z[3]*z[46];
    z[48]= - z[20] + 20*z[56];
    z[48]=z[11]*z[48];
    z[48]= - static_cast<T>(49)+ z[48];
    z[48]=z[11]*z[48];
    z[48]=z[48] - 4*z[3];
    z[48]=z[10]*z[48];
    z[52]=3*z[9];
    z[53]=z[41]*z[52];
    z[43]= - z[43] + z[53];
    z[43]=z[1]*z[43];
    z[43]=z[43] + z[48] + 20*z[45] + z[46] + z[44];
    z[43]=z[8]*z[43];
    z[44]=z[54]*z[11];
    z[45]= - z[17] + z[44];
    z[45]=z[45]*z[33];
    z[45]= - z[22] + z[45];
    z[46]=5*z[11];
    z[45]=z[45]*z[46];
    z[48]=npow(z[2],4);
    z[53]=z[48]*z[11];
    z[57]=z[54] - z[53];
    z[57]=z[57]*z[46];
    z[57]=6*z[17] + z[57];
    z[57]=z[11]*z[57];
    z[57]= - 12*z[2] + z[57];
    z[57]=z[6]*z[57];
    z[58]=5*z[47];
    z[59]= - z[48]*z[58];
    z[59]=9*z[17] + z[59];
    z[55]=z[59]*z[55];
    z[59]= - z[17] - z[44];
    z[59]=z[10]*z[59];
    z[59]=z[56] + z[59];
    z[60]= - z[6]*z[44];
    z[59]=z[60] + z[2] + 2*z[59];
    z[59]=z[59]*z[52];
    z[60]=z[3]*z[2];
    z[45]=z[59] + z[55] + z[57] + 6*z[60] + static_cast<T>(6)+ z[45];
    z[55]=2*z[9];
    z[45]=z[45]*z[55];
    z[57]=z[41]*npow(z[5],5);
    z[57]= - 23*z[27] + 5*z[57];
    z[57]=z[57]*z[3];
    z[59]=npow(z[5],4);
    z[60]=z[59]*z[3];
    z[60]=z[60] - z[27];
    z[61]=3*z[4];
    z[60]=z[60]*z[61];
    z[57]= - z[60] + z[57] + 9*z[26];
    z[60]=2*z[4];
    z[57]=z[57]*z[60];
    z[60]=z[54]*z[5];
    z[48]=z[60] + 2*z[48];
    z[48]=z[48]*z[49];
    z[60]=z[17]*z[5];
    z[62]=z[60] + z[54];
    z[48]=z[48] - 3*z[62];
    z[48]=z[48]*z[11];
    z[48]=z[48] - 26*z[32] - 31*z[17];
    z[48]=z[48]*z[11];
    z[62]=z[30]*z[5];
    z[63]=z[62] + z[17];
    z[63]=z[63]*z[42];
    z[64]=z[28]*z[17];
    z[63]=z[63] - z[64];
    z[63]=z[63]*z[3];
    z[64]= - z[2] + 5*z[5];
    z[65]=8*z[5];
    z[64]=z[64]*z[65];
    z[65]=23*z[17];
    z[63]= - z[63] + z[64] + z[65];
    z[63]=z[63]*z[3];
    z[19]=z[19] + z[2];
    z[19]=z[57] - z[48] - z[63] + 6*z[19];
    z[48]=z[10] + z[6];
    z[19]= - z[19]*z[48];
    z[57]=z[6]*z[11];
    z[50]= - z[57] - z[50];
    z[63]=z[58]*npow(z[2],5);
    z[63]= - z[63] + 11*z[54];
    z[50]=z[63]*z[50];
    z[53]=z[53] + z[54];
    z[48]=z[52]*z[53]*z[48];
    z[48]=z[48] + z[50];
    z[48]=z[48]*z[55];
    z[19]=z[48] + z[19];
    z[19]=z[15]*z[19];
    z[48]=z[2] - 5*z[56];
    z[48]=z[11]*z[48];
    z[48]=static_cast<T>(2)+ z[48];
    z[48]=z[48]*z[33];
    z[50]=2*z[17];
    z[44]= - z[50] + z[44];
    z[44]=z[44]*z[58];
    z[44]=static_cast<T>(6)+ z[44];
    z[44]=z[6]*z[44];
    z[53]= - z[2] + z[56];
    z[53]=z[6]*z[53];
    z[53]= - 2*z[40] + z[53];
    z[52]=z[53]*z[52];
    z[44]=z[52] + z[48] + z[44];
    z[44]=z[44]*z[55];
    z[31]=z[31] + z[5];
    z[48]= - z[31]*z[49];
    z[52]=z[29]*z[61];
    z[48]=z[52] + static_cast<T>(23)+ z[48];
    z[48]=z[47]*z[48];
    z[51]=z[20] - z[51];
    z[51]=z[11]*z[51];
    z[51]=static_cast<T>(15)+ z[51];
    z[51]=z[51]*z[57];
    z[16]=z[40]*z[16];
    z[16]=z[51] - 6*z[16];
    z[16]=z[9]*z[16];
    z[23]= - z[23]*z[49];
    z[23]=static_cast<T>(3)+ z[23];
    z[23]=z[6]*z[23]*z[47];
    z[16]=z[23] + z[16];
    z[16]=z[14]*z[16];
    z[23]=z[50] + z[32];
    z[40]=z[23]*z[49];
    z[40]=z[40] - 43*z[2] - 23*z[5];
    z[40]=z[11]*z[40];
    z[40]=static_cast<T>(6)+ z[40];
    z[40]=z[40]*z[57];
    z[16]=z[16] + z[44] + z[40] + z[48];
    z[16]=z[14]*z[16];
    z[40]=z[5]*z[23];
    z[40]=z[54] + z[40];
    z[40]=z[11]*z[40];
    z[44]= - z[2] - z[28];
    z[44]=z[5]*z[44];
    z[40]=z[40] + z[50] + z[44];
    z[40]=z[11]*z[40];
    z[42]=z[42]*z[30];
    z[44]=z[26] + z[42];
    z[44]=z[3]*z[44];
    z[39]=z[44] - 8*z[39] + z[40];
    z[39]=z[3]*z[39];
    z[20]=z[20] + z[5];
    z[20]=z[5]*z[20];
    z[20]=z[50] + z[20];
    z[20]=z[11]*z[20];
    z[20]= - 2*z[30] + z[20];
    z[20]=z[11]*z[20];
    z[20]=z[39] + static_cast<T>(2)+ z[20];
    z[22]=z[22] - z[28];
    z[21]=z[22]*z[21];
    z[39]=z[27]*z[37];
    z[39]= - 2*z[26] + z[39];
    z[39]=z[6]*z[39];
    z[21]=z[39] + z[5] + z[21];
    z[21]=z[21]*z[34];
    z[38]= - z[59]*z[38];
    z[38]= - z[27] + z[38];
    z[38]=z[38]*z[37];
    z[38]=97*z[26] + z[38];
    z[38]=z[3]*z[38];
    z[38]= - 24*z[5] + z[38];
    z[38]=z[6]*z[38];
    z[20]=z[21] + 3*z[20] + z[38];
    z[20]=z[4]*z[20];
    z[21]=z[30]*z[27]*z[41];
    z[21]= - 21*z[62] + 10*z[21];
    z[21]=z[3]*z[21];
    z[27]= - z[3]*z[30];
    z[27]=static_cast<T>(1)+ z[27];
    z[26]=z[34]*z[26]*z[27];
    z[21]=z[26] + z[35] + z[21];
    z[21]=z[4]*z[21];
    z[26]= - 4*z[54] - z[60];
    z[26]=z[26]*z[49];
    z[23]=3*z[23] + z[26];
    z[23]=z[11]*z[23];
    z[26]=53*z[5];
    z[23]=z[23] + 86*z[2] + z[26];
    z[23]=z[11]*z[23];
    z[27]= - z[32]*z[37];
    z[30]=8*z[2];
    z[27]=z[27] + z[30] - 19*z[5];
    z[27]=z[3]*z[27];
    z[21]=z[21] + z[27] + static_cast<T>(6)+ z[23];
    z[21]=z[10]*z[21];
    z[23]=z[17] + z[32];
    z[23]=z[11]*z[23];
    z[22]=z[23] + z[22];
    z[22]=z[22]*z[33];
    z[23]= - 20*z[36] - 14*z[2] + z[28];
    z[23]=z[3]*z[23];
    z[22]=z[23] + static_cast<T>(2)+ z[22];
    z[22]=z[3]*z[22];
    z[23]= - 2*z[54] - z[60];
    z[23]=z[23]*z[49];
    z[23]=z[23] + z[65] + 13*z[32];
    z[23]=z[11]*z[23];
    z[23]=z[23] + z[30] + 13*z[5];
    z[23]=z[11]*z[23];
    z[27]= - z[5]*z[29];
    z[27]=z[27] - z[42];
    z[27]=z[27]*z[37];
    z[26]=z[27] - 56*z[2] + z[26];
    z[26]=z[3]*z[26];
    z[23]=z[26] - static_cast<T>(12)+ z[23];
    z[23]=z[6]*z[23];
    z[17]=4*z[17] + z[32];
    z[17]=z[11]*z[17];
    z[17]=z[17] - z[31];
    z[17]=z[17]*z[46];
    z[17]=static_cast<T>(2)+ z[17];
    z[17]=z[17]*z[24];

    r += z[16] + z[17] + 3*z[18] + z[19] + z[20] + z[21] + z[22] + 
      z[23] + z[25] + z[43] + z[45];
 
    return r;
}

template double qg_2lLC_r153(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r153(const std::array<dd_real,31>&);
#endif
