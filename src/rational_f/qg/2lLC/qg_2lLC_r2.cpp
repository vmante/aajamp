#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r2(const std::array<T,31>& k) {
  T z[24];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[7];
    z[7]=k[12];
    z[8]=k[6];
    z[9]=k[15];
    z[10]=k[3];
    z[11]=k[5];
    z[12]=k[4];
    z[13]=z[5]*z[2];
    z[14]= - static_cast<T>(1)+ z[13];
    z[14]=z[3]*z[1]*z[14];
    z[15]=z[7]*z[9]*z[10]*z[11];
    z[13]=z[15] + z[14] - static_cast<T>(1)- z[13];
    z[13]=z[4]*z[13];
    z[14]=z[3]*z[5];
    z[15]= - z[2] - z[1];
    z[15]=z[15]*z[14];
    z[16]=2*z[9];
    z[17]=z[11] + z[10];
    z[17]=z[17]*z[16];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[7]*z[17];
    z[13]=z[13] + z[17] + z[15] + z[9];
    z[13]=z[4]*z[13];
    z[15]=z[6]*z[1];
    z[17]=z[8]*z[11];
    z[18]=static_cast<T>(1)+ z[17];
    z[18]=2*z[18] + z[15];
    z[18]=z[3]*z[18];
    z[19]=z[6]*z[10];
    z[20]=z[19] + static_cast<T>(4)+ z[17];
    z[20]=z[20]*z[16];
    z[18]=z[20] + z[6] + z[18];
    z[18]=z[7]*z[18];
    z[20]=z[8]*z[2];
    z[21]=z[20] + z[15];
    z[21]=z[5]*z[21];
    z[21]=z[8] + z[21];
    z[21]=z[3]*z[21];
    z[22]=2*z[19] + static_cast<T>(2)+ z[20];
    z[22]=z[5]*z[22];
    z[22]=z[8] + z[22];
    z[22]=z[9]*z[22];
    z[23]=z[5]*z[6];
    z[13]=2*z[13] + z[18] + z[22] + z[23] + z[21];
    z[13]=z[4]*z[13];
    z[15]=static_cast<T>(1)+ z[15];
    z[14]=z[15]*z[14];
    z[15]=z[5]*z[12];
    z[15]=z[15] + static_cast<T>(2)+ z[17];
    z[15]=z[3]*z[15];
    z[17]=static_cast<T>(2)+ z[19];
    z[17]=z[5]*z[17];
    z[15]=z[17] + z[15];
    z[15]=z[15]*z[16];
    z[14]=z[15] + z[23] + z[14];
    z[14]=z[7]*z[14];
    z[15]=static_cast<T>(1)+ z[20];
    z[15]=z[5]*z[15];
    z[15]=z[8] + z[15];
    z[15]=z[9]*z[3]*z[15];

    r += z[13] + z[14] + z[15];
 
    return r;
}

template double qg_2lLC_r2(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r2(const std::array<dd_real,31>&);
#endif
