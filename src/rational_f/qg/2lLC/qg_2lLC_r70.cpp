#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r70(const std::array<T,31>& k) {
  T z[182];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[22];
    z[7]=k[3];
    z[8]=k[13];
    z[9]=k[4];
    z[10]=k[12];
    z[11]=k[5];
    z[12]=k[7];
    z[13]=k[6];
    z[14]=k[15];
    z[15]=k[19];
    z[16]=k[14];
    z[17]=k[16];
    z[18]=k[17];
    z[19]=k[11];
    z[20]=k[28];
    z[21]=3*z[11];
    z[22]=npow(z[11],2);
    z[23]=z[22]*z[10];
    z[24]=3*z[10];
    z[25]=z[24]*z[7];
    z[26]= - z[11]*z[25];
    z[26]=z[26] - z[21] - z[23];
    z[26]=z[7]*z[26];
    z[27]=npow(z[7],2);
    z[28]=z[27]*z[14];
    z[29]=z[28]*z[23];
    z[26]=z[26] + z[29];
    z[26]=z[14]*z[26];
    z[30]=npow(z[9],2);
    z[31]=z[30]*z[5];
    z[32]=z[31]*z[27];
    z[33]=z[7]*z[9];
    z[34]=3*z[33];
    z[35]=z[34] - z[30];
    z[36]= - z[7]*z[35];
    z[36]=z[36] - z[32];
    z[36]=z[5]*z[36];
    z[37]=3*z[7];
    z[38]=2*z[9];
    z[39]=z[38] + z[11];
    z[40]= - z[37] + z[39];
    z[40]=z[7]*z[40];
    z[41]=z[11]*z[28];
    z[36]=z[41] + z[40] + z[36];
    z[36]=z[12]*z[36];
    z[40]=2*z[11];
    z[41]=z[40] + z[9];
    z[42]=z[41]*z[10];
    z[43]=z[42] + z[25];
    z[43]=z[7]*z[43];
    z[44]=z[33]*z[10];
    z[45]=3*z[9];
    z[46]= - z[45] + z[44];
    z[47]=z[5]*z[7];
    z[46]=z[46]*z[47];
    z[48]=z[11] + z[9];
    z[26]=z[36] + z[26] + z[46] - 3*z[48] + z[43];
    z[26]=z[18]*z[26];
    z[36]=z[33] + z[30];
    z[43]=2*z[7];
    z[46]=z[36]*z[43];
    z[46]=z[46] + z[32];
    z[49]= - z[5]*z[46];
    z[35]= - 3*z[35] + z[49];
    z[35]=z[5]*z[35];
    z[49]=5*z[11];
    z[50]=z[49] - z[43];
    z[50]=z[7]*z[50];
    z[28]=z[40]*z[28];
    z[28]=z[50] + z[28];
    z[28]=z[14]*z[28];
    z[50]=4*z[7];
    z[51]= - z[50] + z[39];
    z[28]=z[28] + 3*z[51] + z[35];
    z[28]=z[12]*z[28];
    z[35]=5*z[9];
    z[51]=z[11]*z[9];
    z[52]=z[3]*z[51];
    z[52]=z[52] - z[35] - z[21];
    z[52]=z[3]*z[52];
    z[53]=z[10]*z[9];
    z[54]=z[53] - 3;
    z[55]=z[7]*z[10];
    z[56]=z[55] - z[54];
    z[56]=z[7]*z[56];
    z[57]=3*z[5];
    z[58]= - z[33]*z[57];
    z[59]=14*z[9];
    z[56]=z[58] - z[59] + z[56];
    z[56]=z[5]*z[56];
    z[58]=2*z[23];
    z[60]= - z[21] - z[58];
    z[60]=z[7]*z[60];
    z[60]=z[60] + z[29];
    z[60]=z[14]*z[60];
    z[61]=z[10]*z[11];
    z[62]=z[55] - z[61];
    z[63]= - static_cast<T>(2)- z[62];
    z[63]=z[63]*z[37];
    z[64]=z[11] + z[23];
    z[60]=z[60] + 2*z[64] + z[63];
    z[60]=z[14]*z[60];
    z[63]=8*z[3];
    z[64]=3*z[14];
    z[65]=z[64] + z[63] + z[57];
    z[65]=z[2]*z[65];
    z[26]=z[26] + z[65] + z[28] + z[60] + z[56] + z[52] - z[25] - static_cast<T>(13)- 2
   *z[42];
    z[26]=z[18]*z[26];
    z[28]=z[39]*z[11];
    z[28]=z[28] + z[30];
    z[42]=z[39]*z[7];
    z[52]= - z[42] + 2*z[28];
    z[52]=z[52]*z[7];
    z[56]=z[28]*z[11];
    z[32]=z[52] - z[56] - z[32];
    z[52]=3*z[16];
    z[56]= - z[32]*z[52];
    z[60]=z[3]*z[11];
    z[28]=z[28]*z[60];
    z[65]=7*z[11];
    z[66]= - z[45] - z[65];
    z[66]=z[11]*z[66];
    z[67]=4*z[9];
    z[68]=z[7] + z[67] + z[65];
    z[68]=z[7]*z[68];
    z[69]=3*z[30];
    z[70]= - z[69] + z[33];
    z[70]=z[70]*z[47];
    z[71]=4*z[30];
    z[56]=z[56] + z[70] - z[28] + z[68] + z[71] + z[66];
    z[56]=z[16]*z[56];
    z[66]=z[40] - z[9];
    z[68]=z[11]*z[66];
    z[68]= - z[69] + z[68];
    z[68]=z[3]*z[68];
    z[70]=z[69] + z[33];
    z[70]=z[5]*z[70];
    z[56]=z[56] + z[70] + z[68] + z[49] + z[43];
    z[56]=z[16]*z[56];
    z[32]= - z[16]*z[32];
    z[68]=z[22] - z[30];
    z[70]=4*z[11];
    z[72]= - z[7] + z[45] + z[70];
    z[72]=z[7]*z[72];
    z[36]= - z[36]*z[47];
    z[32]=z[32] + z[36] - z[28] - 2*z[68] + z[72];
    z[32]=z[16]*z[32];
    z[36]=z[67] - z[11];
    z[36]=z[36]*z[11];
    z[36]=z[36] - z[30];
    z[72]=z[8]*z[36];
    z[72]= - z[70] + z[72];
    z[72]=z[8]*z[72];
    z[73]=2*z[8];
    z[74]=z[11] - z[9];
    z[75]=z[74]*z[8];
    z[76]=static_cast<T>(1)+ z[75];
    z[76]=z[76]*z[73];
    z[77]=npow(z[8],2);
    z[78]=z[77]*z[7];
    z[76]=z[76] - z[78];
    z[76]=z[7]*z[76];
    z[72]=z[76] - static_cast<T>(6)+ z[72];
    z[72]=z[7]*z[72];
    z[76]=z[30]*z[8];
    z[79]=z[76]*z[22];
    z[80]=z[51] + z[30];
    z[81]=z[11]*z[80];
    z[81]=z[81] - z[79];
    z[81]=z[8]*z[81];
    z[81]=z[22] + z[81];
    z[81]=z[3]*z[81];
    z[82]=z[51] - z[30];
    z[83]= - z[82]*z[73];
    z[84]=z[45] - z[11];
    z[83]=z[83] - z[84];
    z[83]=z[11]*z[83];
    z[85]=2*z[30];
    z[83]= - z[85] + z[83];
    z[83]=z[8]*z[83];
    z[86]=z[7]*z[8];
    z[87]=z[73]*z[9];
    z[88]=z[87] - 1;
    z[89]=z[86] + z[88];
    z[89]=z[7]*z[89];
    z[89]=z[76] + z[89];
    z[89]=z[89]*z[47];
    z[32]=z[32] + z[89] + z[81] + z[72] + 6*z[11] + z[83];
    z[32]=z[20]*z[32];
    z[72]=3*z[8];
    z[36]= - z[36]*z[72];
    z[36]=z[36] - z[45] + z[65];
    z[36]=z[8]*z[36];
    z[36]=static_cast<T>(5)+ z[36];
    z[36]=z[8]*z[36];
    z[81]=6*z[8];
    z[83]= - z[74]*z[81];
    z[83]= - static_cast<T>(7)+ z[83];
    z[83]=z[83]*z[77];
    z[89]=npow(z[8],3);
    z[90]=z[89]*z[37];
    z[83]=z[83] + z[90];
    z[83]=z[7]*z[83];
    z[90]=z[10]*z[48];
    z[90]=static_cast<T>(2)+ z[90];
    z[90]=z[10]*z[90];
    z[36]=z[83] + z[36] + z[90];
    z[36]=z[7]*z[36];
    z[83]= - z[69] + z[51];
    z[83]=z[11]*z[83];
    z[83]=z[83] + 3*z[79];
    z[83]=z[8]*z[83];
    z[83]=z[83] + z[69] + z[51];
    z[83]=z[3]*z[83];
    z[82]=z[82]*z[81];
    z[90]=z[67] + z[11];
    z[82]=z[82] + z[90];
    z[82]=z[11]*z[82];
    z[82]=z[71] + z[82];
    z[82]=z[8]*z[82];
    z[82]=z[83] + z[40] + z[82];
    z[82]=z[8]*z[82];
    z[83]=z[10]*z[80];
    z[83]=z[40] + z[83];
    z[83]=z[10]*z[83];
    z[91]=z[8]*z[9];
    z[92]=static_cast<T>(1)- z[91];
    z[92]=z[92]*z[73];
    z[92]=z[92] - z[78];
    z[92]=z[7]*z[92];
    z[93]=z[76] + z[9];
    z[94]=z[93]*z[8];
    z[92]= - z[94] + z[92];
    z[92]=z[7]*z[92];
    z[95]=3*z[76];
    z[92]= - z[95] + z[92];
    z[92]=z[5]*z[92];
    z[32]=z[32] + z[56] + z[92] + z[36] + z[83] - static_cast<T>(4)+ z[82];
    z[32]=z[20]*z[32];
    z[36]=z[49] + z[58];
    z[36]=z[7]*z[36];
    z[29]=z[36] - z[29];
    z[29]=z[14]*z[29];
    z[36]=z[22]*z[8];
    z[56]=z[40] - z[36];
    z[82]=9*z[8];
    z[56]=z[56]*z[82];
    z[56]=z[56] - 8*z[61];
    z[56]=z[7]*z[56];
    z[83]=13*z[11];
    z[29]=z[29] + z[56] - 5*z[23] - z[83] - 19*z[36];
    z[29]=z[14]*z[29];
    z[56]=8*z[11];
    z[92]=npow(z[3],2);
    z[96]=z[92]*z[56];
    z[97]=2*z[5];
    z[98]=9*z[78];
    z[99]= - 17*z[8] + z[98];
    z[99]=z[7]*z[99];
    z[99]=static_cast<T>(8)+ z[99];
    z[99]=z[99]*z[97];
    z[100]=npow(z[3],3);
    z[101]=z[100]*z[56];
    z[102]= - 17*z[3] + 26*z[8] - z[98];
    z[102]=z[5]*z[102];
    z[101]=z[101] + z[102];
    z[101]=z[1]*z[101];
    z[102]=11*z[8] - z[98];
    z[96]=z[101] + z[99] + 3*z[102] + z[96];
    z[96]=z[1]*z[96];
    z[99]=z[5]*z[3];
    z[101]=5*z[99];
    z[102]=z[100]*z[1];
    z[103]= - 8*z[102] + 12*z[92] + z[101];
    z[103]=z[1]*z[103];
    z[104]=4*z[5];
    z[105]=z[104]*z[92];
    z[106]=z[102]*z[5];
    z[105]=z[105] - z[106];
    z[106]=8*z[1];
    z[107]= - z[105]*z[106];
    z[107]=27*z[99] + z[107];
    z[107]=z[2]*z[107];
    z[108]=2*z[3];
    z[109]=z[108] - z[5];
    z[103]=z[107] + 2*z[103] + 10*z[109] - 7*z[14];
    z[103]=z[2]*z[103];
    z[107]= - z[8]*z[21];
    z[109]= - z[10]*z[40];
    z[107]=z[109] - static_cast<T>(5)+ z[107];
    z[109]=9*z[11];
    z[110]= - z[8]*z[109];
    z[110]=static_cast<T>(1)+ z[110];
    z[110]=z[8]*z[110];
    z[110]=z[110] + z[98];
    z[110]=z[110]*z[37];
    z[111]=8*z[8];
    z[112]=z[111] - z[98];
    z[112]=z[7]*z[112];
    z[112]=static_cast<T>(1)+ z[112];
    z[112]=z[112]*z[47];
    z[29]=z[103] + z[96] + z[29] + z[112] + 9*z[60] + 4*z[107] + z[110];
    z[29]=z[4]*z[29];
    z[26]=z[29] + z[32] + z[26];
    z[29]=z[35] - z[70];
    z[32]=z[74]*z[11];
    z[96]=z[32] + z[30];
    z[103]=16*z[8];
    z[107]= - z[96]*z[103];
    z[110]=z[7]*z[74]*z[111];
    z[107]=z[110] + 3*z[29] + z[107];
    z[107]=z[107]*z[43];
    z[110]=npow(z[11],3);
    z[112]=z[110]*z[73];
    z[113]=z[7]*z[36];
    z[113]=z[113] - z[22] - z[112];
    z[113]=z[7]*z[113];
    z[114]=npow(z[11],4);
    z[115]=z[114]*z[8];
    z[113]=z[113] + z[110] + z[115];
    z[113]=z[14]*z[113];
    z[29]= - z[29]*z[40];
    z[29]= - z[30] + z[29];
    z[116]=z[96]*z[11];
    z[117]=z[103]*z[116];
    z[118]=z[7]*z[76];
    z[118]=z[69] + 16*z[118];
    z[118]=z[118]*z[47];
    z[29]=16*z[113] + z[118] + z[107] + 3*z[29] + z[117];
    z[107]=6*z[16];
    z[29]=z[29]*z[107];
    z[113]=z[48]*z[24];
    z[117]=z[96]*z[8];
    z[118]=z[40] + z[117];
    z[119]=48*z[8];
    z[118]=z[118]*z[119];
    z[118]=static_cast<T>(67)+ z[118];
    z[75]=static_cast<T>(1)- z[75];
    z[75]=z[75]*z[111];
    z[75]=z[75] + z[10];
    z[120]=6*z[7];
    z[75]=z[75]*z[120];
    z[75]=z[75] + 2*z[118] - z[113];
    z[75]=z[7]*z[75];
    z[118]=z[45] - z[49];
    z[118]=z[11]*z[118];
    z[121]=npow(z[9],3);
    z[122]=z[121] - z[116];
    z[122]=z[8]*z[122];
    z[118]=z[122] - z[30] + z[118];
    z[118]=z[118]*z[119];
    z[119]=2*z[121];
    z[122]=z[30]*z[11];
    z[123]=z[119] + z[122];
    z[124]=z[123]*z[11];
    z[125]=npow(z[9],4);
    z[124]=z[124] + z[125];
    z[126]=z[124]*z[8];
    z[127]=z[85] + z[51];
    z[128]=z[127]*z[11];
    z[129]=z[128] + z[121];
    z[130]= - 2*z[129] + z[126];
    z[130]=z[130]*z[103];
    z[131]=31*z[9];
    z[132]=15*z[11];
    z[133]=z[131] + z[132];
    z[133]=z[11]*z[133];
    z[130]=z[130] + 16*z[30] + z[133];
    z[130]=z[130]*z[24];
    z[133]= - z[22]*z[38];
    z[133]=z[133] + z[79];
    z[133]=z[133]*z[103];
    z[134]=16*z[22];
    z[133]=z[133] + z[30] + z[134];
    z[135]=3*z[3];
    z[133]=z[133]*z[135];
    z[136]= - z[45] - z[76];
    z[136]=z[136]*z[111];
    z[136]=z[136] + z[53];
    z[136]=z[136]*z[37];
    z[136]=65*z[9] + z[136];
    z[136]=z[7]*z[136];
    z[136]= - z[69] + z[136];
    z[137]=z[30]*z[7];
    z[138]=z[10]*z[137];
    z[138]=5*z[30] + z[138];
    z[138]=z[7]*z[138]*z[57];
    z[136]=2*z[136] + z[138];
    z[136]=z[5]*z[136];
    z[138]=4*z[22];
    z[139]=z[110]*z[8];
    z[140]=z[138] + z[139];
    z[140]=z[140]*z[111];
    z[141]=z[36] + z[11];
    z[142]= - z[8]*z[141]*z[50];
    z[140]=z[142] + z[21] + z[140];
    z[140]=z[140]*z[50];
    z[142]= - 7*z[110] - z[115];
    z[142]=z[142]*z[103];
    z[140]=z[140] - 95*z[22] + z[142];
    z[140]=z[140]*z[64];
    z[29]=z[29] + z[140] + z[136] + z[133] + z[75] + z[130] + z[118] - 
   115*z[9] - 451*z[11];
    z[29]=z[16]*z[29];
    z[75]=z[87] + 1;
    z[87]=z[75]*z[103];
    z[87]=z[87] + z[10];
    z[87]=z[87]*z[37];
    z[118]=z[30]*z[10];
    z[130]=z[118] + z[38];
    z[44]=2*z[130] + z[44];
    z[44]=z[44]*z[57];
    z[44]=z[44] + z[87] + 9*z[53] + static_cast<T>(5)- 48*z[91];
    z[44]=z[7]*z[44];
    z[87]=6*z[9];
    z[133]= - z[87] + 5*z[76];
    z[44]=6*z[133] + z[44];
    z[44]=z[5]*z[44];
    z[133]=z[73]*z[22];
    z[136]=z[133] + z[21];
    z[140]= - z[136]*z[103];
    z[62]=static_cast<T>(17)+ z[140] - z[62];
    z[62]=z[62]*z[37];
    z[140]=z[110]*z[103];
    z[140]=61*z[22] + z[140];
    z[140]=z[140]*z[81];
    z[142]= - z[11]*z[37];
    z[142]= - 2*z[22] + z[142];
    z[142]=z[142]*z[64];
    z[62]=z[142] + z[62] + 247*z[11] + z[140];
    z[62]=z[14]*z[62];
    z[140]=z[73]*z[121];
    z[68]=z[140] - z[68];
    z[140]= - z[68]*z[111];
    z[142]=z[9] + z[65];
    z[140]=3*z[142] + z[140];
    z[140]=z[140]*z[81];
    z[142]=3*z[51];
    z[143]=z[142] + z[71];
    z[143]=z[143]*z[11];
    z[124]=z[124]*z[73];
    z[124]=z[143] - z[124] + z[121];
    z[143]=z[124]*z[111];
    z[144]=8*z[32];
    z[143]=z[143] + 19*z[30] - z[144];
    z[143]=z[8]*z[143];
    z[145]=11*z[9];
    z[143]=z[143] - z[145] - z[109];
    z[146]=6*z[10];
    z[143]=z[143]*z[146];
    z[147]= - z[88]*z[103];
    z[148]=5*z[10];
    z[147]= - 16*z[78] + z[147] + z[148];
    z[147]=z[147]*z[37];
    z[149]=z[45]*z[22];
    z[133]=z[133]*z[30];
    z[133]=z[149] - z[133];
    z[149]=z[133]*z[111];
    z[144]=z[149] + z[69] - z[144];
    z[144]=z[144]*z[73];
    z[149]=17*z[11];
    z[144]=z[144] - z[87] - z[149];
    z[144]=z[144]*z[135];
    z[29]=z[29] + z[62] + z[44] + z[144] + z[147] + z[143] + static_cast<T>(127)+ 
    z[140];
    z[29]=z[29]*z[52];
    z[44]=z[121]*z[11];
    z[62]=npow(z[9],5);
    z[140]=z[62]*z[5];
    z[44]=z[140] + z[44] + 2*z[125];
    z[44]=z[44]*z[14];
    z[140]=z[104]*z[125];
    z[140]=z[140] + 5*z[122] + 9*z[121];
    z[44]= - z[44] + 3*z[140];
    z[44]=z[44]*z[14];
    z[140]=npow(z[5],2);
    z[140]=9*z[140];
    z[62]=z[140]*z[62];
    z[62]=z[62] + 4*z[121];
    z[62]=z[62]*z[5];
    z[143]= - z[62] - 217*z[30] - 104*z[51];
    z[143]=3*z[143] + z[44];
    z[143]=z[14]*z[143];
    z[144]=7*z[30];
    z[147]= - z[144] + 20*z[32];
    z[147]=z[147]*z[11];
    z[150]=z[96]*z[22];
    z[151]=z[10]*npow(z[11],5);
    z[150]=z[150] + z[151];
    z[152]=9*z[3];
    z[150]=z[150]*z[152];
    z[153]=z[114]*z[10];
    z[147]=20*z[153] + z[147] - z[150];
    z[147]=z[147]*z[135];
    z[150]=z[151] + 5*z[114];
    z[151]=9*z[10];
    z[150]=z[150]*z[151];
    z[150]=z[150] - 10*z[110];
    z[150]=z[150]*z[10];
    z[147]= - z[147] + z[150] + 54*z[30];
    z[150]=46*z[11];
    z[154]=255*z[9] + z[150];
    z[154]=z[11]*z[154];
    z[154]=z[154] - z[147];
    z[154]=z[3]*z[154];
    z[155]=z[122] + z[121];
    z[156]=z[14]*z[155];
    z[157]= - z[3]*z[122];
    z[156]=z[156] + z[85] + z[157];
    z[157]=192*z[17];
    z[156]=z[156]*z[157];
    z[158]=z[114]*z[146];
    z[158]=z[158] + 23*z[110];
    z[158]=z[158]*z[24];
    z[159]= - 224*z[22] + z[158];
    z[159]=z[10]*z[159];
    z[160]=z[110]*z[24];
    z[161]=8*z[22];
    z[162]=z[161] + z[160];
    z[163]=z[162]*z[24];
    z[164]=133*z[11] - z[163];
    z[164]=z[10]*z[164];
    z[164]=static_cast<T>(148)+ z[164];
    z[164]=z[7]*z[164];
    z[165]=z[121]*z[7];
    z[165]=z[165] - z[125];
    z[166]=9*z[5];
    z[165]=z[165]*z[166];
    z[165]=z[165] - 5*z[137];
    z[165]=z[165]*z[57];
    z[165]=z[165] - 113*z[30];
    z[167]=62*z[33] + z[165];
    z[167]=z[5]*z[167];
    z[143]=z[156] + z[143] + z[167] + z[154] + z[164] + z[159] - 544*
    z[9] - 275*z[11];
    z[143]=z[17]*z[143];
    z[154]=z[121]*z[22]*z[89];
    z[156]=z[74]*z[10];
    z[159]=z[110]*z[152]*z[156];
    z[164]=10*z[11];
    z[167]=z[45] - z[164];
    z[58]=z[167]*z[58];
    z[58]=z[159] + z[58] + 11*z[51] - 6*z[154];
    z[58]=z[58]*z[135];
    z[154]=3*z[121];
    z[159]=z[154] - 23*z[122];
    z[159]=z[159]*z[81];
    z[159]=z[159] - 69*z[30] + 82*z[51];
    z[159]=z[73]*z[159];
    z[167]=38*z[9];
    z[159]=z[159] - z[167] - 147*z[11];
    z[159]=z[11]*z[159];
    z[159]=47*z[30] + z[159];
    z[159]=z[8]*z[159];
    z[168]=z[21] - z[38];
    z[169]=z[168]*z[138];
    z[170]=z[74]*z[160];
    z[169]=z[169] + z[170];
    z[169]=z[169]*z[24];
    z[170]=47*z[9] - z[49];
    z[170]=z[11]*z[170];
    z[169]=z[170] + z[169];
    z[169]=z[10]*z[169];
    z[170]=40*z[9];
    z[171]=19*z[11];
    z[172]=z[170] - z[171];
    z[58]=z[58] + z[169] + 6*z[172] + z[159];
    z[58]=z[3]*z[58];
    z[159]=9*z[76];
    z[169]= - z[131] + z[159];
    z[169]=z[169]*z[72];
    z[172]=9*z[91];
    z[173]= - static_cast<T>(14)+ z[172];
    z[173]=z[173]*z[73];
    z[98]=z[173] + z[98];
    z[98]=z[98]*z[37];
    z[98]=z[98] - static_cast<T>(5)+ z[169];
    z[98]=z[7]*z[98];
    z[137]=z[137] - z[121];
    z[169]=27*z[5];
    z[173]=z[169]*z[137];
    z[174]= - z[173] - z[69] + 10*z[33];
    z[174]=z[174]*z[57];
    z[98]=z[174] + z[98] + 152*z[9] - z[159];
    z[98]=z[5]*z[98];
    z[159]=z[32]*z[24];
    z[174]=z[110]*z[14];
    z[175]=6*z[174];
    z[176]=11*z[22] - z[175];
    z[176]=z[14]*z[176];
    z[177]= - z[48]*z[23];
    z[177]= - z[22] + z[177];
    z[177]=z[177]*z[52];
    z[164]=z[177] + z[176] - z[159] - 17*z[9] + z[164];
    z[164]=z[164]*z[52];
    z[176]=23*z[30] - 22*z[51];
    z[176]=z[176]*z[70];
    z[176]= - z[154] + z[176];
    z[176]=z[176]*z[81];
    z[177]= - 541*z[9] + 164*z[11];
    z[177]=z[11]*z[177];
    z[176]=z[176] + 111*z[30] + z[177];
    z[176]=z[8]*z[176];
    z[177]=13*z[9];
    z[178]=z[177] + z[149];
    z[176]=12*z[178] + z[176];
    z[176]=z[8]*z[176];
    z[178]=45*z[11];
    z[179]=176*z[9] - z[178];
    z[179]=z[11]*z[179];
    z[179]= - 46*z[30] + z[179];
    z[179]=z[179]*z[81];
    z[179]=z[179] + 323*z[9] - 433*z[11];
    z[179]=z[8]*z[179];
    z[179]=static_cast<T>(189)+ z[179];
    z[179]=z[8]*z[179];
    z[178]= - 44*z[9] + z[178];
    z[178]=z[178]*z[81];
    z[178]=static_cast<T>(121)+ z[178];
    z[178]=z[178]*z[77];
    z[180]=z[89]*z[7];
    z[178]=z[178] - 135*z[180];
    z[178]=z[178]*z[43];
    z[178]=z[179] + z[178];
    z[178]=z[7]*z[178];
    z[179]=z[125]*z[57];
    z[122]=z[179] - 32*z[121] - 35*z[122];
    z[122]=z[14]*z[122];
    z[179]= - z[177] + z[40];
    z[179]=z[11]*z[179];
    z[181]=14*z[30];
    z[179]= - z[181] + z[179];
    z[119]= - z[5]*z[119];
    z[119]=3*z[179] + z[119];
    z[119]=3*z[119] + z[122];
    z[119]=z[14]*z[119];
    z[122]=z[125]*z[166];
    z[122]= - z[121] + z[122];
    z[122]=z[122]*z[166];
    z[122]=53*z[30] + z[122];
    z[122]=z[5]*z[122];
    z[179]=225*z[9] + z[171];
    z[119]=z[119] + 3*z[179] + z[122];
    z[119]=z[14]*z[119];
    z[122]= - z[162]*z[146];
    z[122]=281*z[11] + z[122];
    z[122]=z[10]*z[122];
    z[58]=z[143] + z[164] + z[119] + z[98] + z[58] + z[178] + z[122] + 
   static_cast<T>(504)+ z[176];
    z[58]=z[13]*z[58];
    z[98]= - z[7]*z[85];
    z[98]= - z[121] + z[98];
    z[98]=z[98]*z[43];
    z[119]=z[27]*z[5];
    z[122]=z[121]*z[119];
    z[98]=z[98] + z[122];
    z[98]=z[5]*z[98];
    z[122]=z[65] + 12*z[9];
    z[143]=z[122]*z[11];
    z[143]=z[143] + z[71];
    z[162]= - z[11]*z[143];
    z[122]= - z[7]*z[122];
    z[122]=2*z[143] + z[122];
    z[122]=z[7]*z[122];
    z[98]=z[98] + z[122] + z[121] + z[162];
    z[98]=z[98]*z[107];
    z[122]= - 28*z[30] + 33*z[33];
    z[122]=z[7]*z[122];
    z[119]=z[85]*z[119];
    z[119]=z[122] + z[119];
    z[119]=z[119]*z[97];
    z[122]=149*z[9] + 104*z[11];
    z[122]=z[11]*z[122];
    z[143]=167*z[11];
    z[162]=62*z[7] - 217*z[9] - z[143];
    z[162]=z[7]*z[162];
    z[28]=z[98] + z[119] + z[28] + z[162] + 51*z[30] + z[122];
    z[28]=z[16]*z[28];
    z[46]=z[46]*z[97];
    z[97]=16*z[9];
    z[98]= - z[97] + 7*z[7];
    z[98]=z[7]*z[98];
    z[46]=z[46] + z[30] + z[98];
    z[46]=z[5]*z[46];
    z[98]= - 23*z[9] - z[56];
    z[98]=z[11]*z[98];
    z[98]= - 15*z[30] + z[98];
    z[98]=z[3]*z[98];
    z[28]=z[28] + z[46] + z[98] + z[50] + 32*z[9] - z[109];
    z[28]=z[28]*z[52];
    z[42]=z[42] - z[127];
    z[42]=z[7]*z[42];
    z[46]=z[137]*z[7];
    z[46]=z[46] + z[125];
    z[50]=z[5]*z[46];
    z[42]=z[50] + z[42] + z[123];
    z[42]=z[14]*z[42];
    z[50]=9*z[7] + z[131] + z[149];
    z[50]=z[7]*z[50];
    z[98]=z[181] - z[34];
    z[98]=z[7]*z[98];
    z[98]= - 13*z[121] + z[98];
    z[98]=z[5]*z[98];
    z[42]=z[42] + z[98] + z[50] - 29*z[30] - 16*z[51];
    z[42]=z[14]*z[42];
    z[27]=z[27]*z[59];
    z[46]=z[46]*z[166];
    z[27]=z[27] + z[46];
    z[27]=z[27]*z[57];
    z[46]= - z[38] - 25*z[7];
    z[46]=z[7]*z[46];
    z[27]=z[27] + 25*z[30] + z[46];
    z[27]=z[5]*z[27];
    z[46]= - z[48]*z[36];
    z[50]= - z[9] - z[70];
    z[50]=z[11]*z[50];
    z[46]=z[50] + z[46];
    z[46]=z[8]*z[46];
    z[50]=73*z[11];
    z[27]=z[42] + z[27] - 12*z[7] + 21*z[46] + 296*z[9] + z[50];
    z[27]=z[14]*z[27];
    z[42]=z[146]*z[110];
    z[46]=z[138] - z[139];
    z[46]=5*z[46] + z[42];
    z[46]=z[46]*z[146];
    z[46]=z[46] - 908*z[11] + 17*z[36];
    z[46]=z[10]*z[46];
    z[98]=z[74]*z[22];
    z[98]= - z[98] - z[153];
    z[119]=27*z[3];
    z[98]=z[98]*z[119];
    z[122]=z[110]*z[10];
    z[123]=51*z[11];
    z[125]=35*z[9] + z[123];
    z[125]=z[11]*z[125];
    z[98]=z[98] + z[125] + 51*z[122];
    z[98]=z[98]*z[135];
    z[114]= - z[114]*z[24];
    z[114]= - 14*z[110] + z[114];
    z[114]=z[114]*z[151];
    z[114]=53*z[22] + z[114];
    z[114]=z[10]*z[114];
    z[125]=59*z[11];
    z[98]=z[98] + z[114] + 80*z[9] - z[125];
    z[98]=z[3]*z[98];
    z[114]= - 21*z[36] + 41*z[9] + z[56];
    z[114]=z[8]*z[114];
    z[127]=z[24]*z[22];
    z[131]= - z[49] - z[127];
    z[131]=z[131]*z[24];
    z[131]=static_cast<T>(98)+ z[131];
    z[55]=z[131]*z[55];
    z[131]=z[33]*z[5];
    z[120]=29*z[9] - z[120];
    z[120]=2*z[120] + 19*z[131];
    z[120]=z[120]*z[57];
    z[27]=z[28] + z[27] + z[120] + z[98] + z[55] + z[46] - static_cast<T>(624)+ z[114];
    z[27]=z[12]*z[27];
    z[28]= - 3*z[22] - z[139];
    z[28]=z[28]*z[73];
    z[46]=z[141]*z[86];
    z[28]=z[46] + z[28] + z[9] - z[70];
    z[28]=z[28]*z[43];
    z[43]=5*z[110] + z[115];
    z[43]=z[43]*z[73];
    z[46]= - 7*z[9] + z[56];
    z[46]=z[11]*z[46];
    z[28]=z[28] + z[43] + z[71] + z[46];
    z[28]=z[14]*z[28];
    z[43]= - z[31] + z[23] + z[74];
    z[43]=z[7]*z[43];
    z[43]= - z[122] + z[30] - z[32] + z[43];
    z[43]=z[12]*z[43];
    z[46]= - z[45] + z[70];
    z[46]=z[11]*z[46];
    z[55]=z[121] + z[116];
    z[55]=z[8]*z[55];
    z[46]=z[55] + z[85] + z[46];
    z[46]=z[46]*z[73];
    z[55]=z[22]*z[9];
    z[86]=z[126] + z[121] - z[55];
    z[86]=z[86]*z[73];
    z[84]= - z[11]*z[84];
    z[84]=z[86] + z[30] + z[84];
    z[84]=z[10]*z[84];
    z[86]= - z[117] - z[66];
    z[98]=4*z[8];
    z[86]=z[86]*z[98];
    z[114]=z[74]*z[78];
    z[86]=2*z[114] + z[86] - z[61];
    z[86]=z[7]*z[86];
    z[55]= - z[55] + z[79];
    z[55]=z[55]*z[73];
    z[55]= - z[51] + z[55];
    z[55]=z[3]*z[55];
    z[114]=z[73]*z[7];
    z[93]=z[93]*z[114];
    z[93]= - z[9] + z[93];
    z[47]=z[93]*z[47];
    z[28]=z[43] + z[28] + z[47] + z[55] + z[86] + z[46] + z[84];
    z[28]=z[28]*z[157];
    z[43]=40*z[51];
    z[46]= - z[62] - 89*z[30] - z[43];
    z[44]=3*z[46] + z[44];
    z[44]=z[14]*z[44];
    z[46]=63*z[9] + z[150];
    z[46]=z[11]*z[46];
    z[46]=z[46] - z[147];
    z[46]=z[3]*z[46];
    z[47]= - 608*z[22] + z[158];
    z[47]=z[10]*z[47];
    z[55]=325*z[11] - z[163];
    z[55]=z[10]*z[55];
    z[55]=static_cast<T>(340)+ z[55];
    z[55]=z[7]*z[55];
    z[62]=254*z[33] + z[165];
    z[62]=z[5]*z[62];
    z[44]=z[44] + z[62] + z[46] + z[55] + z[47] - 160*z[9] - 659*z[11];
    z[44]=z[12]*z[44];
    z[46]=z[8]*z[68];
    z[46]=z[46] - z[41];
    z[46]=z[8]*z[46];
    z[47]= - z[8]*z[124];
    z[55]=z[85] - z[32];
    z[47]=z[47] - z[55];
    z[47]=z[8]*z[47];
    z[47]=z[47] + z[66];
    z[47]=z[10]*z[47];
    z[62]=z[8]*z[88];
    z[62]=z[62] + z[78];
    z[62]=z[7]*z[62];
    z[46]=z[62] + z[46] + z[47];
    z[47]=7*z[22];
    z[62]= - z[47] - z[112];
    z[62]=z[62]*z[73];
    z[66]=z[136]*z[114];
    z[21]=z[66] + z[62] - z[38] - z[21];
    z[21]=z[14]*z[21];
    z[62]= - z[8]*z[133];
    z[62]=z[32] + z[62];
    z[62]=z[62]*z[73];
    z[62]=z[11] + z[62];
    z[62]=z[3]*z[62];
    z[66]= - z[114] + 1;
    z[66]=z[7]*z[75]*z[66];
    z[66]= - z[9] + z[66];
    z[66]=z[5]*z[66];
    z[21]=z[21] + z[66] + 2*z[46] + z[62];
    z[21]=z[28] + 192*z[21] + z[44];
    z[21]=z[17]*z[21];
    z[28]=z[137]*z[57];
    z[44]=z[7]*z[90];
    z[28]=z[28] + z[44] - z[71] - z[51];
    z[28]=z[14]*z[28];
    z[44]=z[129]*z[10];
    z[46]=z[44] + z[80];
    z[62]= - z[46]*z[152];
    z[66]= - z[45] - z[11];
    z[66]=z[11]*z[66];
    z[66]= - z[85] + z[66];
    z[66]=z[66]*z[151];
    z[68]= - z[48]*z[151];
    z[68]=static_cast<T>(88)+ z[68];
    z[68]=z[7]*z[68];
    z[34]=z[30] + z[34];
    z[34]=z[34]*z[166];
    z[28]=z[28] + z[34] + z[62] + z[68] + z[66] - 8*z[9] + 25*z[11];
    z[28]=z[14]*z[28];
    z[34]= - z[128] - z[79];
    z[34]=z[8]*z[34];
    z[34]=z[34] + z[44];
    z[34]=z[34]*z[135];
    z[44]=3*z[168];
    z[59]= - z[36]*z[59];
    z[62]=11*z[11];
    z[66]= - z[9] - z[62];
    z[66]=z[11]*z[66];
    z[59]=z[66] + z[59];
    z[59]=z[8]*z[59];
    z[66]=z[9] + z[49];
    z[66]=z[11]*z[66];
    z[66]= - z[71] + z[66];
    z[66]=z[10]*z[66];
    z[34]=z[34] + 4*z[66] + z[44] + z[59];
    z[34]=z[34]*z[135];
    z[59]= - z[30]*z[146];
    z[66]= - z[24]*z[33];
    z[59]=z[66] + 19*z[9] + z[59];
    z[59]=z[7]*z[59];
    z[66]= - z[121]*z[24];
    z[59]=z[173] + z[66] + z[59];
    z[59]=z[59]*z[57];
    z[66]= - z[67] + z[118];
    z[66]=z[66]*z[146];
    z[67]= - static_cast<T>(8)+ z[53];
    z[25]=z[67]*z[25];
    z[25]=z[25] - static_cast<T>(25)+ z[66];
    z[25]=z[7]*z[25];
    z[66]=npow(z[10],2);
    z[67]=z[66]*z[154];
    z[25]=z[59] + z[25] - z[170] + z[67];
    z[25]=z[5]*z[25];
    z[59]=22*z[9];
    z[67]= - z[59] - z[171];
    z[68]=z[49]*z[8];
    z[71]= - z[48]*z[68];
    z[75]= - z[10]*z[51];
    z[67]=z[75] + 2*z[67] + z[71];
    z[67]=z[67]*z[24];
    z[71]=z[143] + 63*z[36];
    z[71]=z[71]*z[72];
    z[75]= - static_cast<T>(25)- z[156];
    z[75]=z[10]*z[75];
    z[79]=z[7]*z[66];
    z[75]=z[75] + z[79];
    z[75]=z[75]*z[37];
    z[25]=z[28] + z[25] + z[34] + z[75] + z[67] - static_cast<T>(617)+ z[71];
    z[25]=z[14]*z[25];
    z[28]= - z[134] - 5*z[139];
    z[28]=z[28]*z[77];
    z[34]= - z[23]*z[111];
    z[28]=z[28] + z[34];
    z[28]=z[28]*z[146];
    z[34]= - z[50] - 138*z[36];
    z[34]=z[34]*z[73];
    z[34]= - static_cast<T>(9)+ z[34];
    z[34]=z[8]*z[34];
    z[28]=z[34] + z[28];
    z[28]=z[10]*z[28];
    z[34]= - z[70] - z[23];
    z[34]=z[34]*z[151];
    z[34]=static_cast<T>(74)+ z[34];
    z[34]=z[10]*z[34];
    z[50]= - z[23]*z[152];
    z[50]=11*z[61] + z[50];
    z[50]=z[50]*z[135];
    z[34]=z[34] + z[50];
    z[34]=z[3]*z[34];
    z[50]=18*z[11];
    z[50]= - z[89]*z[50];
    z[28]=z[34] + z[50] + z[28];
    z[28]=z[12]*z[28];
    z[34]=16*z[74] + 5*z[117];
    z[34]=z[8]*z[34];
    z[34]= - static_cast<T>(8)+ z[34];
    z[34]=z[8]*z[34];
    z[50]=z[111]*z[156];
    z[34]=z[34] + z[50];
    z[34]=z[34]*z[24];
    z[50]= - 36*z[9] + 41*z[11];
    z[50]=z[50]*z[72];
    z[50]=static_cast<T>(25)+ z[50];
    z[50]=z[50]*z[77];
    z[34]=z[50] + z[34];
    z[34]=z[10]*z[34];
    z[34]= - 114*z[89] + z[34];
    z[50]=static_cast<T>(3)+ z[156];
    z[50]=z[50]*z[66];
    z[67]=z[156] - 1;
    z[70]=z[67]*z[152];
    z[70]= - 11*z[10] + z[70];
    z[70]=z[3]*z[70];
    z[50]=3*z[50] + z[70];
    z[50]=z[50]*z[135];
    z[70]=z[53] + 1;
    z[71]= - z[70]*z[57];
    z[75]=static_cast<T>(16)+ z[53];
    z[75]=z[10]*z[75];
    z[71]=z[71] + z[72] + z[75];
    z[71]=z[3]*z[71];
    z[75]=static_cast<T>(3)+ 4*z[91];
    z[75]=z[8]*z[75];
    z[79]=z[10]*z[94];
    z[75]=z[75] + z[79];
    z[75]=z[75]*z[148];
    z[71]=12*z[77] + z[75] + z[71];
    z[71]=z[71]*z[57];
    z[28]=z[28] + z[71] + 2*z[34] + z[50];
    z[28]=z[1]*z[28];
    z[34]=z[30]*z[73];
    z[34]= - z[35] + z[34];
    z[50]=5*z[8];
    z[34]=z[34]*z[50];
    z[34]=z[53] - static_cast<T>(11)+ z[34];
    z[34]=z[10]*z[34];
    z[71]=z[45]*z[8];
    z[53]= - 32*z[53] - static_cast<T>(5)- z[71];
    z[53]=z[3]*z[53];
    z[75]=z[9]*z[103];
    z[75]= - static_cast<T>(49)+ z[75];
    z[75]=z[8]*z[75];
    z[34]=z[104] + z[53] + 19*z[78] + z[75] + z[34];
    z[34]=z[34]*z[57];
    z[53]=z[11] + z[159];
    z[53]=z[53]*z[152];
    z[71]=static_cast<T>(19)- z[71];
    z[75]=z[177] - 42*z[11];
    z[75]=z[10]*z[75];
    z[53]=z[53] + 3*z[71] + z[75];
    z[53]=z[53]*z[135];
    z[71]=static_cast<T>(53)- z[172];
    z[71]=z[71]*z[98];
    z[75]=z[49] - z[9];
    z[75]=2*z[75];
    z[79]=z[75] + z[159];
    z[79]=z[79]*z[151];
    z[79]= - static_cast<T>(112)+ z[79];
    z[79]=z[10]*z[79];
    z[53]=z[53] + z[71] + z[79];
    z[53]=z[3]*z[53];
    z[71]=z[110]*z[77];
    z[79]=6*z[23];
    z[71]=z[79] + z[149] + 20*z[71];
    z[71]=z[71]*z[24];
    z[36]=z[149] + 8*z[36];
    z[36]=z[8]*z[36];
    z[36]= - static_cast<T>(83)+ 18*z[36];
    z[36]=2*z[36] + z[71];
    z[36]=z[10]*z[36];
    z[65]= - z[65] + z[79];
    z[71]=z[122] + z[22];
    z[79]= - z[71]*z[119];
    z[65]=7*z[65] + z[79];
    z[65]=z[3]*z[65];
    z[79]= - 13*z[22] - z[160];
    z[79]=z[10]*z[79];
    z[79]=z[83] + z[79];
    z[79]=z[10]*z[79];
    z[79]=static_cast<T>(5)+ z[79];
    z[65]=3*z[79] + z[65];
    z[65]=z[65]*z[135];
    z[79]=z[132] - z[38];
    z[80]=z[79]*z[82];
    z[80]=static_cast<T>(134)+ z[80];
    z[80]=z[8]*z[80];
    z[36]= - 87*z[5] + z[65] + z[80] + z[36];
    z[36]=z[12]*z[36];
    z[65]=285*z[76] - 125*z[9] - 114*z[11];
    z[65]=z[65]*z[73];
    z[65]= - static_cast<T>(313)+ z[65];
    z[65]=z[8]*z[65];
    z[32]= - z[30] + 4*z[32];
    z[50]= - z[32]*z[50];
    z[50]= - z[167] + z[50];
    z[50]=z[8]*z[50];
    z[80]= - z[11]*z[146];
    z[50]=z[80] + static_cast<T>(1)+ z[50];
    z[50]=z[50]*z[24];
    z[50]=z[65] + z[50];
    z[50]=z[10]*z[50];
    z[65]=static_cast<T>(49)+ 172*z[91];
    z[65]=z[65]*z[77];
    z[28]=z[28] + z[36] + z[34] + z[53] + 204*z[180] + 3*z[65] + z[50];
    z[28]=z[1]*z[28];
    z[34]=z[152]*z[9];
    z[36]=static_cast<T>(8)- z[34];
    z[36]=z[36]*z[99];
    z[50]=z[99]*z[35];
    z[50]=6*z[3] + z[50];
    z[50]=z[14]*z[50];
    z[36]=z[36] + z[50];
    z[36]=z[14]*z[36];
    z[50]=npow(z[14],2);
    z[53]= - z[11]*z[50]*z[135];
    z[50]=z[22]*z[50];
    z[50]=static_cast<T>(1)+ z[50];
    z[50]=z[16]*z[3]*z[50];
    z[50]=z[50] - z[99] + z[53];
    z[50]=z[50]*z[52];
    z[53]= - z[92] + z[140];
    z[53]=z[5]*z[53];
    z[36]=z[50] + z[53] + z[36];
    z[50]=z[57]*z[30];
    z[53]= - z[9] + z[50];
    z[53]=z[53]*z[166];
    z[53]=static_cast<T>(7)+ z[53];
    z[53]=z[5]*z[53];
    z[65]=z[5]*z[9];
    z[80]=static_cast<T>(1)+ z[65];
    z[82]=z[14]*z[31];
    z[80]=18*z[80] + z[82];
    z[80]=z[14]*z[80];
    z[53]=z[53] + z[80];
    z[53]=z[14]*z[53];
    z[80]=z[7] - z[9];
    z[82]= - z[80]*z[57];
    z[82]= - static_cast<T>(1)+ z[82];
    z[82]=z[82]*z[140];
    z[84]= - z[161] - z[174];
    z[84]=z[14]*z[84];
    z[84]=z[11] + z[84];
    z[84]=z[84]*z[52];
    z[86]=z[22]*z[14];
    z[88]=20*z[11] + 9*z[86];
    z[88]=z[14]*z[88];
    z[84]=z[84] - static_cast<T>(3)+ z[88];
    z[84]=z[16]*z[84];
    z[88]=z[14]*z[11];
    z[89]=static_cast<T>(17)- 18*z[88];
    z[89]=z[14]*z[89];
    z[84]=z[89] + 2*z[84];
    z[84]=z[84]*z[52];
    z[53]=z[84] + z[82] + z[53];
    z[53]=z[13]*z[53];
    z[36]=3*z[36] + z[53];
    z[36]=z[2]*z[36];
    z[39]= - z[39]*z[135];
    z[53]=z[22]*z[135];
    z[53]= - 88*z[11] + z[53];
    z[53]=2*z[53] + 15*z[86];
    z[53]=z[14]*z[53];
    z[82]=z[9] + z[109];
    z[82]=3*z[82] + 19*z[86];
    z[82]=z[82]*z[107];
    z[39]=z[82] + z[53] + 12*z[65] + static_cast<T>(65)+ z[39];
    z[39]=z[16]*z[39];
    z[53]=z[166] + z[135];
    z[53]=z[9]*z[53];
    z[53]=static_cast<T>(40)+ z[53];
    z[53]=z[5]*z[53];
    z[82]= - 36*z[88] + static_cast<T>(119)- 15*z[60];
    z[82]=z[14]*z[82];
    z[39]=z[39] + z[82] + 12*z[3] + z[53];
    z[39]=z[39]*z[52];
    z[53]=9*z[9] + z[56];
    z[53]=3*z[53] - 8*z[31];
    z[56]= - z[121]*z[57];
    z[56]=34*z[30] + z[56];
    z[56]=z[14]*z[56];
    z[53]=3*z[53] + z[56];
    z[53]=z[14]*z[53];
    z[56]= - z[121]*z[166];
    z[56]=z[85] + z[56];
    z[56]=z[56]*z[57];
    z[56]= - z[97] + z[56];
    z[56]=z[5]*z[56];
    z[56]= - static_cast<T>(93)+ z[56];
    z[53]=3*z[56] + z[53];
    z[53]=z[14]*z[53];
    z[56]=31*z[22] + z[175];
    z[82]=2*z[14];
    z[56]=z[56]*z[82];
    z[44]= - z[44] + z[56];
    z[44]=z[16]*z[44];
    z[56]=z[83] - 18*z[86];
    z[56]=z[56]*z[82];
    z[44]=z[44] + static_cast<T>(61)+ z[56];
    z[44]=z[44]*z[52];
    z[33]= - z[30] + z[33];
    z[33]=z[33]*z[169];
    z[33]=z[33] + z[87] - 5*z[7];
    z[33]=z[5]*z[33];
    z[33]= - static_cast<T>(10)+ z[33];
    z[33]=z[5]*z[33];
    z[33]= - 43*z[3] + z[33];
    z[33]=z[44] + 3*z[33] + z[53];
    z[33]=z[13]*z[33];
    z[44]= - z[7]*z[166];
    z[44]=static_cast<T>(1)+ z[44];
    z[44]=z[5]*z[44];
    z[44]= - z[135] + z[44];
    z[44]=z[5]*z[44];
    z[52]= - z[3] + z[57];
    z[52]=z[52]*z[57];
    z[52]= - z[92] + z[52];
    z[52]=z[5]*z[52];
    z[52]= - 9*z[100] + z[52];
    z[52]=z[1]*z[52];
    z[44]=z[52] + 36*z[92] + z[44];
    z[50]=z[38] + z[50];
    z[50]=z[14]*z[50];
    z[34]=z[50] + 63*z[65] + static_cast<T>(115)- z[34];
    z[34]=z[14]*z[34];
    z[31]=z[35] + 27*z[31];
    z[31]=z[31]*z[57];
    z[50]=z[3]*z[9];
    z[31]=z[31] + static_cast<T>(55)- 48*z[50];
    z[31]=z[5]*z[31];
    z[31]=z[34] - 66*z[3] + z[31];
    z[31]=z[14]*z[31];
    z[34]= - z[50] + z[65];
    z[34]=z[17]*z[34];
    z[34]=z[34] + z[3] - z[5];
    z[34]=z[34]*z[157];
    z[31]=z[36] + z[33] + z[34] + z[39] + z[31] + 3*z[44];
    z[31]=z[2]*z[31];
    z[33]= - z[30]*z[24];
    z[33]= - z[145] + z[33];
    z[33]=z[10]*z[33];
    z[33]= - static_cast<T>(8)+ z[33];
    z[33]=z[5]*z[33];
    z[34]= - z[11] + z[23];
    z[34]=z[10]*z[34];
    z[34]=z[34] + z[60];
    z[36]=8*z[12];
    z[34]=z[34]*z[36];
    z[39]=static_cast<T>(2)- z[156];
    z[39]=z[10]*z[39];
    z[44]= - static_cast<T>(13)- 8*z[60];
    z[44]=z[3]*z[44];
    z[33]=z[34] + z[33] + 8*z[39] + z[44];
    z[33]=z[1]*z[33];
    z[32]=z[32]*z[24];
    z[34]=z[145] + z[11];
    z[32]=2*z[34] + z[32];
    z[32]=z[10]*z[32];
    z[34]=z[118] + z[9];
    z[39]=6*z[5];
    z[44]= - z[34]*z[39];
    z[42]= - z[47] - z[42];
    z[47]=2*z[12];
    z[52]=z[47]*z[10];
    z[42]=z[42]*z[52];
    z[53]=4*z[60];
    z[32]=z[33] + z[42] + z[44] - z[53] + static_cast<T>(31)+ z[32];
    z[32]=z[1]*z[32];
    z[33]= - z[155]*z[82];
    z[42]=2*z[51];
    z[33]=z[33] - z[30] + z[42];
    z[33]=z[14]*z[33];
    z[44]= - z[45] - z[40];
    z[44]=z[11]*z[44];
    z[44]= - z[30] + z[44];
    z[44]=z[10]*z[44];
    z[47]=z[71]*z[47];
    z[33]=z[47] + z[33] - z[75] + z[44];
    z[44]=z[14]*z[85];
    z[44]=z[45] + z[44];
    z[44]=z[14]*z[44];
    z[44]=static_cast<T>(10)+ z[44];
    z[47]=16*z[92] + z[101];
    z[47]=z[1]*z[47];
    z[39]=z[47] - 7*z[3] - z[39];
    z[39]=z[1]*z[39];
    z[47]= - z[5]*z[92]*z[106];
    z[47]=11*z[99] + z[47];
    z[47]=z[1]*z[47];
    z[47]= - z[57] + z[47];
    z[47]=z[2]*z[47];
    z[39]=z[47] + 3*z[44] + z[39];
    z[39]=z[2]*z[39];
    z[44]= - z[10]*z[96];
    z[44]=z[44] - z[38] + z[11];
    z[44]=z[10]*z[44];
    z[47]=z[12]*z[110]*z[66];
    z[56]=z[60] + 2;
    z[34]=z[5]*z[34];
    z[34]=z[47] + z[34] + z[44] - z[56];
    z[34]=z[1]*z[34];
    z[44]= - z[10]*z[55];
    z[47]= - z[12]*z[71];
    z[34]=z[34] + z[47] - 2*z[48] + z[44];
    z[34]=z[1]*z[34];
    z[44]=z[108] + z[5];
    z[44]=z[1]*z[44];
    z[47]= - z[1]*z[99];
    z[47]=z[5] + z[47];
    z[47]=z[2]*z[47];
    z[44]=z[47] + static_cast<T>(1)+ z[44];
    z[44]=z[1]*z[44];
    z[44]= - z[45] + z[44];
    z[44]=z[2]*z[44];
    z[34]=z[44] + z[142] + z[34];
    z[44]=6*z[6];
    z[34]=z[34]*z[44];
    z[32]=z[34] + z[39] + 3*z[33] + z[32];
    z[32]=z[6]*z[32];
    z[33]= - z[67]*z[66];
    z[34]= - z[56]*z[92];
    z[33]=z[33] + z[34];
    z[34]= - z[9]*z[24];
    z[34]= - static_cast<T>(11)+ z[34];
    z[34]=z[10]*z[34];
    z[34]=z[34] + z[63];
    z[34]=z[5]*z[34];
    z[39]=z[22]*z[66];
    z[39]= - static_cast<T>(1)+ z[39];
    z[39]=z[10]*z[39];
    z[40]=z[3]*z[40];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[3]*z[40];
    z[39]=z[39] + z[40];
    z[36]=z[39]*z[36];
    z[33]=z[36] + 8*z[33] + z[34];
    z[33]=z[1]*z[33];
    z[34]=z[9]*z[146];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[34]*z[24];
    z[36]=static_cast<T>(21)+ z[53];
    z[36]=z[36]*z[108];
    z[39]= - z[61] + z[60];
    z[39]=z[12]*z[39];
    z[40]=z[5]*z[70];
    z[33]=z[33] + 24*z[39] + 21*z[40] + z[34] + z[36];
    z[33]=z[1]*z[33];
    z[34]=16*z[102] - 40*z[92] - 21*z[99];
    z[34]=z[1]*z[34];
    z[36]= - z[14]*z[69];
    z[36]= - 10*z[9] + z[36];
    z[36]=z[14]*z[36];
    z[36]= - static_cast<T>(23)+ z[36];
    z[36]=z[36]*z[82];
    z[39]=z[1]*z[105];
    z[40]= - z[3]*z[57];
    z[39]=z[40] + z[39];
    z[39]=z[2]*z[39];
    z[34]=8*z[39] + z[34] - 13*z[3] + z[36];
    z[34]=z[2]*z[34];
    z[36]=z[155]*z[64];
    z[36]=z[36] + 10*z[30] + 7*z[51];
    z[36]=z[36]*z[82];
    z[39]=z[113] - 27;
    z[39]=z[11]*z[39];
    z[36]=z[36] + z[59] + z[39];
    z[36]=z[14]*z[36];
    z[39]= - z[11]*z[41]*z[24];
    z[39]=z[39] + 48*z[9] - z[49];
    z[39]=z[10]*z[39];
    z[22]= - 5*z[22] + z[160];
    z[22]=z[22]*z[52];
    z[22]=z[32] + z[34] + z[33] + z[22] + z[36] + 10*z[60] + static_cast<T>(15)+ z[39];
    z[22]=z[22]*z[44];
    z[32]= - z[76]*z[49];
    z[32]= - z[42] + z[32];
    z[32]=z[32]*z[72];
    z[33]=26*z[9] - z[123];
    z[33]=z[11]*z[33];
    z[33]=z[144] + z[33];
    z[33]=z[10]*z[33];
    z[34]=z[127]*z[74];
    z[36]= - z[42] + z[34];
    z[36]=z[36]*z[152];
    z[32]=z[36] + z[33] + z[32] - z[59] - z[125];
    z[32]=z[32]*z[135];
    z[33]=z[11]*z[76];
    z[33]= - 27*z[33] + 9*z[30] + 172*z[51];
    z[33]=z[33]*z[73];
    z[33]=z[33] - 108*z[9] - 175*z[11];
    z[33]=z[8]*z[33];
    z[35]= - z[35] + z[62];
    z[35]=z[11]*z[35];
    z[34]=z[35] + z[34];
    z[34]=z[34]*z[151];
    z[34]=z[34] - 88*z[9] - 93*z[11];
    z[34]=z[10]*z[34];
    z[32]=z[32] + z[34] - static_cast<T>(636)+ z[33];
    z[32]=z[3]*z[32];
    z[33]= - static_cast<T>(31)- 28*z[91];
    z[33]=z[8]*z[33];
    z[34]=z[10]*z[54];
    z[33]=42*z[78] + z[33] + z[34];
    z[33]=z[7]*z[33];
    z[34]= - z[59] - z[95];
    z[34]=z[8]*z[34];
    z[35]=z[10]*z[130];
    z[36]=z[38] - z[7];
    z[36]=5*z[36] + 18*z[131];
    z[36]=z[5]*z[36];
    z[33]=z[36] - 16*z[50] + z[33] + z[34] + z[35];
    z[33]=z[33]*z[57];
    z[34]=z[8]*z[155];
    z[34]= - 32*z[34] + z[30] + 52*z[51];
    z[34]=z[8]*z[34];
    z[34]=15*z[34] + 56*z[9] + z[123];
    z[34]=z[8]*z[34];
    z[35]=z[41]*z[68];
    z[23]= - 12*z[23] + z[35] - z[79];
    z[23]=z[23]*z[24];
    z[23]=z[23] + static_cast<T>(381)+ z[34];
    z[23]=z[10]*z[23];
    z[24]=z[3]*z[46];
    z[34]=z[48]*z[61];
    z[24]=z[24] + z[11] + z[34];
    z[24]=z[3]*z[24];
    z[34]= - static_cast<T>(1)+ z[50];
    z[34]=z[2]*z[34];
    z[34]=2*z[34] + z[38];
    z[34]=z[99]*z[34];
    z[35]=static_cast<T>(4)+ z[50];
    z[35]=z[3]*z[35];
    z[34]=3*z[13] + z[35] + z[34];
    z[34]=z[2]*z[34];
    z[24]=z[34] - static_cast<T>(3)+ z[24];
    z[24]=z[15]*z[24];
    z[34]=static_cast<T>(1)+ z[61];
    z[34]=z[7]*z[34];
    z[34]=z[34] + z[131];
    z[34]=z[12]*z[34];
    z[35]= - z[2] - z[80];
    z[35]=z[5]*z[35];
    z[34]=z[34] + static_cast<T>(2)+ z[35];
    z[34]=z[19]*z[34];
    z[24]=z[24] + z[34];
    z[34]= - 20*z[9] - 23*z[11];
    z[34]=z[34]*z[98];
    z[34]= - static_cast<T>(49)+ z[34];
    z[34]=z[34]*z[77];
    z[34]=92*z[180] + z[34] + z[66];
    z[34]=z[34]*z[37];
    z[30]= - 71*z[30] + z[43];
    z[30]=z[30]*z[81];
    z[30]=z[30] - 554*z[9] + 315*z[11];
    z[30]=z[8]*z[30];
    z[30]= - static_cast<T>(161)+ z[30];
    z[30]=z[8]*z[30];

    r += z[21] + z[22] + z[23] + 12*z[24] + z[25] + 6*z[26] + z[27] + 
      z[28] + z[29] + z[30] + z[31] + z[32] + z[33] + z[34] + z[58];
 
    return r;
}

template double qg_2lLC_r70(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r70(const std::array<dd_real,31>&);
#endif
