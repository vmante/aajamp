#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r136(const std::array<T,31>& k) {
  T z[94];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[30];
    z[7]=k[4];
    z[8]=k[12];
    z[9]=k[20];
    z[10]=k[5];
    z[11]=k[7];
    z[12]=k[13];
    z[13]=k[24];
    z[14]=k[23];
    z[15]=k[6];
    z[16]=k[14];
    z[17]=k[15];
    z[18]=k[3];
    z[19]=k[19];
    z[20]=k[17];
    z[21]=k[16];
    z[22]=npow(z[10],2);
    z[23]=3*z[22];
    z[24]=npow(z[10],3);
    z[25]=z[24]*z[17];
    z[26]= - z[23] + z[25];
    z[26]=z[17]*z[26];
    z[27]=2*z[10];
    z[26]=z[27] + z[26];
    z[26]=z[15]*z[26];
    z[28]=z[1]*z[3];
    z[29]=z[28]*z[5];
    z[30]=2*z[5];
    z[29]= - z[3] + z[29] - z[30];
    z[31]= - z[1]*z[29];
    z[32]=z[22]*z[17];
    z[33]=z[32] - z[10];
    z[33]=z[33]*z[3];
    z[34]=z[18]*z[15];
    z[35]=z[5]*z[18];
    z[26]=z[31] + z[26] - static_cast<T>(2)+ z[33] + z[34] - z[35];
    z[26]=z[2]*z[26];
    z[31]=z[22] - z[25];
    z[31]=z[15]*z[31];
    z[36]=z[8]*z[10];
    z[37]=3*z[36];
    z[38]=z[15]*z[10];
    z[38]=z[38] - static_cast<T>(2)- z[37];
    z[38]=z[18]*z[38];
    z[39]=z[28] + static_cast<T>(1)+ z[37];
    z[39]=z[1]*z[39];
    z[40]=3*z[32];
    z[26]=z[26] + z[39] + z[38] + z[31] + z[27] - z[40];
    z[26]=z[6]*z[26];
    z[31]=z[28] - 2;
    z[38]=npow(z[5],2);
    z[31]=z[1]*z[31]*z[38];
    z[39]= - z[27] - z[32];
    z[39]=z[17]*z[39];
    z[39]=static_cast<T>(1)+ z[39];
    z[39]=z[3]*z[39];
    z[41]=9*z[10];
    z[42]=5*z[32];
    z[43]=z[41] - z[42];
    z[43]=z[17]*z[43];
    z[43]= - static_cast<T>(2)+ z[43];
    z[43]=z[15]*z[43];
    z[44]=z[35] + 1;
    z[45]=z[34] - z[44];
    z[45]=z[5]*z[45];
    z[39]= - z[31] + z[45] + z[39] + z[43];
    z[39]=z[2]*z[39];
    z[43]=z[22]*z[8];
    z[45]=2*z[43];
    z[46]=13*z[10];
    z[47]= - z[42] + z[46] - z[45];
    z[47]=z[17]*z[47];
    z[48]=3*z[17];
    z[49]= - z[24]*z[48];
    z[50]=z[24]*z[8];
    z[49]=z[49] + 12*z[22] + z[50];
    z[49]=z[17]*z[49];
    z[51]=7*z[10];
    z[49]=z[49] - z[51] - z[43];
    z[49]=z[15]*z[49];
    z[52]=z[36]*z[17];
    z[53]=z[10] - z[45];
    z[53]=z[8]*z[53];
    z[53]= - static_cast<T>(1)+ z[53];
    z[53]=z[8]*z[53];
    z[54]= - static_cast<T>(2)- z[36];
    z[54]=z[15]*z[54];
    z[53]=z[54] + z[53] + 5*z[52];
    z[53]=z[18]*z[53];
    z[54]=3*z[10];
    z[55]= - z[54] + z[43];
    z[56]=npow(z[8],2);
    z[55]=z[55]*z[56];
    z[29]=2*z[55] + z[29];
    z[29]=z[1]*z[29];
    z[55]= - z[22] + 2*z[50];
    z[55]=z[55]*z[56];
    z[26]=2*z[26] + z[39] + z[29] + z[53] + z[49] - z[33] + z[47] + 
    z[55] + z[44];
    z[26]=z[6]*z[26];
    z[29]=3*z[8];
    z[33]=z[29]*z[22];
    z[39]= - z[27] + z[33];
    z[39]=z[17]*z[39];
    z[44]=z[43] - z[10];
    z[47]=z[18]*z[17];
    z[49]=z[47]*z[8];
    z[53]= - z[44]*z[49];
    z[55]=z[36] + 1;
    z[57]=3*z[55];
    z[58]=z[43] + z[10];
    z[59]= - z[15]*z[58];
    z[39]=z[53] + z[59] + z[57] + z[39];
    z[39]=z[18]*z[39];
    z[53]=npow(z[7],2);
    z[59]=z[3]*z[53]*z[22];
    z[60]=z[7]*z[10];
    z[61]=z[23] - z[60];
    z[61]=z[7]*z[61];
    z[61]=z[61] + z[59];
    z[61]=z[3]*z[61];
    z[61]=z[61] + z[23] - 2*z[60];
    z[61]=z[15]*z[61];
    z[23]= - z[23] - z[60];
    z[23]=z[17]*z[23];
    z[62]=z[17]*z[7];
    z[63]=z[62]*z[22];
    z[64]=3*z[60];
    z[65]=z[64] - z[63];
    z[65]=z[3]*z[65];
    z[66]=3*z[7];
    z[23]=z[39] + z[61] + z[65] + z[66] + z[23];
    z[23]=z[14]*z[23];
    z[39]=3*z[5];
    z[61]= - z[39] - z[8] - z[3];
    z[61]=z[1]*z[61];
    z[23]=z[61] + z[23];
    z[61]= - z[64] + 4*z[22];
    z[61]=z[61]*z[7];
    z[59]=z[61] + 2*z[59];
    z[61]= - z[3]*z[59];
    z[65]=z[51] - z[7];
    z[65]=z[7]*z[65];
    z[61]=z[65] + z[61];
    z[61]=z[3]*z[61];
    z[65]=4*z[43];
    z[67]=5*z[10];
    z[68]=2*z[7];
    z[61]=z[61] - z[68] + z[67] - z[65];
    z[61]=z[15]*z[61];
    z[69]=z[67] - z[43];
    z[69]=z[8]*z[69];
    z[69]= - static_cast<T>(1)+ z[69];
    z[70]=2*z[17];
    z[69]=z[69]*z[70];
    z[27]= - z[27] - z[43];
    z[27]=z[8]*z[27];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[49];
    z[71]=static_cast<T>(3)+ z[36];
    z[71]=z[8]*z[71];
    z[55]= - z[15]*z[55];
    z[27]=z[27] + z[55] + z[71] + z[69];
    z[27]=z[18]*z[27];
    z[55]= - z[60]*z[70];
    z[63]= - z[63] + 4*z[60];
    z[69]= - z[3]*z[63];
    z[55]=z[69] + z[55] - z[10] + z[68];
    z[55]=z[3]*z[55];
    z[69]= - 4*z[10] + z[33];
    z[69]=2*z[69] - z[7];
    z[69]=z[17]*z[69];
    z[71]=z[35] - 1;
    z[72]= - z[7]*z[71];
    z[72]=2*z[18] + z[72];
    z[72]=z[72]*z[30];
    z[73]=2*z[36];
    z[23]=z[72] + z[27] + z[61] + z[55] + z[69] - static_cast<T>(1)+ z[73] + 2*z[23];
    z[23]=z[14]*z[23];
    z[27]=z[7]*z[8];
    z[55]=z[73] + z[27];
    z[61]=z[54] + z[43];
    z[61]=z[17]*z[61];
    z[69]=z[7] + z[10];
    z[72]=3*z[3];
    z[74]=z[69]*z[72];
    z[75]= - z[17]*z[43];
    z[75]=z[37] + z[75];
    z[75]=z[17]*z[75];
    z[75]= - z[29] + z[75];
    z[75]=z[18]*z[75];
    z[76]=z[68] + z[10];
    z[77]=z[17]*z[10];
    z[78]=static_cast<T>(3)- z[77];
    z[78]=z[18]*z[78];
    z[78]=z[78] - z[76];
    z[78]=z[11]*z[78];
    z[71]=z[53]*z[71];
    z[79]=z[66]*z[18];
    z[71]=z[79] + z[71];
    z[71]=z[71]*z[11];
    z[79]=z[27]*z[18];
    z[71]=z[71] - z[79];
    z[79]=z[66] + z[71];
    z[79]=z[5]*z[79];
    z[61]=z[79] + z[78] + z[75] + z[74] + z[61] - z[55];
    z[61]=z[20]*z[61];
    z[44]=z[44]*z[47];
    z[44]=z[44] - z[54];
    z[44]=z[56]*z[44];
    z[74]=npow(z[17],2);
    z[75]=z[74]*z[43];
    z[44]=z[75] + z[44];
    z[44]=z[18]*z[44];
    z[28]= - z[28]*z[30];
    z[28]=z[28] + z[29] + z[5];
    z[28]=z[1]*z[28];
    z[30]=z[18]*z[38];
    z[30]=z[31] + z[3] + z[30];
    z[30]=z[2]*z[30];
    z[31]= - z[70] - z[3];
    z[31]=z[10]*z[31];
    z[28]=z[30] + z[28] + z[35] + z[44] + z[31];
    z[28]=z[4]*z[28];
    z[30]=z[17]*npow(z[7],3);
    z[30]=z[30] + z[53];
    z[31]=z[70]*z[53];
    z[35]= - z[18]*z[62];
    z[35]=z[35] - z[7] - z[31];
    z[35]=z[18]*z[35];
    z[35]=z[35] - z[30];
    z[35]=z[5]*z[35];
    z[38]=z[47] + 1;
    z[44]= - z[62] - z[38];
    z[44]=z[18]*z[44];
    z[35]=z[44] + z[35];
    z[35]=z[35]*z[39];
    z[44]=z[3]*z[7];
    z[75]= - z[5]*z[66];
    z[75]=z[75] + static_cast<T>(9)+ z[44];
    z[75]=z[5]*z[75];
    z[78]=z[1]*z[5];
    z[79]=z[5]*z[44];
    z[79]= - z[3] + z[79];
    z[79]=z[79]*z[78];
    z[75]=z[79] - 2*z[11] + z[75];
    z[75]=z[1]*z[75];
    z[35]=z[75] + static_cast<T>(2)+ z[35];
    z[35]=z[9]*z[35];
    z[55]=z[55] + 1;
    z[55]=z[55]*z[7];
    z[55]=z[55] + z[58];
    z[75]=z[55]*z[44];
    z[79]=z[36]*z[7];
    z[75]=z[75] + z[79] + z[58];
    z[75]=z[3]*z[75];
    z[80]= - static_cast<T>(2)+ z[44];
    z[80]=z[3]*z[80];
    z[80]=z[80] + z[15];
    z[80]=z[2]*z[80];
    z[75]=z[80] - static_cast<T>(1)+ z[75];
    z[75]=z[19]*z[75];
    z[23]=z[26] + z[23] + z[61] + z[35] + z[28] + z[75];
    z[26]= - z[43] - z[79];
    z[26]=z[7]*z[26];
    z[28]= - z[22]*z[44];
    z[26]=z[26] + z[28];
    z[26]=z[12]*z[26];
    z[26]= - z[18] + z[43] + z[26];
    z[28]=22*z[22] - z[64];
    z[28]=z[3]*z[28];
    z[35]=static_cast<T>(3)+ 19*z[36];
    z[35]=z[7]*z[35];
    z[26]=z[28] - 25*z[32] + z[35] - z[54] + 22*z[26];
    z[28]=3*z[12];
    z[26]=z[26]*z[28];
    z[35]=z[32] + z[10];
    z[61]=z[7] - z[35];
    z[61]=z[18]*z[61];
    z[61]=z[61] + z[25] + z[22] - z[60];
    z[61]=z[12]*z[61];
    z[64]=z[27]*z[22];
    z[75]=z[50] + z[22];
    z[64]=z[64] + z[75];
    z[80]=z[15]*z[64];
    z[81]=55*z[22];
    z[82]=49*z[60];
    z[83]=55*z[10];
    z[84]= - z[83] - 49*z[7];
    z[84]=z[18]*z[84];
    z[84]=z[84] + z[81] + z[82];
    z[84]=z[11]*z[84];
    z[61]=66*z[61] + z[84] - 5*z[80] + 78*z[32] + 27*z[10] - 7*z[7];
    z[61]=z[16]*z[61];
    z[80]=49*z[22];
    z[84]= - z[80] + 9*z[25];
    z[84]=z[17]*z[84];
    z[85]=28*z[10];
    z[86]=4*z[7];
    z[84]=z[84] + z[86] - z[85] + 5*z[43];
    z[84]=z[15]*z[84];
    z[54]=z[32] + z[54] - z[68];
    z[54]=z[54]*z[72];
    z[87]=z[60] + z[22];
    z[88]=6*z[3];
    z[89]= - z[87]*z[88];
    z[90]=14*z[7];
    z[91]= - 18*z[10] + z[43];
    z[89]=33*z[18] + z[89] + 3*z[91] - z[90];
    z[89]=z[11]*z[89];
    z[91]=15*z[32] - 94*z[10] + z[33];
    z[91]=z[17]*z[91];
    z[92]=3*z[18];
    z[52]=z[8] - z[52];
    z[52]=z[52]*z[92];
    z[71]= - z[68] - z[71];
    z[71]=z[71]*z[39];
    z[93]=static_cast<T>(20)- z[37];
    z[26]=z[61] + z[26] + z[71] + z[89] + z[52] + z[84] + z[54] + 2*
    z[93] + z[91];
    z[26]=z[16]*z[26];
    z[52]=z[64]*z[16];
    z[54]= - z[58]*z[28];
    z[58]=z[12]*z[43];
    z[58]= - z[36] + z[58];
    z[58]=z[1]*z[12]*z[58];
    z[54]=z[58] + z[54] - static_cast<T>(3)+ z[73];
    z[54]=z[1]*z[54];
    z[58]=3*z[16];
    z[35]=z[35]*z[58];
    z[61]=z[32]*z[16];
    z[64]=z[77] - z[61];
    z[71]=z[2]*z[16];
    z[64]=z[64]*z[71];
    z[84]= - z[10]*z[70];
    z[35]=z[64] + z[35] + static_cast<T>(3)+ z[84];
    z[35]=z[2]*z[35];
    z[64]=z[22]*z[7];
    z[64]=z[64] + z[24];
    z[64]=z[64]*z[17];
    z[64]=z[64] + z[22];
    z[84]=z[64]*z[12];
    z[89]= - 2*z[22] + z[60];
    z[89]=z[17]*z[89];
    z[35]=z[35] + z[54] - z[52] + z[84] + z[89] + z[45] - z[79];
    z[35]=z[13]*z[35];
    z[45]=9*z[43];
    z[54]=z[51] + z[45];
    z[54]=2*z[54] - z[66];
    z[89]=7*z[60];
    z[91]= - 11*z[22] + z[89];
    z[91]=z[17]*z[91];
    z[54]=5*z[84] + 2*z[54] + z[91];
    z[54]=z[12]*z[54];
    z[84]= - static_cast<T>(2)+ z[36];
    z[84]=z[84]*z[66];
    z[52]=5*z[52] + 36*z[32] + z[84] + 24*z[10] - 17*z[43];
    z[52]=z[16]*z[52];
    z[84]= - z[41] - 10*z[32];
    z[58]=z[84]*z[58];
    z[58]=z[58] - static_cast<T>(7)+ 29*z[77];
    z[58]=z[16]*z[58];
    z[61]= - 9*z[61] + 11*z[77];
    z[61]=z[61]*z[16];
    z[61]=z[61] - z[70];
    z[77]= - z[61]*z[71];
    z[84]= - z[17] - z[3];
    z[91]=npow(z[3],2);
    z[91]=2*z[91];
    z[93]=z[1]*z[91];
    z[58]=z[77] + z[93] + 4*z[84] + z[58];
    z[58]=z[2]*z[58];
    z[41]= - z[41] - 10*z[43];
    z[28]=z[41]*z[28];
    z[28]=z[28] - static_cast<T>(13)+ 17*z[36];
    z[28]=z[12]*z[28];
    z[41]=z[12]*z[45];
    z[41]= - 7*z[36] + z[41];
    z[77]=npow(z[12],2);
    z[41]=z[1]*z[41]*z[77];
    z[28]=z[41] - 4*z[3] + z[28];
    z[28]=z[1]*z[28];
    z[41]=23*z[10];
    z[84]= - z[41] + z[68];
    z[84]=z[17]*z[84];
    z[93]=4*z[44];
    z[28]=2*z[35] + z[58] + z[28] + z[52] + z[54] + z[93] + z[84] + 38
    - 15*z[36];
    z[28]=z[13]*z[28];
    z[35]= - 16*z[10] + z[42];
    z[35]=z[35]*z[48];
    z[42]=13*z[22];
    z[25]=z[42] - 6*z[25];
    z[25]=z[17]*z[25];
    z[25]=z[46] + z[25];
    z[25]=z[16]*z[25];
    z[25]=z[25] - static_cast<T>(7)+ z[35];
    z[25]=z[15]*z[25];
    z[35]=z[22]*z[74];
    z[35]=static_cast<T>(2)+ z[35];
    z[35]=z[35]*z[72];
    z[25]=z[35] + z[25];
    z[25]=z[16]*z[25];
    z[35]=z[62]*z[3];
    z[52]=z[70] - z[35];
    z[52]=z[52]*z[72];
    z[54]=z[78]*z[91];
    z[58]=z[71]*z[15]*z[61];
    z[61]=static_cast<T>(29)- 3*z[62];
    z[61]=z[15]*z[17]*z[61];
    z[71]=z[62] + 1;
    z[71]=z[15]*z[71];
    z[71]= - 3*z[71] - 2*z[3];
    z[71]=z[5]*z[71];
    z[25]=z[58] + z[54] + z[25] + z[71] + z[52] + z[61];
    z[25]=z[2]*z[25];
    z[25]=z[28] + z[26] + z[25];
    z[26]=z[44]*z[80];
    z[26]=z[26] + z[81] - z[82];
    z[26]=z[15]*z[26];
    z[28]= - z[34]*z[83];
    z[44]=5*z[64];
    z[44]=z[11]*z[44];
    z[52]=17*z[10] + 26*z[79];
    z[54]=z[3]*z[89];
    z[26]=z[44] + z[28] + z[26] + 3*z[52] + z[54];
    z[26]=z[12]*z[26];
    z[28]=z[59]*z[72];
    z[44]=z[67] + z[66];
    z[44]=z[7]*z[44];
    z[28]=z[28] - 7*z[22] + z[44];
    z[28]=z[3]*z[28];
    z[28]=z[28] + 12*z[7] + 25*z[10] - z[33];
    z[28]=z[15]*z[28];
    z[44]=z[80] - 9*z[50];
    z[44]=z[8]*z[44];
    z[52]=z[17]*z[87];
    z[44]=7*z[52] - z[86] + 35*z[10] + z[44];
    z[44]=z[11]*z[44];
    z[41]= - z[41] + z[45];
    z[41]=z[8]*z[41];
    z[45]=z[27]*z[57];
    z[32]=z[29]*z[32];
    z[52]=z[63]*z[72];
    z[40]=z[52] - z[40] - 42*z[10] - z[7];
    z[40]=z[3]*z[40];
    z[26]=z[26] + z[44] + 9*z[34] + z[28] + z[40] + z[32] + z[45] - 15
    + z[41];
    z[28]=6*z[12];
    z[26]=z[26]*z[28];
    z[32]=z[56]*z[7];
    z[40]=7*z[32];
    z[41]= - z[40] + 45*z[8];
    z[41]= - z[41]*z[38];
    z[44]= - z[8] + 14*z[32];
    z[44]=z[44]*z[62];
    z[45]=69*z[15];
    z[41]= - z[45] + z[44] + z[41];
    z[41]=z[18]*z[41];
    z[38]=z[27]*z[38];
    z[31]=z[8]*z[31];
    z[31]=z[31] + z[38];
    z[31]=z[18]*z[31];
    z[38]=z[8]*z[30];
    z[31]=z[31] + z[38];
    z[38]=z[11]*z[18]*z[68];
    z[31]=3*z[31] + z[38];
    z[31]=z[5]*z[31];
    z[38]=z[40] + 44*z[8];
    z[38]=z[38]*z[7];
    z[44]= - static_cast<T>(50)+ z[38];
    z[44]=z[44]*z[62];
    z[52]=z[53]*z[17];
    z[53]=z[7] + z[52];
    z[45]=z[53]*z[45];
    z[53]=z[47] - z[62];
    z[53]= - static_cast<T>(4)+ 21*z[53];
    z[53]=z[18]*z[53];
    z[53]=21*z[52] + z[53];
    z[53]=z[11]*z[53];
    z[31]=6*z[31] + 3*z[53] + z[41] + z[45] + z[93] + z[44] + static_cast<T>(7)+ z[38];
    z[31]=z[31]*z[39];
    z[38]=z[32] + z[8];
    z[41]=z[27]*z[5];
    z[44]=z[38] - z[41];
    z[44]=z[5]*z[3]*z[44];
    z[45]=z[77]*z[33];
    z[45]= - z[8] + z[45];
    z[45]=z[12]*z[11]*z[45];
    z[44]=z[44] + z[45];
    z[45]=37*z[36];
    z[53]= - static_cast<T>(31)- z[45];
    z[53]=z[53]*z[56];
    z[54]=z[7]*npow(z[8],3);
    z[53]=z[53] + 37*z[54];
    z[54]=7*z[8];
    z[57]=68*z[10] + 37*z[43];
    z[57]=z[57]*z[54];
    z[57]=static_cast<T>(284)+ z[57];
    z[57]=z[11]*z[8]*z[57];
    z[53]=7*z[53] + z[57];
    z[53]=z[3]*z[53];
    z[44]=z[53] + 18*z[44];
    z[44]=z[1]*z[44];
    z[53]=259*z[43];
    z[57]= - 470*z[10] - z[53];
    z[57]=z[57]*z[29];
    z[57]= - static_cast<T>(638)+ z[57];
    z[57]=z[8]*z[57];
    z[45]=static_cast<T>(34)+ z[45];
    z[45]=z[45]*z[32];
    z[45]=z[57] + 21*z[45];
    z[45]=z[3]*z[45];
    z[57]=static_cast<T>(2)- z[27];
    z[57]=z[57]*z[88];
    z[41]=z[41] + z[11];
    z[58]= - 5*z[8] + z[32];
    z[41]=7*z[58] + z[57] + 18*z[41];
    z[39]=z[41]*z[39];
    z[41]=z[56]*z[60];
    z[42]= - z[42] + 6*z[50];
    z[42]=z[8]*z[42];
    z[42]= - z[46] + z[42];
    z[42]=z[11]*z[42];
    z[57]= - 11*z[10] - 6*z[43];
    z[57]=z[8]*z[57];
    z[41]=z[42] + z[57] + 6*z[41];
    z[41]=z[12]*z[41];
    z[42]=z[11]*z[36];
    z[38]=z[41] + 34*z[42] + 3*z[38] + 16*z[3];
    z[28]=z[38]*z[28];
    z[38]= - static_cast<T>(45)+ 518*z[36];
    z[38]=z[38]*z[56];
    z[41]=259*z[50];
    z[42]=729*z[22] + z[41];
    z[42]=z[8]*z[42];
    z[42]=760*z[10] + z[42];
    z[42]=z[8]*z[42];
    z[42]=static_cast<T>(284)+ z[42];
    z[42]=z[42]*z[72];
    z[57]= - 877*z[10] - 554*z[43];
    z[57]=z[8]*z[57];
    z[57]= - static_cast<T>(38)+ z[57];
    z[57]=z[8]*z[57];
    z[42]=z[57] + z[42];
    z[42]=z[11]*z[42];
    z[28]=z[44] + z[28] + z[39] + z[42] + z[38] + z[45];
    z[28]=z[1]*z[28];
    z[38]= - 717*z[22] - z[41];
    z[38]=z[8]*z[38];
    z[38]= - 724*z[10] + z[38];
    z[38]=z[8]*z[38];
    z[38]= - static_cast<T>(40)+ z[38];
    z[39]=464*z[10] + z[53];
    z[39]=z[39]*z[29];
    z[39]=static_cast<T>(568)+ z[39];
    z[39]=z[39]*z[27];
    z[27]= - static_cast<T>(2)- z[27];
    z[27]=z[7]*z[27];
    z[27]=z[43] + z[27];
    z[42]=18*z[17];
    z[27]=z[27]*z[42];
    z[35]=z[55]*z[35];
    z[27]= - 18*z[35] + z[27] + 3*z[38] + z[39];
    z[27]=z[3]*z[27];
    z[35]=z[69]*z[62];
    z[35]=2*z[35] - z[90] - z[67] - z[65];
    z[35]=z[17]*z[35];
    z[38]= - z[17]*z[69];
    z[38]=z[38] - static_cast<T>(3)- z[73];
    z[38]=z[38]*z[42];
    z[39]=401*z[10] + 295*z[43];
    z[39]=z[8]*z[39];
    z[39]=static_cast<T>(145)+ z[39];
    z[39]=z[8]*z[39];
    z[38]= - 36*z[49] + z[39] + z[38];
    z[38]=z[18]*z[38];
    z[39]= - 2919*z[22] - 1072*z[50];
    z[39]=z[8]*z[39];
    z[39]= - 2337*z[10] + z[39];
    z[39]=z[8]*z[39];
    z[42]=npow(z[10],4);
    z[44]=259*z[8];
    z[45]=z[42]*z[44];
    z[49]=976*z[24] + z[45];
    z[49]=z[49]*z[29];
    z[49]=4850*z[22] + z[49];
    z[49]=z[8]*z[49];
    z[49]= - 852*z[7] + 2280*z[10] + z[49];
    z[49]=z[3]*z[49];
    z[35]=z[38] + z[49] + 9*z[35] - static_cast<T>(460)+ z[39];
    z[35]=z[11]*z[35];
    z[38]=z[44]*npow(z[10],5);
    z[38]=z[38] + 1217*z[42];
    z[38]=z[38]*z[8];
    z[38]=z[38] + 2836*z[24];
    z[38]=z[38]*z[8];
    z[39]= - 71*z[7] + 181*z[10];
    z[39]=z[39]*z[86];
    z[38]=z[38] - z[39] + 1878*z[22];
    z[38]=z[38]*z[3];
    z[39]=z[52]*z[69];
    z[44]=z[46] + 40*z[7];
    z[44]=z[44]*z[7];
    z[39]=z[44] - 6*z[39] - 12*z[75];
    z[39]=z[39]*z[48];
    z[42]=z[42]*z[8];
    z[42]=37*z[42] + 142*z[24];
    z[42]=z[42]*z[54];
    z[42]=z[42] + 1326*z[22];
    z[42]=z[42]*z[8];
    z[42]=z[42] - 115*z[7] + 591*z[10];
    z[38]= - z[39] - z[38] + 2*z[42];
    z[39]= - z[15]*z[38];
    z[42]=37*z[50] + 105*z[22];
    z[42]=z[42]*z[8];
    z[42]=z[42] + 96*z[10];
    z[42]=z[42]*z[8];
    z[42]=z[42] + 28;
    z[42]=7*z[42];
    z[44]=z[18]*z[42];
    z[38]=z[44] - z[38];
    z[38]=z[11]*z[38];
    z[44]= - z[15]*z[30];
    z[46]=z[18]*z[7];
    z[30]=z[46] - z[30];
    z[30]=z[11]*z[30];
    z[46]=z[7]*z[34];
    z[30]=z[30] + z[44] + z[46];
    z[30]=z[5]*z[30];
    z[34]=z[34]*z[42];
    z[30]=189*z[30] + z[38] + z[39] + z[34];
    z[30]=z[21]*z[30];
    z[33]=z[85] + z[33];
    z[34]=6*z[76];
    z[38]=z[62]*z[34];
    z[39]= - static_cast<T>(145)- 6*z[36];
    z[39]=z[7]*z[39];
    z[33]=z[38] + 2*z[33] + z[39];
    z[33]=z[33]*z[48];
    z[24]= - 958*z[24] - z[45];
    z[24]=z[8]*z[24];
    z[24]= - 1570*z[22] + z[24];
    z[24]=z[8]*z[24];
    z[38]=696*z[22] + z[41];
    z[38]=z[8]*z[38];
    z[38]=568*z[10] + z[38];
    z[38]=z[8]*z[38];
    z[38]= - static_cast<T>(6)+ z[38];
    z[38]=z[7]*z[38];
    z[24]=z[38] - 36*z[10] + z[24];
    z[24]=z[3]*z[24];
    z[22]=207*z[22] + 74*z[50];
    z[22]=z[22]*z[54];
    z[22]=1104*z[10] + z[22];
    z[22]=z[8]*z[22];
    z[22]=z[24] + z[33] - static_cast<T>(63)+ z[22];
    z[22]=z[15]*z[22];
    z[24]=z[51] - z[43];
    z[24]=z[24]*z[29];
    z[24]=static_cast<T>(34)+ z[24];
    z[29]=static_cast<T>(7)- z[37];
    z[29]=z[8]*z[29];
    z[29]=10*z[29] + z[32];
    z[29]=z[7]*z[29];
    z[32]= - z[17]*z[34];
    z[24]=z[32] + 4*z[24] + z[29];
    z[24]=z[24]*z[48];
    z[29]=13*z[47] + 13;
    z[29]=z[56]*z[29];
    z[32]=static_cast<T>(35)- 18*z[36];
    z[32]=z[8]*z[32];
    z[32]=z[32] + z[40];
    z[32]=z[32]*z[70];
    z[29]=z[32] + z[29];
    z[29]=z[29]*z[92];
    z[32]=z[56]*z[66];
    z[33]=675*z[10] + 518*z[43];
    z[33]=z[8]*z[33];
    z[33]=static_cast<T>(23)+ 2*z[33];
    z[33]=z[8]*z[33];

    r += z[22] + 18*z[23] + z[24] + 6*z[25] + z[26] + z[27] + z[28] + 
      z[29] + z[30] + z[31] + z[32] + z[33] + z[35];
 
    return r;
}

template double qg_2lLC_r136(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r136(const std::array<dd_real,31>&);
#endif
