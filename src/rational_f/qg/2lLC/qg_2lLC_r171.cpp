#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r171(const std::array<T,31>& k) {
  T z[41];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[8];
    z[8]=k[2];
    z[9]=k[6];
    z[10]=k[14];
    z[11]=k[15];
    z[12]=k[10];
    z[13]=k[3];
    z[14]=k[16];
    z[15]=npow(z[5],2);
    z[16]=2*z[6];
    z[17]=z[2] - z[16];
    z[17]=z[9]*z[17];
    z[17]=static_cast<T>(3)+ 2*z[17];
    z[17]=z[17]*z[15];
    z[18]=npow(z[10],2);
    z[19]=2*z[2];
    z[20]=z[19] + 5*z[6];
    z[20]=z[3]*z[20];
    z[20]= - static_cast<T>(3)+ z[20];
    z[20]=z[20]*z[18];
    z[21]= - z[9]*z[15];
    z[22]= - z[3]*z[18];
    z[21]=z[21] + z[22];
    z[21]=z[13]*z[21];
    z[22]= - z[9] - z[3];
    z[22]=z[14]*z[22];
    z[17]=z[21] + z[22] + z[17] + z[20];
    z[17]=z[13]*z[17];
    z[20]=z[3]*z[6];
    z[21]=z[9]*z[6];
    z[22]=z[20] + z[21];
    z[22]=z[22]*z[14];
    z[23]=z[22] - z[3];
    z[24]=z[16] + z[2];
    z[23]=z[24]*z[23];
    z[24]=npow(z[2],2);
    z[25]=z[24]*z[9];
    z[26]=3*z[2];
    z[27]= - z[13] - z[26] - 4*z[25];
    z[27]=z[10]*z[27];
    z[28]=4*z[2];
    z[29]=7*z[6];
    z[30]=z[28] - z[29];
    z[30]=z[9]*z[30];
    z[23]=static_cast<T>(5)+ z[30] + z[27] + z[23];
    z[23]=z[11]*z[23];
    z[27]=z[24]*z[3];
    z[30]=z[25] + z[27];
    z[31]=z[9]*z[2];
    z[32]=z[3]*z[2];
    z[33]= - z[31] - z[32];
    z[33]=z[13]*z[33];
    z[30]=z[33] + 2*z[30];
    z[30]=z[14]*z[30];
    z[33]= - 4*z[27] + z[26] - z[6];
    z[33]=z[5]*z[33];
    z[34]=z[26]*z[3];
    z[30]=z[33] + z[34] - static_cast<T>(1)- 2*z[31] + z[30];
    z[30]=z[4]*z[30];
    z[33]=z[6] - z[2];
    z[35]=z[33]*z[21];
    z[36]=z[6]*z[26];
    z[36]= - z[24] + z[36];
    z[36]=z[36]*z[21];
    z[37]= - z[6]*z[2];
    z[36]=z[37] + z[36];
    z[36]=z[5]*z[36];
    z[37]=3*z[6];
    z[36]=z[36] - z[35] - z[2] + z[37];
    z[36]=z[5]*z[36];
    z[38]=z[33]*z[20];
    z[35]=z[35] + z[38];
    z[35]=z[14]*z[35];
    z[38]=z[33]*z[3];
    z[35]=z[35] - z[38] + z[36];
    z[35]=z[7]*z[35];
    z[36]=npow(z[6],2);
    z[39]=z[36]*z[3];
    z[40]= - z[6] + 3*z[39];
    z[40]=z[10]*z[40];
    z[40]=z[40] - static_cast<T>(1)+ z[20];
    z[40]=z[10]*z[40];
    z[18]= - z[13]*z[18]*z[20];
    z[18]=z[18] - z[22] + z[9] + z[40];
    z[18]=z[13]*z[18];
    z[20]=z[37] - z[39];
    z[20]=z[10]*z[20];
    z[22]=z[9]*z[36];
    z[22]=z[22] + z[39];
    z[22]=z[14]*z[22];
    z[18]=z[18] + z[22] - z[21] + z[20];
    z[18]=z[12]*z[18];
    z[20]=5*z[2] - z[37];
    z[20]=z[6]*z[20];
    z[20]= - z[24] + z[20];
    z[20]=z[9]*z[20];
    z[20]=z[20] - z[26] + z[29];
    z[20]=z[5]*z[20];
    z[19]=z[19] - z[6];
    z[21]=2*z[3];
    z[22]= - z[19]*z[21];
    z[20]=z[20] + static_cast<T>(1)+ z[22];
    z[20]=z[5]*z[20];
    z[22]= - z[28] - z[37];
    z[22]=z[6]*z[22];
    z[22]= - z[24] + z[22];
    z[22]=z[3]*z[22];
    z[22]=z[22] + z[26] + z[29];
    z[22]=z[10]*z[22];
    z[19]= - z[9]*z[19];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=2*z[19] + z[22];
    z[19]=z[10]*z[19];
    z[16]=3*z[27] - z[26] + z[16];
    z[16]=z[5]*z[16];
    z[16]=z[16] + static_cast<T>(2)- z[34];
    z[16]=z[4]*z[16];
    z[22]=z[32] - 1;
    z[24]=z[5]*z[22];
    z[16]= - z[7] + z[16] - z[3] + 3*z[24];
    z[16]=z[5]*z[16];
    z[15]=z[1]*z[4]*z[22]*z[15];
    z[15]=z[15] + z[16];
    z[15]=z[1]*z[15];
    z[16]=z[9]*z[26];
    z[16]= - static_cast<T>(2)+ z[16];
    z[16]=z[10]*z[16];
    z[22]=z[10]*z[31];
    z[22]= - z[9] + z[22];
    z[22]=z[8]*z[11]*z[22];
    z[16]=z[22] - z[12] - 4*z[9] + z[16];
    z[16]=z[10]*z[16];
    z[22]=z[2] + 3*z[25];
    z[22]=z[10]*z[22];
    z[22]=z[22] - static_cast<T>(2)- 9*z[31];
    z[22]=z[10]*z[22];
    z[22]=6*z[9] + z[22];
    z[22]=z[11]*z[22];
    z[16]=z[22] + z[16];
    z[16]=z[8]*z[16];
    z[22]= - z[9]*z[33];
    z[22]=z[22] - z[38];
    z[22]=z[14]*z[22];

    r +=  - z[9] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + 
      z[21] + 2*z[22] + z[23] + z[30] + z[35];
 
    return r;
}

template double qg_2lLC_r171(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r171(const std::array<dd_real,31>&);
#endif
