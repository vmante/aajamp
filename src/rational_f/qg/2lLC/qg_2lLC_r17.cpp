#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r17(const std::array<T,31>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[6];
    z[9]=k[15];
    z[10]=k[5];
    z[11]=k[14];
    z[12]=k[3];
    z[13]=k[7];
    z[14]=npow(z[10],2);
    z[15]=z[14]*z[7];
    z[15]=z[15] + z[10];
    z[16]=13*z[4];
    z[15]=z[15]*z[16];
    z[17]=z[7]*z[10];
    z[18]=z[17]*z[16];
    z[19]=21*z[7];
    z[18]=z[18] + z[19];
    z[18]=z[18]*z[12];
    z[20]=z[18] + z[15] + static_cast<T>(26)+ 57*z[17];
    z[20]=z[4]*z[20];
    z[21]=13*z[13];
    z[20]= - z[21] + 69*z[7] + z[20];
    z[20]=z[12]*z[20];
    z[14]=z[14]*z[8];
    z[14]=z[14] + z[10];
    z[22]=13*z[11];
    z[23]=z[14]*z[22];
    z[24]=z[8]*z[10];
    z[25]=z[22]*z[24];
    z[25]= - 21*z[8] + z[25];
    z[25]=z[2]*z[25];
    z[23]=z[25] + z[23] - static_cast<T>(26)- 57*z[24];
    z[23]=z[11]*z[23];
    z[25]=5*z[4];
    z[26]= - z[3] + 11*z[8];
    z[23]=4*z[26] + z[25] + z[23];
    z[23]=z[2]*z[23];
    z[26]=13*z[24];
    z[14]= - 13*z[14] - 5*z[12];
    z[14]=z[11]*z[14];
    z[27]=3*z[8];
    z[28]= - z[27] + 8*z[7];
    z[28]=z[6]*z[28];
    z[29]= - z[13]*npow(z[12],2);
    z[29]=z[29] + z[2];
    z[29]=z[9]*z[29];
    z[14]=4*z[29] + 6*z[28] + z[23] + z[14] + z[20] + z[15] + 44*z[17]
    + static_cast<T>(30)+ z[26];
    z[14]=z[9]*z[14];
    z[15]= - z[12]*z[22];
    z[15]=static_cast<T>(5)+ z[15];
    z[17]=z[12]*z[13];
    z[20]=z[17] + 1;
    z[15]=z[11]*z[20]*z[15];
    z[23]=z[6]*npow(z[11],2);
    z[28]=z[17]*z[23];
    z[29]=5*z[8];
    z[15]=13*z[28] + z[15] + z[29] + z[19];
    z[15]=z[6]*z[15];
    z[19]=z[7] - z[8];
    z[19]=z[19]*z[6];
    z[28]=z[7]*z[1];
    z[30]=z[12]*z[7];
    z[31]=z[30] + 1;
    z[32]=z[2]*z[8];
    z[33]=z[19] + 2*z[32] + z[28] + z[31];
    z[33]=z[6]*z[33];
    z[34]=z[32] + z[31];
    z[19]=2*z[34] + z[19];
    z[19]=z[6]*z[19];
    z[31]=z[12]*z[31];
    z[32]= - static_cast<T>(1)- z[32];
    z[32]=z[2]*z[32];
    z[19]=z[19] + z[31] + z[32];
    z[31]=z[9]*z[6];
    z[19]=z[19]*z[31];
    z[32]= - z[8]*npow(z[2],2);
    z[19]=z[19] + z[33] + z[1] + z[32];
    z[19]=z[5]*z[19];
    z[32]=z[3]*npow(z[1],2);
    z[33]= - z[1] + z[32];
    z[33]=z[33]*z[16];
    z[34]=z[3]*z[1];
    z[35]= - static_cast<T>(1)+ z[34];
    z[35]=z[35]*z[16];
    z[35]=8*z[3] + z[35];
    z[35]=z[2]*z[35];
    z[33]=z[35] + z[33] + static_cast<T>(8)+ 5*z[34];
    z[33]=z[4]*z[33];
    z[35]=9*z[3];
    z[29]= - 5*z[11] + z[35] - z[29] + z[33];
    z[29]=z[2]*z[29];
    z[20]= - z[12]*z[20];
    z[33]=z[2]*z[3];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[2]*z[33];
    z[20]=z[20] + z[33];
    z[20]=z[20]*z[31];
    z[31]=2*z[13] + 3*z[7];
    z[31]=z[12]*z[31];
    z[31]=static_cast<T>(16)+ z[31];
    z[31]=z[12]*z[31];
    z[33]=3*z[3] + 2*z[8];
    z[33]=z[2]*z[33];
    z[33]=static_cast<T>(16)+ z[33];
    z[33]=z[2]*z[33];
    z[20]=z[20] + z[31] + z[33];
    z[31]= - 3*z[13] + 11*z[7];
    z[31]=z[12]*z[31];
    z[27]=z[27] + 7*z[7];
    z[27]=z[6]*z[27];
    z[27]=z[31] + z[27];
    z[31]=z[35] - 17*z[8];
    z[31]=z[2]*z[31];
    z[27]=z[31] + static_cast<T>(8)+ 3*z[27];
    z[27]=z[6]*z[27];
    z[20]=z[27] + 4*z[20];
    z[20]=z[9]*z[20];
    z[27]=static_cast<T>(21)+ 4*z[28];
    z[25]=z[1]*z[25];
    z[28]= - static_cast<T>(13)- 8*z[17];
    z[28]=z[11]*z[12]*z[28];
    z[15]=4*z[19] + z[20] + z[15] + z[29] + z[28] + 12*z[30] + 2*z[27]
    + z[25];
    z[15]=z[5]*z[15];
    z[19]= - z[1] - z[32];
    z[19]=z[19]*z[16];
    z[20]=z[1] + 2*z[10];
    z[20]=z[7]*z[20];
    z[18]=z[18] + z[19] + 13*z[20] + static_cast<T>(21)+ 8*z[34];
    z[18]=z[4]*z[18];
    z[19]=z[10]*z[13];
    z[20]= - static_cast<T>(3)+ 2*z[19];
    z[25]= - z[17] + z[20];
    z[25]=z[12]*z[25];
    z[19]=z[19] - 3;
    z[27]= - z[10]*z[19];
    z[25]=z[27] + z[25];
    z[25]=z[25]*z[22];
    z[19]=z[25] + 8*z[19] - z[26];
    z[19]=z[11]*z[19];
    z[25]= - static_cast<T>(1)- z[34];
    z[16]=z[25]*z[16];
    z[16]=5*z[3] + z[16];
    z[16]=z[4]*z[16];
    z[24]= - static_cast<T>(2)+ z[24];
    z[24]=z[24]*z[22];
    z[24]= - 34*z[8] + z[24];
    z[24]=z[11]*z[24];
    z[16]=z[16] + z[24];
    z[16]=z[2]*z[16];
    z[17]=3*z[17] - z[20];
    z[17]=z[17]*z[22];
    z[17]=z[17] + 8*z[13] + 13*z[8];
    z[17]=z[11]*z[17];
    z[20]= - z[23]*z[21];
    z[17]=z[17] + z[20];
    z[17]=z[6]*z[17];
    z[20]= - z[3] - z[13];

    r += 39*z[7] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + 4*
      z[20];
 
    return r;
}

template double qg_2lLC_r17(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r17(const std::array<dd_real,31>&);
#endif
