#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r43(const std::array<T,31>& k) {
  T z[57];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[2];
    z[9]=k[10];
    z[10]=k[15];
    z[11]=k[6];
    z[12]=k[14];
    z[13]=k[3];
    z[14]=k[28];
    z[15]=z[2]*z[7];
    z[16]=npow(z[7],2);
    z[17]=z[15] + z[16];
    z[18]=z[17]*z[6];
    z[19]=npow(z[2],2);
    z[20]=z[19]*z[3];
    z[21]=5*z[2];
    z[22]=z[7] + z[21];
    z[22]= - 18*z[18] + 3*z[22] - 5*z[20];
    z[23]=2*z[6];
    z[22]=z[22]*z[23];
    z[24]=npow(z[2],3);
    z[25]=2*z[3];
    z[26]= - z[24]*z[25];
    z[27]=z[2] - z[7];
    z[28]=z[27]*z[2];
    z[26]=z[28] + z[26];
    z[29]=10*z[11];
    z[26]=z[26]*z[29];
    z[24]=z[3]*z[24];
    z[24]= - z[19] + z[24];
    z[30]=16*z[4];
    z[24]=z[24]*z[30];
    z[24]=z[24] + z[26] - 30*z[20] - 10*z[7] + 21*z[2];
    z[24]=z[4]*z[24];
    z[26]=2*z[16];
    z[31]=z[26] + z[15];
    z[31]=z[31]*z[2];
    z[32]=npow(z[7],3);
    z[32]=z[32] + z[31];
    z[32]=z[6]*z[32];
    z[33]=4*z[2];
    z[34]= - 9*z[7] - z[33];
    z[34]=z[2]*z[34];
    z[32]=4*z[32] - 5*z[16] + z[34];
    z[32]=z[6]*z[32];
    z[34]=z[2] + z[7];
    z[32]=z[32] + z[34];
    z[32]=z[12]*z[32];
    z[34]=z[34]*z[13];
    z[34]=z[34] + z[17];
    z[34]=z[34]*z[5];
    z[35]=z[34] - z[2] - z[13];
    z[36]=3*z[14];
    z[35]=z[36]*z[35];
    z[20]=z[20] - z[2];
    z[37]=z[3]*z[2];
    z[38]= - z[13]*z[37];
    z[38]=2*z[20] + z[38];
    z[29]=z[38]*z[29];
    z[38]=z[13]*z[3];
    z[22]=9*z[32] + z[24] + z[22] + z[29] - 20*z[38] - static_cast<T>(33)+ 71*z[37] + 
    z[35];
    z[22]=z[5]*z[22];
    z[24]=2*z[7];
    z[29]=z[24] + z[2];
    z[32]=z[29]*z[2];
    z[32]=z[32] + z[16];
    z[35]=z[32]*z[37];
    z[39]=3*z[7];
    z[40]=z[39] + z[2];
    z[41]= - z[2]*z[40];
    z[41]= - z[26] + z[41];
    z[41]=2*z[41] - z[35];
    z[41]=z[3]*z[41];
    z[42]=z[32]*z[3];
    z[40]=z[42] + z[40];
    z[40]=z[40]*z[25];
    z[43]=z[13]*npow(z[3],2);
    z[44]= - z[29]*z[43];
    z[40]=z[40] + z[44];
    z[40]=z[13]*z[40];
    z[44]=6*z[6];
    z[45]=z[44]*z[16];
    z[46]=4*z[7];
    z[47]=z[46] - z[2];
    z[40]=z[45] + z[40] + z[41] - z[47];
    z[41]=2*z[12];
    z[40]=z[40]*z[41];
    z[48]=z[21] + z[39];
    z[48]=z[48]*z[2];
    z[48]=z[48] - z[35] - z[26];
    z[48]=z[48]*z[3];
    z[49]=3*z[29] + z[48];
    z[49]=z[3]*z[49];
    z[29]=z[29]*z[3];
    z[29]=z[29] - 1;
    z[50]=z[29]*z[43];
    z[51]= - 3*z[2] + z[42] - z[24];
    z[52]=z[51]*z[25];
    z[52]= - static_cast<T>(1)+ z[52];
    z[52]=z[3]*z[52];
    z[52]=z[52] - z[50];
    z[52]=z[13]*z[52];
    z[18]=z[33]*z[18];
    z[53]= - 5*z[7] - z[33];
    z[53]=z[2]*z[53];
    z[18]=z[18] - z[16] + z[53];
    z[18]=z[6]*z[18];
    z[18]=z[42] + z[18];
    z[18]=z[4]*z[18];
    z[42]=z[6]*z[7];
    z[53]= - z[11]*z[46];
    z[18]=z[40] + 3*z[18] - 12*z[42] + z[53] + z[49] + z[52];
    z[18]=z[12]*z[18];
    z[40]=z[24] + z[13];
    z[49]=z[40]*z[13];
    z[49]=z[49] + z[16];
    z[49]=z[49]*z[11];
    z[52]=z[16]*z[3];
    z[53]=z[52] - z[7];
    z[54]=z[3]*z[53];
    z[54]= - static_cast<T>(1)+ z[54];
    z[54]=z[13]*z[54];
    z[54]=z[49] - z[52] + z[54];
    z[54]=z[14]*z[54];
    z[53]=z[53]*z[43];
    z[55]=z[24] - z[52];
    z[55]=z[55]*z[25];
    z[54]=z[54] + z[55] + z[53];
    z[54]=z[13]*z[54];
    z[55]=3*z[16];
    z[56]= - z[46] - z[13];
    z[56]=z[13]*z[56];
    z[56]= - z[55] + z[56];
    z[56]=z[11]*z[56];
    z[54]=z[56] + 3*z[52] + z[54];
    z[54]=z[14]*z[54];
    z[43]= - z[16]*z[43];
    z[43]= - z[45] + 4*z[52] + z[43];
    z[41]=z[41]*z[43];
    z[43]= - z[39] + z[52];
    z[43]=z[43]*z[25];
    z[41]=z[41] + z[43] - z[53];
    z[41]=z[13]*z[41];
    z[43]= - z[24] - z[52];
    z[45]=8*z[7];
    z[52]=z[13]*z[45];
    z[52]=z[16] + z[52];
    z[53]=3*z[6];
    z[52]=z[52]*z[53];
    z[41]=z[52] + 3*z[43] + z[41];
    z[41]=z[12]*z[41];
    z[43]= - 3*z[13] - z[49];
    z[43]=z[43]*z[53];
    z[49]=z[11]*z[13];
    z[41]=z[41] + z[43] + z[54] + static_cast<T>(5)+ z[49];
    z[41]=z[9]*z[41];
    z[43]= - z[7]*z[38];
    z[24]= - z[24] + z[43];
    z[24]=z[9]*z[24];
    z[24]=z[24] - static_cast<T>(1)- z[38];
    z[34]= - z[7] - z[34];
    z[34]=z[5]*z[34];
    z[24]=z[34] + 2*z[24];
    z[24]=z[10]*z[24];
    z[18]=z[24] + z[18] + z[41];
    z[24]=z[16] - z[19];
    z[24]=2*z[24] + z[35];
    z[24]=z[3]*z[24];
    z[32]= - z[32]*z[25];
    z[32]=z[32] + z[39] + z[33];
    z[32]=z[3]*z[32];
    z[29]=z[29]*z[38];
    z[29]=z[29] - static_cast<T>(6)+ z[32];
    z[29]=z[13]*z[29];
    z[32]= - z[13] + 2*z[27];
    z[32]=z[32]*z[13];
    z[33]=z[47]*z[2];
    z[32]=z[32] + z[33] - z[16];
    z[32]=z[32]*z[13];
    z[34]=z[15] - z[16];
    z[41]=2*z[2];
    z[34]=z[34]*z[41];
    z[32]=z[32] - z[34];
    z[32]=z[32]*z[11];
    z[26]=z[32] - z[26];
    z[32]=z[41] - z[13];
    z[34]=2*z[13];
    z[43]= - z[32]*z[34];
    z[47]= - z[39] + z[2];
    z[47]=z[2]*z[47];
    z[43]=z[43] + z[47] + z[26];
    z[43]=z[11]*z[43];
    z[24]=z[43] + z[29] + 6*z[2] + z[24];
    z[24]=z[14]*z[24];
    z[29]=8*z[2] - z[48];
    z[29]=z[3]*z[29];
    z[43]= - z[3]*z[51];
    z[43]= - static_cast<T>(2)+ z[43];
    z[25]=z[43]*z[25];
    z[25]=z[25] + z[50];
    z[25]=z[13]*z[25];
    z[43]=z[41] - z[7];
    z[47]= - 3*z[43] + 5*z[13];
    z[47]=z[47]*z[13];
    z[33]= - z[47] + z[33] - z[26];
    z[47]=z[11]*z[33];
    z[34]= - z[2] + z[34];
    z[34]=4*z[34] + z[47];
    z[34]=z[11]*z[34];
    z[24]=z[24] + z[34] + z[25] - static_cast<T>(4)+ z[29];
    z[24]=z[24]*z[36];
    z[25]=z[11]*z[19]*z[16];
    z[29]=z[31] - z[25];
    z[29]=z[29]*z[11];
    z[15]=4*z[15];
    z[31]= - z[29] + z[55] + z[15];
    z[31]=z[11]*z[31];
    z[17]=z[2]*z[17];
    z[17]=z[17] - z[25];
    z[17]=z[11]*z[17];
    z[17]=z[17] + z[19] - z[35];
    z[17]=z[14]*z[17];
    z[25]= - z[46] - z[2];
    z[25]=z[2]*z[25];
    z[25]= - z[55] + z[25];
    z[25]=z[3]*z[25];
    z[17]=z[17] + z[25] + z[31];
    z[17]=z[17]*z[36];
    z[21]=z[43]*z[21];
    z[21]= - 3*z[29] + 29*z[16] + z[21];
    z[21]=z[11]*z[21];
    z[15]= - z[15] - z[29];
    z[15]=z[15]*z[44];
    z[15]=z[15] + z[21] - 28*z[7] + 47*z[2];
    z[15]=z[6]*z[15];
    z[21]=z[27]*z[37]*z[30];
    z[25]=3*z[3];
    z[29]=z[45] - 11*z[2];
    z[29]=z[29]*z[25];
    z[16]= - z[16] - z[28];
    z[16]=z[3]*z[16];
    z[16]= - z[39] + 10*z[16];
    z[16]=z[11]*z[16];
    z[15]=z[21] + z[15] + z[17] + 2*z[16] + static_cast<T>(35)+ z[29];
    z[15]=z[4]*z[15];
    z[16]=3*z[11];
    z[16]=z[33]*z[16];
    z[16]=z[16] + 35*z[13] + 37*z[7] - 29*z[2];
    z[16]=z[11]*z[16];
    z[17]=z[7] + z[32];
    z[17]=z[13]*z[17];
    z[17]=z[17] - z[19] - z[26];
    z[17]=z[11]*z[17];
    z[17]=3*z[40] + z[17];
    z[17]=z[17]*z[44];
    z[19]=z[3]*z[7];
    z[16]=z[17] + z[16] - static_cast<T>(63)+ 44*z[19];
    z[16]=z[6]*z[16];
    z[17]=z[37] - 1;
    z[19]=z[17]*z[53];
    z[19]= - 4*z[3] + z[19];
    z[19]=z[19]*z[23];
    z[21]=4*z[4];
    z[17]=z[17]*z[21];
    z[17]=z[25] + z[17];
    z[17]=z[4]*z[17];
    z[17]=z[19] + z[17];
    z[17]=z[1]*z[17];
    z[19]=30*z[42] + static_cast<T>(27)- 68*z[37];
    z[19]=z[6]*z[19];
    z[23]=2*z[37];
    z[20]=z[4]*z[20];
    z[20]=32*z[20] + static_cast<T>(11)+ z[23];
    z[20]=z[4]*z[20];
    z[17]=4*z[17] + z[20] + 40*z[3] + z[19];
    z[17]=z[5]*z[17];
    z[19]= - static_cast<T>(7)+ 4*z[37];
    z[19]=z[19]*z[53];
    z[19]= - 38*z[3] + z[19];
    z[19]=z[6]*z[19];
    z[20]= - static_cast<T>(1)+ z[23];
    z[20]=z[4]*z[20];
    z[20]=2*z[20] + z[3] + z[6];
    z[20]=z[20]*z[21];
    z[19]=z[19] + z[20];
    z[17]=2*z[19] + z[17];
    z[17]=z[1]*z[17];
    z[19]=z[3]*z[27];
    z[19]= - 10*z[38] - static_cast<T>(7)+ 20*z[19];
    z[19]=z[11]*z[19];
    z[20]= - z[10]*z[7];
    z[20]=z[20] - 2;
    z[20]=z[9]*z[4]*z[20];
    z[21]= - z[12]*z[41];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[12]*z[11]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[8]*z[20];

    r += 66*z[3] + z[15] + z[16] + z[17] + 3*z[18] + z[19] + 6*z[20] + 
      z[22] + z[24];
 
    return r;
}

template double qg_2lLC_r43(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r43(const std::array<dd_real,31>&);
#endif
