#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r144(const std::array<T,31>& k) {
  T z[105];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[30];
    z[7]=k[4];
    z[8]=k[12];
    z[9]=k[20];
    z[10]=k[7];
    z[11]=k[22];
    z[12]=k[13];
    z[13]=k[23];
    z[14]=k[5];
    z[15]=k[3];
    z[16]=k[6];
    z[17]=k[15];
    z[18]=k[14];
    z[19]=k[28];
    z[20]=k[16];
    z[21]=npow(z[7],2);
    z[22]=z[21]*z[14];
    z[23]=npow(z[7],3);
    z[22]=z[22] + z[23];
    z[24]= - z[3]*z[22];
    z[25]=z[14]*z[7];
    z[26]=3*z[25];
    z[27]=z[23]*z[5];
    z[24]=z[24] + z[27] - z[26];
    z[24]=z[3]*z[24];
    z[28]=3*z[7];
    z[29]=z[21]*z[5];
    z[30]=2*z[29];
    z[31]=3*z[14];
    z[32]=z[5]*z[7];
    z[33]=static_cast<T>(2)+ z[32];
    z[33]=z[15]*z[33];
    z[24]=z[24] + z[33] - z[31] + z[28] + z[30];
    z[24]=z[16]*z[24];
    z[33]=z[3]*z[14];
    z[34]=z[33] - 1;
    z[35]=z[23]*z[34];
    z[36]=z[21]*z[31];
    z[35]=z[36] + z[35];
    z[35]=z[3]*z[35];
    z[36]=2*z[21];
    z[37]=z[14] - z[7];
    z[38]=z[15]*z[37];
    z[35]=z[35] + z[38] - z[36] + z[26];
    z[35]=z[16]*z[35];
    z[26]=z[26] + z[21];
    z[38]=z[33]*z[21];
    z[39]=2*z[7];
    z[40]=z[31] + z[39];
    z[41]= - z[15] - z[40];
    z[41]=z[15]*z[41];
    z[41]= - z[38] + z[41] - z[26];
    z[41]=z[17]*z[41];
    z[42]=z[32] - 1;
    z[43]=z[42]*z[15];
    z[43]=z[43] + z[29];
    z[44]=z[21]*z[3];
    z[45]=z[44] + z[43];
    z[46]=npow(z[15],2);
    z[47]=z[46]*z[17];
    z[48]=z[47]*z[8];
    z[49]=z[48]*z[14];
    z[35]=z[49] + z[41] + 3*z[45] + z[35];
    z[35]=z[13]*z[35];
    z[41]=z[15]*z[14];
    z[45]=z[16]*z[41];
    z[50]=2*z[14];
    z[51]=z[50] + z[15];
    z[51]=z[17]*z[15]*z[51];
    z[45]=z[49] + z[45] + z[51];
    z[45]=z[8]*z[45];
    z[49]=z[15]*z[5];
    z[51]=z[30] + z[7];
    z[51]=z[51]*z[5];
    z[52]= - static_cast<T>(7)- z[51];
    z[52]=z[52]*z[49];
    z[53]=3*z[29];
    z[54]=5*z[44] + z[7] - z[53];
    z[54]=z[3]*z[54];
    z[55]=z[39]*z[14];
    z[56]=z[55] + z[38];
    z[57]= - z[21] + z[56];
    z[57]=z[3]*z[57];
    z[58]= - z[7] - z[15];
    z[57]=3*z[58] + z[57];
    z[57]=z[17]*z[57];
    z[58]=5*z[32];
    z[59]=static_cast<T>(1)- z[58];
    z[59]=z[5]*z[59];
    z[60]=z[3]*z[58];
    z[59]=z[59] + z[60];
    z[59]=z[1]*z[59];
    z[24]=2*z[35] + z[45] + z[59] + z[57] + z[24] + z[54] + z[52] - static_cast<T>(4)- 
   13*z[32];
    z[24]=z[13]*z[24];
    z[35]=2*z[3];
    z[45]= - z[5] - z[35];
    z[45]=z[3]*z[45];
    z[52]=z[2]*z[5]*npow(z[3],2);
    z[45]=z[45] + z[52];
    z[45]=z[2]*z[45];
    z[52]=static_cast<T>(2)+ z[33];
    z[52]=z[3]*z[52];
    z[54]= - z[10]*z[33];
    z[45]=z[54] + z[45] + z[5] + z[52];
    z[45]=z[1]*z[45];
    z[52]=npow(z[14],2);
    z[54]=z[52]*z[10];
    z[57]= - z[54] + z[37];
    z[57]=z[8]*z[57];
    z[59]=z[32] - 2;
    z[60]=z[10]*z[14];
    z[57]=z[57] + z[60] + z[59];
    z[57]=z[1]*z[57];
    z[60]=z[39] + z[14];
    z[54]=z[54] - z[60] + z[57];
    z[54]=z[8]*z[54];
    z[57]=3*z[17];
    z[61]=z[3]*z[5];
    z[62]= - z[2]*z[61];
    z[62]=z[62] - z[3] - z[57];
    z[62]=z[2]*z[62];
    z[63]=z[31]*z[17];
    z[45]=z[54] + z[45] + z[62] + 2*z[34] + z[63];
    z[45]=z[11]*z[45];
    z[54]=npow(z[5],2);
    z[62]=z[54]*z[23];
    z[64]=z[32] + 1;
    z[65]=z[64]*z[49];
    z[51]= - z[51] - z[65];
    z[51]=z[15]*z[51];
    z[51]= - z[62] + z[51];
    z[51]=z[17]*z[51];
    z[66]=3*z[1];
    z[42]=z[42]*z[61]*z[66];
    z[67]=3*z[3];
    z[68]=z[32]*z[67];
    z[69]=static_cast<T>(11)- z[32];
    z[69]=z[5]*z[69];
    z[42]=z[42] + 2*z[10] + z[69] + z[68];
    z[42]=z[1]*z[42];
    z[68]= - z[21]*z[54];
    z[42]=z[42] + z[51] - z[65] - static_cast<T>(2)+ z[68];
    z[42]=z[9]*z[42];
    z[24]=z[42] + z[24] + z[45];
    z[42]=z[52]*z[16];
    z[45]=z[42]*z[17];
    z[51]=4*z[45];
    z[65]=11*z[14];
    z[68]=z[16]*z[65];
    z[68]=z[51] + 6*z[33] + z[68];
    z[68]=z[17]*z[68];
    z[68]=z[68] + 7*z[3] - 24*z[16];
    z[68]=z[17]*z[68];
    z[69]=z[54]*z[15];
    z[70]=5*z[69];
    z[71]= - z[5] - z[70];
    z[71]=z[16]*z[71];
    z[71]=z[61] - z[71];
    z[72]=npow(z[5],3);
    z[73]=z[72]*z[15];
    z[74]=8*z[73];
    z[68]=z[68] + 13*z[54] + z[74] - 2*z[71];
    z[68]=z[2]*z[68];
    z[71]= - static_cast<T>(5)+ 8*z[33];
    z[75]=z[16]*z[14];
    z[76]=6*z[14];
    z[77]= - z[17]*z[76];
    z[71]=z[77] + 3*z[71] - 16*z[75];
    z[71]=z[17]*z[71];
    z[75]= - 12*z[3] - 15*z[5] - 4*z[69];
    z[77]=static_cast<T>(5)- 6*z[49];
    z[77]=z[16]*z[77];
    z[68]=z[68] + z[71] + 2*z[75] + z[77];
    z[68]=z[2]*z[68];
    z[71]=z[16]*z[31];
    z[45]= - z[45] - z[33] + z[71];
    z[45]=z[17]*z[45];
    z[71]=2*z[5];
    z[75]= - static_cast<T>(2)- z[49];
    z[75]=z[16]*z[75];
    z[45]=z[45] + z[75] + z[3] + z[71] + z[69];
    z[45]=z[2]*z[45];
    z[75]=z[31] + z[42];
    z[75]=z[17]*z[75];
    z[77]=z[15] + z[14];
    z[78]=z[77]*z[16];
    z[79]= - static_cast<T>(1)+ z[49];
    z[45]=z[45] + z[75] + 2*z[79] - z[78];
    z[45]=z[2]*z[45];
    z[75]= - 2*z[54] - z[61];
    z[75]=z[2]*z[75];
    z[79]=z[54]*z[3];
    z[80]=z[2]*z[79];
    z[80]= - z[61] + z[80];
    z[80]=z[1]*z[80];
    z[75]=z[80] - z[5] + z[75];
    z[75]=z[2]*z[75];
    z[75]= - static_cast<T>(3)+ z[75];
    z[75]=z[1]*z[75];
    z[80]=3*z[15];
    z[45]=z[75] + z[80] + z[45];
    z[45]=z[6]*z[45];
    z[75]=13*z[14];
    z[81]=21*z[15];
    z[42]=7*z[42] + z[75] + z[81];
    z[42]=z[17]*z[42];
    z[82]=12*z[61];
    z[83]=17*z[54] + z[82];
    z[84]= - z[54]*z[67];
    z[85]=16*z[72];
    z[84]= - z[85] + z[84];
    z[84]=z[2]*z[84];
    z[83]=2*z[83] + z[84];
    z[83]=z[2]*z[83];
    z[84]=z[72]*z[2];
    z[86]=z[84]*z[3];
    z[87]= - 13*z[79] + 4*z[86];
    z[87]=z[2]*z[87];
    z[87]=9*z[61] + z[87];
    z[87]=z[1]*z[87];
    z[83]=2*z[87] + 19*z[5] + z[83];
    z[83]=z[1]*z[83];
    z[87]=z[41] - z[52];
    z[88]= - z[1]*z[14];
    z[88]=z[88] + z[87];
    z[88]=z[8]*z[88];
    z[89]=4*z[14];
    z[88]=11*z[88] + 31*z[1] + z[89] + 27*z[15];
    z[88]=z[8]*z[88];
    z[42]=11*z[45] + z[88] + z[83] + z[68] + z[42] - 7*z[78] + static_cast<T>(9)- 16*
    z[49];
    z[42]=z[6]*z[42];
    z[45]=z[67]*z[18];
    z[68]=z[45] - 3*z[54] - z[74];
    z[68]=z[2]*z[68];
    z[74]=3*z[18];
    z[78]=2*z[33];
    z[83]=static_cast<T>(3)- z[78];
    z[83]=z[83]*z[74];
    z[88]=6*z[3];
    z[90]=3*z[5];
    z[68]=z[68] + z[83] + z[88] - z[90] - z[70];
    z[68]=z[2]*z[68];
    z[70]= - z[5] + z[3];
    z[70]=z[3]*z[70];
    z[70]= - 4*z[54] + z[70];
    z[83]=z[54] - z[61];
    z[83]=z[83]*z[67];
    z[83]=z[85] + z[83];
    z[83]=z[2]*z[83];
    z[70]=6*z[70] + z[83];
    z[70]=z[2]*z[70];
    z[83]=7*z[5];
    z[34]= - z[3]*z[34];
    z[34]= - z[83] + z[34];
    z[85]=29*z[79] - 8*z[86];
    z[85]=z[2]*z[85];
    z[85]= - 21*z[61] + z[85];
    z[85]=z[1]*z[85];
    z[34]=z[85] + 3*z[34] + z[70];
    z[34]=z[1]*z[34];
    z[46]=z[46]*z[74];
    z[48]= - z[31]*z[48];
    z[46]=z[48] + 15*z[47] + 19*z[15] + z[46];
    z[46]=z[8]*z[46];
    z[47]= - static_cast<T>(3)+ 3*z[33];
    z[48]= - z[49] - z[47];
    z[70]=z[52]*z[3];
    z[85]= - z[14] + z[15];
    z[85]=3*z[85] + z[70];
    z[85]=z[85]*z[74];
    z[34]=z[46] + z[34] + z[68] - z[63] + 2*z[48] + z[85];
    z[34]=z[4]*z[34];
    z[34]=z[42] + z[34];
    z[42]=259*z[5];
    z[46]=z[42]*z[23];
    z[48]= - 36*z[21] + z[46];
    z[48]=z[5]*z[48];
    z[48]=455*z[7] + z[48];
    z[48]=z[48]*z[90];
    z[68]=6*z[18];
    z[85]=z[52]*z[18];
    z[86]=7*z[85];
    z[91]= - 80*z[14] + z[86];
    z[91]=z[91]*z[68];
    z[48]=z[91] + static_cast<T>(1036)+ z[48];
    z[48]=z[16]*z[48];
    z[91]=259*z[29];
    z[92]=54*z[7] - z[91];
    z[92]=z[92]*z[90];
    z[92]= - static_cast<T>(605)+ z[92];
    z[92]=z[5]*z[92];
    z[93]=18*z[3];
    z[94]= - static_cast<T>(3)+ z[58];
    z[94]=z[94]*z[93];
    z[95]=z[18]*z[14];
    z[96]=static_cast<T>(5)- z[33];
    z[96]=3*z[96] - 20*z[95];
    z[96]=z[18]*z[96];
    z[97]=z[17]*z[95];
    z[48]=36*z[97] + z[48] + 24*z[96] + z[92] + z[94];
    z[48]=z[17]*z[48];
    z[92]=259*z[32];
    z[94]=static_cast<T>(46)- z[92];
    z[94]=z[94]*z[69];
    z[96]= - z[7]*z[67];
    z[96]= - static_cast<T>(17)+ z[96];
    z[97]=55*z[14] + 6*z[7];
    z[98]=z[18]*z[97];
    z[96]=2*z[96] + z[98];
    z[98]=2*z[18];
    z[96]=z[96]*z[98];
    z[99]=z[32]*z[3];
    z[100]= - 60*z[7] + z[91];
    z[100]=z[5]*z[100];
    z[100]=static_cast<T>(36)+ z[100];
    z[100]=z[5]*z[100];
    z[94]=z[96] + 12*z[99] + z[100] + z[94];
    z[96]=3*z[16];
    z[94]=z[94]*z[96];
    z[86]= - z[65] + z[86];
    z[86]=z[16]*z[86];
    z[51]= - z[51] + z[86];
    z[51]=z[18]*z[51];
    z[86]= - z[33]*z[68];
    z[51]=z[86] + z[51];
    z[51]=z[17]*z[51];
    z[45]=z[51] - 8*z[61] + z[45];
    z[51]=9*z[7];
    z[86]=z[51] - 37*z[29];
    z[83]=z[86]*z[83];
    z[83]= - static_cast<T>(356)+ z[83];
    z[83]=z[5]*z[83];
    z[86]= - static_cast<T>(36)+ 49*z[95];
    z[86]=z[86]*z[68];
    z[83]=z[83] + z[86];
    z[83]=z[16]*z[83];
    z[45]=z[83] + 6*z[45];
    z[45]=z[17]*z[45];
    z[83]=z[3] - z[98];
    z[83]=z[18]*z[83];
    z[83]=z[61] - z[83];
    z[86]=static_cast<T>(81)- z[92];
    z[86]=z[86]*z[54];
    z[73]=259*z[73];
    z[83]=z[86] + z[73] - 18*z[83];
    z[83]=z[16]*z[83];
    z[45]=z[45] + z[83] - 259*z[72] + 18*z[79];
    z[45]=z[2]*z[45];
    z[72]=z[5] + z[3];
    z[72]=z[72]*z[93];
    z[83]=6*z[32];
    z[86]= - static_cast<T>(55)- z[83];
    z[86]=z[86]*z[98];
    z[92]= - static_cast<T>(65)- 12*z[32];
    z[92]=z[5]*z[92];
    z[86]=z[92] + z[86];
    z[86]=z[86]*z[68];
    z[45]=z[45] + z[48] + z[94] + z[86] + z[72] + 521*z[54] + z[73];
    z[45]=z[2]*z[45];
    z[48]=z[60] + z[29];
    z[48]=z[48]*z[15];
    z[60]=z[60]*z[14];
    z[60]=z[60] + z[21];
    z[72]=2*z[60];
    z[48]=z[48] - z[72];
    z[48]=z[48]*z[15];
    z[73]=z[60]*z[14];
    z[48]=z[48] + z[73];
    z[73]=z[48]*z[18];
    z[86]=z[60]*z[33];
    z[92]= - z[21] + z[52];
    z[94]=z[89] - z[29];
    z[95]=z[94] + z[28];
    z[100]=z[64]*z[15];
    z[101]=z[100] - z[95];
    z[101]=z[15]*z[101];
    z[92]= - z[73] + z[86] + 2*z[92] + z[101];
    z[92]=z[18]*z[92];
    z[101]=4*z[7];
    z[102]= - z[101] + z[14];
    z[102]=z[14]*z[102];
    z[103]= - 2*z[37] + z[15];
    z[103]=z[15]*z[103];
    z[102]=z[103] + z[21] + z[102];
    z[102]=z[15]*z[102];
    z[103]= - z[21] + z[25];
    z[103]=z[103]*z[50];
    z[104]=z[52]*z[44];
    z[102]=z[104] + z[103] + z[102];
    z[102]=z[12]*z[102];
    z[64]= - 2*z[64] - z[49];
    z[64]=z[15]*z[64];
    z[64]=z[64] + z[94];
    z[64]=z[15]*z[64];
    z[94]=z[25] + z[21];
    z[103]= - z[94]*z[33];
    z[28]=z[28] - z[14];
    z[28]=z[14]*z[28];
    z[28]=z[102] + z[103] + z[64] + z[36] + z[28];
    z[28]=z[12]*z[28];
    z[64]=static_cast<T>(6)+ z[49];
    z[64]=z[15]*z[64];
    z[28]=z[28] + z[92] - z[70] - z[76] + z[64];
    z[28]=z[19]*z[28];
    z[64]=5*z[100] - z[95];
    z[64]=z[15]*z[64];
    z[92]=z[33]*z[72];
    z[60]= - z[73] + z[92] - 3*z[60] + z[64];
    z[60]=z[18]*z[60];
    z[64]=z[3]*z[72];
    z[72]=static_cast<T>(9)- z[58];
    z[72]=z[15]*z[72];
    z[60]=z[60] + z[64] - z[29] + z[72];
    z[60]=z[18]*z[60];
    z[64]= - static_cast<T>(3)+ z[32];
    z[64]=2*z[64] + z[49];
    z[64]=z[15]*z[64];
    z[72]=8*z[7];
    z[73]=z[72] - z[14];
    z[64]=z[64] + z[29] - z[73];
    z[64]=z[15]*z[64];
    z[92]=z[7] + z[50];
    z[92]=z[14]*z[92];
    z[92]= - z[21] + z[92];
    z[95]=z[21] + 4*z[25];
    z[95]=z[95]*z[33];
    z[64]=z[95] + 2*z[92] + z[64];
    z[64]=z[12]*z[64];
    z[58]=4*z[49] + static_cast<T>(7)+ z[58];
    z[58]=z[15]*z[58];
    z[55]= - z[21] - z[55];
    z[55]=z[55]*z[35];
    z[55]=z[64] + z[55] + z[58] + z[29] + z[89];
    z[55]=z[12]*z[55];
    z[28]=z[28] + z[55] - static_cast<T>(8)+ z[60];
    z[55]=18*z[19];
    z[28]=z[28]*z[55];
    z[58]=3*z[12];
    z[60]= - z[37]*z[58];
    z[58]=z[52]*z[58];
    z[58]=z[65] + z[58];
    z[58]=z[10]*z[58];
    z[58]=z[60] + z[58];
    z[58]=z[1]*z[58];
    z[60]=z[14] + z[7];
    z[64]=z[60]*z[15];
    z[64]=z[64] + z[94];
    z[65]= - 3*z[19] + z[57];
    z[64]=z[64]*z[65];
    z[65]=z[10]*z[87];
    z[58]=z[58] - 11*z[65] + z[64];
    z[58]=z[8]*z[58];
    z[64]=2*z[73] + z[81];
    z[64]=z[17]*z[64];
    z[58]=z[64] + z[58];
    z[64]=10*z[12];
    z[65]=z[64]*z[37];
    z[59]= - 3*z[59] + z[65];
    z[73]=2*z[12];
    z[59]=z[59]*z[73];
    z[64]=z[64]*z[52];
    z[31]= - z[31] - z[64];
    z[31]=z[31]*z[73];
    z[73]=static_cast<T>(16)- 27*z[33];
    z[31]=7*z[73] + z[31];
    z[31]=z[10]*z[31];
    z[73]=25*z[32];
    z[89]=static_cast<T>(63)- z[73];
    z[61]=z[89]*z[61];
    z[89]=z[10]*z[3];
    z[61]=z[61] - 63*z[89];
    z[61]=z[1]*z[61];
    z[89]=static_cast<T>(189)- 107*z[32];
    z[89]=z[3]*z[89];
    z[31]=z[61] + z[31] + z[59] - 82*z[5] + z[89];
    z[31]=z[1]*z[31];
    z[59]=19*z[7];
    z[61]=21*z[14];
    z[89]= - z[59] + z[61];
    z[89]=z[89]*z[33];
    z[89]= - 38*z[14] + z[89];
    z[89]=z[89]*z[96];
    z[64]= - z[64] - 189*z[70] + 274*z[14] - 107*z[15];
    z[64]=z[10]*z[64];
    z[70]= - z[39] + z[77];
    z[55]=z[70]*z[55];
    z[70]= - 139*z[7] + 189*z[14];
    z[70]=z[3]*z[70];
    z[31]=z[31] + z[55] + z[64] + z[65] + z[89] - static_cast<T>(172)+ z[70] + 2*z[58];
    z[31]=z[8]*z[31];
    z[43]=z[50] - z[39] + z[43];
    z[43]=z[15]*z[43];
    z[37]= - z[14]*z[37];
    z[37]=z[43] - z[21] + z[37];
    z[43]=22*z[18];
    z[37]=z[37]*z[43];
    z[55]= - 22*z[49] + static_cast<T>(3)- z[73];
    z[55]=z[15]*z[55];
    z[58]=3*z[21];
    z[64]=z[58] + 44*z[25];
    z[64]=z[3]*z[64];
    z[65]=19*z[14];
    z[37]=z[37] + z[64] + z[55] + z[65] - z[59] - z[53];
    z[37]=z[37]*z[74];
    z[55]= - z[49] - static_cast<T>(3)- 2*z[32];
    z[55]=z[55]*z[80];
    z[59]=24*z[14];
    z[55]=z[55] + z[59] - z[72] - z[53];
    z[55]=z[15]*z[55];
    z[47]= - z[23]*z[47];
    z[26]= - z[26]*z[50];
    z[26]=z[26] + z[47];
    z[26]=z[26]*z[35];
    z[35]=13*z[7] - z[76];
    z[35]=z[35]*z[50];
    z[26]=z[26] + z[55] + 7*z[21] + z[35];
    z[26]=z[16]*z[26];
    z[35]=z[38] - z[21];
    z[38]=66*z[18];
    z[35]= - z[35]*z[38];
    z[47]=122*z[7];
    z[26]=z[26] + z[35] - 12*z[44] - z[47] - z[81];
    z[26]=z[12]*z[26];
    z[22]=z[22]*z[67];
    z[22]=z[22] + 42*z[21] - 25*z[25];
    z[22]=z[3]*z[22];
    z[35]=3*z[32];
    z[50]=z[49] + static_cast<T>(11)+ z[35];
    z[50]=z[15]*z[50];
    z[30]=z[50] + 15*z[7] + z[30];
    z[30]= - z[75] + 2*z[30];
    z[22]=3*z[30] + z[22];
    z[22]=z[16]*z[22];
    z[30]= - z[41] + 2*z[52];
    z[41]=z[15]*z[30];
    z[50]=npow(z[14],3);
    z[41]= - z[50] + z[41];
    z[41]=z[41]*z[43];
    z[41]= - 41*z[87] + z[41];
    z[41]=z[18]*z[41];
    z[43]= - z[3]*z[56];
    z[41]=z[41] - z[59] + z[43];
    z[41]=z[41]*z[57];
    z[43]= - 15*z[44] - 100*z[7] + z[53];
    z[43]=z[3]*z[43];
    z[44]= - static_cast<T>(25)- z[49];
    z[22]=z[26] + z[41] + z[22] + z[37] + 3*z[44] + z[43];
    z[26]=6*z[12];
    z[22]=z[22]*z[26];
    z[37]= - z[48]*z[74];
    z[41]=z[90]*z[23];
    z[43]= - 5*z[21] - z[41];
    z[41]=z[36] + z[41];
    z[41]=z[5]*z[41];
    z[41]= - 15*z[14] - 16*z[7] + z[41];
    z[41]=2*z[41] + 15*z[100];
    z[41]=z[15]*z[41];
    z[44]=9*z[14];
    z[48]=5*z[7] + z[44];
    z[48]=z[14]*z[48];
    z[37]=z[37] + 6*z[86] + z[41] + 2*z[43] + z[48];
    z[37]=z[18]*z[37];
    z[41]= - 14*z[21] - z[27];
    z[41]=z[5]*z[41];
    z[41]= - 20*z[14] - 27*z[7] + z[41];
    z[43]=15*z[49] + static_cast<T>(15)+ 19*z[32];
    z[43]=z[15]*z[43];
    z[48]= - z[101] - z[14];
    z[48]=z[14]*z[48];
    z[48]= - z[58] + z[48];
    z[48]=z[48]*z[67];
    z[37]=z[37] + z[48] + 3*z[41] + z[43];
    z[37]=z[37]*z[68];
    z[41]=z[72] + z[29];
    z[43]=z[33]*z[39];
    z[41]=z[43] + 5*z[41] - 31*z[14];
    z[41]=z[41]*z[93];
    z[43]=223*z[7] + 259*z[62];
    z[43]=z[5]*z[43];
    z[48]=18*z[7] - z[91];
    z[48]=z[5]*z[48];
    z[48]= - static_cast<T>(169)+ z[48];
    z[48]=z[48]*z[49];
    z[43]=z[48] + static_cast<T>(36)+ z[43];
    z[43]=z[15]*z[43];
    z[48]=npow(z[7],4);
    z[52]=z[48]*z[42];
    z[53]=18*z[23];
    z[55]= - z[53] - z[52];
    z[55]=z[5]*z[55];
    z[55]= - 678*z[21] + z[55];
    z[55]=z[5]*z[55];
    z[43]=z[43] - 401*z[14] - 874*z[7] + z[55];
    z[43]=z[17]*z[43];
    z[55]=z[67]*z[21];
    z[40]=z[40]*z[63];
    z[56]=z[7] + 17*z[14];
    z[40]=z[40] + 2*z[56] - z[55];
    z[40]=z[40]*z[26];
    z[56]=49*z[7] + 12*z[29];
    z[56]=z[56]*z[90];
    z[56]=static_cast<T>(26)+ z[56];
    z[56]=z[56]*z[49];
    z[57]=785*z[7] + 36*z[29];
    z[57]=z[5]*z[57];
    z[37]=z[40] + z[43] + z[37] + z[41] + z[56] + static_cast<T>(1116)+ z[57];
    z[37]=z[10]*z[37];
    z[40]=z[42]*npow(z[7],5);
    z[40]=z[40] + 18*z[48];
    z[40]=z[40]*z[5];
    z[40]=z[40] + 678*z[23];
    z[40]=z[40]*z[5];
    z[25]=z[40] + 455*z[25] + 910*z[21];
    z[25]=z[25]*z[17];
    z[40]=z[27] + 8*z[21];
    z[36]=z[36]*z[33];
    z[41]= - z[61] + 20*z[7];
    z[41]=z[41]*z[14];
    z[36]= - z[41] + z[36] + 4*z[40];
    z[40]=9*z[3];
    z[36]=z[36]*z[40];
    z[41]= - z[46] + 18*z[21];
    z[41]=z[41]*z[5];
    z[41]=z[41] - z[51];
    z[41]=z[41]*z[5];
    z[41]=z[41] - 189;
    z[41]=z[41]*z[15];
    z[42]=z[53] - z[52];
    z[42]=z[42]*z[5];
    z[42]=z[42] + 135*z[21];
    z[42]=z[42]*z[5];
    z[43]=z[50]*z[3];
    z[30]=z[43] - z[30];
    z[30]= - z[8]*z[30];
    z[25]=189*z[30] + z[25] + z[41] - z[36] - z[42] + 378*z[14] - 396*
    z[7];
    z[30]=z[10] + z[16];
    z[25]=z[20]*z[30]*z[25];
    z[30]=z[47] + 15*z[29];
    z[30]=z[5]*z[30];
    z[35]=static_cast<T>(29)- z[35];
    z[35]=z[35]*z[49];
    z[36]=z[7] - z[44];
    z[36]=z[36]*z[88];
    z[41]=6*z[29] + 61*z[7];
    z[42]= - 18*z[14] + z[41];
    z[41]= - z[5]*z[41];
    z[41]=static_cast<T>(84)+ z[41];
    z[41]=z[15]*z[41];
    z[41]=3*z[42] + z[41];
    z[41]=z[18]*z[41];
    z[30]=z[41] + z[36] + z[35] + static_cast<T>(164)+ z[30];
    z[30]=z[30]*z[68];
    z[35]=z[55] - z[39] - z[65];
    z[35]=z[35]*z[98];
    z[36]=39*z[21] - z[46];
    z[36]=z[5]*z[36];
    z[36]= - 23*z[7] + z[36];
    z[36]=z[5]*z[36];
    z[39]= - 32*z[7] + z[91];
    z[39]=z[5]*z[39];
    z[39]=static_cast<T>(5)+ z[39];
    z[39]=z[39]*z[49];
    z[33]=z[7]*z[33];
    z[33]= - z[14] + z[33];
    z[33]=z[33]*z[88];
    z[33]=z[35] + z[33] + z[39] - static_cast<T>(58)+ z[36];
    z[33]=z[33]*z[96];
    z[27]= - z[58] + 37*z[27];
    z[27]=z[5]*z[27];
    z[27]=1340*z[7] + 21*z[27];
    z[27]=z[5]*z[27];
    z[35]=33*z[7] - z[91];
    z[35]=z[35]*z[90];
    z[35]= - static_cast<T>(352)+ z[35];
    z[35]=z[35]*z[49];
    z[36]=z[87]*z[38];
    z[36]=z[36] + 137*z[14] - 28*z[15];
    z[36]=z[36]*z[68];
    z[23]=9*z[23] - z[52];
    z[23]=z[23]*z[90];
    z[21]= - 1687*z[21] + z[23];
    z[21]=z[5]*z[21];
    z[21]=66*z[85] - 410*z[14] - 1733*z[7] + z[21];
    z[21]=z[16]*z[21];
    z[21]=z[21] + z[36] + z[35] + static_cast<T>(586)+ z[27];
    z[21]=z[17]*z[21];
    z[23]= - static_cast<T>(21)- 4*z[32];
    z[23]=3*z[23] - z[78];
    z[23]=z[23]*z[40];
    z[27]= - z[12]*z[97];
    z[27]= - static_cast<T>(39)+ z[27];
    z[26]=z[27]*z[26];
    z[23]=z[26] - 445*z[5] + z[23];
    z[23]=z[10]*z[23];
    z[26]= - static_cast<T>(29)+ z[83];
    z[26]=z[5]*z[26];
    z[26]=z[26] + z[67];
    z[26]=z[26]*z[67];
    z[26]=343*z[54] + z[26];
    z[27]=15*z[12] - z[71] - z[99];
    z[27]=z[12]*z[27];
    z[35]=z[10]*z[82];
    z[35]= - 25*z[79] + z[35];
    z[35]=z[35]*z[66];
    z[23]=z[35] + z[23] + 18*z[27] + 2*z[26] - 259*z[84];
    z[23]=z[1]*z[23];
    z[26]= - static_cast<T>(15)- 518*z[32];
    z[26]=z[26]*z[69];
    z[27]= - z[60]*z[88];
    z[29]= - z[7] - z[29];
    z[29]=z[5]*z[29];
    z[27]=z[27] + static_cast<T>(37)+ 6*z[29];
    z[27]=z[27]*z[67];
    z[29]= - static_cast<T>(7)- 554*z[32];
    z[29]=z[5]*z[29];

    r += z[21] + z[22] + z[23] + 18*z[24] + z[25] + z[26] + z[27] + 
      z[28] + z[29] + z[30] + 3*z[31] + z[33] + 6*z[34] + z[37] + z[45]
      ;
 
    return r;
}

template double qg_2lLC_r144(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r144(const std::array<dd_real,31>&);
#endif
