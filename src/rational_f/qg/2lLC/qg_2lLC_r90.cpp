#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r90(const std::array<T,31>& k) {
  T z[27];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[2];
    z[7]=k[4];
    z[8]=k[6];
    z[9]=k[10];
    z[10]=k[15];
    z[11]=k[3];
    z[12]=k[16];
    z[13]=z[9]*z[6];
    z[14]=z[7]*z[9];
    z[15]=2*z[14];
    z[16]=z[15] + 1;
    z[17]= - z[13] + z[16];
    z[17]=z[8]*z[7]*z[17];
    z[18]=z[12]*z[8];
    z[19]=npow(z[7],2);
    z[20]=z[18]*z[19];
    z[14]=z[14] + 1;
    z[21]= - z[14]*z[20];
    z[17]=z[21] + z[17] - z[16];
    z[17]=z[10]*z[17];
    z[20]=z[20] + 1;
    z[20]=z[9]*z[20];
    z[13]=z[13] - z[15];
    z[13]=z[8]*z[13];
    z[13]=z[17] + z[13] + z[20];
    z[15]=npow(z[10],2);
    z[13]=z[13]*z[15];
    z[17]=z[1] + 2*z[2];
    z[20]=z[4]*z[2];
    z[21]=z[17]*z[20];
    z[22]=npow(z[2],2);
    z[23]=npow(z[2],3);
    z[24]=z[4]*z[23];
    z[24]= - z[22] + z[24];
    z[24]=z[12]*z[24];
    z[21]=z[24] - z[2] + z[21];
    z[21]=z[3]*z[21];
    z[24]=z[22]*z[8];
    z[17]= - z[24] - z[17];
    z[17]=z[4]*z[17];
    z[25]=z[4]*z[8];
    z[23]=z[23]*z[25];
    z[23]= - z[24] + z[23];
    z[23]=z[12]*z[23];
    z[17]=z[21] + z[17] + z[23];
    z[17]=z[5]*z[17];
    z[21]=2*z[4];
    z[23]=z[1] + 3*z[2];
    z[23]=z[23]*z[21];
    z[22]=z[22]*z[21];
    z[22]= - z[2] + z[22];
    z[26]=2*z[12];
    z[22]=z[22]*z[26];
    z[22]=z[22] - static_cast<T>(1)+ z[23];
    z[22]=z[3]*z[22];
    z[23]=z[21]*z[24];
    z[24]=z[8]*z[2];
    z[23]= - z[24] + z[23];
    z[23]=z[23]*z[26];
    z[24]= - static_cast<T>(1)- z[24];
    z[24]=z[4]*z[24];
    z[17]=z[17] + z[22] + 3*z[24] + z[23];
    z[17]=z[5]*z[17];
    z[22]=z[21]*z[8];
    z[23]=z[2]*z[25];
    z[23]= - z[8] + 5*z[23];
    z[23]=z[12]*z[23];
    z[20]= - static_cast<T>(1)+ 5*z[20];
    z[20]=z[12]*z[20];
    z[20]=4*z[4] + z[20];
    z[20]=z[3]*z[20];
    z[17]=z[17] + z[20] - z[22] + z[23];
    z[17]=z[5]*z[17];
    z[20]= - z[9]*z[11];
    z[20]=z[20] + z[14];
    z[20]=z[7]*z[20];
    z[19]=z[19]*z[12];
    z[14]= - z[14]*z[19];
    z[14]=z[14] - z[11] + z[20];
    z[14]=z[10]*z[14];
    z[19]=z[9]*z[19];
    z[14]=z[14] + z[19] - z[16];
    z[14]=z[14]*z[15];
    z[15]=z[12]*z[21];
    z[14]=z[15] + z[14];
    z[14]=z[3]*z[14];
    z[15]=z[21]*z[18];

    r += z[13] + z[14] + z[15] + z[17];
 
    return r;
}

template double qg_2lLC_r90(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r90(const std::array<dd_real,31>&);
#endif
