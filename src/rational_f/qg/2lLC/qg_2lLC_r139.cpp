#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r139(const std::array<T,31>& k) {
  T z[111];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[22];
    z[7]=k[30];
    z[8]=k[3];
    z[9]=k[13];
    z[10]=k[4];
    z[11]=k[12];
    z[12]=k[5];
    z[13]=k[7];
    z[14]=k[6];
    z[15]=k[15];
    z[16]=k[14];
    z[17]=k[28];
    z[18]=k[16];
    z[19]=3*z[5];
    z[20]=npow(z[10],2);
    z[21]=z[20]*z[19];
    z[22]=2*z[10];
    z[21]= - z[22] + z[21];
    z[21]=z[5]*z[21];
    z[23]=13*z[12];
    z[24]=npow(z[12],2);
    z[25]=z[24]*z[16];
    z[26]= - z[23] + z[25];
    z[26]=z[16]*z[26];
    z[27]=z[22] + z[25];
    z[27]=z[15]*z[27];
    z[21]=z[27] + z[26] + static_cast<T>(8)+ z[21];
    z[21]=z[15]*z[21];
    z[26]=4*z[12];
    z[27]= - z[26] + z[25];
    z[27]=z[16]*z[27];
    z[28]=3*z[16];
    z[29]=z[24]*z[15];
    z[30]=z[29]*z[28];
    z[27]=z[27] + z[30];
    z[27]=z[15]*z[27];
    z[30]=2*z[16];
    z[31]=z[16]*z[12];
    z[32]= - static_cast<T>(1)+ z[31];
    z[32]=z[32]*z[30];
    z[33]=z[5]*z[10];
    z[34]=static_cast<T>(1)- z[33];
    z[34]=z[5]*z[34];
    z[27]=z[27] + z[34] + z[32];
    z[27]=z[15]*z[27];
    z[32]=npow(z[5],2);
    z[27]= - z[32] + z[27];
    z[27]=z[2]*z[27];
    z[34]=z[10] - z[8];
    z[34]=z[34]*z[19];
    z[34]= - static_cast<T>(2)+ z[34];
    z[34]=z[5]*z[34];
    z[35]= - static_cast<T>(2)+ z[31];
    z[30]=z[35]*z[30];
    z[21]=z[27] + z[21] + z[34] + z[30];
    z[21]=z[2]*z[21];
    z[27]=7*z[10];
    z[30]=z[20]*z[9];
    z[34]=z[27] + 25*z[30];
    z[34]=z[9]*z[34];
    z[35]=2*z[9];
    z[36]=z[9]*z[10];
    z[37]= - static_cast<T>(1)+ z[36];
    z[37]=z[37]*z[35];
    z[38]=npow(z[9],2);
    z[39]=z[38]*z[8];
    z[37]=z[37] + z[39];
    z[37]=z[8]*z[37];
    z[34]=25*z[37] + static_cast<T>(43)+ z[34];
    z[34]=z[8]*z[34];
    z[37]=17*z[10];
    z[40]= - z[37] + 19*z[30];
    z[41]=z[8]*z[10];
    z[42]=z[41] - z[20];
    z[43]=18*z[5];
    z[44]=z[42]*z[43];
    z[34]=z[44] + 3*z[40] + z[34];
    z[34]=z[34]*z[19];
    z[40]=npow(z[10],3);
    z[44]=z[40]*z[5];
    z[45]= - 17*z[20] - 6*z[44];
    z[45]=z[5]*z[45];
    z[46]=z[12] - 12*z[10];
    z[47]=z[10] + z[12];
    z[48]=z[47]*z[10];
    z[49]=4*z[48];
    z[50]= - z[15]*z[49];
    z[45]=z[50] - 4*z[25] + 2*z[46] + z[45];
    z[46]=9*z[15];
    z[45]=z[45]*z[46];
    z[50]=z[10]*z[12];
    z[51]=z[50] - z[24];
    z[52]=z[51]*z[36];
    z[53]=401*z[12] + 31*z[10];
    z[53]=z[10]*z[53];
    z[53]= - 464*z[52] + 34*z[24] + z[53];
    z[53]=z[9]*z[53];
    z[54]= - 72*z[12] + 49*z[10];
    z[53]=2*z[54] + z[53];
    z[53]=z[9]*z[53];
    z[54]=z[26] - z[10];
    z[54]=z[54]*z[10];
    z[54]=z[54] - z[24];
    z[55]=z[54]*z[9];
    z[56]=z[23] - 15*z[10];
    z[56]=39*z[56] - 232*z[55];
    z[56]=z[9]*z[56];
    z[56]=static_cast<T>(544)+ z[56];
    z[56]=z[9]*z[56];
    z[57]=z[10] - z[12];
    z[58]=z[57]*z[9];
    z[59]= - static_cast<T>(77)+ 58*z[58];
    z[59]=z[59]*z[38];
    z[60]=npow(z[9],3);
    z[61]=z[60]*z[8];
    z[59]=z[59] + 29*z[61];
    z[59]=z[8]*z[59];
    z[56]=z[56] + 8*z[59];
    z[56]=z[8]*z[56];
    z[59]=z[30]*z[24];
    z[62]=17*z[24];
    z[63]=z[62] - 53*z[50];
    z[63]=z[10]*z[63];
    z[63]=z[63] + 116*z[59];
    z[63]=z[9]*z[63];
    z[64]=31*z[12];
    z[65]= - z[64] + 53*z[10];
    z[65]=z[10]*z[65];
    z[65]=31*z[24] + z[65];
    z[63]=2*z[65] + z[63];
    z[63]=z[3]*z[63]*z[35];
    z[65]=5*z[12];
    z[66]= - z[65] + z[22];
    z[67]=18*z[16];
    z[66]=z[66]*z[67];
    z[68]= - z[3]*z[51];
    z[69]=2*z[12];
    z[68]= - z[69] + z[68];
    z[68]=z[11]*z[68];
    z[21]=124*z[68] + 18*z[21] + z[45] + z[63] + z[66] + z[34] + z[53]
    + z[56];
    z[21]=z[14]*z[21];
    z[34]=z[1]*z[3];
    z[45]=2*z[3];
    z[53]=z[5] + z[45];
    z[53]=z[53]*z[34];
    z[56]=z[3]*z[5];
    z[63]=npow(z[3],2);
    z[66]=z[63]*z[1];
    z[68]= - z[5]*z[66];
    z[68]=z[56] + z[68];
    z[68]=z[2]*z[68];
    z[53]=z[68] + z[3] + z[53];
    z[53]=z[1]*z[53];
    z[53]=static_cast<T>(3)+ z[53];
    z[53]=z[2]*z[53];
    z[68]=z[22] + z[12];
    z[70]=static_cast<T>(2)- z[33];
    z[70]=z[1]*z[70];
    z[70]=z[70] + z[68];
    z[70]=z[1]*z[70];
    z[71]=npow(z[1],2);
    z[72]=z[71]*z[11];
    z[73]=z[57]*z[72];
    z[70]=z[70] + z[73];
    z[70]=z[11]*z[70];
    z[73]=z[1]*z[12];
    z[74]=z[73] + z[24];
    z[75]= - z[1]*z[74];
    z[76]=z[24]*z[72];
    z[75]=z[75] + z[76];
    z[75]=z[11]*z[75];
    z[76]=z[3]*z[12];
    z[71]=z[71]*z[76];
    z[71]=z[71] + z[75];
    z[71]=z[13]*z[71];
    z[75]=static_cast<T>(1)- z[76];
    z[77]=z[76] + 2;
    z[78]=z[77]*z[3];
    z[79]= - z[5] - z[78];
    z[79]=z[1]*z[79];
    z[75]=2*z[75] + z[79];
    z[75]=z[1]*z[75];
    z[79]=3*z[12];
    z[53]=z[71] + z[70] + z[53] - z[79] + z[75];
    z[53]=z[6]*z[53];
    z[70]=2*z[5];
    z[71]= - z[70] - z[3];
    z[71]=z[71]*z[45];
    z[75]=z[19] + 4*z[3];
    z[66]=z[75]*z[66];
    z[66]=z[71] + z[66];
    z[66]=z[1]*z[66];
    z[71]=2*z[15];
    z[80]= - z[15]*z[10];
    z[80]= - static_cast<T>(1)+ z[80];
    z[80]=z[80]*z[71];
    z[81]=z[63]*z[19];
    z[82]= - z[1]*npow(z[3],3)*z[70];
    z[81]=z[81] + z[82];
    z[81]=z[1]*z[81];
    z[81]= - z[56] + z[81];
    z[81]=z[2]*z[81];
    z[66]=z[81] + z[80] + z[66];
    z[66]=z[2]*z[66];
    z[80]=9*z[10];
    z[81]=z[80] + z[12];
    z[82]=z[10]*z[19];
    z[82]= - static_cast<T>(7)+ z[82];
    z[82]=z[1]*z[82];
    z[82]=z[82] - z[81];
    z[82]=z[1]*z[82];
    z[83]=4*z[72];
    z[84]= - z[57]*z[83];
    z[82]=z[82] + z[84];
    z[82]=z[11]*z[82];
    z[84]=z[1]*z[5];
    z[85]=7*z[84] - static_cast<T>(5)+ 4*z[33];
    z[85]=z[1]*z[85];
    z[86]=11*z[10];
    z[82]=z[82] + z[85] - z[79] - z[86];
    z[82]=z[11]*z[82];
    z[85]=z[1]*z[79];
    z[85]=z[24] + z[85];
    z[85]=z[1]*z[85];
    z[87]= - z[24]*z[83];
    z[85]=z[85] + z[87];
    z[85]=z[11]*z[85];
    z[87]=2*z[1];
    z[88]=z[12] - z[1];
    z[88]=z[88]*z[87];
    z[85]=z[85] + z[24] + z[88];
    z[85]=z[11]*z[85];
    z[77]=z[77]*z[34];
    z[88]=2*z[76];
    z[77]= - z[88] + z[77];
    z[77]=z[1]*z[77];
    z[77]=z[77] + z[85];
    z[77]=z[13]*z[77];
    z[85]=z[69] + z[10];
    z[89]=z[15]*z[48];
    z[89]=z[89] - z[85];
    z[89]=z[89]*z[71];
    z[78]= - z[70] - z[78];
    z[90]=2*z[34];
    z[78]=z[78]*z[90];
    z[91]= - z[12]*z[63];
    z[78]=z[78] + 4*z[5] + z[91];
    z[78]=z[1]*z[78];
    z[53]=2*z[53] + z[77] + z[82] + z[66] + z[78] + z[89] - static_cast<T>(11)- z[76];
    z[53]=z[6]*z[53];
    z[66]=z[34] - 2;
    z[77]= - z[32]*z[66];
    z[77]=z[56] + z[77];
    z[77]=z[1]*z[77];
    z[78]=z[76]*z[15];
    z[82]=z[78] - z[3];
    z[89]=z[5]*z[8];
    z[91]=z[89] + 2;
    z[92]= - z[5]*z[91];
    z[77]=z[77] + z[92] + z[82];
    z[77]=z[2]*z[77];
    z[92]=static_cast<T>(1)+ z[34];
    z[84]=z[92]*z[84];
    z[92]= - z[79] + z[29];
    z[92]=z[15]*z[92];
    z[92]=z[92] + z[91];
    z[92]=z[2]*z[92];
    z[93]= - z[12] + z[29] - z[8];
    z[92]=z[92] - z[93];
    z[92]=z[14]*z[92];
    z[94]=static_cast<T>(1)- z[89];
    z[95]= - z[15]*z[79];
    z[77]=z[92] + z[77] + z[84] + 2*z[94] + z[95];
    z[77]=z[2]*z[77];
    z[84]= - z[8] + z[1];
    z[77]=3*z[84] + z[77];
    z[77]=z[7]*z[77];
    z[84]= - static_cast<T>(3)- 2*z[89];
    z[84]=z[84]*z[32];
    z[82]= - z[82]*z[71];
    z[92]=npow(z[5],3);
    z[66]= - z[92]*z[66];
    z[94]=z[3]*z[32];
    z[66]=z[94] + z[66];
    z[66]=z[66]*z[87];
    z[94]= - z[3]*z[70];
    z[66]=z[66] + z[82] + z[84] + z[94];
    z[66]=z[2]*z[66];
    z[82]=z[34] - 1;
    z[82]=z[32]*z[82];
    z[82]= - z[56] + z[82];
    z[82]=z[1]*z[82];
    z[82]=z[82] + z[5] + z[3];
    z[84]=static_cast<T>(1)- z[88];
    z[94]=7*z[12];
    z[95]=z[15]*z[94];
    z[84]=2*z[84] + z[95];
    z[84]=z[15]*z[84];
    z[66]=z[66] + z[84] + 4*z[82];
    z[66]=z[2]*z[66];
    z[82]=9*z[12];
    z[84]=z[82] - z[29];
    z[84]=z[15]*z[84];
    z[29]=z[26] - 3*z[29];
    z[29]=z[29]*npow(z[15],2);
    z[95]=z[89] + 1;
    z[96]=z[5]*z[95];
    z[29]=z[96] + z[29];
    z[29]=z[2]*z[29];
    z[29]=z[29] - 3*z[91] + z[84];
    z[29]=z[2]*z[29];
    z[29]=2*z[93] + z[29];
    z[29]=z[14]*z[29];
    z[84]=6*z[12];
    z[91]=z[84] - z[8];
    z[71]=z[91]*z[71];
    z[91]= - z[70]*z[34];
    z[91]= - z[5] + z[91];
    z[91]=z[1]*z[91];
    z[93]=z[8]*z[12];
    z[74]= - z[93] + z[74];
    z[96]=2*z[11];
    z[74]=z[74]*z[96];
    z[74]=z[74] - 5*z[1] + z[69] - z[8];
    z[74]=z[11]*z[74];
    z[29]=2*z[77] + z[29] + z[74] + z[66] + z[91] + z[71] - static_cast<T>(10)+ z[89];
    z[29]=z[7]*z[29];
    z[29]=z[53] + z[29];
    z[53]=z[85]*z[10];
    z[53]=z[53] + z[24];
    z[66]=z[68]*z[8];
    z[53]= - z[66] + 2*z[53];
    z[53]=z[53]*z[8];
    z[66]=z[20]*z[5];
    z[68]=npow(z[8],2);
    z[71]=z[66]*z[68];
    z[74]=2*z[24];
    z[77]=z[74] + z[50];
    z[77]=z[77]*z[10];
    z[85]=npow(z[12],3);
    z[77]=z[77] + z[85];
    z[53]=z[53] - z[71] - z[77];
    z[71]=z[53]*z[67];
    z[91]=8*z[10];
    z[97]= - z[82] + z[91];
    z[97]=z[10]*z[97];
    z[98]=3*z[8];
    z[99]= - z[98] + z[64] + 34*z[10];
    z[99]=z[8]*z[99];
    z[100]=3*z[89];
    z[42]= - z[42]*z[100];
    z[42]= - z[71] + z[42] + z[99] - z[62] + z[97];
    z[42]=z[16]*z[42];
    z[97]=11*z[41];
    z[99]=16*z[20] - z[97];
    z[99]=z[99]*z[19];
    z[42]=z[42] + z[99] - 24*z[8] + 41*z[12] + 91*z[10];
    z[99]=6*z[16];
    z[42]=z[42]*z[99];
    z[101]=20*z[12];
    z[102]=z[101] + z[27];
    z[102]=z[10]*z[102];
    z[102]=26*z[24] + z[102];
    z[64]=z[64] + z[86];
    z[64]=z[10]*z[64];
    z[64]=42*z[24] + z[64];
    z[64]=z[10]*z[64];
    z[64]=22*z[85] + z[64];
    z[64]=z[9]*z[64];
    z[64]=2*z[102] + z[64];
    z[64]=z[9]*z[64];
    z[102]=z[20]*z[8];
    z[102]=z[102] - z[40];
    z[102]=z[102]*z[5];
    z[37]=z[37] - z[8];
    z[37]=z[8]*z[37];
    z[37]=z[102] - 18*z[20] + z[37];
    z[37]=z[37]*z[70];
    z[103]=3*z[10];
    z[23]=z[23] - z[103];
    z[104]=z[40]*z[9];
    z[105]=z[104] + z[44];
    z[105]=z[3]*z[105];
    z[23]=11*z[105] + z[37] + 30*z[8] + 2*z[23] + z[64];
    z[23]=z[23]*z[46];
    z[37]=z[77]*z[16];
    z[64]=11*z[37];
    z[77]=z[65] - z[103];
    z[77]=z[10]*z[77];
    z[77]=8*z[24] + z[77];
    z[77]=2*z[77] - z[64];
    z[77]=z[77]*z[28];
    z[105]=z[66] + z[30];
    z[106]=9*z[3];
    z[107]=z[50]*z[106];
    z[77]=z[107] + z[77] - 263*z[12] + 257*z[10] + 9*z[105];
    z[77]=z[77]*z[45];
    z[105]=9*z[9];
    z[107]=5*z[10];
    z[108]= - z[12] + z[107];
    z[108]=z[108]*z[105];
    z[108]= - static_cast<T>(44)+ z[108];
    z[108]=z[108]*z[35];
    z[109]=3*z[3];
    z[110]= - static_cast<T>(71)- 6*z[76];
    z[110]=z[110]*z[109];
    z[73]=z[60]*z[73];
    z[73]=99*z[73] + z[108] + z[110];
    z[73]=z[73]*z[87];
    z[87]=z[24]*z[9];
    z[108]= - z[79] + 2*z[87];
    z[108]=z[1]*z[9]*z[108];
    z[108]=z[108] - z[69] - z[87];
    z[108]=z[1]*z[108];
    z[110]=z[93] - z[24];
    z[83]=z[87]*z[83];
    z[83]=z[83] + 2*z[110] + z[108];
    z[83]=z[11]*z[83];
    z[108]= - z[12]*z[105];
    z[108]= - 93*z[76] + static_cast<T>(142)+ z[108];
    z[82]= - z[82] - 71*z[87];
    z[82]=z[9]*z[82];
    z[82]= - static_cast<T>(53)+ z[82];
    z[82]=z[9]*z[82];
    z[82]=z[82] - 62*z[3];
    z[82]=z[1]*z[82];
    z[82]=2*z[108] + z[82];
    z[82]=z[1]*z[82];
    z[108]=73*z[12] - 31*z[8];
    z[110]=z[3]*z[24];
    z[82]=9*z[83] + z[82] + 4*z[108] - 257*z[110];
    z[82]=z[82]*z[96];
    z[83]=z[101] + z[86];
    z[83]=z[10]*z[83];
    z[83]=22*z[24] + z[83];
    z[83]=z[83]*z[105];
    z[101]=260*z[12] + 89*z[10];
    z[83]=2*z[101] + z[83];
    z[83]=z[9]*z[83];
    z[101]=z[10]*z[89];
    z[101]=z[101] + z[22] - z[8];
    z[101]=z[101]*z[43];
    z[23]=z[82] + z[73] + z[23] + z[77] + z[42] + z[101] + static_cast<T>(870)+ z[83];
    z[23]=z[13]*z[23];
    z[42]= - z[16]*z[53];
    z[53]= - z[24] + z[20];
    z[73]= - z[8] + z[26] + z[103];
    z[73]=z[8]*z[73];
    z[77]= - z[20] - z[41];
    z[77]=z[77]*z[89];
    z[42]=z[42] + z[77] + 2*z[53] + z[73];
    z[42]=z[16]*z[42];
    z[53]= - z[79] - z[22];
    z[53]=z[10]*z[53];
    z[53]=2*z[52] + z[24] + z[53];
    z[53]=z[9]*z[53];
    z[55]= - z[26] + z[55];
    z[55]=z[9]*z[55];
    z[73]=static_cast<T>(1)- z[58];
    z[73]=z[73]*z[35];
    z[73]=z[73] - z[39];
    z[73]=z[8]*z[73];
    z[55]=z[73] - static_cast<T>(6)+ z[55];
    z[55]=z[8]*z[55];
    z[73]=z[24] + z[50];
    z[73]=z[10]*z[73];
    z[73]=z[73] - z[59];
    z[73]=z[9]*z[73];
    z[37]= - z[37] + z[24] + z[73];
    z[37]=z[3]*z[37];
    z[73]=z[8]*z[9];
    z[77]=z[73] - static_cast<T>(1)+ 2*z[36];
    z[77]=z[8]*z[77];
    z[77]=z[30] + z[77];
    z[77]=z[77]*z[89];
    z[37]=z[37] + z[42] + z[77] + z[55] + z[84] + z[53];
    z[37]=z[17]*z[37];
    z[42]=z[79] + 16*z[10];
    z[42]=z[10]*z[42];
    z[42]= - 13*z[24] + z[42];
    z[53]=13*z[41];
    z[55]= - 21*z[20] - z[53];
    z[55]=z[55]*z[89];
    z[77]= - 13*z[8] + 50*z[12] + 29*z[10];
    z[77]=z[8]*z[77];
    z[42]= - z[71] + z[55] + 2*z[42] + z[77];
    z[42]=z[16]*z[42];
    z[53]=12*z[20] + z[53];
    z[53]=z[5]*z[53];
    z[55]=11*z[12];
    z[53]=z[53] - 10*z[8] + z[55] - z[103];
    z[42]=2*z[53] + z[42];
    z[42]=z[16]*z[42];
    z[50]=3*z[50];
    z[53]= - z[24] - z[50];
    z[27]=z[53]*z[27];
    z[27]=z[27] + 18*z[59];
    z[27]=z[9]*z[27];
    z[53]=6*z[10];
    z[71]=z[53] + z[65];
    z[71]=z[71]*z[10];
    z[27]=4*z[71] + z[27];
    z[27]=z[9]*z[27];
    z[71]=z[24] - z[71];
    z[64]=4*z[71] - z[64];
    z[64]=z[16]*z[64];
    z[27]=z[27] + z[64];
    z[27]=z[3]*z[27];
    z[64]=17*z[12] + 32*z[10];
    z[64]=z[10]*z[64];
    z[52]= - 36*z[52] - 7*z[24] + z[64];
    z[52]=z[9]*z[52];
    z[64]= - z[26] + z[103];
    z[52]=2*z[64] + z[52];
    z[52]=z[9]*z[52];
    z[54]= - z[54]*z[105];
    z[64]=19*z[12];
    z[54]=z[54] + z[64] + z[53];
    z[54]=z[9]*z[54];
    z[54]=static_cast<T>(8)+ z[54];
    z[54]=z[9]*z[54];
    z[71]=z[57]*z[105];
    z[71]= - static_cast<T>(5)+ z[71];
    z[77]=2*z[38];
    z[71]=z[71]*z[77];
    z[71]=z[71] + 9*z[61];
    z[71]=z[8]*z[71];
    z[54]=z[54] + z[71];
    z[71]=2*z[8];
    z[54]=z[54]*z[71];
    z[82]=z[47]*z[8];
    z[48]=z[82] + z[48];
    z[82]=z[11]*z[48];
    z[82]=z[82] - z[8];
    z[80]=z[65] + z[80] + 4*z[82];
    z[80]=z[80]*z[96];
    z[82]= - static_cast<T>(1)- 11*z[36];
    z[82]=z[82]*z[35];
    z[83]=11*z[39];
    z[82]=z[82] - z[83];
    z[82]=z[8]*z[82];
    z[101]=11*z[30];
    z[108]= - 26*z[10] - z[101];
    z[108]=z[9]*z[108];
    z[82]=z[108] + z[82];
    z[82]=z[8]*z[82];
    z[82]= - 24*z[30] + z[82];
    z[82]=z[5]*z[82];
    z[27]=8*z[37] + z[80] + z[27] + z[42] + z[82] + z[54] + static_cast<T>(4)+ z[52];
    z[27]=z[17]*z[27];
    z[37]=z[9]*z[12];
    z[42]=static_cast<T>(2)+ z[37];
    z[42]=z[9]*z[42];
    z[42]=z[42] + 3*z[39];
    z[52]=6*z[8];
    z[42]=z[42]*z[52];
    z[54]= - 21*z[9] + 5*z[39];
    z[54]=z[8]*z[54];
    z[54]=static_cast<T>(19)+ z[54];
    z[54]=z[54]*z[89];
    z[80]=z[94] - 5*z[8];
    z[80]=z[80]*z[28];
    z[25]=z[65] - 3*z[25];
    z[25]=z[25]*z[109];
    z[25]=z[25] + z[80] + z[54] + z[42] + static_cast<T>(14)+ 17*z[37];
    z[37]=z[63]*z[84];
    z[42]=14*z[9] + 15*z[39];
    z[54]= - 20*z[9] - z[39];
    z[54]=z[8]*z[54];
    z[54]=static_cast<T>(24)+ z[54];
    z[54]=z[5]*z[54];
    z[37]=z[37] + 2*z[42] + z[54];
    z[42]= - 25*z[9] + 29*z[39];
    z[42]=z[42]*z[19];
    z[54]=static_cast<T>(3)+ z[88];
    z[54]=z[54]*z[106];
    z[54]= - 65*z[5] + z[54];
    z[54]=z[3]*z[54];
    z[42]=z[42] + z[54];
    z[42]=z[1]*z[42];
    z[37]=3*z[37] + z[42];
    z[37]=z[1]*z[37];
    z[42]= - z[3]*z[75];
    z[42]= - 4*z[32] + z[42];
    z[34]=z[42]*z[34];
    z[42]= - 10*z[5] + z[109];
    z[42]=z[3]*z[42];
    z[34]=3*z[34] + 9*z[32] + z[42];
    z[42]=3*z[1];
    z[34]=z[34]*z[42];
    z[54]= - z[3]*z[19];
    z[54]= - 2*z[32] + z[54];
    z[54]=z[3]*z[54];
    z[63]=z[63]*z[5];
    z[63]=z[92] + z[63];
    z[63]=z[63]*z[90];
    z[54]=z[63] - 4*z[92] + z[54];
    z[54]=z[1]*z[54];
    z[63]=z[95]*z[32];
    z[65]= - 35*z[5] - 27*z[16];
    z[65]=z[3]*z[65];
    z[54]=9*z[54] + 18*z[63] + z[65];
    z[54]=z[2]*z[54];
    z[63]= - static_cast<T>(5)+ 6*z[31];
    z[63]=z[63]*z[109];
    z[65]= - static_cast<T>(8)+ z[100];
    z[65]=z[5]*z[65];
    z[63]=z[63] + z[65] - 21*z[16];
    z[65]=71*z[15];
    z[34]=z[54] + z[34] + 3*z[63] + z[65];
    z[34]=z[2]*z[34];
    z[54]=z[24] + z[68];
    z[54]=z[54]*z[65];
    z[63]=z[1] + z[8];
    z[65]=z[16]*z[68];
    z[54]=z[54] - 9*z[65] + 71*z[63];
    z[54]=z[11]*z[54];
    z[63]= - z[9]*z[79];
    z[63]= - static_cast<T>(11)+ z[63];
    z[63]=z[9]*z[63];
    z[63]=z[63] + z[83];
    z[63]=z[63]*z[98];
    z[65]=44*z[12] + 5*z[87];
    z[65]=z[9]*z[65];
    z[63]=z[63] + static_cast<T>(45)+ z[65];
    z[63]=z[63]*z[98];
    z[63]=z[63] + 80*z[12] - 9*z[87];
    z[63]=z[15]*z[63];
    z[25]=z[54] + z[34] + z[37] + 3*z[25] + z[63];
    z[25]=z[4]*z[25];
    z[34]= - static_cast<T>(157)- 9*z[33];
    z[34]=z[34]*z[70];
    z[37]=static_cast<T>(7)- 64*z[31];
    z[28]=z[37]*z[28];
    z[37]=z[12]*z[67];
    z[37]=z[37] - static_cast<T>(40)- 103*z[33];
    z[37]=z[37]*z[45];
    z[45]=z[67]*z[78];
    z[54]= - 71*z[5] - z[67];
    z[54]=z[3]*z[54];
    z[45]=z[54] + z[45];
    z[45]=z[2]*z[45];
    z[31]=z[15]*z[31];
    z[28]=z[45] - 63*z[31] + z[37] + z[34] + z[28];
    z[28]=z[15]*z[28];
    z[31]= - z[19] - 4*z[16];
    z[31]=z[16]*z[31];
    z[31]=z[32] + z[31];
    z[32]= - 215*z[5] + z[67];
    z[32]=z[3]*z[32];
    z[28]=9*z[31] + z[32] + z[28];
    z[28]=z[2]*z[28];
    z[25]=z[28] + z[25];
    z[28]= - z[22] - 99*z[30];
    z[31]= - 99*z[73] - static_cast<T>(67)- 198*z[36];
    z[31]=z[31]*z[71];
    z[32]=3*z[20] - z[41];
    z[34]=9*z[5];
    z[32]=z[32]*z[34];
    z[28]=z[32] + 3*z[28] + z[31];
    z[28]=z[28]*z[70];
    z[31]=38*z[12];
    z[32]= - z[31] - 31*z[87];
    z[32]=z[32]*z[105];
    z[37]=30*z[12];
    z[45]=z[38]*z[37];
    z[45]=z[45] + z[83];
    z[45]=z[45]*z[98];
    z[32]=z[45] - static_cast<T>(20)+ z[32];
    z[32]=z[8]*z[32];
    z[45]=11*z[9];
    z[54]=z[85]*z[45];
    z[54]=20*z[24] + z[54];
    z[63]=3*z[9];
    z[54]=z[54]*z[63];
    z[65]=10*z[12];
    z[68]= - z[65] - 11*z[87];
    z[68]=z[68]*z[63];
    z[71]=z[12]*z[83];
    z[68]=z[68] + z[71];
    z[68]=z[8]*z[68];
    z[54]=z[68] + z[64] + z[54];
    z[54]=z[8]*z[54];
    z[64]= - npow(z[12],4)*z[45];
    z[64]= - 30*z[85] + z[64];
    z[64]=z[9]*z[64];
    z[54]=z[54] - 19*z[24] + z[64];
    z[54]=z[54]*z[99];
    z[64]=z[9]*z[85];
    z[64]=57*z[24] + 26*z[64];
    z[64]=z[64]*z[63];
    z[68]=74*z[12];
    z[64]=z[68] + z[64];
    z[32]=z[54] + 2*z[64] + z[32];
    z[32]=z[32]*z[99];
    z[54]=z[12] - 44*z[10];
    z[54]=z[54]*z[103];
    z[54]= - 269*z[24] + z[54];
    z[54]=z[9]*z[54];
    z[54]=z[54] - 637*z[12] + 57*z[10];
    z[54]=z[9]*z[54];
    z[64]=51*z[12] - 22*z[10];
    z[64]=z[9]*z[64];
    z[64]=static_cast<T>(32)+ z[64];
    z[64]=z[9]*z[64];
    z[64]=z[64] + 44*z[39];
    z[64]=z[64]*z[98];
    z[54]=z[64] - static_cast<T>(112)+ z[54];
    z[64]= - z[55] - z[107];
    z[64]=z[64]*z[103];
    z[69]=z[69] - z[86];
    z[69]=z[10]*z[69];
    z[71]=11*z[24];
    z[69]= - z[71] + z[69];
    z[69]=z[69]*z[36];
    z[62]=z[69] + z[62] + z[64];
    z[62]=z[62]*z[105];
    z[64]= - 295*z[20] - 99*z[104];
    z[64]=z[64]*z[70];
    z[69]=71*z[12];
    z[73]= - z[69] - 394*z[10];
    z[62]=z[64] + 2*z[73] + z[62];
    z[62]=z[3]*z[62];
    z[28]=z[62] + z[32] + 3*z[54] + z[28];
    z[28]=z[15]*z[28];
    z[32]=static_cast<T>(27)- 71*z[58];
    z[32]=z[32]*z[77];
    z[54]= - static_cast<T>(55)- 19*z[36];
    z[54]=z[9]*z[54]*z[19];
    z[32]= - 121*z[56] + z[32] + z[54];
    z[32]=z[1]*z[32];
    z[54]=static_cast<T>(55)+ 21*z[36];
    z[54]=z[54]*z[63];
    z[62]=99*z[36];
    z[64]= - static_cast<T>(142)+ z[62];
    z[64]=z[5]*z[64];
    z[33]=static_cast<T>(13)- 313*z[33];
    z[33]=z[3]*z[33];
    z[32]=z[32] + z[33] + z[54] + z[64];
    z[32]=z[1]*z[32];
    z[33]=z[57]*z[35];
    z[33]=static_cast<T>(7)+ z[33];
    z[33]=z[9]*z[33];
    z[54]= - z[36]*z[19];
    z[33]=z[33] + z[54];
    z[33]=z[1]*z[33];
    z[54]=z[9]*z[81];
    z[33]=z[54] + z[33];
    z[33]=z[33]*z[42];
    z[42]=z[15]*z[48];
    z[48]=z[72]*z[58];
    z[33]=12*z[48] - 8*z[42] + z[33];
    z[33]=z[11]*z[33];
    z[42]=1027*z[10];
    z[48]= - 885*z[12] - z[42];
    z[48]=z[10]*z[48];
    z[44]= - 437*z[44] - 295*z[24] + z[48];
    z[44]=z[3]*z[44];
    z[48]= - z[103] - z[8];
    z[48]=z[8]*z[48];
    z[20]= - 721*z[20] + 142*z[48];
    z[20]=z[5]*z[20];
    z[20]=z[44] + z[20] + 154*z[8] - z[55] - 626*z[10];
    z[20]=z[15]*z[20];
    z[42]= - 437*z[66] - 37*z[12] - z[42];
    z[42]=z[3]*z[42];
    z[44]=60*z[12] + 149*z[10];
    z[44]=z[9]*z[44];
    z[44]= - static_cast<T>(136)+ z[44];
    z[48]= - z[22] - z[8];
    z[48]=z[5]*z[48];
    z[20]=6*z[33] + z[32] + z[20] + z[42] + 3*z[44] + 142*z[48];
    z[20]=z[11]*z[20];
    z[32]=z[65] + z[10];
    z[32]=z[10]*z[32];
    z[32]= - 10*z[24] + z[32];
    z[33]= - z[10]*z[51];
    z[33]= - z[85] + z[33];
    z[33]=z[33]*z[45];
    z[32]=3*z[32] + z[33];
    z[32]=z[32]*z[35];
    z[32]=z[32] - 55*z[12] + z[53];
    z[33]= - z[79] + z[22];
    z[33]=z[10]*z[33];
    z[33]=3*z[24] + z[33];
    z[33]=z[33]*z[45];
    z[37]=z[37] - 19*z[10];
    z[33]=2*z[37] + z[33];
    z[37]=6*z[9];
    z[33]=z[33]*z[37];
    z[42]= - static_cast<T>(10)+ 11*z[58];
    z[42]=z[42]*z[63];
    z[42]=z[42] + z[83];
    z[42]=z[42]*z[52];
    z[33]=z[42] + static_cast<T>(113)+ z[33];
    z[33]=z[8]*z[33];
    z[42]=z[91] - z[101];
    z[42]=z[9]*z[42];
    z[44]= - z[38]*z[97];
    z[42]=z[42] + z[44];
    z[42]=z[8]*z[42];
    z[42]=z[42] - z[10] - 3*z[30];
    z[42]=z[42]*z[89];
    z[32]=6*z[42] + 3*z[32] + z[33];
    z[32]=z[16]*z[32];
    z[33]= - z[68] + z[107];
    z[33]=z[10]*z[33];
    z[42]=z[30]*z[12];
    z[33]=44*z[42] + 30*z[24] + z[33];
    z[33]=z[9]*z[33];
    z[33]=z[33] + 76*z[12] - 35*z[10];
    z[33]=z[33]*z[63];
    z[22]= - z[22] - z[30];
    z[22]=z[9]*z[22];
    z[44]= - static_cast<T>(49)+ 25*z[36];
    z[44]=z[9]*z[44];
    z[44]=z[44] + 33*z[39];
    z[44]=z[44]*z[98];
    z[22]=z[44] + static_cast<T>(91)+ 24*z[22];
    z[22]=z[8]*z[22];
    z[44]=z[103] - 8*z[30];
    z[22]=3*z[44] + z[22];
    z[22]=z[5]*z[22];
    z[44]= - 22*z[30] - 27*z[12] + z[107];
    z[44]=z[9]*z[44];
    z[44]= - static_cast<T>(16)+ z[44];
    z[44]=z[9]*z[44];
    z[44]=z[44] - 36*z[39];
    z[44]=z[44]*z[98];
    z[22]=z[32] + z[22] + z[44] + static_cast<T>(52)+ z[33];
    z[22]=z[22]*z[99];
    z[32]= - static_cast<T>(151)+ 396*z[36];
    z[32]=z[32]*z[38];
    z[33]=z[5] + z[9] - 2*z[39];
    z[33]=z[33]*z[43];
    z[43]= - static_cast<T>(227)+ 117*z[36];
    z[43]=z[9]*z[43];
    z[44]= - static_cast<T>(502)- z[62];
    z[44]=z[5]*z[44];
    z[43]=18*z[3] + z[43] + z[44];
    z[43]=z[3]*z[43];
    z[19]=z[38]*z[19];
    z[19]= - 17*z[60] + z[19];
    z[44]=z[9]*z[56];
    z[19]=20*z[19] - 117*z[44];
    z[19]=z[1]*z[19];
    z[44]=624*z[61];
    z[19]=z[19] + z[43] + z[33] + z[32] + z[44];
    z[19]=z[1]*z[19];
    z[32]=44*z[24] + z[50];
    z[32]=z[10]*z[32];
    z[32]=z[32] - 22*z[59];
    z[32]=z[9]*z[32];
    z[33]= - z[47]*z[103];
    z[33]= - z[71] + z[33];
    z[32]=2*z[33] + z[32];
    z[32]=z[9]*z[32];
    z[26]=z[32] - z[26] - z[10];
    z[26]=z[26]*z[67];
    z[32]= - 218*z[12] - 99*z[10];
    z[32]=z[10]*z[32];
    z[32]=z[32] + 198*z[42];
    z[32]=z[9]*z[32];
    z[32]=z[32] + 109*z[12] - 525*z[10];
    z[32]=z[9]*z[32];
    z[33]= - 590*z[10] - 117*z[30];
    z[33]=z[5]*z[33];
    z[26]= - 18*z[76] + z[26] + z[33] - static_cast<T>(652)+ z[32];
    z[26]=z[3]*z[26];
    z[32]= - 106*z[10] + 133*z[12];
    z[32]=z[32]*z[10];
    z[24]=z[32] - 133*z[24];
    z[24]=z[24]*z[3];
    z[32]=z[102] + z[41];
    z[32]=z[32]*z[34];
    z[24]=z[24] - 62*z[8] - z[32] - 115*z[10] + 124*z[12];
    z[32]=z[93] - z[74];
    z[33]=z[85]*z[3];
    z[32]=133*z[33] + 62*z[32];
    z[32]=z[32]*z[96];
    z[33]=z[70]*npow(z[10],4);
    z[33]=z[33] + 19*z[40];
    z[33]=z[33]*z[5];
    z[33]=z[33] + z[49];
    z[33]=z[33]*z[46];
    z[24]= - z[32] + z[33] + 2*z[24];
    z[32]=z[13] + z[14];
    z[24]=z[18]*z[24]*z[32];
    z[30]= - z[91] + z[30];
    z[30]=z[9]*z[30];
    z[32]= - static_cast<T>(157)+ 69*z[36];
    z[32]=z[32]*z[35];
    z[32]=z[32] + 121*z[39];
    z[32]=z[32]*z[98];
    z[30]= - 36*z[89] + z[32] + static_cast<T>(131)+ 81*z[30];
    z[30]=z[5]*z[30];
    z[32]=z[69] - 33*z[10];
    z[32]=z[32]*z[36];
    z[31]=6*z[32] - z[31] - 313*z[10];
    z[31]=z[9]*z[31];
    z[31]=static_cast<T>(345)+ z[31];
    z[31]=z[9]*z[31];
    z[32]=104*z[12] - 71*z[10];
    z[32]=z[32]*z[37];
    z[32]=static_cast<T>(305)+ z[32];
    z[32]=z[32]*z[38];
    z[32]=z[32] - z[44];
    z[32]=z[8]*z[32];

    r += z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + 2*z[25] + 
      z[26] + 6*z[27] + z[28] + 18*z[29] + z[30] + z[31] + z[32];
 
    return r;
}

template double qg_2lLC_r139(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r139(const std::array<dd_real,31>&);
#endif
