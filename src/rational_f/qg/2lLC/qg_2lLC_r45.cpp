#include "qg_2lLC_rf_decl.hpp"

template<class T>
T qg_2lLC_r45(const std::array<T,31>& k) {
  T z[49];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[8];
    z[8]=k[3];
    z[9]=k[2];
    z[10]=k[6];
    z[11]=k[14];
    z[12]=k[15];
    z[13]=k[10];
    z[14]=z[10]*z[7];
    z[15]=z[14]*z[2];
    z[16]=2*z[10];
    z[17]= - z[16] - z[15];
    z[17]=z[2]*z[17];
    z[18]=z[8]*z[10];
    z[19]=npow(z[1],2);
    z[20]=z[19]*z[3];
    z[21]=4*z[20];
    z[22]=z[6]*z[21];
    z[17]=z[17] + z[22] - z[18];
    z[17]=z[5]*z[17];
    z[22]=npow(z[8],2);
    z[23]=z[22]*z[10];
    z[24]= - 2*z[20] + z[23];
    z[25]=z[16] + 15*z[3];
    z[25]=z[2]*z[25];
    z[25]=z[25] + static_cast<T>(3)+ 4*z[18];
    z[25]=z[2]*z[25];
    z[17]=z[17] + 2*z[24] + z[25];
    z[17]=z[5]*z[17];
    z[24]=npow(z[2],2);
    z[25]=z[2]*z[3];
    z[26]= - static_cast<T>(6)- 7*z[25];
    z[26]=z[26]*z[24];
    z[27]=z[24]*z[5];
    z[28]=7*z[3];
    z[29]= - z[7] - z[28];
    z[29]=z[29]*z[27];
    z[26]=2*z[26] + z[29];
    z[26]=z[5]*z[26];
    z[29]=npow(z[2],3);
    z[30]=3*z[3];
    z[31]=z[29]*z[30];
    z[32]=z[3] + z[7];
    z[27]=z[32]*z[27];
    z[27]=z[31] + z[27];
    z[27]=z[5]*z[27];
    z[31]=z[25] + 1;
    z[33]=z[31]*z[29];
    z[27]=3*z[33] + z[27];
    z[27]=z[5]*z[27];
    z[31]=z[31]*z[2];
    z[33]= - z[8] + z[31];
    z[29]=z[33]*z[29];
    z[27]=z[29] + z[27];
    z[29]=2*z[11];
    z[27]=z[27]*z[29];
    z[31]=6*z[8] - 7*z[31];
    z[24]=z[31]*z[24];
    z[24]=z[27] + z[24] + z[26];
    z[26]=3*z[11];
    z[24]=z[24]*z[26];
    z[27]=z[30]*z[1];
    z[31]=static_cast<T>(5)- z[27];
    z[31]=15*z[25] + 3*z[31] - z[18];
    z[31]=z[2]*z[31];
    z[33]= - z[8]*z[16];
    z[33]= - static_cast<T>(3)+ z[33];
    z[33]=z[8]*z[33];
    z[21]=z[31] + z[33] - 9*z[1] + z[21];
    z[21]=z[2]*z[21];
    z[23]= - 3*z[1] - z[23];
    z[23]=z[8]*z[23];
    z[17]=z[24] + z[17] + z[21] + 4*z[19] + z[23];
    z[17]=z[4]*z[17];
    z[21]= - z[19]*z[30];
    z[17]=z[17] - 5*z[1] + z[21];
    z[21]=9*z[12];
    z[23]=z[3]*z[1];
    z[24]=12*z[23];
    z[31]= - z[6]*z[24];
    z[31]=z[15] + z[31] - z[21] + z[10];
    z[31]=z[5]*z[31];
    z[20]=z[20] - z[1];
    z[33]=6*z[6];
    z[34]=z[20]*z[33];
    z[35]=2*z[7];
    z[36]= - z[35] - z[10];
    z[36]= - z[15] + 7*z[36] - 39*z[3];
    z[36]=z[2]*z[36];
    z[18]=static_cast<T>(5)+ 6*z[18];
    z[24]=z[31] + z[36] + z[34] + z[24] - z[18];
    z[24]=z[5]*z[24];
    z[31]=3*z[8];
    z[34]=z[8]*z[12];
    z[36]= - static_cast<T>(1)+ z[34];
    z[36]=z[36]*z[31];
    z[37]=z[5]*z[12];
    z[38]=z[37] + 1;
    z[39]= - 3*z[34] + z[38];
    z[39]=z[5]*z[39];
    z[36]=z[39] + z[36] - z[2];
    z[36]=z[5]*z[36];
    z[39]=static_cast<T>(3)- z[34];
    z[39]=z[39]*z[22];
    z[40]=z[31] + z[2];
    z[40]=z[2]*z[40];
    z[36]=z[36] + z[39] + z[40];
    z[36]=z[5]*z[36];
    z[39]=z[2]*z[8];
    z[40]= - z[22] - z[39];
    z[41]=3*z[2];
    z[40]=z[40]*z[41];
    z[42]=npow(z[8],3);
    z[36]=z[36] - z[42] + z[40];
    z[36]=z[5]*z[36];
    z[40]=z[8]*z[13];
    z[43]=static_cast<T>(3)- z[40];
    z[43]=z[2]*z[43]*z[22];
    z[43]=z[42] + z[43];
    z[43]=z[2]*z[43];
    z[36]=z[43] + z[36];
    z[36]=z[11]*z[36];
    z[36]=z[42] - z[36];
    z[42]= - static_cast<T>(13)+ 8*z[40];
    z[42]=z[42]*z[22];
    z[43]=3*z[40];
    z[44]=static_cast<T>(10)- z[43];
    z[44]=z[8]*z[44];
    z[45]= - static_cast<T>(5)- 9*z[25];
    z[45]=z[2]*z[45];
    z[44]=z[44] + z[45];
    z[44]=z[2]*z[44];
    z[42]=z[42] + z[44];
    z[42]=z[2]*z[42];
    z[44]=z[2]*z[32];
    z[44]= - 11*z[37] - 8*z[44] - static_cast<T>(7)+ 22*z[34];
    z[44]=z[5]*z[44];
    z[45]=11*z[34];
    z[46]=static_cast<T>(10)- z[45];
    z[46]=z[8]*z[46];
    z[47]=3*z[7];
    z[48]=z[47] - 25*z[3];
    z[48]=z[2]*z[48];
    z[48]=static_cast<T>(3)+ z[48];
    z[48]=z[2]*z[48];
    z[44]=z[44] + z[46] + z[48];
    z[44]=z[5]*z[44];
    z[46]=2*z[8];
    z[48]= - static_cast<T>(23)- 26*z[25];
    z[48]=z[2]*z[48];
    z[48]=z[46] + z[48];
    z[48]=z[2]*z[48];
    z[44]=z[44] + z[22] + z[48];
    z[44]=z[5]*z[44];
    z[36]=z[44] + z[42] - 4*z[36];
    z[36]=z[11]*z[36];
    z[42]= - static_cast<T>(7)+ z[43];
    z[42]=z[42]*z[46];
    z[44]=static_cast<T>(3)+ 8*z[25];
    z[44]=z[44]*z[41];
    z[42]=z[42] + z[44];
    z[42]=z[2]*z[42];
    z[37]=z[37] - z[34];
    z[44]=z[47] + z[28];
    z[41]=z[44]*z[41];
    z[37]=z[41] + static_cast<T>(3)+ 10*z[37];
    z[37]=z[5]*z[37];
    z[41]=z[7] + 45*z[3];
    z[41]=z[2]*z[41];
    z[41]=static_cast<T>(20)+ z[41];
    z[41]=z[2]*z[41];
    z[37]=z[41] + z[37];
    z[37]=z[5]*z[37];
    z[41]=static_cast<T>(1)- 4*z[40];
    z[41]=z[41]*z[22];
    z[36]=z[36] + z[37] + z[41] + z[42];
    z[36]=z[36]*z[26];
    z[23]=6*z[23];
    z[37]=z[10] - 27*z[3];
    z[37]=z[2]*z[37];
    z[18]=z[37] + z[23] + z[18];
    z[18]=z[2]*z[18];
    z[37]=5*z[10];
    z[41]= - 9*z[13] + z[37];
    z[41]=z[8]*z[41];
    z[41]=static_cast<T>(11)+ z[41];
    z[41]=z[8]*z[41];
    z[17]=z[36] + z[24] + z[18] + z[41] + 2*z[17];
    z[17]=z[4]*z[17];
    z[18]= - static_cast<T>(1)- z[43];
    z[18]=z[18]*z[31];
    z[24]=z[40] - 2;
    z[36]=6*z[25] - z[24];
    z[36]=z[2]*z[36];
    z[18]=z[18] + z[36];
    z[18]=z[2]*z[18];
    z[36]= - z[47] + z[28];
    z[41]=2*z[2];
    z[36]=z[36]*z[41];
    z[42]= - static_cast<T>(6)+ 13*z[34];
    z[32]= - 8*z[12] + z[32];
    z[32]=z[5]*z[32];
    z[32]=4*z[32] + 3*z[42] + z[36];
    z[32]=z[5]*z[32];
    z[36]=static_cast<T>(11)- 7*z[34];
    z[36]=z[8]*z[36];
    z[42]=static_cast<T>(5)+ 4*z[25];
    z[42]=z[2]*z[42];
    z[32]=z[32] + z[36] + 4*z[42];
    z[32]=z[5]*z[32];
    z[36]= - 2*z[34] + z[38];
    z[36]=z[5]*z[36];
    z[34]= - static_cast<T>(2)+ z[34];
    z[34]=z[8]*z[34];
    z[34]=z[36] + z[34] - z[2];
    z[34]=z[5]*z[34];
    z[22]=7*z[22];
    z[36]=14*z[8] + z[2];
    z[36]=z[2]*z[36];
    z[34]=7*z[34] + z[22] + z[36];
    z[34]=z[5]*z[34];
    z[24]=z[24]*z[39];
    z[24]= - z[22] + z[24];
    z[24]=z[2]*z[24];
    z[24]=z[24] + z[34];
    z[24]=z[24]*z[29];
    z[18]=z[24] + z[32] + z[22] + z[18];
    z[18]=z[11]*z[18];
    z[24]=z[47] - 17*z[3];
    z[24]=z[2]*z[24];
    z[28]= - z[28] - 7*z[7] + 22*z[12];
    z[28]=z[5]*z[28];
    z[24]=z[28] + z[24] + static_cast<T>(4)- z[45];
    z[24]=z[5]*z[24];
    z[25]= - z[43] - 10*z[25];
    z[25]=z[2]*z[25];
    z[22]=z[13]*z[22];
    z[18]=z[18] + z[24] + z[22] + z[25];
    z[18]=z[18]*z[26];
    z[22]=z[16]*z[7];
    z[24]= - z[2]*z[22];
    z[24]=z[24] - z[6] + z[47] - z[37];
    z[24]=z[24]*z[41];
    z[25]=2*z[6];
    z[26]=static_cast<T>(1)- z[27];
    z[26]=z[26]*z[25];
    z[26]=z[26] + z[30] + 3*z[10] + 5*z[7] - 6*z[12];
    z[27]=z[6]*z[30];
    z[27]= - z[14] + z[27];
    z[28]=2*z[5];
    z[27]=z[27]*z[28];
    z[15]=z[27] + 2*z[26] + 5*z[15];
    z[15]=z[5]*z[15];
    z[20]=z[20]*z[25];
    z[26]=3*z[13];
    z[27]=z[26] - z[16];
    z[27]=z[8]*z[27];
    z[32]=z[7]*z[1];
    z[15]=z[17] + z[18] + z[15] + z[24] + 4*z[27] + z[20] + z[23] + static_cast<T>(3)- 
   8*z[32];
    z[15]=z[4]*z[15];
    z[17]=npow(z[9],2);
    z[18]=z[10]*z[12];
    z[20]=z[17]*z[18];
    z[23]=15*z[12];
    z[24]= - z[23] - z[25];
    z[24]=z[8]*z[24];
    z[27]=3*z[12];
    z[32]=z[27]*z[9];
    z[34]=static_cast<T>(5)+ z[32];
    z[36]=z[2]*z[25];
    z[23]=z[23] + z[6];
    z[23]=z[5]*z[23];
    z[20]=z[23] + z[36] + z[24] + 3*z[34] - 4*z[20];
    z[20]=z[5]*z[20];
    z[23]=z[2]*z[6];
    z[24]=z[8]*z[6];
    z[34]= - static_cast<T>(3)- z[24];
    z[34]=4*z[34] + z[23];
    z[34]=z[2]*z[34];
    z[36]= - static_cast<T>(15)+ z[24];
    z[36]=z[8]*z[36];
    z[20]=z[20] + z[34] + 12*z[9] + z[36];
    z[20]=z[5]*z[20];
    z[34]=static_cast<T>(6)+ z[24];
    z[34]=z[34]*z[46];
    z[36]=z[40]*z[6];
    z[37]= - z[25] + z[36];
    z[37]=z[37]*z[39];
    z[34]=z[34] + z[37];
    z[34]=z[2]*z[34];
    z[20]=z[34] + z[20];
    z[20]=z[20]*z[29];
    z[21]= - z[9]*z[21];
    z[29]=z[17]*z[12];
    z[34]=3*z[9] + 5*z[29];
    z[34]=z[10]*z[34];
    z[21]=z[34] - static_cast<T>(22)+ z[21];
    z[34]=7*z[12];
    z[37]=z[34] + z[25];
    z[31]=z[37]*z[31];
    z[37]=4*z[9];
    z[38]=z[18]*z[37];
    z[34]=z[7] - z[34];
    z[34]= - z[3] + 3*z[34] + z[38];
    z[38]=5*z[6];
    z[34]=3*z[34] - z[38];
    z[34]=z[5]*z[34];
    z[39]= - z[3] - z[6];
    z[39]=z[2]*z[39];
    z[21]=z[34] + 6*z[39] + 2*z[21] + z[31];
    z[21]=z[5]*z[21];
    z[31]=2*z[13] + z[6];
    z[31]=7*z[31] - z[36];
    z[31]=z[8]*z[31];
    z[30]=z[36] - z[30] - z[6];
    z[30]=z[2]*z[30];
    z[30]=z[30] - static_cast<T>(2)+ z[31];
    z[30]=z[2]*z[30];
    z[31]= - static_cast<T>(4)- z[24];
    z[31]=z[8]*z[31];
    z[20]=z[20] + z[21] + z[30] + z[37] + z[31];
    z[20]=z[11]*z[20];
    z[21]= - z[9] - z[29];
    z[21]=z[21]*z[16];
    z[29]=z[13]*z[9];
    z[30]= - static_cast<T>(1)+ 2*z[29];
    z[21]=z[21] + 2*z[30] + z[32];
    z[31]=z[40]*z[25];
    z[31]=z[31] - z[33] - 10*z[13] - z[27];
    z[31]=z[8]*z[31];
    z[26]=z[38] - z[26] + z[10];
    z[23]=z[13]*z[23];
    z[26]=4*z[23] + 2*z[26] - 5*z[36];
    z[26]=z[2]*z[26];
    z[33]=z[5]*z[18];
    z[33]= - z[33] - z[35] + 5*z[12];
    z[32]= - static_cast<T>(1)- z[32];
    z[32]=z[32]*z[16];
    z[32]=z[32] + 4*z[6] + 3*z[33];
    z[32]=z[32]*z[28];
    z[20]=z[20] + z[32] + z[26] + 2*z[21] + z[31];
    z[20]=z[11]*z[20];
    z[21]=z[12]*z[13];
    z[26]= - z[16]*z[21];
    z[31]=z[7] + z[12];
    z[31]=z[10]*z[31];
    z[31]= - z[21] + z[31];
    z[31]=z[6]*z[31];
    z[23]=z[18]*z[23];
    z[23]=z[23] + z[26] + z[31];
    z[23]=z[23]*z[41];
    z[26]= - static_cast<T>(5)+ 3*z[29];
    z[26]=z[26]*z[18];
    z[29]=z[35] - z[10];
    z[31]= - z[12] - z[29];
    z[31]=z[31]*z[25];
    z[32]=z[3]*z[7];
    z[33]=z[36]*z[12];
    z[27]=z[13]*z[27];
    z[23]=z[23] + z[33] + z[31] + z[32] + z[27] + z[26];
    z[23]=z[23]*z[41];
    z[24]=z[10]*z[24];
    z[24]=z[24] - z[47] + z[16];
    z[24]=z[3]*z[24];
    z[26]=z[3]*z[29];
    z[26]=z[14] + z[26];
    z[25]=z[26]*z[25];
    z[14]=z[14]*z[3];
    z[22]=z[5]*z[6]*z[3]*z[22];
    z[22]=z[22] - z[14] + z[25];
    z[22]=z[5]*z[22];
    z[25]=z[1]*z[47];
    z[25]= - static_cast<T>(2)+ z[25];
    z[25]=z[3]*z[25];
    z[16]=z[25] - z[16] + 4*z[7] - z[12];
    z[16]=z[6]*z[16];
    z[25]= - z[35] + z[12];
    z[25]=z[6]*z[10]*z[25];
    z[14]=z[14] + z[25];
    z[14]=z[2]*z[14];
    z[14]=z[22] + z[14] + z[16] + z[18] + z[24];
    z[14]=z[14]*z[28];
    z[16]=2*z[3];
    z[18]=z[10] + z[16];
    z[18]=z[6]*z[18];
    z[18]= - z[33] - z[21] + z[18];
    z[18]=z[18]*z[46];
    z[17]= - z[13]*z[17];
    z[17]=z[37] + z[17];
    z[17]=z[12]*z[17];
    z[17]=static_cast<T>(3)+ 2*z[17];
    z[17]=z[10]*z[17];
    z[21]=z[35]*z[1];
    z[21]=z[21] - 3;
    z[22]= - z[21]*z[16];
    z[19]=z[7]*z[19];
    z[19]= - 4*z[1] + z[19];
    z[16]=z[19]*z[16];
    z[16]=3*z[21] + z[16];
    z[16]=z[6]*z[16];
    z[19]= - z[13] + z[7];
    z[21]= - z[12]*z[30];

    r += z[14] + z[15] + z[16] + z[17] + z[18] + 6*z[19] + z[20] + 
      z[21] + z[22] + z[23];
 
    return r;
}

template double qg_2lLC_r45(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_2lLC_r45(const std::array<dd_real,31>&);
#endif
