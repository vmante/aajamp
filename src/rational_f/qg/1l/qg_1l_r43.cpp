#include "qg_1l_rf_decl.hpp"

template<class T>
T qg_1l_r43(const std::array<T,31>& k) {
  T z[68];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[30];
    z[7]=k[5];
    z[8]=k[7];
    z[9]=k[12];
    z[10]=k[13];
    z[11]=k[24];
    z[12]=k[22];
    z[13]=k[4];
    z[14]=k[6];
    z[15]=k[14];
    z[16]=k[15];
    z[17]=k[3];
    z[18]=k[28];
    z[19]=k[23];
    z[20]=k[17];
    z[21]=2*z[17];
    z[22]=z[18] + z[10];
    z[23]= - z[22]*z[21];
    z[24]=npow(z[2],2);
    z[25]=z[24]*z[16];
    z[26]= - z[15] - z[11];
    z[26]=z[26]*z[25];
    z[23]=z[23] + z[26];
    z[23]=z[14]*z[23];
    z[26]=z[19] + z[20];
    z[27]=z[26] + z[3];
    z[28]=z[11] - z[6];
    z[29]=3*z[12];
    z[30]=2*z[18];
    z[31]=z[2]*z[28];
    z[31]= - static_cast<T>(16)+ 3*z[31];
    z[31]=z[16]*z[31];
    z[32]=z[17]*z[19];
    z[33]= - static_cast<T>(32)- z[32];
    z[33]=z[9]*z[33];
    z[23]=z[23] + z[33] + z[31] + z[30] - z[29] - z[28] + 3*z[27];
    z[23]=z[14]*z[23];
    z[27]=z[18] - z[15];
    z[28]=z[21]*z[27];
    z[31]=z[11] - z[10];
    z[33]=npow(z[1],2);
    z[34]=z[9]*z[31]*z[33];
    z[34]=z[28] + z[34];
    z[34]=z[8]*z[34];
    z[35]=z[3]*z[1];
    z[36]=3*z[11];
    z[37]=z[36] + z[12];
    z[38]= - z[1]*z[37];
    z[38]= - 41*z[35] + static_cast<T>(62)+ z[38];
    z[38]=z[9]*z[38];
    z[39]=z[17]*z[20];
    z[40]= - static_cast<T>(14)- z[39];
    z[40]=z[16]*z[40];
    z[41]=z[1]*z[12];
    z[42]= - static_cast<T>(42)+ z[41];
    z[42]=z[3]*z[42];
    z[43]=z[9]*z[17];
    z[43]=static_cast<T>(29)- 15*z[43];
    z[43]=z[14]*z[43];
    z[34]=z[34] + z[43] + z[38] + z[42] + z[40] + z[18] - z[20] + z[11];
    z[34]=z[8]*z[34];
    z[38]=z[16]*z[17];
    z[40]=z[4] - z[20];
    z[40]=z[40]*z[38];
    z[40]=z[20] + z[40];
    z[40]=z[16]*z[40];
    z[42]=28*z[3];
    z[43]=z[8]*z[14];
    z[44]= - z[7]*z[43]*z[42];
    z[45]=z[3]*z[16];
    z[40]=z[44] + z[40] - 14*z[45];
    z[40]=z[9]*z[40];
    z[44]=z[16]*z[2];
    z[46]= - z[15] + z[6];
    z[46]=z[46]*z[44];
    z[46]= - z[6] + z[46];
    z[46]=z[16]*z[46];
    z[47]=z[22]*z[14];
    z[48]=14*z[3];
    z[49]= - z[11] + z[48];
    z[49]=z[9]*z[49];
    z[46]=z[47] + z[46] + z[49];
    z[46]=z[14]*z[46];
    z[49]=z[16]*z[11];
    z[50]=z[3]*z[18];
    z[51]=z[27]*z[8];
    z[49]= - z[51] + z[49] + z[50];
    z[50]=z[12] - z[10];
    z[50]=z[50]*z[1];
    z[52]=z[50]*z[9];
    z[53]=z[52] - z[12] - 55*z[3];
    z[53]=z[9]*z[53];
    z[42]= - z[42] + 29*z[9];
    z[42]=z[14]*z[42];
    z[42]=z[42] + z[53] + z[49];
    z[42]=z[8]*z[42];
    z[22]=z[13]*z[3]*z[22]*npow(z[14],2);
    z[22]=z[22] + z[42] + z[46] + z[40];
    z[22]=z[7]*z[22];
    z[40]=3*z[17];
    z[42]=z[26]*z[40];
    z[42]= - static_cast<T>(8)+ z[42];
    z[42]=z[16]*z[42];
    z[46]=npow(z[17],2);
    z[53]=z[46]*z[16];
    z[54]=z[4] - z[19];
    z[54]=z[54]*z[53];
    z[50]= - z[50] + z[54];
    z[50]=z[9]*z[50];
    z[54]=3*z[18];
    z[55]=2*z[11];
    z[42]=z[50] + 27*z[3] + z[42] + z[54] + z[55] - 2*z[20] + z[12];
    z[42]=z[9]*z[42];
    z[50]=2*z[10];
    z[56]=3*z[19];
    z[57]= - z[18] + z[56] - z[50];
    z[57]=z[3]*z[57];
    z[58]= - z[11] - 15*z[3];
    z[58]=z[9]*z[58];
    z[59]=z[10] - z[19];
    z[60]= - z[13]*z[59]*npow(z[3],2);
    z[57]=z[60] - 2*z[47] + z[57] + z[58];
    z[57]=z[14]*z[57];
    z[58]=15*z[16] + 13*z[3];
    z[58]=z[14]*z[58];
    z[49]=z[58] + z[49];
    z[49]=z[8]*z[49];
    z[58]=z[45]*z[9];
    z[60]= - z[19]*z[45];
    z[49]=z[49] + z[60] - 41*z[58] + z[57];
    z[49]=z[13]*z[49];
    z[57]=3*z[20];
    z[60]=z[57] - z[56];
    z[61]=3*z[6];
    z[62]= - z[55] - z[61] - z[4] + z[15] + z[60];
    z[62]=z[16]*z[62];
    z[63]=z[12] - z[4];
    z[64]=z[3]*z[63]*z[1];
    z[65]=2*z[12];
    z[66]=z[2]*z[6];
    z[67]= - static_cast<T>(14)+ z[66];
    z[67]=z[16]*z[67];
    z[67]= - z[64] - z[65] + z[67];
    z[67]=z[3]*z[67];
    z[22]=z[22] + z[49] + z[34] + z[23] + z[42] + z[62] + z[67];
    z[22]=z[7]*z[22];
    z[23]= - z[6] + z[26];
    z[23]= - 15*z[14] + 3*z[23] - z[30];
    z[23]=z[17]*z[23];
    z[26]= - z[36] + z[10] + z[61];
    z[26]=z[1]*z[26];
    z[34]=z[40] - 14*z[53];
    z[34]=z[5]*z[34];
    z[42]= - z[33]*z[48];
    z[31]= - z[1]*z[31];
    z[31]=static_cast<T>(31)+ z[31];
    z[31]=z[1]*z[31];
    z[31]=z[42] + z[31] - 17*z[17];
    z[31]=z[9]*z[31];
    z[42]= - z[46]*z[51];
    z[23]=z[42] + z[31] - 42*z[35] + z[34] - 2*z[38] + static_cast<T>(56)+ z[26] + 
    z[23];
    z[23]=z[8]*z[23];
    z[26]= - z[5]*z[27]*z[46];
    z[26]=z[28] + z[26];
    z[26]=z[8]*z[26];
    z[27]=28*z[16];
    z[28]=z[56] - z[27];
    z[31]=z[18] - 2*z[15] + z[57];
    z[31]=z[17]*z[31];
    z[31]=z[31] + 13*z[38];
    z[31]=z[5]*z[31];
    z[34]=z[20] - z[15];
    z[35]= - z[5]*z[40];
    z[35]= - static_cast<T>(5)+ z[35];
    z[35]=z[14]*z[35];
    z[26]=z[26] + 5*z[35] + 42*z[3] + z[31] - z[30] - 2*z[34] + z[28];
    z[26]=z[8]*z[26];
    z[31]=z[17]*z[18];
    z[35]=41*z[44] - static_cast<T>(13)- z[31];
    z[35]=z[5]*z[35];
    z[40]=z[17]*z[47];
    z[30]=z[40] + z[35] - 63*z[16] + z[30] + z[50] + z[57] - 2*z[19];
    z[30]=z[14]*z[30];
    z[35]=z[5]*z[17];
    z[35]=z[35] - 1;
    z[34]=z[34]*z[35];
    z[34]= - z[27] + z[34];
    z[34]=z[5]*z[34];
    z[35]=14*z[5];
    z[40]= - z[48] + 29*z[16] + z[35];
    z[40]=z[14]*z[40];
    z[34]=z[34] + z[40];
    z[34]=z[8]*z[34];
    z[40]=z[5]*z[16];
    z[42]=z[40]*z[43];
    z[43]= - z[5]*z[58];
    z[42]=z[43] + z[42];
    z[42]=z[13]*z[42];
    z[43]=z[45]*z[5];
    z[42]=z[43] - z[42];
    z[40]=55*z[40];
    z[43]= - 55*z[16] - 28*z[5];
    z[43]=z[3]*z[43];
    z[43]= - z[40] + z[43];
    z[43]=z[9]*z[43];
    z[45]=z[3]*z[59];
    z[40]= - z[40] + z[45];
    z[40]=z[14]*z[40];
    z[34]=z[34] + z[40] + z[43] - 28*z[42];
    z[34]=z[13]*z[34];
    z[39]= - 41*z[38] - z[39] - static_cast<T>(28)- z[41];
    z[39]=z[5]*z[39];
    z[40]=z[65] - z[10];
    z[41]=13*z[1];
    z[42]= - z[5]*z[41];
    z[42]= - static_cast<T>(55)+ z[42];
    z[42]=z[3]*z[42];
    z[39]=z[52] + z[42] + z[39] - 47*z[16] - z[11] - z[20] + z[40];
    z[39]=z[9]*z[39];
    z[42]= - static_cast<T>(28)- 13*z[44];
    z[42]=z[5]*z[42];
    z[28]=z[42] - z[10] + z[28];
    z[28]=z[3]*z[28];
    z[42]= - z[19] + z[11];
    z[42]=z[16]*z[42];
    z[43]= - z[15] + z[57];
    z[43]=z[5]*z[43];
    z[26]=z[34] + z[26] + z[30] + z[39] + z[28] + z[42] + z[43];
    z[26]=z[13]*z[26];
    z[28]=z[55] + z[40];
    z[28]=z[1]*z[28];
    z[30]= - z[54] - z[4] - z[60];
    z[30]=z[17]*z[30];
    z[34]= - static_cast<T>(5)+ z[32];
    z[34]=z[34]*z[38];
    z[38]= - z[53] - z[1] - z[17];
    z[38]=z[38]*z[35];
    z[35]= - z[33]*z[35];
    z[35]=z[41] + z[35];
    z[35]=z[3]*z[35];
    z[28]=z[35] + z[38] + z[34] + z[30] - static_cast<T>(44)+ z[28];
    z[28]=z[9]*z[28];
    z[30]= - z[2] - z[17];
    z[27]=z[30]*z[27];
    z[30]=z[6] - z[4];
    z[34]=z[5]*z[30]*z[2];
    z[35]=2*z[1] - z[17];
    z[35]=z[35]*z[34];
    z[38]=z[6] - z[63];
    z[38]=z[1]*z[38];
    z[21]= - z[6]*z[21];
    z[21]=z[35] + z[27] + z[21] + z[38] - static_cast<T>(13)- 2*z[66];
    z[21]=z[5]*z[21];
    z[27]=z[6] + z[12];
    z[27]=z[2]*z[27];
    z[35]=z[1]*z[30];
    z[27]=z[35] - static_cast<T>(13)+ z[27];
    z[27]=z[1]*z[27];
    z[33]= - z[33]*z[34];
    z[25]=14*z[25];
    z[34]=z[2]*z[63];
    z[34]= - static_cast<T>(10)+ z[34];
    z[34]=z[2]*z[34];
    z[27]=z[33] - z[25] + z[34] + z[27];
    z[27]=z[5]*z[27];
    z[30]= - z[30] + z[37];
    z[30]=z[2]*z[30];
    z[33]= - z[65] - z[36];
    z[33]=z[1]*z[33];
    z[24]= - z[5]*z[24];
    z[24]=2*z[2] + z[24];
    z[24]=z[24]*z[64];
    z[24]=z[24] + z[27] - 15*z[44] + z[33] - static_cast<T>(13)+ z[30];
    z[24]=z[3]*z[24];
    z[27]=2*z[6];
    z[29]=z[36] + z[29] + z[15] + z[27];
    z[29]=z[2]*z[29];
    z[30]=z[2]*z[11];
    z[33]=static_cast<T>(36)+ z[30];
    z[33]=z[33]*z[44];
    z[31]= - z[31] + static_cast<T>(13)+ z[66];
    z[31]=z[17]*z[31];
    z[25]= - z[25] - z[2] + z[31];
    z[25]=z[5]*z[25];
    z[31]=z[46]*z[47];
    z[34]= - z[18] - z[19] + z[6];
    z[34]=z[17]*z[34];
    z[25]=z[31] + z[25] + z[33] + z[34] - static_cast<T>(8)+ z[29];
    z[25]=z[14]*z[25];
    z[29]= - z[32] + static_cast<T>(9)- z[30];
    z[29]=z[16]*z[29];

    r +=  - z[15] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + 
      z[27] + z[28] + 2*z[29] - z[50] + z[65];
 
    return r;
}

template double qg_1l_r43(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1l_r43(const std::array<dd_real,31>&);
#endif
