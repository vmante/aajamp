#include "qg_1l_rf_decl.hpp"

template<class T>
T qg_1l_r6(const std::array<T,31>& k) {
  T z[53];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[14];
    z[5]=k[15];
    z[6]=k[10];
    z[7]=k[11];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[7];
    z[11]=npow(z[4],2);
    z[12]=npow(z[11],2);
    z[13]=z[12]*z[2];
    z[14]=npow(z[4],3);
    z[15]=z[13] + z[14];
    z[16]=2*z[2];
    z[15]=z[15]*z[16];
    z[17]=2*z[14];
    z[18]=z[12]*z[9];
    z[19]=z[17] - z[18];
    z[20]=z[13] + z[19];
    z[21]=2*z[9];
    z[20]=z[20]*z[21];
    z[22]=3*z[11];
    z[20]=z[20] - z[22] - z[15];
    z[20]=z[9]*z[20];
    z[23]=npow(z[2],2);
    z[24]=2*z[23];
    z[25]=z[24]*z[12];
    z[25]=z[25] + z[11];
    z[26]=z[2]*z[25];
    z[20]=z[20] + z[4] + z[26];
    z[26]=2*z[7];
    z[20]=z[20]*z[26];
    z[27]=z[18] - z[14];
    z[28]= - z[13] + z[27];
    z[28]=z[28]*z[21];
    z[25]=z[28] + z[25];
    z[28]=z[26]*z[8];
    z[25]=z[25]*z[28];
    z[29]=4*z[2];
    z[30]=z[29]*z[12];
    z[31]=5*z[14];
    z[30]=z[30] - z[31];
    z[30]=z[30]*z[16];
    z[30]=z[30] + z[11];
    z[32]=z[14] + 12*z[13];
    z[32]=z[32]*z[21];
    z[20]=z[25] + z[20] + z[32] + z[30];
    z[20]=z[8]*z[20];
    z[25]=3*z[14];
    z[32]=z[25] - 5*z[13];
    z[32]=z[32]*z[29];
    z[33]=3*z[13];
    z[34]= - z[33] + z[18];
    z[35]=4*z[9];
    z[34]=z[34]*z[35];
    z[32]=z[34] + z[11] + z[32];
    z[32]=z[9]*z[32];
    z[34]=z[16]*z[12];
    z[36]= - z[14] + z[34];
    z[36]=z[36]*z[16];
    z[33]=z[33] + 3*z[18];
    z[37]= - z[14] + z[33];
    z[37]=z[37]*z[35];
    z[36]=z[37] + z[11] + z[36];
    z[36]=z[8]*z[36];
    z[37]=z[13] - z[14];
    z[38]=z[37]*z[29];
    z[39]= - z[11] - z[38];
    z[39]=z[39]*z[16];
    z[32]=z[36] + z[39] + z[32];
    z[32]=z[8]*z[32];
    z[36]=z[25] - z[13];
    z[39]=z[21]*z[12];
    z[40]=z[36] - z[39];
    z[41]= - z[40]*z[35];
    z[42]=z[34] - z[25];
    z[42]=z[42]*z[16];
    z[43]=9*z[11];
    z[41]=z[41] + z[43] + z[42];
    z[41]=z[9]*z[41];
    z[44]=z[37]*z[16];
    z[44]=z[44] + z[11];
    z[45]=z[44]*z[16];
    z[46]=z[45] - z[4];
    z[41]=2*z[46] + z[41];
    z[41]=z[9]*z[41];
    z[47]= - z[31] + z[13];
    z[47]=z[2]*z[47];
    z[48]=7*z[14];
    z[33]= - z[48] + z[33];
    z[33]=z[9]*z[33];
    z[49]=7*z[11];
    z[33]=z[33] + z[49] + z[47];
    z[33]=z[33]*z[21];
    z[47]= - z[14]*z[16];
    z[47]=z[49] + z[47];
    z[47]=z[2]*z[47];
    z[50]=7*z[4];
    z[33]=z[33] - z[50] + z[47];
    z[33]=z[9]*z[33];
    z[47]=2*z[4];
    z[51]=z[2]*z[11];
    z[51]= - z[47] + z[51];
    z[51]=z[2]*z[51];
    z[33]=z[33] + static_cast<T>(1)+ z[51];
    z[33]=z[7]*z[33]*z[21];
    z[42]=z[22] + z[42];
    z[42]=z[42]*z[23];
    z[32]=z[32] + z[33] + z[42] + z[41];
    z[32]=z[10]*z[32];
    z[17]=z[17] - z[13];
    z[17]=z[17]*z[2];
    z[33]=2*z[11];
    z[17]=z[17] - z[33];
    z[41]=z[17]*z[2];
    z[41]=z[41] + z[4];
    z[42]= - z[41]*z[29];
    z[42]=static_cast<T>(1)+ z[42];
    z[42]=z[7]*z[42];
    z[42]=z[42] - z[46];
    z[42]=z[2]*z[42];
    z[51]=z[44]*z[23]*z[26];
    z[52]=z[8]*z[7];
    z[52]=z[52] - 1;
    z[45]=z[52]*z[45];
    z[45]=z[51] + z[4] + z[45];
    z[45]=z[8]*z[45];
    z[42]=z[45] - static_cast<T>(1)+ z[42];
    z[42]=z[5]*z[42];
    z[45]=z[9]*z[2];
    z[51]= - z[23] + z[45];
    z[44]=z[9]*z[44]*z[51];
    z[41]= - z[41]*z[24];
    z[41]=z[41] + z[44];
    z[41]=z[41]*z[26];
    z[17]=z[17]*z[29];
    z[17]=z[50] + z[17];
    z[17]=z[2]*z[17];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[2]*z[17];
    z[29]= - z[2]*z[36];
    z[36]=4*z[11];
    z[29]=z[36] + z[29];
    z[29]=z[2]*z[29];
    z[44]=3*z[4];
    z[29]= - z[44] + z[29];
    z[29]=z[2]*z[29];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[23];
    z[29]=4*z[29] - z[45];
    z[29]=z[7]*z[29];
    z[17]=z[29] + z[17] + z[21];
    z[17]=z[5]*z[17];
    z[29]=z[9] - z[2];
    z[29]=z[46]*z[29];
    z[17]=z[17] + z[41] + static_cast<T>(1)+ z[29];
    z[17]=z[3]*z[17];
    z[29]=z[19]*z[9];
    z[29]=z[29] - z[33];
    z[41]= - z[9]*z[29];
    z[18]= - z[25] + z[18];
    z[18]=z[9]*z[18];
    z[18]=z[36] + z[18];
    z[18]=z[9]*z[18];
    z[18]= - z[44] + z[18];
    z[18]=z[9]*z[18];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[7]*z[18];
    z[18]=z[18] - z[4] + z[41];
    z[18]=z[18]*npow(z[9],2);
    z[25]=z[27]*z[35];
    z[25]=z[22] + z[25];
    z[25]=z[9]*z[25];
    z[14]= - z[14] + z[39];
    z[14]=z[14]*z[21];
    z[14]=z[11] + z[14];
    z[14]=z[8]*z[14];
    z[35]=z[7] - z[4];
    z[14]=z[14] + z[25] + z[35];
    z[14]=z[8]*z[9]*z[14];
    z[14]=4*z[18] + z[14];
    z[14]=z[10]*z[14];
    z[18]= - z[27]*z[21];
    z[18]=z[11] + z[18];
    z[18]=z[9]*z[18];
    z[18]= - z[47] + z[18];
    z[18]=z[18]*z[21];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[7]*z[18];
    z[19]=z[19]*z[21];
    z[19]= - z[22] + z[19];
    z[19]=z[9]*z[19];
    z[19]=z[4] + z[19];
    z[19]=z[19]*z[28];
    z[25]=z[9]*z[22];
    z[18]=z[19] + z[18] - z[4] + z[25];
    z[18]=z[8]*z[18];
    z[19]=z[29]*z[21];
    z[19]=z[44] + z[19];
    z[19]=z[19]*z[21];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[7]*z[19];
    z[19]=z[4] + z[19];
    z[19]=z[9]*z[19];
    z[14]=z[14] + z[18] + z[19];
    z[14]=z[6]*z[14];
    z[18]= - 5*z[11] + z[38];
    z[18]=z[2]*z[18];
    z[18]=9*z[4] + z[18];
    z[18]=z[2]*z[18];
    z[19]=z[31] - z[34];
    z[19]=z[19]*z[16];
    z[19]= - z[43] + z[19];
    z[19]=z[2]*z[19];
    z[19]=z[44] + z[19];
    z[19]=z[1]*z[19];
    z[18]=z[19] - static_cast<T>(4)+ z[18];
    z[18]=z[5]*z[18];
    z[19]=z[38] - z[11];
    z[19]=z[19]*z[2];
    z[19]=z[19] + z[47];
    z[18]=z[19] + z[18];
    z[18]=z[3]*z[18];
    z[13]= - z[48] + 8*z[13];
    z[25]=z[2]*z[13];
    z[25]=z[33] + z[25];
    z[19]=z[5]*z[19];
    z[27]= - z[6]*z[35];
    z[18]=z[27] + 2*z[25] + z[19] + z[18];
    z[18]=z[1]*z[18];
    z[19]=z[40]*z[21];
    z[15]=z[19] - z[22] + z[15];
    z[15]=z[9]*z[15];
    z[12]= - z[12]*z[23];
    z[12]= - z[11] + z[12];
    z[12]=z[12]*z[16];
    z[12]=z[12] + z[15];
    z[12]=z[9]*z[12];
    z[15]=z[2]*z[37];
    z[11]=z[11] + z[15];
    z[11]=z[11]*z[24];
    z[11]=z[12] + static_cast<T>(1)+ z[11];
    z[11]=z[11]*z[26];
    z[12]= - z[13]*z[16];
    z[12]= - z[49] + z[12];
    z[12]=z[2]*z[12];
    z[13]= - z[9]*z[30];

    r += z[11] + z[12] + z[13] + z[14] + z[17] + z[18] + z[20] + z[32]
       + z[42] - z[47];
 
    return r;
}

template double qg_1l_r6(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1l_r6(const std::array<dd_real,31>&);
#endif
