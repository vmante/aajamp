#include "qg_1l_rf_decl.hpp"

template<class T>
T qg_1l_r14(const std::array<T,31>& k) {
  T z[25];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[12];
    z[7]=k[4];
    z[8]=k[15];
    z[9]=k[3];
    z[10]=k[5];
    z[11]=3*z[1];
    z[12]=z[7] + z[1];
    z[12]=z[12]*z[11];
    z[13]=npow(z[7],2);
    z[13]=4*z[13];
    z[14]=z[8]*npow(z[7],3);
    z[12]=4*z[14] + z[13] + z[12];
    z[12]=z[6]*z[12];
    z[14]=3*z[2];
    z[15]=z[7] + z[2];
    z[15]=z[15]*z[14];
    z[13]=z[13] + z[15];
    z[13]=z[8]*z[13];
    z[12]=z[12] + z[13] + z[11] + 4*z[7] + z[14];
    z[12]=z[5]*z[12];
    z[13]=npow(z[10],2);
    z[15]=10*z[7];
    z[16]=9*z[10] + z[15];
    z[16]=z[7]*z[16];
    z[16]=3*z[13] + z[16];
    z[16]=z[8]*z[16];
    z[17]=z[1] + z[10];
    z[18]=9*z[7];
    z[16]=z[16] + z[18] + 7*z[17];
    z[16]=z[6]*z[16];
    z[17]=npow(z[2],2);
    z[19]=2*z[2] + z[1];
    z[19]=z[1]*z[19];
    z[19]=z[17] + z[19];
    z[19]=z[5]*z[19];
    z[20]=z[1]*z[2];
    z[17]=z[20] + z[17];
    z[17]=z[17]*z[5];
    z[20]=z[1] + z[2];
    z[21]= - z[20] + z[17];
    z[22]=4*z[4];
    z[21]=z[22]*z[1]*z[21];
    z[23]=2*z[20];
    z[19]=z[21] + z[23] + z[19];
    z[19]=z[4]*z[19];
    z[21]=2*z[7];
    z[24]=z[10] + z[21];
    z[24]=3*z[24] + z[2];
    z[24]=z[8]*z[24];
    z[12]=z[19] + z[12] + z[16] + static_cast<T>(5)+ z[24];
    z[12]=z[3]*z[12];
    z[16]=z[9]*z[10];
    z[19]=z[13] + z[16];
    z[19]=z[19]*z[8]*z[9];
    z[19]=z[16] + z[19];
    z[19]=z[6]*z[19];
    z[16]=z[8]*z[16];
    z[16]= - z[17] + z[19] + z[16] - z[20];
    z[16]=z[16]*z[22];
    z[17]=5*z[9];
    z[19]=18*z[10] + z[17];
    z[19]=z[9]*z[19];
    z[13]=5*z[13] + z[19];
    z[13]=z[8]*z[13];
    z[11]=z[13] + z[11] + 8*z[10] + z[17];
    z[11]=z[6]*z[11];
    z[13]=z[14] + 5*z[10] + 8*z[9];
    z[13]=z[8]*z[13];
    z[14]=z[5]*z[23];
    z[11]=z[16] + z[14] + z[11] + static_cast<T>(6)+ z[13];
    z[11]=z[4]*z[11];
    z[13]=z[2] + z[9];
    z[13]=z[18] + 7*z[13];
    z[13]=z[8]*z[13];
    z[14]=npow(z[9],2);
    z[15]=9*z[9] + z[15];
    z[15]=z[7]*z[15];
    z[14]=3*z[14] + z[15];
    z[14]=z[8]*z[14];
    z[15]=z[9] + z[21];
    z[14]=z[14] + 3*z[15] + z[1];
    z[14]=z[6]*z[14];
    z[13]=z[14] + static_cast<T>(5)+ z[13];
    z[13]=z[5]*z[13];
    z[14]=z[10] + z[9];
    z[14]=19*z[14] + 22*z[7];
    z[14]=z[8]*z[14];
    z[14]=static_cast<T>(10)+ z[14];
    z[14]=z[6]*z[14];

    r += 10*z[8] + z[11] + z[12] + z[13] + z[14];
 
    return r;
}

template double qg_1l_r14(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1l_r14(const std::array<dd_real,31>&);
#endif
