#include "qg_1l_rf_decl.hpp"

template<class T>
T qg_1l_r50(const std::array<T,31>& k) {
  T z[30];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=npow(z[4],2);
    z[11]=2*z[10];
    z[12]=npow(z[4],3);
    z[13]=z[12]*z[5];
    z[14]=z[11] + z[13];
    z[15]=z[14]*z[5];
    z[15]=z[15] + z[4];
    z[16]=z[13] + 3*z[10];
    z[17]=z[16]*z[5];
    z[17]=z[17] + 3*z[4];
    z[17]=z[17]*z[5];
    z[17]=z[17] + 1;
    z[18]=3*z[3];
    z[18]=z[17]*z[18];
    z[18]= - 2*z[15] + z[18];
    z[18]=z[6]*z[18];
    z[19]= - z[10] - z[13];
    z[19]=z[3]*z[19];
    z[20]=z[6]*z[3];
    z[21]=z[15]*z[20];
    z[19]=z[19] + z[21];
    z[19]=z[1]*z[19];
    z[21]=2*z[5];
    z[22]=z[21]*z[12];
    z[23]=3*z[5];
    z[14]= - z[14]*z[23];
    z[14]= - 2*z[4] + z[14];
    z[14]=z[3]*z[14];
    z[14]=z[19] + z[18] + z[22] + z[14];
    z[14]=z[1]*z[14];
    z[18]=2*z[9];
    z[19]=z[3]*npow(z[5],2);
    z[24]=z[5] - z[19];
    z[24]=z[24]*z[18];
    z[25]=z[5]*z[4];
    z[25]=static_cast<T>(1)+ z[25];
    z[26]=z[1]*z[4];
    z[25]=z[26] + 2*z[25];
    z[25]=z[6]*z[25];
    z[25]= - z[3] + z[25];
    z[25]=z[1]*z[25];
    z[26]=z[3]*z[5];
    z[27]= - static_cast<T>(1)+ z[26];
    z[18]=z[27]*z[18];
    z[27]=z[2]*z[9];
    z[28]= - z[27] + 2;
    z[28]=z[3]*z[28];
    z[18]=z[18] - z[6] + z[28];
    z[18]=z[2]*z[18];
    z[28]=z[4]*z[21];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=z[6]*z[5]*z[28];
    z[18]=z[18] + z[25] + z[24] + z[28] + static_cast<T>(3)- 2*z[26];
    z[18]=z[7]*z[18];
    z[24]=4*z[10];
    z[25]=z[24] + z[13];
    z[28]=z[25]*z[5];
    z[29]= - 6*z[4] - z[28];
    z[29]=z[5]*z[29];
    z[29]= - static_cast<T>(3)+ z[29];
    z[21]=z[29]*z[21];
    z[10]=5*z[10];
    z[29]=z[10] + z[13];
    z[29]=z[5]*z[29];
    z[29]=11*z[4] + z[29];
    z[29]=z[5]*z[29];
    z[29]=static_cast<T>(7)+ z[29];
    z[29]=z[29]*z[19];
    z[21]=z[21] + z[29];
    z[21]=z[6]*z[21];
    z[22]=z[10] + z[22];
    z[22]=z[5]*z[22];
    z[22]=4*z[4] + z[22];
    z[22]=z[5]*z[22];
    z[28]= - 5*z[4] - z[28];
    z[19]=z[28]*z[19];
    z[19]=z[21] + z[22] + z[19];
    z[19]=z[9]*z[19];
    z[21]=z[23]*z[12];
    z[12]=z[1]*z[12];
    z[11]=z[12] + z[11] + z[21];
    z[11]=z[1]*z[11];
    z[12]=z[24] + z[21];
    z[12]=z[5]*z[12];
    z[11]=z[11] + z[4] + z[12];
    z[11]=z[3]*z[11];
    z[12]=z[27] - 3;
    z[12]=z[20]*z[12];
    z[20]=z[15]*z[26];
    z[21]=static_cast<T>(2)- 3*z[26];
    z[21]=z[6]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[9]*z[20];
    z[11]=z[20] + z[12] + z[11];
    z[11]=z[2]*z[11];
    z[12]=z[25]*z[23];
    z[12]=19*z[4] + z[12];
    z[12]=z[5]*z[12];
    z[12]=static_cast<T>(9)+ z[12];
    z[12]=z[12]*z[26];
    z[12]= - 4*z[17] + z[12];
    z[12]=z[6]*z[12];
    z[17]=z[6]*z[17];
    z[17]= - z[7] + z[17];
    z[17]=z[9]*z[17];
    z[15]=z[6]*z[15];
    z[15]=z[15] + z[17];
    z[15]=z[8]*z[15];
    z[10]=z[10] + 4*z[13];
    z[10]=z[5]*z[10];
    z[13]= - z[16]*z[23];
    z[13]= - 7*z[4] + z[13];
    z[13]=z[13]*z[26];

    r += z[4] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[18]
       + z[19];
 
    return r;
}

template double qg_1l_r50(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1l_r50(const std::array<dd_real,31>&);
#endif
