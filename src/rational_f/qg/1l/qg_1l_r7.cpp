#include "qg_1l_rf_decl.hpp"

template<class T>
T qg_1l_r7(const std::array<T,31>& k) {
  T z[57];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[5];
    z[7]=k[7];
    z[8]=k[2];
    z[9]=k[8];
    z[10]=k[16];
    z[11]=k[10];
    z[12]=k[6];
    z[13]=k[14];
    z[14]=k[15];
    z[15]=k[11];
    z[16]=2*z[9];
    z[17]=z[12] + z[5];
    z[18]=2*z[7];
    z[19]=z[18] + z[16] - z[17];
    z[19]=z[15]*z[19];
    z[20]=z[5] + z[9];
    z[21]=npow(z[10],2);
    z[22]=z[20]*z[21];
    z[23]=z[4]*z[22];
    z[24]=z[12]*z[9];
    z[25]=npow(z[13],2);
    z[26]= - 3*z[20] - z[10];
    z[26]=z[10]*z[26];
    z[19]=z[23] + z[25] + 2*z[19] - z[24] + z[26];
    z[19]=z[4]*z[19];
    z[23]=z[2]*z[14];
    z[26]=z[23]*z[21];
    z[27]=2*z[10];
    z[28]=z[14] - z[10];
    z[28]=z[28]*z[27];
    z[28]=z[28] + z[26];
    z[28]=z[2]*z[28];
    z[29]=z[23] - 2;
    z[30]=z[13]*z[2];
    z[31]= - z[29]*z[30];
    z[32]= - static_cast<T>(1)+ 2*z[23];
    z[31]=z[31] - z[32];
    z[31]=z[13]*z[31];
    z[33]=z[10]*z[20];
    z[33]= - z[24] + z[33];
    z[33]=z[4]*z[33];
    z[33]=z[5] + z[33];
    z[33]=z[4]*z[33];
    z[34]=z[5]*z[1];
    z[33]=z[34] + z[33];
    z[35]=2*z[3];
    z[33]=z[33]*z[35];
    z[36]=5*z[5];
    z[37]=static_cast<T>(4)- z[34];
    z[37]=z[7]*z[37];
    z[19]=z[33] + z[19] + z[31] + z[28] - z[10] - z[36] + z[37];
    z[19]=z[3]*z[19];
    z[28]=4*z[10];
    z[31]=7*z[14];
    z[33]=z[28] - z[31] + z[20];
    z[33]=z[10]*z[33];
    z[37]=4*z[5];
    z[38]=z[37] + z[9];
    z[39]=5*z[7];
    z[40]=2*z[12];
    z[41]= - z[39] - z[40] + z[38];
    z[41]=z[15]*z[41];
    z[42]=2*z[13];
    z[43]=z[42]*z[2];
    z[29]=z[29]*z[43];
    z[29]=z[29] - static_cast<T>(5)+ 8*z[23];
    z[29]=z[13]*z[29];
    z[29]=z[31] + z[29];
    z[29]=z[13]*z[29];
    z[44]=npow(z[13],3);
    z[22]= - z[22] - 2*z[44];
    z[22]=z[4]*z[22];
    z[45]=z[7]*z[5];
    z[19]=z[19] + z[22] + z[29] - 6*z[26] + z[41] + z[33] - 2*z[24] + 3*
    z[45];
    z[19]=z[19]*z[35];
    z[22]=3*z[9];
    z[29]= - z[39] - 3*z[12] + z[22] + z[36];
    z[29]=z[15]*z[29];
    z[33]=2*z[14];
    z[39]=z[13]*z[32];
    z[39]=z[33] + z[39];
    z[39]=z[13]*z[39];
    z[41]=z[7] - z[5];
    z[46]= - z[12] + z[9] + z[41];
    z[46]=z[15]*z[46];
    z[46]= - z[24] + z[46];
    z[46]=z[4]*z[46];
    z[47]=z[34] - 1;
    z[48]= - z[7]*z[47];
    z[46]=z[46] - z[5] + z[48];
    z[46]=z[46]*z[35];
    z[48]= - z[33] + z[10];
    z[48]=z[10]*z[48];
    z[29]=z[46] + z[39] - 2*z[26] + z[29] + z[48] - 3*z[24] + 5*z[45];
    z[29]=z[3]*z[29];
    z[39]=z[24] - z[45];
    z[46]=z[39]*z[15];
    z[48]=z[14]*z[21];
    z[48]=z[48] + z[46];
    z[32]= - z[32]*z[42];
    z[32]= - z[31] + z[32];
    z[32]=z[32]*z[25];
    z[29]=z[29] + 5*z[48] + z[32];
    z[29]=z[29]*z[35];
    z[32]= - z[25] + z[21];
    z[32]=z[14]*z[32];
    z[48]= - z[7] - z[12] + z[20];
    z[48]=z[15]*z[48];
    z[39]=z[48] - z[39];
    z[39]=z[39]*z[35];
    z[32]=z[39] + 5*z[46] + z[32];
    z[32]=z[3]*z[32];
    z[39]=z[44]*z[33];
    z[32]=z[39] + z[32];
    z[32]=z[3]*z[32];
    z[39]=z[44]*z[15];
    z[48]=z[12]*z[14];
    z[49]= - z[48]*z[39];
    z[46]=npow(z[3],3)*z[46];
    z[46]=z[49] + z[46];
    z[49]=2*z[6];
    z[46]=z[46]*z[49];
    z[50]=z[12] + z[14];
    z[50]=z[50]*z[15];
    z[51]=z[50] - z[48];
    z[52]= - z[51]*z[42];
    z[53]=z[48]*z[15];
    z[53]=5*z[53];
    z[52]=z[53] + z[52];
    z[52]=z[52]*z[25];
    z[32]=z[46] + z[52] + z[32];
    z[32]=z[32]*z[49];
    z[46]=z[23]*z[15];
    z[49]=z[46] + z[15];
    z[52]=z[14]*z[8];
    z[54]=static_cast<T>(1)- z[52];
    z[54]=z[12]*z[54];
    z[54]= - z[7] + z[33] + z[54] - z[49];
    z[54]=z[54]*z[42];
    z[51]=3*z[51] + z[54];
    z[51]=z[13]*z[51];
    z[51]= - z[53] + z[51];
    z[51]=z[51]*z[42];
    z[53]= - z[21]*z[45];
    z[54]=4*z[12];
    z[39]=z[39]*z[4];
    z[55]=z[39]*z[54];
    z[29]=z[32] + z[29] + z[55] + z[53] + z[51];
    z[29]=z[6]*z[29];
    z[32]=z[7] - z[49];
    z[32]=z[2]*z[32];
    z[32]=static_cast<T>(4)+ z[32];
    z[32]=z[32]*z[42];
    z[51]= - static_cast<T>(2)+ 5*z[52];
    z[51]=z[12]*z[51];
    z[32]=z[32] + z[18] - z[31] + z[51] + z[49];
    z[32]=z[13]*z[32];
    z[32]= - 2*z[50] + z[32];
    z[32]=z[32]*z[42];
    z[49]= - z[18] + z[15];
    z[49]=z[49]*z[42];
    z[50]=z[40] + z[7];
    z[51]= - z[15]*z[50];
    z[49]=z[51] + z[49];
    z[49]=z[49]*z[25];
    z[51]=z[18]*z[39];
    z[49]=z[49] + z[51];
    z[51]=2*z[4];
    z[49]=z[49]*z[51];
    z[41]= - z[41] + 8*z[14];
    z[41]=z[41]*z[10];
    z[52]= - 2*z[45] + z[41];
    z[52]=z[10]*z[52];
    z[53]=z[9] + z[14];
    z[53]=z[12]*z[53];
    z[53]=z[53] - z[45];
    z[53]=z[15]*z[53];
    z[55]=z[21]*z[2];
    z[56]=z[45]*z[55];
    z[19]=z[29] + z[19] + z[49] + z[32] + z[56] + z[52] + 4*z[53];
    z[19]=z[6]*z[19];
    z[29]=3*z[7];
    z[17]=z[29] + z[22] - z[17];
    z[17]=z[15]*z[17];
    z[22]=z[51]*z[44];
    z[30]=static_cast<T>(3)+ 4*z[30];
    z[30]=z[30]*z[25];
    z[20]= - 3*z[10] - z[20];
    z[20]=z[10]*z[20];
    z[17]=z[22] + z[30] + z[17] - z[24] + z[20];
    z[17]=z[4]*z[17];
    z[20]=z[51]*z[5];
    z[24]=z[21]*z[20];
    z[30]= - z[37] + z[10];
    z[30]=z[10]*z[30];
    z[24]=z[24] + z[30] - z[25];
    z[24]=z[4]*z[24];
    z[30]=2*z[25];
    z[32]= - z[2]*z[30];
    z[24]=z[24] + z[32] + 2*z[55] + z[36] - z[12];
    z[24]=z[4]*z[24];
    z[20]=z[10]*z[20];
    z[20]=z[20] - z[5] + z[40];
    z[20]=z[4]*z[20];
    z[32]=z[34] + 1;
    z[36]=z[2]*z[12];
    z[20]=z[20] + 3*z[36] - z[32];
    z[20]=z[4]*z[20];
    z[37]=npow(z[2],2);
    z[49]=z[12]*z[37];
    z[20]=z[20] - z[1] + z[49];
    z[20]=z[20]*z[35];
    z[49]= - z[37]*z[25];
    z[52]= - z[12] + z[55];
    z[52]=z[2]*z[52];
    z[20]=z[20] + z[24] + z[49] + z[52] + z[32];
    z[20]=z[3]*z[20];
    z[24]=3*z[14];
    z[28]=z[24] - z[28];
    z[28]=z[10]*z[28];
    z[26]=z[28] + z[26];
    z[26]=z[2]*z[26];
    z[28]=z[37]*z[42];
    z[32]=static_cast<T>(4)- z[23];
    z[32]=z[2]*z[32];
    z[32]=z[32] + z[28];
    z[32]=z[13]*z[32];
    z[23]=z[32] + static_cast<T>(2)- 3*z[23];
    z[23]=z[13]*z[23];
    z[32]=3*z[5];
    z[17]=z[20] + z[17] + z[23] + z[26] - z[27] - z[32] + z[18];
    z[17]=z[17]*z[35];
    z[20]=z[14] - z[9];
    z[23]=z[20]*z[12];
    z[26]=z[21]*z[23];
    z[49]=7*z[7];
    z[52]= - z[49] - z[42];
    z[30]=z[30]*z[15]*z[52];
    z[39]=z[7]*z[39];
    z[26]=12*z[39] + z[26] + z[30];
    z[26]=z[4]*z[26];
    z[30]=z[7] - z[31] - z[9] - z[32];
    z[30]=z[10]*z[30];
    z[30]= - z[23] + z[30];
    z[30]=z[10]*z[30];
    z[31]=z[29] + z[15];
    z[31]=z[31]*z[43];
    z[32]=z[15] + z[7];
    z[31]=z[31] + z[32];
    z[31]=z[13]*z[31];
    z[39]=z[12] + z[18];
    z[39]=z[15]*z[39];
    z[31]=z[39] + z[31];
    z[31]=z[31]*z[42];
    z[23]= - z[15]*z[23];
    z[23]=z[26] + z[31] + z[30] + z[23];
    z[23]=z[4]*z[23];
    z[26]= - z[18] - z[24] + z[38];
    z[26]=z[10]*z[26];
    z[30]=z[28]*z[15];
    z[31]=z[46] - z[32];
    z[31]=z[2]*z[31];
    z[31]= - z[30] - static_cast<T>(7)+ z[31];
    z[31]=z[13]*z[31];
    z[32]=z[8]*z[48];
    z[24]=z[31] - z[15] + z[24] - 4*z[32];
    z[24]=z[24]*z[42];
    z[31]=2*z[5];
    z[20]= - 4*z[7] + z[31] + z[20];
    z[20]=z[15]*z[20];
    z[32]=z[45] - z[41];
    z[32]=z[10]*z[32];
    z[38]= - z[15]*z[45];
    z[32]=z[32] + z[38];
    z[32]=z[2]*z[32];
    z[38]=z[33]*z[12];
    z[17]=z[19] + z[17] + z[23] + z[24] + z[32] + z[20] + z[38] + z[26];
    z[17]=z[6]*z[17];
    z[19]=z[9] - z[11];
    z[19]=z[8]*z[19];
    z[20]=z[19]*z[10];
    z[23]= - z[20] - z[18] - z[54] - z[33] - z[11] - z[31];
    z[23]=z[10]*z[23];
    z[24]=z[15]*z[11];
    z[26]=z[7]*z[11];
    z[24]=z[24] - z[26];
    z[24]=z[24]*z[2];
    z[32]=z[15] - z[7];
    z[39]= - 2*z[32] - z[24];
    z[39]=z[39]*z[42];
    z[41]=3*z[11];
    z[45]=z[41] - 11*z[7];
    z[45]=z[15]*z[45];
    z[39]=z[39] - 3*z[26] + z[45];
    z[39]=z[13]*z[39];
    z[45]=z[26]*z[15];
    z[46]=5*z[45];
    z[39]=z[46] + z[39];
    z[39]=z[39]*z[42];
    z[52]= - z[11] + z[29];
    z[52]=z[15]*z[52];
    z[52]=z[26] + z[52];
    z[52]=z[52]*z[42];
    z[46]= - z[46] + z[52];
    z[46]=z[46]*z[25];
    z[22]=z[45]*z[22];
    z[22]=z[46] + z[22];
    z[22]=z[22]*z[51];
    z[46]=z[21]*z[48];
    z[22]=z[22] + z[46] + z[39];
    z[22]=z[4]*z[22];
    z[39]=z[2]*z[11];
    z[46]=z[39]*z[15];
    z[48]=z[46] + z[15];
    z[29]=z[29] - z[48];
    z[29]=z[29]*z[43];
    z[24]=z[29] + 4*z[32] + z[24];
    z[24]=z[13]*z[24];
    z[29]=2*z[11];
    z[32]= - z[29] + z[49];
    z[32]=z[15]*z[32];
    z[24]=z[24] + 2*z[26] + z[32];
    z[24]=z[24]*z[42];
    z[32]=z[5] + 4*z[14] + z[50];
    z[32]=z[10]*z[32];
    z[32]= - z[38] + z[32];
    z[32]=z[10]*z[32];
    z[43]=z[39]*z[21];
    z[49]= - z[7]*z[43];
    z[22]=z[22] + z[24] + z[49] + z[32] - 4*z[45];
    z[22]=z[4]*z[22];
    z[18]= - z[18] + 3*z[48];
    z[18]=z[2]*z[18];
    z[18]=z[18] + z[30];
    z[18]=z[13]*z[18];
    z[24]=z[7] + z[46];
    z[18]=2*z[24] + z[18];
    z[18]=z[18]*z[42];
    z[24]=z[7] + z[11];
    z[30]=z[33] - z[24];
    z[30]=z[10]*z[30];
    z[26]=z[26] + z[30];
    z[26]=z[10]*z[26];
    z[26]=z[26] - z[45];
    z[26]=z[2]*z[26];
    z[16]=z[16] + z[11];
    z[16]=z[15]*z[16];
    z[16]=z[22] + z[18] + z[26] + z[23] + z[16];
    z[16]=z[4]*z[16];
    z[18]= - z[37]*z[41];
    z[18]=z[18] - z[28];
    z[18]=z[13]*z[18];
    z[22]=z[39] + 1;
    z[18]=z[18] - z[22];
    z[18]=z[13]*z[18];
    z[23]=z[4]*z[5];
    z[26]=z[23]*z[21];
    z[28]=z[39] - 2;
    z[30]=z[2]*z[28]*z[44];
    z[32]=z[10] - z[5];
    z[33]=z[32]*z[10];
    z[30]=z[33] + z[30];
    z[30]=2*z[30] + z[26];
    z[30]=z[4]*z[30];
    z[27]=z[11] + z[27];
    z[27]=z[10]*z[27];
    z[27]=z[27] + z[43];
    z[27]=z[2]*z[27];
    z[18]=z[30] + z[18] + z[27] - z[10] + z[31] + z[12];
    z[18]=z[4]*z[18];
    z[25]= - z[28]*z[25];
    z[21]=z[25] - 2*z[21] + z[43];
    z[21]=z[2]*z[21];
    z[25]=z[33] + z[26];
    z[25]=z[4]*z[25];
    z[21]=z[25] + z[10] + z[21];
    z[21]=z[4]*z[21];
    z[25]=z[12] + z[10];
    z[26]= - z[29] - z[10];
    z[26]=z[26]*z[2]*z[10];
    z[25]=2*z[25] + z[26];
    z[25]=z[2]*z[25];
    z[26]=2*z[2];
    z[27]= - static_cast<T>(1)+ z[39];
    z[26]=z[27]*z[26];
    z[27]=z[37]*z[13];
    z[26]=z[26] + z[27];
    z[26]=z[13]*z[26];
    z[21]=z[21] + z[26] + z[25] - static_cast<T>(2)- z[34];
    z[21]=z[4]*z[21];
    z[23]=z[10]*z[23];
    z[23]=z[23] + z[32];
    z[23]=z[4]*z[23];
    z[23]=z[23] - z[36] + z[47];
    z[23]=z[4]*z[23];
    z[25]= - z[2]*z[40];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[2]*z[25];
    z[23]=z[23] + z[1] + z[25];
    z[23]=z[4]*z[23];
    z[25]=static_cast<T>(2)- z[36];
    z[25]=z[2]*z[25];
    z[25]= - z[1] + z[25];
    z[25]=z[2]*z[25];
    z[23]=z[25] + z[23];
    z[23]=z[23]*z[35];
    z[25]=z[40] + z[10];
    z[25]=z[2]*z[25];
    z[25]= - static_cast<T>(3)+ z[25];
    z[25]=z[2]*z[25];
    z[21]=z[23] + z[21] - z[27] + z[1] + z[25];
    z[21]=z[3]*z[21];
    z[22]=z[22]*z[2];
    z[23]=z[22] + z[27];
    z[23]=z[13]*z[23];
    z[22]= - z[10]*z[22];
    z[18]=z[21] + z[18] + z[23] + static_cast<T>(1)+ z[22];
    z[18]=z[18]*z[35];
    z[21]=z[10] - z[15];
    z[21]=z[2]*z[24]*z[21];
    z[22]= - z[2]*z[48];
    z[23]= - z[15]*z[27];
    z[22]=z[23] + static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[42];
    z[23]=z[8]*z[38];
    z[19]= - static_cast<T>(4)- z[19];
    z[19]=z[15]*z[19];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[23];
 
    return r;
}

template double qg_1l_r7(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1l_r7(const std::array<dd_real,31>&);
#endif
