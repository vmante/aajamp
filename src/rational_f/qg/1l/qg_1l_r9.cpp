#include "qg_1l_rf_decl.hpp"

template<class T>
T qg_1l_r9(const std::array<T,31>& k) {
  T z[50];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[5];
    z[8]=k[7];
    z[9]=k[6];
    z[10]=k[15];
    z[11]=k[27];
    z[12]=k[3];
    z[13]=k[21];
    z[14]=z[7]*z[6];
    z[14]=z[14] + 1;
    z[15]=npow(z[9],2);
    z[16]= - z[15]*z[8]*z[14];
    z[17]=npow(z[10],2);
    z[18]=z[8]*z[9];
    z[19]= - z[18] - z[17];
    z[19]=z[6]*z[19];
    z[16]=z[19] + z[16];
    z[16]=z[7]*z[16];
    z[19]=z[10]*z[5];
    z[20]=z[19]*z[8];
    z[21]=npow(z[8],2);
    z[22]= - z[5]*z[21];
    z[22]=z[22] + z[20];
    z[22]=z[4]*z[22];
    z[23]=z[10]*z[8];
    z[22]=z[22] - z[21] + z[23];
    z[22]=z[10]*z[22];
    z[23]=z[4]*z[5];
    z[24]=z[23] + 1;
    z[17]= - z[6]*z[17]*z[24];
    z[17]=z[17] + z[22];
    z[17]=z[12]*z[17];
    z[22]=z[9]*z[5];
    z[25]=z[22] - 1;
    z[26]=z[25]*z[18];
    z[27]=npow(z[5],2);
    z[28]=z[27]*z[10];
    z[29]=3*z[28];
    z[30]= - 5*z[5] - z[29];
    z[30]=z[10]*z[30];
    z[31]= - z[4]*z[28];
    z[30]=z[30] + z[31];
    z[30]=z[4]*z[30];
    z[31]=z[19] + 1;
    z[32]=z[31]*z[10];
    z[30]= - 4*z[32] + z[30];
    z[30]=z[6]*z[30];
    z[33]=2*z[8];
    z[34]=z[10]*z[33];
    z[20]= - z[4]*z[20];
    z[16]=z[17] + z[16] + z[30] + z[20] + z[34] - z[15] + z[26];
    z[16]=z[12]*z[16];
    z[17]=2*z[9];
    z[20]=z[25]*z[17];
    z[25]=z[17]*z[27];
    z[26]=3*z[5];
    z[25]=z[25] - z[26];
    z[25]=z[25]*z[9];
    z[30]= - static_cast<T>(1)- z[25];
    z[30]=z[10]*z[30];
    z[34]=z[31]*z[15];
    z[35]=z[2]*z[34];
    z[36]=2*z[3];
    z[37]= - z[12]*z[15];
    z[30]=z[35] + z[37] - z[36] - z[20] + z[30];
    z[30]=z[2]*z[30];
    z[35]=z[27]*z[9];
    z[37]=2*z[5];
    z[38]=z[37] - z[35];
    z[38]=z[8]*z[38];
    z[39]=npow(z[5],3);
    z[40]=z[39]*z[9];
    z[41]=z[40] - 3*z[27];
    z[42]=z[41]*z[9];
    z[43]=z[26] + z[42];
    z[43]=z[10]*z[43];
    z[44]=z[35] - z[5];
    z[45]= - z[44]*z[18];
    z[46]=2*z[22];
    z[47]=z[46] - 1;
    z[48]=z[9]*z[47];
    z[45]=z[48] + z[45];
    z[45]=z[12]*z[45];
    z[48]=z[9]*z[44];
    z[49]=z[3]*z[37];
    z[36]=z[36] + z[8] + z[10];
    z[36]=z[7]*z[36];
    z[30]=z[30] + z[45] + z[36] + z[49] + z[43] + z[38] + static_cast<T>(2)+ z[48];
    z[30]=z[11]*z[30];
    z[36]=z[18] + z[15];
    z[38]=z[3]*z[8];
    z[14]= - z[38]*z[36]*z[14];
    z[36]=2*z[15];
    z[43]=z[36] + z[18];
    z[43]=z[43]*z[8];
    z[45]= - z[9] - z[8];
    z[45]=z[45]*z[33];
    z[45]=z[15] + z[45];
    z[45]=z[3]*z[45];
    z[45]=z[43] + z[45];
    z[45]=z[6]*z[45];
    z[14]=z[45] + z[14];
    z[14]=z[7]*z[14];
    z[45]=z[22] - 3;
    z[45]=z[45]*z[9];
    z[33]= - z[45] - z[33];
    z[33]=z[3]*z[33];
    z[48]=4*z[9] + z[8];
    z[48]=z[8]*z[48];
    z[33]=z[33] - z[36] + z[48];
    z[33]=z[6]*z[33];
    z[36]=z[22] - 2;
    z[36]=z[36]*z[8];
    z[45]=z[45] + z[36];
    z[45]=z[45]*z[38];
    z[14]=z[14] + z[33] + z[43] + z[45];
    z[14]=z[7]*z[14];
    z[33]=z[28] + z[26];
    z[33]=z[33]*z[10];
    z[43]= - z[33] - static_cast<T>(4)- z[22];
    z[43]=z[3]*z[43];
    z[45]= - z[9] + z[8];
    z[19]= - static_cast<T>(3)- 2*z[19];
    z[19]=z[10]*z[19];
    z[19]=z[43] + 2*z[45] + z[19];
    z[19]=z[6]*z[19];
    z[43]=static_cast<T>(5)- z[46];
    z[43]=z[9]*z[43];
    z[36]=z[43] - z[36];
    z[36]=z[8]*z[36];
    z[43]=z[35] - z[26];
    z[45]= - z[9]*z[43];
    z[45]= - static_cast<T>(3)+ z[45];
    z[45]=z[45]*z[38];
    z[14]=z[14] + z[19] + z[36] + z[45];
    z[14]=z[7]*z[14];
    z[19]=z[15]*z[37];
    z[25]=z[10]*z[25];
    z[36]= - z[17]*z[28];
    z[36]=z[36] - z[47];
    z[36]=z[4]*z[36];
    z[45]= - z[5] - z[28];
    z[45]=z[4]*z[45];
    z[45]=z[45] - static_cast<T>(2)- z[33];
    z[45]=z[3]*z[45];
    z[19]=z[45] + z[36] + z[19] + z[25];
    z[19]=z[4]*z[19];
    z[25]=z[10]*z[22];
    z[25]=z[9] + z[25];
    z[25]=z[4]*z[25];
    z[25]=z[25] - z[34];
    z[25]=z[4]*z[25];
    z[34]=npow(z[4],2);
    z[31]= - z[3]*z[31]*z[34];
    z[25]=z[25] + z[31];
    z[25]=z[2]*z[25];
    z[19]=z[25] + z[19];
    z[19]=z[2]*z[19];
    z[15]= - z[27]*z[15];
    z[25]= - z[29] - 10*z[5] - z[42];
    z[25]=z[10]*z[25];
    z[29]=z[10]*z[41];
    z[29]=z[29] + z[44];
    z[29]=z[4]*z[29];
    z[15]=z[29] + z[15] + z[25];
    z[15]=z[4]*z[15];
    z[25]=z[39]*z[10];
    z[29]= - 11*z[27] - 3*z[25];
    z[29]=z[10]*z[29];
    z[31]= - z[27] - 2*z[25];
    z[31]=z[4]*z[31];
    z[36]=4*z[5];
    z[29]=z[31] - z[36] + z[29];
    z[29]=z[4]*z[29];
    z[31]=z[10]*npow(z[5],4);
    z[41]= - 6*z[39] - z[31];
    z[41]=z[10]*z[41];
    z[31]= - z[39] - z[31];
    z[31]=z[4]*z[31];
    z[39]=5*z[27];
    z[31]=z[31] - z[39] + z[41];
    z[31]=z[4]*z[31];
    z[41]= - 4*z[27] - z[25];
    z[41]=z[10]*z[41];
    z[41]= - z[26] + z[41];
    z[31]=2*z[41] + z[31];
    z[31]=z[3]*z[31];
    z[28]= - 12*z[5] - 5*z[28];
    z[28]=z[10]*z[28];
    z[28]=z[31] + z[29] - static_cast<T>(3)+ z[28];
    z[28]=z[6]*z[28];
    z[29]= - z[27] + z[40];
    z[29]=z[8]*z[29];
    z[31]=z[8]*z[44];
    z[31]= - z[22] + z[31];
    z[31]=z[7]*z[31];
    z[29]=z[31] + z[29] + z[37] - 3*z[35];
    z[29]=z[8]*z[29];
    z[31]=z[4] + z[8];
    z[35]=z[8]*z[5];
    z[35]=z[35] - 1;
    z[31]=z[35]*z[31];
    z[24]=z[12]*z[21]*z[24];
    z[21]=z[21]*z[7];
    z[24]=z[24] + z[21] - 2*z[6] - z[9] + z[31];
    z[24]=z[12]*z[24];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[24] + 2*z[22] + z[29];
    z[22]=z[13]*z[22];
    z[23]= - static_cast<T>(2)- z[23];
    z[23]=z[4]*z[23];
    z[24]= - z[2]*z[34];
    z[23]=z[24] + z[23];
    z[23]=z[3]*z[23];
    z[24]= - z[4]*z[27];
    z[24]= - z[26] + z[24];
    z[24]=z[4]*z[24];
    z[24]= - static_cast<T>(6)+ z[24];
    z[26]=z[6]*z[3];
    z[24]=z[24]*z[26];
    z[21]= - z[26]*z[21];
    z[26]= - z[6]*z[38];
    z[21]=z[26] + z[21];
    z[21]=z[7]*z[21];
    z[21]=z[21] + z[24] + z[23];
    z[21]=z[1]*z[21];
    z[17]=z[43]*z[17];
    z[18]=z[27]*z[18];
    z[17]=z[18] + static_cast<T>(3)+ z[17];
    z[17]=z[8]*z[17];
    z[18]= - z[39] - z[25];
    z[18]=z[10]*z[18];
    z[23]= - z[27] - z[25];
    z[23]=z[4]*z[23];
    z[18]=z[23] - z[36] + z[18];
    z[18]=z[4]*z[18];
    z[18]=z[18] - static_cast<T>(6)- z[33];
    z[18]=z[3]*z[18];

    r += z[14] + z[15] + z[16] + z[17] + z[18] + z[19] - z[20] + z[21]
       + z[22] + z[28] + z[30] - 2*z[32];
 
    return r;
}

template double qg_1l_r9(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1l_r9(const std::array<dd_real,31>&);
#endif
