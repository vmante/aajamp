#include "qg_1l_rf_decl.hpp"

template<class T>
T qg_1l_r37(const std::array<T,31>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[2];
    z[7]=k[4];
    z[8]=k[10];
    z[9]=k[15];
    z[10]=k[3];
    z[11]=k[6];
    z[12]=npow(z[3],2);
    z[13]=z[12]*z[2];
    z[14]=2*z[3];
    z[15]=z[2]*z[3];
    z[16]= - static_cast<T>(1)+ 2*z[15];
    z[16]=z[4]*z[16];
    z[16]=z[16] + z[14] + z[13];
    z[16]=z[1]*z[16];
    z[17]=z[7]*z[3];
    z[18]= - z[7]*z[13];
    z[18]=z[18] + static_cast<T>(1)- z[17];
    z[19]= - z[17] + z[15];
    z[20]=z[4]*z[2];
    z[19]=z[19]*z[20];
    z[16]=z[16] + 2*z[18] + z[19];
    z[16]=z[4]*z[16];
    z[18]=z[12]*z[7];
    z[14]=z[18] - z[14];
    z[19]=z[14]*z[7];
    z[21]=z[19] + 1;
    z[22]=z[18] - z[3];
    z[23]= - z[2]*z[22];
    z[23]=z[23] - z[21];
    z[24]=z[9]*z[7];
    z[23]=z[24]*z[23];
    z[25]=z[7]*z[22]*z[20];
    z[21]=z[25] - 2*z[21] + z[23];
    z[21]=z[11]*z[21];
    z[23]= - 2*z[24] - static_cast<T>(1)- z[17];
    z[23]=z[9]*z[23];
    z[25]=z[10]*npow(z[9],2);
    z[17]= - z[17]*z[25];
    z[17]=z[23] + z[17];
    z[17]=z[10]*z[17];
    z[23]= - z[6]*z[9];
    z[23]=z[23] - 1;
    z[24]=z[24] + 1;
    z[23]=z[24]*z[23];
    z[17]=z[17] + z[23];
    z[17]=z[8]*z[17];
    z[19]=static_cast<T>(3)+ z[19];
    z[19]=z[9]*z[19];
    z[22]=z[9]*z[22];
    z[12]= - z[12] + z[22];
    z[12]=z[2]*z[12];
    z[13]= - z[13] - z[9] - z[14];
    z[13]=z[9]*z[13];
    z[14]= - z[3]*z[25];
    z[13]=z[14] + z[13];
    z[13]=z[10]*z[13];
    z[14]=z[15] - 1;
    z[20]=z[14]*z[20];
    z[15]= - z[15] + 2*z[20];
    z[15]=z[4]*z[15];
    z[20]=z[1]*z[14]*npow(z[4],2);
    z[15]=z[15] + z[20];
    z[15]=z[1]*z[15];
    z[14]=z[4]*z[14];
    z[14]= - z[3] + z[14];
    z[14]=z[4]*z[14]*npow(z[2],2);
    z[14]=z[14] + z[15];
    z[14]=z[5]*z[14];

    r += z[3] + z[12] + z[13] + z[14] + z[16] + z[17] + z[18] + z[19]
       + z[21];
 
    return r;
}

template double qg_1l_r37(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1l_r37(const std::array<dd_real,31>&);
#endif
