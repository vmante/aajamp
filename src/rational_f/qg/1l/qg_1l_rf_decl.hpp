#ifndef QG_1L_RF_DECL
#define QG_1L_RF_DECL

#include <array>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
T qg_1l_r1(const std::array<T,31>&);

template <class T>
T qg_1l_r2(const std::array<T,31>&);

template <class T>
T qg_1l_r3(const std::array<T,31>&);

template <class T>
T qg_1l_r4(const std::array<T,31>&);

template <class T>
T qg_1l_r5(const std::array<T,31>&);

template <class T>
T qg_1l_r6(const std::array<T,31>&);

template <class T>
T qg_1l_r7(const std::array<T,31>&);

template <class T>
T qg_1l_r8(const std::array<T,31>&);

template <class T>
T qg_1l_r9(const std::array<T,31>&);

template <class T>
T qg_1l_r10(const std::array<T,31>&);

template <class T>
T qg_1l_r11(const std::array<T,31>&);

template <class T>
T qg_1l_r12(const std::array<T,31>&);

template <class T>
T qg_1l_r13(const std::array<T,31>&);

template <class T>
T qg_1l_r14(const std::array<T,31>&);

template <class T>
T qg_1l_r15(const std::array<T,31>&);

template <class T>
T qg_1l_r16(const std::array<T,31>&);

template <class T>
T qg_1l_r17(const std::array<T,31>&);

template <class T>
T qg_1l_r18(const std::array<T,31>&);

template <class T>
T qg_1l_r19(const std::array<T,31>&);

template <class T>
T qg_1l_r20(const std::array<T,31>&);

template <class T>
T qg_1l_r21(const std::array<T,31>&);

template <class T>
T qg_1l_r22(const std::array<T,31>&);

template <class T>
T qg_1l_r23(const std::array<T,31>&);

template <class T>
T qg_1l_r24(const std::array<T,31>&);

template <class T>
T qg_1l_r25(const std::array<T,31>&);

template <class T>
T qg_1l_r26(const std::array<T,31>&);

template <class T>
T qg_1l_r27(const std::array<T,31>&);

template <class T>
T qg_1l_r28(const std::array<T,31>&);

template <class T>
T qg_1l_r29(const std::array<T,31>&);

template <class T>
T qg_1l_r30(const std::array<T,31>&);

template <class T>
T qg_1l_r31(const std::array<T,31>&);

template <class T>
T qg_1l_r32(const std::array<T,31>&);

template <class T>
T qg_1l_r33(const std::array<T,31>&);

template <class T>
T qg_1l_r34(const std::array<T,31>&);

template <class T>
T qg_1l_r35(const std::array<T,31>&);

template <class T>
T qg_1l_r36(const std::array<T,31>&);

template <class T>
T qg_1l_r37(const std::array<T,31>&);

template <class T>
T qg_1l_r38(const std::array<T,31>&);

template <class T>
T qg_1l_r39(const std::array<T,31>&);

template <class T>
T qg_1l_r40(const std::array<T,31>&);

template <class T>
T qg_1l_r41(const std::array<T,31>&);

template <class T>
T qg_1l_r42(const std::array<T,31>&);

template <class T>
T qg_1l_r43(const std::array<T,31>&);

template <class T>
T qg_1l_r44(const std::array<T,31>&);

template <class T>
T qg_1l_r45(const std::array<T,31>&);

template <class T>
T qg_1l_r46(const std::array<T,31>&);

template <class T>
T qg_1l_r47(const std::array<T,31>&);

template <class T>
T qg_1l_r48(const std::array<T,31>&);

template <class T>
T qg_1l_r49(const std::array<T,31>&);

template <class T>
T qg_1l_r50(const std::array<T,31>&);

template <class T>
T qg_1l_r51(const std::array<T,31>&);

template <class T>
T qg_1l_r52(const std::array<T,31>&);

template <class T>
T qg_1l_r53(const std::array<T,31>&);

template <class T>
T qg_1l_r54(const std::array<T,31>&);

#endif /* QG_1L_RF_DECL */
