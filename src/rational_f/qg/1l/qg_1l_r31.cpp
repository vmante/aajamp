#include "qg_1l_rf_decl.hpp"

template<class T>
T qg_1l_r31(const std::array<T,31>& k) {
  T z[39];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[8];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=k[3];
    z[11]=k[2];
    z[12]=k[15];
    z[13]=k[6];
    z[14]=npow(z[4],3);
    z[15]=z[14]*z[2];
    z[16]=npow(z[4],2);
    z[17]= - 4*z[16] + z[15];
    z[17]=z[2]*z[17];
    z[18]=2*z[2];
    z[19]= - z[14]*z[18];
    z[19]=z[16] + z[19];
    z[20]=z[14]*z[5];
    z[19]=2*z[19] + z[20];
    z[19]=z[5]*z[19];
    z[17]=z[19] + z[4] + z[17];
    z[17]=z[13]*z[17];
    z[15]=z[15] - z[20];
    z[19]= - z[16] + z[15];
    z[21]=z[10]*z[14];
    z[19]=z[21] + 2*z[19];
    z[19]=z[13]*z[19];
    z[21]=3*z[14];
    z[22]=z[9]*z[12]*z[3];
    z[19]=z[22] - z[21] + z[19];
    z[19]=z[10]*z[19];
    z[22]=3*z[2];
    z[23]=z[22]*z[14];
    z[21]=z[1]*z[21];
    z[24]=3*z[3];
    z[25]=z[12]*z[24];
    z[26]=z[2]*z[3];
    z[27]=3*z[26];
    z[28]=z[27] + 1;
    z[29]=z[12]*z[28];
    z[29]=z[3] + z[29];
    z[29]=z[9]*z[29];
    z[17]=z[19] + z[29] + z[25] + z[17] + z[21] + 3*z[20] + 5*z[16] - 
    z[23];
    z[17]=z[10]*z[17];
    z[19]=z[16]*z[26];
    z[20]=2*z[16];
    z[21]=z[3]*z[4];
    z[19]=z[19] - z[20] + z[21];
    z[25]=z[5]*z[3];
    z[29]=3*z[25];
    z[30]= - z[16]*z[29];
    z[31]=z[26] + 1;
    z[32]=z[25] - z[31];
    z[32]=z[14]*z[32];
    z[33]=z[16]*z[3];
    z[32]= - z[33] + z[32];
    z[32]=z[1]*z[32];
    z[34]=z[7]*z[3];
    z[19]=z[32] - z[34] + 2*z[19] + z[30];
    z[19]=z[1]*z[19];
    z[30]=z[31]*z[7];
    z[31]=z[18]*z[30];
    z[32]=z[1]*z[34];
    z[32]=z[32] + z[3] + z[30];
    z[32]=z[1]*z[32];
    z[35]=npow(z[2],2);
    z[30]=z[35]*z[30];
    z[36]=4*z[26];
    z[37]=static_cast<T>(3)+ z[36];
    z[37]=z[2]*z[37];
    z[30]=z[37] + 2*z[30];
    z[30]=z[12]*z[30];
    z[26]=static_cast<T>(1)+ 2*z[26];
    z[30]=z[30] + z[32] + z[31] + z[26];
    z[30]=z[9]*z[30];
    z[31]=static_cast<T>(3)+ 4*z[25];
    z[31]=z[31]*z[5];
    z[22]=z[31] - z[22];
    z[31]=z[25] + 1;
    z[32]=z[31]*z[5];
    z[37]= - z[2] + 2*z[32];
    z[37]=z[5]*z[37];
    z[37]=z[35] + z[37];
    z[37]=z[13]*z[37];
    z[37]=z[37] + z[22];
    z[37]=z[7]*z[37];
    z[38]= - z[14]*z[25];
    z[33]=z[33] + z[38];
    z[33]=z[5]*z[33];
    z[33]=z[34] - z[21] + z[33];
    z[33]=z[1]*z[33];
    z[34]=z[16]*z[31];
    z[34]= - z[21] + z[34];
    z[34]=z[5]*z[34];
    z[38]=z[7]*z[31];
    z[34]=z[38] - z[4] + z[34];
    z[33]=3*z[34] + z[33];
    z[33]=z[1]*z[33];
    z[22]= - z[4]*z[22];
    z[22]=z[33] + z[37] + z[22];
    z[22]=z[6]*z[22];
    z[24]=z[4]*z[24];
    z[23]=z[23] - z[20] + z[24];
    z[23]=z[5]*z[23];
    z[24]=z[16]*z[18];
    z[24]=z[3] + z[24];
    z[24]=z[2]*z[24];
    z[14]=z[14]*z[35];
    z[33]=z[3] - z[4];
    z[14]=z[14] - z[33];
    z[14]=z[5]*z[14];
    z[14]=z[24] + z[14];
    z[14]=z[5]*z[14];
    z[24]=z[4]*z[35];
    z[14]=z[24] + z[14];
    z[14]=z[7]*z[14];
    z[15]=z[20] - z[15];
    z[15]=z[5]*z[15];
    z[20]= - z[2]*z[16];
    z[15]=z[15] + z[4] + z[20];
    z[15]=z[2]*z[15];
    z[14]=2*z[15] + z[14];
    z[14]=z[13]*z[14];
    z[15]=z[26]*z[18];
    z[18]=z[27] + z[31];
    z[18]=z[5]*z[18];
    z[15]=z[15] + z[18];
    z[15]=z[7]*z[15];
    z[15]=z[15] + 2*z[28] + z[29];
    z[15]=z[12]*z[15];
    z[18]=z[10]*z[12];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[10]*z[18];
    z[18]=z[18] + z[1];
    z[18]=z[3]*z[18];
    z[20]=z[12]*z[32];
    z[24]=z[9]*z[7];
    z[26]= - npow(z[1],2)*z[24];
    z[18]=z[26] + z[20] + z[18];
    z[18]=z[8]*z[18];
    z[20]=z[12] - z[8];
    z[20]=z[11]*z[24]*z[20];
    z[24]=z[8] + z[7];
    z[24]=z[12]*z[24];
    z[26]=z[7]*z[2];
    z[26]=static_cast<T>(1)+ z[26];
    z[26]=z[12]*z[26];
    z[26]=z[7] + z[26];
    z[26]=z[9]*z[26];
    z[20]=z[20] + z[26] + z[24];
    z[20]=z[11]*z[20];
    z[16]=6*z[16] - z[21];
    z[16]=z[2]*z[16];
    z[21]= - 2*z[25] + static_cast<T>(1)+ z[36];
    z[21]=z[7]*z[21];

    r += z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21]
       + z[22] + z[23] + z[30] + 2*z[33];
 
    return r;
}

template double qg_1l_r31(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1l_r31(const std::array<dd_real,31>&);
#endif
