#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1002(const std::array<T,31>& k) {
  T z[22];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[2];
    z[7]=k[9];
    z[8]=k[5];
    z[9]=k[4];
    z[10]=z[3]*z[6];
    z[11]=z[10] + 1;
    z[12]=z[4]*z[8];
    z[13]=z[3]*npow(z[6],2);
    z[14]=z[6] - z[13];
    z[14]=z[2]*z[14];
    z[15]=npow(z[8],2);
    z[16]=z[15]*z[4];
    z[16]=z[16] + z[8];
    z[17]= - z[5]*z[16];
    z[14]=z[17] + z[14] + z[12] + z[11];
    z[17]=2*z[7];
    z[14]=z[14]*z[17];
    z[11]= - z[2]*z[11];
    z[18]= - static_cast<T>(4)- 5*z[12];
    z[18]=z[5]*z[18];
    z[11]=z[14] + z[18] + z[11] - z[3] + z[4];
    z[11]=z[11]*z[17];
    z[14]=z[10] - 1;
    z[17]= - z[2]*z[14];
    z[18]= - z[5]*z[12];
    z[17]=z[17] + z[18];
    z[18]=4*z[7];
    z[17]=z[17]*z[18];
    z[19]=z[2]*z[3];
    z[20]=z[5]*z[4];
    z[17]=z[17] - z[19] - 5*z[20];
    z[17]=z[7]*z[17];
    z[19]=z[1]*z[5];
    z[19]= - 4*z[19] + 1;
    z[20]=z[4]*z[3];
    z[19]=z[19]*z[20]*z[2];
    z[21]= - 2*z[3] - 5*z[4];
    z[21]=z[2]*z[21];
    z[21]=z[20] + 2*z[21];
    z[21]=z[5]*z[21];
    z[17]=z[17] + z[21] + z[19];
    z[17]=z[1]*z[17];
    z[12]=3*z[12];
    z[10]= - z[12] - static_cast<T>(2)- z[10];
    z[10]=z[2]*z[10];
    z[10]=z[10] - z[3];
    z[19]=z[3]*z[9];
    z[21]=static_cast<T>(1)+ z[19];
    z[21]=z[4]*z[21];
    z[10]=z[21] + 3*z[10];
    z[10]=z[5]*z[10];
    z[19]=static_cast<T>(3)+ z[19];
    z[19]=z[4]*z[19];
    z[19]=z[3] + z[19];
    z[19]=z[2]*z[19];
    z[10]=z[17] + z[11] + z[10] + 2*z[20] + z[19];
    z[10]=z[1]*z[10];
    z[11]=z[12] - z[14];
    z[12]=2*z[6];
    z[14]= - z[12] - z[13];
    z[14]=z[2]*z[14];
    z[17]=z[13] + z[6] + z[16];
    z[17]=z[17]*z[18];
    z[16]= - 3*z[6] - z[16];
    z[16]=z[5]*z[16];
    z[11]=z[17] + z[16] + 2*z[11] + z[14];
    z[11]=z[7]*z[11];
    z[14]=z[9] + 2*z[8];
    z[14]=z[14]*z[4];
    z[12]=z[9] - z[12];
    z[12]=z[3]*z[12];
    z[12]=z[14] + static_cast<T>(2)+ z[12];
    z[16]=3*z[4];
    z[15]= - z[15]*z[16];
    z[13]=z[13] + z[8];
    z[13]=z[15] - z[6] - 3*z[13];
    z[13]=z[2]*z[13];
    z[12]=2*z[12] + z[13];
    z[12]=z[5]*z[12];
    z[13]=z[3] + 2*z[4];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[2]*z[14];
    z[10]=z[10] + z[11] + z[12] + 2*z[13] + z[14];

    r += z[10]*z[1];
 
    return r;
}

template double qg_2lNLC_r1002(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1002(const std::array<dd_real,31>&);
#endif
