#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r831(const std::array<T,31>& k) {
  T z[64];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[12];
    z[6]=k[15];
    z[7]=k[2];
    z[8]=k[4];
    z[9]=k[7];
    z[10]=k[14];
    z[11]=k[5];
    z[12]=k[13];
    z[13]=k[9];
    z[14]=k[3];
    z[15]=n<T>(1,2)*z[7];
    z[16]=n<T>(11,2)*z[13];
    z[17]=z[16] - 893*z[10];
    z[17]=z[17]*z[15];
    z[18]= - 43*z[8] + n<T>(27,2)*z[7];
    z[19]=z[4]*z[8];
    z[20]= - z[18]*z[19];
    z[21]=n<T>(15,2)*z[7] - 7*z[8];
    z[20]=5*z[21] + z[20];
    z[21]=n<T>(1,2)*z[4];
    z[20]=z[20]*z[21];
    z[22]=z[3]*z[7];
    z[17]=z[20] + n<T>(11,4)*z[22] + static_cast<T>(1761)+ z[17];
    z[17]=z[17]*z[21];
    z[20]=npow(z[10],2);
    z[23]=npow(z[10],3);
    z[24]=n<T>(9,2)*z[23];
    z[25]= - z[7]*z[24];
    z[25]=23*z[20] + z[25];
    z[25]=z[11]*z[25];
    z[26]=n<T>(11,8)*z[13];
    z[27]=z[5]*z[11];
    z[28]=z[27]*z[26];
    z[15]=z[15] - z[8];
    z[29]=z[15]*z[19];
    z[29]= - n<T>(1,2)*z[8] + z[29];
    z[29]=z[6]*z[29];
    z[15]= - z[4]*z[15];
    z[15]=z[15] + z[29];
    z[15]=z[6]*z[15];
    z[29]=z[27] + 1;
    z[30]=z[29]*z[3];
    z[31]=z[16] - 1425*z[10];
    z[32]=z[7]*z[20];
    z[15]=n<T>(21,4)*z[15] + z[17] + n<T>(11,8)*z[30] + z[28] + z[25] + n<T>(1,4)*
    z[31] + 2*z[32];
    z[15]=z[6]*z[15];
    z[17]=2*z[3];
    z[25]=npow(z[11],2);
    z[28]=z[25]*z[5];
    z[31]=z[28] + z[11];
    z[32]= - z[31]*z[17];
    z[32]=z[32] + z[29];
    z[33]=npow(z[8],3);
    z[34]=z[33]*z[4];
    z[35]=npow(z[8],2);
    z[36]=z[34] + 2*z[35];
    z[37]=2*z[4];
    z[36]=z[36]*z[37];
    z[38]=77*z[8];
    z[36]=z[36] + z[38];
    z[39]=z[4]*z[36];
    z[40]=npow(z[8],4);
    z[41]=z[40]*z[4];
    z[42]=3*z[33] + z[41];
    z[42]=z[42]*z[37];
    z[42]=81*z[35] + z[42];
    z[42]=z[4]*z[42];
    z[42]=z[38] + z[42];
    z[42]=z[6]*z[42];
    z[32]=2*z[42] + 75*z[32] + z[39];
    z[32]=z[9]*z[32];
    z[39]=75*z[27];
    z[42]= - static_cast<T>(62)- z[39];
    z[42]=z[42]*z[17];
    z[43]=z[35]*z[4];
    z[44]= - 4*z[8] - 3*z[43];
    z[44]=z[44]*z[37];
    z[44]= - static_cast<T>(77)+ z[44];
    z[44]=z[4]*z[44];
    z[36]= - z[36]*z[37];
    z[36]= - static_cast<T>(41)+ z[36];
    z[45]=3*z[6];
    z[36]=z[36]*z[45];
    z[32]=z[32] + z[36] + z[42] + z[44];
    z[32]=z[9]*z[32];
    z[36]=37*z[8] + 59*z[43];
    z[36]=z[36]*z[21];
    z[36]=static_cast<T>(1729)+ z[36];
    z[36]=z[4]*z[36];
    z[42]= - z[8] - z[43];
    z[42]=z[6]*z[42];
    z[42]=z[19] + z[42];
    z[44]=n<T>(21,2)*z[6];
    z[42]=z[42]*z[44];
    z[36]=z[42] - n<T>(1911,2)*z[10] + z[36];
    z[42]=n<T>(1,2)*z[6];
    z[36]=z[36]*z[42];
    z[46]=npow(z[12],2);
    z[47]= - 55*z[20] - 13*z[46];
    z[48]=2*z[5];
    z[49]=z[29]*z[48];
    z[50]=z[11]*z[46];
    z[50]=559*z[12] + 61*z[50];
    z[49]=n<T>(1,2)*z[50] + z[49];
    z[49]=z[5]*z[49];
    z[50]=3*z[3];
    z[51]= - z[31]*z[50];
    z[51]=z[51] - z[29];
    z[17]=z[51]*z[17];
    z[51]= - 3*z[11] - z[28];
    z[51]=z[5]*z[51];
    z[51]= - n<T>(1263,2) + 4*z[51];
    z[51]=z[5]*z[51];
    z[17]=z[17] + 417*z[12] + z[51];
    z[17]=z[3]*z[17];
    z[51]=9*z[8];
    z[52]= - z[20]*z[51];
    z[53]=static_cast<T>(4)+ n<T>(59,8)*z[19];
    z[53]=z[4]*z[53];
    z[52]=z[53] - n<T>(1253,4)*z[10] + z[52];
    z[52]=z[4]*z[52];
    z[17]=z[32] + z[36] + z[52] + z[17] + n<T>(1,2)*z[47] + z[49];
    z[17]=z[9]*z[17];
    z[32]= - static_cast<T>(2)- z[27];
    z[32]=z[32]*z[48];
    z[32]=n<T>(923,4)*z[12] + z[32];
    z[32]=z[5]*z[32];
    z[36]= - z[27]*z[50];
    z[36]=z[5] + z[36];
    z[36]=z[3]*z[36];
    z[32]=z[32] + z[36];
    z[32]=z[3]*z[32];
    z[36]= - static_cast<T>(5)- 27*z[19];
    z[36]=z[36]*z[21];
    z[36]= - 829*z[10] + z[36];
    z[36]=z[4]*z[36];
    z[47]=static_cast<T>(1)+ z[19];
    z[47]=z[6]*z[47];
    z[47]= - z[4] + z[47];
    z[44]=z[47]*z[44];
    z[36]=z[36] + z[44];
    z[36]=z[6]*z[36];
    z[44]=2*z[8];
    z[47]=z[44] + z[43];
    z[47]=z[47]*z[37];
    z[47]=static_cast<T>(77)+ z[47];
    z[47]=z[6]*z[47]*z[37];
    z[49]=3*z[35];
    z[52]= - z[49] - z[34];
    z[52]=z[52]*z[37];
    z[52]= - 81*z[8] + z[52];
    z[52]=z[4]*z[52];
    z[52]= - static_cast<T>(77)+ z[52];
    z[52]=z[6]*z[52];
    z[39]= - z[3]*z[39];
    z[39]=z[39] + z[52];
    z[39]=z[9]*z[39];
    z[52]=z[3]*z[5];
    z[39]=z[39] - 75*z[52] + z[47];
    z[39]=z[9]*z[39];
    z[47]=npow(z[12],3);
    z[53]=15*z[47];
    z[54]=z[53]*z[11];
    z[55]= - n<T>(11,2)*z[46] - z[54];
    z[55]=z[5]*z[55];
    z[51]=z[23]*z[51];
    z[51]= - 41*z[20] + z[51];
    z[51]=z[51]*z[21];
    z[24]=z[39] + n<T>(1,4)*z[36] + z[51] + z[32] + z[24] + z[55];
    z[24]=z[9]*z[24];
    z[32]=n<T>(11,4)*z[13];
    z[36]=z[32] - z[48];
    z[36]=z[5]*z[36];
    z[39]=z[22] - 1;
    z[51]=z[39]*z[21];
    z[51]=z[51] - z[5] + n<T>(3,2)*z[3];
    z[55]=n<T>(3,4)*z[4];
    z[51]=z[51]*z[55];
    z[56]= - static_cast<T>(1)- n<T>(1,2)*z[22];
    z[56]=z[3]*z[56];
    z[56]=n<T>(77,2)*z[5] + 5*z[56];
    z[56]=z[3]*z[56];
    z[36]=z[51] + z[36] + n<T>(1,4)*z[56];
    z[36]=z[4]*z[36];
    z[39]=z[4]*z[39];
    z[39]=z[39] - z[5] + z[30];
    z[39]=z[39]*z[42];
    z[42]=z[4]*z[3];
    z[39]=z[39] + z[52] + z[42];
    z[42]=n<T>(5,4)*z[6];
    z[39]=z[39]*z[42];
    z[51]=npow(z[5],2);
    z[56]=static_cast<T>(6)+ z[27];
    z[56]=z[56]*z[51];
    z[57]= - z[5] - n<T>(1,2)*z[30];
    z[57]=z[3]*z[57];
    z[56]=z[56] + n<T>(5,4)*z[57];
    z[56]=z[3]*z[56];
    z[57]=npow(z[5],3);
    z[36]=z[39] + z[36] - z[57] + z[56];
    z[36]=z[6]*z[36];
    z[39]= - z[5] - z[3];
    z[39]=z[3]*z[39];
    z[39]=4*z[51] + n<T>(5,8)*z[39];
    z[39]=z[3]*z[39];
    z[51]=z[3] - z[5];
    z[56]=n<T>(1,4)*z[4];
    z[58]=z[51]*z[56];
    z[58]=z[52] + z[58];
    z[59]=n<T>(3,2)*z[4];
    z[58]=z[58]*z[59];
    z[60]=npow(z[3],2);
    z[61]=z[60]*z[5];
    z[62]= - z[57] + n<T>(5,8)*z[61];
    z[62]=z[62]*z[3];
    z[63]=z[52]*npow(z[4],2);
    z[62]=z[62] - n<T>(3,8)*z[63];
    z[63]=npow(z[6],2)*z[52];
    z[63]= - z[62] + n<T>(5,8)*z[63];
    z[63]=z[1]*z[63];
    z[39]=z[63] + z[58] - z[57] + z[39];
    z[39]=z[4]*z[39];
    z[58]=z[52]*z[21];
    z[51]=z[51]*z[21];
    z[51]=z[52] + z[51];
    z[51]=z[6]*z[51];
    z[51]=z[58] + z[51];
    z[42]=z[51]*z[42];
    z[51]=2*z[57] - n<T>(5,4)*z[61];
    z[51]=z[3]*z[51];
    z[39]=z[42] + z[51] + z[39];
    z[39]=z[6]*z[39];
    z[42]=z[4]*z[62];
    z[39]=z[42] + z[39];
    z[39]=z[1]*z[39];
    z[42]=z[32]*z[12];
    z[51]=n<T>(11,4)*z[12] - z[48];
    z[51]=z[5]*z[51];
    z[51]=z[42] + z[51];
    z[51]=z[3]*z[51];
    z[57]= - z[52]*z[55];
    z[42]=z[5]*z[42];
    z[42]=z[57] + z[42] + z[51];
    z[42]=z[4]*z[42];
    z[51]=npow(z[3],3);
    z[51]=z[53] + n<T>(17,4)*z[51];
    z[51]=z[5]*z[51];
    z[24]=z[39] + z[24] + z[36] + z[42] + z[51];
    z[24]=z[1]*z[24];
    z[36]= - z[7] - 17*z[8];
    z[36]=z[36]*z[59];
    z[36]=z[36] + n<T>(73,2) + 3*z[22];
    z[36]=z[36]*z[56];
    z[39]=5*z[22];
    z[42]=static_cast<T>(33)- z[39];
    z[42]=z[3]*z[42];
    z[36]=z[36] + n<T>(1,8)*z[42] - n<T>(11,8)*z[5] + z[32] - 291*z[10];
    z[36]=z[4]*z[36];
    z[39]= - static_cast<T>(37)+ z[39];
    z[39]=z[4]*z[39];
    z[42]= - 5*z[7] + 47*z[8];
    z[42]=z[4]*z[42];
    z[42]=static_cast<T>(21)+ z[42];
    z[42]=z[6]*z[42];
    z[39]=z[42] + 5*z[30] + z[39];
    z[39]=z[6]*z[39];
    z[42]=2*z[27];
    z[51]=n<T>(15,2) + z[42];
    z[51]=z[5]*z[51];
    z[30]=z[51] - n<T>(5,8)*z[30];
    z[30]=z[3]*z[30];
    z[51]=z[32] - z[5];
    z[51]=z[5]*z[51];
    z[30]=n<T>(1,8)*z[39] + z[36] + z[30] + n<T>(41,2)*z[20] + z[51];
    z[30]=z[6]*z[30];
    z[36]=static_cast<T>(29)+ 53*z[27];
    z[36]=z[36]*z[60];
    z[39]=n<T>(1,2)*z[13] + z[12];
    z[39]=z[12]*z[39];
    z[27]=static_cast<T>(2)+ 3*z[27];
    z[27]=z[5]*z[27];
    z[27]=n<T>(1263,4)*z[12] + z[27];
    z[27]=z[5]*z[27];
    z[27]=n<T>(1,8)*z[36] + n<T>(11,2)*z[39] + z[27];
    z[27]=z[3]*z[27];
    z[36]=z[3] - 9*z[4];
    z[36]=z[36]*z[55];
    z[39]=z[13] - n<T>(1,2)*z[5];
    z[39]=11*z[39] - n<T>(5,2)*z[3];
    z[39]=z[3]*z[39];
    z[36]=z[36] + 5*z[20] + n<T>(1,2)*z[39];
    z[21]=z[36]*z[21];
    z[23]=3*z[23] + 10*z[47];
    z[32]=z[32] - 14*z[12];
    z[32]=z[5]*z[12]*z[32];
    z[17]=z[24] + z[17] + z[30] + z[21] + z[27] + 3*z[23] + z[32];
    z[17]=z[1]*z[17];
    z[21]=z[41] + 2*z[33];
    z[21]=z[21]*z[37];
    z[23]=77*z[35];
    z[21]=z[21] + z[23];
    z[21]=z[21]*z[4];
    z[24]=z[8]*z[11];
    z[24]=z[24] - z[25];
    z[27]=npow(z[11],3);
    z[30]=z[27]*z[5];
    z[32]=z[24] - z[30];
    z[32]=z[32]*z[3];
    z[31]=z[31] - z[8];
    z[36]=z[32] + z[31];
    z[39]= - z[4]*npow(z[8],5);
    z[39]= - 3*z[40] + z[39];
    z[39]=z[39]*z[37];
    z[39]= - 81*z[33] + z[39];
    z[39]=z[4]*z[39];
    z[23]= - z[23] + z[39];
    z[23]=z[6]*z[23];
    z[23]=z[23] + 75*z[36] - z[21];
    z[23]=z[9]*z[23];
    z[33]=z[33]*z[37];
    z[33]=z[49] + z[33];
    z[33]=z[33]*z[37];
    z[33]=z[38] + z[33];
    z[33]=z[33]*z[37];
    z[21]=123*z[8] + 4*z[21];
    z[21]=z[6]*z[21];
    z[36]= - 75*z[28] - 150*z[11] + 49*z[8];
    z[36]=z[3]*z[36];
    z[21]=z[23] + z[21] + z[33] + static_cast<T>(75)+ z[36];
    z[21]=z[9]*z[21];
    z[23]= - 4*z[25] - z[30];
    z[23]=z[5]*z[23];
    z[23]= - 353*z[11] + z[23];
    z[23]=z[23]*z[48];
    z[30]=z[32] - z[31];
    z[30]=z[30]*z[50];
    z[31]=z[8]*z[12];
    z[31]= - static_cast<T>(2425)- 713*z[31];
    z[23]=z[30] + n<T>(1,4)*z[31] + z[23];
    z[23]=z[3]*z[23];
    z[30]= - 85*z[35] - 107*z[34];
    z[30]=z[4]*z[30];
    z[30]= - 1027*z[8] + n<T>(1,8)*z[30];
    z[30]=z[4]*z[30];
    z[31]=z[35] + z[34];
    z[31]=z[6]*z[31];
    z[31]= - z[43] + z[31];
    z[31]=z[6]*z[31];
    z[32]=z[11]*z[10];
    z[32]=static_cast<T>(137)- n<T>(1087,2)*z[32];
    z[30]=n<T>(21,8)*z[31] + n<T>(1,2)*z[32] + z[30];
    z[30]=z[6]*z[30];
    z[31]=2*z[11];
    z[32]=z[31] + z[28];
    z[32]=z[32]*z[48];
    z[33]=z[11]*z[12];
    z[36]=static_cast<T>(683)+ 709*z[33];
    z[36]=n<T>(1,4)*z[36] + z[32];
    z[36]=z[5]*z[36];
    z[38]=z[8]*z[10];
    z[38]= - static_cast<T>(829)+ 1095*z[38];
    z[39]= - 12*z[8] - n<T>(107,8)*z[43];
    z[39]=z[4]*z[39];
    z[38]=n<T>(1,4)*z[38] + z[39];
    z[38]=z[4]*z[38];
    z[39]= - 437*z[10] + 163*z[12];
    z[21]=z[21] + z[30] + z[38] + z[23] + n<T>(1,4)*z[39] + z[36];
    z[21]=z[9]*z[21];
    z[23]=n<T>(1,4)*z[13] - 3*z[12];
    z[23]=z[23]*z[33];
    z[30]= - n<T>(13,2)*z[46] + z[54];
    z[30]=z[8]*z[30];
    z[36]=static_cast<T>(629)+ n<T>(1937,2)*z[33];
    z[32]=n<T>(1,4)*z[36] + z[32];
    z[32]=z[5]*z[32];
    z[36]=z[52]*z[25];
    z[38]= - static_cast<T>(1)+ z[36];
    z[38]=z[38]*z[50];
    z[23]=z[38] + z[32] + z[30] + n<T>(11,2)*z[23] + z[26] + 273*z[12];
    z[23]=z[3]*z[23];
    z[16]=z[22]*z[16];
    z[16]= - 1975*z[10] + z[16];
    z[18]= - z[18]*z[56];
    z[18]=static_cast<T>(4)+ z[18];
    z[18]=z[4]*z[18];
    z[16]=n<T>(1,4)*z[16] + z[18];
    z[16]=z[4]*z[16];
    z[18]=z[12]*z[14];
    z[18]= - n<T>(11,2) - 15*z[18];
    z[18]=z[18]*z[46];
    z[22]=z[33]*z[26];
    z[22]=7*z[12] + z[22];
    z[22]=z[5]*z[22];
    z[26]= - z[8]*z[53];
    z[15]=z[17] + z[21] + z[15] + z[16] + z[23] + z[22] + z[26] + z[54]
    - n<T>(9,2)*z[20] + z[18];
    z[15]=z[1]*z[15];
    z[16]= - z[25]*z[48];
    z[16]=z[16] - z[31] + z[8];
    z[16]=z[3]*z[16];
    z[17]=z[35]*z[37];
    z[17]=z[8] + z[17];
    z[17]=z[6]*z[17];
    z[16]=z[16] + z[17];
    z[16]=5*z[19] + 7*z[29] + 6*z[16];
    z[16]=z[9]*z[16];
    z[17]= - static_cast<T>(1)+ z[42];
    z[17]=z[17]*z[50];
    z[18]=z[7] - 3*z[8];
    z[18]=z[18]*z[37];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[45];
    z[16]=z[16] + z[18] + z[17] - 5*z[4];
    z[15]=25*z[16] + z[15];
    z[15]=z[1]*z[15];
    z[16]=z[3]*z[24];
    z[17]= - z[6]*z[35];
    z[16]=z[17] - z[43] + z[28] + z[16];
    z[16]=z[9]*z[16];
    z[17]=z[44] - z[7];
    z[18]=z[6]*z[17]*z[19];
    z[16]=z[16] + z[36] + z[18];
    z[16]=z[16]*npow(z[1],2);
    z[18]= - z[27]*z[52];
    z[19]= - z[6]*z[34];
    z[18]=z[18] + z[19];
    z[18]=z[2]*z[9]*z[18]*npow(z[1],3);
    z[16]=z[16] + z[18];
    z[18]=npow(z[2],2);
    z[16]=z[16]*z[18];
    z[17]=z[4]*z[17];
    z[19]=z[6]*z[8];
    z[20]=z[11] - z[8];
    z[20]=z[9]*z[20];
    z[16]=z[16] + z[20] + z[19] + static_cast<T>(1)+ z[17];
    z[15]=z[15] + 75*z[16];

    r += z[18]*z[15];
 
    return r;
}

template double qg_2lNLC_r831(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r831(const std::array<dd_real,31>&);
#endif
