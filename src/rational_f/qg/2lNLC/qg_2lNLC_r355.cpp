#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r355(const std::array<T,31>& k) {
  T z[61];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[12];
    z[4]=k[15];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[6];
    z[8]=k[2];
    z[9]=k[14];
    z[10]=k[13];
    z[11]=k[9];
    z[12]=k[10];
    z[13]=k[3];
    z[14]=k[7];
    z[15]=npow(z[7],3);
    z[16]=npow(z[5],2);
    z[17]=z[15]*z[16];
    z[18]=n<T>(1,4)*z[14];
    z[19]= - z[10] + z[18];
    z[19]=z[19]*z[17];
    z[20]=z[6]*z[5];
    z[21]=z[15]*z[20];
    z[22]=z[10]*z[21];
    z[19]=z[19] + z[22];
    z[19]=z[6]*z[19];
    z[22]=z[6]*z[4];
    z[23]=z[9]*z[8];
    z[24]=z[23] - n<T>(5,2);
    z[25]=npow(z[8],2);
    z[26]= - z[9]*z[25];
    z[26]= - z[8] + z[26];
    z[26]=z[4]*z[26];
    z[26]= - z[22] + z[26] - z[24];
    z[26]=z[2]*z[26];
    z[27]=n<T>(1,2)*z[9];
    z[28]=z[27] + z[11];
    z[29]=n<T>(1,2)*z[4];
    z[30]=z[8]*z[11];
    z[31]=static_cast<T>(3)+ z[30];
    z[31]=z[12]*z[31];
    z[19]=z[26] + z[19] - z[29] + z[31] - z[28];
    z[19]=z[2]*z[19];
    z[26]=n<T>(1,2)*z[12];
    z[31]=z[11]*z[26];
    z[19]=z[31] + z[19];
    z[31]=n<T>(1,2)*z[2];
    z[19]=z[19]*z[31];
    z[32]=z[6]*z[14];
    z[33]=z[32] - 1;
    z[34]=z[33]*z[6];
    z[35]=z[34] + z[5];
    z[36]=n<T>(1,2)*z[6];
    z[37]=z[35]*z[36];
    z[38]=z[5]*z[12];
    z[39]= - n<T>(1,2) + z[38];
    z[39]=z[39]*z[16];
    z[39]=z[39] + z[37];
    z[39]=z[2]*z[39];
    z[40]=z[38] + 1;
    z[41]=z[5]*z[40];
    z[34]=z[41] + z[34];
    z[34]=n<T>(1,2)*z[34] + z[39];
    z[34]=z[2]*z[34];
    z[27]=z[38]*z[27];
    z[27]=z[27] - n<T>(1,2)*z[10] + z[9];
    z[27]=z[5]*z[27];
    z[33]= - z[10]*z[33];
    z[33]=z[9] + z[33];
    z[33]=z[33]*z[36];
    z[27]=z[34] + z[27] + z[33];
    z[33]=n<T>(1,2)*z[3];
    z[27]=z[27]*z[33];
    z[34]=z[29] - z[9];
    z[34]= - z[34]*z[40];
    z[39]= - z[15]*z[32];
    z[15]=z[15]*z[14];
    z[41]=z[13]*z[15];
    z[39]=z[41] + z[39];
    z[39]=z[39]*z[36];
    z[41]=z[14]*z[10];
    z[39]=z[41] + z[39];
    z[39]=z[6]*z[39];
    z[42]=n<T>(3,2)*z[10];
    z[34]=z[39] - z[42] + z[34];
    z[39]=z[29] + z[14];
    z[43]=z[39]*z[6];
    z[44]=n<T>(5,2) - z[43];
    z[44]=z[44]*z[36];
    z[45]=n<T>(1,2)*z[16];
    z[46]=z[5] + z[36];
    z[46]=z[6]*z[46];
    z[46]=z[45] + z[46];
    z[46]=z[46]*z[31];
    z[47]= - n<T>(3,4) + 2*z[38];
    z[47]=z[5]*z[47];
    z[44]=z[46] + z[47] + z[44];
    z[44]=z[2]*z[44];
    z[46]= - z[14] + n<T>(7,4)*z[21];
    z[46]=z[6]*z[46];
    z[40]=n<T>(3,2)*z[40] + z[46];
    z[40]=n<T>(1,2)*z[40] + z[44];
    z[40]=z[2]*z[40];
    z[27]=z[27] + n<T>(1,2)*z[34] + z[40];
    z[27]=z[3]*z[27];
    z[34]=z[12]*z[9];
    z[40]=z[12] - z[11];
    z[40]=z[4]*z[40];
    z[44]=z[5]*z[4];
    z[46]=npow(z[12],2)*z[44];
    z[34]=z[46] + z[40] - n<T>(3,2)*z[41] + z[34];
    z[17]= - z[21] - z[4] + z[17];
    z[17]=z[17]*z[36];
    z[21]=n<T>(7,2) - z[22];
    z[21]=z[6]*z[21];
    z[21]=n<T>(5,2)*z[5] + z[21];
    z[21]=z[21]*z[31];
    z[40]=3*z[38];
    z[17]=z[21] + z[17] - n<T>(1,4) + z[40];
    z[17]=z[2]*z[17];
    z[21]=n<T>(1,2)*z[14];
    z[46]=z[21] + z[12];
    z[17]=n<T>(3,4)*z[46] + z[17];
    z[17]=z[2]*z[17];
    z[17]=z[27] + n<T>(1,4)*z[34] + z[17];
    z[17]=z[3]*z[17];
    z[27]=z[16]*npow(z[7],4);
    z[34]= - z[36]*z[27];
    z[34]= - 7*z[4] + z[34];
    z[34]=z[6]*z[34];
    z[34]=static_cast<T>(5)+ z[34];
    z[34]=z[2]*z[34];
    z[34]=3*z[4] + z[34];
    z[46]=npow(z[2],2);
    z[34]=z[34]*z[46];
    z[47]=n<T>(1,2)*z[5];
    z[48]=z[36]*z[4];
    z[49]=static_cast<T>(1)- z[48];
    z[49]=z[6]*z[49];
    z[49]=z[47] + z[49];
    z[49]=z[2]*z[49];
    z[27]=z[27]*npow(z[6],2);
    z[49]=n<T>(5,4)*z[27] + z[49];
    z[46]=z[46]*z[3];
    z[49]=z[49]*z[46];
    z[34]=n<T>(1,2)*z[34] + z[49];
    z[34]=z[34]*z[33];
    z[49]=npow(z[5],3);
    z[50]=n<T>(1,4)*z[49];
    z[51]=z[6]*npow(z[7],5)*z[50];
    z[51]= - z[4] + z[51];
    z[51]=z[6]*z[51];
    z[51]=n<T>(1,2) + z[51];
    z[51]=z[2]*z[51];
    z[51]=z[29] + z[51];
    z[46]=z[51]*z[46];
    z[51]=npow(z[2],3);
    z[52]=z[4]*z[51];
    z[46]= - n<T>(5,2)*z[52] + z[46];
    z[46]=z[3]*z[46];
    z[52]= - z[1]*z[51]*npow(z[3],2)*z[29];
    z[46]=z[46] + z[52];
    z[46]=z[1]*z[46];
    z[52]= - static_cast<T>(1)- n<T>(1,4)*z[23];
    z[52]=z[4]*z[52];
    z[27]=z[10]*z[27];
    z[27]=n<T>(1,8)*z[27] - n<T>(1,4)*z[9] + z[52];
    z[27]=z[27]*z[51];
    z[27]=n<T>(1,2)*z[46] + z[27] + z[34];
    z[27]=z[1]*z[27];
    z[15]=z[49]*z[15];
    z[34]=n<T>(5,4)*z[14];
    z[15]=n<T>(3,8)*z[15] - z[34];
    z[46]=z[4]*z[12];
    z[15]=z[46]*z[15];
    z[51]=z[12]*z[14];
    z[52]=z[9]*z[51];
    z[15]=z[27] + z[17] + z[19] + z[52] + z[15];
    z[15]=z[1]*z[15];
    z[17]=z[25]*z[46];
    z[19]=z[12]*z[8];
    z[17]=z[17] - static_cast<T>(1)+ n<T>(9,2)*z[19];
    z[17]=z[17]*z[29];
    z[27]=npow(z[7],2);
    z[52]=z[27]*z[10];
    z[53]=z[27]*z[14];
    z[54]=n<T>(3,4)*z[52] - z[53];
    z[54]=z[54]*z[16];
    z[55]= - z[27]*z[21];
    z[55]= - 7*z[52] + z[55];
    z[55]=z[5]*z[55];
    z[52]=z[6]*z[52];
    z[52]=z[55] + n<T>(9,2)*z[52];
    z[52]=z[52]*z[36];
    z[17]=z[52] + z[54] + z[17] + z[12] + z[34] + n<T>(7,4)*z[10] - z[28];
    z[28]=z[18] - z[4];
    z[52]=z[27]*z[5];
    z[54]= - n<T>(1,4)*z[52] + z[28];
    z[54]=z[6]*z[54];
    z[55]=z[25]*z[12];
    z[56]= - z[8] + n<T>(1,4)*z[55];
    z[56]=z[4]*z[56];
    z[24]=z[54] + z[56] + n<T>(1,4)*z[19] - z[24];
    z[54]=z[36] + z[8] + z[47];
    z[54]=z[2]*z[54];
    z[24]=n<T>(1,2)*z[24] + z[54];
    z[24]=z[2]*z[24];
    z[17]=n<T>(1,2)*z[17] + z[24];
    z[17]=z[2]*z[17];
    z[24]=z[38] + n<T>(7,2);
    z[24]=z[24]*z[45];
    z[35]=z[35]*z[6];
    z[35]=z[35] - z[16];
    z[35]=z[35]*z[36];
    z[35]=z[35] + z[49];
    z[54]=z[2]*z[35];
    z[54]=z[24] + z[54];
    z[54]=z[2]*z[54];
    z[24]=z[9]*z[24];
    z[56]=z[6]*z[9];
    z[57]=2*z[9];
    z[58]=z[5]*z[57];
    z[58]=n<T>(3,4)*z[56] - n<T>(1,4) + z[58];
    z[58]=z[6]*z[58];
    z[24]=z[54] + z[24] + z[58];
    z[24]=z[3]*z[24];
    z[54]= - z[11]*z[29];
    z[54]=z[54] + n<T>(5,4)*z[27] + z[41];
    z[54]=z[6]*z[54];
    z[54]=z[54] - z[29] - z[21] - z[10] + n<T>(9,2)*z[9];
    z[54]=z[6]*z[54];
    z[58]=3*z[9];
    z[59]=z[58]*z[38];
    z[60]= - z[10] + 5*z[9];
    z[60]=3*z[60] - z[4];
    z[60]=n<T>(1,2)*z[60] + z[59];
    z[60]=z[5]*z[60];
    z[54]=z[54] + n<T>(1,2) + z[60];
    z[60]=z[5]*z[36];
    z[60]=z[16] + z[60];
    z[60]=z[6]*z[60];
    z[50]=z[50] + z[60];
    z[50]=z[2]*z[50];
    z[60]=static_cast<T>(3)- z[32];
    z[60]=z[6]*z[60];
    z[60]= - z[5] + z[60];
    z[60]=z[6]*z[60];
    z[50]=z[50] + n<T>(13,2)*z[16] + z[60];
    z[50]=z[50]*z[31];
    z[39]= - z[39]*z[36];
    z[39]=static_cast<T>(1)+ z[39];
    z[39]=z[6]*z[39];
    z[38]=n<T>(11,4) + z[38];
    z[38]=z[5]*z[38];
    z[38]=z[50] + n<T>(3,2)*z[38] + z[39];
    z[38]=z[2]*z[38];
    z[24]=z[24] + n<T>(1,2)*z[54] + z[38];
    z[24]=z[3]*z[24];
    z[38]=z[27]*z[13];
    z[39]=z[10] + 3*z[38];
    z[39]=z[39]*z[21];
    z[50]=z[4]*z[11];
    z[39]=z[39] - z[50];
    z[39]=z[6]*z[39];
    z[50]= - z[10] + z[9];
    z[50]= - 3*z[12] + 9*z[50] + z[14];
    z[39]=z[39] + z[59] + n<T>(1,2)*z[50] - z[4];
    z[50]=z[6]*z[27];
    z[28]= - n<T>(9,8)*z[50] + n<T>(1,2)*z[28] + z[52];
    z[28]=z[6]*z[28];
    z[50]=n<T>(7,2)*z[5] + z[6];
    z[50]=z[6]*z[50];
    z[50]=n<T>(5,4)*z[16] + z[50];
    z[50]=z[50]*z[31];
    z[43]=n<T>(9,4) - z[43];
    z[43]=z[6]*z[43];
    z[43]=z[50] + n<T>(11,4)*z[5] + z[43];
    z[43]=z[2]*z[43];
    z[40]=n<T>(17,4) + z[40];
    z[28]=z[43] + n<T>(1,2)*z[40] + z[28];
    z[28]=z[2]*z[28];
    z[24]=z[24] + n<T>(1,2)*z[39] + z[28];
    z[24]=z[3]*z[24];
    z[25]=z[25]*z[27];
    z[28]= - n<T>(5,4) - 2*z[25];
    z[28]=z[9]*z[28];
    z[25]= - static_cast<T>(1)+ n<T>(11,8)*z[25];
    z[25]=z[12]*z[25];
    z[39]=n<T>(1,4)*z[4];
    z[25]=z[39] + z[25] + z[21] - n<T>(3,4)*z[11] + z[28];
    z[25]=z[4]*z[25];
    z[28]=z[27]*z[51];
    z[40]=z[12]*z[27];
    z[40]= - z[53] + n<T>(7,4)*z[40];
    z[40]=z[4]*z[40];
    z[28]=n<T>(3,2)*z[28] + z[40];
    z[28]=z[28]*z[47];
    z[38]= - n<T>(1,4)*z[38] - z[9];
    z[38]=z[38]*z[51];
    z[40]=z[8]*z[27];
    z[40]= - n<T>(7,8)*z[40] + z[14];
    z[40]=3*z[40] + z[26];
    z[40]=z[12]*z[40];
    z[40]=z[40] + n<T>(1,4)*z[46];
    z[40]=z[4]*z[40];
    z[28]=z[28] + z[38] + z[40];
    z[28]=z[5]*z[28];
    z[38]=2*z[23];
    z[27]= - z[27]*z[38];
    z[40]=n<T>(1,8)*z[14];
    z[43]= - z[52]*z[40];
    z[27]=z[27] + z[43];
    z[27]=z[27]*z[22];
    z[43]=15*z[9] + z[12];
    z[43]=z[12]*z[43];
    z[41]= - z[41] + z[43];
    z[15]=z[15] + z[24] + z[17] + z[27] + z[28] + n<T>(1,4)*z[41] + z[25];
    z[15]=z[1]*z[15];
    z[17]= - z[14] + n<T>(5,2)*z[10];
    z[24]=z[5]*z[17];
    z[24]=n<T>(9,2) + z[24];
    z[24]=n<T>(1,2)*z[24] - z[32];
    z[24]=z[6]*z[24];
    z[25]=3*z[8] + z[5];
    z[27]=z[2]*z[20];
    z[24]=z[27] + n<T>(1,2)*z[25] + z[24];
    z[24]=z[24]*z[31];
    z[25]=n<T>(1,2)*z[30];
    z[27]= - static_cast<T>(1)+ z[25];
    z[27]=z[27]*z[19];
    z[28]=z[8] - 5*z[55];
    z[28]=z[28]*z[39];
    z[32]=n<T>(1,4)*z[5];
    z[39]= - z[12] - n<T>(9,2)*z[10] + z[14];
    z[32]=z[39]*z[32];
    z[39]=13*z[10] + 3*z[14];
    z[39]=n<T>(1,4)*z[39] - z[4];
    z[39]=z[39]*z[36];
    z[24]=z[24] + z[39] + z[32] + z[28] + z[27] + static_cast<T>(3)- n<T>(1,2)*z[23];
    z[24]=z[2]*z[24];
    z[27]=z[21]*z[13];
    z[25]=n<T>(1,2)*z[19] - z[27] + 5*z[23] - static_cast<T>(5)+ z[25];
    z[25]=z[12]*z[25];
    z[28]=z[8] - z[55];
    z[28]=z[28]*z[29];
    z[23]=z[28] - n<T>(5,4)*z[19] - 7*z[23] + n<T>(13,2) - z[30];
    z[23]=z[4]*z[23];
    z[23]=z[23] + z[25] + n<T>(3,2)*z[14] + z[9] + z[11] - n<T>(27,4)*z[10];
    z[25]=z[16]*z[58];
    z[28]=z[5]*z[58];
    z[28]=z[56] - static_cast<T>(1)+ z[28];
    z[28]=z[6]*z[28];
    z[25]=z[25] + z[28];
    z[25]=z[6]*z[25];
    z[28]=3*z[16] - z[37];
    z[28]=z[6]*z[28];
    z[30]=z[2]*z[6];
    z[32]=z[35]*z[30];
    z[28]=z[32] + z[49] + z[28];
    z[28]=z[2]*z[28];
    z[32]=z[9]*z[49];
    z[25]=z[28] + z[32] + z[25];
    z[25]=z[25]*z[33];
    z[21]= - z[21] + n<T>(1,4)*z[11] + z[57];
    z[21]=z[6]*z[21];
    z[28]=z[14]*z[13];
    z[28]=n<T>(1,2) + z[28];
    z[32]=z[5]*z[9];
    z[21]=z[21] + n<T>(1,4)*z[28] + 4*z[32];
    z[21]=z[6]*z[21];
    z[28]=z[5] + z[6];
    z[28]=z[6]*z[28];
    z[28]=n<T>(15,2)*z[16] + z[28];
    z[28]=z[6]*z[28];
    z[32]=z[6]*z[16];
    z[32]=z[49] + z[32];
    z[32]=z[32]*z[30];
    z[28]=z[28] + z[32];
    z[28]=z[2]*z[28];
    z[32]=z[36]*z[14];
    z[32]=z[32] - 1;
    z[32]=z[32]*z[6];
    z[33]=n<T>(13,4)*z[5] - z[32];
    z[33]=z[6]*z[33];
    z[28]=n<T>(1,4)*z[28] + 2*z[16] + z[33];
    z[28]=z[2]*z[28];
    z[33]=z[16]*z[57];
    z[21]=z[25] + z[28] + z[33] + z[21];
    z[21]=z[3]*z[21];
    z[25]=3*z[11] - 7*z[10];
    z[28]= - z[11]*z[48];
    z[18]=z[28] + z[18] + n<T>(1,4)*z[25] + z[58];
    z[18]=z[6]*z[18];
    z[16]=z[16] + z[20];
    z[16]=z[16]*z[30];
    z[20]=n<T>(7,4)*z[5] - 3*z[32];
    z[20]=z[6]*z[20];
    z[16]=z[16] + z[45] + z[20];
    z[16]=z[2]*z[16];
    z[20]=n<T>(17,4) - z[22];
    z[20]=z[6]*z[20];
    z[16]=z[16] + n<T>(29,4)*z[5] + z[20];
    z[16]=z[16]*z[31];
    z[20]= - n<T>(1,4)*z[10] + z[9];
    z[20]=3*z[20] + z[29];
    z[20]=z[5]*z[20];
    z[16]=z[21] + z[16] + z[18] - n<T>(3,8) + z[20];
    z[16]=z[3]*z[16];
    z[18]= - n<T>(1,4)*z[12] - z[57] - z[34];
    z[18]=z[12]*z[18];
    z[20]=static_cast<T>(5)+ z[19];
    z[20]=z[20]*z[26];
    z[19]= - n<T>(1,2) + z[19];
    z[19]=z[4]*z[19];
    z[19]=z[20] + z[19];
    z[19]=z[19]*z[29];
    z[20]= - n<T>(17,2)*z[14] - z[12];
    z[20]=z[12]*z[20];
    z[20]=z[20] - z[46];
    z[20]=z[20]*z[44];
    z[18]=n<T>(1,4)*z[20] + z[18] + z[19];
    z[18]=z[5]*z[18];
    z[19]=z[40] - n<T>(1,2)*z[11] - z[9];
    z[19]=z[19]*z[22];
    z[15]=z[15] + z[16] + z[24] + z[19] + n<T>(1,2)*z[23] + z[18];
    z[15]=z[1]*z[15];
    z[16]= - z[4] + z[26] + z[17];
    z[16]=z[16]*z[47];
    z[17]=n<T>(1,4)*z[6];
    z[18]= - n<T>(7,2)*z[4] - 9*z[10] + z[14];
    z[17]=z[18]*z[17];
    z[18]=z[13]*z[42];
    z[18]= - static_cast<T>(1)+ z[18];
    z[19]= - z[8] + n<T>(1,4)*z[13];
    z[19]=z[12]*z[19];
    z[20]=z[4]*z[8];
    z[21]= - n<T>(7,8)*z[6] + z[8] - z[47];
    z[21]=z[2]*z[21];
    z[22]=z[3]*z[36];

    r += z[15] + z[16] + z[17] + n<T>(3,4)*z[18] + z[19] + 4*z[20] + z[21]
       + z[22] + z[27] - z[38];
 
    return r;
}

template double qg_2lNLC_r355(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r355(const std::array<dd_real,31>&);
#endif
