#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1430(const std::array<T,31>& k) {
  T z[64];
  T r = 0;

    z[1]=k[1];
    z[2]=k[12];
    z[3]=k[13];
    z[4]=k[14];
    z[5]=k[5];
    z[6]=k[2];
    z[7]=k[6];
    z[8]=k[15];
    z[9]=k[4];
    z[10]=k[8];
    z[11]=k[10];
    z[12]=k[3];
    z[13]=k[7];
    z[14]=2*z[8];
    z[15]=z[14] - z[2];
    z[16]=2*z[4];
    z[17]=z[15]*z[16];
    z[18]=z[2]*z[13];
    z[19]=8*z[18];
    z[20]=npow(z[7],2);
    z[17]=z[17] + z[19] - 9*z[20];
    z[17]=z[5]*z[17];
    z[21]=2*z[9];
    z[22]=z[20]*z[21];
    z[23]=z[20]*z[12];
    z[24]= - z[2] + 6*z[23];
    z[22]=z[4] + 3*z[24] + z[22];
    z[17]=2*z[22] + z[17];
    z[17]=z[5]*z[17];
    z[22]=npow(z[12],2);
    z[24]=z[22]*z[20];
    z[25]=z[20]*z[9];
    z[26]=3*z[25] - z[23] + 5*z[10] + 6*z[11];
    z[26]=z[26]*z[21];
    z[27]=z[16]*z[9];
    z[28]=3*z[10];
    z[29]= - z[28] - 2*z[11];
    z[29]=z[9]*z[29];
    z[29]= - static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[27];
    z[30]=4*z[10];
    z[31]=3*z[2];
    z[32]= - z[30] - z[31];
    z[32]=z[9]*z[32];
    z[32]= - static_cast<T>(5)+ z[32];
    z[32]=z[9]*z[32];
    z[32]=5*z[12] + z[32];
    z[33]=2*z[3];
    z[32]=z[32]*z[33];
    z[34]=z[12]*z[11];
    z[35]=z[34] - 2;
    z[17]=z[32] + z[17] + z[29] + z[26] + 4*z[35] - 29*z[24];
    z[17]=z[3]*z[17];
    z[26]=4*z[2];
    z[29]=3*z[8];
    z[32]= - z[29] - z[26];
    z[36]=2*z[5];
    z[37]=npow(z[4],2);
    z[32]=z[32]*z[37]*z[36];
    z[38]=2*z[10];
    z[39]= - z[29] - z[38];
    z[39]=z[39]*z[25];
    z[40]=z[4] - z[2];
    z[40]=4*z[8] - 7*z[40];
    z[40]=z[40]*z[16];
    z[19]=z[32] + z[40] - z[19] + z[39];
    z[19]=z[5]*z[19];
    z[32]=4*z[11];
    z[39]=z[28] + z[32];
    z[39]=z[9]*z[39];
    z[40]=z[4]*z[9];
    z[41]=z[9]*z[11];
    z[42]=static_cast<T>(7)- z[41];
    z[42]=z[42]*z[40];
    z[39]=z[42] + static_cast<T>(2)+ z[39];
    z[39]=z[39]*z[16];
    z[42]=z[31] + z[10];
    z[43]=3*z[11];
    z[44]= - z[43] - z[42];
    z[24]=z[11]*z[24];
    z[45]=3*z[34] - 4*z[41];
    z[45]=z[45]*z[25];
    z[17]=z[17] + z[19] + z[39] + z[45] + 2*z[44] + z[24];
    z[17]=z[3]*z[17];
    z[19]=npow(z[6],2);
    z[24]=z[19]*z[8];
    z[39]=z[20]*z[24];
    z[44]=3*z[13];
    z[45]=z[44]*z[41];
    z[46]= - z[13] + z[11];
    z[46]=5*z[46] - z[45];
    z[46]=z[9]*z[46];
    z[47]=6*z[40];
    z[46]=z[47] - static_cast<T>(7)+ z[46];
    z[46]=z[4]*z[46];
    z[48]=5*z[11];
    z[49]=z[41]*z[13];
    z[50]=2*z[13];
    z[51]= - z[8] + z[50];
    z[39]=z[46] + 9*z[49] + z[39] - z[26] + 3*z[51] - z[48];
    z[39]=z[4]*z[39];
    z[46]=z[10]*npow(z[9],2);
    z[51]=npow(z[7],3);
    z[52]= - z[51]*z[46];
    z[53]= - z[31] + z[16];
    z[53]=z[4]*z[53];
    z[54]=z[37]*z[5];
    z[55]=5*z[2];
    z[56]=z[8] + z[55];
    z[56]=z[56]*z[54];
    z[52]=z[56] + z[52] + 3*z[53];
    z[52]=z[5]*z[52];
    z[53]=z[22]*z[51];
    z[56]=z[51]*z[9];
    z[57]= - z[12]*z[56];
    z[57]= - z[53] + z[57];
    z[58]=z[12]*z[51];
    z[56]=z[58] + z[56];
    z[56]=z[5]*z[56];
    z[56]=2*z[57] + z[56];
    z[57]=3*z[5];
    z[56]=z[56]*z[57];
    z[58]=z[9]*z[53];
    z[59]=z[7]*z[12];
    z[60]=npow(z[59],3);
    z[58]=z[60] + z[58];
    z[46]=z[16]*z[46];
    z[46]=z[56] + 3*z[58] + z[46];
    z[56]=3*z[3];
    z[46]=z[46]*z[56];
    z[58]= - z[51]*z[34]*z[21];
    z[53]= - z[11]*z[53];
    z[53]=z[53] + z[58];
    z[53]=z[9]*z[53];
    z[48]=z[10] + z[48];
    z[48]=z[9]*z[48];
    z[48]= - static_cast<T>(6)+ z[48];
    z[48]=z[48]*z[40];
    z[58]= - 9*z[10] - 10*z[11];
    z[58]=z[9]*z[58];
    z[48]=z[48] - static_cast<T>(2)+ z[58];
    z[48]=z[4]*z[48];
    z[58]=z[60] + 2;
    z[58]=z[11]*z[58];
    z[31]=z[46] + z[52] + z[48] + z[53] + z[31] + z[58];
    z[31]=z[3]*z[31];
    z[46]=z[11] + z[10];
    z[48]=2*z[2];
    z[52]=z[48] + z[46];
    z[53]= - z[38] - z[43];
    z[53]=z[9]*z[53];
    z[47]= - z[47] + static_cast<T>(4)+ z[53];
    z[47]=z[4]*z[47];
    z[47]=3*z[52] + z[47];
    z[47]=z[4]*z[47];
    z[52]=6*z[4];
    z[53]=z[52] - z[29] - 8*z[2];
    z[53]=z[53]*z[37];
    z[58]=z[8]*npow(z[4],3);
    z[60]=z[58]*z[5];
    z[53]=z[53] + 6*z[60];
    z[53]=z[5]*z[53];
    z[31]=z[31] + z[47] + z[53];
    z[31]=z[31]*z[33];
    z[47]=z[50] + z[49];
    z[47]=z[47]*z[27];
    z[47]=z[47] - z[45] + z[48] + z[38] - z[44];
    z[47]=z[47]*z[37];
    z[53]=z[9]*z[10];
    z[61]=static_cast<T>(1)- z[53];
    z[61]=z[61]*z[40];
    z[61]=static_cast<T>(1)+ z[61];
    z[61]=z[4]*z[61];
    z[62]=z[48] - z[4];
    z[62]=z[62]*z[4];
    z[63]= - z[2]*z[54];
    z[63]=z[62] + z[63];
    z[63]=z[5]*z[63];
    z[61]=z[63] - z[2] + z[61];
    z[61]=z[61]*z[33];
    z[63]=static_cast<T>(1)- z[41];
    z[27]=z[63]*z[27];
    z[46]=z[46]*z[9];
    z[27]=z[27] + static_cast<T>(1)+ 4*z[46];
    z[27]=z[4]*z[27];
    z[63]= - z[11] - z[26];
    z[27]=2*z[63] + z[27];
    z[27]=z[4]*z[27];
    z[63]=z[55] - z[4];
    z[63]=z[63]*z[37];
    z[60]=z[63] - z[60];
    z[60]=z[60]*z[36];
    z[27]=z[61] + z[27] + z[60];
    z[27]=z[3]*z[27];
    z[60]= - z[10] - z[48];
    z[60]=z[60]*z[37];
    z[27]=2*z[60] + z[27];
    z[27]=z[3]*z[27];
    z[60]=z[37]*z[55];
    z[54]= - z[48]*z[54];
    z[54]=z[62] + z[54];
    z[54]=z[54]*z[33];
    z[54]=z[60] + z[54];
    z[54]=z[54]*npow(z[3],2);
    z[60]= - z[1]*z[37]*npow(z[3],3)*z[48];
    z[54]=z[54] + z[60];
    z[54]=z[1]*z[54];
    z[19]= - z[19]*npow(z[7],4)*npow(z[5],2)*z[58];
    z[19]=z[54] + z[19] + z[27];
    z[19]=z[1]*z[19];
    z[27]=z[8]*z[6];
    z[54]=z[37]*z[27];
    z[58]=z[13]*z[10];
    z[60]=z[58]*z[5];
    z[61]=z[48]*z[60];
    z[54]=z[54] + z[61];
    z[54]=z[57]*z[51]*z[54];
    z[24]=z[51]*z[24];
    z[51]=z[13]*z[16];
    z[24]=z[24] + z[51];
    z[24]=z[24]*z[37];
    z[24]=z[24] + z[54];
    z[24]=z[5]*z[24];
    z[19]=2*z[19] + z[31] + z[47] + z[24];
    z[19]=z[1]*z[19];
    z[24]=z[58]*z[55];
    z[23]=z[18]*z[23];
    z[31]=3*z[58];
    z[47]=z[2]*z[10];
    z[51]= - z[31] + z[47];
    z[51]=z[51]*z[25];
    z[23]=z[51] + z[24] + z[23];
    z[24]=z[48] + z[29] - z[13];
    z[51]=z[6]*z[20];
    z[24]= - z[52] + 2*z[24] + z[51];
    z[24]=z[4]*z[24];
    z[51]= - z[20]*z[27];
    z[24]=z[51] + z[24];
    z[24]=z[4]*z[24];
    z[28]= - z[28] - 4*z[13];
    z[28]=z[2]*z[28];
    z[28]=z[31] + z[28];
    z[28]=z[28]*z[20];
    z[20]= - z[20] - 8*z[37];
    z[20]=z[4]*z[8]*z[20];
    z[20]=2*z[28] + z[20];
    z[20]=z[5]*z[20];
    z[20]=z[20] + 2*z[23] + z[24];
    z[20]=z[5]*z[20];
    z[23]=z[44]*z[34];
    z[24]= - z[23] + 4*z[49];
    z[24]=z[24]*z[25];
    z[17]=z[19] + z[17] + z[20] + z[24] + z[39];
    z[17]=z[1]*z[17];
    z[15]= - z[4]*z[15];
    z[15]= - 4*z[18] + z[15];
    z[15]=z[15]*z[36];
    z[19]= - z[8] + 8*z[13];
    z[15]=z[15] - z[16] + z[30] - z[19];
    z[15]=z[5]*z[15];
    z[20]= - 2*z[12] - z[9];
    z[20]=z[20]*z[21];
    z[24]=static_cast<T>(2)+ z[53];
    z[24]=z[24]*z[21];
    z[24]=11*z[12] + z[24];
    z[24]=z[5]*z[24];
    z[20]=z[24] - 11*z[22] + z[20];
    z[20]=z[20]*z[33];
    z[24]=static_cast<T>(2)+ z[34];
    z[25]=z[38] - z[11];
    z[25]=z[9]*z[25];
    z[24]=2*z[24] + z[25];
    z[24]=z[24]*z[21];
    z[25]= - z[9]*z[42];
    z[25]= - static_cast<T>(15)+ z[25];
    z[28]= - z[5]*z[29];
    z[25]=2*z[25] + z[28];
    z[25]=z[5]*z[25];
    z[28]=static_cast<T>(8)+ z[34];
    z[28]=z[12]*z[28];
    z[20]=z[20] + z[25] + 3*z[28] + z[24];
    z[20]=z[3]*z[20];
    z[24]=static_cast<T>(1)+ 3*z[41];
    z[24]=z[24]*z[40];
    z[25]= - static_cast<T>(3)- z[34];
    z[24]=z[24] + 2*z[25] - z[46];
    z[15]=z[20] + 2*z[24] + z[15];
    z[15]=z[3]*z[15];
    z[20]= - z[10] - z[13];
    z[20]=z[20]*z[48];
    z[24]=7*z[58];
    z[20]=z[24] + z[20];
    z[25]=z[6]*z[52];
    z[25]=z[25] - static_cast<T>(2)- z[27];
    z[25]=z[4]*z[25];
    z[14]= - z[14] - z[2];
    z[14]=4*z[14] + z[25];
    z[14]=z[4]*z[14];
    z[16]=z[16]*z[27];
    z[16]=z[2] + z[16];
    z[16]=z[16]*z[37];
    z[24]=z[2]*z[24];
    z[16]=z[24] + z[16];
    z[16]=z[16]*z[36];
    z[14]=z[16] + 2*z[20] + z[14];
    z[14]=z[5]*z[14];
    z[16]= - z[13] - z[43];
    z[16]=2*z[16] - z[45];
    z[16]=z[9]*z[16];
    z[20]=z[6]*z[43];
    z[24]=z[6] - z[9];
    z[24]=z[4]*z[24];
    z[16]=4*z[24] + z[16] - static_cast<T>(4)+ z[20];
    z[16]=z[4]*z[16];
    z[20]=5*z[13];
    z[24]=z[29] + z[10];
    z[14]=z[17] + z[15] + z[14] + z[16] - 6*z[49] + z[48] + z[32] + 2*
    z[24] - z[20];
    z[14]=z[1]*z[14];
    z[15]=z[60] - z[10] - z[50];
    z[15]=z[2]*z[15];
    z[15]=z[58] + z[15];
    z[15]=z[5]*z[15];
    z[16]= - z[13] + z[2];
    z[17]=z[12]*z[18];
    z[15]=z[15] + 2*z[16] + z[17];
    z[15]=z[7]*z[15];
    z[16]= - z[58] + z[47];
    z[17]=z[9]*z[7];
    z[16]=z[16]*z[17];
    z[15]=z[16] + z[15];
    z[15]=z[15]*z[36];
    z[16]=z[59]*z[50];
    z[18]=z[17]*z[58];
    z[24]=z[7]*z[50];
    z[18]=z[24] + z[18];
    z[18]=z[18]*z[21];
    z[15]=z[15] - z[4] + z[18] + z[16] + z[26] - z[19];
    z[15]=z[5]*z[15];
    z[16]=z[22]*z[7];
    z[18]=static_cast<T>(9)- 2*z[34];
    z[18]=z[18]*z[16];
    z[19]=z[59]*z[9];
    z[21]= - z[35]*z[19];
    z[18]=z[18] + z[21];
    z[18]=z[9]*z[18];
    z[21]= - 12*z[59] - z[17];
    z[21]=z[9]*z[21];
    z[24]=z[59] + z[17];
    z[24]=z[5]*z[24];
    z[21]=5*z[24] - 11*z[16] + z[21];
    z[21]=z[5]*z[21];
    z[24]=2*z[59];
    z[25]= - z[24] - z[17];
    z[25]=z[9]*z[25];
    z[25]= - z[16] + z[25];
    z[26]=z[10]*z[17];
    z[26]=2*z[7] + z[26];
    z[26]=z[9]*z[26];
    z[26]=z[59] + z[26];
    z[26]=z[5]*z[26];
    z[25]=3*z[25] + z[26];
    z[25]=z[5]*z[25];
    z[19]=2*z[16] + z[19];
    z[19]=z[9]*z[19];
    z[26]=z[7]*npow(z[12],3);
    z[19]=z[26] + z[19];
    z[19]=3*z[19] + z[25];
    z[19]=z[5]*z[19];
    z[22]= - z[22]*z[17];
    z[22]= - 2*z[26] + z[22];
    z[22]=z[9]*z[22];
    z[25]= - z[7]*npow(z[12],4);
    z[19]=z[19] + z[25] + z[22];
    z[19]=z[19]*z[33];
    z[22]=static_cast<T>(7)- z[34];
    z[22]=z[22]*z[26];
    z[18]=z[19] + z[21] + z[22] + z[18];
    z[18]=z[18]*z[56];
    z[19]= - static_cast<T>(4)+ z[34];
    z[16]=z[19]*z[16];
    z[19]=static_cast<T>(1)- z[34];
    z[19]=z[19]*z[17];
    z[19]= - static_cast<T>(2)+ z[19];
    z[19]=z[9]*z[19];
    z[16]=z[16] + z[19];
    z[19]= - static_cast<T>(11)+ 9*z[59];
    z[19]=z[5]*z[19];
    z[16]=z[18] + z[19] + 8*z[12] + 3*z[16];
    z[16]=z[3]*z[16];
    z[18]= - z[12]*z[13];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[7]*z[18];
    z[18]=z[18] + z[20] - z[8] - z[10];
    z[19]= - z[20] + z[23];
    z[17]=z[19]*z[17];
    z[17]=2*z[18] + z[17];
    z[17]=z[9]*z[17];
    z[18]=z[12]*z[50];
    z[19]=2*z[6] - z[9];
    z[19]=z[4]*z[19];

    r +=  - static_cast<T>(7)+ z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[24]
       - 2*z[27];
 
    return r;
}

template double qg_2lNLC_r1430(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1430(const std::array<dd_real,31>&);
#endif
