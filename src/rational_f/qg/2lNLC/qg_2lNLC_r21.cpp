#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r21(const std::array<T,31>& k) {
  T z[135];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[7];
    z[9]=k[5];
    z[10]=k[13];
    z[11]=k[2];
    z[12]=k[14];
    z[13]=k[3];
    z[14]=k[23];
    z[15]=k[9];
    z[16]=k[24];
    z[17]=k[18];
    z[18]=npow(z[7],3);
    z[19]=z[18]*z[13];
    z[20]=4*z[19];
    z[21]=npow(z[10],2);
    z[22]= - z[8]*z[10];
    z[22]=z[22] + 11*z[21] + z[20];
    z[22]=z[8]*z[22];
    z[23]=npow(z[10],3);
    z[22]=z[23] + z[22];
    z[22]=z[8]*z[22];
    z[24]=npow(z[2],2);
    z[25]=z[24]*z[18];
    z[26]=npow(z[16],2);
    z[27]=6*z[26];
    z[28]= - z[18]*z[27];
    z[28]=z[28] + z[25];
    z[28]=z[6]*z[28];
    z[29]=z[15] - z[14];
    z[30]=2*z[29];
    z[31]=z[19]*z[30];
    z[32]=3*z[2];
    z[33]=z[6]*z[18]*z[32];
    z[34]=z[18]*z[8];
    z[35]=3*z[34];
    z[36]=z[13]*z[35];
    z[31]=z[33] + z[31] + z[36];
    z[31]=z[4]*z[31];
    z[33]=z[2]*z[8];
    z[36]=npow(z[8],2);
    z[37]=z[33] + z[36];
    z[38]=z[37]*z[24];
    z[39]=z[10]*z[16];
    z[40]=2*z[26];
    z[41]=z[40] + z[39];
    z[41]=z[41]*z[21];
    z[42]=npow(z[14],2);
    z[43]= - z[42]*z[20];
    z[22]=z[31] + z[28] + z[38] + z[22] + z[41] + z[43];
    z[22]=z[4]*z[22];
    z[28]=z[36]*z[18];
    z[31]=4*z[18];
    z[41]= - z[26]*z[31];
    z[43]=z[18]*z[2];
    z[44]= - z[35] - z[43];
    z[44]=z[2]*z[44];
    z[35]= - z[4]*z[35];
    z[35]=z[35] + z[44] + z[41] - z[28];
    z[35]=z[4]*z[35];
    z[41]=z[21]*z[18];
    z[44]=z[2]*z[34];
    z[44]=z[44] - z[41] + 2*z[28];
    z[44]=z[2]*z[44];
    z[45]=npow(z[5],2);
    z[46]=3*z[12];
    z[47]= - z[18]*z[45]*z[46];
    z[48]=npow(z[8],3);
    z[49]=z[48]*z[18];
    z[35]=z[47] + z[35] + z[49] + z[44];
    z[35]=z[9]*z[35];
    z[44]=2*z[41] - z[28];
    z[47]=5*z[10];
    z[50]= - z[18]*z[47];
    z[50]=z[50] - z[34];
    z[50]=z[2]*z[50];
    z[44]=2*z[44] + z[50];
    z[44]=z[2]*z[44];
    z[50]=z[47]*z[14];
    z[51]=3*z[42];
    z[52]=z[51] + z[50];
    z[52]=z[10]*z[52]*z[31];
    z[44]=z[44] + z[52] - z[49];
    z[44]=z[6]*z[44];
    z[52]=npow(z[12],2);
    z[53]=n<T>(1,3)*z[52];
    z[27]=z[27] - z[53];
    z[27]=z[11]*z[27]*z[18];
    z[54]=npow(z[15],2);
    z[55]=z[54] - z[42];
    z[56]=4*z[4];
    z[57]=z[56]*z[55];
    z[58]=npow(z[2],3);
    z[59]=n<T>(313,18)*z[58];
    z[60]=z[59] + z[57];
    z[60]=z[4]*z[60];
    z[61]=2*z[11];
    z[62]= - z[61]*z[18]*z[12];
    z[63]=z[24]*z[4];
    z[62]=z[62] - z[63];
    z[62]=z[5]*z[62];
    z[27]=6*z[62] + z[27] + z[60];
    z[27]=z[5]*z[27];
    z[60]=z[19]*z[48];
    z[22]=z[35] + z[27] + z[22] - z[60] + z[44];
    z[22]=z[9]*z[22];
    z[27]= - z[18]*z[40];
    z[25]=z[27] - z[25];
    z[27]=npow(z[6],2);
    z[25]=z[25]*z[27];
    z[35]=2*z[55];
    z[44]=npow(z[13],2);
    z[62]=z[29]*z[44];
    z[64]=z[18]*z[62];
    z[64]= - z[35] + z[64];
    z[65]=2*z[4];
    z[64]=z[64]*z[65];
    z[66]=z[44]*z[18];
    z[67]=2*z[42];
    z[68]= - z[67]*z[66];
    z[69]=z[16]*z[21];
    z[68]=z[69] + z[68];
    z[47]=z[47] - 3*z[66];
    z[47]=2*z[47] - z[8];
    z[47]=z[8]*z[47];
    z[47]= - 9*z[21] + z[47];
    z[47]=z[8]*z[47];
    z[66]=n<T>(13,6)*z[36];
    z[69]=n<T>(25,3)*z[17];
    z[70]= - z[69] + 7*z[8];
    z[71]= - n<T>(313,6)*z[2] - z[70];
    z[71]=z[2]*z[71];
    z[71]=z[66] + z[71];
    z[71]=z[2]*z[71];
    z[25]=z[64] + z[25] + z[71] + 2*z[68] + z[47];
    z[25]=z[4]*z[25];
    z[47]=npow(z[11],2);
    z[64]=z[47]*z[18];
    z[68]=z[12]*z[64];
    z[71]=z[27]*z[34];
    z[72]=z[15] - 10*z[2];
    z[72]=z[4]*z[72];
    z[68]=z[72] + 2*z[71] - 4*z[24] - 8*z[68] - z[36];
    z[68]=z[5]*z[68];
    z[71]=n<T>(5,3)*z[52];
    z[72]=8*z[26] + z[71];
    z[64]=z[72]*z[64];
    z[72]=n<T>(5,3)*z[54];
    z[73]=n<T>(161,9)*z[24] + z[67] - z[72];
    z[74]=11*z[29] + z[32];
    z[74]=z[4]*z[74];
    z[73]=2*z[73] + z[74];
    z[73]=z[4]*z[73];
    z[74]=z[27]*z[28];
    z[59]=z[68] + z[73] + n<T>(331,9)*z[74] + z[59] + z[64] + n<T>(295,18)*z[48]
   ;
    z[59]=z[5]*z[59];
    z[64]=z[69] + z[10];
    z[64]=z[64]*z[18];
    z[43]= - z[43] + z[64] - n<T>(31,6)*z[34];
    z[43]=z[2]*z[43];
    z[28]=z[43] + 13*z[41] + n<T>(16,3)*z[28];
    z[28]=z[2]*z[28];
    z[41]=z[14]*z[21]*z[31];
    z[28]=z[28] + z[41] + z[49];
    z[28]=z[6]*z[28];
    z[28]=z[60] + z[28];
    z[28]=z[6]*z[28];
    z[41]=z[52]*z[36];
    z[22]=z[22] + z[59] + z[25] - 2*z[41] + z[28];
    z[22]=z[9]*z[22];
    z[25]=z[12]*z[15];
    z[28]= - z[25] - z[26] - z[67];
    z[43]=2*z[8];
    z[49]=6*z[10];
    z[59]= - z[43] - z[49] + z[3];
    z[43]=z[59]*z[43];
    z[59]=z[69] - z[10];
    z[60]=n<T>(11,3)*z[8];
    z[64]= - n<T>(1123,18)*z[2] - z[60] - n<T>(19,2)*z[3] + z[59];
    z[64]=z[2]*z[64];
    z[68]=z[3] + n<T>(295,6)*z[2];
    z[73]=z[24]*z[6];
    z[74]= - z[68]*z[73];
    z[30]= - z[30] - z[2];
    z[75]=z[6]*z[3];
    z[76]= - z[2]*z[75];
    z[30]=3*z[30] + z[76];
    z[30]=z[4]*z[30];
    z[76]=2*z[14];
    z[77]= - z[76] - z[12];
    z[77]=z[3]*z[77];
    z[28]=z[30] + z[74] + z[64] + z[43] + z[77] + 2*z[28] + z[39];
    z[28]=z[4]*z[28];
    z[30]=npow(z[3],2);
    z[43]=z[30]*z[18];
    z[64]=z[47]*z[43];
    z[74]= - 47*z[3] + 38*z[8];
    z[74]=z[8]*z[74];
    z[74]=z[30] + n<T>(2,9)*z[74];
    z[74]=z[8]*z[74];
    z[64]=z[64] + z[74];
    z[43]=12*z[43];
    z[74]=z[18]*z[3];
    z[77]= - 153*z[74] + n<T>(2245,18)*z[34];
    z[77]=z[8]*z[77];
    z[77]=z[43] + z[77];
    z[77]=z[6]*z[77];
    z[43]= - z[11]*z[43];
    z[78]=z[48]*z[3];
    z[43]=z[77] + z[43] + n<T>(313,18)*z[78];
    z[43]=z[6]*z[43];
    z[43]=4*z[64] + z[43];
    z[43]=z[6]*z[43];
    z[64]=z[3]*z[11];
    z[77]=z[18]*z[64];
    z[79]= - 3*z[74] + z[34];
    z[79]=z[6]*z[79];
    z[80]=z[36]*z[3];
    z[77]=3*z[79] + 10*z[77] + n<T>(17,2)*z[80];
    z[77]=z[6]*z[77];
    z[79]=z[47]*z[3];
    z[81]= - z[79]*z[31];
    z[82]=n<T>(15,2)*z[8];
    z[83]= - n<T>(263,9)*z[3] + z[82];
    z[83]=z[8]*z[83];
    z[77]=z[77] + z[81] + z[83];
    z[77]=z[6]*z[77];
    z[81]=z[61]*z[2];
    z[83]=z[81] + 7;
    z[84]=z[61]*z[3];
    z[85]= - z[84] - z[83];
    z[85]=z[2]*z[85];
    z[86]=3*z[4];
    z[87]=static_cast<T>(4)+ z[75];
    z[87]=z[87]*z[86];
    z[88]=2*z[3];
    z[77]=z[87] + z[77] + z[85] + z[88] - n<T>(463,18)*z[8];
    z[77]=z[5]*z[77];
    z[85]=npow(z[11],3);
    z[87]=z[85]*z[88];
    z[89]=z[18]*z[87];
    z[90]=n<T>(1,2)*z[2];
    z[91]=313*z[11] + 295*z[79];
    z[91]=z[91]*z[90];
    z[91]=z[91] + n<T>(545,2) + 268*z[64];
    z[92]=n<T>(1,9)*z[2];
    z[91]=z[91]*z[92];
    z[89]=z[91] + 11*z[3] + z[89];
    z[89]=z[2]*z[89];
    z[91]=3*z[14];
    z[93]=z[91] + n<T>(77,3)*z[15];
    z[94]=4*z[3];
    z[95]= - z[65] + n<T>(373,18)*z[2] - n<T>(3,2)*z[8] + z[94] + z[93];
    z[95]=z[4]*z[95];
    z[96]=z[3]*z[14];
    z[96]=z[96] + z[67];
    z[96]=2*z[96];
    z[97]=z[85]*z[40];
    z[98]=z[85]*z[3];
    z[99]=z[16]*z[98];
    z[97]=z[97] + z[99];
    z[97]=z[97]*z[18];
    z[99]=z[12] - z[15];
    z[100]= - n<T>(295,9)*z[8] + n<T>(184,9)*z[3] - z[99];
    z[100]=z[8]*z[100];
    z[43]=z[77] + z[95] + z[43] + z[89] + z[100] + z[96] + z[97];
    z[43]=z[5]*z[43];
    z[77]=n<T>(11,2)*z[3] + 107*z[2];
    z[77]=z[77]*z[92];
    z[89]=z[32] + 6*z[29] - z[3];
    z[89]=z[4]*z[89];
    z[92]=z[8]*z[3];
    z[77]=z[89] + z[77] + z[96] + z[92];
    z[77]=z[4]*z[77];
    z[89]=z[2] + z[3];
    z[95]=2*z[2];
    z[89]=z[89]*z[95];
    z[96]=npow(z[6],3);
    z[97]=npow(z[7],4);
    z[100]=z[96]*z[97];
    z[101]=z[8]*z[94]*z[100];
    z[101]= - n<T>(5,2)*z[80] + z[101];
    z[101]=z[6]*z[101];
    z[102]=7*z[2];
    z[103]=z[88] - z[102];
    z[103]=z[4]*z[103];
    z[101]=z[103] + z[101] - n<T>(5,2)*z[36] - z[89];
    z[101]=z[5]*z[101];
    z[103]=304*z[8];
    z[104]=n<T>(1565,2)*z[3] - z[103];
    z[105]=n<T>(1,9)*z[8];
    z[104]=z[105]*z[97]*z[104];
    z[106]=z[30]*z[97];
    z[104]= - 4*z[106] + z[104];
    z[104]=z[8]*z[104]*npow(z[6],4);
    z[106]=z[36]*z[94];
    z[107]=static_cast<T>(304)+ n<T>(295,2)*z[64];
    z[107]=z[2]*z[107];
    z[107]=n<T>(241,2)*z[3] + z[107];
    z[107]=z[107]*z[24];
    z[77]=z[101] + z[77] + z[104] + z[106] + n<T>(1,9)*z[107];
    z[77]=z[5]*z[77];
    z[101]=z[97]*z[45]*z[52];
    z[104]=z[61]*z[101];
    z[106]=z[97]*z[48];
    z[33]=2*z[36] + z[33];
    z[33]=z[2]*z[97]*z[33];
    z[33]=z[106] + z[33];
    z[107]=z[9]*z[4];
    z[33]=z[33]*z[107];
    z[108]=z[21]*z[97];
    z[109]=z[73]*z[108];
    z[110]= - z[4]*z[13]*z[106];
    z[33]=z[33] + z[104] - 2*z[109] + z[110];
    z[33]=z[9]*z[33];
    z[50]=z[67] + z[50];
    z[50]=z[50]*z[108];
    z[104]=z[2]*z[10];
    z[109]= - z[97]*z[104];
    z[108]= - z[108] + z[109];
    z[108]=z[2]*z[108];
    z[23]=z[23]*z[97];
    z[23]=5*z[23] + z[108];
    z[23]=z[2]*z[23];
    z[23]=z[50] + z[23];
    z[23]=z[23]*z[27];
    z[50]=z[47]*z[101];
    z[23]=z[23] + z[50];
    z[23]=2*z[23] + z[33];
    z[23]=z[9]*z[23];
    z[33]=z[96]*z[106];
    z[50]=n<T>(307,6)*z[58] + z[57];
    z[50]=z[4]*z[50];
    z[57]=z[36]*z[100];
    z[57]=z[57] - 6*z[63];
    z[57]=z[5]*z[57];
    z[33]=z[57] - n<T>(295,18)*z[33] + z[50];
    z[33]=z[5]*z[33];
    z[23]=z[33] + z[23];
    z[23]=z[9]*z[23];
    z[33]=z[6]*z[7];
    z[33]=npow(z[33],5);
    z[50]= - z[33]*z[80];
    z[57]= - z[4]*z[89];
    z[50]=z[50] + z[57];
    z[50]=z[5]*z[50];
    z[33]=z[33]*z[78];
    z[57]=z[58]*z[3];
    z[33]=295*z[57] - 313*z[33];
    z[78]=3*z[3];
    z[80]= - z[78] + n<T>(301,6)*z[2];
    z[80]=z[80]*z[24];
    z[89]=z[3]*z[4]*z[2];
    z[80]=z[80] + z[89];
    z[80]=z[4]*z[80];
    z[33]=z[50] + n<T>(1,18)*z[33] + z[80];
    z[33]=z[5]*z[33];
    z[50]= - z[21]*npow(z[7],5)*z[58]*z[27]*npow(z[9],3);
    z[80]=z[1]*z[5];
    z[80]=z[80] - 1;
    z[57]=n<T>(295,18)*z[57];
    z[80]=z[80]*z[57]*z[4];
    z[33]=z[50] + z[33] + z[80];
    z[33]=z[1]*z[33];
    z[50]=z[3]*z[13];
    z[80]=z[50] - z[75];
    z[80]=z[27]*z[106]*z[80];
    z[41]= - z[88]*z[41];
    z[41]=z[41] + n<T>(313,18)*z[80];
    z[41]=z[6]*z[41];
    z[80]= - n<T>(241,3)*z[3] - 301*z[2];
    z[80]=z[80]*z[24];
    z[96]=z[58]*z[75];
    z[80]=z[80] - n<T>(295,3)*z[96];
    z[80]=n<T>(1,6)*z[80] - z[89];
    z[80]=z[4]*z[80];
    z[89]=z[3]*z[12];
    z[96]= - z[89] - 2*z[52];
    z[96]=z[96]*z[36];
    z[23]=z[33] + z[23] + z[77] + z[80] + z[41] + z[96] - z[57];
    z[23]=z[1]*z[23];
    z[20]=z[30]*z[20];
    z[19]=z[3]*z[19];
    z[33]= - z[13]*z[34];
    z[19]= - n<T>(319,6)*z[19] + z[33];
    z[19]=z[8]*z[19];
    z[19]=z[20] + z[19];
    z[19]=z[8]*z[19];
    z[20]= - z[30]*z[31];
    z[31]=n<T>(1583,2)*z[74] - 304*z[34];
    z[31]=z[31]*z[105];
    z[20]=z[20] + z[31];
    z[31]=z[6]*z[8];
    z[20]=z[20]*z[31];
    z[19]=z[19] + z[20];
    z[19]=z[6]*z[19];
    z[20]=z[12]*z[94];
    z[20]= - z[52] + z[20];
    z[33]=z[44]*z[3];
    z[18]= - z[18]*z[33];
    z[18]=2*z[20] + z[18];
    z[18]=z[8]*z[18];
    z[20]=z[71]*z[3];
    z[18]= - z[20] + z[18];
    z[18]=z[8]*z[18];
    z[18]=z[18] + z[19];
    z[18]=z[6]*z[18];
    z[19]=3*z[8];
    z[34]=z[12] - z[3];
    z[34]=z[34]*z[19];
    z[41]=2*z[15];
    z[57]= - z[41] - n<T>(11,3)*z[12];
    z[57]=z[12]*z[57];
    z[34]=z[34] + z[57] - n<T>(7,6)*z[89];
    z[34]=z[8]*z[34];
    z[57]=z[8]*z[12];
    z[68]= - z[2]*z[68];
    z[68]= - z[57] + z[68];
    z[68]=z[2]*z[68];
    z[18]=z[23] + z[22] + z[43] + z[28] + z[18] + z[34] + z[68];
    z[18]=z[1]*z[18];
    z[22]=z[75]*z[48];
    z[23]=n<T>(313,6)*z[22];
    z[28]=6*z[30];
    z[34]=npow(z[7],2);
    z[43]= - z[28] - n<T>(707,9)*z[34];
    z[68]=n<T>(317,2)*z[3] - n<T>(304,3)*z[8];
    z[68]=z[8]*z[68];
    z[43]=2*z[43] + z[68];
    z[43]=z[8]*z[43];
    z[68]=z[34]*z[3];
    z[43]= - z[23] + n<T>(1163,9)*z[68] + z[43];
    z[43]=z[6]*z[43];
    z[71]=z[34]*z[64];
    z[74]= - 1219*z[3] + n<T>(3371,2)*z[8];
    z[74]=z[74]*z[105];
    z[77]=12*z[30];
    z[43]=z[43] + z[74] + z[77] - n<T>(1103,18)*z[71];
    z[43]=z[6]*z[43];
    z[71]=z[75]*z[36];
    z[74]=6*z[34];
    z[80]=n<T>(562,9)*z[3] - z[82];
    z[80]=z[8]*z[80];
    z[80]= - n<T>(21,2)*z[71] - z[74] + z[80];
    z[80]=z[6]*z[80];
    z[82]=z[34]*z[11];
    z[80]=z[65] + z[80] + n<T>(490,9)*z[8] - n<T>(479,9)*z[3] - z[82];
    z[80]=z[6]*z[80];
    z[96]=z[98] + z[47];
    z[96]=z[96]*z[95];
    z[97]=n<T>(113,18)*z[79];
    z[98]=z[11]*z[12];
    z[100]= - static_cast<T>(7)+ z[98];
    z[100]=z[11]*z[100];
    z[100]= - z[96] + z[100] - z[97];
    z[100]=z[2]*z[100];
    z[101]=4*z[34];
    z[101]=z[47]*z[101];
    z[80]=z[100] + z[101] - n<T>(23,2) + z[84] + z[80];
    z[80]=z[5]*z[80];
    z[84]=n<T>(107,9)*z[11] - 3*z[79];
    z[84]=z[2]*z[84];
    z[84]=z[84] + n<T>(7,9)*z[64] + static_cast<T>(20)+ z[98];
    z[84]=z[2]*z[84];
    z[100]=z[26]*z[61];
    z[101]=6*z[16] + z[15];
    z[101]=2*z[101] - n<T>(67,6)*z[12];
    z[101]=z[101]*z[47];
    z[101]=z[101] + n<T>(37,2)*z[79];
    z[101]=z[101]*z[34];
    z[106]=3*z[16];
    z[108]=z[11]*z[16];
    z[109]=n<T>(854,9) + z[108];
    z[109]=z[3]*z[109];
    z[110]=z[6]*z[88];
    z[110]=static_cast<T>(11)+ z[110];
    z[110]=z[4]*z[110];
    z[43]=z[80] + z[110] + z[43] + z[84] - n<T>(895,18)*z[8] + z[101] + 
    z[109] + z[100] - n<T>(7,2)*z[12] + n<T>(98,3)*z[15] + z[106] - z[76];
    z[43]=z[5]*z[43];
    z[80]=n<T>(17,3)*z[54];
    z[84]= - z[52] + z[40] - z[80];
    z[100]=2*z[12];
    z[101]=z[100] + z[15];
    z[109]=z[8] + 6*z[82] - z[101];
    z[110]=8*z[34];
    z[111]=3*z[36];
    z[112]= - z[110] + z[111];
    z[112]=z[6]*z[112];
    z[83]= - z[2]*z[83];
    z[83]=8*z[4] + z[112] + 2*z[109] + z[83];
    z[83]=z[5]*z[83];
    z[109]=17*z[16] - 13*z[12];
    z[109]=z[109]*z[82];
    z[112]=n<T>(304,9)*z[8];
    z[113]=z[112] - z[100] - z[10];
    z[113]=z[8]*z[113];
    z[114]=n<T>(76,9)*z[2] - 7*z[10] + z[82];
    z[114]=z[114]*z[95];
    z[115]= - n<T>(241,3)*z[34] - 295*z[36];
    z[31]=z[115]*z[31];
    z[115]=14*z[14];
    z[116]= - z[16] - z[115];
    z[116]=z[10]*z[116];
    z[115]= - z[4] + n<T>(397,9)*z[2] - n<T>(7,2)*z[8] + z[115] + n<T>(95,3)*z[15];
    z[115]=z[4]*z[115];
    z[31]=z[83] + z[115] + n<T>(1,6)*z[31] + z[114] + z[113] + z[109] + 2*
    z[84] + z[116];
    z[31]=z[5]*z[31];
    z[83]=4*z[42];
    z[84]= - z[15]*z[46];
    z[84]=z[84] + z[72] + z[26] - z[83];
    z[109]=4*z[13];
    z[113]=z[109]*z[55];
    z[114]= - z[32] - z[19] - 15*z[29] - z[113];
    z[114]=z[4]*z[114];
    z[115]=10*z[10];
    z[116]=z[34]*z[13];
    z[117]=z[8]*z[13];
    z[118]=2*z[117];
    z[119]= - n<T>(197,6) + z[118];
    z[119]=z[8]*z[119];
    z[119]=z[119] + z[115] - n<T>(1,2)*z[116];
    z[119]=z[8]*z[119];
    z[120]=z[12] + z[16];
    z[121]=z[120]*z[34];
    z[122]=z[34] - n<T>(295,3)*z[24];
    z[122]=z[122]*z[90];
    z[122]= - 4*z[121] + z[122];
    z[122]=z[6]*z[122];
    z[123]=3*z[15];
    z[124]= - 13*z[14] + z[123];
    z[124]=z[124]*z[116];
    z[125]= - 5*z[16] + z[100];
    z[125]=z[10]*z[125];
    z[59]= - n<T>(1541,18)*z[2] - n<T>(7,6)*z[8] + z[59];
    z[59]=z[2]*z[59];
    z[59]=z[114] + z[122] + z[59] + z[119] + z[124] + 2*z[84] + z[125];
    z[59]=z[4]*z[59];
    z[84]=n<T>(11,3)*z[54];
    z[114]=n<T>(103,9)*z[24] + z[67] - z[84];
    z[119]=5*z[29] + z[2];
    z[119]=z[4]*z[119];
    z[114]=2*z[114] + z[119];
    z[114]=z[4]*z[114];
    z[119]= - z[41] - z[102];
    z[119]=z[4]*z[119];
    z[122]=3*z[34];
    z[119]=z[119] + z[122] - 2*z[24];
    z[119]=z[5]*z[119];
    z[124]=z[67] - z[26];
    z[125]= - z[16] - 6*z[14];
    z[125]=z[10]*z[125];
    z[125]= - 2*z[124] + z[125];
    z[125]=z[10]*z[125];
    z[126]=2*z[16];
    z[127]=z[126] - n<T>(19,6)*z[12];
    z[127]=z[127]*z[34];
    z[128]= - z[10]*z[19];
    z[128]= - z[21] + z[128];
    z[128]=z[8]*z[128];
    z[129]=3*z[21];
    z[104]= - z[129] - z[104];
    z[104]=z[104]*z[95];
    z[104]=z[119] + z[114] + z[104] + z[128] + z[125] + z[127];
    z[104]=z[5]*z[104];
    z[70]= - 2*z[70] - n<T>(349,18)*z[2];
    z[70]=z[2]*z[70];
    z[70]=z[70] - n<T>(27,2)*z[34] + n<T>(13,3)*z[36];
    z[70]=z[2]*z[70];
    z[114]=3*z[26];
    z[39]= - z[114] - z[39];
    z[119]=2*z[10];
    z[39]=z[39]*z[119];
    z[125]= - z[46] - z[15] - z[126] - 7*z[14];
    z[125]=z[125]*z[34];
    z[110]= - z[21] - z[110];
    z[126]=29*z[10];
    z[127]= - z[126] + z[8];
    z[127]=z[8]*z[127];
    z[110]=2*z[110] + z[127];
    z[110]=z[8]*z[110];
    z[35]= - z[35] + z[122];
    z[35]=z[35]*z[65];
    z[35]=z[35] + z[70] + z[110] + z[39] + z[125];
    z[35]=z[4]*z[35];
    z[39]=n<T>(13,2)*z[10] - z[8];
    z[39]=z[34]*z[39];
    z[65]=z[37]*z[95];
    z[39]=z[65] + z[39];
    z[39]=z[2]*z[39];
    z[37]=z[37]*z[63];
    z[45]= - z[45]*z[63];
    z[45]=z[37] + z[45];
    z[45]=z[9]*z[45];
    z[65]= - z[26] + z[51];
    z[70]= - 8*z[10] + 11*z[14] - z[41];
    z[70]=z[10]*z[70];
    z[65]=4*z[65] + z[70];
    z[65]=z[65]*z[34];
    z[35]=2*z[45] + z[104] + z[35] + z[65] + z[39];
    z[35]=z[9]*z[35];
    z[39]=n<T>(5,3)*z[12];
    z[45]= - z[41] - z[39];
    z[45]=z[12]*z[45];
    z[65]= - z[15] - 17*z[10];
    z[65]=z[65]*z[119];
    z[70]=z[116] + z[12] - z[49];
    z[70]=3*z[70] + z[8];
    z[70]=z[8]*z[70];
    z[45]=z[70] + z[45] + z[65];
    z[45]=z[8]*z[45];
    z[65]=4*z[11];
    z[70]=z[65]*z[54];
    z[104]=z[34]*z[61];
    z[110]=n<T>(331,18)*z[2];
    z[104]= - z[110] - n<T>(115,6)*z[8] + z[104] - z[70] - z[10] + 25*z[17]
    + z[41];
    z[104]=z[2]*z[104];
    z[122]=z[41] + n<T>(23,3)*z[8];
    z[122]=z[8]*z[122];
    z[104]=z[104] + z[122] - 4*z[54] - z[129];
    z[104]=z[2]*z[104];
    z[122]=9*z[10];
    z[125]=z[41] + z[122];
    z[125]=z[10]*z[125];
    z[127]=8*z[42];
    z[125]=z[127] + z[125];
    z[125]=z[13]*z[125];
    z[101]=z[101]*z[12];
    z[128]=z[40] - z[101];
    z[128]=z[128]*z[61];
    z[125]=z[125] + z[128];
    z[125]=z[125]*z[34];
    z[128]=z[10] + z[14];
    z[122]=z[128]*z[122];
    z[122]= - z[111] + z[122] + z[124];
    z[122]=z[34]*z[122];
    z[124]= - z[32] - n<T>(21,2)*z[8] + z[69] + 14*z[10];
    z[130]=z[34]*z[2];
    z[124]=z[124]*z[130];
    z[122]=2*z[122] + z[124];
    z[122]=z[6]*z[122];
    z[124]= - z[26]*z[49];
    z[31]=z[35] + z[31] + z[59] + z[122] + z[104] + z[45] + z[124] + 
    z[125];
    z[31]=z[9]*z[31];
    z[35]=z[3]*z[24];
    z[35]= - z[121] - 6*z[35];
    z[35]=z[6]*z[35];
    z[45]=4*z[26];
    z[59]= - n<T>(29,2)*z[3] - z[102];
    z[59]=z[2]*z[59];
    z[35]=z[35] + z[59] + z[45] - z[89];
    z[35]=z[6]*z[35];
    z[59]= - z[13]*z[41];
    z[59]=7*z[117] - 8*z[50] + n<T>(227,6) + z[59];
    z[59]=z[8]*z[59];
    z[89]= - z[42]*z[109];
    z[102]=2*z[34];
    z[104]=z[62]*z[102];
    z[121]=3*z[13];
    z[122]= - z[29]*z[121];
    z[122]=static_cast<T>(1)+ z[122];
    z[124]=z[6]*z[2];
    z[122]=2*z[122] + z[124];
    z[122]=z[4]*z[122];
    z[125]=z[13]*z[14];
    z[131]= - n<T>(27,2) + 8*z[125];
    z[131]=z[3]*z[131];
    z[35]=z[122] + z[35] - n<T>(45,2)*z[2] + z[59] + z[104] + z[131] + z[89]
    - z[46] + z[16] - z[93];
    z[35]=z[4]*z[35];
    z[59]=z[61]*z[54];
    z[89]=z[15] + z[59];
    z[89]=z[11]*z[89];
    z[89]= - n<T>(176,9) + z[89];
    z[93]= - z[123] - z[59];
    z[93]=z[93]*z[61];
    z[93]= - n<T>(23,6) + z[93];
    z[93]=z[11]*z[93];
    z[93]=z[93] - 2*z[79];
    z[93]=z[3]*z[93];
    z[104]= - z[47]*z[102];
    z[122]= - n<T>(295,9)*z[11] - z[79];
    z[122]=z[2]*z[122];
    z[89]=z[122] + z[104] + 4*z[89] + z[93];
    z[89]=z[2]*z[89];
    z[93]=n<T>(23,3)*z[54] - z[25];
    z[93]=z[93]*z[61];
    z[104]= - z[11] - z[79];
    z[104]=z[104]*z[88];
    z[122]=z[11]*z[54];
    z[122]= - 53*z[15] - 34*z[122];
    z[122]=z[11]*z[122];
    z[122]=n<T>(71,6) + z[122];
    z[104]=n<T>(1,3)*z[122] + z[104];
    z[104]=z[3]*z[104];
    z[122]=4*z[15];
    z[131]=z[122] + z[12];
    z[132]=z[131]*z[47];
    z[132]=z[132] + 5*z[79];
    z[132]=z[132]*z[34];
    z[133]=7*z[15];
    z[89]=z[89] - n<T>(19,3)*z[8] + z[132] + z[104] + z[93] - 16*z[10] - 
    z[46] + z[69] + z[133];
    z[89]=z[2]*z[89];
    z[93]=z[16]*z[79];
    z[93]=z[93] - 5*z[125] + z[108];
    z[93]=z[93]*z[88];
    z[104]=z[54]*z[109];
    z[72]= - z[114] - z[72];
    z[72]=z[72]*z[61];
    z[132]=12*z[14];
    z[134]=n<T>(50,3)*z[10];
    z[39]=z[93] + z[72] + z[104] + z[134] + z[39] - z[132] - n<T>(35,3)*
    z[15];
    z[39]=z[3]*z[39];
    z[72]=7*z[12];
    z[93]= - z[112] + 10*z[116] + z[72] + n<T>(617,18)*z[3];
    z[93]=z[8]*z[93];
    z[104]=z[34]*z[50];
    z[112]=n<T>(29,3)*z[12] - z[94];
    z[112]=z[3]*z[112];
    z[53]=z[93] + n<T>(313,6)*z[104] + z[53] + z[112];
    z[53]=z[8]*z[53];
    z[93]=4*z[64];
    z[104]= - z[98] - z[93];
    z[104]=z[104]*z[34];
    z[82]= - z[88] + z[82];
    z[82]=z[82]*z[32];
    z[112]=z[10]*z[100];
    z[57]=z[82] + z[57] + z[112] + z[104];
    z[57]=z[2]*z[57];
    z[32]= - z[32] - n<T>(22,3)*z[8] + z[78] - z[69] + 4*z[10];
    z[32]=z[32]*z[130];
    z[78]=3*z[10];
    z[82]=z[78]*z[14];
    z[82]=z[82] + z[67];
    z[28]=z[28] - z[82];
    z[28]=z[28]*z[102];
    z[92]=1505*z[34] - 313*z[92];
    z[92]=z[92]*z[105];
    z[68]= - 299*z[68] + z[92];
    z[92]=n<T>(1,2)*z[8];
    z[68]=z[68]*z[92];
    z[28]=z[32] + z[28] + z[68];
    z[28]=z[6]*z[28];
    z[32]=2*z[13];
    z[68]=z[32] + 3*z[11];
    z[102]= - z[68]*z[94];
    z[104]=5*z[108];
    z[102]= - z[104] + z[102];
    z[102]=z[3]*z[102];
    z[108]= - z[13]*z[127];
    z[102]=z[108] + z[102];
    z[102]=z[102]*z[34];
    z[20]=z[28] + z[57] + z[53] - z[20] + z[102];
    z[20]=z[6]*z[20];
    z[28]= - z[44]*z[67];
    z[53]=z[114] - z[101];
    z[53]=z[53]*z[47];
    z[28]=z[28] + z[53];
    z[53]=z[106] - z[99];
    z[53]=z[11]*z[53];
    z[57]=z[13]*z[15];
    z[53]=z[57] + z[53];
    z[53]=z[11]*z[53];
    z[57]=z[11] + z[13];
    z[67]=z[57]*z[93];
    z[53]=z[53] + z[67];
    z[53]=z[3]*z[53];
    z[28]=2*z[28] + z[53];
    z[28]=z[28]*z[34];
    z[53]= - z[44]*z[74];
    z[53]=z[118] + z[53] - n<T>(137,6) + 7*z[50];
    z[53]=z[8]*z[53];
    z[67]=n<T>(47,9) + 5*z[50];
    z[67]=z[67]*z[88];
    z[34]=z[34]*z[33];
    z[34]=z[53] + z[34] + z[67] - 42*z[10] + z[41] + n<T>(25,3)*z[12];
    z[34]=z[8]*z[34];
    z[45]= - z[45] + z[84];
    z[53]= - z[131]*z[46];
    z[67]= - z[14] - z[69] + z[106];
    z[67]=z[67]*z[119];
    z[18]=z[18] + z[31] + z[43] + z[35] + z[20] + z[89] + z[34] + z[28]
    + z[39] + z[67] + 2*z[45] + z[53];
    z[18]=z[1]*z[18];
    z[20]=z[26] - z[42];
    z[28]= - z[15]*z[100];
    z[20]=z[28] + 4*z[20] + z[84];
    z[28]= - 21*z[10] - z[60];
    z[28]=z[28]*z[92];
    z[31]= - z[8] - n<T>(677,9)*z[2];
    z[31]=z[31]*z[90];
    z[34]=z[113] + 9*z[29];
    z[35]=z[2] + 6*z[8];
    z[39]= - z[35] - z[34];
    z[39]=z[4]*z[39];
    z[43]=z[100] - z[16];
    z[45]=z[10]*z[43];
    z[53]=z[6]*z[58];
    z[20]=z[39] - n<T>(295,18)*z[53] + z[31] + z[28] + 2*z[20] + z[45];
    z[20]=z[4]*z[20];
    z[28]=z[52]*z[61];
    z[28]= - z[95] + z[28] - z[41] - z[72];
    z[28]=z[5]*z[28];
    z[31]= - z[114] - z[52];
    z[39]=4*z[14];
    z[45]=z[16] - z[39];
    z[45]=z[45]*z[78];
    z[53]= - z[49] + z[2];
    z[53]=z[53]*z[95];
    z[58]= - z[12] + z[78];
    z[58]=z[8]*z[58];
    z[60]=24*z[2] - z[8] + z[132] + 11*z[15];
    z[60]=z[4]*z[60];
    z[28]=z[28] + z[60] + z[53] + z[58] + 2*z[31] + z[45];
    z[28]=z[5]*z[28];
    z[31]=z[69] - z[35];
    z[31]=z[2]*z[31];
    z[35]=n<T>(25,6)*z[36];
    z[31]=z[35] + z[31];
    z[31]=z[2]*z[31];
    z[31]=2*z[48] + z[31];
    z[31]=z[4]*z[31];
    z[45]= - z[15] - z[2];
    z[45]=z[45]*z[5]*z[4];
    z[45]=z[63] + z[45];
    z[45]=z[5]*z[45];
    z[37]=z[9]*z[37];
    z[31]=z[37] + 2*z[45] + z[38] + z[31];
    z[31]=z[9]*z[31];
    z[37]=5*z[8];
    z[38]= - z[37] + z[69] + z[119];
    z[38]=z[2]*z[38];
    z[35]=z[38] + z[129] + z[35];
    z[35]=z[2]*z[35];
    z[38]= - z[119] - z[8];
    z[38]=z[2]*z[38];
    z[38]=z[38] - z[129] - z[36];
    z[38]=z[38]*z[73];
    z[45]=z[10]*z[82];
    z[45]=z[45] + z[48];
    z[20]=z[31] + z[28] + z[20] + z[38] + 2*z[45] + z[35];
    z[20]=z[9]*z[20];
    z[28]=z[19] - z[34];
    z[28]=z[13]*z[28];
    z[28]=5*z[124] + static_cast<T>(8)+ z[28];
    z[28]=z[4]*z[28];
    z[31]=z[54]*z[13];
    z[34]=z[44]*z[8];
    z[35]=n<T>(169,6)*z[13] - z[34];
    z[35]=z[8]*z[35];
    z[35]=n<T>(97,6) + z[35];
    z[35]=z[8]*z[35];
    z[28]=z[28] - 5*z[73] - n<T>(57,2)*z[2] + z[35] - n<T>(14,3)*z[31] - z[46]
    - 29*z[15] + 7*z[16] + z[128];
    z[28]=z[4]*z[28];
    z[35]= - 5*z[26] - 2*z[54];
    z[35]=2*z[35] - n<T>(7,3)*z[52];
    z[35]=z[11]*z[35];
    z[38]=z[2]*z[11];
    z[45]=static_cast<T>(6)+ z[38];
    z[45]=z[45]*z[95];
    z[53]=z[6]*z[48];
    z[36]= - n<T>(662,3)*z[36] + n<T>(295,2)*z[53];
    z[36]=z[6]*z[36];
    z[53]= - z[15] - 6*z[12];
    z[53]=z[11]*z[53];
    z[53]= - z[38] + static_cast<T>(6)+ z[53];
    z[58]= - z[6]*z[111];
    z[58]= - 4*z[8] + z[58];
    z[58]=z[6]*z[58];
    z[53]=2*z[53] + z[58];
    z[53]=z[5]*z[53];
    z[58]=10*z[16];
    z[35]=z[53] + 11*z[4] + n<T>(1,3)*z[36] + z[45] + n<T>(295,18)*z[8] + z[35]
    - n<T>(17,3)*z[12] + 15*z[15] - z[58] - z[14];
    z[35]=z[5]*z[35];
    z[36]=n<T>(1,3)*z[12];
    z[41]= - z[41] - z[36];
    z[41]=z[41]*z[100];
    z[41]=z[41] + z[84] + z[26] + z[83];
    z[45]=z[16] + z[76];
    z[45]= - z[126] + z[100] + 5*z[45] - z[122];
    z[45]=z[10]*z[45];
    z[53]= - z[59] - z[69] + z[123];
    z[59]=z[11]*z[15];
    z[60]= - n<T>(581,18) + 6*z[59];
    z[60]=z[2]*z[60];
    z[53]=z[60] + 2*z[53] + n<T>(67,6)*z[8];
    z[53]=z[2]*z[53];
    z[21]= - z[21] - z[66];
    z[60]= - 5*z[17] - z[78];
    z[60]= - z[110] + 5*z[60] + n<T>(52,3)*z[8];
    z[60]=z[2]*z[60];
    z[21]=5*z[21] + z[60];
    z[21]=z[2]*z[21];
    z[60]=z[14]*z[119];
    z[60]=z[42] + z[60];
    z[49]=z[60]*z[49];
    z[48]=z[49] - z[48];
    z[21]=2*z[48] + z[21];
    z[21]=z[6]*z[21];
    z[48]=n<T>(31,6)*z[8] + n<T>(41,6)*z[12] + 24*z[10];
    z[48]=z[8]*z[48];
    z[20]=z[20] + z[35] + z[28] + z[21] + z[53] + z[48] + 2*z[41] + 
    z[45];
    z[20]=z[9]*z[20];
    z[21]=z[39] + z[120];
    z[28]=z[13]*z[42];
    z[35]= - z[11]*z[52];
    z[21]=z[35] + 6*z[28] + 3*z[21] - n<T>(25,3)*z[10];
    z[28]=z[94]*z[44];
    z[35]= - z[14]*z[28];
    z[39]= - static_cast<T>(3)- z[125];
    z[35]=z[35] + 4*z[39] + z[104];
    z[35]=z[3]*z[35];
    z[21]=2*z[21] + z[35];
    z[21]=z[3]*z[21];
    z[35]= - z[81] + n<T>(262,9) - 7*z[64];
    z[35]=z[2]*z[35];
    z[39]= - n<T>(589,18) - 5*z[64];
    z[39]=z[3]*z[39];
    z[35]=z[35] + 9*z[8] + z[39] + z[69] - z[78];
    z[35]=z[2]*z[35];
    z[39]= - z[52] - z[26] + 12*z[42];
    z[41]=z[44]*z[30];
    z[41]=n<T>(377,9) + 2*z[41];
    z[41]=z[3]*z[41];
    z[41]=z[41] + z[46] - z[119];
    z[42]=n<T>(425,18)*z[13] + z[28];
    z[42]=z[3]*z[42];
    z[45]=z[8]*z[33];
    z[42]=z[45] - n<T>(736,9) + z[42];
    z[42]=z[8]*z[42];
    z[41]=2*z[41] + z[42];
    z[41]=z[8]*z[41];
    z[42]=static_cast<T>(608)- n<T>(313,2)*z[50];
    z[42]=z[8]*z[42];
    z[42]= - 1118*z[3] + z[42];
    z[42]=z[42]*z[105];
    z[42]=8*z[30] + z[42];
    z[42]=z[8]*z[42];
    z[22]=z[42] + n<T>(313,9)*z[22];
    z[22]=z[6]*z[22];
    z[42]=36*z[14] + n<T>(50,3)*z[17] - z[43];
    z[42]=z[10]*z[42];
    z[21]=z[22] + z[35] + z[41] + z[21] + 2*z[39] + z[42];
    z[21]=z[6]*z[21];
    z[22]=z[13]*z[55];
    z[35]=n<T>(70,3)*z[15];
    z[22]= - 8*z[22] + 23*z[14] - z[35];
    z[22]=z[13]*z[22];
    z[39]=z[88]*z[44];
    z[41]= - z[91] + z[15];
    z[41]=z[41]*z[39];
    z[28]= - 4*z[34] - n<T>(97,6)*z[13] + z[28];
    z[28]=z[8]*z[28];
    z[24]= - z[40] - 7*z[24];
    z[24]=z[6]*z[24];
    z[42]= - z[12] - 9*z[2];
    z[24]=2*z[42] + z[24];
    z[24]=z[6]*z[24];
    z[42]=z[62]*z[56];
    z[22]=z[42] + z[24] + z[28] + z[41] - n<T>(69,2) + z[22];
    z[22]=z[4]*z[22];
    z[24]= - n<T>(67,9)*z[3] + z[92];
    z[24]=z[24]*z[37];
    z[24]=z[24] + n<T>(11,2)*z[71];
    z[24]=z[6]*z[24];
    z[28]=569*z[3] - n<T>(571,2)*z[8];
    z[24]=n<T>(1,9)*z[28] + z[24];
    z[24]=z[6]*z[24];
    z[24]=z[24] + n<T>(43,2) - n<T>(322,9)*z[64];
    z[24]=z[6]*z[24];
    z[28]=z[2]*z[87];
    z[37]=n<T>(211,18) - z[98];
    z[37]=z[11]*z[37];
    z[24]=z[24] + z[28] + z[37] + z[97];
    z[24]=z[5]*z[24];
    z[28]=n<T>(428,9) - z[98];
    z[28]=z[11]*z[28];
    z[28]=z[96] + z[28] + n<T>(343,18)*z[79];
    z[28]=z[2]*z[28];
    z[37]= - 623*z[3] + z[103];
    z[37]=z[8]*z[37];
    z[37]=z[77] + n<T>(1,3)*z[37];
    z[37]=z[8]*z[37];
    z[23]=z[37] + z[23];
    z[23]=z[6]*z[23];
    z[37]=270*z[3] - n<T>(2513,9)*z[8];
    z[37]=z[8]*z[37];
    z[23]=z[23] - 24*z[30] + z[37];
    z[23]=z[6]*z[23];
    z[30]= - n<T>(3499,18) + 12*z[64];
    z[30]=z[3]*z[30];
    z[23]=z[23] + n<T>(607,3)*z[8] + z[14] + z[30];
    z[23]=z[6]*z[23];
    z[30]= - z[26]*z[65];
    z[30]=z[30] - n<T>(32,3)*z[12] - 13*z[16] + 16*z[15];
    z[30]=z[11]*z[30];
    z[37]= - z[16]*z[61];
    z[37]=n<T>(1727,18) + z[37];
    z[37]=z[37]*z[64];
    z[23]=z[24] + z[23] + z[28] + z[37] - n<T>(44,9) + z[30];
    z[23]=z[5]*z[23];
    z[24]=z[114] + z[80];
    z[24]=z[24]*z[61];
    z[24]=z[24] + n<T>(10,3)*z[31] + n<T>(17,6)*z[12] + z[106] - 35*z[15];
    z[24]=z[11]*z[24];
    z[28]=z[51] - z[54];
    z[28]=z[28]*z[109];
    z[28]=z[28] - z[134] + 22*z[14] - n<T>(41,3)*z[15];
    z[28]=z[13]*z[28];
    z[30]=static_cast<T>(2)- z[125];
    z[30]=z[13]*z[30];
    z[31]= - z[11]*z[106];
    z[31]=static_cast<T>(2)+ z[31];
    z[31]=z[11]*z[31];
    z[30]=z[30] + z[31];
    z[30]=z[30]*z[88];
    z[24]=z[30] + z[24] - n<T>(11,2) + z[28];
    z[24]=z[3]*z[24];
    z[25]= - z[61]*z[25];
    z[25]=z[25] + z[133] + z[100];
    z[25]=z[11]*z[25];
    z[28]= - n<T>(179,9) - z[59];
    z[28]=z[28]*z[61];
    z[30]= - z[15]*z[65];
    z[30]=static_cast<T>(5)+ z[30];
    z[30]=z[30]*z[79];
    z[28]=z[28] + z[30];
    z[28]=z[2]*z[28];
    z[30]= - n<T>(73,3)*z[15] + z[70];
    z[30]=z[11]*z[30];
    z[30]=n<T>(1043,18) + z[30];
    z[30]=z[11]*z[30];
    z[30]=z[30] + 6*z[79];
    z[30]=z[3]*z[30];
    z[25]=z[28] + z[30] + n<T>(43,18) + z[25];
    z[25]=z[2]*z[25];
    z[28]=z[10]*z[69];
    z[28]=z[28] + z[127] - n<T>(1,3)*z[54];
    z[28]=z[28]*z[32];
    z[30]= - 5*z[15] - n<T>(8,3)*z[12];
    z[30]=z[12]*z[30];
    z[30]=z[30] + z[40] + z[84];
    z[30]=z[30]*z[61];
    z[31]= - n<T>(547,18)*z[13] + z[39];
    z[31]=z[3]*z[31];
    z[32]= - z[34] + n<T>(115,6)*z[13] - 7*z[33];
    z[32]=z[8]*z[32];
    z[31]=z[32] + n<T>(440,9) + z[31];
    z[31]=z[8]*z[31];
    z[18]=z[18] + z[20] + z[23] + z[22] + z[21] + z[25] + z[31] + z[24]
    + z[30] + z[28] - 32*z[10] + n<T>(50,3)*z[12] - z[35] + 21*z[14] - 
    z[69] + 8*z[16];
    z[18]=z[1]*z[18];
    z[19]=z[19]*z[7];
    z[20]=z[120]*z[7];
    z[21]=z[19] + 2*z[20];
    z[22]= - z[107] - 1;
    z[21]=z[21]*z[22];
    z[22]=z[2]*z[7];
    z[22]= - z[20] + z[22];
    z[22]=z[6]*z[22];
    z[23]=z[8]*z[7];
    z[24]=z[13]*z[23];
    z[22]=z[22] + 2*z[7] + z[24];
    z[22]=z[22]*z[86];
    z[24]=z[52] - z[26];
    z[25]=z[11]*z[24];
    z[25]=z[25] - z[120];
    z[25]=z[5]*z[7]*z[25]*z[61];
    z[21]=z[25] + z[22] + z[21];
    z[21]=z[9]*z[21];
    z[22]=3*z[7];
    z[25]=z[2]*z[22];
    z[19]=z[25] - z[20] + z[19];
    z[19]=z[6]*z[19];
    z[25]= - z[120]*z[61];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[22];
    z[20]= - z[4]*z[27]*z[20];
    z[24]=z[24]*z[61];
    z[24]= - 7*z[120] + z[24];
    z[24]=z[7]*z[24]*z[47];
    z[24]= - n<T>(5,6) + z[24];
    z[24]=z[5]*z[24];
    z[26]=z[22]*z[13];
    z[27]= - static_cast<T>(13)+ z[26];
    z[27]=z[8]*z[27];
    z[19]=z[21] + z[24] + z[20] + z[19] - 8*z[2] + z[27] + z[25] + 
    z[119] - z[36] - z[123] - z[16] + 26*z[14];
    z[19]=z[9]*z[19];
    z[20]=z[23]*z[75];
    z[21]=z[22]*z[3];
    z[20]=z[21] - z[20];
    z[21]= - static_cast<T>(1)+ z[50];
    z[21]=z[21]*z[23];
    z[21]=z[21] + z[20];
    z[21]=z[6]*z[21];
    z[24]=z[115] - z[69];
    z[25]= - z[12] - 5*z[14] - z[106] + z[24];
    z[27]= - z[3]*z[68];
    z[27]= - static_cast<T>(2)+ z[27];
    z[27]=z[7]*z[27];
    z[26]= - n<T>(521,9) - z[26];
    z[26]=z[8]*z[26];
    z[21]=z[21] + n<T>(69,2)*z[2] + z[26] + z[27] + 2*z[25] + n<T>(2155,18)*z[3]
   ;
    z[21]=z[6]*z[21];
    z[25]=z[57]*z[64];
    z[26]=z[11]*z[120];
    z[26]=static_cast<T>(3)- 5*z[26];
    z[26]=z[11]*z[26];
    z[25]=z[25] + z[121] + z[26];
    z[25]=z[7]*z[25];
    z[22]= - z[120]*z[85]*z[22];
    z[20]= - z[23] + z[20];
    z[20]=z[6]*z[20];
    z[23]=static_cast<T>(2)- 3*z[64];
    z[23]=z[7]*z[23];
    z[20]=z[23] + z[20];
    z[20]=z[6]*z[20];
    z[23]= - z[11] + z[79];
    z[23]=z[7]*z[23];
    z[20]=z[20] + static_cast<T>(77)+ z[23];
    z[20]=z[6]*z[20];
    z[20]=z[20] + n<T>(19,3)*z[11] + z[22];
    z[20]=z[5]*z[20];
    z[22]=z[29] + z[24];
    z[22]=z[13]*z[22];
    z[23]=n<T>(2,3)*z[12] + z[58] + z[15];
    z[23]=z[11]*z[23];
    z[24]= - n<T>(313,2)*z[13] - 376*z[11];
    z[24]=z[3]*z[24];

    r += n<T>(125,9) + z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + n<T>(1,9)
      *z[24] + z[25] - 22*z[38] - n<T>(27,2)*z[117];
 
    return r;
}

template double qg_2lNLC_r21(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r21(const std::array<dd_real,31>&);
#endif
