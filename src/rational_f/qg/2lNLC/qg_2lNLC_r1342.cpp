#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1342(const std::array<T,31>& k) {
  T z[90];
  T r = 0;

    z[1]=k[1];
    z[2]=k[9];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[8];
    z[7]=k[14];
    z[8]=k[2];
    z[9]=k[5];
    z[10]=k[6];
    z[11]=k[3];
    z[12]=k[7];
    z[13]=k[26];
    z[14]=k[4];
    z[15]=k[10];
    z[16]=2*z[12];
    z[17]=19*z[4];
    z[18]=13*z[7];
    z[19]= - z[18] + z[16] + z[17];
    z[20]=2*z[9];
    z[21]=npow(z[7],2);
    z[22]=z[20]*z[21];
    z[19]=z[19]*z[22];
    z[23]=z[3]*z[5];
    z[24]=z[6]*z[3];
    z[25]=z[23] + z[24];
    z[26]=4*z[12];
    z[27]=n<T>(122,3)*z[7] - z[26] - 17*z[4];
    z[27]=z[7]*z[27];
    z[19]=z[19] + 18*z[25] + z[27];
    z[19]=z[9]*z[19];
    z[27]=2*z[8];
    z[28]=npow(z[10],2);
    z[29]= - z[28]*z[27];
    z[30]=5*z[5];
    z[29]=z[29] - 17*z[6] - z[30] + z[12];
    z[31]=3*z[7];
    z[32]=z[7]*z[8];
    z[33]=6*z[32];
    z[34]= - static_cast<T>(5)+ z[33];
    z[34]=z[34]*z[31];
    z[35]=3*z[4];
    z[19]=z[19] + z[34] + 2*z[29] + z[35];
    z[19]=z[9]*z[19];
    z[29]=2*z[7];
    z[34]= - n<T>(16,3) + 5*z[32];
    z[34]=z[34]*z[29];
    z[36]=7*z[4];
    z[37]=z[36] - z[29];
    z[38]=z[21]*z[9];
    z[37]=z[37]*z[38];
    z[39]=6*z[4];
    z[40]= - z[39] + n<T>(25,3)*z[7];
    z[40]=z[7]*z[40];
    z[37]=z[40] + z[37];
    z[37]=z[37]*z[20];
    z[34]=z[37] + z[35] + z[34];
    z[34]=z[9]*z[34];
    z[37]=z[4]*z[11];
    z[40]=8*z[37];
    z[41]=npow(z[8],2);
    z[42]=z[41]*z[7];
    z[43]=3*z[42];
    z[44]= - 4*z[8] - z[43];
    z[44]=z[7]*z[44];
    z[34]=z[34] + z[44] + n<T>(7,3) + z[40];
    z[34]=z[9]*z[34];
    z[44]= - z[35] + z[29];
    z[44]=z[7]*z[44];
    z[22]=z[4]*z[22];
    z[22]=z[44] + z[22];
    z[22]=z[9]*z[22];
    z[44]=2*z[32];
    z[45]= - static_cast<T>(3)+ z[44];
    z[45]=z[7]*z[45];
    z[22]=z[22] + z[4] + z[45];
    z[22]=z[9]*z[22];
    z[45]= - 3*z[8] - z[42];
    z[45]=z[7]*z[45];
    z[22]=z[22] + z[45] + static_cast<T>(1)+ 2*z[37];
    z[22]=z[9]*z[22];
    z[45]=z[42] + z[8];
    z[46]=2*z[11];
    z[22]=z[22] + z[46] + z[45];
    z[22]=z[9]*z[22];
    z[47]=z[11]*z[27];
    z[22]=z[47] + z[22];
    z[47]=2*z[2];
    z[22]=z[22]*z[47];
    z[48]= - z[15]*z[27];
    z[48]=n<T>(37,3) + z[48];
    z[48]=z[8]*z[48];
    z[22]=z[22] + z[34] + 4*z[11] + z[48];
    z[22]=z[22]*z[47];
    z[34]= - z[28]*z[46];
    z[48]=z[6]*z[15];
    z[49]=n<T>(2,3)*z[28] - z[48];
    z[49]=z[8]*z[49];
    z[34]=z[49] + 18*z[6] + 7*z[15] + z[34];
    z[34]=z[8]*z[34];
    z[49]=z[8]*z[6];
    z[50]=static_cast<T>(5)- z[49];
    z[50]=z[8]*z[50];
    z[50]=z[50] - z[43];
    z[50]=z[7]*z[50];
    z[19]=z[22] + z[19] + z[50] + z[40] + static_cast<T>(11)+ z[34];
    z[19]=z[2]*z[19];
    z[22]=npow(z[3],2);
    z[34]=z[22]*z[6];
    z[40]=z[22]*z[5];
    z[50]=z[34] + z[40];
    z[51]=n<T>(1,3)*z[4];
    z[52]=z[28]*z[51];
    z[16]=z[16] + z[35];
    z[53]=9*z[7];
    z[16]=2*z[16] - z[53];
    z[16]=z[7]*z[16];
    z[16]= - z[28] + z[16];
    z[16]=z[7]*z[16];
    z[16]=z[16] + 4*z[50] + z[52];
    z[52]=4*z[9];
    z[16]=z[16]*z[52];
    z[54]= - z[23] - 17*z[24];
    z[55]=5*z[4];
    z[56]=z[28]*z[8];
    z[57]= - 6*z[12] + z[56];
    z[57]=22*z[7] + 2*z[57] - z[55];
    z[57]=z[7]*z[57];
    z[16]=z[16] + 2*z[54] + z[57];
    z[16]=z[9]*z[16];
    z[54]=z[15]*z[56];
    z[54]=z[48] - n<T>(4,3)*z[54];
    z[54]=z[8]*z[54];
    z[57]=z[6] - n<T>(2,3)*z[56];
    z[57]=z[8]*z[57];
    z[33]=z[33] - static_cast<T>(12)+ 5*z[57];
    z[33]=z[7]*z[33];
    z[57]=4*z[5];
    z[58]=z[28]*npow(z[11],2);
    z[59]= - n<T>(4,3)*z[58] - 3;
    z[59]=z[15]*z[59];
    z[60]=static_cast<T>(1)+ z[58];
    z[60]=z[4]*z[60];
    z[16]=z[19] + z[16] + z[33] + n<T>(4,3)*z[60] + z[54] - 8*z[6] + 12*z[3]
    + n<T>(14,3)*z[12] - z[57] + z[59];
    z[16]=z[2]*z[16];
    z[19]=n<T>(4,3)*z[12];
    z[33]=z[19] + z[36];
    z[33]=z[33]*z[21];
    z[36]=npow(z[7],3);
    z[54]=z[36]*z[9];
    z[59]= - z[19]*z[54];
    z[33]=z[33] + z[59];
    z[33]=z[9]*z[33];
    z[59]=z[4]*z[5];
    z[60]= - 4*z[25] + z[59];
    z[61]=n<T>(2,3)*z[12];
    z[62]=7*z[7] - z[61] - z[35];
    z[62]=z[7]*z[62];
    z[33]=z[33] + 2*z[60] + z[62];
    z[33]=z[9]*z[33];
    z[60]=npow(z[10],3);
    z[62]=z[60]*z[41];
    z[63]=10*z[6];
    z[64]=z[35] + z[63] + z[62];
    z[65]=static_cast<T>(1)- 10*z[32];
    z[65]=z[7]*z[65];
    z[33]=z[33] + 2*z[64] + z[65];
    z[33]=z[9]*z[33];
    z[64]=z[38]*z[4];
    z[65]= - z[4] + z[7];
    z[65]=z[7]*z[65];
    z[65]=z[65] + z[64];
    z[65]=z[9]*z[65];
    z[66]= - static_cast<T>(1)- z[44];
    z[66]=z[7]*z[66];
    z[65]=z[65] + z[4] + z[66];
    z[65]=z[9]*z[65];
    z[66]=4*z[37];
    z[45]=z[7]*z[45];
    z[45]=z[65] + z[45] + static_cast<T>(1)- z[66];
    z[45]=z[9]*z[45];
    z[65]= - z[11] - z[8];
    z[45]=4*z[65] + z[45];
    z[45]=z[45]*z[47];
    z[65]=z[11]*z[60];
    z[48]=n<T>(2,3)*z[65] + z[48];
    z[48]=z[8]*z[48];
    z[48]= - 16*z[6] + z[48];
    z[48]=z[8]*z[48];
    z[65]=z[49] - 2;
    z[67]=z[8]*z[65];
    z[67]=z[67] + z[42];
    z[67]=z[67]*z[31];
    z[33]=z[45] + z[33] + z[67] - 12*z[37] - static_cast<T>(20)+ z[48];
    z[33]=z[33]*z[47];
    z[45]=6*z[59] - 7*z[23] + 41*z[24];
    z[48]=z[60]*z[8];
    z[17]=n<T>(20,3)*z[12] + z[17];
    z[17]=z[7]*z[17];
    z[17]=8*z[48] + z[17];
    z[17]=z[7]*z[17];
    z[54]=z[12]*z[54];
    z[17]= - n<T>(34,3)*z[54] - 28*z[50] + z[17];
    z[17]=z[9]*z[17];
    z[54]=2*z[60];
    z[67]= - z[41]*z[54];
    z[67]= - z[12] + z[67];
    z[67]=19*z[7] + 2*z[67] - z[35];
    z[67]=z[7]*z[67];
    z[17]=z[17] + 2*z[45] + z[67];
    z[17]=z[9]*z[17];
    z[45]=2*z[4];
    z[67]=6*z[3];
    z[68]= - z[11]*z[67];
    z[68]=static_cast<T>(7)+ z[68];
    z[68]=z[68]*z[45];
    z[69]= - static_cast<T>(6)+ z[49];
    z[69]=z[69]*z[32];
    z[69]=3*z[69] + static_cast<T>(19)- 23*z[49];
    z[69]=z[7]*z[69];
    z[70]= - 35*z[3] + z[6];
    z[71]=3*z[49];
    z[72]= - z[15]*z[71];
    z[17]=z[33] + z[17] + z[69] + z[68] + 2*z[70] + z[72];
    z[17]=z[2]*z[17];
    z[33]=7*z[5];
    z[68]=z[33] + z[67];
    z[69]= - z[3]*z[68];
    z[70]=2*z[6];
    z[72]=n<T>(1,3)*z[12];
    z[73]= - z[72] - 7*z[3];
    z[73]=z[73]*z[70];
    z[74]=3*z[5] + z[3];
    z[74]=z[74]*z[45];
    z[69]=z[74] + z[69] + z[73];
    z[39]=z[39] + z[26] + n<T>(7,3)*z[62];
    z[39]=z[39]*z[21];
    z[34]= - z[40] + 2*z[34];
    z[62]=z[7]*z[12];
    z[48]= - z[48] - n<T>(31,3)*z[62];
    z[48]=z[48]*z[38];
    z[39]=z[48] + 8*z[34] + z[39];
    z[39]=z[39]*z[20];
    z[48]= - z[12] + z[63];
    z[62]=z[7]*z[65];
    z[48]= - 6*z[62] + 2*z[48] - z[4];
    z[48]=z[7]*z[48];
    z[17]=z[17] + z[39] + 2*z[69] + z[48];
    z[17]=z[2]*z[17];
    z[39]=z[35]*z[5];
    z[48]=3*z[23];
    z[39]=z[39] - z[48];
    z[62]= - z[50]*z[20];
    z[62]=z[62] + z[21] + 7*z[24] + z[39];
    z[62]=z[9]*z[62];
    z[63]=z[65]*z[32];
    z[63]=z[63] + static_cast<T>(1)- z[71];
    z[63]=z[7]*z[63];
    z[65]=3*z[6];
    z[69]=z[3]*z[11];
    z[69]=8*z[69];
    z[73]=static_cast<T>(5)- z[69];
    z[73]=z[4]*z[73];
    z[62]=z[62] + z[63] + z[73] - 17*z[3] - z[65];
    z[62]=z[62]*z[47];
    z[63]=z[41]*z[21];
    z[73]=npow(z[9],2);
    z[74]=npow(z[10],5);
    z[75]=z[74]*z[73]*z[63];
    z[75]=3*z[34] + z[75];
    z[75]=z[75]*z[52];
    z[76]=3*z[3];
    z[77]= - z[5] - z[3];
    z[77]=z[77]*z[76];
    z[77]=z[77] - 4*z[24];
    z[78]=z[46]*z[3];
    z[79]=static_cast<T>(11)- z[78];
    z[79]=z[3]*z[79];
    z[79]=12*z[5] + z[79];
    z[79]=z[4]*z[79];
    z[77]=4*z[77] + z[79];
    z[79]=static_cast<T>(7)- 10*z[49];
    z[79]=z[7]*z[79];
    z[79]=15*z[6] + z[79];
    z[79]=z[7]*z[79];
    z[62]=z[62] + z[75] + 2*z[77] + z[79];
    z[62]=z[62]*z[47];
    z[75]=z[4]*z[3];
    z[77]=35*z[5] + z[67];
    z[77]=z[77]*z[75];
    z[77]= - 14*z[50] + z[77];
    z[79]=z[36]*z[41];
    z[80]=npow(z[9],3);
    z[81]=z[74]*z[80]*z[79];
    z[82]=z[21]*z[6];
    z[62]=z[62] - n<T>(10,3)*z[81] + 2*z[77] + 19*z[82];
    z[62]=z[2]*z[62];
    z[77]=z[40]*z[4];
    z[62]=16*z[77] + z[62];
    z[62]=z[2]*z[62];
    z[67]=z[30] + z[67];
    z[67]=z[3]*z[67];
    z[81]= - static_cast<T>(7)+ z[78];
    z[81]=z[3]*z[81];
    z[30]= - z[30] + z[81];
    z[30]=z[4]*z[30];
    z[81]= - static_cast<T>(1)+ 2*z[49];
    z[83]=z[7]*z[81];
    z[83]= - z[70] + z[83];
    z[83]=z[7]*z[83];
    z[84]= - z[34]*z[20];
    z[30]=z[84] + z[83] + z[30] + z[67] + 6*z[24];
    z[30]=z[30]*z[47];
    z[67]=2*z[3];
    z[83]= - 9*z[5] - z[67];
    z[83]=z[83]*z[75];
    z[83]=3*z[50] + z[83];
    z[79]=n<T>(4,3)*z[79];
    z[84]= - npow(z[10],6)*npow(z[9],4)*z[79];
    z[30]=z[30] + z[84] + 4*z[83] - 7*z[82];
    z[30]=z[2]*z[30];
    z[30]= - 14*z[77] + z[30];
    z[30]=z[30]*npow(z[2],2);
    z[33]=z[33] + z[67];
    z[33]=z[33]*z[75];
    z[33]=z[82] - 2*z[50] + z[33];
    z[33]=z[2]*z[33];
    z[33]=6*z[77] + z[33];
    z[33]=z[33]*npow(z[2],3);
    z[83]= - z[1]*npow(z[2],4)*z[40]*z[45];
    z[33]=z[33] + z[83];
    z[83]=2*z[1];
    z[33]=z[33]*z[83];
    z[30]=z[30] + z[33];
    z[30]=z[30]*z[83];
    z[33]=npow(z[13],3);
    z[83]=z[33]*z[12];
    z[84]=z[83]*z[6];
    z[85]= - z[74]*z[84];
    z[86]=z[12]*z[13];
    z[87]=z[86]*z[4];
    z[88]=npow(z[13],2);
    z[89]=z[88]*z[12];
    z[87]=z[87] + z[89];
    z[87]=z[87]*z[4];
    z[83]=z[87] + z[83];
    z[74]= - z[4]*z[74]*z[83];
    z[74]=z[85] + z[74];
    z[74]=z[74]*npow(z[9],5);
    z[30]=z[30] + n<T>(4,3)*z[74] + z[62];
    z[30]=z[1]*z[30];
    z[59]= - z[21] - z[59] + z[25];
    z[59]=z[9]*z[59];
    z[62]=z[21]*z[27];
    z[74]= - z[6] - z[4];
    z[59]=z[59] + 3*z[74] + z[62];
    z[59]=z[9]*z[59];
    z[62]=static_cast<T>(1)- z[49];
    z[62]=z[8]*z[62];
    z[62]=z[62] - z[42];
    z[62]=z[7]*z[62];
    z[59]=z[59] + z[62] + 6*z[37] + static_cast<T>(8)+ z[71];
    z[59]=z[59]*z[47];
    z[62]=npow(z[28],2);
    z[74]=z[62]*z[42];
    z[74]=6*z[50] - n<T>(7,3)*z[74];
    z[74]=z[74]*z[20];
    z[39]= - 10*z[24] - z[39];
    z[39]=z[74] + 4*z[39] - 7*z[21];
    z[39]=z[9]*z[39];
    z[74]=4*z[6];
    z[69]= - static_cast<T>(7)+ z[69];
    z[69]=z[4]*z[69];
    z[69]=z[69] + 29*z[3] + z[74];
    z[71]=static_cast<T>(10)- z[71];
    z[71]=z[71]*z[32];
    z[71]=8*z[81] + z[71];
    z[71]=z[7]*z[71];
    z[39]=z[59] + z[39] + 2*z[69] + z[71];
    z[39]=z[39]*z[47];
    z[59]=z[68]*z[76];
    z[68]= - static_cast<T>(13)+ z[78];
    z[68]=z[3]*z[68];
    z[68]= - 21*z[5] + z[68];
    z[68]=z[4]*z[68];
    z[59]=z[68] + z[59] + 32*z[24];
    z[38]= - z[8]*z[38];
    z[38]=n<T>(5,3)*z[63] + z[38];
    z[38]=z[9]*z[62]*z[38];
    z[34]= - 7*z[34] + z[38];
    z[34]=z[34]*z[52];
    z[38]= - static_cast<T>(19)+ 18*z[49];
    z[38]=z[7]*z[38];
    z[38]= - 38*z[6] + z[38];
    z[38]=z[7]*z[38];
    z[34]=z[39] + z[34] + 2*z[59] + z[38];
    z[34]=z[2]*z[34];
    z[38]= - z[62]*z[73]*z[79];
    z[39]= - 15*z[5] - z[67];
    z[39]=z[39]*z[75];
    z[38]=z[38] - 6*z[82] + 8*z[50] + z[39];
    z[34]=2*z[38] + z[34];
    z[34]=z[2]*z[34];
    z[38]=z[62]*z[88];
    z[39]=2*z[62];
    z[49]= - z[86]*z[39];
    z[50]=z[13]*z[62];
    z[59]=z[12]*z[39];
    z[50]=z[50] + z[59];
    z[50]=z[4]*z[50];
    z[49]=z[50] + z[38] + z[49];
    z[49]=z[4]*z[49];
    z[39]=z[39]*z[89];
    z[50]=z[62]*z[33];
    z[39]=z[39] - z[50];
    z[49]=z[49] - z[39];
    z[49]=z[4]*z[49];
    z[39]= - z[6]*z[39];
    z[39]=z[39] + z[49];
    z[39]=z[9]*z[39];
    z[38]= - z[14]*z[38];
    z[49]=z[13]*z[14];
    z[59]= - z[12]*z[11];
    z[59]= - z[49] + z[59];
    z[59]=z[4]*z[62]*z[59];
    z[38]=z[38] + z[59];
    z[38]=z[4]*z[38];
    z[50]=z[50]*z[14];
    z[38]= - z[50] + z[38];
    z[38]=z[4]*z[38];
    z[50]= - z[6]*z[50];
    z[38]=z[39] + z[50] + z[38];
    z[38]=z[38]*z[80];
    z[38]= - z[77] + n<T>(1,3)*z[38];
    z[30]=z[30] + 4*z[38] + z[34];
    z[30]=z[1]*z[30];
    z[34]=z[7]*z[14];
    z[38]=z[14]*z[15];
    z[39]= - static_cast<T>(17)- 4*z[38];
    z[39]=z[39]*z[61]*z[34];
    z[50]=2*z[38];
    z[59]=static_cast<T>(5)+ z[50];
    z[59]=z[59]*z[19];
    z[39]=z[39] + z[35] + z[59] + z[65];
    z[39]=z[7]*z[39];
    z[59]= - z[15]*z[19];
    z[39]=z[59] + z[39];
    z[39]=z[7]*z[39];
    z[59]=z[60]*z[88];
    z[62]=z[54]*z[86];
    z[59]=z[59] - z[62];
    z[62]=z[6]*z[59];
    z[63]= - z[45] + z[13];
    z[60]=z[60]*z[63];
    z[54]=z[12]*z[54];
    z[54]=z[54] + z[60];
    z[54]=z[4]*z[54];
    z[54]=z[54] + z[59];
    z[54]=z[4]*z[54];
    z[54]=z[62] + z[54];
    z[54]=z[9]*z[54];
    z[59]=z[83]*z[4];
    z[59]=z[59] + z[84];
    z[54]=z[54] - z[59];
    z[54]=z[54]*z[20];
    z[60]=z[4]*z[13];
    z[60]=z[60] + z[88] + z[86];
    z[60]=z[4]*z[60];
    z[62]=z[89] + z[33];
    z[60]=z[60] + z[62];
    z[60]=z[4]*z[60];
    z[62]=z[6]*z[62];
    z[60]=z[62] + z[60];
    z[36]=z[36]*z[12];
    z[36]=z[54] + 2*z[60] - 31*z[36];
    z[36]=z[9]*z[36];
    z[54]=z[49] + 2;
    z[60]=z[54]*z[88];
    z[60]=z[60] + z[86];
    z[60]=n<T>(1,3)*z[60];
    z[54]= - z[13]*z[54];
    z[62]= - z[4]*z[49];
    z[54]=z[54] + z[62];
    z[51]=z[54]*z[51];
    z[23]=z[51] - z[60] + z[23];
    z[23]=z[4]*z[23];
    z[51]= - z[60] - z[22];
    z[51]=z[6]*z[51];
    z[23]=z[23] - z[40] + z[51];
    z[17]=z[30] + z[17] + n<T>(2,3)*z[36] + 4*z[23] + z[39];
    z[17]=z[1]*z[17];
    z[23]=z[49] + 1;
    z[30]= - z[13]*z[23];
    z[36]= - n<T>(2,3) - z[49];
    z[36]=z[4]*z[36];
    z[30]=z[36] + z[30] - z[61];
    z[30]=z[4]*z[30];
    z[23]=z[23]*z[88];
    z[30]= - z[23] + z[30];
    z[30]=z[4]*z[30];
    z[23]= - z[23] - 2*z[22];
    z[23]=z[6]*z[23];
    z[23]=z[30] + z[40] + z[23];
    z[18]= - z[18] + z[35] + 13*z[12] - z[56];
    z[18]=z[18]*z[21];
    z[30]= - z[28]*z[72];
    z[36]=z[13] + z[61];
    z[36]=z[4]*z[36];
    z[36]=z[88] + z[36];
    z[36]=z[4]*z[36];
    z[30]=z[36] + z[33] + z[30];
    z[30]=z[4]*z[30];
    z[36]=z[59]*z[9];
    z[39]=z[6]*z[33];
    z[30]= - z[36] + z[39] + z[30];
    z[30]=z[30]*z[20];
    z[18]=z[30] + 2*z[23] + z[18];
    z[18]=z[9]*z[18];
    z[23]=z[11]*z[15];
    z[30]=z[50] + z[23];
    z[30]=z[11]*z[30];
    z[39]=z[15]*npow(z[14],2);
    z[30]=2*z[39] + z[30];
    z[28]=z[30]*z[28];
    z[28]=z[15] + z[28];
    z[28]=z[28]*z[72];
    z[30]=static_cast<T>(2)- z[58];
    z[30]=z[30]*z[72];
    z[30]=z[30] + z[4];
    z[30]=z[4]*z[30];
    z[24]=z[30] + z[28] + z[24];
    z[28]=z[12]*z[14];
    z[30]=3*z[38];
    z[39]=static_cast<T>(10)+ z[30];
    z[39]=z[39]*z[28];
    z[50]=static_cast<T>(1)- z[38];
    z[34]= - 4*z[34] + n<T>(5,3)*z[50] + z[39];
    z[34]=z[7]*z[34];
    z[39]= - static_cast<T>(2)- z[38];
    z[39]=z[12]*z[39];
    z[39]=n<T>(1,3)*z[15] + z[39];
    z[39]=2*z[39] - z[6];
    z[34]=2*z[39] + z[34];
    z[34]=z[7]*z[34];
    z[18]=z[18] + 2*z[24] + z[34];
    z[16]=z[17] + 2*z[18] + z[16];
    z[16]=z[1]*z[16];
    z[17]=n<T>(1,3)*z[7];
    z[18]= - static_cast<T>(10)+ 23*z[32];
    z[18]=z[18]*z[17];
    z[18]=z[18] - z[45] + z[74] + z[57] + z[12];
    z[24]= - z[61] - z[76];
    z[24]=z[6]*z[24];
    z[24]= - z[48] + z[24];
    z[34]=z[44] - 1;
    z[34]=z[34]*z[53];
    z[39]= - z[12] - z[4];
    z[39]=4*z[39] - z[34];
    z[39]=z[7]*z[39];
    z[24]=12*z[64] + 2*z[24] + z[39];
    z[24]=z[9]*z[24];
    z[18]=2*z[18] + z[24];
    z[18]=z[9]*z[18];
    z[24]= - static_cast<T>(5)+ 4*z[32];
    z[24]=z[24]*z[7];
    z[39]= - 9*z[4] - z[24];
    z[39]=z[7]*z[39];
    z[39]=z[39] + 7*z[64];
    z[39]=z[9]*z[39];
    z[44]= - static_cast<T>(5)+ n<T>(37,3)*z[32];
    z[44]=z[7]*z[44];
    z[39]=z[39] + z[45] + z[44];
    z[39]=z[9]*z[39];
    z[44]= - z[11]*z[35];
    z[48]= - n<T>(29,3)*z[8] + z[43];
    z[48]=z[7]*z[48];
    z[39]=z[39] + z[44] + z[48];
    z[39]=z[9]*z[39];
    z[44]=z[42] - z[27];
    z[48]=z[7]*z[44];
    z[45]= - z[45] + z[7];
    z[45]=z[7]*z[45];
    z[45]=z[45] + z[64];
    z[45]=z[9]*z[45];
    z[50]= - static_cast<T>(2)+ z[32];
    z[50]=z[7]*z[50];
    z[45]=z[45] + z[4] + z[50];
    z[45]=z[9]*z[45];
    z[45]=z[45] + static_cast<T>(1)+ z[48];
    z[45]=z[9]*z[45];
    z[42]=2*z[42];
    z[45]=z[45] + z[8] - z[42];
    z[45]=z[9]*z[45];
    z[41]=z[41] + z[45];
    z[41]=z[41]*z[47];
    z[39]=z[41] + z[39] - z[43] - 3*z[11] + n<T>(4,3)*z[8];
    z[39]=z[9]*z[39];
    z[41]= - n<T>(2,3) + z[23];
    z[41]=z[8]*z[41];
    z[41]= - n<T>(7,3)*z[11] + z[41];
    z[41]=z[8]*z[41];
    z[39]=z[41] + z[39];
    z[39]=z[39]*z[47];
    z[41]= - static_cast<T>(8)+ 77*z[32];
    z[17]=z[41]*z[17];
    z[34]= - 14*z[4] - z[34];
    z[34]=z[7]*z[34];
    z[34]=z[34] + 19*z[64];
    z[34]=z[9]*z[34];
    z[17]=z[34] - z[55] + z[17];
    z[17]=z[9]*z[17];
    z[31]=z[44]*z[31];
    z[17]=z[17] + z[31] - n<T>(17,3) - z[66];
    z[17]=z[9]*z[17];
    z[31]=z[8]*z[15];
    z[34]=z[31] - static_cast<T>(1)- 4*z[23];
    z[34]=z[8]*z[34];
    z[17]=z[39] + z[17] - z[42] + n<T>(4,3)*z[11] + z[34];
    z[17]=z[2]*z[17];
    z[34]= - z[6] - z[15] - z[5];
    z[34]=z[34]*z[27];
    z[39]=z[15]*z[46];
    z[41]=z[11]*z[61];
    z[17]=z[17] + z[18] - n<T>(14,3)*z[32] - n<T>(8,3)*z[37] + z[34] + z[41] + 3
    + z[39];
    z[17]=z[2]*z[17];
    z[18]= - static_cast<T>(8)- z[30];
    z[18]=z[18]*z[28];
    z[30]=z[14] + n<T>(1,3)*z[8];
    z[30]=z[30]*z[29];
    z[32]=static_cast<T>(7)+ 10*z[38];
    z[18]=z[30] - z[31] + n<T>(1,3)*z[32] + z[18];
    z[18]=z[18]*z[29];
    z[29]=z[13] + z[19];
    z[29]=z[4]*z[29];
    z[29]=z[29] + z[88] - z[86];
    z[29]=z[4]*z[29];
    z[30]=z[89] - z[33];
    z[29]=z[29] - z[30];
    z[29]=z[4]*z[29];
    z[30]= - z[6]*z[30];
    z[29]= - z[36] + z[30] + z[29];
    z[29]=z[29]*z[52];
    z[30]=z[33]*z[14];
    z[30]=z[30] + n<T>(1,3)*z[86];
    z[32]=z[72]*z[11];
    z[33]= - z[32] - n<T>(4,3) - z[49];
    z[33]=z[4]*z[33];
    z[34]=z[88]*z[14];
    z[33]= - z[34] + z[33];
    z[33]=z[4]*z[33];
    z[33]=z[33] - z[30];
    z[33]=z[4]*z[33];
    z[22]= - z[22] - z[30];
    z[22]=z[6]*z[22];
    z[22]=z[33] - z[40] + z[22];
    z[21]=z[21]*z[35];
    z[21]=z[29] + 4*z[22] + z[21];
    z[21]=z[9]*z[21];
    z[22]=static_cast<T>(1)+ z[32];
    z[22]=z[22]*npow(z[4],2);
    z[22]=z[22] + z[25];
    z[24]= - 7*z[12] - z[24];
    z[24]=z[7]*z[24];
    z[22]=2*z[22] + z[24];
    z[21]=2*z[22] + z[21];
    z[21]=z[9]*z[21];
    z[22]=static_cast<T>(1)+ n<T>(2,3)*z[38];
    z[22]=2*z[22] + n<T>(1,3)*z[23];
    z[22]=z[22]*z[26];
    z[19]= - z[37]*z[19];
    z[16]=z[16] + z[17] + z[21] + z[18] + z[19] + z[70] + z[22] - n<T>(5,3)*
    z[15] + 2*z[5];
    z[16]=z[1]*z[16];
    z[17]= - static_cast<T>(1)- z[38];
    z[17]=z[28] + 2*z[17] - z[23];
    z[17]=n<T>(2,3)*z[17] + z[31];
    z[18]=z[72] - z[7];
    z[18]=z[18]*z[20];
    z[19]= - z[14] + z[27];
    z[19]=z[7]*z[19];
    z[20]=n<T>(1,3)*z[9] + z[11] + n<T>(7,3)*z[8];
    z[20]=z[2]*z[20];
    z[17]=z[20] + z[18] + 2*z[17] + z[19];
    z[16]=2*z[17] + z[16];

    r += 2*z[16];
 
    return r;
}

template double qg_2lNLC_r1342(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1342(const std::array<dd_real,31>&);
#endif
