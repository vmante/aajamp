#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r538(const std::array<T,31>& k) {
  T z[65];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[9];
    z[6]=k[13];
    z[7]=k[12];
    z[8]=k[15];
    z[9]=k[10];
    z[10]=k[14];
    z[11]=k[2];
    z[12]=k[4];
    z[13]=k[5];
    z[14]=k[3];
    z[15]=n<T>(1,2)*z[13];
    z[16]=z[12]*z[10];
    z[17]=n<T>(1,2)*z[16];
    z[18]= - static_cast<T>(3)- z[17];
    z[18]=z[12]*z[18];
    z[18]=z[15] + z[18];
    z[18]=z[6]*z[18];
    z[19]=z[8]*z[13];
    z[20]=n<T>(1,2)*z[19];
    z[21]=z[3]*z[12];
    z[22]=z[9]*z[12];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[18] + n<T>(15,2)*z[21] - z[20] + 3*z[17] + n<T>(1,2)*z[22];
    z[18]=n<T>(1,2)*z[6];
    z[17]=z[17]*z[18];
    z[23]=z[9]*z[11];
    z[24]=z[7]*z[13];
    z[25]= - z[10]*z[11];
    z[25]=z[24] + z[25] - z[23];
    z[26]=n<T>(1,4)*z[8];
    z[25]=z[25]*z[26];
    z[27]=n<T>(1,4)*z[3];
    z[28]=npow(z[13],2);
    z[29]=z[28]*z[7];
    z[30]=z[29] + z[13];
    z[31]= - 27*z[12] + 17*z[30];
    z[31]=z[31]*z[27];
    z[32]=3*z[16];
    z[33]=static_cast<T>(13)+ z[32];
    z[34]= - n<T>(11,4)*z[13] - z[29];
    z[34]=z[7]*z[34];
    z[31]=z[31] + n<T>(1,4)*z[33] + z[34];
    z[31]=z[3]*z[31];
    z[33]= - z[12] + z[30];
    z[33]=z[3]*z[33];
    z[34]=z[12] - z[13];
    z[34]=z[6]*z[34];
    z[33]=z[33] + z[34];
    z[33]=z[4]*z[33];
    z[34]=z[10] + z[5];
    z[35]=n<T>(1,2)*z[9];
    z[36]=2*z[24];
    z[37]= - n<T>(5,2) + z[36];
    z[37]=z[7]*z[37];
    z[17]=n<T>(1,2)*z[33] + z[17] + z[31] + z[25] + z[37] - z[35] - z[34];
    z[17]=z[4]*z[17];
    z[25]=n<T>(3,4)*z[19];
    z[31]=n<T>(7,2)*z[21] - z[25] - n<T>(1,4)*z[22] - static_cast<T>(1)- n<T>(3,4)*z[16];
    z[31]=z[6]*z[31];
    z[33]=static_cast<T>(29)+ 5*z[21];
    z[33]=z[33]*z[27];
    z[37]=3*z[9];
    z[38]=2*z[7];
    z[39]=n<T>(1,2)*z[8];
    z[40]=3*z[5] + 5*z[10];
    z[31]=z[31] + z[33] - z[39] + z[38] + n<T>(1,2)*z[40] - z[37];
    z[31]=z[6]*z[31];
    z[33]= - n<T>(1,2) - 3*z[24];
    z[33]=n<T>(1,2)*z[33] - z[21];
    z[33]=z[3]*z[33];
    z[40]=n<T>(1,2)*z[10];
    z[41]= - static_cast<T>(5)- z[24];
    z[41]=z[7]*z[41];
    z[33]=z[33] - z[40] + z[41];
    z[33]=z[3]*z[33];
    z[41]=z[9]*z[5];
    z[42]=z[5]*z[11];
    z[43]=n<T>(1,2) + z[42];
    z[43]=z[43]*z[41];
    z[44]= - n<T>(5,2) - z[36];
    z[44]=z[7]*z[44];
    z[45]=z[8]*z[9];
    z[46]=z[11]*z[45];
    z[44]=z[46] + z[44] + n<T>(3,2)*z[10] - 2*z[9];
    z[44]=z[8]*z[44];
    z[46]=npow(z[5],2);
    z[47]=z[9] - z[7];
    z[47]=z[7]*z[47];
    z[17]=z[17] + z[31] + z[33] + z[44] + z[47] + z[46] + z[43];
    z[17]=z[4]*z[17];
    z[31]=n<T>(19,4)*z[9];
    z[33]=z[8] - z[31] - z[38];
    z[33]=z[8]*z[33];
    z[43]=static_cast<T>(3)+ 5*z[22];
    z[43]=z[43]*z[26];
    z[44]=npow(z[12],2);
    z[47]=z[44]*z[9];
    z[48]= - z[12] - n<T>(1,2)*z[47];
    z[48]=z[3]*z[8]*z[48];
    z[43]=z[48] + z[7] + z[43];
    z[43]=z[3]*z[43];
    z[48]=z[9]*z[10];
    z[49]=z[7]*z[9];
    z[33]=z[43] + z[33] + n<T>(1,2)*z[48] - z[49];
    z[33]=z[3]*z[33];
    z[43]=z[5]*z[14];
    z[43]=static_cast<T>(1)+ n<T>(1,2)*z[43];
    z[43]=z[9]*z[43];
    z[43]=z[43] - z[5] + z[40];
    z[50]= - z[5] + z[10];
    z[25]=z[50]*z[25];
    z[50]= - 3*z[19] + static_cast<T>(5)- z[22];
    z[27]=z[50]*z[27];
    z[25]=z[27] + z[25] + n<T>(3,2)*z[43] + z[38];
    z[25]=z[6]*z[25];
    z[27]=z[24] + 2;
    z[43]=n<T>(5,4)*z[22];
    z[50]= - z[20] + z[43] - z[27];
    z[50]=z[3]*z[50];
    z[51]=z[24] + n<T>(3,2);
    z[51]=z[51]*z[7];
    z[31]=z[50] - z[8] - z[31] + z[51];
    z[31]=z[3]*z[31];
    z[50]=z[7]*z[5];
    z[52]=n<T>(7,2)*z[41] + 13*z[50];
    z[53]=n<T>(5,2)*z[10];
    z[54]= - 2*z[5] + z[53];
    z[54]=z[8]*z[54];
    z[25]=z[25] + z[31] + n<T>(1,2)*z[52] + z[54];
    z[25]=z[6]*z[25];
    z[31]=npow(z[12],3);
    z[52]=z[31]*z[10];
    z[54]=z[31]*z[3];
    z[52]=z[52] - z[54];
    z[55]=z[52]*z[6];
    z[56]=z[54]*z[10];
    z[55]=z[55] - z[56];
    z[55]=z[55]*z[4];
    z[54]= - z[54]*z[48];
    z[56]= - z[9]*z[52];
    z[57]= - z[6]*z[56];
    z[54]= - z[55] + z[54] + z[57];
    z[54]=z[54]*npow(z[2],3);
    z[27]=z[7]*z[46]*z[27];
    z[57]=z[46]*z[13];
    z[36]= - z[36] + n<T>(1,2);
    z[36]=z[5]*z[36];
    z[36]= - z[9] - z[57] + z[36];
    z[36]=z[7]*z[36];
    z[36]=z[36] + z[45];
    z[36]=z[8]*z[36];
    z[58]=z[46]*z[9];
    z[17]=n<T>(1,4)*z[54] + z[17] + z[25] + z[33] + z[36] + z[58] + z[27];
    z[25]=npow(z[2],2);
    z[17]=z[17]*z[25];
    z[27]= - z[19] + static_cast<T>(1)- z[16];
    z[27]=z[6]*z[27];
    z[33]=5*z[9];
    z[36]=5*z[3];
    z[27]=z[27] - z[36] + z[8] - z[33] + 3*z[10];
    z[54]=n<T>(1,4)*z[6];
    z[27]=z[27]*z[54];
    z[59]= - z[24]*z[39];
    z[60]=z[24] + 1;
    z[61]=z[3]*z[60];
    z[62]=z[6]*z[20];
    z[59]=z[62] + z[59] + z[61];
    z[59]=z[4]*z[59];
    z[42]= - static_cast<T>(2)- z[42];
    z[42]=z[42]*z[41];
    z[61]=static_cast<T>(2)- z[22];
    z[61]=z[7]*z[61];
    z[37]= - z[37] + z[61];
    z[37]=z[7]*z[37];
    z[61]=z[24] + n<T>(3,4);
    z[61]=z[61]*z[7];
    z[40]=z[40] - z[61];
    z[40]=z[8]*z[40];
    z[62]=n<T>(1,2)*z[3];
    z[63]=n<T>(27,2) + 11*z[24];
    z[63]=z[63]*z[62];
    z[61]=z[63] - n<T>(1,4)*z[10] - z[61];
    z[61]=z[3]*z[61];
    z[27]=z[59] + z[27] + z[61] + z[40] + z[37] + z[46] + z[42];
    z[27]=z[4]*z[27];
    z[37]=z[7] - 3*z[3];
    z[36]=z[37]*z[36];
    z[37]=z[8]*z[10];
    z[36]=z[36] - 7*z[37] - 17*z[41] - 13*z[49];
    z[40]=z[8] + z[10];
    z[42]= - 7*z[3] + z[35] - z[40];
    z[42]=z[6]*z[42];
    z[36]=n<T>(1,2)*z[36] + z[42];
    z[36]=z[36]*z[18];
    z[42]= - n<T>(9,4)*z[9] - z[7];
    z[42]=z[7]*z[42];
    z[46]= - z[9] - z[7];
    z[46]=z[8]*z[46];
    z[42]=z[42] + z[46];
    z[42]=z[8]*z[42];
    z[46]=z[38] + z[62];
    z[46]=z[3]*z[46];
    z[59]=npow(z[7],2);
    z[46]= - 2*z[59] + z[46];
    z[46]=z[3]*z[46];
    z[27]=z[27] + z[36] + z[46] - z[58] + z[42];
    z[27]=z[4]*z[27];
    z[36]=z[10]*z[26];
    z[42]=z[5] + n<T>(3,4)*z[7];
    z[42]=z[3]*z[42];
    z[36]=z[42] + z[36] - z[41] - n<T>(3,4)*z[49];
    z[36]=z[6]*z[36];
    z[42]=npow(z[3],2);
    z[46]=z[42]*z[7];
    z[36]= - n<T>(11,4)*z[46] + z[36];
    z[36]=z[6]*z[36];
    z[40]= - z[6]*z[40];
    z[37]= - 3*z[37] + z[40];
    z[37]=z[37]*z[54];
    z[35]= - z[35] - z[7];
    z[35]=z[8]*z[35];
    z[40]=z[7]*z[62];
    z[35]=z[35] + z[40];
    z[35]=z[4]*z[35];
    z[40]=z[59]*z[8];
    z[35]=z[35] + z[37] - z[40] + n<T>(9,4)*z[46];
    z[35]=z[4]*z[35];
    z[37]= - npow(z[8],2)*z[49];
    z[35]=z[35] + z[37] + z[36];
    z[35]=z[4]*z[35];
    z[36]=z[8]*z[5];
    z[37]=z[42]*z[36];
    z[46]=z[41]*z[7];
    z[58]=z[3]*z[50];
    z[58]= - z[46] + z[58];
    z[58]=z[6]*z[58];
    z[37]=z[37] + z[58];
    z[58]=npow(z[6],2);
    z[61]=n<T>(1,2)*z[58];
    z[37]=z[37]*z[61];
    z[63]=z[7] + z[5];
    z[64]=z[3]*z[63];
    z[64]=z[64] - z[41] - z[49];
    z[64]=z[6]*z[64];
    z[42]=z[63]*z[42];
    z[42]=z[42] + z[64];
    z[42]=z[42]*z[61];
    z[61]=z[10]*z[58]*z[39];
    z[49]= - z[4]*z[8]*z[49];
    z[49]=z[61] + z[49];
    z[49]=z[4]*z[49];
    z[42]=z[42] + z[49];
    z[42]=z[4]*z[42];
    z[37]=z[37] + z[42];
    z[37]=z[1]*z[37];
    z[42]=7*z[7] - z[8];
    z[42]=z[3]*z[42];
    z[42]=z[42] + 5*z[50] - z[36];
    z[42]=z[3]*z[42];
    z[42]= - 5*z[46] + z[42];
    z[42]=z[42]*z[58];
    z[35]=n<T>(1,2)*z[37] + n<T>(1,4)*z[42] + z[35];
    z[35]=z[1]*z[35];
    z[20]=n<T>(7,2) + z[20] + z[24];
    z[20]=z[20]*z[62];
    z[37]=n<T>(1,4)*z[9];
    z[20]=z[20] + z[39] + z[37] - z[51];
    z[20]=z[3]*z[20];
    z[42]= - z[34]*z[39];
    z[20]=z[20] + z[42] - n<T>(5,4)*z[41] + z[59];
    z[20]=z[6]*z[20];
    z[41]= - static_cast<T>(1)- z[19];
    z[41]=z[41]*z[62];
    z[41]=z[41] + z[26] - z[37] - 4*z[7];
    z[41]=z[3]*z[41];
    z[42]=z[9] - z[5];
    z[42]=n<T>(5,4)*z[42];
    z[38]= - z[42] + z[38];
    z[38]=z[7]*z[38];
    z[36]=z[41] + z[38] - n<T>(5,4)*z[36];
    z[36]=z[3]*z[36];
    z[20]=z[20] - z[46] + z[36];
    z[20]=z[6]*z[20];
    z[36]= - z[5] - z[57];
    z[36]=z[36]*z[40];
    z[38]= - z[7]*z[42];
    z[38]=z[38] + z[45];
    z[38]=z[8]*z[38];
    z[26]= - z[26]*z[3]*z[9];
    z[26]=z[38] + z[26];
    z[26]=z[3]*z[26];
    z[20]=z[35] + z[27] + z[20] + z[36] + z[26];
    z[20]=z[1]*z[25]*z[20];
    z[17]=z[17] + z[20];
    z[17]=z[1]*z[17];
    z[20]=z[44]*z[3];
    z[26]=static_cast<T>(3)- z[16];
    z[26]=z[12]*z[26];
    z[15]= - n<T>(1,4)*z[20] - z[15] + z[26];
    z[15]=z[6]*z[15];
    z[26]=n<T>(5,4)*z[19];
    z[27]=n<T>(13,4)*z[16];
    z[35]=z[13]*z[5];
    z[15]=z[15] - n<T>(21,4)*z[21] - z[26] + n<T>(5,4)*z[24] + z[43] + z[27] - 3.
   /2. - z[35];
    z[15]=z[6]*z[15];
    z[21]= - z[11]*z[53];
    z[21]=n<T>(1,2)*z[24] - n<T>(5,2)*z[23] - static_cast<T>(3)+ z[21];
    z[21]=z[21]*z[39];
    z[23]=n<T>(1,4)*z[12];
    z[30]=n<T>(1,2)*z[20] - z[23] - z[30];
    z[30]=z[3]*z[30];
    z[36]= - static_cast<T>(3)+ 5*z[16];
    z[30]=n<T>(1,4)*z[36] + z[30];
    z[30]=z[3]*z[30];
    z[36]= - z[13] + n<T>(9,4)*z[12];
    z[36]=z[12]*z[36];
    z[38]=z[7]*npow(z[13],3);
    z[28]=z[38] + z[28] + z[36];
    z[28]=z[3]*z[28];
    z[32]= - static_cast<T>(7)- z[32];
    z[23]=z[32]*z[23];
    z[23]=z[28] + z[23] - z[29];
    z[23]=z[3]*z[23];
    z[28]=static_cast<T>(7)- z[16];
    z[28]=z[12]*z[28];
    z[28]=z[13] + n<T>(1,2)*z[28];
    z[28]=z[6]*z[28];
    z[28]=z[28] + static_cast<T>(1)+ n<T>(3,2)*z[16];
    z[28]=z[12]*z[28];
    z[28]= - n<T>(11,2)*z[20] + 3*z[13] + z[28];
    z[18]=z[28]*z[18];
    z[28]=z[12]*z[24];
    z[28]= - n<T>(3,2)*z[13] + z[28];
    z[28]=z[7]*z[28];
    z[18]=z[18] + z[23] + n<T>(1,2) + z[28];
    z[18]=z[4]*z[18];
    z[23]=z[7]*z[60];
    z[15]=z[18] + z[15] + z[30] + z[21] + z[23] - n<T>(5,2)*z[9] - z[5] - 4*
    z[10];
    z[15]=z[4]*z[15];
    z[18]= - static_cast<T>(1)+ n<T>(1,4)*z[35];
    z[18]=z[7]*z[18];
    z[21]= - z[11] + 2*z[12];
    z[21]=z[9]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[8]*z[21];
    z[18]=z[21] + z[18] - z[33] - z[5] - z[53];
    z[18]=z[8]*z[18];
    z[19]=z[34]*z[19];
    z[19]= - z[19] - z[10] + z[9];
    z[21]=z[5]*z[24];
    z[19]=z[21] + 5*z[19];
    z[21]=z[26] - n<T>(5,4) - z[22];
    z[21]=z[3]*z[21];
    z[23]=z[20]*z[37];
    z[16]= - n<T>(7,4) + z[16];
    z[16]=z[12]*z[16];
    z[16]= - n<T>(3,4)*z[14] + z[16];
    z[16]=z[9]*z[16];
    z[16]=z[23] + n<T>(9,4) + z[16];
    z[16]=z[6]*z[16];
    z[16]=z[16] + n<T>(1,4)*z[19] + z[21];
    z[16]=z[6]*z[16];
    z[19]=static_cast<T>(1)+ n<T>(5,2)*z[22];
    z[21]= - z[12] - z[47];
    z[21]=z[8]*z[21];
    z[19]=n<T>(3,2)*z[19] + z[21];
    z[19]=z[8]*z[19];
    z[21]=z[31]*z[9];
    z[21]=z[44] + z[21];
    z[21]=z[3]*z[21];
    z[21]=z[21] - n<T>(5,2)*z[12] - 3*z[47];
    z[21]=z[8]*z[21];
    z[21]=z[60] + z[21];
    z[21]=z[21]*z[62];
    z[22]= - static_cast<T>(1)- z[27];
    z[22]=z[9]*z[22];
    z[19]=z[21] + z[19] - z[7] - 2*z[10] + z[22];
    z[19]=z[3]*z[19];
    z[15]=z[15] + z[16] + z[19] + n<T>(3,2)*z[48] + z[18];
    z[15]=z[15]*z[25];
    z[15]=z[15] + z[17];
    z[15]=z[1]*z[15];
    z[16]= - z[56]*z[54];
    z[17]= - z[10]*z[47];
    z[18]=z[9]*z[20];
    z[16]=z[16] + z[17] + z[18];
    z[16]=z[6]*z[16];
    z[17]= - z[52]*z[54];
    z[18]=z[10]*z[44];
    z[17]=z[17] + z[18] - z[20];
    z[17]=z[6]*z[17];
    z[18]= - z[10]*z[20];
    z[17]= - n<T>(1,4)*z[55] + z[18] + z[17];
    z[17]=z[4]*z[17];
    z[18]=z[20]*z[48];
    z[16]=z[17] + z[18] + z[16];
    z[16]=z[16]*z[25];
    z[15]=z[16] + z[15];

    r += z[15]*z[1];
 
    return r;
}

template double qg_2lNLC_r538(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r538(const std::array<dd_real,31>&);
#endif
