#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r187(const std::array<T,31>& k) {
  T z[147];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[6];
    z[9]=k[13];
    z[10]=k[2];
    z[11]=k[7];
    z[12]=k[23];
    z[13]=k[9];
    z[14]=k[14];
    z[15]=k[29];
    z[16]=k[24];
    z[17]=k[3];
    z[18]=k[18];
    z[19]=k[20];
    z[20]=k[25];
    z[21]=z[5]*z[10];
    z[22]=z[3]*z[10];
    z[23]=n<T>(713,2) + 280*z[22];
    z[23]=n<T>(1,9)*z[23] - z[21];
    z[23]=z[5]*z[23];
    z[24]=z[6]*z[3];
    z[25]=n<T>(1,2)*z[4];
    z[26]=z[6]*z[25];
    z[26]=z[26] - n<T>(1195,18) - z[24];
    z[26]=z[4]*z[26];
    z[27]=z[4]*z[6];
    z[28]=295*z[27];
    z[29]=npow(z[10],2);
    z[30]=z[29]*z[3];
    z[31]=313*z[10] + 295*z[30];
    z[31]=z[5]*z[31];
    z[31]= - z[28] - static_cast<T>(295)+ n<T>(1,3)*z[31];
    z[32]=n<T>(1,6)*z[2];
    z[31]=z[31]*z[32];
    z[33]=2*z[13];
    z[34]=npow(z[13],2);
    z[35]=z[34]*z[10];
    z[36]=z[13] + z[35];
    z[36]=z[10]*z[36];
    z[36]= - static_cast<T>(1)+ z[36];
    z[36]=z[3]*z[36];
    z[23]=z[31] + z[26] + z[23] + z[36] + z[33] - n<T>(3,2)*z[35];
    z[23]=z[2]*z[23];
    z[26]=n<T>(5,3)*z[18];
    z[31]=z[26] - z[9];
    z[36]=n<T>(9,2)*z[11];
    z[37]=n<T>(11,2) + n<T>(7,3)*z[24];
    z[37]=z[4]*z[37];
    z[37]=z[37] + n<T>(475,18)*z[5] - n<T>(80,9)*z[3] + z[36] + z[31];
    z[37]=z[4]*z[37];
    z[38]=npow(z[14],2);
    z[39]=z[38]*z[13];
    z[40]=z[38]*z[15];
    z[41]=z[39] + z[40];
    z[42]=2*z[10];
    z[43]=z[41]*z[42];
    z[44]=4*z[9];
    z[45]=npow(z[9],2);
    z[46]=4*z[45];
    z[47]= - z[6]*z[46];
    z[47]=z[47] - z[13] + z[44];
    z[47]=z[14]*z[47];
    z[48]=npow(z[10],3);
    z[49]=z[48]*z[3];
    z[50]=npow(z[8],3);
    z[51]=z[49]*z[50];
    z[52]=n<T>(1,2)*z[13];
    z[53]=z[52]*z[51];
    z[51]=n<T>(299,9)*z[3] + 5*z[51];
    z[54]= - n<T>(7,2) - z[22];
    z[54]=z[5]*z[54];
    z[51]=n<T>(1,2)*z[51] + z[54];
    z[51]=z[5]*z[51];
    z[54]=3*z[14];
    z[55]=z[54]*z[15];
    z[56]=2*z[14];
    z[57]= - z[56] + z[13];
    z[57]=z[11]*z[57];
    z[58]=z[3]*z[13];
    z[23]=z[23] + z[37] + z[51] + z[53] - n<T>(97,18)*z[58] - z[43] + z[57]
    + z[55] + z[47];
    z[23]=z[2]*z[23];
    z[37]=3*z[15];
    z[47]=z[37] - z[19];
    z[51]=4*z[12];
    z[53]=npow(z[3],2);
    z[57]=z[6]*z[53];
    z[58]=z[15] - z[3];
    z[58]=z[6]*z[58];
    z[58]=n<T>(3,2) + z[58];
    z[58]=z[5]*z[58];
    z[57]=z[58] + z[57] - n<T>(133,18)*z[3] + n<T>(25,18)*z[13] - z[51] - z[47];
    z[57]=z[5]*z[57];
    z[58]=7*z[14];
    z[59]=3*z[16];
    z[60]= - 11*z[9] + z[59] - z[58];
    z[60]=z[9]*z[60];
    z[61]=n<T>(1,2)*z[9];
    z[62]= - z[19] + n<T>(17,6)*z[3];
    z[62]=z[6]*z[62];
    z[62]=n<T>(13,2) + z[62];
    z[62]=z[5]*z[62];
    z[62]=z[61] + z[62];
    z[62]=z[4]*z[62];
    z[63]=z[11]*z[9];
    z[64]=npow(z[16],2);
    z[65]=2*z[64];
    z[66]= - z[13]*z[14];
    z[67]= - z[3]*z[56];
    z[57]=z[62] + z[57] + z[67] + n<T>(13,2)*z[63] + z[66] + z[60] - z[65]
    + z[55];
    z[57]=z[4]*z[57];
    z[60]=6*z[34];
    z[62]=npow(z[12],2);
    z[66]=5*z[62];
    z[67]=3*z[13];
    z[68]= - z[5]*z[67];
    z[68]=z[68] - z[66] - z[60];
    z[68]=z[5]*z[68];
    z[69]=4*z[14];
    z[70]= - z[16] + z[69];
    z[70]=z[9]*z[70];
    z[70]=z[65] + z[70];
    z[70]=z[9]*z[70];
    z[71]=7*z[45] - z[63];
    z[71]=z[11]*z[71];
    z[72]=npow(z[6],2);
    z[73]=z[72]*z[50];
    z[74]= - z[65]*z[73];
    z[75]=z[13] - z[12];
    z[76]=z[75]*z[5];
    z[77]= - n<T>(1,2)*z[63] - 10*z[76];
    z[77]=z[4]*z[77];
    z[68]=z[77] + z[68] + z[74] + z[71] + z[70] + z[41];
    z[68]=z[4]*z[68];
    z[70]=z[17]*z[12];
    z[71]=z[13]*z[17];
    z[74]=z[70] + z[71];
    z[77]=z[11]*z[17];
    z[78]=n<T>(3,2)*z[77];
    z[79]=z[78] + z[74];
    z[79]=z[79]*z[50];
    z[60]=z[60] - z[66];
    z[80]= - z[5]*z[60];
    z[81]=z[4]*z[5];
    z[82]=z[75]*z[81];
    z[79]=z[82] + z[79] + z[80];
    z[79]=z[4]*z[79];
    z[80]=npow(z[5],2);
    z[82]=z[80]*z[34];
    z[83]=z[66]*z[17];
    z[84]=z[50]*z[83];
    z[85]=4*z[64];
    z[86]=z[50]*z[6];
    z[87]= - z[86]*z[85];
    z[79]=z[79] - z[82] + z[84] + z[87];
    z[79]=z[4]*z[79];
    z[84]=n<T>(1,2)*z[34];
    z[87]=npow(z[11],2);
    z[88]= - z[5]*z[52];
    z[89]=2*z[11];
    z[90]= - z[4]*z[89];
    z[88]=z[90] + z[88] + z[87] + z[84] - z[65];
    z[90]=z[50]*z[4];
    z[88]=z[88]*z[90];
    z[91]=z[4] - z[11];
    z[91]=z[91]*z[2];
    z[92]= - n<T>(1,2)*z[91] - z[45] - z[87];
    z[92]=z[50]*z[92];
    z[93]=n<T>(1,2)*z[5];
    z[94]= - z[25] + 6*z[11] - z[93];
    z[90]=z[94]*z[90];
    z[90]=z[90] + z[92];
    z[90]=z[2]*z[90];
    z[88]=z[88] + z[90];
    z[88]=z[7]*z[88];
    z[90]=z[25]*z[11];
    z[92]=z[90] + z[86] - 3*z[80];
    z[92]=z[4]*z[92];
    z[94]= - n<T>(11,2)*z[9] - z[89];
    z[94]=z[94]*z[86];
    z[95]=z[2]*z[4];
    z[96]=n<T>(313,18)*z[5];
    z[97]=z[11] + z[96];
    z[97]=z[97]*z[95];
    z[98]=z[11]*z[84];
    z[92]=z[97] + z[92] + z[98] + z[94];
    z[92]=z[2]*z[92];
    z[94]=npow(z[4],2);
    z[97]=n<T>(19,3)*z[94] - z[46] + z[87];
    z[97]=z[86]*z[97];
    z[92]=z[92] + z[97];
    z[92]=z[2]*z[92];
    z[97]=z[50]*z[71]*z[46];
    z[98]=15*z[62];
    z[99]=2*z[9];
    z[100]=z[12]*z[99];
    z[100]= - z[98] + z[100];
    z[100]=z[9]*z[100]*z[86];
    z[101]= - z[34] + 3*z[86];
    z[101]=z[80]*z[11]*z[101];
    z[79]=z[88] + z[92] + z[79] + n<T>(1,2)*z[101] + z[97] + z[100];
    z[79]=z[7]*z[79];
    z[88]=3*z[64];
    z[92]=z[88] - n<T>(2,3)*z[38];
    z[92]=z[92]*z[29]*z[50];
    z[97]=z[38] + z[34];
    z[97]=z[9]*z[97];
    z[100]=z[87]*z[73];
    z[101]=z[11]*z[73];
    z[102]= - z[13]*z[89];
    z[101]=z[102] + z[101];
    z[101]=z[5]*z[101];
    z[92]=z[101] - n<T>(2,3)*z[100] + 2*z[97] + z[92];
    z[97]=2*z[5];
    z[92]=z[92]*z[97];
    z[100]=n<T>(7,3)*z[18];
    z[101]=z[36] - z[100] - z[61];
    z[101]=z[101]*z[73];
    z[102]=n<T>(11,2)*z[11];
    z[103]=z[25] + z[102] - z[26];
    z[104]=n<T>(460,9)*z[5] + z[73] - z[103];
    z[104]=z[4]*z[104];
    z[96]= - n<T>(313,6)*z[4] - z[73] + z[96];
    z[96]=z[2]*z[96];
    z[105]= - z[11]*z[67];
    z[96]=z[96] + z[104] - 2*z[80] + z[105] + z[101];
    z[96]=z[2]*z[96];
    z[101]=8*z[45];
    z[104]=n<T>(7,3)*z[87];
    z[105]=z[101] - z[104];
    z[105]=z[105]*z[73];
    z[106]=n<T>(3,2)*z[11];
    z[107]=z[106] - 7*z[5];
    z[107]=z[4]*z[107];
    z[107]= - 6*z[80] + z[107];
    z[107]=z[4]*z[107];
    z[96]=z[96] + z[105] + z[107];
    z[96]=z[2]*z[96];
    z[105]=npow(z[17],2);
    z[101]= - z[13]*z[105]*z[101];
    z[107]=z[29]*z[40];
    z[101]=z[101] + z[107];
    z[101]=z[101]*z[50];
    z[107]= - z[9]*z[12];
    z[107]=z[62] + z[107];
    z[73]=z[9]*z[107]*z[73];
    z[107]=z[11]*z[40];
    z[68]=z[79] + z[96] + z[68] + z[92] + 5*z[73] + z[107] + z[101];
    z[68]=z[7]*z[68];
    z[73]=2*z[6];
    z[79]=npow(z[2],2);
    z[92]=npow(z[8],4);
    z[96]= - z[45]*z[92]*z[79]*z[73];
    z[90]=z[90]*z[92];
    z[101]= - z[87]*z[92];
    z[101]=z[101] + z[90];
    z[101]=z[4]*z[101];
    z[90]=z[2]*z[90];
    z[90]=z[101] + z[90];
    z[101]=z[7]*z[2];
    z[90]=z[90]*z[101];
    z[90]=z[96] + z[90];
    z[90]=z[7]*z[90];
    z[96]=z[92]*z[72];
    z[107]=3*z[9];
    z[108]= - z[12]*z[107];
    z[108]= - z[66] + z[108];
    z[108]=z[108]*z[45]*z[96];
    z[94]=n<T>(1,2)*z[94];
    z[109]=3*z[45];
    z[110]= - z[2]*z[99];
    z[110]=z[110] - z[109] + z[94];
    z[110]=z[2]*z[110];
    z[111]=npow(z[9],3);
    z[110]= - 3*z[111] + z[110];
    z[96]=z[2]*z[96]*z[110];
    z[90]=z[90] + z[108] + z[96];
    z[90]=z[7]*z[90];
    z[96]=z[80]*z[4];
    z[108]=z[2]*z[81];
    z[96]= - 3*z[96] + n<T>(307,6)*z[108];
    z[96]=z[96]*z[79];
    z[90]=z[96] + z[90];
    z[90]=z[7]*z[90];
    z[96]= - static_cast<T>(301)- n<T>(295,3)*z[24];
    z[96]=z[96]*z[25];
    z[108]=static_cast<T>(304)+ n<T>(295,2)*z[22];
    z[108]=z[5]*z[108];
    z[108]= - n<T>(295,2)*z[3] + z[108];
    z[96]=n<T>(1,3)*z[108] + z[96];
    z[96]=z[2]*z[96];
    z[108]=n<T>(265,18)*z[3] - z[5];
    z[108]=z[5]*z[108];
    z[110]= - n<T>(265,2)*z[3] + 194*z[5];
    z[110]=z[4]*z[110];
    z[96]=n<T>(1,3)*z[96] + z[108] + n<T>(1,9)*z[110];
    z[96]=z[2]*z[96];
    z[108]=n<T>(5,9)*z[3] - n<T>(7,2)*z[5];
    z[108]=z[5]*z[108];
    z[110]=n<T>(1,3)*z[3] - z[5];
    z[110]=z[4]*z[110];
    z[108]=z[108] + 7*z[110];
    z[108]=z[4]*z[108];
    z[110]=z[80]*z[3];
    z[96]=z[96] + z[108] - z[110] - z[41];
    z[96]=z[2]*z[96];
    z[108]= - n<T>(5,3)*z[3] - z[5];
    z[108]=z[108]*z[81];
    z[111]=n<T>(295,3)*z[3];
    z[112]=z[5]*z[111];
    z[111]= - z[111] + 301*z[5];
    z[111]=z[4]*z[111];
    z[111]=z[112] + z[111];
    z[111]=z[111]*z[32];
    z[108]=z[108] + z[111];
    z[108]=z[2]*z[108];
    z[81]=z[81]*z[3];
    z[110]=z[110] + n<T>(7,3)*z[81];
    z[111]= - z[4]*z[110];
    z[108]=z[111] + z[108];
    z[108]=z[2]*z[108];
    z[101]= - z[45]*npow(z[8],5)*z[72]*npow(z[101],3);
    z[81]=z[1]*npow(z[2],3)*z[81];
    z[81]=n<T>(295,18)*z[81] + z[108] + z[101];
    z[81]=z[1]*z[81];
    z[101]=z[89]*z[3];
    z[108]=z[101] + z[104];
    z[108]=z[108]*z[3];
    z[111]=z[3]*z[11];
    z[112]= - z[5]*z[111];
    z[112]= - z[108] + z[112];
    z[92]=z[5]*z[92]*z[112]*npow(z[6],4);
    z[112]=z[14]*z[46];
    z[110]=z[112] + z[110];
    z[110]=z[4]*z[110];
    z[81]=z[81] + z[90] + z[96] + z[92] + z[110];
    z[81]=z[1]*z[81];
    z[90]=z[53]*z[50];
    z[92]=8*z[10];
    z[96]= - z[90]*z[92];
    z[110]=7*z[3];
    z[112]=n<T>(19,2)*z[11] + z[110];
    z[112]=z[3]*z[112];
    z[112]= - n<T>(11,3)*z[87] + z[112];
    z[112]=z[112]*z[86];
    z[96]=z[96] + z[112];
    z[96]=z[6]*z[96];
    z[112]=z[53]*z[89];
    z[113]=z[29]*z[90];
    z[96]=z[96] + z[112] + 3*z[113];
    z[96]=z[6]*z[96];
    z[112]=z[50]*z[22];
    z[113]=3*z[3];
    z[114]= - n<T>(1,2)*z[11] + z[113];
    z[114]=z[114]*z[86];
    z[112]= - 3*z[112] + z[114];
    z[112]=z[6]*z[112];
    z[114]=z[50]*z[30];
    z[115]=4*z[111];
    z[116]=z[11]*z[15];
    z[112]=z[112] + z[114] + z[116] - z[115];
    z[112]=z[6]*z[112];
    z[112]=z[112] - n<T>(13,2)*z[11] + z[3];
    z[112]=z[5]*z[112];
    z[114]=z[48]*z[65];
    z[117]=z[16]*z[49];
    z[114]=z[114] + z[117];
    z[114]=z[114]*z[50];
    z[117]= - z[37] - z[13];
    z[117]=z[11]*z[117];
    z[96]=z[112] + z[96] + z[114] + z[117] + n<T>(1,2)*z[111];
    z[96]=z[5]*z[96];
    z[112]= - z[48]*z[84];
    z[114]=z[30]*z[71];
    z[112]=z[112] + z[114];
    z[112]=z[3]*z[112];
    z[114]=npow(z[17],3);
    z[117]=z[13]*z[114]*z[46];
    z[112]=z[117] + z[112];
    z[50]=z[112]*z[50];
    z[90]=z[77]*z[90];
    z[86]= - z[86]*z[108];
    z[86]=2*z[90] + z[86];
    z[86]=z[6]*z[86];
    z[90]=5*z[11];
    z[108]= - z[44] - z[90];
    z[108]=z[3]*z[38]*z[108];
    z[86]=z[108] + z[86];
    z[86]=z[6]*z[86];
    z[108]=z[9]*z[14];
    z[108]=z[38] + z[108];
    z[108]=z[108]*z[44];
    z[112]=z[15] - z[56];
    z[112]=z[112]*z[54];
    z[117]= - z[14] + z[9];
    z[117]=z[13]*z[117];
    z[112]=z[112] + 5*z[117];
    z[112]=z[11]*z[112];
    z[117]=7*z[11];
    z[118]=z[44] - z[117];
    z[118]=z[3]*z[14]*z[118];
    z[23]=z[81] + z[68] + z[23] + z[57] + z[96] + z[86] + z[50] + z[118]
    + z[112] + z[108] + z[41];
    z[23]=z[1]*z[23];
    z[50]= - static_cast<T>(4)- 3*z[24];
    z[50]=z[50]*z[73];
    z[57]=z[72]*z[25];
    z[50]=z[50] + z[57];
    z[50]=z[4]*z[50];
    z[57]=npow(z[8],2);
    z[68]=z[29]*z[57];
    z[81]=9*z[13];
    z[86]=z[81] + 13*z[35];
    z[86]=z[10]*z[86];
    z[96]=n<T>(7,2)*z[13];
    z[108]= - z[96] - 3*z[35];
    z[108]=z[10]*z[108];
    z[108]= - n<T>(13,3) + z[108];
    z[108]=z[10]*z[108];
    z[112]=2*z[30];
    z[108]=z[108] - z[112];
    z[108]=z[3]*z[108];
    z[118]=z[57]*z[10];
    z[119]=z[57]*z[6];
    z[120]= - z[119] - 2*z[3] + z[118];
    z[120]=z[6]*z[120];
    z[121]= - z[29] - z[49];
    z[121]=z[5]*z[121];
    z[121]=z[121] + n<T>(134,9)*z[10] - z[30];
    z[121]=z[5]*z[121];
    z[122]= - n<T>(295,9)*z[10] - z[30];
    z[122]=z[2]*z[122];
    z[50]=z[122] + z[50] + z[121] + 3*z[120] - 2*z[68] + z[108] - n<T>(728,9)
    + n<T>(1,2)*z[86];
    z[50]=z[2]*z[50];
    z[86]=n<T>(23,2)*z[11];
    z[108]=z[113] - z[86] + z[100] + n<T>(5,2)*z[9];
    z[108]=z[108]*z[119];
    z[113]=z[54] + z[44];
    z[113]=z[9]*z[113];
    z[120]=z[10]*z[14];
    z[121]= - z[120] - 4*z[22];
    z[121]=z[121]*z[57];
    z[122]=z[14]*z[89];
    z[108]=z[108] + z[121] + z[113] + z[122];
    z[108]=z[6]*z[108];
    z[113]= - n<T>(7,2)*z[10] - 8*z[30];
    z[113]=z[5]*z[113];
    z[113]=z[113] - n<T>(11,2)*z[68] + n<T>(32,9)*z[22] + n<T>(410,9) - z[120];
    z[113]=z[5]*z[113];
    z[120]=z[41]*z[10];
    z[121]=n<T>(13,2)*z[13];
    z[122]= - 6*z[14] + z[121];
    z[122]=z[13]*z[122];
    z[55]= - z[120] + z[55] + z[122];
    z[55]=z[10]*z[55];
    z[122]=n<T>(5,2)*z[13];
    z[123]=2*z[15];
    z[124]=z[122] + z[123];
    z[125]=2*z[29];
    z[53]= - z[53]*z[125];
    z[126]= - n<T>(229,9)*z[13] - n<T>(21,2)*z[35];
    z[126]=z[10]*z[126];
    z[53]=z[53] - n<T>(55,9) + z[126];
    z[53]=z[3]*z[53];
    z[126]=n<T>(3,2)*z[13];
    z[127]=z[14] - z[126];
    z[127]=z[127]*z[29];
    z[127]=z[127] + 5*z[30];
    z[127]=z[127]*z[57];
    z[128]= - static_cast<T>(28)- n<T>(37,3)*z[24];
    z[128]=n<T>(1,3)*z[128] + 2*z[27];
    z[129]=5*z[4];
    z[128]=z[128]*z[129];
    z[129]=n<T>(19,3)*z[18];
    z[50]=z[50] + z[128] + z[113] + z[108] + z[127] + z[53] + z[55] + 
   n<T>(115,6)*z[11] - 29*z[9] - z[54] - z[129] - z[124];
    z[50]=z[2]*z[50];
    z[53]= - 6*z[9] + z[11];
    z[53]=z[53]*z[89];
    z[55]=4*z[74] + n<T>(1,2)*z[77];
    z[55]=z[55]*z[57];
    z[74]= - z[16] - z[56];
    z[74]=z[74]*z[119];
    z[108]=n<T>(5,2)*z[11];
    z[113]=n<T>(25,6)*z[5] - z[108] + n<T>(3,2)*z[9] + 10*z[75];
    z[113]=z[4]*z[113];
    z[127]= - z[14] + z[13];
    z[127]=n<T>(247,9)*z[5] + 6*z[127];
    z[127]=z[13]*z[127];
    z[128]=5*z[14];
    z[130]=z[15]*z[128];
    z[58]=4*z[16] - z[58];
    z[58]=z[9]*z[58];
    z[53]=z[113] + z[74] + z[55] + z[53] + z[58] + z[130] + z[66] - 
    z[65] + z[127];
    z[53]=z[4]*z[53];
    z[55]=z[42]*z[57];
    z[28]= - n<T>(331,3) - z[28];
    z[28]=z[28]*z[32];
    z[32]=n<T>(161,9) - z[21];
    z[32]=z[5]*z[32];
    z[58]= - n<T>(811,9) + z[27];
    z[58]=z[4]*z[58];
    z[28]=z[28] + z[58] + z[32] - n<T>(5,2)*z[119] + z[55] - 4*z[35] - 
    z[108] + z[126] + z[18] - z[9];
    z[28]=z[2]*z[28];
    z[32]=n<T>(7,3)*z[11];
    z[58]= - z[67] - z[32];
    z[58]=z[11]*z[58];
    z[74]=z[67]*z[118];
    z[113]=n<T>(31,3)*z[11];
    z[127]=10*z[20] - 7*z[18];
    z[127]= - z[113] + n<T>(1,3)*z[127] - z[9];
    z[127]=z[127]*z[119];
    z[130]=11*z[118] - 9*z[5];
    z[130]=z[130]*z[93];
    z[31]=n<T>(5,2)*z[4] + n<T>(1091,18)*z[5] + n<T>(5,3)*z[119] + 18*z[11] + z[31];
    z[31]=z[4]*z[31];
    z[28]=z[28] + z[31] + z[130] + z[127] + z[74] + z[58] - z[109] + 4*
    z[34];
    z[28]=z[2]*z[28];
    z[31]= - z[9] - z[13];
    z[31]= - n<T>(28,3)*z[119] + 3*z[31] - n<T>(4,3)*z[11];
    z[31]=z[11]*z[31];
    z[58]=z[64] - 2*z[38];
    z[74]=z[14] + z[16];
    z[127]= - z[74]*z[107];
    z[130]=n<T>(11,2)*z[13];
    z[131]= - z[107] - z[130];
    z[131]=z[13]*z[131];
    z[122]=z[122] + 2*z[16] - z[14];
    z[122]=z[122]*z[118];
    z[55]= - 4*z[119] + z[55] - 11*z[13] + 10*z[11];
    z[55]=z[5]*z[55];
    z[31]=z[55] + z[122] + z[131] + 2*z[58] + z[127] + z[31];
    z[31]=z[5]*z[31];
    z[55]=z[44]*z[12];
    z[58]=z[55] - z[66];
    z[58]=z[58]*z[9];
    z[122]= - z[34]*z[44];
    z[127]= - z[57]*z[52];
    z[131]=z[90]*z[13];
    z[132]= - 3*z[34] + z[131];
    z[132]=n<T>(1,2)*z[132] - z[57];
    z[132]=z[5]*z[132];
    z[122]=z[132] + z[127] - z[58] + z[122];
    z[122]=z[5]*z[122];
    z[75]= - z[4]*z[75];
    z[60]=z[75] + 3*z[76] + n<T>(28,3)*z[57] - n<T>(3,2)*z[63] + z[60];
    z[60]=z[4]*z[60];
    z[63]= - z[5]*z[130];
    z[63]=z[63] - z[66] - n<T>(11,2)*z[34];
    z[63]=z[5]*z[63];
    z[75]=5*z[12];
    z[76]= - n<T>(7,2)*z[11] - z[52] + z[75] - z[14];
    z[76]=z[76]*z[57];
    z[60]=z[60] + z[63] + 2*z[41] + z[76];
    z[60]=z[4]*z[60];
    z[63]=z[34] - z[131];
    z[76]=9*z[9];
    z[127]=z[76] + z[5];
    z[130]= - z[5]*z[127];
    z[86]= - z[4] + n<T>(260,9)*z[5] + n<T>(10,3)*z[18] - z[86];
    z[86]=z[4]*z[86];
    z[131]=z[89] - n<T>(349,18)*z[4];
    z[131]=z[2]*z[131];
    z[63]=z[131] + z[86] + z[130] + n<T>(1,2)*z[63] + z[57];
    z[63]=z[2]*z[63];
    z[86]=z[36] - n<T>(7,3)*z[5];
    z[86]=z[4]*z[86];
    z[86]=z[86] - n<T>(11,2)*z[80] - z[87] - 4*z[57];
    z[86]=z[4]*z[86];
    z[36]= - z[99] + z[36];
    z[36]=z[36]*z[57];
    z[46]= - z[46] - n<T>(1,2)*z[57];
    z[46]=z[5]*z[46];
    z[36]=z[63] + z[86] + z[36] + z[46];
    z[36]=z[2]*z[36];
    z[46]=z[98] + z[65];
    z[63]=3*z[12];
    z[86]=z[63] + z[9];
    z[86]=z[86]*z[107];
    z[130]=z[44] + z[52];
    z[130]=z[13]*z[130];
    z[86]=z[87] + z[130] + z[86] - z[46];
    z[86]=z[86]*z[57];
    z[130]=z[4]*z[11];
    z[131]= - z[80] + z[130];
    z[131]=z[4]*z[131];
    z[132]=z[89]*z[95];
    z[131]=z[131] + z[132];
    z[131]=z[131]*z[79];
    z[82]= - z[4]*z[82];
    z[82]=z[82] + z[131];
    z[82]=z[7]*z[82];
    z[36]=z[82] + z[36] + z[60] + z[86] + z[122];
    z[36]=z[7]*z[36];
    z[60]=n<T>(10,3)*z[20];
    z[82]= - z[77]*z[60];
    z[86]=z[14]*z[15];
    z[122]=z[86] + z[65];
    z[131]=z[84] - z[122];
    z[131]=z[10]*z[131];
    z[132]= - z[17]*z[109];
    z[82]=z[131] + z[82] + z[83] + z[132];
    z[82]=z[82]*z[57];
    z[51]= - z[51] + z[107];
    z[51]=z[51]*z[107];
    z[51]=z[51] + z[66] - z[64];
    z[51]=2*z[51] - z[87];
    z[51]=z[51]*z[119];
    z[131]= - z[13]*z[99];
    z[131]=z[38] + z[131];
    z[131]=z[131]*z[33];
    z[132]= - z[11]*z[86];
    z[28]=z[36] + z[28] + z[53] + z[31] + z[51] + z[82] + z[132] + 5*
    z[40] + z[131];
    z[28]=z[7]*z[28];
    z[31]= - z[74]*z[119];
    z[36]= - z[56] + z[9];
    z[36]=z[3]*z[36];
    z[31]=z[31] + z[85] + z[36];
    z[31]=z[6]*z[31];
    z[36]=4*z[15];
    z[51]= - n<T>(37,18)*z[3] - z[19] - z[15];
    z[51]=z[6]*z[51];
    z[51]= - n<T>(143,6) + z[51];
    z[51]=z[5]*z[51];
    z[53]=z[5]*z[6];
    z[53]=static_cast<T>(4)+ n<T>(11,6)*z[53];
    z[53]=z[4]*z[53];
    z[31]=z[53] + z[51] + z[31] - 16*z[11] - z[67] + n<T>(39,2)*z[9] - 
    z[128] - z[59] - z[12] - 3*z[19] + z[36];
    z[31]=z[4]*z[31];
    z[51]= - z[104] - 6*z[111];
    z[51]=z[3]*z[51];
    z[53]=n<T>(1,3)*z[11];
    z[82]= - z[53] + n<T>(13,2)*z[3];
    z[82]=z[82]*z[57];
    z[51]=z[51] + z[82];
    z[51]=z[6]*z[51];
    z[82]=z[57]*z[22];
    z[131]=n<T>(11,3)*z[11];
    z[132]=5*z[15] - z[131];
    z[132]=z[11]*z[132];
    z[133]=n<T>(17,2)*z[11] + 9*z[3];
    z[133]=z[3]*z[133];
    z[51]=z[51] - n<T>(193,6)*z[82] + z[132] + z[133];
    z[51]=z[6]*z[51];
    z[82]=7*z[111];
    z[132]= - z[15]*z[89];
    z[132]=z[132] + z[82];
    z[132]=z[6]*z[132];
    z[118]=z[132] + n<T>(3,2)*z[118] - 16*z[3] + z[36] + n<T>(25,2)*z[11];
    z[118]=z[6]*z[118];
    z[68]=z[118] - z[68] - static_cast<T>(18)+ z[22];
    z[68]=z[5]*z[68];
    z[118]=9*z[16] - n<T>(49,6)*z[14];
    z[118]=z[118]*z[29];
    z[118]=z[118] + 22*z[30];
    z[118]=z[118]*z[57];
    z[132]=z[10]*z[65];
    z[133]=z[10]*z[16];
    z[134]= - n<T>(169,18) + z[133];
    z[134]=z[3]*z[134];
    z[51]=z[68] + z[51] + z[118] + z[134] + z[132] + 11*z[11] + n<T>(355,9)*
    z[13] - 8*z[15] - z[128];
    z[51]=z[5]*z[51];
    z[68]=7*z[12];
    z[118]= - n<T>(14,3)*z[14] + z[68] + z[59];
    z[132]=4*z[13];
    z[134]=z[105]*z[9];
    z[135]= - z[17] + z[134];
    z[135]=z[135]*z[132];
    z[136]=z[9]*z[17];
    z[135]=z[135] + static_cast<T>(1)+ z[136];
    z[135]=z[13]*z[135];
    z[137]= - z[88] - z[34];
    z[137]=z[137]*z[42];
    z[138]=z[10]*z[13];
    z[139]=z[16]*z[112];
    z[139]=z[139] + static_cast<T>(1)- z[138];
    z[139]=z[3]*z[139];
    z[118]=z[139] + z[137] - z[106] + z[135] + 2*z[118] - n<T>(11,3)*z[9];
    z[118]=z[3]*z[118];
    z[135]=3*z[17];
    z[92]= - z[135] - z[92];
    z[92]=z[3]*z[92];
    z[133]=5*z[133];
    z[92]=z[92] - z[133] - z[136] - n<T>(11,6)*z[77];
    z[92]=z[3]*z[92];
    z[137]=10*z[17];
    z[139]= - z[45]*z[137];
    z[92]=z[139] + z[92];
    z[92]=z[92]*z[57];
    z[139]=7*z[9];
    z[140]=13*z[11] + z[139];
    z[140]=z[14]*z[140];
    z[140]= - z[101] + n<T>(1,3)*z[38] + z[140];
    z[140]=z[3]*z[140];
    z[141]=z[63] - z[139];
    z[141]=z[9]*z[141];
    z[142]=n<T>(13,6)*z[11] + z[3];
    z[110]=z[142]*z[110];
    z[110]=z[110] + z[141] + z[87];
    z[110]=z[110]*z[119];
    z[92]=z[110] + z[140] + z[92];
    z[92]=z[6]*z[92];
    z[110]= - z[56] + z[52];
    z[110]=z[13]*z[110];
    z[119]=6*z[64];
    z[140]= - z[15] - z[69];
    z[140]=z[14]*z[140];
    z[110]=z[110] + z[119] + z[140];
    z[110]=z[110]*z[29];
    z[140]=2*z[71];
    z[141]=z[59] - z[14];
    z[141]=z[10]*z[141];
    z[141]=z[140] + z[141];
    z[141]=z[10]*z[141];
    z[141]=3*z[30] - z[134] + z[141];
    z[141]=z[3]*z[141];
    z[142]=z[44]*z[13];
    z[143]=z[45] - z[142];
    z[143]=z[105]*z[143];
    z[110]=z[141] + z[110] + z[143];
    z[57]=z[110]*z[57];
    z[110]=n<T>(2,3)*z[18];
    z[141]= - z[110] + 25*z[9];
    z[143]=6*z[16];
    z[75]= - 10*z[14] + z[143] - z[75] - z[141];
    z[75]=z[9]*z[75];
    z[144]=z[15] - n<T>(7,3)*z[14];
    z[69]=z[144]*z[69];
    z[69]= - z[64] + z[69];
    z[144]= - z[44]*z[71];
    z[145]=z[44]*z[17];
    z[146]=static_cast<T>(9)+ z[145];
    z[146]=z[9]*z[146];
    z[144]=z[144] + z[146] - z[19] - 18*z[14];
    z[144]=z[13]*z[144];
    z[146]= - z[123] + z[128];
    z[146]=z[89] + z[13] + 2*z[146] - n<T>(29,2)*z[9];
    z[146]=z[11]*z[146];
    z[23]=z[23] + z[28] + z[50] + z[31] + z[51] + z[92] + z[57] + z[118]
    + z[43] + z[146] + z[144] + 2*z[69] + z[75];
    z[23]=z[1]*z[23];
    z[28]= - z[88] + 4*z[38];
    z[28]=2*z[28] + z[84];
    z[28]=z[10]*z[28];
    z[31]=z[6]*z[11];
    z[31]= - 8*z[31] + n<T>(19,2) - 4*z[138];
    z[31]=z[5]*z[31];
    z[43]=n<T>(20,3)*z[20];
    z[50]=z[6]*z[87];
    z[28]=z[31] + n<T>(8,3)*z[50] + z[28] + n<T>(37,3)*z[11] + n<T>(214,9)*z[13] + 
    z[99] - z[128] - z[43] + z[59];
    z[28]=z[5]*z[28];
    z[31]=3*z[70];
    z[50]=z[78] - z[71] + n<T>(77,6) + z[31];
    z[50]=z[4]*z[50];
    z[51]= - z[62]*z[137];
    z[57]= - static_cast<T>(1)+ 9*z[71];
    z[57]=z[57]*z[33];
    z[62]=z[54] - z[15];
    z[69]=z[65]*z[6];
    z[75]= - static_cast<T>(14)- z[77];
    z[75]=z[11]*z[75];
    z[50]=z[50] + 13*z[5] + z[69] + z[75] + z[57] + z[44] + z[51] + 
    z[16] + 11*z[12] - z[62];
    z[50]=z[4]*z[50];
    z[51]= - n<T>(701,9) + z[27];
    z[51]=z[51]*z[25];
    z[57]= - z[99] - z[11];
    z[57]=z[6]*z[57];
    z[57]=z[57] - n<T>(295,18)*z[27];
    z[57]=z[2]*z[57];
    z[75]= - z[6]*z[109];
    z[26]=z[57] + z[51] + z[75] - z[108] - z[126] + z[26] + z[127];
    z[26]=z[2]*z[26];
    z[51]= - z[76] - z[97];
    z[51]=z[5]*z[51];
    z[57]= - n<T>(7,3)*z[4] + 43*z[11] + n<T>(635,9)*z[5];
    z[25]=z[57]*z[25];
    z[57]=2*z[87];
    z[25]=z[26] + z[25] + z[51] + z[45] - z[57];
    z[25]=z[2]*z[25];
    z[26]=static_cast<T>(1)+ z[140];
    z[26]=z[26]*z[67];
    z[51]=z[70] - z[71];
    z[51]=z[4]*z[51];
    z[26]=z[51] - z[90] + z[26] - z[63] - z[83];
    z[26]=z[4]*z[26];
    z[51]=z[66] + z[122];
    z[75]= - z[128] + z[121];
    z[75]=z[13]*z[75];
    z[68]=z[68] + n<T>(617,18)*z[13];
    z[68]=z[5]*z[68];
    z[26]=z[26] + z[68] + z[57] + 2*z[51] + z[75];
    z[26]=z[4]*z[26];
    z[51]= - z[5]*z[33];
    z[51]=z[84] + z[51];
    z[51]=z[5]*z[51];
    z[41]=z[51] + z[41];
    z[41]=z[4]*z[41];
    z[51]=z[5] - z[103];
    z[51]=z[4]*z[51];
    z[51]=z[51] - z[91];
    z[51]=z[2]*z[51];
    z[68]= - z[87] - z[80];
    z[68]=2*z[68] + n<T>(7,2)*z[130];
    z[68]=z[4]*z[68];
    z[51]=z[68] + z[51];
    z[51]=z[2]*z[51];
    z[68]=z[94] + z[95];
    z[68]=z[7]*z[79]*z[11]*z[68];
    z[41]=z[68] + z[41] + z[51];
    z[41]=z[7]*z[41];
    z[51]= - z[13] - z[117];
    z[51]=z[51]*z[93];
    z[68]= - z[12]*z[139];
    z[52]= - z[9] + z[52];
    z[52]=z[13]*z[52];
    z[51]=z[51] + z[68] + z[52];
    z[51]=z[5]*z[51];
    z[52]=z[38] + z[142];
    z[52]=z[13]*z[52];
    z[25]=z[41] + z[25] + z[26] + z[51] + z[52] + z[40] + z[58];
    z[25]=z[7]*z[25];
    z[26]= - z[106] + z[18] - n<T>(15,2)*z[9];
    z[26]= - n<T>(331,18)*z[2] + 3*z[26];
    z[26]=z[6]*z[26];
    z[41]=z[72]*z[4];
    z[51]= - 6*z[6] + z[41];
    z[51]=z[4]*z[51];
    z[26]=z[51] + z[21] - n<T>(635,18) + 7*z[138] + z[26];
    z[26]=z[2]*z[26];
    z[45]= - 13*z[45] + z[104];
    z[45]=z[45]*z[73];
    z[21]=n<T>(116,9) - z[21];
    z[21]=z[21]*z[97];
    z[51]= - 5*z[20] + z[18];
    z[52]= - n<T>(403,9) + 14*z[27];
    z[52]=z[4]*z[52];
    z[21]=z[26] + z[52] + z[21] + z[45] + n<T>(79,3)*z[11] - z[81] - 34*z[9]
    + n<T>(2,3)*z[51] + z[15];
    z[21]=z[2]*z[21];
    z[26]=static_cast<T>(1)+ z[145];
    z[26]=z[26]*z[99];
    z[45]=n<T>(7,2) - z[145];
    z[45]=z[45]*z[67];
    z[26]=z[45] - z[128] + z[26];
    z[26]=z[13]*z[26];
    z[45]= - z[98] - z[55];
    z[45]=z[6]*z[45];
    z[51]=14*z[9];
    z[45]=z[45] - 13*z[12] - z[51];
    z[45]=z[9]*z[45];
    z[39]=4*z[40] + z[39];
    z[39]=z[10]*z[39];
    z[21]=z[25] + z[21] + z[50] + z[28] + z[39] + z[57] + z[26] + z[85]
    + z[45] - z[86] - z[66];
    z[21]=z[7]*z[21];
    z[25]= - z[9]*z[114];
    z[25]=z[105] + z[25];
    z[25]=z[25]*z[132];
    z[25]=z[25] + z[135] - 5*z[134];
    z[25]=z[13]*z[25];
    z[26]= - static_cast<T>(9)+ z[71];
    z[26]=z[26]*z[33];
    z[28]=z[119] + n<T>(19,2)*z[34];
    z[28]=z[10]*z[28];
    z[26]=z[28] + z[26] + z[59] + n<T>(23,6)*z[14];
    z[26]=z[10]*z[26];
    z[28]= - z[143] - z[13];
    z[28]=z[10]*z[28];
    z[28]=z[28] + static_cast<T>(5)+ z[71];
    z[28]=z[10]*z[28];
    z[28]=z[17] + z[28];
    z[28]=z[3]*z[28];
    z[25]=z[28] + z[26] + n<T>(13,2)*z[77] + z[25] - n<T>(10,3)*z[136] + n<T>(55,3)
    - 8*z[70];
    z[25]=z[3]*z[25];
    z[26]= - z[2]*z[42];
    z[26]=z[26] + n<T>(253,9) - 7*z[22];
    z[26]=z[6]*z[26];
    z[28]= - z[13]*z[42];
    z[28]=static_cast<T>(5)+ z[28];
    z[28]=z[28]*z[30];
    z[33]=n<T>(1,2)*z[29] + 4*z[49];
    z[33]=z[5]*z[33];
    z[34]= - n<T>(349,9) - n<T>(7,2)*z[138];
    z[34]=z[10]*z[34];
    z[26]= - 5*z[41] + z[33] + z[34] + z[28] + z[26];
    z[26]=z[2]*z[26];
    z[28]= - n<T>(3,2)*z[29] + z[49];
    z[28]=z[5]*z[28];
    z[33]= - z[10]*z[54];
    z[33]=n<T>(358,9) + z[33];
    z[33]=z[10]*z[33];
    z[28]=z[28] + z[33] + n<T>(421,18)*z[30];
    z[28]=z[5]*z[28];
    z[33]= - z[128] - z[96];
    z[33]=z[33]*z[138];
    z[33]=z[33] + n<T>(15,2)*z[13] + z[62];
    z[33]=z[10]*z[33];
    z[34]= - n<T>(358,9)*z[13] + n<T>(7,2)*z[35];
    z[34]=z[10]*z[34];
    z[34]=n<T>(953,18) + z[34];
    z[34]=z[10]*z[34];
    z[34]=z[34] + 6*z[30];
    z[34]=z[3]*z[34];
    z[35]= - n<T>(517,18) - 5*z[22];
    z[35]=z[3]*z[35];
    z[35]=z[35] - n<T>(31,6)*z[11] + n<T>(17,3)*z[18] + n<T>(13,2)*z[9];
    z[35]=z[6]*z[35];
    z[26]=z[26] - n<T>(131,9)*z[27] + z[28] + z[35] + z[34] - n<T>(209,6) + 
    z[33];
    z[26]=z[2]*z[26];
    z[28]=7*z[77];
    z[33]=z[133] - z[28] - static_cast<T>(8)+ 5*z[70];
    z[33]=z[3]*z[33];
    z[34]= - z[38]*z[42];
    z[35]= - static_cast<T>(12)+ z[77];
    z[35]=z[11]*z[35];
    z[33]=z[33] + z[34] + z[35] - n<T>(7,3)*z[9] + z[54] - 12*z[12] + z[16];
    z[33]=z[3]*z[33];
    z[34]=z[104] + z[115];
    z[34]=z[34]*z[24];
    z[35]=9*z[12] + z[141];
    z[35]=z[9]*z[35];
    z[39]=z[15] - z[89];
    z[39]=z[11]*z[39];
    z[33]=z[34] + z[33] + z[39] + z[35] - z[46];
    z[33]=z[6]*z[33];
    z[34]= - z[15] + z[131];
    z[34]=z[34]*z[89];
    z[35]=z[104] + 3*z[111];
    z[24]=z[35]*z[24];
    z[35]= - n<T>(97,6)*z[11] - 15*z[3];
    z[35]=z[3]*z[35];
    z[24]=2*z[24] + z[34] + z[35];
    z[24]=z[6]*z[24];
    z[34]=n<T>(1,6) + 9*z[22];
    z[34]=z[3]*z[34];
    z[24]=z[24] + z[34] + z[36] - z[113];
    z[24]=z[6]*z[24];
    z[34]=7*z[16];
    z[35]= - z[10]*z[85];
    z[35]=z[35] + n<T>(376,9)*z[13] - n<T>(67,3)*z[14] - z[123] - z[34];
    z[35]=z[10]*z[35];
    z[36]=z[116] - z[101];
    z[36]=z[6]*z[36];
    z[36]=z[36] + 11*z[3] - z[37] - z[102];
    z[36]=z[6]*z[36];
    z[37]=z[10]*z[15];
    z[37]= - 8*z[22] + static_cast<T>(8)+ z[37];
    z[36]=2*z[37] + z[36];
    z[36]=z[6]*z[36];
    z[37]= - 3*z[10] + z[112];
    z[36]=4*z[37] + z[36];
    z[36]=z[5]*z[36];
    z[37]= - z[16]*z[42];
    z[37]=n<T>(139,6) + z[37];
    z[37]=z[37]*z[22];
    z[24]=z[36] + z[24] + z[37] - n<T>(95,6) + z[35];
    z[24]=z[5]*z[24];
    z[35]= - z[56] + z[126];
    z[35]=z[13]*z[35];
    z[36]=z[15] - n<T>(4,3)*z[14];
    z[36]=z[14]*z[36];
    z[35]=z[120] + 7*z[35] - z[65] + z[36];
    z[35]=z[10]*z[35];
    z[36]=z[16] - z[14];
    z[36]=8*z[5] - z[69] + 2*z[36] + z[9];
    z[36]=z[6]*z[36];
    z[31]=8*z[77] + 4*z[71] + n<T>(50,3) + z[31] + z[36];
    z[31]=z[4]*z[31];
    z[36]= - z[105]*z[44];
    z[36]=5*z[17] + z[36];
    z[36]=z[36]*z[99];
    z[37]=z[105]*z[107];
    z[37]=z[17] + z[37];
    z[37]=z[37]*z[132];
    z[36]=z[37] - n<T>(17,2) + z[36];
    z[36]=z[13]*z[36];
    z[37]= - z[17]*z[110];
    z[37]=17*z[136] + static_cast<T>(7)+ z[37];
    z[37]=z[9]*z[37];
    z[39]= - z[17]*z[98];
    z[40]= - n<T>(25,3) - z[77];
    z[40]=z[11]*z[40];
    z[21]=z[23] + z[21] + z[26] + z[31] + z[24] + z[33] + z[25] + z[35]
    + z[40] + z[36] + z[37] + n<T>(38,3)*z[14] + z[39] + z[34] + 15*z[12]
    + z[129] + z[43] - z[47];
    z[21]=z[1]*z[21];
    z[23]=z[20]*z[137];
    z[23]= - static_cast<T>(7)+ z[23];
    z[23]=z[23]*z[53];
    z[24]= - z[11]*z[60];
    z[25]=z[11] - z[74];
    z[25]=z[4]*z[25];
    z[24]=z[24] + z[25];
    z[24]=z[7]*z[24];
    z[23]=z[24] + z[23] + z[43] - z[74];
    z[23]=z[8]*z[23];
    z[24]=z[6]*z[8];
    z[25]=z[74]*z[24];
    z[26]= - n<T>(13,3) - z[77];
    z[26]=z[8]*z[26];
    z[25]=z[26] - 2*z[25];
    z[25]=z[4]*z[25];
    z[26]=z[20]*z[24];
    z[27]= - z[8]*z[27];
    z[26]=z[26] + z[27];
    z[26]=z[2]*z[26];
    z[23]=n<T>(10,3)*z[26] + z[25] + z[23];
    z[23]=z[7]*z[23];
    z[25]= - z[10]*z[74];
    z[25]=n<T>(7,3)*z[77] + z[25];
    z[25]=z[8]*z[25];
    z[26]=z[32] - z[74];
    z[26]=z[26]*z[24];
    z[27]= - z[64] + z[38];
    z[27]=z[10]*z[27];
    z[27]=z[27] - z[74];
    z[27]=z[8]*z[27]*z[125];
    z[27]=n<T>(15,2) + z[27];
    z[27]=z[5]*z[27];
    z[31]=z[74]*z[8];
    z[32]= - z[72]*z[31];
    z[32]=n<T>(91,6) + z[32];
    z[32]=z[4]*z[32];
    z[23]=z[23] + n<T>(13,2)*z[2] + z[32] + z[27] + z[26] + z[25] - n<T>(23,6)*
    z[11] + z[67] + z[51] - 18*z[12] - z[14];
    z[23]=z[7]*z[23];
    z[24]=z[82]*z[24];
    z[25]=static_cast<T>(17)+ z[28];
    z[25]=z[3]*z[25];
    z[25]= - z[117] + z[25];
    z[25]=z[8]*z[25];
    z[25]=z[25] - z[24];
    z[26]=n<T>(1,3)*z[6];
    z[25]=z[25]*z[26];
    z[27]=static_cast<T>(1)- z[77];
    z[28]= - 14*z[17] - 13*z[10];
    z[28]=z[3]*z[28];
    z[27]=7*z[27] + z[28];
    z[27]=z[8]*z[27];
    z[25]=z[25] + n<T>(1,3)*z[27] - n<T>(47,6)*z[3] + n<T>(19,6)*z[11] + z[61] + 6*
    z[12] + n<T>(14,3)*z[18] - z[15] - z[74];
    z[25]=z[6]*z[25];
    z[27]= - z[99] + z[100] + z[63];
    z[27]=z[17]*z[27];
    z[28]=z[74]*z[29];
    z[29]=n<T>(7,3)*z[17];
    z[32]=z[29] + z[10];
    z[32]=z[32]*z[22];
    z[28]=z[32] + z[29] - 5*z[28];
    z[28]=z[8]*z[28];
    z[29]=z[48]*z[31];
    z[31]= - z[117] + 17*z[3];
    z[31]=z[8]*z[31];
    z[24]=z[31] - z[24];
    z[24]=z[6]*z[24];
    z[22]=static_cast<T>(10)- 13*z[22];
    z[22]=z[8]*z[22];
    z[22]=z[22] + z[24];
    z[22]=z[22]*z[26];
    z[24]= - z[10] + z[30];
    z[24]=z[8]*z[24];
    z[22]=z[22] + n<T>(235,6) + z[24];
    z[22]=z[6]*z[22];
    z[22]=z[22] - n<T>(31,2)*z[10] - 3*z[29];
    z[22]=z[5]*z[22];
    z[24]=n<T>(2,3)*z[14] + z[16] + z[124];
    z[24]=z[10]*z[24];
    z[26]=n<T>(5,3)*z[17] - 9*z[10];
    z[26]=z[3]*z[26];
    z[29]= - z[10] + n<T>(3,2)*z[6];
    z[29]=z[2]*z[29];

    r += static_cast<T>(2)+ z[21] + z[22] + z[23] + z[24] + z[25] + n<T>(1,2)*z[26] + 
      z[27] + z[28] + 17*z[29] + 3*z[71] - z[77];
 
    return r;
}

template double qg_2lNLC_r187(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r187(const std::array<dd_real,31>&);
#endif
