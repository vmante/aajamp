#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r680(const std::array<T,31>& k) {
  T z[58];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[5];
    z[4]=k[6];
    z[5]=k[7];
    z[6]=k[12];
    z[7]=k[4];
    z[8]=k[10];
    z[9]=k[15];
    z[10]=k[2];
    z[11]=k[8];
    z[12]=k[14];
    z[13]=k[13];
    z[14]=z[7]*z[8];
    z[15]=z[14] - 1;
    z[15]=z[15]*z[7];
    z[16]=2*z[3];
    z[15]=z[15] - z[16];
    z[17]=z[7] + z[3];
    z[18]=z[17]*z[9];
    z[19]=4*z[7];
    z[20]= - z[19]*z[18];
    z[20]=z[20] + z[15];
    z[20]=z[9]*z[20];
    z[21]=npow(z[4],3);
    z[22]=z[21]*z[16];
    z[23]=z[21]*z[2];
    z[24]=3*z[23] - z[22];
    z[25]=npow(z[3],2);
    z[24]=z[24]*z[25];
    z[26]= - z[23] + z[22];
    z[26]=z[7]*z[26];
    z[27]=z[21]*z[3];
    z[28]= - z[2]*z[27];
    z[26]=z[28] + z[26];
    z[26]=z[7]*z[26];
    z[20]=z[20] + z[24] + z[26];
    z[20]=z[5]*z[20];
    z[24]=z[8]*z[2];
    z[26]= - 3*z[24] + z[14];
    z[28]=npow(z[7],2);
    z[26]=z[28]*z[21]*z[26];
    z[29]=3*z[28];
    z[30]= - z[21]*z[29];
    z[30]= - 2*z[8] + z[30];
    z[30]=z[7]*z[30];
    z[31]=z[16]*z[6];
    z[32]=2*z[7];
    z[33]= - z[9]*z[32];
    z[30]=z[33] + z[30] - static_cast<T>(3)+ z[31];
    z[30]=z[9]*z[30];
    z[20]=z[20] + z[26] + z[30];
    z[20]=z[5]*z[20];
    z[26]=npow(z[6],2);
    z[30]=2*z[26];
    z[23]=z[23]*z[30];
    z[33]=npow(z[11],2);
    z[34]=2*z[33];
    z[35]=2*z[6];
    z[36]= - z[11] - z[35];
    z[36]=z[6]*z[36];
    z[36]= - z[34] + z[36];
    z[36]=z[36]*z[27];
    z[23]=z[23] + z[36];
    z[23]=z[23]*z[25];
    z[36]=z[33]*z[21]*z[25];
    z[37]=npow(z[12],2);
    z[38]=4*z[37];
    z[39]=z[8]*z[38];
    z[36]=z[39] + z[36];
    z[36]=z[36]*z[32];
    z[39]= - z[21]*z[32];
    z[39]= - z[27] + z[39];
    z[39]=z[39]*z[19];
    z[39]=z[8] + z[39];
    z[39]=z[7]*z[39];
    z[40]=z[31] + 1;
    z[41]=z[14] + 2;
    z[41]=z[41]*z[7];
    z[41]=z[41] + z[3];
    z[42]=z[41]*z[9];
    z[39]=z[42] + z[39] - z[40];
    z[39]=z[9]*z[39];
    z[43]=npow(z[7],3);
    z[44]=z[21]*z[43];
    z[44]=6*z[44] - 8;
    z[44]=z[8]*z[44];
    z[39]=z[39] + z[6] + z[44];
    z[39]=z[9]*z[39];
    z[44]=z[8]*z[12];
    z[44]=z[44] + 2*z[37];
    z[44]=4*z[44];
    z[20]=z[20] + z[39] + z[36] + z[44] + z[23];
    z[20]=z[5]*z[20];
    z[23]=z[3]*z[6];
    z[36]=4*z[23];
    z[39]=z[6]*z[2];
    z[45]=5*z[39] - z[36];
    z[46]=npow(z[3],3);
    z[47]=npow(z[4],4);
    z[45]=z[46]*z[47]*z[45];
    z[48]=z[43]*z[47];
    z[49]=z[24] - z[14];
    z[49]=z[49]*z[48];
    z[50]=z[16] + z[7];
    z[48]=z[50]*z[48];
    z[48]=z[48] + z[18];
    z[48]=z[9]*z[48];
    z[45]=z[48] + z[45] + z[49];
    z[45]=z[5]*z[45];
    z[48]=npow(z[7],4);
    z[49]=z[47]*z[8];
    z[50]=z[48]*z[49];
    z[51]= - z[47]*z[48];
    z[51]=static_cast<T>(1)+ z[51];
    z[51]=z[9]*z[51];
    z[51]=z[50] + z[51];
    z[51]=z[9]*z[51];
    z[52]=npow(z[3],4);
    z[53]=z[52]*z[47];
    z[30]=z[53]*z[30];
    z[30]=z[45] + z[30] + z[51];
    z[30]=z[5]*z[30];
    z[45]=z[2] - z[3];
    z[51]=npow(z[4],5);
    z[45]=z[52]*z[45]*z[51]*z[26];
    z[18]=z[48]*z[51]*z[18];
    z[48]=z[7]*z[4];
    z[48]=z[8]*npow(z[48],5);
    z[18]= - z[48] + z[18];
    z[18]=z[9]*z[18];
    z[18]=2*z[45] + z[18];
    z[18]=z[18]*npow(z[5],2);
    z[45]= - npow(z[9],3)*z[48];
    z[18]=z[45] + z[18];
    z[18]=z[1]*z[18];
    z[45]=z[35]*z[33];
    z[48]= - z[45]*z[53];
    z[51]= - z[3] - z[32];
    z[43]=z[43]*z[9];
    z[51]=z[43]*z[47]*z[51];
    z[50]=z[51] - z[6] - 4*z[50];
    z[50]=z[50]*npow(z[9],2);
    z[18]=z[18] + z[30] + z[48] + z[50];
    z[18]=z[5]*z[18];
    z[30]=npow(z[10],2);
    z[48]=z[30]*z[49];
    z[49]=z[8]*z[10];
    z[50]=z[49] - z[14];
    z[51]=3*z[7];
    z[50]=z[50]*z[51];
    z[47]= - z[47]*z[50];
    z[47]=z[48] + z[47];
    z[48]=z[28]*z[9];
    z[47]=z[47]*z[48];
    z[52]=z[6]*z[11];
    z[47]=z[52] + z[47];
    z[47]=z[9]*z[47];
    z[47]= - z[45] + z[47];
    z[47]=z[9]*z[47];
    z[18]=z[47] + z[18];
    z[18]=z[1]*z[18];
    z[47]=z[30]*z[8];
    z[47]=z[47] - z[50];
    z[47]=z[19]*z[21]*z[47];
    z[50]=z[19] - 2*z[10];
    z[50]=z[21]*z[50];
    z[27]=z[27] + z[50];
    z[27]=z[27]*z[48];
    z[40]=z[11]*z[40];
    z[27]=z[27] + z[47] + z[6] + z[40];
    z[27]=z[9]*z[27];
    z[40]=z[37]*z[10];
    z[22]= - z[22]*z[40];
    z[47]=z[33]*z[6];
    z[22]= - z[47] + z[22];
    z[50]=4*z[3];
    z[22]=z[22]*z[50];
    z[22]=z[27] + z[22] - z[34] + 3*z[52];
    z[22]=z[9]*z[22];
    z[21]=z[21]*z[46];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[47]*z[21];
    z[18]=z[18] + z[20] + 2*z[21] + z[22];
    z[18]=z[1]*z[18];
    z[20]=npow(z[4],2);
    z[21]=z[26] - z[20];
    z[21]=z[21]*z[16];
    z[22]=z[20]*z[2];
    z[21]=z[21] - z[35] + z[22];
    z[21]=z[3]*z[21];
    z[27]= - z[7]*z[15];
    z[46]=z[17]*z[48];
    z[27]=z[27] + 2*z[46];
    z[27]=z[9]*z[27];
    z[46]= - npow(z[23],2);
    z[46]=static_cast<T>(1)+ z[46];
    z[46]=z[46]*z[16];
    z[28]= - z[8]*z[28];
    z[27]=3*z[27] + z[46] + z[28];
    z[27]=z[5]*z[27];
    z[22]=z[8] + z[22];
    z[22]=z[22]*z[32];
    z[28]=z[23] + 1;
    z[46]=z[28]*z[50];
    z[53]=static_cast<T>(3)+ 5*z[14];
    z[53]=z[7]*z[53];
    z[46]=z[46] + z[53];
    z[46]=z[9]*z[46];
    z[21]=z[27] + z[46] + z[21] + z[22];
    z[21]=z[5]*z[21];
    z[22]=4*z[39];
    z[27]=z[20]*z[22];
    z[46]= - z[11] - z[6];
    z[46]=z[6]*z[46];
    z[27]=z[27] + z[38] + z[46];
    z[46]= - z[11] - 10*z[6];
    z[46]=z[46]*z[20];
    z[45]= - z[45] + z[46];
    z[45]=z[3]*z[45];
    z[27]=2*z[27] + z[45];
    z[27]=z[3]*z[27];
    z[45]=9*z[14];
    z[46]=z[45] - z[24];
    z[46]=z[20]*z[46];
    z[53]=2*z[12];
    z[54]= - z[8]*z[53];
    z[37]=z[37] + z[54];
    z[37]=8*z[37] + z[46];
    z[37]=z[7]*z[37];
    z[46]=6*z[7];
    z[54]= - z[20]*z[46];
    z[55]=z[20]*z[3];
    z[54]=z[54] + 23*z[8] - 3*z[55];
    z[54]=z[7]*z[54];
    z[56]=2*z[14];
    z[42]= - z[42] - static_cast<T>(3)- z[56];
    z[42]=z[51]*z[42];
    z[51]= - static_cast<T>(5)- z[23];
    z[51]=z[3]*z[51];
    z[42]=z[51] + z[42];
    z[42]=z[9]*z[42];
    z[31]=static_cast<T>(1)- z[31];
    z[31]=z[42] + 2*z[31] + z[54];
    z[31]=z[9]*z[31];
    z[42]=8*z[12];
    z[21]=z[21] + z[31] + z[37] + z[27] - 8*z[6] - z[42] + 5*z[8];
    z[21]=z[5]*z[21];
    z[27]=z[20]*z[12];
    z[31]= - z[47] + 6*z[27];
    z[31]=z[31]*z[16];
    z[27]=z[10]*z[27];
    z[27]=z[31] + 16*z[27] - z[34] + 5*z[52];
    z[27]=z[3]*z[27];
    z[31]=17*z[7] - 9*z[10];
    z[31]=z[20]*z[31];
    z[34]=3*z[8];
    z[37]=z[34] - z[6];
    z[31]=5*z[55] + 2*z[37] + z[31];
    z[31]=z[7]*z[31];
    z[37]=z[11]*z[10];
    z[51]=z[28]*z[11];
    z[52]= - z[6] + z[51];
    z[52]=z[3]*z[52];
    z[54]=3*z[14];
    z[57]=static_cast<T>(4)+ z[54];
    z[57]=z[7]*z[57];
    z[57]=z[3] + z[57];
    z[57]=z[9]*z[57];
    z[31]=z[57] + z[31] + z[52] + static_cast<T>(2)+ z[37];
    z[31]=z[9]*z[31];
    z[30]=z[30]*z[34];
    z[52]=4*z[14];
    z[57]= - 5*z[49] - z[52];
    z[57]=z[7]*z[57];
    z[30]=z[57] + z[30];
    z[20]=z[20]*z[30];
    z[30]=static_cast<T>(1)- z[37];
    z[30]=z[11]*z[30];
    z[30]=z[53] + z[30];
    z[20]=z[31] + z[27] - z[35] + 2*z[30] - 15*z[8] + z[20];
    z[20]=z[9]*z[20];
    z[27]=z[26]*z[55];
    z[27]=3*z[47] + z[27];
    z[27]=z[3]*z[27];
    z[27]=z[27] + z[44] + z[26];
    z[18]=z[18] + z[21] + 2*z[27] + z[20];
    z[18]=z[1]*z[18];
    z[15]=z[15]*z[29];
    z[17]=z[17]*z[43];
    z[15]=z[15] - 4*z[17];
    z[15]=z[9]*z[15];
    z[17]=z[26]*z[16];
    z[20]= - static_cast<T>(2)+ z[39];
    z[20]=z[6]*z[20];
    z[20]=z[20] - z[17];
    z[20]=z[20]*z[16];
    z[20]=3*z[39] + z[20];
    z[20]=z[3]*z[20];
    z[20]=z[2] + z[20];
    z[20]=z[3]*z[20];
    z[21]= - z[24] + z[56];
    z[21]=z[7]*z[21];
    z[21]=z[21] + z[2] - z[50];
    z[21]=z[7]*z[21];
    z[15]=z[15] + z[20] + z[21];
    z[15]=z[5]*z[15];
    z[20]=z[28]*z[25];
    z[20]=z[43] + z[20];
    z[21]=static_cast<T>(3)- z[52];
    z[21]=z[7]*z[21];
    z[21]= - z[16] + z[21];
    z[21]=z[7]*z[21];
    z[20]=z[21] + 2*z[20];
    z[20]=z[9]*z[20];
    z[17]= - z[6] + z[17];
    z[17]=z[3]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[16];
    z[21]=2*z[24] - z[54];
    z[21]=z[7]*z[21];
    z[15]=z[15] + z[20] + z[21] - z[2] + z[17];
    z[15]=z[5]*z[15];
    z[17]= - static_cast<T>(10)+ z[39];
    z[17]=z[6]*z[17];
    z[17]=z[17] - z[53] - z[11];
    z[20]= - z[36] - 4;
    z[20]=z[33]*z[20];
    z[21]=z[6] + z[13];
    z[26]= - 3*z[11] - 4*z[21];
    z[26]=z[6]*z[26];
    z[20]=z[26] + z[20];
    z[20]=z[3]*z[20];
    z[17]=2*z[17] + z[20];
    z[17]=z[3]*z[17];
    z[20]=z[41]*z[48];
    z[26]=static_cast<T>(17)+ z[45];
    z[26]=z[7]*z[26];
    z[26]=8*z[3] + z[26];
    z[26]=z[7]*z[26];
    z[20]=z[26] + 3*z[20];
    z[20]=z[9]*z[20];
    z[25]=z[6]*z[25];
    z[26]=static_cast<T>(5)- 21*z[14];
    z[26]=z[7]*z[26];
    z[20]=z[20] - 5*z[25] + z[26];
    z[20]=z[9]*z[20];
    z[25]=4*z[12];
    z[26]= - z[25] - z[34];
    z[26]=z[26]*z[32];
    z[15]=z[15] + z[20] + z[26] + z[17] + z[22] - static_cast<T>(12)+ 5*z[24];
    z[15]=z[5]*z[15];
    z[17]= - 6*z[14] - static_cast<T>(8)+ 3*z[49];
    z[17]=z[7]*z[17];
    z[20]=z[10] - z[3];
    z[17]=2*z[20] + z[17];
    z[17]=z[9]*z[17];
    z[17]=z[17] - 18*z[14] - static_cast<T>(20)+ 9*z[49];
    z[17]=z[7]*z[17];
    z[17]=z[10] - 5*z[3] + z[17];
    z[17]=z[9]*z[17];
    z[20]=z[37] - z[49];
    z[22]=z[53] - z[40];
    z[22]=4*z[22] + z[51];
    z[22]=z[22]*z[16];
    z[24]=11*z[8] - z[6];
    z[24]=z[24]*z[32];
    z[26]=z[10]*z[42];
    z[17]=z[17] + z[24] + z[22] - static_cast<T>(17)+ z[26] + 4*z[20];
    z[17]=z[9]*z[17];
    z[20]=z[6]*z[21];
    z[20]=z[20] + z[38] + z[33];
    z[21]=z[33]*z[23];
    z[20]=2*z[20] + 3*z[21];
    z[20]=z[20]*z[16];
    z[19]= - z[13]*z[19];
    z[19]=z[19] + 8;
    z[19]=z[6]*z[19];
    z[21]= - z[13] + z[11];
    z[15]=z[18] + z[15] + z[17] + z[20] + 4*z[21] + 7*z[8] + z[19];
    z[15]=z[1]*z[15];
    z[17]=z[25] + 3*z[6];
    z[17]=z[17]*z[16];
    z[16]=z[2] - z[16];
    z[16]=5*z[16] + z[46];
    z[16]=z[5]*z[16];
    z[18]=z[2]*z[13];
    z[18]= - static_cast<T>(1)+ 2*z[18];
    z[19]=4*z[10] + 3*z[2];
    z[19]=z[8]*z[19];
    z[20]=23*z[7] - 19*z[10] - 7*z[3];
    z[20]=z[9]*z[20];

    r +=  - 13*z[14] + z[15] + z[16] + z[17] + 2*z[18] + z[19] + z[20];
 
    return r;
}

template double qg_2lNLC_r680(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r680(const std::array<dd_real,31>&);
#endif
