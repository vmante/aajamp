#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r30(const std::array<T,31>& k) {
  T z[45];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[9];
    z[6]=k[13];
    z[7]=k[12];
    z[8]=k[15];
    z[9]=k[10];
    z[10]=k[2];
    z[11]=k[5];
    z[12]=k[14];
    z[13]=k[3];
    z[14]=k[4];
    z[15]=n<T>(1,2)*z[4];
    z[16]=z[8]*z[10];
    z[17]=static_cast<T>(1)+ z[16];
    z[17]=z[17]*z[15];
    z[18]=z[5] - z[6];
    z[19]= - static_cast<T>(4)- z[16];
    z[19]=z[8]*z[19];
    z[17]=z[17] + z[19] + z[18];
    z[17]=z[4]*z[17];
    z[19]=n<T>(1,2)*z[5];
    z[20]=z[13] + z[10];
    z[20]=z[20]*z[19];
    z[20]= - static_cast<T>(2)+ z[20];
    z[20]=z[5]*z[20];
    z[21]=z[5]*z[10];
    z[22]= - z[16] - z[21];
    z[22]=z[4]*z[22];
    z[23]=2*z[8];
    z[24]= - z[14]*z[23];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[3]*z[24];
    z[20]=z[22] + z[20] - 8*z[8] + z[24];
    z[20]=z[9]*z[20];
    z[22]=5*z[12];
    z[24]=z[22] - z[23];
    z[24]=z[24]*z[8];
    z[25]=3*z[8];
    z[26]=z[8]*z[14];
    z[27]=static_cast<T>(2)- z[26];
    z[27]=z[27]*z[25];
    z[27]=z[27] - z[22] - z[6];
    z[27]=z[3]*z[27];
    z[28]=11*z[8] + 7*z[5];
    z[28]=z[28]*z[19];
    z[17]=z[20] + z[17] + z[28] - z[24] + z[27];
    z[17]=z[9]*z[17];
    z[20]=n<T>(1,2)*z[6];
    z[25]=n<T>(7,2)*z[5] + z[20] + z[25];
    z[25]=z[5]*z[25];
    z[20]=z[20] - z[23];
    z[20]=z[4]*z[20];
    z[27]=n<T>(3,2)*z[4];
    z[28]= - z[27] - 6*z[8] + z[19];
    z[28]=z[9]*z[28];
    z[20]=z[28] + z[25] + z[20];
    z[20]=z[9]*z[20];
    z[25]=z[3]*z[11];
    z[28]= - static_cast<T>(1)+ z[25];
    z[28]=z[28]*z[15];
    z[29]=n<T>(1,2)*z[3];
    z[28]=z[28] - z[23] + z[29];
    z[28]=z[4]*z[28];
    z[30]=3*z[3];
    z[31]=z[8]*z[11];
    z[32]=static_cast<T>(1)- z[31];
    z[32]=z[5]*z[32];
    z[32]=z[32] - n<T>(7,2)*z[8] - z[30];
    z[32]=z[5]*z[32];
    z[33]= - z[6]*z[29];
    z[34]=2*z[3];
    z[35]= - 5*z[4] + n<T>(5,2)*z[5] - z[8] - z[34];
    z[35]=z[9]*z[35];
    z[28]=z[35] + z[28] + z[33] + z[32];
    z[28]=z[7]*z[28];
    z[32]=z[8] - z[6];
    z[33]=z[32]*z[3];
    z[33]=5*z[33];
    z[35]=z[5]*z[8];
    z[36]= - z[33] - 3*z[35];
    z[36]=z[36]*z[19];
    z[37]=z[4]*z[3];
    z[38]= - z[6] + z[15];
    z[38]=z[38]*z[37];
    z[20]=z[28] + z[20] + z[36] + z[38];
    z[20]=z[7]*z[20];
    z[28]=z[3]*z[8];
    z[36]=npow(z[5],2);
    z[21]= - n<T>(5,2) - z[21];
    z[21]=z[5]*z[21];
    z[21]= - z[8] + z[21];
    z[21]=z[4]*z[21];
    z[21]=z[21] + z[28] + n<T>(1,2)*z[36];
    z[21]=z[9]*z[21];
    z[36]= - z[8] + 4*z[12];
    z[36]=z[36]*z[28];
    z[38]=z[19]*z[4];
    z[18]= - z[18]*z[38];
    z[18]=z[21] - z[36] + z[18];
    z[18]=z[9]*z[18];
    z[21]=z[5]*z[6];
    z[39]=npow(z[6],2);
    z[40]=z[21] + z[39];
    z[41]= - z[5]*z[40];
    z[42]= - z[4]*z[39];
    z[41]=z[41] + z[42];
    z[42]=2*z[6];
    z[43]= - z[42] - z[8];
    z[43]=z[4]*z[43];
    z[21]= - 2*z[21] + z[43];
    z[21]=z[9]*z[21];
    z[21]=n<T>(1,2)*z[41] + z[21];
    z[21]=z[9]*z[21];
    z[41]=z[28] - 5*z[35];
    z[43]= - z[4]*z[8];
    z[44]= - z[8] - z[4];
    z[44]=z[9]*z[44];
    z[41]=z[44] + n<T>(1,2)*z[41] + z[43];
    z[41]=z[9]*z[41];
    z[28]=n<T>(1,2)*z[28] - z[35];
    z[28]=z[5]*z[28];
    z[28]=z[28] + z[41];
    z[28]=z[7]*z[28];
    z[35]=z[5]*z[3];
    z[32]= - z[32]*z[35];
    z[39]=z[39]*z[3];
    z[32]=z[39] + z[32];
    z[32]=z[5]*z[32];
    z[41]=z[4]*z[39];
    z[32]=z[32] + z[41];
    z[21]=z[28] + n<T>(1,2)*z[32] + z[21];
    z[21]=z[7]*z[21];
    z[28]= - z[40]*z[38];
    z[32]=z[4]*z[5];
    z[40]= - z[42] - z[5];
    z[40]=z[9]*z[40]*z[32];
    z[28]=z[28] + z[40];
    z[28]=z[9]*z[28];
    z[40]=z[6]*z[35];
    z[39]=z[39] + z[40];
    z[38]=z[39]*z[38];
    z[21]=z[21] + z[38] + z[28];
    z[21]=z[1]*z[21];
    z[28]= - z[5]*z[36];
    z[36]=n<T>(3,2)*z[6];
    z[35]= - z[35]*z[36];
    z[32]=z[3]*z[32];
    z[32]=z[35] + z[32];
    z[32]=z[4]*z[32];
    z[18]=z[21] + z[20] + z[18] + z[28] + z[32];
    z[18]=z[1]*z[18];
    z[20]=npow(z[11],2);
    z[21]=z[20]*z[3];
    z[28]=z[21] - z[11];
    z[32]= - z[14] + z[28];
    z[32]=z[32]*z[15];
    z[35]=z[11]*z[29];
    z[32]=z[32] + z[35] - static_cast<T>(7)- z[31];
    z[32]=z[4]*z[32];
    z[35]=z[3]*z[13];
    z[38]=z[5]*z[13];
    z[35]=z[35] - z[38];
    z[35]=z[35]*z[9];
    z[39]= - n<T>(1,2)*z[13] + z[11];
    z[39]=z[5]*z[39];
    z[39]=z[39] - n<T>(7,2) - z[31];
    z[39]=z[5]*z[39];
    z[40]=n<T>(1,2)*z[8];
    z[41]=z[6]*z[11];
    z[42]=static_cast<T>(2)- n<T>(1,2)*z[41];
    z[42]=z[3]*z[42];
    z[32]= - z[35] + z[32] + z[39] + z[40] + z[42];
    z[32]=z[7]*z[32];
    z[39]=static_cast<T>(1)+ z[25];
    z[27]=z[39]*z[27];
    z[27]=z[27] + z[34] + z[36] - z[8];
    z[27]=z[4]*z[27];
    z[39]=11*z[6] + 9*z[5];
    z[39]=z[39]*z[19];
    z[35]=z[35] - n<T>(17,2)*z[4] - 5*z[5] - n<T>(25,2)*z[8] + z[30];
    z[35]=z[9]*z[35];
    z[27]=z[32] + z[35] + z[27] + z[33] + z[39];
    z[27]=z[7]*z[27];
    z[32]=z[3]*z[36];
    z[33]= - n<T>(7,2)*z[6] + z[34];
    z[33]=z[5]*z[33];
    z[35]= - z[6] - z[30];
    z[35]=n<T>(1,2)*z[35] + z[5];
    z[35]=z[4]*z[35];
    z[32]=z[35] + z[32] + z[33];
    z[32]=z[4]*z[32];
    z[33]=npow(z[8],2);
    z[30]= - z[33]*z[30];
    z[35]= - 9*z[12] + z[8];
    z[35]=z[8]*z[35];
    z[36]=z[3]*z[23];
    z[35]=z[35] + z[36];
    z[35]=z[5]*z[35];
    z[17]=z[18] + z[27] + z[17] + z[32] + z[30] + z[35];
    z[18]=npow(z[2],2);
    z[17]=z[1]*z[18]*z[17];
    z[27]=n<T>(3,2)*z[13];
    z[30]=npow(z[14],2);
    z[32]=z[30]*z[8];
    z[35]=z[32] + z[27] + z[14];
    z[35]=z[3]*z[35];
    z[36]=2*z[14];
    z[39]= - 3*z[10] + z[36];
    z[39]=z[8]*z[39];
    z[42]=z[19]*z[13];
    z[43]=z[42] - 3;
    z[43]=z[10]*z[43];
    z[43]= - 2*z[13] + z[43];
    z[43]=z[5]*z[43];
    z[35]=z[43] + z[35] + static_cast<T>(2)+ z[39];
    z[35]=z[9]*z[35];
    z[39]=z[10] - z[36];
    z[39]=z[8]*z[39];
    z[39]=static_cast<T>(1)+ z[39];
    z[39]=z[39]*z[23];
    z[23]=z[30]*z[23];
    z[23]= - 5*z[14] + z[23];
    z[23]=z[8]*z[23];
    z[30]=z[6]*z[14];
    z[30]=n<T>(5,2)*z[30];
    z[23]=z[23] + static_cast<T>(6)- z[30];
    z[23]=z[3]*z[23];
    z[30]=z[30] - 3;
    z[43]=n<T>(5,2)*z[16] - z[30];
    z[43]=z[4]*z[43];
    z[16]= - n<T>(7,2) - z[16];
    z[16]=z[5]*z[16];
    z[16]=z[35] + z[43] + z[16] + z[39] + z[23];
    z[16]=z[9]*z[16];
    z[23]=z[11] - z[14];
    z[35]= - z[6]*z[23];
    z[35]= - static_cast<T>(1)+ z[35];
    z[39]=z[14] + n<T>(1,2)*z[11];
    z[39]=z[3]*z[39];
    z[35]=n<T>(1,2)*z[35] + z[39];
    z[35]=z[4]*z[35];
    z[30]=z[3]*z[30];
    z[39]= - static_cast<T>(1)- 3*z[41];
    z[39]=z[5]*z[39];
    z[30]=z[35] + 2*z[39] + z[30] + 6*z[6] - z[40];
    z[30]=z[4]*z[30];
    z[20]=z[20]*z[29];
    z[20]=z[20] + z[23];
    z[20]=z[4]*z[20];
    z[29]= - static_cast<T>(13)- z[41];
    z[20]=z[20] + n<T>(1,2)*z[29] - z[25];
    z[20]=z[4]*z[20];
    z[25]= - z[38] - 7;
    z[25]=z[11]*z[25];
    z[25]=3*z[13] + z[25];
    z[19]=z[25]*z[19];
    z[25]= - z[4]*z[14];
    z[25]=z[25] - 7;
    z[25]=z[11]*z[25];
    z[21]= - z[21] + z[25];
    z[15]=z[21]*z[15];
    z[21]= - z[27] + z[11];
    z[21]=z[3]*z[21];
    z[15]=z[15] + z[21] + z[19];
    z[15]=z[7]*z[15];
    z[19]= - z[42] - static_cast<T>(7)- z[41];
    z[19]=z[5]*z[19];
    z[21]=static_cast<T>(7)+ n<T>(5,2)*z[31];
    z[21]=z[3]*z[21];
    z[15]=z[15] + z[20] + z[19] - n<T>(11,2)*z[8] + z[21];
    z[15]=z[7]*z[15];
    z[19]=z[14]*z[33]*z[34];
    z[20]= - z[10]*z[22];
    z[20]=static_cast<T>(4)+ z[20];
    z[20]=z[8]*z[20];
    z[20]=5*z[6] + z[20];
    z[20]=z[5]*z[20];
    z[15]=z[15] + z[16] + z[30] + z[20] + z[24] + z[19];
    z[15]=z[15]*z[18];
    z[15]=z[15] + z[17];
    z[15]=z[1]*z[15];
    z[16]= - z[23]*z[37];
    z[17]= - z[36] - z[32];
    z[17]=z[3]*z[17];
    z[17]=z[17] + static_cast<T>(2)+ z[26];
    z[17]=z[9]*z[17];
    z[19]=z[11]*z[34];
    z[20]= - z[4]*z[28];
    z[19]=z[19] + z[20];
    z[19]=z[7]*z[19];
    z[16]=z[19] + z[17] + z[34] + z[16];
    z[16]=z[16]*z[18];
    z[15]=z[16] + z[15];

    r += z[15]*z[1];
 
    return r;
}

template double qg_2lNLC_r30(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r30(const std::array<dd_real,31>&);
#endif
