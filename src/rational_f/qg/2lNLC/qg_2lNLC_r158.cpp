#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r158(const std::array<T,31>& k) {
  T z[129];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[2];
    z[7]=k[5];
    z[8]=k[6];
    z[9]=k[14];
    z[10]=k[4];
    z[11]=k[30];
    z[12]=k[7];
    z[13]=k[13];
    z[14]=k[9];
    z[15]=k[3];
    z[16]=k[28];
    z[17]=k[26];
    z[18]=k[21];
    z[19]=k[20];
    z[20]=k[27];
    z[21]=npow(z[16],2);
    z[22]=2*z[21];
    z[23]=npow(z[15],2);
    z[24]=npow(z[8],3);
    z[25]=z[23]*z[24]*z[22];
    z[26]=z[9]*z[16];
    z[26]=z[26] - z[22];
    z[26]=z[26]*z[9];
    z[27]=z[2] + z[17];
    z[28]=npow(z[4],2);
    z[27]=z[28]*z[27];
    z[29]=npow(z[10],2);
    z[30]=z[29]*z[24];
    z[31]=npow(z[12],2);
    z[32]=z[30]*z[31];
    z[33]=z[12]*z[9];
    z[34]=npow(z[9],2);
    z[35]= - z[33] + 7*z[34];
    z[36]= - z[12]*z[35];
    z[37]=4*z[34];
    z[38]= - z[13]*z[37];
    z[25]= - z[32] + z[25] + z[36] + z[26] + z[38] + z[27];
    z[25]=z[10]*z[25];
    z[27]=npow(z[6],2);
    z[36]=z[24]*z[27];
    z[38]=z[15]*z[11];
    z[39]=z[14]*z[15];
    z[40]= - z[38] - z[39];
    z[40]=z[40]*z[36];
    z[41]=z[12]*z[15];
    z[42]=z[10]*z[12];
    z[43]= - 3*z[41] + z[42];
    z[44]=z[24]*z[10];
    z[43]=z[43]*z[44];
    z[45]=z[2]*z[4];
    z[43]=z[43] + z[33] + n<T>(11,3)*z[45];
    z[46]=n<T>(1,2)*z[10];
    z[43]=z[43]*z[46];
    z[47]=npow(z[11],2);
    z[48]=5*z[47];
    z[49]=npow(z[14],2);
    z[50]= - z[48] + 6*z[49];
    z[51]= - z[50]*z[27];
    z[51]=n<T>(11,6) + z[51];
    z[51]=z[2]*z[51];
    z[52]=z[14] - z[11];
    z[53]=z[52]*z[3];
    z[54]=z[27]*z[2];
    z[55]=z[54]*z[53];
    z[40]=z[55] + z[43] + z[51] + z[40];
    z[40]=z[3]*z[40];
    z[43]=npow(z[15],3);
    z[51]=z[43]*z[22];
    z[55]=z[49]*z[6];
    z[56]=n<T>(1,2)*z[55];
    z[57]=z[48]*z[15];
    z[58]= - z[57] + z[56];
    z[58]=z[58]*z[27];
    z[59]=npow(z[6],3);
    z[60]=z[59]*z[2];
    z[61]=n<T>(1,2)*z[14];
    z[62]= - z[61]*z[60];
    z[51]=z[62] + z[51] + z[58];
    z[51]=z[51]*z[24];
    z[58]=3*z[17];
    z[62]=4*z[9];
    z[63]=z[15]*z[17];
    z[64]= - z[13]*z[63];
    z[64]=z[64] + z[58] + z[62];
    z[64]=z[13]*z[64];
    z[65]=z[6]*z[50];
    z[66]=z[2]*z[6];
    z[67]= - z[14] - z[55];
    z[67]=z[67]*z[66];
    z[65]=z[67] + z[65] - n<T>(169,18)*z[4] + z[11] + n<T>(115,18)*z[14];
    z[65]=z[2]*z[65];
    z[67]=npow(z[13],2);
    z[68]= - z[67]*z[39];
    z[69]=2*z[13];
    z[70]=z[58] - z[69];
    z[70]=z[4]*z[70];
    z[25]=z[40] + z[25] + z[51] + z[65] + z[70] + 6*z[33] + z[68] + 
    z[34] + z[64];
    z[25]=z[3]*z[25];
    z[40]=z[55]*z[62];
    z[51]=2*z[4];
    z[64]= - z[14] + z[51];
    z[64]=2*z[64] + n<T>(3,2)*z[55];
    z[64]=z[2]*z[64];
    z[65]=z[62]*z[67];
    z[68]= - z[19]*z[28];
    z[68]= - z[65] + z[68];
    z[68]=z[10]*z[68];
    z[70]=3*z[16];
    z[71]=z[70]*z[9];
    z[72]=z[13]*z[62];
    z[73]=3*z[9];
    z[74]= - z[73] - z[14];
    z[74]=z[12]*z[74];
    z[75]=z[4] - z[19] - n<T>(7,2)*z[12];
    z[75]=z[4]*z[75];
    z[64]=z[68] + z[64] - z[40] + z[75] + z[74] - z[71] + z[72];
    z[64]=z[2]*z[64];
    z[68]=n<T>(1,2)*z[12];
    z[72]=z[68]*z[49];
    z[74]=z[24]*z[42];
    z[75]=npow(z[2],2);
    z[76]=z[75]*z[4];
    z[77]=3*z[76];
    z[78]=z[49]*z[4];
    z[79]=z[24]*z[6];
    z[80]=z[9]*z[79];
    z[74]= - n<T>(3,2)*z[74] + n<T>(15,2)*z[80] - z[77] + z[72] + z[78];
    z[74]=z[7]*z[74];
    z[80]=7*z[11];
    z[81]= - z[80] + n<T>(41,2)*z[9];
    z[81]=z[81]*z[36];
    z[82]=n<T>(1,3)*z[18];
    z[83]=z[82] - z[12];
    z[30]=z[83]*z[30];
    z[83]=z[12]*z[14];
    z[84]=3*z[14];
    z[85]=z[4]*z[84];
    z[86]=2*z[2];
    z[87]=n<T>(43,3)*z[4] - z[86];
    z[87]=z[2]*z[87];
    z[30]=z[74] + 7*z[30] + z[81] + z[87] + 4*z[83] + z[85];
    z[30]=z[7]*z[30];
    z[74]= - z[59]*z[24];
    z[81]=2*z[12];
    z[85]=z[36] + z[81] - n<T>(295,18)*z[4];
    z[85]=z[10]*z[85];
    z[87]= - 295*z[6] + 313*z[54];
    z[88]= - n<T>(295,18) + z[42];
    z[88]=z[10]*z[88];
    z[87]=n<T>(1,18)*z[87] + z[88];
    z[87]=z[3]*z[87];
    z[88]=z[45]*z[7];
    z[89]=z[88] + z[2];
    z[89]= - z[36] + z[12] - n<T>(295,9)*z[4] + n<T>(295,18)*z[89];
    z[89]=z[7]*z[89];
    z[90]=n<T>(1,2)*z[66];
    z[91]= - static_cast<T>(1)+ z[90];
    z[74]=z[89] + z[87] + z[85] + n<T>(295,9)*z[91] + z[74];
    z[74]=z[5]*z[74];
    z[85]= - z[79] - z[44];
    z[85]=z[85]*z[46];
    z[87]=n<T>(5,3)*z[18];
    z[85]=z[85] + n<T>(1,2)*z[36] - z[4] - z[87] + n<T>(13,2)*z[12];
    z[85]=z[10]*z[85];
    z[29]=z[29]*z[3];
    z[89]=z[68]*z[29];
    z[91]=z[66] - 1;
    z[85]=z[89] + n<T>(280,9)*z[91] + z[85];
    z[85]=z[3]*z[85];
    z[89]=5*z[12];
    z[92]=n<T>(7,3)*z[18];
    z[93]=z[92] - z[89];
    z[94]=z[24]*npow(z[10],3);
    z[93]=z[93]*z[94];
    z[95]=n<T>(355,18) - z[66];
    z[95]=z[2]*z[95];
    z[30]=z[74] + z[30] + z[85] + z[93] + z[95] - n<T>(337,18)*z[4] - z[87]
    + n<T>(19,2)*z[12];
    z[30]=z[5]*z[30];
    z[74]=15*z[47];
    z[85]=z[74] - z[37];
    z[85]=z[85]*z[36];
    z[93]=z[48]*z[79];
    z[95]=z[4]*z[61];
    z[96]=z[16] + n<T>(7,2)*z[4];
    z[96]=z[2]*z[96];
    z[95]=z[95] + z[96];
    z[95]=z[7]*z[95]*z[24];
    z[93]=z[93] + z[95];
    z[93]=z[7]*z[93];
    z[95]=z[75]*z[51];
    z[96]=z[13]*z[34];
    z[96]=z[96] - z[95];
    z[85]=z[93] + n<T>(4,3)*z[32] + 4*z[96] + z[85];
    z[85]=z[7]*z[85];
    z[44]= - 67*z[79] + 29*z[44];
    z[44]=z[10]*z[44];
    z[79]=n<T>(7,3)*z[4];
    z[93]=n<T>(3,2)*z[12];
    z[36]=n<T>(1,6)*z[44] + n<T>(35,6)*z[36] - z[93] + z[79];
    z[36]=z[10]*z[36];
    z[36]= - n<T>(7,3)*z[91] + z[36];
    z[36]=z[3]*z[36];
    z[44]=z[24]*z[60];
    z[96]=n<T>(299,18) - z[66];
    z[96]=z[2]*z[96];
    z[36]=z[36] - n<T>(1,2)*z[44] + z[96] - n<T>(80,9)*z[4] - z[9] + 3*z[12];
    z[36]=z[3]*z[36];
    z[44]=3*z[2];
    z[96]=n<T>(44,3)*z[4] - z[44];
    z[96]=z[96]*z[86];
    z[94]=z[31]*z[94];
    z[97]=z[4]*z[14];
    z[30]=z[30] + z[85] + z[36] + n<T>(1,3)*z[94] + z[96] + z[83] - n<T>(97,18)*
    z[97];
    z[30]=z[5]*z[30];
    z[36]=4*z[67];
    z[85]=z[36] + 7*z[31];
    z[94]=n<T>(5,2)*z[2];
    z[96]=z[12]*z[94];
    z[85]=z[96] + n<T>(1,3)*z[85] + z[28];
    z[85]=z[10]*z[2]*z[85];
    z[96]=n<T>(7,3)*z[31];
    z[98]= - n<T>(49,6)*z[12] + z[51];
    z[98]=z[4]*z[98];
    z[99]=z[4] - z[12];
    z[99]=z[2]*z[99];
    z[98]=z[99] - z[96] + z[98];
    z[98]=z[2]*z[98];
    z[99]=z[4]*z[12];
    z[100]=z[96] + 4*z[99];
    z[101]=n<T>(1,2)*z[49];
    z[102]= - z[101] + z[100];
    z[102]=z[4]*z[102];
    z[98]=z[102] + z[98];
    z[98]=z[7]*z[98];
    z[102]= - z[67]*z[63];
    z[103]= - z[39] - 2*z[41];
    z[103]=z[103]*z[28];
    z[85]=z[98] + z[85] + z[102] + z[103];
    z[85]=z[24]*z[85];
    z[72]= - z[75]*z[72];
    z[72]=z[72] + z[85];
    z[72]=z[7]*z[72];
    z[75]=z[83] + z[99];
    z[44]=z[75]*z[44];
    z[75]= - z[12]*z[28];
    z[32]= - z[32] + z[75] + z[44];
    z[32]=z[2]*z[32];
    z[44]=z[67]*z[89];
    z[75]=z[99]*z[17];
    z[44]= - z[75] + z[65] + z[44];
    z[44]=z[4]*z[44];
    z[85]=2*z[14];
    z[98]= - z[34]*z[27]*z[85];
    z[102]=z[67]*z[17];
    z[103]=z[23]*z[102];
    z[98]=z[103] + z[98];
    z[98]=z[98]*z[24];
    z[103]= - z[12]*z[102];
    z[32]=z[72] + 2*z[98] + z[103] + z[44] + z[32];
    z[32]=z[7]*z[32];
    z[44]=npow(z[8],4);
    z[72]=z[44]*z[27];
    z[98]= - z[6] + z[46];
    z[98]=z[10]*z[44]*z[98];
    z[98]=n<T>(1,2)*z[72] + z[98];
    z[98]=z[98]*z[29];
    z[103]=z[2] - z[4];
    z[104]=z[10]*z[8];
    z[105]=npow(z[104],4);
    z[106]=z[12]*z[105];
    z[106]=n<T>(265,9)*z[103] + z[106];
    z[98]=n<T>(1,2)*z[106] + z[98];
    z[98]=z[3]*z[98];
    z[72]=z[72]*z[7];
    z[106]= - z[48] - 8*z[34];
    z[106]=z[106]*z[72];
    z[77]= - z[77] + z[106];
    z[77]=z[7]*z[77];
    z[106]=2*z[9];
    z[107]=z[59]*z[44]*z[106];
    z[108]=z[80] - 6*z[9];
    z[72]=z[108]*z[72];
    z[72]=z[72] + n<T>(295,6)*z[45] + z[107];
    z[72]=z[7]*z[72];
    z[107]= - z[10]*z[4];
    z[91]=z[107] + z[91];
    z[91]=z[3]*z[91];
    z[91]=n<T>(1,6)*z[91] - n<T>(1,2)*z[4] + n<T>(1,3)*z[2];
    z[72]=n<T>(295,3)*z[91] + z[72];
    z[72]=z[5]*z[72];
    z[91]=n<T>(8,3)*z[4] - z[2];
    z[91]=z[2]*z[91];
    z[72]=z[72] + z[77] + z[91] + z[98];
    z[72]=z[5]*z[72];
    z[77]= - z[31]*z[105];
    z[91]=z[105]*z[68];
    z[91]= - n<T>(7,3)*z[103] + z[91];
    z[91]=z[3]*z[91];
    z[98]=n<T>(5,9)*z[4] - z[2];
    z[98]=z[2]*z[98];
    z[77]=z[91] + z[98] + z[77];
    z[77]=z[3]*z[77];
    z[72]=z[72] - 6*z[76] + z[77];
    z[72]=z[5]*z[72];
    z[77]=z[3]*z[103];
    z[77]=z[45] + n<T>(1,3)*z[77];
    z[59]= - z[34]*z[59]*npow(z[8],5)*npow(z[7],2);
    z[59]=n<T>(295,6)*z[77] + z[59];
    z[59]=z[5]*z[59];
    z[77]=z[45]*z[3];
    z[59]=z[59] - z[76] - n<T>(5,3)*z[77];
    z[59]=z[5]*z[59];
    z[91]=z[76] + n<T>(7,3)*z[77];
    z[91]=z[91]*z[3];
    z[59]= - z[91] + z[59];
    z[59]=z[5]*z[59];
    z[77]=z[1]*npow(z[5],3)*z[77];
    z[59]=z[59] + n<T>(295,18)*z[77];
    z[59]=z[1]*z[59];
    z[65]=z[4]*z[65];
    z[77]= - z[96] - 2*z[99];
    z[77]=z[4]*z[77];
    z[98]=z[45]*z[12];
    z[77]=z[77] - z[98];
    z[44]=z[2]*z[77]*z[44]*npow(z[7],4);
    z[44]=z[59] + z[72] + z[44] + z[65] + z[91];
    z[44]=z[1]*z[44];
    z[59]=5*z[14];
    z[65]=z[9] - z[13];
    z[65]=z[65]*z[59];
    z[72]=z[69]*z[17];
    z[35]=z[65] + z[72] - z[35];
    z[35]=z[12]*z[35];
    z[65]=9*z[13];
    z[77]=7*z[9];
    z[91]= - z[77] - z[65];
    z[91]=z[13]*z[91];
    z[103]=z[4]*z[17];
    z[105]=2*z[17];
    z[107]=z[105] - 7*z[13];
    z[107]=z[12]*z[107];
    z[91]=z[103] + z[91] + z[107];
    z[91]=z[4]*z[91];
    z[24]= - z[43]*z[24]*z[102];
    z[107]=z[17] + z[62];
    z[107]=z[13]*z[107];
    z[37]=z[37] + z[107];
    z[37]=z[13]*z[37];
    z[24]=z[44] + z[30] + z[32] + z[25] + z[24] + z[91] + z[35] + z[26]
    + z[37] + z[64];
    z[24]=z[1]*z[24];
    z[25]=2*z[42];
    z[26]=z[15]*z[70];
    z[26]=z[25] + z[26] - n<T>(5,2)*z[41];
    z[30]=npow(z[8],2);
    z[26]=z[30]*z[26];
    z[32]=2*z[16];
    z[35]= - z[9]*z[32];
    z[35]=z[21] + z[35];
    z[37]=z[13]*z[77];
    z[44]=9*z[9] - z[12];
    z[44]=z[12]*z[44];
    z[64]= - z[69] + z[17] + z[9];
    z[64]=z[4]*z[64];
    z[26]= - n<T>(73,18)*z[45] + z[64] + z[44] + 2*z[35] + z[37] + z[26];
    z[26]=z[10]*z[26];
    z[35]=z[15] - n<T>(61,6)*z[6];
    z[35]=z[35]*z[30];
    z[37]= - n<T>(3,2)*z[33] + n<T>(13,3)*z[30];
    z[37]=z[10]*z[37];
    z[35]=z[37] + z[35] + n<T>(3,2)*z[9] + z[12];
    z[35]=z[10]*z[35];
    z[37]=2*z[6];
    z[44]=z[50]*z[37];
    z[50]=13*z[11];
    z[44]=z[44] + z[50] - 11*z[14];
    z[44]=z[6]*z[44];
    z[64]=3*z[6];
    z[52]=z[52]*z[64];
    z[52]= - n<T>(1,2) + z[52];
    z[52]=z[52]*z[66];
    z[91]=z[30]*z[6];
    z[107]=n<T>(5,2)*z[15] + n<T>(16,3)*z[6];
    z[107]=z[107]*z[91];
    z[108]=2*z[27];
    z[53]= - z[53]*z[108];
    z[35]=z[53] + z[35] + z[107] + z[52] + static_cast<T>(1)+ z[44];
    z[35]=z[3]*z[35];
    z[44]= - z[48] - n<T>(11,2)*z[49];
    z[44]=z[6]*z[44];
    z[52]=5*z[11];
    z[53]=z[52] + n<T>(146,9)*z[14];
    z[44]=2*z[53] + z[44];
    z[44]=z[6]*z[44];
    z[53]=n<T>(5,2)*z[14];
    z[107]= - z[53] - z[55];
    z[107]=z[107]*z[54];
    z[44]=z[107] - n<T>(181,18) + z[44];
    z[44]=z[2]*z[44];
    z[107]=z[23]*z[70];
    z[109]=3*z[38] - z[39];
    z[110]=z[11] - z[85];
    z[110]=z[6]*z[110];
    z[109]=2*z[109] + z[110];
    z[109]=z[6]*z[109];
    z[110]=z[23]*z[13];
    z[107]=z[109] + z[107] - z[110];
    z[107]=z[107]*z[30];
    z[72]=z[23]*z[72];
    z[72]=z[72] - static_cast<T>(3)- 5*z[63];
    z[72]=z[13]*z[72];
    z[109]=z[15] + z[110];
    z[109]=z[13]*z[109];
    z[109]= - static_cast<T>(7)+ z[109];
    z[109]=z[109]*z[85];
    z[111]=3*z[4];
    z[112]=z[48]*z[6];
    z[26]=z[35] + z[26] + z[107] + z[44] - z[112] - z[111] - 6*z[12] + 
    z[109] + z[72] - z[73] + z[16] + 4*z[17] + z[80];
    z[26]=z[3]*z[26];
    z[35]=2*z[11];
    z[44]=z[94] - z[4] + z[61] - z[35] + z[13];
    z[44]=z[44]*z[30];
    z[72]= - z[14]*z[67];
    z[44]=z[44] - z[95] - n<T>(21,2)*z[78] - z[102] + z[72];
    z[44]=z[7]*z[44];
    z[72]=n<T>(4,3)*z[12];
    z[94]=z[72] - z[69] + z[84];
    z[94]=z[12]*z[94];
    z[95]=2*z[34];
    z[107]=3*z[21];
    z[109]= - z[107] - z[95];
    z[113]= - z[53] - 16*z[11] + 13*z[9];
    z[113]=z[6]*z[113];
    z[113]=z[113] - n<T>(7,2)*z[66];
    z[113]=z[113]*z[30];
    z[114]=z[92] - z[32];
    z[115]= - z[51] + n<T>(22,3)*z[12] + z[13] + z[114];
    z[116]=z[30]*z[10];
    z[115]=z[115]*z[116];
    z[117]= - z[17] - z[73];
    z[117]=z[13]*z[117];
    z[118]=z[69] - n<T>(29,2)*z[14];
    z[118]=z[14]*z[118];
    z[119]=4*z[2];
    z[120]= - z[119] - z[13] + n<T>(239,6)*z[4];
    z[120]=z[2]*z[120];
    z[44]=z[44] + z[115] + z[113] + z[120] - n<T>(418,9)*z[97] + z[94] + 
    z[118] + 2*z[109] + z[117];
    z[44]=z[7]*z[44];
    z[94]= - 5*z[97] - z[49] - z[83];
    z[109]= - 3*z[78] - z[76];
    z[109]=z[7]*z[109];
    z[113]=14*z[4] - z[2];
    z[113]=z[2]*z[113];
    z[94]=z[109] + z[30] + n<T>(5,2)*z[94] + z[113];
    z[94]=z[7]*z[94];
    z[109]=15*z[12];
    z[113]=static_cast<T>(9)- z[66];
    z[113]=z[2]*z[113];
    z[59]=z[94] + n<T>(9,2)*z[116] - n<T>(23,2)*z[91] + z[113] - n<T>(118,3)*z[4] - 
    z[109] - z[59] + z[114];
    z[59]=z[7]*z[59];
    z[94]=n<T>(206,9)*z[27] - z[60];
    z[94]=z[2]*z[94];
    z[113]=n<T>(10,3)*z[18];
    z[114]=6*z[4];
    z[115]= - z[114] + z[113] - n<T>(29,2)*z[12];
    z[115]=z[10]*z[115];
    z[115]=n<T>(671,9) + z[115];
    z[115]=z[10]*z[115];
    z[117]=n<T>(3,2) - z[42];
    z[117]=z[117]*z[29];
    z[94]=z[117] + z[115] - n<T>(95,2)*z[6] + z[94];
    z[94]=z[3]*z[94];
    z[115]= - n<T>(5,6)*z[116] + n<T>(13,2)*z[91] - n<T>(161,6)*z[4] - 26*z[12] + 17.
   /3.*z[18] - z[32];
    z[115]=z[10]*z[115];
    z[117]=2*z[10];
    z[118]=n<T>(161,9) - z[42];
    z[118]=z[118]*z[117];
    z[118]= - n<T>(295,18)*z[6] + z[118];
    z[118]=z[10]*z[118];
    z[120]=n<T>(313,18)*z[27];
    z[118]= - z[120] + z[118];
    z[118]=z[3]*z[118];
    z[121]=z[32] + z[4];
    z[122]= - z[12] + z[121];
    z[122]=z[122]*z[117];
    z[121]=z[7]*z[121];
    z[121]=z[122] + z[121];
    z[121]=z[7]*z[121];
    z[81]=z[16] - z[81];
    z[81]=2*z[81] + z[4];
    z[81]=z[10]*z[81];
    z[81]=n<T>(322,9) + z[81];
    z[81]=z[10]*z[81];
    z[81]=z[121] + z[118] - n<T>(313,18)*z[6] + z[81];
    z[81]=z[5]*z[81];
    z[118]=z[30]*z[27];
    z[121]= - z[11]*z[64];
    z[121]=static_cast<T>(1)+ z[121];
    z[121]=z[121]*z[64];
    z[121]=z[121] - z[54];
    z[121]=z[2]*z[121];
    z[122]=z[6]*z[9];
    z[59]=z[81] + z[59] + z[94] + z[115] - n<T>(11,2)*z[118] + z[121] - n<T>(38,3) - z[122];
    z[59]=z[5]*z[59];
    z[81]= - z[31] + 14*z[30];
    z[81]=z[10]*z[81];
    z[94]=7*z[12];
    z[81]=z[81] - n<T>(25,2)*z[91] - n<T>(185,9)*z[4] + 5*z[18] - z[94];
    z[81]=z[10]*z[81];
    z[115]=n<T>(5,3) + 9*z[42];
    z[46]=z[115]*z[46];
    z[115]=z[6] - z[54];
    z[46]=n<T>(7,3)*z[115] + z[46];
    z[46]=z[3]*z[46];
    z[115]=8*z[6] - n<T>(1,2)*z[54];
    z[115]=z[2]*z[115];
    z[46]=z[46] + z[81] - z[118] + 5*z[115] + n<T>(199,18) - z[122];
    z[46]=z[3]*z[46];
    z[81]=7*z[18];
    z[115]=10*z[20];
    z[118]= - z[4] - z[94] - z[70] + z[81] - z[115];
    z[118]=z[118]*z[116];
    z[91]=z[115]*z[91];
    z[121]=n<T>(1,3)*z[31];
    z[122]=6*z[21];
    z[91]=z[118] + z[91] - z[122] + z[121];
    z[91]=z[10]*z[91];
    z[118]=z[81] + z[115];
    z[118]=n<T>(1,3)*z[118];
    z[64]=z[64]*z[34];
    z[123]=z[6]*z[47];
    z[123]= - z[35] + z[123];
    z[123]=z[6]*z[123];
    z[123]= - 6*z[66] + n<T>(229,9) + 5*z[123];
    z[123]=z[2]*z[123];
    z[124]=6*z[11];
    z[125]=z[85] - z[124] + n<T>(13,2)*z[9];
    z[125]=z[125]*z[27];
    z[126]=n<T>(7,2)*z[54];
    z[125]=z[125] + z[126];
    z[125]=z[125]*z[30];
    z[127]=8*z[16];
    z[44]=z[59] + z[44] + z[46] + z[91] + z[125] + z[123] - z[64] - n<T>(125,2)*z[4] + n<T>(19,3)*z[12] - n<T>(427,9)*z[14] - 18*z[9] + z[127] - z[118]
    - z[52];
    z[44]=z[5]*z[44];
    z[46]= - z[69] - z[61];
    z[46]=z[14]*z[46];
    z[59]= - z[17] - n<T>(5,3)*z[13];
    z[59]=z[13]*z[59];
    z[91]= - z[51] + n<T>(73,3)*z[12] - z[9] - z[13] - n<T>(3,2)*z[14];
    z[91]=z[4]*z[91];
    z[123]= - z[2] + 20*z[4] - n<T>(17,2)*z[12] - z[32] - n<T>(19,2)*z[13];
    z[123]=z[2]*z[123];
    z[46]=z[123] + z[91] + z[96] + z[46] + z[122] + z[59];
    z[46]=z[46]*z[30];
    z[59]= - z[4]*z[100];
    z[83]= - z[49] + 5*z[83];
    z[91]=5*z[99];
    z[83]=n<T>(1,2)*z[83] + z[91];
    z[83]=z[2]*z[83];
    z[59]=z[59] + z[83];
    z[59]=z[2]*z[59];
    z[83]=z[49]*z[106];
    z[83]=z[83] - z[75];
    z[83]=z[83]*z[51];
    z[46]=z[46] + z[83] + z[59];
    z[46]=z[7]*z[46];
    z[59]=4*z[39];
    z[83]=z[13]*z[15];
    z[100]=z[59] + z[63] + n<T>(10,3)*z[83];
    z[100]=z[13]*z[100];
    z[59]= - z[59] - n<T>(35,6)*z[41];
    z[59]=z[4]*z[59];
    z[48]= - z[48] - z[101];
    z[48]=z[6]*z[48];
    z[101]= - z[66]*z[84];
    z[123]=z[21]*z[15];
    z[48]=z[101] + z[48] + z[59] - 10*z[123] + z[100];
    z[48]=z[48]*z[30];
    z[59]=z[61] + z[114];
    z[59]=z[2]*z[59];
    z[100]=z[84] - z[12];
    z[100]=z[12]*z[100];
    z[101]=4*z[4];
    z[114]= - n<T>(101,6)*z[12] + z[101];
    z[114]=z[4]*z[114];
    z[59]=z[59] + z[114] - 4*z[49] + z[100];
    z[59]=z[2]*z[59];
    z[100]=z[21] - n<T>(4,3)*z[67];
    z[114]= - z[9] - z[69];
    z[114]=z[4]*z[114];
    z[125]= - n<T>(3,2)*z[4] + n<T>(109,6)*z[12] + z[32] + n<T>(11,3)*z[13];
    z[125]=z[2]*z[125];
    z[100]=z[125] + z[114] + 2*z[100] - z[96];
    z[100]=z[100]*z[116];
    z[114]=z[14]*z[9];
    z[125]= - z[13]*z[9];
    z[125]=z[125] + z[114];
    z[105]=9*z[12] + z[105] - z[52];
    z[105]=z[4]*z[105];
    z[128]= - z[12] + z[17] - 13*z[13];
    z[128]=z[12]*z[128];
    z[105]=z[105] + 7*z[125] + z[128];
    z[105]=z[4]*z[105];
    z[125]= - z[34] + z[114];
    z[125]=z[14]*z[125];
    z[128]=z[12]*z[13]*z[17];
    z[46]=z[46] + z[100] + z[48] + z[59] + z[105] + 4*z[125] + z[128];
    z[46]=z[7]*z[46];
    z[48]=z[9] + n<T>(9,2)*z[14];
    z[48]=z[48]*z[84];
    z[48]=z[48] - z[40];
    z[48]=z[6]*z[48];
    z[55]= - z[14] + 3*z[55];
    z[55]=z[55]*z[90];
    z[59]=z[16] - n<T>(13,3)*z[9];
    z[48]=z[55] + z[48] - n<T>(86,3)*z[4] + z[68] + z[53] + 2*z[59] - z[65];
    z[48]=z[2]*z[48];
    z[55]=z[73] + 4*z[13];
    z[55]=z[13]*z[55];
    z[59]=z[19] + z[4];
    z[59]=z[4]*z[59];
    z[33]=z[59] + 3*z[33] + z[71] + z[55];
    z[33]=z[2]*z[33];
    z[55]=n<T>(1,3)*z[67];
    z[59]=z[15]*z[55];
    z[59]= - z[123] + z[59];
    z[65]=n<T>(5,3)*z[20];
    z[68]=z[41]*z[65];
    z[59]=2*z[59] + z[68];
    z[59]=z[59]*z[30];
    z[68]= - z[115] + z[94];
    z[68]=z[12]*z[68];
    z[71]= - z[4]*z[13];
    z[90]= - z[2]*z[93];
    z[68]=z[90] + n<T>(1,3)*z[68] + z[71];
    z[68]=z[68]*z[116];
    z[71]=z[17]*z[51];
    z[55]=z[55] + z[71];
    z[55]=z[4]*z[55];
    z[33]=z[68] + 2*z[59] + z[55] + z[33];
    z[33]=z[10]*z[33];
    z[55]= - z[51] + z[109] + n<T>(32,3)*z[13] + n<T>(19,3)*z[9] + z[124] + 3*
    z[19] + 5*z[17];
    z[55]=z[4]*z[55];
    z[59]= - z[14]*z[69];
    z[59]=z[59] + z[22] - n<T>(5,3)*z[67];
    z[59]=z[23]*z[59];
    z[68]=10*z[47];
    z[71]=8*z[9] - z[61];
    z[71]=z[14]*z[71];
    z[71]= - z[68] + z[71];
    z[71]=z[6]*z[71];
    z[71]= - z[57] + z[71];
    z[71]=z[6]*z[71];
    z[90]=z[14]*z[126];
    z[59]=z[90] + z[71] + z[59];
    z[30]=z[59]*z[30];
    z[59]=5*z[16];
    z[71]=z[18] + z[115];
    z[71]= - 36*z[9] + n<T>(2,3)*z[71] + z[59];
    z[71]=z[9]*z[71];
    z[90]=10*z[9];
    z[94]=6*z[63];
    z[100]= - n<T>(35,3) - z[94];
    z[100]=z[13]*z[100];
    z[100]=z[100] + 9*z[17] - z[90];
    z[100]=z[13]*z[100];
    z[105]=z[12] - z[14];
    z[109]=n<T>(10,3)*z[20];
    z[116]= - 6*z[13] - z[109] - z[9] - z[105];
    z[116]=z[12]*z[116];
    z[124]= - static_cast<T>(17)- 3*z[83];
    z[124]=z[13]*z[124];
    z[124]=z[124] - z[19] + 17*z[9];
    z[124]=z[14]*z[124];
    z[24]=z[24] + z[44] + z[46] + z[26] + z[33] + z[30] + z[48] + z[40]
    + z[55] + z[116] + z[124] + z[100] + z[71] - z[74] + z[22];
    z[24]=z[1]*z[24];
    z[26]=n<T>(11,2)*z[14];
    z[30]=z[77] + z[26];
    z[30]=z[14]*z[30];
    z[33]= - z[17] + z[72];
    z[33]=z[12]*z[33];
    z[44]=13*z[12] + z[17] - z[52];
    z[44]=z[4]*z[44];
    z[30]=z[44] + z[30] + z[33];
    z[30]=z[4]*z[30];
    z[33]=z[21] - z[96];
    z[44]=z[84] - z[89];
    z[44]=n<T>(1,2)*z[44] + z[101];
    z[44]=z[2]*z[44];
    z[46]= - n<T>(23,6)*z[12] + z[4];
    z[46]=z[4]*z[46];
    z[33]=z[44] + 2*z[33] + 5*z[46];
    z[33]=z[2]*z[33];
    z[44]=z[49]*z[62];
    z[44]=z[44] - z[75];
    z[44]=z[4]*z[44];
    z[46]= - n<T>(14,3)*z[31] - z[91];
    z[46]=z[4]*z[46];
    z[46]=z[46] + z[98];
    z[46]=z[2]*z[46];
    z[44]=z[44] + z[46];
    z[44]=z[7]*z[44];
    z[46]=z[14]*z[62];
    z[46]=z[67] + z[46];
    z[46]=z[14]*z[46];
    z[30]=z[44] + z[33] + z[30] + z[102] + z[46];
    z[30]=z[7]*z[30];
    z[33]=7*z[41];
    z[44]= - z[33] - z[39] - static_cast<T>(4)+ 5*z[38];
    z[44]=z[4]*z[44];
    z[46]=6*z[14];
    z[48]= - static_cast<T>(1)- z[39];
    z[48]=z[48]*z[46];
    z[52]=n<T>(8,3)*z[9] + z[17] + z[35];
    z[55]=n<T>(131,3) + z[41];
    z[55]=z[12]*z[55];
    z[44]=z[44] + z[55] + z[48] + 2*z[52] + n<T>(23,6)*z[13];
    z[44]=z[4]*z[44];
    z[28]=z[28] - z[36] + z[31];
    z[28]=z[28]*z[86];
    z[36]= - 2*z[67] + z[103];
    z[36]=z[4]*z[36];
    z[28]=z[36] + z[28];
    z[28]=z[10]*z[28];
    z[36]=z[9]*z[82];
    z[36]=8*z[21] + z[36];
    z[48]=z[94] + n<T>(19,3);
    z[52]= - z[13]*z[48];
    z[52]=z[58] + z[52];
    z[52]=z[13]*z[52];
    z[55]= - static_cast<T>(7)- z[83];
    z[55]=z[13]*z[55];
    z[55]=z[9] + z[55];
    z[55]=3*z[55] + z[26];
    z[55]=z[14]*z[55];
    z[58]= - z[95] + z[114];
    z[62]=z[6]*z[14];
    z[58]=z[58]*z[62];
    z[71]=4*z[16];
    z[72]=n<T>(13,2) - z[62];
    z[72]=z[2]*z[72];
    z[72]=z[72] + n<T>(31,18)*z[4] - n<T>(187,6)*z[12] + 9*z[14] - z[71] - n<T>(43,3)
   *z[13];
    z[72]=z[2]*z[72];
    z[28]=z[30] + z[28] + z[72] + 4*z[58] + z[44] + n<T>(4,3)*z[31] + z[55]
    + 2*z[36] + z[52];
    z[28]=z[7]*z[28];
    z[30]=z[22]*z[15];
    z[36]=z[30] + z[70];
    z[44]=5*z[13];
    z[52]= - static_cast<T>(3)- z[41];
    z[52]=z[12]*z[52];
    z[55]=z[10]*z[31];
    z[52]=z[55] + z[2] + z[52] - z[44] - z[73] - z[113] + z[36];
    z[52]=z[10]*z[52];
    z[32]=z[32] + z[17];
    z[30]= - z[30] - z[50] + z[87] - z[32];
    z[30]=z[15]*z[30];
    z[50]= - static_cast<T>(1)- 3*z[39];
    z[50]=z[50]*z[85];
    z[50]=z[50] + 4*z[11] + z[57];
    z[50]=z[6]*z[50];
    z[50]=z[50] + 11*z[39] + n<T>(10,3) - 13*z[38];
    z[50]=z[6]*z[50];
    z[55]= - z[25] - n<T>(10,3) + n<T>(3,2)*z[41];
    z[55]=z[10]*z[55];
    z[58]= - z[38] + z[39];
    z[58]=z[58]*z[27]*z[3];
    z[50]=z[58] + z[55] + n<T>(1,2)*z[15] + z[50];
    z[50]=z[3]*z[50];
    z[55]=z[35] + z[9];
    z[55]=2*z[55] + z[57];
    z[57]=z[68] + n<T>(13,2)*z[49];
    z[57]=z[6]*z[57];
    z[58]= - static_cast<T>(29)- 6*z[39];
    z[58]=z[14]*z[58];
    z[55]=z[57] + 2*z[55] + z[58];
    z[55]=z[6]*z[55];
    z[56]=z[56] + z[80] + n<T>(295,9)*z[14];
    z[56]=z[6]*z[56];
    z[56]=static_cast<T>(1)+ z[56];
    z[56]=z[6]*z[56];
    z[57]= - z[60]*z[85];
    z[56]=z[56] + z[57];
    z[56]=z[2]*z[56];
    z[57]=2*z[15];
    z[58]= - static_cast<T>(4)+ z[63];
    z[58]=z[58]*z[57];
    z[63]=z[43]*z[13];
    z[68]= - z[17]*z[63];
    z[58]=z[58] + z[68];
    z[58]=z[13]*z[58];
    z[68]= - 2*z[23] - z[63];
    z[68]=z[13]*z[68];
    z[72]=5*z[15];
    z[68]=z[72] + z[68];
    z[68]=z[14]*z[68];
    z[30]=z[50] + z[52] + z[56] + z[55] - n<T>(11,2)*z[41] + z[68] + z[58]
    + n<T>(17,2) + z[30];
    z[30]=z[3]*z[30];
    z[50]=n<T>(7,2)*z[78] + z[76];
    z[50]=z[7]*z[50];
    z[44]= - z[44] + n<T>(7,2)*z[14];
    z[44]=z[14]*z[44];
    z[52]=3*z[13];
    z[55]=z[2] + n<T>(152,9)*z[4] + z[16] - z[52];
    z[55]=z[2]*z[55];
    z[44]=z[50] + z[55] - n<T>(527,18)*z[97] + z[122] + z[44];
    z[44]=z[7]*z[44];
    z[34]=n<T>(7,2)*z[49] + z[74] + 28*z[34];
    z[34]=z[6]*z[34];
    z[49]=z[107] - n<T>(2,3)*z[31];
    z[49]=z[10]*z[49];
    z[50]=n<T>(277,18) + z[66];
    z[50]=z[2]*z[50];
    z[34]=z[44] + 4*z[49] + z[50] + z[34] - n<T>(925,18)*z[4] - n<T>(31,3)*z[12]
    - n<T>(259,9)*z[14] - z[52] + z[90] - z[127] - 24*z[11] - z[92] - z[17]
   ;
    z[34]=z[7]*z[34];
    z[44]= - z[82] + z[93];
    z[44]=z[10]*z[44];
    z[44]= - n<T>(953,18) + 5*z[44];
    z[44]=z[10]*z[44];
    z[44]=n<T>(1483,18)*z[6] + z[44];
    z[44]=z[10]*z[44];
    z[49]=n<T>(1,2)*z[42];
    z[50]= - static_cast<T>(2)+ z[49];
    z[50]=z[10]*z[50];
    z[50]=n<T>(3,2)*z[6] + z[50];
    z[29]=z[50]*z[29];
    z[29]=z[29] + z[44] - n<T>(725,18)*z[27] + z[60];
    z[29]=z[3]*z[29];
    z[44]=11*z[11];
    z[46]=z[64] - z[46] - z[44] + n<T>(63,2)*z[9];
    z[46]=z[6]*z[46];
    z[45]= - z[97] + 2*z[45];
    z[45]=z[7]*z[45];
    z[45]=2*z[45] + z[119] + n<T>(7,2)*z[105] - 13*z[4];
    z[45]=z[7]*z[45];
    z[50]= - 10*z[4] + 18*z[12] - n<T>(14,3)*z[18] + z[59];
    z[50]=z[10]*z[50];
    z[45]=z[45] + z[50] + 4*z[66] - static_cast<T>(15)+ z[46];
    z[45]=z[7]*z[45];
    z[46]= - n<T>(349,18) + z[42];
    z[46]=z[10]*z[46];
    z[46]=n<T>(322,9)*z[6] + z[46];
    z[46]=z[10]*z[46];
    z[46]= - z[120] + z[46];
    z[46]=z[3]*z[46];
    z[25]= - n<T>(367,18) + z[25];
    z[25]=z[10]*z[25];
    z[25]=z[46] + n<T>(331,18)*z[6] + z[25];
    z[25]=z[10]*z[25];
    z[46]=z[27]*z[106];
    z[50]= - static_cast<T>(1)+ z[42];
    z[50]=z[10]*z[50];
    z[46]=z[46] + z[50];
    z[46]=z[7]*z[46];
    z[25]=z[46] - z[108] + z[25];
    z[25]=z[5]*z[25];
    z[46]= - z[111] + n<T>(43,2)*z[12] - n<T>(19,3)*z[18] + z[59];
    z[46]=z[10]*z[46];
    z[46]= - n<T>(37,3) + z[46];
    z[46]=z[10]*z[46];
    z[50]=9*z[11];
    z[55]=z[50] - z[9];
    z[55]=z[6]*z[55];
    z[55]= - n<T>(61,2) + z[55];
    z[55]=z[6]*z[55];
    z[25]=z[25] + z[45] + z[29] + z[46] + z[55] + z[126];
    z[25]=z[5]*z[25];
    z[29]=5*z[20];
    z[45]= - z[92] + z[29];
    z[46]=z[107] - z[121];
    z[46]=z[46]*z[117];
    z[55]=n<T>(7,3)*z[12];
    z[32]=z[46] - n<T>(503,18)*z[4] - z[55] + 2*z[45] - z[32];
    z[32]=z[10]*z[32];
    z[45]=n<T>(152,9)*z[27] - z[60];
    z[45]=z[45]*z[86];
    z[31]=z[31]*z[117];
    z[46]= - z[18] + z[12];
    z[31]=5*z[46] + z[31];
    z[31]=z[10]*z[31];
    z[31]= - n<T>(287,9) + z[31];
    z[31]=z[10]*z[31];
    z[46]= - n<T>(2,3) - z[49];
    z[46]=z[10]*z[46];
    z[46]=n<T>(5,6)*z[6] + z[46];
    z[46]=z[10]*z[46];
    z[46]=n<T>(1,3)*z[27] + z[46];
    z[46]=z[3]*z[46];
    z[31]=7*z[46] + z[31] + n<T>(611,18)*z[6] + z[45];
    z[31]=z[3]*z[31];
    z[45]= - z[65] - z[50];
    z[45]= - z[112] - n<T>(385,9)*z[14] + 2*z[45] - n<T>(23,2)*z[9];
    z[45]=z[6]*z[45];
    z[46]= - z[80] - z[106];
    z[46]=z[6]*z[46];
    z[46]=n<T>(475,18) + z[46];
    z[46]=z[6]*z[46];
    z[49]=n<T>(3,2)*z[54];
    z[46]=z[46] + z[49];
    z[46]=z[2]*z[46];
    z[25]=z[25] + z[34] + z[31] + z[32] + z[46] + n<T>(539,18) + z[45];
    z[25]=z[5]*z[25];
    z[31]= - z[9]*z[65];
    z[31]=z[21] + z[31];
    z[32]=z[29] - z[12];
    z[32]=z[12]*z[32];
    z[31]=n<T>(2,3)*z[32] + 2*z[31] + 3*z[67];
    z[32]= - n<T>(109,18)*z[4] - z[93] - z[52] - z[70] + n<T>(17,3)*z[9];
    z[32]=z[2]*z[32];
    z[34]= - z[10]*z[67]*z[51];
    z[45]=z[17] + n<T>(41,6)*z[13];
    z[45]=z[4]*z[45];
    z[31]=z[34] + z[32] + 2*z[31] + z[45];
    z[31]=z[10]*z[31];
    z[32]= - n<T>(10,3) + z[38];
    z[32]= - n<T>(29,2)*z[41] + 8*z[32] - 5*z[39];
    z[32]=z[4]*z[32];
    z[34]=z[9]*z[20];
    z[34]= - z[47] + n<T>(2,3)*z[34];
    z[26]=16*z[9] + z[26];
    z[26]=z[14]*z[26];
    z[26]=z[40] + 10*z[34] + z[26];
    z[26]=z[6]*z[26];
    z[22]=3*z[47] - z[22];
    z[22]=z[22]*z[72];
    z[34]=z[48]*z[83];
    z[34]=z[34] + n<T>(65,2) - z[94];
    z[34]=z[13]*z[34];
    z[38]=6*z[15] + z[110];
    z[38]=z[13]*z[38];
    z[38]= - 2*z[39] - n<T>(19,2) + z[38];
    z[38]=z[38]*z[84];
    z[40]= - z[9] - z[61];
    z[40]=z[40]*z[62];
    z[40]=z[40] - n<T>(20,3)*z[9] + z[61];
    z[40]=z[6]*z[40];
    z[45]=z[14]*z[49];
    z[40]=z[45] - n<T>(143,9) + z[40];
    z[40]=z[2]*z[40];
    z[45]=n<T>(20,3)*z[20];
    z[46]=static_cast<T>(34)+ z[41];
    z[46]=z[12]*z[46];
    z[22]=z[24] + z[25] + z[28] + z[30] + z[31] + z[40] + z[26] + z[32]
    + z[46] + z[38] + z[34] + z[22] + z[90] - 10*z[16] + 31*z[11] + 
    z[17] + z[45] + z[19] + 4*z[18];
    z[22]=z[1]*z[22];
    z[24]=z[13] + z[16];
    z[25]=z[79] - z[55] - z[24];
    z[25]=z[2]*z[25];
    z[21]=z[67] - z[21];
    z[26]=n<T>(7,3)*z[99] - z[21];
    z[28]= - z[55]*z[88];
    z[25]=z[28] + 2*z[26] + z[25];
    z[25]=z[8]*z[25];
    z[26]= - z[21]*z[104]*z[86];
    z[25]=z[26] + z[25];
    z[25]=z[7]*z[25];
    z[24]= - n<T>(4,3)*z[4] - 4*z[24] + z[55];
    z[24]=z[2]*z[24];
    z[21]=6*z[21] + z[24];
    z[21]=z[21]*z[104];
    z[24]= - static_cast<T>(1)+ z[83];
    z[24]=z[13]*z[24];
    z[24]=z[24] - z[16] - z[123];
    z[24]=3*z[24] + z[55];
    z[26]= - static_cast<T>(11)- z[33];
    z[26]=z[4]*z[26];
    z[24]=2*z[24] + n<T>(1,3)*z[26];
    z[24]=z[8]*z[24];
    z[21]=z[25] + z[24] + z[21];
    z[21]=z[7]*z[21];
    z[24]=z[127] + 3*z[123];
    z[24]=z[24]*z[57];
    z[25]=8*z[15] - 3*z[110];
    z[25]=z[25]*z[69];
    z[26]=n<T>(7,3)*z[41];
    z[24]= - z[26] + z[25] + static_cast<T>(1)+ z[24];
    z[24]=z[8]*z[24];
    z[25]= - z[15]*z[69];
    z[25]=static_cast<T>(3)+ z[25];
    z[25]=z[13]*z[25];
    z[25]=z[25] + z[36];
    z[28]= - n<T>(7,3)*z[42] + 1;
    z[28]=z[2]*z[28];
    z[25]=3*z[25] - n<T>(14,3)*z[12] + z[28];
    z[25]=z[25]*z[104];
    z[28]=11*z[16];
    z[21]=z[21] + z[25] + z[24] - z[2] - n<T>(70,3)*z[4] + n<T>(68,3)*z[12] - 
    z[84] + n<T>(49,3)*z[13] - z[9] + z[35] - z[28];
    z[21]=z[7]*z[21];
    z[24]= - z[59] - z[123];
    z[24]=z[24]*z[57];
    z[24]= - n<T>(7,3) + z[24];
    z[24]=z[15]*z[24];
    z[23]= - 5*z[23] + z[63];
    z[23]=z[23]*z[69];
    z[25]=z[6] - z[15];
    z[25]=z[109]*z[25];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[6]*z[25];
    z[23]=z[25] + z[24] + z[23];
    z[23]=z[8]*z[23];
    z[24]= - z[123] + z[109] - z[70];
    z[24]=z[24]*z[57];
    z[25]= - 3*z[15] + z[110];
    z[25]=z[25]*z[69];
    z[30]= - z[6]*z[45];
    z[24]=z[30] + z[26] + z[25] - n<T>(17,3) + z[24];
    z[24]=z[8]*z[24];
    z[25]=z[81] - z[29];
    z[25]=6*z[2] - 11*z[12] - n<T>(7,3)*z[13] + n<T>(1,3)*z[25] - z[71];
    z[26]= - z[15]*z[29];
    z[26]=static_cast<T>(7)+ z[26];
    z[26]=z[12]*z[26];
    z[26]=z[29] + z[26];
    z[26]=z[26]*z[104];
    z[24]=n<T>(2,3)*z[26] + 2*z[25] + z[24];
    z[24]=z[10]*z[24];
    z[25]=z[16]*z[43];
    z[26]=z[15] - n<T>(10,3)*z[6];
    z[26]=z[6]*z[26];
    z[25]=z[26] + z[25] + z[63];
    z[25]=z[8]*z[25];
    z[26]=z[15]*z[16];
    z[26]= - static_cast<T>(2)+ z[26];
    z[26]=z[15]*z[26];
    z[26]=n<T>(20,3)*z[6] + z[26] + z[110];
    z[26]=z[8]*z[26];
    z[29]= - n<T>(10,3) + z[41];
    z[29]=z[29]*z[104];
    z[26]=z[29] + static_cast<T>(6)+ z[26];
    z[26]=z[10]*z[26];
    z[29]=11*z[15] - 13*z[6];
    z[25]=z[26] + n<T>(1,2)*z[29] + z[25];
    z[25]=z[3]*z[25];
    z[26]=z[28] - z[118] + z[35];
    z[26]=z[15]*z[26];
    z[28]= - z[53] - n<T>(5,2)*z[9] + z[45] + z[44];
    z[28]=z[6]*z[28];
    z[27]=z[27]*z[8];
    z[29]=z[115]*z[27];
    z[30]=z[37]*z[8];
    z[30]=z[30] - z[104];
    z[30]=z[30]*z[10];
    z[31]= - z[20]*z[30];
    z[29]=10*z[31] + static_cast<T>(13)+ z[29];
    z[27]= - z[27] + z[30];
    z[27]=z[3]*z[27];
    z[27]=n<T>(10,3)*z[27] + n<T>(1,3)*z[29];
    z[27]=z[10]*z[27];
    z[29]=z[7] - z[6];
    z[27]=z[27] - n<T>(7,2)*z[29];
    z[27]=z[5]*z[27];

    r += n<T>(19,2) + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[27]
       + z[28] - z[39] - n<T>(23,6)*z[41] + z[66] - n<T>(35,6)*z[83];
 
    return r;
}

template double qg_2lNLC_r158(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r158(const std::array<dd_real,31>&);
#endif
