#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r61(const std::array<T,31>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[14];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[12];
    z[10]=k[13];
    z[11]=k[15];
    z[12]=k[9];
    z[13]=k[2];
    z[14]=z[10]*z[12];
    z[15]=z[14]*z[7];
    z[16]=z[5]*z[12];
    z[17]=z[13]*z[16];
    z[17]=z[12] + z[15] + z[17];
    z[18]=n<T>(1,4)*z[11];
    z[19]=z[5]*z[13];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[19]*z[18];
    z[20]=z[11]*z[7];
    z[21]=z[7]*z[10];
    z[22]=n<T>(1,2)*z[20] + static_cast<T>(1)+ n<T>(3,2)*z[21];
    z[22]=z[9]*z[22];
    z[23]=z[9]*z[7];
    z[24]=z[23] + 1;
    z[25]= - z[10]*z[2];
    z[25]=z[25] - z[24];
    z[25]=z[4]*z[25];
    z[26]=z[7] - z[2];
    z[26]= - z[7]*z[26];
    z[27]= - z[9]*npow(z[7],3);
    z[26]=z[26] + z[27];
    z[26]=z[4]*z[26];
    z[27]=z[9]*npow(z[7],2);
    z[26]=z[27] + z[26];
    z[26]=z[8]*z[26];
    z[27]=z[27] + z[7];
    z[28]=z[2] - z[27];
    z[28]=z[4]*z[28];
    z[26]=z[26] - static_cast<T>(1)+ z[28];
    z[26]=z[8]*z[26];
    z[17]=n<T>(3,16)*z[26] + z[25] + n<T>(1,2)*z[22] + z[19] + z[10] + n<T>(1,4)*
    z[17];
    z[17]=z[8]*z[17];
    z[19]=z[11]*z[5];
    z[22]=z[19] + z[14] + z[16];
    z[25]=n<T>(1,2)*z[5];
    z[26]=5*z[10] - z[25];
    z[26]=n<T>(1,2)*z[26] + z[11];
    z[26]=z[9]*z[26];
    z[22]=n<T>(1,2)*z[22] + z[26];
    z[26]= - n<T>(11,2)*z[4] + z[11];
    z[24]=z[24]*z[26];
    z[26]= - z[4]*z[27];
    z[26]=z[26] + n<T>(1,2) + z[23];
    z[26]=z[8]*z[26];
    z[24]=3*z[26] + z[24];
    z[24]=z[8]*z[24];
    z[26]=z[10] - z[9];
    z[26]=z[4]*z[26];
    z[22]=n<T>(1,8)*z[24] + n<T>(1,2)*z[22] + 2*z[26];
    z[22]=z[8]*z[22];
    z[24]=z[14]*z[25];
    z[26]=z[10]*z[25];
    z[26]=z[26] + z[19];
    z[26]=z[9]*z[26];
    z[24]=z[24] + z[26];
    z[26]=z[4]*z[9];
    z[18]=z[9]*z[18];
    z[23]= - z[4]*z[23];
    z[23]=z[9] + z[23];
    z[23]=z[8]*z[23];
    z[18]=n<T>(3,8)*z[23] + z[18] - z[26];
    z[18]=z[8]*z[18];
    z[23]=z[10]*z[26];
    z[18]=n<T>(1,2)*z[18] + n<T>(1,4)*z[24] + z[23];
    z[18]=z[8]*z[18];
    z[23]=z[5]*z[14];
    z[16]=z[11]*z[16];
    z[16]=z[23] + z[16];
    z[16]=z[9]*z[16];
    z[23]=npow(z[6],2);
    z[24]=z[6]*z[2];
    z[26]=static_cast<T>(1)+ z[24];
    z[26]=z[5]*z[26]*z[23];
    z[27]=npow(z[6],3);
    z[26]=z[27] + z[26];
    z[19]= - z[6]*z[19];
    z[19]=n<T>(1,2)*z[26] + z[19];
    z[19]=z[4]*z[19];
    z[16]=z[16] + 3*z[19];
    z[16]=n<T>(1,8)*z[16] + z[18];
    z[16]=z[1]*z[16];
    z[18]=n<T>(3,4)*z[23];
    z[19]=z[12] - n<T>(3,2)*z[6];
    z[19]=z[5]*z[19];
    z[19]= - z[18] + z[19];
    z[19]=z[11]*z[19];
    z[25]=z[12] - z[25];
    z[25]=z[11]*z[25];
    z[14]=z[14] + z[25];
    z[14]=z[9]*z[14];
    z[14]=z[14] + n<T>(3,2)*z[27] + z[19];
    z[19]=z[5]*z[6];
    z[24]= - static_cast<T>(1)- n<T>(1,2)*z[24];
    z[24]=z[24]*z[19];
    z[25]= - z[6] + z[5];
    z[25]=z[11]*z[25];
    z[24]=z[24] + z[25];
    z[25]=z[9]*z[10];
    z[26]=z[4]*z[11];
    z[24]=n<T>(3,16)*z[26] + n<T>(3,4)*z[24] + z[25];
    z[24]=z[4]*z[24];
    z[14]=z[16] + z[22] + n<T>(1,4)*z[14] + z[24];
    z[14]=z[1]*z[14];
    z[16]=z[6]*z[13];
    z[22]= - static_cast<T>(1)- z[16];
    z[18]=z[7]*z[22]*z[18];
    z[22]=z[12]*z[13];
    z[22]= - n<T>(3,4)*z[16] + n<T>(3,2) + z[22];
    z[22]=z[5]*z[22];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[6]*z[16];
    z[16]=z[22] + z[18] + z[12] + n<T>(3,4)*z[16];
    z[16]=z[11]*z[16];
    z[18]= - n<T>(1,2)*z[23] - z[19];
    z[20]=z[12]*z[20];
    z[15]=z[15] + z[20];
    z[15]=z[9]*z[15];
    z[15]=z[15] + n<T>(3,2)*z[18] + z[16];
    z[16]= - z[7]*z[6];
    z[18]= - z[5]*z[2];
    z[16]=z[18] + n<T>(3,2) + z[16];
    z[16]=z[11]*z[16];
    z[18]=z[2]*z[19];
    z[16]=z[16] - z[6] + z[18];
    z[18]=static_cast<T>(1)+ z[21];
    z[18]=z[9]*z[18];
    z[19]=z[2]*z[26];
    z[16]= - n<T>(3,8)*z[19] + n<T>(3,4)*z[16] + z[18];
    z[16]=z[4]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[14]=z[14] + n<T>(1,2)*z[15] + z[17];

    r += z[14]*npow(z[3],2)*npow(z[1],2);
 
    return r;
}

template double qg_2lNLC_r61(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r61(const std::array<dd_real,31>&);
#endif
