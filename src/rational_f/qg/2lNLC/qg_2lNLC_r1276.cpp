#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1276(const std::array<T,31>& k) {
  T z[81];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[13];
    z[5]=k[15];
    z[6]=k[3];
    z[7]=k[6];
    z[8]=k[10];
    z[9]=k[4];
    z[10]=k[29];
    z[11]=k[5];
    z[12]=k[12];
    z[13]=k[14];
    z[14]=k[2];
    z[15]=k[17];
    z[16]=k[8];
    z[17]=k[26];
    z[18]=k[22];
    z[19]=npow(z[6],3);
    z[20]=npow(z[7],3);
    z[21]=z[4]*z[19]*z[20];
    z[22]=z[20]*z[8];
    z[23]=z[19]*z[22];
    z[23]=13*z[23] - 20*z[21];
    z[24]=n<T>(1,3)*z[4];
    z[23]=z[23]*z[24];
    z[25]=npow(z[14],2);
    z[26]=2*z[6];
    z[27]=z[14] - z[26];
    z[27]=z[6]*z[27];
    z[27]= - z[25] + z[27];
    z[27]=z[6]*z[27]*z[22];
    z[21]=z[27] - 2*z[21];
    z[21]=z[3]*z[21];
    z[27]=3*z[12];
    z[28]=z[27]*z[15];
    z[29]=z[13]*z[12];
    z[30]= - z[28] + 2*z[29];
    z[31]=9*z[15];
    z[32]=3*z[4];
    z[33]= - n<T>(5,3)*z[5] + z[32] + z[31] - n<T>(17,3)*z[13];
    z[33]=z[2]*z[33];
    z[34]=9*z[12];
    z[35]=z[34] - z[4];
    z[35]=z[5]*z[35];
    z[21]=n<T>(2,3)*z[21] + z[33] + z[35] + 3*z[30] + z[23];
    z[21]=z[3]*z[21];
    z[23]=z[20]*z[12];
    z[30]=n<T>(2,3)*z[23];
    z[33]=npow(z[6],2);
    z[35]=z[33]*z[30];
    z[36]= - z[12]*z[31];
    z[37]=z[27] - 2*z[4];
    z[37]=z[5]*z[37];
    z[38]=3*z[15];
    z[39]= - n<T>(11,3)*z[5] + z[38] - n<T>(4,3)*z[4];
    z[39]=z[2]*z[39];
    z[35]=z[39] + 3*z[37] + 6*z[29] + z[36] + z[35];
    z[35]=z[3]*z[35];
    z[36]=npow(z[4],2);
    z[37]=z[33]*z[20]*z[36];
    z[39]=npow(z[5],2);
    z[39]=2*z[39];
    z[40]=npow(z[13],2);
    z[41]= - 5*z[40] + z[39];
    z[41]=z[2]*z[41];
    z[37]=5*z[37] + z[41];
    z[35]=n<T>(4,3)*z[37] + z[35];
    z[35]=z[3]*z[35];
    z[37]=n<T>(20,3)*z[40];
    z[41]=z[37]*z[20]*z[14];
    z[42]=z[5]*z[12];
    z[43]= - z[12]*z[15];
    z[43]=z[43] + z[42];
    z[43]=z[3]*z[43];
    z[30]=z[6]*z[30];
    z[30]=z[30] + 3*z[43];
    z[30]=z[3]*z[30];
    z[30]= - z[41] + z[30];
    z[30]=z[3]*z[30];
    z[43]= - z[20]*z[29];
    z[44]= - z[13]*z[20];
    z[23]=z[23] + z[44];
    z[23]=z[3]*z[23];
    z[23]=z[43] + n<T>(4,3)*z[23];
    z[23]=z[3]*z[23];
    z[43]=z[16] + z[12];
    z[43]=z[17]*z[43];
    z[44]=z[2]*z[43]*z[20];
    z[23]= - n<T>(10,3)*z[44] + z[23];
    z[23]=z[11]*z[23];
    z[41]= - z[5]*z[41];
    z[23]=z[23] + z[41] + z[30];
    z[23]=z[11]*z[23];
    z[30]=z[36]*z[29];
    z[41]=z[5]*z[4];
    z[44]= - z[40]*z[41];
    z[44]= - 2*z[30] + z[44];
    z[45]=npow(z[9],2);
    z[46]=z[45]*z[20];
    z[39]= - z[2]*z[46]*z[39];
    z[39]=5*z[44] + z[39];
    z[23]=z[23] + n<T>(4,3)*z[39] + z[35];
    z[23]=z[11]*z[23];
    z[35]=npow(z[10],2);
    z[39]= - z[5]*z[10];
    z[39]= - z[35] + z[39];
    z[39]=z[5]*z[39];
    z[44]=z[35]*z[8];
    z[39]= - z[44] + z[39];
    z[47]=2*z[2];
    z[48]=z[9]*z[7];
    z[39]=z[47]*z[39]*npow(z[48],4);
    z[47]=npow(z[3],2);
    z[48]=z[47]*z[29];
    z[49]=z[11]*z[7];
    z[49]=z[48]*npow(z[49],4);
    z[50]=z[41]*z[2];
    z[47]=z[1]*z[47]*z[50];
    z[39]=z[47] - z[49] - 5*z[30] + z[39];
    z[47]=z[7]*z[6];
    z[47]=z[4]*z[8]*npow(z[47],4);
    z[49]=z[5] + z[4];
    z[51]= - z[2]*z[49];
    z[47]=z[51] + z[47] + z[41];
    z[47]=z[3]*z[47];
    z[47]= - 13*z[50] + 4*z[47];
    z[47]=z[3]*z[47];
    z[39]=z[47] + 4*z[39];
    z[39]=z[1]*z[39];
    z[47]=n<T>(26,3)*z[8];
    z[50]=5*z[12];
    z[51]=n<T>(20,3)*z[13];
    z[52]= - z[51] - z[47] + z[50];
    z[52]=z[13]*z[52];
    z[53]=z[9]*z[8];
    z[37]=z[37]*z[53];
    z[54]=n<T>(20,3)*z[4];
    z[55]=z[12] - z[13];
    z[55]=z[55]*z[54];
    z[37]=z[55] + z[52] + z[37];
    z[37]=z[4]*z[37];
    z[52]=z[9] - z[14];
    z[52]=z[52]*z[9];
    z[55]=n<T>(1,3)*z[25];
    z[52]=z[52] + z[55];
    z[56]=z[52]*z[35]*z[20];
    z[57]=z[9]*z[10];
    z[58]=z[10]*z[14];
    z[59]= - z[58] + z[57];
    z[59]=z[9]*z[59];
    z[55]=z[10]*z[55];
    z[55]=z[55] + z[59];
    z[59]=4*z[5];
    z[55]=z[59]*z[20]*z[55];
    z[55]=z[55] + 4*z[56];
    z[55]=z[9]*z[55];
    z[56]=z[4]*z[13];
    z[55]= - z[56] + n<T>(5,3)*z[43] + z[55];
    z[60]=2*z[5];
    z[55]=z[55]*z[60];
    z[61]=z[38] - n<T>(11,3)*z[8];
    z[61]=z[61]*z[40];
    z[22]=z[10]*z[45]*z[22];
    z[22]=n<T>(7,3)*z[22] + n<T>(8,3)*z[44] + z[61];
    z[22]=z[9]*z[22];
    z[61]=7*z[10];
    z[62]=z[46]*z[61];
    z[63]=8*z[35];
    z[62]=z[63] + z[62];
    z[62]=z[9]*z[62];
    z[62]= - 16*z[10] + z[62];
    z[46]=z[10] - 2*z[46];
    z[46]=z[9]*z[46];
    z[46]=static_cast<T>(2)+ z[46];
    z[64]=n<T>(8,3)*z[5];
    z[46]=z[46]*z[64];
    z[46]=z[46] + n<T>(1,3)*z[62] + z[32];
    z[46]=z[5]*z[46];
    z[62]=n<T>(40,3)*z[13];
    z[65]= - z[62] + z[31] + n<T>(10,3)*z[8];
    z[65]=z[13]*z[65];
    z[66]=z[8]*z[10];
    z[22]=z[46] + z[22] - n<T>(16,3)*z[66] + z[65];
    z[22]=z[2]*z[22];
    z[20]=z[9]*z[52]*z[44]*z[20];
    z[46]= - z[29]*z[38];
    z[20]=n<T>(1,3)*z[39] + z[23] + z[21] + z[22] + z[55] + z[37] + z[46] + 
   8*z[20];
    z[20]=z[1]*z[20];
    z[21]=z[18]*z[25];
    z[21]=z[21] + z[26];
    z[22]=3*z[8];
    z[21]=z[21]*z[22];
    z[23]=npow(z[7],2);
    z[37]=z[23]*z[6];
    z[39]=n<T>(2,3)*z[37];
    z[46]=z[14] + z[6];
    z[46]=z[46]*z[39];
    z[52]=6*z[4];
    z[55]=z[33]*z[8];
    z[65]= - z[55]*z[52];
    z[67]=3*z[18];
    z[68]=z[67]*z[14];
    z[69]=n<T>(1,3) - z[68];
    z[21]=z[65] + z[46] + 2*z[69] + z[21];
    z[21]=z[3]*z[21];
    z[46]=8*z[14] - 17*z[6];
    z[46]=z[6]*z[46];
    z[46]= - 8*z[25] + z[46];
    z[65]=n<T>(1,3)*z[8];
    z[46]=z[46]*z[65];
    z[69]=2*z[12];
    z[70]=z[33]*z[69];
    z[46]=z[46] + z[70];
    z[46]=z[46]*z[23];
    z[70]=n<T>(5,3)*z[16];
    z[71]=z[70] - z[67];
    z[72]=z[33]*z[23];
    z[73]= - z[6]*z[54];
    z[73]=z[73] + n<T>(7,3) + 8*z[72];
    z[73]=z[4]*z[73];
    z[74]=6*z[12];
    z[75]=8*z[5];
    z[76]=z[8]*z[68];
    z[21]=z[21] - n<T>(2,3)*z[2] - z[75] + z[73] + n<T>(16,3)*z[13] + z[46] - 
    z[74] + 2*z[71] + z[76];
    z[21]=z[3]*z[21];
    z[46]=z[23]*z[14];
    z[34]= - z[62] + z[34] + n<T>(35,3)*z[46];
    z[34]=z[13]*z[34];
    z[62]=z[64] - z[50] + z[52];
    z[62]=z[5]*z[62];
    z[71]=n<T>(10,3)*z[16];
    z[73]=n<T>(14,3)*z[5] + 4*z[4] + z[67] - z[71] - z[38];
    z[73]=z[2]*z[73];
    z[76]= - z[69] - z[38] + z[18];
    z[77]=n<T>(4,3)*z[2];
    z[39]= - z[77] + n<T>(20,3)*z[5] + n<T>(22,3)*z[4] + 12*z[13] + 3*z[76] + 
    z[39];
    z[39]=z[3]*z[39];
    z[76]= - z[37] - 20*z[4];
    z[76]=z[76]*z[24];
    z[78]= - z[15]*z[74];
    z[79]=z[12]*z[37];
    z[34]=z[39] + z[73] + z[62] + z[76] + z[34] + z[78] + n<T>(1,3)*z[79];
    z[34]=z[3]*z[34];
    z[39]=z[13] - z[49];
    z[39]=z[39]*z[77];
    z[39]=z[39] - n<T>(7,3)*z[42] + n<T>(32,3)*z[29] - z[28] + n<T>(4,3)*z[23];
    z[39]=z[3]*z[39];
    z[49]=5*z[13];
    z[62]=z[27] - z[49];
    z[62]=z[23]*z[62];
    z[73]= - z[40] + z[36];
    z[73]=z[2]*z[73];
    z[39]=z[39] + n<T>(20,3)*z[73] + z[62];
    z[39]=z[3]*z[39];
    z[62]= - 10*z[16] - 17*z[12];
    z[62]= - n<T>(4,3)*z[5] + n<T>(1,3)*z[62];
    z[62]=z[23]*z[62];
    z[73]= - z[67] + n<T>(11,3)*z[12];
    z[36]=z[73]*z[36];
    z[36]=z[36] + z[62];
    z[36]=z[2]*z[36];
    z[62]=3*z[29];
    z[73]= - z[23]*z[62];
    z[76]=z[13]*z[23];
    z[78]=z[4]*z[40];
    z[76]=z[76] - n<T>(5,3)*z[78];
    z[59]=z[76]*z[59];
    z[30]=z[39] + z[36] + z[59] + z[73] - n<T>(20,3)*z[30];
    z[30]=z[11]*z[30];
    z[36]=z[12] + z[18];
    z[36]= - z[51] + 3*z[36];
    z[39]=z[23]*z[9];
    z[59]=n<T>(20,3)*z[39] + n<T>(40,3)*z[37] + z[36];
    z[59]=z[4]*z[59];
    z[73]=4*z[13];
    z[76]=z[27] - n<T>(5,3)*z[13];
    z[76]=z[76]*z[73];
    z[59]=z[76] + z[59];
    z[59]=z[4]*z[59];
    z[76]=7*z[46] + z[73];
    z[76]=z[13]*z[76];
    z[76]=4*z[43] + z[76];
    z[78]=z[75]*z[39];
    z[76]=z[78] + 5*z[76] + 13*z[56];
    z[78]=n<T>(1,3)*z[5];
    z[76]=z[76]*z[78];
    z[50]=z[37]*z[50];
    z[50]= - 8*z[40] - 2*z[43] + z[50];
    z[79]= - 8*z[39] + 5*z[4];
    z[79]=n<T>(1,3)*z[79] - z[75];
    z[79]=z[5]*z[79];
    z[80]=z[67] + n<T>(25,3)*z[12];
    z[80]=z[4]*z[80];
    z[50]=z[79] + n<T>(5,3)*z[50] + z[80];
    z[50]=z[2]*z[50];
    z[30]=z[30] + z[34] + z[50] + z[59] + z[76];
    z[30]=z[11]*z[30];
    z[34]=z[67] + n<T>(14,3)*z[37];
    z[34]=z[8]*z[34];
    z[22]= - z[22] + n<T>(4,3)*z[13];
    z[22]=z[22]*z[49];
    z[22]=z[22] + z[34];
    z[22]=z[9]*z[22];
    z[34]=static_cast<T>(1)- 2*z[72];
    z[36]= - n<T>(20,3)*z[37] - z[36];
    z[36]=z[9]*z[36];
    z[34]=n<T>(20,3)*z[34] + z[36];
    z[34]=z[4]*z[34];
    z[36]=z[23]*z[55];
    z[49]=6*z[18];
    z[22]=z[34] + z[22] - z[73] + n<T>(23,3)*z[36] - n<T>(20,3)*z[12] - z[49] + 
   n<T>(17,3)*z[8];
    z[22]=z[4]*z[22];
    z[34]=2*z[13];
    z[36]=5*z[16] - z[61];
    z[36]= - z[34] + n<T>(1,3)*z[36] - z[27];
    z[50]=z[58]*z[23];
    z[59]=4*z[35] - n<T>(1,3)*z[50];
    z[61]=n<T>(5,3)*z[39];
    z[72]= - z[10]*z[61];
    z[59]=2*z[59] + z[72];
    z[59]=z[9]*z[59];
    z[39]=n<T>(4,3)*z[39] + z[10] - n<T>(2,3)*z[46];
    z[39]=z[9]*z[39];
    z[39]=n<T>(4,3) + z[39];
    z[39]=z[39]*z[75];
    z[36]=z[39] + 2*z[36] + z[59];
    z[36]=z[5]*z[36];
    z[39]= - z[15] + 2*z[8];
    z[39]=3*z[39] - z[51];
    z[39]=z[13]*z[39];
    z[46]=z[8]*z[23];
    z[46]= - 8*z[44] + 5*z[46];
    z[46]=z[9]*z[46];
    z[37]=z[8]*z[37];
    z[37]=z[46] + z[39] + 13*z[66] - n<T>(19,3)*z[37];
    z[37]=z[9]*z[37];
    z[39]= - z[63] - n<T>(25,3)*z[23];
    z[39]=z[9]*z[39];
    z[39]=13*z[10] + z[39];
    z[39]=z[9]*z[39];
    z[46]=z[57] + 2;
    z[59]=z[75]*z[9];
    z[63]= - z[46]*z[59];
    z[39]=z[63] + static_cast<T>(4)+ z[39];
    z[39]=z[5]*z[39];
    z[63]= - z[12]*z[33];
    z[63]=z[55] + z[63];
    z[23]=z[63]*z[23];
    z[23]=n<T>(25,3)*z[13] + z[23] - z[12] - z[38] + z[8];
    z[23]=z[39] + 2*z[23] + z[37];
    z[23]=z[2]*z[23];
    z[37]= - z[65]*z[50];
    z[37]=4*z[44] + z[37];
    z[39]= - z[66]*z[61];
    z[37]=2*z[37] + z[39];
    z[37]=z[9]*z[37];
    z[39]=z[67] - n<T>(14,3)*z[10];
    z[39]=z[8]*z[39];
    z[50]=z[15] + z[8];
    z[50]=3*z[50] + z[12];
    z[50]=z[13]*z[50];
    z[20]=z[20] + z[30] + z[21] + z[23] + z[36] + z[22] + z[37] + 3*
    z[50] + z[39] - z[28];
    z[20]=z[1]*z[20];
    z[21]=z[52] - z[74];
    z[21]=z[6]*z[21];
    z[22]=z[13]*z[14];
    z[21]=6*z[22] - n<T>(7,3) + z[21];
    z[21]=z[3]*z[21];
    z[23]=z[2] - z[4];
    z[28]=static_cast<T>(13)- 10*z[22];
    z[28]=z[13]*z[28];
    z[21]=z[21] - n<T>(55,3)*z[5] + n<T>(2,3)*z[28] - z[12] - z[49] - z[71] + 
    z[31] + n<T>(16,3)*z[23];
    z[21]=z[3]*z[21];
    z[23]= - static_cast<T>(1)- z[22];
    z[23]=z[13]*z[23];
    z[23]= - z[64] - z[32] + n<T>(10,3)*z[23] + z[70] - z[27];
    z[23]=z[23]*z[60];
    z[28]=z[67] - z[12];
    z[28]=z[4]*z[28];
    z[28]=n<T>(10,3)*z[41] - n<T>(20,3)*z[43] + z[28];
    z[28]=z[2]*z[28];
    z[30]=z[4] - z[13];
    z[30]=2*z[30] + z[5];
    z[30]=z[30]*z[77];
    z[31]=z[12] - z[51];
    z[31]=z[13]*z[31];
    z[30]=z[30] + z[31] - n<T>(4,3)*z[42];
    z[31]=6*z[13];
    z[36]= - n<T>(7,3)*z[12] + z[31];
    z[36]=z[3]*z[36];
    z[30]=2*z[30] + z[36];
    z[30]=z[3]*z[30];
    z[36]=2*z[40] + z[43];
    z[36]=10*z[36] - z[56];
    z[36]=z[36]*z[78];
    z[37]=z[11]*z[48];
    z[29]=z[4]*z[29];
    z[28]=n<T>(10,3)*z[37] + z[30] + z[28] + 7*z[29] + z[36];
    z[28]=z[11]*z[28];
    z[29]= - z[18] - z[69];
    z[29]=3*z[29] + z[51];
    z[29]=z[4]*z[29];
    z[30]= - 10*z[12] + z[13];
    z[30]=n<T>(2,3)*z[30] + z[32];
    z[32]=static_cast<T>(5)+ z[59];
    z[32]=z[5]*z[32];
    z[30]=2*z[30] + z[32];
    z[30]=z[2]*z[30];
    z[21]=z[28] + z[21] + z[30] + z[23] - z[62] + z[29];
    z[21]=z[11]*z[21];
    z[23]= - z[78] + z[71] + z[67];
    z[23]=z[14]*z[23];
    z[28]=7*z[6];
    z[29]= - z[14] - z[28];
    z[26]=z[29]*z[26];
    z[25]= - 7*z[25] + z[26];
    z[25]=z[25]*z[65];
    z[19]=z[8]*z[19];
    z[19]= - 20*z[33] + 7*z[19];
    z[26]=n<T>(2,3)*z[4];
    z[19]=z[19]*z[26];
    z[19]=z[19] - 5*z[14] + z[25];
    z[19]=z[3]*z[19];
    z[25]=z[4]*z[33];
    z[25]=n<T>(40,3)*z[25] - n<T>(73,3)*z[6] + 5*z[55];
    z[25]=z[4]*z[25];
    z[29]=n<T>(8,3)*z[2] - n<T>(4,3)*z[12];
    z[29]=z[6]*z[29];
    z[30]=static_cast<T>(2)+ z[68];
    z[30]=z[14]*z[30];
    z[30]=z[30] + z[6];
    z[30]=z[8]*z[30];
    z[19]=z[19] + z[25] + 5*z[22] + z[30] + n<T>(10,3) + z[29] + z[23];
    z[19]=z[3]*z[19];
    z[23]=z[35]*z[9];
    z[25]=n<T>(19,3) + 8*z[58];
    z[29]=z[10]*z[25];
    z[29]= - 16*z[23] + z[29] - z[27];
    z[29]=z[9]*z[29];
    z[30]=z[14]*z[16];
    z[30]= - static_cast<T>(1)+ z[30];
    z[30]=5*z[30] + z[58];
    z[22]=n<T>(1,3)*z[30] + z[22];
    z[30]= - 2*z[57] - n<T>(8,3) + z[58];
    z[30]=z[9]*z[30];
    z[30]=n<T>(2,3)*z[14] + z[30];
    z[30]=z[30]*z[75];
    z[22]=z[30] + 2*z[22] + z[29];
    z[22]=z[5]*z[22];
    z[27]= - z[51] - z[27] - z[49] + n<T>(25,3)*z[8];
    z[27]=z[9]*z[27];
    z[29]=z[6] - z[9];
    z[29]=z[29]*z[54];
    z[30]=z[6]*z[47];
    z[27]=z[29] + z[27] - static_cast<T>(7)+ z[30];
    z[27]=z[4]*z[27];
    z[29]=z[46]*z[45]*z[75];
    z[30]=2*z[9];
    z[23]= - 5*z[10] + 4*z[23];
    z[23]=z[23]*z[30];
    z[23]=static_cast<T>(9)+ z[23];
    z[23]=z[9]*z[23];
    z[23]=z[23] + z[29];
    z[23]=z[5]*z[23];
    z[29]=z[44]*z[9];
    z[32]= - 5*z[66] + 4*z[29];
    z[32]=z[9]*z[32];
    z[33]= - 19*z[8] + z[13];
    z[32]=n<T>(1,3)*z[33] + z[32];
    z[32]=z[9]*z[32];
    z[33]=z[69] + z[8];
    z[33]=z[6]*z[33];
    z[32]=z[32] - n<T>(16,3) + z[33];
    z[23]=2*z[32] + z[23];
    z[23]=z[2]*z[23];
    z[25]=z[25]*z[66];
    z[25]=z[25] - 16*z[29];
    z[25]=z[9]*z[25];
    z[29]=z[38] - 2*z[18];
    z[32]= - static_cast<T>(2)+ z[58];
    z[32]=z[8]*z[32];
    z[19]=z[20] + z[21] + z[19] + z[23] + z[22] + z[27] + z[25] - z[31]
    + z[74] + 3*z[29] + n<T>(2,3)*z[32];
    z[19]=z[1]*z[19];
    z[20]= - z[34] + z[67] + z[69];
    z[20]= - z[3] - n<T>(5,3)*z[2] + n<T>(17,3)*z[5] + 3*z[20] - z[24];
    z[20]=z[11]*z[20];
    z[21]=22*z[14] - z[6];
    z[21]=z[21]*z[65];
    z[22]=z[28] + z[30];
    z[22]=z[22]*z[26];
    z[23]= - 58*z[14] + 47*z[9];
    z[23]=z[23]*z[78];
    z[24]=z[14]*z[18];
    z[25]=29*z[6] + 23*z[9];
    z[25]=z[2]*z[25];
    z[26]=z[14] - n<T>(2,3)*z[6];
    z[26]=z[3]*z[26];
    z[19]=z[19] + z[20] + z[26] + n<T>(1,3)*z[25] + z[23] + z[22] - n<T>(29,3)*
    z[53] + z[21] - static_cast<T>(1)- 9*z[24];

    r += 2*z[19];
 
    return r;
}

template double qg_2lNLC_r1276(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1276(const std::array<dd_real,31>&);
#endif
