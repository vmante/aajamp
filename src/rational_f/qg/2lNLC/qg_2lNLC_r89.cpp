#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r89(const std::array<T,31>& k) {
  T z[131];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[8];
    z[8]=k[12];
    z[9]=k[2];
    z[10]=k[3];
    z[11]=k[9];
    z[12]=k[5];
    z[13]=k[14];
    z[14]=k[13];
    z[15]=k[24];
    z[16]=npow(z[11],2);
    z[17]=npow(z[10],2);
    z[18]=z[16]*z[17];
    z[19]=npow(z[8],2);
    z[20]=n<T>(1,2)*z[19];
    z[21]= - z[18]*z[20];
    z[22]=npow(z[9],2);
    z[23]=z[22]*z[6];
    z[24]=npow(z[13],3);
    z[25]=z[24]*z[23];
    z[26]=npow(z[14],3);
    z[27]=n<T>(1,2)*z[7];
    z[28]=z[26]*npow(z[2],2)*z[27];
    z[21]=z[28] + z[21] + z[25];
    z[25]=npow(z[3],4);
    z[21]=z[21]*z[25];
    z[28]=npow(z[11],3);
    z[29]= - z[6]*z[28]*z[19];
    z[21]=z[29] + z[21];
    z[21]=z[12]*z[21];
    z[29]=2*z[16];
    z[30]= - z[19]*z[29];
    z[31]=npow(z[7],2);
    z[32]=z[31]*z[6];
    z[33]=n<T>(3,2)*z[8];
    z[34]=z[33]*z[32];
    z[30]=z[30] + z[34];
    z[30]=z[6]*z[30];
    z[34]=npow(z[2],3);
    z[35]=npow(z[4],3);
    z[36]=z[35]*z[6];
    z[37]=z[34]*z[25]*z[36];
    z[21]=z[21] + z[30] + n<T>(3,2)*z[37];
    z[21]=z[12]*z[21];
    z[30]=z[8]*z[11];
    z[37]= - z[31] - z[30];
    z[37]=z[8]*z[37];
    z[38]=n<T>(115,6)*z[7] - z[8];
    z[38]=z[8]*z[38];
    z[39]=z[7] - z[8];
    z[39]=z[5]*z[39];
    z[38]=z[38] + z[39];
    z[39]=n<T>(1,2)*z[5];
    z[38]=z[38]*z[39];
    z[40]=z[8]*z[7];
    z[41]=z[40] - z[31];
    z[42]=n<T>(1,2)*z[6];
    z[43]= - z[41]*z[42];
    z[37]=z[43] + z[37] + z[38];
    z[37]=z[6]*z[37];
    z[38]=npow(z[5],2);
    z[43]=n<T>(1,2)*z[22];
    z[44]=z[38]*z[43]*z[18];
    z[45]=n<T>(3,2)*z[10];
    z[46]=z[35]*z[5];
    z[47]= - z[46]*z[45];
    z[48]=3*z[35];
    z[49]=npow(z[4],2);
    z[50]=z[49]*z[5];
    z[51]= - z[48] + n<T>(13,2)*z[50];
    z[52]=z[5]*z[4];
    z[53]=z[52]*z[6];
    z[54]= - 20*z[53] - z[51];
    z[54]=z[6]*z[54];
    z[54]=n<T>(3,2)*z[46] + z[54];
    z[54]=z[2]*z[54];
    z[47]=z[47] + z[54];
    z[34]=z[47]*z[34];
    z[34]=z[44] + z[34];
    z[25]=z[34]*z[25];
    z[34]=2*z[28];
    z[44]=z[7]*z[9];
    z[47]= - z[44]*z[34];
    z[54]=z[22]*z[7];
    z[55]=z[28]*z[54];
    z[56]=n<T>(1,2)*z[8];
    z[57]=z[56]*z[7];
    z[55]=z[55] - z[57];
    z[55]=z[5]*z[55];
    z[47]=z[47] + z[55];
    z[47]=z[5]*z[47];
    z[55]= - z[2]*z[19]*z[38]*z[42];
    z[38]=z[38]*z[40];
    z[32]=z[8]*z[32];
    z[32]=z[38] + z[32];
    z[32]=z[6]*z[32];
    z[38]=z[3]*z[2];
    z[58]=z[36]*z[5];
    z[38]=z[58]*npow(z[38],5);
    z[32]=z[32] + 3*z[38];
    z[32]=z[1]*z[32];
    z[38]=npow(z[13],2);
    z[59]=z[27]*z[38];
    z[60]=z[7]*z[16];
    z[60]= - z[59] + z[60];
    z[60]=z[11]*z[60];
    z[61]= - z[38]*z[56];
    z[62]=z[14]*z[8]*z[13];
    z[61]=z[61] + z[62];
    z[61]=z[14]*z[61];
    z[21]=n<T>(1,2)*z[32] + z[21] + z[25] + z[55] + z[37] + z[61] + z[60] + 
    z[47];
    z[21]=z[1]*z[21];
    z[25]=n<T>(3,4)*z[19];
    z[32]=npow(z[14],2);
    z[37]=n<T>(1,2)*z[32] - z[25] + 2*z[49];
    z[37]=z[6]*z[37];
    z[47]=2*z[7];
    z[55]=z[47]*z[49];
    z[60]=n<T>(1,2)*z[38];
    z[61]=npow(z[15],2);
    z[62]=z[60] - z[61];
    z[63]=n<T>(3,2)*z[40] - z[62];
    z[63]=z[8]*z[63];
    z[64]=4*z[7];
    z[65]=z[14] - z[64] + z[56];
    z[65]=z[65]*z[32];
    z[37]=z[37] + z[65] + z[63] - z[55];
    z[37]=z[2]*z[37];
    z[63]=z[10]*z[16];
    z[65]=z[11]*z[10];
    z[66]= - z[8]*z[65];
    z[63]=z[63] + z[66];
    z[63]=z[63]*z[56];
    z[66]=n<T>(1,2)*z[10];
    z[67]=z[19]*z[66];
    z[68]=z[4]*z[10];
    z[69]=3*z[68];
    z[70]= - z[8]*z[69];
    z[67]=z[67] + z[70];
    z[67]=z[4]*z[67];
    z[70]= - z[16] - z[32];
    z[71]=z[14]*z[10];
    z[70]=z[70]*z[71];
    z[72]= - z[61] - n<T>(3,2)*z[38];
    z[73]=z[6]*z[9];
    z[72]=z[72]*z[73];
    z[37]=z[37] + 3*z[72] + z[70] + z[63] + z[67];
    z[63]=npow(z[3],3);
    z[37]=z[37]*z[63];
    z[67]= - z[8]*z[62];
    z[70]=n<T>(7,2)*z[8];
    z[72]= - 15*z[7] - z[70];
    z[72]=z[72]*z[56];
    z[74]=z[4]*z[8];
    z[72]=z[72] + 3*z[74];
    z[72]=z[4]*z[72];
    z[75]= - n<T>(3,2)*z[19] + z[32];
    z[75]=z[75]*z[42];
    z[67]=z[75] + z[67] + z[72];
    z[67]=z[67]*z[63];
    z[72]=npow(z[6],2);
    z[75]=z[8]*z[28]*z[72];
    z[67]=z[75] + z[67];
    z[67]=z[12]*z[67];
    z[75]=n<T>(3,2)*z[31] + z[29];
    z[75]=z[8]*z[75];
    z[76]=n<T>(1,2)*z[4];
    z[77]=z[16]*z[76];
    z[75]=z[75] + z[77];
    z[75]=z[6]*z[75];
    z[77]=z[16]*z[33];
    z[77]= - n<T>(14,3)*z[28] + z[77];
    z[77]=z[8]*z[77];
    z[75]=z[77] + z[75];
    z[75]=z[6]*z[75];
    z[77]=z[31] + z[19];
    z[77]=z[28]*z[77];
    z[78]=z[16]*z[4];
    z[79]=n<T>(1,2)*z[31];
    z[80]= - z[79]*z[78];
    z[81]=n<T>(7,2)*z[74];
    z[82]=3*z[8];
    z[83]= - z[15]*z[82];
    z[83]=z[83] - z[81];
    z[83]=z[14]*z[83];
    z[84]= - z[8]*z[61];
    z[83]=z[84] + z[83];
    z[83]=z[83]*z[32];
    z[37]=z[67] + z[37] + z[75] + z[83] + z[80] + z[77];
    z[37]=z[12]*z[37];
    z[67]=z[17]*z[32];
    z[67]=z[18] + z[67];
    z[75]=2*z[14];
    z[67]=z[67]*z[75];
    z[77]=z[56] + z[7];
    z[80]= - z[14] + z[77];
    z[80]=z[80]*z[32];
    z[83]=z[7]*z[49];
    z[84]=5*z[72];
    z[85]= - z[4]*z[84];
    z[80]=z[85] + z[83] + z[80];
    z[80]=z[2]*z[80];
    z[83]=2*z[10];
    z[85]= - z[26]*z[83];
    z[80]=z[85] + z[80];
    z[80]=z[2]*z[80];
    z[85]=z[16]*z[22];
    z[86]= - z[13]*z[85];
    z[18]= - z[8]*z[18];
    z[87]=4*z[38];
    z[88]= - z[61] - z[87];
    z[88]=z[88]*z[22];
    z[89]=z[13]*z[23];
    z[88]=z[88] + z[89];
    z[88]=z[6]*z[88];
    z[18]=z[80] + z[88] + z[67] + z[86] + z[18];
    z[18]=z[18]*z[63];
    z[67]=n<T>(5,2)*z[31];
    z[29]=3*z[30] - z[67] + z[29];
    z[29]=z[8]*z[29];
    z[80]=z[16] - z[49];
    z[86]=n<T>(3,2)*z[4];
    z[80]=z[80]*z[86];
    z[88]=z[11]*z[76];
    z[88]=z[88] - z[41];
    z[88]=z[6]*z[88];
    z[29]=z[88] + z[29] + z[80];
    z[29]=z[6]*z[29];
    z[80]=z[31]*z[56];
    z[88]= - z[13] + z[27];
    z[88]=z[11]*z[88];
    z[88]=z[79] + z[88];
    z[88]=z[11]*z[88];
    z[88]=z[80] - z[24] + z[88];
    z[88]=z[4]*z[88];
    z[89]=2*z[13];
    z[90]=z[89] + 5*z[15];
    z[91]=z[8]*z[90];
    z[92]=z[14]*z[56];
    z[91]=z[92] + z[91] + z[81];
    z[91]=z[14]*z[91];
    z[92]=z[61] - z[38];
    z[93]=z[8]*z[92];
    z[78]=z[91] + z[93] - z[78];
    z[78]=z[14]*z[78];
    z[91]=z[11]*z[7];
    z[93]=n<T>(14,3) - 3*z[44];
    z[93]=z[93]*z[91];
    z[93]=z[31] + z[93];
    z[93]=z[93]*z[16];
    z[94]=2*z[8];
    z[95]=z[16]*z[94];
    z[96]=z[11]*z[60];
    z[95]=z[96] + z[95];
    z[95]=z[8]*z[95];
    z[18]=z[37] + z[18] + z[29] + z[78] + z[88] + z[93] + z[95];
    z[18]=z[12]*z[18];
    z[29]=z[65]*z[5];
    z[37]=n<T>(1,2)*z[14];
    z[78]= - z[37] + z[29] + z[13] - z[33];
    z[78]=z[14]*z[78];
    z[88]=3*z[15];
    z[93]=n<T>(7,2)*z[13];
    z[95]= - z[88] - z[93];
    z[95]=z[8]*z[95];
    z[96]=3*z[4];
    z[97]=z[11] + z[56];
    z[97]=z[97]*z[96];
    z[98]=4*z[11];
    z[99]= - z[5]*z[98];
    z[78]=z[78] + z[99] + z[97] + z[95] - z[60] + z[16];
    z[78]=z[14]*z[78];
    z[95]=z[60] + 2*z[40];
    z[95]=z[8]*z[95];
    z[97]=z[4]*z[87];
    z[57]= - z[5]*z[57];
    z[57]=z[57] + z[95] + z[97];
    z[57]=z[5]*z[57];
    z[95]= - z[5]*z[8];
    z[20]=z[95] - z[20] + z[49];
    z[95]=n<T>(9,2)*z[4];
    z[97]= - z[8] + z[95];
    z[97]=z[6]*z[97];
    z[20]=z[97] + n<T>(5,2)*z[20];
    z[20]=z[5]*z[20];
    z[20]= - z[48] + z[20];
    z[20]=z[6]*z[20];
    z[48]=z[26]*z[56];
    z[97]=2*z[4];
    z[99]= - z[24]*z[97];
    z[100]= - z[24]*z[52];
    z[100]=z[100] - n<T>(3,2)*z[58];
    z[100]=z[2]*z[100];
    z[20]=z[100] + z[20] - z[48] + z[99] + z[57];
    z[20]=z[2]*z[20];
    z[57]=z[79]*z[9];
    z[99]=z[95] + z[57] - z[94];
    z[99]=z[6]*z[99];
    z[100]=n<T>(5,2)*z[4];
    z[101]=2*z[11];
    z[102]=z[101] + z[100];
    z[102]=z[4]*z[102];
    z[103]= - static_cast<T>(1)+ z[44];
    z[103]=z[103]*z[39];
    z[103]=z[103] - n<T>(23,4)*z[4] + n<T>(109,12)*z[7] - 8*z[8];
    z[103]=z[5]*z[103];
    z[104]=n<T>(1,4)*z[8] + n<T>(91,4)*z[7] + z[98];
    z[104]=z[8]*z[104];
    z[99]=z[99] + z[103] + z[102] - z[79] + z[104];
    z[99]=z[6]*z[99];
    z[102]=z[5]*z[10];
    z[103]=z[49]*z[102];
    z[104]=3*z[10];
    z[105]=z[104]*z[5];
    z[106]= - z[105] + z[71];
    z[106]=z[106]*z[32];
    z[103]=9*z[103] + z[106];
    z[106]= - 17*z[49] - n<T>(31,2)*z[52];
    z[107]= - 25*z[4] + 36*z[5];
    z[107]=z[6]*z[107];
    z[106]=n<T>(1,2)*z[106] + z[107];
    z[106]=z[6]*z[106];
    z[51]=z[106] - z[51];
    z[51]=z[2]*z[51];
    z[106]=z[5]*z[9];
    z[107]=z[72]*z[106];
    z[51]=z[51] + n<T>(1,2)*z[103] - 22*z[107];
    z[51]=z[2]*z[51];
    z[103]=z[17]*z[5];
    z[107]=z[17]*z[14];
    z[108]= - 3*z[103] + z[107];
    z[108]=z[108]*z[32];
    z[109]=z[22]*z[5];
    z[84]=z[109]*z[84];
    z[51]=z[51] + z[108] + z[84];
    z[51]=z[2]*z[51];
    z[84]= - z[66]*z[85];
    z[85]=z[10]*z[9];
    z[108]= - z[43] - z[85];
    z[29]=z[108]*z[29];
    z[29]=z[84] + z[29];
    z[29]=z[5]*z[29];
    z[84]=npow(z[10],3);
    z[108]= - z[84]*z[16];
    z[110]=2*z[5];
    z[111]= - z[84]*z[110];
    z[112]=z[84]*z[14];
    z[111]=z[111] - z[112];
    z[111]=z[14]*z[111];
    z[108]=z[108] + z[111];
    z[108]=z[14]*z[108];
    z[29]=z[51] + z[29] + z[108];
    z[29]=z[29]*z[63];
    z[51]=3*z[11];
    z[63]= - z[22]*z[51];
    z[63]=n<T>(5,2)*z[9] + z[63];
    z[63]=z[63]*z[16];
    z[63]= - z[56] - z[27] + z[63];
    z[63]=z[5]*z[63];
    z[108]=npow(z[9],3);
    z[111]=z[108]*z[7];
    z[113]=n<T>(14,3)*z[22];
    z[111]=z[111] - z[113];
    z[114]= - z[7]*z[111];
    z[114]=4*z[9] + z[114];
    z[114]=z[11]*z[114];
    z[114]=z[114] - static_cast<T>(1)+ n<T>(13,3)*z[44];
    z[114]=z[11]*z[114];
    z[114]=n<T>(25,3)*z[7] + z[114];
    z[114]=z[11]*z[114];
    z[115]=z[13] + n<T>(25,6)*z[7];
    z[115]=n<T>(1,2)*z[115] + z[8];
    z[115]=z[8]*z[115];
    z[116]=n<T>(19,2)*z[4];
    z[117]=z[13]*z[116];
    z[63]=z[63] + z[117] + z[114] + z[115];
    z[63]=z[5]*z[63];
    z[114]=z[38]*z[9];
    z[115]=n<T>(1,2)*z[13];
    z[117]= - z[27] + z[115] - z[114];
    z[117]=z[7]*z[117];
    z[118]= - n<T>(29,3) - z[44];
    z[118]=z[118]*z[27];
    z[119]= - n<T>(28,3)*z[9] + 3*z[54];
    z[119]=z[7]*z[119];
    z[119]= - static_cast<T>(1)+ z[119];
    z[119]=z[11]*z[119];
    z[118]=z[118] + z[119];
    z[118]=z[11]*z[118];
    z[117]=z[118] + z[60] + z[117];
    z[117]=z[11]*z[117];
    z[118]=3*z[7];
    z[119]=n<T>(1,2)*z[11];
    z[120]= - z[56] + z[118] - z[119];
    z[120]=z[8]*z[120];
    z[120]=z[120] + z[87] + n<T>(3,2)*z[91];
    z[120]=z[4]*z[120];
    z[121]=3*z[38];
    z[122]=z[11]*z[13];
    z[122]=z[121] + z[122];
    z[122]=n<T>(1,2)*z[122] + z[30];
    z[122]=z[8]*z[122];
    z[18]=z[21] + z[18] + z[29] + z[20] + z[99] + z[78] + z[63] + z[120]
    + z[122] + z[59] + z[117];
    z[18]=z[1]*z[18];
    z[20]= - z[11] - z[97];
    z[20]=z[20]*z[97];
    z[21]= - n<T>(7,2)*z[4] + z[57] + z[51];
    z[21]=z[6]*z[21];
    z[29]= - z[33] + n<T>(37,2)*z[7] + 16*z[11];
    z[29]=z[8]*z[29];
    z[57]= - z[4] - z[90];
    z[57]=z[14]*z[57];
    z[20]=z[21] + z[57] + z[20] + z[29] + n<T>(67,6)*z[16] - z[61] - z[31];
    z[20]=z[6]*z[20];
    z[21]=7*z[4];
    z[29]=z[64] + z[21];
    z[29]=z[4]*z[29];
    z[57]=9*z[14] + z[47] - z[56];
    z[57]=z[14]*z[57];
    z[63]=4*z[6];
    z[64]= - z[8] - z[96];
    z[64]=n<T>(1,2)*z[64] - z[14];
    z[64]=n<T>(5,2)*z[64] + z[63];
    z[64]=z[6]*z[64];
    z[78]=z[115] + z[15];
    z[97]=3*z[78];
    z[99]= - z[97] + n<T>(7,2)*z[7];
    z[99]=z[8]*z[99];
    z[29]=z[64] + z[57] + z[99] + z[29];
    z[29]=z[2]*z[29];
    z[57]=3*z[61];
    z[64]= - z[57] + n<T>(7,2)*z[38];
    z[64]=z[9]*z[64];
    z[99]= - z[44] - z[65];
    z[99]=z[99]*z[119];
    z[117]=z[65]*z[94];
    z[120]=z[8]*z[10];
    z[122]= - 5*z[120] - z[69];
    z[122]=z[4]*z[122];
    z[119]= - z[42] - z[119] - 19*z[15] + n<T>(23,2)*z[13];
    z[119]=z[9]*z[119];
    z[123]=z[27]*z[9];
    z[119]= - z[123] + z[119];
    z[119]=z[6]*z[119];
    z[124]=z[10]*z[32];
    z[29]=z[29] + z[119] + n<T>(15,2)*z[124] + z[122] + z[117] + z[64] + 
    z[99];
    z[64]=npow(z[3],2);
    z[29]=z[29]*z[64];
    z[99]=z[11] + z[7];
    z[117]= - z[15] + z[115] + z[99];
    z[117]=z[14]*z[117];
    z[92]=z[117] - z[92];
    z[92]=z[14]*z[92];
    z[28]= - z[28]*z[94];
    z[117]=z[6]*z[80];
    z[28]=z[28] + z[117];
    z[28]=z[12]*z[28];
    z[117]= - 2*z[31] + n<T>(41,6)*z[16];
    z[117]=z[8]*z[117];
    z[41]=5*z[16] - z[41];
    z[119]= - z[4]*z[11];
    z[41]=n<T>(1,2)*z[41] + z[119];
    z[41]=z[6]*z[41];
    z[28]=z[28] + z[41] + z[92] - z[34] + z[117];
    z[28]=z[6]*z[28];
    z[34]=n<T>(9,2)*z[8] - z[101] + 14*z[7] + z[78];
    z[34]=z[8]*z[34];
    z[41]= - z[27] + z[8];
    z[41]=5*z[41] + z[4];
    z[41]=z[41]*z[96];
    z[92]= - 11*z[14] - 7*z[7] - z[82];
    z[92]=z[92]*z[37];
    z[117]= - z[15] - n<T>(1,4)*z[13];
    z[117]=z[6]*z[117];
    z[34]=9*z[117] + z[92] + z[41] + z[34] - z[62];
    z[34]=z[34]*z[64];
    z[41]=z[38] + n<T>(14,3)*z[16];
    z[41]=z[11]*z[41];
    z[92]= - n<T>(1,2) + z[65];
    z[92]=z[8]*z[92]*z[16];
    z[41]=z[41] + z[92];
    z[41]=z[8]*z[41];
    z[92]=z[57] - z[60];
    z[92]=z[8]*z[92];
    z[117]=9*z[15];
    z[119]=z[117] + z[13];
    z[119]=z[8]*z[119];
    z[119]=z[119] + n<T>(23,2)*z[74];
    z[119]=z[14]*z[119];
    z[92]=z[92] + z[119];
    z[92]=z[14]*z[92];
    z[67]= - z[16]*z[67];
    z[119]=z[11] + z[8];
    z[119]=z[4]*z[31]*z[119];
    z[28]=z[34] + z[92] + z[119] + z[67] + z[41] + z[28];
    z[28]=z[12]*z[28];
    z[34]=4*z[15];
    z[41]=z[34] + z[115];
    z[41]=z[116] + 3*z[41] - z[70];
    z[41]=z[14]*z[41];
    z[67]=6*z[13];
    z[70]= - 13*z[15] - z[67];
    z[70]=z[8]*z[70];
    z[41]=z[41] - z[81] + z[70] + z[57] + z[60];
    z[41]=z[14]*z[41];
    z[70]= - static_cast<T>(1)+ 2*z[65];
    z[70]=z[70]*z[30];
    z[81]=2*z[61];
    z[92]= - z[13] - z[101];
    z[92]=z[11]*z[92];
    z[70]=z[70] + z[92] - z[81] + n<T>(5,2)*z[38];
    z[70]=z[8]*z[70];
    z[92]=11*z[7] - z[33];
    z[92]=z[92]*z[56];
    z[116]=z[13] + z[77];
    z[116]=z[4]*z[116];
    z[91]=z[116] + z[92] + n<T>(9,2)*z[38] - z[91];
    z[91]=z[4]*z[91];
    z[92]=n<T>(7,3) + z[44];
    z[92]=z[92]*z[101];
    z[116]=3*z[13];
    z[119]=5*z[44];
    z[122]=n<T>(4,3) + z[119];
    z[122]=z[7]*z[122];
    z[92]=z[92] - z[116] + z[122];
    z[92]=z[11]*z[92];
    z[92]=z[92] + z[38] - z[31];
    z[92]=z[11]*z[92];
    z[122]=n<T>(9,2)*z[2];
    z[124]=z[36]*z[122];
    z[24]=3*z[24];
    z[20]=z[28] + z[29] + z[124] + z[20] + z[41] + z[91] + z[70] - z[24]
    + z[92];
    z[20]=z[12]*z[20];
    z[28]=z[5]*z[38];
    z[29]=z[39] + z[7];
    z[29]=z[29]*z[13];
    z[41]=z[14]*z[29];
    z[28]=z[41] - z[59] + z[28];
    z[28]=z[14]*z[28];
    z[41]=9*z[35];
    z[70]= - 17*z[53] + z[41] - n<T>(23,2)*z[50];
    z[70]=z[6]*z[70];
    z[58]=z[122]*z[58];
    z[91]=2*z[38];
    z[92]=z[13] + z[86];
    z[92]=z[4]*z[92];
    z[92]=z[91] + z[92];
    z[92]=z[92]*z[52];
    z[28]=z[58] + z[70] + z[92] + z[28];
    z[28]=z[2]*z[28];
    z[70]= - 3*z[19] - 31*z[49];
    z[92]= - n<T>(7,2)*z[5] - 4*z[8] + n<T>(79,4)*z[4];
    z[92]=z[5]*z[92];
    z[122]=11*z[5];
    z[124]= - n<T>(39,2)*z[4] + z[122];
    z[124]=z[6]*z[124];
    z[70]=z[124] + n<T>(1,2)*z[70] + z[92];
    z[70]=z[6]*z[70];
    z[92]=z[38] + 11*z[40];
    z[92]=z[92]*z[56];
    z[124]=z[89] + z[96];
    z[124]=z[4]*z[124];
    z[124]=n<T>(13,2)*z[38] + z[124];
    z[124]=z[4]*z[124];
    z[125]= - z[116] + n<T>(13,6)*z[7];
    z[125]=z[8]*z[125];
    z[121]=z[121] + z[125];
    z[125]= - 26*z[13] - z[100];
    z[125]=z[4]*z[125];
    z[121]=n<T>(1,2)*z[121] + z[125];
    z[121]=z[5]*z[121];
    z[125]=n<T>(3,2)*z[13];
    z[126]= - z[125] - z[8];
    z[126]=z[14]*z[126];
    z[126]= - z[60] + z[126];
    z[126]=z[14]*z[126];
    z[28]=z[28] + z[70] + z[126] + z[121] + z[124] + z[92] - z[24] + 
    z[59];
    z[28]=z[2]*z[28];
    z[59]=z[101]*z[17];
    z[70]=z[59] + z[104];
    z[92]= - z[5]*z[70];
    z[121]= - static_cast<T>(1)+ z[65];
    z[92]=n<T>(3,2)*z[71] + 3*z[121] + z[92];
    z[92]=z[14]*z[92];
    z[121]=z[65] + 1;
    z[124]= - z[121]*z[51];
    z[126]=static_cast<T>(5)+ 8*z[65];
    z[126]=z[5]*z[126];
    z[90]=z[92] + z[126] - z[100] + z[124] - 2*z[90] - z[118];
    z[90]=z[14]*z[90];
    z[92]=z[14] - 5*z[7] + z[5];
    z[92]=z[92]*z[37];
    z[118]=z[7] - n<T>(17,2)*z[4];
    z[118]=z[4]*z[118];
    z[124]=n<T>(61,2)*z[6] - z[21] + n<T>(69,4)*z[5];
    z[124]=z[6]*z[124];
    z[92]=z[124] + z[92] + z[118] + n<T>(41,4)*z[52];
    z[92]=z[2]*z[92];
    z[118]= - z[49]*z[104];
    z[124]=n<T>(13,2)*z[68];
    z[126]=z[124] - z[102];
    z[126]=z[126]*z[39];
    z[127]=z[104]*z[14];
    z[128]= - z[102] - z[127];
    z[128]=z[128]*z[37];
    z[129]= - z[9]*z[39];
    z[129]=z[129] - 19*z[73];
    z[129]=z[129]*z[42];
    z[92]=z[92] + z[129] + z[128] + z[118] + z[126];
    z[92]=z[2]*z[92];
    z[118]=z[22]*z[116];
    z[126]= - z[9] + z[10];
    z[126]=z[126]*z[66];
    z[126]=z[22] + z[126];
    z[126]=z[11]*z[126];
    z[118]=z[118] + z[126];
    z[118]=z[11]*z[118];
    z[126]=n<T>(3,2)*z[22];
    z[128]= - z[126] + z[85];
    z[128]=z[11]*z[128];
    z[129]=z[9] + z[66];
    z[129]=z[129]*z[102];
    z[128]=z[128] + z[129];
    z[128]=z[5]*z[128];
    z[129]= - z[42] - z[34] + z[93];
    z[129]=z[22]*z[129];
    z[130]=z[11]*z[43];
    z[129]= - n<T>(17,4)*z[109] + z[130] + z[129];
    z[129]=z[6]*z[129];
    z[130]=z[103] + z[107];
    z[130]=z[130]*z[37];
    z[92]=z[92] + z[129] + z[130] + z[118] + z[128];
    z[92]=z[92]*z[64];
    z[118]=static_cast<T>(1)+ z[120];
    z[118]=z[118]*z[56];
    z[67]= - z[86] + z[118] - z[98] - z[67] - z[27];
    z[67]=z[4]*z[67];
    z[98]= - n<T>(44,3)*z[9] - n<T>(5,2)*z[54];
    z[98]=z[7]*z[98];
    z[118]=n<T>(7,3)*z[9] - 2*z[54];
    z[118]=2*z[118] - z[10];
    z[118]=z[11]*z[118];
    z[98]=z[118] - n<T>(31,6) + z[98];
    z[98]=z[11]*z[98];
    z[118]= - z[9]*z[60];
    z[118]= - z[13] + z[118];
    z[118]=z[9]*z[118];
    z[118]=static_cast<T>(9)+ z[118];
    z[118]=z[7]*z[118];
    z[89]=z[98] + z[118] + z[89] + z[114];
    z[89]=z[11]*z[89];
    z[98]=6*z[15];
    z[118]= - n<T>(25,2) + z[65];
    z[118]=z[11]*z[118];
    z[128]=static_cast<T>(4)- n<T>(1,2)*z[65];
    z[128]=z[8]*z[128];
    z[118]=z[128] + z[118] + n<T>(29,4)*z[7] + z[98] + z[115];
    z[118]=z[8]*z[118];
    z[128]=3*z[65];
    z[129]=z[22]*z[128];
    z[129]=z[129] + z[43] - 5*z[85];
    z[129]=z[11]*z[129];
    z[129]=z[45] + z[129];
    z[129]=z[11]*z[129];
    z[130]= - z[9]*z[15];
    z[129]=z[129] - n<T>(3,2) + z[130];
    z[129]=z[5]*z[129];
    z[111]= - z[85] + z[111];
    z[111]=z[111]*z[101];
    z[130]=n<T>(13,2)*z[9] + 8*z[54];
    z[111]=z[111] + n<T>(5,3)*z[130] + z[10];
    z[111]=z[11]*z[111];
    z[119]= - n<T>(7,3) + z[119];
    z[111]=2*z[119] + z[111];
    z[111]=z[11]*z[111];
    z[119]= - n<T>(1,3) + z[65];
    z[119]=z[119]*z[56];
    z[111]=z[129] - n<T>(1,4)*z[4] + z[119] + z[111] - n<T>(35,12)*z[7] - z[15]
    + n<T>(33,2)*z[13];
    z[111]=z[5]*z[111];
    z[119]= - static_cast<T>(7)+ z[44];
    z[129]=z[54] + z[9];
    z[130]= - z[5]*z[129];
    z[119]=n<T>(23,6)*z[119] + z[130];
    z[119]=z[119]*z[39];
    z[129]=z[129]*z[27];
    z[130]= - z[5]*z[54];
    z[129]=z[130] + static_cast<T>(6)+ z[129];
    z[129]=z[6]*z[129];
    z[100]=z[129] + z[119] + z[100] - n<T>(21,4)*z[8] + n<T>(113,6)*z[11] + n<T>(73,6)*z[7] - z[34] - n<T>(9,2)*z[13];
    z[100]=z[6]*z[100];
    z[119]=z[114] - z[116];
    z[129]=z[119]*z[27];
    z[18]=z[18] + z[20] + z[92] + z[28] + z[100] + z[90] + z[111] + 
    z[67] + z[118] + z[89] + z[129] + z[57] + n<T>(25,2)*z[38];
    z[18]=z[1]*z[18];
    z[20]=2*z[15] - z[13];
    z[20]= - z[37] + 2*z[20] - z[99];
    z[20]=z[14]*z[20];
    z[28]=n<T>(5,2)*z[8];
    z[67]=29*z[7] - z[11];
    z[67]=n<T>(1,3)*z[67] - z[28];
    z[67]=z[67]*z[56];
    z[89]= - z[51] + z[4];
    z[89]=z[89]*z[42];
    z[20]=z[89] + z[20] - z[49] + z[67] - z[79] + z[57] - z[38];
    z[20]=z[6]*z[20];
    z[65]= - n<T>(25,2) + 14*z[65];
    z[65]=z[11]*z[65];
    z[65]= - z[125] + n<T>(1,3)*z[65];
    z[65]=z[11]*z[65];
    z[67]=z[17]*z[11];
    z[79]=z[66] + z[67];
    z[79]=z[11]*z[79];
    z[79]= - n<T>(1,2) + z[79];
    z[79]=z[79]*z[30];
    z[65]=z[79] + z[65] - z[81] + z[60];
    z[65]=z[8]*z[65];
    z[79]=n<T>(5,2)*z[13];
    z[89]= - z[117] - z[79];
    z[89]=z[8]*z[89];
    z[90]= - z[14]*z[99];
    z[89]=z[90] + z[89] - n<T>(17,2)*z[74];
    z[89]=z[14]*z[89];
    z[90]= - z[6] + z[4];
    z[80]=z[80]*z[90];
    z[60]=z[60] + z[16];
    z[30]=z[60]*z[30];
    z[30]=z[30] + z[80];
    z[30]=z[12]*z[30];
    z[31]=z[38] + 3*z[31];
    z[16]=n<T>(1,2)*z[31] + z[16];
    z[16]=z[11]*z[16];
    z[31]=z[7] + z[28];
    z[31]=z[4]*z[31];
    z[25]= - z[25] + z[31];
    z[25]=z[4]*z[25];
    z[16]=z[30] + z[20] + z[89] + z[25] + z[16] + z[65];
    z[16]=z[12]*z[16];
    z[20]=z[6]*z[4];
    z[19]=n<T>(13,2)*z[20] - n<T>(5,4)*z[19] + 6*z[49];
    z[19]=z[6]*z[19];
    z[25]=5*z[40] - z[62];
    z[25]=z[8]*z[25];
    z[30]=z[75] - z[7] - z[33];
    z[30]=z[30]*z[32];
    z[31]=z[8]*z[26];
    z[31]=z[31] - 9*z[36];
    z[36]=n<T>(1,2)*z[2];
    z[31]=z[31]*z[36];
    z[19]=z[31] + z[19] + z[30] + z[25] - z[55];
    z[19]=z[2]*z[19];
    z[25]=z[81] - 5*z[38];
    z[25]=z[9]*z[25];
    z[30]=z[14] - z[13];
    z[31]=n<T>(10,3) - z[123];
    z[31]=z[7]*z[31];
    z[55]=z[11]*z[9];
    z[60]= - n<T>(29,6) - z[55];
    z[60]=z[11]*z[60];
    z[62]= - static_cast<T>(5)+ n<T>(3,2)*z[55];
    z[62]=z[6]*z[62];
    z[25]=z[62] + n<T>(7,4)*z[4] - n<T>(47,4)*z[8] + z[60] + z[31] + z[25] + 18*
    z[15] + n<T>(11,2)*z[30];
    z[25]=z[6]*z[25];
    z[30]= - n<T>(23,6)*z[10] + z[59];
    z[30]=z[11]*z[30];
    z[30]= - n<T>(74,3) + z[30];
    z[30]=z[11]*z[30];
    z[31]=n<T>(1,2)*z[67];
    z[59]= - z[10] - z[31];
    z[59]=z[11]*z[59];
    z[59]=n<T>(23,4) + z[59];
    z[59]=z[8]*z[59];
    z[30]=z[59] + z[30] + n<T>(41,3)*z[7] + z[117] + z[115];
    z[30]=z[8]*z[30];
    z[59]=z[116]*z[9];
    z[60]=z[9] + n<T>(14,3)*z[10];
    z[60]=z[11]*z[60];
    z[60]=z[60] - z[44] - n<T>(31,6) - z[59];
    z[60]=z[11]*z[60];
    z[62]= - n<T>(11,3) - n<T>(3,2)*z[44];
    z[62]=z[7]*z[62];
    z[60]=z[60] + n<T>(1,2)*z[119] + z[62];
    z[60]=z[11]*z[60];
    z[62]=z[56]*z[10];
    z[65]=static_cast<T>(25)+ z[62];
    z[65]=z[65]*z[56];
    z[80]= - static_cast<T>(3)- z[120];
    z[80]=z[80]*z[76];
    z[65]=z[80] + z[65] + n<T>(5,4)*z[13] + z[7];
    z[65]=z[4]*z[65];
    z[80]= - z[11]*z[121];
    z[80]=z[80] - z[47] - z[15] - z[125];
    z[81]=z[71] - n<T>(19,2) + z[128];
    z[81]=z[14]*z[81];
    z[80]=z[81] - 9*z[4] + 3*z[80] + z[8];
    z[80]=z[14]*z[80];
    z[24]= - z[9]*z[24];
    z[16]=z[16] + z[19] + z[25] + z[80] + z[65] + z[30] + z[60] + z[24]
    + z[61] + 10*z[38];
    z[16]=z[12]*z[16];
    z[19]=n<T>(51,2)*z[53] - z[41] + n<T>(31,2)*z[50];
    z[19]=z[6]*z[19];
    z[19]= - z[58] + z[19] - 3*z[46] + z[48];
    z[19]=z[2]*z[19];
    z[24]=z[37] - z[39] - z[77];
    z[24]=z[14]*z[24];
    z[24]=z[24] - z[29];
    z[24]=z[14]*z[24];
    z[25]= - n<T>(85,4)*z[4] + z[110];
    z[25]=z[5]*z[25];
    z[29]=z[95] - 5*z[5];
    z[29]=z[6]*z[29];
    z[25]=7*z[29] + n<T>(45,2)*z[49] + z[25];
    z[25]=z[6]*z[25];
    z[29]=n<T>(3,2)*z[68];
    z[30]=static_cast<T>(8)+ z[29];
    z[30]=z[4]*z[30];
    z[30]=z[116] + z[30];
    z[30]=z[30]*z[52];
    z[19]=z[19] + z[25] + z[24] - 6*z[35] + z[30];
    z[19]=z[2]*z[19];
    z[24]= - 2*z[71] + n<T>(1,2) + z[105];
    z[24]=z[14]*z[24];
    z[25]=z[4] + z[97] + z[7];
    z[24]=z[24] + 3*z[25] - z[110];
    z[24]=z[14]*z[24];
    z[25]= - static_cast<T>(55)+ 23*z[106];
    z[25]=z[25]*z[42];
    z[30]=n<T>(11,2)*z[8];
    z[35]=n<T>(35,6) - z[106];
    z[35]=z[5]*z[35];
    z[25]=z[25] + n<T>(5,2)*z[35] - z[30] - 12*z[4];
    z[25]=z[6]*z[25];
    z[35]=9*z[68];
    z[41]= - static_cast<T>(7)- z[35];
    z[41]=z[41]*z[76];
    z[34]=z[5] + z[41] - z[7] + z[34] - n<T>(15,2)*z[13];
    z[34]=z[5]*z[34];
    z[41]=z[7]*z[125];
    z[42]=z[13] + z[15];
    z[46]= - 3*z[42] + n<T>(91,12)*z[7];
    z[46]=z[8]*z[46];
    z[48]=10*z[4] - n<T>(13,2)*z[13] + z[7];
    z[48]=z[4]*z[48];
    z[19]=z[19] + z[25] + z[24] + z[34] + z[48] + z[46] + z[87] + z[41];
    z[19]=z[2]*z[19];
    z[24]= - 11*z[22] - 13*z[85];
    z[25]=z[10]*z[113];
    z[25]= - z[108] + z[25];
    z[25]=z[11]*z[25];
    z[24]=n<T>(7,6)*z[24] + z[25];
    z[24]=z[11]*z[24];
    z[25]=2*z[9];
    z[34]=z[25] + n<T>(5,3)*z[54];
    z[24]=z[24] + 5*z[34] + n<T>(71,6)*z[10];
    z[24]=z[11]*z[24];
    z[34]= - z[22] + n<T>(5,2)*z[85];
    z[34]=z[10]*z[34];
    z[41]= - z[22]*z[67];
    z[34]=z[34] + z[41];
    z[34]=z[11]*z[34];
    z[41]=z[9] - z[45];
    z[41]=z[10]*z[41];
    z[34]=z[34] + z[43] + z[41];
    z[34]=z[11]*z[34];
    z[41]=n<T>(3,2)*z[9];
    z[34]= - z[41] + z[34];
    z[34]=z[5]*z[34];
    z[43]= - z[15] + z[93];
    z[43]=z[9]*z[43];
    z[24]=z[34] - n<T>(7,4)*z[68] + z[24] - 4*z[44] + n<T>(1,12) + z[43];
    z[24]=z[5]*z[24];
    z[34]=z[109] + z[25] - z[54];
    z[34]=z[6]*z[34];
    z[43]=7*z[9] + 101*z[54];
    z[43]=n<T>(1,6)*z[43] + z[109];
    z[43]=z[43]*z[39];
    z[46]=8*z[15] - z[116];
    z[46]=z[9]*z[46];
    z[34]=z[34] + z[43] + n<T>(19,2)*z[55] - n<T>(61,12)*z[44] - n<T>(22,3) + z[46];
    z[34]=z[6]*z[34];
    z[43]=z[11]*z[84];
    z[43]=n<T>(9,2)*z[17] + z[43];
    z[43]=z[5]*z[43];
    z[43]= - z[107] + z[43] + z[10] - 3*z[67];
    z[43]=z[14]*z[43];
    z[44]=z[83] + z[67];
    z[44]=z[44]*z[51];
    z[46]= - z[70]*z[110];
    z[43]=z[43] + z[46] + n<T>(11,2) + z[44];
    z[43]=z[14]*z[43];
    z[44]=n<T>(41,3) - z[59];
    z[44]=z[44]*z[123];
    z[46]=n<T>(14,3)*z[9] + z[10];
    z[46]=z[10]*z[46];
    z[46]=z[22] + z[46];
    z[46]=z[11]*z[46];
    z[46]=z[46] - 12*z[10] - n<T>(31,6)*z[9] + z[54];
    z[46]=z[11]*z[46];
    z[48]=9*z[13] + z[114];
    z[48]=z[9]*z[48];
    z[44]=z[46] + z[44] - static_cast<T>(24)+ n<T>(1,2)*z[48];
    z[44]=z[11]*z[44];
    z[38]= - z[57] + n<T>(11,2)*z[38];
    z[38]=z[9]*z[38];
    z[46]=n<T>(9,2) - z[120];
    z[46]=3*z[46] - n<T>(1,2)*z[68];
    z[46]=z[4]*z[46];
    z[48]=z[9]*z[13];
    z[48]=n<T>(7,12) + z[48];
    z[48]=z[7]*z[48];
    z[31]=n<T>(1,3)*z[10] - z[31];
    z[31]=z[11]*z[31];
    z[31]= - n<T>(7,12) + z[31];
    z[31]=z[8]*z[31];
    z[16]=z[18] + z[16] + z[19] + z[34] + z[43] + z[24] + z[46] + z[31]
    + z[44] + z[48] + z[38] + z[98] - 23*z[13];
    z[16]=z[1]*z[16];
    z[18]=18*z[5];
    z[19]=11*z[4] - z[18];
    z[19]=z[6]*z[19];
    z[19]=z[19] - z[49] + 22*z[52];
    z[19]=z[6]*z[19];
    z[24]=z[72]*z[2];
    z[31]=z[24]*z[52];
    z[19]=z[19] + 9*z[31];
    z[19]=z[2]*z[19];
    z[34]= - static_cast<T>(14)+ 11*z[106];
    z[34]=z[6]*z[34];
    z[34]=z[34] + 19*z[4] - n<T>(185,4)*z[5];
    z[34]=z[6]*z[34];
    z[19]=z[19] + z[34] - z[49] + 10*z[52];
    z[19]=z[2]*z[19];
    z[34]=z[102] + z[71];
    z[34]=z[14]*z[34];
    z[38]=z[25] - z[109];
    z[38]=z[6]*z[38];
    z[38]=2*z[38] - static_cast<T>(20)+ 29*z[106];
    z[38]=z[6]*z[38];
    z[43]=static_cast<T>(11)+ 4*z[68];
    z[43]=z[4]*z[43];
    z[44]=z[17]*z[4];
    z[46]=z[44] + n<T>(7,2)*z[10];
    z[46]=z[46]*z[4];
    z[48]= - n<T>(81,4) - z[46];
    z[48]=z[5]*z[48];
    z[19]=z[19] + z[38] + z[34] + z[43] + z[48];
    z[19]=z[2]*z[19];
    z[34]=z[84]*z[5];
    z[38]=n<T>(3,2)*z[112] - z[17] + z[34];
    z[38]=z[14]*z[38];
    z[38]=z[38] - z[104] + z[103];
    z[38]=z[14]*z[38];
    z[43]=2*z[44] + n<T>(23,2)*z[9] + 5*z[10];
    z[43]=z[5]*z[43];
    z[48]=5*z[9] - n<T>(21,4)*z[109];
    z[48]=z[6]*z[48];
    z[19]=z[19] + z[48] + z[38] + z[43] - static_cast<T>(10)- z[46];
    z[19]=z[2]*z[19];
    z[38]= - z[76] + z[5];
    z[38]=z[6]*z[38];
    z[38]= - 7*z[52] + 5*z[38];
    z[38]=z[6]*z[38];
    z[31]=z[38] - 2*z[31];
    z[31]=z[2]*z[31];
    z[38]=static_cast<T>(1)- z[106];
    z[38]=z[38]*z[63];
    z[18]=z[38] - n<T>(11,2)*z[4] + z[18];
    z[18]=z[6]*z[18];
    z[18]=z[31] - 5*z[52] + z[18];
    z[18]=z[2]*z[18];
    z[31]= - z[41] + z[109];
    z[31]=z[6]*z[31];
    z[31]=z[31] + static_cast<T>(9)- n<T>(29,2)*z[106];
    z[31]=z[6]*z[31];
    z[38]=static_cast<T>(13)+ z[69];
    z[38]=z[5]*z[38];
    z[18]=z[18] + z[31] - z[96] + z[38];
    z[18]=z[2]*z[18];
    z[31]= - z[112] + 3*z[17] - z[34];
    z[31]=z[31]*z[37];
    z[34]= - z[17]*z[39];
    z[31]=z[31] + z[10] + z[34];
    z[31]=z[14]*z[31];
    z[34]= - n<T>(21,2)*z[9] - 8*z[10];
    z[34]=z[5]*z[34];
    z[38]= - z[9] + z[109];
    z[38]=z[6]*z[38];
    z[18]=z[18] + n<T>(7,2)*z[38] + z[31] + z[34] + static_cast<T>(5)- z[69];
    z[18]=z[2]*z[18];
    z[31]=npow(z[10],4);
    z[34]=z[31]*z[5];
    z[38]=z[31]*z[14];
    z[41]= - z[38] + n<T>(9,2)*z[84] - z[34];
    z[41]=z[14]*z[41];
    z[43]=z[39]*z[84];
    z[43]=z[43] - z[17];
    z[41]=z[41] - z[43];
    z[41]=z[14]*z[41];
    z[46]=13*z[9];
    z[48]=z[46] + z[104];
    z[48]=z[10]*z[48];
    z[48]=5*z[22] + z[48];
    z[48]=z[48]*z[39];
    z[50]= - z[9] + z[83];
    z[18]=z[18] + z[41] + 2*z[50] + z[48];
    z[18]=z[2]*z[18];
    z[41]= - z[9] + z[66];
    z[41]=z[10]*z[41];
    z[41]= - z[126] + z[41];
    z[41]=z[41]*z[102];
    z[39]= - z[37] - z[39];
    z[39]=z[39]*npow(z[10],5);
    z[31]=3*z[31] + z[39];
    z[31]=z[14]*z[31];
    z[31]= - n<T>(1,2)*z[84] + z[31];
    z[31]=z[14]*z[31];
    z[39]= - z[25] - z[10];
    z[39]=z[10]*z[39];
    z[18]=z[18] + z[31] + z[39] + z[41];
    z[18]=z[3]*z[18];
    z[31]=n<T>(3,2)*z[38] - 4*z[84] + z[34];
    z[31]=z[14]*z[31];
    z[31]=z[31] - z[43];
    z[31]=z[14]*z[31];
    z[34]= - z[25] - z[45];
    z[34]=z[10]*z[34];
    z[22]= - n<T>(7,4)*z[22] + z[34];
    z[22]=z[5]*z[22];
    z[18]=z[18] + z[19] + z[31] + z[22] + z[44] + z[25] - z[66];
    z[18]=z[3]*z[18];
    z[19]= - z[49] + z[20];
    z[20]=2*z[2];
    z[19]=z[19]*z[20];
    z[19]=z[19] - n<T>(3,2)*z[6] + n<T>(13,4)*z[4] + z[14];
    z[19]=z[6]*z[19];
    z[22]= - z[47] - 5*z[4];
    z[22]=z[4]*z[22];
    z[31]= - z[7] - z[75];
    z[31]=z[14]*z[31];
    z[19]=z[22] + z[31] + z[19];
    z[19]=z[2]*z[19];
    z[22]= - n<T>(13,2) + 2*z[68];
    z[22]=z[4]*z[22];
    z[31]= - z[10] - n<T>(3,2)*z[107];
    z[31]=z[14]*z[31];
    z[31]=static_cast<T>(1)+ z[31];
    z[31]=z[14]*z[31];
    z[19]=z[19] - n<T>(7,4)*z[6] + 3*z[31] - n<T>(1,4)*z[7] + z[22];
    z[19]=z[2]*z[19];
    z[22]= - z[45] + 2*z[107];
    z[31]=z[14]*z[22];
    z[31]= - n<T>(1,2) + z[31];
    z[31]=z[14]*z[31];
    z[34]= - z[86] + z[6];
    z[34]=z[6]*z[34];
    z[24]= - z[4]*z[24];
    z[24]=z[34] + z[24];
    z[24]=z[24]*z[36];
    z[24]=z[24] + n<T>(3,4)*z[6] + n<T>(15,4)*z[4] + z[31];
    z[24]=z[2]*z[24];
    z[31]= - n<T>(15,2)*z[17] + 4*z[112];
    z[31]=z[14]*z[31];
    z[31]= - z[66] + z[31];
    z[31]=z[14]*z[31];
    z[34]= - static_cast<T>(13)+ z[35];
    z[24]=z[24] + n<T>(1,4)*z[34] + z[31];
    z[24]=z[2]*z[24];
    z[31]= - 3*z[84] + z[38];
    z[31]=z[31]*z[75];
    z[31]=z[17] + z[31];
    z[31]=z[14]*z[31];
    z[24]=z[24] - n<T>(7,4)*z[10] + z[31];
    z[24]=z[3]*z[24];
    z[23]=z[23]*z[42];
    z[31]=7*z[17] - 9*z[112];
    z[31]=z[14]*z[31];
    z[31]=z[10] + z[31];
    z[31]=z[31]*z[37];
    z[34]=z[9]*z[42];
    z[34]=n<T>(1,4) + z[34];
    z[35]= - n<T>(13,4)*z[10] + z[44];
    z[35]=z[4]*z[35];
    z[19]=z[24] + z[19] + 6*z[23] + z[31] + 9*z[34] + z[35];
    z[19]=z[3]*z[19];
    z[24]=static_cast<T>(5)+ n<T>(9,2)*z[71];
    z[24]=z[24]*z[32];
    z[31]= - z[6] + z[7];
    z[31]=z[49]*z[31];
    z[34]=z[7]*z[32];
    z[31]=z[34] + z[31];
    z[31]=z[2]*z[31];
    z[34]=n<T>(13,2)*z[7];
    z[35]=z[34] - z[4];
    z[35]=z[4]*z[35];
    z[33]= - z[33] + z[14];
    z[33]=z[6]*z[33];
    z[24]=z[31] + z[33] + z[24] - 4*z[40] + z[35];
    z[24]=z[2]*z[24];
    z[31]=n<T>(11,2)*z[10] - 6*z[107];
    z[31]=z[31]*z[32];
    z[33]=n<T>(1,2) - z[127];
    z[33]=z[33]*z[32];
    z[35]= - z[7]*z[86];
    z[33]=z[35] + z[33];
    z[33]=z[2]*z[33];
    z[31]=z[33] - z[96] + z[31];
    z[31]=z[2]*z[31];
    z[33]=5*z[17] - 3*z[112];
    z[33]=z[14]*z[33];
    z[33]= - z[66] + z[33];
    z[33]=z[14]*z[33];
    z[23]=z[31] - z[23] - z[29] + z[33];
    z[23]=z[3]*z[23];
    z[17]=z[8]*z[17];
    z[17]= - z[83] + z[17];
    z[17]=z[4]*z[17];
    z[17]=n<T>(19,2) + z[17];
    z[17]=z[4]*z[17];
    z[29]=z[61] - z[91];
    z[29]=z[9]*z[29];
    z[29]=6*z[42] + z[29];
    z[29]=z[29]*z[73];
    z[31]=z[10] + n<T>(9,2)*z[107];
    z[31]=z[14]*z[31];
    z[31]= - static_cast<T>(1)+ z[31];
    z[31]=z[14]*z[31];
    z[17]=z[23] + z[24] + z[29] + z[31] - 7*z[8] + z[17];
    z[17]=z[3]*z[17];
    z[23]=z[4]*z[7];
    z[23]= - z[40] + z[23];
    z[24]= - n<T>(3,2) + 4*z[71];
    z[24]=z[24]*z[32];
    z[20]=z[26]*z[20];
    z[20]=z[20] + n<T>(3,2)*z[23] + z[24];
    z[20]=z[2]*z[20];
    z[23]=static_cast<T>(3)- z[120];
    z[23]=z[4]*z[23];
    z[23]= - z[82] + z[23];
    z[22]=z[22]*z[32];
    z[20]=z[20] + n<T>(3,4)*z[23] + z[22];
    z[20]=z[3]*z[20];
    z[22]= - static_cast<T>(1)- z[71];
    z[23]= - z[47] - n<T>(3,2)*z[14];
    z[23]=z[2]*z[23];
    z[22]=z[23] + n<T>(3,2)*z[22];
    z[22]=z[32]*z[22];
    z[23]=13*z[7];
    z[24]=z[23] + z[30];
    z[24]=z[24]*z[56];
    z[29]=static_cast<T>(13)+ n<T>(5,2)*z[120];
    z[29]=z[8]*z[29];
    z[23]= - z[23] + z[29];
    z[29]=static_cast<T>(1)- 2*z[120];
    z[29]=z[4]*z[29];
    z[23]=n<T>(1,2)*z[23] + z[29];
    z[23]=z[4]*z[23];
    z[20]=z[20] + z[24] + z[23] + z[22];
    z[20]=z[3]*z[20];
    z[22]=z[8]*z[77];
    z[23]=static_cast<T>(1)+ z[62];
    z[23]=z[8]*z[23];
    z[23]= - z[7] + z[23];
    z[23]=z[4]*z[23];
    z[22]=z[22] + z[23];
    z[23]= - z[10]*z[26];
    z[22]=3*z[22] + z[23];
    z[23]= - z[2]*z[27];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[2]*z[26]*z[23];
    z[22]=n<T>(1,2)*z[22] + z[23];
    z[22]=z[3]*z[22];
    z[23]= - z[34] - z[94];
    z[23]=z[8]*z[23];
    z[23]=z[23] + z[74];
    z[23]=z[4]*z[23];
    z[22]=z[23] + z[22];
    z[22]=z[3]*z[22];
    z[23]=z[12]*z[77]*z[64]*z[74];
    z[22]=z[22] - n<T>(3,2)*z[23];
    z[22]=z[12]*z[22];
    z[20]=z[20] + z[22];
    z[20]=z[12]*z[20];
    z[17]=z[17] + z[20];
    z[17]=z[12]*z[17];
    z[20]=z[14] - z[7];
    z[17]=z[17] + z[19] - n<T>(19,4)*z[6] + n<T>(71,4)*z[4] - z[28] - z[101] - 
    z[98] + z[115] - n<T>(9,2)*z[20];
    z[17]=z[12]*z[17];
    z[19]=z[27] + z[88] + z[79];
    z[19]=n<T>(49,4)*z[6] - 5*z[14] - z[122] + 3*z[19] - z[21];
    z[19]=z[2]*z[19];
    z[20]= - z[78]*z[46];
    z[21]= - z[25] + z[10];
    z[21]=z[21]*z[101];
    z[22]=29*z[9] - 21*z[10];
    z[22]=z[5]*z[22];

    r += n<T>(51,4) + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + n<T>(1,4)
      *z[22] + n<T>(7,2)*z[71] - n<T>(71,4)*z[73] + z[123] - z[124];
 
    return r;
}

template double qg_2lNLC_r89(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r89(const std::array<dd_real,31>&);
#endif
