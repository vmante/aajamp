#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r665(const std::array<T,31>& k) {
  T z[60];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[5];
    z[7]=k[2];
    z[8]=k[4];
    z[9]=k[6];
    z[10]=k[7];
    z[11]=k[9];
    z[12]=k[3];
    z[13]=k[13];
    z[14]=npow(z[7],2);
    z[15]=z[14]*z[3];
    z[16]=3*z[7];
    z[17]=z[6]*z[4];
    z[18]= - static_cast<T>(15)- 16*z[17];
    z[18]=z[6]*z[18];
    z[19]=z[5]*z[14];
    z[18]=z[19] + z[18] - z[16] - 4*z[15];
    z[18]=z[5]*z[18];
    z[19]=2*z[5];
    z[20]=npow(z[7],3);
    z[21]=z[20]*z[19];
    z[22]=z[17] + 1;
    z[23]=z[22]*z[6];
    z[23]=z[23] + z[7];
    z[23]=z[23]*z[6];
    z[24]=2*z[14];
    z[21]=z[21] + z[24] + z[23];
    z[21]=z[5]*z[21];
    z[25]=2*z[10];
    z[26]= - z[17]*z[25];
    z[26]= - z[4] + z[26];
    z[27]=npow(z[6],2);
    z[26]=z[26]*z[27];
    z[28]=2*z[8];
    z[29]=z[28]*z[4];
    z[30]=z[17] + static_cast<T>(3)+ z[29];
    z[30]=z[6]*z[30];
    z[30]= - z[8] + z[30];
    z[30]=z[2]*z[6]*z[30];
    z[31]= - z[7] + z[8];
    z[21]=z[30] + z[21] + 2*z[31] + z[26];
    z[21]=z[2]*z[21];
    z[26]=z[3]*z[7];
    z[30]=z[7]*z[11];
    z[31]=static_cast<T>(2)+ z[30];
    z[31]=z[31]*z[26];
    z[32]=3*z[13];
    z[33]=z[32] - z[10];
    z[33]=2*z[33];
    z[34]=z[17]*z[10];
    z[35]= - 3*z[34] - z[4] - z[33];
    z[35]=z[6]*z[35];
    z[36]=z[4]*z[8];
    z[37]=z[8]*z[3];
    z[38]=2*z[7];
    z[39]=z[11]*z[38];
    z[18]=z[21] + z[18] + z[35] + 6*z[36] - z[37] + z[31] - static_cast<T>(9)+ z[39];
    z[18]=z[2]*z[18];
    z[21]=npow(z[9],2);
    z[31]= - z[21]*z[24];
    z[35]= - static_cast<T>(2)- 5*z[17];
    z[35]=z[6]*z[35];
    z[35]=z[15] + z[35];
    z[35]=z[5]*z[35];
    z[31]=z[35] - 26*z[17] + z[31] - static_cast<T>(16)+ z[26];
    z[31]=z[5]*z[31];
    z[35]=static_cast<T>(1)+ 2*z[17];
    z[29]= - z[29] - z[35];
    z[29]=z[6]*z[29];
    z[29]= - z[7] + z[29];
    z[23]= - z[14] - z[23];
    z[23]=z[6]*z[23];
    z[23]= - z[20] + z[23];
    z[23]=z[5]*z[23];
    z[39]=static_cast<T>(2)+ 3*z[17];
    z[39]=z[6]*z[39];
    z[39]=z[7] + z[39];
    z[39]=z[6]*z[39];
    z[23]=z[39] + z[23];
    z[23]=z[5]*z[23];
    z[23]=2*z[29] + z[23];
    z[23]=z[2]*z[23];
    z[29]=2*z[6];
    z[39]=z[29] + z[38];
    z[39]=z[21]*z[39];
    z[40]=5*z[4];
    z[39]=z[40] + z[39];
    z[39]=z[6]*z[39];
    z[41]=4*z[17];
    z[42]=z[41] + 3;
    z[42]=z[42]*z[6];
    z[42]=z[42] + z[38];
    z[42]=z[42]*z[6];
    z[42]=z[42] + z[14];
    z[42]=z[42]*z[5];
    z[43]= - static_cast<T>(3)- 7*z[17];
    z[43]=z[6]*z[43];
    z[43]=z[42] - z[7] + z[43];
    z[43]=z[5]*z[43];
    z[23]=z[23] + z[39] + z[43];
    z[23]=z[2]*z[23];
    z[33]=z[40] + z[33];
    z[39]=z[21]*z[8];
    z[33]=z[39]*z[33];
    z[43]=3*z[4];
    z[44]= - z[43] - 12*z[13] - z[10];
    z[44]=z[6]*z[21]*z[44];
    z[33]=z[44] + z[33];
    z[33]=z[6]*z[33];
    z[44]=npow(z[11],2);
    z[45]=3*z[44];
    z[46]=z[45]*z[7];
    z[47]= - z[46] + 4*z[11];
    z[48]=z[47]*z[38];
    z[48]=static_cast<T>(1)+ z[48];
    z[48]=z[3]*z[48];
    z[49]=z[44]*z[7];
    z[50]=3*z[37];
    z[51]=static_cast<T>(5)+ z[50];
    z[51]=z[4]*z[51];
    z[23]=z[23] + z[31] + z[33] + z[51] + 6*z[49] + z[48];
    z[23]=z[2]*z[23];
    z[31]=z[8]*z[40];
    z[31]=z[31] + z[17];
    z[33]=npow(z[6],3);
    z[48]=npow(z[9],4);
    z[31]=z[33]*z[48]*z[31];
    z[35]=z[35]*z[6];
    z[49]=z[7] + 3*z[35];
    z[51]= - z[5]*z[49];
    z[51]=z[51] + 12*z[17] + static_cast<T>(4)+ z[26];
    z[51]=z[5]*z[51];
    z[52]= - static_cast<T>(5)- z[37];
    z[52]=z[4]*z[52];
    z[31]=z[51] + z[31] - z[3] + z[52];
    z[31]=z[2]*z[31];
    z[51]=3*z[3];
    z[52]=z[10]*z[48]*npow(z[6],4);
    z[52]=z[51] - 4*z[52];
    z[52]=z[4]*z[52];
    z[41]=z[41] + 1;
    z[41]=z[41]*z[5];
    z[53]=3*z[41] - z[51] - 17*z[4];
    z[53]=z[5]*z[53];
    z[31]=z[31] + z[53] + z[52];
    z[31]=z[2]*z[31];
    z[52]=npow(z[5],2);
    z[40]= - z[52]*z[40];
    z[31]=z[40] + z[31];
    z[31]=z[2]*z[31];
    z[40]=npow(z[8],3);
    z[48]=z[48]*z[40];
    z[53]=z[3] - z[48];
    z[53]=z[8]*z[53];
    z[48]= - z[6]*z[48];
    z[48]=z[48] + static_cast<T>(1)+ z[53];
    z[48]=z[52]*z[48]*npow(z[10],2);
    z[52]=z[5]*z[4];
    z[53]=z[4]*z[3];
    z[52]=z[52] - z[53];
    z[52]=z[52]*z[5];
    z[41]= - z[41] + z[3] + 6*z[4];
    z[41]=z[5]*z[41];
    z[41]= - z[53] + z[41];
    z[41]=z[2]*z[41];
    z[41]=4*z[52] + z[41];
    z[41]=z[41]*npow(z[2],2);
    z[52]= - z[1]*npow(z[2],3)*z[52];
    z[41]=z[41] + z[52];
    z[41]=z[1]*z[41];
    z[31]=z[41] + 2*z[48] + z[31];
    z[31]=z[1]*z[31];
    z[41]=npow(z[8],2);
    z[48]=npow(z[9],3);
    z[52]=z[41]*z[48];
    z[53]=2*z[3];
    z[54]=z[53] + z[52];
    z[54]=z[8]*z[54];
    z[54]=static_cast<T>(2)+ z[54];
    z[55]=z[10]*z[8];
    z[56]= - static_cast<T>(2)- z[50];
    z[56]=z[56]*z[55];
    z[57]=z[52] + z[10];
    z[57]=z[6]*z[57];
    z[54]=z[57] + 2*z[54] + z[56];
    z[54]=z[19]*z[10]*z[54];
    z[52]= - z[51] - z[52];
    z[52]=z[8]*z[52];
    z[52]= - static_cast<T>(1)+ z[52];
    z[52]=z[10]*z[52];
    z[56]=4*z[3];
    z[52]=z[56] + z[52];
    z[52]=z[52]*z[25];
    z[57]=6*z[17];
    z[58]= - z[44]*z[57];
    z[59]= - 15*z[11] + z[25];
    z[59]=z[4]*z[59];
    z[52]=z[54] + z[58] + z[52] + z[59];
    z[52]=z[5]*z[52];
    z[35]= - z[42] + 5*z[35] + z[38] + z[15];
    z[35]=z[5]*z[35];
    z[16]=z[16] + z[6];
    z[16]=z[6]*z[48]*z[16];
    z[16]= - 8*z[4] + z[16];
    z[16]=z[6]*z[16];
    z[36]= - static_cast<T>(1)- z[36];
    z[16]=z[35] + 4*z[36] + z[16];
    z[16]=z[2]*z[16];
    z[24]=z[48]*z[24];
    z[24]=z[24] - 21*z[4];
    z[24]=z[6]*z[24];
    z[19]=z[49]*z[19];
    z[35]= - z[3]*z[38];
    z[19]=z[19] + z[24] - static_cast<T>(7)+ z[35];
    z[19]=z[5]*z[19];
    z[24]=z[33]*z[48];
    z[35]= - z[10] + z[4];
    z[35]=z[35]*z[24];
    z[16]=z[16] + z[19] + 9*z[4] + 4*z[35];
    z[16]=z[2]*z[16];
    z[19]= - z[20]*z[48];
    z[20]=z[14]*z[48];
    z[35]=10*z[4];
    z[20]=z[20] - z[35];
    z[20]=z[6]*z[20];
    z[19]=z[20] - static_cast<T>(2)+ z[19];
    z[19]=z[5]*z[19];
    z[19]=z[19] - z[51] - z[35];
    z[19]=z[5]*z[19];
    z[20]= - z[10]*z[24];
    z[20]=z[51] + z[20];
    z[20]=z[4]*z[20];
    z[16]=z[16] + z[19] + z[20];
    z[16]=z[2]*z[16];
    z[16]=z[31] + z[52] + z[16];
    z[16]=z[1]*z[16];
    z[19]= - static_cast<T>(3)- 4*z[37];
    z[19]=z[19]*z[28];
    z[20]=z[10]*z[40]*z[51];
    z[19]=z[19] + z[20];
    z[19]=z[19]*z[25];
    z[14]= - z[14]*z[21];
    z[20]= - z[7]*z[21];
    z[20]= - z[39] + 11*z[3] + z[20];
    z[20]=z[8]*z[20];
    z[24]=static_cast<T>(1)- 3*z[55];
    z[24]=z[24]*z[25];
    z[24]=z[39] + z[24];
    z[24]=z[6]*z[24];
    z[14]=z[24] + z[19] + z[20] + static_cast<T>(5)+ z[14];
    z[14]=z[5]*z[14];
    z[19]= - 6*z[3] + z[39];
    z[19]=z[19]*z[28];
    z[20]=static_cast<T>(1)+ 6*z[37];
    z[20]=z[20]*z[55];
    z[19]=z[20] + static_cast<T>(3)+ z[19];
    z[19]=z[10]*z[19];
    z[20]= - z[10]*z[39];
    z[20]= - z[44] + z[20];
    z[24]= - z[17]*z[45];
    z[31]= - 6*z[11] + z[10];
    z[31]=z[4]*z[31];
    z[20]=z[24] + 3*z[20] + 2*z[31];
    z[20]=z[6]*z[20];
    z[19]=z[20] + z[19] - 11*z[11] + z[56];
    z[14]=2*z[19] + z[14];
    z[14]=z[5]*z[14];
    z[19]=z[10]*z[37];
    z[19]= - z[53] + z[19];
    z[19]=z[19]*z[25];
    z[20]=z[3]*z[47];
    z[19]=z[19] + z[45] + z[20];
    z[20]=z[12]*z[21];
    z[20]=2*z[13] + z[20];
    z[20]=z[10]*z[20];
    z[20]=2*z[44] + z[20];
    z[20]=z[20]*z[43];
    z[21]=z[21]*z[34];
    z[20]=z[20] - 8*z[21];
    z[20]=z[6]*z[20];
    z[21]=5*z[11] + z[3];
    z[21]=3*z[21] - 10*z[10];
    z[21]=z[4]*z[21];
    z[14]=z[16] + z[23] + z[14] + z[20] + 2*z[19] + z[21];
    z[14]=z[1]*z[14];
    z[16]=static_cast<T>(2)- z[37];
    z[16]=z[10]*z[16];
    z[16]=z[56] + z[16];
    z[16]=z[25]*z[40]*z[16];
    z[19]= - static_cast<T>(1)+ z[26];
    z[19]=6*z[19] - 11*z[37];
    z[19]=z[8]*z[19];
    z[20]=z[41]*z[10];
    z[21]= - z[28] + 3*z[20];
    z[21]=z[21]*z[25];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[6]*z[21];
    z[15]=z[21] + z[16] - z[15] + z[19];
    z[15]=z[5]*z[15];
    z[16]=static_cast<T>(1)- z[50];
    z[16]=z[16]*z[20];
    z[19]=2*z[37];
    z[20]= - static_cast<T>(1)+ z[19];
    z[20]=z[8]*z[20];
    z[16]=4*z[20] + z[16];
    z[16]=z[16]*z[25];
    z[20]=9*z[11];
    z[21]= - z[20] + z[25];
    z[17]=z[21]*z[17];
    z[17]=z[17] - z[20] + 8*z[10];
    z[17]=z[6]*z[17];
    z[15]=z[15] + z[17] + z[16] - 10*z[37] - 4*z[26] + static_cast<T>(11)- 7*z[30];
    z[15]=z[5]*z[15];
    z[16]=z[10]*z[13];
    z[16]=z[44] + z[16];
    z[17]=z[11]*z[12];
    z[20]=3*z[17];
    z[21]=static_cast<T>(8)+ z[20];
    z[21]=z[11]*z[21];
    z[23]=z[25]*z[12];
    z[24]= - static_cast<T>(15)- z[23];
    z[24]=z[10]*z[24];
    z[21]=2*z[21] + z[24];
    z[21]=z[4]*z[21];
    z[24]=z[13]*z[25];
    z[24]=z[44] + z[24];
    z[24]=z[24]*z[57];
    z[16]=z[24] + 6*z[16] + z[21];
    z[16]=z[6]*z[16];
    z[21]=static_cast<T>(5)+ z[20];
    z[21]=z[11]*z[21];
    z[21]=z[21] + z[46];
    z[20]= - static_cast<T>(2)+ z[20];
    z[20]=z[11]*z[20];
    z[20]=z[20] + z[46];
    z[20]=z[20]*z[38];
    z[20]=z[20] + static_cast<T>(3)- 5*z[17];
    z[20]=z[3]*z[20];
    z[24]=z[3]*z[12];
    z[24]=z[24] - z[19];
    z[24]=z[8]*z[24];
    z[24]= - z[12] + z[24];
    z[24]=z[10]*z[24];
    z[19]= - static_cast<T>(3)+ z[19];
    z[19]=2*z[19] + z[24];
    z[19]=z[19]*z[25];
    z[17]=z[23] + static_cast<T>(9)+ z[17];
    z[17]=z[4]*z[17];
    z[14]=z[14] + z[18] + z[15] + z[16] + z[17] + z[19] + 2*z[21] + 
    z[20];
    z[14]=z[1]*z[14];
    z[15]=z[10]*z[9];
    z[16]=z[15]*z[22];
    z[17]=2*z[9];
    z[18]=z[4]*z[17];
    z[18]=z[18] - z[16];
    z[18]=z[6]*z[18];
    z[19]=z[15]*z[8];
    z[18]=z[18] + z[9] + z[19];
    z[18]=z[6]*z[18];
    z[20]=z[8]*z[9];
    z[21]=z[9]*z[7];
    z[18]=z[18] + z[21] - z[20];
    z[18]=z[18]*z[27];
    z[21]=z[2]*z[33]*z[20]*z[22];
    z[18]=z[18] + z[21];
    z[18]=z[2]*z[18];
    z[21]=z[6]*z[16];
    z[22]=z[4]*z[20];
    z[19]=z[21] + z[22] + z[9] - z[19];
    z[19]=z[19]*z[29];
    z[21]=z[28]*z[9];
    z[19]=z[19] - static_cast<T>(9)+ z[21];
    z[19]=z[6]*z[19];
    z[18]=2*z[18] + z[19] - 5*z[7] + 8*z[8];
    z[18]=z[2]*z[18];
    z[15]=z[12]*z[15];
    z[15]=z[17] + z[15];
    z[15]=z[4]*z[15];
    z[15]=z[15] - z[16];
    z[15]=z[6]*z[15];
    z[16]=z[9]*z[12];
    z[19]=z[20] - static_cast<T>(4)+ z[16];
    z[19]=z[10]*z[19];
    z[15]=z[15] + 4*z[4] + z[19] + z[32] + z[9];
    z[15]=z[15]*z[29];
    z[19]=z[12] + z[7];
    z[17]=z[19]*z[17];
    z[16]=static_cast<T>(2)- z[16];
    z[16]=z[16]*z[28];
    z[16]=z[16] + 3*z[12];
    z[16]=z[10]*z[16];
    z[19]= - z[7] - z[28] + 3*z[6];
    z[19]=z[5]*z[19];

    r += static_cast<T>(3)+ z[14] + z[15] + z[16] + z[17] + z[18] + z[19] - z[21];
 
    return r;
}

template double qg_2lNLC_r665(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r665(const std::array<dd_real,31>&);
#endif
