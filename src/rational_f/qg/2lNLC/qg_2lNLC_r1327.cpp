#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1327(const std::array<T,31>& k) {
  T z[112];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[12];
    z[7]=k[15];
    z[8]=k[14];
    z[9]=k[2];
    z[10]=k[5];
    z[11]=k[6];
    z[12]=k[3];
    z[13]=k[13];
    z[14]=k[4];
    z[15]=k[7];
    z[16]=npow(z[8],2);
    z[17]=n<T>(5,2)*z[6];
    z[18]= - n<T>(2,3)*z[15] - z[17];
    z[18]=z[18]*z[16];
    z[19]=npow(z[13],2);
    z[20]=2*z[19];
    z[21]= - z[15] + z[13];
    z[21]=z[21]*z[20];
    z[22]=n<T>(1,3)*z[15];
    z[23]=npow(z[8],3);
    z[24]=z[23]*z[22];
    z[25]=npow(z[13],3);
    z[26]=z[25]*z[15];
    z[24]=z[24] - z[26];
    z[26]=2*z[10];
    z[24]=z[24]*z[26];
    z[18]=z[24] + z[18] + z[21];
    z[18]=z[10]*z[18];
    z[21]=n<T>(3,2)*z[6];
    z[24]= - n<T>(5,2)*z[8] + z[22] + z[21];
    z[24]=z[8]*z[24];
    z[27]=n<T>(9,2)*z[7];
    z[28]=z[27] - z[15];
    z[29]=z[13]*z[12];
    z[30]= - n<T>(3,2) + 2*z[29];
    z[31]=z[13]*z[30];
    z[31]=z[31] + z[28];
    z[31]=z[13]*z[31];
    z[32]=z[2] + z[7];
    z[33]=z[32]*z[5];
    z[34]=z[6]*z[7];
    z[18]=z[18] + z[31] + z[24] - n<T>(3,2)*z[34] - 8*z[33];
    z[18]=z[10]*z[18];
    z[24]=npow(z[12],2);
    z[31]=npow(z[11],3);
    z[35]=z[24]*z[31];
    z[36]=npow(z[9],2);
    z[37]=z[31]*z[36];
    z[38]=2*z[13];
    z[39]=z[24]*z[13];
    z[40]=z[39] - z[12];
    z[41]=z[13]*z[40];
    z[41]= - static_cast<T>(1)+ z[41];
    z[41]=z[41]*z[38];
    z[42]=3*z[6];
    z[43]=z[8]*z[9];
    z[44]=3*z[43];
    z[45]=z[44] - n<T>(1,2);
    z[46]=z[8]*z[45];
    z[18]=z[18] + z[41] - z[35] + z[46] - z[37] - z[42] + 19*z[2];
    z[18]=z[10]*z[18];
    z[41]=3*z[13];
    z[46]=static_cast<T>(1)- z[29];
    z[46]=z[46]*z[41];
    z[47]=z[16]*z[10];
    z[48]=z[47]*z[6];
    z[49]=z[6] - z[8];
    z[49]=z[8]*z[49];
    z[49]=z[49] - z[48];
    z[49]=z[10]*z[49];
    z[50]=2*z[43];
    z[51]=static_cast<T>(1)+ z[50];
    z[51]=z[8]*z[51];
    z[46]=z[49] + z[46] - z[6] + z[51];
    z[46]=z[10]*z[46];
    z[49]=z[12]*z[6];
    z[51]=4*z[49];
    z[52]=z[41]*z[12];
    z[53]=z[36]*z[8];
    z[54]=z[53] + z[9];
    z[55]= - z[8]*z[54];
    z[46]=z[46] - z[52] + z[51] + static_cast<T>(2)+ z[55];
    z[46]=z[10]*z[46];
    z[55]=z[12]*z[4];
    z[56]=6*z[55];
    z[57]=z[9]*z[4];
    z[58]=9*z[57];
    z[59]=z[56] + static_cast<T>(7)+ z[58];
    z[59]=z[12]*z[59];
    z[60]=npow(z[12],3);
    z[61]=3*z[4];
    z[62]= - z[60]*z[61];
    z[63]=npow(z[12],4);
    z[64]=z[63]*z[13];
    z[65]=z[4]*z[64];
    z[62]=z[62] + z[65];
    z[62]=z[62]*z[41];
    z[65]= - static_cast<T>(11)+ z[58];
    z[65]=z[9]*z[65];
    z[46]=z[46] + z[62] + z[65] + z[59];
    z[46]=z[3]*z[46];
    z[59]=3*z[12];
    z[62]=z[59]*z[31];
    z[65]=z[31]*z[9];
    z[66]= - z[65] + z[62];
    z[66]=z[12]*z[66];
    z[67]=2*z[6];
    z[68]=z[67] + n<T>(7,2)*z[4];
    z[66]=z[66] + 3*z[68] - n<T>(1,3)*z[37];
    z[66]=z[12]*z[66];
    z[68]=z[61]*z[12];
    z[69]=static_cast<T>(1)- z[68];
    z[69]=z[69]*z[59];
    z[70]=n<T>(9,2)*z[55];
    z[71]= - static_cast<T>(4)+ z[70];
    z[71]=z[71]*z[24];
    z[72]=z[60]*z[13];
    z[71]=z[71] + 2*z[72];
    z[71]=z[13]*z[71];
    z[69]=z[69] + z[71];
    z[69]=z[13]*z[69];
    z[71]=z[9]*z[2];
    z[73]=n<T>(1,2)*z[71];
    z[74]=z[73] - 1;
    z[75]=3*z[9];
    z[76]= - z[74]*z[75];
    z[77]=n<T>(1,2)*z[53];
    z[76]=z[76] - z[77];
    z[76]=z[8]*z[76];
    z[78]=z[2]*z[4];
    z[79]=z[78]*z[9];
    z[80]=n<T>(9,2)*z[79] + 15*z[4] - 14*z[2];
    z[80]=z[9]*z[80];
    z[18]=z[46] + z[18] + z[69] + z[66] + z[76] - static_cast<T>(27)+ z[80];
    z[18]=z[3]*z[18];
    z[46]=5*z[13];
    z[66]=5*z[15];
    z[69]=z[46] - z[66] + n<T>(3,2)*z[7];
    z[69]=z[13]*z[69];
    z[69]= - z[62] + z[69];
    z[76]=n<T>(1,2)*z[13];
    z[69]=z[69]*z[76];
    z[80]=npow(z[5],2);
    z[81]=z[32]*z[80];
    z[82]=n<T>(1,2)*z[8];
    z[83]=n<T>(9,2)*z[6];
    z[84]= - n<T>(7,3)*z[15] - z[83];
    z[84]=z[84]*z[82];
    z[84]= - 2*z[65] + z[84];
    z[84]=z[8]*z[84];
    z[85]= - z[25]*z[66];
    z[86]=z[15]*z[23];
    z[85]=n<T>(11,3)*z[86] + z[85];
    z[86]=n<T>(1,2)*z[10];
    z[85]=z[85]*z[86];
    z[69]=z[85] + z[69] - 14*z[81] + z[84];
    z[69]=z[10]*z[69];
    z[84]=z[12]*z[19];
    z[28]=n<T>(5,2)*z[84] + n<T>(1,2)*z[28] + 10*z[35];
    z[28]=z[13]*z[28];
    z[84]=n<T>(1,2)*z[6];
    z[85]=z[15] + z[84];
    z[85]= - n<T>(9,4)*z[8] + n<T>(1,2)*z[85] - z[37];
    z[85]=z[8]*z[85];
    z[87]=11*z[7] + 59*z[2];
    z[87]=z[5]*z[87];
    z[28]=z[69] + z[28] + z[85] - n<T>(15,4)*z[34] + z[87];
    z[28]=z[10]*z[28];
    z[69]= - n<T>(15,4)*z[4] - 14*z[35];
    z[69]=z[12]*z[69];
    z[85]=5*z[39];
    z[70]= - static_cast<T>(5)+ z[70];
    z[70]=z[12]*z[70];
    z[70]=z[70] + z[85];
    z[70]=z[70]*z[76];
    z[69]=z[70] + n<T>(1,2) + z[69];
    z[69]=z[13]*z[69];
    z[70]=z[61] + z[6];
    z[87]=z[70]*z[59];
    z[87]=z[87] - 35;
    z[87]=z[5]*z[87];
    z[73]=z[73] - 3;
    z[73]=z[73]*z[43];
    z[88]= - static_cast<T>(13)+ 17*z[71];
    z[88]=n<T>(1,2)*z[88] - z[73];
    z[88]=z[88]*z[82];
    z[89]=n<T>(7,2)*z[6];
    z[90]=12*z[4];
    z[18]=z[18] + z[28] + z[69] + z[88] + n<T>(9,4)*z[79] - n<T>(67,2)*z[2] - 
    z[89] + z[90] + z[87];
    z[18]=z[3]*z[18];
    z[28]=n<T>(1,2)*z[15];
    z[35]=z[38] - z[28] - 31*z[35];
    z[35]=z[35]*z[19];
    z[38]=z[8]*z[15];
    z[65]= - z[65] + n<T>(13,6)*z[38];
    z[65]=z[65]*z[16];
    z[69]=z[13]*z[15];
    z[62]=z[62] - z[69];
    z[20]=z[62]*z[20];
    z[20]=z[65] + z[20];
    z[20]=z[10]*z[20];
    z[62]=z[7] + 7*z[2];
    z[62]=z[62]*z[80];
    z[37]=n<T>(2,3)*z[37];
    z[65]= - z[37] - z[28] - z[6];
    z[65]=z[65]*z[16];
    z[20]=z[20] + z[35] + 4*z[62] + z[65];
    z[20]=z[10]*z[20];
    z[35]=6*z[5];
    z[65]= - z[35] - n<T>(85,2)*z[2] + z[90] + n<T>(7,2)*z[7] - z[6];
    z[65]=z[5]*z[65];
    z[87]=z[84] - z[15];
    z[88]=5*z[2];
    z[91]= - z[88] - z[87];
    z[74]=z[8]*z[74];
    z[74]=n<T>(1,2)*z[91] + z[74];
    z[74]=z[8]*z[74];
    z[91]=z[31]*z[60];
    z[30]=25*z[91] + z[30];
    z[30]=z[13]*z[30];
    z[30]= - z[15] + z[30];
    z[30]=z[13]*z[30];
    z[91]=z[22]*z[7];
    z[92]= - z[15] + z[61];
    z[92]=z[2]*z[92];
    z[18]=z[18] + z[20] + z[30] + z[74] + z[65] + z[92] + z[91] - n<T>(13,4)
   *z[34];
    z[18]=z[3]*z[18];
    z[20]=npow(z[11],4);
    z[30]=z[20]*z[53];
    z[65]= - z[20]*z[24];
    z[74]=n<T>(3,2)*z[13];
    z[92]= - z[7]*z[74];
    z[65]=z[65] + z[92];
    z[65]=z[13]*z[65];
    z[30]=z[65] + 12*z[81] + n<T>(7,3)*z[30];
    z[30]=z[10]*z[30];
    z[65]=3*z[34];
    z[92]=3*z[7];
    z[93]= - z[92] - 29*z[2];
    z[93]=z[5]*z[93];
    z[93]=z[65] + z[93];
    z[94]=z[20]*z[72];
    z[30]=z[30] + 6*z[94] + 2*z[93] + n<T>(5,2)*z[16];
    z[30]=z[10]*z[30];
    z[93]= - z[7] - z[13];
    z[93]=z[93]*z[41];
    z[93]=z[93] + z[16] + z[34] + 2*z[33];
    z[93]=z[10]*z[93];
    z[94]=2*z[9];
    z[95]= - z[16]*z[94];
    z[96]=2*z[2];
    z[97]=z[6] - z[96];
    z[93]=z[93] - z[41] + 3*z[97] + z[95];
    z[93]=z[10]*z[93];
    z[95]= - z[79] - 7*z[4] + z[96];
    z[95]=z[95]*z[75];
    z[97]= - static_cast<T>(1)+ z[71];
    z[97]=z[9]*z[97];
    z[97]=z[97] + z[53];
    z[97]=z[8]*z[97];
    z[70]=z[70]*z[12];
    z[93]=z[93] - 6*z[70] + z[97] + static_cast<T>(25)+ z[95];
    z[93]=z[3]*z[93];
    z[95]= - static_cast<T>(1)+ 2*z[71];
    z[73]= - 3*z[95] + z[73];
    z[73]=z[8]*z[73];
    z[97]=z[20]*z[64];
    z[98]=7*z[6];
    z[99]=z[70]*z[5];
    z[99]=8*z[99];
    z[30]=z[93] + z[30] - 7*z[97] - z[99] + z[73] - 12*z[79] + 58*z[5]
    + 52*z[2] + z[98] - 36*z[4];
    z[30]=z[3]*z[30];
    z[73]=z[16]*z[36];
    z[93]=z[20]*z[73];
    z[97]=z[19]*z[20];
    z[100]=z[24]*z[97];
    z[93]=n<T>(1,6)*z[93] + z[100];
    z[100]=z[9]*z[20]*z[47];
    z[93]=5*z[93] + z[100];
    z[93]=z[10]*z[93];
    z[100]=z[60]*z[97];
    z[93]=z[93] - 7*z[62] - 24*z[100];
    z[93]=z[10]*z[93];
    z[70]=z[70]*z[80];
    z[100]=7*z[34] - 11*z[78];
    z[101]=3*z[8];
    z[102]=n<T>(3,2) - z[71];
    z[102]=z[102]*z[101];
    z[102]=11*z[2] + z[102];
    z[102]=z[102]*z[82];
    z[97]=z[63]*z[97];
    z[103]=205*z[2] - 105*z[4] - 21*z[7] + 13*z[6];
    z[103]=n<T>(1,2)*z[103] + 18*z[5];
    z[103]=z[5]*z[103];
    z[30]=z[30] + z[93] + 19*z[97] - z[70] + z[102] + n<T>(3,2)*z[100] + 
    z[103];
    z[30]=z[3]*z[30];
    z[93]=z[20]*z[23]*z[36];
    z[97]=z[25]*z[20];
    z[100]=n<T>(31,2)*z[97];
    z[102]= - z[24]*z[100];
    z[93]=n<T>(2,3)*z[93] + z[102];
    z[93]=z[10]*z[93];
    z[102]=z[60]*z[97];
    z[93]=31*z[102] + z[93];
    z[93]=z[10]*z[93];
    z[102]=z[61]*z[2];
    z[102]=z[102] - z[34];
    z[103]= - z[6] + 6*z[4];
    z[104]=32*z[2] - 4*z[7] - z[103];
    z[104]=z[5]*z[104];
    z[104]= - n<T>(15,2)*z[102] + z[104];
    z[104]=z[5]*z[104];
    z[100]= - z[63]*z[100];
    z[105]=z[16]*z[2];
    z[30]=z[30] + z[93] + z[100] + z[104] + z[105];
    z[30]=z[3]*z[30];
    z[93]=z[19]*z[92];
    z[93]= - 4*z[81] + z[93];
    z[93]=z[10]*z[93];
    z[100]=z[5]*z[2];
    z[104]=z[7]*z[41];
    z[65]=z[93] + z[104] - z[16] - z[65] + 20*z[100];
    z[65]=z[10]*z[65];
    z[93]=9*z[9];
    z[93]=z[78]*z[93];
    z[100]=static_cast<T>(2)- z[71];
    z[100]=z[100]*z[43];
    z[71]=3*z[71];
    z[100]=z[100] - static_cast<T>(1)+ z[71];
    z[100]=z[8]*z[100];
    z[104]=5*z[6];
    z[106]=24*z[2];
    z[65]=z[65] + z[99] + z[100] + z[93] - 34*z[5] - z[106] - z[104] + 
   33*z[4];
    z[65]=z[3]*z[65];
    z[93]=npow(z[11],5);
    z[99]= - z[93]*z[36]*z[47];
    z[100]=z[93]*z[19];
    z[107]=z[60]*z[100];
    z[99]=z[107] + z[99];
    z[99]=z[10]*z[99];
    z[107]=z[63]*z[100];
    z[99]=z[99] + 3*z[62] - 4*z[107];
    z[99]=z[99]*z[26];
    z[70]=2*z[70];
    z[107]= - 4*z[34] + 9*z[78];
    z[108]=npow(z[12],5);
    z[100]=z[108]*z[100];
    z[109]= - 24*z[5] - 104*z[2] + 81*z[4] + 12*z[7] - 11*z[6];
    z[109]=z[5]*z[109];
    z[110]= - n<T>(5,2) + z[71];
    z[110]=z[8]*z[110];
    z[110]= - n<T>(11,2)*z[2] + z[110];
    z[110]=z[8]*z[110];
    z[65]=z[65] + z[99] + 6*z[100] + z[70] + z[110] + 3*z[107] + z[109];
    z[65]=z[3]*z[65];
    z[99]=7*z[7];
    z[100]= - 56*z[2] + 18*z[4] + z[99] - z[42];
    z[100]=z[5]*z[100];
    z[100]=n<T>(35,2)*z[102] + z[100];
    z[100]=z[5]*z[100];
    z[107]=z[93]*z[25];
    z[109]=17*z[107];
    z[110]= - z[60]*z[109];
    z[111]=n<T>(1,3)*z[36];
    z[23]=z[111]*z[23];
    z[93]= - z[10]*z[93]*z[23];
    z[93]=z[110] + z[93];
    z[93]=z[93]*z[86];
    z[109]=z[63]*z[109];
    z[93]=z[109] + z[93];
    z[93]=z[10]*z[93];
    z[107]=z[108]*z[107];
    z[65]=z[65] + z[93] - n<T>(17,2)*z[107] + z[100] - n<T>(9,4)*z[105];
    z[65]=z[3]*z[65];
    z[80]=z[102]*z[80];
    z[65]=4*z[80] + z[65];
    z[65]=z[3]*z[65];
    z[93]= - z[8]*z[95];
    z[93]=z[96] + z[93];
    z[93]=z[8]*z[93];
    z[95]= - z[26]*z[62];
    z[98]=12*z[5] + 39*z[2] - 45*z[4] - 5*z[7] + z[98];
    z[98]=z[5]*z[98];
    z[70]=z[95] - z[70] + z[93] - 5*z[102] + z[98];
    z[70]=z[3]*z[70];
    z[90]=z[106] - z[90] - z[92] + z[67];
    z[90]=z[5]*z[90];
    z[90]= - 9*z[102] + z[90];
    z[92]=2*z[5];
    z[90]=z[90]*z[92];
    z[93]=npow(z[10],2);
    z[95]=npow(z[11],6);
    z[98]=z[95]*z[93]*z[23];
    z[95]=z[95]*z[25];
    z[100]= - z[63]*z[95];
    z[98]=z[100] + z[98];
    z[98]=z[10]*z[98];
    z[95]=2*z[95];
    z[100]=z[108]*z[95];
    z[98]=z[100] + z[98];
    z[26]=z[98]*z[26];
    z[95]= - npow(z[12],6)*z[95];
    z[26]=z[70] + z[26] + z[95] + z[90] + n<T>(5,2)*z[105];
    z[26]=z[3]*z[26];
    z[26]= - 7*z[80] + z[26];
    z[26]=z[26]*npow(z[3],2);
    z[70]= - z[7] + 8*z[2];
    z[90]=z[103] - z[70];
    z[90]=z[90]*z[92];
    z[90]=7*z[102] + z[90];
    z[90]=z[5]*z[90];
    z[90]=z[90] - z[105];
    z[90]=z[3]*z[90];
    z[90]=6*z[80] + z[90];
    z[90]=z[90]*npow(z[3],3);
    z[92]=z[1]*npow(z[3],4)*z[80];
    z[90]=z[90] - 2*z[92];
    z[90]=z[1]*z[90];
    z[26]=z[26] + z[90];
    z[26]=z[1]*z[26];
    z[26]=z[65] + z[26];
    z[26]=z[1]*z[26];
    z[20]=z[7]*z[20]*z[23];
    z[23]=npow(z[14],2);
    z[65]= - z[2]*z[23]*z[97];
    z[20]=z[20] + z[65];
    z[20]=z[20]*z[93];
    z[20]=z[26] + z[30] - z[80] + 2*z[20];
    z[20]=z[1]*z[20];
    z[26]=z[9]*z[7];
    z[30]=z[31]*z[16]*z[26];
    z[65]=z[31]*z[14];
    z[80]=z[96]*z[65];
    z[90]=z[31]*z[12];
    z[65]=17*z[65] + 31*z[90];
    z[90]= - z[65]*z[76];
    z[80]=z[80] + z[90];
    z[80]=z[80]*z[19];
    z[30]= - n<T>(3,2)*z[30] + z[80];
    z[30]=z[10]*z[30];
    z[65]=z[65]*z[12];
    z[31]=z[23]*z[31];
    z[80]=4*z[31] + z[65];
    z[80]=z[13]*z[80];
    z[90]=z[2]*z[31];
    z[80]=n<T>(1,2)*z[90] + z[80];
    z[80]=z[80]*z[19];
    z[37]= - z[7]*z[37];
    z[37]=z[37] + n<T>(3,2)*z[38];
    z[37]=z[37]*z[16];
    z[30]=z[30] + z[37] + z[80];
    z[30]=z[10]*z[30];
    z[31]= - 2*z[31] - n<T>(1,2)*z[65];
    z[25]=z[12]*z[31]*z[25];
    z[31]= - z[5]*z[70];
    z[31]=z[31] + z[102];
    z[31]=z[5]*z[31];
    z[37]= - z[6] - z[2];
    z[38]=z[15]*z[14];
    z[65]=z[8]*z[38];
    z[37]=n<T>(1,2)*z[37] + z[65];
    z[37]=z[37]*z[16];
    z[18]=z[20] + z[18] + z[30] + z[25] + z[31] + n<T>(1,2)*z[37];
    z[18]=z[1]*z[18];
    z[20]=n<T>(7,2)*z[8] - z[15] - z[83];
    z[20]=z[20]*z[16];
    z[25]=3*z[15];
    z[19]= - z[19]*z[25];
    z[19]=z[20] + z[19];
    z[19]=z[10]*z[19];
    z[20]=3*z[33];
    z[30]=n<T>(1,4)*z[34] + z[20];
    z[31]=n<T>(1,4)*z[13] - z[15] - n<T>(3,4)*z[7];
    z[31]=z[31]*z[41];
    z[37]=n<T>(1,4)*z[6];
    z[65]= - n<T>(11,3)*z[8] + z[15] - z[37];
    z[65]=z[8]*z[65];
    z[19]=z[19] + z[31] + 3*z[30] + z[65];
    z[19]=z[10]*z[19];
    z[30]= - z[45]*z[82];
    z[31]= - n<T>(7,4) + z[29];
    z[31]=z[31]*z[41];
    z[45]=2*z[15];
    z[65]=npow(z[11],2);
    z[70]=z[65]*z[9];
    z[80]=z[65]*z[12];
    z[19]=z[19] + z[31] - n<T>(3,2)*z[80] + z[30] + z[70] - 15*z[2] + z[84]
    - z[45] - z[27];
    z[19]=z[10]*z[19];
    z[27]= - static_cast<T>(1)- z[52];
    z[27]=z[27]*z[74];
    z[30]=2*z[8];
    z[31]= - z[104] + z[30];
    z[31]=z[31]*z[47];
    z[83]=4*z[6] - n<T>(19,3)*z[8];
    z[83]=z[8]*z[83];
    z[31]=z[83] + z[31];
    z[31]=z[10]*z[31];
    z[83]=n<T>(10,3) - z[44];
    z[83]=z[8]*z[83];
    z[27]=z[31] + z[27] - z[6] + z[83];
    z[27]=z[10]*z[27];
    z[31]= - n<T>(7,3) + z[73];
    z[73]=6*z[29];
    z[27]=z[27] - z[73] + n<T>(1,2)*z[31] - z[51];
    z[27]=z[10]*z[27];
    z[30]=z[42] - z[30];
    z[30]=z[8]*z[30];
    z[31]= - z[47]*z[67];
    z[30]=z[30] + z[31];
    z[30]=z[10]*z[30];
    z[31]=static_cast<T>(3)- z[50];
    z[31]=z[8]*z[31];
    z[30]=z[30] - z[6] + z[31];
    z[30]=z[10]*z[30];
    z[31]=z[75] + z[53];
    z[31]=z[8]*z[31];
    z[40]= - z[40]*z[41];
    z[42]=z[67]*z[12];
    z[42]=z[42] + 1;
    z[30]=z[30] + z[40] + z[31] - z[42];
    z[30]=z[10]*z[30];
    z[31]= - z[24]*z[41];
    z[30]=z[30] + z[31] + z[12] - z[54];
    z[30]=z[10]*z[30];
    z[31]= - static_cast<T>(1)+ z[55];
    z[31]=z[31]*z[60];
    z[40]=static_cast<T>(3)- 2*z[55];
    z[40]=z[40]*z[64];
    z[31]=7*z[31] + z[40];
    z[31]=z[31]*z[41];
    z[40]=z[9]*z[61];
    z[40]= - 5*z[55] + static_cast<T>(1)+ z[40];
    z[40]=z[40]*z[59];
    z[47]=static_cast<T>(1)- z[58];
    z[47]=z[9]*z[47];
    z[40]=z[47] + z[40];
    z[40]=z[12]*z[40];
    z[30]=z[30] + z[40] + z[31];
    z[30]=z[3]*z[30];
    z[31]= - n<T>(71,2) + 36*z[55];
    z[31]=z[31]*z[24];
    z[40]=n<T>(67,2) - 15*z[55];
    z[40]=z[40]*z[60];
    z[47]=6*z[64];
    z[40]=z[40] - z[47];
    z[40]=z[13]*z[40];
    z[31]=z[31] + z[40];
    z[31]=z[13]*z[31];
    z[40]=2*z[57] - 7*z[55];
    z[40]=z[40]*z[59];
    z[51]=n<T>(77,6) - z[58];
    z[51]=z[9]*z[51];
    z[27]=z[30] + z[27] + z[31] + z[40] + z[51] + z[53];
    z[27]=z[3]*z[27];
    z[30]=n<T>(7,2)*z[80] + n<T>(1,2)*z[70] - z[67] - n<T>(39,4)*z[4];
    z[30]=z[12]*z[30];
    z[31]=n<T>(1,3)*z[65] - n<T>(9,4)*z[78];
    z[31]=z[9]*z[31];
    z[40]=n<T>(9,2)*z[4] + z[88];
    z[31]=n<T>(3,2)*z[40] + z[31];
    z[31]=z[9]*z[31];
    z[40]= - static_cast<T>(11)+ z[71];
    z[40]=z[9]*z[40];
    z[40]=z[40] + z[53];
    z[40]=z[8]*z[40];
    z[51]=n<T>(143,2) - 27*z[55];
    z[51]=z[51]*z[24];
    z[51]=z[51] - 27*z[72];
    z[51]=z[13]*z[51];
    z[54]= - static_cast<T>(23)+ 31*z[55];
    z[54]=z[12]*z[54];
    z[51]=n<T>(3,2)*z[54] + z[51];
    z[51]=z[51]*z[76];
    z[19]=z[27] + z[19] + z[51] + z[30] + n<T>(1,4)*z[40] + n<T>(13,4) + z[31];
    z[19]=z[3]*z[19];
    z[27]=z[101] - n<T>(5,2)*z[15] - z[67];
    z[27]=z[8]*z[27];
    z[27]=z[65] + z[27];
    z[27]=z[8]*z[27];
    z[30]= - z[65] - z[69];
    z[30]=z[30]*z[74];
    z[27]=z[30] + 8*z[81] + z[27];
    z[27]=z[10]*z[27];
    z[30]= - z[15] - n<T>(1,2)*z[7];
    z[30]=z[76] + n<T>(1,2)*z[30] + 2*z[80];
    z[30]=z[30]*z[41];
    z[31]=z[25] - z[89];
    z[31]=n<T>(1,2)*z[31] + z[70];
    z[31]=z[8]*z[31];
    z[40]= - z[99] - 23*z[2];
    z[40]=z[5]*z[40];
    z[27]=z[27] + z[30] + z[31] + n<T>(5,4)*z[34] + z[40];
    z[27]=z[10]*z[27];
    z[30]= - n<T>(5,4)*z[2] + n<T>(1,3)*z[70];
    z[30]=z[9]*z[30];
    z[30]= - n<T>(1,2)*z[43] + static_cast<T>(1)+ z[30];
    z[30]=z[8]*z[30];
    z[31]=static_cast<T>(35)- 9*z[55];
    z[31]=z[12]*z[31];
    z[31]=z[31] - 27*z[39];
    z[31]=z[31]*z[76];
    z[40]=n<T>(21,4)*z[4] - 11*z[80];
    z[40]=z[12]*z[40];
    z[31]=z[31] - n<T>(17,4) + z[40];
    z[31]=z[13]*z[31];
    z[19]=z[19] + z[27] + z[31] + z[30] + n<T>(3,4)*z[79] + z[35] + z[88] - 
   n<T>(9,4)*z[4] + n<T>(8,3)*z[15] + z[7];
    z[19]=z[3]*z[19];
    z[27]=z[65]*z[38]*z[96];
    z[30]=z[65]*z[14];
    z[31]= - z[96]*z[30];
    z[28]= - z[28] - 2*z[30];
    z[28]=3*z[28] - n<T>(43,2)*z[80];
    z[28]=z[13]*z[28];
    z[28]=z[31] + z[28];
    z[28]=z[13]*z[28];
    z[31]=z[2]*z[15];
    z[35]=z[31] - z[91];
    z[40]=n<T>(3,2)*z[8];
    z[40]=z[7]*z[40];
    z[46]= - z[2] + z[46];
    z[46]=z[13]*z[46];
    z[40]=z[46] + z[40] + z[35];
    z[40]=z[10]*z[65]*z[40];
    z[46]=z[101] - z[66] - z[6];
    z[46]=z[46]*z[82];
    z[51]=z[7]*z[70];
    z[46]=z[51] + z[46];
    z[46]=z[8]*z[46];
    z[27]=z[40] + z[28] + z[46] + z[27] - z[62];
    z[27]=z[10]*z[27];
    z[28]=z[7]*z[65]*z[111];
    z[32]= - z[6] + z[32];
    z[40]=static_cast<T>(1)- z[38];
    z[40]=z[8]*z[40];
    z[28]=z[40] + n<T>(1,2)*z[32] + z[28];
    z[28]=z[8]*z[28];
    z[23]=z[23]*z[65];
    z[32]=static_cast<T>(5)- z[23];
    z[30]=4*z[30] + n<T>(33,2)*z[80];
    z[30]=z[12]*z[30];
    z[30]= - z[73] + n<T>(1,2)*z[32] + z[30];
    z[30]=z[13]*z[30];
    z[32]= - n<T>(3,2) - 2*z[23];
    z[32]=z[2]*z[32];
    z[30]=z[30] + z[32] + n<T>(3,2)*z[15];
    z[30]=z[13]*z[30];
    z[22]= - z[22]*z[23];
    z[22]= - z[15] + z[22];
    z[22]=z[7]*z[22];
    z[23]=z[23]*z[45];
    z[23]=z[25] + z[23];
    z[23]=z[2]*z[23];
    z[32]=z[5]*z[88];
    z[18]=z[18] + z[19] + z[27] + z[30] + z[28] + z[32] + z[23] + z[22]
    + n<T>(1,2)*z[34];
    z[18]=z[1]*z[18];
    z[19]=static_cast<T>(1)- n<T>(31,3)*z[43];
    z[19]=z[19]*z[82];
    z[22]= - n<T>(3,2) + z[50];
    z[22]=z[8]*z[22];
    z[22]=z[17] + z[22];
    z[22]=z[8]*z[22];
    z[22]=z[22] - n<T>(5,2)*z[48];
    z[22]=z[10]*z[22];
    z[19]=z[19] + z[22];
    z[19]=z[10]*z[19];
    z[22]=n<T>(17,3)*z[9] - z[53];
    z[22]=z[22]*z[82];
    z[23]=z[12] - z[85];
    z[23]=z[23]*z[74];
    z[19]=z[19] + z[23] + z[22] + z[42];
    z[19]=z[10]*z[19];
    z[22]=n<T>(71,2)*z[60] - z[47];
    z[22]=z[13]*z[22];
    z[22]= - 34*z[24] + z[22];
    z[22]=z[13]*z[22];
    z[19]=z[19] + z[22] + n<T>(7,2)*z[12] + n<T>(1,3)*z[9] - z[77];
    z[19]=z[10]*z[19];
    z[22]=z[94] - z[53];
    z[22]=z[8]*z[22];
    z[23]=z[67] - z[8];
    z[23]=z[8]*z[23];
    z[23]=z[23] - z[48];
    z[23]=z[10]*z[23];
    z[27]=static_cast<T>(2)- z[43];
    z[27]=z[8]*z[27];
    z[23]=z[23] - z[6] + z[27];
    z[23]=z[10]*z[23];
    z[22]=z[23] - static_cast<T>(1)+ z[22];
    z[22]=z[10]*z[22];
    z[23]=z[24] - z[72];
    z[23]=z[23]*z[41];
    z[22]=z[22] + z[23] - z[9] + 2*z[53];
    z[22]=z[10]*z[22];
    z[23]= - 2*z[60] + z[64];
    z[23]=z[13]*z[23];
    z[27]=3*z[24];
    z[22]=z[22] + 9*z[23] - z[36] + z[27];
    z[22]=z[10]*z[22];
    z[23]=static_cast<T>(9)- 4*z[55];
    z[23]=z[23]*z[63];
    z[28]=z[108]*z[13];
    z[30]= - static_cast<T>(3)+ z[55];
    z[30]=z[30]*z[28];
    z[23]=z[23] + z[30];
    z[23]=z[13]*z[23];
    z[30]= - static_cast<T>(2)- z[57];
    z[30]=2*z[30] + z[68];
    z[30]=z[12]*z[30];
    z[32]=z[57] + 1;
    z[32]=z[32]*z[9];
    z[30]=z[32] + z[30];
    z[30]=z[30]*z[24];
    z[23]=z[30] + z[23];
    z[22]=3*z[23] + z[22];
    z[22]=z[3]*z[22];
    z[23]=n<T>(29,2) - z[56];
    z[23]=z[23]*z[60];
    z[30]= - static_cast<T>(89)+ 21*z[55];
    z[30]=z[30]*z[63];
    z[28]=n<T>(1,2)*z[30] + 6*z[28];
    z[28]=z[13]*z[28];
    z[23]=5*z[23] + z[28];
    z[23]=z[13]*z[23];
    z[28]=n<T>(39,2)*z[55] - static_cast<T>(25)- z[58];
    z[28]=z[12]*z[28];
    z[30]=n<T>(31,3) + z[58];
    z[30]=z[9]*z[30];
    z[28]=n<T>(1,2)*z[30] + z[28];
    z[28]=z[12]*z[28];
    z[19]=z[22] + z[19] + z[23] + n<T>(4,3)*z[36] + z[28];
    z[19]=z[3]*z[19];
    z[22]=n<T>(1,3)*z[8];
    z[23]= - static_cast<T>(4)- n<T>(5,4)*z[43];
    z[23]=z[23]*z[22];
    z[28]=n<T>(3,4)*z[13];
    z[29]=static_cast<T>(1)- 9*z[29];
    z[29]=z[29]*z[28];
    z[30]=z[43] - n<T>(1,2);
    z[34]=z[30]*z[101];
    z[34]= - z[6] + z[34];
    z[34]=z[8]*z[34];
    z[34]=z[34] - n<T>(9,2)*z[48];
    z[34]=z[34]*z[86];
    z[17]=z[34] + z[29] + z[17] + z[23];
    z[17]=z[10]*z[17];
    z[23]= - z[75] - z[77];
    z[23]=z[8]*z[23];
    z[23]=n<T>(23,6) + z[23];
    z[29]=n<T>(179,2)*z[24] - 39*z[72];
    z[29]=z[13]*z[29];
    z[29]= - 45*z[12] + z[29];
    z[29]=z[29]*z[76];
    z[17]=z[17] + z[29] + n<T>(1,2)*z[23] + z[49];
    z[17]=z[10]*z[17];
    z[23]=n<T>(91,2) - 19*z[55];
    z[23]=z[23]*z[27];
    z[27]= - static_cast<T>(287)+ 57*z[55];
    z[27]=z[27]*z[60];
    z[27]=n<T>(1,2)*z[27] + 39*z[64];
    z[27]=z[13]*z[27];
    z[23]=z[23] + z[27];
    z[23]=z[23]*z[76];
    z[27]=n<T>(9,2)*z[32] - z[53];
    z[29]=n<T>(57,4)*z[55] - n<T>(49,3) - z[58];
    z[29]=z[12]*z[29];
    z[17]=z[19] + z[17] + z[23] + n<T>(1,2)*z[27] + z[29];
    z[17]=z[3]*z[17];
    z[19]= - n<T>(7,2) + z[44];
    z[19]=z[19]*z[82];
    z[19]=z[19] + z[15] - z[21];
    z[19]=z[8]*z[19];
    z[21]= - z[15] - z[28];
    z[21]=z[21]*z[41];
    z[19]= - z[48] + z[21] + z[19] - z[20] - z[35];
    z[19]=z[10]*z[19];
    z[20]= - z[30]*z[22];
    z[21]=n<T>(17,2)*z[12] - 9*z[39];
    z[21]=z[13]*z[21];
    z[21]= - n<T>(3,2) + z[21];
    z[21]=z[21]*z[41];
    z[19]=z[19] + z[21] + z[20] + 3*z[2] + z[6] - z[45] + n<T>(7,3)*z[7];
    z[19]=z[10]*z[19];
    z[20]= - n<T>(73,4) + z[68];
    z[20]=z[20]*z[24];
    z[20]=z[20] + 9*z[72];
    z[20]=z[20]*z[41];
    z[21]=n<T>(59,2) - 12*z[55];
    z[21]=z[12]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[13]*z[20];
    z[21]= - n<T>(3,2)*z[2] - n<T>(7,6)*z[7] - z[61];
    z[21]=z[9]*z[21];
    z[22]= - n<T>(5,3)*z[15] + z[61];
    z[22]=z[12]*z[22];
    z[17]=z[17] + z[19] + z[20] + 2*z[22] + n<T>(2,3)*z[43] - n<T>(17,4) + z[21]
   ;
    z[17]=z[3]*z[17];
    z[19]=n<T>(13,2)*z[12] + 2*z[14];
    z[20]= - z[19]*z[41];
    z[21]=z[88]*z[14];
    z[22]=n<T>(19,2) - z[21];
    z[20]=n<T>(1,2)*z[22] + z[20];
    z[20]=z[13]*z[20];
    z[20]=z[20] - z[25] + z[96];
    z[20]=z[13]*z[20];
    z[16]= - z[16]*z[37];
    z[16]= - 2*z[81] + z[16];
    z[16]=z[10]*z[16];
    z[22]= - z[31] + z[33];
    z[23]= - static_cast<T>(1)- n<T>(5,3)*z[26];
    z[23]=z[23]*z[82];
    z[23]=z[23] + n<T>(5,3)*z[7] - z[87];
    z[23]=z[8]*z[23];
    z[16]=z[16] + z[20] + 2*z[22] + z[23];
    z[16]=z[10]*z[16];
    z[20]= - z[68] + z[21] + static_cast<T>(17)- 3*z[38];
    z[19]=z[19]*z[52];
    z[21]=n<T>(1,2)*z[14];
    z[22]= - static_cast<T>(19)+ n<T>(9,4)*z[55];
    z[22]=z[12]*z[22];
    z[19]=z[19] - z[21] + z[22];
    z[19]=z[13]*z[19];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[13]*z[19];
    z[20]=n<T>(1,6)*z[26] - static_cast<T>(1)+ n<T>(1,2)*z[38];
    z[20]=z[8]*z[20];
    z[22]= - n<T>(1,2) + z[38];
    z[22]=z[7]*z[22];
    z[23]=n<T>(1,2) - 4*z[38];
    z[23]=z[2]*z[23];
    z[16]=z[18] + z[17] + z[16] + z[19] + z[20] + z[23] + n<T>(3,4)*z[4] - 
   n<T>(19,3)*z[15] + z[22];
    z[16]=z[1]*z[16];
    z[17]= - z[10] - z[9] - z[12];
    z[17]=z[3]*z[17];
    z[17]=z[26] - z[17];
    z[18]= - z[86] + z[21];
    z[18]=z[7]*z[18];
    z[19]= - 7*z[14] - 11*z[12];
    z[19]=z[19]*z[76];
    z[20]=z[12]*z[15];

    r += n<T>(1,2) + z[16] - n<T>(1,2)*z[17] + z[18] + z[19] + n<T>(4,3)*z[20] + n<T>(5,3)*z[38];
 
    return r;
}

template double qg_2lNLC_r1327(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1327(const std::array<dd_real,31>&);
#endif
