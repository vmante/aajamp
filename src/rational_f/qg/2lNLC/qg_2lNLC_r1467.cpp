#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1467(const std::array<T,31>& k) {
  T z[43];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[11];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[5];
    z[9]=k[8];
    z[10]=k[3];
    z[11]=npow(z[8],2);
    z[12]=z[8]*z[9];
    z[13]=7*z[12];
    z[14]=z[13] - 1;
    z[14]=z[14]*z[8];
    z[15]=7*z[7] - z[14];
    z[15]=z[15]*z[11];
    z[16]=3*z[12];
    z[17]=z[16] - 2;
    z[17]=z[17]*z[8];
    z[18]=3*z[7];
    z[19]= - z[18] + z[17];
    z[20]=npow(z[8],3);
    z[19]=z[19]*z[20];
    z[21]=static_cast<T>(1)- z[12];
    z[21]=z[8]*z[21];
    z[21]=z[7] + z[21];
    z[21]=z[4]*z[21]*npow(z[8],4);
    z[19]=z[19] + z[21];
    z[21]=2*z[4];
    z[19]=z[19]*z[21];
    z[15]=z[15] + z[19];
    z[15]=z[4]*z[15];
    z[19]=z[9]*z[7];
    z[16]=z[16] + static_cast<T>(2)+ z[19];
    z[16]=z[8]*z[16];
    z[22]=2*z[7];
    z[16]=z[16] + z[10] - z[22];
    z[16]=z[8]*z[16];
    z[23]=npow(z[10],2);
    z[24]=z[7]*z[10];
    z[15]=z[15] + z[16] + z[23] + z[24];
    z[15]=z[6]*z[15];
    z[16]=2*z[12];
    z[23]=z[16] - 1;
    z[24]=z[23]*z[8];
    z[18]= - z[18] + 4*z[24];
    z[18]=z[18]*z[11];
    z[20]=z[20]*z[4];
    z[17]=z[7] - z[17];
    z[17]=z[17]*z[20];
    z[17]=z[18] + z[17];
    z[17]=z[17]*z[21];
    z[18]=16*z[12];
    z[25]=static_cast<T>(3)- z[18];
    z[25]=z[8]*z[25];
    z[25]=6*z[7] + z[25];
    z[25]=z[8]*z[25];
    z[17]=z[25] + z[17];
    z[17]=z[4]*z[17];
    z[19]=z[19] + static_cast<T>(1)+ 5*z[12];
    z[25]=z[8]*z[19];
    z[15]=z[15] + z[17] - z[7] + z[25];
    z[17]=2*z[6];
    z[15]=z[15]*z[17];
    z[18]= - static_cast<T>(5)+ z[18];
    z[18]=z[8]*z[18];
    z[18]= - z[22] + z[18];
    z[18]=z[8]*z[18];
    z[25]=z[11]*z[4];
    z[26]=z[13] - 3;
    z[26]=z[26]*z[8];
    z[27]=z[7] - z[26];
    z[27]=z[27]*z[25];
    z[18]=z[18] + z[27];
    z[18]=z[4]*z[18];
    z[27]=static_cast<T>(1)- 13*z[12];
    z[27]=z[8]*z[27];
    z[18]=z[18] + z[7] + z[27];
    z[18]=z[18]*z[21];
    z[15]=z[15] + z[18] + z[19];
    z[15]=z[6]*z[15];
    z[18]=4*z[12];
    z[19]=z[18] - 1;
    z[27]= - z[19]*z[25];
    z[14]=z[14] + z[27];
    z[14]=z[4]*z[14];
    z[14]= - z[18] + z[14];
    z[14]=z[14]*z[21];
    z[14]=z[15] + z[9] + z[14];
    z[14]=z[6]*z[14];
    z[15]= - z[9]*z[25];
    z[15]=z[12] + z[15];
    z[15]=z[15]*z[21];
    z[15]= - z[9] + z[15];
    z[15]=z[4]*z[15];
    z[14]=z[15] + z[14];
    z[15]=npow(z[2],2);
    z[14]=z[14]*z[15];
    z[18]=z[3] + z[5];
    z[27]=z[18]*z[4];
    z[28]=z[3]*z[5];
    z[29]=7*z[28];
    z[30]=z[29] - 3*z[27];
    z[31]=npow(z[4],2);
    z[32]=2*z[31];
    z[30]=z[30]*z[32];
    z[33]=z[5]*z[7];
    z[34]=z[8]*z[3];
    z[35]=z[34] - 1;
    z[36]=z[33] - z[35];
    z[37]=z[4]*z[36];
    z[38]=2*z[18];
    z[37]= - z[38] + z[37];
    z[37]=z[37]*z[21];
    z[37]=z[29] + z[37];
    z[37]=z[6]*z[4]*z[37];
    z[30]=z[30] + z[37];
    z[30]=z[6]*z[30];
    z[37]=npow(z[4],3);
    z[39]=z[37]*z[29];
    z[30]=z[39] + z[30];
    z[30]=z[30]*npow(z[6],2);
    z[39]=3*z[28];
    z[40]= - z[37]*z[39];
    z[27]= - z[39] + z[27];
    z[27]=z[6]*z[27]*z[31];
    z[27]=z[40] + z[27];
    z[27]=z[27]*npow(z[6],3);
    z[37]=z[37]*z[28];
    z[40]=z[1]*npow(z[6],4)*z[37];
    z[27]=z[27] + z[40];
    z[40]=2*z[1];
    z[27]=z[40]*z[27];
    z[27]=z[30] + z[27];
    z[27]=z[1]*z[15]*z[27];
    z[30]=3*z[34] - static_cast<T>(3)- z[33];
    z[30]=z[4]*z[30];
    z[30]=4*z[18] + z[30];
    z[30]=z[4]*z[30];
    z[30]= - 6*z[28] + z[30];
    z[30]=z[30]*z[21];
    z[41]=z[35]*z[8];
    z[41]=z[41] + z[7];
    z[42]=z[4]*z[41];
    z[42]=z[42] - z[36];
    z[42]=z[42]*z[21];
    z[18]=z[42] + 3*z[18];
    z[18]=z[4]*z[18];
    z[18]= - z[39] + z[18];
    z[18]=z[6]*z[18];
    z[18]=z[30] + z[18];
    z[18]=z[6]*z[18];
    z[30]=5*z[5] + 7*z[3];
    z[30]=z[4]*z[30];
    z[30]= - 12*z[28] + z[30];
    z[30]=z[30]*z[31];
    z[18]=z[30] + z[18];
    z[18]=z[6]*z[18];
    z[18]= - 4*z[37] + z[18];
    z[18]=z[6]*z[18]*z[15];
    z[18]=z[18] + z[27];
    z[18]=z[1]*z[18];
    z[27]=2*z[8];
    z[27]= - z[41]*z[31]*z[27];
    z[27]=z[27] + z[36];
    z[27]=z[4]*z[27];
    z[30]=z[28]*z[8];
    z[36]=z[30] - z[5];
    z[27]=z[27] + z[36];
    z[27]=z[6]*z[27];
    z[41]=3*z[8];
    z[42]= - z[35]*z[41];
    z[42]= - z[7] + z[42];
    z[42]=z[4]*z[42];
    z[35]=z[42] - z[35];
    z[35]=z[4]*z[35];
    z[35]= - z[38] + z[35];
    z[35]=z[4]*z[35];
    z[38]=2*z[28];
    z[35]=z[38] + z[35];
    z[27]=2*z[35] + z[27];
    z[27]=z[6]*z[27];
    z[33]= - 7*z[34] + static_cast<T>(5)+ z[33];
    z[33]=z[4]*z[33];
    z[33]=z[33] - 3*z[5] - 5*z[3];
    z[33]=z[4]*z[33];
    z[29]=z[29] + z[33];
    z[29]=z[4]*z[29];
    z[27]=z[29] + z[27];
    z[27]=z[6]*z[27];
    z[29]=2*z[3];
    z[33]= - z[5] - z[29];
    z[33]=z[33]*z[21];
    z[33]=5*z[28] + z[33];
    z[31]=z[33]*z[31];
    z[27]=z[31] + z[27];
    z[27]=z[6]*z[27];
    z[27]=z[37] + z[27];
    z[27]=z[27]*z[15];
    z[18]=z[27] + z[18];
    z[18]=z[18]*z[40];
    z[27]=z[12] + 1;
    z[31]= - z[27]*z[41];
    z[31]=z[7] + z[31];
    z[33]=z[4]*z[8];
    z[31]=z[31]*z[33];
    z[34]=3*z[9];
    z[35]=z[3] + z[34];
    z[35]=z[8]*z[35];
    z[35]=static_cast<T>(2)+ z[35];
    z[35]=z[8]*z[35];
    z[31]=z[31] - z[7] + z[35];
    z[31]=z[4]*z[31];
    z[31]=z[31] - z[27];
    z[31]=z[31]*z[21];
    z[35]=z[8] - z[25];
    z[35]=z[21]*z[35];
    z[35]=z[35] - 1;
    z[27]=z[27]*z[8];
    z[27]=z[27] - z[7];
    z[27]=z[4]*z[27]*z[35];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[6]*z[27];
    z[27]=z[27] + z[31] - z[36];
    z[27]=z[27]*z[17];
    z[29]=z[29] + z[34];
    z[29]=z[8]*z[29];
    z[29]=static_cast<T>(1)+ z[29];
    z[13]= - static_cast<T>(5)- z[13];
    z[13]=z[8]*z[13];
    z[13]=z[7] + z[13];
    z[13]=z[4]*z[13];
    z[13]=2*z[29] + z[13];
    z[13]=z[4]*z[13];
    z[13]=z[13] + z[3] - z[9];
    z[13]=z[13]*z[21];
    z[13]=z[27] - z[39] + z[13];
    z[13]=z[6]*z[13];
    z[16]= - static_cast<T>(1)- z[16];
    z[16]=z[16]*z[21];
    z[27]=2*z[9];
    z[16]=z[16] + z[27] + z[5] + 3*z[3];
    z[16]=z[4]*z[16];
    z[16]= - z[38] + z[16];
    z[16]=z[16]*z[21];
    z[13]=z[16] + z[13];
    z[13]=z[6]*z[13];
    z[16]= - z[4]*z[9];
    z[16]= - z[28] + z[16];
    z[16]=z[16]*z[32];
    z[13]=z[16] + z[13];
    z[13]=z[13]*z[15];
    z[13]=z[13] + z[18];
    z[13]=z[1]*z[13];
    z[16]= - static_cast<T>(5)+ 11*z[12];
    z[16]=z[8]*z[16];
    z[16]=z[22] + z[16];
    z[16]=z[8]*z[16];
    z[18]= - z[23]*z[41];
    z[18]= - z[7] + z[18];
    z[18]=z[18]*z[25];
    z[16]=z[16] + z[18];
    z[16]=z[4]*z[16];
    z[16]=z[16] - z[7] - z[26];
    z[16]=z[16]*z[21];
    z[11]=2*z[11] - z[20];
    z[11]=z[21]*z[11];
    z[11]= - z[41] + z[11];
    z[18]=z[24] + z[7];
    z[11]=z[4]*z[18]*z[11];
    z[18]= - static_cast<T>(2)+ z[12];
    z[18]=z[8]*z[18];
    z[11]=z[11] - 2*z[10] + z[18];
    z[11]=z[6]*z[11];
    z[11]=z[11] + z[16] + z[23];
    z[11]=z[11]*z[17];
    z[16]=static_cast<T>(5)- 14*z[12];
    z[16]=z[8]*z[16];
    z[16]= - z[7] + z[16];
    z[16]=z[16]*z[33];
    z[17]= - static_cast<T>(7)+ 22*z[12];
    z[17]=z[8]*z[17];
    z[16]=z[16] + z[7] + z[17];
    z[16]=z[4]*z[16];
    z[17]= - z[3] - 10*z[9];
    z[17]=z[8]*z[17];
    z[16]=z[16] + static_cast<T>(2)+ z[17];
    z[16]=z[16]*z[21];
    z[11]=z[11] + z[16] + z[30] + z[3] + z[9];
    z[11]=z[6]*z[11];
    z[16]= - z[8]*z[19]*z[21];
    z[16]=z[16] - static_cast<T>(1)+ 9*z[12];
    z[16]=z[4]*z[16];
    z[16]=z[16] - z[3] - z[27];
    z[16]=z[16]*z[21];
    z[11]=z[16] + z[11];
    z[11]=z[6]*z[11];
    z[12]= - z[12]*z[21];
    z[12]=z[9] + z[12];
    z[12]=z[12]*z[21];
    z[12]=z[28] + z[12];
    z[12]=z[4]*z[12];
    z[11]=z[12] + z[11];
    z[11]=z[11]*z[15];
    z[11]=z[11] + z[13];
    z[11]=z[1]*z[11];
    z[11]=z[14] + z[11];

    r += z[11]*npow(z[1],2);
 
    return r;
}

template double qg_2lNLC_r1467(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1467(const std::array<dd_real,31>&);
#endif
