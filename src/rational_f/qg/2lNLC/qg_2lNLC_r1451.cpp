#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1451(const std::array<T,31>& k) {
  T z[62];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[13];
    z[6]=k[5];
    z[7]=k[8];
    z[8]=k[10];
    z[9]=k[12];
    z[10]=k[15];
    z[11]=k[4];
    z[12]=k[2];
    z[13]=k[7];
    z[14]=npow(z[3],3);
    z[15]=npow(z[2],2);
    z[16]=z[14]*z[15];
    z[17]=2*z[16];
    z[18]=2*z[6];
    z[19]=z[14]*z[2];
    z[20]=z[19]*z[18];
    z[21]=3*z[10];
    z[20]=z[20] - z[17] + 2*z[13] + z[21];
    z[20]=z[6]*z[20];
    z[22]=z[6]*z[13];
    z[22]=z[22] - 1;
    z[23]= - z[6]*z[22];
    z[23]=z[2] + z[23];
    z[23]=z[6]*z[23];
    z[23]=z[15] + z[23];
    z[24]=2*z[5];
    z[23]=z[23]*z[24];
    z[25]=z[2]*z[8];
    z[26]=z[25] - 3;
    z[27]=z[2]*z[26];
    z[28]= - z[13] - z[10];
    z[28]=z[6]*z[28];
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=z[6]*z[28];
    z[23]=z[23] + z[27] + z[28];
    z[23]=z[5]*z[23];
    z[27]=3*z[8];
    z[17]= - z[27] - z[17];
    z[17]=z[2]*z[17];
    z[17]=z[23] + z[20] - static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[24];
    z[20]= - z[15]*z[27];
    z[23]=npow(z[6],2);
    z[28]=npow(z[2],3);
    z[29]=z[8]*z[28];
    z[29]=z[29] - z[23];
    z[29]=z[5]*z[29];
    z[20]=z[20] + z[29];
    z[20]=z[20]*z[24];
    z[29]=npow(z[9],2);
    z[30]=z[23]*z[29];
    z[31]=4*z[25];
    z[32]=z[8]*z[12];
    z[33]=static_cast<T>(1)- z[32];
    z[20]=z[20] - 3*z[30] + 5*z[33] + z[31];
    z[33]=2*z[4];
    z[20]=z[20]*z[33];
    z[34]=z[9]*z[16];
    z[34]= - 2*z[29] + z[34];
    z[35]=z[6]*z[9];
    z[36]=z[35]*z[19];
    z[34]=3*z[34] - 2*z[36];
    z[34]=z[34]*z[18];
    z[36]=4*z[8];
    z[37]=z[7]*z[12];
    z[38]=static_cast<T>(1)- 2*z[37];
    z[38]=z[38]*z[36];
    z[39]=3*z[13];
    z[17]=z[20] + z[17] + z[34] + z[38] + z[39] + 8*z[7];
    z[17]=z[4]*z[17];
    z[20]=z[14]*z[6];
    z[34]=3*z[2];
    z[38]=z[34]*z[20];
    z[16]=z[38] + z[13] - 15*z[16];
    z[16]=z[6]*z[16];
    z[38]=z[14]*z[28];
    z[16]=z[16] - static_cast<T>(1)+ 12*z[38];
    z[38]=npow(z[5],2);
    z[40]=2*z[38];
    z[16]=z[16]*z[40];
    z[41]=z[30]*z[34];
    z[42]=z[14]*z[41];
    z[43]=2*z[9];
    z[44]=z[13] - z[43];
    z[44]=z[9]*z[44];
    z[42]=z[44] + z[42];
    z[16]=z[17] + 3*z[42] + z[16];
    z[16]=z[4]*z[16];
    z[17]=npow(z[5],3);
    z[42]=npow(z[11],2);
    z[44]=npow(z[3],4);
    z[45]= - z[7]*z[42]*z[44]*z[23]*z[17];
    z[46]=z[7] + z[10];
    z[47]=z[46]*z[8];
    z[48]=z[21]*z[7];
    z[49]= - z[48] + z[47];
    z[49]=z[9]*z[49];
    z[48]= - z[8]*z[48];
    z[48]=z[48] + z[49];
    z[48]=z[9]*z[48];
    z[49]=z[28]*z[6];
    z[50]=npow(z[2],4);
    z[51]=2*z[50] - z[49];
    z[51]=z[6]*z[51]*npow(z[3],5);
    z[52]=z[2]*z[3];
    z[52]= - npow(z[52],5);
    z[51]=z[52] + z[51];
    z[17]=2*z[17];
    z[51]=z[51]*npow(z[4],2)*z[17];
    z[52]=z[29]*z[10];
    z[53]= - z[8]*z[7]*z[52];
    z[51]=z[53] + z[51];
    z[51]=z[1]*z[51];
    z[45]=z[51] + z[48] + z[45];
    z[48]=z[44]*z[28];
    z[51]=z[15]*z[6];
    z[53]=z[51]*z[44];
    z[54]= - 6*z[48] + z[53];
    z[54]=z[6]*z[54];
    z[44]=z[50]*z[44];
    z[54]=5*z[44] + z[54];
    z[40]=z[54]*z[40];
    z[54]=2*z[10];
    z[55]=z[23]*z[38]*z[54];
    z[30]=z[21]*z[30];
    z[30]=z[30] + z[55];
    z[30]=z[30]*z[33];
    z[55]=z[29]*z[53];
    z[55]=4*z[52] + z[55];
    z[56]=3*z[6];
    z[55]=z[55]*z[56];
    z[30]=z[30] + z[55] + z[40];
    z[30]=z[4]*z[30];
    z[40]=2*z[48] - z[53];
    z[40]=z[6]*z[40];
    z[40]= - z[44] + z[40];
    z[17]=z[40]*z[17];
    z[17]=z[52] + z[17];
    z[17]=6*z[17] + z[30];
    z[17]=z[4]*z[17];
    z[17]=z[17] + 2*z[45];
    z[17]=z[1]*z[17];
    z[30]=z[14]*z[11];
    z[19]=7*z[19] + 4*z[30];
    z[30]=z[19]*z[2];
    z[14]=z[42]*z[14];
    z[30]=z[30] + z[14];
    z[40]= - z[2]*z[30];
    z[44]=z[9]*z[13];
    z[19]=z[44] - z[19];
    z[19]=z[6]*z[19];
    z[19]=z[19] - z[9] + 2*z[30];
    z[19]=z[6]*z[19];
    z[30]=z[9]*z[11];
    z[19]=z[19] + z[40] + static_cast<T>(1)+ z[30];
    z[19]=z[19]*z[24];
    z[30]=z[7]*z[11];
    z[20]=z[20]*z[30];
    z[40]=3*z[7];
    z[14]= - z[14]*z[40];
    z[14]=z[20] + z[14] + z[44];
    z[14]=z[6]*z[14];
    z[14]=z[19] - z[9] + z[14];
    z[14]=z[14]*z[38];
    z[19]=z[54]*z[7];
    z[38]= - z[19] + z[47];
    z[45]=z[10]*z[11];
    z[47]=z[45] + z[30];
    z[48]=z[8]*z[47];
    z[46]=3*z[46] + z[48];
    z[46]=z[9]*z[46];
    z[38]=3*z[38] + z[46];
    z[38]=z[9]*z[38];
    z[46]= - z[8]*z[19];
    z[38]=z[46] + z[38];
    z[20]= - z[19] + z[20];
    z[20]=z[56]*z[29]*z[20];
    z[14]=z[17] + z[16] + z[14] + 2*z[38] + z[20];
    z[14]=z[1]*z[14];
    z[16]=z[25] - 1;
    z[17]=6*z[15];
    z[20]=z[16]*z[17];
    z[38]=2*z[25];
    z[46]=static_cast<T>(3)- z[38];
    z[46]=z[46]*z[28];
    z[48]= - z[2]*z[23];
    z[46]=z[46] + z[48];
    z[46]=z[5]*z[46];
    z[20]=z[20] + z[46];
    z[20]=z[20]*z[24];
    z[46]=2*z[32];
    z[48]=static_cast<T>(1)+ z[46];
    z[48]=5*z[48] - 8*z[25];
    z[48]=z[2]*z[48];
    z[20]=z[20] + z[48] - z[41];
    z[20]=z[20]*z[33];
    z[41]= - z[26]*z[17];
    z[48]=6*z[5];
    z[52]= - z[28]*z[48];
    z[23]=z[52] + z[41] - z[23];
    z[23]=z[5]*z[23];
    z[41]=5*z[25];
    z[52]= - static_cast<T>(3)+ z[41];
    z[52]=z[52]*z[34];
    z[53]= - z[6]*z[10];
    z[53]= - static_cast<T>(3)+ z[53];
    z[53]=z[6]*z[53];
    z[23]=z[23] + z[52] + z[53];
    z[23]=z[23]*z[24];
    z[52]=npow(z[3],2);
    z[53]=z[52]*z[2];
    z[55]=2*z[8];
    z[57]= - z[55] - z[9];
    z[57]=12*z[57] + z[53];
    z[57]=z[2]*z[57];
    z[58]=3*z[9];
    z[59]=z[10] - z[58];
    z[59]=z[59]*z[35];
    z[60]=z[29]*z[2];
    z[61]=6*z[60];
    z[59]= - z[61] + z[59];
    z[59]=z[59]*z[18];
    z[20]=z[20] + z[23] + z[59] + z[57] + static_cast<T>(5)+ 12*z[32];
    z[20]=z[4]*z[20];
    z[23]=2*z[2];
    z[57]=static_cast<T>(6)- z[25];
    z[23]=z[57]*z[23];
    z[22]= - z[22]*z[56];
    z[57]= - z[15]*z[48];
    z[22]=z[57] + z[23] + z[22];
    z[22]=z[5]*z[22];
    z[23]=4*z[52];
    z[57]=z[23]*z[6];
    z[59]=z[57] - 6*z[53] - 4*z[13] - z[21];
    z[59]=z[6]*z[59];
    z[27]=z[27] - z[53];
    z[27]=z[2]*z[27];
    z[22]=z[22] + z[59] + static_cast<T>(2)+ z[27];
    z[22]=z[22]*z[24];
    z[27]= - static_cast<T>(1)- z[37];
    z[27]=z[27]*z[36];
    z[36]=5*z[9];
    z[27]= - z[36] + z[27] - z[13] + z[21];
    z[37]=z[10] - z[43];
    z[37]=z[37]*z[43];
    z[36]=z[53]*z[36];
    z[36]=z[37] + z[36];
    z[37]=z[52]*z[35];
    z[36]=3*z[36] - 8*z[37];
    z[36]=z[6]*z[36];
    z[37]= - z[13] + z[9];
    z[37]=z[9]*z[37]*z[34];
    z[20]=z[20] + z[22] + z[36] + 2*z[27] + z[37];
    z[20]=z[4]*z[20];
    z[22]= - z[13] + z[54];
    z[22]=2*z[22] + z[40];
    z[27]=z[47]*z[55];
    z[36]=static_cast<T>(3)+ z[45];
    z[36]=2*z[36] + 7*z[30];
    z[36]=z[9]*z[36];
    z[22]=z[36] + 3*z[22] + z[27];
    z[22]=z[9]*z[22];
    z[27]=z[52]*z[11];
    z[36]= - z[48] + 14*z[27] + 25*z[53];
    z[36]=z[2]*z[36];
    z[37]=5*z[52] - z[44];
    z[37]=z[6]*z[37];
    z[27]= - z[13] - 3*z[27];
    z[27]=z[37] - 30*z[53] + 5*z[27] + z[9];
    z[27]=z[6]*z[27];
    z[37]=z[42]*z[52];
    z[37]=static_cast<T>(3)+ z[37];
    z[27]=z[27] + 3*z[37] + z[36];
    z[27]=z[5]*z[27];
    z[36]=z[30]*z[52];
    z[37]=z[7]*z[57];
    z[37]=z[37] - 3*z[36] - z[44];
    z[37]=z[6]*z[37];
    z[27]=z[27] + z[37] + z[13] + z[7];
    z[27]=z[5]*z[27];
    z[36]= - z[19] + z[36];
    z[37]=z[54] + z[40];
    z[37]=z[37]*z[43];
    z[36]=3*z[36] + z[37];
    z[36]=z[9]*z[36];
    z[37]= - z[19] + 9*z[52];
    z[37]=z[9]*z[37];
    z[23]=z[23]*z[7];
    z[23]= - z[23] + z[37];
    z[23]=z[23]*z[35];
    z[23]=z[36] + z[23];
    z[23]=z[6]*z[23];
    z[36]=z[10]*z[12];
    z[36]=static_cast<T>(1)- 5*z[36];
    z[36]=z[7]*z[36];
    z[21]= - z[21] + z[36];
    z[21]=z[8]*z[21];
    z[19]= - z[19] + z[21];
    z[14]=z[14] + z[20] + z[27] + z[23] + 2*z[19] + z[22];
    z[14]=z[1]*z[14];
    z[19]= - static_cast<T>(26)+ z[41];
    z[19]=z[19]*z[28];
    z[20]= - z[6]*z[2];
    z[17]=z[17] + z[20];
    z[17]=z[17]*z[56];
    z[20]=z[50] - z[49];
    z[20]=z[20]*z[48];
    z[17]=z[20] + z[19] + z[17];
    z[17]=z[5]*z[17];
    z[19]=z[25] - 2;
    z[20]=z[19]*z[15];
    z[21]=6*z[2];
    z[22]= - z[21] + z[6];
    z[22]=z[6]*z[22];
    z[17]=z[17] - 12*z[20] + z[22];
    z[17]=z[17]*z[24];
    z[20]= - z[15]*z[58];
    z[22]= - z[29]*z[51];
    z[20]=z[20] + z[22];
    z[20]=z[20]*z[56];
    z[22]=z[26]*z[50];
    z[23]=3*z[28] - z[51];
    z[23]=z[6]*z[23];
    z[22]=z[22] + z[23];
    z[22]=z[5]*z[22];
    z[19]= - z[19]*z[28];
    z[19]=z[19] - z[51];
    z[19]=3*z[19] + z[22];
    z[19]=z[19]*z[24];
    z[22]=z[32] + 2;
    z[23]= - 5*z[22] + z[31];
    z[23]=z[23]*z[15];
    z[19]=z[19] + z[23] + z[20];
    z[19]=z[19]*z[33];
    z[20]=z[29]*z[34];
    z[20]= - 34*z[9] + z[20];
    z[20]=z[2]*z[20];
    z[23]= - z[9] - z[61];
    z[23]=z[23]*z[18];
    z[20]=z[20] + z[23];
    z[20]=z[6]*z[20];
    z[23]= - static_cast<T>(13)- z[46];
    z[25]=20*z[8] + z[58];
    z[25]=z[2]*z[25];
    z[23]=2*z[23] + z[25];
    z[23]=z[2]*z[23];
    z[17]=z[19] + z[17] + z[23] + z[20];
    z[17]=z[4]*z[17];
    z[19]= - static_cast<T>(15)+ z[38];
    z[15]=z[19]*z[15];
    z[19]=z[28] - z[51];
    z[19]=z[5]*z[19];
    z[20]=15*z[2] - z[6];
    z[20]=z[6]*z[20];
    z[15]=12*z[19] + 2*z[15] + z[20];
    z[15]=z[5]*z[15];
    z[16]= - z[16]*z[21];
    z[15]=z[15] + z[16] + 5*z[6];
    z[15]=z[15]*z[24];
    z[16]=z[55] + z[58];
    z[16]=z[2]*z[16];
    z[16]=z[16] - z[22];
    z[19]=z[6]*z[29];
    z[19]= - 6*z[19] - 25*z[9] + 12*z[60];
    z[19]=z[6]*z[19];
    z[15]=z[17] + z[15] + 2*z[16] + z[19];
    z[15]=z[4]*z[15];
    z[16]=z[2] - z[6];
    z[17]=z[34] + z[11];
    z[16]=z[48]*z[17]*z[16];
    z[17]= - static_cast<T>(35)+ z[38];
    z[17]=z[2]*z[17];
    z[19]=static_cast<T>(7)- z[30];
    z[19]=z[6]*z[19];
    z[16]=z[16] + z[19] - 9*z[11] + z[17];
    z[16]=z[5]*z[16];
    z[17]= - z[11]*z[13];
    z[17]= - z[30] - static_cast<T>(1)+ z[17];
    z[19]= - z[13]*z[35];
    z[19]=z[19] - z[39] + 5*z[7];
    z[19]=z[6]*z[19];
    z[16]=z[16] + 3*z[17] + z[19];
    z[16]=z[5]*z[16];
    z[17]=z[39] + z[54];
    z[19]=static_cast<T>(15)+ 8*z[30];
    z[19]=z[9]*z[19];
    z[19]=z[19] - z[7] - z[17];
    z[19]=z[9]*z[19];
    z[18]=z[7]*z[29]*z[18];
    z[18]=z[19] + z[18];
    z[18]=z[6]*z[18];
    z[17]=z[7] - z[17];
    z[19]= - z[11]*z[54];
    z[19]=5*z[30] + static_cast<T>(7)+ z[19];
    z[19]=z[9]*z[19];
    z[20]=z[34]*z[44];
    z[14]=z[14] + z[15] + z[16] + z[18] + z[20] + 2*z[17] + z[19];
    z[14]=z[1]*z[14];
    z[15]= - z[5]*z[6];
    z[15]=z[35] + z[15];

    r += z[14] + 12*z[15];
 
    return r;
}

template double qg_2lNLC_r1451(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1451(const std::array<dd_real,31>&);
#endif
