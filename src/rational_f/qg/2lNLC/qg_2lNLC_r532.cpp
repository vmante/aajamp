#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r532(const std::array<T,31>& k) {
  T z[79];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[2];
    z[9]=k[4];
    z[10]=k[10];
    z[11]=k[5];
    z[12]=k[15];
    z[13]=k[7];
    z[14]=k[3];
    z[15]=z[10]*z[9];
    z[16]=z[3]*z[9];
    z[17]=z[12]*z[11];
    z[18]=z[6]*z[11];
    z[19]=z[17] - 11*z[16] + 8*z[18] + static_cast<T>(9)- 4*z[15];
    z[19]=z[5]*z[19];
    z[20]=z[11]*z[14];
    z[21]=npow(z[14],2);
    z[22]=npow(z[14],3);
    z[23]=z[10]*z[22];
    z[23]=z[23] - 2*z[21] + z[20];
    z[23]=z[4]*z[23];
    z[24]=2*z[10];
    z[25]=z[21]*z[24];
    z[26]=6*z[14];
    z[27]=npow(z[11],2);
    z[28]=z[27]*z[13];
    z[23]=z[23] + z[25] - z[26] + z[28];
    z[25]=3*z[4];
    z[23]=z[23]*z[25];
    z[28]=z[11]*z[13];
    z[29]=3*z[28];
    z[30]=6*z[10];
    z[31]=z[30]*z[14];
    z[23]=z[23] - z[17] + z[31] - static_cast<T>(31)+ z[29];
    z[23]=z[4]*z[23];
    z[32]=z[3]*z[11];
    z[33]= - z[28] - z[32];
    z[33]=z[12]*z[33];
    z[34]=z[4]*z[21];
    z[34]=z[14] + z[34];
    z[34]=z[4]*z[34];
    z[34]=z[34] - static_cast<T>(1)- z[16];
    z[35]=6*z[7];
    z[34]=z[34]*z[35];
    z[36]=7*z[10];
    z[37]= - static_cast<T>(1)- 8*z[28];
    z[37]=z[6]*z[37];
    z[19]=z[34] + z[23] + z[19] + z[33] + 4*z[3] + z[36] + z[37];
    z[23]=2*z[7];
    z[19]=z[19]*z[23];
    z[33]=11*z[17];
    z[34]=z[33] - z[16] + 4*z[18] + static_cast<T>(15)- 8*z[15];
    z[37]=2*z[5];
    z[34]=z[34]*z[37];
    z[38]=8*z[12];
    z[39]=z[30] - n<T>(7,2)*z[6];
    z[34]=z[34] - z[38] + 3*z[39] + 14*z[3];
    z[34]=z[5]*z[34];
    z[39]=npow(z[13],2);
    z[40]=z[39]*z[11];
    z[41]=2*z[13];
    z[42]= - static_cast<T>(4)+ z[32];
    z[42]=z[3]*z[42];
    z[42]=z[42] - z[41] + z[40];
    z[42]=z[12]*z[42];
    z[43]=2*z[14];
    z[44]=z[21]*z[10];
    z[45]= - 3*z[44] + z[43] + z[11];
    z[45]=z[45]*z[25];
    z[46]=z[10]*z[14];
    z[33]=z[45] + z[33] + static_cast<T>(31)- 22*z[46];
    z[45]=2*z[4];
    z[33]=z[33]*z[45];
    z[46]=z[3] + z[6];
    z[47]=4*z[13];
    z[33]=z[33] + 6*z[12] + z[47] - n<T>(43,2)*z[10] - 12*z[46];
    z[33]=z[4]*z[33];
    z[48]=z[10]*z[13];
    z[49]=z[28] - 1;
    z[50]=z[49]*z[6];
    z[51]= - z[50] + 26*z[13] + z[10];
    z[51]=z[6]*z[51];
    z[52]= - z[41] - z[10];
    z[52]=z[3]*z[52];
    z[19]=z[19] + z[33] + z[34] + z[42] + z[52] + z[51] + z[39] - z[48];
    z[19]=z[7]*z[19];
    z[33]=z[27]*z[37];
    z[34]=z[28] - 2;
    z[42]=z[34]*z[11];
    z[51]=z[27]*z[6];
    z[33]=z[33] + z[42] - z[51];
    z[52]=3*z[5];
    z[33]=z[33]*z[52];
    z[53]=z[3]*z[8];
    z[54]=static_cast<T>(4)+ 5*z[18];
    z[33]=z[33] + 2*z[54] + 11*z[53];
    z[33]=z[33]*z[37];
    z[54]=z[11] + z[8];
    z[55]= - z[11]*z[54];
    z[56]= - z[6]*npow(z[11],3);
    z[55]=z[55] + z[56];
    z[55]=z[5]*z[55];
    z[56]=2*z[8];
    z[55]=z[56] + z[55];
    z[55]=z[5]*z[55];
    z[57]=z[43] - z[8];
    z[57]=z[10]*z[57];
    z[55]=z[55] + static_cast<T>(1)+ z[57];
    z[57]=6*z[4];
    z[55]=z[55]*z[57];
    z[58]=z[18]*z[12];
    z[59]= - static_cast<T>(2)- z[18];
    z[59]=z[6]*z[59];
    z[60]=z[10]*z[8];
    z[61]= - static_cast<T>(1)- 23*z[60];
    z[61]=z[3]*z[61];
    z[33]=z[55] + z[33] - 21*z[58] + z[61] + 31*z[10] + z[59];
    z[33]=z[4]*z[33];
    z[55]= - z[18] - static_cast<T>(8)+ z[28];
    z[59]=6*z[5];
    z[61]=z[11]*z[59];
    z[55]=z[61] + 3*z[55] - z[53];
    z[55]=z[55]*z[37];
    z[47]=z[55] - n<T>(19,2)*z[3] + z[47] + n<T>(21,2)*z[6];
    z[47]=z[5]*z[47];
    z[55]=5*z[10];
    z[61]= - static_cast<T>(13)+ z[18];
    z[61]=z[6]*z[61];
    z[61]= - z[55] + z[61];
    z[61]=2*z[61] - z[12];
    z[61]=z[12]*z[61];
    z[62]=npow(z[10],2);
    z[63]= - 4*z[10] - z[6];
    z[63]=z[6]*z[63];
    z[36]= - z[3]*z[36];
    z[33]=z[33] + z[47] + z[61] + z[36] - z[62] + z[63];
    z[33]=z[4]*z[33];
    z[36]= - static_cast<T>(14)- z[60];
    z[36]=z[10]*z[36];
    z[47]= - n<T>(47,2) + z[18];
    z[47]=z[6]*z[47];
    z[61]= - z[3]*z[18];
    z[36]=z[61] + z[36] + z[47];
    z[36]=z[3]*z[36];
    z[47]=z[13]*z[9];
    z[61]= - n<T>(47,2) + z[47];
    z[61]=z[13]*z[61];
    z[63]=2*z[47];
    z[64]=z[63] - 3;
    z[65]=z[10]*z[64];
    z[61]=z[61] + z[65];
    z[61]=z[10]*z[61];
    z[64]= - z[53] + z[64];
    z[64]=z[10]*z[64];
    z[64]=z[41] + z[64];
    z[64]=z[12]*z[64];
    z[65]=z[10] - z[13];
    z[66]=2*z[65] + z[6];
    z[66]=z[6]*z[66];
    z[36]=z[64] + z[36] + z[66] + z[39] + z[61];
    z[36]=z[12]*z[36];
    z[61]=z[3] + 8*z[10];
    z[61]=z[47]*z[61];
    z[64]=z[17]*z[59];
    z[66]=z[53] + 1;
    z[66]=z[66]*z[12];
    z[61]=z[64] - z[66] + 9*z[13] + z[61];
    z[61]=z[61]*z[37];
    z[64]=z[3]*z[13];
    z[67]= - n<T>(19,2) - z[47];
    z[67]=z[67]*z[64];
    z[68]= - z[13] + z[55];
    z[66]=z[66] + 2*z[68] - z[3];
    z[66]=z[12]*z[66];
    z[68]=z[6]*z[10];
    z[69]=28*z[13] + z[10];
    z[69]=z[10]*z[69];
    z[61]=z[61] + z[66] + z[67] + z[69] + z[68];
    z[61]=z[5]*z[61];
    z[66]= - z[50] - n<T>(47,2)*z[13] - z[40];
    z[66]=z[6]*z[66];
    z[67]=z[34]*z[18];
    z[67]=z[67] + static_cast<T>(3)- z[28];
    z[69]=z[3]*z[6];
    z[67]=z[67]*z[69];
    z[66]=z[67] - z[39] + z[66];
    z[66]=z[3]*z[66];
    z[67]=z[6]*z[13];
    z[70]=5*z[48] + z[67];
    z[70]=z[6]*z[70];
    z[71]=z[62]*z[13];
    z[19]=z[19] + z[33] + z[61] + z[36] + z[66] + z[71] + z[70];
    z[33]=npow(z[2],2);
    z[19]=z[19]*z[33];
    z[36]=2*z[12];
    z[61]=z[13] + z[30];
    z[61]=z[61]*z[36];
    z[66]=3*z[10];
    z[70]=z[14]*z[66];
    z[70]=z[70] - z[17];
    z[70]=z[70]*z[25];
    z[70]=z[70] - 4*z[12] + z[41] + 11*z[10];
    z[70]=z[70]*z[45];
    z[72]=6*z[13];
    z[73]=z[72] + z[10];
    z[73]=z[6]*z[73];
    z[74]=3*z[13];
    z[75]=z[74] - z[10];
    z[75]=z[3]*z[75];
    z[76]=z[5]*z[46];
    z[61]=z[70] + 12*z[76] + z[61] + z[73] + n<T>(7,2)*z[75];
    z[61]=z[4]*z[61];
    z[70]=z[18] + 1;
    z[73]=3*z[16] + z[70];
    z[73]=z[5]*z[73];
    z[75]=z[11]*z[49];
    z[75]= - z[14] + z[75];
    z[75]=z[4]*z[75];
    z[75]=z[75] - z[49];
    z[75]=z[4]*z[75];
    z[76]=z[18]*z[13];
    z[73]=z[75] - z[76] + z[73];
    z[73]=z[73]*z[35];
    z[75]= - z[13] + z[3];
    z[75]=z[75]*z[36];
    z[77]=z[15] - z[70];
    z[77]=z[77]*z[59];
    z[77]=z[77] + z[12] - 11*z[3] - 13*z[10] + 10*z[6];
    z[77]=z[77]*z[37];
    z[78]=z[11] - z[44];
    z[78]=z[78]*npow(z[4],2);
    z[55]=3*z[78] + z[12] - 12*z[13] + z[55];
    z[55]=z[55]*z[45];
    z[50]= - 21*z[13] + z[50];
    z[50]=z[6]*z[50];
    z[50]=z[73] + z[55] + z[77] + z[50] + z[75];
    z[50]=z[7]*z[50];
    z[55]= - 3*z[17] - static_cast<T>(3)- z[15];
    z[55]=z[55]*z[52];
    z[36]=z[55] - z[36] + z[3] - z[10] - 3*z[6];
    z[36]=z[5]*z[36];
    z[55]= - z[24] + z[3];
    z[55]=z[12]*z[55];
    z[36]=z[36] + z[68] + 3*z[55];
    z[36]=z[36]*z[37];
    z[55]=z[65]*z[69];
    z[73]= - z[41] + z[10];
    z[73]=z[73]*npow(z[6],2);
    z[36]=z[50] + z[61] + z[36] + z[73] - n<T>(19,2)*z[55];
    z[36]=z[7]*z[36];
    z[50]=z[17] + static_cast<T>(1)- z[15];
    z[50]=z[5]*z[50];
    z[50]=z[50] + z[12] + 2*z[3] + z[24] - z[6];
    z[50]=z[50]*z[52];
    z[55]=z[30] - z[3];
    z[55]=z[12]*z[55];
    z[50]=z[50] - z[68] + z[55];
    z[50]=z[5]*z[50];
    z[55]= - z[4]*z[17];
    z[55]=z[12] + z[55];
    z[55]=z[55]*z[25];
    z[61]=z[13] - z[30];
    z[61]=z[12]*z[61];
    z[55]=z[61] + z[55];
    z[55]=z[4]*z[55];
    z[61]= - z[16] + z[70];
    z[73]=npow(z[5],2);
    z[61]=z[7]*z[61]*z[73];
    z[50]=3*z[61] + z[50] + z[55];
    z[50]=z[7]*z[50];
    z[55]=z[12]*z[3];
    z[61]= - z[68] - z[55];
    z[61]=z[61]*z[73];
    z[46]=z[46]*z[73];
    z[75]=z[12]*z[13];
    z[77]= - z[4]*z[75];
    z[46]= - 6*z[46] + z[77];
    z[46]=z[4]*z[46];
    z[46]=z[50] + z[61] + z[46];
    z[23]=z[46]*z[23];
    z[46]= - z[62] - z[68];
    z[46]=z[6]*z[46];
    z[50]= - z[10] + z[6];
    z[50]=z[50]*z[69];
    z[46]=z[46] + z[50];
    z[46]=z[3]*z[46];
    z[50]= - z[68]*z[55];
    z[46]=z[46] + z[50];
    z[46]=z[12]*z[46];
    z[50]=z[53] - 1;
    z[25]= - z[50]*z[73]*z[25];
    z[59]=z[3]*z[59];
    z[59]= - z[64] + z[59];
    z[59]=z[5]*z[59];
    z[25]=z[59] + z[25];
    z[25]=z[4]*z[25];
    z[59]=z[73]*z[64];
    z[25]=z[59] + z[25];
    z[25]=z[25]*z[45];
    z[59]= - z[3]*npow(z[4],3);
    z[61]=z[6]*npow(z[7],3);
    z[59]=z[59] + z[61];
    z[59]=z[1]*z[73]*z[59];
    z[23]=6*z[59] + z[23] + z[46] + z[25];
    z[23]=z[1]*z[23];
    z[25]= - 16*z[10] - z[6];
    z[25]=z[6]*z[25];
    z[46]=z[18] - 1;
    z[46]=z[6]*z[46];
    z[46]= - z[10] + z[46];
    z[46]=z[3]*z[46];
    z[25]=z[46] - z[62] + z[25];
    z[25]=z[3]*z[25];
    z[30]= - z[13]*z[30];
    z[30]=z[30] + z[68];
    z[30]=z[6]*z[30];
    z[46]= - z[10] - z[6];
    z[46]=z[3]*z[46];
    z[46]= - z[48] + z[46];
    z[46]=z[12]*z[46];
    z[25]=z[46] + z[25] - z[71] + z[30];
    z[25]=z[12]*z[25];
    z[30]=z[10]*z[47];
    z[30]=z[74] + z[30];
    z[30]=z[30]*z[52];
    z[46]=z[41]*z[3];
    z[30]=z[30] + z[55] - 11*z[48] - z[46];
    z[30]=z[30]*z[37];
    z[48]=3*z[48] - 7*z[68];
    z[48]=z[12]*z[48];
    z[55]=z[6]*z[62];
    z[39]=z[3]*z[39];
    z[30]=z[30] + n<T>(3,2)*z[48] + z[55] + z[39];
    z[30]=z[5]*z[30];
    z[39]= - z[51] + z[8] - z[11];
    z[39]=z[5]*z[39];
    z[39]=z[39] - z[18] - z[50];
    z[39]=z[5]*z[39];
    z[48]=z[10]*z[53];
    z[39]=z[39] + z[58] - z[24] + z[48];
    z[39]=z[39]*z[57];
    z[27]=z[27]*z[5];
    z[48]=z[13]*z[27];
    z[48]= - 2*z[70] + z[48];
    z[48]=z[48]*z[52];
    z[50]=5*z[3] + z[13] + 4*z[6];
    z[48]=2*z[50] + z[48];
    z[48]=z[48]*z[37];
    z[50]= - static_cast<T>(1)+ z[60];
    z[50]=z[3]*z[50];
    z[50]= - 21*z[10] + z[50];
    z[50]=z[3]*z[50];
    z[55]= - static_cast<T>(16)+ z[18];
    z[55]=z[12]*z[6]*z[55];
    z[39]=z[39] + z[48] + z[50] + z[55];
    z[39]=z[4]*z[39];
    z[48]= - z[72] + z[6];
    z[50]=z[5]*z[28];
    z[48]=2*z[48] + 9*z[50];
    z[48]=z[48]*z[37];
    z[50]=z[13] - z[6];
    z[50]=z[12]*z[50];
    z[46]=z[48] + z[46] + n<T>(9,2)*z[50];
    z[46]=z[5]*z[46];
    z[48]=6*z[65] + z[6];
    z[48]=z[6]*z[48];
    z[48]=z[48] - z[75];
    z[48]=z[12]*z[48];
    z[50]= - z[13] + z[24];
    z[50]=z[50]*npow(z[3],2);
    z[39]=z[39] + z[46] + z[50] + z[48];
    z[39]=z[4]*z[39];
    z[46]=z[62] + z[67];
    z[46]=z[6]*z[46];
    z[34]=z[15] + z[34];
    z[34]=z[6]*z[34];
    z[34]=z[34] - z[13] + z[66];
    z[34]=z[34]*z[69];
    z[34]=z[46] + z[34];
    z[34]=z[3]*z[34];
    z[23]=z[23] + z[36] + z[39] + z[30] + z[34] + z[25];
    z[23]=z[1]*z[33]*z[23];
    z[19]=z[19] + z[23];
    z[19]=z[1]*z[19];
    z[23]=6*z[28];
    z[25]=z[23] - 5;
    z[30]=z[11]*z[25];
    z[26]= - z[44] - z[26] + z[30];
    z[26]=z[26]*z[45];
    z[30]=z[32] + z[18];
    z[26]=z[26] + n<T>(9,2)*z[17] - z[31] - n<T>(43,2) + 12*z[28] + 7*z[30];
    z[26]=z[4]*z[26];
    z[15]=z[17] - z[16] + z[15] - z[18];
    z[15]=z[5]*z[15];
    z[30]=z[21]*z[57];
    z[31]=z[43] - z[11];
    z[30]=z[30] + 11*z[31] + z[44];
    z[30]=z[30]*z[45];
    z[20]= - z[21] + z[20];
    z[21]=z[11]*z[21];
    z[21]= - z[22] + z[21];
    z[21]=z[4]*z[21];
    z[20]=3*z[20] + z[21];
    z[20]=z[4]*z[20];
    z[21]=z[16] + 3;
    z[21]=z[11]*z[21];
    z[20]=z[20] - 3*z[14] - z[9] + z[21];
    z[20]=z[20]*z[35];
    z[21]=z[43] + z[9];
    z[21]=z[10]*z[21];
    z[21]=static_cast<T>(27)+ z[21];
    z[16]= - z[16] - 22;
    z[16]=z[11]*z[16];
    z[16]=23*z[9] + z[16];
    z[16]=z[3]*z[16];
    z[16]=z[20] + z[30] + 2*z[21] + z[16];
    z[16]=z[7]*z[16];
    z[20]= - n<T>(9,2) + z[63];
    z[20]=z[20]*z[66];
    z[21]= - z[28] + z[32];
    z[21]=z[12]*z[21];
    z[22]=16*z[13];
    z[30]=static_cast<T>(4)+ n<T>(9,2)*z[28];
    z[30]=z[6]*z[30];
    z[31]=static_cast<T>(7)- 2*z[32];
    z[31]=z[3]*z[31];
    z[15]=z[16] + z[26] + n<T>(9,2)*z[15] + n<T>(21,2)*z[21] + z[31] + z[30] + 
    z[22] + z[20];
    z[15]=z[7]*z[15];
    z[16]=z[56] - z[11];
    z[20]=z[52]*z[8];
    z[21]= - z[11]*z[20];
    z[21]=z[21] - 4*z[16] + z[51];
    z[21]=z[21]*z[37];
    z[26]=z[23] - z[70];
    z[21]=z[21] + 2*z[26] + n<T>(9,2)*z[53];
    z[21]=z[5]*z[21];
    z[16]= - z[11]*z[16];
    z[26]=z[8]*z[27];
    z[16]=z[16] + z[26];
    z[16]=z[16]*z[52];
    z[23]= - static_cast<T>(7)+ z[23];
    z[23]=z[11]*z[23];
    z[16]=z[16] - z[51] + 4*z[8] + z[23];
    z[16]=z[5]*z[16];
    z[23]=z[14] - z[8];
    z[23]=z[10]*z[23];
    z[16]=z[16] + static_cast<T>(4)+ z[23];
    z[16]=z[16]*z[45];
    z[23]= - static_cast<T>(1)+ 2*z[60];
    z[23]=3*z[23] - n<T>(7,2)*z[18];
    z[23]=z[12]*z[23];
    z[26]=static_cast<T>(4)- z[60];
    z[26]=z[10]*z[26];
    z[27]= - z[6]*z[70];
    z[30]=static_cast<T>(1)- n<T>(3,2)*z[60];
    z[30]=z[3]*z[30];
    z[16]=z[16] + z[21] + z[23] + 3*z[30] + z[26] + z[27];
    z[16]=z[4]*z[16];
    z[21]= - z[22] - z[40];
    z[21]=z[11]*z[21];
    z[23]= - static_cast<T>(1)- z[28];
    z[23]=z[23]*z[18];
    z[21]=z[21] + z[23];
    z[21]=z[6]*z[21];
    z[23]= - z[9]*z[18];
    z[23]= - z[42] + z[23];
    z[23]=z[6]*z[23];
    z[23]= - z[28] + z[23];
    z[23]=z[3]*z[23];
    z[26]= - static_cast<T>(15)+ z[47];
    z[26]=z[13]*z[26];
    z[21]=z[23] + z[21] - z[24] + z[26] - z[40];
    z[21]=z[3]*z[21];
    z[23]= - z[54]*z[38];
    z[17]= - z[17]*z[20];
    z[17]=z[23] + z[17];
    z[17]=z[17]*z[37];
    z[20]=z[60] - z[18];
    z[20]=static_cast<T>(22)- z[29] - 3*z[20];
    z[20]=2*z[20] + n<T>(21,2)*z[53];
    z[20]=z[12]*z[20];
    z[23]= - static_cast<T>(2)- n<T>(9,2)*z[47];
    z[23]=z[10]*z[23];
    z[24]=z[3]*z[47];
    z[17]=z[17] + z[20] + n<T>(21,2)*z[24] + z[6] + z[22] + z[23];
    z[17]=z[5]*z[17];
    z[20]=z[13]*npow(z[9],2);
    z[22]= - 2*z[9] + z[20] + z[8];
    z[22]=z[22]*z[10];
    z[23]=16*z[9] - z[20];
    z[23]=z[13]*z[23];
    z[23]= - z[22] - n<T>(47,2) + z[23];
    z[23]=z[10]*z[23];
    z[24]= - n<T>(19,2)*z[18] + static_cast<T>(2)- n<T>(21,2)*z[60];
    z[24]=z[3]*z[24];
    z[22]= - z[22] + static_cast<T>(1)- z[47];
    z[22]=z[12]*z[22];
    z[25]= - z[6]*z[25];
    z[22]=z[22] + z[24] + z[25] - n<T>(19,2)*z[13] + z[23];
    z[22]=z[12]*z[22];
    z[23]=z[41] + z[76];
    z[23]=z[6]*z[23];
    z[24]=static_cast<T>(2)- z[47];
    z[24]=z[10]*z[24];
    z[24]=z[13] + z[24];
    z[24]=z[10]*z[24];
    z[15]=z[15] + z[16] + z[17] + z[22] + z[21] + z[24] + z[23];
    z[15]=z[15]*z[33];
    z[15]=z[15] + z[19];
    z[15]=z[1]*z[15];
    z[16]= - z[49]*z[18];
    z[16]=z[16] + z[47] - z[28];
    z[16]=z[3]*z[16];
    z[17]=z[9] - z[20];
    z[17]=z[12]*z[17];
    z[17]=z[17] + 1;
    z[17]=z[10]*z[17];
    z[18]=z[5]*z[70];
    z[19]= - z[14] - z[9];
    z[19]=z[10]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[7]*z[19];
    z[16]=z[19] + z[18] + z[16] + z[17];
    z[16]=z[16]*z[33];
    z[15]=z[16] + z[15];

    r += z[15]*z[1];
 
    return r;
}

template double qg_2lNLC_r532(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r532(const std::array<dd_real,31>&);
#endif
