#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r963(const std::array<T,31>& k) {
  T z[113];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[6];
    z[9]=k[7];
    z[10]=k[2];
    z[11]=k[14];
    z[12]=k[3];
    z[13]=k[13];
    z[14]=k[9];
    z[15]=npow(z[1],3);
    z[16]=z[15]*z[5];
    z[17]=n<T>(163,2)*z[16];
    z[18]=npow(z[1],2);
    z[19]= - 202*z[18] - z[17];
    z[20]=z[6]*z[1];
    z[19]=n<T>(1,5)*z[19] + 73*z[20];
    z[21]=z[7]*z[1];
    z[19]=n<T>(1,3)*z[19] - n<T>(189,10)*z[21];
    z[19]=z[7]*z[19];
    z[22]=z[18]*z[6];
    z[23]=npow(z[1],4);
    z[24]=z[23]*z[5];
    z[19]=z[19] + n<T>(176,5)*z[22] + 10*z[15] - n<T>(163,10)*z[24];
    z[19]=z[7]*z[19];
    z[25]=npow(z[1],5);
    z[26]=z[25]*z[5];
    z[27]=z[26] - z[23];
    z[28]=z[15]*z[6];
    z[29]= - n<T>(163,2)*z[27] + 113*z[28];
    z[19]=n<T>(1,5)*z[29] + z[19];
    z[19]=z[4]*z[19];
    z[17]=n<T>(541,2)*z[20] - 13*z[18] - z[17];
    z[17]=z[7]*z[17];
    z[17]=n<T>(1,3)*z[17] + 113*z[15] - n<T>(163,3)*z[24];
    z[29]=z[23]*z[6];
    z[30]=npow(z[1],6);
    z[31]=z[30]*z[5];
    z[32]=z[25] + z[29] - z[31];
    z[32]=z[32]*z[4];
    z[32]=z[32] - z[27];
    z[33]=z[3]*z[32];
    z[34]=z[21] + z[18];
    z[35]=z[34] - z[20];
    z[36]=npow(z[7],2);
    z[37]=z[35]*z[36];
    z[34]=z[34]*z[7];
    z[38]=z[15] + 3*z[34];
    z[39]=z[36]*z[4];
    z[38]=z[38]*z[39];
    z[38]=3*z[37] + z[38];
    z[38]=z[9]*z[38];
    z[17]=n<T>(63,10)*z[38] + n<T>(163,30)*z[33] + n<T>(1,5)*z[17] + z[19];
    z[17]=z[2]*z[17];
    z[19]=z[18]*z[5];
    z[33]=2977*z[1] - 1283*z[19];
    z[33]=z[7]*z[33];
    z[33]=n<T>(1,30)*z[33] + n<T>(2047,10)*z[18] - n<T>(259,3)*z[16];
    z[33]=z[7]*z[33];
    z[38]=1117*z[15] - n<T>(1331,3)*z[24];
    z[33]=n<T>(1,10)*z[38] + z[33];
    z[33]=z[4]*z[33];
    z[38]=z[18] + n<T>(1,2)*z[21];
    z[40]=n<T>(1,2)*z[20] - z[38];
    z[40]=z[7]*z[40];
    z[38]= - z[7]*z[38];
    z[41]=n<T>(1,2)*z[15];
    z[38]= - z[41] + z[38];
    z[42]=z[4]*z[7];
    z[38]=z[38]*z[42];
    z[38]=z[40] + z[38];
    z[38]=z[9]*z[38];
    z[40]=n<T>(1,5)*z[7];
    z[43]=241*z[1] - n<T>(1283,6)*z[19];
    z[43]=z[43]*z[40];
    z[44]=n<T>(187,6)*z[23] - 4*z[26];
    z[44]=z[4]*z[44];
    z[44]= - n<T>(187,6)*z[24] + z[44];
    z[45]=n<T>(1,5)*z[3];
    z[44]=z[44]*z[45];
    z[17]=z[17] + n<T>(1531,15)*z[38] + z[44] + z[33] + z[43] + n<T>(26,15)*
    z[20] + n<T>(1829,30)*z[18] - 49*z[16];
    z[17]=z[2]*z[17];
    z[33]= - z[24] + n<T>(9,5)*z[15];
    z[38]=32*z[5];
    z[43]=z[33]*z[38];
    z[43]= - n<T>(779,10)*z[18] + z[43];
    z[43]=z[5]*z[43];
    z[44]=n<T>(7,5)*z[18] - z[16];
    z[44]=z[5]*z[44];
    z[46]=n<T>(2,5)*z[1];
    z[44]= - z[46] + z[44];
    z[47]=8*z[5];
    z[44]=z[7]*z[44]*z[47];
    z[43]=z[44] - n<T>(9459,10)*z[1] + z[43];
    z[43]=z[7]*z[43];
    z[44]= - z[26] + n<T>(11,5)*z[23];
    z[48]=16*z[5];
    z[44]=z[44]*z[48];
    z[49]= - n<T>(1953,20)*z[15] + z[44];
    z[49]=z[5]*z[49];
    z[43]=3*z[43] + n<T>(13989,10)*z[20] - n<T>(25807,20)*z[18] + 9*z[49];
    z[43]=z[7]*z[43];
    z[49]=n<T>(12,5)*z[15] - z[24];
    z[49]=z[5]*z[49];
    z[50]=n<T>(9,5)*z[18];
    z[49]= - z[50] + z[49];
    z[51]=2*z[18] - z[16];
    z[51]=z[5]*z[51];
    z[51]= - z[1] + z[51];
    z[51]=z[7]*z[51];
    z[49]=n<T>(24,5)*z[51] + 24*z[49];
    z[49]=z[5]*z[49];
    z[49]= - n<T>(4943,5)*z[1] + z[49];
    z[49]=z[7]*z[49];
    z[51]=n<T>(14,5)*z[23] - z[26];
    z[51]=z[51]*z[48];
    z[51]= - n<T>(271,4)*z[15] + z[51];
    z[52]=3*z[5];
    z[51]=z[51]*z[52];
    z[49]=z[49] + n<T>(21533,20)*z[20] - n<T>(17627,20)*z[18] + z[51];
    z[49]=z[7]*z[49];
    z[51]=n<T>(16,5)*z[25] - z[31];
    z[48]=z[51]*z[48];
    z[48]= - n<T>(2657,20)*z[23] + z[48];
    z[48]=z[48]*z[52];
    z[48]=z[49] + n<T>(24671,20)*z[22] - n<T>(2387,60)*z[15] + z[48];
    z[48]=z[7]*z[48];
    z[49]=npow(z[1],7);
    z[51]=z[49]*z[5];
    z[53]=n<T>(18,5)*z[30] - z[51];
    z[53]=z[53]*z[47];
    z[53]= - n<T>(2241,20)*z[25] + z[53];
    z[53]=z[5]*z[53];
    z[54]=n<T>(365,4)*z[23];
    z[53]=z[54] + z[53];
    z[48]=z[48] + 3*z[53] + n<T>(6817,12)*z[28];
    z[48]=z[4]*z[48];
    z[53]= - z[31] + n<T>(13,5)*z[25];
    z[55]=z[53]*z[38];
    z[55]= - 365*z[23] + z[55];
    z[55]=z[55]*z[52];
    z[43]=z[48] + z[43] + n<T>(5339,5)*z[22] + n<T>(1826,3)*z[15] + z[55];
    z[43]=z[4]*z[43];
    z[48]=n<T>(864,5)*z[15] - n<T>(331,2)*z[24];
    z[48]=z[5]*z[48];
    z[55]=n<T>(192,5)*z[18] - n<T>(331,6)*z[16];
    z[55]=z[5]*z[55];
    z[55]= - n<T>(12247,60)*z[1] + z[55];
    z[55]=z[7]*z[55];
    z[48]=z[55] - n<T>(23699,30)*z[18] + z[48];
    z[48]=z[5]*z[48];
    z[48]= - n<T>(3999,2)*z[1] + z[48];
    z[48]=z[7]*z[48];
    z[55]=n<T>(1152,5)*z[23] - n<T>(331,2)*z[26];
    z[55]=z[5]*z[55];
    z[55]= - n<T>(43189,40)*z[15] + z[55];
    z[55]=z[5]*z[55];
    z[43]=z[43] + z[48] + n<T>(1277,3)*z[20] + n<T>(10697,30)*z[18] + z[55];
    z[43]=z[4]*z[43];
    z[48]= - z[5]*npow(z[1],8);
    z[48]=4*z[49] + z[48];
    z[48]=z[48]*z[47];
    z[48]= - n<T>(683,4)*z[30] + z[48];
    z[48]=z[5]*z[48];
    z[49]=n<T>(587,4)*z[25];
    z[48]=n<T>(523,4)*z[29] + z[49] + z[48];
    z[48]=z[4]*z[48];
    z[30]= - z[51] + 3*z[30];
    z[30]=z[30]*z[47];
    z[30]=z[30] - z[49];
    z[30]=z[30]*z[5];
    z[49]=n<T>(523,4)*z[23];
    z[51]=z[49] + z[30];
    z[48]=n<T>(3,5)*z[48] + 3*z[51] + n<T>(7367,30)*z[28];
    z[48]=z[4]*z[48];
    z[51]=96*z[25] - n<T>(331,6)*z[31];
    z[51]=z[5]*z[51];
    z[51]= - n<T>(25877,120)*z[23] + z[51];
    z[51]=z[5]*z[51];
    z[48]=z[48] - n<T>(3779,60)*z[22] + n<T>(9059,30)*z[15] + z[51];
    z[48]=z[4]*z[48];
    z[51]=n<T>(1,60)*z[3];
    z[32]= - z[32]*z[51];
    z[55]=n<T>(1,3)*z[24];
    z[56]= - n<T>(1,4)*z[15] + z[55];
    z[57]=n<T>(1,3)*z[27] - n<T>(1,5)*z[28];
    z[58]=n<T>(1,4)*z[4];
    z[57]=z[57]*z[58];
    z[32]=z[32] + n<T>(1,5)*z[56] + z[57];
    z[32]=z[3]*z[32];
    z[56]=649*z[23] - 1079*z[26];
    z[57]=n<T>(1,3)*z[5];
    z[56]=z[56]*z[57];
    z[56]= - n<T>(6587,4)*z[15] + z[56];
    z[56]=z[5]*z[56];
    z[56]=n<T>(967,3)*z[18] + n<T>(1,2)*z[56];
    z[32]=n<T>(8347,2)*z[32] + z[48] + n<T>(1,5)*z[56] - n<T>(513,8)*z[20];
    z[32]=z[3]*z[32];
    z[34]=z[34] + n<T>(1,3)*z[15];
    z[34]=z[34]*z[39];
    z[48]=3*z[18];
    z[56]=z[48] + 4*z[21];
    z[56]=z[7]*z[56];
    z[56]=n<T>(2,3)*z[15] + z[56];
    z[56]=z[7]*z[56];
    z[56]=z[56] + z[34];
    z[59]=4991*z[4];
    z[56]=z[56]*z[59];
    z[60]=n<T>(95611,3)*z[18] + 70647*z[21];
    z[60]=z[7]*z[60];
    z[60]=n<T>(8711,3)*z[15] + z[60];
    z[56]=n<T>(1,2)*z[60] + z[56];
    z[56]=z[4]*z[56];
    z[34]= - z[37] - z[34];
    z[34]=z[9]*z[34];
    z[37]= - z[18] + 2*z[20];
    z[37]=n<T>(476,3)*z[37] - n<T>(1493,4)*z[21];
    z[37]=z[7]*z[37];
    z[34]=z[37] + n<T>(1493,4)*z[34];
    z[34]=z[9]*z[34];
    z[34]=z[34] + z[56] + n<T>(424561,24)*z[21] + 4906*z[18] - n<T>(124411,24)*
    z[20];
    z[37]=n<T>(1,5)*z[9];
    z[34]=z[34]*z[37];
    z[56]=n<T>(1081,2)*z[15] - 1079*z[24];
    z[60]=n<T>(1,5)*z[5];
    z[56]=z[56]*z[60];
    z[56]= - n<T>(5315,4)*z[18] + z[56];
    z[56]=z[5]*z[56];
    z[56]=n<T>(2647,10)*z[1] + z[56];
    z[61]=72*z[18] - n<T>(1079,6)*z[16];
    z[61]=z[5]*z[61];
    z[61]= - n<T>(11959,12)*z[1] + z[61];
    z[61]=z[5]*z[61];
    z[61]=n<T>(1651,4) + z[61];
    z[61]=z[61]*z[40];
    z[17]=z[17] + z[34] + z[32] + z[43] + z[61] + n<T>(1,3)*z[56] - n<T>(1651,10)
   *z[6];
    z[17]=z[2]*z[17];
    z[32]=z[23]*z[7];
    z[34]=npow(z[6],3);
    z[43]=z[34]*z[32];
    z[56]=npow(z[20],4);
    z[43]=z[56] + n<T>(1,2)*z[43];
    z[61]=npow(z[5],3);
    z[43]=z[61]*z[43];
    z[62]=npow(z[7],4);
    z[63]=z[62]*z[23];
    z[64]=npow(z[4],3);
    z[65]=z[64]*z[63];
    z[43]=13*z[43] - n<T>(322,5)*z[65];
    z[65]=z[34]*z[24];
    z[66]=n<T>(1,2)*z[7];
    z[67]=z[66]*z[65];
    z[68]=z[56]*z[5];
    z[67]=z[68] + z[67];
    z[69]=z[4]*z[63];
    z[70]=z[56]*z[3];
    z[67]=n<T>(121,2)*z[70] + 121*z[67] + n<T>(1493,5)*z[69];
    z[67]=z[9]*z[67];
    z[69]=z[3]*z[68];
    z[67]= - n<T>(605,2)*z[69] + z[67];
    z[69]=n<T>(1,6)*z[9];
    z[67]=z[67]*z[69];
    z[71]=npow(z[3],2);
    z[72]=npow(z[5],2);
    z[73]=96*z[72] + n<T>(437,120)*z[71];
    z[70]=z[73]*z[70];
    z[43]=z[67] + n<T>(31,3)*z[43] + z[70];
    z[43]=z[9]*z[43];
    z[29]=z[32] - z[29];
    z[32]=z[29]*z[7];
    z[67]=npow(z[6],2);
    z[70]=z[67]*z[23];
    z[32]=z[32] + z[70];
    z[32]=z[32]*z[36];
    z[73]=npow(z[9],2);
    z[74]=z[73]*z[32];
    z[75]=npow(z[4],2);
    z[63]=z[75]*z[63];
    z[63]=4991*z[63] - n<T>(1493,20)*z[74];
    z[63]=z[9]*z[63];
    z[74]=npow(z[42],3);
    z[76]=z[29]*z[74];
    z[63]= - n<T>(4991,5)*z[76] + z[63];
    z[32]=z[9]*z[32];
    z[76]=npow(z[7],3);
    z[77]=z[76]*z[4];
    z[29]= - z[29]*z[77];
    z[29]=z[29] + z[32];
    z[32]=npow(z[2],2);
    z[29]=z[29]*z[32];
    z[29]=n<T>(1,3)*z[63] + n<T>(63,10)*z[29];
    z[29]=z[2]*z[29];
    z[63]=n<T>(121,2)*z[73] + n<T>(437,20)*z[71];
    z[78]=npow(z[20],5);
    z[63]=z[63]*z[78]*z[5];
    z[78]=z[61]*z[78];
    z[63]=403*z[78] + z[63];
    z[63]=z[69]*z[3]*z[63];
    z[78]=npow(z[21],5);
    z[64]=z[64]*z[78];
    z[78]=z[78]*z[4];
    z[79]=z[73]*z[78];
    z[64]=4991*z[64] - n<T>(1493,4)*z[79];
    z[79]=n<T>(1,3)*z[9];
    z[64]=z[64]*z[79];
    z[78]=z[9]*z[32]*z[78];
    z[64]=z[64] + n<T>(63,2)*z[78];
    z[64]=z[2]*z[64];
    z[63]=z[63] + n<T>(1,5)*z[64];
    z[63]=z[8]*z[63];
    z[56]=z[61]*z[56];
    z[64]=z[71]*z[68];
    z[56]= - 1439*z[56] - n<T>(437,4)*z[64];
    z[56]=z[3]*z[56];
    z[29]=z[63] + z[29] + n<T>(1,10)*z[56] + z[43];
    z[29]=z[8]*z[29];
    z[43]=z[34]*z[15];
    z[56]=z[43]*z[72];
    z[63]=z[67]*z[15];
    z[64]=3*z[72];
    z[68]=z[7]*z[63]*z[64];
    z[68]=8*z[56] + z[68];
    z[78]=z[76]*z[75]*z[15];
    z[68]=12*z[68] - n<T>(4991,3)*z[78];
    z[78]=z[16]*z[3];
    z[80]=z[34]*z[78];
    z[68]=8*z[68] + n<T>(3231,8)*z[80];
    z[80]=z[15]*z[7];
    z[81]=z[80] - z[28];
    z[82]=z[81]*z[7];
    z[82]=z[82] + z[63];
    z[83]=z[7]*z[82];
    z[83]=121*z[43] + n<T>(1493,5)*z[83];
    z[83]=z[83]*z[79];
    z[84]=z[43]*z[3];
    z[85]=z[7]*z[67];
    z[85]= - 137*z[34] + n<T>(2203,6)*z[85];
    z[85]=z[16]*z[85];
    z[83]=z[83] + n<T>(1,5)*z[85] - n<T>(605,6)*z[84];
    z[85]=n<T>(1,2)*z[9];
    z[83]=z[83]*z[85];
    z[68]=n<T>(1,5)*z[68] + z[83];
    z[68]=z[9]*z[68];
    z[83]= - z[63]*z[66];
    z[43]= - 2*z[43] + z[83];
    z[43]=z[61]*z[43];
    z[83]=z[15]*z[74];
    z[43]=1439*z[43] + 9982*z[83];
    z[83]=n<T>(3389,3)*z[16];
    z[86]=z[34]*z[83];
    z[84]=z[86] - 437*z[84];
    z[84]=z[3]*z[84];
    z[56]= - 864*z[56] + n<T>(1,8)*z[84];
    z[56]=z[3]*z[56];
    z[43]=n<T>(1,3)*z[43] + z[56];
    z[56]=z[75]*z[36];
    z[75]=865*z[28] - n<T>(9982,5)*z[80];
    z[75]=z[75]*z[56];
    z[84]=n<T>(1,2)*z[80];
    z[86]=z[28] - z[84];
    z[86]=z[7]*z[86];
    z[86]= - 476*z[63] + n<T>(1493,2)*z[86];
    z[86]=z[9]*z[7]*z[86];
    z[87]=z[15]*z[77];
    z[86]=n<T>(134191,2)*z[87] + z[86];
    z[86]=z[86]*z[37];
    z[75]=2*z[75] + z[86];
    z[86]=n<T>(1,3)*z[2];
    z[75]=z[75]*z[86];
    z[29]=z[29] + z[75] + n<T>(1,5)*z[43] + z[68];
    z[29]=z[8]*z[29];
    z[43]=z[67]*z[18];
    z[68]=z[18]*z[7];
    z[75]= - z[22] + n<T>(1,2)*z[68];
    z[87]=z[7]*z[75];
    z[87]=191*z[43] + n<T>(1493,3)*z[87];
    z[37]=z[87]*z[37];
    z[87]=z[61]*z[6];
    z[88]=z[64] + z[87];
    z[89]=z[88]*z[6];
    z[90]=48*z[89] + n<T>(289,6)*z[5];
    z[90]=z[90]*z[6];
    z[91]=z[90] - n<T>(287,6);
    z[92]=n<T>(1,5)*z[6];
    z[93]=z[91]*z[92];
    z[93]= - n<T>(2231,12)*z[19] + z[93];
    z[93]=z[93]*z[67];
    z[94]=z[87] + 2*z[72];
    z[95]=24*z[6];
    z[96]=z[94]*z[95];
    z[96]= - z[96] + n<T>(287,12)*z[5];
    z[96]=z[96]*z[92];
    z[96]=z[96] + n<T>(305,2);
    z[97]= - z[6]*z[96];
    z[97]= - n<T>(683,15)*z[19] + z[97];
    z[97]=z[6]*z[97];
    z[98]=z[7] - z[6];
    z[99]=z[98]*z[7];
    z[97]=z[97] - n<T>(305,2)*z[99];
    z[97]=z[7]*z[97];
    z[100]= - 9403*z[18] - 305*z[36];
    z[101]=n<T>(1,2)*z[39];
    z[100]=z[100]*z[101];
    z[102]=z[87] + 4*z[72];
    z[103]=z[102]*z[95];
    z[103]=z[103] + n<T>(1153,12)*z[5];
    z[103]=z[103]*z[6];
    z[103]=z[103] + n<T>(289,12);
    z[104]=z[103]*z[67];
    z[104]= - n<T>(1879,12)*z[18] + z[104];
    z[105]=z[67]*z[3];
    z[106]=n<T>(1,5)*z[105];
    z[104]=z[104]*z[106];
    z[37]=z[37] + z[104] + z[100] + z[93] + z[97];
    z[37]=z[9]*z[37];
    z[93]=z[72]*z[18];
    z[97]= - n<T>(289,3)*z[5] - 96*z[89];
    z[97]=z[6]*z[97];
    z[97]=z[97] + n<T>(287,3) - 792*z[93];
    z[97]=z[97]*z[67];
    z[96]= - n<T>(144,5)*z[93] + z[96];
    z[96]=z[7]*z[6]*z[96];
    z[100]=z[102]*z[6];
    z[100]=72*z[100] + n<T>(1153,4)*z[5];
    z[100]=z[100]*z[6];
    z[100]=z[100] + n<T>(289,4);
    z[100]=z[100]*z[6];
    z[102]=n<T>(147,2)*z[19] - z[100];
    z[102]=z[102]*z[67];
    z[104]=z[18]*z[105];
    z[102]=z[102] + n<T>(3389,24)*z[104];
    z[102]=z[102]*z[45];
    z[104]=z[99] + z[67];
    z[107]=n<T>(187033,15)*z[18] + 305*z[104];
    z[107]=z[7]*z[107];
    z[107]= - n<T>(175117,30)*z[22] + z[107];
    z[107]=z[7]*z[107];
    z[108]=z[4]*npow(z[7],5);
    z[107]=305*z[108] + n<T>(13837,10)*z[43] + z[107];
    z[108]=n<T>(1,4)*z[9];
    z[107]=z[107]*z[108];
    z[109]= - n<T>(31213,15)*z[18] - n<T>(305,4)*z[99];
    z[109]=z[7]*z[109];
    z[109]=n<T>(15061,30)*z[22] + z[109];
    z[109]=z[109]*z[42];
    z[107]=z[109] + z[107];
    z[107]=z[2]*z[107];
    z[109]=z[18]*z[39];
    z[76]=n<T>(305,2)*z[76] + n<T>(23623,15)*z[109];
    z[76]=z[4]*z[76];
    z[29]=z[29] + z[107] + z[37] + z[102] + z[76] + n<T>(1,5)*z[97] + z[96];
    z[29]=z[8]*z[29];
    z[37]=2*z[87];
    z[76]= - n<T>(23,5)*z[72] - z[37];
    z[76]=z[6]*z[76];
    z[76]= - n<T>(181,15)*z[5] + 12*z[76];
    z[76]=z[6]*z[76];
    z[76]= - n<T>(5149,60) + 2*z[76];
    z[76]=z[76]*z[67];
    z[96]= - n<T>(8,5)*z[72] - z[87];
    z[96]=z[96]*z[95];
    z[96]=n<T>(287,20)*z[5] + z[96];
    z[96]=z[6]*z[96];
    z[96]=n<T>(2283,4) + z[96];
    z[96]=z[6]*z[96];
    z[96]=z[96] - n<T>(2283,4)*z[7];
    z[96]=z[7]*z[96];
    z[88]= - z[88]*z[95];
    z[88]= - n<T>(577,12)*z[5] + z[88];
    z[88]=z[6]*z[88];
    z[88]= - n<T>(73,15) + z[88];
    z[88]=z[3]*z[88]*z[34];
    z[76]=z[88] - n<T>(2283,4)*z[77] + z[76] + z[96];
    z[76]=z[9]*z[76];
    z[88]=3*z[6];
    z[96]=z[88] - 5*z[7];
    z[96]=z[96]*z[101];
    z[97]=z[67] + n<T>(5,2)*z[99];
    z[97]=z[7]*z[97];
    z[62]=z[4]*z[62];
    z[62]=z[97] + n<T>(5,2)*z[62];
    z[62]=z[9]*z[62];
    z[62]=z[96] + z[62];
    z[62]=z[2]*z[62];
    z[96]=7*z[72] + n<T>(16,5)*z[87];
    z[96]=z[96]*z[95];
    z[96]=n<T>(73,5)*z[5] + z[96];
    z[96]=z[6]*z[96];
    z[96]=n<T>(14299,60) + z[96];
    z[96]=z[6]*z[96];
    z[37]=z[64] + z[37];
    z[37]=z[6]*z[37];
    z[37]= - n<T>(287,6)*z[5] + 48*z[37];
    z[37]=z[37]*z[92];
    z[37]= - n<T>(305,2) + z[37];
    z[37]=z[7]*z[37];
    z[64]=529*z[5] + 288*z[89];
    z[64]=z[6]*z[64];
    z[64]=n<T>(99,4) + z[64];
    z[64]=z[64]*z[106];
    z[29]=z[29] + n<T>(305,2)*z[62] + z[76] + z[64] + n<T>(1673,4)*z[39] + z[96]
    + z[37];
    z[29]=z[8]*z[29];
    z[37]=n<T>(1,2)*z[19];
    z[62]=z[20]*z[5];
    z[64]=n<T>(1,3)*z[1] - z[37] + n<T>(1,2)*z[62];
    z[64]=z[64]*z[6];
    z[76]=z[16] - z[18];
    z[96]=n<T>(1,6)*z[76];
    z[64]=z[64] + z[96];
    z[97]= - n<T>(121,2)*z[105] - 121*z[6];
    z[97]=z[64]*z[97];
    z[99]=n<T>(1493,5)*z[18];
    z[101]=z[99] - n<T>(121,2)*z[16];
    z[102]=z[62] - z[19];
    z[102]= - n<T>(1493,15)*z[1] - n<T>(121,4)*z[102];
    z[102]=z[6]*z[102];
    z[101]=n<T>(1493,15)*z[21] + n<T>(1,6)*z[101] + z[102];
    z[101]=z[7]*z[101];
    z[102]=n<T>(1,2)*z[18] + z[21];
    z[102]=z[102]*z[39];
    z[97]=n<T>(1493,15)*z[102] + z[101] + z[97];
    z[97]=z[9]*z[97];
    z[101]=1427*z[62] - 2639*z[1] - 1621*z[19];
    z[101]=z[101]*z[92];
    z[99]=z[101] + z[99] + 121*z[16];
    z[101]= - n<T>(2203,2)*z[62] + 1493*z[1] + n<T>(2203,4)*z[19];
    z[101]=z[101]*z[40];
    z[99]=n<T>(1,2)*z[99] + z[101];
    z[101]=n<T>(7,3)*z[1] - 3*z[19];
    z[101]=n<T>(1,4)*z[101] + z[62];
    z[101]=z[6]*z[101];
    z[96]=z[96] + z[101];
    z[101]=z[3]*z[6];
    z[96]=z[96]*z[101];
    z[96]=z[97] + n<T>(1,3)*z[99] + 121*z[96];
    z[96]=z[9]*z[96];
    z[97]=z[61]*z[20];
    z[99]=211*z[97];
    z[102]=163*z[19];
    z[106]= - n<T>(23,3)*z[1] + z[102];
    z[106]=z[106]*z[72];
    z[106]=z[106] - z[99];
    z[107]=n<T>(1,2)*z[6];
    z[106]=z[106]*z[107];
    z[109]= - 265*z[18] - n<T>(719,10)*z[16];
    z[109]=z[5]*z[109];
    z[109]= - n<T>(7121,40)*z[1] + z[109];
    z[109]=z[109]*z[57];
    z[106]=z[109] + z[106];
    z[106]=z[6]*z[106];
    z[109]=538*z[18] + n<T>(1469,2)*z[16];
    z[109]=z[109]*z[57];
    z[109]= - n<T>(1191,2)*z[1] + z[109];
    z[106]=n<T>(1,5)*z[109] + z[106];
    z[106]=z[6]*z[106];
    z[109]=z[71]*z[67];
    z[64]=z[64]*z[109];
    z[110]= - n<T>(563,2)*z[18] - 853*z[16];
    z[64]= - n<T>(437,20)*z[64] + n<T>(1,30)*z[110] + z[106];
    z[64]=z[3]*z[64];
    z[102]= - n<T>(1267,30)*z[1] + z[102];
    z[102]=z[102]*z[72];
    z[99]=z[102] - z[99];
    z[99]=z[6]*z[99];
    z[102]= - 1181*z[18] - 719*z[16];
    z[102]=z[102]*z[57];
    z[102]=n<T>(7261,4)*z[1] + z[102];
    z[102]=z[5]*z[102];
    z[102]=static_cast<T>(3768)+ z[102];
    z[99]=n<T>(1,5)*z[102] + z[99];
    z[99]=z[6]*z[99];
    z[102]= - n<T>(192,5)*z[1] + n<T>(163,2)*z[19];
    z[102]=z[102]*z[72];
    z[102]=z[102] - n<T>(211,2)*z[97];
    z[102]=z[6]*z[102];
    z[106]=48*z[18] - n<T>(719,6)*z[16];
    z[106]=z[5]*z[106];
    z[106]=n<T>(1079,6)*z[1] + z[106];
    z[106]=z[106]*z[60];
    z[102]=z[102] - n<T>(7966,3) + z[106];
    z[102]=z[7]*z[102];
    z[106]= - n<T>(1,3)*z[18] - z[21];
    z[106]=z[7]*z[106];
    z[110]=2*z[21];
    z[111]= - z[18] - z[110];
    z[111]=z[111]*z[39];
    z[106]=2*z[106] + n<T>(1,3)*z[111];
    z[106]=z[4]*z[106];
    z[106]=9982*z[106] - n<T>(8381,3)*z[18] - 20756*z[21];
    z[111]=n<T>(1,5)*z[4];
    z[106]=z[106]*z[111];
    z[112]= - 4879*z[18] + 1469*z[16];
    z[112]=z[5]*z[112];
    z[112]= - 37213*z[1] + z[112];
    z[64]=z[96] + z[64] + z[106] + z[102] + n<T>(1,30)*z[112] + z[99];
    z[64]=z[9]*z[64];
    z[96]=z[61]*z[70];
    z[99]=z[24]*z[109];
    z[96]= - 719*z[96] - n<T>(437,4)*z[99];
    z[96]=z[8]*z[96];
    z[99]=z[72]*z[28];
    z[102]=z[16]*z[6];
    z[106]=z[3]*z[28];
    z[106]=3389*z[102] - 437*z[106];
    z[106]=z[3]*z[106];
    z[96]=n<T>(1,6)*z[96] - 48*z[99] + n<T>(1,24)*z[106];
    z[96]=z[8]*z[96];
    z[99]= - z[6]*z[103];
    z[103]=z[18]*z[3];
    z[96]=z[96] + n<T>(3389,24)*z[103] + n<T>(641,12)*z[19] + z[99];
    z[96]=z[8]*z[3]*z[96];
    z[90]= - n<T>(287,12) + z[90];
    z[90]=z[3]*z[90];
    z[90]=z[90] + z[96];
    z[90]=z[8]*z[90];
    z[96]=536*z[1] + n<T>(359,2)*z[19];
    z[96]=z[96]*z[72];
    z[96]=z[96] + n<T>(359,2)*z[97];
    z[99]=z[62] + z[19];
    z[99]= - 366*z[1] - n<T>(8347,24)*z[99];
    z[99]=z[3]*z[99];
    z[106]=z[5]*z[1];
    z[99]= - n<T>(8347,12)*z[106] + z[99];
    z[99]=z[3]*z[99];
    z[96]=n<T>(1,3)*z[96] + z[99];
    z[96]=z[3]*z[96];
    z[99]= - 713*z[18] - 359*z[16];
    z[99]=z[99]*z[57];
    z[99]= - n<T>(2403,2)*z[1] + z[99];
    z[99]=z[5]*z[99];
    z[112]=z[19] + n<T>(1,2)*z[78];
    z[112]=z[3]*z[112];
    z[99]=z[99] + n<T>(8347,6)*z[112];
    z[99]=z[3]*z[99];
    z[32]=z[32]*z[78];
    z[32]=z[99] - n<T>(163,3)*z[32];
    z[99]=n<T>(1,2)*z[2];
    z[32]=z[32]*z[99];
    z[32]=z[90] + z[96] + z[32];
    z[32]=z[10]*z[32];
    z[90]=542*z[1] + n<T>(647,2)*z[19];
    z[90]=z[90]*z[57];
    z[90]= - n<T>(3593,4) + z[90];
    z[90]=z[5]*z[90];
    z[32]=z[32] + z[90] - n<T>(503,2)*z[97];
    z[90]= - 45*z[1] + n<T>(647,15)*z[19];
    z[90]=z[90]*z[72];
    z[90]=z[90] - n<T>(2587,15)*z[97];
    z[90]=z[90]*z[107];
    z[37]=z[1] - z[37];
    z[37]=8347*z[37] + n<T>(15383,2)*z[62];
    z[37]=z[6]*z[37];
    z[37]=n<T>(1,12)*z[37] - 366*z[18] - n<T>(8347,24)*z[16];
    z[37]=z[37]*z[45];
    z[45]=n<T>(4447,3)*z[1] - 8347*z[19];
    z[45]=n<T>(1,10)*z[45] + 1345*z[62];
    z[37]=n<T>(1,4)*z[45] + z[37];
    z[37]=z[3]*z[37];
    z[45]=359*z[18] + n<T>(647,3)*z[16];
    z[45]=z[5]*z[45];
    z[45]=n<T>(9331,12)*z[1] + z[45];
    z[45]=z[5]*z[45];
    z[37]=z[37] + z[90] - static_cast<T>(91)+ n<T>(1,10)*z[45];
    z[37]=z[3]*z[37];
    z[45]=43*z[15];
    z[90]= - z[45] - n<T>(647,10)*z[24];
    z[90]=z[90]*z[57];
    z[90]= - n<T>(1279,10)*z[18] + z[90];
    z[90]=z[5]*z[90];
    z[96]=z[3]*z[55];
    z[96]=z[16] + z[96];
    z[96]=z[3]*z[96];
    z[90]=n<T>(8347,40)*z[96] + n<T>(513,4)*z[1] + z[90];
    z[90]=z[3]*z[90];
    z[96]=z[3]*z[24];
    z[96]=z[16] + z[96];
    z[96]=103*z[18] - n<T>(163,5)*z[96];
    z[96]=z[96]*z[99];
    z[107]= - 26*z[1] - n<T>(1283,2)*z[19];
    z[78]=z[96] + n<T>(1,5)*z[107] - 35*z[78];
    z[78]=z[78]*z[86];
    z[96]=z[15]*z[72];
    z[96]= - 429*z[1] - n<T>(647,6)*z[96];
    z[96]=z[5]*z[96];
    z[96]=n<T>(1651,4) + z[96];
    z[78]=z[78] + n<T>(1,5)*z[96] + z[90];
    z[78]=z[2]*z[78];
    z[90]=z[63]*z[3];
    z[83]= - z[67]*z[83];
    z[83]=z[83] + n<T>(437,2)*z[90];
    z[83]=z[3]*z[83];
    z[34]=z[34]*z[23];
    z[96]=z[61]*z[34];
    z[65]=z[71]*z[65];
    z[65]=53*z[96] + n<T>(23,4)*z[65];
    z[65]=z[8]*z[65];
    z[96]=z[72]*z[63];
    z[65]=n<T>(19,2)*z[65] + 432*z[96] + n<T>(1,4)*z[83];
    z[65]=z[3]*z[65];
    z[83]=z[61]*z[63];
    z[65]=n<T>(1007,3)*z[83] + z[65];
    z[65]=z[8]*z[65];
    z[83]=144*z[93] + z[91];
    z[83]=z[6]*z[83];
    z[91]= - n<T>(2633,8)*z[19] + z[100];
    z[91]=z[6]*z[91];
    z[93]=z[22]*z[3];
    z[91]=z[91] - n<T>(3389,12)*z[93];
    z[91]=z[3]*z[91];
    z[65]=z[65] + z[83] + z[91];
    z[65]=z[8]*z[65];
    z[83]=z[6]*z[94];
    z[89]= - n<T>(449,4)*z[5] - 72*z[89];
    z[89]=z[89]*z[88];
    z[89]=n<T>(47,2) + z[89];
    z[89]=z[3]*z[89];
    z[83]=z[89] + n<T>(143,3)*z[5] - 144*z[83];
    z[83]=z[92]*z[83];
    z[65]=n<T>(1,5)*z[65] - n<T>(305,2) + z[83];
    z[65]=z[8]*z[65];
    z[32]=z[65] + z[78] + z[37] + n<T>(1,5)*z[32];
    z[32]=z[10]*z[32];
    z[37]=z[9]*z[39];
    z[37]=z[37] + 1;
    z[37]=z[15]*z[37];
    z[65]= - z[4]*z[81];
    z[78]=z[20]*z[7];
    z[37]=z[65] - 3*z[78] + z[37];
    z[65]=z[36]*z[8];
    z[70]= - z[70]*z[99]*z[65];
    z[83]=z[28]*z[7];
    z[89]=z[63] - z[83];
    z[89]=z[7]*z[89];
    z[70]=z[89] + z[70];
    z[89]=npow(z[8],3);
    z[70]=z[70]*z[89];
    z[37]=n<T>(1,2)*z[37] + z[70];
    z[70]=z[28] - n<T>(1,4)*z[80];
    z[70]=z[7]*z[70];
    z[70]= - n<T>(1,4)*z[63] + z[70];
    z[70]=z[70]*z[89];
    z[41]= - z[12]*z[41];
    z[41]=z[41] + z[81];
    z[41]=z[89]*z[41];
    z[41]=n<T>(3,2)*z[1] + z[41];
    z[81]=n<T>(1,2)*z[12];
    z[41]=z[41]*z[81];
    z[35]=z[41] - n<T>(3,4)*z[35] + z[70];
    z[35]=z[12]*z[35];
    z[35]=n<T>(1,2)*z[37] + z[35];
    z[35]=z[13]*z[35];
    z[37]= - n<T>(17239,15)*z[21] - 247*z[18] - 1243*z[20];
    z[41]=n<T>(4027,6)*z[68] - n<T>(637,4)*z[15] - 789*z[22];
    z[41]=z[41]*z[111];
    z[70]= - 703*z[22] - 10589*z[68];
    z[81]= - 2451*z[63] - n<T>(10589,3)*z[83];
    z[81]=z[8]*z[2]*z[81];
    z[70]=z[81] + n<T>(1,3)*z[70];
    z[70]=z[7]*z[70];
    z[70]=2451*z[43] + z[70];
    z[81]=npow(z[8],2);
    z[70]=z[70]*z[81];
    z[83]=n<T>(637,2)*z[15] - n<T>(4027,3)*z[68];
    z[83]=z[83]*z[42];
    z[83]= - n<T>(2519,2)*z[68] + z[83];
    z[83]=z[9]*z[83];
    z[91]=n<T>(941,5)*z[22] + n<T>(665,6)*z[68];
    z[94]=z[12]*z[18];
    z[91]=n<T>(1313,40)*z[94] + n<T>(1,2)*z[91];
    z[91]=z[81]*z[91];
    z[91]= - n<T>(169,5)*z[1] + z[91];
    z[91]=z[12]*z[91];
    z[78]=z[2]*z[78];
    z[35]=n<T>(2587,10)*z[35] + z[91] + n<T>(1,40)*z[70] - n<T>(3861,40)*z[78] + n<T>(1,10)*z[83] + n<T>(1,8)*z[37] + z[41];
    z[35]=z[13]*z[35];
    z[37]=n<T>(163,6)*z[21] + 841*z[18] + n<T>(789,2)*z[20];
    z[37]=z[4]*z[37];
    z[41]=n<T>(3049,4)*z[18] - 399*z[20];
    z[70]=10707*z[18] + n<T>(25399,2)*z[21];
    z[70]=z[7]*z[70];
    z[70]=5937*z[15] + z[70];
    z[70]=z[70]*z[58];
    z[41]=z[70] + 3*z[41] + n<T>(6019,3)*z[21];
    z[41]=z[9]*z[41];
    z[70]= - n<T>(98837,12)*z[21] - 3287*z[18] + n<T>(17117,4)*z[20];
    z[70]=z[70]*z[99];
    z[78]=2306*z[1] - 1223*z[6];
    z[37]=z[70] + z[41] + z[37] + 2*z[78] + 3677*z[7];
    z[41]=n<T>(69781,6)*z[22] - 14573*z[68];
    z[41]=z[41]*z[40];
    z[41]= - n<T>(1425,2)*z[43] + z[41];
    z[41]=z[2]*z[41]*z[81];
    z[35]=z[35] - n<T>(5937,20)*z[12] + n<T>(1,5)*z[37] + n<T>(1,4)*z[41];
    z[35]=z[13]*z[35];
    z[37]=799*z[1];
    z[41]=z[37] - n<T>(958,3)*z[19];
    z[41]=z[7]*z[41];
    z[41]= - 1007*z[76] + z[41];
    z[41]=z[41]*z[42];
    z[43]=z[15]*z[3];
    z[70]=3197*z[18] + 257*z[43];
    z[70]=z[70]*z[86];
    z[78]=z[3]*z[1];
    z[83]= - z[2]*z[103];
    z[83]=z[78] + z[83];
    z[83]=z[10]*z[83];
    z[86]=47*z[1] + n<T>(46,3)*z[103];
    z[70]=n<T>(1358,3)*z[83] + 17*z[86] + z[70];
    z[70]=z[10]*z[70];
    z[37]=z[37] - n<T>(2797,3)*z[19];
    z[37]=z[7]*z[37];
    z[83]= - n<T>(257,3)*z[15] - 613*z[68];
    z[83]=z[2]*z[83];
    z[37]=z[70] + z[83] + z[41] + n<T>(1582,3)*z[18] + z[37];
    z[41]=z[2]*z[43];
    z[41]= - 2*z[103] + z[41];
    z[41]=z[10]*z[41];
    z[70]=z[2]*z[15];
    z[41]=z[41] + z[18] - 2*z[70];
    z[41]=z[10]*z[41];
    z[70]= - z[76]*z[39];
    z[83]=z[39]*z[1];
    z[86]=z[83] + z[21];
    z[91]=z[78]*z[10];
    z[94]=z[1] + z[91];
    z[94]=z[10]*z[94];
    z[94]=z[94] + z[86];
    z[94]=z[12]*z[94];
    z[80]=z[2]*z[80];
    z[41]=z[94] + z[41] + z[80] + z[68] + z[70];
    z[41]=z[14]*z[41];
    z[70]=z[4]*z[21];
    z[70]= - n<T>(1039,15)*z[91] + n<T>(80,3)*z[1] + n<T>(613,5)*z[70];
    z[70]=z[12]*z[70];
    z[37]=n<T>(1439,15)*z[41] + n<T>(1,5)*z[37] + z[70];
    z[37]=z[14]*z[37];
    z[41]= - n<T>(4127,12)*z[43] + n<T>(6073,2)*z[18] + 613*z[21];
    z[41]=z[2]*z[41];
    z[43]=z[4]*z[1];
    z[43]=613*z[43] + n<T>(1039,3)*z[78];
    z[43]=z[12]*z[43];
    z[41]=z[41] + z[43];
    z[43]= - 479*z[18] + n<T>(6851,10)*z[16];
    z[70]=17*z[19];
    z[80]=z[21]*z[5];
    z[94]=173*z[80] + n<T>(1061,2)*z[1] + z[70];
    z[94]=z[94]*z[40];
    z[43]=n<T>(1,2)*z[43] + z[94];
    z[43]=z[4]*z[43];
    z[94]=1073*z[106] - n<T>(7681,5)*z[78];
    z[96]=n<T>(173,4)*z[1] - 326*z[103];
    z[96]=z[2]*z[96];
    z[94]=n<T>(1,4)*z[94] + n<T>(13,5)*z[96];
    z[91]=z[2]*z[91];
    z[91]=n<T>(1,3)*z[94] - n<T>(3599,20)*z[91];
    z[91]=z[10]*z[91];
    z[94]= - 61*z[1] + n<T>(1063,15)*z[19];
    z[37]=z[37] + z[91] - n<T>(17191,60)*z[103] + n<T>(1,2)*z[43] + n<T>(7,4)*z[94]
    + n<T>(393,5)*z[80] + n<T>(1,10)*z[41];
    z[37]=z[14]*z[37];
    z[41]= - n<T>(1,2)*z[90] - z[28] - z[84];
    z[41]=z[41]*z[79];
    z[43]= - z[22] - z[68];
    z[55]=z[10]*z[36]*npow(z[8],4)*z[55];
    z[55]= - z[21] + z[55];
    z[84]=n<T>(1,2)*z[10];
    z[55]=z[55]*z[84];
    z[41]=z[55] + n<T>(1,2)*z[43] + z[41];
    z[41]=z[11]*z[41];
    z[43]=n<T>(2767,10)*z[68] + n<T>(67,5)*z[15] + 151*z[22];
    z[22]=n<T>(67,3)*z[15] - n<T>(419,2)*z[22];
    z[22]=z[3]*z[22]*z[92];
    z[22]=n<T>(1,3)*z[43] + z[22];
    z[22]=z[9]*z[22];
    z[43]=z[16]*z[65];
    z[43]= - n<T>(5477,5)*z[68] - n<T>(4393,3)*z[43];
    z[43]=z[43]*z[81];
    z[55]=z[10]*z[7]*z[89]*z[16];
    z[65]=677*z[1] - n<T>(4907,5)*z[80];
    z[43]= - n<T>(8651,15)*z[55] + n<T>(1,3)*z[65] + z[43];
    z[43]=z[10]*z[43];
    z[22]=n<T>(4639,10)*z[41] + n<T>(1,4)*z[43] + z[22] - n<T>(2633,30)*z[93] + 2767.
   /15.*z[21] + n<T>(379,5)*z[18] - n<T>(1263,4)*z[20];
    z[22]=z[11]*z[22];
    z[21]= - n<T>(2,3)*z[21] + 33*z[18] + n<T>(359,3)*z[20];
    z[41]=1055*z[18] + n<T>(19889,10)*z[20];
    z[41]=z[6]*z[41];
    z[41]=n<T>(2891,10)*z[15] + z[41];
    z[43]=n<T>(1,12)*z[3];
    z[41]=z[41]*z[43];
    z[21]=n<T>(8,5)*z[21] + z[41];
    z[21]=z[9]*z[21];
    z[41]=z[81]*z[19];
    z[55]=n<T>(3613,30)*z[10] + n<T>(31079,60)*z[7];
    z[55]=z[41]*z[55];
    z[55]= - n<T>(3841,60)*z[78] + n<T>(2543,15) + n<T>(179,2)*z[106] + z[55];
    z[55]=z[55]*z[84];
    z[65]=static_cast<T>(1655)+ n<T>(5519,5)*z[106];
    z[65]=z[65]*z[66];
    z[80]=2947*z[1] - n<T>(2623,10)*z[19];
    z[65]=z[65] + n<T>(1,4)*z[80] + n<T>(118,5)*z[6];
    z[80]= - 137*z[18] - n<T>(6079,3)*z[20];
    z[80]=z[3]*z[80];
    z[41]=z[36]*z[41];
    z[21]=n<T>(1,2)*z[22] + z[55] + n<T>(2769,10)*z[41] + z[21] + n<T>(1,3)*z[65] + 
   n<T>(1,20)*z[80];
    z[21]=z[11]*z[21];
    z[22]=z[87]*z[18];
    z[41]= - z[18] + n<T>(6,5)*z[16];
    z[41]=z[41]*z[52];
    z[41]=z[46] + z[41];
    z[41]=z[5]*z[41];
    z[46]=z[61]*z[68];
    z[41]=n<T>(2,5)*z[46] + z[41] + n<T>(6,5)*z[22];
    z[46]=24*z[7];
    z[41]=z[41]*z[46];
    z[55]=2*z[22];
    z[65]= - z[48] + 8*z[16];
    z[65]=z[65]*z[72];
    z[65]=z[65] + z[55];
    z[68]=n<T>(72,5)*z[6];
    z[65]=z[65]*z[68];
    z[80]= - z[24] + n<T>(6,5)*z[15];
    z[84]=z[5]*z[80];
    z[84]=n<T>(563,5)*z[18] - 48*z[84];
    z[84]=z[84]*z[52];
    z[41]=z[41] + z[65] + n<T>(54613,30)*z[1] + z[84];
    z[41]=z[7]*z[41];
    z[65]=2*z[5];
    z[33]= - z[33]*z[65];
    z[33]=z[50] + z[33];
    z[50]=z[5]*z[76];
    z[50]=z[1] + 6*z[50];
    z[50]=z[6]*z[50];
    z[84]=2*z[16];
    z[48]= - z[48] + z[84];
    z[48]=z[5]*z[48];
    z[48]=z[1] + z[48];
    z[48]=z[7]*z[48];
    z[33]=n<T>(48,5)*z[48] + n<T>(24,5)*z[50] + 24*z[33];
    z[33]=z[5]*z[33];
    z[33]=n<T>(16855,12)*z[1] + z[33];
    z[33]=z[7]*z[33];
    z[48]= - z[80]*z[65];
    z[48]=n<T>(3,5)*z[18] + z[48];
    z[48]=z[5]*z[48];
    z[50]= - z[18] + z[84];
    z[50]=z[6]*z[50]*z[72];
    z[48]=z[48] + n<T>(2,5)*z[50];
    z[48]=z[48]*z[95];
    z[44]=n<T>(731,10)*z[15] - z[44];
    z[44]=z[44]*z[52];
    z[33]=z[33] + z[48] + n<T>(44347,60)*z[18] + z[44];
    z[33]=z[7]*z[33];
    z[44]=n<T>(3,5)*z[15];
    z[48]=z[44] - z[24];
    z[50]= - z[48]*z[72];
    z[65]=z[15]*z[87];
    z[50]=z[50] + n<T>(1,5)*z[65];
    z[65]=8*z[6];
    z[50]=z[50]*z[65];
    z[80]= - z[26] + n<T>(8,5)*z[23];
    z[84]= - z[80]*z[47];
    z[84]=n<T>(587,20)*z[15] + z[84];
    z[84]=z[5]*z[84];
    z[50]=z[84] + z[50];
    z[50]=z[6]*z[50];
    z[53]= - z[53]*z[47];
    z[53]=z[54] + z[53];
    z[53]=z[5]*z[53];
    z[50]=z[53] + z[50];
    z[33]=3*z[50] + z[33];
    z[33]=z[4]*z[33];
    z[50]= - z[18] + 14*z[16];
    z[50]=z[50]*z[72];
    z[50]=z[50] + z[55];
    z[50]=z[6]*z[50];
    z[48]= - z[48]*z[38];
    z[48]=n<T>(491,10)*z[18] + z[48];
    z[48]=z[5]*z[48];
    z[48]=z[48] + n<T>(8,5)*z[50];
    z[48]=z[48]*z[88];
    z[38]= - z[80]*z[38];
    z[38]=n<T>(2647,10)*z[15] + z[38];
    z[38]=z[38]*z[52];
    z[33]=z[33] + z[41] + z[48] + n<T>(2063,12)*z[18] + z[38];
    z[33]=z[4]*z[33];
    z[38]=4*z[1] + z[70];
    z[38]=z[38]*z[72];
    z[38]=z[38] + 2*z[97];
    z[38]=z[38]*z[95];
    z[41]=2*z[1];
    z[48]=z[41] + 7*z[19];
    z[48]=z[48]*z[72];
    z[48]=z[48] + z[97];
    z[46]=z[48]*z[46];
    z[48]= - 24*z[18] + n<T>(329,3)*z[16];
    z[48]=z[5]*z[48];
    z[48]=194*z[1] + z[48];
    z[48]=z[5]*z[48];
    z[38]=z[46] + z[38] + n<T>(14653,2) + 7*z[48];
    z[38]=z[38]*z[40];
    z[40]=z[41] + 13*z[19];
    z[40]=z[40]*z[72];
    z[40]=z[40] + z[97];
    z[40]=z[40]*z[95];
    z[41]=96*z[18] + n<T>(4247,6)*z[16];
    z[41]=z[5]*z[41];
    z[41]=679*z[1] + z[41];
    z[41]=z[5]*z[41];
    z[40]=z[41] + z[40];
    z[40]=z[40]*z[92];
    z[41]= - n<T>(2377,15)*z[15] + 331*z[24];
    z[41]=z[5]*z[41];
    z[41]=n<T>(41249,30)*z[18] + z[41];
    z[41]=z[5]*z[41];
    z[41]=n<T>(22681,30)*z[1] + z[41];
    z[33]=z[33] + z[38] + n<T>(1,2)*z[41] + z[40];
    z[33]=z[4]*z[33];
    z[38]= - 1759*z[1] + n<T>(15383,4)*z[19];
    z[38]=n<T>(1,5)*z[38] - n<T>(1145,4)*z[62];
    z[38]=z[6]*z[38];
    z[38]= - n<T>(8347,20)*z[76] + z[38];
    z[38]=z[6]*z[38];
    z[40]= - z[6]*z[24];
    z[40]= - z[26] + z[40];
    z[40]=z[4]*z[40];
    z[40]=z[24] - z[40];
    z[38]=z[38] - n<T>(8347,20)*z[40];
    z[38]=z[3]*z[38];
    z[40]=n<T>(529,2)*z[18] - 8347*z[16];
    z[41]= - n<T>(4447,5)*z[1] + 7429*z[19];
    z[41]=n<T>(1,8)*z[41] - n<T>(2957,5)*z[62];
    z[41]=z[6]*z[41];
    z[46]= - n<T>(8347,2)*z[24] - 1651*z[102];
    z[46]=z[46]*z[58];
    z[38]=n<T>(1,2)*z[38] + z[46] + n<T>(1,10)*z[40] + z[41];
    z[38]=z[3]*z[38];
    z[40]=z[23]*z[61];
    z[41]=z[87]*z[44];
    z[40]=z[40] + z[41];
    z[40]=z[40]*z[65];
    z[41]=z[27]*z[47];
    z[41]=n<T>(1473,20)*z[15] + z[41];
    z[41]=z[5]*z[41];
    z[40]=z[41] + z[40];
    z[40]=z[6]*z[40];
    z[27]=z[27]*z[72];
    z[41]=z[23]*z[87];
    z[27]=z[27] + z[41];
    z[27]=z[27]*z[65];
    z[25]= - z[31] + 2*z[25];
    z[25]=z[25]*z[47];
    z[25]=z[25] - z[49];
    z[25]=z[25]*z[5];
    z[27]= - z[25] + z[27];
    z[27]=z[6]*z[27];
    z[27]= - z[30] + z[27];
    z[27]=z[27]*z[111];
    z[25]=z[27] - z[25] + z[40];
    z[25]=z[4]*z[25];
    z[27]=z[18] + 3*z[16];
    z[27]=z[27]*z[72];
    z[22]=z[27] + z[22];
    z[22]=z[22]*z[68];
    z[27]=z[23]*z[72];
    z[27]=580*z[18] + n<T>(331,2)*z[27];
    z[27]=z[27]*z[57];
    z[22]=z[27] + z[22];
    z[22]=z[6]*z[22];
    z[26]= - 245*z[23] + 331*z[26];
    z[26]=z[5]*z[26];
    z[26]=n<T>(14213,10)*z[15] + z[26];
    z[26]=z[26]*z[57];
    z[26]= - n<T>(2851,10)*z[18] + z[26];
    z[22]=3*z[25] + n<T>(1,2)*z[26] + z[22];
    z[22]=z[4]*z[22];
    z[25]= - n<T>(23,5)*z[1] - n<T>(431,6)*z[19];
    z[25]=z[25]*z[72];
    z[25]=z[25] + n<T>(5393,30)*z[97];
    z[25]=z[6]*z[25];
    z[26]=934*z[18] + n<T>(1079,2)*z[16];
    z[26]=z[5]*z[26];
    z[26]= - n<T>(1167,8)*z[1] + n<T>(1,15)*z[26];
    z[26]=z[5]*z[26];
    z[25]=z[25] + n<T>(3253,20) + z[26];
    z[25]=z[6]*z[25];
    z[24]=z[45] + n<T>(1079,10)*z[24];
    z[24]=z[24]*z[57];
    z[24]=n<T>(10623,40)*z[18] + z[24];
    z[24]=z[5]*z[24];
    z[22]=n<T>(1,3)*z[38] + z[22] + z[25] + n<T>(69,2)*z[1] + z[24];
    z[22]=z[3]*z[22];
    z[24]=n<T>(10747,5)*z[18] + n<T>(305,2)*z[36];
    z[24]=z[24]*z[42];
    z[25]= - 401*z[18] + n<T>(287,3)*z[67];
    z[25]=z[25]*z[101];
    z[26]=n<T>(1493,15)*z[9];
    z[27]= - z[75]*z[26];
    z[24]=z[27] + n<T>(1,10)*z[25] + n<T>(305,2)*z[104] + z[24];
    z[24]=z[9]*z[24];
    z[25]= - z[6] + z[66];
    z[24]=z[24] + 305*z[25] - n<T>(287,10)*z[105];
    z[25]=z[23]*z[74];
    z[27]=npow(z[3],3);
    z[30]=z[27]*z[34];
    z[25]=217*z[25] - n<T>(19,8)*z[30];
    z[23]=z[23]*z[77];
    z[30]=121*z[3];
    z[31]= - z[34]*z[30];
    z[23]= - n<T>(1493,5)*z[23] + z[31];
    z[23]=z[23]*z[73];
    z[23]=n<T>(23,5)*z[25] + n<T>(1,4)*z[23];
    z[23]=z[8]*z[23]*z[79];
    z[25]= - z[82]*z[26];
    z[25]=121*z[90] + z[25];
    z[25]=z[25]*z[108];
    z[26]=z[15]*z[56];
    z[25]=n<T>(4991,5)*z[26] + z[25];
    z[25]=z[9]*z[25];
    z[26]=z[27]*z[63];
    z[23]=z[23] + n<T>(437,40)*z[26] + z[25];
    z[23]=z[8]*z[23];
    z[23]=n<T>(1,2)*z[24] + z[23];
    z[23]=z[8]*z[23];
    z[24]=z[39] + z[98];
    z[24]=n<T>(379,2)*z[24] - n<T>(157,3)*z[105];
    z[24]=z[9]*z[24];
    z[23]=z[23] + z[24] + n<T>(379,2) + n<T>(314,3)*z[101];
    z[23]=z[8]*z[23];
    z[24]=z[110] + z[83];
    z[24]=z[24]*z[59];
    z[24]=n<T>(8381,2)*z[1] + z[24];
    z[24]=z[4]*z[24];
    z[25]= - static_cast<T>(563)+ n<T>(437,2)*z[109];
    z[25]=z[43]*z[1]*z[25];
    z[24]=z[25] + n<T>(7687,4) + n<T>(1,3)*z[24];
    z[25]=z[20] - z[86];
    z[26]=z[67]*z[78];
    z[25]=n<T>(1493,5)*z[25] + 121*z[26];
    z[25]=z[25]*z[85];
    z[26]= - z[20]*z[30];
    z[25]=z[25] - n<T>(1493,10)*z[1] + z[26];
    z[25]=z[25]*z[69];
    z[24]=n<T>(1,5)*z[24] + z[25];
    z[24]=z[9]*z[24];
    z[25]=z[27]*z[8];
    z[26]=z[28]*z[25];
    z[26]= - n<T>(437,10)*z[26] + static_cast<T>(305)+ n<T>(287,5)*z[101];
    z[26]=z[8]*z[26];
    z[26]= - n<T>(157,3)*z[3] + n<T>(1,4)*z[26];
    z[26]=z[8]*z[26];
    z[15]=z[15]*z[25];
    z[15]= - 287*z[3] + n<T>(437,2)*z[15];
    z[15]=z[10]*z[15]*z[81];
    z[25]=z[1]*z[27];
    z[15]=n<T>(1,60)*z[15] + n<T>(437,120)*z[25] + z[26];
    z[15]=z[10]*z[15];
    z[20]=z[71]*z[20];
    z[20]= - static_cast<T>(7)- 437*z[20];
    z[20]=z[20]*z[51];
    z[15]=z[15] + z[23] + z[20] + z[24];
    z[15]=z[12]*z[15];
    z[16]=n<T>(851,2)*z[18] + n<T>(1079,3)*z[16];
    z[16]=z[5]*z[16];
    z[16]= - n<T>(4711,3)*z[1] + z[16];
    z[16]=z[5]*z[16];
    z[18]= - n<T>(179,2)*z[1] - 359*z[19];
    z[18]=z[5]*z[18];
    z[18]=n<T>(21413,12) + z[18];
    z[18]=z[5]*z[18];
    z[18]=z[18] + n<T>(2421,2)*z[97];
    z[18]=z[6]*z[18];
    z[16]=z[18] - n<T>(8713,6) + z[16];
    z[18]=96*z[1] + n<T>(1,6)*z[19];
    z[18]=z[5]*z[18];
    z[18]= - n<T>(4597,4) + z[18];
    z[18]=z[18]*z[60];
    z[18]=z[18] + n<T>(187,3)*z[97];
    z[18]=z[7]*z[18];

    r += z[15] + n<T>(1,5)*z[16] + z[17] + z[18] + z[21] + z[22] + z[29] + 
      z[32] + z[33] + z[35] + z[37] + z[64];
 
    return r;
}

template double qg_2lNLC_r963(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r963(const std::array<dd_real,31>&);
#endif
