#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1264(const std::array<T,31>& k) {
  T z[86];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[19];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[10];
    z[9]=k[11];
    z[10]=k[15];
    z[11]=k[5];
    z[12]=k[6];
    z[13]=k[2];
    z[14]=k[3];
    z[15]=k[4];
    z[16]=k[26];
    z[17]=3*z[8];
    z[18]=z[17]*z[13];
    z[19]=z[3]*z[14];
    z[20]=z[14]*z[8];
    z[21]=npow(z[13],2);
    z[22]=z[21]*z[8];
    z[23]=3*z[13] - z[22];
    z[23]=z[6]*z[23];
    z[23]=z[23] - 3*z[20] - z[18] + z[19] + 5;
    z[23]=z[7]*z[23];
    z[24]=2*z[3];
    z[25]=z[8]*z[13];
    z[26]=z[25] - 5;
    z[27]= - z[6]*z[26];
    z[23]=z[23] - z[24] - 4*z[8] + z[27];
    z[23]=z[7]*z[23];
    z[27]=z[3]*z[10];
    z[28]=2*z[27];
    z[29]=z[6]*z[8];
    z[30]= - z[29] - z[28];
    z[23]=2*z[30] + z[23];
    z[30]=npow(z[7],2);
    z[23]=z[23]*z[30];
    z[31]=npow(z[14],2);
    z[32]=z[31]*z[8];
    z[33]=z[31]*z[3];
    z[32]=z[32] + z[33];
    z[34]=z[32]*z[7];
    z[35]= - z[34] + z[19] + static_cast<T>(12)- 5*z[20];
    z[35]=z[7]*z[35];
    z[36]=4*z[10];
    z[37]=9*z[6];
    z[38]=z[24] + 7*z[8];
    z[35]=z[35] + z[37] + z[36] - z[38];
    z[35]=z[7]*z[35];
    z[39]=z[29] + z[27];
    z[40]=3*z[39];
    z[35]= - z[40] + z[35];
    z[35]=z[9]*z[7]*z[35];
    z[41]=npow(z[7],4);
    z[42]=z[41]*z[27];
    z[43]=z[41]*z[9];
    z[44]=z[6] + z[10];
    z[45]=z[44]*z[43];
    z[45]= - z[42] + z[45];
    z[45]=z[11]*z[45];
    z[46]=3*z[27];
    z[47]=3*z[6];
    z[48]=z[47] + z[3];
    z[49]= - z[7]*z[48];
    z[49]= - z[46] + z[49];
    z[50]=npow(z[7],3);
    z[49]=z[49]*z[50];
    z[51]=z[50]*z[9];
    z[52]=3*z[10];
    z[53]=z[52] - 7*z[6];
    z[53]=z[53]*z[51];
    z[45]=z[45] + z[49] + z[53];
    z[45]=z[11]*z[45];
    z[23]=z[45] + z[23] + z[35];
    z[35]=z[17] + z[3];
    z[45]= - static_cast<T>(3)+ z[25];
    z[45]=z[6]*z[45];
    z[45]=z[45] + z[35];
    z[45]=z[7]*z[45];
    z[45]=z[45] + 2*z[29] + z[46];
    z[45]=z[45]*z[50];
    z[46]=z[20] - 2;
    z[49]= - z[19] + 3*z[46];
    z[53]=z[7]*z[49];
    z[53]=z[53] - 8*z[6] - z[52] + z[38];
    z[53]=z[7]*z[53];
    z[53]=4*z[39] + z[53];
    z[53]=z[9]*z[53]*z[30];
    z[54]= - z[10] + z[47];
    z[43]=z[54]*z[43];
    z[42]=z[42] + z[43];
    z[42]=z[11]*z[42];
    z[42]=z[42] + z[45] + z[53];
    z[35]=z[47] + z[10] - z[35];
    z[35]=z[7]*z[35];
    z[35]= - z[40] + z[35];
    z[35]=z[35]*z[51];
    z[39]=z[39]*z[41];
    z[35]= - z[39] + z[35];
    z[40]=z[2]*z[5];
    z[41]=z[3]*z[2];
    z[43]=z[40] - z[41];
    z[45]=z[3] - z[5];
    z[45]=z[45]*z[9];
    z[45]=z[45] + z[43];
    z[51]=z[11]*z[12];
    z[53]=z[9]*z[6];
    z[51]=z[53]*npow(z[51],6);
    z[51]=z[51] + z[45];
    z[51]=z[4]*z[51];
    z[54]=z[41]*z[5];
    z[55]=z[9]*z[5];
    z[56]=z[55]*z[3];
    z[54]=z[54] - z[56];
    z[56]=3*z[54];
    z[51]= - z[56] + z[51];
    z[51]=z[51]*npow(z[4],3);
    z[57]=2*z[9];
    z[39]=z[57]*z[39];
    z[58]=z[54]*npow(z[4],4);
    z[39]=z[39] + z[58];
    z[39]=z[1]*z[39];
    z[35]=z[39] + 2*z[35] + z[51];
    z[35]=z[1]*z[35];
    z[39]=npow(z[12],5);
    z[51]=z[39]*npow(z[11],5)*z[53];
    z[51]=z[51] - z[45];
    z[58]=npow(z[15],2);
    z[59]=z[58]*z[6];
    z[60]=z[58]*z[9];
    z[61]=z[59] + z[60];
    z[62]= - z[39]*z[61];
    z[63]=z[6]*z[15];
    z[64]=z[9]*z[15];
    z[65]=z[63] + z[64];
    z[66]=z[9] + z[6];
    z[67]=z[66]*z[11];
    z[68]= - z[67] - z[65];
    z[39]=z[11]*z[39]*z[68];
    z[39]=z[39] + z[62];
    z[39]=z[39]*npow(z[11],2);
    z[55]=z[43] - z[55];
    z[39]=z[39] - z[55];
    z[39]=z[11]*z[39];
    z[62]=z[3]*z[15];
    z[68]=z[62] + 1;
    z[68]=z[68]*z[9];
    z[68]=z[68] - z[3];
    z[39]=z[39] + z[68];
    z[39]=z[4]*z[39];
    z[39]=3*z[51] + z[39];
    z[39]=z[4]*z[39];
    z[39]=4*z[54] + z[39];
    z[39]=z[39]*npow(z[4],2);
    z[35]=z[35] + 2*z[42] + z[39];
    z[35]=z[1]*z[35];
    z[39]=npow(z[12],4);
    z[42]=z[39]*z[31];
    z[51]=z[39]*z[15];
    z[54]=z[14]*z[39];
    z[54]=z[54] + z[51];
    z[69]=3*z[15];
    z[54]=z[54]*z[69];
    z[54]=z[54] + z[42] + z[55];
    z[54]=z[11]*z[54];
    z[70]=npow(z[14],3);
    z[71]=2*z[70];
    z[72]=z[71]*z[39];
    z[51]=z[14]*z[51];
    z[51]= - 5*z[42] - 3*z[51];
    z[51]=z[15]*z[51];
    z[51]=z[54] - z[9] + z[3] - z[72] + z[51];
    z[51]=z[11]*z[51];
    z[42]=z[15]*z[42];
    z[42]=z[72] + z[42];
    z[42]=z[15]*z[42];
    z[54]=z[39]*npow(z[14],4);
    z[72]=z[58]*z[3];
    z[73]=z[15] + z[72];
    z[73]=z[9]*z[73];
    z[42]=z[51] + z[73] - z[62] + z[54] + z[42];
    z[42]=z[4]*z[42];
    z[51]=2*z[11];
    z[54]= - z[51]*z[66];
    z[54]=z[54] - z[65];
    z[54]=z[11]*z[39]*z[54];
    z[61]= - z[39]*z[61];
    z[54]=z[54] + z[61];
    z[54]=z[11]*z[54];
    z[54]=3*z[55] + z[54];
    z[54]=z[11]*z[54];
    z[42]=z[42] - 3*z[68] + z[54];
    z[42]=z[4]*z[42];
    z[39]=z[39]*npow(z[11],4)*z[53];
    z[39]=z[39] + z[45];
    z[39]=4*z[39] + z[42];
    z[39]=z[4]*z[39];
    z[39]= - z[56] + z[39];
    z[39]=z[4]*z[39];
    z[23]=z[35] + 2*z[23] + z[39];
    z[23]=z[1]*z[23];
    z[35]=npow(z[12],3);
    z[39]=2*z[15];
    z[42]=z[35]*z[39];
    z[45]=z[35]*z[14];
    z[54]=2*z[6];
    z[55]=z[5] + z[54];
    z[55]=z[9]*z[55];
    z[55]=z[55] + 3*z[41] - z[42] - z[40] - z[45];
    z[55]=z[11]*z[55];
    z[56]=3*z[31];
    z[61]=z[56]*z[35];
    z[42]=5*z[45] + z[42];
    z[42]=z[15]*z[42];
    z[68]=3*z[9];
    z[42]=z[55] + z[68] - 3*z[3] + z[61] + z[42];
    z[42]=z[11]*z[42];
    z[55]= - z[35]*z[71];
    z[45]= - z[15]*z[45];
    z[45]= - z[61] + z[45];
    z[45]=z[15]*z[45];
    z[61]= - z[69] - z[72];
    z[61]=z[9]*z[61];
    z[69]=z[53]*z[11];
    z[71]=z[9] + z[69];
    z[71]=z[11]*z[71];
    z[71]=z[71] - static_cast<T>(1)- z[64];
    z[71]=z[11]*z[71];
    z[73]=z[60] + z[15];
    z[71]=z[71] - z[14] + z[73];
    z[71]=z[4]*z[71];
    z[74]=3*z[62];
    z[42]=z[71] + z[42] + z[61] + z[74] + z[45] + static_cast<T>(1)+ z[55];
    z[45]=2*z[4];
    z[42]=z[42]*z[45];
    z[55]= - z[35]*z[58];
    z[58]=5*z[5];
    z[55]=z[47] + z[58] + z[55];
    z[55]=z[9]*z[55];
    z[61]=4*z[11];
    z[71]= - z[61]*z[66];
    z[71]=z[71] - z[65];
    z[71]=z[11]*z[35]*z[71];
    z[59]= - z[35]*z[59];
    z[75]=6*z[41];
    z[76]=5*z[40];
    z[55]=z[71] + z[55] + z[75] - z[76] + z[59];
    z[55]=z[11]*z[55];
    z[59]=6*z[3];
    z[71]=static_cast<T>(8)+ 5*z[62];
    z[71]=z[9]*z[71];
    z[42]=z[42] + z[55] - z[59] + z[71];
    z[42]=z[4]*z[42];
    z[35]=z[35]*npow(z[11],3)*z[53];
    z[55]= - 5*z[3] + z[58] + z[6];
    z[55]=z[9]*z[55];
    z[35]=z[42] + 6*z[35] + z[55] - z[76] + 8*z[41];
    z[35]=z[35]*z[45];
    z[21]=z[21]*z[17];
    z[42]=z[20] - 1;
    z[18]=z[18] + z[42];
    z[18]=z[14]*z[18];
    z[18]=z[33] + z[18] - 5*z[13] + z[21];
    z[18]=z[7]*z[18];
    z[55]= - z[13]*z[54];
    z[18]=z[18] - z[19] + z[55] + 2*z[20] + z[26];
    z[18]=z[7]*z[18];
    z[26]=z[8] - z[6];
    z[18]=z[18] + 3*z[26] + z[24];
    z[26]=2*z[7];
    z[18]=z[18]*z[26];
    z[55]=5*z[27];
    z[18]=z[55] + z[18];
    z[58]=4*z[7];
    z[18]=z[18]*z[58];
    z[71]=npow(z[16],2);
    z[76]=z[71]*z[2];
    z[77]=z[2]*z[16];
    z[78]=z[77]*z[6];
    z[76]=z[76] + z[78];
    z[76]=z[76]*z[6];
    z[78]=z[71]*z[41];
    z[76]=z[76] + z[78];
    z[78]=z[54] + z[3];
    z[79]=static_cast<T>(1)- z[19];
    z[79]=z[7]*z[79];
    z[78]=2*z[78] + z[79];
    z[78]=z[78]*z[26];
    z[79]=13*z[6];
    z[80]= - 7*z[10] + z[79];
    z[80]=z[9]*z[80];
    z[55]=z[80] + z[55] + z[78];
    z[78]=4*z[30];
    z[55]=z[78]*z[55];
    z[80]= - z[44]*z[57];
    z[81]=z[7]*z[3];
    z[80]=z[80] + z[27] + z[81];
    z[50]=z[11]*z[50]*z[80];
    z[50]=8*z[50] + z[55] - z[76];
    z[50]=z[11]*z[50];
    z[55]=z[39]*z[71];
    z[80]=z[55]*z[8];
    z[80]=z[80] - z[71];
    z[81]=z[8]*z[16];
    z[82]=z[6]*z[16];
    z[82]=z[82] - 4*z[77] - 10*z[81] - z[80];
    z[82]=z[6]*z[82];
    z[83]=z[39]*z[3];
    z[84]= - z[83] - 10;
    z[84]=z[81]*z[84];
    z[85]=z[5] - z[16];
    z[85]=z[2]*z[85];
    z[80]=4*z[85] + z[84] - z[80];
    z[80]=z[3]*z[80];
    z[32]=z[32]*z[26];
    z[32]=z[32] - z[19] - static_cast<T>(20)+ 7*z[20];
    z[32]=z[7]*z[32];
    z[32]=z[32] - 11*z[6] - 6*z[10] + z[38];
    z[32]=z[32]*z[26];
    z[38]=5*z[10];
    z[84]=z[38] - 2*z[5];
    z[84]=z[3]*z[84];
    z[29]=z[32] + 5*z[29] + z[84];
    z[29]=z[29]*z[57];
    z[18]=4*z[23] + z[35] + z[50] + z[29] + z[18] + z[82] + z[80];
    z[18]=z[1]*z[18];
    z[23]=z[14]*z[30]*z[24];
    z[23]= - z[27] + z[23];
    z[23]=z[23]*z[30];
    z[27]=z[44]*z[30];
    z[29]=npow(z[12],2);
    z[30]=z[6]*z[29];
    z[27]=z[30] + 3*z[27];
    z[27]=z[27]*z[57];
    z[23]=z[27] + 2*z[23] - z[76];
    z[23]=z[23]*z[51];
    z[27]=2*z[71];
    z[30]= - z[27] + 5*z[77];
    z[32]=z[2] - z[16];
    z[35]= - z[32]*z[54];
    z[35]=z[35] - z[30];
    z[35]=z[6]*z[35];
    z[30]= - z[3]*z[30];
    z[50]=z[14] - z[33];
    z[50]=z[7]*z[50];
    z[50]=z[19] + z[50];
    z[50]=z[50]*z[26];
    z[50]=z[50] - z[48];
    z[50]=z[7]*z[50];
    z[28]= - z[28] + z[50];
    z[28]=z[28]*z[58];
    z[50]=z[29]*z[63];
    z[57]=2*z[10] - z[47];
    z[57]=z[7]*z[57];
    z[50]=z[50] + 8*z[57];
    z[50]=z[9]*z[50];
    z[23]=z[23] + z[50] + z[28] + z[35] + z[30];
    z[23]=z[11]*z[23];
    z[28]= - z[5] + z[79];
    z[28]=z[9]*z[28];
    z[28]=z[28] - z[75] + z[29] + z[40];
    z[28]=z[11]*z[28];
    z[30]=4*z[14];
    z[35]= - 4*z[15] - z[30];
    z[35]=z[29]*z[35];
    z[28]=z[28] - 8*z[9] + z[59] - z[47] - 3*z[2] + z[35];
    z[28]=z[11]*z[28];
    z[35]=7*z[69] - z[54] - z[68];
    z[35]=z[11]*z[35];
    z[50]=3*z[64];
    z[35]=z[35] + static_cast<T>(1)+ z[50];
    z[35]=z[11]*z[35];
    z[54]=3*z[69];
    z[57]=z[54] - z[66];
    z[57]=z[11]*z[57];
    z[57]=z[64] + z[57];
    z[57]=z[11]*z[57];
    z[57]=z[57] - z[73];
    z[57]=z[11]*z[57];
    z[57]=z[56] + z[57];
    z[57]=z[4]*z[57];
    z[35]=z[57] + z[35] - z[30] - z[73];
    z[35]=z[35]*z[45];
    z[57]=z[29]*z[31];
    z[57]=static_cast<T>(2)+ z[57];
    z[59]=z[30] + z[15];
    z[68]=z[15]*z[29]*z[59];
    z[73]=5*z[15];
    z[72]=z[73] + z[72];
    z[72]=z[9]*z[72];
    z[28]=z[35] + z[28] + z[72] - z[74] + 4*z[57] + z[68];
    z[28]=z[4]*z[28];
    z[29]= - z[29]*z[67];
    z[35]= - z[5] + z[47];
    z[35]=z[9]*z[35];
    z[29]=z[29] + z[35] + z[43];
    z[29]=z[29]*z[51];
    z[35]=2*z[2];
    z[43]= - static_cast<T>(5)- z[83];
    z[43]=z[9]*z[43];
    z[28]=z[28] + z[29] + z[43] + 4*z[3] + z[35] - z[6];
    z[28]=z[28]*z[45];
    z[29]= - z[35] + z[17] - z[52] - 7*z[16];
    z[27]=z[27] - z[81];
    z[27]=z[27]*z[39];
    z[35]=z[39]*z[16];
    z[35]=z[35] + 1;
    z[43]= - z[35]*z[24];
    z[47]= - z[14]*z[40];
    z[29]=z[43] - z[27] + 2*z[29] + z[47];
    z[29]=z[3]*z[29];
    z[17]= - z[17] + z[38] - z[5];
    z[38]=z[14]*z[5];
    z[43]= - z[10]*z[39];
    z[38]=z[38] + z[43];
    z[38]=z[3]*z[38];
    z[43]=static_cast<T>(4)- z[20];
    z[34]=2*z[43] - z[34];
    z[34]=z[34]*z[58];
    z[43]= - z[8]*z[39];
    z[43]=static_cast<T>(15)+ z[43];
    z[43]=z[6]*z[43];
    z[17]=z[34] + z[38] + 2*z[17] + z[43];
    z[17]=z[9]*z[17];
    z[25]= - static_cast<T>(1)- z[25];
    z[25]=z[14]*z[25];
    z[21]=z[25] + z[13] - z[21];
    z[21]=z[7]*z[14]*z[21];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[13]*z[20];
    z[20]=z[21] - z[33] + z[20];
    z[20]=z[20]*z[26];
    z[20]=z[20] - z[49];
    z[20]=z[7]*z[20];
    z[20]= - z[3] + z[20];
    z[20]=z[20]*z[58];
    z[21]=z[6] - z[27] - 14*z[16] - 5*z[2];
    z[21]=z[6]*z[21];
    z[17]=z[18] + z[28] + z[23] + z[17] + z[20] + z[29] + 2*z[40] + 
    z[21];
    z[17]=z[1]*z[17];
    z[18]= - z[11]*z[76];
    z[20]=z[77] - z[71];
    z[21]= - z[6]*z[32];
    z[21]=z[21] - z[20];
    z[21]=z[6]*z[21];
    z[20]= - z[3]*z[20];
    z[23]=z[7]*z[33];
    z[19]=z[19] + z[23];
    z[19]=z[19]*z[26];
    z[19]=z[3] + z[19];
    z[19]=z[19]*z[78];
    z[23]= - z[9]*z[44]*z[58];
    z[18]=z[18] + z[23] + z[19] + z[21] + z[20];
    z[18]=z[11]*z[18];
    z[19]=z[55] + 4*z[16];
    z[20]= - z[15]*z[16];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[20]*z[24];
    z[20]=z[20] + z[2] - z[19];
    z[20]=z[3]*z[20];
    z[21]=z[63] - 1;
    z[21]=z[2]*z[21];
    z[19]=z[21] - z[19];
    z[19]=z[6]*z[19];
    z[21]=z[31]*z[7];
    z[23]=z[21] + z[14] + z[33];
    z[23]=z[23]*z[26];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[78];
    z[24]= - z[36] + z[37];
    z[24]=z[9]*z[24];
    z[18]=z[18] + z[24] + z[23] + z[19] + z[20];
    z[18]=z[11]*z[18];
    z[19]=2*z[69] - z[66];
    z[19]=z[19]*z[61];
    z[20]= - z[6]*z[39];
    z[19]=z[19] + z[20] - z[50];
    z[19]=z[11]*z[19];
    z[19]=z[19] + z[60] - z[59];
    z[19]=z[11]*z[19];
    z[20]= - 2*z[66] + z[54];
    z[20]=z[11]*z[20];
    z[20]=z[20] - z[65];
    z[20]=z[11]*z[20];
    z[20]=z[60] + z[20];
    z[20]=z[11]*z[20];
    z[23]=z[14]*z[73];
    z[20]=z[20] + z[56] + z[23];
    z[20]=z[11]*z[20];
    z[23]= - z[15]*z[31];
    z[23]= - z[70] + z[23];
    z[20]=3*z[23] + z[20];
    z[20]=z[4]*z[20];
    z[23]=z[15]*z[30];
    z[19]=z[20] + z[19] + 5*z[31] + z[23];
    z[19]=z[19]*z[45];
    z[20]=18*z[69] - 6*z[6] - 5*z[9];
    z[20]=z[11]*z[20];
    z[20]=z[20] - 5*z[64] + static_cast<T>(4)- z[63];
    z[20]=z[11]*z[20];
    z[23]= - z[39] - 5*z[14];
    z[19]=z[19] + z[20] + 2*z[23] + z[60];
    z[19]=z[4]*z[19];
    z[20]=2*z[41] + 11*z[53];
    z[20]=z[11]*z[20];
    z[20]=z[20] - z[9] + z[2] - z[48];
    z[20]=z[11]*z[20];
    z[23]= - z[15]*z[2];
    z[24]= - z[9]*z[39];
    z[19]=z[19] + z[20] + z[24] + z[62] + static_cast<T>(4)+ z[23];
    z[19]=z[19]*z[45];
    z[20]=z[13] + z[22];
    z[20]=z[20]*z[21];
    z[21]= - z[14]*z[13]*z[42];
    z[20]=z[21] + z[20];
    z[20]=z[20]*z[26];
    z[21]=z[14]*z[46];
    z[20]=z[20] + z[21] + z[33];
    z[20]=z[20]*z[78];
    z[21]=z[14]*z[2];
    z[21]=z[21] + z[35];
    z[21]=z[3]*z[21];
    z[22]= - z[15]*z[10];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=2*z[22] - z[63];
    z[22]=z[9]*z[22];
    z[23]=2*z[16] + z[2];
    z[23]=z[15]*z[23];
    z[23]= - static_cast<T>(2)+ z[23];
    z[23]=z[6]*z[23];
    z[17]=z[17] + z[19] + z[18] + z[22] + z[20] + z[23] + z[21];
    z[17]=z[1]*z[17];
    z[18]=z[11] - 2*z[14] - z[15];
    z[18]=z[18]*z[45];

    r += static_cast<T>(1)+ z[17] + z[18];
 
    return r;
}

template double qg_2lNLC_r1264(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1264(const std::array<dd_real,31>&);
#endif
