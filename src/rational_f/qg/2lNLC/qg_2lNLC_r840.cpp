#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r840(const std::array<T,31>& k) {
  T z[27];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[12];
    z[9]=k[13];
    z[10]=k[8];
    z[11]=k[14];
    z[12]=k[2];
    z[13]=k[3];
    z[14]=z[6]*z[2];
    z[15]=z[14] + 1;
    z[15]=z[5]*z[15];
    z[15]= - z[6] + z[15];
    z[16]=n<T>(1,2)*z[12];
    z[17]=z[16] - z[2];
    z[18]=n<T>(1,2)*z[5];
    z[15]=z[18]*z[17]*z[15];
    z[16]=z[11]*z[16];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[6]*z[16];
    z[15]=z[15] + z[11] + z[16];
    z[15]=z[5]*z[15];
    z[16]=npow(z[2],2);
    z[17]=z[6]*npow(z[2],3);
    z[17]=z[16] + z[17];
    z[17]=z[5]*z[17];
    z[16]=z[16]*z[6];
    z[17]= - z[16] + z[17];
    z[17]=z[17]*z[18];
    z[19]= - z[2]*z[11];
    z[17]=z[17] + z[19] + z[14];
    z[17]=z[17]*z[18];
    z[19]=z[8]*z[7];
    z[20]=z[2]*z[10];
    z[20]=z[19] - z[20];
    z[20]= - static_cast<T>(1)- 3*z[20];
    z[20]=z[9]*z[20];
    z[21]=z[10]*z[7];
    z[22]= - n<T>(1,4) + z[21];
    z[22]=z[8]*z[22];
    z[20]=n<T>(1,4)*z[20] + n<T>(3,4)*z[10] + z[22];
    z[22]=z[6]*z[11];
    z[23]=z[7]*z[22];
    z[17]=z[17] + 3*z[20] + n<T>(1,4)*z[23];
    z[17]=z[4]*z[17];
    z[20]=npow(z[9],2);
    z[23]= - static_cast<T>(1)+ z[21];
    z[23]=z[2]*z[23];
    z[23]=z[23] - z[13] + z[7];
    z[23]=z[23]*z[20];
    z[21]=static_cast<T>(1)- n<T>(3,2)*z[21];
    z[21]=z[8]*z[21];
    z[21]= - z[10] + z[21];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[21]=z[9]*z[21];
    z[23]=z[8]*z[10];
    z[21]= - n<T>(1,4)*z[23] + z[21];
    z[24]=n<T>(1,2)*z[22];
    z[15]=z[17] + z[15] + 3*z[21] + z[24];
    z[17]=npow(z[3],2);
    z[21]=n<T>(1,2)*z[17];
    z[15]=z[15]*z[21];
    z[25]=n<T>(1,2)*z[8];
    z[26]=z[9]*z[19];
    z[26]=z[26] - z[10] - z[25];
    z[26]=z[9]*z[26];
    z[26]=n<T>(3,2)*z[23] + z[26];
    z[16]= - n<T>(1,2)*z[2] - z[16];
    z[16]=z[5]*z[16];
    z[16]=z[14] + z[16];
    z[16]=z[5]*z[16];
    z[16]=z[16] + z[11] - z[6];
    z[16]=z[16]*z[18];
    z[16]=z[16] + 3*z[26] + z[24];
    z[16]=z[4]*z[16];
    z[18]= - z[25] + z[9];
    z[18]=z[9]*z[18];
    z[18]= - n<T>(1,2)*z[23] + z[18];
    z[24]=3*z[9];
    z[18]=z[18]*z[24];
    z[25]=n<T>(1,2) + z[14];
    z[25]=z[5]*z[25];
    z[25]= - z[6] + z[25];
    z[25]=z[5]*z[25];
    z[25]=z[22] + z[25];
    z[26]=n<T>(1,4)*z[5];
    z[25]=z[25]*z[26];
    z[16]=n<T>(1,2)*z[16] + z[18] + z[25];
    z[16]=z[16]*z[17];
    z[17]= - z[20]*z[19];
    z[17]= - n<T>(3,4)*z[23] + z[17];
    z[17]=z[17]*z[24];
    z[14]=z[5]*z[14];
    z[14]= - z[6] + z[14];
    z[14]=z[5]*z[14];
    z[14]=z[22] + z[14];
    z[14]=z[14]*z[26];
    z[14]=z[17] + z[14];
    z[14]=z[4]*z[14];
    z[17]=z[8]*npow(z[9],3);
    z[14]=3*z[17] + z[14];
    z[14]=z[1]*z[14]*z[21];
    z[14]=z[16] + z[14];
    z[14]=z[1]*z[14];
    z[14]=z[15] + z[14];

    r += z[14]*npow(z[1],2);
 
    return r;
}

template double qg_2lNLC_r840(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r840(const std::array<dd_real,31>&);
#endif
