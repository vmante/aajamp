#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r987(const std::array<T,31>& k) {
  T z[79];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[12];
    z[10]=k[13];
    z[11]=k[14];
    z[12]=k[19];
    z[13]=k[2];
    z[14]=k[30];
    z[15]=k[9];
    z[16]=k[3];
    z[17]=k[23];
    z[18]=k[20];
    z[19]=k[22];
    z[20]=k[17];
    z[21]=7*z[2];
    z[22]=npow(z[3],2);
    z[23]= - z[22]*z[21];
    z[24]=z[8] - z[12];
    z[25]=6*z[24];
    z[26]=7*z[4];
    z[23]=z[23] + z[26] - z[25];
    z[27]=2*z[6];
    z[23]=z[23]*z[27];
    z[28]=z[22]*z[13];
    z[29]=3*z[14];
    z[30]= - z[29] - z[11];
    z[30]=z[30]*z[28];
    z[31]=z[22]*z[2];
    z[32]=14*z[4];
    z[33]=z[32]*z[31];
    z[34]=z[15]*z[18];
    z[35]= - z[17] + z[11];
    z[35]=z[10]*z[35];
    z[36]=z[10] + z[20];
    z[37]= - z[15] - z[36];
    z[37]=z[4]*z[37];
    z[38]=14*z[8];
    z[39]=z[38] - z[10];
    z[40]=z[8]*z[39];
    z[41]=3*z[20];
    z[42]=n<T>(19,2)*z[8] - 16*z[12] + z[41] + z[17];
    z[42]=z[9]*z[42];
    z[23]=z[23] + z[42] + z[33] + z[40] + z[37] + z[30] + z[34] + z[35];
    z[23]=z[6]*z[23];
    z[30]=z[12] - z[17];
    z[33]=z[22]*z[16];
    z[34]=z[30]*z[33];
    z[35]=2*z[10];
    z[37]= - z[35] + 15*z[33];
    z[37]=z[4]*z[37];
    z[40]=n<T>(97,2)*z[4];
    z[42]=42*z[8];
    z[43]=z[40] - z[42];
    z[43]=z[8]*z[43];
    z[44]= - z[11] + n<T>(27,2)*z[8];
    z[31]=z[44]*z[31];
    z[44]=z[15] - z[17];
    z[45]= - z[42] - z[44] - 28*z[4];
    z[45]=z[9]*z[45];
    z[46]= - 6*z[11] - z[10];
    z[46]=z[10]*z[46];
    z[31]=z[45] + z[31] + z[43] + z[37] + z[46] + z[34];
    z[31]=z[9]*z[31];
    z[34]=z[15] + z[12];
    z[37]=2*z[22];
    z[43]=z[34]*z[37];
    z[45]=npow(z[10],2);
    z[46]=z[45] - 58*z[22];
    z[46]=z[4]*z[46];
    z[47]=z[32]*z[8];
    z[48]= - 31*z[22] + z[47];
    z[48]=z[8]*z[48];
    z[49]=3*z[8];
    z[50]=z[49]*z[4];
    z[37]=z[37] + z[50];
    z[51]=14*z[9];
    z[37]=z[37]*z[51];
    z[37]=z[37] + z[48] + z[43] + z[46];
    z[37]=z[9]*z[37];
    z[43]=2*z[11];
    z[46]= - z[14] + z[43];
    z[46]=z[46]*z[22];
    z[48]=npow(z[8],2);
    z[52]=z[48]*z[51];
    z[53]=z[22]*z[4];
    z[54]=z[15] - z[20] - 12*z[24];
    z[54]=z[6]*z[9]*z[54];
    z[46]=z[54] + z[52] + z[46] - z[53];
    z[46]=z[6]*z[46];
    z[52]=z[22]*z[10];
    z[54]=z[52] + 18*z[53];
    z[54]=z[8]*z[54];
    z[55]=z[45]*z[19];
    z[56]=z[4]*z[55];
    z[37]=z[46] + z[37] + z[56] + z[54];
    z[37]=z[7]*z[37];
    z[46]=3*z[10];
    z[54]=z[46]*z[22]*z[17];
    z[56]=z[35]*z[22];
    z[57]=z[56] - 17*z[53];
    z[57]=z[8]*z[57];
    z[54]=z[54] + z[57];
    z[54]=z[2]*z[54];
    z[57]=z[15]*z[19];
    z[58]= - z[10]*z[19];
    z[58]= - z[57] + z[58];
    z[58]=z[4]*z[58];
    z[59]= - z[18] + z[15];
    z[59]=z[15]*z[59];
    z[60]=2*z[15];
    z[61]=z[4]*z[60];
    z[59]=z[59] + z[61];
    z[59]=z[8]*z[59];
    z[61]=npow(z[15],2);
    z[62]= - z[19]*z[61];
    z[23]=z[37] + z[23] + z[31] + z[54] + z[59] + z[58] + z[62] - z[55];
    z[23]=z[7]*z[23];
    z[31]=npow(z[3],3);
    z[37]=z[31]*z[13];
    z[54]=npow(z[5],2);
    z[58]=z[54]*z[37];
    z[59]=z[31]*z[4];
    z[62]=z[5]*z[31];
    z[62]=6*z[59] - 7*z[62];
    z[63]=2*z[5];
    z[62]=z[2]*z[62]*z[63];
    z[58]=7*z[58] + z[62];
    z[58]=z[2]*z[58];
    z[62]= - z[54]*z[26];
    z[58]=z[62] + z[58];
    z[58]=z[2]*z[58];
    z[62]=2*z[12];
    z[64]=z[62] + z[60];
    z[65]=12*z[9];
    z[66]=z[18] - z[5];
    z[65]=z[66]*z[65];
    z[65]=z[65] + 14*z[54];
    z[65]=z[2]*z[65];
    z[66]=14*z[18];
    z[65]=n<T>(1,2)*z[8] + 11*z[5] - z[66] + z[17] + z[65] - z[64];
    z[65]=z[9]*z[65];
    z[67]=3*z[5];
    z[68]=z[67]*z[37];
    z[69]=2*z[4];
    z[70]=z[69] - z[67];
    z[70]=z[2]*z[31]*z[70];
    z[68]=z[68] + z[70];
    z[68]=z[2]*z[68];
    z[70]=npow(z[13],2);
    z[71]=z[70]*z[5];
    z[72]= - z[31]*z[71];
    z[68]=z[72] + z[68];
    z[68]=z[68]*z[21];
    z[25]=z[68] - z[25];
    z[25]=z[25]*z[27];
    z[68]=z[60]*z[18];
    z[72]=z[10]*z[11];
    z[68]=z[68] - z[72];
    z[39]=z[63] - z[11] + z[39];
    z[39]=z[8]*z[39];
    z[73]=z[5]*z[4];
    z[25]=z[25] + z[65] + 2*z[58] + z[39] - 13*z[73] + z[68];
    z[25]=z[6]*z[25];
    z[39]=z[72]*z[31];
    z[58]=z[59]*z[10];
    z[58]=z[58] - z[39];
    z[65]=z[31]*z[8];
    z[74]=16*z[59] - 7*z[65];
    z[75]=2*z[8];
    z[74]=z[74]*z[75];
    z[76]= - z[31]*z[69];
    z[76]=z[76] - z[65];
    z[76]=z[76]*z[51];
    z[74]=z[76] + z[74] + z[58];
    z[74]=z[9]*z[74];
    z[76]=z[32]*z[31];
    z[77]=z[48]*z[76];
    z[58]= - z[6]*z[58];
    z[58]=z[58] + z[77] + z[74];
    z[58]=z[7]*z[58];
    z[74]= - z[2]*z[48]*z[59];
    z[77]=z[16]*z[59];
    z[78]=z[2]*z[65];
    z[77]=z[77] + z[78];
    z[78]=npow(z[9],2);
    z[77]=z[77]*z[78];
    z[74]=z[74] + z[77];
    z[77]=z[14] - z[11];
    z[37]=z[77]*npow(z[6],2)*z[37];
    z[37]=z[58] + 14*z[74] + z[37];
    z[37]=z[7]*z[37];
    z[58]=z[17]*z[31];
    z[58]=z[58] + z[65];
    z[65]=npow(z[2],2);
    z[58]=z[65]*z[45]*z[58];
    z[74]=28*z[48];
    z[44]=z[9]*z[44];
    z[44]=z[74] + z[44];
    z[44]=z[9]*z[44];
    z[59]=z[65]*z[59];
    z[24]=z[24]*z[9];
    z[59]=7*z[59] - 12*z[24];
    z[59]=z[59]*z[27];
    z[44]=z[44] + z[59];
    z[44]=z[6]*z[44];
    z[47]=z[78]*z[47];
    z[37]=z[37] + z[44] + z[58] + z[47];
    z[37]=z[7]*z[37];
    z[44]=z[4]*z[11];
    z[47]=z[44]*z[31];
    z[39]=z[47] - z[39];
    z[47]=z[5]*z[76];
    z[47]=z[47] - z[39];
    z[47]=z[5]*z[47];
    z[39]=z[8]*z[39];
    z[39]=z[47] + z[39];
    z[39]=z[2]*z[39];
    z[47]=z[54]*z[32];
    z[31]= - z[16]*z[31]*z[47];
    z[31]=z[31] + z[39];
    z[31]=z[31]*z[65];
    z[39]=6*z[18] + z[5];
    z[39]=z[2]*z[39];
    z[39]= - static_cast<T>(7)+ z[39];
    z[54]=2*z[9];
    z[39]=z[54]*z[39];
    z[39]=z[39] - z[38] - z[67] + z[66] + z[4];
    z[39]=z[8]*z[39];
    z[58]=z[4]*z[10];
    z[59]= - z[58] + 4*z[72];
    z[39]= - z[59] + z[39];
    z[39]=z[9]*z[39];
    z[65]=z[5] + 7*z[8];
    z[65]=z[65]*z[54];
    z[65]=z[65] + z[72];
    z[65]=z[8]*z[65];
    z[66]=npow(z[3],4);
    z[76]=z[66]*npow(z[2],4);
    z[77]=z[47]*z[76];
    z[76]=z[73]*z[76];
    z[24]=7*z[76] - 6*z[24];
    z[24]=z[24]*z[27];
    z[24]=z[24] + z[77] + z[65];
    z[24]=z[6]*z[24];
    z[65]=z[9]*z[8];
    z[65]=z[48] + z[65];
    z[65]=npow(z[7],4)*z[51]*z[66]*z[4]*z[65];
    z[66]= - z[9]*z[5]*z[72];
    z[24]=z[65] + z[66] + z[24];
    z[24]=z[1]*z[24];
    z[65]=z[15] - z[14];
    z[66]=z[5]*z[13];
    z[76]=z[65]*z[66];
    z[77]=3*z[15];
    z[76]=z[76] + z[14] - z[77];
    z[76]=z[5]*z[76];
    z[68]=z[76] - z[44] + z[68];
    z[68]=z[8]*z[68];
    z[76]=z[44] - z[72];
    z[78]=z[5]*z[76];
    z[24]=z[24] + z[37] + z[25] + z[39] + z[31] + z[78] + z[68];
    z[24]=z[1]*z[24];
    z[25]=6*z[13];
    z[31]= - z[25] + z[71];
    z[31]=z[8]*z[31];
    z[37]= - 4*z[22] + z[73];
    z[37]=z[2]*z[37];
    z[28]=z[28] + z[4];
    z[37]=2*z[28] + z[37];
    z[21]=z[37]*z[21];
    z[37]=z[13] + z[71];
    z[37]=z[12]*z[37];
    z[21]=z[21] + 6*z[37] + z[31];
    z[21]=z[21]*z[27];
    z[27]=z[70]*z[22];
    z[31]=z[27] + 1;
    z[31]=z[11]*z[31];
    z[25]= - z[12]*z[25];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=2*z[25] - 13*z[27];
    z[25]=z[5]*z[25];
    z[27]=z[38] - z[14];
    z[27]=z[13]*z[27];
    z[27]=n<T>(7,2)*z[66] - n<T>(1,2) + z[27];
    z[27]=z[8]*z[27];
    z[37]=z[5]*z[32];
    z[37]= - n<T>(31,2)*z[22] + z[37];
    z[37]=z[37]*z[67];
    z[37]=25*z[53] + z[37];
    z[37]=z[2]*z[37];
    z[28]=z[5]*z[28];
    z[28]=n<T>(71,2)*z[28] + z[37];
    z[28]=z[2]*z[28];
    z[37]=4*z[18];
    z[39]=z[37] - n<T>(7,2)*z[5];
    z[39]=z[2]*z[39];
    z[39]=static_cast<T>(2)+ z[39];
    z[39]=z[9]*z[39];
    z[21]=z[21] + 3*z[39] + z[28] + z[27] + z[25] - n<T>(9,2)*z[4] - z[18]
    - z[62] + z[31];
    z[21]=z[6]*z[21];
    z[25]=z[61]*z[13];
    z[27]=z[25]*z[19];
    z[28]= - z[57] - z[27];
    z[28]=z[13]*z[28];
    z[31]=z[14] - z[12];
    z[31]=z[16]*z[31];
    z[34]=z[13]*z[34];
    z[31]=z[31] + 2*z[34];
    z[31]=z[13]*z[31];
    z[34]= - z[10]*npow(z[16],2);
    z[31]=z[31] + z[34];
    z[31]=z[31]*z[22];
    z[34]= - 7*z[33] - z[65];
    z[34]=z[13]*z[34];
    z[34]=static_cast<T>(7)+ z[34];
    z[34]=z[34]*z[63];
    z[28]=z[34] + z[32] + z[31] - z[46] + z[28] + z[77] + z[14] - z[19];
    z[28]=z[5]*z[28];
    z[31]= - z[35]*z[33];
    z[33]=z[43] - 13*z[33];
    z[33]=z[4]*z[33];
    z[34]=2*z[16] + z[13];
    z[34]=z[34]*z[22];
    z[34]=z[34] + z[4];
    z[39]=14*z[5];
    z[34]=z[34]*z[39];
    z[61]=z[11] - z[19];
    z[61]=z[61]*z[10];
    z[31]=z[34] + z[33] + z[61] + z[31];
    z[31]=z[5]*z[31];
    z[33]=npow(z[11],2);
    z[34]=z[33] + 13*z[22];
    z[34]=z[4]*z[34];
    z[22]=z[5]*z[22];
    z[22]= - 28*z[22] - z[56] + z[34];
    z[22]=z[5]*z[22];
    z[34]=z[56] + 3*z[53];
    z[34]=z[8]*z[34];
    z[52]= - z[17]*z[52];
    z[33]=z[4]*z[20]*z[33];
    z[22]=z[34] + z[22] + z[52] + z[33];
    z[22]=z[2]*z[22];
    z[33]=z[44]*z[41];
    z[34]=2*z[44];
    z[52]= - z[72] + z[34];
    z[52]=z[8]*z[52];
    z[22]=z[22] + z[52] + z[31] + z[55] + z[33];
    z[22]=z[2]*z[22];
    z[31]=z[10] + z[11];
    z[31]= - z[31]*z[63];
    z[33]= - z[37] + n<T>(1,2)*z[5];
    z[33]=z[33]*z[49];
    z[37]= - z[11]*z[20];
    z[31]=z[33] + z[31] + z[37] + z[45];
    z[31]=z[2]*z[31];
    z[33]=z[29] - z[41];
    z[37]=z[2]*z[49];
    z[37]=static_cast<T>(2)+ z[37];
    z[37]=z[37]*z[51];
    z[31]=z[37] + z[31] - 9*z[8] - 42*z[5] - z[4] + 5*z[10] + z[60] + 
    z[30] + z[33];
    z[31]=z[9]*z[31];
    z[37]=z[60] - z[10];
    z[45]=z[25] + z[14] + z[15];
    z[45]=z[13]*z[45];
    z[45]=n<T>(3,2) + z[45];
    z[45]=z[5]*z[45];
    z[25]=z[45] + 5*z[4] - 2*z[25] - z[43] - z[18] - z[37];
    z[25]=z[8]*z[25];
    z[45]=12*z[18] + z[19];
    z[45]=z[15]*z[45];
    z[27]= - z[61] + z[45] + z[27];
    z[45]=z[20] - 8*z[15];
    z[45]=3*z[45] + z[11];
    z[45]=z[4]*z[45];
    z[21]=z[24] + z[23] + z[21] + z[31] + z[22] + z[25] + z[28] + 2*
    z[27] + z[45];
    z[21]=z[1]*z[21];
    z[22]=14*z[12];
    z[23]=z[22] + z[77];
    z[24]=14*z[6];
    z[25]=z[69]*z[2];
    z[27]=static_cast<T>(1)- z[25];
    z[27]=z[27]*z[24];
    z[27]=z[27] + z[54] + 10*z[8] - 11*z[4] + z[43] - z[18] - z[33] - 
    z[23];
    z[27]=z[6]*z[27];
    z[28]=n<T>(53,2)*z[4] - z[38];
    z[28]=z[28]*z[49];
    z[31]=4*z[4];
    z[33]= - z[31] - z[49];
    z[33]=z[33]*z[51];
    z[28]=z[33] + z[28] - z[59];
    z[28]=z[9]*z[28];
    z[23]=11*z[8] + z[20] - z[23];
    z[23]=z[9]*z[23];
    z[33]= - z[8]*z[10];
    z[23]=z[23] + z[33] - z[72] + z[58];
    z[23]=z[6]*z[23];
    z[33]=z[48]*z[69];
    z[38]=z[9]*z[50];
    z[33]=z[33] + z[38];
    z[33]=z[7]*z[33]*z[51];
    z[38]= - z[19]*z[58];
    z[45]=z[4]*z[74];
    z[23]=z[33] + z[23] + z[28] + z[38] + z[45];
    z[23]=z[7]*z[23];
    z[28]=z[32] + z[17];
    z[28]=z[16]*z[28];
    z[32]=z[15]*z[16];
    z[33]=z[2]*z[42];
    z[28]=z[33] - z[32] + static_cast<T>(56)+ z[28];
    z[28]=z[9]*z[28];
    z[33]=z[17] + z[20];
    z[28]=z[28] - 51*z[8] - n<T>(113,2)*z[4] + z[35] - 2*z[33] + 3*z[12];
    z[28]=z[9]*z[28];
    z[37]= - 28*z[8] + z[40] + z[18] - z[37];
    z[37]=z[8]*z[37];
    z[38]=z[19]*z[60];
    z[40]=4*z[11];
    z[42]= - z[40] + z[17] + z[19];
    z[42]=z[10]*z[42];
    z[45]= - z[20] + z[35];
    z[45]=z[4]*z[45];
    z[23]=z[23] + z[27] + z[28] + z[37] + z[45] + z[38] + z[42];
    z[23]=z[7]*z[23];
    z[27]=3*z[17];
    z[28]=2*z[19];
    z[37]=3*z[11];
    z[38]=z[37] + z[27] + z[28];
    z[38]=z[10]*z[38];
    z[42]= - 28*z[73] + z[76];
    z[42]=z[5]*z[42];
    z[34]= - z[72] - z[34];
    z[34]=z[8]*z[34];
    z[44]= - z[20]*z[44];
    z[34]=z[34] + z[44] + z[42];
    z[34]=z[2]*z[34];
    z[42]=z[4]*z[16];
    z[44]=static_cast<T>(1)+ z[42];
    z[44]=z[44]*z[39];
    z[45]=z[17] + z[10];
    z[44]=z[44] + 3*z[45] - n<T>(59,2)*z[4];
    z[44]=z[5]*z[44];
    z[26]=z[5] - z[26] + z[40] + z[10];
    z[26]=z[8]*z[26];
    z[40]=z[11]*z[41];
    z[45]= - 2*z[20] - z[10];
    z[45]=z[4]*z[45];
    z[26]=z[34] + z[26] + z[44] + z[45] + z[40] + z[38];
    z[26]=z[2]*z[26];
    z[25]= - z[5]*z[25];
    z[25]=z[25] - z[31] + z[67];
    z[25]=z[2]*z[25];
    z[25]=static_cast<T>(4)+ z[25];
    z[25]=z[2]*z[25];
    z[25]= - z[71] + z[25];
    z[24]=z[25]*z[24];
    z[25]= - z[2]*z[47];
    z[31]= - n<T>(31,2)*z[4] + z[39];
    z[31]=z[5]*z[31];
    z[25]=z[31] + z[25];
    z[25]=z[2]*z[25];
    z[25]=z[54] + 3*z[25] - n<T>(43,2)*z[4] + 22*z[5];
    z[25]=z[2]*z[25];
    z[31]=z[13]*z[22];
    z[31]=n<T>(17,2) + z[31];
    z[31]=z[31]*z[66];
    z[34]= - z[13]*z[11];
    z[34]= - static_cast<T>(32)+ z[34];
    z[34]=z[13]*z[34];
    z[34]=z[34] - 3*z[71];
    z[34]=z[8]*z[34];
    z[22]=z[14] - z[22];
    z[22]=z[13]*z[22];
    z[22]=z[24] + z[34] + z[31] - n<T>(25,2) + z[22] + z[25];
    z[22]=z[6]*z[22];
    z[24]=2*z[14];
    z[25]=z[46] + z[12] + z[27] - z[24];
    z[25]=z[16]*z[25];
    z[27]=z[57]*z[13];
    z[31]= - z[27] - z[24] - z[12];
    z[31]=z[13]*z[31];
    z[34]= - z[16]*z[14];
    z[34]=z[32] + static_cast<T>(14)+ z[34];
    z[34]=z[13]*z[34];
    z[34]= - 14*z[16] + z[34];
    z[34]=z[5]*z[34];
    z[25]=z[34] + 13*z[42] + z[31] - n<T>(59,2) + z[25];
    z[25]=z[5]*z[25];
    z[31]=2*z[17];
    z[29]=z[31] - z[29];
    z[29]=z[16]*z[29];
    z[34]=15*z[8] - z[43] - z[36];
    z[34]=z[2]*z[34];
    z[29]=z[34] + z[42] - z[32] - static_cast<T>(15)+ z[29];
    z[29]=z[9]*z[29];
    z[32]= - z[60]*z[71];
    z[34]=4*z[15] - z[37];
    z[34]=z[13]*z[34];
    z[32]=z[32] - static_cast<T>(28)+ z[34];
    z[32]=z[8]*z[32];
    z[34]=z[46] - z[11];
    z[21]=z[21] + z[23] + z[22] + z[29] + z[26] + z[32] + z[25] + n<T>(91,2)
   *z[4] - z[27] - 23*z[12] + z[28] + z[24] - 24*z[18] + z[17] - z[34];
    z[21]=z[1]*z[21];
    z[22]=3*z[19];
    z[23]=z[75] + z[60] + 26*z[12] + z[24] + z[22];
    z[23]=z[13]*z[23];
    z[24]= - z[75] - n<T>(19,2)*z[5] - 46*z[4] + z[41] - z[31] + z[34];
    z[24]=z[2]*z[24];
    z[22]= - 17*z[6] + n<T>(53,2)*z[9] + 4*z[8] - 30*z[4] - z[35] + z[11] - 
    z[22] + 3*z[33] + z[14] + z[64];
    z[22]=z[7]*z[22];
    z[25]=z[35] + z[14] + z[30];
    z[25]=z[16]*z[25];
    z[26]=12*z[16] - n<T>(5,2)*z[13];
    z[26]=z[5]*z[26];
    z[27]=33*z[13] - n<T>(109,2)*z[2];
    z[27]=z[6]*z[27];

    r += n<T>(87,2) + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[27]
       + z[42];
 
    return r;
}

template double qg_2lNLC_r987(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r987(const std::array<dd_real,31>&);
#endif
