#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r290(const std::array<T,31>& k) {
  T z[121];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[5];
    z[4]=k[6];
    z[5]=k[14];
    z[6]=k[15];
    z[7]=k[4];
    z[8]=k[7];
    z[9]=k[10];
    z[10]=k[8];
    z[11]=k[12];
    z[12]=k[3];
    z[13]=k[13];
    z[14]=k[9];
    z[15]=k[21];
    z[16]=npow(z[4],5);
    z[17]=npow(z[9],2);
    z[18]=z[16]*z[17];
    z[19]=47*z[12];
    z[20]=z[18]*z[19];
    z[21]=npow(z[10],2);
    z[22]=z[21]*z[3];
    z[23]=3*z[22];
    z[24]= - z[16]*z[23];
    z[20]=z[20] + z[24];
    z[24]=npow(z[8],3);
    z[20]=z[20]*z[24];
    z[25]=npow(z[6],3);
    z[26]=z[18]*z[25];
    z[27]=z[2]*z[26];
    z[20]=21*z[27] + z[20];
    z[27]=z[6]*z[9];
    z[28]=z[16]*z[27];
    z[18]= - z[18] - n<T>(7,2)*z[28];
    z[18]=z[8]*z[18];
    z[28]=z[17]*z[6];
    z[29]=z[16]*z[28];
    z[18]=z[29] + n<T>(1,4)*z[18];
    z[29]=npow(z[8],2);
    z[18]=z[18]*z[29];
    z[26]=n<T>(21,8)*z[26];
    z[18]= - z[26] + 53*z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(1,4)*z[20] + z[18];
    z[18]=z[7]*z[18];
    z[20]=npow(z[2],2);
    z[26]= - z[20]*z[26];
    z[18]=z[26] + z[18];
    z[18]=z[18]*npow(z[7],3);
    z[26]=npow(z[2],4);
    z[30]=z[26]*z[25];
    z[31]=npow(z[4],6);
    z[32]=npow(z[3],2);
    z[33]=npow(z[5],3);
    z[34]= - z[33]*z[31]*z[32]*z[30];
    z[35]=z[10]*z[11];
    z[36]=z[35]*z[9];
    z[37]=z[25]*z[36];
    z[34]=n<T>(4,3)*z[37] + z[34];
    z[31]=z[31]*z[24]*npow(z[7],6)*z[28];
    z[31]=2*z[34] - n<T>(53,4)*z[31];
    z[31]=z[1]*z[31];
    z[34]=z[10] - z[11];
    z[34]=z[34]*z[9];
    z[37]=z[35] + n<T>(1,3)*z[34];
    z[37]=z[6]*z[37];
    z[38]=z[21]*z[11];
    z[39]=4*z[38];
    z[37]=8*z[37] - z[39] - n<T>(13,2)*z[36];
    z[37]=z[6]*z[37];
    z[40]=z[17]*z[35];
    z[41]=npow(z[35],2);
    z[40]=z[41] + z[40];
    z[37]=4*z[40] + z[37];
    z[37]=z[6]*z[37];
    z[40]=npow(z[5],2);
    z[42]=z[6]*z[40];
    z[42]= - z[33] + z[42];
    z[43]=npow(z[2],3);
    z[44]=npow(z[6],2);
    z[42]=z[3]*z[44]*z[43]*z[42];
    z[30]=z[40]*z[30];
    z[30]=z[30] + z[42];
    z[42]=4*z[3];
    z[30]=z[42]*z[16]*z[30];
    z[45]=npow(z[3],3);
    z[16]=z[16]*z[45]*z[35];
    z[46]=npow(z[13],3);
    z[47]=z[46]*z[11];
    z[48]=n<T>(21,8)*z[47];
    z[16]= - z[48] - n<T>(10,3)*z[16];
    z[16]=z[8]*z[16]*z[32];
    z[49]=z[3]*z[47];
    z[16]=n<T>(21,4)*z[49] + z[16];
    z[16]=z[8]*z[16];
    z[16]= - z[48] + z[16];
    z[16]=z[8]*z[16];
    z[16]=z[31] + z[18] + z[16] + z[37] + z[30];
    z[16]=z[1]*z[16];
    z[18]=npow(z[4],4);
    z[30]=z[43]*z[18];
    z[31]=z[40]*z[30];
    z[31]=z[31] - z[38];
    z[37]=8*z[5];
    z[30]= - z[30]*z[37];
    z[48]=n<T>(21,4)*z[13];
    z[49]=8*z[11] + z[48];
    z[49]=z[10]*z[49];
    z[30]=z[30] + z[49];
    z[30]=z[6]*z[30];
    z[30]=12*z[31] + z[30];
    z[30]=z[6]*z[30];
    z[31]=z[6]*z[5];
    z[31]=12*z[40] - n<T>(37,8)*z[31];
    z[31]=z[6]*z[31];
    z[49]=6*z[33];
    z[31]= - z[49] + z[31];
    z[50]=z[18]*z[20];
    z[31]=z[3]*z[50]*z[31];
    z[51]=12*z[41];
    z[30]=z[31] + z[51] + z[30];
    z[30]=z[3]*z[30];
    z[26]= - z[5]*z[26]*z[18];
    z[31]=4*z[11];
    z[26]= - z[31] + z[26];
    z[52]=z[10]*z[2];
    z[53]= - static_cast<T>(8)+ n<T>(95,4)*z[52];
    z[54]=z[20]*z[9];
    z[55]=n<T>(21,4)*z[54];
    z[56]=z[10]*z[55];
    z[53]=n<T>(1,3)*z[53] + z[56];
    z[53]=z[9]*z[53];
    z[26]=z[53] + 2*z[26] + n<T>(127,12)*z[10];
    z[26]=z[6]*z[26];
    z[53]=4*z[10];
    z[56]= - n<T>(41,4)*z[11] - z[53];
    z[56]=z[10]*z[56];
    z[26]=z[26] + z[56] - n<T>(23,6)*z[34];
    z[26]=z[6]*z[26];
    z[56]=npow(z[11],2);
    z[57]=4*z[56];
    z[58]= - z[57] + n<T>(43,6)*z[35];
    z[58]=z[10]*z[58];
    z[34]=n<T>(169,3)*z[35] + 4*z[34];
    z[34]=z[9]*z[34];
    z[26]=z[30] + z[26] + z[58] + z[34];
    z[26]=z[6]*z[26];
    z[30]=z[6]*z[13];
    z[34]=npow(z[13],2);
    z[58]=n<T>(21,2)*z[34];
    z[59]=n<T>(7,2)*z[30] + z[58] - z[21];
    z[60]=z[11]*z[12];
    z[61]= - z[18]*z[60];
    z[62]=z[18]*z[10];
    z[63]=z[11]*z[18];
    z[63]=2*z[63] - z[62];
    z[63]=z[3]*z[63];
    z[61]=z[61] + z[63];
    z[61]=z[3]*z[61];
    z[63]=n<T>(1,4)*z[6];
    z[64]=z[34]*z[63];
    z[65]=z[34]*z[11];
    z[64]=z[65] + z[64];
    z[61]=n<T>(21,2)*z[64] + n<T>(10,3)*z[61];
    z[61]=z[3]*z[61];
    z[59]=n<T>(3,4)*z[59] + z[61];
    z[61]=z[8]*z[3];
    z[59]=z[59]*z[61];
    z[64]=n<T>(3,4)*z[13];
    z[66]= - z[11] + z[64];
    z[66]=z[66]*z[58];
    z[45]=z[45]*z[18];
    z[67]=z[35]*z[45];
    z[39]=4*z[67] + z[66] - z[39];
    z[39]=z[3]*z[39];
    z[66]=n<T>(7,2)*z[34];
    z[67]= - z[66] + z[21];
    z[39]=z[59] + n<T>(3,2)*z[67] + z[39];
    z[39]=z[8]*z[39];
    z[59]=n<T>(7,2)*z[56] - z[35];
    z[67]=n<T>(3,2)*z[10];
    z[59]=z[67]*z[59]*z[45];
    z[68]=4*z[41];
    z[59]=z[68] + z[59];
    z[59]=z[3]*z[59];
    z[69]=z[31] + z[64];
    z[69]=z[10]*z[69];
    z[70]=n<T>(3,4)*z[34];
    z[69]=z[70] + z[69];
    z[69]=z[10]*z[69];
    z[71]=z[34]*z[9];
    z[39]=z[39] + z[59] + n<T>(15,8)*z[71] - n<T>(63,8)*z[46] + z[69];
    z[39]=z[8]*z[39];
    z[59]=z[18]*z[17];
    z[19]= - z[59]*z[19];
    z[69]=z[18]*z[22];
    z[72]=3*z[10];
    z[73]=z[72] + n<T>(53,2)*z[6];
    z[73]=z[73]*z[3];
    z[74]=z[9]*z[12];
    z[75]=n<T>(47,2)*z[74] - z[73];
    z[75]=z[8]*z[18]*z[75];
    z[19]=n<T>(3,4)*z[75] + z[19] + n<T>(19,4)*z[69];
    z[19]=z[19]*z[29];
    z[69]=5*z[59];
    z[75]=z[27]*z[18];
    z[76]= - z[69] - z[75];
    z[76]=z[76]*z[44];
    z[75]=z[59] + n<T>(19,8)*z[75];
    z[77]= - n<T>(353,8)*z[9] - 53*z[6];
    z[77]=z[8]*z[18]*z[77];
    z[75]=53*z[75] + z[77];
    z[75]=z[8]*z[75];
    z[18]=z[18]*z[28];
    z[18]= - n<T>(371,4)*z[18] + z[75];
    z[18]=z[8]*z[18];
    z[18]=n<T>(21,8)*z[76] + z[18];
    z[18]=z[7]*z[18];
    z[59]=z[2]*z[44]*z[59];
    z[18]=z[18] + n<T>(105,4)*z[59] + z[19];
    z[18]=z[7]*z[18];
    z[19]= - z[20]*z[69];
    z[50]=z[27]*z[50];
    z[19]=z[19] + z[50];
    z[19]=z[19]*z[44];
    z[50]=z[62]*z[32];
    z[59]= - z[46]*z[50];
    z[19]=z[19] + z[59];
    z[24]=z[24]*z[50];
    z[18]=z[18] + n<T>(21,8)*z[19] - n<T>(10,3)*z[24];
    z[18]=z[7]*z[18];
    z[19]= - z[45] + z[9];
    z[19]=z[41]*z[19];
    z[24]=z[9]*z[11];
    z[25]=z[25]*z[24];
    z[19]= - n<T>(2,3)*z[25] + z[19];
    z[25]=npow(z[61],3)*z[62];
    z[19]=2*z[19] + n<T>(5,3)*z[25];
    z[18]=2*z[19] + z[18];
    z[18]=z[7]*z[18];
    z[19]=z[31] - z[64];
    z[19]=z[10]*z[19];
    z[19]= - z[70] + z[19];
    z[19]=z[10]*z[19];
    z[19]=z[19] - 4*z[36];
    z[19]=z[9]*z[19];
    z[25]=8*z[41];
    z[16]=z[16] + z[18] + z[39] - z[25] + z[19] + z[26];
    z[16]=z[1]*z[16];
    z[18]=z[13] - z[72];
    z[18]=z[18]*z[67];
    z[19]=npow(z[4],3);
    z[26]=z[19]*z[6];
    z[39]=z[19]*z[10];
    z[45]=n<T>(17,3)*z[39] + 8*z[26];
    z[45]=z[3]*z[45];
    z[50]=12*z[33];
    z[45]= - z[50] + z[45];
    z[45]=z[3]*z[45];
    z[59]=z[19]*z[3];
    z[62]=z[19]*z[12];
    z[64]= - n<T>(20,3)*z[59] + n<T>(10,3)*z[62];
    z[69]=3*z[21] + z[64];
    z[69]=z[3]*z[69];
    z[69]=z[69] + n<T>(21,8)*z[13] + 32*z[6];
    z[69]=z[8]*z[69];
    z[75]=n<T>(63,8)*z[34];
    z[76]=z[9]*z[13];
    z[77]=11*z[40];
    z[18]=z[69] + z[45] - n<T>(319,8)*z[27] + n<T>(15,4)*z[76] + z[18] + z[77]
    + z[75];
    z[18]=z[8]*z[18];
    z[45]=n<T>(21,8)*z[46];
    z[69]=4*z[33];
    z[78]=z[45] + z[69];
    z[79]=z[10]*z[13];
    z[80]=n<T>(7,4)*z[34] - z[79];
    z[81]=n<T>(1,2)*z[10];
    z[80]=z[80]*z[81];
    z[80]=z[80] + z[78];
    z[32]=z[19]*z[21]*z[32];
    z[82]=z[40] - z[58];
    z[83]=z[9]*z[5];
    z[83]=19*z[83];
    z[82]=n<T>(1,2)*z[82] + z[83];
    z[82]=z[9]*z[82];
    z[84]= - 3*z[17] + n<T>(107,4)*z[27];
    z[84]=z[6]*z[84];
    z[18]=z[18] + n<T>(3,2)*z[32] + z[84] + 3*z[80] + z[82];
    z[18]=z[8]*z[18];
    z[32]=n<T>(10,3)*z[59] - n<T>(5,3)*z[62] + 14*z[27];
    z[32]=z[8]*z[32];
    z[59]=z[77] - n<T>(399,4)*z[62];
    z[80]=n<T>(1,2)*z[9];
    z[59]=z[59]*z[80];
    z[82]=n<T>(395,3)*z[39] + n<T>(447,2)*z[26];
    z[84]=n<T>(1,4)*z[3];
    z[82]=z[82]*z[84];
    z[32]=2*z[32] + z[82] - 16*z[28] - z[50] + z[59];
    z[32]=z[8]*z[32];
    z[59]= - 91*z[21] - 115*z[44];
    z[59]=z[84]*z[19]*z[59];
    z[82]=n<T>(701,2)*z[62] + z[83];
    z[84]=n<T>(1,4)*z[9];
    z[82]=z[82]*z[84];
    z[49]=z[49] + z[82];
    z[49]=z[9]*z[49];
    z[82]=5*z[28];
    z[85]=npow(z[9],3);
    z[86]= - z[85] + z[82];
    z[86]=z[6]*z[86];
    z[32]=z[32] + z[59] + z[49] + n<T>(19,4)*z[86];
    z[32]=z[8]*z[32];
    z[49]= - z[69] + n<T>(965,8)*z[19];
    z[49]=z[9]*z[49];
    z[59]= - n<T>(141,4)*z[19] + 8*z[28];
    z[59]=z[8]*z[59];
    z[26]=z[59] + n<T>(483,4)*z[26] - n<T>(11,2)*z[39] + z[49];
    z[26]=z[8]*z[26];
    z[49]=z[17]*z[19];
    z[59]=19*z[85];
    z[86]= - 115*z[19] + z[59];
    z[86]=z[86]*z[63];
    z[87]=z[9]*z[19];
    z[86]= - 92*z[87] + z[86];
    z[86]=z[6]*z[86];
    z[26]=z[26] - n<T>(371,4)*z[49] + z[86];
    z[26]=z[8]*z[26];
    z[86]=32*z[15];
    z[87]=z[86]*z[19];
    z[88]=z[11]*z[17];
    z[88]= - n<T>(21,2)*z[19] - 19*z[88];
    z[88]=z[88]*z[80];
    z[88]=z[87] + z[88];
    z[88]=z[6]*z[88];
    z[88]=65*z[49] + z[88];
    z[88]=z[6]*z[88];
    z[26]=z[88] + z[26];
    z[26]=z[7]*z[26];
    z[49]=z[2]*z[49];
    z[88]=n<T>(21,4)*z[2];
    z[89]= - z[19]*z[88];
    z[24]=z[89] - 38*z[24];
    z[24]=z[24]*z[27];
    z[24]=n<T>(5,2)*z[49] + z[24];
    z[24]=z[6]*z[24];
    z[49]=z[46]*z[19];
    z[89]=z[34]*z[39];
    z[89]=21*z[49] + n<T>(275,2)*z[89];
    z[87]=z[44]*z[87];
    z[87]=n<T>(1,4)*z[89] + z[87];
    z[87]=z[3]*z[87];
    z[89]=n<T>(21,2)*z[46];
    z[90]= - z[89] - 5*z[71];
    z[90]=z[62]*z[90];
    z[24]=z[26] + z[32] + z[87] + n<T>(1,4)*z[90] + z[24];
    z[24]=z[7]*z[24];
    z[26]= - z[69] + n<T>(37,4)*z[65];
    z[32]=npow(z[12],2);
    z[87]=z[32]*z[34];
    z[90]=z[19]*z[87];
    z[91]=z[13]*z[72];
    z[91]= - n<T>(11,2)*z[56] + z[91];
    z[91]=z[91]*z[81];
    z[36]=n<T>(11,8)*z[36] + z[91] - n<T>(5,2)*z[90] + z[26];
    z[36]=z[9]*z[36];
    z[90]=z[20]*z[19];
    z[91]=n<T>(21,2)*z[90] + z[86] - n<T>(249,4)*z[11];
    z[91]=z[9]*z[91];
    z[92]=n<T>(37,8)*z[90];
    z[93]=z[9] + z[11];
    z[93]= - z[92] - n<T>(8,3)*z[93];
    z[93]=z[6]*z[93];
    z[91]=z[91] + z[93];
    z[91]=z[6]*z[91];
    z[93]= - z[31] - n<T>(29,2)*z[90];
    z[93]=z[93]*z[17];
    z[91]=z[93] + z[91];
    z[91]=z[6]*z[91];
    z[93]=21*z[56] - n<T>(319,2)*z[34];
    z[93]=z[93]*z[19];
    z[94]=8*z[13];
    z[95]= - z[39]*z[94];
    z[93]=n<T>(1,4)*z[93] + z[95];
    z[93]=z[10]*z[93];
    z[93]= - n<T>(21,4)*z[49] + z[93];
    z[93]=z[3]*z[93];
    z[89]=z[62]*z[89];
    z[89]=z[89] + z[93];
    z[89]=z[3]*z[89];
    z[93]=z[49]*z[32];
    z[93]= - z[93] + n<T>(1,2)*z[47];
    z[18]=z[24] + z[18] + z[89] + z[91] + z[36] + n<T>(21,4)*z[93] + z[25];
    z[18]=z[7]*z[18];
    z[24]=3*z[14];
    z[36]=npow(z[14],2);
    z[89]=z[36]*z[2];
    z[91]=n<T>(25,2)*z[13] - n<T>(189,4)*z[11] + z[24] + n<T>(113,2)*z[89];
    z[95]=8*z[14];
    z[96]= - z[95] + n<T>(3,2)*z[89];
    z[96]=z[2]*z[96];
    z[96]= - n<T>(19,4) + z[96];
    z[96]=z[10]*z[96];
    z[91]=n<T>(11,8)*z[9] + n<T>(1,2)*z[91] + z[96];
    z[91]=z[10]*z[91];
    z[96]=z[33]*z[2];
    z[97]=2*z[40] + z[96];
    z[98]=n<T>(1,2)*z[13];
    z[99]=z[12]*z[14];
    z[100]=static_cast<T>(29)- n<T>(37,2)*z[99];
    z[100]=z[100]*z[98];
    z[100]=z[100] + z[95] - n<T>(5,4)*z[11];
    z[100]=z[13]*z[100];
    z[101]=npow(z[12],3);
    z[102]=z[101]*z[34];
    z[103]=z[19]*z[102];
    z[91]= - n<T>(5,4)*z[103] + 2*z[97] + z[100] + z[91];
    z[91]=z[9]*z[91];
    z[97]=z[43]*z[19];
    z[100]=z[20]*z[10];
    z[103]= - n<T>(95,4)*z[2] + 8*z[100];
    z[103]=n<T>(1,3)*z[103] - z[55];
    z[103]=z[9]*z[103];
    z[103]=z[103] + n<T>(8,3)*z[52] - n<T>(127,12) + 4*z[97];
    z[103]=z[6]*z[103];
    z[37]= - z[97]*z[37];
    z[97]=static_cast<T>(7)- n<T>(173,2)*z[52];
    z[104]=n<T>(21,2)*z[54];
    z[105]=z[10]*z[104];
    z[97]=n<T>(1,3)*z[97] + z[105];
    z[97]=z[97]*z[80];
    z[105]=4*z[52];
    z[106]= - n<T>(55,3) - z[105];
    z[106]=z[10]*z[106];
    z[37]=z[103] + z[97] + z[106] + z[37] + z[86] - n<T>(229,6)*z[11] + n<T>(15,4)*z[13];
    z[37]=z[6]*z[37];
    z[97]=n<T>(807,2)*z[11] - n<T>(5,3)*z[10];
    z[97]=z[97]*z[81];
    z[103]=z[40]*z[2];
    z[106]= - 2*z[5] - z[103];
    z[107]= - static_cast<T>(1)+ z[52];
    z[107]=z[9]*z[107];
    z[106]=4*z[107] + n<T>(97,2)*z[10] + 2*z[106] - n<T>(321,8)*z[11];
    z[106]=z[9]*z[106];
    z[107]=z[11]*z[14];
    z[37]=z[37] + z[106] + 28*z[107] + z[97];
    z[37]=z[6]*z[37];
    z[97]=z[5]*z[90];
    z[97]= - 19*z[107] - n<T>(91,2)*z[97];
    z[106]=n<T>(3,2)*z[13];
    z[92]=n<T>(8,3)*z[10] + z[92] - n<T>(16,3)*z[11] - z[106];
    z[92]=z[6]*z[92];
    z[108]= - 8*z[10] - z[11] - n<T>(53,4)*z[13];
    z[108]=z[10]*z[108];
    z[92]=z[92] + n<T>(1,2)*z[97] + z[108];
    z[92]=z[6]*z[92];
    z[90]=z[40]*z[90];
    z[97]=8*z[56];
    z[108]= - z[97] + n<T>(55,3)*z[35];
    z[108]=z[10]*z[108];
    z[90]=z[92] + n<T>(51,2)*z[90] + z[108];
    z[90]=z[6]*z[90];
    z[92]=n<T>(11,4)*z[19];
    z[108]=z[2]*z[5];
    z[109]= - z[108]*z[92];
    z[110]=n<T>(8,3)*z[6];
    z[111]=z[35]*z[110];
    z[112]=12*z[38];
    z[109]=z[111] + z[109] - z[112];
    z[109]=z[6]*z[109];
    z[92]=z[103]*z[92];
    z[92]=z[109] + z[92] + z[51];
    z[92]=z[6]*z[92];
    z[109]=z[81]*z[19];
    z[111]= - n<T>(37,2)*z[56] + 3*z[35];
    z[111]=z[3]*z[111]*z[109];
    z[45]= - z[12]*z[45];
    z[45]= - 4*z[96] + z[45];
    z[45]=z[45]*z[19];
    z[45]=z[111] + z[45] + z[92];
    z[45]=z[3]*z[45];
    z[92]=n<T>(1,2)*z[36] - z[97];
    z[92]=z[92]*z[21];
    z[92]= - n<T>(7,4)*z[93] + z[92];
    z[45]=z[45] + 3*z[92] + z[90];
    z[45]=z[3]*z[45];
    z[90]=z[19]*z[60];
    z[92]=z[11]*z[19];
    z[39]=n<T>(37,4)*z[92] + n<T>(2,3)*z[39];
    z[39]=z[3]*z[39];
    z[26]=z[39] - z[112] - n<T>(53,4)*z[90] + z[26];
    z[26]=z[3]*z[26];
    z[39]=z[77] - z[58];
    z[90]=n<T>(21,2)*z[30];
    z[92]=6*z[11] - n<T>(67,4)*z[10];
    z[92]=z[10]*z[92];
    z[26]=z[26] + z[90] + n<T>(1,2)*z[39] + z[92];
    z[26]=z[3]*z[26];
    z[39]=z[13]*z[11];
    z[64]= - z[90] - n<T>(10,3)*z[35] - n<T>(63,4)*z[39] - z[64];
    z[64]=z[3]*z[64];
    z[90]=n<T>(21,2)*z[13];
    z[92]= - z[90] + z[10];
    z[92]=3*z[92] + n<T>(25,2)*z[6];
    z[64]=n<T>(3,4)*z[92] + z[64];
    z[64]=z[64]*z[61];
    z[92]=n<T>(9,4)*z[13];
    z[93]=z[92] + z[10];
    z[93]=7*z[93] - n<T>(191,4)*z[6];
    z[26]=z[64] + n<T>(1,2)*z[93] + z[26];
    z[26]=z[8]*z[26];
    z[64]=9*z[56];
    z[19]= - z[19]*z[64];
    z[93]=n<T>(1081,6)*z[11] - z[72];
    z[93]=z[93]*z[109];
    z[19]=z[19] + z[93];
    z[19]=z[3]*z[19];
    z[93]=5*z[56];
    z[62]=z[62]*z[93];
    z[47]=n<T>(7,2)*z[47] + z[62];
    z[47]=n<T>(1,4)*z[47] + z[68];
    z[19]=3*z[47] + z[19];
    z[19]=z[3]*z[19];
    z[47]=n<T>(7,2)*z[13];
    z[62]=13*z[11] + z[47];
    z[62]=z[62]*z[70];
    z[62]=2*z[33] + z[62];
    z[68]=n<T>(5,4)*z[56] + n<T>(74,3)*z[35];
    z[68]=z[10]*z[68];
    z[19]=z[19] + 3*z[62] + z[68];
    z[19]=z[3]*z[19];
    z[62]=9*z[11];
    z[47]=z[62] - z[47];
    z[47]=z[13]*z[47];
    z[68]=n<T>(845,3)*z[11] - n<T>(113,2)*z[13];
    z[68]=n<T>(1,2)*z[68] + 35*z[10];
    z[68]=z[10]*z[68];
    z[47]=z[68] + z[40] + n<T>(15,2)*z[47];
    z[68]= - n<T>(323,2)*z[9] + 31*z[6];
    z[68]=z[68]*z[63];
    z[97]=16*z[5] - n<T>(13,8)*z[13];
    z[97]=z[9]*z[97];
    z[19]=z[26] + z[19] + z[68] + n<T>(1,2)*z[47] + z[97];
    z[19]=z[8]*z[19];
    z[26]= - 113*z[36] - z[93];
    z[47]= - n<T>(23,6)*z[11] + z[95] - 3*z[89];
    z[47]=z[10]*z[47];
    z[26]=n<T>(1,4)*z[26] + z[47];
    z[26]=z[10]*z[26];
    z[47]= - 11*z[11] - z[106];
    z[47]=z[47]*z[66];
    z[49]=z[101]*z[49];
    z[16]=z[16] + z[18] + z[19] + z[45] + z[37] + z[91] + z[26] - n<T>(21,8)
   *z[49] - z[69] + z[47];
    z[16]=z[1]*z[16];
    z[18]= - z[13] + z[67];
    z[18]=z[18]*z[72];
    z[19]=n<T>(1,2)*z[8];
    z[26]= - 9*z[22] - n<T>(269,4)*z[9] - 255*z[6];
    z[26]=z[26]*z[19];
    z[37]=npow(z[4],2);
    z[45]=n<T>(49,4)*z[9] - 14*z[5] - z[92];
    z[45]=z[9]*z[45];
    z[18]=z[26] + n<T>(749,4)*z[27] + z[45] + z[18] + 16*z[40] + n<T>(365,6)*
    z[37];
    z[18]=z[8]*z[18];
    z[26]=n<T>(107,2)*z[27];
    z[45]= - z[26] - 173*z[37] - 81*z[17];
    z[45]=z[6]*z[45];
    z[47]= - z[70] + 13*z[37];
    z[49]=z[13]*z[67];
    z[47]=13*z[47] + z[49];
    z[47]=z[47]*z[81];
    z[49]=n<T>(13,2)*z[34];
    z[66]= - 17*z[40] + z[49];
    z[66]=3*z[66] - n<T>(623,2)*z[37];
    z[66]=n<T>(1,2)*z[66] - z[83];
    z[66]=z[66]*z[80];
    z[18]=z[18] + z[45] + z[66] - z[50] + z[47];
    z[18]=z[8]*z[18];
    z[45]= - z[39]*z[59];
    z[47]= - z[59] + n<T>(21,4)*z[28];
    z[47]=z[6]*z[47];
    z[66]= - z[11]*z[59];
    z[47]=z[66] + z[47];
    z[47]=z[6]*z[47];
    z[45]=z[45] + z[47];
    z[47]= - 8*z[17] - 105*z[27];
    z[47]=z[8]*z[47];
    z[66]=8*z[40];
    z[68]=z[9]*z[66];
    z[47]=z[47] + z[68] + n<T>(149,2)*z[28];
    z[47]=z[8]*z[47];
    z[68]=z[17]*z[44];
    z[47]= - n<T>(95,2)*z[68] + z[47];
    z[47]=z[8]*z[47];
    z[68]=z[29]*z[28];
    z[59]= - z[44]*z[59];
    z[59]=z[59] - n<T>(117,2)*z[68];
    z[59]=z[7]*z[59]*z[19];
    z[45]=z[59] + n<T>(1,2)*z[45] + z[47];
    z[45]=z[7]*z[45];
    z[47]=n<T>(53,2)*z[5];
    z[59]= - z[13]*z[47];
    z[59]= - 15*z[37] + z[77] + z[59];
    z[59]=z[13]*z[59];
    z[69]=3*z[34] - z[79];
    z[69]=z[69]*z[72];
    z[59]=z[69] + z[59];
    z[69]= - n<T>(19,2)*z[76] - 27*z[39] + n<T>(151,2)*z[37];
    z[69]=z[9]*z[69];
    z[59]=n<T>(1,4)*z[59] + z[69];
    z[59]=z[9]*z[59];
    z[69]=64*z[15];
    z[70]= - z[69] - n<T>(57,4)*z[9];
    z[70]=z[9]*z[70];
    z[70]=n<T>(407,24)*z[27] - n<T>(33,2)*z[37] + z[70];
    z[70]=z[6]*z[70];
    z[76]=96*z[15];
    z[83]=z[37]*z[76];
    z[91]=3*z[9];
    z[62]= - z[62] - n<T>(31,4)*z[9];
    z[62]=z[62]*z[91];
    z[62]=n<T>(197,4)*z[37] + z[62];
    z[62]=z[9]*z[62];
    z[62]=z[70] + z[83] + z[62];
    z[62]=z[6]*z[62];
    z[70]=z[34]*z[37];
    z[83]=27*z[13];
    z[92]= - z[37]*z[83];
    z[46]=z[46] + z[92];
    z[46]=z[10]*z[46];
    z[46]= - 265*z[70] + 21*z[46];
    z[18]=z[45] + z[18] + z[62] + n<T>(1,8)*z[46] + z[59];
    z[18]=z[7]*z[18];
    z[45]= - n<T>(325,2)*z[12] - 37*z[2];
    z[45]=z[45]*z[37];
    z[46]=n<T>(19,2)*z[9];
    z[59]=z[13]*z[12];
    z[62]= - static_cast<T>(1)+ z[59];
    z[46]=z[62]*z[46];
    z[45]=z[46] + n<T>(1,2)*z[45] + n<T>(95,4)*z[5] - z[83];
    z[45]=z[9]*z[45];
    z[46]=3*z[13];
    z[62]= - 16*z[11] - n<T>(7,4)*z[13];
    z[62]=z[62]*z[46];
    z[70]=z[37]*z[59];
    z[83]=n<T>(1,4)*z[10];
    z[92]=19*z[10];
    z[93]=z[92] - n<T>(111,2)*z[11] - 47*z[13];
    z[93]=z[93]*z[83];
    z[45]=z[45] + z[93] - n<T>(13,2)*z[70] + z[62] + n<T>(9,2)*z[40] - z[57];
    z[45]=z[9]*z[45];
    z[62]=n<T>(105,4)*z[21] + z[66] - n<T>(55,12)*z[37];
    z[62]=z[3]*z[62];
    z[66]=z[37]*z[12];
    z[70]= - static_cast<T>(9)- n<T>(1,2)*z[73];
    z[70]=z[8]*z[70];
    z[62]=n<T>(9,2)*z[70] + z[62] + n<T>(621,4)*z[6] + n<T>(287,8)*z[9] - n<T>(25,2)*
    z[10] - 17*z[5] + n<T>(83,12)*z[66];
    z[62]=z[8]*z[62];
    z[70]=31*z[40] + z[58];
    z[93]=17*z[9];
    z[97]=11*z[66] + 109*z[5] - n<T>(31,2)*z[13];
    z[97]=n<T>(1,2)*z[97] - z[93];
    z[97]=z[97]*z[80];
    z[109]=n<T>(11,2)*z[9] - 177*z[6];
    z[109]=z[109]*z[63];
    z[111]= - n<T>(733,8)*z[6] - n<T>(1589,12)*z[10];
    z[111]=z[37]*z[111];
    z[111]= - 24*z[33] + z[111];
    z[111]=z[3]*z[111];
    z[113]=n<T>(199,8)*z[13] - 23*z[10];
    z[113]=z[10]*z[113];
    z[62]=z[62] + z[111] + z[109] + z[97] + n<T>(1,2)*z[70] + z[113];
    z[62]=z[8]*z[62];
    z[70]= - n<T>(95,3)*z[2] - 21*z[54];
    z[70]=z[70]*z[80];
    z[70]=n<T>(77,3) + z[70];
    z[97]=n<T>(1,2)*z[6];
    z[70]=z[70]*z[97];
    z[109]=z[37]*z[2];
    z[70]=z[70] + n<T>(283,6)*z[9] + n<T>(29,2)*z[109] + z[106] - z[86] + n<T>(3,4)*
    z[11];
    z[70]=z[6]*z[70];
    z[111]= - n<T>(89,4)*z[9] - n<T>(289,4)*z[109] - z[76] - n<T>(577,8)*z[11];
    z[111]=z[9]*z[111];
    z[70]=z[111] + z[70];
    z[70]=z[6]*z[70];
    z[111]= - z[40] + z[56];
    z[113]=29*z[11] + 283*z[13];
    z[113]=n<T>(1,8)*z[113] + 22*z[10];
    z[113]=z[10]*z[113];
    z[114]=z[86] - n<T>(53,4)*z[6];
    z[114]=z[6]*z[114];
    z[111]=z[114] + z[113] + 4*z[111] + n<T>(1059,8)*z[34];
    z[111]=z[3]*z[37]*z[111];
    z[113]=n<T>(53,8)*z[5];
    z[48]=z[48] + z[113] - 24*z[11];
    z[48]=z[13]*z[48];
    z[114]=n<T>(11,4)*z[40];
    z[48]= - z[114] + z[48];
    z[48]=z[13]*z[48];
    z[115]=z[34]*z[66];
    z[116]= - z[56] - n<T>(63,2)*z[34];
    z[116]=z[116]*z[83];
    z[117]=18*z[33];
    z[18]=z[18] + z[62] + z[111] + z[70] + z[45] + z[116] - 90*z[115] + 
    z[117] + z[48];
    z[18]=z[7]*z[18];
    z[45]=z[20]*z[37];
    z[48]=4*z[5];
    z[62]= - z[48] - z[103];
    z[70]=2*z[2];
    z[62]=z[62]*z[70];
    z[111]= - z[20]*z[53];
    z[88]= - z[88] + z[111];
    z[88]=z[10]*z[88];
    z[43]=z[53]*z[43];
    z[111]= - n<T>(29,3)*z[20] - z[43];
    z[115]=2*z[10];
    z[111]=z[111]*z[115];
    z[104]= - z[104] + n<T>(13,2)*z[2] + z[111];
    z[104]=z[9]*z[104];
    z[111]= - n<T>(4,3) - z[108];
    z[111]=z[2]*z[111];
    z[111]=z[111] - n<T>(4,3)*z[54];
    z[116]=2*z[6];
    z[111]=z[111]*z[116];
    z[62]=z[111] + z[104] + z[88] + n<T>(17,8)*z[45] + n<T>(53,2) + z[62];
    z[62]=z[6]*z[62];
    z[88]= - z[5] - z[103];
    z[88]=z[88]*z[70];
    z[104]= - n<T>(367,3)*z[2] - 83*z[100];
    z[104]=z[104]*z[83];
    z[111]=4*z[2];
    z[118]= - z[111] + n<T>(37,4)*z[100];
    z[118]=z[9]*z[118];
    z[88]=z[118] + z[104] + n<T>(71,2)*z[45] - n<T>(207,4) + z[88];
    z[88]=z[9]*z[88];
    z[104]=n<T>(3,8)*z[45] - n<T>(79,4);
    z[104]=z[5]*z[104];
    z[118]=3*z[96];
    z[119]= - 7*z[40] - z[118];
    z[119]=z[119]*z[70];
    z[45]=n<T>(221,3) + 4*z[45];
    z[45]=2*z[45] - n<T>(53,6)*z[52];
    z[45]=z[10]*z[45];
    z[45]=z[62] + z[88] + z[45] - z[94] - n<T>(789,8)*z[11] + z[119] - z[86]
    + n<T>(361,4)*z[14] + z[104];
    z[45]=z[6]*z[45];
    z[62]=n<T>(215,3)*z[35] + n<T>(31,2)*z[56] + 171*z[37];
    z[62]=z[62]*z[81];
    z[33]= - 3*z[33] + z[65];
    z[65]=z[11]*z[37];
    z[75]=z[6]*z[75];
    z[51]=z[3]*z[51];
    z[33]=z[51] + z[75] + z[62] + 4*z[33] - n<T>(517,4)*z[65];
    z[33]=z[3]*z[33];
    z[51]=n<T>(1,4)*z[13];
    z[62]= - 141*z[11] + n<T>(211,2)*z[13];
    z[62]=z[62]*z[51];
    z[65]=z[37]*z[60];
    z[75]=z[90] - 115*z[6];
    z[75]=z[75]*z[63];
    z[88]=1265*z[11] - n<T>(113,2)*z[10];
    z[88]=z[10]*z[88];
    z[33]=z[33] + z[75] + n<T>(1,6)*z[88] + n<T>(227,4)*z[65] + z[62] + n<T>(113,4)*
    z[40] - z[64];
    z[33]=z[3]*z[33];
    z[62]= - 95*z[39] + n<T>(191,3)*z[37];
    z[65]=z[31] - z[72];
    z[65]=z[65]*z[53];
    z[30]=n<T>(21,4)*z[30];
    z[72]= - z[3]*z[112];
    z[62]=z[72] - z[30] + n<T>(1,4)*z[62] + z[65];
    z[62]=z[3]*z[62];
    z[62]=z[62] + n<T>(277,8)*z[6] + n<T>(539,12)*z[10] - n<T>(199,12)*z[66] - n<T>(211,8)*z[13] - 3*z[5] + n<T>(115,8)*z[11];
    z[62]=z[3]*z[62];
    z[65]=10*z[3];
    z[66]=z[65]*z[35];
    z[66]=z[66] + 10*z[10];
    z[72]=n<T>(103,6)*z[11] - z[66];
    z[72]=z[3]*z[72];
    z[72]=n<T>(103,6) + z[72];
    z[72]=z[72]*z[61];
    z[62]=z[72] + n<T>(631,24) + z[62];
    z[62]=z[8]*z[62];
    z[72]=n<T>(665,2)*z[13] + 77*z[5] - n<T>(1121,2)*z[11];
    z[72]=n<T>(1,2)*z[72] + 431*z[10];
    z[33]=z[62] + z[33] + n<T>(355,8)*z[6] + n<T>(1,2)*z[72] + 36*z[9];
    z[33]=z[8]*z[33];
    z[62]=n<T>(231,8)*z[12] - z[111];
    z[62]=z[62]*z[109];
    z[72]=z[2]*z[14];
    z[75]=n<T>(43,2) + 21*z[72];
    z[75]=z[75]*z[52];
    z[88]= - static_cast<T>(61)+ 33*z[59];
    z[90]=z[32]*z[13];
    z[94]=z[90] - z[12];
    z[104]= - z[2] - z[94];
    z[104]=z[9]*z[104];
    z[62]=n<T>(17,4)*z[104] + n<T>(1,4)*z[75] + n<T>(1,2)*z[88] + z[62];
    z[62]=z[9]*z[62];
    z[75]= - n<T>(113,2)*z[36] - 6*z[40];
    z[75]=z[2]*z[75];
    z[88]=n<T>(1,2)*z[59];
    z[104]=37*z[99];
    z[111]= - static_cast<T>(21)+ z[104];
    z[111]=z[111]*z[88];
    z[111]=z[111] - n<T>(363,8) - 8*z[99];
    z[111]=z[13]*z[111];
    z[112]=z[37]*z[90];
    z[119]= - n<T>(35,2)*z[14] + 29*z[89];
    z[119]=z[2]*z[119];
    z[119]= - n<T>(27,2)*z[52] + n<T>(287,8) + z[119];
    z[119]=z[10]*z[119];
    z[120]= - 53*z[14] - n<T>(79,2)*z[5];
    z[62]=z[62] + z[119] - n<T>(11,4)*z[112] + z[111] - n<T>(247,8)*z[11] + n<T>(1,2)
   *z[120] + z[75];
    z[62]=z[9]*z[62];
    z[75]=16*z[15] + z[11];
    z[111]= - n<T>(5,4) - z[105];
    z[111]=z[10]*z[111];
    z[75]=z[111] + 2*z[75] + n<T>(27,4)*z[109];
    z[75]=z[6]*z[75];
    z[111]=n<T>(11,2)*z[5];
    z[95]=z[95] - z[111];
    z[95]=z[13]*z[95];
    z[112]=z[5]*z[109];
    z[119]=n<T>(19,6)*z[10] + n<T>(1595,6)*z[11] - 4*z[109];
    z[119]=z[10]*z[119];
    z[75]=z[75] + z[119] + 46*z[112] + z[95] + 32*z[36] + n<T>(471,4)*z[107]
   ;
    z[75]=z[6]*z[75];
    z[95]=n<T>(11,4)*z[11] - z[53];
    z[95]=z[6]*z[95];
    z[57]=z[95] - z[57] + n<T>(91,6)*z[35];
    z[57]=z[10]*z[57];
    z[47]= - 37*z[14] - z[47];
    z[47]=z[13]*z[47];
    z[47]=z[77] + z[47];
    z[47]=z[13]*z[47];
    z[95]=75*z[5];
    z[112]=z[37]*z[95];
    z[119]=z[11]*z[36];
    z[47]=z[112] + 107*z[119] + z[47];
    z[47]=n<T>(1,4)*z[47] + z[57];
    z[47]=z[6]*z[47];
    z[57]=z[64] - n<T>(319,8)*z[34];
    z[57]=z[57]*z[37];
    z[64]= - 501*z[11] - 79*z[13];
    z[64]=z[64]*z[37];
    z[112]=z[10]*z[56];
    z[64]=n<T>(1,8)*z[64] - 24*z[112];
    z[64]=z[10]*z[64];
    z[112]=z[38]*z[6];
    z[119]=z[41] - z[112];
    z[120]=4*z[6];
    z[119]=z[3]*z[119]*z[120];
    z[47]=z[119] + z[47] + z[57] + z[64];
    z[47]=z[3]*z[47];
    z[57]=21*z[36] - 47*z[56];
    z[64]= - 8*z[109] - z[24] - n<T>(109,6)*z[11];
    z[64]=z[10]*z[64];
    z[57]=n<T>(1,4)*z[57] + z[64];
    z[57]=z[10]*z[57];
    z[31]= - z[113] - z[31];
    z[31]=z[13]*z[31];
    z[31]=z[114] + z[31];
    z[31]=z[13]*z[31];
    z[64]=z[12]*z[56];
    z[64]=7*z[103] + z[64];
    z[109]=z[34]*z[12];
    z[64]=2*z[64] + 49*z[109];
    z[64]=z[64]*z[37];
    z[31]=z[47] + z[75] + z[57] + 2*z[64] + z[117] + z[31];
    z[31]=z[3]*z[31];
    z[47]= - 7*z[59] - n<T>(403,2) - z[104];
    z[46]=z[47]*z[46];
    z[47]=11*z[5];
    z[46]=z[46] + 109*z[11] - 47*z[14] - z[47];
    z[46]=z[46]*z[51];
    z[37]=z[37]*z[87];
    z[57]= - n<T>(589,4)*z[13] - n<T>(1181,4)*z[11] + 61*z[14] - n<T>(137,2)*z[89];
    z[64]=n<T>(331,12) + 3*z[72];
    z[64]=z[10]*z[64];
    z[57]=n<T>(1,2)*z[57] + z[64];
    z[57]=z[10]*z[57];
    z[64]=12*z[96];
    z[75]=z[69] - n<T>(13,2)*z[5];
    z[75]=z[5]*z[75];
    z[113]=64*z[5];
    z[114]= - z[113] + z[11];
    z[114]=z[11]*z[114];
    z[16]=z[16] + z[18] + z[33] + z[31] + z[45] + z[62] + z[57] - n<T>(455,8)
   *z[37] + z[46] + z[114] + z[75] - z[64];
    z[16]=z[1]*z[16];
    z[18]=111*z[14];
    z[31]= - z[18] - z[47];
    z[31]=z[13]*z[31];
    z[31]=z[31] - z[77] + 239*z[107];
    z[33]=n<T>(1397,12)*z[11] + z[53];
    z[33]=z[10]*z[33];
    z[31]=n<T>(1,4)*z[31] + z[33];
    z[31]=z[6]*z[31];
    z[33]= - n<T>(79,2)*z[56] - n<T>(53,3)*z[35];
    z[33]=z[33]*z[81];
    z[37]= - 2*z[41] + z[112];
    z[37]=z[37]*z[42];
    z[45]=n<T>(107,4)*z[36];
    z[46]= - z[11]*z[45];
    z[53]=z[14]*z[34];
    z[31]=z[37] + z[31] + z[33] + n<T>(37,4)*z[53] - z[50] + z[46];
    z[31]=z[3]*z[31];
    z[33]=5*z[40] + z[118];
    z[33]=z[33]*z[70];
    z[33]= - n<T>(23,8)*z[5] + z[33];
    z[33]=z[2]*z[33];
    z[37]=z[120]*z[20];
    z[46]= - z[5] + z[103];
    z[46]=z[46]*z[37];
    z[33]=z[46] + n<T>(53,4) + z[33];
    z[33]=z[6]*z[33];
    z[46]= - n<T>(13,2)*z[40] + z[64];
    z[46]=z[2]*z[46];
    z[50]=n<T>(1445,12) + z[105];
    z[50]=z[10]*z[50];
    z[33]=z[33] + z[50] - 29*z[13] - 82*z[11] + z[46] + n<T>(39,4)*z[5] - 
    z[86] + n<T>(107,4)*z[14];
    z[33]=z[6]*z[33];
    z[46]= - n<T>(21,2)*z[59] - n<T>(361,2) - z[104];
    z[46]=z[46]*z[106];
    z[18]=z[46] - n<T>(341,4)*z[11] - z[18] - z[111];
    z[18]=z[18]*z[98];
    z[36]=z[36]*z[12];
    z[46]= - 13*z[14] + 113*z[36];
    z[46]=10*z[11] + n<T>(1,4)*z[46] - z[113];
    z[46]=z[11]*z[46];
    z[50]= - n<T>(605,2)*z[13] - 21*z[14] - n<T>(1493,2)*z[11];
    z[50]=n<T>(1,2)*z[50] - n<T>(109,3)*z[10];
    z[50]=z[50]*z[81];
    z[53]=z[69] + n<T>(165,4)*z[5];
    z[53]=z[5]*z[53];
    z[18]=z[31] + z[33] + z[50] + z[18] + z[46] + 18*z[96] - z[45] + 
    z[53];
    z[18]=z[3]*z[18];
    z[31]= - static_cast<T>(275)+ n<T>(141,2)*z[74];
    z[31]=z[31]*z[80];
    z[33]=static_cast<T>(1)- n<T>(1,4)*z[74];
    z[33]=47*z[33] + n<T>(3,2)*z[73];
    z[33]=z[8]*z[33];
    z[22]=n<T>(3,2)*z[33] - n<T>(73,4)*z[22] - n<T>(1985,8)*z[6] + z[31] - z[48] + 
   n<T>(29,2)*z[10];
    z[22]=z[8]*z[22];
    z[31]= - n<T>(43,2)*z[13] + z[92];
    z[31]=z[31]*z[81];
    z[33]=295*z[9] - z[95] + 43*z[13];
    z[33]=z[33]*z[84];
    z[46]=4*z[40];
    z[50]=131*z[9] + n<T>(261,4)*z[6];
    z[50]=z[6]*z[50];
    z[22]=z[22] + z[50] + z[33] + z[46] + z[31];
    z[22]=z[8]*z[22];
    z[31]=z[13] - z[10];
    z[31]=z[31]*z[67];
    z[33]= - 60*z[9] - z[48] - z[106];
    z[33]=z[9]*z[33];
    z[50]=static_cast<T>(311)- 47*z[74];
    z[50]=z[50]*z[80];
    z[50]=z[50] + 297*z[6];
    z[23]=n<T>(1,2)*z[50] + z[23];
    z[23]=z[8]*z[23];
    z[23]=z[23] - n<T>(2207,8)*z[27] + z[31] + z[33];
    z[23]=z[8]*z[23];
    z[31]=z[85]*z[44];
    z[31]=n<T>(19,2)*z[31] + 69*z[68];
    z[31]=z[7]*z[31];
    z[33]=z[85] + z[82];
    z[33]=z[6]*z[33];
    z[50]=85*z[17] + n<T>(987,2)*z[27];
    z[19]=z[50]*z[19];
    z[19]= - 223*z[28] + z[19];
    z[19]=z[8]*z[19];
    z[19]=z[31] + n<T>(19,2)*z[33] + z[19];
    z[19]=z[7]*z[19];
    z[28]=z[10]*z[34];
    z[28]=z[28] - z[71];
    z[26]=343*z[17] + z[26];
    z[26]=z[6]*z[26];
    z[26]=3*z[28] + z[26];
    z[19]=n<T>(1,2)*z[19] + n<T>(1,2)*z[26] + z[23];
    z[19]=z[8]*z[19];
    z[23]=z[9]*z[2];
    z[26]= - n<T>(51,2) - 19*z[23];
    z[26]=z[26]*z[84];
    z[26]=z[86] + z[26];
    z[26]=z[9]*z[26];
    z[28]= - n<T>(11,3) + 3*z[23];
    z[28]=z[28]*z[27];
    z[26]=z[26] + n<T>(7,4)*z[28];
    z[26]=z[6]*z[26];
    z[26]=n<T>(17,4)*z[85] + z[26];
    z[26]=z[6]*z[26];
    z[28]= - z[67]*z[71];
    z[19]=z[28] + z[26] + z[19];
    z[19]=z[7]*z[19];
    z[26]= - 181*z[5] + 53*z[13];
    z[26]=z[26]*z[51];
    z[17]=n<T>(17,2)*z[17] + z[26] + 11*z[79];
    z[17]=z[9]*z[17];
    z[26]=z[10]*z[58];
    z[17]=z[26] + z[17];
    z[26]= - n<T>(250,3) + n<T>(63,8)*z[23];
    z[26]=z[9]*z[26];
    z[28]=n<T>(43,3)*z[2] - z[55];
    z[28]=z[9]*z[28];
    z[28]= - n<T>(77,12) + z[28];
    z[28]=z[6]*z[28];
    z[26]=z[28] - z[86] + z[26];
    z[26]=z[6]*z[26];
    z[28]= - static_cast<T>(187)- 37*z[23];
    z[28]=z[28]*z[80];
    z[28]=z[76] + z[28];
    z[28]=z[9]*z[28];
    z[26]=z[28] + z[26];
    z[26]=z[6]*z[26];
    z[17]=z[19] + z[22] + n<T>(1,2)*z[17] + z[26];
    z[17]=z[7]*z[17];
    z[19]= - static_cast<T>(327)+ 11*z[23];
    z[19]=z[19]*z[80];
    z[22]=z[2] - z[54];
    z[22]=z[22]*z[110];
    z[26]=n<T>(1279,6)*z[2] - 63*z[54];
    z[26]=z[9]*z[26];
    z[26]= - static_cast<T>(75)+ z[26];
    z[22]=n<T>(1,4)*z[26] + z[22];
    z[22]=z[6]*z[22];
    z[19]=z[22] + z[19] + z[106] - z[69] - n<T>(93,2)*z[11];
    z[19]=z[6]*z[19];
    z[22]=z[56] - z[49];
    z[22]=z[10]*z[22];
    z[26]= - z[44]*z[69];
    z[25]= - z[3]*z[25];
    z[22]=z[25] + z[26] - 3*z[78] + n<T>(31,4)*z[22];
    z[22]=z[3]*z[22];
    z[25]=n<T>(115,2)*z[44] + z[46] + n<T>(269,6)*z[21];
    z[25]=z[3]*z[25];
    z[21]=z[21]*z[42];
    z[26]= - n<T>(431,3)*z[10] - n<T>(351,2)*z[6];
    z[21]=n<T>(1,2)*z[26] + z[21];
    z[21]=z[3]*z[21];
    z[26]=n<T>(143,24)*z[12];
    z[28]=z[10]*z[65];
    z[28]= - n<T>(383,24) + z[28];
    z[28]=z[3]*z[28];
    z[28]=z[26] + z[28];
    z[28]=z[8]*z[28];
    z[31]= - static_cast<T>(631)+ 329*z[74];
    z[21]=z[28] + n<T>(1,8)*z[31] + z[21];
    z[21]=z[8]*z[21];
    z[28]= - static_cast<T>(4)- n<T>(419,8)*z[74];
    z[28]=z[9]*z[28];
    z[21]=z[21] + z[25] + n<T>(817,8)*z[6] + z[28] - n<T>(2755,12)*z[10] - 35*
    z[5] - n<T>(251,4)*z[13];
    z[21]=z[8]*z[21];
    z[25]= - static_cast<T>(43)+ 21*z[59];
    z[28]= - 55*z[2] - 21*z[94];
    z[28]=z[9]*z[28];
    z[25]=3*z[25] + z[28];
    z[25]=z[25]*z[84];
    z[25]=z[25] - n<T>(121,2)*z[10] + n<T>(63,4)*z[109] + z[69] + n<T>(275,4)*z[5];
    z[25]=z[9]*z[25];
    z[28]=z[47] - n<T>(27,2)*z[11];
    z[31]=static_cast<T>(31)+ n<T>(9,4)*z[59];
    z[31]=z[13]*z[31];
    z[28]=n<T>(1,2)*z[28] + 7*z[31];
    z[28]=z[28]*z[98];
    z[31]= - 71*z[11] + 1091*z[13];
    z[31]=n<T>(1,2)*z[31] - 145*z[10];
    z[31]=z[31]*z[83];
    z[17]=z[17] + z[21] + z[22] + z[19] + z[25] + z[31] + 14*z[40] + 
    z[28];
    z[17]=z[7]*z[17];
    z[19]=z[2]*z[12];
    z[21]=z[13]*z[101];
    z[19]=z[21] - z[32] + z[19];
    z[19]=z[19]*z[93];
    z[21]= - static_cast<T>(27)- 7*z[72];
    z[21]=z[2]*z[21];
    z[21]=n<T>(121,2)*z[12] + z[21];
    z[19]=z[19] + 3*z[21] - 83*z[90];
    z[19]=z[19]*z[84];
    z[21]= - z[86] - z[24];
    z[21]=z[12]*z[21];
    z[22]=n<T>(113,2)*z[36];
    z[24]= - 61*z[89] - n<T>(67,4)*z[5] + 7*z[14] + z[22];
    z[24]=z[2]*z[24];
    z[25]= - n<T>(1,2) - z[104];
    z[25]=z[25]*z[90];
    z[25]=n<T>(379,2)*z[12] + z[25];
    z[25]=z[25]*z[51];
    z[28]=22*z[72];
    z[31]=n<T>(343,4) + z[28];
    z[31]=z[31]*z[52];
    z[33]=4*z[60];
    z[19]=z[19] + z[31] + z[25] + z[33] + n<T>(1,2)*z[24] - n<T>(37,8) + z[21];
    z[19]=z[9]*z[19];
    z[21]= - 7*z[5] - z[103];
    z[21]=z[21]*z[70];
    z[21]= - n<T>(15,8) + z[21];
    z[21]=z[2]*z[21];
    z[24]=n<T>(25,3)*z[20] + z[43];
    z[24]=z[9]*z[24];
    z[25]=static_cast<T>(1)- z[108];
    z[25]=z[25]*z[37];
    z[21]=z[25] + z[21] + 2*z[24];
    z[21]=z[6]*z[21];
    z[24]= - 79*z[54] + n<T>(993,2)*z[2] + 329*z[100];
    z[24]=z[24]*z[84];
    z[25]= - 6*z[103] + 10*z[14] + n<T>(101,4)*z[5];
    z[25]=z[2]*z[25];
    z[31]=n<T>(493,12)*z[2] + 16*z[100];
    z[31]=z[10]*z[31];
    z[21]=z[21] + z[24] + z[31] - n<T>(727,6) + z[25];
    z[21]=z[6]*z[21];
    z[24]=n<T>(47,4)*z[56] + n<T>(41,3)*z[35];
    z[24]=z[10]*z[24];
    z[25]=z[41]*z[42];
    z[24]=z[24] + z[25];
    z[24]=z[3]*z[24];
    z[25]= - 6*z[56] - n<T>(37,8)*z[39];
    z[31]=n<T>(453,2)*z[11] + n<T>(17,3)*z[10];
    z[31]=z[10]*z[31];
    z[24]=z[24] - z[30] + 3*z[25] + z[31];
    z[24]=z[3]*z[24];
    z[25]= - n<T>(1877,2) + 15*z[60];
    z[25]=z[11]*z[25];
    z[25]=n<T>(563,2)*z[6] + n<T>(3319,3)*z[10] - 211*z[13] - n<T>(51,2)*z[5] + 
    z[25];
    z[24]=n<T>(1,4)*z[25] + z[24];
    z[24]=z[3]*z[24];
    z[25]=2*z[3];
    z[30]= - z[38]*z[25];
    z[31]=7*z[11] - z[115];
    z[31]=z[10]*z[31];
    z[30]=z[31] + z[30];
    z[25]=z[30]*z[25];
    z[30]=n<T>(21,2)*z[11] + z[10];
    z[25]=z[25] + n<T>(5,2)*z[30] - z[120];
    z[25]=z[3]*z[25];
    z[30]=n<T>(761,3) - 89*z[60];
    z[25]=n<T>(1,8)*z[30] + z[25];
    z[25]=z[3]*z[25];
    z[30]=n<T>(383,24)*z[11] - z[66];
    z[30]=z[3]*z[30];
    z[31]=static_cast<T>(383)- 143*z[60];
    z[30]=n<T>(1,24)*z[31] + z[30];
    z[30]=z[3]*z[30];
    z[26]= - z[26] + z[30];
    z[26]=z[26]*z[61];
    z[25]=z[26] - n<T>(205,12)*z[12] + z[25];
    z[25]=z[8]*z[25];
    z[26]= - z[74] - n<T>(2453,2) + 267*z[60];
    z[24]=z[25] + n<T>(1,4)*z[26] + z[24];
    z[24]=z[8]*z[24];
    z[25]= - z[45] - 21*z[40];
    z[25]=z[2]*z[25];
    z[26]= - z[33] + n<T>(51,8) + 16*z[99];
    z[26]=z[11]*z[26];
    z[30]=n<T>(1421,2) + 111*z[99];
    z[30]=z[12]*z[30];
    z[30]=z[30] + n<T>(63,2)*z[90];
    z[30]=z[13]*z[30];
    z[31]=n<T>(245,2) + z[104];
    z[30]=3*z[31] + z[30];
    z[30]=z[30]*z[51];
    z[28]=n<T>(171,4)*z[52] + n<T>(149,12) - z[28];
    z[28]=z[10]*z[28];
    z[22]= - 27*z[14] + z[22];
    z[16]=z[16] + z[17] + z[24] + z[18] + z[21] + z[19] + z[28] + z[30]
    + z[26] + z[25] + n<T>(1,2)*z[22] + 26*z[5];
    z[16]=z[1]*z[16];
    z[17]= - n<T>(57,2) + 25*z[23];
    z[17]=z[97]*z[17];
    z[18]= - static_cast<T>(1)- z[88];
    z[18]=z[13]*z[18];
    z[17]=z[17] + z[18] - n<T>(25,4)*z[9];
    z[17]=z[4]*z[17];
    z[18]=z[3]*z[4];
    z[19]= - z[81]*z[18];
    z[19]=z[19] + z[4];
    z[19]=z[34]*z[19];
    z[21]=z[10]*z[4];
    z[22]=z[13]*z[21];
    z[19]=z[22] + z[19];
    z[19]=z[3]*z[19];
    z[17]=z[19] + z[17];
    z[19]=z[32]*z[4];
    z[22]=z[91]*z[19];
    z[23]=z[4]*z[12];
    z[24]= - z[23] + z[22];
    z[25]=z[120]*z[4];
    z[26]= - z[4]*z[67];
    z[26]=z[26] - z[25];
    z[26]=z[3]*z[26];
    z[26]= - n<T>(7,2)*z[4] + z[26];
    z[26]=z[3]*z[26];
    z[24]=n<T>(1,2)*z[24] + z[26];
    z[24]=z[8]*z[24];
    z[26]=z[6]*z[4];
    z[26]=z[21] + 16*z[26];
    z[26]=z[3]*z[26];
    z[28]= - static_cast<T>(1)+ 7*z[74];
    z[28]=z[4]*z[28];
    z[24]=z[24] + n<T>(5,2)*z[28] + z[26];
    z[24]=z[8]*z[24];
    z[26]= - z[18]*z[116];
    z[26]= - z[4] + z[26];
    z[26]=z[8]*z[26];
    z[26]=z[25] + z[26];
    z[28]=4*z[8];
    z[26]=z[26]*z[28];
    z[29]= - z[7]*z[29]*z[25];
    z[27]=z[4]*z[27];
    z[26]=z[29] - n<T>(75,4)*z[27] + z[26];
    z[26]=z[7]*z[26];
    z[17]=z[26] + 3*z[17] + z[24];
    z[17]=z[7]*z[17];
    z[24]= - z[42]*z[21];
    z[24]=9*z[4] + z[24];
    z[24]=z[3]*z[24];
    z[23]=2*z[23];
    z[26]=z[23] - z[18];
    z[26]=z[3]*z[26];
    z[26]= - z[19] + z[26];
    z[26]=z[26]*z[28];
    z[22]=z[26] + z[24] - z[22] + n<T>(1501,12) - z[23];
    z[22]=z[8]*z[22];
    z[23]=static_cast<T>(1)+ 2*z[59];
    z[23]=z[4]*z[13]*z[23];
    z[23]=z[23] - n<T>(9,4)*z[21];
    z[24]=3*z[4];
    z[26]= - z[34]*z[24];
    z[27]=z[4]*z[35];
    z[26]=z[26] - n<T>(11,4)*z[27];
    z[26]=z[3]*z[26];
    z[23]=3*z[23] + z[26];
    z[23]=z[3]*z[23];
    z[26]= - z[12] - z[90];
    z[26]=z[13]*z[26];
    z[26]=n<T>(11,4) + z[26];
    z[26]=z[26]*z[24];
    z[27]= - 75*z[54] + 139*z[2];
    z[27]=z[4]*z[27];
    z[27]=n<T>(1013,2) + z[27];
    z[27]=z[27]*z[63];
    z[28]= - n<T>(15,2)*z[2] + 7*z[12];
    z[29]= - z[4]*z[28];
    z[29]= - n<T>(15,8) + z[29];
    z[29]=z[9]*z[29];
    z[17]=z[17] + z[22] + z[23] + z[27] + 5*z[29] - n<T>(83,4)*z[10] + z[26]
    - n<T>(215,2)*z[13] + z[69] - n<T>(283,8)*z[5];
    z[17]=z[7]*z[17];
    z[22]= - z[35]*z[42];
    z[23]=n<T>(5,2)*z[11] - 3*z[109];
    z[22]=z[22] + n<T>(1,2)*z[23];
    z[22]=z[4]*z[22];
    z[23]=z[48] - z[103];
    z[23]=z[2]*z[23]*z[25];
    z[22]=z[23] + z[22];
    z[22]=z[3]*z[22];
    z[23]=3*z[87] - n<T>(27,4) + 8*z[108];
    z[23]=z[4]*z[23];
    z[25]= - static_cast<T>(2)+ z[108];
    z[25]=z[4]*z[25]*z[70];
    z[25]= - static_cast<T>(3)+ z[25];
    z[25]=z[25]*z[120];
    z[26]=n<T>(59,2)*z[5] + 217*z[11];
    z[26]=n<T>(1,2)*z[26] + 67*z[13];
    z[22]=z[22] + z[25] + n<T>(35,4)*z[10] + n<T>(1,2)*z[26] + z[23];
    z[22]=z[3]*z[22];
    z[23]=z[35]*z[18];
    z[24]= - z[11]*z[24];
    z[21]=z[23] + z[24] + z[21];
    z[21]=z[3]*z[21];
    z[23]= - static_cast<T>(1)+ z[60];
    z[23]=z[4]*z[23];
    z[21]=2*z[23] + z[21];
    z[21]=z[21]*z[42];
    z[21]= - n<T>(313,3) + z[21];
    z[21]=z[3]*z[21];
    z[23]=static_cast<T>(1)- 2*z[60];
    z[23]=z[4]*z[23];
    z[18]=z[11]*z[18];
    z[18]=z[23] + z[18];
    z[18]=z[3]*z[18];
    z[23]=z[11]*z[32];
    z[23]= - 2*z[12] + z[23];
    z[23]=z[4]*z[23];
    z[18]=z[23] + z[18];
    z[18]=z[3]*z[18];
    z[18]=z[19] + z[18];
    z[18]=z[8]*z[18]*z[42];
    z[18]=z[18] + z[21] + n<T>(533,12)*z[12] + 4*z[19];
    z[18]=z[8]*z[18];
    z[19]= - n<T>(3,2)*z[102] + 4*z[12] + n<T>(5,4)*z[2];
    z[19]=z[4]*z[19];
    z[21]=z[2]*z[28];
    z[21]=3*z[32] + 5*z[21];
    z[21]=z[4]*z[21];
    z[21]=z[21] + 67*z[12] + 29*z[2];
    z[21]=z[21]*z[80];
    z[20]= - z[4]*z[20];
    z[20]= - 19*z[2] + z[20];
    z[20]=z[20]*z[120];
    z[23]= - z[12]*z[86];

    r +=  - n<T>(611,12) + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + 
      z[22] + z[23] + z[33] + n<T>(11,2)*z[52] - n<T>(235,8)*z[59] + n<T>(189,4)*
      z[108];
 
    return r;
}

template double qg_2lNLC_r290(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r290(const std::array<dd_real,31>&);
#endif
