#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1112(const std::array<T,31>& k) {
  T z[76];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[13];
    z[6]=k[5];
    z[7]=k[8];
    z[8]=k[15];
    z[9]=k[26];
    z[10]=k[12];
    z[11]=k[2];
    z[12]=k[10];
    z[13]=k[4];
    z[14]=k[7];
    z[15]=npow(z[1],3);
    z[16]=4*z[15];
    z[17]=npow(z[4],2);
    z[18]=z[6]*z[17]*z[16];
    z[19]=z[4]*z[6];
    z[20]=z[19]*z[15];
    z[21]=z[15]*z[5];
    z[22]=z[21]*z[6];
    z[23]=z[20] - 10*z[22];
    z[23]=z[5]*z[23];
    z[18]=z[18] + z[23];
    z[18]=z[5]*z[18];
    z[23]=3*z[15];
    z[24]= - z[17]*z[23];
    z[25]=2*z[4];
    z[26]=z[25]*z[15];
    z[27]= - z[26] + 5*z[21];
    z[27]=z[5]*z[27];
    z[24]=z[24] + z[27];
    z[27]=z[2]*z[5];
    z[24]=z[24]*z[27];
    z[18]=z[18] + z[24];
    z[18]=z[2]*z[18];
    z[24]=npow(z[6],2);
    z[28]=z[24]*z[15];
    z[29]=z[28]*z[5];
    z[30]=z[28]*z[4];
    z[31]=z[30] + 5*z[29];
    z[31]=z[5]*z[31];
    z[32]= - z[17]*z[28];
    z[31]=z[32] + z[31];
    z[31]=z[5]*z[31];
    z[18]=z[31] + z[18];
    z[31]=2*z[2];
    z[18]=z[18]*z[31];
    z[32]=2*z[7];
    z[33]=npow(z[5],2);
    z[34]= - z[33]*z[32];
    z[35]=npow(z[5],3);
    z[34]=3*z[35] + z[34];
    z[34]=z[28]*z[34];
    z[36]=2*z[6];
    z[37]=z[36] - z[2];
    z[35]= - z[2]*z[37]*z[35]*z[15];
    z[34]=3*z[35] + z[34];
    z[35]=z[10] + z[7];
    z[35]=z[35]*z[9];
    z[38]=npow(z[10],2);
    z[38]=z[38] + z[35];
    z[38]=z[9]*z[28]*z[38];
    z[34]=2*z[34] + z[38];
    z[34]=z[13]*z[34];
    z[38]=z[25]*z[5];
    z[39]=z[17] + z[38];
    z[40]=npow(z[1],4);
    z[39]=z[40]*z[39];
    z[41]=z[24]*z[39];
    z[37]= - z[2]*z[39]*z[37];
    z[37]=z[41] + z[37];
    z[37]=z[3]*npow(z[2],2)*z[33]*z[37];
    z[18]=2*z[37] + z[18] + z[34];
    z[18]=z[3]*z[18];
    z[34]=npow(z[1],2);
    z[37]=2*z[34];
    z[39]=z[37]*z[17];
    z[41]=7*z[34];
    z[42]= - z[33]*z[41];
    z[42]=z[39] + z[42];
    z[42]=z[2]*z[42];
    z[43]=z[34]*z[6];
    z[44]= - z[17]*z[43];
    z[45]=z[25]*z[43];
    z[46]=z[43]*z[5];
    z[45]=z[45] + 9*z[46];
    z[45]=z[5]*z[45];
    z[42]=z[42] + z[44] + z[45];
    z[42]=z[42]*z[31];
    z[44]=z[24]*z[34];
    z[45]=z[44]*z[4];
    z[47]= - z[5]*z[24]*z[37];
    z[47]= - z[45] + z[47];
    z[48]=2*z[5];
    z[47]=z[47]*z[48];
    z[49]=z[44]*z[5];
    z[50]=z[44]*z[10];
    z[51]=z[49] + z[50];
    z[51]=z[10]*z[51];
    z[52]=10*z[43];
    z[53]=6*z[34];
    z[54]= - z[2]*z[53];
    z[54]=z[54] + z[52];
    z[54]=z[33]*z[54];
    z[55]=z[43]*z[10];
    z[56]= - z[7]*z[43];
    z[56]=z[56] - z[55];
    z[56]=z[9]*z[56];
    z[57]=z[5]*z[55];
    z[54]=z[56] + z[57] + z[54];
    z[54]=z[13]*z[54];
    z[18]=z[18] + z[54] + z[42] + z[47] + z[51];
    z[42]=npow(z[3],2);
    z[18]=z[18]*z[42];
    z[47]=z[28]*z[25];
    z[51]=z[43] - z[15];
    z[54]=z[6]*z[51];
    z[54]=z[54] - z[47];
    z[54]=z[4]*z[54];
    z[56]=z[36]*z[34];
    z[54]= - z[56] + z[54];
    z[54]=z[54]*z[25];
    z[57]=z[44] - z[30];
    z[57]=z[57]*z[25];
    z[58]=z[24]*z[1];
    z[57]= - z[58] + z[57];
    z[57]=z[5]*z[57];
    z[59]=z[6]*z[1];
    z[60]=3*z[59];
    z[54]=z[57] + z[60] + z[54];
    z[54]=z[5]*z[54];
    z[57]=z[43] + z[15];
    z[61]=2*z[15];
    z[62]=z[61] + z[43];
    z[62]=z[62]*z[6];
    z[40]=z[62] + z[40];
    z[62]=z[7]*z[40];
    z[62]= - 3*z[57] + z[62];
    z[62]=z[7]*z[62];
    z[63]=3*z[10];
    z[64]= - z[57]*z[63];
    z[40]=z[40]*z[35];
    z[40]=z[40] + z[62] + z[64];
    z[40]=z[9]*z[40];
    z[46]= - z[46] + z[55];
    z[46]=z[13]*z[46];
    z[46]=z[46] - z[49] + z[50];
    z[46]=z[46]*z[42];
    z[49]= - z[56] - z[45];
    z[50]=4*z[4];
    z[49]=z[49]*z[50];
    z[55]=z[37] + z[59];
    z[62]= - z[13]*z[1];
    z[49]=z[62] + z[49] - z[55];
    z[49]=z[10]*z[49];
    z[62]=4*z[34];
    z[64]= - z[62] + z[59];
    z[64]=z[6]*z[64];
    z[45]=z[64] - z[45];
    z[45]=z[4]*z[45];
    z[64]=z[59] - z[34];
    z[45]=2*z[64] + z[45];
    z[45]=z[45]*z[25];
    z[65]=z[7]*z[57];
    z[65]= - z[37] + z[65];
    z[65]=z[7]*z[65];
    z[40]=z[46] + z[40] + z[65] + z[54] - z[1] + z[45] + z[49];
    z[40]=z[8]*z[40];
    z[45]=npow(z[4],3);
    z[46]=z[62]*z[45];
    z[49]=z[26] - z[34];
    z[54]=z[15]*z[7];
    z[65]=z[49] + z[54];
    z[66]=z[17]*z[7];
    z[65]=z[65]*z[66];
    z[67]=z[45]*z[1];
    z[68]=z[67]*z[31];
    z[69]= - z[37] + z[54];
    z[45]=z[7]*z[45]*z[69];
    z[45]=z[67] + z[45];
    z[45]=z[11]*z[45];
    z[45]=z[45] + z[68] - z[46] + z[65];
    z[65]=2*z[12];
    z[45]=z[65]*z[45];
    z[68]=2*z[17];
    z[69]=3*z[34];
    z[70]=z[4]*z[69];
    z[70]= - z[1] + z[70];
    z[70]=z[70]*z[68];
    z[71]=z[15]*z[4];
    z[72]=5*z[34] - 6*z[71];
    z[66]=z[72]*z[66];
    z[66]=z[70] + z[66];
    z[66]=z[7]*z[66];
    z[70]=z[34]*z[17]*z[42];
    z[45]=z[70] + z[66] + z[45];
    z[45]=z[11]*z[45];
    z[66]= - z[4]*z[49];
    z[66]=z[1] + z[66];
    z[66]=z[4]*z[66];
    z[70]= - z[37] + z[71];
    z[70]=z[4]*z[70];
    z[72]=z[7]*z[71];
    z[70]=z[70] + z[72];
    z[70]=z[7]*z[70];
    z[72]=z[25]*z[34];
    z[72]=z[72] - z[1];
    z[73]=z[72]*z[17];
    z[74]=z[67]*z[2];
    z[73]=z[73] - z[74];
    z[73]=z[73]*z[31];
    z[66]=z[73] + z[66] + z[70];
    z[65]=z[66]*z[65];
    z[52]=6*z[20] - z[23] - z[52];
    z[52]=z[4]*z[52];
    z[52]=3*z[55] + z[52];
    z[52]=z[7]*z[4]*z[52];
    z[49]= - z[49]*z[25];
    z[66]=3*z[1];
    z[49]= - z[66] + z[49];
    z[49]=z[49]*z[25];
    z[49]=z[49] + z[52];
    z[49]=z[7]*z[49];
    z[52]=z[34]*z[7];
    z[70]=z[19]*z[52];
    z[39]= - z[2]*z[39];
    z[39]=z[70] + z[39];
    z[39]=z[39]*z[42];
    z[42]=11*z[1];
    z[70]=z[62]*z[4];
    z[73]=z[42] + z[70];
    z[73]=z[73]*z[17];
    z[75]= - z[4]*z[59];
    z[75]= - z[1] + z[75];
    z[75]=z[75]*z[25];
    z[52]=z[1] + z[52];
    z[52]=z[7]*z[52];
    z[52]=z[75] + z[52];
    z[52]=z[8]*z[52];
    z[39]=z[45] + z[52] + z[39] + z[65] + 4*z[74] + z[73] + z[49];
    z[39]=z[11]*z[39];
    z[45]= - z[69] - z[26];
    z[45]=z[45]*z[68];
    z[49]= - z[17]*z[61];
    z[49]= - z[1] + z[49];
    z[49]=z[5]*z[49];
    z[45]=z[45] + z[49];
    z[45]=z[5]*z[45];
    z[49]=z[5]*z[4]*z[72];
    z[46]=z[46] + z[49];
    z[46]=z[5]*z[46];
    z[49]=2*z[67];
    z[52]= - z[17]*z[5]*z[1];
    z[52]= - z[49] + z[52];
    z[52]=z[52]*z[27];
    z[46]=z[52] + z[49] + z[46];
    z[46]=z[46]*z[31];
    z[49]=z[1] - z[70];
    z[49]=z[49]*z[68];
    z[45]=z[46] + z[49] + z[45];
    z[45]=z[2]*z[45];
    z[46]=z[62] + 3*z[71];
    z[46]=z[4]*z[46];
    z[49]=z[34] + z[71];
    z[49]=z[5]*z[49];
    z[46]=z[46] + z[49];
    z[46]=z[5]*z[46];
    z[49]=z[26] + z[34];
    z[52]=z[4]*z[49];
    z[52]=z[1] + z[52];
    z[52]=z[4]*z[52];
    z[46]=z[52] + z[46];
    z[45]=2*z[46] + z[45];
    z[45]=z[2]*z[45];
    z[46]=z[15]*z[10];
    z[52]=z[54] + z[46];
    z[54]=z[9]*z[52];
    z[65]=z[10]*z[21];
    z[54]=z[65] - z[54];
    z[65]= - z[37] - z[71];
    z[65]=z[65]*z[50];
    z[49]= - 5*z[49] - 3*z[21];
    z[49]=z[5]*z[49];
    z[70]=z[61]*z[7];
    z[70]=z[70] - z[69];
    z[71]=8*z[71] + z[70];
    z[71]=z[7]*z[71];
    z[45]=z[45] + z[71] + z[49] - z[1] + z[65] - 11*z[54];
    z[45]=z[12]*z[45];
    z[49]=z[56] + z[15];
    z[54]=z[22] - z[49];
    z[54]=z[54]*z[48];
    z[54]=z[54] - z[62] - z[59];
    z[65]=z[10]*z[5];
    z[54]=z[54]*z[65];
    z[71]= - z[34] - 2*z[21];
    z[71]=z[10]*z[71];
    z[72]= - z[37] - z[46];
    z[72]=z[12]*z[72];
    z[53]=z[53]*z[5];
    z[53]=z[53] + z[1];
    z[71]=z[72] - z[53] + z[71];
    z[71]=z[13]*z[33]*z[71];
    z[72]= - z[69] - z[59];
    z[72]=z[72]*z[48];
    z[72]=z[1] + z[72];
    z[72]=z[72]*z[48];
    z[54]=z[71] + z[72] + z[54];
    z[54]=z[10]*z[54];
    z[71]=z[7]*z[55];
    z[71]= - 2*z[1] + z[71];
    z[32]=z[71]*z[32];
    z[71]=z[41] + 5*z[59];
    z[71]=z[10]*z[71];
    z[72]=4*z[1];
    z[71]= - z[72] + z[71];
    z[71]=z[10]*z[71];
    z[73]=7*z[59];
    z[74]=11*z[34];
    z[75]=z[73] + z[74];
    z[35]=z[75]*z[35];
    z[32]=z[35] + z[32] + z[71];
    z[32]=z[9]*z[32];
    z[35]=z[7]*z[70];
    z[46]= - z[34] + z[46];
    z[46]=z[46]*z[63];
    z[70]=5*z[9];
    z[52]=z[52]*z[70];
    z[35]=z[52] + z[35] + z[46];
    z[35]=z[9]*z[35];
    z[46]= - z[69] - 4*z[21];
    z[46]=z[5]*z[46];
    z[52]= - z[21]*z[63];
    z[46]=z[46] + z[52];
    z[46]=z[10]*z[46];
    z[52]= - z[33]*z[37];
    z[35]=z[35] + z[52] + z[46];
    z[35]=z[12]*z[35];
    z[46]= - z[53]*z[33];
    z[32]=z[35] + z[32] + z[46] + z[54];
    z[32]=z[13]*z[32];
    z[35]=z[74] + z[60];
    z[35]=z[35]*z[6];
    z[46]=8*z[15];
    z[35]=z[35] + z[46];
    z[52]=z[55]*z[6];
    z[52]=z[52] + z[15];
    z[53]=z[52]*z[7];
    z[54]=z[53]*z[6];
    z[55]= - 3*z[54] - z[35];
    z[55]=z[7]*z[55];
    z[63]=z[52]*z[10];
    z[69]= - z[36]*z[63];
    z[35]=z[69] - z[35];
    z[35]=z[10]*z[35];
    z[69]= - z[6]*z[63];
    z[54]= - z[54] + z[69];
    z[54]=z[54]*z[70];
    z[35]=z[54] + z[55] + z[35];
    z[35]=z[9]*z[35];
    z[54]= - z[15] - 5*z[43];
    z[54]=z[6]*z[54];
    z[55]=z[28]*z[48];
    z[54]=z[54] + z[55];
    z[54]=z[5]*z[54];
    z[37]=z[37] + z[60];
    z[37]=z[6]*z[37];
    z[37]=z[54] + z[16] + z[37];
    z[37]=z[5]*z[37];
    z[54]=z[52]*z[6];
    z[55]= - z[49]*z[24];
    z[21]=npow(z[6],3)*z[21];
    z[21]=z[55] + z[21];
    z[21]=z[5]*z[21];
    z[21]=z[54] + z[21];
    z[21]=z[21]*z[65];
    z[54]=2*z[59];
    z[21]=z[21] + z[54] + z[37];
    z[21]=z[10]*z[21];
    z[37]= - z[6]*z[57];
    z[37]=z[37] + z[30];
    z[37]=z[4]*z[37];
    z[28]=z[28]*z[38];
    z[28]=z[37] + z[28];
    z[28]=z[5]*z[28];
    z[16]=z[16] - z[20];
    z[16]=z[4]*z[16];
    z[16]=z[28] + z[59] + z[16];
    z[16]=z[5]*z[16];
    z[16]=z[21] + z[1] + z[16];
    z[20]=z[26]*z[6];
    z[21]=z[20] - z[46] - z[43];
    z[21]=z[4]*z[21];
    z[28]= - z[23] - z[56];
    z[28]=z[6]*z[28];
    z[28]=z[28] + z[30];
    z[28]=z[4]*z[28];
    z[28]= - 2*z[58] + z[28];
    z[28]=z[7]*z[28];
    z[21]=z[28] - z[54] + z[21];
    z[21]=z[7]*z[21];
    z[16]=z[35] + z[21] + 2*z[16];
    z[16]=z[14]*z[16];
    z[21]=3*z[43] - z[22];
    z[21]=z[21]*z[48];
    z[22]=9*z[59];
    z[21]=z[21] - z[74] - z[22];
    z[21]=z[5]*z[21];
    z[28]=z[6]*z[49];
    z[28]=z[28] - z[29];
    z[28]=z[5]*z[28];
    z[28]=z[28] - z[52];
    z[28]=z[28]*z[48];
    z[28]=z[59] + z[28];
    z[28]=z[10]*z[28];
    z[29]=z[4]*z[1];
    z[24]=z[24]*z[29];
    z[24]=z[54] + z[24];
    z[24]=z[24]*z[50];
    z[21]=z[28] + z[21] + z[24] - z[66] - z[36];
    z[21]=z[10]*z[21];
    z[22]=z[22] + 20*z[34];
    z[24]=2*z[53] + z[22];
    z[24]=z[7]*z[24];
    z[22]=2*z[63] + z[22];
    z[22]=z[10]*z[22];
    z[28]=z[53] + z[63];
    z[28]=z[9]*z[28];
    z[22]=4*z[28] + z[24] + z[22];
    z[22]=z[9]*z[22];
    z[24]=z[51]*z[19];
    z[23]=z[24] + z[23] + z[43];
    z[23]=z[23]*z[25];
    z[20]=z[15] - z[20];
    z[20]=z[20]*z[48];
    z[20]=z[20] + z[23] - 17*z[34] - 15*z[59];
    z[20]=z[5]*z[20];
    z[23]=z[59] + z[34];
    z[24]=z[6]*z[23];
    z[28]=z[25]*z[44];
    z[24]=z[28] + z[61] + z[24];
    z[24]=z[4]*z[24];
    z[24]=z[24] + z[23];
    z[24]=z[24]*z[25];
    z[20]=z[20] + z[24] - z[42] - 4*z[6];
    z[20]=z[5]*z[20];
    z[24]=z[6]*z[64];
    z[28]=z[61] - z[43];
    z[19]=z[28]*z[19];
    z[19]=z[19] - z[61] + z[24];
    z[19]=z[4]*z[19];
    z[19]=z[19] + z[34] + z[54];
    z[19]=z[19]*z[25];
    z[24]=5*z[44] - z[47];
    z[24]=z[4]*z[24];
    z[28]=z[60] + z[62];
    z[30]= - z[6]*z[28];
    z[24]=z[24] + z[15] + z[30];
    z[24]=z[4]*z[24];
    z[24]=z[24] + z[28];
    z[24]=z[7]*z[24];
    z[19]=z[24] + z[19] + z[72] + z[6];
    z[19]=z[7]*z[19];
    z[15]= - z[15] + z[58];
    z[15]=z[4]*z[15];
    z[15]=z[15] - z[41] - 4*z[59];
    z[15]=z[15]*z[25];
    z[24]=z[34] + z[60];
    z[24]=3*z[24] - z[26];
    z[24]=z[24]*z[48];
    z[15]=z[24] + 29*z[1] + z[15];
    z[15]=z[5]*z[15];
    z[24]=z[25]*z[58];
    z[24]=z[24] - z[62] - z[73];
    z[24]=z[4]*z[24];
    z[24]= - z[72] + z[24];
    z[24]=z[4]*z[24];
    z[24]=static_cast<T>(3)+ z[24];
    z[15]=2*z[24] + z[15];
    z[15]=z[5]*z[15];
    z[24]=z[23]*z[25];
    z[26]=z[24] - z[66];
    z[17]= - z[26]*z[17];
    z[26]=z[5]*z[26];
    z[26]=3*z[29] + z[26];
    z[26]=z[5]*z[26];
    z[17]=z[17] + z[26];
    z[17]=z[5]*z[17];
    z[26]= - z[33]*z[29];
    z[26]=z[67] + z[26];
    z[26]=z[26]*z[27];
    z[17]=6*z[26] - 4*z[67] + 3*z[17];
    z[17]=z[17]*z[31];
    z[26]=7*z[1];
    z[24]= - z[26] + z[24];
    z[24]=z[24]*z[68];
    z[15]=z[17] + z[24] + z[15];
    z[15]=z[2]*z[15];
    z[17]=z[57]*z[25];
    z[17]=3*z[23] + z[17];
    z[17]=z[17]*z[25];
    z[17]=z[17] + z[26] - z[36];
    z[17]=z[4]*z[17];

    r +=  - static_cast<T>(2)+ z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21]
       + z[22] + z[32] + z[39] + z[40] + z[45];
 
    return r;
}

template double qg_2lNLC_r1112(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1112(const std::array<dd_real,31>&);
#endif
