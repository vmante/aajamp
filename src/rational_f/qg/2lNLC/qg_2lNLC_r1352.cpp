#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1352(const std::array<T,31>& k) {
  T z[69];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[8];
    z[6]=k[26];
    z[7]=k[12];
    z[8]=k[2];
    z[9]=k[14];
    z[10]=k[15];
    z[11]=k[3];
    z[12]=k[4];
    z[13]=k[13];
    z[14]=k[10];
    z[15]= - z[6] + z[4];
    z[16]=z[2]*z[1];
    z[17]=npow(z[16],2);
    z[15]=z[17]*z[15];
    z[18]=56*z[6];
    z[19]=z[18] + n<T>(73813,20)*z[4];
    z[20]=npow(z[1],2);
    z[21]=z[20]*z[2];
    z[19]=z[21]*z[19];
    z[22]=z[20]*z[4];
    z[23]=z[22]*z[12];
    z[19]=n<T>(74933,10)*z[23] + z[19];
    z[19]=z[12]*z[19];
    z[15]=224*z[15] + z[19];
    z[19]=z[3]*z[4];
    z[24]=npow(z[16],5);
    z[25]=z[24]*z[19];
    z[26]=npow(z[17],2);
    z[27]=npow(z[1],4);
    z[28]=z[27]*npow(z[2],3);
    z[29]=z[12]*z[28];
    z[25]= - z[25] + z[26] - z[29];
    z[25]= - 2*z[25];
    z[25]=z[25]*npow(z[6],3);
    z[29]=npow(z[6],2);
    z[30]=z[4]*z[29]*z[26];
    z[25]=7*z[30] + z[25];
    z[30]=n<T>(1,9)*z[3];
    z[25]=z[25]*z[30];
    z[31]=npow(z[1],3);
    z[32]=n<T>(1,3)*z[31];
    z[33]=npow(z[2],2);
    z[34]=z[12]*z[33]*z[32];
    z[35]=npow(z[16],3);
    z[34]=z[34] - n<T>(5,9)*z[35];
    z[29]=z[29]*z[34];
    z[34]=z[35]*z[4];
    z[36]=z[6]*z[34];
    z[25]=z[25] + z[36] + z[29];
    z[29]=56*z[3];
    z[25]=z[25]*z[29];
    z[15]=n<T>(1,9)*z[15] + z[25];
    z[25]=npow(z[3],2);
    z[15]=z[15]*z[25];
    z[36]=z[33]*z[31];
    z[37]=z[31]*z[2];
    z[38]=z[37]*z[12];
    z[39]=n<T>(74933,12)*z[36] + 19663*z[38];
    z[40]=npow(z[3],3);
    z[39]=z[39]*z[40];
    z[39]=n<T>(27101,3)*z[16] + z[39];
    z[39]=z[12]*z[39];
    z[41]=z[3]*z[1];
    z[41]=z[33]*npow(z[41],4);
    z[42]=npow(z[12],2);
    z[43]=z[13]*z[42]*z[41];
    z[39]=n<T>(3719,2)*z[43] + z[39];
    z[39]=z[13]*z[39];
    z[43]=z[20]*z[12];
    z[44]= - n<T>(1,2)*z[21] - z[43];
    z[44]=z[44]*z[25];
    z[44]=n<T>(74933,30)*z[44] + n<T>(19663,5)*z[1];
    z[44]=z[12]*z[44];
    z[45]=8609*z[20];
    z[46]=z[45] + n<T>(74933,10)*z[16];
    z[39]=n<T>(1,5)*z[39] + n<T>(1,6)*z[46] + z[44];
    z[44]=n<T>(1,3)*z[13];
    z[39]=z[39]*z[44];
    z[46]=z[20] + z[16];
    z[47]=z[46]*z[2];
    z[47]=z[47] + z[32];
    z[48]=n<T>(2,3)*z[47];
    z[49]=z[48]*z[6];
    z[50]=n<T>(1,3)*z[20];
    z[51]=z[50] + z[16];
    z[49]= - z[49] - z[51];
    z[49]=z[2]*z[49];
    z[52]=n<T>(1,9)*z[31];
    z[49]=z[52] + z[49];
    z[49]=z[6]*z[49];
    z[53]=z[50] - z[16];
    z[54]=n<T>(1,3)*z[53];
    z[49]=z[54] + z[49];
    z[49]=z[49]*z[18];
    z[55]=z[20] + n<T>(5,3)*z[16];
    z[55]=z[55]*z[2];
    z[55]=z[55] + z[52];
    z[55]=z[55]*z[2];
    z[56]=n<T>(2,3)*z[33];
    z[57]=z[47]*z[56];
    z[58]=z[6]*z[57];
    z[58]=z[55] + z[58];
    z[58]=z[6]*z[58];
    z[59]=z[20] + 4*z[16];
    z[60]=n<T>(1,3)*z[2];
    z[59]=z[59]*z[60];
    z[58]=z[59] + z[58];
    z[58]=z[58]*z[18];
    z[58]= - n<T>(71573,180)*z[16] + z[58];
    z[58]=z[4]*z[58];
    z[60]=2*z[16];
    z[61]=z[60] + z[20];
    z[62]=2*z[6];
    z[63]=z[47]*z[62];
    z[63]=z[63] + z[61];
    z[63]=z[6]*z[63];
    z[64]=n<T>(1,3)*z[1];
    z[63]=z[64] + z[63];
    z[63]=z[63]*z[18];
    z[65]=z[4]*z[1];
    z[63]=z[63] - n<T>(74933,30)*z[65];
    z[66]=n<T>(1,3)*z[12];
    z[63]=z[63]*z[66];
    z[15]=z[39] + z[15] + z[63] + z[49] + z[58];
    z[15]=z[5]*z[15];
    z[39]=z[7]*z[2];
    z[27]=z[27]*npow(z[39],3);
    z[49]=npow(z[7],2);
    z[58]=z[6]*z[7];
    z[63]=z[49] + z[58];
    z[63]=z[63]*z[6];
    z[28]=z[28]*z[63];
    z[28]=z[27] + z[28];
    z[28]=z[12]*z[28];
    z[67]=npow(z[7],3);
    z[68]=z[67] + z[63];
    z[24]=z[19]*z[24]*z[68];
    z[68]=z[26]*z[63];
    z[26]=z[67]*z[26];
    z[24]=z[24] + z[28] - z[26] - z[68];
    z[24]=z[62]*z[24];
    z[26]= - 4*z[26] + 7*z[68];
    z[26]=z[4]*z[26];
    z[24]=z[26] + z[24];
    z[24]=z[24]*z[30];
    z[26]=z[66]*z[36]*z[63];
    z[28]=4*z[67] - 5*z[63];
    z[28]=z[35]*z[28];
    z[30]= - n<T>(10,9)*z[49] + z[58];
    z[30]=z[30]*z[34];
    z[24]=z[24] + z[26] + n<T>(1,9)*z[28] + z[30];
    z[24]=z[24]*z[29];
    z[26]=z[49] - n<T>(2,3)*z[58];
    z[28]=z[4]*z[7];
    z[26]=112*z[26] + n<T>(7825,6)*z[28];
    z[26]=z[17]*z[26];
    z[28]=z[6]*z[39]*z[43];
    z[26]=n<T>(56,3)*z[28] + z[26];
    z[24]=n<T>(1,3)*z[26] + z[24];
    z[24]=z[24]*z[25];
    z[26]=z[51]*z[2];
    z[28]=z[48]*z[39];
    z[26]= - z[52] + z[26] + z[28];
    z[26]=z[26]*z[7];
    z[30]=z[54] - z[26];
    z[30]=z[7]*z[30];
    z[28]= - z[6]*z[28];
    z[26]= - z[26] + z[28];
    z[26]=z[6]*z[26];
    z[26]=z[30] + z[26];
    z[26]=z[26]*z[18];
    z[28]=z[56]*z[7];
    z[30]=z[28]*z[47];
    z[30]=z[55] + z[30];
    z[30]=z[30]*z[7];
    z[34]=z[59] + z[30];
    z[34]=z[7]*z[34];
    z[35]=z[58]*z[57];
    z[30]=z[30] + z[35];
    z[30]=z[6]*z[30];
    z[30]=z[34] + z[30];
    z[18]=z[30]*z[18];
    z[30]= - z[50] - z[60];
    z[30]=z[2]*z[30];
    z[28]= - z[61]*z[28];
    z[28]=z[30] + z[28];
    z[28]=z[7]*z[28];
    z[30]=56*z[20] - n<T>(26051,2)*z[16];
    z[28]=n<T>(1,3)*z[30] + 112*z[28];
    z[28]=z[7]*z[28];
    z[30]=8497*z[2];
    z[34]= - n<T>(226999,10)*z[1] + z[30];
    z[28]=n<T>(1,6)*z[34] + z[28];
    z[18]=n<T>(1,3)*z[28] + z[18];
    z[18]=z[4]*z[18];
    z[28]=z[40]*z[37];
    z[34]=z[9]*z[41];
    z[28]=n<T>(676837,2)*z[28] - 187573*z[34];
    z[28]=z[9]*z[28];
    z[34]=z[25]*z[20];
    z[28]= - n<T>(301691,2)*z[34] + z[28];
    z[28]=z[8]*z[10]*z[28];
    z[35]=z[21]*z[25];
    z[41]=z[9]*z[16];
    z[41]=n<T>(187573,30)*z[41] - n<T>(57059,45)*z[1] + n<T>(4897,4)*z[35];
    z[41]=z[9]*z[41];
    z[41]= - n<T>(4897,6) + z[41];
    z[48]=z[14]*z[1];
    z[28]=n<T>(1,180)*z[28] + n<T>(4897,24)*z[48] + n<T>(1,2)*z[41];
    z[28]=z[9]*z[28];
    z[35]= - 46201*z[1] - n<T>(448601,3)*z[35];
    z[41]=z[40]*z[36];
    z[41]=20386*z[16] + n<T>(65257,2)*z[41];
    z[48]=n<T>(1,3)*z[9];
    z[41]=z[41]*z[48];
    z[35]=n<T>(1,8)*z[35] + z[41];
    z[35]=z[9]*z[35];
    z[35]=n<T>(187573,24) + z[35];
    z[35]=z[10]*z[35];
    z[28]=n<T>(1,5)*z[35] + z[28];
    z[28]=z[8]*z[28];
    z[35]=z[61]*z[39];
    z[35]=n<T>(4,3)*z[35] - z[20] + z[16];
    z[35]=z[7]*z[35];
    z[30]=8161*z[1] - z[30];
    z[30]=n<T>(1,6)*z[30] + 56*z[35];
    z[30]=z[7]*z[30];
    z[30]= - n<T>(57059,60) + z[30];
    z[15]=z[28] + z[15] + z[24] + z[18] + n<T>(1,3)*z[30] + z[26];
    z[18]= - n<T>(97247,10)*z[21] + n<T>(8609,3)*z[31];
    z[24]=z[7]*z[18];
    z[18]= - z[18]*z[39];
    z[18]=8609*z[21] + z[18];
    z[18]=z[4]*z[18];
    z[18]=z[24] + z[18];
    z[24]= - static_cast<T>(1)+ z[39];
    z[24]=z[31]*z[24];
    z[26]=z[36]*z[4];
    z[28]= - z[7]*z[26];
    z[24]=z[28] + z[24];
    z[28]= - z[7]*z[32];
    z[28]=z[16] + z[28];
    z[28]=z[12]*z[28];
    z[24]=n<T>(1,3)*z[24] + z[28];
    z[28]=z[36] - z[38];
    z[30]=n<T>(1,3)*z[40];
    z[28]=z[12]*z[28]*z[30];
    z[24]=n<T>(1,2)*z[24] + z[28];
    z[28]=n<T>(3719,5)*z[13];
    z[24]=z[24]*z[28];
    z[35]=n<T>(19663,3)*z[20] + n<T>(11869,2)*z[16];
    z[38]=n<T>(97247,12)*z[21] - 19663*z[43];
    z[38]=z[12]*z[38];
    z[38]=n<T>(74933,12)*z[17] + z[38];
    z[38]=z[38]*z[25];
    z[41]=z[7]*z[45];
    z[41]=n<T>(51017,5)*z[1] + z[41];
    z[41]=z[12]*z[41];
    z[18]=z[24] + n<T>(1,15)*z[38] + n<T>(2,9)*z[41] + n<T>(1,5)*z[35] + n<T>(1,6)*z[18]
   ;
    z[18]=z[13]*z[18];
    z[24]=z[20] - 5*z[16];
    z[35]=z[45] - n<T>(441607,30)*z[16];
    z[35]=z[35]*z[39];
    z[24]=n<T>(8609,3)*z[24] + z[35];
    z[24]=z[4]*z[24];
    z[35]= - z[7]*z[53];
    z[35]= - n<T>(1,6)*z[1] + z[35];
    z[38]=z[7]*z[1];
    z[41]= - z[38] + n<T>(1,2)*z[65];
    z[41]=z[12]*z[41];
    z[24]=n<T>(8609,3)*z[41] + 8609*z[35] + n<T>(1,2)*z[24];
    z[18]=n<T>(1,3)*z[24] + z[18];
    z[18]=z[18]*z[44];
    z[24]=z[49]*z[26];
    z[19]=z[19]*z[27];
    z[19]=z[24] + n<T>(2,3)*z[19];
    z[19]=z[19]*z[29];
    z[24]=z[39]*z[22];
    z[19]=n<T>(8833,6)*z[24] + z[19];
    z[24]=n<T>(1,9)*z[25];
    z[19]=z[19]*z[24];
    z[26]=z[31]*z[12];
    z[27]= - 2*z[37] + n<T>(1,2)*z[26];
    z[27]=z[12]*z[27];
    z[27]=n<T>(1,2)*z[36] + z[27];
    z[27]=z[27]*z[30];
    z[29]= - z[12]*z[1];
    z[29]=z[29] + z[46];
    z[27]=n<T>(1,2)*z[29] + z[27];
    z[27]=z[27]*z[28];
    z[28]= - n<T>(138709,20)*z[21] - 8609*z[43];
    z[24]=z[28]*z[24];
    z[24]=z[27] - n<T>(11869,10)*z[1] + z[24];
    z[27]=npow(z[13],2);
    z[24]=z[24]*z[27];
    z[26]= - z[37] + z[26];
    z[26]=z[26]*z[30];
    z[28]=n<T>(1,2)*z[1];
    z[26]= - z[28] + z[26];
    z[26]=z[13]*z[26];
    z[26]=n<T>(15944,9)*z[34] + 3719*z[26];
    z[26]=z[26]*z[27];
    z[27]=z[11]*z[31]*z[40]*npow(z[13],3);
    z[26]=z[26] + n<T>(3719,6)*z[27];
    z[26]=z[11]*z[26];
    z[27]=z[33]*z[38];
    z[27]=z[16] + 2*z[27];
    z[27]=z[7]*z[27];
    z[27]=z[1] + z[27];
    z[27]=z[7]*z[27];
    z[27]=n<T>(323,2) + n<T>(56,27)*z[27];
    z[27]=z[4]*z[27];
    z[19]=n<T>(1,15)*z[26] + n<T>(1,3)*z[24] + z[27] + z[19];
    z[19]=z[11]*z[19];
    z[24]=2*z[47];
    z[26]=z[24]*z[7];
    z[26]=z[26] + z[61];
    z[26]=z[26]*z[7];
    z[27]=z[64] + z[26];
    z[27]=z[7]*z[27];
    z[24]=z[58]*z[24];
    z[24]=z[26] + z[24];
    z[24]=z[6]*z[24];
    z[24]=z[27] + z[24];
    z[24]=z[6]*z[24];
    z[24]=n<T>(56,9)*z[24] - n<T>(323,2)*z[4];
    z[24]=z[12]*z[24];
    z[26]= - n<T>(57059,3)*z[31] - n<T>(187573,2)*z[21];
    z[26]=z[4]*z[26];
    z[26]=z[26] - n<T>(529927,6)*z[20] - 187573*z[16];
    z[27]=z[32]*z[4];
    z[29]=z[2]*z[27];
    z[29]=z[21] + z[29];
    z[27]=n<T>(1,2)*z[20] + z[27];
    z[27]=z[12]*z[27];
    z[27]=n<T>(1,2)*z[29] + z[27];
    z[27]=z[9]*z[27];
    z[29]= - n<T>(57059,6)*z[1] - 20386*z[22];
    z[29]=z[12]*z[29];
    z[26]=n<T>(187573,6)*z[27] + n<T>(1,12)*z[26] + z[29];
    z[26]=z[9]*z[26];
    z[27]=57059*z[20] + n<T>(187573,4)*z[16];
    z[27]=z[4]*z[27];
    z[29]=n<T>(40663,5)*z[1] - 24485*z[2];
    z[27]=n<T>(1,4)*z[29] + n<T>(1,15)*z[27];
    z[29]=n<T>(4897,6) + n<T>(46201,5)*z[65];
    z[29]=z[12]*z[29];
    z[27]=n<T>(1,3)*z[27] + n<T>(1,2)*z[29];
    z[26]=n<T>(1,2)*z[27] + n<T>(1,5)*z[26];
    z[26]=z[26]*z[48];
    z[27]= - z[42]*z[25]*z[22];
    z[29]=static_cast<T>(1)- z[65];
    z[29]=z[12]*z[29];
    z[27]=z[27] + z[28] + z[29];
    z[28]=z[31]*z[4];
    z[29]=n<T>(448601,2)*z[20] - 57059*z[28];
    z[29]=n<T>(1,3)*z[29] - n<T>(301691,2)*z[23];
    z[29]=z[12]*z[29];
    z[28]=z[9]*z[42]*z[28];
    z[28]=z[29] + n<T>(187573,3)*z[28];
    z[28]=z[9]*z[28];
    z[29]=z[12]*z[65];
    z[22]=n<T>(252721,2)*z[29] - n<T>(301691,3)*z[1] + 57059*z[22];
    z[22]=z[12]*z[22];
    z[22]=z[28] - n<T>(57059,3)*z[20] + z[22];
    z[22]=z[9]*z[22];
    z[28]= - z[25]*z[23];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=z[11]*z[28];
    z[22]=n<T>(57059,6)*z[28] + n<T>(57059,3)*z[27] + n<T>(1,2)*z[22];
    z[22]=z[14]*z[22];
    z[17]=z[25]*z[17];
    z[17]= - 69356*z[17] - n<T>(57059,4)*z[20] - 65257*z[16];
    z[17]=z[9]*z[17];
    z[17]=z[17] + n<T>(187573,4)*z[1] + 69356*z[2];
    z[20]= - z[4]*z[21];
    z[20]=z[20] + z[23];
    z[20]=z[20]*z[25];
    z[21]= - n<T>(334483,15) - 4897*z[65];
    z[20]=n<T>(4897,8)*z[20] + n<T>(1,8)*z[21];
    z[20]=z[12]*z[20];
    z[16]=z[4]*z[16];
    z[16]=n<T>(4897,8)*z[16] + z[20] + n<T>(1,15)*z[17];
    z[16]=z[10]*z[16];

    r += n<T>(1,3)*z[15] + n<T>(1,9)*z[16] + z[18] + z[19] + n<T>(1,90)*z[22] + 
      z[24] + z[26];
 
    return r;
}

template double qg_2lNLC_r1352(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1352(const std::array<dd_real,31>&);
#endif
