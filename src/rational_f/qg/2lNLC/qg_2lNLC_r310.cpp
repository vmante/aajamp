#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r310(const std::array<T,31>& k) {
  T z[93];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[7];
    z[9]=k[5];
    z[10]=k[13];
    z[11]=k[2];
    z[12]=k[14];
    z[13]=k[9];
    z[14]=k[3];
    z[15]=npow(z[1],2);
    z[16]=3*z[15];
    z[17]=z[9]*z[1];
    z[18]=5*z[1];
    z[19]= - z[14]*z[18];
    z[19]=z[19] + z[16] - z[17];
    z[20]=npow(z[9],2);
    z[21]=z[20]*z[15];
    z[22]=npow(z[1],3);
    z[23]=z[22]*z[5];
    z[24]=z[20]*z[23];
    z[25]=z[20]*z[1];
    z[26]= - z[14]*z[25];
    z[24]=z[26] - z[21] + z[24];
    z[24]=z[4]*z[24];
    z[21]=z[5]*z[21];
    z[21]= - z[25] + z[21];
    z[26]=z[14]*z[17];
    z[21]=n<T>(3,4)*z[24] + n<T>(1,4)*z[21] - 2*z[26];
    z[21]=z[4]*z[21];
    z[24]=z[22]*z[3];
    z[26]=n<T>(1,2)*z[15];
    z[27]=z[26] - z[24];
    z[28]=n<T>(1,2)*z[2];
    z[27]=z[27]*z[28];
    z[29]=z[15]*z[3];
    z[30]=n<T>(1,2)*z[1];
    z[31]=z[30]*z[14];
    z[32]= - z[3]*z[31];
    z[27]=z[27] + z[32] - n<T>(1,4)*z[1] + z[29];
    z[27]=z[11]*z[27];
    z[32]=n<T>(1,2)*z[9];
    z[33]= - z[15]*z[32];
    z[33]=z[22] + z[33];
    z[33]=z[33]*z[28];
    z[19]=z[27] + z[33] + n<T>(1,4)*z[19] + z[21];
    z[19]=z[13]*z[19];
    z[21]=npow(z[10],2);
    z[27]=n<T>(3,2)*z[1];
    z[33]= - z[21]*z[27];
    z[34]=2*z[1];
    z[35]=z[15]*z[10];
    z[36]= - z[34] - z[35];
    z[36]=z[3]*z[10]*z[36];
    z[37]=z[21]*z[3];
    z[31]=z[37]*z[31];
    z[31]=z[31] + z[33] + z[36];
    z[31]=z[14]*z[31];
    z[33]=z[17] + z[15];
    z[36]=z[10]*z[33];
    z[38]=3*z[1];
    z[36]=z[38] + z[36];
    z[39]=n<T>(3,2)*z[10];
    z[36]=z[36]*z[39];
    z[40]=z[22]*z[10];
    z[41]=4*z[15] + n<T>(1,2)*z[40];
    z[41]=z[10]*z[41];
    z[41]=z[34] + z[41];
    z[41]=z[3]*z[41];
    z[31]=z[31] + z[36] + z[41];
    z[31]=z[14]*z[31];
    z[36]=n<T>(3,2)*z[15];
    z[41]= - z[36] - z[17];
    z[42]=z[20]*z[10];
    z[43]= - z[30]*z[42];
    z[41]=3*z[41] + z[43];
    z[41]=z[10]*z[41];
    z[43]=n<T>(1,2)*z[17];
    z[23]=z[23] - z[15] - z[43];
    z[44]=n<T>(3,2)*z[4];
    z[23]=z[44]*z[23];
    z[45]=n<T>(3,4)*z[17];
    z[46]=2*z[15] + z[45];
    z[46]=z[5]*z[46];
    z[23]=z[23] + z[46];
    z[23]=z[9]*z[23];
    z[46]= - z[15] - n<T>(3,2)*z[17];
    z[23]=n<T>(1,2)*z[46] + z[23];
    z[23]=z[4]*z[23];
    z[46]=3*z[24] - z[15] + z[43];
    z[28]=z[46]*z[28];
    z[40]= - n<T>(7,2)*z[15] - 2*z[40];
    z[40]=z[3]*z[40];
    z[46]=z[42]*z[15];
    z[25]=z[46] - z[25];
    z[47]=z[25]*z[10];
    z[48]=z[17] + z[47];
    z[49]=n<T>(1,2)*z[5];
    z[48]=z[48]*z[49];
    z[50]= - z[30] + z[29];
    z[50]=z[2]*z[50];
    z[51]= - z[3]*z[1];
    z[50]=z[51] + z[50];
    z[50]=z[11]*z[50];
    z[51]=n<T>(7,4)*z[1];
    z[19]=z[19] + n<T>(1,2)*z[50] + z[28] + z[23] + z[31] + z[48] + z[40] - 
    z[51] + z[41];
    z[19]=z[13]*z[19];
    z[23]=npow(z[6],2);
    z[28]=z[7]*z[1];
    z[31]=npow(z[28],3);
    z[40]=z[23]*z[31];
    z[41]=z[6]*z[7];
    z[48]= - 3*z[31] + z[41];
    z[48]=z[6]*z[48];
    z[48]= - z[1] + z[48];
    z[50]=z[41]*z[9];
    z[48]=n<T>(1,2)*z[48] - z[50];
    z[48]=z[9]*z[48];
    z[40]=n<T>(1,2)*z[40] + z[48];
    z[40]=z[9]*z[40];
    z[48]=npow(z[7],2);
    z[52]=z[20]*z[48];
    z[53]=npow(z[28],4);
    z[54]=z[53] - z[52];
    z[54]=z[42]*z[23]*z[54];
    z[40]=z[40] + n<T>(1,4)*z[54];
    z[40]=z[10]*z[40];
    z[28]=npow(z[28],2);
    z[54]=n<T>(1,2)*z[6];
    z[55]=z[28]*z[54];
    z[56]=z[1] - z[55];
    z[56]=z[6]*z[56];
    z[57]=z[28] - z[41];
    z[57]=z[57]*z[6];
    z[57]=z[57] - z[38];
    z[58]=z[57]*z[32];
    z[40]=z[40] + z[58] - z[15] + z[56];
    z[40]=z[10]*z[40];
    z[56]=13*z[15];
    z[58]=z[56] + z[17];
    z[59]=npow(z[1],4);
    z[60]=z[3]*z[59];
    z[60]=n<T>(7,2)*z[22] + z[60];
    z[61]=n<T>(3,2)*z[3];
    z[60]=z[60]*z[61];
    z[47]=z[60] + n<T>(1,2)*z[58] + z[47];
    z[47]=z[47]*z[49];
    z[58]=z[22]*z[6];
    z[58]=z[58] + z[59];
    z[58]=z[58]*z[3];
    z[60]=z[15]*z[6];
    z[62]= - 9*z[22] - 5*z[60];
    z[62]=n<T>(1,2)*z[62] - z[58];
    z[62]=z[3]*z[62];
    z[63]=3*z[6];
    z[64]=z[63]*z[1];
    z[56]= - z[56] - z[64];
    z[56]=z[62] + n<T>(1,2)*z[56] - 3*z[17];
    z[43]=z[16] + z[43];
    z[43]=z[9]*z[43];
    z[62]=z[3]*npow(z[1],5);
    z[59]=n<T>(9,2)*z[59] + z[62];
    z[62]=n<T>(1,2)*z[3];
    z[59]=z[59]*z[62];
    z[43]=z[59] + n<T>(17,4)*z[22] + z[43];
    z[43]=z[5]*z[43];
    z[59]=z[31]*z[9];
    z[65]=z[38] + z[59];
    z[65]=z[9]*z[65];
    z[16]=z[16] + z[65];
    z[16]=z[9]*z[16];
    z[16]=z[22] + z[16];
    z[65]=n<T>(1,2)*z[8];
    z[16]=z[16]*z[65];
    z[16]=z[16] + n<T>(1,2)*z[56] + z[43];
    z[16]=z[16]*z[44];
    z[43]=z[6]*z[1];
    z[44]= - n<T>(3,4)*z[24] - n<T>(31,8)*z[15] + z[43];
    z[44]=z[3]*z[44];
    z[56]=n<T>(3,2)*z[9];
    z[66]=z[56]*z[28];
    z[57]=z[66] - z[57];
    z[57]=z[9]*z[57];
    z[67]=z[28]*z[6];
    z[68]=z[67] - z[1];
    z[69]=z[6]*z[68];
    z[57]=z[57] + z[36] + z[69];
    z[57]=z[57]*z[65];
    z[69]=z[7]*z[54];
    z[69]= - static_cast<T>(1)+ z[69];
    z[69]=z[9]*z[69];
    z[69]=z[69] - n<T>(15,2)*z[1] - z[6];
    z[16]=z[16] + z[57] + z[47] + z[44] + n<T>(1,4)*z[69] + z[40];
    z[16]=z[2]*z[16];
    z[40]=z[48]*z[6];
    z[44]=2*z[40];
    z[47]=n<T>(11,4)*z[7] - z[44];
    z[47]=z[6]*z[47];
    z[47]=n<T>(3,8)*z[28] + z[47];
    z[47]=z[6]*z[47];
    z[57]=z[31]*z[6];
    z[69]=z[27] - z[57];
    z[70]= - z[6]*z[69];
    z[70]= - z[36] + z[70];
    z[70]=z[70]*z[62];
    z[47]=z[70] - n<T>(9,8)*z[1] + z[47];
    z[47]=z[3]*z[47];
    z[70]=z[54]*z[48];
    z[71]=z[70] - z[7];
    z[72]=z[71]*z[6];
    z[73]=z[28] + z[72];
    z[73]=z[73]*z[54];
    z[74]= - z[31] - z[72];
    z[75]=z[23]*z[3];
    z[74]=z[74]*z[75];
    z[73]=z[73] + z[74];
    z[73]=z[5]*z[73];
    z[74]=5*z[7];
    z[76]=3*z[40];
    z[77]=z[74] - z[76];
    z[77]=z[77]*z[6];
    z[78]=n<T>(1,2) - z[77];
    z[47]=z[73] + n<T>(1,4)*z[78] + z[47];
    z[47]=z[5]*z[47];
    z[73]=n<T>(1,2)*z[7];
    z[78]=z[73] - z[40];
    z[79]=z[78]*z[63];
    z[80]= - z[30] + z[67];
    z[80]=z[3]*z[80];
    z[79]=z[80] + n<T>(7,4) + z[79];
    z[79]=z[3]*z[79];
    z[78]=z[79] - z[78];
    z[79]=z[48]*z[14];
    z[80]= - z[62]*z[79];
    z[81]=n<T>(1,4)*z[48];
    z[82]=z[3]*z[40];
    z[80]=z[80] - z[81] + z[82];
    z[80]=z[14]*z[80];
    z[82]= - z[3]*z[26];
    z[82]= - z[34] + z[82];
    z[82]=z[3]*z[82];
    z[36]=z[36] + z[24];
    z[36]=z[3]*z[36];
    z[36]=n<T>(5,2)*z[1] + 3*z[36];
    z[36]=z[5]*z[36];
    z[36]=n<T>(1,4)*z[36] + n<T>(1,2) + z[82];
    z[36]=z[2]*z[36];
    z[82]= - 3*z[28] + z[41];
    z[83]=z[27] + z[29];
    z[83]=z[2]*z[83];
    z[84]=z[57] + z[1];
    z[84]= - z[3]*z[84];
    z[82]=z[83] + n<T>(1,2)*z[82] + z[84];
    z[82]=z[5]*z[82];
    z[83]= - z[3]*z[28];
    z[73]=z[82] + z[73] + z[83];
    z[73]=z[11]*z[3]*z[73];
    z[36]=n<T>(1,4)*z[73] + z[36] + z[80] + n<T>(1,2)*z[78] + z[47];
    z[36]=z[11]*z[36];
    z[47]=z[15] - z[17];
    z[47]=z[9]*z[47];
    z[73]=z[42]*z[26];
    z[47]=2*z[47] + z[73];
    z[47]=z[10]*z[47];
    z[73]= - z[7] + n<T>(1,4)*z[40];
    z[73]=z[73]*z[6];
    z[78]=z[73] + n<T>(7,4)*z[31];
    z[78]=z[78]*z[6];
    z[78]=z[78] - z[27];
    z[78]=z[78]*z[6];
    z[78]=z[78] + z[26];
    z[80]= - z[6] + z[9];
    z[78]=z[78]*z[80];
    z[80]=2*z[7];
    z[82]=z[80] - z[70];
    z[82]=z[6]*z[82];
    z[53]= - n<T>(7,2)*z[53] + z[82];
    z[53]=z[6]*z[53];
    z[53]=z[38] + z[53];
    z[53]=z[6]*z[53];
    z[53]= - z[15] + z[53];
    z[53]=z[53]*z[75];
    z[53]=z[53] + z[78];
    z[53]=z[5]*z[53];
    z[78]= - n<T>(9,4)*z[28] - z[73];
    z[78]=z[6]*z[78];
    z[78]=4*z[1] + z[78];
    z[78]=z[6]*z[78];
    z[82]=3*z[7];
    z[83]=z[82] - z[40];
    z[84]= - z[83]*z[63];
    z[84]=7*z[28] + z[84];
    z[84]=z[6]*z[84];
    z[84]= - 17*z[1] + z[84];
    z[85]=n<T>(1,8)*z[9];
    z[84]=z[84]*z[85];
    z[86]=19*z[7] - 7*z[40];
    z[86]=z[6]*z[86];
    z[86]= - n<T>(25,2)*z[31] + z[86];
    z[86]=z[6]*z[86];
    z[87]=n<T>(21,2)*z[1];
    z[86]= - z[87] + z[86];
    z[86]=z[6]*z[86];
    z[86]=n<T>(31,2)*z[15] + z[86];
    z[86]=z[6]*z[86];
    z[86]= - n<T>(9,2)*z[22] + z[86];
    z[86]=z[3]*z[86];
    z[47]=z[53] + n<T>(1,4)*z[86] + z[47] + z[84] - z[15] + z[78];
    z[47]=z[5]*z[47];
    z[53]=z[9]*z[35];
    z[53]=n<T>(5,4)*z[53] - n<T>(7,2)*z[17] - n<T>(1,4)*z[15] + z[43];
    z[53]=z[10]*z[53];
    z[78]=n<T>(1,4)*z[9];
    z[84]=z[78] - z[54];
    z[86]=z[40] - z[7];
    z[84]=z[86]*z[84];
    z[88]=n<T>(3,2)*z[28];
    z[89]=z[6]*z[86];
    z[89]=z[88] + z[89];
    z[89]=z[6]*z[89];
    z[89]= - z[30] + z[89];
    z[89]=z[89]*z[62];
    z[84]=z[89] - static_cast<T>(1)+ z[84];
    z[84]=z[14]*z[84];
    z[89]=z[32]*z[48];
    z[90]=z[89] - z[7];
    z[90]=z[90]*z[32];
    z[73]= - z[90] + n<T>(3,4) + z[73];
    z[73]=z[73]*z[56];
    z[91]=z[80] - n<T>(5,4)*z[40];
    z[91]=z[6]*z[91];
    z[91]=n<T>(5,8)*z[28] + z[91];
    z[91]=z[6]*z[91];
    z[91]= - n<T>(9,4)*z[1] + z[91];
    z[91]=z[6]*z[91];
    z[91]=n<T>(13,8)*z[15] + z[91];
    z[91]=z[3]*z[91];
    z[92]= - static_cast<T>(1)+ n<T>(1,4)*z[41];
    z[92]=z[6]*z[92];
    z[47]=n<T>(3,2)*z[84] + z[47] + z[91] + z[53] + z[73] + n<T>(3,4)*z[1] + 
    z[92];
    z[47]=z[8]*z[47];
    z[53]=z[54]*z[1];
    z[53]=z[53] + z[15];
    z[59]=z[59] + z[57];
    z[73]= - z[1] - z[59];
    z[73]=z[73]*z[32];
    z[73]=z[73] - z[53];
    z[73]=z[9]*z[73];
    z[24]= - z[54]*z[24];
    z[24]=z[24] + z[73] - z[60] + n<T>(1,2)*z[22];
    z[24]=z[24]*z[49];
    z[73]= - z[7] - z[89];
    z[73]=z[9]*z[73];
    z[73]= - n<T>(3,2)*z[31] + z[73];
    z[73]=z[9]*z[73];
    z[27]= - z[27] + z[73];
    z[27]=z[9]*z[27];
    z[73]=z[48]*z[9];
    z[84]=z[7] + z[73];
    z[84]=z[84]*z[32];
    z[84]=z[31] + z[84];
    z[84]=z[9]*z[84];
    z[84]=z[30] + z[84];
    z[84]=z[14]*z[84];
    z[27]=z[84] - z[26] + z[27];
    z[27]=z[65]*z[27];
    z[84]=z[82] + z[73];
    z[84]=z[84]*z[78];
    z[84]=z[28] + z[84];
    z[84]=z[9]*z[84];
    z[27]=z[27] + n<T>(5,4)*z[1] + z[84];
    z[27]=z[9]*z[27];
    z[24]=z[24] + z[26] + z[27];
    z[24]=z[4]*z[24];
    z[27]= - static_cast<T>(1)+ z[90];
    z[27]=z[9]*z[27];
    z[24]= - z[24] + z[29] + z[51] - z[27];
    z[27]= - z[38] + z[57];
    z[27]=z[6]*z[27];
    z[51]=5*z[15];
    z[84]=z[9]*z[57];
    z[27]=z[84] - z[51] + z[27];
    z[27]=z[27]*z[32];
    z[84]=z[23]*z[1];
    z[89]=z[84] - z[22];
    z[90]=z[6]*z[89];
    z[91]=z[22] + z[84];
    z[91]=z[9]*z[91];
    z[90]=z[90] + z[91];
    z[91]=n<T>(1,2)*z[10];
    z[90]=z[90]*z[91];
    z[53]= - z[6]*z[53];
    z[27]=z[90] + z[27] - n<T>(3,2)*z[22] + z[53];
    z[27]=z[10]*z[27];
    z[53]= - z[66] - z[1] - z[55];
    z[53]=z[9]*z[53];
    z[27]=z[53] + z[27];
    z[27]=z[10]*z[27];
    z[53]= - z[22] - z[60];
    z[53]=3*z[53] - z[58];
    z[53]=z[53]*z[61];
    z[55]= - n<T>(1,2)*z[68] - z[50];
    z[55]=z[55]*z[56];
    z[53]=z[53] + z[55] - n<T>(13,4)*z[15] - z[64];
    z[53]=z[5]*z[53];
    z[55]=3*z[22];
    z[58]=z[15]*z[9];
    z[61]=z[55] + 5*z[58];
    z[61]=z[9]*z[61];
    z[64]= - z[22]*z[42];
    z[61]=z[61] + z[64];
    z[61]=z[10]*z[61];
    z[64]= - 9*z[15] - 7*z[17];
    z[64]=z[9]*z[64];
    z[55]=z[61] - z[55] + z[64];
    z[55]=z[10]*z[55];
    z[52]= - z[28] + n<T>(1,2)*z[52];
    z[52]=z[9]*z[52];
    z[52]= - z[1] + z[52];
    z[52]=z[14]*z[52];
    z[61]=z[48]*npow(z[9],4);
    z[52]=z[61] - z[52];
    z[52]=z[55] - 3*z[52];
    z[52]=z[52]*z[65];
    z[24]=z[52] + z[53] + z[27] - 3*z[24];
    z[24]=z[4]*z[24];
    z[27]=z[58] - z[60];
    z[29]= - z[23]*z[29];
    z[29]=n<T>(1,2)*z[27] + z[29];
    z[29]=z[8]*z[29];
    z[20]= - z[11]*z[20]*z[31]*z[49];
    z[49]=z[10]*z[27];
    z[52]=z[35]*z[75];
    z[25]=z[5]*z[25];
    z[20]=z[20] + z[29] + z[25] + z[49] + z[52];
    z[20]=z[12]*z[20];
    z[25]=n<T>(1,4)*z[10];
    z[27]=z[27]*z[25];
    z[17]=z[27] - z[17] - z[26] + z[43];
    z[17]=z[10]*z[17];
    z[26]= - z[9]*z[33];
    z[26]=z[26] + n<T>(1,4)*z[46];
    z[26]=z[10]*z[26];
    z[27]= - z[28]*z[32];
    z[29]=z[11]*z[28];
    z[27]=z[29] + z[1] + z[27];
    z[27]=z[78]*z[27];
    z[26]=z[26] + z[27];
    z[26]=z[5]*z[26];
    z[27]=z[45] + z[15] - n<T>(5,2)*z[43];
    z[29]=n<T>(1,4)*z[6];
    z[33]=z[1]*z[29];
    z[33]= - z[15] + z[33];
    z[33]=z[3]*z[6]*z[33];
    z[27]=n<T>(1,2)*z[27] + z[33];
    z[27]=z[8]*z[27];
    z[23]=z[23]*z[35];
    z[23]= - z[84] + z[23];
    z[23]=z[3]*z[23]*z[25];
    z[33]=z[1] + z[9];
    z[17]=n<T>(1,2)*z[20] + z[27] + z[23] + n<T>(1,4)*z[33] + z[17] + z[26];
    z[17]=z[12]*z[17];
    z[20]= - n<T>(5,2)*z[28] + z[41];
    z[20]=z[6]*z[20];
    z[20]=z[50] + n<T>(11,2)*z[1] + z[20];
    z[20]=z[9]*z[20];
    z[23]= - z[1] + z[59];
    z[23]=z[23]*z[42];
    z[20]=z[20] + n<T>(1,2)*z[23];
    z[20]=z[10]*z[20];
    z[23]= - static_cast<T>(1)+ z[77];
    z[23]=z[23]*z[78];
    z[26]=n<T>(7,2)*z[7] - z[40];
    z[26]=z[6]*z[26];
    z[26]= - n<T>(7,4) + z[26];
    z[26]=z[6]*z[26];
    z[20]=z[20] + z[23] + z[30] + z[26];
    z[23]= - n<T>(21,2)*z[7] + 5*z[40];
    z[23]=z[23]*z[63];
    z[23]=n<T>(25,2)*z[28] + z[23];
    z[23]=z[6]*z[23];
    z[23]=z[87] + z[23];
    z[23]=z[6]*z[23];
    z[23]= - n<T>(21,2)*z[15] + z[23];
    z[26]=z[29]*z[31];
    z[26]=z[26] - z[1];
    z[26]=z[26]*z[6];
    z[26]=z[26] + n<T>(3,4)*z[15];
    z[27]= - z[6]*z[26];
    z[22]= - n<T>(3,4)*z[22] + z[27];
    z[22]=z[3]*z[22];
    z[22]=n<T>(1,4)*z[23] + z[22];
    z[22]=z[3]*z[22];
    z[23]=z[83]*z[29];
    z[23]= - z[28] + z[23];
    z[23]=z[6]*z[23];
    z[23]=z[30] + z[23];
    z[23]=z[9]*z[23];
    z[27]= - z[6]*z[83];
    z[27]=4*z[31] + z[27];
    z[27]=z[6]*z[27];
    z[27]= - z[34] + z[27];
    z[27]=z[27]*z[75];
    z[23]=z[23] + z[27];
    z[23]=z[5]*z[23];
    z[20]=z[23] + n<T>(1,2)*z[20] + z[22];
    z[20]=z[5]*z[20];
    z[22]=7*z[7] - 15*z[40];
    z[22]=n<T>(1,2)*z[22] + 5*z[73];
    z[22]=z[9]*z[22];
    z[23]=n<T>(1,2)*z[28];
    z[27]= - z[7] + n<T>(3,2)*z[40];
    z[27]=z[6]*z[27];
    z[22]=z[22] + z[23] + z[27];
    z[27]=n<T>(9,4)*z[7];
    z[33]= - z[27] + z[40];
    z[33]=z[6]*z[33];
    z[34]=n<T>(3,4)*z[7];
    z[35]=z[34] - z[40];
    z[42]=3*z[35] + z[73];
    z[42]=z[9]*z[42];
    z[33]=z[42] + z[31] + z[33];
    z[33]=z[9]*z[33];
    z[42]=z[57] - z[1];
    z[33]=n<T>(1,2)*z[42] + z[33];
    z[33]=z[10]*z[33];
    z[22]=n<T>(1,2)*z[22] + z[33];
    z[22]=z[10]*z[22];
    z[33]=z[40] + z[7];
    z[42]=z[73] + z[33];
    z[22]=n<T>(1,2)*z[42] + z[22];
    z[22]=z[10]*z[22];
    z[42]=n<T>(3,2)*z[73];
    z[27]= - z[42] - z[27] + z[44];
    z[27]=z[9]*z[27];
    z[43]=z[83]*z[54];
    z[45]= - z[31] + z[43];
    z[27]=n<T>(1,2)*z[45] + z[27];
    z[27]=z[10]*z[27];
    z[27]=z[27] - 3*z[73] - z[80] + n<T>(9,4)*z[40];
    z[27]=z[10]*z[27];
    z[27]= - z[81] + z[27];
    z[27]=z[10]*z[27];
    z[45]= - z[40] + n<T>(3,2)*z[7];
    z[46]=n<T>(1,2)*z[45] + z[73];
    z[46]=z[10]*z[46];
    z[49]= - z[86]*z[62];
    z[46]=z[49] + n<T>(3,2)*z[48] + z[46];
    z[21]=z[21]*z[46];
    z[46]= - npow(z[10],3);
    z[37]=z[46] - z[37];
    z[37]=z[37]*z[79];
    z[21]=n<T>(1,4)*z[37] + z[21];
    z[21]=z[14]*z[21];
    z[37]= - z[71]*z[54];
    z[37]= - z[31] + z[37];
    z[37]=z[10]*z[37];
    z[37]= - n<T>(1,4)*z[33] + z[37];
    z[37]=z[10]*z[37];
    z[37]=z[81] + z[37];
    z[37]=z[3]*z[37];
    z[21]=z[21] + z[27] + z[37];
    z[21]=z[14]*z[21];
    z[27]=z[10]*z[69];
    z[27]=3*z[27] + z[23] - z[72];
    z[27]=z[10]*z[27];
    z[37]= - z[7] + z[76];
    z[27]=n<T>(1,2)*z[37] + z[27];
    z[27]=z[27]*z[62];
    z[21]=z[21] + z[27] + z[22] - n<T>(1,2)*z[48];
    z[21]=z[14]*z[21];
    z[22]= - z[7] - z[70];
    z[22]=z[22]*z[63];
    z[27]= - z[42] + z[7] + n<T>(11,2)*z[40];
    z[27]=z[9]*z[27];
    z[22]=z[27] + n<T>(13,2)*z[28] + z[22];
    z[22]=z[22]*z[32];
    z[27]=n<T>(1,4)*z[73];
    z[34]= - z[27] - z[34] + z[44];
    z[34]=z[9]*z[34];
    z[37]=z[45]*z[63];
    z[37]= - z[31] + z[37];
    z[34]=n<T>(1,2)*z[37] + z[34];
    z[34]=z[9]*z[34];
    z[30]=z[34] + z[30] - z[57];
    z[30]=z[9]*z[30];
    z[26]=z[30] + z[26];
    z[26]=z[10]*z[26];
    z[30]= - z[88] + z[41];
    z[30]=z[30]*z[54];
    z[22]=z[26] + z[22] + z[1] + z[30];
    z[22]=z[10]*z[22];
    z[26]= - z[73] - z[86];
    z[26]=z[26]*z[78];
    z[22]=z[22] + z[26] + static_cast<T>(1)- z[43];
    z[22]=z[10]*z[22];
    z[23]= - z[23] + z[41];
    z[23]=z[23]*z[54];
    z[26]=z[31]*z[54];
    z[30]=z[26] - z[1];
    z[30]=z[30]*z[6];
    z[31]= - z[15] - z[30];
    z[31]=z[31]*z[39];
    z[23]=z[31] - z[38] + z[23];
    z[23]=z[10]*z[23];
    z[31]=z[7] - n<T>(7,2)*z[40];
    z[31]=z[6]*z[31];
    z[31]= - n<T>(3,4) + z[31];
    z[23]=n<T>(1,2)*z[31] + z[23];
    z[23]=z[3]*z[23];
    z[27]= - z[27] - z[45];
    z[21]=z[21] + z[23] + n<T>(1,2)*z[27] + z[22];
    z[21]=z[14]*z[21];
    z[22]=z[74] + z[70];
    z[22]=z[6]*z[22];
    z[23]= - z[33]*z[56];
    z[22]=z[23] - n<T>(9,2)*z[28] + z[22];
    z[22]=z[22]*z[32];
    z[23]=4*z[28] - z[41];
    z[23]=z[6]*z[23];
    z[22]=z[22] - n<T>(13,4)*z[1] + z[23];
    z[22]=z[9]*z[22];
    z[23]= - z[6]*z[35];
    z[27]= - z[54]*z[73];
    z[23]=z[23] + z[27];
    z[23]=z[9]*z[23];
    z[23]=z[26] + z[23];
    z[23]=z[9]*z[23];
    z[23]= - z[30] + z[23];
    z[23]=z[9]*z[23];
    z[23]=n<T>(1,4)*z[89] + z[23];
    z[23]=z[10]*z[23];
    z[18]=z[18] + z[67];
    z[18]=z[18]*z[54];
    z[18]= - z[51] + z[18];
    z[18]=z[23] + n<T>(1,2)*z[18] + z[22];
    z[18]=z[10]*z[18];
    z[22]=z[82] - z[70];
    z[22]=z[6]*z[22];
    z[23]= - z[9]*z[7];
    z[22]=z[23] - n<T>(9,2) + z[22];
    z[22]=z[22]*z[32];
    z[18]=z[18] + z[22] + z[38] - 2*z[6];
    z[18]=z[10]*z[18];
    z[22]=z[28]*z[29];
    z[22]= - z[1] + z[22];
    z[22]=z[6]*z[22];
    z[23]= - z[84]*z[25];
    z[15]=z[23] + n<T>(5,2)*z[15] + z[22];
    z[15]=z[10]*z[15];
    z[22]= - n<T>(25,2)*z[7] + 11*z[40];
    z[22]=z[22]*z[54];
    z[22]= - static_cast<T>(3)+ z[22];
    z[22]=z[6]*z[22];
    z[22]=n<T>(15,4)*z[1] + z[22];
    z[23]= - z[3]*z[68]*z[29];
    z[15]=z[23] + n<T>(1,2)*z[22] + z[15];
    z[15]=z[3]*z[15];
    z[22]= - z[33]*z[85];

    r +=  - n<T>(5,8) + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + 
      z[21] + z[22] + n<T>(1,2)*z[24] + z[36] + z[47] - z[72];
 
    return r;
}

template double qg_2lNLC_r310(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r310(const std::array<dd_real,31>&);
#endif
