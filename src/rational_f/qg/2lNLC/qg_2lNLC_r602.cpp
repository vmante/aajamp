#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r602(const std::array<T,31>& k) {
  T z[54];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[6];
    z[4]=k[8];
    z[5]=k[9];
    z[6]=k[10];
    z[7]=k[4];
    z[8]=k[7];
    z[9]=k[15];
    z[10]=k[14];
    z[11]=k[5];
    z[12]=k[12];
    z[13]=k[13];
    z[14]=k[3];
    z[15]=3*z[7];
    z[16]=npow(z[13],2);
    z[17]= - z[16]*z[15];
    z[17]=z[17] + n<T>(1,2)*z[5] + 69*z[13];
    z[18]=n<T>(23,2)*z[5];
    z[19]=9*z[13];
    z[20]= - z[18] - z[19];
    z[21]=n<T>(1,2)*z[13];
    z[20]=z[20]*z[21];
    z[22]=npow(z[13],3);
    z[23]=9*z[22];
    z[24]=z[7]*z[23];
    z[20]=z[20] + z[24];
    z[20]=z[11]*z[20];
    z[24]=z[12]*z[11];
    z[25]=z[6]*z[2];
    z[25]=z[24] + z[25];
    z[25]=static_cast<T>(3)- 23*z[25];
    z[25]=z[9]*z[25];
    z[26]=z[11]*z[19];
    z[26]=n<T>(13,2) + z[26];
    z[26]=z[12]*z[26];
    z[27]=z[5]*z[2];
    z[28]=n<T>(23,4)*z[27];
    z[29]= - z[6]*z[28];
    z[17]=n<T>(1,4)*z[25] + z[29] + 9*z[26] + n<T>(3,2)*z[17] + z[20];
    z[17]=z[4]*z[17];
    z[20]= - z[13]*z[14];
    z[20]= - n<T>(1,2) + z[20];
    z[20]=z[20]*z[16];
    z[25]=npow(z[10],2);
    z[26]=n<T>(1,2)*z[25];
    z[29]=z[22]*z[11];
    z[22]= - z[7]*z[22];
    z[20]=z[29] + z[22] - z[26] + z[20];
    z[22]=z[10]*z[2];
    z[30]=n<T>(3,2) - z[22];
    z[30]=z[11]*z[30]*z[25];
    z[31]= - static_cast<T>(43)+ n<T>(3,2)*z[22];
    z[31]=z[10]*z[31];
    z[30]=3*z[30] + z[31] + n<T>(1,4)*z[5];
    z[31]=n<T>(23,4)*z[5];
    z[32]= - z[24]*z[31];
    z[22]=static_cast<T>(1)- n<T>(1,4)*z[22];
    z[22]=321*z[22] - z[28];
    z[22]=z[6]*z[22];
    z[22]=z[22] + 3*z[30] + z[32];
    z[22]=z[9]*z[22];
    z[28]=z[11]*z[13];
    z[18]= - z[28]*z[18];
    z[18]=z[19] + z[18];
    z[30]=n<T>(1,2)*z[12];
    z[18]=z[18]*z[30];
    z[32]=z[6]*z[10];
    z[17]=z[17] + z[22] - n<T>(357,2)*z[32] + 9*z[20] + z[18];
    z[18]=z[11] - z[7];
    z[20]=npow(z[11],2);
    z[22]=z[20]*z[12];
    z[32]=z[22] + z[18];
    z[33]=npow(z[7],2);
    z[34]=npow(z[7],3);
    z[35]= - z[6]*z[34];
    z[35]= - z[33] + z[35];
    z[36]=105*z[9];
    z[35]=z[35]*z[36];
    z[37]=z[18]*z[11];
    z[38]=npow(z[11],3);
    z[39]= - z[12]*z[38];
    z[39]= - z[37] + z[39];
    z[40]=121*z[4];
    z[39]=z[39]*z[40];
    z[41]=z[33]*z[6];
    z[32]=z[39] + z[35] + 121*z[32] - 105*z[41];
    z[32]=z[8]*z[32];
    z[35]=3*z[9];
    z[39]=n<T>(29,2)*z[7] + 35*z[41];
    z[39]=z[39]*z[35];
    z[42]=n<T>(1,2)*z[4];
    z[43]= - n<T>(121,2)*z[22] + n<T>(85,2)*z[7] - 121*z[11];
    z[43]=z[43]*z[42];
    z[44]=z[6]*z[7];
    z[45]=105*z[44];
    z[46]=n<T>(121,2) + z[45];
    z[32]=n<T>(1,4)*z[32] + z[43] + n<T>(1,2)*z[46] + z[39];
    z[39]=n<T>(1,2)*z[8];
    z[32]=z[32]*z[39];
    z[43]= - 159*z[10] + 59*z[13];
    z[28]=static_cast<T>(63)+ n<T>(257,4)*z[28];
    z[28]=z[12]*z[28];
    z[46]=z[7]*z[10];
    z[46]= - static_cast<T>(25)+ n<T>(137,4)*z[46];
    z[46]=z[6]*z[46];
    z[28]=3*z[46] + n<T>(1,4)*z[43] + z[28];
    z[43]=z[11]*z[10];
    z[43]= - n<T>(497,8)*z[44] + n<T>(35,8) - 17*z[43];
    z[43]=z[43]*z[35];
    z[46]=z[7]*z[13];
    z[46]= - static_cast<T>(91)- n<T>(53,2)*z[46];
    z[46]=5*z[46] - n<T>(1033,2)*z[24];
    z[46]=z[4]*z[46];
    z[28]=z[32] + n<T>(1,4)*z[46] + n<T>(1,2)*z[28] + z[43];
    z[28]=z[8]*z[28];
    z[17]=n<T>(1,2)*z[17] + z[28];
    z[28]=z[13]*z[5];
    z[32]=z[12]*z[13];
    z[28]=z[28] + z[32];
    z[43]= - static_cast<T>(1)+ n<T>(11,2)*z[27];
    z[46]=npow(z[5],2);
    z[43]=z[43]*z[46];
    z[28]= - z[43] - n<T>(23,8)*z[28];
    z[28]=z[6]*z[28];
    z[47]=npow(z[5],3);
    z[48]=n<T>(11,2)*z[47];
    z[49]=z[9]*z[6];
    z[50]=z[12]*z[49];
    z[51]=z[4]*z[12];
    z[52]=z[9]*z[51];
    z[28]=z[52] - n<T>(9,2)*z[50] + z[48] + z[28];
    z[28]=z[4]*z[28];
    z[29]= - n<T>(1,2)*z[16] - z[29];
    z[29]=z[12]*z[29];
    z[50]=npow(z[10],3);
    z[52]=z[7]*z[50];
    z[26]= - z[26] + z[52];
    z[26]=z[6]*z[26];
    z[26]=z[26] + z[50] + z[29];
    z[29]=z[10]*z[49];
    z[26]=3*z[26] - n<T>(53,2)*z[29];
    z[29]= - static_cast<T>(1)- z[44];
    z[29]=z[29]*z[36];
    z[52]= - z[24]*z[40];
    z[29]=z[29] + z[52];
    z[29]=z[29]*z[39];
    z[29]=z[29] + 105*z[49] - n<T>(121,2)*z[51];
    z[29]=z[29]*z[39];
    z[52]=z[13]*z[51];
    z[26]=z[29] + 3*z[26] + n<T>(355,4)*z[52];
    z[26]=z[26]*z[39];
    z[29]= - z[6]*z[31]*z[32];
    z[31]=z[47]*z[11];
    z[47]= - z[46] - z[31];
    z[47]=z[12]*z[47];
    z[52]=z[12]*z[5];
    z[53]=z[6]*z[52];
    z[47]=11*z[47] - n<T>(31,4)*z[53];
    z[47]=z[9]*z[47];
    z[53]=z[12]*z[23];
    z[29]=z[47] + z[53] + z[29];
    z[26]=z[26] + n<T>(1,2)*z[29] + z[28];
    z[26]=z[1]*z[26];
    z[28]=5*z[5];
    z[19]= - z[28] - z[19];
    z[19]=z[13]*z[19];
    z[19]=11*z[31] + 9*z[46] + z[19];
    z[19]=z[19]*z[30];
    z[29]=n<T>(9,4)*z[25];
    z[30]=n<T>(9,4)*z[52] + z[29] + z[43];
    z[30]=z[6]*z[30];
    z[19]=z[26] + z[30] + z[19] + z[23] + 9*z[50] + z[48];
    z[23]=z[24] + 1;
    z[26]=z[23]*z[42];
    z[30]=n<T>(1,2)*z[6];
    z[31]= - z[12] - z[30];
    z[26]=z[26] + n<T>(23,8)*z[31];
    z[26]=z[9]*z[26];
    z[28]= - z[28] + n<T>(9,2)*z[13];
    z[21]=z[28]*z[21];
    z[21]=z[46] + z[21];
    z[27]= - n<T>(1,4) + z[27];
    z[27]=z[5]*z[27];
    z[27]=z[27] + n<T>(13,8)*z[12];
    z[27]=z[27]*z[30];
    z[21]=z[27] + n<T>(1,2)*z[21] + 28*z[32] + z[26];
    z[21]=z[4]*z[21];
    z[26]=121*z[23] + z[45];
    z[27]=z[7] + z[41];
    z[27]=z[27]*z[36];
    z[28]=z[22] + z[11];
    z[30]= - z[28]*z[40];
    z[26]=z[30] + n<T>(1,2)*z[26] + z[27];
    z[26]=z[8]*z[26];
    z[27]= - static_cast<T>(29)- z[45];
    z[27]=z[9]*z[27];
    z[27]= - n<T>(35,2)*z[6] + z[27];
    z[30]= - static_cast<T>(103)- 121*z[24];
    z[30]=z[4]*z[30];
    z[26]=z[26] + 3*z[27] + z[30];
    z[26]=z[8]*z[26];
    z[27]= - z[25] - n<T>(1,4)*z[16];
    z[16]=z[11]*z[16];
    z[16]=z[13] + n<T>(1,8)*z[16];
    z[16]=z[12]*z[16];
    z[16]=n<T>(1,2)*z[27] + 3*z[16];
    z[25]=z[7]*z[25];
    z[25]= - 77*z[10] - 9*z[25];
    z[25]=z[6]*z[25];
    z[27]= - n<T>(59,2)*z[10] + 53*z[6];
    z[27]=z[9]*z[27];
    z[16]=n<T>(1,2)*z[27] + 3*z[16] + n<T>(1,8)*z[25];
    z[25]=n<T>(319,4)*z[13] - 117*z[12];
    z[25]=z[25]*z[42];
    z[16]=n<T>(1,8)*z[26] + 3*z[16] + z[25];
    z[16]=z[8]*z[16];
    z[25]=z[29] - z[46];
    z[26]= - z[11]*z[46];
    z[26]= - n<T>(13,8)*z[5] + z[26];
    z[26]=z[12]*z[26];
    z[27]= - n<T>(71,2)*z[10] - 3*z[5];
    z[27]=3*z[27] + n<T>(5,4)*z[12];
    z[27]=z[6]*z[27];
    z[25]=n<T>(1,4)*z[27] + n<T>(1,2)*z[25] + z[26];
    z[25]=z[9]*z[25];
    z[16]=z[16] + z[21] + z[25] + n<T>(1,2)*z[19];
    z[16]=z[1]*z[16];
    z[16]=n<T>(1,2)*z[17] + z[16];
    z[16]=z[1]*z[16];
    z[17]=n<T>(131,2)*z[23] + 45*z[44];
    z[19]=n<T>(1,2)*z[7];
    z[21]=z[19] + z[41];
    z[23]=27*z[9];
    z[21]=z[21]*z[23];
    z[19]=z[19] - z[28];
    z[25]=n<T>(113,4)*z[4];
    z[19]=z[19]*z[25];
    z[17]=z[19] + n<T>(1,4)*z[17] + z[21];
    z[17]=z[8]*z[17];
    z[15]=z[2] - z[15];
    z[15]=z[6]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[15]*z[35];
    z[15]= - n<T>(5,2)*z[6] + z[15];
    z[19]= - n<T>(1,2) + z[24];
    z[19]=z[19]*z[25];
    z[15]=9*z[15] + z[19];
    z[15]=z[16] + n<T>(1,2)*z[15] + z[17];
    z[15]=z[1]*z[15];
    z[16]= - z[33]*z[23];
    z[17]= - z[25]*z[37];
    z[16]=z[17] + z[16] + n<T>(113,4)*z[22] - 27*z[41];
    z[16]=z[16]*z[39];
    z[17]= - z[7] + n<T>(1,2)*z[2];
    z[19]= - z[17]*z[44]*z[23];
    z[20]=z[20]*z[51];
    z[16]=z[16] + z[19] + n<T>(113,8)*z[20];
    z[16]=z[16]*npow(z[1],2);
    z[19]=z[34]*z[49];
    z[20]=z[38]*z[51];
    z[19]= - 27*z[19] - n<T>(113,4)*z[20];
    z[19]=z[3]*z[19]*npow(z[1],3)*z[39];
    z[16]=z[16] + z[19];
    z[19]=npow(z[3],2);
    z[16]=z[16]*z[19];
    z[17]=z[6]*z[17];
    z[20]=z[9]*z[7];
    z[18]=z[8]*z[18];
    z[16]=z[16] + n<T>(113,8)*z[18] + n<T>(27,2)*z[20] + n<T>(113,8) - 27*z[17];
    z[15]=z[15] + n<T>(1,2)*z[16];

    r += z[19]*z[15];
 
    return r;
}

template double qg_2lNLC_r602(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r602(const std::array<dd_real,31>&);
#endif
