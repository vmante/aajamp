#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r107(const std::array<T,31>& k) {
  T z[68];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[3];
    z[4]=k[6];
    z[5]=k[9];
    z[6]=k[10];
    z[7]=k[8];
    z[8]=k[5];
    z[9]=k[12];
    z[10]=k[4];
    z[11]=k[7];
    z[12]=k[15];
    z[13]=k[14];
    z[14]=k[13];
    z[15]=n<T>(1,2)*z[3];
    z[16]=z[2] - z[3];
    z[16]=z[16]*z[15];
    z[17]=npow(z[2],2);
    z[16]=z[17] + z[16];
    z[18]=npow(z[4],2);
    z[16]=z[16]*z[18];
    z[19]=z[3]*z[2];
    z[20]=n<T>(1,2)*z[17];
    z[21]=z[20] - 5*z[19];
    z[21]=z[6]*z[21];
    z[21]=z[21] - n<T>(1,2)*z[2] - z[3];
    z[21]=z[6]*z[21];
    z[22]=z[3]*z[14];
    z[23]=3*z[22];
    z[24]=n<T>(1,2)*z[7];
    z[25]=z[24]*z[2];
    z[26]=z[9]*z[3];
    z[16]=z[21] - 3*z[26] + z[25] + z[23] + z[16];
    z[21]=n<T>(1,2)*z[12];
    z[27]=z[21] - z[24];
    z[28]=z[15]*z[18];
    z[29]=3*z[13];
    z[30]= - z[29] + z[28] + z[27];
    z[31]=npow(z[9],2);
    z[32]=n<T>(1,2)*z[31];
    z[33]=z[32]*z[8];
    z[34]= - n<T>(1,2) - z[26];
    z[34]=z[9]*z[34];
    z[30]= - z[33] + n<T>(1,2)*z[30] + z[34];
    z[30]=z[8]*z[30];
    z[34]=npow(z[8],2);
    z[35]= - z[31]*z[34];
    z[35]=z[35] + 1;
    z[35]=z[15]*z[35];
    z[36]=z[17]*z[6];
    z[37]=n<T>(3,2)*z[3];
    z[38]=z[37]*z[36];
    z[38]=z[19] + z[38];
    z[38]=z[6]*z[38];
    z[35]=z[38] + z[35];
    z[35]=z[5]*z[35];
    z[16]=z[35] + n<T>(1,2)*z[16] + z[30];
    z[16]=z[5]*z[16];
    z[30]=npow(z[7],2);
    z[35]=npow(z[12],2);
    z[38]=z[30] - z[35];
    z[39]=z[12] - z[7];
    z[40]=n<T>(3,2)*z[11];
    z[41]= - z[39]*z[40];
    z[41]=z[41] + z[38];
    z[42]=z[28] - z[9];
    z[42]=z[9]*z[42];
    z[43]=n<T>(1,2)*z[9];
    z[44]= - z[8]*z[18]*z[43];
    z[41]=z[44] + n<T>(1,2)*z[41] + z[42];
    z[41]=z[8]*z[41];
    z[42]= - z[20] + z[19];
    z[42]=z[42]*z[18];
    z[44]=z[15]*z[9];
    z[45]=z[6]*z[37];
    z[42]=z[45] + z[42] + z[44];
    z[45]=n<T>(1,2)*z[6];
    z[42]=z[42]*z[45];
    z[46]=z[18]*z[13];
    z[47]= - z[20]*z[46];
    z[48]=z[7]*z[2];
    z[49]=static_cast<T>(3)- z[48];
    z[49]=z[49]*z[24];
    z[50]=z[11]*z[3];
    z[44]=z[44] - static_cast<T>(1)- z[50];
    z[44]=z[44]*z[43];
    z[51]=n<T>(5,2)*z[12];
    z[52]=n<T>(3,2)*z[13];
    z[53]= - z[52] - z[14];
    z[16]=z[16] + z[41] + z[42] + z[44] + z[11] + z[51] + z[49] + 3*
    z[53] + z[47];
    z[16]=z[5]*z[16];
    z[41]=npow(z[4],3);
    z[42]=z[41]*z[22];
    z[44]=z[15]*z[41];
    z[47]= - z[9]*z[44];
    z[42]=z[42] + z[47];
    z[47]=n<T>(1,2)*z[8];
    z[42]=z[42]*z[47];
    z[49]= - z[13]*z[20];
    z[53]=npow(z[3],2);
    z[54]= - z[14]*z[53];
    z[49]=z[49] + z[54];
    z[49]=z[49]*z[41];
    z[27]= - z[13] + z[14] + z[27];
    z[54]=n<T>(1,2)*z[11];
    z[27]=z[27]*z[54];
    z[55]=z[53]*z[41];
    z[55]=z[55] + z[12];
    z[55]=n<T>(1,2)*z[55] - z[9];
    z[55]=z[9]*z[55];
    z[27]=z[42] + z[55] + z[49] + z[27];
    z[27]=z[8]*z[27];
    z[42]=z[17]*z[44];
    z[49]=n<T>(5,2)*z[2];
    z[49]=z[6]*z[49];
    z[42]=z[49] + z[48] - static_cast<T>(1)+ z[42];
    z[42]=z[6]*z[42];
    z[49]=npow(z[3],3)*z[41];
    z[49]= - static_cast<T>(1)+ z[49];
    z[49]=z[14]*z[49];
    z[42]=z[42] + z[11] - z[24] + z[49];
    z[49]= - z[34]*z[32];
    z[55]=z[2] - n<T>(3,2)*z[36];
    z[55]=z[6]*z[55];
    z[49]=z[49] + n<T>(1,2) + z[55];
    z[49]=z[5]*z[49];
    z[27]=z[49] + n<T>(1,2)*z[42] + z[27];
    z[27]=z[5]*z[27];
    z[42]= - z[20] - z[19];
    z[49]=npow(z[6],2);
    z[42]=z[42]*z[49]*z[44];
    z[44]=z[31]*z[8];
    z[55]=z[41]*z[44]*z[37];
    z[38]=z[11]*z[38];
    z[38]=z[38] + z[55];
    z[38]=z[38]*z[47];
    z[55]=z[54] + z[12];
    z[56]= - z[9] + z[55];
    z[56]=z[56]*z[43];
    z[57]=n<T>(1,2)*z[30];
    z[58]=n<T>(3,2)*z[14];
    z[59]=z[12] + n<T>(3,4)*z[7] - 2*z[13] - z[58];
    z[59]=z[11]*z[59];
    z[27]=z[27] + z[38] + z[42] + z[56] - z[57] + z[59];
    z[27]=z[5]*z[27];
    z[38]=z[12]*z[2];
    z[42]=z[41]*z[38];
    z[56]=z[21]*z[41];
    z[59]= - z[10]*z[56];
    z[42]=z[42] + z[59];
    z[42]=z[10]*z[42];
    z[59]= - z[17]*z[56];
    z[42]=z[59] + z[42];
    z[42]=z[10]*z[42];
    z[59]=npow(z[10],2);
    z[60]=z[15]*z[11];
    z[61]= - z[41]*z[59]*z[60];
    z[42]=z[61] - z[7] + z[42];
    z[61]= - z[24] - z[12];
    z[61]=z[10]*z[61];
    z[61]=n<T>(1,4) + z[61];
    z[61]=z[9]*z[61];
    z[42]=n<T>(1,2)*z[42] + z[61];
    z[42]=z[6]*z[42];
    z[61]=z[2] - z[10];
    z[62]=3*z[10];
    z[61]=z[62]*z[61];
    z[61]= - z[17] + z[61];
    z[63]=z[35]*z[41];
    z[61]=z[10]*z[63]*z[61];
    z[56]= - z[59]*z[56];
    z[56]= - z[35] + z[56];
    z[56]=z[10]*z[56];
    z[64]=z[21] - z[13];
    z[56]=3*z[64] + z[56];
    z[56]=z[11]*z[56];
    z[56]=z[61] + z[56];
    z[61]= - z[7] - z[21];
    z[61]=z[10]*z[61];
    z[61]=n<T>(1,4) + z[61];
    z[61]=z[9]*z[61];
    z[61]=n<T>(1,4)*z[11] + z[61];
    z[61]=z[9]*z[61];
    z[42]=z[42] + n<T>(1,2)*z[56] + z[61];
    z[42]=z[6]*z[42];
    z[56]=z[54]*z[9];
    z[61]=z[11]*z[24];
    z[61]= - z[56] - z[30] + z[61];
    z[61]=z[9]*z[61];
    z[64]=z[30]*z[11];
    z[61]=z[64] + z[61];
    z[61]=z[8]*z[41]*z[61];
    z[65]=z[41]*z[10];
    z[66]= - z[64]*z[65];
    z[67]=z[41]*z[50];
    z[65]= - z[7]*z[65];
    z[65]=z[65] + z[67];
    z[32]=z[65]*z[32];
    z[32]=z[61] + z[66] + z[32];
    z[32]=z[32]*z[47];
    z[41]= - z[41]*z[57];
    z[41]=z[41] + z[63];
    z[41]=z[11]*z[41]*z[59];
    z[56]=z[30]*z[56];
    z[32]=z[32] + z[41] + z[56];
    z[32]=z[8]*z[32];
    z[41]=z[24] - z[14];
    z[56]=3*z[11];
    z[41]=z[41]*z[56];
    z[55]= - z[9]*z[55];
    z[41]=z[55] - z[30] + z[41];
    z[41]=z[9]*z[41];
    z[55]=npow(z[10],3)*z[63];
    z[55]= - z[35] + z[55];
    z[55]=z[55]*z[56];
    z[41]=z[55] + z[41];
    z[32]=z[32] + n<T>(1,2)*z[41] + z[42];
    z[41]=z[21]*z[31];
    z[42]=npow(z[4],4);
    z[55]=z[11]*z[42]*z[35]*npow(z[10],4);
    z[59]=z[12] + z[7];
    z[59]=z[59]*z[10];
    z[61]= - z[9]*z[59];
    z[61]= - z[7] + z[61];
    z[61]=z[6]*z[61]*z[43];
    z[55]=z[61] + z[55] - z[41];
    z[55]=z[6]*z[55];
    z[61]=z[9]*z[42]*npow(z[8],4)*z[64];
    z[55]=z[55] + z[61];
    z[42]=z[42]*z[53];
    z[42]=n<T>(1,4)*z[42];
    z[49]=z[17]*z[49]*z[42];
    z[36]=z[24]*z[36];
    z[36]= - z[48] + z[36];
    z[36]=z[6]*z[36];
    z[34]=z[34]*z[41];
    z[34]=z[34] + z[24] + z[36];
    z[34]=z[5]*z[34];
    z[36]=z[44]*z[42];
    z[31]=z[12]*z[31];
    z[31]=z[31] + z[36];
    z[31]=z[8]*z[31];
    z[31]=z[34] + z[49] + z[31];
    z[31]=z[5]*z[31];
    z[31]=z[41] + z[31];
    z[31]=z[5]*z[31];
    z[31]=n<T>(1,4)*z[55] + z[31];
    z[31]=z[1]*z[31];
    z[27]=z[31] + n<T>(1,2)*z[32] + z[27];
    z[27]=z[1]*z[27];
    z[31]=z[18]*z[7];
    z[32]= - z[10]*z[31];
    z[32]= - z[30] + z[32];
    z[28]=n<T>(7,8)*z[7] - z[14] - z[28];
    z[28]=z[11]*z[28];
    z[34]=n<T>(1,8)*z[9];
    z[36]= - z[12] - z[56];
    z[36]=z[36]*z[34];
    z[28]=z[36] + n<T>(3,4)*z[32] + z[28];
    z[28]=z[9]*z[28];
    z[32]=n<T>(1,2)*z[18];
    z[36]=z[32]*z[35];
    z[41]=n<T>(1,4)*z[31];
    z[42]=z[14]*z[18];
    z[42]=z[42] + z[41];
    z[42]=z[7]*z[42];
    z[42]=z[42] - z[36];
    z[42]=z[10]*z[42];
    z[49]=z[57] - z[35];
    z[55]=z[24]*z[18];
    z[56]=z[18]*z[12];
    z[57]=z[55] - z[56];
    z[61]=n<T>(1,2)*z[10];
    z[57]=z[57]*z[61];
    z[57]= - 3*z[49] + z[57];
    z[57]=z[57]*z[54];
    z[63]=3*z[12];
    z[65]= - z[63]*z[46];
    z[66]=z[11]*z[55];
    z[31]= - z[14]*z[31];
    z[31]=z[66] + z[31] + z[65];
    z[64]=3*z[64];
    z[65]= - z[55] + z[64];
    z[66]=z[9]*z[18];
    z[65]=n<T>(1,2)*z[65] + z[66];
    z[65]=z[9]*z[65];
    z[31]=n<T>(1,2)*z[31] + z[65];
    z[31]=z[31]*z[47];
    z[47]= - z[38]*z[46];
    z[28]=z[31] + z[28] + z[57] + z[47] + z[42];
    z[28]=z[8]*z[28];
    z[31]= - z[14]*z[55];
    z[42]= - z[18]*z[35];
    z[31]=z[31] + z[42];
    z[31]=z[10]*z[31];
    z[36]=z[2]*z[36];
    z[31]=z[36] + z[31];
    z[31]=z[31]*z[62];
    z[36]= - z[41] - z[56];
    z[36]=z[10]*z[36];
    z[36]=n<T>(9,2)*z[35] + z[36];
    z[36]=z[10]*z[36];
    z[41]=5*z[13];
    z[42]=3*z[14];
    z[36]=z[36] - 5*z[12] - n<T>(7,4)*z[7] + z[41] + z[42];
    z[36]=z[11]*z[36];
    z[46]= - z[17]*z[46];
    z[29]= - z[29] + z[46];
    z[29]=n<T>(1,2)*z[29] - z[63];
    z[29]=z[12]*z[29];
    z[46]= - 3*z[7] - z[21];
    z[46]=z[10]*z[46];
    z[46]=z[60] + static_cast<T>(3)+ z[46];
    z[46]=z[9]*z[46];
    z[46]=z[46] + z[11] - z[39];
    z[46]=z[46]*z[43];
    z[47]= - z[58] + z[7];
    z[47]=z[7]*z[47];
    z[29]=z[46] + z[36] + z[31] + z[47] + z[29];
    z[31]=z[18]*z[2];
    z[36]= - z[10]*z[32];
    z[36]=z[36] + z[31] - z[63];
    z[36]=z[10]*z[36];
    z[46]=z[3] + n<T>(3,2)*z[2];
    z[47]=z[3]*z[46];
    z[47]= - z[20] + z[47];
    z[47]=z[47]*z[18];
    z[25]= - z[60] + z[36] - z[25] + n<T>(1,2) + z[47];
    z[25]=z[6]*z[25];
    z[31]= - n<T>(5,2)*z[31] - z[63];
    z[31]=z[12]*z[31];
    z[36]=z[62]*z[56];
    z[31]=z[31] + z[36];
    z[31]=z[10]*z[31];
    z[25]=z[31] + z[25];
    z[31]= - z[32] + z[35];
    z[31]=z[10]*z[31];
    z[18]=z[3]*z[18];
    z[18]=z[18] - z[31];
    z[18]= - n<T>(7,8)*z[12] + z[13] - n<T>(3,4)*z[18];
    z[18]=z[10]*z[18];
    z[18]=n<T>(3,8) + z[18];
    z[18]=z[11]*z[18];
    z[31]=z[50] - n<T>(1,2);
    z[32]= - 5*z[59] - z[31];
    z[36]=n<T>(1,4)*z[9];
    z[32]=z[32]*z[36];
    z[18]=z[32] + z[18] + n<T>(9,8)*z[12] - z[13] - n<T>(1,8)*z[7] + n<T>(1,4)*z[25]
   ;
    z[18]=z[6]*z[18];
    z[16]=z[27] + z[16] + z[28] + n<T>(1,2)*z[29] + z[18];
    z[16]=z[1]*z[16];
    z[18]= - z[17] + n<T>(5,2)*z[19];
    z[18]=z[6]*z[18]*z[15];
    z[19]= - n<T>(1,4)*z[2] + z[3];
    z[19]=z[3]*z[19];
    z[17]=z[18] + z[19] - n<T>(1,4)*z[17];
    z[17]=z[6]*z[17];
    z[18]=z[2]*z[13];
    z[19]= - z[18] + z[22];
    z[25]=z[53]*z[9];
    z[27]= - 15*z[3] + z[25];
    z[27]=z[27]*z[43];
    z[19]=3*z[19] + z[27];
    z[27]= - z[3]*z[44];
    z[19]=n<T>(1,2)*z[19] + z[27];
    z[19]=z[8]*z[19];
    z[27]= - z[6]*z[20];
    z[27]= - z[2] + z[27];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(3,2) + z[27];
    z[27]=z[53]*z[27];
    z[28]= - z[53]*z[33];
    z[25]= - 2*z[25] + z[28];
    z[25]=z[8]*z[25];
    z[25]=z[25] + z[27];
    z[25]=z[5]*z[25];
    z[27]=z[53]*z[43];
    z[23]= - n<T>(5,2) - z[23];
    z[23]=z[3]*z[23];
    z[23]=z[27] + z[2] + z[23];
    z[17]=z[25] + z[19] + n<T>(1,2)*z[23] + z[17];
    z[17]=z[5]*z[17];
    z[19]= - z[42] - z[39];
    z[23]= - n<T>(5,4) + z[26];
    z[23]=z[9]*z[23];
    z[19]= - z[33] + n<T>(1,2)*z[19] + z[23];
    z[19]=z[8]*z[19];
    z[15]=z[14]*z[15];
    z[15]=z[15] - n<T>(1,4) - z[18];
    z[23]=z[2] - z[37];
    z[23]=z[3]*z[23];
    z[20]=z[20] + z[23];
    z[20]=z[6]*z[20];
    z[20]=3*z[3] + z[20];
    z[20]=z[20]*z[45];
    z[23]=n<T>(3,2)*z[38];
    z[15]=z[17] + z[19] + z[20] - n<T>(1,4)*z[26] + z[23] + 3*z[15] + z[48];
    z[15]=z[5]*z[15];
    z[17]=n<T>(5,2)*z[38] - z[48] + n<T>(7,2) - z[18];
    z[19]=z[10]*z[12];
    z[19]=z[60] + z[19] + n<T>(1,4) - z[38];
    z[19]=z[10]*z[19];
    z[19]= - n<T>(1,2)*z[46] + z[19];
    z[19]=z[6]*z[19];
    z[20]=z[62]*z[35];
    z[20]=z[20] + z[13];
    z[23]= - static_cast<T>(5)- z[23];
    z[23]=z[12]*z[23];
    z[23]=z[23] - z[7] + z[20];
    z[23]=z[10]*z[23];
    z[20]=z[51] - z[20];
    z[20]=z[10]*z[20];
    z[20]=n<T>(1,2) + z[20];
    z[20]=z[20]*z[61];
    z[20]=z[3] + z[20];
    z[20]=z[11]*z[20];
    z[17]=z[19] + z[20] + n<T>(1,2)*z[17] + z[23];
    z[17]=z[17]*z[45];
    z[19]= - z[14] + n<T>(5,2)*z[7];
    z[19]=z[11]*z[19];
    z[20]= - z[9]*z[40];
    z[19]=z[20] - 3*z[30] + z[19];
    z[19]=z[9]*z[19];
    z[20]=z[8]*z[9];
    z[20]=z[20] + 1;
    z[20]=z[64]*z[20];
    z[19]=z[19] + z[20];
    z[19]=z[8]*z[19];
    z[20]=z[49]*z[62];
    z[20]=z[20] + n<T>(3,2)*z[12] - n<T>(1,4)*z[7] + z[13] - z[58];
    z[20]=z[20]*z[54];
    z[23]= - z[10]*z[7];
    z[23]=z[60] + n<T>(9,4) + z[23];
    z[23]=z[9]*z[23];
    z[25]= - z[12] + z[40];
    z[23]=n<T>(1,2)*z[25] + z[23];
    z[23]=z[23]*z[43];
    z[25]= - z[14] - z[7];
    z[25]=z[7]*z[25];
    z[26]= - z[52] + z[12];
    z[26]=z[12]*z[26];
    z[19]=n<T>(1,4)*z[19] + z[23] + z[20] + z[25] + z[26];
    z[19]=z[8]*z[19];
    z[20]= - n<T>(21,2) + z[48];
    z[20]=z[20]*z[24];
    z[20]=z[20] + z[41] + n<T>(21,2)*z[14];
    z[23]= - n<T>(3,4)*z[38] - n<T>(9,4) - z[18];
    z[23]=z[12]*z[23];
    z[25]=z[42] - z[24];
    z[25]=z[25]*z[24];
    z[25]=z[25] + 3*z[35];
    z[25]=z[10]*z[25];
    z[26]=n<T>(5,4)*z[7] - z[13] - z[58];
    z[27]=z[10]*z[35];
    z[26]= - n<T>(9,4)*z[27] + n<T>(1,2)*z[26] + 2*z[12];
    z[26]=z[10]*z[26];
    z[26]= - n<T>(3,4) + z[26];
    z[26]=z[11]*z[26];
    z[27]= - 7*z[7] - z[63];
    z[27]=z[10]*z[27];
    z[27]=z[27] - z[31];
    z[27]=z[27]*z[36];
    z[15]=z[16] + z[15] + z[19] + z[17] + z[27] + z[26] + z[25] + n<T>(1,2)*
    z[20] + z[23];
    z[15]=z[1]*z[15];
    z[16]= - n<T>(5,2)*z[3] - z[10];
    z[16]=z[16]*z[54];
    z[17]=z[21] + n<T>(3,2)*z[7] - n<T>(1,2)*z[13] - 5*z[14];
    z[17]=z[10]*z[17];
    z[19]= - z[2] - z[8];
    z[19]=z[5]*z[19];
    z[16]=z[19] + z[16] + z[17] + z[38] - z[48] - n<T>(9,2)*z[22] + n<T>(13,4)
    + z[18];
    z[17]=z[34] + n<T>(1,8)*z[11] + z[12] - z[24] - n<T>(1,4)*z[13] + 2*z[14];
    z[17]=z[8]*z[17];
    z[18]=z[2] - n<T>(3,8)*z[10];
    z[18]=z[6]*z[18];

    r += z[15] + n<T>(1,2)*z[16] + z[17] + z[18];
 
    return r;
}

template double qg_2lNLC_r107(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r107(const std::array<dd_real,31>&);
#endif
