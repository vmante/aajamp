#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r148(const std::array<T,31>& k) {
  T z[137];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[9];
    z[4]=k[14];
    z[5]=k[10];
    z[6]=k[12];
    z[7]=k[15];
    z[8]=k[13];
    z[9]=k[2];
    z[10]=k[5];
    z[11]=k[6];
    z[12]=k[4];
    z[13]=k[7];
    z[14]=k[3];
    z[15]=k[27];
    z[16]=npow(z[2],2);
    z[17]=n<T>(1,2)*z[13];
    z[18]=z[16]*z[17];
    z[19]=z[8]*z[14];
    z[20]=n<T>(1,2)*z[6];
    z[21]= - z[14]*z[20];
    z[21]=z[19] + z[21];
    z[22]=npow(z[11],3);
    z[21]=z[21]*z[22];
    z[23]=4*z[6];
    z[24]= - z[17] - z[23];
    z[25]=3*z[7];
    z[24]=z[24]*z[25];
    z[26]=npow(z[6],2);
    z[24]=z[26] + z[24];
    z[24]=z[7]*z[24];
    z[27]=2*z[6];
    z[28]= - z[7]*z[27];
    z[28]=z[16] + z[28];
    z[28]=z[3]*z[28];
    z[21]=z[28] + z[24] - z[18] + z[21];
    z[21]=z[3]*z[21];
    z[24]=z[22]*z[14];
    z[28]=z[26]*z[24];
    z[21]=z[28] + z[21];
    z[21]=z[3]*z[21];
    z[28]=3*z[9];
    z[29]= - z[22]*z[28];
    z[30]=5*z[6];
    z[31]=2*z[3];
    z[32]=z[30] + z[31];
    z[33]=z[13] + z[32];
    z[33]=z[3]*z[33];
    z[29]=z[29] + z[33];
    z[29]=z[3]*z[29];
    z[33]=npow(z[8],2);
    z[34]=z[30]*z[33];
    z[35]=n<T>(13,2)*z[9];
    z[36]=z[22]*z[35];
    z[36]=z[33] + z[36];
    z[36]=z[7]*z[36];
    z[37]=z[7]*z[8];
    z[38]= - z[3]*z[13];
    z[38]=z[37] + z[38];
    z[39]=6*z[4];
    z[38]=z[38]*z[39];
    z[29]=z[38] + z[29] + z[34] + z[36];
    z[36]=npow(z[4],2);
    z[29]=z[29]*z[36];
    z[38]=npow(z[3],3);
    z[40]=z[38]*z[27];
    z[41]= - z[22]*z[20];
    z[42]=npow(z[3],2);
    z[43]=z[42]*z[4];
    z[44]=2*z[13];
    z[45]= - z[44]*z[43];
    z[41]=z[45] + z[41] + z[40];
    z[41]=z[41]*z[36];
    z[45]=npow(z[8],3);
    z[46]= - z[13]*z[45]*z[26];
    z[47]=npow(z[13],2);
    z[48]=3*z[47];
    z[49]=z[2]*z[13];
    z[50]= - z[48] + n<T>(7,2)*z[49];
    z[51]=z[44] + z[2];
    z[51]=z[6]*z[51];
    z[51]=z[51] - z[50];
    z[51]=z[6]*z[51];
    z[52]=z[47]*z[2];
    z[51]= - z[52] + z[51];
    z[51]=z[51]*z[22];
    z[53]=z[38]*z[6];
    z[54]=npow(z[7],2);
    z[55]=z[54]*z[53];
    z[41]=z[41] - 3*z[55] + z[46] + z[51];
    z[41]=z[10]*z[41];
    z[46]=n<T>(1,2)*z[47];
    z[51]=z[8]*z[13];
    z[55]=z[46] + z[51];
    z[55]=z[55]*z[33];
    z[56]=z[33]*z[6];
    z[57]=z[8] + z[13];
    z[58]=z[57]*z[56];
    z[55]=z[55] + z[58];
    z[55]=z[6]*z[55];
    z[58]=2*z[14];
    z[59]= - z[45]*z[58];
    z[60]=z[58]*z[47];
    z[61]=z[13]*z[14];
    z[62]= - z[6]*z[61];
    z[62]= - z[60] + z[62];
    z[62]=z[6]*z[62];
    z[59]=z[59] + z[62];
    z[59]=z[59]*z[22];
    z[62]=z[36]*z[20];
    z[63]=n<T>(3,2)*z[2];
    z[64]= - z[63] - z[8];
    z[64]=z[64]*z[33];
    z[64]=z[52] + z[64];
    z[65]=z[26]*z[2];
    z[64]= - z[62] + 3*z[64] + z[65];
    z[64]=z[22]*z[64];
    z[66]=z[22]*z[7];
    z[67]=z[47]*z[66];
    z[64]=z[67] + z[64];
    z[64]=z[12]*z[64];
    z[67]=3*z[6];
    z[68]=z[16]*z[67];
    z[69]=z[6]*z[2];
    z[70]=z[69]*z[7];
    z[68]=z[68] + z[70];
    z[68]=z[68]*z[54];
    z[21]=z[41] + z[64] + z[29] + z[21] + n<T>(1,2)*z[68] + z[55] + z[59];
    z[21]=z[10]*z[21];
    z[29]=npow(z[9],2);
    z[41]=z[22]*z[29];
    z[55]=5*z[41];
    z[59]=7*z[13];
    z[64]=4*z[9];
    z[68]=z[64]*z[3];
    z[71]=static_cast<T>(5)- z[68];
    z[71]=z[3]*z[71];
    z[71]=z[71] + z[55] + z[59] + z[30];
    z[71]=z[3]*z[71];
    z[72]=3*z[8];
    z[55]= - z[72] + z[55];
    z[55]=z[7]*z[55];
    z[73]= - z[13] + z[8];
    z[39]=z[73]*z[39];
    z[73]=10*z[6];
    z[74]= - z[8]*z[73];
    z[39]=z[39] + z[71] + z[55] + z[74] + z[47] + 6*z[33];
    z[39]=z[4]*z[39];
    z[55]= - 6*z[41] - z[44] - z[30];
    z[55]=z[55]*z[42];
    z[71]=7*z[56];
    z[39]=z[39] + z[71] + z[55];
    z[39]=z[4]*z[39];
    z[55]=z[49] - z[47];
    z[74]=n<T>(1,2)*z[2];
    z[75]=z[55]*z[74];
    z[76]=2*z[8];
    z[77]=z[76] + z[17];
    z[78]= - z[77]*z[33];
    z[79]=z[6]*z[8];
    z[80]= - z[57]*z[79];
    z[75]=z[80] + z[75] + z[78];
    z[75]=z[6]*z[75];
    z[78]=z[2]*z[9];
    z[80]=3*z[78];
    z[81]=static_cast<T>(2)- z[80];
    z[81]=z[3]*z[81];
    z[81]=z[81] - z[17] + z[2];
    z[81]=z[2]*z[81];
    z[82]=npow(z[14],2);
    z[83]=z[76]*z[82];
    z[84]= - z[22]*z[83];
    z[85]=n<T>(3,2)*z[13] + z[27];
    z[85]=z[7]*z[85];
    z[81]=z[85] + z[84] + z[51] + z[81];
    z[81]=z[3]*z[81];
    z[84]=6*z[6];
    z[85]=n<T>(5,2)*z[13];
    z[86]= - z[85] - z[84];
    z[86]=z[86]*z[25];
    z[87]=2*z[26];
    z[86]=z[87] + z[86];
    z[86]=z[7]*z[86];
    z[18]=z[81] + z[18] + z[86];
    z[18]=z[3]*z[18];
    z[81]=z[74] - z[6];
    z[81]=z[7]*z[81];
    z[81]=z[81] + z[16] - 4*z[69];
    z[81]=z[7]*z[81];
    z[86]=n<T>(5,2)*z[16];
    z[88]=z[2]*z[27];
    z[88]= - z[86] + z[88];
    z[88]=z[6]*z[88];
    z[81]=z[88] + z[81];
    z[81]=z[7]*z[81];
    z[88]=z[66]*z[17];
    z[89]=z[22]*z[48];
    z[89]=z[89] - z[88];
    z[89]=z[7]*z[89];
    z[90]=5*z[2];
    z[91]=z[90] + z[76];
    z[91]=z[91]*z[33];
    z[92]=2*z[52];
    z[91]=z[92] + z[91];
    z[91]=z[91]*z[22];
    z[89]=z[91] + z[89];
    z[89]=z[12]*z[89];
    z[24]= - z[26] + 6*z[24];
    z[24]=z[45]*z[24];
    z[24]=z[89] + z[24];
    z[24]=z[12]*z[24];
    z[89]=z[82]*z[22];
    z[91]=z[45]*z[89];
    z[18]=z[21] + z[24] + z[39] + z[18] + z[81] + z[75] + 4*z[91];
    z[18]=z[10]*z[18];
    z[21]=2*z[5];
    z[24]= - z[21] + z[74];
    z[24]=z[24]*z[69];
    z[39]=z[2]*z[5];
    z[75]=3*z[2];
    z[81]= - z[5] + z[75];
    z[81]=z[6]*z[81];
    z[81]=z[39] + z[81];
    z[91]=n<T>(1,2)*z[7];
    z[81]=z[81]*z[91];
    z[24]=z[24] + z[81];
    z[24]=z[24]*z[54];
    z[81]= - static_cast<T>(1)+ 2*z[78];
    z[93]= - z[81]*z[31];
    z[93]=z[90] + z[93];
    z[93]=z[93]*z[42];
    z[93]=z[93] - 2*z[45] + z[34];
    z[93]=z[4]*z[93];
    z[94]=z[45]*z[6];
    z[95]=z[38]*z[2];
    z[94]=z[94] + z[95];
    z[93]=4*z[94] + z[93];
    z[93]=z[4]*z[93];
    z[95]=z[27]*z[45];
    z[96]= - z[36]*z[95];
    z[97]=z[42]*z[29];
    z[98]=npow(z[10],2);
    z[99]= - npow(z[11],5)*npow(z[4],3)*z[98]*z[97];
    z[96]=z[96] + z[99];
    z[96]=z[10]*z[96];
    z[99]=npow(z[7],3);
    z[100]=z[6]*z[5];
    z[101]=z[99]*z[74]*z[100];
    z[102]=2*z[36];
    z[94]= - z[94]*z[102];
    z[94]=z[101] + z[94];
    z[94]=z[1]*z[94];
    z[24]=z[94] + 2*z[96] + z[24] + z[93];
    z[24]=z[1]*z[24];
    z[93]=z[33]*z[4];
    z[34]= - z[93] - z[38] - z[45] + z[34];
    z[34]=z[4]*z[34];
    z[34]=z[95] + z[34];
    z[94]=2*z[4];
    z[34]=z[34]*z[94];
    z[96]=npow(z[12],2);
    z[101]=z[45]*z[2];
    z[103]= - z[96]*z[101];
    z[104]= - z[6]*z[49];
    z[104]= - z[52] + z[104];
    z[98]=z[6]*z[104]*z[98];
    z[98]=z[98] + z[103];
    z[103]=npow(z[11],4);
    z[98]=z[103]*z[98];
    z[97]=z[103]*z[97];
    z[104]=2*z[33];
    z[105]= - z[4]*z[7]*z[104];
    z[97]=z[105] - z[95] + 5*z[97];
    z[97]=z[97]*z[36];
    z[97]=z[97] + z[98];
    z[97]=z[10]*z[97];
    z[98]=z[6]*z[16];
    z[70]=z[98] + z[70];
    z[70]=z[70]*z[54];
    z[34]=z[97] + n<T>(3,2)*z[70] + z[34];
    z[34]=z[10]*z[34];
    z[70]=n<T>(3,2)*z[6];
    z[97]=n<T>(1,2)*z[5];
    z[98]=z[5]*z[9];
    z[105]=n<T>(1,2)*z[98];
    z[106]=z[105] + 1;
    z[107]=z[2]*z[106];
    z[107]= - z[70] - z[97] + z[107];
    z[107]=z[7]*z[107];
    z[108]=3*z[5];
    z[109]=z[108] - z[2];
    z[110]= - z[109]*z[74];
    z[111]=n<T>(3,2)*z[5];
    z[112]=z[111] - z[90];
    z[112]=z[6]*z[112];
    z[107]=z[107] + z[110] + z[112];
    z[107]=z[7]*z[107];
    z[110]=n<T>(15,4)*z[5] - z[2];
    z[110]=z[2]*z[110];
    z[110]=z[110] + z[69];
    z[110]=z[6]*z[110];
    z[107]=z[110] + z[107];
    z[107]=z[7]*z[107];
    z[110]=2*z[2];
    z[112]= - z[30] - z[110] + z[8];
    z[112]=z[8]*z[112];
    z[113]=2*z[9];
    z[114]=z[29]*z[2];
    z[115]=z[113] - z[114];
    z[115]=z[115]*z[31];
    z[115]=z[115] - static_cast<T>(5)+ 6*z[78];
    z[115]=z[3]*z[115];
    z[115]= - z[90] + z[115];
    z[115]=z[3]*z[115];
    z[116]=n<T>(1,2)*z[49];
    z[112]=z[115] + z[116] + z[112];
    z[112]=z[4]*z[112];
    z[115]=z[8] - z[5];
    z[117]=z[115]*z[104];
    z[81]=z[3]*z[81];
    z[81]= - z[110] + z[81];
    z[81]=z[81]*z[42];
    z[81]=z[112] + 2*z[81] + z[52] + z[117];
    z[81]=z[4]*z[81];
    z[112]=z[47]*z[5];
    z[96]=z[7]*z[103]*z[96]*z[112];
    z[103]= - z[5]*z[93];
    z[103]= - z[101] + z[103];
    z[103]=z[103]*z[102];
    z[96]=z[103] + z[96];
    z[96]=z[12]*z[96];
    z[103]=z[20]*z[5];
    z[99]= - z[99]*z[103];
    z[117]=z[2] + z[5];
    z[118]=2*z[117] + z[8];
    z[118]=z[118]*z[33];
    z[93]=z[118] + z[93];
    z[93]=z[93]*z[102];
    z[93]=z[96] + z[99] + z[93];
    z[93]=z[12]*z[93];
    z[38]= - z[38]*z[110];
    z[24]=z[24] + z[34] + z[93] + z[81] + z[38] - z[95] + z[107];
    z[24]=z[1]*z[24];
    z[34]=n<T>(1,2)*z[14];
    z[38]=npow(z[5],2);
    z[81]= - z[38]*z[34];
    z[93]=z[5]*z[14];
    z[95]=z[93]*z[44];
    z[81]=z[81] + z[95];
    z[81]=z[13]*z[81];
    z[95]=n<T>(1,2)*z[93];
    z[96]=z[95] - z[19];
    z[96]=z[96]*z[33];
    z[81]=z[81] + z[96];
    z[81]=z[81]*z[22];
    z[66]= - z[38]*z[113]*z[66];
    z[96]=z[13] + z[5];
    z[99]=3*z[13];
    z[102]=z[96]*z[99];
    z[102]=z[38] + z[102];
    z[102]=z[102]*z[22];
    z[88]=z[102] - z[88];
    z[88]=z[7]*z[88];
    z[102]=z[22]*z[112];
    z[88]=z[102] + z[88];
    z[88]=z[12]*z[88];
    z[102]=z[26]*z[33];
    z[107]=z[97] + z[8];
    z[118]=z[107]*z[102];
    z[119]=5*z[5] + z[2];
    z[119]=z[119]*z[33];
    z[119]=z[112] + z[119];
    z[119]=z[4]*z[119];
    z[119]=6*z[101] + z[119];
    z[119]=z[4]*z[119];
    z[66]=z[88] + z[119] + z[66] + z[118] + z[81];
    z[66]=z[12]*z[66];
    z[81]=6*z[8];
    z[88]= - z[81] - z[108] - z[110];
    z[88]=z[8]*z[88];
    z[118]=z[21] + z[13];
    z[119]=z[118]*z[44];
    z[120]= - z[13] - z[72];
    z[120]=z[120]*z[94];
    z[88]=z[120] + z[103] + z[88] + z[119] - z[49];
    z[88]=z[4]*z[88];
    z[103]=10*z[5];
    z[119]= - z[103] - 9*z[2];
    z[119]=z[119]*z[33];
    z[88]=z[88] - z[92] + z[119];
    z[88]=z[4]*z[88];
    z[119]= - z[38]*z[74];
    z[120]=3*z[33];
    z[107]=z[107]*z[120];
    z[121]= - z[5]*z[74];
    z[122]=z[111] + z[8];
    z[122]=z[8]*z[122];
    z[121]=z[121] + z[122];
    z[121]=z[6]*z[121];
    z[107]=z[121] + z[119] + z[107];
    z[107]=z[6]*z[107];
    z[119]=z[5] - z[72];
    z[89]=z[33]*z[119]*z[89];
    z[119]= - z[13]*z[111];
    z[119]=z[119] - z[100];
    z[121]= - z[5] - z[6];
    z[121]=z[121]*z[91];
    z[119]=3*z[119] + z[121];
    z[119]=z[7]*z[119];
    z[41]=z[38]*z[41];
    z[121]=n<T>(1,2)*z[112];
    z[41]=z[119] + z[121] + z[41];
    z[41]=z[7]*z[41];
    z[41]=z[66] + z[88] + z[41] + z[107] + z[89];
    z[41]=z[12]*z[41];
    z[66]=4*z[13];
    z[88]=5*z[78];
    z[89]= - static_cast<T>(1)+ z[88];
    z[89]=z[3]*z[89];
    z[89]=z[89] - z[30] - z[66] + z[75];
    z[89]=z[3]*z[89];
    z[107]=z[31]*z[29];
    z[119]=6*z[9];
    z[122]=z[107] - z[119] + z[114];
    z[122]=z[3]*z[122];
    z[123]=4*z[78];
    z[122]=z[122] + static_cast<T>(5)- z[123];
    z[122]=z[3]*z[122];
    z[124]=n<T>(9,2)*z[6];
    z[125]=7*z[2];
    z[126]=19*z[13] + z[125];
    z[122]=z[122] + z[124] + n<T>(1,2)*z[126] + z[72];
    z[122]=z[4]*z[122];
    z[117]=z[76] + z[117];
    z[117]=z[117]*z[72];
    z[126]=7*z[8];
    z[127]= - z[111] - z[126];
    z[127]=z[6]*z[127];
    z[118]= - z[13]*z[118];
    z[89]=z[122] + z[89] + z[127] + z[117] + z[118] - n<T>(3,2)*z[49];
    z[89]=z[4]*z[89];
    z[117]=z[74]*z[9];
    z[118]=z[117] - static_cast<T>(2)- z[98];
    z[118]=z[2]*z[118];
    z[122]=z[29]*z[5];
    z[127]=z[122] + z[9];
    z[128]=z[127]*z[74];
    z[106]=z[128] - z[106];
    z[106]=z[7]*z[106];
    z[106]=z[106] - z[73] + z[118] + z[5] - n<T>(21,2)*z[13];
    z[106]=z[7]*z[106];
    z[118]=z[21] + z[17];
    z[118]=z[13]*z[118];
    z[128]=n<T>(9,2)*z[5];
    z[129]=z[128] - z[2];
    z[129]=z[129]*z[74];
    z[130]= - z[5] + z[125];
    z[130]=z[6]*z[130];
    z[106]=z[106] + n<T>(9,4)*z[130] + z[118] + z[129];
    z[106]=z[7]*z[106];
    z[118]=7*z[5];
    z[129]= - z[74] - z[118] + z[17];
    z[129]=z[2]*z[129];
    z[130]= - z[113] + z[122];
    z[131]=npow(z[9],3);
    z[132]= - z[5]*z[131];
    z[132]=3*z[29] + z[132];
    z[132]=z[2]*z[132];
    z[130]=2*z[130] + z[132];
    z[130]=z[3]*z[130];
    z[130]=z[130] - z[117] + n<T>(3,2) - 2*z[98];
    z[130]=z[2]*z[130];
    z[132]=z[122]*z[34];
    z[133]=npow(z[14],3);
    z[134]=z[8]*z[133];
    z[132]=z[132] + z[134];
    z[132]=z[132]*z[22];
    z[130]=z[132] - z[8] + z[130];
    z[130]=z[3]*z[130];
    z[132]= - z[19] + 4;
    z[132]=z[5]*z[132];
    z[132]=z[13] + z[132];
    z[132]=z[8]*z[132];
    z[134]=z[13] + z[27];
    z[134]=z[7]*z[134];
    z[129]=z[130] + 2*z[134] + z[129] + z[132];
    z[129]=z[3]*z[129];
    z[130]=z[97] - z[76];
    z[22]=z[22]*z[133]*z[130];
    z[22]=z[22] + z[108] + z[76];
    z[22]=z[33]*z[22];
    z[130]=4*z[8];
    z[132]= - z[130] + z[108] - z[13];
    z[132]=z[8]*z[132];
    z[134]= - z[2] + z[8];
    z[134]=z[6]*z[134];
    z[132]=z[134] - n<T>(37,4)*z[39] + z[132];
    z[132]=z[6]*z[132];
    z[134]= - n<T>(1,2)*z[38] - z[47];
    z[134]=z[2]*z[134];
    z[18]=z[24] + z[18] + z[41] + z[89] + z[129] + z[106] + z[132] + 
    z[134] + z[22];
    z[18]=z[1]*z[18];
    z[22]=n<T>(1,2)*z[8];
    z[24]=z[13] - z[2];
    z[24]=z[24]*z[22];
    z[24]=z[47] + z[24];
    z[24]=z[8]*z[24];
    z[41]= - 2*z[16] + z[69];
    z[41]=z[6]*z[41];
    z[89]=npow(z[11],2);
    z[106]=z[89]*z[22];
    z[129]=n<T>(1,2)*z[16] - z[69];
    z[129]=z[7]*z[129];
    z[24]=z[129] + z[106] + z[24] + z[41];
    z[24]=z[7]*z[24];
    z[41]=z[73] + z[99];
    z[73]=static_cast<T>(13)+ z[68];
    z[73]=z[3]*z[73];
    z[73]=z[73] + z[41];
    z[73]=z[3]*z[73];
    z[106]= - z[8]*z[30];
    z[129]= - z[7] - 3*z[3];
    z[129]=z[4]*z[129];
    z[73]=4*z[129] + z[73] + 5*z[37] + z[106] - n<T>(1,2)*z[89];
    z[73]=z[4]*z[73];
    z[70]=z[89]*z[70];
    z[106]= - z[33] - n<T>(17,2)*z[89];
    z[106]=z[7]*z[106];
    z[129]=17*z[6];
    z[132]=4*z[3];
    z[134]= - z[129] - z[132];
    z[134]=z[134]*z[42];
    z[70]=z[73] + z[134] + z[106] + z[71] + z[70];
    z[70]=z[4]*z[70];
    z[71]=z[32]*z[42];
    z[71]=z[71] - 3*z[43];
    z[71]=z[4]*z[71];
    z[71]= - z[40] + z[71];
    z[71]=z[71]*z[94];
    z[36]=z[36]*z[10];
    z[73]=4*z[36] + 6*z[7];
    z[73]=z[53]*z[73];
    z[102]=z[44]*z[102];
    z[106]=z[16]*z[20];
    z[54]=z[54]*z[106];
    z[54]=z[71] + z[102] + z[54] + z[73];
    z[54]=z[10]*z[54];
    z[71]=2*z[47];
    z[73]= - z[71] + z[49];
    z[73]=z[2]*z[73];
    z[102]= - z[71] + n<T>(3,2)*z[51];
    z[102]=z[8]*z[102];
    z[57]= - z[57]*z[76];
    z[57]= - z[49] + z[57];
    z[57]=z[6]*z[57];
    z[57]=z[57] + z[73] + z[102];
    z[57]=z[6]*z[57];
    z[73]=13*z[13];
    z[102]=n<T>(7,4)*z[2];
    z[134]=z[20] - z[22] + z[73] - z[102];
    z[134]=z[6]*z[134];
    z[135]=n<T>(9,2)*z[8];
    z[125]=z[125] - z[135];
    z[125]=z[8]*z[125];
    z[50]=z[134] + z[125] - z[50];
    z[50]=z[50]*z[89];
    z[125]=n<T>(3,2)*z[7];
    z[23]=z[23] - z[125];
    z[134]=5*z[7];
    z[23]=z[23]*z[134];
    z[134]=z[134] + z[110] + z[6];
    z[134]=z[134]*z[31];
    z[23]=z[134] + z[23] - z[86] - z[26];
    z[23]=z[3]*z[23];
    z[86]=z[7]*z[99];
    z[86]=z[86] - z[33] + z[26];
    z[86]=z[7]*z[86];
    z[134]=z[76] - z[6];
    z[134]=z[134]*z[89];
    z[136]=z[13]*z[16];
    z[23]=z[23] + z[86] + z[136] + 2*z[134];
    z[23]=z[3]*z[23];
    z[23]=z[54] + z[70] + z[23] + z[24] + z[57] + z[50];
    z[23]=z[10]*z[23];
    z[24]=z[99] + z[2];
    z[24]=z[24]*z[22];
    z[50]=z[89]*z[9];
    z[54]= - z[74]*z[50];
    z[57]= - static_cast<T>(1)+ z[78];
    z[57]=z[2]*z[57];
    z[57]=z[50] - 29*z[13] - z[57];
    z[57]=z[6] - n<T>(1,2)*z[57];
    z[57]=z[7]*z[57];
    z[24]=z[57] + z[54] + n<T>(75,4)*z[69] + z[24] + z[71] - z[16];
    z[24]=z[7]*z[24];
    z[54]=z[91] - z[59] + z[22];
    z[54]=z[7]*z[54];
    z[57]= - n<T>(7,2)*z[2] - z[8];
    z[57]=z[57]*z[20];
    z[70]=7*z[47];
    z[71]=z[4]*z[6];
    z[86]= - 12*z[2] + z[126];
    z[86]=z[8]*z[86];
    z[54]=z[54] - n<T>(5,2)*z[71] + z[57] + z[86] - z[70] + 5*z[49];
    z[54]=z[89]*z[54];
    z[26]=z[26]*z[104];
    z[26]=z[26] + z[54];
    z[26]=z[12]*z[26];
    z[54]=z[89]*z[34];
    z[57]=static_cast<T>(1)- z[80];
    z[57]=z[57]*z[31];
    z[86]=n<T>(7,2) + z[88];
    z[86]=z[2]*z[86];
    z[54]=z[57] + n<T>(47,2)*z[7] + z[54] + z[86] + z[67];
    z[54]=z[3]*z[54];
    z[57]=8*z[19];
    z[86]=z[6]*z[14];
    z[88]=n<T>(5,2)*z[86] - z[117] - z[57];
    z[88]=z[88]*z[89];
    z[126]=z[13] + 18*z[6];
    z[126]= - 27*z[7] + 2*z[126] + n<T>(3,2)*z[50];
    z[126]=z[7]*z[126];
    z[134]= - z[13] - z[2];
    z[134]=z[2]*z[134];
    z[54]=z[54] + z[126] + z[88] + z[134] - z[87];
    z[54]=z[3]*z[54];
    z[79]= - 8*z[79] - z[47] + z[120];
    z[88]=z[50] + z[99] + z[129];
    z[126]=z[29]*z[3];
    z[129]=z[28] - z[126];
    z[129]=z[129]*z[31];
    z[129]=static_cast<T>(25)+ z[129];
    z[129]=z[3]*z[129];
    z[25]= - 12*z[4] + z[129] + n<T>(1,2)*z[88] + z[25];
    z[25]=z[4]*z[25];
    z[88]= - z[76] - n<T>(35,2)*z[50];
    z[88]=z[7]*z[88];
    z[68]= - static_cast<T>(21)- z[68];
    z[68]=z[3]*z[68];
    z[41]= - 2*z[41] + z[68];
    z[41]=z[3]*z[41];
    z[25]=z[25] + z[41] + 2*z[79] + z[88];
    z[25]=z[4]*z[25];
    z[41]= - 15*z[13] - 13*z[8];
    z[41]=z[41]*z[22];
    z[68]=z[76] - z[2];
    z[79]=z[44] + z[68];
    z[79]=z[6]*z[79];
    z[88]=2*z[49];
    z[41]=z[79] + z[41] + z[46] - z[88];
    z[41]=z[6]*z[41];
    z[46]= - z[30]*z[61];
    z[79]=z[33]*z[14];
    z[46]=z[46] - z[60] + z[79];
    z[46]=z[46]*z[89];
    z[60]=z[13]*z[33];
    z[23]=z[23] + z[26] + z[25] + z[54] + z[24] + z[46] + z[41] + z[52]
    - n<T>(7,2)*z[60];
    z[23]=z[10]*z[23];
    z[24]=4*z[2];
    z[25]= - z[72] - z[108] - z[24];
    z[25]=z[25]*z[76];
    z[26]=z[20] + z[74] + z[97] - 8*z[13];
    z[26]=z[4]*z[26];
    z[41]=5*z[13];
    z[46]= - z[5] - z[41];
    z[46]=z[13]*z[46];
    z[54]= - z[6]*z[111];
    z[25]=z[26] + z[54] + z[25] + z[46] + n<T>(11,2)*z[49];
    z[25]=z[4]*z[25];
    z[26]=2*z[15];
    z[46]= - z[85] - z[26] + z[5];
    z[46]=z[13]*z[46];
    z[54]= - n<T>(11,2)*z[8] + n<T>(7,2)*z[5] + z[110];
    z[54]=z[8]*z[54];
    z[46]=z[54] + z[49] + z[38] + z[46];
    z[46]=z[46]*z[89];
    z[54]= - z[5]*z[59];
    z[60]=z[118] - z[2];
    z[60]=z[8]*z[60];
    z[54]=z[60] + z[54] + z[116];
    z[54]=z[4]*z[54];
    z[60]= - z[5] + z[2];
    z[60]=z[60]*z[33];
    z[54]=z[54] + 5*z[60] - 4*z[112] + z[52];
    z[54]=z[4]*z[54];
    z[60]= - 3*z[15] - z[21];
    z[60]=2*z[60] + z[17];
    z[60]=z[60]*z[89];
    z[97]=z[7]*z[5];
    z[129]=z[13]*z[5];
    z[97]=z[97] + 9*z[129];
    z[134]=z[89] + z[97];
    z[134]=z[7]*z[134];
    z[60]=z[60] + z[134];
    z[60]=z[7]*z[60];
    z[134]=z[20] + z[5] + z[72];
    z[56]=z[134]*z[56];
    z[46]=z[54] + z[60] + z[46] - 4*z[101] + z[56];
    z[46]=z[12]*z[46];
    z[54]=z[26]*z[14];
    z[56]=5*z[61] + z[54] - n<T>(5,2)*z[93];
    z[56]=z[13]*z[56];
    z[60]=z[14] - z[113];
    z[60]=z[60]*z[38];
    z[101]=z[93] - z[19];
    z[134]=z[8]*z[101];
    z[56]=n<T>(7,2)*z[134] + z[60] + z[56];
    z[56]=z[56]*z[89];
    z[60]=static_cast<T>(1)- z[105];
    z[60]=z[7]*z[60];
    z[50]=z[60] - z[50] + z[20] - 14*z[5] + n<T>(41,2)*z[13];
    z[50]=z[7]*z[50];
    z[60]=z[15]*z[119];
    z[60]=z[60] + n<T>(23,4)*z[98];
    z[60]=z[60]*z[89];
    z[44]=n<T>(5,2)*z[5] + z[44];
    z[44]=z[13]*z[44];
    z[44]=z[50] + z[60] + z[44] + n<T>(5,4)*z[100];
    z[44]=z[7]*z[44];
    z[50]=z[108] + n<T>(13,2)*z[8];
    z[50]=z[8]*z[50];
    z[60]=z[6]*z[68];
    z[50]=z[60] - n<T>(13,4)*z[39] + z[50];
    z[50]=z[6]*z[50];
    z[60]=z[8] + z[118] + z[90];
    z[60]=z[60]*z[33];
    z[25]=z[46] + z[25] + z[44] + z[56] + z[50] + z[60] - z[121] + z[92]
   ;
    z[25]=z[12]*z[25];
    z[44]=11*z[9];
    z[46]=z[107] - z[44] + 5*z[114];
    z[46]=z[3]*z[46];
    z[50]=z[89]*z[29];
    z[56]=3*z[50];
    z[60]= - static_cast<T>(10)- z[78];
    z[46]=z[46] + 2*z[60] - z[56];
    z[46]=z[3]*z[46];
    z[60]= - z[126] + z[64] - z[114];
    z[60]=z[3]*z[60];
    z[68]=z[29]*z[7];
    z[90]= - z[74]*z[68];
    z[60]=z[60] + z[90] + static_cast<T>(12)+ z[117];
    z[60]=z[4]*z[60];
    z[90]=z[78] - z[50];
    z[90]=z[90]*z[125];
    z[92]=4*z[15];
    z[46]=z[60] + z[46] + z[90] - z[124] - z[76] - n<T>(11,2)*z[2] - 10*
    z[13] + z[92] - z[111];
    z[46]=z[4]*z[46];
    z[60]=2*z[122];
    z[76]= - n<T>(5,2)*z[114] - z[35] - z[60];
    z[76]=z[2]*z[76];
    z[90]=z[34] + z[9];
    z[90]=z[9]*z[90];
    z[90]= - z[82] + z[90];
    z[90]=z[90]*z[89];
    z[39]=z[131]*z[39];
    z[39]=z[39] + z[9] - z[60];
    z[39]=z[39]*z[31];
    z[60]=3*z[19];
    z[39]=z[39] + z[90] + z[60] + z[76] + static_cast<T>(1)+ n<T>(13,2)*z[98];
    z[39]=z[3]*z[39];
    z[76]=z[130]*z[82];
    z[90]=z[9] - z[14];
    z[100]= - z[90]*z[105];
    z[100]=z[100] + z[76];
    z[100]=z[100]*z[89];
    z[21]=z[21]*z[82];
    z[105]=3*z[14];
    z[107]= - z[105] + z[21];
    z[107]=z[8]*z[107];
    z[107]=z[107] + static_cast<T>(15)- 8*z[93];
    z[107]=z[8]*z[107];
    z[56]=static_cast<T>(35)- z[56];
    z[56]=z[56]*z[91];
    z[108]=4*z[5] + z[13];
    z[111]= - static_cast<T>(13)- 4*z[98];
    z[111]=z[2]*z[111];
    z[39]=z[39] + z[56] + z[100] + z[107] + 2*z[108] + z[111];
    z[39]=z[3]*z[39];
    z[56]=z[114] + z[122];
    z[100]=z[2]*z[56];
    z[107]= - z[7]*z[127];
    z[50]=z[107] + z[50] + z[100] - static_cast<T>(67)+ z[98];
    z[50]=z[50]*z[91];
    z[100]=11*z[2];
    z[107]=static_cast<T>(1)+ n<T>(1,4)*z[98];
    z[107]=z[107]*z[100];
    z[108]=z[89]*z[122];
    z[111]=n<T>(13,2)*z[13];
    z[50]=z[50] - n<T>(23,4)*z[108] + n<T>(29,4)*z[6] + z[107] + z[111] - z[26]
    - n<T>(3,4)*z[5];
    z[50]=z[7]*z[50];
    z[107]=z[34] - z[9];
    z[108]= - z[9]*z[107]*z[38];
    z[117]=z[82]*z[120];
    z[108]=z[108] + z[117];
    z[108]=z[108]*z[89];
    z[117]=z[17] + z[5];
    z[118]= - z[26] + z[117];
    z[118]=z[13]*z[118];
    z[120]= - n<T>(33,2) - z[98];
    z[120]=z[5]*z[120];
    z[120]=z[120] - 9*z[13];
    z[120]=z[120]*z[74];
    z[121]=4*z[19];
    z[124]=static_cast<T>(5)+ 3*z[93];
    z[124]=n<T>(1,2)*z[124] - z[121];
    z[124]=z[8]*z[124];
    z[131]=z[75] + z[5] - 17*z[13];
    z[124]=n<T>(1,2)*z[131] + z[124];
    z[124]=z[8]*z[124];
    z[131]=12*z[8];
    z[96]=z[6] + z[131] + 2*z[96] - n<T>(113,4)*z[2];
    z[96]=z[6]*z[96];
    z[18]=z[18] + z[23] + z[25] + z[46] + z[39] + z[50] + z[108] + z[96]
    + z[124] + z[120] + z[38] + z[118];
    z[18]=z[1]*z[18];
    z[23]=4*z[7];
    z[25]=static_cast<T>(1)- z[86];
    z[25]=z[6]*z[25];
    z[39]=z[14]*z[27];
    z[39]= - static_cast<T>(1)+ z[39];
    z[39]=z[3]*z[39];
    z[25]=z[39] - z[23] - z[24] + z[25];
    z[25]=z[3]*z[25];
    z[39]=n<T>(9,2)*z[7] + z[8] + 8*z[6];
    z[39]=z[7]*z[39];
    z[25]=z[25] + z[39] - z[87] + n<T>(3,2)*z[16] + z[33];
    z[25]=z[3]*z[25];
    z[39]= - 5*z[47] + z[49];
    z[39]=z[39]*z[74];
    z[46]= - z[88] - z[51];
    z[46]=z[6]*z[46];
    z[39]=z[39] + z[46];
    z[39]=z[6]*z[39];
    z[46]=z[9]*z[31];
    z[46]=static_cast<T>(3)+ z[46];
    z[46]=z[3]*z[46];
    z[30]=z[30] + z[46];
    z[30]=z[3]*z[30];
    z[46]= - z[43]*z[119];
    z[30]=z[30] + z[46];
    z[30]=z[4]*z[30];
    z[46]= - z[67] - z[3];
    z[42]=z[46]*z[42];
    z[30]=4*z[42] + z[30];
    z[30]=z[4]*z[30];
    z[32]=z[32]*z[43];
    z[32]= - 4*z[53] + z[32];
    z[32]=z[4]*z[32];
    z[36]=z[40]*z[36];
    z[32]=z[32] + z[36];
    z[32]=z[10]*z[32];
    z[36]= - z[7]*z[106];
    z[30]=z[32] + z[30] - z[53] + z[39] + z[36];
    z[30]=z[10]*z[30];
    z[22]=z[22] - z[66] - z[74];
    z[22]=z[8]*z[22];
    z[16]= - z[47] - z[16];
    z[32]= - z[13]*z[125];
    z[16]=z[32] + n<T>(27,4)*z[69] + n<T>(1,2)*z[16] + z[22];
    z[16]=z[7]*z[16];
    z[22]=z[119] + z[126];
    z[22]=z[3]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[31];
    z[32]=z[7]*z[9];
    z[28]= - z[3]*z[28];
    z[28]=z[32] + z[28];
    z[28]=z[28]*z[94];
    z[22]=z[28] + z[22] + n<T>(7,2)*z[6] - 8*z[7];
    z[22]=z[4]*z[22];
    z[28]= - z[6]*z[72];
    z[28]=z[28] + z[37];
    z[36]= - z[3]*z[9];
    z[36]= - static_cast<T>(2)+ z[36];
    z[36]=z[36]*z[132];
    z[36]= - 15*z[6] + z[36];
    z[36]=z[3]*z[36];
    z[22]=z[22] + 3*z[28] + z[36];
    z[22]=z[4]*z[22];
    z[28]= - 3*z[55] - 7*z[51];
    z[36]=z[8] + z[2];
    z[37]=z[66] + z[36];
    z[37]=z[6]*z[37];
    z[28]=n<T>(3,2)*z[28] + z[37];
    z[28]=z[6]*z[28];
    z[37]=z[33]*z[2];
    z[39]= - 7*z[52] + z[37];
    z[16]=z[30] + z[22] + z[25] + z[16] + n<T>(1,2)*z[39] + z[28];
    z[16]=z[10]*z[16];
    z[22]= - z[75] + z[8];
    z[22]=z[22]*z[33];
    z[25]=z[8]*z[20];
    z[25]=z[104] + z[25];
    z[25]=z[6]*z[25];
    z[28]= - z[7]*z[111];
    z[28]= - z[48] + z[28];
    z[28]=z[7]*z[28];
    z[30]=z[12]*z[45]*z[110];
    z[22]=z[30] - z[62] + z[28] + z[25] - n<T>(7,2)*z[52] + z[22];
    z[22]=z[12]*z[22];
    z[25]=z[58] - z[9];
    z[25]=z[25]*z[3];
    z[28]=7*z[32];
    z[30]=z[25] + z[28] - n<T>(7,2)*z[86] + z[60] + static_cast<T>(2)+ z[80];
    z[30]=z[3]*z[30];
    z[39]=static_cast<T>(5)- z[19];
    z[39]=z[39]*z[72];
    z[40]= - static_cast<T>(5)- n<T>(3,2)*z[78];
    z[40]=z[2]*z[40];
    z[42]= - static_cast<T>(7)+ z[86];
    z[42]=z[6]*z[42];
    z[32]=static_cast<T>(7)- n<T>(9,2)*z[32];
    z[32]=z[7]*z[32];
    z[30]=z[30] + z[32] + z[42] + z[40] + z[39];
    z[30]=z[3]*z[30];
    z[32]= - z[64] - z[126];
    z[32]=z[32]*z[132];
    z[32]= - static_cast<T>(15)+ z[32];
    z[32]=z[3]*z[32];
    z[39]= - z[9]*z[91];
    z[39]= - static_cast<T>(6)+ z[39];
    z[40]=20*z[9] + z[126];
    z[40]=z[3]*z[40];
    z[39]=3*z[39] + z[40];
    z[39]=z[4]*z[39];
    z[23]=z[39] + z[32] + z[23] - 3*z[77] - z[20];
    z[23]=z[4]*z[23];
    z[32]= - z[73] + z[100];
    z[39]= - static_cast<T>(2)- z[19];
    z[39]=z[39]*z[130];
    z[32]=n<T>(1,2)*z[32] + z[39];
    z[32]=z[8]*z[32];
    z[39]=n<T>(23,2) - z[78];
    z[39]=z[2]*z[39];
    z[39]=z[8] - z[99] + z[39];
    z[39]=n<T>(35,2)*z[7] + n<T>(1,2)*z[39] + z[84];
    z[39]=z[7]*z[39];
    z[40]=4*z[49];
    z[42]=static_cast<T>(1)- z[61];
    z[42]=z[6]*z[42];
    z[42]=z[42] + n<T>(25,2)*z[8] + 16*z[13] - n<T>(87,4)*z[2];
    z[42]=z[6]*z[42];
    z[16]=z[16] + z[22] + z[23] + z[30] + z[39] + z[42] + z[32] + z[70]
    - z[40];
    z[16]=z[10]*z[16];
    z[22]= - n<T>(1,2) + z[98];
    z[22]=z[7]*z[22];
    z[22]=z[22] + z[103] - n<T>(19,2)*z[13];
    z[22]=z[7]*z[22];
    z[23]= - n<T>(13,2)*z[5] - z[41];
    z[23]=z[13]*z[23];
    z[22]=z[22] - z[38] + z[23];
    z[22]=z[7]*z[22];
    z[23]= - z[109]*z[72];
    z[23]=z[23] + n<T>(11,2)*z[129] - z[40];
    z[23]=z[4]*z[23];
    z[30]= - z[75] - z[115];
    z[30]=z[30]*z[104];
    z[32]=z[33]*z[20];
    z[33]= - z[7]*z[97];
    z[33]= - 3*z[112] + z[33];
    z[33]=z[12]*z[33]*z[91];
    z[22]=z[33] + z[23] + z[22] + z[32] + z[30] + n<T>(5,2)*z[112] - z[52];
    z[22]=z[12]*z[22];
    z[23]= - n<T>(1,2)*z[4] - z[67] + z[81] + n<T>(13,2)*z[2] + z[85] - z[92] + 
    z[128];
    z[23]=z[4]*z[23];
    z[30]= - static_cast<T>(6)+ z[95];
    z[30]=z[5]*z[30];
    z[30]=z[66] + z[92] + z[30];
    z[30]=z[13]*z[30];
    z[32]=static_cast<T>(11)+ 5*z[93];
    z[19]=n<T>(1,2)*z[32] - z[19];
    z[19]=z[8]*z[19];
    z[19]=z[19] + z[110] + 6*z[5] + z[41];
    z[19]=z[8]*z[19];
    z[32]=z[122] - z[9];
    z[33]= - z[32]*z[91];
    z[39]=n<T>(3,2)*z[98];
    z[40]=static_cast<T>(8)- z[39];
    z[33]=3*z[40] + z[33];
    z[33]=z[7]*z[33];
    z[40]=n<T>(37,4) + z[98];
    z[40]=z[5]*z[40];
    z[33]=z[33] + n<T>(17,4)*z[6] - z[59] + 6*z[15] + z[40];
    z[33]=z[7]*z[33];
    z[40]= - n<T>(17,2)*z[2] + z[72];
    z[40]=z[40]*z[20];
    z[42]=z[5] + z[99];
    z[42]=z[2]*z[42];
    z[19]=z[22] + z[23] + z[33] + z[40] + z[19] + z[42] - z[38] + z[30];
    z[19]=z[12]*z[19];
    z[22]= - z[105] + z[9];
    z[22]=z[22]*z[39];
    z[23]= - z[82]*z[72];
    z[25]=z[127]*z[25];
    z[22]=z[25] + z[23] + z[114] + z[22] - n<T>(3,2)*z[14] + z[64];
    z[22]=z[3]*z[22];
    z[23]= - 9*z[14] + z[21];
    z[25]= - z[5]*z[133];
    z[25]=3*z[82] + z[25];
    z[25]=z[8]*z[25];
    z[23]=2*z[23] + z[25];
    z[23]=z[8]*z[23];
    z[25]=z[14] + z[64];
    z[25]=z[5]*z[25];
    z[30]= - 9*z[9] + z[122];
    z[30]=z[2]*z[30];
    z[22]=z[22] + z[28] + n<T>(3,2)*z[86] + z[23] + z[30] + static_cast<T>(8)+ z[25];
    z[22]=z[3]*z[22];
    z[23]= - 5*z[9] + 3*z[114];
    z[23]=z[23]*z[91];
    z[25]= - z[44] - 6*z[126];
    z[25]=z[3]*z[25];
    z[28]=z[9] + z[68];
    z[28]=n<T>(1,2)*z[28] + z[126];
    z[28]=z[4]*z[28];
    z[30]=z[26]*z[9];
    z[33]=static_cast<T>(1)+ z[30];
    z[23]=z[28] + z[25] + z[23] - z[123] + 2*z[33] - n<T>(5,2)*z[98];
    z[23]=z[4]*z[23];
    z[25]= - n<T>(25,2)*z[9] - z[56];
    z[25]=z[7]*z[25];
    z[28]=z[2]*z[32];
    z[25]=z[25] + n<T>(17,4)*z[28] - n<T>(9,4)*z[98] + n<T>(17,2) - z[30];
    z[25]=z[7]*z[25];
    z[28]=n<T>(5,2)*z[14];
    z[32]= - z[28] + z[76];
    z[32]=z[8]*z[32];
    z[32]=z[32] - static_cast<T>(15)+ z[95];
    z[32]=z[8]*z[32];
    z[33]=static_cast<T>(4)- z[61];
    z[27]=z[33]*z[27];
    z[33]= - z[5]*z[107];
    z[33]=n<T>(9,2) + z[33];
    z[33]=z[5]*z[33];
    z[38]=n<T>(29,2) + z[93];
    z[38]=z[13]*z[38];
    z[39]= - n<T>(63,4) + 5*z[98];
    z[39]=z[2]*z[39];
    z[16]=z[18] + z[16] + z[19] + z[23] + z[22] + z[25] + z[27] + z[32]
    + z[39] + z[38] + z[92] + z[33];
    z[16]=z[1]*z[16];
    z[18]=z[82]*z[17];
    z[19]= - z[18] + z[28] - z[21];
    z[19]=z[13]*z[19];
    z[21]= - z[83] - 5*z[14] + z[21];
    z[21]=z[8]*z[21];
    z[22]=z[11] + z[92];
    z[22]=z[14]*z[22];
    z[23]=z[64]*z[15];
    z[25]= - 4*z[14] + n<T>(7,2)*z[9];
    z[25]=z[5]*z[25];
    z[19]=z[21] + z[19] + z[25] - z[23] - n<T>(9,4) + z[22];
    z[19]=z[11]*z[19];
    z[21]=z[5]*z[82];
    z[21]= - z[28] + z[21];
    z[21]=z[13]*z[21];
    z[22]= - z[14]*z[15];
    z[22]=static_cast<T>(2)+ z[22];
    z[21]=z[21] + 2*z[22] + z[93];
    z[21]=z[13]*z[21];
    z[22]= - static_cast<T>(3)+ z[101];
    z[22]=z[8]*z[22];
    z[25]= - z[26] + n<T>(1,4)*z[5];
    z[17]=z[17]*z[14];
    z[27]=z[17]*z[11];
    z[21]= - z[27] + z[22] + z[21] - z[25];
    z[21]=z[11]*z[21];
    z[22]=z[11]*z[13];
    z[28]=z[7]*z[11];
    z[32]= - z[12]*z[13]*z[28];
    z[32]=z[32] - z[22];
    z[32]=z[117]*z[32];
    z[25]= - z[25]*z[28];
    z[25]=z[25] + z[32];
    z[25]=z[12]*z[25];
    z[32]=n<T>(7,2)*z[98] - n<T>(1,4) - z[23];
    z[32]=z[32]*z[28];
    z[21]=z[25] + z[21] + z[32];
    z[21]=z[12]*z[21];
    z[25]=n<T>(5,4) + z[30];
    z[25]=z[9]*z[25];
    z[25]=z[25] - n<T>(9,4)*z[122];
    z[25]=z[11]*z[25];
    z[25]= - n<T>(25,2) + z[25];
    z[25]=z[7]*z[25];
    z[19]=z[21] - n<T>(13,2)*z[4] + z[25] + z[19] + z[131] - z[24] - z[41]
    - z[26] - n<T>(27,4)*z[5];
    z[19]=z[12]*z[19];
    z[21]= - z[47]*z[91];
    z[24]= - z[11]*z[116];
    z[21]=z[21] + z[24] - z[52] - z[37];
    z[21]=z[12]*z[21];
    z[24]= - z[47] + 3*z[49];
    z[25]=n<T>(1,2)*z[24];
    z[21]=z[21] - z[71] - z[22] - n<T>(3,4)*z[69] + z[25] - z[104];
    z[21]=z[12]*z[11]*z[21];
    z[26]=z[61] - 2;
    z[26]=z[26]*z[13];
    z[32]=z[20] - z[26] + z[63];
    z[32]=z[6]*z[32];
    z[33]=z[49] - z[69];
    z[33]=z[11]*z[33];
    z[33]=z[65] + z[33];
    z[33]=z[12]*z[33];
    z[37]= - static_cast<T>(1)- z[17];
    z[37]=z[6]*z[37];
    z[37]=z[13] + z[37];
    z[37]=z[11]*z[37];
    z[25]=n<T>(1,2)*z[33] - z[71] + z[37] - z[25] + z[32];
    z[25]=z[11]*z[25];
    z[24]= - z[24]*z[20];
    z[32]=z[13] + z[74];
    z[32]=z[6]*z[32];
    z[32]= - z[116] + z[32];
    z[32]=z[11]*z[32];
    z[24]=z[24] + z[32];
    z[24]=z[11]*z[24];
    z[32]= - z[10]*z[89]*z[49]*z[20];
    z[24]=z[24] + z[32];
    z[24]=z[10]*z[24];
    z[24]=z[24] + z[25];
    z[24]=z[10]*z[24];
    z[25]=z[82]*z[13];
    z[32]= - z[14] + z[25];
    z[32]=z[13]*z[32];
    z[32]=n<T>(1,2) + z[32];
    z[20]=z[32]*z[20];
    z[32]=n<T>(5,2) - z[61];
    z[32]=z[13]*z[32];
    z[20]= - z[27] + z[20] + z[32] - z[79];
    z[20]=z[11]*z[20];
    z[27]= - z[4]*z[29]*z[91];
    z[27]=z[27] - 1;
    z[27]=z[11]*z[27];
    z[32]=z[28]*z[113];
    z[27]=z[32] + z[27];
    z[27]=z[4]*z[27];
    z[20]=z[24] + z[21] + z[20] + z[27];
    z[20]=z[10]*z[20];
    z[21]=static_cast<T>(3)+ z[121];
    z[21]=z[8]*z[21];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[11]*z[17];
    z[17]=z[17] + z[21] + z[26] - n<T>(1,4)*z[2];
    z[17]=z[11]*z[17];
    z[21]= - z[12]*z[47];
    z[21]=z[21] + z[13];
    z[21]=z[28]*z[21];
    z[24]=z[8]*z[36];
    z[24]=z[24] - z[55];
    z[22]=2*z[24] + z[22];
    z[22]=z[11]*z[22];
    z[21]=z[22] + z[21];
    z[21]=z[12]*z[21];
    z[17]=z[21] + z[17] + n<T>(3,2)*z[28];
    z[17]=z[12]*z[17];
    z[21]= - z[135] + z[66] - z[102];
    z[22]= - z[105] + z[25];
    z[22]=z[13]*z[22];
    z[22]=n<T>(9,2) + z[22];
    z[24]=z[14] + z[83];
    z[24]=z[8]*z[24];
    z[25]=z[34]*z[11];
    z[22]= - z[25] + n<T>(1,2)*z[22] + z[24];
    z[22]=z[11]*z[22];
    z[24]=z[29]*z[28];
    z[24]= - n<T>(15,2) + z[24];
    z[24]=z[4]*z[24];
    z[26]= - z[11]*z[113];
    z[26]=n<T>(17,2) + z[26];
    z[26]=z[7]*z[26];
    z[17]=z[20] + z[17] + z[24] + z[31] + z[26] + z[22] + 3*z[21] - n<T>(13,4)*z[6];
    z[17]=z[10]*z[17];
    z[20]= - z[25] + z[30] + n<T>(7,4) - z[54];
    z[20]=z[9]*z[20];
    z[21]=z[58] - n<T>(9,4)*z[9];
    z[21]=z[21]*z[98];
    z[22]= - z[133]*z[115];
    z[22]= - 2*z[82] + z[22];
    z[22]=z[8]*z[22];
    z[18]=z[22] + z[18] + z[21] + z[58] + z[20];
    z[18]=z[11]*z[18];
    z[20]= - z[11]*z[29];
    z[20]=n<T>(85,2)*z[9] + z[20];
    z[20]=z[20]*z[91];
    z[21]=z[90]*z[31];
    z[22]= - z[14] + n<T>(7,4)*z[9];
    z[22]=z[5]*z[22];
    z[24]=z[4]*z[35];

    r +=  - n<T>(23,4) + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + 
      z[22] + z[23] + z[24] - z[54] + z[57] - 8*z[61] + z[80];
 
    return r;
}

template double qg_2lNLC_r148(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r148(const std::array<dd_real,31>&);
#endif
