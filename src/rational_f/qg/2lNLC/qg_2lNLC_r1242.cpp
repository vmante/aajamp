#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1242(const std::array<T,31>& k) {
  T z[158];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[14];
    z[5]=k[19];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[8];
    z[9]=k[9];
    z[10]=k[11];
    z[11]=k[15];
    z[12]=k[5];
    z[13]=k[6];
    z[14]=k[2];
    z[15]=k[3];
    z[16]=k[4];
    z[17]=k[26];
    z[18]=k[29];
    z[19]=z[12]*z[1];
    z[20]=7*z[19];
    z[21]=npow(z[1],2);
    z[22]=5*z[21];
    z[23]= - z[22] - z[20];
    z[23]=z[12]*z[23];
    z[24]=npow(z[1],3);
    z[23]= - z[24] + z[23];
    z[23]=z[10]*z[23];
    z[25]=3*z[19];
    z[23]=z[23] + z[21] + z[25];
    z[26]=npow(z[12],2);
    z[23]=z[26]*z[23];
    z[27]=z[19] + z[21];
    z[28]=3*z[12];
    z[29]=z[28]*z[27];
    z[30]=z[29] + z[24];
    z[30]=z[30]*z[10];
    z[31]=2*z[19];
    z[32]=z[31] + z[21];
    z[30]=z[30] - z[32];
    z[33]=npow(z[12],3);
    z[34]=z[33]*z[30];
    z[35]=z[13]*z[10];
    z[36]=z[35]*npow(z[19],6);
    z[37]=npow(z[19],5);
    z[38]= - z[37] + z[36];
    z[38]=z[38]*npow(z[13],5);
    z[34]=z[38] + z[34];
    z[34]=z[4]*z[34];
    z[38]=z[37]*z[35];
    z[39]=npow(z[19],4);
    z[40]=2*z[39];
    z[38]=z[40] - 3*z[38];
    z[41]=npow(z[13],4);
    z[38]=z[38]*z[41];
    z[23]=z[34] + z[38] + z[23];
    z[34]=2*z[4];
    z[23]=z[23]*z[34];
    z[38]=4*z[19];
    z[42]= - z[21] - z[38];
    z[42]=z[12]*z[42];
    z[43]=z[32]*z[12];
    z[44]=z[24] + 7*z[43];
    z[45]=z[10]*z[12];
    z[44]=z[44]*z[45];
    z[46]=z[35]*z[40];
    z[47]=npow(z[19],3);
    z[46]= - z[47] + z[46];
    z[48]=npow(z[13],3);
    z[46]=z[46]*z[48];
    z[23]=z[23] + 4*z[46] + z[42] + z[44];
    z[23]=z[4]*z[23];
    z[42]=2*z[21];
    z[44]= - z[42] - z[20];
    z[44]=z[44]*z[45];
    z[23]=z[44] + z[23];
    z[44]=z[26]*z[21];
    z[35]=z[47]*z[35];
    z[35]=n<T>(961,8)*z[44] - 150*z[35];
    z[46]=npow(z[13],2);
    z[35]=z[35]*z[46];
    z[49]=n<T>(173,4)*z[21] + 163*z[19];
    z[23]=z[35] + n<T>(1,2)*z[49] + 25*z[23];
    z[23]=z[4]*z[23];
    z[35]=z[33]*z[9];
    z[49]= - z[21]*z[35];
    z[49]= - z[44] + z[49];
    z[49]=z[9]*z[49];
    z[50]= - n<T>(289,4)*z[21] - z[19];
    z[50]=z[12]*z[50];
    z[49]=z[50] + z[49];
    z[49]=z[9]*z[49];
    z[50]= - 101*z[21] + 29*z[19];
    z[49]=n<T>(1,4)*z[50] + z[49];
    z[49]=z[9]*z[49];
    z[50]=z[24]*z[12];
    z[51]=2*z[50];
    z[52]=npow(z[1],4);
    z[53]=3*z[52];
    z[54]=z[53] - z[51];
    z[54]=z[12]*z[54];
    z[55]=z[52]*z[12];
    z[56]=npow(z[1],5);
    z[57]=z[55] - z[56];
    z[58]=z[57]*z[12];
    z[59]=npow(z[1],6);
    z[58]=z[58] + z[59];
    z[60]=z[9]*z[58];
    z[61]=3*z[56];
    z[54]=z[60] - z[61] + z[54];
    z[54]=z[9]*z[54];
    z[60]=z[21]*z[12];
    z[62]=3*z[60];
    z[63]=7*z[24];
    z[64]= - z[63] + z[62];
    z[65]=n<T>(1,2)*z[12];
    z[64]=z[64]*z[65];
    z[66]=4*z[52];
    z[54]=z[54] + z[66] + z[64];
    z[54]=z[9]*z[54];
    z[64]=n<T>(1,2)*z[19];
    z[67]=z[42] - z[64];
    z[67]=z[12]*z[67];
    z[68]=3*z[24];
    z[54]=z[54] - z[68] + z[67];
    z[67]=31*z[9];
    z[54]=z[54]*z[67];
    z[54]=z[54] + n<T>(155,4)*z[21] - z[25];
    z[69]=3*z[10];
    z[54]=z[54]*z[69];
    z[70]=z[44]*z[10];
    z[71]=n<T>(1,2)*z[60];
    z[72]=z[16]*z[10];
    z[73]= - z[72]*z[71];
    z[73]=2*z[70] + z[73];
    z[74]=25*z[46];
    z[73]=z[73]*z[74];
    z[75]=63*z[24] - 4*z[60];
    z[75]=z[12]*z[75];
    z[76]=279*z[52];
    z[75]= - z[76] + z[75];
    z[75]=z[12]*z[75];
    z[77]=z[60] - z[24];
    z[78]= - z[12]*z[77];
    z[79]=93*z[52];
    z[78]= - z[79] + z[78];
    z[78]=z[12]*z[78];
    z[80]=93*z[56];
    z[78]=z[80] + z[78];
    z[78]=z[12]*z[78];
    z[78]= - 93*z[59] + z[78];
    z[78]=z[9]*z[78];
    z[81]=279*z[56];
    z[75]=z[78] + z[81] + z[75];
    z[75]=z[9]*z[75];
    z[78]=799*z[24] - 239*z[60];
    z[82]=n<T>(1,4)*z[12];
    z[78]=z[78]*z[82];
    z[75]=z[75] - 372*z[52] + z[78];
    z[75]=z[9]*z[75];
    z[78]= - 25*z[21] - n<T>(19,4)*z[19];
    z[78]=z[12]*z[78];
    z[75]=z[75] + n<T>(481,2)*z[24] + 5*z[78];
    z[75]=z[9]*z[75];
    z[78]=npow(z[1],7);
    z[83]=z[78]*z[9];
    z[84]=3*z[59];
    z[83]=z[83] - z[84];
    z[83]=z[83]*z[9];
    z[85]=4*z[56];
    z[83]=z[83] + z[85];
    z[83]=z[83]*z[9];
    z[83]=z[83] - z[53];
    z[83]=z[83]*z[9];
    z[83]=z[83] + n<T>(5,4)*z[24];
    z[86]=93*z[10];
    z[83]=z[83]*z[86];
    z[87]=z[21]*z[16];
    z[88]=z[87]*z[10];
    z[83]=z[83] - n<T>(93,4)*z[88];
    z[75]= - n<T>(1313,16)*z[21] + z[75] + z[83];
    z[75]=z[6]*z[75];
    z[89]=n<T>(1,2)*z[21];
    z[90]=z[89] + z[19];
    z[91]=z[60] + z[24];
    z[92]=n<T>(1,4)*z[8];
    z[93]= - z[91]*z[92];
    z[93]=z[93] - z[90];
    z[93]=z[8]*z[93];
    z[94]=z[16]*z[1];
    z[95]=z[94] - z[21];
    z[95]=z[95]*z[11];
    z[96]=n<T>(1,4)*z[16];
    z[97]= - z[1]*z[86];
    z[97]=n<T>(781,4) + z[97];
    z[97]=z[97]*z[96];
    z[23]=n<T>(65,8)*z[95] + n<T>(95,4)*z[93] + z[23] + z[75] + z[73] + z[97] + 
    z[54] + z[49] - 37*z[1] - n<T>(561,8)*z[12];
    z[23]=z[11]*z[23];
    z[49]= - n<T>(79,4)*z[60] - 47*z[87];
    z[54]=z[46]*z[16];
    z[49]=z[49]*z[54];
    z[73]=z[64] + z[21];
    z[75]=z[73]*z[12];
    z[93]= - z[16]*z[64];
    z[93]=z[75] + z[93];
    z[93]=z[8]*z[93];
    z[49]=n<T>(69,2)*z[93] + z[49] + n<T>(67,2)*z[94] + n<T>(37,2)*z[21] + 5*z[19];
    z[49]=z[49]*z[92];
    z[92]= - 154*z[50] + 195*z[52];
    z[93]= - z[12]*z[92];
    z[97]=65*z[7];
    z[98]= - z[58]*z[97];
    z[93]=z[98] + 195*z[56] + z[93];
    z[93]=z[7]*z[93];
    z[98]= - 619*z[60] + 947*z[24];
    z[99]=z[98]*z[82];
    z[93]=z[93] - 260*z[52] + z[99];
    z[93]=z[7]*z[93];
    z[99]=87*z[19];
    z[100]= - n<T>(301,2)*z[21] + z[99];
    z[100]=z[12]*z[100];
    z[93]=z[93] + n<T>(775,4)*z[24] + z[100];
    z[93]=z[7]*z[93];
    z[100]= - 13*z[21] + n<T>(103,16)*z[19];
    z[101]= - z[13]*z[44];
    z[101]= - n<T>(1,4)*z[33] + z[101];
    z[101]=z[13]*z[101];
    z[93]=n<T>(85,4)*z[101] + 5*z[100] + z[93];
    z[93]=z[6]*z[93];
    z[100]=z[52]*z[16];
    z[101]=z[100] + z[56];
    z[102]= - z[55] - z[101];
    z[102]=z[4]*z[102];
    z[103]=z[24]*z[16];
    z[102]=150*z[102] + n<T>(527,2)*z[103] + 350*z[52] + n<T>(827,4)*z[50];
    z[102]=z[4]*z[102];
    z[104]= - n<T>(31,4)*z[24] - z[62];
    z[104]=87*z[104] - n<T>(1663,4)*z[87];
    z[102]=n<T>(1,2)*z[104] + z[102];
    z[102]=z[4]*z[102];
    z[104]=349*z[94] + 611*z[21] + 111*z[19];
    z[102]=n<T>(1,4)*z[104] + z[102];
    z[102]=z[4]*z[102];
    z[104]=150*z[52] + n<T>(427,4)*z[103];
    z[104]=z[16]*z[104];
    z[101]= - z[16]*z[101];
    z[101]= - z[59] + z[101];
    z[105]=50*z[4];
    z[101]=z[101]*z[105];
    z[101]=z[101] + 150*z[56] + z[104];
    z[101]=z[4]*z[101];
    z[104]= - 499*z[24] - 273*z[87];
    z[104]=z[16]*z[104];
    z[101]=z[101] - 200*z[52] + n<T>(3,8)*z[104];
    z[101]=z[4]*z[101];
    z[104]=1001*z[21] + 503*z[94];
    z[106]=n<T>(1,2)*z[16];
    z[104]=z[104]*z[106];
    z[104]=565*z[24] + z[104];
    z[101]=n<T>(1,4)*z[104] + z[101];
    z[101]=z[4]*z[101];
    z[104]=z[46]*z[21];
    z[107]=npow(z[16],2);
    z[108]=z[104]*z[107];
    z[101]=z[101] - n<T>(69,4)*z[108] - 50*z[21] - n<T>(269,8)*z[94];
    z[101]=z[3]*z[101];
    z[109]= - z[94] + z[89] - z[19];
    z[109]=z[16]*z[109];
    z[110]=z[107]*z[48];
    z[110]=n<T>(1,2)*z[110];
    z[111]=z[50] + z[103];
    z[111]=z[111]*z[110];
    z[109]=z[111] + z[71] + z[109];
    z[109]=z[11]*z[109];
    z[110]= - z[50]*z[110];
    z[111]=z[19]*z[16];
    z[110]=z[110] - z[71] + z[111];
    z[110]=z[8]*z[110];
    z[112]= - z[94] + z[108];
    z[109]=47*z[109] + 31*z[112] + 15*z[110];
    z[109]=z[2]*z[109];
    z[73]=3*z[94] - z[73];
    z[110]=n<T>(1,4)*z[60];
    z[112]=z[110] - z[87];
    z[54]=z[112]*z[54];
    z[54]=n<T>(1,2)*z[73] + z[54];
    z[73]= - z[21] + n<T>(1,2)*z[94];
    z[73]=z[73]*z[16];
    z[112]=n<T>(1,2)*z[24];
    z[73]=z[73] + z[112];
    z[113]=z[73]*z[11];
    z[54]=47*z[54] - n<T>(35,2)*z[113];
    z[54]=z[11]*z[54];
    z[114]=85*z[12];
    z[54]=z[54] - 23*z[16] - n<T>(37,4)*z[1] - z[114];
    z[115]= - 79*z[21] + 199*z[19];
    z[116]=z[7]*z[60];
    z[115]= - n<T>(175,4)*z[116] + n<T>(1,4)*z[115] - 8*z[94];
    z[115]=z[7]*z[115];
    z[114]=z[114] + 69*z[16];
    z[114]=z[16]*z[114];
    z[114]= - 85*z[26] + z[114];
    z[114]=z[13]*z[114];
    z[49]=n<T>(1,8)*z[109] + z[101] + z[49] + z[102] + z[93] + n<T>(1,16)*z[114]
    + z[115] + n<T>(1,4)*z[54];
    z[49]=z[2]*z[49];
    z[54]=65*z[21];
    z[93]=z[9]*z[24];
    z[93]= - z[54] + 279*z[93];
    z[93]=z[9]*z[93];
    z[93]=n<T>(161,4)*z[1] + z[93];
    z[101]=npow(z[9],2);
    z[93]=z[93]*z[101];
    z[102]= - z[52]*z[67];
    z[102]=11*z[24] + z[102];
    z[109]=3*z[9];
    z[102]=z[102]*z[109];
    z[102]= - n<T>(161,4)*z[21] + z[102];
    z[102]=z[9]*z[102];
    z[102]= - n<T>(95,4)*z[1] + z[102];
    z[114]=z[8]*z[9];
    z[102]=z[102]*z[114];
    z[115]=z[3]*z[104];
    z[93]=n<T>(133,16)*z[115] + z[93] + z[102];
    z[93]=z[3]*z[93];
    z[102]=z[48]*z[24];
    z[115]=z[109]*z[21];
    z[116]= - z[1] + z[115];
    z[116]=z[8]*z[116];
    z[116]=z[116] - z[102];
    z[117]=npow(z[9],3);
    z[116]=z[117]*z[116];
    z[115]= - 2*z[1] - z[115];
    z[115]=z[115]*z[117];
    z[118]=npow(z[101],2);
    z[119]=z[8]*z[24]*z[118];
    z[115]=z[115] + z[119];
    z[115]=z[3]*z[115];
    z[119]=z[21]*z[8];
    z[120]= - z[118]*z[119];
    z[121]=z[118]*z[1];
    z[122]=3*z[121];
    z[120]=z[122] + z[120];
    z[123]=z[14]*z[3];
    z[120]=z[120]*z[123];
    z[115]=z[120] + z[115] - 5*z[121] + z[116];
    z[115]=z[14]*z[115];
    z[116]=z[21]*z[101];
    z[120]=z[117]*z[13];
    z[124]=z[50]*z[120];
    z[116]=z[116] + z[124];
    z[116]=z[13]*z[116];
    z[124]=z[12]*z[101];
    z[116]=z[124] + z[116];
    z[116]=z[13]*z[116];
    z[91]= - z[91]*z[109];
    z[91]=2*z[27] + z[91];
    z[91]=z[8]*z[91]*z[117];
    z[124]=z[13]*z[1];
    z[124]=npow(z[124],5);
    z[125]= - z[33]*z[124]*z[105];
    z[126]=z[26]*z[52];
    z[127]=z[41]*z[126];
    z[125]=n<T>(573,4)*z[127] + z[125];
    z[125]=z[4]*z[125];
    z[127]=z[50]*z[48];
    z[125]= - n<T>(1385,8)*z[127] + z[125];
    z[125]=z[4]*z[125];
    z[125]=n<T>(905,8)*z[104] + z[125];
    z[125]=z[11]*z[4]*z[125];
    z[127]=z[22] + z[19];
    z[128]=z[127]*z[118];
    z[91]=z[115] + z[125] + z[93] + z[91] + z[128] + z[116];
    z[91]=z[14]*z[91];
    z[93]=z[53]*z[9];
    z[115]=z[24] - z[93];
    z[116]=93*z[9];
    z[115]=z[115]*z[116];
    z[125]=n<T>(253,4)*z[21];
    z[115]= - z[125] + z[115];
    z[115]=z[9]*z[115];
    z[115]= - n<T>(95,2)*z[1] + z[115];
    z[115]=z[9]*z[115];
    z[128]=z[56]*z[9];
    z[129]=z[128] - z[52];
    z[130]=z[129]*z[116];
    z[130]=n<T>(253,4)*z[24] + z[130];
    z[130]=z[9]*z[130];
    z[130]= - n<T>(19,2)*z[21] + z[130];
    z[130]=z[130]*z[114];
    z[131]=z[87]*z[46];
    z[132]=43*z[1] - n<T>(133,2)*z[131];
    z[133]=n<T>(1,4)*z[3];
    z[132]=z[132]*z[133];
    z[134]=z[4]*z[1];
    z[115]=z[132] + z[130] + n<T>(27,8)*z[134] - n<T>(69,16) + z[115];
    z[115]=z[3]*z[115];
    z[130]=z[33]*z[52];
    z[132]=z[130]*z[41];
    z[134]=npow(z[12],4);
    z[135]=z[134]*z[124];
    z[136]=z[1]*z[33];
    z[135]=z[136] + z[135];
    z[135]=z[4]*z[135];
    z[136]=z[26]*z[1];
    z[135]=z[135] - z[136] - z[132];
    z[135]=z[135]*z[105];
    z[137]=z[102]*z[26];
    z[138]= - 83*z[19] - 271*z[137];
    z[135]=n<T>(3,8)*z[138] + z[135];
    z[135]=z[4]*z[135];
    z[138]=z[46]*z[60];
    z[139]=71*z[1] + n<T>(361,2)*z[138];
    z[135]=n<T>(5,4)*z[139] + z[135];
    z[135]=z[4]*z[135];
    z[139]= - z[9]*z[136];
    z[139]= - z[19] + z[139];
    z[139]=z[9]*z[139];
    z[139]= - n<T>(33,2)*z[1] + z[139];
    z[139]=z[9]*z[139];
    z[140]= - z[8]*z[89];
    z[140]=z[1] + z[140];
    z[140]=z[8]*z[140];
    z[141]=z[11]*z[1];
    z[135]= - n<T>(95,16)*z[141] + n<T>(95,8)*z[140] + z[135] - static_cast<T>(124)+ z[139];
    z[135]=z[11]*z[135];
    z[139]=z[19] - z[21];
    z[140]=z[139]*z[12];
    z[141]=465*z[24];
    z[142]= - z[141] + z[140];
    z[142]=z[9]*z[142];
    z[142]=z[142] + 127*z[21] + z[19];
    z[142]=z[9]*z[142];
    z[142]=n<T>(157,4)*z[1] + z[142];
    z[142]=z[142]*z[101];
    z[143]=z[101]*z[60];
    z[144]=z[26]*z[24];
    z[145]=z[144]*z[120];
    z[143]=z[143] + z[145];
    z[143]=z[13]*z[143];
    z[145]=z[26]*z[101];
    z[143]=z[145] + z[143];
    z[143]=z[13]*z[143];
    z[132]=4*z[136] + z[132];
    z[132]=z[132]*z[105];
    z[132]=z[132] - n<T>(919,4)*z[19] - 50*z[137];
    z[132]=z[4]*z[132];
    z[136]=233*z[1] - n<T>(457,4)*z[138];
    z[132]=n<T>(1,2)*z[136] + z[132];
    z[132]=z[4]*z[132];
    z[132]=n<T>(407,4) + z[132];
    z[132]=z[4]*z[132];
    z[136]=4*z[21];
    z[137]= - z[136] - z[19];
    z[137]=z[12]*z[137];
    z[138]=z[68] + z[60];
    z[138]=z[12]*z[138];
    z[138]=z[76] + z[138];
    z[138]=z[9]*z[138];
    z[137]=z[138] - 252*z[24] + z[137];
    z[137]=z[9]*z[137];
    z[137]=z[137] + n<T>(425,4)*z[21] + z[19];
    z[137]=z[9]*z[137];
    z[137]=n<T>(29,4)*z[1] + z[137];
    z[114]=z[137]*z[114];
    z[91]=z[91] + z[135] + z[115] + z[114] + z[132] + z[142] + z[143];
    z[91]=z[14]*z[91];
    z[114]=z[118]*z[48];
    z[115]=z[114]*z[52];
    z[132]=z[9]*z[12];
    z[132]=z[132] - 1;
    z[135]=z[132]*z[117];
    z[135]=z[135] - z[115];
    z[135]=z[13]*z[135];
    z[137]=z[52]*z[117]*z[48];
    z[137]=z[101] + z[137];
    z[137]=z[13]*z[137];
    z[138]=93*z[121];
    z[137]=z[138] + z[137];
    z[137]=z[3]*z[137];
    z[142]= - z[56]*z[118]*z[41];
    z[143]=2*z[117];
    z[142]= - z[143] + z[142];
    z[142]=z[3]*z[13]*z[142];
    z[145]=z[123] + 1;
    z[146]=z[118]*z[13];
    z[145]=z[146]*z[145];
    z[142]=z[142] + z[145];
    z[142]=z[14]*z[142];
    z[135]=z[142] + z[135] + z[137];
    z[135]=z[14]*z[135];
    z[137]=z[26]*z[118];
    z[142]= - z[55]*z[114];
    z[137]=z[137] + z[142];
    z[137]=z[13]*z[137];
    z[142]=93*z[3];
    z[145]=z[21]*z[9];
    z[147]= - z[1] - z[145];
    z[147]=z[147]*z[117]*z[142];
    z[135]=z[135] + z[147] + z[138] + z[137];
    z[135]=z[14]*z[135];
    z[137]= - 5*z[55] + z[100];
    z[137]=z[16]*z[137];
    z[137]=z[126] + z[137];
    z[138]=npow(z[7],4);
    z[137]=z[137]*z[138];
    z[147]= - z[118]*z[126];
    z[137]=z[147] + 65*z[137];
    z[137]=z[13]*z[137];
    z[147]= - z[117]*z[50];
    z[148]=z[50] - z[103];
    z[149]=npow(z[7],3);
    z[148]=z[148]*z[149];
    z[137]=z[137] + z[147] + 113*z[148];
    z[137]=z[13]*z[137];
    z[147]=npow(z[7],2);
    z[148]=z[21]*z[147];
    z[137]=n<T>(189,4)*z[148] + z[137];
    z[137]=z[13]*z[137];
    z[35]=z[35] + z[26];
    z[148]=z[35]*z[117];
    z[137]=z[148] + z[137];
    z[137]=z[13]*z[137];
    z[148]=z[140] + z[24];
    z[150]=z[9]*z[148];
    z[150]=z[150] + z[139];
    z[150]=z[9]*z[150];
    z[151]=z[52]*z[9];
    z[151]=z[151] - z[24];
    z[151]=z[151]*z[9];
    z[89]=z[151] + z[89];
    z[89]=z[89]*z[10];
    z[89]=z[89] - n<T>(1,2)*z[1];
    z[150]=z[150] - z[89];
    z[150]=z[101]*z[150];
    z[151]= - z[126]*z[143];
    z[152]=z[33]*z[56];
    z[153]= - z[146]*z[152];
    z[151]=z[151] + z[153];
    z[151]=z[13]*z[151];
    z[153]= - z[101]*z[50];
    z[151]=z[153] + z[151];
    z[151]=z[151]*z[46];
    z[153]=z[134]*z[9];
    z[153]=z[153] + 2*z[33];
    z[153]=z[153]*z[9];
    z[154]=z[153] + z[26];
    z[155]=z[154]*z[101];
    z[151]=z[155] + z[151];
    z[151]=z[13]*z[151];
    z[150]=93*z[150] + z[151];
    z[150]=z[6]*z[150];
    z[151]=z[24]*z[101];
    z[89]=z[151] - z[89];
    z[89]=z[142]*z[101]*z[89];
    z[142]=z[24]*z[149];
    z[100]= - z[55] + z[100];
    z[151]=z[138]*z[13];
    z[100]=z[100]*z[151];
    z[100]= - 89*z[142] + 130*z[100];
    z[100]=z[100]*z[48];
    z[142]=z[15]*z[52]*z[41];
    z[142]=65*z[142] - 195*z[1];
    z[142]=z[138]*z[142];
    z[100]=z[100] + z[142];
    z[100]=z[15]*z[100];
    z[142]=z[139]*z[118];
    z[155]=z[27] - z[94];
    z[156]=z[7]*z[155];
    z[156]=202*z[1] + 195*z[156];
    z[156]=z[156]*z[149];
    z[89]=z[100] + z[135] + z[89] + z[150] + z[137] + 93*z[142] + z[156]
   ;
    z[89]=z[15]*z[89];
    z[100]=31*z[21];
    z[135]=z[100] + 41*z[19];
    z[135]=z[12]*z[135];
    z[135]= - 31*z[24] + z[135];
    z[137]= - z[19] + 93*z[21];
    z[142]=z[137]*z[12];
    z[150]=93*z[24];
    z[142]=z[142] - z[150];
    z[156]=z[12]*z[142];
    z[156]=z[79] + z[156];
    z[156]=z[9]*z[156];
    z[135]=3*z[135] + z[156];
    z[135]=z[9]*z[135];
    z[156]=n<T>(31,2)*z[21] + 21*z[19];
    z[135]=3*z[156] + z[135];
    z[135]=z[9]*z[135];
    z[135]=32*z[1] + z[135];
    z[135]=z[9]*z[135];
    z[129]= - z[9]*z[129];
    z[129]= - z[112] + z[129];
    z[86]=z[129]*z[101]*z[86];
    z[129]=z[130]*z[143];
    z[156]=z[134]*z[56];
    z[157]=z[146]*z[156];
    z[129]=z[129] + z[157];
    z[129]=z[13]*z[129];
    z[157]=z[101]*z[144];
    z[129]=z[157] + z[129];
    z[129]=z[129]*z[46];
    z[154]=z[9]*z[154];
    z[129]=2*z[154] + z[129];
    z[129]=z[13]*z[129];
    z[86]=z[129] + z[135] + z[86];
    z[86]=z[6]*z[86];
    z[129]=z[117]*z[41]*z[66];
    z[122]=z[122] + z[129];
    z[122]=z[3]*z[122];
    z[123]=z[118]*z[124]*z[123];
    z[115]=z[143] + z[115];
    z[115]=z[13]*z[115];
    z[115]= - 3*z[123] + z[115] + z[122];
    z[115]=z[14]*z[115];
    z[122]= - z[24]*z[117];
    z[123]=z[55]*z[146];
    z[122]=z[122] + z[123];
    z[122]=z[122]*z[46];
    z[123]=2*z[101];
    z[124]=z[132]*z[123];
    z[122]=z[124] + z[122];
    z[122]=z[13]*z[122];
    z[124]=31*z[117];
    z[129]=z[1] - 9*z[145];
    z[129]=z[129]*z[124];
    z[132]= - z[101]*z[102];
    z[129]=z[129] + z[132];
    z[129]=z[3]*z[129];
    z[115]=z[115] + z[129] - z[121] + z[122];
    z[115]=z[14]*z[115];
    z[114]=z[126]*z[114];
    z[121]=z[26]*z[143];
    z[114]=z[121] + z[114];
    z[114]=z[13]*z[114];
    z[121]=z[9]*z[137];
    z[121]=125*z[1] + z[121];
    z[121]=z[121]*z[117];
    z[122]=z[24]*z[109];
    z[122]=z[21] + z[122];
    z[122]=z[122]*z[124];
    z[122]=z[122] - n<T>(23,16)*z[13];
    z[124]=3*z[3];
    z[122]=z[122]*z[124];
    z[114]=z[115] + z[122] + z[121] + z[114];
    z[114]=z[14]*z[114];
    z[115]=161*z[50] - 24*z[103];
    z[115]=z[16]*z[115];
    z[115]= - 24*z[144] + z[115];
    z[115]=z[115]*z[149];
    z[118]=z[118]*z[130];
    z[121]= - z[16]*z[55];
    z[121]=z[126] + z[121];
    z[121]=z[16]*z[121]*z[138];
    z[118]=z[118] + 195*z[121];
    z[118]=z[13]*z[118];
    z[121]=z[117]*z[144];
    z[115]=z[118] + z[121] + z[115];
    z[115]=z[13]*z[115];
    z[118]= - 20*z[60] + n<T>(23,2)*z[87];
    z[118]=z[118]*z[147];
    z[115]=3*z[118] + z[115];
    z[115]=z[13]*z[115];
    z[118]=z[35]*z[123];
    z[115]=z[115] + n<T>(85,16) + z[118];
    z[115]=z[13]*z[115];
    z[118]= - z[24] + 5*z[111];
    z[118]=z[118]*z[97];
    z[118]= - 137*z[155] + z[118];
    z[118]=z[7]*z[118];
    z[118]= - n<T>(471,4)*z[1] + z[118];
    z[118]=z[7]*z[118];
    z[118]=n<T>(5,4) + z[118];
    z[118]=z[7]*z[118];
    z[121]=2*z[24];
    z[93]=z[121] - z[93];
    z[93]=z[9]*z[93];
    z[93]= - n<T>(3,2)*z[21] + z[93];
    z[93]=z[93]*z[101];
    z[122]=z[109]*z[56];
    z[129]=5*z[52];
    z[132]= - z[129] + z[122];
    z[132]=z[9]*z[132];
    z[135]=n<T>(7,2)*z[24];
    z[132]=z[135] + z[132];
    z[132]=z[9]*z[132];
    z[132]= - z[21] + z[132];
    z[132]=z[10]*z[132]*z[67];
    z[137]=z[13]*z[16];
    z[93]=n<T>(23,8)*z[137] + z[132] + n<T>(23,16) + 31*z[93];
    z[93]=z[93]*z[124];
    z[124]= - z[13]*z[60];
    z[124]=z[26] + z[124];
    z[124]=z[13]*z[124];
    z[132]=13*z[1];
    z[124]=z[132] + n<T>(17,4)*z[124];
    z[124]=z[6]*z[124];
    z[137]=z[12] - z[16];
    z[137]=z[13]*z[137];
    z[137]= - static_cast<T>(1)+ z[137];
    z[124]=n<T>(17,4)*z[137] + z[124];
    z[137]= - z[13]*z[87];
    z[137]= - z[107] + z[137];
    z[137]=z[13]*z[137];
    z[137]= - 25*z[1] + n<T>(69,8)*z[137];
    z[137]=z[3]*z[137];
    z[124]=n<T>(5,2)*z[124] + z[137];
    z[124]=z[2]*z[124];
    z[137]=z[9]*z[142];
    z[137]=124*z[19] + z[137];
    z[137]=z[9]*z[137];
    z[137]= - 61*z[1] + z[137];
    z[137]=z[137]*z[101];
    z[86]=z[89] + z[114] + n<T>(1,2)*z[124] + z[93] + z[86] + z[115] + 
    z[137] + z[118];
    z[86]=z[15]*z[86];
    z[89]=z[53] - z[50];
    z[89]=z[89]*z[12];
    z[58]=z[58]*z[7];
    z[58]=z[58] + z[89] - z[61];
    z[58]=z[58]*z[7];
    z[89]=5*z[24];
    z[93]=z[89] - z[60];
    z[93]=z[93]*z[65];
    z[58]=z[58] - z[93] + z[66];
    z[93]=z[7]*z[58];
    z[114]= - z[60] + n<T>(5,2)*z[24];
    z[93]=z[93] - z[114];
    z[115]=13*z[7];
    z[93]=z[93]*z[115];
    z[118]=z[129] + z[50];
    z[118]=z[118]*z[12];
    z[55]=z[61] + z[55];
    z[55]=z[55]*z[12];
    z[55]=z[55] + z[84];
    z[124]=z[55]*z[4];
    z[129]=7*z[56];
    z[118]= - z[124] + z[118] + z[129];
    z[34]=z[118]*z[34];
    z[118]=z[63] + z[60];
    z[118]=z[118]*z[12];
    z[34]=z[34] - z[118] - 14*z[52];
    z[118]= - z[4]*z[34];
    z[124]=2*z[60];
    z[63]=z[124] + z[63];
    z[118]=z[118] - z[63];
    z[137]=5*z[4];
    z[118]=z[118]*z[137];
    z[142]=z[78]*z[7];
    z[142]=z[142] - z[84];
    z[142]=z[142]*z[7];
    z[143]=z[85] + z[142];
    z[143]=z[7]*z[143];
    z[143]= - z[53] + z[143];
    z[143]=z[7]*z[143];
    z[143]=z[24] + z[143];
    z[145]=13*z[6];
    z[143]=z[143]*z[145];
    z[78]=z[78]*z[4];
    z[78]=z[78] - z[84];
    z[78]=z[78]*z[4];
    z[84]=z[85] + z[78];
    z[84]=z[4]*z[84];
    z[84]= - z[53] + z[84];
    z[84]=z[4]*z[84];
    z[84]=z[24] + z[84];
    z[85]=10*z[3];
    z[84]=z[84]*z[85];
    z[146]=n<T>(23,2)*z[21];
    z[84]=z[84] + z[118] + z[143] + z[146] + z[93];
    z[84]=z[2]*z[84];
    z[58]= - z[7]*z[10]*z[58];
    z[93]=z[10]*z[114];
    z[58]=z[93] + z[58];
    z[58]=z[58]*z[115];
    z[34]=z[4]*z[10]*z[34];
    z[63]=z[10]*z[63];
    z[34]=z[63] + z[34];
    z[34]=z[34]*z[137];
    z[63]= - z[10]*z[142];
    z[93]=z[56]*z[10];
    z[114]=4*z[93];
    z[63]= - z[114] + z[63];
    z[63]=z[7]*z[63];
    z[115]=z[53]*z[10];
    z[63]=z[115] + z[63];
    z[63]=z[7]*z[63];
    z[118]=z[24]*z[10];
    z[63]= - z[118] + z[63];
    z[63]=z[63]*z[145];
    z[78]= - z[10]*z[78];
    z[78]= - z[114] + z[78];
    z[78]=z[4]*z[78];
    z[78]=z[115] + z[78];
    z[78]=z[4]*z[78];
    z[78]= - z[118] + z[78];
    z[78]=z[78]*z[85];
    z[85]=z[22]*z[3];
    z[114]=z[21]*z[6];
    z[85]= - z[85] + n<T>(13,2)*z[114];
    z[137]= - z[2] + z[10];
    z[85]=z[15]*z[85]*z[137];
    z[137]= - z[10]*z[146];
    z[34]=n<T>(1,2)*z[85] + z[84] + z[78] + z[34] + z[63] + z[137] + z[58];
    z[34]=z[5]*z[34];
    z[58]=z[99] + 41*z[21];
    z[58]=z[58]*z[16];
    z[63]=z[103]*z[48];
    z[78]=z[63]*z[26];
    z[75]=z[75] + z[112];
    z[71]=z[71] + z[24];
    z[71]=z[71]*z[12];
    z[71]=z[71] + n<T>(1,2)*z[52];
    z[84]= - z[11]*z[71];
    z[84]=n<T>(95,2)*z[84] + n<T>(3,4)*z[58] + n<T>(133,4)*z[78] + 13*z[75];
    z[85]=z[8] + z[6];
    z[84]=z[85]*z[84];
    z[85]=z[75]*z[8];
    z[99]=z[85]*z[12];
    z[75]=z[6]*z[75]*z[12];
    z[75]=z[75] + z[99];
    z[75]=z[2]*z[75];
    z[137]=z[103]*z[6];
    z[142]= - z[8]*z[103];
    z[142]= - z[137] + z[142];
    z[142]=z[3]*z[142];
    z[75]=n<T>(69,2)*z[75] + n<T>(5,4)*z[142] + z[84];
    z[75]=z[17]*z[75];
    z[84]=59*z[19] + 39*z[21];
    z[84]=z[84]*z[82];
    z[84]=z[84] - z[89];
    z[99]=n<T>(69,2)*z[99] - z[84];
    z[99]=z[8]*z[99];
    z[84]= - z[6]*z[84];
    z[84]=z[84] + z[99];
    z[84]=z[2]*z[84];
    z[75]=z[84] + z[75];
    z[84]=z[96]*z[60];
    z[84]=z[84] + z[44];
    z[99]=n<T>(133,4)*z[46];
    z[84]=z[84]*z[99];
    z[99]=n<T>(739,8)*z[19] + 45*z[21];
    z[84]=n<T>(1,2)*z[99] + z[84] + 4*z[94];
    z[58]=3*z[58] + 133*z[78];
    z[58]=z[6]*z[58];
    z[58]=n<T>(1,16)*z[58] - z[84];
    z[58]=z[6]*z[58];
    z[78]=n<T>(13,4)*z[85] - z[84];
    z[78]=z[8]*z[78];
    z[84]= - 23*z[87] + 51*z[24];
    z[84]=3*z[84];
    z[85]= - z[84] - 5*z[137];
    z[85]=z[6]*z[85];
    z[84]= - z[8]*z[84];
    z[84]=z[85] + z[84];
    z[84]=z[3]*z[84];
    z[85]=z[19] + n<T>(5,4)*z[21];
    z[85]=z[85]*z[12];
    z[85]=z[85] + n<T>(1,4)*z[24];
    z[71]=z[8]*z[71];
    z[71]= - n<T>(1,2)*z[71] - z[85];
    z[71]=z[8]*z[71];
    z[85]= - z[6]*z[85];
    z[71]=z[85] + z[71];
    z[71]=z[11]*z[71];
    z[58]=n<T>(95,4)*z[71] + n<T>(1,16)*z[84] + z[58] + z[78] + n<T>(1,4)*z[75];
    z[58]=z[17]*z[58];
    z[71]=65*z[10];
    z[75]= - z[103]*z[71];
    z[78]=z[52]*z[10];
    z[75]=z[75] + 154*z[24] - 195*z[78];
    z[75]=z[16]*z[75];
    z[84]=z[72] - 1;
    z[84]=z[52]*z[84];
    z[84]=z[93] + z[84];
    z[84]=z[16]*z[84];
    z[85]=z[59]*z[10];
    z[57]=z[84] + z[85] + z[57];
    z[57]=z[57]*z[97];
    z[57]=z[57] + z[75] - 195*z[93] + z[92];
    z[57]=z[7]*z[57];
    z[75]=z[87]*z[71];
    z[75]=z[75] - 171*z[21] + 325*z[118];
    z[75]=z[75]*z[106];
    z[57]=z[57] + z[75] - n<T>(1,4)*z[98] + 260*z[78];
    z[57]=z[7]*z[57];
    z[75]= - z[21]*z[71];
    z[75]=n<T>(53,2)*z[1] + z[75];
    z[75]=z[16]*z[75];
    z[57]=z[57] + z[75] - n<T>(325,2)*z[118] + 124*z[21] - 75*z[19];
    z[57]=z[7]*z[57];
    z[75]=10*z[21];
    z[84]=z[75] + z[19];
    z[84]=z[12]*z[84];
    z[84]=62*z[24] + z[84];
    z[84]=z[12]*z[84];
    z[84]= - 62*z[52] + z[84];
    z[92]=z[150] + z[140];
    z[92]=z[92]*z[12];
    z[79]= - z[79] + z[92];
    z[79]=z[12]*z[79];
    z[79]=z[80] + z[79];
    z[79]=z[9]*z[79];
    z[79]=3*z[84] + z[79];
    z[79]=z[9]*z[79];
    z[80]= - 55*z[21] + 359*z[19];
    z[80]=z[80]*z[82];
    z[79]=z[79] + 186*z[24] + z[80];
    z[79]=z[9]*z[79];
    z[79]=z[79] - 75*z[21] + 34*z[19];
    z[79]=z[9]*z[79];
    z[80]=z[101]*z[47];
    z[82]=z[39]*z[120];
    z[80]=3*z[80] + z[82];
    z[80]=z[13]*z[80];
    z[82]=z[44]*z[109];
    z[80]=z[82] + z[80];
    z[80]=z[13]*z[80];
    z[80]=z[80] + n<T>(101,16)*z[26] + z[153];
    z[80]=z[13]*z[80];
    z[82]=z[44]*z[46];
    z[84]=8*z[19];
    z[98]=n<T>(133,16)*z[82] - n<T>(5,16)*z[21] + z[84];
    z[98]=z[6]*z[98];
    z[99]=z[59]*z[9];
    z[99]= - z[99] + 2*z[56];
    z[99]=z[99]*z[9];
    z[101]=2*z[52];
    z[99]=z[99] - z[101];
    z[120]=z[9]*z[99];
    z[120]=z[24] + z[120];
    z[120]=z[120]*z[116];
    z[120]=n<T>(37,4)*z[21] + z[120];
    z[120]=z[10]*z[120];
    z[140]=n<T>(185,4)*z[1] + 113*z[12];
    z[57]=z[98] + z[80] + z[57] + z[120] + n<T>(1,4)*z[140] + z[79];
    z[57]=z[6]*z[57];
    z[79]=3*z[21];
    z[80]=z[79] + z[31];
    z[80]=z[80]*z[12];
    z[98]=z[121] + z[80];
    z[98]=z[98]*z[72];
    z[80]=z[80] + z[98];
    z[98]= - z[42] - z[25];
    z[98]=z[12]*z[98];
    z[98]=z[121] + z[98];
    z[98]=z[12]*z[98];
    z[98]=z[53] + z[98];
    z[98]=z[10]*z[98];
    z[80]=z[98] + 2*z[80];
    z[80]=z[16]*z[80];
    z[98]=z[93]*z[134];
    z[120]=z[152]*z[72];
    z[134]= - z[98] + z[120];
    z[134]=z[16]*z[134];
    z[140]=z[37]*z[10];
    z[134]=z[140] + z[134];
    z[134]=z[13]*z[134];
    z[142]=z[16]*z[130];
    z[134]=z[134] - z[39] + z[142];
    z[41]=z[134]*z[41];
    z[134]=z[139]*z[26];
    z[29]=z[121] + z[29];
    z[29]=z[12]*z[29];
    z[29]=z[53] + z[29];
    z[29]=z[12]*z[29];
    z[29]=z[61] + z[29];
    z[29]=z[10]*z[29];
    z[29]=z[41] + z[80] - 2*z[134] + z[29];
    z[29]=z[29]*z[105];
    z[27]=z[27]*z[72];
    z[27]=z[19] + z[27];
    z[41]=4*z[24];
    z[61]= - z[41] - z[62];
    z[61]=z[10]*z[61];
    z[27]=100*z[61] - n<T>(119,4)*z[21] - 150*z[27];
    z[27]=z[16]*z[27];
    z[61]=z[126]*z[72];
    z[80]=z[130]*z[10];
    z[134]=z[80] - z[61];
    z[134]=z[16]*z[134];
    z[40]=z[40]*z[10];
    z[134]= - z[40] + z[134];
    z[134]=z[13]*z[134];
    z[142]= - z[26]*z[103];
    z[134]=z[134] + z[47] + z[142];
    z[48]=z[134]*z[48];
    z[134]= - n<T>(719,4)*z[21] - 50*z[19];
    z[134]=z[12]*z[134];
    z[38]=z[38] + z[79];
    z[38]=z[38]*z[12];
    z[142]= - z[89] - z[38];
    z[142]=z[12]*z[142];
    z[143]=7*z[52];
    z[142]= - z[143] + z[142];
    z[145]=50*z[10];
    z[142]=z[142]*z[145];
    z[27]=z[29] + 50*z[48] + z[27] + z[134] + z[142];
    z[27]=z[4]*z[27];
    z[29]=z[79] + z[19];
    z[48]=25*z[10];
    z[29]=z[29]*z[48];
    z[29]= - n<T>(23,2)*z[1] + z[29];
    z[134]=z[94]*z[145];
    z[29]=3*z[29] + z[134];
    z[29]=z[16]*z[29];
    z[134]=z[144]*z[10];
    z[142]=z[45]*z[103];
    z[150]= - z[134] + z[142];
    z[150]=z[16]*z[150];
    z[153]=z[47]*z[10];
    z[150]=4*z[153] + z[150];
    z[150]=z[13]*z[150];
    z[154]=z[16]*z[60];
    z[150]=z[150] - z[44] + z[154];
    z[74]=z[150]*z[74];
    z[20]=z[75] + z[20];
    z[20]=z[12]*z[20];
    z[20]=15*z[24] + z[20];
    z[20]=z[20]*z[48];
    z[20]=z[27] + z[74] + z[29] + z[20] + n<T>(445,8)*z[21] + 39*z[19];
    z[20]=z[4]*z[20];
    z[27]= - z[42] - z[19];
    z[27]=z[10]*z[27];
    z[29]= - z[145]*z[82];
    z[48]= - z[1]*z[145];
    z[48]= - n<T>(173,8) + z[48];
    z[48]=z[16]*z[48];
    z[20]=z[20] + z[29] + z[48] + 75*z[27] - z[132] + n<T>(383,8)*z[12];
    z[20]=z[4]*z[20];
    z[27]=z[32] + z[94];
    z[29]=7*z[21] + z[84];
    z[29]=z[12]*z[29];
    z[29]=z[121] + z[29];
    z[29]=z[10]*z[29];
    z[27]=z[29] - 2*z[27];
    z[27]=z[26]*z[27];
    z[29]= - z[94] + z[30];
    z[29]=z[7]*z[33]*z[29];
    z[27]=z[29] + z[27];
    z[27]=z[27]*z[97];
    z[28]= - z[90]*z[28];
    z[29]=n<T>(13,2)*z[21] + 9*z[19];
    z[29]=z[12]*z[29];
    z[29]=n<T>(3,2)*z[24] + z[29];
    z[29]=z[29]*z[45];
    z[28]=z[28] + z[29];
    z[27]=z[27] + 65*z[28] - n<T>(133,4)*z[111];
    z[27]=z[7]*z[27];
    z[28]=z[79] + n<T>(11,2)*z[19];
    z[28]=z[12]*z[28];
    z[28]=z[112] + z[28];
    z[28]=z[28]*z[71];
    z[27]=z[27] - n<T>(121,4)*z[94] + z[28] - 8*z[21] - 35*z[19];
    z[27]=z[7]*z[27];
    z[28]=z[16]*z[126];
    z[28]= - 65*z[130] - 24*z[28];
    z[28]=z[16]*z[28];
    z[28]= - 130*z[39] + z[28];
    z[28]=z[7]*z[28];
    z[29]=z[10]*z[39];
    z[28]=260*z[29] + z[28];
    z[28]=z[28]*z[147];
    z[29]= - z[16]*z[152];
    z[29]= - z[156] + z[29];
    z[29]=z[16]*z[29];
    z[29]= - z[37] + z[29];
    z[29]=z[7]*z[29];
    z[29]=3*z[140] + z[29];
    z[29]=z[29]*z[149];
    z[30]=z[138]*z[36];
    z[29]=z[29] + z[30];
    z[30]=65*z[13];
    z[29]=z[29]*z[30];
    z[28]=z[28] + z[29];
    z[28]=z[13]*z[28];
    z[29]=z[103]*z[12];
    z[32]= - z[144] - z[29];
    z[32]=z[16]*z[32];
    z[32]= - 130*z[47] + n<T>(79,4)*z[32];
    z[32]=z[7]*z[32];
    z[32]=195*z[153] + z[32];
    z[32]=z[7]*z[32];
    z[28]=z[32] + z[28];
    z[28]=z[13]*z[28];
    z[32]=z[72]*z[110];
    z[32]=z[70] + z[32];
    z[33]= - z[124] + n<T>(47,4)*z[87];
    z[33]=z[16]*z[33];
    z[33]= - n<T>(127,4)*z[44] + z[33];
    z[33]=z[7]*z[33];
    z[28]=z[28] + 65*z[32] + z[33];
    z[28]=z[28]*z[46];
    z[32]= - z[143] - z[51];
    z[32]=z[12]*z[32];
    z[33]=z[9]*z[55];
    z[32]=z[33] - 8*z[56] + z[32];
    z[32]=z[9]*z[32];
    z[33]=13*z[24] + z[62];
    z[33]=z[33]*z[65];
    z[32]=z[32] + 9*z[52] + z[33];
    z[32]=z[9]*z[32];
    z[33]= - z[79] - z[64];
    z[33]=z[12]*z[33];
    z[32]=z[32] - n<T>(11,2)*z[24] + z[33];
    z[32]=z[32]*z[116];
    z[32]= - 7*z[94] + z[32] + 179*z[21] + n<T>(511,4)*z[19];
    z[32]=z[10]*z[32];
    z[33]=405*z[24] + z[124];
    z[33]=z[12]*z[33];
    z[36]= - z[76] - z[50];
    z[36]=z[12]*z[36];
    z[36]= - z[81] + z[36];
    z[36]=z[9]*z[36];
    z[37]=465*z[52];
    z[33]=z[36] + z[37] + z[33];
    z[33]=z[9]*z[33];
    z[36]= - n<T>(411,2)*z[21] - z[19];
    z[36]=z[12]*z[36];
    z[33]=z[33] - n<T>(1369,4)*z[24] + z[36];
    z[33]=z[9]*z[33];
    z[25]=z[146] + z[25];
    z[25]=11*z[25] + z[33];
    z[25]=z[9]*z[25];
    z[33]= - 69*z[21] + 95*z[19];
    z[33]=z[8]*z[33];
    z[25]=n<T>(1,16)*z[33] + z[28] + z[27] - n<T>(309,16)*z[1] + z[25] + z[32];
    z[25]=z[8]*z[25];
    z[27]=25*z[88] + n<T>(347,8)*z[21] + 125*z[118];
    z[27]=z[16]*z[27];
    z[28]= - z[10]*z[103];
    z[28]= - z[115] + z[28];
    z[28]=z[16]*z[28];
    z[32]=z[52]*z[72];
    z[32]=z[93] + z[32];
    z[32]=z[16]*z[32];
    z[32]=z[85] + z[32];
    z[32]=z[4]*z[32];
    z[28]=z[32] - 3*z[93] + z[28];
    z[28]=z[28]*z[105];
    z[27]=z[28] + 200*z[78] + z[27];
    z[27]=z[4]*z[27];
    z[28]= - n<T>(53,8)*z[21] - 25*z[118];
    z[32]= - z[21]*z[145];
    z[32]= - n<T>(197,4)*z[1] + z[32];
    z[32]=z[16]*z[32];
    z[27]=z[27] + 5*z[28] + z[32];
    z[27]=z[4]*z[27];
    z[28]=z[99]*z[116];
    z[28]=n<T>(335,4)*z[24] + z[28];
    z[28]=z[9]*z[28];
    z[28]= - n<T>(303,16)*z[21] + z[28] + z[83];
    z[28]=z[8]*z[28];
    z[32]=n<T>(133,4)*z[108];
    z[33]=z[32] + n<T>(39,4)*z[21] - 43*z[94];
    z[33]=z[33]*z[133];
    z[36]= - z[66] + z[122];
    z[36]=z[9]*z[36];
    z[36]=z[68] + z[36];
    z[36]=z[36]*z[116];
    z[36]= - z[125] + z[36];
    z[36]=z[9]*z[36];
    z[39]= - z[59]*z[109];
    z[39]=z[129] + z[39];
    z[39]=z[9]*z[39];
    z[39]= - z[143] + z[39];
    z[39]=z[9]*z[39];
    z[39]=z[135] + z[39];
    z[39]=z[39]*z[67];
    z[39]= - n<T>(43,4)*z[21] + z[39];
    z[39]=z[39]*z[69];
    z[27]=z[33] + z[28] + z[27] + n<T>(13,8)*z[114] + n<T>(345,16)*z[16] + z[39]
    + n<T>(371,16)*z[1] + z[36];
    z[27]=z[3]*z[27];
    z[28]=21*z[94] + n<T>(25,4)*z[21];
    z[33]=z[89] - z[87];
    z[36]=n<T>(39,4)*z[6];
    z[33]=z[33]*z[36];
    z[28]=5*z[28] + z[33] - z[32];
    z[32]=z[102]*npow(z[16],3);
    z[33]=211*z[21] - 305*z[94];
    z[33]=z[16]*z[33];
    z[33]=39*z[137] + z[33] + 133*z[32];
    z[33]=z[33]*z[133];
    z[33]=z[33] + z[28];
    z[33]=z[3]*z[33];
    z[36]=z[16]*z[95];
    z[28]=n<T>(165,4)*z[36] + z[28];
    z[28]=z[11]*z[28];
    z[36]=z[22] - z[94];
    z[36]=z[36]*z[96];
    z[36]=z[36] - z[24];
    z[39]= - z[106]*z[113];
    z[39]=z[39] - z[36];
    z[39]=z[11]*z[39];
    z[36]= - z[3]*z[36];
    z[36]=z[36] + z[39];
    z[36]=z[2]*z[36];
    z[28]=35*z[36] + z[33] + z[28];
    z[33]= - n<T>(665,16)*z[131] - n<T>(95,16)*z[119] + 82*z[1];
    z[36]=z[102]*z[107];
    z[39]=n<T>(305,2)*z[94] - 133*z[36];
    z[39]=z[3]*z[39];
    z[39]=n<T>(1,8)*z[39] - z[33];
    z[39]=z[3]*z[39];
    z[45]=5*z[1];
    z[46]=z[45]*z[8];
    z[46]=z[46] + 7*z[104];
    z[48]=z[3]*z[63];
    z[48]=n<T>(7,4)*z[48] - z[46];
    z[48]=z[3]*z[48];
    z[51]=z[1] - z[119];
    z[51]=z[11]*z[51];
    z[46]=n<T>(5,4)*z[51] - z[46];
    z[46]=z[11]*z[46];
    z[46]=z[48] + z[46];
    z[46]=z[14]*z[46];
    z[48]=z[11]*z[94];
    z[33]= - n<T>(225,16)*z[48] - z[33];
    z[33]=z[11]*z[33];
    z[33]=n<T>(19,4)*z[46] + z[39] + z[33];
    z[33]=z[14]*z[33];
    z[39]= - z[146] + 35*z[94];
    z[39]=z[39]*z[16];
    z[32]=z[39] - n<T>(133,4)*z[32] - n<T>(39,4)*z[137];
    z[39]=z[11] + z[3];
    z[32]= - z[32]*z[39];
    z[46]= - z[3]*z[73];
    z[46]=z[46] - z[113];
    z[46]=z[2]*z[16]*z[46];
    z[32]=n<T>(35,2)*z[46] + z[32];
    z[22]=z[22]*z[8];
    z[22]=z[45] - z[22] + 7*z[63];
    z[22]=z[14]*z[22];
    z[22]=n<T>(19,16)*z[22] - n<T>(133,8)*z[36] + 5*z[94];
    z[22]=z[14]*z[39]*z[22];
    z[22]=n<T>(1,4)*z[32] + z[22];
    z[22]=z[18]*z[22];
    z[22]=z[22] + n<T>(1,4)*z[28] + z[33];
    z[22]=z[18]*z[22];
    z[28]=z[10]*z[148];
    z[32]=z[139]*z[72];
    z[28]=65*z[32] - 195*z[28] - z[54] + 58*z[19];
    z[28]=z[16]*z[28];
    z[32]= - z[12]*z[148];
    z[32]=z[52] + z[32];
    z[32]=z[10]*z[32];
    z[33]=z[148]*z[72];
    z[32]=z[33] + z[32] - z[77];
    z[32]=z[16]*z[32];
    z[33]=z[24] - z[43];
    z[33]=z[12]*z[33];
    z[33]= - z[52] + z[33];
    z[33]=z[12]*z[33];
    z[33]=z[56] + z[33];
    z[33]=z[10]*z[33];
    z[32]=z[32] - z[50] + z[33];
    z[32]=z[32]*z[97];
    z[33]=z[68] - z[38];
    z[33]=z[12]*z[33];
    z[33]= - z[53] + z[33];
    z[33]=z[33]*z[71];
    z[24]=z[32] + z[28] + z[33] + 24*z[24] + 65*z[60];
    z[24]=z[7]*z[24];
    z[28]=z[10]*z[139];
    z[32]=z[94]*z[71];
    z[28]=z[32] - n<T>(87,2)*z[1] - 325*z[28];
    z[28]=z[28]*z[106];
    z[32]=125*z[21] + 97*z[19];
    z[33]= - z[136] - n<T>(5,2)*z[19];
    z[33]=z[12]*z[33];
    z[33]=z[41] + z[33];
    z[33]=z[33]*z[71];
    z[24]=z[24] + z[28] + n<T>(3,4)*z[32] + z[33];
    z[24]=z[7]*z[24];
    z[28]=z[10]*z[127];
    z[32]= - 9*z[1] - n<T>(23,4)*z[12];
    z[33]= - z[1]*z[71];
    z[33]=n<T>(21,2) + z[33];
    z[33]=z[16]*z[33];
    z[24]=z[24] + z[33] + 3*z[32] - n<T>(65,2)*z[28];
    z[24]=z[7]*z[24];
    z[28]= - z[80] - z[61];
    z[28]=z[16]*z[28];
    z[26]=z[7]*z[26]*z[107]*z[53];
    z[26]=z[26] - z[40] + z[28];
    z[26]=z[26]*z[149];
    z[28]= - z[98] - z[120];
    z[28]=z[16]*z[28];
    z[28]= - z[140] + z[28];
    z[28]=z[28]*z[151];
    z[26]=z[26] + z[28];
    z[26]=z[26]*z[30];
    z[28]= - z[134] - z[142];
    z[28]=z[28]*z[106];
    z[28]= - 2*z[153] + z[28];
    z[29]= - z[144] + z[29];
    z[29]=z[7]*z[16]*z[29];
    z[28]=65*z[28] + 48*z[29];
    z[28]=z[28]*z[147];
    z[29]=z[117]*z[47];
    z[26]=z[26] + z[29] + z[28];
    z[26]=z[13]*z[26];
    z[28]= - 189*z[60] + 79*z[87];
    z[28]=z[16]*z[28];
    z[28]=181*z[44] + z[28];
    z[28]=z[7]*z[28];
    z[29]= - z[44]*z[71];
    z[28]=z[29] + n<T>(1,4)*z[28];
    z[28]=z[7]*z[28];
    z[29]=z[44]*z[123];
    z[26]=z[26] + z[29] + z[28];
    z[26]=z[13]*z[26];
    z[28]=z[9]*z[35];
    z[26]=z[26] + z[28] - n<T>(69,16)*z[16];
    z[26]=z[13]*z[26];
    z[28]=z[100] + z[31];
    z[28]=z[12]*z[28];
    z[29]=z[37] + z[92];
    z[29]=z[9]*z[29];
    z[28]=z[29] - z[141] + z[28];
    z[28]=z[9]*z[28];
    z[19]=1127*z[21] + 351*z[19];
    z[19]=n<T>(1,4)*z[19] + z[28];
    z[19]=z[9]*z[19];
    z[19]=z[19] - n<T>(33,4)*z[1] + z[12];
    z[19]=z[9]*z[19];
    z[21]=z[101] - z[128];
    z[21]=z[21]*z[109];
    z[21]= - z[89] + z[21];
    z[21]=z[9]*z[21];
    z[21]=z[42] + z[21];
    z[21]=z[9]*z[21];
    z[21]=11*z[1] + 186*z[21];
    z[21]=z[10]*z[21];

    r +=  - n<T>(61,16) + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + 
      z[25] + z[26] + z[27] + 5*z[34] + z[49] + z[57] + z[58] + z[86]
       + z[91];
 
    return r;
}

template double qg_2lNLC_r1242(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1242(const std::array<dd_real,31>&);
#endif
