#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r385(const std::array<T,31>& k) {
  T z[24];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[8];
    z[6]=k[12];
    z[7]=k[10];
    z[8]=k[14];
    z[9]=k[15];
    z[10]=k[2];
    z[11]=k[4];
    z[12]=z[5]*z[2];
    z[13]= - n<T>(1,2) - z[12];
    z[14]=z[2]*z[6];
    z[15]=z[14] + 1;
    z[16]=npow(z[5],2);
    z[13]=z[16]*z[15]*z[13];
    z[17]=z[9] - z[8];
    z[17]=z[17]*z[7];
    z[18]=z[9]*z[8];
    z[13]=z[13] - z[18] + z[17];
    z[13]=z[4]*z[13];
    z[19]=z[7]*z[9];
    z[20]= - z[18] + n<T>(1,2)*z[19];
    z[20]=z[7]*z[20];
    z[21]=npow(z[5],3);
    z[22]=n<T>(1,2) + z[14];
    z[22]=z[22]*z[21];
    z[14]= - z[21]*z[14];
    z[23]= - z[7]*z[18];
    z[14]=z[23] + z[14];
    z[14]=z[4]*z[14];
    z[21]=z[6]*z[21];
    z[14]=z[21] + z[14];
    z[21]=n<T>(1,2)*z[1];
    z[14]=z[14]*z[21];
    z[13]=z[14] + z[13] + z[20] + z[22];
    z[13]=z[21]*z[13];
    z[12]= - z[12] - 1;
    z[14]=z[15]*z[2];
    z[14]=z[14] - z[11];
    z[12]=z[16]*z[14]*z[12];
    z[14]= - z[11]*z[17];
    z[15]=n<T>(1,2)*z[18];
    z[17]= - z[2]*z[15];
    z[12]=n<T>(1,2)*z[12] + z[14] + z[17];
    z[12]=z[4]*z[12];
    z[14]=z[10] - z[11];
    z[14]=z[14]*z[19];
    z[17]=n<T>(1,2)*z[8];
    z[17]= - z[10]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[9]*z[17];
    z[14]=n<T>(1,4)*z[14] - z[8] + z[17];
    z[14]=z[7]*z[14];
    z[17]=z[5]*z[6]*npow(z[2],2);
    z[17]= - static_cast<T>(1)+ z[17];
    z[16]=z[17]*z[16];
    z[12]=z[13] + n<T>(1,2)*z[12] + n<T>(1,4)*z[16] - z[15] + z[14];

    r += n<T>(3,4)*z[12]*npow(z[3],2)*npow(z[1],2);
 
    return r;
}

template double qg_2lNLC_r385(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r385(const std::array<dd_real,31>&);
#endif
