#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r903(const std::array<T,31>& k) {
  T z[64];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[6];
    z[9]=k[7];
    z[10]=k[3];
    z[11]=k[2];
    z[12]=k[9];
    z[13]=k[13];
    z[14]=6*z[7];
    z[15]=z[7]*z[2];
    z[16]=3*z[15];
    z[17]= - static_cast<T>(5)+ z[16];
    z[17]=z[17]*z[14];
    z[17]=13*z[10] + z[17];
    z[17]=z[7]*z[17];
    z[18]=npow(z[10],2);
    z[17]= - z[18] + z[17];
    z[19]=2*z[9];
    z[17]=z[17]*z[19];
    z[20]=5*z[10];
    z[21]=7*z[15];
    z[22]=static_cast<T>(6)- z[21];
    z[22]=z[7]*z[22];
    z[17]=z[17] - z[20] + 9*z[22];
    z[17]=z[9]*z[17];
    z[22]=3*z[10];
    z[21]=static_cast<T>(10)- z[21];
    z[21]=z[7]*z[21];
    z[21]= - z[22] + z[21];
    z[23]=3*z[7];
    z[21]=z[21]*z[23];
    z[24]=4*z[15];
    z[25]= - static_cast<T>(9)+ z[24];
    z[25]=z[7]*z[25];
    z[25]=6*z[10] + z[25];
    z[25]=z[7]*z[25];
    z[25]= - z[18] + z[25];
    z[26]=2*z[7];
    z[27]=z[26]*z[9];
    z[25]=z[25]*z[27];
    z[21]=z[21] + z[25];
    z[21]=z[9]*z[21];
    z[25]=2*z[2];
    z[28]=z[25]*z[6];
    z[29]=14*z[15] - static_cast<T>(11)- z[28];
    z[29]=z[7]*z[29];
    z[21]=z[29] + z[21];
    z[21]=z[4]*z[21];
    z[29]=z[2]*z[6];
    z[30]=z[6]*z[5];
    z[31]= - z[29] - static_cast<T>(1)- z[30];
    z[32]=npow(z[8],2);
    z[33]=z[2]*z[5];
    z[33]= - 3*z[32] + z[33];
    z[33]=z[33]*z[26];
    z[34]=4*z[5];
    z[33]=z[33] - z[34] + 19*z[2];
    z[33]=z[7]*z[33];
    z[17]=z[21] + z[17] + 2*z[31] + z[33];
    z[17]=z[4]*z[17];
    z[21]=5*z[32];
    z[31]=z[21]*z[15];
    z[33]=npow(z[12],2);
    z[35]=3*z[33];
    z[36]=z[5]*z[12];
    z[37]= - z[35] - 4*z[36];
    z[38]=z[32]*z[6];
    z[39]=6*z[5] + z[38];
    z[39]=z[2]*z[39];
    z[37]=z[31] + 2*z[37] + z[39];
    z[37]=z[7]*z[37];
    z[39]=z[32]*z[7];
    z[21]=z[21]*z[10];
    z[40]=6*z[13];
    z[41]=20*z[39] - 33*z[2] + z[40] - z[21];
    z[41]=z[7]*z[41];
    z[42]= - static_cast<T>(14)+ 13*z[15];
    z[42]=z[7]*z[42];
    z[42]=z[22] + z[42];
    z[43]=4*z[9];
    z[42]=z[42]*z[43];
    z[41]=z[42] - static_cast<T>(4)+ z[41];
    z[41]=z[9]*z[41];
    z[42]=z[3]*z[6];
    z[44]=z[42] - 6;
    z[45]= - z[44]*z[25];
    z[46]= - 4*z[12] - 11*z[5];
    z[47]= - static_cast<T>(11)- 6*z[30];
    z[47]=z[3]*z[47];
    z[17]=z[17] + z[41] + z[37] + z[45] + 2*z[46] + z[47];
    z[17]=z[4]*z[17];
    z[37]=npow(z[8],3);
    z[41]=z[26]*z[37];
    z[45]=2*z[15];
    z[46]= - z[29] + z[45];
    z[46]=z[46]*z[41];
    z[47]=3*z[5];
    z[48]=z[2]*z[47];
    z[46]=z[48] + z[46];
    z[46]=z[7]*z[46];
    z[48]=z[37]*z[10];
    z[41]= - z[48] + z[41];
    z[41]=z[7]*z[41];
    z[49]=7*z[2];
    z[41]= - z[49] + z[41];
    z[41]=z[41]*z[23];
    z[50]=z[3]*z[10];
    z[51]= - static_cast<T>(15)+ 16*z[15];
    z[51]=z[7]*z[51];
    z[51]=2*z[10] + z[51];
    z[51]=z[9]*z[51];
    z[41]=z[51] + z[41] + static_cast<T>(6)+ z[50];
    z[41]=z[41]*z[19];
    z[51]=z[5] + 9*z[2];
    z[52]= - z[5]*z[15];
    z[51]=2*z[51] + z[52];
    z[51]=z[7]*z[51];
    z[45]= - static_cast<T>(3)+ z[45];
    z[45]=z[7]*z[45];
    z[45]=z[10] + z[45];
    z[45]=z[45]*z[27];
    z[52]=9*z[15];
    z[53]=static_cast<T>(8)- z[52];
    z[53]=z[7]*z[53];
    z[45]=z[45] - z[10] + z[53];
    z[45]=z[9]*z[45];
    z[45]=3*z[45] + z[51] - static_cast<T>(6)+ z[30];
    z[45]=z[4]*z[45];
    z[51]=2*z[5];
    z[53]=static_cast<T>(1)- z[30];
    z[53]=z[3]*z[53];
    z[41]=z[45] + z[41] + z[46] + z[49] - z[51] + z[53];
    z[41]=z[4]*z[41];
    z[45]=2*z[18];
    z[46]= - z[37]*z[45];
    z[49]=z[37]*z[7];
    z[53]=4*z[48] - 5*z[49];
    z[53]=z[53]*z[26];
    z[54]=11*z[2];
    z[46]=z[53] + z[46] + z[54];
    z[46]=z[7]*z[46];
    z[46]= - static_cast<T>(4)+ z[46];
    z[46]=z[46]*z[19];
    z[53]=z[37]*npow(z[7],3);
    z[53]= - 15*z[53] - 5;
    z[53]=z[2]*z[53];
    z[55]=12*z[13];
    z[50]=z[50] - 1;
    z[56]=z[3]*z[50];
    z[46]=z[46] + z[55] + z[56] + z[53];
    z[46]=z[9]*z[46];
    z[53]=z[30]*z[3];
    z[34]= - z[34] - z[53];
    z[34]=z[3]*z[34];
    z[56]=z[33]*z[5];
    z[57]=z[14]*z[56];
    z[58]=5*z[36];
    z[44]= - z[3]*z[44];
    z[44]=12*z[5] + z[44];
    z[44]=z[2]*z[44];
    z[34]=z[41] + z[46] + z[57] + z[44] + z[58] + z[34];
    z[34]=z[4]*z[34];
    z[41]= - z[15] + 1;
    z[41]=z[47]*z[41];
    z[44]=static_cast<T>(5)+ z[42];
    z[44]=z[44]*z[25];
    z[46]= - static_cast<T>(3)+ z[24];
    z[27]=z[46]*z[27];
    z[46]=static_cast<T>(2)- 5*z[15];
    z[27]=3*z[46] + z[27];
    z[27]=z[9]*z[27];
    z[27]=z[27] + z[44] + z[53] + z[41];
    z[27]=z[4]*z[27];
    z[41]=z[10] - z[7];
    z[23]=z[23]*z[41];
    z[23]= - z[18] + z[23];
    z[23]=z[7]*z[23]*npow(z[8],4);
    z[23]=5*z[2] + z[23];
    z[23]=z[7]*z[23];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[19];
    z[41]=z[7]*z[8];
    z[44]=npow(z[41],4);
    z[46]= - 6*z[44] - 9;
    z[46]=z[2]*z[46];
    z[53]=2*z[3];
    z[23]=z[23] - z[53] + z[46];
    z[23]=z[9]*z[23];
    z[46]=z[2]*z[3];
    z[57]=z[5] - z[53];
    z[57]=z[3]*z[57];
    z[23]=z[27] + z[23] + z[57] + z[46];
    z[23]=z[4]*z[23];
    z[27]=z[47]*z[46];
    z[57]=npow(z[9],2);
    z[44]=z[2]*z[57]*z[44];
    z[27]=z[27] + 4*z[44];
    z[23]=2*z[27] + z[23];
    z[23]=z[4]*z[23];
    z[27]=z[57]*z[25]*npow(z[41],5);
    z[41]=static_cast<T>(2)+ z[42];
    z[41]=z[3]*z[41];
    z[41]= - z[47] + z[41];
    z[41]=z[2]*z[41];
    z[44]=z[15]*z[19];
    z[44]= - 3*z[2] + z[44];
    z[44]=z[9]*z[44];
    z[57]=z[3]*z[5];
    z[41]=z[44] + z[57] + z[41];
    z[41]=z[4]*z[41];
    z[44]= - z[5] + z[3];
    z[44]=z[44]*z[46];
    z[27]=z[41] + z[44] + z[27];
    z[27]=z[27]*npow(z[4],2);
    z[41]= - z[1]*z[5]*npow(z[4],3)*z[46];
    z[27]=z[27] + z[41];
    z[27]=z[1]*z[27];
    z[23]=z[23] + z[27];
    z[23]=z[1]*z[23];
    z[27]=npow(z[6],2);
    z[41]=z[37]*z[27]*z[25];
    z[16]= - z[28] + z[16];
    z[16]=z[16]*z[49];
    z[16]=z[41] + z[16];
    z[41]=z[9]*z[7];
    z[16]=z[16]*z[41];
    z[44]=npow(z[42],2)*z[48];
    z[16]=z[44] + z[16];
    z[16]=z[16]*z[19];
    z[44]=2*z[11];
    z[46]=z[5]*z[44];
    z[46]=z[46] - z[30];
    z[46]=z[6]*z[46];
    z[48]=npow(z[11],2);
    z[49]=z[48]*z[5];
    z[46]= - z[49] + z[46];
    z[37]=z[6]*npow(z[3],2)*z[37]*z[46];
    z[46]=z[33]*z[44];
    z[46]=z[51] + z[46] + 3*z[12];
    z[46]=3*z[46] - z[3];
    z[46]=z[3]*z[46];
    z[46]= - 6*z[33] + z[46];
    z[46]=z[2]*z[46];
    z[16]=z[23] + z[34] + z[16] + 8*z[37] + z[46];
    z[16]=z[1]*z[16];
    z[23]=2*z[12];
    z[34]=z[33]*z[11];
    z[37]=z[23] + z[34];
    z[37]=z[11]*z[37];
    z[46]=2*z[32];
    z[57]= - z[48]*z[46];
    z[59]=z[44]*z[32];
    z[60]=z[59] - z[38];
    z[61]=2*z[6];
    z[60]=z[60]*z[61];
    z[62]=z[5]*z[11];
    z[63]=z[3]*z[44];
    z[37]=z[63] + z[60] + z[57] + 5*z[62] + static_cast<T>(7)+ 6*z[37];
    z[37]=z[3]*z[37];
    z[27]=z[27]*z[32];
    z[57]= - z[27] - 1;
    z[57]=z[40]*z[57];
    z[57]=z[51] - 6*z[34] + z[12] + z[57];
    z[37]=2*z[57] + z[37];
    z[37]=z[2]*z[37];
    z[21]= - 7*z[39] + 13*z[2] + z[21] + 4*z[38];
    z[21]=z[7]*z[21];
    z[39]= - z[18]*z[32];
    z[57]= - z[10]*z[32];
    z[57]=z[57] - z[38];
    z[57]=z[57]*z[61];
    z[21]=z[21] + z[57] - static_cast<T>(7)+ z[39];
    z[21]=z[21]*z[19];
    z[39]=4*z[29];
    z[57]= - z[32]*z[39];
    z[31]=z[57] - z[31];
    z[31]=z[7]*z[31];
    z[57]= - z[32]*z[22];
    z[60]=5*z[38];
    z[57]=z[57] - z[60];
    z[57]=z[6]*z[57];
    z[57]= - static_cast<T>(1)+ z[57];
    z[57]=z[3]*z[57];
    z[57]=z[55] + z[57];
    z[27]=static_cast<T>(11)+ 6*z[27];
    z[27]=z[2]*z[27];
    z[21]=z[21] + z[31] + 2*z[57] + z[27];
    z[21]=z[9]*z[21];
    z[27]=z[47]*z[11];
    z[31]=z[32]*z[27];
    z[57]= - z[30]*z[46];
    z[31]=z[31] + z[57];
    z[31]=z[6]*z[31];
    z[32]=4*z[32];
    z[32]= - z[49]*z[32];
    z[34]=3*z[34];
    z[31]=4*z[31] + z[32] - z[5] - z[12] - z[34];
    z[32]=5*z[11];
    z[57]=z[32] - z[10];
    z[46]=z[57]*z[46];
    z[46]= - z[60] - z[47] + z[46];
    z[46]=z[46]*z[61];
    z[57]= - z[57]*z[59];
    z[46]=z[46] + static_cast<T>(1)+ z[57];
    z[46]=z[3]*z[46];
    z[31]=2*z[31] + z[46];
    z[31]=z[3]*z[31];
    z[35]= - z[35] + z[36];
    z[36]=z[13]*z[38];
    z[33]=z[33] + z[36];
    z[33]=z[2]*z[33];
    z[33]=z[56] + z[33];
    z[14]=z[33]*z[14];
    z[14]=z[16] + z[17] + z[21] + z[14] + z[37] + 2*z[35] + z[31];
    z[14]=z[1]*z[14];
    z[16]=z[10]*z[42];
    z[16]=z[16] + z[22] + 11*z[6];
    z[16]=z[3]*z[16];
    z[17]=3*z[13];
    z[21]= - z[17] + z[2];
    z[21]=z[21]*z[26];
    z[31]=6*z[15] - static_cast<T>(9)- z[39];
    z[31]=z[7]*z[31];
    z[33]=z[10] + z[6];
    z[31]=3*z[33] + z[31];
    z[31]=z[31]*z[43];
    z[33]= - z[6]*z[55];
    z[16]=z[31] + z[21] - 16*z[29] + z[16] - static_cast<T>(29)+ z[33];
    z[16]=z[9]*z[16];
    z[21]=z[23] - z[34];
    z[21]=z[21]*z[44];
    z[23]=z[62] - z[30];
    z[23]=static_cast<T>(9)- 11*z[23];
    z[23]=z[6]*z[23];
    z[23]=z[23] - z[10] - 9*z[11];
    z[23]=z[3]*z[23];
    z[31]=8*z[62];
    z[21]=z[23] + 10*z[30] - z[31] - static_cast<T>(21)+ z[21];
    z[21]=z[3]*z[21];
    z[23]=z[11]*z[12];
    z[33]= - static_cast<T>(2)+ 3*z[23];
    z[33]=z[11]*z[33];
    z[33]=z[33] + z[49];
    z[34]= - z[6]*z[11];
    z[34]=z[48] + z[34];
    z[34]=z[34]*z[53];
    z[33]=z[34] + 3*z[33] - z[61];
    z[33]=z[3]*z[33];
    z[34]= - z[12]*z[44];
    z[34]=static_cast<T>(7)+ z[34];
    z[35]=z[6]*z[13];
    z[33]=z[33] + 24*z[35] + 2*z[34] - z[27];
    z[33]=z[2]*z[33];
    z[34]=4*z[10];
    z[35]=static_cast<T>(13)- z[52];
    z[35]=z[7]*z[35];
    z[35]= - z[34] + z[35];
    z[35]=z[35]*z[26];
    z[36]=8*z[15];
    z[37]= - static_cast<T>(19)+ z[36];
    z[37]=z[7]*z[37];
    z[37]=14*z[10] + z[37];
    z[37]=z[7]*z[37];
    z[37]= - 3*z[18] + z[37];
    z[37]=z[37]*z[41];
    z[35]=z[35] + z[37];
    z[35]=z[9]*z[35];
    z[28]=z[36] - static_cast<T>(5)- z[28];
    z[28]=z[7]*z[28];
    z[28]=z[28] + z[35];
    z[35]= - static_cast<T>(3)+ z[15];
    z[35]=z[7]*z[35];
    z[35]=z[22] + z[35];
    z[35]=z[7]*z[35];
    z[18]= - z[18] + z[35];
    z[18]=z[9]*z[18];
    z[35]=static_cast<T>(2)- z[15];
    z[35]=z[7]*z[35];
    z[35]= - z[10] + z[35];
    z[18]=3*z[35] + z[18];
    z[18]=z[19]*z[18];
    z[18]=z[18] + z[24] - static_cast<T>(5)- z[29];
    z[18]=z[4]*z[18]*npow(z[7],2);
    z[18]=2*z[28] + z[18];
    z[18]=z[4]*z[18];
    z[24]=z[51] + z[54];
    z[24]=z[7]*z[24];
    z[28]= - static_cast<T>(34)+ 19*z[15];
    z[28]=z[7]*z[28];
    z[28]=17*z[10] + z[28];
    z[28]=z[7]*z[28];
    z[28]= - z[45] + z[28];
    z[19]=z[28]*z[19];
    z[15]=static_cast<T>(30)- 43*z[15];
    z[15]=z[7]*z[15];
    z[15]=z[19] + z[10] + z[15];
    z[15]=z[9]*z[15];
    z[15]=z[18] + z[15] + z[24] + static_cast<T>(2)+ 3*z[30];
    z[15]=z[4]*z[15];
    z[18]= - static_cast<T>(2)- z[23];
    z[18]=z[5]*z[18];
    z[18]=z[55] + z[18];
    z[19]= - z[40] - 5*z[12];
    z[19]=z[2]*z[19];
    z[19]= - z[58] + z[19];
    z[19]=z[7]*z[19];
    z[14]=z[14] + z[15] + z[16] + z[19] + z[33] + 3*z[18] + z[21];
    z[14]=z[1]*z[14];
    z[15]=z[48]*z[47];
    z[15]= - z[11] + z[15];
    z[15]=z[8]*z[15];
    z[16]=z[30]*z[8];
    z[18]=3*z[16];
    z[19]= - static_cast<T>(1)- 6*z[62];
    z[19]=z[8]*z[19];
    z[19]=z[19] + z[18];
    z[19]=z[6]*z[19];
    z[15]=z[19] + static_cast<T>(6)+ z[15];
    z[15]=z[6]*z[15];
    z[19]=z[11] - z[10];
    z[21]=z[49] - z[19];
    z[21]=z[8]*z[21];
    z[23]=static_cast<T>(1)- z[27];
    z[23]=z[8]*z[23];
    z[23]=z[23] + z[16];
    z[23]=z[6]*z[23];
    z[21]=3*z[21] + z[23];
    z[21]=z[6]*z[21];
    z[23]=3*z[11];
    z[23]=z[19]*z[23];
    z[24]= - z[5]*npow(z[11],3);
    z[23]=z[23] + z[24];
    z[23]=z[8]*z[23];
    z[21]=z[23] + z[21];
    z[21]=z[6]*z[21];
    z[23]=z[48]*z[8];
    z[19]= - z[19]*z[23];
    z[19]=z[19] + z[21];
    z[19]=z[3]*z[19];
    z[15]=z[19] + z[15] + z[23] + z[22] - z[11];
    z[15]=z[15]*z[53];
    z[19]= - z[17] - z[5];
    z[21]= - z[8]*z[27];
    z[18]=z[18] + 4*z[19] + z[21];
    z[18]=z[18]*z[61];
    z[19]=z[8]*z[29];
    z[16]=z[19] + z[17] + z[16];
    z[16]=z[16]*z[26];
    z[17]=z[6] - z[11];
    z[17]=z[8]*z[17];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[6]*z[17];
    z[17]=z[32] + z[17];
    z[17]=z[17]*z[25];
    z[19]= - z[53]*z[8]*z[50]*npow(z[6],3);
    z[19]=z[19] + z[20] + 12*z[6];
    z[19]=z[9]*z[19];
    z[20]= - z[13]*z[34];
    z[20]=static_cast<T>(1)+ z[20];

    r += z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + 3*z[20] + 
      z[31];
 
    return r;
}

template double qg_2lNLC_r903(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r903(const std::array<dd_real,31>&);
#endif
