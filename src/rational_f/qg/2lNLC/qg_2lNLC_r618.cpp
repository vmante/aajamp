#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r618(const std::array<T,31>& k) {
  T z[90];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[12];
    z[10]=k[2];
    z[11]=k[3];
    z[12]=k[9];
    z[13]=k[14];
    z[14]=k[13];
    z[15]=npow(z[7],3);
    z[16]=npow(z[3],4);
    z[17]=z[15]*z[16];
    z[18]=n<T>(1,2)*z[9];
    z[19]=z[7]*z[8];
    z[20]=3*z[19];
    z[21]= - n<T>(5,2) + z[20];
    z[21]=z[21]*z[18];
    z[21]=z[21] + z[8] - 7*z[17];
    z[21]=z[7]*z[21];
    z[22]=z[11]*z[7];
    z[23]=npow(z[7],2);
    z[24]= - 5*z[23] + z[22];
    z[25]=z[16]*z[11];
    z[24]=z[24]*z[25];
    z[24]=9*z[17] + z[24];
    z[24]=z[11]*z[24];
    z[21]=z[24] - n<T>(1,4) + z[21];
    z[21]=z[9]*z[21];
    z[24]=npow(z[7],4);
    z[26]=z[24]*z[16];
    z[27]=z[8]*z[26];
    z[21]=n<T>(7,2)*z[27] + z[21];
    z[21]=z[9]*z[21];
    z[27]=npow(z[2],2);
    z[28]=z[27]*z[11];
    z[29]=npow(z[2],3);
    z[30]=z[28] - z[29];
    z[31]=z[30]*z[25];
    z[32]=n<T>(1,2)*z[5];
    z[33]=z[2]*z[6];
    z[34]=3*z[33];
    z[35]=n<T>(1,2) + z[34];
    z[35]=z[2]*z[35]*z[32];
    z[36]=z[3]*z[2];
    z[37]=npow(z[36],4);
    z[31]=z[35] + z[31] + z[37] + n<T>(1,4) + 2*z[33];
    z[31]=z[5]*z[31];
    z[35]=z[37]*z[6];
    z[38]=n<T>(1,2)*z[6];
    z[31]=z[31] + z[38] + z[35];
    z[31]=z[5]*z[31];
    z[21]=z[21] + z[31];
    z[21]=z[4]*z[21];
    z[31]=n<T>(1,4)*z[11];
    z[39]=z[7]*z[31];
    z[39]=z[23] + z[39];
    z[39]=z[39]*z[25];
    z[39]= - n<T>(9,2)*z[17] + z[39];
    z[39]=z[11]*z[39];
    z[40]= - n<T>(23,8)*z[8] + 5*z[17];
    z[40]=z[7]*z[40];
    z[39]=z[39] + n<T>(5,4) + z[40];
    z[39]=z[9]*z[39];
    z[26]= - n<T>(7,2) - 19*z[26];
    z[26]=z[8]*z[26];
    z[26]= - z[12] + z[26];
    z[26]=n<T>(1,4)*z[26] + z[39];
    z[26]=z[9]*z[26];
    z[39]=npow(z[12],2);
    z[26]=n<T>(1,4)*z[39] + z[26];
    z[26]=z[9]*z[26];
    z[40]=z[29]*z[16];
    z[41]=z[11]*z[2];
    z[42]= - z[27] + z[41];
    z[25]=z[42]*z[25];
    z[25]=z[40] + z[25];
    z[25]=z[11]*z[25];
    z[25]=z[37] - z[25];
    z[37]=z[33] - 1;
    z[37]=n<T>(1,2)*z[37];
    z[25]= - z[37] - 5*z[25];
    z[40]=n<T>(1,4)*z[5];
    z[25]=z[25]*z[40];
    z[25]= - z[35] + z[25];
    z[25]=z[5]*z[25];
    z[35]=npow(z[9],2);
    z[25]= - n<T>(3,8)*z[35] + z[25];
    z[25]=z[5]*z[25];
    z[21]=z[21] + z[26] + z[25];
    z[21]=z[4]*z[21];
    z[25]=npow(z[12],3);
    z[26]=z[25]*z[8];
    z[42]=npow(z[10],2);
    z[43]= - z[42]*z[26];
    z[44]=z[39]*z[42];
    z[45]=npow(z[3],5);
    z[46]=z[45]*npow(z[11],3);
    z[47]=z[44]*z[46];
    z[48]=z[33]*z[9];
    z[49]=z[6] + z[48];
    z[49]=z[49]*z[18];
    z[43]=z[49] + z[43] + z[47];
    z[43]=z[43]*z[32];
    z[47]=z[8]*z[10];
    z[49]=z[25]*z[47];
    z[50]=z[2]*z[8];
    z[51]=z[9]*z[50];
    z[51]=z[51] + z[6] + z[8];
    z[51]=z[51]*z[35];
    z[43]=z[43] + z[49] + n<T>(1,4)*z[51];
    z[43]=z[5]*z[43];
    z[49]=npow(z[9],3);
    z[51]=n<T>(1,2)*z[8];
    z[52]=z[49]*z[51];
    z[26]= - z[26] + z[52];
    z[26]=n<T>(1,2)*z[26] + z[43];
    z[26]=z[5]*z[26];
    z[43]=n<T>(1,2)*z[12];
    z[52]=z[43]*z[23];
    z[52]=z[52] + z[7];
    z[53]=z[52]*z[12];
    z[54]= - z[6]*z[53];
    z[54]= - z[38] + z[54];
    z[54]=z[12]*z[54];
    z[55]=z[39]*z[23];
    z[56]=n<T>(1,2)*z[55];
    z[46]= - z[56]*z[46];
    z[46]=z[54] + z[46];
    z[46]=z[46]*z[49];
    z[54]=z[7]*z[3];
    z[57]=z[8]*z[49]*npow(z[54],6);
    z[58]=npow(z[5],3);
    z[59]=z[6]*z[58]*npow(z[36],6);
    z[57]=z[57] + z[59];
    z[57]=z[1]*z[57]*npow(z[4],2);
    z[26]=z[57] + z[46] + z[26];
    z[46]=n<T>(1,2)*z[11];
    z[57]=z[45]*z[23]*z[46];
    z[59]=2*z[45];
    z[60]= - z[15]*z[59];
    z[57]=z[60] + z[57];
    z[57]=z[11]*z[57];
    z[60]=z[45]*z[24];
    z[57]=3*z[60] + z[57];
    z[57]=z[11]*z[57];
    z[60]= - z[24]*z[59];
    z[60]=n<T>(1,4)*z[8] + z[60];
    z[60]=z[7]*z[60];
    z[57]=z[60] + z[57];
    z[57]=z[9]*z[57];
    z[60]=z[8]*npow(z[7],5);
    z[59]=z[59]*z[60];
    z[57]=z[59] + z[57];
    z[57]=z[57]*z[35];
    z[30]=z[30]*z[11];
    z[59]=npow(z[2],4);
    z[30]= - z[59] - z[30];
    z[30]=z[11]*z[45]*z[30];
    z[61]=npow(z[36],5);
    z[30]=z[30] - n<T>(1,2)*z[33] + z[61];
    z[30]=z[30]*z[32];
    z[61]=z[61]*z[6];
    z[62]=n<T>(1,4)*z[6];
    z[30]=z[30] - z[62] + z[61];
    z[30]=z[30]*npow(z[5],2);
    z[30]=z[57] + z[30];
    z[30]=z[4]*z[30];
    z[45]=z[45]*z[60];
    z[57]=n<T>(3,2)*z[8];
    z[45]= - z[57] - 7*z[45];
    z[45]=z[45]*z[49];
    z[49]= - z[38] - 5*z[61];
    z[49]=z[49]*z[58];
    z[45]=z[45] + z[49];
    z[30]=n<T>(1,4)*z[45] + z[30];
    z[30]=z[4]*z[30];
    z[26]=z[30] + n<T>(1,2)*z[26];
    z[26]=z[1]*z[26];
    z[30]=5*z[19];
    z[45]= - 3*z[50] + z[30];
    z[17]=z[45]*z[17];
    z[17]=n<T>(11,2)*z[8] + z[17];
    z[45]=z[53] + n<T>(1,2);
    z[49]=z[12]*z[45];
    z[17]=n<T>(1,2)*z[17] + z[49];
    z[49]=npow(z[11],2);
    z[58]=z[49]*z[16];
    z[60]=z[12]*z[7];
    z[61]= - z[31]*z[60];
    z[63]=z[23]*z[12];
    z[61]= - z[63] + z[61];
    z[61]=z[61]*z[58];
    z[17]=n<T>(1,2)*z[17] + z[61];
    z[17]=z[9]*z[17];
    z[61]= - z[39]*z[22];
    z[56]=z[56] + z[61];
    z[56]=z[56]*z[58];
    z[58]=z[55] - 1;
    z[61]=z[58]*z[12]*z[6];
    z[56]=z[61] + z[56];
    z[17]=n<T>(1,2)*z[56] + z[17];
    z[17]=z[17]*z[35];
    z[56]=z[6]*z[10];
    z[61]=z[56] + z[34];
    z[61]=z[2]*z[61];
    z[64]=z[42]*z[6];
    z[61]= - z[64] + z[61];
    z[65]=n<T>(1,4)*z[27];
    z[61]=z[61]*z[16]*z[65];
    z[66]=n<T>(1,4)*z[49];
    z[16]=z[66]*z[16];
    z[66]=z[11]*z[12];
    z[67]=z[66]*z[10];
    z[68]=z[42]*z[12];
    z[69]= - z[68] - z[67];
    z[69]=z[69]*z[16];
    z[70]= - n<T>(3,4)*z[10] + z[68];
    z[70]=z[70]*z[39];
    z[48]=n<T>(1,8)*z[48] + z[69] + z[61] + z[70];
    z[48]=z[5]*z[48];
    z[16]=z[44]*z[16];
    z[61]=n<T>(5,2)*z[10];
    z[69]=z[42]*z[8];
    z[69]= - z[61] + z[69];
    z[69]=z[69]*z[43];
    z[69]=z[69] + n<T>(1,2) - z[47];
    z[69]=z[69]*z[39];
    z[16]=z[48] - n<T>(1,2)*z[35] + z[69] + z[16];
    z[16]=z[5]*z[16];
    z[35]=z[18]*z[50];
    z[35]=z[12] + z[35];
    z[35]=z[35]*z[18];
    z[48]=3*z[6];
    z[69]=z[48]*z[8];
    z[35]=z[35] - z[69] - n<T>(1,2)*z[39];
    z[35]=z[35]*z[18];
    z[70]=n<T>(1,4) - z[47];
    z[70]=z[12]*z[70];
    z[70]=z[8] + z[70];
    z[70]=z[70]*z[39];
    z[16]=z[16] + z[70] + z[35];
    z[16]=z[5]*z[16];
    z[35]=z[25]*z[51];
    z[16]=z[26] + z[21] + z[16] + z[35] + z[17];
    z[16]=z[1]*z[16];
    z[17]=npow(z[3],3);
    z[21]=z[46]*z[17];
    z[26]=3*z[2];
    z[35]=z[26] + z[10];
    z[70]=3*z[11];
    z[71]=z[35] - z[70];
    z[71]=z[71]*z[21];
    z[35]=z[35]*z[2];
    z[35]=z[35] - z[42];
    z[35]=z[35]*z[17];
    z[72]=3*z[12];
    z[73]=n<T>(3,2)*z[10] - z[68];
    z[73]=z[73]*z[72];
    z[73]= - n<T>(1,2) + z[73];
    z[73]=z[12]*z[73];
    z[71]=z[71] - n<T>(1,2)*z[35] + z[73];
    z[71]=z[11]*z[71];
    z[35]=z[2]*z[35];
    z[35]= - z[44] + n<T>(1,2) + z[35];
    z[35]=n<T>(1,2)*z[35] + z[71];
    z[35]=z[35]*z[32];
    z[71]=z[42]*z[38];
    z[73]=z[56] - n<T>(3,2)*z[33];
    z[73]=z[2]*z[73];
    z[71]=z[71] + z[73];
    z[73]=n<T>(1,2)*z[2];
    z[71]=z[71]*z[17]*z[73];
    z[74]=z[42]*z[17];
    z[75]=z[12]*z[10];
    z[76]=z[75] - 1;
    z[77]=z[76]*z[72];
    z[77]= - z[74] + z[77];
    z[77]=z[77]*z[43];
    z[67]=z[17]*z[67];
    z[67]=z[77] + z[67];
    z[67]=z[67]*z[46];
    z[77]=3*z[68];
    z[78]=n<T>(13,2)*z[10] - z[77];
    z[78]=z[78]*z[43];
    z[78]= - static_cast<T>(1)+ z[78];
    z[78]=z[12]*z[78];
    z[79]= - n<T>(1,4) - z[66];
    z[79]=z[79]*z[18];
    z[35]=z[35] + z[79] + z[67] + z[71] + z[78];
    z[35]=z[5]*z[35];
    z[67]=z[46]*z[39];
    z[71]=z[74] + z[43];
    z[71]=z[71]*z[67];
    z[74]= - static_cast<T>(3)- n<T>(7,2)*z[47];
    z[74]=n<T>(1,2)*z[74] + z[75];
    z[74]=z[12]*z[74];
    z[74]= - n<T>(3,4)*z[8] + z[74];
    z[74]=z[12]*z[74];
    z[78]=z[39]*z[11];
    z[79]=n<T>(3,2)*z[78];
    z[80]=z[46]*z[12];
    z[81]=z[9]*z[80];
    z[81]=z[81] - z[12] + z[79];
    z[81]=z[81]*z[18];
    z[82]= - z[6]*z[57];
    z[35]=z[35] + z[81] + z[71] + z[82] + z[74];
    z[35]=z[5]*z[35];
    z[71]=z[17]*z[23];
    z[74]=10*z[71];
    z[81]=z[17]*z[7];
    z[21]= - 4*z[81] + z[21];
    z[21]=z[11]*z[21];
    z[21]=z[74] + z[21];
    z[21]=z[11]*z[21];
    z[82]=n<T>(7,4)*z[8];
    z[74]=z[82] - z[74];
    z[74]=z[7]*z[74];
    z[83]=7*z[19];
    z[84]= - n<T>(23,2) + z[83];
    z[84]=z[84]*z[23];
    z[84]=z[84] + n<T>(9,2)*z[22];
    z[84]=z[84]*z[18];
    z[30]= - n<T>(11,2) + z[30];
    z[30]=z[7]*z[30];
    z[30]=z[84] + z[30] + z[11];
    z[30]=z[9]*z[30];
    z[21]=z[30] + z[21] - n<T>(3,4) + z[74];
    z[21]=z[9]*z[21];
    z[30]=7*z[33];
    z[74]= - n<T>(5,2) - z[30];
    z[74]=z[74]*z[27];
    z[84]=z[2]*z[46];
    z[74]=z[74] + z[84];
    z[74]=z[5]*z[74];
    z[84]= - n<T>(7,2) - 11*z[33];
    z[84]=z[2]*z[84];
    z[74]=z[74] + z[84] + z[46];
    z[74]=z[74]*z[32];
    z[84]=z[29]*z[17];
    z[85]= - z[17]*z[28];
    z[86]= - static_cast<T>(1)- n<T>(9,2)*z[33];
    z[74]=z[74] + z[85] + n<T>(1,2)*z[86] + z[84];
    z[74]=z[5]*z[74];
    z[85]=z[27]*z[17]*z[38];
    z[86]= - z[50] + 2*z[19];
    z[86]=z[86]*z[81];
    z[85]=z[85] + z[86];
    z[85]=z[7]*z[85];
    z[86]=z[84]*z[6];
    z[21]=z[74] + z[21] + z[85] - z[62] + z[86];
    z[21]=z[4]*z[21];
    z[74]=z[15]*z[17];
    z[85]= - n<T>(37,4)*z[74] - 1;
    z[85]=z[8]*z[85];
    z[87]=z[43] - z[14];
    z[85]= - z[78] + z[85] + z[87];
    z[72]= - n<T>(59,2)*z[71] - z[72];
    z[88]=z[17]*z[31];
    z[88]=z[81] + z[88];
    z[88]=z[11]*z[88];
    z[72]=n<T>(1,4)*z[72] + z[88];
    z[72]=z[11]*z[72];
    z[88]= - 25*z[8] + n<T>(87,2)*z[71];
    z[88]=z[7]*z[88];
    z[88]=n<T>(13,2) + z[88];
    z[89]=static_cast<T>(8)- n<T>(65,8)*z[19];
    z[89]=z[7]*z[89];
    z[89]=z[89] - n<T>(9,8)*z[11];
    z[89]=z[9]*z[89];
    z[72]=z[89] + n<T>(1,4)*z[88] + z[72];
    z[72]=z[9]*z[72];
    z[72]=n<T>(1,2)*z[85] + z[72];
    z[72]=z[9]*z[72];
    z[85]=n<T>(7,8)*z[27] - z[41];
    z[85]=z[17]*z[85];
    z[85]=n<T>(3,8)*z[9] + z[85];
    z[85]=z[11]*z[85];
    z[88]=static_cast<T>(1)+ n<T>(19,2)*z[33];
    z[88]=z[2]*z[88];
    z[88]=z[88] + z[46];
    z[40]=z[88]*z[40];
    z[40]=z[40] + z[33] - z[84] + z[85];
    z[40]=z[5]*z[40];
    z[84]=n<T>(1,4)*z[9];
    z[85]= - z[9]*z[11];
    z[85]= - static_cast<T>(3)+ z[85];
    z[85]=z[85]*z[84];
    z[85]=z[85] - n<T>(7,4)*z[86] - z[13] - z[62];
    z[40]=n<T>(1,2)*z[85] + z[40];
    z[40]=z[5]*z[40];
    z[85]= - z[51] + z[38] - z[13] + z[14];
    z[85]=z[7]*z[85];
    z[85]=n<T>(1,2) + z[85];
    z[85]=z[12]*z[85];
    z[85]=z[85] - z[51] - 2*z[13] - z[14];
    z[85]=z[12]*z[85];
    z[21]=z[21] + z[40] + z[85] + z[72];
    z[21]=z[4]*z[21];
    z[40]=n<T>(37,8)*z[8] - 3*z[71];
    z[40]=z[7]*z[40];
    z[71]=z[52]*z[43];
    z[45]=z[45]*z[80];
    z[40]=z[45] + z[71] + z[40] - static_cast<T>(2)- n<T>(9,8)*z[50];
    z[40]=z[9]*z[40];
    z[45]=n<T>(1,8) + 2*z[74];
    z[45]=z[8]*z[45];
    z[58]= - z[58]*z[43];
    z[71]= - z[12]*z[31];
    z[71]= - z[60] + z[71];
    z[71]=z[11]*z[71];
    z[72]=n<T>(3,4)*z[63];
    z[71]=z[72] + z[71];
    z[71]=z[11]*z[17]*z[71];
    z[40]=z[40] + z[71] + z[58] + z[45];
    z[40]=z[9]*z[40];
    z[45]=z[81]*z[39];
    z[58]= - z[17]*z[67];
    z[58]=z[45] + z[58];
    z[58]=z[11]*z[58];
    z[71]=z[55]*z[17];
    z[58]= - z[71] + z[58];
    z[58]=z[58]*z[46];
    z[74]= - static_cast<T>(5)- n<T>(7,2)*z[60];
    z[74]=z[43]*z[6]*z[74];
    z[40]=z[40] + z[58] - z[69] + z[74];
    z[40]=z[9]*z[40];
    z[58]= - z[13]*z[42]*z[81];
    z[69]=n<T>(9,4)*z[8];
    z[58]=z[58] + z[69] + z[87];
    z[58]=z[58]*z[39];
    z[74]=2*z[14];
    z[45]= - z[74]*z[45];
    z[17]=z[78]*z[17]*z[14];
    z[17]=z[45] + z[17];
    z[17]=z[11]*z[17];
    z[45]=z[14]*z[71];
    z[17]=z[45] + z[17];
    z[17]=z[11]*z[17];
    z[16]=z[16] + z[21] + z[35] + z[40] + z[58] + z[17];
    z[16]=z[1]*z[16];
    z[17]=4*z[19];
    z[21]= - n<T>(39,4) + z[17];
    z[21]=z[21]*z[15];
    z[35]=15*z[23] - n<T>(7,2)*z[22];
    z[35]=z[35]*z[46];
    z[21]=z[21] + z[35];
    z[21]=z[9]*z[21];
    z[20]= - n<T>(23,4) + z[20];
    z[20]=z[20]*z[23];
    z[35]=n<T>(5,2)*z[11];
    z[40]=19*z[7] - z[35];
    z[40]=z[40]*z[46];
    z[20]=z[21] + 3*z[20] + z[40];
    z[20]=z[9]*z[20];
    z[21]= - static_cast<T>(9)+ z[83];
    z[21]=z[7]*z[21];
    z[20]=z[20] + z[21] + z[35];
    z[20]=z[9]*z[20];
    z[21]=4*z[33];
    z[40]=n<T>(9,4) + z[21];
    z[40]=z[40]*z[29];
    z[45]=z[2]*z[31];
    z[45]= - z[27] + z[45];
    z[45]=z[11]*z[45];
    z[40]=z[40] + z[45];
    z[40]=z[5]*z[40];
    z[30]=n<T>(15,4) + z[30];
    z[30]=z[30]*z[27];
    z[26]= - z[26] + z[11];
    z[26]=z[26]*z[46];
    z[26]=z[40] + z[30] + z[26];
    z[26]=z[5]*z[26];
    z[21]=n<T>(7,4) + z[21];
    z[21]=z[2]*z[21];
    z[21]=z[26] + z[21] - z[46];
    z[21]=z[5]*z[21];
    z[26]=3*z[8];
    z[30]=z[26] + z[6];
    z[40]=npow(z[3],2);
    z[45]=z[40]*z[7];
    z[58]=5*z[45];
    z[71]=z[40]*z[2];
    z[81]= - z[58] + n<T>(1,2)*z[30] + 2*z[71];
    z[81]=z[7]*z[81];
    z[85]=4*z[45];
    z[86]= - z[11]*z[40];
    z[86]=z[86] - z[71] + z[85];
    z[86]=z[11]*z[86];
    z[87]=npow(z[36],2);
    z[88]= - static_cast<T>(1)+ z[34];
    z[20]=z[21] + z[20] + z[86] + z[81] + n<T>(1,2)*z[88] + z[87];
    z[20]=z[4]*z[20];
    z[21]= - z[49]*z[18];
    z[81]= - static_cast<T>(17)- 41*z[33];
    z[65]=z[81]*z[65];
    z[81]=n<T>(3,4)*z[2] + z[11];
    z[81]=z[11]*z[81];
    z[65]=z[65] + z[81];
    z[65]=z[65]*z[32];
    z[81]= - static_cast<T>(1)- z[34];
    z[81]=z[2]*z[81];
    z[21]=z[65] + z[21] + z[81] + z[31];
    z[21]=z[5]*z[21];
    z[65]= - z[33] - 11*z[87];
    z[81]=z[40]*z[41];
    z[65]=n<T>(1,2)*z[65] + z[81];
    z[81]=z[49]*z[9];
    z[86]= - z[11] + z[81];
    z[86]=z[9]*z[86];
    z[65]=n<T>(1,2)*z[65] + z[86];
    z[21]=n<T>(1,2)*z[65] + z[21];
    z[21]=z[5]*z[21];
    z[65]=z[6] - z[69];
    z[65]=z[65]*z[71];
    z[69]=z[40]*z[19];
    z[65]=z[65] - n<T>(5,4)*z[69];
    z[69]=n<T>(1,2)*z[7];
    z[65]=z[65]*z[69];
    z[71]=static_cast<T>(23)- n<T>(29,2)*z[19];
    z[71]=z[71]*z[23];
    z[71]=z[71] - n<T>(17,2)*z[22];
    z[71]=z[9]*z[71];
    z[86]=static_cast<T>(53)- n<T>(117,2)*z[19];
    z[86]=z[7]*z[86];
    z[88]= - n<T>(1,8) + z[66];
    z[88]=z[11]*z[88];
    z[71]=n<T>(3,4)*z[71] + n<T>(1,4)*z[86] + z[88];
    z[71]=z[9]*z[71];
    z[58]= - z[26] + z[58];
    z[86]=3*z[7];
    z[58]=z[58]*z[86];
    z[58]= - n<T>(5,2) + z[58];
    z[88]= - 3*z[45] - z[12];
    z[88]=z[11]*z[88];
    z[58]=z[71] + n<T>(1,2)*z[58] + z[88];
    z[58]=z[9]*z[58];
    z[71]= - z[6] + z[57];
    z[71]=z[71]*z[87];
    z[87]=2*z[12];
    z[20]=z[20] + z[21] + z[58] + z[87] + z[65] + n<T>(1,4)*z[71] + z[6] + 
    z[82];
    z[20]=z[4]*z[20];
    z[21]=9*z[33];
    z[58]=z[21] + 1;
    z[65]= - z[47] - z[58];
    z[71]=z[10] - n<T>(3,2)*z[2];
    z[71]=z[2]*z[71];
    z[71]=n<T>(1,2)*z[42] + z[71];
    z[71]=z[71]*z[40];
    z[44]=n<T>(1,2)*z[44] + n<T>(1,4)*z[65] + z[71];
    z[65]= - n<T>(5,8)*z[10] + z[2];
    z[65]=z[65]*z[40];
    z[71]=7*z[10];
    z[82]= - z[71] + z[77];
    z[82]=z[12]*z[82];
    z[82]=n<T>(1,2) + z[82];
    z[82]=z[82]*z[43];
    z[88]=z[10]*z[25];
    z[88]= - 3*z[40] + z[88];
    z[88]=z[88]*z[31];
    z[65]=z[88] + z[65] + z[82];
    z[65]=z[11]*z[65];
    z[58]=z[58]*z[73];
    z[58]=z[58] - z[68];
    z[82]= - n<T>(9,4)*z[10] + z[68];
    z[82]=z[12]*z[82];
    z[82]=n<T>(1,2) + z[82];
    z[82]=z[82]*z[66];
    z[77]= - z[10] + z[77];
    z[77]=z[12]*z[77];
    z[77]=n<T>(1,2) + z[77];
    z[77]=n<T>(1,4)*z[77] + z[82];
    z[77]=z[11]*z[77];
    z[58]=n<T>(1,4)*z[58] + z[77];
    z[58]=z[5]*z[58];
    z[77]=z[43]*z[81];
    z[44]=z[58] + z[77] + n<T>(1,2)*z[44] + z[65];
    z[44]=z[5]*z[44];
    z[58]=n<T>(1,2) - z[56];
    z[58]=z[58]*z[57];
    z[65]= - 11*z[56] + n<T>(17,2)*z[33];
    z[65]=z[65]*z[73];
    z[64]=z[64] + z[65];
    z[64]=z[64]*z[40];
    z[58]=z[64] - z[6] + z[58];
    z[64]=z[40]*z[42];
    z[26]=z[26]*z[42];
    z[65]=z[71] - z[26];
    z[77]=n<T>(1,4)*z[12];
    z[65]=z[65]*z[77];
    z[65]=z[65] + n<T>(1,2)*z[64] - static_cast<T>(2)- n<T>(3,4)*z[47];
    z[65]=z[12]*z[65];
    z[82]=z[10]*z[40];
    z[88]= - n<T>(1,4) + z[75];
    z[88]=z[12]*z[88];
    z[82]=n<T>(1,4)*z[82] + z[88];
    z[82]=z[12]*z[82];
    z[88]= - z[25]*z[31];
    z[82]=z[82] + z[88];
    z[82]=z[11]*z[82];
    z[49]= - z[39]*z[49];
    z[81]= - z[12]*z[81];
    z[49]=z[49] + z[81];
    z[49]=z[49]*z[18];
    z[44]=z[44] + z[49] + z[82] + n<T>(1,2)*z[58] + z[65];
    z[44]=z[5]*z[44];
    z[49]=z[62] + z[51];
    z[58]= - 3*z[13] - z[49];
    z[58]=z[7]*z[58];
    z[62]=static_cast<T>(7)+ 5*z[47];
    z[58]=z[58] + n<T>(1,4)*z[62] + z[64];
    z[58]=z[12]*z[58];
    z[62]=z[64] - 3;
    z[62]=z[13]*z[62];
    z[65]=z[40]*z[23];
    z[81]=z[74]*z[65];
    z[58]=z[58] + z[81] - z[57] - n<T>(5,2)*z[6] + z[62];
    z[58]=z[12]*z[58];
    z[62]=n<T>(1,2)*z[10];
    z[81]=z[40]*z[62];
    z[82]=3*z[14];
    z[81]=z[43] + n<T>(1,2)*z[45] + z[82] + z[81];
    z[81]=z[12]*z[81];
    z[85]= - z[14]*z[85];
    z[81]=z[85] + z[81];
    z[81]=z[12]*z[81];
    z[85]=z[74] - n<T>(3,4)*z[12];
    z[85]=z[66]*z[40]*z[85];
    z[81]=z[81] + z[85];
    z[81]=z[11]*z[81];
    z[85]= - n<T>(17,2)*z[45] - z[38] + 11*z[8];
    z[85]=z[7]*z[85];
    z[37]= - z[37] + z[85];
    z[85]=n<T>(1,2) - z[55];
    z[85]=z[12]*z[85];
    z[79]=z[85] + z[79];
    z[79]=z[11]*z[79];
    z[37]=z[79] + n<T>(1,2)*z[37] - z[55];
    z[52]=z[52]*z[67];
    z[52]=z[53] + z[52];
    z[52]=z[11]*z[52];
    z[53]=n<T>(55,8)*z[19] - static_cast<T>(7)- n<T>(23,8)*z[50];
    z[53]=z[7]*z[53];
    z[52]=z[52] + z[53] + n<T>(1,4)*z[63];
    z[52]=z[9]*z[52];
    z[37]=n<T>(1,2)*z[37] + z[52];
    z[37]=z[9]*z[37];
    z[52]=n<T>(5,2)*z[50];
    z[53]=n<T>(9,2)*z[19] - z[52];
    z[40]=z[40]*z[53];
    z[53]=z[8]*z[6];
    z[40]= - 9*z[53] + z[40];
    z[40]=z[7]*z[40];
    z[53]=z[48]*z[7];
    z[55]=static_cast<T>(7)- z[53];
    z[55]=z[55]*z[60];
    z[79]=static_cast<T>(1)- z[53];
    z[55]=3*z[79] + z[55];
    z[55]=z[12]*z[55];
    z[30]=z[55] + z[40] + z[30];
    z[40]=z[45] - 5*z[12];
    z[40]=z[40]*z[43];
    z[45]= - z[11]*z[25];
    z[40]=z[40] + z[45];
    z[40]=z[11]*z[40];
    z[30]=n<T>(1,2)*z[30] + z[40];
    z[30]=n<T>(1,2)*z[30] + z[37];
    z[30]=z[9]*z[30];
    z[37]=z[6]*z[13];
    z[40]= - z[14] - z[48];
    z[40]=z[8]*z[40];
    z[40]= - z[37] + z[40];
    z[37]=z[64]*z[37];
    z[45]=z[8]*z[14]*z[65];
    z[16]=z[16] + z[20] + z[44] + z[30] + z[81] + z[58] + z[45] + n<T>(1,2)*
    z[40] + z[37];
    z[16]=z[1]*z[16];
    z[20]=n<T>(19,4)*z[19] - static_cast<T>(8)- z[52];
    z[20]=z[20]*z[23];
    z[30]= - z[7] + z[63];
    z[30]=z[12]*z[30];
    z[30]= - static_cast<T>(1)+ z[30];
    z[30]=z[30]*z[31];
    z[30]=z[30] - z[7] + z[72];
    z[30]=z[30]*z[66];
    z[30]=z[72] + z[30];
    z[30]=z[11]*z[30];
    z[20]=z[20] + z[30];
    z[20]=z[9]*z[20];
    z[30]= - n<T>(1,2) + z[19];
    z[30]=z[7]*z[30];
    z[30]=9*z[30] - z[63];
    z[37]= - static_cast<T>(1)+ 3*z[60];
    z[37]=z[37]*z[67];
    z[40]=5*z[7] - z[63];
    z[40]=z[12]*z[40];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[12]*z[40];
    z[37]=z[40] + z[37];
    z[37]=z[37]*z[46];
    z[40]=n<T>(9,4)*z[7] - z[63];
    z[40]=z[12]*z[40];
    z[37]=z[40] + z[37];
    z[37]=z[11]*z[37];
    z[20]=z[20] + n<T>(1,2)*z[30] + z[37];
    z[20]=z[9]*z[20];
    z[30]= - z[19]*z[48];
    z[30]=z[30] + z[6] + n<T>(13,2)*z[8];
    z[30]=z[7]*z[30];
    z[37]= - static_cast<T>(1)- z[53];
    z[37]=z[7]*z[37];
    z[37]=z[37] + 3*z[63];
    z[37]=z[12]*z[37];
    z[40]= - z[6] - z[57];
    z[40]=z[2]*z[40];
    z[30]=z[37] + z[30] - static_cast<T>(3)+ z[40];
    z[37]=n<T>(3,2) - 2*z[60];
    z[37]=z[37]*z[39];
    z[25]=z[25]*z[46];
    z[25]=z[37] + z[25];
    z[25]=z[11]*z[25];
    z[37]= - n<T>(3,4) - 4*z[60];
    z[37]=z[12]*z[37];
    z[25]=z[37] + z[25];
    z[25]=z[11]*z[25];
    z[20]=z[20] + n<T>(1,4)*z[30] + z[25];
    z[20]=z[9]*z[20];
    z[25]=z[61] - z[68];
    z[25]=z[12]*z[25];
    z[25]=n<T>(3,2) + z[25];
    z[25]=z[12]*z[25];
    z[30]= - z[76]*z[67];
    z[25]=z[25] + z[30];
    z[25]=z[11]*z[25];
    z[30]= - z[10] - z[68];
    z[30]=z[12]*z[30];
    z[25]=z[25] - n<T>(1,2) + z[30];
    z[25]=z[11]*z[25];
    z[30]=n<T>(7,2)*z[33] + n<T>(3,2) - z[56];
    z[30]=z[2]*z[30];
    z[37]=z[42]*z[43];
    z[25]=z[25] + z[37] + z[62] + z[30];
    z[30]=3*z[10] - z[68];
    z[30]=z[12]*z[30];
    z[30]= - static_cast<T>(1)+ z[30];
    z[30]=z[30]*z[80];
    z[37]=z[10] - n<T>(3,2)*z[68];
    z[37]=z[12]*z[37];
    z[30]=z[30] + static_cast<T>(1)+ z[37];
    z[30]=z[11]*z[30];
    z[37]= - z[10] + z[2];
    z[30]=z[30] + n<T>(1,2)*z[37] + z[68];
    z[30]=z[30]*z[46];
    z[37]= - n<T>(7,4)*z[33] - static_cast<T>(1)- n<T>(1,4)*z[56];
    z[37]=z[37]*z[27];
    z[30]=z[30] - n<T>(1,4)*z[42] + z[37];
    z[30]=z[5]*z[30];
    z[25]=n<T>(1,2)*z[25] + z[30];
    z[25]=z[5]*z[25];
    z[30]= - z[42]*z[48];
    z[30]=z[10] + z[30];
    z[30]=z[8]*z[30];
    z[30]=z[30] - n<T>(23,2) + z[56];
    z[26]= - z[26] + z[68];
    z[26]=z[26]*z[77];
    z[37]=n<T>(3,4) - z[75];
    z[37]=z[37]*z[78];
    z[37]=n<T>(7,4)*z[12] + z[37];
    z[37]=z[11]*z[37];
    z[39]= - 2*z[6] - z[51];
    z[39]=z[2]*z[39];
    z[25]=z[25] + z[37] + z[26] + n<T>(1,4)*z[30] + z[39];
    z[25]=z[5]*z[25];
    z[26]= - static_cast<T>(29)+ 9*z[19];
    z[24]=z[26]*z[24];
    z[22]= - n<T>(15,2)*z[23] + z[22];
    z[22]=z[11]*z[22];
    z[22]=n<T>(33,2)*z[15] + z[22];
    z[22]=z[11]*z[22];
    z[22]=n<T>(1,2)*z[24] + z[22];
    z[18]=z[22]*z[18];
    z[22]= - static_cast<T>(19)+ z[83];
    z[22]=z[22]*z[15];
    z[24]= - 6*z[7] + z[46];
    z[24]=z[11]*z[24];
    z[24]=n<T>(35,2)*z[23] + z[24];
    z[24]=z[11]*z[24];
    z[18]=z[18] + z[22] + z[24];
    z[18]=z[9]*z[18];
    z[22]= - static_cast<T>(73)+ 35*z[19];
    z[22]=z[22]*z[23];
    z[24]=47*z[7] - 9*z[11];
    z[24]=z[11]*z[24];
    z[22]=z[22] + z[24];
    z[18]=n<T>(1,4)*z[22] + z[18];
    z[18]=z[9]*z[18];
    z[21]= - static_cast<T>(7)- z[21];
    z[21]=z[21]*z[59];
    z[22]= - z[41] + n<T>(3,2)*z[27];
    z[24]= - z[11]*z[22];
    z[24]=n<T>(5,2)*z[29] + z[24];
    z[24]=z[11]*z[24];
    z[21]=n<T>(1,2)*z[21] + z[24];
    z[21]=z[5]*z[21];
    z[24]=n<T>(1,2)*z[29];
    z[26]= - static_cast<T>(13)- 17*z[33];
    z[26]=z[26]*z[24];
    z[22]=z[22]*z[70];
    z[21]=z[21] + z[26] + z[22];
    z[21]=z[5]*z[21];
    z[22]= - static_cast<T>(9)- 13*z[33];
    z[22]=z[22]*z[27];
    z[21]=z[21] + n<T>(1,2)*z[22] + 3*z[41];
    z[21]=z[21]*z[32];
    z[22]= - z[6] - z[51];
    z[22]=z[2]*z[22];
    z[17]=z[17] - n<T>(13,2) + z[22];
    z[17]=z[7]*z[17];
    z[22]= - static_cast<T>(1)- n<T>(9,4)*z[33];
    z[22]=z[2]*z[22];
    z[17]=z[21] + z[18] + z[35] + z[22] + z[17];
    z[17]=z[4]*z[17];
    z[18]=n<T>(31,2) - z[83];
    z[18]=z[18]*z[15];
    z[21]=z[7] + z[31];
    z[21]=z[11]*z[21];
    z[21]= - n<T>(39,4)*z[23] + z[21];
    z[21]=z[11]*z[21];
    z[18]=z[18] + z[21];
    z[18]=z[9]*z[18];
    z[21]=n<T>(179,8) - 14*z[19];
    z[21]=z[21]*z[23];
    z[22]= - 8*z[7] - z[46];
    z[22]=z[11]*z[22];
    z[18]=z[18] + z[21] + z[22];
    z[18]=z[9]*z[18];
    z[21]=static_cast<T>(45)- 67*z[19];
    z[21]=z[21]*z[69];
    z[21]=z[21] + z[70];
    z[18]=n<T>(1,4)*z[21] + z[18];
    z[18]=z[9]*z[18];
    z[21]=static_cast<T>(3)+ n<T>(17,4)*z[33];
    z[21]=z[21]*z[29];
    z[22]=z[2] - n<T>(3,2)*z[11];
    z[22]=z[11]*z[22];
    z[22]= - n<T>(7,2)*z[27] + z[22];
    z[22]=z[22]*z[46];
    z[21]=z[21] + z[22];
    z[21]=z[5]*z[21];
    z[22]=static_cast<T>(2)+ z[34];
    z[22]=z[22]*z[27];
    z[21]=z[21] + z[22] - z[41];
    z[21]=z[5]*z[21];
    z[22]=z[33] + 1;
    z[26]=z[2]*z[22];
    z[26]=n<T>(11,4)*z[26] - z[11];
    z[21]=n<T>(1,2)*z[26] + z[21];
    z[21]=z[5]*z[21];
    z[26]= - static_cast<T>(15)- n<T>(13,2)*z[50];
    z[29]= - z[6] + n<T>(5,4)*z[8];
    z[29]=z[7]*z[29];
    z[26]=n<T>(1,2)*z[26] + z[29];
    z[17]=z[17] + z[21] + n<T>(1,2)*z[26] + z[18];
    z[17]=z[4]*z[17];
    z[18]=z[82] - z[49];
    z[18]=z[7]*z[18];
    z[21]= - z[10]*z[13];
    z[21]=n<T>(1,4) + z[21];
    z[21]=z[21]*z[86];
    z[21]=n<T>(7,4)*z[10] + z[21];
    z[21]=z[12]*z[21];
    z[18]=z[21] + z[18] + n<T>(1,4)*z[47] + static_cast<T>(2)- z[56];
    z[18]=z[12]*z[18];
    z[21]= - n<T>(3,2)*z[19] + n<T>(15,2);
    z[21]=z[6]*z[21];
    z[26]=static_cast<T>(7)+ n<T>(3,2)*z[56];
    z[26]=z[8]*z[26];
    z[21]=z[26] + z[21];
    z[26]= - z[14] - z[43];
    z[26]=z[26]*z[78];
    z[29]= - static_cast<T>(1)+ z[60];
    z[29]=z[12]*z[14]*z[29];
    z[26]=z[29] + z[26];
    z[26]=z[26]*z[70];
    z[16]=z[16] + z[17] + z[25] + z[20] + z[26] + n<T>(1,2)*z[21] + z[18];
    z[16]=z[1]*z[16];
    z[17]= - z[6] + z[51];
    z[17]=z[2]*z[17];
    z[17]= - n<T>(3,2) + z[17];
    z[17]=z[17]*z[36];
    z[18]=z[50] + 3;
    z[18]=z[18]*z[3];
    z[19]=z[19]*z[3];
    z[18]=z[18] - z[19];
    z[20]=z[18]*z[69];
    z[17]=z[20] - n<T>(3,4) + z[17];
    z[17]=z[7]*z[17];
    z[19]=3*z[3] - z[19];
    z[15]=z[19]*z[15];
    z[19]=z[11]*z[3];
    z[20]= - z[23]*z[19];
    z[15]=n<T>(1,2)*z[15] + z[20];
    z[15]=z[9]*z[15];
    z[20]= - static_cast<T>(5)- z[34];
    z[20]=z[3]*z[20]*z[27];
    z[20]=5*z[2] + z[20];
    z[21]= - z[54] + n<T>(7,4) + z[36];
    z[21]=z[11]*z[21];
    z[23]= - z[22]*z[24];
    z[23]=z[23] + z[28];
    z[23]=z[5]*z[3]*z[23];
    z[15]=z[23] + z[15] + z[21] + n<T>(1,2)*z[20] + z[17];
    z[15]=z[4]*z[15];
    z[15]=z[15] - static_cast<T>(5)- z[56];
    z[17]=z[6] - z[8];
    z[17]=z[17]*z[36];
    z[18]= - z[7]*z[18];
    z[18]= - n<T>(11,2) + z[18];
    z[18]=z[18]*z[84];
    z[17]=z[18] + n<T>(1,2)*z[17] - z[74] + z[57];
    z[17]=z[7]*z[17];
    z[18]= - n<T>(1,2)*z[56] + z[22];
    z[18]=z[2]*z[18];
    z[18]= - z[62] + z[18];
    z[18]=z[18]*z[36];
    z[18]=z[18] - z[71] + n<T>(27,4)*z[2];
    z[20]=z[62] - z[2];
    z[19]=z[20]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[5]*z[18];
    z[19]=n<T>(5,2)*z[33] + n<T>(9,2) - z[56];
    z[19]=z[19]*z[73];
    z[19]= - z[10] + z[19];
    z[19]=z[3]*z[19];
    z[20]= - z[38] - 2*z[8];
    z[20]=z[2]*z[20];
    z[21]= - z[10] + 2*z[7];
    z[21]=z[12]*z[21];
    z[22]= - z[87] + z[14] - n<T>(1,2)*z[3];
    z[22]=z[11]*z[22];

    r += n<T>(1,2)*z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + 
      z[22] + z[47];
 
    return r;
}

template double qg_2lNLC_r618(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r618(const std::array<dd_real,31>&);
#endif
