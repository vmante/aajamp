#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r970(const std::array<T,31>& k) {
  T z[129];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[6];
    z[9]=k[7];
    z[10]=k[2];
    z[11]=k[14];
    z[12]=k[3];
    z[13]=k[13];
    z[14]=k[9];
    z[15]=npow(z[1],2);
    z[16]=5*z[15];
    z[17]=npow(z[1],3);
    z[18]=z[17]*z[5];
    z[19]=z[6]*z[1];
    z[20]= - 197*z[19] - z[16] + 101*z[18];
    z[21]=z[7]*z[1];
    z[20]=n<T>(1,3)*z[20] + 32*z[21];
    z[20]=z[7]*z[20];
    z[22]=z[15]*z[6];
    z[23]=npow(z[1],4);
    z[24]=z[23]*z[5];
    z[20]=z[20] - 133*z[22] - n<T>(271,3)*z[17] + 101*z[24];
    z[20]=z[7]*z[20];
    z[25]=npow(z[1],5);
    z[26]=z[25]*z[5];
    z[27]=z[26] - z[23];
    z[28]=z[17]*z[6];
    z[20]=z[20] + 101*z[27] - n<T>(335,3)*z[28];
    z[20]=z[4]*z[20];
    z[29]=z[21] + z[15];
    z[30]=z[29]*z[7];
    z[31]=n<T>(1,3)*z[17];
    z[32]=z[30] + z[31];
    z[33]=npow(z[7],2);
    z[34]=z[33]*z[4];
    z[32]=z[32]*z[34];
    z[29]=z[29] - z[19];
    z[35]=z[29]*z[33];
    z[32]=z[32] + z[35];
    z[32]=z[32]*z[9];
    z[35]=n<T>(101,3)*z[18];
    z[36]= - 55*z[19] - 23*z[15] + z[35];
    z[36]=z[7]*z[36];
    z[37]=npow(z[1],6);
    z[38]=z[37]*z[5];
    z[39]=z[23]*z[6];
    z[40]= - z[25] + z[38] - z[39];
    z[40]=z[40]*z[4];
    z[40]=z[40] + z[27];
    z[41]=n<T>(101,3)*z[3];
    z[42]=z[40]*z[41];
    z[43]= - 335*z[17] + 202*z[24];
    z[20]= - 32*z[32] + z[42] + z[20] + n<T>(1,3)*z[43] + z[36];
    z[20]=z[2]*z[20];
    z[36]=z[15]*z[5];
    z[42]=161*z[36];
    z[43]= - n<T>(562,3)*z[1] + z[42];
    z[43]=z[7]*z[43];
    z[43]=z[43] - n<T>(1409,3)*z[15] + 316*z[18];
    z[43]=z[7]*z[43];
    z[43]=z[43] - n<T>(1132,3)*z[17] + 149*z[24];
    z[44]=n<T>(1,3)*z[4];
    z[43]=z[43]*z[44];
    z[45]=2*z[15];
    z[46]=z[45] + z[21];
    z[47]=z[19] - z[46];
    z[47]=z[7]*z[47];
    z[46]= - z[7]*z[46];
    z[46]= - z[17] + z[46];
    z[48]=z[4]*z[7];
    z[46]=z[46]*z[48];
    z[46]=z[47] + z[46];
    z[46]=z[9]*z[46];
    z[47]= - n<T>(389,3)*z[15] + 128*z[18];
    z[47]=n<T>(1,3)*z[47] + 23*z[19];
    z[49]=n<T>(1,3)*z[7];
    z[42]= - 262*z[1] + z[42];
    z[42]=z[42]*z[49];
    z[50]=2*z[5];
    z[51]= - z[25]*z[50];
    z[51]= - n<T>(95,3)*z[23] + z[51];
    z[51]=z[4]*z[51];
    z[51]=n<T>(95,3)*z[24] + z[51];
    z[51]=z[3]*z[51];
    z[20]=z[20] + n<T>(224,9)*z[46] + z[51] + z[43] + 2*z[47] + z[42];
    z[42]=4*z[2];
    z[20]=z[20]*z[42];
    z[43]=5*z[5];
    z[46]=z[43]*z[25];
    z[47]=14*z[23] - z[46];
    z[47]=z[5]*z[47];
    z[47]= - n<T>(62,3)*z[17] + z[47];
    z[47]=z[5]*z[47];
    z[47]= - n<T>(13,3)*z[19] + n<T>(71,3)*z[15] + z[47];
    z[51]=12*z[17];
    z[52]=5*z[24];
    z[53]=z[51] - z[52];
    z[53]=z[5]*z[53];
    z[54]=9*z[15];
    z[53]= - z[54] + z[53];
    z[53]=z[5]*z[53];
    z[45]=z[45] - z[18];
    z[45]=z[5]*z[45];
    z[45]= - z[1] + z[45];
    z[55]=z[7]*z[5];
    z[45]=z[45]*z[55];
    z[45]=z[45] + 26*z[1] + z[53];
    z[45]=z[7]*z[45];
    z[45]=2*z[47] + z[45];
    z[45]=z[7]*z[45];
    z[47]=z[37]*z[43];
    z[53]=16*z[25] - z[47];
    z[53]=z[5]*z[53];
    z[53]= - 40*z[23] + z[53];
    z[53]=z[5]*z[53];
    z[53]=11*z[22] + 33*z[17] + z[53];
    z[45]=2*z[53] + z[45];
    z[45]=z[7]*z[45];
    z[53]=npow(z[1],7);
    z[56]= - z[53]*z[43];
    z[56]=18*z[37] + z[56];
    z[56]=z[5]*z[56];
    z[56]= - 67*z[25] + z[56];
    z[56]=z[5]*z[56];
    z[57]=54*z[23];
    z[45]=z[45] + 38*z[28] + z[57] + z[56];
    z[45]=z[4]*z[45];
    z[56]= - z[52] + 9*z[17];
    z[58]=z[5]*z[56];
    z[58]= - n<T>(35,3)*z[15] + z[58];
    z[58]=z[5]*z[58];
    z[58]=n<T>(77,3)*z[1] + z[58];
    z[59]=7*z[15];
    z[60]=z[59] - 5*z[18];
    z[60]=z[5]*z[60];
    z[61]=2*z[1];
    z[60]= - z[61] + z[60];
    z[60]=z[60]*z[55];
    z[58]=4*z[58] + z[60];
    z[58]=z[7]*z[58];
    z[60]= - z[46] + 11*z[23];
    z[62]=z[5]*z[60];
    z[62]= - 29*z[17] + z[62];
    z[62]=z[5]*z[62];
    z[62]=31*z[15] + z[62];
    z[62]=3*z[62] + n<T>(38,3)*z[19];
    z[58]=2*z[62] + z[58];
    z[58]=z[7]*z[58];
    z[47]= - z[47] + 13*z[25];
    z[47]=z[47]*z[5];
    z[47]=z[47] - z[57];
    z[47]=z[47]*z[5];
    z[57]=48*z[17] + z[47];
    z[45]=z[45] + z[58] + 4*z[57] + n<T>(149,3)*z[22];
    z[57]=8*z[4];
    z[45]=z[45]*z[57];
    z[51]=z[51] - 11*z[24];
    z[58]=24*z[5];
    z[51]=z[51]*z[58];
    z[51]=n<T>(5227,18)*z[15] + z[51];
    z[51]=z[5]*z[51];
    z[62]=11*z[18];
    z[63]=8*z[15] - z[62];
    z[64]=8*z[5];
    z[63]=z[63]*z[64];
    z[63]=n<T>(8161,18)*z[1] + z[63];
    z[55]=z[63]*z[55];
    z[51]=z[55] + n<T>(8189,9)*z[1] + z[51];
    z[51]=z[7]*z[51];
    z[55]=11*z[26];
    z[63]=16*z[23] - z[55];
    z[58]=z[63]*z[58];
    z[58]= - n<T>(7685,9)*z[17] + z[58];
    z[58]=z[5]*z[58];
    z[45]=z[45] + z[51] - n<T>(539,2)*z[19] + n<T>(10375,18)*z[15] + z[58];
    z[45]=z[4]*z[45];
    z[51]= - z[5]*npow(z[1],8);
    z[51]=4*z[53] + z[51];
    z[51]=z[5]*z[51];
    z[51]= - n<T>(61,3)*z[37] + z[51];
    z[51]=z[5]*z[51];
    z[58]=n<T>(52,3)*z[25];
    z[51]=n<T>(46,3)*z[39] + z[58] + z[51];
    z[51]=z[4]*z[51];
    z[53]=z[53]*z[5];
    z[37]= - z[53] + 3*z[37];
    z[37]=z[37]*z[5];
    z[37]=z[37] - z[58];
    z[37]=z[37]*z[5];
    z[53]=n<T>(46,3)*z[23];
    z[58]=z[53] + z[37];
    z[51]=z[51] + 5*z[58] + n<T>(74,3)*z[28];
    z[51]=z[51]*z[57];
    z[58]=20*z[25] - 11*z[38];
    z[63]=4*z[5];
    z[58]=z[58]*z[63];
    z[58]= - n<T>(1949,9)*z[23] + z[58];
    z[58]=z[58]*z[50];
    z[51]=z[51] - n<T>(4483,18)*z[22] + n<T>(173,18)*z[17] + z[58];
    z[51]=z[4]*z[51];
    z[58]=n<T>(1,3)*z[3];
    z[40]=z[40]*z[58];
    z[65]=n<T>(5,3)*z[27] - z[28];
    z[65]=z[4]*z[65];
    z[40]=z[40] + z[65] - z[17] + n<T>(4,3)*z[24];
    z[65]=n<T>(580,3)*z[3];
    z[40]=z[40]*z[65];
    z[66]=5*z[23];
    z[67]=z[66] - 7*z[26];
    z[67]=z[67]*z[63];
    z[67]= - n<T>(151,9)*z[17] + z[67];
    z[67]=z[67]*z[50];
    z[40]=z[40] + z[51] - n<T>(1792,9)*z[19] + n<T>(847,6)*z[15] + z[67];
    z[40]=z[3]*z[40];
    z[51]= - n<T>(8219,3)*z[21] - n<T>(4448,3)*z[15] + 1583*z[19];
    z[67]=3*z[15];
    z[68]= - z[67] - 4*z[21];
    z[68]=z[7]*z[68];
    z[69]=2*z[17];
    z[68]= - z[69] + 3*z[68];
    z[68]=z[7]*z[68];
    z[30]= - z[17] - 3*z[30];
    z[30]=z[30]*z[34];
    z[30]=z[68] + z[30];
    z[30]=z[4]*z[30];
    z[68]= - 3371*z[15] - 6001*z[21];
    z[68]=z[7]*z[68];
    z[68]= - 835*z[17] + z[68];
    z[30]=n<T>(1,9)*z[68] + 16*z[30];
    z[30]=z[4]*z[30];
    z[68]= - z[15] + 2*z[19];
    z[68]=n<T>(199,3)*z[68] + 14*z[21];
    z[68]=z[7]*z[68];
    z[32]=z[68] + 14*z[32];
    z[68]=n<T>(2,3)*z[9];
    z[32]=z[32]*z[68];
    z[30]=z[32] + n<T>(1,3)*z[51] + 2*z[30];
    z[32]=2*z[9];
    z[30]=z[30]*z[32];
    z[51]=7*z[24];
    z[70]=4*z[17] - z[51];
    z[71]=16*z[5];
    z[70]=z[70]*z[71];
    z[70]=345*z[15] + z[70];
    z[70]=z[5]*z[70];
    z[72]=7*z[18];
    z[73]=z[67] - z[72];
    z[73]=z[73]*z[64];
    z[73]=n<T>(8305,18)*z[1] + z[73];
    z[73]=z[5]*z[73];
    z[73]= - n<T>(1412,9) + z[73];
    z[73]=z[7]*z[73];
    z[20]=z[20] + z[30] + z[40] + z[45] + z[73] + n<T>(2824,9)*z[6] - n<T>(4355,18)*z[1] + z[70];
    z[20]=z[2]*z[20];
    z[30]=z[19]*z[5];
    z[40]=z[30] - z[36];
    z[45]=n<T>(2,3)*z[1] + z[40];
    z[45]=z[45]*z[6];
    z[70]=z[18] - z[15];
    z[45]=z[45] + n<T>(1,3)*z[70];
    z[73]=npow(z[6],2);
    z[74]=z[73]*z[3];
    z[75]= - 11*z[74] - 22*z[6];
    z[75]=z[45]*z[75];
    z[76]= - 14*z[15] - z[62];
    z[40]=n<T>(28,3)*z[1] - 11*z[40];
    z[40]=z[6]*z[40];
    z[40]= - n<T>(28,3)*z[21] + n<T>(1,3)*z[76] + z[40];
    z[40]=z[7]*z[40];
    z[76]=2*z[21];
    z[77]=z[76] + z[15];
    z[77]=z[77]*z[34];
    z[40]= - n<T>(14,3)*z[77] + z[40] + z[75];
    z[40]=z[40]*z[68];
    z[68]=n<T>(4,3)*z[30] + n<T>(7,9)*z[1] - z[36];
    z[68]=z[6]*z[68];
    z[68]=n<T>(2,9)*z[70] + z[68];
    z[75]=z[3]*z[6];
    z[68]=z[68]*z[75];
    z[62]= - z[59] + z[62];
    z[78]=n<T>(206,3)*z[30] + 8*z[1] - n<T>(125,3)*z[36];
    z[78]=z[6]*z[78];
    z[79]=118*z[30] - n<T>(56,3)*z[1] - 59*z[36];
    z[79]=z[79]*z[49];
    z[40]=z[40] + 22*z[68] + z[79] + n<T>(4,9)*z[62] + z[78];
    z[40]=z[40]*z[32];
    z[62]=npow(z[5],3);
    z[68]=z[62]*z[19];
    z[78]=13*z[68];
    z[79]=npow(z[5],2);
    z[80]= - n<T>(83,9)*z[1] + 8*z[36];
    z[80]=z[80]*z[79];
    z[80]=z[80] - z[78];
    z[81]=2*z[6];
    z[80]=z[80]*z[81];
    z[82]= - n<T>(7,9)*z[15] - z[18];
    z[82]=z[82]*z[63];
    z[82]=n<T>(2983,9)*z[1] + z[82];
    z[82]=z[5]*z[82];
    z[80]=z[82] + z[80];
    z[80]=z[6]*z[80];
    z[82]= - 341*z[15] + n<T>(32,3)*z[18];
    z[82]=z[5]*z[82];
    z[82]=n<T>(149,3)*z[1] + z[82];
    z[80]=n<T>(1,3)*z[82] + z[80];
    z[80]=z[6]*z[80];
    z[82]=npow(z[3],2);
    z[83]=z[82]*z[73];
    z[45]=z[45]*z[83];
    z[45]=n<T>(136,3)*z[45] + z[80] + n<T>(142,9)*z[15] + 15*z[18];
    z[80]=2*z[3];
    z[45]=z[45]*z[80];
    z[84]= - n<T>(155,9)*z[1] + 16*z[36];
    z[84]=z[84]*z[79];
    z[84]=z[84] - 26*z[68];
    z[84]=z[6]*z[84];
    z[85]=n<T>(1,9)*z[15] - z[18];
    z[85]=z[85]*z[50];
    z[85]=n<T>(589,9)*z[1] + z[85];
    z[85]=z[5]*z[85];
    z[85]= - n<T>(175,3) + z[85];
    z[84]=2*z[85] + z[84];
    z[85]=4*z[6];
    z[84]=z[84]*z[85];
    z[86]=8*z[79];
    z[87]= - z[1] + z[36];
    z[87]=z[87]*z[86];
    z[78]=z[87] - z[78];
    z[78]=z[78]*z[85];
    z[87]= - z[70]*z[64];
    z[87]=n<T>(457,3)*z[1] + z[87];
    z[87]=z[5]*z[87];
    z[78]=z[78] + n<T>(1736,3) + z[87];
    z[78]=z[7]*z[78];
    z[87]=3*z[21];
    z[88]=z[15] + z[87];
    z[89]=2*z[7];
    z[88]=z[88]*z[89];
    z[77]=z[88] + z[77];
    z[77]=z[4]*z[77];
    z[88]=464*z[15] + 5023*z[21];
    z[77]=n<T>(1,9)*z[88] + 64*z[77];
    z[77]=z[4]*z[77];
    z[88]=2*z[18];
    z[90]= - 17*z[15] + z[88];
    z[90]=z[5]*z[90];
    z[90]=881*z[1] + n<T>(32,3)*z[90];
    z[40]=z[40] + z[45] + z[77] + z[78] + n<T>(1,3)*z[90] + z[84];
    z[32]=z[40]*z[32];
    z[40]=5*z[79] - n<T>(17,9)*z[82];
    z[45]=4*z[3];
    z[77]=npow(z[19],4);
    z[40]=z[45]*z[77]*z[40];
    z[78]=z[77]*z[50];
    z[84]=npow(z[6],3);
    z[90]=z[84]*z[24];
    z[91]=z[7]*z[90];
    z[92]=z[77]*z[3];
    z[78]=z[92] + z[78] + z[91];
    z[91]=npow(z[7],4);
    z[93]=z[91]*z[23];
    z[94]=z[4]*z[93];
    z[78]= - 14*z[94] + 11*z[78];
    z[78]=z[9]*z[78];
    z[94]=z[5]*z[92];
    z[78]= - 55*z[94] + z[78];
    z[94]=n<T>(1,9)*z[9];
    z[78]=z[78]*z[94];
    z[95]=z[23]*z[7];
    z[96]=z[84]*z[95];
    z[77]=2*z[77] + z[96];
    z[77]=z[62]*z[77];
    z[96]=npow(z[4],3);
    z[97]=z[96]*z[93];
    z[40]=z[78] + z[40] + 11*z[77] + 16*z[97];
    z[40]=z[9]*z[40];
    z[39]=z[95] - z[39];
    z[77]=z[39]*z[7];
    z[78]=z[73]*z[23];
    z[77]=z[77] + z[78];
    z[77]=z[77]*z[33];
    z[95]=npow(z[9],2);
    z[97]=n<T>(7,9)*z[95];
    z[98]=z[97]*z[77];
    z[99]=npow(z[4],2);
    z[93]=z[99]*z[93];
    z[93]= - 40*z[93] + z[98];
    z[93]=z[9]*z[93];
    z[77]= - z[9]*z[77];
    z[98]=npow(z[7],3);
    z[100]=z[98]*z[4];
    z[101]=z[39]*z[100];
    z[77]=z[101] + z[77];
    z[101]=npow(z[2],2);
    z[102]=n<T>(16,3)*z[101];
    z[77]=z[77]*z[102];
    z[103]=npow(z[48],3);
    z[39]=z[39]*z[103];
    z[39]=z[77] + 8*z[39] + z[93];
    z[39]=z[2]*z[39];
    z[77]=n<T>(11,9)*z[95] - n<T>(68,9)*z[82];
    z[93]=npow(z[19],5);
    z[77]=z[77]*z[93]*z[5];
    z[93]=z[62]*z[93];
    z[77]=11*z[93] + z[77];
    z[77]=z[3]*z[77];
    z[93]= - z[102] + z[97];
    z[97]=npow(z[21],5);
    z[93]=z[93]*z[97]*z[4];
    z[96]=z[96]*z[97];
    z[93]= - 8*z[96] + z[93];
    z[93]=z[2]*z[93];
    z[77]=z[77] + z[93];
    z[77]=z[8]*z[9]*z[77];
    z[93]=z[5]*z[82];
    z[93]= - 21*z[62] + n<T>(68,3)*z[93];
    z[92]=z[93]*z[92];
    z[39]=z[77] + z[39] + z[92] + z[40];
    z[40]=2*z[8];
    z[39]=z[39]*z[40];
    z[77]=16*z[17];
    z[92]=z[98]*z[99]*z[77];
    z[93]=z[84]*z[17];
    z[96]=z[93]*z[86];
    z[97]=z[73]*z[17];
    z[102]=z[97]*z[7];
    z[104]=3*z[79];
    z[105]=z[104]*z[102];
    z[92]=z[92] + z[96] + z[105];
    z[96]=z[17]*z[7];
    z[105]=z[96] - z[28];
    z[106]=z[105]*z[7];
    z[106]=z[106] + z[97];
    z[107]=7*z[7];
    z[108]= - z[106]*z[107];
    z[108]=11*z[93] + z[108];
    z[108]=z[9]*z[108];
    z[109]=z[93]*z[3];
    z[110]=z[7]*z[73];
    z[110]= - n<T>(287,3)*z[84] - 59*z[110];
    z[110]=z[18]*z[110];
    z[108]=n<T>(4,3)*z[108] - n<T>(110,3)*z[109] + z[110];
    z[110]=n<T>(1,3)*z[9];
    z[108]=z[108]*z[110];
    z[111]=z[18]*z[3];
    z[112]=z[84]*z[111];
    z[92]=z[108] + 8*z[92] - n<T>(1099,3)*z[112];
    z[92]=z[9]*z[92];
    z[102]= - 4*z[93] - z[102];
    z[102]=z[62]*z[102];
    z[108]=z[84]*z[18];
    z[108]= - n<T>(1,3)*z[108] + z[109];
    z[108]=z[3]*z[108];
    z[109]=9*z[79];
    z[93]= - z[93]*z[109];
    z[93]=z[93] + n<T>(17,3)*z[108];
    z[93]=z[93]*z[45];
    z[77]= - z[103]*z[77];
    z[77]=z[93] + 7*z[102] + z[77];
    z[93]=z[99]*z[33];
    z[99]=n<T>(1,3)*z[28] + 32*z[96];
    z[99]=z[99]*z[93];
    z[102]=z[81]*z[17];
    z[108]= - z[102] + z[96];
    z[108]=z[7]*z[108];
    z[108]= - 199*z[97] + 14*z[108];
    z[108]=z[9]*z[7]*z[108];
    z[112]=z[17]*z[100];
    z[108]= - 4493*z[112] + z[108];
    z[94]=z[108]*z[94];
    z[94]=2*z[99] + z[94];
    z[94]=z[2]*z[94];
    z[39]=z[39] + z[94] + 2*z[77] + z[92];
    z[39]=z[39]*z[40];
    z[77]=z[73]*z[15];
    z[92]=z[81]*z[15];
    z[94]=z[15]*z[7];
    z[99]=z[92] - z[94];
    z[107]=z[99]*z[107];
    z[107]= - 11*z[77] + z[107];
    z[107]=z[9]*z[107];
    z[108]=z[62]*z[6];
    z[112]=z[104] + z[108];
    z[113]=z[112]*z[81];
    z[113]= - z[113] + n<T>(287,9)*z[5];
    z[114]=z[113]*z[6];
    z[115]=z[114] + n<T>(323,9);
    z[116]= - z[6]*z[115];
    z[116]= - n<T>(1975,9)*z[36] + z[116];
    z[116]=z[116]*z[73];
    z[117]=z[108] + 2*z[79];
    z[118]=z[117]*z[81];
    z[119]=n<T>(323,9)*z[5];
    z[118]=z[118] - z[119];
    z[118]=z[118]*z[6];
    z[120]=n<T>(514,9) + z[118];
    z[120]=z[120]*z[81];
    z[120]= - n<T>(2663,9)*z[36] + z[120];
    z[120]=z[6]*z[120];
    z[121]=z[7] - z[6];
    z[122]=z[121]*z[7];
    z[120]=z[120] + n<T>(1028,9)*z[122];
    z[120]=z[7]*z[120];
    z[123]=1748*z[15] + 257*z[33];
    z[123]=z[123]*z[34];
    z[124]=z[108] + 4*z[79];
    z[125]=z[124]*z[81];
    z[125]= - z[125] + n<T>(233,9)*z[5];
    z[125]=z[125]*z[6];
    z[125]=z[125] + n<T>(287,9);
    z[126]=2*z[73];
    z[127]= - z[125]*z[126];
    z[127]= - n<T>(491,3)*z[15] + z[127];
    z[127]=z[127]*z[74];
    z[107]=n<T>(8,9)*z[107] + z[127] + n<T>(4,9)*z[123] + 4*z[116] + z[120];
    z[107]=z[9]*z[107];
    z[113]=z[113]*z[81];
    z[116]=z[79]*z[15];
    z[120]=z[113] + n<T>(646,9) - 33*z[116];
    z[120]=z[120]*z[126];
    z[116]=6*z[116];
    z[123]= - n<T>(257,9) - z[116];
    z[118]=2*z[123] - z[118];
    z[118]=z[7]*z[6]*z[118];
    z[123]=6*z[6];
    z[124]=z[123]*z[124];
    z[124]= - z[124] + n<T>(233,3)*z[5];
    z[124]=z[124]*z[6];
    z[124]=z[124] + n<T>(287,3);
    z[124]=z[124]*z[6];
    z[127]=n<T>(6727,9)*z[36] + z[124];
    z[127]=z[127]*z[73];
    z[128]=z[15]*z[74];
    z[127]=z[127] - n<T>(136,9)*z[128];
    z[127]=z[3]*z[127];
    z[128]=z[15]*z[34];
    z[98]= - n<T>(257,3)*z[98] - 71*z[128];
    z[98]=z[4]*z[98];
    z[98]=z[127] + n<T>(2,3)*z[98] + z[120] + z[118];
    z[118]=z[122] + z[73];
    z[120]= - 1301*z[15] - n<T>(257,3)*z[118];
    z[120]=z[120]*z[89];
    z[120]=n<T>(4751,3)*z[22] + z[120];
    z[49]=z[120]*z[49];
    z[120]=z[4]*npow(z[7],5);
    z[49]= - n<T>(514,9)*z[120] - 219*z[77] + z[49];
    z[49]=z[9]*z[49];
    z[120]=2380*z[15] + 257*z[122];
    z[120]=z[7]*z[120];
    z[120]= - 850*z[22] + z[120];
    z[120]=z[120]*z[48];
    z[49]=n<T>(2,9)*z[120] + z[49];
    z[49]=z[2]*z[49];
    z[39]=z[39] + z[49] + 2*z[98] + z[107];
    z[39]=z[8]*z[39];
    z[49]= - 23*z[79] - 10*z[108];
    z[49]=z[6]*z[49];
    z[49]=n<T>(1220,9)*z[5] + z[49];
    z[49]=z[6]*z[49];
    z[49]=n<T>(938,9) + z[49];
    z[49]=z[49]*z[126];
    z[86]= - z[86] - 5*z[108];
    z[86]=z[86]*z[81];
    z[86]=n<T>(323,3)*z[5] + z[86];
    z[86]=z[6]*z[86];
    z[86]= - n<T>(1678,9) + z[86];
    z[86]=z[6]*z[86];
    z[86]=z[86] + n<T>(1678,9)*z[7];
    z[86]=z[7]*z[86];
    z[98]=n<T>(1292,9) + 5*z[114];
    z[98]=z[3]*z[98]*z[84];
    z[49]=z[98] + n<T>(1678,9)*z[100] + z[49] + z[86];
    z[49]=z[9]*z[49];
    z[86]=35*z[79] + 16*z[108];
    z[86]=z[6]*z[86];
    z[86]= - n<T>(616,3)*z[5] + z[86];
    z[86]=z[6]*z[86];
    z[86]= - n<T>(1195,9) + z[86];
    z[86]=z[6]*z[86];
    z[98]=z[62]*z[81];
    z[98]=z[104] + z[98];
    z[98]=z[98]*z[81];
    z[98]= - z[119] + z[98];
    z[98]=z[6]*z[98];
    z[98]=n<T>(257,9) + z[98];
    z[98]=z[7]*z[98];
    z[86]= - n<T>(194,3)*z[34] + z[86] + z[98];
    z[98]= - z[6] + n<T>(5,3)*z[7];
    z[98]=z[98]*z[34];
    z[104]= - z[126] - 5*z[122];
    z[104]=z[7]*z[104];
    z[91]=z[4]*z[91];
    z[91]=z[104] - 5*z[91];
    z[91]=z[91]*z[110];
    z[91]=z[98] + z[91];
    z[91]=z[2]*z[91];
    z[98]=z[112]*z[123];
    z[98]= - n<T>(287,3)*z[5] + z[98];
    z[98]=z[98]*z[85];
    z[98]= - static_cast<T>(323)+ z[98];
    z[98]=z[98]*z[74];
    z[49]=n<T>(257,3)*z[91] + z[49] + 2*z[86] + z[98];
    z[39]=2*z[49] + z[39];
    z[39]=z[39]*z[40];
    z[49]= - z[62]*z[78];
    z[86]=z[24]*z[83];
    z[49]=z[49] + n<T>(34,9)*z[86];
    z[49]=z[8]*z[49];
    z[86]= - z[79]*z[28];
    z[91]= - z[6]*z[18];
    z[98]=z[3]*z[28];
    z[91]=z[91] + z[98];
    z[91]=z[3]*z[91];
    z[49]=z[49] + z[86] + n<T>(34,9)*z[91];
    z[86]=4*z[8];
    z[49]=z[86]*z[49];
    z[91]=z[6]*z[125];
    z[98]=z[15]*z[3];
    z[49]=z[49] - n<T>(136,9)*z[98] + 88*z[36] + z[91];
    z[49]=z[8]*z[3]*z[49];
    z[91]= - n<T>(323,9) - z[113];
    z[91]=z[3]*z[91];
    z[49]=z[91] + z[49];
    z[49]=z[49]*z[86];
    z[91]=z[61] + z[36];
    z[91]=z[91]*z[79];
    z[91]=z[91] + z[68];
    z[104]=z[30] + z[36];
    z[104]= - z[1] - n<T>(145,9)*z[104];
    z[104]=z[3]*z[104];
    z[107]=z[5]*z[1];
    z[104]= - n<T>(290,9)*z[107] + z[104];
    z[104]=z[3]*z[104];
    z[91]=4*z[91] + z[104];
    z[91]=z[91]*z[45];
    z[104]= - z[15] - z[18];
    z[104]=z[104]*z[71];
    z[104]=n<T>(707,6)*z[1] + z[104];
    z[104]=z[5]*z[104];
    z[113]=2*z[36] + z[111];
    z[113]=z[3]*z[113];
    z[104]=z[104] + n<T>(580,9)*z[113];
    z[104]=z[3]*z[104];
    z[101]=z[101]*z[111];
    z[101]=z[104] + n<T>(404,3)*z[101];
    z[101]=z[2]*z[101];
    z[49]=z[49] + z[91] + z[101];
    z[49]=z[10]*z[49];
    z[91]=2*z[68];
    z[101]= - n<T>(14,9)*z[1] + z[36];
    z[101]=z[101]*z[79];
    z[101]=z[101] - z[91];
    z[101]=z[6]*z[101];
    z[104]=z[61] - z[36];
    z[104]=145*z[104] + 698*z[30];
    z[104]=z[6]*z[104];
    z[104]=n<T>(1,9)*z[104] - z[15] - n<T>(145,9)*z[18];
    z[104]=z[3]*z[104];
    z[113]=n<T>(308,3)*z[30] - 136*z[1] - 145*z[36];
    z[104]=n<T>(1,3)*z[113] + z[104];
    z[80]=z[104]*z[80];
    z[104]=z[67] + z[88];
    z[104]=z[104]*z[64];
    z[104]=n<T>(3659,9)*z[1] + z[104];
    z[104]=z[5]*z[104];
    z[80]=z[80] + 16*z[101] - n<T>(182,3) + z[104];
    z[80]=z[3]*z[80];
    z[101]=z[1] + z[36];
    z[101]=z[101]*z[64];
    z[101]= - n<T>(1727,9) + z[101];
    z[101]=z[5]*z[101];
    z[101]=z[101] - 8*z[68];
    z[80]=2*z[101] + z[80];
    z[101]= - z[17] - 2*z[24];
    z[71]=z[101]*z[71];
    z[71]=n<T>(313,18)*z[15] + z[71];
    z[71]=z[5]*z[71];
    z[101]=z[24]*z[58];
    z[101]=z[18] + z[101];
    z[65]=z[101]*z[65];
    z[65]=z[65] + n<T>(3584,9)*z[1] + z[71];
    z[65]=z[3]*z[65];
    z[41]=z[24]*z[41];
    z[35]=z[41] - 78*z[15] + z[35];
    z[35]=z[2]*z[35];
    z[41]= - z[61] + n<T>(7,3)*z[36];
    z[35]=z[35] + 23*z[41] + n<T>(196,3)*z[111];
    z[35]=z[35]*z[42];
    z[41]=z[17]*z[79];
    z[41]=n<T>(5515,18)*z[1] - 32*z[41];
    z[41]=z[5]*z[41];
    z[35]=z[35] + z[65] - n<T>(1412,9) + z[41];
    z[35]=z[2]*z[35];
    z[41]=z[97]*z[3];
    z[42]=z[73]*z[18];
    z[42]=n<T>(2,3)*z[42] - z[41];
    z[42]=z[3]*z[42];
    z[65]=z[84]*z[23];
    z[71]=z[62]*z[65];
    z[84]=z[82]*z[90];
    z[71]=3*z[71] - n<T>(17,3)*z[84];
    z[40]=z[71]*z[40];
    z[71]=z[97]*z[109];
    z[40]=z[40] + z[71] + n<T>(34,3)*z[42];
    z[40]=z[3]*z[40];
    z[42]=z[62]*z[97];
    z[40]=4*z[42] + z[40];
    z[40]=z[40]*z[86];
    z[42]=z[116] - z[115];
    z[42]=z[42]*z[81];
    z[71]= - n<T>(1378,3)*z[36] - z[124];
    z[71]=z[6]*z[71];
    z[84]=z[22]*z[3];
    z[71]=z[71] + n<T>(272,9)*z[84];
    z[71]=z[3]*z[71];
    z[40]=z[40] + z[42] + z[71];
    z[40]=z[8]*z[40];
    z[42]=3*z[6];
    z[71]= - z[117]*z[42];
    z[71]=n<T>(314,9)*z[5] + z[71];
    z[71]=z[71]*z[81];
    z[71]=n<T>(257,9) + z[71];
    z[90]=z[6]*z[112];
    z[90]=287*z[5] - 18*z[90];
    z[90]=z[6]*z[90];
    z[90]=n<T>(646,3) + z[90];
    z[90]=z[90]*z[75];
    z[40]=z[40] + 2*z[71] + z[90];
    z[40]=z[40]*z[86];
    z[35]=z[49] + z[40] + 2*z[80] + z[35];
    z[35]=z[10]*z[35];
    z[40]= - 4*z[28] + z[96];
    z[40]=z[7]*z[40];
    z[40]=z[97] + z[40];
    z[49]=npow(z[8],3);
    z[71]=n<T>(1,3)*z[49];
    z[40]=z[40]*z[71];
    z[80]=z[12]*z[31];
    z[80]=z[80] - n<T>(2,3)*z[105];
    z[80]=z[49]*z[80];
    z[80]= - z[1] + z[80];
    z[80]=z[12]*z[80];
    z[29]=z[80] + z[40] + z[29];
    z[29]=z[12]*z[29];
    z[40]=z[28]*z[7];
    z[80]= - z[97] + z[40];
    z[80]=z[80]*z[89];
    z[90]=z[33]*z[8];
    z[78]=z[2]*z[78]*z[90];
    z[78]=z[80] + z[78];
    z[71]=z[78]*z[71];
    z[78]= - z[9]*z[34];
    z[78]=z[78] - 1;
    z[31]=z[31]*z[78];
    z[78]=z[105]*z[44];
    z[80]=z[19]*z[7];
    z[29]=z[29] + z[71] + z[78] + z[80] + z[31];
    z[29]=z[13]*z[29];
    z[31]=1154*z[22] - 1235*z[94];
    z[71]=z[12]*z[15];
    z[31]=973*z[71] + 2*z[31];
    z[71]=npow(z[8],2);
    z[31]=z[71]*z[31];
    z[31]= - 3353*z[1] + z[31];
    z[31]=z[12]*z[31];
    z[31]=z[31] + 3967*z[21] + 3787*z[15] - 3643*z[19];
    z[40]= - 445*z[97] + 499*z[40];
    z[40]=z[8]*z[2]*z[40];
    z[40]=z[40] - n<T>(3805,3)*z[22] + 499*z[94];
    z[40]=z[7]*z[40];
    z[40]=445*z[77] + z[40];
    z[40]=z[40]*z[71];
    z[78]=n<T>(557,3)*z[94] + 469*z[17];
    z[101]=n<T>(506,3)*z[22] + z[78];
    z[101]=z[4]*z[101];
    z[78]= - z[78]*z[48];
    z[78]= - n<T>(901,3)*z[94] + z[78];
    z[78]=z[9]*z[78];
    z[80]=z[2]*z[80];
    z[29]=434*z[29] + z[40] + n<T>(1841,3)*z[80] + z[78] + z[101] + n<T>(1,3)*
    z[31];
    z[29]=z[13]*z[29];
    z[31]= - 1489*z[21] - 1450*z[15] - 253*z[19];
    z[31]=z[31]*z[44];
    z[40]=1882*z[21] + 2399*z[15] - 968*z[19];
    z[44]=1007*z[15] + n<T>(2188,3)*z[21];
    z[44]=z[7]*z[44];
    z[44]=n<T>(1178,3)*z[17] + z[44];
    z[44]=z[4]*z[44];
    z[40]=n<T>(1,3)*z[40] + z[44];
    z[40]=z[9]*z[40];
    z[44]=1333*z[21] + 229*z[15] - 343*z[19];
    z[78]=n<T>(1,3)*z[2];
    z[44]=z[44]*z[78];
    z[80]=305*z[22] + 266*z[94];
    z[80]=z[7]*z[80];
    z[77]=n<T>(367,3)*z[77] + z[80];
    z[77]=z[2]*z[77]*z[71];
    z[29]=z[29] - n<T>(1178,3)*z[12] + z[77] + z[44] + z[40] + z[31] + n<T>(164,3)*z[7] + 709*z[1] - n<T>(992,3)*z[6];
    z[29]=z[13]*z[29];
    z[31]= - 235*z[84] - 16*z[21] - 937*z[15] + 1790*z[19];
    z[40]=27*z[17];
    z[44]= - z[40] + n<T>(733,9)*z[22];
    z[44]=z[44]*z[75];
    z[40]=z[44] - n<T>(8,9)*z[94] - z[40] + n<T>(725,9)*z[22];
    z[40]=z[9]*z[40];
    z[44]=z[41] + z[102] + z[96];
    z[44]=z[9]*z[44];
    z[22]=z[22] + z[94];
    z[77]= - z[10]*z[33]*npow(z[8],4)*z[24];
    z[77]=z[87] + z[77];
    z[77]=z[10]*z[77];
    z[22]=z[77] + 3*z[22] + z[44];
    z[22]=z[11]*z[22];
    z[44]=z[18]*z[90];
    z[44]=n<T>(1057,3)*z[94] + 355*z[44];
    z[44]=z[44]*z[71];
    z[49]=z[10]*z[7]*z[49]*z[18];
    z[77]=z[21]*z[5];
    z[44]=n<T>(559,9)*z[49] + n<T>(1,3)*z[44] - n<T>(794,9)*z[1] + 39*z[77];
    z[44]=z[10]*z[44];
    z[22]=12*z[22] + z[44] + n<T>(1,9)*z[31] + z[40];
    z[22]=z[11]*z[22];
    z[31]= - static_cast<T>(305)- n<T>(448,3)*z[107];
    z[31]=z[31]*z[89];
    z[40]= - 1063*z[15] + 3964*z[19];
    z[40]=z[40]*z[58];
    z[44]= - 1192*z[6] + 1408*z[1] + 83*z[36];
    z[31]=z[40] + n<T>(1,3)*z[44] + z[31];
    z[40]= - n<T>(247,9)*z[21] + n<T>(109,3)*z[15] - 167*z[19];
    z[44]=n<T>(1201,3)*z[15] - 994*z[19];
    z[44]=z[6]*z[44];
    z[44]= - n<T>(326,3)*z[17] + z[44];
    z[44]=z[44]*z[58];
    z[40]=2*z[40] + z[44];
    z[40]=z[9]*z[40];
    z[44]=z[71]*z[36];
    z[49]=n<T>(437,9)*z[10] - n<T>(445,3)*z[7];
    z[49]=z[44]*z[49];
    z[58]=z[3]*z[1];
    z[80]=n<T>(230,3) + 7*z[107];
    z[49]=2*z[80] - n<T>(1231,9)*z[58] + z[49];
    z[49]=z[10]*z[49];
    z[44]=z[33]*z[44];
    z[22]=z[22] + z[49] - n<T>(1336,9)*z[44] + n<T>(1,3)*z[31] + z[40];
    z[22]=z[11]*z[22];
    z[31]= - 1405*z[15] - 514*z[33];
    z[31]=z[31]*z[48];
    z[31]= - 514*z[118] + z[31];
    z[33]=13*z[15] + n<T>(646,9)*z[73];
    z[33]=z[33]*z[75];
    z[40]=z[9]*z[99];
    z[31]= - n<T>(28,9)*z[40] + n<T>(1,9)*z[31] + z[33];
    z[31]=z[9]*z[31];
    z[33]=z[23]*z[103];
    z[40]=npow(z[3],3);
    z[44]=z[40]*z[65];
    z[33]= - 2*z[33] + n<T>(17,9)*z[44];
    z[44]=z[23]*z[100];
    z[49]=z[3]*z[65];
    z[44]=7*z[44] - 11*z[49];
    z[44]=z[44]*z[95];
    z[33]=4*z[33] + n<T>(1,9)*z[44];
    z[33]=z[8]*z[33];
    z[44]=z[9]*z[106];
    z[41]=11*z[41] + n<T>(7,3)*z[44];
    z[41]=z[41]*z[110];
    z[44]=z[17]*z[93];
    z[33]=z[33] - 24*z[44] + z[41];
    z[33]=z[9]*z[33];
    z[41]=z[40]*z[97];
    z[33]= - n<T>(68,3)*z[41] + z[33];
    z[33]=z[33]*z[86];
    z[41]=z[81] - z[7];
    z[41]=n<T>(257,3)*z[41] - 323*z[74];
    z[31]=z[33] + n<T>(2,3)*z[41] + z[31];
    z[31]=z[8]*z[31];
    z[33]= - z[74] - z[34] - z[121];
    z[33]=z[9]*z[33];
    z[41]=z[3]*z[81];
    z[33]=z[33] - static_cast<T>(1)+ z[41];
    z[31]=n<T>(262,3)*z[33] + z[31];
    z[31]=z[8]*z[31];
    z[33]=z[34]*z[1];
    z[41]=z[33] + z[21];
    z[44]= - z[19] + z[41];
    z[49]=z[73]*z[58];
    z[44]=7*z[44] + 11*z[49];
    z[44]=z[9]*z[44];
    z[49]=z[3]*z[19];
    z[44]=z[44] + 7*z[1] - 22*z[49];
    z[44]=z[9]*z[44];
    z[49]=static_cast<T>(71)- 68*z[83];
    z[49]=z[49]*z[58];
    z[44]=z[49] + z[44];
    z[33]= - z[76] - z[33];
    z[33]=z[4]*z[33];
    z[33]= - n<T>(29,9)*z[1] + 4*z[33];
    z[33]=z[33]*z[57];
    z[33]= - static_cast<T>(89)+ z[33] + n<T>(4,9)*z[44];
    z[33]=z[9]*z[33];
    z[44]=z[40]*z[8];
    z[28]=z[28]*z[44];
    z[28]=136*z[28] - n<T>(257,3) + 323*z[75];
    z[28]=z[8]*z[28];
    z[28]= - 131*z[3] + z[28];
    z[28]=z[8]*z[28];
    z[44]=z[17]*z[44];
    z[44]= - 19*z[3] - 8*z[44];
    z[44]=z[10]*z[44]*z[71];
    z[40]=z[1]*z[40];
    z[28]=n<T>(17,3)*z[44] - n<T>(136,3)*z[40] + z[28];
    z[28]=z[10]*z[28];
    z[19]=z[82]*z[19];
    z[19]= - static_cast<T>(205)+ 544*z[19];
    z[40]=n<T>(1,9)*z[3];
    z[19]=z[19]*z[40];
    z[19]=n<T>(2,3)*z[28] + z[31] + z[19] + z[33];
    z[19]=z[12]*z[19];
    z[28]=n<T>(37,9)*z[1] - z[36];
    z[28]=z[28]*z[64];
    z[28]=n<T>(10099,9) + z[28];
    z[28]=z[5]*z[28];
    z[28]=z[28] + 104*z[68];
    z[28]=z[6]*z[28];
    z[31]=4*z[1];
    z[33]=z[31] + 3*z[36];
    z[33]=z[33]*z[50];
    z[33]=n<T>(527,3) + z[33];
    z[33]=z[5]*z[33];
    z[33]=z[33] + 14*z[68];
    z[33]=z[33]*z[89];
    z[44]=n<T>(26,9)*z[15] + z[72];
    z[44]=z[44]*z[64];
    z[44]= - n<T>(1087,3)*z[1] + z[44];
    z[44]=z[5]*z[44];
    z[19]=z[22] + z[19] + z[33] + z[28] + n<T>(733,3) + z[44];
    z[22]=z[17]*z[3];
    z[28]= - z[2]*z[22];
    z[28]=2*z[98] + z[28];
    z[28]=z[10]*z[28];
    z[33]=z[2]*z[69];
    z[28]=z[28] - z[15] + z[33];
    z[28]=z[10]*z[28];
    z[33]=z[70]*z[34];
    z[34]=z[58]*z[10];
    z[44]= - z[1] - z[34];
    z[44]=z[10]*z[44];
    z[41]=z[44] - z[41];
    z[41]=z[12]*z[41];
    z[44]= - z[2]*z[96];
    z[28]=z[41] + z[28] + z[44] - z[94] + z[33];
    z[28]=z[14]*z[28];
    z[33]= - 323*z[15] + 643*z[22];
    z[33]=z[33]*z[78];
    z[41]=z[2]*z[98];
    z[41]= - z[58] + z[41];
    z[41]=z[10]*z[41];
    z[33]=193*z[41] + z[33] - n<T>(875,3)*z[1] - 313*z[98];
    z[33]=z[10]*z[33];
    z[41]= - 643*z[17] - 256*z[94];
    z[41]=z[2]*z[41];
    z[44]= - 875*z[1] + 599*z[36];
    z[44]=z[7]*z[44];
    z[41]=z[44] + z[41];
    z[44]= - n<T>(175,9)*z[1] + 19*z[36];
    z[44]=z[7]*z[44];
    z[44]=n<T>(407,9)*z[70] + 5*z[44];
    z[44]=z[44]*z[48];
    z[48]=z[4]*z[21];
    z[48]=n<T>(74,3)*z[34] + 23*z[1] + n<T>(64,3)*z[48];
    z[48]=z[12]*z[48];
    z[28]=n<T>(20,9)*z[28] + n<T>(4,3)*z[48] + n<T>(1,3)*z[33] + z[44] - 43*z[15] + 
   n<T>(1,9)*z[41];
    z[28]=z[14]*z[28];
    z[33]= - n<T>(7733,2)*z[34] - 661*z[1] - 6317*z[98];
    z[33]=z[2]*z[33];
    z[33]=n<T>(10049,2)*z[107] + 1819*z[58] + z[33];
    z[33]=z[10]*z[33];
    z[34]=179*z[1] + 1084*z[36];
    z[33]=z[33] + 7*z[34] + n<T>(12475,2)*z[77];
    z[34]=517*z[15] + n<T>(10631,2)*z[18];
    z[41]=n<T>(11963,18)*z[77] - n<T>(497,9)*z[1] + 1119*z[36];
    z[41]=z[7]*z[41];
    z[34]=n<T>(1,9)*z[34] + z[41];
    z[34]=z[4]*z[34];
    z[21]=251*z[15] + 256*z[21];
    z[21]=n<T>(1,3)*z[21] - n<T>(3031,2)*z[22];
    z[21]=z[21]*z[78];
    z[22]=32*z[1];
    z[41]=z[4]*z[22];
    z[41]=z[41] - 37*z[58];
    z[41]=z[12]*z[41];
    z[21]=2*z[28] + n<T>(8,9)*z[41] + z[21] + 115*z[98] + z[34] + n<T>(1,9)*
    z[33];
    z[21]=z[14]*z[21];
    z[28]=z[108]*z[15];
    z[16]= - z[16] + 6*z[18];
    z[16]=z[5]*z[16];
    z[16]=z[61] + 3*z[16];
    z[16]=z[5]*z[16];
    z[33]=z[62]*z[94];
    z[16]=2*z[33] + z[16] + 6*z[28];
    z[16]=z[7]*z[16];
    z[33]=z[92]*z[62];
    z[34]= - z[67] + 8*z[18];
    z[34]=z[34]*z[79];
    z[34]=z[34] + z[33];
    z[34]=z[34]*z[42];
    z[41]= - z[52] + 6*z[17];
    z[42]=6*z[5];
    z[44]= - z[41]*z[42];
    z[44]=n<T>(199,3)*z[15] + z[44];
    z[44]=z[5]*z[44];
    z[16]=z[16] + z[34] + z[1] + z[44];
    z[16]=z[7]*z[16];
    z[34]= - z[15] + z[88];
    z[34]=z[34]*z[79]*z[81];
    z[41]= - z[41]*z[50];
    z[41]=z[67] + z[41];
    z[41]=z[5]*z[41];
    z[34]=z[41] + z[34];
    z[34]=z[6]*z[34];
    z[41]= - z[56]*z[50];
    z[42]=z[70]*z[42];
    z[42]=z[1] + z[42];
    z[42]=z[6]*z[42];
    z[41]=z[42] + z[54] + z[41];
    z[41]=z[5]*z[41];
    z[42]= - z[67] + z[88];
    z[42]=z[5]*z[42];
    z[42]=z[1] + z[42];
    z[42]=z[7]*z[42]*z[50];
    z[41]=z[42] - n<T>(53,3)*z[1] + z[41];
    z[41]=z[7]*z[41];
    z[42]= - z[60]*z[50];
    z[42]=n<T>(131,3)*z[17] + z[42];
    z[42]=z[5]*z[42];
    z[34]=z[41] + z[34] - n<T>(5,3)*z[15] + z[42];
    z[34]=z[7]*z[34];
    z[41]=3*z[17];
    z[42]=z[41] - z[52];
    z[44]= - z[42]*z[79];
    z[48]=z[17]*z[108];
    z[44]=z[44] + z[48];
    z[44]=z[6]*z[44];
    z[46]= - z[46] + 8*z[23];
    z[48]= - z[5]*z[46];
    z[48]=n<T>(52,3)*z[17] + z[48];
    z[48]=z[5]*z[48];
    z[44]=z[48] + z[44];
    z[44]=z[6]*z[44];
    z[34]=z[34] - z[47] + z[44];
    z[34]=z[4]*z[34];
    z[44]= - z[15] + 14*z[18];
    z[44]=z[44]*z[79];
    z[33]=z[44] + z[33];
    z[33]=z[6]*z[33];
    z[42]= - z[42]*z[50];
    z[42]=n<T>(43,3)*z[15] + z[42];
    z[42]=z[42]*z[50];
    z[33]=z[42] + z[33];
    z[33]=z[6]*z[33];
    z[42]= - z[46]*z[50];
    z[42]=n<T>(233,3)*z[17] + z[42];
    z[42]=z[42]*z[50];
    z[16]=z[34] + z[16] + z[33] + n<T>(31,3)*z[15] + z[42];
    z[16]=z[16]*z[57];
    z[31]=z[31] + 17*z[36];
    z[31]=z[31]*z[79];
    z[31]=z[31] + z[91];
    z[31]=z[31]*z[85];
    z[33]=z[61] + 7*z[36];
    z[33]=z[33]*z[79];
    z[33]=z[33] + z[68];
    z[33]=z[7]*z[33];
    z[34]= - z[59] + 31*z[18];
    z[34]=z[34]*z[63];
    z[42]=n<T>(725,9)*z[1];
    z[34]=z[42] + z[34];
    z[34]=z[5]*z[34];
    z[31]=4*z[33] + z[31] - n<T>(1940,9) + z[34];
    z[31]=z[31]*z[89];
    z[33]=z[61] + 13*z[36];
    z[33]=z[33]*z[79];
    z[33]=z[33] + z[68];
    z[34]=8*z[6];
    z[33]=z[33]*z[34];
    z[44]=4*z[15] + 29*z[18];
    z[44]=z[44]*z[64];
    z[42]=z[42] + z[44];
    z[42]=z[5]*z[42];
    z[33]=z[42] + z[33];
    z[33]=z[6]*z[33];
    z[42]= - 17*z[17] + 33*z[24];
    z[42]=z[42]*z[64];
    z[42]=n<T>(12637,18)*z[15] + z[42];
    z[42]=z[5]*z[42];
    z[16]=z[16] + z[31] + z[33] - z[22] + z[42];
    z[16]=z[4]*z[16];
    z[22]=z[62]*z[66];
    z[31]=z[108]*z[41];
    z[22]=z[22] + z[31];
    z[22]=z[6]*z[22];
    z[31]=z[27]*z[43];
    z[31]=43*z[17] + z[31];
    z[31]=z[5]*z[31];
    z[22]=z[31] + z[22];
    z[22]=z[6]*z[22];
    z[27]=z[27]*z[79];
    z[31]=z[23]*z[108];
    z[27]=z[27] + z[31];
    z[27]=z[6]*z[27];
    z[25]= - z[38] + 2*z[25];
    z[25]=z[25]*z[5];
    z[25]=z[25] - z[53];
    z[31]= - z[5]*z[25];
    z[27]=z[31] + z[27];
    z[27]=z[6]*z[27];
    z[27]= - z[37] + z[27];
    z[27]=z[4]*z[27];
    z[25]= - z[25]*z[43];
    z[22]=z[27] + z[25] + z[22];
    z[22]=z[22]*z[57];
    z[25]=z[15] + 3*z[18];
    z[25]=z[25]*z[79];
    z[25]=z[25] + z[28];
    z[25]=z[6]*z[25];
    z[27]=z[23]*z[79];
    z[27]=n<T>(2413,18)*z[15] + 88*z[27];
    z[27]=z[5]*z[27];
    z[25]=z[27] + 24*z[25];
    z[25]=z[6]*z[25];
    z[23]= - 9*z[23] + z[55];
    z[23]=z[23]*z[64];
    z[17]=n<T>(7837,18)*z[17] + z[23];
    z[17]=z[5]*z[17];
    z[17]=z[22] + z[25] - n<T>(283,9)*z[15] + z[17];
    z[17]=z[4]*z[17];
    z[22]= - 68*z[15] - 145*z[18];
    z[23]=34*z[1] + 37*z[36];
    z[23]=n<T>(2,3)*z[23] - z[30];
    z[23]=z[6]*z[23];
    z[22]=n<T>(2,9)*z[22] + z[23];
    z[23]= - 961*z[30] - 553*z[1] + 698*z[36];
    z[23]=z[6]*z[23];
    z[23]= - 145*z[70] + z[23];
    z[23]=z[6]*z[23];
    z[25]= - z[6]*z[24];
    z[25]= - z[26] + z[25];
    z[25]=z[4]*z[25];
    z[24]=z[24] - z[25];
    z[23]=z[23] - 145*z[24];
    z[23]=z[23]*z[40];
    z[18]= - z[18]*z[81];
    z[18]= - z[52] + z[18];
    z[18]=z[4]*z[18];
    z[18]=z[23] + 2*z[22] + n<T>(145,9)*z[18];
    z[18]=z[18]*z[45];
    z[22]=n<T>(112,9)*z[1] - 5*z[36];
    z[22]=z[22]*z[79];
    z[22]=z[22] + 19*z[68];
    z[22]=z[22]*z[34];
    z[23]=n<T>(5,9)*z[15] + z[72];
    z[23]=z[23]*z[64];
    z[23]= - n<T>(18095,9)*z[1] + z[23];
    z[23]=z[5]*z[23];
    z[22]=z[22] + n<T>(4390,9) + z[23];
    z[22]=z[6]*z[22];
    z[23]=z[69] + z[51];
    z[23]=z[23]*z[64];
    z[15]=n<T>(3931,6)*z[15] + z[23];
    z[15]=z[5]*z[15];
    z[15]=z[18] + z[17] + z[22] - n<T>(997,9)*z[1] + z[15];
    z[15]=z[3]*z[15];

    r += z[15] + z[16] + 2*z[19] + z[20] + z[21] + n<T>(2,3)*z[29] + z[32]
       + z[35] + z[39];
 
    return r;
}

template double qg_2lNLC_r970(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r970(const std::array<dd_real,31>&);
#endif
