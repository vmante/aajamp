#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r835(const std::array<T,31>& k) {
  T z[49];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[12];
    z[6]=k[15];
    z[7]=k[2];
    z[8]=k[9];
    z[9]=k[4];
    z[10]=k[7];
    z[11]=k[14];
    z[12]=k[5];
    z[13]=k[13];
    z[14]=k[3];
    z[15]=z[4]*z[7];
    z[16]=z[5]*z[12];
    z[17]= - 3*z[16] - static_cast<T>(3)- z[15];
    z[17]=z[6]*z[17];
    z[17]= - z[4] + z[17];
    z[18]=static_cast<T>(29)+ 53*z[16];
    z[19]=z[5]*npow(z[12],2);
    z[20]=z[19] + z[12];
    z[21]=z[10]*z[20];
    z[18]=n<T>(1,8)*z[18] - 6*z[21];
    z[18]=z[3]*z[18];
    z[21]= - static_cast<T>(1)- z[16];
    z[21]=z[10]*z[21];
    z[17]=z[18] + n<T>(5,8)*z[17] + 8*z[21];
    z[17]=z[3]*z[17];
    z[18]=z[4]*z[8];
    z[21]=z[8]*z[7];
    z[22]=static_cast<T>(11)+ 7*z[21];
    z[22]=z[22]*z[18];
    z[23]=z[6]*z[12];
    z[24]= - n<T>(27,8)*z[16] - static_cast<T>(4)+ n<T>(27,4)*z[23];
    z[24]=z[5]*z[24];
    z[24]=z[24] + n<T>(43,4)*z[6] + 318*z[13] - n<T>(5,8)*z[4];
    z[24]=z[5]*z[24];
    z[25]=n<T>(305,2)*z[10];
    z[26]= - z[20]*z[25];
    z[26]=z[26] - static_cast<T>(129)- n<T>(305,2)*z[16];
    z[26]=z[10]*z[26];
    z[26]=z[26] + 424*z[13] - 621*z[5];
    z[26]=z[10]*z[26];
    z[27]=npow(z[8],2);
    z[28]=z[6]*z[4];
    z[29]=n<T>(11,2)*z[8];
    z[30]=z[29] + 14*z[13];
    z[30]=z[13]*z[30];
    z[17]=z[17] + z[26] + z[24] + 6*z[28] + n<T>(3,8)*z[22] + 2*z[27] + 
    z[30];
    z[17]=z[3]*z[17];
    z[22]=z[5]*z[6];
    z[24]=n<T>(1,2)*z[28];
    z[26]= - z[24] - z[22];
    z[30]= - z[10]*z[5];
    z[26]=n<T>(1,2)*z[26] + z[30];
    z[30]= - static_cast<T>(1)- z[15];
    z[30]=z[6]*z[30];
    z[31]=static_cast<T>(17)- n<T>(5,2)*z[23];
    z[31]=z[5]*z[31];
    z[30]=n<T>(5,2)*z[30] + z[31];
    z[31]=z[16]*z[10];
    z[30]=n<T>(1,4)*z[30] - 3*z[31];
    z[30]=z[3]*z[30];
    z[26]=5*z[26] + z[30];
    z[26]=z[3]*z[26];
    z[30]=n<T>(43,2) - 9*z[21];
    z[30]=z[30]*z[27];
    z[32]=43*z[13];
    z[33]=z[8]*z[32];
    z[30]=z[30] + z[33];
    z[30]=z[4]*z[30];
    z[33]=npow(z[8],3);
    z[30]=9*z[33] + z[30];
    z[34]=z[4]*z[32];
    z[34]=z[34] + n<T>(155,2)*z[28];
    z[35]=n<T>(1,2)*z[6];
    z[36]=z[12]*z[35];
    z[36]= - static_cast<T>(1)+ z[36];
    z[36]=z[5]*z[36];
    z[36]=n<T>(27,4)*z[36] - 4*z[4] + n<T>(81,4)*z[6];
    z[36]=z[5]*z[36];
    z[34]=n<T>(1,4)*z[34] + z[36];
    z[34]=z[5]*z[34];
    z[31]= - z[5] - z[31];
    z[31]=z[31]*z[25];
    z[36]=z[5]*z[13];
    z[31]=477*z[36] + z[31];
    z[36]=n<T>(1,2)*z[10];
    z[31]=z[31]*z[36];
    z[26]=z[26] + z[31] + n<T>(1,4)*z[30] + z[34];
    z[26]=z[3]*z[26];
    z[30]=z[18]*z[32];
    z[31]=z[33]*z[12];
    z[32]= - n<T>(5,2)*z[27] - z[31];
    z[32]=9*z[32] + n<T>(31,2)*z[18];
    z[32]=z[6]*z[32];
    z[34]= - z[5]*z[35];
    z[34]= - z[28] + z[34];
    z[34]=z[5]*z[34];
    z[37]=npow(z[13],3);
    z[38]=93*z[37];
    z[30]=27*z[34] + z[32] + z[38] + z[30];
    z[30]=z[5]*z[30];
    z[32]=npow(z[11],2);
    z[34]=npow(z[11],3);
    z[39]=7*z[34];
    z[40]=z[9]*z[39];
    z[40]= - n<T>(15,4)*z[32] + z[40];
    z[40]=z[4]*z[40];
    z[41]=z[11]*z[28];
    z[39]= - n<T>(355,4)*z[41] + z[39] + z[40];
    z[40]=n<T>(93,4)*z[37];
    z[41]=z[40]*z[12];
    z[42]=npow(z[13],2);
    z[43]= - 14*z[42] - z[41];
    z[43]=z[5]*z[43];
    z[44]=z[4]*z[9];
    z[45]= - static_cast<T>(1)- z[44];
    z[45]=z[10]*z[45]*z[35];
    z[45]=z[28] + z[45];
    z[45]=z[10]*z[45];
    z[39]=n<T>(597,4)*z[45] + n<T>(5,2)*z[39] + z[43];
    z[39]=z[10]*z[39];
    z[43]=n<T>(1,2)*z[4];
    z[45]=z[43] - z[6];
    z[46]=n<T>(1,2)*z[5];
    z[47]= - z[45]*z[46];
    z[47]=z[28] + z[47];
    z[47]=z[47]*npow(z[5],2);
    z[45]=z[5]*z[45];
    z[45]= - z[24] + z[45];
    z[45]=z[3]*z[45];
    z[24]= - z[5]*z[24];
    z[24]=z[24] + z[45];
    z[24]=z[3]*z[24];
    z[45]=npow(z[5],3);
    z[48]=z[5]*npow(z[3],2);
    z[48]=27*z[45] - 5*z[48];
    z[48]=z[1]*z[28]*z[48];
    z[24]=n<T>(1,4)*z[48] + 27*z[47] + n<T>(5,2)*z[24];
    z[24]=z[3]*z[24];
    z[45]=z[45]*z[28];
    z[24]= - n<T>(27,4)*z[45] + z[24];
    z[24]=z[1]*z[24];
    z[24]=n<T>(1,2)*z[24] + z[26] + n<T>(1,4)*z[30] + z[39];
    z[24]=z[1]*z[24];
    z[26]=25*z[13];
    z[29]=z[29] - z[26];
    z[29]=z[13]*z[29];
    z[30]=2*z[4];
    z[39]=z[12]*z[27];
    z[39]=3*z[8] - n<T>(37,4)*z[39];
    z[39]=n<T>(1,2)*z[39] - z[30];
    z[39]=z[6]*z[39];
    z[18]= - n<T>(27,8)*z[22] + z[39] + n<T>(1,8)*z[18] + n<T>(9,4)*z[31] + n<T>(1,4)*
    z[27] + z[29];
    z[18]=z[5]*z[18];
    z[22]=z[9]*z[32];
    z[22]= - 1319*z[11] - n<T>(271,2)*z[22];
    z[22]=z[22]*z[43];
    z[22]=z[22] - 89*z[32] - n<T>(49,2)*z[42];
    z[29]=npow(z[9],2);
    z[31]=z[29]*z[4];
    z[39]=z[9] + z[31];
    z[39]=z[6]*z[39];
    z[39]=305*z[16] + 597*z[39] + static_cast<T>(305)+ n<T>(597,2)*z[44];
    z[45]=n<T>(1,4)*z[10];
    z[39]=z[39]*z[45];
    z[47]= - static_cast<T>(125)- n<T>(1791,4)*z[44];
    z[47]=z[6]*z[47];
    z[39]=z[39] - n<T>(597,8)*z[4] + z[47];
    z[39]=z[10]*z[39];
    z[47]= - 493*z[11] + n<T>(3575,4)*z[4];
    z[47]=z[6]*z[47];
    z[48]=z[12]*z[42];
    z[48]=115*z[13] + n<T>(29,2)*z[48];
    z[48]=z[5]*z[48];
    z[22]=z[39] + n<T>(5,2)*z[48] + n<T>(1,2)*z[22] + z[47];
    z[22]=z[10]*z[22];
    z[39]=25*z[32] - 7*z[27];
    z[47]= - n<T>(1197,2)*z[11] + 5*z[8];
    z[47]=n<T>(1,2)*z[47] + z[30];
    z[47]=z[4]*z[47];
    z[39]=n<T>(3,8)*z[39] + z[47];
    z[39]=z[6]*z[39];
    z[47]= - static_cast<T>(2)+ n<T>(9,4)*z[21];
    z[27]=z[47]*z[27];
    z[27]=n<T>(141,8)*z[32] + z[27];
    z[27]=z[4]*z[27];
    z[17]=z[24] + z[17] + z[22] + z[18] + z[39] + z[27] + n<T>(93,2)*z[37]
    + 35*z[34] + n<T>(9,4)*z[33];
    z[17]=z[1]*z[17];
    z[18]= - 13*z[8] - 45*z[13];
    z[18]=z[13]*z[18];
    z[22]=z[9]*z[38];
    z[18]=z[18] + z[22];
    z[18]=z[12]*z[18];
    z[22]= - z[43]*z[21];
    z[15]= - static_cast<T>(9)- z[15];
    z[15]=z[15]*z[35];
    z[24]= - n<T>(3,2)*z[8] + 365*z[13];
    z[27]=z[9]*z[42];
    z[15]=z[15] + z[22] + z[18] + 3*z[24] - 49*z[27];
    z[18]=z[12] - z[9];
    z[22]=z[18]*z[12];
    z[24]=z[5]*npow(z[12],3);
    z[27]=z[22] + z[24];
    z[25]= - z[27]*z[25];
    z[25]=z[25] - n<T>(305,2)*z[19] + n<T>(211,2)*z[9] - 305*z[12];
    z[25]=z[25]*z[36];
    z[33]=z[9]*z[13];
    z[25]=z[25] - n<T>(1367,2)*z[16] - static_cast<T>(598)- n<T>(699,4)*z[33];
    z[25]=z[10]*z[25];
    z[33]=z[18] + z[19];
    z[34]= - z[10]*z[33];
    z[27]= - z[10]*z[27];
    z[27]=z[19] + z[27];
    z[27]=z[3]*z[27];
    z[27]=z[27] - static_cast<T>(1)+ z[34];
    z[27]=z[3]*z[27];
    z[34]=z[12]*z[13];
    z[35]= - n<T>(13,4)*z[23] + n<T>(617,4) + 226*z[34];
    z[35]=z[5]*z[35];
    z[15]=3*z[27] + z[25] + n<T>(1,4)*z[15] + z[35];
    z[15]=z[3]*z[15];
    z[25]=z[12]*z[11];
    z[25]= - n<T>(8377,4)*z[44] + n<T>(535,4) - 561*z[25];
    z[25]=z[6]*z[25];
    z[27]=z[9]*z[11];
    z[27]= - static_cast<T>(839)+ n<T>(2299,2)*z[27];
    z[27]=z[4]*z[27];
    z[27]=z[27] - n<T>(863,2)*z[11] + 147*z[13];
    z[35]=static_cast<T>(1)+ z[34];
    z[35]=z[5]*z[35];
    z[25]=n<T>(667,2)*z[35] + n<T>(1,2)*z[27] + z[25];
    z[27]=npow(z[9],3);
    z[35]= - z[4]*z[27];
    z[35]= - z[29] + z[35];
    z[35]=z[6]*z[35];
    z[36]=n<T>(597,2)*z[31];
    z[33]=n<T>(597,2)*z[35] - z[36] + 305*z[33];
    z[33]=z[33]*z[45];
    z[35]=static_cast<T>(305)+ 597*z[44];
    z[36]=125*z[9] + z[36];
    z[36]=z[6]*z[36];
    z[33]=z[33] + n<T>(1,4)*z[35] + z[36];
    z[33]=z[10]*z[33];
    z[25]=n<T>(1,2)*z[25] + z[33];
    z[25]=z[10]*z[25];
    z[33]=z[7] - z[9];
    z[30]=z[33]*z[30];
    z[33]=z[11]*z[7];
    z[21]=z[30] - n<T>(1,8)*z[21] + n<T>(3593,4) - 223*z[33];
    z[21]=z[4]*z[21];
    z[30]=n<T>(45,4) - 7*z[33];
    z[30]=z[12]*z[30]*z[32];
    z[33]= - static_cast<T>(2787)- z[33];
    z[33]=z[11]*z[33];
    z[33]=z[33] - 9*z[8];
    z[30]=n<T>(1,4)*z[33] + 5*z[30];
    z[21]=n<T>(1,2)*z[30] + z[21];
    z[21]=z[6]*z[21];
    z[30]=z[13]*z[14];
    z[30]= - static_cast<T>(14)- n<T>(93,4)*z[30];
    z[30]=z[30]*z[42];
    z[23]=z[34] + z[23];
    z[23]= - n<T>(13,2)*z[23];
    z[23]=z[8]*z[23];
    z[23]=z[26] + z[23];
    z[23]=z[23]*z[46];
    z[26]= - z[9]*z[40];
    z[33]=z[4]*z[11];
    z[15]=z[17] + z[15] + z[25] + z[23] + z[21] - n<T>(2009,4)*z[33] + z[41]
    + z[26] - n<T>(131,8)*z[32] + z[30];
    z[15]=z[1]*z[15];
    z[17]=303*z[6];
    z[21]=n<T>(1,2)*z[9] + z[31];
    z[21]=z[21]*z[17];
    z[21]=175*z[16] + z[21] + static_cast<T>(175)+ n<T>(503,4)*z[44];
    z[21]=z[10]*z[21];
    z[23]=z[7] - 3*z[9];
    z[23]=z[4]*z[23];
    z[23]= - n<T>(1,2) + z[23];
    z[17]=z[23]*z[17];
    z[17]= - n<T>(503,2)*z[4] + z[17];
    z[20]=z[9] - 2*z[20];
    z[20]=z[10]*z[20];
    z[16]=z[16] + z[20];
    z[16]= - static_cast<T>(1)+ 2*z[16];
    z[16]=z[3]*z[16];
    z[15]=z[15] + 75*z[16] + n<T>(1,2)*z[17] + z[21];
    z[15]=z[1]*z[15];
    z[16]= - z[9] + n<T>(1,2)*z[7];
    z[16]=n<T>(101,2)*z[16];
    z[17]= - z[9]*z[28]*z[16];
    z[20]= - z[6]*z[29];
    z[20]= - z[31] + z[20];
    z[20]=n<T>(101,4)*z[20] + 25*z[19];
    z[20]=z[10]*z[20];
    z[21]= - z[10]*z[22];
    z[19]=z[19] + z[21];
    z[21]=25*z[3];
    z[19]=z[19]*z[21];
    z[17]=z[19] + z[17] + z[20];
    z[17]=z[17]*npow(z[1],2);
    z[19]=z[27]*z[28];
    z[20]= - z[24]*z[21];
    z[19]= - n<T>(101,4)*z[19] + z[20];
    z[19]=z[2]*npow(z[1],3)*z[10]*z[19];
    z[17]=z[17] + z[19];
    z[19]=npow(z[2],2);
    z[17]=z[17]*z[19];
    z[16]= - z[4]*z[16];
    z[20]=z[6]*z[9];
    z[18]=z[10]*z[18];
    z[16]=z[17] + 25*z[18] + n<T>(101,4)*z[20] + static_cast<T>(25)+ z[16];
    z[15]=z[15] + 3*z[16];

    r += z[19]*z[15];
 
    return r;
}

template double qg_2lNLC_r835(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r835(const std::array<dd_real,31>&);
#endif
