#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1067(const std::array<T,31>& k) {
  T z[134];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[14];
    z[6]=k[19];
    z[7]=k[11];
    z[8]=k[12];
    z[9]=k[13];
    z[10]=k[8];
    z[11]=k[9];
    z[12]=k[15];
    z[13]=k[4];
    z[14]=k[5];
    z[15]=k[26];
    z[16]=k[2];
    z[17]=k[3];
    z[18]=k[29];
    z[19]=npow(z[1],2);
    z[20]=npow(z[2],2);
    z[21]=z[20]*npow(z[19],2);
    z[22]=z[21]*z[5];
    z[23]=n<T>(1,2)*z[22];
    z[24]=z[21]*z[3];
    z[25]=n<T>(1,2)*z[5];
    z[26]=z[20]*npow(z[1],3);
    z[27]=z[26]*z[14];
    z[28]=z[27]*z[25];
    z[29]=3*z[26];
    z[28]=n<T>(1,2)*z[24] + z[28] + z[29] + z[23];
    z[28]=z[3]*z[28];
    z[19]=z[19]*z[20];
    z[30]=z[19]*z[14];
    z[31]=npow(z[5],2);
    z[32]=z[30]*z[31];
    z[33]=10*z[19];
    z[34]=z[26]*z[5];
    z[35]= - z[33] + 9*z[34];
    z[35]=z[5]*z[35];
    z[35]=z[35] + 6*z[32];
    z[35]=z[14]*z[35];
    z[36]=npow(z[14],2);
    z[37]=2*z[11];
    z[38]= - z[36]*z[37];
    z[39]=2*z[14];
    z[38]=z[38] - z[39];
    z[40]=z[21]*z[8];
    z[38]=z[40]*z[38];
    z[41]=z[21]*z[14];
    z[42]=z[41]*z[3];
    z[43]=3*z[27];
    z[38]= - z[43] - z[42] + z[38];
    z[38]=z[11]*z[38];
    z[44]=z[3]*z[27];
    z[38]=z[38] + z[44] + z[40];
    z[38]=z[11]*z[38];
    z[44]=z[27]*z[5];
    z[45]= - z[22] - z[44];
    z[46]=n<T>(1,2)*z[8];
    z[45]=z[45]*z[46];
    z[47]=z[27] + z[21];
    z[48]=z[47]*z[10];
    z[49]= - z[29] - n<T>(5,2)*z[48];
    z[49]=z[10]*z[49];
    z[28]=z[49] + z[38] + z[45] + z[28] + z[35] + n<T>(19,2)*z[19] - 8*z[34]
   ;
    z[28]=z[12]*z[28];
    z[35]=z[21]*z[9];
    z[38]=z[35]*z[14];
    z[45]=z[27] - z[21];
    z[49]=z[38] - z[45];
    z[50]=2*z[9];
    z[49]=z[49]*z[50];
    z[51]=2*z[20];
    z[52]=npow(z[1],5);
    z[53]=z[51]*z[52];
    z[54]=2*z[21];
    z[55]= - z[54] + z[27];
    z[55]=z[14]*z[55];
    z[55]=z[53] + z[55];
    z[55]=z[8]*z[55];
    z[56]=z[41]*z[50];
    z[55]=z[55] + z[27] + z[56];
    z[55]=z[55]*z[37];
    z[43]= - z[21] + z[43];
    z[57]=2*z[8];
    z[43]=z[43]*z[57];
    z[43]=z[55] + z[43] + z[26] + z[49];
    z[43]=z[11]*z[43];
    z[49]=n<T>(4,3)*z[22];
    z[55]=n<T>(2,3)*z[30];
    z[58]=z[55] - z[26] - z[49];
    z[59]=2*z[3];
    z[58]=z[58]*z[59];
    z[60]=z[27]*z[9];
    z[61]= - z[26] - z[60];
    z[61]=z[61]*z[50];
    z[62]=2*z[5];
    z[63]=z[21]*z[62];
    z[64]=z[63] + z[26];
    z[57]=z[64]*z[57];
    z[65]=n<T>(1,3)*z[34];
    z[43]=z[43] + z[57] + z[58] + z[61] - z[19] + z[65];
    z[43]=z[11]*z[43];
    z[57]=2*z[19];
    z[58]=z[31]*z[21];
    z[61]=7*z[26];
    z[66]= - z[31]*z[61];
    z[67]=z[20]*z[1];
    z[66]=z[67] + z[66];
    z[68]=n<T>(1,2)*z[14];
    z[66]=z[66]*z[68];
    z[69]=3*z[30];
    z[70]=z[26] + z[69];
    z[70]=z[70]*z[46];
    z[58]=z[70] + z[66] - z[57] - n<T>(7,2)*z[58];
    z[58]=z[8]*z[58];
    z[66]=z[22] - z[26];
    z[70]=z[66]*z[5];
    z[70]=z[70] + z[19];
    z[71]=z[70]*z[50];
    z[72]=npow(z[5],3);
    z[73]=z[72]*z[21];
    z[71]=z[71] + z[67] - 4*z[73];
    z[71]=z[14]*z[71];
    z[70]=2*z[70] + z[71];
    z[70]=z[9]*z[70];
    z[71]= - n<T>(8,3)*z[26] + z[23];
    z[71]=z[5]*z[71];
    z[73]= - n<T>(8,3)*z[19] + n<T>(1,2)*z[34];
    z[74]=z[14]*z[5];
    z[73]=z[73]*z[74];
    z[75]=z[26]*z[3];
    z[71]= - n<T>(3,2)*z[75] + z[73] - n<T>(19,6)*z[19] + z[71];
    z[71]=z[3]*z[71];
    z[73]=z[30]*npow(z[5],4);
    z[76]=11*z[73];
    z[77]=4*z[19];
    z[78]=11*z[34];
    z[79]=z[77] - z[78];
    z[79]=z[79]*z[72];
    z[79]=z[79] - z[76];
    z[79]=z[14]*z[79];
    z[80]=5*z[19];
    z[81]=z[80] + n<T>(67,3)*z[34];
    z[81]=z[81]*z[31];
    z[79]=z[81] + n<T>(4,3)*z[79];
    z[79]=z[14]*z[79];
    z[81]=z[22] + z[26];
    z[82]=z[81]*z[5];
    z[83]=5*z[26];
    z[84]=z[10]*z[83];
    z[84]=z[84] + z[19] - 3*z[82];
    z[84]=z[10]*z[84];
    z[85]= - 11*z[19] - 17*z[34];
    z[86]=n<T>(1,3)*z[5];
    z[85]=z[85]*z[86];
    z[87]=n<T>(3,2)*z[67];
    z[28]=z[28] + z[84] + z[43] + z[58] + z[71] + z[70] + z[79] + z[87]
    + z[85];
    z[28]=z[12]*z[28];
    z[43]=z[63] - z[29];
    z[58]=z[5]*z[43];
    z[58]=z[57] + z[58];
    z[58]=z[58]*z[62];
    z[70]=2*z[26];
    z[71]=z[70] - z[22];
    z[79]=z[71]*z[5];
    z[79]=z[79] - z[19];
    z[84]=z[79]*z[50];
    z[58]=z[84] - z[67] + z[58];
    z[58]=z[9]*z[58];
    z[84]=z[20]*z[5];
    z[85]=z[84]*z[52];
    z[88]= - 41*z[21] + 22*z[85];
    z[88]=z[5]*z[88];
    z[88]=25*z[26] + z[88];
    z[88]=z[88]*z[86];
    z[88]= - z[57] + z[88];
    z[88]=z[88]*z[62];
    z[89]=3*z[21];
    z[90]=z[89] - z[85];
    z[91]=z[5]*z[90];
    z[91]= - z[29] + z[91];
    z[91]=z[5]*z[91];
    z[91]=z[19] + z[91];
    z[91]=z[3]*z[91];
    z[92]=n<T>(1,2)*z[67];
    z[88]=z[91] + z[92] + z[88];
    z[88]=z[3]*z[88];
    z[91]=z[26]*z[31];
    z[93]=n<T>(3,2)*z[91];
    z[94]=z[66]*z[3];
    z[95]= - z[5]*z[94];
    z[96]=z[31]*z[40];
    z[95]= - n<T>(1,2)*z[96] - z[93] + z[95];
    z[95]=z[4]*z[95];
    z[96]=z[26]*z[62];
    z[97]=n<T>(1,2)*z[19];
    z[98]=z[97] - z[96];
    z[98]=z[5]*z[98];
    z[98]=z[92] + z[98];
    z[98]=z[5]*z[98];
    z[99]=z[40] + z[26];
    z[100]=npow(z[9],2);
    z[101]= - z[100]*z[99];
    z[101]= - n<T>(1,2)*z[91] + z[101];
    z[101]=z[8]*z[101];
    z[58]=z[95] + z[101] + z[88] + z[98] + z[58];
    z[58]=z[4]*z[58];
    z[88]=npow(z[8],2);
    z[95]=npow(z[10],2);
    z[98]=z[88] + z[95];
    z[98]=z[4]*z[21]*z[98];
    z[101]=z[21]*z[10];
    z[102]=z[101] + z[40];
    z[102]=z[102]*z[4];
    z[103]=z[26]*z[8];
    z[104]=z[26]*z[10];
    z[105]=z[102] + z[103] + z[104];
    z[105]=z[15]*z[105];
    z[106]=z[95]*z[26];
    z[88]=z[88]*z[26];
    z[88]=2*z[105] + z[98] + z[88] + z[106];
    z[88]=z[88]*npow(z[15],2);
    z[98]=z[14]*z[67];
    z[105]= - z[57] - z[98];
    z[105]=z[14]*z[105];
    z[105]= - 2*z[66] + z[105];
    z[105]=z[9]*z[105];
    z[79]=z[105] - z[79];
    z[79]=z[79]*z[50];
    z[105]=z[77] - z[34];
    z[105]=z[105]*z[5];
    z[107]=z[105] + z[67];
    z[79]=z[79] - z[107];
    z[79]=z[9]*z[79];
    z[108]=n<T>(3,2)*z[19];
    z[109]= - z[70] + z[23];
    z[109]=z[5]*z[109];
    z[109]=z[108] + z[109];
    z[109]=z[3]*z[109];
    z[107]=z[109] + z[107];
    z[107]=z[3]*z[107];
    z[109]=z[19]*z[5];
    z[109]=z[109] + z[67];
    z[109]=z[109]*z[25];
    z[79]=z[107] - z[109] + z[79];
    z[79]=z[10]*z[79];
    z[107]=z[21] - z[85];
    z[107]=z[107]*z[62];
    z[107]= - z[26] + z[107];
    z[110]=n<T>(1,3)*z[4];
    z[107]=z[107]*z[110];
    z[111]=n<T>(1,3)*z[19];
    z[112]=n<T>(2,3)*z[26];
    z[113]=z[112] - z[22];
    z[113]=z[113]*z[62];
    z[107]=z[107] - z[111] + z[113];
    z[107]=z[31]*z[107];
    z[113]=z[111] - z[34];
    z[113]=z[113]*z[72];
    z[113]=z[113] - n<T>(1,3)*z[73];
    z[114]=z[113]*z[39];
    z[107]=z[114] + z[107];
    z[107]=z[7]*z[107];
    z[114]=6*z[26];
    z[115]=npow(z[9],3);
    z[116]= - z[115]*z[114];
    z[117]=z[21]*z[50];
    z[118]=z[117] + z[26];
    z[119]= - z[8]*z[118]*z[100];
    z[116]=z[116] + z[119];
    z[116]=z[8]*z[116];
    z[119]=npow(z[3],2)*z[108];
    z[120]= - n<T>(7,2)*z[19] - z[75];
    z[121]=z[12]*z[3];
    z[120]=z[120]*z[121];
    z[119]=z[119] + z[120];
    z[119]=z[12]*z[119];
    z[120]=npow(z[4],2);
    z[122]= - z[31]*z[120]*z[94];
    z[121]=z[97]*npow(z[121],2);
    z[121]=z[121] + z[122];
    z[121]=z[13]*z[121];
    z[122]=z[19] + z[98];
    z[123]=4*z[9];
    z[122]=z[122]*z[123];
    z[122]=z[67] + z[122];
    z[122]=z[122]*z[100];
    z[58]=z[121] + 4*z[88] + 22*z[107] + z[58] + z[119] + z[79] + z[122]
    + z[116];
    z[58]=z[13]*z[58];
    z[79]=z[37]*z[21];
    z[88]=z[35] + z[26];
    z[107]= - z[79] - z[88];
    z[116]=4*z[11];
    z[107]=z[107]*z[116];
    z[119]= - z[88]*z[50];
    z[65]=z[107] + n<T>(4,3)*z[94] + z[65] + z[119];
    z[65]=z[11]*z[65];
    z[53]=z[11]*z[53];
    z[53]=z[21] + z[53];
    z[53]=z[53]*z[37];
    z[53]=6*z[101] + z[53] + z[26] + 4*z[35];
    z[53]=z[11]*z[53];
    z[53]=z[19] + z[53];
    z[53]=z[10]*z[53];
    z[94]=z[66]*z[62];
    z[94]=z[94] + z[19];
    z[43]=z[9]*z[43];
    z[43]=z[43] - z[94];
    z[43]=z[9]*z[43];
    z[107]=npow(z[1],6);
    z[119]=z[107]*z[84];
    z[121]=z[52]*z[20];
    z[122]= - 11*z[119] + 29*z[121];
    z[124]=z[5]*z[122];
    z[124]=115*z[21] - 4*z[124];
    z[124]=z[5]*z[124];
    z[124]= - 40*z[26] + z[124];
    z[124]=z[124]*z[86];
    z[124]= - z[97] + z[124];
    z[124]=z[3]*z[124];
    z[99]= - 3*z[35] - z[99];
    z[99]=z[9]*z[99];
    z[125]=3*z[22];
    z[126]=z[125] + z[26];
    z[127]= - z[126]*z[25];
    z[99]=z[127] + z[99];
    z[99]=z[8]*z[99];
    z[127]=z[97] - n<T>(4,3)*z[34];
    z[127]=z[5]*z[127];
    z[128]= - n<T>(3,2)*z[8] - z[3];
    z[128]=z[22]*z[128];
    z[128]= - n<T>(5,2)*z[34] + z[128];
    z[128]=z[4]*z[128];
    z[43]=z[128] + z[53] + z[65] + z[99] + z[124] + z[43] + z[92] + 
    z[127];
    z[43]=z[4]*z[43];
    z[53]=4*z[22];
    z[65]= - z[53] + n<T>(5,2)*z[26];
    z[65]=z[65]*z[8];
    z[99]=z[53] - z[26];
    z[124]=z[99]*z[3];
    z[65]=z[65] + z[124] + z[108] - z[104] + z[34];
    z[124]=9*z[26] + z[24];
    z[124]=n<T>(1,2)*z[124] + z[40];
    z[124]=z[12]*z[124];
    z[124]=z[124] - z[65];
    z[124]=z[12]*z[124];
    z[127]=n<T>(3,2)*z[40] + n<T>(7,2)*z[26] + z[24];
    z[127]=z[4]*z[127];
    z[65]=z[127] - z[65];
    z[65]=z[4]*z[65];
    z[24]=n<T>(3,2)*z[24] + n<T>(5,2)*z[40] + 8*z[26];
    z[127]=z[4] + z[12];
    z[24]=z[24]*z[127];
    z[128]=z[67]*z[12];
    z[129]=z[67]*z[4];
    z[128]=z[128] + z[129];
    z[128]=z[128]*z[13];
    z[130]=z[29]*z[3];
    z[130]=z[130] + z[19];
    z[130]= - z[130]*z[127];
    z[130]= - z[128] + z[130];
    z[130]=z[13]*z[130];
    z[131]=z[104] + z[19];
    z[95]=z[95]*z[83];
    z[132]=z[95] + z[67];
    z[133]=n<T>(1,2)*z[16];
    z[132]= - z[133]*z[132];
    z[131]=z[132] + n<T>(1,2)*z[131];
    z[127]=z[127]*z[131];
    z[127]=z[128] + z[127];
    z[127]=z[16]*z[127];
    z[24]=z[127] + n<T>(1,2)*z[130] + z[24];
    z[24]=z[18]*z[24];
    z[127]= - z[12]*z[75];
    z[127]= - 3*z[67] + z[127];
    z[127]=z[12]*z[127];
    z[75]= - z[97] - z[75];
    z[75]=z[4]*z[75];
    z[75]= - z[87] + z[75];
    z[75]=z[4]*z[75];
    z[87]= - z[92]*z[120]*z[13];
    z[75]=z[87] + n<T>(1,2)*z[127] + z[75];
    z[75]=z[13]*z[75];
    z[87]=z[106] - z[67];
    z[87]=3*z[87];
    z[92]=z[19]*z[4];
    z[97]= - z[87] + z[92];
    z[97]=z[4]*z[97];
    z[106]=z[12]*z[104];
    z[87]= - z[87] + z[106];
    z[87]=z[12]*z[87];
    z[87]=z[87] + z[97];
    z[97]=z[120]*z[67];
    z[95]= - npow(z[12],2)*z[95];
    z[95]=z[95] - z[97];
    z[95]=z[95]*z[133];
    z[97]=z[13]*z[97];
    z[87]=z[95] + n<T>(1,2)*z[87] + z[97];
    z[87]=z[16]*z[87];
    z[24]=z[24] + z[87] + z[75] + z[124] + z[65];
    z[24]=z[18]*z[24];
    z[65]= - z[44] + z[71];
    z[65]=z[14]*z[65];
    z[65]= - z[21] + z[65];
    z[65]=z[8]*z[65];
    z[44]=z[65] - z[60] - z[44] - z[66];
    z[44]=z[44]*z[37];
    z[60]=z[31] - z[100];
    z[42]=z[60]*z[42];
    z[60]=z[27]*z[31];
    z[65]= - z[29] - z[22];
    z[65]=z[5]*z[65];
    z[65]=z[65] - z[60];
    z[65]=z[14]*z[65];
    z[65]=z[65] + z[70] - z[125];
    z[65]=z[8]*z[65];
    z[75]= - z[70] - z[22];
    z[75]=z[5]*z[75];
    z[87]= - z[9]*z[45];
    z[87]= - z[26] + z[87];
    z[87]=z[9]*z[87];
    z[42]=z[44] + z[65] + z[42] + z[87] + z[75] + z[60];
    z[42]=z[42]*z[37];
    z[44]= - z[56] + z[47];
    z[44]=z[9]*z[44];
    z[44]=z[26] + z[44];
    z[44]=z[9]*z[44];
    z[56]=z[5]*z[64];
    z[64]=z[63] - z[26];
    z[65]=z[14]*z[64]*z[31];
    z[44]=z[44] + z[56] + z[65];
    z[44]=z[44]*z[59];
    z[56]= - z[82] - z[60];
    z[60]=23*z[26];
    z[65]=z[60] + 8*z[22];
    z[75]=n<T>(1,3)*z[9];
    z[65]=z[65]*z[75];
    z[75]= - z[70] + z[35];
    z[75]=z[3]*z[75];
    z[56]=n<T>(4,3)*z[75] + 5*z[56] - z[65];
    z[56]=z[8]*z[56];
    z[75]= - z[19] + 4*z[34];
    z[75]=z[75]*z[31]*z[39];
    z[87]=z[9]*z[118];
    z[87]=z[19] + z[87];
    z[87]=z[87]*z[50];
    z[42]=z[42] + z[56] + z[44] + z[87] + z[105] + z[75];
    z[42]=z[11]*z[42];
    z[44]=z[30] - z[26];
    z[56]=z[44]*z[14];
    z[75]=z[21] + z[56];
    z[75]=z[9]*z[75];
    z[44]=z[75] + z[44];
    z[44]=z[44]*z[50];
    z[75]=z[121]*z[9];
    z[87]= - z[21] + z[75];
    z[87]=z[87]*z[50];
    z[49]=z[87] + z[26] - z[49];
    z[49]=z[8]*z[49];
    z[44]=z[49] + z[19] + z[44];
    z[44]=z[9]*z[44];
    z[49]=n<T>(11,3)*z[85];
    z[87]=9*z[21] - z[49];
    z[87]=z[87]*z[62];
    z[87]= - n<T>(25,3)*z[26] + z[87];
    z[87]=z[87]*z[62];
    z[95]=n<T>(7,3)*z[19];
    z[87]=z[95] + z[87];
    z[87]=z[5]*z[87];
    z[78]= - z[57] + z[78];
    z[78]=z[78]*z[72];
    z[78]=2*z[78] + z[76];
    z[78]=z[14]*z[78];
    z[97]=14*z[34];
    z[105]= - z[19] + z[97];
    z[105]=z[105]*z[31];
    z[78]=z[105] + n<T>(2,3)*z[78];
    z[78]=z[14]*z[78];
    z[105]=z[121]*z[11];
    z[106]=z[8]*z[105];
    z[106]= - z[40] + z[106];
    z[106]=z[106]*z[37];
    z[118]=z[61] + z[53];
    z[120]=z[8]*z[118];
    z[106]=n<T>(1,3)*z[120] + z[106];
    z[106]=z[11]*z[106];
    z[44]=z[106] + z[87] + z[78] + z[44];
    z[78]= - 10*z[30] - z[99];
    z[87]=z[30] + z[26];
    z[106]=z[36]*z[50];
    z[120]= - z[87]*z[106];
    z[124]= - z[70] - z[69];
    z[124]=z[14]*z[124];
    z[120]=z[124] + z[120];
    z[120]=z[9]*z[120];
    z[78]=n<T>(1,3)*z[78] + z[120];
    z[78]=z[78]*z[50];
    z[105]=z[105] - z[21];
    z[105]=z[105]*z[37];
    z[53]=z[83] - z[53];
    z[120]=2*z[30];
    z[124]=z[120] - z[53];
    z[124]=n<T>(1,3)*z[124] - z[105];
    z[124]=z[124]*z[37];
    z[78]=z[124] - z[95] + z[78];
    z[78]=z[10]*z[78];
    z[95]=19*z[26];
    z[124]= - z[95] + 11*z[22];
    z[124]=z[5]*z[124];
    z[33]=z[33] + z[124];
    z[33]=z[33]*z[31];
    z[80]= - z[80] + n<T>(22,3)*z[34];
    z[80]=z[80]*z[72];
    z[73]=z[80] + n<T>(11,3)*z[73];
    z[73]=z[14]*z[73];
    z[33]=n<T>(1,3)*z[33] + z[73];
    z[33]=z[33]*z[39];
    z[73]=z[53]*z[62];
    z[73]= - z[19] + z[73];
    z[73]=z[73]*z[86];
    z[33]=z[73] + z[33];
    z[33]=z[33]*z[39];
    z[73]=z[107]*z[20];
    z[80]= - z[8]*z[73];
    z[80]= - z[121] + z[80];
    z[80]=z[80]*z[37];
    z[20]=3*z[20];
    z[20]=z[20]*z[52];
    z[52]=z[8]*z[20];
    z[52]=z[80] + z[54] + z[52];
    z[52]=z[11]*z[52];
    z[80]= - z[120] - z[118];
    z[52]=z[52] + n<T>(1,3)*z[80] - 2*z[40];
    z[52]=z[52]*z[37];
    z[80]=z[61] + z[22];
    z[80]=z[5]*z[80];
    z[33]=z[52] + z[103] + z[33] - z[19] + n<T>(2,3)*z[80];
    z[33]=z[12]*z[33];
    z[52]=z[122]*z[62];
    z[52]= - 47*z[21] + z[52];
    z[52]=z[52]*z[86];
    z[52]=z[114] + z[52];
    z[52]=z[5]*z[52];
    z[53]=n<T>(1,3)*z[53] + z[105];
    z[53]=z[11]*z[53];
    z[52]=z[52] + z[53];
    z[51]= - z[107]*z[51]*z[11];
    z[51]=z[20] + z[51];
    z[51]=z[11]*z[51];
    z[51]= - z[54] + z[51];
    z[51]=z[51]*z[37];
    z[51]=z[26] + z[51];
    z[51]=z[10]*z[51];
    z[51]=2*z[52] + z[51];
    z[51]=z[4]*z[51];
    z[33]=z[51] + z[33] + 2*z[44] + z[78];
    z[33]=z[7]*z[33];
    z[44]= - z[26] + n<T>(10,3)*z[30];
    z[44]=z[44]*z[14];
    z[44]=z[44] - n<T>(13,3)*z[21];
    z[44]=z[44]*z[3];
    z[51]=3*z[98];
    z[52]= - z[51] + n<T>(19,3)*z[19];
    z[52]=z[52]*z[68];
    z[53]=n<T>(23,3)*z[26];
    z[44]=z[44] - z[52] - z[53];
    z[56]=z[56] - z[54];
    z[78]=z[3]*z[14];
    z[80]=z[78] - 1;
    z[105]=n<T>(4,3)*z[8];
    z[107]=z[80]*z[105];
    z[56]= - z[56]*z[107];
    z[56]=z[56] - z[44];
    z[56]=z[8]*z[56];
    z[114]=z[8]*z[47];
    z[114]=z[114] + z[48];
    z[118]=z[21] + n<T>(1,2)*z[27];
    z[118]=z[118]*z[14];
    z[118]=z[118] + n<T>(1,2)*z[121];
    z[122]=z[10] + z[8];
    z[124]= - z[12]*z[118]*z[122];
    z[114]=n<T>(3,2)*z[114] + z[124];
    z[114]=z[12]*z[114];
    z[124]=z[80]*z[48];
    z[44]=4*z[124] - z[44];
    z[44]=z[10]*z[44];
    z[124]=4*z[26];
    z[125]=z[124] - z[30];
    z[125]=z[125]*z[14];
    z[125]=z[125] + 5*z[21];
    z[80]=z[15]*z[122]*z[125]*z[80];
    z[44]=n<T>(4,3)*z[80] + 11*z[102] + 5*z[114] + z[56] + z[44];
    z[44]=z[15]*z[44];
    z[56]= - z[26] + n<T>(8,3)*z[35] + n<T>(16,3)*z[30];
    z[56]=z[56]*z[3];
    z[80]=4*z[30];
    z[102]=z[80] + z[26];
    z[114]=n<T>(2,3)*z[9];
    z[102]=z[102]*z[114];
    z[114]=n<T>(2,3)*z[19];
    z[122]=n<T>(1,2)*z[98];
    z[125]=z[122] - z[114];
    z[56]=z[56] - z[102] + 5*z[125];
    z[102]= - z[70] - n<T>(5,3)*z[30];
    z[102]=z[14]*z[102];
    z[102]=n<T>(5,3)*z[21] + z[102];
    z[102]=z[102]*z[59];
    z[125]=z[70] + z[30];
    z[127]=z[125]*z[14];
    z[127]=z[127] + z[21];
    z[107]= - z[127]*z[107];
    z[52]=z[107] + z[102] + n<T>(17,3)*z[26] + z[52];
    z[52]=z[8]*z[52];
    z[52]=z[52] - z[56];
    z[52]=z[8]*z[52];
    z[102]=z[21] + 5*z[27];
    z[102]=z[3]*z[102];
    z[102]=z[70] + z[102];
    z[102]=z[10]*z[102];
    z[56]=z[102] - z[56];
    z[56]=z[10]*z[56];
    z[102]=z[117] - z[26];
    z[40]=2*z[102] + z[40];
    z[40]=z[8]*z[40];
    z[102]=5*z[101] + z[102];
    z[107]=2*z[10];
    z[102]=z[102]*z[107];
    z[40]=z[40] + z[102];
    z[40]=z[4]*z[40];
    z[102]=z[10]*z[118];
    z[102]= - n<T>(1,2)*z[47] - 5*z[102];
    z[102]=z[10]*z[102];
    z[46]= - z[47]*z[46];
    z[46]=z[46] + z[102];
    z[46]=z[12]*z[46];
    z[47]= - z[26] + n<T>(15,2)*z[48];
    z[47]=z[10]*z[47];
    z[46]=z[46] - z[103] + z[47];
    z[46]=z[12]*z[46];
    z[40]=z[44] + z[40] + z[46] + z[52] + z[56];
    z[40]=z[15]*z[40];
    z[44]=z[19]*z[31];
    z[46]= - z[72]*z[69];
    z[46]=5*z[44] + z[46];
    z[39]=z[46]*z[39];
    z[46]=n<T>(5,2)*z[19];
    z[47]= - z[5]*z[126];
    z[47]= - n<T>(5,2)*z[104] + z[46] + z[47];
    z[47]=z[10]*z[47];
    z[48]=3*z[34];
    z[52]= - z[77] + z[48];
    z[52]=z[5]*z[52];
    z[39]=z[47] + z[52] + z[39];
    z[39]=z[12]*z[39];
    z[47]=z[72]*z[19];
    z[52]= - 14*z[47] + z[76];
    z[52]=z[14]*z[52];
    z[52]=13*z[44] + 4*z[52];
    z[56]= - z[31]*z[70];
    z[46]=z[10]*z[46];
    z[46]=z[56] + z[46];
    z[46]=z[10]*z[46];
    z[56]=npow(z[11],2);
    z[69]=z[56]*z[19];
    z[39]=z[39] + z[46] + n<T>(1,3)*z[52] + z[69];
    z[39]=z[12]*z[39];
    z[46]=z[101] - z[26];
    z[52]= - z[79] + z[46];
    z[52]=z[56]*z[52];
    z[69]=npow(z[11],3);
    z[46]= - z[16]*z[69]*z[46];
    z[46]=z[46] + z[52];
    z[46]=z[107]*z[46];
    z[52]=z[69]*z[124];
    z[46]= - z[109] + z[52] + z[46];
    z[46]=z[4]*z[46];
    z[52]= - z[19] - z[34];
    z[52]=z[5]*z[52];
    z[69]= - z[11]*z[96];
    z[32]=z[69] + z[52] + z[32];
    z[32]=z[11]*z[32];
    z[52]=z[72]*z[120];
    z[32]=z[32] - z[44] + z[52];
    z[32]=z[32]*z[37];
    z[44]=z[64]*z[37];
    z[44]=z[44] + z[94];
    z[44]=z[11]*z[44];
    z[52]= - z[31]*z[29];
    z[44]=z[52] + z[44];
    z[44]=z[11]*z[44];
    z[52]=z[21]*z[116];
    z[52]= - z[29] + z[52];
    z[52]=z[10]*z[52]*z[56];
    z[44]=z[52] + z[109] + z[44];
    z[44]=z[10]*z[44];
    z[32]=z[39] + z[44] - z[47] + z[32] + z[46];
    z[32]=z[16]*z[32];
    z[39]= - z[125]*z[50];
    z[39]= - z[19] + z[39];
    z[39]=z[39]*z[100];
    z[44]= - z[9]*z[30];
    z[46]=z[11]*z[124];
    z[44]=z[46] - z[19] + z[44];
    z[44]=z[11]*z[9]*z[44];
    z[46]=z[19]*z[8];
    z[47]=z[3]*z[46];
    z[39]=z[44] + z[39] + n<T>(2,3)*z[47];
    z[39]=z[11]*z[39];
    z[44]=7*z[19] + 2*z[98];
    z[44]=z[9]*z[44];
    z[44]=2*z[67] + z[44];
    z[44]=z[44]*z[100];
    z[39]=z[44] + z[39];
    z[44]=z[115]*z[57];
    z[47]=z[100]*z[19];
    z[52]=z[11]*z[47];
    z[44]=z[44] + z[52];
    z[44]=z[11]*z[44];
    z[52]=z[115]*z[67];
    z[44]= - z[52] + z[44];
    z[57]= - z[11]*z[50];
    z[57]= - z[100] + z[57];
    z[56]=z[56]*z[26]*z[57];
    z[57]= - z[100]*z[67];
    z[56]=z[57] + 2*z[56];
    z[56]=z[4]*z[56];
    z[44]=2*z[44] + z[56];
    z[44]=z[17]*z[44];
    z[46]=z[46] - z[92];
    z[56]= - z[7]*z[11];
    z[57]= - z[7] + z[3];
    z[57]=z[6]*z[57];
    z[56]=z[56] + z[57];
    z[46]=z[46]*z[56];
    z[56]= - z[3]*z[114];
    z[57]=z[9] + z[37];
    z[57]=z[11]*z[88]*z[57];
    z[56]=z[56] + z[57];
    z[56]=z[56]*z[37];
    z[47]= - z[47] + z[56];
    z[47]=z[4]*z[47];
    z[56]= - z[100]*z[129];
    z[52]= - 2*z[52] + z[56];
    z[52]=z[13]*z[52];
    z[39]=z[44] + z[52] + 2*z[39] + z[47] + n<T>(4,3)*z[46];
    z[39]=z[17]*z[39];
    z[44]= - z[21] - z[85];
    z[44]=z[5]*z[44];
    z[44]=z[26] + z[44];
    z[46]= - z[81]*z[62];
    z[46]= - z[19] + z[46];
    z[46]=z[14]*z[46];
    z[47]=z[41] + z[121];
    z[47]=z[62]*z[47];
    z[47]= - z[89] + z[47];
    z[47]=z[47]*z[50];
    z[44]=z[47] + 2*z[44] + z[46];
    z[44]=z[9]*z[44];
    z[46]= - z[67] + z[91];
    z[46]=z[14]*z[46];
    z[44]=z[44] + z[46] - 3*z[19] + z[82];
    z[44]=z[9]*z[44];
    z[46]=z[54] - z[75];
    z[46]=z[46]*z[123];
    z[47]= - z[60] + 8*z[30];
    z[46]=n<T>(1,3)*z[47] + z[46];
    z[46]=z[9]*z[46];
    z[34]=z[46] - z[122] + z[111] + z[34];
    z[34]=z[3]*z[34];
    z[46]=z[26] - z[35];
    z[36]=z[9]*z[36]*z[46];
    z[36]= - z[54] + z[36];
    z[36]=z[36]*z[50];
    z[46]=z[111] - z[51];
    z[46]=z[46]*z[68];
    z[36]=z[36] - z[112] + z[46];
    z[36]=z[3]*z[36];
    z[46]=z[87]*z[78]*z[105];
    z[38]= - z[27] + z[38];
    z[38]=z[38]*z[50];
    z[38]= - z[26] + z[38];
    z[38]=z[9]*z[38];
    z[36]=z[46] + z[38] + z[36];
    z[36]=z[8]*z[36];
    z[34]=z[36] + z[34] - z[93] + z[44];
    z[34]=z[8]*z[34];
    z[35]= - n<T>(4,3)*z[35] - z[26] - z[55];
    z[35]=z[35]*z[59];
    z[36]= - z[90]*z[62];
    z[38]=z[85]*z[116];
    z[36]=z[38] - z[26] + z[36];
    z[36]=z[11]*z[36];
    z[22]=z[26] - 5*z[22];
    z[22]=z[5]*z[22];
    z[22]=z[36] + z[35] - z[65] - z[19] + z[22];
    z[22]=z[11]*z[22];
    z[35]=z[19]*z[106];
    z[30]=z[35] + z[30] + z[66];
    z[30]=z[30]*z[50];
    z[35]=9*z[19];
    z[36]=z[71]*z[62];
    z[30]=z[30] + z[35] + z[36];
    z[30]=z[9]*z[30];
    z[36]=z[54] - n<T>(1,2)*z[85];
    z[36]=z[5]*z[36];
    z[36]= - n<T>(3,2)*z[26] + z[36];
    z[36]=z[3]*z[36];
    z[23]= - z[29] + z[23];
    z[23]=z[5]*z[23];
    z[23]=z[36] - n<T>(17,2)*z[19] + z[23];
    z[23]=z[3]*z[23];
    z[29]= - z[37] + z[3];
    z[29]=z[41]*z[29];
    z[29]=3*z[45] + z[29];
    z[29]=z[11]*z[29];
    z[36]= - z[3]*z[45];
    z[29]=z[29] + z[124] + z[36];
    z[29]=z[11]*z[29];
    z[29]=z[108] + z[29];
    z[29]=z[10]*z[29];
    z[36]= - z[19] + z[48];
    z[25]=z[36]*z[25];
    z[22]=z[29] + z[22] + z[23] + z[30] - z[67] + z[25];
    z[22]=z[10]*z[22];
    z[23]= - z[49] + 6*z[21];
    z[23]=z[23]*z[62];
    z[23]=z[23] - z[83];
    z[23]=z[23]*z[5];
    z[25]=n<T>(4,3)*z[19];
    z[23]=z[23] + z[25];
    z[23]=z[23]*z[74];
    z[29]= - 22*z[119] + 47*z[121];
    z[29]=z[29]*z[86];
    z[29]=z[29] - 11*z[21];
    z[29]=z[29]*z[62];
    z[29]=z[29] + z[53];
    z[29]=z[29]*z[5];
    z[30]=z[41] - z[121];
    z[30]=z[30]*z[50];
    z[27]=z[30] + z[89] - z[27];
    z[27]=z[27]*z[9];
    z[30]=z[80] - z[61];
    z[27]=z[27] + n<T>(1,3)*z[30];
    z[27]=z[27]*z[9];
    z[23]= - z[27] - z[25] + z[23] + z[29];
    z[25]=z[23]*z[59];
    z[27]=z[84]*npow(z[1],7);
    z[27]= - 11*z[27] + 29*z[73];
    z[27]=z[27]*z[5];
    z[27]=z[27] - 31*z[121];
    z[27]=z[27]*z[62];
    z[27]=z[27] + 37*z[21];
    z[27]=z[27]*z[62];
    z[27]=z[27] - z[95];
    z[27]=z[27]*z[110];
    z[29]=z[73]*z[50];
    z[20]=z[29] - z[20];
    z[20]=z[20]*z[9];
    z[20]=z[20] + n<T>(10,3)*z[21];
    z[20]=z[20]*z[50];
    z[20]=z[20] - n<T>(7,3)*z[26];
    z[20]=z[20]*z[8];
    z[20]=z[27] + z[20];
    z[23]= - 2*z[23] - z[20];
    z[23]=z[7]*z[23];
    z[20]=z[3]*z[20];
    z[20]=z[23] + z[25] + z[20];
    z[20]=z[6]*z[20];
    z[23]= - z[19] - z[96];
    z[25]= - z[35] - z[98];
    z[25]=z[14]*z[25];
    z[25]=z[25] - z[83] + z[63];
    z[25]=z[25]*z[50];
    z[23]=z[25] + 2*z[23] - z[51];
    z[23]=z[9]*z[23];
    z[25]= - z[99]*z[31];
    z[23]=z[23] - z[67] + z[25];
    z[23]=z[9]*z[23];
    z[21]= - 163*z[21] + 88*z[85];
    z[21]=z[21]*z[86];
    z[21]=n<T>(61,2)*z[26] + z[21];
    z[21]=z[5]*z[21];
    z[25]=z[54] - z[85];
    z[25]=z[5]*z[25];
    z[25]= - z[26] + z[25];
    z[25]=z[3]*z[25];
    z[21]=z[25] - 8*z[19] + z[21];
    z[21]=z[5]*z[21];
    z[25]=z[9]*z[19];
    z[21]=z[25] + z[67] + z[21];
    z[21]=z[3]*z[21];
    z[25]=z[14]*z[113];
    z[19]= - n<T>(13,3)*z[19] + z[97];
    z[19]=z[19]*z[31];

    r += z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + 44*z[25] + 
      z[28] + z[32] + z[33] + z[34] + z[39] + z[40] + z[42] + z[43] + 
      z[58];
 
    return r;
}

template double qg_2lNLC_r1067(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1067(const std::array<dd_real,31>&);
#endif
