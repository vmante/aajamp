#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1395(const std::array<T,31>& k) {
  T z[96];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[7];
    z[9]=k[5];
    z[10]=k[13];
    z[11]=k[23];
    z[12]=k[14];
    z[13]=k[2];
    z[14]=k[24];
    z[15]=k[9];
    z[16]=k[3];
    z[17]=k[18];
    z[18]=n<T>(5,3)*z[8];
    z[19]=2*z[17];
    z[20]=z[18] - z[19];
    z[21]=npow(z[15],2);
    z[22]=npow(z[7],2);
    z[23]=z[21] - z[22];
    z[24]=6*z[13];
    z[23]=z[23]*z[24];
    z[24]=15*z[4];
    z[25]=z[22]*z[6];
    z[26]=3*z[15];
    z[27]=3*z[5];
    z[23]=n<T>(11,3)*z[25] + z[23] - z[27] - z[26] + 2*z[20] + z[24];
    z[23]=z[2]*z[23];
    z[28]=z[22]*z[13];
    z[29]= - z[27]*z[28];
    z[30]=z[18] + z[19];
    z[31]=5*z[10];
    z[32]= - 2*z[30] + z[31];
    z[32]=z[32]*z[25];
    z[33]=npow(z[8],2);
    z[34]=n<T>(4,3)*z[33];
    z[35]=10*z[4] + z[10];
    z[35]=z[5]*z[35];
    z[23]=z[23] + z[32] + z[29] + z[34] + z[35];
    z[23]=z[2]*z[23];
    z[29]=z[10]*z[11];
    z[32]=4*z[21];
    z[29]=z[29] - z[32];
    z[29]=z[29]*z[10];
    z[35]=npow(z[4],2);
    z[36]=2*z[15];
    z[37]= - z[4]*z[36];
    z[37]= - z[35] + z[37];
    z[37]=z[15]*z[37];
    z[35]=z[11]*z[35];
    z[35]=z[29] + z[35] + z[37];
    z[35]=z[5]*z[35];
    z[37]=3*z[11];
    z[38]=6*z[10];
    z[39]=z[38] - z[37] + z[36];
    z[39]=z[10]*z[39];
    z[40]=2*z[12];
    z[41]=z[40] + z[11];
    z[42]=z[41] + 8*z[14];
    z[43]= - z[15] + z[42];
    z[43]=z[4]*z[43];
    z[44]=npow(z[14],2);
    z[45]=6*z[44];
    z[39]=z[39] + z[45] + z[43];
    z[39]=z[39]*z[22];
    z[43]=8*z[12];
    z[46]=z[43]*z[21];
    z[47]=z[46]*z[4];
    z[48]=4*z[4];
    z[49]=z[48] - z[10];
    z[49]=z[49]*z[2];
    z[50]=npow(z[10],2);
    z[49]=z[49] - z[50];
    z[51]= - z[5]*z[49];
    z[52]=z[10]*z[22];
    z[51]=z[52] + z[51];
    z[51]=z[2]*z[51];
    z[35]=z[51] + z[39] + z[47] + z[35];
    z[35]=z[9]*z[35];
    z[39]=4*z[8];
    z[41]= - z[36] + z[39] + z[41] + 4*z[14];
    z[41]=z[10]*z[41];
    z[51]=npow(z[12],2);
    z[52]=2*z[51];
    z[53]= - z[52] + 3*z[44];
    z[54]=n<T>(2,3)*z[33];
    z[55]=z[54] - z[53];
    z[56]= - 5*z[4] - z[26];
    z[56]=z[56]*z[36];
    z[57]=z[4]*z[11];
    z[41]=z[41] + z[56] + 2*z[55] - z[57];
    z[41]=z[5]*z[41];
    z[55]=2*z[10];
    z[56]=z[11] - 4*z[10];
    z[56]=z[56]*z[55];
    z[58]=5*z[12];
    z[59]=11*z[14] + z[58];
    z[59]=z[4]*z[59];
    z[60]=z[5]*z[8];
    z[56]= - n<T>(10,3)*z[60] + z[56] + z[45] + z[59];
    z[56]=z[56]*z[25];
    z[59]=z[40] + z[14];
    z[60]=3*z[4];
    z[61]=z[59]*z[60];
    z[62]=z[15]*z[16];
    z[63]=3*z[10];
    z[64]= - z[16]*z[63];
    z[64]=z[62] + z[64];
    z[65]=4*z[22];
    z[64]=z[64]*z[65];
    z[32]=z[64] + z[61] - z[32];
    z[32]=z[10]*z[32];
    z[61]=z[60]*z[44];
    z[64]=9*z[4];
    z[66]=4*z[15];
    z[67]=z[66] + z[64];
    z[67]=z[12]*z[67];
    z[67]= - 4*z[51] + z[67];
    z[67]=z[15]*z[67];
    z[67]=z[61] + z[67];
    z[68]=4*z[5];
    z[69]= - z[14]*z[68];
    z[69]=z[44] + z[69];
    z[69]=z[69]*z[28];
    z[23]=z[35] + z[23] + z[56] + 6*z[69] + z[41] + 2*z[67] + z[32];
    z[23]=z[9]*z[23];
    z[32]=npow(z[16],2);
    z[35]=z[32]*z[66];
    z[41]=npow(z[7],3);
    z[56]=z[41]*z[50];
    z[67]= - z[56]*z[35];
    z[69]= - z[51]*z[66];
    z[70]=9*z[44];
    z[71]= - z[70] - z[51];
    z[71]=z[5]*z[71];
    z[69]=z[69] + z[71];
    z[71]=z[41]*npow(z[13],2);
    z[69]=z[69]*z[71];
    z[59]= - z[10]*z[4]*z[59];
    z[59]= - z[61] + z[59];
    z[59]=z[10]*z[59];
    z[72]= - z[51] + z[21];
    z[72]=z[5]*z[72]*z[55];
    z[59]=z[69] + z[67] + z[59] + z[72];
    z[67]=npow(z[6],2);
    z[69]=z[67]*z[41];
    z[72]=3*z[50];
    z[73]=z[34] - z[72];
    z[73]=z[73]*z[69];
    z[74]=z[10] + z[20];
    z[74]=z[74]*z[69];
    z[24]= - z[5]*z[24];
    z[24]=z[24] + 2*z[74];
    z[24]=z[2]*z[24];
    z[24]=z[73] + z[24];
    z[24]=z[2]*z[24];
    z[73]=z[45]*z[4];
    z[74]=z[50]*z[11];
    z[75]=z[5]*z[34];
    z[75]=z[75] + z[73] + z[74];
    z[75]=z[75]*z[69];
    z[76]=z[66]*z[16];
    z[77]=z[2]*z[6];
    z[77]=z[77] + z[76];
    z[77]=z[56]*z[77];
    z[78]=z[44]*z[48];
    z[78]=z[78] - z[74];
    z[79]=3*z[6];
    z[78]=z[78]*z[79];
    z[73]=z[9]*z[73];
    z[73]=z[73] + z[78];
    z[73]=z[41]*z[73];
    z[73]=z[77] + z[73];
    z[73]=z[9]*z[73];
    z[24]=z[73] + z[24] + 2*z[59] + z[75];
    z[24]=z[9]*z[24];
    z[59]=2*z[14];
    z[73]=3*z[12];
    z[75]= - z[59] + z[73];
    z[75]=z[4]*z[75];
    z[77]= - z[40] + z[4];
    z[77]=z[10]*z[77];
    z[75]=z[77] - z[52] + z[75];
    z[75]=z[10]*z[75];
    z[77]=z[5]*z[3];
    z[78]= - z[77]*z[60];
    z[80]=z[36]*npow(z[16],3);
    z[56]=z[56]*z[80];
    z[81]=z[55]*z[3];
    z[82]= - z[12]*z[81];
    z[56]=z[56] + z[78] + z[82] + z[61] + z[75];
    z[71]=z[77]*z[71];
    z[46]= - z[46] - 3*z[71];
    z[46]=z[13]*z[46];
    z[71]=6*z[4] + z[3];
    z[75]=z[13]*z[77];
    z[78]=z[60]*z[3];
    z[82]=z[6]*z[78];
    z[71]=z[82] - 7*z[75] + 3*z[71] - 11*z[5];
    z[71]=z[2]*z[71];
    z[75]=z[55]*z[12];
    z[75]= - z[75] - z[78];
    z[82]=4*z[12];
    z[83]=z[6]*z[50]*z[82];
    z[84]=6*z[3];
    z[85]=23*z[4] + z[84];
    z[85]=z[5]*z[85];
    z[46]=z[71] + z[83] + z[46] + 2*z[75] + z[85];
    z[46]=z[2]*z[46];
    z[71]=n<T>(1,3)*z[77];
    z[75]=npow(z[7],4);
    z[83]=z[33]*z[75]*npow(z[6],4)*z[71];
    z[85]=z[50]*z[4];
    z[86]= - z[12]*z[85];
    z[83]=z[86] + z[83];
    z[86]=npow(z[10],3);
    z[87]=z[2]*z[50];
    z[87]= - z[86] + z[87];
    z[87]=z[2]*z[87];
    z[86]= - z[11]*z[86];
    z[86]=z[86] + z[87];
    z[67]=npow(z[9],2)*z[86]*z[75]*z[67];
    z[75]=7*z[4];
    z[86]=z[3]*z[75];
    z[87]= - 18*z[4] - 7*z[3];
    z[87]=z[5]*z[87];
    z[86]=z[86] + z[87];
    z[86]=z[2]*z[86];
    z[87]=z[4]*z[77];
    z[86]=13*z[87] + z[86];
    z[86]=z[2]*z[86];
    z[87]= - z[1]*npow(z[2],2)*z[77]*z[75];
    z[67]=z[87] + z[67] + 4*z[83] + z[86];
    z[67]=z[1]*z[67];
    z[83]=z[3]*z[8];
    z[86]=z[34] - 5*z[83];
    z[86]=z[5]*z[86];
    z[54]=z[54]*z[3];
    z[86]=z[54] + z[86];
    z[69]=z[86]*z[69];
    z[81]=z[51]*z[81];
    z[69]=z[81] + z[69];
    z[81]=2*z[6];
    z[69]=z[69]*z[81];
    z[86]=z[3]*z[14];
    z[87]=z[86] + 2*z[44];
    z[87]=z[87]*z[27];
    z[41]= - z[41]*npow(z[13],3)*z[87];
    z[24]=z[67] + z[24] + z[46] + z[69] + 2*z[56] + z[41];
    z[24]=z[1]*z[24];
    z[41]= - z[12] - z[55];
    z[41]=z[41]*z[55];
    z[46]=z[12] + z[3];
    z[46]=z[46]*z[28];
    z[56]=z[17] - n<T>(7,3)*z[8];
    z[56]= - 3*z[3] + 2*z[56] - z[10];
    z[25]=z[56]*z[25];
    z[25]=2*z[25] + 3*z[46] + z[41] - z[78];
    z[25]=z[6]*z[25];
    z[41]=z[13]*z[3];
    z[46]=z[41]*z[21];
    z[56]=2*z[21];
    z[67]=npow(z[3],2);
    z[69]= - z[56] + z[67];
    z[69]=3*z[69] - z[71];
    z[71]=3*z[22];
    z[69]=6*z[46] + 2*z[69] + z[71];
    z[69]=z[13]*z[69];
    z[69]=z[69] - z[27] + z[26] - n<T>(23,3)*z[3];
    z[69]=z[13]*z[69];
    z[78]=2*z[13];
    z[88]= - z[22]*z[78];
    z[89]=z[3]*z[4];
    z[89]=z[89] + z[71];
    z[89]=z[6]*z[89];
    z[88]=z[89] + z[88] + z[48] + z[3];
    z[88]=z[88]*z[79];
    z[69]=z[88] + n<T>(61,3) + z[69];
    z[69]=z[2]*z[69];
    z[88]=z[82]*z[21];
    z[89]=z[21] + 3*z[67];
    z[89]=z[3]*z[89];
    z[89]= - z[88] + z[89];
    z[90]=2*z[3];
    z[91]=z[27] - z[12] - z[90];
    z[71]=z[91]*z[71];
    z[71]=2*z[89] + z[71];
    z[71]=z[13]*z[71];
    z[89]= - z[3] + z[58] + z[26];
    z[89]=z[15]*z[89];
    z[89]=z[77] + z[89];
    z[71]=2*z[89] + z[71];
    z[71]=z[13]*z[71];
    z[89]=z[73] - z[15];
    z[91]=n<T>(4,3)*z[3];
    z[25]=z[69] + z[25] + z[71] + z[91] + z[63] - 8*z[4] - 4*z[30] + 
    z[89] + 7*z[5];
    z[25]=z[2]*z[25];
    z[69]=z[12] + z[14];
    z[71]=z[69]*z[60];
    z[92]=9*z[3];
    z[93]= - n<T>(40,3)*z[8] + z[92];
    z[93]=z[5]*z[93];
    z[71]=z[93] - 4*z[83] + z[71] + 2*z[50];
    z[71]=z[71]*z[22];
    z[34]=z[34]*z[77];
    z[71]=z[34] + z[71];
    z[71]=z[6]*z[71];
    z[40]=z[4]*z[40];
    z[93]= - z[10]*z[73];
    z[40]=z[93] - 3*z[51] + z[40];
    z[40]=z[3]*z[40];
    z[85]= - z[61] - z[85];
    z[40]=2*z[85] + z[40];
    z[85]=n<T>(8,3)*z[8];
    z[75]= - z[85] - z[75];
    z[75]=z[3]*z[75];
    z[93]=n<T>(8,3)*z[33];
    z[75]=z[93] + z[75];
    z[75]=z[5]*z[75];
    z[50]=z[16]*z[50]*z[65];
    z[65]=3*z[86] + n<T>(7,3)*z[77];
    z[28]=z[65]*z[28];
    z[28]=z[71] + 5*z[28] + z[50] + 2*z[40] + z[75];
    z[28]=z[6]*z[28];
    z[40]= - static_cast<T>(1)- z[62];
    z[40]=z[40]*z[66];
    z[50]=z[36]*z[16];
    z[65]= - static_cast<T>(1)+ z[50];
    z[65]=z[65]*z[55];
    z[71]=3*z[14];
    z[75]=z[17] + z[8];
    z[40]=z[65] + z[40] + z[43] + 4*z[75] - z[71];
    z[40]=z[10]*z[40];
    z[65]= - z[15] + z[10];
    z[65]=z[22]*z[38]*z[32]*z[65];
    z[56]=z[70] + z[56];
    z[56]=z[3]*z[56];
    z[56]=z[88] + z[56];
    z[75]=z[15]*z[12];
    z[53]=z[75] - z[53];
    z[94]= - z[71] + z[12];
    z[94]=3*z[94] - z[36];
    z[94]=z[3]*z[94];
    z[95]= - 18*z[3] + z[26] - 35*z[14] + 6*z[12];
    z[95]=z[5]*z[95];
    z[53]=z[95] + 6*z[53] + z[94];
    z[22]=z[53]*z[22];
    z[53]=z[14]*npow(z[3],3);
    z[22]= - 6*z[53] + z[22];
    z[22]=z[13]*z[22];
    z[22]=z[22] + 2*z[56] - z[87];
    z[22]=z[13]*z[22];
    z[51]=z[51] - z[44];
    z[53]=z[32]*z[36];
    z[56]=z[16] + z[53];
    z[56]=z[10]*z[56];
    z[50]=z[56] - static_cast<T>(1)- z[50];
    z[50]=z[36]*z[50];
    z[56]= - z[14] + z[12];
    z[50]=3*z[56] + z[50];
    z[50]=z[3]*z[50];
    z[26]= - n<T>(17,3)*z[3] - z[26] - 21*z[4] - z[85] - 5*z[14];
    z[26]=z[5]*z[26];
    z[56]= - z[14] + 12*z[12];
    z[56]=z[4]*z[56];
    z[43]=z[15]*z[43];
    z[22]=z[24] + z[23] + z[25] + z[28] + z[22] + z[65] + z[26] + z[50]
    + z[40] + z[43] - 6*z[51] + z[56];
    z[22]=z[1]*z[22];
    z[23]=z[15]*z[4];
    z[24]=z[11] - z[15];
    z[24]=z[10]*z[24];
    z[23]=z[24] - z[57] + z[23];
    z[24]=2*z[5];
    z[23]=z[23]*z[24];
    z[25]=12*z[44];
    z[26]= - z[25] - z[57];
    z[26]=z[4]*z[26];
    z[28]=z[82] - z[60];
    z[28]=z[28]*z[36];
    z[40]=18*z[12] + z[4];
    z[40]=z[4]*z[40];
    z[28]=z[40] + z[28];
    z[28]=z[15]*z[28];
    z[40]=z[2]*z[49];
    z[43]=z[9]*z[47];
    z[23]=z[43] + z[40] + z[23] - z[29] + z[26] + z[28];
    z[23]=z[9]*z[23];
    z[20]=z[63] - 4*z[20] + z[64];
    z[20]=z[6]*z[20];
    z[26]=9*z[13];
    z[28]= - z[15]*z[26];
    z[20]=z[20] + n<T>(7,3) + z[28];
    z[20]=z[2]*z[20];
    z[28]=2*z[4];
    z[29]=z[28] + z[30];
    z[30]= - z[93] + z[72];
    z[30]=z[6]*z[30];
    z[40]=6*z[21];
    z[40]=z[13]*z[40];
    z[20]=z[20] + z[30] + z[40] + 2*z[29] + z[10];
    z[20]=z[2]*z[20];
    z[29]= - static_cast<T>(5)- 6*z[62];
    z[29]=z[29]*z[36];
    z[30]=static_cast<T>(3)+ z[76];
    z[30]=z[30]*z[55];
    z[29]=z[30] + z[29] - z[11] - z[48];
    z[29]=z[10]*z[29];
    z[30]=z[70] - z[52] + 3*z[21];
    z[30]=z[5]*z[30];
    z[40]= - z[52] + z[75];
    z[40]=z[40]*z[66];
    z[30]=z[40] + z[30];
    z[30]=z[30]*z[78];
    z[40]= - z[15] + z[58] + z[4];
    z[40]=z[40]*z[36];
    z[18]= - z[4] + z[12] + z[18] + z[71];
    z[18]=z[18]*z[24];
    z[43]=z[44]*z[4];
    z[43]= - 2*z[43] - z[74];
    z[44]= - z[5]*z[93];
    z[43]=3*z[43] + z[44];
    z[43]=z[6]*z[43];
    z[44]= - 11*z[12] + 13*z[14];
    z[47]= - 2*z[11] - z[44];
    z[47]=z[4]*z[47];
    z[18]=z[23] + z[20] + z[43] + z[30] + z[18] + z[29] + z[40] - z[25]
    + z[47];
    z[18]=z[9]*z[18];
    z[20]= - 8*z[33] + 19*z[83];
    z[20]=z[5]*z[20];
    z[23]= - z[6]*z[34];
    z[20]=z[23] + n<T>(1,3)*z[20] + z[61] - z[54];
    z[20]=z[20]*z[81];
    z[23]= - z[38] - z[28] - 4*z[17] - z[37];
    z[23]=z[10]*z[23];
    z[25]=z[38] - z[71];
    z[28]=z[82] + z[85] + z[25];
    z[28]=z[3]*z[28];
    z[29]=z[59] + z[73];
    z[29]=z[4]*z[29];
    z[30]= - z[84] + 10*z[8] - z[4];
    z[24]=z[30]*z[24];
    z[30]=z[52] - 5*z[86];
    z[33]=3*z[41];
    z[30]=z[30]*z[33];
    z[20]=z[20] + z[30] + z[24] + z[28] + z[23] + z[45] + z[29];
    z[20]=z[6]*z[20];
    z[23]=z[36] - z[92];
    z[23]=z[3]*z[23];
    z[24]= - z[12] + n<T>(13,3)*z[3];
    z[28]=z[5]*z[24];
    z[23]= - 3*z[46] + z[28] + z[75] + z[23];
    z[23]=z[23]*z[78];
    z[28]= - z[73] - z[36];
    z[23]=z[23] - n<T>(17,3)*z[5] + 2*z[28] + n<T>(91,3)*z[3];
    z[23]=z[13]*z[23];
    z[28]=z[17] + z[39];
    z[29]=z[13]*z[67];
    z[24]=15*z[29] + z[31] + 4*z[28] + z[24];
    z[24]=z[6]*z[24];
    z[28]=z[3]*z[15];
    z[28]=z[28] - 2*z[77];
    z[28]=z[13]*z[28];
    z[29]=z[15] - z[3];
    z[28]=z[28] + 2*z[29] + z[5];
    z[28]=z[13]*z[28];
    z[28]=n<T>(31,3) + 3*z[28];
    z[28]=z[13]*z[28];
    z[29]=z[6]*z[60];
    z[29]=z[29] - n<T>(28,3) + 15*z[41];
    z[29]=z[6]*z[29];
    z[28]=z[28] + z[29];
    z[28]=z[2]*z[28];
    z[23]=z[28] + z[24] - n<T>(59,3) + z[23];
    z[23]=z[2]*z[23];
    z[21]=9*z[86] - z[70] - z[21];
    z[21]=z[3]*z[21];
    z[21]=z[87] + z[88] + z[21];
    z[21]=z[21]*z[78];
    z[24]=z[15]*z[89];
    z[24]= - 3*z[51] + z[24];
    z[28]= - 12*z[14] - z[12];
    z[29]=static_cast<T>(5)- z[76];
    z[29]=z[15]*z[29];
    z[28]=2*z[28] + z[29];
    z[28]=z[3]*z[28];
    z[29]=n<T>(28,3)*z[3] + z[66] + 31*z[14] + 7*z[12];
    z[29]=z[5]*z[29];
    z[21]=z[21] + z[29] + 2*z[24] + z[28];
    z[21]=z[13]*z[21];
    z[24]=3*z[16];
    z[28]= - z[24] - z[35];
    z[28]=z[10]*z[28];
    z[29]= - z[16]*z[19];
    z[30]=z[15]*z[32];
    z[30]=11*z[16] + 6*z[30];
    z[30]=z[15]*z[30];
    z[28]=z[28] + z[30] + static_cast<T>(5)+ z[29];
    z[28]=z[28]*z[55];
    z[24]=z[24] + z[53];
    z[24]=z[24]*z[36];
    z[29]= - 3*z[32] - z[80];
    z[29]=z[15]*z[29];
    z[29]=2*z[16] + z[29];
    z[29]=z[29]*z[55];
    z[24]=z[29] - static_cast<T>(7)+ z[24];
    z[24]=z[3]*z[24];
    z[29]=8*z[17];
    z[30]= - static_cast<T>(1)- 8*z[62];
    z[30]=z[15]*z[30];
    z[18]=z[22] + z[18] + z[23] + z[20] + z[21] - 8*z[5] + z[24] + z[28]
    + z[30] + z[29] - z[37] - z[44];
    z[18]=z[1]*z[18];
    z[20]= - z[90] - z[5];
    z[20]=z[7]*z[20];
    z[21]=z[77]*z[7];
    z[22]=z[13]*z[21];
    z[20]=z[20] + z[22];
    z[20]=z[13]*z[20];
    z[22]=z[6] - z[78];
    z[21]=z[21]*z[22];
    z[22]=z[3] + z[5];
    z[22]=z[7]*z[22];
    z[21]=z[22] + z[21];
    z[21]=z[6]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[68] - n<T>(5,3)*z[3] + z[73] + z[29] - z[25] + n<T>(4,3)*z[20];
    z[20]=z[6]*z[20];
    z[21]= - z[13]*z[7]*z[51]*z[27];
    z[22]=z[69]*z[7];
    z[23]=z[22]*z[5];
    z[21]=7*z[23] + z[21];
    z[21]=z[21]*z[78];
    z[21]=3*z[22] + z[21];
    z[21]=z[13]*z[21];
    z[24]=z[22]*z[4];
    z[25]=z[6]*z[24];
    z[25]=z[22] + z[25];
    z[25]=z[25]*z[79];
    z[27]=z[9] + z[81];
    z[24]=z[24]*z[27];
    z[22]=z[22] + z[24];
    z[22]=z[9]*z[22];
    z[21]=3*z[22] + z[25] + z[21] - z[5] + z[15] + z[60] + z[42];
    z[21]=z[9]*z[21];
    z[22]=15*z[69] + z[91];
    z[22]=z[7]*z[22];
    z[23]=z[23]*z[26];
    z[22]=z[22] + z[23];
    z[22]=z[13]*z[22];
    z[23]= - 9*z[14] - z[82];
    z[22]=z[22] - n<T>(44,3)*z[5] + n<T>(19,3)*z[3] + 2*z[23] + z[15];
    z[22]=z[13]*z[22];
    z[19]= - z[55] + z[19];
    z[19]=z[16]*z[19];
    z[19]=z[62] - static_cast<T>(1)+ z[19];
    z[23]=29*z[13] + 26*z[6];
    z[23]=z[2]*z[23];

    r += z[18] + 2*z[19] + z[20] + z[21] + z[22] + n<T>(1,3)*z[23];
 
    return r;
}

template double qg_2lNLC_r1395(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1395(const std::array<dd_real,31>&);
#endif
