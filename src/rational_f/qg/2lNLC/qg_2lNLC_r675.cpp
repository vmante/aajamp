#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r675(const std::array<T,31>& k) {
  T z[135];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[12];
    z[8]=k[13];
    z[9]=k[14];
    z[10]=k[3];
    z[11]=k[5];
    z[12]=k[8];
    z[13]=k[2];
    z[14]=k[28];
    z[15]=z[11]*z[1];
    z[16]=npow(z[1],2);
    z[17]=z[15] - z[16];
    z[18]=z[11]*z[17];
    z[19]=npow(z[1],3);
    z[18]= - z[19] + z[18];
    z[18]=z[11]*z[18];
    z[20]=z[15] + z[16];
    z[21]=3*z[11];
    z[20]=z[20]*z[21];
    z[20]=z[20] + z[19];
    z[22]=npow(z[11],2);
    z[23]=z[22]*z[4];
    z[24]=z[20]*z[23];
    z[18]=z[18] + z[24];
    z[24]=2*z[4];
    z[18]=z[18]*z[24];
    z[25]=18*z[16] + 13*z[15];
    z[25]=z[25]*z[21];
    z[26]=22*z[19];
    z[18]=z[18] + z[26] + z[25];
    z[18]=z[4]*z[18];
    z[25]=z[16]*z[11];
    z[27]=z[19] + z[25];
    z[27]=z[8]*z[27];
    z[28]=2*z[15];
    z[29]=3*z[16];
    z[27]=z[27] + z[29] + z[28];
    z[27]=z[11]*z[27];
    z[27]=z[19] + z[27];
    z[27]=z[8]*z[27];
    z[30]=npow(z[15],4);
    z[31]=npow(z[4],2);
    z[32]=z[31]*z[30];
    z[33]=npow(z[4],3);
    z[34]=z[33]*z[3];
    z[35]=npow(z[15],5)*z[34];
    z[32]=z[32] + z[35];
    z[35]=2*z[3];
    z[32]=z[32]*z[35];
    z[36]=npow(z[11],3);
    z[37]=z[36]*z[4];
    z[38]=z[37]*z[19];
    z[32]=7*z[38] + z[32];
    z[32]=z[3]*z[32];
    z[39]=z[16]*z[2];
    z[40]=3*z[39];
    z[41]= - z[40] - 11*z[25];
    z[41]=z[11]*z[41];
    z[32]=z[41] + z[32];
    z[41]=npow(z[3],2);
    z[32]=z[32]*z[41];
    z[42]=z[2]*z[1];
    z[43]=3*z[42];
    z[18]=z[32] + z[27] + z[18] - 27*z[15] - 16*z[16] - z[43];
    z[18]=z[7]*z[18];
    z[27]= - z[28] + z[16] - 34*z[42];
    z[27]=z[11]*z[27];
    z[32]=npow(z[2],2);
    z[44]=z[32]*z[16];
    z[45]=2*z[42];
    z[46]=z[45] - z[16];
    z[47]=z[11]*z[46];
    z[48]=z[32]*z[1];
    z[47]= - 16*z[48] + z[47];
    z[47]=z[11]*z[47];
    z[47]= - 15*z[44] + z[47];
    z[47]=z[8]*z[47];
    z[49]=6*z[42];
    z[50]=z[49] - z[16];
    z[51]=7*z[2];
    z[52]=z[50]*z[51];
    z[53]=z[39] - z[19];
    z[54]=z[53]*z[4];
    z[55]= - z[2]*z[54];
    z[27]=z[47] + z[55] + z[52] + z[27];
    z[27]=z[8]*z[27];
    z[47]=9*z[16];
    z[52]= - z[47] + z[45];
    z[55]=5*z[16];
    z[56]=z[55] - z[45];
    z[56]=z[2]*z[56];
    z[57]=3*z[19];
    z[56]= - z[57] + z[56];
    z[56]=z[4]*z[56];
    z[27]=z[27] + z[56] + 3*z[52] - z[28];
    z[27]=z[8]*z[27];
    z[52]=z[31]*z[22];
    z[56]=2*z[11];
    z[58]=z[56]*z[19];
    z[59]=z[19]*z[2];
    z[60]= - z[59] + z[58];
    z[60]=z[60]*z[52];
    z[61]=z[32]*z[19];
    z[62]=z[19]*z[11];
    z[63]=z[62]*z[2];
    z[64]=7*z[61] + 3*z[63];
    z[65]=npow(z[8],2);
    z[64]=z[11]*z[64]*z[65];
    z[60]=z[60] + z[64];
    z[64]=npow(z[1],4);
    z[66]=z[64]*z[32];
    z[67]=z[64]*z[2];
    z[68]=z[64]*z[11];
    z[69]=z[67] - z[68];
    z[70]= - z[11]*z[69];
    z[70]=z[66] + z[70];
    z[71]=2*z[33];
    z[70]=z[70]*z[71];
    z[72]=npow(z[8],3);
    z[73]= - z[72]*z[66];
    z[70]=z[70] + z[73];
    z[70]=z[3]*z[22]*z[70];
    z[60]=2*z[60] + z[70];
    z[60]=z[3]*z[60];
    z[70]=z[22]*z[16];
    z[73]=14*z[44] + 11*z[70];
    z[73]=z[4]*z[73];
    z[74]=z[32]*z[56];
    z[74]=15*z[39] + z[74];
    z[74]=z[74]*z[56];
    z[74]= - 19*z[44] + z[74];
    z[74]=z[8]*z[74];
    z[60]=z[60] + z[73] + z[74];
    z[60]=z[3]*z[60];
    z[73]= - z[56] - z[23];
    z[74]=z[32]*z[4];
    z[73]=z[73]*z[74];
    z[75]=2*z[2];
    z[76]=z[8]*z[32];
    z[76]= - z[75] + z[76];
    z[76]=z[8]*z[22]*z[76];
    z[73]=z[73] + z[76];
    z[60]=2*z[73] + z[60];
    z[60]=z[3]*z[60];
    z[73]=2*z[19];
    z[76]=z[73] - z[39];
    z[76]=z[76]*z[2];
    z[76]=z[76] - z[64];
    z[76]=z[76]*z[4];
    z[77]=2*z[16];
    z[78]=z[43] - z[77];
    z[79]=z[2]*z[78];
    z[79]=z[76] - z[19] + z[79];
    z[79]=z[79]*z[24];
    z[80]=17*z[39];
    z[81]= - 13*z[19] - z[80];
    z[81]=z[2]*z[81];
    z[82]=z[8]*z[61];
    z[81]=z[81] + 10*z[82];
    z[81]=z[8]*z[81];
    z[82]=11*z[16];
    z[83]=z[82] + z[42];
    z[83]=z[83]*z[75];
    z[81]=z[81] + z[57] + z[83];
    z[83]=4*z[8];
    z[81]=z[81]*z[83];
    z[84]=12*z[64];
    z[85]=z[84] + 7*z[59];
    z[85]=z[2]*z[85];
    z[86]=6*z[8];
    z[87]= - z[66]*z[86];
    z[85]=z[85] + z[87];
    z[85]=z[85]*z[83];
    z[87]=9*z[39];
    z[88]= - 56*z[19] - z[87];
    z[88]=z[2]*z[88];
    z[89]=24*z[64];
    z[85]=z[85] - z[89] + z[88];
    z[85]=z[8]*z[85];
    z[85]=z[85] + 19*z[19] + 18*z[39];
    z[85]=z[9]*z[85];
    z[88]=4*z[42];
    z[90]= - z[16] - z[88];
    z[79]=z[85] + z[81] + 5*z[90] + z[79];
    z[79]=z[9]*z[79];
    z[81]=3*z[15];
    z[85]= - z[81] - z[29] + z[45];
    z[85]=z[11]*z[85];
    z[90]=z[4]*z[11];
    z[20]=z[20]*z[90];
    z[91]=z[59] - z[62];
    z[92]=z[91]*z[22];
    z[93]=z[3]*z[4]*z[30];
    z[92]=z[92] + z[93];
    z[93]=npow(z[3],3);
    z[92]=z[92]*z[93];
    z[20]=z[92] + z[20] + z[85] + z[53];
    z[20]=z[7]*z[20];
    z[53]=z[77] - z[42];
    z[85]=z[81] + z[53];
    z[85]=z[85]*z[90];
    z[92]=z[91]*z[3];
    z[94]= - z[23]*z[92];
    z[95]=z[11]*z[39];
    z[94]=z[95] + z[94];
    z[94]=z[94]*z[41];
    z[20]=z[20] + z[94] - z[15] + z[85];
    z[85]=4*z[12];
    z[20]=z[20]*z[85];
    z[94]=z[53]*z[2];
    z[94]=z[94] - z[19];
    z[53]=z[81] - z[53];
    z[53]=z[11]*z[53];
    z[81]=z[15] - z[42];
    z[95]=z[16] + z[81];
    z[95]=z[95]*z[23];
    z[53]=3*z[95] + z[53] - z[94];
    z[53]=z[4]*z[53];
    z[95]=23*z[16];
    z[53]=z[53] + 17*z[15] + z[95] - 19*z[42];
    z[53]=z[53]*z[24];
    z[96]=3*z[1];
    z[97]=4*z[11];
    z[98]=z[96] + z[97];
    z[99]=14*z[2];
    z[18]=z[20] + z[18] + z[79] + z[60] + z[27] + z[53] + z[99] - z[98];
    z[18]=z[12]*z[18];
    z[20]= - 11*z[19] + z[25];
    z[20]=z[11]*z[20];
    z[27]=z[22]*z[19];
    z[53]= - z[8]*z[27];
    z[20]=z[20] + z[53];
    z[20]=z[8]*z[20];
    z[53]=z[22]*z[64];
    z[60]=3*z[8];
    z[79]= - z[60]*z[53];
    z[79]=13*z[27] + z[79];
    z[79]=z[8]*z[79];
    z[79]= - 10*z[70] + z[79];
    z[100]=2*z[9];
    z[79]=z[79]*z[100];
    z[101]=14*z[15];
    z[102]=z[82] - z[101];
    z[102]=z[11]*z[102];
    z[20]=z[79] + z[102] + z[20];
    z[79]=4*z[9];
    z[20]=z[20]*z[79];
    z[102]=23*z[15];
    z[103]=6*z[16];
    z[104]=z[103] + z[102];
    z[104]=z[104]*z[56];
    z[105]=z[8]*z[70];
    z[104]=z[104] - 17*z[105];
    z[104]=z[8]*z[104];
    z[105]=z[70]*z[41];
    z[20]=z[20] - 59*z[105] + z[104] - z[16] + 30*z[15];
    z[20]=z[9]*z[20];
    z[104]=8*z[42];
    z[106]=z[29] - z[104];
    z[107]=npow(z[2],3);
    z[106]=z[106]*z[107];
    z[108]=npow(z[2],4);
    z[109]=z[108]*z[4];
    z[46]=z[46]*z[109];
    z[46]=z[106] + z[46];
    z[46]=z[4]*z[46];
    z[106]=16*z[42];
    z[55]= - z[55] + z[106];
    z[55]=z[55]*z[32];
    z[46]=z[55] + z[46];
    z[46]=z[4]*z[46];
    z[55]=5*z[31];
    z[110]=npow(z[42],5);
    z[111]=z[110]*z[55];
    z[112]= - npow(z[42],6)*z[34];
    z[111]=z[111] + z[112];
    z[111]=z[3]*z[111];
    z[112]=npow(z[42],4);
    z[113]=z[4]*z[112];
    z[111]= - 12*z[113] + z[111];
    z[111]=z[3]*z[111];
    z[113]=z[107]*z[19];
    z[111]=16*z[113] + z[111];
    z[111]=z[111]*z[93];
    z[106]=z[29] - z[106];
    z[106]=z[2]*z[106];
    z[46]=z[111] + z[106] + z[46];
    z[106]=2*z[5];
    z[46]=z[46]*z[106];
    z[111]=z[47] - 22*z[42];
    z[114]=2*z[32];
    z[111]=z[111]*z[114];
    z[115]=8*z[16];
    z[116]= - z[115] + 15*z[42];
    z[117]=z[107]*z[4];
    z[116]=z[116]*z[117];
    z[111]=z[111] + z[116];
    z[111]=z[4]*z[111];
    z[116]= - 7*z[16] + 33*z[42];
    z[116]=z[2]*z[116];
    z[111]=z[111] + z[57] + z[116];
    z[111]=z[4]*z[111];
    z[110]=z[110]*z[34];
    z[116]=z[31]*z[112];
    z[116]=25*z[116] - 7*z[110];
    z[116]=z[3]*z[116];
    z[118]=z[113]*z[4];
    z[116]= - 43*z[118] + z[116];
    z[116]=z[3]*z[116];
    z[119]=z[16] + z[32];
    z[120]=3*z[32];
    z[119]=z[119]*z[120];
    z[121]= - z[4]*npow(z[2],5);
    z[119]=z[119] + z[121];
    z[116]=7*z[119] + z[116];
    z[116]=z[3]*z[116];
    z[119]=3*z[4];
    z[121]=z[108]*z[119];
    z[121]= - 8*z[107] + z[121];
    z[116]=7*z[121] + z[116];
    z[116]=z[3]*z[116];
    z[121]=13*z[16];
    z[46]=z[46] + z[116] + z[111] - z[121] + 30*z[42];
    z[46]=z[5]*z[46];
    z[111]=5*z[42];
    z[116]= - z[29] + z[111];
    z[116]=z[116]*z[32];
    z[122]= - z[16] + z[42];
    z[122]=z[2]*z[122];
    z[122]= - z[19] + 9*z[122];
    z[122]=z[11]*z[122];
    z[116]=4*z[116] + z[122];
    z[116]=z[4]*z[116];
    z[122]=13*z[42];
    z[123]=z[47] - z[122];
    z[123]=z[123]*z[75];
    z[124]=z[16] + z[45];
    z[124]=11*z[124] + z[28];
    z[124]=z[11]*z[124];
    z[116]=z[116] + z[123] + z[124];
    z[116]=z[4]*z[116];
    z[123]= - z[29] + 10*z[42];
    z[124]= - 2*z[123] - 25*z[15];
    z[116]=2*z[124] + z[116];
    z[116]=z[4]*z[116];
    z[124]=5*z[25];
    z[125]= - z[19] + z[124];
    z[125]=z[125]*z[97];
    z[126]=4*z[62];
    z[127]=z[64] - z[126];
    z[90]=z[127]*z[90];
    z[90]=z[125] + z[90];
    z[90]=z[4]*z[90];
    z[125]=z[23]*z[64];
    z[127]= - 4*z[27] + z[125];
    z[127]=z[4]*z[127];
    z[127]=15*z[70] + z[127];
    z[127]=z[8]*z[127];
    z[128]=z[121] - 38*z[15];
    z[128]=z[11]*z[128];
    z[90]=z[127] + z[128] + z[90];
    z[90]=z[4]*z[90];
    z[127]=12*z[15];
    z[90]= - z[127] + z[90];
    z[90]=z[8]*z[90];
    z[128]=7*z[32];
    z[121]=z[121] - z[128];
    z[121]=z[121]*z[114];
    z[129]=z[103] - z[128];
    z[130]=z[11]*z[2];
    z[129]=z[129]*z[130];
    z[121]=z[121] + z[129];
    z[121]=z[4]*z[121];
    z[129]=4*z[2];
    z[131]= - z[62]*z[129];
    z[131]= - 17*z[61] + z[131];
    z[131]=z[11]*z[131];
    z[132]=z[107]*z[73];
    z[131]=z[132] + z[131];
    z[131]=z[131]*z[31];
    z[132]=3*z[107];
    z[133]= - z[68]*z[132];
    z[133]= - 8*z[112] + z[133];
    z[133]=z[133]*z[34];
    z[131]=z[131] + z[133];
    z[131]=z[3]*z[131];
    z[133]=z[32]*z[11];
    z[134]=4*z[107] + z[133];
    z[121]=z[131] + 7*z[134] + z[121];
    z[121]=z[3]*z[121];
    z[131]=2*z[107] + z[133];
    z[131]=z[11]*z[131];
    z[131]=z[108] + z[131];
    z[131]=z[131]*z[24];
    z[131]=z[131] + 34*z[107] + 13*z[133];
    z[131]=z[4]*z[131];
    z[134]= - 29*z[32] - 7*z[130];
    z[121]=z[121] + 2*z[134] + z[131];
    z[121]=z[3]*z[121];
    z[131]= - z[43] - z[17];
    z[134]= - z[39] - z[25];
    z[134]=z[7]*z[134];
    z[131]=2*z[131] + z[134];
    z[131]=z[7]*z[131];
    z[98]= - z[129] + z[98];
    z[20]=z[131] + z[46] + z[20] + z[121] + z[90] + 12*z[98] + z[116];
    z[20]=z[6]*z[20];
    z[46]=z[75]*z[19];
    z[46]=z[46] + z[62];
    z[46]=z[46]*z[4];
    z[80]=z[46] - 6*z[25] - z[73] - z[80];
    z[80]=z[80]*z[24];
    z[90]=npow(z[1],5);
    z[98]= - z[90] + z[69];
    z[98]=z[98]*z[86];
    z[116]=18*z[62];
    z[98]=z[98] + z[116] + 7*z[64] - 18*z[59];
    z[98]=z[98]*z[83];
    z[121]=z[25] - z[39];
    z[98]=z[98] + 47*z[19] - 85*z[121];
    z[98]=z[8]*z[98];
    z[69]=z[69]*z[60];
    z[69]= - 13*z[91] + z[69];
    z[131]=2*z[8];
    z[69]=z[69]*z[131];
    z[134]=z[87] - z[124];
    z[46]=z[69] + 3*z[134] + z[46];
    z[46]=z[46]*z[79];
    z[46]=z[46] - 9*z[105] + z[98] + z[80] - 101*z[15] - 43*z[16] - 100*
    z[42];
    z[46]=z[9]*z[46];
    z[69]=z[56]*z[16];
    z[79]= - z[69] + z[19] - 7*z[39];
    z[79]=z[4]*z[79];
    z[80]=z[88] + z[17];
    z[79]=7*z[80] + z[79];
    z[79]=z[79]*z[24];
    z[80]=4*z[59];
    z[88]= - z[126] - z[64] + z[80];
    z[88]=z[8]*z[88];
    z[98]=8*z[19];
    z[88]=8*z[88] - z[98] + 43*z[121];
    z[88]=z[8]*z[88];
    z[88]=z[88] + 40*z[16] + z[81];
    z[88]=z[8]*z[88];
    z[105]= - z[22]*z[35];
    z[46]=z[46] + z[105] + z[88] + z[79] - 14*z[11] - 48*z[1] - z[51];
    z[46]=z[9]*z[46];
    z[79]=z[61]*z[4];
    z[88]= - z[73] - 11*z[39];
    z[88]=z[2]*z[88];
    z[88]=z[88] + z[79];
    z[88]=z[4]*z[88];
    z[84]=z[84] + 11*z[59];
    z[84]=z[8]*z[84];
    z[105]=19*z[39];
    z[84]=z[84] - 23*z[19] - z[105];
    z[84]=z[131]*z[2]*z[84];
    z[60]= - z[66]*z[60];
    z[60]=4*z[61] + z[60];
    z[60]=z[60]*z[131];
    z[60]=z[79] + z[60];
    z[60]=z[60]*z[100];
    z[60]=z[60] + z[84] + 36*z[39] + z[88];
    z[60]=z[60]*z[100];
    z[66]= - z[29] + 23*z[42];
    z[66]=z[66]*z[75];
    z[84]=8*z[44];
    z[88]= - z[4]*z[84];
    z[66]=z[88] - z[57] + z[66];
    z[66]=z[4]*z[66];
    z[88]=156*z[16] + 41*z[42];
    z[88]=z[2]*z[88];
    z[98]= - z[98] - z[40];
    z[121]=11*z[2];
    z[98]=z[98]*z[121];
    z[89]= - z[89] + z[98];
    z[89]=z[8]*z[89];
    z[88]=z[89] + 60*z[19] + z[88];
    z[88]=z[8]*z[88];
    z[89]= - 17*z[16] - 45*z[42];
    z[60]=z[60] + z[88] + 2*z[89] + z[66];
    z[60]=z[9]*z[60];
    z[50]=z[2]*z[50];
    z[54]= - z[75]*z[54];
    z[50]=z[54] - z[57] + z[50];
    z[50]=z[4]*z[50];
    z[54]=46*z[16] + 9*z[42];
    z[54]=z[2]*z[54];
    z[26]= - z[76] + z[26] + z[54];
    z[26]=z[8]*z[26];
    z[54]=15*z[16];
    z[66]= - z[54] - 26*z[42];
    z[26]=z[26] + 3*z[66] + z[50];
    z[26]=z[8]*z[26];
    z[50]=z[31]*z[113];
    z[66]=z[112]*z[34];
    z[50]=23*z[50] - 7*z[66];
    z[50]=z[3]*z[50];
    z[66]= - 48*z[16] - z[128];
    z[66]=z[66]*z[74];
    z[76]= - z[44]*z[86];
    z[50]=z[50] + z[76] + 21*z[107] + z[66];
    z[50]=z[3]*z[50];
    z[66]=z[107]*z[24];
    z[76]=5*z[32];
    z[66]= - z[76] + z[66];
    z[50]=7*z[66] + z[50];
    z[50]=z[3]*z[50];
    z[66]=z[77] + z[111];
    z[66]=z[2]*z[66];
    z[43]=z[43] + z[16];
    z[88]= - z[43]*z[32];
    z[89]=z[1]*z[109];
    z[88]=z[88] + z[89];
    z[88]=z[4]*z[88];
    z[66]=z[66] + z[88];
    z[66]=z[4]*z[66];
    z[88]=z[112]*z[55];
    z[88]=z[88] - z[110];
    z[88]=z[3]*z[88];
    z[88]= - 12*z[118] + z[88];
    z[88]=z[3]*z[88];
    z[88]=16*z[44] + z[88];
    z[88]=z[88]*z[41];
    z[89]= - z[16] - z[42];
    z[66]=z[88] + 3*z[89] + z[66];
    z[66]=z[66]*z[106];
    z[88]=z[123]*z[74];
    z[48]= - 13*z[48] + z[88];
    z[48]=z[4]*z[48];
    z[48]=z[48] + z[82] + z[45];
    z[48]=z[4]*z[48];
    z[26]=z[66] + z[60] + z[50] + z[26] + z[48] + 41*z[1] + 27*z[2];
    z[26]=z[5]*z[26];
    z[48]=19*z[15] + 65*z[16] - 18*z[42];
    z[48]=z[11]*z[48];
    z[50]=2*z[64];
    z[60]=z[50] + z[62];
    z[60]=z[11]*z[60];
    z[60]=z[90] + z[60];
    z[66]= - 2*z[90] - z[68];
    z[66]=z[11]*z[66];
    z[88]=npow(z[1],6);
    z[66]= - z[88] + z[66];
    z[66]=z[66]*z[86];
    z[60]=19*z[60] + z[66];
    z[60]=z[60]*z[83];
    z[66]= - z[73] - z[25];
    z[66]=z[11]*z[66];
    z[66]= - z[64] + z[66];
    z[60]=89*z[66] + z[60];
    z[60]=z[8]*z[60];
    z[66]= - z[59] - z[62];
    z[66]=z[66]*z[22]*z[93];
    z[48]=9*z[66] + z[60] + z[48] + 37*z[19] - z[87];
    z[48]=z[9]*z[48];
    z[60]=z[19] + z[124];
    z[60]=z[11]*z[60];
    z[60]= - 4*z[64] + z[60];
    z[66]= - z[64] - z[58];
    z[66]=z[11]*z[66];
    z[66]=z[90] + z[66];
    z[66]=z[66]*z[83];
    z[60]=3*z[60] + z[66];
    z[60]=z[60]*z[83];
    z[66]=z[115] - 45*z[15];
    z[66]=z[11]*z[66];
    z[60]=z[60] + 53*z[19] + z[66];
    z[60]=z[8]*z[60];
    z[66]= - z[2] - z[11];
    z[66]=z[66]*z[22];
    z[86]=23*z[25];
    z[87]=z[40] + z[86];
    z[87]=z[3]*z[11]*z[87];
    z[66]=2*z[66] + z[87];
    z[66]=z[3]*z[66];
    z[48]=z[48] + z[66] + z[60] + 11*z[15] - 14*z[16] + z[111];
    z[48]=z[9]*z[48];
    z[60]=z[90] + 4*z[68];
    z[60]=z[60]*z[56];
    z[66]= - z[90]*z[23];
    z[60]=z[60] + z[66];
    z[60]=z[4]*z[60];
    z[66]= - 16*z[64] + 33*z[62];
    z[66]=z[11]*z[66];
    z[60]=z[60] - z[90] + z[66];
    z[60]=z[4]*z[60];
    z[58]=z[60] + z[58] + 21*z[64] + 10*z[59];
    z[58]=z[8]*z[58];
    z[60]= - z[64] - 8*z[62];
    z[60]=z[11]*z[60];
    z[60]=z[60] + z[125];
    z[66]=4*z[4];
    z[60]=z[60]*z[66];
    z[86]=33*z[19] - z[86];
    z[21]=z[86]*z[21];
    z[21]=z[21] + z[60];
    z[21]=z[4]*z[21];
    z[60]=z[75]*z[16];
    z[21]=z[58] + z[21] + 14*z[25] - 67*z[19] - z[60];
    z[21]=z[8]*z[21];
    z[58]= - z[23]*z[57];
    z[58]=25*z[70] + z[58];
    z[58]=z[58]*z[24];
    z[86]= - 171*z[16] + 53*z[15];
    z[86]=z[11]*z[86];
    z[58]=z[58] - z[73] + z[86];
    z[58]=z[4]*z[58];
    z[73]=19*z[16];
    z[86]=z[73] - z[42];
    z[86]=2*z[86] - z[15];
    z[21]=z[21] + 2*z[86] + z[58];
    z[21]=z[8]*z[21];
    z[58]= - z[64]*z[56];
    z[58]=2*z[125] + z[58] + z[90] + 5*z[67];
    z[58]=z[8]*z[58];
    z[67]= - z[64] - 3*z[62];
    z[86]=z[56]*z[4];
    z[67]=z[67]*z[86];
    z[87]=7*z[62];
    z[58]=z[58] + z[87] + z[67];
    z[58]=z[8]*z[58];
    z[67]=z[68] - z[125];
    z[88]=z[11] - z[2];
    z[89]=z[23] - z[88];
    z[89]=z[8]*z[90]*z[89];
    z[67]=2*z[67] + z[89];
    z[67]=z[8]*z[67];
    z[89]=z[23]*z[19];
    z[67]=z[89] + z[67];
    z[67]=z[7]*z[67];
    z[69]=z[19] + z[69];
    z[69]=z[69]*z[86];
    z[58]=z[67] + z[69] + z[58];
    z[58]=z[8]*z[58];
    z[38]= - z[3]*z[38];
    z[38]=z[70] + z[38];
    z[38]=z[38]*z[41];
    z[67]=z[23]*z[1];
    z[67]=z[67] - z[28];
    z[38]=z[38] + z[58] - z[67];
    z[38]=z[7]*z[38];
    z[58]= - z[73] + 22*z[15];
    z[69]=z[103] - 37*z[15];
    z[69]=z[11]*z[69];
    z[70]=z[1]*z[37];
    z[69]=z[69] - 7*z[70];
    z[69]=z[4]*z[69];
    z[58]=2*z[58] + z[69];
    z[58]=z[4]*z[58];
    z[69]=4*z[31];
    z[70]= - z[19]*z[36]*z[69];
    z[30]= - z[30]*z[34];
    z[30]=z[70] + z[30];
    z[30]=z[30]*z[35];
    z[23]= - z[23]*z[29];
    z[23]=z[23] + z[30];
    z[23]=z[3]*z[23];
    z[30]= - z[4]*npow(z[11],4);
    z[30]=2*z[36] + z[30];
    z[30]=z[4]*z[30];
    z[23]=z[30] + z[23];
    z[23]=z[23]*z[35];
    z[21]=z[38] + z[48] + z[23] + z[21] + z[58] - 19*z[1] - 17*z[11];
    z[21]=z[7]*z[21];
    z[23]=2*z[65];
    z[30]= - z[55] + z[23];
    z[30]=z[61]*z[30];
    z[38]=z[34]*z[64];
    z[48]=z[38]*z[132];
    z[30]=z[48] + z[30];
    z[30]=z[3]*z[30];
    z[48]=z[29] + z[128];
    z[48]=z[4]*z[2]*z[48];
    z[55]=21*z[32];
    z[58]=14*z[39];
    z[70]= - z[8]*z[58];
    z[30]=z[30] + z[70] - z[55] + z[48];
    z[30]=z[3]*z[30];
    z[48]=z[32]*z[83];
    z[70]=6*z[2];
    z[30]=z[30] + z[48] + z[70] - 11*z[74];
    z[30]=z[3]*z[30];
    z[48]= - z[69]*z[107]*z[64];
    z[34]=z[90]*z[108]*z[34];
    z[34]=z[48] + z[34];
    z[34]=z[3]*z[34];
    z[34]=8*z[79] + z[34];
    z[34]=z[3]*z[34];
    z[34]= - 8*z[39] + z[34];
    z[34]=z[34]*z[41];
    z[48]=z[32]*z[96];
    z[69]= - z[1]*z[117];
    z[48]=z[48] + z[69];
    z[48]=z[4]*z[48];
    z[48]= - z[111] + z[48];
    z[48]=z[4]*z[48];
    z[34]=z[34] + z[96] + z[48];
    z[34]=z[34]*z[106];
    z[48]= - z[1]*z[74];
    z[48]=z[45] + z[48];
    z[48]=z[48]*z[119];
    z[48]=z[1] + z[48];
    z[48]=z[4]*z[48];
    z[69]=z[115] + 7*z[42];
    z[69]=z[69]*z[131];
    z[69]= - 33*z[1] + z[69];
    z[69]=z[8]*z[69];
    z[30]=z[34] + z[30] + z[69] + static_cast<T>(5)+ z[48];
    z[30]=z[5]*z[30];
    z[34]= - z[8]*z[82];
    z[34]= - 4*z[88] + z[34];
    z[34]=z[8]*z[34];
    z[48]= - z[72]*z[92];
    z[34]=z[34] + z[48];
    z[34]=z[3]*z[34];
    z[48]= - z[8]*z[88];
    z[48]= - static_cast<T>(6)+ z[48];
    z[48]=z[48]*z[131];
    z[52]= - z[7]*z[52];
    z[69]= - z[4]*z[88];
    z[69]= - static_cast<T>(1)+ z[69];
    z[69]=z[4]*z[69];
    z[34]=z[52] + z[34] + z[69] + z[48];
    z[34]=z[35]*z[34];
    z[48]= - z[75] + z[74];
    z[48]=z[4]*z[48];
    z[52]=z[3]*z[65]*z[59];
    z[69]= - z[8]*z[77];
    z[52]=z[69] + z[52];
    z[52]=z[52]*z[35];
    z[69]=z[2]*z[83];
    z[48]=z[52] + z[69] - static_cast<T>(3)+ z[48];
    z[48]=z[48]*z[35];
    z[52]=5*z[1];
    z[69]=z[65]*z[52];
    z[48]=z[69] + z[48];
    z[48]=z[5]*z[48];
    z[65]=z[19]*z[65]*z[41];
    z[65]=z[131] + z[65];
    z[65]=z[5]*z[65]*z[35];
    z[69]= - z[19]*z[72]*z[3];
    z[69]=z[83] + z[69];
    z[69]=z[3]*z[69];
    z[23]=z[23] + z[69];
    z[23]=z[3]*z[23];
    z[23]=z[23] + z[65];
    z[23]=z[10]*z[23];
    z[65]=z[1]*z[72];
    z[23]=z[23] + z[48] + 21*z[65] + z[34];
    z[23]=z[10]*z[23];
    z[34]=z[91]*z[11];
    z[34]=z[34] - z[61];
    z[48]= - z[34]*z[71];
    z[65]=z[80] - z[62];
    z[65]=z[11]*z[65];
    z[65]= - z[61] + z[65];
    z[65]=z[65]*z[72];
    z[48]=z[48] + z[65];
    z[48]=z[3]*z[48];
    z[65]=z[129] - z[11];
    z[65]=z[65]*z[11];
    z[65]=z[65] - z[32];
    z[69]= - z[105] + 7*z[25];
    z[69]=z[8]*z[69];
    z[69]= - 2*z[65] + z[69];
    z[69]=z[8]*z[69];
    z[40]= - z[40] + 8*z[25];
    z[40]=z[40]*z[31];
    z[40]=z[40] + z[69];
    z[40]=2*z[40] + z[48];
    z[40]=z[3]*z[40];
    z[48]=z[88]*z[11];
    z[69]= - z[114] + z[48];
    z[69]=z[4]*z[69];
    z[69]=z[129] + z[69];
    z[24]=z[69]*z[24];
    z[69]= - z[8]*z[65];
    z[51]= - z[51] + 6*z[11];
    z[51]=2*z[51] + z[69];
    z[51]=z[8]*z[51];
    z[24]=z[51] + static_cast<T>(1)+ z[24];
    z[24]=2*z[24] + z[40];
    z[24]=z[3]*z[24];
    z[31]=z[31]*z[27];
    z[36]=z[36]*z[38];
    z[31]=7*z[31] + z[36];
    z[31]=z[31]*z[35];
    z[36]=z[4]*z[25];
    z[31]= - 9*z[36] + z[31];
    z[31]=z[3]*z[31];
    z[36]= - z[22] + z[37];
    z[36]=z[36]*z[66];
    z[31]=z[36] + z[31];
    z[31]=z[3]*z[31];
    z[36]=z[4]*z[67];
    z[36]=18*z[1] + z[36];
    z[36]=z[4]*z[36];
    z[37]=z[7]*z[93]*z[89];
    z[31]=z[37] + z[36] + z[31];
    z[31]=z[7]*z[31];
    z[36]= - z[102] - 73*z[16] + 37*z[42];
    z[36]=z[8]*z[36];
    z[36]=96*z[1] + z[36];
    z[36]=z[8]*z[36];
    z[36]= - static_cast<T>(18)+ z[36];
    z[36]=z[8]*z[36];
    z[37]=z[4]*z[81];
    z[37]= - z[1] + z[37];
    z[37]=z[4]*z[37];
    z[37]=static_cast<T>(2)+ z[37];
    z[37]=z[4]*z[37];
    z[23]=z[23] + z[31] + z[30] + z[24] + z[37] + z[36];
    z[23]=z[10]*z[23];
    z[24]= - z[3]*z[120];
    z[24]=z[129] + z[24];
    z[30]=7*z[3];
    z[24]=z[24]*z[30];
    z[31]=z[41]*z[5];
    z[36]=z[39]*z[31];
    z[37]=z[77]*z[31];
    z[38]=z[30]*z[2];
    z[40]= - static_cast<T>(1)+ z[38];
    z[40]=z[3]*z[40];
    z[37]=z[40] + z[37];
    z[37]=z[10]*z[37];
    z[40]=z[9]*z[1];
    z[24]=3*z[37] - 22*z[36] + 25*z[40] + static_cast<T>(3)+ z[24];
    z[24]=z[5]*z[24];
    z[36]=11*z[93];
    z[27]=z[27]*z[36];
    z[27]= - 6*z[15] + z[27];
    z[37]=z[22]*z[40];
    z[27]=3*z[27] + 28*z[37];
    z[27]=z[9]*z[27];
    z[37]= - z[25]*z[30];
    z[37]= - z[22] + z[37];
    z[37]=z[3]*z[37];
    z[27]=z[27] + z[52] + 4*z[37];
    z[27]=z[9]*z[27];
    z[37]= - z[32]*z[30];
    z[40]=5*z[2] + z[56];
    z[37]=2*z[40] + z[37];
    z[37]=z[3]*z[37];
    z[27]=z[27] + static_cast<T>(6)+ z[37];
    z[37]=z[73] - z[55];
    z[40]=z[3]*z[2];
    z[37]=z[37]*z[40];
    z[37]=49*z[32] + z[37];
    z[37]=z[3]*z[37];
    z[36]= - z[61]*z[36];
    z[36]=z[49] + z[36];
    z[36]=z[36]*z[106];
    z[36]=z[36] - 31*z[1] + z[37];
    z[36]=z[5]*z[36];
    z[27]=2*z[27] + z[36];
    z[27]=z[6]*z[27];
    z[36]=z[41]*z[116];
    z[22]=z[22] + z[36];
    z[22]=z[3]*z[22];
    z[36]=z[9]*npow(z[3],4)*z[53];
    z[22]=z[22] - 18*z[36];
    z[22]=z[22]*z[100];
    z[36]=z[3]*z[82];
    z[36]= - z[97] + z[36];
    z[36]=z[3]*z[36];
    z[22]=z[36] + z[22];
    z[22]=z[9]*z[22];
    z[36]= - z[54] + z[128];
    z[36]=z[3]*z[36];
    z[36]= - z[99] + z[36];
    z[36]=z[3]*z[36];
    z[37]=z[5]*z[93]*z[59];
    z[36]=z[36] + 6*z[37];
    z[36]=z[5]*z[36];
    z[22]=z[36] + z[35] + z[22];
    z[22]=z[6]*z[22];
    z[36]= - static_cast<T>(1)+ z[40];
    z[30]=z[36]*z[30];
    z[36]=z[31]*z[103];
    z[30]=z[30] + z[36];
    z[30]=z[5]*z[30];
    z[31]=z[10]*z[31];
    z[22]= - 7*z[31] + z[30] + z[22];
    z[22]=z[13]*z[22];
    z[30]= - static_cast<T>(1)- z[38];
    z[30]=z[30]*z[35];
    z[31]=z[41]*z[25];
    z[36]=z[9]*z[15];
    z[31]=108*z[36] + 45*z[1] + z[31];
    z[31]=z[9]*z[31];
    z[36]=z[56]*z[3];
    z[37]=static_cast<T>(11)- z[36];
    z[31]=3*z[37] + z[31];
    z[31]=z[9]*z[31];
    z[37]= - z[9]*z[47];
    z[37]=2*z[1] + z[37];
    z[37]=z[9]*z[37];
    z[38]=z[1]*z[85];
    z[37]=z[38] - static_cast<T>(8)+ z[37];
    z[37]=z[12]*z[37];
    z[22]=z[22] + z[27] + z[37] + z[30] + z[31] + z[24];
    z[22]=z[13]*z[22];
    z[24]= - z[32]*z[41]*z[77];
    z[27]=z[2]*z[43];
    z[30]= - z[9]*z[44];
    z[27]=z[27] + z[30];
    z[27]=z[9]*z[27];
    z[24]=z[27] - z[42] + z[24];
    z[24]=z[5]*z[24];
    z[27]=z[60] + z[133];
    z[27]=z[11]*z[27];
    z[27]=z[44] + z[27];
    z[27]=z[3]*z[27];
    z[30]= - z[32] - z[130];
    z[30]=z[11]*z[30];
    z[27]=z[30] + z[27];
    z[27]=z[3]*z[27];
    z[30]=z[9]*z[94];
    z[17]=z[30] + z[27] - z[45] - z[17];
    z[17]=z[12]*z[17];
    z[27]= - z[3]*z[88];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[35];
    z[30]=z[3]*z[16];
    z[30]= - z[75] + z[30];
    z[31]=z[5]*z[3];
    z[30]=z[30]*z[31];
    z[37]=z[41] - z[31];
    z[37]=z[10]*z[37];
    z[27]=z[37] + z[27] + z[30];
    z[27]=z[10]*z[27];
    z[30]= - z[3]*z[65];
    z[30]=z[97] + z[30];
    z[30]=z[3]*z[30];
    z[37]= - z[3]*z[39];
    z[37]= - z[32] + z[37];
    z[31]=z[37]*z[31];
    z[27]=z[27] + z[31] + static_cast<T>(5)+ z[30];
    z[27]=z[10]*z[27];
    z[30]=z[130] - z[32];
    z[31]=z[30]*z[36];
    z[36]=3*z[2] - z[11];
    z[36]=z[11]*z[36];
    z[31]=z[31] + z[114] + z[36];
    z[31]=z[3]*z[31];
    z[36]= - z[60] - z[25];
    z[36]=z[9]*z[36];
    z[36]=z[36] + z[77] - z[81];
    z[36]=z[9]*z[36];
    z[17]=z[27] + z[17] + z[24] + z[36] + z[31] - z[1] - z[97];
    z[17]=z[14]*z[17];
    z[24]=z[68]*z[119];
    z[24]=z[24] - 22*z[62] - z[50] + 3*z[59];
    z[24]=z[4]*z[24];
    z[24]=z[24] + 42*z[25] + z[57] - z[58];
    z[24]=z[4]*z[24];
    z[27]=z[4]*z[68];
    z[27]=z[27] - z[87] - z[64] + z[59];
    z[27]=z[27]*z[119];
    z[28]=z[28] + z[115] - 39*z[42];
    z[28]=z[11]*z[28];
    z[31]= - 39*z[16] + z[104];
    z[31]=z[31]*z[75];
    z[27]=z[27] + z[28] + 63*z[19] + z[31];
    z[27]=z[8]*z[27];
    z[24]=z[27] + z[24] - z[101] - 133*z[16] + 135*z[42];
    z[24]=z[8]*z[24];
    z[27]= - z[127] - z[95] + z[122];
    z[19]=z[19] - 4*z[39];
    z[28]=z[59] - 9*z[62];
    z[28]=z[4]*z[28];
    z[19]=z[28] + 3*z[19] + 59*z[25];
    z[19]=z[4]*z[19];
    z[19]=2*z[27] + z[19];
    z[19]=z[4]*z[19];
    z[19]=z[24] + z[19] + 28*z[11] + 51*z[1] - 50*z[2];
    z[19]=z[8]*z[19];
    z[24]= - z[32]*z[29];
    z[27]=z[39] - z[124];
    z[27]=z[27]*z[56];
    z[24]=z[24] + z[27];
    z[24]=z[4]*z[24];
    z[24]= - 7*z[107] + z[24];
    z[24]=z[4]*z[24];
    z[25]=z[58] + 3*z[25];
    z[25]=z[11]*z[25];
    z[25]= - z[84] + z[25];
    z[25]=z[8]*z[25];
    z[27]=z[30]*z[97];
    z[25]=z[27] + z[25];
    z[25]=z[8]*z[25];
    z[27]=z[34]*z[56];
    z[28]= - z[107]*z[57];
    z[27]=z[28] + z[27];
    z[27]=z[27]*z[33];
    z[28]=z[61] - z[63];
    z[28]=z[11]*z[28]*z[72];
    z[27]=z[27] + z[28];
    z[27]=z[3]*z[27];
    z[28]=14*z[32];
    z[24]=z[27] + z[25] + z[28] + z[24];
    z[24]=z[3]*z[24];
    z[25]=z[76] - z[48];
    z[25]=z[11]*z[25];
    z[25]=z[107] + z[25];
    z[25]=z[4]*z[25];
    z[27]=z[70] - z[11];
    z[29]= - z[11]*z[27];
    z[25]=z[25] + z[28] + z[29];
    z[25]=z[4]*z[25];
    z[28]=z[8]*z[30];
    z[27]=z[28] + z[27];
    z[27]=z[11]*z[27];
    z[27]= - z[32] + z[27];
    z[27]=z[27]*z[131];
    z[24]=z[24] + z[27] + z[25] - 8*z[2] + z[11];
    z[24]=z[24]*z[35];
    z[15]= - 9*z[15] + z[16] + z[49];
    z[16]=z[78]*z[129];
    z[25]=z[11]*z[81];
    z[16]=z[16] - 7*z[25];
    z[16]=z[4]*z[16];
    z[15]=4*z[15] + z[16];
    z[15]=z[4]*z[15];
    z[16]=z[121] - 16*z[1];
    z[15]=z[15] + 2*z[16] - 21*z[11];
    z[15]=z[4]*z[15];

    r +=  - static_cast<T>(12)+ z[15] + 4*z[17] + z[18] + z[19] + z[20] + z[21] + 
      z[22] + z[23] + z[24] + z[26] + z[46];
 
    return r;
}

template double qg_2lNLC_r675(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r675(const std::array<dd_real,31>&);
#endif
