#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1027(const std::array<T,31>& k) {
  T z[105];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[14];
    z[5]=k[19];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[11];
    z[9]=k[5];
    z[10]=k[6];
    z[11]=k[26];
    z[12]=k[12];
    z[13]=k[13];
    z[14]=k[2];
    z[15]=k[3];
    z[16]=k[22];
    z[17]=k[4];
    z[18]=k[29];
    z[19]=k[15];
    z[20]=k[17];
    z[21]=npow(z[13],2);
    z[22]=5*z[21];
    z[23]=n<T>(4,3)*z[8];
    z[24]=z[3]*z[23];
    z[24]=z[24] - z[22];
    z[25]=npow(z[1],3);
    z[26]=z[25]*z[7];
    z[24]=z[26]*z[24];
    z[27]=z[25]*z[3];
    z[28]=npow(z[7],2);
    z[29]=z[27]*z[28];
    z[30]=z[28]*z[25];
    z[31]=z[8]*z[26];
    z[31]= - z[30] + z[31];
    z[32]=n<T>(4,3)*z[12];
    z[31]=z[31]*z[32];
    z[24]=z[31] - n<T>(4,3)*z[29] + z[24];
    z[31]=npow(z[10],3);
    z[24]=z[24]*z[31];
    z[33]=5*z[13];
    z[34]=z[33] + z[23];
    z[35]=z[28]*z[1];
    z[36]=z[35]*z[3];
    z[34]=z[36]*z[34];
    z[37]=z[8]*z[35]*z[32];
    z[24]=z[24] + z[37] + z[34];
    z[24]=z[15]*z[24];
    z[34]=npow(z[1],2);
    z[37]=z[34]*z[7];
    z[38]=z[1] - z[37];
    z[38]=z[3]*z[7]*z[38];
    z[38]= - z[35] + z[38];
    z[39]=z[7]*z[1];
    z[40]=z[13]*z[39];
    z[38]=2*z[38] + 3*z[40];
    z[38]=z[38]*z[33];
    z[40]=z[34]*z[3];
    z[41]=4*z[40];
    z[42]= - z[8]*z[28]*z[41];
    z[43]=z[8]*z[7];
    z[44]= - z[1] - z[37];
    z[44]=z[44]*z[43];
    z[44]=z[35] + z[44];
    z[44]=z[44]*z[32];
    z[24]=z[24] + z[44] + z[42] - z[36] + z[38];
    z[38]=5*z[40];
    z[42]=z[13]*z[34];
    z[42]= - 30*z[42] + n<T>(64,3)*z[37] + z[38];
    z[42]=z[13]*z[42];
    z[44]=z[34]*z[2];
    z[45]= - z[37] + z[44];
    z[45]=z[3]*z[45];
    z[46]=z[7]*z[9];
    z[47]=z[46]*z[25];
    z[21]=z[10]*z[21]*z[47];
    z[21]=10*z[21] + 8*z[45] + z[42];
    z[42]=npow(z[10],2);
    z[21]=z[21]*z[42];
    z[21]=z[21] + 2*z[24];
    z[21]=z[15]*z[21];
    z[24]=2*z[34];
    z[45]=z[9]*z[1];
    z[48]=z[24] + z[45];
    z[49]=5*z[7];
    z[48]=z[48]*z[49];
    z[48]= - 42*z[1] + z[48];
    z[48]=z[7]*z[48];
    z[48]=n<T>(61,3) + z[48];
    z[50]= - z[24] + z[26];
    z[51]=2*z[7];
    z[50]=z[50]*z[51];
    z[52]=3*z[1];
    z[50]=z[52] + z[50];
    z[53]=5*z[3];
    z[50]=z[50]*z[53];
    z[54]=3*z[34];
    z[55]=z[54] + z[45];
    z[56]= - z[7]*z[55];
    z[56]=5*z[1] + z[56];
    z[57]=10*z[13];
    z[56]=z[56]*z[57];
    z[48]=z[56] + 2*z[48] + z[50];
    z[48]=z[13]*z[48];
    z[50]=z[34]*z[9];
    z[56]=n<T>(2,3)*z[50];
    z[58]= - z[28]*z[56];
    z[59]=z[46]*z[34];
    z[60]=z[50]*z[13];
    z[61]= - n<T>(1,3)*z[59] + 3*z[60];
    z[61]=z[61]*z[33];
    z[62]= - z[13]*z[54];
    z[62]=z[40] + z[62];
    z[62]=z[13]*z[62];
    z[63]=n<T>(1,3)*z[44];
    z[64]=z[63]*z[3];
    z[62]= - z[64] + z[62];
    z[65]=5*z[17];
    z[62]=z[62]*z[65];
    z[58]=z[62] + z[58] + z[61];
    z[61]=npow(z[9],2);
    z[30]= - z[10]*z[61]*z[32]*z[30];
    z[62]=z[46]*z[54];
    z[65]=z[50]*z[2];
    z[62]=z[62] + n<T>(50,3)*z[65];
    z[62]=z[12]*z[62];
    z[30]=z[30] + 2*z[58] + z[62];
    z[30]=z[30]*z[42];
    z[58]=z[45] + z[34];
    z[62]=z[58]*z[7];
    z[66]= - 19*z[1] - 20*z[62];
    z[67]=n<T>(1,3)*z[7];
    z[66]=z[66]*z[67];
    z[68]=z[34] + 10*z[26];
    z[43]=z[68]*z[43];
    z[68]=z[2]*z[1];
    z[69]=13*z[68];
    z[43]=n<T>(2,3)*z[43] + z[66] + z[69];
    z[43]=z[12]*z[43];
    z[66]= - static_cast<T>(13)+ 2*z[39];
    z[66]=z[66]*z[67];
    z[70]=2*z[2];
    z[71]=n<T>(29,3) + 4*z[39];
    z[71]=z[71]*z[70];
    z[72]= - z[1] + n<T>(16,3)*z[37];
    z[72]=z[7]*z[72];
    z[72]=n<T>(7,3)*z[68] - static_cast<T>(6)+ z[72];
    z[72]=z[3]*z[72];
    z[73]=5*z[34];
    z[74]= - z[73] + n<T>(22,3)*z[26];
    z[74]=z[3]*z[74];
    z[75]= - z[1] + 2*z[37];
    z[74]=n<T>(4,3)*z[75] + z[74];
    z[75]=2*z[8];
    z[74]=z[75]*z[7]*z[74];
    z[21]=z[21] + z[30] + z[43] + z[74] + z[48] + z[72] + z[66] + z[71];
    z[21]=z[15]*z[21];
    z[30]=npow(z[9],3);
    z[43]=z[25]*z[2];
    z[48]=z[30]*z[43];
    z[66]=3*z[48];
    z[71]=z[25]*z[6];
    z[72]=z[71]*z[30];
    z[74]=z[66] - n<T>(8,3)*z[72];
    z[74]=z[6]*z[74];
    z[66]=z[12]*z[66];
    z[76]=z[10]*z[2]*npow(z[45],4);
    z[77]=npow(z[6],2);
    z[77]=n<T>(8,3)*z[77];
    z[78]=z[77]*z[76];
    z[66]=z[78] + z[74] + z[66];
    z[66]=z[10]*z[66];
    z[74]=z[34]*z[6];
    z[78]=z[61]*z[74];
    z[79]=z[61]*z[34];
    z[80]=z[50]*z[17];
    z[81]=n<T>(13,3)*z[80];
    z[79]= - 3*z[79] + z[81];
    z[79]=z[12]*z[79];
    z[81]=z[6]*z[81];
    z[66]=z[66] + z[79] - 3*z[78] + z[81];
    z[66]=z[66]*z[42];
    z[30]= - z[12]*z[25]*z[30];
    z[78]=z[12] + z[6];
    z[76]=z[78]*z[76];
    z[30]=z[76] - z[72] + z[30];
    z[30]=z[30]*z[31];
    z[76]=z[58]*z[9];
    z[79]=n<T>(1,3)*z[25];
    z[81]=z[76] + z[79];
    z[82]=z[9]*z[2];
    z[82]=z[82] - 1;
    z[81]=z[81]*z[82];
    z[78]=z[81]*z[78];
    z[30]=n<T>(1,3)*z[30] + z[78];
    z[30]=z[11]*z[30];
    z[78]=38*z[45] + 49*z[34];
    z[78]=z[78]*z[9];
    z[78]=z[78] + 20*z[25];
    z[78]=z[78]*z[2];
    z[82]=8*z[45] + 7*z[34];
    z[78]=z[78] - 5*z[82];
    z[82]=17*z[1];
    z[83]=z[82] + z[41];
    z[83]=z[17]*z[83];
    z[83]=z[83] - 4*z[27] + z[78];
    z[83]=z[12]*z[83];
    z[84]=z[6]*z[1];
    z[41]=z[6]*z[41];
    z[41]=17*z[84] + z[41];
    z[41]=z[17]*z[41];
    z[41]=z[41] + z[83];
    z[81]=z[6]*z[81];
    z[78]= - n<T>(4,3)*z[27] + n<T>(1,3)*z[78] + 8*z[81];
    z[78]=z[6]*z[78];
    z[30]=8*z[30] + z[66] + z[78] + n<T>(1,3)*z[41];
    z[30]=z[11]*z[30];
    z[41]=z[8]*z[25];
    z[41]= - z[43] + z[41];
    z[41]=z[12]*z[41];
    z[66]=z[43]*z[3];
    z[78]=z[8]*z[27];
    z[41]=z[41] - z[66] + z[78];
    z[31]=z[31]*z[15];
    z[41]=z[41]*z[31];
    z[66]= - z[8]*z[1];
    z[66]=z[68] + z[66];
    z[66]=z[12]*z[66];
    z[41]=z[66] + z[41];
    z[41]=z[15]*z[41];
    z[66]=n<T>(8,3)*z[1];
    z[78]=z[66] + z[40];
    z[78]=z[8]*z[78];
    z[81]=z[8]*z[34];
    z[81]= - z[44] + z[81];
    z[81]=z[12]*z[81];
    z[83]= - z[3]*z[44];
    z[41]=n<T>(4,3)*z[41] + n<T>(7,3)*z[81] + z[78] - n<T>(8,3)*z[68] + z[83];
    z[41]=z[15]*z[41];
    z[78]=z[25] + n<T>(1,3)*z[50];
    z[78]=z[78]*z[9];
    z[81]=npow(z[1],4);
    z[78]=z[78] + z[81];
    z[83]=4*z[4];
    z[78]=z[78]*z[83];
    z[85]= - z[34] + n<T>(4,3)*z[45];
    z[85]=z[85]*z[9];
    z[86]=5*z[25];
    z[78]=z[78] + z[85] - z[86];
    z[78]=z[78]*z[4];
    z[85]=4*z[45];
    z[87]=z[85] - z[34];
    z[87]=z[87]*z[9];
    z[88]=4*z[25];
    z[87]=z[87] + z[88];
    z[89]=n<T>(1,3)*z[13];
    z[87]=z[87]*z[89];
    z[89]=2*z[45];
    z[90]=z[89] - z[34];
    z[78]=z[78] - z[87] - n<T>(4,3)*z[90];
    z[87]=z[2]*z[78];
    z[90]=npow(z[1],5);
    z[91]=z[90]*z[4];
    z[92]=2*z[81] - z[91];
    z[92]=z[4]*z[92];
    z[92]= - z[25] + z[92];
    z[93]=n<T>(4,3)*z[3];
    z[92]=z[92]*z[93];
    z[78]=z[92] - z[78];
    z[78]=z[8]*z[78];
    z[92]=z[81]*z[13];
    z[94]= - z[25] + z[92];
    z[94]=z[8]*z[94];
    z[92]= - z[2]*z[92];
    z[92]=z[94] + z[43] + z[92];
    z[32]=z[92]*z[32];
    z[92]= - z[81]*z[70];
    z[91]=z[2]*z[91];
    z[91]=z[92] + z[91];
    z[91]=z[4]*z[91];
    z[91]=z[43] + z[91];
    z[91]=z[91]*z[93];
    z[32]=z[41] + z[32] + z[78] + z[91] + z[87];
    z[32]=z[5]*z[32];
    z[30]=z[30] + z[32];
    z[32]= - z[34] - n<T>(2,3)*z[45];
    z[32]=z[9]*z[32];
    z[41]=2*z[25];
    z[78]=z[41] + z[50];
    z[87]=2*z[9];
    z[91]=z[78]*z[87];
    z[91]=7*z[81] + z[91];
    z[92]=n<T>(2,3)*z[7];
    z[91]=z[91]*z[92];
    z[32]=z[91] - n<T>(31,3)*z[25] + 4*z[32];
    z[32]=z[7]*z[32];
    z[91]=23*z[34] + z[45];
    z[92]= - z[13]*z[76];
    z[32]=z[92] + n<T>(1,3)*z[91] + z[32];
    z[32]=z[6]*z[32];
    z[91]=n<T>(17,3)*z[25];
    z[92]=z[81]*z[7];
    z[93]=z[91] - 4*z[92];
    z[93]=z[93]*z[51];
    z[94]=10*z[7];
    z[90]=z[90]*z[94];
    z[90]= - 23*z[81] + z[90];
    z[90]=z[7]*z[90];
    z[90]=22*z[25] + z[90];
    z[90]=z[6]*z[90];
    z[95]=z[81]*z[4];
    z[96]= - z[41] + z[95];
    z[96]=z[4]*z[96];
    z[90]=n<T>(4,3)*z[96] + n<T>(1,3)*z[90] - n<T>(19,3)*z[34] + z[93];
    z[90]=z[3]*z[90];
    z[93]=npow(z[4],2);
    z[96]=z[93]*z[79];
    z[96]= - z[74] + z[96];
    z[97]=z[4]*z[1];
    z[97]=z[84] + z[97];
    z[97]=z[17]*z[97];
    z[96]=z[97] + 4*z[96];
    z[96]=z[3]*z[96];
    z[58]=z[58]*z[83];
    z[58]=z[1] + z[58];
    z[58]=z[4]*z[58];
    z[97]= - z[13]*z[1];
    z[58]=z[97] - 4*z[84] + z[58] + z[96];
    z[58]=z[17]*z[58];
    z[96]=z[34] + n<T>(1,3)*z[45];
    z[56]= - z[25] - z[56];
    z[56]=z[7]*z[56];
    z[56]=z[56] + z[96];
    z[56]=z[7]*z[56];
    z[97]= - 2*z[1] - z[9];
    z[56]=n<T>(1,3)*z[97] + z[56];
    z[96]=z[9]*z[96];
    z[96]=z[25] + z[96];
    z[96]=z[96]*z[83];
    z[96]=z[96] - z[73] - z[45];
    z[96]=z[4]*z[96];
    z[32]=z[58] + z[90] + z[96] + 4*z[56] + z[32];
    z[32]=z[32]*z[75];
    z[56]=n<T>(7,3)*z[34];
    z[58]=3*z[26];
    z[90]=z[56] - z[58];
    z[90]=z[7]*z[90];
    z[66]=z[66] + z[90];
    z[66]=z[6]*z[7]*z[66];
    z[35]= - n<T>(5,3)*z[35] + z[66];
    z[35]=z[3]*z[35];
    z[66]=z[40]*z[42];
    z[90]=z[7]*z[66];
    z[31]=z[29]*z[31];
    z[31]= - n<T>(2,3)*z[31] + z[35] - n<T>(8,3)*z[90];
    z[35]=2*z[14];
    z[31]=z[31]*z[35];
    z[90]=z[45] - z[34];
    z[96]=z[90]*z[7];
    z[97]=z[1] + z[96];
    z[94]=z[97]*z[94];
    z[97]= - z[34] + z[26];
    z[97]=z[97]*z[51];
    z[97]=z[1] + z[97];
    z[98]=5*z[6];
    z[97]=z[97]*z[98];
    z[94]=z[97] + static_cast<T>(1)+ z[94];
    z[94]=z[4]*z[94];
    z[97]=z[28]*z[34];
    z[99]=static_cast<T>(3)- n<T>(4,3)*z[97];
    z[100]=z[34] - 8*z[26];
    z[100]=z[100]*z[67];
    z[100]= - 11*z[1] + z[100];
    z[100]=z[6]*z[100];
    z[99]=5*z[99] + z[100];
    z[99]=z[3]*z[99];
    z[100]=z[34]*z[4];
    z[101]=z[46]*z[100];
    z[93]=z[93]*z[61];
    z[102]=z[10]*z[26]*z[93];
    z[101]=7*z[101] - n<T>(11,3)*z[102];
    z[101]=z[101]*z[42];
    z[102]= - z[73] + z[58];
    z[102]=z[7]*z[102];
    z[103]=n<T>(4,3)*z[74];
    z[102]= - z[103] + n<T>(16,3)*z[1] + z[102];
    z[102]=z[7]*z[102];
    z[102]=static_cast<T>(3)+ z[102];
    z[102]=z[6]*z[102];
    z[104]=static_cast<T>(2)- n<T>(7,3)*z[39];
    z[104]=z[7]*z[104];
    z[102]=z[104] + z[102];
    z[104]=z[7]*z[40];
    z[97]=4*z[97] + 17*z[104];
    z[97]=z[97]*z[42];
    z[36]=10*z[36] + z[97];
    z[36]=z[15]*z[36];
    z[31]=z[31] + n<T>(1,3)*z[36] + 2*z[101] + z[99] + 4*z[102] + z[94];
    z[31]=z[14]*z[31];
    z[36]=z[50] + z[25];
    z[94]= - 2*z[36] + z[47];
    z[94]=z[7]*z[94];
    z[97]=z[47] - z[78];
    z[97]=z[2]*z[97];
    z[94]=z[97] + z[94] - z[24] + 3*z[45];
    z[94]=z[7]*z[94];
    z[78]= - z[2]*z[78];
    z[97]=z[9]*z[43];
    z[97]= - z[25] + z[97];
    z[97]=z[13]*z[97];
    z[78]=z[97] + z[27] - z[24] + z[78];
    z[78]=z[13]*z[78];
    z[97]=z[34] - 2*z[26];
    z[97]=z[7]*z[97];
    z[97]= - z[52] + z[97];
    z[97]=z[7]*z[97];
    z[99]=z[34] + z[26];
    z[28]=z[14]*z[3]*z[99]*z[28];
    z[28]=z[28] + z[97] + z[29];
    z[28]=z[14]*z[28];
    z[29]=z[3]*z[26];
    z[28]=z[28] + z[78] + z[29] + z[94];
    z[28]=z[16]*z[28];
    z[29]=z[37]*z[61];
    z[37]= - z[9]*z[55];
    z[37]=z[37] - z[29];
    z[37]=z[7]*z[37];
    z[55]= - z[1] - z[100];
    z[55]=z[17]*z[55];
    z[37]=z[55] + z[37] - z[54] - z[89];
    z[37]=z[12]*z[37];
    z[54]=z[59] + z[54] - z[45];
    z[54]=z[2]*z[54];
    z[55]=z[4]*z[44];
    z[55]= - z[68] + z[55];
    z[55]=z[17]*z[55];
    z[78]=z[1] + z[44];
    z[55]=z[55] + 3*z[78];
    z[55]=z[4]*z[55];
    z[55]=static_cast<T>(3)- 2*z[68] + z[55];
    z[55]=z[17]*z[55];
    z[78]=z[45]*z[7];
    z[78]=z[78] - z[9];
    z[37]=z[37] + z[55] - 3*z[78] + z[54];
    z[37]=z[20]*z[37];
    z[28]=z[28] + z[37];
    z[37]=z[50] - z[25];
    z[54]=z[2]*z[46];
    z[54]=z[54] - z[7];
    z[54]=z[37]*z[54];
    z[55]=z[4]*z[25];
    z[54]=z[55] - z[34] - z[89] + z[54];
    z[54]=z[54]*z[57];
    z[55]= - z[56] + z[89];
    z[55]=z[55]*z[87];
    z[55]=z[91] + z[55];
    z[55]=z[55]*z[51];
    z[55]=z[55] - 17*z[34] + n<T>(62,3)*z[45];
    z[55]=z[2]*z[55];
    z[91]=27*z[34] + n<T>(8,3)*z[45];
    z[91]=z[91]*z[51];
    z[94]=z[34] + n<T>(13,3)*z[45];
    z[94]=z[6]*z[94];
    z[24]= - z[24] + z[71];
    z[24]=z[4]*z[24];
    z[24]=z[54] - z[38] + 10*z[24] + 2*z[94] + z[55] + z[91] - 77*z[1]
    - 5*z[9];
    z[24]=z[13]*z[24];
    z[52]=z[52] - 4*z[100];
    z[52]=z[52]*z[53];
    z[53]= - z[6]*z[90];
    z[54]= - z[4]*z[71];
    z[53]=z[54] + z[1] + z[53];
    z[53]=z[53]*z[57];
    z[54]=z[100]*z[6];
    z[52]=z[53] + z[52] - 10*z[54] - 7*z[84] + n<T>(73,3) + 12*z[68];
    z[52]=z[13]*z[52];
    z[53]=z[3]*z[4];
    z[55]= - z[1] + z[100];
    z[55]=z[55]*z[53];
    z[54]= - z[84] + z[54];
    z[54]=z[13]*z[54];
    z[54]=z[55] + z[54];
    z[54]=z[54]*z[57];
    z[55]=5*z[4];
    z[91]= - z[6]*z[68]*z[55];
    z[94]= - z[44]*z[83];
    z[94]= - 3*z[68] + z[94];
    z[53]=z[94]*z[53];
    z[53]=z[54] + z[91] + z[53];
    z[53]=z[17]*z[53];
    z[54]=2*z[84] - static_cast<T>(4)- n<T>(5,3)*z[68];
    z[91]=n<T>(8,3)*z[4];
    z[94]= - z[43]*z[91];
    z[94]=z[94] + 18*z[1] + 17*z[44];
    z[94]=z[4]*z[94];
    z[54]=4*z[54] + z[94];
    z[54]=z[3]*z[54];
    z[94]=3*z[6];
    z[69]= - static_cast<T>(2)- z[69];
    z[69]=z[69]*z[94];
    z[94]= - 15*z[4] + z[98];
    z[94]=z[44]*z[94];
    z[94]= - static_cast<T>(5)- n<T>(4,3)*z[68] + z[94];
    z[97]=2*z[4];
    z[94]=z[94]*z[97];
    z[52]=z[53] + z[52] + z[54] + z[94] + n<T>(92,3)*z[2] + z[69];
    z[52]=z[17]*z[52];
    z[53]= - z[61]*z[43]*z[77];
    z[22]=z[17]*z[9]*z[71]*z[22];
    z[22]=z[53] + z[22];
    z[22]=z[17]*z[22];
    z[53]= - z[13]*z[72];
    z[54]=z[17]*z[25]*z[93];
    z[53]=z[53] + z[54];
    z[23]=z[53]*z[23];
    z[48]=z[48]*z[77];
    z[22]=z[23] + z[48] + z[22];
    z[22]=z[10]*z[22];
    z[23]=z[100]*z[61];
    z[48]= - z[29] + n<T>(1,3)*z[23];
    z[48]=z[48]*z[83];
    z[53]=z[44]*z[6];
    z[54]=z[13]*z[103];
    z[54]=z[54] + 3*z[53];
    z[54]=z[61]*z[54];
    z[22]=z[22] + z[48] + z[54];
    z[48]=z[61]*z[44];
    z[48]=z[29] - n<T>(19,3)*z[48];
    z[54]= - z[55]*z[80];
    z[48]=z[54] + 4*z[48] - 5*z[23];
    z[48]=z[12]*z[48];
    z[54]= - z[34]*z[57];
    z[38]=z[54] + 7*z[74] + z[38];
    z[38]=z[13]*z[38];
    z[53]=4*z[53] + z[64];
    z[38]=4*z[53] + z[38];
    z[38]=z[17]*z[38];
    z[53]=z[50]*z[6];
    z[54]= - n<T>(1,3)*z[53] + 6*z[60];
    z[33]=z[54]*z[33];
    z[53]=z[2]*z[53];
    z[33]=z[38] - n<T>(22,3)*z[53] + z[33];
    z[33]=z[17]*z[33];
    z[38]=z[74] + z[100];
    z[38]=z[17]*z[38];
    z[53]=z[4]*z[50];
    z[38]=z[38] + z[53] + z[60];
    z[38]=z[17]*z[38];
    z[23]=n<T>(4,3)*z[23] + z[38];
    z[23]=z[23]*z[75];
    z[22]=z[48] + z[23] + z[33] + 2*z[22];
    z[22]=z[22]*z[42];
    z[23]= - 3*z[47] - z[41] + n<T>(19,3)*z[50];
    z[23]=z[23]*z[51];
    z[33]=85*z[34] - 62*z[45];
    z[23]=n<T>(1,3)*z[33] + z[23];
    z[23]=z[7]*z[23];
    z[33]=19*z[25] + 32*z[50];
    z[33]=n<T>(1,3)*z[33] - 6*z[47];
    z[33]=z[7]*z[33];
    z[38]=15*z[34] + 11*z[45];
    z[33]=2*z[38] + z[33];
    z[33]=z[2]*z[33];
    z[37]=z[7]*z[37];
    z[37]= - z[89] + z[37];
    z[38]=z[46]*z[79];
    z[47]=z[1]*z[61];
    z[38]=z[47] + z[38];
    z[38]=z[2]*z[38];
    z[37]=n<T>(1,3)*z[37] + z[38];
    z[37]=z[6]*z[37];
    z[38]= - 6*z[1] + 7*z[9];
    z[23]=16*z[37] + z[33] + 2*z[38] + z[23];
    z[23]=z[6]*z[23];
    z[33]=z[73] + n<T>(17,3)*z[45];
    z[37]= - z[25] + z[76];
    z[37]=z[37]*z[49];
    z[33]=2*z[33] + z[37];
    z[33]=z[33]*z[51];
    z[37]=20*z[34] - 17*z[45];
    z[38]= - n<T>(1,3)*z[34] - z[89];
    z[38]=z[38]*z[46];
    z[37]=n<T>(1,3)*z[37] + 2*z[38];
    z[37]=z[37]*z[70];
    z[29]= - n<T>(11,3)*z[29] - z[88] - 15*z[50];
    z[29]=z[2]*z[29];
    z[38]=z[39]*z[61];
    z[39]= - z[45] - n<T>(2,3)*z[38];
    z[29]=11*z[39] + z[29];
    z[29]=z[29]*z[97];
    z[39]= - z[41] + z[92];
    z[39]=z[7]*z[39];
    z[39]=z[34] + z[39];
    z[39]=2*z[39] - z[43];
    z[39]=z[39]*z[98];
    z[29]=z[29] + z[39] + z[37] + z[33] + 10*z[1] - n<T>(71,3)*z[9];
    z[29]=z[4]*z[29];
    z[33]= - z[43]*z[87];
    z[37]=z[9]*z[25];
    z[37]=z[81] + z[37];
    z[37]=z[37]*z[55];
    z[33]=z[37] - 3*z[25] + z[33];
    z[33]=z[13]*z[33];
    z[37]= - z[86] + z[50];
    z[37]=z[2]*z[37];
    z[36]=z[4]*z[36];
    z[27]=2*z[33] + z[27] - 20*z[36] + z[34] + z[37];
    z[27]=z[13]*z[27];
    z[33]= - 2*z[38] - n<T>(10,3)*z[34] - 7*z[45];
    z[33]=z[7]*z[33];
    z[36]= - 25*z[34] - 73*z[45];
    z[36]=z[2]*z[36];
    z[37]=z[82] + 23*z[9];
    z[33]=n<T>(1,3)*z[36] + n<T>(2,3)*z[37] + z[33];
    z[36]=z[61]*z[62];
    z[36]=2*z[76] + z[36];
    z[36]=z[36]*z[51];
    z[36]=z[36] + z[73] + z[85];
    z[36]=z[36]*z[55];
    z[37]= - z[17]*z[13];
    z[37]=z[37] + 1;
    z[37]=z[34]*z[37];
    z[26]= - n<T>(4,3)*z[26] + z[37];
    z[26]=z[26]*z[75];
    z[37]= - z[17]*z[4]*z[40];
    z[26]=z[26] + z[37] + z[27] + 2*z[33] + z[36];
    z[26]=z[12]*z[26];
    z[27]=2*z[90] - z[59];
    z[27]=z[2]*z[27];
    z[33]=z[17]*z[44];
    z[33]= - z[65] - 2*z[33];
    z[36]=z[42]*z[17];
    z[33]=z[33]*z[36];
    z[37]=static_cast<T>(1)+ z[68];
    z[37]=z[17]*z[37];
    z[37]=z[37] - z[1];
    z[27]= - z[35] + z[33] + z[27] - z[78] + 4*z[37];
    z[27]=z[19]*z[27];
    z[33]=n<T>(1,3)*z[10];
    z[33]=z[33]*z[43]*npow(z[17],3);
    z[35]=npow(z[17],2);
    z[37]=z[34]*z[35];
    z[37]=z[37] - z[33];
    z[37]=z[37]*z[42];
    z[36]= - z[34]*z[36];
    z[38]=z[34]*z[42];
    z[38]= - z[84] + z[38];
    z[39]=n<T>(1,3)*z[14];
    z[38]=z[38]*z[39];
    z[36]=z[36] + z[38];
    z[36]=z[14]*z[36];
    z[38]=z[68]*z[17];
    z[38]= - z[63] - z[1] + n<T>(2,3)*z[38];
    z[38]=z[38]*z[17];
    z[36]=z[36] + z[38] + z[37];
    z[36]=z[19]*z[36];
    z[35]=z[35]*z[40];
    z[33]= - z[3]*z[33];
    z[33]=z[35] + z[33];
    z[33]=z[33]*z[42];
    z[35]= - z[17]*z[66];
    z[37]= - z[3]*z[84];
    z[37]=z[37] + z[66];
    z[37]=z[37]*z[39];
    z[35]=z[35] + z[37];
    z[35]=z[14]*z[35];
    z[37]=z[3]*z[38];
    z[33]=z[36] + z[35] + z[37] + z[33];
    z[33]=z[18]*z[33];
    z[35]=z[34] + z[58];
    z[35]=z[35]*z[51];
    z[25]=z[25] - n<T>(20,3)*z[92];
    z[25]=z[7]*z[25];
    z[25]= - z[56] + z[25];
    z[25]=z[6]*z[25];
    z[36]= - z[2]*z[95];
    z[36]=2*z[43] + z[36];
    z[36]=z[36]*z[91];
    z[25]=z[36] + z[25] - n<T>(8,3)*z[44] + z[1] + z[35];
    z[25]=z[3]*z[25];
    z[34]=z[45] + 4*z[34];
    z[34]=z[34]*z[67];
    z[34]=z[34] + n<T>(14,3)*z[1] - z[9];
    z[34]=z[7]*z[34];
    z[35]= - 49*z[1] - 26*z[9];
    z[35]=n<T>(1,3)*z[35] + 4*z[96];
    z[35]=z[35]*z[70];

    r +=  - n<T>(35,3) + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + 20.
      /3.*z[27] + 6*z[28] + z[29] + 2*z[30] + z[31] + z[32] + 20*z[33]
       + 4*z[34] + z[35] + z[52];
 
    return r;
}

template double qg_2lNLC_r1027(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1027(const std::array<dd_real,31>&);
#endif
