#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r370(const std::array<T,31>& k) {
  T z[100];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[12];
    z[4]=k[15];
    z[5]=k[10];
    z[6]=k[2];
    z[7]=k[3];
    z[8]=k[6];
    z[9]=k[9];
    z[10]=k[4];
    z[11]=k[7];
    z[12]=k[5];
    z[13]=k[14];
    z[14]=k[13];
    z[15]=2*z[11];
    z[16]=npow(z[13],2);
    z[17]= - z[16]*z[15];
    z[18]=z[5]*z[3];
    z[19]=npow(z[3],2);
    z[20]=3*z[19];
    z[21]=z[20] + n<T>(1,2)*z[18];
    z[21]=z[5]*z[21];
    z[22]=npow(z[5],2);
    z[23]=3*z[22];
    z[24]=npow(z[6],2);
    z[25]=z[23]*z[24];
    z[26]=npow(z[8],3);
    z[27]= - z[26]*z[25];
    z[28]=n<T>(3,2)*z[22];
    z[29]=z[26]*z[10];
    z[30]=z[6]*z[29]*z[28];
    z[17]=z[30] + z[27] + z[17] + z[21];
    z[17]=z[10]*z[17];
    z[21]=npow(z[9],2);
    z[27]=z[24]*z[21];
    z[30]=n<T>(1,2) - 3*z[27];
    z[30]=z[5]*z[30];
    z[31]=11*z[3];
    z[32]=z[21]*z[6];
    z[30]=z[30] + z[31] + 6*z[9] + z[32];
    z[30]=z[5]*z[30];
    z[33]=npow(z[6],3);
    z[34]=z[33]*z[26]*z[28];
    z[35]=2*z[16];
    z[36]=z[6]*z[9];
    z[37]=z[35]*z[36];
    z[38]=4*z[3];
    z[39]=z[4]*z[6];
    z[40]= - static_cast<T>(3)+ z[39];
    z[40]=z[4]*z[40];
    z[40]= - z[38] + z[40];
    z[40]=z[2]*z[40];
    z[41]=3*z[16];
    z[42]=2*z[9];
    z[43]=z[13] + z[42];
    z[43]=z[9]*z[43];
    z[44]=n<T>(23,2)*z[3];
    z[45]= - z[13] + z[44];
    z[45]=z[11]*z[45];
    z[46]= - 19*z[3] - 7*z[5];
    z[46]=z[4]*z[46];
    z[17]=z[40] + z[17] + z[34] + n<T>(1,2)*z[46] + z[30] + z[45] + z[19] - 
    z[37] + z[41] + z[43];
    z[17]=z[2]*z[17];
    z[30]=npow(z[11],2);
    z[34]=4*z[30];
    z[40]=npow(z[14],2);
    z[43]=3*z[40];
    z[45]=z[34] + z[43];
    z[46]=npow(z[10],2);
    z[47]=z[46]*z[26];
    z[48]=z[45]*z[47];
    z[49]=14*z[11];
    z[50]=3*z[3];
    z[51]= - z[50] + z[49];
    z[51]=z[51]*z[47];
    z[52]=9*z[3];
    z[53]=2*z[4];
    z[54]= - z[52] + z[53];
    z[54]=z[4]*z[54];
    z[55]=z[11]*z[3];
    z[51]=2*z[51] + z[54] + 7*z[55];
    z[51]=z[2]*z[51];
    z[54]=z[19]*z[15];
    z[56]= - z[3]*z[53];
    z[56]= - z[20] + z[56];
    z[56]=z[4]*z[56];
    z[48]=z[51] + z[48] + z[54] + z[56];
    z[48]=z[2]*z[48];
    z[51]=npow(z[4],2);
    z[54]=2*z[3];
    z[56]=npow(z[9],3);
    z[57]=z[56]*z[51]*z[54];
    z[58]=z[19]*z[11];
    z[59]= - z[26]*z[58];
    z[57]=z[57] + z[59];
    z[59]=2*z[19];
    z[60]=z[11] - z[3];
    z[61]=z[2]*z[60];
    z[61]=22*z[61] - z[59] - n<T>(31,2)*z[55];
    z[61]=z[2]*z[26]*z[61];
    z[57]=2*z[57] + z[61];
    z[57]=z[12]*z[57];
    z[61]=4*z[16];
    z[62]=z[26]*z[39]*z[61];
    z[63]=5*z[21];
    z[64]=z[3]*z[63];
    z[65]=z[11]*z[21];
    z[64]=z[64] + z[65];
    z[64]=z[64]*z[51];
    z[62]=z[64] + z[62];
    z[64]=z[51]*z[50];
    z[65]=11*z[11];
    z[52]=z[52] - z[65];
    z[52]=z[52]*z[29];
    z[52]=z[64] + 2*z[52];
    z[52]=z[2]*z[52];
    z[64]=2*z[40];
    z[66]=z[20] - z[64];
    z[67]=z[66]*z[29];
    z[52]=z[67] + z[52];
    z[52]=z[2]*z[52];
    z[52]=z[57] + 2*z[62] + z[52];
    z[52]=z[12]*z[52];
    z[57]=2*z[30];
    z[62]=z[4]*z[11];
    z[67]=z[57] - z[62];
    z[47]=z[67]*z[47];
    z[67]=z[50] + z[15];
    z[67]=z[21]*z[67];
    z[68]=z[15]*z[9];
    z[69]=z[9]*z[50];
    z[69]=z[69] + z[68];
    z[69]=z[69]*z[53];
    z[47]=z[47] + z[69] + z[67];
    z[47]=z[4]*z[47];
    z[61]= - z[24]*z[61];
    z[67]=z[24]*z[4];
    z[69]=z[13]*z[67];
    z[61]=z[61] + z[69];
    z[61]=z[61]*z[26]*z[53];
    z[69]=z[13]*z[14];
    z[69]=z[69] - z[64];
    z[70]=2*z[13];
    z[71]= - z[69]*z[70];
    z[72]=z[16]*z[9];
    z[71]=z[71] + z[72];
    z[71]=z[3]*z[71];
    z[47]=z[52] + z[48] + z[61] + z[71] + z[47];
    z[47]=z[12]*z[47];
    z[18]= - z[19] - n<T>(3,2)*z[18];
    z[48]=z[4]*z[3];
    z[52]= - z[38] + z[4];
    z[52]=z[2]*z[52];
    z[18]=z[52] + 3*z[18] - z[48];
    z[18]=z[4]*z[18];
    z[52]=8*z[6];
    z[61]=z[52]*z[56];
    z[71]=z[24]*z[5];
    z[73]=z[56]*z[71];
    z[73]=4*z[73] - z[61] + z[19];
    z[73]=z[5]*z[73];
    z[74]=4*z[21];
    z[75]= - z[16] + z[74];
    z[75]=z[9]*z[75];
    z[76]=z[4]*z[5];
    z[77]=z[76]*z[19];
    z[78]=z[2]*z[3]*z[51];
    z[78]= - z[77] + z[78];
    z[78]=z[1]*z[78];
    z[79]=z[16]*z[11];
    z[18]=z[78] + z[73] + z[75] + z[79] + z[18];
    z[18]=z[2]*z[18];
    z[73]=2*z[27];
    z[75]=npow(z[7],2);
    z[78]=z[75]*z[73];
    z[80]= - z[4]*npow(z[10],4)*z[15];
    z[78]=z[80] + z[78];
    z[80]=npow(z[8],4);
    z[78]=z[80]*z[22]*z[78];
    z[81]=z[2]*z[55];
    z[81]=z[58] + 11*z[81];
    z[82]=2*z[2];
    z[80]=npow(z[12],3)*z[82]*z[80]*z[81];
    z[81]=npow(z[2],2);
    z[51]=z[81]*z[51];
    z[83]=z[50]*z[51];
    z[80]=z[83] + z[80];
    z[80]=z[12]*z[80];
    z[69]=z[69]*z[13];
    z[83]= - z[3]*z[69];
    z[18]=z[80] + z[83] + z[77] + z[78] + z[18];
    z[18]=z[1]*z[18];
    z[77]=7*z[3];
    z[78]= - z[77] + z[15];
    z[78]=z[5]*z[78];
    z[80]=z[60]*z[53];
    z[78]=z[80] + z[19] + z[78];
    z[78]=z[5]*z[78];
    z[80]=z[53]*z[71];
    z[25]= - z[25] + z[80];
    z[25]=z[25]*z[26];
    z[25]=z[25] + z[78];
    z[25]=z[4]*z[25];
    z[78]=z[7]*z[30];
    z[80]=z[11]*z[7];
    z[83]=z[5]*z[80];
    z[78]=z[78] + z[83];
    z[78]=z[5]*z[78];
    z[83]=z[6]*z[22];
    z[84]=z[5]*z[6];
    z[85]= - z[4]*z[84];
    z[83]=z[83] + z[85];
    z[83]=z[83]*z[53];
    z[78]=z[78] + z[83];
    z[78]=z[78]*z[26];
    z[83]=z[11] + z[5];
    z[83]=z[5]*z[83];
    z[85]=2*z[5];
    z[86]=z[85] - z[11];
    z[87]=z[4]*z[86];
    z[83]=z[87] + z[57] + z[83];
    z[83]=z[4]*z[83];
    z[22]= - z[22]*z[15];
    z[22]=z[22] + z[83];
    z[22]=z[22]*z[29];
    z[22]=2*z[78] + z[22];
    z[22]=z[10]*z[22];
    z[29]=z[16]*z[3];
    z[78]=z[29] + 8*z[79];
    z[78]=z[5]*z[78];
    z[22]=z[22] + z[78] + z[25];
    z[22]=z[10]*z[22];
    z[25]=5*z[9];
    z[78]= - z[25] + z[50];
    z[78]=z[3]*z[78];
    z[83]= - z[3] + z[15];
    z[83]=z[83]*z[53];
    z[87]=4*z[11];
    z[88]= - n<T>(21,2)*z[3] - z[87];
    z[88]=z[5]*z[88];
    z[78]=z[83] + z[88] + z[78] + z[68];
    z[78]=z[4]*z[78];
    z[83]= - z[14]*z[38];
    z[83]=z[83] + 7*z[16] + 2*z[21];
    z[83]=z[11]*z[83];
    z[88]=3*z[6];
    z[89]=z[56]*z[88];
    z[89]= - z[21] + z[89];
    z[90]=z[6]*z[56];
    z[90]=z[63] - 6*z[90];
    z[91]=2*z[84];
    z[90]=z[90]*z[91];
    z[92]=z[3]*z[13];
    z[89]=z[90] + 4*z[89] + z[92];
    z[89]=z[5]*z[89];
    z[27]= - z[7]*z[27];
    z[90]=z[42]*z[75];
    z[93]= - z[84]*z[90];
    z[27]=z[27] + z[93];
    z[26]=z[27]*z[26]*z[85];
    z[27]=z[9] - z[14];
    z[93]=3*z[13];
    z[94]=z[93] + z[27];
    z[94]=z[13]*z[94];
    z[64]= - z[64] + z[94];
    z[64]=z[3]*z[64];
    z[17]=z[18] + z[47] + z[17] + z[22] + z[26] + z[78] + z[89] + z[83]
    + z[64] - z[69] + z[72];
    z[17]=z[1]*z[17];
    z[18]=npow(z[8],2);
    z[22]=z[18]*z[10];
    z[26]= - static_cast<T>(4)+ z[39];
    z[26]=z[4]*z[26];
    z[47]= - z[6]*z[18];
    z[26]= - 6*z[22] + z[47] + z[26] - 16*z[3] + 23*z[11];
    z[26]=z[2]*z[26];
    z[47]=z[53]*z[6];
    z[64]= - z[18]*z[47];
    z[72]=9*z[11];
    z[38]=z[72] + 3*z[14] - z[38];
    z[38]=z[38]*z[22];
    z[78]=n<T>(59,2)*z[3] + z[87];
    z[78]=z[11]*z[78];
    z[26]=z[26] + z[38] + z[64] - n<T>(25,2)*z[48] + z[78] - z[20] + 11*
    z[21] + z[61];
    z[26]=z[2]*z[26];
    z[20]=z[11]*z[20];
    z[20]= - 2*z[56] + z[20];
    z[38]= - z[19] - z[48];
    z[38]=z[4]*z[38];
    z[61]= - n<T>(35,2)*z[11] - 7*z[14] + 29*z[3];
    z[61]=z[61]*z[18];
    z[64]= - 6*z[3] + z[4];
    z[64]=z[4]*z[64];
    z[64]=22*z[55] + z[64];
    z[64]=z[2]*z[64];
    z[20]=z[64] + z[61] + 2*z[20] + z[38];
    z[20]=z[2]*z[20];
    z[38]=z[42]*z[16];
    z[38]=z[38] - z[69];
    z[61]=z[3]*z[38];
    z[63]=z[63] - z[68];
    z[63]=z[63]*z[53];
    z[64]=z[3]*z[21];
    z[63]=z[63] - 12*z[56] - 25*z[64];
    z[63]=z[4]*z[63];
    z[64]=8*z[13];
    z[68]= - z[4]*z[64];
    z[66]=z[68] - 3*z[55] + z[66];
    z[66]=z[66]*z[18];
    z[56]=z[56]*z[48];
    z[51]=z[3]*z[51];
    z[51]= - 8*z[56] + z[51];
    z[51]=z[12]*z[51];
    z[20]=z[51] + z[20] + z[66] + z[61] + z[63];
    z[20]=z[12]*z[20];
    z[51]=5*z[7];
    z[56]=z[40]*z[51];
    z[61]=z[16]*z[52];
    z[63]=n<T>(5,2)*z[7];
    z[66]=z[55]*z[63];
    z[68]=2*z[6];
    z[78]=4*z[13];
    z[83]=z[78] - z[9];
    z[83]=z[83]*z[68];
    z[83]=z[83] - z[39];
    z[83]=z[4]*z[83];
    z[56]=z[83] + z[66] + z[56] + z[61];
    z[56]=z[56]*z[18];
    z[61]=3*z[9];
    z[66]=z[61] - z[11];
    z[83]= - z[66]*z[15];
    z[89]= - 39*z[9] + z[54];
    z[89]=z[3]*z[89];
    z[94]=z[42] - z[11];
    z[94]=z[4]*z[94];
    z[83]=9*z[94] + z[83] - 8*z[21] + z[89];
    z[83]=z[4]*z[83];
    z[89]= - z[40] + z[41];
    z[94]= - z[70] - z[61];
    z[94]=z[9]*z[94];
    z[89]=2*z[89] + z[94];
    z[89]=z[3]*z[89];
    z[59]=z[40] - z[59];
    z[59]=z[11]*z[59];
    z[94]=4*z[40];
    z[95]= - z[15] + z[4];
    z[95]=z[4]*z[95];
    z[95]=z[94] + z[95];
    z[95]=z[95]*z[22];
    z[20]=z[20] + z[26] + 2*z[95] + z[56] + z[83] + z[59] + z[89] + 
    z[38];
    z[20]=z[12]*z[20];
    z[26]= - 13*z[5] - n<T>(13,2)*z[3] + z[87];
    z[26]=z[5]*z[26];
    z[38]= - z[72] + z[5];
    z[38]=z[4]*z[38];
    z[26]=z[38] + z[26] + z[19] + z[57];
    z[26]=z[4]*z[26];
    z[38]=z[5]*z[16]*z[87];
    z[56]= - z[15]*z[76];
    z[59]= - z[11]*z[23];
    z[56]=z[59] + z[56];
    z[56]=z[4]*z[56];
    z[38]=z[38] + z[56];
    z[56]= - z[86]*z[85];
    z[59]=z[4] - z[86];
    z[59]=z[4]*z[59];
    z[45]=5*z[59] + z[56] - z[45];
    z[45]=z[45]*z[18];
    z[38]=2*z[38] + z[45];
    z[38]=z[10]*z[38];
    z[41]=z[41] - z[92];
    z[45]= - z[11]*z[64];
    z[56]= - z[5]*z[15];
    z[41]=z[56] + 3*z[41] + z[45];
    z[41]=z[5]*z[41];
    z[45]=3*z[7];
    z[56]= - z[40]*z[45];
    z[59]=z[15]*z[7];
    z[72]=2*z[7];
    z[83]= - z[72] + 7*z[6];
    z[83]=z[5]*z[83];
    z[83]= - z[59] + z[83];
    z[83]=z[5]*z[83];
    z[56]=z[56] + z[83];
    z[83]=6*z[84];
    z[86]=z[83] - z[39];
    z[86]=z[4]*z[86];
    z[56]=2*z[56] + 3*z[86];
    z[56]=z[56]*z[18];
    z[86]=z[16]*z[54];
    z[26]=z[38] + z[56] + z[26] + z[41] + 9*z[79] + z[69] + z[86];
    z[26]=z[10]*z[26];
    z[38]=4*z[5];
    z[41]=n<T>(5,2)*z[14] - z[38];
    z[41]=z[41]*z[18];
    z[41]=z[41] - z[69] + z[79];
    z[41]=z[10]*z[41];
    z[56]=z[18]*z[84];
    z[69]=9*z[19];
    z[79]=5*z[11];
    z[86]=z[13]*z[79];
    z[89]=z[5]*z[77];
    z[41]=z[41] + n<T>(17,2)*z[56] + z[89] + z[86] - z[16] + z[69];
    z[41]=z[10]*z[41];
    z[56]=z[74] + z[16];
    z[86]=z[56]*z[36];
    z[89]= - z[70] - 13*z[9];
    z[89]=z[9]*z[89];
    z[35]= - z[86] + z[35] + z[89];
    z[35]=z[6]*z[35];
    z[33]=z[33]*z[5];
    z[89]=z[24] - z[33];
    z[89]=z[4]*z[89];
    z[92]= - z[6] - z[71];
    z[89]=2*z[92] + z[89];
    z[89]=z[4]*z[89];
    z[92]=z[18]*z[88];
    z[92]=z[54] + z[92];
    z[92]=2*z[92] - 11*z[22];
    z[92]=z[10]*z[92];
    z[89]=z[92] + z[89] - static_cast<T>(11)- z[84];
    z[89]=z[2]*z[89];
    z[92]=11*z[9];
    z[95]=2*z[32];
    z[96]=z[92] + z[95];
    z[96]=z[6]*z[96];
    z[91]=z[91] + n<T>(11,2) + z[96];
    z[91]=z[5]*z[91];
    z[96]= - static_cast<T>(1)- z[84];
    z[97]=3*z[71];
    z[98]=z[6] - z[97];
    z[98]=z[4]*z[98];
    z[96]=7*z[96] + z[98];
    z[96]=z[4]*z[96];
    z[98]= - n<T>(9,2)*z[71] + z[67];
    z[98]=z[98]*z[18];
    z[27]= - z[13] - z[27];
    z[27]=z[89] + z[41] + z[98] + z[96] + z[91] + n<T>(67,2)*z[11] + 22*z[3]
    + 4*z[27] + z[35];
    z[27]=z[2]*z[27];
    z[35]=z[36]*z[45];
    z[41]=z[7] - 4*z[6];
    z[41]=z[6]*z[41];
    z[41]=z[75] + z[41];
    z[41]=z[5]*z[41];
    z[35]=z[35] + z[41];
    z[35]=z[35]*z[85];
    z[41]=z[64] + z[9];
    z[89]=z[41]*z[24];
    z[89]= - z[67] + 2*z[89] - 9*z[71];
    z[89]=z[4]*z[89];
    z[91]= - z[75]*z[43];
    z[35]=z[89] + z[35] + z[91] + z[73];
    z[35]=z[35]*z[18];
    z[73]=z[9]*z[7];
    z[89]=z[73] - 1;
    z[91]=z[89]*z[54];
    z[96]=z[9]*z[45];
    z[96]=static_cast<T>(2)+ z[96];
    z[96]=z[96]*z[32];
    z[98]= - static_cast<T>(2)- 5*z[73];
    z[98]=z[9]*z[98];
    z[96]=z[98] + z[96];
    z[96]=z[96]*z[68];
    z[96]=3*z[89] + z[96];
    z[96]=z[96]*z[85];
    z[98]=z[54]*z[7];
    z[99]=static_cast<T>(5)- z[98];
    z[99]=z[11]*z[99];
    z[91]=z[96] + z[99] + z[91] + 17*z[32] + z[13] - z[9];
    z[91]=z[5]*z[91];
    z[96]= - z[61] - 5*z[14] - z[78];
    z[99]= - z[7]*z[42];
    z[99]= - static_cast<T>(5)+ z[99];
    z[99]=z[3]*z[99];
    z[96]= - z[15] + 2*z[96] + z[99];
    z[96]=z[11]*z[96];
    z[99]= - z[70] - z[25];
    z[99]=z[9]*z[99];
    z[73]= - static_cast<T>(15)- 4*z[73];
    z[73]=z[9]*z[73];
    z[54]=z[54] + z[73] - 5*z[13];
    z[54]=z[3]*z[54];
    z[41]=6*z[4] - n<T>(77,2)*z[5] - z[15] - n<T>(37,2)*z[3] + z[41];
    z[41]=z[4]*z[41];
    z[17]=z[17] + z[20] + z[27] + z[26] + z[35] + z[41] + z[91] + z[96]
    + z[54] + z[37] + z[99] - z[43] + 25*z[16];
    z[17]=z[1]*z[17];
    z[20]= - z[4] - 26*z[3] + 29*z[11];
    z[20]=z[2]*z[20];
    z[19]=z[20] - n<T>(15,2)*z[48] + n<T>(41,2)*z[55] + z[74] - 5*z[19];
    z[19]=z[2]*z[19];
    z[20]= - z[66]*z[53];
    z[26]= - z[9]*z[77];
    z[20]=z[20] + z[74] + z[26];
    z[20]=z[4]*z[20];
    z[26]=z[56]*z[9];
    z[27]=z[3]*z[26];
    z[35]=29*z[55] - z[48];
    z[35]=z[2]*z[35];
    z[35]=6*z[58] + z[35];
    z[35]=z[2]*z[35];
    z[27]=z[27] + z[35];
    z[27]=z[12]*z[27];
    z[35]= - z[13] + z[25];
    z[35]=z[35]*z[61];
    z[37]=z[14] + z[93];
    z[37]=z[13]*z[37];
    z[35]=z[37] + z[35];
    z[35]=z[3]*z[35];
    z[37]=z[14] - z[3];
    z[37]=z[37]*z[55];
    z[19]=z[27] + z[19] + z[20] + 4*z[37] + z[26] + z[35];
    z[19]=z[12]*z[19];
    z[20]= - z[16] - z[21];
    z[20]=z[20]*z[52];
    z[21]= - static_cast<T>(7)+ 3*z[36];
    z[21]=z[21]*z[53];
    z[26]=12*z[11];
    z[20]=z[21] + z[26] - z[77] + z[20] + z[64] - 15*z[9];
    z[20]=z[4]*z[20];
    z[21]=z[49] + z[31] - z[95] - z[14] - z[61];
    z[27]=z[69] - 8*z[30];
    z[27]=z[10]*z[27];
    z[30]=14*z[3] - 33*z[11];
    z[30]=z[10]*z[30];
    z[30]=z[30] + static_cast<T>(2)- z[39];
    z[30]=z[2]*z[30];
    z[35]=n<T>(17,2)*z[4];
    z[21]=z[30] + z[27] + 2*z[21] - z[35];
    z[21]=z[2]*z[21];
    z[27]= - z[93] + z[92];
    z[27]=z[9]*z[27];
    z[30]=z[75]*z[9];
    z[37]=z[51] + z[30];
    z[41]= - z[37]*z[42];
    z[41]= - static_cast<T>(3)+ z[41];
    z[41]=z[9]*z[41];
    z[41]= - z[93] + z[41];
    z[43]=5*z[3];
    z[41]=2*z[41] + z[43];
    z[41]=z[3]*z[41];
    z[48]= - z[57] + 3*z[62];
    z[48]=z[48]*z[53];
    z[29]=z[29] + z[48];
    z[29]=z[10]*z[29];
    z[48]=z[14] + z[70];
    z[48]=z[13]*z[48];
    z[51]= - 17*z[3] + n<T>(11,2)*z[14] + z[70];
    z[51]=z[11]*z[51];
    z[19]=z[19] + z[21] + z[29] + z[20] + z[51] + z[41] + z[86] + z[27]
    - z[94] + z[48];
    z[19]=z[12]*z[19];
    z[20]=6*z[11];
    z[21]=5*z[5];
    z[27]=z[20] - z[21];
    z[27]=z[4]*z[27];
    z[21]= - z[11] + z[21];
    z[21]=z[5]*z[21];
    z[21]=z[27] - z[34] + z[21];
    z[21]=z[4]*z[21];
    z[27]= - z[13]*z[87];
    z[29]=z[5]*z[11];
    z[27]=z[27] + z[29];
    z[27]=z[27]*z[38];
    z[23]=z[23] + z[76];
    z[23]=z[10]*z[53]*z[11]*z[23];
    z[21]=z[23] + z[27] + z[21];
    z[21]=z[10]*z[21];
    z[23]= - n<T>(1,2) - z[59];
    z[23]=z[11]*z[23];
    z[27]=n<T>(1,2)*z[80];
    z[29]=static_cast<T>(8)- z[27];
    z[29]=z[5]*z[29];
    z[23]=z[29] + 7*z[13] + z[23];
    z[23]=z[5]*z[23];
    z[29]=n<T>(5,2) - z[84];
    z[29]=z[5]*z[29];
    z[34]= - static_cast<T>(11)+ z[84];
    z[34]=z[4]*z[34];
    z[29]=z[34] + 11*z[29] - n<T>(5,2)*z[3] - z[11];
    z[29]=z[4]*z[29];
    z[34]= - z[50] - z[14] + 16*z[13];
    z[34]=z[13]*z[34];
    z[20]=z[20] + n<T>(13,2)*z[14] - 6*z[13];
    z[20]=z[11]*z[20];
    z[20]=z[21] + z[29] + z[23] + z[20] + z[94] + z[34];
    z[20]=z[10]*z[20];
    z[21]= - 3*z[24] + z[33];
    z[21]=z[4]*z[21];
    z[23]=14*z[6];
    z[21]=z[21] - z[23] + 5*z[71];
    z[21]=z[4]*z[21];
    z[29]=z[14]*z[70];
    z[34]= - z[13]*z[15];
    z[29]=z[34] - z[40] + z[29];
    z[29]=z[10]*z[29];
    z[34]= - static_cast<T>(1)- n<T>(5,2)*z[84];
    z[34]=z[5]*z[34];
    z[29]=2*z[29] + z[34] - 30*z[11] + z[31] - 9*z[14] + z[13];
    z[29]=z[10]*z[29];
    z[31]= - z[36]*z[93];
    z[31]=z[31] + z[70] - z[9];
    z[31]=z[6]*z[31];
    z[34]=n<T>(3,2) + 8*z[36];
    z[34]=z[6]*z[34];
    z[34]=z[34] + n<T>(5,2)*z[71];
    z[34]=z[5]*z[34];
    z[36]= - z[10]*z[3];
    z[36]=static_cast<T>(7)+ z[36];
    z[36]=z[10]*z[36];
    z[36]=z[36] - z[6] + z[67];
    z[36]=z[36]*z[82];
    z[21]=z[36] + z[29] + z[21] + z[34] + n<T>(87,2) + z[31];
    z[21]=z[2]*z[21];
    z[29]=6*z[7];
    z[31]=z[29] + 5*z[30];
    z[31]=z[9]*z[31];
    z[31]= - static_cast<T>(6)+ z[31];
    z[30]=4*z[30];
    z[34]= - 7*z[7] - z[30];
    z[32]=z[34]*z[32];
    z[31]=2*z[31] + z[32];
    z[31]=z[6]*z[31];
    z[32]= - n<T>(3,2)*z[7] - z[90];
    z[31]=3*z[32] + z[31];
    z[31]=z[5]*z[31];
    z[32]=17*z[7];
    z[30]=z[32] + z[30];
    z[30]=z[9]*z[30];
    z[34]= - 9*z[7] - z[90];
    z[34]=z[34]*z[42];
    z[34]= - static_cast<T>(3)+ z[34];
    z[34]=z[9]*z[34];
    z[34]= - z[70] + z[34];
    z[34]=z[6]*z[34];
    z[27]=z[31] + z[27] + z[34] + static_cast<T>(9)+ z[30];
    z[27]=z[5]*z[27];
    z[25]= - z[93] + z[25];
    z[25]=z[9]*z[25];
    z[16]=z[86] + 16*z[16] + z[25];
    z[16]=z[6]*z[16];
    z[25]=20*z[13] + z[9];
    z[25]=z[6]*z[25];
    z[30]=z[4]*z[97];
    z[25]=z[30] - 24*z[84] - static_cast<T>(45)+ z[25];
    z[25]=z[4]*z[25];
    z[30]=z[40]*z[29];
    z[31]=z[9]*z[37];
    z[31]= - static_cast<T>(21)- 4*z[31];
    z[31]=z[9]*z[31];
    z[34]= - z[89]*z[43];
    z[36]=z[50]*z[7];
    z[37]= - static_cast<T>(31)+ z[36];
    z[37]=z[11]*z[37];
    z[16]=z[17] + z[19] + z[21] + z[20] + z[25] + z[27] + n<T>(1,2)*z[37] + 
    z[34] + z[16] + z[31] - 10*z[13] - n<T>(37,2)*z[14] + z[30];
    z[16]=z[1]*z[16];
    z[17]=z[63] - z[88];
    z[19]=3*z[5];
    z[20]=z[17]*z[19];
    z[21]=n<T>(3,2)*z[80];
    z[20]=z[20] + static_cast<T>(1)- z[21];
    z[20]=z[5]*z[20];
    z[25]=3*z[80];
    z[27]= - n<T>(3,2) + z[84];
    z[27]=z[5]*z[7]*z[27];
    z[27]=9*z[27] + static_cast<T>(22)- z[25];
    z[27]=z[8]*z[27];
    z[30]=17*z[11];
    z[20]=z[27] + z[35] + z[30] + z[20];
    z[20]=z[8]*z[20];
    z[27]=n<T>(5,2)*z[80];
    z[31]=static_cast<T>(3)- z[27];
    z[31]=z[5]*z[31];
    z[31]=z[87] + z[31];
    z[31]=z[5]*z[31];
    z[34]= - z[7]*z[85];
    z[21]=z[21] + z[34];
    z[21]=z[21]*z[19];
    z[21]= - z[65] + z[21];
    z[21]=z[8]*z[21];
    z[34]= - z[4]*z[87];
    z[21]=z[21] + z[31] + z[34];
    z[21]=z[8]*z[21];
    z[28]=z[22]*z[80]*z[28];
    z[21]=z[21] + z[28];
    z[21]=z[10]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[10]*z[20];
    z[21]=z[17]*z[84];
    z[21]= - 3*z[21] + z[45] - z[23];
    z[21]=z[5]*z[21];
    z[23]= - z[71]*z[72];
    z[28]=z[6]*z[7];
    z[23]=n<T>(9,2)*z[28] + z[23];
    z[23]=z[23]*z[19];
    z[23]=z[23] + z[29] - 11*z[6];
    z[23]=z[8]*z[23];
    z[21]=z[23] - n<T>(9,2)*z[39] + z[21] - n<T>(37,2) + z[59];
    z[21]=z[8]*z[21];
    z[20]=z[20] + z[21] - z[4] - 17*z[5] + 11*z[14] + z[26];
    z[20]=z[10]*z[20];
    z[21]=static_cast<T>(17)- z[98];
    z[21]=z[11]*z[21];
    z[23]= - z[13]*z[47];
    z[26]= - z[8]*z[25];
    z[21]=z[26] + z[23] - z[44] + z[21];
    z[21]=z[8]*z[21];
    z[23]=z[55]*z[18];
    z[26]=z[60]*z[8];
    z[29]=5*z[55] + z[26];
    z[29]=z[8]*z[29]*z[82];
    z[29]= - 7*z[23] + z[29];
    z[29]=z[2]*z[29];
    z[18]=z[12]*z[18]*z[15]*z[81]*z[3];
    z[18]=z[29] + z[18];
    z[18]=z[12]*z[18];
    z[29]=z[60]*z[22];
    z[31]= - z[29] + 5*z[26];
    z[31]=z[31]*z[82];
    z[34]= - 25*z[55] - 7*z[26];
    z[34]=z[8]*z[34];
    z[34]=z[34] + z[31];
    z[34]=z[2]*z[34];
    z[18]=z[18] + 8*z[23] + z[34];
    z[18]=z[12]*z[18];
    z[23]= - z[10]*z[31];
    z[23]=z[23] - 25*z[26] + 7*z[29];
    z[23]=z[2]*z[23];
    z[26]=static_cast<T>(8)- z[36];
    z[26]=z[11]*z[26];
    z[26]= - 8*z[3] + z[26];
    z[26]=z[8]*z[26];
    z[26]=17*z[55] + z[26];
    z[26]=z[8]*z[26];
    z[18]=z[18] + z[26] + z[23];
    z[18]=z[12]*z[18];
    z[23]= - z[50] + z[79];
    z[23]=z[8]*z[23];
    z[22]=z[22]*z[11];
    z[23]=z[23] - z[22];
    z[23]=z[23]*z[46]*z[82];
    z[26]= - n<T>(31,2)*z[3] + 25*z[11];
    z[26]=z[8]*z[26];
    z[26]=z[26] - 7*z[22];
    z[26]=z[10]*z[26];
    z[23]=z[26] + z[23];
    z[23]=z[2]*z[23];
    z[18]=z[18] + z[23] + z[21] - 8*z[22];
    z[18]=z[12]*z[18];
    z[15]=z[8]*z[15];
    z[15]= - z[62] + z[15];
    z[15]=z[10]*z[15];
    z[21]= - static_cast<T>(8)+ z[25];
    z[21]=z[8]*z[21];
    z[15]=4*z[15] + z[21] - z[30] + n<T>(5,2)*z[4];
    z[15]=z[10]*z[15];
    z[21]= - z[8]*z[45];
    z[15]=z[15] + z[21] + z[47] + n<T>(11,2) - z[59];
    z[15]=z[8]*z[15];
    z[21]= - 59*z[11] - 17*z[8];
    z[21]=z[8]*z[21];
    z[21]=z[21] + 17*z[22];
    z[21]=z[10]*z[21];
    z[21]=13*z[8] + n<T>(1,2)*z[21];
    z[21]=z[10]*z[21];
    z[23]=3*z[8];
    z[25]= - z[49] - z[23];
    z[25]=z[8]*z[25];
    z[25]=z[25] + 3*z[22];
    z[25]=z[10]*z[25];
    z[25]=7*z[8] + z[25];
    z[26]=z[46]*z[2];
    z[25]=z[25]*z[26];
    z[21]=z[25] + static_cast<T>(11)+ z[21];
    z[21]=z[2]*z[21];
    z[15]=z[18] + z[21] + 13*z[4] - z[11] + n<T>(7,2)*z[3] + z[14] - z[78]
    + z[15];
    z[15]=z[12]*z[15];
    z[18]= - n<T>(1,2)*z[7] + z[88];
    z[18]=z[18]*z[88];
    z[17]=z[17]*z[71];
    z[17]=z[18] + z[17];
    z[17]=z[5]*z[17];
    z[18]= - z[24]*z[45];
    z[21]=z[7]*z[33];
    z[18]=z[18] + z[21];
    z[21]=n<T>(1,2)*z[5];
    z[18]=z[18]*z[21];
    z[18]= - z[28] + z[18];
    z[18]=z[18]*z[23];
    z[17]=z[18] + z[17] - z[72] + n<T>(3,2)*z[6];
    z[17]=z[8]*z[17];
    z[18]=n<T>(9,2)*z[6];
    z[18]= - z[8]*z[18];
    z[18]=z[18] - n<T>(29,2) - z[83];
    z[18]=z[8]*z[18];
    z[19]=9*z[8] + n<T>(25,2)*z[11] + z[19];
    z[19]=z[8]*z[19];
    z[19]=z[19] - n<T>(9,2)*z[22];
    z[19]=z[10]*z[19];
    z[18]=z[18] + z[19];
    z[18]=z[10]*z[18];
    z[19]=z[68] + z[97];
    z[19]=z[8]*z[19];
    z[18]=z[18] - static_cast<T>(18)+ z[19];
    z[18]=z[10]*z[18];
    z[19]= - z[6] + z[10];
    z[19]=z[26]*z[8]*z[19];
    z[18]=3*z[19] + n<T>(27,2)*z[6] + z[18];
    z[18]=z[2]*z[18];
    z[19]=z[78] - z[61];
    z[19]=z[19]*z[68];
    z[21]=z[14]*z[32];
    z[21]= - static_cast<T>(13)+ z[21];
    z[22]= - z[72] + 15*z[6];
    z[22]=z[5]*z[22];

    r += z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + n<T>(1,2)*z[21] + 
      z[22] + z[27] - 22*z[39];
 
    return r;
}

template double qg_2lNLC_r370(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r370(const std::array<dd_real,31>&);
#endif
