#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r881(const std::array<T,31>& k) {
  T z[49];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[8];
    z[8]=k[12];
    z[9]=k[2];
    z[10]=k[5];
    z[11]=k[14];
    z[12]=k[3];
    z[13]=k[9];
    z[14]=k[13];
    z[15]=npow(z[5],2);
    z[16]=2*z[4];
    z[17]=z[15]*z[16];
    z[18]=z[17]*z[2];
    z[19]=npow(z[3],2);
    z[20]=5*z[19];
    z[21]=z[5]*z[4];
    z[21]= - z[18] + z[20] - 4*z[21];
    z[22]=2*z[2];
    z[21]=z[21]*z[22];
    z[23]=7*z[4];
    z[24]=6*z[5];
    z[25]=z[19]*z[9];
    z[26]=3*z[19];
    z[27]=z[10]*z[26];
    z[21]=z[21] - z[24] - z[23] - 5*z[25] + z[27];
    z[21]=z[2]*z[21];
    z[27]= - z[7] + 4*z[13];
    z[28]=z[10]*z[8];
    z[29]=z[28]*z[7];
    z[30]=z[29] - z[8] - z[27];
    z[30]=z[10]*z[30];
    z[31]=z[4]*z[10];
    z[32]=3*z[31];
    z[33]=z[9]*z[7];
    z[21]=z[21] - z[32] + z[30] - static_cast<T>(10)+ z[33];
    z[21]=z[6]*z[21];
    z[30]=npow(z[13],2);
    z[33]=z[28]*z[30];
    z[34]=4*z[11];
    z[34]= - z[25]*z[34];
    z[35]=12*z[13] - 7*z[7];
    z[35]=z[8]*z[35];
    z[34]=4*z[33] + z[34] + 6*z[30] + z[35];
    z[34]=z[10]*z[34];
    z[35]=3*z[4];
    z[36]= - z[19]*z[35];
    z[37]=z[16]*z[5];
    z[26]= - z[26] - z[37];
    z[26]=z[5]*z[26];
    z[26]=z[36] + z[26];
    z[26]=z[2]*z[26];
    z[36]= - z[19]*z[31];
    z[38]=4*z[8];
    z[39]= - z[5] + z[4] + z[38] + 3*z[25];
    z[39]=z[5]*z[39];
    z[26]=z[26] + z[36] + z[39];
    z[26]=z[2]*z[26];
    z[36]=npow(z[9],2);
    z[39]=z[36]*z[19];
    z[40]= - z[11]*z[39];
    z[41]=2*z[11];
    z[40]=z[40] + 7*z[8] + z[41] + 5*z[13] - z[7];
    z[42]=2*z[5];
    z[39]= - z[42]*z[39];
    z[21]=z[21] + z[26] + z[39] - z[16] + 2*z[40] + z[34];
    z[21]=z[6]*z[21];
    z[26]=2*z[9];
    z[34]=npow(z[11],2);
    z[39]=npow(z[3],3);
    z[40]=z[34]*z[39]*npow(z[10],2)*z[26];
    z[43]=z[2]*z[5];
    z[44]=z[16] + 5*z[5];
    z[44]=z[43]*z[39]*z[44];
    z[45]=z[39]*z[9];
    z[46]=z[15]*z[45];
    z[44]= - 4*z[46] + z[44];
    z[44]=z[2]*z[44];
    z[36]=z[36]*z[39];
    z[46]=z[36] - z[16];
    z[46]=z[46]*z[15];
    z[44]=z[46] + z[44];
    z[44]=z[2]*z[44];
    z[46]=5*z[4];
    z[47]= - z[46] + z[24];
    z[47]=z[2]*z[47];
    z[32]=z[47] - z[32];
    z[32]=z[39]*z[32];
    z[47]= - z[8] + z[4];
    z[47]=z[5]*z[47];
    z[45]= - 3*z[45] + z[47];
    z[45]=z[45]*z[42];
    z[32]=z[45] + z[32];
    z[32]=z[2]*z[32];
    z[36]= - z[38] + z[36];
    z[36]=2*z[36] + z[35];
    z[36]=z[5]*z[36];
    z[32]=z[36] + z[32];
    z[32]=z[2]*z[32];
    z[27]= - z[8]*z[27];
    z[27]=z[27] - z[33];
    z[36]=2*z[10];
    z[27]=z[27]*z[36];
    z[27]=z[32] + z[4] + z[27] + z[7] - 13*z[8];
    z[27]=z[6]*z[27];
    z[32]=z[2]*z[3];
    z[32]=npow(z[32],4);
    z[17]= - z[32]*z[17];
    z[32]= - z[37]*z[32];
    z[38]=z[8]*z[7];
    z[32]=z[38] + z[32];
    z[32]=z[6]*z[32];
    z[17]=z[17] + z[32];
    z[17]=z[1]*z[17];
    z[32]=3*z[8];
    z[45]= - z[7]*z[32];
    z[17]=z[17] + z[27] + z[44] - z[37] + z[45] + z[40];
    z[17]=z[6]*z[17];
    z[27]=z[39]*npow(z[10],3)*z[35]*z[38];
    z[35]=z[12]*z[39];
    z[35]=z[34] + z[35];
    z[38]=z[15]*z[4];
    z[35]=z[35]*z[38];
    z[38]=z[2]*z[38];
    z[39]= - z[39]*z[38];
    z[35]=z[35] + z[39];
    z[35]=z[2]*z[35];
    z[39]=z[34]*z[4];
    z[37]=z[37]*z[11];
    z[40]=z[39] + z[37];
    z[40]=z[5]*z[40];
    z[35]=z[40] + z[35];
    z[35]=z[35]*z[22];
    z[17]=z[35] + z[27] - z[37] + z[17];
    z[17]=z[1]*z[17];
    z[27]=z[30]*z[7];
    z[35]= - z[26]*z[27];
    z[37]=z[34]*z[25];
    z[37]=z[27] + z[37];
    z[37]=z[10]*z[37];
    z[40]=z[7]*z[13];
    z[44]=4*z[14];
    z[45]=z[8]*z[44];
    z[35]=z[37] + z[45] + z[40] + z[35];
    z[37]=z[9] + z[12];
    z[45]= - z[37]*z[19];
    z[45]=z[11] + z[45];
    z[45]=2*z[45] - z[4];
    z[45]=z[5]*z[45];
    z[47]=z[19]*z[12];
    z[48]=z[11] + z[47];
    z[48]=z[4]*z[48];
    z[45]=z[45] + 2*z[34] + z[48];
    z[45]=z[5]*z[45];
    z[39]=z[39] + z[45];
    z[45]=z[34] + z[19];
    z[45]=z[45]*z[16];
    z[48]=z[4]*z[11];
    z[20]=z[20] - 4*z[48];
    z[20]=z[5]*z[20];
    z[20]=z[45] + z[20];
    z[20]=z[20]*z[43];
    z[20]=2*z[39] + z[20];
    z[20]=z[2]*z[20];
    z[39]=z[44] - z[7];
    z[43]= - z[39] + z[47];
    z[43]=z[8]*z[43];
    z[45]=2*z[8];
    z[47]=3*z[7] + z[45];
    z[19]=z[10]*z[47]*z[19];
    z[19]=2*z[43] + z[19];
    z[19]=z[10]*z[19];
    z[43]= - z[11] - z[8];
    z[19]=4*z[43] + z[19];
    z[19]=z[4]*z[19];
    z[27]=z[9]*z[27];
    z[27]= - z[40] + z[27];
    z[27]=z[9]*z[27];
    z[27]=z[16] + z[11] + z[27];
    z[43]=z[9] + 2*z[12];
    z[43]=z[43]*z[5];
    z[25]=z[25]*z[43];
    z[25]=2*z[27] + z[25];
    z[25]=z[5]*z[25];
    z[17]=z[17] + z[21] + z[20] + z[25] + 2*z[35] + z[19];
    z[17]=z[1]*z[17];
    z[19]=z[41] + z[32];
    z[20]=z[29] + z[7];
    z[21]=z[9]*z[34];
    z[20]=z[21] - z[13] + z[19] - 2*z[20];
    z[20]=z[20]*z[36];
    z[15]=z[15]*z[26];
    z[21]= - z[4] - 8*z[5];
    z[21]=z[5]*z[21];
    z[21]=z[21] + 6*z[38];
    z[21]=z[2]*z[21];
    z[15]=z[21] + z[46] + z[15];
    z[15]=z[2]*z[15];
    z[21]=z[23] - z[42];
    z[21]=z[5]*z[21];
    z[18]=z[21] + z[18];
    z[18]=z[2]*z[18];
    z[18]=z[18] + 11*z[4] - z[24];
    z[18]=z[2]*z[18];
    z[21]= - static_cast<T>(1)+ z[31];
    z[23]=z[5]*z[9];
    z[18]=z[18] + 6*z[21] + z[23];
    z[18]=z[2]*z[18];
    z[18]=z[18] - z[9] - z[10];
    z[18]=z[6]*z[18];
    z[21]=z[13] - z[11];
    z[21]=z[21]*z[26];
    z[15]=z[18] + z[15] + z[31] + z[20] - static_cast<T>(9)+ z[21];
    z[15]=z[6]*z[15];
    z[18]=2*z[14];
    z[20]=z[18] - z[13];
    z[20]=z[20]*z[45];
    z[20]= - z[33] + z[20] - z[30] - z[40];
    z[20]=z[10]*z[20];
    z[21]=z[34] - z[30] + z[40];
    z[21]=z[9]*z[21];
    z[23]=8*z[14];
    z[19]=z[20] + z[21] - z[7] + z[23] - z[13] - z[19];
    z[20]=z[38] + z[34] + z[48];
    z[21]= - z[11] - z[4];
    z[16]= - z[12]*z[16];
    z[16]= - static_cast<T>(5)+ z[16];
    z[16]=z[5]*z[16];
    z[16]=2*z[21] + z[16];
    z[16]=z[5]*z[16];
    z[16]=z[16] + 4*z[20];
    z[16]=z[2]*z[16];
    z[20]= - z[23] + 5*z[7];
    z[20]=z[20]*z[28];
    z[21]=z[8] - z[39];
    z[20]=2*z[21] + z[20];
    z[20]=z[10]*z[20];
    z[21]=z[12]*z[32];
    z[21]=static_cast<T>(1)+ z[21];
    z[20]=2*z[21] + z[20];
    z[20]=z[4]*z[20];
    z[21]= - z[9]*z[30];
    z[21]=z[11] + z[21];
    z[21]=z[9]*z[21];
    z[23]=z[4]*z[12];
    z[21]=z[23] - static_cast<T>(1)+ z[21];
    z[21]=2*z[21] + z[43];
    z[21]=z[5]*z[21];
    z[15]=z[17] + z[15] + z[16] + z[21] + 2*z[19] + z[20];
    z[15]=z[1]*z[15];
    z[16]=z[18] - z[8];
    z[16]=z[16]*z[36];
    z[17]= - z[12]*z[44];
    z[19]=z[9]*z[11];
    z[20]=z[12] + z[10];
    z[20]=z[4]*z[20];
    z[21]= - z[5]*z[37];
    z[18]= - z[18] + z[11];
    z[18]=2*z[18] - z[4];
    z[18]=z[2]*z[18];
    z[16]=z[18] + z[21] + z[20] + z[16] - 3*z[19] + static_cast<T>(3)+ z[17];
    z[17]=z[22] + 7*z[9] + z[10];
    z[17]=z[6]*z[17];

    r += z[15] + 2*z[16] + z[17];
 
    return r;
}

template double qg_2lNLC_r881(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r881(const std::array<dd_real,31>&);
#endif
