#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r445(const std::array<T,31>& k) {
  T z[85];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[7];
    z[9]=k[5];
    z[10]=k[2];
    z[11]=k[14];
    z[12]=k[3];
    z[13]=k[9];
    z[14]=k[13];
    z[15]=npow(z[8],3);
    z[16]=z[4]*z[2];
    z[17]=npow(z[9],5)*z[15]*z[16];
    z[18]=npow(z[8],2);
    z[19]=z[3]*z[5];
    z[20]=z[18]*z[19];
    z[21]=npow(z[3],3);
    z[22]=z[21]*z[5];
    z[20]=z[22] + z[20];
    z[22]= - z[8]*npow(z[6],5)*z[20];
    z[17]=z[22] + z[17];
    z[17]=z[17]*npow(z[7],5);
    z[22]=npow(z[4],3);
    z[23]=3*z[2];
    z[24]=z[22]*z[23];
    z[25]=z[24]*z[5];
    z[26]=npow(z[4],2);
    z[27]=z[26]*z[5];
    z[28]=5*z[2];
    z[29]= - z[28] + z[4];
    z[29]=z[29]*z[27];
    z[30]=z[22]*z[2];
    z[29]=z[30] + z[29];
    z[29]=z[3]*z[29];
    z[31]= - z[1]*z[19]*z[30];
    z[17]=z[31] - z[25] + z[29] + z[17];
    z[17]=z[1]*z[17];
    z[29]=npow(z[7],4);
    z[31]=z[21]*z[29];
    z[32]=z[12]*z[31];
    z[33]=z[3]*z[12];
    z[34]=z[29]*z[33];
    z[35]=z[29]*z[9];
    z[36]= - z[5]*z[35];
    z[34]=z[34] + z[36];
    z[18]=z[34]*z[18];
    z[18]=z[32] + z[18];
    z[18]=z[8]*z[18];
    z[32]=5*z[19];
    z[34]=2*z[5];
    z[36]=z[34] + z[3];
    z[37]= - z[8]*z[36];
    z[37]=z[32] + z[37];
    z[37]=z[8]*z[29]*z[37];
    z[37]= - z[31] + z[37];
    z[37]=z[8]*z[37];
    z[38]=3*z[5];
    z[39]=z[31]*z[38];
    z[37]=z[39] + z[37];
    z[37]=z[6]*z[37];
    z[39]= - z[10]*z[39];
    z[18]=z[37] + z[39] + z[18];
    z[18]=z[6]*z[18];
    z[37]=z[29]*z[2];
    z[39]=npow(z[9],2);
    z[15]=z[15]*z[39]*z[37];
    z[40]=npow(z[10],2);
    z[41]=z[40]*z[5];
    z[31]=z[31]*z[41];
    z[15]=z[18] + z[31] + z[15];
    z[15]=z[6]*z[15];
    z[18]=z[22]*z[5];
    z[22]=z[30] + z[18];
    z[22]=z[3]*z[22];
    z[31]=z[8]*z[9];
    z[42]=npow(z[31],3);
    z[37]= - z[42]*z[37];
    z[15]=z[15] + z[22] + z[37];
    z[15]=z[6]*z[15];
    z[22]=4*z[5];
    z[37]=z[22]*z[40];
    z[43]=npow(z[11],3);
    z[44]=z[43]*z[35]*z[37];
    z[25]= - z[25] + z[44];
    z[25]=z[9]*z[25];
    z[44]=z[4]*z[12];
    z[29]=z[29]*z[44];
    z[45]=2*z[4];
    z[46]=z[45] - z[2];
    z[35]= - z[46]*z[35];
    z[29]=z[29] + z[35];
    z[29]=z[29]*z[42];
    z[35]=4*z[2];
    z[42]= - z[35] + z[4];
    z[42]=z[42]*z[27];
    z[42]=z[30] + z[42];
    z[47]=17*z[2];
    z[48]=5*z[4];
    z[49]= - z[47] + z[48];
    z[50]=z[5]*z[4];
    z[49]=z[49]*z[50];
    z[51]=z[26]*z[2];
    z[49]=5*z[51] + z[49];
    z[49]=z[3]*z[49];
    z[15]=z[17] + z[15] + z[29] + z[25] + 3*z[42] + z[49];
    z[15]=z[1]*z[15];
    z[17]=npow(z[3],2);
    z[25]=npow(z[7],3);
    z[29]=z[17]*z[25];
    z[42]=z[10] + z[12];
    z[49]=3*z[3];
    z[52]= - z[42]*z[49];
    z[53]=z[5]*z[10];
    z[52]=4*z[53] + z[52];
    z[52]=z[52]*z[29];
    z[54]=z[22] + z[2];
    z[55]=z[54]*z[9];
    z[56]=3*z[33];
    z[57]= - z[56] + z[55];
    z[57]=z[25]*z[57];
    z[58]=2*z[9];
    z[59]=z[58]*z[25];
    z[60]=z[25]*z[12];
    z[59]=z[59] - z[60];
    z[61]=z[19] - z[59];
    z[61]=z[8]*z[61];
    z[57]=z[61] + z[57];
    z[57]=z[8]*z[57];
    z[61]=4*z[43];
    z[62]=z[5]*z[17];
    z[62]= - z[61] + z[62];
    z[62]=z[3]*z[62];
    z[57]=z[62] + z[57];
    z[57]=z[8]*z[57];
    z[62]= - z[34] + z[49];
    z[62]=z[62]*z[29];
    z[63]=5*z[3];
    z[64]=2*z[8];
    z[65]= - z[64] + 9*z[5] + z[63];
    z[65]=z[8]*z[65];
    z[65]=8*z[19] + z[65];
    z[65]=z[8]*z[25]*z[65];
    z[62]=z[62] + z[65];
    z[62]=z[6]*z[62];
    z[52]=z[62] + z[52] + z[57];
    z[52]=z[6]*z[52];
    z[57]=z[10] + 3*z[12];
    z[62]=z[3]*z[10];
    z[57]=z[57]*z[62];
    z[65]= - z[40]*z[34];
    z[65]=z[65] + z[57];
    z[29]=z[65]*z[29];
    z[39]=z[39]*z[25];
    z[65]= - z[2]*z[39];
    z[65]= - z[19] + z[65];
    z[66]=z[9]*z[59];
    z[66]=z[34] + z[66];
    z[66]=z[8]*z[66];
    z[65]=2*z[65] + z[66];
    z[65]=z[8]*z[65];
    z[66]=npow(z[11],2);
    z[67]= - z[3]*z[66];
    z[65]=z[65] - 8*z[43] + z[67];
    z[65]=z[8]*z[65];
    z[67]=2*z[2];
    z[68]=z[67] + z[38];
    z[68]=z[3]*z[26]*z[68];
    z[18]=z[52] + z[65] + z[29] + z[68] + z[24] + z[18];
    z[18]=z[6]*z[18];
    z[29]=npow(z[13],3);
    z[52]=z[51] - z[29];
    z[65]= - z[52]*z[50];
    z[68]=z[66]*z[25];
    z[69]=9*z[53];
    z[70]= - z[68]*z[69];
    z[65]=z[65] + z[70];
    z[65]=z[9]*z[65];
    z[70]=9*z[2];
    z[71]= - z[70] + z[45];
    z[71]=z[4]*z[71];
    z[72]=npow(z[13],2);
    z[73]=4*z[72];
    z[71]= - z[73] + z[71];
    z[71]=z[71]*z[50];
    z[68]=z[41]*z[68];
    z[74]= - z[2]*z[29];
    z[65]=z[65] - 7*z[68] + z[71] + z[74] + z[24];
    z[65]=z[9]*z[65];
    z[59]=z[16] - z[59];
    z[59]=z[9]*z[59];
    z[59]=z[5] + z[59];
    z[59]=z[59]*z[31];
    z[25]=z[2]*z[25]*npow(z[9],3);
    z[25]=z[59] - z[34] + z[25];
    z[25]=z[8]*z[25];
    z[59]=2*z[11];
    z[68]=z[59] + z[38];
    z[68]=z[3]*z[68];
    z[39]=z[16]*z[39];
    z[39]= - z[61] + 23*z[39];
    z[39]=z[9]*z[39];
    z[61]=3*z[16];
    z[25]=z[25] + z[39] + z[68] - z[66] + z[61];
    z[25]=z[8]*z[25];
    z[39]=z[29]*z[10];
    z[68]=5*z[72];
    z[71]= - z[68] - z[39];
    z[71]=z[10]*z[71];
    z[71]= - 4*z[13] + z[71];
    z[71]=z[2]*z[71];
    z[74]=13*z[2];
    z[75]= - z[74] + 11*z[4];
    z[75]=z[5]*z[75];
    z[71]=z[75] + z[71] + 9*z[16];
    z[71]=z[3]*z[71];
    z[60]= - z[40]*z[21]*z[60];
    z[75]=2*z[39];
    z[76]=z[68] + z[75];
    z[76]=z[2]*z[76];
    z[77]=10*z[4];
    z[78]=11*z[2];
    z[79]= - z[13] - z[78];
    z[79]=3*z[79] + z[77];
    z[79]=z[79]*z[50];
    z[15]=z[15] + z[18] + z[25] + z[65] + z[60] + z[71] + z[79] + z[76]
    + 12*z[51];
    z[15]=z[1]*z[15];
    z[18]=z[69] + 4*z[62];
    z[25]=npow(z[7],2);
    z[18]=z[18]*z[25];
    z[60]=5*z[50];
    z[18]=z[18] - 2*z[19] + z[60] - 5*z[66] + z[61];
    z[18]=z[3]*z[18];
    z[65]=z[25]*z[12];
    z[69]=2*z[3];
    z[71]=z[25]*z[9];
    z[76]=z[38]*z[9];
    z[79]= - static_cast<T>(2)- z[76];
    z[79]=z[8]*z[79];
    z[79]=z[79] + 4*z[71] - 2*z[65] + 13*z[5] + z[69];
    z[79]=z[8]*z[79];
    z[80]=11*z[5];
    z[81]=3*z[11];
    z[82]= - z[81] - z[80];
    z[82]=z[3]*z[82];
    z[83]=z[25]*z[56];
    z[47]= - z[47] + z[22];
    z[47]=z[47]*z[71];
    z[47]=z[79] + z[47] + z[83] + 11*z[66] + z[82];
    z[47]=z[8]*z[47];
    z[79]=7*z[66] - z[17];
    z[79]=z[3]*z[79];
    z[82]=z[2] + z[34];
    z[63]=4*z[82] - z[63];
    z[63]=z[63]*z[25];
    z[82]=3*z[19] + 2*z[25];
    z[83]=6*z[5];
    z[84]= - z[83] - z[3];
    z[84]=z[8]*z[84];
    z[82]=3*z[82] + z[84];
    z[82]=z[8]*z[82];
    z[63]=z[82] + z[79] + z[63];
    z[63]=z[8]*z[63];
    z[79]= - z[80] - z[3];
    z[79]=z[79]*z[25]*z[69];
    z[80]=z[6]*z[8];
    z[20]=z[20]*z[80];
    z[20]=3*z[20];
    z[82]=z[21]*z[38];
    z[63]= - z[20] + z[63] + z[82] + z[79];
    z[63]=z[6]*z[63];
    z[79]=z[67]*z[14];
    z[84]= - z[79] + 7*z[16];
    z[84]=z[84]*z[25];
    z[24]=z[24] + z[84];
    z[24]=z[9]*z[24];
    z[51]= - 3*z[43] + z[51];
    z[27]=2*z[51] + z[27];
    z[18]=z[63] + z[47] + z[24] + 2*z[27] + z[18];
    z[18]=z[6]*z[18];
    z[24]=3*z[72];
    z[27]=z[24] + z[75];
    z[27]=z[10]*z[27];
    z[47]= - z[10]*z[35];
    z[47]=static_cast<T>(3)+ z[47];
    z[47]=z[47]*z[34];
    z[51]=3*z[13];
    z[63]= - z[10]*z[72];
    z[63]= - z[13] + z[63];
    z[63]=z[10]*z[63];
    z[63]=static_cast<T>(3)+ 5*z[63];
    z[63]=z[2]*z[63];
    z[75]= - static_cast<T>(2)+ z[62];
    z[75]=z[3]*z[75];
    z[27]=z[75] + z[47] + z[45] + z[63] + z[27] + 5*z[11] - z[51];
    z[27]=z[3]*z[27];
    z[47]= - 2*z[72] - z[16];
    z[34]=z[47]*z[34];
    z[34]=z[52] + z[34];
    z[34]=z[4]*z[34];
    z[47]=z[14]*z[23];
    z[52]=z[11]*z[83];
    z[47]=z[52] + z[47] - 19*z[16];
    z[47]=z[47]*z[25];
    z[34]=z[47] + z[34];
    z[34]=z[9]*z[34];
    z[47]=z[70] + z[4];
    z[47]=z[4]*z[47];
    z[47]=z[73] + z[47];
    z[47]=z[4]*z[47];
    z[52]=4*z[4];
    z[63]=z[52] - 8*z[13] - 21*z[2];
    z[63]=z[4]*z[63];
    z[63]= - z[73] + z[63];
    z[63]=z[5]*z[63];
    z[70]=z[66]*z[10];
    z[73]=z[11]*z[53];
    z[73]= - z[70] + z[73];
    z[73]=z[73]*z[25];
    z[43]=12*z[43];
    z[34]=z[34] + 5*z[73] + z[63] + z[47] - z[43] - z[29];
    z[34]=z[9]*z[34];
    z[47]=2*z[14];
    z[63]= - z[47] + z[74];
    z[63]=z[4]*z[63];
    z[73]=3*z[25];
    z[74]=z[44]*z[73];
    z[75]=z[35] - 3*z[4];
    z[75]=z[75]*z[71];
    z[63]=6*z[75] + z[74] + 4*z[66] + z[63];
    z[63]=z[9]*z[63];
    z[74]= - z[22] + z[28] - z[45];
    z[54]= - 2*z[71] + z[65] + z[54];
    z[54]=z[9]*z[54];
    z[65]=z[61]*z[9];
    z[65]=z[65] + z[23];
    z[75]= - z[45] + z[65];
    z[75]=z[9]*z[75];
    z[75]= - static_cast<T>(2)+ z[75];
    z[75]=z[75]*z[31];
    z[54]=z[75] - static_cast<T>(2)+ z[54];
    z[54]=z[8]*z[54];
    z[54]=z[54] + z[63] + 2*z[74] - z[49];
    z[54]=z[8]*z[54];
    z[37]= - z[11]*z[37];
    z[63]= - z[40]*z[69];
    z[63]=z[41] + z[63];
    z[63]=z[3]*z[63];
    z[37]=z[37] + z[63];
    z[37]=z[37]*z[25];
    z[63]=2*z[13];
    z[74]=z[10]*z[68];
    z[74]= - z[63] + z[74];
    z[74]=z[2]*z[74];
    z[75]=z[4] + z[63] + z[78];
    z[75]=z[4]*z[75];
    z[83]= - z[11] - z[63];
    z[83]=15*z[4] + 3*z[83] - 14*z[2];
    z[83]=z[5]*z[83];
    z[15]=z[15] + z[18] + z[54] + z[34] + z[37] + z[27] + z[83] + z[75]
    + z[74] - z[39] + z[66] + z[68];
    z[15]=z[1]*z[15];
    z[18]=z[28] - z[13];
    z[27]=z[77] - 3*z[70] - z[18];
    z[27]=z[5]*z[27];
    z[29]=z[29]*z[12];
    z[24]=z[24] - z[29];
    z[34]=z[67] + z[4];
    z[34]=z[4]*z[34];
    z[34]=z[34] + z[24];
    z[34]=z[4]*z[34];
    z[18]= - z[18]*z[50];
    z[18]=z[34] + z[18];
    z[18]=z[9]*z[18];
    z[34]=z[63] - z[23];
    z[34]=3*z[34] + z[45];
    z[34]=z[4]*z[34];
    z[37]= - z[10]*z[43];
    z[18]=z[18] + z[27] + z[34] + z[79] + z[37] + 8*z[66] + z[24];
    z[18]=z[9]*z[18];
    z[27]=z[38] + z[3];
    z[27]=z[8]*z[27];
    z[27]= - 6*z[19] + z[27];
    z[27]=z[8]*z[27];
    z[21]=z[21] + z[27];
    z[21]=z[8]*z[21];
    z[21]= - z[82] + z[21];
    z[20]=2*z[21] + z[20];
    z[20]=z[6]*z[20];
    z[21]=z[53] - 1;
    z[27]=z[21]*z[49];
    z[34]=z[22] + z[27];
    z[34]=z[34]*z[17];
    z[37]=z[76] + static_cast<T>(4)- z[33];
    z[37]=z[8]*z[37];
    z[37]=z[37] - 20*z[5] - 7*z[3];
    z[37]=z[8]*z[37];
    z[17]= - z[12]*z[17];
    z[17]=z[17] + 7*z[11] + 8*z[5];
    z[17]=z[3]*z[17];
    z[17]=z[17] + z[37];
    z[17]=z[8]*z[17];
    z[17]=z[20] + z[34] + z[17];
    z[17]=z[6]*z[17];
    z[20]=z[33] - 2;
    z[34]= - z[55] + z[20];
    z[23]=z[23]*z[9];
    z[23]=z[23] - 4;
    z[23]=z[23]*z[9];
    z[23]=z[23] + z[12];
    z[37]= - z[8]*z[23];
    z[34]=2*z[34] + z[37];
    z[34]=z[8]*z[34];
    z[37]=10*z[11];
    z[34]=z[34] + 8*z[3] + 12*z[5] + z[37] - z[78];
    z[34]=z[8]*z[34];
    z[39]=z[66] + z[61];
    z[21]= - 2*z[21] + z[33];
    z[21]=z[21]*z[69];
    z[21]=z[21] - 14*z[11] - z[5];
    z[21]=z[3]*z[21];
    z[26]=z[26]*z[67];
    z[30]=z[9]*z[30];
    z[26]=z[26] + z[30];
    z[26]=z[9]*z[26];
    z[17]=z[17] + z[34] + z[26] + z[21] + 2*z[39] + z[60];
    z[17]=z[6]*z[17];
    z[21]=z[72]*z[12];
    z[26]=z[21] + z[81] - z[63];
    z[29]=z[68] - z[29];
    z[29]=z[10]*z[29];
    z[26]=2*z[26] + z[29];
    z[26]=z[10]*z[26];
    z[29]= - z[40]*z[35];
    z[29]=z[29] - z[12];
    z[29]=z[13]*z[29];
    z[30]=z[10] - z[12];
    z[34]=z[30]*z[62];
    z[34]= - 4*z[10] + z[34];
    z[34]=z[3]*z[34];
    z[39]=z[2]*z[40];
    z[39]= - z[10] - 6*z[39];
    z[39]=z[5]*z[39];
    z[26]=z[34] + z[39] + z[26] + z[29];
    z[26]=z[3]*z[26];
    z[29]= - 3*z[14] + 25*z[2];
    z[29]=z[9]*z[29];
    z[29]=z[29] - 11;
    z[29]=z[4]*z[29];
    z[29]=23*z[2] - z[47] + z[81] + z[29];
    z[29]=z[9]*z[29];
    z[34]= - z[52] + z[65];
    z[34]=z[9]*z[34];
    z[34]=z[34] - static_cast<T>(4)+ z[44];
    z[34]=z[9]*z[34];
    z[34]=z[12] + z[34];
    z[31]=z[34]*z[31];
    z[23]=z[31] + z[23];
    z[23]=z[8]*z[23];
    z[31]= - static_cast<T>(6)+ z[44];
    z[23]=z[23] + z[29] + 2*z[31] - z[56];
    z[23]=z[8]*z[23];
    z[24]=2*z[66] + z[24];
    z[24]=z[10]*z[24];
    z[29]= - z[10]*z[13];
    z[29]=static_cast<T>(7)+ z[29];
    z[29]=z[2]*z[29];
    z[31]= - z[67] - 4*z[11] - z[13];
    z[31]=z[10]*z[31];
    z[31]=static_cast<T>(7)+ z[31];
    z[31]=z[5]*z[31];
    z[15]=z[15] + z[17] + z[23] + z[18] + z[26] + z[31] + z[77] + z[29]
    + z[24] + z[21] - z[37] + z[51];
    z[15]=z[1]*z[15];
    z[17]=z[71]*z[16];
    z[16]=5*z[16];
    z[18]=z[7]*z[45];
    z[18]= - z[16] + z[18];
    z[18]=z[7]*z[18];
    z[18]=z[18] - z[17];
    z[18]=z[9]*z[18];
    z[21]=z[7]*z[48];
    z[18]=z[21] + z[18];
    z[18]=z[18]*z[58];
    z[21]=z[7]*z[12];
    z[23]=z[21] - 2;
    z[24]=2*z[7];
    z[26]=z[23]*z[24];
    z[29]=9*z[11];
    z[18]=z[18] + z[26] - 10*z[5] + z[77] + z[67] - z[47] + z[29];
    z[18]=z[9]*z[18];
    z[25]=z[25]*z[80];
    z[25]=z[25] - z[73];
    z[25]=z[19]*z[25];
    z[26]=z[7]*z[36];
    z[26]= - z[32] + z[26];
    z[26]=z[8]*z[7]*z[26];
    z[25]=z[26] + z[25];
    z[25]=z[6]*z[25];
    z[22]=z[27] - z[22];
    z[26]=z[7]*z[22];
    z[19]=12*z[19] + z[26];
    z[19]=z[7]*z[19];
    z[20]= - z[7]*z[20];
    z[20]= - 4*z[36] + z[20];
    z[20]=z[7]*z[20];
    z[26]=z[71]*z[5];
    z[20]=z[20] + z[26];
    z[20]=z[8]*z[20];
    z[19]=z[25] + z[19] + z[20];
    z[19]=z[6]*z[19];
    z[20]=3*z[42] - z[41];
    z[20]=z[3]*z[20];
    z[25]=z[53] - 2;
    z[25]=2*z[25];
    z[20]=z[25] + z[20];
    z[20]=z[7]*z[20];
    z[20]= - 3*z[22] + z[20];
    z[20]=z[7]*z[20];
    z[22]= - z[24] + z[67] - z[38];
    z[22]=z[7]*z[22];
    z[27]=z[71]*z[2];
    z[22]=z[22] + z[27];
    z[22]=z[9]*z[22];
    z[31]=z[21] - static_cast<T>(6)+ z[33];
    z[31]=z[7]*z[31];
    z[22]=z[31] + z[22];
    z[22]=z[8]*z[22];
    z[19]=z[19] + z[22] + z[20] - z[26];
    z[19]=z[6]*z[19];
    z[20]=z[41] - z[12] - 3*z[10];
    z[20]=z[3]*z[20];
    z[20]= - z[25] + z[20];
    z[22]=2*z[30];
    z[25]=z[22] - z[57];
    z[25]=z[7]*z[25];
    z[20]=2*z[20] + z[25];
    z[20]=z[20]*z[24];
    z[25]=z[5] + z[7];
    z[25]=z[25]*z[24];
    z[26]=z[7]*z[61];
    z[26]=z[26] + z[17];
    z[26]=z[9]*z[26];
    z[25]=z[25] + z[26];
    z[25]=z[25]*z[58];
    z[26]= - z[28] + z[24];
    z[26]=z[7]*z[26];
    z[26]=z[26] - z[27];
    z[26]=z[9]*z[26];
    z[27]=z[21] - 7;
    z[30]= - z[7]*z[27];
    z[26]=z[30] + z[26];
    z[26]=z[9]*z[26];
    z[23]= - 2*z[23] + z[26];
    z[23]=z[23]*z[64];
    z[19]=2*z[19] + z[23] + z[25] + z[20] + z[3] - 18*z[5] + z[29] - 
    z[35];
    z[19]=z[6]*z[19];
    z[20]=z[42]*z[62];
    z[23]=z[40]*z[33];
    z[25]=z[10]*z[12];
    z[23]=z[25] + z[23];
    z[23]=z[7]*z[23];
    z[20]=z[23] - z[22] + z[20];
    z[20]=z[20]*z[24];
    z[22]= - z[7]*z[46];
    z[16]=z[16] + z[22];
    z[16]=z[7]*z[16];
    z[16]=z[16] + z[17];
    z[16]=z[9]*z[16];
    z[17]= - static_cast<T>(2)+ z[44];
    z[17]=z[7]*z[17];
    z[17]=z[17] + z[28] - 7*z[4];
    z[17]=z[7]*z[17];
    z[16]=z[17] + z[16];
    z[16]=z[9]*z[16];
    z[17]=z[12]*z[45];
    z[17]=z[17] + z[27];
    z[17]=z[7]*z[17];
    z[16]=z[17] + z[16];
    z[16]=z[9]*z[16];
    z[17]= - static_cast<T>(5)+ z[21];
    z[16]=2*z[17] + z[16];
    z[16]=z[9]*z[16];
    z[16]=z[16] + 2*z[12];
    z[16]=z[16]*z[64];
    z[17]= - z[10]*z[59];
    z[17]= - static_cast<T>(1)+ z[17];
    z[21]=z[10]*z[67];

    r += z[15] + z[16] + 7*z[17] + z[18] + z[19] + z[20] + z[21] - 
      z[33] + 12*z[53];
 
    return r;
}

template double qg_2lNLC_r445(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r445(const std::array<dd_real,31>&);
#endif
