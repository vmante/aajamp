#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r415(const std::array<T,31>& k) {
  T z[24];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[8];
    z[6]=k[12];
    z[7]=k[10];
    z[8]=k[14];
    z[9]=k[15];
    z[10]=k[4];
    z[11]=k[2];
    z[12]= - z[8] + n<T>(1,2)*z[9];
    z[13]=z[7]*z[9];
    z[13]=3*z[13];
    z[12]=z[12]*z[13];
    z[13]=n<T>(1,2)*z[6];
    z[14]=z[5]*z[2];
    z[15]= - z[14]*z[13];
    z[15]= - z[5] + z[15];
    z[16]=npow(z[6],2);
    z[15]=z[15]*z[16];
    z[15]=z[12] + z[15];
    z[15]=z[4]*z[15];
    z[17]=z[5]*npow(z[6],3);
    z[15]=n<T>(1,2)*z[17] + z[15];
    z[15]=z[1]*z[15];
    z[17]=z[6]*z[14];
    z[17]=n<T>(3,2)*z[5] + z[17];
    z[16]=z[17]*z[16];
    z[12]=z[15] + z[12] + z[16];
    z[15]=n<T>(1,2)*z[8];
    z[16]=z[9]*z[10];
    z[17]=static_cast<T>(1)- n<T>(1,4)*z[16];
    z[17]=z[9]*z[17];
    z[17]= - z[15] + z[17];
    z[17]=z[7]*z[17];
    z[18]=z[9]*z[8];
    z[17]= - z[18] + z[17];
    z[19]=3*z[14];
    z[20]=npow(z[2],2);
    z[21]=z[20]*z[5];
    z[22]=z[21] - n<T>(1,2)*z[2];
    z[23]= - z[6]*z[22];
    z[23]=z[23] + n<T>(1,2) - z[19];
    z[23]=z[23]*z[13];
    z[23]= - z[5] + z[23];
    z[23]=z[6]*z[23];
    z[17]=3*z[17] + z[23];
    z[17]=z[4]*z[17];
    z[12]=z[17] + n<T>(1,2)*z[12];
    z[12]=z[1]*z[12];
    z[17]= - z[11]*z[15];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[9]*z[17];
    z[17]= - z[8] + z[17];
    z[17]=z[7]*z[17];
    z[17]= - z[18] + z[17];
    z[21]=z[6]*z[21];
    z[19]=z[19] + z[21];
    z[13]=z[19]*z[13];
    z[13]=z[5] + z[13];
    z[13]=z[6]*z[13];
    z[13]=3*z[17] + z[13];
    z[17]=z[8]*z[10];
    z[17]= - static_cast<T>(1)+ z[17];
    z[16]=n<T>(1,2)*z[17] - z[16];
    z[16]=z[7]*z[16];
    z[17]= - z[2]*z[18];
    z[15]=z[17] + z[16] - z[15] + z[9];
    z[15]=3*z[15] - z[5];
    z[16]= - z[5]*npow(z[2],3);
    z[16]=z[20] + z[16];
    z[16]=z[6]*z[16];
    z[16]=n<T>(1,4)*z[16] - z[22];
    z[16]=z[6]*z[16];
    z[14]=static_cast<T>(1)- 5*z[14];
    z[14]=n<T>(1,4)*z[14] + z[16];
    z[14]=z[6]*z[14];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[14]=z[4]*z[14];
    z[12]=z[12] + n<T>(1,2)*z[13] + z[14];

    r += n<T>(1,2)*z[12]*npow(z[3],2)*npow(z[1],2);
 
    return r;
}

template double qg_2lNLC_r415(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r415(const std::array<dd_real,31>&);
#endif
