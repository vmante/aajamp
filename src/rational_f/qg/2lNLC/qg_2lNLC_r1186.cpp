#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1186(const std::array<T,31>& k) {
  T z[121];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[3];
    z[4]=k[6];
    z[5]=k[9];
    z[6]=k[10];
    z[7]=k[8];
    z[8]=k[5];
    z[9]=k[12];
    z[10]=k[4];
    z[11]=k[13];
    z[12]=k[15];
    z[13]=k[7];
    z[14]=k[26];
    z[15]=k[14];
    z[16]=k[29];
    z[17]=n<T>(1,3)*z[13];
    z[18]=2*z[11];
    z[19]=2*z[12];
    z[20]=z[17] + z[18] - z[19];
    z[20]=z[20]*z[18];
    z[21]=2*z[5];
    z[22]=npow(z[9],2);
    z[23]=z[21]*z[22];
    z[24]=npow(z[3],2);
    z[25]=z[24]*z[23];
    z[26]=z[22]*z[3];
    z[27]=5*z[15];
    z[28]= - z[27] - z[9];
    z[25]=z[25] + 8*z[11] + 4*z[28] + 7*z[26];
    z[25]=z[5]*z[25];
    z[28]=z[13]*z[15];
    z[29]=n<T>(2,3)*z[28];
    z[30]=npow(z[15],2);
    z[31]=3*z[30];
    z[32]= - z[31] + z[29];
    z[33]=z[9] + z[12];
    z[34]=2*z[9];
    z[35]=z[33]*z[34];
    z[20]=z[25] + z[20] + 5*z[32] + z[35];
    z[20]=z[5]*z[20];
    z[25]=npow(z[15],3);
    z[32]=npow(z[4],2);
    z[35]=n<T>(2,3)*z[32];
    z[36]= - z[15]*z[35];
    z[36]= - 3*z[25] + z[36];
    z[37]=z[30]*z[13];
    z[36]=2*z[36] + z[37];
    z[38]=z[9]*z[12];
    z[39]= - n<T>(14,3)*z[32] + n<T>(23,2)*z[38];
    z[39]=z[9]*z[39];
    z[40]=3*z[13];
    z[41]=4*z[12];
    z[42]= - z[41] - z[40];
    z[42]=z[11]*z[42];
    z[42]=3*z[32] + z[42];
    z[42]=z[11]*z[42];
    z[20]=z[20] + z[42] + 5*z[36] + z[39];
    z[20]=z[5]*z[20];
    z[36]=6*z[12];
    z[39]=z[30]*z[36];
    z[42]=npow(z[13],2);
    z[43]=z[42]*z[12];
    z[44]=npow(z[7],2);
    z[45]=z[44]*z[12];
    z[46]=z[42]*z[9];
    z[39]=7*z[46] + 3*z[43] + z[39] - z[45];
    z[47]=z[13] - z[15];
    z[48]=z[19]*z[47];
    z[49]= - z[19] + z[7];
    z[49]=z[7]*z[49];
    z[48]=z[49] + 5*z[32] + z[48];
    z[49]=z[9]*z[13];
    z[48]=2*z[48] + 3*z[49];
    z[48]=z[11]*z[48];
    z[39]=2*z[39] + z[48];
    z[39]=z[11]*z[39];
    z[48]=16*z[14];
    z[50]= - z[48] + n<T>(27,2)*z[12];
    z[51]=npow(z[14],2);
    z[50]=z[50]*z[51];
    z[52]=z[7]*z[14];
    z[53]=2*z[52];
    z[54]=4*z[14];
    z[55]=z[12] - z[54];
    z[55]=z[55]*z[53];
    z[55]=z[50] + z[55];
    z[55]=z[7]*z[55];
    z[56]=2*z[7];
    z[57]= - z[56] - z[13];
    z[58]=2*z[13];
    z[57]=z[58]*z[32]*z[57];
    z[59]=8*z[14];
    z[60]=n<T>(23,2)*z[12] - z[59];
    z[60]=z[14]*z[60];
    z[60]= - 8*z[32] + z[60];
    z[60]=z[9]*z[60];
    z[61]=z[13]*z[32];
    z[60]=z[60] + z[50] + n<T>(56,3)*z[61];
    z[60]=z[9]*z[60];
    z[61]=8*z[12];
    z[62]= - z[25]*z[61];
    z[20]=z[20] + z[39] + z[60] + z[57] + z[62] + z[55];
    z[20]=z[8]*z[20];
    z[39]=z[22]*z[24];
    z[55]=n<T>(16,3) + z[39];
    z[57]=z[5]*z[9];
    z[60]= - z[24]*z[57];
    z[55]=2*z[55] + z[60];
    z[55]=z[55]*z[21];
    z[60]=z[32]*z[2];
    z[62]= - n<T>(2,3)*z[60] - n<T>(11,3)*z[15] - z[19];
    z[63]= - z[32] + 14*z[22];
    z[63]=z[3]*z[63];
    z[64]=8*z[7];
    z[65]=n<T>(4,3)*z[13];
    z[55]=z[55] + n<T>(10,3)*z[11] + z[63] - 8*z[9] + z[65] + 5*z[62] - 
    z[64];
    z[55]=z[5]*z[55];
    z[62]=z[2]*z[15];
    z[63]= - z[62]*z[35];
    z[66]=z[15]*z[58];
    z[63]=z[66] + z[31] + z[63];
    z[66]=n<T>(19,2)*z[9];
    z[67]=z[32]*z[3];
    z[68]= - z[67]*z[66];
    z[69]=6*z[67] - z[41] + n<T>(11,3)*z[13];
    z[70]=3*z[11];
    z[69]=2*z[69] + z[70];
    z[69]=z[11]*z[69];
    z[71]=4*z[9];
    z[72]=21*z[12] + z[71];
    z[72]=z[9]*z[72];
    z[55]=z[55] + z[69] + z[68] + 5*z[63] + z[72];
    z[55]=z[5]*z[55];
    z[63]= - z[15]*z[19];
    z[63]=z[31] + z[63];
    z[68]=3*z[12];
    z[69]= - z[68] + z[56];
    z[69]=z[7]*z[69];
    z[72]=z[12] + z[40];
    z[72]=z[72]*z[40];
    z[63]= - n<T>(58,3)*z[49] + z[72] + 2*z[63] + z[69];
    z[69]=7*z[13];
    z[72]=z[32]*z[10];
    z[73]=z[69] - z[15] + z[72];
    z[73]=z[67] + 4*z[73] - 3*z[9];
    z[73]=z[11]*z[73];
    z[63]=2*z[63] + z[73];
    z[63]=z[11]*z[63];
    z[73]=3*z[14];
    z[74]=z[73] - z[56];
    z[74]=z[74]*z[56];
    z[68]=z[68] + z[72];
    z[68]=z[68]*z[58];
    z[75]=33*z[30];
    z[68]=z[68] + z[75] + z[74];
    z[68]=z[13]*z[68];
    z[74]=z[42] - 8*z[49];
    z[76]=2*z[3];
    z[74]=z[76]*z[74];
    z[77]=z[30]*z[2];
    z[74]=z[74] - 15*z[77];
    z[74]=z[32]*z[74];
    z[78]=z[30]*z[12];
    z[79]= - 21*z[25] + z[78];
    z[80]= - 43*z[14] + 29*z[12];
    z[81]=z[14]*z[80];
    z[81]=z[81] - 20*z[52];
    z[81]=z[7]*z[81];
    z[82]=16*z[51];
    z[83]=z[82] + z[32];
    z[84]= - z[14]*z[83];
    z[85]=z[51]*z[7];
    z[84]=z[84] - 8*z[85];
    z[86]=2*z[10];
    z[87]=z[86]*z[7];
    z[84]=z[84]*z[87];
    z[83]= - z[83]*z[86];
    z[80]=6*z[13] + z[80] + z[83];
    z[80]=z[14]*z[80];
    z[82]= - z[10]*z[82];
    z[82]= - 23*z[14] + z[82];
    z[82]=z[9]*z[82];
    z[80]=z[82] + z[80];
    z[80]=z[9]*z[80];
    z[20]=z[20] + z[55] + z[63] + z[80] + z[68] + z[84] + z[81] + 4*
    z[79] + z[74];
    z[20]=z[8]*z[20];
    z[43]=z[43] + z[45];
    z[45]= - z[30]*z[19];
    z[55]=npow(z[4],3);
    z[63]=z[55]*z[87];
    z[68]=z[55]*z[10];
    z[74]=z[55]*z[3];
    z[79]=5*z[68] + 8*z[74];
    z[80]= - z[49] + z[79];
    z[80]=z[11]*z[80];
    z[45]=z[80] - 6*z[46] + z[63] + z[45] - z[43];
    z[45]=z[11]*z[45];
    z[63]= - z[44]*z[68];
    z[80]=z[12]*z[25];
    z[63]=z[80] + z[63];
    z[45]=4*z[63] + z[45];
    z[45]=z[11]*z[45];
    z[63]=z[51]*z[41];
    z[80]=7*z[14];
    z[81]= - z[68]*z[80];
    z[63]=z[63] + z[81];
    z[63]=z[9]*z[63];
    z[81]=npow(z[14],3);
    z[82]=z[81]*z[61];
    z[83]=3*z[51];
    z[68]= - z[68]*z[83];
    z[63]=z[63] + z[82] + z[68];
    z[63]=z[9]*z[63];
    z[68]=z[52] + z[51];
    z[82]=z[68]*z[7];
    z[84]= - z[14] + z[7];
    z[88]=z[13]*z[7];
    z[84]=z[88]*z[84];
    z[84]= - z[82] + z[84];
    z[84]=z[55]*z[84];
    z[89]=2*z[55];
    z[90]= - z[51]*z[89];
    z[91]=z[55]*z[13];
    z[92]= - z[14]*z[89];
    z[92]=z[92] - z[91];
    z[92]=z[13]*z[92];
    z[90]=z[90] + z[92];
    z[90]=z[9]*z[90];
    z[84]=2*z[84] + z[90];
    z[84]=z[8]*z[84];
    z[90]=4*z[52];
    z[83]= - z[83] + z[90];
    z[92]=z[10]*z[7];
    z[83]=z[92]*z[55]*z[83];
    z[93]=z[81]*z[19];
    z[94]=z[85]*z[12];
    z[94]=z[94] + z[93];
    z[95]=4*z[7];
    z[96]=z[94]*z[95];
    z[97]=z[44]*z[86];
    z[91]= - z[97]*z[91];
    z[98]=z[74]*z[46];
    z[45]=z[84] + z[45] + z[98] + z[63] + z[91] + z[96] + z[83];
    z[63]=5*z[9];
    z[83]=z[41] + z[63];
    z[83]=z[9]*z[83];
    z[84]=z[17] + z[19];
    z[91]=4*z[11];
    z[96]= - z[84]*z[91];
    z[98]=z[21]*z[26];
    z[28]=z[98] + z[96] - n<T>(20,3)*z[28] + z[83];
    z[28]=z[5]*z[28];
    z[83]=z[55]*z[62];
    z[83]=n<T>(2,3)*z[83] - z[37];
    z[96]=z[74]*z[34];
    z[40]= - z[41] + z[40];
    z[40]=z[11]*z[40];
    z[98]=3*z[74];
    z[40]= - z[98] + z[40];
    z[40]=z[11]*z[40];
    z[99]=z[22]*z[12];
    z[100]=3*z[99];
    z[28]=z[28] + z[40] + z[96] + 5*z[83] + z[100];
    z[28]=z[5]*z[28];
    z[40]=z[55]*z[77];
    z[83]= - z[25]*z[58];
    z[40]=z[40] + z[83];
    z[83]=z[22]*z[74];
    z[96]=z[11]*z[58];
    z[96]=z[98] + z[96];
    z[98]=npow(z[11],2);
    z[101]=3*z[98];
    z[96]=z[96]*z[101];
    z[28]=z[28] + z[96] + 5*z[40] - n<T>(11,2)*z[83];
    z[28]=z[5]*z[28];
    z[28]=z[28] + 2*z[45];
    z[28]=z[8]*z[28];
    z[40]=z[24]*z[55];
    z[45]= - z[70] + n<T>(38,3)*z[40] - z[41] + z[17];
    z[45]=z[11]*z[45];
    z[70]= - z[57] + 2*z[22] - n<T>(1,3)*z[74];
    z[70]=z[3]*z[70];
    z[83]=npow(z[2],2);
    z[96]=z[83]*z[55];
    z[102]=n<T>(2,3)*z[13] + n<T>(5,3)*z[96] + z[7];
    z[103]=n<T>(2,3)*z[11];
    z[70]=z[103] + 2*z[102] + z[9] + z[70];
    z[70]=z[70]*z[21];
    z[33]= - n<T>(22,3)*z[40] + 10*z[33];
    z[33]=z[9]*z[33];
    z[102]=n<T>(35,3)*z[15];
    z[104]=z[96]*z[102];
    z[105]=z[19] + 15*z[15];
    z[106]=10*z[7] - z[105];
    z[106]=z[13]*z[106];
    z[33]=z[70] + z[45] + z[104] + z[106] + z[33];
    z[33]=z[5]*z[33];
    z[45]=5*z[96];
    z[70]=z[30]*z[45];
    z[37]=z[70] - z[37];
    z[70]= - z[18] + z[13] - 3*z[40];
    z[70]=z[70]*z[101];
    z[33]=z[33] + z[70] + 5*z[37] + 6*z[99];
    z[33]=z[5]*z[33];
    z[37]=2*z[25] - z[78] - z[43];
    z[43]=npow(z[10],2);
    z[70]=z[55]*z[43];
    z[70]=z[70] - z[58];
    z[78]= - z[79]*z[76];
    z[70]=z[78] + 4*z[70] + z[9];
    z[70]=z[70]*z[18];
    z[78]=2*z[30];
    z[104]= - z[78] - 5*z[42];
    z[70]=z[70] + 2*z[104] + 27*z[49];
    z[70]=z[11]*z[70];
    z[37]=4*z[37] + z[70];
    z[37]=z[11]*z[37];
    z[70]= - 32*z[14] + 27*z[12];
    z[104]=z[70]*z[51];
    z[106]=5*z[12];
    z[107]=z[106] - z[54];
    z[107]=z[107]*z[90];
    z[104]=z[104] + z[107];
    z[104]=z[7]*z[104];
    z[70]=z[70] - z[58];
    z[70]=z[51]*z[70];
    z[107]=z[9]*z[14];
    z[48]=7*z[12] - z[48];
    z[48]=z[48]*z[107];
    z[48]=z[48] + z[70];
    z[48]=z[9]*z[48];
    z[70]= - 19*z[25] - z[82];
    z[70]=z[70]*z[58];
    z[28]=z[28] + z[33] + z[37] + z[48] + z[104] + z[70];
    z[28]=z[8]*z[28];
    z[33]=3*z[7];
    z[37]=z[33]*z[2];
    z[48]=n<T>(5,3) - z[37];
    z[70]=z[83]*z[7];
    z[82]= - n<T>(14,3)*z[2] + z[70];
    z[82]=z[6]*z[82];
    z[48]=2*z[48] + z[82];
    z[82]=5*z[6];
    z[48]=z[48]*z[82];
    z[104]=npow(z[4],4);
    z[108]=z[24]*z[104];
    z[109]=z[83]*z[6];
    z[110]=z[109]*z[108];
    z[111]=20*z[109];
    z[112]= - 41*z[2] + z[111];
    z[112]=z[6]*z[112];
    z[112]=static_cast<T>(22)+ z[112];
    z[112]=z[112]*z[21];
    z[113]=z[104]*npow(z[3],4);
    z[114]=z[11]*z[113];
    z[48]=z[112] - n<T>(4,3)*z[114] + n<T>(10,3)*z[110] - z[34] + z[48] + 45*
    z[7] + n<T>(26,3)*z[13];
    z[48]=z[5]*z[48];
    z[110]=z[104]*z[83];
    z[112]=4*z[3];
    z[114]= - z[2]*z[104]*z[112];
    z[114]= - n<T>(1,2)*z[110] + z[114];
    z[115]=npow(z[6],2);
    z[114]=z[24]*z[115]*z[114];
    z[116]=z[13]*z[19];
    z[117]= - z[19] + z[9];
    z[117]=z[9]*z[117];
    z[116]=z[116] + z[117];
    z[117]=z[101]*z[113];
    z[48]=z[48] + z[117] + 2*z[116] + n<T>(5,3)*z[114];
    z[48]=z[5]*z[48];
    z[114]=npow(z[11],3);
    z[116]=2*z[114];
    z[113]=z[113]*z[116];
    z[113]= - z[99] + z[113];
    z[48]=3*z[113] + z[48];
    z[48]=z[5]*z[48];
    z[113]=n<T>(20,3)*z[15];
    z[117]= - z[110]*z[113];
    z[118]=z[9]*z[108];
    z[119]=z[24]*z[11];
    z[120]=z[104]*z[119];
    z[23]=z[23] - n<T>(4,3)*z[120] + n<T>(2,3)*z[118] + z[117] - z[100];
    z[23]=z[5]*z[23];
    z[100]= - z[30]*z[110];
    z[117]=z[22]*z[108];
    z[100]=z[100] - n<T>(5,6)*z[117];
    z[101]=z[108]*z[101];
    z[23]=z[23] + 5*z[100] + z[101];
    z[23]=z[5]*z[23];
    z[100]=z[25]*z[110];
    z[101]=z[114]*z[108];
    z[100]= - 5*z[100] + 3*z[101];
    z[23]=2*z[100] + z[23];
    z[23]=z[5]*z[23];
    z[100]=2*z[81];
    z[85]=z[100] + z[85];
    z[101]=z[85]*z[7];
    z[108]= - z[9]*z[51];
    z[100]= - z[100] + z[108];
    z[100]=z[9]*z[100];
    z[100]= - z[101] + z[100];
    z[108]=z[43]*z[104];
    z[100]=z[108]*z[100];
    z[68]=z[68]*z[88];
    z[88]=z[49]*z[51];
    z[68]=z[68] + z[88];
    z[88]=npow(z[8],2)*z[104]*z[68];
    z[108]= - z[7]*z[108];
    z[46]=z[108] + z[46];
    z[46]=z[46]*z[114];
    z[46]=z[88] + 2*z[100] + z[46];
    z[23]=z[23] + 4*z[46];
    z[23]=z[8]*z[23];
    z[46]=z[38]*z[51];
    z[46]=z[46] + z[93];
    z[46]=z[46]*z[9];
    z[88]=z[94]*z[7];
    z[46]=z[46] + z[88];
    z[88]=npow(z[10],3);
    z[93]=z[88]*z[104]*z[7];
    z[93]=z[93] - z[49];
    z[93]=z[93]*z[114];
    z[93]=2*z[46] + z[93];
    z[94]=n<T>(1,3)*z[9];
    z[100]=z[103] - z[94];
    z[103]=npow(z[3],3);
    z[104]=z[103]*z[104];
    z[100]=z[104]*z[100];
    z[108]= - z[12] + 5*z[7];
    z[108]=z[108]*z[13];
    z[110]= - z[12] + z[9];
    z[110]=z[9]*z[110];
    z[100]= - z[108] + z[110] + z[100];
    z[57]=2*z[100] - z[57];
    z[57]=z[5]*z[57];
    z[98]= - z[98]*z[104];
    z[98]= - z[99] + z[98];
    z[57]=3*z[98] + z[57];
    z[57]=z[5]*z[57];
    z[98]=z[114]*z[104];
    z[57]= - 6*z[98] + z[57];
    z[57]=z[5]*z[57];
    z[57]=4*z[93] + z[57];
    z[23]=2*z[57] + z[23];
    z[23]=z[8]*z[23];
    z[57]=npow(z[4],5);
    z[93]=z[57]*z[103];
    z[98]=z[83]*z[115]*z[93];
    z[100]= - z[7]*z[109];
    z[37]=z[37] + z[100];
    z[37]=z[6]*z[37];
    z[37]= - z[33] + z[37];
    z[37]=z[5]*z[37];
    z[100]= - z[13]*z[12];
    z[37]=5*z[37] + n<T>(5,3)*z[98] + z[100] + z[38];
    z[37]=z[5]*z[37];
    z[37]= - z[99] + z[37];
    z[37]=z[37]*npow(z[5],2);
    z[57]= - z[57]*z[44]*z[88]*z[116];
    z[93]=z[22]*z[93];
    z[98]= - z[5]*z[99];
    z[93]= - n<T>(1,3)*z[93] + z[98];
    z[98]=npow(z[5],3);
    z[93]=z[93]*z[98];
    z[57]=z[57] + z[93];
    z[57]=z[8]*z[57];
    z[93]= - z[22]*z[19];
    z[38]=z[108] + z[38];
    z[38]=z[5]*z[38];
    z[38]=z[93] + z[38];
    z[38]=z[38]*z[98];
    z[38]=z[38] + z[57];
    z[38]=z[8]*z[38];
    z[37]=z[38] + 4*z[46] + z[37];
    z[37]=z[1]*z[37];
    z[38]=9*z[12] - z[54];
    z[38]=z[38]*z[53];
    z[38]=z[50] + z[38];
    z[38]=z[7]*z[38];
    z[46]= - n<T>(9,2)*z[12] - z[59];
    z[46]=z[46]*z[107];
    z[46]=z[50] + z[46];
    z[46]=z[9]*z[46];
    z[50]=z[114]*z[71];
    z[23]=2*z[37] + z[23] + z[48] + z[50] + z[38] + z[46];
    z[23]=z[1]*z[23];
    z[37]=z[3]*z[6];
    z[38]=2*z[37];
    z[46]=n<T>(1,3) - z[38];
    z[46]=z[46]*z[91];
    z[48]=3*z[6];
    z[50]=10*z[109];
    z[54]=11*z[2] - z[50];
    z[54]=z[54]*z[48];
    z[54]= - static_cast<T>(2)+ z[54];
    z[54]=z[54]*z[21];
    z[46]=z[54] + z[46];
    z[46]=z[3]*z[46];
    z[45]=z[45] + z[58];
    z[54]=z[55]*z[2];
    z[55]= - z[54] + z[74];
    z[57]=n<T>(1,3)*z[3];
    z[55]=z[55]*z[57];
    z[59]=z[6]*z[2];
    z[93]=35*z[59];
    z[98]= - n<T>(26,3) + z[93];
    z[98]=z[6]*z[98];
    z[45]=z[55] - z[9] + n<T>(2,3)*z[45] + z[98];
    z[45]=z[45]*z[76];
    z[55]=z[56]*z[83];
    z[98]=21*z[2] + z[55];
    z[98]=2*z[98] - n<T>(55,3)*z[109];
    z[98]=z[6]*z[98];
    z[99]=z[7]*z[2];
    z[45]=z[45] + z[98] - n<T>(89,3) - 8*z[99] + z[46];
    z[45]=z[5]*z[45];
    z[46]=4*z[6];
    z[98]= - z[46] - z[96] - z[13];
    z[100]=n<T>(5,3)*z[6];
    z[98]=z[98]*z[100];
    z[104]= - z[17] + z[9];
    z[104]=z[9]*z[104];
    z[98]=z[98] + z[104];
    z[104]=z[37]*z[54];
    z[98]=2*z[98] - n<T>(5,3)*z[104];
    z[98]=z[3]*z[98];
    z[40]=z[46] - n<T>(29,3)*z[40];
    z[40]=z[3]*z[40];
    z[46]=z[11]*z[3];
    z[104]=4*z[37];
    z[107]= - static_cast<T>(3)- z[104];
    z[107]=z[107]*z[46];
    z[40]=z[107] + n<T>(8,3) + z[40];
    z[40]=z[11]*z[40];
    z[107]=n<T>(5,2)*z[2] - z[70];
    z[107]=z[107]*z[82];
    z[107]=z[107] - n<T>(35,3) + 12*z[99];
    z[107]=z[6]*z[107];
    z[108]=n<T>(14,3)*z[13];
    z[40]=z[45] + z[40] + z[98] + z[34] + z[107] - 17*z[7] - z[108];
    z[40]=z[5]*z[40];
    z[45]=z[115]*z[96];
    z[54]=z[54] + 2*z[74];
    z[74]=z[115]*z[3];
    z[54]=z[54]*z[74];
    z[45]=n<T>(7,2)*z[45] + z[54];
    z[45]=z[3]*z[45];
    z[54]=z[63] + z[36] - n<T>(11,6)*z[13];
    z[54]=z[9]*z[54];
    z[63]=z[104] - 3;
    z[96]= - 6*z[46] - z[63];
    z[96]=z[11]*z[96];
    z[17]= - z[17] + z[6];
    z[17]=4*z[17] + z[96];
    z[17]=z[11]*z[17];
    z[96]= - z[13]*z[105];
    z[17]=z[40] + z[17] + n<T>(5,3)*z[45] + z[96] + z[54];
    z[17]=z[5]*z[17];
    z[40]=z[30]*z[10];
    z[45]=2*z[15];
    z[54]=z[45] - z[40];
    z[54]=z[10]*z[54];
    z[54]= - static_cast<T>(1)+ z[54];
    z[54]=z[6]*z[54];
    z[54]=z[54] + z[40] + z[47];
    z[96]= - z[43]*z[89];
    z[79]=z[3]*z[79];
    z[79]=z[96] + z[79];
    z[79]=z[3]*z[79];
    z[88]= - z[88]*z[89];
    z[89]=z[9]*z[10];
    z[79]=z[79] - z[89] + static_cast<T>(7)+ z[88];
    z[79]=z[79]*z[18];
    z[54]=z[79] + 4*z[54] - 15*z[9];
    z[54]=z[11]*z[54];
    z[79]=z[25]*z[86];
    z[88]=z[79] - z[30];
    z[31]= - z[31] + z[79];
    z[31]=z[31]*z[86];
    z[31]=z[31] + z[45] + z[7];
    z[31]=z[6]*z[31];
    z[31]= - n<T>(2,3)*z[49] - 2*z[88] + z[31];
    z[31]=2*z[31] + z[54];
    z[31]=z[11]*z[31];
    z[54]=z[10]*z[88];
    z[54]= - z[113] - 9*z[54];
    z[54]=z[6]*z[54];
    z[79]=z[10]*z[25];
    z[54]=z[54] + 9*z[30] - 46*z[79];
    z[54]=z[13]*z[54];
    z[79]=n<T>(39,2)*z[14];
    z[88]=z[106] - z[79];
    z[88]=z[88]*z[14];
    z[96]=z[12] - 2*z[14];
    z[64]=z[96]*z[64];
    z[64]=z[88] + z[64];
    z[64]=z[7]*z[64];
    z[96]=8*z[10];
    z[98]=z[96]*z[51];
    z[80]= - z[12] - z[80];
    z[80]=n<T>(1,2)*z[80] - z[98];
    z[80]=z[9]*z[80];
    z[105]=z[81]*z[10];
    z[105]=16*z[105];
    z[80]=z[80] + z[88] - z[105];
    z[80]=z[9]*z[80];
    z[88]=z[96]*z[101];
    z[17]=z[23] + z[28] + z[17] + z[31] + z[80] + z[64] - z[88] + z[54];
    z[17]=z[1]*z[17];
    z[23]=npow(z[16],2);
    z[28]=n<T>(5,2)*z[23];
    z[31]= - z[83]*z[28];
    z[31]= - static_cast<T>(2)+ z[31];
    z[31]=z[7]*z[31];
    z[54]=n<T>(1,2)*z[23];
    z[64]= - z[54] - z[30];
    z[64]=5*z[64] - z[90];
    z[64]=z[10]*z[64];
    z[80]=27*z[30] + n<T>(20,3)*z[32];
    z[80]=z[10]*z[80];
    z[80]= - n<T>(44,3)*z[15] + z[80];
    z[80]=z[10]*z[80];
    z[80]=n<T>(20,3) + z[80];
    z[80]=z[13]*z[80];
    z[90]= - z[70] - z[10];
    z[101]=n<T>(5,2)*z[6];
    z[90]=z[101]*z[16]*z[90];
    z[106]=n<T>(25,3)*z[15];
    z[107]=3*z[16];
    z[31]=z[90] + z[80] + z[64] + z[31] - z[107] - z[106];
    z[31]=z[6]*z[31];
    z[64]=z[13] - z[9];
    z[64]=z[64]*z[34];
    z[80]=z[13] + 8*z[6];
    z[80]=z[6]*z[80];
    z[64]=z[64] + n<T>(17,2)*z[32] + 10*z[80];
    z[64]=z[3]*z[64];
    z[80]=4*z[13];
    z[90]= - static_cast<T>(56)- 95*z[59];
    z[90]=z[6]*z[90];
    z[64]=z[64] - 10*z[9] + z[90] - 25*z[60] - z[80];
    z[64]=z[64]*z[57];
    z[90]=6*z[2];
    z[110]=z[90] + 25*z[109];
    z[110]=z[6]*z[110];
    z[113]=n<T>(10,3)*z[9];
    z[93]= - n<T>(23,3) - z[93];
    z[93]=z[6]*z[93];
    z[93]=z[93] - z[113];
    z[93]=z[93]*z[76];
    z[93]=z[93] + static_cast<T>(13)+ z[110];
    z[93]=z[3]*z[93];
    z[110]=3*z[2];
    z[111]= - z[110] + z[111];
    z[111]=z[6]*z[111];
    z[111]= - static_cast<T>(2)+ z[111];
    z[111]=z[111]*z[24]*z[21];
    z[114]= - static_cast<T>(7)+ z[104];
    z[114]=z[114]*z[119];
    z[115]= - 5*z[2] - n<T>(7,3)*z[109];
    z[93]=z[111] + 4*z[114] + 2*z[115] + z[93];
    z[93]=z[5]*z[93];
    z[111]=z[83]*z[32];
    z[114]=static_cast<T>(14)- 5*z[111];
    z[114]=n<T>(2,3)*z[114] + 7*z[99];
    z[55]=z[110] - z[55];
    z[55]=3*z[55] + n<T>(55,6)*z[109];
    z[55]=z[6]*z[55];
    z[115]=8*z[37];
    z[116]= - static_cast<T>(3)+ z[115];
    z[116]=z[116]*z[119];
    z[117]= - static_cast<T>(27)+ z[104];
    z[117]=z[3]*z[117];
    z[116]=z[117] + z[116];
    z[116]=z[11]*z[116];
    z[55]=z[93] + z[116] + z[64] + 2*z[114] + z[55];
    z[55]=z[5]*z[55];
    z[64]= - z[110] - n<T>(1,2)*z[70];
    z[64]=z[64]*z[82];
    z[70]=z[2]*z[56];
    z[64]=z[64] + z[70] + n<T>(23,3) + 10*z[111];
    z[64]=z[6]*z[64];
    z[69]= - z[60] + z[69];
    z[69]=n<T>(1,3)*z[69] + z[48];
    z[69]=z[69]*z[82];
    z[70]=n<T>(17,3)*z[13] - 7*z[9];
    z[70]=z[9]*z[70];
    z[69]=z[69] + z[70];
    z[70]=z[82]*z[32];
    z[35]= - z[9]*z[35];
    z[35]= - z[70] + z[35];
    z[35]=z[3]*z[35];
    z[35]=n<T>(1,2)*z[69] + z[35];
    z[35]=z[3]*z[35];
    z[69]=2*z[6];
    z[93]= - z[69] - n<T>(5,3)*z[67];
    z[93]=z[93]*z[112];
    z[112]= - static_cast<T>(13)+ z[115];
    z[112]=z[3]*z[112];
    z[112]=z[112] + 9*z[119];
    z[112]=z[11]*z[112];
    z[93]=z[112] - static_cast<T>(5)+ z[93];
    z[93]=z[93]*z[18];
    z[111]= - n<T>(95,3)*z[111] - n<T>(95,3);
    z[111]=z[15]*z[111];
    z[35]=z[55] + z[93] + z[35] - n<T>(37,3)*z[9] + z[64] + n<T>(20,3)*z[13] + 
    z[95] + z[111] - n<T>(1,2)*z[12];
    z[35]=z[5]*z[35];
    z[55]=z[96]*z[81];
    z[64]=11*z[51];
    z[81]= - z[64] - z[55];
    z[81]=z[10]*z[81];
    z[93]=4*z[10];
    z[96]=z[93]*z[51];
    z[73]= - z[73] - z[96];
    z[73]=z[10]*z[73];
    z[73]= - static_cast<T>(1)+ z[73];
    z[73]=z[9]*z[73];
    z[111]= - z[6]*z[86];
    z[111]=z[111] - 13;
    z[111]=z[14]*z[111];
    z[73]=z[73] + z[108] + z[81] - z[19] + z[111];
    z[73]=z[73]*z[34];
    z[81]= - z[78] - z[44];
    z[108]=3*z[10];
    z[81]=z[81]*z[108];
    z[111]=8*z[15];
    z[112]= - 6*z[40] + z[111] - z[7];
    z[112]=z[10]*z[112];
    z[114]=z[13]*z[10];
    z[112]= - z[114] - static_cast<T>(6)+ z[112];
    z[112]=z[6]*z[112];
    z[116]= - static_cast<T>(2)- 3*z[114];
    z[116]=z[13]*z[116];
    z[81]=z[112] + z[116] + z[81] + z[45] - z[33];
    z[112]=z[15] - z[33];
    z[112]=z[112]*z[86];
    z[116]=z[43]*z[6];
    z[117]=z[45]*z[116];
    z[112]=z[89] + z[117] - 6*z[114] - static_cast<T>(19)+ z[112];
    z[117]=z[86]*z[32];
    z[118]= - z[117] + z[48];
    z[67]=4*z[118] - 11*z[67];
    z[67]=z[3]*z[67];
    z[118]=z[11]*z[10];
    z[67]= - 24*z[118] + 2*z[112] + z[67];
    z[67]=z[11]*z[67];
    z[67]=z[67] + 2*z[81] + 17*z[9];
    z[67]=z[11]*z[67];
    z[54]= - z[12]*z[54];
    z[54]= - 6*z[25] + z[54];
    z[81]=8*z[52];
    z[64]= - z[64] - z[81];
    z[64]=z[64]*z[56];
    z[54]= - z[88] + 5*z[54] + z[64];
    z[54]=z[10]*z[54];
    z[64]=4*z[114];
    z[112]=z[32]*z[64];
    z[60]= - z[60] + n<T>(1,2)*z[72];
    z[60]=z[6]*z[60];
    z[60]=z[112] + 7*z[60];
    z[60]=z[60]*z[100];
    z[72]=z[58] - n<T>(1,2)*z[6];
    z[70]=z[72]*z[70];
    z[32]=z[32]*z[49];
    z[32]=z[70] + 2*z[32];
    z[32]=z[32]*z[57];
    z[42]= - z[42]*z[117];
    z[32]=z[32] + z[42] + z[60];
    z[32]=z[3]*z[32];
    z[42]=z[19]*z[114];
    z[60]= - z[45] - z[12];
    z[60]=2*z[60] - z[7];
    z[40]=z[42] + 2*z[60] + 55*z[40];
    z[40]=z[13]*z[40];
    z[42]= - z[23]*z[83];
    z[42]=static_cast<T>(3)+ z[42];
    z[42]=z[12]*z[42];
    z[60]=6*z[7];
    z[42]= - z[60] + n<T>(5,2)*z[42] - 26*z[14];
    z[42]=z[7]*z[42];
    z[70]= - z[107] + 4*z[15];
    z[70]=z[12]*z[70];
    z[17]=z[17] + z[20] + z[35] + z[67] + z[32] + z[73] + z[31] + z[40]
    + z[54] + z[42] + z[75] + z[70];
    z[17]=z[1]*z[17];
    z[20]=9*z[26];
    z[31]= - z[71] + z[20];
    z[31]=z[3]*z[31];
    z[32]=z[21]*z[103];
    z[22]=z[22]*z[32];
    z[22]=z[22] + 8*z[46] - 20*z[62] + z[31];
    z[22]=z[5]*z[22];
    z[31]= - z[30]*z[110];
    z[35]=n<T>(4,3)*z[15];
    z[31]=z[35] + z[31];
    z[40]=static_cast<T>(3)+ z[46];
    z[40]=z[40]*z[91];
    z[22]=z[22] + z[40] + z[20] + 5*z[31] - 6*z[9];
    z[22]=z[5]*z[22];
    z[31]= - z[25]*z[110];
    z[29]= - z[29] + z[30] + z[31];
    z[31]=z[61] - z[66];
    z[31]=z[9]*z[31];
    z[40]=z[18] - z[84];
    z[40]=z[40]*z[91];
    z[22]=z[22] + z[40] + 10*z[29] + z[31];
    z[22]=z[5]*z[22];
    z[29]=z[14] - z[80];
    z[29]=z[29]*z[58];
    z[31]= - z[79] - z[98];
    z[31]=z[9]*z[31];
    z[40]= - z[61] + n<T>(55,2)*z[14];
    z[40]=z[40]*z[14];
    z[29]=z[31] + z[29] - z[40] - z[105];
    z[29]=z[9]*z[29];
    z[31]= - z[36]*z[47];
    z[42]= - z[41] + z[7];
    z[42]=z[7]*z[42];
    z[31]=z[42] + z[31];
    z[42]=z[44]*z[10];
    z[44]=z[33] + z[42];
    z[44]=z[44]*z[18];
    z[31]=z[44] + 2*z[31] - n<T>(7,3)*z[49];
    z[31]=z[11]*z[31];
    z[44]=z[8]*z[68];
    z[25]=z[25]*z[2];
    z[46]= - z[78] + z[25];
    z[46]=z[46]*z[61];
    z[40]= - z[40] - z[81];
    z[40]=z[7]*z[40];
    z[33]=z[14] + z[33];
    z[33]=z[13]*z[33]*z[56];
    z[22]=6*z[44] + z[22] + z[31] + z[29] + z[33] - z[88] + z[46] + 
    z[40];
    z[22]=z[8]*z[22];
    z[29]=static_cast<T>(2)- n<T>(1,3)*z[39];
    z[29]=z[3]*z[29];
    z[31]= - z[9]*z[32];
    z[29]=z[31] - 14*z[119] + 10*z[2] + z[29];
    z[21]=z[29]*z[21];
    z[26]= - 14*z[9] - n<T>(29,2)*z[26];
    z[26]=z[3]*z[26];
    z[26]=z[26] + static_cast<T>(2)+ 25*z[62];
    z[29]= - n<T>(131,3)*z[3] - 3*z[119];
    z[29]=z[11]*z[29];
    z[21]=z[21] + n<T>(1,3)*z[26] + z[29];
    z[21]=z[5]*z[21];
    z[26]=z[35] + 9*z[77];
    z[29]= - 19*z[3] + 18*z[119];
    z[29]=z[11]*z[29];
    z[29]= - static_cast<T>(9)+ z[29];
    z[29]=z[11]*z[29];
    z[20]=z[21] + z[29] - z[20] - n<T>(253,6)*z[9] + z[65] + z[95] + 5*z[26]
    + z[41];
    z[20]=z[5]*z[20];
    z[21]= - z[85]*z[93];
    z[26]=7*z[51];
    z[21]=z[21] - z[26] - z[53];
    z[21]=z[21]*z[87];
    z[29]=z[45] - z[77];
    z[29]=z[29]*z[61];
    z[31]=21*z[14];
    z[33]= - z[31] - z[56];
    z[33]=z[7]*z[33];
    z[21]=z[21] + z[33] + z[29] + z[30] - 15*z[25];
    z[25]= - z[26] - z[55];
    z[25]=z[25]*z[86];
    z[25]= - z[31] + z[25];
    z[26]= - 5*z[14] - z[96];
    z[26]=z[26]*z[86];
    z[26]= - static_cast<T>(5)+ z[26];
    z[26]=z[26]*z[34];
    z[25]=z[26] + 2*z[25] + n<T>(223,6)*z[13];
    z[25]=z[9]*z[25];
    z[26]= - z[60] - 5*z[42];
    z[26]=z[10]*z[26];
    z[29]= - static_cast<T>(1)- z[87];
    z[29]=z[29]*z[93];
    z[30]=13*z[3];
    z[29]=z[29] + z[30];
    z[29]=z[11]*z[29];
    z[26]=z[29] - static_cast<T>(10)+ z[26];
    z[26]=z[11]*z[26];
    z[26]=z[26] - 19*z[13] + 6*z[15] - z[7];
    z[26]=z[26]*z[18];
    z[29]= - 8*z[13] + z[97] + z[60] - 25*z[15] - 12*z[12];
    z[29]=z[13]*z[29];
    z[20]=z[22] + z[20] + z[26] + z[25] + 2*z[21] + z[29];
    z[20]=z[8]*z[20];
    z[21]=5*z[109];
    z[22]= - z[90] - z[21];
    z[22]=z[22]*z[48];
    z[25]=static_cast<T>(8)+ n<T>(35,3)*z[59];
    z[25]=z[6]*z[25];
    z[25]=z[25] - z[94];
    z[25]=z[25]*z[76];
    z[22]=z[25] - static_cast<T>(29)+ z[22];
    z[22]=z[3]*z[22];
    z[25]=7*z[2];
    z[26]=z[25] - z[109];
    z[22]=n<T>(8,3)*z[26] + z[22];
    z[22]=z[3]*z[22];
    z[26]=static_cast<T>(7)- z[38];
    z[26]=z[26]*z[103]*z[91];
    z[29]= - z[21] - 4*z[2];
    z[29]=z[6]*z[29];
    z[29]= - static_cast<T>(3)+ z[29];
    z[29]=z[29]*z[32];
    z[22]=z[29] + z[26] - n<T>(20,3)*z[83] + z[22];
    z[22]=z[5]*z[22];
    z[26]=static_cast<T>(91)+ n<T>(155,2)*z[59];
    z[26]=z[6]*z[26];
    z[26]= - 40*z[74] + z[26] - z[66];
    z[26]=z[26]*z[57];
    z[29]=n<T>(1,3)*z[6];
    z[21]= - 29*z[2] - z[21];
    z[21]=z[21]*z[29];
    z[21]= - static_cast<T>(26)+ z[21];
    z[21]=2*z[21] + z[26];
    z[21]=z[3]*z[21];
    z[26]=static_cast<T>(25)- z[104];
    z[26]=z[26]*z[24];
    z[31]=z[103]*z[11];
    z[32]= - z[63]*z[31];
    z[26]=2*z[26] + z[32];
    z[26]=z[11]*z[26];
    z[32]=19*z[2] + 14*z[109];
    z[21]=z[22] + z[26] + n<T>(2,3)*z[32] + z[21];
    z[21]=z[5]*z[21];
    z[22]= - z[13] - n<T>(13,2)*z[6];
    z[22]=z[22]*z[82];
    z[22]=z[22] - z[49];
    z[22]=z[22]*z[57];
    z[26]=static_cast<T>(13)+ n<T>(40,3)*z[59];
    z[26]=z[6]*z[26];
    z[22]=z[22] - z[113] + n<T>(8,3)*z[13] + z[26];
    z[22]=z[3]*z[22];
    z[26]=static_cast<T>(9)+ z[104];
    z[26]=z[26]*z[76];
    z[32]=static_cast<T>(35)- 12*z[37];
    z[24]=z[32]*z[24];
    z[24]=z[24] - 18*z[31];
    z[24]=z[11]*z[24];
    z[24]=z[26] + z[24];
    z[24]=z[11]*z[24];
    z[25]=z[25] - z[50];
    z[25]=z[25]*z[29];
    z[26]= - static_cast<T>(28)- 29*z[62];
    z[29]= - z[2]*z[36];
    z[21]=z[21] + z[24] + z[22] + z[25] - 6*z[99] + n<T>(5,3)*z[26] + z[29];
    z[21]=z[5]*z[21];
    z[22]=static_cast<T>(8)+ z[92];
    z[22]=z[10]*z[22];
    z[22]=z[22] - z[116];
    z[24]=z[6]*z[10];
    z[24]= - z[115] + static_cast<T>(31)- 6*z[24];
    z[24]=z[3]*z[24];
    z[25]=z[93] - z[30];
    z[25]=z[3]*z[25];
    z[25]=8*z[43] + z[25];
    z[25]=z[11]*z[25];
    z[22]=z[25] + 2*z[22] + z[24];
    z[18]=z[22]*z[18];
    z[22]= - z[15] + z[56];
    z[22]=z[22]*z[93];
    z[24]=z[10]*z[15];
    z[25]= - static_cast<T>(1)+ z[24];
    z[25]=z[25]*z[86];
    z[26]=z[13]*z[43];
    z[25]=z[25] + z[26];
    z[25]=z[25]*z[69];
    z[18]=z[18] + 10*z[37] - z[89] + z[25] + 16*z[114] + static_cast<T>(9)+ z[22];
    z[18]=z[11]*z[18];
    z[22]=z[28]*z[10];
    z[23]=z[23]*z[2];
    z[22]= - z[22] - z[107] + 5*z[23];
    z[23]=n<T>(55,3)*z[15] - z[22];
    z[23]=z[10]*z[23];
    z[25]=z[28]*z[2];
    z[25]=z[25] - z[107];
    z[26]= - z[102] + z[25];
    z[26]=z[2]*z[26];
    z[28]=static_cast<T>(17)- 47*z[24];
    z[28]=z[28]*z[114];
    z[29]=z[2]*z[16];
    z[29]=n<T>(1,3) + z[29];
    z[29]=z[2]*z[29];
    z[30]=n<T>(1,2)*z[10] - z[2];
    z[30]=z[16]*z[30];
    z[30]=n<T>(1,2) + z[30];
    z[30]=z[10]*z[30];
    z[29]=n<T>(1,2)*z[29] + z[30];
    z[29]=z[29]*z[82];
    z[23]=z[29] + n<T>(1,3)*z[28] + z[23] - n<T>(13,6) + z[26];
    z[23]=z[6]*z[23];
    z[22]= - z[12]*z[22];
    z[22]=10*z[52] + z[22];
    z[22]=z[10]*z[22];
    z[19]= - z[27] + z[19];
    z[19]=3*z[19] + z[7];
    z[19]=z[19]*z[86];
    z[26]= - z[10]*z[12];
    z[26]=static_cast<T>(1)+ z[26];
    z[26]=z[26]*z[64];
    z[19]=z[26] + n<T>(127,6) + z[19];
    z[19]=z[13]*z[19];
    z[25]=z[111] + z[25];
    z[25]=z[2]*z[25];
    z[25]= - static_cast<T>(14)+ z[25];
    z[25]=z[12]*z[25];
    z[26]= - n<T>(5,6)*z[13] - z[69];
    z[26]=z[26]*z[82];
    z[26]=z[26] - n<T>(43,6)*z[49];
    z[26]=z[3]*z[26];
    z[27]=z[10]*z[14];
    z[27]=static_cast<T>(3)+ 10*z[27];
    z[27]=z[9]*z[27];
    z[17]=z[17] + z[20] + z[21] + z[18] + z[26] + z[27] + z[23] + z[19]
    + z[22] - n<T>(57,2)*z[7] + z[25] + n<T>(139,3)*z[15] + 20*z[77];
    z[17]=z[1]*z[17];
    z[18]= - n<T>(11,3)*z[2] - z[108];
    z[18]=z[18]*z[101];
    z[19]= - z[86] - 3*z[3];
    z[19]=z[19]*z[91];
    z[20]= - n<T>(5,3)*z[5] - 20*z[9] + n<T>(58,3)*z[13] - z[106] + z[95];
    z[20]=z[8]*z[20];
    z[21]= - 14*z[13] - z[101];
    z[21]=z[3]*z[21];
    z[22]=z[2] + n<T>(2,3)*z[3];
    z[22]=z[5]*z[22];

    r +=  - static_cast<T>(8)+ z[17] + z[18] + z[19] + z[20] + z[21] + 10*z[22] - n<T>(35,3)*z[24] + n<T>(115,3)*z[62] - n<T>(28,3)*z[114];
 
    return r;
}

template double qg_2lNLC_r1186(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1186(const std::array<dd_real,31>&);
#endif
