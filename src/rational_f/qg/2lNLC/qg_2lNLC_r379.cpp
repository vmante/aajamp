#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r379(const std::array<T,31>& k) {
  T z[59];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[2];
    z[7]=k[5];
    z[8]=k[6];
    z[9]=k[14];
    z[10]=k[3];
    z[11]=k[4];
    z[12]=k[7];
    z[13]=k[13];
    z[14]=k[9];
    z[15]=z[4]*z[13];
    z[16]=npow(z[9],2);
    z[17]=z[2]*z[4];
    z[18]=z[17] + z[15] + z[16];
    z[19]=npow(z[8],2);
    z[20]=z[19]*z[10];
    z[21]=n<T>(1,4)*z[4];
    z[22]= - z[21]*z[20];
    z[23]=z[5] - z[2];
    z[24]=z[19]*z[11];
    z[25]=z[23]*z[24];
    z[26]=n<T>(1,2)*z[4];
    z[27]=npow(z[13],2);
    z[28]=z[26]*z[27];
    z[29]=z[4]*z[19];
    z[29]= - z[28] + z[29];
    z[30]=n<T>(1,2)*z[7];
    z[29]=z[29]*z[30];
    z[18]=z[29] + n<T>(3,2)*z[25] + z[22] + z[27] - n<T>(7,4)*z[18];
    z[18]=z[7]*z[18];
    z[22]=n<T>(1,2)*z[17];
    z[25]= - z[19] + z[15] + z[22];
    z[25]=z[7]*z[25];
    z[29]=n<T>(1,2)*z[13];
    z[25]=z[25] + 2*z[24] + z[20] + z[2] - z[21] + z[9] + z[29];
    z[25]=z[7]*z[25];
    z[31]=z[3]*z[9];
    z[32]=z[5]*z[3];
    z[33]=n<T>(1,4)*z[19] + 2*z[31] + n<T>(1,2)*z[32];
    z[33]=z[11]*z[33];
    z[34]=n<T>(1,4)*z[3];
    z[35]=n<T>(1,2)*z[5];
    z[33]=z[33] - n<T>(3,2)*z[20] + z[35] + 3*z[9] + z[34];
    z[33]=z[11]*z[33];
    z[25]=z[25] - n<T>(1,4) + z[33];
    z[25]=z[12]*z[25];
    z[33]=n<T>(3,2)*z[32] - n<T>(9,2)*z[16] - z[31];
    z[36]=n<T>(3,2)*z[5];
    z[37]=z[2] + z[36];
    z[37]=z[37]*z[19];
    z[38]=n<T>(1,2)*z[16];
    z[39]= - z[3]*z[38];
    z[37]=z[39] + z[37];
    z[37]=z[11]*z[37];
    z[20]=z[3]*z[20];
    z[20]=z[37] + n<T>(1,2)*z[33] + z[20];
    z[20]=z[11]*z[20];
    z[33]=z[35] - z[2] + 3*z[4] + n<T>(3,2)*z[13] + z[9] + n<T>(5,2)*z[3];
    z[18]=z[25] + z[18] + n<T>(1,2)*z[33] + z[20];
    z[18]=z[12]*z[18];
    z[20]=z[21]*z[27];
    z[25]=z[4]*z[10];
    z[33]=z[7]*z[4];
    z[37]= - z[33] + z[25];
    z[39]=npow(z[8],3);
    z[37]=z[39]*z[37];
    z[40]=z[35] + z[2];
    z[41]=z[39]*z[11];
    z[42]= - z[40]*z[41];
    z[37]=z[42] - z[20] + z[37];
    z[37]=z[7]*z[37];
    z[22]= - z[16] + z[22];
    z[42]=z[39]*npow(z[11],2);
    z[43]=n<T>(1,2)*z[2];
    z[44]=z[43] - z[5];
    z[44]=z[44]*z[42];
    z[22]=z[37] + n<T>(1,2)*z[22] + z[44];
    z[22]=z[7]*z[22];
    z[37]= - z[10]*z[39];
    z[37]= - z[38] + z[37];
    z[37]=z[3]*z[37];
    z[38]= - z[35]*z[41];
    z[37]=z[38] + z[37];
    z[37]=z[11]*z[37];
    z[37]=z[37] - z[16] - n<T>(1,4)*z[32];
    z[37]=z[11]*z[37];
    z[38]=n<T>(1,4)*z[5];
    z[22]=z[22] - z[38] + z[37];
    z[22]=z[12]*z[22];
    z[37]=z[27]*z[33];
    z[37]=z[32] - z[37];
    z[22]=z[22] - z[17] - n<T>(3,4)*z[37];
    z[22]=z[12]*z[22];
    z[37]=z[16]*z[6];
    z[39]=z[5]*z[39]*z[37];
    z[44]=z[27]*z[2];
    z[41]= - z[44]*z[41];
    z[39]=z[39] + z[41];
    z[39]=z[39]*z[30];
    z[41]=z[44]*z[42];
    z[39]=z[41] + z[39];
    z[39]=z[39]*z[30];
    z[41]=3*z[3];
    z[42]=z[4]*z[41];
    z[45]= - z[41] + 5*z[4];
    z[46]= - z[2]*z[45];
    z[42]=z[42] + z[46];
    z[46]=n<T>(1,8)*z[5];
    z[42]=z[42]*z[46];
    z[47]=n<T>(3,8)*z[32];
    z[48]=z[1]*z[17]*z[47];
    z[49]=2*z[14];
    z[50]=z[49] + n<T>(11,8)*z[4];
    z[50]=z[2]*z[3]*z[50];
    z[22]=z[48] + z[22] + z[39] + z[42] - z[28] + z[50];
    z[22]=z[1]*z[22];
    z[28]=z[5]*z[9];
    z[39]= - z[28] - z[27] + n<T>(3,2)*z[17];
    z[42]=n<T>(1,4)*z[7];
    z[39]=z[39]*z[42];
    z[48]=n<T>(3,4)*z[6];
    z[50]= - z[28]*z[48];
    z[51]=3*z[2];
    z[52]=z[13] + z[21];
    z[52]=z[52]*z[51];
    z[52]=n<T>(11,2)*z[27] + z[52];
    z[53]=n<T>(1,2)*z[11];
    z[52]=z[52]*z[53];
    z[54]=z[27]*z[10];
    z[37]=z[39] + z[52] + z[50] - z[37] + n<T>(9,4)*z[54];
    z[37]=z[19]*z[37];
    z[39]=z[4]*z[14];
    z[50]= - z[39] - n<T>(15,8)*z[17];
    z[50]=z[5]*z[50];
    z[20]=z[20] + z[50] + z[37];
    z[20]=z[7]*z[20];
    z[37]= - z[17]*z[34];
    z[50]=z[32]*z[21];
    z[52]= - z[32]*z[48];
    z[52]= - 5*z[54] + z[52];
    z[52]=z[52]*z[19];
    z[55]= - z[4]*z[27];
    z[37]=z[52] + z[50] + z[55] + z[37];
    z[50]=z[2]*z[13];
    z[52]= - n<T>(1,2)*z[27] - z[50];
    z[24]=z[52]*z[24];
    z[24]=n<T>(1,2)*z[37] + z[24];
    z[24]=z[11]*z[24];
    z[37]=npow(z[10],2);
    z[52]=z[37]*z[27];
    z[55]=npow(z[6],2);
    z[47]=z[55]*z[47];
    z[47]= - 2*z[52] + z[47];
    z[19]=z[47]*z[19];
    z[47]= - z[3]*z[14];
    z[47]=z[47] - z[27];
    z[56]=z[29] - z[49] - n<T>(3,2)*z[3];
    z[56]=z[4]*z[56];
    z[57]=z[3]*z[6];
    z[58]= - static_cast<T>(1)+ n<T>(1,8)*z[57];
    z[58]=z[2]*z[58];
    z[45]=n<T>(1,8)*z[45] + z[58];
    z[45]=z[5]*z[45];
    z[58]=z[6]*z[49];
    z[58]=n<T>(7,8) + z[58];
    z[58]=z[3]*z[58];
    z[58]=n<T>(23,8)*z[4] - n<T>(1,4)*z[13] + z[14] + z[58];
    z[58]=z[2]*z[58];
    z[18]=z[22] + z[18] + z[20] + z[24] + z[19] + z[45] + z[58] + 2*
    z[47] + z[56];
    z[18]=z[1]*z[18];
    z[19]=3*z[13];
    z[20]=z[21] - z[3] + z[19];
    z[20]=z[2]*z[20];
    z[22]=z[3] - z[4];
    z[22]=z[5]*z[22];
    z[20]=n<T>(3,4)*z[22] + z[20] + 3*z[27] - n<T>(5,2)*z[16] - z[31];
    z[20]=z[20]*z[53];
    z[22]= - z[39] - n<T>(7,8)*z[17];
    z[22]=z[7]*z[22];
    z[22]=z[22] - n<T>(7,8)*z[2] - n<T>(3,4)*z[4] - z[14] - n<T>(1,4)*z[9];
    z[22]=z[5]*z[22];
    z[16]= - 3*z[16] - z[27];
    z[24]=n<T>(1,4)*z[11];
    z[39]= - z[44]*z[24];
    z[45]= - z[49] - z[13];
    z[45]=z[4]*z[45];
    z[47]= - z[13] + n<T>(15,2)*z[4];
    z[47]=z[2]*z[47];
    z[16]=z[39] + n<T>(1,4)*z[47] + n<T>(3,4)*z[16] + z[45] + z[22];
    z[16]=z[7]*z[16];
    z[22]=z[11]*z[31];
    z[22]=z[22] - z[36] - z[51] - z[13] + n<T>(13,2)*z[9] + z[3];
    z[22]=z[11]*z[22];
    z[15]=z[15] - z[17];
    z[15]=z[7]*z[15];
    z[29]=z[9] - z[29];
    z[15]=n<T>(3,2)*z[15] - z[5] + 5*z[29] + n<T>(7,2)*z[4];
    z[15]=z[7]*z[15];
    z[15]=z[15] - static_cast<T>(7)+ z[22];
    z[22]= - n<T>(5,2)*z[2] + z[5];
    z[22]=z[22]*z[53];
    z[29]=z[7]*z[17];
    z[29]=z[29] + z[5] - 7*z[4] + z[51];
    z[29]=z[29]*z[42];
    z[22]=z[29] - static_cast<T>(2)+ z[22];
    z[22]=z[7]*z[22];
    z[29]= - z[11]*z[32];
    z[29]= - 7*z[3] + z[29];
    z[24]=z[29]*z[24];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[11]*z[24];
    z[22]=z[24] + z[22];
    z[22]=z[12]*z[22];
    z[15]=n<T>(1,2)*z[15] + z[22];
    z[15]=z[12]*z[15];
    z[22]= - z[57] - 1;
    z[22]=z[49]*z[22];
    z[24]=z[13]*z[10];
    z[29]=n<T>(1,2) + z[24];
    z[29]=z[29]*z[19];
    z[31]=z[6]*z[14];
    z[31]= - z[57] + n<T>(9,8) + z[31];
    z[31]=z[2]*z[31];
    z[39]= - z[55]*z[41];
    z[39]= - z[6] + z[39];
    z[39]=z[39]*z[43];
    z[39]=z[39] + static_cast<T>(1)- n<T>(1,2)*z[57];
    z[39]=z[39]*z[38];
    z[15]=z[18] + z[15] + z[16] + z[20] + z[39] + z[31] - n<T>(11,4)*z[4] + 
    z[29] + z[9] + z[22];
    z[15]=z[1]*z[15];
    z[16]=z[3]*z[55];
    z[16]= - 9*z[6] + z[16];
    z[16]=z[16]*z[46];
    z[18]=n<T>(9,2) - z[57];
    z[20]=z[13]*z[37];
    z[20]=z[10] + z[20];
    z[20]=z[13]*z[20];
    z[16]=z[16] + n<T>(1,4)*z[18] + z[20];
    z[16]=z[8]*z[16];
    z[18]=n<T>(13,2) - z[57];
    z[18]=z[18]*z[38];
    z[20]=static_cast<T>(1)+ n<T>(1,2)*z[24];
    z[20]=z[13]*z[20];
    z[18]=z[18] + n<T>(1,8)*z[3] + z[20];
    z[18]=z[8]*z[18];
    z[20]=z[11]*z[8];
    z[22]=z[32]*z[20];
    z[18]=z[18] + n<T>(1,8)*z[22];
    z[18]=z[11]*z[18];
    z[22]= - z[9] + z[34];
    z[16]=z[18] + z[16] - z[36] + n<T>(1,2)*z[22] - z[19];
    z[16]=z[11]*z[16];
    z[18]= - z[37] + n<T>(1,4)*z[55];
    z[18]=z[3]*z[18];
    z[19]=z[55]*z[35];
    z[22]=npow(z[10],3)*z[27];
    z[18]=z[19] + z[22] - z[48] + z[18];
    z[18]=z[8]*z[18];
    z[19]= - z[38] - z[43] + z[9];
    z[19]=z[6]*z[19];
    z[18]=z[18] - z[24] - n<T>(1,4)*z[57] - n<T>(9,4) + z[19];
    z[19]=z[54] + z[21];
    z[21]=z[55]*z[9];
    z[22]= - z[6] + n<T>(1,4)*z[21];
    z[22]=z[22]*z[28];
    z[19]=n<T>(1,2)*z[19] + z[22];
    z[19]=z[8]*z[19];
    z[17]=z[27] + n<T>(1,8)*z[17];
    z[17]=z[8]*z[17];
    z[22]=z[20]*z[44];
    z[17]=z[17] + n<T>(1,2)*z[22];
    z[17]=z[11]*z[17];
    z[17]=z[19] + z[17];
    z[17]=z[7]*z[17];
    z[19]= - z[9]*z[6];
    z[19]=n<T>(1,4) + z[19];
    z[21]=z[6] - n<T>(1,2)*z[21];
    z[21]=z[5]*z[21];
    z[19]=z[21] + n<T>(1,2)*z[19] - z[52];
    z[19]=z[8]*z[19];
    z[21]= - z[27] - z[50];
    z[21]=z[21]*z[20];
    z[22]= - static_cast<T>(1)- 2*z[24];
    z[22]=z[13]*z[22];
    z[22]=z[22] + n<T>(1,8)*z[2];
    z[22]=z[8]*z[22];
    z[21]=z[22] + z[21];
    z[21]=z[11]*z[21];
    z[22]=z[5] + n<T>(3,2)*z[2] - z[26] + 7*z[9] - z[13];
    z[17]=z[17] + z[21] + n<T>(1,4)*z[22] + z[19];
    z[17]=z[7]*z[17];
    z[19]= - z[37]*z[26];
    z[19]=z[10] + z[19];
    z[19]=z[8]*z[19];
    z[21]=z[40]*z[20];
    z[22]=n<T>(1,2)*z[8];
    z[21]=z[22] + z[21];
    z[21]=z[11]*z[21];
    z[24]=z[8]*z[4];
    z[26]= - z[30]*z[24];
    z[25]= - n<T>(1,2) + z[25];
    z[25]=z[8]*z[25];
    z[25]=z[25] + z[26];
    z[25]=z[7]*z[25];
    z[19]=z[25] + z[19] + z[21];
    z[19]=z[19]*z[30];
    z[21]=z[20]*z[35];
    z[21]= - z[8] + z[21];
    z[21]=z[11]*z[21];
    z[25]=z[22]*z[10];
    z[21]= - z[25] + z[21];
    z[21]=z[11]*z[21];
    z[26]=z[37]*z[8];
    z[19]=z[19] - n<T>(1,4)*z[26] + z[21];
    z[19]=z[7]*z[19];
    z[21]=z[37]*z[22];
    z[22]=z[20]*z[5];
    z[27]=z[8] + z[22];
    z[27]=z[27]*z[53];
    z[28]=z[37]*z[3];
    z[29]=n<T>(5,2)*z[10] - z[28];
    z[29]=z[8]*z[29];
    z[27]=z[29] + z[27];
    z[27]=z[11]*z[27];
    z[21]=z[21] + z[27];
    z[21]=z[21]*z[53];
    z[19]=z[21] + z[19];
    z[19]=z[12]*z[19];
    z[20]= - z[23]*z[20];
    z[20]=n<T>(3,4)*z[8] + z[20];
    z[20]=z[11]*z[20];
    z[21]= - z[10]*z[24];
    z[23]=z[8]*z[33];
    z[21]=z[21] + z[23];
    z[21]=z[21]*z[42];
    z[23]= - static_cast<T>(1)+ z[25];
    z[20]=z[21] + n<T>(1,2)*z[23] + z[20];
    z[20]=z[7]*z[20];
    z[21]= - n<T>(7,4)*z[10] + z[28];
    z[21]=z[8]*z[21];
    z[22]= - 2*z[8] - z[22];
    z[22]=z[11]*z[22];
    z[21]=z[22] + static_cast<T>(3)+ z[21];
    z[21]=z[11]*z[21];
    z[22]=3*z[10] - z[26];
    z[19]=z[19] + z[20] + n<T>(1,4)*z[22] + z[21];
    z[19]=z[12]*z[19];

    r += z[15] + z[16] + z[17] + n<T>(1,2)*z[18] + z[19];
 
    return r;
}

template double qg_2lNLC_r379(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r379(const std::array<dd_real,31>&);
#endif
