#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r39(const std::array<T,31>& k) {
  T z[50];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[12];
    z[6]=k[15];
    z[7]=k[2];
    z[8]=k[9];
    z[9]=k[4];
    z[10]=k[7];
    z[11]=k[14];
    z[12]=k[5];
    z[13]=k[13];
    z[14]=k[3];
    z[15]=n<T>(7,2)*z[9];
    z[16]=npow(z[9],2);
    z[17]=z[16]*z[10];
    z[18]= - z[15] + 3*z[17];
    z[18]=z[4]*z[18];
    z[19]=z[10]*z[9];
    z[20]=4*z[19];
    z[21]=5*z[9];
    z[22]= - z[5]*z[21];
    z[22]= - static_cast<T>(1)+ z[22];
    z[18]=z[18] + n<T>(1,2)*z[22] + z[20];
    z[18]=z[4]*z[18];
    z[22]=9*z[11];
    z[23]= - z[4]*z[9];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[22];
    z[24]=n<T>(1,2)*z[13];
    z[25]=n<T>(1,2)*z[12];
    z[26]= - z[13]*z[25];
    z[26]= - static_cast<T>(2)+ z[26];
    z[26]=z[5]*z[26];
    z[27]=z[9]*z[24];
    z[27]=static_cast<T>(2)+ z[27];
    z[27]=z[3]*z[27];
    z[28]=z[3]*z[7];
    z[29]= - static_cast<T>(1)- n<T>(1,2)*z[28];
    z[29]=z[6]*z[29];
    z[30]=z[21]*z[3];
    z[30]=z[30] + 7;
    z[30]=n<T>(1,2)*z[30];
    z[31]= - z[10]*z[30];
    z[18]=z[23] + z[18] + z[31] + z[29] + z[27] - z[24] + z[26];
    z[18]=z[11]*z[18];
    z[23]=4*z[8];
    z[24]=z[23] + z[13];
    z[26]=npow(z[13],2);
    z[27]=z[26]*z[12];
    z[29]=z[27] + z[24];
    z[29]=z[5]*z[29];
    z[29]=z[26] + z[29];
    z[31]=2*z[9];
    z[32]= - z[26]*z[31];
    z[24]=z[32] - z[24];
    z[24]=z[3]*z[24];
    z[32]=2*z[10];
    z[33]= - z[6] + z[32];
    z[33]=z[33]*z[32];
    z[34]=z[19] - 1;
    z[35]=z[10]*z[34];
    z[36]=n<T>(7,2) - z[19];
    z[36]=z[4]*z[36];
    z[35]=z[36] + 6*z[35] + n<T>(11,2)*z[5] - 8*z[6];
    z[35]=z[4]*z[35];
    z[36]= - 6*z[8] + z[5];
    z[36]=2*z[36] + n<T>(7,2)*z[3];
    z[36]=z[6]*z[36];
    z[18]=z[18] + z[35] + z[33] + z[36] + 2*z[29] + z[24];
    z[18]=z[11]*z[18];
    z[24]=2*z[28];
    z[29]=npow(z[8],2);
    z[33]= - z[29]*z[24];
    z[35]=z[6]*z[5];
    z[36]=z[10]*z[6];
    z[33]=z[36] + z[33] - n<T>(3,2)*z[35];
    z[33]=z[4]*z[33];
    z[36]=npow(z[5],2);
    z[37]=z[29] - z[36];
    z[38]=2*z[3];
    z[37]=z[37]*z[38];
    z[39]=2*z[5];
    z[40]= - z[8] - z[5];
    z[40]=z[40]*z[39];
    z[41]=z[3]*z[5];
    z[40]=z[40] + 5*z[41];
    z[40]=z[6]*z[40];
    z[42]=2*z[6];
    z[43]=z[1]*z[42]*z[36]*z[3];
    z[33]=z[43] + z[33] + z[37] + z[40];
    z[33]=z[4]*z[33];
    z[37]=n<T>(1,2)*z[5];
    z[40]= - z[9]*z[37];
    z[40]=z[40] - z[19];
    z[40]=z[4]*z[40];
    z[43]=z[32]*z[9];
    z[44]= - static_cast<T>(1)- z[43];
    z[44]=z[10]*z[44];
    z[40]=z[40] + z[37] + z[44];
    z[40]=z[4]*z[40];
    z[44]=z[17]*z[4];
    z[43]= - z[43] - z[44];
    z[43]=z[4]*z[43];
    z[43]= - z[10] + z[43];
    z[43]=z[11]*z[43];
    z[37]= - z[13]*z[37];
    z[45]=n<T>(3,2)*z[3] - z[32];
    z[45]=z[10]*z[45];
    z[37]=3*z[43] + z[40] + z[37] + z[45];
    z[37]=z[11]*z[37];
    z[40]=z[5]*z[26];
    z[43]= - z[8]*z[35];
    z[40]=z[40] + z[43];
    z[43]=n<T>(3,2)*z[5] - z[10];
    z[43]=z[4]*z[43];
    z[43]= - 7*z[35] + z[43];
    z[43]=z[4]*z[43];
    z[37]=z[37] + 2*z[40] + z[43];
    z[37]=z[11]*z[37];
    z[36]=z[38]*z[36];
    z[38]= - z[5]*z[29];
    z[38]=z[38] + z[36];
    z[38]=z[6]*z[38];
    z[40]=z[5]*z[12];
    z[43]=z[10]*z[40];
    z[43]= - z[5] + z[43];
    z[43]=z[10]*z[26]*z[43];
    z[38]=z[38] + z[43];
    z[33]=z[37] + 2*z[38] + z[33];
    z[33]=z[1]*z[33];
    z[24]=z[19] - n<T>(15,2) - z[24];
    z[24]=z[6]*z[24];
    z[37]=z[7]*z[29];
    z[38]=z[7]*z[8];
    z[43]= - z[3]*z[38];
    z[37]=z[43] - z[8] + z[37];
    z[24]=2*z[37] + z[24];
    z[24]=z[4]*z[24];
    z[37]=z[13] + z[8];
    z[37]=z[3]*z[37];
    z[37]=z[29] + z[37];
    z[43]=z[42] + z[13] - z[5];
    z[43]=z[43]*z[32];
    z[45]=5*z[3] + 2*z[8] - n<T>(5,2)*z[5];
    z[45]=z[6]*z[45];
    z[24]=z[24] + z[43] + 2*z[37] + z[45];
    z[24]=z[4]*z[24];
    z[29]= - z[26] + z[29];
    z[29]=z[5]*z[29];
    z[29]=z[29] - z[36];
    z[23]= - z[23] - z[5];
    z[23]=z[23]*z[39];
    z[36]=2*z[40];
    z[37]=static_cast<T>(13)+ z[36];
    z[37]=z[37]*z[41];
    z[23]=z[23] + z[37];
    z[23]=z[6]*z[23];
    z[37]=z[5]*z[13];
    z[35]=z[35] - 2*z[26] + z[37];
    z[37]=static_cast<T>(1)+ 3*z[40];
    z[39]= - z[32]*z[13]*z[37];
    z[43]= - 4*z[13] + 5*z[5];
    z[43]=z[3]*z[43];
    z[35]=z[39] + z[43] + 2*z[35];
    z[35]=z[10]*z[35];
    z[18]=z[33] + z[18] + z[24] + z[35] + 2*z[29] + z[23];
    z[18]=z[1]*z[18];
    z[23]=7*z[9];
    z[24]=z[12]*z[8];
    z[29]= - static_cast<T>(9)- z[24];
    z[29]=z[12]*z[29];
    z[29]=z[29] - z[23];
    z[29]=z[5]*z[29];
    z[33]=static_cast<T>(5)- z[38];
    z[33]=z[7]*z[33];
    z[33]=z[33] - z[21];
    z[33]=z[3]*z[33];
    z[29]=z[33] + z[29] - z[38] - static_cast<T>(7)- z[24];
    z[33]=2*z[12];
    z[35]=npow(z[7],2);
    z[39]=z[35]*z[3];
    z[43]=z[39] + z[33] - z[7];
    z[43]=z[6]*z[43];
    z[45]=3*z[7];
    z[21]=z[45] - z[21];
    z[21]=n<T>(1,2)*z[21] - z[17];
    z[21]=z[4]*z[21];
    z[46]=z[3]*z[17];
    z[22]= - z[7]*z[22];
    z[21]=z[22] + z[21] + z[46] + n<T>(1,2)*z[29] + z[43];
    z[21]=z[11]*z[21];
    z[22]=5*z[40];
    z[29]= - n<T>(7,2)*z[28] + z[22] + static_cast<T>(1)- n<T>(25,2)*z[38];
    z[29]=z[6]*z[29];
    z[43]=z[16]*z[32];
    z[46]=2*z[7];
    z[47]=3*z[9];
    z[43]=z[43] + z[46] - z[47];
    z[43]=z[4]*z[43];
    z[48]=z[6]*z[7];
    z[48]= - static_cast<T>(15)- 13*z[48];
    z[19]=z[43] + n<T>(1,2)*z[48] + 6*z[19];
    z[19]=z[4]*z[19];
    z[43]=5*z[8] + 9*z[13];
    z[48]=z[43]*z[25];
    z[48]=static_cast<T>(4)+ z[48];
    z[48]=z[5]*z[48];
    z[49]=n<T>(3,2) - 2*z[38];
    z[15]= - z[13]*z[15];
    z[15]=5*z[49] + z[15];
    z[15]=z[3]*z[15];
    z[49]=z[6]*z[12];
    z[30]=z[30] - 7*z[49];
    z[30]=z[10]*z[30];
    z[15]=z[21] + z[19] + z[30] + z[29] + z[15] + n<T>(1,2)*z[43] + z[48];
    z[15]=z[11]*z[15];
    z[19]=z[3]*z[12];
    z[21]=z[9] + z[12];
    z[29]= - z[6]*z[21];
    z[19]=z[29] + z[19] + z[37];
    z[19]=z[19]*z[32];
    z[29]=z[13]*z[31];
    z[29]=n<T>(5,2)*z[40] + n<T>(9,2) + z[29];
    z[29]=z[3]*z[29];
    z[30]=z[12]*z[13];
    z[30]= - static_cast<T>(5)- n<T>(13,2)*z[30];
    z[30]=z[5]*z[30];
    z[22]=static_cast<T>(3)- z[22];
    z[22]=z[6]*z[22];
    z[19]=z[19] + z[22] + z[29] + 5*z[13] + z[30];
    z[19]=z[10]*z[19];
    z[22]=3*z[6];
    z[17]= - z[17]*z[22];
    z[29]= - 5*z[7] + z[23];
    z[29]=z[6]*z[29];
    z[17]=z[17] + z[28] + n<T>(3,2)*z[29];
    z[17]=z[4]*z[17];
    z[29]=10*z[38];
    z[28]=6*z[28] + static_cast<T>(11)+ z[29];
    z[28]=z[6]*z[28];
    z[30]=z[6]*z[9];
    z[20]= - z[20] - static_cast<T>(1)- 4*z[30];
    z[20]=z[10]*z[20];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[3]*z[29];
    z[17]=z[17] + z[20] + z[29] + z[28];
    z[17]=z[4]*z[17];
    z[20]=z[40] + 1;
    z[28]=z[3]*z[20];
    z[28]=z[28] - z[8];
    z[29]=static_cast<T>(1)- n<T>(5,2)*z[24];
    z[29]=z[5]*z[29];
    z[28]=z[29] + n<T>(5,2)*z[28];
    z[28]=z[6]*z[28];
    z[29]= - n<T>(5,2) - z[36];
    z[29]=z[5]*z[29];
    z[29]=3*z[13] + z[29];
    z[29]=z[3]*z[29];
    z[15]=z[18] + z[15] + z[17] + z[19] + z[28] - 4*z[26] + z[29];
    z[15]=z[1]*z[15];
    z[17]= - static_cast<T>(3)- z[24];
    z[17]=z[17]*z[25];
    z[18]=n<T>(1,2)*z[24];
    z[19]= - n<T>(1,2)*z[38] - static_cast<T>(7)- z[18];
    z[19]=z[7]*z[19];
    z[24]=npow(z[12],2);
    z[18]= - static_cast<T>(2)- z[18];
    z[18]=z[18]*z[24];
    z[25]=z[9]*z[12];
    z[18]=z[18] - n<T>(3,2)*z[25];
    z[18]=z[5]*z[18];
    z[28]=npow(z[7],3);
    z[29]=z[28]*z[3];
    z[37]= - 6*z[12] - n<T>(13,2)*z[7];
    z[37]=z[7]*z[37];
    z[37]=z[37] - n<T>(1,2)*z[29];
    z[37]=z[6]*z[37];
    z[40]=z[11]*z[12]*z[35]*z[22];
    z[43]=z[29]*z[8];
    z[17]=z[40] + z[37] - n<T>(1,2)*z[43] + z[18] + z[17] + z[19];
    z[17]=z[11]*z[17];
    z[18]=static_cast<T>(4)- z[38];
    z[18]=z[18]*z[45];
    z[19]=n<T>(3,2)*z[39];
    z[18]= - z[19] + z[12] + z[18];
    z[18]=z[6]*z[18];
    z[37]=z[35]*z[42];
    z[39]=z[45] - z[9];
    z[37]=n<T>(1,2)*z[39] + z[37];
    z[37]=z[4]*z[37];
    z[33]=z[33] + z[9];
    z[33]=z[5]*z[33];
    z[39]=3*z[38];
    z[40]=n<T>(7,2) - z[39];
    z[40]=z[7]*z[40];
    z[40]=z[40] - n<T>(5,2)*z[9];
    z[40]=z[3]*z[40];
    z[17]=z[17] + z[37] + z[18] + z[40] + static_cast<T>(6)+ z[33];
    z[17]=z[11]*z[17];
    z[18]= - static_cast<T>(2)+ z[39];
    z[18]=z[7]*z[18];
    z[18]=z[19] + z[18] - z[47];
    z[18]=z[6]*z[18];
    z[19]=z[46] - n<T>(3,2)*z[9];
    z[19]=z[19]*z[47];
    z[19]= - n<T>(5,2)*z[35] + z[19];
    z[19]=z[6]*z[19];
    z[33]=z[6]*npow(z[9],3);
    z[33]= - z[16] + z[33];
    z[33]=z[10]*z[33];
    z[35]=z[31] - z[7];
    z[19]=z[33] + z[19] + z[35];
    z[19]=z[4]*z[19];
    z[33]= - static_cast<T>(1)+ z[38];
    z[33]=z[33]*z[45];
    z[33]=z[33] + z[9];
    z[33]=z[3]*z[33];
    z[34]= - z[14]*z[34];
    z[16]=z[6]*z[16];
    z[16]=z[16] + z[9] + z[34];
    z[16]=z[16]*z[32];
    z[16]=z[19] + z[16] + z[18] - static_cast<T>(2)+ z[33];
    z[16]=z[4]*z[16];
    z[18]=z[12] - z[14];
    z[18]=z[18]*z[20];
    z[19]=z[21]*z[30];
    z[20]= - z[3]*z[25];
    z[18]=z[19] + z[20] + z[47] + z[18];
    z[18]=z[18]*z[32];
    z[19]=z[24]*z[5];
    z[20]=z[19] + z[12];
    z[20]= - z[23] + 3*z[20];
    z[20]=z[3]*z[20];
    z[21]= - static_cast<T>(3)- z[36];
    z[23]= - z[6]*z[47];
    z[18]=z[18] + z[23] + 2*z[21] + z[20];
    z[18]=z[10]*z[18];
    z[20]=z[13]*z[14];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[13]*z[20];
    z[21]=z[9]*z[26];
    z[20]=z[21] + z[20] - z[27];
    z[21]=2*z[13] - z[27];
    z[21]=z[21]*z[31];
    z[23]= - n<T>(5,2)*z[12] + z[9];
    z[23]=z[5]*z[23];
    z[21]=z[23] + n<T>(1,2) + z[21];
    z[21]=z[3]*z[21];
    z[15]=z[15] + z[17] + z[16] + z[18] - z[22] + 2*z[20] + z[21];
    z[15]=z[1]*z[15];
    z[16]= - z[24] + z[25];
    z[16]=z[16]*z[41];
    z[17]=z[9] - z[12];
    z[18]=z[9]*z[17];
    z[18]=z[24] + z[18];
    z[18]=z[3]*z[18];
    z[18]= - z[19] + z[18];
    z[18]=z[10]*z[18];
    z[19]=z[24] + z[25];
    z[19]=z[11]*z[5]*z[19];
    z[16]=z[19] - z[44] + z[16] + z[18];
    z[16]=z[16]*npow(z[1],2);
    z[18]=z[28]*z[8];
    z[18]=z[18] + z[29];
    z[18]=z[18]*z[6];
    z[18]=z[18] + z[43];
    z[19]=z[11] - z[4];
    z[19]= - n<T>(1,2)*z[19];
    z[18]=z[18]*z[19];
    z[19]=z[10]*npow(z[12],3)*z[41];
    z[18]=z[19] + z[18];
    z[18]=z[2]*z[18]*npow(z[1],3);
    z[16]=z[16] + z[18];
    z[18]=npow(z[2],2);
    z[16]=z[16]*z[18];
    z[17]=z[10]*z[17];
    z[19]=z[4]*z[35];
    z[20]=z[12] + z[7];
    z[20]=z[11]*z[20];
    z[15]=z[16] + z[15] + z[20] + z[19] - static_cast<T>(1)+ z[17];

    r += z[18]*z[15];
 
    return r;
}

template double qg_2lNLC_r39(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r39(const std::array<dd_real,31>&);
#endif
