#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r955(const std::array<T,31>& k) {
  T z[103];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[7];
    z[9]=k[5];
    z[10]=k[2];
    z[11]=k[14];
    z[12]=k[3];
    z[13]=k[13];
    z[14]=k[9];
    z[15]=3*z[10];
    z[16]=npow(z[5],3);
    z[17]=npow(z[7],4);
    z[18]= - z[17]*z[16]*z[15];
    z[19]=npow(z[3],2);
    z[20]=6*z[19];
    z[21]=z[20]*z[17];
    z[22]=3*z[5];
    z[23]=z[22]*z[10];
    z[24]=z[8]*z[12];
    z[25]= - z[24] + z[23];
    z[25]=z[25]*z[21];
    z[26]=npow(z[8],3);
    z[27]=z[26]*z[17];
    z[28]= - z[12]*z[27];
    z[18]=z[25] + z[28] + z[18];
    z[18]=z[3]*z[18];
    z[25]=npow(z[8],2);
    z[28]=5*z[25];
    z[29]=npow(z[5],2);
    z[30]= - z[28] + 3*z[29];
    z[30]=z[5]*z[17]*z[30];
    z[31]=z[8] - z[22];
    z[21]=z[31]*z[21];
    z[21]=z[21] + z[27] + z[30];
    z[21]=z[3]*z[21];
    z[30]=z[29]*z[8];
    z[31]= - z[17]*z[30];
    z[31]=z[27] + z[31];
    z[31]=z[5]*z[31];
    z[21]=2*z[31] + z[21];
    z[21]=z[6]*z[21];
    z[31]=z[8]*z[9];
    z[32]= - z[17]*z[29]*z[31];
    z[33]=z[9]*z[27];
    z[32]=z[33] + z[32];
    z[32]=z[5]*z[32];
    z[18]=z[21] + z[32] + z[18];
    z[18]=z[6]*z[18];
    z[21]=npow(z[13],3);
    z[32]=4*z[21];
    z[33]=z[17]*z[32];
    z[33]=z[33] - z[27];
    z[34]=npow(z[9],2);
    z[35]=z[34]*z[2];
    z[33]=z[35]*z[33];
    z[36]=npow(z[10],2);
    z[37]=z[36]*z[16];
    z[38]=z[36]*z[5];
    z[39]= - z[38]*z[20];
    z[37]=z[37] + z[39];
    z[37]=z[3]*z[17]*z[37];
    z[18]=z[18] + z[37] + z[33];
    z[18]=z[6]*z[18];
    z[33]=npow(z[9],3);
    z[27]=z[33]*z[27];
    z[37]=z[2]*z[27];
    z[39]=z[2]*z[4];
    z[40]=z[5]*z[4];
    z[40]=z[39] + z[40];
    z[40]=z[40]*z[19];
    z[41]= - z[4]*z[16];
    z[40]=z[41] + n<T>(11,3)*z[40];
    z[40]=z[3]*z[40];
    z[18]=z[18] + z[37] + z[40];
    z[18]=z[6]*z[18];
    z[37]=npow(z[7],5);
    z[40]=z[37]*z[26];
    z[41]= - z[37]*z[30];
    z[41]=z[40] + z[41];
    z[41]=z[5]*z[41];
    z[42]=z[5]*z[8];
    z[20]=z[20]*z[42];
    z[37]=z[37]*z[20];
    z[37]=z[41] + z[37];
    z[37]=z[37]*npow(z[6],5);
    z[41]=z[2] - z[4];
    z[43]=z[41]*z[16];
    z[44]= - z[5]*z[41];
    z[44]=z[39] + z[44];
    z[45]=11*z[3];
    z[44]=z[44]*z[45];
    z[46]=49*z[39];
    z[47]= - z[5]*z[46];
    z[44]=z[47] + z[44];
    z[47]=n<T>(1,3)*z[3];
    z[44]=z[44]*z[47];
    z[48]=n<T>(11,3)*z[5];
    z[49]= - z[19]*z[48];
    z[49]=z[16] + z[49];
    z[49]=z[1]*z[39]*z[49];
    z[37]=z[49] + z[37] + z[43] + z[44];
    z[37]=z[3]*z[37];
    z[40]= - npow(z[9],5)*z[40];
    z[40]=z[40] + 3*z[16];
    z[40]=z[39]*z[40];
    z[37]=z[37] + z[40];
    z[37]=z[1]*z[37];
    z[40]=npow(z[11],3);
    z[43]=8*z[40];
    z[17]=z[36]*z[34]*z[17]*z[43];
    z[44]=2*z[2];
    z[49]=z[39]*z[9];
    z[50]=3*z[49];
    z[51]=z[50] - 3*z[4] + z[44];
    z[51]=z[51]*z[29];
    z[17]=z[17] + z[51];
    z[17]=z[5]*z[17];
    z[51]=z[4]*z[12];
    z[52]=2*z[4];
    z[53]=z[52] - z[2];
    z[53]=z[9]*z[53];
    z[53]= - z[51] + z[53];
    z[27]=z[53]*z[27];
    z[53]=z[2]*z[10];
    z[54]=z[53] - 1;
    z[55]=z[54]*z[5];
    z[56]=z[2] - z[55];
    z[45]=z[56]*z[45];
    z[56]=49*z[4] - 38*z[2];
    z[56]=z[5]*z[56];
    z[45]=z[45] + z[46] + z[56];
    z[45]=z[45]*z[47];
    z[46]=z[55] + z[41];
    z[46]=z[5]*z[46];
    z[46]= - 22*z[39] + z[46];
    z[46]=z[5]*z[46];
    z[45]=z[46] + z[45];
    z[45]=z[3]*z[45];
    z[17]=z[37] + z[18] + z[45] + z[27] + z[17];
    z[17]=z[1]*z[17];
    z[18]=z[9]*z[4];
    z[27]=npow(z[14],3);
    z[37]=4*z[27];
    z[45]=z[18]*z[37];
    z[46]=7*z[4];
    z[55]=npow(z[14],2);
    z[56]= - z[55]*z[46];
    z[56]=z[56] - z[45];
    z[56]=z[9]*z[56];
    z[57]=npow(z[11],2);
    z[58]=z[57]*z[36];
    z[59]=z[57]*z[10];
    z[60]=z[9]*z[59];
    z[58]= - 9*z[58] - 7*z[60];
    z[60]=2*z[9];
    z[61]=npow(z[7],3);
    z[58]=z[58]*z[61]*z[60];
    z[62]=z[52] - z[49];
    z[63]=z[62] - z[2];
    z[63]=z[63]*z[9];
    z[64]=z[31] - 2;
    z[65]= - z[63] + z[53] + z[64];
    z[65]=z[5]*z[65];
    z[66]=3*z[8];
    z[41]=z[65] - z[66] + z[41];
    z[41]=z[5]*z[41];
    z[65]=3*z[14];
    z[67]= - z[4]*z[65];
    z[67]=z[67] - n<T>(2,3)*z[39];
    z[68]= - z[64]*z[25];
    z[41]=z[41] + z[68] + z[58] + 7*z[67] + z[56];
    z[41]=z[5]*z[41];
    z[56]=4*z[61];
    z[58]=z[5]*z[10];
    z[67]= - z[58]*z[56];
    z[68]=z[10] + z[12];
    z[69]=3*z[61];
    z[70]=z[68]*z[69];
    z[70]=z[70] - z[42];
    z[70]=z[3]*z[70];
    z[67]=z[67] + z[70];
    z[70]=6*z[3];
    z[67]=z[67]*z[70];
    z[71]=z[30] - z[26];
    z[71]=z[71]*z[5];
    z[72]=z[24]*z[69];
    z[72]= - z[43] + z[72];
    z[72]=z[8]*z[72];
    z[67]=z[67] + z[72] + z[71];
    z[67]=z[3]*z[67];
    z[28]= - z[61]*z[28];
    z[72]=z[29]*z[56];
    z[28]=z[28] + z[72];
    z[28]=z[5]*z[28];
    z[72]= - z[25] - 10*z[42];
    z[72]=z[61]*z[72];
    z[73]=2*z[61];
    z[74]=z[5]*z[73];
    z[69]= - z[3]*z[69];
    z[69]=z[74] + z[69];
    z[69]=z[69]*z[70];
    z[69]=5*z[72] + z[69];
    z[69]=z[3]*z[69];
    z[72]=2*z[26];
    z[74]=z[61]*z[72];
    z[28]=z[69] + z[74] + z[28];
    z[28]=z[6]*z[28];
    z[69]=5*z[2];
    z[74]=npow(z[13],2);
    z[75]= - z[74]*z[69];
    z[76]=8*z[21];
    z[75]= - z[76] + z[75];
    z[75]=z[9]*z[75];
    z[77]=2*z[10];
    z[78]=z[77] - z[9];
    z[16]= - z[78]*z[16];
    z[79]=z[21]*z[12];
    z[16]=z[16] + 4*z[79] + z[75];
    z[16]=z[61]*z[16];
    z[75]=z[9]*z[2];
    z[80]=3*z[75];
    z[81]= - z[61]*z[80];
    z[82]=z[60] - z[12];
    z[83]=z[82]*z[61];
    z[84]=z[8]*z[83];
    z[81]=z[81] + z[84];
    z[81]=z[81]*z[25];
    z[16]=z[28] + z[67] + z[81] + z[16];
    z[16]=z[6]*z[16];
    z[28]=3*z[12];
    z[67]=z[28] + z[10];
    z[81]=6*z[10];
    z[84]= - z[67]*z[61]*z[81];
    z[84]=z[84] + z[48];
    z[84]=z[3]*z[84];
    z[85]=z[38]*z[56];
    z[85]=3*z[39] + z[85];
    z[84]=3*z[85] + z[84];
    z[84]=z[3]*z[84];
    z[85]=2*z[8];
    z[86]=z[57]*z[85];
    z[87]= - z[66] - z[5];
    z[87]=z[5]*z[87];
    z[87]=2*z[25] + z[87];
    z[87]=z[5]*z[87];
    z[88]=npow(z[4],2);
    z[89]=z[88]*z[2];
    z[84]=z[84] + z[87] - z[89] + z[86];
    z[84]=z[3]*z[84];
    z[86]=npow(z[12],2);
    z[87]=z[86]*z[76];
    z[90]=3*z[74];
    z[91]=z[90] + n<T>(5,3)*z[88];
    z[92]=z[2]*z[91];
    z[76]=z[76] + z[92];
    z[76]=z[9]*z[76];
    z[76]= - 16*z[79] + z[76];
    z[76]=z[9]*z[76];
    z[76]=z[87] + z[76];
    z[76]=z[76]*z[61];
    z[35]=z[35]*z[73];
    z[73]= - z[31]*z[83];
    z[35]=z[35] + z[73];
    z[35]=z[8]*z[35];
    z[35]= - 16*z[40] + z[35];
    z[35]=z[8]*z[35];
    z[73]= - z[4] + z[85];
    z[73]=z[73]*z[29];
    z[73]= - z[72] + z[73];
    z[73]=z[5]*z[73];
    z[87]= - z[4]*z[32];
    z[16]=z[16] + z[84] + z[73] + z[35] + z[87] + z[76];
    z[16]=z[6]*z[16];
    z[35]=z[46]*z[74];
    z[73]= - z[18]*z[32];
    z[43]=z[73] - z[43] - z[35];
    z[43]=z[9]*z[43];
    z[73]=z[34]*z[39];
    z[76]=z[34]*z[83];
    z[76]= - z[73] + z[76];
    z[76]=z[8]*z[76];
    z[33]=z[33]*z[61];
    z[83]= - z[2]*z[33];
    z[76]=z[83] + z[76];
    z[76]=z[8]*z[76];
    z[33]=42*z[33] - 1;
    z[33]=z[39]*z[33];
    z[83]=z[4]*z[13];
    z[84]=3*z[83];
    z[87]=z[57] - z[84];
    z[33]=z[76] + z[43] + 2*z[87] + z[33];
    z[33]=z[8]*z[33];
    z[43]=6*z[12];
    z[61]=z[36]*z[61]*z[43];
    z[76]=z[36]*z[2];
    z[87]=z[76] - z[10];
    z[48]= - z[87]*z[48];
    z[48]=z[61] + z[48];
    z[48]=z[3]*z[48];
    z[61]=n<T>(38,3) - 9*z[53];
    z[61]=z[5]*z[61];
    z[48]=z[48] + 9*z[2] + z[61];
    z[48]=z[3]*z[48];
    z[61]=z[37]*z[10];
    z[92]=3*z[55];
    z[61]=z[61] + z[92];
    z[93]=z[10]*z[61];
    z[93]=n<T>(59,3)*z[4] + 7*z[14] + z[93];
    z[93]=z[2]*z[93];
    z[94]=z[5]*z[87];
    z[54]=2*z[54] + z[94];
    z[54]=z[5]*z[54];
    z[94]= - 11*z[4] - 25*z[2];
    z[54]=z[54] + n<T>(1,3)*z[94] + 11*z[8];
    z[54]=z[5]*z[54];
    z[94]=z[8]*z[11];
    z[48]=z[48] + z[54] + z[93] - 21*z[94];
    z[48]=z[3]*z[48];
    z[54]=8*z[10];
    z[93]=z[54]*z[27];
    z[94]= - z[92] - z[93];
    z[94]=z[2]*z[94];
    z[95]=z[4]*z[21];
    z[96]=z[2]*z[27];
    z[95]=z[95] + z[96];
    z[95]=z[9]*z[95];
    z[96]=z[21]*z[86];
    z[97]=z[9]*z[79];
    z[96]= - 2*z[96] + z[97];
    z[96]=z[9]*z[96];
    z[97]=z[21]*npow(z[12],3);
    z[96]=z[97] + z[96];
    z[56]=z[96]*z[56];
    z[16]=z[17] + z[16] + z[48] + z[41] + z[33] + z[56] + 4*z[95] + 
    z[94] - z[32] + z[35];
    z[16]=z[1]*z[16];
    z[17]=18*z[11] - z[8];
    z[17]=z[17]*z[85];
    z[32]=npow(z[7],2);
    z[33]=16*z[32];
    z[29]= - z[29] - z[33];
    z[29]=z[10]*z[29];
    z[29]= - 30*z[8] - n<T>(11,3)*z[4] + z[29];
    z[29]=z[5]*z[29];
    z[41]=3*z[32];
    z[48]= - z[10]*z[41];
    z[48]=z[48] - n<T>(5,3)*z[5];
    z[56]= - static_cast<T>(1)+ z[58];
    z[56]=z[3]*z[56];
    z[48]=8*z[48] + n<T>(11,3)*z[56];
    z[48]=z[3]*z[48];
    z[17]=z[48] + z[29] + z[17] - 6*z[57] + n<T>(11,3)*z[39];
    z[17]=z[3]*z[17];
    z[29]=6*z[8] + 5*z[5];
    z[29]=z[5]*z[29];
    z[29]=z[29] + 86*z[32] - 9*z[25];
    z[29]=z[5]*z[29];
    z[48]=3*z[57] - z[32];
    z[48]=6*z[48] + z[25];
    z[48]=z[8]*z[48];
    z[56]=z[66] - n<T>(38,3)*z[5];
    z[56]=z[3]*z[56];
    z[56]=6*z[32] + z[56];
    z[94]=2*z[3];
    z[56]=z[56]*z[94];
    z[29]=z[56] + z[48] + z[29];
    z[29]=z[3]*z[29];
    z[20]=z[20] - z[71];
    z[48]=z[6]*z[3];
    z[56]=3*z[48];
    z[20]=z[20]*z[56];
    z[56]=3*z[2];
    z[95]=z[13]*z[56];
    z[95]=z[74] + z[95];
    z[96]=z[8]*z[2];
    z[95]=5*z[95] - 7*z[96];
    z[95]=z[32]*z[95];
    z[96]=12*z[32];
    z[97]= - z[96] + z[25];
    z[97]=z[8]*z[97];
    z[30]=z[97] - z[30];
    z[97]=6*z[5];
    z[30]=z[30]*z[97];
    z[29]=z[20] + z[29] + z[30] + z[95];
    z[29]=z[6]*z[29];
    z[30]= - z[75]*z[96];
    z[95]= - z[82]*z[32];
    z[95]=z[95] + z[8];
    z[95]=z[8]*z[95];
    z[30]=z[95] + 13*z[57] + z[30];
    z[30]=z[30]*z[85];
    z[95]=z[32]*z[9];
    z[96]=z[31] - 3;
    z[98]=z[8]*z[96];
    z[98]= - 6*z[95] + z[98];
    z[98]=z[8]*z[98];
    z[64]= - z[5]*z[64];
    z[64]=z[85] + z[64];
    z[64]=z[5]*z[64];
    z[64]=z[98] + z[64];
    z[64]=z[64]*z[22];
    z[98]=z[4]*z[74];
    z[98]= - 4*z[40] + z[98];
    z[99]=11*z[74];
    z[100]=25*z[13];
    z[101]= - z[100] + n<T>(43,3)*z[4];
    z[101]=z[2]*z[101];
    z[101]= - z[99] + z[101];
    z[101]=z[9]*z[101];
    z[102]=z[74]*z[12];
    z[101]=8*z[102] + z[101];
    z[101]=z[101]*z[32];
    z[17]=z[29] + z[17] + z[64] + z[30] + z[101] + 6*z[98] - n<T>(1,3)*z[89]
   ;
    z[17]=z[6]*z[17];
    z[29]=z[9]*z[35];
    z[29]=z[29] + n<T>(19,3)*z[39] - z[83] + 8*z[57] - z[74];
    z[29]=z[9]*z[29];
    z[30]=z[82]*z[95];
    z[35]=z[56] + z[50];
    z[50]=z[52] - z[35];
    z[50]=z[9]*z[50];
    z[50]=static_cast<T>(2)+ z[50];
    z[50]=z[50]*z[31];
    z[30]=z[50] + z[30] + static_cast<T>(2)- z[80];
    z[30]=z[8]*z[30];
    z[50]= - 6*z[4] + 41*z[2];
    z[50]=z[9]*z[50];
    z[50]= - z[51] + z[50];
    z[50]=z[50]*z[95];
    z[56]=13*z[13];
    z[64]=12*z[4];
    z[29]=z[30] + z[50] + z[29] - z[44] + z[64] - 2*z[11] - z[56];
    z[29]=z[8]*z[29];
    z[30]=z[36]*z[41];
    z[30]=n<T>(5,3) + z[30];
    z[44]=9*z[10];
    z[50]=z[44] - n<T>(16,3)*z[76];
    z[50]=z[5]*z[50];
    z[76]=7*z[10];
    z[82]= - z[76] + 11*z[38];
    z[47]=z[82]*z[47];
    z[30]=z[47] + 4*z[30] + z[50];
    z[30]=z[3]*z[30];
    z[33]= - z[36]*z[33];
    z[47]=3*z[87] - z[38];
    z[47]=z[5]*z[47];
    z[50]= - static_cast<T>(35)+ 8*z[53];
    z[33]=z[47] + n<T>(1,3)*z[50] + z[33];
    z[33]=z[5]*z[33];
    z[47]= - z[55] - z[93];
    z[47]=z[10]*z[47];
    z[50]=5*z[10];
    z[82]=z[55]*z[50];
    z[89]=17*z[14];
    z[82]=z[89] + z[82];
    z[82]=z[10]*z[82];
    z[82]= - n<T>(11,3) + z[82];
    z[82]=z[2]*z[82];
    z[93]=42*z[11];
    z[30]=z[30] + z[33] + z[66] + z[82] + n<T>(14,3)*z[4] + z[47] - z[93] + 
   11*z[14];
    z[30]=z[3]*z[30];
    z[33]=z[9]*z[55];
    z[33]=z[33] + z[14];
    z[33]=z[46]*z[33];
    z[33]=n<T>(29,3)*z[39] + z[55] + z[33];
    z[33]=z[9]*z[33];
    z[47]=3*z[9];
    z[54]=z[54] + z[47];
    z[47]=z[47]*z[54];
    z[47]=17*z[36] + z[47];
    z[47]=z[41]*z[11]*z[47];
    z[54]=23*z[11];
    z[82]=12*z[8];
    z[95]= - z[10] + z[9];
    z[95]=z[5]*z[95];
    z[95]= - static_cast<T>(3)+ z[95];
    z[95]=z[5]*z[95];
    z[33]=z[95] - z[82] + z[47] + z[33] + n<T>(14,3)*z[2] - n<T>(19,3)*z[4] + 
    z[54] - 20*z[14];
    z[33]=z[5]*z[33];
    z[47]= - 6*z[40] + z[27];
    z[95]=z[55] - z[74];
    z[46]=z[95]*z[46];
    z[95]=z[2]*z[55];
    z[45]=z[45] - 6*z[95] + 4*z[47] + z[46];
    z[45]=z[9]*z[45];
    z[46]= - z[102] - z[59];
    z[47]=15*z[13] - n<T>(122,3)*z[4];
    z[47]=z[2]*z[47];
    z[47]=z[47] + z[91];
    z[47]=z[9]*z[47];
    z[46]=6*z[46] + z[47];
    z[46]=z[9]*z[46];
    z[47]=z[86]*z[90];
    z[46]=z[47] + z[46];
    z[46]=z[46]*z[32];
    z[47]=z[10]*z[55];
    z[47]= - n<T>(10,3)*z[4] + z[47] - 8*z[14] + z[56];
    z[47]=z[2]*z[47];
    z[56]=12*z[79];
    z[79]= - z[4] + z[89] - 6*z[13];
    z[79]=z[4]*z[79];
    z[16]=z[16] + z[17] + z[30] + z[33] + z[29] + z[46] + z[45] + z[47]
    + z[79] + z[56] + 17*z[74] + 14*z[57] + z[61];
    z[16]=z[1]*z[16];
    z[17]= - static_cast<T>(1)- z[24];
    z[17]=z[17]*z[85];
    z[29]=z[50] - z[38];
    z[29]=z[5]*z[29];
    z[29]= - static_cast<T>(35)+ z[29];
    z[29]=z[5]*z[29];
    z[30]= - z[43] - n<T>(11,3)*z[10];
    z[30]=2*z[30] + n<T>(11,3)*z[38];
    z[30]=z[3]*z[30];
    z[23]= - n<T>(28,3) + z[23];
    z[23]=2*z[23] + z[30];
    z[23]=z[3]*z[23];
    z[30]=n<T>(11,3)*z[2];
    z[17]=z[23] + z[29] + z[17] + 84*z[11] + z[30];
    z[17]=z[3]*z[17];
    z[23]= - z[66] - 7*z[5];
    z[23]=z[5]*z[23];
    z[23]=12*z[25] + z[23];
    z[23]=z[5]*z[23];
    z[29]= - z[82] + n<T>(119,3)*z[5];
    z[19]=z[29]*z[19];
    z[19]=z[19] - z[72] + z[23];
    z[19]=z[3]*z[19];
    z[19]= - z[20] + 6*z[71] + z[19];
    z[19]=z[6]*z[19];
    z[20]= - n<T>(76,3)*z[58] + n<T>(65,3) + 6*z[24];
    z[20]=z[3]*z[20];
    z[20]= - n<T>(34,3)*z[5] + z[20];
    z[20]=z[3]*z[20];
    z[23]=static_cast<T>(7)+ z[24];
    z[23]=z[8]*z[23];
    z[23]= - 51*z[11] + z[23];
    z[23]=z[8]*z[23];
    z[29]=static_cast<T>(2)+ 5*z[58];
    z[29]=z[5]*z[29];
    z[29]=45*z[8] + z[29];
    z[29]=z[5]*z[29];
    z[20]=z[20] + z[23] + z[29];
    z[20]=z[3]*z[20];
    z[23]=static_cast<T>(4)- z[31];
    z[23]=z[23]*z[25];
    z[25]=z[5]*z[96];
    z[25]= - z[8] + z[25];
    z[25]=z[5]*z[25];
    z[23]=z[23] + z[25];
    z[23]=z[23]*z[22];
    z[19]=z[19] + z[20] - 4*z[26] + z[23];
    z[19]=z[6]*z[19];
    z[20]=5*z[13];
    z[23]=4*z[2] - z[54] + z[20];
    z[25]= - static_cast<T>(1)+ z[80];
    z[26]=z[80] - 4;
    z[26]=z[26]*z[9];
    z[26]=z[26] + z[12];
    z[29]=z[8]*z[26];
    z[25]=2*z[25] + z[29];
    z[25]=z[8]*z[25];
    z[23]=2*z[23] + z[25];
    z[23]=z[8]*z[23];
    z[25]=z[56] + 13*z[74];
    z[29]=n<T>(2,3)*z[4];
    z[20]= - z[20] + z[29];
    z[20]=z[20]*z[69];
    z[33]=z[99] + n<T>(7,3)*z[88];
    z[33]=z[2]*z[33];
    z[33]=12*z[21] + z[33];
    z[33]=z[9]*z[33];
    z[45]=z[15] - z[60];
    z[45]=z[5]*z[45];
    z[45]=static_cast<T>(3)+ z[45];
    z[45]=z[5]*z[45];
    z[46]= - z[4] + 8*z[8];
    z[45]=6*z[46] + z[45];
    z[45]=z[5]*z[45];
    z[17]=z[19] + z[17] + z[45] + z[23] + z[33] + z[20] - z[84] + 12*
    z[57] - z[25];
    z[17]=z[6]*z[17];
    z[19]=z[55]*z[43];
    z[20]=17*z[13];
    z[19]=z[29] + z[20] + z[19];
    z[19]=z[4]*z[19];
    z[21]=z[27] + 3*z[21];
    z[23]=4*z[12];
    z[21]=z[21]*z[23];
    z[20]= - n<T>(103,3)*z[4] + z[65] + z[20];
    z[20]=z[2]*z[20];
    z[27]=z[37]*z[12];
    z[29]= - z[92] + z[27];
    z[18]=z[29]*z[18];
    z[29]=z[10]*z[40];
    z[18]=z[18] + z[20] + z[19] - 24*z[29] + z[21] + 9*z[74] + 16*z[57]
    - z[92];
    z[18]=z[9]*z[18];
    z[19]= - 5*z[55] + z[27];
    z[19]=z[10]*z[19];
    z[20]=39*z[11];
    z[21]=2*z[55];
    z[29]= - z[12]*z[21];
    z[19]=z[19] + z[29] - z[20] + 10*z[14];
    z[19]=z[10]*z[19];
    z[29]=11*z[10];
    z[33]=z[14]*z[29];
    z[33]= - n<T>(16,3) + z[33];
    z[33]=z[33]*z[53];
    z[37]=11*z[87] - 4*z[38];
    z[37]=z[5]*z[37];
    z[40]=z[76] + 2*z[38];
    z[43]=z[43] - n<T>(7,3)*z[10];
    z[43]=z[3]*z[10]*z[43];
    z[40]=n<T>(8,3)*z[40] + z[43];
    z[40]=z[3]*z[40];
    z[43]=z[12]*z[14];
    z[19]=z[40] + z[37] + 3*z[24] + z[33] + z[19] - n<T>(13,3) + z[43];
    z[19]=z[3]*z[19];
    z[33]= - z[14]*z[52];
    z[33]=z[33] + 5*z[39];
    z[33]=z[9]*z[33];
    z[33]=z[33] + z[69] - z[64] - 10*z[59] + 48*z[11] + z[14];
    z[33]=z[9]*z[33];
    z[37]=28*z[11] - z[65];
    z[37]=z[10]*z[37];
    z[37]= - z[53] - static_cast<T>(35)+ z[37];
    z[33]=6*z[31] + 2*z[37] + z[33];
    z[33]=z[5]*z[33];
    z[37]= - 29*z[83] + n<T>(98,3)*z[39];
    z[37]=z[9]*z[37];
    z[39]= - z[11] - z[13];
    z[37]=z[37] + 22*z[2] + 18*z[39] + n<T>(17,3)*z[4];
    z[37]=z[9]*z[37];
    z[35]=4*z[4] - z[35];
    z[35]=z[9]*z[35];
    z[35]=z[35] + static_cast<T>(4)- z[51];
    z[35]=z[9]*z[35];
    z[35]= - z[12] + z[35];
    z[35]=z[35]*z[31];
    z[26]=z[35] - z[26];
    z[26]=z[8]*z[26];
    z[26]=z[26] + z[37] + n<T>(65,3) - 6*z[51];
    z[26]=z[8]*z[26];
    z[21]=z[21] - z[25];
    z[21]=z[12]*z[21];
    z[25]=4*z[57] - z[55];
    z[25]=3*z[25] + z[27];
    z[25]=z[10]*z[25];
    z[27]=24*z[11] + z[14];
    z[35]=z[14]*z[28];
    z[35]= - static_cast<T>(2)+ z[35];
    z[35]=z[4]*z[35];
    z[37]= - z[14]*z[50];
    z[37]=n<T>(19,3) + z[37];
    z[37]=z[2]*z[37];
    z[16]=z[16] + z[17] + z[19] + z[33] + z[26] + z[18] + z[37] + z[35]
    + z[25] + z[21] + 3*z[27] - z[100];
    z[16]=z[1]*z[16];
    z[17]=z[8]*z[7];
    z[18]=z[7]*z[12];
    z[18]=static_cast<T>(4)+ z[18];
    z[18]=z[18]*z[17];
    z[19]=z[7]*z[10];
    z[21]= - static_cast<T>(4)- z[19];
    z[21]=z[7]*z[21]*z[22];
    z[18]=z[21] + z[41] + z[18];
    z[18]=z[3]*z[18];
    z[21]= - z[42]*z[48];
    z[25]= - z[5]*z[85];
    z[21]=z[21] + z[25];
    z[21]=z[32]*z[21];
    z[25]=z[41] + 5*z[17];
    z[25]=z[5]*z[25];
    z[26]= - z[8]*z[32];
    z[25]=z[26] + z[25];
    z[25]=z[3]*z[25];
    z[21]=z[25] + z[21];
    z[21]=z[6]*z[21];
    z[25]= - z[32]*z[85];
    z[26]=z[7]*z[9];
    z[27]=static_cast<T>(8)- z[26];
    z[27]=z[27]*z[17];
    z[27]=4*z[32] + z[27];
    z[27]=z[5]*z[27];
    z[18]=z[21] + z[18] + z[25] + z[27];
    z[18]=z[6]*z[18];
    z[21]=z[36]*z[7];
    z[25]=z[44] + z[21];
    z[25]=z[5]*z[25];
    z[24]=z[25] - z[24];
    z[24]=z[7]*z[24];
    z[25]=3*z[7];
    z[27]= - z[7]*z[68];
    z[27]= - static_cast<T>(3)+ z[27];
    z[27]=z[27]*z[25];
    z[24]=z[27] + z[24];
    z[24]=z[24]*z[70];
    z[27]=z[75] - 2;
    z[27]=z[27]*z[9];
    z[27]=z[27] + z[12];
    z[33]=11*z[7];
    z[35]=z[27]*z[33];
    z[36]=static_cast<T>(11)+ 7*z[75];
    z[35]=4*z[36] + z[35];
    z[17]=z[35]*z[17];
    z[31]=z[31]*z[25];
    z[35]=z[78]*z[7];
    z[36]= - static_cast<T>(12)- z[35];
    z[36]=z[7]*z[36];
    z[31]=z[36] + z[31];
    z[31]=z[31]*z[97];
    z[17]=6*z[18] + z[24] + z[31] + 24*z[32] + n<T>(1,3)*z[17];
    z[17]=z[6]*z[17];
    z[18]=static_cast<T>(2)+ z[73];
    z[18]=z[9]*z[18];
    z[24]= - n<T>(11,3)*z[12] - z[81];
    z[18]=2*z[24] + n<T>(11,3)*z[18];
    z[18]=z[7]*z[18];
    z[18]=z[18] - n<T>(22,3) + 13*z[73];
    z[18]=z[7]*z[18];
    z[24]=z[27]*z[26];
    z[26]=n<T>(8,3)*z[12];
    z[27]=static_cast<T>(23)- n<T>(61,3)*z[75];
    z[27]=z[9]*z[27];
    z[24]= - n<T>(11,3)*z[24] - z[26] + z[27];
    z[24]=z[7]*z[24];
    z[24]= - n<T>(52,3) + z[24];
    z[24]=z[8]*z[24];
    z[27]=static_cast<T>(37)+ 4*z[35];
    z[22]=z[27]*z[22];
    z[19]=z[67]*z[19];
    z[15]=z[12] + z[15];
    z[15]=2*z[15] + z[19];
    z[15]=z[15]*z[25];
    z[19]=z[7]*z[38];
    z[15]= - 6*z[19] + static_cast<T>(7)+ z[15];
    z[15]=z[15]*z[94];
    z[15]=z[17] + z[15] + z[22] + z[24] + z[18] - n<T>(28,3)*z[2] - z[20] + 
   20*z[13];
    z[15]=z[6]*z[15];
    z[17]=z[23] - z[29];
    z[18]=z[62]*z[34];
    z[18]=z[12] + z[18];
    z[18]=z[9]*z[18];
    z[19]=z[10]*z[12];
    z[18]=z[19] + z[18];
    z[18]=z[18]*z[33];
    z[19]=47*z[4] - 61*z[49];
    z[19]=z[9]*z[19];
    z[19]= - static_cast<T>(22)+ z[19];
    z[19]=z[9]*z[19];
    z[17]=z[18] + 2*z[17] + z[19];
    z[17]=z[7]*z[17];
    z[18]=z[49] + z[2];
    z[18]= - 23*z[4] + n<T>(61,3)*z[18];
    z[18]=z[9]*z[18];
    z[18]=z[18] - static_cast<T>(23)+ n<T>(8,3)*z[51];
    z[18]=z[9]*z[18];
    z[18]=z[26] + z[18];
    z[18]=z[9]*z[18];
    z[19]= - z[63] - static_cast<T>(2)+ z[51];
    z[19]=z[9]*z[19];
    z[19]=z[12] + z[19];
    z[19]=z[7]*z[19]*z[34];
    z[18]=z[18] + n<T>(11,3)*z[19];
    z[18]=z[7]*z[18];
    z[19]= - z[12] - z[60];
    z[18]=2*z[19] + z[18];
    z[18]=z[8]*z[18];
    z[19]=z[13]*z[28];
    z[20]=z[10]*z[93];
    z[19]=z[20] + n<T>(1,3) + z[19];
    z[20]= - z[10]*z[68];
    z[21]= - z[12]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[20]*z[25];
    z[20]=z[20] - z[12] - z[77];
    z[20]=z[20]*z[94];
    z[21]=z[30] + n<T>(22,3)*z[4] + 9*z[11] - 14*z[13];
    z[21]=z[9]*z[21];
    z[22]= - 94*z[10] - 9*z[9];
    z[22]=z[5]*z[22];
    z[15]=z[16] + z[15] + z[20] + z[22] + z[18] + n<T>(1,3)*z[17] + z[21] + 
   2*z[19] + n<T>(11,3)*z[53];

    r += 4*z[15];
 
    return r;
}

template double qg_2lNLC_r955(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r955(const std::array<dd_real,31>&);
#endif
