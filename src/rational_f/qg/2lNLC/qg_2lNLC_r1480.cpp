#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1480(const std::array<T,31>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[5];
    z[4]=k[6];
    z[5]=k[8];
    z[6]=k[13];
    z[7]=k[2];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=k[3];
    z[11]=k[12];
    z[12]=k[7];
    z[13]=k[15];
    z[14]=z[9]*z[13];
    z[15]= - z[2]*z[14];
    z[16]=z[9]*z[2];
    z[16]= - static_cast<T>(3)+ z[16];
    z[16]=z[5]*z[16];
    z[15]=z[16] - 4*z[13] + z[15];
    z[15]=z[11]*z[15];
    z[16]=z[10] + z[2];
    z[17]=npow(z[4],2);
    z[18]=z[17]*z[16];
    z[19]=z[10]*z[18];
    z[19]=static_cast<T>(2)+ z[19];
    z[20]=z[6]*z[10];
    z[19]=2*z[19] - 3*z[20];
    z[19]=z[6]*z[19];
    z[19]=z[12] + z[19];
    z[21]=2*z[6];
    z[19]=z[19]*z[21];
    z[22]= - 2*z[12] + z[9];
    z[22]=z[5]*z[22];
    z[15]=z[19] + z[15] + 3*z[14] + z[22];
    z[19]=z[11]*z[12];
    z[17]=z[17] - 3*z[19];
    z[22]=npow(z[6],2);
    z[17]=z[17]*z[22];
    z[23]=z[11]*z[13];
    z[24]=2*z[8];
    z[25]= - z[11]*z[24];
    z[25]= - z[23] + z[25];
    z[26]=npow(z[8],2);
    z[27]=2*z[26];
    z[25]=z[25]*z[27];
    z[17]=z[17] + z[25];
    z[25]=n<T>(1,5)*z[3];
    z[17]=z[17]*z[25];
    z[18]=n<T>(3,5)*z[11] - n<T>(2,5)*z[12] - z[18];
    z[18]=z[6]*z[18];
    z[18]=n<T>(3,5)*z[19] + z[18];
    z[18]=z[6]*z[18];
    z[28]=n<T>(2,5)*z[8];
    z[29]=3*z[13] + 4*z[5];
    z[29]= - z[28] + n<T>(1,5)*z[29] - z[11];
    z[29]=z[29]*z[24];
    z[29]= - z[23] + z[29];
    z[29]=z[8]*z[29];
    z[30]=z[19]*z[5];
    z[17]=z[17] + z[29] - n<T>(3,5)*z[30] + z[18];
    z[17]=z[3]*z[17];
    z[18]=npow(z[4],3);
    z[29]=z[18]*z[10];
    z[31]=z[18]*z[2];
    z[29]=z[29] + 2*z[31];
    z[32]=z[29]*z[10];
    z[33]=npow(z[2],2);
    z[18]=z[33]*z[18];
    z[18]=z[32] + z[18];
    z[32]= - z[10]*z[18];
    z[34]=z[11]*z[2];
    z[32]=z[34] + static_cast<T>(1)+ z[32];
    z[32]=z[6]*z[32];
    z[32]=z[11] + z[32];
    z[32]=z[6]*z[32];
    z[32]=z[19] + z[32];
    z[32]=z[6]*z[32];
    z[34]= - z[12] - z[9];
    z[34]=z[5]*z[34];
    z[14]=z[14] + z[34];
    z[14]=z[11]*z[14];
    z[34]=z[9]*npow(z[7],2);
    z[35]=2*z[7] - z[34];
    z[35]=z[35]*z[24];
    z[36]=z[9]*z[7];
    z[35]=z[35] + static_cast<T>(1)- z[36];
    z[35]=z[8]*z[35];
    z[35]=z[35] - z[9];
    z[35]=z[5]*z[35];
    z[37]=4*z[23];
    z[35]=z[37] + z[35];
    z[35]=z[8]*z[35];
    z[33]=z[1]*z[5]*npow(z[4],4)*z[33]*npow(z[6],3)*npow(z[3],2);
    z[14]= - z[33] + z[35] + z[14] + z[32];
    z[18]=2*z[18] - z[11];
    z[18]=z[6]*z[18];
    z[18]= - z[19] + z[18];
    z[18]=z[18]*z[22];
    z[32]= - z[5]*z[28];
    z[23]=z[23] + z[32];
    z[23]=z[23]*z[26];
    z[18]=n<T>(1,5)*z[18] + z[23];
    z[23]=z[19] - z[29];
    z[23]=z[23]*z[21];
    z[26]=z[5]*z[31];
    z[23]=z[26] + z[23];
    z[22]=z[23]*z[22];
    z[23]=npow(z[8],3)*z[37];
    z[22]=z[22] + z[23];
    z[22]=z[22]*z[25];
    z[18]=2*z[18] + z[22];
    z[18]=z[3]*z[18];
    z[14]=z[18] + n<T>(2,5)*z[14];
    z[14]=z[1]*z[14];
    z[18]= - z[7] + 2*z[34];
    z[18]=z[18]*z[28];
    z[22]= - static_cast<T>(3)+ z[36];
    z[23]= - z[7] + n<T>(1,5)*z[34];
    z[23]=z[5]*z[23];
    z[18]=z[18] + n<T>(1,5)*z[22] + z[23];
    z[18]=z[18]*z[24];
    z[22]= - static_cast<T>(4)+ z[36];
    z[22]=z[5]*z[22];
    z[22]= - 8*z[11] + z[22] + 9*z[13] + 2*z[9];
    z[18]=n<T>(1,5)*z[22] + z[18];
    z[18]=z[8]*z[18];
    z[14]=z[14] + z[17] + n<T>(1,5)*z[15] + z[18];
    z[14]=z[1]*z[14];
    z[15]=z[24]*z[10];
    z[15]=z[15] + 1;
    z[17]= - z[27]*z[11]*z[15];
    z[18]=z[6]*z[19];
    z[17]=z[17] - z[30] + z[18];
    z[17]=z[3]*z[17];
    z[18]=z[11]*z[10];
    z[18]= - 4*z[18] - z[15];
    z[18]=z[8]*z[18];
    z[19]= - z[11] - z[13] - z[5];
    z[18]=2*z[19] + z[18];
    z[18]=z[18]*z[24];
    z[19]=z[11]*z[5];
    z[22]= - z[5]*z[12];
    z[23]= - z[6]*z[16];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[6]*z[23];
    z[23]=z[12] + 6*z[23];
    z[23]=z[6]*z[23];
    z[17]=z[17] + z[18] + z[23] + z[22] - z[19];
    z[17]=z[17]*z[25];
    z[18]=z[34] + z[7];
    z[15]= - z[18]*z[15];
    z[18]=2*z[10];
    z[15]= - z[18] + z[15];
    z[15]=z[8]*z[15];
    z[22]=z[7]*z[13];
    z[23]=z[5]*z[7];
    z[24]= - z[11]*z[18];
    z[15]=z[15] + z[24] + z[23] - static_cast<T>(1)+ z[22];
    z[15]=z[15]*z[28];
    z[23]=2*z[2];
    z[23]= - z[13]*z[23];
    z[22]=z[23] - static_cast<T>(2)+ z[22];
    z[22]=z[9]*z[22];
    z[19]=z[2]*z[19];
    z[19]=z[19] + z[5] - 6*z[13] + z[22];
    z[16]=z[16]*z[20];
    z[16]=n<T>(3,5)*z[16] - n<T>(2,5)*z[2] - z[10];
    z[16]=z[6]*z[16];
    z[16]=n<T>(2,5) + z[16];
    z[16]=z[16]*z[21];
    z[14]=z[14] + z[17] + z[15] + n<T>(1,5)*z[19] + z[16];
    z[14]=z[1]*z[14];
    z[15]= - z[18] + z[3];
    z[15]=z[6]*z[15];
    z[14]=n<T>(1,5)*z[15] + z[14];

    r += 72*z[14];
 
    return r;
}

template double qg_2lNLC_r1480(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1480(const std::array<dd_real,31>&);
#endif
