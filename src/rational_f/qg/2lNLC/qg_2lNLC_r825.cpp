#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r825(const std::array<T,31>& k) {
  T z[47];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[12];
    z[6]=k[15];
    z[7]=k[2];
    z[8]=k[9];
    z[9]=k[4];
    z[10]=k[7];
    z[11]=k[14];
    z[12]=k[5];
    z[13]=k[13];
    z[14]=k[3];
    z[15]=n<T>(71,2)*z[4];
    z[16]=z[5]*z[12];
    z[17]=static_cast<T>(3)+ n<T>(1,2)*z[16];
    z[17]=z[5]*z[17];
    z[17]=z[15] + 11*z[17];
    z[17]=z[6]*z[17];
    z[18]=npow(z[5],2);
    z[15]=z[13]*z[15];
    z[15]=z[17] + z[15] - 11*z[18];
    z[15]=z[5]*z[15];
    z[17]=npow(z[8],2);
    z[19]=z[8]*z[7];
    z[20]=n<T>(27,2) + z[19];
    z[20]=z[20]*z[17];
    z[21]=n<T>(71,2)*z[13];
    z[21]=z[8]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[4]*z[20];
    z[21]=n<T>(113,2)*z[10];
    z[22]= - z[10]*z[16];
    z[22]= - z[5] + z[22];
    z[22]=z[22]*z[21];
    z[23]=z[5]*z[13];
    z[22]=167*z[23] + z[22];
    z[22]=z[10]*z[22];
    z[23]=npow(z[8],3);
    z[15]=z[22] - z[23] + z[20] + z[15];
    z[20]=z[16] + 1;
    z[22]=z[4]*z[7];
    z[24]=z[22] + z[20];
    z[25]=n<T>(1,2)*z[6];
    z[24]=z[24]*z[25];
    z[24]= - z[5] + z[24];
    z[24]=z[3]*z[24];
    z[26]=n<T>(1,2)*z[4];
    z[27]=z[26] + z[5];
    z[27]=z[27]*z[6];
    z[24]=z[27] + n<T>(1,2)*z[24];
    z[24]=z[3]*z[24];
    z[15]=n<T>(1,2)*z[15] + 3*z[24];
    z[15]=z[3]*z[15];
    z[24]=n<T>(1,2)*z[5];
    z[28]=z[6]*z[4];
    z[29]=z[28]*z[24];
    z[30]= - z[4]*z[24];
    z[27]=z[30] + z[27];
    z[27]=z[3]*z[27];
    z[27]=z[29] + z[27];
    z[27]=z[3]*z[27];
    z[29]=npow(z[5],3);
    z[30]=z[5]*npow(z[3],2);
    z[30]=11*z[29] + 3*z[30];
    z[31]=n<T>(1,4)*z[1];
    z[30]=z[31]*z[28]*z[30];
    z[32]=z[24] + z[4];
    z[33]=z[6]*z[32]*z[18];
    z[34]=z[4]*z[29];
    z[33]= - n<T>(1,4)*z[34] + z[33];
    z[27]=z[30] + 11*z[33] + n<T>(3,2)*z[27];
    z[27]=z[3]*z[27];
    z[29]=z[29]*z[28];
    z[27]= - n<T>(11,4)*z[29] + z[27];
    z[27]=z[27]*z[31];
    z[29]=z[23]*z[12];
    z[30]= - n<T>(17,2)*z[17] + z[29];
    z[31]=z[4]*z[8];
    z[32]=z[5]*z[32];
    z[30]= - n<T>(11,8)*z[32] + n<T>(1,8)*z[30] + z[31];
    z[30]=z[6]*z[30];
    z[32]=npow(z[13],3);
    z[33]=33*z[32];
    z[34]=z[13]*z[31];
    z[34]=z[33] + 71*z[34];
    z[30]=n<T>(1,16)*z[34] + z[30];
    z[30]=z[5]*z[30];
    z[34]=npow(z[11],2);
    z[35]=npow(z[11],3);
    z[36]=9*z[35];
    z[37]=z[9]*z[36];
    z[37]= - 5*z[34] + z[37];
    z[37]=z[4]*z[37];
    z[36]=z[36] + z[37];
    z[37]=npow(z[13],2);
    z[32]=n<T>(11,4)*z[32];
    z[38]= - z[12]*z[32];
    z[38]= - z[37] + z[38];
    z[38]=z[5]*z[38];
    z[39]=z[11]*z[28];
    z[36]= - n<T>(53,2)*z[39] + n<T>(1,4)*z[36] + z[38];
    z[38]=z[4]*z[9];
    z[39]= - static_cast<T>(1)- z[38];
    z[39]=z[10]*z[39]*z[25];
    z[39]=z[28] + z[39];
    z[40]=9*z[10];
    z[39]=z[39]*z[40];
    z[36]=n<T>(1,2)*z[36] + z[39];
    z[36]=z[10]*z[36];
    z[15]=z[27] + n<T>(1,4)*z[15] + n<T>(3,2)*z[36] + z[30];
    z[15]=z[1]*z[15];
    z[27]=9*z[13];
    z[30]=n<T>(1,2)*z[13];
    z[36]=z[8] - z[30];
    z[36]=z[36]*z[27];
    z[29]= - n<T>(5,8)*z[31] - n<T>(1,4)*z[29] - n<T>(5,4)*z[17] + z[36];
    z[29]=z[5]*z[29];
    z[36]=9*z[9];
    z[36]= - z[34]*z[36];
    z[36]= - 79*z[11] + z[36];
    z[36]=z[4]*z[36];
    z[36]=z[36] - 11*z[34] - n<T>(3,2)*z[37];
    z[39]=3*z[6];
    z[41]= - 119*z[11] + 215*z[4];
    z[41]=z[41]*z[39];
    z[42]=z[12]*z[37];
    z[42]=203*z[13] + n<T>(57,2)*z[42];
    z[42]=z[5]*z[42];
    z[36]=z[41] + 3*z[36] + z[42];
    z[41]=27*z[38];
    z[42]=n<T>(113,4)*z[16] + n<T>(113,4) + z[41];
    z[43]=27*z[6];
    z[44]=npow(z[9],2);
    z[45]=z[44]*z[4];
    z[46]=z[9] + z[45];
    z[46]=z[46]*z[43];
    z[42]=n<T>(1,2)*z[42] + z[46];
    z[42]=z[10]*z[42];
    z[46]= - n<T>(5,2) - 9*z[38];
    z[46]=z[6]*z[46];
    z[46]= - n<T>(3,2)*z[4] + z[46];
    z[42]=9*z[46] + z[42];
    z[42]=z[10]*z[42];
    z[36]=n<T>(1,4)*z[36] + z[42];
    z[36]=z[10]*z[36];
    z[23]=z[33] + 27*z[35] - z[23];
    z[35]= - static_cast<T>(1)- n<T>(1,4)*z[19];
    z[35]=z[35]*z[17];
    z[42]=3*z[34];
    z[35]=z[42] + z[35];
    z[35]=z[4]*z[35];
    z[23]=z[36] + z[29] + n<T>(1,4)*z[23] + z[35];
    z[29]=static_cast<T>(39)+ 5*z[19];
    z[29]=z[29]*z[31];
    z[30]=3*z[8] + z[30];
    z[30]=z[13]*z[30];
    z[18]=z[12]*z[18];
    z[18]=257*z[13] - n<T>(11,2)*z[18];
    z[18]=z[5]*z[18];
    z[35]=static_cast<T>(9)+ n<T>(11,4)*z[16];
    z[35]=z[5]*z[35];
    z[35]=n<T>(33,8)*z[4] + z[35];
    z[35]=z[6]*z[35];
    z[18]=z[35] + n<T>(1,4)*z[18] + n<T>(1,8)*z[29] + z[17] + 3*z[30];
    z[29]=z[5]*npow(z[12],2);
    z[30]=z[29] + z[12];
    z[35]= - z[30]*z[21];
    z[35]=z[35] - static_cast<T>(49)- n<T>(113,2)*z[16];
    z[36]=n<T>(1,4)*z[10];
    z[35]=z[35]*z[36];
    z[35]=z[35] + 38*z[13] - n<T>(221,4)*z[5];
    z[35]=z[10]*z[35];
    z[46]=3*z[16] + static_cast<T>(3)+ z[22];
    z[46]=z[6]*z[46];
    z[20]= - z[3]*z[20];
    z[20]=z[20] + z[4] + z[46];
    z[20]=z[3]*z[20];
    z[18]=n<T>(3,16)*z[20] + n<T>(1,2)*z[18] + z[35];
    z[18]=z[3]*z[18];
    z[20]=z[42] - z[17];
    z[35]=z[5] + z[4];
    z[17]=z[12]*z[17];
    z[17]=17*z[8] - n<T>(13,2)*z[17] - n<T>(11,2)*z[35];
    z[17]=z[5]*z[17];
    z[35]= - n<T>(213,8)*z[11] + z[8];
    z[35]=z[4]*z[35];
    z[17]=n<T>(1,8)*z[17] + n<T>(5,16)*z[20] + z[35];
    z[17]=z[6]*z[17];
    z[15]=z[15] + z[18] + z[17] + n<T>(1,2)*z[23];
    z[15]=z[1]*z[15];
    z[17]=z[12] - z[9];
    z[18]=z[32]*z[17];
    z[20]=z[13]*z[14];
    z[20]= - static_cast<T>(1)- n<T>(11,4)*z[20];
    z[20]=z[20]*z[37];
    z[23]=z[4]*z[11];
    z[18]= - 61*z[23] - n<T>(9,4)*z[34] + z[20] + z[18];
    z[20]=z[12]*z[13];
    z[23]=z[8]*z[20];
    z[23]=z[27] + n<T>(23,2)*z[23];
    z[23]=z[23]*z[24];
    z[18]=3*z[18] + z[23];
    z[23]=z[11]*z[7];
    z[24]=static_cast<T>(13)- 9*z[23];
    z[24]=z[12]*z[24]*z[34];
    z[27]= - static_cast<T>(169)+ z[23];
    z[27]=z[11]*z[27];
    z[24]=z[27] + z[24];
    z[27]=13*z[8];
    z[24]=z[27] + n<T>(3,2)*z[24];
    z[23]=static_cast<T>(217)- n<T>(107,2)*z[23];
    z[19]=n<T>(3,4)*z[23] + z[19];
    z[19]=z[4]*z[19];
    z[23]=n<T>(23,8)*z[16];
    z[32]=z[8]*z[23];
    z[19]=z[32] + n<T>(1,4)*z[24] + z[19];
    z[19]=z[6]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[19]=z[9]*z[11];
    z[19]= - n<T>(101,2) + 71*z[19];
    z[19]=z[19]*z[26];
    z[19]=z[19] - 13*z[11] + n<T>(17,4)*z[13];
    z[24]=static_cast<T>(1)+ z[20];
    z[24]=z[5]*z[24];
    z[19]=3*z[19] + n<T>(239,4)*z[24];
    z[24]=z[12]*z[11];
    z[24]= - n<T>(507,8)*z[38] + n<T>(33,8) - 17*z[24];
    z[24]=z[24]*z[39];
    z[19]=n<T>(1,2)*z[19] + z[24];
    z[24]=z[17] + z[29];
    z[26]=npow(z[9],3);
    z[32]= - z[4]*z[26];
    z[32]= - z[44] + z[32];
    z[32]=z[32]*z[43];
    z[24]=z[32] - 27*z[45] + n<T>(113,4)*z[24];
    z[24]=z[24]*z[36];
    z[32]=n<T>(113,8) + z[41];
    z[34]=n<T>(5,4)*z[9] + 3*z[45];
    z[34]=z[6]*z[34];
    z[24]=z[24] + n<T>(1,2)*z[32] + 9*z[34];
    z[24]=z[10]*z[24];
    z[19]=n<T>(1,2)*z[19] + z[24];
    z[19]=z[10]*z[19];
    z[24]=23*z[8] - 21*z[13];
    z[24]=z[13]*z[24];
    z[32]=z[9]*z[33];
    z[24]=z[24] + z[32];
    z[24]=z[12]*z[24];
    z[32]=z[9]*z[37];
    z[24]=n<T>(1,2)*z[24] - n<T>(9,2)*z[32] + z[27] + n<T>(391,2)*z[13];
    z[27]=z[7]*z[31];
    z[24]=n<T>(1,4)*z[24] + z[27];
    z[27]=z[5]*npow(z[12],3);
    z[31]=z[17]*z[12];
    z[32]= - z[31] - z[27];
    z[21]=z[32]*z[21];
    z[21]=z[21] - n<T>(113,2)*z[29] + n<T>(83,2)*z[9] - 113*z[12];
    z[21]=z[10]*z[21];
    z[32]=z[9]*z[13];
    z[21]=n<T>(1,8)*z[21] - n<T>(491,8)*z[16] - static_cast<T>(54)- n<T>(259,16)*z[32];
    z[21]=z[10]*z[21];
    z[22]=z[23] + n<T>(13,4) + z[22];
    z[22]=z[22]*z[25];
    z[20]=n<T>(221,16) + 22*z[20];
    z[20]=z[5]*z[20];
    z[20]=z[21] + z[22] + n<T>(1,2)*z[24] + z[20];
    z[20]=z[3]*z[20];
    z[15]=z[15] + z[20] + n<T>(1,2)*z[18] + z[19];
    z[15]=z[1]*z[15];
    z[18]=7*z[16] + static_cast<T>(7)+ 5*z[38];
    z[19]=n<T>(1,2)*z[9];
    z[20]=z[19] + z[45];
    z[20]=z[6]*z[20];
    z[18]=3*z[18] + 37*z[20];
    z[18]=z[10]*z[18];
    z[20]=z[7] - 3*z[9];
    z[20]=z[4]*z[20];
    z[20]= - n<T>(1,2) + z[20];
    z[20]=z[6]*z[20];
    z[18]=z[18] - 15*z[4] + n<T>(37,2)*z[20];
    z[19]=z[19] - z[30];
    z[19]=z[10]*z[19];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=n<T>(1,2)*z[16] + z[19];
    z[19]=9*z[3];
    z[16]=z[16]*z[19];
    z[16]=n<T>(1,4)*z[18] + z[16];
    z[15]=3*z[16] + z[15];
    z[15]=z[1]*z[15];
    z[16]=n<T>(37,4)*z[6];
    z[18]= - z[44]*z[16];
    z[18]=z[18] - n<T>(37,4)*z[45] + 9*z[29];
    z[18]=z[10]*z[18];
    z[20]= - z[9] + n<T>(1,2)*z[7];
    z[20]=n<T>(37,2)*z[20];
    z[21]= - z[9]*z[28]*z[20];
    z[22]= - z[10]*z[31];
    z[22]=z[29] + z[22];
    z[22]=z[22]*z[19];
    z[18]=z[22] + z[21] + z[18];
    z[18]=z[18]*npow(z[1],2);
    z[21]=z[26]*z[28];
    z[19]= - z[19]*z[27];
    z[19]= - n<T>(37,4)*z[21] + z[19];
    z[19]=z[2]*npow(z[1],3)*z[10]*z[19];
    z[18]=z[18] + z[19];
    z[19]=npow(z[2],2);
    z[18]=z[18]*z[19];
    z[17]=z[17]*z[40];
    z[20]= - z[4]*z[20];
    z[16]=z[9]*z[16];
    z[16]=z[18] + z[17] + z[16] + static_cast<T>(9)+ z[20];
    z[15]=z[15] + n<T>(3,4)*z[16];

    r += z[19]*z[15];
 
    return r;
}

template double qg_2lNLC_r825(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r825(const std::array<dd_real,31>&);
#endif
