#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1031(const std::array<T,31>& k) {
  T z[145];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[12];
    z[7]=k[15];
    z[8]=k[7];
    z[9]=k[14];
    z[10]=k[19];
    z[11]=k[13];
    z[12]=k[2];
    z[13]=k[5];
    z[14]=k[6];
    z[15]=k[26];
    z[16]=k[3];
    z[17]=k[4];
    z[18]=k[29];
    z[19]=k[30];
    z[20]=k[23];
    z[21]=k[20];
    z[22]=npow(z[15],2);
    z[23]=n<T>(8,3)*z[22];
    z[24]=npow(z[11],2);
    z[25]=z[23] + 5*z[24];
    z[26]=npow(z[14],3);
    z[25]=z[25]*z[26];
    z[27]=z[26]*z[5];
    z[28]=n<T>(1,2)*z[27];
    z[29]=z[26]*z[8];
    z[30]=z[28] - z[29];
    z[30]=z[2]*z[30];
    z[31]=z[11]*z[27];
    z[32]=z[15]*z[29];
    z[25]=z[30] - 4*z[32] + z[25] - 8*z[31];
    z[25]=z[2]*z[25];
    z[30]=npow(z[9],2);
    z[31]=5*z[30];
    z[32]=z[26]*z[31];
    z[33]=8*z[9];
    z[34]=z[27]*z[33];
    z[28]= - z[28] - n<T>(5,3)*z[29];
    z[28]=z[7]*z[28];
    z[35]=n<T>(1,2)*z[26];
    z[36]=z[35]*z[11];
    z[37]=z[2]*z[36];
    z[28]=z[28] + z[37] + z[32] + z[34];
    z[28]=z[7]*z[28];
    z[32]=4*z[6];
    z[34]= - z[15] + n<T>(4,3)*z[6];
    z[34]=z[34]*z[29]*z[32];
    z[37]=n<T>(8,3)*z[29];
    z[38]=z[36] + z[37];
    z[38]=z[7]*z[38];
    z[39]=z[5] - z[6];
    z[40]=npow(z[7],2);
    z[41]=z[39]*z[40];
    z[42]=npow(z[2],2);
    z[43]=z[42]*z[5];
    z[44]=5*z[43] - z[41];
    z[45]=n<T>(2,3)*z[3];
    z[44]=z[44]*z[45];
    z[36]= - z[36] + z[44];
    z[36]=z[3]*z[36];
    z[37]=z[2]*z[37];
    z[36]=z[36] + z[37] + z[38];
    z[36]=z[3]*z[36];
    z[37]=z[6]*z[15];
    z[38]=z[37] + z[22];
    z[44]=n<T>(2,3)*z[6];
    z[46]=z[38]*z[44];
    z[47]=npow(z[9],3);
    z[48]=z[46] - z[47];
    z[49]=4*z[26];
    z[48]=z[48]*z[49];
    z[50]= - z[30] + z[24];
    z[50]=z[50]*z[27];
    z[25]=z[36] + z[28] + z[25] + z[34] + z[48] + 5*z[50];
    z[25]=z[13]*z[25];
    z[28]=n<T>(10,3)*z[2];
    z[34]=2*z[21];
    z[36]=n<T>(1,3)*z[8];
    z[48]= - z[28] + z[34] - z[36];
    z[48]=z[2]*z[48];
    z[50]=z[34] - z[20];
    z[51]= - z[36] + z[44] + z[50];
    z[51]=z[7]*z[51];
    z[48]=z[48] + z[51];
    z[51]=2*z[3];
    z[48]=z[48]*z[51];
    z[52]=z[6]*z[16];
    z[53]=n<T>(2,3)*z[52];
    z[54]=z[16]*z[20];
    z[55]=z[54] - z[53];
    z[56]=z[11]*z[16];
    z[57]=n<T>(5,6)*z[56];
    z[58]=z[12]*z[9];
    z[55]= - n<T>(8,3)*z[58] + 2*z[55] - z[57];
    z[55]=z[55]*z[26];
    z[59]=z[24] + z[30];
    z[60]=z[59]*z[8];
    z[61]= - 34*z[5] + z[8];
    z[61]=z[61]*z[42];
    z[62]=n<T>(1,3)*z[40];
    z[63]=5*z[5];
    z[64]= - z[6] - z[63];
    z[64]=n<T>(1,2)*z[64] + z[8];
    z[64]=z[64]*z[62];
    z[48]=z[48] + z[64] + n<T>(1,3)*z[61] + z[55] - z[60];
    z[48]=z[3]*z[48];
    z[55]=npow(z[6],2);
    z[61]=n<T>(8,3)*z[55] - z[24];
    z[61]=z[16]*z[61];
    z[64]=z[30]*z[12];
    z[61]=z[64] + z[61];
    z[61]=z[61]*z[26];
    z[65]=npow(z[11],3);
    z[66]=z[65] + z[47];
    z[67]=4*z[8];
    z[68]= - z[66]*z[67];
    z[48]=z[48] + z[61] + z[68];
    z[48]=z[3]*z[48];
    z[61]=z[24]*z[26];
    z[68]=z[17]*z[20];
    z[69]= - z[16] - z[17];
    z[69]=z[11]*z[69];
    z[68]=z[68] + 6*z[69];
    z[68]=z[68]*z[61];
    z[69]=z[65]*z[10];
    z[70]=z[47]*z[10];
    z[70]=z[69] + z[70];
    z[59]=z[26]*z[17]*z[59];
    z[59]=2*z[70] + z[59];
    z[59]=z[5]*z[59];
    z[71]=n<T>(2,3)*z[26];
    z[55]= - z[16]*z[55]*z[71];
    z[55]=z[55] - z[70];
    z[70]=2*z[8];
    z[55]=z[55]*z[70];
    z[55]=z[55] + z[68] + z[59];
    z[59]=2*z[17];
    z[61]=z[59]*z[61];
    z[68]=z[27] - z[29];
    z[68]=z[2]*z[17]*z[68];
    z[61]=z[61] + z[68];
    z[61]=z[2]*z[61];
    z[68]=z[2]*z[15];
    z[71]=z[68] + z[22];
    z[72]=z[71]*z[2];
    z[73]=z[22]*z[6];
    z[72]=z[72] + z[73];
    z[74]= - z[59]*z[29];
    z[27]=z[17]*z[27];
    z[27]=z[74] + z[27] + n<T>(7,2)*z[72];
    z[27]=z[7]*z[27];
    z[74]= - z[26]*z[64];
    z[75]=4*z[47];
    z[76]=z[5]*z[75];
    z[27]=z[27] + z[74] + z[76];
    z[27]=z[7]*z[27];
    z[25]=z[25] + z[48] + z[27] + 2*z[55] + z[61];
    z[25]=z[13]*z[25];
    z[27]=n<T>(5,6)*z[5];
    z[48]=2*z[2];
    z[55]=4*z[21];
    z[61]=z[48] - z[36] - z[55] + z[27];
    z[61]=z[2]*z[61];
    z[74]=npow(z[16],2);
    z[76]=z[74]*z[11];
    z[77]=npow(z[12],2);
    z[78]=z[9]*z[77];
    z[78]= - n<T>(3,2)*z[76] + n<T>(13,3)*z[78];
    z[78]=z[78]*z[26];
    z[79]= - z[9] + z[11];
    z[79]=z[79]*z[36];
    z[80]=n<T>(1,2)*z[11];
    z[50]=2*z[50];
    z[81]= - z[8] + n<T>(1,6)*z[5] + z[80] + z[50] + n<T>(5,6)*z[6];
    z[81]=z[7]*z[81];
    z[82]=z[2]*z[12];
    z[83]= - static_cast<T>(7)+ 10*z[82];
    z[83]=z[2]*z[83];
    z[83]=z[6] + z[83];
    z[83]=z[83]*z[45];
    z[61]=z[83] + z[81] + z[61] + z[79] + z[24] + z[78];
    z[61]=z[3]*z[61];
    z[78]=z[77]*z[30];
    z[79]=7*z[78];
    z[81]=z[74]*z[24];
    z[83]= - 5*z[81] + z[79];
    z[83]=z[83]*z[26];
    z[84]=3*z[24];
    z[31]= - z[31] + z[84];
    z[31]=z[8]*z[31];
    z[85]=n<T>(11,3)*z[5] + z[70];
    z[85]=z[85]*z[42];
    z[86]=2*z[5];
    z[87]= - z[67] - 7*z[6] - z[86];
    z[87]=z[87]*z[62];
    z[88]=4*z[65];
    z[31]=z[61] + z[87] + z[85] + z[31] + z[88] + z[83];
    z[31]=z[3]*z[31];
    z[61]=npow(z[17],2);
    z[29]=z[61]*z[29];
    z[83]= - z[15] + 7*z[2];
    z[83]=z[2]*z[83];
    z[29]=z[83] - z[37] - 5*z[29];
    z[83]=n<T>(1,3)*z[7];
    z[29]=z[29]*z[83];
    z[79]=z[26]*z[79];
    z[85]=z[30]*z[5];
    z[29]=z[29] - z[85] + z[79] - n<T>(15,2)*z[72];
    z[29]=z[7]*z[29];
    z[79]=3*z[16];
    z[87]=z[17]*z[79];
    z[87]=z[87] + 5*z[74];
    z[87]=z[87]*z[65];
    z[89]= - z[19]*z[78];
    z[87]=2*z[87] + z[89];
    z[89]=2*z[26];
    z[87]=z[87]*z[89];
    z[90]= - z[33] - 11*z[10];
    z[90]=z[90]*z[30];
    z[91]=4*z[11];
    z[92]=9*z[10] + z[91];
    z[92]=z[92]*z[24];
    z[90]=z[90] + z[92];
    z[90]=z[5]*z[90];
    z[92]=4*z[37];
    z[93]= - z[22] - z[92];
    z[44]=z[93]*z[44];
    z[93]=3*z[10];
    z[94]= - z[93] + z[32];
    z[94]=z[94]*z[84];
    z[95]=z[30]*z[10];
    z[44]=z[94] + z[44] + 11*z[95];
    z[44]=z[8]*z[44];
    z[94]= - z[22]*z[36];
    z[96]=z[8]*z[68];
    z[94]=z[94] + z[96];
    z[94]=z[94]*z[48];
    z[25]=z[25] + z[31] + z[29] + z[94] + z[44] + z[87] + z[90];
    z[25]=z[13]*z[25];
    z[29]=n<T>(1,2)*z[4];
    z[31]=4*z[9];
    z[44]=z[29] + z[31];
    z[87]=n<T>(1,2)*z[8];
    z[90]=z[12]*z[4];
    z[94]=n<T>(1,2)*z[90];
    z[96]=n<T>(4,3) + z[94];
    z[96]=z[2]*z[96];
    z[96]=z[96] - z[87] + n<T>(7,6)*z[5] + z[44];
    z[96]=z[2]*z[96];
    z[97]=z[90] - 5;
    z[98]=z[97]*z[82];
    z[99]=n<T>(1,3)*z[90];
    z[100]=static_cast<T>(1)- z[99];
    z[98]=2*z[100] - n<T>(1,3)*z[98];
    z[98]=z[2]*z[98];
    z[100]=2*z[4];
    z[101]=z[100] + z[6];
    z[102]=z[4]*z[16];
    z[103]=n<T>(2,3)*z[102];
    z[104]=static_cast<T>(1)- z[103];
    z[104]=z[5]*z[104];
    z[98]=z[98] - n<T>(1,3)*z[101] + z[104];
    z[98]=z[98]*z[51];
    z[104]=npow(z[14],4);
    z[105]=z[104]*npow(z[16],4);
    z[106]= - z[24]*z[105];
    z[107]=z[11] + z[9];
    z[108]= - z[107]*z[87];
    z[109]=n<T>(1,2)*z[7];
    z[110]=n<T>(5,3)*z[5];
    z[111]= - z[8] + z[110] - n<T>(5,3)*z[6] + z[11];
    z[111]=z[111]*z[109];
    z[112]=z[5]*z[4];
    z[96]=z[98] + z[111] + z[96] + z[108] + z[106] - n<T>(5,6)*z[112];
    z[96]=z[3]*z[96];
    z[98]=3*z[9];
    z[106]= - z[8]*z[98];
    z[106]= - z[112] + z[106];
    z[108]=n<T>(7,3)*z[5];
    z[111]= - z[29] + z[108];
    z[111]=z[2]*z[111];
    z[106]=n<T>(1,2)*z[106] + z[111];
    z[106]=z[2]*z[106];
    z[105]= - z[88]*z[105];
    z[111]=z[8]*z[11];
    z[113]=z[5]*z[6];
    z[114]= - z[113] - 3*z[111];
    z[115]=z[39]*z[7];
    z[116]=n<T>(2,3)*z[115];
    z[114]=n<T>(1,2)*z[114] + z[116];
    z[114]=z[7]*z[114];
    z[96]=z[96] + z[114] + z[105] + z[106];
    z[96]=z[3]*z[96];
    z[105]=z[104]*z[88];
    z[106]=z[104]*z[5];
    z[114]=9*z[106];
    z[117]= - z[24]*z[114];
    z[118]=z[104]*z[8];
    z[119]= - z[118]*z[23];
    z[105]=z[119] + z[105] + z[117];
    z[105]=z[2]*z[105];
    z[66]=z[66]*z[106];
    z[46]= - z[118]*z[46];
    z[46]=z[66] + z[46];
    z[66]=z[75]*z[104];
    z[114]= - z[30]*z[114];
    z[114]= - z[66] + z[114];
    z[114]=z[7]*z[114];
    z[117]=z[42] + z[40];
    z[119]=z[7] + z[2];
    z[120]=z[51]*z[119];
    z[120]=z[120] - z[117];
    z[121]=z[118] - z[106];
    z[120]=z[45]*z[121]*z[120];
    z[46]=z[120] + z[114] + 4*z[46] + z[105];
    z[46]=z[13]*z[46];
    z[105]=z[7]*z[12];
    z[114]=z[47]*z[105];
    z[120]=z[65]*z[2];
    z[121]=z[17]*z[120];
    z[114]=z[114] + z[121];
    z[114]=z[104]*z[114];
    z[121]= - z[47] + z[65];
    z[106]=z[106]*z[17]*z[121];
    z[106]=z[106] + z[114];
    z[46]=4*z[106] + z[46];
    z[46]=z[13]*z[46];
    z[106]= - z[74]*z[65];
    z[114]= - z[47]*z[77];
    z[106]=z[106] + z[114];
    z[106]=z[106]*z[104];
    z[78]= - z[81] - z[78];
    z[78]=z[78]*z[104];
    z[81]=10*z[43];
    z[114]=z[81] + z[41];
    z[114]=z[114]*z[45];
    z[78]=z[78] + z[114];
    z[78]=z[3]*z[78];
    z[78]=4*z[106] + z[78];
    z[78]=z[3]*z[78];
    z[66]= - z[7]*z[77]*z[66];
    z[46]=z[46] + z[66] + z[78];
    z[46]=z[13]*z[46];
    z[66]=npow(z[16],3);
    z[78]=z[66]*z[104];
    z[106]=z[24]*z[78];
    z[114]= - z[5] - n<T>(5,3)*z[2];
    z[114]=z[2]*z[114];
    z[114]=z[114] + n<T>(1,3)*z[115];
    z[114]=z[3]*z[114];
    z[43]=4*z[114] + n<T>(5,6)*z[41] + 2*z[106] - n<T>(31,3)*z[43];
    z[43]=z[3]*z[43];
    z[78]=z[65]*z[78];
    z[43]=8*z[78] + z[43];
    z[43]=z[3]*z[43];
    z[78]=z[93]*z[47];
    z[106]=z[6] - z[10];
    z[114]= - z[106]*z[65];
    z[114]= - z[78] + z[114];
    z[114]=z[8]*z[114];
    z[69]=z[78] - z[69];
    z[69]=z[5]*z[69];
    z[69]=z[69] + z[114];
    z[78]=z[72]*z[40];
    z[43]=z[46] + z[43] + 4*z[69] + n<T>(7,2)*z[78];
    z[43]=z[13]*z[43];
    z[46]=z[113]*z[40];
    z[69]=z[112]*z[42];
    z[46]=z[46] + z[69];
    z[114]= - z[4] + z[63];
    z[114]=z[2]*z[114];
    z[114]= - 2*z[112] + z[114];
    z[114]=z[2]*z[114];
    z[121]= - 2*z[113] + z[115];
    z[121]=z[7]*z[121];
    z[114]=z[114] + z[121];
    z[114]=z[114]*z[51];
    z[114]= - n<T>(5,2)*z[46] + z[114];
    z[121]=npow(z[3],2);
    z[122]=n<T>(1,3)*z[121];
    z[114]=z[114]*z[122];
    z[123]=z[4]*z[10];
    z[124]=z[123]*z[47];
    z[125]=z[6]*z[10];
    z[126]=z[125]*z[65];
    z[124]=z[124] + z[126];
    z[126]=z[8] - z[5];
    z[124]= - z[124]*z[126];
    z[46]=n<T>(2,3)*z[46];
    z[127]=npow(z[3],3);
    z[128]=z[1]*z[127]*z[46];
    z[114]=z[128] + 4*z[124] + z[114];
    z[114]=z[1]*z[114];
    z[124]=z[8]*z[9];
    z[128]=n<T>(5,3)*z[112] + z[124];
    z[129]=n<T>(1,3)*z[2];
    z[130]=z[29] - 14*z[5];
    z[130]=z[130]*z[129];
    z[128]=n<T>(1,2)*z[128] + z[130];
    z[128]=z[2]*z[128];
    z[130]=n<T>(1,3)*z[5];
    z[101]=z[101]*z[130];
    z[131]=z[97]*z[129];
    z[132]=n<T>(1,3)*z[4];
    z[133]=z[132] - z[5];
    z[131]=2*z[133] + z[131];
    z[131]=z[2]*z[131];
    z[101]= - z[116] + z[101] + z[131];
    z[101]=z[101]*z[51];
    z[115]=z[115] - z[113];
    z[111]=z[111] - n<T>(5,3)*z[115];
    z[111]=z[111]*z[109];
    z[101]=z[101] + z[128] + z[111];
    z[101]=z[3]*z[101];
    z[46]=z[46] + z[101];
    z[46]=z[3]*z[46];
    z[101]=z[93] - z[4];
    z[101]=z[101]*z[31];
    z[101]=z[101] - 9*z[123];
    z[101]=z[101]*z[30];
    z[111]=z[106]*z[91];
    z[111]=z[111] + 9*z[125];
    z[111]=z[111]*z[24];
    z[101]=z[101] - z[111];
    z[101]= - z[101]*z[126];
    z[41]=z[81] - z[41];
    z[41]=z[41]*z[127];
    z[81]=z[47]*z[7];
    z[111]= - z[120] + z[81];
    z[111]=npow(z[13],4)*z[5]*z[111]*npow(z[14],5);
    z[41]=n<T>(1,3)*z[41] + 2*z[111];
    z[41]=z[13]*z[41];
    z[41]=z[114] + 2*z[41] + z[46] + n<T>(7,6)*z[78] + z[101];
    z[41]=z[1]*z[41];
    z[46]=npow(z[17],4);
    z[78]=npow(z[18],2);
    z[101]=z[46]*z[78];
    z[111]=z[4]*z[18];
    z[46]= - z[46]*z[111];
    z[46]= - z[101] + z[46];
    z[46]=z[4]*z[46]*z[104];
    z[104]=z[4]*z[17];
    z[114]=z[104] + 3;
    z[114]=z[114]*z[31];
    z[114]=z[114] - 9*z[4] + 19*z[10];
    z[114]=z[114]*z[9];
    z[114]=z[114] - 8*z[123];
    z[114]=z[114]*z[9];
    z[115]=8*z[125];
    z[106]=9*z[106];
    z[116]= - z[11]*z[106];
    z[116]= - z[115] + z[116];
    z[116]=z[11]*z[116];
    z[46]=n<T>(8,3)*z[46] + z[114] + z[116];
    z[46]=z[8]*z[46];
    z[101]=z[118]*z[101];
    z[101]= - 8*z[101] - n<T>(19,2)*z[72];
    z[116]=n<T>(7,2)*z[2];
    z[118]= - 5*z[15] + z[116];
    z[118]=z[118]*z[129];
    z[118]=z[118] - n<T>(5,3)*z[37] - n<T>(3,2)*z[113];
    z[118]=z[7]*z[118];
    z[101]=n<T>(1,3)*z[101] + z[118];
    z[101]=z[7]*z[101];
    z[118]=z[17]*z[6];
    z[128]= - static_cast<T>(1)- z[118];
    z[128]=z[128]*z[91];
    z[106]=z[106] + z[128];
    z[106]=z[11]*z[106];
    z[106]=z[115] + z[106];
    z[106]=z[11]*z[106];
    z[106]= - z[114] + z[106];
    z[106]=z[5]*z[106];
    z[114]=z[6]*z[88];
    z[41]=z[41] + z[43] + z[96] + z[101] - n<T>(3,2)*z[69] + z[46] + z[114]
    + z[106];
    z[41]=z[1]*z[41];
    z[43]=npow(z[17],3);
    z[46]=z[43]*z[26];
    z[69]=z[59]*z[18];
    z[96]= - n<T>(2,3)*z[46] - n<T>(11,3) - z[69];
    z[96]=z[8]*z[96];
    z[101]=n<T>(1,2)*z[2];
    z[106]=n<T>(7,3)*z[82];
    z[114]= - static_cast<T>(1)+ z[106];
    z[114]=z[114]*z[101];
    z[115]=z[17]*z[18];
    z[128]=z[115] + 1;
    z[131]= - z[6]*z[128];
    z[118]= - n<T>(3,2) + z[118];
    z[118]=z[5]*z[118];
    z[96]=z[114] + z[96] + z[118] + z[131];
    z[96]=z[7]*z[96];
    z[114]= - z[78]*z[61];
    z[118]=n<T>(1,3)*z[12];
    z[131]=z[78]*z[17];
    z[133]=z[131]*z[118];
    z[114]=z[114] + z[133];
    z[114]=z[12]*z[114];
    z[133]=z[78]*z[43];
    z[114]=z[133] + z[114];
    z[114]=z[114]*z[26];
    z[133]=z[59]*z[78];
    z[134]=n<T>(73,2)*z[18] + z[133];
    z[46]=z[18]*z[46];
    z[46]= - n<T>(20,3)*z[46] + n<T>(1,3)*z[134] + z[11];
    z[46]=z[8]*z[46];
    z[134]=npow(z[12],3);
    z[135]=z[134]*z[9];
    z[136]=z[26]*z[135];
    z[136]=7*z[15] + z[136];
    z[136]=n<T>(1,2)*z[136] - z[48];
    z[136]=z[2]*z[136];
    z[137]= - z[6]*z[131];
    z[46]=z[96] + z[136] + z[46] + z[113] + 8*z[114] + n<T>(7,2)*z[37] + 
    z[137];
    z[46]=z[7]*z[46];
    z[96]=7*z[9];
    z[113]=2*z[19];
    z[114]=z[96] - z[113] - n<T>(11,6)*z[4];
    z[114]=z[12]*z[114];
    z[94]=static_cast<T>(1)- z[94];
    z[94]=z[94]*z[82];
    z[94]=n<T>(13,3)*z[94] - n<T>(1,3) + z[114];
    z[94]=z[2]*z[94];
    z[114]=z[90] - static_cast<T>(1)+ z[102];
    z[136]=z[74]*z[6];
    z[137]=z[74]*z[4];
    z[136]=z[136] + z[137];
    z[138]=z[5]*z[136];
    z[114]=2*z[114] + z[138];
    z[138]=z[129]*z[77];
    z[97]=z[97]*z[138];
    z[139]= - z[19] + n<T>(2,3)*z[4];
    z[139]=z[12]*z[139];
    z[139]=n<T>(5,3) + z[139];
    z[139]=z[12]*z[139];
    z[97]=z[139] + z[97];
    z[97]=z[2]*z[97];
    z[97]=n<T>(1,3)*z[114] + z[97];
    z[97]=z[97]*z[51];
    z[114]=z[16]*z[19];
    z[139]= - z[114] + z[103];
    z[139]=2*z[139] - n<T>(1,2)*z[58];
    z[139]=z[139]*z[77];
    z[140]=z[66]*z[11];
    z[139]=n<T>(17,6)*z[140] + z[139];
    z[139]=z[139]*z[26];
    z[141]=n<T>(5,6)*z[102];
    z[142]=n<T>(1,2)*z[52];
    z[143]=z[141] + static_cast<T>(2)+ z[142];
    z[143]=z[5]*z[143];
    z[56]= - n<T>(11,6) + z[56];
    z[56]=z[11]*z[56];
    z[44]=z[97] + z[94] + z[143] + z[139] + z[56] - z[44];
    z[44]=z[3]*z[44];
    z[56]=z[66]*z[84];
    z[84]=npow(z[4],2);
    z[94]=z[16]*z[84]*z[77];
    z[56]=z[56] - n<T>(4,3)*z[94];
    z[56]=z[56]*z[89];
    z[35]=z[35]*z[135];
    z[35]= - z[48] + 11*z[8] + n<T>(5,2)*z[5] + z[35] + z[4] - z[33];
    z[35]=z[2]*z[35];
    z[48]=z[16]*z[91];
    z[48]= - static_cast<T>(7)+ z[48];
    z[48]=z[48]*z[24];
    z[89]= - n<T>(29,3)*z[9] - 9*z[11];
    z[89]=z[89]*z[87];
    z[94]=n<T>(32,3)*z[8] - z[11] - n<T>(1,2)*z[39];
    z[94]=z[7]*z[94];
    z[35]=z[44] + z[94] + z[35] + z[89] + n<T>(2,3)*z[112] + z[48] + z[56];
    z[35]=z[3]*z[35];
    z[44]= - z[22]*z[104];
    z[48]=19*z[15] + z[98];
    z[48]=z[48]*z[87];
    z[56]= - z[15]*z[104];
    z[89]= - static_cast<T>(4)+ z[104];
    z[89]=z[5]*z[89];
    z[56]=z[56] + z[89];
    z[56]=z[2]*z[56];
    z[23]=z[56] + z[48] + z[112] + z[23] + z[44];
    z[23]=z[2]*z[23];
    z[44]=z[111] + z[78];
    z[48]=z[100]*z[43]*z[44];
    z[56]=z[61]*z[4];
    z[44]= - z[44]*z[56];
    z[89]=z[104]*z[18];
    z[94]=z[131] + z[89];
    z[94]=z[94]*z[99];
    z[44]=z[44] + z[94];
    z[94]=2*z[12];
    z[44]=z[44]*z[94];
    z[97]= - z[17]*z[74];
    z[66]=z[97] - 2*z[66];
    z[66]=z[66]*z[65];
    z[44]=z[44] + z[48] + z[66];
    z[44]=z[44]*z[49];
    z[48]= - z[125] - z[123];
    z[49]=z[9]*z[17];
    z[66]=8*z[49];
    z[97]= - z[66] + static_cast<T>(11)+ z[104];
    z[112]=2*z[9];
    z[97]=z[97]*z[112];
    z[123]=2*z[10];
    z[125]=z[123] - z[4];
    z[97]=5*z[125] + z[97];
    z[97]=z[9]*z[97];
    z[125]=z[91]*z[17];
    z[125]=z[125] - 9;
    z[135]=z[6]*z[59];
    z[135]=z[135] - z[125];
    z[135]=z[11]*z[135];
    z[139]=8*z[10];
    z[135]=z[135] + z[139] - 5*z[6];
    z[135]=z[11]*z[135];
    z[48]=z[135] + 3*z[48] + z[97];
    z[48]=z[5]*z[48];
    z[97]=z[100]*z[61];
    z[135]=z[17] - z[16];
    z[143]= - z[135]*z[97];
    z[43]=z[43]*z[18];
    z[43]= - 5*z[43] + z[143];
    z[26]=z[43]*z[26];
    z[26]=n<T>(4,3)*z[26] + n<T>(8,3)*z[89] + n<T>(2,3)*z[131] + z[93] + n<T>(73,6)*
    z[18];
    z[26]=z[4]*z[26];
    z[43]= - z[123] + z[29];
    z[143]= - static_cast<T>(13)- 6*z[104];
    z[143]=z[143]*z[112];
    z[43]=5*z[43] + z[143];
    z[43]=z[9]*z[43];
    z[93]=z[93] + n<T>(19,2)*z[15];
    z[93]=z[6]*z[93];
    z[139]= - z[139] + n<T>(5,2)*z[6];
    z[139]=z[11]*z[139];
    z[26]=z[139] + z[43] + z[93] + z[26];
    z[26]=z[8]*z[26];
    z[38]=z[38]*z[6];
    z[43]= - z[78] - z[22];
    z[43]=z[6]*z[43]*z[104];
    z[93]=3*z[6];
    z[139]= - z[93] - z[11];
    z[139]=z[139]*z[24];
    z[23]=z[41] + z[25] + z[35] + z[46] + z[23] + z[26] + z[48] + z[44]
    + 4*z[139] + n<T>(8,3)*z[38] + z[43];
    z[23]=z[1]*z[23];
    z[25]=13*z[15] + z[116];
    z[25]=z[25]*z[129];
    z[26]=npow(z[14],2);
    z[25]=z[25] + n<T>(13,3)*z[37] + n<T>(7,2)*z[26];
    z[25]=z[7]*z[25];
    z[35]=n<T>(5,3)*z[8] - z[55] - z[98];
    z[35]=z[26]*z[35];
    z[41]=n<T>(1,3)*z[26];
    z[43]= - z[30] - z[41];
    z[44]=11*z[5];
    z[43]=z[43]*z[44];
    z[46]=z[26] - 11*z[71];
    z[46]=z[46]*z[101];
    z[25]=z[25] + z[46] + z[43] - n<T>(11,2)*z[73] - z[75] + z[35];
    z[25]=z[7]*z[25];
    z[27]= - z[27] + n<T>(7,3)*z[8];
    z[35]=n<T>(5,2)*z[11];
    z[43]= - z[34] - z[20];
    z[43]=n<T>(4,3)*z[7] + z[35] + 2*z[43] + n<T>(1,6)*z[6] + z[27];
    z[43]=z[7]*z[43];
    z[46]=n<T>(1,3)*z[6];
    z[48]= - z[83] + z[129] + z[20] - z[46];
    z[48]=z[48]*z[51];
    z[71]=n<T>(8,3)*z[8];
    z[116]= - z[107]*z[71];
    z[27]=9*z[2] - z[55] + z[27];
    z[27]=z[2]*z[27];
    z[139]=n<T>(1,2)*z[26];
    z[143]=3*z[30];
    z[27]=z[48] + z[43] + z[27] + z[116] - z[143] - z[139];
    z[27]=z[3]*z[27];
    z[43]=6*z[20];
    z[48]= - n<T>(7,3)*z[11] + z[43] - n<T>(13,3)*z[6];
    z[48]=z[48]*z[26];
    z[116]=5*z[8];
    z[44]=z[44] - z[116];
    z[44]=z[2]*z[44];
    z[44]= - n<T>(13,2)*z[26] + z[44];
    z[44]=z[44]*z[129];
    z[144]= - 8*z[8] + 13*z[6] + z[86];
    z[144]=z[7]*z[144];
    z[144]= - 5*z[26] + z[144];
    z[83]=z[144]*z[83];
    z[144]=12*z[47];
    z[27]=z[27] + z[83] + z[44] + z[60] - z[144] + z[48];
    z[27]=z[3]*z[27];
    z[44]=n<T>(1,3)*z[15];
    z[34]= - z[34] - z[44];
    z[34]=2*z[34] + n<T>(11,2)*z[11];
    z[34]=z[34]*z[26];
    z[48]= - 8*z[24] - n<T>(11,3)*z[26];
    z[48]=z[5]*z[48];
    z[60]= - 4*z[22] + z[41];
    z[60]=z[8]*z[60];
    z[83]=z[15]*z[67];
    z[83]=z[26] + z[83];
    z[83]=z[2]*z[83];
    z[34]=z[83] + z[60] + z[34] + z[48];
    z[34]=z[2]*z[34];
    z[33]= - z[10] + z[33];
    z[33]=z[33]*z[30];
    z[48]= - z[10] - z[91];
    z[48]=z[48]*z[24];
    z[60]=z[107]*z[26];
    z[33]=3*z[60] + z[33] + z[48];
    z[33]=z[5]*z[33];
    z[48]= - z[22] - 2*z[37];
    z[32]=z[48]*z[32];
    z[48]=z[10] + 2*z[6];
    z[48]=z[48]*z[24];
    z[60]=z[6]*z[26];
    z[32]=n<T>(8,3)*z[60] + z[48] + z[32] + z[95];
    z[32]=z[8]*z[32];
    z[48]= - n<T>(37,6)*z[5] - z[8];
    z[48]=z[48]*z[42];
    z[60]=11*z[6] + z[63];
    z[60]=n<T>(1,2)*z[60] - z[116];
    z[60]=z[60]*z[62];
    z[62]=z[2]*z[86];
    z[63]= - z[6] + z[86];
    z[63]=z[7]*z[63];
    z[62]=z[62] + z[63];
    z[63]=n<T>(4,3)*z[3];
    z[62]=z[62]*z[63];
    z[48]=z[62] + z[48] + z[60];
    z[48]=z[48]*z[121];
    z[60]=n<T>(7,6)*z[7];
    z[62]=z[72]*z[60];
    z[83]=z[5]*z[144];
    z[62]=z[83] + z[62];
    z[62]=z[7]*z[62];
    z[83]=z[117]*z[126];
    z[95]=n<T>(2,3)*z[127];
    z[95]= - z[13]*z[83]*z[95];
    z[107]= - z[2]*z[5]*z[88];
    z[48]=z[95] + z[48] + z[107] + z[62];
    z[48]=z[13]*z[48];
    z[62]=n<T>(1,3)*z[37];
    z[95]= - z[62] + z[30];
    z[107]=z[43] - z[11];
    z[107]=z[11]*z[107];
    z[95]=2*z[95] + z[107];
    z[95]=z[95]*z[26];
    z[25]=z[48] + z[27] + z[25] + z[34] + z[32] + z[95] + z[33];
    z[25]=z[13]*z[25];
    z[27]=17*z[17] - 19*z[12];
    z[27]=z[27]*z[41];
    z[32]=3*z[5];
    z[27]=z[27] + z[32];
    z[33]=static_cast<T>(5)+ z[106];
    z[33]=z[33]*z[101];
    z[27]=z[33] + n<T>(1,2)*z[27] + z[8];
    z[27]=z[7]*z[27];
    z[33]=n<T>(4,3)*z[26];
    z[34]= - z[17]*z[33];
    z[34]=z[80] + z[34];
    z[34]=z[8]*z[34];
    z[48]=z[26]*z[12];
    z[80]=z[9]*z[48];
    z[95]=z[5]*z[112];
    z[101]= - n<T>(19,6)*z[2] - z[15] + n<T>(1,2)*z[48];
    z[101]=z[2]*z[101];
    z[27]=z[27] + z[101] + z[34] + z[95] - n<T>(19,3)*z[80] - z[37] + z[30];
    z[27]=z[7]*z[27];
    z[34]= - 2*z[16] + n<T>(13,2)*z[12];
    z[34]=z[34]*z[41];
    z[80]=static_cast<T>(23)- 8*z[82];
    z[80]=z[80]*z[129];
    z[95]=static_cast<T>(1)- 4*z[82];
    z[63]=z[95]*z[63];
    z[95]=2*z[20];
    z[34]=z[63] + z[60] + z[80] + z[34] + n<T>(13,6)*z[11] + z[95] + n<T>(11,3)*
    z[9];
    z[34]=z[3]*z[34];
    z[53]= - z[54] - z[53];
    z[60]= - z[12]*z[31];
    z[53]=z[60] + 2*z[53] + z[57];
    z[53]=z[53]*z[26];
    z[57]=z[7] + n<T>(55,3)*z[8] - z[32] - n<T>(5,3)*z[48] + z[93] - z[11];
    z[57]=z[57]*z[109];
    z[60]=n<T>(3,2)*z[5];
    z[48]= - n<T>(3,2)*z[8] + z[48] + z[60];
    z[48]=3*z[48] + n<T>(13,2)*z[2];
    z[48]=z[2]*z[48];
    z[34]=z[34] + z[57] + z[48] - n<T>(16,3)*z[124] + z[53] - z[143] - z[24]
   ;
    z[34]=z[3]*z[34];
    z[48]= - z[10] + z[9];
    z[48]=z[48]*z[112];
    z[33]=z[52]*z[33];
    z[53]=9*z[15];
    z[57]=z[53] + n<T>(16,3)*z[6];
    z[57]=z[6]*z[57];
    z[63]=z[123] - n<T>(23,2)*z[6];
    z[63]=z[11]*z[63];
    z[33]=z[33] + z[63] + z[57] + z[48];
    z[33]=z[8]*z[33];
    z[48]=n<T>(2,3)*z[17];
    z[57]=z[26]*z[48];
    z[53]=z[53] + z[57];
    z[53]=z[8]*z[53];
    z[41]=z[17]*z[41];
    z[41]= - 6*z[5] + z[41];
    z[41]=z[11]*z[41];
    z[57]= - z[2]*z[32];
    z[41]=z[57] + z[53] + 8*z[22] + z[41];
    z[41]=z[2]*z[41];
    z[53]=3*z[47];
    z[57]=z[38] - z[53];
    z[63]=2*z[11];
    z[80]= - z[6] + z[63];
    z[80]=z[80]*z[24];
    z[57]=4*z[57] + z[80];
    z[80]= - z[20]*z[59];
    z[101]= - 11*z[16] - z[59];
    z[101]=z[11]*z[101];
    z[80]=z[80] + z[101];
    z[80]=z[11]*z[80];
    z[101]=z[19] + z[9];
    z[101]=z[12]*z[101]*z[112];
    z[80]=z[80] + z[101];
    z[80]=z[80]*z[26];
    z[101]=z[59]*z[9];
    z[106]=static_cast<T>(1)- z[101];
    z[106]=z[9]*z[106];
    z[106]=z[10] + 6*z[106];
    z[106]=z[106]*z[112];
    z[107]=z[11]*z[125];
    z[107]= - z[123] + z[107];
    z[107]=z[11]*z[107];
    z[106]=z[106] + z[107];
    z[106]=z[5]*z[106];
    z[25]=z[25] + z[34] + z[27] + z[41] + z[33] + z[106] + 2*z[57] + 
    z[80];
    z[25]=z[13]*z[25];
    z[27]=z[113] - z[132];
    z[27]=z[12]*z[27];
    z[33]=2*z[102];
    z[34]= - static_cast<T>(13)- z[33];
    z[27]=n<T>(1,3)*z[34] + z[27];
    z[27]=z[12]*z[27];
    z[34]=static_cast<T>(7)- 2*z[90];
    z[34]=z[34]*z[138];
    z[27]=z[34] - n<T>(1,3)*z[136] + z[27];
    z[27]=z[27]*z[51];
    z[29]= - z[96] - z[113] + z[29];
    z[29]=z[12]*z[29];
    z[34]=z[31] - z[113] + n<T>(25,6)*z[4];
    z[34]=z[12]*z[34];
    z[34]= - n<T>(73,6) + z[34];
    z[34]=z[12]*z[34];
    z[41]=z[77]*z[2];
    z[57]= - static_cast<T>(3)+ n<T>(11,6)*z[90];
    z[57]=z[57]*z[41];
    z[34]=z[34] + z[57];
    z[34]=z[2]*z[34];
    z[57]=z[102] - n<T>(5,3) - z[52];
    z[80]=z[79] - n<T>(5,3)*z[12];
    z[80]=z[12]*z[80];
    z[80]= - n<T>(11,3)*z[74] + z[80];
    z[80]=z[80]*z[139];
    z[106]=3*z[76];
    z[107]=n<T>(17,2)*z[16] - z[106];
    z[107]=z[11]*z[107];
    z[27]=z[27] + z[34] + z[80] + z[29] + n<T>(1,2)*z[57] + z[107];
    z[27]=z[3]*z[27];
    z[29]=z[77]*z[26];
    z[34]=n<T>(11,3)*z[29];
    z[57]=n<T>(1,6)*z[82];
    z[80]= - static_cast<T>(17)- 7*z[90];
    z[80]=z[80]*z[57];
    z[96]= - n<T>(17,6)*z[4] - z[96];
    z[96]=z[12]*z[96];
    z[80]=z[80] - z[34] + n<T>(52,3) + z[96];
    z[80]=z[2]*z[80];
    z[96]=5*z[16] - z[106];
    z[96]=z[11]*z[96];
    z[96]= - static_cast<T>(4)+ z[96];
    z[96]=z[96]*z[91];
    z[33]=z[114] + z[33];
    z[107]= - n<T>(9,2)*z[9] + 4*z[19] - 3*z[4];
    z[107]=z[12]*z[107];
    z[33]=2*z[33] + z[107];
    z[33]=z[12]*z[33];
    z[33]= - z[76] + z[33];
    z[33]=z[33]*z[26];
    z[107]= - n<T>(1,2)*z[102] - static_cast<T>(2)+ z[142];
    z[107]=z[5]*z[107];
    z[29]=n<T>(11,2) - z[29];
    z[29]=z[7]*z[29];
    z[84]=z[84]*z[12];
    z[84]=n<T>(8,3)*z[84];
    z[109]=3*z[19] + z[46];
    z[27]=z[27] + z[29] + z[80] - 10*z[8] + z[107] + z[33] + z[84] + 
    z[96] - n<T>(5,6)*z[9] + 2*z[109] - n<T>(13,2)*z[4];
    z[27]=z[3]*z[27];
    z[29]= - z[59] + n<T>(7,6)*z[12];
    z[29]=z[12]*z[29];
    z[29]=n<T>(3,2)*z[61] + z[29];
    z[29]=z[29]*z[26];
    z[33]=n<T>(5,3) + z[115];
    z[33]=z[17]*z[33]*z[67];
    z[80]=z[12]*z[18];
    z[96]=static_cast<T>(1)+ z[80];
    z[96]=z[96]*z[41];
    z[107]=n<T>(2,3) + n<T>(3,2)*z[80];
    z[107]=z[12]*z[107];
    z[96]=z[107] + n<T>(7,6)*z[96];
    z[96]=z[2]*z[96];
    z[107]=z[5]*z[17];
    z[109]= - static_cast<T>(25)- n<T>(41,2)*z[115];
    z[29]=z[96] + z[33] + z[107] + n<T>(1,3)*z[109] + z[29];
    z[29]=z[7]*z[29];
    z[33]=z[78]*z[12];
    z[96]= - z[18] + z[33];
    z[96]=z[12]*z[96];
    z[96]= - static_cast<T>(5)+ 7*z[96];
    z[57]=z[96]*z[57];
    z[96]=n<T>(3,2)*z[33];
    z[109]=n<T>(5,3)*z[18] + z[96];
    z[109]=z[12]*z[109];
    z[34]=z[57] - z[34] + static_cast<T>(1)+ z[109];
    z[34]=z[2]*z[34];
    z[57]=n<T>(1,2)*z[9];
    z[109]= - 2*z[18] - z[57];
    z[109]=z[12]*z[109];
    z[109]= - n<T>(8,3)*z[115] + z[109];
    z[109]=z[12]*z[109];
    z[116]=z[61]*z[18];
    z[109]=n<T>(34,3)*z[116] + z[109];
    z[109]=z[109]*z[26];
    z[117]=n<T>(17,3)*z[61];
    z[117]= - z[26]*z[117];
    z[121]=4*z[131] + 25*z[18];
    z[121]=z[121]*z[17];
    z[117]=z[117] - n<T>(67,6) - z[121];
    z[117]=z[8]*z[117];
    z[29]=z[29] + z[34] + z[117] + z[109] + n<T>(7,6)*z[131] + n<T>(52,3)*z[18]
    + z[39];
    z[29]=z[7]*z[29];
    z[34]=n<T>(1,2)*z[10];
    z[39]= - z[102]*z[34];
    z[109]=z[59]*z[11];
    z[109]=z[109] - 5;
    z[109]=z[109]*z[11];
    z[117]=z[10]*z[142];
    z[124]=6*z[49];
    z[127]= - static_cast<T>(7)+ z[124];
    z[127]=z[9]*z[127];
    z[39]=z[109] + z[127] + z[39] - z[123] + z[117];
    z[39]=z[5]*z[39];
    z[117]=8*z[104];
    z[127]= - n<T>(1,3) - z[115];
    z[127]=z[127]*z[117];
    z[34]=z[34]*z[16];
    z[121]=z[127] - z[121] - n<T>(29,3) + z[34];
    z[121]=z[4]*z[121];
    z[56]=z[59] + z[56];
    z[56]=z[56]*z[112];
    z[56]=z[56] + n<T>(31,3) + n<T>(23,2)*z[104];
    z[56]=z[9]*z[56];
    z[127]=z[135]*z[104];
    z[136]=z[26]*z[127];
    z[34]= - static_cast<T>(11)- z[34];
    z[34]=z[6]*z[34];
    z[34]= - n<T>(20,3)*z[136] + z[56] + z[121] + z[123] + z[34];
    z[34]=z[8]*z[34];
    z[56]=z[90]*z[78];
    z[78]= - z[111] + z[56];
    z[77]=z[78]*z[77];
    z[78]=3*z[107];
    z[121]= - z[15]*z[59];
    z[77]=z[78] + n<T>(7,6)*z[77] + static_cast<T>(1)+ z[121];
    z[77]=z[2]*z[77];
    z[121]=z[22]*z[17];
    z[123]= - z[44] - z[121];
    z[136]=n<T>(3,2)*z[56];
    z[139]=n<T>(5,3)*z[111] + z[136];
    z[139]=z[12]*z[139];
    z[143]= - static_cast<T>(3)- z[49];
    z[143]=z[143]*z[87];
    z[77]=z[77] + z[143] - z[110] + z[139] + 2*z[123] + 5*z[9];
    z[77]=z[2]*z[77];
    z[110]= - z[4]*z[135];
    z[110]= - n<T>(1,3)*z[115] + z[110];
    z[110]=z[4]*z[110];
    z[123]= - z[18] + n<T>(4,3)*z[4];
    z[123]=z[4]*z[123];
    z[98]=z[19]*z[98];
    z[98]=z[123] + z[98];
    z[98]=z[12]*z[98];
    z[98]=4*z[110] + z[98];
    z[98]=z[98]*z[94];
    z[110]=n<T>(17,3)*z[116] + 4*z[127];
    z[100]=z[110]*z[100];
    z[110]=z[16]*z[59];
    z[110]=9*z[74] + z[110];
    z[110]=z[110]*z[24];
    z[98]=z[98] + z[100] + z[110];
    z[26]=z[98]*z[26];
    z[73]=z[73]*z[17];
    z[62]= - z[62] - z[73];
    z[89]=8*z[89];
    z[98]=52*z[18] + n<T>(7,2)*z[131];
    z[98]=n<T>(1,3)*z[98] + z[89];
    z[98]=z[4]*z[98];
    z[100]= - static_cast<T>(6)- z[104];
    z[100]=z[100]*z[112];
    z[100]= - n<T>(19,2)*z[4] + z[100];
    z[100]=z[9]*z[100];
    z[110]=z[6] + z[11];
    z[110]=z[110]*z[63];
    z[23]=z[23] + z[25] + z[27] + z[29] + z[77] + z[34] + z[39] + z[26]
    + z[110] + z[100] + 2*z[62] + z[98];
    z[23]=z[1]*z[23];
    z[25]=z[68]*z[70];
    z[26]=z[5]*z[24];
    z[27]=z[8]*z[22];
    z[25]=z[25] - 6*z[27] + z[88] - 17*z[26];
    z[25]=z[2]*z[25];
    z[26]=z[37] + z[68];
    z[26]=z[7]*z[26];
    z[26]=3*z[26] - 19*z[85] - 8*z[47] - n<T>(7,6)*z[72];
    z[26]=z[7]*z[26];
    z[27]=n<T>(10,3)*z[8];
    z[29]=z[27] + z[93] + z[130];
    z[29]=z[29]*z[40];
    z[34]=n<T>(8,3)*z[2];
    z[39]= - z[5] - z[8];
    z[34]=z[39]*z[34];
    z[39]= - n<T>(11,3)*z[6] + z[11];
    z[39]= - z[71] + n<T>(1,2)*z[39] - n<T>(8,3)*z[5];
    z[39]=z[7]*z[39];
    z[40]=z[6]*z[45];
    z[34]=z[40] + z[34] + z[39];
    z[34]=z[3]*z[34];
    z[39]=z[108] + z[70];
    z[39]=z[39]*z[42];
    z[29]=z[34] + z[39] + z[29];
    z[29]=z[3]*z[29];
    z[34]= - z[3]*z[126]*z[119];
    z[34]=z[34] + z[83];
    z[34]=z[34]*z[122];
    z[39]= - z[86]*z[120];
    z[32]=z[32]*z[81];
    z[32]=z[34] + z[39] + z[32];
    z[32]=z[13]*z[32];
    z[34]=z[53] + z[65];
    z[34]=z[34]*z[86];
    z[39]= - 3*z[22] - z[92];
    z[40]=z[8]*z[6];
    z[39]=z[39]*z[40];
    z[34]=z[34] + z[39];
    z[25]=4*z[32] + z[29] + z[26] + 2*z[34] + z[25];
    z[25]=z[13]*z[25];
    z[26]=2*z[30];
    z[29]= - n<T>(3,2)*z[37] + z[26];
    z[32]=3*z[2];
    z[34]=z[32] - z[60] + n<T>(13,3)*z[8];
    z[34]=z[7]*z[34];
    z[39]= - z[20]*z[63];
    z[42]=z[12]*z[75];
    z[45]=z[5]*z[9];
    z[53]=n<T>(9,2)*z[15];
    z[62]= - n<T>(7,6)*z[2] - z[53] - z[11];
    z[62]=z[2]*z[62];
    z[29]=z[34] + z[62] + 10*z[45] + z[42] + 3*z[29] + z[39];
    z[29]=z[7]*z[29];
    z[34]=n<T>(1,3) + z[54];
    z[34]=z[34]*z[51];
    z[39]=n<T>(4,3)*z[9];
    z[42]=z[20] - z[39];
    z[34]=z[34] + n<T>(5,6)*z[7] + n<T>(13,6)*z[2] - 3*z[64] + 4*z[42] - z[35];
    z[34]=z[3]*z[34];
    z[27]=z[27] - z[60];
    z[28]= - z[28] + z[55] - z[27];
    z[28]=z[2]*z[28];
    z[35]=z[12]*z[47];
    z[35]=z[30] - 6*z[35];
    z[27]=n<T>(2,3)*z[7] + z[50] - n<T>(9,2)*z[6] - z[27];
    z[27]=z[7]*z[27];
    z[27]=z[34] + z[27] + 2*z[35] + z[28];
    z[27]=z[3]*z[27];
    z[28]= - static_cast<T>(3)- z[66];
    z[26]=z[28]*z[26];
    z[28]= - z[125]*z[24];
    z[26]=z[26] + z[28];
    z[26]=z[5]*z[26];
    z[28]=z[38] - 2*z[47];
    z[34]= - z[63] - z[53] + n<T>(32,3)*z[6];
    z[34]=z[34]*z[40];
    z[22]=z[22] + z[24];
    z[35]=7*z[11];
    z[38]= - z[5]*z[35];
    z[22]=4*z[22] + z[38];
    z[38]=n<T>(13,3)*z[2] - z[53];
    z[38]=z[8]*z[38];
    z[22]=2*z[22] + z[38];
    z[22]=z[2]*z[22];
    z[22]=z[25] + z[27] + z[29] + z[22] + z[34] + 8*z[28] + z[26];
    z[22]=z[13]*z[22];
    z[25]=z[74]*z[46];
    z[25]=z[118] + z[16] + z[25];
    z[25]=z[25]*z[51];
    z[26]=n<T>(25,6)*z[16] - z[106];
    z[26]=z[11]*z[26];
    z[25]=z[25] - n<T>(7,3)*z[105] + 2*z[82] + n<T>(25,3)*z[58] + z[26] + z[142]
    + n<T>(17,3) - 4*z[54];
    z[25]=z[3]*z[25];
    z[26]=8*z[52];
    z[27]= - n<T>(13,2) + z[26];
    z[27]=z[27]*z[46];
    z[28]= - z[74]*z[91];
    z[28]=z[79] + z[28];
    z[29]=3*z[11];
    z[28]=z[28]*z[29];
    z[28]= - n<T>(35,6) + z[28];
    z[28]=z[11]*z[28];
    z[29]= - static_cast<T>(59)+ n<T>(5,2)*z[82];
    z[29]=z[29]*z[129];
    z[34]=n<T>(23,6)*z[105];
    z[38]=static_cast<T>(7)+ z[34];
    z[38]=z[7]*z[38];
    z[25]=z[25] + z[38] + z[29] + 9*z[64] + z[28] - n<T>(28,3)*z[9] + z[95]
    + z[27];
    z[25]=z[3]*z[25];
    z[27]= - z[17]*z[15];
    z[27]=z[78] - static_cast<T>(1)+ z[27];
    z[27]=z[2]*z[27];
    z[28]= - z[21] - z[44];
    z[27]=z[27] + n<T>(14,3)*z[11] + 4*z[28] - z[121] + n<T>(19,3)*z[126];
    z[27]=z[2]*z[27];
    z[28]=z[8]*z[59];
    z[28]=3*z[82] + z[28] - n<T>(7,3) - z[107];
    z[28]=z[7]*z[28];
    z[29]= - static_cast<T>(11)- n<T>(7,2)*z[82];
    z[29]=z[29]*z[129];
    z[28]=z[28] + z[29] - n<T>(41,6)*z[8] - z[5] - z[64] + z[55] - z[9];
    z[28]=z[7]*z[28];
    z[24]=z[16]*z[24];
    z[24]= - 20*z[24] + z[43] - z[6];
    z[24]=z[11]*z[24];
    z[29]=static_cast<T>(1)+ z[124];
    z[29]=z[9]*z[29];
    z[29]=z[29] - z[109];
    z[29]=z[5]*z[29];
    z[26]= - n<T>(23,2) - z[26];
    z[26]=z[6]*z[26];
    z[26]= - z[35] + z[26] + z[9];
    z[26]=z[26]*z[36];
    z[22]=z[22] + z[25] + z[28] + z[27] + z[26] + z[29] + z[24] + 10*
    z[30] - n<T>(4,3)*z[37] - z[73];
    z[22]=z[13]*z[22];
    z[24]= - z[112] + z[113] - n<T>(5,3)*z[4];
    z[24]=z[24]*z[94];
    z[24]=z[24] + z[141] - n<T>(11,6) + 4*z[114];
    z[24]=z[12]*z[24];
    z[25]= - n<T>(19,2)*z[74] + 3*z[140];
    z[25]=z[11]*z[25];
    z[26]= - n<T>(11,3)*z[4] + z[9];
    z[26]=z[12]*z[26];
    z[26]= - n<T>(1,3) + z[26];
    z[26]=z[26]*z[41];
    z[27]=z[99] - n<T>(2,3) - z[114];
    z[27]=z[12]*z[27];
    z[27]=z[27] + z[16] + n<T>(1,3)*z[137];
    z[27]=z[12]*z[27];
    z[27]=n<T>(1,3)*z[74] + z[27];
    z[27]=z[27]*z[51];
    z[24]=z[27] + n<T>(1,2)*z[26] + z[24] + z[25] + z[79] - n<T>(2,3)*z[137];
    z[24]=z[3]*z[24];
    z[25]= - static_cast<T>(1)- z[103];
    z[25]=z[4]*z[25];
    z[25]=z[19] + z[25];
    z[25]=z[84] + 4*z[25] - z[57];
    z[25]=z[12]*z[25];
    z[26]=z[20] - z[19];
    z[26]=z[26]*z[79];
    z[26]=n<T>(2,3) + z[26];
    z[27]= - 19*z[74] + 12*z[140];
    z[27]=z[11]*z[27];
    z[27]=n<T>(19,2)*z[16] + z[27];
    z[27]=z[11]*z[27];
    z[28]= - n<T>(17,6) + 3*z[90];
    z[28]=z[28]*z[41];
    z[29]=z[113] - z[4];
    z[30]= - z[12]*z[29];
    z[30]=n<T>(97,6) + z[30];
    z[30]=z[12]*z[30];
    z[28]=z[30] + z[28];
    z[28]=z[2]*z[28];
    z[24]=z[24] - z[34] + z[28] + z[25] + z[27] + n<T>(19,3)*z[102] + 2*
    z[26] - n<T>(9,2)*z[52];
    z[24]=z[3]*z[24];
    z[25]=3*z[80];
    z[26]= - z[25] + n<T>(5,3) - 7*z[115];
    z[26]=z[12]*z[26];
    z[27]=z[17]*z[128];
    z[26]=n<T>(35,3)*z[27] + z[26];
    z[27]= - n<T>(7,3) - z[69];
    z[27]=z[8]*z[27]*z[61];
    z[28]=static_cast<T>(4)- n<T>(7,2)*z[80];
    z[28]=z[28]*z[138];
    z[26]=z[28] + n<T>(1,2)*z[26] + z[27];
    z[26]=z[7]*z[26];
    z[27]= - n<T>(9,2)*z[131] + n<T>(4,3)*z[18];
    z[28]= - z[96] - z[39] - z[27];
    z[28]=z[12]*z[28];
    z[30]=z[133] + n<T>(13,2)*z[18];
    z[34]=3*z[17];
    z[30]=z[30]*z[34];
    z[34]=n<T>(43,6) + z[30];
    z[34]=z[8]*z[34];
    z[35]=n<T>(61,6)*z[131] + 19*z[18];
    z[34]=z[34] - z[35];
    z[34]=z[17]*z[34];
    z[25]= - n<T>(17,6) + z[25];
    z[25]=z[25]*z[41];
    z[33]= - n<T>(7,6)*z[33] + z[18] - z[9];
    z[33]=z[12]*z[33];
    z[33]=n<T>(23,3) + z[33];
    z[33]=z[12]*z[33];
    z[25]=z[33] + z[25];
    z[25]=z[2]*z[25];
    z[25]=z[26] + z[25] + z[28] + n<T>(26,3) + z[34];
    z[25]=z[7]*z[25];
    z[26]=z[89] - z[27];
    z[26]=z[4]*z[26];
    z[27]=3*z[29] - z[31];
    z[27]=z[9]*z[27];
    z[26]= - z[136] + z[26] + z[27];
    z[26]=z[12]*z[26];
    z[27]= - z[19]*z[112];
    z[27]= - n<T>(7,6)*z[56] + z[111] + z[27];
    z[27]=z[12]*z[27];
    z[27]=z[112] + z[27];
    z[27]=z[12]*z[27];
    z[28]=z[134]*z[111]*z[32];
    z[29]=z[17]*z[87];
    z[27]=z[28] + z[29] + z[27] - static_cast<T>(8)- z[49];
    z[27]=z[2]*z[27];
    z[28]=n<T>(34,3) + z[30];
    z[28]=z[17]*z[28];
    z[29]=n<T>(2,3) + z[115];
    z[29]=z[17]*z[29];
    z[29]= - n<T>(1,3)*z[16] + z[29];
    z[29]=z[29]*z[117];
    z[28]=z[29] - n<T>(43,6)*z[16] + z[28];
    z[28]=z[4]*z[28];
    z[29]= - n<T>(5,3)*z[17] - z[97];
    z[29]=z[9]*z[29];
    z[30]= - static_cast<T>(1)+ 3*z[52];
    z[28]=z[29] + n<T>(3,2)*z[30] + z[28];
    z[28]=z[8]*z[28];
    z[29]= - z[17]*z[35];
    z[30]= - static_cast<T>(1)- z[69];
    z[30]=z[17]*z[30];
    z[30]=z[30] + n<T>(2,3)*z[16];
    z[30]=z[4]*z[30];
    z[29]=8*z[30] - n<T>(14,3) + z[29];
    z[29]=z[4]*z[29];
    z[30]=z[17]*z[31];
    z[30]=z[30] - n<T>(14,3) + 5*z[104];
    z[30]=z[9]*z[30];
    z[31]= - 7*z[16] + 6*z[76];
    z[31]=z[31]*z[63];
    z[31]=static_cast<T>(5)+ z[31];
    z[31]=z[11]*z[31];
    z[22]=z[23] + z[22] + z[24] + z[25] + z[27] + z[28] + z[26] + z[31]
    + z[30] + z[43] + z[29];
    z[22]=z[1]*z[22];
    z[23]= - z[112] - z[19] - z[132];
    z[23]=z[23]*z[94];
    z[24]=z[16] - z[48];
    z[24]=z[24]*z[67];
    z[25]= - 12*z[16] + n<T>(31,3)*z[17];
    z[25]=z[4]*z[25];
    z[26]= - n<T>(5,2)*z[16] - z[48];
    z[26]=z[11]*z[26];
    z[27]=z[17] - z[12];
    z[27]=z[2]*z[27];
    z[28]=41*z[17] - 17*z[12];
    z[28]=z[7]*z[28];
    z[29]= - n<T>(17,3)*z[16] + z[94];
    z[29]=z[3]*z[29];
    z[30]= - n<T>(7,6)*z[3] + 8*z[7] + n<T>(3,2)*z[2] - z[71] + n<T>(2,3)*z[11] - 4*
    z[20] + n<T>(1,3)*z[9];
    z[30]=z[13]*z[30];

    r += n<T>(1,6) + z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + n<T>(1,6)*
      z[28] + z[29] + z[30] + z[101];
 
    return r;
}

template double qg_2lNLC_r1031(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1031(const std::array<dd_real,31>&);
#endif
