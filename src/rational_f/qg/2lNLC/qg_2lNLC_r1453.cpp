#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1453(const std::array<T,31>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[2];
    z[7]=k[4];
    z[8]=k[6];
    z[9]=k[14];
    z[10]=k[27];
    z[11]=k[5];
    z[12]=2*z[7];
    z[13]=2*z[6];
    z[14]=z[12] - z[13];
    z[15]=npow(z[8],2);
    z[14]=z[15]*z[14];
    z[14]=z[4] + z[14];
    z[14]=z[7]*z[14];
    z[16]=z[6]*z[2];
    z[14]=z[14] + static_cast<T>(2)- z[16];
    z[17]=npow(z[6],2);
    z[18]=z[6] - z[12];
    z[18]=z[7]*z[18];
    z[18]=z[17] + z[18];
    z[19]=n<T>(1,3)*z[3];
    z[18]=z[18]*z[19];
    z[20]=static_cast<T>(2)- n<T>(5,3)*z[16];
    z[20]=z[6]*z[20];
    z[18]=z[18] + z[20] + n<T>(4,3)*z[7];
    z[18]=z[3]*z[18];
    z[14]=2*z[14] + z[18];
    z[18]=2*z[3];
    z[14]=z[14]*z[18];
    z[20]=npow(z[11],2);
    z[21]=z[20]*z[9];
    z[22]=z[9]*z[17];
    z[22]=z[21] + z[22];
    z[22]=z[22]*z[15];
    z[22]=z[22] + z[9];
    z[23]=n<T>(1,3)*z[4];
    z[24]=z[2]*z[11];
    z[25]=static_cast<T>(26)- 19*z[24];
    z[25]=z[25]*z[23];
    z[26]=z[6]*z[10];
    z[27]= - z[7]*z[10];
    z[27]=z[26] + z[27];
    z[28]=z[15]*z[7];
    z[27]=z[27]*z[28];
    z[14]=z[14] + 3*z[27] + z[25] - 5*z[2] - z[10] + n<T>(2,3)*z[22];
    z[14]=z[5]*z[14];
    z[22]= - z[1]*z[5];
    z[22]=z[22] + 1;
    z[25]=z[4]*z[2];
    z[22]=z[25]*z[22]*npow(z[3],3);
    z[27]=8*z[25];
    z[29]= - z[2] + z[4];
    z[29]=z[3]*z[29];
    z[29]= - z[27] + z[29];
    z[30]=npow(z[3],2);
    z[29]=z[5]*z[29]*z[30];
    z[22]=z[29] + z[22];
    z[22]=z[1]*z[22];
    z[29]=z[25]*z[7];
    z[31]=z[29] + z[2];
    z[32]=z[3]*z[31];
    z[27]=z[27] + z[32];
    z[27]=z[27]*z[30];
    z[30]=z[16] - 1;
    z[32]=z[7]*z[4];
    z[33]=z[32] - z[30];
    z[33]=z[3]*z[33];
    z[33]=z[33] - 7*z[2] + 8*z[4];
    z[33]=z[3]*z[33];
    z[33]= - 20*z[25] + z[33];
    z[33]=z[5]*z[3]*z[33];
    z[22]=z[22] + z[27] + z[33];
    z[22]=z[1]*z[22];
    z[27]= - z[9] + 16*z[2];
    z[27]=z[27]*z[23];
    z[31]=z[31]*z[18];
    z[27]=z[27] + z[31];
    z[27]=z[27]*z[18];
    z[31]=z[13] - z[7];
    z[33]=z[31]*z[7];
    z[33]=z[33] - z[17];
    z[34]= - z[33]*npow(z[8],3);
    z[34]=z[4] + z[34];
    z[34]=z[34]*z[12];
    z[30]= - z[6]*z[30];
    z[30]=z[30] + z[7];
    z[30]=z[30]*z[19];
    z[35]= - z[2]*z[13];
    z[30]=z[30] + z[34] + n<T>(7,3) + z[35];
    z[30]=z[3]*z[30];
    z[30]=z[30] - n<T>(13,3)*z[2] + 6*z[4];
    z[30]=z[30]*z[18];
    z[25]= - n<T>(41,3)*z[25] + z[30];
    z[25]=z[5]*z[25];
    z[22]=n<T>(2,3)*z[22] + z[27] + z[25];
    z[22]=z[1]*z[22];
    z[12]= - z[12]*z[31];
    z[12]=z[12] + 2*z[17];
    z[12]=z[15]*z[12];
    z[25]=z[7] - z[6];
    z[27]= - z[25]*z[19];
    z[12]=z[27] + n<T>(1,3) + z[12];
    z[12]=z[3]*z[12];
    z[27]=2*z[2];
    z[12]=z[12] + n<T>(8,3)*z[29] + z[27] + z[23];
    z[12]=z[12]*z[18];
    z[29]=z[20]*z[2];
    z[30]= - 5*z[21] - 4*z[29];
    z[15]=z[30]*z[15]*z[23];
    z[23]=z[2] + z[9];
    z[23]=z[23]*z[32];
    z[30]=2*z[4];
    z[31]=z[11]*z[9];
    z[32]= - z[31]*z[30];
    z[32]=z[32] - z[23];
    z[28]=z[32]*z[28];
    z[32]=z[10] - z[2];
    z[32]=z[9]*z[32];
    z[34]= - z[9] + n<T>(11,3)*z[2];
    z[34]=z[4]*z[34];
    z[12]=z[22] + z[14] + z[12] + z[28] + z[15] + z[34] + z[32];
    z[12]=z[1]*z[12];
    z[14]=z[11] - z[29];
    z[14]=z[4]*z[14];
    z[14]=z[24] - z[14];
    z[15]=z[31] - 1;
    z[14]=z[15] - 4*z[14];
    z[19]= - z[33]*z[19];
    z[19]=z[19] + z[13] - n<T>(11,3)*z[7];
    z[19]=z[7]*z[19];
    z[19]=n<T>(5,3)*z[17] + z[19];
    z[19]=z[3]*z[19];
    z[16]=static_cast<T>(2)- n<T>(1,3)*z[16];
    z[16]=z[6]*z[16];
    z[16]=z[19] + z[16] - 5*z[7];
    z[16]=z[16]*z[18];
    z[19]= - z[10] - n<T>(2,3)*z[2];
    z[19]=z[6]*z[19];
    z[22]=3*z[10] + n<T>(2,3)*z[4];
    z[22]=z[7]*z[22];
    z[14]=z[16] + z[22] + n<T>(1,3)*z[14] + z[19];
    z[14]=z[5]*z[14];
    z[16]= - z[9] - z[23];
    z[16]=z[7]*z[16];
    z[19]= - z[3]*z[33];
    z[19]= - 7*z[25] + z[19];
    z[19]=z[3]*z[19];
    z[22]= - z[6]*z[9];
    z[16]=z[19] + z[16] + static_cast<T>(7)+ z[22];
    z[16]=z[3]*z[16];
    z[19]= - 11*z[24] + static_cast<T>(14)- 5*z[31];
    z[19]=z[4]*z[19];
    z[19]=z[19] - 2*z[9] - 13*z[2];
    z[22]=z[27] - z[9];
    z[23]=z[4]*z[22];
    z[23]=n<T>(4,3)*z[23] - z[32];
    z[23]=z[7]*z[23];
    z[25]=z[6]*z[32];
    z[12]=z[12] + z[14] + n<T>(2,3)*z[16] + z[23] + n<T>(1,3)*z[19] + z[25];
    z[12]=z[1]*z[12];
    z[14]=npow(z[11],3);
    z[16]= - z[9]*z[14]*z[30];
    z[19]= - static_cast<T>(1)- z[31];
    z[19]=z[11]*z[19];
    z[16]= - z[13] + z[19] + z[16];
    z[19]=n<T>(2,3)*z[8];
    z[16]=z[16]*z[19];
    z[23]= - z[21] - n<T>(1,3)*z[29];
    z[23]=z[23]*z[30];
    z[25]=z[13]*z[10];
    z[16]=z[16] - z[25] + z[23] - 2*z[24] + n<T>(1,3) - z[31];
    z[16]=z[8]*z[16];
    z[23]= - z[31] - z[24];
    z[23]=z[4]*z[23];
    z[21]= - z[21] - z[29];
    z[21]=z[4]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[19];
    z[21]=z[21] + z[10] + z[23];
    z[23]=z[7]*z[8];
    z[21]=z[21]*z[23];
    z[16]=z[21] + z[16] + z[22];
    z[16]=z[7]*z[16];
    z[21]= - z[4]*z[20];
    z[21]= - z[11] + z[21];
    z[22]=z[31] + n<T>(1,3);
    z[21]=z[22]*z[21];
    z[22]=z[26] - z[22];
    z[22]=z[6]*z[22];
    z[14]= - z[4]*z[14];
    z[14]= - z[20] + z[14];
    z[14]=z[15]*z[14];
    z[20]= - z[11]*z[15];
    z[20]=z[20] + z[6];
    z[20]=z[6]*z[20];
    z[14]=z[20] + z[14];
    z[14]=z[14]*z[19];
    z[14]=z[14] + z[22] + z[21];
    z[14]=z[8]*z[14];
    z[20]=z[13] - z[11];
    z[21]= - z[20]*z[19];
    z[21]=z[21] - n<T>(7,3) - z[25];
    z[21]=z[8]*z[21];
    z[22]=z[10] + z[19];
    z[22]=z[22]*z[23];
    z[21]=z[21] + z[22];
    z[21]=z[7]*z[21];
    z[19]=z[17]*z[19];
    z[22]=n<T>(8,3) + z[26];
    z[22]=z[6]*z[22];
    z[19]=z[19] - n<T>(4,3)*z[11] + z[22];
    z[19]=z[8]*z[19];
    z[13]=z[13]*z[8];
    z[13]=z[13] - z[23];
    z[22]=z[7]*z[13];
    z[23]=z[17]*z[8];
    z[22]= - z[23] + z[22];
    z[22]=z[22]*z[18];
    z[19]=z[22] + z[21] + n<T>(8,3) + z[19];
    z[19]=z[7]*z[19];
    z[21]=z[11]*z[23];
    z[17]=z[17] + 2*z[21];
    z[15]=z[8]*z[15]*z[17];
    z[15]= - 2*z[20] + z[15];
    z[15]=n<T>(1,3)*z[15] + z[19];
    z[15]=z[5]*z[15];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[7]*z[13];
    z[13]=z[13] + z[6] - z[23];
    z[13]=z[13]*z[18];
    z[17]=z[11]*z[30];
    z[18]= - z[9] - z[27];
    z[18]=z[6]*z[18];
    z[12]=z[12] + z[15] + z[13] + z[16] + z[14] + z[18] + z[17] + static_cast<T>(2)- 
    z[31];

    r += 2*z[12];
 
    return r;
}

template double qg_2lNLC_r1453(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1453(const std::array<dd_real,31>&);
#endif
