#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r154(const std::array<T,31>& k) {
  T z[140];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[3];
    z[7]=k[6];
    z[8]=k[9];
    z[9]=k[13];
    z[10]=k[5];
    z[11]=k[7];
    z[12]=k[14];
    z[13]=k[2];
    z[14]=k[4];
    z[15]=k[24];
    z[16]=k[26];
    z[17]=k[17];
    z[18]=k[18];
    z[19]=k[27];
    z[20]=2*z[5];
    z[21]=npow(z[11],2);
    z[22]=z[20]*z[21];
    z[23]=npow(z[15],2);
    z[24]=3*z[2];
    z[25]= - z[4]*z[24];
    z[25]= - z[23] + z[25];
    z[25]=z[4]*z[25];
    z[26]=npow(z[9],3);
    z[25]=6*z[26] + z[22] + z[25];
    z[25]=z[14]*z[25];
    z[27]=npow(z[12],2);
    z[28]=4*z[27];
    z[29]=z[8]*z[13];
    z[30]=z[29]*z[28];
    z[31]=3*z[23];
    z[28]=z[31] + z[28];
    z[32]=z[5]*z[13];
    z[28]=z[28]*z[32];
    z[33]=2*z[6];
    z[34]=z[33]*z[21];
    z[35]=z[11]*z[6];
    z[36]=z[35]*z[4];
    z[37]= - z[34] + z[36];
    z[38]=2*z[4];
    z[37]=z[37]*z[38];
    z[39]=z[9]*z[6];
    z[40]=6*z[39];
    z[41]=n<T>(1,2)*z[8];
    z[42]=z[16] + z[41];
    z[42]=z[6]*z[42];
    z[42]=z[42] + z[40];
    z[43]=npow(z[9],2);
    z[42]=z[42]*z[43];
    z[25]=z[25] + z[42] + z[37] + z[30] + z[28];
    z[28]=npow(z[7],3);
    z[25]=z[25]*z[28];
    z[30]=2*z[17];
    z[37]=n<T>(5,4)*z[2];
    z[42]=z[37] + z[30] - n<T>(3,2)*z[8];
    z[42]=z[5]*z[42];
    z[44]=4*z[4];
    z[45]= - z[2] - z[11];
    z[45]=z[45]*z[44];
    z[46]=6*z[11];
    z[47]=n<T>(3605,18)*z[2] + z[46];
    z[47]=z[11]*z[47];
    z[42]=z[45] + z[47] - z[23] + z[42];
    z[42]=z[4]*z[42];
    z[45]=z[4]*z[11];
    z[47]=z[11]*z[5];
    z[48]=z[45] - z[47];
    z[49]=z[2] + z[8];
    z[50]=z[49]*z[9];
    z[51]=n<T>(7,2)*z[2];
    z[52]=3*z[8] + z[51];
    z[52]=z[5]*z[52];
    z[52]=z[50] + z[52] - z[48];
    z[53]=n<T>(1,2)*z[9];
    z[52]=z[52]*z[53];
    z[54]=2*z[2];
    z[55]= - z[21]*z[54];
    z[42]=z[52] + z[55] + z[42];
    z[42]=z[42]*z[28];
    z[52]=npow(z[4],2);
    z[55]=z[52]*z[11];
    z[56]=z[26]*z[55];
    z[42]=n<T>(3,2)*z[56] + z[42];
    z[42]=z[10]*z[42];
    z[56]=n<T>(1,2)*z[4];
    z[57]=z[56]*z[11];
    z[57]=z[57] - z[23];
    z[58]=z[21] - z[57];
    z[58]=z[4]*z[58];
    z[59]=n<T>(3,2)*z[4];
    z[60]=z[15] + z[11];
    z[60]=5*z[60] - z[59];
    z[61]=z[9]*z[4];
    z[60]=z[60]*z[61];
    z[58]=z[58] + z[60];
    z[58]=z[58]*z[43];
    z[60]=npow(z[8],2);
    z[62]= - z[5]*z[60]*z[52];
    z[25]=z[42] + z[25] + z[62] + z[58];
    z[25]=z[10]*z[25];
    z[42]=npow(z[5],2);
    z[58]=z[42]*z[54];
    z[62]= - z[49]*z[20];
    z[63]=n<T>(1,2)*z[2];
    z[64]=z[16] + z[63];
    z[64]=z[11]*z[64];
    z[62]=z[62] + z[64];
    z[62]=z[4]*z[62];
    z[64]=npow(z[2],2);
    z[65]=z[64]*z[18];
    z[66]=z[11]*z[64];
    z[58]=z[62] + z[66] - z[65] + z[58];
    z[58]=z[4]*z[58];
    z[62]=z[23] + z[27];
    z[66]=npow(z[13],2);
    z[67]=z[66]*z[5];
    z[62]=z[62]*z[67];
    z[68]=z[8] + z[16];
    z[69]=npow(z[6],2);
    z[70]= - z[68]*z[69];
    z[71]=z[69]*z[9];
    z[72]=6*z[71];
    z[70]=z[70] - z[72];
    z[70]=z[70]*z[43];
    z[73]=5*z[5];
    z[74]=z[21]*z[73];
    z[75]=z[2]*z[43];
    z[74]=z[74] + z[75];
    z[74]=z[14]*z[74];
    z[75]=z[6]*z[26];
    z[74]= - 12*z[75] + z[74];
    z[74]=z[14]*z[74];
    z[62]=z[74] + z[62] + 2*z[70];
    z[62]=z[62]*z[28];
    z[70]=4*z[12];
    z[74]=2*z[11];
    z[75]=z[56] - z[74] - 5*z[15] + z[70];
    z[75]=z[4]*z[75];
    z[76]=z[11]*z[16];
    z[75]=n<T>(3,2)*z[61] + z[76] + z[75];
    z[75]=z[9]*z[75];
    z[57]=z[4]*z[57];
    z[57]=z[57] + z[75];
    z[57]=z[9]*z[57];
    z[75]= - z[27]*z[21];
    z[76]=z[26]*z[14];
    z[77]=z[52]*z[76];
    z[25]=z[25] + z[62] + n<T>(3,2)*z[77] + z[57] + z[75] + z[58];
    z[25]=z[10]*z[25];
    z[57]=z[5]*z[2];
    z[58]=z[4]*z[2];
    z[62]= - z[54] + z[3];
    z[62]=z[3]*z[62];
    z[62]= - z[58] + z[62] + z[57];
    z[62]=z[5]*z[62];
    z[75]=npow(z[3],2);
    z[77]=z[75]*z[2];
    z[62]=z[77] + z[62];
    z[62]=z[4]*z[62];
    z[78]=npow(z[6],4);
    z[79]=n<T>(1,2)*z[3];
    z[80]= - z[78]*z[43]*npow(z[7],4)*z[79]*z[8];
    z[81]=z[74]*z[58];
    z[82]= - z[2]*z[21];
    z[81]=z[82] + z[81];
    z[81]=z[81]*z[38];
    z[49]=z[49]*z[5];
    z[82]=n<T>(1,4)*z[49];
    z[83]=z[43]*z[82];
    z[81]=z[81] + z[83];
    z[83]=z[10]*z[7];
    z[81]=z[81]*npow(z[83],4);
    z[84]= - z[1]*z[58];
    z[84]=z[84] - z[2];
    z[85]=z[75]*z[5];
    z[84]=z[85]*z[84];
    z[86]=z[70]*z[60];
    z[87]= - z[2]*z[86];
    z[62]=z[81] + z[80] + z[62] + z[87] + z[84];
    z[62]=z[1]*z[62];
    z[80]=3*z[16];
    z[81]= - z[80] - z[37];
    z[81]=z[3]*z[81];
    z[84]= - z[16] + n<T>(58,9)*z[2];
    z[84]=z[84]*z[74];
    z[87]=z[2]*z[18];
    z[88]=2*z[3];
    z[89]= - z[5] + n<T>(97,6)*z[2] + z[88];
    z[89]=z[5]*z[89];
    z[90]= - z[16] + z[63];
    z[90]=z[4]*z[90];
    z[81]=z[90] + z[84] + z[89] - z[87] + z[81];
    z[81]=z[4]*z[81];
    z[84]=z[60]*z[6];
    z[89]=2*z[8];
    z[90]=z[84] - z[80] - z[89];
    z[90]=z[3]*z[90];
    z[91]=z[68]*z[6];
    z[92]=n<T>(1,4) + z[91];
    z[92]=z[3]*z[92];
    z[92]= - n<T>(3,2)*z[9] - z[56] - z[16] + z[92];
    z[92]=z[9]*z[92];
    z[93]=z[70]*z[2];
    z[94]=2*z[16];
    z[95]=z[94] + z[8];
    z[96]= - z[11]*z[95];
    z[97]= - z[56] - n<T>(62,9)*z[11] + z[15] + n<T>(17,4)*z[3];
    z[97]=z[4]*z[97];
    z[90]=z[92] + z[97] + z[96] - z[93] + z[90];
    z[90]=z[9]*z[90];
    z[92]=n<T>(1,4)*z[2];
    z[96]= - z[16] + z[92];
    z[96]=z[4]*z[3]*z[96];
    z[77]=z[96] + z[77] + z[85];
    z[77]=z[4]*z[77];
    z[96]=3*z[9];
    z[97]= - z[4]*z[96];
    z[93]=z[97] + z[93] - n<T>(1,2)*z[52];
    z[93]=z[9]*z[93];
    z[97]=z[3]*z[52];
    z[93]=n<T>(3,4)*z[97] + z[93];
    z[93]=z[9]*z[93];
    z[97]= - z[79] - z[96];
    z[97]=z[43]*z[52]*z[97];
    z[98]= - z[27]*z[75];
    z[99]=z[27]*z[3];
    z[100]= - z[11]*z[99];
    z[98]=z[98] + z[100];
    z[98]=z[11]*z[98];
    z[97]=z[98] + n<T>(1,2)*z[97];
    z[97]=z[14]*z[97];
    z[98]= - z[11]*z[27];
    z[98]= - z[99] + z[98];
    z[98]=z[98]*z[74];
    z[77]=z[97] + z[93] + z[98] + z[77];
    z[77]=z[14]*z[77];
    z[34]= - z[3]*z[34];
    z[93]=n<T>(1,4)*z[43];
    z[97]=z[3]*z[6];
    z[93]=z[97]*z[93];
    z[98]=z[5]*z[3];
    z[100]=n<T>(11,4)*z[98] + 3*z[47];
    z[100]=z[11]*z[100];
    z[100]= - 2*z[85] + z[100];
    z[100]=z[14]*z[100];
    z[101]=z[13]*z[85];
    z[34]=z[100] + z[93] + z[101] + z[34];
    z[34]=z[14]*z[34];
    z[93]=z[66]*z[85];
    z[100]=z[69]*z[3];
    z[72]=n<T>(5,2)*z[100] + z[72];
    z[72]=z[72]*z[43];
    z[34]=z[34] + z[93] + z[72];
    z[34]=z[14]*z[34];
    z[72]=npow(z[6],3);
    z[93]=z[16] + n<T>(7,4)*z[8];
    z[93]=z[93]*z[72];
    z[101]=z[72]*z[3];
    z[102]=z[72]*z[9];
    z[93]=6*z[102] + z[93] + n<T>(7,4)*z[101];
    z[93]=z[9]*z[93];
    z[103]=z[72]*z[79];
    z[104]=z[8]*z[103];
    z[93]=z[104] + z[93];
    z[93]=z[9]*z[93];
    z[34]=z[93] + z[34];
    z[28]=z[34]*z[28];
    z[34]=2*z[13];
    z[93]=z[34]*z[12];
    z[104]= - static_cast<T>(1)- z[93];
    z[104]=z[104]*z[89];
    z[105]=5*z[12];
    z[104]=z[105] + z[104];
    z[104]=z[2]*z[8]*z[104];
    z[106]=z[2]*z[13];
    z[107]= - static_cast<T>(1)+ 6*z[106];
    z[107]=z[60]*z[107];
    z[108]=z[3]*z[2];
    z[107]=z[108] + z[107];
    z[107]=z[3]*z[107];
    z[108]=z[106] - 1;
    z[109]= - z[3]*z[108];
    z[109]= - z[2] + z[109];
    z[109]=z[3]*z[109];
    z[109]=z[109] + z[57];
    z[109]=z[5]*z[109];
    z[110]=z[12] + z[8];
    z[110]=z[2]*z[110];
    z[110]= - z[27] + z[110];
    z[110]=z[11]*z[110];
    z[25]=z[62] + z[25] + z[28] + z[77] + z[90] + z[81] + z[110] + 
    z[109] + z[107] + z[86] + z[104];
    z[25]=z[1]*z[25];
    z[28]=3*z[15];
    z[62]= - n<T>(9,4)*z[5] + n<T>(559,12)*z[2] - z[30] - z[28];
    z[62]=z[4]*z[62];
    z[77]=npow(z[17],2);
    z[81]=12*z[77];
    z[90]=z[74] - n<T>(1172,9)*z[2] - 7*z[5];
    z[90]=z[11]*z[90];
    z[104]=n<T>(5,4)*z[4];
    z[107]=n<T>(15,2)*z[9] - n<T>(166,9)*z[2] - z[104];
    z[107]=z[9]*z[107];
    z[62]=z[107] + z[62] + z[81] + z[90];
    z[62]=z[14]*z[62];
    z[90]= - z[23] + 9*z[27];
    z[107]=3*z[13];
    z[109]= - z[90]*z[107];
    z[110]=5*z[17];
    z[111]=2*z[15];
    z[112]=z[110] + z[111];
    z[112]= - z[89] + 2*z[112] - n<T>(23,2)*z[12];
    z[112]=z[13]*z[112];
    z[112]=3*z[106] + z[112];
    z[112]=z[5]*z[112];
    z[113]=4*z[6];
    z[114]= - z[21]*z[113];
    z[91]= - z[91] + 17*z[39];
    z[91]=z[9]*z[91];
    z[115]=8*z[29];
    z[116]= - z[12]*z[115];
    z[36]=z[62] + z[91] + n<T>(223,18)*z[36] + z[114] + z[112] + z[109] + 
    z[116];
    z[62]=npow(z[7],2);
    z[36]=z[36]*z[62];
    z[91]=4*z[17];
    z[109]= - z[89] + z[91] - z[28];
    z[112]=n<T>(17,4)*z[12];
    z[114]=z[24] - z[112] - z[109];
    z[114]=z[5]*z[114];
    z[116]=2*z[12];
    z[104]= - z[104] - n<T>(1387,9)*z[11] - n<T>(13,4)*z[5] - n<T>(707,4)*z[2] + 
    z[116] - z[109];
    z[104]=z[4]*z[104];
    z[109]=n<T>(3515,9)*z[2] - 11*z[5];
    z[109]=n<T>(1,2)*z[109] + z[46];
    z[109]=z[11]*z[109];
    z[117]=z[2]*z[89];
    z[118]=n<T>(5,2)*z[8];
    z[119]= - n<T>(7,2)*z[9] + n<T>(7,4)*z[4] - n<T>(3,2)*z[5] + n<T>(581,18)*z[2] + 
    z[16] + z[118];
    z[119]=z[9]*z[119];
    z[104]=z[119] + z[104] + z[109] + z[114] + z[117] + z[81] - z[23];
    z[104]=z[104]*z[62];
    z[109]=n<T>(1,3)*z[60];
    z[114]=z[8] - z[17];
    z[117]=16*z[114] + z[2];
    z[117]=z[5]*z[117];
    z[117]=z[117] + z[81] - z[109];
    z[117]=z[5]*z[117];
    z[119]=z[16] + z[54];
    z[119]=z[119]*z[74];
    z[119]=z[119] + z[60] - z[49];
    z[119]=z[4]*z[119];
    z[120]=z[116]*z[60];
    z[121]= - z[120] - z[65];
    z[122]=z[11]*z[2];
    z[123]=z[64] - z[122];
    z[123]=z[123]*z[74];
    z[117]=z[119] + z[123] + 2*z[121] + z[117];
    z[117]=z[4]*z[117];
    z[119]=3*z[27];
    z[121]= - z[21] + z[60] + z[23] + z[119];
    z[121]=z[5]*z[121];
    z[123]=n<T>(5,4)*z[8];
    z[124]=z[123] + z[16];
    z[125]=z[124] + z[92];
    z[126]=n<T>(1,4)*z[12] + z[125];
    z[126]=z[5]*z[126];
    z[127]=3*z[4];
    z[128]=z[127] - n<T>(33,2)*z[11] - 11*z[15] + z[70];
    z[128]=z[4]*z[128];
    z[126]=z[128] + z[126] - n<T>(1,4)*z[47];
    z[126]=z[9]*z[126];
    z[31]= - z[31] - 4*z[21];
    z[31]=z[4]*z[31];
    z[31]=z[126] + z[31] + z[121];
    z[31]=z[9]*z[31];
    z[121]=z[27]*z[89];
    z[114]=z[114]*z[5];
    z[126]= - 2*z[77] + z[114];
    z[126]=z[5]*z[126];
    z[121]=z[121] + z[126];
    z[121]=z[121]*z[74];
    z[126]=z[77] - z[60];
    z[42]= - z[126]*z[42]*z[44];
    z[44]=3*z[43];
    z[128]= - z[55]*z[44];
    z[42]=z[42] + z[128];
    z[42]=z[10]*z[42];
    z[31]=z[42] + z[104] + z[31] + z[121] + z[117];
    z[31]=z[10]*z[31];
    z[42]= - 6*z[77] + z[23];
    z[104]= - z[74] - z[2] - z[16] + z[8];
    z[104]=z[104]*z[38];
    z[117]= - z[8]*z[105];
    z[121]= - z[5] + n<T>(389,12)*z[2] + 38*z[17] + 13*z[8];
    z[121]=z[5]*z[121];
    z[128]= - z[16] + n<T>(721,9)*z[2];
    z[128]=z[11]*z[128];
    z[42]=z[104] + z[128] + z[121] - z[87] + 2*z[42] + z[117];
    z[42]=z[4]*z[42];
    z[104]=4*z[77];
    z[117]=z[104] - z[27];
    z[121]= - z[8]*z[70];
    z[128]=z[8] + z[54];
    z[128]=z[2]*z[128];
    z[129]=10*z[17];
    z[130]= - z[129] + z[8];
    z[130]=z[5]*z[130];
    z[131]=z[12]*z[74];
    z[117]=z[131] + z[130] + z[128] + 3*z[117] + z[121];
    z[117]=z[11]*z[117];
    z[121]= - z[23] + z[27];
    z[37]= - z[37] - z[41] + z[16] + n<T>(3,2)*z[12];
    z[37]=z[5]*z[37];
    z[128]=7*z[15];
    z[130]= - n<T>(247,9)*z[11] + z[128] - z[105];
    z[130]=z[4]*z[130];
    z[59]= - z[59] - n<T>(23,2)*z[11] - 18*z[15] + z[112];
    z[59]=z[9]*z[59];
    z[131]= - 3*z[11] - n<T>(7,4)*z[5] - z[16] - z[89];
    z[131]=z[11]*z[131];
    z[37]=z[59] + z[130] + z[131] + 3*z[121] + z[37];
    z[37]=z[9]*z[37];
    z[59]= - z[109] - z[87];
    z[59]=z[2]*z[59];
    z[59]= - z[120] + z[59];
    z[57]=z[23] + z[57];
    z[57]=z[5]*z[57];
    z[44]= - z[14]*z[52]*z[44];
    z[31]=z[31] + z[36] + z[44] + z[37] + z[42] + z[117] + 2*z[59] + 
    z[57];
    z[31]=z[10]*z[31];
    z[36]= - z[17]*z[105];
    z[37]= - n<T>(3,4)*z[2] - z[16] - z[105];
    z[37]=z[3]*z[37];
    z[42]= - z[94] - z[2];
    z[42]=z[4]*z[42];
    z[36]=z[42] + n<T>(65,6)*z[98] + z[37] - z[104] + z[36];
    z[36]=z[4]*z[36];
    z[37]=z[46] - z[2] + 17*z[17] - 15*z[12];
    z[37]=z[12]*z[37];
    z[42]=z[3]*z[12];
    z[44]= - z[5]*z[79];
    z[37]=z[44] + z[42] + z[81] + z[37];
    z[37]=z[11]*z[37];
    z[44]=n<T>(3,2)*z[3];
    z[57]=n<T>(3,4)*z[4];
    z[59]= - 6*z[9] + z[57] - z[44] - z[112] - 4*z[2];
    z[59]=z[9]*z[59];
    z[81]=z[79] - z[2];
    z[81]=z[12]*z[81];
    z[109]=n<T>(15,4)*z[3] + z[4];
    z[109]=z[4]*z[109];
    z[59]=z[59] + z[109] - z[119] + z[81];
    z[59]=z[9]*z[59];
    z[81]=z[11]*z[70];
    z[81]=z[81] + z[27] + z[42];
    z[81]=z[3]*z[81];
    z[109]=9*z[12];
    z[117]=z[109]*z[17];
    z[117]=z[117] + z[104];
    z[120]=z[12]*z[117];
    z[81]=z[120] + z[81];
    z[81]=z[11]*z[81];
    z[120]=3*z[3];
    z[121]= - z[120] - z[56];
    z[121]=z[4]*z[121];
    z[130]=z[12]*z[79];
    z[121]=z[130] + z[121];
    z[61]=n<T>(1,2)*z[121] - 6*z[61];
    z[61]=z[9]*z[61];
    z[61]= - z[99] + z[61];
    z[61]=z[9]*z[61];
    z[99]=z[4]*z[85];
    z[61]=z[61] + z[81] + z[99];
    z[61]=z[14]*z[61];
    z[81]= - z[119] - 2*z[42];
    z[81]=z[3]*z[81];
    z[36]=z[61] + z[59] + z[36] + z[37] + z[81] + z[85];
    z[36]=z[14]*z[36];
    z[37]=n<T>(1,2)*z[5];
    z[59]=5*z[19];
    z[44]=4*z[11] + z[37] - z[44] + z[59] + n<T>(116,9)*z[2];
    z[44]=z[11]*z[44];
    z[61]=3*z[5];
    z[81]=z[19] - n<T>(21,4)*z[3];
    z[81]=z[81]*z[61];
    z[99]=n<T>(1,4)*z[3];
    z[119]= - n<T>(5,4)*z[9] + n<T>(71,9)*z[2] - z[99];
    z[119]=z[9]*z[119];
    z[44]=z[119] + z[44] - z[75] + z[81];
    z[44]=z[14]*z[44];
    z[81]=z[6] - z[13];
    z[119]=z[81]*z[75];
    z[121]=z[3]*z[13];
    z[130]=z[13]*z[19];
    z[131]= - z[130] + n<T>(19,4)*z[121];
    z[131]=z[131]*z[61];
    z[132]= - z[19] + z[79];
    z[133]=5*z[11];
    z[132]=z[133]*z[6]*z[132];
    z[134]= - n<T>(9,4)*z[97] - 10*z[39];
    z[134]=z[9]*z[134];
    z[44]=z[44] + z[134] + z[132] + z[119] + z[131];
    z[44]=z[14]*z[44];
    z[119]= - z[6]*z[13];
    z[119]=2*z[66] + z[119];
    z[119]=z[3]*z[119];
    z[131]=z[8]*z[66];
    z[132]=z[6]*z[29];
    z[119]=z[119] + z[131] - n<T>(1,2)*z[132];
    z[119]=z[3]*z[119];
    z[131]= - z[69]*z[41];
    z[131]= - n<T>(63,2)*z[71] + z[131] - 7*z[100];
    z[131]=z[131]*z[53];
    z[132]= - z[12] + z[24];
    z[132]=z[66]*z[132];
    z[134]=z[66]*z[3];
    z[132]= - n<T>(15,4)*z[134] + z[132];
    z[132]=z[5]*z[132];
    z[44]=z[44] + z[131] + z[119] + z[132];
    z[44]=z[44]*z[62];
    z[62]=5*z[16];
    z[38]=z[38] - z[133] + 20*z[5] - n<T>(1387,36)*z[2] + 6*z[8] - z[116] - 
    z[111] - z[62] - 23*z[17];
    z[38]=z[4]*z[38];
    z[116]=z[33]*z[60];
    z[119]=6*z[29];
    z[131]=static_cast<T>(5)- z[119];
    z[131]=z[8]*z[131];
    z[131]=z[131] - z[94] - z[15];
    z[132]=z[13]*z[15];
    z[135]= - z[2]*z[34];
    z[132]=z[132] + z[135];
    z[132]=z[3]*z[132];
    z[135]=n<T>(13,4) - 7*z[29];
    z[135]=z[2]*z[135];
    z[131]=z[132] + z[116] + 2*z[131] + z[135];
    z[131]=z[3]*z[131];
    z[132]=z[66]*z[2];
    z[135]=z[13] - z[132];
    z[135]=z[3]*z[135];
    z[135]=z[135] + static_cast<T>(1)- n<T>(1,4)*z[106];
    z[135]=z[3]*z[135];
    z[136]=z[13]*z[12];
    z[137]=n<T>(53,3) - z[136];
    z[137]=z[2]*z[137];
    z[108]=z[5]*z[108];
    z[108]=z[108] + z[135] + z[19] + z[137];
    z[108]=z[5]*z[108];
    z[68]= - z[68]*z[33];
    z[68]=static_cast<T>(1)+ z[68];
    z[68]=z[68]*z[97];
    z[95]=z[6]*z[95];
    z[40]=z[40] + z[68] - n<T>(13,2) + 3*z[95];
    z[40]=z[9]*z[40];
    z[62]=z[62] - z[116];
    z[62]=z[6]*z[62];
    z[62]=n<T>(11,4) + z[62];
    z[62]=z[3]*z[62];
    z[68]=3*z[84];
    z[95]=z[68] + z[18];
    z[40]=z[40] + n<T>(29,4)*z[4] - n<T>(133,9)*z[11] + z[62] + n<T>(107,9)*z[2] + 
    z[8] + n<T>(17,2)*z[12] + z[15] - 9*z[16] + z[95];
    z[40]=z[9]*z[40];
    z[62]=2*z[19];
    z[116]=z[62] + z[12];
    z[116]=z[116]*z[12];
    z[135]= - z[23] - z[116];
    z[137]= - static_cast<T>(1)+ z[93];
    z[137]=z[137]*z[89];
    z[137]= - z[105] + z[137];
    z[137]=z[8]*z[137];
    z[138]=n<T>(2,3)*z[29] - n<T>(59,3) + z[136];
    z[138]=z[8]*z[138];
    z[138]=z[138] + z[18] + 6*z[12];
    z[138]=z[2]*z[138];
    z[139]=z[19] + z[91];
    z[139]= - n<T>(21,2)*z[5] + z[3] + n<T>(107,3)*z[2] + 5*z[139] - 16*z[12];
    z[139]=z[11]*z[139];
    z[25]=z[25] + z[31] + z[44] + z[36] + z[40] + z[38] + z[139] + 
    z[108] + z[131] + z[138] + 3*z[135] + z[137];
    z[25]=z[1]*z[25];
    z[31]=z[84] - z[16];
    z[36]=8*z[11];
    z[38]= - z[36] + z[37] - 6*z[2] + z[89] + z[31];
    z[38]=z[4]*z[38];
    z[40]=z[104] - z[23];
    z[44]= - z[105] + n<T>(13,3)*z[8];
    z[44]=z[8]*z[44];
    z[108]=z[89] + 9*z[17];
    z[108]=2*z[108] + n<T>(67,4)*z[2];
    z[108]=z[5]*z[108];
    z[36]=z[36] + z[16] + n<T>(3461,18)*z[2];
    z[36]=z[11]*z[36];
    z[36]=z[38] + z[36] + z[108] - 2*z[40] + z[44];
    z[36]=z[4]*z[36];
    z[38]=2*z[126] + z[114];
    z[38]=z[38]*z[20];
    z[44]=z[64] - 4*z[122];
    z[44]=z[11]*z[44];
    z[108]=z[16] + 7*z[2];
    z[45]=z[108]*z[45];
    z[38]=z[45] + z[44] + z[38] - z[86] - z[65];
    z[38]=z[4]*z[38];
    z[44]= - z[49] + z[48];
    z[44]=z[9]*z[44];
    z[44]=z[55] + z[44];
    z[44]=z[44]*z[53];
    z[38]=z[38] + z[44];
    z[38]=z[10]*z[38];
    z[44]=z[17]*z[20];
    z[45]= - z[24] - z[5];
    z[45]=z[11]*z[45];
    z[44]=z[45] + z[44] - z[104] + z[64];
    z[44]=z[11]*z[44];
    z[45]= - z[56] - n<T>(160,9)*z[11] + z[28] - z[105];
    z[45]=z[4]*z[45];
    z[48]= - z[56] - z[125];
    z[48]=z[9]*z[48];
    z[55]= - z[74] + n<T>(9,4)*z[2] + z[12] + n<T>(15,4)*z[8];
    z[55]=z[5]*z[55];
    z[45]=z[48] + z[45] - z[60] + z[55];
    z[45]=z[9]*z[45];
    z[40]= - z[27] + z[40];
    z[40]=10*z[114] + 3*z[40] - 8*z[60];
    z[40]=z[5]*z[40];
    z[48]= - z[8]*z[12];
    z[48]=2*z[27] + z[48];
    z[48]=z[8]*z[48];
    z[55]= - 4*z[60] - z[87];
    z[55]=z[2]*z[55];
    z[36]=z[38] + z[45] + z[36] + z[44] + z[40] + 4*z[48] + z[55];
    z[36]=z[10]*z[36];
    z[38]=9*z[39];
    z[40]=6*z[16];
    z[44]=z[40] + n<T>(13,4)*z[8];
    z[44]=z[6]*z[44];
    z[44]=z[38] - n<T>(9,4) + z[44];
    z[44]=z[9]*z[44];
    z[44]=z[44] + z[57] - n<T>(137,36)*z[11] + z[68] + n<T>(319,9)*z[2] + n<T>(17,2)
   *z[8] - z[80] - z[70];
    z[44]=z[9]*z[44];
    z[45]= - 4*z[47] - 8*z[77] - z[64];
    z[45]=z[11]*z[45];
    z[48]=z[5] - z[16] - z[51];
    z[48]=z[4]*z[48];
    z[48]=z[48] - z[104] - z[23];
    z[48]=z[4]*z[48];
    z[51]=n<T>(13,2)*z[2] - z[127];
    z[51]=z[9]*z[51];
    z[51]= - n<T>(1,4)*z[52] + z[51];
    z[51]=z[9]*z[51];
    z[45]=z[51] + z[48] + z[65] + z[45];
    z[45]=z[14]*z[45];
    z[48]= - z[16] - z[129];
    z[51]=z[74]*z[6];
    z[55]=static_cast<T>(3)+ z[51];
    z[55]=z[4]*z[55];
    z[48]=z[55] - n<T>(325,6)*z[11] + n<T>(53,4)*z[5] + 7*z[84] - n<T>(5087,36)*z[2]
    + n<T>(55,3)*z[8] + 2*z[48] + z[15];
    z[48]=z[4]*z[48];
    z[55]=n<T>(5,3) - z[93];
    z[55]=z[55]*z[89];
    z[55]= - 13*z[12] + z[55];
    z[55]=z[8]*z[55];
    z[57]=z[13]*z[89];
    z[57]= - n<T>(13,3) + z[57];
    z[57]=z[57]*z[89];
    z[57]=z[18] + z[57];
    z[57]=z[2]*z[57];
    z[60]= - z[23]*z[34];
    z[60]=18*z[2] + n<T>(17,6)*z[8] + z[60] - n<T>(1,2)*z[12] - z[28] + z[16] + 
   47*z[17];
    z[60]=z[5]*z[60];
    z[64]=n<T>(105,2)*z[2] - z[91] + z[112];
    z[64]=z[46] + 3*z[64] + n<T>(17,2)*z[5];
    z[64]=z[11]*z[64];
    z[27]=z[36] + z[45] + z[44] + z[48] + z[64] + z[60] + z[57] + z[55]
    - z[23] - 27*z[27];
    z[27]=z[10]*z[27];
    z[36]= - z[120] - z[20];
    z[36]=z[11]*z[36];
    z[36]=z[36] - n<T>(3,4)*z[98] + n<T>(25,4)*z[42] - z[117];
    z[36]=z[11]*z[36];
    z[44]= - z[3] - 5*z[4];
    z[44]=z[44]*z[53];
    z[42]=z[44] - n<T>(5,2)*z[42] + z[52];
    z[42]=z[42]*z[53];
    z[44]=z[52]*z[37];
    z[36]=z[42] + z[44] + z[85] + z[36];
    z[36]=z[14]*z[36];
    z[42]=7*z[17];
    z[44]= - z[109] + z[62] + z[42];
    z[44]=z[12]*z[44];
    z[44]=z[104] + z[44];
    z[30]= - z[19] - z[30];
    z[30]= - z[46] + 9*z[5] - z[79] - n<T>(110,3)*z[2] + 10*z[30] + 31*z[12]
   ;
    z[30]=z[11]*z[30];
    z[45]=9*z[15];
    z[46]= - z[6]*z[79];
    z[46]=static_cast<T>(1)+ z[46];
    z[46]=z[9]*z[46];
    z[46]=z[46] + n<T>(15,4)*z[4] - n<T>(1,9)*z[11] + n<T>(9,2)*z[3] - n<T>(115,9)*z[2]
    + z[70] - z[18] - z[45];
    z[46]=z[9]*z[46];
    z[48]=3*z[12];
    z[55]= - z[18] - z[48];
    z[54]=z[55]*z[54];
    z[55]=z[15] - z[70];
    z[55]=z[55]*z[88];
    z[57]=n<T>(39,2) + z[121];
    z[57]=z[3]*z[57];
    z[57]=z[5] + z[57] - 3*z[19] + z[16];
    z[57]=z[5]*z[57];
    z[42]=n<T>(11,2)*z[5] - z[3] + n<T>(257,36)*z[2] + z[15] - z[16] - z[42];
    z[42]=z[4]*z[42];
    z[30]=z[36] + z[46] + z[42] + z[30] + z[57] + z[55] + 3*z[44] + 
    z[54];
    z[30]=z[14]*z[30];
    z[36]= - z[66]*z[123];
    z[36]=12*z[13] + z[36];
    z[36]=z[2]*z[36];
    z[42]= - n<T>(43,4) + z[119];
    z[42]=z[8]*z[42];
    z[31]=z[42] - z[31];
    z[31]=z[6]*z[31];
    z[42]=z[34]*z[3];
    z[44]= - 8*z[15] - z[12];
    z[44]=z[13]*z[44];
    z[31]=z[42] + z[31] + z[36] + z[115] - n<T>(1,6) + z[44];
    z[31]=z[3]*z[31];
    z[36]=z[13] + 15*z[132];
    z[36]=n<T>(1,4)*z[36] + z[134];
    z[36]=z[3]*z[36];
    z[36]= - z[32] + z[36] + n<T>(13,4)*z[106] + n<T>(79,2) + z[130];
    z[36]=z[5]*z[36];
    z[44]=z[84] - z[94] + z[118];
    z[44]=z[6]*z[44];
    z[44]=static_cast<T>(8)+ z[44];
    z[44]=z[44]*z[97];
    z[46]=z[6]*z[124];
    z[46]= - static_cast<T>(3)+ z[46];
    z[46]=z[46]*z[100];
    z[54]= - z[40] - n<T>(7,2)*z[8];
    z[54]=z[6]*z[54];
    z[54]=n<T>(77,4) + z[54];
    z[54]=z[6]*z[54];
    z[46]= - 9*z[71] + z[54] + z[46];
    z[46]=z[9]*z[46];
    z[40]= - n<T>(19,2)*z[8] + z[40] - z[95];
    z[40]=z[6]*z[40];
    z[40]=z[46] + z[44] - n<T>(401,12) + z[40];
    z[40]=z[9]*z[40];
    z[23]=z[23] - z[116];
    z[23]=z[23]*z[107];
    z[44]=n<T>(10,3)*z[29] + static_cast<T>(22)- z[136];
    z[44]=z[8]*z[44];
    z[46]= - n<T>(38,3)*z[29] + n<T>(1501,36) + 6*z[136];
    z[46]=z[2]*z[46];
    z[54]=z[89]*z[6];
    z[55]=z[35] + n<T>(149,9) - z[54];
    z[55]=z[4]*z[55];
    z[57]=z[33]*z[3];
    z[57]= - n<T>(599,18) + z[57];
    z[57]=z[11]*z[57];
    z[23]=z[25] + z[27] + z[30] + z[40] + z[55] + z[57] + z[36] + z[31]
    + 8*z[84] + z[46] + z[44] + z[23] - 26*z[12] + z[128] - z[91] - 
    z[16] - z[18] - 10*z[19];
    z[23]=z[1]*z[23];
    z[20]= - z[3]*z[20];
    z[20]=z[20] - z[47];
    z[20]=z[11]*z[20];
    z[20]=z[85] + z[20];
    z[20]=z[14]*z[20];
    z[25]=n<T>(61,12) - z[42];
    z[25]=z[3]*z[25];
    z[25]= - z[19] + z[25];
    z[25]=z[5]*z[25];
    z[27]= - z[79] - z[5];
    z[27]=3*z[27] - z[11];
    z[27]=z[11]*z[27];
    z[20]=z[20] + z[27] + z[75] + z[25];
    z[20]=z[14]*z[20];
    z[25]=z[113] - z[100];
    z[25]=z[11]*z[25];
    z[27]=z[6]*z[59];
    z[25]=z[25] - z[97] - n<T>(773,18) + z[27];
    z[25]=z[11]*z[25];
    z[27]=z[19]*z[34];
    z[30]= - n<T>(19,6)*z[13] + z[134];
    z[30]=z[3]*z[30];
    z[27]=z[30] - n<T>(5,6) + z[27];
    z[27]=z[5]*z[27];
    z[30]=z[79]*z[69];
    z[31]=z[30] - z[6];
    z[36]=n<T>(3,2)*z[71];
    z[40]= - z[36] - z[31];
    z[40]=z[9]*z[40];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[9]*z[40];
    z[42]= - z[34] - z[6];
    z[42]=z[3]*z[42];
    z[42]=n<T>(49,12) + z[42];
    z[42]=z[3]*z[42];
    z[20]=z[20] + z[40] + z[25] + z[27] - z[19] + z[42];
    z[20]=z[14]*z[20];
    z[25]=3*z[69] - z[103];
    z[25]=n<T>(1,2)*z[25] - z[102];
    z[25]=z[25]*z[96];
    z[25]=z[25] + z[113] - z[30];
    z[25]=z[9]*z[25];
    z[27]=z[13]*z[33];
    z[27]=z[66] + z[27];
    z[27]=z[3]*z[27];
    z[27]=z[27] - n<T>(8,3)*z[13] + n<T>(13,4)*z[6];
    z[27]=z[3]*z[27];
    z[40]=n<T>(65,6) - z[130];
    z[40]=z[13]*z[40];
    z[40]=z[40] + n<T>(1,12)*z[134];
    z[40]=z[5]*z[40];
    z[42]=z[69]*z[11];
    z[44]= - z[42] - n<T>(809,18)*z[6] + 2*z[100];
    z[44]=z[11]*z[44];
    z[46]= - 10*z[6] + z[34];
    z[46]=z[19]*z[46];
    z[20]=z[20] + z[25] + z[44] + z[40] + z[27] + n<T>(731,9) + z[46];
    z[20]=z[14]*z[20];
    z[25]=n<T>(1,2)*z[11];
    z[27]=z[3] + z[61];
    z[27]=z[27]*z[25];
    z[40]=z[47]*z[14];
    z[44]=z[79]*z[40];
    z[27]=z[44] - z[98] + z[27];
    z[27]=z[14]*z[27];
    z[44]= - static_cast<T>(5)+ z[121];
    z[37]=z[44]*z[37];
    z[44]=static_cast<T>(3)- z[97];
    z[25]=z[44]*z[25];
    z[25]=z[27] + z[25] - z[3] + z[37];
    z[25]=z[14]*z[25];
    z[27]=z[9]*z[31];
    z[31]=n<T>(152,9)*z[35];
    z[37]=z[6] + n<T>(1,2)*z[13];
    z[44]=z[3]*z[37];
    z[25]=z[25] + z[27] + z[31] + z[32] - n<T>(5,2) + z[44];
    z[25]=z[14]*z[25];
    z[27]= - z[37]*z[97];
    z[37]= - n<T>(5,2)*z[69] + z[101];
    z[37]=z[9]*z[37];
    z[25]=z[25] + z[37] + z[27] + z[13] - n<T>(581,18)*z[6];
    z[25]=z[14]*z[25];
    z[27]= - z[81]*z[30];
    z[30]=z[3]*z[78];
    z[30]= - 3*z[72] + z[30];
    z[30]=z[30]*z[53];
    z[37]=n<T>(295,18)*z[13] + z[6];
    z[37]=z[6]*z[37];
    z[25]=z[25] + z[30] + z[37] + z[27];
    z[25]=z[7]*z[25];
    z[27]=z[6]*z[8];
    z[30]=5*z[27];
    z[37]=static_cast<T>(9)- z[30];
    z[37]=z[37]*z[72];
    z[44]=z[3]*z[8]*npow(z[6],5);
    z[37]=z[37] + z[44];
    z[44]= - z[78]*z[96];
    z[37]=n<T>(1,2)*z[37] + z[44];
    z[37]=z[37]*z[53];
    z[44]=n<T>(5,2)*z[27];
    z[46]=static_cast<T>(3)+ z[44];
    z[46]=z[46]*z[69];
    z[47]=z[27] + n<T>(3,2);
    z[55]= - z[47]*z[101];
    z[37]=z[37] + n<T>(3,2)*z[46] + z[55];
    z[37]=z[9]*z[37];
    z[46]=z[13]*z[59];
    z[46]= - z[54] + n<T>(3,4)*z[29] + n<T>(1339,36) + z[46];
    z[46]=z[6]*z[46];
    z[41]=z[66]*z[41];
    z[54]=n<T>(3,2)*z[27];
    z[55]=z[54] + static_cast<T>(1)- z[29];
    z[55]=z[6]*z[55];
    z[41]=z[55] - 5*z[13] + z[41];
    z[41]=z[6]*z[41];
    z[41]=n<T>(1,6)*z[66] + z[41];
    z[55]= - z[66]*z[97];
    z[41]=n<T>(1,2)*z[41] + z[55];
    z[41]=z[3]*z[41];
    z[55]= - n<T>(707,18) - z[130];
    z[55]=z[13]*z[55];
    z[20]=z[25] + z[20] + z[37] + z[42] - 6*z[67] + z[41] + z[55] + 
    z[46];
    z[20]=z[7]*z[20];
    z[25]= - z[133] + n<T>(346,9)*z[2] + z[73];
    z[25]=z[11]*z[25];
    z[37]= - static_cast<T>(1)+ z[38];
    z[37]=z[37]*z[53];
    z[37]= - z[2] + z[37];
    z[37]=z[9]*z[37];
    z[22]= - z[14]*z[22];
    z[22]=z[22] + z[25] + z[37];
    z[22]=z[14]*z[22];
    z[25]= - z[33] + 3*z[71];
    z[25]=z[9]*z[25];
    z[25]= - static_cast<T>(2)+ z[25];
    z[25]=z[25]*z[96];
    z[33]=3*z[17] + n<T>(1,3)*z[2];
    z[37]=n<T>(2539,18) + z[51];
    z[37]=z[11]*z[37];
    z[22]=z[22] + z[25] + z[37] + 4*z[33] - n<T>(169,6)*z[5];
    z[22]=z[14]*z[22];
    z[25]= - z[105] - z[110] - z[28];
    z[25]=z[25]*z[34];
    z[28]=5*z[6];
    z[33]= - z[47]*z[28];
    z[34]= - static_cast<T>(3)+ z[44];
    z[34]=z[34]*z[69];
    z[34]=z[34] + n<T>(9,2)*z[102];
    z[34]=z[9]*z[34];
    z[33]=z[33] + z[34];
    z[33]=z[9]*z[33];
    z[34]=z[39] - n<T>(76,9)*z[35] + n<T>(152,9) - z[32];
    z[37]= - n<T>(617,9)*z[11] + z[9];
    z[37]=n<T>(1,2)*z[37] + z[40];
    z[37]=z[14]*z[37];
    z[34]=2*z[34] + z[37];
    z[34]=z[14]*z[34];
    z[34]=z[34] + z[36] + n<T>(295,18)*z[6] + z[67];
    z[34]=z[7]*z[34];
    z[36]= - z[15] - 8*z[12];
    z[36]=z[13]*z[36];
    z[36]=static_cast<T>(29)+ z[36];
    z[36]=z[36]*z[32];
    z[37]=n<T>(301,9)*z[6] + z[42];
    z[37]=z[11]*z[37];
    z[22]=z[34] + z[22] + z[33] + z[37] + z[36] + z[54] - n<T>(1711,36) + 
    z[25];
    z[22]=z[7]*z[22];
    z[25]=n<T>(1,2) - 3*z[39];
    z[25]=z[25]*z[96];
    z[25]=n<T>(5,2)*z[2] + z[25];
    z[25]=z[9]*z[25];
    z[33]=z[2] - z[5];
    z[21]=z[33]*z[21];
    z[21]=z[21] - n<T>(9,2)*z[26];
    z[21]=z[14]*z[21];
    z[26]=z[11] + n<T>(1667,18)*z[2];
    z[33]=6*z[5] - z[26];
    z[33]=z[11]*z[33];
    z[34]= - 4*z[15] + n<T>(661,12)*z[2];
    z[34]=z[4]*z[34];
    z[21]=z[21] + z[25] + z[33] + z[34];
    z[21]=z[14]*z[21];
    z[25]=z[14]*z[122];
    z[25]=n<T>(313,18)*z[25] - z[53] + n<T>(617,18)*z[11] + z[5] - z[91] - z[24]
   ;
    z[25]=z[14]*z[25];
    z[33]= - z[12]*z[67];
    z[25]=z[25] - n<T>(1,2)*z[39] + z[33] + z[31];
    z[25]=z[7]*z[25];
    z[24]=z[24] + z[110] - z[111];
    z[31]=z[13]*z[90];
    z[33]= - z[15] - 11*z[12];
    z[31]=2*z[33] + z[31];
    z[31]=z[13]*z[31];
    z[31]= - static_cast<T>(5)+ z[31];
    z[31]=z[5]*z[31];
    z[33]= - static_cast<T>(1)- z[44];
    z[33]=z[6]*z[33];
    z[33]=z[33] - n<T>(9,2)*z[71];
    z[33]=z[9]*z[33];
    z[30]=static_cast<T>(9)+ z[30];
    z[30]=n<T>(1,2)*z[30] + z[33];
    z[30]=z[9]*z[30];
    z[33]= - n<T>(2269,18) - z[51];
    z[33]=z[11]*z[33];
    z[34]=n<T>(629,18)*z[6] + z[42];
    z[34]=z[11]*z[34];
    z[34]=n<T>(1499,18) + z[34];
    z[34]=z[4]*z[34];
    z[21]=z[25] + z[21] + z[30] + z[34] + z[33] + 2*z[24] + z[31];
    z[21]=z[7]*z[21];
    z[24]= - z[52]*z[92];
    z[25]=z[92] + z[9];
    z[25]=z[25]*z[43];
    z[30]=z[63]*z[76];
    z[24]=z[30] + z[24] + z[25];
    z[25]=3*z[14];
    z[24]=z[24]*z[25];
    z[25]= - n<T>(5,2) - z[35];
    z[25]=z[25]*z[56];
    z[30]= - z[111] - n<T>(401,9)*z[2];
    z[31]= - n<T>(2287,18) - z[51];
    z[31]=z[11]*z[31];
    z[25]=z[25] + 2*z[30] + z[31];
    z[25]=z[4]*z[25];
    z[27]=n<T>(3,2)*z[39] + static_cast<T>(2)+ n<T>(5,4)*z[27];
    z[27]=z[27]*z[43];
    z[30]= - z[122] + z[58];
    z[30]=z[14]*z[30];
    z[31]=n<T>(617,2) + 152*z[35];
    z[31]=z[4]*z[31];
    z[30]=n<T>(313,2)*z[30] - n<T>(617,2)*z[11] + z[31];
    z[30]=z[7]*z[30];
    z[31]=n<T>(829,9)*z[2] + z[11];
    z[31]=z[11]*z[31];
    z[24]=n<T>(1,9)*z[30] + z[24] + z[27] + z[31] + z[25];
    z[24]=z[7]*z[24];
    z[25]=z[11]*z[26];
    z[26]= - z[2]*z[56];
    z[25]=z[26] - z[82] + z[25];
    z[25]=z[4]*z[25];
    z[26]=z[49] - z[50];
    z[26]=z[9]*z[26];
    z[27]= - 313*z[2] - 617*z[11];
    z[27]=z[4]*z[27];
    z[27]=313*z[122] + z[27];
    z[27]=z[7]*z[27];
    z[25]=n<T>(1,18)*z[27] + z[25] + n<T>(1,4)*z[26];
    z[25]=z[7]*z[25];
    z[26]=z[7]*z[4];
    z[26]=z[52] + n<T>(313,9)*z[26];
    z[26]=z[83]*z[122]*z[26];
    z[25]=z[25] + n<T>(1,2)*z[26];
    z[25]=z[10]*z[25];
    z[24]=z[24] + z[25];
    z[24]=z[10]*z[24];
    z[21]=z[21] + z[24];
    z[21]=z[10]*z[21];
    z[21]=z[21] + z[22] - n<T>(889,36)*z[9] + n<T>(2519,36)*z[4] - n<T>(1450,9)*
    z[11] + z[61] + n<T>(527,36)*z[2] + n<T>(11,4)*z[8] + n<T>(55,2)*z[12] + 19*
    z[17] + 6*z[15];
    z[21]=z[10]*z[21];
    z[22]=n<T>(257,18)*z[9] + n<T>(641,9)*z[11] - n<T>(93,4)*z[5] - n<T>(17,3)*z[3] - 
   n<T>(202,9)*z[2] + z[48] - z[45] + z[59] + 21*z[17];
    z[22]=z[14]*z[22];
    z[24]= - z[59] - z[17];
    z[24]= - 17*z[12] + 2*z[24] + z[128];
    z[24]=z[13]*z[24];
    z[25]=n<T>(47,3)*z[13] + z[28];
    z[25]=z[25]*z[99];
    z[26]=z[59] - z[8];
    z[26]=z[6]*z[26];

    r +=  - n<T>(2525,36) + z[20] + z[21] + z[22] + z[23] + z[24] + z[25]
       + z[26] + n<T>(21,4)*z[29] + n<T>(97,4)*z[32] + n<T>(187,18)*z[35] + n<T>(125,9)
      *z[39] + n<T>(427,18)*z[106];
 
    return r;
}

template double qg_2lNLC_r154(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r154(const std::array<dd_real,31>&);
#endif
