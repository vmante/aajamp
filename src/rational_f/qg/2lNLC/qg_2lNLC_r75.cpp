#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r75(const std::array<T,31>& k) {
  T z[61];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[4];
    z[4]=k[6];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[14];
    z[9]=k[8];
    z[10]=k[7];
    z[11]=k[12];
    z[12]=k[13];
    z[13]=k[3];
    z[14]=k[9];
    z[15]=n<T>(3,2)*z[7];
    z[16]=z[2]*z[8];
    z[17]=npow(z[4],3);
    z[18]=z[17]*z[16]*z[15];
    z[19]=npow(z[2],2);
    z[20]=z[19]*z[17];
    z[21]=n<T>(3,2)*z[20];
    z[22]=z[8]*z[21];
    z[23]=z[11]*z[14];
    z[18]=z[18] + z[22] - z[23];
    z[18]=z[7]*z[18];
    z[22]=z[3]*z[10];
    z[24]=z[22]*z[17];
    z[25]=z[7]*z[10];
    z[26]= - z[17]*z[25];
    z[26]=z[26] - z[24];
    z[27]=npow(z[3],2);
    z[28]=n<T>(1,2)*z[27];
    z[26]=z[26]*z[28];
    z[29]=z[12] - 9*z[11];
    z[30]= - n<T>(3,2)*z[12] + z[11];
    z[30]=z[7]*z[30];
    z[30]= - static_cast<T>(1)+ z[30];
    z[30]=z[9]*z[30];
    z[18]=z[26] + z[30] + n<T>(1,2)*z[29] + z[18];
    z[26]=n<T>(1,2)*z[10];
    z[29]=z[26] - z[11];
    z[30]=npow(z[3],3);
    z[31]=n<T>(1,2)*z[5];
    z[32]=z[29]*z[30]*z[31];
    z[33]=n<T>(3,4)*z[9];
    z[34]=z[33]*z[19];
    z[35]=2*z[11];
    z[36]= - z[35] + n<T>(5,4)*z[10];
    z[36]=z[36]*z[27];
    z[32]=z[32] + z[34] + z[36];
    z[32]=z[5]*z[32];
    z[36]=z[3] + z[2];
    z[37]=n<T>(3,4)*z[3];
    z[37]= - z[37]*z[17]*z[36];
    z[38]=n<T>(1,2)*z[20] - z[11];
    z[37]=z[37] + 3*z[38] + z[10];
    z[37]=z[3]*z[37];
    z[38]=z[33]*z[2];
    z[32]=z[32] - z[38] + z[37];
    z[32]=z[5]*z[32];
    z[20]=n<T>(3,4)*z[20];
    z[37]= - z[12] + z[20];
    z[37]=z[7]*z[37];
    z[20]= - z[3]*z[20];
    z[39]=z[19]*z[5];
    z[40]= - z[2] - z[39];
    z[40]=z[5]*z[40];
    z[20]=n<T>(3,2)*z[40] + z[20] - n<T>(3,2) + z[37];
    z[37]=n<T>(1,2)*z[6];
    z[20]=z[20]*z[37];
    z[18]=z[20] + n<T>(1,2)*z[18] + z[32];
    z[18]=z[6]*z[18];
    z[20]=n<T>(1,2)*z[3];
    z[32]=z[20] - z[2];
    z[40]=3*z[3];
    z[41]= - z[40]*z[17]*z[32];
    z[21]=z[41] - z[21] - z[10];
    z[21]=z[3]*z[21];
    z[41]=z[27]*z[5];
    z[42]=z[41]*z[10];
    z[42]=n<T>(1,4)*z[42];
    z[21]=z[21] - z[42];
    z[21]=z[5]*z[21];
    z[43]=z[17]*z[10];
    z[44]=z[30]*z[43];
    z[21]= - n<T>(3,8)*z[44] + z[21];
    z[21]=z[5]*z[21];
    z[44]=z[7]*z[11];
    z[45]= - n<T>(1,2) - z[44];
    z[45]=z[9]*z[45];
    z[45]=n<T>(3,4)*z[11] + z[45];
    z[45]=z[9]*z[45];
    z[18]=z[18] + z[45] + z[21];
    z[18]=z[6]*z[18];
    z[21]=npow(z[4],4);
    z[45]=z[21]*z[19];
    z[46]=n<T>(1,2)*z[45];
    z[32]=z[32]*z[3];
    z[47]= - z[21]*z[32];
    z[47]= - z[46] + z[47];
    z[48]=npow(z[5],2);
    z[47]=z[47]*z[27]*z[48];
    z[49]= - z[21]*z[27];
    z[45]=z[45] + z[49];
    z[45]=z[45]*z[28];
    z[49]=z[9]*z[2];
    z[39]=z[9]*z[39];
    z[39]=z[39] + z[49] + z[45];
    z[39]=z[5]*z[39];
    z[45]=npow(z[7],2);
    z[46]= - z[8]*z[45]*z[46];
    z[50]=z[7]*z[12];
    z[50]=static_cast<T>(1)+ z[50];
    z[50]=z[9]*z[50];
    z[39]=z[39] + z[46] + z[50];
    z[39]=z[6]*z[39];
    z[46]=z[9]*z[11];
    z[39]=3*z[39] + z[46] + 15*z[47];
    z[39]=z[39]*z[37];
    z[47]=npow(z[9],2);
    z[50]= - z[11]*z[47];
    z[39]=z[50] + z[39];
    z[39]=z[6]*z[39];
    z[32]= - n<T>(1,2)*z[19] - z[32];
    z[32]=z[1]*z[30]*z[48]*npow(z[6],3)*z[32]*npow(z[4],5);
    z[48]=npow(z[11],2);
    z[50]=z[10]*z[48];
    z[51]=z[10]*z[11];
    z[52]= - z[9]*z[51];
    z[50]=n<T>(3,2)*z[50] + z[52];
    z[21]=z[9]*z[21]*z[50]*npow(z[7],4);
    z[21]=n<T>(3,2)*z[32] + z[21] + z[39];
    z[21]=z[1]*z[21];
    z[32]=z[10]*z[13];
    z[39]=n<T>(1,4)*z[32] - z[25];
    z[50]=z[48]*z[17];
    z[39]=z[45]*z[50]*z[39];
    z[52]=3*z[48];
    z[53]= - z[52] + n<T>(25,2)*z[51];
    z[53]=z[45]*z[17]*z[53];
    z[54]=z[10]*z[52];
    z[53]=z[54] + z[53];
    z[53]=z[7]*z[53];
    z[54]= - z[48] + n<T>(3,2)*z[51];
    z[53]=3*z[54] + z[53];
    z[17]=z[11]*z[17];
    z[17]=z[17] - z[43];
    z[17]=z[17]*npow(z[7],3);
    z[17]=z[11] + z[17];
    z[17]=z[9]*z[17];
    z[17]=n<T>(1,2)*z[53] + z[17];
    z[53]=n<T>(1,2)*z[9];
    z[17]=z[17]*z[53];
    z[15]= - z[47]*z[24]*z[15];
    z[43]=z[9]*z[43];
    z[43]=n<T>(3,2)*z[50] + z[43];
    z[43]=z[9]*z[45]*z[43];
    z[15]=z[43] + z[15];
    z[15]=z[15]*z[20];
    z[43]=n<T>(3,8)*z[13];
    z[24]= - z[24]*z[43];
    z[45]=z[10]*z[8];
    z[24]=z[24] + z[45] - n<T>(3,8)*z[46];
    z[24]=z[3]*z[24];
    z[42]=z[8]*z[42];
    z[24]=z[42] - n<T>(3,8)*z[9] + z[24];
    z[24]=z[5]*z[24];
    z[42]=z[3]*z[9]*z[48];
    z[42]= - n<T>(1,2)*z[46] + z[42];
    z[24]=n<T>(3,4)*z[42] + z[24];
    z[24]=z[5]*z[24];
    z[15]=n<T>(1,2)*z[21] + z[18] + z[24] + z[15] + z[39] + z[17];
    z[15]=z[1]*z[15];
    z[17]=npow(z[4],2);
    z[18]=z[17]*z[3];
    z[21]=z[17]*z[7];
    z[24]=z[26] - z[12];
    z[39]=z[17]*z[2];
    z[42]=z[18] + n<T>(1,2)*z[21] + z[39] - z[24];
    z[42]=z[42]*z[20];
    z[45]=n<T>(3,2) + 5*z[22];
    z[46]= - z[45]*z[27];
    z[30]=z[30]*z[5];
    z[47]= - static_cast<T>(1)- z[22];
    z[47]=z[47]*z[30];
    z[46]=z[47] - 3*z[19] + z[46];
    z[46]=z[46]*z[31];
    z[47]=z[27]*z[10];
    z[46]= - 2*z[47] + z[46];
    z[46]=z[5]*z[46];
    z[50]=z[19]*z[17];
    z[54]=n<T>(1,4)*z[7];
    z[55]= - z[10] - 3*z[39] - z[11];
    z[55]=z[55]*z[54];
    z[56]=z[44] + 1;
    z[56]=z[56]*z[7];
    z[57]=z[56] + z[2];
    z[58]=n<T>(1,4)*z[9];
    z[59]=z[57]*z[58];
    z[28]= - z[19] + z[28];
    z[28]=z[5]*z[28]*z[40];
    z[60]= - 3*z[2] + n<T>(11,2)*z[3];
    z[60]=z[3]*z[60];
    z[28]=z[60] + z[28];
    z[28]=z[28]*z[31];
    z[28]=z[3] + z[28];
    z[28]=z[28]*z[37];
    z[28]=z[28] + z[46] + z[42] + z[59] + z[55] - static_cast<T>(1)- n<T>(9,8)*z[50];
    z[28]=z[6]*z[28];
    z[37]=n<T>(15,8)*z[8];
    z[42]=z[21]*z[37];
    z[46]=z[8]*z[39];
    z[23]=z[42] + 4*z[46] + z[23];
    z[23]=z[7]*z[23];
    z[42]= - 3*z[25] - z[22];
    z[46]=n<T>(1,4)*z[3];
    z[42]=z[46]*z[17]*z[42];
    z[55]= - z[3]*z[11];
    z[55]= - n<T>(3,2) + z[55];
    z[55]=z[55]*z[27]*z[31];
    z[59]=z[3]*z[29];
    z[59]= - n<T>(3,4) + z[59];
    z[59]=z[3]*z[59];
    z[34]=z[55] + z[34] + z[59];
    z[34]=z[5]*z[34];
    z[55]=z[39] - z[10];
    z[55]=5*z[55] - n<T>(17,4)*z[18];
    z[55]=z[3]*z[55];
    z[49]=static_cast<T>(11)- 3*z[49];
    z[49]=n<T>(1,2)*z[49] + z[55];
    z[34]=n<T>(1,2)*z[49] + z[34];
    z[34]=z[5]*z[34];
    z[49]=n<T>(9,2)*z[50] - 3;
    z[49]=z[8]*z[49];
    z[49]=7*z[14] + z[49];
    z[50]= - z[9]*z[57];
    z[44]=z[50] + static_cast<T>(1)+ n<T>(5,2)*z[44];
    z[44]=z[44]*z[53];
    z[23]=z[28] + z[34] + z[42] + z[44] + z[23] + n<T>(1,4)*z[49] + z[11];
    z[23]=z[6]*z[23];
    z[28]= - z[39]*z[43];
    z[34]=z[2]*z[14];
    z[39]= - n<T>(1,2) + z[34];
    z[38]=z[39]*z[38];
    z[39]=z[11]*z[12];
    z[26]= - z[8]*z[26];
    z[26]= - z[39] + z[26];
    z[26]=z[3]*z[26];
    z[42]=z[17]*z[13];
    z[26]=z[26] + n<T>(3,4)*z[42] + n<T>(5,4)*z[8] - z[12];
    z[26]=z[3]*z[26];
    z[43]=z[12]*z[13];
    z[44]= - z[3]*z[39];
    z[44]= - z[12] + z[44];
    z[44]=z[3]*z[44];
    z[44]=z[44] - static_cast<T>(1)+ z[43];
    z[44]=z[3]*z[44];
    z[49]=npow(z[13],2);
    z[50]=z[49]*z[12];
    z[55]=z[50] - z[13];
    z[57]=z[2] + z[55];
    z[44]=n<T>(1,2)*z[57] + z[44];
    z[44]=z[44]*z[31];
    z[57]=n<T>(1,2)*z[43];
    z[26]=z[44] + z[26] + z[38] + z[28] + static_cast<T>(1)- z[57];
    z[26]=z[5]*z[26];
    z[28]=z[17]*z[22];
    z[38]=5*z[8] - n<T>(9,4)*z[42];
    z[38]=z[10]*z[38];
    z[33]=z[11]*z[33];
    z[28]= - n<T>(3,2)*z[28] + z[33] - z[39] + z[38];
    z[28]=z[3]*z[28];
    z[33]=3*z[9];
    z[38]=n<T>(5,4) - z[34];
    z[38]=z[38]*z[33];
    z[44]=3*z[10];
    z[59]=z[11] + z[8];
    z[59]=z[12] - 3*z[59];
    z[28]=z[28] + z[38] + n<T>(1,2)*z[59] + z[44];
    z[26]=n<T>(1,2)*z[28] + z[26];
    z[26]=z[5]*z[26];
    z[28]=2*z[12];
    z[38]=z[28] - z[11];
    z[42]=n<T>(11,8)*z[42] + z[38];
    z[42]=z[42]*z[51];
    z[59]=z[48] - n<T>(15,4)*z[51];
    z[59]=z[59]*z[21];
    z[42]=z[42] + z[59];
    z[42]=z[7]*z[42];
    z[59]=9*z[48];
    z[60]=n<T>(19,2)*z[17] + z[59];
    z[60]=z[10]*z[60];
    z[17]=z[11]*z[17];
    z[17]= - n<T>(21,2)*z[17] + z[60];
    z[17]=z[7]*z[17];
    z[17]=z[17] - z[59] + n<T>(41,2)*z[51];
    z[17]=z[7]*z[17];
    z[59]=n<T>(7,2)*z[10] + z[14] - n<T>(9,2)*z[11];
    z[17]=3*z[59] + z[17];
    z[59]=z[25]*z[11];
    z[60]= - 3*z[29] - z[59];
    z[60]=z[7]*z[60];
    z[60]=n<T>(3,2) + z[60];
    z[60]=z[9]*z[60];
    z[17]=n<T>(1,2)*z[17] + z[60];
    z[17]=z[17]*z[53];
    z[53]=z[58] - n<T>(7,8)*z[10] + z[12] + n<T>(3,8)*z[11];
    z[21]=z[21]*z[53];
    z[53]= - z[12] + n<T>(1,8)*z[10];
    z[18]=z[53]*z[18];
    z[18]=z[18] + n<T>(3,4)*z[48] + z[21];
    z[18]=z[40]*z[9]*z[18];
    z[21]= - z[11]*z[38];
    z[38]=n<T>(1,4)*z[11];
    z[40]=3*z[12] - z[38];
    z[40]=z[10]*z[40];
    z[15]=z[15] + z[23] + z[26] + z[18] + z[17] + z[42] + z[21] + z[40];
    z[15]=z[1]*z[15];
    z[17]=z[3]*z[45];
    z[17]=n<T>(9,2)*z[2] + z[17];
    z[17]=z[3]*z[17];
    z[17]= - 9*z[19] + z[17];
    z[17]=z[3]*z[17];
    z[18]= - z[2] + z[47];
    z[18]=z[18]*z[30];
    z[17]=z[17] + z[18];
    z[17]=z[5]*z[17];
    z[18]= - n<T>(1,4) + z[22];
    z[18]=z[3]*z[18];
    z[18]=n<T>(23,8)*z[2] + z[18];
    z[18]=z[3]*z[18];
    z[17]=z[18] + n<T>(1,4)*z[17];
    z[17]=z[5]*z[17];
    z[18]=z[2] - z[46];
    z[18]=z[18]*z[27];
    z[21]=z[3]*z[2];
    z[21]= - z[19] + z[21];
    z[21]=z[21]*z[41];
    z[18]=z[18] + n<T>(3,4)*z[21];
    z[18]=z[5]*z[18];
    z[18]= - n<T>(1,4)*z[27] + z[18];
    z[18]=z[6]*z[18];
    z[21]=z[25] + z[22];
    z[21]=z[3]*z[21];
    z[23]= - static_cast<T>(1)+ n<T>(15,2)*z[16];
    z[23]=z[7]*z[23];
    z[23]= - n<T>(17,2)*z[2] + z[23];
    z[21]=n<T>(1,2)*z[23] + z[21];
    z[17]=z[18] + n<T>(1,2)*z[21] + z[17];
    z[17]=z[6]*z[17];
    z[18]= - static_cast<T>(1)+ z[22];
    z[18]=z[18]*z[20];
    z[18]= - z[2] + z[18];
    z[18]=z[18]*z[41];
    z[21]= - static_cast<T>(3)+ z[22];
    z[21]=z[3]*z[21];
    z[21]=5*z[2] + z[21];
    z[21]=z[3]*z[21];
    z[18]=z[18] + z[21] - n<T>(9,2)*z[19];
    z[18]=z[5]*z[18];
    z[19]=n<T>(3,4)*z[2];
    z[21]= - static_cast<T>(3)+ n<T>(23,4)*z[22];
    z[21]=z[3]*z[21];
    z[18]=z[18] + z[19] + z[21];
    z[18]=z[18]*z[31];
    z[21]=z[12] + z[29];
    z[20]=z[21]*z[20];
    z[21]= - static_cast<T>(15)+ 7*z[16];
    z[23]=n<T>(3,4)*z[10] + n<T>(9,4)*z[8] - z[35];
    z[23]=z[7]*z[23];
    z[26]=z[2] + n<T>(1,2)*z[56];
    z[26]=z[9]*z[26];
    z[17]=z[17] + z[18] + z[20] + z[26] + n<T>(1,4)*z[21] + z[23];
    z[17]=z[6]*z[17];
    z[18]= - 3*z[55] - z[36];
    z[18]=z[3]*z[18];
    z[20]= - z[12]*npow(z[13],3);
    z[21]= - z[2]*z[13];
    z[18]=z[18] + z[21] + z[49] + z[20];
    z[18]=z[5]*z[18];
    z[20]=n<T>(3,2)*z[32] - static_cast<T>(13)+ 9*z[43];
    z[20]=z[3]*z[20];
    z[21]=static_cast<T>(11)- 3*z[34];
    z[21]=z[2]*z[21];
    z[18]=z[18] + z[20] + z[21] - n<T>(7,2)*z[13] + 3*z[50];
    z[18]=z[18]*z[31];
    z[20]= - z[44] + 11*z[8] - 9*z[12];
    z[21]=z[8]*z[22];
    z[20]= - n<T>(15,4)*z[21] + n<T>(1,2)*z[20] - z[33];
    z[20]=z[3]*z[20];
    z[21]= - static_cast<T>(1)- z[57];
    z[22]= - z[14] - n<T>(1,4)*z[8];
    z[22]=z[2]*z[22];
    z[18]=z[18] + z[20] - n<T>(3,4)*z[32] + 3*z[21] + z[22];
    z[18]=z[18]*z[31];
    z[20]=z[12] - z[11];
    z[20]=z[20]*z[25]*z[35];
    z[21]=z[35] - n<T>(1,2)*z[14] - z[28];
    z[21]=z[11]*z[21];
    z[22]=z[38]*z[13];
    z[23]= - static_cast<T>(5)+ z[22];
    z[23]=z[11]*z[23];
    z[23]=z[28] + z[23];
    z[23]=z[10]*z[23];
    z[20]=z[20] + z[21] + z[23];
    z[20]=z[7]*z[20];
    z[21]=z[25] - 1;
    z[21]=z[52]*z[21];
    z[21]=n<T>(19,2)*z[51] + z[21];
    z[21]=z[7]*z[21];
    z[23]= - 45*z[11] + 47*z[10];
    z[21]=n<T>(1,2)*z[23] + 3*z[21];
    z[21]=z[21]*z[54];
    z[23]= - z[59] + n<T>(3,2)*z[11] - z[10];
    z[23]=z[7]*z[23];
    z[23]=n<T>(1,4) + z[23];
    z[23]=z[7]*z[23];
    z[19]=z[19] + z[23];
    z[19]=z[9]*z[19];
    z[19]=z[19] - static_cast<T>(2)+ z[21];
    z[19]=z[9]*z[19];
    z[21]=n<T>(1,2)*z[11] - z[24];
    z[23]=z[7]*z[52];
    z[21]=5*z[21] + z[23];
    z[23]= - n<T>(1,2) + z[25];
    z[23]=z[9]*z[23];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[23]=n<T>(3,2)*z[9];
    z[21]=z[21]*z[23];
    z[24]= - z[12]*z[44];
    z[21]=z[21] - n<T>(1,2)*z[39] + z[24];
    z[21]=z[3]*z[21];
    z[24]=z[12] + z[8];
    z[22]= - static_cast<T>(7)+ z[22];
    z[22]=z[10]*z[22];
    z[22]=z[22] + n<T>(7,2)*z[11] - z[14] + n<T>(9,2)*z[24];
    z[15]=z[15] + z[17] + z[18] + z[21] + z[19] + n<T>(1,2)*z[22] + z[20];
    z[15]=z[1]*z[15];
    z[17]= - z[2] - n<T>(1,2)*z[7];
    z[17]=z[17]*z[23];
    z[18]=z[33] + n<T>(7,4)*z[10] - z[37] - 4*z[12];
    z[18]=z[3]*z[18];
    z[19]= - n<T>(11,4)*z[10] + n<T>(5,2)*z[11] + z[37] + z[12];
    z[19]=z[7]*z[19];
    z[20]=n<T>(3,2)*z[13] + z[3];
    z[20]=z[5]*z[20];
    z[21]=n<T>(23,8)*z[3] - 4*z[2] - n<T>(17,8)*z[7];
    z[21]=z[6]*z[21];

    r += n<T>(1,4) + z[15] + n<T>(13,4)*z[16] + z[17] + z[18] + z[19] + n<T>(3,4)*
      z[20] + z[21] + n<T>(9,8)*z[32] - z[43];
 
    return r;
}

template double qg_2lNLC_r75(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r75(const std::array<dd_real,31>&);
#endif
