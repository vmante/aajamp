#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1119(const std::array<T,31>& k) {
  T z[149];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[14];
    z[5]=k[19];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[11];
    z[9]=k[2];
    z[10]=k[3];
    z[11]=k[6];
    z[12]=k[9];
    z[13]=k[8];
    z[14]=k[5];
    z[15]=k[15];
    z[16]=k[4];
    z[17]=k[26];
    z[18]=k[29];
    z[19]=npow(z[1],2);
    z[20]=n<T>(1,2)*z[19];
    z[21]=z[14]*z[1];
    z[22]=z[20] + 7*z[21];
    z[22]=z[14]*z[22];
    z[23]=npow(z[1],3);
    z[22]= - n<T>(3,2)*z[23] + z[22];
    z[22]=z[14]*z[22];
    z[24]=npow(z[14],2);
    z[25]=3*z[24];
    z[26]=z[25]*z[1];
    z[26]=z[26] - z[23];
    z[27]=z[26]*z[24]*z[7];
    z[22]=z[22] + z[27];
    z[22]=z[7]*z[22];
    z[27]=z[19] + n<T>(13,2)*z[21];
    z[27]=z[14]*z[27];
    z[28]=n<T>(1,2)*z[23];
    z[22]=z[22] - z[28] + z[27];
    z[22]=z[7]*z[22];
    z[27]=npow(z[1],4);
    z[29]=z[27]*z[3];
    z[30]=z[29] + z[23];
    z[31]=n<T>(1,4)*z[3];
    z[32]= - z[30]*z[31];
    z[33]=2*z[19];
    z[34]=z[23]*z[12];
    z[35]=z[21]*z[12];
    z[36]=z[1] + z[35];
    z[36]=z[14]*z[36];
    z[22]=z[22] + z[32] + z[36] - z[33] + z[34];
    z[22]=z[8]*z[22];
    z[32]=npow(z[7],3);
    z[36]=2*z[32];
    z[37]=npow(z[21],4);
    z[38]=z[36]*z[37];
    z[39]=5*z[37];
    z[40]=npow(z[7],2);
    z[39]=z[39]*z[40]*z[8];
    z[41]=z[11]*z[8];
    z[42]=z[41]*npow(z[21],5);
    z[36]=z[36]*z[42];
    z[36]=z[36] - z[38] + z[39];
    z[36]=z[11]*z[36];
    z[39]=3*z[23];
    z[43]=npow(z[14],3);
    z[44]=z[39]*z[43];
    z[45]=z[44]*z[40];
    z[46]=z[43]*z[23];
    z[47]=z[8]*z[7]*z[46];
    z[36]=z[36] - z[45] + 7*z[47];
    z[36]=z[11]*z[36];
    z[47]=z[25]*z[19];
    z[48]=z[8]*z[47];
    z[36]=z[48] + z[36];
    z[48]=npow(z[11],2);
    z[36]=z[36]*z[48];
    z[49]=2*z[27];
    z[50]= - z[32]*z[48]*z[43]*z[49];
    z[51]=z[19]*z[14];
    z[52]=z[51]*z[8];
    z[53]= - z[7]*z[51];
    z[50]=z[50] + z[53] + n<T>(3,2)*z[52];
    z[50]=z[50]*z[48];
    z[53]=z[24]*z[1];
    z[54]=z[40]*z[53];
    z[55]=n<T>(1,4)*z[1];
    z[54]= - z[55] + z[54];
    z[54]=z[7]*z[54];
    z[56]=z[23]*z[3];
    z[57]=z[19] + z[56];
    z[58]=n<T>(1,2)*z[3];
    z[57]=z[57]*z[58];
    z[59]=3*z[1];
    z[57]=z[59] + z[57];
    z[57]=z[8]*z[57];
    z[50]=z[50] + z[54] + n<T>(1,2)*z[57];
    z[50]=z[16]*z[50];
    z[54]=z[20] - z[21];
    z[57]=3*z[14];
    z[60]=z[54]*z[57];
    z[61]=z[21] - z[19];
    z[62]=z[61]*z[24];
    z[63]= - z[7]*z[62];
    z[60]=z[60] + z[63];
    z[60]=z[7]*z[60];
    z[60]=n<T>(3,4)*z[21] + z[60];
    z[60]=z[7]*z[60];
    z[63]=npow(z[12],2);
    z[64]=z[49]*z[63];
    z[64]=z[64] + z[20];
    z[64]=z[64]*z[12];
    z[65]=n<T>(1,2)*z[1];
    z[64]=z[64] + z[65];
    z[66]=npow(z[1],5);
    z[67]=z[66]*z[12];
    z[68]=n<T>(1,2)*z[27];
    z[69]=z[68] - z[67];
    z[69]=z[12]*z[69];
    z[69]=z[28] + z[69];
    z[69]=z[12]*z[69];
    z[69]=z[20] + z[69];
    z[69]=z[3]*z[69];
    z[69]=z[64] + z[69];
    z[69]=z[9]*z[69];
    z[70]=3*z[12];
    z[71]=z[66]*z[70];
    z[72]= - z[49] + z[71];
    z[72]=z[12]*z[72];
    z[72]= - z[28] + z[72];
    z[72]=z[12]*z[72];
    z[69]=z[69] - z[20] + z[72];
    z[69]=z[3]*z[12]*z[69];
    z[72]=z[70]*z[27];
    z[73]=z[23] - z[72];
    z[74]=2*z[12];
    z[73]=z[73]*z[74];
    z[73]= - z[20] + z[73];
    z[73]=z[12]*z[73];
    z[73]= - z[65] + z[73];
    z[73]=z[12]*z[73];
    z[69]=z[73] + z[69];
    z[69]=z[9]*z[69];
    z[73]=z[27]*z[12];
    z[75]=z[73] - z[23];
    z[76]=npow(z[12],3);
    z[77]=z[76]*z[14];
    z[78]=2*z[77];
    z[75]=z[75]*z[78];
    z[79]= - z[27] + z[67];
    z[79]=z[79]*z[76];
    z[79]=z[79] - z[75];
    z[79]=z[2]*z[14]*z[79];
    z[80]=n<T>(5,2)*z[27];
    z[81]=z[80] - z[71];
    z[81]=z[81]*z[76];
    z[82]=2*z[23];
    z[83]=z[72] - z[82];
    z[84]=z[83]*z[78];
    z[85]=npow(z[3],2);
    z[86]=z[23]*z[85];
    z[87]=z[21] + z[19];
    z[88]=z[9]*z[1];
    z[89]= - z[88] + z[87];
    z[90]=n<T>(1,2)*z[15];
    z[89]=z[89]*z[90];
    z[22]=z[50] + z[79] + z[89] + z[69] + z[36] + z[22] + z[60] + n<T>(1,8)*
    z[86] + z[84] + z[55] + z[81];
    z[22]=z[13]*z[22];
    z[36]=z[37]*z[41];
    z[50]=z[46] - z[36];
    z[55]=npow(z[11],3);
    z[50]=z[50]*z[55];
    z[60]=z[12]*z[1];
    z[69]=z[60]*z[25];
    z[79]=z[73]*z[24];
    z[81]=npow(z[11],4);
    z[84]=z[9]*z[81]*z[79];
    z[69]=z[69] + z[84];
    z[69]=z[9]*z[69];
    z[50]=z[50] + z[69];
    z[69]=5*z[1];
    z[84]=z[19]*z[12];
    z[86]=6*z[84];
    z[89]=z[69] + z[86];
    z[89]=z[14]*z[89];
    z[91]=3*z[19];
    z[89]=z[91] + z[89];
    z[89]=z[14]*z[89];
    z[92]=3*z[21];
    z[93]=z[33] - z[92];
    z[93]=z[14]*z[93];
    z[94]=4*z[23];
    z[93]=z[94] + z[93];
    z[93]=z[14]*z[93];
    z[95]=z[66]*z[3];
    z[93]=z[93] - z[95];
    z[93]=z[8]*z[93];
    z[50]=z[89] + z[93] + 2*z[50];
    z[50]=z[4]*z[50];
    z[89]= - z[24]*z[33];
    z[93]=z[41]*z[44];
    z[89]=z[89] + z[93];
    z[89]=z[89]*z[48];
    z[93]=5*z[23];
    z[96]=z[14]*z[12];
    z[97]= - z[96]*z[93];
    z[98]=z[63]*z[24];
    z[99]=z[98]*z[27];
    z[100]=z[11]*z[99];
    z[97]=z[97] + z[100];
    z[97]=z[9]*z[97];
    z[100]=z[34]*z[24];
    z[97]=z[97] - z[100];
    z[97]=z[55]*z[97];
    z[101]=z[63]*z[21];
    z[102]= - 3*z[60] + z[101];
    z[57]=z[102]*z[57];
    z[57]=z[57] + z[97];
    z[57]=z[9]*z[57];
    z[97]=3*z[84];
    z[102]=2*z[1];
    z[103]= - z[102] + z[97];
    z[103]=z[103]*z[96];
    z[104]= - z[1] - z[84];
    z[103]=3*z[104] + z[103];
    z[103]=z[14]*z[103];
    z[104]=n<T>(9,2)*z[19];
    z[105]= - z[104] + z[21];
    z[105]=z[14]*z[105];
    z[106]=n<T>(5,2)*z[29];
    z[105]=z[105] + z[106];
    z[105]=z[8]*z[105];
    z[50]=z[50] + z[57] + z[89] + z[105] + z[19] + z[103];
    z[50]=z[4]*z[50];
    z[57]=z[96]*z[19];
    z[89]=z[63]*z[23];
    z[103]=z[24]*z[11];
    z[105]=z[89]*z[103];
    z[105]=11*z[57] + z[105];
    z[107]=n<T>(1,3)*z[48];
    z[105]=z[105]*z[107];
    z[108]= - z[49]*z[103]*z[76];
    z[109]=z[89]*z[14];
    z[108]= - 7*z[109] + z[108];
    z[108]=z[11]*z[108];
    z[108]=13*z[84] + z[108];
    z[110]=n<T>(1,3)*z[9];
    z[108]=z[108]*z[48]*z[110];
    z[111]=z[76]*z[21];
    z[112]=z[1]*z[63];
    z[112]= - n<T>(10,3)*z[112] - z[111];
    z[112]=z[14]*z[112];
    z[112]=n<T>(10,3)*z[60] + z[112];
    z[105]=z[108] + 2*z[112] + z[105];
    z[105]=z[9]*z[105];
    z[108]=n<T>(1,3)*z[1];
    z[112]=z[108] - z[84];
    z[113]=2*z[14];
    z[112]=z[112]*z[63]*z[113];
    z[114]=n<T>(1,3)*z[12];
    z[115]=19*z[84];
    z[116]=8*z[1] - z[115];
    z[116]=z[116]*z[114];
    z[112]=z[112] + static_cast<T>(1)+ z[116];
    z[112]=z[14]*z[112];
    z[116]=z[84]*z[24];
    z[117]=z[24]*z[8];
    z[118]= - z[19]*z[117];
    z[118]= - n<T>(1,3)*z[116] + z[118];
    z[118]=z[118]*z[48];
    z[119]=z[82]*z[3];
    z[87]=n<T>(3,2)*z[87] - z[119];
    z[87]=z[8]*z[87];
    z[120]=n<T>(5,4)*z[1];
    z[121]=z[19]*z[3];
    z[50]=z[50] + z[105] + 2*z[118] + z[87] - n<T>(5,4)*z[121] + z[112] + 
    z[120] + n<T>(1,3)*z[84];
    z[50]=z[4]*z[50];
    z[87]=2*z[34];
    z[105]=z[87]*z[24];
    z[112]=z[14]*z[27];
    z[112]= - z[66] + z[112];
    z[118]=3*z[6];
    z[112]=z[112]*z[118];
    z[112]=z[105] + z[112];
    z[112]=z[7]*z[112];
    z[122]=z[34] - z[19];
    z[123]=z[122]*z[96];
    z[123]=z[123] + z[34];
    z[123]=z[123]*z[14];
    z[124]=z[82]*z[14];
    z[80]=z[80] - z[124];
    z[80]=z[80]*z[118];
    z[80]=z[112] + z[123] + z[80];
    z[80]=z[7]*z[80];
    z[112]=z[87] - z[19];
    z[112]=z[112]*z[12];
    z[125]=z[112] + z[102];
    z[126]=z[125]*z[96];
    z[127]= - z[84] + z[126];
    z[127]=z[14]*z[127];
    z[128]= - z[93] + n<T>(7,2)*z[51];
    z[129]=n<T>(3,2)*z[6];
    z[128]=z[128]*z[129];
    z[130]=n<T>(1,4)*z[19];
    z[80]=z[80] + z[128] + z[127] - z[130] + z[87];
    z[80]=z[7]*z[80];
    z[95]=z[105] + z[95];
    z[95]=z[4]*z[95];
    z[95]=z[95] - z[106] + z[23] + z[123];
    z[95]=z[4]*z[95];
    z[105]=z[87] + z[19];
    z[105]=z[105]*z[12];
    z[106]= - z[105] - n<T>(1,3)*z[126];
    z[106]=z[14]*z[106];
    z[123]=n<T>(5,2)*z[3];
    z[126]=z[23]*z[123];
    z[95]=z[95] + z[126] + z[106] + z[20] - z[87];
    z[95]=z[4]*z[95];
    z[106]=z[10]*z[61];
    z[106]=z[106] - z[23] - z[53];
    z[106]=z[6]*z[106];
    z[126]=z[34] - z[92];
    z[127]=z[65] - z[84];
    z[127]=z[127]*z[10];
    z[127]=z[127] + z[34];
    z[128]= - n<T>(11,4)*z[19] + z[127];
    z[128]=z[12]*z[128];
    z[131]=n<T>(9,8)*z[1];
    z[128]=z[131] + z[128];
    z[128]=z[10]*z[128];
    z[106]=n<T>(3,8)*z[106] + n<T>(7,8)*z[126] + z[128];
    z[106]=z[6]*z[106];
    z[105]=z[1] + z[105];
    z[105]=z[105]*z[96];
    z[125]=z[10]*z[12]*z[125];
    z[105]=z[105] + z[125];
    z[125]=z[130] - z[127];
    z[125]=z[114]*z[125];
    z[125]= - n<T>(1,8)*z[121] - n<T>(3,4)*z[1] + z[125];
    z[125]=z[10]*z[125];
    z[125]= - z[19] + n<T>(1,8)*z[34] + z[125];
    z[125]=z[3]*z[125];
    z[126]= - z[71] + 7*z[27];
    z[126]=z[126]*z[12];
    z[127]=z[94] - z[126];
    z[127]=z[127]*z[63];
    z[75]=z[127] - z[75];
    z[75]=z[14]*z[75];
    z[127]= - z[71] + 5*z[27];
    z[127]=z[127]*z[12];
    z[128]=z[82] - z[127];
    z[128]=z[12]*z[128];
    z[75]=z[128] + z[75];
    z[75]=z[15]*z[75];
    z[128]=11*z[23] - 7*z[73];
    z[128]=z[12]*z[128];
    z[130]=5*z[19];
    z[128]= - z[130] + z[128];
    z[128]=z[128]*z[114];
    z[75]=z[75] + z[95] + z[80] + z[125] + z[106] - z[120] + z[128] + n<T>(2,3)*z[105];
    z[75]=z[2]*z[75];
    z[80]= - z[27]*z[43]*z[41];
    z[95]=z[39]*z[24];
    z[105]=z[23]*z[10];
    z[106]= - z[14]*z[39];
    z[106]=z[106] + z[105];
    z[106]=z[10]*z[106];
    z[80]=z[80] + z[95] + z[106];
    z[32]=z[32]*z[80];
    z[80]= - z[23]*z[40]*z[117];
    z[32]=z[80] + z[32];
    z[32]=z[11]*z[32];
    z[80]=z[19]*z[10];
    z[106]=z[85]*z[80];
    z[52]= - z[7]*z[52];
    z[32]=2*z[32] - n<T>(5,12)*z[106] + z[52];
    z[32]=z[32]*z[48];
    z[52]=5*z[21];
    z[106]=z[91] + z[52];
    z[106]=z[14]*z[106];
    z[125]=z[81]*z[49];
    z[128]=z[43]*z[125];
    z[30]=z[128] + z[106] - z[30];
    z[30]=z[30]*z[4]*z[8];
    z[106]= - z[55]*z[82]*z[117];
    z[128]= - z[92] + z[56];
    z[128]=z[8]*z[128];
    z[30]=z[30] + z[128] + z[106];
    z[30]=z[4]*z[30];
    z[106]=z[51]*z[48];
    z[128]=z[1] - z[121];
    z[128]=n<T>(1,2)*z[128] + z[106];
    z[128]=z[8]*z[128];
    z[30]=z[30] + z[128];
    z[30]=z[4]*z[30];
    z[128]=z[27]*z[6];
    z[132]=z[128] + z[23];
    z[133]=z[14]*z[61];
    z[133]=z[133] + z[132];
    z[133]=z[7]*z[133];
    z[134]=z[23]*z[6];
    z[133]=z[133] - z[134] + z[61];
    z[133]=z[7]*z[133];
    z[135]=z[19]*z[6];
    z[136]=z[1] + z[135];
    z[133]=n<T>(1,2)*z[136] + z[133];
    z[133]=z[8]*z[133];
    z[136]=2*z[21];
    z[137]=z[10]*z[1];
    z[138]= - z[136] + z[137];
    z[40]=z[138]*z[40];
    z[40]=3*z[133] + static_cast<T>(1)+ z[40];
    z[40]=z[7]*z[40];
    z[133]=z[48]*z[19];
    z[138]=z[133]*z[9];
    z[139]=z[16]*z[133];
    z[139]=n<T>(5,12)*z[139] - n<T>(5,6)*z[138];
    z[139]=z[85]*z[139];
    z[140]= - z[19] - z[134];
    z[140]=z[140]*z[129];
    z[140]= - n<T>(1,3)*z[106] + z[140];
    z[140]=z[8]*z[140];
    z[140]= - n<T>(1,3) + z[140];
    z[90]=z[140]*z[90];
    z[140]=7*z[1];
    z[141]= - z[3]*z[137];
    z[141]= - z[140] + z[141];
    z[142]=n<T>(1,8)*z[3];
    z[141]=z[141]*z[142];
    z[143]=z[4]*z[29];
    z[119]= - z[119] + z[143];
    z[119]=z[4]*z[119];
    z[119]=n<T>(7,4)*z[121] + z[119];
    z[119]=z[4]*z[119];
    z[119]=z[141] + z[119];
    z[119]=z[2]*z[119];
    z[141]=n<T>(1,3)*z[3];
    z[143]=z[3]*z[1];
    z[144]=n<T>(1,4) - 5*z[143];
    z[144]=z[144]*z[141];
    z[30]=z[119] + z[90] + z[30] + z[32] + z[144] + z[139] + z[40];
    z[30]=z[16]*z[30];
    z[32]=z[76]*z[27];
    z[40]=z[32]*z[24];
    z[90]=z[77]*z[49];
    z[119]=z[10]*z[90];
    z[139]=z[128]*z[98];
    z[119]=n<T>(17,4)*z[139] - z[40] + z[119];
    z[139]=npow(z[10],2);
    z[119]=z[6]*z[139]*z[119];
    z[144]=z[63]*z[14];
    z[145]= - z[49]*z[144];
    z[146]=z[63]*z[10];
    z[147]=z[27]*z[146];
    z[145]=z[145] + z[147];
    z[145]=z[10]*z[145];
    z[49]= - z[96]*z[49];
    z[147]=z[10]*z[73];
    z[49]=z[49] + z[147];
    z[49]=z[10]*z[49];
    z[49]=z[79] + z[49];
    z[49]=z[7]*z[49];
    z[49]=2*z[49] + z[99] + z[145];
    z[49]=z[7]*z[49];
    z[79]=z[10]*z[32];
    z[79]= - z[90] + z[79];
    z[79]=z[10]*z[79];
    z[40]=z[40] + z[79];
    z[40]=2*z[40] + z[49];
    z[40]=z[7]*z[139]*z[40];
    z[49]=z[66]*z[76];
    z[79]=npow(z[6],2);
    z[90]=z[103]*z[79];
    z[99]=npow(z[10],3);
    z[103]=z[99]*z[90]*z[49];
    z[38]= - z[8]*z[38];
    z[38]=z[103] + z[38] + z[119] + z[40];
    z[38]=z[11]*z[38];
    z[40]= - z[96]*z[105];
    z[40]=z[100] + z[40];
    z[40]=z[10]*z[40];
    z[103]= - z[14]*z[93];
    z[82]=z[10]*z[82];
    z[82]=z[103] + z[82];
    z[82]=z[10]*z[82];
    z[82]=z[95] + z[82];
    z[95]=2*z[10];
    z[82]=z[7]*z[82]*z[95];
    z[40]=3*z[40] + z[82];
    z[40]=z[7]*z[40];
    z[82]=4*z[63];
    z[103]=z[105]*z[82];
    z[103]= - 9*z[109] + z[103];
    z[103]=z[10]*z[103];
    z[109]=z[93]*z[98];
    z[103]=z[109] + z[103];
    z[103]=z[10]*z[103];
    z[40]=z[103] + z[40];
    z[40]=z[7]*z[40];
    z[103]=z[77]*z[23];
    z[109]=z[105]*z[76];
    z[119]=z[103] - z[109];
    z[119]=z[119]*z[139];
    z[98]= - z[28]*z[98];
    z[145]=z[105]*z[144];
    z[98]=z[98] + z[145];
    z[100]=z[6]*z[100];
    z[98]=7*z[98] + n<T>(29,4)*z[100];
    z[98]=z[6]*z[10]*z[98];
    z[99]=z[99]*z[85];
    z[100]=n<T>(1,3)*z[34];
    z[145]=z[99]*z[100];
    z[45]= - z[8]*z[45];
    z[38]=z[38] + z[45] + z[40] + z[145] + z[119] + z[98];
    z[38]=z[11]*z[38];
    z[40]= - z[51] - z[80];
    z[40]=z[10]*z[40];
    z[40]=z[47] + z[40];
    z[40]=z[7]*z[40];
    z[45]=z[84]*z[10];
    z[47]= - 9*z[57] + 5*z[45];
    z[47]=z[10]*z[47];
    z[80]= - z[33]*z[117];
    z[40]=z[80] + z[40] + 7*z[116] + z[47];
    z[40]=z[7]*z[40];
    z[47]=z[10]*z[57];
    z[57]=z[24]*z[135];
    z[47]=n<T>(23,2)*z[57] - 11*z[116] + n<T>(37,2)*z[47];
    z[47]=z[6]*z[47];
    z[57]=n<T>(5,2)*z[19];
    z[80]=z[146]*z[57];
    z[98]=3*z[63];
    z[116]=z[51]*z[98];
    z[116]=z[116] - z[80];
    z[116]=z[10]*z[116];
    z[119]= - z[3]*z[20];
    z[119]= - z[84] + z[119];
    z[119]=z[141]*z[139]*z[119];
    z[47]=z[119] + z[116] + z[47];
    z[38]=z[38] + n<T>(1,2)*z[47] + z[40];
    z[38]=z[38]*z[48];
    z[40]= - z[63]*z[51];
    z[47]=z[10]*z[121];
    z[47]= - n<T>(1,2)*z[45] + 5*z[47];
    z[47]=z[47]*z[58];
    z[40]=z[47] + z[40] - z[80];
    z[47]=z[23]*z[76]*z[139];
    z[80]=z[99]*z[11];
    z[99]=z[27]*z[63]*z[80];
    z[116]=z[3]*z[34];
    z[89]= - z[89] + z[116];
    z[116]=z[139]*z[3];
    z[89]=z[89]*z[116];
    z[47]= - n<T>(2,3)*z[99] + z[47] + n<T>(1,6)*z[89];
    z[47]=z[11]*z[47];
    z[40]=n<T>(1,3)*z[40] + z[47];
    z[40]=z[40]*z[48];
    z[47]=z[63]*z[29];
    z[32]=z[32] - n<T>(1,4)*z[47];
    z[32]=z[32]*z[116];
    z[47]=z[80]*z[49];
    z[32]=z[32] + z[47];
    z[32]=z[11]*z[32];
    z[47]=z[103] + z[109];
    z[49]=z[63]*z[105];
    z[80]= - z[10]*z[34]*z[123];
    z[49]=z[49] + z[80];
    z[49]=z[49]*z[58];
    z[32]=z[32] + 2*z[47] + z[49];
    z[32]=z[11]*z[32];
    z[47]=z[19]*z[82];
    z[49]= - z[84] + n<T>(1,4)*z[121];
    z[49]=z[3]*z[49];
    z[32]=z[32] + z[47] + 5*z[49];
    z[32]=z[32]*z[107];
    z[47]=npow(z[12],4);
    z[49]=z[47]*z[137];
    z[80]= - z[108] + z[97];
    z[80]=z[80]*z[76];
    z[80]=z[80] - z[49];
    z[80]=z[80]*z[95];
    z[89]=n<T>(3,2)*z[1];
    z[99]= - z[89] + 4*z[84];
    z[99]=z[99]*z[76];
    z[99]=z[99] - z[49];
    z[99]=z[10]*z[99];
    z[103]=6*z[34];
    z[108]=z[57] - z[103];
    z[108]=z[12]*z[108];
    z[109]=n<T>(2,3)*z[1];
    z[108]=z[109] + z[108];
    z[108]=z[108]*z[63];
    z[99]=z[108] + z[99];
    z[99]=z[10]*z[99];
    z[108]= - n<T>(11,6)*z[23] + 4*z[73];
    z[108]=z[12]*z[108];
    z[108]= - n<T>(13,12)*z[19] + z[108];
    z[108]=z[12]*z[108];
    z[108]= - n<T>(1,12)*z[1] + z[108];
    z[108]=z[12]*z[108];
    z[99]=z[108] + z[99];
    z[99]=z[3]*z[99];
    z[108]=n<T>(1,3)*z[19];
    z[116]=z[108] - z[103];
    z[116]=z[12]*z[116];
    z[109]= - z[109] + z[116];
    z[109]=z[109]*z[63];
    z[80]=z[99] + z[109] + z[80];
    z[80]=z[3]*z[80];
    z[99]=z[76]*z[1];
    z[32]=z[32] - n<T>(2,3)*z[99] + z[80];
    z[32]=z[9]*z[32];
    z[80]= - z[1] + 9*z[84];
    z[80]=z[80]*z[76];
    z[80]=z[80] - 4*z[49];
    z[80]=z[10]*z[80];
    z[109]= - z[63]*z[39];
    z[109]=n<T>(1,6)*z[1] + z[109];
    z[109]=z[109]*z[63];
    z[80]=z[109] + z[80];
    z[80]=z[10]*z[80];
    z[109]=z[137]*z[76];
    z[116]=n<T>(31,12)*z[1] - 7*z[84];
    z[116]=z[116]*z[63];
    z[116]=z[116] + n<T>(7,3)*z[109];
    z[116]=z[10]*z[116];
    z[119]=n<T>(19,6)*z[19];
    z[141]= - z[119] + 7*z[34];
    z[141]=z[12]*z[141];
    z[145]=n<T>(5,3)*z[1];
    z[141]= - z[145] + z[141];
    z[141]=z[12]*z[141];
    z[116]=z[141] + z[116];
    z[116]=z[10]*z[116];
    z[141]=n<T>(5,4)*z[23] - n<T>(7,3)*z[73];
    z[141]=z[12]*z[141];
    z[146]=n<T>(3,2)*z[19];
    z[141]=z[146] + z[141];
    z[141]=z[12]*z[141];
    z[147]=n<T>(7,4)*z[1];
    z[116]=z[116] + z[147] + z[141];
    z[116]=z[3]*z[116];
    z[141]=5*z[73];
    z[148]=z[39] - z[141];
    z[148]=z[12]*z[148];
    z[148]=z[20] + z[148];
    z[148]=z[12]*z[148];
    z[89]= - z[89] + z[148];
    z[89]=z[12]*z[89];
    z[80]=z[116] + z[80] + n<T>(19,12) + z[89];
    z[80]=z[3]*z[80];
    z[89]=z[84] + n<T>(7,3)*z[1];
    z[116]= - z[89]*z[76];
    z[116]=z[116] - z[49];
    z[95]=z[116]*z[95];
    z[108]=z[108] + z[87];
    z[108]=z[12]*z[108];
    z[108]=n<T>(19,6)*z[1] + 5*z[108];
    z[108]=z[12]*z[108];
    z[108]= - n<T>(13,6) + z[108];
    z[108]=z[12]*z[108];
    z[116]=z[76]*z[136];
    z[32]=z[32] + z[40] + z[80] + z[95] + z[108] + z[116];
    z[32]=z[9]*z[32];
    z[40]=z[24]*z[9];
    z[40]=z[40] - z[43];
    z[40]=z[125]*z[40];
    z[40]=z[53] + z[40];
    z[40]=z[9]*z[40];
    z[26]= - z[26]*z[117];
    z[37]=z[37] - z[42];
    z[37]=z[37]*z[81];
    z[26]=z[40] + 2*z[37] + z[62] + z[26];
    z[26]=z[4]*z[26];
    z[37]= - z[20] + z[52];
    z[37]=z[14]*z[37];
    z[37]= - z[28] + z[37];
    z[37]=z[8]*z[37];
    z[37]=z[54] + z[37];
    z[37]=z[14]*z[37];
    z[36]= - z[44] + 5*z[36];
    z[36]=z[36]*z[55];
    z[40]=z[55]*z[23];
    z[42]=z[40]*z[24];
    z[43]=z[40]*z[9];
    z[44]=5*z[43];
    z[54]= - z[14]*z[44];
    z[52]=z[54] - z[52] + z[42];
    z[52]=z[9]*z[52];
    z[26]=z[26] + z[52] + z[36] + z[37];
    z[26]=z[4]*z[26];
    z[36]=4*z[19];
    z[24]=z[24]*z[36];
    z[37]=z[41]*z[46];
    z[24]=z[24] - 13*z[37];
    z[24]=z[24]*z[107];
    z[37]=13*z[138] + n<T>(95,4)*z[1] + 14*z[106];
    z[37]=z[37]*z[110];
    z[41]=n<T>(1,2)*z[21];
    z[46]=z[41] + z[19];
    z[41]=z[91] - z[41];
    z[41]=z[14]*z[41];
    z[41]=n<T>(2,3)*z[23] + z[41];
    z[41]=z[8]*z[41];
    z[24]=z[26] + z[37] + z[24] + n<T>(5,6)*z[46] + z[41];
    z[24]=z[4]*z[24];
    z[26]=z[28] + z[126];
    z[26]=z[12]*z[26];
    z[26]=z[146] + z[26];
    z[26]=z[12]*z[26];
    z[37]=z[64]*z[96];
    z[26]=z[26] + z[37];
    z[26]=z[14]*z[26];
    z[37]=z[27] + 6*z[67];
    z[37]=z[12]*z[37];
    z[37]=z[23] + z[37];
    z[37]=z[12]*z[37];
    z[41]=z[71] + z[68];
    z[41]=z[41]*z[12];
    z[52]=z[28] + z[41];
    z[52]=z[12]*z[52];
    z[52]=z[20] + z[52];
    z[52]=z[52]*z[96];
    z[37]=z[37] + z[52];
    z[37]=z[14]*z[37];
    z[37]=z[37] - n<T>(3,8)*z[23] + z[41];
    z[37]=z[6]*z[37];
    z[41]=z[28] + z[127];
    z[41]=z[12]*z[41];
    z[26]=z[37] + z[41] + z[26];
    z[26]=z[6]*z[26];
    z[37]= - z[21]*z[114];
    z[37]=z[37] - z[1] - n<T>(2,3)*z[84];
    z[37]=z[14]*z[37];
    z[41]=z[6]*z[132];
    z[37]=n<T>(3,4)*z[41] - n<T>(2,3)*z[34] + z[37];
    z[37]=z[8]*z[37];
    z[41]=n<T>(7,2)*z[1];
    z[52]= - z[41] + 5*z[84];
    z[54]=z[133]*z[117];
    z[55]= - static_cast<T>(8)- z[60];
    z[55]=z[55]*z[110];
    z[62]=z[12]*z[89];
    z[62]= - n<T>(4,3) + n<T>(1,2)*z[62];
    z[62]=z[14]*z[62];
    z[24]=z[24] + z[55] + n<T>(5,3)*z[54] + z[37] + z[26] + n<T>(1,6)*z[52] + 
    z[62];
    z[24]=z[15]*z[24];
    z[25]=z[25]*z[133];
    z[26]=n<T>(3,4)*z[19];
    z[25]= - z[25] + z[26] - z[136];
    z[37]=z[19] - z[56];
    z[37]=z[37]*z[58];
    z[37]=z[37] - z[106];
    z[52]=n<T>(1,4)*z[16];
    z[37]=z[37]*z[52];
    z[54]= - z[23] + z[29];
    z[54]=z[54]*z[142];
    z[55]=z[91] + z[21];
    z[56]=n<T>(1,2)*z[14];
    z[55]=z[55]*z[56];
    z[55]=z[55] + z[23];
    z[56]=z[15]*z[55];
    z[37]=z[37] + z[56] + z[54] + z[25];
    z[37]=z[13]*z[37];
    z[54]=z[23] + n<T>(1,2)*z[51];
    z[54]=z[54]*z[14];
    z[54]=z[54] + z[68];
    z[56]=z[54]*z[15];
    z[46]=z[46]*z[14];
    z[46]=z[46] + z[28];
    z[56]=z[56] - z[46];
    z[62]=n<T>(11,2)*z[19];
    z[64]=z[62] + 13*z[21];
    z[42]=z[64] + 7*z[42];
    z[67]=z[85]*z[68];
    z[67]=z[67] + z[42];
    z[67]=z[67]*z[52];
    z[67]=z[67] + z[56];
    z[67]=z[13]*z[67];
    z[56]=z[6]*z[56];
    z[42]=z[6]*z[42];
    z[71]=z[85]*z[128];
    z[42]=n<T>(1,2)*z[71] + z[42];
    z[42]=z[42]*z[52];
    z[42]=z[67] + z[42] + z[56];
    z[42]=z[17]*z[42];
    z[54]=z[6]*z[54];
    z[54]=z[54] + z[55];
    z[54]=z[15]*z[54];
    z[46]= - z[6]*z[46];
    z[25]=z[54] + z[46] + z[25];
    z[25]=z[6]*z[25];
    z[46]=z[64]*z[79];
    z[54]=z[128] - z[23];
    z[55]=z[3]*z[6]*z[54];
    z[55]=z[135] + z[55];
    z[55]=z[55]*z[58];
    z[56]=7*z[23];
    z[64]=z[90]*z[56];
    z[67]= - z[6]*z[51];
    z[64]=z[67] + z[64];
    z[64]=z[64]*z[48];
    z[46]=z[64] + z[46] + z[55];
    z[46]=z[46]*z[52];
    z[52]=z[3]*z[128];
    z[52]= - z[134] + z[52];
    z[52]=z[52]*z[142];
    z[25]=z[42] + z[37] + z[46] + z[52] + z[25];
    z[25]=z[17]*z[25];
    z[37]=z[63]*z[92];
    z[42]= - z[137]*z[98];
    z[46]=z[1] + z[97];
    z[46]=z[12]*z[46];
    z[37]=z[42] + z[46] + z[37];
    z[37]=z[10]*z[37];
    z[42]= - z[33] - z[34];
    z[42]=z[12]*z[42];
    z[46]=z[21]*z[70];
    z[37]=z[37] + z[46] + z[102] + z[42];
    z[37]=z[10]*z[37];
    z[42]=z[60]*z[10];
    z[35]=z[35] - z[42];
    z[35]= - z[69] + z[86] + 6*z[35];
    z[35]=z[10]*z[35];
    z[46]=z[34] + z[19];
    z[35]=z[35] - 2*z[46] + 9*z[21];
    z[35]=z[10]*z[35];
    z[52]= - z[46]*z[113];
    z[35]= - 3*z[128] + z[35] + z[39] + z[52];
    z[35]=z[7]*z[35];
    z[52]=z[122]*z[12];
    z[55]=4*z[1];
    z[64]=z[55] - z[52];
    z[64]=z[14]*z[64];
    z[67]=6*z[134];
    z[35]=z[35] + z[67] + z[37] + z[64] - z[57] + z[34];
    z[35]=z[7]*z[35];
    z[37]=z[111] - z[109];
    z[64]= - z[140] + z[86];
    z[64]=z[64]*z[63];
    z[37]=z[64] + 6*z[37];
    z[37]=z[10]*z[37];
    z[52]= - z[55] - z[52];
    z[52]=z[52]*z[74];
    z[37]=z[37] + 8*z[101] + static_cast<T>(3)+ z[52];
    z[37]=z[10]*z[37];
    z[52]=z[130] + z[34];
    z[52]=z[12]*z[52];
    z[55]=z[140] - z[112];
    z[55]=z[12]*z[55];
    z[55]= - static_cast<T>(4)+ z[55];
    z[55]=z[14]*z[55];
    z[64]= - z[19]*z[129];
    z[35]=z[35] + z[64] + z[37] + z[55] - z[147] + z[52];
    z[35]=z[7]*z[35];
    z[37]=2*z[84];
    z[52]= - z[37] + z[42];
    z[52]=z[10]*z[52];
    z[55]=n<T>(3,4)*z[134];
    z[52]= - z[55] + z[52] - n<T>(9,4)*z[19] + z[87];
    z[52]=z[6]*z[52];
    z[64]=z[139]*z[60];
    z[69]=z[3]*z[28];
    z[64]=z[69] - n<T>(1,3)*z[64] + n<T>(5,4)*z[19] - z[100];
    z[64]=z[3]*z[64];
    z[69]=z[91] - z[21];
    z[69]=z[14]*z[69];
    z[39]= - z[39] + z[69];
    z[39]=z[14]*z[39];
    z[69]=z[66]*z[118];
    z[70]=3*z[27];
    z[39]=z[69] + z[70] + z[39];
    z[39]=z[7]*z[39];
    z[69]=n<T>(15,2)*z[19] - 4*z[21];
    z[69]=z[14]*z[69];
    z[39]=z[39] + z[69] - n<T>(15,2)*z[132];
    z[39]=z[7]*z[39];
    z[39]=z[39] + z[67] + 6*z[19] - n<T>(7,2)*z[21];
    z[39]=z[7]*z[39];
    z[60]=static_cast<T>(1)- z[60];
    z[60]=z[14]*z[60];
    z[42]=z[60] + z[42] - z[84];
    z[39]=z[39] + z[64] + z[52] + z[1] + n<T>(4,3)*z[42];
    z[39]=z[8]*z[39];
    z[42]=z[124] - z[70];
    z[42]=z[42]*z[14];
    z[52]=npow(z[1],6);
    z[60]=z[52]*z[118];
    z[42]=z[42] + z[60] + 3*z[66];
    z[42]=z[42]*z[7];
    z[60]=z[66]*z[6];
    z[60]=z[60] + z[27];
    z[64]= - z[51] + 6*z[23];
    z[64]=z[64]*z[14];
    z[42]= - z[42] - z[64] + n<T>(15,2)*z[60];
    z[42]=z[42]*z[7];
    z[33]=z[33] - z[21];
    z[33]=z[33]*z[113];
    z[33]=z[42] + z[33] - n<T>(19,2)*z[128] - 9*z[23];
    z[33]=z[33]*z[7];
    z[42]=z[57] + z[137];
    z[42]=z[42]*z[10];
    z[42]=z[42] + z[93];
    z[60]=z[31]*z[105];
    z[42]=z[60] + n<T>(1,3)*z[42];
    z[42]=z[42]*z[3];
    z[55]=z[55] + z[57] - z[137];
    z[55]=z[10]*z[55];
    z[55]= - z[93] + z[55];
    z[55]=z[55]*z[6];
    z[57]=z[137] + 2*z[61];
    z[33]=z[33] - z[42] - z[55] - n<T>(4,3)*z[57];
    z[42]=z[8]*z[33];
    z[55]=z[124] + z[70];
    z[55]=z[55]*z[14];
    z[52]=z[52]*z[3];
    z[52]=z[55] - z[52];
    z[52]=z[52]*z[4];
    z[51]=z[94] + z[51];
    z[51]=z[51]*z[14];
    z[55]=z[123]*z[66];
    z[51]=z[52] - z[51] + z[55] - z[68];
    z[51]=z[51]*z[4];
    z[29]=z[51] - z[23] - n<T>(2,3)*z[53] - n<T>(19,6)*z[29];
    z[51]=z[4]*z[29];
    z[33]=z[51] - z[33];
    z[33]=z[2]*z[33];
    z[29]= - z[4]*z[8]*z[29];
    z[29]=z[33] + z[42] + z[29];
    z[29]=z[5]*z[29];
    z[33]=z[134] - z[19];
    z[33]=z[33]*z[129];
    z[33]=z[33] - z[145];
    z[42]=z[79]*z[70];
    z[42]= - z[42] + n<T>(49,3)*z[19];
    z[51]=z[42]*z[58];
    z[52]=z[51] + z[33];
    z[52]=z[3]*z[52];
    z[53]=z[44] + 31*z[1];
    z[55]=z[85]*z[53];
    z[57]=z[121]*z[48];
    z[55]=19*z[57] + z[55];
    z[55]=z[55]*z[110];
    z[33]=n<T>(19,3)*z[138] + z[33];
    z[33]=z[15]*z[33];
    z[33]=z[33] + z[52] + z[55];
    z[52]=z[16]*z[40];
    z[52]=n<T>(5,2)*z[52] - z[44];
    z[52]=z[85]*z[52];
    z[55]=z[1]*z[85];
    z[55]= - 25*z[55] + z[57];
    z[20]=z[15]*z[48]*z[20];
    z[20]=z[20] + n<T>(1,2)*z[55] + z[52];
    z[20]=z[16]*z[20];
    z[20]=n<T>(1,2)*z[33] + n<T>(1,3)*z[20];
    z[33]=n<T>(1,2)*z[16];
    z[20]=z[20]*z[33];
    z[48]=z[53]*z[110];
    z[42]=n<T>(1,2)*z[42] + z[48];
    z[42]=z[15]*z[42];
    z[44]=z[3]*z[44];
    z[44]=31*z[143] + z[44];
    z[44]=z[44]*z[110];
    z[42]=z[42] + z[51] + z[44];
    z[44]=z[3] + z[15];
    z[33]=z[33]*z[40]*z[44];
    z[40]= - z[3]*z[43];
    z[44]=n<T>(5,2)*z[1];
    z[43]= - z[44] - z[43];
    z[43]=z[15]*z[43];
    z[33]=z[33] + z[43] - n<T>(5,2)*z[143] + z[40];
    z[33]=z[16]*z[33];
    z[33]=n<T>(1,2)*z[42] + n<T>(5,3)*z[33];
    z[33]=z[16]*z[33];
    z[40]=z[15]*z[19];
    z[40]=z[121] + z[40];
    z[40]=z[13]*z[40];
    z[42]= - z[15]*z[1];
    z[40]=z[40] - z[143] + z[42];
    z[40]=z[40]*npow(z[9],2);
    z[33]=z[33] + z[40];
    z[33]=z[18]*z[33];
    z[40]= - z[85]*z[65];
    z[40]=z[40] - n<T>(5,3)*z[57];
    z[40]=z[9]*z[40];
    z[42]= - z[19] + z[88];
    z[42]=z[15]*z[42];
    z[43]=z[1] + z[121];
    z[43]=z[9]*z[3]*z[43];
    z[42]=z[42] - z[121] + z[43];
    z[42]=z[13]*z[42];
    z[40]=n<T>(1,2)*z[42] - n<T>(7,3)*z[143] + z[40];
    z[40]=z[9]*z[40];
    z[42]=z[54]*z[129];
    z[42]=z[42] + n<T>(11,3)*z[19];
    z[43]= - z[140] - 5*z[138];
    z[43]=z[43]*z[110];
    z[43]= - n<T>(1,4)*z[42] + z[43];
    z[43]=z[15]*z[43];
    z[31]= - z[42]*z[31];
    z[20]=n<T>(1,2)*z[33] + z[20] + z[43] + z[31] + z[40];
    z[20]=z[18]*z[20];
    z[31]=z[136]*z[47];
    z[33]= - 28*z[1] + z[97];
    z[33]=z[33]*z[76];
    z[33]=z[33] - z[31];
    z[33]=z[14]*z[33];
    z[40]=n<T>(11,4)*z[1] - z[86];
    z[40]=z[40]*z[63];
    z[21]=z[47]*z[21];
    z[21]=z[99] - 10*z[21];
    z[21]=z[10]*z[21];
    z[21]=z[21] + z[40] + z[33];
    z[21]=z[10]*z[21];
    z[33]=z[102] + z[84];
    z[33]=z[33]*z[78];
    z[40]=3*z[34];
    z[42]= - z[19] + z[40];
    z[42]=z[12]*z[42];
    z[42]= - n<T>(55,2)*z[1] + z[42];
    z[42]=z[42]*z[63];
    z[42]=z[42] - z[33];
    z[42]=z[14]*z[42];
    z[43]=11*z[19];
    z[48]= - z[43] + z[40];
    z[48]=z[12]*z[48];
    z[48]=z[59] + z[48];
    z[48]=z[12]*z[48];
    z[21]=z[21] + z[48] + z[42];
    z[21]=z[10]*z[21];
    z[42]=z[147] - z[37];
    z[42]=z[42]*z[98];
    z[48]= - n<T>(19,2)*z[1] - z[97];
    z[48]=z[48]*z[77];
    z[47]= - z[47]*z[92];
    z[47]=z[99] + z[47];
    z[47]=z[10]*z[47];
    z[42]=z[47] + z[42] + z[48];
    z[42]=z[14]*z[42];
    z[47]=z[19]*z[63];
    z[42]=z[47] + z[42];
    z[42]=z[10]*z[42];
    z[47]= - 13*z[19] - z[103];
    z[47]=z[12]*z[47];
    z[48]=n<T>(21,2)*z[1];
    z[47]=z[48] + z[47];
    z[47]=z[12]*z[47];
    z[51]= - n<T>(13,2)*z[19] - z[40];
    z[51]=z[12]*z[51];
    z[48]= - z[48] + z[51];
    z[48]=z[48]*z[144];
    z[47]=z[47] + z[48];
    z[47]=z[14]*z[47];
    z[40]=n<T>(13,4)*z[19] - z[40];
    z[40]=z[12]*z[40];
    z[40]=z[42] + z[40] + z[47];
    z[40]=z[10]*z[40];
    z[42]= - z[56] - 6*z[73];
    z[42]=z[12]*z[42];
    z[42]= - 8*z[19] + z[42];
    z[42]=z[12]*z[42];
    z[23]=n<T>(7,2)*z[23];
    z[47]=z[23] + z[72];
    z[47]=z[47]*z[12];
    z[36]= - z[36] - z[47];
    z[36]=z[12]*z[36];
    z[36]= - n<T>(9,2)*z[1] + z[36];
    z[36]=z[36]*z[96];
    z[36]=z[36] + n<T>(79,8)*z[1] + z[42];
    z[36]=z[14]*z[36];
    z[36]=z[40] + z[36] + n<T>(17,4)*z[19] - z[47];
    z[36]=z[6]*z[36];
    z[40]= - z[94] + z[72];
    z[40]=z[12]*z[40];
    z[40]= - z[104] + z[40];
    z[40]=z[12]*z[40];
    z[40]= - n<T>(47,4)*z[1] + z[40];
    z[40]=z[12]*z[40];
    z[42]= - z[46]*z[74];
    z[42]= - z[44] + z[42];
    z[42]=z[42]*z[144];
    z[40]=z[42] + static_cast<T>(4)+ z[40];
    z[40]=z[14]*z[40];
    z[42]=z[12]*z[83];
    z[42]= - z[130] + z[42];
    z[42]=z[12]*z[42];
    z[21]=z[36] + z[21] + z[40] + z[1] + z[42];
    z[21]=z[6]*z[21];
    z[36]=n<T>(23,2)*z[1] - z[115];
    z[36]=z[36]*z[63];
    z[36]=n<T>(1,3)*z[36] + 4*z[109];
    z[36]=z[10]*z[36];
    z[40]= - z[43] + z[87];
    z[40]=z[40]*z[114];
    z[40]=z[1] + z[40];
    z[40]=z[12]*z[40];
    z[36]=z[36] - n<T>(1,12) + z[40];
    z[36]=z[10]*z[36];
    z[40]=n<T>(7,4)*z[134] - n<T>(1,4)*z[46] + z[45];
    z[40]=z[6]*z[40];
    z[26]=z[26] - n<T>(4,3)*z[34];
    z[26]=z[12]*z[26];
    z[42]= - z[137]*z[82];
    z[43]= - n<T>(13,4)*z[1] + 8*z[84];
    z[43]=z[12]*z[43];
    z[42]=z[43] + z[42];
    z[42]=z[10]*z[42];
    z[26]=n<T>(1,3)*z[42] + z[131] + z[26];
    z[26]=z[10]*z[26];
    z[26]= - n<T>(5,8)*z[134] + n<T>(1,8)*z[19] + z[26];
    z[26]=z[3]*z[26];
    z[28]= - z[28] + z[141];
    z[28]=z[28]*z[114];
    z[28]= - z[19] + z[28];
    z[28]=z[12]*z[28];
    z[26]=z[26] + n<T>(1,2)*z[40] + z[36] + n<T>(5,12)*z[1] + z[28];
    z[26]=z[3]*z[26];
    z[28]= - z[41] + z[37];
    z[28]=z[28]*z[76];
    z[28]= - 7*z[49] + 3*z[28] - z[31];
    z[28]=z[10]*z[28];
    z[31]=z[62] + z[103];
    z[31]=z[12]*z[31];
    z[31]= - n<T>(17,3)*z[1] + z[31];
    z[31]=z[12]*z[31];
    z[31]=n<T>(7,6) + z[31];
    z[31]=z[12]*z[31];
    z[28]=z[28] + z[31] - z[33];
    z[28]=z[10]*z[28];
    z[27]= - z[27]*z[74];
    z[23]=z[23] + z[27];
    z[23]=z[12]*z[23];
    z[23]=z[119] + z[23];
    z[23]=z[12]*z[23];
    z[23]= - z[120] + z[23];
    z[23]=z[12]*z[23];
    z[19]= - n<T>(2,3)*z[19] - z[34];
    z[19]=z[19]*z[74];
    z[19]= - z[44] + z[19];
    z[19]=z[12]*z[19];
    z[19]=n<T>(1,6) + z[19];
    z[19]=z[19]*z[96];

    r += n<T>(7,12) + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25]
       + z[26] + z[28] + z[29] + z[30] + z[32] + z[35] + z[38] + z[39]
       + z[50] + z[75];
 
    return r;
}

template double qg_2lNLC_r1119(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1119(const std::array<dd_real,31>&);
#endif
