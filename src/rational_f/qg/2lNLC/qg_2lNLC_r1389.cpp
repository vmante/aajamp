#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1389(const std::array<T,31>& k) {
  T z[140];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[7];
    z[9]=k[5];
    z[10]=k[13];
    z[11]=k[23];
    z[12]=k[14];
    z[13]=k[9];
    z[14]=k[2];
    z[15]=k[24];
    z[16]=k[29];
    z[17]=k[3];
    z[18]=k[18];
    z[19]=npow(z[15],2);
    z[20]=3*z[19];
    z[21]=z[3]*z[12];
    z[21]= - z[20] + z[21];
    z[22]=z[12] + z[15];
    z[23]=npow(z[7],2);
    z[24]=z[22]*z[23];
    z[25]=npow(z[2],2);
    z[26]=z[25]*z[3];
    z[24]=z[24] + z[26];
    z[27]=3*z[6];
    z[24]=z[24]*z[27];
    z[28]= - n<T>(5,3)*z[3] + 12*z[2];
    z[28]=z[2]*z[28];
    z[21]=z[24] + 4*z[21] + z[28];
    z[21]=z[6]*z[21];
    z[24]=2*z[11];
    z[28]=z[11]*z[17];
    z[29]=3*z[28];
    z[30]= - static_cast<T>(2)+ z[29];
    z[30]=z[30]*z[24];
    z[31]=z[13]*z[17];
    z[32]=z[8]*z[17];
    z[33]= - 4*z[32] - static_cast<T>(5)+ z[31];
    z[34]=3*z[8];
    z[33]=z[33]*z[34];
    z[35]=npow(z[17],2);
    z[36]=z[35]*z[13];
    z[37]=z[35]*z[11];
    z[38]=z[36] - z[37];
    z[39]=3*z[23];
    z[40]= - z[38]*z[39];
    z[41]=z[34]*z[17];
    z[42]=z[41] + n<T>(2,3) - z[29];
    z[42]=z[3]*z[42];
    z[43]=3*z[16];
    z[44]=n<T>(1,3)*z[3];
    z[45]= - z[43] + z[44];
    z[46]=2*z[6];
    z[45]=z[45]*z[46];
    z[45]= - n<T>(53,3) + z[45];
    z[47]=2*z[5];
    z[45]=z[45]*z[47];
    z[48]=z[31] - z[28];
    z[49]=z[6]*z[2];
    z[49]=n<T>(8,3)*z[49] + n<T>(4,3) + 9*z[48];
    z[49]=z[4]*z[49];
    z[50]=n<T>(4,3)*z[2];
    z[51]=3*z[13];
    z[52]=6*z[16];
    z[53]=z[52] - z[15];
    z[21]=z[49] + z[45] + z[21] - z[50] + 4*z[42] + z[40] + z[33] + 23*
    z[12] + z[51] + 7*z[53] + z[30];
    z[21]=z[4]*z[21];
    z[30]=z[41] + 11*z[28] - 2*z[31];
    z[30]=z[30]*z[23];
    z[33]=2*z[2];
    z[40]= - static_cast<T>(7)- z[29];
    z[40]=z[11]*z[40];
    z[42]=z[51]*z[17];
    z[45]=static_cast<T>(7)+ z[42];
    z[45]=z[13]*z[45];
    z[40]=n<T>(2,3)*z[5] + z[33] + z[40] + z[45];
    z[40]=z[4]*z[40];
    z[45]=npow(z[11],2);
    z[49]=2*z[45];
    z[53]= - z[19] + z[49];
    z[54]=5*z[16];
    z[55]=9*z[13];
    z[56]=z[54] + z[55];
    z[57]=12*z[12];
    z[56]=z[56]*z[57];
    z[58]=17*z[10];
    z[59]=6*z[12];
    z[60]=n<T>(35,3)*z[8] + z[58] + z[59];
    z[60]=z[8]*z[60];
    z[61]=8*z[12];
    z[62]=17*z[15] + z[61];
    z[62]=z[62]*z[23];
    z[63]=z[23]*z[2];
    z[62]=z[62] + 3*z[63];
    z[62]=z[6]*z[62];
    z[64]=npow(z[13],2);
    z[65]=6*z[64];
    z[66]=z[10]*z[15];
    z[67]=2*z[18];
    z[68]= - n<T>(47,3)*z[2] - z[67] - 19*z[8];
    z[68]=z[2]*z[68];
    z[69]=6*z[2] - z[11] - 26*z[13];
    z[69]=z[5]*z[69];
    z[30]=2*z[40] + z[69] + z[62] + z[68] + z[30] + z[60] + z[56] - 
    z[65] + 6*z[53] + 17*z[66];
    z[30]=z[4]*z[30];
    z[40]=z[23]*z[14];
    z[53]=62*z[12];
    z[56]= - 65*z[15] + z[53];
    z[56]=z[56]*z[40];
    z[60]=3*z[2];
    z[62]= - z[60] + z[10] - 3*z[40];
    z[62]=z[2]*z[62];
    z[68]=n<T>(1,3)*z[8];
    z[69]= - z[6]*z[23]*z[68];
    z[70]=z[11] - z[15];
    z[70]=z[70]*z[10];
    z[71]=npow(z[12],2);
    z[72]=6*z[71];
    z[73]=n<T>(16,3)*z[8] - z[10] - z[59];
    z[73]=z[8]*z[73];
    z[56]=z[69] + z[62] + z[56] + z[73] + z[72] - z[65] - 12*z[19] + 
    z[70];
    z[56]=z[5]*z[56];
    z[62]=2*z[10];
    z[69]=z[15]*z[62];
    z[69]=z[20] + z[69];
    z[69]=z[10]*z[69];
    z[73]=z[13] + z[16];
    z[74]=z[73]*z[12];
    z[75]=2*z[64];
    z[76]=z[74] + z[75];
    z[77]=z[76]*z[59];
    z[69]=z[69] + z[77];
    z[77]=npow(z[10],2);
    z[78]= - z[8]*z[10];
    z[78]=6*z[77] + z[78];
    z[78]=z[8]*z[78];
    z[69]=3*z[69] + z[78];
    z[78]= - z[18] + n<T>(7,3)*z[8];
    z[79]=4*z[2];
    z[80]=z[78]*z[79];
    z[81]=npow(z[8],2);
    z[82]=n<T>(4,3)*z[81];
    z[80]=z[80] + z[82] + z[39];
    z[80]=z[2]*z[80];
    z[83]=z[45] - z[64];
    z[84]=z[13] - z[11];
    z[85]= - z[50] + z[84];
    z[85]=z[5]*z[85];
    z[85]= - 6*z[83] + z[85];
    z[85]=z[4]*z[85];
    z[86]=2*z[13];
    z[87]=z[86] + 8*z[15] + z[11];
    z[87]=2*z[87] + z[12];
    z[87]=z[87]*z[23];
    z[75]= - z[45] - z[75];
    z[75]=3*z[75] + 2*z[25];
    z[75]=z[75]*z[47];
    z[69]=z[85] + z[75] + z[80] + 2*z[69] + z[87];
    z[69]=z[4]*z[69];
    z[75]=z[45] - z[19];
    z[75]=6*z[75];
    z[70]=z[75] - z[70];
    z[70]=z[10]*z[70];
    z[80]=7*z[10];
    z[85]=z[8]*z[80];
    z[85]=z[77] + z[85];
    z[85]=z[8]*z[85];
    z[87]=5*z[15];
    z[88]=4*z[12];
    z[89]= - z[87] + z[88];
    z[39]=z[89]*z[39];
    z[89]= - z[2]*z[80];
    z[89]= - z[77] + z[89];
    z[89]=z[2]*z[89];
    z[39]=z[89] + z[39] + z[70] + z[85];
    z[39]=z[5]*z[39];
    z[70]=3*z[45];
    z[85]=2*z[19];
    z[89]=z[11]*z[62];
    z[89]=z[89] + z[85] - z[70];
    z[89]=z[89]*z[23];
    z[90]=z[13] - z[16];
    z[91]=2*z[71];
    z[90]=z[90]*z[91];
    z[92]= - z[8]*z[90];
    z[89]=z[92] + z[89];
    z[92]=z[10]*z[63];
    z[39]=z[69] + z[39] + 6*z[89] + z[92];
    z[39]=z[9]*z[39];
    z[69]=z[20] + z[66];
    z[69]=z[10]*z[69];
    z[89]=z[12]*z[16];
    z[89]=z[89] + z[64];
    z[92]=z[89]*z[57];
    z[69]=z[69] + z[92];
    z[73]= - z[88] + z[73];
    z[92]=2*z[12];
    z[73]=z[73]*z[92];
    z[73]=z[77] + z[73];
    z[93]=5*z[10];
    z[94]=3*z[12];
    z[95]=z[93] + z[94];
    z[96]=z[8]*z[95];
    z[73]=6*z[73] + z[96];
    z[73]=z[8]*z[73];
    z[96]= - z[23] + z[64];
    z[97]=6*z[14];
    z[96]=z[97]*z[96];
    z[98]=n<T>(2,3)*z[10];
    z[99]= - 3*z[18] - z[98];
    z[96]=12*z[8] + 2*z[99] - z[51] + z[96];
    z[96]=z[2]*z[96];
    z[99]=4*z[8];
    z[100]= - z[51] + z[99];
    z[100]=z[8]*z[100];
    z[96]=z[96] + z[65] + z[100];
    z[96]=z[2]*z[96];
    z[100]=z[10]*z[11];
    z[75]=3*z[81] - z[75] - 17*z[100];
    z[75]=z[75]*z[23];
    z[101]= - n<T>(5,3)*z[8] - z[67] - n<T>(11,3)*z[10];
    z[101]=z[101]*z[23];
    z[101]=z[101] + n<T>(11,3)*z[63];
    z[101]=z[2]*z[101];
    z[75]=z[75] + z[101];
    z[75]=z[6]*z[75];
    z[101]=z[12] - z[43] + z[86];
    z[101]=z[12]*z[101];
    z[101]= - z[19] + z[101];
    z[101]=z[14]*z[101];
    z[102]=z[45]*z[17];
    z[101]= - z[102] + z[101];
    z[101]=z[101]*z[23];
    z[30]=z[39] + z[30] + z[56] + z[75] + z[96] + 12*z[101] + 6*z[69] + 
    z[73];
    z[30]=z[9]*z[30];
    z[39]=n<T>(14,3)*z[8];
    z[56]=9*z[16];
    z[69]= - z[56] - z[39];
    z[69]=z[69]*z[23];
    z[73]= - 47*z[81] + 67*z[23];
    z[73]=z[73]*z[44];
    z[69]=2*z[69] + z[73];
    z[69]=z[6]*z[69];
    z[73]=12*z[16];
    z[75]=z[40]*z[73];
    z[96]=8*z[16];
    z[101]=z[96] + z[8];
    z[101]=z[8]*z[101];
    z[75]=z[101] + z[75];
    z[101]= - 83*z[8] - 5*z[40];
    z[101]=z[101]*z[44];
    z[69]=z[69] + 3*z[75] + z[101];
    z[69]=z[6]*z[69];
    z[75]=npow(z[14],2);
    z[101]=z[75]*z[23];
    z[103]=18*z[16];
    z[104]=36*z[12] + z[51] - z[103] - 41*z[15];
    z[104]=z[104]*z[101];
    z[105]=3*z[14];
    z[106]=z[75]*z[3];
    z[107]= - z[105] - n<T>(2,3)*z[106];
    z[107]=z[2]*z[107];
    z[108]=z[3]*z[14];
    z[107]=z[107] - n<T>(8,3)*z[108] + n<T>(13,3) + 9*z[101];
    z[107]=z[2]*z[107];
    z[109]=z[75]*z[16];
    z[109]=z[109] - z[106];
    z[110]= - z[2]*z[109];
    z[111]=z[8]*z[16];
    z[112]=z[3]*z[8];
    z[113]=z[111] - z[112];
    z[114]= - z[6]*z[113];
    z[115]=z[3] - z[16];
    z[114]=z[114] - z[115];
    z[114]=z[114]*z[46];
    z[110]=z[110] + z[114];
    z[114]=6*z[5];
    z[110]=z[110]*z[114];
    z[116]= - z[19]*z[97];
    z[117]=z[105]*z[15];
    z[118]= - 14*z[101] + n<T>(113,3) - z[117];
    z[118]=z[3]*z[118];
    z[119]=60*z[16];
    z[120]=n<T>(37,3)*z[8];
    z[69]=z[110] + z[69] + z[107] + z[118] + z[104] + z[116] - z[120] - 
    z[88] - 22*z[13] - z[119] - 11*z[15];
    z[69]=z[5]*z[69];
    z[104]= - 18*z[13] - z[56] + z[24];
    z[107]=z[115]*z[6];
    z[110]= - z[114]*z[107];
    z[115]=n<T>(29,3)*z[2];
    z[104]=z[110] + z[115] - 10*z[3] + 2*z[104] - z[94];
    z[104]=z[5]*z[104];
    z[110]=3*z[11];
    z[116]= - z[34] + z[110] + z[88];
    z[116]=z[3]*z[116];
    z[118]=z[19] + z[45];
    z[121]=2*z[16];
    z[122]=z[121] + z[51];
    z[122]=z[122]*z[57];
    z[123]=n<T>(7,3)*z[10];
    z[124]=9*z[8];
    z[125]=z[123] + z[124];
    z[125]=z[8]*z[125];
    z[126]= - n<T>(17,3)*z[2] - z[3] - z[18] - n<T>(10,3)*z[8];
    z[33]=z[126]*z[33];
    z[126]=z[25]*z[6];
    z[127]=3*z[3];
    z[128]=z[127]*z[126];
    z[129]=z[79] + 9*z[84];
    z[130]=z[50]*z[6];
    z[131]=z[3]*z[130];
    z[131]=n<T>(8,3)*z[5] + z[131] + z[129];
    z[131]=z[4]*z[131];
    z[33]=z[131] + z[104] + z[128] + z[33] + z[116] + z[125] + z[122] + 
   6*z[118] - z[66];
    z[33]=z[4]*z[33];
    z[104]=npow(z[6],2);
    z[116]=npow(z[7],3);
    z[118]=z[116]*z[104]*z[20];
    z[78]=z[78]*z[2];
    z[78]=z[78] + n<T>(1,3)*z[81];
    z[78]=z[78]*z[2];
    z[122]=z[76]*z[92];
    z[125]= - z[15]*z[77];
    z[122]=z[125] + z[122];
    z[125]=3*z[77];
    z[128]=z[10]*z[68];
    z[128]= - z[125] + z[128];
    z[128]=z[8]*z[128];
    z[131]=3*z[116];
    z[132]=z[131]*z[45];
    z[133]=z[35]*z[132];
    z[118]=z[118] + z[78] + z[133] + 3*z[122] + z[128];
    z[122]= - z[38]*z[116];
    z[122]= - 2*z[83] + z[122];
    z[84]= - 2*z[84] - z[2];
    z[84]=z[5]*z[84];
    z[84]=3*z[122] + 4*z[84];
    z[84]=z[4]*z[84];
    z[122]=3*z[64];
    z[128]= - z[45] - z[122];
    z[128]=6*z[128] + n<T>(31,3)*z[25];
    z[128]=z[5]*z[128];
    z[84]=z[84] + 2*z[118] + z[128];
    z[84]=z[4]*z[84];
    z[48]= - z[48]*z[116];
    z[118]=z[83]*z[47];
    z[48]=z[48] + z[118];
    z[118]=3*z[4];
    z[48]=z[48]*z[118];
    z[128]= - z[15]*z[93];
    z[128]= - 6*z[19] + z[128];
    z[128]=z[128]*z[77];
    z[133]=npow(z[10],3);
    z[133]=5*z[133];
    z[134]=z[8]*z[77];
    z[134]= - z[133] + z[134];
    z[134]=z[8]*z[134];
    z[135]=6*z[45];
    z[136]=z[17]*z[116]*z[135];
    z[137]=z[116]*z[19];
    z[138]=z[6]*z[137];
    z[48]=z[48] + 18*z[138] + z[136] + z[128] + z[134];
    z[48]=z[4]*z[48];
    z[128]= - z[20] - z[91];
    z[128]=z[5]*z[128];
    z[128]=z[128] - z[90];
    z[134]=z[116]*z[14];
    z[128]=z[134]*z[128];
    z[132]= - z[6]*z[10]*z[132];
    z[128]=z[132] + z[128];
    z[132]=z[9]*z[4];
    z[136]=z[137]*z[132];
    z[48]=12*z[136] + 6*z[128] + z[48];
    z[48]=z[9]*z[48];
    z[128]=z[75]*z[116];
    z[136]= - z[128]*z[90];
    z[138]= - z[8]*z[71];
    z[90]= - z[90] + z[138];
    z[90]=z[8]*z[90];
    z[90]=z[90] + z[136];
    z[136]=n<T>(1,3)*z[10];
    z[138]=n<T>(4,3)*z[8];
    z[139]=z[138] - z[18] + z[136];
    z[139]=z[2]*z[139];
    z[82]=z[139] - z[125] + z[82];
    z[82]=z[2]*z[82];
    z[125]= - z[77]*z[110];
    z[82]=z[125] + z[82];
    z[82]=z[104]*z[116]*z[82];
    z[82]=3*z[90] + z[82];
    z[90]=3*z[71];
    z[125]= - 4*z[19] - z[90];
    z[125]=z[125]*z[75]*z[131];
    z[139]=z[25] + z[81];
    z[98]=z[98]*z[139];
    z[98]=z[125] + z[98];
    z[125]=z[81]*z[116];
    z[139]=z[104]*z[125];
    z[98]=2*z[98] - n<T>(5,3)*z[139];
    z[98]=z[5]*z[98];
    z[48]=z[48] + z[84] + 2*z[82] + z[98];
    z[48]=z[9]*z[48];
    z[82]= - z[116]*z[108];
    z[84]=z[16]*z[134];
    z[82]=z[84] + z[82];
    z[84]=z[116]*z[107];
    z[82]=2*z[82] + z[84];
    z[82]=z[6]*z[82];
    z[84]= - z[16]*z[128];
    z[98]= - z[8] + z[128];
    z[98]=z[3]*z[98];
    z[82]=z[82] + z[98] + z[111] + z[84];
    z[82]=z[82]*z[114];
    z[84]=z[116]*z[112];
    z[84]=n<T>(2,3)*z[125] - 15*z[84];
    z[84]=z[84]*z[104];
    z[98]=z[81]*z[3];
    z[82]=z[82] + n<T>(56,3)*z[98] + z[84];
    z[82]=z[6]*z[82];
    z[84]=npow(z[14],3);
    z[107]=3*z[15];
    z[111]= - z[84]*z[116]*z[107];
    z[111]=z[111] - z[110] + 13*z[8];
    z[111]=z[3]*z[111];
    z[116]= - z[84]*z[131];
    z[116]=n<T>(32,3) + z[116];
    z[116]=z[3]*z[116];
    z[128]=n<T>(1,3)*z[2];
    z[131]=static_cast<T>(23)+ 11*z[108];
    z[131]=z[131]*z[128];
    z[116]=z[116] + z[131];
    z[116]=z[2]*z[116];
    z[56]= - z[56] + z[86];
    z[56]=2*z[56] + n<T>(11,3)*z[8];
    z[56]=z[8]*z[56];
    z[131]=z[84]*z[137];
    z[56]=z[116] + z[111] - 6*z[131] - z[135] + z[56] + z[82];
    z[56]=z[5]*z[56];
    z[82]=z[12]*z[11];
    z[111]= - z[3]*z[92];
    z[111]=z[111] - z[49] - z[82];
    z[116]=17*z[3] + 26*z[2];
    z[116]=z[116]*z[128];
    z[79]= - z[4]*z[3]*z[79];
    z[79]=7*z[26] + z[79];
    z[79]=z[1]*z[79];
    z[79]=n<T>(1,3)*z[79] + 3*z[111] + z[116];
    z[79]=z[5]*z[79];
    z[111]=z[8]*z[12];
    z[116]= - z[82] - z[111];
    z[116]=z[116]*z[127];
    z[129]=n<T>(4,3)*z[3] - z[129];
    z[129]=z[5]*z[129];
    z[131]=z[50]*z[3];
    z[129]=z[131] + z[129];
    z[129]=z[4]*z[129];
    z[26]=n<T>(7,3)*z[26];
    z[79]=z[129] + z[116] - z[26] + z[79];
    z[79]=z[4]*z[79];
    z[116]=2*z[8];
    z[129]=z[12] - z[116];
    z[129]=z[8]*z[129];
    z[82]=z[82] + z[129];
    z[82]=z[82]*z[127];
    z[129]=npow(z[7],4);
    z[131]=z[129]*npow(z[6],4)*z[98];
    z[26]=n<T>(7,3)*z[131] + z[82] + z[26];
    z[26]=z[5]*z[26];
    z[82]= - z[11]*z[93];
    z[82]= - z[135] + z[82];
    z[82]=z[82]*z[77];
    z[131]=z[2]*z[77];
    z[131]= - z[133] + z[131];
    z[131]=z[2]*z[131];
    z[82]=z[82] + z[131];
    z[82]=z[9]*z[104]*z[129]*z[82];
    z[83]=z[83]*npow(z[4],2)*z[114];
    z[82]=z[83] + z[82];
    z[82]=z[9]*z[82];
    z[83]=z[81]*z[6];
    z[129]= - z[3]*z[83];
    z[129]=z[129] - z[81];
    z[129]=z[91]*z[129];
    z[98]= - z[12]*z[98];
    z[98]=z[98] + z[129];
    z[26]=z[82] + 3*z[98] + z[26] + z[79];
    z[26]=z[1]*z[26];
    z[79]=4*z[16];
    z[82]=z[79] - 9*z[12];
    z[82]=z[82]*z[92];
    z[98]= - z[92] + z[8];
    z[98]=z[98]*z[127];
    z[82]=z[98] + z[82] + z[111];
    z[82]=z[8]*z[82];
    z[98]=z[76]*z[14];
    z[129]= - z[88]*z[98];
    z[131]=z[2]*z[3];
    z[129]=z[131] - z[111] + z[129];
    z[129]=z[2]*z[129];
    z[82]=z[129] + z[82];
    z[129]= - z[71] + z[111];
    z[129]=z[8]*z[129];
    z[131]= - z[35]*z[125];
    z[129]=6*z[129] + z[131];
    z[129]=z[3]*z[129];
    z[91]= - z[81]*z[91];
    z[91]=z[91] + z[129];
    z[129]=z[17]*z[127];
    z[131]=z[6]*z[3];
    z[129]=z[129] + n<T>(7,3)*z[131];
    z[125]=z[6]*z[125]*z[129];
    z[91]=3*z[91] + z[125];
    z[91]=z[6]*z[91];
    z[26]=z[26] + z[48] + z[33] + z[56] + 3*z[82] + z[91];
    z[26]=z[1]*z[26];
    z[33]=z[20] + z[64];
    z[33]=z[33]*z[105];
    z[48]=z[15] + z[110];
    z[56]= - static_cast<T>(1)- z[42];
    z[56]=z[13]*z[56];
    z[41]= - n<T>(17,3) - z[41];
    z[41]=z[8]*z[41];
    z[33]=z[33] + z[41] - z[61] + z[56] + 3*z[48] - z[62];
    z[41]=9*z[15];
    z[48]=4*z[13];
    z[56]=z[94] - z[41] - z[48];
    z[56]=z[14]*z[56];
    z[56]= - 4*z[31] + z[56];
    z[56]=z[14]*z[56];
    z[61]=z[34]*z[35];
    z[56]=z[61] + z[56];
    z[56]=z[56]*z[23];
    z[82]=z[106] + z[14];
    z[91]=2*z[15];
    z[125]= - z[91]*z[82];
    z[129]=z[28] - z[32];
    z[125]=5*z[129] + z[125];
    z[125]=z[125]*z[127];
    z[33]=z[125] + 2*z[33] + z[56];
    z[33]=z[3]*z[33];
    z[56]=n<T>(26,3)*z[8];
    z[125]=6*z[3];
    z[123]= - z[125] - z[56] + z[67] + z[123];
    z[123]=z[23]*z[123];
    z[63]=9*z[63] + z[123];
    z[63]=z[2]*z[63];
    z[123]=z[135] - z[100];
    z[129]= - n<T>(31,3)*z[112] - n<T>(22,3)*z[81] + z[123];
    z[129]=z[23]*z[129];
    z[63]=z[63] + z[129];
    z[63]=z[6]*z[63];
    z[129]= - 4*z[71] + 5*z[111];
    z[129]=z[8]*z[129];
    z[102]=z[23]*z[102];
    z[102]=z[129] + 4*z[102];
    z[120]=17*z[12] - z[120];
    z[120]=z[8]*z[120];
    z[90]= - z[90] + z[120];
    z[120]=15*z[15];
    z[129]=z[14]*z[120];
    z[129]= - n<T>(13,3)*z[32] + z[129];
    z[129]=z[129]*z[23];
    z[90]=2*z[90] + z[129];
    z[90]=z[3]*z[90];
    z[129]=z[3] + z[12];
    z[129]=z[40]*z[129];
    z[40]= - 2*z[40] + z[3];
    z[40]=z[2]*z[40];
    z[40]=z[40] + z[111] + z[129];
    z[40]=z[40]*z[60];
    z[40]=z[63] + z[40] + 3*z[102] + z[90];
    z[40]=z[6]*z[40];
    z[63]=z[64]*z[14];
    z[90]=z[13] - 4*z[63];
    z[90]=z[90]*z[105];
    z[102]=z[75]*z[65];
    z[102]= - n<T>(23,3) + z[102];
    z[102]=z[14]*z[102];
    z[102]=z[102] + 6*z[106];
    z[102]=z[3]*z[102];
    z[90]=z[102] + 3*z[101] - n<T>(11,3) + z[90];
    z[90]=z[2]*z[90];
    z[82]=z[82]*z[125];
    z[102]= - z[13] + 9*z[63];
    z[102]=z[14]*z[102];
    z[82]=z[82] - 6*z[101] - n<T>(5,3) + 2*z[102];
    z[82]=z[3]*z[82];
    z[102]= - z[98] + z[16] + z[86];
    z[111]=12*z[14];
    z[102]=z[111]*z[102];
    z[102]=z[102] + 1;
    z[102]=z[12]*z[102];
    z[101]= - z[94]*z[101];
    z[125]= - z[18] - z[93];
    z[82]=z[90] + z[82] + z[101] - n<T>(79,3)*z[8] + 2*z[125] - z[13] + 
    z[102];
    z[82]=z[2]*z[82];
    z[53]=z[56] + z[53] - 8*z[13] - z[103] - z[58];
    z[53]=z[8]*z[53];
    z[56]=z[35]*z[45];
    z[90]=z[12] - z[121] + z[13];
    z[90]=z[90]*z[92];
    z[20]= - z[20] + z[90];
    z[20]=z[20]*z[75];
    z[20]=z[56] + z[20];
    z[20]=z[20]*z[23];
    z[23]=z[67] - z[107];
    z[23]=2*z[23] + z[110];
    z[23]=z[10]*z[23];
    z[56]=5*z[13];
    z[79]=z[79] + z[56];
    z[90]=11*z[12];
    z[79]=2*z[79] - z[90];
    z[79]=z[79]*z[59];
    z[101]=z[57]*z[98];
    z[20]=z[26] + z[30] + z[21] + z[69] + z[40] + z[82] + z[33] + 6*
    z[20] + z[101] + z[53] + z[79] - 12*z[64] + 24*z[19] + z[23];
    z[20]=z[1]*z[20];
    z[21]= - z[107] - z[24];
    z[21]=z[10]*z[21];
    z[23]= - z[10] - z[12];
    z[23]=z[23]*z[34];
    z[26]= - z[2]*z[62];
    z[21]=z[26] + z[23] + z[72] + 18*z[19] + z[21];
    z[21]=z[5]*z[21];
    z[23]= - z[85] + z[45];
    z[26]=z[121] + z[56];
    z[26]=z[26]*z[92];
    z[23]=z[26] - z[65] + 2*z[23] - z[66];
    z[26]= - static_cast<T>(5)- 6*z[28];
    z[26]=z[11]*z[26];
    z[30]=static_cast<T>(5)+ 6*z[31];
    z[30]=z[13]*z[30];
    z[26]=z[50] + z[26] + z[30];
    z[26]=z[4]*z[26];
    z[30]= - 37*z[8] - 20*z[2];
    z[30]=z[30]*z[128];
    z[33]=z[11] + z[2];
    z[33]=z[33]*z[47];
    z[40]= - z[62] + z[12];
    z[40]=3*z[40] + n<T>(13,3)*z[8];
    z[40]=z[8]*z[40];
    z[23]=z[26] + z[33] + z[30] + 6*z[23] + z[40];
    z[23]=z[4]*z[23];
    z[26]=z[39] - z[67] + z[80];
    z[26]=z[2]*z[26];
    z[26]=z[26] + z[77] + n<T>(2,3)*z[81];
    z[26]=z[2]*z[26];
    z[30]=z[76]*z[57];
    z[30]=z[30] + z[78];
    z[30]=z[30]*z[132];
    z[33]= - z[10]*z[123];
    z[39]=z[12]*z[89];
    z[21]=2*z[30] + z[23] + z[21] + z[26] + z[33] + 48*z[39];
    z[21]=z[9]*z[21];
    z[23]=5*z[17];
    z[26]=6*z[37];
    z[30]= - z[23] - z[26];
    z[30]=z[11]*z[30];
    z[33]=6*z[36];
    z[23]=z[23] + z[33];
    z[23]=z[13]*z[23];
    z[23]=z[130] + z[23] + n<T>(4,3) + z[30];
    z[23]=z[4]*z[23];
    z[30]=z[43] - z[87];
    z[39]= - static_cast<T>(10)+ z[42];
    z[39]=z[13]*z[39];
    z[30]=7*z[12] + 2*z[30] + z[39];
    z[39]=n<T>(4,3)*z[5];
    z[40]= - static_cast<T>(19)- 13*z[32];
    z[40]=z[8]*z[40];
    z[23]=z[23] - z[39] + 9*z[126] + z[115] + 2*z[30] + z[40];
    z[23]=z[4]*z[23];
    z[30]=z[64]*z[97];
    z[40]=n<T>(16,3)*z[10];
    z[45]=z[18] - z[40];
    z[50]= - z[14]*z[55];
    z[50]=n<T>(7,3) + z[50];
    z[50]=z[2]*z[50];
    z[30]=z[50] + z[30] - 7*z[8] + 4*z[45] - z[55];
    z[30]=z[2]*z[30];
    z[45]= - z[71] + 5*z[19] + z[64];
    z[45]=z[45]*z[97];
    z[50]=z[87] + z[136];
    z[50]=z[12] + 4*z[50] - z[51];
    z[45]=n<T>(10,3)*z[83] + z[45] + 2*z[50] + z[68];
    z[45]=z[5]*z[45];
    z[50]= - z[70] - z[100];
    z[50]=z[10]*z[50];
    z[53]= - z[77] - z[81];
    z[55]= - 10*z[8] + 6*z[18] - n<T>(19,3)*z[10];
    z[55]=z[2]*z[55];
    z[53]=6*z[53] + z[55];
    z[53]=z[2]*z[53];
    z[50]=6*z[50] + z[53];
    z[50]=z[6]*z[50];
    z[49]= - z[19] - z[49];
    z[53]= - z[107] + z[11];
    z[53]=z[53]*z[93];
    z[55]= - z[16] + z[86];
    z[55]=4*z[55] - z[94];
    z[55]=z[55]*z[59];
    z[56]= - z[52] - z[93];
    z[56]=z[138] + 2*z[56] + 25*z[12];
    z[56]=z[8]*z[56];
    z[54]=z[54] - z[13];
    z[54]=z[12]*z[54];
    z[54]=4*z[64] + z[54];
    z[54]=z[14]*z[54]*z[57];
    z[21]=z[21] + z[23] + z[45] + z[50] + z[30] + z[54] + z[56] + z[55]
    - 36*z[64] + 6*z[49] + z[53];
    z[21]=z[9]*z[21];
    z[23]=z[14]*z[127];
    z[23]=n<T>(8,3) + z[23];
    z[23]=z[3]*z[23];
    z[30]= - n<T>(28,3) + 15*z[108];
    z[30]=z[2]*z[30];
    z[23]=z[30] + 5*z[23] + n<T>(85,3)*z[8] - z[88] - z[67] + z[58];
    z[23]=z[2]*z[23];
    z[29]= - static_cast<T>(1)- z[29];
    z[29]=z[11]*z[29];
    z[29]= - z[107] + z[29];
    z[29]=z[92] + 3*z[29] + z[62];
    z[30]=z[35]*z[8];
    z[45]= - z[17] - z[30];
    z[45]=z[8]*z[45];
    z[45]=z[28] + z[45];
    z[49]=z[30] - z[37];
    z[50]=2*z[3];
    z[53]= - z[49]*z[50];
    z[54]= - z[14]*z[87];
    z[45]=z[53] + 2*z[45] + z[54];
    z[45]=z[45]*z[127];
    z[53]=z[71]*z[97];
    z[54]=n<T>(5,3) - 2*z[32];
    z[54]=z[8]*z[54];
    z[29]=z[45] + z[53] + 2*z[29] + z[54];
    z[29]=z[3]*z[29];
    z[45]=z[19] - z[135];
    z[53]=2*z[95] + z[34];
    z[53]=z[8]*z[53];
    z[54]=z[127]*z[83];
    z[55]=6*z[11] - 4*z[18] - z[107];
    z[55]=z[10]*z[55];
    z[23]=z[54] + z[23] + z[29] + z[53] + z[72] + 6*z[45] + z[55];
    z[23]=z[6]*z[23];
    z[29]=static_cast<T>(11)- z[42];
    z[29]=z[29]*z[86];
    z[42]= - z[19] - z[64];
    z[42]=z[14]*z[42];
    z[29]=18*z[42] - z[92] - z[120] + z[29];
    z[29]=z[14]*z[29];
    z[26]= - z[17] - z[26];
    z[26]=z[26]*z[110];
    z[42]=z[35]*z[51];
    z[45]=2*z[17];
    z[42]=z[45] + z[42];
    z[42]=z[42]*z[86];
    z[45]=z[45] + z[61];
    z[45]=z[45]*z[99];
    z[53]=z[15]*z[75];
    z[49]=6*z[53] - z[49];
    z[49]=z[49]*z[127];
    z[53]=z[10]*z[17];
    z[26]=z[49] + z[29] + z[45] + z[42] + 4*z[53] - n<T>(58,3) + z[26];
    z[26]=z[3]*z[26];
    z[29]=z[19]*z[111];
    z[29]=z[29] + 22*z[12] + z[48] - z[119] + 43*z[15];
    z[29]=z[14]*z[29];
    z[42]= - z[84]*z[50];
    z[42]=z[75] + z[42];
    z[42]=z[42]*z[60];
    z[45]= - z[43] - z[12];
    z[45]=z[14]*z[45];
    z[45]= - n<T>(17,3) + 4*z[45];
    z[45]=z[14]*z[45];
    z[42]=z[42] + z[45] + n<T>(68,3)*z[106];
    z[42]=z[2]*z[42];
    z[45]= - z[16] - z[8];
    z[34]=z[45]*z[34];
    z[45]=z[44]*z[83];
    z[34]=z[45] + z[34] + n<T>(44,3)*z[112];
    z[34]=z[34]*z[46];
    z[45]= - 22*z[3] + 16*z[16] + z[124];
    z[34]=3*z[45] + z[34];
    z[34]=z[6]*z[34];
    z[45]= - z[14]*z[16];
    z[45]=z[45] + z[108];
    z[46]=z[113]*z[104];
    z[45]=2*z[45] + z[46];
    z[45]=z[6]*z[45];
    z[45]=z[45] + z[109];
    z[45]=z[45]*z[114];
    z[46]=n<T>(97,3) + z[117];
    z[46]=z[46]*z[108];
    z[29]=z[45] + z[34] + z[42] + 2*z[46] - n<T>(23,3) + z[29];
    z[29]=z[5]*z[29];
    z[25]=z[85] + z[25];
    z[25]=z[25]*z[27];
    z[25]=z[39] + z[25] + n<T>(13,3)*z[2] + z[94] + z[87] + n<T>(4,3)*z[10];
    z[25]=z[6]*z[25];
    z[34]=z[35]*z[110];
    z[30]= - 2*z[30] + z[34] - z[36];
    z[30]=z[30]*z[127];
    z[34]=7*z[17];
    z[33]=z[34] + z[33];
    z[33]=z[33]*z[86];
    z[34]=z[34] + z[61];
    z[34]=z[34]*z[116];
    z[35]=z[4]*z[38];
    z[36]= - 13*z[17] - 12*z[37];
    z[36]=z[11]*z[36];
    z[25]= - 6*z[35] + z[30] + z[34] + z[33] + n<T>(32,3) + z[36] + z[25];
    z[25]=z[4]*z[25];
    z[30]= - z[43] - z[86];
    z[33]= - z[74]*z[111];
    z[30]=z[33] + 2*z[30] - z[94];
    z[30]=z[14]*z[30];
    z[33]=z[14]*z[13];
    z[33]= - static_cast<T>(2)+ z[33];
    z[33]=z[33]*z[75]*z[127];
    z[34]=z[13]*z[97];
    z[34]=n<T>(31,3) + z[34];
    z[34]=z[14]*z[34];
    z[33]=z[34] + z[33];
    z[33]=z[2]*z[33];
    z[34]=z[51] - z[63];
    z[34]=z[34]*z[105];
    z[34]=n<T>(41,3) + z[34];
    z[34]=z[14]*z[34];
    z[34]=z[34] - 9*z[106];
    z[34]=z[34]*z[50];
    z[30]=z[33] + z[34] - n<T>(119,3) + z[30];
    z[30]=z[2]*z[30];
    z[33]= - z[19] - z[122];
    z[34]=z[92]*z[98];
    z[35]= - z[96] - z[12];
    z[35]=z[12]*z[35];
    z[33]=z[34] + 2*z[33] + z[35];
    z[33]=z[33]*z[97];
    z[34]=static_cast<T>(5)- 12*z[28];
    z[24]=z[34]*z[24];
    z[34]= - z[17]*z[67];
    z[34]= - n<T>(1,3) + z[34];
    z[34]=z[34]*z[62];
    z[35]=z[18] + z[73];
    z[36]= - n<T>(67,3) - 10*z[32];
    z[36]=z[8]*z[36];
    z[20]=z[20] + z[21] + z[25] + z[29] + z[23] + z[30] + z[26] + z[33]
    + z[36] + 21*z[12] - 12*z[13] + z[34] + z[24] + 2*z[35] - 25*z[15];
    z[20]=z[1]*z[20];
    z[21]=z[4]*z[6];
    z[21]=6*z[132] + 9*z[21] + 6;
    z[23]=z[22]*z[7];
    z[21]=z[23]*z[21];
    z[19]=z[71] - z[19];
    z[24]= - z[19]*z[97];
    z[24]=11*z[22] + z[24];
    z[25]=z[7]*z[14];
    z[24]=z[5]*z[24]*z[25];
    z[21]=z[24] + z[21];
    z[21]=z[9]*z[21];
    z[24]=z[22]*z[25];
    z[26]=z[23]*z[27];
    z[19]= - z[19]*z[105];
    z[19]=13*z[22] + z[19];
    z[19]=z[7]*z[19]*z[75];
    z[19]= - static_cast<T>(14)+ z[19];
    z[19]=z[19]*z[47];
    z[22]=z[104]*z[23];
    z[22]=static_cast<T>(6)+ z[22];
    z[22]=z[22]*z[118];
    z[27]=z[52] + z[15];
    z[19]=z[21] + z[22] + z[19] + z[26] + 18*z[24] - z[57] + z[48] + 
    z[40] + 2*z[27] - 19*z[11];
    z[19]=z[9]*z[19];
    z[21]=z[112]*z[6];
    z[21]=z[68] + n<T>(1,3)*z[21];
    z[22]=static_cast<T>(1)+ n<T>(1,3)*z[32];
    z[22]=z[3]*z[22];
    z[22]=z[22] - z[21];
    z[24]=4*z[6];
    z[22]=z[24]*z[22];
    z[22]=z[22] + n<T>(4,3);
    z[22]=z[7]*z[22];
    z[26]=z[18] - z[52];
    z[26]=2*z[26] + z[41];
    z[27]=4*z[7];
    z[29]= - n<T>(2,3)*z[17] - z[14];
    z[29]=z[29]*z[27];
    z[29]=n<T>(25,3) + z[29];
    z[29]=z[3]*z[29];
    z[22]=n<T>(26,3)*z[2] + z[29] + n<T>(31,3)*z[8] + z[59] + n<T>(20,3)*z[10] + 2*
    z[26] - 5*z[11] + z[22];
    z[22]=z[6]*z[22];
    z[26]=z[43] - z[91];
    z[26]=z[115] - z[90] + 18*z[26] - 11*z[13];
    z[26]=z[14]*z[26];
    z[27]=z[106]*z[27];
    z[29]=4*z[25];
    z[27]=z[27] + static_cast<T>(79)- z[29];
    z[21]=z[3] - z[21];
    z[21]=z[6]*z[21];
    z[21]=z[21] + n<T>(2,3);
    z[21]=z[7]*z[21];
    z[25]= - z[3]*z[25];
    z[21]=z[25] + z[21];
    z[21]=z[21]*z[24];
    z[21]=n<T>(1,3)*z[27] + z[21];
    z[21]=z[6]*z[21];
    z[24]=z[84]*z[23];
    z[21]=z[21] - n<T>(230,3)*z[14] + 9*z[24];
    z[21]=z[5]*z[21];
    z[23]=z[75]*z[23];
    z[24]=z[17] + z[14];
    z[24]=z[24]*z[29];
    z[24]=z[24] + 16*z[17] + 11*z[14];
    z[24]=z[24]*z[44];
    z[25]=n<T>(14,3)*z[10] + z[67];
    z[25]=z[17]*z[25];

    r += n<T>(29,3) + z[19] + z[20] + z[21] + z[22] + 15*z[23] + z[24] + 
      z[25] + z[26] - 7*z[28] + z[31] + 6*z[32];
 
    return r;
}

template double qg_2lNLC_r1389(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1389(const std::array<dd_real,31>&);
#endif
