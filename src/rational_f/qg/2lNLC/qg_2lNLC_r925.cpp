#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r925(const std::array<T,31>& k) {
  T z[88];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[12];
    z[10]=k[2];
    z[11]=k[3];
    z[12]=k[9];
    z[13]=k[14];
    z[14]=k[13];
    z[15]=npow(z[3],3);
    z[16]=3*z[15];
    z[17]=z[16]*z[2];
    z[18]=z[15]*z[10];
    z[17]=z[17] + z[18];
    z[17]=z[17]*z[2];
    z[19]=npow(z[10],2);
    z[20]=z[19]*z[15];
    z[17]=z[17] - z[20];
    z[21]=3*z[12];
    z[22]=z[19]*z[12];
    z[23]=n<T>(3,2)*z[10] - z[22];
    z[23]=z[23]*z[21];
    z[23]= - n<T>(1,2) + z[23];
    z[23]=z[12]*z[23];
    z[24]=z[15]*z[11];
    z[18]=z[18] - n<T>(3,2)*z[24];
    z[18]=z[11]*z[18];
    z[18]=z[18] - n<T>(1,4)*z[17] + z[23];
    z[18]=z[11]*z[18];
    z[17]=z[2]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[23]=npow(z[12],2);
    z[25]=z[19]*z[23];
    z[17]=n<T>(1,2)*z[17] - z[25];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[18]=n<T>(1,2)*z[11];
    z[26]=z[2]*z[6];
    z[27]=static_cast<T>(1)+ 7*z[26];
    z[27]=z[2]*z[27];
    z[27]=z[27] + z[18];
    z[28]=static_cast<T>(1)+ n<T>(5,2)*z[26];
    z[29]=npow(z[2],2);
    z[30]= - z[28]*z[29];
    z[31]=n<T>(1,4)*z[11];
    z[32]=z[2]*z[31];
    z[30]=z[30] + z[32];
    z[30]=z[4]*z[30];
    z[27]=n<T>(1,4)*z[27] + z[30];
    z[27]=z[4]*z[27];
    z[17]=n<T>(1,2)*z[17] + z[27];
    z[17]=z[5]*z[17];
    z[27]=n<T>(1,2)*z[6];
    z[30]=z[27]*z[20];
    z[32]=z[29]*z[15];
    z[33]= - z[6]*z[32];
    z[30]=z[30] + z[33];
    z[30]=z[2]*z[30];
    z[33]=n<T>(3,4)*z[8];
    z[30]=z[33] + z[30];
    z[34]=z[12]*z[10];
    z[35]=z[34] - 1;
    z[36]=z[35]*z[21];
    z[36]= - z[20] + z[36];
    z[37]=n<T>(1,2)*z[12];
    z[36]=z[36]*z[37];
    z[38]=z[34]*z[24];
    z[36]=z[36] + z[38];
    z[36]=z[36]*z[18];
    z[38]=z[24]*z[29];
    z[28]= - z[2]*z[28];
    z[28]=z[28] + z[31];
    z[28]=z[4]*z[28];
    z[28]=n<T>(1,8)*z[38] + z[28];
    z[28]=z[4]*z[28];
    z[39]=z[4]*z[11];
    z[40]=z[18]*z[12];
    z[41]=z[6] + n<T>(3,8)*z[8];
    z[41]=z[2]*z[41];
    z[41]=n<T>(3,8)*z[39] - z[40] - n<T>(1,2) + z[41];
    z[41]=z[9]*z[41];
    z[42]=3*z[22];
    z[43]=n<T>(13,2)*z[10] - z[42];
    z[43]=z[43]*z[37];
    z[43]= - static_cast<T>(1)+ z[43];
    z[43]=z[12]*z[43];
    z[17]=z[17] + z[41] + z[28] + z[36] + n<T>(1,2)*z[30] + z[43];
    z[17]=z[5]*z[17];
    z[28]=n<T>(3,2)*z[4];
    z[30]=z[23]*z[11];
    z[36]=n<T>(3,2)*z[30];
    z[41]=z[8] + z[6];
    z[43]= - z[28] + z[36] - n<T>(1,4)*z[41] - z[12];
    z[44]=z[31]*z[12];
    z[45]=n<T>(3,8)*z[6] + z[8];
    z[45]=z[2]*z[45];
    z[45]= - n<T>(1,8)*z[39] + z[44] - n<T>(3,8) + z[45];
    z[45]=z[9]*z[45];
    z[43]=n<T>(1,2)*z[43] + z[45];
    z[43]=z[9]*z[43];
    z[45]=z[18]*z[23];
    z[46]=z[20] + z[37];
    z[46]=z[46]*z[45];
    z[47]= - z[6] + z[32];
    z[48]=n<T>(1,2)*z[2];
    z[47]=z[47]*z[48];
    z[38]=z[47] - z[38];
    z[38]=z[4]*z[38];
    z[47]=npow(z[2],3);
    z[49]= - z[47]*z[16];
    z[49]=z[49] + 1;
    z[49]=z[6]*z[49];
    z[49]= - 3*z[13] + z[49];
    z[38]=n<T>(3,4)*z[49] + z[38];
    z[49]=n<T>(1,2)*z[4];
    z[38]=z[38]*z[49];
    z[50]=z[8]*z[10];
    z[51]= - static_cast<T>(3)+ z[50];
    z[51]=n<T>(1,2)*z[51] + z[34];
    z[51]=z[12]*z[51];
    z[51]= - z[33] + z[51];
    z[51]=z[12]*z[51];
    z[17]=z[17] + z[43] + z[38] + z[51] + z[46];
    z[17]=z[5]*z[17];
    z[38]=npow(z[3],4);
    z[43]=z[47]*z[38];
    z[46]= - z[38]*z[29];
    z[51]=z[38]*z[11];
    z[52]=z[2]*z[51];
    z[46]=z[46] + n<T>(5,2)*z[52];
    z[46]=z[11]*z[46];
    z[46]=n<T>(5,4)*z[43] + z[46];
    z[46]=z[11]*z[46];
    z[52]= - z[6] - 5*z[43];
    z[52]=z[2]*z[52];
    z[52]=static_cast<T>(1)+ z[52];
    z[53]=5*z[26];
    z[54]=static_cast<T>(1)+ z[53];
    z[54]=z[2]*z[54]*z[49];
    z[46]=z[54] + n<T>(1,4)*z[52] + z[46];
    z[46]=z[46]*z[49];
    z[52]=z[38]*z[6];
    z[54]= - z[19]*z[52];
    z[55]=3*z[26];
    z[56]=z[10]*z[6];
    z[57]=z[56] + z[55];
    z[57]=z[2]*z[38]*z[57];
    z[54]=z[54] + z[57];
    z[57]=n<T>(1,8)*z[29];
    z[54]=z[54]*z[57];
    z[57]= - z[38]*z[22];
    z[58]= - z[34]*z[51];
    z[57]=z[57] + z[58];
    z[58]=npow(z[11],2);
    z[59]=n<T>(1,4)*z[58];
    z[57]=z[57]*z[59];
    z[60]= - n<T>(3,4)*z[10] + z[22];
    z[60]=z[60]*z[23];
    z[61]=z[26]*z[9];
    z[46]=n<T>(1,8)*z[61] + z[46] + z[57] + z[54] + z[60];
    z[46]=z[5]*z[46];
    z[54]=z[38]*z[25]*z[59];
    z[57]=z[29]*z[51];
    z[57]= - n<T>(1,2)*z[43] + z[57];
    z[57]=z[11]*z[57];
    z[43]=5*z[6] + z[43];
    z[43]=z[2]*z[43];
    z[43]=static_cast<T>(1)+ z[43];
    z[43]=n<T>(1,2)*z[43] + z[57];
    z[57]=npow(z[4],2);
    z[59]=n<T>(1,2)*z[57];
    z[43]=z[43]*z[59];
    z[60]=n<T>(5,2)*z[10];
    z[62]=z[19]*z[8];
    z[62]= - z[60] + z[62];
    z[62]=z[62]*z[37];
    z[62]=z[62] + n<T>(1,2) - z[50];
    z[62]=z[62]*z[23];
    z[63]=z[41]*z[2];
    z[64]= - static_cast<T>(1)+ n<T>(3,4)*z[63];
    z[64]=z[9]*z[64];
    z[33]=z[33] + z[64];
    z[64]=n<T>(1,2)*z[9];
    z[33]=z[33]*z[64];
    z[33]=z[46] + z[33] + z[43] + z[62] + z[54];
    z[33]=z[5]*z[33];
    z[43]=npow(z[2],4);
    z[46]=z[43]*z[57]*z[52];
    z[52]=z[2]*z[8];
    z[54]=z[52]*z[64];
    z[62]=n<T>(3,2)*z[6];
    z[28]=z[54] - z[28] + z[62] + z[12];
    z[28]=z[9]*z[28];
    z[28]= - z[23] + z[28];
    z[54]=n<T>(1,4)*z[9];
    z[28]=z[28]*z[54];
    z[65]=n<T>(1,4) - z[50];
    z[65]=z[12]*z[65];
    z[65]=z[8] + z[65];
    z[65]=z[65]*z[23];
    z[28]=z[33] + z[28] + z[65] + n<T>(1,4)*z[46];
    z[28]=z[5]*z[28];
    z[33]=npow(z[7],3);
    z[46]=z[33]*z[38];
    z[65]=3*z[52];
    z[66]=z[7]*z[8];
    z[67]=5*z[66];
    z[68]= - z[65] + z[67];
    z[68]=z[68]*z[46];
    z[68]=9*z[8] + z[68];
    z[69]=npow(z[7],2);
    z[70]=z[69]*z[12];
    z[71]=n<T>(1,2)*z[70];
    z[72]=z[71] + z[7];
    z[73]=z[72]*z[12];
    z[74]=z[73] + n<T>(1,2);
    z[75]=z[12]*z[74];
    z[68]=n<T>(1,4)*z[68] + z[75];
    z[75]=z[12]*z[7];
    z[76]= - z[75]*z[31];
    z[76]= - z[70] + z[76];
    z[76]=z[58]*z[38]*z[76];
    z[77]=z[7]*z[31];
    z[77]=z[69] + z[77];
    z[77]=z[77]*z[51];
    z[77]= - n<T>(29,8)*z[46] + z[77];
    z[77]=z[11]*z[77];
    z[78]= - n<T>(19,2)*z[8] + 13*z[46];
    z[78]=z[7]*z[78];
    z[78]=static_cast<T>(5)+ z[78];
    z[79]= - static_cast<T>(1)+ z[66];
    z[79]=z[4]*z[7]*z[79];
    z[77]=n<T>(5,4)*z[79] + n<T>(1,4)*z[78] + z[77];
    z[77]=z[4]*z[77];
    z[68]=z[77] + n<T>(1,2)*z[68] + z[76];
    z[68]=z[9]*z[68];
    z[76]=n<T>(1,2)*z[69];
    z[76]=z[76]*z[23];
    z[77]=z[38]*z[76];
    z[51]=z[51]*z[7];
    z[78]= - z[23]*z[51];
    z[77]=z[77] + z[78];
    z[77]=z[77]*z[58];
    z[78]=z[23]*z[69];
    z[79]=z[78] - 1;
    z[79]=z[79]*z[12];
    z[80]=z[6]*z[79];
    z[77]=z[80] + z[77];
    z[80]=z[38]*z[69];
    z[51]= - n<T>(19,4)*z[80] + z[51];
    z[51]=z[11]*z[51];
    z[51]=n<T>(31,4)*z[46] + z[51];
    z[51]=z[11]*z[51];
    z[46]=5*z[8] - 21*z[46];
    z[46]=z[7]*z[46];
    z[46]= - static_cast<T>(1)+ z[46];
    z[46]=n<T>(1,4)*z[46] + z[51];
    z[46]=z[4]*z[46];
    z[51]=npow(z[7],4);
    z[38]=z[51]*z[38];
    z[80]= - static_cast<T>(9)- 25*z[38];
    z[80]=z[8]*z[80];
    z[80]=n<T>(1,2)*z[80] - z[12];
    z[46]=n<T>(1,4)*z[80] + z[46];
    z[46]=z[4]*z[46];
    z[46]=z[68] + n<T>(1,2)*z[77] + z[46];
    z[46]=z[9]*z[46];
    z[68]=11*z[8];
    z[38]=z[4]*z[38]*z[68];
    z[38]=z[23] + z[38];
    z[77]=n<T>(1,4)*z[4];
    z[38]=z[38]*z[77];
    z[38]=z[38] + z[46];
    z[38]=z[9]*z[38];
    z[46]=npow(z[12],3);
    z[80]=z[46]*z[8];
    z[81]= - z[19]*z[80];
    z[82]=npow(z[3],5);
    z[83]=z[82]*npow(z[11],3);
    z[84]=z[25]*z[83];
    z[81]=z[81] + z[84];
    z[84]=z[43]*z[82];
    z[85]= - z[11]*z[29];
    z[85]=n<T>(1,2)*z[47] + z[85];
    z[86]=z[82]*z[11];
    z[85]=z[85]*z[86];
    z[85]= - n<T>(1,2)*z[84] + z[85];
    z[85]=z[11]*z[85];
    z[84]= - z[6] + z[84];
    z[84]=z[84]*z[48];
    z[84]=z[84] + z[85];
    z[84]=z[4]*z[84];
    z[85]=z[82]*npow(z[2],5);
    z[87]= - static_cast<T>(1)- 5*z[85];
    z[87]=z[6]*z[87];
    z[84]=n<T>(1,4)*z[87] + z[84];
    z[84]=z[4]*z[84];
    z[61]=z[6] + z[61];
    z[54]=z[61]*z[54];
    z[54]=z[54] + n<T>(1,2)*z[81] + z[84];
    z[54]=z[5]*z[54];
    z[61]=z[46]*z[50];
    z[81]= - static_cast<T>(1)+ z[85];
    z[59]=z[59]*z[6]*z[81];
    z[81]=z[9]*z[52];
    z[41]=z[81] + z[41];
    z[81]=npow(z[9],2);
    z[41]=z[41]*z[81];
    z[41]=z[54] + n<T>(1,4)*z[41] + z[61] + z[59];
    z[41]=z[5]*z[41];
    z[54]=npow(z[9],3);
    z[59]=n<T>(1,2)*z[8];
    z[61]=z[54]*z[59];
    z[61]= - z[80] + z[61];
    z[41]=n<T>(1,2)*z[61] + z[41];
    z[41]=z[5]*z[41];
    z[61]=z[51]*z[82];
    z[80]=z[11]*z[69];
    z[80]= - n<T>(7,2)*z[33] + z[80];
    z[80]=z[80]*z[86];
    z[80]=n<T>(9,2)*z[61] + z[80];
    z[80]=z[11]*z[80];
    z[61]=z[8] - 5*z[61];
    z[84]=n<T>(1,2)*z[7];
    z[61]=z[61]*z[84];
    z[61]=z[61] + z[80];
    z[61]=z[4]*z[61];
    z[80]=z[82]*npow(z[7],5);
    z[82]= - static_cast<T>(3)- 7*z[80];
    z[82]=z[8]*z[82];
    z[61]=n<T>(1,4)*z[82] + z[61];
    z[61]=z[4]*z[61];
    z[82]= - z[70]*z[27];
    z[85]= - z[7]*z[6];
    z[82]=z[85] + z[82];
    z[82]=z[12]*z[82];
    z[82]= - z[27] + z[82];
    z[82]=z[12]*z[82];
    z[83]= - z[76]*z[83];
    z[61]=z[61] + z[82] + z[83];
    z[61]=z[9]*z[61];
    z[80]=z[8]*z[57]*z[80];
    z[61]=n<T>(5,2)*z[80] + z[61];
    z[61]=z[61]*z[81];
    z[54]=z[8]*npow(z[7],6)*z[54];
    z[80]=z[6]*npow(z[2],6)*npow(z[5],3);
    z[54]=z[54] + z[80];
    z[80]=n<T>(1,2)*z[1];
    z[54]=z[80]*z[57]*z[54]*npow(z[3],6);
    z[41]=z[54] + z[61] + z[41];
    z[41]=z[41]*z[80];
    z[54]=z[46]*z[59];
    z[28]=z[41] + z[28] + z[54] + z[38];
    z[28]=z[1]*z[28];
    z[38]=z[69]*z[15];
    z[41]=n<T>(13,4)*z[8] - 2*z[38];
    z[41]=z[7]*z[41];
    z[54]=z[72]*z[37];
    z[57]=z[74]*z[40];
    z[61]= - static_cast<T>(1)+ n<T>(1,2)*z[66];
    z[74]=z[61]*z[69];
    z[80]=z[11]*z[7];
    z[74]=5*z[74] + n<T>(9,4)*z[80];
    z[74]=z[4]*z[74];
    z[81]=static_cast<T>(7)- n<T>(23,4)*z[66];
    z[81]=z[7]*z[81];
    z[74]=z[74] + z[81] - n<T>(9,8)*z[11];
    z[74]=z[4]*z[74];
    z[81]= - static_cast<T>(7)- z[65];
    z[41]=z[74] + z[57] + z[54] + n<T>(1,4)*z[81] + z[41];
    z[41]=z[9]*z[41];
    z[54]=z[15]*z[52];
    z[16]=z[66]*z[16];
    z[16]= - n<T>(1,4)*z[54] + z[16];
    z[16]=z[16]*z[69];
    z[54]=z[27] + z[8];
    z[16]= - z[79] + n<T>(3,2)*z[54] + z[16];
    z[57]=n<T>(3,4)*z[70];
    z[74]=z[15]*z[57];
    z[79]= - z[75] - z[44];
    z[79]=z[79]*z[24];
    z[74]=z[74] + z[79];
    z[74]=z[11]*z[74];
    z[21]= - 29*z[38] - z[21];
    z[79]=z[15]*z[31];
    z[81]=z[15]*z[7];
    z[79]=z[81] + z[79];
    z[79]=z[11]*z[79];
    z[21]=n<T>(1,4)*z[21] + z[79];
    z[21]=z[11]*z[21];
    z[79]= - 13*z[8] + n<T>(73,4)*z[38];
    z[79]=z[7]*z[79];
    z[79]=n<T>(11,2) + z[79];
    z[82]= - static_cast<T>(6)+ z[67];
    z[82]=z[7]*z[82];
    z[82]=z[82] + z[11];
    z[82]=z[4]*z[82];
    z[21]=z[82] + n<T>(1,2)*z[79] + z[21];
    z[21]=z[4]*z[21];
    z[16]=z[41] + z[21] + n<T>(1,2)*z[16] + z[74];
    z[16]=z[9]*z[16];
    z[21]=z[45]*z[15];
    z[41]=z[81]*z[23];
    z[21]=z[21] - z[41];
    z[21]=z[21]*z[11];
    z[41]= - z[23]*z[38];
    z[41]=z[41] - z[21];
    z[41]=z[11]*z[41];
    z[74]=z[6]*z[75];
    z[74]= - z[27] + z[74];
    z[74]=z[12]*z[74];
    z[41]=z[74] + z[41];
    z[24]= - n<T>(17,2)*z[81] + z[24];
    z[24]=z[11]*z[24];
    z[24]=n<T>(41,2)*z[38] + z[24];
    z[24]=z[24]*z[18];
    z[38]=z[68] - 37*z[38];
    z[68]=n<T>(1,4)*z[7];
    z[38]=z[38]*z[68];
    z[24]=z[24] - static_cast<T>(1)+ z[38];
    z[24]=z[4]*z[24];
    z[38]=z[15]*z[33];
    z[38]= - 29*z[38] - 5;
    z[38]=z[8]*z[38];
    z[38]= - 9*z[14] + z[38];
    z[38]=n<T>(1,2)*z[38] + z[12];
    z[38]=n<T>(1,2)*z[38] - z[30];
    z[24]=n<T>(1,2)*z[38] + z[24];
    z[24]=z[4]*z[24];
    z[16]=z[16] + n<T>(1,2)*z[41] + z[24];
    z[16]=z[9]*z[16];
    z[24]=z[15]*z[76];
    z[21]=z[24] + z[21];
    z[21]=z[11]*z[14]*z[21];
    z[24]=z[59]*z[32];
    z[32]= - z[52] + n<T>(7,4)*z[66];
    z[32]=z[32]*z[81];
    z[24]=z[24] + z[32];
    z[24]=z[7]*z[24];
    z[15]=z[15]*z[47];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[6]*z[15];
    z[15]=n<T>(1,4)*z[15] + z[24];
    z[15]=z[4]*z[15];
    z[24]= - z[13] + z[14];
    z[24]=z[7]*z[24];
    z[24]=static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[37];
    z[32]=n<T>(3,2)*z[14];
    z[24]=z[24] - 2*z[13] - z[32];
    z[24]=z[12]*z[24];
    z[15]=z[24] + z[15];
    z[15]=z[4]*z[15];
    z[24]=z[7]*z[13];
    z[20]= - z[20]*z[24];
    z[20]=z[12] + z[20] - z[14] - z[8];
    z[20]=z[20]*z[23];
    z[15]=z[28] + z[17] + z[16] + z[15] + n<T>(1,2)*z[20] + z[21];
    z[15]=z[1]*z[15];
    z[16]=npow(z[3],2);
    z[17]=z[16]*z[10];
    z[20]=z[16]*z[2];
    z[21]=n<T>(1,2)*z[17] + z[20];
    z[28]=z[10]*z[46];
    z[28]= - 5*z[16] + z[28];
    z[28]=z[28]*z[18];
    z[38]= - 7*z[10] + z[42];
    z[38]=z[12]*z[38];
    z[38]=n<T>(1,2) + z[38];
    z[38]=z[12]*z[38];
    z[21]=z[28] + n<T>(1,2)*z[21] + z[38];
    z[21]=z[11]*z[21];
    z[28]=z[4] - z[12];
    z[28]=z[9]*z[28]*z[58];
    z[38]=z[16]*z[19];
    z[41]=n<T>(1,2)*z[38];
    z[74]=z[29]*z[16];
    z[53]=z[53] + 3;
    z[76]=z[53]*z[29];
    z[79]= - n<T>(3,2)*z[2] + z[11];
    z[79]=z[11]*z[79];
    z[76]=z[76] + z[79];
    z[76]=z[4]*z[76];
    z[76]=n<T>(7,4)*z[11] + z[76];
    z[76]=z[4]*z[76];
    z[21]= - z[28] + z[76] + z[21] + n<T>(1,2)*z[25] - z[74] + z[41] - static_cast<T>(1)+ 1.
   /4.*z[50];
    z[25]=z[53]*z[47];
    z[53]=z[11]*z[2];
    z[76]= - 3*z[29] + z[53];
    z[76]=z[76]*z[18];
    z[25]=z[25] + z[76];
    z[25]=z[4]*z[25];
    z[76]= - static_cast<T>(3)- n<T>(13,2)*z[26];
    z[76]=z[76]*z[29];
    z[79]=n<T>(3,4)*z[2] + z[11];
    z[79]=z[11]*z[79];
    z[25]=z[25] + z[76] + z[79];
    z[25]=z[25]*z[49];
    z[76]=n<T>(1,2) + z[55];
    z[76]=z[2]*z[76];
    z[76]= - z[22] + z[76] + n<T>(1,2)*z[10];
    z[79]=z[11]*z[12];
    z[81]= - n<T>(9,4)*z[10] + z[22];
    z[81]=z[12]*z[81];
    z[81]=n<T>(1,2) + z[81];
    z[81]=z[81]*z[79];
    z[42]= - z[10] + z[42];
    z[42]=z[12]*z[42];
    z[42]= - n<T>(1,2) + z[42];
    z[42]=n<T>(1,4)*z[42] + z[81];
    z[42]=z[11]*z[42];
    z[25]=z[25] + n<T>(1,4)*z[76] + z[42];
    z[25]=z[5]*z[25];
    z[21]=n<T>(1,2)*z[21] + z[25];
    z[21]=z[5]*z[21];
    z[25]= - 7*z[56] + 15*z[26];
    z[25]=z[48]*z[16]*z[25];
    z[42]= - z[27]*z[38];
    z[76]=7*z[13];
    z[81]=3*z[6];
    z[82]=n<T>(3,2)*z[8];
    z[25]=z[25] + z[42] - z[82] - z[76] + z[81];
    z[42]= - z[34] + n<T>(1,2) + z[38];
    z[42]=z[12]*z[42];
    z[25]=n<T>(1,2)*z[25] + z[42];
    z[42]= - z[23]*z[58];
    z[63]=static_cast<T>(1)+ 9*z[63];
    z[28]=z[28] - z[39] + n<T>(1,4)*z[63] + z[42];
    z[28]=z[28]*z[64];
    z[39]= - n<T>(1,4) + z[34];
    z[39]=z[12]*z[39];
    z[17]=n<T>(1,4)*z[17] + z[39];
    z[17]=z[12]*z[17];
    z[39]= - z[46]*z[31];
    z[17]=z[17] + z[39];
    z[17]=z[11]*z[17];
    z[39]=static_cast<T>(1)+ z[55];
    z[39]=z[4]*z[39];
    z[39]=z[39] - n<T>(7,2)*z[20] + z[76] - n<T>(15,2)*z[6];
    z[39]=z[2]*z[39];
    z[42]=5*z[11];
    z[63]=z[20]*z[42];
    z[39]=z[63] + static_cast<T>(1)+ z[39];
    z[39]=z[39]*z[77];
    z[17]=z[21] + z[28] + z[39] + n<T>(1,2)*z[25] + z[17];
    z[17]=z[5]*z[17];
    z[21]= - z[6] + z[82];
    z[21]=z[21]*z[74];
    z[25]=5*z[13];
    z[21]=z[21] + n<T>(11,4)*z[8] - z[6] + z[25] + z[32];
    z[28]=z[66]*z[16];
    z[39]=z[52]*z[16];
    z[63]= - z[39] - n<T>(3,8)*z[28];
    z[63]=z[7]*z[63];
    z[64]=z[16]*z[7];
    z[76]=z[11]*z[16];
    z[76]= - n<T>(5,2)*z[76] - 3*z[20] + n<T>(19,2)*z[64];
    z[76]=z[11]*z[76];
    z[20]= - n<T>(21,2)*z[64] + n<T>(13,2)*z[8] + 5*z[20];
    z[20]=z[7]*z[20];
    z[55]= - static_cast<T>(5)+ z[55];
    z[20]=z[76] + n<T>(1,2)*z[55] + z[20];
    z[20]=z[20]*z[49];
    z[20]=z[20] + 2*z[12] + n<T>(1,2)*z[21] + z[63];
    z[20]=z[4]*z[20];
    z[21]=n<T>(1,2) - z[78];
    z[21]=z[12]*z[21];
    z[21]=z[21] + z[36];
    z[21]=z[11]*z[21];
    z[27]=z[27] + 3*z[8];
    z[27]=z[2]*z[27];
    z[27]= - n<T>(11,2) + z[27];
    z[36]= - n<T>(19,4)*z[64] + 7*z[8] + n<T>(1,4)*z[6];
    z[36]=z[7]*z[36];
    z[21]=z[21] - z[78] + n<T>(1,2)*z[27] + z[36];
    z[27]= - static_cast<T>(3)+ z[66];
    z[27]=z[27]*z[33];
    z[36]=27*z[69];
    z[55]=z[36] - 7*z[80];
    z[55]=z[55]*z[18];
    z[27]=5*z[27] + z[55];
    z[27]=z[4]*z[27];
    z[36]= - z[61]*z[36];
    z[27]=z[27] + z[36] - n<T>(47,4)*z[80];
    z[27]=z[27]*z[49];
    z[36]=n<T>(17,2)*z[66] - static_cast<T>(11)- n<T>(7,2)*z[52];
    z[36]=z[7]*z[36];
    z[36]=z[36] + z[71];
    z[55]=z[72]*z[45];
    z[55]=z[73] + z[55];
    z[55]=z[11]*z[55];
    z[27]=z[27] + n<T>(1,2)*z[36] + z[55];
    z[27]=z[9]*z[27];
    z[36]=3*z[69];
    z[55]= - static_cast<T>(11)+ z[67];
    z[55]=z[55]*z[36];
    z[61]=39*z[7] - z[42];
    z[61]=z[61]*z[18];
    z[55]=z[55] + z[61];
    z[55]=z[55]*z[49];
    z[61]=static_cast<T>(5)- n<T>(17,4)*z[66];
    z[61]=z[7]*z[61];
    z[63]= - static_cast<T>(1)+ z[79];
    z[63]=z[11]*z[63];
    z[55]=z[55] + 3*z[61] + z[63];
    z[55]=z[4]*z[55];
    z[21]=z[27] + n<T>(1,2)*z[21] + z[55];
    z[21]=z[9]*z[21];
    z[27]=n<T>(3,2)*z[39] + z[28];
    z[27]=z[7]*z[27];
    z[28]=z[6] + n<T>(9,2)*z[8];
    z[27]=z[27] - z[28];
    z[55]=z[64] - 5*z[12];
    z[37]=z[55]*z[37];
    z[55]= - z[11]*z[46];
    z[37]=z[37] + z[55];
    z[37]=z[11]*z[37];
    z[55]=n<T>(3,2) - z[75];
    z[55]=z[12]*z[55];
    z[27]=z[37] + n<T>(1,2)*z[27] + z[55];
    z[37]=7*z[14];
    z[55]= - z[37] - n<T>(39,2)*z[8];
    z[55]=n<T>(1,4)*z[55] + 8*z[64];
    z[55]=z[7]*z[55];
    z[61]= - static_cast<T>(15)+ 11*z[66];
    z[61]=z[61]*z[68];
    z[61]=z[61] + z[11];
    z[61]=z[4]*z[61];
    z[63]= - n<T>(27,8)*z[64] - z[12];
    z[63]=z[11]*z[63];
    z[55]=3*z[61] + z[63] + n<T>(1,2) + z[55];
    z[55]=z[4]*z[55];
    z[21]=z[21] + n<T>(1,2)*z[27] + z[55];
    z[21]=z[9]*z[21];
    z[27]= - z[13]*z[16]*z[56];
    z[39]=z[14]*z[39];
    z[55]=z[6]*z[13];
    z[61]=3*z[55];
    z[63]=z[8]*z[14];
    z[71]= - z[61] + z[63];
    z[64]=z[71]*z[64];
    z[27]=n<T>(1,2)*z[64] + z[27] + z[39];
    z[27]=z[7]*z[27];
    z[39]=z[63]*z[74];
    z[39]= - z[39] - z[55] - z[63];
    z[38]=z[38]*z[55];
    z[38]=z[38] + 3*z[39];
    z[27]=n<T>(1,2)*z[38] + z[27];
    z[38]= - z[13]*z[41];
    z[39]= - n<T>(3,2)*z[24] + static_cast<T>(1)+ z[41];
    z[39]=z[12]*z[39];
    z[41]= - n<T>(3,2)*z[13] - z[14];
    z[38]=z[39] + 3*z[41] + z[38];
    z[38]=z[12]*z[38];
    z[39]=3*z[14] + z[12];
    z[39]=z[39]*z[23];
    z[16]= - z[16]*z[45];
    z[16]=z[39] + z[16];
    z[16]=z[16]*z[18];
    z[15]=z[15] + z[17] + z[21] + z[20] + z[16] + n<T>(1,4)*z[27] + z[38];
    z[15]=z[1]*z[15];
    z[16]=5*z[51];
    z[17]= - static_cast<T>(1)+ n<T>(1,4)*z[66];
    z[16]=z[17]*z[16];
    z[17]= - 7*z[69] + z[80];
    z[17]=z[11]*z[17];
    z[17]=n<T>(27,2)*z[33] + z[17];
    z[17]=z[17]*z[18];
    z[16]=z[16] + z[17];
    z[16]=z[4]*z[16];
    z[17]=static_cast<T>(11)- n<T>(31,8)*z[66];
    z[17]=z[17]*z[33];
    z[20]=z[7] + z[31];
    z[20]=z[11]*z[20];
    z[20]= - n<T>(67,8)*z[69] + z[20];
    z[20]=z[11]*z[20];
    z[16]=z[16] + z[17] + z[20];
    z[16]=z[4]*z[16];
    z[17]=n<T>(21,2)*z[66] - static_cast<T>(23)- n<T>(11,2)*z[52];
    z[17]=z[17]*z[69];
    z[20]= - z[7] + z[70];
    z[20]=z[12]*z[20];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[20]*z[44];
    z[21]= - z[7] + z[57];
    z[21]=z[12]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[11]*z[20];
    z[20]=z[57] + z[20];
    z[20]=z[11]*z[20];
    z[16]=z[16] + n<T>(1,4)*z[17] + z[20];
    z[16]=z[9]*z[16];
    z[17]= - static_cast<T>(16)+ z[67];
    z[17]=z[17]*z[33];
    z[20]= - 6*z[7] + z[18];
    z[20]=z[11]*z[20];
    z[20]=n<T>(33,2)*z[69] + z[20];
    z[20]=z[11]*z[20];
    z[17]=z[17] + z[20];
    z[17]=z[4]*z[17];
    z[20]=n<T>(85,2) - 21*z[66];
    z[20]=z[20]*z[69];
    z[21]= - n<T>(71,4)*z[7] - z[11];
    z[21]=z[11]*z[21];
    z[20]=z[20] + z[21];
    z[17]=n<T>(1,2)*z[20] + z[17];
    z[17]=z[4]*z[17];
    z[20]= - static_cast<T>(31)+ 33*z[66];
    z[20]=z[20]*z[68];
    z[20]=z[20] - z[70];
    z[21]= - static_cast<T>(1)+ 3*z[75];
    z[21]=z[21]*z[45];
    z[27]=5*z[7] - z[70];
    z[27]=z[12]*z[27];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[12]*z[27];
    z[21]=z[27] + z[21];
    z[21]=z[21]*z[18];
    z[27]=n<T>(9,4)*z[7] - z[70];
    z[27]=z[12]*z[27];
    z[21]=z[27] + z[21];
    z[21]=z[11]*z[21];
    z[16]=z[16] + z[17] + n<T>(1,2)*z[20] + z[21];
    z[16]=z[9]*z[16];
    z[17]=z[2]*z[28];
    z[20]= - z[6] - z[82];
    z[20]=z[7]*z[20];
    z[17]= - z[75] + z[20] + static_cast<T>(3)+ z[17];
    z[20]=n<T>(3,2) - 2*z[75];
    z[20]=z[20]*z[23];
    z[21]=z[46]*z[18];
    z[20]=z[20] + z[21];
    z[20]=z[11]*z[20];
    z[21]= - n<T>(3,4) - 4*z[75];
    z[21]=z[12]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[11]*z[20];
    z[21]=5*z[14];
    z[23]= - z[21] - 63*z[8];
    z[23]=z[23]*z[68];
    z[23]=static_cast<T>(19)+ z[23];
    z[23]=z[7]*z[23];
    z[27]= - static_cast<T>(13)+ n<T>(11,2)*z[66];
    z[27]=z[27]*z[36];
    z[28]=n<T>(53,2)*z[7] - z[42];
    z[28]=z[11]*z[28];
    z[27]=z[27] + z[28];
    z[27]=z[4]*z[27];
    z[23]=z[27] + z[23] + z[11];
    z[23]=z[23]*z[49];
    z[16]=z[16] + z[23] + n<T>(1,4)*z[17] + z[20];
    z[16]=z[9]*z[16];
    z[17]=static_cast<T>(7)+ n<T>(19,2)*z[26];
    z[17]=z[17]*z[47];
    z[20]= - n<T>(3,2)*z[29] - z[58];
    z[23]=3*z[11];
    z[20]=z[20]*z[23];
    z[17]=z[17] + z[20];
    z[20]=static_cast<T>(1)+ n<T>(5,4)*z[26];
    z[27]= - z[20]*z[43];
    z[28]= - z[29] + z[53];
    z[28]=z[11]*z[28];
    z[28]=n<T>(3,2)*z[47] + z[28];
    z[28]=z[28]*z[18];
    z[27]=z[27] + z[28];
    z[27]=z[4]*z[27];
    z[17]=n<T>(1,4)*z[17] + z[27];
    z[17]=z[4]*z[17];
    z[27]= - static_cast<T>(5)- z[56];
    z[27]=n<T>(1,8)*z[27] - z[26];
    z[27]=z[27]*z[29];
    z[28]=3*z[10];
    z[31]=z[28] - z[22];
    z[31]=z[31]*z[12];
    z[33]= - static_cast<T>(1)+ z[31];
    z[33]=z[33]*z[40];
    z[36]=z[10] - n<T>(3,2)*z[22];
    z[36]=z[12]*z[36];
    z[33]=z[33] + n<T>(3,2) + z[36];
    z[33]=z[11]*z[33];
    z[33]=z[33] + z[22] - n<T>(5,4)*z[10] + z[2];
    z[33]=z[33]*z[18];
    z[17]=z[17] + z[33] - n<T>(1,8)*z[19] + z[27];
    z[17]=z[5]*z[17];
    z[19]=z[60] - z[22];
    z[19]=z[12]*z[19];
    z[19]=n<T>(3,2) + z[19];
    z[19]=z[12]*z[19];
    z[27]= - z[35]*z[45];
    z[19]=z[19] + z[27];
    z[19]=z[11]*z[19];
    z[27]= - z[10] - z[22];
    z[27]=z[12]*z[27];
    z[19]=z[19] + n<T>(7,4) + z[27];
    z[19]=z[19]*z[18];
    z[20]= - z[20]*z[47];
    z[27]=n<T>(3,4)*z[29] - z[53];
    z[27]=z[11]*z[27];
    z[20]=z[20] + z[27];
    z[20]=z[4]*z[20];
    z[27]= - n<T>(5,4)*z[2] - z[11];
    z[18]=z[27]*z[18];
    z[18]=z[18] + z[20];
    z[18]=z[4]*z[18];
    z[20]=z[26] + static_cast<T>(1)+ n<T>(1,4)*z[56];
    z[20]=z[2]*z[20];
    z[22]=z[22] - z[10];
    z[17]=z[17] + z[18] + z[19] + z[20] + n<T>(1,4)*z[22];
    z[17]=z[5]*z[17];
    z[18]= - z[25] + z[81];
    z[18]=z[10]*z[18];
    z[18]= - static_cast<T>(7)+ z[18];
    z[18]=n<T>(1,2)*z[18] - z[50];
    z[19]=n<T>(1,2)*z[13];
    z[20]=z[19] - z[6];
    z[20]=5*z[20] + z[8];
    z[20]=z[2]*z[20];
    z[18]=z[31] + n<T>(1,2)*z[18] + z[20];
    z[20]= - z[25] + 21*z[6];
    z[20]=z[20]*z[48];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[20]*z[48];
    z[22]=n<T>(3,2)*z[26];
    z[26]= - static_cast<T>(1)- z[22];
    z[26]=z[26]*z[29];
    z[26]=z[26] + z[53];
    z[26]=z[4]*z[26];
    z[20]=z[26] + z[20] - z[23];
    z[20]=z[20]*z[49];
    z[26]=n<T>(3,4) - z[34];
    z[26]=z[26]*z[30];
    z[26]=n<T>(7,4)*z[12] + z[26];
    z[26]=z[11]*z[26];
    z[17]=z[17] + z[20] + n<T>(1,2)*z[18] + z[26];
    z[17]=z[5]*z[17];
    z[18]= - z[61] - z[63];
    z[18]=z[7]*z[18];
    z[20]= - z[56] + 13;
    z[20]=z[13]*z[20];
    z[26]=z[65] + n<T>(33,2);
    z[26]=z[14]*z[26];
    z[18]=z[18] - z[59] + z[6] + z[26] + z[20];
    z[20]=static_cast<T>(1)- z[24];
    z[20]=z[20]*z[34];
    z[24]= - z[13]*z[28];
    z[26]= - z[7]*z[32];
    z[20]=n<T>(3,2)*z[20] + z[26] + n<T>(1,4) + z[24];
    z[20]=z[12]*z[20];
    z[22]=static_cast<T>(1)- z[22];
    z[22]=z[22]*z[48];
    z[24]=n<T>(9,2)*z[66] - n<T>(17,2) - 2*z[52];
    z[24]=z[7]*z[24];
    z[26]=n<T>(7,2)*z[11];
    z[22]=z[26] + z[22] + z[24];
    z[22]=z[4]*z[22];
    z[24]=z[82] + z[13] - n<T>(9,4)*z[14];
    z[24]=z[24]*z[84];
    z[27]= - z[13] - n<T>(3,4)*z[14];
    z[27]= - 2*z[8] + n<T>(1,2)*z[27] + z[6];
    z[27]=z[2]*z[27];
    z[22]=z[22] + z[24] - n<T>(15,4) + z[27];
    z[22]=z[4]*z[22];
    z[24]= - z[14] - z[12];
    z[24]=z[24]*z[30];
    z[27]=z[7]*z[14];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[12]*z[27];
    z[27]=z[14] + z[27];
    z[27]=z[12]*z[27];
    z[24]=z[27] + z[24];
    z[24]=z[11]*z[24];
    z[15]=z[15] + z[17] + z[16] + z[22] + n<T>(3,2)*z[24] + n<T>(1,4)*z[18] + 
    z[20];
    z[15]=z[1]*z[15];
    z[16]=z[62] + z[19] + z[21];
    z[16]=z[7]*z[16];
    z[17]= - n<T>(5,2)*z[13] - z[37] - z[54];
    z[17]=z[2]*z[17];
    z[18]=z[11]*z[14];
    z[19]=z[25] + z[6];
    z[19]=z[10]*z[19];
    z[16]= - n<T>(15,2)*z[18] + z[16] + z[17] + n<T>(7,2) + z[19];
    z[17]= - z[23] - z[28] + n<T>(13,4)*z[2];
    z[17]=z[5]*z[17];
    z[18]=z[26] + z[2] - n<T>(3,4)*z[7];
    z[18]=z[4]*z[18];
    z[19]=z[9]*z[68];
    z[16]=z[17] + z[19] + n<T>(1,2)*z[16] + z[18];

    r += z[15] + n<T>(1,2)*z[16];
 
    return r;
}

template double qg_2lNLC_r925(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r925(const std::array<dd_real,31>&);
#endif
