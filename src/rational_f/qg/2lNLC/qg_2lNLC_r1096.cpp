#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1096(const std::array<T,31>& k) {
  T z[130];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[14];
    z[5]=k[19];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[11];
    z[9]=k[12];
    z[10]=k[15];
    z[11]=k[4];
    z[12]=k[5];
    z[13]=k[6];
    z[14]=k[13];
    z[15]=k[2];
    z[16]=k[3];
    z[17]=k[26];
    z[18]=k[29];
    z[19]=npow(z[13],5);
    z[20]=npow(z[14],3);
    z[21]=z[19]*z[20];
    z[22]=npow(z[6],2);
    z[23]=n<T>(3,2)*z[22];
    z[24]=npow(z[11],3);
    z[25]= - z[24]*z[23]*z[21];
    z[26]=npow(z[4],3);
    z[27]=z[26]*z[8];
    z[28]=z[27]*z[10];
    z[19]=z[19]*z[28];
    z[29]=2*z[8];
    z[30]=z[29]*z[6];
    z[21]=z[21]*z[30];
    z[19]=z[19] + z[21];
    z[19]=z[19]*npow(z[12],3);
    z[19]=z[25] + 2*z[19];
    z[19]=z[19]*npow(z[12],2);
    z[21]=6*z[5];
    z[25]=z[21]*z[26];
    z[31]=2*z[4];
    z[32]=z[31] + 5*z[5];
    z[33]=npow(z[4],2);
    z[34]=z[33]*z[3];
    z[35]= - z[32]*z[34];
    z[35]=z[25] + z[35];
    z[35]=z[2]*z[35];
    z[36]=z[3]*z[8];
    z[37]=z[8]*z[10];
    z[38]=z[10] + z[8];
    z[38]=z[9]*z[38];
    z[38]=z[38] - z[37] - z[36];
    z[39]=z[29] - z[3];
    z[40]=2*z[6];
    z[41]=z[39]*z[40];
    z[38]=3*z[38] + z[41];
    z[41]=2*z[7];
    z[38]=z[38]*z[41];
    z[42]=3*z[9];
    z[43]=z[42]*z[37];
    z[44]=z[29]*z[3];
    z[45]=z[44]*z[6];
    z[43]=z[43] - z[45];
    z[38]=5*z[43] + z[38];
    z[46]=npow(z[7],2);
    z[38]=z[38]*z[46];
    z[47]= - z[3]*z[27];
    z[48]=z[26]*z[3];
    z[49]=z[2]*z[48];
    z[47]=z[47] + z[49];
    z[47]=z[5]*z[47];
    z[43]= - z[43]*npow(z[7],3);
    z[43]=z[43] + z[47];
    z[43]=z[1]*z[43];
    z[47]=z[21]*z[27];
    z[49]=z[36]*z[33];
    z[32]=z[32]*z[49];
    z[19]=2*z[43] + z[19] + z[38] + z[35] - z[47] + z[32];
    z[19]=z[1]*z[19];
    z[32]=npow(z[13],4);
    z[35]=z[32]*z[6];
    z[38]=npow(z[11],2);
    z[43]=z[38]*z[35];
    z[50]=npow(z[2],2);
    z[51]=z[50]*z[42];
    z[43]= - 11*z[43] + z[51];
    z[51]=z[41]*z[32];
    z[52]=npow(z[16],2);
    z[53]=z[52]*z[51];
    z[43]=n<T>(1,2)*z[43] + z[53];
    z[43]=z[14]*z[43];
    z[53]=z[38]*z[32];
    z[54]= - z[23]*z[53];
    z[55]=z[46]*z[32];
    z[56]=z[52]*z[55];
    z[43]=z[43] + z[54] + z[56];
    z[54]=npow(z[14],2);
    z[43]=z[43]*z[54];
    z[56]=2*z[10];
    z[57]=z[56]*z[26];
    z[58]=5*z[10];
    z[59]= - z[58] + z[31];
    z[60]=z[33]*z[8];
    z[59]=z[60]*z[59];
    z[59]= - z[57] + z[59];
    z[59]=z[32]*z[59];
    z[61]=z[6]*z[8];
    z[62]=5*z[61];
    z[63]=z[32]*z[62];
    z[64]=z[32]*z[8];
    z[64]=z[64] + z[35];
    z[65]=2*z[14];
    z[66]= - z[64]*z[65];
    z[63]=z[63] + z[66];
    z[66]=2*z[54];
    z[63]=z[63]*z[66];
    z[59]=z[63] + z[59];
    z[59]=z[12]*z[59];
    z[63]=z[27]*z[11];
    z[67]=z[26]*z[10];
    z[68]=z[15]*z[67];
    z[68]=z[68] - z[63];
    z[68]=z[32]*z[68];
    z[64]=z[11]*z[64]*z[20];
    z[64]= - 2*z[64] + z[68];
    z[59]=2*z[64] + z[59];
    z[59]=z[12]*z[59];
    z[64]=npow(z[9],2);
    z[68]=z[64]*z[32]*z[52];
    z[69]=z[64]*z[10];
    z[70]=z[7]*z[69];
    z[68]=n<T>(1,2)*z[68] + z[70];
    z[68]=z[68]*z[46];
    z[53]=z[27]*z[53];
    z[43]=z[59] + z[43] + 4*z[53] + z[68];
    z[43]=z[12]*z[43];
    z[53]=z[32]*z[22]*z[24];
    z[59]=z[6]*z[17];
    z[68]=npow(z[17],2);
    z[70]=z[59] + z[68];
    z[70]=z[70]*z[6];
    z[71]=z[68]*z[9];
    z[72]=z[71] + z[70];
    z[53]=n<T>(1,2)*z[72] + z[53];
    z[73]=n<T>(1,2)*z[2];
    z[53]=z[53]*z[73];
    z[25]=z[25] + z[53];
    z[25]=z[2]*z[25];
    z[53]=4*z[7];
    z[74]=npow(z[16],3);
    z[32]= - z[32]*z[74]*z[53];
    z[35]=z[24]*z[35];
    z[75]= - z[2]*z[9];
    z[35]=z[35] + z[75];
    z[32]=3*z[35] + z[32];
    z[32]=z[14]*z[32];
    z[35]=z[74]*z[55];
    z[32]= - 2*z[35] + z[32];
    z[32]=z[32]*z[54];
    z[35]=z[9]*z[10];
    z[75]=z[37] - z[35];
    z[30]=3*z[75] + z[30];
    z[30]=z[7]*z[30];
    z[30]=z[69] + z[30];
    z[30]=z[30]*z[46];
    z[25]=z[43] + z[32] + 2*z[30] - z[47] + z[25];
    z[25]=z[12]*z[25];
    z[30]=3*z[37];
    z[32]=z[30] + z[44];
    z[43]=3*z[8];
    z[47]= - z[58] - z[43];
    z[47]=z[47]*z[42];
    z[69]=6*z[6];
    z[75]= - z[43] + z[3];
    z[75]=z[75]*z[69];
    z[76]=z[3]*z[15];
    z[77]= - static_cast<T>(2)+ z[76];
    z[40]=z[77]*z[40];
    z[77]=z[8]*z[16];
    z[78]= - static_cast<T>(1)+ z[77];
    z[78]=z[9]*z[78];
    z[78]=z[78] + z[8] + z[3];
    z[40]=3*z[78] + z[40];
    z[40]=z[40]*z[41];
    z[32]=z[40] + z[75] + 5*z[32] + z[47];
    z[32]=z[7]*z[32];
    z[40]=15*z[37];
    z[47]= - z[40] + z[35];
    z[47]=z[9]*z[47];
    z[75]=z[6]*z[36];
    z[32]=z[32] + z[47] + 10*z[75];
    z[32]=z[7]*z[32];
    z[47]=6*z[4];
    z[75]=z[47] + 11*z[5];
    z[78]= - z[75]*z[33];
    z[79]=z[4] + z[5];
    z[80]=z[3]*z[4];
    z[81]=5*z[80];
    z[82]=z[79]*z[81];
    z[48]=z[48]*z[11];
    z[83]=2*z[48];
    z[78]= - z[83] + z[78] + z[82] - n<T>(1,4)*z[72];
    z[78]=z[2]*z[78];
    z[82]=5*z[4];
    z[79]= - z[79]*z[36]*z[82];
    z[84]=n<T>(1,2)*z[17];
    z[85]=z[84] - z[8];
    z[86]=npow(z[3],2);
    z[85]=z[85]*z[86];
    z[87]=z[10]*z[17];
    z[88]=n<T>(9,4)*z[87] - z[36];
    z[88]=z[6]*z[88];
    z[89]=z[68]*z[10];
    z[90]=n<T>(13,4)*z[89];
    z[88]=z[88] + z[90] - z[85];
    z[88]=z[6]*z[88];
    z[91]=z[9]*z[17];
    z[92]= - z[68] - z[91];
    z[93]=n<T>(1,2)*z[9];
    z[92]=z[93]*z[92];
    z[94]=n<T>(1,2)*z[6];
    z[95]= - z[68]*z[94];
    z[92]=z[95] + z[92];
    z[92]=z[86]*z[92];
    z[44]=z[26]*z[44];
    z[44]=z[44] + z[92];
    z[44]=z[11]*z[44];
    z[92]=npow(z[16],4);
    z[55]=z[92]*z[55];
    z[51]=z[92]*z[51];
    z[51]=n<T>(3,2)*z[9] + z[51];
    z[51]=z[14]*z[51];
    z[51]=z[55] + z[51];
    z[51]=z[51]*z[54];
    z[54]=z[75]*z[60];
    z[55]=n<T>(13,2)*z[89];
    z[75]= - z[17]*z[86];
    z[75]=z[55] + z[75];
    z[86]=z[10]*z[91];
    z[75]=n<T>(1,2)*z[75] + z[86];
    z[75]=z[9]*z[75];
    z[19]=z[19] + z[25] + z[51] + z[32] + z[78] + z[44] + z[88] + z[75]
    + z[54] + z[79];
    z[19]=z[1]*z[19];
    z[25]=z[41] - z[9];
    z[32]=4*z[5];
    z[44]=z[32] + z[25];
    z[44]=z[2]*z[44];
    z[51]=z[6] - z[5];
    z[54]=4*z[8];
    z[51]=z[54]*z[51];
    z[75]=npow(z[13],3);
    z[78]=z[75]*z[11];
    z[79]=z[75]*z[16];
    z[44]=n<T>(7,2)*z[78] + n<T>(11,2)*z[79] + z[51] + z[44];
    z[44]=z[14]*z[44];
    z[51]=n<T>(3,4)*z[10];
    z[86]= - z[22]*z[51];
    z[88]= - z[75]*z[54];
    z[95]=z[75]*z[6];
    z[88]=z[88] - n<T>(7,2)*z[95];
    z[88]=z[11]*z[88];
    z[96]=4*z[9];
    z[97]= - z[51] - z[96];
    z[97]=z[97]*z[50];
    z[98]=z[56] - z[2];
    z[99]= - z[7]*z[98];
    z[99]=3*z[79] + z[99];
    z[99]=z[7]*z[99];
    z[100]= - z[10]*z[33];
    z[44]=z[44] + z[99] + z[97] + z[88] + z[100] + z[86];
    z[44]=z[14]*z[44];
    z[86]=z[22]*z[11];
    z[88]=z[75]*z[86];
    z[97]=4*z[10];
    z[99]= - z[7]*z[97];
    z[99]=z[79] + z[99];
    z[99]=z[99]*z[46];
    z[44]=z[44] + z[99] + z[57] - n<T>(3,2)*z[88];
    z[44]=z[14]*z[44];
    z[57]=z[68]*z[75];
    z[88]=n<T>(1,2)*z[75];
    z[99]=z[91]*z[88];
    z[99]=z[57] + z[99];
    z[99]=z[99]*z[42];
    z[100]=z[59]*z[88];
    z[57]=z[57] + z[100];
    z[100]=3*z[6];
    z[57]=z[57]*z[100];
    z[101]=z[75]*z[26];
    z[57]=z[57] + 4*z[101] + z[99];
    z[57]=z[11]*z[57];
    z[99]=3*z[10];
    z[101]=z[99] - z[31];
    z[101]=z[101]*z[33];
    z[102]=3*z[4];
    z[103]=z[58] - z[102];
    z[104]=z[8]*z[4];
    z[103]=z[104]*z[103];
    z[105]= - z[50]*z[93];
    z[103]=z[105] + z[101] + z[103];
    z[103]=z[75]*z[103];
    z[62]=z[75]*z[62];
    z[105]= - z[8]*z[75];
    z[105]=z[105] - z[95];
    z[105]=z[14]*z[105];
    z[62]=z[62] + 3*z[105];
    z[62]=z[62]*z[65];
    z[62]=z[62] + z[103];
    z[62]=z[12]*z[62];
    z[65]= - z[75]*z[15]*z[101];
    z[101]=z[10] - z[5];
    z[103]=z[101]*z[27];
    z[65]=z[65] + z[103];
    z[103]=n<T>(3,2)*z[70];
    z[105]=n<T>(3,2)*z[68] + z[79];
    z[105]=z[9]*z[105];
    z[105]=z[105] + z[103];
    z[105]=z[105]*z[73];
    z[106]=2*z[5];
    z[107]=z[26]*z[106];
    z[105]=z[107] + z[105];
    z[105]=z[2]*z[105];
    z[107]= - z[79] + z[35];
    z[107]=z[9]*z[107];
    z[108]=z[7]*z[9];
    z[109]=z[97] - z[9];
    z[110]=z[109]*z[108];
    z[107]=z[107] + z[110];
    z[107]=z[7]*z[107];
    z[110]=z[64]*z[79];
    z[107]=n<T>(5,2)*z[110] + z[107];
    z[107]=z[7]*z[107];
    z[44]=z[62] + z[44] + z[107] + z[105] + 2*z[65] + z[57];
    z[44]=z[12]*z[44];
    z[57]=z[52]*z[75];
    z[62]=5*z[79] + 11*z[78];
    z[62]=z[11]*z[62];
    z[25]= - 6*z[2] + z[62] - 7*z[57] - z[25];
    z[25]=z[14]*z[25];
    z[62]=z[38]*z[75];
    z[65]=n<T>(3,2)*z[6];
    z[105]=z[65]*z[62];
    z[107]= - z[7] - 3*z[57] + z[2];
    z[107]=z[7]*z[107];
    z[110]=6*z[61];
    z[111]= - z[8]*z[21];
    z[21]= - n<T>(15,4)*z[2] + z[21] + n<T>(13,2)*z[9];
    z[21]=z[2]*z[21];
    z[21]=z[25] + z[107] + z[21] + z[105] + z[110] - z[33] + z[111];
    z[21]=z[14]*z[21];
    z[25]= - z[50] - z[22];
    z[105]=n<T>(3,2)*z[10];
    z[25]=z[105]*z[25];
    z[107]= - z[10] + z[31];
    z[107]=z[107]*z[33];
    z[46]= - z[98]*z[46];
    z[21]=z[21] + z[46] + z[107] + z[25];
    z[21]=z[14]*z[21];
    z[25]=z[22]*z[38]*z[88];
    z[46]=n<T>(1,4)*z[6];
    z[88]=z[46] - z[17];
    z[88]=z[88]*z[6];
    z[88]=z[88] - z[91];
    z[98]=z[75]*z[51];
    z[98]=z[98] + z[95];
    z[38]=z[98]*z[38];
    z[38]=z[38] + z[88];
    z[38]=z[2]*z[38];
    z[98]=7*z[5];
    z[107]=z[98] + 10*z[4];
    z[111]= - z[107]*z[33];
    z[25]=z[38] + z[25] - z[103] + z[111] - n<T>(3,2)*z[71];
    z[25]=z[2]*z[25];
    z[38]=2*z[9];
    z[71]= - z[38] + 12*z[10] + z[57];
    z[71]=z[9]*z[71];
    z[71]=6*z[108] - 8*z[61] - 12*z[37] + z[71];
    z[71]=z[7]*z[71];
    z[64]=z[64]*z[56];
    z[64]=z[64] + z[71];
    z[64]=z[7]*z[64];
    z[71]=z[29]*z[33];
    z[103]= - z[8]*z[22];
    z[103]= - z[71] + z[103];
    z[62]=z[103]*z[62];
    z[103]=8*z[4];
    z[111]=z[103] + z[98] - z[10];
    z[111]=z[111]*z[60];
    z[112]=z[91]*z[56];
    z[55]=z[55] + z[112];
    z[55]=z[9]*z[55];
    z[112]=z[59]*z[10];
    z[89]=13*z[89] + 9*z[112];
    z[89]=z[89]*z[94];
    z[21]=z[44] + z[21] + z[64] + z[25] + z[62] + z[89] + z[111] + z[55]
   ;
    z[21]=z[12]*z[21];
    z[25]=z[34]*z[11];
    z[44]=z[3]*z[31];
    z[44]= - z[25] + z[33] + z[44];
    z[44]=z[11]*z[44];
    z[55]= - 11*z[79] - 3*z[78];
    z[62]=n<T>(1,2)*z[11];
    z[55]=z[55]*z[62];
    z[55]=z[55] - n<T>(9,2)*z[57] - z[9];
    z[55]=z[11]*z[55];
    z[57]=z[74]*z[75];
    z[64]=z[41]*z[16];
    z[55]= - z[64] + z[55] + static_cast<T>(5)+ n<T>(3,2)*z[57];
    z[55]=z[14]*z[55];
    z[78]=z[3]*z[16];
    z[79]=z[52]*z[3];
    z[89]=2*z[79];
    z[111]= - z[16] - z[89];
    z[111]=z[7]*z[111];
    z[111]=z[111] + static_cast<T>(1)- n<T>(3,2)*z[78];
    z[111]=z[7]*z[111];
    z[113]=n<T>(3,2)*z[3];
    z[114]=n<T>(5,2)*z[9];
    z[44]=z[55] + z[111] + n<T>(3,2)*z[2] + z[44] - z[114] - z[4] - z[113];
    z[44]=z[14]*z[44];
    z[55]=2*z[26];
    z[83]=z[83] - z[55] - 3*z[34];
    z[83]=z[11]*z[83];
    z[111]=n<T>(1,4)*z[3];
    z[115]=z[46] + z[106] - z[111];
    z[115]=z[2]*z[115];
    z[116]= - z[79]*z[53];
    z[117]=2*z[78];
    z[57]=z[116] - z[57] + z[117];
    z[57]=z[7]*z[57];
    z[57]=z[113] + z[57];
    z[57]=z[7]*z[57];
    z[116]=n<T>(1,2)*z[3];
    z[118]=z[29] + z[116];
    z[118]=z[6]*z[118];
    z[119]=z[29]*z[5];
    z[44]=z[44] + z[57] + z[115] + z[83] + z[118] + z[80] + z[33] - 
    z[119];
    z[44]=z[14]*z[44];
    z[57]= - z[5]*z[78];
    z[57]=z[57] - z[106] - z[82];
    z[57]=z[3]*z[57];
    z[83]=n<T>(3,2)*z[17];
    z[115]=z[83] + z[6];
    z[115]=z[115]*z[94];
    z[55]= - z[55] + z[34];
    z[48]=7*z[55] - 6*z[48];
    z[48]=z[11]*z[48];
    z[55]=z[75]*z[105];
    z[55]=z[55] - z[95];
    z[24]=z[55]*z[24];
    z[24]= - z[6] + z[24];
    z[24]=z[24]*z[73];
    z[55]=z[98] + 13*z[4];
    z[55]=z[4]*z[55];
    z[24]=z[24] + z[48] + z[115] + n<T>(3,4)*z[91] + z[55] + z[57];
    z[24]=z[2]*z[24];
    z[48]=z[52]*z[29];
    z[55]=4*z[16];
    z[48]=z[48] - 11*z[15] + z[55];
    z[48]=z[3]*z[48];
    z[57]=3*z[16];
    z[75]=z[52]*z[8];
    z[95]=z[57] - z[75];
    z[98]= - z[95]*z[38];
    z[48]=z[98] + static_cast<T>(5)+ z[48];
    z[48]=z[7]*z[48];
    z[98]=4*z[6];
    z[115]=static_cast<T>(3)- z[76];
    z[115]=z[115]*z[98];
    z[118]= - static_cast<T>(4)+ z[77];
    z[118]=z[3]*z[118];
    z[77]=static_cast<T>(9)- 5*z[77];
    z[77]=z[9]*z[77];
    z[48]=z[48] + z[73] + z[115] + z[77] - 9*z[8] + z[118];
    z[48]=z[7]*z[48];
    z[77]=7*z[10];
    z[115]=z[77] + z[43];
    z[115]= - z[9] + 2*z[115] + z[116];
    z[115]=z[9]*z[115];
    z[118]=z[54] - z[3];
    z[98]=z[118]*z[98];
    z[118]= - z[2]*z[116];
    z[48]=z[48] + z[118] + z[98] + z[115] - z[40] - 7*z[36];
    z[48]=z[7]*z[48];
    z[98]= - z[116] + z[84] - z[43];
    z[98]=z[3]*z[98];
    z[115]= - z[17] + z[10];
    z[115]=z[116] + n<T>(3,2)*z[115] - z[29];
    z[115]=z[6]*z[115];
    z[118]= - z[68] + n<T>(1,2)*z[87];
    z[98]=z[115] + n<T>(5,2)*z[118] + z[98];
    z[98]=z[6]*z[98];
    z[115]= - z[29]*z[34];
    z[115]=z[27] + z[115];
    z[118]=z[17] + z[18];
    z[120]=z[118]*z[3];
    z[121]=npow(z[18],2);
    z[122]=z[121] + z[120];
    z[122]=z[3]*z[122];
    z[123]=z[121]*z[10];
    z[122]=z[123] + z[122];
    z[122]=z[122]*z[93];
    z[45]=z[85] + z[45];
    z[45]=z[6]*z[45];
    z[85]=4*z[63];
    z[124]=z[3]*z[85];
    z[45]=z[124] + z[45] + 4*z[115] + z[122];
    z[45]=z[11]*z[45];
    z[115]=z[5] + z[102];
    z[115]=z[115]*z[29];
    z[122]=z[16]*z[5];
    z[122]= - static_cast<T>(2)+ z[122];
    z[122]=z[122]*z[36];
    z[115]=z[115] + z[122];
    z[115]=z[3]*z[115];
    z[122]=5*z[68];
    z[124]=z[18] + n<T>(5,2)*z[17];
    z[124]=z[10]*z[124];
    z[40]=z[40] - z[122] + z[124];
    z[118]=n<T>(1,2)*z[118] + z[3];
    z[118]=z[3]*z[118];
    z[124]= - z[17] - z[116];
    z[124]=z[9]*z[124];
    z[40]=z[124] + n<T>(1,2)*z[40] + z[118];
    z[40]=z[9]*z[40];
    z[107]= - z[107]*z[104];
    z[19]=z[19] + z[21] + z[44] + z[48] + z[24] + z[45] + z[98] + z[40]
    + z[107] + z[115];
    z[19]=z[1]*z[19];
    z[21]=z[114] - z[106] + z[105];
    z[21]=z[2]*z[21];
    z[24]=n<T>(3,4)*z[6];
    z[40]=z[24] - z[105] + 22*z[8];
    z[40]=z[6]*z[40];
    z[44]=z[41] - z[105] - z[2];
    z[44]=z[7]*z[44];
    z[45]=4*z[14];
    z[48]=z[45]*z[6];
    z[98]=npow(z[13],2);
    z[107]=2*z[98];
    z[115]= - z[4]*z[105];
    z[21]= - z[48] + z[44] + z[21] + z[40] + z[119] + z[107] + z[115];
    z[21]=z[14]*z[21];
    z[40]=z[54]*z[98];
    z[44]=z[33]*z[99];
    z[115]=n<T>(9,4)*z[6];
    z[118]= - z[10]*z[115];
    z[118]= - z[98] + z[118];
    z[118]=z[6]*z[118];
    z[119]=n<T>(7,4)*z[10] + z[96];
    z[119]=z[119]*z[50];
    z[124]=z[41] - z[10];
    z[125]=z[124]*z[41];
    z[126]=5*z[98];
    z[125]=z[126] + z[125];
    z[125]=z[7]*z[125];
    z[21]=z[21] + z[125] + z[119] + z[118] + z[44] - z[40];
    z[21]=z[14]*z[21];
    z[44]=2*z[91];
    z[24]= - 2*z[17] + z[24];
    z[24]=z[6]*z[24];
    z[24]=z[24] - n<T>(1,2)*z[98] - z[44];
    z[24]=z[2]*z[24];
    z[118]=n<T>(9,2)*z[68];
    z[119]= - z[118] + z[98];
    z[119]=z[119]*z[93];
    z[125]= - z[5]*z[33];
    z[24]=z[24] - n<T>(9,4)*z[70] + z[125] + z[119];
    z[24]=z[2]*z[24];
    z[50]=z[72]*z[50];
    z[50]=n<T>(1,4)*z[50];
    z[67]=z[29]*z[67];
    z[70]=z[54]*z[20]*z[6];
    z[67]=z[70] + z[67] + z[50];
    z[67]=z[12]*z[67];
    z[70]=z[98]*z[99];
    z[119]= - z[33]*z[97];
    z[70]=z[70] + z[119];
    z[70]=z[4]*z[70];
    z[119]=z[56]*z[98];
    z[125]=z[103] + z[5] - z[77];
    z[125]=z[4]*z[125];
    z[125]=z[107] + z[125];
    z[125]=z[4]*z[125];
    z[125]= - z[119] + z[125];
    z[125]=z[8]*z[125];
    z[127]=3*z[17];
    z[128]=z[127]*z[98];
    z[90]=z[128] - z[90];
    z[128]=n<T>(7,2)*z[98] + z[87];
    z[128]=z[9]*z[128];
    z[128]=z[128] - z[90];
    z[128]=z[9]*z[128];
    z[40]=n<T>(9,4)*z[112] + z[40] - z[90];
    z[40]=z[6]*z[40];
    z[90]=z[10] - z[9];
    z[90]=z[90]*z[38];
    z[112]=z[9]*z[16];
    z[129]= - static_cast<T>(4)- z[112];
    z[129]=z[129]*z[108];
    z[90]=z[90] + z[129];
    z[90]=z[7]*z[90];
    z[35]= - z[126] + z[35];
    z[35]=z[9]*z[35];
    z[35]=z[35] + z[90];
    z[35]=z[7]*z[35];
    z[21]=3*z[67] + z[21] + z[35] + z[24] + z[40] + z[128] + z[70] + 
    z[125];
    z[21]=z[12]*z[21];
    z[24]=z[98]*z[11];
    z[35]=z[24] - z[4];
    z[40]=z[98]*z[16];
    z[35]= - z[45] + z[7] + 4*z[2] - z[69] - z[114] - z[40] + n<T>(3,2)*
    z[35];
    z[35]=z[14]*z[35];
    z[67]=14*z[8];
    z[70]=z[65] - z[51] + z[67];
    z[70]=z[6]*z[70];
    z[90]= - z[10] + z[4];
    z[90]=z[90]*z[102];
    z[125]= - z[29] + z[65];
    z[125]=z[125]*z[24];
    z[126]=n<T>(7,2)*z[9];
    z[32]=n<T>(21,4)*z[2] - z[126] - z[32] + n<T>(11,4)*z[10];
    z[32]=z[2]*z[32];
    z[124]=2*z[2] - 4*z[40] + z[124];
    z[124]=z[7]*z[124];
    z[128]=z[5]*z[54];
    z[32]=z[35] + z[124] + z[32] + z[125] + z[70] + z[90] + z[128];
    z[32]=z[14]*z[32];
    z[35]=z[126] + z[58] + z[40];
    z[23]=z[98] + z[23];
    z[23]=z[23]*z[62];
    z[23]=z[23] + n<T>(1,2)*z[35] - z[6];
    z[23]=z[2]*z[23];
    z[35]=n<T>(15,2)*z[17] + z[40];
    z[35]=z[35]*z[93];
    z[51]=z[51] + z[6];
    z[51]=z[51]*z[24];
    z[70]=z[106] + 9*z[4];
    z[70]=z[4]*z[70];
    z[90]=n<T>(15,4)*z[17] - z[6];
    z[90]=z[6]*z[90];
    z[23]=z[23] + z[51] + z[90] + z[70] + z[35];
    z[23]=z[2]*z[23];
    z[35]=z[99] + 7*z[40];
    z[35]=n<T>(1,2)*z[35] - z[96];
    z[35]=z[9]*z[35];
    z[51]= - static_cast<T>(3)- z[112];
    z[51]=z[9]*z[51];
    z[70]=z[42]*z[16];
    z[90]=z[7]*z[70];
    z[51]=z[51] + z[90];
    z[51]=z[51]*z[41];
    z[35]=z[51] + z[110] + 9*z[37] + z[35];
    z[35]=z[7]*z[35];
    z[37]=z[98]*z[15];
    z[51]= - z[47] - 4*z[37] + z[10];
    z[51]=z[51]*z[31];
    z[90]=4*z[4];
    z[101]= - z[90] + z[101];
    z[101]=z[101]*z[29];
    z[37]=z[10]*z[37];
    z[37]=z[101] + n<T>(11,2)*z[37] + z[51];
    z[37]=z[4]*z[37];
    z[51]= - z[59] - z[91] - z[33];
    z[51]=z[107]*z[51];
    z[101]= - z[4] + n<T>(1,2)*z[10];
    z[101]=z[8]*z[98]*z[101];
    z[51]=z[101] + z[51];
    z[51]=z[11]*z[51];
    z[101]= - z[122] + n<T>(29,4)*z[87];
    z[44]= - z[44] + z[101];
    z[44]=z[9]*z[44];
    z[110]= - z[127] - z[8];
    z[110]=z[6]*z[110];
    z[101]=z[110] + z[101];
    z[101]=z[6]*z[101];
    z[21]=z[21] + z[32] + z[35] + z[23] + z[51] + z[101] + z[44] + z[37]
   ;
    z[21]=z[12]*z[21];
    z[23]=z[10]*z[18];
    z[30]= - z[120] - z[30] + 9*z[68] - z[23];
    z[32]=3*z[91];
    z[30]=n<T>(1,2)*z[30] + z[32];
    z[30]=z[9]*z[30];
    z[35]= - z[116] + z[83] + z[54];
    z[35]=z[6]*z[35];
    z[37]= - z[84] - z[8];
    z[37]=z[3]*z[37];
    z[35]=z[35] + z[118] + z[37];
    z[35]=z[6]*z[35];
    z[27]=6*z[27] - z[49];
    z[36]= - z[22]*z[36];
    z[27]=2*z[27] + z[36];
    z[27]=z[11]*z[27];
    z[36]=z[3]*z[18];
    z[36]=z[36] + z[121];
    z[37]=z[8]*z[82];
    z[37]=z[37] + z[36];
    z[37]=z[3]*z[37];
    z[27]=z[27] + z[35] + z[30] + z[37] + z[123] - 16*z[60];
    z[27]=z[11]*z[27];
    z[24]= - n<T>(1,4)*z[24] + z[94] - z[111] + z[10] - n<T>(1,2)*z[40];
    z[24]=z[11]*z[24];
    z[30]=z[52]*z[9];
    z[35]=z[30] + z[79];
    z[37]= - z[5]*z[35];
    z[37]=n<T>(5,2) + z[37];
    z[24]=n<T>(1,2)*z[37] + z[24];
    z[24]=z[2]*z[24];
    z[37]= - z[4]*z[116];
    z[37]=z[33] + z[37];
    z[34]= - z[119] + 9*z[34];
    z[34]=z[11]*z[34];
    z[34]=z[34] + 17*z[37] - n<T>(7,4)*z[22];
    z[34]=z[11]*z[34];
    z[37]=z[16]*npow(z[5],2);
    z[40]= - z[106] + n<T>(1,2)*z[37];
    z[44]=z[116] + z[40];
    z[44]=z[16]*z[44];
    z[44]=n<T>(11,4) + z[44];
    z[44]=z[3]*z[44];
    z[37]=z[37] - z[5];
    z[37]=z[37]*z[16];
    z[49]= - static_cast<T>(3)+ z[37];
    z[49]=z[49]*z[93];
    z[24]=z[24] + z[34] - z[115] + z[49] + z[44] - n<T>(21,2)*z[4] - 3*z[5]
    - z[56];
    z[24]=z[2]*z[24];
    z[34]=z[16] - z[15];
    z[44]=z[34]*z[78];
    z[49]= - z[52]*z[38];
    z[51]=11*z[16];
    z[44]=z[49] + z[51] - 10*z[44];
    z[44]=z[7]*z[44];
    z[49]=z[98]*z[52];
    z[82]=static_cast<T>(9)+ z[49];
    z[94]=z[9]*z[95];
    z[75]= - z[75] + 7*z[15] - 8*z[16];
    z[75]=z[3]*z[75];
    z[44]=z[44] + z[94] + n<T>(1,2)*z[82] + z[75];
    z[44]=z[7]*z[44];
    z[75]=z[93] - z[116] + z[43];
    z[75]=z[16]*z[75];
    z[75]= - static_cast<T>(7)+ z[75];
    z[75]=z[9]*z[75];
    z[43]= - z[10] + z[43];
    z[76]= - static_cast<T>(4)+ z[76];
    z[76]=z[6]*z[76];
    z[82]=z[16]*z[116];
    z[82]=static_cast<T>(1)+ z[82];
    z[82]=z[2]*z[82];
    z[43]=z[44] + z[82] + z[76] + z[75] + 2*z[43] - z[116];
    z[43]=z[7]*z[43];
    z[44]=z[11]*z[116];
    z[44]=z[44] + n<T>(3,2);
    z[44]=z[4]*z[44];
    z[75]=z[16]*z[107];
    z[44]= - n<T>(9,2)*z[6] + z[38] + z[3] + z[75] + z[44];
    z[44]=z[11]*z[44];
    z[75]=z[2]*z[11];
    z[76]=z[75] - z[78];
    z[82]=3*z[52];
    z[94]=z[74]*z[3];
    z[95]= - z[82] + 4*z[94];
    z[95]=z[7]*z[95];
    z[98]= - n<T>(3,2)*z[16] + z[79];
    z[95]=7*z[98] + z[95];
    z[95]=z[7]*z[95];
    z[98]=z[52]*z[7];
    z[101]=6*z[98];
    z[106]=z[101] - z[55] - 9*z[11];
    z[106]=z[14]*z[106];
    z[44]=z[106] + z[95] + z[44] - n<T>(11,2) + z[49] - n<T>(9,2)*z[76];
    z[44]=z[14]*z[44];
    z[25]=z[25] + z[33];
    z[49]= - z[3] - z[115];
    z[49]=z[6]*z[49];
    z[25]=z[49] + z[81] - 3*z[25];
    z[25]=z[11]*z[25];
    z[49]= - z[82] + 2*z[94];
    z[49]=z[49]*z[53];
    z[76]=7*z[16];
    z[49]=z[49] - z[76] + z[89];
    z[49]=z[7]*z[49];
    z[49]=z[49] - static_cast<T>(3)- 4*z[78];
    z[49]=z[7]*z[49];
    z[46]= - z[3] + z[46];
    z[46]=z[11]*z[46];
    z[46]= - n<T>(9,4)*z[75] - n<T>(3,2) + z[46];
    z[46]=z[2]*z[46];
    z[25]=z[44] + z[49] + z[46] + z[25] - n<T>(13,4)*z[6] - 3*z[3] + z[93];
    z[25]=z[14]*z[25];
    z[44]= - z[84] - z[56];
    z[37]= - static_cast<T>(3)- z[37];
    z[37]=z[8]*z[37];
    z[37]=z[114] - z[116] + 3*z[44] + n<T>(1,2)*z[37];
    z[37]=z[9]*z[37];
    z[44]=z[116]*z[6];
    z[46]= - z[15]*z[44];
    z[39]=z[46] - z[83] + z[10] - z[39];
    z[39]=z[6]*z[39];
    z[46]=z[10] + z[31];
    z[46]=z[46]*z[31];
    z[49]=z[5] + n<T>(5,2)*z[10];
    z[49]=3*z[49] + z[103];
    z[49]=z[8]*z[49];
    z[40]= - z[16]*z[40];
    z[40]= - n<T>(7,2) + z[40];
    z[40]=z[8]*z[40];
    z[40]=z[113] + z[40] - n<T>(1,2)*z[18];
    z[40]=z[3]*z[40];
    z[19]=z[19] + z[21] + z[25] + z[43] + z[24] + z[27] + z[39] + z[37]
    + z[40] + z[49] - n<T>(1,2)*z[23] + z[46];
    z[19]=z[1]*z[19];
    z[21]=z[2]*z[88];
    z[21]=z[21] - z[72];
    z[21]=z[2]*z[21];
    z[20]=z[20]*z[61];
    z[20]=12*z[20] + 6*z[28] + z[50];
    z[20]=z[12]*z[20];
    z[23]= - z[26]*z[97];
    z[24]= - 11*z[10] + z[47];
    z[24]=z[24]*z[60];
    z[25]= - z[8] - z[6];
    z[25]=z[25]*z[45];
    z[25]=13*z[61] + z[25];
    z[25]=z[25]*z[66];
    z[20]=z[20] + z[25] + z[21] + z[23] + z[24];
    z[20]=z[12]*z[20];
    z[21]=n<T>(7,2) + z[64];
    z[21]=z[7]*z[21];
    z[23]= - z[11]*z[48];
    z[21]=z[23] + z[21] + n<T>(3,4)*z[86] - z[67] - n<T>(39,4)*z[6];
    z[21]=z[14]*z[21];
    z[23]= - z[99] + 11*z[8];
    z[23]=2*z[23] + z[115];
    z[23]=z[6]*z[23];
    z[24]=static_cast<T>(3)+ z[64];
    z[24]=z[7]*z[24];
    z[24]= - z[99] + z[24];
    z[24]=z[24]*z[41];
    z[25]=n<T>(7,2)*z[4];
    z[27]=z[10]*z[25];
    z[28]= - z[38] - n<T>(7,2)*z[10];
    z[28]=z[2]*z[28];
    z[21]=z[21] + z[24] + z[28] + z[27] + z[23];
    z[21]=z[14]*z[21];
    z[23]= - static_cast<T>(2)- z[112];
    z[23]=z[23]*z[42];
    z[24]= - z[55] - z[30];
    z[24]=z[24]*z[108];
    z[23]=z[23] + z[24];
    z[23]=z[7]*z[23];
    z[24]=z[56] - z[9];
    z[24]=z[24]*z[42];
    z[23]=z[24] + z[23];
    z[23]=z[7]*z[23];
    z[24]=z[97]*z[15];
    z[27]=z[26]*z[24];
    z[28]=z[77] - z[47];
    z[28]=z[28]*z[104];
    z[37]= - 6*z[87] + n<T>(5,2)*z[68];
    z[39]= - z[91] - z[37];
    z[39]=z[9]*z[39];
    z[37]= - n<T>(3,2)*z[59] - z[37];
    z[37]=z[6]*z[37];
    z[40]=z[86] + z[9];
    z[40]= - z[6] - n<T>(1,4)*z[40];
    z[40]=z[2]*z[40];
    z[40]= - 3*z[88] + z[40];
    z[40]=z[2]*z[40];
    z[20]=z[20] + z[21] + z[23] + z[40] - z[85] + z[37] + z[39] + z[27]
    + z[28];
    z[20]=z[12]*z[20];
    z[21]=9*z[16];
    z[23]= - z[11]*z[69];
    z[23]= - static_cast<T>(7)+ z[23];
    z[23]=z[11]*z[23];
    z[23]=z[101] + z[21] + z[23];
    z[23]=z[14]*z[23];
    z[27]= - 11*z[6] - n<T>(15,4)*z[86];
    z[27]=z[11]*z[27];
    z[28]= - n<T>(19,2)*z[16] - 3*z[98];
    z[28]=z[7]*z[28];
    z[23]=z[23] + z[28] - n<T>(27,4) + z[27];
    z[23]=z[14]*z[23];
    z[27]= - 5*z[16] - 4*z[98];
    z[27]=z[7]*z[27];
    z[27]= - n<T>(13,2) + 3*z[27];
    z[27]=z[7]*z[27];
    z[23]=z[23] + z[27] - n<T>(7,2)*z[2] - n<T>(9,2)*z[86] - 12*z[6] + z[38] + 
    z[25] - 10*z[8];
    z[23]=z[14]*z[23];
    z[25]=3*z[26] - z[71];
    z[26]=7*z[68];
    z[27]=z[26] + 4*z[91];
    z[27]=z[9]*z[27];
    z[28]=z[127] + z[29];
    z[28]=z[6]*z[28];
    z[26]=z[26] + z[28];
    z[26]=z[6]*z[26];
    z[25]=12*z[63] + z[26] + 4*z[25] + z[27];
    z[25]=z[11]*z[25];
    z[26]= - n<T>(5,4)*z[86] - z[105] - z[6];
    z[26]=z[11]*z[26];
    z[27]= - n<T>(1,2) - z[70];
    z[26]=n<T>(1,2)*z[27] + z[26];
    z[26]=z[2]*z[26];
    z[27]=z[86] + n<T>(17,2)*z[10] - z[4];
    z[26]=z[26] + z[6] - z[96] - n<T>(1,2)*z[27];
    z[26]=z[2]*z[26];
    z[27]=z[52]*z[93];
    z[21]= - z[98] - z[21] + z[27];
    z[21]=z[21]*z[108];
    z[27]= - static_cast<T>(3)+ z[112];
    z[27]=z[27]*z[42];
    z[21]=z[27] + z[21];
    z[21]=z[7]*z[21];
    z[27]= - static_cast<T>(3)- z[24];
    z[27]=z[27]*z[31];
    z[27]=13*z[10] + z[27];
    z[27]=z[4]*z[27];
    z[28]= - 11*z[17] + 13*z[9];
    z[28]=z[28]*z[93];
    z[37]= - n<T>(9,2)*z[10] + z[90];
    z[37]=z[8]*z[37];
    z[38]= - n<T>(11,2)*z[17] + 6*z[8];
    z[38]=z[6]*z[38];
    z[20]=z[20] + z[23] + z[21] + z[26] + z[25] + z[38] + z[28] + z[27]
    + z[37];
    z[20]=z[12]*z[20];
    z[21]= - z[33]*z[54];
    z[23]= - z[36]*z[116];
    z[25]= - z[22]*z[29];
    z[26]=n<T>(1,2)*z[123];
    z[21]=z[25] + z[23] - z[26] + z[21];
    z[21]=z[11]*z[21];
    z[23]=n<T>(1,2)*z[15];
    z[23]=z[121]*z[23];
    z[25]=z[15]*z[18];
    z[27]= - static_cast<T>(1)+ z[25];
    z[27]=z[27]*z[116];
    z[23]=z[27] + z[23] - z[4];
    z[23]=z[3]*z[23];
    z[27]= - z[105] + z[90];
    z[27]=z[8]*z[27];
    z[26]=z[15]*z[26];
    z[28]= - z[127] - z[6];
    z[28]=z[6]*z[28];
    z[21]=z[21] + z[28] - z[32] + z[23] + z[27] + z[26] - 4*z[33];
    z[21]=z[11]*z[21];
    z[23]=static_cast<T>(9)+ z[78];
    z[23]=z[23]*z[116];
    z[22]= - 3*z[80] + z[22];
    z[22]=z[11]*z[22];
    z[26]=n<T>(1,2)*z[4];
    z[22]=z[22] + z[100] + z[23] + z[77] - z[26];
    z[22]=z[11]*z[22];
    z[23]=z[6] - z[58] + z[116];
    z[23]=z[11]*z[23];
    z[23]=z[78] + z[23];
    z[23]=z[11]*z[23];
    z[23]=z[23] - z[57] + z[35];
    z[23]=z[23]*z[73];
    z[22]=z[23] + z[22] + n<T>(3,2)*z[112] - n<T>(3,2) + z[117];
    z[22]=z[2]*z[22];
    z[23]=31*z[52] - 11*z[94];
    z[27]=3*z[74];
    z[28]=z[92]*z[3];
    z[29]=z[27] - 2*z[28];
    z[29]=z[7]*z[29];
    z[23]=n<T>(1,2)*z[23] + z[29];
    z[23]=z[7]*z[23];
    z[29]=z[51] + 6*z[11];
    z[29]=z[11]*z[29];
    z[32]=z[7]*z[74];
    z[29]= - 6*z[32] - z[52] + z[29];
    z[29]=z[14]*z[29];
    z[32]= - static_cast<T>(13)+ 7*z[78];
    z[33]= - z[3] + z[65];
    z[33]=z[11]*z[33];
    z[33]= - n<T>(1,2)*z[32] + z[33];
    z[33]=z[11]*z[33];
    z[23]=z[29] + z[23] + z[33] + n<T>(25,2)*z[16] - 6*z[79];
    z[23]=z[14]*z[23];
    z[29]=z[80] + z[44];
    z[29]=z[11]*z[29];
    z[26]= - z[26] - z[3];
    z[26]=z[29] + z[100] + 3*z[26] - z[9];
    z[26]=z[11]*z[26];
    z[27]=z[27] - z[28];
    z[27]=z[7]*z[27];
    z[27]=z[27] + 4*z[52] - z[94];
    z[27]=z[27]*z[53];
    z[28]=z[52]*z[116];
    z[27]=z[27] + z[76] + z[28];
    z[27]=z[7]*z[27];
    z[28]=n<T>(5,2)*z[3] - z[6];
    z[28]=z[28]*z[62];
    z[28]=static_cast<T>(4)+ z[28];
    z[28]=z[28]*z[75];
    z[29]=3*z[78];
    z[33]=static_cast<T>(29)- z[29];
    z[23]=z[23] + z[27] + z[28] + n<T>(1,4)*z[33] + z[26];
    z[23]=z[14]*z[23];
    z[26]= - z[15] + 2*z[16];
    z[26]=z[26]*z[29];
    z[27]= - 3*z[15] + z[55];
    z[27]=z[27]*z[79];
    z[27]= - 8*z[52] + z[27];
    z[27]=z[7]*z[27];
    z[26]=z[27] + n<T>(3,2)*z[30] - 13*z[16] + z[26];
    z[26]=z[7]*z[26];
    z[27]= - z[112] + z[32];
    z[26]=n<T>(1,2)*z[27] + z[26];
    z[26]=z[7]*z[26];
    z[27]= - z[34]*z[116];
    z[27]=z[27] + n<T>(5,2) - z[25];
    z[27]=z[3]*z[27];
    z[25]= - n<T>(15,2) - z[25];
    z[25]=z[10]*z[25];
    z[24]=static_cast<T>(7)+ z[24];
    z[24]=z[4]*z[24];
    z[28]=z[116]*z[15];
    z[28]= - static_cast<T>(1)- z[28];
    z[28]=z[6]*z[28];
    z[19]=z[19] + z[20] + z[23] + z[26] + z[22] + z[21] + z[28] + z[27]
    - n<T>(9,2)*z[8] + z[25] + z[24];
    z[19]=z[1]*z[19];
    z[20]=z[14] - z[65] + n<T>(9,4)*z[10] - z[31];
    z[20]=z[11]*z[20];
    z[21]= - z[15]*z[99];
    z[21]= - n<T>(5,2) + z[21];
    z[22]=z[15]*z[102];
    z[23]=z[16] + z[62];
    z[23]=z[2]*z[23];
    z[24]= - z[14] + n<T>(13,2)*z[4] - z[109];
    z[24]=z[12]*z[24];

    r += z[19] + z[20] + n<T>(1,2)*z[21] + z[22] + z[23] + z[24];
 
    return r;
}

template double qg_2lNLC_r1096(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1096(const std::array<dd_real,31>&);
#endif
