#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r599(const std::array<T,31>& k) {
  T z[32];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[12];
    z[6]=k[15];
    z[7]=k[2];
    z[8]=k[4];
    z[9]=k[7];
    z[10]=k[14];
    z[11]=k[5];
    z[12]=k[9];
    z[13]=k[13];
    z[14]=z[5]*z[12];
    z[15]=3*z[9];
    z[16]= - z[10]*z[15];
    z[16]=z[16] - z[14];
    z[17]=n<T>(1,2)*z[4];
    z[16]=z[16]*z[17];
    z[18]=z[5]*z[11];
    z[19]=z[18] + 1;
    z[20]=z[4]*z[7];
    z[21]=z[19] + z[20];
    z[22]=n<T>(1,2)*z[3];
    z[23]=z[21]*z[22];
    z[24]= - z[23] - z[5] - z[4];
    z[24]=z[3]*z[24];
    z[25]=z[4]*z[5];
    z[24]= - z[25] + z[24];
    z[24]=z[3]*z[24];
    z[16]=z[16] + z[24];
    z[16]=z[6]*z[16];
    z[24]= - z[1]*z[6];
    z[24]=z[24] + 1;
    z[25]=z[17]*z[5];
    z[24]=z[24]*z[25]*npow(z[3],3);
    z[26]= - z[5] - z[17];
    z[26]=z[3]*z[26];
    z[25]= - z[25] + z[26];
    z[26]=npow(z[3],2);
    z[25]=z[6]*z[25]*z[26];
    z[24]=z[25] + z[24];
    z[24]=z[1]*z[24];
    z[25]=npow(z[10],3);
    z[27]=2*z[9];
    z[28]=z[25]*z[27];
    z[27]=z[27]*z[8];
    z[25]=z[25]*z[27];
    z[29]=z[14]*z[13];
    z[25]=z[25] - n<T>(1,2)*z[29];
    z[25]=z[4]*z[25];
    z[30]=z[12]*z[13];
    z[31]=z[5]*z[13];
    z[31]=z[30] + z[31];
    z[17]= - z[31]*z[17];
    z[26]=z[5]*z[26];
    z[17]=z[17] + z[26];
    z[17]=z[3]*z[17];
    z[16]=z[24] + z[16] + z[17] + z[28] + z[25];
    z[17]=npow(z[2],2);
    z[16]=z[1]*z[17]*z[16];
    z[24]= - z[12] + n<T>(1,2)*z[5];
    z[25]=2*z[10];
    z[15]= - z[25] + z[15] + z[24];
    z[15]=z[4]*z[15];
    z[23]= - z[23] - 2*z[5] - n<T>(3,2)*z[4];
    z[23]=z[3]*z[23];
    z[26]=z[25]*z[9];
    z[14]=z[23] + z[15] - z[26] - z[14];
    z[14]=z[6]*z[14];
    z[15]=z[3]*z[19];
    z[15]= - z[4] + z[15];
    z[15]=z[15]*z[22];
    z[22]=z[4]*z[24];
    z[15]=z[15] + z[22] - z[31];
    z[15]=z[3]*z[15];
    z[22]= - z[9] + z[25];
    z[22]=z[22]*npow(z[10],2);
    z[23]= - z[8]*z[26];
    z[23]= - z[9] + z[23];
    z[23]=z[4]*z[10]*z[23];
    z[14]=z[14] + z[15] + z[23] + 2*z[22] - z[29];
    z[14]=z[14]*z[17];
    z[14]=z[14] + z[16];
    z[14]=z[1]*z[14];
    z[15]= - z[10]*z[7];
    z[15]=z[15] + 1;
    z[15]=z[11]*z[15];
    z[15]=z[7] + z[15];
    z[15]=z[15]*z[25];
    z[16]=n<T>(1,2)*z[9];
    z[22]= - z[11]*z[16];
    z[22]= - static_cast<T>(1)+ z[22];
    z[15]=3*z[22] + z[15];
    z[15]=z[10]*z[15];
    z[22]= - n<T>(3,2)*z[10] - z[12];
    z[22]=z[7]*z[22];
    z[22]=static_cast<T>(3)- z[27] + z[22];
    z[22]=z[4]*z[22];
    z[21]= - z[3]*z[21];
    z[19]= - z[12]*z[19];
    z[15]=z[21] + z[22] + z[15] + z[9] + z[19];
    z[15]=z[6]*z[15];
    z[19]= - z[18]*z[30];
    z[21]=z[9]*z[8];
    z[21]= - static_cast<T>(2)+ n<T>(3,2)*z[21];
    z[21]=z[10]*z[21];
    z[21]= - z[16] + z[21];
    z[21]=z[4]*z[21];
    z[22]= - z[11]*z[13];
    z[20]= - z[20] - static_cast<T>(1)+ z[22];
    z[20]=z[12]*z[20];
    z[18]= - z[13]*z[18];
    z[18]=z[18] + z[20];
    z[18]=z[3]*z[18];
    z[16]= - z[10]*z[16];
    z[15]=z[15] + z[18] + z[21] + z[16] + z[19];
    z[15]=z[15]*z[17];
    z[14]=z[15] + z[14];

    r += z[14]*npow(z[1],2);
 
    return r;
}

template double qg_2lNLC_r599(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r599(const std::array<dd_real,31>&);
#endif
