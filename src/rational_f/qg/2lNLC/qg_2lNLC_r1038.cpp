#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1038(const std::array<T,31>& k) {
  T z[87];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[19];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[10];
    z[9]=k[11];
    z[10]=k[15];
    z[11]=k[5];
    z[12]=k[6];
    z[13]=k[2];
    z[14]=k[3];
    z[15]=k[26];
    z[16]=k[4];
    z[17]=k[29];
    z[18]=z[9]*z[5];
    z[19]=npow(z[12],3);
    z[20]=3*z[19];
    z[21]=z[20]*z[16];
    z[22]=z[19]*z[14];
    z[23]=z[2]*z[5];
    z[24]=3*z[22] - z[18] + z[23] + z[21];
    z[25]=7*z[2];
    z[26]=2*z[10];
    z[27]=z[25] - z[26];
    z[28]= - z[15]*z[27];
    z[29]=2*z[7];
    z[30]=z[10]*z[29];
    z[24]=z[28] + 7*z[24] + z[30];
    z[24]=z[11]*z[24];
    z[21]= - z[21] - 5*z[22];
    z[21]=z[14]*z[21];
    z[21]=z[15] - z[9] + z[21];
    z[21]=7*z[21] + z[24];
    z[21]=z[11]*z[21];
    z[24]=z[16]*z[19];
    z[24]=z[24] + 2*z[22];
    z[28]=npow(z[14],2);
    z[24]=z[24]*z[28];
    z[30]=z[9]*z[16];
    z[31]=z[30] + 1;
    z[24]=z[24] + z[31];
    z[32]=z[29]*z[28];
    z[33]=z[8]*z[32];
    z[34]=z[15]*z[14];
    z[35]=z[14]*z[8];
    z[36]=2*z[35];
    z[37]=static_cast<T>(7)+ z[36];
    z[37]=z[37]*z[34];
    z[21]=z[21] + z[37] + 7*z[24] + z[33];
    z[24]=2*z[4];
    z[21]=z[21]*z[24];
    z[33]=z[19]*z[28];
    z[37]=5*z[15];
    z[38]=z[37] + z[26] + 15*z[33];
    z[38]=z[15]*z[38];
    z[39]=z[9] + z[6];
    z[40]=z[39]*z[11];
    z[41]=15*z[30];
    z[42]=z[6]*z[16];
    z[43]= - 22*z[40] - 8*z[42] - z[41];
    z[43]=z[19]*z[43];
    z[44]=z[2] + z[10];
    z[45]= - z[44]*z[37];
    z[45]= - 18*z[22] + z[45];
    z[45]=z[15]*z[45];
    z[46]=npow(z[7],2);
    z[47]=z[46]*z[10];
    z[43]=z[45] + 9*z[47] + z[43];
    z[43]=z[11]*z[43];
    z[45]=z[3]*z[2];
    z[48]=z[23] - z[45];
    z[49]=z[48] - z[18];
    z[50]=z[7]*z[10];
    z[50]=17*z[49] + z[50];
    z[38]=z[43] + 2*z[50] + z[38];
    z[38]=z[11]*z[38];
    z[43]=z[35] - 1;
    z[50]=z[37]*z[14];
    z[43]=z[43]*z[50];
    z[20]=z[20]*z[28];
    z[51]=8*z[8];
    z[52]= - z[51] + z[20];
    z[52]=z[14]*z[52];
    z[52]= - z[43] - static_cast<T>(14)+ z[52];
    z[52]=z[15]*z[52];
    z[53]=z[28]*z[7];
    z[54]=z[53]*z[8];
    z[55]=8*z[35];
    z[56]= - z[55] + 9*z[54];
    z[56]=z[7]*z[56];
    z[57]=z[3]*z[16];
    z[58]= - static_cast<T>(34)- 15*z[57];
    z[58]=z[9]*z[58];
    z[21]=z[21] + z[38] + z[52] + z[56] + 34*z[3] + z[58];
    z[21]=z[4]*z[21];
    z[38]=7*z[10];
    z[52]=npow(z[7],3);
    z[38]=z[52]*z[38];
    z[56]=npow(z[15],2);
    z[58]=5*z[56];
    z[22]= - z[22]*z[58];
    z[22]=z[38] + z[22];
    z[38]=z[9]*z[6];
    z[59]=z[38]*z[11];
    z[60]=z[19]*z[59];
    z[22]=2*z[22] + 37*z[60];
    z[22]=z[11]*z[22];
    z[20]= - z[10] + z[20];
    z[20]=z[20]*z[58];
    z[20]=z[22] + 7*z[47] + z[20];
    z[20]=z[11]*z[20];
    z[22]= - 9*z[35] + 7*z[54];
    z[22]=z[7]*z[22];
    z[47]=2*z[8];
    z[22]=z[47] + z[22];
    z[22]=z[22]*z[29];
    z[33]=z[47] - z[33];
    z[33]=z[14]*z[33];
    z[33]= - static_cast<T>(1)+ z[33];
    z[33]=z[33]*z[37];
    z[54]=4*z[8];
    z[33]=z[54] + z[33];
    z[33]=z[15]*z[33];
    z[60]=28*z[3];
    z[61]= - 33*z[5] + z[60];
    z[61]=z[9]*z[61];
    z[20]=z[21] + z[20] + z[33] + z[22] + 33*z[48] + z[61];
    z[20]=z[20]*z[24];
    z[21]= - z[40] - z[42] - z[30];
    z[22]=npow(z[12],4);
    z[21]=z[11]*z[22]*z[21];
    z[33]=z[28]*z[15];
    z[40]= - z[22]*z[33];
    z[21]=z[40] + z[21];
    z[21]=z[11]*z[21];
    z[40]=npow(z[14],3);
    z[61]=z[40]*z[22];
    z[62]=2*z[15];
    z[63]=z[61]*z[62];
    z[21]=z[21] + z[63] - z[49];
    z[21]=z[11]*z[21];
    z[49]=static_cast<T>(1)+ z[57];
    z[49]=z[9]*z[49];
    z[63]=z[22]*npow(z[14],4);
    z[64]= - z[15]*z[63];
    z[21]=z[21] + z[64] - z[3] + z[49];
    z[21]=z[4]*z[21];
    z[49]=z[56]*z[61];
    z[61]= - z[28]*z[58];
    z[64]=npow(z[11],2)*z[38];
    z[61]=z[61] + 36*z[64];
    z[22]=z[61]*z[22]*z[11];
    z[22]=10*z[49] + z[22];
    z[22]=z[11]*z[22];
    z[49]=z[3] - z[5];
    z[49]=z[49]*z[9];
    z[48]=z[49] + z[48];
    z[49]= - z[58]*z[63];
    z[21]=14*z[21] + z[22] - 34*z[48] + z[49];
    z[21]=z[4]*z[21];
    z[22]=z[18]*z[3];
    z[49]=z[45]*z[5];
    z[22]=z[22] - z[49];
    z[21]= - 33*z[22] + z[21];
    z[21]=z[21]*z[24];
    z[61]=z[8]*z[6];
    z[63]=z[3]*z[10];
    z[64]=z[61] - z[63];
    z[65]=2*z[6];
    z[66]=z[65] + z[3];
    z[67]=z[47] + z[10] - z[66];
    z[67]=z[9]*z[67];
    z[67]=z[67] + z[64];
    z[68]=7*z[7];
    z[67]=z[67]*z[68];
    z[69]=17*z[63];
    z[70]= - z[69] + 18*z[61];
    z[70]=z[9]*z[70];
    z[67]=z[70] + z[67];
    z[67]=z[67]*z[46];
    z[70]=z[11]*z[12];
    z[70]=z[38]*npow(z[70],5);
    z[48]=z[70] + z[48];
    z[48]=z[4]*z[48];
    z[48]=17*z[22] + 7*z[48];
    z[48]=z[48]*npow(z[4],2);
    z[64]= - z[9]*z[64]*z[52];
    z[22]= - z[22]*npow(z[4],3);
    z[22]=z[64] + z[22];
    z[22]=z[1]*z[22];
    z[22]=7*z[22] + z[67] + z[48];
    z[22]=z[1]*z[22];
    z[48]=z[8] + z[3];
    z[48]=z[48]*z[14]*z[9];
    z[64]=z[6]*z[13];
    z[67]= - static_cast<T>(2)- z[64];
    z[67]=z[8]*z[67];
    z[66]= - z[48] + z[67] + z[66];
    z[67]=14*z[7];
    z[66]=z[66]*z[67];
    z[69]=z[69] - 11*z[61];
    z[70]= - 51*z[8] + 20*z[3] - 34*z[10] + 63*z[6];
    z[70]=z[9]*z[70];
    z[66]=z[66] + 2*z[69] + z[70];
    z[66]=z[7]*z[66];
    z[69]=33*z[63] - 37*z[61];
    z[69]=z[9]*z[69];
    z[66]=z[69] + z[66];
    z[66]=z[66]*z[29];
    z[69]=z[26]*z[6];
    z[70]=7*z[63];
    z[71]=z[69] + z[70];
    z[71]=z[71]*z[56];
    z[72]=z[6] + z[10];
    z[73]=z[72]*z[9];
    z[74]=z[63] - z[73];
    z[75]=28*z[52];
    z[74]=z[11]*z[74]*z[75];
    z[21]=4*z[22] + z[21] + z[74] + z[66] + z[71];
    z[21]=z[1]*z[21];
    z[22]= - z[3]*z[67];
    z[22]=z[22] - 34*z[63] + 27*z[73];
    z[22]=z[22]*z[46];
    z[66]=7*z[3];
    z[71]=z[66] + z[65];
    z[74]=z[26] + z[2];
    z[76]=z[56]*z[74]*z[71];
    z[77]=2*z[57];
    z[78]=z[77] + 7*z[42];
    z[19]= - z[56]*z[19]*z[78];
    z[79]= - z[63]*z[75];
    z[19]=z[79] + z[19];
    z[19]=z[11]*z[19];
    z[19]=z[19] + 2*z[22] + z[76];
    z[19]=z[11]*z[19];
    z[22]=static_cast<T>(4)+ z[64];
    z[22]=z[22]*z[51];
    z[51]=z[8]*z[13];
    z[76]=3*z[51];
    z[79]=z[14]*z[3];
    z[79]=z[79] - static_cast<T>(2)+ z[76];
    z[79]=z[79]*z[67];
    z[80]=7*z[6];
    z[81]= - z[80] - 4*z[3];
    z[22]=z[79] + 15*z[48] + 5*z[81] + z[22];
    z[22]=z[7]*z[22];
    z[63]= - 11*z[63] + 5*z[61];
    z[79]=13*z[3];
    z[81]=37*z[8] - z[79] + 33*z[10] - 56*z[6];
    z[81]=z[9]*z[81];
    z[22]=z[22] + 3*z[63] + z[81];
    z[22]=z[22]*z[29];
    z[63]= - static_cast<T>(15)+ z[78];
    z[63]=z[8]*z[63];
    z[81]=14*z[3];
    z[82]=z[81] + 4*z[6];
    z[63]=z[63] + 5*z[10] - z[82];
    z[63]=z[15]*z[63];
    z[63]=z[63] + 7*z[61] + z[69] - z[70];
    z[63]=z[15]*z[63];
    z[69]=31*z[10];
    z[83]=26*z[5] - z[69];
    z[83]=z[3]*z[83];
    z[61]=z[83] + 39*z[61];
    z[61]=z[9]*z[61];
    z[19]=z[21] + z[20] + z[19] + z[63] + z[22] - 26*z[49] + z[61];
    z[19]=z[1]*z[19];
    z[20]=npow(z[12],2);
    z[21]=z[20]*z[16];
    z[22]=z[20]*z[14];
    z[60]=20*z[22] - z[60] + 5*z[21] - 8*z[6];
    z[60]=z[15]*z[60];
    z[61]=2*z[44] - 7*z[21];
    z[61]=z[6]*z[61];
    z[63]=2*z[21];
    z[83]= - 7*z[44] + z[63];
    z[83]=z[3]*z[83];
    z[60]=z[60] + z[61] + z[83];
    z[60]=z[15]*z[60];
    z[61]=z[7]*z[14];
    z[83]= - z[61]*z[66];
    z[83]=10*z[3] + z[83];
    z[83]=z[83]*z[29];
    z[70]=z[83] + z[70] - 19*z[73];
    z[70]=z[70]*z[29];
    z[73]=2*z[3];
    z[83]= - z[20]*z[73];
    z[84]=z[10] + 2*z[2];
    z[71]=z[84]*z[71];
    z[71]= - 15*z[20] + z[71];
    z[71]=z[15]*z[71];
    z[71]=z[83] + z[71];
    z[71]=z[15]*z[71];
    z[83]=z[45] + 30*z[38];
    z[83]=z[20]*z[83];
    z[75]=z[3]*z[75];
    z[71]=z[71] + z[75] + z[83];
    z[71]=z[11]*z[71];
    z[75]=z[21]*z[2];
    z[72]= - z[75]*z[72];
    z[26]=z[26] + 9*z[6];
    z[26]=z[26]*z[9];
    z[83]=z[21]*z[26];
    z[84]= - z[45]*z[22];
    z[60]=z[71] + z[60] + z[70] + z[84] + z[83] + z[72];
    z[60]=z[11]*z[60];
    z[70]=z[46] - z[20];
    z[25]=z[25] + z[37];
    z[25]=z[15]*z[25];
    z[71]=7*z[23];
    z[72]=27*z[6];
    z[83]=7*z[5] + z[72];
    z[83]=z[9]*z[83];
    z[25]=z[25] + z[83] - z[71] - 9*z[70];
    z[25]=z[11]*z[25];
    z[70]= - z[7] + 9*z[22] + 8*z[21] + 17*z[9];
    z[25]=z[25] + 2*z[70] - 9*z[15];
    z[25]=z[11]*z[25];
    z[70]=z[59] + z[9];
    z[62]= - z[62] - z[29] + 7*z[70];
    z[62]=z[11]*z[62];
    z[62]= - 7*z[31] + z[62];
    z[62]=z[11]*z[62];
    z[70]=z[36] - 3;
    z[32]= - z[70]*z[32];
    z[83]= - static_cast<T>(15)- 4*z[35];
    z[83]=z[83]*z[33];
    z[32]=z[62] + z[32] + z[83];
    z[32]=z[32]*z[24];
    z[62]=5*z[22];
    z[63]= - z[63] - z[62];
    z[63]=z[14]*z[63];
    z[70]=z[70]*z[53];
    z[83]=2*z[14];
    z[84]=z[83] - 9*z[70];
    z[84]=z[7]*z[84];
    z[85]=z[35] - 3;
    z[86]=z[85]*z[33];
    z[86]=33*z[14] + 10*z[86];
    z[86]=z[15]*z[86];
    z[25]=z[32] + z[25] + z[86] + z[84] - 15*z[31] + 4*z[63];
    z[25]=z[4]*z[25];
    z[31]= - z[20]*z[39];
    z[32]=z[2]*z[37];
    z[32]= - 4*z[20] + z[32];
    z[32]=z[15]*z[32];
    z[31]=z[32] + 15*z[31] - 14*z[52];
    z[31]=z[11]*z[31];
    z[32]= - z[71] + 13*z[45];
    z[39]=z[10] - z[2];
    z[39]=3*z[39] - z[62];
    z[52]=3*z[15];
    z[39]=z[39]*z[52];
    z[62]=2*z[5] - z[21];
    z[62]=7*z[62] + 19*z[6];
    z[62]=z[9]*z[62];
    z[63]= - z[68] + 9*z[10];
    z[63]=z[7]*z[63];
    z[31]=z[31] + z[39] + z[63] + 2*z[32] + z[62];
    z[31]=z[11]*z[31];
    z[32]=15*z[35];
    z[39]= - static_cast<T>(8)+ z[32];
    z[39]=z[14]*z[39];
    z[39]=z[39] - 14*z[70];
    z[39]=z[7]*z[39];
    z[32]=z[39] + static_cast<T>(4)+ z[32];
    z[32]=z[7]*z[32];
    z[39]=5*z[8] + 4*z[22];
    z[39]=z[14]*z[39];
    z[39]= - z[43] + static_cast<T>(2)+ z[39];
    z[39]=z[39]*z[52];
    z[43]=static_cast<T>(4)+ z[57];
    z[43]=z[9]*z[43];
    z[43]= - 3*z[3] + z[43];
    z[25]=z[25] + z[31] + z[39] + 7*z[43] + z[32];
    z[25]=z[25]*z[24];
    z[31]=14*z[5];
    z[32]=5*z[16];
    z[39]=z[32]*z[10];
    z[43]= - static_cast<T>(9)+ z[39];
    z[43]=z[3]*z[43];
    z[62]= - static_cast<T>(25)- 9*z[42];
    z[62]=z[8]*z[62];
    z[43]=z[62] + z[43] + 54*z[6] + z[31] - z[69];
    z[43]=z[9]*z[43];
    z[22]=z[54] - z[22];
    z[22]=z[14]*z[22];
    z[54]=z[44]*z[16];
    z[22]=z[22] - static_cast<T>(6)- z[54];
    z[62]= - z[8]*z[32];
    z[22]=z[62] + z[78] + 5*z[22];
    z[22]=z[15]*z[22];
    z[42]= - static_cast<T>(5)+ z[42];
    z[42]=7*z[42] - z[77];
    z[42]=z[8]*z[42];
    z[22]=z[22] + z[42] + z[73] - z[27];
    z[22]=z[15]*z[22];
    z[42]=z[13]*z[20];
    z[42]=z[42] + z[2];
    z[62]=z[10]*z[17];
    z[42]=z[62]*z[42];
    z[63]=z[2]*z[10];
    z[68]= - z[62] - z[63];
    z[68]=z[68]*z[21];
    z[42]=z[68] + z[42];
    z[42]=z[16]*z[42];
    z[68]=z[17]*z[13];
    z[20]=z[20]*z[68];
    z[21]= - z[21] + z[2];
    z[21]=z[17]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[16]*z[20];
    z[21]= - static_cast<T>(2)+ z[68];
    z[21]=z[6]*z[21];
    z[57]=z[17]*z[57];
    z[20]=z[57] + z[20] + z[21];
    z[20]=z[8]*z[20];
    z[21]= - 2*z[23] + z[75];
    z[21]=z[8]*z[21];
    z[23]= - 5*z[3] + z[47];
    z[18]=z[23]*z[18];
    z[18]=z[18] + 5*z[49] + z[21];
    z[18]=z[14]*z[18];
    z[21]=z[36] - static_cast<T>(2)- z[76];
    z[21]=z[21]*z[61];
    z[23]= - z[13]*z[47];
    z[23]=static_cast<T>(1)+ z[23];
    z[49]= - 15*z[3] + z[47];
    z[49]=z[14]*z[49];
    z[21]=14*z[21] + 8*z[23] + z[49];
    z[21]=z[7]*z[21];
    z[21]=z[21] - 7*z[48] - 21*z[8] + 14*z[6] + z[79];
    z[21]=z[21]*z[29];
    z[23]=z[62]*z[64];
    z[48]=z[62]*z[16];
    z[49]=10*z[10] + 13*z[2];
    z[49]=2*z[49] + z[48];
    z[49]=z[3]*z[49];
    z[31]= - z[31] + z[10];
    z[31]=z[2]*z[31];
    z[18]=z[19] + z[25] + z[60] + z[22] + z[21] + z[18] + z[43] + z[20]
    + z[49] + z[23] + z[31] + z[42];
    z[18]=z[1]*z[18];
    z[19]=7*z[45];
    z[20]=z[65]*z[2];
    z[21]=5*z[2] - z[82];
    z[21]=z[15]*z[21];
    z[21]=z[21] + z[20] - z[19];
    z[21]=z[15]*z[21];
    z[22]=z[61] + 1;
    z[23]=z[22]*z[46];
    z[25]=z[3]*z[23];
    z[19]=z[20] + z[19];
    z[19]=z[11]*z[19]*z[56];
    z[19]=z[19] + 28*z[25] + z[21];
    z[19]=z[11]*z[19];
    z[20]= - static_cast<T>(6)+ z[54];
    z[20]=5*z[20] - z[78];
    z[20]=z[15]*z[20];
    z[20]=z[20] - z[80] + z[27];
    z[20]=z[15]*z[20];
    z[21]=static_cast<T>(8)+ 15*z[61];
    z[21]=z[29]*z[3]*z[21];
    z[25]=z[6]*z[2];
    z[19]=z[19] + z[20] + z[21] + 5*z[26] - 6*z[45] + z[63] + z[25];
    z[19]=z[11]*z[19];
    z[20]=9*z[7];
    z[21]= - z[22]*z[20];
    z[22]= - static_cast<T>(9)+ z[50];
    z[22]=z[15]*z[22];
    z[21]=63*z[59] + z[22] + z[21] - z[72] - 34*z[9];
    z[21]=z[11]*z[21];
    z[22]= - z[28]*z[37];
    z[25]=3*z[14];
    z[22]=z[25] + z[22];
    z[22]=z[15]*z[22];
    z[26]=26*z[14] + 27*z[53];
    z[26]=z[7]*z[26];
    z[21]=z[21] + 6*z[22] + z[26] + static_cast<T>(7)+ z[41];
    z[21]=z[11]*z[21];
    z[22]=7*z[11];
    z[26]=z[11]*z[65];
    z[26]=z[26] - 1;
    z[26]=z[9]*z[26];
    z[26]= - z[6] + z[26];
    z[26]=z[26]*z[22];
    z[27]=7*z[30];
    z[30]= - z[14]*z[29];
    z[26]=z[26] - 2*z[34] + z[27] + z[30];
    z[26]=z[11]*z[26];
    z[30]=35*z[14];
    z[26]=z[26] - 15*z[33] + z[30] + 6*z[53];
    z[26]=z[11]*z[26];
    z[31]=z[85]*z[40];
    z[33]=z[29]*z[31];
    z[36]=static_cast<T>(15)+ z[36];
    z[36]=z[36]*z[40]*z[15];
    z[26]=z[26] + z[36] - 21*z[28] + z[33];
    z[26]=z[26]*z[24];
    z[33]= - static_cast<T>(25)+ z[55];
    z[33]=z[33]*z[28];
    z[20]=z[31]*z[20];
    z[20]=z[33] + z[20];
    z[20]=z[7]*z[20];
    z[33]= - static_cast<T>(45)+ z[55];
    z[33]=z[33]*z[28];
    z[36]=z[40]*z[37];
    z[40]=static_cast<T>(6)- z[35];
    z[40]=z[40]*z[36];
    z[33]=z[33] + z[40];
    z[33]=z[15]*z[33];
    z[20]=z[26] + z[21] + z[33] + z[30] + z[20];
    z[20]=z[4]*z[20];
    z[21]= - static_cast<T>(2)- z[50];
    z[21]=z[21]*z[52];
    z[23]=4*z[38] - z[23];
    z[23]=14*z[23] + z[58];
    z[23]=z[11]*z[23];
    z[26]= - z[6] - 2*z[9];
    z[30]=31*z[14] + 42*z[53];
    z[30]=z[7]*z[30];
    z[30]= - static_cast<T>(2)+ z[30];
    z[30]=z[7]*z[30];
    z[21]=z[23] + z[21] + 14*z[26] + z[30];
    z[21]=z[11]*z[21];
    z[23]= - static_cast<T>(20)+ 3*z[35];
    z[23]=z[23]*z[28];
    z[26]=z[67]*z[31];
    z[23]=z[23] + z[26];
    z[23]=z[7]*z[23];
    z[26]=static_cast<T>(7)- 6*z[35];
    z[26]=z[26]*z[83];
    z[23]=z[26] + z[23];
    z[23]=z[7]*z[23];
    z[26]= - 12*z[28] + z[36];
    z[26]=z[15]*z[8]*z[26];
    z[20]=z[20] + z[21] + z[26] + z[23] + static_cast<T>(6)+ z[27];
    z[20]=z[20]*z[24];
    z[21]= - 12*z[35] + static_cast<T>(27)+ 8*z[51];
    z[21]=z[14]*z[21];
    z[23]=z[35] - 2;
    z[24]=z[51] - z[23];
    z[24]=z[24]*z[53];
    z[21]=z[21] + 14*z[24];
    z[21]=z[7]*z[21];
    z[24]=z[66] + 6*z[8];
    z[24]=z[14]*z[24];
    z[21]=z[21] + static_cast<T>(1)+ z[24];
    z[21]=z[21]*z[29];
    z[24]=z[44]*z[32];
    z[26]=z[44]*npow(z[16],2);
    z[23]= - z[14]*z[23];
    z[23]=z[26] + z[23];
    z[23]=z[23]*z[37];
    z[23]=z[23] + 14*z[35] - static_cast<T>(14)+ z[24];
    z[23]=z[15]*z[23];
    z[24]= - z[16]*z[2];
    z[24]=static_cast<T>(4)+ z[24];
    z[24]=z[24]*z[48];
    z[26]= - z[16]*z[17];
    z[26]=z[26] - 1;
    z[26]=z[2]*z[26];
    z[26]=4*z[17] + z[26];
    z[26]=z[16]*z[26];
    z[26]=z[26] + static_cast<T>(5)- z[68];
    z[26]=z[8]*z[26];
    z[27]=z[2]*z[47];
    z[27]= - 5*z[45] + z[27];
    z[27]=z[14]*z[27];
    z[28]=static_cast<T>(4)- z[68];
    z[28]=z[10]*z[28];
    z[29]= - static_cast<T>(14)+ z[39];
    z[29]=z[9]*z[29];
    z[18]=z[18] + z[20] + z[19] + z[23] + z[21] + z[27] + z[29] + z[26]
    + z[81] + z[24] + z[28] + z[2];
    z[18]=z[1]*z[18];
    z[19]= - z[25] + z[22];
    z[19]=z[4]*z[19];
    z[20]= - z[16]*z[74];
    z[21]=z[13] - z[16];
    z[21]=z[8]*z[21];
    z[22]= - z[2] - z[8];
    z[22]=z[14]*z[22];
    z[23]= - 30*z[15] + z[2] + z[3];
    z[23]=z[11]*z[23];

    r += z[18] + 4*z[19] + z[20] + z[21] + z[22] + z[23] + 14*z[34];
 
    return r;
}

template double qg_2lNLC_r1038(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1038(const std::array<dd_real,31>&);
#endif
