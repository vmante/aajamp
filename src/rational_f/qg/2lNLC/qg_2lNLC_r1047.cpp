#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1047(const std::array<T,31>& k) {
  T z[63];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[14];
    z[6]=k[19];
    z[7]=k[12];
    z[8]=k[13];
    z[9]=k[8];
    z[10]=k[9];
    z[11]=k[11];
    z[12]=k[15];
    z[13]=k[4];
    z[14]=k[29];
    z[15]=k[2];
    z[16]=k[5];
    z[17]=k[3];
    z[18]=z[8]*z[9];
    z[19]=z[7]*z[10];
    z[20]=z[19] - z[18];
    z[21]=n<T>(1,2)*z[12];
    z[20]=z[20]*z[21];
    z[22]=z[9]*z[10];
    z[23]= - z[12]*z[10];
    z[23]= - z[22] + z[23];
    z[24]= - z[4] - z[9];
    z[24]=z[3]*z[24];
    z[23]=n<T>(1,2)*z[23] + z[24];
    z[23]=z[3]*z[23];
    z[24]=npow(z[10],2);
    z[25]=z[24]*z[9];
    z[26]=npow(z[8],2);
    z[27]= - z[7]*z[26];
    z[28]=z[4]*z[13];
    z[29]= - static_cast<T>(1)- z[28];
    z[29]=z[5]*z[29]*npow(z[3],2);
    z[20]=2*z[29] + z[23] + z[20] - z[25] + z[27];
    z[20]=z[5]*z[20];
    z[23]=z[11]*z[4];
    z[27]=npow(z[6],2);
    z[29]=z[23]*z[27];
    z[30]=z[7]*z[11];
    z[31]=z[27]*z[30];
    z[32]=npow(z[12],2);
    z[33]=z[32]*z[30];
    z[31]= - z[33] - z[29] + z[31];
    z[33]=z[12]*z[14];
    z[34]=z[7]*z[6];
    z[35]= - n<T>(1,2)*z[6] + 2*z[14];
    z[35]=z[4]*z[35];
    z[35]=2*z[33] + z[35] + n<T>(1,2)*z[34];
    z[35]=z[3]*z[35];
    z[36]=z[4] - z[7];
    z[36]=z[27]*z[36];
    z[35]=n<T>(1,2)*z[36] + z[35];
    z[35]=z[3]*z[35];
    z[36]=npow(z[10],3);
    z[37]=4*z[36];
    z[38]= - z[9]*z[37];
    z[39]=npow(z[8],3);
    z[40]=z[7]*z[39];
    z[20]=z[20] + z[35] + 4*z[40] + z[38] + n<T>(1,2)*z[31];
    z[20]=z[5]*z[20];
    z[31]=z[30]*z[6];
    z[35]= - z[3]*z[34];
    z[35]=z[35] + z[31];
    z[35]=z[39]*z[35];
    z[38]=z[23]*z[36];
    z[39]=z[9]*z[38];
    z[36]=z[30]*z[36];
    z[40]=z[12]*z[36];
    z[35]=z[40] + z[39] + z[35];
    z[39]= - z[3]*z[6];
    z[39]=z[27] + z[39];
    z[39]=z[3]*z[4]*z[39];
    z[39]= - z[29] + z[39];
    z[39]=z[39]*npow(z[5],2);
    z[35]=4*z[35] + n<T>(1,2)*z[39];
    z[35]=z[1]*z[35];
    z[36]= - z[38] - z[36];
    z[38]=z[7] - z[6];
    z[39]=4*z[8];
    z[40]=z[38]*z[39];
    z[40]=5*z[34] + z[40];
    z[40]=z[40]*z[26];
    z[41]=npow(z[14],2);
    z[33]=z[33] + z[41];
    z[33]=z[33]*z[12];
    z[42]=z[41]*z[4];
    z[43]= - z[42] - z[33];
    z[44]=n<T>(1,2)*z[3];
    z[43]=z[43]*z[44];
    z[40]=z[40] + z[43];
    z[40]=z[3]*z[40];
    z[43]=4*z[10];
    z[45]= - z[4] + z[11];
    z[45]=z[45]*z[43];
    z[45]= - 5*z[23] + z[45];
    z[45]=z[45]*z[25];
    z[46]=z[11]*z[6];
    z[47]=z[46] - z[30];
    z[47]=z[47]*z[39];
    z[47]= - 5*z[31] + z[47];
    z[47]=z[47]*z[26];
    z[48]=z[24]*z[7];
    z[43]= - 5*z[11] - z[43];
    z[43]=z[43]*z[48];
    z[49]=z[11]*z[37];
    z[43]=z[49] + z[43];
    z[43]=z[12]*z[43];
    z[20]=z[35] + z[20] + z[40] + z[43] + z[47] + 4*z[36] + z[45];
    z[20]=z[1]*z[20];
    z[35]=z[9]*z[13];
    z[36]=z[35] + static_cast<T>(2)+ 5*z[28];
    z[36]=z[3]*z[36];
    z[40]=6*z[5];
    z[43]=n<T>(1,2)*z[7];
    z[36]= - z[40] + z[36] + z[12] - z[4] + z[43];
    z[36]=z[3]*z[36];
    z[45]=2*z[7];
    z[47]=n<T>(1,2)*z[8];
    z[49]=z[9]*z[15];
    z[50]= - z[12]*z[49];
    z[50]=z[50] - n<T>(3,2)*z[9] - z[45] + z[47] - z[11];
    z[50]=z[12]*z[50];
    z[51]=z[16]*z[7];
    z[52]=z[51] + 1;
    z[53]=z[52]*z[24];
    z[54]=z[10]*z[15];
    z[55]= - static_cast<T>(1)- z[54];
    z[55]=z[55]*z[22];
    z[56]=z[52] - z[35];
    z[57]= - z[8]*z[56];
    z[58]= - z[7] - z[9];
    z[57]=n<T>(1,2)*z[58] + z[57];
    z[57]=z[8]*z[57];
    z[58]=n<T>(1,2)*z[23];
    z[36]=z[50] + z[57] + z[55] - z[19] - z[58] + z[53] + z[36];
    z[36]=z[5]*z[36];
    z[50]=3*z[7];
    z[55]=z[50]*z[24];
    z[57]=z[37]*z[51];
    z[37]=z[37] + z[55] + z[57];
    z[55]=n<T>(1,2)*z[19];
    z[59]= - z[11]*z[21];
    z[18]=z[59] + z[55] + z[18];
    z[18]=z[12]*z[18];
    z[59]=2*z[4];
    z[60]=3*z[9];
    z[43]=3*z[12] + z[60] + z[59] - z[43];
    z[43]=z[3]*z[43];
    z[43]= - z[32] + z[43];
    z[43]=z[3]*z[43];
    z[61]= - static_cast<T>(1)+ 4*z[54];
    z[25]= - z[61]*z[25];
    z[56]=z[56]*z[39];
    z[62]= - z[7] + z[9];
    z[56]=3*z[62] + z[56];
    z[26]=z[56]*z[26];
    z[18]=z[36] + z[43] + z[18] + z[26] + z[25] + z[37];
    z[18]=z[5]*z[18];
    z[25]=z[14]*z[13];
    z[26]=z[25] - n<T>(3,2);
    z[36]=z[26]*z[14];
    z[25]=z[25] + n<T>(1,2);
    z[43]=z[12]*z[25];
    z[43]=z[36] + z[43];
    z[43]=z[12]*z[43];
    z[36]=z[6] + z[36];
    z[36]=z[4]*z[36];
    z[36]=z[43] + z[36] + z[34];
    z[36]=z[3]*z[36];
    z[43]= - z[27] + z[41];
    z[43]=z[4]*z[43];
    z[56]= - z[16]*z[39];
    z[56]= - static_cast<T>(5)+ z[56];
    z[38]=z[8]*z[38]*z[56];
    z[34]= - 3*z[34] + z[38];
    z[34]=z[8]*z[34];
    z[38]= - z[7]*z[27];
    z[33]=z[36] + z[33] + z[34] + z[43] + z[38];
    z[33]=z[3]*z[33];
    z[34]=z[16]*z[11];
    z[36]=z[34]*z[6];
    z[38]= - z[11] - z[36];
    z[38]=z[38]*z[39];
    z[38]=z[38] - 5*z[46] + 2*z[30];
    z[38]=z[8]*z[38];
    z[31]=3*z[31] + z[38];
    z[31]=z[8]*z[31];
    z[38]=4*z[11];
    z[43]= - z[24]*z[38];
    z[56]=2*z[10];
    z[62]=3*z[11] + z[56];
    z[19]=z[62]*z[19];
    z[30]=z[30]*z[12];
    z[19]=z[30] + z[43] + z[19];
    z[19]=z[12]*z[19];
    z[30]=z[4]*z[56];
    z[30]=z[23] + z[30];
    z[24]=2*z[24];
    z[30]=z[30]*z[24];
    z[43]=z[11] + z[56];
    z[43]=z[43]*z[24];
    z[27]=z[11]*z[27];
    z[27]=z[27] + z[43];
    z[27]=z[7]*z[27];
    z[43]=z[54]*z[4];
    z[38]=4*z[43] + z[4] - z[38];
    z[38]=z[10]*z[38];
    z[23]=3*z[23] + z[38];
    z[23]=z[23]*z[22];
    z[18]=z[20] + z[18] + z[33] + z[19] + z[31] + z[23] + z[27] + z[29]
    + z[30];
    z[19]=npow(z[2],2);
    z[18]=z[1]*z[19]*z[18];
    z[20]=n<T>(1,2)*z[51];
    z[23]= - z[16]*z[47];
    z[27]=z[15] + 4*z[16];
    z[27]=z[12]*z[27];
    z[23]=z[27] + z[23] - n<T>(3,2)*z[49] - static_cast<T>(5)- z[20];
    z[23]=z[12]*z[23];
    z[27]= - z[55] - z[46] + z[53];
    z[27]=z[16]*z[27];
    z[29]=n<T>(3,2)*z[35];
    z[30]=z[12]*z[16];
    z[31]=z[6] + z[45];
    z[31]=z[16]*z[31];
    z[31]= - z[30] - z[29] + static_cast<T>(5)+ z[31];
    z[31]=z[3]*z[31];
    z[29]=z[29] - z[52];
    z[29]=z[8]*z[29];
    z[33]=z[30]*z[40];
    z[38]=z[54]*z[9];
    z[40]= - n<T>(3,2) + z[54];
    z[40]=z[10]*z[40];
    z[23]=z[33] + z[31] + z[23] + z[29] - n<T>(3,2)*z[38] + z[40] + z[27];
    z[23]=z[5]*z[23];
    z[27]= - static_cast<T>(15)+ z[34];
    z[29]=2*z[9];
    z[31]= - z[15]*z[29];
    z[27]=n<T>(1,2)*z[27] + z[31];
    z[27]=z[12]*z[27];
    z[31]=z[11] + n<T>(9,2)*z[14];
    z[27]=z[27] - z[47] - z[29] - z[56] + z[31];
    z[27]=z[12]*z[27];
    z[33]=z[30] - 3*z[35] + z[20] + n<T>(1,2) - 4*z[28];
    z[33]=z[3]*z[33];
    z[40]=z[16]*z[32];
    z[33]=z[33] - 3*z[40] - z[29] - z[6] + z[59];
    z[33]=z[3]*z[33];
    z[37]=z[16]*z[37];
    z[40]= - n<T>(5,2) - 3*z[54];
    z[22]=z[40]*z[22];
    z[40]= - 3*z[52] - 4*z[35];
    z[40]=z[8]*z[40];
    z[40]=z[29] + z[40];
    z[40]=z[8]*z[40];
    z[45]=z[4]*z[14];
    z[52]=z[6] - n<T>(1,2)*z[4];
    z[52]=z[11]*z[52];
    z[53]=z[10]*z[61];
    z[53]=n<T>(7,2)*z[4] + z[53];
    z[53]=z[10]*z[53];
    z[22]=z[23] + z[33] + z[27] + z[40] + z[22] + z[37] + z[53] + n<T>(9,2)*
    z[45] + z[52];
    z[22]=z[5]*z[22];
    z[23]=npow(z[13],2);
    z[27]=z[23]*z[14];
    z[33]=z[27] + z[13];
    z[37]=n<T>(1,2)*z[14];
    z[33]=z[33]*z[37];
    z[37]= - static_cast<T>(1)- z[33];
    z[37]=z[4]*z[37];
    z[27]= - z[13] - n<T>(1,2)*z[27];
    z[27]=z[12]*z[27];
    z[27]=z[27] - static_cast<T>(3)- z[33];
    z[27]=z[12]*z[27];
    z[27]=z[27] - z[29] + z[37] - z[7];
    z[27]=z[3]*z[27];
    z[33]= - z[25]*z[45];
    z[37]=z[8]*z[16];
    z[37]=z[37] + 1;
    z[40]=z[50] - z[6];
    z[37]=z[8]*z[40]*z[37];
    z[25]= - z[14]*z[25];
    z[26]= - z[12]*z[26];
    z[25]=z[25] + z[26];
    z[25]=z[12]*z[25];
    z[25]=z[27] + z[25] + z[33] + z[37];
    z[25]=z[3]*z[25];
    z[26]=z[34] - 1;
    z[27]=npow(z[16],2);
    z[33]=z[27]*z[11];
    z[37]=z[13] + z[33];
    z[37]=z[9]*z[37];
    z[37]=z[37] + z[26];
    z[37]=z[37]*z[39];
    z[40]=2*z[11];
    z[50]= - static_cast<T>(3)+ 4*z[34];
    z[50]=z[9]*z[50];
    z[36]=z[37] + z[50] + z[40] + z[36];
    z[36]=z[8]*z[36];
    z[37]=z[11]*z[29];
    z[36]=z[36] + z[46] + z[37];
    z[36]=z[8]*z[36];
    z[37]=z[14]*z[15];
    z[46]= - static_cast<T>(1)+ 2*z[37];
    z[50]=z[14]*z[46];
    z[47]= - z[47] + z[50];
    z[47]=z[9]*z[47];
    z[29]=z[37]*z[29];
    z[29]=z[29] + z[31];
    z[29]=z[12]*z[29];
    z[31]=z[11]*z[56];
    z[29]=z[29] + n<T>(9,2)*z[41] + z[31] + z[47];
    z[29]=z[12]*z[29];
    z[31]=z[46]*z[45];
    z[40]=3*z[43] - n<T>(3,2)*z[4] + z[40];
    z[40]=z[10]*z[40];
    z[31]=z[40] + z[31] - z[58];
    z[31]=z[9]*z[31];
    z[40]= - z[4]*z[15];
    z[40]= - static_cast<T>(1)+ z[40];
    z[40]=z[40]*z[56];
    z[40]=z[4] + z[40];
    z[24]=z[40]*z[24];
    z[22]=z[22] + z[25] + z[29] + z[36] + z[31] - z[57] - 2*z[48] + n<T>(9,2)
   *z[42] + z[24];
    z[22]=z[22]*z[19];
    z[18]=z[22] + z[18];
    z[18]=z[1]*z[18];
    z[22]=z[27]*z[8];
    z[24]=z[22]*z[7];
    z[25]=z[34] - z[24];
    z[29]=n<T>(1,2)*z[34];
    z[31]=static_cast<T>(3)+ z[29];
    z[31]=z[16]*z[31];
    z[31]=5*z[15] + z[31];
    z[30]=z[31]*z[30];
    z[30]=z[30] + z[16] + n<T>(1,2)*z[22];
    z[30]=z[12]*z[30];
    z[31]=z[12] - z[7];
    z[31]=z[31]*z[27];
    z[36]= - z[31]*z[44];
    z[27]=z[32]*z[27];
    z[27]=2*z[27];
    z[32]= - z[5]*z[15]*z[27];
    z[25]=z[32] + z[36] + n<T>(1,2)*z[25] + z[30];
    z[25]=z[5]*z[25];
    z[30]=z[37]*z[4];
    z[30]=z[30] + z[43];
    z[30]=z[11] + 11*z[30];
    z[32]= - z[51] - 11*z[35];
    z[32]=z[8]*z[32];
    z[32]=z[32] - 11*z[38] + z[30];
    z[36]=n<T>(11,2)*z[37];
    z[29]= - static_cast<T>(7)+ z[29];
    z[29]=z[16]*z[29];
    z[29]= - 4*z[15] + z[29];
    z[29]=z[12]*z[29];
    z[29]=z[29] - n<T>(11,2)*z[49] - static_cast<T>(2)+ z[36];
    z[29]=z[12]*z[29];
    z[27]= - z[27] + n<T>(11,2)*z[35] - static_cast<T>(1)+ z[20];
    z[27]=z[3]*z[27];
    z[25]=z[25] + z[27] + n<T>(1,2)*z[32] + z[29];
    z[25]=z[5]*z[25];
    z[26]=z[16]*z[26];
    z[26]= - z[13] + z[26];
    z[26]=z[9]*z[16]*z[26];
    z[26]=z[26] - z[33] + z[17] + z[13];
    z[26]=z[26]*z[39];
    z[27]= - static_cast<T>(1)+ 5*z[34];
    z[27]=z[16]*z[27];
    z[27]=3*z[13] + z[27];
    z[27]=z[9]*z[27];
    z[26]=z[26] + z[27] + static_cast<T>(1)- 2*z[34];
    z[26]=z[8]*z[26];
    z[27]= - n<T>(1,2) + z[34];
    z[27]=z[27]*z[60];
    z[26]=z[27] + z[26];
    z[26]=z[8]*z[26];
    z[27]=z[13] - z[15];
    z[29]= - z[27]*z[41];
    z[32]= - z[14]*z[27];
    z[32]= - z[34] + static_cast<T>(4)+ z[32];
    z[32]=z[12]*z[32];
    z[33]=z[9]*z[36];
    z[29]=z[32] + z[29] + z[33];
    z[29]=z[12]*z[29];
    z[21]=z[23]*z[21];
    z[21]=2*z[13] + z[21];
    z[21]=z[12]*z[21];
    z[21]=z[21] + z[28] + 2*z[35];
    z[21]=z[3]*z[21];
    z[20]= - static_cast<T>(2)+ z[20];
    z[20]=z[8]*z[20];
    z[23]= - n<T>(1,2)*z[13] + 2*z[16];
    z[23]=z[12]*z[23];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[12]*z[23];
    z[20]=z[21] + z[23] + z[20] - z[4] + z[9];
    z[20]=z[3]*z[20];
    z[21]= - z[27]*z[42];
    z[23]=z[9]*z[30];
    z[20]=z[25] + z[20] + z[29] + z[26] + z[21] + n<T>(1,2)*z[23];
    z[20]=z[20]*z[19];
    z[18]=z[20] + z[18];
    z[18]=z[1]*z[18];
    z[20]=z[22]*z[12];
    z[20]=z[20] - z[24];
    z[21]= - z[3]*z[31];
    z[21]=z[21] + z[20];
    z[21]=z[5]*z[21];
    z[20]= - z[3]*z[20];
    z[20]=z[20] + z[21];
    z[19]=z[20]*z[19];
    z[18]=n<T>(1,2)*z[19] + z[18];

    r += z[18]*z[1];
 
    return r;
}

template double qg_2lNLC_r1047(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1047(const std::array<dd_real,31>&);
#endif
