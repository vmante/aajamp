#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r639(const std::array<T,31>& k) {
  T z[133];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[12];
    z[10]=k[2];
    z[11]=k[3];
    z[12]=k[9];
    z[13]=k[14];
    z[14]=k[17];
    z[15]=k[13];
    z[16]=n<T>(3,2)*z[11];
    z[17]=z[2]*z[6];
    z[18]=2*z[17];
    z[19]=static_cast<T>(1)- z[18];
    z[19]=z[2]*z[19];
    z[19]= - z[16] + z[19];
    z[19]=z[2]*z[19];
    z[20]=npow(z[2],2);
    z[21]=2*z[6];
    z[22]=z[20]*z[21];
    z[22]=z[22] + z[11];
    z[22]=z[22]*z[2];
    z[23]=npow(z[11],2);
    z[24]= - z[23] + z[22];
    z[25]=z[4]*z[2];
    z[24]=z[24]*z[25];
    z[26]=4*z[23];
    z[19]=z[24] + z[26] + z[19];
    z[19]=z[4]*z[19];
    z[24]=z[23]*z[12];
    z[27]=4*z[24];
    z[28]=3*z[11];
    z[29]=z[28] + z[27];
    z[29]=z[12]*z[29];
    z[29]= - static_cast<T>(1)+ z[29];
    z[30]=z[10]*z[12];
    z[29]=z[29]*z[30];
    z[31]= - z[11] - 9*z[24];
    z[31]=z[12]*z[31];
    z[29]=z[29] + n<T>(13,2) + z[31];
    z[29]=z[10]*z[29];
    z[31]=z[6]*z[10];
    z[32]=6*z[31];
    z[33]= - 12*z[17] - n<T>(13,2) + z[32];
    z[33]=z[2]*z[33];
    z[34]=2*z[24];
    z[35]=npow(z[10],2);
    z[36]=z[35]*z[6];
    z[16]=z[19] + z[33] + 6*z[36] + z[29] - z[16] + z[34];
    z[16]=z[5]*z[16];
    z[19]=z[6] - z[13];
    z[29]=npow(z[6],2);
    z[33]=z[29]*z[2];
    z[37]= - z[33] + z[19];
    z[37]=z[37]*z[20];
    z[38]=2*z[23];
    z[22]=z[38] + z[22];
    z[22]=z[4]*z[22];
    z[39]= - z[9]*z[38];
    z[22]=z[22] + 4*z[37] + n<T>(9,2)*z[11] + z[39];
    z[22]=z[4]*z[22];
    z[37]=2*z[9];
    z[39]=z[37]*z[24];
    z[40]=5*z[8];
    z[41]=z[12]*z[11];
    z[42]= - static_cast<T>(7)+ 12*z[41];
    z[42]=z[12]*z[42];
    z[42]=z[40] + z[42];
    z[43]=2*z[10];
    z[42]=z[42]*z[43];
    z[44]= - 44*z[11] + z[24];
    z[44]=z[12]*z[44];
    z[42]=z[42] + static_cast<T>(26)+ z[44];
    z[42]=z[12]*z[42];
    z[42]= - z[8] + z[42];
    z[42]=z[10]*z[42];
    z[44]=z[28] + z[43];
    z[44]=z[10]*z[44];
    z[44]= - z[38] + 7*z[44];
    z[45]=npow(z[3],2);
    z[44]=z[44]*z[45];
    z[46]=11*z[2] - 7*z[11] - 26*z[10];
    z[46]=z[45]*z[46];
    z[47]=2*z[13];
    z[46]= - z[6] + z[47] + z[46];
    z[46]=z[2]*z[46];
    z[48]=5*z[41];
    z[49]=static_cast<T>(1)+ z[48];
    z[50]=16*z[10] - z[36];
    z[50]=z[6]*z[50];
    z[16]=z[16] + z[22] + z[46] + z[39] + z[50] + z[44] + 2*z[49] + 
    z[42];
    z[16]=z[5]*z[16];
    z[22]=n<T>(17,2)*z[45];
    z[42]=z[10]*z[22];
    z[42]=z[42] - z[21];
    z[44]=3*z[6];
    z[42]=z[42]*z[44];
    z[46]=z[45]*z[17];
    z[49]=npow(z[13],2);
    z[50]=z[40] - z[13];
    z[50]=2*z[50] + n<T>(37,2)*z[6];
    z[50]=z[9]*z[50];
    z[42]= - n<T>(25,2)*z[46] + z[50] - z[49] + z[42];
    z[42]=z[2]*z[42];
    z[46]=2*z[29];
    z[22]= - z[46] + 5*z[49] + z[22];
    z[22]=z[2]*z[22];
    z[50]=z[45]*z[11];
    z[22]=z[22] + n<T>(35,2)*z[6] + z[13] - n<T>(7,2)*z[50];
    z[22]=z[2]*z[22];
    z[51]=3*z[17];
    z[52]=z[51] + 1;
    z[52]=z[52]*z[25];
    z[53]=z[23]*z[9];
    z[54]=2*z[11];
    z[55]= - z[54] + z[53];
    z[55]=z[55]*z[37];
    z[22]= - z[52] + z[22] - static_cast<T>(7)+ z[55];
    z[22]=z[4]*z[22];
    z[55]=4*z[12];
    z[56]=3*z[8];
    z[57]= - z[56] + z[55];
    z[57]=z[12]*z[57];
    z[58]=z[10]*z[8];
    z[59]=npow(z[12],2);
    z[60]=z[59]*z[58];
    z[57]=z[57] + 25*z[60];
    z[57]=z[10]*z[57];
    z[60]=z[43]*z[45];
    z[61]=3*z[41];
    z[62]= - z[61] - 11*z[30];
    z[62]=z[62]*z[60];
    z[63]=z[35]*z[45];
    z[64]=z[35]*z[8];
    z[65]=z[21]*z[64];
    z[65]=z[65] - n<T>(39,2)*z[63] - static_cast<T>(3)- 23*z[58];
    z[65]=z[6]*z[65];
    z[66]=z[23]*z[59];
    z[66]= - static_cast<T>(9)+ z[66];
    z[66]=z[12]*z[66];
    z[67]=4*z[11] - z[24];
    z[67]=z[12]*z[67];
    z[39]= - z[39] - n<T>(11,2) + z[67];
    z[39]=z[9]*z[39];
    z[67]=2*z[8];
    z[16]=z[16] + z[22] + z[42] + z[39] + z[65] + z[62] + z[57] + z[66]
    + z[67] + z[13];
    z[16]=z[5]*z[16];
    z[22]=3*z[2];
    z[39]=z[22] - z[28] + z[10];
    z[42]=n<T>(1,2)*z[2];
    z[57]=npow(z[3],3);
    z[39]=z[42]*z[57]*z[39];
    z[62]=n<T>(1,2)*z[57];
    z[65]= - z[11] - z[10];
    z[65]=z[10]*z[65]*z[62];
    z[66]=6*z[6];
    z[39]=z[39] + z[65] + z[66];
    z[39]=z[2]*z[39];
    z[65]=z[17] + 1;
    z[68]=2*z[2];
    z[69]=z[65]*z[68];
    z[69]=z[69] - z[11];
    z[70]=z[69]*z[25];
    z[71]=n<T>(1,2)*z[11];
    z[18]= - static_cast<T>(1)- z[18];
    z[18]=z[2]*z[18];
    z[18]=z[70] - z[71] + z[18];
    z[18]=z[4]*z[18];
    z[70]=9*z[11];
    z[72]= - static_cast<T>(1)- 6*z[41];
    z[72]=z[10]*z[72];
    z[72]=z[70] + z[72];
    z[73]=z[59]*z[10];
    z[72]=z[72]*z[73];
    z[74]=z[10]*z[71];
    z[74]=z[38] + z[74];
    z[74]=z[10]*z[74];
    z[75]=npow(z[11],3);
    z[76]=3*z[75];
    z[74]= - z[76] + z[74];
    z[74]=z[74]*z[57];
    z[77]=z[58] - 1;
    z[32]= - z[77]*z[32];
    z[18]=z[18] + z[39] + z[32] + z[74] + z[72] - n<T>(1,2) - z[41];
    z[18]=z[5]*z[18];
    z[32]=15*z[8];
    z[39]=12*z[6];
    z[72]=z[39] + z[32] - z[47];
    z[72]=z[9]*z[72];
    z[74]=z[36]*z[57];
    z[78]=5*z[17] - 12*z[31];
    z[78]=z[57]*z[78];
    z[79]=z[29]*z[9];
    z[78]= - z[79] + z[78];
    z[78]=z[78]*z[68];
    z[72]=z[78] + 13*z[74] + z[72];
    z[72]=z[2]*z[72];
    z[78]=3*z[10];
    z[80]= - z[41]*z[78];
    z[80]= - z[24] + z[80];
    z[81]=4*z[10];
    z[80]=z[80]*z[57]*z[81];
    z[82]=2*z[49];
    z[83]=z[57]*z[11];
    z[84]=z[46] + z[82] - z[83];
    z[84]=z[2]*z[84];
    z[19]= - 4*z[19] + z[84];
    z[19]=z[2]*z[19];
    z[69]=z[4]*z[69];
    z[84]=z[9]*z[11];
    z[85]=n<T>(7,2)*z[84];
    z[19]=z[69] + z[85] + z[19];
    z[19]=z[4]*z[19];
    z[69]=8*z[12];
    z[86]=z[56] - z[69];
    z[86]=z[86]*z[78];
    z[86]=z[86] + static_cast<T>(28)+ z[61];
    z[73]=z[86]*z[73];
    z[86]=8*z[58];
    z[87]=static_cast<T>(11)- z[86];
    z[88]=z[8]*z[36];
    z[87]=2*z[87] + z[88];
    z[87]=z[6]*z[87];
    z[61]= - static_cast<T>(4)- z[61];
    z[61]=z[12]*z[61];
    z[88]= - static_cast<T>(1)- 2*z[41];
    z[88]=z[9]*z[88];
    z[18]=z[18] + z[19] + z[72] + z[88] + z[87] + z[80] + z[73] + z[32]
    + z[61];
    z[18]=z[5]*z[18];
    z[19]= - z[29]*z[78];
    z[19]=z[19] + z[33];
    z[19]=z[68]*z[57]*z[19];
    z[32]=z[35]*z[57];
    z[61]=4*z[29];
    z[72]=z[32]*z[61];
    z[73]= - z[49] - z[61];
    z[80]=n<T>(1,2)*z[6];
    z[87]=z[9]*z[80];
    z[73]=2*z[73] + z[87];
    z[73]=z[9]*z[73];
    z[19]=z[19] + z[72] + z[73];
    z[19]=z[2]*z[19];
    z[72]=z[57]*z[17];
    z[73]=npow(z[13],3);
    z[72]= - z[73] + n<T>(15,2)*z[72];
    z[72]=z[2]*z[72];
    z[72]=z[72] - 4*z[49] + z[29];
    z[72]=z[2]*z[72];
    z[87]=z[20]*z[57];
    z[88]=z[6] - z[87];
    z[88]=z[88]*z[25];
    z[89]=11*z[13];
    z[90]=z[89] - 5*z[6];
    z[91]= - static_cast<T>(4)+ n<T>(1,2)*z[84];
    z[91]=z[9]*z[91];
    z[72]=z[88] + z[72] + n<T>(1,2)*z[90] + z[91];
    z[72]=z[4]*z[72];
    z[88]=z[28]*z[32];
    z[90]=12*z[12];
    z[91]= - z[8] + z[90];
    z[91]=z[10]*z[91];
    z[88]=z[88] - static_cast<T>(5)+ z[91];
    z[88]=z[59]*z[88];
    z[91]=z[6]*z[8];
    z[88]= - 13*z[91] + z[88];
    z[92]=23*z[8];
    z[93]=z[11]*z[59];
    z[93]=z[93] + z[92] + z[13];
    z[94]=n<T>(5,2) + z[41];
    z[94]=z[9]*z[94];
    z[93]=z[94] + 2*z[93] + n<T>(71,2)*z[6];
    z[93]=z[9]*z[93];
    z[18]=z[18] + z[72] + z[19] + 2*z[88] + z[93];
    z[18]=z[5]*z[18];
    z[19]= - 16*z[8] + n<T>(41,2)*z[9];
    z[72]=z[7]*z[9];
    z[19]=z[72]*z[57]*z[19];
    z[88]= - z[9]*z[92];
    z[88]= - 8*z[83] + z[88];
    z[92]=npow(z[9],2);
    z[88]=z[88]*z[92];
    z[93]=z[49]*z[57];
    z[94]= - z[2]*z[93];
    z[19]=z[19] + z[88] + z[94];
    z[19]=z[7]*z[19];
    z[88]=2*z[14];
    z[94]=z[88] - z[12];
    z[95]=z[94]*z[21];
    z[96]=npow(z[14],2);
    z[97]=2*z[96];
    z[95]=z[95] + z[97] + z[59];
    z[95]=z[6]*z[95];
    z[98]= - z[57]*z[46];
    z[98]= - z[93] + z[98];
    z[98]=z[98]*z[20];
    z[99]=24*z[49];
    z[100]=z[12]*z[13];
    z[101]=7*z[100];
    z[102]=z[99] + z[101];
    z[102]=z[102]*z[12];
    z[103]=2*z[92];
    z[104]=13*z[8];
    z[105]= - z[104] + 8*z[9];
    z[105]=z[105]*z[103];
    z[19]=z[19] + z[98] + z[105] + z[95] + z[73] - z[102];
    z[19]=z[7]*z[19];
    z[95]=7*z[8];
    z[98]=z[57]*z[95];
    z[105]=z[92]*z[8];
    z[106]= - 23*z[57] + 10*z[105];
    z[106]=z[9]*z[106];
    z[98]=z[98] + z[106];
    z[98]=z[7]*z[98];
    z[106]=3*z[9];
    z[107]=z[40] - z[106];
    z[108]=4*z[9];
    z[109]=z[107]*z[108];
    z[83]=15*z[83] + z[109];
    z[83]=z[9]*z[83];
    z[109]=4*z[8];
    z[110]= - z[2]*z[57]*z[109];
    z[83]=z[98] + z[83] + z[110];
    z[83]=z[7]*z[83];
    z[98]=3*z[23];
    z[110]= - z[57]*z[98];
    z[111]=11*z[8];
    z[112]=z[106]*z[11];
    z[113]= - static_cast<T>(16)+ z[112];
    z[113]=z[9]*z[113];
    z[110]=z[113] + z[111] + z[110];
    z[110]=z[9]*z[110];
    z[87]=z[67]*z[87];
    z[83]=z[83] + z[110] + z[87];
    z[83]=z[7]*z[83];
    z[87]= - static_cast<T>(2)+ z[84];
    z[87]=z[87]*z[37];
    z[110]= - z[57]*npow(z[2],3);
    z[110]=z[110] + 1;
    z[110]=z[6]*z[110];
    z[83]=z[83] + z[87] + z[110];
    z[83]=z[4]*z[83];
    z[87]=3*z[14];
    z[110]=z[87] - 8*z[13];
    z[110]=z[110]*z[47];
    z[113]=2*z[12];
    z[114]= - z[47] + z[12];
    z[114]=z[114]*z[113];
    z[115]=3*z[96];
    z[110]=z[114] - z[115] + z[110];
    z[114]= - n<T>(3,2)*z[84] + static_cast<T>(4)- z[41];
    z[114]=z[9]*z[114];
    z[41]= - static_cast<T>(2)- z[41];
    z[41]=z[12]*z[41];
    z[41]=z[114] + z[41] + n<T>(9,2)*z[15] - z[109];
    z[41]=z[9]*z[41];
    z[114]=z[13]*z[14];
    z[116]= - z[96] - z[114];
    z[116]=z[116]*z[49];
    z[117]= - z[57]*z[33];
    z[116]=z[116] + z[117];
    z[116]=z[2]*z[116];
    z[117]=z[14] + z[13];
    z[117]=z[13]*z[117];
    z[117]= - z[115] + z[117];
    z[117]=z[13]*z[117];
    z[116]=z[117] + z[116];
    z[116]=z[116]*z[68];
    z[117]=z[12] - z[14];
    z[118]=8*z[117] + z[6];
    z[118]=z[6]*z[118];
    z[19]=z[83] + z[19] + z[116] + z[41] + 2*z[110] + z[118];
    z[19]=z[4]*z[19];
    z[41]=npow(z[12],3);
    z[83]=z[41]*z[81];
    z[110]=z[75]*z[12];
    z[116]= - z[10]*z[24];
    z[116]= - z[110] + z[116];
    z[118]=npow(z[3],4);
    z[116]=z[116]*z[118];
    z[83]=z[116] - 3*z[59] + z[83];
    z[83]=z[10]*z[83];
    z[116]=z[36]*z[118];
    z[119]=z[31] + z[51];
    z[120]=z[118]*z[2];
    z[119]=z[119]*z[120];
    z[119]= - z[116] + z[119];
    z[119]=z[119]*z[42];
    z[121]=6*z[8];
    z[122]=z[121] + n<T>(11,2)*z[6];
    z[122]=z[122]*z[9];
    z[119]=z[122] + z[119];
    z[119]=z[2]*z[119];
    z[123]=z[118]*z[11];
    z[124]=z[120] - z[123];
    z[124]=z[124]*z[2];
    z[125]=z[38]*z[118];
    z[126]= - z[125] - n<T>(5,2)*z[124];
    z[126]=z[2]*z[126];
    z[127]=z[75]*z[118];
    z[126]=z[126] + 5*z[127] + z[80];
    z[126]=z[2]*z[126];
    z[52]= - z[52] - n<T>(1,2) + z[126];
    z[52]=z[4]*z[52];
    z[66]= - z[77]*z[66];
    z[52]=z[52] + z[119] + z[66] + z[121] + z[83];
    z[52]=z[5]*z[52];
    z[66]=5*z[12];
    z[77]= - z[109] - z[66];
    z[77]=z[77]*z[59];
    z[83]=z[41]*z[86];
    z[77]=z[77] + z[83];
    z[77]=z[10]*z[77];
    z[83]=z[35]*z[118];
    z[119]= - z[29]*z[83];
    z[126]=z[29]*z[43];
    z[126]=z[126] - z[33];
    z[126]=z[126]*z[120];
    z[119]=z[119] + z[126];
    z[119]=z[2]*z[119];
    z[126]=z[6] - z[8];
    z[127]=n<T>(1,2)*z[92];
    z[128]= - z[126]*z[127];
    z[119]=z[128] + z[119];
    z[119]=z[2]*z[119];
    z[124]=z[125] + z[124];
    z[124]=z[2]*z[124];
    z[124]= - z[44] + z[124];
    z[124]=z[2]*z[124];
    z[124]= - static_cast<T>(1)+ z[124];
    z[125]=npow(z[4],2);
    z[124]=z[124]*z[125];
    z[128]=z[26]*z[83];
    z[128]=z[128] + 2;
    z[128]=z[59]*z[128];
    z[129]=n<T>(57,2)*z[8] + 28*z[6];
    z[129]=z[9]*z[129];
    z[52]=z[52] + z[124] + z[119] + z[129] - 22*z[91] + z[77] + z[128];
    z[52]=z[5]*z[52];
    z[77]=z[12] - z[80];
    z[77]=z[9]*z[77];
    z[77]=z[77] - z[59] - 48*z[91];
    z[77]=z[9]*z[77];
    z[119]=npow(z[9],3);
    z[124]=z[119]*z[8];
    z[42]=z[42]*z[124];
    z[128]=z[109] + z[12];
    z[128]=z[128]*z[59];
    z[129]=z[41]*z[58];
    z[130]=npow(z[2],4);
    z[131]= - z[4]*z[6]*z[118]*z[130];
    z[131]= - n<T>(5,2)*z[92] + z[131];
    z[131]=z[4]*z[131];
    z[52]=z[52] + z[131] + z[42] + z[77] + z[128] - 16*z[129];
    z[52]=z[5]*z[52];
    z[77]=npow(z[3],5);
    z[128]=z[77]*z[75];
    z[129]= - z[11] + z[2];
    z[129]=z[2]*z[129];
    z[129]=z[23] + z[129];
    z[129]=z[2]*z[77]*z[129];
    z[129]= - 2*z[128] + z[129];
    z[129]=z[2]*z[129];
    z[129]=z[6] + z[129];
    z[129]=z[129]*z[25];
    z[131]=z[77]*npow(z[2],5);
    z[132]=static_cast<T>(1)- 5*z[131];
    z[132]=z[6]*z[132];
    z[129]=n<T>(1,2)*z[132] + z[129];
    z[129]=z[4]*z[129];
    z[128]=z[59]*z[128];
    z[41]=z[41]*z[8];
    z[128]= - z[41] + z[128];
    z[128]=z[35]*z[128];
    z[127]= - z[17]*z[127];
    z[132]=6*z[91];
    z[122]=z[129] + z[127] + z[122] - z[132] + z[128];
    z[122]=z[5]*z[122];
    z[127]=z[43]*z[41];
    z[126]=z[9]*z[126];
    z[91]= - 28*z[91] - n<T>(1,2)*z[126];
    z[91]=z[9]*z[91];
    z[126]=static_cast<T>(1)+ z[131];
    z[126]=z[125]*z[6]*z[126];
    z[42]=z[122] + z[126] + z[42] + z[127] + z[91];
    z[42]=z[5]*z[42];
    z[41]=z[42] - z[41] + n<T>(1,2)*z[124];
    z[41]=z[5]*z[41];
    z[42]=z[56]*z[119];
    z[91]=z[77]*npow(z[7],5)*z[124];
    z[91]= - z[42] - 7*z[91];
    z[122]=z[92]*z[7];
    z[126]=z[122]*z[77]*z[107];
    z[77]=z[77]*z[119];
    z[127]=z[28]*z[77];
    z[126]=z[127] + z[126];
    z[126]=z[7]*z[126];
    z[77]= - z[23]*z[77];
    z[77]=z[77] + z[126];
    z[126]=npow(z[7],2);
    z[77]=z[77]*z[126];
    z[77]=z[124] + z[77];
    z[77]=z[4]*z[7]*z[77];
    z[77]=n<T>(1,2)*z[91] + z[77];
    z[77]=z[4]*z[77];
    z[91]=z[125]*npow(z[3],6);
    z[125]=npow(z[7],6)*z[124]*z[91];
    z[127]= - z[9]*z[132];
    z[91]=z[6]*npow(z[2],6)*z[91];
    z[91]=z[127] + z[91];
    z[91]=z[91]*npow(z[5],3);
    z[91]=z[125] + z[91];
    z[91]=z[1]*z[91];
    z[41]=z[91] + z[77] + z[41];
    z[41]=z[1]*z[41];
    z[77]= - z[73]*z[83];
    z[83]=3*z[49];
    z[91]= - z[116]*z[83];
    z[77]=z[77] + z[91];
    z[77]=z[6]*z[77];
    z[42]= - z[120]*z[42];
    z[91]=z[124]*z[7];
    z[116]=5*z[91];
    z[120]=z[118]*z[116];
    z[42]=z[42] + z[120];
    z[42]=z[7]*z[42];
    z[42]=z[77] + n<T>(1,2)*z[42];
    z[42]=z[42]*z[126];
    z[77]= - n<T>(25,2)*z[8] + 7*z[9];
    z[77]=z[122]*z[118]*z[77];
    z[119]=z[119]*z[123];
    z[77]= - n<T>(7,2)*z[119] + z[77];
    z[77]=z[77]*z[126];
    z[77]= - n<T>(19,2)*z[124] + z[77];
    z[77]=z[7]*z[77];
    z[119]=z[111] - 13*z[9];
    z[119]=z[72]*z[118]*z[119];
    z[120]=z[92]*z[123];
    z[119]=11*z[120] + z[119];
    z[119]=z[7]*z[119];
    z[118]= - z[118]*z[98];
    z[120]=z[9]*z[40];
    z[118]=z[118] + z[120];
    z[118]=z[118]*z[92];
    z[118]=z[118] + z[119];
    z[118]=z[7]*z[118];
    z[107]=z[107]*z[92];
    z[118]=z[107] + z[118];
    z[118]=z[7]*z[118];
    z[118]= - z[92] + z[118];
    z[118]=z[4]*z[118];
    z[119]=z[106] - n<T>(9,2)*z[8] - z[12];
    z[119]=z[9]*z[119];
    z[119]=z[59] + z[119];
    z[119]=z[9]*z[119];
    z[77]=z[118] + z[119] + z[77];
    z[77]=z[4]*z[77];
    z[118]= - z[13]*z[56];
    z[119]=z[12]*z[8];
    z[118]=z[118] + z[119];
    z[119]=8*z[59];
    z[118]=z[118]*z[119];
    z[41]=z[41] + z[52] + z[77] + z[42] + z[118] + n<T>(9,2)*z[124];
    z[41]=z[1]*z[41];
    z[42]= - z[43]*z[93];
    z[52]=z[31]*z[57]*z[13];
    z[42]=z[42] + z[52];
    z[42]=z[6]*z[42];
    z[52]=z[73]*z[57];
    z[77]=z[10]*z[52];
    z[42]=z[77] + z[42];
    z[77]=z[96] - z[59];
    z[93]=z[117]*z[6];
    z[117]= - z[93] + z[77];
    z[79]=z[117]*z[79];
    z[42]=3*z[42] + z[79];
    z[62]= - z[105]*z[62];
    z[62]=z[52] + z[62];
    z[62]=z[62]*z[22];
    z[37]=z[56] - z[37];
    z[37]=z[103]*z[57]*z[37];
    z[37]=3*z[52] + z[37];
    z[37]=z[7]*z[37];
    z[37]=z[37] + 2*z[42] + z[62];
    z[37]=z[7]*z[37];
    z[42]= - z[32]*z[102];
    z[32]=z[49]*z[32];
    z[52]=z[13]*z[74];
    z[32]= - 8*z[32] + 5*z[52];
    z[32]=z[32]*z[21];
    z[52]= - z[14] - z[12];
    z[52]=z[52]*z[21];
    z[52]=z[52] - z[115] - z[119];
    z[52]=z[6]*z[52];
    z[57]=z[59]*z[13];
    z[52]=12*z[57] + z[52];
    z[52]=2*z[52] + 13*z[105];
    z[52]=z[9]*z[52];
    z[32]=z[37] + z[52] + z[42] + z[32];
    z[32]=z[7]*z[32];
    z[37]=5*z[114];
    z[42]=14*z[100] + z[115] - z[37];
    z[52]= - z[108] + z[56] + z[80];
    z[52]=z[9]*z[52];
    z[59]=z[96] - z[114];
    z[59]=z[59]*z[47];
    z[62]= - z[92]*z[56];
    z[59]=z[59] + z[62];
    z[59]=z[2]*z[59];
    z[62]=z[87] - 20*z[8];
    z[39]= - z[39] + 2*z[62] - 19*z[12];
    z[39]=z[6]*z[39];
    z[39]=z[59] + z[52] + 2*z[42] + z[39];
    z[39]=z[9]*z[39];
    z[42]=z[57]*z[58];
    z[52]=24*z[13];
    z[59]=17*z[8];
    z[62]=z[59] + z[52];
    z[62]=z[12]*z[62];
    z[74]=z[13]*z[8];
    z[62]=28*z[74] + z[62];
    z[62]=z[12]*z[62];
    z[18]=z[41] + z[18] + z[19] + z[32] + z[62] - 48*z[42] + z[39];
    z[18]=z[1]*z[18];
    z[19]= - z[45]*z[78];
    z[19]=z[19] - z[87] + z[113];
    z[19]=z[19]*z[21];
    z[32]=z[10]*z[13];
    z[39]=z[45]*z[32];
    z[19]=z[19] - 6*z[77] + 91*z[39];
    z[19]=z[6]*z[19];
    z[39]=9*z[13];
    z[41]=z[39] - z[12];
    z[41]=z[41]*z[113];
    z[41]=z[96] + z[41];
    z[62]=z[113] + 22*z[14] - z[59];
    z[62]=z[6]*z[62];
    z[79]=14*z[8];
    z[102]= - 12*z[9] + z[79] + z[80];
    z[102]=z[9]*z[102];
    z[41]=z[102] + 4*z[41] + z[62];
    z[41]=z[9]*z[41];
    z[62]=4*z[93] - z[77];
    z[62]=z[62]*z[21];
    z[77]=z[9]*z[59];
    z[77]= - n<T>(13,2)*z[45] + z[77];
    z[77]=z[9]*z[77];
    z[93]=z[45]*z[8];
    z[62]=z[77] + z[62] + 48*z[57] + n<T>(11,2)*z[93];
    z[62]=z[9]*z[62];
    z[77]=z[8]*z[15];
    z[102]= - n<T>(5,2)*z[77] + z[49];
    z[102]=z[102]*z[45];
    z[103]=z[45]*z[6];
    z[115]=z[13]*z[103];
    z[62]=z[62] + z[102] + n<T>(35,2)*z[115];
    z[62]=z[7]*z[62];
    z[102]=z[82] + z[101];
    z[60]=z[102]*z[60];
    z[102]=z[15]*z[56];
    z[102]=z[46] + z[102] + 7*z[49];
    z[102]=z[45]*z[102];
    z[93]= - z[93] - 7*z[105];
    z[93]=z[9]*z[93];
    z[93]=z[93] + z[102];
    z[93]=z[2]*z[93];
    z[39]=z[8] + z[39];
    z[39]=z[12]*z[39];
    z[39]= - z[99] + z[39];
    z[39]=z[12]*z[39];
    z[39]=z[73] + z[39];
    z[19]=z[62] + z[93] + z[41] + z[19] + 3*z[39] + z[60];
    z[19]=z[7]*z[19];
    z[39]= - z[45]*z[56];
    z[41]= - z[59] + 10*z[9];
    z[41]=z[41]*z[106];
    z[41]=17*z[45] + z[41];
    z[41]=z[9]*z[41];
    z[39]= - 27*z[91] + z[41] + 2*z[73] + z[39];
    z[39]=z[7]*z[39];
    z[41]=z[101] + z[96] + 12*z[49];
    z[60]= - n<T>(7,2)*z[8] - z[13];
    z[60]=z[60]*z[45];
    z[60]=z[103] + 4*z[73] + z[60];
    z[60]=z[2]*z[60];
    z[62]= - z[21] + z[94];
    z[62]=z[62]*z[44];
    z[93]=static_cast<T>(33)- n<T>(13,2)*z[84];
    z[93]=z[9]*z[93];
    z[93]=z[93] - 4*z[50] + 7*z[15] - 24*z[8];
    z[93]=z[9]*z[93];
    z[39]=z[39] + z[60] + z[93] + 2*z[41] + z[62];
    z[39]=z[7]*z[39];
    z[41]= - static_cast<T>(14)+ z[112];
    z[41]=z[9]*z[41];
    z[41]=z[111] + z[41];
    z[41]=z[41]*z[106];
    z[60]=3*z[107] + z[116];
    z[62]=2*z[7];
    z[60]=z[60]*z[62];
    z[41]=z[60] - 13*z[45] + z[41];
    z[41]=z[7]*z[41];
    z[60]=z[45]*z[2];
    z[93]=15*z[11] - z[53];
    z[93]=z[9]*z[93];
    z[93]= - static_cast<T>(31)+ z[93];
    z[93]=z[9]*z[93];
    z[41]=z[41] + 6*z[60] + z[93] + z[104] + 7*z[50];
    z[41]=z[7]*z[41];
    z[50]= - z[23]*z[45];
    z[93]= - z[68] - z[54];
    z[93]=z[45]*z[93];
    z[93]= - z[44] + z[93];
    z[93]=z[2]*z[93];
    z[94]=6*z[11] - z[53];
    z[94]=z[9]*z[94];
    z[41]=z[41] + z[93] + z[94] - static_cast<T>(7)+ z[50];
    z[41]=z[4]*z[41];
    z[50]=z[13] - z[14];
    z[93]=z[13]*z[50];
    z[93]=z[96] + z[93];
    z[93]=z[93]*z[47];
    z[94]=n<T>(1,2)*z[8];
    z[101]= - z[94] - z[47];
    z[101]=z[101]*z[45];
    z[93]=z[93] + z[101];
    z[93]=z[2]*z[93];
    z[101]=14*z[14];
    z[102]= - z[101] + 31*z[13];
    z[102]=z[13]*z[102];
    z[93]=z[93] - z[61] + 4*z[96] + z[102];
    z[93]=z[2]*z[93];
    z[34]= - z[11] + z[34];
    z[34]=z[9]*z[34];
    z[34]=z[34] + static_cast<T>(3)- z[48];
    z[34]=z[9]*z[34];
    z[48]=16*z[14];
    z[102]=14*z[6];
    z[34]=z[41] + z[39] + z[93] + z[34] + z[102] + 34*z[13] + z[67] + 5*
    z[15] - z[48];
    z[34]=z[4]*z[34];
    z[39]=z[45]*z[29]*z[81];
    z[37]=z[96] - z[37];
    z[41]= - z[56] + z[80];
    z[41]=z[9]*z[41];
    z[37]=2*z[37] + z[41];
    z[37]=z[9]*z[37];
    z[41]= - z[97] + z[49];
    z[41]=z[13]*z[41];
    z[45]= - n<T>(1,2)*z[77] + z[61];
    z[45]=z[45]*z[60];
    z[37]=z[45] + z[37] + 3*z[41] + z[39];
    z[37]=z[2]*z[37];
    z[39]=z[13]*z[121];
    z[41]= - z[8] + 12*z[13];
    z[41]=z[12]*z[41];
    z[39]=z[39] + z[41];
    z[39]=z[12]*z[39];
    z[39]=z[39] - 6*z[42];
    z[39]=z[39]*z[81];
    z[41]=n<T>(93,2)*z[63] - z[86] + n<T>(19,2);
    z[41]=z[13]*z[41];
    z[42]= - z[13]*z[64];
    z[42]= - static_cast<T>(4)+ z[42];
    z[42]=2*z[42] - 7*z[63];
    z[42]=z[6]*z[42];
    z[41]=z[42] + 9*z[12] - z[79] + z[41];
    z[41]=z[6]*z[41];
    z[42]=7*z[12];
    z[45]=43*z[13] + z[42];
    z[45]=z[45]*z[63];
    z[45]=z[45] + z[69] - 19*z[8] + 51*z[13];
    z[45]=z[12]*z[45];
    z[60]=z[14] - z[67];
    z[60]=3*z[60] - 25*z[13];
    z[60]=z[60]*z[47];
    z[61]= - z[70] + z[24];
    z[61]=z[12]*z[61];
    z[61]= - static_cast<T>(14)+ z[61];
    z[61]=z[12]*z[61];
    z[63]=n<T>(3,2)*z[9];
    z[61]= - z[63] + 44*z[6] + z[61] + 40*z[13] - 26*z[14] + 35*z[8];
    z[61]=z[9]*z[61];
    z[16]=z[18] + z[16] + z[34] + z[19] + z[37] + z[61] + z[41] + z[39]
    - n<T>(9,2)*z[77] + z[60] + z[45];
    z[16]=z[1]*z[16];
    z[18]= - static_cast<T>(1)+ 4*z[32];
    z[18]=z[18]*z[21];
    z[19]=10*z[14];
    z[34]=z[49]*z[10];
    z[18]=z[18] - 7*z[34] - 13*z[12] + 36*z[13] + z[19] - z[56];
    z[18]=z[6]*z[18];
    z[37]=z[89] - 6*z[12];
    z[37]=z[37]*z[55];
    z[39]=z[88] - z[8];
    z[41]= - 3*z[12] + z[39];
    z[41]=z[41]*z[44];
    z[45]= - z[108] + n<T>(11,2)*z[8];
    z[45]=z[45]*z[106];
    z[37]=z[45] + z[37] + z[41];
    z[37]=z[9]*z[37];
    z[41]=z[73] + 4*z[57];
    z[45]=8*z[57] + n<T>(7,2)*z[105];
    z[45]=z[7]*z[45]*z[106];
    z[57]=z[2]*z[124];
    z[37]=z[45] - n<T>(11,2)*z[57] + 6*z[41] + z[37];
    z[37]=z[7]*z[37];
    z[41]= - z[90] - z[8] + z[52];
    z[41]=z[41]*z[113];
    z[45]= - z[99] + z[100];
    z[45]=z[12]*z[45];
    z[45]=z[73] + z[45];
    z[45]=z[45]*z[78];
    z[52]=z[54] + z[24];
    z[57]= - z[52]*z[113];
    z[57]= - static_cast<T>(9)+ z[57];
    z[55]=z[57]*z[55];
    z[55]= - n<T>(21,2)*z[9] - z[6] + z[55] - 12*z[14] + z[59] + 28*z[13];
    z[55]=z[9]*z[55];
    z[57]=3*z[73] - 2*z[105];
    z[57]=z[57]*z[68];
    z[59]=6*z[96];
    z[18]=z[37] + z[57] + z[55] + z[18] + z[45] + z[41] + 44*z[49] - 
    z[59] - 7*z[77];
    z[18]=z[7]*z[18];
    z[37]=z[38] + 3*z[110];
    z[37]=z[12]*z[37];
    z[41]= - z[98] - z[110];
    z[41]=z[12]*z[41];
    z[41]=z[54] + z[41];
    z[41]=z[12]*z[41];
    z[41]=n<T>(11,2) + z[41];
    z[41]=z[10]*z[41];
    z[45]=n<T>(5,2)*z[11];
    z[37]=z[41] - z[45] + z[37];
    z[37]=z[10]*z[37];
    z[41]= - z[43] + z[36];
    z[55]=static_cast<T>(11)- 25*z[31];
    z[55]=n<T>(1,2)*z[55] + 4*z[17];
    z[55]=z[2]*z[55];
    z[41]=6*z[41] + z[55];
    z[41]=z[2]*z[41];
    z[55]=static_cast<T>(3)+ n<T>(11,2)*z[17];
    z[55]=z[2]*z[55];
    z[55]= - z[71] + z[55];
    z[55]=z[2]*z[55];
    z[55]= - z[26] + z[55];
    z[55]=z[2]*z[55];
    z[51]=z[51] + 2;
    z[57]=z[51]*z[2];
    z[57]=z[57] - z[11];
    z[60]= - z[57]*z[20];
    z[60]=z[60] + 2*z[75];
    z[60]=z[60]*z[25];
    z[55]=z[60] - z[76] + z[55];
    z[55]=z[4]*z[55];
    z[37]=z[55] + z[41] + z[37] + 5*z[23] - z[110];
    z[37]=z[5]*z[37];
    z[41]=20*z[23] - z[110];
    z[41]=z[12]*z[41];
    z[41]= - 22*z[11] + z[41];
    z[41]=z[12]*z[41];
    z[24]=z[11] - 8*z[24];
    z[24]=z[12]*z[24];
    z[24]=static_cast<T>(2)+ z[24];
    z[24]=z[24]*z[30];
    z[24]=z[24] + static_cast<T>(25)+ z[41];
    z[24]=z[10]*z[24];
    z[41]=z[29]*z[130];
    z[41]= - z[23] + z[41];
    z[55]= - z[2]*z[57];
    z[26]= - z[26] + z[55];
    z[25]=z[26]*z[25];
    z[25]=2*z[41] + z[25];
    z[25]=z[4]*z[25];
    z[23]= - z[110] + 6*z[23];
    z[23]=z[23]*z[12];
    z[26]=27*z[10] - z[36];
    z[26]=z[6]*z[26];
    z[41]= - 23*z[6] - z[33];
    z[41]=z[2]*z[41];
    z[26]=z[41] - static_cast<T>(25)+ z[26];
    z[26]=z[2]*z[26];
    z[24]=z[37] + z[25] + z[26] - z[36] + z[24] + 20*z[11] - z[23];
    z[24]=z[5]*z[24];
    z[25]= - z[29]*z[68];
    z[26]=17*z[13];
    z[37]=static_cast<T>(19)+ 7*z[31];
    z[37]=z[6]*z[37];
    z[25]=z[25] + z[37] + z[40] + z[26];
    z[25]=z[2]*z[25];
    z[27]= - z[11] - z[27];
    z[27]=z[12]*z[27];
    z[27]= - static_cast<T>(3)+ z[27];
    z[27]=z[27]*z[113];
    z[37]=10*z[8] - 31*z[12];
    z[30]=z[37]*z[30];
    z[37]= - z[56] - n<T>(7,2)*z[13];
    z[27]=z[30] + 3*z[37] + z[27];
    z[27]=z[10]*z[27];
    z[30]= - 13*z[13] - 45*z[6];
    z[30]=n<T>(1,2)*z[30] + z[33];
    z[30]=z[2]*z[30];
    z[30]= - static_cast<T>(5)+ z[30];
    z[30]=z[2]*z[30];
    z[20]=z[20]*z[4];
    z[33]=z[51]*z[20];
    z[30]=z[33] - n<T>(13,2)*z[11] + z[30];
    z[30]=z[4]*z[30];
    z[23]=z[70] + z[23];
    z[23]=z[12]*z[23];
    z[33]= - z[35]*z[21];
    z[35]= - n<T>(1,2) + 7*z[58];
    z[35]=z[10]*z[35];
    z[33]=z[35] + z[33];
    z[33]=z[6]*z[33];
    z[23]=z[24] + z[30] + z[25] + z[33] + z[27] - n<T>(1,2) + z[23];
    z[23]=z[5]*z[23];
    z[24]= - z[95] + z[108];
    z[24]=z[24]*z[92];
    z[24]= - n<T>(31,2)*z[91] + z[73] + 6*z[24];
    z[24]=z[7]*z[24];
    z[25]=n<T>(5,2)*z[15];
    z[27]=static_cast<T>(48)- n<T>(17,2)*z[84];
    z[27]=z[9]*z[27];
    z[27]=z[27] + z[25] - 36*z[8];
    z[27]=z[9]*z[27];
    z[30]=z[73]*z[68];
    z[24]=z[24] + z[30] + z[49] + z[27];
    z[24]=z[7]*z[24];
    z[27]=z[2]*z[73];
    z[27]=z[27] + z[49] + 6*z[29];
    z[27]=z[2]*z[27];
    z[30]=static_cast<T>(8)- z[85];
    z[30]=z[30]*z[106];
    z[24]=z[24] + z[27] + z[30] - z[44] + z[13] - z[94] + z[25] + 4*
    z[14];
    z[24]=z[7]*z[24];
    z[25]=4*z[107] + z[116];
    z[25]=z[7]*z[25];
    z[27]= - static_cast<T>(40)+ 9*z[84];
    z[27]=z[9]*z[27];
    z[27]=33*z[8] + z[27];
    z[27]=z[9]*z[27];
    z[25]=z[27] + z[25];
    z[25]=z[7]*z[25];
    z[27]=12*z[11] - z[53];
    z[27]=z[9]*z[27];
    z[27]= - static_cast<T>(25)+ z[27];
    z[27]=z[9]*z[27];
    z[27]=9*z[8] + z[27];
    z[25]=2*z[27] + z[25];
    z[25]=z[7]*z[25];
    z[27]=21*z[11] - 4*z[53];
    z[27]=z[9]*z[27];
    z[30]=z[2]*z[8];
    z[25]=z[25] - 8*z[30] - static_cast<T>(22)+ z[27];
    z[25]=z[7]*z[25];
    z[27]=z[28] - z[53];
    z[28]=static_cast<T>(2)+ z[17];
    z[22]=z[28]*z[22];
    z[22]=z[25] + 2*z[27] + z[22];
    z[22]=z[4]*z[22];
    z[25]=4*z[114] + 5*z[29];
    z[25]=z[2]*z[25];
    z[25]=z[25] - z[102] - 20*z[13] - n<T>(5,2)*z[8] - n<T>(1,2)*z[15] + 8*z[14]
   ;
    z[25]=z[2]*z[25];
    z[22]=z[22] + z[24] + z[25] - static_cast<T>(10)- n<T>(9,2)*z[84];
    z[22]=z[4]*z[22];
    z[24]= - 5*z[14] + z[8];
    z[24]=2*z[24] + z[26];
    z[24]=z[13]*z[24];
    z[25]=z[15]*z[40];
    z[26]=6*z[50] + z[6];
    z[26]=z[9]*z[26];
    z[24]=z[26] - z[46] + z[24] - z[59] + z[25];
    z[24]=z[2]*z[24];
    z[25]= - static_cast<T>(4)+ z[32];
    z[25]=z[25]*z[21];
    z[26]= - 10*z[58] + 40;
    z[26]=z[13]*z[26];
    z[25]=z[25] - z[42] + 3*z[39] + z[26];
    z[25]=z[10]*z[25];
    z[25]= - static_cast<T>(24)+ z[25];
    z[25]=z[6]*z[25];
    z[26]= - z[74] + 6*z[100];
    z[26]=z[26]*z[81];
    z[26]=z[26] - z[66] - z[56] + 74*z[13];
    z[26]=z[12]*z[26];
    z[27]=z[87] - z[109];
    z[27]=2*z[27] - z[89];
    z[27]=z[13]*z[27];
    z[26]=z[27] + z[26];
    z[26]=z[10]*z[26];
    z[27]= - z[52]*z[69];
    z[27]= - static_cast<T>(27)+ z[27];
    z[27]=z[12]*z[27];
    z[28]= - z[38] - z[110];
    z[28]=z[12]*z[28];
    z[28]= - 5*z[11] + z[28];
    z[28]=z[12]*z[28];
    z[28]= - static_cast<T>(20)+ z[28];
    z[28]=z[9]*z[28];
    z[16]=z[16] + z[23] + z[22] + z[18] + z[24] + z[28] + z[25] + z[26]
    + z[27] - z[47] + n<T>(27,2)*z[8] + n<T>(11,2)*z[15] - z[101];
    z[16]=z[1]*z[16];
    z[18]=z[73]*z[78];
    z[22]=z[49]*z[43];
    z[22]= - z[13] + z[22];
    z[22]=z[3]*z[22];
    z[18]=z[18] + z[22];
    z[18]=z[3]*z[18];
    z[22]=z[73]*z[10];
    z[23]=z[83] - z[22];
    z[23]=z[10]*z[23];
    z[23]= - z[47] + z[23];
    z[24]= - z[47] + z[34];
    z[24]=z[24]*z[43];
    z[24]=static_cast<T>(1)+ z[24];
    z[24]=z[3]*z[24];
    z[23]=2*z[23] + z[24];
    z[24]=z[6]*z[3];
    z[23]=z[23]*z[24];
    z[22]=z[82] - z[22];
    z[22]=z[10]*z[22];
    z[22]= - z[13] + z[22];
    z[22]=z[7]*z[22]*z[103];
    z[18]=z[22] + z[18] + z[23];
    z[18]=z[7]*z[18];
    z[22]=z[43]*z[3];
    z[23]= - z[22] - 4;
    z[23]=z[13]*z[23];
    z[23]=5*z[34] + z[23];
    z[23]=z[3]*z[23];
    z[25]=z[49]*z[78];
    z[26]=7*z[13];
    z[25]= - z[26] + z[25];
    z[25]=z[10]*z[25];
    z[25]=static_cast<T>(2)+ z[25];
    z[27]=z[32] - 2;
    z[27]=z[27]*z[3];
    z[28]= - z[10]*z[27];
    z[25]=2*z[25] + z[28];
    z[25]=z[25]*z[24];
    z[28]=z[3]*z[13];
    z[28]=z[49] + z[28];
    z[28]=z[3]*z[28];
    z[28]=z[28] - z[103];
    z[28]=z[2]*z[28];
    z[29]=2*z[3];
    z[30]=z[9]*z[29];
    z[18]=z[18] + z[28] + z[30] + z[23] + z[25];
    z[18]=z[7]*z[18];
    z[23]= - z[81]*z[27];
    z[23]= - n<T>(31,2) + z[23];
    z[23]=z[6]*z[23];
    z[25]=4*z[13] + z[88] + z[8];
    z[25]=z[3]*z[25];
    z[24]=z[25] - 5*z[24];
    z[24]=z[2]*z[24];
    z[25]=z[15] - z[88];
    z[27]=z[3]*z[32];
    z[18]=z[18] + z[24] - z[63] + z[23] - 8*z[27] + n<T>(37,2)*z[13] + 3*
    z[25] + z[8];
    z[18]=z[7]*z[18];
    z[23]= - 14*z[12] - z[26] + z[48] + z[56];
    z[23]=z[10]*z[23];
    z[17]= - z[17] + 1;
    z[17]=z[29]*z[17];
    z[22]=n<T>(63,2) + z[22];
    z[22]=z[6]*z[22];
    z[17]=z[22] + n<T>(21,2)*z[13] + z[8] - z[15] - z[19] + z[17];
    z[17]=z[2]*z[17];
    z[19]=z[68]*z[3];
    z[22]=z[54]*z[3];
    z[19]=z[19] + z[22];
    z[24]= - z[72] - static_cast<T>(1)+ z[84];
    z[24]=z[62]*z[3]*z[24];
    z[24]=z[24] - n<T>(5,2) + z[19];
    z[24]=z[7]*z[24];
    z[19]=static_cast<T>(3)- z[19];
    z[19]=z[2]*z[19];
    z[19]=z[24] + z[45] + z[19];
    z[19]=z[4]*z[19];
    z[21]=z[10]*z[21];
    z[21]=z[21] - z[65];
    z[21]=z[2]*z[21];
    z[21]=z[21] - z[36];
    z[21]=z[3]*z[21];
    z[24]=z[54] - z[10];
    z[25]= - z[24]*z[29];
    z[21]= - n<T>(31,2) + z[25] + z[21];
    z[21]=z[2]*z[21];
    z[20]=z[22]*z[20];
    z[24]=z[3]*z[24];
    z[24]=z[24] + 30;
    z[24]=z[10]*z[24];
    z[20]=z[20] + z[21] + n<T>(7,2)*z[11] + z[24];
    z[20]=z[5]*z[20];
    z[21]= - z[11]*z[15];
    z[21]=static_cast<T>(15)+ z[21];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + n<T>(1,2)*z[21] + z[22] + 
      z[23] - 78*z[31];
 
    return r;
}

template double qg_2lNLC_r639(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r639(const std::array<dd_real,31>&);
#endif
