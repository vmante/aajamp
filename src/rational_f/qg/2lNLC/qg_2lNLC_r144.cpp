#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r144(const std::array<T,31>& k) {
  T z[109];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[7];
    z[8]=k[14];
    z[9]=k[2];
    z[10]=k[5];
    z[11]=k[6];
    z[12]=k[9];
    z[13]=k[3];
    z[14]=k[13];
    z[15]=k[27];
    z[16]=z[10]*z[1];
    z[17]=2*z[16];
    z[18]=npow(z[1],2);
    z[19]=z[17] + z[18];
    z[20]=n<T>(1,3)*z[5];
    z[21]=z[19]*z[20];
    z[22]=npow(z[10],2);
    z[23]=z[22]*npow(z[11],3);
    z[24]=npow(z[1],4);
    z[25]=z[24]*z[5];
    z[26]=z[25]*z[23];
    z[27]=npow(z[10],3);
    z[28]=z[27]*z[5];
    z[26]=n<T>(2,3)*z[28] + z[26];
    z[26]=z[11]*z[26];
    z[21]=z[21] + z[26];
    z[21]=z[9]*z[21];
    z[26]=npow(z[1],3);
    z[29]=2*z[26];
    z[30]= - z[29]*z[23];
    z[31]=3*z[16];
    z[32]=2*z[18];
    z[33]=z[26]*z[3];
    z[21]=z[21] + z[30] - z[33] - z[32] - z[31];
    z[21]=z[9]*z[21];
    z[30]=z[18]*z[10];
    z[34]=z[18]*z[6];
    z[35]= - n<T>(2,3)*z[30] - z[26] - n<T>(4,3)*z[34];
    z[35]=z[10]*z[35];
    z[36]=z[24]*z[6];
    z[37]=npow(z[1],5);
    z[38]=z[36] + z[37];
    z[39]=2*z[6];
    z[40]= - z[38]*z[39];
    z[41]=npow(z[6],2);
    z[42]=z[41]*z[3];
    z[43]= - z[37]*z[42];
    z[40]=z[40] + z[43];
    z[43]=n<T>(1,3)*z[3];
    z[40]=z[40]*z[43];
    z[44]=z[26]*z[6];
    z[45]= - n<T>(4,3)*z[24] - z[44];
    z[45]=z[6]*z[45];
    z[46]=n<T>(1,3)*z[37];
    z[40]=z[40] - z[46] + z[45];
    z[40]=z[3]*z[40];
    z[45]= - z[26] - n<T>(1,3)*z[34];
    z[45]=z[6]*z[45];
    z[47]=n<T>(1,3)*z[24];
    z[45]= - z[47] + z[45];
    z[35]=z[40] + 2*z[45] + z[35];
    z[35]=z[7]*z[35];
    z[40]=3*z[18];
    z[45]=z[6]*z[1];
    z[48]=2*z[45];
    z[49]= - z[17] - z[40] - z[48];
    z[49]=z[10]*z[49];
    z[50]=z[39]*z[26];
    z[51]=z[50] + z[24];
    z[52]=z[36]*z[3];
    z[53]= - z[52] - z[51];
    z[53]=z[3]*z[53];
    z[21]=z[21] + z[35] + z[53] + z[49] - z[29] - 3*z[34];
    z[21]=z[8]*z[21];
    z[35]=7*z[18];
    z[49]=z[26]*z[5];
    z[53]= - z[33] - z[35] - z[49];
    z[53]=z[53]*z[43];
    z[54]= - z[22] - z[28];
    z[55]=z[22]*z[11];
    z[56]=z[49]*z[55];
    z[56]= - z[30] - 5*z[56];
    z[56]=z[11]*z[56];
    z[54]=4*z[54] + z[56];
    z[56]=n<T>(1,3)*z[11];
    z[54]=z[54]*z[56];
    z[57]=n<T>(4,3)*z[5];
    z[58]= - z[22]*z[57];
    z[59]=npow(z[11],2);
    z[60]=z[59]*z[10];
    z[61]= - z[49]*z[60];
    z[58]=z[58] + z[61];
    z[58]=z[11]*z[58];
    z[61]=z[18]*z[5];
    z[62]= - z[43]*z[61];
    z[58]=z[62] + z[58];
    z[58]=z[9]*z[58];
    z[62]=z[16] + z[18];
    z[63]= - z[5]*z[62];
    z[53]=z[58] + z[54] + z[53] - z[1] + z[63];
    z[53]=z[9]*z[53];
    z[54]= - z[3]*z[24]*z[39];
    z[58]=5*z[44];
    z[54]= - z[58] + z[54];
    z[54]=z[3]*z[54];
    z[54]= - z[29] + z[54];
    z[54]=z[3]*z[54];
    z[54]=z[54] + z[45] + z[17];
    z[63]=z[47] + z[44];
    z[63]=z[6]*z[63];
    z[64]=z[43]*z[41];
    z[65]=z[24]*z[64];
    z[63]=z[63] + z[65];
    z[63]=z[3]*z[63];
    z[65]=z[26] + z[34];
    z[65]=z[6]*z[65];
    z[63]=z[65] + z[63];
    z[63]=z[3]*z[63];
    z[65]=n<T>(1,3)*z[45];
    z[66]=z[65] + z[18];
    z[66]=z[6]*z[66];
    z[67]=z[10]*z[65];
    z[63]=z[63] + z[66] + z[67];
    z[66]=2*z[7];
    z[63]=z[63]*z[66];
    z[67]=z[60]*z[34];
    z[21]=z[21] + z[53] - n<T>(2,3)*z[67] + n<T>(1,3)*z[54] + z[63];
    z[21]=z[8]*z[21];
    z[53]=4*z[18];
    z[54]=z[53] + z[33];
    z[63]= - z[3]*z[54];
    z[68]=7*z[1];
    z[63]=z[63] - z[68] - z[61];
    z[63]=z[3]*z[63];
    z[69]=2*z[5];
    z[70]=z[22]*z[69];
    z[70]=z[10] + z[70];
    z[71]=z[11]*z[5];
    z[72]=z[30]*z[71];
    z[70]=4*z[70] + z[72];
    z[70]=z[11]*z[70];
    z[72]=z[5]*z[1];
    z[73]= - z[3]*z[72];
    z[74]=z[10]*z[71];
    z[73]=z[73] + z[74];
    z[73]=z[9]*z[73];
    z[74]=8*z[72];
    z[75]=z[2]*z[1];
    z[63]=2*z[73] - 20*z[75] + z[70] - z[74] + z[63];
    z[63]=z[9]*z[63];
    z[70]=z[33]*z[6];
    z[73]=4*z[34] + z[70];
    z[73]=z[3]*z[73];
    z[75]=4*z[45];
    z[73]=z[73] + z[18] + z[75];
    z[73]=z[3]*z[73];
    z[73]=z[73] - z[1] - z[10];
    z[76]=z[33]*z[41];
    z[77]=z[41]*z[18];
    z[78]= - 4*z[77] - z[76];
    z[78]=z[78]*z[43];
    z[79]= - n<T>(4,3)*z[18] - z[45];
    z[79]=z[6]*z[79];
    z[80]=n<T>(1,3)*z[26];
    z[78]=z[78] + z[80] + z[79];
    z[78]=z[3]*z[78];
    z[75]= - z[75] - z[16];
    z[75]=n<T>(1,3)*z[75] + z[78];
    z[75]=z[7]*z[75];
    z[78]=z[10] - z[6];
    z[78]=z[78]*z[10];
    z[28]=z[78] + z[28];
    z[79]= - z[61]*z[55];
    z[28]=2*z[28] + z[79];
    z[28]=z[28]*z[56];
    z[79]= - z[5]*z[19];
    z[81]=z[45] - z[18];
    z[82]=z[2]*z[81];
    z[21]=z[21] + n<T>(1,3)*z[63] + n<T>(20,3)*z[82] + z[28] + z[75] + z[79] + 2.
   /3.*z[73];
    z[21]=z[8]*z[21];
    z[28]=z[30] - z[34];
    z[63]=z[28]*z[60];
    z[23]=z[23]*z[44];
    z[73]= - z[10]*z[45];
    z[73]=z[73] - z[23];
    z[73]=z[2]*z[73];
    z[63]=z[63] - z[73];
    z[73]=z[30] + z[26];
    z[75]=z[7]*z[10];
    z[79]=z[75] - 1;
    z[82]=n<T>(1,3)*z[4];
    z[79]=z[82]*z[73]*z[79];
    z[83]=z[32] - z[45];
    z[84]=z[16] + n<T>(1,3)*z[83];
    z[85]=z[59]*z[30];
    z[85]=z[1] + z[85];
    z[86]=n<T>(1,3)*z[13];
    z[85]=z[85]*z[86];
    z[63]=z[85] + z[79] - z[84] - n<T>(1,3)*z[63];
    z[63]=z[14]*z[63];
    z[79]=z[29] + z[34];
    z[85]= - z[6]*z[79];
    z[76]=z[85] - z[76];
    z[76]=z[3]*z[76];
    z[85]=z[39]*z[18];
    z[76]=z[76] - z[26] - z[85];
    z[76]=z[76]*z[43];
    z[87]=n<T>(1,3)*z[16];
    z[88]=z[87] - z[18];
    z[88]= - z[10]*z[88];
    z[88]=n<T>(4,3)*z[26] + z[88];
    z[88]=z[88]*z[66];
    z[89]=n<T>(5,3)*z[18];
    z[76]=z[88] + z[76] + z[89] + z[17];
    z[76]=z[4]*z[76];
    z[88]=n<T>(7,3)*z[34] - 2*z[30];
    z[60]=z[88]*z[60];
    z[88]=9*z[16];
    z[90]=5*z[18];
    z[60]=2*z[60] - z[88] - z[90] + n<T>(14,3)*z[45];
    z[60]=z[2]*z[60];
    z[91]=z[18]*z[3];
    z[92]=z[91] + z[1];
    z[93]=npow(z[3],2);
    z[94]= - z[92]*z[93]*z[86];
    z[54]=z[54]*z[43];
    z[54]=z[1] + z[54];
    z[54]=z[3]*z[54];
    z[54]=z[54] + z[94];
    z[54]=z[13]*z[54];
    z[70]= - z[70] - z[79];
    z[70]=z[3]*z[70];
    z[70]= - z[90] + z[70];
    z[70]=z[70]*z[43];
    z[79]=z[62]*z[66];
    z[94]=4*z[10];
    z[54]=8*z[63] + z[54] + z[60] + z[76] + z[79] + z[70] + 3*z[1] + 
    z[94];
    z[54]=z[14]*z[54];
    z[21]=z[21] + z[54];
    z[54]=5*z[16];
    z[60]= - z[54] + z[90] - n<T>(61,3)*z[45];
    z[60]=z[10]*z[60];
    z[63]=z[26] - n<T>(13,3)*z[34];
    z[70]=z[29] + z[30];
    z[76]=z[70]*z[10];
    z[76]=z[76] + z[24];
    z[79]=5*z[5];
    z[79]= - z[76]*z[79];
    z[60]=z[79] + 5*z[63] + z[60];
    z[63]=z[26]*z[10];
    z[79]= - n<T>(41,3)*z[44] - 10*z[63];
    z[59]=z[79]*z[22]*z[59];
    z[79]=z[6]*z[27];
    z[59]=z[79] + z[59];
    z[59]=z[11]*z[59];
    z[79]= - n<T>(73,3)*z[44] - 7*z[52];
    z[79]=z[3]*z[79];
    z[95]=z[32] + z[16];
    z[95]=z[95]*z[10];
    z[95]=z[95] + z[26];
    z[96]=10*z[75];
    z[97]= - z[95]*z[96];
    z[59]=z[59] + z[97] + 2*z[60] + z[79];
    z[59]=z[59]*z[82];
    z[60]= - n<T>(32,3)*z[24] - z[58];
    z[79]=z[5]*npow(z[1],6);
    z[38]=z[79] - z[38];
    z[79]=n<T>(17,3)*z[3];
    z[38]=z[38]*z[79];
    z[97]=z[37]*z[5];
    z[38]=z[38] + 2*z[60] + n<T>(43,3)*z[97];
    z[38]=z[3]*z[38];
    z[60]=11*z[24] + 10*z[97];
    z[60]=z[5]*z[60];
    z[38]=z[38] + z[60] + 55*z[26] - 23*z[34];
    z[38]=z[38]*z[43];
    z[60]=z[27]*z[7];
    z[98]= - z[26]*z[60];
    z[99]=npow(z[7],2);
    z[100]=z[99]*z[11];
    z[100]=10*z[100];
    z[101]=npow(z[16],4)*z[100];
    z[98]=z[98] + z[101];
    z[98]=z[11]*z[98];
    z[101]=112*z[18] + z[78];
    z[102]=n<T>(1,3)*z[10];
    z[101]=z[101]*z[102];
    z[101]= - 35*z[34] + z[101];
    z[101]=z[10]*z[101];
    z[103]=npow(z[10],5);
    z[104]=n<T>(1,3)*z[7];
    z[105]= - z[103]*z[104];
    z[98]=z[98] + z[101] + z[105];
    z[98]=z[98]*z[56];
    z[101]= - z[39] + n<T>(11,9)*z[10];
    z[101]=z[101]*z[22];
    z[105]=npow(z[10],4);
    z[106]=z[105]*z[7];
    z[98]=z[98] + z[101] - n<T>(11,9)*z[106];
    z[98]=z[11]*z[98];
    z[17]=n<T>(23,9)*z[18] + z[17];
    z[17]=z[10]*z[17];
    z[101]=z[26] + n<T>(1,3)*z[30];
    z[101]=z[101]*z[10];
    z[101]=z[101] + z[24];
    z[107]=z[5]*z[101];
    z[17]=10*z[107] + n<T>(17,9)*z[26] + 2*z[17];
    z[17]=z[5]*z[17];
    z[107]=100*z[18] + 53*z[16];
    z[107]=z[10]*z[107];
    z[108]=z[10]*z[62];
    z[80]=z[80] + z[108];
    z[80]=z[80]*z[96];
    z[80]=z[80] + 6*z[26] + n<T>(1,9)*z[107];
    z[80]=z[7]*z[80];
    z[17]=z[59] + z[98] + z[80] + z[38] + z[17] + 37*z[16] + n<T>(443,9)*
    z[18] - 16*z[45];
    z[17]=z[4]*z[17];
    z[38]= - z[24] + z[44];
    z[59]=z[87] + z[65];
    z[80]= - z[18] - z[59];
    z[80]=z[10]*z[80];
    z[80]= - z[26] + z[80];
    z[80]=z[10]*z[80];
    z[38]=n<T>(1,3)*z[38] + z[80];
    z[80]=z[27]*npow(z[11],4);
    z[80]= - n<T>(1,3)*z[80] + z[43];
    z[36]=z[36]*z[80];
    z[80]=z[10]*z[101];
    z[46]=z[46] + z[80];
    z[46]=z[5]*z[46];
    z[80]=z[87] + z[18];
    z[80]=z[10]*z[80];
    z[80]=z[26] + z[80];
    z[80]=z[10]*z[80];
    z[47]=z[47] + z[80];
    z[47]=z[47]*z[75];
    z[36]=z[47] + 2*z[38] + z[46] + z[36];
    z[36]=z[4]*z[36];
    z[38]=z[5]*z[76];
    z[46]=z[95]*z[75];
    z[38]=z[38] + z[46];
    z[46]= - n<T>(13,3)*z[16] - z[35] - n<T>(10,3)*z[45];
    z[46]=z[10]*z[46];
    z[23]=z[23] - z[34];
    z[47]=z[3]*z[51];
    z[23]=5*z[36] + n<T>(5,3)*z[47] + z[46] - z[26] - n<T>(10,3)*z[23] + 3*z[38]
   ;
    z[23]=z[4]*z[23];
    z[36]=z[73]*z[5];
    z[38]=z[62]*z[75];
    z[38]=z[36] + z[38];
    z[46]=z[67] - z[45] + z[62];
    z[38]=10*z[33] - 5*z[46] + 4*z[38];
    z[23]=n<T>(1,3)*z[38] + z[23];
    z[23]=z[2]*z[23];
    z[38]=n<T>(13,3)*z[25] - 5*z[26];
    z[46]= - z[24] + z[97];
    z[46]=z[46]*z[79];
    z[38]=2*z[38] + z[46];
    z[38]=z[3]*z[38];
    z[46]=211*z[18] - 89*z[45];
    z[47]=n<T>(37,3)*z[26] + 10*z[25];
    z[47]=z[5]*z[47];
    z[38]=z[38] + n<T>(1,3)*z[46] + z[47];
    z[38]=z[3]*z[38];
    z[46]=z[70]*z[5];
    z[47]=z[89] + z[16];
    z[47]=7*z[47] + 5*z[46];
    z[47]=z[5]*z[47];
    z[51]=41*z[6];
    z[67]=52*z[10] + 76*z[1] - z[51];
    z[47]=n<T>(1,3)*z[67] + z[47];
    z[38]=2*z[47] + z[38];
    z[47]=z[84]*z[96];
    z[67]=n<T>(19,3)*z[45];
    z[47]=z[47] + n<T>(43,3)*z[16] + 12*z[18] - z[67];
    z[47]=z[7]*z[47];
    z[70]=z[78] + z[41];
    z[70]=z[40] - n<T>(1,9)*z[70];
    z[70]=z[10]*z[70];
    z[70]= - n<T>(5,3)*z[34] + z[70];
    z[70]=z[10]*z[70];
    z[70]=z[77] + z[70];
    z[70]=z[7]*z[70];
    z[76]= - z[44] + z[63];
    z[76]=z[76]*z[99]*z[55];
    z[70]=z[70] + n<T>(10,3)*z[76];
    z[70]=z[11]*z[70];
    z[76]= - 8*z[41] - 11*z[78];
    z[76]=z[76]*z[75];
    z[78]=z[10]*z[6];
    z[76]= - 11*z[78] + n<T>(1,3)*z[76];
    z[70]=n<T>(1,3)*z[76] + z[70];
    z[70]=z[11]*z[70];
    z[17]=2*z[23] + z[17] + z[70] + n<T>(1,3)*z[38] + z[47];
    z[17]=z[2]*z[17];
    z[23]= - z[18] + z[59];
    z[23]=z[10]*z[23];
    z[38]=n<T>(5,3)*z[26];
    z[23]=z[23] - z[38] - z[34];
    z[23]=z[23]*z[69];
    z[23]=z[23] + n<T>(46,9)*z[18] + z[31];
    z[47]= - 2*z[24] - z[58];
    z[47]=z[47]*z[57];
    z[52]=z[5]*z[52];
    z[47]= - n<T>(8,3)*z[52] + 3*z[26] + z[47];
    z[47]=z[3]*z[47];
    z[52]=z[44] + z[63];
    z[52]=z[5]*z[52]*z[22];
    z[58]= - z[29]*z[60];
    z[52]=z[52] + z[58];
    z[52]=z[11]*z[52];
    z[58]= - n<T>(8,3)*z[18] - z[22];
    z[58]=z[58]*z[22];
    z[59]=z[7]*z[103];
    z[52]=z[52] + z[58] + z[59];
    z[52]=z[11]*z[52];
    z[52]=4*z[52] - 11*z[27] + 8*z[106];
    z[52]=z[52]*z[56];
    z[58]= - z[35] - 8*z[16];
    z[58]=z[10]*z[58];
    z[58]= - z[29] + z[58];
    z[58]=z[7]*z[58];
    z[23]=z[52] + n<T>(4,3)*z[58] + 2*z[23] + z[47];
    z[23]=z[4]*z[23];
    z[19]=z[19]*z[75];
    z[47]=14*z[18];
    z[19]= - n<T>(10,3)*z[19] + n<T>(7,3)*z[33] - z[47] - 23*z[16];
    z[19]=z[7]*z[19];
    z[52]= - z[26]*z[100];
    z[52]=z[52] - n<T>(14,3);
    z[52]=z[27]*z[52];
    z[30]=z[30] + 5*z[34];
    z[30]=z[5]*z[10]*z[30];
    z[58]=13*z[22];
    z[59]=z[18] + z[58];
    z[63]=z[22]*z[7];
    z[59]=z[59]*z[63];
    z[30]=n<T>(2,3)*z[59] + z[30] + z[52];
    z[30]=z[11]*z[30];
    z[52]=z[5]*z[6];
    z[59]= - n<T>(65,3) + 8*z[52];
    z[59]=z[22]*z[59];
    z[30]=z[30] + n<T>(88,3)*z[60] + z[59];
    z[30]=z[30]*z[56];
    z[46]=z[46] + z[18];
    z[59]=17*z[45];
    z[46]= - z[88] - z[59] - n<T>(10,3)*z[46];
    z[46]=z[5]*z[46];
    z[60]= - n<T>(61,3)*z[26] - z[85];
    z[60]=z[6]*z[60];
    z[60]= - n<T>(43,3)*z[24] + z[60];
    z[60]=z[60]*z[20];
    z[50]= - n<T>(17,3)*z[24] - z[50];
    z[50]=z[6]*z[50];
    z[37]= - n<T>(17,3)*z[37] + z[50];
    z[37]=z[5]*z[37]*z[43];
    z[37]=z[37] + z[29] + z[60];
    z[37]=z[3]*z[37];
    z[50]=7*z[26] - 47*z[34];
    z[50]=n<T>(2,3)*z[50] - 5*z[25];
    z[50]=z[5]*z[50];
    z[50]= - n<T>(47,3)*z[18] + z[50];
    z[37]=n<T>(2,3)*z[50] + z[37];
    z[37]=z[3]*z[37];
    z[50]=73*z[1] - 62*z[10];
    z[19]=z[23] + z[30] + z[19] + z[37] + n<T>(1,9)*z[50] + z[46];
    z[19]=z[4]*z[19];
    z[23]= - z[41]*z[61];
    z[30]= - z[104]*z[77];
    z[23]=z[23] + z[30];
    z[23]=z[11]*z[23];
    z[30]=npow(z[6],3);
    z[37]=z[5]*z[30];
    z[37]=z[41] + z[37];
    z[23]=n<T>(1,3)*z[37] + z[23];
    z[23]=z[11]*z[23];
    z[37]=z[34]*z[71];
    z[46]=z[41]*z[5];
    z[50]= - z[6] - z[46];
    z[37]=n<T>(2,3)*z[50] + z[37];
    z[37]=z[11]*z[37];
    z[50]=z[52] + 1;
    z[60]=z[9]*z[50]*z[56];
    z[70]=static_cast<T>(2)- z[72];
    z[37]=z[60] + n<T>(1,3)*z[70] + z[37];
    z[37]=z[9]*z[37];
    z[60]=z[11]*z[7];
    z[70]=z[34]*z[60];
    z[76]= - z[7]*z[41];
    z[70]= - z[9] + z[70] + z[39] + z[76];
    z[70]=z[11]*z[70];
    z[70]= - static_cast<T>(1)+ z[70];
    z[70]=z[70]*z[86];
    z[76]= - n<T>(1,3)*z[18] + z[45];
    z[76]=z[5]*z[76];
    z[79]=z[48] - z[18];
    z[80]=z[79]*z[104];
    z[84]=z[9]*z[1];
    z[84]=z[84] - z[81];
    z[84]=z[8]*z[84];
    z[86]=2*z[1];
    z[87]=z[86] - z[6];
    z[23]=z[70] + n<T>(2,3)*z[84] + z[37] + z[23] + z[80] + n<T>(1,3)*z[87] + 
    z[76];
    z[23]=z[15]*z[23];
    z[37]=2*z[41];
    z[70]=n<T>(19,3)*z[18] - z[37];
    z[70]=z[70]*z[37];
    z[76]= - z[18] - z[37];
    z[76]=z[76]*z[78];
    z[80]=npow(z[6],5);
    z[84]=npow(z[6],4);
    z[87]= - z[10]*z[84];
    z[87]= - z[80] + z[87];
    z[87]=z[87]*z[57];
    z[70]=z[87] + z[70] + z[76];
    z[70]=z[5]*z[70];
    z[76]=5*z[10];
    z[28]= - z[28]*z[76];
    z[28]= - 8*z[77] + z[28];
    z[28]=z[28]*z[104];
    z[77]= - z[6] + 7*z[10];
    z[77]=z[10]*z[77];
    z[77]= - 5*z[41] + z[77];
    z[77]=z[77]*z[102];
    z[77]= - 8*z[30] + z[77];
    z[87]=3*z[41];
    z[88]=z[91]*z[87];
    z[28]=z[28] + z[88] + n<T>(1,3)*z[77] + z[70];
    z[28]=z[7]*z[28];
    z[70]=z[41]*z[10];
    z[77]=z[70]*z[26];
    z[88]=z[30]*z[26];
    z[89]= - n<T>(7,3)*z[88] - 5*z[77];
    z[95]=npow(z[5],2);
    z[89]=z[89]*z[95];
    z[77]= - z[88] - z[77];
    z[77]=z[7]*z[77]*z[57];
    z[96]=z[33]*z[5];
    z[97]=z[96]*z[30];
    z[77]=z[77] + z[89] + 5*z[97];
    z[77]=z[77]*z[66];
    z[89]=npow(z[45],4)*z[60];
    z[88]=n<T>(16,3)*z[89] - 8*z[88];
    z[88]=z[95]*z[88];
    z[88]=n<T>(13,3)*z[97] + z[88];
    z[88]=z[3]*z[88];
    z[77]=z[77] + z[88];
    z[77]=z[11]*z[77];
    z[88]=z[18] + n<T>(1,3)*z[41];
    z[88]=z[88]*z[41];
    z[89]=z[40] + n<T>(2,3)*z[41];
    z[89]=z[89]*z[78];
    z[88]=4*z[88] + z[89];
    z[88]=z[5]*z[88];
    z[89]=4*z[30];
    z[88]=z[88] + z[89] + z[70];
    z[88]=z[88]*z[69];
    z[97]= - 7*z[61] + 13*z[91];
    z[64]=z[97]*z[64];
    z[78]=n<T>(5,3)*z[78] + 16*z[41];
    z[28]=z[77] + z[28] + z[64] + n<T>(1,3)*z[78] + z[88];
    z[28]=z[11]*z[28];
    z[64]=z[10]*z[30];
    z[64]=z[84] + n<T>(4,3)*z[64];
    z[64]=z[64]*z[69];
    z[64]=z[64] + z[89] + 3*z[70];
    z[64]=z[64]*z[69];
    z[77]= - 17*z[6] + 32*z[10];
    z[77]=z[10]*z[77];
    z[77]=67*z[41] + z[77];
    z[78]= - z[80]*z[20];
    z[78]= - z[84] + z[78];
    z[78]=z[5]*z[78];
    z[78]= - n<T>(2,3)*z[30] + z[78];
    z[80]=4*z[3];
    z[78]=z[78]*z[80];
    z[64]=z[78] + n<T>(1,9)*z[77] + z[64];
    z[64]=z[64]*z[66];
    z[77]= - 19*z[6] - z[94];
    z[77]=z[10]*z[77];
    z[77]= - 89*z[41] + z[77];
    z[70]= - n<T>(5,3)*z[30] - z[70];
    z[78]=8*z[5];
    z[70]=z[70]*z[78];
    z[70]=n<T>(1,3)*z[77] + z[70];
    z[70]=z[5]*z[70];
    z[77]=z[5]*z[84];
    z[77]=9*z[30] + n<T>(16,3)*z[77];
    z[77]=z[5]*z[77];
    z[77]=n<T>(11,3)*z[41] + z[77];
    z[77]=z[3]*z[77];
    z[84]=8*z[10];
    z[88]= - 151*z[6] + z[84];
    z[28]=2*z[28] + z[64] + z[77] + n<T>(1,9)*z[88] + z[70];
    z[28]=z[11]*z[28];
    z[64]= - 49*z[5] - 52*z[3];
    z[64]=z[43]*z[34]*z[64];
    z[70]= - z[90] - z[37];
    z[70]=z[70]*z[52];
    z[70]= - z[37] + n<T>(1,3)*z[70];
    z[70]=z[5]*z[70];
    z[70]= - n<T>(4,3)*z[6] + z[70];
    z[77]=z[26]*z[95];
    z[77]=4*z[77] - 13*z[96];
    z[42]=z[11]*z[77]*z[42];
    z[42]=n<T>(4,3)*z[42] + 4*z[70] + z[64];
    z[42]=z[11]*z[42];
    z[46]=16*z[46] + z[51] - z[84];
    z[46]=z[5]*z[46];
    z[46]=n<T>(17,3) + z[46];
    z[30]= - z[30]*z[57];
    z[30]=z[87] + z[30];
    z[30]=z[5]*z[30];
    z[30]=n<T>(13,3)*z[6] + z[30];
    z[51]=2*z[3];
    z[30]=z[30]*z[51];
    z[30]=z[42] + n<T>(1,3)*z[46] + z[30];
    z[30]=z[11]*z[30];
    z[42]=z[93]*z[44]*z[71];
    z[44]=z[61] + n<T>(26,3)*z[91];
    z[44]=z[3]*z[44];
    z[42]=z[44] + n<T>(26,3)*z[42];
    z[42]=z[11]*z[42];
    z[44]=z[3]*z[50];
    z[42]= - 7*z[44] + z[42];
    z[42]=z[11]*z[42];
    z[44]=z[45] + z[18];
    z[46]= - z[5]*z[44];
    z[46]= - z[1] + z[46];
    z[46]=z[3]*z[46];
    z[46]=z[74] + 17*z[46];
    z[46]=z[46]*z[93];
    z[50]= - 8*z[61] + 17*z[96];
    z[50]=z[3]*z[50];
    z[50]= - z[72] + n<T>(1,9)*z[50];
    z[50]=z[2]*z[3]*z[50];
    z[42]=z[50] + n<T>(1,9)*z[46] + z[42];
    z[42]=z[9]*z[42];
    z[46]=z[6]*z[79];
    z[46]= - z[26] + z[46];
    z[46]=z[5]*z[46];
    z[46]=z[46] + z[79];
    z[50]=n<T>(17,9)*z[3];
    z[46]=z[46]*z[50];
    z[57]= - z[18] + n<T>(179,9)*z[45];
    z[57]=z[5]*z[57];
    z[46]=z[46] + n<T>(17,3)*z[1] + z[57];
    z[46]=z[3]*z[46];
    z[48]= - z[18] - z[48];
    z[57]=n<T>(10,3)*z[5];
    z[48]=z[48]*z[57];
    z[48]=21*z[1] + z[48];
    z[48]=z[5]*z[48];
    z[46]=z[46] + static_cast<T>(5)+ z[48];
    z[46]=z[3]*z[46];
    z[25]=z[25]*z[50];
    z[25]=z[25] + z[40] + z[49];
    z[25]=z[3]*z[25];
    z[48]=n<T>(13,3)*z[18] + 10*z[49];
    z[48]=z[5]*z[48];
    z[48]=n<T>(82,3)*z[1] + z[48];
    z[25]=n<T>(1,3)*z[48] + z[25];
    z[25]=z[3]*z[25];
    z[48]=z[68] + 10*z[61];
    z[48]=z[5]*z[48];
    z[48]=n<T>(47,3) + z[48];
    z[49]= - z[1] + z[91];
    z[49]=z[2]*z[49];
    z[25]=n<T>(10,3)*z[49] + n<T>(1,3)*z[48] + z[25];
    z[25]=z[2]*z[25];
    z[48]= - z[5]*z[86];
    z[48]=static_cast<T>(5)+ z[48];
    z[48]=z[5]*z[48];
    z[25]=z[42] + z[25] + z[30] + n<T>(5,3)*z[48] + z[46];
    z[25]=z[9]*z[25];
    z[30]=z[6]*z[44];
    z[42]=z[10]*z[79];
    z[30]=z[42] - z[26] + z[30];
    z[30]=z[30]*z[78];
    z[42]= - z[6]*z[81];
    z[44]=z[6]*z[83];
    z[44]= - z[26] + z[44];
    z[44]=z[44]*z[52];
    z[42]=z[42] + z[44];
    z[42]=z[3]*z[42];
    z[30]=8*z[42] + z[30] - 10*z[16] - z[90] + 13*z[45];
    z[30]=z[30]*z[104];
    z[42]=z[40] - z[65];
    z[42]=z[6]*z[42];
    z[44]= - z[90] + 14*z[45];
    z[44]=z[44]*z[102];
    z[42]=z[44] - z[38] + z[42];
    z[42]=z[42]*z[69];
    z[44]=z[32] - 7*z[45];
    z[31]=z[42] + n<T>(10,3)*z[44] - z[31];
    z[31]=z[5]*z[31];
    z[42]= - 67*z[18] + 20*z[45];
    z[42]=z[6]*z[42];
    z[42]=29*z[26] + z[42];
    z[44]=n<T>(14,3)*z[18] - 5*z[45];
    z[44]=z[6]*z[44];
    z[44]= - z[38] + z[44];
    z[39]=z[5]*z[44]*z[39];
    z[39]=n<T>(1,3)*z[42] + z[39];
    z[39]=z[5]*z[39];
    z[42]= - z[47] + 23*z[45];
    z[39]=n<T>(1,3)*z[42] + z[39];
    z[39]=z[3]*z[39];
    z[42]= - z[76] - 77*z[1] + 13*z[6];
    z[30]=z[30] + z[39] + n<T>(1,9)*z[42] + z[31];
    z[30]=z[30]*z[66];
    z[31]= - z[26]*z[41]*z[60];
    z[31]=z[31] + z[85];
    z[31]=z[93]*z[31];
    z[39]=11*z[6];
    z[42]= - z[39] - z[10];
    z[42]=z[10]*z[42];
    z[41]=23*z[41] + z[42];
    z[34]=z[3]*z[34];
    z[34]=n<T>(1,3)*z[41] + 13*z[34];
    z[34]=z[7]*z[34];
    z[41]= - 46*z[6] + 11*z[10];
    z[31]=z[34] + n<T>(1,3)*z[41] + z[31];
    z[31]=z[11]*z[31];
    z[34]=47*z[6] - 65*z[10];
    z[37]=z[3]*z[37];
    z[34]=n<T>(1,3)*z[34] + z[37];
    z[34]=z[7]*z[34];
    z[37]= - z[18]*z[93];
    z[37]=n<T>(23,3) + z[37];
    z[37]=z[11]*z[37];
    z[37]=z[51] + z[37];
    z[37]=z[9]*z[37];
    z[41]= - z[6]*z[80];
    z[31]=z[37] + z[31] + z[34] - n<T>(47,3) + z[41];
    z[31]=z[56]*z[31];
    z[34]= - z[105]*z[104];
    z[37]=z[7]*z[26]*z[55];
    z[34]=z[34] + z[37];
    z[34]=z[11]*z[34];
    z[27]= - z[27]*z[104];
    z[27]=z[27] + z[34];
    z[27]=z[11]*z[27];
    z[32]=z[32] + z[54];
    z[32]=z[32]*z[104];
    z[27]=z[32] + z[27];
    z[27]=z[4]*z[27];
    z[32]=8*z[18] - z[58];
    z[32]=z[11]*z[32]*z[75];
    z[32]= - 77*z[63] + z[32];
    z[32]=z[32]*z[56];
    z[34]=25*z[1] + z[91];
    z[34]=z[7]*z[34];
    z[32]=z[34] + z[32];
    z[27]=n<T>(1,3)*z[32] + 4*z[27];
    z[27]=z[4]*z[27];
    z[32]=5*z[1];
    z[34]= - z[32] + n<T>(2,3)*z[91];
    z[34]=z[3]*z[34];
    z[34]= - n<T>(13,3) + z[34];
    z[34]=z[3]*z[34];
    z[35]= - z[35] + z[59];
    z[35]=z[3]*z[35];
    z[32]=z[32] + z[35];
    z[32]=z[3]*z[32];
    z[32]= - n<T>(28,3) + z[32];
    z[32]=z[32]*z[104];
    z[27]=z[27] + z[34] + z[32] + z[31];
    z[27]=z[13]*z[27];
    z[31]= - z[62] + z[36];
    z[31]=z[4]*z[10]*z[31];
    z[32]= - z[2]*z[73];
    z[34]=z[18] + z[33];
    z[34]=z[2]*z[34];
    z[34]=z[34] - z[92];
    z[34]=z[9]*z[34];
    z[31]=z[34] + z[32] + z[31] - z[62];
    z[31]=z[12]*z[31];
    z[32]=z[53] + z[16];
    z[34]=z[9]*z[92];
    z[32]=n<T>(2,3)*z[34] + n<T>(1,3)*z[32] + z[33];
    z[32]=z[2]*z[32];
    z[22]= - z[1]*z[22];
    z[22]=z[26] + z[22];
    z[22]=z[5]*z[22];
    z[22]= - 2*z[62] + z[22];
    z[22]=z[22]*z[82];
    z[22]=n<T>(2,3)*z[31] + z[22] + z[32];
    z[22]=z[12]*z[22];
    z[31]=11*z[18] - z[59];
    z[31]=z[6]*z[31];
    z[32]=28*z[18] - z[59];
    z[32]=z[6]*z[32];
    z[32]= - 17*z[26] + z[32];
    z[32]=z[6]*z[32];
    z[24]= - 17*z[24] + z[32];
    z[20]=z[24]*z[20];
    z[20]=z[20] - z[29] + n<T>(1,3)*z[31];
    z[20]=z[20]*z[43];
    z[24]=104*z[18] - 193*z[45];
    z[24]=z[6]*z[24];
    z[24]= - 26*z[26] + z[24];
    z[24]=z[5]*z[24];
    z[20]=z[20] + n<T>(1,9)*z[24] + z[40] - z[67];
    z[20]=z[3]*z[20];
    z[24]= - z[90] + 12*z[45];
    z[24]=z[6]*z[24];
    z[24]= - z[38] + z[24];
    z[24]=z[5]*z[24];
    z[18]=z[24] + n<T>(227,9)*z[18] - 32*z[45];
    z[18]=z[18]*z[69];
    z[18]=z[20] + z[18] - n<T>(28,9)*z[1] - z[39];
    z[18]=z[3]*z[18];
    z[16]= - n<T>(10,3)*z[16] - z[90] + z[45];
    z[16]=z[16]*z[69];
    z[20]=13*z[1] - z[39];
    z[20]=8*z[20] + z[10];
    z[16]=n<T>(1,3)*z[20] + z[16];
    z[16]=z[5]*z[16];

    r +=  - n<T>(41,9) + z[16] + z[17] + z[18] + z[19] + 2*z[21] + 10*z[22]
       + 20*z[23] + z[25] + z[27] + z[28] + z[30];
 
    return r;
}

template double qg_2lNLC_r144(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r144(const std::array<dd_real,31>&);
#endif
