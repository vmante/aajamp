#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1229(const std::array<T,31>& k) {
  T z[31];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[19];
    z[7]=k[11];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[8];
    z[11]=k[3];
    z[12]=z[3]*z[6];
    z[13]=z[4]*z[3];
    z[14]= - z[12] + 2*z[13];
    z[15]=z[9]*z[14];
    z[16]=z[4]*z[8];
    z[17]=z[10] + z[6];
    z[17]=z[9]*z[17];
    z[17]=z[17] + static_cast<T>(2)+ z[16];
    z[17]=z[7]*z[17];
    z[18]=z[9]*z[10];
    z[19]=z[18] + 1;
    z[20]=z[9]*z[19];
    z[20]= - z[8] + z[20];
    z[20]=z[7]*z[20];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[5]*z[20];
    z[15]=z[20] + z[17] + z[15] - 2*z[4];
    z[17]=2*z[5];
    z[15]=z[15]*z[17];
    z[20]=4*z[13];
    z[21]= - z[10] + 3*z[4];
    z[22]=3*z[6] - z[21];
    z[22]=z[7]*z[22];
    z[15]=z[15] + z[22] - 3*z[12] + z[20];
    z[15]=z[5]*z[15];
    z[22]=z[7]*z[4];
    z[22]=z[13] - z[22];
    z[22]=z[6]*z[22];
    z[23]=2*z[22];
    z[15]=z[23] + z[15];
    z[24]=npow(z[2],2);
    z[15]=z[5]*z[15]*z[24];
    z[25]=z[12] - z[13];
    z[26]=z[9]*z[25];
    z[27]= - z[9]*z[6];
    z[27]=z[27] - static_cast<T>(1)- z[16];
    z[27]=z[7]*z[27];
    z[26]=z[27] + z[4] + z[26];
    z[26]=z[5]*z[26];
    z[27]=z[4] - z[6];
    z[28]=z[27]*z[7];
    z[25]=z[28] + z[25];
    z[26]=2*z[25] + z[26];
    z[26]=z[5]*z[26];
    z[26]= - z[23] + z[26];
    z[26]=z[26]*npow(z[5],2);
    z[25]= - z[5]*z[25];
    z[23]=z[23] + z[25];
    z[23]=z[23]*npow(z[5],3);
    z[25]= - z[1]*z[22]*npow(z[5],4);
    z[23]=z[23] + z[25];
    z[23]=z[1]*z[23];
    z[23]=z[26] + z[23];
    z[23]=z[1]*z[24]*z[23];
    z[15]=z[15] + 2*z[23];
    z[15]=z[1]*z[15];
    z[23]=z[7]*z[9];
    z[25]=2*z[18];
    z[26]=z[25] - 1;
    z[26]=z[26]*z[9];
    z[28]=z[8] + z[26];
    z[28]=z[28]*z[23];
    z[29]=npow(z[9],2);
    z[30]= - z[10]*z[29];
    z[28]=z[28] + 2*z[11] + z[30];
    z[28]=z[5]*z[28];
    z[30]= - static_cast<T>(2)+ 3*z[18];
    z[30]=z[9]*z[30];
    z[30]=z[8] + z[30];
    z[30]=z[7]*z[30];
    z[28]=z[28] + z[30] - z[19];
    z[28]=z[28]*z[17];
    z[12]=z[12] - z[20];
    z[12]=z[9]*z[12];
    z[20]=4*z[10] - z[6];
    z[20]=z[9]*z[20];
    z[16]=z[20] - static_cast<T>(3)- z[16];
    z[16]=z[7]*z[16];
    z[12]=z[28] + z[16] + z[12] - z[3] + z[21];
    z[12]=z[5]*z[12];
    z[16]=z[10] + z[27];
    z[16]=z[7]*z[16];
    z[12]=z[12] + z[16] - z[14];
    z[12]=z[5]*z[12];
    z[12]= - n<T>(1,2)*z[22] + z[12];
    z[12]=z[12]*z[24];
    z[12]=z[12] + z[15];
    z[12]=z[1]*z[12];
    z[14]=z[10]*z[8];
    z[15]=z[14] + z[18];
    z[16]= - z[9]*z[15];
    z[16]=z[16] + z[11] + 2*z[8];
    z[16]=z[9]*z[16];
    z[20]= - static_cast<T>(1)+ z[18];
    z[20]=z[9]*z[20];
    z[20]= - z[8] + z[20];
    z[20]=z[7]*z[20]*z[29];
    z[21]=npow(z[11],2);
    z[22]= - z[8]*z[11];
    z[16]=z[20] + z[16] - z[21] + z[22];
    z[16]=z[5]*z[16];
    z[14]= - z[14] - z[19];
    z[14]=z[9]*z[14];
    z[19]= - z[8] + z[26];
    z[19]=z[19]*z[23];
    z[14]=z[16] + z[19] + z[14] + z[11] + z[8];
    z[14]=z[14]*z[17];
    z[16]= - static_cast<T>(1)+ 4*z[18];
    z[16]=z[9]*z[16];
    z[16]= - z[8] + z[16];
    z[16]=z[7]*z[16];
    z[14]=z[14] + z[16] - z[15];
    z[14]=z[5]*z[14];
    z[15]=2*z[9];
    z[15]=z[13]*z[15];
    z[16]=z[7]*z[25];
    z[14]=z[14] + z[16] + z[15] + z[3] - z[4];
    z[14]=z[5]*z[14];
    z[15]=z[7]*z[10];
    z[15]=z[13] + z[15];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[14]=z[14]*z[24];
    z[12]=z[14] + z[12];
    z[12]=z[1]*z[12];
    z[13]= - z[9]*z[13];
    z[13]= - z[3] + z[13];
    z[13]=z[13]*z[24];
    z[12]=n<T>(1,2)*z[13] + z[12];

    r += z[12]*z[1];
 
    return r;
}

template double qg_2lNLC_r1229(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1229(const std::array<dd_real,31>&);
#endif
