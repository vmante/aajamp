#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1411(const std::array<T,31>& k) {
  T z[151];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[5];
    z[9]=k[6];
    z[10]=k[8];
    z[11]=k[15];
    z[12]=k[4];
    z[13]=k[2];
    z[14]=k[3];
    z[15]=npow(z[4],2);
    z[16]=z[15]*z[8];
    z[17]=z[16]*z[11];
    z[18]=npow(z[9],2);
    z[19]=145*z[11] - 38*z[4];
    z[19]=z[4]*z[19];
    z[19]= - 134*z[17] - 9*z[18] + z[19];
    z[19]=z[8]*z[19];
    z[20]=z[18]*z[12];
    z[21]=2*z[6];
    z[22]= - n<T>(23,2)*z[11] - z[21];
    z[23]=z[13]*z[18];
    z[24]=z[4]*z[12];
    z[25]=6*z[24];
    z[26]=static_cast<T>(29)+ z[25];
    z[26]=z[4]*z[26];
    z[19]=z[19] + z[26] - z[20] - n<T>(43,6)*z[23] + 3*z[22] + n<T>(32,3)*z[2];
    z[19]=z[8]*z[19];
    z[22]=4*z[4];
    z[23]=3*z[4];
    z[26]= - 8*z[11] + z[23];
    z[26]=z[26]*z[22];
    z[26]=z[26] + 21*z[17];
    z[26]=z[8]*z[26];
    z[27]=2*z[4];
    z[28]=z[27]*z[12];
    z[29]= - static_cast<T>(5)- z[28];
    z[29]=z[4]*z[29];
    z[26]=8*z[26] + 24*z[29] + 73*z[11] - z[21];
    z[26]=z[8]*z[26];
    z[29]=10*z[24];
    z[30]=static_cast<T>(19)+ z[29];
    z[30]=z[30]*z[27];
    z[31]=2*z[11];
    z[32]=z[31] - z[4];
    z[32]=z[4]*z[32];
    z[32]=z[32] - z[17];
    z[32]=z[8]*z[32];
    z[33]=13*z[11];
    z[30]=18*z[32] - z[33] + z[30];
    z[30]=z[8]*z[30];
    z[32]=npow(z[12],2);
    z[34]=z[32]*z[4];
    z[35]=10*z[34];
    z[36]= - 27*z[12] - z[35];
    z[36]=z[4]*z[36];
    z[30]=z[30] - static_cast<T>(15)+ z[36];
    z[30]=z[8]*z[30];
    z[36]=z[34] + z[12];
    z[30]=3*z[36] + z[30];
    z[37]=4*z[5];
    z[30]=z[30]*z[37];
    z[38]=6*z[4];
    z[39]=z[36]*z[38];
    z[40]=z[12]*z[6];
    z[39]=z[39] + static_cast<T>(6)+ z[40];
    z[26]=z[30] + 4*z[39] + z[26];
    z[26]=z[8]*z[26];
    z[30]=2*z[13];
    z[39]=6*z[34];
    z[26]=z[39] - z[30] + 7*z[12] + z[26];
    z[26]=z[5]*z[26];
    z[41]=n<T>(7,2)*z[3];
    z[42]=3*z[10];
    z[43]=z[41] + z[42];
    z[44]=z[12]*z[3];
    z[45]=z[44]*z[2];
    z[46]=n<T>(5,3)*z[45];
    z[47]= - z[46] + 9*z[2] - z[6] + z[43];
    z[47]=z[12]*z[47];
    z[48]= - static_cast<T>(5)- z[44];
    z[48]=z[12]*z[48];
    z[48]=z[48] - z[39];
    z[48]=z[4]*z[48];
    z[19]=z[26] + z[19] + z[48] - static_cast<T>(8)+ z[47];
    z[19]=z[5]*z[19];
    z[26]=4*z[6];
    z[47]=n<T>(79,2)*z[11] + z[26];
    z[47]=z[47]*z[18];
    z[48]=z[4]*z[11];
    z[49]= - 7*z[18] + 19*z[48];
    z[49]=z[49]*z[27];
    z[47]=n<T>(1,3)*z[47] + z[49];
    z[47]=z[8]*z[47];
    z[49]=z[11]*z[13];
    z[50]=n<T>(3,2)*z[49] + n<T>(4,3)*z[40];
    z[50]=z[18]*z[50];
    z[51]=9*z[11];
    z[52]= - z[51] + z[20];
    z[52]=z[52]*z[23];
    z[47]=z[47] + z[52] + z[50];
    z[47]=z[8]*z[47];
    z[50]=2*z[12];
    z[52]=z[50]*z[6];
    z[53]=z[18]*z[52];
    z[54]=z[2]*z[3];
    z[55]=17*z[54];
    z[56]=8*z[2];
    z[57]= - z[10]*z[56];
    z[53]=z[53] - z[55] + z[57];
    z[57]=n<T>(1,3)*z[12];
    z[53]=z[53]*z[57];
    z[58]=z[11]*npow(z[13],2);
    z[59]=z[18]*z[58];
    z[60]=z[44] - 2;
    z[25]=z[25] - z[60];
    z[25]=z[4]*z[25];
    z[61]=7*z[11];
    z[62]=5*z[6];
    z[19]=z[19] + z[47] + z[25] + z[53] + n<T>(7,2)*z[59] + z[10] - z[56] - 
    z[62] + z[61] - n<T>(13,2)*z[3];
    z[19]=z[5]*z[19];
    z[25]=npow(z[9],3);
    z[47]=z[50]*z[25];
    z[53]=z[2]*z[6];
    z[59]=2*z[53];
    z[63]=z[14]*z[25];
    z[63]=z[47] + z[59] + z[63];
    z[64]= - z[2] - z[10];
    z[64]=2*z[64] + z[23];
    z[65]=8*z[4];
    z[64]=z[64]*z[65];
    z[66]=z[16]*z[10];
    z[67]=24*z[66];
    z[63]=z[67] + n<T>(5,3)*z[63] + z[64];
    z[63]=z[8]*z[63];
    z[64]=npow(z[14],2);
    z[68]=z[64]*z[25];
    z[69]=2*z[14];
    z[70]=z[25]*z[69];
    z[71]=z[25]*z[12];
    z[70]=z[70] + 13*z[71];
    z[72]=n<T>(2,3)*z[12];
    z[70]=z[70]*z[72];
    z[73]=n<T>(5,3)*z[6];
    z[74]=6*z[10];
    z[63]=z[63] - z[38] + z[70] - n<T>(10,3)*z[68] + z[73] - z[74];
    z[63]=z[8]*z[63];
    z[70]=24*z[10];
    z[75]=z[14]*z[71];
    z[68]= - n<T>(13,3)*z[75] - n<T>(20,3)*z[68] + z[70] + 16*z[3] - z[73];
    z[68]=z[12]*z[68];
    z[75]=z[32]*z[27];
    z[76]=5*z[12];
    z[77]=z[76] - z[75];
    z[77]=z[4]*z[77];
    z[78]=z[15]*z[10];
    z[79]=2*z[8];
    z[80]=z[79]*z[78];
    z[81]= - z[10] + z[27];
    z[81]=z[4]*z[81];
    z[80]=z[81] + z[80];
    z[80]=z[8]*z[80];
    z[81]= - static_cast<T>(1)- z[28];
    z[81]=z[4]*z[81];
    z[80]=z[81] + z[80];
    z[80]=z[80]*z[79];
    z[81]=z[12]*z[10];
    z[82]=3*z[81];
    z[77]=z[80] + z[77] + static_cast<T>(2)+ z[82];
    z[77]=z[8]*z[77];
    z[80]= - static_cast<T>(1)- z[82];
    z[80]=z[12]*z[80];
    z[80]=z[14] + z[80];
    z[77]=z[77] + 2*z[80] + z[34];
    z[80]=2*z[7];
    z[77]=z[77]*z[80];
    z[83]=npow(z[14],3);
    z[84]=z[83]*z[25];
    z[85]=static_cast<T>(6)- z[40];
    z[85]=z[12]*z[85];
    z[85]=z[85] - z[75];
    z[85]=z[85]*z[27];
    z[86]= - z[31] - z[62];
    z[86]=z[8]*z[86];
    z[86]= - static_cast<T>(7)+ z[86];
    z[86]=z[8]*z[86];
    z[87]=13*z[10];
    z[88]= - 14*z[3] + z[87];
    z[88]=z[12]*z[88];
    z[88]=static_cast<T>(15)+ z[88];
    z[88]=z[12]*z[88];
    z[86]=z[88] + z[86];
    z[86]=z[5]*z[86];
    z[88]=2*z[3];
    z[89]=z[88]*z[14];
    z[90]= - n<T>(7,3) + z[89];
    z[63]=z[77] + z[86] + z[63] + z[85] + z[68] + 2*z[90] - n<T>(7,3)*z[84];
    z[63]=z[7]*z[63];
    z[68]=z[25]*z[81];
    z[77]=z[22]*z[10];
    z[84]= - z[71] + z[77];
    z[85]=7*z[4];
    z[84]=z[84]*z[85];
    z[86]=z[25]*z[10];
    z[90]=z[25]*z[4];
    z[91]= - z[86] - z[90];
    z[91]=z[8]*z[91];
    z[68]=25*z[91] - n<T>(53,6)*z[68] + z[84];
    z[68]=z[8]*z[68];
    z[84]=z[25]*z[32];
    z[91]=16*z[4];
    z[92]= - 16*z[2] - 9*z[10];
    z[92]=z[91] + 2*z[92] - 5*z[84];
    z[92]=z[4]*z[92];
    z[93]=z[10]*z[84];
    z[93]=53*z[53] - n<T>(11,2)*z[93];
    z[68]=z[68] + n<T>(1,3)*z[93] + z[92];
    z[68]=z[8]*z[68];
    z[92]=11*z[10];
    z[93]=37*z[3] + z[92];
    z[93]=z[12]*z[93];
    z[93]=static_cast<T>(24)+ n<T>(1,2)*z[93];
    z[93]=z[12]*z[93];
    z[94]= - 11*z[11] - 37*z[6];
    z[94]=z[8]*z[94];
    z[94]= - static_cast<T>(24)+ n<T>(1,2)*z[94];
    z[94]=z[8]*z[94];
    z[93]=z[93] + z[94];
    z[93]=z[5]*z[93];
    z[94]= - 25*z[3] - n<T>(13,2)*z[10];
    z[94]=z[12]*z[94];
    z[95]=6*z[11];
    z[96]=z[95] - n<T>(11,2)*z[6];
    z[96]=z[8]*z[96];
    z[93]=z[93] + z[96] - static_cast<T>(30)+ z[94];
    z[93]=z[5]*z[93];
    z[94]=z[40] + 2;
    z[96]=z[94]*z[28];
    z[96]=z[96] - static_cast<T>(4)- 3*z[40];
    z[96]=z[4]*z[96];
    z[97]=4*z[3];
    z[63]=z[63] + z[93] + z[68] + z[96] - 12*z[10] + z[97] + n<T>(53,6)*z[6]
   ;
    z[63]=z[7]*z[63];
    z[68]=5*z[3];
    z[93]=2*z[10];
    z[96]=z[68] + z[93];
    z[96]=z[12]*z[96];
    z[96]= - static_cast<T>(9)+ z[96];
    z[96]=z[12]*z[96];
    z[98]=14*z[6];
    z[99]= - z[33] + z[98];
    z[99]=z[8]*z[99];
    z[99]=static_cast<T>(1)+ z[99];
    z[99]=z[8]*z[99];
    z[96]=z[96] + z[99];
    z[96]=z[5]*z[96];
    z[99]= - n<T>(19,2)*z[3] - z[10];
    z[99]=z[99]*z[76];
    z[100]=n<T>(39,2)*z[11] + 8*z[6];
    z[100]=z[8]*z[100];
    z[96]=z[96] + z[100] - static_cast<T>(2)+ z[99];
    z[96]=z[5]*z[96];
    z[99]=n<T>(1,2)*z[10];
    z[96]=z[96] - z[99] + z[98] - z[95] + n<T>(73,2)*z[3];
    z[96]=z[5]*z[96];
    z[98]= - z[93] - z[6] - z[56];
    z[100]=static_cast<T>(1)- z[40];
    z[100]=z[100]*z[22];
    z[98]=3*z[98] + z[100];
    z[98]=z[4]*z[98];
    z[47]=z[47]*z[15];
    z[100]=8*z[90];
    z[86]=43*z[86] + z[100];
    z[101]=z[8]*z[4];
    z[86]=z[86]*z[101];
    z[86]=z[47] + z[86];
    z[86]=z[8]*z[86];
    z[86]=16*z[78] + z[86];
    z[86]=z[8]*z[86];
    z[102]= - z[31] + 23*z[6];
    z[102]=z[2]*z[102];
    z[63]=z[63] + z[96] + z[86] + n<T>(2,3)*z[102] + z[98];
    z[63]=z[7]*z[63];
    z[86]=npow(z[18],2);
    z[96]=z[64]*z[86];
    z[98]=z[86]*z[12];
    z[102]= - z[14]*z[86];
    z[102]=z[102] + z[98];
    z[103]=3*z[12];
    z[102]=z[102]*z[103];
    z[104]=z[2] - z[27];
    z[104]=z[104]*z[27];
    z[105]=4*z[2];
    z[106]=z[16]*z[105];
    z[59]=z[106] + z[104] + z[102] - z[59] - z[96];
    z[59]=z[8]*z[59];
    z[102]= - z[14]*z[98];
    z[96]= - z[96] + z[102];
    z[96]=z[96]*z[103];
    z[102]= - static_cast<T>(1)+ z[28];
    z[102]=z[102]*z[27];
    z[59]=z[59] + z[102] - z[6] + z[96];
    z[59]=z[8]*z[59];
    z[96]=z[83]*z[86];
    z[102]=z[64]*z[98];
    z[96]=z[102] + 2*z[96] - z[42] + z[6];
    z[96]=z[12]*z[96];
    z[102]=z[40] + 1;
    z[104]=z[102]*z[12];
    z[106]= - z[104] + z[75];
    z[106]=z[4]*z[106];
    z[107]=z[8]*z[6];
    z[108]=z[107] + 1;
    z[108]=z[108]*z[8];
    z[109]= - static_cast<T>(1)+ z[82];
    z[109]=z[12]*z[109];
    z[109]=z[109] + z[108];
    z[110]=3*z[5];
    z[109]=z[109]*z[110];
    z[111]=npow(z[14],4)*z[86];
    z[59]=z[109] + z[59] + z[106] + z[111] + z[96];
    z[59]=z[59]*z[80];
    z[96]=z[81] + z[24];
    z[106]= - z[86]*z[96];
    z[109]=z[86]*z[10];
    z[111]=z[86]*z[4];
    z[112]= - z[109] - z[111];
    z[112]=z[8]*z[112];
    z[106]=6*z[106] + 13*z[112];
    z[106]=z[106]*z[79];
    z[112]=z[86]*z[32];
    z[113]=z[10]*z[112];
    z[114]=z[4]*z[2];
    z[114]= - 7*z[112] + 12*z[114];
    z[114]=z[114]*z[27];
    z[106]=z[106] - n<T>(13,3)*z[113] + z[114];
    z[106]=z[8]*z[106];
    z[113]=z[2] - z[4];
    z[113]=z[4]*z[113];
    z[113]= - z[53] + z[113];
    z[106]=24*z[113] + z[106];
    z[106]=z[8]*z[106];
    z[113]=z[88] + z[42];
    z[114]=z[12]*z[113];
    z[115]=3*z[3];
    z[116]=z[115] - z[93];
    z[116]=z[12]*z[116];
    z[116]= - static_cast<T>(5)+ z[116];
    z[116]=z[12]*z[116];
    z[117]=z[11] + z[26];
    z[117]=z[8]*z[117];
    z[117]=static_cast<T>(5)+ z[117];
    z[117]=z[8]*z[117];
    z[116]=z[116] + z[117];
    z[116]=z[116]*z[37];
    z[114]=z[116] - 18*z[107] + static_cast<T>(13)- 12*z[114];
    z[114]=z[5]*z[114];
    z[116]=z[74] + z[97];
    z[117]= - z[6]*z[75];
    z[118]= - static_cast<T>(3)+ z[52];
    z[117]=3*z[118] + z[117];
    z[117]=z[117]*z[27];
    z[59]=z[59] + z[114] + z[106] + z[117] + 15*z[6] + z[116];
    z[59]=z[7]*z[59];
    z[106]=5*z[10];
    z[114]=13*z[3] + z[106];
    z[114]=z[114]*z[50];
    z[117]=19*z[6];
    z[95]= - z[95] - z[117];
    z[95]=z[8]*z[95];
    z[118]=3*z[6];
    z[119]=z[31] - z[118];
    z[119]=z[8]*z[119];
    z[119]= - static_cast<T>(1)+ z[119];
    z[119]=z[8]*z[119];
    z[120]= - z[97] - z[10];
    z[120]=z[12]*z[120];
    z[120]=static_cast<T>(1)+ z[120];
    z[120]=z[12]*z[120];
    z[119]=z[120] + z[119];
    z[37]=z[119]*z[37];
    z[37]=z[37] + z[95] + n<T>(11,2) + z[114];
    z[37]=z[5]*z[37];
    z[95]=18*z[10];
    z[114]=9*z[6];
    z[37]=z[37] + z[95] - 10*z[3] - z[114];
    z[37]=z[5]*z[37];
    z[119]=7*z[2];
    z[120]=z[21] + z[119];
    z[121]= - static_cast<T>(4)+ z[40];
    z[121]=z[121]*z[27];
    z[120]=3*z[120] + z[121];
    z[120]=z[4]*z[120];
    z[120]= - 21*z[53] + z[120];
    z[121]=6*z[15];
    z[122]=z[121]*z[98];
    z[123]=14*z[111];
    z[109]=65*z[109] + z[123];
    z[109]=z[109]*z[101];
    z[109]=z[122] + z[109];
    z[109]=z[8]*z[109];
    z[124]=2*z[15];
    z[125]=z[112]*z[124];
    z[109]=z[125] + z[109];
    z[109]=z[8]*z[109];
    z[126]=z[15]*z[2];
    z[127]=28*z[126];
    z[109]=z[127] + z[109];
    z[109]=z[8]*z[109];
    z[37]=z[59] + z[37] + 2*z[120] + z[109];
    z[37]=z[7]*z[37];
    z[59]=9*z[3] + z[93];
    z[59]=z[12]*z[59];
    z[59]=7*z[107] - static_cast<T>(4)+ z[59];
    z[109]=z[8]*z[11];
    z[120]=z[109] + 1;
    z[128]=z[120]*z[8];
    z[129]=static_cast<T>(3)- z[44];
    z[129]=z[12]*z[129];
    z[129]=z[129] - 3*z[128];
    z[129]=z[129]*z[110];
    z[59]=2*z[59] + z[129];
    z[59]=z[5]*z[59];
    z[59]=z[59] - z[10] - 15*z[3] - z[21];
    z[129]=npow(z[5],2);
    z[59]=z[59]*z[129];
    z[130]=z[6] + z[105];
    z[130]=z[130]*z[27];
    z[130]= - 17*z[53] + z[130];
    z[130]=z[4]*z[130];
    z[131]=z[86]*npow(z[8],4);
    z[131]=11*z[131];
    z[132]= - z[78]*z[131];
    z[59]=z[59] + z[130] + z[132];
    z[37]=2*z[59] + z[37];
    z[37]=z[7]*z[37];
    z[59]=z[105] + z[3];
    z[59]=z[59]*z[27];
    z[130]=7*z[54];
    z[132]= - z[130] + z[59];
    z[132]=z[4]*z[132];
    z[133]=z[8]*z[9];
    z[133]=npow(z[133],7);
    z[134]=z[15]*z[11];
    z[135]= - z[133]*z[134];
    z[132]=z[132] + z[135];
    z[132]=z[5]*z[132];
    z[135]= - z[54]*z[121];
    z[132]=z[135] + z[132];
    z[135]=npow(z[5],3);
    z[132]=z[132]*z[135];
    z[136]=z[2] + z[6];
    z[137]= - z[136]*z[27];
    z[137]=7*z[53] + z[137];
    z[137]=z[4]*z[137];
    z[133]= - z[78]*z[133];
    z[133]=z[137] + z[133];
    z[137]= - z[129]*z[118];
    z[133]=2*z[133] + z[137];
    z[133]=z[7]*z[133];
    z[121]=z[53]*z[121];
    z[137]=z[6]*z[135];
    z[121]=z[121] + z[137];
    z[121]=2*z[121] + z[133];
    z[121]=z[121]*npow(z[7],3);
    z[133]=z[54]*z[15];
    z[137]=npow(z[5],4)*z[133];
    z[138]=z[53]*z[15];
    z[139]= - npow(z[7],4)*z[138];
    z[137]=z[137] + z[139];
    z[137]=z[1]*z[137];
    z[121]=4*z[137] + 2*z[132] + z[121];
    z[121]=z[1]*z[121];
    z[132]=3*z[2];
    z[137]= - z[3] - z[132];
    z[137]=z[4]*z[137];
    z[137]=3*z[54] + z[137];
    z[137]=z[137]*z[23];
    z[139]=npow(z[9],6);
    z[140]=z[124]*z[139];
    z[141]=npow(z[8],6);
    z[142]=z[11]*z[141]*z[140];
    z[137]=z[137] + z[142];
    z[142]=z[101]*z[139];
    z[143]=z[61] - z[27];
    z[143]=z[143]*z[142];
    z[139]=z[139]*z[15];
    z[144]=z[139]*z[50];
    z[143]=z[144] + z[143];
    z[143]=z[8]*z[143];
    z[140]=z[140]*z[32];
    z[143]= - z[140] + z[143];
    z[145]=npow(z[8],3);
    z[143]=z[143]*z[145];
    z[143]=24*z[126] + z[143];
    z[143]=z[8]*z[143];
    z[146]=static_cast<T>(4)+ z[44];
    z[146]=z[146]*z[27];
    z[68]=z[146] - z[68] - 21*z[2];
    z[68]=z[4]*z[68];
    z[68]=5*z[54] + z[68];
    z[68]=2*z[68] + z[143];
    z[68]=z[5]*z[68];
    z[68]=4*z[137] + z[68];
    z[68]=z[5]*z[68];
    z[68]=14*z[133] + z[68];
    z[68]=z[68]*z[129];
    z[137]=7*z[10] + z[27];
    z[137]=z[137]*z[142];
    z[137]=z[144] + z[137];
    z[137]=z[8]*z[137];
    z[137]=z[140] + z[137];
    z[137]=z[137]*z[145];
    z[140]=z[105]*z[15];
    z[137]=z[140] + z[137];
    z[137]=z[8]*z[137];
    z[142]= - z[102]*z[27];
    z[142]=5*z[136] + z[142];
    z[142]=z[4]*z[142];
    z[143]=5*z[53];
    z[142]= - z[143] + z[142];
    z[144]=z[79]*z[6];
    z[144]=z[144] + 1;
    z[144]=z[144]*z[5];
    z[145]=z[21] - z[144];
    z[145]=z[145]*z[110];
    z[137]=z[145] + 2*z[142] + z[137];
    z[137]=z[7]*z[137];
    z[139]= - z[141]*z[93]*z[139];
    z[141]=3*z[53];
    z[136]=z[4]*z[136];
    z[136]= - z[141] + z[136];
    z[136]=z[136]*z[23];
    z[136]=z[136] + z[139];
    z[139]=z[21] + z[144];
    z[139]=z[139]*z[129];
    z[136]=2*z[136] + z[139];
    z[136]=2*z[136] + z[137];
    z[136]=z[7]*z[136];
    z[137]= - z[135]*z[118];
    z[137]= - 7*z[138] + z[137];
    z[136]=2*z[137] + z[136];
    z[136]=z[136]*npow(z[7],2);
    z[68]=z[121] + z[68] + z[136];
    z[68]=z[1]*z[68];
    z[121]=7*z[6];
    z[136]=z[22] - z[121] - z[56];
    z[136]=z[4]*z[136];
    z[136]=8*z[53] + z[136];
    z[137]=npow(z[9],5);
    z[139]=z[137]*z[15];
    z[142]=z[139]*z[50];
    z[144]=z[137]*z[10];
    z[145]=z[137]*z[4];
    z[146]=4*z[144] + z[145];
    z[147]=z[23]*z[8];
    z[146]=z[146]*z[147];
    z[146]=z[142] + z[146];
    z[146]=z[8]*z[146];
    z[148]=z[139]*z[32];
    z[146]=z[148] + z[146];
    z[149]=npow(z[8],2);
    z[146]=z[146]*z[149];
    z[146]= - 6*z[126] + z[146];
    z[146]=z[146]*z[79];
    z[136]=3*z[136] + z[146];
    z[96]= - z[137]*z[96];
    z[146]= - z[144] - z[145];
    z[150]=5*z[8];
    z[146]=z[146]*z[150];
    z[96]=3*z[96] + z[146];
    z[96]=z[8]*z[96];
    z[144]= - z[32]*z[144];
    z[146]=z[137]*z[34];
    z[96]=z[96] + z[144] - 7*z[146];
    z[96]=z[8]*z[96];
    z[96]= - z[140] + z[96];
    z[96]=z[8]*z[96];
    z[144]= - z[132] + z[27];
    z[144]=z[4]*z[144];
    z[141]=z[141] + z[144];
    z[96]=2*z[141] + z[96];
    z[96]=z[8]*z[96];
    z[141]=z[81] - 1;
    z[144]= - z[12]*z[141];
    z[108]=z[144] - z[108];
    z[108]=z[5]*z[108];
    z[108]=z[108] + 3*z[107] + static_cast<T>(1)+ z[81];
    z[108]=z[108]*z[110];
    z[144]= - static_cast<T>(2)+ z[40];
    z[144]=z[144]*z[28];
    z[94]=3*z[94] + z[144];
    z[94]=z[4]*z[94];
    z[94]=z[108] + z[96] - z[114] + z[94];
    z[94]=z[94]*z[80];
    z[96]= - z[3] + z[10];
    z[32]=z[96]*z[32];
    z[96]= - z[11] + z[6];
    z[96]=z[96]*z[149];
    z[32]=z[32] + z[96];
    z[96]=2*z[5];
    z[32]=z[32]*z[96];
    z[108]=z[113]*z[50];
    z[107]=static_cast<T>(1)+ 12*z[107];
    z[32]=z[32] + z[108] + z[107];
    z[32]=z[32]*z[96];
    z[32]=z[32] - z[114] - z[116];
    z[32]=z[5]*z[32];
    z[32]=z[94] + 2*z[136] + z[32];
    z[32]=z[7]*z[32];
    z[94]= - z[26] - z[119];
    z[94]=z[94]*z[27];
    z[94]=35*z[53] + z[94];
    z[94]=z[4]*z[94];
    z[108]=z[139]*npow(z[8],5);
    z[87]= - z[108]*z[87];
    z[87]=z[94] + z[87];
    z[94]=z[115] + z[93];
    z[113]= - z[94]*z[50];
    z[114]= - static_cast<T>(1)+ z[44];
    z[114]=z[12]*z[114];
    z[114]=z[114] + z[128];
    z[114]=z[114]*z[110];
    z[107]=z[114] + z[113] - z[107];
    z[107]=z[107]*z[96];
    z[113]=n<T>(1,2)*z[6];
    z[74]=z[107] - z[74] + 6*z[3] - z[113];
    z[74]=z[74]*z[129];
    z[32]=z[32] + 2*z[87] + z[74];
    z[32]=z[7]*z[32];
    z[74]= - z[44] + z[120];
    z[74]=z[74]*z[110];
    z[74]=z[74] + z[121] + z[94];
    z[74]=z[74]*z[135];
    z[74]=8*z[138] + z[74];
    z[32]=2*z[74] + z[32];
    z[32]=z[7]*z[32];
    z[74]=z[137]*z[11];
    z[87]= - 4*z[74] + z[145];
    z[87]=z[87]*z[147];
    z[87]= - z[142] + z[87];
    z[87]=z[8]*z[87];
    z[87]=z[148] + z[87];
    z[87]=z[87]*z[149];
    z[87]= - 18*z[126] + z[87];
    z[87]=z[87]*z[79];
    z[94]=3*z[49];
    z[107]=z[94] - 7*z[24];
    z[107]=z[137]*z[107];
    z[74]= - 5*z[74] + 7*z[145];
    z[74]=z[8]*z[74];
    z[74]=z[74] + z[107];
    z[74]=z[8]*z[74];
    z[107]= - z[58] + z[34];
    z[107]=z[137]*z[107];
    z[74]=z[74] + z[107];
    z[74]=z[8]*z[74];
    z[107]=3*z[11];
    z[110]= - z[107] + z[56];
    z[110]=z[110]*z[124];
    z[74]=z[110] + z[74];
    z[74]=z[8]*z[74];
    z[110]=14*z[2];
    z[38]=z[38] - z[11] - z[110];
    z[38]=z[38]*z[23];
    z[38]=z[38] + z[74];
    z[38]=z[8]*z[38];
    z[74]=z[44] - 11;
    z[114]= - z[74]*z[28];
    z[116]=3*z[44];
    z[114]=z[114] - static_cast<T>(29)- z[116];
    z[114]=z[4]*z[114];
    z[120]=13*z[2] + 3*z[45];
    z[38]=z[38] + 2*z[120] + z[114];
    z[38]=z[5]*z[38];
    z[114]= - 42*z[4] + 21*z[3] + 80*z[2];
    z[114]=z[4]*z[114];
    z[38]=z[38] + z[87] - 24*z[54] + z[114];
    z[38]=z[5]*z[38];
    z[87]=z[88] + z[119];
    z[87]=z[87]*z[22];
    z[87]= - 35*z[54] + z[87];
    z[87]=z[4]*z[87];
    z[108]= - z[33]*z[108];
    z[38]=z[38] + z[87] + z[108];
    z[38]=z[5]*z[38];
    z[38]= - 8*z[133] + z[38];
    z[38]=z[38]*z[96];
    z[32]=2*z[68] + z[38] + z[32];
    z[32]=z[1]*z[32];
    z[38]= - z[132] + 5*z[11];
    z[38]=z[38]*z[22];
    z[38]=z[112] + z[38];
    z[38]=z[38]*z[27];
    z[68]= - z[94] + 5*z[24];
    z[68]=z[86]*z[68];
    z[87]=z[86]*z[33];
    z[87]=z[87] - 17*z[111];
    z[87]=z[8]*z[87];
    z[68]=2*z[68] + z[87];
    z[68]=z[68]*z[79];
    z[87]=z[86]*z[58];
    z[38]=z[68] + n<T>(13,3)*z[87] + z[38];
    z[38]=z[8]*z[38];
    z[68]=26*z[2] - 21*z[4];
    z[68]=z[68]*z[22];
    z[38]=z[68] + z[38];
    z[38]=z[8]*z[38];
    z[68]=10*z[4];
    z[51]= - z[68] + z[51] - z[110];
    z[51]=z[4]*z[51];
    z[87]= - z[150] + z[13];
    z[87]=z[86]*z[87];
    z[94]= - z[33] + 2*z[2];
    z[108]=z[94]*z[124];
    z[87]=z[108] + z[98] + z[87];
    z[87]=z[8]*z[87];
    z[51]=z[51] + z[87];
    z[51]=z[8]*z[51];
    z[87]= - static_cast<T>(9)+ 50*z[24];
    z[87]=z[4]*z[87];
    z[51]=z[51] + 18*z[2] + z[87];
    z[51]=z[8]*z[51];
    z[87]=5*z[2];
    z[98]=z[87] + z[45];
    z[98]=z[98]*z[50];
    z[108]= - static_cast<T>(19)+ z[44];
    z[108]=z[12]*z[108];
    z[108]=z[108] - z[35];
    z[108]=z[4]*z[108];
    z[51]=z[51] + z[98] + z[108];
    z[51]=z[51]*z[96];
    z[60]=z[60]*z[28];
    z[60]=z[60] + static_cast<T>(53)- 6*z[44];
    z[60]=z[4]*z[60];
    z[60]=z[60] - 12*z[45] - z[10] - z[118] - 44*z[2];
    z[38]=z[51] + 2*z[60] + z[38];
    z[38]=z[5]*z[38];
    z[51]=65*z[86];
    z[51]=z[11]*z[51];
    z[51]=z[51] - z[123];
    z[51]=z[51]*z[101];
    z[51]=z[122] + z[51];
    z[51]=z[8]*z[51];
    z[51]= - z[125] + z[51];
    z[51]=z[8]*z[51];
    z[51]=z[127] + z[51];
    z[51]=z[8]*z[51];
    z[60]= - z[74]*z[27];
    z[74]= - z[88] - 17*z[2];
    z[60]=3*z[74] + z[60];
    z[60]=z[4]*z[60];
    z[60]=21*z[54] + z[60];
    z[38]=z[38] + 2*z[60] + z[51];
    z[38]=z[5]*z[38];
    z[51]=z[55] - z[59];
    z[51]=z[4]*z[51];
    z[55]=z[134]*z[131];
    z[51]=z[51] + z[55];
    z[38]=2*z[51] + z[38];
    z[38]=z[5]*z[38];
    z[51]=z[6] - z[3];
    z[55]= - z[51]*z[140];
    z[32]=z[32] + z[37] + z[55] + z[38];
    z[32]=z[1]*z[32];
    z[37]=z[23] + z[94];
    z[37]=z[37]*z[22];
    z[38]=2*z[25] + 17*z[134];
    z[55]=4*z[8];
    z[38]=z[38]*z[55];
    z[59]=z[13]*z[25];
    z[37]=z[38] + z[37] + z[59] + z[71];
    z[37]=z[8]*z[37];
    z[38]=z[24] - 1;
    z[59]= - z[38]*z[91];
    z[37]=z[37] + z[59] - z[62] - n<T>(53,3)*z[2];
    z[37]=z[8]*z[37];
    z[29]=static_cast<T>(9)+ z[29];
    z[29]=z[29]*z[23];
    z[59]=z[33] - z[68];
    z[59]=z[4]*z[59];
    z[59]=z[59] - 11*z[17];
    z[59]=z[59]*z[79];
    z[29]=z[29] + z[59];
    z[29]=z[8]*z[29];
    z[59]= - 23*z[12] - z[35];
    z[59]=z[4]*z[59];
    z[29]=z[59] + z[29];
    z[29]=z[8]*z[29];
    z[29]=z[75] + z[29];
    z[29]=z[29]*z[96];
    z[46]= - z[46] - z[93] + z[6] - n<T>(34,3)*z[2];
    z[46]=z[12]*z[46];
    z[59]= - static_cast<T>(8)+ z[44];
    z[59]=z[12]*z[59];
    z[59]=z[59] + 8*z[34];
    z[59]=z[4]*z[59];
    z[60]=z[10]*z[13];
    z[29]=z[29] + z[37] + z[59] + z[46] + static_cast<T>(1)+ z[60];
    z[29]=z[29]*z[96];
    z[37]=z[25]*z[49];
    z[46]= - 11*z[71] - 54*z[48];
    z[46]=z[4]*z[46];
    z[48]=z[25]*z[11];
    z[59]= - 25*z[48] + 31*z[90];
    z[59]=z[8]*z[59];
    z[37]=z[59] + n<T>(37,6)*z[37] + z[46];
    z[37]=z[8]*z[37];
    z[46]=z[25]*z[58];
    z[59]=22*z[4] - z[84] + 21*z[11] - 32*z[2];
    z[59]=z[4]*z[59];
    z[37]=z[37] - n<T>(9,2)*z[46] + z[59];
    z[37]=z[8]*z[37];
    z[44]= - static_cast<T>(3)- z[44];
    z[44]=z[44]*z[28];
    z[44]=z[44] - static_cast<T>(19)+ z[116];
    z[44]=z[4]*z[44];
    z[29]=z[29] + z[37] + z[44] + n<T>(53,3)*z[45] - n<T>(5,2)*z[10] + z[113] + 
   n<T>(149,3)*z[2];
    z[29]=z[5]*z[29];
    z[37]= - 45*z[48] + z[100];
    z[37]=z[37]*z[101];
    z[37]= - z[47] + z[37];
    z[37]=z[37]*z[149];
    z[44]=z[105]*z[10];
    z[46]= - 23*z[54] + z[44];
    z[47]=z[3]*z[50];
    z[47]= - static_cast<T>(5)+ z[47];
    z[47]=z[47]*z[27];
    z[48]=z[3] + z[56];
    z[47]=3*z[48] + z[47];
    z[47]=z[4]*z[47];
    z[29]=z[29] + z[37] + n<T>(2,3)*z[46] + z[47];
    z[29]=z[5]*z[29];
    z[37]=z[11] - z[10];
    z[37]=z[37]*z[15]*z[71];
    z[46]= - z[11] - z[10];
    z[25]=z[46]*z[25]*z[16];
    z[25]=z[37] + 4*z[25];
    z[25]=z[25]*z[149];
    z[37]=z[51]*z[105];
    z[46]=z[42] + z[51];
    z[46]=z[4]*z[46];
    z[37]=z[37] + z[46];
    z[37]=z[4]*z[37];
    z[46]= - z[11]*z[54];
    z[47]=z[10]*z[53];
    z[46]=z[46] + z[47];
    z[25]=z[25] + n<T>(4,3)*z[46] + z[37];
    z[25]=z[32] + z[63] + 2*z[25] + z[29];
    z[25]=z[1]*z[25];
    z[29]=z[143] - n<T>(11,2)*z[18];
    z[32]= - 29*z[10] - z[65];
    z[32]=z[32]*z[27];
    z[29]=30*z[66] + n<T>(1,3)*z[29] + z[32];
    z[29]=z[8]*z[29];
    z[32]=static_cast<T>(5)- z[24];
    z[32]=z[32]*z[22];
    z[37]=z[20] + z[6];
    z[46]=z[18]*z[14];
    z[29]=z[29] + z[32] + n<T>(11,3)*z[46] + z[70] + n<T>(37,3)*z[2] - z[107] + 
   n<T>(5,6)*z[37];
    z[29]=z[8]*z[29];
    z[32]= - 8*z[10] - z[23];
    z[32]=z[4]*z[32];
    z[32]=z[32] + 4*z[66];
    z[32]=z[32]*z[55];
    z[37]=15*z[4];
    z[32]=z[32] + z[92] + z[37];
    z[32]=z[8]*z[32];
    z[47]=z[103] - z[34];
    z[48]= - z[47]*z[27];
    z[32]=z[32] + z[48] + static_cast<T>(1)- 6*z[81];
    z[32]=z[32]*z[79];
    z[48]= - z[42] - z[4];
    z[48]=z[48]*z[27];
    z[16]=z[42]*z[16];
    z[16]=z[48] + z[16];
    z[16]=z[8]*z[16];
    z[48]=static_cast<T>(3)+ z[28];
    z[48]=z[4]*z[48];
    z[16]=z[16] + z[10] + z[48];
    z[16]=z[16]*z[79];
    z[48]=9*z[12];
    z[55]= - z[48] + z[75];
    z[55]=z[4]*z[55];
    z[16]=z[16] + z[55] - static_cast<T>(4)- z[82];
    z[16]=z[8]*z[16];
    z[55]=static_cast<T>(2)+ z[81];
    z[55]=z[55]*z[103];
    z[56]=3*z[34];
    z[16]=z[16] - z[56] - 4*z[14] + z[55];
    z[16]=z[8]*z[16];
    z[55]= - z[69] - z[12];
    z[55]=z[55]*z[103];
    z[16]=z[55] + z[16];
    z[16]=z[16]*z[80];
    z[55]=z[64]*z[88];
    z[59]=z[88] - z[42];
    z[59]=z[59]*z[103];
    z[60]=z[3]*z[14];
    z[59]=z[59] + static_cast<T>(3)+ 8*z[60];
    z[59]=z[12]*z[59];
    z[16]=z[16] + z[32] - z[39] + z[59] - 7*z[14] + z[55];
    z[16]=z[7]*z[16];
    z[32]=z[64]*z[18];
    z[46]=n<T>(11,6)*z[20] + 3*z[46] + n<T>(9,2)*z[10] + 11*z[3] - n<T>(13,3)*z[6];
    z[46]=z[12]*z[46];
    z[55]=z[104] + z[75];
    z[55]=z[4]*z[55];
    z[59]= - z[97] - n<T>(3,2)*z[10];
    z[59]=z[12]*z[59];
    z[59]= - n<T>(7,2) + z[59];
    z[59]=z[59]*z[103];
    z[63]=z[107] - n<T>(5,2)*z[6];
    z[63]=z[8]*z[63];
    z[63]= - n<T>(7,2) + z[63];
    z[63]=z[8]*z[63];
    z[59]=z[59] + z[63];
    z[59]=z[5]*z[59];
    z[16]=z[16] + z[59] + z[29] + z[55] + z[46] + n<T>(1,6)*z[32] - n<T>(7,6) + 
    z[89];
    z[16]=z[7]*z[16];
    z[22]= - z[22] - z[95] - z[20];
    z[22]=z[4]*z[22];
    z[29]=z[18]*z[81];
    z[29]= - z[53] - n<T>(1,2)*z[29];
    z[32]=z[4]*z[10];
    z[32]= - z[18] + z[32];
    z[46]=5*z[4];
    z[32]=z[32]*z[46];
    z[55]=z[10]*z[18];
    z[32]= - n<T>(17,3)*z[55] + z[32];
    z[32]=z[32]*z[79];
    z[22]=z[32] + n<T>(17,3)*z[29] + z[22];
    z[22]=z[8]*z[22];
    z[29]= - z[18]*z[89];
    z[32]= - z[3] - z[10];
    z[32]=z[32]*z[20];
    z[29]=z[29] + z[32];
    z[29]=z[29]*z[72];
    z[32]=43*z[3] - z[106];
    z[32]=z[12]*z[32];
    z[55]= - 23*z[11] + z[117];
    z[55]=z[8]*z[55];
    z[32]=z[55] + static_cast<T>(29)+ z[32];
    z[55]=n<T>(5,2)*z[3] - z[42];
    z[55]=z[12]*z[55];
    z[55]= - n<T>(21,2) + z[55];
    z[55]=z[12]*z[55];
    z[26]=n<T>(3,2)*z[11] + z[26];
    z[26]=z[8]*z[26];
    z[26]=n<T>(25,2) + 3*z[26];
    z[26]=z[8]*z[26];
    z[26]=z[55] + z[26];
    z[26]=z[5]*z[26];
    z[26]=n<T>(1,2)*z[32] + z[26];
    z[26]=z[5]*z[26];
    z[32]=z[18]*z[3];
    z[55]=z[64]*z[32];
    z[59]=4*z[24];
    z[63]= - z[59] + z[102];
    z[63]=z[4]*z[63];
    z[16]=z[16] + z[26] + z[22] + z[63] + z[29] - n<T>(4,3)*z[55] + n<T>(7,2)*
    z[10] + z[2] - n<T>(10,3)*z[6] + n<T>(22,3)*z[11] - n<T>(29,2)*z[3];
    z[16]=z[7]*z[16];
    z[22]=z[2] - z[6];
    z[26]=n<T>(2,3)*z[10];
    z[29]=z[22]*z[26];
    z[55]=z[58]*z[32];
    z[58]= - n<T>(1,3)*z[11] - z[51];
    z[58]=z[2]*z[58];
    z[29]= - n<T>(1,3)*z[55] + z[58] + z[29];
    z[31]=z[54]*z[31];
    z[32]=z[49]*z[32];
    z[31]=z[31] + z[32];
    z[32]= - z[3]*z[11];
    z[54]=z[10]*z[6];
    z[32]=z[32] + z[54];
    z[32]=z[32]*z[20];
    z[31]=2*z[31] + z[32];
    z[31]=z[31]*z[57];
    z[29]=2*z[29] + z[31];
    z[31]=z[53]*z[93];
    z[32]=z[18]*z[6];
    z[54]=z[81]*z[32];
    z[31]=z[31] + z[54];
    z[32]=z[32]*z[26];
    z[54]=z[61] + z[106];
    z[18]=z[4]*z[54]*z[18];
    z[18]=z[32] + z[18];
    z[18]=z[18]*z[79];
    z[32]= - z[107] + z[10];
    z[20]=z[32]*z[20];
    z[20]=z[20] + z[77];
    z[20]=z[4]*z[20];
    z[18]=z[18] + n<T>(4,3)*z[31] + z[20];
    z[18]=z[8]*z[18];
    z[20]= - z[27]*z[81];
    z[20]=z[20] - 2*z[51] - z[42];
    z[20]=z[4]*z[20];
    z[16]=z[25] + z[16] + z[19] + z[18] + 2*z[29] + z[20];
    z[16]=z[1]*z[16];
    z[18]=189*z[11] - 46*z[4];
    z[18]=z[4]*z[18];
    z[18]=z[18] - 106*z[17];
    z[18]=z[8]*z[18];
    z[19]=14*z[24];
    z[20]=static_cast<T>(67)+ z[19];
    z[20]=z[4]*z[20];
    z[18]=z[18] + z[20] - 40*z[11] - n<T>(13,2)*z[6];
    z[18]=z[8]*z[18];
    z[20]= - n<T>(67,3) + 43*z[49];
    z[25]= - 19*z[12] - z[39];
    z[25]=z[4]*z[25];
    z[18]=z[18] + z[25] + n<T>(1,2)*z[20] - z[40];
    z[18]=z[8]*z[18];
    z[20]= - 10*z[11] + z[23];
    z[20]=z[20]*z[46];
    z[20]=z[20] + 22*z[17];
    z[20]=z[20]*z[79];
    z[23]=39*z[11];
    z[25]= - static_cast<T>(61)- 16*z[24];
    z[25]=z[4]*z[25];
    z[20]=z[20] + z[25] + z[23] + z[6];
    z[20]=z[20]*z[79];
    z[25]=z[103] + z[34];
    z[25]=z[25]*z[91];
    z[29]=static_cast<T>(12)- 11*z[49];
    z[20]=z[20] + z[25] + 3*z[29] + z[52];
    z[20]=z[8]*z[20];
    z[25]=z[75] - z[13] + z[50];
    z[20]=3*z[25] + z[20];
    z[20]=z[8]*z[20];
    z[23]=z[23] - 14*z[4];
    z[23]=z[4]*z[23];
    z[23]=z[23] - 14*z[17];
    z[23]=z[8]*z[23];
    z[19]=static_cast<T>(39)+ z[19];
    z[19]=z[4]*z[19];
    z[19]=z[23] - 22*z[11] + z[19];
    z[19]=z[8]*z[19];
    z[23]= - 31*z[12] - z[35];
    z[23]=z[4]*z[23];
    z[19]=z[19] + z[23] - static_cast<T>(20)+ 9*z[49];
    z[19]=z[8]*z[19];
    z[23]=z[13] + z[36];
    z[19]=4*z[23] + z[19];
    z[19]=z[19]*z[149]*z[96];
    z[19]=z[20] + z[19];
    z[19]=z[5]*z[19];
    z[18]=z[19] + z[18] - z[75] + z[30] - n<T>(7,3)*z[12];
    z[18]=z[5]*z[18];
    z[19]=z[130] + z[44];
    z[19]=z[19]*z[57];
    z[20]=n<T>(8,3)*z[2];
    z[19]=z[19] + z[20] - z[3] + z[21];
    z[19]=z[12]*z[19];
    z[20]=z[107] + z[20];
    z[21]= - 41*z[11] + z[85];
    z[21]=z[4]*z[21];
    z[17]=z[21] + 30*z[17];
    z[17]=z[17]*z[79];
    z[17]=z[17] + 2*z[20] - z[37];
    z[17]=z[8]*z[17];
    z[20]=n<T>(1,2)*z[3];
    z[21]=z[13]*z[20];
    z[17]=z[18] + z[17] + z[28] + z[19] + z[21] + n<T>(5,6) - 6*z[49];
    z[17]=z[5]*z[17];
    z[18]=z[4]*z[47];
    z[18]=4*z[81] + z[18];
    z[19]=z[149]*z[78];
    z[21]=z[10] + z[4];
    z[19]=7*z[21] - 4*z[19];
    z[19]=z[8]*z[19];
    z[18]=2*z[18] + z[19];
    z[18]=z[18]*z[79];
    z[19]=z[141]*z[48];
    z[18]=z[18] + z[35] - 8*z[14] + z[19];
    z[18]=z[8]*z[18];
    z[19]=static_cast<T>(3)- z[28];
    z[19]=z[4]*z[19];
    z[19]=z[42] + z[19];
    z[21]= - z[42] - z[27];
    z[21]=z[21]*z[101];
    z[19]=2*z[19] + z[21];
    z[19]=z[8]*z[19];
    z[21]=13*z[12] - z[75];
    z[21]=z[4]*z[21];
    z[19]=z[19] + 5*z[81] + z[21];
    z[19]=z[8]*z[19];
    z[19]=z[19] - 8*z[12] + 5*z[34];
    z[19]=z[8]*z[19];
    z[21]=z[12]*z[14];
    z[19]=z[19] + 5*z[64] + 11*z[21];
    z[19]=z[8]*z[19];
    z[21]= - z[12]*z[64];
    z[21]= - z[83] + z[21];
    z[19]=3*z[21] + z[19];
    z[19]=z[19]*z[80];
    z[21]=z[3]*z[83];
    z[21]=2*z[64] + z[21];
    z[23]=z[64]*z[97];
    z[25]= - static_cast<T>(11)+ z[89];
    z[25]=z[12]*z[25];
    z[23]=z[25] - z[14] + z[23];
    z[23]=z[12]*z[23];
    z[18]=z[19] + z[18] + 2*z[21] + z[23];
    z[18]=z[7]*z[18];
    z[19]= - z[76] - z[75];
    z[19]=z[4]*z[19];
    z[21]=z[106] + z[27];
    z[21]=z[21]*z[46];
    z[21]=z[21] - z[67];
    z[21]=z[8]*z[21];
    z[23]=z[4]*z[38];
    z[23]= - z[10] + z[23];
    z[21]=4*z[23] + z[21];
    z[21]=z[8]*z[21];
    z[23]=static_cast<T>(5)+ z[81];
    z[19]=z[21] + n<T>(1,2)*z[23] + z[19];
    z[19]=z[8]*z[19];
    z[20]=z[64]*z[20];
    z[21]=z[12]*z[43];
    z[23]=n<T>(5,3) + z[60];
    z[21]=4*z[23] + z[21];
    z[21]=z[12]*z[21];
    z[18]=z[18] + z[19] + z[56] + z[21] + n<T>(1,3)*z[14] + z[20];
    z[18]=z[7]*z[18];
    z[19]=z[87] + z[33] - z[62];
    z[20]= - z[121] - 4*z[11];
    z[20]=z[2]*z[20];
    z[21]=31*z[10] + z[68];
    z[21]=z[4]*z[21];
    z[20]= - 28*z[66] + n<T>(1,3)*z[20] + z[21];
    z[20]=z[8]*z[20];
    z[21]=z[59] - 7;
    z[23]=z[4]*z[21];
    z[19]=z[20] + z[23] + n<T>(1,3)*z[19] - n<T>(15,2)*z[10];
    z[19]=z[8]*z[19];
    z[20]=z[99] + n<T>(19,3)*z[2] + z[73] - n<T>(13,3)*z[11] - z[115];
    z[20]=z[12]*z[20];
    z[23]= - z[41] + z[42];
    z[23]=z[12]*z[23];
    z[23]=n<T>(3,2) + z[23];
    z[23]=z[12]*z[23];
    z[25]= - z[107] + n<T>(7,2)*z[6];
    z[25]=z[8]*z[25];
    z[25]=n<T>(9,2) + z[25];
    z[25]=z[8]*z[25];
    z[23]=z[23] + z[25];
    z[23]=z[5]*z[23];
    z[25]= - static_cast<T>(17)- 7*z[60];
    z[18]=z[18] + z[23] + z[19] - 3*z[24] + n<T>(1,2)*z[25] + z[20];
    z[18]=z[7]*z[18];
    z[19]=z[53]*z[26];
    z[15]= - z[54]*z[15];
    z[15]=z[19] + z[15];
    z[15]=z[15]*z[79];
    z[19]=z[22]*z[10];
    z[20]= - z[2]*z[11];
    z[20]=z[20] + z[19];
    z[21]= - z[10]*z[21];
    z[21]=15*z[11] + z[21];
    z[21]=z[4]*z[21];
    z[15]=z[15] + n<T>(4,3)*z[20] + z[21];
    z[15]=z[8]*z[15];
    z[20]= - z[45] + z[2];
    z[20]=z[11]*z[20];
    z[19]= - z[19] + z[20];
    z[19]=z[12]*z[19];
    z[20]=z[10]*z[24];
    z[15]=z[16] + z[18] + z[17] + z[15] + z[20] + n<T>(4,3)*z[19] + z[3] - 
    z[99];
    z[15]=z[1]*z[15];
    z[16]= - z[13] + z[14];
    z[16]=z[16]*z[97];
    z[17]=n<T>(1,2) - z[49];
    z[18]=n<T>(7,2)*z[11] + z[97];
    z[18]=z[12]*z[18];
    z[19]= - 7*z[8] + z[69] - z[12];
    z[19]=z[7]*z[19];
    z[16]=z[19] - n<T>(25,2)*z[109] + z[18] + 5*z[17] + z[16];
    z[17]=n<T>(17,6)*z[8] + n<T>(19,3)*z[13] - n<T>(5,2)*z[12];
    z[17]=z[5]*z[17];

    r += z[15] + n<T>(1,3)*z[16] + z[17];
 
    return r;
}

template double qg_2lNLC_r1411(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1411(const std::array<dd_real,31>&);
#endif
