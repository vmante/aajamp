#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r865(const std::array<T,31>& k) {
  T z[103];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[2];
    z[7]=k[4];
    z[8]=k[13];
    z[9]=k[5];
    z[10]=k[7];
    z[11]=k[6];
    z[12]=k[14];
    z[13]=k[9];
    z[14]=k[3];
    z[15]=k[25];
    z[16]=npow(z[2],2);
    z[17]=8*z[16];
    z[18]=z[4]*z[5];
    z[19]=z[18]*z[17];
    z[20]=z[4] - z[5];
    z[21]=2*z[2];
    z[22]= - z[20]*z[21];
    z[22]= - 7*z[18] + z[22];
    z[22]=z[3]*z[2]*z[22];
    z[19]=z[19] + z[22];
    z[19]=z[3]*z[19];
    z[22]=npow(z[4],3);
    z[23]=z[22]*z[5];
    z[24]=7*z[23];
    z[25]=z[2]*z[24];
    z[19]=z[25] + z[19];
    z[19]=z[3]*z[19];
    z[25]=npow(z[3],3);
    z[26]=z[1]*z[18]*z[16]*z[25];
    z[19]=z[19] + 2*z[26];
    z[19]=z[1]*z[19];
    z[26]=npow(z[8],3);
    z[27]=z[22]*z[26];
    z[28]=z[9]*z[10];
    z[29]=z[28]*z[26];
    z[30]= - z[22]*z[29];
    z[30]=z[27] + z[30];
    z[30]=z[9]*z[30];
    z[31]=4*z[4];
    z[32]=3*z[5];
    z[33]=z[32] - z[31];
    z[33]=z[33]*z[21];
    z[33]= - 19*z[18] + z[33];
    z[33]=z[2]*z[33];
    z[34]=z[5]*z[6];
    z[35]=z[34] - 1;
    z[36]=z[35]*z[21];
    z[36]=5*z[20] + z[36];
    z[36]=z[2]*z[36];
    z[36]=5*z[18] + z[36];
    z[36]=z[3]*z[36];
    z[33]=z[33] + z[36];
    z[33]=z[3]*z[33];
    z[36]=npow(z[4],2);
    z[37]=7*z[4];
    z[38]=33*z[5] - z[37];
    z[38]=z[38]*z[36];
    z[39]=z[2]*z[18];
    z[38]=z[38] + 12*z[39];
    z[38]=z[2]*z[38];
    z[33]=z[33] - z[24] + z[38];
    z[33]=z[3]*z[33];
    z[38]=2*z[4];
    z[39]=z[38]*z[16];
    z[40]=z[39]*z[25];
    z[27]= - z[27] - z[40];
    z[27]=z[7]*z[27];
    z[41]= - z[26]*z[36];
    z[42]=z[2]*z[23];
    z[19]=z[19] + z[27] + z[33] + 21*z[42] + z[41] + z[30];
    z[19]=z[1]*z[19];
    z[27]=npow(z[9],2);
    z[30]=z[27]*z[26];
    z[33]=z[21]*z[30];
    z[41]=npow(z[6],2);
    z[42]=z[41]*z[5];
    z[43]=z[25]*z[42];
    z[33]=z[33] + 9*z[43];
    z[43]=npow(z[11],4);
    z[33]=z[33]*z[43];
    z[44]=7*z[34];
    z[45]=2*z[5];
    z[46]=z[7]*z[45];
    z[46]= - z[44] + z[46];
    z[47]=z[43]*z[25];
    z[46]=z[7]*z[47]*z[46];
    z[33]=z[46] - z[40] + z[33];
    z[33]=z[7]*z[33];
    z[40]= - z[2]*z[31];
    z[40]=z[36] + z[40];
    z[40]=z[40]*z[21];
    z[46]=3*z[4];
    z[48]=z[46] - z[21];
    z[48]=z[2]*z[48];
    z[49]=z[32]*z[4];
    z[48]=z[49] + z[48];
    z[48]=z[3]*z[48];
    z[50]=z[36]*z[5];
    z[40]=z[48] + z[50] + z[40];
    z[40]=z[3]*z[40];
    z[48]=z[22]*z[2];
    z[23]= - z[23] - z[48];
    z[23]=7*z[23] + z[40];
    z[23]=z[3]*z[23];
    z[40]=5*z[5];
    z[51]=npow(z[6],3);
    z[52]=z[40]*z[51];
    z[47]= - z[52]*z[47];
    z[53]=5*z[26];
    z[54]= - z[36]*z[53];
    z[23]=z[33] + z[47] + z[54] + z[23];
    z[23]=z[7]*z[23];
    z[33]=z[4] - z[10];
    z[47]=z[8] - z[33];
    z[54]=npow(z[8],2);
    z[47]=z[54]*z[47];
    z[55]=z[4]*z[10];
    z[56]=z[54]*z[55];
    z[57]= - z[10]*z[26];
    z[56]=z[57] + z[56];
    z[56]=z[9]*z[56];
    z[47]=z[47] + z[56];
    z[56]=2*z[9];
    z[47]=z[56]*z[36]*z[47];
    z[57]=26*z[5] - z[37];
    z[57]=z[57]*z[36];
    z[24]=z[24]*z[9];
    z[57]=z[57] + z[24];
    z[58]=8*z[2];
    z[59]=z[18]*z[58];
    z[57]=3*z[57] + z[59];
    z[57]=z[2]*z[57];
    z[59]= - static_cast<T>(3)+ 2*z[34];
    z[59]=z[59]*z[21];
    z[60]=13*z[4];
    z[59]=z[59] - 8*z[5] + z[60];
    z[59]=z[2]*z[59];
    z[61]=z[42] - z[6];
    z[62]=z[61]*z[21];
    z[35]= - 3*z[35] + z[62];
    z[35]=z[2]*z[35];
    z[35]=z[32] + z[35];
    z[35]=z[3]*z[35];
    z[35]=z[35] + 11*z[18] + z[59];
    z[35]=z[3]*z[35];
    z[59]=z[5] - z[38];
    z[59]=z[59]*z[21];
    z[62]=14*z[5] - 11*z[4];
    z[62]=z[4]*z[62];
    z[59]=z[62] + z[59];
    z[62]=3*z[2];
    z[59]=z[59]*z[62];
    z[35]=z[35] - 32*z[50] + z[59];
    z[35]=z[3]*z[35];
    z[59]=npow(z[9],4);
    z[17]= - z[59]*z[55]*z[17];
    z[63]=npow(z[6],4);
    z[64]=z[63]*z[5];
    z[65]=z[25]*z[64];
    z[17]=z[17] + z[65];
    z[17]=z[17]*z[43];
    z[43]= - z[53] - 21*z[50];
    z[43]=z[4]*z[43];
    z[17]=z[19] + z[23] + z[17] + z[35] + z[57] + z[43] + z[47];
    z[17]=z[1]*z[17];
    z[19]=z[58]*z[33];
    z[19]= - z[19] - z[54] - 6*z[36];
    z[19]=z[2]*z[27]*z[19];
    z[23]=npow(z[14],2);
    z[35]=z[23]*z[26];
    z[43]=z[54]*z[9];
    z[47]=4*z[8];
    z[57]=z[10] + z[47];
    z[57]=z[57]*z[43];
    z[65]=z[26]*z[14];
    z[57]= - 20*z[65] + z[57];
    z[57]=z[9]*z[57];
    z[66]=z[62]*z[41];
    z[67]=13*z[42];
    z[68]=z[67] + z[66];
    z[69]=3*z[41];
    z[70]=z[3]*z[69];
    z[68]=2*z[68] + z[70];
    z[70]=npow(z[3],2);
    z[68]=z[68]*z[70];
    z[19]=z[68] + z[19] + 22*z[35] + z[57];
    z[35]=npow(z[11],3);
    z[19]=z[19]*z[35];
    z[57]=4*z[2];
    z[68]=6*z[10];
    z[71]= - z[68] + z[4];
    z[57]=z[71]*z[57];
    z[71]=npow(z[10],2);
    z[72]=2*z[71];
    z[57]=z[57] + z[72] - 3*z[54];
    z[57]=z[2]*z[9]*z[57];
    z[73]= - z[3] - z[2];
    z[74]=3*z[6];
    z[73]=z[74]*z[73];
    z[75]=z[10]*z[14];
    z[73]= - z[75] - 37*z[34] + z[73];
    z[73]=z[73]*z[70];
    z[76]=z[10] - 10*z[8];
    z[43]=z[76]*z[43];
    z[76]=z[5]*z[10];
    z[77]=16*z[5];
    z[78]=z[77] + z[3];
    z[78]=z[3]*z[78];
    z[78]=39*z[76] + z[78];
    z[78]=z[7]*z[3]*z[78];
    z[43]=z[78] + z[73] + z[57] + 11*z[65] + z[43];
    z[43]=z[35]*z[43];
    z[57]= - z[16]*z[70]*z[31];
    z[43]=z[57] + z[43];
    z[43]=z[7]*z[43];
    z[57]=2*z[10];
    z[73]=npow(z[12],2);
    z[78]=z[57]*z[73];
    z[79]=10*z[2];
    z[79]= - z[4]*z[79];
    z[79]= - 13*z[36] + z[79];
    z[79]=z[2]*z[79];
    z[80]=z[4] - z[2];
    z[80]=z[80]*z[21];
    z[80]=z[49] + z[80];
    z[81]=z[3]*z[5];
    z[80]=2*z[80] + z[81];
    z[80]=z[3]*z[80];
    z[79]=z[80] + z[79] + z[78] - 17*z[50];
    z[79]=z[3]*z[79];
    z[80]=11*z[26];
    z[50]= - z[80] - 7*z[50];
    z[50]=z[4]*z[50];
    z[19]=z[43] + z[19] + z[79] + z[50] - 21*z[48];
    z[19]=z[7]*z[19];
    z[43]=2*z[6];
    z[48]= - z[43] + z[42];
    z[48]=z[48]*z[21];
    z[50]=z[34] - 4;
    z[48]=z[48] - z[50];
    z[48]=z[2]*z[48];
    z[79]=z[51]*z[21];
    z[81]=z[5]*z[79];
    z[81]= - z[42] + z[81];
    z[81]=z[2]*z[81];
    z[81]=z[34] + z[81];
    z[81]=z[3]*z[81];
    z[48]=z[81] + z[48] + z[45] - z[46];
    z[48]=z[3]*z[48];
    z[81]= - static_cast<T>(3)+ z[34];
    z[81]=z[81]*z[21];
    z[82]=30*z[5];
    z[83]=npow(z[13],2);
    z[84]=6*z[83];
    z[85]=z[84]*z[6];
    z[81]=z[81] - 53*z[4] + z[82] - 5*z[13] - z[85];
    z[81]=z[2]*z[81];
    z[86]=z[10]*z[12];
    z[87]=z[86] - 2*z[76];
    z[88]=51*z[5];
    z[89]= - z[57] - z[88];
    z[89]=z[4]*z[89];
    z[48]=z[48] + z[81] + 3*z[87] + z[89];
    z[48]=z[3]*z[48];
    z[81]= - z[73]*z[42];
    z[53]= - z[23]*z[53];
    z[87]=z[28] - z[75];
    z[87]=z[36]*z[87];
    z[87]=z[65] + z[87];
    z[87]=z[9]*z[87];
    z[53]=z[87] + z[81] + z[53];
    z[53]=z[53]*z[56];
    z[81]=npow(z[9],3);
    z[87]=z[81]*z[55];
    z[89]=z[81]*z[33];
    z[90]=z[58]*z[89];
    z[87]=15*z[87] + z[90];
    z[87]=z[2]*z[87];
    z[90]= - z[3] - z[62];
    z[90]=z[51]*z[90];
    z[90]= - z[52] + z[90];
    z[90]=z[90]*z[70];
    z[91]=npow(z[14],3);
    z[92]=z[91]*z[80];
    z[53]=z[90] + z[87] + z[92] + z[53];
    z[35]=z[53]*z[35];
    z[53]=z[57]*z[26];
    z[68]=z[54]*z[68];
    z[87]= - z[8]*z[55];
    z[68]=z[68] + z[87];
    z[68]=z[4]*z[68];
    z[68]= - z[53] + z[68];
    z[68]=z[9]*z[68];
    z[87]=3*z[10];
    z[90]=z[87] - z[8];
    z[92]=z[90]*z[54];
    z[93]= - z[57] - 7*z[8];
    z[93]=z[8]*z[93];
    z[93]=z[93] - 14*z[18];
    z[93]=z[4]*z[93];
    z[94]= - z[5]*z[84];
    z[68]=z[68] + z[93] + z[94] + z[92];
    z[92]=z[9]*z[4];
    z[68]=z[68]*z[92];
    z[93]=19*z[5];
    z[94]=z[93] - z[37];
    z[94]=z[94]*z[46];
    z[94]=z[94] + z[72] - z[54];
    z[94]=z[4]*z[94];
    z[24]=z[94] + z[24];
    z[24]=z[9]*z[24];
    z[94]= - z[87] + z[5];
    z[92]=z[94]*z[92];
    z[20]=z[92] - z[20];
    z[20]=z[20]*z[21];
    z[92]= - z[10] + 13*z[5];
    z[92]= - 78*z[4] + 8*z[92] - z[8];
    z[92]=z[4]*z[92];
    z[20]=z[20] + z[24] + z[84] + z[92];
    z[20]=z[2]*z[20];
    z[24]= - z[13]*z[40];
    z[84]=8*z[10];
    z[92]=z[84] + z[8];
    z[92]=z[8]*z[92];
    z[94]=z[10] - 62*z[5];
    z[94]=z[4]*z[94];
    z[24]=z[94] + z[24] + z[92];
    z[24]=z[4]*z[24];
    z[17]=z[17] + z[19] + z[35] + z[48] + z[20] + z[68] + z[24] + z[78]
    - z[80];
    z[17]=z[1]*z[17];
    z[19]=13*z[76] + z[73] - 6*z[86];
    z[20]=5*z[4];
    z[24]= - z[20] - z[21];
    z[24]=z[2]*z[24];
    z[35]= - static_cast<T>(1)- z[34];
    z[35]=z[3]*z[35];
    z[35]=11*z[5] + z[35];
    z[35]=z[3]*z[35];
    z[18]=z[35] + z[24] + 2*z[19] - 9*z[18];
    z[18]=z[3]*z[18];
    z[19]= - z[14] + 15*z[6];
    z[24]=2*z[3];
    z[35]= - z[19]*z[24];
    z[48]=z[2]*z[6];
    z[35]=z[35] - 16*z[48] + z[75] - 9*z[34];
    z[35]=z[3]*z[35];
    z[68]=21*z[8];
    z[80]=2*z[15];
    z[46]=z[46] + z[68] - z[80] - 25*z[10];
    z[46]=z[9]*z[46];
    z[92]= - z[43] + 5*z[9];
    z[92]=z[92]*z[21];
    z[46]=z[46] + z[92];
    z[46]=z[2]*z[46];
    z[92]=9*z[76];
    z[94]= - z[10] + z[47];
    z[94]=z[8]*z[94];
    z[94]=z[92] + z[94];
    z[94]=z[9]*z[94];
    z[95]=z[14]*z[54];
    z[35]=z[35] + z[46] - 24*z[95] + z[94];
    z[46]=npow(z[11],2);
    z[35]=z[35]*z[46];
    z[94]=7*z[10];
    z[95]=8*z[8];
    z[96]=z[58] + z[94] - z[95];
    z[96]=z[2]*z[96];
    z[97]= - z[71] + 21*z[76];
    z[98]=z[10] - z[8];
    z[98]=z[8]*z[98];
    z[99]=15*z[3] + 6*z[2] + 28*z[10] - 43*z[5];
    z[99]=z[3]*z[99];
    z[96]=z[99] + z[96] + 2*z[97] + z[98];
    z[96]=z[96]*z[46];
    z[97]=z[73]*z[10];
    z[16]= - z[4]*z[16];
    z[70]= - z[5]*z[70];
    z[16]=z[70] - z[97] + z[16];
    z[16]=z[16]*z[24];
    z[16]=z[16] + z[96];
    z[16]=z[7]*z[16];
    z[70]= - z[36]*z[40];
    z[70]= - z[78] + z[70];
    z[78]= - z[9]*z[22];
    z[78]= - 2*z[36] + z[78];
    z[58]= - z[4]*z[58];
    z[58]=21*z[78] + z[58];
    z[58]=z[2]*z[58];
    z[16]=z[16] + z[35] + z[18] + 2*z[70] + z[58];
    z[16]=z[7]*z[16];
    z[18]=z[13]*z[45];
    z[18]=z[18] + 3*z[83] - z[71];
    z[35]=15*z[10];
    z[58]=z[35] - z[95];
    z[58]=z[8]*z[58];
    z[70]= - z[37] + z[87] - 22*z[5];
    z[70]=z[4]*z[70];
    z[18]=z[70] + 2*z[18] + z[58];
    z[18]=z[4]*z[18];
    z[58]=z[54]*z[87];
    z[55]= - z[47]*z[55];
    z[55]=z[58] + z[55];
    z[55]=z[4]*z[55];
    z[53]= - z[53] + z[55];
    z[53]=z[9]*z[53];
    z[55]= - z[83]*z[32];
    z[55]= - z[97] + z[55];
    z[58]=z[10] - 6*z[8];
    z[58]=z[58]*z[54];
    z[18]=z[53] + z[18] + 2*z[55] + z[58];
    z[18]=z[9]*z[18];
    z[53]=9*z[10];
    z[55]=5*z[8];
    z[58]= - z[55] + z[93] - z[53] - z[13] + z[85];
    z[70]=12*z[5] - z[37];
    z[70]=z[4]*z[70];
    z[70]=z[70] + 4*z[71] - z[54];
    z[70]=z[4]*z[70];
    z[70]=z[26] + z[70];
    z[70]=z[9]*z[70];
    z[78]= - z[83] + z[71];
    z[95]=3*z[8];
    z[96]=5*z[10];
    z[97]= - z[96] + 28*z[5];
    z[97]= - 57*z[4] + 2*z[97] - z[95];
    z[97]=z[4]*z[97];
    z[70]=z[70] + z[97] + 6*z[78] - z[54];
    z[70]=z[9]*z[70];
    z[78]=z[28]*z[4];
    z[97]= - 8*z[78] - 11*z[10] + z[37];
    z[97]=z[9]*z[97];
    z[97]=static_cast<T>(5)+ z[97];
    z[97]=z[97]*z[21];
    z[58]=z[97] + z[70] + 2*z[58] - 99*z[4];
    z[58]=z[2]*z[58];
    z[70]=z[41]*z[2];
    z[97]= - 4*z[70] - z[74] + 2*z[42];
    z[97]=z[2]*z[97];
    z[98]=z[51] + z[64];
    z[98]=z[98]*z[21];
    z[99]=z[5]*z[51];
    z[98]=z[98] + z[99] - 4*z[41];
    z[98]=z[2]*z[98];
    z[61]=z[98] - z[61];
    z[61]=z[3]*z[61];
    z[98]=3*z[34];
    z[99]=z[98] + 1;
    z[100]=2*z[75];
    z[61]=z[61] + z[97] + z[100] - z[99];
    z[61]=z[3]*z[61];
    z[97]=z[83]*z[74];
    z[101]= - 4*z[13] - z[97];
    z[101]=z[101]*z[43];
    z[102]= - z[6]*z[21];
    z[101]=z[102] + 14*z[34] - static_cast<T>(17)+ z[101];
    z[101]=z[2]*z[101];
    z[97]=z[97] - z[13] + 4*z[12];
    z[102]=static_cast<T>(10)+ z[75];
    z[102]=z[4]*z[102];
    z[61]=z[61] + z[101] + z[102] - z[82] + 2*z[97] + z[10];
    z[61]=z[3]*z[61];
    z[97]=4*z[14];
    z[101]=z[54]*z[97];
    z[102]= - z[6]*z[73];
    z[101]=z[102] + z[101];
    z[102]=z[75]*z[20];
    z[31]= - z[96] - z[31];
    z[31]=z[31]*z[38];
    z[31]= - z[54] + z[31];
    z[31]=z[9]*z[31];
    z[31]=z[31] + 2*z[101] + z[102];
    z[31]=z[9]*z[31];
    z[101]=z[96] - z[37];
    z[27]=z[101]*z[27]*z[62];
    z[19]=z[3]*z[6]*z[19];
    z[19]=z[19] + z[67] + 10*z[70];
    z[19]=z[3]*z[19];
    z[67]=z[23]*z[54];
    z[70]= - z[12]*z[42];
    z[19]=z[19] + z[27] + z[31] + z[70] - 23*z[67];
    z[19]=z[19]*z[46];
    z[27]=z[13] + z[57];
    z[31]= - static_cast<T>(15)- z[75];
    z[31]=z[4]*z[31];
    z[27]=z[31] + 4*z[27] - z[88];
    z[27]=z[4]*z[27];
    z[31]=z[83] + z[73];
    z[46]=3*z[12];
    z[70]=z[46] + z[10];
    z[70]=z[70]*z[10];
    z[31]=3*z[31] - z[70];
    z[83]=3*z[14];
    z[88]=z[83]*z[8];
    z[101]= - static_cast<T>(2)+ z[88];
    z[101]=z[101]*z[55];
    z[101]=17*z[10] + z[101];
    z[101]=z[8]*z[101];
    z[102]=z[84] - 2*z[13] - z[12];
    z[102]=z[5]*z[102];
    z[16]=z[17] + z[16] + z[19] + z[61] + z[58] + z[18] + z[27] + z[101]
    + 2*z[31] + z[102];
    z[16]=z[1]*z[16];
    z[17]= - 2*z[26] - 7*z[22];
    z[17]=z[9]*z[17];
    z[17]=z[17] - 33*z[36] - 6*z[71] + z[54];
    z[17]=z[9]*z[17];
    z[18]=16*z[10];
    z[19]=z[18] - z[20];
    z[19]=z[9]*z[19];
    z[19]= - static_cast<T>(7)+ z[19];
    z[19]=z[19]*z[21];
    z[17]=z[19] + z[17] - z[60] + z[53] + 17*z[8];
    z[17]=z[2]*z[17];
    z[19]=z[32]*z[41];
    z[22]=z[66] - z[43] - z[19];
    z[22]=z[3]*z[22];
    z[27]=z[41]*z[21];
    z[27]=7*z[6] + z[27];
    z[27]=z[2]*z[27];
    z[31]= - static_cast<T>(4)- z[75];
    z[22]=z[22] + z[27] + 3*z[31] + 28*z[34];
    z[22]=z[3]*z[22];
    z[27]=7*z[12];
    z[31]=z[2] + z[77] - z[27] - z[84];
    z[22]=2*z[31] + z[22];
    z[22]=z[3]*z[22];
    z[31]= - z[10] + 9*z[8];
    z[31]=z[31]*z[54];
    z[31]=z[31] + z[29];
    z[31]=z[9]*z[31];
    z[36]=z[86] - 5*z[76];
    z[58]=z[3]*z[99];
    z[58]= - z[93] + z[58];
    z[58]=z[3]*z[58];
    z[36]=9*z[36] + z[58];
    z[36]=z[3]*z[36];
    z[36]=z[39] + z[36];
    z[36]=z[7]*z[36];
    z[39]= - z[73] + z[70];
    z[39]=2*z[39] - 15*z[76];
    z[58]=static_cast<T>(1)- z[88];
    z[58]=z[8]*z[58];
    z[58]= - z[57] + z[58];
    z[58]=z[58]*z[55];
    z[17]=z[36] + z[22] + z[17] + z[31] - z[49] + 2*z[39] + z[58];
    z[17]=z[7]*z[17];
    z[22]=z[43]*z[73];
    z[31]=z[80] + z[13];
    z[36]=2*z[12];
    z[39]= - z[87] - z[22] - z[36] + z[31];
    z[39]=z[5]*z[39];
    z[49]= - z[8]*z[10];
    z[37]=4*z[10] - z[37];
    z[37]=z[4]*z[37];
    z[37]=z[37] - z[72] + z[49];
    z[37]=z[4]*z[37];
    z[49]= - z[10] - z[95];
    z[49]=z[49]*z[54];
    z[29]=z[29] + z[49] + z[37];
    z[29]=z[9]*z[29];
    z[37]=z[46] - z[57];
    z[37]=z[10]*z[37];
    z[49]=z[8]*z[14];
    z[57]= - static_cast<T>(7)+ 9*z[49];
    z[57]=z[8]*z[57];
    z[57]=10*z[10] + z[57];
    z[57]=z[8]*z[57];
    z[58]= - static_cast<T>(35)- z[100];
    z[58]=z[4]*z[58];
    z[58]= - 10*z[5] + z[58];
    z[58]=z[4]*z[58];
    z[29]=z[29] + z[58] + z[57] + z[39] - 4*z[73] + z[37];
    z[29]=z[9]*z[29];
    z[37]= - z[79] - 2*z[41] + z[52];
    z[37]=z[2]*z[37];
    z[39]= - z[51]*z[32];
    z[57]= - 2*z[51] - z[64];
    z[57]=z[57]*z[62];
    z[39]=z[57] + z[41] + z[39];
    z[39]=z[3]*z[39];
    z[57]=4*z[6];
    z[58]=z[14] + z[57];
    z[37]=z[39] + z[37] + 3*z[58] - 4*z[42];
    z[37]=z[3]*z[37];
    z[39]=5*z[12];
    z[58]=z[85] - 8*z[13] + z[39];
    z[58]=z[6]*z[58];
    z[60]= - z[6]*z[13];
    z[60]=static_cast<T>(2)+ z[60];
    z[60]=z[60]*z[48];
    z[61]=3*z[75];
    z[37]=z[37] + 9*z[60] + z[44] - z[61] + static_cast<T>(23)+ z[58];
    z[37]=z[3]*z[37];
    z[44]=z[71]*z[56];
    z[58]=6*z[5];
    z[60]=2*z[8];
    z[44]=z[44] - 12*z[4] - z[60] + z[94] + z[58];
    z[44]=z[4]*z[44];
    z[70]=z[71] + z[54];
    z[44]=2*z[70] + z[44];
    z[44]=z[9]*z[44];
    z[31]=z[44] - 49*z[4] - z[68] + z[58] + 13*z[10] + z[31];
    z[31]=z[9]*z[31];
    z[44]=z[13]*z[57];
    z[44]=6*z[34] - static_cast<T>(27)+ z[44];
    z[58]=z[78] - z[33];
    z[68]=z[9]*z[58];
    z[68]= - static_cast<T>(3)- 8*z[68];
    z[68]=z[9]*z[68];
    z[68]=z[74] + z[68];
    z[68]=z[68]*z[21];
    z[31]=z[68] + 2*z[44] + z[31];
    z[31]=z[2]*z[31];
    z[22]=z[10] + z[22] - z[15] - z[27];
    z[27]=z[23]*z[8];
    z[44]=z[97] - 3*z[27];
    z[44]=z[44]*z[55];
    z[44]=static_cast<T>(22)+ z[44];
    z[44]=z[8]*z[44];
    z[36]=3*z[13] - z[36];
    z[36]=z[6]*z[36];
    z[36]=static_cast<T>(23)+ z[36];
    z[36]=z[5]*z[36];
    z[68]= - static_cast<T>(17)- 5*z[75];
    z[68]=z[4]*z[68];
    z[16]=z[16] + z[17] + z[37] + z[31] + z[29] + z[68] + z[44] + 2*
    z[22] + z[36];
    z[16]=z[1]*z[16];
    z[17]= - 22*z[14] + 21*z[27];
    z[17]=z[8]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[8]*z[17];
    z[22]=z[9]*z[65];
    z[17]= - 6*z[22] + z[17] + z[18] + z[32];
    z[17]=z[9]*z[17];
    z[18]=5*z[6];
    z[22]=z[18] - z[83];
    z[29]= - z[22]*z[74];
    z[31]=4*z[51] - z[64];
    z[31]=z[3]*z[31];
    z[29]=z[31] + z[29] + z[52];
    z[29]=z[3]*z[29];
    z[31]=9*z[14];
    z[18]= - z[42] - z[31] - z[18];
    z[18]=z[29] + 2*z[18] - z[66];
    z[18]=z[3]*z[18];
    z[29]= - z[91]*z[47];
    z[29]=9*z[23] + z[29];
    z[29]=z[8]*z[29];
    z[36]=6*z[14];
    z[29]= - z[36] + z[29];
    z[29]=z[8]*z[29];
    z[37]= - z[83] + 2*z[27];
    z[37]=z[37]*z[8];
    z[37]=z[37] + 1;
    z[44]=z[9]*z[8];
    z[52]=z[37]*z[44];
    z[29]=z[52] + static_cast<T>(1)+ z[29];
    z[29]=z[9]*z[29];
    z[52]=npow(z[14],4);
    z[64]=z[52]*z[8];
    z[64]= - z[64] + 3*z[91];
    z[64]=z[64]*z[8];
    z[66]=3*z[23];
    z[64]=z[64] - z[66];
    z[68]= - z[8]*z[64];
    z[70]=z[74] - z[14];
    z[68]=z[68] + z[70];
    z[29]=2*z[68] + z[29];
    z[68]= - z[36] - z[6];
    z[68]=z[68]*z[74];
    z[71]=z[97]*z[3];
    z[72]=z[41]*z[71];
    z[68]=z[68] + z[72];
    z[68]=z[3]*z[68];
    z[29]=2*z[29] + z[68];
    z[29]=z[11]*z[29];
    z[20]=z[20] - z[15] - z[90];
    z[20]=z[20]*z[56];
    z[20]= - static_cast<T>(5)+ z[20];
    z[20]=z[9]*z[20];
    z[68]= - z[21]*z[89];
    z[20]=z[68] + z[74] + z[20];
    z[20]=z[2]*z[20];
    z[68]=z[91]*z[8];
    z[72]= - z[68] + 2*z[23];
    z[72]=z[72]*z[8];
    z[72]=z[72] - z[14];
    z[73]=z[72]*z[8];
    z[17]=z[29] + z[18] + z[20] + z[17] + 15*z[73] - 5*z[34] + static_cast<T>(29)- 12*
    z[75];
    z[17]=z[11]*z[17];
    z[18]=4*z[49];
    z[20]=static_cast<T>(3)- z[18];
    z[20]=z[20]*z[54];
    z[29]=z[32]*z[10];
    z[79]=z[26]*z[9];
    z[20]=4*z[79] + z[29] + 2*z[20];
    z[20]=z[9]*z[20];
    z[80]=z[56]*z[54];
    z[84]=z[21] - z[87] - z[80];
    z[84]=z[9]*z[84];
    z[84]= - static_cast<T>(6)+ z[84];
    z[84]=z[2]*z[84];
    z[27]=z[27] - z[14];
    z[27]=z[27]*z[54];
    z[85]=z[5] - z[10];
    z[86]= - 3*z[85] + z[27];
    z[88]= - static_cast<T>(3)+ z[98] - z[100];
    z[71]=3*z[88] + z[71];
    z[71]=z[3]*z[71];
    z[20]=z[71] + z[84] + 4*z[86] + z[20];
    z[20]=z[11]*z[20];
    z[71]= - z[21]*z[28];
    z[30]=z[71] + z[87] + z[30];
    z[30]=z[2]*z[30];
    z[26]= - z[26]*z[56];
    z[32]=z[10] - z[32];
    z[71]= - z[3]*z[75];
    z[32]=3*z[32] + z[71];
    z[32]=z[3]*z[32];
    z[26]=z[32] + z[30] + z[26] + 6*z[76] + z[65];
    z[26]=z[11]*z[26];
    z[30]= - static_cast<T>(1)+ 4*z[34];
    z[30]=z[3]*z[30];
    z[30]= - z[40] + z[30];
    z[30]=z[3]*z[30];
    z[30]= - 17*z[76] + z[30];
    z[30]=z[3]*z[30];
    z[25]= - z[5]*z[25];
    z[29]=z[11]*z[3]*z[29];
    z[25]=z[25] + z[29];
    z[25]=z[7]*z[25];
    z[25]=z[25] + z[30] + z[26];
    z[25]=z[7]*z[25];
    z[26]=z[54] - z[79];
    z[26]=z[26]*z[56];
    z[28]= - static_cast<T>(1)+ 5*z[28];
    z[28]=z[28]*z[21];
    z[26]=z[28] - z[53] + z[26];
    z[26]=z[2]*z[26];
    z[19]=z[43] - z[19];
    z[19]=z[19]*z[24];
    z[19]=z[19] + 15*z[34] - static_cast<T>(5)+ z[61];
    z[19]=z[3]*z[19];
    z[28]= - z[53] + z[77];
    z[19]=z[19] + 2*z[28] - z[62];
    z[19]=z[3]*z[19];
    z[19]=z[25] + z[20] + z[19] + z[26] + 7*z[79] - 26*z[76] - 5*z[65];
    z[19]=z[7]*z[11]*z[19];
    z[20]= - 5*z[42] + z[22];
    z[25]=z[51]*z[45];
    z[25]= - z[69] + z[25];
    z[24]=z[25]*z[24];
    z[20]=3*z[20] + z[24];
    z[20]=z[3]*z[20];
    z[20]=z[20] + 6*z[48] - 13*z[34] + static_cast<T>(32)+ 9*z[75];
    z[20]=z[3]*z[20];
    z[24]= - z[37]*z[60];
    z[25]= - static_cast<T>(1)+ z[49];
    z[25]=z[25]*z[80];
    z[24]=z[25] - z[5] + z[24];
    z[24]=z[9]*z[24];
    z[25]= - z[73] + z[50];
    z[24]=2*z[25] + z[24];
    z[25]=z[3]*z[14];
    z[26]= - z[43]*z[25];
    z[28]=2*z[14] + z[6];
    z[26]=z[26] + 3*z[28] - z[42];
    z[28]=3*z[3];
    z[26]=z[26]*z[28];
    z[28]=static_cast<T>(3)+ z[44];
    z[28]=z[9]*z[28];
    z[28]=z[74] + z[28];
    z[28]=z[2]*z[28];
    z[24]=z[26] + 3*z[24] + z[28];
    z[24]=z[11]*z[24];
    z[26]= - static_cast<T>(8)+ 21*z[49];
    z[26]=z[26]*z[54];
    z[26]= - 6*z[79] - z[92] + z[26];
    z[26]=z[9]*z[26];
    z[28]=z[35] - z[47];
    z[28]=z[9]*z[28];
    z[29]=z[9]*z[33];
    z[29]= - static_cast<T>(3)+ z[29];
    z[29]=z[9]*z[29];
    z[29]=z[6] + z[29];
    z[29]=z[29]*z[21];
    z[28]=z[29] + static_cast<T>(6)+ z[28];
    z[28]=z[2]*z[28];
    z[20]=z[24] + z[20] + z[28] + z[26] + 31*z[85] - 15*z[27];
    z[20]=z[11]*z[20];
    z[19]=z[20] + z[19];
    z[19]=z[7]*z[19];
    z[17]=z[19] + z[17] - 31*z[3] + 30*z[2] - 16*z[8] - z[82] + z[39] + 
   22*z[10];
    z[17]=z[7]*z[17];
    z[19]=z[64]*z[60];
    z[20]= - z[72]*z[44];
    z[19]=z[20] + z[14] + z[19];
    z[19]=z[9]*z[19];
    z[20]=z[41]*z[36];
    z[24]= - z[51]*z[25];
    z[20]=z[20] + z[24];
    z[20]=z[3]*z[20];
    z[24]=z[8]*npow(z[14],5);
    z[24]= - 4*z[52] + z[24];
    z[24]=z[8]*z[24];
    z[24]=6*z[91] + z[24];
    z[24]=z[8]*z[24];
    z[25]=z[6]*z[14];
    z[19]=z[20] + z[19] + z[24] - z[66] + z[25];
    z[19]=z[11]*z[19];
    z[20]=static_cast<T>(1)- z[67];
    z[20]=z[8]*z[20];
    z[24]=static_cast<T>(7)+ z[61];
    z[24]=z[4]*z[24];
    z[20]= - 5*z[78] + z[24] + z[20] - z[15] - z[96];
    z[20]=z[20]*z[56];
    z[23]= - 12*z[23] + 7*z[68];
    z[23]=z[8]*z[23];
    z[23]=z[83] + z[23];
    z[23]=z[8]*z[23];
    z[24]=static_cast<T>(2)+ z[61];
    z[20]=z[20] + 2*z[24] + z[23];
    z[20]=z[9]*z[20];
    z[23]= - z[2]*z[59];
    z[23]=3*z[81] + z[23];
    z[21]=z[21]*z[58]*z[23];
    z[23]=z[31] - z[57];
    z[23]=z[6]*z[23];
    z[22]=z[22]*z[41];
    z[24]= - z[3]*z[63];
    z[22]=z[22] + z[24];
    z[22]=z[3]*z[22];
    z[22]=z[23] + z[22];
    z[22]=z[3]*z[22];
    z[23]=z[64]*z[55];
    z[19]=z[19] + z[22] + z[21] + z[20] + z[23] + 17*z[14] + z[43];
    z[19]=z[11]*z[19];
    z[20]= - z[38] + 22*z[8] - z[5] - 18*z[10] + 4*z[15] + z[46];
    z[20]=z[9]*z[20];
    z[21]= - z[12]*z[74];
    z[21]=static_cast<T>(1)+ z[21];
    z[22]= - 23*z[6] - 4*z[9];
    z[22]=z[2]*z[22];
    z[23]=z[3]*z[70];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + 2*z[21] + z[22] + 
      z[23] + 7*z[75];
 
    return r;
}

template double qg_2lNLC_r865(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r865(const std::array<dd_real,31>&);
#endif
