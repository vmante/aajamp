#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r526(const std::array<T,31>& k) {
  T z[162];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[7];
    z[9]=k[5];
    z[10]=k[13];
    z[11]=k[2];
    z[12]=k[14];
    z[13]=k[3];
    z[14]=k[23];
    z[15]=k[9];
    z[16]=k[24];
    z[17]=k[29];
    z[18]=k[18];
    z[19]=7*z[8];
    z[20]=3*z[10];
    z[21]=2*z[3];
    z[22]=z[19] + z[20] - z[21];
    z[22]=z[8]*z[22];
    z[23]=npow(z[16],2);
    z[24]=npow(z[14],2);
    z[25]=z[23] + z[24];
    z[26]=3*z[12];
    z[27]=16*z[17];
    z[28]=z[27] + 23*z[15];
    z[28]=z[28]*z[26];
    z[29]=z[14] + z[26];
    z[29]=z[29]*z[21];
    z[30]=19*z[2];
    z[31]=3*z[3];
    z[32]=z[30] - z[31];
    z[33]=npow(z[2],2);
    z[34]=z[33]*z[6];
    z[35]= - z[32]*z[34];
    z[36]=z[15] - z[14];
    z[37]=z[2] + z[36];
    z[38]=z[6]*z[3];
    z[39]=z[2]*z[38];
    z[37]=3*z[37] + z[39];
    z[39]=2*z[4];
    z[37]=z[37]*z[39];
    z[40]=5*z[10];
    z[41]=z[16]*z[40];
    z[42]=z[10] - z[18];
    z[43]=n<T>(29,2)*z[2] + 14*z[8] + n<T>(33,2)*z[3] - z[42];
    z[43]=z[2]*z[43];
    z[22]=z[37] + z[35] + z[43] + z[22] + z[29] + z[28] + 4*z[25] + 
    z[41];
    z[22]=z[4]*z[22];
    z[25]=z[3]*z[11];
    z[28]=9*z[25];
    z[29]=8*z[17];
    z[35]=z[29]*z[11];
    z[37]=z[35] - z[28];
    z[41]=npow(z[7],3);
    z[43]=3*z[41];
    z[37]=z[37]*z[43];
    z[44]=11*z[3];
    z[45]=12*z[17];
    z[46]= - z[45] + z[44];
    z[46]=z[46]*z[41];
    z[47]=z[41]*z[8];
    z[46]=z[46] - 8*z[47];
    z[46]=z[6]*z[46];
    z[48]=npow(z[8],2);
    z[49]=z[48]*z[3];
    z[37]=z[46] + z[37] + n<T>(31,2)*z[49];
    z[37]=z[6]*z[37];
    z[46]=4*z[17];
    z[50]=npow(z[11],2);
    z[51]= - z[50]*z[46];
    z[52]=z[50]*z[3];
    z[51]=z[51] + 5*z[52];
    z[43]=z[51]*z[43];
    z[51]=13*z[8];
    z[53]=z[51] + z[45] - n<T>(53,2)*z[3];
    z[53]=z[8]*z[53];
    z[37]=z[37] + z[43] + z[53];
    z[37]=z[6]*z[37];
    z[43]=z[50]*z[41];
    z[53]=2*z[43];
    z[54]=3*z[6];
    z[55]=3*z[11];
    z[56]=z[54] - z[55];
    z[56]=z[41]*z[56];
    z[57]=z[8]*z[21];
    z[56]=z[57] + z[56];
    z[57]=2*z[6];
    z[56]=z[56]*z[57];
    z[56]=z[56] + 5*z[8] - z[3] + z[53];
    z[56]=z[6]*z[56];
    z[58]=z[25] + 2;
    z[59]=z[52] + z[11];
    z[60]=z[2]*z[59];
    z[56]=z[56] + z[60] - z[58];
    z[56]=z[5]*z[56];
    z[60]=npow(z[11],3);
    z[61]=z[60]*z[3];
    z[62]= - z[41]*z[61];
    z[63]=z[21]*z[50];
    z[64]=n<T>(13,2)*z[11] + z[63];
    z[64]=z[2]*z[64];
    z[64]= - n<T>(9,2) + z[64];
    z[64]=z[2]*z[64];
    z[65]=6*z[17];
    z[66]=5*z[3];
    z[67]=z[65] - z[66];
    z[67]=z[6]*z[67];
    z[67]=static_cast<T>(6)+ z[67];
    z[67]=z[67]*z[39];
    z[37]=z[56] + z[67] + z[37] + z[64] + z[62] - n<T>(17,2)*z[8];
    z[37]=z[5]*z[37];
    z[56]=n<T>(5,2)*z[8];
    z[62]=4*z[4];
    z[64]=2*z[12];
    z[67]= - 9*z[17] - z[14];
    z[67]=z[62] - n<T>(119,2)*z[2] - z[56] + 12*z[3] - z[64] + 4*z[67] - 65*
    z[15];
    z[67]=z[4]*z[67];
    z[68]=z[3]*z[14];
    z[69]=2*z[24];
    z[68]=z[68] + z[69];
    z[70]=2*z[23];
    z[71]= - z[60]*z[70];
    z[72]= - z[16]*z[61];
    z[71]=z[71] + z[72];
    z[71]=z[71]*z[41];
    z[71]=z[71] - z[68];
    z[72]=10*z[3];
    z[73]= - z[46] + z[15];
    z[73]=3*z[73] + z[12];
    z[73]= - z[56] + 3*z[73] - z[72];
    z[73]=z[8]*z[73];
    z[74]= - z[60]*z[41]*z[21];
    z[75]=7*z[11];
    z[76]=z[75] + 6*z[52];
    z[76]=z[2]*z[76];
    z[77]=z[21]*z[11];
    z[76]=z[76] - n<T>(17,2) + z[77];
    z[76]=z[2]*z[76];
    z[74]=z[76] - n<T>(51,2)*z[3] + z[74];
    z[74]=z[2]*z[74];
    z[76]=6*z[3];
    z[78]=npow(z[8],3);
    z[79]=z[76]*z[78];
    z[80]=z[41]*z[3];
    z[81]=n<T>(29,2)*z[80] + 24*z[47];
    z[82]=z[6]*z[8];
    z[81]=z[81]*z[82];
    z[81]=z[79] + z[81];
    z[81]=z[6]*z[81];
    z[83]=11*z[8];
    z[84]=n<T>(9,2)*z[3] + z[83];
    z[84]=z[84]*z[48];
    z[81]=z[84] + z[81];
    z[81]=z[6]*z[81];
    z[37]=z[37] + z[67] + z[81] + z[74] + 2*z[71] + z[73];
    z[37]=z[5]*z[37];
    z[67]=2*z[8];
    z[71]=z[67]*z[10];
    z[73]=npow(z[10],2);
    z[74]= - z[13]*z[41];
    z[74]= - z[71] - z[73] + z[74];
    z[74]=z[8]*z[74];
    z[81]=npow(z[10],3);
    z[81]=9*z[81];
    z[74]= - z[81] + z[74];
    z[74]=z[8]*z[74];
    z[84]=z[13]*z[14];
    z[85]=z[15]*z[13];
    z[86]=z[84] - z[85];
    z[86]=z[86]*z[41];
    z[87]=z[47]*z[13];
    z[88]=z[41]*z[2];
    z[89]=z[6]*z[88];
    z[86]=z[89] + z[86] + z[87];
    z[86]=z[86]*z[39];
    z[89]=4*z[23];
    z[90]=z[10]*z[16];
    z[91]= - z[89] - 9*z[90];
    z[91]=z[91]*z[73];
    z[92]=4*z[41];
    z[93]=z[24]*z[13];
    z[94]=z[93]*z[92];
    z[95]=12*z[23];
    z[96]=z[95] - z[33];
    z[97]=z[41]*z[6];
    z[96]=z[96]*z[97];
    z[74]=z[86] + z[96] + z[74] + z[91] + z[94];
    z[74]=z[4]*z[74];
    z[86]=2*z[33];
    z[91]=8*z[23];
    z[94]= - z[86] + z[91];
    z[94]=z[41]*z[94];
    z[96]= - z[8]*z[73];
    z[96]= - z[41] + z[96];
    z[96]=z[96]*z[48];
    z[98]= - z[39]*z[47];
    z[94]=z[98] + z[96] + z[94];
    z[94]=z[4]*z[94];
    z[96]=z[73]*z[41];
    z[98]=z[48]*z[41];
    z[99]=z[2]*z[47];
    z[99]=z[99] - z[96] + z[98];
    z[99]=z[2]*z[99];
    z[100]=5*z[12];
    z[101]=npow(z[5],2);
    z[102]= - z[41]*z[101]*z[100];
    z[103]=z[78]*z[41];
    z[94]=z[102] + z[94] + z[103] + z[99];
    z[94]=z[9]*z[94];
    z[99]= - z[41]*z[40];
    z[99]= - z[88] + z[99] - z[47];
    z[99]=z[2]*z[99];
    z[96]=z[99] - 13*z[96] - z[98];
    z[96]=z[2]*z[96];
    z[99]=z[10]*z[14];
    z[102]=17*z[99];
    z[104]= - 12*z[24] - z[102];
    z[104]=z[10]*z[104]*z[41];
    z[96]=z[96] + z[104] - z[103];
    z[96]=z[6]*z[96];
    z[104]=npow(z[12],2);
    z[105]= - z[70] - 7*z[104];
    z[106]=6*z[11];
    z[105]=z[105]*z[41]*z[106];
    z[107]=z[11]*z[12];
    z[108]=z[41]*z[107];
    z[109]=npow(z[15],2);
    z[110]=2*z[109];
    z[111]=z[110] + n<T>(31,2)*z[33];
    z[111]=z[4]*z[111];
    z[112]=z[5]*z[4];
    z[113]=z[112]*z[2];
    z[108]=z[113] - 12*z[108] + z[111];
    z[108]=z[5]*z[108];
    z[111]=z[24] - z[109];
    z[114]=z[111]*z[62];
    z[115]=npow(z[2],3);
    z[116]=z[114] + 9*z[115];
    z[116]=z[4]*z[116];
    z[105]=z[108] + z[105] + z[116];
    z[105]=z[5]*z[105];
    z[108]=z[103]*z[13];
    z[116]=z[15] - z[17];
    z[116]=z[116]*z[104];
    z[117]=24*z[116];
    z[118]= - z[11]*z[41]*z[117];
    z[74]=z[94] + z[105] + z[74] + z[96] + z[118] - z[108];
    z[74]=z[9]*z[74];
    z[94]=npow(z[13],2);
    z[96]=z[24]*z[94]*z[41];
    z[105]=z[15] + z[17];
    z[105]=z[105]*z[12];
    z[105]=z[105] + z[110];
    z[118]=6*z[12];
    z[119]=z[105]*z[118];
    z[120]= - z[16]*z[73];
    z[96]=z[96] + z[120] + z[119];
    z[119]=npow(z[6],2);
    z[120]=z[23]*z[119]*z[92];
    z[121]=z[94]*z[14];
    z[122]=z[94]*z[15];
    z[123]=z[121] - z[122];
    z[124]=z[123]*z[41];
    z[124]= - 2*z[111] + z[124];
    z[124]=z[124]*z[39];
    z[125]=z[8] + z[10];
    z[126]= - z[8]*z[125];
    z[126]= - 6*z[73] + z[126];
    z[126]=z[8]*z[126];
    z[127]=9*z[8];
    z[128]= - 21*z[2] + z[18] - z[127];
    z[128]=z[2]*z[128];
    z[128]= - z[48] + z[128];
    z[128]=z[2]*z[128];
    z[96]=z[124] + z[120] + z[128] + 4*z[96] + z[126];
    z[96]=z[4]*z[96];
    z[43]= - z[118]*z[43];
    z[120]=n<T>(9,2)*z[119];
    z[124]= - z[47]*z[120];
    z[126]=3*z[4];
    z[128]=2*z[15];
    z[129]=z[128] - 5*z[2];
    z[129]=z[129]*z[126];
    z[130]= - z[11] + z[57];
    z[97]=z[130]*z[97];
    z[97]= - z[4] + z[97] + z[8] + z[2];
    z[97]=z[5]*z[97];
    z[56]=z[15] - z[56];
    z[56]=z[8]*z[56];
    z[43]=z[97] + z[129] + z[124] + 11*z[33] + z[43] + z[56];
    z[43]=z[5]*z[43];
    z[56]= - z[91] - 23*z[104];
    z[53]=z[56]*z[53];
    z[56]=z[69] + 23*z[109];
    z[56]=2*z[56];
    z[97]= - 2*z[36] - z[2];
    z[97]=z[4]*z[97];
    z[97]=6*z[97] - z[56] - n<T>(69,2)*z[33];
    z[97]=z[4]*z[97];
    z[98]=z[98]*z[120];
    z[43]=z[43] + z[97] + z[98] + 8*z[115] + z[53] + 5*z[78];
    z[43]=z[5]*z[43];
    z[53]=3*z[73];
    z[97]= - z[41]*z[53];
    z[98]= - z[48]*z[92];
    z[97]=z[97] + z[98];
    z[98]= - z[42]*z[41];
    z[88]= - z[88] + z[98] - 2*z[47];
    z[88]=z[2]*z[88];
    z[88]=2*z[97] + z[88];
    z[88]=z[2]*z[88];
    z[97]= - z[14]*z[73]*z[92];
    z[88]=z[88] + z[97] + z[103];
    z[88]=z[6]*z[88];
    z[88]=z[108] + z[88];
    z[88]=z[6]*z[88];
    z[97]= - z[50]*z[92]*z[116];
    z[98]= - z[8]*z[104];
    z[98]= - 4*z[116] + z[98];
    z[98]=z[8]*z[98];
    z[97]=z[97] + z[98];
    z[43]=z[74] + z[43] + z[96] + 6*z[97] + z[88];
    z[43]=z[9]*z[43];
    z[74]=3*z[25];
    z[88]=npow(z[7],4);
    z[96]= - z[88]*z[74];
    z[97]=z[88]*z[21];
    z[98]=z[88]*z[8];
    z[97]=z[97] - z[98];
    z[97]=z[6]*z[97];
    z[96]=z[96] + z[97];
    z[96]=z[6]*z[96];
    z[97]=z[88]*z[63];
    z[96]=z[97] + z[96];
    z[96]=z[96]*z[57];
    z[97]= - z[88]*z[61];
    z[103]=z[8]*z[3];
    z[96]=z[96] + z[97] - z[103];
    z[96]=z[6]*z[96];
    z[58]=z[2]*z[58];
    z[58]= - z[39] + z[96] + z[58] - z[3] - z[8];
    z[58]=z[5]*z[58];
    z[96]=n<T>(17,2) + z[77];
    z[96]=z[2]*z[96];
    z[96]= - z[31] + z[96];
    z[96]=z[2]*z[96];
    z[97]=4*z[3];
    z[108]=z[88]*z[97];
    z[108]=z[108] + n<T>(1,2)*z[98];
    z[116]=npow(z[6],3);
    z[108]=z[8]*z[108]*z[116];
    z[108]= - n<T>(9,2)*z[49] + z[108];
    z[108]=z[6]*z[108];
    z[120]=z[3] - 6*z[2];
    z[120]=z[120]*z[126];
    z[58]=z[58] + z[120] + z[108] - n<T>(9,2)*z[48] + z[96];
    z[58]=z[5]*z[58];
    z[68]= - z[103] - z[68];
    z[96]=3*z[2];
    z[108]=z[96] - z[3];
    z[36]=3*z[36];
    z[120]= - z[36] - z[108];
    z[120]=z[120]*z[39];
    z[124]= - n<T>(37,2)*z[3] - 36*z[2];
    z[124]=z[2]*z[124];
    z[68]=z[120] + 2*z[68] + z[124];
    z[68]=z[4]*z[68];
    z[120]=static_cast<T>(13)+ 6*z[25];
    z[120]=z[2]*z[120];
    z[120]= - z[76] + z[120];
    z[120]=z[120]*z[33];
    z[124]=z[3]*z[88];
    z[124]=16*z[124] - 11*z[98];
    z[124]=z[124]*z[48]*npow(z[6],4);
    z[58]=z[58] + z[68] + z[124] - z[49] + z[120];
    z[58]=z[5]*z[58];
    z[68]=z[78]*z[88];
    z[120]=z[88]*z[48];
    z[124]=z[2]*z[98];
    z[124]=z[120] + z[124];
    z[124]=z[2]*z[124];
    z[124]=z[68] + z[124];
    z[124]=z[9]*z[124];
    z[129]= - z[13]*z[68];
    z[130]=z[115]*z[6];
    z[131]= - z[88]*z[130];
    z[124]=z[124] + z[129] + z[131];
    z[124]=z[4]*z[124];
    z[129]=z[73]*z[88];
    z[131]= - z[33]*z[57]*z[129];
    z[101]=z[88]*z[101]*z[104];
    z[132]=2*z[11];
    z[133]=z[132]*z[101];
    z[124]=z[133] + z[131] + z[124];
    z[124]=z[9]*z[124];
    z[131]=4*z[24];
    z[133]= - z[131] - 9*z[99];
    z[133]=z[133]*z[129];
    z[134]=2*z[10];
    z[135]=z[134]*z[2];
    z[136]= - z[88]*z[135];
    z[129]= - z[129] + z[136];
    z[129]=z[2]*z[129];
    z[81]= - z[88]*z[81];
    z[81]=z[81] + z[129];
    z[81]=z[2]*z[81];
    z[81]=z[133] + z[81];
    z[81]=z[81]*z[119];
    z[88]=z[50]*z[101];
    z[81]=z[124] + z[81] + 2*z[88];
    z[81]=z[9]*z[81];
    z[88]=5*z[116];
    z[101]=z[120]*z[88];
    z[120]=z[33]*z[4];
    z[101]=z[101] + 39*z[120];
    z[98]= - z[116]*z[98];
    z[116]=z[96]*z[4];
    z[98]=z[98] + z[116];
    z[98]=z[5]*z[98];
    z[98]=n<T>(1,2)*z[101] + z[98];
    z[98]=z[5]*z[98];
    z[88]= - z[68]*z[88];
    z[101]=21*z[115] + z[114];
    z[101]=z[4]*z[101];
    z[88]=z[98] + z[88] + z[101];
    z[88]=z[5]*z[88];
    z[81]=z[88] + z[81];
    z[81]=z[9]*z[81];
    z[88]=z[6]*z[7];
    z[88]=npow(z[88],5);
    z[98]= - z[88]*z[103];
    z[101]=z[2]*z[3];
    z[103]=z[4]*z[108];
    z[98]=z[103] + z[101] + z[98];
    z[98]=z[5]*z[98];
    z[101]= - z[88]*z[49];
    z[103]=z[3]*z[33];
    z[101]=z[103] + z[101];
    z[103]= - z[21] + n<T>(7,2)*z[2];
    z[103]=z[103]*z[116];
    z[98]=z[98] + 2*z[101] + z[103];
    z[98]=z[5]*z[98];
    z[101]=z[115]*z[76];
    z[103]=z[101]*z[4];
    z[108]=z[21]*z[120];
    z[113]=z[3]*z[113];
    z[108]=z[108] + z[113];
    z[108]=z[5]*z[108];
    z[108]=z[103] + z[108];
    z[108]=z[1]*z[108];
    z[88]= - z[78]*z[88];
    z[88]=z[115] + z[88];
    z[88]=z[3]*z[88];
    z[113]= - 14*z[3] + z[30];
    z[113]=z[113]*z[33];
    z[114]=z[21]*z[4]*z[2];
    z[113]=z[113] - z[114];
    z[113]=z[4]*z[113];
    z[88]=z[108] + z[98] + 6*z[88] + z[113];
    z[88]=z[5]*z[88];
    z[98]= - z[73]*npow(z[7],5)*z[115]*z[119]*npow(z[9],3);
    z[88]=z[98] - z[103] + z[88];
    z[88]=z[1]*z[88];
    z[98]=z[3]*z[13];
    z[103]=z[98] - z[38];
    z[68]=z[119]*z[68]*z[103];
    z[49]= - z[104]*z[49];
    z[49]=z[49] + z[68];
    z[49]=z[6]*z[49];
    z[30]=z[76] - z[30];
    z[30]=z[30]*z[33];
    z[68]= - z[6]*z[101];
    z[30]=z[114] + z[30] + z[68];
    z[30]=z[4]*z[30];
    z[68]=6*z[104];
    z[103]=z[3]*z[12];
    z[108]= - z[68] - 5*z[103];
    z[108]=z[108]*z[48];
    z[30]=z[88] + z[81] + z[58] + z[30] + 6*z[49] + z[108] - z[101];
    z[30]=z[1]*z[30];
    z[49]=z[94]*z[3];
    z[58]= - z[49]*z[92];
    z[58]=z[58] - 3*z[104] + 13*z[103];
    z[58]=z[8]*z[58];
    z[81]= - z[104]*z[76];
    z[58]=z[81] + z[58];
    z[58]=z[58]*z[67];
    z[41]=z[41]*z[98];
    z[47]=18*z[80] - 11*z[47];
    z[47]=z[6]*z[47];
    z[41]=z[47] - 15*z[41] - z[87];
    z[47]=z[48]*z[6];
    z[41]=z[41]*z[47];
    z[41]=z[58] + z[41];
    z[41]=z[6]*z[41];
    z[58]=z[27] - z[15];
    z[58]=3*z[58] - 70*z[12];
    z[58]=z[12]*z[58];
    z[80]=7*z[12] + z[76];
    z[80]=z[8]*z[80];
    z[58]=z[80] + z[58] - 18*z[103];
    z[58]=z[8]*z[58];
    z[80]=z[105]*z[107];
    z[81]=8*z[80];
    z[87]= - z[8]*z[12];
    z[87]= - z[81] + z[87];
    z[32]= - z[2]*z[32];
    z[32]=3*z[87] + z[32];
    z[32]=z[2]*z[32];
    z[22]=z[30] + z[43] + z[37] + z[22] + z[41] + z[58] + z[32];
    z[22]=z[1]*z[22];
    z[30]=19*z[3];
    z[32]=4*z[8];
    z[37]= - z[32] - z[29] + z[30];
    z[41]=3*z[8];
    z[37]=z[37]*z[41];
    z[43]=z[38]*z[48];
    z[58]=npow(z[7],2);
    z[87]=19*z[58];
    z[37]= - n<T>(39,2)*z[43] + z[87] + z[37];
    z[37]=z[6]*z[37];
    z[88]=24*z[17];
    z[92]=z[58]*z[11];
    z[37]=z[4] + z[37] + z[127] - 19*z[92] + z[88] - 29*z[3];
    z[37]=z[6]*z[37];
    z[101]=z[50] + z[61];
    z[108]=n<T>(9,2)*z[2];
    z[101]=z[101]*z[108];
    z[113]=z[45] - z[12];
    z[113]=z[113]*z[11];
    z[114]=static_cast<T>(3)- z[113];
    z[114]=z[11]*z[114];
    z[101]=z[101] + z[114] + n<T>(51,2)*z[52];
    z[101]=z[2]*z[101];
    z[114]= - z[21]*z[82];
    z[116]=z[3] - z[8];
    z[114]=3*z[116] + z[114];
    z[114]=z[114]*z[54];
    z[114]=z[114] + static_cast<T>(10)- z[25];
    z[114]=z[6]*z[114];
    z[116]=z[2]*z[61];
    z[114]=z[114] + z[116] - z[59];
    z[114]=z[5]*z[114];
    z[116]=z[50]*z[58];
    z[37]=z[114] + z[101] + n<T>(15,2)*z[116] + static_cast<T>(19)- z[74] + z[37];
    z[37]=z[5]*z[37];
    z[101]=4*z[11];
    z[114]=z[2]*z[50];
    z[114]=z[101] + z[114];
    z[114]=z[2]*z[114];
    z[114]=z[114] + n<T>(1,2)*z[116] + n<T>(5,2)*z[25] - static_cast<T>(35)+ z[107];
    z[114]=z[2]*z[114];
    z[116]=z[38]*z[78];
    z[116]=18*z[116];
    z[120]= - z[45] - n<T>(35,2)*z[3];
    z[120]=z[120]*z[58];
    z[124]=33*z[8];
    z[127]=z[72] - z[124];
    z[127]=z[8]*z[127];
    z[127]= - 10*z[58] + z[127];
    z[127]=z[8]*z[127];
    z[120]= - z[116] + 3*z[120] + z[127];
    z[120]=z[6]*z[120];
    z[127]=z[35] + n<T>(9,2)*z[25];
    z[127]=z[127]*z[58];
    z[129]=48*z[17];
    z[133]=28*z[8] + z[129] + n<T>(91,2)*z[3];
    z[133]=z[8]*z[133];
    z[120]=z[120] + 9*z[127] + z[133];
    z[120]=z[6]*z[120];
    z[127]=z[101]*z[23];
    z[133]=17*z[16];
    z[136]= - 18*z[17] - z[133];
    z[136]=65*z[12] + 2*z[136] - z[15];
    z[136]=z[136]*z[50];
    z[136]=z[136] - n<T>(33,2)*z[52];
    z[136]=z[136]*z[58];
    z[137]= - z[29] + z[31];
    z[137]=z[6]*z[137];
    z[137]= - static_cast<T>(4)+ z[137];
    z[126]=z[137]*z[126];
    z[137]=10*z[12];
    z[138]=2*z[14];
    z[139]= - z[16]*z[132];
    z[139]= - n<T>(55,2) + z[139];
    z[139]=z[3]*z[139];
    z[37]=z[37] + z[126] + z[120] + z[114] - z[41] + z[136] + z[139] - 
    z[127] + z[137] - 61*z[15] - z[138] - 120*z[17] - 13*z[16];
    z[37]=z[5]*z[37];
    z[114]=2*z[58];
    z[120]=9*z[16] + z[12];
    z[120]=z[120]*z[114];
    z[126]=11*z[58];
    z[136]=z[126] - 21*z[33];
    z[136]=z[2]*z[136];
    z[120]=z[120] + z[136];
    z[120]=z[6]*z[120];
    z[136]=4*z[14];
    z[139]=static_cast<T>(4)+ z[85];
    z[139]=z[15]*z[139];
    z[139]=z[139] - z[136] - z[93];
    z[139]=z[96] + 2*z[139] - z[8];
    z[139]=z[139]*z[39];
    z[140]= - z[23] + z[69];
    z[141]=69*z[15] + 40*z[17] + z[10];
    z[141]=z[141]*z[26];
    z[142]=7*z[84] + z[85];
    z[142]=z[142]*z[114];
    z[143]=z[8]*z[13];
    z[144]=static_cast<T>(9)+ z[143];
    z[144]=z[144]*z[67];
    z[145]=z[13]*z[126];
    z[144]=z[144] + 15*z[10] + z[145];
    z[144]=z[8]*z[144];
    z[145]=9*z[2];
    z[146]=z[145] + 35*z[8] - z[42];
    z[146]=z[2]*z[146];
    z[133]=z[133] - 4*z[10];
    z[133]=z[10]*z[133];
    z[120]=z[139] + z[120] + z[146] + z[144] + z[142] + z[141] - z[110]
    + 4*z[140] + z[133];
    z[120]=z[4]*z[120];
    z[133]=3*z[15];
    z[139]=3*z[16];
    z[140]=z[139] + z[14];
    z[140]= - z[26] + 4*z[140] + z[133];
    z[140]=z[140]*z[114];
    z[141]=z[125]*z[41];
    z[126]=z[141] + 29*z[73] - z[126];
    z[126]=z[8]*z[126];
    z[141]= - z[145] + 2*z[18] - 17*z[8];
    z[141]=z[2]*z[141];
    z[141]=z[141] + z[114] - z[48];
    z[141]=z[2]*z[141];
    z[105]=z[105]*z[12];
    z[142]=z[95] + 25*z[90];
    z[142]=z[10]*z[142];
    z[111]=z[58] - z[111];
    z[111]=z[111]*z[62];
    z[111]=z[111] + z[141] + z[126] + z[140] + z[142] + 72*z[105];
    z[111]=z[4]*z[111];
    z[126]=z[23] - z[24];
    z[126]=4*z[126];
    z[140]=z[14] - z[16];
    z[141]=z[140]*z[40];
    z[141]= - z[126] + z[141];
    z[141]=z[10]*z[141];
    z[36]= - z[36] - z[2];
    z[36]=z[36]*z[39];
    z[36]=z[36] - z[56] - 17*z[33];
    z[36]=z[4]*z[36];
    z[56]=z[110] + n<T>(9,2)*z[33];
    z[142]=8*z[15];
    z[144]=z[4]*z[142];
    z[144]=z[144] + 5*z[58] + z[56];
    z[144]=z[5]*z[144];
    z[146]= - 28*z[16] + 43*z[12];
    z[146]=z[146]*z[58];
    z[147]=5*z[73];
    z[148]= - z[8]*z[10];
    z[148]= - z[147] + z[148];
    z[148]=z[8]*z[148];
    z[149]=z[10] + z[2];
    z[149]=z[2]*z[149];
    z[147]=z[147] + z[149];
    z[147]=z[2]*z[147];
    z[36]=z[144] + z[36] + z[147] + z[148] + z[141] + z[146];
    z[36]=z[5]*z[36];
    z[71]=z[53] + z[71];
    z[71]=z[8]*z[71];
    z[71]= - z[117] + z[71];
    z[71]=z[8]*z[71];
    z[117]=z[10]*z[78]*z[39];
    z[141]=z[56]*z[112];
    z[115]=z[4]*z[115];
    z[115]=z[115] + z[141];
    z[115]=z[5]*z[115];
    z[115]=z[117] + z[115];
    z[115]=z[9]*z[115];
    z[117]=z[70] - 3*z[24];
    z[141]=z[40]*z[14];
    z[117]=2*z[117] - z[141];
    z[144]=z[20]*z[15];
    z[117]=2*z[117] - z[144];
    z[117]=z[117]*z[58];
    z[146]=z[58]*z[2];
    z[147]=16*z[10];
    z[148]= - z[2] - z[147] + z[8];
    z[148]=z[148]*z[146];
    z[36]=z[115] + z[36] + z[111] + z[148] + z[117] + z[71];
    z[36]=z[9]*z[36];
    z[71]=n<T>(5,2)*z[92];
    z[111]=z[128] - z[12];
    z[111]=2*z[111] + z[71];
    z[115]=15*z[48];
    z[117]= - z[58] + z[115];
    z[117]=z[6]*z[117];
    z[128]=n<T>(13,2)*z[8];
    z[148]=z[96]*z[11];
    z[149]=static_cast<T>(1)+ z[148];
    z[149]=z[2]*z[149];
    z[150]=z[41]*z[6];
    z[151]=static_cast<T>(2)- z[150];
    z[151]=z[5]*z[151];
    z[111]=z[151] + 7*z[4] + n<T>(1,2)*z[117] + n<T>(3,2)*z[149] + 3*z[111] - 
    z[128];
    z[111]=z[5]*z[111];
    z[117]=z[140]*z[20];
    z[140]=10*z[109];
    z[117]= - z[140] - z[70] + z[117];
    z[149]= - 34*z[16] + 79*z[12];
    z[149]=z[149]*z[92];
    z[117]=z[149] + 2*z[117] + z[104];
    z[149]=6*z[10];
    z[151]=z[149] - z[92];
    z[152]=z[2]*z[11];
    z[153]= - static_cast<T>(6)+ z[152];
    z[153]=z[2]*z[153];
    z[151]=2*z[151] + z[153];
    z[151]=z[2]*z[151];
    z[87]=z[87] - z[115];
    z[82]=z[87]*z[82];
    z[87]= - z[138] - 13*z[15];
    z[87]=3*z[87] - z[64];
    z[87]=z[39] - n<T>(153,2)*z[2] + 2*z[87] - z[8];
    z[87]=z[4]*z[87];
    z[115]=z[118] - z[15];
    z[153]=n<T>(7,2)*z[8] - 12*z[10] + z[115];
    z[153]=z[8]*z[153];
    z[82]=z[111] + z[87] + z[82] + z[151] + 2*z[117] + z[153];
    z[82]=z[5]*z[82];
    z[87]=z[109]*z[106];
    z[111]=3*z[18];
    z[71]= - z[145] - 20*z[8] - z[71] + z[87] - z[133] + z[111] - z[10];
    z[71]=z[2]*z[71];
    z[87]= - z[73] + z[110];
    z[117]=10*z[8];
    z[151]= - z[133] - z[117];
    z[151]=z[8]*z[151];
    z[71]=z[71] + 3*z[87] + z[151];
    z[71]=z[2]*z[71];
    z[87]=4*z[93];
    z[151]=z[13]*z[73];
    z[151]= - z[87] + z[151];
    z[153]=z[85]*z[20];
    z[154]=5*z[15];
    z[155]= - z[29] + z[154];
    z[156]=8*z[12];
    z[155]=9*z[155] + z[156];
    z[155]=z[12]*z[155];
    z[155]= - z[91] + z[155];
    z[155]=z[11]*z[155];
    z[151]=z[155] + 2*z[151] + z[153];
    z[151]=z[151]*z[58];
    z[153]=2*z[13];
    z[155]= - z[58]*z[153];
    z[157]=10*z[10];
    z[155]=z[41] + z[155] + z[157] + 9*z[12];
    z[155]=z[8]*z[155];
    z[158]=7*z[15];
    z[159]=z[29] + z[158];
    z[159]=3*z[159] - 64*z[12];
    z[159]=z[12]*z[159];
    z[160]=16*z[73];
    z[144]=z[155] + z[159] + z[160] - z[144];
    z[144]=z[8]*z[144];
    z[155]=z[12]*z[17];
    z[155]=z[155] + z[109];
    z[155]=z[155]*z[12];
    z[159]=z[89] + 7*z[90];
    z[159]=z[10]*z[159];
    z[159]=z[159] + 48*z[155];
    z[161]= - z[10] - z[41];
    z[161]=z[8]*z[161];
    z[102]=z[161] + z[126] - z[102];
    z[102]=z[58]*z[102];
    z[117]=z[117] + z[18] - z[40];
    z[117]=z[117]*z[146];
    z[102]=z[117] + z[102];
    z[102]=z[6]*z[102];
    z[36]=z[36] + z[82] + z[120] + z[102] + z[71] + z[144] + 3*z[159] + 
    z[151];
    z[36]=z[9]*z[36];
    z[71]=12*z[11];
    z[82]=z[71]*z[109];
    z[102]= - z[154] - z[82];
    z[102]=z[11]*z[102];
    z[117]=z[55]*z[109];
    z[120]=4*z[15];
    z[126]=z[120] + z[117];
    z[126]=z[126]*z[132];
    z[126]= - static_cast<T>(7)+ z[126];
    z[126]=z[11]*z[126];
    z[144]=4*z[52];
    z[126]=z[126] + z[144];
    z[126]=z[3]*z[126];
    z[151]=z[50]*z[114];
    z[159]= - z[71] - z[52];
    z[159]=z[2]*z[159];
    z[102]=z[159] + z[151] + z[126] + static_cast<T>(1)+ z[102];
    z[102]=z[2]*z[102];
    z[126]=z[29] + 15*z[15];
    z[126]=z[12]*z[126];
    z[81]= - z[81] + z[140] + z[126];
    z[81]=z[81]*z[55];
    z[82]=z[15] + z[82];
    z[82]=z[11]*z[82];
    z[59]=z[59]*z[97];
    z[59]=z[59] + n<T>(5,2) + z[82];
    z[59]=z[3]*z[59];
    z[82]= - z[133] - z[64];
    z[82]=z[82]*z[50];
    z[82]=z[82] - z[144];
    z[82]=z[82]*z[58];
    z[59]=z[102] + 61*z[8] + z[82] + z[59] + z[81] - z[142] + z[18] - 
    z[149];
    z[59]=z[2]*z[59];
    z[81]=z[13]*z[58];
    z[30]= - z[83] + 13*z[81] + 23*z[12] - z[30];
    z[30]=z[8]*z[30];
    z[81]=n<T>(15,2)*z[58];
    z[81]= - z[98]*z[81];
    z[30]=z[30] + z[81] - z[68] + 25*z[103];
    z[30]=z[8]*z[30];
    z[68]=z[26]*z[125];
    z[81]=z[107] + z[25];
    z[81]=z[81]*z[114];
    z[82]=8*z[3];
    z[83]=z[82] - 5*z[92];
    z[83]=z[2]*z[83];
    z[68]=z[83] + z[81] + z[68];
    z[68]=z[2]*z[68];
    z[81]= - z[10] - n<T>(7,2)*z[3];
    z[81]=z[81]*z[58];
    z[76]= - z[8]*z[76];
    z[76]=n<T>(53,2)*z[58] + z[76];
    z[76]=z[8]*z[76];
    z[76]=z[81] + z[76];
    z[76]=z[8]*z[76];
    z[81]=z[141] + z[131];
    z[83]=z[81]*z[58];
    z[92]=7*z[2] + 15*z[8] - z[97] - z[18] + z[20];
    z[92]=z[92]*z[146];
    z[76]=z[92] + z[83] + z[76];
    z[76]=z[6]*z[76];
    z[83]=5*z[16];
    z[92]=z[25]*z[83];
    z[92]=z[87] + z[92];
    z[92]=z[92]*z[58];
    z[102]= - z[3]*z[104];
    z[92]=z[102] + z[92];
    z[30]=z[76] + z[68] + 2*z[92] + z[30];
    z[30]=z[6]*z[30];
    z[68]=z[153]*z[15];
    z[76]= - 9*z[143] + 8*z[98] - n<T>(65,2) + z[68];
    z[76]=z[8]*z[76];
    z[92]=z[12] + z[16];
    z[102]=z[92]*z[58];
    z[33]=z[33]*z[97];
    z[33]=z[102] + z[33];
    z[33]=z[33]*z[57];
    z[97]=z[3]*z[26];
    z[89]= - z[89] + z[97];
    z[97]=n<T>(39,2)*z[3] + 11*z[2];
    z[97]=z[2]*z[97];
    z[33]=z[33] + 2*z[89] + z[97];
    z[33]=z[6]*z[33];
    z[89]=3*z[85];
    z[97]=z[96]*z[6];
    z[102]=z[97] + z[89] + static_cast<T>(2)- 3*z[84];
    z[102]=z[102]*z[39];
    z[103]=4*z[16];
    z[125]=z[93] + z[14] + 21*z[17] - z[103];
    z[125]=19*z[12] - z[15] + 2*z[125] - z[10];
    z[114]=z[123]*z[114];
    z[126]= - n<T>(15,2) - 8*z[84];
    z[126]=z[3]*z[126];
    z[33]=z[102] + z[33] + n<T>(83,2)*z[2] + z[76] + z[114] + 2*z[125] + 
    z[126];
    z[33]=z[4]*z[33];
    z[76]= - 12*z[12] + z[27] + 17*z[15];
    z[76]=z[76]*z[26];
    z[102]=z[14] - z[18] + 15*z[16];
    z[102]=z[10]*z[102];
    z[76]=12*z[80] + z[76] - 3*z[109] + z[91] + z[102];
    z[91]=6*z[23] + z[109];
    z[91]=z[91]*z[132];
    z[63]= - z[63] - z[132];
    z[63]=z[16]*z[63];
    z[63]=5*z[84] + z[63];
    z[63]=z[63]*z[21];
    z[84]=7*z[16] + z[136];
    z[102]=4*z[85];
    z[114]= - static_cast<T>(7)- z[102];
    z[114]=z[15]*z[114];
    z[63]=z[63] + z[91] + z[114] + 3*z[84] + z[134];
    z[63]=z[3]*z[63];
    z[45]= - z[120] - z[45] + 7*z[10];
    z[84]=static_cast<T>(8)- 5*z[98];
    z[84]=z[84]*z[21];
    z[91]=z[58]*z[49];
    z[114]=3*z[98];
    z[120]=z[8]*z[153];
    z[120]=z[120] + static_cast<T>(13)- z[114];
    z[120]=z[8]*z[120];
    z[45]=z[120] + 8*z[91] + z[84] + 3*z[45] + 107*z[12];
    z[45]=z[8]*z[45];
    z[84]=z[94]*z[131];
    z[91]= - z[27] + z[158];
    z[91]=3*z[91] + z[156];
    z[91]=z[12]*z[91];
    z[91]= - z[95] + z[91];
    z[91]=z[91]*z[50];
    z[120]= - z[16] - z[15];
    z[120]=3*z[120] + z[12];
    z[120]=z[11]*z[120];
    z[89]= - z[89] + z[120];
    z[77]=z[89]*z[77];
    z[77]=z[77] + z[84] + z[91];
    z[58]=z[77]*z[58];
    z[22]=z[22] + z[36] + z[37] + z[33] + z[30] + z[59] + z[45] + z[58]
    + 2*z[76] + z[63];
    z[22]=z[1]*z[22];
    z[30]=z[104]*z[132];
    z[30]=z[96] + z[30] + z[133] - 11*z[12];
    z[30]=z[5]*z[30];
    z[33]=z[139] + z[138];
    z[33]=z[33]*z[40];
    z[36]=z[40] + z[12];
    z[36]=z[36]*z[41];
    z[37]=z[157] - z[108];
    z[37]=z[2]*z[37];
    z[41]=9*z[15];
    z[45]= - n<T>(71,2)*z[2] - n<T>(1,2)*z[8] - z[64] - 10*z[14] - z[41];
    z[45]=z[4]*z[45];
    z[30]=z[30] + z[45] + z[37] + z[36] + 2*z[104] - z[110] + z[95] + 
    z[33];
    z[30]=z[5]*z[30];
    z[33]= - z[70] + z[24];
    z[33]= - 26*z[109] + 4*z[33] - 11*z[90];
    z[36]=z[153]*z[24];
    z[36]=z[36] + 5*z[14];
    z[37]=static_cast<T>(5)+ z[68];
    z[37]=z[15]*z[37];
    z[37]=z[2] - z[67] + z[37] - z[36];
    z[37]=z[37]*z[39];
    z[45]=38*z[15] + z[27] + z[10];
    z[45]=z[45]*z[26];
    z[58]=n<T>(1,2)*z[2];
    z[59]=z[19] - z[58];
    z[59]=z[59]*z[96];
    z[63]=static_cast<T>(5)- z[143];
    z[63]=z[8]*z[63];
    z[63]= - 33*z[10] + z[63];
    z[63]=z[8]*z[63];
    z[33]=z[37] - 9*z[130] + z[59] + z[63] + 2*z[33] + z[45];
    z[33]=z[4]*z[33];
    z[37]=24*z[105] + z[78];
    z[45]=z[19] + z[2];
    z[59]=z[18] - z[45];
    z[59]=z[2]*z[59];
    z[59]=z[48] + z[59];
    z[59]=z[2]*z[59];
    z[37]=2*z[37] + z[59];
    z[37]=z[4]*z[37];
    z[56]= - z[4]*z[56];
    z[59]=z[15] + z[2];
    z[59]=z[59]*z[112];
    z[56]=z[56] + 3*z[59];
    z[56]=z[5]*z[56];
    z[37]=z[37] + z[56];
    z[37]=z[9]*z[37];
    z[45]= - z[42] - z[45];
    z[45]=z[2]*z[45];
    z[45]=z[45] - 8*z[73] + z[48];
    z[45]=z[2]*z[45];
    z[56]= - z[10]*z[81];
    z[59]=z[53] + 2*z[48];
    z[59]=z[8]*z[59];
    z[63]= - z[53] - z[135];
    z[63]=z[63]*z[34];
    z[30]=z[37] + z[30] + z[33] + z[63] + z[45] + z[59] + z[56] + 96*
    z[155];
    z[30]=z[9]*z[30];
    z[33]=z[109]*z[101];
    z[37]=z[132]*z[2];
    z[45]= - z[11]*z[41];
    z[45]= - z[37] - static_cast<T>(14)+ z[45];
    z[45]=z[2]*z[45];
    z[40]= - z[18] - z[40];
    z[33]=z[45] + z[51] + z[33] + 2*z[40] - z[41];
    z[33]=z[2]*z[33];
    z[36]= - z[13]*z[36];
    z[40]=5*z[13] + 2*z[122];
    z[40]=z[15]*z[40];
    z[36]=z[97] + z[143] + z[40] + static_cast<T>(4)+ z[36];
    z[36]=z[36]*z[39];
    z[40]=z[46] - z[139];
    z[45]= - static_cast<T>(17)+ 6*z[85];
    z[45]=z[15]*z[45];
    z[56]=z[94]*z[8];
    z[59]= - n<T>(27,2)*z[13] - z[56];
    z[59]=z[8]*z[59];
    z[59]= - static_cast<T>(75)+ z[59];
    z[59]=z[8]*z[59];
    z[34]=z[36] + 7*z[34] + 29*z[2] + z[59] + 22*z[12] + z[45] + 8*z[10]
    + 6*z[40] + z[14];
    z[34]=z[4]*z[34];
    z[36]=z[15] - z[64];
    z[36]=z[36]*z[106];
    z[40]=z[67] - n<T>(3,2)*z[47];
    z[45]=5*z[6];
    z[40]=z[40]*z[45];
    z[45]= - static_cast<T>(4)+ z[150];
    z[45]=z[6]*z[45];
    z[45]=z[11] + z[45];
    z[45]=z[5]*z[45];
    z[36]=z[45] + z[40] + z[148] + n<T>(5,2) + z[36];
    z[36]=z[5]*z[36];
    z[40]= - 20*z[104] + 10*z[23] + z[109];
    z[40]=z[40]*z[132];
    z[45]= - static_cast<T>(3)- n<T>(1,2)*z[152];
    z[45]=z[45]*z[145];
    z[47]=z[78]*z[6];
    z[47]= - 3*z[48] + 5*z[47];
    z[47]=z[47]*z[54];
    z[36]=z[36] + 21*z[4] + z[47] + z[45] - z[128] + z[40] + 50*z[12] - 
   14*z[15] + 55*z[16] - z[14];
    z[36]=z[5]*z[36];
    z[40]= - 8*z[2] + z[51] - z[111] - z[157];
    z[40]=z[2]*z[40];
    z[40]=z[40] - z[160] + 17*z[48];
    z[40]=z[2]*z[40];
    z[45]= - z[131] - 7*z[99];
    z[45]=z[45]*z[20];
    z[47]=z[10] - z[67];
    z[47]=z[8]*z[47];
    z[47]=z[53] + z[47];
    z[47]=z[8]*z[47];
    z[40]=z[40] + z[45] + z[47];
    z[40]=z[6]*z[40];
    z[45]= - z[20] - z[83] - z[136];
    z[45]=z[10]*z[45];
    z[47]= - z[23] - z[69];
    z[51]= - z[20] - 26*z[15];
    z[51]=z[15]*z[51];
    z[45]=z[51] + 2*z[47] + z[45];
    z[27]=28*z[15] - z[27] + z[10];
    z[27]=3*z[27] - z[156];
    z[27]=z[12]*z[27];
    z[47]=5*z[17] - z[15];
    z[47]=z[12]*z[47];
    z[47]=4*z[109] + z[47];
    z[47]=z[47]*z[107];
    z[51]=static_cast<T>(9)- z[143];
    z[51]=z[8]*z[51];
    z[51]=z[51] + 47*z[12] - z[88] + z[10];
    z[51]=z[8]*z[51];
    z[27]=z[30] + z[36] + z[34] + z[40] + z[33] + z[51] + 24*z[47] + 2*
    z[45] + z[27];
    z[27]=z[9]*z[27];
    z[30]=z[21]*z[121];
    z[33]=z[14]*z[153];
    z[34]= - z[11]*z[83];
    z[30]=z[30] + z[33] + z[34];
    z[30]=z[30]*z[21];
    z[33]= - z[87] - z[103] - 7*z[14];
    z[34]=z[104]*z[101];
    z[30]=z[30] + z[34] - z[100] + 3*z[33] - z[134];
    z[30]=z[3]*z[30];
    z[33]=z[49] + z[13];
    z[34]= - z[33]*z[21];
    z[34]= - static_cast<T>(3)+ z[34];
    z[34]=z[3]*z[34];
    z[34]=z[34] + z[20] - z[64];
    z[36]=4*z[49];
    z[40]=n<T>(15,2)*z[13] - z[36];
    z[40]=z[3]*z[40];
    z[33]=z[8]*z[33];
    z[33]=z[33] - n<T>(61,2) + z[40];
    z[33]=z[8]*z[33];
    z[33]=2*z[34] + z[33];
    z[33]=z[8]*z[33];
    z[34]=static_cast<T>(1)+ z[25];
    z[34]=z[34]*z[72];
    z[37]= - z[37] - static_cast<T>(4)+ z[28];
    z[37]=z[2]*z[37];
    z[34]=z[37] - 62*z[8] + z[34] - z[118] + z[18] - 24*z[10];
    z[34]=z[2]*z[34];
    z[37]=static_cast<T>(11)- z[114];
    z[37]=z[8]*z[37];
    z[37]= - z[72] + z[37];
    z[37]=z[37]*z[48];
    z[40]=z[6]*z[79];
    z[37]=z[37] + z[40];
    z[37]=z[37]*z[57];
    z[24]=z[23] - 6*z[24];
    z[40]=15*z[14];
    z[45]= - z[40] + z[18] - z[16];
    z[45]=z[10]*z[45];
    z[24]=2*z[24] + z[45];
    z[20]= - z[20] + 4*z[12];
    z[20]=z[12]*z[20];
    z[20]=z[37] + z[34] + z[33] + z[30] + 2*z[24] + z[20];
    z[20]=z[6]*z[20];
    z[24]=19*z[14];
    z[30]= - z[24] - 8*z[93];
    z[30]=z[13]*z[30];
    z[33]=3*z[121] - z[122];
    z[33]=z[33]*z[21];
    z[34]=4*z[56] + n<T>(51,2)*z[13] - z[36];
    z[34]=z[8]*z[34];
    z[36]=z[23] + z[86];
    z[36]=z[6]*z[36];
    z[37]=z[83] + z[12];
    z[36]=4*z[36] + 2*z[37] + n<T>(65,2)*z[2];
    z[36]=z[6]*z[36];
    z[37]=z[123]*z[62];
    z[45]=19*z[13] + 8*z[122];
    z[45]=z[15]*z[45];
    z[30]=z[37] + z[36] + z[34] + z[33] + z[45] + static_cast<T>(29)+ z[30];
    z[30]=z[4]*z[30];
    z[33]=z[8] + z[46] - n<T>(23,2)*z[3];
    z[33]=z[8]*z[33];
    z[33]=z[33] + n<T>(7,2)*z[43];
    z[33]=z[33]*z[54];
    z[33]=z[33] + z[82] + n<T>(15,2)*z[8];
    z[33]=z[6]*z[33];
    z[34]= - n<T>(21,2) - z[35];
    z[33]=z[33] + 3*z[34] + 40*z[25];
    z[33]=z[6]*z[33];
    z[32]=z[38]*z[32];
    z[19]=z[32] - z[44] + z[19];
    z[19]=z[6]*z[19];
    z[19]=z[19] - static_cast<T>(14)+ z[28];
    z[19]=z[6]*z[19];
    z[19]=z[19] + z[75] - z[52];
    z[19]=z[6]*z[19];
    z[19]= - z[61] + z[19];
    z[19]=z[5]*z[19];
    z[28]=z[50] - 9*z[61];
    z[28]=z[28]*z[58];
    z[32]=n<T>(49,2) + z[113];
    z[32]=z[11]*z[32];
    z[19]=z[19] + z[33] + z[28] + z[32] - n<T>(57,2)*z[52];
    z[19]=z[5]*z[19];
    z[28]= - 3*z[17] + z[16];
    z[28]=4*z[28] + z[115];
    z[28]=5*z[28] + z[127];
    z[28]=z[28]*z[132];
    z[32]= - z[29] - z[26];
    z[32]=z[32]*z[55];
    z[32]=n<T>(25,2) + z[32];
    z[32]=z[11]*z[32];
    z[33]= - 5*z[50] - z[61];
    z[33]=z[2]*z[33];
    z[32]=z[33] + z[32] - 10*z[52];
    z[32]=z[2]*z[32];
    z[33]= - n<T>(59,2)*z[3] + z[124];
    z[33]=z[33]*z[48];
    z[33]=z[33] + z[116];
    z[33]=z[6]*z[33];
    z[34]= - z[65] - 25*z[3];
    z[34]=2*z[34] - n<T>(99,2)*z[8];
    z[34]=z[8]*z[34];
    z[33]=z[34] + z[33];
    z[33]=z[6]*z[33];
    z[33]=z[33] - n<T>(11,2)*z[8] + n<T>(171,2)*z[3] + 96*z[17] + z[14];
    z[33]=z[6]*z[33];
    z[34]=z[16]*z[101];
    z[34]= - n<T>(57,2) + z[34];
    z[34]=z[34]*z[25];
    z[19]=z[19] + z[33] + z[32] + z[34] + n<T>(117,2) + z[28];
    z[19]=z[5]*z[19];
    z[28]= - z[23] - z[109];
    z[28]=z[28]*z[71];
    z[32]=27*z[16];
    z[33]=static_cast<T>(5)- z[68];
    z[33]=z[15]*z[33];
    z[28]=z[28] + z[137] - z[32] + z[33];
    z[28]=z[11]*z[28];
    z[24]= - z[24] - 12*z[93];
    z[24]=z[13]*z[24];
    z[33]=6*z[16];
    z[34]=z[50]*z[33];
    z[34]=z[121] + z[34];
    z[34]=z[34]*z[21];
    z[35]= - z[10]*z[153];
    z[36]= - 3*z[13] + 4*z[122];
    z[36]=z[15]*z[36];
    z[24]=z[34] + z[28] + z[36] + z[35] + static_cast<T>(22)+ z[24];
    z[24]=z[3]*z[24];
    z[28]= - z[70] - 13*z[109];
    z[34]= - 32*z[17] - z[154];
    z[34]=z[34]*z[26];
    z[28]=24*z[80] + 4*z[28] + z[34];
    z[28]=z[11]*z[28];
    z[34]= - z[46] - z[154];
    z[35]= - z[29] - z[41];
    z[26]=z[35]*z[26];
    z[26]=z[110] + z[26];
    z[26]=z[11]*z[26];
    z[26]=z[26] + 3*z[34] - z[64];
    z[26]=z[11]*z[26];
    z[34]=z[154] - z[117];
    z[34]=z[34]*z[132];
    z[34]=static_cast<T>(25)+ z[34];
    z[34]=z[11]*z[34];
    z[34]=z[34] - 12*z[52];
    z[34]=z[3]*z[34];
    z[35]=z[11]*z[154];
    z[35]= - static_cast<T>(3)+ z[35];
    z[35]=z[35]*z[52];
    z[36]=z[15]*z[101];
    z[36]=static_cast<T>(1)+ z[36];
    z[36]=z[11]*z[36];
    z[35]=z[36] + z[35];
    z[35]=z[2]*z[35];
    z[26]=z[35] + z[34] + n<T>(125,2) + z[26];
    z[26]=z[2]*z[26];
    z[34]= - z[56] - n<T>(37,2)*z[13] + 7*z[49];
    z[34]=z[8]*z[34];
    z[35]=15*z[13] - z[49];
    z[35]=z[3]*z[35];
    z[35]= - static_cast<T>(36)+ z[35];
    z[34]=2*z[35] + z[34];
    z[34]=z[8]*z[34];
    z[35]=z[18]*z[153];
    z[35]= - static_cast<T>(3)+ z[35];
    z[35]=z[10]*z[35];
    z[36]= - static_cast<T>(13)+ z[68];
    z[36]=z[15]*z[36];
    z[19]=z[22] + z[27] + z[19] + z[30] + z[20] + z[26] + z[34] + z[24]
    + z[28] + 41*z[12] + z[36] + z[35] - 16*z[93] - z[40] - z[32] - 
    z[18] + z[129];
    z[19]=z[1]*z[19];
    z[20]=z[92]*z[7];
    z[22]=3*z[20];
    z[24]=z[2]*z[7];
    z[26]=z[22] + z[24];
    z[26]=z[6]*z[26];
    z[27]=z[8]*z[7];
    z[28]= - z[27] + 2*z[20];
    z[30]=z[9]*z[28];
    z[32]=2*z[7];
    z[34]=z[7]*z[143];
    z[26]=z[30] + z[26] + z[32] + z[34];
    z[26]=z[4]*z[26];
    z[23]=z[104] - z[23];
    z[30]= - z[23]*z[132];
    z[30]=7*z[92] + z[30];
    z[30]=z[5]*z[7]*z[11]*z[30];
    z[26]=z[30] + z[26] + z[28];
    z[26]=z[9]*z[26];
    z[24]=z[24] + z[20] + z[27];
    z[24]=z[24]*z[57];
    z[28]= - 8*z[14] + z[29] - z[139];
    z[29]=z[92]*z[106];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[32];
    z[20]=z[119]*z[20];
    z[20]=static_cast<T>(25)+ z[20];
    z[20]=z[20]*z[39];
    z[23]= - z[11]*z[23];
    z[23]=6*z[92] + z[23];
    z[23]=z[7]*z[23]*z[50];
    z[23]= - n<T>(185,2) + 4*z[23];
    z[23]=z[5]*z[23];
    z[30]=z[153]*z[7];
    z[34]= - static_cast<T>(9)+ z[30];
    z[34]=z[8]*z[34];
    z[20]=2*z[26] + z[23] + z[20] + z[24] - n<T>(61,2)*z[2] + z[34] + z[29]
    - 25*z[12] + z[133] + 3*z[28] + z[147];
    z[20]=z[9]*z[20];
    z[23]=z[64] + z[136] + z[33] - z[18] - z[88];
    z[24]= - z[153] - z[55];
    z[21]=z[7]*z[24]*z[21];
    z[24]=z[38] + 1;
    z[26]=z[98] - z[24];
    z[26]=z[27]*z[26];
    z[28]=z[31]*z[7];
    z[26]=z[28] + z[26];
    z[26]=z[26]*z[57];
    z[29]= - static_cast<T>(7)- z[30];
    z[29]=z[8]*z[29];
    z[21]=z[26] - z[145] + z[29] + z[21] + 2*z[23] - n<T>(25,2)*z[3];
    z[21]=z[6]*z[21];
    z[23]= - z[11] + z[52];
    z[23]=z[23]*z[32];
    z[24]= - z[27]*z[24];
    z[24]=z[28] + z[24];
    z[24]=z[6]*z[24];
    z[26]=static_cast<T>(2)- z[74];
    z[26]=z[7]*z[26];
    z[24]=z[26] + z[24];
    z[24]=z[24]*z[57];
    z[23]=z[24] + n<T>(65,2) + z[23];
    z[23]=z[6]*z[23];
    z[22]=z[60]*z[22];
    z[22]= - 62*z[11] + z[22];
    z[22]=2*z[22] + z[23];
    z[22]=z[5]*z[22];
    z[23]=z[138] + z[42];
    z[23]=z[13]*z[23];
    z[24]=n<T>(9,2)*z[13] + z[132];
    z[24]=z[24]*z[66];
    z[26]=z[13] + z[11];
    z[25]=z[26]*z[25];
    z[26]=z[11]*z[92];
    z[26]=static_cast<T>(1)+ 5*z[26];
    z[26]=z[11]*z[26];
    z[25]=z[25] + z[13] + z[26];
    z[25]=z[25]*z[32];
    z[26]= - 21*z[12] - 20*z[15] + 108*z[17] - 41*z[16];
    z[26]=z[11]*z[26];

    r += static_cast<T>(45)+ z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + 
      z[26] + z[102] + z[143] + 15*z[152];
 
    return r;
}

template double qg_2lNLC_r526(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r526(const std::array<dd_real,31>&);
#endif
