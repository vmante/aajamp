#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r508(const std::array<T,31>& k) {
  T z[146];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[13];
    z[5]=k[15];
    z[6]=k[8];
    z[7]=k[10];
    z[8]=k[12];
    z[9]=k[14];
    z[10]=k[4];
    z[11]=k[6];
    z[12]=k[5];
    z[13]=k[2];
    z[14]=k[28];
    z[15]=k[16];
    z[16]=k[19];
    z[17]=k[22];
    z[18]=k[3];
    z[19]=k[17];
    z[20]=k[25];
    z[21]=2*z[18];
    z[22]=npow(z[8],2);
    z[23]=npow(z[11],3);
    z[24]= - z[22]*z[23]*z[21];
    z[25]=z[8]*z[18];
    z[26]= - z[23]*z[25];
    z[27]=n<T>(1,2)*z[5];
    z[28]=z[27] + z[8];
    z[29]=npow(z[4],2);
    z[30]=z[28]*z[29];
    z[31]=z[2]*z[4];
    z[32]= - z[5]*z[31];
    z[26]=z[32] + z[26] + z[30];
    z[30]=n<T>(1,2)*z[2];
    z[26]=z[26]*z[30];
    z[32]=z[8] + z[17];
    z[33]=5*z[4];
    z[32]=z[32]*z[33];
    z[34]=npow(z[17],2);
    z[32]=z[32] + z[34];
    z[35]=z[32] + z[22];
    z[36]=z[35]*z[29];
    z[24]=z[26] + z[24] + z[36];
    z[24]=z[2]*z[24];
    z[26]=z[4]*z[5];
    z[36]=z[8]*z[5];
    z[37]=z[2]*z[8];
    z[38]=15*z[37] - z[36] + z[26];
    z[39]=n<T>(1,2)*z[6];
    z[38]=z[38]*z[39];
    z[40]=n<T>(1,4)*z[29];
    z[41]= - z[22] - z[40];
    z[41]=z[5]*z[41];
    z[42]=z[22] + n<T>(31,4)*z[37];
    z[42]=z[2]*z[42];
    z[38]=z[38] + z[42] + z[41];
    z[41]=npow(z[6],2);
    z[38]=z[38]*z[41];
    z[42]=3*z[23];
    z[43]=npow(z[2],2);
    z[44]=z[42]*z[43];
    z[45]=z[23]*z[29];
    z[46]=z[23]*z[4];
    z[47]= - z[8]*z[42];
    z[47]=z[47] + z[46];
    z[48]=z[23]*z[2];
    z[47]=n<T>(3,2)*z[47] + 7*z[48];
    z[47]=z[6]*z[47];
    z[45]=z[47] + 2*z[45] + z[44];
    z[45]=z[10]*z[6]*z[45];
    z[47]=2*z[22];
    z[49]=z[23]*z[47];
    z[50]=z[8]*z[23];
    z[50]=z[50] - z[48];
    z[50]=z[6]*z[50];
    z[44]=8*z[50] + z[49] - z[44];
    z[44]=z[6]*z[44];
    z[49]=4*z[22];
    z[50]=z[49] + n<T>(5,2)*z[37];
    z[50]=z[50]*z[48];
    z[44]=z[50] + z[44];
    z[44]=z[12]*z[44];
    z[50]=npow(z[9],2);
    z[51]=z[50]*z[13];
    z[52]=3*z[5];
    z[53]=z[23]*z[52]*z[51];
    z[54]=npow(z[5],2);
    z[55]= - z[54] - z[36];
    z[56]=npow(z[3],2);
    z[55]=z[8]*z[56]*z[55];
    z[24]=z[44] + z[45] + z[38] + z[24] + z[55] + z[53];
    z[24]=z[12]*z[24];
    z[38]=3*z[8];
    z[44]=2*z[3];
    z[45]=z[44] - z[38];
    z[45]=2*z[45] + n<T>(3,2)*z[2];
    z[45]=z[6]*z[45];
    z[53]= - 7*z[5] + z[8];
    z[53]=z[8]*z[53];
    z[55]=9*z[8];
    z[57]=9*z[2] - z[3] - z[55];
    z[57]=z[2]*z[57];
    z[45]=z[45] + z[57] + z[40] - 5*z[56] + z[53];
    z[45]=z[6]*z[45];
    z[53]=2*z[8];
    z[57]=z[53]*z[5];
    z[57]=z[57] - z[54];
    z[57]=z[57]*z[8];
    z[58]=z[27]*z[29];
    z[59]=5*z[8];
    z[60]=z[59] + z[2];
    z[60]=z[60]*z[30];
    z[61]=2*z[56];
    z[60]=z[61] + z[60];
    z[60]=z[2]*z[60];
    z[62]=npow(z[3],3);
    z[45]=z[45] + z[60] - z[58] + 7*z[62] - z[57];
    z[45]=z[6]*z[45];
    z[60]=z[47] + z[34];
    z[62]=6*z[17];
    z[63]= - 30*z[8] - z[62] + z[27];
    z[63]=z[4]*z[63];
    z[63]=z[63] - z[60];
    z[63]=z[4]*z[63];
    z[64]= - z[17]*z[44];
    z[64]= - z[34] + z[64];
    z[64]=z[3]*z[64];
    z[65]=11*z[4];
    z[66]=z[65] + z[52] - 35*z[8];
    z[67]=n<T>(1,2)*z[4];
    z[66]=z[66]*z[67];
    z[66]=z[66] - z[31];
    z[66]=z[66]*z[30];
    z[68]=npow(z[9],3);
    z[69]=2*z[68];
    z[63]=z[66] + z[63] + z[64] - z[69];
    z[63]=z[2]*z[63];
    z[64]=z[54]*z[23];
    z[66]=n<T>(5,4)*z[5];
    z[70]=z[48]*z[66];
    z[64]= - 6*z[64] + z[70];
    z[64]=z[2]*z[64];
    z[70]=8*z[8];
    z[71]= - z[6] + z[70];
    z[71]=z[23]*z[71];
    z[71]= - n<T>(7,4)*z[48] - n<T>(15,2)*z[46] + z[71];
    z[71]=z[6]*z[71];
    z[72]= - 5*z[29] - z[43];
    z[72]=z[23]*z[72];
    z[71]=n<T>(7,4)*z[72] + z[71];
    z[71]=z[6]*z[71];
    z[64]=z[64] + z[71];
    z[71]=npow(z[10],2);
    z[64]=z[64]*z[71];
    z[72]= - z[16] - z[3];
    z[72]=z[72]*z[54];
    z[73]=z[50]*z[15];
    z[74]= - z[3]*z[36];
    z[72]=z[74] - z[73] + z[72];
    z[72]=z[72]*z[53];
    z[74]=z[9] + z[3];
    z[75]= - z[74]*z[27];
    z[76]=24*z[9];
    z[77]=z[76] - z[8];
    z[77]=z[8]*z[77];
    z[32]=z[77] + z[75] - z[32];
    z[32]=z[4]*z[32];
    z[75]=z[50]*z[53];
    z[32]=z[75] + z[32];
    z[32]=z[4]*z[32];
    z[75]=2*z[14];
    z[77]=z[68]*z[75];
    z[78]=7*z[17];
    z[79]=z[78]*z[3];
    z[80]= - z[34] - z[79];
    z[80]=z[80]*z[56];
    z[24]=z[24] + z[64] + z[45] + z[63] + z[32] + z[72] + z[80] - z[77];
    z[24]=z[12]*z[24];
    z[32]=npow(z[13],2);
    z[45]=z[44]*z[32];
    z[63]=3*z[13];
    z[64]= - z[63] - z[45];
    z[64]=z[3]*z[64];
    z[64]= - n<T>(13,4) + z[64];
    z[64]=z[7]*z[64];
    z[72]=z[7]*z[13];
    z[80]=n<T>(1,2)*z[72];
    z[81]=z[9]*z[13];
    z[82]=z[80] - static_cast<T>(3)+ n<T>(1,2)*z[81];
    z[82]=z[5]*z[82];
    z[45]=z[13] + z[45];
    z[45]=z[7]*z[45];
    z[83]=z[3]*z[13];
    z[45]=z[45] + n<T>(13,2) - 6*z[83];
    z[45]=z[6]*z[45];
    z[84]=n<T>(15,2)*z[8];
    z[85]=static_cast<T>(2)+ 7*z[83];
    z[85]=z[3]*z[85];
    z[45]=z[45] - n<T>(39,4)*z[2] + z[4] + z[84] + z[82] + z[85] + z[64];
    z[45]=z[6]*z[45];
    z[64]=22*z[9];
    z[82]=2*z[5];
    z[85]= - z[67] - z[82] + z[64] - z[7];
    z[85]=z[4]*z[85];
    z[86]=14*z[83];
    z[87]= - static_cast<T>(9)- z[86];
    z[87]=z[87]*z[56];
    z[88]=3*z[9];
    z[89]=z[75] - z[3];
    z[89]=z[89]*z[88];
    z[90]=7*z[3];
    z[91]=z[32]*z[3];
    z[92]=z[13] + z[91];
    z[92]=z[92]*z[90];
    z[92]=n<T>(45,4) + z[92];
    z[93]=z[7]*z[3];
    z[92]=z[92]*z[93];
    z[94]=n<T>(1,4)*z[7];
    z[95]= - z[5]*z[72];
    z[95]=z[95] - z[9] + z[94];
    z[95]=z[5]*z[95];
    z[96]=5*z[7];
    z[97]=z[8] + z[96] + 6*z[5];
    z[97]=z[8]*z[97];
    z[98]=n<T>(23,4)*z[2] + n<T>(9,4)*z[4] - 14*z[8] - z[90] - n<T>(13,4)*z[9];
    z[98]=z[2]*z[98];
    z[45]=z[45] + z[98] + z[85] + z[97] + z[95] + z[92] + z[87] + z[89];
    z[45]=z[6]*z[45];
    z[85]= - z[32]*z[56];
    z[85]=static_cast<T>(1)+ z[85];
    z[85]=z[7]*z[85];
    z[61]=z[13]*z[61];
    z[80]= - static_cast<T>(1)- z[80];
    z[80]=z[5]*z[80];
    z[61]=z[8] + z[80] + z[61] + z[85];
    z[61]=z[6]*z[61];
    z[80]=n<T>(1,4)*z[2];
    z[85]= - z[9] - z[55];
    z[85]=z[85]*z[80];
    z[87]=z[5]*z[9];
    z[89]= - n<T>(15,4)*z[5] + z[8];
    z[89]=z[8]*z[89];
    z[92]=n<T>(1,4)*z[4];
    z[95]=z[92] + z[9];
    z[97]= - z[5] - z[95];
    z[97]=z[4]*z[97];
    z[61]=z[61] + z[85] + z[97] - n<T>(3,4)*z[87] + z[89];
    z[61]=z[6]*z[61];
    z[85]=z[5] + z[9];
    z[85]=z[85]*z[92];
    z[85]=z[85] + z[87];
    z[89]=z[8]*z[7];
    z[97]=n<T>(3,4)*z[89] + z[93] + z[85];
    z[97]=z[4]*z[97];
    z[98]=2*z[50];
    z[99]= - z[3] - n<T>(3,4)*z[8];
    z[99]=z[4]*z[99];
    z[100]= - z[9] + z[38];
    z[100]=z[2]*z[100];
    z[99]=z[100] - z[98] + z[99];
    z[99]=z[2]*z[99];
    z[100]= - z[50]*z[75];
    z[101]= - z[5] + n<T>(3,4)*z[7];
    z[101]=z[101]*z[36];
    z[102]=z[54]*z[7];
    z[61]=z[61] + z[99] + z[97] + z[101] + z[100] - z[102];
    z[61]=z[6]*z[61];
    z[97]=z[22] + n<T>(1,2)*z[29];
    z[99]=npow(z[11],4);
    z[97]=z[99]*z[97];
    z[100]=z[43]*z[99];
    z[101]=z[99]*z[2];
    z[103]=6*z[6];
    z[104]=z[101]*z[103];
    z[97]=z[104] + z[100] + z[97];
    z[71]=z[97]*z[41]*z[71];
    z[97]=z[99]*z[8];
    z[97]=z[97] - z[101];
    z[97]=z[97]*z[103];
    z[104]= - z[100] + z[97];
    z[105]=z[41]*z[10];
    z[104]=z[104]*z[105];
    z[106]= - z[99]*z[55];
    z[106]=z[106] + z[101];
    z[106]=z[2]*z[106];
    z[97]=z[106] - z[97];
    z[97]=z[6]*z[97];
    z[106]= - z[47] - 3*z[37];
    z[106]=z[106]*z[101];
    z[97]=z[106] + z[97];
    z[97]=z[12]*z[6]*z[97];
    z[97]=z[104] + z[97];
    z[97]=z[12]*z[97];
    z[71]=z[71] + z[97];
    z[71]=z[12]*z[71];
    z[97]=11*z[8];
    z[104]= - z[5] + z[97];
    z[104]=z[104]*z[43]*z[40];
    z[106]=n<T>(1,2)*z[37] - z[56] - n<T>(3,2)*z[36];
    z[106]=z[6]*z[106];
    z[107]=z[47] + z[40];
    z[108]= - z[5]*z[107];
    z[109]=z[43]*z[8];
    z[106]=z[106] + n<T>(9,4)*z[109] + z[108];
    z[106]=z[106]*z[41];
    z[108]=z[99]*z[29];
    z[100]= - 13*z[108] - 9*z[100];
    z[100]=z[100]*z[39];
    z[108]=npow(z[2],3);
    z[110]=z[108]*z[99];
    z[100]= - z[110] + z[100];
    z[100]=z[6]*z[100];
    z[110]= - z[5]*z[110];
    z[100]=z[110] + z[100];
    z[110]=npow(z[10],3);
    z[100]=z[100]*z[110];
    z[71]=z[71] + n<T>(1,2)*z[100] + z[104] + z[106];
    z[71]=z[12]*z[71];
    z[100]=z[27]*z[7];
    z[104]=z[100] - z[36];
    z[104]=z[8]*z[104];
    z[85]=z[4]*z[85];
    z[106]=z[5]*z[7];
    z[111]=z[7] - z[52];
    z[111]=z[8]*z[111];
    z[111]= - z[106] + z[111];
    z[111]=z[111]*z[39];
    z[85]=z[111] + z[104] + z[85];
    z[85]=z[6]*z[85];
    z[31]= - z[29] - z[31];
    z[104]=z[8] + z[3];
    z[31]=z[80]*z[104]*z[31];
    z[93]=z[93] + z[89];
    z[93]=z[93]*z[40];
    z[111]= - z[8]*z[102];
    z[31]=z[85] + z[31] + z[111] + z[93];
    z[31]=z[6]*z[31];
    z[85]= - z[108]*npow(z[10],5)*z[100];
    z[93]=z[37]*z[103];
    z[93]=z[109] + z[93];
    z[93]=z[41]*npow(z[12],5)*z[93];
    z[85]=z[93] + z[85];
    z[85]=z[85]*npow(z[11],5);
    z[93]=z[5]*z[3];
    z[109]= - z[5] + z[8];
    z[109]=z[4]*z[109];
    z[109]= - z[93] + z[109];
    z[109]=z[109]*z[43]*z[92];
    z[58]=z[3]*z[43]*z[58];
    z[111]=z[29]*z[9];
    z[112]= - z[27]*z[111];
    z[36]= - z[6]*z[7]*z[36];
    z[36]=z[112] + z[36];
    z[36]=z[36]*z[41];
    z[36]=z[58] + z[36];
    z[36]=z[1]*z[36];
    z[31]=n<T>(1,2)*z[36] + z[109] + z[31] + z[85];
    z[31]=z[1]*z[31];
    z[36]=z[7]*z[9];
    z[58]= - z[99]*z[36]*z[40];
    z[85]=2*z[7];
    z[109]=z[5]*z[99]*z[85];
    z[112]=n<T>(1,2)*z[7];
    z[113]= - z[112] - z[5];
    z[113]=z[113]*z[101];
    z[109]=z[109] + z[113];
    z[109]=z[2]*z[109];
    z[113]=z[99]*z[102];
    z[109]= - 6*z[113] + z[109];
    z[109]=z[2]*z[109];
    z[113]=z[99]*z[111];
    z[114]=z[9] - z[4];
    z[99]=z[4]*z[99]*z[114];
    z[101]= - z[9]*z[101];
    z[99]=z[99] + z[101];
    z[99]=z[6]*z[99];
    z[99]=z[113] + z[99];
    z[99]=z[6]*z[99];
    z[58]=n<T>(1,4)*z[99] + z[58] + z[109];
    z[58]=z[58]*z[110];
    z[99]=z[40]*z[9];
    z[89]=z[6]*z[89];
    z[89]=z[99] + z[89];
    z[89]=z[89]*z[41];
    z[58]=z[89] + z[58];
    z[58]=z[10]*z[58];
    z[89]=z[5] - 13*z[8];
    z[89]=z[89]*z[92];
    z[89]= - z[93] + z[89];
    z[89]=z[4]*z[89];
    z[93]=3*z[4];
    z[101]= - z[37]*z[93];
    z[89]=z[89] + z[101];
    z[89]=z[2]*z[89];
    z[101]=z[7] - z[16];
    z[101]=z[101]*z[54];
    z[109]= - z[73] + z[101];
    z[109]=z[8]*z[109];
    z[110]=z[76] + z[112];
    z[110]=z[8]*z[110];
    z[113]=z[9] - z[3];
    z[114]=z[5]*z[113];
    z[110]=n<T>(1,4)*z[114] + z[110];
    z[110]=z[4]*z[110];
    z[114]=z[50]*z[8];
    z[110]=z[114] + z[110];
    z[110]=z[4]*z[110];
    z[31]=z[31] + z[71] + z[58] + z[61] + z[89] + z[109] + z[110];
    z[31]=z[1]*z[31];
    z[58]=n<T>(7,4)*z[46];
    z[61]=z[9]*z[23];
    z[61]=n<T>(9,4)*z[48] + z[61] - z[58];
    z[61]=z[2]*z[61];
    z[71]= - z[9] + z[85];
    z[71]=z[71]*z[23];
    z[71]=z[71] + z[58];
    z[71]=z[4]*z[71];
    z[89]=z[46] + z[48];
    z[109]=3*z[6];
    z[89]=z[89]*z[109];
    z[61]=z[89] + z[71] + z[61];
    z[61]=z[6]*z[61];
    z[71]=z[23]*z[36];
    z[89]=n<T>(3,2)*z[7];
    z[46]=z[46]*z[89];
    z[46]=z[71] + z[46];
    z[46]=z[4]*z[46];
    z[71]=z[9]*z[19];
    z[110]=2*z[71];
    z[115]=n<T>(7,4)*z[7];
    z[116]=z[115] - 12*z[5];
    z[116]=z[5]*z[116];
    z[116]=z[116] - z[110] - 3*z[36];
    z[116]=z[116]*z[23];
    z[117]=n<T>(13,4)*z[5];
    z[118]=z[85] + z[117];
    z[48]=z[118]*z[48];
    z[58]=z[7]*z[58];
    z[48]=z[48] + z[116] + z[58];
    z[48]=z[2]*z[48];
    z[58]=z[102]*z[23];
    z[46]=z[61] + z[48] + 18*z[58] + z[46];
    z[46]=z[10]*z[46];
    z[48]=z[7]*z[18];
    z[61]=z[29]*z[48]*z[42];
    z[116]=9*z[13];
    z[58]= - z[58]*z[116];
    z[116]=z[68]*z[7];
    z[118]= - z[14]*z[116];
    z[58]=z[118] + z[58];
    z[118]=npow(z[7],2);
    z[119]= - z[18]*z[118]*z[23];
    z[116]= - z[116] + z[119];
    z[119]=2*z[2];
    z[116]=z[116]*z[119];
    z[120]= - z[7]*z[22];
    z[120]=z[120] + z[99];
    z[120]=z[120]*z[41];
    z[46]=z[46] + z[120] + z[116] + 2*z[58] + z[61];
    z[46]=z[10]*z[46];
    z[58]=z[95]*z[4];
    z[61]=z[2]*z[9];
    z[95]=z[58] - z[61];
    z[116]=3*z[7];
    z[120]= - z[116] + z[53];
    z[120]=z[8]*z[120];
    z[121]=z[6]*z[55];
    z[120]=z[121] + z[120] - z[95];
    z[120]=z[6]*z[120];
    z[121]=4*z[14];
    z[122]=z[50]*z[121];
    z[123]=z[98] + z[61];
    z[124]=z[123]*z[119];
    z[120]=z[120] + z[124] + z[122] - n<T>(47,2)*z[111];
    z[120]=z[6]*z[120];
    z[42]=z[32]*z[102]*z[42];
    z[122]=z[50]*z[7];
    z[124]=z[14]*z[122];
    z[42]=z[42] - z[77] + z[124];
    z[23]=z[23]*z[7];
    z[77]=npow(z[18],2);
    z[124]=z[77]*z[23];
    z[35]=n<T>(5,2)*z[124] + z[35];
    z[35]=z[4]*z[35];
    z[124]= - z[7]*z[34];
    z[35]=z[124] + z[35];
    z[35]=z[4]*z[35];
    z[69]= - z[69] + z[122];
    z[124]= - z[80]*z[106];
    z[69]=z[124] + 2*z[69] + z[102];
    z[69]=z[2]*z[69];
    z[35]=z[46] + z[120] + z[69] + 2*z[42] + z[35];
    z[35]=z[10]*z[35];
    z[42]=z[3]*z[18];
    z[46]=3*z[42];
    z[69]=n<T>(1,2) - z[46];
    z[69]=z[69]*z[112];
    z[120]=npow(z[18],3);
    z[23]=z[120]*z[23];
    z[124]=n<T>(1,4)*z[3];
    z[23]=n<T>(3,4)*z[23] + n<T>(13,2)*z[8] + z[69] + n<T>(47,2)*z[9] + 10*z[17] + 
    z[124];
    z[23]=z[4]*z[23];
    z[69]=n<T>(5,4)*z[3];
    z[125]=2*z[9];
    z[126]=z[69] + z[125];
    z[126]=z[126]*z[5];
    z[127]=4*z[17];
    z[90]= - z[127] - z[90];
    z[90]=z[7]*z[90];
    z[128]=9*z[9];
    z[129]= - z[128] - z[85];
    z[129]=5*z[129] + z[53];
    z[129]=z[8]*z[129];
    z[130]=2*z[34];
    z[23]=z[23] + z[129] - z[126] + z[90] + z[130] + z[50];
    z[23]=z[4]*z[23];
    z[90]= - n<T>(9,2)*z[4] - n<T>(1,4)*z[8] + z[66] + z[78] - z[94];
    z[90]=z[4]*z[90];
    z[129]=4*z[50];
    z[131]=n<T>(5,4)*z[7];
    z[132]=z[131] + z[5];
    z[132]=z[5]*z[132];
    z[133]=z[9] - 7*z[4];
    z[133]=z[2]*z[133];
    z[79]=z[133] + z[90] + z[132] + z[79] + z[129];
    z[79]=z[2]*z[79];
    z[90]=z[83]*z[78];
    z[132]=z[34]*z[13];
    z[90]=z[90] + z[127] + z[132];
    z[90]=z[3]*z[90];
    z[90]=z[34] + z[90];
    z[90]=z[90]*z[44];
    z[62]= - z[62] - z[132];
    z[62]=z[13]*z[62];
    z[133]= - z[78]*z[91];
    z[62]=z[62] + z[133];
    z[62]=z[3]*z[62];
    z[62]=z[62] - 5*z[17] - z[132];
    z[62]=z[3]*z[62];
    z[62]= - z[34] + z[62];
    z[62]=z[7]*z[62];
    z[133]= - z[15] + z[121];
    z[133]=z[133]*z[50];
    z[134]= - z[15]*z[125];
    z[69]=z[69] - z[7];
    z[69]=z[5]*z[69];
    z[69]=z[134] + z[69];
    z[69]=z[8]*z[69];
    z[23]=z[31] + z[24] + z[35] + z[45] + z[79] + z[23] + z[69] + z[101]
    + z[62] + z[90] + z[133];
    z[23]=z[1]*z[23];
    z[24]= - z[5] + z[4];
    z[24]=z[24]*z[92];
    z[31]= - 73*z[8] + 39*z[2];
    z[31]=z[31]*z[80];
    z[35]=19*z[8];
    z[45]=n<T>(39,2)*z[2] - z[35] - z[67];
    z[45]=z[6]*z[45];
    z[62]= - n<T>(11,4)*z[5] - z[8];
    z[62]=z[8]*z[62];
    z[24]=z[45] + z[31] + z[62] + z[24];
    z[24]=z[6]*z[24];
    z[31]=6*z[37];
    z[45]=npow(z[11],2);
    z[62]= - z[31] - z[49] - z[45];
    z[62]=z[2]*z[62];
    z[69]=z[45]*z[38];
    z[26]=7*z[45] - n<T>(3,4)*z[26];
    z[26]=z[4]*z[26];
    z[24]=z[24] + z[62] + z[26] - z[57] + z[69];
    z[24]=z[6]*z[24];
    z[26]=z[55] + z[78];
    z[57]= - n<T>(3,4)*z[5] - z[26];
    z[57]=z[4]*z[57];
    z[57]=z[57] - z[60];
    z[57]=z[4]*z[57];
    z[60]=z[45]*z[84];
    z[28]= - z[28]*z[93];
    z[62]=5*z[45];
    z[28]=z[62] + z[28];
    z[28]=z[28]*z[30];
    z[28]=z[28] + z[60] + z[57];
    z[28]=z[2]*z[28];
    z[57]=4*z[19];
    z[60]=z[57] - z[16];
    z[69]=3*z[3];
    z[79]= - z[69] + z[60];
    z[79]=z[5]*z[79];
    z[84]=npow(z[19],2);
    z[90]=4*z[84];
    z[79]=z[79] - z[90] + z[56];
    z[79]=z[5]*z[79];
    z[101]= - z[5]*z[44];
    z[101]=z[56] + z[101];
    z[101]=z[8]*z[101];
    z[79]=z[101] - z[73] + z[79];
    z[79]=z[8]*z[79];
    z[101]=7*z[9];
    z[133]=5*z[15];
    z[38]= - z[38] - z[133] - z[101];
    z[38]=z[8]*z[38];
    z[38]=n<T>(1,4)*z[87] + z[38];
    z[38]=z[38]*z[45];
    z[113]=z[113]*z[27];
    z[113]=z[113] + z[45];
    z[134]=n<T>(3,2)*z[4];
    z[113]=z[113]*z[134];
    z[113]=z[114] + z[113];
    z[113]=z[4]*z[113];
    z[114]=z[47] + n<T>(39,4)*z[37];
    z[114]=z[2]*z[114];
    z[135]=z[37]*z[6];
    z[114]=z[114] + n<T>(39,2)*z[135];
    z[136]=z[41]*z[12];
    z[114]=z[114]*z[136];
    z[137]= - z[5]*z[73];
    z[24]=z[114] + z[24] + z[28] + z[113] + z[38] + z[137] + z[79];
    z[24]=z[12]*z[24];
    z[28]=n<T>(3,4)*z[4];
    z[38]=z[28] - z[5] + z[104];
    z[38]=z[4]*z[38];
    z[79]= - 12*z[6] - n<T>(17,4)*z[2] + z[35] + 14*z[3] - z[52];
    z[79]=z[6]*z[79];
    z[113]=n<T>(13,2)*z[5] + z[70];
    z[113]=z[8]*z[113];
    z[114]=4*z[3];
    z[137]= - n<T>(19,4)*z[2] - z[114] - n<T>(31,2)*z[8];
    z[137]=z[2]*z[137];
    z[38]=z[79] + z[137] + z[38] + z[113] - 17*z[56] + z[54];
    z[38]=z[6]*z[38];
    z[79]=n<T>(3,4)*z[9];
    z[113]=z[45]*z[18];
    z[26]= - n<T>(25,4)*z[113] + z[79] + n<T>(1,2)*z[3] + z[26];
    z[26]=z[4]*z[26];
    z[137]= - z[44] + z[88];
    z[137]=z[5]*z[137];
    z[138]=z[53] + z[3] - 44*z[9];
    z[138]=z[8]*z[138];
    z[26]=z[26] + z[138] + z[137] + z[34] + z[50];
    z[26]=z[4]*z[26];
    z[137]=7*z[8];
    z[138]=z[137] - z[30];
    z[109]=z[138]*z[109];
    z[40]=z[109] - n<T>(27,2)*z[43] + z[40] + z[49] - n<T>(7,2)*z[45];
    z[40]=z[6]*z[40];
    z[109]= - z[75] + n<T>(9,2)*z[8];
    z[109]=3*z[109] - 41*z[4];
    z[109]=z[45]*z[109];
    z[138]=n<T>(31,2)*z[45] - 3*z[43];
    z[138]=z[138]*z[30];
    z[40]=z[40] + z[138] + z[109];
    z[40]=z[6]*z[40];
    z[109]=12*z[84];
    z[138]=5*z[5];
    z[139]=z[15] + z[138];
    z[139]=z[5]*z[139];
    z[140]= - z[9]*z[55];
    z[139]= - n<T>(47,4)*z[29] + z[140] - z[109] + z[139];
    z[139]=z[45]*z[139];
    z[140]=2*z[19];
    z[141]= - z[140] + n<T>(23,4)*z[5];
    z[141]=z[141]*z[45];
    z[142]= - z[2]*z[52];
    z[62]= - z[62] + z[142];
    z[62]=z[62]*z[30];
    z[62]=z[141] + z[62];
    z[62]=z[2]*z[62];
    z[40]=z[40] + z[62] + z[139];
    z[40]=z[10]*z[40];
    z[62]= - z[133] + z[69];
    z[62]=z[9]*z[62];
    z[82]=z[82] + n<T>(9,2)*z[3] + z[16] - 22*z[19];
    z[82]=z[5]*z[82];
    z[139]=z[3]*z[53];
    z[62]=z[139] + z[82] + z[109] + z[62];
    z[62]=z[8]*z[62];
    z[82]=z[3]*z[17];
    z[129]= - z[129] - z[84] + z[82];
    z[139]=z[140] - z[52];
    z[139]=z[5]*z[139];
    z[47]=z[47] + 2*z[129] + z[139];
    z[129]=z[18]*z[20];
    z[139]= - z[129] + n<T>(1,2)*z[25];
    z[139]=z[139]*z[45];
    z[141]=n<T>(1,2)*z[113];
    z[27]= - n<T>(15,4)*z[4] - z[141] + z[27] + 6*z[8];
    z[27]=z[2]*z[27];
    z[142]=2*z[17];
    z[143]=z[142] - z[5];
    z[143]= - n<T>(91,4)*z[4] + 2*z[143] + 43*z[8];
    z[143]=z[4]*z[143];
    z[27]=z[27] + z[143] + 2*z[47] + z[139];
    z[27]=z[2]*z[27];
    z[47]=z[9]*z[15];
    z[139]= - z[16] - z[44];
    z[139]=z[5]*z[139];
    z[47]=z[139] + 3*z[56] + z[47];
    z[47]=z[5]*z[47];
    z[139]=z[5]*z[81];
    z[143]=z[15]*z[25];
    z[51]=z[143] + 5*z[51] - n<T>(17,2)*z[139];
    z[51]=z[51]*z[45];
    z[82]=z[130] + 19*z[82];
    z[82]=z[3]*z[82];
    z[24]=z[24] + z[40] + z[38] + z[27] + z[26] + z[51] + z[62] + z[47]
    + z[82] - 2*z[73];
    z[24]=z[12]*z[24];
    z[26]=z[32]*z[9];
    z[27]=z[32]*z[7];
    z[38]= - n<T>(5,4)*z[27] - z[63] - n<T>(1,4)*z[26];
    z[38]=z[5]*z[38];
    z[40]=12*z[13];
    z[47]=z[40] - z[27];
    z[47]=z[6]*z[47];
    z[51]=n<T>(5,2)*z[13] + z[91];
    z[51]=z[7]*z[51];
    z[38]=z[47] + z[38] + z[51] - n<T>(5,4)*z[81] - n<T>(59,4) - z[86];
    z[38]=z[6]*z[38];
    z[47]=2*z[81];
    z[51]=7*z[27];
    z[62]=z[13] - z[51];
    z[62]=z[5]*z[62];
    z[62]=z[62] - n<T>(49,4)*z[72] + static_cast<T>(3)+ z[47];
    z[62]=z[5]*z[62];
    z[73]=static_cast<T>(1)- z[83];
    z[73]=z[73]*z[88];
    z[82]=z[45]*z[27];
    z[86]=n<T>(11,2)*z[8];
    z[139]= - static_cast<T>(1)+ 13*z[83];
    z[139]=z[3]*z[139];
    z[143]= - n<T>(3,2)*z[13] + z[91];
    z[143]=z[3]*z[143];
    z[143]= - n<T>(39,4) + z[143];
    z[143]=z[7]*z[143];
    z[38]=z[38] + z[80] + n<T>(83,4)*z[4] - 4*z[82] + z[86] + z[62] + z[143]
    + z[73] - z[121] + z[139];
    z[38]=z[6]*z[38];
    z[58]= - n<T>(3,2)*z[61] - z[58] - z[22] - 3*z[45];
    z[58]=z[6]*z[58];
    z[61]=z[4]*z[9];
    z[61]=n<T>(99,4)*z[61] + z[50] + 30*z[45];
    z[61]=z[4]*z[61];
    z[62]= - z[15] - z[75];
    z[62]=z[62]*z[50];
    z[73]=3*z[14];
    z[82]= - z[116] - z[73] + z[9];
    z[139]=2*z[45];
    z[82]=z[82]*z[139];
    z[143]=9*z[45];
    z[123]= - z[143] - z[123];
    z[123]=z[2]*z[123];
    z[58]=z[58] + z[123] + z[61] + z[62] + z[82];
    z[58]=z[6]*z[58];
    z[61]= - z[109] - 17*z[71];
    z[61]=z[9]*z[61];
    z[57]=n<T>(11,2)*z[5] - z[57] - z[89];
    z[57]=z[57]*z[45];
    z[62]=z[2]*z[5];
    z[82]=z[62] - z[106];
    z[82]=z[139] - n<T>(3,2)*z[82];
    z[82]=z[2]*z[82];
    z[57]=z[82] + z[57] - 8*z[102] + z[61] - 17*z[122];
    z[57]=z[2]*z[57];
    z[61]= - z[84] - z[110];
    z[61]=z[61]*z[50];
    z[68]= - z[68]*z[85];
    z[61]=z[61] + z[68];
    z[68]= - z[43]*z[100];
    z[61]=4*z[61] + z[68];
    z[61]=z[2]*z[61];
    z[41]=z[99]*z[41];
    z[61]=z[61] + z[41];
    z[61]=z[10]*z[61];
    z[68]=8*z[14];
    z[82]= - z[15] + z[68];
    z[82]=z[82]*z[122];
    z[89]=2*z[84];
    z[99]=z[89] + z[71];
    z[99]=z[9]*z[99];
    z[99]=z[99] + z[122];
    z[99]=z[99]*z[53];
    z[100]=z[89] - z[71];
    z[110]=z[14] + z[9];
    z[110]=z[7]*z[110];
    z[110]=z[110] - z[100];
    z[123]=2*z[15];
    z[144]=23*z[5] - z[123] - n<T>(21,4)*z[7];
    z[144]=z[5]*z[144];
    z[110]=6*z[110] + z[144];
    z[110]=z[110]*z[45];
    z[144]=n<T>(23,4)*z[7];
    z[145]=n<T>(21,4)*z[4] + z[144];
    z[145]=z[45]*z[145];
    z[145]=24*z[122] + z[145];
    z[145]=z[4]*z[145];
    z[57]=z[61] + z[58] + z[57] + z[145] + z[110] + z[82] + z[99];
    z[57]=z[10]*z[57];
    z[58]=z[48]*z[45];
    z[61]=2*z[4];
    z[82]= - z[94] + z[61];
    z[82]=z[4]*z[82];
    z[64]= - 13*z[19] - z[64];
    z[64]=z[9]*z[64];
    z[94]=z[144] - 14*z[5];
    z[94]=z[5]*z[94];
    z[99]=n<T>(23,4)*z[4] + z[141] - z[9] + n<T>(5,2)*z[5];
    z[99]=z[2]*z[99];
    z[64]=z[99] + z[82] + n<T>(13,2)*z[58] + z[94] + n<T>(9,2)*z[36] - z[109] + 
    z[64];
    z[64]=z[2]*z[64];
    z[59]= - z[59] + z[9] - z[112];
    z[82]=2*z[6];
    z[94]=z[13]*z[143];
    z[59]=z[82] + n<T>(63,4)*z[2] + n<T>(37,4)*z[4] + n<T>(5,2)*z[59] + z[94];
    z[59]=z[6]*z[59];
    z[94]=z[13]*z[85];
    z[94]= - z[81] + z[94];
    z[94]=z[94]*z[139];
    z[99]= - n<T>(5,4)*z[2] + z[101] - z[134];
    z[99]=z[2]*z[99];
    z[101]= - z[15] - 14*z[14];
    z[101]=z[9]*z[101];
    z[109]=4*z[8];
    z[110]=z[7] - z[109];
    z[110]=z[8]*z[110];
    z[112]=n<T>(109,4)*z[4] - 31*z[9] + z[131];
    z[112]=z[4]*z[112];
    z[59]=z[59] + z[99] + z[112] + z[94] + z[101] + z[110];
    z[59]=z[6]*z[59];
    z[94]=11*z[17];
    z[79]=n<T>(29,4)*z[113] + z[109] + z[94] - z[79];
    z[79]=z[4]*z[79];
    z[76]=z[17] - z[76];
    z[76]=z[76]*z[85];
    z[50]=z[79] + n<T>(51,4)*z[58] + z[76] + z[130] - z[50];
    z[50]=z[4]*z[50];
    z[58]=z[90] + z[71];
    z[76]= - z[96] + z[5];
    z[76]=z[5]*z[76];
    z[58]=z[76] + 3*z[58] + 11*z[36];
    z[58]=z[8]*z[58];
    z[76]=z[73]*z[18];
    z[79]=z[21]*z[7];
    z[96]=z[79] + z[76] - 4*z[81];
    z[96]=z[7]*z[96];
    z[99]=5*z[13];
    z[99]= - z[19]*z[99];
    z[47]=z[99] + z[47];
    z[47]=z[9]*z[47];
    z[47]=z[47] + z[96];
    z[96]=z[5]*z[13];
    z[99]=n<T>(23,4)*z[72] - 11*z[96];
    z[99]=z[5]*z[99];
    z[47]=2*z[47] + z[99];
    z[47]=z[47]*z[45];
    z[99]=z[15] + z[73];
    z[98]=z[99]*z[98];
    z[99]=z[15] - 10*z[14];
    z[99]=z[99]*z[36];
    z[47]=z[57] + z[59] + z[64] + z[50] + z[47] + z[58] + z[102] + z[98]
    + z[99];
    z[47]=z[10]*z[47];
    z[50]=z[77]*z[44];
    z[50]=n<T>(11,4)*z[18] + z[50];
    z[50]=z[7]*z[50];
    z[57]=4*z[77];
    z[58]=z[45]*z[57];
    z[59]=static_cast<T>(155)- 13*z[42];
    z[50]=z[58] + n<T>(1,4)*z[59] + z[50];
    z[50]=z[4]*z[50];
    z[58]=z[77]*z[45];
    z[58]=6*z[58] + n<T>(25,4) + 11*z[42];
    z[58]=z[7]*z[58];
    z[59]=16*z[8];
    z[50]=z[50] - z[59] - 41*z[9] + z[94] - n<T>(57,4)*z[3] + z[58];
    z[50]=z[4]*z[50];
    z[58]=z[142] - z[132];
    z[58]=z[13]*z[58];
    z[64]=z[13]*z[17];
    z[94]= - static_cast<T>(2)- 5*z[64];
    z[94]=z[94]*z[83];
    z[58]=z[94] - static_cast<T>(11)+ z[58];
    z[58]=z[3]*z[58];
    z[58]=z[85] + z[142] + z[58];
    z[58]=z[7]*z[58];
    z[94]= - z[13]*z[118]*z[21];
    z[98]=z[26] - 3*z[27];
    z[98]=z[98]*z[66];
    z[94]=z[94] + z[98];
    z[94]=z[94]*z[45];
    z[98]= - static_cast<T>(1)- 14*z[64];
    z[98]=z[3]*z[98];
    z[98]= - z[132] + z[98];
    z[98]=z[3]*z[98];
    z[99]= - z[15] - z[14];
    z[99]=2*z[99] + z[69];
    z[99]=z[9]*z[99];
    z[101]=z[72] - 1;
    z[110]=z[13]*z[16];
    z[112]=z[110] + 1;
    z[101]=z[5]*z[112]*z[101];
    z[112]=n<T>(5,4) - z[110];
    z[112]=z[7]*z[112];
    z[101]=z[101] + z[112] + z[3] + n<T>(7,4)*z[9];
    z[101]=z[5]*z[101];
    z[44]= - z[53] - 22*z[5] - z[7] + 33*z[9] + z[44] - z[15] + 19*z[19]
   ;
    z[44]=z[8]*z[44];
    z[112]=12*z[19];
    z[113]=4*z[2];
    z[118]=z[113] - n<T>(29,2)*z[4] + 32*z[8] + n<T>(51,4)*z[5] + z[7] - z[112]
    + 23*z[9];
    z[118]=z[2]*z[118];
    z[23]=z[23] + z[24] + z[47] + z[38] + z[118] + z[50] + z[94] + z[44]
    + z[101] + z[58] + z[99] + z[130] + z[98];
    z[23]=z[1]*z[23];
    z[24]= - z[15] + z[3];
    z[24]=z[24]*z[88];
    z[38]=n<T>(21,4)*z[3] - z[60];
    z[38]=z[5]*z[38];
    z[44]=static_cast<T>(3)+ z[42];
    z[44]=z[8]*z[3]*z[44];
    z[24]=z[44] + z[38] + z[24] + z[90] + z[56];
    z[24]=z[8]*z[24];
    z[38]=z[117] + z[55];
    z[38]=z[8]*z[38];
    z[44]=z[4] - z[66] + z[104];
    z[44]=z[4]*z[44];
    z[47]= - z[97] - 35*z[2];
    z[47]=z[47]*z[80];
    z[50]= - n<T>(65,2)*z[2] + 39*z[8] - z[67];
    z[50]=z[50]*z[39];
    z[38]=z[50] + z[47] + z[38] + z[44];
    z[38]=z[6]*z[38];
    z[44]=z[8]*z[74];
    z[28]=z[3]*z[28];
    z[28]=z[28] - z[126] + z[44];
    z[28]=z[4]*z[28];
    z[44]=z[5] + z[17];
    z[44]=2*z[44] - n<T>(5,2)*z[8];
    z[44]=z[4]*z[44];
    z[31]=z[31] + 8*z[22] + z[44];
    z[31]=z[2]*z[31];
    z[35]= - n<T>(37,2)*z[2] + z[35] - z[67];
    z[35]=z[6]*z[35];
    z[35]=z[35] - n<T>(21,4)*z[43] + z[107];
    z[35]=z[35]*z[105];
    z[44]= - 41*z[8] + n<T>(21,2)*z[2];
    z[44]=z[44]*z[30];
    z[47]=n<T>(37,2)*z[6];
    z[50]= - z[8] + z[2];
    z[47]=z[50]*z[47];
    z[44]=z[47] - z[22] + z[44];
    z[44]=z[6]*z[44];
    z[47]= - 6*z[22] - n<T>(35,4)*z[37];
    z[47]=z[2]*z[47];
    z[44]=z[47] + z[44];
    z[44]=z[6]*z[44];
    z[37]=z[22] + n<T>(21,4)*z[37];
    z[37]=z[2]*z[37];
    z[37]=z[37] + n<T>(37,2)*z[135];
    z[37]=z[37]*z[136];
    z[37]=z[44] + z[37];
    z[37]=z[12]*z[37];
    z[44]=3*z[15];
    z[47]= - z[44] + z[9];
    z[47]=z[47]*z[87];
    z[24]=z[37] + z[35] + z[38] + z[31] + z[28] + z[47] + z[24];
    z[24]=z[12]*z[24];
    z[28]=6*z[19];
    z[31]=static_cast<T>(13)+ z[46];
    z[31]=z[3]*z[31];
    z[31]= - z[137] - z[52] + 27*z[9] + z[31] - z[133] + z[28];
    z[31]=z[8]*z[31];
    z[33]= - z[137] + z[33];
    z[33]=16*z[6] + 2*z[33] + n<T>(5,2)*z[2];
    z[33]=z[6]*z[33];
    z[33]=z[33] + n<T>(25,4)*z[43] - z[49] + n<T>(23,4)*z[29];
    z[33]=z[6]*z[33];
    z[35]=z[4] + z[2];
    z[35]=z[35]*z[39];
    z[22]=z[35] + 9*z[43] + z[22] + n<T>(7,2)*z[29];
    z[22]=z[6]*z[22];
    z[22]=n<T>(3,2)*z[108] + z[22];
    z[22]=z[6]*z[22];
    z[35]=z[108]*z[5];
    z[22]=n<T>(3,2)*z[35] + z[22];
    z[22]=z[10]*z[22];
    z[35]=z[84]*z[70];
    z[37]=z[84] + 3*z[54];
    z[37]=4*z[37] - n<T>(5,2)*z[62];
    z[37]=z[2]*z[37];
    z[22]=z[22] + z[33] + z[35] + z[37];
    z[22]=z[10]*z[22];
    z[33]=18*z[19];
    z[35]= - z[52] - n<T>(17,2)*z[9] + z[114] - z[33] + z[44] - z[20] + 
    z[16];
    z[35]=z[5]*z[35];
    z[37]=static_cast<T>(3)- z[25];
    z[37]=z[37]*z[119];
    z[38]=5*z[9];
    z[39]= - z[8]*z[21];
    z[39]=n<T>(3,2) + z[39];
    z[39]=z[8]*z[39];
    z[28]=z[37] + n<T>(17,2)*z[4] + z[39] - n<T>(31,4)*z[5] + z[28] - z[38];
    z[28]=z[2]*z[28];
    z[37]=z[44] - z[14];
    z[39]=14*z[9];
    z[43]= - z[39] - 2*z[37] + z[69];
    z[43]=z[9]*z[43];
    z[44]= - static_cast<T>(1)- n<T>(9,4)*z[42];
    z[44]=z[4]*z[44];
    z[44]=z[44] + z[53] - z[9] + z[78] - n<T>(15,2)*z[3];
    z[44]=z[4]*z[44];
    z[34]=z[90] - z[34];
    z[46]=z[5] + z[14] + 10*z[3];
    z[46]= - n<T>(5,2)*z[6] - n<T>(37,4)*z[2] + 28*z[4] + 2*z[46] + z[86];
    z[46]=z[6]*z[46];
    z[47]=z[3] - 19*z[17];
    z[47]=z[3]*z[47];
    z[22]=z[24] + z[22] + z[46] + z[28] + z[44] + z[31] + z[35] + z[43]
    + 3*z[34] + z[47];
    z[22]=z[12]*z[22];
    z[24]= - z[7] - n<T>(23,2)*z[5];
    z[24]=z[24]*z[30];
    z[28]=n<T>(37,4)*z[7];
    z[31]= - z[28] + 25*z[5];
    z[31]=z[5]*z[31];
    z[34]= - z[7]*z[134];
    z[24]=z[24] + z[34] + z[31] + n<T>(53,4)*z[36] + 8*z[84] + 19*z[71];
    z[24]=z[2]*z[24];
    z[31]=z[89] + 5*z[71];
    z[31]=z[9]*z[31];
    z[31]=z[31] + 5*z[122];
    z[34]=z[7] + z[138];
    z[34]=z[2]*z[34];
    z[34]= - n<T>(13,2)*z[106] + z[34];
    z[30]=z[34]*z[30];
    z[30]=z[30] + 2*z[31] + 13*z[102];
    z[30]=z[2]*z[30];
    z[31]=z[108]*z[106];
    z[31]=z[31] + z[41];
    z[31]=z[10]*z[31];
    z[29]= - z[29]*z[36];
    z[34]= - z[6]*z[95];
    z[34]=z[111] + z[34];
    z[34]=z[6]*z[34];
    z[29]=z[31] + z[34] + z[29] + z[30];
    z[29]=z[10]*z[29];
    z[30]= - z[128] + n<T>(5,2)*z[4];
    z[30]=n<T>(1,2)*z[30] - z[113];
    z[30]=z[2]*z[30];
    z[31]=z[7] + z[109];
    z[31]= - n<T>(45,4)*z[2] + 2*z[31] - z[65];
    z[31]=z[6]*z[31];
    z[34]=z[15] + z[68];
    z[34]=z[9]*z[34];
    z[35]= - n<T>(65,2)*z[4] - z[125] - n<T>(9,4)*z[7];
    z[35]=z[4]*z[35];
    z[30]=z[31] + z[30] + z[34] + z[35];
    z[30]=z[6]*z[30];
    z[31]= - 6*z[84] - 11*z[71];
    z[31]=z[31]*z[125];
    z[34]= - z[39] + z[15] - 12*z[14];
    z[34]=z[34]*z[36];
    z[35]=z[100]*z[53];
    z[39]= - z[7]*z[92];
    z[39]= - 28*z[36] + z[39];
    z[39]=z[4]*z[39];
    z[24]=z[29] + z[30] + z[24] + z[39] + z[35] - 20*z[102] + z[31] + 
    z[34];
    z[24]=z[10]*z[24];
    z[29]= - n<T>(29,2) + z[79];
    z[29]=z[7]*z[29];
    z[29]= - 5*z[2] - z[61] - n<T>(81,4)*z[5] + z[29] + z[33] - z[38];
    z[29]=z[2]*z[29];
    z[30]=10*z[19];
    z[31]= - z[30] + z[37];
    z[31]=2*z[31] + 21*z[9];
    z[31]=z[9]*z[31];
    z[33]= - z[85] + z[68] + n<T>(23,2)*z[9];
    z[33]=z[7]*z[33];
    z[34]= - static_cast<T>(22)+ z[72];
    z[34]=z[5]*z[34];
    z[34]=z[34] - z[15] - z[115];
    z[34]=z[5]*z[34];
    z[35]= - static_cast<T>(9)- n<T>(1,2)*z[48];
    z[35]=z[4]*z[35];
    z[35]=n<T>(13,2)*z[35] + z[8] + n<T>(77,2)*z[7] + z[78] + z[9];
    z[35]=z[4]*z[35];
    z[37]= - z[13]*z[82];
    z[37]=z[37] + n<T>(53,4) - 5*z[72];
    z[37]=z[6]*z[37];
    z[38]=5*z[14] + 4*z[9];
    z[37]=z[37] + n<T>(7,2)*z[2] - 48*z[4] + n<T>(21,2)*z[8] + 2*z[38] - n<T>(11,2)*
    z[7];
    z[37]=z[6]*z[37];
    z[38]=z[19] - z[5];
    z[38]=z[8]*z[38];
    z[24]=z[24] + z[37] + z[29] + z[35] + z[38] + z[34] + z[31] + z[33];
    z[24]=z[10]*z[24];
    z[29]=n<T>(57,4) + z[110];
    z[29]=z[29]*z[72];
    z[31]= - z[13] + 6*z[27];
    z[31]=z[5]*z[31];
    z[29]=z[31] + z[29] - n<T>(5,2)*z[81] + z[83] + static_cast<T>(11)- 5*z[110];
    z[29]=z[5]*z[29];
    z[31]=n<T>(27,4)*z[13] - z[91];
    z[31]=z[31]*z[116];
    z[27]=11*z[27] + 14*z[13] - z[26];
    z[27]=z[5]*z[27];
    z[33]= - z[40] + z[51];
    z[33]=z[6]*z[33];
    z[27]=z[33] + z[27] + z[31] + n<T>(3,4)*z[81] + static_cast<T>(25)- 11*z[83];
    z[27]=z[6]*z[27];
    z[31]=z[18] + z[13];
    z[31]=z[31]*z[83];
    z[33]= - n<T>(3,2) + z[64];
    z[33]=z[13]*z[33];
    z[31]=z[31] + n<T>(19,4)*z[18] + z[33];
    z[31]=z[3]*z[31];
    z[33]= - z[18] + z[13];
    z[33]=z[33]*z[85];
    z[31]=z[33] - static_cast<T>(17)+ z[31];
    z[31]=z[7]*z[31];
    z[33]= - z[120]*z[124];
    z[33]= - z[77] + z[33];
    z[33]=z[7]*z[33];
    z[34]=z[77]*z[3];
    z[35]=7*z[18];
    z[33]=z[33] - z[35] + n<T>(3,4)*z[34];
    z[33]=z[33]*z[93];
    z[34]=z[35] - 5*z[34];
    z[34]=z[7]*z[34];
    z[37]= - static_cast<T>(109)+ 15*z[42];
    z[33]=z[33] + n<T>(3,4)*z[37] + z[34];
    z[33]=z[4]*z[33];
    z[34]=z[21]*z[2];
    z[37]=n<T>(9,2) - 5*z[48];
    z[25]= - z[34] + 3*z[37] - 17*z[25];
    z[25]=z[2]*z[25];
    z[37]=z[21] + z[13];
    z[37]=z[3]*z[37];
    z[37]=z[37] + static_cast<T>(2)+ 9*z[64];
    z[37]=z[3]*z[37];
    z[38]=z[121] + z[15];
    z[39]=8*z[19];
    z[40]=n<T>(73,2) + 3*z[83];
    z[40]=z[9]*z[40];
    z[22]=z[23] + z[22] + z[24] + z[27] + z[25] + z[33] + n<T>(7,2)*z[8] + 
    z[29] + z[31] + z[40] + z[37] + 3*z[132] - z[142] + z[39] + z[20] - 
    z[38];
    z[22]=z[1]*z[22];
    z[23]=z[11]*z[14];
    z[24]=z[23]*z[21];
    z[24]=z[24] + z[15];
    z[25]= - z[63] + z[26];
    z[25]=z[25]*z[87];
    z[26]=z[18]*z[15];
    z[26]=static_cast<T>(2)+ z[26];
    z[26]=z[8]*z[26];
    z[25]=z[26] + z[25] - z[9] + z[75] + z[20] - z[24];
    z[25]=z[11]*z[25];
    z[26]=z[4]*z[11];
    z[27]=z[26] + z[45];
    z[27]=z[27]*z[4];
    z[29]= - z[14]*z[45];
    z[29]=z[29] - z[27];
    z[29]=z[10]*z[29];
    z[29]=z[29] + z[23] + z[26];
    z[29]=z[82]*z[29];
    z[27]=4*z[27];
    z[31]= - z[8]*z[9];
    z[31]=z[31] - 4*z[23];
    z[31]=z[11]*z[31];
    z[29]=z[31] - z[27] + z[29];
    z[29]=z[10]*z[29];
    z[31]=z[26]*z[18];
    z[33]=z[11]*z[18];
    z[37]=static_cast<T>(1)- z[33];
    z[37]=z[11]*z[37];
    z[37]=z[37] - z[31];
    z[37]=z[37]*z[61];
    z[40]=z[2]*z[11];
    z[41]= - static_cast<T>(1)+ z[129];
    z[41]=z[41]*z[40];
    z[42]=z[9] + z[15];
    z[42]= - z[11]*z[8]*z[42];
    z[43]= - z[20]*z[40];
    z[42]=z[42] + z[43];
    z[42]=z[12]*z[42];
    z[25]=z[42] + z[29] + z[41] + z[25] + z[37];
    z[25]=z[12]*z[25];
    z[29]=z[6]*z[11];
    z[37]=z[14] + z[11];
    z[37]=z[11]*z[37];
    z[37]=z[29] + z[37] + z[26];
    z[37]=z[6]*z[37];
    z[41]=z[5]*z[15];
    z[42]=z[14] + z[19];
    z[42]=z[11]*z[42];
    z[42]= - z[41] + 4*z[42];
    z[42]=z[11]*z[42];
    z[27]=4*z[37] + z[42] + z[27];
    z[27]=z[10]*z[27];
    z[37]=8*z[18];
    z[37]=z[23]*z[37];
    z[30]=z[30] - z[17];
    z[37]=z[37] + z[5] - z[30] - z[75] + z[15];
    z[37]=z[11]*z[37];
    z[42]= - static_cast<T>(3)+ 8*z[33];
    z[42]=z[11]*z[42];
    z[31]=z[42] + 8*z[31];
    z[31]=z[4]*z[31];
    z[27]=z[27] - 10*z[29] + z[40] + z[37] + z[31];
    z[27]=z[10]*z[27];
    z[23]=z[23]*z[57];
    z[31]= - z[32]*z[125];
    z[31]=z[63] + z[31];
    z[31]=z[5]*z[31];
    z[37]=6*z[14];
    z[42]=z[15] - z[37];
    z[42]=z[18]*z[42];
    z[31]=z[23] + z[31] - 3*z[81] + static_cast<T>(1)+ z[42];
    z[31]=z[11]*z[31];
    z[42]=3*z[17] - z[140] + z[73] + z[20] - z[123];
    z[43]=z[57]*z[26];
    z[44]=z[57]*z[11];
    z[35]= - z[35] + z[44];
    z[35]=z[11]*z[35];
    z[35]=z[43] - n<T>(53,2) + z[35];
    z[35]=z[4]*z[35];
    z[45]=static_cast<T>(5)+ z[33];
    z[45]=z[2]*z[45];
    z[25]=z[25] + z[27] - n<T>(7,2)*z[6] + z[45] + z[35] + z[31] + z[59] + 
    z[66] + 2*z[42] - n<T>(43,4)*z[9];
    z[25]=z[12]*z[25];
    z[27]=z[21]*z[14];
    z[31]= - static_cast<T>(1)+ z[27];
    z[31]=z[31]*z[79];
    z[35]= - z[18]*z[38];
    z[30]=z[13]*z[30];
    z[23]= - z[23] - 4*z[96] + z[31] + z[30] + static_cast<T>(2)+ z[35];
    z[23]=z[11]*z[23];
    z[30]=z[7]*z[27];
    z[24]=4*z[5] + z[30] + z[17] - z[112] - z[37] - z[24];
    z[24]=z[11]*z[24];
    z[30]= - z[26]*z[36];
    z[31]=z[11]*z[36];
    z[35]=z[7]*z[26];
    z[31]=z[31] + z[35];
    z[31]=z[2]*z[31];
    z[35]= - z[11]*z[9];
    z[35]=z[35] - z[26];
    z[35]=z[2]*z[35];
    z[36]=z[9]*z[26];
    z[35]=z[36] + z[35];
    z[35]=z[6]*z[35];
    z[30]=z[35] + z[30] + z[31];
    z[30]=z[10]*z[30];
    z[31]= - z[11]*z[41];
    z[30]=z[31] + n<T>(1,4)*z[30];
    z[30]=z[10]*z[30];
    z[31]= - z[11]*z[21];
    z[31]=z[31] - static_cast<T>(5)+ z[48];
    z[31]=z[11]*z[31];
    z[21]= - z[21]*z[26];
    z[21]=z[31] + z[21];
    z[21]=z[4]*z[21];
    z[31]= - static_cast<T>(1)+ z[48];
    z[31]=z[31]*z[40];
    z[21]=z[30] - 12*z[29] + 2*z[31] + z[24] + z[21];
    z[21]=z[10]*z[21];
    z[24]=z[77]*z[85];
    z[24]= - z[44] - z[18] + z[24];
    z[24]=z[11]*z[24];
    z[24]= - z[43] + static_cast<T>(57)+ z[24];
    z[24]=z[4]*z[24];
    z[29]= - n<T>(5,2) - z[33];
    z[29]=z[2]*z[29];
    z[30]=z[11]*z[13];
    z[30]= - n<T>(29,4) + 10*z[30];
    z[30]=z[6]*z[30];
    z[21]=z[21] + z[30] + z[29] + z[24] + z[23] - n<T>(19,2)*z[5] + z[28] - 
   n<T>(57,4)*z[9] + 8*z[17] + z[15] + z[39];
    z[21]=z[10]*z[21];
    z[23]=z[27] + 1;
    z[24]=z[23]*z[77];
    z[27]=z[13]*z[18];
    z[24]=z[24] + z[27];
    z[24]=z[7]*z[24];
    z[27]= - z[11]*z[120]*z[75];
    z[23]=z[18]*z[23];
    z[28]=z[5]*z[32];
    z[23]=z[27] + z[28] + z[23] + z[24];
    z[23]=z[11]*z[23];
    z[24]= - 2*z[11] + z[7];
    z[24]=z[120]*z[24];
    z[24]=z[57] + z[24];
    z[24]=z[11]*z[24];
    z[26]=z[120]*z[26];
    z[24]= - 2*z[26] + 24*z[18] + z[24];
    z[24]=z[4]*z[24];
    z[26]= - z[19] - z[127];
    z[26]=z[13]*z[26];
    z[26]=z[26] - static_cast<T>(2)- z[76];
    z[27]= - n<T>(45,4)*z[18] - 11*z[13];
    z[27]=z[7]*z[27];
    z[28]=z[13]*z[103];

    r += z[21] + z[22] + z[23] + z[24] + z[25] + 2*z[26] + z[27] + 
      z[28] + z[34] + n<T>(15,2)*z[81] + n<T>(9,2)*z[96];
 
    return r;
}

template double qg_2lNLC_r508(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r508(const std::array<dd_real,31>&);
#endif
