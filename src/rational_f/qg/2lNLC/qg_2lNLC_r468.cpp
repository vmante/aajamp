#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r468(const std::array<T,31>& k) {
  T z[37];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[12];
    z[4]=k[15];
    z[5]=k[2];
    z[6]=k[5];
    z[7]=k[6];
    z[8]=k[14];
    z[9]=k[4];
    z[10]=k[7];
    z[11]=k[10];
    z[12]=k[9];
    z[13]=k[3];
    z[14]=z[10]*z[13];
    z[15]=npow(z[7],2);
    z[16]=z[15]*z[14];
    z[17]=4*z[8];
    z[18]=z[10] + z[8];
    z[18]=z[18]*z[17];
    z[19]=npow(z[8],2);
    z[20]=2*z[19];
    z[21]=z[15] - z[20];
    z[22]=2*z[9];
    z[21]=z[22]*z[10]*z[21];
    z[16]=z[21] - 7*z[16] + z[18];
    z[16]=z[9]*z[16];
    z[18]=z[5]*z[12];
    z[18]= - static_cast<T>(2)+ z[18];
    z[21]= - z[2]*z[5];
    z[18]=2*z[18] + z[21];
    z[21]=2*z[2];
    z[18]=z[18]*z[21];
    z[23]=3*z[10];
    z[16]=z[18] + z[16] - 4*z[3] + z[23] - z[17];
    z[16]=z[11]*z[16];
    z[18]=npow(z[5],2);
    z[24]=z[15]*z[18];
    z[24]= - static_cast<T>(4)- 3*z[24];
    z[25]=z[15]*z[5];
    z[26]=z[9]*z[15];
    z[26]=z[25] - z[26];
    z[23]=z[23] - z[3] + 6*z[26];
    z[23]=z[9]*z[23];
    z[26]= - z[18]*z[21];
    z[26]= - 5*z[5] + z[26];
    z[26]=z[2]*z[26];
    z[23]=z[26] + 2*z[24] + 3*z[23];
    z[23]=z[11]*z[23];
    z[24]=z[6]*z[8];
    z[26]=z[24]*z[15];
    z[27]=2*z[3];
    z[28]= - z[12]*z[27];
    z[28]=z[28] + 5*z[26];
    z[28]=z[6]*z[28];
    z[25]=z[8]*z[25];
    z[25]=12*z[26] + 5*z[25];
    z[25]=z[5]*z[25];
    z[25]=z[25] - 2*z[12] + z[28];
    z[26]=z[6]*z[3];
    z[28]=z[26] + 1;
    z[29]=z[28]*z[6];
    z[29]=z[29] + z[5];
    z[30]= - z[29]*z[21];
    z[30]=z[30] - static_cast<T>(4)- 5*z[26];
    z[30]=z[2]*z[30];
    z[23]=z[23] + 2*z[25] + z[30];
    z[23]=z[4]*z[23];
    z[25]=z[19]*z[5];
    z[30]=z[25]*z[6];
    z[31]= - z[19]*npow(z[6],2);
    z[31]=z[31] - z[30];
    z[32]=4*z[5];
    z[33]=npow(z[7],3);
    z[31]=z[32]*z[33]*z[31];
    z[26]=static_cast<T>(1)+ 2*z[26];
    z[34]= - z[2]*z[26];
    z[34]= - z[3] + z[34];
    z[34]=z[34]*z[21];
    z[35]=npow(z[3],2);
    z[33]=z[10]*z[33]*npow(z[9],2);
    z[33]= - z[35] + 6*z[33];
    z[33]=z[9]*z[33];
    z[36]= - z[10] + z[3];
    z[33]=3*z[36] + z[33];
    z[33]=z[11]*z[33];
    z[31]=z[33] + z[34] - z[35] + z[31];
    z[31]=z[4]*z[31];
    z[33]= - z[1]*z[4];
    z[33]=z[33] + 1;
    z[34]=npow(z[2],2);
    z[27]=z[33]*z[34]*z[27];
    z[27]=z[31] + z[27];
    z[27]=z[1]*z[27];
    z[26]=z[26]*z[34];
    z[20]=z[26] + z[20] + z[35];
    z[15]=z[15]*z[30];
    z[26]=z[9]*z[10];
    z[30]=4*z[26];
    z[19]= - z[19]*z[30];
    z[31]=z[10]*z[35]*z[6];
    z[15]=z[27] + z[23] + z[16] + z[19] - 4*z[15] - z[31] + 2*z[20];
    z[15]=z[1]*z[15];
    z[16]=z[8]*z[26];
    z[16]= - 5*z[10] + z[16];
    z[16]=z[16]*z[22];
    z[19]=4*z[9];
    z[20]=3*z[12];
    z[22]=z[5]*z[20];
    z[22]= - static_cast<T>(8)+ z[22];
    z[22]=z[5]*z[22];
    z[22]=z[22] + z[19];
    z[22]=z[2]*z[22];
    z[23]=z[5]*z[8];
    z[27]=static_cast<T>(3)+ z[14];
    z[16]=z[22] + z[16] + 3*z[27] - 2*z[23];
    z[16]=z[11]*z[16];
    z[22]= - z[20]*z[28];
    z[27]=8*z[8];
    z[22]=z[27] + z[22];
    z[22]=z[6]*z[22];
    z[20]= - z[20] + z[27];
    z[20]=z[5]*z[20];
    z[27]=static_cast<T>(7)- z[30];
    z[27]=z[9]*z[27];
    z[27]= - z[32] + z[27];
    z[18]= - z[2]*z[18];
    z[18]=3*z[27] + z[18];
    z[18]=z[11]*z[18];
    z[27]=z[2]*z[29];
    z[18]=z[18] - 3*z[27] + z[20] - static_cast<T>(8)+ z[22];
    z[18]=z[4]*z[18];
    z[20]=static_cast<T>(1)+ z[14];
    z[20]=z[20]*z[35];
    z[20]=z[20] - z[31];
    z[20]=z[6]*z[20];
    z[22]=z[10] - 2*z[8];
    z[19]=z[8]*z[22]*z[19];
    z[22]= - z[9] + z[29];
    z[21]=z[22]*z[21];
    z[21]= - static_cast<T>(3)+ z[21];
    z[21]=z[2]*z[21];
    z[14]= - static_cast<T>(1)- 2*z[14];
    z[14]=z[3]*z[14];
    z[14]=z[15] + z[18] + z[16] + z[21] + z[19] + 8*z[25] + z[20] + 
    z[17] + z[14];
    z[14]=z[1]*z[14];
    z[15]=z[9] - z[6] - 2*z[5];
    z[15]=z[4]*z[15];
    z[15]=z[24] + z[15];
    z[16]=z[26] - 2;
    z[17]=z[7]*z[13];
    z[16]=z[17]*z[16];
    z[16]= - static_cast<T>(9)+ z[16];
    z[16]=z[9]*z[16];
    z[17]=static_cast<T>(7)+ z[17];
    z[17]=z[5]*z[17];
    z[16]=z[16] + 7*z[13] + z[17];
    z[16]=z[11]*z[16];
    z[17]= - z[26] + 1;
    z[17]=z[7]*z[17];
    z[17]= - 6*z[8] + 2*z[10] + z[17];
    z[17]=z[9]*z[17];

    r +=  - static_cast<T>(1)+ z[14] + 10*z[15] + z[16] + z[17] + 16*z[23];
 
    return r;
}

template double qg_2lNLC_r468(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r468(const std::array<dd_real,31>&);
#endif
