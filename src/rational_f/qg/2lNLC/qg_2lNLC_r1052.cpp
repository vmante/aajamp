#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1052(const std::array<T,31>& k) {
  T z[157];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[14];
    z[5]=k[19];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[11];
    z[9]=k[5];
    z[10]=k[6];
    z[11]=k[8];
    z[12]=k[15];
    z[13]=k[9];
    z[14]=k[16];
    z[15]=k[2];
    z[16]=k[4];
    z[17]=k[29];
    z[18]=k[26];
    z[19]=k[30];
    z[20]=k[3];
    z[21]=k[23];
    z[22]=k[20];
    z[23]=npow(z[1],2);
    z[24]=2*z[23];
    z[25]=z[9]*z[1];
    z[26]=z[24] + z[25];
    z[26]=z[26]*z[9];
    z[27]=z[25] + z[23];
    z[28]=npow(z[9],2);
    z[29]=z[4]*z[27]*z[28];
    z[30]=z[26] + z[29];
    z[31]=3*z[23];
    z[32]=z[31] + z[25];
    z[32]=z[16]*z[32];
    z[30]=2*z[30] + z[32];
    z[30]=z[6]*z[30];
    z[32]=z[23]*z[16];
    z[33]=z[23]*z[9];
    z[34]=z[32] - z[33];
    z[35]=2*z[16];
    z[36]=npow(z[10],2);
    z[37]= - z[34]*z[36]*z[35];
    z[38]=3*z[25];
    z[39]=z[23] + z[38];
    z[39]=z[9]*z[39];
    z[40]=npow(z[1],3);
    z[39]= - z[40] + z[39];
    z[41]=2*z[4];
    z[39]=z[39]*z[41];
    z[42]=z[16]*z[1];
    z[43]=4*z[42];
    z[30]=z[37] + z[30] + z[43] + z[25] + z[39];
    z[37]=z[10]*z[16];
    z[39]=z[40]*npow(z[37],3);
    z[44]=n<T>(16,3)*z[40];
    z[45]= - z[44] + 27*z[32];
    z[45]=z[45]*z[16];
    z[46]= - z[6]*z[45];
    z[47]=z[42] - z[23];
    z[48]=z[47]*z[16];
    z[49]=n<T>(1,3)*z[40];
    z[50]=z[48] + z[49];
    z[50]=z[50]*z[16];
    z[51]=z[1]*z[37];
    z[51]=npow(z[51],4);
    z[50]=z[50] - n<T>(1,3)*z[51];
    z[52]=16*z[2];
    z[53]= - z[50]*z[52];
    z[54]=npow(z[16],2);
    z[55]=z[54]*z[1];
    z[46]=z[53] - n<T>(113,3)*z[39] + 16*z[55] + z[46];
    z[46]=z[12]*z[46];
    z[53]=n<T>(1,3)*z[6];
    z[56]=z[40]*z[16];
    z[57]=z[53]*z[56];
    z[55]= - z[55] - z[57] + n<T>(2,3)*z[39];
    z[57]=16*z[3];
    z[55]=z[55]*z[57];
    z[58]=2*z[42];
    z[59]=2*z[25];
    z[60]= - z[58] + z[31] - z[59];
    z[60]=z[16]*z[60];
    z[61]=z[28]*z[23];
    z[62]=z[61]*z[4];
    z[63]=2*z[33];
    z[64]=z[63] + z[62];
    z[60]=2*z[64] + z[60];
    z[64]= - z[44] - 43*z[48];
    z[64]=z[16]*z[64];
    z[51]=z[64] + n<T>(16,3)*z[51];
    z[51]=z[3]*z[51];
    z[51]=27*z[60] + z[51];
    z[51]=z[2]*z[51];
    z[60]=z[25] - z[23];
    z[64]=z[60]*z[9];
    z[65]=npow(z[1],4);
    z[66]=z[65]*z[41];
    z[67]=2*z[40];
    z[66]=z[66] - z[67] - z[64];
    z[68]=27*z[11];
    z[66]=z[66]*z[68];
    z[30]=z[46] + z[66] + z[51] + 27*z[30] - z[55];
    z[30]=z[17]*z[30];
    z[46]= - 95*z[23] - 142*z[25];
    z[46]=z[9]*z[46];
    z[46]=z[46] + 20*z[29];
    z[51]=n<T>(2,3)*z[4];
    z[46]=z[46]*z[51];
    z[66]= - z[24] + z[42];
    z[66]=z[16]*z[66];
    z[66]=z[67] + z[66];
    z[69]=9*z[7];
    z[66]=z[66]*z[69];
    z[70]=9*z[42];
    z[71]=7*z[23];
    z[46]=z[66] - z[70] + z[46] - z[71] - n<T>(115,3)*z[25];
    z[46]=z[6]*z[46];
    z[66]=z[36]*z[23];
    z[72]=z[66]*z[54];
    z[73]=3*z[32];
    z[74]= - n<T>(5,3)*z[40] + z[73];
    z[75]=4*z[6];
    z[74]=z[74]*z[75];
    z[76]=9*z[23];
    z[55]= - z[55] + n<T>(74,3)*z[72] + z[74] - z[76] - n<T>(56,3)*z[42];
    z[55]=z[3]*z[55];
    z[74]=9*z[25];
    z[77]= - n<T>(149,3)*z[23] - z[74];
    z[77]=z[9]*z[77];
    z[77]=z[77] + n<T>(20,3)*z[62];
    z[77]=z[4]*z[77];
    z[78]=n<T>(47,3)*z[25];
    z[79]=n<T>(20,3)*z[42];
    z[77]= - z[79] + z[77] - 35*z[23] - z[78];
    z[80]= - n<T>(113,3)*z[23] + 5*z[42];
    z[80]=z[16]*z[80];
    z[50]= - z[50]*z[57];
    z[50]=z[50] - 10*z[39] + n<T>(20,3)*z[40] + z[80];
    z[50]=z[3]*z[50];
    z[80]=z[27]*z[9];
    z[81]=z[40] + 2*z[80];
    z[82]= - z[81]*z[69];
    z[50]=z[50] + 2*z[77] + z[82];
    z[50]=z[2]*z[50];
    z[77]= - 26*z[23] + 31*z[42];
    z[77]=z[16]*z[77];
    z[77]=10*z[40] + z[77];
    z[77]=n<T>(1,3)*z[77] - 5*z[39];
    z[82]=2*z[2];
    z[77]=z[77]*z[82];
    z[70]= - z[70] + n<T>(146,3)*z[23];
    z[70]=z[70]*z[16];
    z[70]=z[70] + n<T>(32,3)*z[40];
    z[83]= - z[6]*z[70];
    z[77]=z[77] + 9*z[72] - n<T>(29,3)*z[42] + z[83];
    z[77]=z[12]*z[77];
    z[83]=n<T>(1,3)*z[23] + z[25];
    z[83]=z[9]*z[83];
    z[83]= - z[49] + z[83];
    z[83]=z[4]*z[83];
    z[83]=20*z[83] - n<T>(61,3)*z[23] - 81*z[25];
    z[83]=z[83]*z[41];
    z[84]=z[63] - z[32];
    z[84]=z[16]*z[84];
    z[84]= - 2*z[61] + z[84];
    z[84]=z[7]*z[84];
    z[85]=z[61]*z[41];
    z[84]= - z[85] + z[84];
    z[84]=z[84]*z[36];
    z[86]=z[65]*z[4];
    z[87]=41*z[40] + 20*z[86];
    z[87]=z[87]*z[41];
    z[88]=115*z[25];
    z[87]=z[87] - 68*z[23] + z[88];
    z[89]=z[28]*z[1];
    z[90]=z[40] + z[89];
    z[90]=z[90]*z[69];
    z[91]=z[40]*z[3];
    z[87]=9*z[91] + n<T>(1,3)*z[87] + z[90];
    z[87]=z[11]*z[87];
    z[43]= - z[38] - z[43];
    z[43]=z[43]*z[69];
    z[69]=53*z[1] + 148*z[9];
    z[30]=z[30] + z[77] + z[87] + z[50] + z[55] + 9*z[84] + z[46] + 
    z[43] - n<T>(229,3)*z[16] + n<T>(1,3)*z[69] + z[83];
    z[30]=z[17]*z[30];
    z[43]=z[7]*z[61];
    z[43]= - 9*z[64] + n<T>(20,3)*z[43];
    z[46]=2*z[7];
    z[43]=z[43]*z[46];
    z[50]=n<T>(209,3)*z[23] + 53*z[25];
    z[50]=z[9]*z[50];
    z[49]=z[49] + z[80];
    z[55]=z[6]*z[9];
    z[69]=z[49]*z[55];
    z[50]=16*z[69] + 12*z[40] + z[50];
    z[50]=z[6]*z[50];
    z[69]=npow(z[9],3);
    z[77]=z[69]*z[40];
    z[83]=z[6]*z[77];
    z[84]=npow(z[6],2);
    z[87]=z[84]*z[10];
    z[90]=npow(z[25],4);
    z[92]=z[90]*z[87];
    z[83]=47*z[83] + 8*z[92];
    z[92]=npow(z[10],3);
    z[83]=z[83]*z[92];
    z[93]=9*z[4];
    z[81]= - z[81]*z[93];
    z[43]=n<T>(2,3)*z[83] + z[50] + z[43] + n<T>(101,3)*z[42] + z[81] + z[76] + 
    z[78];
    z[43]=z[2]*z[43];
    z[50]=z[27]*z[16];
    z[78]=z[50] - z[49];
    z[81]=z[28]*z[40];
    z[83]=z[40]*z[9];
    z[94]=z[83]*z[16];
    z[95]=n<T>(16,3)*z[81] + 27*z[94];
    z[95]=z[16]*z[95];
    z[95]= - n<T>(16,3)*z[77] + z[95];
    z[95]=z[95]*z[92];
    z[45]= - z[3]*z[45];
    z[96]=z[9]*z[49];
    z[97]=z[90]*npow(z[10],4);
    z[96]=z[96] + n<T>(1,3)*z[97];
    z[52]=z[96]*z[52];
    z[45]=z[52] + z[45] + 16*z[78] + z[95];
    z[45]=z[11]*z[45];
    z[52]=z[25] + z[42];
    z[52]=z[16]*z[52];
    z[95]=z[61]*z[46];
    z[52]=z[95] + z[80] + z[52];
    z[95]=z[44] + 43*z[80];
    z[95]=z[95]*z[55];
    z[96]=n<T>(16,3)*z[6];
    z[97]=z[96]*z[97];
    z[52]=z[97] + 27*z[52] + z[95];
    z[52]=z[2]*z[52];
    z[95]= - z[33]*z[46];
    z[95]=z[95] - z[60];
    z[97]= - 16*z[23] - 43*z[25];
    z[97]=z[9]*z[97];
    z[50]=70*z[50] + n<T>(65,3)*z[40] + z[97];
    z[50]=z[6]*z[50];
    z[97]=z[81]*z[16];
    z[98]=z[97] - z[77];
    z[98]=n<T>(16,3)*z[98];
    z[99]=z[10]*z[6];
    z[100]=z[99]*z[98];
    z[101]=z[33]*z[16];
    z[100]=81*z[101] + z[100];
    z[100]=z[100]*z[36];
    z[102]=z[56]*z[96];
    z[102]=81*z[32] + z[102];
    z[102]=z[3]*z[102];
    z[45]=z[45] + z[52] + z[102] + z[100] + 54*z[95] + z[50];
    z[45]=z[18]*z[45];
    z[50]=9*z[32];
    z[52]= - n<T>(43,3)*z[33] - z[50];
    z[52]=z[16]*z[52];
    z[52]= - 26*z[61] + z[52];
    z[52]=z[52]*z[36];
    z[95]=74*z[23] + 103*z[25];
    z[100]=n<T>(1,3)*z[9];
    z[95]=z[95]*z[100];
    z[102]=z[92]*z[77];
    z[95]=n<T>(47,3)*z[102] + 6*z[40] + z[95];
    z[95]=z[95]*z[82];
    z[102]= - 50*z[23] - 79*z[25];
    z[70]= - z[3]*z[70];
    z[52]=z[95] + z[70] + z[52] + n<T>(2,3)*z[102] + 27*z[42];
    z[52]=z[11]*z[52];
    z[70]=z[6]*z[78];
    z[78]=28*z[23];
    z[95]=18*z[42];
    z[70]=8*z[70] + z[95] - z[78] - n<T>(5,3)*z[25];
    z[102]=2*z[6];
    z[70]=z[70]*z[102];
    z[103]= - z[41]*z[33];
    z[104]= - z[4]*z[32];
    z[103]=z[103] + z[104];
    z[103]=z[16]*z[103];
    z[85]= - z[85] + z[103];
    z[98]=z[87]*z[98];
    z[103]=13*z[61];
    z[104]= - z[103] + 3*z[101];
    z[102]=z[104]*z[102];
    z[85]=z[98] + 9*z[85] + z[102];
    z[85]=z[85]*z[36];
    z[98]=n<T>(4,3)*z[6];
    z[102]=z[56]*z[98];
    z[102]=z[102] - n<T>(8,3)*z[40] + z[73];
    z[102]=z[102]*z[75];
    z[104]=z[42]*z[4];
    z[105]=z[23]*z[4];
    z[106]=z[1] - z[105];
    z[106]=2*z[106] + z[104];
    z[106]=z[16]*z[106];
    z[107]=z[40]*z[4];
    z[102]=z[102] + 9*z[106] + 101*z[23] + 18*z[107];
    z[102]=z[3]*z[102];
    z[106]=z[33]*z[7];
    z[108]= - 40*z[106] + 20*z[23] + 61*z[25];
    z[109]=n<T>(2,3)*z[7];
    z[108]=z[108]*z[109];
    z[110]=z[60]*z[4];
    z[111]=z[4]*z[1];
    z[112]=3*z[111];
    z[113]=static_cast<T>(1)- z[112];
    z[113]=z[16]*z[113];
    z[43]=z[45] + z[52] + z[43] + z[102] + z[85] + z[70] + z[108] + 18*
    z[113] - 61*z[9] - 9*z[110];
    z[43]=z[18]*z[43];
    z[45]= - z[16]*z[34];
    z[52]= - z[77]*z[99];
    z[45]=z[52] - z[61] + z[45];
    z[45]=z[45]*z[36];
    z[52]=2*z[83];
    z[70]= - z[65] - z[52];
    z[70]=z[4]*z[70];
    z[85]=4*z[33];
    z[70]=z[70] - z[40] - z[85];
    z[70]=z[70]*z[41];
    z[102]=z[24] + z[107];
    z[108]=4*z[4];
    z[102]=z[102]*z[108];
    z[102]= - z[1] + z[102];
    z[102]=z[16]*z[102];
    z[45]=z[45] + z[102] + z[70] - z[23] - 4*z[25];
    z[70]= - z[40] - n<T>(2,3)*z[33];
    z[70]=z[9]*z[70];
    z[102]=2*z[65];
    z[113]=z[102] + z[83];
    z[113]=z[113]*z[9];
    z[114]=npow(z[1],5);
    z[113]=z[113] + z[114];
    z[115]=n<T>(1,3)*z[4];
    z[116]= - z[113]*z[115];
    z[70]=z[116] - n<T>(1,3)*z[65] + z[70];
    z[70]=z[4]*z[70];
    z[116]= - 38*z[40] - 235*z[80];
    z[70]=n<T>(1,3)*z[116] + 94*z[70];
    z[70]=z[6]*z[70];
    z[45]=z[70] + n<T>(47,3)*z[45];
    z[70]=z[107]*z[28];
    z[63]=z[40] - z[63];
    z[63]=z[9]*z[63];
    z[63]=z[63] - z[70];
    z[63]=z[4]*z[63];
    z[116]= - z[58] - z[24] + z[25];
    z[117]=n<T>(47,9)*z[16];
    z[116]=z[116]*z[117];
    z[118]=z[92]*z[54];
    z[119]=n<T>(47,9)*z[118];
    z[120]= - z[83] - z[56];
    z[120]=z[120]*z[119];
    z[121]=n<T>(188,9)*z[33];
    z[63]=z[120] + z[116] + n<T>(94,9)*z[63] + z[40] + z[121];
    z[63]=z[12]*z[63];
    z[116]=z[24] - z[107];
    z[116]=z[116]*z[4];
    z[116]=z[116] - z[1];
    z[116]=z[116]*z[35];
    z[120]=z[41]*z[40];
    z[122]=z[116] - z[120] - z[60];
    z[122]=z[122]*z[117];
    z[119]=z[83]*z[119];
    z[119]=z[119] + z[40] + z[122];
    z[119]=z[11]*z[119];
    z[120]=z[120] - z[23];
    z[116]=z[116] + z[120];
    z[116]=z[116]*z[117];
    z[116]=z[40] + z[116];
    z[116]=z[3]*z[116];
    z[45]=z[63] + z[119] + n<T>(1,3)*z[45] + z[116];
    z[45]=z[14]*z[45];
    z[63]=14*z[107];
    z[116]=z[63] - 25*z[23] + 7*z[25];
    z[117]=4*z[23];
    z[119]= - z[117] + n<T>(47,9)*z[107];
    z[119]=z[4]*z[119];
    z[122]= - z[71] + 3*z[107];
    z[123]= - z[122]*z[41];
    z[123]= - 11*z[1] + z[123];
    z[124]=z[16]*z[4];
    z[123]=z[123]*z[124];
    z[119]=z[123] + 3*z[1] + z[119];
    z[119]=z[119]*z[35];
    z[123]= - n<T>(7,9)*z[33] + 6*z[32];
    z[125]=z[36]*z[16];
    z[123]=z[123]*z[125];
    z[116]=z[123] + n<T>(1,9)*z[116] + z[119];
    z[116]=z[11]*z[116];
    z[119]=62*z[33];
    z[123]=127*z[40] - z[119];
    z[123]=z[9]*z[123];
    z[123]=z[123] - 134*z[70];
    z[126]=n<T>(1,9)*z[4];
    z[123]=z[123]*z[126];
    z[127]=n<T>(8,9)*z[23] + z[25];
    z[127]=z[9]*z[127];
    z[123]=11*z[127] + z[123];
    z[123]=z[123]*z[41];
    z[78]= - z[78] - n<T>(205,3)*z[25];
    z[127]=7*z[33];
    z[128]=z[127] + 155*z[32];
    z[128]=z[128]*z[125];
    z[78]=n<T>(1,9)*z[128] + n<T>(158,9)*z[42] + n<T>(1,3)*z[78] + z[123];
    z[78]=z[12]*z[78];
    z[123]=79*z[23] + 188*z[25];
    z[128]=3*z[65];
    z[129]= - z[128] - n<T>(94,9)*z[83];
    z[129]=z[4]*z[129];
    z[129]=n<T>(17,3)*z[40] + z[129];
    z[129]=z[129]*z[41];
    z[123]=n<T>(1,9)*z[123] + z[129];
    z[123]=z[123]*z[41];
    z[119]=z[119] + 265*z[40];
    z[119]=z[119]*z[9];
    z[119]=z[119] + 203*z[65];
    z[113]=z[113]*z[4];
    z[119]= - 6*z[113] + n<T>(1,9)*z[119];
    z[129]=z[4]*z[119];
    z[130]=110*z[23] + n<T>(89,3)*z[25];
    z[130]=z[9]*z[130];
    z[130]=n<T>(154,3)*z[40] + z[130];
    z[129]=n<T>(1,3)*z[130] + z[129];
    z[129]=z[129]*z[41];
    z[130]=23*z[23] + n<T>(200,3)*z[25];
    z[129]=n<T>(1,3)*z[130] + z[129];
    z[129]=z[6]*z[129];
    z[130]=z[16] + z[9];
    z[130]=z[130]*z[16];
    z[131]=z[69]*z[6];
    z[130]=z[130] - z[131] - z[28];
    z[131]=z[61]*z[99];
    z[131]=94*z[130] - 155*z[131];
    z[132]=n<T>(1,9)*z[10];
    z[131]=z[131]*z[132];
    z[133]= - n<T>(224,3)*z[23] + 47*z[107];
    z[133]=z[4]*z[133];
    z[134]=125*z[23] - 67*z[107];
    z[134]=z[134]*z[41];
    z[134]= - 89*z[1] + z[134];
    z[134]=z[16]*z[134]*z[115];
    z[133]=z[134] + n<T>(74,3)*z[1] + z[133];
    z[133]=z[133]*z[35];
    z[63]= - 11*z[23] - z[63];
    z[63]=n<T>(1,3)*z[63] + z[133];
    z[63]=n<T>(1,3)*z[63] - 6*z[72];
    z[63]=z[3]*z[63];
    z[133]=npow(z[4],2);
    z[134]=z[40]*z[133];
    z[134]= - z[1] + z[134];
    z[134]=z[4]*z[134];
    z[134]=static_cast<T>(155)+ 376*z[134];
    z[134]=z[16]*z[134];
    z[134]=z[134] + 151*z[1] - 155*z[9];
    z[45]=2*z[45] + z[78] + z[116] + z[63] + z[131] + z[129] + z[123] + 
   n<T>(1,9)*z[134];
    z[63]=4*z[14];
    z[45]=z[45]*z[63];
    z[78]=z[80] - z[40];
    z[78]=z[78]*z[4];
    z[80]=z[65]*z[3];
    z[116]= - z[86] - z[80];
    z[116]=z[11]*z[116];
    z[116]=z[116] + z[91] + z[106] + z[25] - z[78];
    z[123]=z[29] - z[40];
    z[129]= - 85*z[23] + 76*z[25];
    z[129]=z[9]*z[129];
    z[123]=z[129] - 76*z[123];
    z[123]=z[6]*z[123];
    z[129]=z[114]*z[3];
    z[129]=z[129] + z[65];
    z[129]=z[129]*z[11];
    z[131]=z[65]*z[6];
    z[129]=z[129] - z[131] - z[80];
    z[134]=76*z[8];
    z[135]=z[129]*z[134];
    z[136]= - 76*z[65] + 85*z[83];
    z[136]=z[6]*z[136];
    z[137]=z[114]*z[6];
    z[137]=z[137] + z[65];
    z[134]=z[137]*z[134];
    z[138]=z[83]*z[7];
    z[134]=z[134] - 76*z[138] + z[136];
    z[134]=z[12]*z[134];
    z[116]=z[134] + z[135] + z[123] + 76*z[116];
    z[123]=n<T>(1,3)*z[13];
    z[116]=z[116]*z[123];
    z[134]= - z[12]*z[137];
    z[129]=z[134] - z[129];
    z[129]=z[14]*z[129];
    z[134]=z[40]*z[6];
    z[135]=z[91] + z[134];
    z[136]=z[3]*z[102];
    z[136]=z[40] + z[136];
    z[136]=z[11]*z[136];
    z[137]=z[6]*z[102];
    z[137]=z[40] + z[137];
    z[137]=z[12]*z[137];
    z[129]=z[129] + z[137] - 2*z[135] + z[136];
    z[63]=z[129]*z[63];
    z[78]=n<T>(10,3)*z[78] - n<T>(53,3)*z[23] - 16*z[25];
    z[78]=z[4]*z[78];
    z[129]= - n<T>(10,3)*z[106] + z[23] + n<T>(20,3)*z[25];
    z[129]=z[7]*z[129];
    z[78]=z[129] + n<T>(46,3)*z[1] + z[78];
    z[129]=n<T>(10,3)*z[138] - z[40] + 6*z[33];
    z[129]=z[129]*z[46];
    z[136]=137*z[40] - 43*z[33];
    z[136]=z[136]*z[53];
    z[137]=70*z[40];
    z[131]= - z[137] - 73*z[131];
    z[139]=n<T>(2,3)*z[8];
    z[131]=z[131]*z[139];
    z[129]=z[131] + z[129] + z[136];
    z[129]=z[12]*z[129];
    z[131]= - 73*z[23] - 86*z[25];
    z[131]=z[9]*z[131];
    z[29]=z[131] + 10*z[29];
    z[29]=z[29]*z[41];
    z[29]=z[29] - 67*z[23] + 110*z[25];
    z[29]=z[29]*z[53];
    z[131]=43*z[40] + 10*z[86];
    z[131]=z[4]*z[131];
    z[131]=26*z[91] + 8*z[23] + z[131];
    z[131]=z[11]*z[131];
    z[80]= - z[137] - 73*z[80];
    z[80]=z[11]*z[80];
    z[80]=38*z[135] + z[80];
    z[80]=z[80]*z[139];
    z[135]=z[23]*z[3];
    z[29]=z[116] + z[63] + z[129] + z[80] + n<T>(2,3)*z[131] + n<T>(62,3)*z[135]
    + 2*z[78] + z[29];
    z[63]=2*z[13];
    z[29]=z[29]*z[63];
    z[78]=npow(z[7],2);
    z[80]=z[10]*z[78]*z[83];
    z[116]=z[23]*z[7];
    z[129]=71*z[116] + 10*z[80];
    z[129]=z[129]*z[36];
    z[131]=z[78]*z[1];
    z[129]=40*z[131] + z[129];
    z[136]=101*z[1];
    z[137]=z[136] + 20*z[116];
    z[139]=z[3]*z[7];
    z[137]=z[137]*z[139];
    z[129]=2*z[129] + z[137];
    z[137]=z[10]*z[138];
    z[137]= - z[23] + z[137];
    z[137]=z[137]*z[36];
    z[140]=z[116] + z[1];
    z[140]=z[140]*z[3];
    z[141]=4*z[7];
    z[142]=z[1]*z[141];
    z[137]=z[140] + z[142] + z[137];
    z[142]=27*z[18];
    z[137]=z[137]*z[142];
    z[129]=n<T>(1,3)*z[129] + z[137];
    z[129]=z[18]*z[129];
    z[137]= - z[142]*z[7];
    z[137]= - n<T>(20,3)*z[78] + z[137];
    z[92]=z[92]*z[40];
    z[143]=z[3]*z[1];
    z[144]=z[92] + z[143];
    z[137]=z[18]*z[144]*z[137];
    z[144]=19*z[13];
    z[145]=z[7]*z[144];
    z[145]= - 5*z[78] + z[145];
    z[146]=n<T>(4,3)*z[13];
    z[145]=z[146]*z[143]*z[145];
    z[147]=npow(z[7],3);
    z[92]=z[147]*z[92];
    z[92]=z[145] + n<T>(632,9)*z[92] + z[137];
    z[92]=z[20]*z[92];
    z[137]=z[23]*z[78];
    z[145]= - z[83] + 2*z[56];
    z[148]=z[147]*z[10];
    z[145]=z[145]*z[148];
    z[137]= - 557*z[137] + 632*z[145];
    z[137]=z[137]*z[36];
    z[145]=z[1]*z[147];
    z[137]= - 1264*z[145] + z[137];
    z[145]=z[66]*z[139];
    z[137]=n<T>(1,3)*z[137] + z[145];
    z[145]=29*z[1] + 5*z[116];
    z[139]=z[145]*z[139];
    z[145]= - z[1]*z[46];
    z[140]=z[145] - z[140];
    z[140]=z[140]*z[144];
    z[131]=z[140] + 10*z[131] + z[139];
    z[131]=z[131]*z[146];
    z[92]=z[92] + z[131] + n<T>(1,3)*z[137] + z[129];
    z[129]=2*z[20];
    z[92]=z[92]*z[129];
    z[131]= - z[27]*z[46];
    z[137]=z[85] - z[32];
    z[137]=z[137]*z[36];
    z[117]= - z[117] - z[42];
    z[117]=z[3]*z[117];
    z[117]=z[117] + z[131] + z[137];
    z[117]=z[117]*z[142];
    z[109]= - z[27]*z[109];
    z[109]= - 9*z[1] + z[109];
    z[109]=z[7]*z[109];
    z[131]=z[36]*z[106];
    z[137]=n<T>(1,3)*z[3];
    z[139]= - 209*z[1] - 134*z[116];
    z[139]=z[139]*z[137];
    z[109]=z[117] + z[139] - n<T>(298,3)*z[131] + n<T>(169,3) + 20*z[109];
    z[109]=z[18]*z[109];
    z[117]=z[28]*z[6];
    z[131]=z[16] - z[9];
    z[117]=z[117] - z[131];
    z[139]=z[81]*z[10];
    z[84]=z[84]*z[139];
    z[140]=z[6]*z[33];
    z[84]=n<T>(29,3)*z[140] + 4*z[84];
    z[84]=z[10]*z[84];
    z[84]= - n<T>(94,3)*z[117] + z[84];
    z[140]=2*z[10];
    z[84]=z[84]*z[140];
    z[144]=5*z[23];
    z[145]=z[37]*z[144];
    z[146]=94*z[54];
    z[145]=z[146] + z[145];
    z[145]=z[145]*z[140];
    z[149]=23*z[1];
    z[145]= - z[149] + z[145];
    z[150]=z[118]*z[40];
    z[151]=z[42] - z[150];
    z[151]=z[3]*z[151];
    z[145]=n<T>(1,3)*z[145] + 8*z[151];
    z[145]=z[3]*z[145];
    z[151]=8*z[25];
    z[152]=z[6]*z[151];
    z[152]=n<T>(47,3)*z[1] + z[152];
    z[152]=z[6]*z[152];
    z[84]=z[145] + z[84] + n<T>(34,3) + z[152];
    z[84]=z[2]*z[84];
    z[99]=z[99]*z[127];
    z[99]=94*z[117] + z[99];
    z[99]=z[10]*z[99];
    z[117]=z[6]*z[139];
    z[34]=z[117] - z[34];
    z[34]=z[34]*z[36];
    z[117]= - z[42] - z[150];
    z[117]=z[3]*z[117];
    z[127]=z[6]*z[25];
    z[34]=z[117] + z[127] + z[34];
    z[34]=z[14]*z[34];
    z[71]=z[37]*z[71];
    z[71]= - z[146] + z[71];
    z[71]=z[10]*z[71];
    z[71]=7*z[1] + z[71];
    z[71]=z[3]*z[71];
    z[34]=94*z[34] + z[71] + static_cast<T>(7)+ z[99];
    z[34]=z[14]*z[34];
    z[71]=z[33]*z[36];
    z[99]= - z[71] + z[1];
    z[117]=3*z[6];
    z[99]=z[117]*z[99];
    z[127]=z[25]*z[7];
    z[145]= - 13*z[1] - n<T>(5,3)*z[127];
    z[145]=z[145]*z[141];
    z[127]=z[135] + z[1] + z[127];
    z[146]= - z[6]*z[38];
    z[127]=z[146] + n<T>(76,3)*z[127];
    z[127]=z[13]*z[127];
    z[116]= - 19*z[1] - n<T>(23,3)*z[116];
    z[116]=z[3]*z[116];
    z[99]=z[127] + 2*z[116] + z[145] + z[99];
    z[99]=z[99]*z[63];
    z[116]= - z[83] + n<T>(1,3)*z[56];
    z[37]=z[116]*z[147]*z[37];
    z[116]=19*z[33] - 743*z[32];
    z[116]=z[116]*z[78];
    z[37]=n<T>(1,3)*z[116] + 632*z[37];
    z[116]=2*z[36];
    z[37]=z[37]*z[116];
    z[47]=z[7]*z[47];
    z[47]=41*z[1] - n<T>(79,3)*z[47];
    z[47]=z[7]*z[47];
    z[47]= - static_cast<T>(257)+ 16*z[47];
    z[47]=z[7]*z[47];
    z[37]=z[47] + z[37];
    z[47]=z[32]*z[36];
    z[127]= - n<T>(8,3)*z[47] - n<T>(169,3)*z[1];
    z[127]=z[7]*z[127];
    z[145]= - z[1] + 2*z[47];
    z[146]=n<T>(16,3)*z[3];
    z[145]=z[145]*z[146];
    z[127]=z[145] - static_cast<T>(12)+ z[127];
    z[127]=z[3]*z[127];
    z[34]=z[92] + z[99] + n<T>(4,9)*z[34] + z[109] + n<T>(2,3)*z[84] + n<T>(1,3)*
    z[37] + z[127];
    z[34]=z[20]*z[34];
    z[37]=z[59] + z[23];
    z[84]=z[33] + z[40];
    z[92]= - z[12]*z[84];
    z[99]= - z[12]*z[83];
    z[99]=z[33] + z[99];
    z[99]=z[13]*z[99];
    z[92]=z[99] + z[92] + z[37];
    z[92]=z[13]*z[92];
    z[99]=3*z[33] - z[32];
    z[99]=z[7]*z[16]*z[99];
    z[80]=z[54]*z[80];
    z[80]=z[99] + z[80];
    z[80]=z[80]*z[36];
    z[99]=2*z[1];
    z[109]=z[13]*z[25];
    z[109]=z[109] - z[99] + z[71];
    z[109]=z[13]*z[109];
    z[109]=z[109] - static_cast<T>(1)+ 3*z[143];
    z[109]=z[20]*z[109];
    z[127]=3*z[42];
    z[145]=z[25] + z[127];
    z[145]=z[7]*z[145];
    z[152]=3*z[9];
    z[153]=z[3]*z[127];
    z[154]= - z[12]*z[106];
    z[80]=z[109] + z[92] + z[154] + z[153] + z[80] + z[145] - z[35] + 
    z[1] + z[152];
    z[80]=z[21]*z[80];
    z[92]=z[40]*z[11];
    z[24]=z[24] - z[92];
    z[24]=z[13]*z[24];
    z[109]= - z[13]*z[1];
    z[109]= - z[66] + z[109];
    z[109]=z[20]*z[109];
    z[145]= - z[11]*z[23];
    z[24]=z[109] + z[24] + z[99] + z[145];
    z[24]=z[13]*z[24];
    z[109]=z[139]*z[133];
    z[145]=z[33]*z[4];
    z[153]= - 3*z[145] + z[109];
    z[153]=z[153]*z[36];
    z[154]= - z[11]*z[105];
    z[24]=z[154] + z[153] + static_cast<T>(2)+ z[111] + z[24];
    z[24]=z[15]*z[24];
    z[153]= - z[36]*z[62];
    z[154]= - z[23] - z[92];
    z[154]=z[13]*z[154];
    z[117]=z[63] - z[117];
    z[117]=z[1]*z[117];
    z[117]=static_cast<T>(1)+ z[117];
    z[117]=z[20]*z[117];
    z[155]= - z[4]*z[38];
    z[156]=z[6]*z[31];
    z[24]=z[24] + z[117] + z[154] + z[153] + z[156] + z[155] + z[99] + 
    z[9];
    z[24]=z[19]*z[24];
    z[24]=z[24] + z[80];
    z[80]=z[83]*z[46];
    z[99]=z[83] + z[65];
    z[117]= - z[6]*z[99];
    z[48]=z[117] + z[80] - z[48] - z[84];
    z[48]=z[48]*z[142];
    z[26]=z[67] + z[26];
    z[26]=z[26]*z[93];
    z[80]= - z[40] + z[33];
    z[80]=9*z[80] + n<T>(20,3)*z[138];
    z[80]=z[80]*z[46];
    z[84]=22*z[40] - 5*z[33];
    z[84]=z[84]*z[98];
    z[93]=n<T>(7,3)*z[23];
    z[26]=z[48] + z[84] + z[80] - z[79] + z[26] - z[93] - z[74];
    z[26]=z[18]*z[26];
    z[48]=901*z[90];
    z[74]= - z[133]*z[48];
    z[79]=npow(z[25],5);
    z[79]=316*z[79];
    z[80]=npow(z[4],3);
    z[84]=z[80]*z[10];
    z[98]=z[84]*z[79];
    z[74]=z[74] + z[98];
    z[74]=z[74]*z[140];
    z[98]=z[69]*z[107];
    z[74]=1871*z[98] + z[74];
    z[74]=z[74]*z[132];
    z[98]=n<T>(701,9)*z[61];
    z[117]=7*z[32];
    z[138]=z[121] + z[117];
    z[138]=z[16]*z[138];
    z[74]=z[74] - z[98] + z[138];
    z[74]=z[74]*z[36];
    z[138]=316*z[4];
    z[49]=z[49]*z[28]*z[138];
    z[142]=2083*z[25];
    z[153]= - 1463*z[23] - z[142];
    z[153]=z[9]*z[153];
    z[153]= - 281*z[40] + z[153];
    z[153]=z[153]*z[100];
    z[49]=z[153] + z[49];
    z[49]=z[49]*z[115];
    z[153]=229*z[23] + 1295*z[25];
    z[153]=z[9]*z[153];
    z[49]=z[49] - 19*z[40] + n<T>(1,9)*z[153];
    z[49]=z[49]*z[41];
    z[88]=257*z[23] - z[88];
    z[153]=11*z[42];
    z[154]= - z[153] + n<T>(35,3)*z[23];
    z[154]=z[154]*z[16];
    z[154]=z[154] + n<T>(41,3)*z[40];
    z[155]=z[6]*z[154];
    z[49]=z[74] + z[155] - n<T>(127,3)*z[42] + n<T>(1,3)*z[88] + z[49];
    z[74]=2*z[8];
    z[49]=z[49]*z[74];
    z[88]=n<T>(2,3)*z[23];
    z[38]=n<T>(5,3)*z[42] + z[88] + z[38];
    z[155]= - z[31] + n<T>(10,3)*z[25];
    z[155]=z[7]*z[9]*z[155];
    z[156]= - 8*z[33] - 23*z[32];
    z[125]=z[156]*z[125];
    z[38]=n<T>(1,3)*z[125] + 2*z[38] + z[155];
    z[38]=z[38]*z[82];
    z[67]=z[67] - z[33];
    z[67]=z[67]*z[152];
    z[67]=z[67] - n<T>(32,3)*z[70];
    z[67]=z[67]*z[41];
    z[70]=24*z[23] + n<T>(19,3)*z[25];
    z[70]=z[9]*z[70];
    z[67]=z[67] + 58*z[40] + z[70];
    z[67]=z[4]*z[67];
    z[59]= - z[76] - z[59];
    z[59]=z[95] + 4*z[59] + z[67];
    z[59]=z[7]*z[59];
    z[67]=5*z[25];
    z[70]= - 12*z[42] - n<T>(163,3)*z[23] - z[67];
    z[70]=z[6]*z[70];
    z[95]=65*z[77];
    z[125]=z[95]*z[133];
    z[152]=z[90]*z[84];
    z[152]=z[125] - n<T>(316,9)*z[152];
    z[152]=z[10]*z[152];
    z[152]= - n<T>(257,3)*z[62] + 4*z[152];
    z[152]=z[152]*z[36];
    z[155]=z[69]*z[111];
    z[155]=2479*z[89] - 1264*z[155];
    z[155]=z[155]*z[51];
    z[155]= - 833*z[25] + z[155];
    z[155]=z[4]*z[155];
    z[155]=19*z[16] + z[155] + z[1] - n<T>(305,3)*z[9];
    z[26]=z[49] + z[26] + z[38] + z[152] + z[70] + n<T>(1,3)*z[155] + z[59];
    z[26]=z[12]*z[26];
    z[38]=18*z[106] - z[127] - z[76] + n<T>(305,9)*z[25];
    z[38]=z[7]*z[38];
    z[49]=z[6]*z[61];
    z[59]=z[77]*z[87];
    z[49]=n<T>(157,3)*z[49] - 16*z[59];
    z[49]=z[10]*z[49];
    z[49]= - n<T>(188,3)*z[130] + z[49];
    z[49]=z[10]*z[49];
    z[59]=z[65]*z[138];
    z[70]=667*z[40];
    z[59]= - z[70] + z[59];
    z[59]=z[59]*z[108];
    z[87]=2131*z[23];
    z[59]=z[87] + z[59];
    z[59]=z[4]*z[59];
    z[106]= - 131*z[23] + 79*z[107];
    z[127]=16*z[4];
    z[106]=z[106]*z[127];
    z[106]=631*z[1] + z[106];
    z[106]=z[106]*z[124];
    z[59]=z[106] - 826*z[1] + z[59];
    z[59]=z[16]*z[59];
    z[106]=z[138]*z[114];
    z[130]=667*z[65];
    z[106]=z[106] - z[130];
    z[106]=z[106]*z[41];
    z[152]=935*z[40] + z[106];
    z[152]=z[4]*z[152];
    z[152]= - 290*z[23] + z[152];
    z[59]=2*z[152] + z[59];
    z[58]=z[23] - z[58];
    z[58]=z[16]*z[58];
    z[39]=z[58] + z[39];
    z[39]=z[39]*z[57];
    z[39]=z[39] + n<T>(1,3)*z[59] + 82*z[72];
    z[39]=z[39]*z[137];
    z[57]=z[99]*z[4];
    z[58]=632*z[57] - n<T>(2762,3)*z[40] - 479*z[33];
    z[58]=z[4]*z[58];
    z[59]=1186*z[23] - 59*z[25];
    z[58]=n<T>(1,3)*z[59] + z[58];
    z[58]=z[4]*z[58];
    z[58]=z[58] + 86*z[1] + n<T>(205,3)*z[9];
    z[59]= - 259*z[23] + n<T>(632,3)*z[107];
    z[59]=z[4]*z[59];
    z[59]=n<T>(133,3)*z[1] + z[59];
    z[59]=z[4]*z[59];
    z[59]=n<T>(295,9) + z[59];
    z[59]=z[16]*z[59];
    z[58]=n<T>(1,3)*z[58] + z[59];
    z[59]=z[83] - z[65];
    z[59]=z[59]*z[9];
    z[59]=z[59] + z[114];
    z[99]=316*z[7];
    z[59]=z[59]*z[99];
    z[59]=z[59] - z[130];
    z[152]=z[70] - 524*z[33];
    z[152]=z[9]*z[152];
    z[152]=z[152] + z[59];
    z[141]=z[152]*z[141];
    z[87]= - z[87] + 631*z[25];
    z[87]=z[9]*z[87];
    z[87]=z[141] + 1870*z[40] + z[87];
    z[87]=z[7]*z[87];
    z[141]=94*z[25];
    z[152]= - 193*z[23] + z[141];
    z[87]=4*z[152] + z[87];
    z[55]=z[37]*z[55];
    z[55]=n<T>(1,3)*z[87] - 32*z[55];
    z[53]=z[55]*z[53];
    z[38]=z[39] + n<T>(2,3)*z[49] + z[53] + 2*z[58] + z[38];
    z[38]=z[2]*z[38];
    z[39]=41*z[23] - n<T>(544,3)*z[25];
    z[39]=z[9]*z[39];
    z[49]=z[88] + z[25];
    z[49]=z[9]*z[49];
    z[53]=n<T>(2,3)*z[40];
    z[49]= - z[53] + z[49];
    z[49]=z[9]*z[49];
    z[49]= - z[65] + z[49];
    z[49]=z[4]*z[49];
    z[39]=158*z[49] + 236*z[40] + z[39];
    z[39]=z[39]*z[41];
    z[39]=z[39] - 221*z[23] + 118*z[25];
    z[39]=z[4]*z[39];
    z[49]= - z[23] - n<T>(2,3)*z[25];
    z[49]=z[9]*z[49];
    z[49]= - z[53] + z[49];
    z[49]=z[49]*z[41];
    z[27]=z[49] + z[27];
    z[27]=z[27]*z[138];
    z[27]= - n<T>(235,3)*z[1] + z[27];
    z[27]=z[27]*z[124];
    z[27]=z[27] - n<T>(50,3)*z[1] + z[39];
    z[39]=z[64] + z[40];
    z[49]=z[16]*z[60];
    z[53]= - z[39]*z[131];
    z[53]= - z[65] + z[53];
    z[53]=z[53]*z[99];
    z[39]=z[53] + 667*z[39] - 316*z[49];
    z[39]=z[39]*z[46];
    z[49]= - 209*z[23] + 254*z[25];
    z[39]=z[39] + 5*z[49] - 307*z[42];
    z[49]=n<T>(1,3)*z[7];
    z[39]=z[39]*z[49];
    z[27]=2*z[27] + z[39];
    z[39]=n<T>(316,9)*z[97];
    z[53]=z[95] + z[39];
    z[53]=z[53]*z[78];
    z[55]=z[69]*z[65];
    z[58]=z[55]*z[16];
    z[64]=z[90] - z[58];
    z[64]=z[80]*z[64];
    z[58]=z[90] + z[58];
    z[58]=z[58]*z[147];
    z[58]=z[58] + z[64];
    z[58]=z[10]*z[58];
    z[39]=z[133]*z[39];
    z[39]=n<T>(316,9)*z[58] + z[53] - z[125] + z[39];
    z[39]=z[39]*z[140];
    z[53]=n<T>(163,9)*z[33] + z[117];
    z[53]=z[16]*z[53];
    z[53]=n<T>(989,9)*z[61] + z[53];
    z[53]=z[7]*z[53];
    z[58]=z[4]*z[117];
    z[58]= - n<T>(163,9)*z[145] + z[58];
    z[58]=z[16]*z[58];
    z[39]=z[39] + z[53] + n<T>(989,9)*z[62] + z[58];
    z[39]=z[39]*z[36];
    z[48]= - z[78]*z[48];
    z[53]= - z[148]*z[79];
    z[48]=z[48] + z[53];
    z[48]=z[48]*z[140];
    z[53]=z[7]*z[77];
    z[48]= - 1871*z[53] + z[48];
    z[48]=z[48]*z[132];
    z[53]= - z[121] + z[117];
    z[53]=z[16]*z[53];
    z[48]=z[48] - z[98] + z[53];
    z[48]=z[48]*z[36];
    z[53]= - 569*z[42] + 415*z[23] - 1057*z[25];
    z[58]= - n<T>(620,9)*z[23] - 169*z[25];
    z[58]=z[58]*z[28];
    z[62]=z[7]*z[37]*z[69];
    z[58]=z[58] - n<T>(316,9)*z[62];
    z[58]=z[58]*z[46];
    z[62]= - 99*z[23] - n<T>(3023,9)*z[25];
    z[62]=z[9]*z[62];
    z[58]=z[58] - 9*z[40] + z[62];
    z[58]=z[7]*z[58];
    z[62]=z[3]*z[154];
    z[48]=z[62] + z[48] + n<T>(1,9)*z[53] + z[58];
    z[48]=z[11]*z[48];
    z[53]=z[65]*z[16];
    z[58]= - z[114] - z[53];
    z[58]=z[58]*z[99];
    z[56]=z[58] + z[130] + 316*z[56];
    z[58]=n<T>(2,9)*z[7];
    z[56]=z[56]*z[58];
    z[62]= - n<T>(145,9)*z[23] - z[153];
    z[62]=z[16]*z[62];
    z[56]=z[56] - n<T>(1207,9)*z[40] + z[62];
    z[56]=z[7]*z[56];
    z[56]=n<T>(313,9)*z[23] + z[56];
    z[56]=z[6]*z[56];
    z[62]= - 1207*z[40] - z[106];
    z[62]=z[62]*z[126];
    z[64]=z[40] - z[86];
    z[64]=z[4]*z[64];
    z[64]= - 145*z[23] + 632*z[64];
    z[64]=z[64]*z[126];
    z[64]=z[64] - 11*z[104];
    z[64]=z[16]*z[64];
    z[62]=z[64] + 32*z[23] + z[62];
    z[62]=z[3]*z[62];
    z[27]=z[48] + z[62] + z[39] + n<T>(1,3)*z[27] + z[56];
    z[27]=z[27]*z[74];
    z[39]=z[41]*z[139];
    z[39]=z[39] - z[85] + z[73];
    z[39]=z[39]*z[36];
    z[48]=z[110] - z[1];
    z[39]=2*z[48] + z[39];
    z[56]=z[42] + z[23];
    z[44]=z[118]*z[44];
    z[44]=27*z[56] + z[44];
    z[44]=z[3]*z[44];
    z[62]= - z[91] + z[120];
    z[62]=z[62]*z[68];
    z[64]=z[12]*z[150];
    z[39]=n<T>(97,3)*z[64] + z[62] + 27*z[39] + z[44];
    z[39]=z[17]*z[39];
    z[44]=z[91]*z[118];
    z[44]=16*z[44] + z[136] - 44*z[47];
    z[44]=z[44]*z[137];
    z[62]= - 47*z[1] + 20*z[110];
    z[62]=z[62]*z[51];
    z[64]= - 13*z[145] + 20*z[109];
    z[64]=z[64]*z[36];
    z[68]=z[76] + n<T>(20,3)*z[107];
    z[68]=z[4]*z[68];
    z[68]=z[68] - n<T>(10,3)*z[135];
    z[68]=z[11]*z[68];
    z[73]=z[12]*z[47];
    z[39]=z[39] - 35*z[73] + 2*z[68] + z[44] + n<T>(2,3)*z[64] + static_cast<T>(29)+ z[62];
    z[39]=z[17]*z[39];
    z[44]= - 38*z[48] - 85*z[135];
    z[48]= - 76*z[107] + 85*z[91];
    z[48]=z[11]*z[48];
    z[44]=2*z[44] + z[48];
    z[44]=z[44]*z[123];
    z[48]= - 14*z[1] + 5*z[110];
    z[48]=z[48]*z[41];
    z[48]=z[48] + 17*z[143];
    z[31]=z[31] + n<T>(5,3)*z[107];
    z[31]=z[31]*z[108];
    z[31]=z[31] - n<T>(43,3)*z[135];
    z[31]=z[11]*z[31];
    z[31]=z[44] + n<T>(2,3)*z[48] + z[31];
    z[31]=z[31]*z[63];
    z[44]=z[133]*z[71];
    z[47]=z[1] + z[47];
    z[47]=z[3]*z[47];
    z[47]= - static_cast<T>(5)+ 4*z[47];
    z[47]=z[3]*z[47];
    z[44]=n<T>(2,3)*z[47] - 13*z[4] + 9*z[44];
    z[47]=z[133]*z[81];
    z[48]=z[55]*z[84];
    z[47]= - 1649*z[47] + 632*z[48];
    z[47]=z[47]*z[140];
    z[47]=2459*z[145] + z[47];
    z[47]=z[47]*z[36];
    z[47]= - static_cast<T>(425)+ z[47];
    z[47]=z[12]*z[47];
    z[48]=npow(z[3],2)*z[66];
    z[62]=z[3]*z[66];
    z[63]=z[13]*z[143];
    z[62]=3*z[62] + n<T>(85,3)*z[63];
    z[62]=z[13]*z[62];
    z[48]= - n<T>(8,3)*z[48] + z[62];
    z[48]=z[48]*z[129];
    z[62]=z[111] - z[143];
    z[62]=z[11]*z[62];
    z[31]=z[48] + z[31] + z[39] + n<T>(1,9)*z[47] + 2*z[44] + 5*z[62];
    z[31]=z[15]*z[31];
    z[39]=382*z[107] - 301*z[23] + z[142];
    z[44]=z[144] - n<T>(94,9)*z[107];
    z[44]=z[4]*z[44];
    z[44]=n<T>(130,9)*z[1] + z[44];
    z[47]=z[122]*z[127];
    z[47]=n<T>(283,3)*z[1] + z[47];
    z[47]=z[47]*z[124];
    z[44]=4*z[44] + z[47];
    z[44]=z[16]*z[44];
    z[47]=z[60] + z[107];
    z[48]=32*z[107];
    z[60]= - 55*z[23] + z[48];
    z[60]=z[4]*z[60];
    z[60]=z[149] + z[60];
    z[60]=z[16]*z[60];
    z[60]=78*z[47] + n<T>(5,9)*z[60];
    z[60]=z[16]*z[60];
    z[62]= - z[47]*z[54];
    z[63]=z[1]*z[69];
    z[62]=z[63] + z[62];
    z[63]=n<T>(632,9)*z[7];
    z[62]=z[62]*z[63];
    z[60]=z[62] + n<T>(1240,9)*z[89] + z[60];
    z[60]=z[60]*z[46];
    z[39]=z[60] + n<T>(1,9)*z[39] + z[44];
    z[39]=z[7]*z[39];
    z[44]=n<T>(538,9)*z[81] + 17*z[94];
    z[44]=z[16]*z[44];
    z[44]=130*z[77] + z[44];
    z[44]=z[44]*z[78];
    z[53]=z[28]*z[53];
    z[53]=z[55] + z[53];
    z[53]=z[16]*z[53];
    z[53]=z[90] + z[53];
    z[53]=z[53]*z[148];
    z[44]=z[44] + n<T>(632,9)*z[53];
    z[44]=z[44]*z[140];
    z[53]=n<T>(671,3)*z[33] - 109*z[32];
    z[53]=z[16]*z[53];
    z[53]=n<T>(1402,3)*z[61] + z[53];
    z[49]=z[53]*z[49];
    z[44]=z[49] + z[44];
    z[44]=z[44]*z[36];
    z[49]=10*z[104] - 43*z[1] - 11*z[105];
    z[49]=z[16]*z[49];
    z[53]=71*z[25];
    z[49]=z[49] + z[107] + 53*z[23] + z[53];
    z[55]= - 23*z[33] + 8*z[32];
    z[55]=z[16]*z[55];
    z[55]=z[103] + n<T>(1,3)*z[55];
    z[36]=z[55]*z[36];
    z[36]=n<T>(1,3)*z[49] + z[36];
    z[36]=z[36]*z[82];
    z[49]= - n<T>(97,3)*z[23] - 7*z[42];
    z[49]=z[3]*z[49];
    z[36]=z[36] + z[49] + z[44] + z[39] - 22*z[1] - n<T>(37,3)*z[105];
    z[36]=z[11]*z[36];
    z[39]=1129*z[33];
    z[44]=160*z[57] + 899*z[40] + z[39];
    z[44]=z[4]*z[44];
    z[47]=z[42] + z[47];
    z[47]=z[16]*z[47];
    z[47]=z[47] + z[33] - z[57];
    z[47]=z[7]*z[47];
    z[49]= - 1129*z[23] - 160*z[107];
    z[49]=z[4]*z[49];
    z[49]=749*z[1] + z[49];
    z[49]=z[16]*z[49];
    z[44]=632*z[47] + z[49] + z[44] - 427*z[23] - z[141];
    z[44]=z[44]*z[58];
    z[47]= - 478*z[40] - 275*z[33];
    z[49]=z[128] + n<T>(5,3)*z[83];
    z[55]=8*z[4];
    z[49]=z[49]*z[55];
    z[47]=n<T>(1,9)*z[47] + z[49];
    z[47]=z[47]*z[41];
    z[49]= - 265*z[23] - 664*z[25];
    z[47]=n<T>(1,9)*z[49] + z[47];
    z[47]=z[4]*z[47];
    z[49]=n<T>(55,3)*z[23] - 8*z[107];
    z[49]=z[4]*z[49];
    z[49]=54*z[1] + n<T>(5,3)*z[49];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(442,9) + z[49];
    z[35]=z[49]*z[35];
    z[35]=z[44] + z[35] + z[47] + n<T>(58,9)*z[1] + 31*z[9];
    z[35]=z[7]*z[35];
    z[39]=2500*z[40] + z[39];
    z[39]=z[39]*z[100];
    z[39]=n<T>(160,3)*z[113] + 457*z[65] + z[39];
    z[39]=z[39]*z[115];
    z[44]=z[102] - z[113];
    z[44]=z[44]*z[63];
    z[39]=z[44] + z[50] + z[39] - n<T>(2233,9)*z[40] - 9*z[33];
    z[39]=z[39]*z[46];
    z[44]= - z[119]*z[55];
    z[47]= - 970*z[23] - n<T>(721,3)*z[25];
    z[47]=z[9]*z[47];
    z[47]= - n<T>(971,3)*z[40] + z[47];
    z[44]=n<T>(1,3)*z[47] + z[44];
    z[44]=z[4]*z[44];
    z[47]=n<T>(1279,9)*z[23] + z[67];
    z[39]=z[39] - 13*z[42] + 2*z[47] + z[44];
    z[39]=z[7]*z[39];
    z[42]= - 92*z[23] + z[53];
    z[42]=z[42]*z[115];
    z[42]=27*z[104] + z[42] - n<T>(311,9)*z[1] + 5*z[9];
    z[37]=z[6]*z[37];
    z[37]=n<T>(32,3)*z[37] + 2*z[42] + z[39];
    z[37]=z[6]*z[37];
    z[39]=47*z[33];
    z[42]=z[39] + 761*z[40];
    z[42]=z[42]*z[9];
    z[42]=z[42] + 1381*z[65];
    z[44]=z[65] + n<T>(1,3)*z[83];
    z[44]=z[44]*z[9];
    z[44]=z[44] + z[114];
    z[44]=z[44]*z[138];
    z[42]= - z[44] + n<T>(1,3)*z[42];
    z[42]=z[42]*z[115];
    z[44]=z[151] + n<T>(97,9)*z[23];
    z[47]=2*z[9];
    z[44]=z[44]*z[47];
    z[42]=z[42] + z[44] - n<T>(377,9)*z[40];
    z[42]=z[42]*z[4];
    z[39]=z[70] - z[39];
    z[39]=z[39]*z[9];
    z[39]=z[39] + z[59];
    z[44]=n<T>(1,9)*z[7];
    z[39]=z[39]*z[44];
    z[44]=z[151] + n<T>(47,9)*z[23];
    z[44]=z[44]*z[47];
    z[39]= - z[39] + z[44] - n<T>(427,9)*z[40];
    z[39]=z[39]*z[7];
    z[39]=z[42] + z[39] - n<T>(50,9)*z[23];
    z[42]=npow(z[1],6);
    z[44]=z[42]*z[138];
    z[47]=667*z[114];
    z[44]=z[44] - z[47];
    z[44]=z[44]*z[41];
    z[49]=935*z[65];
    z[44]=z[44] + z[49];
    z[44]=z[44]*z[4];
    z[50]=314*z[40];
    z[44]=z[44] - z[50];
    z[53]=n<T>(1,9)*z[3];
    z[44]=z[44]*z[53];
    z[42]=z[42]*z[99];
    z[42]=z[42] - z[47];
    z[42]=z[42]*z[46];
    z[42]=z[42] + z[49];
    z[42]=z[42]*z[7];
    z[42]=z[42] - z[50];
    z[46]=n<T>(1,9)*z[6];
    z[42]=z[42]*z[46];
    z[46]=z[23]*z[6];
    z[46]=z[46] - z[135];
    z[46]= - z[20]*z[46];
    z[39]=n<T>(25,9)*z[46] + z[44] + z[42] - 2*z[39];
    z[42]=z[8] - z[2];
    z[39]=z[5]*z[42]*z[39];
    z[42]= - z[12] - z[11];
    z[42]=z[23]*z[42];
    z[44]=z[134] + z[92];
    z[44]=z[18]*z[44];
    z[40]=z[40]*z[12];
    z[46]= - z[134] + z[40];
    z[46]=z[17]*z[46];
    z[40]=z[92] + z[40];
    z[40]=z[13]*z[40];
    z[40]=z[40] + z[46] + z[44] + z[42];
    z[40]=z[22]*z[40];
    z[42]= - z[54]*z[147]*z[52];
    z[44]= - z[80]*z[77];
    z[42]=z[44] + z[42];
    z[42]=z[10]*z[42];
    z[44]=z[133]*z[61];
    z[42]=z[44] + z[42];
    z[44]=z[96] + z[115];
    z[44]=z[61]*z[44];
    z[46]=z[41]*z[101];
    z[44]=z[46] + z[44];
    z[44]=z[6]*z[44];
    z[33]=n<T>(205,9)*z[33] - 14*z[32];
    z[33]=z[16]*z[33]*z[78];
    z[33]=z[44] + z[33] + n<T>(632,9)*z[42];
    z[33]=z[33]*z[116];
    z[42]= - 22*z[23] + 23*z[107];
    z[42]=z[4]*z[42];
    z[42]= - z[1] + z[42];
    z[44]=43*z[23] - z[48];
    z[44]=z[44]*z[51];
    z[44]= - z[1] + z[44];
    z[44]=z[44]*z[124];
    z[42]=n<T>(4,3)*z[42] + z[44];
    z[42]=z[16]*z[42];
    z[23]= - 113*z[23] + 146*z[107];
    z[23]= - n<T>(10,3)*z[72] + n<T>(1,3)*z[23] + z[42];
    z[23]=z[7]*z[23];
    z[42]= - 2*z[72] + z[56];
    z[42]=z[42]*z[146];
    z[44]= - z[7] - z[4];
    z[32]=z[32]*z[44];
    z[32]= - z[93] + z[32];
    z[32]=z[32]*z[75];
    z[44]=n<T>(20,3) - z[112];
    z[44]=z[16]*z[44];
    z[23]=z[42] + z[32] + n<T>(100,3)*z[1] + z[44] + z[23];
    z[23]=z[3]*z[23];
    z[28]=z[28]*z[111];
    z[25]=2425*z[25] - 2528*z[28];
    z[25]=z[4]*z[25];
    z[25]=n<T>(2,9)*z[25] - n<T>(440,9)*z[1] + z[9];
    z[25]=z[4]*z[25];
    z[28]=static_cast<T>(13)+ 18*z[111];
    z[28]=z[16]*z[28]*z[41];

    r +=  - n<T>(68,3) + z[23] + 6*z[24] + z[25] + z[26] + z[27] + z[28] + 
      z[29] + z[30] + z[31] + z[33] + z[34] + z[35] + z[36] + z[37] + 
      z[38] + 2*z[39] + 4*z[40] + z[43] + z[45];
 
    return r;
}

template double qg_2lNLC_r1052(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1052(const std::array<dd_real,31>&);
#endif
