#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1230(const std::array<T,31>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[19];
    z[7]=k[11];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[8];
    z[11]=k[26];
    z[12]=k[3];
    z[13]=npow(z[9],2);
    z[14]= - z[10]*z[13];
    z[15]=z[9]*z[10];
    z[16]=2*z[15];
    z[17]=z[16] - 1;
    z[17]=z[17]*z[9];
    z[18]=z[8] + z[17];
    z[18]=z[7]*z[9]*z[18];
    z[14]=z[18] + 2*z[12] + z[14];
    z[14]=z[5]*z[14];
    z[18]=z[15] + 1;
    z[19]= - static_cast<T>(2)+ 3*z[15];
    z[19]=z[9]*z[19];
    z[19]=z[8] + z[19];
    z[19]=z[7]*z[19];
    z[14]=z[14] + z[19] - z[18];
    z[19]=2*z[5];
    z[14]=z[14]*z[19];
    z[20]=2*z[4];
    z[21]=z[20] - z[6];
    z[22]=z[9]*z[3];
    z[21]=z[21]*z[22];
    z[23]=z[4]*z[8];
    z[24]= - z[6] + 4*z[10];
    z[24]=z[9]*z[24];
    z[24]=z[24] - static_cast<T>(3)- z[23];
    z[24]=z[7]*z[24];
    z[14]=z[14] + z[24] - z[21] - z[3] + z[10] + z[4];
    z[14]=z[5]*z[14];
    z[24]=z[10]*z[11];
    z[25]=z[4]*z[11];
    z[26]=z[10] + z[6];
    z[27]=z[3]*z[26];
    z[28]=z[4] - z[6];
    z[29]=z[10] + z[28];
    z[29]=z[7]*z[29];
    z[14]=z[14] + z[29] + z[27] + z[24] + z[25];
    z[14]=z[5]*z[14];
    z[25]=n<T>(1,2)*z[10];
    z[27]= - z[11] - z[25];
    z[27]=z[10]*z[27];
    z[29]= - z[11] - n<T>(1,2)*z[6];
    z[29]=z[4]*z[29];
    z[27]=z[27] + z[29];
    z[27]=z[3]*z[27];
    z[29]=npow(z[11],2);
    z[30]=z[24] + z[29];
    z[30]=z[30]*z[10];
    z[29]=z[29]*z[4];
    z[29]=z[30] + z[29];
    z[29]=n<T>(1,2)*z[29];
    z[30]= - z[22]*z[29];
    z[31]=z[4]*z[6];
    z[32]=z[31]*z[7];
    z[14]=z[14] + n<T>(1,2)*z[32] + z[27] + z[30];
    z[27]=npow(z[2],2);
    z[14]=z[14]*z[27];
    z[26]=z[9]*z[26];
    z[26]=z[26] + static_cast<T>(2)+ z[23];
    z[26]=z[7]*z[26];
    z[30]=z[9]*z[18];
    z[30]= - z[8] + z[30];
    z[30]=z[7]*z[30];
    z[30]= - static_cast<T>(1)+ z[30];
    z[30]=z[5]*z[30];
    z[21]=z[30] + z[26] - z[20] + z[21];
    z[21]=z[21]*z[19];
    z[26]=3*z[6];
    z[20]= - z[26] + z[20];
    z[20]=z[3]*z[20];
    z[26]= - 3*z[4] + z[26] + z[10];
    z[26]=z[7]*z[26];
    z[20]=z[21] + z[20] + z[26];
    z[20]=z[5]*z[20];
    z[21]=z[11] + z[6];
    z[21]=z[4]*z[21];
    z[21]=z[24] + z[21];
    z[21]=z[3]*z[21];
    z[21]=z[21] - z[32];
    z[20]=2*z[21] + z[20];
    z[20]=z[5]*z[20];
    z[21]= - z[3]*z[29];
    z[20]=z[21] + z[20];
    z[20]=z[20]*z[27];
    z[21]= - z[28]*z[22];
    z[22]= - z[9]*z[6];
    z[22]=z[22] - static_cast<T>(1)- z[23];
    z[22]=z[7]*z[22];
    z[21]=z[22] + z[4] + z[21];
    z[21]=z[5]*z[21];
    z[22]=z[3] - z[7];
    z[22]=z[28]*z[22];
    z[21]= - 2*z[22] + z[21];
    z[21]=z[5]*z[21];
    z[23]=z[31]*z[3];
    z[23]=z[23] - z[32];
    z[24]=2*z[23];
    z[21]= - z[24] + z[21];
    z[21]=z[21]*npow(z[5],2);
    z[22]=z[5]*z[22];
    z[22]=z[24] + z[22];
    z[22]=z[22]*npow(z[5],3);
    z[23]= - z[1]*z[23]*npow(z[5],4);
    z[22]=z[22] + z[23];
    z[22]=z[1]*z[22];
    z[21]=z[21] + z[22];
    z[21]=z[1]*z[27]*z[21];
    z[20]=z[20] + 2*z[21];
    z[20]=z[1]*z[20];
    z[14]=z[14] + z[20];
    z[14]=z[1]*z[14];
    z[20]=z[10]*z[8];
    z[21]=z[20] + z[15];
    z[22]= - z[9]*z[21];
    z[22]=z[22] + z[12] + 2*z[8];
    z[22]=z[9]*z[22];
    z[23]= - static_cast<T>(1)+ z[15];
    z[23]=z[9]*z[23];
    z[23]= - z[8] + z[23];
    z[13]=z[7]*z[23]*z[13];
    z[23]=npow(z[12],2);
    z[24]= - z[8]*z[12];
    z[13]=z[13] + z[22] - z[23] + z[24];
    z[13]=z[5]*z[13];
    z[17]= - z[8] + z[17];
    z[17]=z[7]*z[17];
    z[17]=z[17] - z[20] - z[18];
    z[17]=z[9]*z[17];
    z[13]=z[13] + z[12] + z[8] + z[17];
    z[13]=z[13]*z[19];
    z[15]= - static_cast<T>(1)+ 4*z[15];
    z[15]=z[9]*z[15];
    z[15]= - z[8] + z[15];
    z[15]=z[7]*z[15];
    z[13]=z[13] + z[15] - static_cast<T>(2)- z[21];
    z[13]=z[5]*z[13];
    z[15]=z[7]*z[16];
    z[13]=z[15] + z[13];
    z[13]=z[5]*z[13];
    z[15]=z[7]*z[25];
    z[13]=z[15] + z[13];
    z[13]=z[13]*z[27];
    z[13]=z[13] + z[14];

    r += z[13]*npow(z[1],2);
 
    return r;
}

template double qg_2lNLC_r1230(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1230(const std::array<dd_real,31>&);
#endif
