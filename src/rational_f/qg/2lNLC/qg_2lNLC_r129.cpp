#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r129(const std::array<T,31>& k) {
  T z[138];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[5];
    z[4]=k[6];
    z[5]=k[8];
    z[6]=k[13];
    z[7]=k[7];
    z[8]=k[12];
    z[9]=k[10];
    z[10]=k[15];
    z[11]=k[3];
    z[12]=k[2];
    z[13]=k[14];
    z[14]=k[9];
    z[15]=k[18];
    z[16]=npow(z[1],2);
    z[17]=z[16]*z[2];
    z[18]=z[17]*z[5];
    z[19]=z[18] - z[16];
    z[19]=z[19]*z[2];
    z[20]=npow(z[1],3);
    z[21]=2*z[20];
    z[22]=z[16]*z[3];
    z[23]=z[21] + z[22];
    z[24]=z[23]*z[3];
    z[25]=npow(z[1],4);
    z[24]=z[24] + z[25];
    z[26]=z[24]*z[8];
    z[27]=z[22] + z[20];
    z[19]=z[19] + z[26] + z[27];
    z[26]= - z[6]*z[19];
    z[28]=z[16]*z[5];
    z[29]=2*z[1];
    z[30]=z[28] - z[29];
    z[31]=z[3]*z[1];
    z[32]=z[31] - z[16];
    z[33]= - z[8]*z[32];
    z[33]=z[33] + z[30];
    z[33]=z[2]*z[33];
    z[34]=npow(z[3],2);
    z[35]=z[34]*z[16];
    z[36]=z[34]*z[20];
    z[37]=z[36]*z[8];
    z[38]= - z[2]*z[37];
    z[39]=npow(z[31],3);
    z[40]=z[39]*z[8];
    z[38]= - z[40] + z[38];
    z[38]=z[4]*z[38];
    z[38]= - z[35] + z[38];
    z[41]=npow(z[4],2);
    z[38]=z[38]*z[41];
    z[42]=z[20]*z[8];
    z[43]=z[42] - z[16];
    z[44]=z[2]*z[43];
    z[45]=z[7]*z[2];
    z[46]= - z[21]*z[45];
    z[44]=z[44] + z[46];
    z[44]=z[9]*z[44];
    z[46]=4*z[31];
    z[47]=3*z[16];
    z[48]=z[46] + z[47];
    z[49]=5*z[16];
    z[50]=z[49] + z[31];
    z[50]=z[3]*z[50];
    z[51]=3*z[20];
    z[50]=z[51] + z[50];
    z[50]=z[8]*z[50];
    z[52]= - z[17] - z[27];
    z[53]=2*z[7];
    z[52]=z[52]*z[53];
    z[54]=z[20]*z[5];
    z[26]=z[44] + z[26] + z[38] + z[52] + z[33] + z[50] + z[54] - z[48];
    z[26]=z[13]*z[26];
    z[33]=2*z[6];
    z[19]=z[19]*z[33];
    z[33]=5*z[31];
    z[38]= - 12*z[16] - z[33];
    z[38]=z[3]*z[38];
    z[44]=7*z[20];
    z[38]= - z[44] + z[38];
    z[38]=z[8]*z[38];
    z[50]=2*z[5];
    z[52]=z[2]*z[1];
    z[55]= - z[52]*z[50];
    z[56]=5*z[1];
    z[55]=z[56] + z[55];
    z[55]=z[2]*z[55];
    z[57]=6*z[16];
    z[19]=z[19] + z[55] + z[38] - z[57] - z[33];
    z[19]=z[6]*z[19];
    z[38]=4*z[16];
    z[55]=z[38] + z[33];
    z[58]=z[31] + z[16];
    z[59]=z[8]*z[3];
    z[60]= - z[58]*z[59];
    z[60]=z[60] - z[55];
    z[60]=z[8]*z[60];
    z[61]=9*z[1];
    z[60]= - z[61] + z[60];
    z[62]=2*z[8];
    z[60]=z[60]*z[62];
    z[63]=z[58]*z[8];
    z[64]= - z[29] - z[63];
    z[65]=npow(z[8],2);
    z[64]=z[64]*z[65];
    z[66]=npow(z[8],3);
    z[67]= - z[66]*z[52];
    z[64]=2*z[64] + z[67];
    z[67]=2*z[2];
    z[64]=z[64]*z[67];
    z[68]=3*z[5];
    z[69]=z[68]*z[1];
    z[60]=z[64] + z[60] - static_cast<T>(4)+ z[69];
    z[60]=z[2]*z[60];
    z[64]=2*z[16];
    z[70]=z[64]*z[2];
    z[71]=z[70] + z[22];
    z[72]=z[71]*z[53];
    z[72]=z[72] - 6*z[52] + 20*z[16] + 11*z[31];
    z[72]=z[7]*z[72];
    z[73]=z[34]*z[8];
    z[74]=z[73]*z[16];
    z[75]=z[22]*z[8];
    z[76]=3*z[2];
    z[77]= - z[76]*z[75];
    z[78]=z[10]*z[35];
    z[77]= - 21*z[78] + z[74] + z[77];
    z[77]=z[77]*z[41];
    z[78]= - z[65]*z[47];
    z[79]= - z[66]*z[17];
    z[78]=z[78] + z[79];
    z[78]=z[78]*z[67];
    z[79]=9*z[16];
    z[80]= - z[8]*z[79];
    z[78]=z[78] + z[56] + z[80];
    z[78]=z[2]*z[78];
    z[80]=npow(z[2],2);
    z[81]=z[80]*z[7];
    z[82]=z[64]*z[81];
    z[83]=4*z[17];
    z[82]=z[82] + z[44] - z[83];
    z[82]=z[7]*z[82];
    z[43]=z[82] + z[78] + z[43];
    z[43]=z[9]*z[43];
    z[78]=2*z[3];
    z[63]= - z[78]*z[63];
    z[63]=z[63] - z[47] - z[33];
    z[63]=z[8]*z[63];
    z[82]=npow(z[5],2);
    z[84]=z[82]*z[25];
    z[85]= - z[47] + 2*z[84];
    z[85]=z[5]*z[85];
    z[86]=32*z[31];
    z[87]= - z[79] - z[86];
    z[87]=z[10]*z[87];
    z[88]=11*z[3];
    z[19]=z[26] + z[43] + z[19] + z[77] + z[87] + z[72] + z[60] + z[63]
    + z[85] - z[1] - z[88];
    z[19]=z[13]*z[19];
    z[26]=npow(z[1],5);
    z[43]=z[68]*z[26];
    z[60]=4*z[25];
    z[43]=z[43] + z[60];
    z[43]=z[43]*z[82];
    z[63]=npow(z[1],6);
    z[72]= - z[63]*z[68];
    z[72]= - z[26] + z[72];
    z[72]=z[5]*z[72];
    z[72]=z[60] + z[72];
    z[72]=z[5]*z[72];
    z[72]= - z[20] + z[72];
    z[72]=z[8]*z[72];
    z[77]=23*z[1];
    z[85]= - z[8]*z[16];
    z[85]= - z[77] + z[85];
    z[85]=z[2]*z[85];
    z[72]=z[85] + z[72] + z[57] - z[43];
    z[85]=41*z[16] - 52*z[52];
    z[85]=z[2]*z[85];
    z[87]=10*z[20];
    z[85]= - z[87] + z[85];
    z[85]=z[2]*z[85];
    z[89]=z[52] - z[16];
    z[90]=z[89]*z[76];
    z[90]=z[90] + z[20];
    z[91]=5*z[81];
    z[92]=z[90]*z[91];
    z[85]=z[85] + z[92];
    z[85]=z[7]*z[85];
    z[92]= - z[49] + 8*z[52];
    z[92]=z[2]*z[92];
    z[85]=z[85] + z[44] + 10*z[92];
    z[85]=z[7]*z[85];
    z[92]= - z[8]*z[21];
    z[92]= - 7*z[52] + z[47] + z[92];
    z[92]=z[2]*z[92];
    z[93]= - z[64] + z[52];
    z[93]=z[2]*z[93];
    z[93]=z[20] + z[93];
    z[93]=z[93]*z[45];
    z[92]=z[92] + 3*z[93];
    z[92]=z[10]*z[92];
    z[72]=z[92] + 2*z[72] + z[85];
    z[72]=z[10]*z[72];
    z[85]=5*z[10];
    z[92]=npow(z[7],3);
    z[93]=z[92]*z[4];
    z[94]= - npow(z[52],5)*z[93]*z[85];
    z[95]=npow(z[52],4);
    z[96]=z[92]*z[95];
    z[97]=npow(z[7],2);
    z[98]=21*z[97];
    z[95]=z[95]*z[10];
    z[99]=z[95]*z[98];
    z[94]=z[94] - 5*z[96] + z[99];
    z[94]=z[4]*z[94];
    z[96]=z[21]*z[10];
    z[99]=npow(z[2],3);
    z[100]=z[96]*z[99];
    z[101]=z[99]*z[20];
    z[102]=z[101]*z[7];
    z[103]= - 37*z[102] + z[100];
    z[103]=z[10]*z[103];
    z[98]=z[101]*z[98];
    z[94]=z[94] + z[98] + z[103];
    z[94]=z[4]*z[94];
    z[98]=z[16]*z[10];
    z[101]=z[98]*z[80];
    z[103]=z[16]*z[81];
    z[94]=z[94] - 37*z[103] + 29*z[101];
    z[94]=z[94]*z[41];
    z[103]=2*z[52];
    z[104]=z[103] - z[16];
    z[105]=z[104]*z[67];
    z[90]= - z[90]*z[45];
    z[90]=z[105] + z[90];
    z[90]=z[10]*z[90];
    z[95]=z[4]*z[7]*z[95];
    z[95]=z[95] + z[102] - z[100];
    z[95]=z[4]*z[95];
    z[100]= - z[80]*z[64];
    z[95]=z[100] + z[95];
    z[95]=z[95]*z[41];
    z[100]= - z[104]*z[45];
    z[90]=z[95] + z[90] + z[103] + z[100];
    z[95]=2*z[9];
    z[90]=z[90]*z[95];
    z[100]=4*z[54];
    z[102]= - z[47] - z[100];
    z[105]=z[65]*z[5];
    z[102]=z[102]*z[105];
    z[106]=z[54] + z[16];
    z[106]=z[106]*z[5];
    z[66]= - z[2]*z[66]*z[106];
    z[66]=z[102] + z[66];
    z[66]=z[66]*z[67];
    z[102]=z[25]*z[5];
    z[107]= - z[21] + z[102];
    z[107]=z[5]*z[107];
    z[107]= - z[64] + z[107];
    z[108]=z[8]*z[5];
    z[107]=z[107]*z[108];
    z[109]=z[5]*z[29];
    z[66]=z[66] + 6*z[107] + static_cast<T>(25)+ z[109];
    z[66]=z[2]*z[66];
    z[107]=10*z[16];
    z[109]=z[107] - 33*z[52];
    z[109]=z[2]*z[109];
    z[91]=z[104]*z[91];
    z[91]=z[109] + z[91];
    z[91]=z[7]*z[91];
    z[91]=z[91] - 7*z[16] + 40*z[52];
    z[91]=z[7]*z[91];
    z[89]=z[89]*z[6];
    z[104]=z[102] - z[20];
    z[109]=z[5]*z[104];
    z[109]= - z[16] + z[109];
    z[109]=z[5]*z[109];
    z[110]=4*z[1];
    z[109]= - z[110] + z[109];
    z[43]= - z[47] + 2*z[43];
    z[43]=z[8]*z[43];
    z[43]=z[90] + 12*z[89] + z[94] + z[72] + z[91] + z[66] + 6*z[109] + 
    z[43];
    z[43]=z[9]*z[43];
    z[66]=z[53]*z[40];
    z[66]=z[66] - 6*z[35] - 5*z[37];
    z[66]=z[7]*z[66];
    z[72]=npow(z[3],4);
    z[90]=npow(z[5],3);
    z[91]=z[80]*z[4];
    z[94]=z[63]*z[72]*z[90]*z[91];
    z[109]=z[82]*z[2];
    z[111]=npow(z[3],3);
    z[112]= - z[26]*z[111]*z[109];
    z[72]=z[72]*z[26];
    z[113]=z[82]*z[72];
    z[112]=z[113] + z[112];
    z[112]=z[112]*z[67];
    z[94]=z[112] + z[94];
    z[94]=z[4]*z[94];
    z[112]=z[111]*z[5];
    z[60]= - z[112]*z[60];
    z[113]=z[34]*z[5];
    z[114]=z[113]*z[2];
    z[115]= - z[25]*z[114];
    z[60]=z[60] + z[115];
    z[60]=z[2]*z[60];
    z[115]=npow(z[31],4);
    z[116]=z[5]*z[115];
    z[60]=z[94] + z[116] + z[60];
    z[60]=z[4]*z[60];
    z[94]=z[20]*z[3];
    z[116]=z[94]*z[2];
    z[117]= - z[36] + z[116];
    z[117]=z[2]*z[117];
    z[117]= - z[39] + z[117];
    z[60]=2*z[117] + z[60];
    z[117]=npow(z[4],3);
    z[60]=z[60]*z[117];
    z[118]=z[64] - z[31];
    z[119]=z[3]*z[118];
    z[120]=z[58]*z[113];
    z[119]=z[119] + z[120];
    z[120]=z[78]*z[42];
    z[121]=z[112]*z[1];
    z[122]=z[34]*z[1];
    z[123]= - z[122] + z[121];
    z[123]=z[5]*z[123];
    z[124]=2*z[31];
    z[123]= - z[124] + z[123];
    z[123]=z[123]*z[76];
    z[60]=z[60] + z[66] + z[123] + 3*z[119] + z[120];
    z[60]=z[6]*z[60];
    z[66]=z[64]*z[34];
    z[119]=z[78]*z[16];
    z[120]=z[119] - z[20];
    z[123]= - z[120]*z[73];
    z[123]= - z[66] + z[123];
    z[123]=z[123]*z[53];
    z[125]= - 9*z[20] + 7*z[22];
    z[125]=z[125]*z[59];
    z[126]= - z[2]*z[124];
    z[123]=z[123] + z[126] - 9*z[22] + z[125];
    z[123]=z[7]*z[123];
    z[125]= - z[2]*z[34];
    z[125]=z[111] + z[125];
    z[125]=z[76]*z[84]*z[125];
    z[126]=z[5]*z[3];
    z[127]=z[26]*npow(z[126],3)*z[91];
    z[125]=z[125] + z[127];
    z[125]=z[4]*z[125];
    z[127]=z[20]*z[2];
    z[128]=z[127]*z[126];
    z[129]=z[5]*z[36];
    z[129]= - 8*z[129] + z[128];
    z[129]=z[2]*z[129];
    z[130]=z[39]*z[50];
    z[125]=z[125] + z[130] + z[129];
    z[125]=z[4]*z[125];
    z[129]= - z[34]*z[49];
    z[130]=z[119] + z[17];
    z[130]=z[2]*z[130];
    z[125]=z[125] + z[129] + z[130];
    z[129]=2*z[41];
    z[125]=z[125]*z[129];
    z[130]=z[28]*z[34];
    z[88]=z[58]*z[88];
    z[88]=z[88] + z[130];
    z[88]=z[5]*z[88];
    z[131]=z[27]*z[126];
    z[120]= - 2*z[120] + z[131];
    z[120]=z[8]*z[120];
    z[121]=4*z[122] + z[121];
    z[121]=z[5]*z[121];
    z[131]=6*z[31];
    z[121]= - z[52] - z[131] + z[121];
    z[121]=z[5]*z[121];
    z[121]= - z[56] + z[121];
    z[121]=z[121]*z[67];
    z[132]=z[49] - z[131];
    z[60]=z[60] + z[125] + z[123] + z[121] + z[120] + 2*z[132] + z[88];
    z[60]=z[6]*z[60];
    z[88]=z[94]*z[8];
    z[120]= - z[88] + z[51] + z[119];
    z[120]=z[8]*z[120];
    z[121]=z[27]*z[68];
    z[123]=9*z[31];
    z[120]=z[120] + z[121] + z[49] + z[123];
    z[120]=z[8]*z[120];
    z[121]= - z[27]*z[78];
    z[121]=z[121] + z[37];
    z[121]=z[8]*z[121];
    z[125]=15*z[16] + z[46];
    z[125]=z[3]*z[125];
    z[121]=z[121] + z[20] + z[125];
    z[121]=z[8]*z[121];
    z[125]=z[124] + z[47];
    z[132]= - z[3] - z[73];
    z[132]=z[118]*z[132];
    z[133]= - z[2]*z[31];
    z[132]=z[133] + z[132];
    z[132]=z[132]*z[53];
    z[121]=z[132] - 4*z[52] + z[121] + z[125];
    z[121]=z[7]*z[121];
    z[132]= - z[78]*z[28];
    z[132]=z[132] + z[18];
    z[132]=z[2]*z[132];
    z[132]= - z[130] + z[132];
    z[133]=z[34]*z[4];
    z[134]=z[21]*z[109]*z[133];
    z[132]=5*z[132] + z[134];
    z[132]=z[132]*z[41];
    z[134]=4*z[22] + z[130];
    z[134]=z[5]*z[134];
    z[134]=3*z[125] + z[134];
    z[134]=z[5]*z[134];
    z[135]=z[29]*z[113];
    z[135]= - z[31] + z[135];
    z[135]=z[5]*z[135];
    z[135]= - z[56] + z[135];
    z[135]=z[5]*z[135];
    z[135]=static_cast<T>(4)+ z[135];
    z[42]=z[47] + z[42];
    z[42]=z[8]*z[42];
    z[136]=3*z[1];
    z[42]=z[136] + z[42];
    z[42]=z[8]*z[42];
    z[42]=2*z[135] + z[42];
    z[42]=z[2]*z[42];
    z[135]= - z[1] - z[3];
    z[42]=z[60] + z[132] + z[121] + z[42] + z[120] + 6*z[135] + z[134];
    z[42]=z[6]*z[42];
    z[60]=z[111]*z[8];
    z[120]=5*z[25];
    z[92]= - z[92]*z[120]*z[60];
    z[93]=z[93]*z[65];
    z[72]=z[93]*z[72];
    z[72]=z[92] - 2*z[72];
    z[72]=z[4]*z[72];
    z[92]=z[127] + z[94];
    z[121]=z[2]*z[92];
    z[121]= - 3*z[36] + z[121];
    z[121]=z[121]*z[97];
    z[132]=z[65]*z[36];
    z[121]= - 4*z[132] + z[121];
    z[121]=z[7]*z[121];
    z[72]=z[121] + z[72];
    z[72]=z[4]*z[72];
    z[83]= - z[22] + z[83];
    z[83]=z[7]*z[83];
    z[75]= - 14*z[75] + z[83];
    z[75]=z[7]*z[75];
    z[83]= - z[12]*npow(z[9],2)*z[64];
    z[72]=z[83] + z[75] + z[72];
    z[72]=z[41]*z[72];
    z[75]=z[97]*z[80];
    z[83]=17*z[20];
    z[121]= - z[83]*z[75];
    z[132]=npow(z[45],3);
    z[134]=z[4]*z[132]*z[120];
    z[121]=z[121] + z[134];
    z[121]=z[4]*z[121];
    z[134]=z[7]*z[17];
    z[121]=21*z[134] + z[121];
    z[121]=z[121]*z[41];
    z[134]= - z[56]*z[81];
    z[134]=9*z[52] + z[134];
    z[134]=z[7]*z[134];
    z[134]= - 7*z[1] + z[134];
    z[134]=z[7]*z[134];
    z[135]= - z[4]*z[20]*z[81];
    z[70]=z[70] + z[135];
    z[70]=z[70]*z[41];
    z[135]=z[7]*z[52];
    z[70]=z[70] - z[1] + z[135];
    z[70]=z[70]*z[95];
    z[135]=12*z[6];
    z[135]=z[1]*z[135];
    z[70]=z[70] + z[135] + z[121] - static_cast<T>(9)+ z[134];
    z[70]=z[9]*z[70];
    z[60]= - z[29]*z[60];
    z[60]= - 3*z[122] + z[60];
    z[60]=z[8]*z[60];
    z[60]= - z[52] - z[31] + z[60];
    z[60]=z[7]*z[60];
    z[121]=z[31]*z[8];
    z[60]=z[60] + z[29] + z[121];
    z[60]=z[7]*z[60];
    z[134]= - z[8]*z[46];
    z[134]= - z[61] + z[134];
    z[134]=z[8]*z[134];
    z[60]=z[60] - static_cast<T>(15)+ z[134];
    z[60]=z[7]*z[60];
    z[71]=z[71]*z[41];
    z[71]= - z[110] + z[71];
    z[116]= - z[36] + 4*z[116];
    z[116]=z[116]*z[117];
    z[116]= - z[131] + z[116];
    z[116]=z[6]*z[116];
    z[71]=2*z[71] + z[116];
    z[71]=z[6]*z[71];
    z[71]=static_cast<T>(2)+ z[71];
    z[71]=z[6]*z[71];
    z[116]=z[9]*z[1];
    z[134]= - z[12]*z[116];
    z[121]=z[134] - z[1] - z[121];
    z[134]=2*z[14];
    z[121]=z[121]*z[134];
    z[135]=z[8]*z[1];
    z[121]=z[121] - z[135] + z[116];
    z[121]=z[121]*z[134];
    z[134]=z[41]*z[16];
    z[137]=z[6]*z[117]*z[94];
    z[137]=z[134] + z[137];
    z[137]=z[11]*z[137]*npow(z[6],2);
    z[60]=2*z[137] + z[121] + z[70] + z[71] + z[60] + z[72];
    z[60]=z[11]*z[60];
    z[70]=z[90]*z[34];
    z[71]= - z[26]*z[65]*z[99]*z[70];
    z[72]=5*z[5];
    z[99]=4*z[8];
    z[121]= - z[72] + z[99];
    z[121]=z[7]*z[8]*z[121];
    z[137]=4*z[105];
    z[121]=z[137] + z[121];
    z[97]=z[97]*z[121]*npow(z[31],5);
    z[93]= - npow(z[31],6)*z[50]*z[93];
    z[71]=z[93] + z[71] + z[97];
    z[71]=z[4]*z[71];
    z[93]=z[5] - z[8];
    z[93]=z[99]*z[115]*z[93];
    z[97]= - z[68] + 10*z[8];
    z[97]=z[115]*z[97];
    z[99]=z[112] + z[114];
    z[99]=z[2]*z[25]*z[99];
    z[97]=z[99] + z[97];
    z[97]=z[7]*z[97];
    z[93]=z[93] + z[97];
    z[93]=z[7]*z[93];
    z[97]= - z[115]*z[137];
    z[93]=z[97] + z[93];
    z[93]=z[7]*z[93];
    z[97]= - z[3] - z[67];
    z[85]=z[132]*z[85]*z[25]*z[97];
    z[70]=z[25]*z[70];
    z[84]=z[73]*z[84];
    z[70]=z[70] - 5*z[84];
    z[70]=z[8]*z[70]*z[80];
    z[70]=z[71] + z[85] + z[70] + z[93];
    z[70]=z[4]*z[70];
    z[71]=z[50]*z[36];
    z[71]=z[71] - z[128];
    z[71]=z[2]*z[71];
    z[84]= - z[94] - 5*z[127];
    z[84]=z[2]*z[84];
    z[84]= - z[36] + z[84];
    z[84]=z[2]*z[84];
    z[84]=3*z[39] + z[84];
    z[53]=z[84]*z[53];
    z[84]=z[39]*z[5];
    z[53]=z[53] + z[71] - z[84] - 4*z[40];
    z[53]=z[7]*z[53];
    z[71]=z[78]*z[20];
    z[80]=z[82]*z[80]*z[71];
    z[40]= - 4*z[84] + 3*z[40];
    z[40]=z[8]*z[40];
    z[40]=z[40] + z[80];
    z[40]=2*z[40] + z[53];
    z[40]=z[7]*z[40];
    z[53]=z[82]*z[36];
    z[37]= - z[50]*z[37];
    z[37]=z[53] + z[37];
    z[37]=z[8]*z[37];
    z[53]= - z[109]*z[88];
    z[37]=z[37] + z[53];
    z[37]=z[37]*z[67];
    z[39]=z[39]*z[105];
    z[37]=z[39] + z[37];
    z[39]=5*z[94] + 26*z[127];
    z[39]=z[39]*z[75];
    z[53]=z[10]*z[92]*z[81];
    z[39]=z[39] + z[53];
    z[39]=z[10]*z[39];
    z[37]=z[70] + z[39] + 2*z[37] + z[40];
    z[37]=z[4]*z[37];
    z[39]= - 6*z[22] + 23*z[17];
    z[39]=z[2]*z[39];
    z[39]=z[66] + z[39];
    z[39]=z[7]*z[39];
    z[40]=z[126]*z[16];
    z[53]=7*z[40];
    z[66]=z[53] - 6*z[18];
    z[66]=z[2]*z[66];
    z[39]=z[39] + z[66] - 7*z[130] + 20*z[74];
    z[39]=z[7]*z[39];
    z[66]=15*z[130] - 8*z[74];
    z[66]=z[8]*z[66];
    z[70]=3*z[22];
    z[75]=z[82]*z[70];
    z[53]= - z[8]*z[53];
    z[53]=z[75] + z[53];
    z[53]=z[2]*z[53];
    z[75]= - z[119] - 13*z[17];
    z[45]=z[75]*z[45];
    z[45]=z[45] + z[101];
    z[45]=z[10]*z[45];
    z[37]=z[37] + z[45] + z[39] + z[66] + z[53];
    z[37]=z[37]*z[41];
    z[39]= - z[25]*z[68];
    z[39]= - z[44] + z[39];
    z[39]=z[5]*z[39];
    z[39]=z[16] + z[39];
    z[39]=z[39]*z[50];
    z[45]=z[52]*z[10];
    z[39]=3*z[45] + z[61] + z[39];
    z[39]=z[10]*z[39];
    z[53]=npow(z[10],2);
    z[66]= - z[53]*z[21]*z[91];
    z[75]=3*z[10];
    z[75]= - z[17]*z[75];
    z[66]=z[75] + z[66];
    z[66]=z[66]*z[129];
    z[75]=z[50]*z[16];
    z[80]= - 11*z[1] - z[75];
    z[80]=z[5]*z[80];
    z[81]=z[10]*z[20]*z[91];
    z[81]=z[17] + z[81];
    z[81]=z[81]*z[41];
    z[45]= - z[45] + z[81];
    z[45]=z[45]*z[95];
    z[39]=z[45] + z[66] + z[39] - static_cast<T>(4)+ z[80];
    z[39]=z[9]*z[39];
    z[45]=z[90]*z[96];
    z[66]=z[53]*z[4];
    z[80]=z[71]*z[66];
    z[81]=z[10]*z[47];
    z[80]=z[81] + z[80];
    z[80]=z[80]*z[41];
    z[81]=z[13]*z[10]*z[117]*z[71];
    z[45]=z[81] + z[45] + z[80];
    z[45]=z[13]*z[45];
    z[30]=z[5]*z[30];
    z[30]=z[30] - z[134];
    z[30]=z[53]*z[30];
    z[53]= - z[16] - 6*z[54];
    z[53]=z[5]*z[53];
    z[80]=8*z[1];
    z[53]=z[80] + z[53];
    z[53]=z[5]*z[53];
    z[81]=z[28] - z[1];
    z[84]= - z[10]*z[81];
    z[53]=z[53] + z[84];
    z[53]=z[10]*z[53];
    z[66]=z[127]*z[66];
    z[66]= - z[98] + z[66];
    z[66]=z[66]*z[129];
    z[53]=z[53] + z[66];
    z[53]=z[9]*z[53];
    z[30]=z[45] + z[53] + z[30];
    z[30]=z[12]*z[30];
    z[45]= - z[100] - z[57] - z[31];
    z[45]=z[5]*z[45];
    z[45]= - 17*z[1] + z[45];
    z[45]=z[5]*z[45];
    z[17]= - z[22] - z[17];
    z[17]=z[10]*z[17];
    z[17]= - z[40] + z[17];
    z[17]=z[17]*z[41];
    z[53]=z[5]*z[27];
    z[53]=z[16] + z[53];
    z[53]=z[5]*z[53];
    z[53]= - z[136] + z[53];
    z[53]=z[10]*z[53];
    z[17]=z[17] + z[53] - static_cast<T>(1)+ z[45];
    z[17]=z[10]*z[17];
    z[45]=z[96]*z[133];
    z[45]= - z[22] + z[45];
    z[45]=z[45]*z[41];
    z[53]=z[10]*z[124];
    z[45]=z[45] + z[53] - z[136] + z[28];
    z[45]=z[13]*z[45];
    z[53]=z[104]*z[82];
    z[53]= - z[110] + z[53];
    z[57]=z[41]*z[22];
    z[53]= - 16*z[57] + 2*z[53];
    z[53]=z[10]*z[53];
    z[57]=z[29] + z[106];
    z[57]=z[57]*z[50];
    z[45]=z[45] - 11*z[116] + static_cast<T>(3)+ z[57] + z[53];
    z[45]=z[13]*z[45];
    z[53]=10*z[1] + 9*z[28];
    z[53]=z[5]*z[53];
    z[53]= - static_cast<T>(1)+ z[53];
    z[53]=z[5]*z[53];
    z[17]=z[30] + z[45] + z[39] + z[53] + z[17];
    z[17]=z[12]*z[17];
    z[30]=z[64] + z[31];
    z[39]=z[30]*z[3];
    z[39]=z[39] + z[20];
    z[45]=z[74] + z[39];
    z[45]=z[8]*z[45];
    z[53]= - z[10]*z[88];
    z[45]=z[53] + z[45] + z[38] + z[31];
    z[45]=z[10]*z[45];
    z[53]= - z[1]*z[73];
    z[53]=z[53] - z[49] - 8*z[31];
    z[53]=z[8]*z[53];
    z[57]=z[47] + z[54];
    z[57]=z[5]*z[57];
    z[45]=z[45] + z[53] - z[56] + z[57];
    z[53]=3*z[31];
    z[57]=z[53] + z[64];
    z[66]= - z[3]*z[57];
    z[66]=z[20] + z[66];
    z[66]=z[8]*z[66];
    z[73]=z[59] + 1;
    z[74]=z[39]*z[73];
    z[74]= - z[102] + z[74];
    z[74]=z[13]*z[74];
    z[57]=z[74] + z[66] + z[54] - z[57];
    z[57]=z[13]*z[57];
    z[66]= - z[20]*z[50];
    z[66]=z[66] + z[30];
    z[66]=z[13]*z[66];
    z[66]=z[66] - z[136] - z[75];
    z[66]=z[13]*z[66];
    z[74]=z[9]*z[5];
    z[75]=z[56]*z[74];
    z[84]= - z[13]*z[81];
    z[69]= - z[69] + z[84];
    z[69]=z[13]*z[69];
    z[69]=z[75] + z[69];
    z[69]=z[12]*z[69];
    z[75]=z[49] - z[54];
    z[74]=z[75]*z[74];
    z[75]=z[10]*z[1];
    z[74]=z[75] + z[74];
    z[66]=z[69] + 2*z[74] + z[66];
    z[66]=z[12]*z[66];
    z[69]= - z[58]*z[73];
    z[59]=z[27]*z[59];
    z[59]=z[22] + z[59];
    z[59]=z[10]*z[59];
    z[73]=z[12]*z[81];
    z[73]=z[73] + z[16];
    z[73]=z[9]*z[73];
    z[28]= - z[1] - z[28] + z[73];
    z[28]=z[12]*z[28];
    z[28]=z[28] + z[59] + z[69];
    z[28]=z[14]*z[28];
    z[54]= - z[38] + 7*z[54];
    z[54]=z[9]*z[54];
    z[28]=4*z[28] + z[66] + z[57] + 2*z[45] + z[54];
    z[28]=z[14]*z[28];
    z[45]=z[51] + z[22];
    z[51]=z[3]*z[45];
    z[54]=3*z[25];
    z[51]=z[54] + z[51];
    z[51]=z[3]*z[51];
    z[51]=z[26] + z[51];
    z[51]=z[5]*z[51];
    z[51]=z[51] - z[24];
    z[51]=z[5]*z[51];
    z[51]= - 4*z[20] + z[51];
    z[51]=z[8]*z[51];
    z[57]=z[52] - z[49] - z[124];
    z[57]=z[2]*z[57];
    z[45]=z[57] + z[45];
    z[45]=z[7]*z[45];
    z[57]=z[24]*z[82];
    z[45]=z[45] - z[103] + z[51] - z[16] + z[57];
    z[45]=z[10]*z[45];
    z[51]= - z[107] - z[31];
    z[51]=z[3]*z[51];
    z[51]= - 21*z[20] + z[51];
    z[51]=z[3]*z[51];
    z[57]=z[94] + 2*z[25];
    z[59]= - z[57]*z[78];
    z[59]= - 5*z[26] + z[59];
    z[59]=z[59]*z[50];
    z[51]=z[59] - 20*z[25] + z[51];
    z[51]=z[5]*z[51];
    z[59]=8*z[16];
    z[66]=z[59] - z[31];
    z[66]=z[3]*z[66];
    z[51]=z[51] + z[83] + z[66];
    z[51]=z[5]*z[51];
    z[25]=z[3]*z[25];
    z[25]=2*z[26] + z[25];
    z[25]=z[3]*z[25];
    z[25]=z[63] + z[25];
    z[25]=z[5]*z[25];
    z[25]=z[25] - z[26] + z[36];
    z[25]=z[5]*z[25];
    z[25]=z[35] + z[25];
    z[35]=z[50]*z[8];
    z[25]=z[25]*z[35];
    z[25]=z[25] + z[38] + z[51];
    z[25]=z[8]*z[25];
    z[36]=z[103] - z[118];
    z[36]=z[36]*z[76];
    z[36]=z[36] + z[21] - z[70];
    z[36]=z[2]*z[36];
    z[36]=z[94] + z[36];
    z[36]=z[7]*z[36];
    z[22]=z[36] - z[21] + z[22];
    z[36]=23*z[16];
    z[38]=31*z[52];
    z[51]= - z[38] + z[36] - z[33];
    z[51]=z[51]*z[67];
    z[22]=z[51] + 5*z[22];
    z[22]=z[7]*z[22];
    z[51]= - z[79] + z[31];
    z[22]=z[22] + 2*z[51] + z[38];
    z[22]=z[7]*z[22];
    z[38]=z[59] + z[31];
    z[38]=z[38]*z[3];
    z[51]= - z[54] - z[71];
    z[51]=z[51]*z[50];
    z[51]=z[51] - z[83] - z[38];
    z[51]=z[5]*z[51];
    z[51]= - z[124] + z[51];
    z[51]=z[5]*z[51];
    z[54]=z[1] + 5*z[3];
    z[59]= - static_cast<T>(13)- 6*z[135];
    z[59]=z[2]*z[59];
    z[22]=z[45] + z[22] + z[59] + z[25] + 6*z[54] + z[51];
    z[22]=z[10]*z[22];
    z[25]=z[124] - z[16];
    z[45]=z[47] + z[31];
    z[47]= - z[45]*z[126];
    z[47]=z[47] + z[25];
    z[47]=z[5]*z[47];
    z[51]=z[39]*z[126];
    z[54]= - z[51] - z[27];
    z[54]=z[54]*z[108];
    z[59]= - z[82]*z[94]*z[91];
    z[18]= - z[40] + z[18];
    z[18]=z[2]*z[18];
    z[18]=z[18] + z[59];
    z[18]=z[18]*z[41];
    z[40]=z[31]*z[68];
    z[40]= - z[1] + z[40];
    z[40]=z[5]*z[40];
    z[40]=static_cast<T>(2)+ z[40];
    z[40]=z[2]*z[40];
    z[41]= - z[6]*z[29];
    z[41]=static_cast<T>(1)+ z[41];
    z[41]=z[11]*z[41];
    z[18]=z[41] - 2*z[89] + z[18] + z[40] + z[54] + z[1] + z[47];
    z[18]=z[15]*z[18];
    z[40]=z[124] + z[16];
    z[41]= - z[3]*z[40];
    z[24]= - z[5]*z[24];
    z[24]=z[24] + z[20] + z[41];
    z[24]=z[24]*z[50];
    z[24]=z[24] - 11*z[16] - 16*z[31];
    z[24]=z[5]*z[24];
    z[41]=z[122] - z[20];
    z[47]= - z[51] + z[41];
    z[47]=z[47]*z[108];
    z[24]=z[24] + z[47];
    z[24]=z[8]*z[24];
    z[47]=z[30]*z[78];
    z[47]=5*z[20] + z[47];
    z[47]=z[5]*z[47];
    z[45]= - 2*z[45] + z[47];
    z[45]=z[45]*z[50];
    z[45]= - 21*z[1] + z[45];
    z[45]=z[5]*z[45];
    z[24]=z[45] + z[24];
    z[24]=z[8]*z[24];
    z[45]= - z[49] - z[46];
    z[46]= - z[39]*z[50];
    z[45]=3*z[45] + z[46];
    z[45]=z[5]*z[45];
    z[45]= - z[80] + z[45];
    z[45]=z[5]*z[45];
    z[46]=z[5]*z[39];
    z[46]= - 4*z[58] + z[46];
    z[46]=z[46]*z[108];
    z[45]=z[45] + z[46];
    z[45]=z[8]*z[45];
    z[46]= - z[5]*z[125];
    z[29]= - z[29] + z[46];
    z[29]=z[29]*z[82];
    z[29]=z[29] + z[45];
    z[29]=z[8]*z[29];
    z[45]= - z[40]*z[90];
    z[30]= - z[5]*z[30];
    z[30]= - z[1] + z[30];
    z[30]=z[30]*z[35];
    z[30]=z[45] + z[30];
    z[30]=z[2]*z[30]*z[65];
    z[29]=z[29] + z[30];
    z[29]=z[2]*z[29];
    z[30]= - z[16] + z[53];
    z[30]=z[5]*z[30];
    z[30]= - z[61] + z[30];
    z[30]=z[30]*z[50];
    z[30]= - static_cast<T>(5)+ z[30];
    z[30]=z[5]*z[30];
    z[24]=z[29] + z[30] + z[24];
    z[24]=z[2]*z[24];
    z[29]=z[40]*z[111];
    z[30]=3*z[3];
    z[35]= - z[58]*z[30];
    z[35]= - z[20] + z[35];
    z[35]=z[35]*z[112];
    z[29]=2*z[29] + z[35];
    z[29]=z[29]*z[62];
    z[35]=z[16] + z[131];
    z[35]=z[35]*z[34];
    z[45]=z[53] + z[16];
    z[46]= - z[45]*z[30];
    z[46]=z[20] + z[46];
    z[46]=z[46]*z[113];
    z[29]=z[29] + 2*z[35] + z[46];
    z[29]=z[8]*z[29];
    z[35]= - z[49] + z[124];
    z[46]= - z[113]*z[136];
    z[35]=20*z[52] + 2*z[35] + z[46];
    z[35]=z[2]*z[35];
    z[46]=z[25]*z[78];
    z[32]=z[32]*z[113];
    z[29]=z[35] + z[29] + z[46] - 3*z[32];
    z[29]=z[7]*z[29];
    z[32]= - z[45]*z[34];
    z[35]=z[3]*z[55];
    z[35]=z[20] + z[35];
    z[35]=z[35]*z[113];
    z[32]=z[32] + z[35];
    z[32]=z[32]*z[62];
    z[33]=z[33] + z[64];
    z[35]=z[3]*z[33];
    z[35]=z[20] + z[35];
    z[35]=z[35]*z[126];
    z[32]=z[35] + z[32];
    z[32]=z[8]*z[32];
    z[35]=static_cast<T>(4)- z[126];
    z[25]=z[25]*z[35];
    z[35]=z[5]*z[31];
    z[35]= - z[77] + z[35];
    z[35]=z[2]*z[35];
    z[25]=z[29] + z[35] + z[32] + z[25];
    z[25]=z[7]*z[25];
    z[29]= - z[48]*z[34];
    z[27]=z[27]*z[113];
    z[27]=z[29] + z[27];
    z[27]=z[5]*z[27];
    z[29]=z[40]*z[30];
    z[27]=z[29] + z[27];
    z[29]=z[41]*z[34];
    z[32]= - z[39]*z[112];
    z[29]=z[29] + z[32];
    z[29]=z[29]*z[108];
    z[27]=2*z[27] + z[29];
    z[27]=z[8]*z[27];
    z[29]= - 21*z[16] - 20*z[31];
    z[29]=z[3]*z[29];
    z[21]= - z[21] + z[122];
    z[21]=z[21]*z[126];
    z[21]=z[21] - z[87] + z[29];
    z[21]=z[5]*z[21];
    z[21]=z[27] + z[21] + z[36] + z[86];
    z[21]=z[8]*z[21];
    z[27]=17*z[31];
    z[29]= - z[79] - z[27];
    z[29]=z[5]*z[29];
    z[31]= - z[5]*z[131];
    z[31]=z[56] + z[31];
    z[31]=z[5]*z[31];
    z[31]= - static_cast<T>(16)+ z[31];
    z[31]=z[2]*z[31];
    z[21]=z[25] + z[31] + z[21] + z[29] - z[1] + 16*z[3];
    z[21]=z[7]*z[21];
    z[25]= - z[44] - z[38];
    z[25]=z[3]*z[25];
    z[29]= - z[3]*z[57];
    z[26]= - z[26] + z[29];
    z[26]=z[26]*z[50];
    z[25]=z[25] + z[26];
    z[25]=z[5]*z[25];
    z[25]=z[25] - z[20] - z[122];
    z[25]=z[5]*z[25];
    z[26]= - z[3]*z[41];
    z[29]=z[39]*z[113];
    z[26]=z[26] + z[29];
    z[26]=z[26]*z[108];
    z[25]=z[26] - 2*z[33] + z[25];
    z[25]=z[8]*z[25];
    z[26]=42*z[16] + z[27];
    z[26]=z[3]*z[26];
    z[27]=z[23]*z[78];
    z[27]=z[120] + z[27];
    z[27]=z[27]*z[50];
    z[20]=z[27] + 40*z[20] + z[26];
    z[20]=z[5]*z[20];
    z[20]=z[20] - z[58];
    z[20]=z[5]*z[20];
    z[26]= - z[110] - z[30];
    z[20]=z[25] + 4*z[26] + z[20];
    z[20]=z[8]*z[20];
    z[23]=z[23]*z[72];
    z[16]=z[23] + 35*z[16] + z[123];
    z[16]=z[5]*z[16];
    z[16]=z[16] - z[77] + 7*z[3];
    z[16]=z[5]*z[16];

    r +=  - static_cast<T>(13)+ z[16] + z[17] + 6*z[18] + z[19] + z[20] + z[21] + 
      z[22] + z[24] + z[28] + z[37] + z[42] + z[43] + z[60];
 
    return r;
}

template double qg_2lNLC_r129(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r129(const std::array<dd_real,31>&);
#endif
