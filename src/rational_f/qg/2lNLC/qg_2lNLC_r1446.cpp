#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1446(const std::array<T,31>& k) {
  T z[109];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[3];
    z[7]=k[6];
    z[8]=k[9];
    z[9]=k[5];
    z[10]=k[4];
    z[11]=k[8];
    z[12]=k[10];
    z[13]=k[15];
    z[14]=k[2];
    z[15]=2*z[8];
    z[16]=z[8]*z[6];
    z[17]= - static_cast<T>(1)- n<T>(1,3)*z[16];
    z[17]=z[17]*z[15];
    z[18]=n<T>(2,3)*z[13];
    z[19]=3*z[2];
    z[17]=z[17] - z[18] - z[19];
    z[17]=z[8]*z[17];
    z[20]=12*z[11];
    z[21]=5*z[2];
    z[22]=z[20] - z[21];
    z[23]=14*z[3];
    z[24]=z[23] + z[22];
    z[25]=2*z[3];
    z[24]=z[24]*z[25];
    z[26]=npow(z[7],2);
    z[27]=4*z[26];
    z[28]=npow(z[3],2);
    z[29]=z[28]*z[11];
    z[30]=z[29]*z[9];
    z[31]=z[4]*z[2];
    z[32]=3*z[31];
    z[17]= - 24*z[30] + z[17] + z[24] + z[27] - z[32];
    z[17]=z[9]*z[17];
    z[24]=z[3]*z[10];
    z[33]= - static_cast<T>(7)- 10*z[24];
    z[33]=z[33]*z[25];
    z[34]=3*z[4];
    z[35]=z[26]*z[6];
    z[36]=z[13] - z[19];
    z[37]= - static_cast<T>(1)- 4*z[16];
    z[37]=z[8]*z[37];
    z[17]=z[17] + z[37] + z[33] - z[35] + 2*z[36] + z[34];
    z[17]=z[9]*z[17];
    z[33]=3*z[3];
    z[36]= - static_cast<T>(1)- z[24];
    z[36]=z[36]*z[33];
    z[37]=npow(z[6],2);
    z[38]=z[37]*z[8];
    z[39]=z[38] + z[6];
    z[39]=z[39]*z[8];
    z[39]=z[39] - 1;
    z[40]=n<T>(1,3)*z[8];
    z[41]= - z[39]*z[40];
    z[42]=z[11] + z[33];
    z[42]=z[3]*z[42];
    z[42]=z[42] - z[30];
    z[42]=z[9]*z[42];
    z[36]=z[42] + z[36] + z[41];
    z[41]=2*z[9];
    z[36]=z[36]*z[41];
    z[42]=3*z[10];
    z[43]=npow(z[10],2);
    z[44]=z[43]*z[3];
    z[45]=z[42] + z[44];
    z[45]=z[45]*z[25];
    z[46]=z[6]*z[15];
    z[36]=z[36] + z[46] + static_cast<T>(3)+ z[45];
    z[36]=z[36]*z[41];
    z[45]=n<T>(2,3)*z[6];
    z[46]=z[6]*z[12];
    z[47]=static_cast<T>(1)- z[46];
    z[47]=z[47]*z[45];
    z[48]=3*z[44];
    z[47]= - z[48] - z[42] + z[47];
    z[49]=npow(z[6],3);
    z[50]=n<T>(1,3)*z[49];
    z[51]=static_cast<T>(53)- 16*z[46];
    z[51]=z[51]*z[50];
    z[52]=n<T>(2,3)*z[46];
    z[53]=z[52] - 1;
    z[54]=npow(z[6],4);
    z[55]=z[54]*z[8];
    z[56]=4*z[55];
    z[57]= - z[53]*z[56];
    z[51]=z[51] + z[57];
    z[51]=z[8]*z[51];
    z[57]=2*z[37];
    z[58]=2*z[46];
    z[59]=n<T>(25,3) - z[58];
    z[59]=z[59]*z[57];
    z[51]=z[59] + z[51];
    z[51]=z[8]*z[51];
    z[59]=z[43]*z[25];
    z[42]=z[42] + z[59];
    z[42]=z[3]*z[42];
    z[60]=z[25] + z[2];
    z[60]=z[60]*z[3];
    z[61]=z[8]*z[2];
    z[61]=z[60] + z[61];
    z[61]=z[9]*z[61];
    z[62]=z[25]*z[10];
    z[63]=z[62] + 3;
    z[64]= - z[3]*z[63];
    z[61]=z[61] + z[64] - z[8];
    z[61]=z[9]*z[61];
    z[42]=z[61] - z[16] + static_cast<T>(3)+ z[42];
    z[42]=z[9]*z[42];
    z[61]=z[10] + z[44] - z[6];
    z[42]=z[42] - 3*z[61] - z[38];
    z[42]=z[9]*z[42];
    z[64]=2*z[49];
    z[65]= - z[64] - z[55];
    z[66]=3*z[8];
    z[65]=z[65]*z[66];
    z[42]=z[65] + z[42];
    z[65]=2*z[5];
    z[42]=z[42]*z[65];
    z[36]=z[42] + z[36] + 2*z[47] + z[51];
    z[36]=z[36]*z[65];
    z[42]= - n<T>(71,3) + 20*z[46];
    z[42]=z[42]*z[37];
    z[47]=z[15]*z[49];
    z[51]= - static_cast<T>(7)+ n<T>(22,3)*z[46];
    z[51]=z[51]*z[47];
    z[42]=z[42] + z[51];
    z[42]=z[8]*z[42];
    z[51]= - static_cast<T>(3)+ n<T>(14,3)*z[46];
    z[67]=2*z[6];
    z[68]=z[51]*z[67];
    z[42]=z[68] + z[42];
    z[42]=z[8]*z[42];
    z[68]=z[26]*z[10];
    z[69]= - z[34] + z[68];
    z[69]=z[10]*z[69];
    z[69]=n<T>(4,3) + z[69];
    z[70]=z[10]*z[4];
    z[71]=static_cast<T>(7)+ z[70];
    z[71]=z[10]*z[71];
    z[71]=z[71] + z[59];
    z[71]=z[71]*z[25];
    z[72]=2*z[12] - 9*z[35];
    z[72]=z[6]*z[72];
    z[17]=z[36] + z[17] + z[42] + z[71] + 2*z[69] + z[72];
    z[17]=z[5]*z[17];
    z[36]=7*z[2];
    z[42]=4*z[13];
    z[69]=4*z[8];
    z[71]= - z[69] - 11*z[35] - z[42] - z[36];
    z[71]=z[8]*z[71];
    z[72]=4*z[11];
    z[73]= - z[26]*z[72];
    z[74]=7*z[3];
    z[75]= - z[11]*z[74];
    z[27]= - z[27] + z[75];
    z[27]=z[3]*z[27];
    z[75]=z[8] + z[13];
    z[76]=n<T>(2,3)*z[8];
    z[77]= - z[75]*z[76];
    z[77]=z[26] + z[77];
    z[77]=z[8]*z[77];
    z[27]=z[77] + z[73] + z[27];
    z[73]=4*z[9];
    z[27]=z[27]*z[73];
    z[77]=8*z[3];
    z[22]=z[77] - z[68] + z[22];
    z[22]=z[22]*z[25];
    z[68]=z[11]*z[10];
    z[78]=z[68]*z[26];
    z[22]=z[27] + z[71] + z[22] - z[32] - 2*z[78];
    z[22]=z[9]*z[22];
    z[27]=n<T>(13,3)*z[46];
    z[71]=static_cast<T>(3)- z[27];
    z[71]=z[71]*z[37]*z[15];
    z[79]=static_cast<T>(8)- n<T>(17,3)*z[46];
    z[79]=z[6]*z[79];
    z[71]=z[79] + z[71];
    z[71]=z[8]*z[71];
    z[35]=5*z[35];
    z[79]= - z[12] + z[35];
    z[79]=z[6]*z[79];
    z[71]=z[71] - static_cast<T>(1)+ z[79];
    z[71]=z[8]*z[71];
    z[79]=4*z[24];
    z[80]=z[70] + 1;
    z[81]= - z[79] + z[80];
    z[81]=z[3]*z[81];
    z[82]=z[4] + z[2];
    z[83]=z[82] + z[11];
    z[84]=2*z[13];
    z[71]=z[71] + z[81] - z[84] - z[83];
    z[17]=z[17] + 2*z[71] + z[22];
    z[17]=z[5]*z[17];
    z[22]=n<T>(4,3)*z[16];
    z[71]=static_cast<T>(1)- z[22];
    z[71]=z[8]*z[71];
    z[18]=z[71] + z[18] - z[19];
    z[18]=z[8]*z[18];
    z[71]=npow(z[8],2);
    z[81]=z[71]*z[2];
    z[85]=z[28]*z[2];
    z[86]=4*z[85] - z[81];
    z[86]=z[9]*z[86];
    z[87]=12*z[28];
    z[88]=npow(z[7],3);
    z[89]=z[88]*z[10];
    z[90]=4*z[89];
    z[91]=z[88]*z[6];
    z[18]=3*z[86] + z[18] - z[87] - 5*z[91] - z[31] - z[90];
    z[18]=z[9]*z[18];
    z[86]=z[88]*z[43];
    z[90]= - z[90] - 7*z[91];
    z[90]=z[90]*z[67];
    z[87]=z[10]*z[87];
    z[92]=static_cast<T>(1)- z[16];
    z[92]=z[8]*z[92];
    z[18]=z[18] + z[92] + z[87] + z[90] + z[4] + 4*z[86];
    z[18]=z[9]*z[18];
    z[87]=2*z[89] + 13*z[91];
    z[87]=z[6]*z[87];
    z[87]= - 2*z[86] + z[87];
    z[87]=z[6]*z[87];
    z[90]=2*z[85];
    z[81]=z[90] - z[81];
    z[81]=z[9]*z[81];
    z[92]= - z[2] + z[8];
    z[92]=z[8]*z[92];
    z[60]=z[81] - z[60] + z[92];
    z[60]=z[9]*z[60];
    z[81]=static_cast<T>(1)+ z[62];
    z[81]=z[3]*z[81];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[8]*z[16];
    z[16]=z[60] + z[81] + z[16];
    z[16]=z[9]*z[16];
    z[60]= - z[10] - z[59];
    z[60]=z[3]*z[60];
    z[16]=z[16] + z[60] + z[39];
    z[16]=z[9]*z[16];
    z[39]=z[49]*z[8];
    z[60]=z[37] + z[39];
    z[60]=z[8]*z[60];
    z[16]=z[16] + z[60] + z[61];
    z[16]=z[16]*z[65];
    z[60]= - z[4]*z[43];
    z[60]=z[60] - z[59];
    z[60]=z[60]*z[25];
    z[61]=n<T>(4,3)*z[46];
    z[81]= - static_cast<T>(3)+ z[61];
    z[81]=z[81]*z[37];
    z[92]=z[12]*z[55];
    z[81]=z[81] + n<T>(4,3)*z[92];
    z[81]=z[8]*z[81];
    z[53]=z[6]*z[53];
    z[53]=z[53] + z[81];
    z[53]=z[8]*z[53];
    z[16]=z[16] + z[18] + z[53] + z[60] + z[87] + static_cast<T>(1)- z[70];
    z[16]=z[16]*z[65];
    z[18]=z[88]*z[68];
    z[53]=z[2]*z[23];
    z[53]= - z[89] + z[53];
    z[53]=z[3]*z[53];
    z[18]= - 5*z[18] + z[53];
    z[53]=n<T>(4,3)*z[13];
    z[60]= - z[76] + z[53] - z[19];
    z[60]=z[8]*z[60];
    z[60]=z[91] + z[60];
    z[60]=z[8]*z[60];
    z[81]= - z[11] - z[3];
    z[81]=z[9]*z[88]*z[81];
    z[18]=28*z[81] + 2*z[18] + z[60];
    z[18]=z[9]*z[18];
    z[23]= - z[23] + z[36] - 5*z[86];
    z[23]=z[23]*z[25];
    z[36]=z[37]*z[88];
    z[60]= - z[66] + z[36] - z[42] - z[2];
    z[60]=z[8]*z[60];
    z[66]=2*z[11];
    z[81]= - z[86]*z[66];
    z[18]=z[18] + z[60] + z[23] - 13*z[31] + z[81];
    z[18]=z[9]*z[18];
    z[23]= - z[12] - 7*z[36];
    z[23]=z[23]*z[67];
    z[51]= - z[6]*z[51];
    z[60]=z[12]*z[39];
    z[51]=z[51] - n<T>(22,3)*z[60];
    z[51]=z[8]*z[51];
    z[23]=z[51] - static_cast<T>(1)+ z[23];
    z[23]=z[8]*z[23];
    z[51]=static_cast<T>(5)+ z[70];
    z[51]=z[51]*z[62];
    z[51]=z[51] - static_cast<T>(7)- 3*z[70];
    z[51]=z[51]*z[25];
    z[60]=13*z[4];
    z[16]=z[16] + z[18] + z[23] + z[60] + z[51];
    z[16]=z[5]*z[16];
    z[18]=14*z[11] + z[33];
    z[23]=z[9]*z[3];
    z[18]=z[23]*z[88]*z[18];
    z[51]=z[28]*z[89];
    z[81]=z[76]*z[13];
    z[86]=z[91] + z[81];
    z[86]=z[86]*z[71];
    z[18]=z[18] + z[51] + z[86];
    z[18]=z[18]*z[41];
    z[51]=z[2] + z[13];
    z[86]=2*z[51] - 5*z[36];
    z[86]=z[86]*z[71];
    z[18]=z[18] + 8*z[85] + z[86];
    z[18]=z[18]*z[41];
    z[86]=4*z[3];
    z[87]= - static_cast<T>(2)- z[70];
    z[87]=z[87]*z[86];
    z[87]=9*z[82] + z[87];
    z[87]=z[3]*z[87];
    z[91]=n<T>(2,3)*z[12];
    z[92]= - 11*z[2] - z[91];
    z[92]=z[4]*z[92];
    z[87]=z[92] + z[87];
    z[36]=z[12] + 4*z[36];
    z[36]=z[36]*z[67];
    z[92]=z[12]*z[38];
    z[36]=n<T>(26,3)*z[92] - static_cast<T>(5)+ z[36];
    z[36]=z[8]*z[36];
    z[92]=z[2] - z[13];
    z[93]= - 2*z[92] + z[12];
    z[36]=2*z[93] + z[36];
    z[36]=z[8]*z[36];
    z[16]=z[16] + z[18] + 2*z[87] + z[36];
    z[16]=z[5]*z[16];
    z[18]= - z[68] + z[24];
    z[36]=npow(z[7],4);
    z[18]=z[36]*z[18];
    z[87]=z[36]*z[11];
    z[93]= - z[3]*z[36];
    z[93]= - z[87] + z[93];
    z[93]=z[93]*z[41];
    z[18]=z[93] + z[18];
    z[18]=z[18]*z[41];
    z[93]=z[36]*z[43];
    z[94]= - z[11]*z[93];
    z[95]=6*z[2];
    z[96]= - z[3]*z[95];
    z[96]= - 7*z[93] + z[96];
    z[96]=z[3]*z[96];
    z[75]=z[75]*z[71];
    z[18]=z[18] - n<T>(2,3)*z[75] + z[94] + z[96];
    z[18]=z[9]*z[18];
    z[75]= - z[36]*z[39];
    z[94]= - z[2] + z[3];
    z[94]=z[3]*z[94];
    z[75]=z[75] + z[31] + z[94];
    z[18]=6*z[75] + z[18];
    z[18]=z[9]*z[18];
    z[75]=z[36]*z[10];
    z[94]=2*z[75];
    z[96]=z[36]*z[6];
    z[97]= - z[94] - z[96];
    z[98]=3*z[6];
    z[97]=z[97]*z[98];
    z[99]= - 5*z[75] - z[90];
    z[99]=z[9]*z[99];
    z[100]= - z[2] + z[25];
    z[100]=z[3]*z[100];
    z[97]=z[99] + z[100] + z[31] + z[97];
    z[97]=z[9]*z[97];
    z[99]=6*z[75] + 11*z[96];
    z[99]=z[99]*z[37];
    z[100]=z[62] - 1;
    z[101]=z[100]*z[3];
    z[97]=z[97] - z[101] - z[4] + z[99];
    z[97]=z[9]*z[97];
    z[94]= - z[94] - 5*z[96];
    z[94]=z[94]*z[49];
    z[96]= - z[10]*z[80];
    z[96]=z[96] + z[59];
    z[96]=z[3]*z[96];
    z[94]=z[97] + z[96] + z[70] + z[94];
    z[94]=z[5]*z[94];
    z[96]= - static_cast<T>(3)- z[70];
    z[96]=z[96]*z[24];
    z[96]=3*z[80] + z[96];
    z[96]=z[3]*z[96];
    z[96]= - z[34] + z[96];
    z[97]=7*z[55];
    z[99]=z[36]*z[97];
    z[18]=z[94] + z[18] + 2*z[96] + z[99];
    z[94]=4*z[5];
    z[18]=z[18]*z[94];
    z[96]=2*z[28];
    z[75]=z[96]*z[75];
    z[99]=z[36]*z[86];
    z[87]=15*z[87] + z[99];
    z[87]=z[87]*z[23];
    z[75]=z[75] + z[87];
    z[75]=z[75]*z[73];
    z[87]=z[28]*z[93];
    z[93]= - z[36]*z[37];
    z[81]=z[93] + z[81];
    z[81]=z[81]*z[71];
    z[75]=z[75] + 4*z[87] + z[81];
    z[75]=z[9]*z[75];
    z[81]=z[36]*z[49];
    z[81]=z[84] + 3*z[81];
    z[81]=z[81]*z[71];
    z[81]= - 14*z[85] + z[81];
    z[75]=2*z[81] + z[75];
    z[75]=z[9]*z[75];
    z[81]=static_cast<T>(7)+ 5*z[70];
    z[81]=z[81]*z[25];
    z[81]= - 21*z[82] + z[81];
    z[81]=z[3]*z[81];
    z[81]=21*z[31] + z[81];
    z[87]=z[54]*z[71];
    z[93]=z[36]*z[87];
    z[18]=z[18] + z[75] + 2*z[81] - 9*z[93];
    z[18]=z[5]*z[18];
    z[75]= - z[82]*z[77];
    z[75]=17*z[31] + z[75];
    z[75]=z[3]*z[75];
    z[36]=z[36]*npow(z[9],4)*z[29];
    z[36]=z[75] - 14*z[36];
    z[18]=2*z[36] + z[18];
    z[18]=z[5]*z[18];
    z[36]=z[19] - z[25];
    z[36]=z[3]*z[36];
    z[75]=npow(z[7],5);
    z[81]=z[75]*z[97];
    z[93]=z[9]*z[24];
    z[93]= - 7*z[44] + 5*z[93];
    z[93]=z[9]*z[93];
    z[93]=z[93] - 3*z[39];
    z[93]=z[75]*z[93];
    z[93]=z[90] + z[93];
    z[93]=z[9]*z[93];
    z[36]=z[93] + z[81] - z[32] + z[36];
    z[36]=z[9]*z[36];
    z[81]=z[80]*z[3];
    z[62]= - static_cast<T>(3)+ z[62];
    z[62]=z[62]*z[81];
    z[93]=npow(z[6],5);
    z[97]=z[93]*z[69];
    z[99]= - z[75]*z[97];
    z[36]=z[36] + z[99] + z[34] + z[62];
    z[36]=z[36]*z[65];
    z[62]=z[71]*z[93]*z[75];
    z[99]=3*z[11];
    z[102]=z[99] + z[3];
    z[102]=z[23]*z[75]*z[102];
    z[103]=z[75]*z[28];
    z[104]=z[10]*z[103];
    z[102]=z[104] + z[102];
    z[102]=z[9]*z[102];
    z[103]=z[43]*z[103];
    z[102]=z[103] + z[102];
    z[73]=z[102]*z[73];
    z[102]=z[75]*z[49];
    z[103]=n<T>(4,3)*z[8];
    z[104]=z[13]*z[103];
    z[102]=z[102] + z[104];
    z[102]=z[102]*z[71];
    z[73]=z[102] + z[73];
    z[73]=z[9]*z[73];
    z[87]= - z[75]*z[87];
    z[87]=z[90] + z[87];
    z[73]=6*z[87] + z[73];
    z[73]=z[9]*z[73];
    z[87]=2*z[82];
    z[81]=z[87] - z[81];
    z[81]=z[3]*z[81];
    z[90]=2*z[31];
    z[81]= - z[90] + z[81];
    z[36]=z[36] + z[73] + 12*z[81] + 5*z[62];
    z[36]=z[5]*z[36];
    z[62]=z[82]*z[25];
    z[73]=5*z[31];
    z[81]= - z[73] + z[62];
    z[74]=z[81]*z[74];
    z[75]=z[75]*npow(z[9],5)*z[29];
    z[36]=z[36] + z[74] - 12*z[75];
    z[36]=z[5]*z[36];
    z[28]=z[31]*z[28];
    z[36]= - 8*z[28] + z[36];
    z[36]=z[5]*z[36];
    z[74]=npow(z[7],6);
    z[43]=z[74]*z[43]*npow(z[9],2)*z[96];
    z[75]=z[74]*z[71];
    z[81]= - z[54]*z[75];
    z[43]=z[81] + z[43];
    z[43]=z[9]*z[43];
    z[81]=z[93]*z[75];
    z[81]= - z[85] + z[81];
    z[43]=2*z[81] + z[43];
    z[43]=z[9]*z[43];
    z[81]=z[80]*z[25];
    z[81]= - 5*z[82] + z[81];
    z[81]=z[3]*z[81];
    z[75]= - npow(z[6],6)*z[75];
    z[43]=z[43] + z[75] + z[73] + z[81];
    z[43]=z[5]*z[43];
    z[73]= - z[3]*z[82];
    z[73]=z[32] + z[73];
    z[33]=z[73]*z[33];
    z[73]= - z[74]*npow(z[9],6)*z[29];
    z[33]=z[33] + z[73];
    z[33]=2*z[33] + z[43];
    z[33]=z[5]*z[33];
    z[33]=7*z[28] + z[33];
    z[33]=z[33]*npow(z[5],2);
    z[43]= - 7*z[31] + z[62];
    z[43]=z[5]*z[3]*z[43];
    z[28]= - 6*z[28] + z[43];
    z[28]=z[28]*npow(z[5],3);
    z[43]=z[96]*z[31];
    z[62]=z[1]*npow(z[5],4)*z[43];
    z[28]=z[28] + z[62];
    z[28]=z[1]*z[28];
    z[62]=z[4]*z[13];
    z[73]=z[62]*npow(z[11],3);
    z[74]=n<T>(1,3)*z[12];
    z[75]=z[74]*z[73];
    z[28]=z[28] + z[75] + z[33];
    z[33]=2*z[1];
    z[28]=z[28]*z[33];
    z[75]=z[74]*z[13];
    z[81]=z[13] - z[74];
    z[81]=z[4]*z[81];
    z[81]=z[75] + z[81];
    z[81]=z[81]*z[66];
    z[82]=z[12]*z[13];
    z[85]=z[82]*z[4];
    z[81]= - z[85] + z[81];
    z[96]=npow(z[11],2);
    z[81]=z[81]*z[96];
    z[28]=z[28] + z[81] + z[36];
    z[28]=z[28]*z[33];
    z[33]=z[13]*z[14];
    z[36]= - static_cast<T>(1)+ z[33];
    z[36]=z[12]*z[36];
    z[36]=z[84] + z[36];
    z[81]= - z[70]*z[74];
    z[36]=z[81] + n<T>(1,3)*z[36] - z[4];
    z[36]=z[36]*z[66];
    z[81]= - z[84] + z[74];
    z[81]=z[4]*z[81];
    z[36]=z[36] - z[75] + z[81];
    z[36]=z[11]*z[36];
    z[36]=n<T>(5,3)*z[85] + z[36];
    z[36]=z[11]*z[36];
    z[81]=z[41]*z[73];
    z[36]=z[81] + z[36] + z[43];
    z[18]=z[28] + 2*z[36] + z[18];
    z[18]=z[1]*z[18];
    z[28]= - z[2] + z[15];
    z[28]=z[71]*z[96]*z[28];
    z[36]= - z[29]*z[89];
    z[36]=n<T>(1,3)*z[73] + z[36];
    z[43]=z[88]*z[30];
    z[28]= - 12*z[43] + 4*z[36] + 3*z[28];
    z[28]=z[9]*z[28];
    z[36]= - z[84] + z[19];
    z[36]=z[4]*z[36];
    z[43]=n<T>(1,3)*z[13] - z[4];
    z[43]=z[43]*z[72];
    z[36]=z[36] + z[43];
    z[36]=z[36]*z[96];
    z[43]=2*z[2];
    z[73]=z[11]*z[14];
    z[81]=9*z[73];
    z[84]= - n<T>(4,3) - z[81];
    z[84]=z[8]*z[84];
    z[84]=z[84] - z[43] + z[99];
    z[84]=z[15]*z[11]*z[84];
    z[19]=z[96]*z[19];
    z[19]=z[19] + z[84];
    z[19]=z[8]*z[19];
    z[19]=z[28] + z[36] + z[19];
    z[19]=z[9]*z[19];
    z[28]=3*z[13];
    z[36]= - z[91] + z[28] + z[43];
    z[36]=z[4]*z[36];
    z[36]=n<T>(4,3)*z[82] + z[36];
    z[43]=npow(z[14],2);
    z[84]=z[43]*z[75];
    z[84]=z[84] + n<T>(1,3)*z[33] - z[80];
    z[84]=z[84]*z[72];
    z[88]=z[82]*z[14];
    z[89]= - z[13] + z[88];
    z[34]=z[84] + n<T>(2,3)*z[89] - z[34];
    z[34]=z[11]*z[34];
    z[34]=2*z[36] + z[34];
    z[34]=z[11]*z[34];
    z[36]=z[43]*z[12];
    z[84]=2*z[14];
    z[89]=z[84] - z[36];
    z[102]=3*z[43];
    z[104]=z[12]*npow(z[14],3);
    z[105]=z[102] - z[104];
    z[105]=z[105]*z[99];
    z[89]=n<T>(4,3)*z[89] + z[105];
    z[89]=z[11]*z[89];
    z[105]=z[12]*z[14];
    z[106]=z[58] - z[105];
    z[89]=z[89] - static_cast<T>(1)- z[106];
    z[89]=z[89]*z[15];
    z[107]=static_cast<T>(5)+ z[105];
    z[108]= - z[14]*z[99];
    z[107]=n<T>(2,3)*z[107] + z[108];
    z[107]=z[11]*z[107];
    z[89]=z[107] + z[89];
    z[89]=z[8]*z[89];
    z[107]=z[2] - z[74];
    z[107]=4*z[107] - z[99];
    z[107]=z[11]*z[107];
    z[89]=z[107] + z[89];
    z[89]=z[8]*z[89];
    z[83]=z[3]*z[83];
    z[83]= - z[90] + z[83];
    z[83]=z[83]*z[86];
    z[16]=z[18] + z[16] + z[19] + z[89] + z[83] - n<T>(8,3)*z[85] + z[34];
    z[16]=z[1]*z[16];
    z[18]= - z[102] + 2*z[104];
    z[18]=z[18]*z[99];
    z[19]=4*z[46];
    z[34]=z[19] - static_cast<T>(1)- 2*z[105];
    z[34]=z[6]*z[34];
    z[83]= - z[14] + 2*z[36];
    z[18]=z[34] + n<T>(4,3)*z[83] + z[18];
    z[18]=z[18]*z[15];
    z[34]= - z[26]*z[102];
    z[83]=z[43]*z[74];
    z[85]= - z[84] - z[83];
    z[86]=z[11]*z[43];
    z[85]=2*z[85] - 15*z[86];
    z[85]=z[11]*z[85];
    z[86]=z[26]*z[84];
    z[86]=z[86] + z[12];
    z[35]=2*z[86] - z[35];
    z[35]=z[6]*z[35];
    z[18]=z[18] + z[35] + z[85] - n<T>(10,3)*z[105] - n<T>(37,3) + z[34];
    z[18]=z[8]*z[18];
    z[34]=z[73] + 1;
    z[34]=z[74]*z[34];
    z[34]=11*z[13] + z[34];
    z[18]=2*z[34] + z[18];
    z[18]=z[8]*z[18];
    z[34]=3*z[14];
    z[35]= - z[26]*z[34];
    z[85]=6*z[11];
    z[35]= - z[85] + z[21] + z[35];
    z[35]=z[11]*z[35];
    z[86]=z[26]*z[67];
    z[73]=n<T>(7,3) + 15*z[73];
    z[73]=z[11]*z[73];
    z[73]= - z[103] + z[86] + 10*z[13] + z[73];
    z[73]=z[73]*z[15];
    z[35]=z[73] + 24*z[62] + z[35];
    z[35]=z[8]*z[35];
    z[73]=z[11]*z[4];
    z[73]=z[32] - n<T>(2,3)*z[73];
    z[73]=z[73]*z[96];
    z[26]=z[11]*z[26]*z[77];
    z[26]=z[73] + z[26];
    z[73]=z[13] + z[11];
    z[73]=z[8]*z[73];
    z[73]=6*z[73] + 8*z[62] - 15*z[96];
    z[73]=z[8]*z[73];
    z[77]=z[96]*z[95];
    z[73]=z[77] + z[73];
    z[73]=z[8]*z[73];
    z[26]=2*z[26] + z[73];
    z[26]=z[9]*z[26];
    z[21]=z[42] + z[21];
    z[21]=z[4]*z[21];
    z[73]=2*z[4];
    z[77]= - n<T>(1,3) - z[70];
    z[77]=z[11]*z[77];
    z[77]= - z[73] + z[77];
    z[77]=z[77]*z[72];
    z[21]=z[21] + z[77];
    z[21]=z[11]*z[21];
    z[77]= - z[11]*z[25];
    z[77]=z[78] + z[77];
    z[77]=z[77]*z[25];
    z[21]=z[26] + z[35] + z[21] + z[77];
    z[21]=z[9]*z[21];
    z[26]=z[91]*z[70];
    z[35]= - static_cast<T>(1)+ 2*z[33];
    z[35]=z[12]*z[35];
    z[35]=z[42] + z[35];
    z[42]=n<T>(1,3)*z[11];
    z[77]= - z[14]*z[72];
    z[77]= - static_cast<T>(5)+ z[77];
    z[77]=z[77]*z[42];
    z[35]=z[77] - z[26] + n<T>(1,3)*z[35] - z[73];
    z[35]=z[11]*z[35];
    z[77]=n<T>(5,3)*z[82];
    z[77]=z[77]*z[80];
    z[78]=5*z[13] + z[2];
    z[78]=z[4]*z[78];
    z[77]=z[78] + z[77];
    z[78]= - z[11]*z[100];
    z[78]= - z[87] + z[78];
    z[78]=z[3]*z[78];
    z[35]=z[78] + 2*z[77] + z[35];
    z[16]=z[16] + z[17] + z[21] + 2*z[35] + z[18];
    z[16]=z[1]*z[16];
    z[17]= - z[57] - z[39];
    z[17]=z[17]*z[76];
    z[17]= - z[6] + z[17];
    z[17]=z[8]*z[17];
    z[18]= - z[29]*z[41];
    z[21]=z[72] + z[3];
    z[21]=z[21]*z[3];
    z[18]=z[21] + z[18];
    z[18]=z[9]*z[18];
    z[35]=3*z[24];
    z[57]=static_cast<T>(2)+ z[35];
    z[57]=z[3]*z[57];
    z[18]=z[18] - z[11] + z[57];
    z[18]=z[18]*z[41];
    z[57]= - 6*z[10] - z[44];
    z[57]=z[3]*z[57];
    z[57]= - n<T>(5,3) + z[57];
    z[17]=z[18] + 2*z[57] + z[17];
    z[17]=z[17]*z[41];
    z[18]=n<T>(7,3)*z[10];
    z[44]=5*z[44];
    z[57]=z[44] + z[18] + z[6];
    z[56]=n<T>(49,3)*z[49] + z[56];
    z[56]=z[8]*z[56];
    z[76]=n<T>(35,3)*z[37];
    z[56]=z[76] + z[56];
    z[56]=z[8]*z[56];
    z[17]=z[17] + 2*z[57] + z[56];
    z[17]=z[9]*z[17];
    z[56]=5*z[10];
    z[57]= - z[56] - z[59];
    z[57]=z[3]*z[57];
    z[23]=z[63]*z[23];
    z[23]=z[23] - static_cast<T>(3)+ z[57];
    z[23]=z[9]*z[23];
    z[23]=z[23] + z[44] + z[56] - z[98];
    z[23]=z[9]*z[23];
    z[44]= - 7*z[49] - 3*z[55];
    z[44]=z[8]*z[44];
    z[23]=z[23] - 10*z[37] + z[44];
    z[23]=z[9]*z[23];
    z[44]=z[8]*z[93];
    z[44]=3*z[54] + z[44];
    z[44]=z[8]*z[44];
    z[44]=z[64] + z[44];
    z[23]=3*z[44] + z[23];
    z[23]=z[23]*z[65];
    z[44]= - n<T>(71,3) + z[19];
    z[44]=z[44]*z[54];
    z[49]= - static_cast<T>(1)+ n<T>(1,3)*z[46];
    z[49]=z[49]*z[97];
    z[44]=z[44] + z[49];
    z[44]=z[8]*z[44];
    z[49]= - static_cast<T>(109)+ 14*z[46];
    z[49]=z[49]*z[50];
    z[44]=z[49] + z[44];
    z[44]=z[8]*z[44];
    z[49]=z[10]*z[74];
    z[49]=z[61] - static_cast<T>(9)+ z[49];
    z[49]=z[6]*z[49];
    z[18]=z[18] + z[49];
    z[18]=z[18]*z[67];
    z[17]=z[23] + z[17] + z[18] + z[44];
    z[17]=z[17]*z[65];
    z[18]=4*z[6];
    z[23]= - z[18] - z[38];
    z[23]=z[8]*z[23];
    z[23]= - static_cast<T>(2)+ z[23];
    z[23]=z[23]*z[40];
    z[25]=z[25] + 7*z[11];
    z[25]=z[3]*z[25];
    z[25]=z[25] - 4*z[30];
    z[25]=z[9]*z[25];
    z[20]=6*z[25] + z[23] - z[20] + 5*z[101];
    z[20]=z[20]*z[41];
    z[23]= - 17*z[10] - z[59];
    z[23]=z[3]*z[23];
    z[25]= - z[10]*z[99];
    z[23]=z[23] - static_cast<T>(2)+ z[25];
    z[25]= - z[76] - 6*z[39];
    z[25]=z[8]*z[25];
    z[25]=7*z[6] + z[25];
    z[25]=z[8]*z[25];
    z[20]=z[20] + 2*z[23] + z[25];
    z[20]=z[9]*z[20];
    z[23]=static_cast<T>(7)- n<T>(11,3)*z[46];
    z[23]=z[23]*z[54]*z[15];
    z[25]=static_cast<T>(131)- 46*z[46];
    z[25]=z[25]*z[50];
    z[23]=z[25] + z[23];
    z[23]=z[8]*z[23];
    z[25]=static_cast<T>(31)- n<T>(32,3)*z[46];
    z[25]=z[25]*z[37];
    z[23]=z[25] + z[23];
    z[23]=z[8]*z[23];
    z[25]=static_cast<T>(5)- z[58];
    z[25]=z[25]*z[45];
    z[25]=z[25] + z[48];
    z[17]=z[17] + z[20] + 2*z[25] + z[23];
    z[17]=z[5]*z[17];
    z[20]= - z[26] - n<T>(1,3)*z[4] - z[91] - z[92];
    z[20]=z[10]*z[20];
    z[20]= - z[35] + z[52] - z[68] - n<T>(1,3) + z[20];
    z[23]= - z[71]*z[67];
    z[23]=z[13] + z[23];
    z[23]=z[23]*z[103];
    z[21]= - 56*z[30] + z[23] + z[31] + 20*z[21];
    z[21]=z[9]*z[21];
    z[23]=9*z[11];
    z[25]= - static_cast<T>(7)+ z[79];
    z[25]=z[3]*z[25];
    z[25]=z[25] - z[23] - n<T>(5,3)*z[4] + z[51];
    z[26]=n<T>(17,3)*z[6] + 3*z[38];
    z[26]=z[26]*z[15];
    z[26]=n<T>(11,3) + z[26];
    z[26]=z[8]*z[26];
    z[21]=z[21] + 2*z[25] + z[26];
    z[21]=z[9]*z[21];
    z[25]= - static_cast<T>(25)+ n<T>(28,3)*z[46];
    z[25]=z[25]*z[37];
    z[26]= - static_cast<T>(6)+ z[27];
    z[26]=z[26]*z[47];
    z[25]=z[25] + z[26];
    z[25]=z[8]*z[25];
    z[19]= - static_cast<T>(23)+ z[19];
    z[19]=z[19]*z[45];
    z[19]=z[19] + z[25];
    z[19]=z[8]*z[19];
    z[17]=z[17] + z[21] + 2*z[20] + z[19];
    z[17]=z[5]*z[17];
    z[19]=z[85] + z[91];
    z[19]=z[43]*z[19];
    z[20]= - z[46] + static_cast<T>(7)+ n<T>(4,3)*z[105];
    z[20]=z[20]*z[67];
    z[21]=static_cast<T>(2)- z[106];
    z[21]=z[6]*z[21];
    z[25]= - z[14] - z[36];
    z[21]=n<T>(4,3)*z[25] + z[21];
    z[21]=z[6]*z[21];
    z[21]= - 3*z[104] + z[21];
    z[21]=z[21]*z[15];
    z[19]=z[21] + z[20] - 19*z[14] + z[19];
    z[19]=z[8]*z[19];
    z[20]=z[14] - z[83];
    z[20]=z[11]*z[20];
    z[19]=z[19] - z[61] + 8*z[20] - n<T>(8,3)*z[105] + n<T>(17,3) + 14*z[33];
    z[19]=z[8]*z[19];
    z[20]= - z[70]*z[72];
    z[20]= - z[60] + z[20];
    z[20]=z[20]*z[42];
    z[21]=z[53] + z[2];
    z[21]=z[4]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[11]*z[20];
    z[21]=4*z[4];
    z[25]=z[28] + z[21];
    z[26]= - z[99] - z[25];
    z[26]=z[26]*z[15];
    z[26]=9*z[96] + z[26];
    z[26]=z[8]*z[26];
    z[27]=z[9]*z[96]*z[32];
    z[20]=z[27] + z[26] + z[20] - 20*z[29];
    z[20]=z[9]*z[20];
    z[26]= - static_cast<T>(1)- z[68];
    z[26]=z[11]*z[26];
    z[26]=z[26] + z[13] - z[73];
    z[26]=z[26]*z[42];
    z[26]= - z[62] + z[26];
    z[27]= - z[24]*z[72];
    z[23]=z[23] + z[27];
    z[23]=z[3]*z[23];
    z[23]=2*z[26] + z[23];
    z[22]= - z[22] - static_cast<T>(4)+ 3*z[33];
    z[22]=z[22]*z[15];
    z[26]= - n<T>(28,3) - z[81];
    z[26]=z[11]*z[26];
    z[22]=z[22] - 4*z[25] + z[26];
    z[22]=z[8]*z[22];
    z[20]=z[20] + 2*z[23] + z[22];
    z[20]=z[9]*z[20];
    z[22]=z[13]*z[43];
    z[22]=z[84] + z[22];
    z[22]=z[12]*z[22];
    z[23]= - z[14] + z[10];
    z[23]=z[23]*z[66];
    z[25]= - z[12] - z[4];
    z[25]=z[10]*z[25];
    z[22]=z[23] + z[25] + z[22] - static_cast<T>(2)+ z[33];
    z[22]=z[24] + n<T>(2,3)*z[22];
    z[22]=z[11]*z[22];
    z[23]= - z[75]*z[80];
    z[23]= - z[62] + z[23];
    z[23]=z[10]*z[23];
    z[24]= - 13*z[13] + 10*z[88];
    z[21]=2*z[23] + n<T>(1,3)*z[24] - z[21] + z[22];
    z[16]=z[16] + z[17] + z[20] + 2*z[21] + z[19];
    z[16]=z[1]*z[16];
    z[17]=z[34] - z[18];
    z[15]=z[17]*z[15];
    z[17]=z[99] + z[69];
    z[17]=z[9]*z[17];
    z[18]=z[6] - z[9];
    z[18]=z[18]*z[94];

    r +=  - static_cast<T>(3)+ z[15] + z[16] + z[17] + z[18];
 
    return r;
}

template double qg_2lNLC_r1446(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1446(const std::array<dd_real,31>&);
#endif
