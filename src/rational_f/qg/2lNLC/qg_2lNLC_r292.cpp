#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r292(const std::array<T,31>& k) {
  T z[135];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[9];
    z[4]=k[14];
    z[5]=k[10];
    z[6]=k[12];
    z[7]=k[15];
    z[8]=k[13];
    z[9]=k[2];
    z[10]=k[5];
    z[11]=k[6];
    z[12]=k[3];
    z[13]=k[4];
    z[14]=k[7];
    z[15]=k[22];
    z[16]=2*z[14];
    z[17]=z[4] - z[16];
    z[17]=z[6]*z[17];
    z[18]=z[14]*z[15];
    z[19]=npow(z[11],3);
    z[20]=z[19]*z[12];
    z[17]=z[20] - z[18] + z[17];
    z[21]=3*z[3];
    z[22]= - z[14] - z[3];
    z[22]=z[22]*z[21];
    z[17]=2*z[17] + z[22];
    z[17]=z[8]*z[17];
    z[22]=npow(z[15],2);
    z[23]= - z[22]*z[16];
    z[24]=z[19]*z[13];
    z[25]=z[2]*z[7];
    z[26]= - n<T>(11,2)*z[24] + z[25];
    z[26]=z[2]*z[26];
    z[27]=5*z[7];
    z[28]=13*z[6];
    z[29]=z[27] + z[28];
    z[30]=npow(z[4],2);
    z[31]=n<T>(1,2)*z[30];
    z[29]=z[29]*z[31];
    z[32]=z[3] + z[7];
    z[33]=z[16] + z[32];
    z[33]=z[3]*z[33];
    z[33]=z[20] + z[33];
    z[33]=z[33]*z[21];
    z[17]=z[17] + z[33] + z[29] + z[23] + z[26];
    z[17]=z[8]*z[17];
    z[23]= - z[7]*z[21];
    z[23]=z[20] + z[23];
    z[26]=npow(z[3],2);
    z[29]=3*z[26];
    z[23]=z[23]*z[29];
    z[33]=npow(z[4],3);
    z[34]=z[33]*z[7];
    z[35]=npow(z[6],3);
    z[36]=z[14]*z[35];
    z[17]=z[17] + z[23] + z[36] + 7*z[34];
    z[17]=z[8]*z[17];
    z[23]=npow(z[14],2);
    z[36]=z[23]*z[19];
    z[37]=2*z[2];
    z[38]= - n<T>(37,12)*z[14] + z[37];
    z[38]=z[2]*z[19]*z[38];
    z[39]=npow(z[2],2);
    z[40]=z[39]*z[14];
    z[41]=npow(z[6],2);
    z[42]= - z[41]*z[40];
    z[38]=z[42] - n<T>(11,2)*z[36] + z[38];
    z[38]=z[6]*z[38];
    z[42]=n<T>(5,2)*z[36];
    z[43]=z[19]*z[14];
    z[44]= - z[37]*z[43];
    z[44]=z[42] + z[44];
    z[44]=z[2]*z[44];
    z[45]=npow(z[8],3);
    z[46]=z[45] - z[33];
    z[46]=z[14]*z[46];
    z[47]=z[30]*z[6];
    z[48]=z[3]*z[47];
    z[46]=z[48] + z[46];
    z[46]=z[29]*z[46];
    z[48]=z[19]*z[47];
    z[38]= - n<T>(1,4)*z[48] + z[44] + z[38] + z[46];
    z[38]=z[10]*z[38];
    z[44]=z[2]*z[13];
    z[46]=z[19]*z[44]*z[16];
    z[48]= - z[13]*z[42];
    z[46]=z[48] + z[46];
    z[46]=z[2]*z[46];
    z[48]=z[39]*z[16];
    z[49]=z[6]*z[2];
    z[50]=n<T>(1,3)*z[7];
    z[51]=z[2] + z[50] - z[14];
    z[51]=z[51]*z[49];
    z[51]=z[48] + z[51];
    z[51]=z[6]*z[51];
    z[52]=3*z[12];
    z[36]=z[36]*z[52];
    z[53]=npow(z[2],3);
    z[54]=z[53]*z[14];
    z[55]=n<T>(1,3)*z[54];
    z[51]=z[51] + z[36] - z[55];
    z[51]=z[6]*z[51];
    z[56]=z[6]*z[13];
    z[57]=n<T>(1,2)*z[56];
    z[58]=z[9]*z[7];
    z[59]=11*z[58] - z[57];
    z[59]=z[31]*z[19]*z[59];
    z[60]=3*z[6];
    z[61]=z[16] + z[60];
    z[61]=z[61]*z[30];
    z[62]=z[6]*z[7];
    z[60]= - z[60] + z[4];
    z[60]=z[4]*z[60];
    z[60]=3*z[62] + z[60];
    z[60]=z[3]*z[60];
    z[63]= - z[6]*z[20];
    z[60]=z[60] + z[63] + z[61];
    z[60]=z[3]*z[60];
    z[61]=4*z[14];
    z[63]= - z[33]*z[61];
    z[60]=z[63] + z[60];
    z[60]=z[60]*z[21];
    z[17]=z[38] + z[17] + z[60] + z[59] + z[46] + z[51];
    z[17]=z[10]*z[17];
    z[38]=npow(z[13],2);
    z[46]=3*z[38];
    z[51]=npow(z[12],2);
    z[59]=2*z[51];
    z[60]= - z[46] - z[59];
    z[60]=z[19]*z[60];
    z[60]=z[4] + z[6] + z[14] + z[15] + z[60];
    z[63]= - z[3]*z[12];
    z[63]=static_cast<T>(1)+ z[63];
    z[63]=z[63]*z[21];
    z[60]=2*z[60] + z[63];
    z[60]=z[8]*z[60];
    z[18]=z[18] - z[22];
    z[63]=z[38]*z[2];
    z[64]=z[19]*z[63];
    z[65]=n<T>(7,2)*z[6];
    z[66]=9*z[4];
    z[67]=z[65] + z[66];
    z[67]=z[4]*z[67];
    z[68]=z[51]*z[19];
    z[69]=z[68] - z[16];
    z[69]=2*z[69] - z[3];
    z[69]=z[69]*z[21];
    z[70]=z[6]*z[14];
    z[60]=z[60] + z[69] + z[67] - 15*z[70] - 2*z[18] + n<T>(27,2)*z[64];
    z[60]=z[8]*z[60];
    z[64]=z[22]*z[14];
    z[67]=z[7]*z[39];
    z[67]=z[64] + z[67];
    z[69]= - z[16] - z[6];
    z[69]=z[69]*z[41];
    z[71]=n<T>(1,2)*z[7];
    z[72]= - z[71] - z[6];
    z[72]=11*z[72] + 7*z[4];
    z[72]=z[72]*z[30];
    z[73]=2*z[7];
    z[74]= - z[73] + z[14];
    z[74]=z[74]*z[29];
    z[60]=z[60] + z[74] + z[72] + 2*z[67] + z[69];
    z[60]=z[8]*z[60];
    z[67]=n<T>(7,3)*z[14];
    z[69]=n<T>(1,3)*z[2];
    z[72]=z[69] - z[73] - z[67];
    z[72]=z[72]*z[39];
    z[74]=9*z[7];
    z[75]=3*z[2];
    z[76]= - z[75] + z[74] + z[16];
    z[76]=z[2]*z[76];
    z[77]=n<T>(2,3)*z[7];
    z[78]= - z[13]*z[39];
    z[78]= - z[77] + z[78];
    z[78]=z[6]*z[78];
    z[76]=z[76] + z[78];
    z[76]=z[6]*z[76];
    z[72]=z[72] + z[76];
    z[72]=z[6]*z[72];
    z[76]=z[2]*z[14];
    z[18]=6*z[62] + z[76] - z[18];
    z[78]=npow(z[9],2);
    z[79]=z[78]*z[19];
    z[80]=3*z[4];
    z[81]= - z[80] + 8*z[6];
    z[82]= - 6*z[79] - z[14] - z[81];
    z[82]=z[82]*z[80];
    z[83]=2*z[9];
    z[84]=z[83]*z[4];
    z[85]= - static_cast<T>(3)- z[84];
    z[85]=z[85]*z[80];
    z[86]=z[2] - z[15];
    z[85]= - 2*z[86] + z[85];
    z[85]=z[3]*z[85];
    z[18]=z[85] + 2*z[18] + z[82];
    z[18]=z[3]*z[18];
    z[48]=z[64] - z[48];
    z[64]=z[65] + 3*z[79] + 8*z[14];
    z[65]=3*z[30];
    z[64]=z[64]*z[65];
    z[18]=z[18] + 2*z[48] + z[64];
    z[18]=z[3]*z[18];
    z[42]=z[63]*z[42];
    z[48]=z[71]*z[79];
    z[64]=6*z[14];
    z[82]= - z[4]*z[64];
    z[48]=z[48] + z[82];
    z[48]=z[48]*z[80];
    z[82]=2*z[35];
    z[48]= - z[82] + z[48];
    z[48]=z[4]*z[48];
    z[17]=z[17] + z[60] + z[18] + z[48] + z[42] + z[72];
    z[17]=z[10]*z[17];
    z[18]= - z[38]*z[55];
    z[33]=z[33]*z[78];
    z[42]=z[73]*z[33];
    z[18]=z[18] + z[42];
    z[42]=npow(z[11],4);
    z[18]=z[42]*z[18];
    z[48]=z[42]*z[3];
    z[60]=z[78]*z[30]*z[48];
    z[72]= - z[42]*z[33];
    z[60]=z[72] + 4*z[60];
    z[60]=z[60]*z[21];
    z[72]=z[42]*z[63];
    z[85]=z[59]*z[48];
    z[72]=z[85] + z[72] - z[47];
    z[72]=z[8]*z[72];
    z[85]=z[51]*z[42];
    z[87]= - z[3]*z[7];
    z[85]=z[85] + z[87];
    z[85]=z[85]*z[26];
    z[34]=z[72] - z[34] + z[85];
    z[72]=npow(z[8],2);
    z[34]=z[34]*z[72];
    z[85]=z[42]*z[23];
    z[87]= - z[16] + z[69];
    z[87]=z[2]*z[42]*z[87];
    z[87]=n<T>(5,2)*z[85] + z[87];
    z[87]=z[87]*z[49];
    z[88]= - z[42]*z[55];
    z[87]=z[88] + z[87];
    z[87]=z[10]*z[87];
    z[53]= - z[53]*z[56];
    z[88]=z[13]*z[54];
    z[53]=z[88] + z[53];
    z[53]=z[42]*z[53];
    z[53]=n<T>(1,3)*z[53] + z[87];
    z[53]=z[10]*z[53];
    z[18]=z[53] + 3*z[34] + z[60] + z[18];
    z[18]=z[10]*z[18];
    z[34]=npow(z[13],3);
    z[53]=z[34]*z[2];
    z[60]=z[42]*z[53];
    z[87]=10*z[6];
    z[88]=z[87] - z[80];
    z[88]=z[4]*z[88];
    z[89]=npow(z[12],3);
    z[90]=z[89]*z[48];
    z[60]= - 12*z[90] - 4*z[60] + z[88];
    z[60]=z[8]*z[60];
    z[88]=z[28] - z[80];
    z[88]=z[88]*z[30];
    z[60]=z[88] + z[60];
    z[60]=z[60]*z[72];
    z[88]=z[35]*z[25];
    z[90]=npow(z[3],3);
    z[91]= - z[90]*z[65];
    z[18]=z[18] + z[60] + z[88] + z[91];
    z[18]=z[10]*z[18];
    z[60]=npow(z[11],5);
    z[88]=z[60]*z[34]*z[39];
    z[91]=z[60]*z[29];
    z[92]=z[89]*z[91];
    z[88]=2*z[88] + z[92];
    z[88]=z[88]*z[45];
    z[33]= - z[91]*z[33];
    z[55]= - z[6]*z[60]*npow(z[10],2)*z[55];
    z[33]=z[33] + z[55];
    z[33]=z[10]*z[33];
    z[33]=z[88] + z[33];
    z[33]=z[10]*z[33];
    z[55]=npow(z[12],4);
    z[60]= - z[55]*z[60]*z[26];
    z[60]= - z[47] + z[60];
    z[60]=z[60]*z[45];
    z[33]=6*z[60] + z[33];
    z[33]=z[10]*z[33];
    z[60]=z[5]*z[7];
    z[88]=z[35]*z[60]*z[69];
    z[92]=z[75]*z[30];
    z[93]= - z[90]*z[92];
    z[94]=3*z[47];
    z[45]= - z[45]*z[94];
    z[45]=z[45] + z[88] + z[93];
    z[45]=z[1]*z[45];
    z[88]=z[3]*z[4];
    z[93]=z[37]*z[9];
    z[93]=z[93] - 1;
    z[95]= - z[4]*z[93];
    z[95]= - z[2] + z[95];
    z[95]=z[95]*z[88];
    z[92]=z[92] + z[95];
    z[92]=z[92]*z[29];
    z[95]=z[7] - n<T>(1,3)*z[5];
    z[95]=z[2]*z[95];
    z[95]= - n<T>(1,3)*z[60] + z[95];
    z[95]=z[6]*z[95];
    z[96]=n<T>(11,3)*z[60];
    z[97]=z[2]*z[96];
    z[95]=z[97] + z[95];
    z[95]=z[95]*z[41];
    z[97]=npow(z[12],5);
    z[91]=z[97]*z[91];
    z[81]=z[4]*z[81];
    z[81]=z[81] + z[91];
    z[81]=z[8]*z[81];
    z[81]=n<T>(13,2)*z[47] + z[81];
    z[72]=z[81]*z[72];
    z[33]=z[45] + z[33] + z[72] + z[95] + z[92];
    z[33]=z[1]*z[33];
    z[45]=z[80]*z[13];
    z[72]=z[13]*z[5];
    z[81]=static_cast<T>(1)- z[72];
    z[81]=z[81]*z[45];
    z[91]=6*z[44];
    z[81]=z[81] + z[91] + n<T>(1,2) + 6*z[72];
    z[81]=z[4]*z[81];
    z[92]=3*z[5];
    z[95]=2*z[6];
    z[81]=z[81] - z[92] - z[95];
    z[81]=z[4]*z[81];
    z[42]= - z[55]*z[42]*z[29];
    z[98]=z[13] - z[63];
    z[98]=z[98]*z[80];
    z[98]=static_cast<T>(5)+ z[98];
    z[98]=z[4]*z[98];
    z[48]=z[55]*z[48];
    z[99]=5*z[6];
    z[48]=6*z[48] - z[99] + z[98];
    z[48]=z[8]*z[48];
    z[42]=z[48] + z[81] + z[42];
    z[42]=z[8]*z[42];
    z[48]= - z[75] - n<T>(11,2)*z[6];
    z[48]=z[48]*z[30];
    z[42]=z[48] + z[42];
    z[42]=z[8]*z[42];
    z[48]=z[72]*z[7];
    z[81]= - static_cast<T>(1)- n<T>(1,3)*z[72];
    z[81]=z[2]*z[81];
    z[81]=z[81] - z[7] - n<T>(1,3)*z[48];
    z[81]=z[6]*z[81];
    z[98]=10*z[7];
    z[100]=z[98] - n<T>(11,3)*z[5];
    z[100]=z[2]*z[100];
    z[81]=z[81] - z[96] + z[100];
    z[81]=z[6]*z[81];
    z[96]=n<T>(38,3)*z[60] - z[25];
    z[96]=z[2]*z[96];
    z[81]=z[96] + z[81];
    z[81]=z[6]*z[81];
    z[96]=4*z[9];
    z[100]=z[96]*z[2];
    z[101]=z[100] - 3;
    z[102]=z[4]*z[101];
    z[102]=z[75] + z[102];
    z[102]=z[4]*z[102];
    z[103]=z[78]*z[2];
    z[104]=z[103] - z[83];
    z[105]= - z[4]*z[104];
    z[93]= - 2*z[93] + z[105];
    z[93]=z[4]*z[93];
    z[93]=z[37] + z[93];
    z[93]=z[3]*z[93];
    z[93]=z[102] + z[93];
    z[93]=z[3]*z[93];
    z[102]=z[2]*z[30];
    z[93]= - n<T>(7,2)*z[102] + z[93];
    z[93]=z[93]*z[21];
    z[85]=npow(z[13],4)*z[60]*z[85];
    z[102]=n<T>(1,2)*z[4];
    z[105]=z[14]*z[102];
    z[105]=z[23] + z[105];
    z[105]=z[4]*z[2]*z[105];
    z[18]=z[33] + z[18] + z[42] + z[93] + n<T>(5,2)*z[105] + n<T>(1,2)*z[85] + 
    z[81];
    z[18]=z[1]*z[18];
    z[33]=2*z[13];
    z[42]=z[52] + z[33];
    z[24]=z[42]*z[24];
    z[42]=2*z[15];
    z[24]= - z[95] - z[42] + z[24];
    z[24]=z[13]*z[24];
    z[81]=z[89]*z[19];
    z[85]= - static_cast<T>(2)+ z[81];
    z[93]=11*z[63];
    z[105]= - z[33] + z[93];
    z[105]=z[4]*z[105];
    z[106]=z[51]*z[3];
    z[107]=z[12] - z[106];
    z[107]=z[107]*z[21];
    z[24]=z[107] + z[105] + 2*z[85] + z[24];
    z[24]=z[8]*z[24];
    z[68]= - z[42] + n<T>(5,2)*z[68];
    z[68]=z[5]*z[68];
    z[20]=z[72]*z[20];
    z[85]=2*z[22];
    z[20]=n<T>(5,4)*z[20] - z[85] + z[68];
    z[20]=z[13]*z[20];
    z[68]= - static_cast<T>(9)+ n<T>(17,2)*z[72];
    z[68]=z[13]*z[68];
    z[68]=z[68] + n<T>(1,2)*z[63];
    z[68]=z[4]*z[68];
    z[68]=z[68] - n<T>(37,2)*z[44] + static_cast<T>(9)- 17*z[72];
    z[68]=z[4]*z[68];
    z[105]=static_cast<T>(1)- z[81];
    z[107]=z[89]*z[5];
    z[108]= - z[29]*z[107];
    z[109]=z[5]*z[12];
    z[105]=z[108] + 9*z[105] - n<T>(1,2)*z[109];
    z[105]=z[3]*z[105];
    z[81]=static_cast<T>(11)+ n<T>(5,2)*z[81];
    z[108]=n<T>(1,2)*z[5];
    z[81]=z[81]*z[108];
    z[110]=static_cast<T>(17)- z[72];
    z[110]=z[6]*z[110];
    z[20]=z[24] + z[105] + z[68] + n<T>(1,2)*z[110] + z[20] + z[42] + z[81];
    z[20]=z[8]*z[20];
    z[24]=3*z[14];
    z[68]=z[24]*z[15];
    z[68]=z[68] - z[85];
    z[81]=z[22]*z[72];
    z[105]=5*z[5];
    z[110]= - z[15]*z[105];
    z[81]=z[81] + z[110] + z[68];
    z[110]=static_cast<T>(5)+ z[72];
    z[111]=2*z[72];
    z[112]=z[111] + 7;
    z[112]=z[112]*z[13];
    z[113]= - z[4]*z[112];
    z[110]=z[113] + n<T>(1,2)*z[110] - z[44];
    z[110]=z[4]*z[110];
    z[113]=5*z[2];
    z[114]=z[5] + z[113];
    z[114]=3*z[114] - z[28];
    z[110]=n<T>(1,2)*z[114] + z[110];
    z[110]=z[4]*z[110];
    z[114]=4*z[72];
    z[115]=z[56] + static_cast<T>(3)- z[114];
    z[115]=z[6]*z[115];
    z[67]=z[115] - n<T>(29,2)*z[5] - z[67];
    z[67]=z[6]*z[67];
    z[115]=9*z[14];
    z[116]=6*z[109];
    z[117]=z[3]*z[116];
    z[117]=z[117] - 7*z[5] - z[115];
    z[117]=z[3]*z[117];
    z[118]= - z[2]*z[5];
    z[20]=z[20] + z[117] + z[110] + z[67] + 2*z[81] + z[118];
    z[20]=z[8]*z[20];
    z[67]=z[72]*z[14];
    z[81]= - z[61] - z[67];
    z[45]=z[81]*z[45];
    z[81]=z[13]*z[14];
    z[110]=static_cast<T>(3)- z[81];
    z[110]=z[2]*z[110];
    z[117]=static_cast<T>(8)+ n<T>(3,2)*z[72];
    z[117]=z[6]*z[117];
    z[45]=z[45] + z[117] + n<T>(5,2)*z[110] + n<T>(115,4)*z[14] + 12*z[67];
    z[45]=z[4]*z[45];
    z[110]=9*z[5];
    z[117]=n<T>(5,2)*z[14];
    z[118]= - z[110] - z[117];
    z[118]=z[14]*z[118];
    z[119]=z[23]*z[13];
    z[120]= - n<T>(3,4)*z[14] - z[119];
    z[120]=z[120]*z[113];
    z[121]= - static_cast<T>(2)- z[72];
    z[121]=z[121]*z[56];
    z[122]=z[72] + 1;
    z[121]= - 2*z[122] + z[121];
    z[121]=z[121]*z[95];
    z[121]= - n<T>(13,2)*z[5] + z[121];
    z[121]=z[6]*z[121];
    z[45]=z[45] + z[121] + z[118] + z[120];
    z[45]=z[4]*z[45];
    z[118]=z[9]*z[15];
    z[120]= - static_cast<T>(3)+ z[118];
    z[120]=z[120]*z[83];
    z[120]=z[52] + z[120];
    z[120]=z[5]*z[120];
    z[121]=z[78]*z[5];
    z[123]=z[96] + 7*z[121];
    z[123]=z[2]*z[123];
    z[124]=z[78]*z[4];
    z[104]= - 3*z[104] + z[124];
    z[104]=z[104]*z[80];
    z[125]=z[42]*z[9];
    z[125]=z[125] - 3;
    z[104]=z[104] + z[123] - 2*z[125] + z[120];
    z[104]=z[3]*z[104];
    z[79]=z[52]*z[79];
    z[120]=z[22]*z[9];
    z[123]=z[120] - z[15];
    z[126]=z[9]*z[123];
    z[126]= - static_cast<T>(3)+ z[126];
    z[79]=2*z[126] + z[79];
    z[79]=z[5]*z[79];
    z[126]=4*z[2];
    z[127]=z[5]*z[9];
    z[128]= - static_cast<T>(4)- z[127];
    z[126]=z[128]*z[126];
    z[128]=z[103] - z[96];
    z[129]=z[4]*z[128];
    z[101]=2*z[101] + z[129];
    z[101]=z[101]*z[80];
    z[79]=z[104] + z[101] + z[126] - 4*z[123] + z[79];
    z[79]=z[3]*z[79];
    z[101]= - 3*z[15] + z[120];
    z[101]=z[5]*z[101];
    z[101]=z[101] + z[68];
    z[104]=static_cast<T>(3)- z[127];
    z[104]=z[104]*z[37];
    z[104]=z[104] + n<T>(23,3)*z[5] - z[64];
    z[104]=z[2]*z[104];
    z[123]= - z[9]*z[75];
    z[123]=n<T>(7,2) + z[123];
    z[123]=z[4]*z[123];
    z[123]=z[123] - n<T>(13,2)*z[6] - z[61] - n<T>(3,2)*z[2];
    z[123]=z[123]*z[80];
    z[79]=z[79] + z[123] + n<T>(13,3)*z[62] + 2*z[101] + z[104];
    z[79]=z[3]*z[79];
    z[101]=n<T>(9,2)*z[60];
    z[19]= - z[19]*z[101];
    z[104]=z[7] + z[5];
    z[43]=z[104]*z[43];
    z[19]=z[19] + z[43];
    z[43]=n<T>(1,2)*z[14];
    z[19]=z[13]*z[19]*z[43];
    z[36]= - z[5]*z[36];
    z[19]=z[36] + z[19];
    z[19]=z[19]*z[38];
    z[36]=6*z[5];
    z[104]= - z[36] - z[69];
    z[104]=z[2]*z[104];
    z[104]=z[104] + n<T>(28,3)*z[60] - n<T>(5,2)*z[23];
    z[104]=z[2]*z[104];
    z[122]= - z[122]*z[33];
    z[111]=z[111] - 1;
    z[123]= - z[111]*z[63];
    z[122]=z[122] + z[123];
    z[122]=z[2]*z[122];
    z[123]=z[13]*z[7];
    z[122]= - n<T>(1,3)*z[123] + z[122];
    z[122]=z[6]*z[122];
    z[111]=z[111]*z[44];
    z[111]= - 4*z[111] - static_cast<T>(13)- n<T>(14,3)*z[72];
    z[111]=z[2]*z[111];
    z[111]=z[122] + z[111] - n<T>(28,3)*z[7] - 3*z[48];
    z[111]=z[6]*z[111];
    z[122]= - n<T>(1,3)*z[44] + n<T>(19,3) - 12*z[72];
    z[122]=z[2]*z[122];
    z[126]= - 20*z[14] + 91*z[7] - 22*z[5];
    z[122]=n<T>(1,3)*z[126] + z[122];
    z[122]=z[2]*z[122];
    z[111]=z[111] - 12*z[60] + z[122];
    z[111]=z[6]*z[111];
    z[122]=z[5]*z[85];
    z[17]=z[18] + z[17] + z[20] + z[79] + z[45] + z[111] + z[104] + 
    z[122] + z[19];
    z[17]=z[1]*z[17];
    z[18]=z[26]*z[52];
    z[18]=z[18] - z[71] + z[115];
    z[18]=z[3]*z[18];
    z[19]=n<T>(1,4)*z[7];
    z[20]= - z[42] + z[19];
    z[20]=z[14]*z[20];
    z[45]=3*z[7];
    z[79]=11*z[6];
    z[104]= - z[45] + z[79];
    z[104]=z[104]*z[102];
    z[111]=z[2] - z[7];
    z[122]=z[111]*z[2];
    z[126]=npow(z[11],2);
    z[129]=n<T>(11,2)*z[126];
    z[61]= - z[6]*z[61];
    z[18]=z[18] + z[104] + z[61] - z[122] - z[129] + z[20];
    z[18]=z[8]*z[18];
    z[20]=z[7]*z[43];
    z[20]=z[85] + z[20];
    z[20]=z[14]*z[20];
    z[25]= - n<T>(17,6)*z[126] + z[25];
    z[25]=z[2]*z[25];
    z[61]= - z[41]*z[16];
    z[74]=z[74] - z[79];
    z[31]=z[74]*z[31];
    z[74]=9*z[26];
    z[32]=z[32]*z[74];
    z[18]=z[18] + z[32] + z[31] + z[61] + z[20] + z[25];
    z[18]=z[8]*z[18];
    z[20]=7*z[6];
    z[25]= - z[66] + z[24] + z[20];
    z[25]=z[25]*z[30];
    z[30]=4*z[4];
    z[20]= - z[20] + z[30];
    z[20]=z[4]*z[20];
    z[20]= - z[62] + z[20];
    z[31]=2*z[88];
    z[32]=z[4]*z[9];
    z[61]= - static_cast<T>(4)+ z[32];
    z[61]=z[61]*z[31];
    z[20]=3*z[20] + z[61];
    z[20]=z[3]*z[20];
    z[20]=z[25] + z[20];
    z[20]=z[20]*z[21];
    z[25]=z[40]*z[35];
    z[35]=z[94]*z[90];
    z[25]=z[25] - z[35];
    z[25]=z[25]*z[10];
    z[35]=z[95] - z[4];
    z[35]=z[35]*z[65];
    z[61]=4*z[6];
    z[65]= - z[61] + z[4];
    z[31]=z[65]*z[31];
    z[31]=z[35] + z[31];
    z[31]=z[31]*z[29];
    z[35]=z[6]*z[39];
    z[35]=z[40] + z[35];
    z[35]=z[35]*z[95];
    z[35]= - z[54] + z[35];
    z[35]=z[6]*z[35];
    z[31]= - 2*z[25] + z[35] + z[31];
    z[31]=z[10]*z[31];
    z[35]= - z[37] + n<T>(4,3)*z[7] + z[14];
    z[35]=z[35]*z[37];
    z[39]=2*z[44];
    z[65]= - n<T>(1,3) - z[39];
    z[65]=z[65]*z[49];
    z[35]=z[35] + z[65];
    z[35]=z[6]*z[35];
    z[65]=5*z[23];
    z[90]=n<T>(61,6)*z[126] + z[65];
    z[104]= - n<T>(8,3)*z[14] + z[111];
    z[104]=z[2]*z[104];
    z[90]=n<T>(1,2)*z[90] + z[104];
    z[90]=z[2]*z[90];
    z[104]=z[126]*z[14];
    z[35]=z[35] + n<T>(5,3)*z[104] + z[90];
    z[35]=z[6]*z[35];
    z[90]=n<T>(3,4)*z[126] - 4*z[41];
    z[90]=z[6]*z[90];
    z[111]= - z[7]*z[30];
    z[111]= - n<T>(1,4)*z[126] + z[111];
    z[111]=z[4]*z[111];
    z[130]= - z[7]*z[129];
    z[90]=z[111] + z[130] + z[90];
    z[90]=z[4]*z[90];
    z[111]= - z[23]*z[129];
    z[40]= - n<T>(7,12)*z[104] - z[40];
    z[40]=z[2]*z[40];
    z[18]=z[31] + z[18] + z[20] + z[90] + z[35] + z[111] + z[40];
    z[18]=z[10]*z[18];
    z[20]=9*z[3];
    z[31]=z[126]*z[12];
    z[35]=z[126]*z[13];
    z[30]= - z[20] + z[30] - n<T>(3,2)*z[6] + n<T>(143,4)*z[35] - n<T>(11,4)*z[14]
    + z[42] + n<T>(113,4)*z[31];
    z[30]=z[8]*z[30];
    z[40]=z[52]*z[126];
    z[27]=z[21] + z[115] + z[27] + z[40];
    z[27]=z[3]*z[27];
    z[90]= - z[37] - z[73] + n<T>(23,3)*z[35];
    z[90]=z[2]*z[90];
    z[19]=z[42] + z[19];
    z[19]=3*z[19] + z[43];
    z[19]=z[14]*z[19];
    z[104]=n<T>(53,3)*z[14] + z[95];
    z[104]=z[6]*z[104];
    z[111]= - z[4] - z[7] - 12*z[6];
    z[111]=z[4]*z[111];
    z[19]=z[30] + 2*z[27] + z[111] + z[104] + z[90] - z[85] + z[19];
    z[19]=z[8]*z[19];
    z[27]=n<T>(1,6)*z[31] - z[24];
    z[27]=z[14]*z[27];
    z[30]=n<T>(26,3) - z[44];
    z[30]=z[2]*z[30];
    z[30]=z[30] - n<T>(73,12)*z[35] + 20*z[7] - n<T>(47,3)*z[14];
    z[30]=z[2]*z[30];
    z[85]= - static_cast<T>(11)+ z[91];
    z[85]=z[2]*z[85];
    z[90]= - z[13] + 2*z[63];
    z[90]=z[2]*z[90];
    z[90]=n<T>(5,3) + z[90];
    z[90]=z[6]*z[90];
    z[85]=z[90] - n<T>(16,3)*z[7] + z[85];
    z[85]=z[6]*z[85];
    z[27]=z[85] + z[27] + z[30];
    z[27]=z[6]*z[27];
    z[30]=z[126]*z[58];
    z[85]=z[56] + 1;
    z[90]= - z[85]*z[95];
    z[90]= - n<T>(1,4)*z[35] + z[90];
    z[90]=z[90]*z[99];
    z[91]=n<T>(1,2)*z[9];
    z[104]= - z[126]*z[91];
    z[104]=z[7] + z[104];
    z[104]= - 36*z[4] + n<T>(63,4)*z[6] + n<T>(11,2)*z[104] + 15*z[14];
    z[104]=z[4]*z[104];
    z[30]=z[104] - 19*z[30] + z[90];
    z[30]=z[4]*z[30];
    z[90]= - z[24] - z[37];
    z[90]=z[2]*z[90];
    z[104]= - n<T>(25,3)*z[7] - z[40];
    z[104]=z[6]*z[104];
    z[68]=z[104] + z[90] + z[68];
    z[90]=z[32] - 5;
    z[104]=2*z[4];
    z[104]=z[90]*z[104];
    z[86]=z[104] + z[95] + z[86];
    z[104]= - 8*z[9] - z[124];
    z[104]=z[4]*z[104];
    z[104]=static_cast<T>(3)+ z[104];
    z[104]=z[3]*z[104];
    z[86]=2*z[86] + z[104];
    z[86]=z[86]*z[21];
    z[104]= - z[24] - z[87];
    z[104]=2*z[104] + 23*z[4];
    z[104]=z[104]*z[80];
    z[68]=z[86] + 2*z[68] + z[104];
    z[68]=z[3]*z[68];
    z[86]=n<T>(11,2)*z[35] + z[71] + z[40];
    z[86]=z[23]*z[86];
    z[104]=z[35]*z[14];
    z[111]=z[23] - n<T>(7,6)*z[104];
    z[115]=10*z[14] + z[2];
    z[115]=z[115]*z[69];
    z[111]=n<T>(5,2)*z[111] + z[115];
    z[111]=z[2]*z[111];
    z[18]=z[18] + z[19] + z[68] + z[30] + z[27] + z[111] + z[86];
    z[18]=z[10]*z[18];
    z[19]= - n<T>(59,4)*z[35] + z[16] + z[110] - z[42] - n<T>(131,4)*z[31];
    z[19]=z[13]*z[19];
    z[27]=2*z[5];
    z[30]=z[27]*z[55];
    z[68]= - 3*z[89] + z[30];
    z[68]=z[3]*z[68];
    z[68]=z[68] - 3*z[51] + 2*z[107];
    z[68]=z[68]*z[21];
    z[86]=z[51]*z[5];
    z[68]=z[68] - n<T>(45,2)*z[12] + z[86];
    z[68]=z[3]*z[68];
    z[111]=z[89]*z[74];
    z[115]=5*z[13];
    z[111]=z[111] - 8*z[63] + 4*z[12] + z[115];
    z[111]=z[8]*z[111];
    z[129]=z[51]*z[126];
    z[130]= - static_cast<T>(4)- n<T>(19,2)*z[72];
    z[130]=z[13]*z[130];
    z[130]=z[130] + n<T>(21,2)*z[63];
    z[130]=z[4]*z[130];
    z[19]=z[111] + z[68] + z[130] + z[57] + n<T>(33,2)*z[44] + z[19] - n<T>(5,4)
   *z[109] - n<T>(13,4) - 24*z[129];
    z[19]=z[8]*z[19];
    z[57]= - z[42] + n<T>(7,4)*z[31];
    z[57]=z[57]*z[105];
    z[68]=z[126]*z[72];
    z[111]=4*z[22];
    z[57]=n<T>(35,4)*z[68] - z[111] + z[57];
    z[57]=z[13]*z[57];
    z[68]=static_cast<T>(1)+ n<T>(31,2)*z[72];
    z[68]=z[13]*z[68];
    z[68]=z[68] - n<T>(3,2)*z[63];
    z[68]=z[4]*z[68];
    z[68]=z[68] - 16*z[44] - static_cast<T>(3)- 19*z[72];
    z[68]=z[4]*z[68];
    z[129]= - z[51]*z[21];
    z[130]=2*z[12];
    z[129]=z[129] + z[130] - 3*z[86];
    z[129]=z[129]*z[21];
    z[116]=z[129] - n<T>(53,2) + z[116];
    z[116]=z[3]*z[116];
    z[35]=z[5] - n<T>(35,3)*z[35];
    z[35]=z[13]*z[35];
    z[35]=3*z[44] - n<T>(43,6) + z[35];
    z[35]=z[2]*z[35];
    z[129]= - static_cast<T>(47)- 29*z[72];
    z[129]=n<T>(1,2)*z[129] - z[56];
    z[129]=z[6]*z[129];
    z[131]=n<T>(10,3)*z[14];
    z[19]=z[19] + z[116] + z[68] + z[129] + z[35] + z[57] + z[131] - 4*
    z[15] + n<T>(7,4)*z[5];
    z[19]=z[8]*z[19];
    z[35]= - z[72]*z[24];
    z[35]= - 15*z[4] + z[35] - z[36] + n<T>(17,4)*z[14];
    z[35]=z[13]*z[35];
    z[57]= - static_cast<T>(1)+ n<T>(5,4)*z[81];
    z[57]=z[13]*z[57];
    z[57]=n<T>(11,4)*z[9] + z[57];
    z[57]=z[2]*z[57];
    z[35]=n<T>(11,4)*z[56] + z[57] + static_cast<T>(38)+ z[35];
    z[35]=z[4]*z[35];
    z[57]=z[78]*z[126];
    z[68]= - z[73]*z[57];
    z[68]=z[7] + z[68];
    z[116]= - z[36] + z[117];
    z[116]=z[116]*z[81];
    z[129]=n<T>(11,2)*z[14] + z[119];
    z[129]=z[129]*z[115];
    z[129]= - n<T>(11,2) + z[129];
    z[129]=z[2]*z[129];
    z[132]=z[72] + 4;
    z[133]=z[38]*z[6];
    z[134]= - z[132]*z[133];
    z[112]= - z[112] + z[134];
    z[112]=z[112]*z[95];
    z[112]=z[112] - static_cast<T>(29)- n<T>(13,2)*z[72];
    z[112]=z[6]*z[112];
    z[35]=z[35] + z[112] + n<T>(1,2)*z[129] + z[116] - 24*z[14] + 2*z[68] - 
    z[105];
    z[35]=z[4]*z[35];
    z[68]=z[130] + 4*z[124];
    z[112]=3*z[9];
    z[116]=z[130] - z[112];
    z[116]=z[9]*z[116];
    z[116]= - z[51] + z[116];
    z[27]=z[116]*z[27];
    z[27]=z[27] + z[112] + z[68];
    z[27]=z[3]*z[27];
    z[116]=3*z[57];
    z[118]=static_cast<T>(2)- z[118];
    z[118]=z[118]*z[83];
    z[118]=z[12] + z[118];
    z[118]=z[5]*z[118];
    z[129]= - z[96] - z[121];
    z[129]=z[2]*z[129];
    z[128]=3*z[128] - z[124];
    z[128]=z[4]*z[128];
    z[134]=z[15]*z[96];
    z[27]=z[27] + z[128] + z[129] + z[118] + z[116] + static_cast<T>(5)+ z[134];
    z[27]=z[27]*z[21];
    z[118]= - 5*z[15] + z[120];
    z[118]=z[118]*z[83];
    z[116]=z[116] - static_cast<T>(7)+ z[118];
    z[116]=z[5]*z[116];
    z[118]= - static_cast<T>(7)- z[57];
    z[128]=z[112] - n<T>(1,2)*z[103];
    z[128]=z[4]*z[128];
    z[118]=z[128] + 3*z[118] - z[100];
    z[118]=z[118]*z[80];
    z[77]=z[15] - z[77];
    z[77]=2*z[77] + z[120];
    z[100]=z[100] + static_cast<T>(3)+ n<T>(32,3)*z[127];
    z[100]=z[2]*z[100];
    z[27]=z[27] + z[118] + z[100] + 18*z[14] + 2*z[77] + z[116] + 15*
    z[6];
    z[27]=z[3]*z[27];
    z[38]= - z[132]*z[38];
    z[77]= - z[34]*z[37];
    z[38]=z[38] + z[77];
    z[37]=z[37]*z[6];
    z[38]=z[38]*z[37];
    z[77]= - n<T>(37,3) - z[114];
    z[77]=z[13]*z[77];
    z[77]=z[77] - z[93];
    z[77]=z[2]*z[77];
    z[100]=static_cast<T>(5)- 8*z[123];
    z[38]=z[38] + n<T>(1,3)*z[100] + z[77];
    z[38]=z[6]*z[38];
    z[77]= - 8*z[44] - n<T>(5,2) + n<T>(8,3)*z[72];
    z[77]=z[2]*z[77];
    z[38]=z[38] + z[77] - 6*z[48] + z[64] - n<T>(89,3)*z[7] + z[110];
    z[38]=z[6]*z[38];
    z[36]= - z[126]*z[36];
    z[64]=z[126] + z[60];
    z[64]=z[64]*z[43];
    z[36]=z[36] + z[64];
    z[36]=z[14]*z[36];
    z[64]=z[60]*z[126];
    z[36]=n<T>(17,4)*z[64] + z[36];
    z[36]=z[13]*z[36];
    z[64]= - z[112]*z[64];
    z[40]=z[7] - z[40];
    z[40]=z[14]*z[40];
    z[31]= - z[7] + n<T>(15,2)*z[31];
    z[31]=z[5]*z[31];
    z[31]=z[31] + z[40];
    z[31]=z[14]*z[31];
    z[31]=z[36] + z[64] + z[31];
    z[31]=z[13]*z[31];
    z[36]=z[23] + n<T>(13,12)*z[104];
    z[36]=z[36]*z[115];
    z[40]=n<T>(19,6)*z[14];
    z[64]=n<T>(11,3) + 6*z[58];
    z[64]=z[5]*z[64];
    z[77]= - z[9]*z[69];
    z[77]= - static_cast<T>(2)+ z[77];
    z[77]=z[2]*z[77];
    z[36]=z[77] + z[36] - z[40] + 12*z[7] + z[64];
    z[36]=z[2]*z[36];
    z[57]=z[57]*z[71];
    z[64]= - z[42] - z[7];
    z[57]=5*z[64] + z[57];
    z[57]=z[5]*z[57];
    z[64]=z[24] - z[7] + z[92];
    z[64]=z[14]*z[64];
    z[17]=z[17] + z[18] + z[19] + z[27] + z[35] + z[38] + z[36] + z[31]
    + z[64] - z[111] + z[57];
    z[17]=z[1]*z[17];
    z[18]=z[42] - z[7];
    z[18]=z[18]*z[16];
    z[19]= - z[7] - z[79];
    z[19]=z[19]*z[102];
    z[27]=n<T>(7,2)*z[7];
    z[31]=z[12]*z[74];
    z[31]= - z[27] + z[31];
    z[31]=z[3]*z[31];
    z[35]=z[12] + z[106];
    z[35]=z[35]*z[21];
    z[35]=n<T>(1,2) + z[35];
    z[35]=z[3]*z[35];
    z[36]= - static_cast<T>(2)- z[44];
    z[36]=z[2]*z[36];
    z[35]=z[36] + z[35];
    z[35]=z[8]*z[35];
    z[18]=z[35] + z[31] + z[19] + n<T>(41,6)*z[70] + z[18] - z[122];
    z[18]=z[8]*z[18];
    z[19]=z[14] + z[2];
    z[19]=z[2]*z[19]*z[41];
    z[19]= - z[54] + z[19];
    z[19]=z[6]*z[19];
    z[31]= - z[99] + z[4];
    z[31]=z[31]*z[88];
    z[31]=z[94] + z[31];
    z[29]=z[31]*z[29];
    z[19]= - z[25] + z[19] + z[29];
    z[19]=z[10]*z[19];
    z[25]= - z[131] + z[2];
    z[25]=z[2]*z[25];
    z[25]=z[65] + z[25];
    z[25]=z[2]*z[25];
    z[29]= - static_cast<T>(1)- z[44];
    z[29]=z[29]*z[49];
    z[31]=z[16] - z[2];
    z[31]=z[2]*z[31];
    z[29]=z[31] + z[29];
    z[29]=z[6]*z[29];
    z[25]=z[25] + z[29];
    z[25]=z[6]*z[25];
    z[29]= - z[9]*z[80];
    z[29]=static_cast<T>(2)+ z[29];
    z[29]=z[4]*z[29];
    z[28]= - z[28] + z[29];
    z[28]=z[4]*z[28];
    z[29]=z[4]*z[90];
    z[29]=z[61] + z[29];
    z[29]=z[3]*z[29];
    z[28]=z[28] + z[29];
    z[28]=z[3]*z[28];
    z[28]=n<T>(7,2)*z[47] + z[28];
    z[28]=z[28]*z[21];
    z[29]= - z[4]*z[82];
    z[19]=z[19] + z[28] + z[29] - z[54] + z[25];
    z[19]=z[10]*z[19];
    z[25]= - n<T>(157,4)*z[14] + 7*z[7];
    z[28]=n<T>(16,3) - z[44];
    z[28]=z[2]*z[28];
    z[25]=n<T>(1,3)*z[25] + z[28];
    z[25]=z[2]*z[25];
    z[28]= - n<T>(11,3) + z[39];
    z[28]=z[2]*z[28];
    z[29]=n<T>(2,3)*z[13] + z[63];
    z[29]=z[2]*z[29];
    z[29]=n<T>(5,3) + z[29];
    z[29]=z[6]*z[29];
    z[28]=z[28] + z[29];
    z[28]=z[6]*z[28];
    z[25]=z[28] - n<T>(17,2)*z[23] + z[25];
    z[25]=z[6]*z[25];
    z[28]=5*z[9];
    z[29]= - z[28] + z[124];
    z[29]=z[4]*z[29];
    z[29]=static_cast<T>(4)+ z[29];
    z[29]=z[3]*z[29];
    z[31]= - static_cast<T>(8)+ 11*z[32];
    z[31]=z[4]*z[31];
    z[29]=z[29] + z[87] + z[31];
    z[29]=z[29]*z[21];
    z[31]=n<T>(3,2) - z[84];
    z[31]=z[4]*z[31];
    z[31]= - n<T>(9,2)*z[6] + z[31];
    z[31]=z[31]*z[66];
    z[29]=z[29] + n<T>(37,6)*z[62] + z[31];
    z[29]=z[3]*z[29];
    z[31]= - n<T>(13,3) + z[44];
    z[31]=z[31]*z[76];
    z[31]=z[65] + z[31];
    z[31]=z[2]*z[31];
    z[35]=z[85]*z[41];
    z[36]=z[32] - 8;
    z[36]=z[7]*z[36];
    z[36]=n<T>(15,2)*z[6] + z[36];
    z[36]=z[4]*z[36];
    z[35]= - 6*z[35] + z[36];
    z[35]=z[4]*z[35];
    z[18]=z[19] + z[18] + z[29] + z[35] + z[31] + z[25];
    z[18]=z[10]*z[18];
    z[19]= - z[75] + z[42] + z[50];
    z[25]=z[96] + z[52];
    z[29]= - 5*z[124] + z[25];
    z[29]=z[3]*z[29];
    z[31]=z[6]*z[12];
    z[35]= - 15*z[9] + z[124];
    z[35]=z[4]*z[35];
    z[29]=z[29] + z[35] + static_cast<T>(6)- z[31];
    z[29]=z[29]*z[21];
    z[35]=n<T>(3,2)*z[4];
    z[36]= - static_cast<T>(25)+ 31*z[32];
    z[36]=z[36]*z[35];
    z[19]=z[29] + z[36] + 2*z[19] + 39*z[6];
    z[19]=z[3]*z[19];
    z[29]=z[89]*z[3];
    z[29]=z[29] + z[51];
    z[36]=z[29]*z[20];
    z[36]=z[36] + z[93] + z[130] + 13*z[13];
    z[36]=z[8]*z[36];
    z[38]=n<T>(1,2)*z[13];
    z[39]=z[38] + 5*z[63];
    z[39]=z[2]*z[39];
    z[29]= - z[29]*z[21];
    z[41]=n<T>(5,2)*z[12];
    z[29]= - z[41] + z[29];
    z[29]=z[29]*z[21];
    z[29]=z[36] + z[29] - n<T>(51,4) + z[39];
    z[29]=z[8]*z[29];
    z[26]= - z[51]*z[26];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[3]*z[26];
    z[36]=z[15] - z[45];
    z[26]=z[29] + 18*z[26] - 6*z[4] - z[99] + 2*z[36] - n<T>(43,6)*z[2];
    z[26]=z[8]*z[26];
    z[29]= - z[23]*z[115];
    z[36]= - n<T>(2,3)*z[44] + static_cast<T>(6)- n<T>(5,3)*z[81];
    z[36]=z[2]*z[36];
    z[39]=z[98] - n<T>(49,4)*z[14];
    z[29]=z[36] + n<T>(1,3)*z[39] + z[29];
    z[29]=z[2]*z[29];
    z[36]= - z[46] - z[53];
    z[36]=z[36]*z[37];
    z[37]=3*z[63];
    z[39]= - n<T>(14,3)*z[13] - z[37];
    z[39]=z[2]*z[39];
    z[36]=z[36] + n<T>(10,3) + z[39];
    z[36]=z[6]*z[36];
    z[39]=z[52]*z[14];
    z[47]= - n<T>(1,2) + z[39];
    z[47]=z[14]*z[47];
    z[50]=n<T>(71,12) + z[44];
    z[50]=z[2]*z[50];
    z[36]=z[36] + z[50] + n<T>(19,3)*z[7] + z[47];
    z[36]=z[6]*z[36];
    z[32]= - 15*z[32] + z[56] - static_cast<T>(7)- n<T>(11,2)*z[58];
    z[32]=z[4]*z[32];
    z[47]= - z[33] - z[133];
    z[50]=6*z[6];
    z[47]=z[47]*z[50];
    z[47]= - n<T>(85,4) + z[47];
    z[47]=z[6]*z[47];
    z[24]=13*z[7] - z[24];
    z[24]=z[32] + n<T>(1,2)*z[24] + z[47];
    z[24]=z[4]*z[24];
    z[32]= - z[73] - n<T>(17,2)*z[14];
    z[32]=z[14]*z[32];
    z[18]=z[18] + z[26] + z[19] + z[24] + z[36] + z[29] + 6*z[22] + 
    z[32];
    z[18]=z[10]*z[18];
    z[19]= - z[5]*z[97];
    z[19]=3*z[55] + z[19];
    z[19]=z[3]*z[19];
    z[19]=z[19] + 7*z[89] - z[30];
    z[19]=z[19]*z[21];
    z[22]=57*z[51] - 7*z[107];
    z[19]=n<T>(1,2)*z[22] + z[19];
    z[19]=z[3]*z[19];
    z[22]= - z[3]*z[55];
    z[22]= - z[89] + z[22];
    z[20]=z[22]*z[20];
    z[22]= - 13*z[12] - 11*z[13];
    z[22]=z[13]*z[22];
    z[20]=z[20] - z[59] + z[22];
    z[20]=z[8]*z[20];
    z[22]=static_cast<T>(13)+ 17*z[109];
    z[22]=n<T>(1,2)*z[22] + 5*z[72];
    z[22]=z[13]*z[22];
    z[24]=n<T>(45,4)*z[12] + z[86];
    z[19]=z[20] + z[19] - 13*z[63] + 3*z[24] + z[22];
    z[19]=z[8]*z[19];
    z[20]=z[92] - n<T>(5,6)*z[14];
    z[20]=z[20]*z[115];
    z[22]=static_cast<T>(6)- n<T>(43,2)*z[72];
    z[22]=z[13]*z[22];
    z[22]=z[22] + n<T>(19,2)*z[63];
    z[22]=z[4]*z[22];
    z[24]=z[89]*z[21];
    z[24]=z[107] + z[24];
    z[24]=z[24]*z[21];
    z[24]=z[24] + n<T>(33,2)*z[12] - 2*z[86];
    z[24]=z[3]*z[24];
    z[26]=static_cast<T>(7)- n<T>(3,2)*z[109];
    z[19]=z[19] + z[24] + z[22] - 8*z[56] + n<T>(61,3)*z[44] + n<T>(3,2)*z[26]
    + z[20];
    z[19]=z[8]*z[19];
    z[20]=n<T>(3,2)*z[67] + 21*z[5] - z[117];
    z[20]=z[13]*z[20];
    z[16]= - z[13]*z[16];
    z[16]=n<T>(1,2) + z[16];
    z[16]=z[13]*z[16];
    z[16]=z[91] + z[16];
    z[16]=z[16]*z[113];
    z[22]= - z[6]*z[34];
    z[22]= - z[46] + z[22];
    z[22]=z[22]*z[95];
    z[22]= - n<T>(45,4)*z[13] + z[22];
    z[22]=z[6]*z[22];
    z[24]=z[91] + 19*z[13];
    z[24]=z[24]*z[102];
    z[26]= - z[7]*z[83];
    z[16]=z[24] + z[22] + z[16] + z[20] - 8*z[127] + n<T>(77,4) + z[26];
    z[16]=z[4]*z[16];
    z[20]=z[9]*z[25];
    z[22]=z[9]*z[52];
    z[22]= - z[59] + z[22];
    z[22]=z[9]*z[22];
    z[22]=z[89] + z[22];
    z[22]=z[5]*z[22];
    z[20]=z[22] - 4*z[51] + z[20];
    z[20]=z[3]*z[20];
    z[22]= - z[12] + z[9];
    z[22]=z[22]*z[83];
    z[22]=z[51] + z[22];
    z[22]=z[5]*z[22];
    z[20]=z[20] + z[22] + 7*z[9] - z[68];
    z[20]=z[20]*z[21];
    z[21]= - z[125]*z[112];
    z[21]=n<T>(17,2)*z[12] + z[21];
    z[21]=z[5]*z[21];
    z[22]= - z[83] + n<T>(47,6)*z[121];
    z[22]=z[2]*z[22];
    z[24]= - 9*z[9] - z[103];
    z[24]=3*z[24] + z[124];
    z[24]=z[24]*z[35];
    z[25]= - z[42] - n<T>(5,3)*z[7];
    z[25]=z[9]*z[25];
    z[25]=static_cast<T>(9)+ z[25];
    z[20]=z[20] + z[24] - 6*z[31] + z[22] + 2*z[25] + z[21];
    z[20]=z[3]*z[20];
    z[21]= - z[34]*z[49];
    z[21]= - z[37] + z[21];
    z[21]=z[21]*z[95];
    z[21]=z[21] - n<T>(149,12)*z[44] + n<T>(13,6)*z[123] + n<T>(35,6) - z[39];
    z[21]=z[6]*z[21];
    z[22]= - z[45] - z[5];
    z[22]=z[14]*z[22];
    z[22]=n<T>(13,2)*z[60] + z[22];
    z[22]=z[22]*z[43];
    z[23]= - z[23]*z[48];
    z[22]=z[22] + z[23];
    z[22]=z[13]*z[22];
    z[23]=z[7] + z[105];
    z[24]=z[5]*z[52];
    z[24]= - n<T>(7,2) + z[24];
    z[24]=z[14]*z[24];
    z[23]=2*z[23] + z[24];
    z[23]=z[14]*z[23];
    z[22]=z[22] - z[101] + z[23];
    z[22]=z[13]*z[22];
    z[23]=z[78]*z[60];
    z[23]=z[58] + z[23];
    z[23]=n<T>(13,2) + 10*z[23];
    z[24]=n<T>(5,2)*z[13];
    z[25]= - z[119] - n<T>(7,6)*z[14];
    z[25]=z[25]*z[24];
    z[26]=z[9] - z[13];
    z[26]=z[2]*z[26];
    z[23]=n<T>(2,3)*z[26] + n<T>(1,3)*z[23] + z[25];
    z[23]=z[2]*z[23];
    z[25]=6*z[15];
    z[26]= - n<T>(17,2) + z[58];
    z[26]=z[5]*z[26];
    z[29]= - n<T>(22,3) + z[39];
    z[29]=z[14]*z[29];
    z[16]=z[17] + z[18] + z[19] + z[20] + z[16] + z[21] + z[23] + z[22]
    + z[29] + n<T>(3,2)*z[26] - 6*z[120] + z[25] + z[27];
    z[16]=z[1]*z[16];
    z[17]=5*z[11];
    z[18]=z[17]*z[12];
    z[19]=z[11]*z[115];
    z[19]=z[19] + n<T>(67,3) + z[18];
    z[19]=z[8]*z[19];
    z[20]= - z[10]*z[6];
    z[20]=z[20] - z[85];
    z[20]=z[4]*z[20];
    z[20]=z[6] + z[20];
    z[20]=z[10]*z[11]*z[20];
    z[19]=z[19] + z[20];
    z[20]=z[7] + z[15];
    z[21]=z[11]*z[20]*z[33];
    z[22]= - z[11]*z[9];
    z[22]=n<T>(11,2) + z[22];
    z[22]=z[22]*z[102];
    z[23]=n<T>(1,2)*z[11];
    z[19]=z[22] - n<T>(79,6)*z[6] + n<T>(5,3)*z[2] - z[21] - n<T>(23,6)*z[14] + 
    z[23] + z[25] + n<T>(19,2)*z[7] + n<T>(1,2)*z[19];
    z[19]=z[10]*z[19];
    z[22]= - z[41] - z[112];
    z[25]=z[9]*z[12];
    z[25]= - z[51] + z[25];
    z[25]=z[25]*z[17];
    z[22]=7*z[22] + z[25];
    z[22]=z[22]*z[108];
    z[25]= - z[15] - z[73];
    z[20]=z[9]*z[20];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[11]*z[20];
    z[20]=2*z[25] + z[20];
    z[18]=n<T>(17,4) - z[18];
    z[18]=z[5]*z[18];
    z[18]= - z[21] + n<T>(28,3)*z[14] + 2*z[20] + z[18];
    z[18]=z[13]*z[18];
    z[20]= - static_cast<T>(1)+ z[109];
    z[20]=z[24]*z[20];
    z[20]=z[20] - n<T>(15,2)*z[12];
    z[20]=z[11]*z[20];
    z[17]=z[86]*z[17];
    z[17]=z[17] - n<T>(64,3) + z[20];
    z[17]=z[13]*z[17];
    z[20]=z[107]*z[23];
    z[21]= - z[11]*z[51];
    z[20]=z[20] - n<T>(5,6)*z[12] + z[21];
    z[17]=5*z[20] + z[17];
    z[17]=z[8]*z[17];
    z[20]=z[42] + z[27];
    z[20]=z[20]*z[28];
    z[21]=5*z[12] + z[9];
    z[21]=z[21]*z[23];
    z[23]= - z[9] + z[38];
    z[23]=z[23]*z[69];
    z[24]=n<T>(53,2)*z[9] - 23*z[13];
    z[24]=z[24]*z[102];
    z[25]=z[12]*z[40];

    r +=  - n<T>(29,3) + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + 
      z[22] + z[23] + z[24] + z[25];
 
    return r;
}

template double qg_2lNLC_r292(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r292(const std::array<dd_real,31>&);
#endif
