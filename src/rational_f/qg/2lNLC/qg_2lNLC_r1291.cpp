#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1291(const std::array<T,31>& k) {
  T z[155];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[13];
    z[5]=k[15];
    z[6]=k[11];
    z[7]=k[12];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=k[10];
    z[11]=k[8];
    z[12]=k[5];
    z[13]=k[2];
    z[14]=k[14];
    z[15]=k[4];
    z[16]=k[26];
    z[17]=k[22];
    z[18]=k[29];
    z[19]=k[17];
    z[20]=k[19];
    z[21]=2*z[2];
    z[22]=npow(z[14],2);
    z[23]=z[21]*z[22];
    z[24]=z[14]*z[13];
    z[25]=2*z[8];
    z[26]= - z[7]*z[25];
    z[26]=z[26] + 5*z[24];
    z[27]=npow(z[9],3);
    z[26]=z[27]*z[26];
    z[28]=3*z[7];
    z[29]=npow(z[19],2);
    z[30]= - z[29]*z[28];
    z[31]=z[27]*z[8];
    z[32]=npow(z[4],2);
    z[33]=z[31] + 4*z[32];
    z[34]=n<T>(1,3)*z[4];
    z[33]=z[33]*z[34];
    z[35]=n<T>(4,3)*z[2];
    z[36]=npow(z[4],3);
    z[37]= - z[36]*z[35];
    z[38]= - z[14]*z[27];
    z[37]=z[38] + z[37];
    z[37]=z[12]*z[37];
    z[26]=z[37] + z[33] - z[23] + z[30] + z[26];
    z[30]=2*z[12];
    z[26]=z[26]*z[30];
    z[33]=3*z[19];
    z[37]=2*z[5];
    z[38]= - z[33] + z[37];
    z[39]=2*z[7];
    z[38]=z[38]*z[39];
    z[40]=3*z[5];
    z[41]=z[40]*z[6];
    z[42]=3*z[14];
    z[43]=z[7]*z[42];
    z[38]=z[43] + z[41] + z[38];
    z[43]=npow(z[8],2);
    z[44]=z[43]*z[27];
    z[45]=z[4]*z[8];
    z[46]=static_cast<T>(11)+ 8*z[45];
    z[46]=z[46]*z[34];
    z[47]=8*z[5];
    z[46]=z[46] - n<T>(4,3)*z[44] + z[47] + n<T>(11,3)*z[2];
    z[46]=z[4]*z[46];
    z[48]= - z[39]*z[44];
    z[49]= - 8*z[6] - z[11];
    z[49]=z[11]*z[49];
    z[50]=4*z[17];
    z[51]=z[50] - z[19];
    z[52]= - n<T>(7,3)*z[11] + n<T>(13,3)*z[5] + z[51];
    z[52]=z[2]*z[52];
    z[26]=z[26] + z[46] + z[48] + z[52] + 2*z[38] + z[49];
    z[26]=z[12]*z[26];
    z[38]=5*z[19];
    z[46]= - z[38] + z[37];
    z[46]=z[7]*z[46];
    z[48]=2*z[4];
    z[49]=z[37] - n<T>(5,3)*z[4];
    z[49]=z[49]*z[48];
    z[52]=z[32]*z[12];
    z[53]=z[35]*z[52];
    z[54]=z[2]*z[5];
    z[55]=n<T>(2,3)*z[54];
    z[56]=4*z[14];
    z[57]=z[56]*z[7];
    z[46]=z[53] + z[49] - z[55] + z[46] + z[57];
    z[46]=z[12]*z[46];
    z[49]=3*z[11];
    z[53]=z[17] - z[7];
    z[46]=z[46] - 2*z[44] + 4*z[53] - z[49];
    z[46]=z[12]*z[46];
    z[53]=2*z[13];
    z[58]=z[53]*z[17];
    z[59]= - static_cast<T>(9)+ z[58];
    z[59]=z[59]*z[53];
    z[60]=npow(z[13],2);
    z[61]= - z[60]*z[49];
    z[62]=npow(z[8],3);
    z[63]=z[62]*z[4];
    z[64]= - 7*z[43] + z[63];
    z[64]=z[48]*z[64];
    z[59]=z[64] + z[59] + z[61];
    z[59]=z[10]*z[59];
    z[61]=z[10]*z[13];
    z[64]=3*z[61];
    z[65]=z[8]*z[7];
    z[66]= - z[65] + static_cast<T>(2)- z[64];
    z[66]=z[8]*z[66];
    z[67]= - z[7] + z[4];
    z[67]=z[12]*z[67];
    z[67]=z[65] + z[67];
    z[67]=z[12]*z[67];
    z[68]= - z[10]*z[63];
    z[66]=z[67] + z[66] + z[68];
    z[67]=4*z[3];
    z[66]=z[66]*z[67];
    z[68]=4*z[13];
    z[69]=z[68]*z[17];
    z[70]=z[69] - 11;
    z[71]=z[13]*z[49];
    z[71]=z[71] - z[70];
    z[72]= - z[13]*z[27];
    z[72]=z[72] - n<T>(2,3)*z[31];
    z[72]=z[8]*z[72];
    z[73]=4*z[10];
    z[72]=z[73] + z[72];
    z[72]=z[72]*z[25];
    z[46]=z[66] + z[46] + z[72] + 2*z[71] + z[59];
    z[46]=z[3]*z[46];
    z[59]=z[53]*z[14];
    z[66]=z[11]*z[13];
    z[71]=z[66] + n<T>(11,3) + z[59];
    z[71]=z[11]*z[71];
    z[72]=2*z[14];
    z[71]=z[71] - z[72] - z[7] - z[50] + 9*z[6];
    z[74]=z[8]*z[10];
    z[75]=z[74]*z[27];
    z[76]= - z[53]*z[27]*z[10];
    z[76]=z[76] + n<T>(25,3)*z[75];
    z[76]=z[8]*z[76];
    z[77]=n<T>(1,3)*z[27];
    z[77]=z[60]*z[77];
    z[78]=2*z[6];
    z[77]= - z[78] + z[77];
    z[77]=z[10]*z[77];
    z[79]=z[7]*z[6];
    z[77]=z[79] + z[77];
    z[76]=2*z[77] + z[76];
    z[76]=z[8]*z[76];
    z[77]=13*z[10] + 4*z[44];
    z[77]=z[8]*z[77];
    z[80]=z[43]*z[4];
    z[81]=4*z[74];
    z[82]= - static_cast<T>(7)- z[81];
    z[82]=z[8]*z[82];
    z[82]=z[82] + n<T>(8,3)*z[80];
    z[82]=z[4]*z[82];
    z[77]=z[82] + n<T>(11,3) + z[77];
    z[77]=z[4]*z[77];
    z[82]=z[60]*z[11];
    z[83]= - z[13] - z[82];
    z[83]=z[11]*z[83];
    z[70]=z[83] + z[70];
    z[70]=z[10]*z[70];
    z[26]=z[46] + z[26] + z[77] + z[76] + 2*z[71] + z[70];
    z[26]=z[3]*z[26];
    z[46]=npow(z[5],2);
    z[70]=4*z[46];
    z[71]=3*z[29];
    z[76]= - 4*z[22] + z[71] - z[70];
    z[77]=npow(z[11],2);
    z[76]=2*z[76] + n<T>(11,3)*z[77];
    z[76]=z[2]*z[76];
    z[83]=6*z[4];
    z[84]= - z[83] - n<T>(73,3)*z[44] - z[5] + z[35];
    z[84]=z[84]*z[32];
    z[85]=z[68]*z[22];
    z[86]= - z[27]*z[85];
    z[83]=z[2]*z[83];
    z[83]=n<T>(7,3)*z[31] + z[83];
    z[83]=z[83]*z[32];
    z[83]=z[86] + z[83];
    z[83]=z[12]*z[83];
    z[86]=18*z[29];
    z[87]= - z[7]*z[86];
    z[76]=z[83] + z[84] + z[87] + z[76];
    z[76]=z[12]*z[76];
    z[83]=3*z[6];
    z[84]=2*z[17];
    z[87]=2*z[11];
    z[88]= - z[87] + z[84] - z[83];
    z[89]=2*z[10];
    z[88]=z[88]*z[89];
    z[90]=z[62]*z[27];
    z[91]=z[90]*z[10];
    z[92]=6*z[10];
    z[93]= - z[92] + n<T>(35,3)*z[44];
    z[93]=z[8]*z[93];
    z[93]= - 6*z[45] + n<T>(11,3) + z[93];
    z[93]=z[4]*z[93];
    z[93]=z[93] - 17*z[91] + z[73] - z[5] - n<T>(7,3)*z[2];
    z[93]=z[4]*z[93];
    z[94]= - 2*z[19] + z[40];
    z[94]=z[94]*z[28];
    z[95]=n<T>(4,3)*z[11] + z[72] - 7*z[5] - 8*z[17] - z[33];
    z[95]=z[2]*z[95];
    z[96]=z[6]*z[37];
    z[97]= - z[7]*z[72];
    z[98]=z[78] - z[14];
    z[98]=6*z[98] - n<T>(11,3)*z[11];
    z[98]=z[11]*z[98];
    z[26]=z[26] + z[76] + z[93] + z[88] + z[95] + z[98] + z[97] + z[96]
    + z[94];
    z[26]=z[3]*z[26];
    z[76]=z[10]*z[6];
    z[88]=2*z[76];
    z[93]=z[79] - z[88];
    z[93]=z[8]*z[93];
    z[94]=z[5]*z[6];
    z[95]=z[7]*z[5];
    z[96]=z[94] - z[95];
    z[97]=z[12]*z[96];
    z[93]=z[97] + z[93] - z[10] + z[83] - z[7];
    z[93]=z[3]*z[93];
    z[97]=z[87] + z[37];
    z[97]=z[6]*z[97];
    z[98]=n<T>(2,3)*z[4];
    z[99]= - z[98] - z[10];
    z[100]=z[8]*z[9];
    z[101]=npow(z[100],5);
    z[99]=z[101]*z[99];
    z[102]=z[2] - z[5];
    z[99]= - n<T>(2,3)*z[102] + z[99];
    z[99]=z[4]*z[99];
    z[103]=npow(z[9],5);
    z[104]=npow(z[8],4);
    z[105]=z[103]*z[104];
    z[105]=z[105] + z[102];
    z[52]=z[105]*z[52];
    z[105]= - z[6] - z[37];
    z[105]=z[7]*z[105];
    z[106]=4*z[6];
    z[107]= - z[106] - z[11];
    z[107]=z[10]*z[107];
    z[52]=z[93] + n<T>(1,3)*z[52] + z[99] + z[107] - z[55] + z[105] + z[97];
    z[52]=z[52]*z[67];
    z[93]= - z[40]*z[79];
    z[97]=4*z[11];
    z[99]= - z[76]*z[97];
    z[93]=z[93] + z[99];
    z[99]= - z[12]*z[62];
    z[99]=2*z[104] + z[99];
    z[99]=z[12]*z[103]*z[36]*z[99];
    z[103]= - 8*z[4] + 17*z[10];
    z[101]=z[101]*z[103];
    z[101]= - 13*z[102] + z[101];
    z[101]=z[4]*z[101];
    z[101]= - 43*z[54] + z[101];
    z[101]=z[101]*z[34];
    z[52]=z[52] + n<T>(8,3)*z[99] + 2*z[93] + z[101];
    z[52]=z[3]*z[52];
    z[93]=z[54]*z[32];
    z[52]= - n<T>(31,3)*z[93] + z[52];
    z[52]=z[3]*z[52];
    z[99]=z[6] + z[5];
    z[99]=z[7]*z[99];
    z[94]=z[76] - z[94] + z[99];
    z[94]=z[3]*z[94];
    z[99]=z[37]*z[79];
    z[101]=z[10]*npow(z[100],6);
    z[101]=z[101] + z[102];
    z[101]=z[101]*z[34];
    z[101]=z[54] + z[101];
    z[101]=z[4]*z[101];
    z[103]=z[11]*z[76];
    z[94]=z[94] + z[101] + z[99] + z[103];
    z[94]=z[94]*z[67];
    z[94]=n<T>(17,3)*z[93] + z[94];
    z[94]=z[94]*npow(z[3],2);
    z[99]=z[79]*z[5];
    z[101]= - z[3]*z[99];
    z[93]= - n<T>(1,3)*z[93] + z[101];
    z[93]=z[1]*z[93]*npow(z[3],3);
    z[93]=z[94] + 4*z[93];
    z[93]=z[1]*z[93];
    z[52]=z[52] + z[93];
    z[52]=z[1]*z[52];
    z[93]= - z[106] + z[14];
    z[93]=z[93]*z[87];
    z[94]=z[6] + z[40];
    z[94]=z[7]*z[94];
    z[101]=7*z[6] + z[87];
    z[101]=z[10]*z[101];
    z[41]=z[101] + n<T>(11,3)*z[54] + z[93] - z[41] + z[94];
    z[93]=2*z[74];
    z[94]= - z[61] + z[93];
    z[94]=z[8]*z[94];
    z[101]=z[60]*z[10];
    z[94]= - z[101] + z[94];
    z[103]=n<T>(1,3)*z[8];
    z[105]=npow(z[9],4);
    z[94]=z[103]*z[105]*z[94];
    z[94]=3*z[76] + z[94];
    z[94]=z[8]*z[94];
    z[107]=n<T>(4,3)*z[4];
    z[108]=npow(z[100],4);
    z[109]=z[108]*z[107];
    z[110]=6*z[6];
    z[111]=static_cast<T>(3)+ z[66];
    z[111]=z[10]*z[111];
    z[94]=z[109] + z[94] + z[111] - z[87] - z[110] + z[7];
    z[109]=z[105]*z[62];
    z[111]= - z[109] - z[102];
    z[111]=z[111]*z[34];
    z[112]=z[11]*z[6];
    z[96]=z[111] - n<T>(1,3)*z[54] - 2*z[96] + z[112];
    z[111]=z[43]*z[7];
    z[112]= - z[105]*z[111];
    z[113]=n<T>(2,3)*z[2];
    z[114]=z[5] + z[113];
    z[114]=z[114]*z[32];
    z[112]=z[112] + z[114];
    z[112]=z[12]*z[112];
    z[96]=2*z[96] + z[112];
    z[96]=z[12]*z[96];
    z[112]=z[76] + z[79];
    z[112]=z[112]*z[8];
    z[114]=z[112] - z[7] + z[89];
    z[114]=z[8]*z[114];
    z[115]= - z[4]*z[5];
    z[95]=z[95] + z[115];
    z[95]=z[12]*z[95];
    z[95]=z[7] + z[95];
    z[95]=z[12]*z[95];
    z[95]=z[95] + z[114] - static_cast<T>(2)+ z[61];
    z[114]=2*z[3];
    z[95]=z[95]*z[114];
    z[94]=z[95] + 2*z[94] + z[96];
    z[94]=z[94]*z[114];
    z[95]=5*z[5];
    z[96]=3*z[2];
    z[109]= - 5*z[109] - z[95] - z[96];
    z[109]=z[109]*z[32];
    z[115]=n<T>(1,3)*z[32];
    z[115]= - z[43]*z[115];
    z[85]= - z[12]*z[85];
    z[85]=z[115] + z[85];
    z[85]=z[12]*z[105]*z[85];
    z[85]=z[109] + z[85];
    z[85]=z[12]*z[85];
    z[109]=z[10]*z[108];
    z[102]=n<T>(11,3)*z[102] - 7*z[109];
    z[109]= - z[108]*z[34];
    z[102]=2*z[102] + z[109];
    z[102]=z[4]*z[102];
    z[41]=z[94] + z[85] + 2*z[41] + z[102];
    z[41]=z[3]*z[41];
    z[85]= - z[12]*z[43];
    z[85]=2*z[62] + z[85];
    z[36]=z[36]*z[105];
    z[85]=z[12]*z[36]*z[85];
    z[76]=z[49]*z[76];
    z[76]=z[99] + z[76];
    z[94]= - 10*z[4] + n<T>(31,3)*z[10];
    z[94]=z[108]*z[94];
    z[99]= - z[5] + z[96];
    z[94]=2*z[99] + z[94];
    z[94]=z[4]*z[94];
    z[54]=n<T>(73,3)*z[54] + z[94];
    z[54]=z[4]*z[54];
    z[41]=z[41] + 10*z[85] + 2*z[76] + z[54];
    z[41]=z[3]*z[41];
    z[54]= - z[72] - z[5] - z[39];
    z[54]=z[54]*z[72];
    z[76]=z[73]*z[22];
    z[85]=z[15]*z[76];
    z[94]=z[10]*z[14];
    z[99]=z[2]*z[40];
    z[54]=z[85] - 6*z[94] + z[54] + z[99];
    z[85]=2*z[32];
    z[54]=z[54]*z[85];
    z[99]=4*z[5];
    z[102]= - z[22]*z[99];
    z[108]=3*z[77];
    z[109]=z[105]*npow(z[15],3)*z[108];
    z[102]=z[102] + z[109];
    z[85]=z[102]*z[85];
    z[102]=npow(z[15],2);
    z[36]= - z[102]*z[87]*z[36];
    z[109]=z[11]*z[16];
    z[115]=npow(z[16],2);
    z[116]=z[109] + z[115];
    z[117]=z[116]*z[11];
    z[118]=z[115]*z[7];
    z[118]=z[118] + z[117];
    z[119]=z[118]*z[2];
    z[105]=z[105]*npow(z[12],2)*z[119];
    z[36]=z[36] + n<T>(25,3)*z[105];
    z[36]=z[12]*z[36];
    z[36]=z[85] + z[36];
    z[36]=z[12]*z[36];
    z[36]=z[52] + z[41] + z[54] + z[36];
    z[36]=z[1]*z[36];
    z[41]=z[22]*z[89];
    z[41]=z[41] + n<T>(1,3)*z[75];
    z[52]=z[27]*z[15];
    z[54]=z[52]*z[49];
    z[41]=2*z[41] - z[54];
    z[75]=2*z[15];
    z[41]=z[41]*z[75];
    z[85]=z[56]*z[11];
    z[105]= - z[85] - 15*z[94];
    z[120]=z[10]*z[44];
    z[41]=z[41] + 2*z[105] + n<T>(17,3)*z[120];
    z[41]=z[15]*z[41];
    z[105]=5*z[14];
    z[120]= - z[96] - z[105] - z[84] + n<T>(5,3)*z[7];
    z[85]=z[85] - z[31];
    z[85]=z[15]*z[85];
    z[85]= - n<T>(13,3)*z[44] + z[85];
    z[85]=z[15]*z[85];
    z[85]= - 7*z[90] + z[85];
    z[85]=z[85]*z[48];
    z[41]=z[85] + z[41] + n<T>(31,3)*z[91] + 2*z[120] + 9*z[10];
    z[41]=z[4]*z[41];
    z[85]= - z[77]*z[27];
    z[90]=z[27]*z[11];
    z[91]=z[27]*z[2];
    z[120]= - z[90] - z[91];
    z[120]=z[10]*z[120];
    z[121]=z[11]*z[91];
    z[85]=z[120] + z[85] + z[121];
    z[85]=z[85]*z[102];
    z[76]= - z[76] + z[85];
    z[85]=3*z[15];
    z[76]=z[76]*z[85];
    z[120]=14*z[14];
    z[121]=9*z[7] + z[120];
    z[121]=z[121]*z[72];
    z[122]= - z[17] - z[5];
    z[122]=2*z[122] + n<T>(1,3)*z[7];
    z[122]=4*z[122] + z[49];
    z[122]=z[2]*z[122];
    z[123]=6*z[14];
    z[124]=z[17] + z[123];
    z[124]=z[124]*z[73];
    z[125]=z[49]*z[5];
    z[41]=z[41] + z[76] + z[124] + z[122] + z[121] - z[125];
    z[41]=z[4]*z[41];
    z[76]=n<T>(13,3)*z[31] + z[52];
    z[76]=z[15]*z[76];
    z[44]=7*z[44] + z[76];
    z[76]=4*z[4];
    z[44]=z[44]*z[76];
    z[121]= - z[5] - 4*z[7];
    z[121]=z[121]*z[72];
    z[122]=n<T>(7,3)*z[7] + z[84] + z[40];
    z[122]=z[122]*z[21];
    z[90]=z[102]*z[90];
    z[44]=z[44] + n<T>(37,3)*z[90] + z[122] + z[121] - z[125];
    z[44]=z[4]*z[44];
    z[90]=z[5]*z[22];
    z[44]=28*z[90] + z[44];
    z[44]=z[4]*z[44];
    z[27]=z[118]*z[27];
    z[90]=3*z[27];
    z[121]=z[91]*z[77];
    z[122]=z[90] - n<T>(29,3)*z[121];
    z[122]=z[15]*z[122];
    z[31]= - 7*z[31] - n<T>(13,3)*z[52];
    z[31]=z[31]*z[48];
    z[31]=z[54] + z[31];
    z[31]=z[31]*z[32];
    z[52]=z[7]*z[16];
    z[54]= - 49*z[16] + 25*z[11];
    z[54]=z[11]*z[54];
    z[54]= - 49*z[52] + z[54];
    z[54]=z[54]*z[91];
    z[27]= - 25*z[27] + z[54];
    z[54]=n<T>(1,3)*z[12];
    z[27]=z[27]*z[54];
    z[27]=z[27] + z[122] + z[31];
    z[27]=z[12]*z[27];
    z[31]= - z[90] + n<T>(2,3)*z[121];
    z[31]=z[31]*z[102];
    z[27]=z[27] + z[44] + n<T>(16,3)*z[119] + z[31];
    z[27]=z[12]*z[27];
    z[31]=z[10]*z[18];
    z[44]=z[31]*z[35];
    z[54]= - z[22]*z[28];
    z[90]=npow(z[18],2);
    z[121]=n<T>(4,3)*z[90];
    z[122]= - z[121] + z[22];
    z[124]=5*z[2];
    z[122]=z[122]*z[124];
    z[54]=z[44] + z[54] + z[122];
    z[54]=z[10]*z[54];
    z[122]=8*z[18];
    z[125]=z[122] + z[42];
    z[126]=z[10]*z[125];
    z[127]=6*z[77];
    z[128]=z[14]*z[19];
    z[129]=3*z[128];
    z[130]=z[18]*z[47];
    z[126]=z[126] - z[127] + z[130] + z[129];
    z[126]=z[15]*z[91]*z[126];
    z[130]=npow(z[10],2);
    z[131]=n<T>(2,3)*z[8];
    z[91]=z[130]*z[91]*z[131];
    z[132]=z[72]*z[19];
    z[132]=z[132] + z[29];
    z[132]=z[132]*z[22]*z[2];
    z[133]=z[10]*z[21]*npow(z[14],3);
    z[132]=z[132] + z[133];
    z[91]=3*z[132] + z[91];
    z[91]=2*z[91] + z[126];
    z[91]=z[15]*z[91];
    z[126]=z[37]*z[18];
    z[126]=z[126] + n<T>(5,3)*z[90];
    z[133]=z[126]*z[99];
    z[134]=z[86] + 17*z[128];
    z[134]=z[14]*z[134];
    z[134]= - z[133] + z[134];
    z[134]=z[2]*z[134];
    z[135]=z[39]*z[29];
    z[136]=z[128]*z[7];
    z[135]=z[135] + z[136];
    z[136]= - z[135]*z[42];
    z[54]=z[91] + z[54] + z[136] + z[134];
    z[54]=z[15]*z[54];
    z[91]=n<T>(5,3)*z[18];
    z[134]=z[91] - z[47];
    z[134]=z[5]*z[134];
    z[136]=9*z[29];
    z[134]=n<T>(1,3)*z[52] + z[136] + z[134];
    z[137]=10*z[14];
    z[138]= - z[33] - z[137];
    z[138]=z[14]*z[138];
    z[139]=n<T>(2,3)*z[16] + z[49];
    z[139]=z[11]*z[139];
    z[134]=z[139] + 2*z[134] + z[138];
    z[134]=z[2]*z[134];
    z[78]= - z[78] + z[49];
    z[78]=z[11]*z[78];
    z[138]=z[18] - z[14];
    z[138]=z[7]*z[138];
    z[139]=n<T>(10,3)*z[2];
    z[140]=z[18]*z[139];
    z[78]=z[140] + 3*z[138] + z[78];
    z[78]=z[10]*z[78];
    z[138]= - n<T>(8,3)*z[115] - z[136];
    z[140]=z[40]*z[18];
    z[138]=2*z[138] + z[140];
    z[138]=z[7]*z[138];
    z[141]=z[7]*z[19];
    z[142]=z[56]*z[141];
    z[26]=z[36] + z[26] + z[27] + z[41] + z[54] + z[78] + z[134] - n<T>(16,3)
   *z[117] + z[138] + z[142];
    z[26]=z[1]*z[26];
    z[27]=npow(z[9],2);
    z[36]=n<T>(7,3)*z[27] + n<T>(20,3)*z[90] - 11*z[22];
    z[36]=z[36]*z[21];
    z[41]=z[49] - z[125];
    z[41]=z[41]*z[27];
    z[54]= - z[7]*z[22];
    z[41]=z[54] + z[41];
    z[54]=n<T>(8,3)*z[2];
    z[31]= - z[31]*z[54];
    z[31]=z[31] + 3*z[41] + z[36];
    z[31]=z[10]*z[31];
    z[36]= - z[18]*z[99];
    z[36]=z[71] + z[36];
    z[41]= - z[14] + z[87];
    z[41]=z[11]*z[41];
    z[36]=z[41] + 2*z[36] - z[129];
    z[36]=z[36]*z[27];
    z[41]= - z[14]*z[135];
    z[36]=z[41] + z[36];
    z[41]=z[126]*z[37];
    z[78]=z[71] - z[128];
    z[78]=z[14]*z[78];
    z[41]=z[41] + z[78];
    z[78]=z[33] + z[47];
    z[125]=z[78] - n<T>(37,3)*z[11];
    z[125]=z[125]*z[27];
    z[41]=2*z[41] + z[125];
    z[41]=z[41]*z[21];
    z[125]=6*z[15];
    z[125]=z[132]*z[125];
    z[31]=z[125] + z[31] + 3*z[36] + z[41];
    z[31]=z[15]*z[31];
    z[36]=z[13]*z[18];
    z[41]=z[47]*z[36];
    z[125]=z[13]*z[38];
    z[125]=z[125] - z[59];
    z[125]=z[14]*z[125];
    z[126]=z[24] - 5*z[66];
    z[126]=z[11]*z[126];
    z[41]=z[126] + z[41] + z[125];
    z[41]=z[41]*z[27];
    z[125]= - z[2] + 2*z[18];
    z[126]=n<T>(4,3)*z[10];
    z[125]=z[125]*z[126];
    z[125]=z[125] - n<T>(64,3)*z[90];
    z[129]=z[53]*z[18];
    z[132]=z[129] + z[24];
    z[132]=2*z[132] - z[66];
    z[132]=z[132]*z[27];
    z[134]=21*z[22];
    z[122]=z[122] - 9*z[14];
    z[122]=z[2]*z[122];
    z[122]=z[122] + 6*z[132] + z[134] + z[125];
    z[122]=z[10]*z[122];
    z[86]=33*z[128] + z[86] + 13*z[141];
    z[86]=z[14]*z[86];
    z[132]=z[18] + z[99];
    z[132]=z[132]*z[99];
    z[132]=z[71] + z[132];
    z[135]=22*z[14];
    z[138]= - 41*z[19] - z[135];
    z[138]=z[14]*z[138];
    z[132]= - 12*z[77] + 2*z[132] + z[138];
    z[132]=z[2]*z[132];
    z[138]=z[140] + n<T>(8,3)*z[90];
    z[138]=z[138]*z[47];
    z[140]=3*z[115] - 8*z[29];
    z[140]=z[140]*z[28];
    z[142]=z[27]*z[2];
    z[143]= - z[10]*z[27];
    z[143]= - z[142] + z[143];
    z[143]=z[143]*z[74];
    z[31]=z[31] + n<T>(8,3)*z[143] + z[122] + z[132] + 3*z[41] + 9*z[117] + 
    z[86] - z[138] + z[140];
    z[31]=z[15]*z[31];
    z[41]=z[37] + z[28];
    z[41]=z[41]*z[42];
    z[86]=7*z[7];
    z[84]= - z[86] - z[84] - 9*z[5];
    z[84]=z[2]*z[84];
    z[41]=z[41] + z[84];
    z[84]=z[27]*z[15];
    z[122]=z[27]*z[8];
    z[132]=z[11] + z[124];
    z[132]= - n<T>(11,3)*z[84] + 3*z[132] - 28*z[122];
    z[132]=z[4]*z[132];
    z[84]=z[11]*z[84];
    z[41]=z[132] + 2*z[41] + n<T>(5,3)*z[84];
    z[41]=z[4]*z[41];
    z[84]=9*z[16];
    z[132]=2*z[20];
    z[140]= - z[132] + z[84];
    z[140]=z[7]*z[140];
    z[109]=n<T>(17,3)*z[32] + 9*z[109] + z[140] + z[57];
    z[109]=z[27]*z[109];
    z[140]=n<T>(1,3)*z[27];
    z[143]=43*z[7] - 49*z[11];
    z[143]=z[143]*z[140];
    z[118]=19*z[118] + z[143];
    z[118]=z[2]*z[118];
    z[109]=z[118] + z[109];
    z[109]=z[12]*z[109];
    z[118]= - z[16]*z[28];
    z[118]=z[70] + z[118];
    z[143]= - 3*z[16] + z[11];
    z[143]=z[143]*z[87];
    z[118]=z[143] + 2*z[118] - 9*z[22];
    z[118]=z[118]*z[21];
    z[143]=2*z[29];
    z[144]=z[143] - z[52];
    z[145]=n<T>(49,3)*z[11];
    z[84]= - z[84] + z[145];
    z[84]=z[11]*z[84];
    z[57]=z[84] + 9*z[144] + z[57];
    z[57]=z[57]*z[27];
    z[78]=n<T>(31,3)*z[11] + z[78];
    z[78]=z[78]*z[142];
    z[57]=z[57] + z[78];
    z[57]=z[15]*z[57];
    z[78]=z[27]*z[13];
    z[84]= - 18*z[78] - 20*z[5];
    z[84]=z[22]*z[84];
    z[144]= - 19*z[115] - 30*z[29];
    z[144]=z[7]*z[144];
    z[146]=z[27]*z[20];
    z[147]=6*z[7];
    z[148]= - z[147]*z[146];
    z[149]=z[142]*z[7];
    z[148]=z[148] - n<T>(13,3)*z[149];
    z[148]=z[8]*z[148];
    z[41]=z[109] + z[41] + z[57] + z[148] + z[118] - 19*z[117] + z[144]
    + z[84];
    z[41]=z[12]*z[41];
    z[57]= - z[29]*z[147];
    z[84]= - z[39] - z[42];
    z[84]=z[84]*z[27];
    z[109]=3*z[22];
    z[118]= - z[109] - n<T>(1,3)*z[77];
    z[118]=z[118]*z[21];
    z[144]= - z[2]*z[48];
    z[140]=z[140] + z[144];
    z[140]=z[4]*z[140];
    z[57]=z[140] + z[118] + z[57] + z[84];
    z[57]=z[57]*z[30];
    z[84]=12*z[14];
    z[118]=9*z[19];
    z[140]= - z[11] + z[84] + n<T>(19,3)*z[5] - z[50] - z[118];
    z[140]=z[2]*z[140];
    z[144]= - z[6] - z[99];
    z[144]=z[5]*z[144];
    z[144]= - z[136] + z[144];
    z[148]=z[56] - z[7];
    z[150]= - z[148]*z[72];
    z[151]=8*z[27];
    z[152]=z[24]*z[151];
    z[153]= - z[122]*z[147];
    z[154]= - z[40] - n<T>(17,3)*z[2];
    z[154]=n<T>(34,3)*z[4] + 2*z[154] - n<T>(5,3)*z[122];
    z[154]=z[4]*z[154];
    z[110]=z[110] + n<T>(35,3)*z[11];
    z[110]=z[11]*z[110];
    z[57]=z[57] + z[154] + z[153] + z[140] + z[152] + z[110] + z[150] + 
   2*z[144] + 11*z[141];
    z[57]=z[12]*z[57];
    z[110]=z[39] - z[14];
    z[110]=z[110]*z[56];
    z[140]=n<T>(2,3)*z[5] + z[14];
    z[140]=z[2]*z[140];
    z[110]=z[140] + z[27] + z[141] + z[110];
    z[140]= - z[32]*z[96];
    z[23]= - z[23] + z[140];
    z[23]=z[23]*z[30];
    z[140]=z[76] - z[5] - z[139];
    z[140]=z[4]*z[140];
    z[23]=z[23] + 2*z[110] + z[140];
    z[23]=z[12]*z[23];
    z[110]= - z[50] - z[33];
    z[140]= - static_cast<T>(12)+ n<T>(41,3)*z[45];
    z[140]=z[4]*z[140];
    z[23]=z[23] + z[140] - n<T>(4,3)*z[122] - z[54] - 6*z[78] + n<T>(14,3)*z[11]
    + 18*z[14] - z[147] + 2*z[110] + n<T>(23,3)*z[5];
    z[23]=z[12]*z[23];
    z[54]= - z[93] + static_cast<T>(2)+ z[64];
    z[54]=z[54]*z[43];
    z[93]= - static_cast<T>(3)+ z[93];
    z[93]=z[93]*z[63];
    z[110]= - z[65] + z[45];
    z[110]=z[12]*z[110];
    z[110]=z[111] + z[110];
    z[110]=z[12]*z[110];
    z[54]=z[110] + z[54] + z[93];
    z[54]=z[54]*z[67];
    z[93]=2*z[43];
    z[110]= - n<T>(29,3) + 8*z[74];
    z[110]=z[110]*z[93];
    z[140]=static_cast<T>(11)- z[81];
    z[140]=z[140]*z[63];
    z[110]=z[110] + n<T>(1,3)*z[140];
    z[110]=z[110]*z[48];
    z[140]=z[12]*z[7];
    z[140]=z[140] + 1;
    z[144]=8*z[14];
    z[150]=z[144]*z[140];
    z[32]= - z[32]*z[25];
    z[152]=5*z[7];
    z[32]=z[32] - z[152] + z[150];
    z[32]=z[12]*z[32];
    z[150]= - z[8] + z[80];
    z[107]=z[150]*z[107];
    z[32]=z[32] - static_cast<T>(3)+ z[107];
    z[32]=z[12]*z[32];
    z[107]= - 50*z[74] + static_cast<T>(13)+ 34*z[61];
    z[107]=z[107]*z[131];
    z[32]=z[54] + z[32] + z[110] + z[107] + 7*z[13] - n<T>(10,3)*z[101];
    z[32]=z[3]*z[32];
    z[54]=z[88] + 2*z[79] + n<T>(5,3)*z[27];
    z[54]=z[8]*z[54];
    z[88]= - z[27]*z[68];
    z[88]= - z[92] - z[7] + z[88];
    z[54]=2*z[88] + z[54];
    z[54]=z[8]*z[54];
    z[58]=static_cast<T>(1)+ z[58];
    z[58]=z[58]*z[53];
    z[58]=z[58] + z[82];
    z[58]=z[10]*z[58];
    z[88]=n<T>(68,3) - z[74];
    z[88]=z[88]*z[43];
    z[88]=z[88] - 8*z[63];
    z[88]=z[4]*z[88];
    z[92]= - static_cast<T>(17)+ 13*z[74];
    z[92]=z[8]*z[92];
    z[88]=z[92] + z[88];
    z[88]=z[4]*z[88];
    z[23]=z[32] + z[23] + z[88] + z[54] + z[58] - n<T>(17,3)*z[66] - 4*z[24]
    - static_cast<T>(7)+ z[69];
    z[23]=z[3]*z[23];
    z[32]=6*z[19];
    z[54]=16*z[5] - z[32] - z[50] - z[83];
    z[58]=z[74]*z[27];
    z[69]=z[6] - n<T>(1,3)*z[78];
    z[69]=z[10]*z[69];
    z[69]=n<T>(11,3)*z[58] + z[79] + z[69];
    z[69]=z[69]*z[25];
    z[83]=n<T>(17,3) - z[24];
    z[83]=2*z[83] - n<T>(41,3)*z[66];
    z[83]=z[11]*z[83];
    z[88]=z[43]*z[48];
    z[92]= - n<T>(53,3) + 9*z[74];
    z[92]=z[8]*z[92];
    z[88]=z[92] + z[88];
    z[88]=z[4]*z[88];
    z[92]=n<T>(46,3) - 5*z[74];
    z[88]=2*z[92] + z[88];
    z[88]=z[4]*z[88];
    z[92]= - z[13] + n<T>(10,3)*z[82];
    z[92]=z[11]*z[92];
    z[92]=n<T>(13,3) + z[92];
    z[92]=z[10]*z[92];
    z[23]=z[23] + z[57] + z[88] + z[69] + z[92] + z[83] + z[72] + 2*
    z[54] - z[28] - 6*z[2];
    z[23]=z[3]*z[23];
    z[54]=z[68]*z[90];
    z[57]=3*z[18];
    z[54]=z[54] + z[57];
    z[69]= - z[54]*z[53];
    z[83]= - n<T>(7,3) + z[129];
    z[83]=z[83]*z[66];
    z[69]=z[83] - static_cast<T>(3)+ z[69];
    z[69]=z[11]*z[69];
    z[83]=z[49] + z[20] - 4*z[18];
    z[83]=z[27]*z[60]*z[83];
    z[69]= - z[139] + 2*z[83] + z[69] - z[120] + z[132] - z[91];
    z[69]=z[10]*z[69];
    z[54]= - z[13]*z[54];
    z[83]=z[60]*z[18];
    z[88]= - z[99]*z[83];
    z[54]=z[54] + z[88];
    z[37]=z[54]*z[37];
    z[54]=z[5]*z[83];
    z[54]=static_cast<T>(3)+ z[54];
    z[54]=z[54]*z[87];
    z[88]=5*z[16];
    z[37]=z[54] - z[72] + z[37] - z[106] + z[88];
    z[37]=z[11]*z[37];
    z[40]=z[96] - z[87] + 11*z[14] + n<T>(4,3)*z[7] - z[50] + z[40];
    z[50]=z[135] + z[49];
    z[50]=z[10]*z[50];
    z[54]=z[105] - 6*z[11];
    z[54]=z[11]*z[54];
    z[92]= - z[2]*z[49];
    z[50]=z[50] + z[92] + 8*z[22] + z[54];
    z[54]=z[27]*z[11];
    z[22]=z[10]*z[22];
    z[22]= - 7*z[54] - 12*z[22];
    z[22]=z[15]*z[22];
    z[22]=z[22] + 2*z[50] - z[58];
    z[22]=z[15]*z[22];
    z[50]= - z[11]*z[137];
    z[50]= - 10*z[94] + z[50] - n<T>(41,3)*z[27];
    z[50]=z[15]*z[50];
    z[50]=z[50] - 5*z[122] + 22*z[10] + 9*z[2] - z[56] - 7*z[11];
    z[50]=z[15]*z[50];
    z[54]= - z[102]*z[97];
    z[58]=9*z[8];
    z[54]=z[58] + z[54];
    z[54]=z[54]*z[48];
    z[92]=n<T>(29,3)*z[10] + 6*z[122];
    z[92]=z[8]*z[92];
    z[92]= - n<T>(70,3) + z[92];
    z[50]=z[54] + 2*z[92] + z[50];
    z[50]=z[4]*z[50];
    z[54]= - z[43]*z[151];
    z[54]=z[54] - 23;
    z[54]=z[10]*z[54];
    z[22]=z[50] + z[22] + 2*z[40] + z[54];
    z[22]=z[4]*z[22];
    z[40]= - z[27]*z[47]*z[83];
    z[50]=z[7]*z[20];
    z[54]= - z[27]*z[50];
    z[83]= - z[146] - z[142];
    z[83]=z[10]*z[83];
    z[54]=z[83] + z[54] - z[149];
    z[54]=z[8]*z[54];
    z[27]=z[27]*z[61];
    z[83]=z[6] + z[78];
    z[83]=z[20]*z[83];
    z[27]=n<T>(2,3)*z[27] + z[83];
    z[27]=z[10]*z[27];
    z[79]=z[20]*z[79];
    z[27]=z[54] + z[79] + z[27];
    z[27]=z[27]*z[25];
    z[54]= - 36*z[14] - 26*z[7] + z[118] + z[99];
    z[54]=z[14]*z[54];
    z[79]=15*z[11];
    z[83]= - z[79] + 26*z[14] - z[147] - 34*z[19] + n<T>(53,3)*z[5];
    z[83]=z[2]*z[83];
    z[91]= - z[91] - 32*z[5];
    z[91]=z[5]*z[91];
    z[92]=19*z[19] - 4*z[20] + z[88];
    z[92]=z[7]*z[92];
    z[22]=z[26] + z[23] + z[41] + z[22] + z[31] + z[27] + z[69] + z[83]
    + z[40] + z[37] + z[54] + z[91] + z[92];
    z[22]=z[1]*z[22];
    z[23]=n<T>(29,3)*z[18];
    z[26]= - z[23] + 7*z[14];
    z[26]=z[26]*z[21];
    z[26]=z[26] - z[108] + z[134] - z[125];
    z[26]=z[10]*z[26];
    z[27]= - z[143] - 5*z[128];
    z[27]=z[27]*z[42];
    z[27]= - z[133] + z[27];
    z[27]=z[2]*z[27];
    z[31]= - z[121] - z[109];
    z[31]=z[31]*z[124];
    z[31]=z[31] + z[44];
    z[31]=z[10]*z[31];
    z[27]=z[27] + z[31];
    z[27]=z[15]*z[27];
    z[31]=6*z[29];
    z[37]=11*z[128];
    z[40]=z[37] + z[31] + z[141];
    z[40]=z[40]*z[42];
    z[23]= - z[23] - z[47];
    z[23]=z[5]*z[23];
    z[23]= - z[31] + z[23];
    z[23]=15*z[77] + 2*z[23] - z[37];
    z[23]=z[2]*z[23];
    z[31]= - z[115] - z[143];
    z[28]=z[31]*z[28];
    z[31]= - z[116]*z[49];
    z[23]=z[27] + z[26] + z[23] + z[31] + z[40] + z[138] + z[28];
    z[23]=z[15]*z[23];
    z[26]=z[90]*z[13];
    z[26]= - 53*z[18] + 34*z[26];
    z[26]=n<T>(1,3)*z[26];
    z[27]= - z[26] - z[135];
    z[28]= - static_cast<T>(1)+ z[36];
    z[28]=z[28]*z[126];
    z[31]= - n<T>(25,3) + 6*z[66];
    z[31]=z[11]*z[31];
    z[27]=z[28] + z[113] + 2*z[27] + z[31];
    z[27]=z[10]*z[27];
    z[28]=static_cast<T>(4)- 3*z[36];
    z[28]=z[28]*z[99];
    z[26]= - z[26] + z[28];
    z[26]=z[5]*z[26];
    z[26]=z[136] + z[26];
    z[28]= - z[144] - z[33] - z[7];
    z[28]=z[28]*z[123];
    z[31]=n<T>(32,3)*z[11];
    z[37]= - 2*z[16] + z[14];
    z[37]=3*z[37] - z[31];
    z[37]=z[11]*z[37];
    z[35]= - z[8]*z[130]*z[35];
    z[40]= - 6*z[16] + 13*z[19];
    z[40]=z[7]*z[40];
    z[38]=39*z[11] + 27*z[14] + z[38] - n<T>(148,3)*z[5];
    z[38]=z[2]*z[38];
    z[23]=z[23] + z[35] + z[27] + z[38] + z[37] + z[28] + 2*z[26] + 
    z[40];
    z[23]=z[15]*z[23];
    z[26]= - 11*z[115] - z[71];
    z[26]=z[7]*z[26];
    z[26]=z[26] - 11*z[117];
    z[27]= - 29*z[16] + n<T>(74,3)*z[11];
    z[27]=z[11]*z[27];
    z[27]= - 29*z[52] + z[27];
    z[27]=z[2]*z[27];
    z[28]=z[12]*z[119];
    z[26]=22*z[28] + 2*z[26] + z[27];
    z[26]=z[12]*z[26];
    z[27]=z[115] - z[29];
    z[27]=z[7]*z[27];
    z[27]=z[27] + z[117];
    z[28]= - z[71] - z[70];
    z[28]=2*z[28] - z[108];
    z[28]=z[2]*z[28];
    z[27]=12*z[27] + z[28];
    z[27]=z[15]*z[27];
    z[28]= - z[15]*z[127];
    z[28]= - z[145] + z[28];
    z[28]=z[15]*z[28];
    z[29]=z[15]*z[97];
    z[29]=static_cast<T>(9)+ z[29];
    z[29]=z[15]*z[29];
    z[29]=z[25] + z[29];
    z[29]=z[29]*z[48];
    z[28]=z[29] - static_cast<T>(5)+ z[28];
    z[28]=z[4]*z[28];
    z[29]=z[72] - n<T>(1,3)*z[11];
    z[35]= - z[15]*z[108];
    z[28]=z[28] + z[35] + 2*z[29] - 17*z[2];
    z[28]=z[4]*z[28];
    z[29]=z[5]*z[13];
    z[35]= - static_cast<T>(2)+ z[29];
    z[35]=z[35]*z[84];
    z[35]=z[35] - 22*z[5] - 15*z[7];
    z[35]=z[14]*z[35];
    z[37]= - z[20] + z[88];
    z[37]=4*z[37] + 17*z[19];
    z[37]=z[7]*z[37];
    z[38]= - z[6] + 10*z[16];
    z[38]=2*z[38] - n<T>(79,3)*z[11];
    z[38]=z[11]*z[38];
    z[40]=13*z[14];
    z[41]= - n<T>(59,3)*z[11] + z[40] + 19*z[7] + 7*z[19] - n<T>(52,3)*z[5];
    z[41]=z[2]*z[41];
    z[26]=z[26] + z[28] + z[27] + z[41] + z[38] + z[35] + 8*z[46] + 
    z[37];
    z[26]=z[12]*z[26];
    z[27]= - z[148]*z[56];
    z[28]=4*z[2];
    z[35]= - z[28] - z[4];
    z[35]=z[35]*z[48];
    z[27]=z[35] - z[55] + 7*z[141] + z[27];
    z[27]=z[12]*z[27];
    z[35]=12*z[8] - 7*z[80];
    z[35]=z[35]*z[48];
    z[35]=static_cast<T>(3)+ z[35];
    z[35]=z[4]*z[35];
    z[37]=3*z[51];
    z[27]=z[27] + z[35] - z[28] - z[31] + z[137] - z[86] + z[37] + n<T>(40,3)
   *z[5];
    z[27]=z[12]*z[27];
    z[28]=z[14]*z[140];
    z[28]= - z[7] + z[28];
    z[28]=z[12]*z[28];
    z[28]=z[28] - static_cast<T>(1)+ z[24];
    z[31]=z[25] - z[80];
    z[31]=z[31]*z[48];
    z[28]=z[31] - 9*z[65] + 4*z[28];
    z[28]=z[12]*z[28];
    z[31]= - 47*z[43] + 11*z[63];
    z[31]=z[31]*z[98];
    z[35]=5*z[8];
    z[28]=z[28] + z[31] - z[68] - z[35];
    z[28]=z[12]*z[28];
    z[31]=z[74] - static_cast<T>(2)- z[61];
    z[31]=z[31]*z[62];
    z[38]=static_cast<T>(3)- z[74];
    z[38]=z[4]*z[38]*z[104];
    z[41]= - z[111] + z[80];
    z[41]=z[12]*z[41];
    z[41]= - 3*z[63] + z[41];
    z[41]=z[12]*z[41];
    z[31]=z[41] + z[31] + z[38];
    z[31]=z[31]*z[67];
    z[38]= - static_cast<T>(7)- z[74];
    z[34]=z[38]*z[104]*z[34];
    z[38]=n<T>(68,3) - 7*z[74];
    z[38]=z[38]*z[62];
    z[34]=z[38] + z[34];
    z[34]=z[34]*z[48];
    z[38]=22*z[74] - static_cast<T>(32)- 11*z[61];
    z[38]=z[38]*z[131];
    z[38]=z[38] + n<T>(11,3)*z[101] - 3*z[13];
    z[38]=z[8]*z[38];
    z[28]=z[31] + z[28] + z[38] + z[34];
    z[28]=z[3]*z[28];
    z[31]=static_cast<T>(9)- z[59];
    z[31]=z[14]*z[31];
    z[31]= - z[147] + z[31];
    z[34]=z[152] - z[56];
    z[34]=z[14]*z[34];
    z[38]= - z[4] - z[14];
    z[38]=z[2]*z[38];
    z[34]=z[34] + z[38];
    z[30]=z[34]*z[30];
    z[34]=static_cast<T>(3)- 4*z[45];
    z[34]=z[4]*z[34];
    z[30]=z[30] + 2*z[31] + z[34];
    z[30]=z[12]*z[30];
    z[31]=13*z[43] - 4*z[63];
    z[31]=z[31]*z[48];
    z[31]= - n<T>(77,3)*z[8] + z[31];
    z[31]=z[4]*z[31];
    z[34]=12*z[24];
    z[30]=z[30] + z[31] - 5*z[65] - static_cast<T>(17)+ z[34];
    z[30]=z[12]*z[30];
    z[31]= - static_cast<T>(62)- z[74];
    z[31]=z[31]*z[62];
    z[38]=z[104]*z[76];
    z[31]=n<T>(1,3)*z[31] + z[38];
    z[31]=z[4]*z[31];
    z[38]=n<T>(61,3) - z[81];
    z[38]=z[38]*z[93];
    z[31]=z[38] + z[31];
    z[31]=z[31]*z[48];
    z[38]=z[60]*z[73];
    z[38]= - 17*z[13] + z[38];
    z[41]= - z[7] + n<T>(16,3)*z[10];
    z[41]=z[41]*z[25];
    z[41]=z[41] - n<T>(92,3) - z[64];
    z[41]=z[8]*z[41];
    z[28]=z[28] + z[30] + z[31] + n<T>(1,3)*z[38] + z[41];
    z[28]=z[3]*z[28];
    z[30]= - z[53] - n<T>(19,3)*z[82];
    z[30]=z[10]*z[30];
    z[31]= - z[7] - n<T>(1,3)*z[2];
    z[31]=z[112] + 5*z[31] + n<T>(7,3)*z[10];
    z[31]=z[31]*z[25];
    z[38]= - static_cast<T>(26)+ z[74];
    z[38]=z[38]*z[43];
    z[38]=z[38] + 7*z[63];
    z[38]=z[38]*z[48];
    z[41]=static_cast<T>(9)- z[81];
    z[41]=z[8]*z[41];
    z[38]=3*z[41] + z[38];
    z[38]=z[4]*z[38];
    z[37]= - z[13]*z[37];
    z[27]=z[28] + z[27] + z[38] + z[31] + z[30] + 16*z[66] + n<T>(25,3)*
    z[29] - static_cast<T>(8)+ z[37];
    z[27]=z[3]*z[27];
    z[28]=z[2]*z[7];
    z[28]= - z[50] + z[28];
    z[30]= - z[124] + z[89];
    z[30]=z[10]*z[30];
    z[31]=z[20]*z[112];
    z[28]=z[31] + 3*z[28] + n<T>(1,3)*z[30];
    z[28]=z[28]*z[25];
    z[30]=static_cast<T>(14)+ 25*z[74];
    z[30]=z[30]*z[103];
    z[31]=static_cast<T>(24)+ n<T>(17,3)*z[74];
    z[37]=28*z[11] + 5*z[10];
    z[37]=z[15]*z[37];
    z[31]=2*z[31] + z[37];
    z[31]=z[15]*z[31];
    z[37]= - z[58] - 4*z[15];
    z[37]=z[15]*z[37];
    z[37]= - z[93] + z[37];
    z[37]=z[37]*z[48];
    z[30]=z[37] + z[30] + z[31];
    z[30]=z[4]*z[30];
    z[31]=z[56] + z[79];
    z[31]=z[11]*z[31];
    z[37]=z[10]*z[144];
    z[31]=z[31] + z[37];
    z[31]=z[15]*z[31];
    z[37]=z[2] - z[11];
    z[31]=z[31] - 25*z[10] + z[120] - 13*z[37];
    z[31]=z[15]*z[31];
    z[30]=z[30] + z[31] + n<T>(73,3) - 23*z[74];
    z[30]=z[4]*z[30];
    z[31]=z[90]*z[53];
    z[31]=z[31] - z[57];
    z[31]=z[31]*z[68];
    z[37]=z[47]*z[13];
    z[38]= - static_cast<T>(2)+ z[36];
    z[38]=z[38]*z[37];
    z[38]=z[38] + n<T>(175,3) + z[31];
    z[38]=z[5]*z[38];
    z[33]=z[13]*z[33];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]= - 6*z[24] + 5*z[33] - z[37];
    z[33]=z[14]*z[33];
    z[36]=static_cast<T>(4)+ z[36];
    z[36]=z[5]*z[36]*z[53];
    z[36]=n<T>(31,3)*z[66] - z[59] - n<T>(5,3) + z[36];
    z[36]=z[36]*z[87];
    z[37]=z[18]*z[68];
    z[37]=n<T>(17,3) + z[37];
    z[37]=z[13]*z[37];
    z[37]=z[37] - 9*z[82];
    z[37]=z[11]*z[37];
    z[31]=z[37] + z[34] + static_cast<T>(9)+ z[31];
    z[31]=z[10]*z[31];
    z[21]=z[22] + z[27] + z[26] + z[30] + z[23] + z[28] + z[31] - z[21]
    + z[36] + z[33] - n<T>(19,3)*z[7] + z[38] - z[132] - 45*z[19];
    z[21]=z[1]*z[21];
    z[22]=z[9]*z[20];
    z[23]= - z[39]*z[22];
    z[26]= - z[122]*z[50];
    z[23]=z[23] + z[26];
    z[23]=z[12]*z[23];
    z[26]= - z[50] - z[22];
    z[26]=z[26]*z[100];
    z[23]=z[23] - 2*z[22] + z[26];
    z[23]=z[25]*z[23];
    z[26]= - z[95] - z[20] - z[118];
    z[27]=z[11] + z[19];
    z[28]= - z[9]*z[27];
    z[28]= - z[77] + z[28];
    z[28]=z[28]*z[75];
    z[28]=3*z[27] + z[28];
    z[28]=z[85]*z[9]*z[28];
    z[23]=z[28] + n<T>(43,3)*z[2] - n<T>(2,3)*z[11] + z[40] + 2*z[26] - n<T>(49,3)*
    z[7] + z[23];
    z[23]=z[12]*z[23];
    z[26]= - z[7] - z[9];
    z[26]=z[26]*z[100];
    z[28]= - z[9]*z[39];
    z[30]= - z[7]*z[122];
    z[28]=z[28] + z[30];
    z[28]=z[12]*z[28];
    z[26]=z[28] - 2*z[9] + z[26];
    z[26]=z[8]*z[26];
    z[26]= - n<T>(1,3) + z[26];
    z[26]=z[12]*z[26];
    z[28]= - z[13] - z[101];
    z[28]=z[43]*z[28];
    z[30]= - z[12]*z[111];
    z[30]= - z[43] + z[30];
    z[30]=z[12]*z[30];
    z[28]=z[28] + z[30];
    z[28]=z[3]*z[9]*z[28];
    z[30]=z[10]*z[9];
    z[31]= - z[9]*z[60];
    z[31]=z[13] + z[31];
    z[31]=z[31]*z[30];
    z[31]= - z[78] + z[31];
    z[31]=z[8]*z[31];
    z[33]= - z[9]*z[53];
    z[31]=z[31] - n<T>(5,3) + z[33];
    z[31]=z[8]*z[31];
    z[26]=z[28] + z[26] - z[13] + z[31];
    z[26]=z[26]*z[114];
    z[28]= - z[53]*z[22];
    z[28]= - z[20] + z[28];
    z[22]= - z[60]*z[22];
    z[31]=z[13]*z[20];
    z[22]=z[31] + z[22];
    z[22]=z[22]*z[30];
    z[30]= - z[20]*z[78];
    z[22]=z[30] + z[22];
    z[22]=z[22]*z[25];
    z[25]=z[10] - z[2];
    z[22]=z[22] + 2*z[28] + n<T>(13,3)*z[25];
    z[22]=z[8]*z[22];
    z[25]= - z[20] - z[32];
    z[25]=z[25]*z[53];
    z[28]= - z[13]*z[19];
    z[28]=z[28] - z[66];
    z[27]=z[15]*z[27];
    z[27]=18*z[27] + 15*z[28];
    z[27]=z[9]*z[27];
    z[27]= - n<T>(26,3)*z[10] - n<T>(29,3)*z[2] - n<T>(8,3)*z[11] - z[144] - 33*
    z[19] - 34*z[5] + z[27];
    z[27]=z[15]*z[27];
    z[28]= - z[35] - n<T>(23,3)*z[15];
    z[28]=z[28]*z[48];
    z[21]=z[21] + z[26] + z[23] + z[28] + z[27] + z[22] + 6*z[61] - n<T>(50,3)*z[66] + 16*z[24] + 18*z[29] - n<T>(7,3) + z[25];

    r += 2*z[21];
 
    return r;
}

template double qg_2lNLC_r1291(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1291(const std::array<dd_real,31>&);
#endif
