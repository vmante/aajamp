#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r130(const std::array<T,31>& k) {
  T z[101];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[8];
    z[6]=k[12];
    z[7]=k[14];
    z[8]=k[9];
    z[9]=k[13];
    z[10]=k[2];
    z[11]=k[10];
    z[12]=k[4];
    z[13]=k[15];
    z[14]=k[3];
    z[15]=npow(z[2],2);
    z[16]=npow(z[3],2);
    z[17]=z[15]*z[16];
    z[18]=z[3]*z[1];
    z[19]=npow(z[18],3);
    z[20]=3*z[3];
    z[21]=z[2]*z[20];
    z[21]=z[19] + z[21];
    z[21]=z[2]*z[21];
    z[21]= - n<T>(3,4)*z[1] + z[21];
    z[21]=z[7]*z[21];
    z[22]=npow(z[18],2);
    z[23]=n<T>(1,2)*z[22];
    z[21]=z[21] + z[23] + 3*z[17];
    z[21]=z[7]*z[21];
    z[24]=z[16]*z[2];
    z[25]=z[24] + z[3];
    z[26]=z[16]*z[12];
    z[27]=z[26] - z[25];
    z[28]=z[5]*z[7];
    z[29]=npow(z[1],2);
    z[30]=z[29]*z[7];
    z[31]=z[1] + z[30];
    z[31]=z[31]*z[28];
    z[21]=n<T>(3,4)*z[31] + 3*z[27] + z[21];
    z[27]=n<T>(1,2)*z[7];
    z[15]= - z[3]*z[15]*z[27];
    z[15]= - z[17] + z[15];
    z[15]=z[7]*z[15];
    z[31]=n<T>(1,2)*z[3];
    z[15]=z[15] + z[31] + 2*z[24];
    z[15]=z[7]*z[15];
    z[17]=z[27]*z[17];
    z[17]= - z[24] + z[17];
    z[17]=z[7]*z[17];
    z[17]=n<T>(1,2)*z[16] + z[17];
    z[17]=z[10]*z[17]*z[27];
    z[15]=z[17] - z[16] + z[15];
    z[15]=z[10]*z[15];
    z[15]=n<T>(1,2)*z[21] + z[15];
    z[15]=z[10]*z[15];
    z[17]=z[2]*z[3];
    z[21]=z[22] - n<T>(3,2)*z[17];
    z[21]=z[2]*z[21];
    z[32]=n<T>(3,4)*z[7];
    z[33]=z[2]*z[1];
    z[34]= - z[33]*z[32];
    z[21]=z[34] + n<T>(15,4)*z[1] + z[21];
    z[21]=z[21]*z[27];
    z[34]=n<T>(9,8)*z[5];
    z[30]=z[1] - z[30];
    z[30]=z[30]*z[34];
    z[35]=n<T>(9,4)*z[3] - z[26];
    z[35]=z[12]*z[35];
    z[15]=z[15] + z[30] + z[21] + n<T>(3,4)*z[17] - static_cast<T>(2)+ z[35];
    z[15]=z[10]*z[15];
    z[21]=z[12]*z[3];
    z[30]=z[19] + n<T>(3,2)*z[21];
    z[30]=z[12]*z[30];
    z[35]=z[29]*z[5];
    z[36]=n<T>(3,2)*z[1];
    z[37]=n<T>(1,2)*z[12];
    z[38]=z[37]*z[3];
    z[39]= - z[10]*z[38];
    z[30]=z[39] - n<T>(3,2)*z[35] + z[36] + z[30];
    z[39]=n<T>(1,2)*z[10];
    z[30]=z[30]*z[39];
    z[40]=n<T>(3,4)*z[21];
    z[41]= - z[19] - z[40];
    z[41]=z[12]*z[41];
    z[41]=n<T>(9,4)*z[1] + z[41];
    z[41]=z[12]*z[41];
    z[30]=z[41] + z[30];
    z[30]=z[10]*z[30];
    z[41]=z[19] + z[38];
    z[41]=z[12]*z[41];
    z[41]= - n<T>(15,2)*z[1] + z[41];
    z[41]=z[12]*z[41];
    z[41]=n<T>(17,2)*z[29] + z[41];
    z[41]=z[41]*z[37];
    z[30]=z[41] + z[30];
    z[30]=z[11]*z[30];
    z[41]=3*z[1];
    z[42]=z[41]*z[5];
    z[43]=5*z[22];
    z[44]= - z[42] + z[43] + z[21];
    z[44]=z[10]*z[44];
    z[45]=2*z[22];
    z[46]= - z[45] - z[38];
    z[46]=z[12]*z[46];
    z[44]=n<T>(1,4)*z[44] + n<T>(9,2)*z[1] + z[46];
    z[44]=z[10]*z[44];
    z[46]=3*z[22];
    z[47]=z[46] + z[21];
    z[47]=z[47]*z[37];
    z[48]=9*z[1];
    z[47]= - z[48] + z[47];
    z[47]=z[12]*z[47];
    z[47]=15*z[29] + z[47];
    z[30]=z[30] + n<T>(1,2)*z[47] + z[44];
    z[30]=z[11]*z[30];
    z[44]=z[29] + z[33];
    z[47]=z[5]*z[2];
    z[44]=z[44]*z[47];
    z[49]=z[12]*z[1];
    z[44]=n<T>(9,2)*z[44] + 11*z[33] + 19*z[29] + 5*z[49];
    z[50]=z[29]*z[12];
    z[51]=5*z[50];
    z[52]=npow(z[1],3);
    z[53]=z[52]*z[11];
    z[54]=z[12]*z[53];
    z[54]=n<T>(5,2)*z[54] + n<T>(11,2)*z[52] + z[51];
    z[54]=z[11]*z[54];
    z[44]=n<T>(1,2)*z[44] + z[54];
    z[54]=n<T>(1,2)*z[6];
    z[44]=z[44]*z[54];
    z[55]=z[38] - z[22];
    z[56]=z[55]*z[12];
    z[57]=n<T>(1,2)*z[1];
    z[58]=z[56] + z[57];
    z[59]=z[12] + z[2];
    z[58]=z[58]*z[59];
    z[59]=z[37]*z[1];
    z[60]=z[59] - z[29];
    z[61]=z[60]*z[12];
    z[62]=n<T>(1,2)*z[52];
    z[63]=z[61] + z[62];
    z[64]=z[11]*z[12];
    z[64]=z[64] + 1;
    z[63]=z[11]*z[63]*z[64];
    z[58]=z[63] + z[58];
    z[58]=z[4]*z[58];
    z[63]=2*z[3];
    z[64]=n<T>(1,4)*z[26];
    z[65]= - z[63] + z[64];
    z[65]=z[12]*z[65];
    z[65]=n<T>(13,4) + z[65];
    z[65]=z[12]*z[65];
    z[66]=n<T>(1,2)*z[2];
    z[67]=z[66]*z[1];
    z[68]= - z[29] + z[67];
    z[32]=z[68]*z[32];
    z[34]=z[33]*z[34];
    z[64]= - z[3] + z[64];
    z[64]=z[12]*z[64];
    z[64]=n<T>(3,4) + z[64];
    z[64]=z[2]*z[64];
    z[15]=n<T>(3,2)*z[58] + z[44] + z[30] + z[15] + z[34] + z[32] + z[64] + 
   n<T>(1,8)*z[1] + z[65];
    z[15]=z[13]*z[15];
    z[30]=z[63] + n<T>(7,4)*z[26];
    z[30]=z[12]*z[30];
    z[32]=z[3] + n<T>(5,2)*z[24];
    z[34]=n<T>(11,2)*z[26] + z[32];
    z[34]=z[34]*z[66];
    z[30]=z[34] - n<T>(1,4)*z[22] + z[30];
    z[30]=z[2]*z[30];
    z[34]=z[26] + 7*z[3];
    z[34]=z[34]*z[12];
    z[44]= - z[22] + z[34];
    z[58]=n<T>(1,4)*z[12];
    z[44]=z[44]*z[58];
    z[30]=z[30] - 2*z[1] + z[44];
    z[30]=z[2]*z[30];
    z[44]=3*z[26];
    z[63]= - z[44] - z[25];
    z[64]=n<T>(1,4)*z[2];
    z[63]=z[63]*z[64];
    z[65]= - z[3] - n<T>(3,4)*z[26];
    z[65]=z[12]*z[65];
    z[63]=z[63] + z[65] - n<T>(1,4)*z[19];
    z[63]=z[2]*z[63];
    z[65]=5*z[3];
    z[68]= - z[65] - z[26];
    z[68]=z[12]*z[68];
    z[68]= - z[19] + z[68];
    z[68]=z[12]*z[68];
    z[69]=7*z[1];
    z[68]= - z[69] + z[68];
    z[63]=n<T>(1,4)*z[68] + z[63];
    z[63]=z[2]*z[63];
    z[68]=n<T>(1,2)*z[19];
    z[70]= - z[68] - z[21];
    z[70]=z[12]*z[70];
    z[71]=n<T>(5,2)*z[1];
    z[70]= - z[71] + z[70];
    z[70]=z[12]*z[70];
    z[72]=5*z[29];
    z[70]= - z[72] + z[70];
    z[63]=n<T>(1,2)*z[70] + z[63];
    z[63]=z[2]*z[63];
    z[70]=npow(z[12],2);
    z[73]=z[70]*z[19];
    z[74]=2*z[29];
    z[75]= - z[74] - n<T>(1,4)*z[73];
    z[75]=z[12]*z[75];
    z[63]=z[63] - z[52] + z[75];
    z[63]=z[7]*z[63];
    z[75]=z[70]*z[3];
    z[76]= - z[1] + z[75];
    z[76]=z[76]*z[58];
    z[30]=z[63] + z[30] - 4*z[29] + z[76];
    z[30]=z[7]*z[30];
    z[63]=z[31] - z[26];
    z[76]=z[63]*z[12];
    z[77]= - n<T>(5,2)*z[22] - z[76];
    z[78]=25*z[3];
    z[79]= - z[78] + 7*z[26];
    z[80]=7*z[24];
    z[81]=z[80] - z[79];
    z[82]= - z[81]*z[66];
    z[77]=7*z[77] + z[82];
    z[77]=z[2]*z[77];
    z[34]= - n<T>(17,2)*z[22] + z[34];
    z[34]=z[12]*z[34];
    z[34]=z[77] - n<T>(45,2)*z[1] + z[34];
    z[34]=z[34]*z[66];
    z[77]=n<T>(7,2)*z[1];
    z[82]= - z[77] + z[75];
    z[82]=z[82]*z[37];
    z[83]=9*z[29];
    z[34]=z[34] - z[83] + z[82];
    z[82]=z[65] - z[26];
    z[84]=z[82] + z[24];
    z[84]=z[84]*z[2];
    z[82]=z[82]*z[12];
    z[82]= - z[82] + 11*z[19];
    z[84]=z[84] + z[82];
    z[85]=z[84]*z[66];
    z[76]= - n<T>(5,2)*z[19] - z[76];
    z[76]=z[12]*z[76];
    z[76]=z[85] + n<T>(13,2)*z[1] + z[76];
    z[76]=z[76]*z[66];
    z[85]= - n<T>(5,4)*z[19] + z[21];
    z[85]=z[12]*z[85];
    z[85]=n<T>(1,4)*z[1] + z[85];
    z[85]=z[12]*z[85];
    z[76]=z[76] + z[74] + z[85];
    z[76]=z[2]*z[76];
    z[85]=z[19]*z[12];
    z[86]= - z[36] + z[85];
    z[86]=z[12]*z[86];
    z[86]=z[29] + z[86];
    z[86]=z[12]*z[86];
    z[86]=z[52] + z[86];
    z[76]=n<T>(1,2)*z[86] + z[76];
    z[76]=z[5]*z[76];
    z[34]=n<T>(1,2)*z[34] + z[76];
    z[34]=z[5]*z[34];
    z[76]= - z[52] - z[50];
    z[86]=n<T>(3,2)*z[52];
    z[87]= - z[86] - z[50];
    z[87]=z[7]*z[12]*z[87];
    z[76]=3*z[76] + z[87];
    z[76]=z[7]*z[76];
    z[87]=n<T>(1,2)*z[29];
    z[76]=z[87] + z[76];
    z[88]=3*z[12];
    z[89]=z[88]*z[52];
    z[90]=z[70]*z[7];
    z[91]= - z[52]*z[90];
    z[91]= - z[89] + z[91];
    z[92]=n<T>(1,4)*z[7];
    z[91]=z[11]*z[91]*z[92];
    z[93]=z[37]*z[29];
    z[94]= - z[52] - z[93];
    z[94]=z[5]*z[94];
    z[76]=z[91] + n<T>(1,2)*z[76] + z[94];
    z[76]=z[11]*z[76];
    z[91]= - z[29]*z[88];
    z[94]=z[85]*z[2];
    z[95]= - 3*z[49] - z[94];
    z[95]=z[2]*z[95];
    z[91]=z[91] + z[95];
    z[95]=n<T>(1,2)*z[5];
    z[91]=z[91]*z[95];
    z[96]=z[22]*z[66];
    z[96]=z[96] + z[1];
    z[97]= - z[2]*z[96];
    z[53]= - z[5]*z[37]*z[53];
    z[53]=z[53] + z[91] - z[87] + z[97];
    z[53]=z[53]*z[54];
    z[91]=z[3]*z[66];
    z[91]=static_cast<T>(1)+ z[91];
    z[91]=z[2]*z[91];
    z[91]=z[36] + z[91];
    z[30]=z[53] + z[76] + z[34] + n<T>(1,4)*z[91] + z[30];
    z[30]=z[6]*z[30];
    z[34]=z[79]*z[58];
    z[34]=z[34] + z[43];
    z[53]=z[81]*z[64];
    z[53]=z[53] + z[34];
    z[53]=z[2]*z[53];
    z[34]= - z[12]*z[34];
    z[34]=z[53] - z[1] + z[34];
    z[34]=z[34]*z[66];
    z[53]= - n<T>(5,4)*z[29] + z[49];
    z[53]=z[12]*z[53];
    z[76]=z[52] - z[93];
    z[76]=z[12]*z[76];
    z[79]=npow(z[1],4);
    z[81]=n<T>(1,2)*z[79];
    z[76]= - z[81] + z[76];
    z[76]=z[76]*z[27];
    z[53]=z[76] + n<T>(1,4)*z[52] + z[53];
    z[76]=n<T>(3,2)*z[7];
    z[53]=z[53]*z[76];
    z[84]= - z[2]*z[84];
    z[82]=z[82]*z[12];
    z[91]=13*z[1];
    z[82]=z[82] - z[91];
    z[84]=z[84] + z[82];
    z[84]=z[84]*z[64];
    z[82]= - z[82]*z[58];
    z[82]=z[84] - z[74] + z[82];
    z[82]=z[82]*z[47];
    z[84]=z[20] - z[26];
    z[93]=z[12]*z[84];
    z[93]= - z[22] - n<T>(3,4)*z[93];
    z[93]=z[12]*z[93];
    z[97]=5*z[1];
    z[93]=z[97] + z[93];
    z[93]=z[93]*z[37];
    z[34]=z[82] + z[53] + z[34] - z[74] + z[93];
    z[34]=z[5]*z[34];
    z[53]=2*z[50];
    z[82]= - z[52] + z[53];
    z[82]=z[12]*z[82];
    z[93]= - z[90]*z[86];
    z[82]=z[82] + z[93];
    z[82]=z[7]*z[82];
    z[93]=z[29] - z[49];
    z[93]=z[12]*z[93];
    z[93]= - z[86] + z[93];
    z[82]=n<T>(1,2)*z[93] + z[82];
    z[82]=z[7]*z[82];
    z[93]= - z[62] + z[50];
    z[93]=z[93]*z[70];
    z[98]= - z[52]*npow(z[12],3)*z[27];
    z[93]=z[93] + z[98];
    z[93]=z[7]*z[93];
    z[98]=n<T>(3,4)*z[52];
    z[61]= - z[98] - z[61];
    z[61]=z[12]*z[61];
    z[61]=z[61] + z[93];
    z[61]=z[7]*z[61];
    z[93]=z[70]*z[1];
    z[61]=n<T>(1,4)*z[93] + z[61];
    z[61]=z[11]*z[61];
    z[55]= - z[55]*z[88];
    z[55]=z[57] + z[55];
    z[55]=z[55]*z[58];
    z[55]=z[61] + z[55] + z[82];
    z[55]=z[11]*z[55];
    z[61]=n<T>(17,8)*z[3];
    z[82]=z[61] - z[26];
    z[82]=z[12]*z[82];
    z[61]=z[61] + z[24];
    z[99]=z[26] - z[61];
    z[99]=z[2]*z[99];
    z[82]=z[99] - n<T>(3,4) + z[82];
    z[82]=z[2]*z[82];
    z[99]= - z[52] + z[51];
    z[100]= - z[2]*z[52];
    z[89]= - z[89] + z[100];
    z[89]=z[7]*z[89];
    z[89]=n<T>(1,4)*z[99] + z[89];
    z[89]=z[7]*z[89];
    z[99]=z[67] + z[29];
    z[59]=z[89] - z[59] - z[99];
    z[59]=z[59]*z[27];
    z[78]=z[78] + z[80];
    z[78]=z[78]*z[64];
    z[78]=5*z[19] + z[78];
    z[78]=z[2]*z[78];
    z[78]= - z[1] + z[78];
    z[78]=z[2]*z[78];
    z[80]=3*z[29];
    z[78]= - z[80] + z[78];
    z[78]=z[2]*z[78];
    z[78]= - z[86] + z[78];
    z[89]= - z[65] - z[24];
    z[89]=z[2]*z[89];
    z[18]=npow(z[18],4);
    z[18]= - 11*z[18] + z[89];
    z[18]=z[2]*z[18];
    z[18]= - z[91] + z[18];
    z[18]=z[18]*z[64];
    z[18]= - z[74] + z[18];
    z[18]=z[2]*z[18];
    z[18]= - z[62] + z[18];
    z[18]=z[18]*z[47];
    z[18]=n<T>(1,2)*z[78] + z[18];
    z[18]=z[5]*z[18];
    z[47]= - z[2]*z[61];
    z[61]=n<T>(3,4)*z[22];
    z[47]= - z[61] + z[47];
    z[47]=z[2]*z[47];
    z[47]=n<T>(3,8)*z[1] + z[47];
    z[47]=z[2]*z[47];
    z[18]=z[47] + z[18];
    z[18]=z[6]*z[18];
    z[47]= - n<T>(11,8)*z[3] + z[26];
    z[47]=z[12]*z[47];
    z[47]= - n<T>(1,4) + z[47];
    z[47]=z[12]*z[47];
    z[18]=z[18] + z[55] + z[34] + z[59] + z[82] + n<T>(7,8)*z[1] + z[47];
    z[18]=z[4]*z[18];
    z[34]=z[88]*z[22];
    z[47]=z[1] + z[85];
    z[47]=z[12]*z[47];
    z[55]= - z[1]*z[90];
    z[47]=z[47] + z[55];
    z[47]=z[47]*z[27];
    z[47]=z[47] - z[71] - z[34];
    z[47]=z[7]*z[47];
    z[55]= - z[21] + n<T>(3,2)*z[22];
    z[59]= - z[12]*z[55];
    z[59]=z[36] + z[59];
    z[59]=z[5]*z[59];
    z[47]=z[59] + n<T>(13,4)*z[21] + z[47];
    z[59]= - z[7]*z[19]*z[58];
    z[59]=z[22] + z[59];
    z[59]=z[7]*z[59];
    z[78]=n<T>(1,4)*z[5];
    z[82]=z[55]*z[78];
    z[59]=z[82] - z[3] + z[59];
    z[59]=z[10]*z[59];
    z[47]=n<T>(1,2)*z[47] + z[59];
    z[47]=z[10]*z[47];
    z[59]= - z[49]*z[76];
    z[82]=z[19]*z[37];
    z[82]=z[82] - z[1];
    z[82]=z[82]*z[12];
    z[89]= - z[87] - z[82];
    z[89]=z[89]*z[95];
    z[91]= - z[19]*z[39];
    z[85]=z[85] - z[1];
    z[91]=z[91] + z[85];
    z[91]=z[5]*z[91];
    z[43]=z[43] + z[91];
    z[43]=z[43]*z[39];
    z[91]=z[22]*z[12];
    z[43]=z[43] + z[89] + z[59] + z[1] - 5*z[91];
    z[43]=z[43]*z[39];
    z[59]=z[90]*z[29];
    z[89]= - z[74] + n<T>(5,4)*z[49];
    z[89]=z[12]*z[89];
    z[89]=z[89] - n<T>(5,4)*z[59];
    z[89]=z[7]*z[89];
    z[90]= - z[41] + n<T>(5,2)*z[91];
    z[90]=z[12]*z[90];
    z[90]=z[29] + z[90];
    z[43]=z[43] + n<T>(1,2)*z[90] + z[89];
    z[43]=z[11]*z[43];
    z[89]= - 11*z[29] - z[49];
    z[89]=z[89]*z[58];
    z[89]=z[89] - z[59];
    z[89]=z[7]*z[89];
    z[90]=z[71] + z[91];
    z[90]=z[12]*z[90];
    z[90]= - z[83] + z[90];
    z[89]=n<T>(1,2)*z[90] + z[89];
    z[89]=z[7]*z[89];
    z[90]= - static_cast<T>(3)- z[21];
    z[58]=z[90]*z[58];
    z[55]=z[55]*z[70];
    z[90]=n<T>(5,2)*z[29];
    z[55]= - z[90] + z[55];
    z[55]=z[55]*z[78];
    z[43]=z[43] + z[47] + z[55] + z[89] + z[1] + z[58];
    z[43]=z[11]*z[43];
    z[47]= - z[73] + z[94];
    z[47]=z[2]*z[47];
    z[47]= - z[59] + z[93] + z[47];
    z[47]=z[47]*z[95];
    z[55]=z[29]*z[2];
    z[58]=z[55] + z[52];
    z[73]=z[66]*z[29];
    z[73]=z[73] + z[52];
    z[73]=z[73]*z[2];
    z[73]=z[73] + z[81];
    z[78]= - z[7]*z[73];
    z[78]=n<T>(1,2)*z[58] + z[78];
    z[78]=z[6]*z[78];
    z[81]=z[91] - z[1];
    z[89]=z[12]*z[81];
    z[89]=z[29] + z[89];
    z[94]= - n<T>(3,2)*z[91] + z[96];
    z[94]=z[2]*z[94];
    z[50]=z[58] - z[50];
    z[58]= - z[50]*z[27];
    z[47]=z[78] + z[47] + z[58] + n<T>(1,2)*z[89] + z[94];
    z[47]=z[9]*z[47];
    z[58]=n<T>(1,8)*z[33];
    z[78]=z[74] + z[58];
    z[78]=z[2]*z[78];
    z[73]= - z[73]*z[92];
    z[73]=z[73] + n<T>(15,8)*z[52] + z[78];
    z[73]=z[6]*z[73];
    z[50]=z[7]*z[50];
    z[50]=z[49] + z[50];
    z[50]=z[73] + z[58] + z[29] - n<T>(1,8)*z[50];
    z[50]=z[7]*z[50];
    z[58]=z[2]*z[61];
    z[58]=z[58] - z[71] + z[34];
    z[58]=z[2]*z[58];
    z[71]=z[1] - n<T>(3,4)*z[91];
    z[71]=z[12]*z[71];
    z[71]= - z[87] + z[71];
    z[59]=z[93] - n<T>(1,4)*z[59];
    z[59]=z[7]*z[59];
    z[58]=z[59] + 3*z[71] + z[58];
    z[58]=z[58]*z[95];
    z[59]= - 3*z[60] + z[67];
    z[60]=z[29] + n<T>(1,4)*z[33];
    z[60]=z[2]*z[60];
    z[60]=z[98] + z[60];
    z[60]=z[6]*z[60];
    z[59]=n<T>(1,2)*z[59] + z[60];
    z[59]=z[4]*z[59];
    z[60]=9*z[2] + n<T>(41,2)*z[1] - 15*z[12];
    z[47]=n<T>(3,2)*z[47] + z[59] + z[58] + n<T>(1,4)*z[60] + z[50];
    z[47]=z[9]*z[47];
    z[50]= - z[5]*z[80];
    z[42]= - z[10]*z[42];
    z[42]=z[42] - z[57] + z[50];
    z[42]=z[10]*z[42];
    z[50]= - z[5]*z[86];
    z[42]=z[42] - z[29] + z[50];
    z[35]=z[1] - z[35];
    z[35]=z[10]*z[35];
    z[35]= - z[29] + n<T>(3,4)*z[35];
    z[50]=z[11]*z[10];
    z[35]=z[35]*z[50];
    z[58]=z[62]*z[5];
    z[59]=z[29] - z[58];
    z[60]=z[11]*npow(z[10],2);
    z[59]=z[59]*z[60];
    z[58]=z[10]*z[58];
    z[58]=z[58] + z[59];
    z[58]=z[8]*z[58];
    z[35]=z[58] + n<T>(1,2)*z[42] + z[35];
    z[35]=z[11]*z[35];
    z[42]=npow(z[7],2);
    z[58]= - z[42]*z[97];
    z[59]=5*z[7];
    z[71]=z[29]*z[59];
    z[71]= - z[41] + z[71];
    z[71]=z[71]*z[28];
    z[58]=z[58] + z[71];
    z[58]=z[58]*z[39];
    z[71]= - z[99]*z[59];
    z[36]= - z[36] + z[71];
    z[36]=z[7]*z[36];
    z[71]=z[52]*z[59];
    z[71]= - z[83] + z[71];
    z[71]=z[71]*z[28];
    z[36]=z[58] + z[36] + z[71];
    z[36]=z[10]*z[36];
    z[58]=z[99]*z[2];
    z[62]=z[58] + z[62];
    z[59]=z[62]*z[59];
    z[62]=z[80] + z[67];
    z[71]= - 3*z[62] - z[59];
    z[71]=z[7]*z[71];
    z[73]=z[7]*z[79];
    z[73]= - 3*z[52] + z[73];
    z[28]=z[73]*z[28];
    z[28]=z[36] + n<T>(5,2)*z[28] - z[1] + z[71];
    z[36]= - z[2]*z[62];
    z[36]=z[36] - n<T>(5,2)*z[52];
    z[59]= - z[2]*z[59];
    z[36]=3*z[36] + z[59];
    z[36]=z[36]*z[27];
    z[36]=z[36] - z[99];
    z[36]=z[36]*z[54];
    z[54]=z[10]*z[57];
    z[54]=z[54] + z[99];
    z[58]=n<T>(7,2)*z[52] + 5*z[58];
    z[58]=z[6]*z[58];
    z[54]=5*z[54] + z[58];
    z[54]=z[13]*z[54];
    z[28]=n<T>(3,4)*z[54] + z[36] + n<T>(1,4)*z[28] + z[35];
    z[28]=z[8]*z[28];
    z[35]= - z[88] + z[2];
    z[35]=z[92]*z[19]*z[35];
    z[36]=z[65] - z[24];
    z[36]=z[36]*z[64];
    z[35]=z[35] + z[36] + z[45] - z[40];
    z[35]=z[7]*z[35];
    z[36]=z[37]*z[16];
    z[40]=5*z[24];
    z[45]= - z[36] + z[40];
    z[35]=n<T>(1,2)*z[45] + z[35];
    z[35]=z[7]*z[35];
    z[45]=z[7]*z[85];
    z[45]=z[45] + z[22] - z[21];
    z[45]=z[5]*z[45]*z[27];
    z[19]= - z[42]*z[19]*z[95];
    z[17]= - z[17]*z[76];
    z[42]=n<T>(3,2)*z[24];
    z[17]=z[17] + z[3] - z[42];
    z[17]=z[7]*z[17];
    z[17]=n<T>(3,2)*z[16] + z[17];
    z[17]=z[7]*z[17];
    z[17]=z[17] + z[19];
    z[17]=z[17]*z[39];
    z[17]=z[17] + z[45] - n<T>(3,4)*z[16] + z[35];
    z[17]=z[10]*z[17];
    z[19]= - z[26] - z[25];
    z[19]=z[19]*z[66];
    z[19]=z[19] - n<T>(7,2)*z[22] - z[21];
    z[19]=z[2]*z[19];
    z[35]=z[41] - z[91];
    z[41]= - z[49] - z[67];
    z[41]=z[7]*z[41];
    z[19]=z[41] + n<T>(1,2)*z[35] + z[19];
    z[19]=z[19]*z[27];
    z[35]=z[40] - z[84];
    z[35]=z[35]*z[64];
    z[19]=z[19] + z[35] - n<T>(7,4) + z[21];
    z[19]=z[7]*z[19];
    z[35]= - z[23] + z[21];
    z[35]=z[12]*z[35];
    z[40]= - z[90] - z[82];
    z[40]=z[7]*z[40];
    z[35]=z[40] + z[77] + z[35];
    z[35]=z[35]*z[27];
    z[40]=z[1] + z[56];
    z[41]=n<T>(3,2)*z[5];
    z[40]=z[40]*z[41];
    z[41]= - z[3] + n<T>(9,8)*z[26];
    z[41]=z[12]*z[41];
    z[35]=z[40] + z[35] - static_cast<T>(3)+ z[41];
    z[35]=z[5]*z[35];
    z[17]=z[17] + z[35] + z[19] + n<T>(5,2)*z[26] - z[25];
    z[17]=z[10]*z[17];
    z[19]=z[20] + z[36];
    z[19]=z[12]*z[19];
    z[25]=z[44] + z[32];
    z[25]=z[2]*z[25];
    z[19]=z[25] - n<T>(3,2) + z[19];
    z[19]=z[19]*z[66];
    z[25]= - z[16]*z[66];
    z[25]=z[25] - z[31] - z[26];
    z[25]=z[2]*z[25];
    z[20]= - z[20] - z[26];
    z[20]=z[12]*z[20];
    z[20]= - z[22] + z[20];
    z[20]=n<T>(1,2)*z[20] + z[25];
    z[20]=z[2]*z[20];
    z[20]=z[20] - n<T>(17,4)*z[1] - z[75];
    z[20]=z[2]*z[20];
    z[25]= - z[48] - z[91];
    z[25]=z[12]*z[25];
    z[25]= - n<T>(7,2)*z[29] + z[25];
    z[20]=n<T>(1,2)*z[25] + z[20];
    z[25]= - z[53] - z[55];
    z[25]=z[7]*z[25];
    z[20]=n<T>(1,2)*z[20] + z[25];
    z[20]=z[7]*z[20];
    z[21]=static_cast<T>(1)+ n<T>(1,4)*z[21];
    z[21]=z[12]*z[21];
    z[19]=z[20] + z[19] - n<T>(25,8)*z[1] + z[21];
    z[19]=z[7]*z[19];
    z[20]= - z[69] - z[34];
    z[20]=z[20]*z[37];
    z[21]= - 7*z[52] + z[51];
    z[21]=z[21]*z[27];
    z[20]=z[21] + z[72] + z[20];
    z[20]=z[20]*z[27];
    z[21]=z[46] - z[38];
    z[21]=z[12]*z[21];
    z[21]= - z[97] + z[21];
    z[21]=z[21]*z[37];
    z[25]=z[36] - z[3];
    z[25]=z[25]*z[12];
    z[27]=z[22] + z[25];
    z[27]=z[2]*z[12]*z[27];
    z[21]=n<T>(3,2)*z[27] + z[74] + z[21];
    z[21]=z[5]*z[21];
    z[27]=n<T>(23,2)*z[3] - 5*z[26];
    z[27]=z[12]*z[27];
    z[27]=static_cast<T>(17)+ z[27];
    z[27]=z[12]*z[27];
    z[27]= - 33*z[1] + z[27];
    z[29]= - 4*z[3] + n<T>(21,8)*z[26];
    z[29]=z[12]*z[29];
    z[29]= - n<T>(3,2) + z[29];
    z[29]=z[2]*z[29];
    z[20]=z[21] + z[20] + n<T>(1,4)*z[27] + z[29];
    z[20]=z[5]*z[20];
    z[21]=z[42] + z[3];
    z[27]=z[2]*z[21];
    z[27]= - z[23] + z[27];
    z[27]=z[6]*z[27];
    z[29]= - z[3] + n<T>(3,2)*z[26];
    z[27]=z[27] + z[42] - z[29];
    z[27]=z[2]*z[27];
    z[29]=z[29]*z[12];
    z[31]=z[23] - z[29];
    z[31]=z[12]*z[31];
    z[25]= - z[68] - z[25];
    z[25]=z[11]*z[25]*z[70];
    z[25]=z[31] + z[25];
    z[25]=z[11]*z[25];
    z[25]=z[25] - n<T>(1,2) + z[29] + z[27];
    z[25]=z[4]*z[25];
    z[16]=z[16]*z[10];
    z[27]=n<T>(3,2)*z[16];
    z[21]=z[25] + z[27] - z[44] + z[21];
    z[25]=z[26] - z[3];
    z[29]= - z[25]*z[88];
    z[29]= - z[23] + z[29];
    z[16]= - n<T>(1,4)*z[16] - z[63];
    z[16]=z[10]*z[16];
    z[16]=n<T>(1,2)*z[29] + z[16];
    z[16]=z[10]*z[16];
    z[29]= - n<T>(3,2)*z[3] + z[26];
    z[29]=z[12]*z[29];
    z[23]=z[23] + z[29];
    z[23]=z[12]*z[23];
    z[16]=z[16] - z[1] + z[23];
    z[16]=z[11]*z[16];
    z[23]=z[27] + z[3] - n<T>(9,2)*z[26];
    z[23]=z[23]*z[39];
    z[27]= - z[3] + n<T>(9,4)*z[26];
    z[27]=z[12]*z[27];
    z[16]=z[16] + z[23] - n<T>(1,4) + z[27];
    z[16]=z[11]*z[16];
    z[22]= - z[2]*z[22];
    z[22]=z[22] + z[81];
    z[22]=z[9]*z[22];
    z[22]= - n<T>(7,4) + z[22];
    z[22]=z[9]*z[22];
    z[16]=3*z[22] + z[16] + n<T>(1,2)*z[21];
    z[21]=z[1]*z[50];
    z[21]=z[57] + z[21];
    z[21]=z[11]*z[21];
    z[22]= - z[10] - z[60];
    z[22]=z[11]*z[1]*z[22];
    z[23]= - z[6]*z[33];
    z[22]=z[23] - z[1] + z[22];
    z[22]=z[8]*z[22];
    z[21]=z[21] + n<T>(1,2)*z[22];
    z[21]=z[8]*z[21];
    z[22]=z[14]*npow(z[9],2)*z[61];
    z[16]=z[22] + n<T>(1,2)*z[16] + z[21];
    z[16]=z[14]*z[16];
    z[21]=2*z[25] - z[24];
    z[21]=z[2]*z[21];
    z[22]=n<T>(19,2)*z[3] - 11*z[26];
    z[22]=z[12]*z[22];
    z[22]=n<T>(21,2) + z[22];

    r += z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + n<T>(1,4)*
      z[22] + z[28] + z[30] + z[43] + z[47];
 
    return r;
}

template double qg_2lNLC_r130(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r130(const std::array<dd_real,31>&);
#endif
