#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1345(const std::array<T,31>& k) {
  T z[102];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[12];
    z[7]=k[15];
    z[8]=k[14];
    z[9]=k[2];
    z[10]=k[5];
    z[11]=k[6];
    z[12]=k[3];
    z[13]=k[13];
    z[14]=k[4];
    z[15]=k[7];
    z[16]=n<T>(3,4)*z[7];
    z[17]=n<T>(1,3)*z[15];
    z[18]= - n<T>(1,6)*z[13] + z[16] - z[17];
    z[19]=npow(z[13],2);
    z[18]=z[18]*z[19];
    z[20]=2*z[9];
    z[21]=npow(z[11],3);
    z[22]=z[21]*z[20];
    z[23]=n<T>(5,3)*z[15];
    z[24]=z[23] + n<T>(19,4)*z[6];
    z[24]=z[8]*z[24];
    z[22]=z[22] + z[24];
    z[22]=z[8]*z[22];
    z[24]=z[21]*z[12];
    z[25]=n<T>(1,2)*z[13];
    z[26]= - z[25]*z[24];
    z[27]=npow(z[13],3);
    z[28]=z[27]*z[15];
    z[29]=npow(z[8],3);
    z[30]=z[29]*z[15];
    z[31]=z[28] - 17*z[30];
    z[31]=z[10]*z[31];
    z[18]=n<T>(1,6)*z[31] + z[26] + z[18] + z[22];
    z[18]=z[10]*z[18];
    z[22]=npow(z[9],2);
    z[26]=z[21]*z[22];
    z[31]=n<T>(1,2)*z[15];
    z[32]=n<T>(3,4)*z[6];
    z[33]=n<T>(19,4)*z[8] - z[32] - z[31] - z[26];
    z[33]=z[8]*z[33];
    z[34]=n<T>(3,2)*z[7];
    z[35]=z[13] - z[34] - z[17];
    z[35]=z[35]*z[25];
    z[36]=7*z[13];
    z[37]=z[24]*z[36];
    z[37]= - n<T>(1,2)*z[27] + z[37];
    z[38]=n<T>(1,3)*z[12];
    z[37]=z[37]*z[38];
    z[39]=z[2] - z[7];
    z[40]=z[39]*z[5];
    z[41]=z[6]*z[7];
    z[18]=z[18] + z[37] + z[33] + z[35] - n<T>(29,4)*z[41] - 12*z[40];
    z[18]=z[10]*z[18];
    z[33]=z[13] - z[15];
    z[35]=n<T>(2,3)*z[33];
    z[37]=z[19]*z[35];
    z[42]=npow(z[8],2);
    z[43]=n<T>(2,3)*z[15] + n<T>(7,2)*z[6];
    z[43]=z[43]*z[42];
    z[44]= - z[28] - z[30];
    z[45]=n<T>(2,3)*z[10];
    z[44]=z[44]*z[45];
    z[37]=z[44] + z[37] + z[43];
    z[37]=z[10]*z[37];
    z[43]=z[25] + z[34] - z[17];
    z[43]=z[13]*z[43];
    z[44]=z[27]*z[12];
    z[46]=n<T>(7,2)*z[8] - z[17] - n<T>(3,2)*z[6];
    z[46]=z[8]*z[46];
    z[37]=z[37] + n<T>(2,3)*z[44] + z[46] - n<T>(3,2)*z[41] + z[43];
    z[37]=z[10]*z[37];
    z[43]= - z[21] + 2*z[27];
    z[43]=z[12]*z[43];
    z[46]=2*z[19];
    z[43]= - z[46] + z[43];
    z[38]=z[43]*z[38];
    z[43]=5*z[6];
    z[47]=n<T>(2,3)*z[13];
    z[48]=z[8]*z[9];
    z[49]=5*z[48];
    z[50]=n<T>(1,2) - z[49];
    z[50]=z[8]*z[50];
    z[37]=z[37] + z[38] + z[50] - z[47] + z[26] - z[43];
    z[37]=z[10]*z[37];
    z[38]=n<T>(1,2)*z[4];
    z[50]=z[38] + z[47];
    z[50]=z[50]*z[19];
    z[50]=z[21] + z[50];
    z[50]=z[12]*z[50];
    z[51]=z[21]*z[9];
    z[52]=z[6] + z[4];
    z[53]=z[52]*z[5];
    z[50]=z[50] - n<T>(4,3)*z[19] - n<T>(1,3)*z[51] - 14*z[53];
    z[50]=z[12]*z[50];
    z[54]=2*z[6];
    z[50]=z[50] + z[13] + z[54] + n<T>(1,3)*z[26] - n<T>(57,2)*z[4];
    z[50]=z[12]*z[50];
    z[55]=4*z[4];
    z[56]= - z[55] - z[43];
    z[57]=z[19]*z[12];
    z[58]=z[57]*z[4];
    z[59]=3*z[13];
    z[60]= - z[4]*z[59];
    z[60]=z[60] + z[58];
    z[60]=z[12]*z[60];
    z[56]=2*z[56] + z[60];
    z[56]=z[12]*z[56];
    z[60]=z[4]*z[9];
    z[61]=static_cast<T>(1)- 5*z[60];
    z[56]=3*z[61] + z[56];
    z[56]=z[12]*z[56];
    z[61]=z[42]*z[10];
    z[62]=z[61]*z[6];
    z[63]= - z[6] + z[8];
    z[63]=z[8]*z[63];
    z[63]=z[63] + z[62];
    z[63]=z[10]*z[63];
    z[64]=z[57] - z[13];
    z[65]=2*z[48];
    z[66]= - static_cast<T>(1)- z[65];
    z[66]=z[8]*z[66];
    z[63]=z[63] + z[66] - z[6] - z[64];
    z[63]=z[10]*z[63];
    z[66]=z[22]*z[8];
    z[67]=z[66] + z[9];
    z[68]=z[8]*z[67];
    z[69]=z[54] - z[13];
    z[69]=z[12]*z[69];
    z[63]=z[63] + z[68] + z[69];
    z[63]=z[10]*z[63];
    z[68]=z[22]*z[4];
    z[69]=3*z[68];
    z[56]=z[63] + z[56] + z[9] - z[69];
    z[56]=z[3]*z[56];
    z[63]=z[9]*z[2];
    z[70]=n<T>(3,2)*z[63];
    z[71]= - static_cast<T>(16)- z[70];
    z[71]=z[71]*z[60];
    z[72]=n<T>(1,2)*z[66];
    z[73]=n<T>(1,2)*z[63];
    z[74]=z[73] - 1;
    z[75]=z[9]*z[74];
    z[75]=z[75] + z[72];
    z[76]=3*z[8];
    z[75]=z[75]*z[76];
    z[37]=z[56] + z[37] + z[50] + z[75] + n<T>(68,3) + z[71];
    z[37]=z[3]*z[37];
    z[50]=npow(z[5],2);
    z[52]=z[52]*z[50];
    z[56]=n<T>(1,3)*z[13];
    z[71]=z[38] - z[56];
    z[75]=n<T>(1,2)*z[19];
    z[71]=z[71]*z[75];
    z[24]=z[13]*z[24];
    z[24]= - n<T>(8,3)*z[24] - 2*z[52] + z[71];
    z[24]=z[12]*z[24];
    z[71]=2*z[5];
    z[77]= - 20*z[4] + z[6];
    z[77]=z[77]*z[71];
    z[78]=n<T>(1,4)*z[4] + z[47];
    z[78]=z[13]*z[78];
    z[24]=z[24] + z[77] + z[78];
    z[24]=z[12]*z[24];
    z[77]=3*z[48];
    z[73]= - static_cast<T>(3)+ z[73];
    z[73]=z[73]*z[77];
    z[78]=static_cast<T>(19)- 23*z[63];
    z[73]=n<T>(1,2)*z[78] + z[73];
    z[78]=n<T>(1,2)*z[8];
    z[73]=z[73]*z[78];
    z[79]= - static_cast<T>(23)- n<T>(17,4)*z[63];
    z[79]=z[4]*z[79];
    z[18]=z[37] + z[18] + z[24] + z[73] - z[56] + 61*z[5] - n<T>(17,2)*z[6]
    + 21*z[2] + z[79];
    z[18]=z[3]*z[18];
    z[24]=2*z[15];
    z[37]=3*z[6];
    z[73]=z[24] + z[37];
    z[26]=n<T>(7,6)*z[26] + z[73];
    z[26]=z[26]*z[42];
    z[79]=z[8]*z[15];
    z[51]= - z[51] - n<T>(31,3)*z[79];
    z[79]=n<T>(1,2)*z[42];
    z[51]=z[51]*z[79];
    z[80]=n<T>(1,2)*z[57];
    z[81]=z[21]*z[80];
    z[28]=z[81] - n<T>(2,3)*z[28] + z[51];
    z[28]=z[10]*z[28];
    z[51]=npow(z[12],2);
    z[81]=z[51]*z[21];
    z[35]= - n<T>(17,6)*z[81] + z[35];
    z[35]=z[19]*z[35];
    z[82]=z[39]*z[50];
    z[26]=z[28] + z[26] - 11*z[82] + z[35];
    z[26]=z[10]*z[26];
    z[28]=31*z[4];
    z[35]=6*z[6];
    z[83]=21*z[7] + 59*z[2];
    z[83]=24*z[5] - z[35] + n<T>(1,2)*z[83] - z[28];
    z[83]=z[5]*z[83];
    z[84]=5*z[4];
    z[85]=z[84] - z[6];
    z[85]=z[85]*z[50];
    z[81]=z[19]*z[81];
    z[81]=n<T>(7,3)*z[81] - z[85] + n<T>(2,3)*z[27];
    z[81]=z[12]*z[81];
    z[74]= - z[74]*z[76];
    z[31]=z[74] - n<T>(1,4)*z[6] - z[31] + 5*z[2];
    z[31]=z[8]*z[31];
    z[74]=z[4]*z[2];
    z[86]= - z[13]*z[17];
    z[18]=z[18] + z[26] + z[81] + z[31] + z[86] + z[83] - 5*z[74] - n<T>(43,4)*z[41];
    z[18]=z[3]*z[18];
    z[26]= - z[39]*z[71];
    z[31]=z[10]*z[19];
    z[31]=z[31] + z[13];
    z[31]=z[7]*z[31];
    z[26]=z[42] - 3*z[41] + z[26] + z[31];
    z[26]=z[10]*z[26];
    z[31]= - static_cast<T>(2)+ z[63];
    z[31]=z[31]*z[48];
    z[81]=3*z[63];
    z[31]=z[31] + static_cast<T>(1)- z[81];
    z[31]=z[8]*z[31];
    z[83]=2*z[12];
    z[86]= - z[83]*z[52];
    z[87]= - 17*z[4] + z[37];
    z[87]=z[5]*z[87];
    z[86]=z[87] + z[86];
    z[86]=z[86]*z[83];
    z[81]= - static_cast<T>(19)- z[81];
    z[81]=z[4]*z[81];
    z[26]=z[26] + z[86] + z[31] + 26*z[5] - z[43] + 6*z[2] + z[81];
    z[26]=z[3]*z[26];
    z[31]=npow(z[12],4);
    z[43]=npow(z[11],5);
    z[81]=z[31]*z[43];
    z[86]=z[81]*z[19];
    z[87]= - z[82] - n<T>(1,3)*z[86];
    z[88]=n<T>(1,3)*z[19];
    z[89]=npow(z[12],3);
    z[88]=z[89]*z[88];
    z[90]=z[22]*z[61];
    z[88]=z[88] + z[90];
    z[88]=z[10]*z[43]*z[88];
    z[87]=4*z[87] + z[88];
    z[88]=2*z[10];
    z[87]=z[87]*z[88];
    z[86]= - 2*z[85] + z[86];
    z[86]=z[86]*z[83];
    z[90]=2*z[2];
    z[91]=z[90] + z[7];
    z[92]=36*z[5] - 17*z[6] + 16*z[91] - 71*z[4];
    z[92]=z[5]*z[92];
    z[93]=n<T>(7,2) - 5*z[63];
    z[93]=z[8]*z[93];
    z[93]=n<T>(15,2)*z[2] + z[93];
    z[93]=z[8]*z[93];
    z[26]=z[26] + z[87] + z[86] + z[93] + z[92] - 13*z[74] - 16*z[41];
    z[26]=z[3]*z[26];
    z[86]=z[74] + z[41];
    z[87]=7*z[6];
    z[92]= - z[87] + 13*z[91] - 28*z[4];
    z[92]=z[5]*z[92];
    z[92]= - n<T>(65,2)*z[86] + z[92];
    z[92]=z[5]*z[92];
    z[93]=npow(z[12],5);
    z[94]=z[43]*z[27]*z[93];
    z[95]=11*z[27];
    z[96]= - z[89]*z[95];
    z[29]=z[29]*z[22];
    z[97]=z[10]*z[29];
    z[96]=z[96] - 5*z[97];
    z[97]=n<T>(1,2)*z[10];
    z[43]=z[97]*z[43]*z[96];
    z[81]=z[81]*z[95];
    z[43]=z[81] + z[43];
    z[81]=n<T>(1,3)*z[10];
    z[43]=z[43]*z[81];
    z[95]=z[42]*z[2];
    z[26]=z[26] + z[43] - n<T>(11,6)*z[94] + z[92] + n<T>(19,4)*z[95];
    z[26]=z[3]*z[26];
    z[43]=z[86]*z[50];
    z[26]= - 11*z[43] + z[26];
    z[26]=z[3]*z[26];
    z[50]=9*z[2];
    z[92]= - 12*z[5] + z[87] + 27*z[4] - 5*z[7] - z[50];
    z[92]=z[5]*z[92];
    z[94]= - static_cast<T>(1)+ 2*z[63];
    z[96]=z[8]*z[94];
    z[96]= - z[90] + z[96];
    z[96]=z[8]*z[96];
    z[98]=z[83]*z[85];
    z[88]=z[82]*z[88];
    z[88]=z[88] + z[98] + z[96] + 5*z[86] + z[92];
    z[88]=z[3]*z[88];
    z[92]=3*z[4];
    z[96]=z[92] - z[91];
    z[96]=4*z[96] + z[37];
    z[96]=z[5]*z[96];
    z[96]=12*z[86] + z[96];
    z[96]=z[96]*z[71];
    z[98]=npow(z[11],6);
    z[99]=z[98]*z[27];
    z[100]= - z[31]*z[99];
    z[98]= - z[98]*npow(z[10],2)*z[29];
    z[98]=z[100] + z[98];
    z[98]=z[10]*z[98];
    z[93]=z[93]*z[99];
    z[93]=2*z[93] + z[98];
    z[45]=z[93]*z[45];
    z[93]=npow(z[12],6)*z[99];
    z[45]=z[88] + z[45] - n<T>(2,3)*z[93] + z[96] - n<T>(7,2)*z[95];
    z[45]=z[3]*z[45];
    z[45]=13*z[43] + z[45];
    z[45]=z[45]*npow(z[3],2);
    z[88]= - z[6] - z[55] + z[91];
    z[88]=z[88]*z[71];
    z[88]= - 7*z[86] + z[88];
    z[88]=z[5]*z[88];
    z[88]=z[88] + z[95];
    z[88]=z[3]*z[88];
    z[88]= - 8*z[43] + z[88];
    z[88]=z[88]*npow(z[3],3);
    z[93]=z[1]*npow(z[3],4)*z[43];
    z[88]=z[88] + 2*z[93];
    z[88]=z[1]*z[88];
    z[45]=z[45] + z[88];
    z[45]=z[1]*z[45];
    z[26]=z[26] + z[45];
    z[26]=z[1]*z[26];
    z[45]= - z[7] - z[13];
    z[45]=z[13]*z[45];
    z[45]= - z[42] + z[41] + z[45];
    z[45]=z[10]*z[45];
    z[88]=z[42]*z[20];
    z[45]=z[45] + z[88] + z[37] - z[13];
    z[45]=z[10]*z[45];
    z[88]=static_cast<T>(11)+ z[63];
    z[88]=z[88]*z[60];
    z[93]=static_cast<T>(1)- z[63];
    z[93]=z[9]*z[93];
    z[93]=z[93] - z[66];
    z[93]=z[8]*z[93];
    z[96]=6*z[4];
    z[98]=z[96] - z[6];
    z[99]=z[12]*z[53];
    z[98]=2*z[98] + 7*z[99];
    z[98]=z[98]*z[83];
    z[45]=z[45] + z[98] + z[93] - static_cast<T>(11)+ z[88];
    z[45]=z[3]*z[45];
    z[88]= - z[19]*z[34];
    z[93]=npow(z[11],4);
    z[98]=z[93]*z[66];
    z[99]=z[51]*z[93];
    z[100]= - z[56]*z[99];
    z[88]=z[100] + z[88] - n<T>(7,3)*z[98];
    z[88]=z[10]*z[88];
    z[98]=z[41] + z[40];
    z[89]=z[89]*z[93];
    z[100]=2*z[13];
    z[101]=z[100]*z[89];
    z[88]=z[88] + z[101] + 8*z[98] - n<T>(7,2)*z[42];
    z[88]=z[10]*z[88];
    z[98]=z[13]*z[99];
    z[52]=4*z[52] - n<T>(7,3)*z[98];
    z[52]=z[12]*z[52];
    z[28]=z[28] - z[37];
    z[28]=z[28]*z[71];
    z[28]=z[28] + z[52];
    z[28]=z[12]*z[28];
    z[52]=static_cast<T>(6)+ z[63];
    z[52]=z[52]*z[96];
    z[70]=static_cast<T>(5)- z[70];
    z[70]=z[70]*z[48];
    z[70]=4*z[94] + z[70];
    z[70]=z[8]*z[70];
    z[71]=19*z[2];
    z[28]=z[45] + z[88] + z[28] + z[70] - 66*z[5] + 11*z[6] - z[71] + 
    z[52];
    z[28]=z[3]*z[28];
    z[45]=z[93]*z[22]*z[42];
    z[52]=z[19]*z[99];
    z[45]=5*z[45] + n<T>(7,2)*z[52];
    z[52]= - z[9]*z[93]*z[61];
    z[45]=n<T>(1,3)*z[45] + z[52];
    z[45]=z[10]*z[45];
    z[52]=z[89]*z[19];
    z[45]=z[45] + 13*z[82] - 5*z[52];
    z[45]=z[10]*z[45];
    z[52]=3*z[85] + n<T>(23,6)*z[52];
    z[52]=z[12]*z[52];
    z[70]=25*z[74] + 39*z[41];
    z[74]= - n<T>(19,2) + 9*z[63];
    z[74]=z[8]*z[74];
    z[71]= - z[71] + z[74];
    z[71]=z[71]*z[78];
    z[74]=31*z[6] + 141*z[4] - 39*z[7] - 89*z[2];
    z[74]=n<T>(1,2)*z[74] - 42*z[5];
    z[74]=z[5]*z[74];
    z[28]=z[28] + z[45] + z[52] + z[71] + n<T>(1,2)*z[70] + z[74];
    z[28]=z[3]*z[28];
    z[31]=z[93]*z[31]*z[27];
    z[29]=z[93]*z[29];
    z[45]=z[27]*z[99];
    z[29]= - 2*z[29] - n<T>(13,2)*z[45];
    z[29]=z[10]*z[29];
    z[45]=z[27]*z[89];
    z[29]=13*z[45] + z[29];
    z[29]=z[29]*z[81];
    z[45]=4*z[6] - 11*z[91] + 16*z[4];
    z[45]=z[5]*z[45];
    z[45]=n<T>(43,2)*z[86] + z[45];
    z[45]=z[5]*z[45];
    z[28]=z[28] + z[29] - n<T>(13,6)*z[31] + z[45] - 3*z[95];
    z[28]=z[3]*z[28];
    z[26]=z[26] + 5*z[43] + z[28];
    z[26]=z[1]*z[26];
    z[28]=npow(z[14],2);
    z[29]=z[28]*z[24];
    z[31]= - z[4]*z[29];
    z[43]=z[15]*z[14];
    z[31]= - n<T>(17,2)*z[43] + z[31];
    z[45]=n<T>(1,3)*z[8];
    z[31]=z[31]*z[45];
    z[52]=n<T>(3,4)*z[2];
    z[70]=n<T>(2,3)*z[43];
    z[71]=z[4]*z[70];
    z[23]=z[31] + z[32] + z[71] + z[23] + z[52];
    z[23]=z[8]*z[23];
    z[31]=z[17]*z[4];
    z[23]= - z[31] + z[23];
    z[23]=z[8]*z[23];
    z[32]=z[21]*z[14];
    z[71]=z[32]*z[27];
    z[21]=z[44]*z[21];
    z[71]=z[71] + 3*z[21];
    z[74]=z[12]*z[71];
    z[85]=z[6]*z[15];
    z[32]= - n<T>(1,2)*z[32] - n<T>(2,3)*z[85];
    z[27]=z[32]*z[27];
    z[21]=z[27] - n<T>(3,2)*z[21];
    z[21]=z[10]*z[21];
    z[27]=z[13]*z[6];
    z[32]= - z[85] + z[27];
    z[32]=z[32]*z[46];
    z[30]=z[32] - n<T>(31,2)*z[30];
    z[21]=z[21] + n<T>(1,3)*z[30] + z[74];
    z[21]=z[10]*z[21];
    z[30]=z[7]*z[14];
    z[32]=z[30] + 1;
    z[32]=z[32]*z[6];
    z[46]=z[2]*z[14];
    z[74]= - static_cast<T>(4)- z[46];
    z[74]=z[4]*z[74];
    z[74]= - z[32] + 5*z[91] + z[74];
    z[74]=z[5]*z[74];
    z[74]= - n<T>(13,2)*z[86] + z[74];
    z[74]=z[5]*z[74];
    z[71]=z[71]*z[51];
    z[86]=z[6]*z[14];
    z[86]=z[86] + 1;
    z[88]=z[86]*z[13];
    z[89]=z[6] - z[88];
    z[89]=z[89]*z[100];
    z[89]= - z[85] + z[89];
    z[89]=z[89]*z[56];
    z[18]=z[26] + z[18] + z[21] - n<T>(1,2)*z[71] + z[23] + z[74] + z[89];
    z[18]=z[1]*z[18];
    z[21]= - n<T>(13,2)*z[8] + z[15] + n<T>(19,2)*z[6];
    z[21]=z[21]*z[42];
    z[23]= - z[15]*z[19];
    z[21]=z[23] + z[21];
    z[21]=z[10]*z[21];
    z[23]=n<T>(3,4)*z[13];
    z[26]=z[23] - n<T>(1,4)*z[7] - z[15];
    z[26]=z[13]*z[26];
    z[42]=n<T>(61,6)*z[8] - z[15] - n<T>(17,4)*z[6];
    z[42]=z[8]*z[42];
    z[21]=z[21] + z[42] + n<T>(1,4)*z[41] + z[26];
    z[21]=z[10]*z[21];
    z[26]=npow(z[11],2);
    z[42]=z[26]*z[9];
    z[71]=z[42] - z[37];
    z[74]=n<T>(1,2)*z[26];
    z[89]= - z[74] + z[19];
    z[89]=z[12]*z[89];
    z[91]=n<T>(1,4)*z[13];
    z[77]= - n<T>(5,2) + z[77];
    z[77]=z[8]*z[77];
    z[21]=z[21] + z[89] + n<T>(3,2)*z[77] - z[91] - z[71];
    z[21]=z[10]*z[21];
    z[77]=z[54] - z[25];
    z[89]=2*z[8];
    z[93]=z[87] - z[89];
    z[93]=z[93]*z[61];
    z[94]= - z[35] + n<T>(25,3)*z[8];
    z[94]=z[8]*z[94];
    z[93]=z[94] + z[93];
    z[93]=z[10]*z[93];
    z[49]= - n<T>(16,3) + z[49];
    z[49]=z[8]*z[49];
    z[49]=z[93] - z[80] + z[49] + z[77];
    z[49]=z[10]*z[49];
    z[93]=n<T>(3,2)*z[66];
    z[94]= - z[20] - z[93];
    z[94]=z[8]*z[94];
    z[95]=z[6] - z[13];
    z[83]=z[95]*z[83];
    z[49]=z[49] + z[83] + n<T>(3,2) + z[94];
    z[49]=z[10]*z[49];
    z[83]=7*z[4];
    z[94]=z[83] + z[59];
    z[94]=z[13]*z[94];
    z[94]=z[94] - 2*z[58];
    z[94]=z[12]*z[94];
    z[84]=z[94] - z[84] - z[36];
    z[84]=z[12]*z[84];
    z[84]=z[84] + static_cast<T>(7)+ 9*z[60];
    z[84]=z[12]*z[84];
    z[84]=z[84] + z[9] + z[69];
    z[84]=z[12]*z[84];
    z[94]= - z[37] + z[89];
    z[94]=z[8]*z[94];
    z[95]=z[61]*z[54];
    z[94]=z[94] + z[95];
    z[94]=z[10]*z[94];
    z[95]= - static_cast<T>(3)+ z[65];
    z[95]=z[8]*z[95];
    z[94]=z[94] + z[6] + z[95];
    z[94]=z[10]*z[94];
    z[95]=3*z[9];
    z[98]= - z[95] - z[66];
    z[98]=z[8]*z[98];
    z[99]= - z[12]*z[64];
    z[94]=z[94] + z[99] + static_cast<T>(1)+ z[98];
    z[94]=z[10]*z[94];
    z[98]=z[35] - z[13];
    z[98]=z[12]*z[98];
    z[98]=static_cast<T>(1)+ z[98];
    z[98]=z[12]*z[98];
    z[67]=z[94] + z[98] + z[67];
    z[67]=z[10]*z[67];
    z[67]=z[84] + z[67];
    z[67]=z[3]*z[67];
    z[84]= - z[92] - z[100];
    z[84]=z[84]*z[57];
    z[92]=z[96] + n<T>(49,6)*z[13];
    z[92]=z[13]*z[92];
    z[84]=z[92] + z[84];
    z[84]=z[12]*z[84];
    z[84]=z[84] - n<T>(35,6)*z[13] + z[4] + 10*z[6];
    z[84]=z[12]*z[84];
    z[84]=z[84] + n<T>(5,3) + 14*z[60];
    z[84]=z[12]*z[84];
    z[49]=z[67] + z[49] + z[84] - n<T>(7,6)*z[9] + z[69];
    z[49]=z[3]*z[49];
    z[67]=n<T>(1,3)*z[26];
    z[69]=z[22]*z[67];
    z[84]=static_cast<T>(25)+ z[63];
    z[84]=z[84]*z[60];
    z[92]=3*z[66];
    z[63]=static_cast<T>(5)- z[63];
    z[63]=z[9]*z[63];
    z[63]=z[63] - z[92];
    z[63]=z[63]*z[78];
    z[63]=z[63] + n<T>(1,2)*z[84] - n<T>(51,2) + z[69];
    z[69]=z[4] + n<T>(35,3)*z[13];
    z[69]=z[69]*z[91];
    z[84]=n<T>(3,2)*z[57];
    z[94]= - z[4] - z[13];
    z[94]=z[94]*z[84];
    z[96]=n<T>(2,3)*z[26];
    z[53]=z[94] + z[69] + z[96] + 5*z[53];
    z[53]=z[12]*z[53];
    z[69]= - z[42] + n<T>(33,2)*z[4];
    z[53]=z[53] + n<T>(7,4)*z[13] + n<T>(1,2)*z[69] + z[6];
    z[53]=z[12]*z[53];
    z[21]=z[49] + z[21] + n<T>(1,2)*z[63] + z[53];
    z[21]=z[3]*z[21];
    z[49]=z[13]*z[15];
    z[49]= - z[74] + z[49];
    z[49]=z[13]*z[49];
    z[53]=9*z[8];
    z[63]=2*z[73] - z[53];
    z[63]=z[8]*z[63];
    z[63]= - z[26] + z[63];
    z[63]=z[8]*z[63];
    z[69]= - z[6]*z[67];
    z[49]=z[63] + z[69] + z[49];
    z[49]=z[10]*z[49];
    z[16]=z[16] - z[33];
    z[16]=z[13]*z[16];
    z[33]=z[12]*z[26]*z[25];
    z[63]=n<T>(11,2)*z[8] - n<T>(5,4)*z[6] + n<T>(1,2)*z[42] - 3*z[15];
    z[63]=z[8]*z[63];
    z[16]=z[49] + z[33] + z[63] + z[16] + n<T>(5,4)*z[41] + 8*z[40];
    z[16]=z[10]*z[16];
    z[33]=z[26]*z[4];
    z[40]= - z[6]*z[26];
    z[40]= - z[33] + z[40];
    z[41]=z[59] + z[4];
    z[49]= - z[41]*z[25];
    z[49]= - z[96] + z[49];
    z[49]=z[13]*z[49];
    z[40]=n<T>(1,3)*z[40] + z[49];
    z[40]=z[12]*z[40];
    z[49]= - z[38] - z[56];
    z[49]=z[49]*z[25];
    z[63]=9*z[4];
    z[69]=z[63] + z[6];
    z[69]=z[5]*z[69];
    z[40]=z[40] + z[69] + z[49];
    z[40]=z[12]*z[40];
    z[42]=n<T>(1,3)*z[42];
    z[49]=z[52] - z[42];
    z[49]=z[9]*z[49];
    z[49]=n<T>(15,4) + z[49];
    z[49]=z[4]*z[49];
    z[52]=n<T>(1,2)*z[2];
    z[42]=z[52] - z[42];
    z[42]=z[9]*z[42];
    z[42]=n<T>(3,2)*z[48] - static_cast<T>(3)+ n<T>(5,2)*z[42];
    z[42]=z[8]*z[42];
    z[16]=z[21] + z[16] + z[40] + z[42] + n<T>(1,12)*z[13] - 22*z[5] + n<T>(2,3)
   *z[6] + z[49] - z[50] + z[7] + z[15];
    z[16]=z[3]*z[16];
    z[21]=2*z[85] - n<T>(3,2)*z[27];
    z[21]=z[13]*z[21];
    z[27]=z[8] - z[15];
    z[27]= - z[71] - 13*z[27];
    z[27]=z[27]*z[79];
    z[40]= - z[26]*z[80];
    z[42]=z[67] + n<T>(3,2)*z[19];
    z[42]=z[10]*z[85]*z[42];
    z[21]=z[42] + z[40] + z[27] + 5*z[82] + z[21];
    z[21]=z[10]*z[21];
    z[27]=static_cast<T>(9)+ z[46];
    z[27]=z[4]*z[27];
    z[27]=z[32] + z[27] - 3*z[7] - 17*z[2];
    z[32]= - z[14]*z[90];
    z[32]=z[32] - static_cast<T>(6)- z[30];
    z[32]=z[5]*z[32];
    z[27]=n<T>(1,2)*z[27] + z[32];
    z[27]=z[5]*z[27];
    z[32]=n<T>(5,3)*z[14];
    z[28]=z[28]*z[15];
    z[40]= - z[32] + 3*z[28];
    z[40]=z[40]*z[38];
    z[42]=n<T>(1,6) + z[43];
    z[49]= - z[14]*z[89];
    z[40]=z[49] + 5*z[42] + z[40];
    z[40]=z[8]*z[40];
    z[42]=n<T>(1,3) - z[43];
    z[42]=z[4]*z[42];
    z[40]=z[40] + 2*z[42] - 4*z[15] - z[2];
    z[40]=z[8]*z[40];
    z[42]=z[26]*z[85];
    z[49]=z[15]*z[33];
    z[42]=z[49] + z[42];
    z[49]=z[19]*z[74];
    z[42]=n<T>(1,3)*z[42] + z[49];
    z[42]=z[12]*z[42];
    z[33]=z[33]*z[70];
    z[33]=z[33] + z[42];
    z[33]=z[12]*z[33];
    z[26]=z[26]*z[29];
    z[26]=z[15] + z[26];
    z[26]=n<T>(1,3)*z[26] + z[52];
    z[26]=z[4]*z[26];
    z[29]= - z[54] + z[88];
    z[29]=z[29]*z[47];
    z[34]=z[34] + z[15];
    z[34]=z[6]*z[34];
    z[16]=z[18] + z[16] + z[21] + z[33] + z[40] + z[29] + z[27] + z[26]
    + z[34];
    z[16]=z[1]*z[16];
    z[18]=z[37]*z[61];
    z[21]= - z[15] - z[91];
    z[21]=z[13]*z[21];
    z[26]=z[48] - n<T>(1,2);
    z[27]=z[8]*z[26];
    z[27]= - n<T>(9,2)*z[27] - z[15] - z[6];
    z[27]=z[8]*z[27];
    z[18]=z[18] + z[21] + z[27];
    z[18]=z[10]*z[18];
    z[21]= - static_cast<T>(5)+ n<T>(23,2)*z[48];
    z[21]=z[21]*z[45];
    z[27]=z[19] - 3*z[44];
    z[27]=z[12]*z[27];
    z[18]=z[18] + z[27] + z[21] - z[77];
    z[18]=z[10]*z[18];
    z[21]= - static_cast<T>(2)+ n<T>(77,4)*z[48];
    z[21]=z[21]*z[45];
    z[26]= - z[26]*z[53];
    z[26]= - z[87] + z[26];
    z[26]=z[8]*z[26];
    z[26]=z[26] + n<T>(19,2)*z[62];
    z[26]=z[26]*z[97];
    z[21]=z[26] - n<T>(3,4)*z[57] + z[21] - z[6] - z[91];
    z[21]=z[10]*z[21];
    z[26]= - z[9] + z[72];
    z[26]=z[26]*z[76];
    z[26]= - n<T>(19,6) + z[26];
    z[27]=n<T>(71,6)*z[19] - 7*z[44];
    z[29]=n<T>(1,2)*z[12];
    z[27]=z[27]*z[29];
    z[27]=z[27] - z[87] - n<T>(3,2)*z[13];
    z[27]=z[12]*z[27];
    z[21]=z[21] + n<T>(1,2)*z[26] + z[27];
    z[21]=z[10]*z[21];
    z[26]= - z[84] - z[37] + z[25];
    z[26]=z[12]*z[26];
    z[27]= - static_cast<T>(5)+ n<T>(37,3)*z[48];
    z[27]=z[27]*z[78];
    z[33]=z[65] - n<T>(5,2);
    z[33]=z[33]*z[8];
    z[34]= - n<T>(9,2)*z[6] - z[33];
    z[34]=z[8]*z[34];
    z[34]=z[34] + n<T>(7,2)*z[62];
    z[34]=z[10]*z[34];
    z[27]=z[34] + z[6] + z[27];
    z[27]=z[10]*z[27];
    z[34]= - n<T>(29,3)*z[9] + z[92];
    z[34]=z[34]*z[78];
    z[26]=z[27] + z[34] + z[26];
    z[26]=z[10]*z[26];
    z[19]=n<T>(53,6)*z[19] - 2*z[44];
    z[19]=z[12]*z[19];
    z[19]=z[19] - z[35] - n<T>(25,3)*z[13];
    z[19]=z[12]*z[19];
    z[19]= - n<T>(5,2) + z[19];
    z[19]=z[12]*z[19];
    z[19]=z[26] + z[19] + n<T>(2,3)*z[9] - z[93];
    z[19]=z[10]*z[19];
    z[20]= - z[20] + z[66];
    z[20]=z[8]*z[20];
    z[26]= - z[54] + z[8];
    z[26]=z[8]*z[26];
    z[26]=z[26] + z[62];
    z[26]=z[10]*z[26];
    z[27]= - static_cast<T>(2)+ z[48];
    z[27]=z[8]*z[27];
    z[26]=z[26] + z[6] + z[27];
    z[26]=z[10]*z[26];
    z[20]=z[26] + static_cast<T>(1)+ z[20];
    z[20]=z[10]*z[20];
    z[26]= - z[54] - z[64];
    z[26]=z[26]*z[51];
    z[20]=z[20] + z[26] + z[9] - 2*z[66];
    z[20]=z[10]*z[20];
    z[26]= - z[100] + z[57];
    z[26]=z[12]*z[26];
    z[26]= - static_cast<T>(1)+ 3*z[26];
    z[26]=z[26]*z[51];
    z[20]=z[20] + z[22] + z[26];
    z[20]=z[10]*z[20];
    z[26]= - z[55] - z[59];
    z[26]=z[13]*z[26];
    z[26]=z[26] + z[58];
    z[26]=z[12]*z[26];
    z[26]=3*z[41] + z[26];
    z[26]=z[12]*z[26];
    z[27]= - static_cast<T>(2)- z[60];
    z[26]=2*z[27] + z[26];
    z[26]=z[12]*z[26];
    z[27]=z[68] + z[9];
    z[26]=z[26] - z[27];
    z[26]=z[26]*z[51];
    z[20]=z[26] + z[20];
    z[20]=z[3]*z[20];
    z[26]=n<T>(5,2)*z[4] + z[100];
    z[26]=z[26]*z[57];
    z[34]= - z[83] - n<T>(71,6)*z[13];
    z[34]=z[13]*z[34];
    z[26]=z[34] + z[26];
    z[26]=z[12]*z[26];
    z[34]=z[63] + n<T>(109,3)*z[13];
    z[26]=n<T>(1,2)*z[34] + z[26];
    z[26]=z[12]*z[26];
    z[26]=z[26] - n<T>(31,3) - 4*z[60];
    z[26]=z[12]*z[26];
    z[26]= - n<T>(3,2)*z[27] + z[26];
    z[26]=z[12]*z[26];
    z[19]=z[20] + z[19] - n<T>(1,3)*z[22] + z[26];
    z[19]=z[3]*z[19];
    z[20]= - z[95] - z[68];
    z[20]=n<T>(1,2)*z[20] - z[66];
    z[22]=n<T>(9,2)*z[4] + z[36];
    z[22]=z[22]*z[57];
    z[26]= - z[83] - n<T>(131,6)*z[13];
    z[26]=z[13]*z[26];
    z[22]=z[26] + z[22];
    z[22]=z[22]*z[29];
    z[26]=n<T>(3,4)*z[4] - z[6];
    z[22]=z[22] + 3*z[26] + n<T>(31,4)*z[13];
    z[22]=z[12]*z[22];
    z[26]= - n<T>(8,3) - z[60];
    z[22]=2*z[26] + z[22];
    z[22]=z[12]*z[22];
    z[19]=z[19] + z[21] + n<T>(1,2)*z[20] + z[22];
    z[19]=z[3]*z[19];
    z[20]= - z[4] - n<T>(13,2)*z[13];
    z[20]=z[20]*z[25];
    z[21]=z[41]*z[57];
    z[20]=z[20] + z[21];
    z[20]=z[12]*z[20];
    z[20]=z[20] + n<T>(5,6)*z[13] + z[38] - n<T>(7,3)*z[6];
    z[20]=z[12]*z[20];
    z[21]= - n<T>(7,3)*z[48] + n<T>(1,2) - z[60];
    z[18]=z[19] + z[18] + n<T>(1,2)*z[21] + z[20];
    z[18]=z[3]*z[18];
    z[19]= - z[13]*z[85];
    z[20]=z[6]*z[79];
    z[19]=z[19] + z[20];
    z[19]=z[10]*z[19];
    z[19]=z[44] - z[19];
    z[20]=z[46] - z[30];
    z[21]= - z[5]*z[20];
    z[21]= - 2*z[39] + z[21];
    z[21]=z[5]*z[21];
    z[22]=z[6] - z[23];
    z[22]=z[13]*z[22];
    z[23]= - n<T>(7,2)*z[15] - z[33];
    z[23]=z[8]*z[23];
    z[19]=z[23] + z[22] - z[85] + z[21] - n<T>(3,2)*z[19];
    z[19]=z[10]*z[19];
    z[21]=z[38] + z[59];
    z[21]=z[21]*z[80];
    z[21]=z[21] - z[75] + z[31] - z[85];
    z[21]=z[12]*z[21];
    z[22]= - n<T>(1,2)*z[9] + z[32] - n<T>(3,2)*z[28];
    z[22]=z[4]*z[22];
    z[23]=4*z[43];
    z[25]=z[14] + n<T>(1,3)*z[9];
    z[25]=z[8]*z[25];
    z[22]=z[25] + z[22] + n<T>(7,6) - z[23];
    z[22]=z[8]*z[22];
    z[20]=static_cast<T>(1)+ n<T>(1,2)*z[20];
    z[20]=z[5]*z[20];
    z[25]= - z[86]*z[56];
    z[23]= - n<T>(5,4) + z[23];
    z[23]=z[4]*z[23];
    z[16]=z[16] + z[18] + z[19] + z[21] + z[22] + z[25] + z[20] - n<T>(1,6)*
    z[6] + n<T>(1,3)*z[23] + z[24] + z[39];
    z[16]=z[1]*z[16];
    z[18]= - n<T>(2,3)*z[12] - n<T>(4,3)*z[14] + z[9];
    z[18]=z[4]*z[18];
    z[19]= - static_cast<T>(2)+ z[43];
    z[20]= - n<T>(1,2)*z[14] + z[9];
    z[20]=z[8]*z[20];
    z[17]=z[17] - z[8];
    z[17]=z[10]*z[17];
    z[21]=z[81] + n<T>(7,3)*z[9] + z[12];
    z[21]=z[3]*z[21];

    r += z[16] + z[17] + z[18] + n<T>(2,3)*z[19] + z[20] + n<T>(1,2)*z[21];
 
    return r;
}

template double qg_2lNLC_r1345(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1345(const std::array<dd_real,31>&);
#endif
