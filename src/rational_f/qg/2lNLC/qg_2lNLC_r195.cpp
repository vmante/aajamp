#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r195(const std::array<T,31>& k) {
  T z[135];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[7];
    z[9]=k[5];
    z[10]=k[13];
    z[11]=k[2];
    z[12]=k[14];
    z[13]=k[3];
    z[14]=k[23];
    z[15]=k[9];
    z[16]=k[24];
    z[17]=k[18];
    z[18]=npow(z[8],2);
    z[19]=n<T>(3,2)*z[18];
    z[20]=npow(z[7],2);
    z[21]= - 4*z[20] + z[19];
    z[21]=z[6]*z[21];
    z[22]=2*z[12];
    z[23]=z[20]*z[11];
    z[24]=z[2]*z[11];
    z[25]=z[24] + n<T>(7,2);
    z[26]= - z[2]*z[25];
    z[21]=4*z[4] + z[26] + z[21] + z[8] + 6*z[23] - z[22] - z[15];
    z[21]=z[5]*z[21];
    z[26]=n<T>(7,4)*z[8];
    z[27]=3*z[14];
    z[28]=n<T>(1,2)*z[4];
    z[29]= - z[28] + n<T>(313,18)*z[2] - z[26] + z[27] + n<T>(91,6)*z[15];
    z[29]=z[4]*z[29];
    z[30]=4*z[12];
    z[31]=13*z[16] - z[30];
    z[31]=z[31]*z[23];
    z[32]= - n<T>(457,3)*z[20] - 295*z[18];
    z[33]=z[6]*z[8];
    z[32]=z[32]*z[33];
    z[34]=3*z[10];
    z[35]=n<T>(76,9)*z[2] - z[34] + z[23];
    z[35]=z[2]*z[35];
    z[36]=npow(z[16],2);
    z[37]=n<T>(29,6)*z[36];
    z[38]=npow(z[15],2);
    z[39]=n<T>(17,3)*z[38];
    z[40]=npow(z[12],2);
    z[41]=2*z[16] - z[27];
    z[41]=z[10]*z[41];
    z[42]=n<T>(152,9)*z[8];
    z[43]=z[42] - z[12] + 2*z[10];
    z[43]=z[8]*z[43];
    z[21]=z[21] + z[29] + z[35] + n<T>(1,12)*z[32] + z[43] + n<T>(7,3)*z[31] - 
    z[39] + z[41] + z[37] - z[40];
    z[21]=z[5]*z[21];
    z[29]=n<T>(1,6)*z[2];
    z[31]=z[2]*z[6];
    z[32]=295*z[31];
    z[35]= - n<T>(1541,3) - z[32];
    z[35]=z[35]*z[29];
    z[41]=n<T>(41,3)*z[17];
    z[43]=z[41] - z[10];
    z[44]=z[20]*z[6];
    z[35]=z[35] + n<T>(11,6)*z[44] - n<T>(83,6)*z[8] + z[43];
    z[45]=n<T>(1,2)*z[2];
    z[35]=z[35]*z[45];
    z[46]=npow(z[14],2);
    z[47]=z[38] - z[46];
    z[48]=z[47]*z[13];
    z[49]=n<T>(31,6)*z[8];
    z[50]=z[15] - z[14];
    z[51]=n<T>(17,2)*z[2] + z[49] - n<T>(53,6)*z[50] - 2*z[48];
    z[51]=z[4]*z[51];
    z[52]=13*z[46];
    z[53]=20*z[36] - z[52];
    z[54]=z[20]*z[13];
    z[55]= - n<T>(5,3)*z[14] + z[15];
    z[55]=z[55]*z[54];
    z[56]=10*z[10];
    z[57]=z[56] - n<T>(43,4)*z[54];
    z[58]=z[8]*z[13];
    z[59]= - n<T>(209,12) + z[58];
    z[59]=z[8]*z[59];
    z[57]=n<T>(1,3)*z[57] + z[59];
    z[57]=z[8]*z[57];
    z[59]= - n<T>(4,3)*z[16] - z[12];
    z[59]=z[59]*z[44];
    z[60]= - n<T>(13,6)*z[16] + z[12];
    z[60]=z[10]*z[60];
    z[61]=3*z[12];
    z[62]= - z[61] + n<T>(5,3)*z[15];
    z[62]=z[15]*z[62];
    z[35]=z[51] + z[35] + 2*z[59] + z[57] + n<T>(5,2)*z[55] + z[62] + n<T>(1,3)*
    z[53] + z[60];
    z[35]=z[4]*z[35];
    z[51]=n<T>(1,2)*z[10];
    z[53]=z[16] - z[14];
    z[53]=z[53]*z[51];
    z[55]=n<T>(23,2)*z[36];
    z[57]=7*z[46];
    z[53]=z[53] + z[55] - z[57];
    z[59]=n<T>(1,3)*z[10];
    z[53]=z[53]*z[59];
    z[60]=npow(z[2],2);
    z[62]=n<T>(149,6)*z[60] + z[57] - 11*z[38];
    z[63]=n<T>(11,3)*z[50] + z[2];
    z[63]=z[63]*z[28];
    z[62]=n<T>(1,3)*z[62] + z[63];
    z[62]=z[4]*z[62];
    z[63]=n<T>(7,2)*z[2];
    z[64]= - z[15] - z[63];
    z[64]=z[4]*z[64];
    z[64]=z[64] + n<T>(3,2)*z[20] - z[60];
    z[64]=z[5]*z[64];
    z[65]=18*z[16] - n<T>(19,12)*z[12];
    z[65]=z[65]*z[20];
    z[66]=npow(z[10],2);
    z[67]=n<T>(1,2)*z[66];
    z[68]=11*z[10];
    z[69]= - z[8]*z[68];
    z[69]=z[67] + z[69];
    z[70]=n<T>(1,3)*z[8];
    z[69]=z[69]*z[70];
    z[71]=z[2]*z[10];
    z[72]= - z[66] + 13*z[71];
    z[29]=z[72]*z[29];
    z[29]=z[64] + z[62] + z[29] + z[69] + z[53] + z[65];
    z[29]=z[5]*z[29];
    z[53]=n<T>(1,2)*z[8];
    z[62]= - 31*z[10] + z[8];
    z[62]=z[62]*z[53];
    z[62]=z[62] - n<T>(43,3)*z[66] - z[20];
    z[62]=z[8]*z[62];
    z[64]= - n<T>(67,2)*z[20] + 13*z[18];
    z[65]=6*z[8];
    z[69]= - n<T>(349,36)*z[2] + z[41] - z[65];
    z[69]=z[2]*z[69];
    z[64]=n<T>(1,6)*z[64] + z[69];
    z[64]=z[2]*z[64];
    z[69]=z[50]*z[4];
    z[72]=n<T>(13,6)*z[69] - n<T>(9,2)*z[38] + n<T>(7,3)*z[46];
    z[73]=3*z[20];
    z[74]=z[2]*z[8];
    z[75]=z[73] + z[72] - 10*z[74];
    z[75]=z[4]*z[75];
    z[76]=z[10]*z[16];
    z[77]= - z[55] - n<T>(43,3)*z[76];
    z[77]=z[10]*z[77];
    z[78]= - 59*z[16] - 67*z[14];
    z[78]= - z[15] + n<T>(1,3)*z[78] - z[61];
    z[79]=n<T>(1,2)*z[20];
    z[78]=z[78]*z[79];
    z[62]=z[75] + z[64] + z[62] + z[77] + z[78];
    z[62]=z[4]*z[62];
    z[64]=z[15]*z[10];
    z[75]=n<T>(19,2)*z[14] - z[34];
    z[75]=z[10]*z[75];
    z[37]= - z[64] + z[75] - z[37] + z[57];
    z[37]=z[37]*z[20];
    z[75]=z[20]*z[8];
    z[77]=z[10]*z[20];
    z[77]=n<T>(31,2)*z[77] - n<T>(37,3)*z[75];
    z[78]=z[74] + z[18];
    z[80]=z[2]*z[78];
    z[77]=n<T>(1,2)*z[77] + z[80];
    z[77]=z[2]*z[77];
    z[80]=npow(z[5],2);
    z[81]=z[78] - z[80];
    z[82]=z[60]*z[4];
    z[81]=z[9]*z[82]*z[81];
    z[29]=z[81] + z[29] + z[62] + z[37] + z[77];
    z[29]=z[9]*z[29];
    z[37]=n<T>(331,36)*z[2];
    z[62]=2*z[38];
    z[77]=z[62]*z[11];
    z[81]=41*z[17];
    z[83]=z[81] - z[10];
    z[44]= - z[37] - n<T>(3,2)*z[44] - n<T>(45,4)*z[8] + z[23] - z[77] + n<T>(1,2)*
    z[83] + z[15];
    z[44]=z[2]*z[44];
    z[83]=n<T>(41,2)*z[17];
    z[68]=z[83] + z[68];
    z[68]=z[68]*z[20];
    z[68]=z[68] - n<T>(67,4)*z[75];
    z[68]=z[6]*z[68];
    z[65]=z[15] + z[65];
    z[65]=z[8]*z[65];
    z[44]=z[44] + n<T>(1,3)*z[68] + z[65] - z[62] - n<T>(3,2)*z[66];
    z[44]=z[2]*z[44];
    z[64]=z[64] + n<T>(11,3)*z[46] + n<T>(7,2)*z[66];
    z[64]=z[13]*z[64];
    z[65]=n<T>(21,2)*z[36];
    z[68]=z[15]*z[12];
    z[84]= - z[68] + z[65] - 17*z[40];
    z[84]=z[11]*z[84];
    z[64]=z[64] + z[84];
    z[64]=z[64]*z[20];
    z[84]= - n<T>(35,3)*z[40] - 47*z[66];
    z[85]=z[13]*z[73];
    z[85]=z[8] + z[85] + z[61] - n<T>(73,3)*z[10];
    z[85]=z[85]*z[53];
    z[86]= - z[12] - z[10];
    z[86]=z[15]*z[86];
    z[84]=z[85] + n<T>(1,2)*z[84] + z[86];
    z[84]=z[8]*z[84];
    z[85]=7*z[10];
    z[86]=n<T>(34,3)*z[14] + z[85];
    z[86]=z[10]*z[86];
    z[86]=z[86] - z[36] + n<T>(4,3)*z[46];
    z[86]=z[86]*z[20];
    z[87]= - z[18]*z[73];
    z[86]=z[86] + z[87];
    z[86]=z[6]*z[86];
    z[87]=23*z[36];
    z[88]= - z[87] - 19*z[76];
    z[88]=z[88]*z[51];
    z[21]=z[29] + z[21] + z[35] + z[44] + z[86] + z[84] + z[88] + z[64];
    z[21]=z[9]*z[21];
    z[29]=z[6]*z[3];
    z[35]= - n<T>(295,6)*z[31] - n<T>(1123,18) - z[29];
    z[35]=z[35]*z[45];
    z[43]= - n<T>(19,2)*z[3] + z[43];
    z[35]=z[35] + n<T>(1,2)*z[43] - z[70];
    z[35]=z[2]*z[35];
    z[43]=2*z[8];
    z[44]= - z[43] - z[85] + z[3];
    z[44]=z[8]*z[44];
    z[64]=n<T>(11,3) - z[29];
    z[64]=z[64]*z[45];
    z[84]=3*z[50];
    z[64]= - z[84] + z[64];
    z[64]=z[4]*z[64];
    z[85]=2*z[46];
    z[86]= - z[16]*z[51];
    z[88]=n<T>(1,2)*z[12];
    z[89]= - z[14] - z[88];
    z[89]=z[3]*z[89];
    z[35]=z[64] + z[35] + z[44] + z[89] - z[68] + z[86] - z[36] - z[85];
    z[35]=z[4]*z[35];
    z[44]=npow(z[13],2);
    z[64]=npow(z[7],3);
    z[86]=z[44]*z[64];
    z[89]=n<T>(11,2)*z[10];
    z[90]= - z[53] + z[89] - 3*z[86];
    z[90]=z[8]*z[90];
    z[91]=4*z[66];
    z[90]= - z[91] + z[90];
    z[90]=z[8]*z[90];
    z[47]=2*z[47];
    z[92]=z[50]*z[86];
    z[92]= - z[47] + z[92] - n<T>(10,3)*z[74];
    z[92]=z[4]*z[92];
    z[93]= - n<T>(17,3)*z[36] + z[76];
    z[93]=z[93]*z[51];
    z[86]= - z[85]*z[86];
    z[94]=z[36]*z[64];
    z[95]=npow(z[6],2);
    z[96]= - z[95]*z[94];
    z[97]=n<T>(1,2)*z[64];
    z[98]= - z[95]*z[97];
    z[99]=n<T>(41,6)*z[17];
    z[98]= - n<T>(313,12)*z[2] + z[98] + z[99] - 3*z[8];
    z[98]=z[2]*z[98];
    z[98]=n<T>(13,12)*z[18] + z[98];
    z[98]=z[2]*z[98];
    z[86]=z[92] + z[98] + z[96] + z[90] + z[93] + z[86];
    z[86]=z[4]*z[86];
    z[76]=z[87] + 29*z[76];
    z[76]=z[76]*z[67];
    z[90]=z[64]*z[13];
    z[92]= - z[90]*z[57];
    z[76]=z[76] + z[92];
    z[92]=3*z[15];
    z[93]=n<T>(11,3)*z[14] + z[92];
    z[93]=z[93]*z[90];
    z[96]=z[64]*z[31];
    z[98]=z[64]*z[8];
    z[100]=z[98]*z[13];
    z[93]=3*z[96] + z[93] - n<T>(11,3)*z[100];
    z[93]=z[93]*z[28];
    z[96]=3*z[66];
    z[101]=z[96] + z[90];
    z[102]= - z[10]*z[53];
    z[101]=2*z[101] + z[102];
    z[101]=z[8]*z[101];
    z[102]=npow(z[10],3);
    z[101]=n<T>(29,6)*z[102] + z[101];
    z[101]=z[8]*z[101];
    z[103]=z[6]*z[94];
    z[104]=z[6]*z[64];
    z[104]=z[104] + z[78];
    z[105]=n<T>(1,2)*z[60];
    z[104]=z[104]*z[105];
    z[76]=z[93] + z[104] - n<T>(35,6)*z[103] + n<T>(1,3)*z[76] + z[101];
    z[76]=z[4]*z[76];
    z[93]=z[18]*z[64];
    z[101]= - z[66]*z[97];
    z[103]=z[64]*z[2];
    z[104]=z[53]*z[103];
    z[101]=z[104] + z[101] + z[93];
    z[101]=z[2]*z[101];
    z[104]= - 21*z[98] - z[103];
    z[104]=z[2]*z[104];
    z[94]=z[104] - n<T>(29,3)*z[94] - z[93];
    z[104]=n<T>(31,2)*z[98] + 10*z[103];
    z[104]=z[4]*z[104];
    z[94]=n<T>(1,2)*z[94] + n<T>(1,3)*z[104];
    z[94]=z[4]*z[94];
    z[104]=n<T>(3,2)*z[12];
    z[105]= - z[64]*z[80]*z[104];
    z[106]=npow(z[8],3);
    z[107]=z[97]*z[106];
    z[94]=z[105] + z[94] + z[107] + z[101];
    z[94]=z[9]*z[94];
    z[101]= - z[4]*z[72];
    z[105]=npow(z[2],3);
    z[101]=n<T>(313,36)*z[105] + z[101];
    z[101]=z[4]*z[101];
    z[87]=z[87] - n<T>(1,3)*z[40];
    z[87]=z[11]*z[87]*z[97];
    z[108]=2*z[11];
    z[109]= - z[12]*z[64]*z[108];
    z[109]=z[109] - z[82];
    z[109]=z[5]*z[109];
    z[87]=3*z[109] + z[87] + z[101];
    z[87]=z[5]*z[87];
    z[101]=z[10]*z[14];
    z[109]=z[57] + n<T>(26,3)*z[101];
    z[110]=z[64]*z[10];
    z[109]=z[109]*z[110];
    z[109]=z[109] - z[107];
    z[109]=z[6]*z[109];
    z[111]=z[66]*z[64];
    z[112]=n<T>(8,3)*z[111] - z[93];
    z[112]=z[6]*z[112];
    z[113]= - 5*z[110] - z[98];
    z[114]=n<T>(1,2)*z[31];
    z[113]=z[113]*z[114];
    z[112]=z[112] + z[113];
    z[112]=z[2]*z[112];
    z[113]=z[90]*z[106];
    z[113]=n<T>(1,2)*z[113];
    z[76]=z[94] + z[87] + z[76] + z[112] - z[113] + z[109];
    z[76]=z[9]*z[76];
    z[87]=npow(z[11],2);
    z[94]=z[87]*z[64];
    z[109]= - z[94]*z[30];
    z[112]=z[95]*z[98];
    z[115]=n<T>(1,2)*z[18];
    z[116]=n<T>(1,2)*z[15];
    z[117]=z[116] - 5*z[2];
    z[117]=z[4]*z[117];
    z[109]=z[117] - 2*z[60] + z[112] + z[109] - z[115];
    z[109]=z[5]*z[109];
    z[112]=41*z[36] + 5*z[40];
    z[112]=z[112]*z[94];
    z[112]=z[112] + n<T>(295,6)*z[106];
    z[117]=z[95]*z[93];
    z[112]=n<T>(313,12)*z[105] + n<T>(1,2)*z[112] + n<T>(185,3)*z[117];
    z[117]=5*z[38];
    z[118]=n<T>(104,3)*z[60] + z[57] - z[117];
    z[119]=n<T>(41,3)*z[50] + 3*z[2];
    z[119]=z[119]*z[28];
    z[118]=n<T>(1,3)*z[118] + z[119];
    z[118]=z[4]*z[118];
    z[109]=z[109] + n<T>(1,3)*z[112] + z[118];
    z[109]=z[5]*z[109];
    z[112]=z[64]*z[41];
    z[103]= - z[103] + z[112] - n<T>(21,2)*z[98];
    z[103]=z[2]*z[103];
    z[93]=z[103] + 15*z[111] + n<T>(29,3)*z[93];
    z[93]=z[45]*z[95]*z[93];
    z[103]= - n<T>(1,3)*z[46] + 4*z[101];
    z[103]=z[103]*z[110];
    z[103]=z[103] + z[107];
    z[103]=z[6]*z[103];
    z[103]=z[113] + z[103];
    z[103]=z[6]*z[103];
    z[107]= - z[40]*z[18];
    z[76]=z[76] + z[109] + z[86] + z[93] + z[107] + z[103];
    z[76]=z[9]*z[76];
    z[86]=npow(z[3],2);
    z[93]=z[86]*z[94];
    z[94]= - 47*z[3] + 38*z[8];
    z[94]=z[8]*z[94];
    z[94]=z[86] + n<T>(2,9)*z[94];
    z[94]=z[8]*z[94];
    z[93]=z[93] + z[94];
    z[94]=6*z[86];
    z[103]=z[94]*z[64];
    z[107]=z[64]*z[3];
    z[109]= - 153*z[107] + n<T>(2323,18)*z[98];
    z[109]=z[109]*z[53];
    z[109]=z[103] + z[109];
    z[109]=z[6]*z[109];
    z[103]= - z[11]*z[103];
    z[110]=z[106]*z[3];
    z[103]=z[109] + z[103] + n<T>(313,36)*z[110];
    z[103]=z[6]*z[103];
    z[93]=2*z[93] + z[103];
    z[93]=z[6]*z[93];
    z[103]=z[3]*z[11];
    z[109]=z[64]*z[103];
    z[111]=3*z[3];
    z[112]= - z[64]*z[111];
    z[112]=z[112] + z[98];
    z[113]=n<T>(3,2)*z[6];
    z[112]=z[112]*z[113];
    z[113]=z[18]*z[3];
    z[109]=z[112] + 5*z[109] + n<T>(17,4)*z[113];
    z[109]=z[6]*z[109];
    z[112]=z[87]*z[3];
    z[118]=z[64]*z[112];
    z[119]= - n<T>(263,9)*z[3] + n<T>(15,2)*z[8];
    z[119]=z[119]*z[53];
    z[109]=z[109] - 2*z[118] + z[119];
    z[109]=z[6]*z[109];
    z[25]= - z[103] - z[25];
    z[25]=z[2]*z[25];
    z[118]=static_cast<T>(2)+ n<T>(1,2)*z[29];
    z[118]=z[4]*z[118];
    z[25]=3*z[118] + z[25] + z[109] + z[3] - n<T>(463,36)*z[8];
    z[25]=z[5]*z[25];
    z[109]=npow(z[11],3);
    z[118]=z[36]*z[109];
    z[119]=n<T>(1,2)*z[16];
    z[120]=z[109]*z[3];
    z[121]=z[120]*z[119];
    z[118]=z[118] + z[121];
    z[118]=z[118]*z[64];
    z[121]=z[64]*z[120];
    z[122]=n<T>(1,4)*z[2];
    z[123]=313*z[11] + 295*z[112];
    z[123]=z[123]*z[122];
    z[123]=z[123] + n<T>(431,4) + 134*z[103];
    z[124]=n<T>(1,9)*z[2];
    z[123]=z[123]*z[124];
    z[121]=z[123] + n<T>(11,2)*z[3] + z[121];
    z[121]=z[2]*z[121];
    z[123]=2*z[3];
    z[125]=z[14] + n<T>(77,2)*z[15];
    z[37]= - z[4] + z[37] - n<T>(3,4)*z[8] + n<T>(1,3)*z[125] + z[123];
    z[37]=z[4]*z[37];
    z[125]=z[3]*z[14];
    z[125]=z[125] + z[85];
    z[126]=z[15] - z[12];
    z[127]= - n<T>(295,18)*z[8] + n<T>(1,2)*z[126] + n<T>(92,9)*z[3];
    z[127]=z[8]*z[127];
    z[25]=z[25] + z[37] + z[121] + z[93] + z[127] + z[118] + z[125];
    z[25]=z[5]*z[25];
    z[37]=n<T>(11,4)*z[3] + 25*z[2];
    z[37]=z[37]*z[124];
    z[93]=z[3]*z[53];
    z[84]=n<T>(3,2)*z[2] + z[84] - n<T>(1,2)*z[3];
    z[84]=z[4]*z[84];
    z[37]=z[84] + z[37] + z[93] + z[125];
    z[37]=z[4]*z[37];
    z[84]=npow(z[6],3);
    z[93]=npow(z[7],4);
    z[118]=z[84]*z[93];
    z[121]=z[8]*z[123]*z[118];
    z[121]= - n<T>(5,4)*z[113] + z[121];
    z[121]=z[6]*z[121];
    z[125]=z[2] + z[3];
    z[127]= - z[2]*z[125];
    z[128]=z[3] - z[63];
    z[128]=z[4]*z[128];
    z[121]=z[128] + z[127] - n<T>(5,4)*z[18] + z[121];
    z[121]=z[5]*z[121];
    z[127]=152*z[8];
    z[128]=n<T>(1565,4)*z[3] - z[127];
    z[129]=n<T>(1,9)*z[8];
    z[128]=z[129]*z[93]*z[128];
    z[130]=2*z[86];
    z[131]= - z[93]*z[130];
    z[128]=z[131] + z[128];
    z[128]=z[8]*z[128]*npow(z[6],4);
    z[131]=z[18]*z[123];
    z[132]=static_cast<T>(152)+ n<T>(295,4)*z[103];
    z[132]=z[2]*z[132];
    z[132]=n<T>(241,4)*z[3] + z[132];
    z[132]=z[132]*z[60];
    z[37]=z[121] + z[37] + n<T>(1,9)*z[132] + z[131] + z[128];
    z[37]=z[5]*z[37];
    z[121]= - z[6]*z[66]*z[93]*z[60];
    z[128]=z[2]*z[53];
    z[128]=z[18] + z[128];
    z[128]=z[2]*z[128];
    z[74]=z[74]*z[4];
    z[128]= - n<T>(10,3)*z[74] + z[128];
    z[128]=z[93]*z[128];
    z[131]=z[93]*z[106];
    z[128]=n<T>(1,2)*z[131] + z[128];
    z[128]=z[9]*z[4]*z[128];
    z[132]= - z[13]*z[28]*z[131];
    z[80]=z[80]*z[93];
    z[133]=z[40]*z[11];
    z[134]=z[133]*z[80];
    z[121]=z[128] + z[134] + z[121] + z[132];
    z[121]=z[9]*z[121];
    z[93]=z[95]*z[93];
    z[128]=z[93]*z[66];
    z[132]=z[57] + n<T>(23,2)*z[101];
    z[132]=z[132]*z[128];
    z[71]= - z[93]*z[71];
    z[71]= - n<T>(3,2)*z[128] + z[71];
    z[71]=z[2]*z[71];
    z[93]=z[102]*z[93];
    z[71]=n<T>(23,6)*z[93] + z[71];
    z[71]=z[2]*z[71];
    z[80]=z[40]*z[87]*z[80];
    z[71]=z[121] + z[80] + n<T>(1,3)*z[132] + z[71];
    z[71]=z[9]*z[71];
    z[80]=z[84]*z[131];
    z[47]=z[4]*z[47];
    z[47]=n<T>(307,12)*z[105] + z[47];
    z[47]=z[4]*z[47];
    z[84]=z[118]*z[115];
    z[84]=z[84] - 3*z[82];
    z[84]=z[5]*z[84];
    z[47]=z[84] - n<T>(295,36)*z[80] + z[47];
    z[47]=z[5]*z[47];
    z[47]=z[47] + z[71];
    z[47]=z[9]*z[47];
    z[71]=z[6]*z[7];
    z[71]=npow(z[71],5);
    z[80]=z[71]*z[110];
    z[84]=z[105]*z[3];
    z[80]= - 313*z[80] + 295*z[84];
    z[93]= - z[111] + n<T>(301,6)*z[2];
    z[93]=z[93]*z[60];
    z[102]=z[4]*z[2];
    z[110]=z[102]*z[3];
    z[93]=z[93] + z[110];
    z[93]=z[4]*z[93];
    z[80]=n<T>(1,18)*z[80] + z[93];
    z[71]=z[113]*z[71];
    z[93]= - z[125]*z[102];
    z[71]= - n<T>(1,2)*z[71] + z[93];
    z[71]=z[5]*z[71];
    z[71]=n<T>(1,2)*z[80] + z[71];
    z[71]=z[5]*z[71];
    z[67]= - z[95]*z[105]*npow(z[9],3)*z[67]*npow(z[7],5);
    z[80]=z[1]*z[5];
    z[80]=z[80] - 1;
    z[84]=n<T>(295,36)*z[84];
    z[80]=z[80]*z[84]*z[4];
    z[67]=z[67] + z[71] + z[80];
    z[67]=z[1]*z[67];
    z[71]=z[3]*z[13];
    z[80]=z[71] - z[29];
    z[80]=z[95]*z[131]*z[80];
    z[93]=z[40]*z[3];
    z[105]= - z[18]*z[93];
    z[80]=z[105] + n<T>(313,36)*z[80];
    z[80]=z[6]*z[80];
    z[105]=z[88]*z[3];
    z[113]= - z[40] - z[105];
    z[113]=z[113]*z[18];
    z[115]= - static_cast<T>(301)- n<T>(295,3)*z[29];
    z[115]=z[2]*z[115];
    z[115]= - n<T>(241,3)*z[3] + z[115];
    z[115]=z[115]*z[60];
    z[110]=n<T>(1,6)*z[115] - z[110];
    z[110]=z[110]*z[28];
    z[37]=z[67] + z[47] + z[37] + z[110] - z[84] + z[113] + z[80];
    z[37]=z[1]*z[37];
    z[47]=z[3]*z[12];
    z[67]=z[12] - z[3];
    z[67]=z[8]*z[67];
    z[47]=n<T>(3,2)*z[67] - n<T>(7,12)*z[47] - n<T>(11,6)*z[40] - z[68];
    z[47]=z[8]*z[47];
    z[67]=z[90]*z[130];
    z[80]=z[3]*z[90];
    z[80]= - n<T>(319,6)*z[80] - z[100];
    z[80]=z[80]*z[53];
    z[67]=z[67] + z[80];
    z[67]=z[8]*z[67];
    z[64]= - z[64]*z[130];
    z[80]=n<T>(1583,4)*z[107] - 152*z[98];
    z[80]=z[80]*z[129];
    z[64]=z[64] + z[80];
    z[33]=z[64]*z[33];
    z[33]=z[67] + z[33];
    z[33]=z[6]*z[33];
    z[64]=z[44]*z[3];
    z[67]= - z[97]*z[64];
    z[30]=z[3]*z[30];
    z[30]=z[67] - z[40] + z[30];
    z[30]=z[8]*z[30];
    z[67]=n<T>(5,6)*z[93];
    z[30]= - z[67] + z[30];
    z[30]=z[8]*z[30];
    z[30]=z[30] + z[33];
    z[30]=z[6]*z[30];
    z[33]=z[38]*z[11];
    z[80]= - z[92] - 5*z[33];
    z[80]=z[3]*z[80];
    z[84]=z[8]*z[126];
    z[90]= - z[3] - n<T>(295,6)*z[2];
    z[90]=z[2]*z[90];
    z[80]=z[90] + z[84] + z[117] + z[80];
    z[80]=z[80]*z[45];
    z[25]=z[37] + z[76] + z[25] + z[35] + z[80] + z[47] + z[30];
    z[25]=z[1]*z[25];
    z[30]=z[50]*z[44]*z[20];
    z[35]=z[13]*z[15];
    z[37]=n<T>(7,2)*z[58] - 4*z[71] + n<T>(227,12) - z[35];
    z[37]=z[8]*z[37];
    z[47]=n<T>(1,2)*z[6];
    z[76]=z[47]*z[20];
    z[80]=z[12] + z[16];
    z[84]= - z[80]*z[76];
    z[84]=z[84] + 2*z[36] - z[105];
    z[84]=z[6]*z[84];
    z[90]= - n<T>(7,2) - 3*z[29];
    z[90]=z[90]*z[31];
    z[97]= - n<T>(5,3) - z[29];
    z[90]=n<T>(29,4)*z[97] + z[90];
    z[90]=z[2]*z[90];
    z[97]=3*z[13];
    z[98]= - z[50]*z[97];
    z[98]=z[114] + static_cast<T>(1)+ z[98];
    z[98]=z[4]*z[98];
    z[100]=15*z[15];
    z[105]= - z[13]*z[85];
    z[107]=z[13]*z[14];
    z[110]= - n<T>(27,4) + 4*z[107];
    z[110]=z[3]*z[110];
    z[30]=z[98] + z[90] + z[84] + z[37] + z[30] + z[110] + z[105] - 
    z[100] - z[104] + n<T>(5,2)*z[16] - n<T>(22,3)*z[14];
    z[30]=z[4]*z[30];
    z[37]=z[87]*z[20];
    z[84]= - z[92] - z[77];
    z[84]=z[11]*z[84];
    z[84]= - n<T>(29,12) + z[84];
    z[84]=z[11]*z[84];
    z[84]=z[84] - z[112];
    z[84]=z[3]*z[84];
    z[76]= - z[76] - z[3] + n<T>(1,2)*z[23];
    z[76]=z[6]*z[76];
    z[90]= - n<T>(295,9)*z[11] - z[112];
    z[90]=z[90]*z[45];
    z[98]=z[15] + z[77];
    z[98]=z[11]*z[98];
    z[98]= - n<T>(176,9) + z[98];
    z[76]=z[90] + 3*z[76] - z[37] + 2*z[98] + z[84];
    z[76]=z[2]*z[76];
    z[34]=z[111] - z[41] + z[34];
    z[34]=z[34]*z[79];
    z[34]=z[34] + z[75];
    z[34]=z[6]*z[34];
    z[75]=2*z[103];
    z[84]=z[88]*z[11];
    z[90]= - z[84] - z[75];
    z[90]=z[90]*z[20];
    z[98]=z[53] + z[10];
    z[105]=z[12]*z[98];
    z[34]=z[34] + z[90] + z[105];
    z[34]=z[6]*z[34];
    z[90]=z[41] - z[61];
    z[105]=z[11]*z[15];
    z[110]= - z[12] + n<T>(23,3)*z[15];
    z[110]=z[110]*z[105];
    z[33]= - n<T>(23,2)*z[15] - n<T>(17,3)*z[33];
    z[33]=z[11]*z[33];
    z[111]= - n<T>(23,6)*z[11] - z[112];
    z[111]=z[3]*z[111];
    z[33]=z[111] + n<T>(107,36) + z[33];
    z[33]=z[3]*z[33];
    z[111]=z[88] + 2*z[15];
    z[111]=z[111]*z[87];
    z[111]=z[111] + n<T>(5,2)*z[112];
    z[111]=z[111]*z[20];
    z[33]=z[76] + z[34] - n<T>(9,2)*z[8] + z[111] + z[33] + z[110] + 4*z[15]
    + n<T>(1,2)*z[90] + z[59];
    z[33]=z[2]*z[33];
    z[34]=n<T>(317,4)*z[3] - n<T>(152,3)*z[8];
    z[34]=z[8]*z[34];
    z[34]=z[34] - z[94] - n<T>(749,9)*z[20];
    z[34]=z[8]*z[34];
    z[76]=z[29]*z[106];
    z[90]=n<T>(313,12)*z[76];
    z[110]=z[20]*z[3];
    z[34]= - z[90] + n<T>(1163,18)*z[110] + z[34];
    z[34]=z[6]*z[34];
    z[111]=z[20]*z[103];
    z[113]= - 1171*z[3] + n<T>(3371,2)*z[8];
    z[113]=z[8]*z[113];
    z[34]=z[34] + n<T>(1,18)*z[113] + z[94] - n<T>(1103,36)*z[111];
    z[34]=z[6]*z[34];
    z[111]=z[29]*z[18];
    z[113]=n<T>(281,9)*z[3] - n<T>(15,4)*z[8];
    z[113]=z[8]*z[113];
    z[113]= - n<T>(21,4)*z[111] - z[73] + z[113];
    z[113]=z[6]*z[113];
    z[23]= - n<T>(479,9)*z[3] - z[23];
    z[23]=z[4] + z[113] + n<T>(1,2)*z[23] + n<T>(245,9)*z[8];
    z[23]=z[6]*z[23];
    z[113]=z[120] + z[87];
    z[113]=z[113]*z[2];
    z[114]=n<T>(113,18)*z[112];
    z[115]=z[11]*z[12];
    z[118]= - static_cast<T>(7)+ z[115];
    z[118]=z[11]*z[118];
    z[118]=z[118] - z[114];
    z[118]=n<T>(1,2)*z[118] - z[113];
    z[118]=z[2]*z[118];
    z[23]=z[118] + 2*z[37] - n<T>(23,4) + z[103] + z[23];
    z[23]=z[5]*z[23];
    z[37]=3*z[112];
    z[118]=n<T>(107,9)*z[11] - z[37];
    z[118]=z[118]*z[45];
    z[118]=z[118] - n<T>(41,18)*z[103] + static_cast<T>(10)+ z[84];
    z[118]=z[2]*z[118];
    z[121]=47*z[16] - n<T>(67,2)*z[12];
    z[121]=n<T>(1,6)*z[121] + z[15];
    z[121]=z[121]*z[87];
    z[121]=z[121] + n<T>(37,4)*z[112];
    z[121]=z[121]*z[20];
    z[125]=z[11]*z[36];
    z[128]=z[11]*z[16];
    z[130]=n<T>(427,9) + n<T>(1,2)*z[128];
    z[130]=z[3]*z[130];
    z[131]=n<T>(11,2) + z[29];
    z[131]=z[4]*z[131];
    z[23]=z[23] + z[131] + z[118] + z[34] - n<T>(799,36)*z[8] + z[121] + 
    z[130] + z[125] + n<T>(49,3)*z[15] - n<T>(7,4)*z[12] + n<T>(10,3)*z[16] - z[14];
    z[23]=z[5]*z[23];
    z[34]=7*z[12] + n<T>(617,18)*z[3];
    z[34]= - z[42] + n<T>(1,2)*z[34] + 5*z[54];
    z[34]=z[8]*z[34];
    z[42]=z[20]*z[71];
    z[54]=n<T>(13,6)*z[12] - z[123];
    z[54]=z[3]*z[54];
    z[34]=z[34] + n<T>(313,12)*z[42] - n<T>(59,6)*z[40] + z[54];
    z[34]=z[8]*z[34];
    z[42]=z[94] - z[85] - n<T>(7,2)*z[101];
    z[42]=z[42]*z[20];
    z[54]=313*z[3];
    z[54]= - z[8]*z[54];
    z[54]=1505*z[20] + z[54];
    z[54]=z[8]*z[54];
    z[54]=n<T>(1,36)*z[54] - 5*z[93] - n<T>(299,4)*z[110];
    z[54]=z[8]*z[54];
    z[42]=z[42] + z[54];
    z[42]=z[6]*z[42];
    z[54]=2*z[13];
    z[93]= - z[54] - 3*z[11];
    z[93]=z[93]*z[123];
    z[110]=n<T>(5,2)*z[128];
    z[93]= - z[110] + z[93];
    z[93]=z[3]*z[93];
    z[118]=z[46]*z[13];
    z[93]= - 4*z[118] + z[93];
    z[93]=z[93]*z[20];
    z[34]=z[42] + z[34] - z[67] + z[93];
    z[34]=z[6]*z[34];
    z[42]=z[16]*z[112];
    z[42]=z[42] - 5*z[107] + n<T>(23,6)*z[128];
    z[42]=z[3]*z[42];
    z[67]=3*z[36];
    z[93]= - z[67] + n<T>(10,3)*z[38];
    z[93]=z[11]*z[93];
    z[121]=41*z[10];
    z[125]=z[121] + n<T>(5,2)*z[12] - 11*z[16] - 26*z[14];
    z[130]=z[13]*z[62];
    z[42]=z[42] + z[93] + z[130] + n<T>(1,3)*z[125] - n<T>(17,2)*z[15];
    z[42]=z[3]*z[42];
    z[68]= - z[68] + z[67] - 2*z[40];
    z[68]=z[68]*z[87];
    z[93]=3*z[16] + z[126];
    z[93]=z[11]*z[93];
    z[35]=z[35] + z[93];
    z[35]=z[11]*z[35];
    z[93]=z[11] + z[13];
    z[75]=z[93]*z[75];
    z[35]=n<T>(1,2)*z[35] + z[75];
    z[35]=z[3]*z[35];
    z[75]= - z[44]*z[85];
    z[35]=z[35] + z[75] + z[68];
    z[20]=z[35]*z[20];
    z[35]= - z[44]*z[73];
    z[68]= - n<T>(149,6) + 7*z[71];
    z[35]=z[58] + n<T>(1,2)*z[68] + z[35];
    z[35]=z[8]*z[35];
    z[68]=z[64]*z[79];
    z[73]=n<T>(10,9) + z[71];
    z[73]=z[3]*z[73];
    z[35]=z[35] + z[68] + 5*z[73] + z[15] + z[104] - n<T>(58,3)*z[10];
    z[35]=z[8]*z[35];
    z[68]= - 25*z[36] - 3*z[40];
    z[73]= - z[81] + 4*z[16];
    z[73]=n<T>(1,3)*z[73] + 4*z[14];
    z[73]=z[10]*z[73];
    z[75]=6*z[12];
    z[79]= - z[75] + n<T>(37,6)*z[15];
    z[79]=z[15]*z[79];
    z[20]=z[25] + z[21] + z[23] + z[30] + z[33] + z[34] + z[35] + z[20]
    + z[42] + z[79] + n<T>(1,2)*z[68] + z[73];
    z[20]=z[1]*z[20];
    z[21]= - z[48] + n<T>(2,3)*z[14] + z[15];
    z[21]=z[13]*z[21];
    z[21]=static_cast<T>(2)+ z[21];
    z[21]=n<T>(5,2)*z[31] + 2*z[21] - n<T>(11,6)*z[58];
    z[21]=z[4]*z[21];
    z[23]=z[85] - n<T>(59,2)*z[38];
    z[25]=n<T>(1,3)*z[13];
    z[23]=z[23]*z[25];
    z[30]=z[44]*z[8];
    z[33]=n<T>(175,6)*z[13] - z[30];
    z[33]=z[8]*z[33];
    z[33]=n<T>(251,6) + z[33];
    z[33]=z[33]*z[53];
    z[34]= - n<T>(57,2) - 5*z[31];
    z[34]=z[34]*z[45];
    z[35]=z[36]*z[6];
    z[21]=z[21] + z[34] - n<T>(17,6)*z[35] + z[33] + z[23] - n<T>(33,2)*z[15] + 
    z[51] - z[104] + z[119] - n<T>(41,3)*z[14];
    z[21]=z[4]*z[21];
    z[23]=z[11]*z[92];
    z[33]=n<T>(27,2)*z[8] - z[83] - n<T>(16,3)*z[10];
    z[33]=z[6]*z[33];
    z[23]= - n<T>(331,36)*z[31] + z[33] - n<T>(581,36) + z[23];
    z[23]=z[2]*z[23];
    z[33]= - 2*z[66] - n<T>(39,4)*z[18];
    z[33]=z[6]*z[33];
    z[34]=16*z[10];
    z[23]=z[23] + z[33] + z[26] - z[77] + z[92] - z[41] + z[34];
    z[23]=z[2]*z[23];
    z[26]=17*z[2] - z[8] + 5*z[14] + 11*z[15];
    z[26]=z[26]*z[28];
    z[33]=z[2] + z[15];
    z[42]=z[133] - n<T>(7,2)*z[12] - z[33];
    z[42]=z[5]*z[42];
    z[51]= - n<T>(8,3)*z[16] - n<T>(5,2)*z[14];
    z[51]=z[10]*z[51];
    z[66]= - z[88] - n<T>(8,3)*z[10];
    z[66]=z[8]*z[66];
    z[68]= - n<T>(5,2)*z[10] + z[2];
    z[68]=z[2]*z[68];
    z[26]=z[42] + z[26] + z[68] + z[66] + z[51] - z[55] - z[40];
    z[26]=z[5]*z[26];
    z[42]=z[13]*z[72];
    z[42]=n<T>(19,2)*z[2] + n<T>(31,3)*z[8] - n<T>(19,3)*z[50] + z[42];
    z[42]=z[4]*z[42];
    z[51]=29*z[36] - 14*z[46];
    z[55]=n<T>(11,3)*z[15];
    z[66]= - z[22] + z[55];
    z[66]=z[15]*z[66];
    z[32]= - static_cast<T>(677)- z[32];
    z[32]=z[32]*z[124];
    z[32]= - 67*z[8] + z[32];
    z[32]=z[32]*z[122];
    z[68]=n<T>(27,2)*z[16] + z[12];
    z[68]=z[10]*z[68];
    z[72]=35*z[10] - n<T>(11,3)*z[8];
    z[72]=z[8]*z[72];
    z[32]=z[42] + z[32] + n<T>(1,4)*z[72] + z[66] + n<T>(1,3)*z[51] + z[68];
    z[32]=z[4]*z[32];
    z[42]= - z[96] - z[18];
    z[42]=z[42]*z[47];
    z[51]= - z[98]*z[31];
    z[66]=z[81] - 13*z[10];
    z[42]=z[51] + z[42] + n<T>(1,6)*z[66] - z[43];
    z[42]=z[2]*z[42];
    z[51]= - z[91] + n<T>(25,4)*z[18];
    z[42]=n<T>(1,3)*z[51] + z[42];
    z[42]=z[2]*z[42];
    z[51]=z[9]*z[28];
    z[51]=z[51] + n<T>(1,2);
    z[51]=z[51]*z[78]*z[60];
    z[60]=5*z[8];
    z[66]= - z[2] + z[41] - z[60];
    z[66]=z[2]*z[66];
    z[66]=n<T>(25,6)*z[18] + z[66];
    z[45]=z[66]*z[45];
    z[45]= - 10*z[74] + z[106] + z[45];
    z[45]=z[4]*z[45];
    z[33]= - z[33]*z[5]*z[4];
    z[33]=z[82] + z[33];
    z[33]=z[5]*z[33];
    z[33]=z[33] + z[45] + z[51];
    z[33]=z[9]*z[33];
    z[45]=z[57] + n<T>(1,2)*z[101];
    z[45]=z[45]*z[59];
    z[26]=z[33] + z[26] + z[32] + z[42] + z[45] + z[106];
    z[26]=z[9]*z[26];
    z[19]= - z[6]*z[19];
    z[19]= - z[43] + z[19];
    z[19]=z[6]*z[19];
    z[32]= - z[75] - z[15];
    z[32]=z[11]*z[32];
    z[19]= - z[24] + z[19] + static_cast<T>(6)+ z[32];
    z[19]=z[5]*z[19];
    z[32]= - 32*z[36] - n<T>(7,2)*z[40];
    z[32]=n<T>(1,3)*z[32] - z[62];
    z[32]=z[11]*z[32];
    z[33]=z[6]*z[106];
    z[18]= - n<T>(74,3)*z[18] + n<T>(59,4)*z[33];
    z[18]=z[6]*z[18];
    z[33]=z[100] - n<T>(17,3)*z[12] - 45*z[16] - z[14];
    z[24]=static_cast<T>(6)+ z[24];
    z[24]=z[2]*z[24];
    z[18]=z[19] + n<T>(11,2)*z[4] + z[24] + n<T>(5,3)*z[18] + n<T>(361,36)*z[8] + n<T>(1,2)*z[33] + z[32];
    z[18]=z[5]*z[18];
    z[19]= - 47*z[40] - n<T>(11,2)*z[36] + z[52];
    z[22]= - z[22] - z[10];
    z[22]=2*z[22] + z[55];
    z[22]=z[15]*z[22];
    z[24]=z[49] + n<T>(27,2)*z[12] + 37*z[10];
    z[24]=z[24]*z[53];
    z[32]=z[57] + n<T>(19,2)*z[101];
    z[32]=z[10]*z[32];
    z[32]=z[32] - z[106];
    z[32]=z[6]*z[32];
    z[33]= - n<T>(23,2)*z[10] + z[12] + 17*z[16] + n<T>(39,2)*z[14];
    z[33]=z[10]*z[33];
    z[18]=z[26] + z[18] + z[21] + z[23] + z[32] + z[24] + z[22] + n<T>(1,3)*
    z[19] + z[33];
    z[18]=z[9]*z[18];
    z[19]=z[123]*z[44];
    z[21]= - z[14]*z[19];
    z[21]=z[21] + z[110] - static_cast<T>(6)- n<T>(7,3)*z[107];
    z[21]=z[3]*z[21];
    z[22]= - z[121] - 17*z[12] + 32*z[16] + n<T>(91,2)*z[14];
    z[21]=z[21] - z[133] + n<T>(1,3)*z[22] + 6*z[118];
    z[21]=z[3]*z[21];
    z[22]=n<T>(407,36)*z[13] + z[19];
    z[22]=z[3]*z[22];
    z[23]=z[53]*z[64];
    z[22]=z[23] - n<T>(368,9) + z[22];
    z[22]=z[8]*z[22];
    z[23]=z[25] + z[19];
    z[23]=z[3]*z[23];
    z[23]=n<T>(745,18) + z[23];
    z[23]=z[3]*z[23];
    z[24]=37*z[12] - z[34];
    z[22]=z[22] + n<T>(1,3)*z[24] + z[23];
    z[22]=z[8]*z[22];
    z[23]=z[61] + z[123];
    z[23]=z[23]*z[123];
    z[24]=static_cast<T>(304)- n<T>(313,4)*z[71];
    z[24]=z[8]*z[24];
    z[24]= - 559*z[3] + z[24];
    z[24]=z[24]*z[129];
    z[23]=z[23] + z[24];
    z[23]=z[8]*z[23];
    z[23]=z[23] + n<T>(313,18)*z[76];
    z[23]=z[6]*z[23];
    z[24]=z[81] + 35*z[14];
    z[24]=n<T>(1,3)*z[24] - z[12];
    z[24]=z[10]*z[24];
    z[21]=z[23] + z[22] + z[21] + z[24] - 16*z[40] - z[36] + z[52];
    z[21]=z[6]*z[21];
    z[22]= - z[44]*z[43];
    z[19]=z[22] - n<T>(103,12)*z[13] + z[19];
    z[19]=z[8]*z[19];
    z[22]=6*z[14];
    z[23]= - 4*z[48] + z[22] - n<T>(17,3)*z[15];
    z[23]=z[13]*z[23];
    z[24]= - z[27] + z[15];
    z[24]=z[24]*z[64];
    z[25]= - z[35] - z[80];
    z[25]=z[6]*z[25];
    z[26]= - z[95]*z[63];
    z[26]= - 8*z[6] + z[26];
    z[26]=z[2]*z[26];
    z[27]=z[44]*z[69];
    z[19]=2*z[27] + z[26] + z[25] + z[19] + z[24] - n<T>(69,4) + z[23];
    z[19]=z[4]*z[19];
    z[23]= - n<T>(623,2)*z[3] + z[127];
    z[23]=z[23]*z[70];
    z[23]=z[94] + z[23];
    z[23]=z[8]*z[23];
    z[23]=z[23] + z[90];
    z[23]=z[6]*z[23];
    z[24]=397*z[3] - n<T>(1276,3)*z[8];
    z[24]=z[24]*z[70];
    z[23]=z[23] - 12*z[86] + z[24];
    z[23]=z[6]*z[23];
    z[24]= - n<T>(3499,36) + 6*z[103];
    z[24]=z[3]*z[24];
    z[23]=z[23] + n<T>(617,6)*z[8] + n<T>(1,2)*z[14] + z[24];
    z[23]=z[6]*z[23];
    z[24]=n<T>(214,9) - z[84];
    z[24]=z[11]*z[24];
    z[24]=z[113] + z[24] + n<T>(343,36)*z[112];
    z[24]=z[2]*z[24];
    z[25]= - n<T>(67,9)*z[3] + z[53];
    z[25]=z[25]*z[60];
    z[25]=z[25] + n<T>(11,2)*z[111];
    z[25]=z[6]*z[25];
    z[26]=569*z[3] - n<T>(571,2)*z[8];
    z[25]=n<T>(1,9)*z[26] + z[25];
    z[25]=z[25]*z[47];
    z[25]=z[25] + n<T>(43,4) - n<T>(161,9)*z[103];
    z[25]=z[6]*z[25];
    z[26]=n<T>(211,18) - z[115];
    z[26]=z[11]*z[26];
    z[26]=z[26] + z[114];
    z[27]=z[2]*z[120];
    z[25]=z[27] + n<T>(1,2)*z[26] + z[25];
    z[25]=z[5]*z[25];
    z[26]= - z[36]*z[108];
    z[27]= - n<T>(61,2)*z[16] - 16*z[12];
    z[26]=z[26] + n<T>(1,3)*z[27] + 8*z[15];
    z[26]=z[11]*z[26];
    z[27]=n<T>(1823,36) - z[128];
    z[27]=z[27]*z[103];
    z[23]=z[25] + z[24] + z[23] + z[27] - n<T>(82,9) + z[26];
    z[23]=z[5]*z[23];
    z[24]=3*z[46] - z[38];
    z[24]=z[24]*z[54];
    z[24]=z[24] - n<T>(8,3)*z[15] + n<T>(27,2)*z[14] - n<T>(41,3)*z[10];
    z[24]=z[13]*z[24];
    z[25]=z[67] + z[39];
    z[25]=z[11]*z[25];
    z[26]= - z[13]*z[117];
    z[26]=z[26] - 89*z[15] - 31*z[16] + n<T>(17,2)*z[12];
    z[25]=n<T>(1,6)*z[26] + z[25];
    z[25]=z[11]*z[25];
    z[26]=static_cast<T>(2)- z[107];
    z[26]=z[13]*z[26];
    z[27]=static_cast<T>(2)- 3*z[128];
    z[27]=z[11]*z[27];
    z[26]=z[26] + z[27];
    z[26]=z[3]*z[26];
    z[24]=z[26] + z[25] - n<T>(11,4) + z[24];
    z[24]=z[3]*z[24];
    z[25]= - z[31] - n<T>(179,9) - z[105];
    z[25]=z[11]*z[25];
    z[26]= - z[15]*z[108];
    z[26]=n<T>(5,2) + z[26];
    z[26]=z[26]*z[112];
    z[27]=n<T>(122,9) - n<T>(7,2)*z[103];
    z[27]=z[6]*z[27];
    z[25]=z[27] + z[26] + z[25];
    z[25]=z[2]*z[25];
    z[26]= - n<T>(73,6)*z[15] + z[77];
    z[26]=z[11]*z[26];
    z[26]=n<T>(1067,36) + z[26];
    z[26]=z[11]*z[26];
    z[26]=z[26] + z[37];
    z[26]=z[3]*z[26];
    z[27]=z[83] - 32*z[10];
    z[32]= - n<T>(149,18) - z[103];
    z[32]=z[3]*z[32];
    z[27]=n<T>(13,6)*z[8] + n<T>(1,3)*z[27] + n<T>(5,2)*z[32];
    z[27]=z[6]*z[27];
    z[32]= - z[115] + n<T>(7,2);
    z[32]=z[15]*z[32];
    z[32]=z[12] + z[32];
    z[32]=z[11]*z[32];
    z[25]=z[25] + z[27] + z[26] + n<T>(31,36) + z[32];
    z[25]=z[2]*z[25];
    z[26]=z[10]*z[41];
    z[26]= - n<T>(16,3)*z[38] + 9*z[46] + z[26];
    z[26]=z[13]*z[26];
    z[27]= - 5*z[12] + z[55];
    z[27]=z[15]*z[27];
    z[27]=z[27] + z[65] - n<T>(8,3)*z[40];
    z[27]=z[11]*z[27];
    z[30]= - z[30] + n<T>(121,6)*z[13] - 7*z[64];
    z[30]=z[30]*z[53];
    z[32]= - n<T>(667,36)*z[13] + z[64];
    z[32]=z[3]*z[32];
    z[30]=z[30] + n<T>(371,18) + z[32];
    z[30]=z[8]*z[30];
    z[18]=z[20] + z[18] + z[23] + z[19] + z[25] + z[21] + z[30] + z[24]
    + z[27] + z[26] - n<T>(71,6)*z[15] - z[34] + n<T>(29,6)*z[12] + n<T>(26,3)*
    z[14] - z[99] + 23*z[16];
    z[18]=z[1]*z[18];
    z[19]= - n<T>(23,6)*z[36] + 6*z[40];
    z[19]=z[11]*z[19];
    z[19]= - n<T>(83,6)*z[80] + z[19];
    z[19]=z[11]*z[19];
    z[20]=z[80]*z[7];
    z[21]=z[87]*z[20];
    z[19]=z[19] + n<T>(13,6)*z[21];
    z[19]=z[5]*z[19];
    z[21]=z[12] + n<T>(2,3)*z[16];
    z[19]=z[19] - z[21];
    z[19]=z[7]*z[19];
    z[23]= - z[9]*z[102];
    z[23]=z[23] - z[2];
    z[24]=z[8]*z[7];
    z[25]=n<T>(1,3)*z[24];
    z[23]=z[25]*z[23];
    z[21]=n<T>(1,3)*z[2] - z[21];
    z[21]=z[7]*z[21];
    z[26]=n<T>(7,6)*z[24];
    z[21]= - z[26] + z[21];
    z[21]=z[4]*z[21];
    z[21]=z[21] + z[23];
    z[21]=z[9]*z[21];
    z[23]=z[31]*z[25];
    z[27]= - n<T>(7,3)*z[16] - z[61];
    z[27]=z[27]*z[47];
    z[27]=n<T>(7,6)*z[31] + z[27];
    z[27]=z[7]*z[27];
    z[30]=z[13]*z[24];
    z[30]=z[7] + n<T>(1,2)*z[30];
    z[27]=3*z[30] + z[27];
    z[27]=z[4]*z[27];
    z[19]=z[21] + z[27] + z[23] - z[26] + z[19];
    z[19]=z[9]*z[19];
    z[21]= - z[95]*z[28]*z[20];
    z[23]=n<T>(1,2)*z[7];
    z[26]= - 33*z[16] - n<T>(101,3)*z[12];
    z[26]=z[11]*z[26];
    z[26]=n<T>(11,3) + z[26];
    z[26]=z[26]*z[23];
    z[27]=3*z[7];
    z[28]=z[27]*z[13];
    z[30]= - n<T>(37,3) + z[28];
    z[30]=z[30]*z[53];
    z[20]= - z[20] + n<T>(7,3)*z[24];
    z[20]=z[20]*z[47];
    z[31]= - z[36] + z[40];
    z[31]=z[11]*z[31];
    z[31]= - n<T>(31,3)*z[80] + z[31];
    z[31]=z[7]*z[31]*z[87];
    z[31]=n<T>(199,12) + z[31];
    z[31]=z[5]*z[31];
    z[32]= - z[89] + 13*z[12] + n<T>(53,2)*z[16] + 32*z[14];
    z[25]= - z[6]*z[25];
    z[25]=n<T>(3,2)*z[7] + z[25];
    z[25]=z[6]*z[25];
    z[25]= - static_cast<T>(4)+ z[25];
    z[25]=z[2]*z[25];
    z[19]=z[19] + z[31] + z[21] + z[25] + z[20] + z[30] + z[26] + n<T>(1,3)*
    z[32] - n<T>(3,2)*z[15];
    z[19]=z[9]*z[19];
    z[20]=z[29]*z[24];
    z[21]=z[27]*z[3];
    z[20]=z[20] - z[21];
    z[21]= - n<T>(1,3) + z[71];
    z[21]=z[21]*z[24];
    z[21]=z[21] - z[20];
    z[21]=z[21]*z[47];
    z[25]= - n<T>(407,9) - z[28];
    z[25]=z[25]*z[53];
    z[26]= - z[13] - n<T>(3,2)*z[11];
    z[26]=z[3]*z[26];
    z[26]= - n<T>(5,3) + z[26];
    z[26]=z[7]*z[26];
    z[21]=z[21] + z[25] + z[26] + n<T>(2155,36)*z[3] + z[56] - n<T>(20,3)*z[12]
    - z[22] - z[41] - n<T>(19,2)*z[16];
    z[21]=z[6]*z[21];
    z[22]=z[93]*z[103];
    z[25]=z[11]*z[80];
    z[25]=n<T>(11,3) - 5*z[25];
    z[25]=z[11]*z[25];
    z[22]=z[22] + z[97] + z[25];
    z[22]=z[22]*z[23];
    z[23]= - z[80]*z[109]*z[27];
    z[23]=13*z[11] + z[23];
    z[20]= - z[24] - z[20];
    z[20]=z[20]*z[47];
    z[24]=static_cast<T>(1)- n<T>(3,2)*z[103];
    z[24]=z[7]*z[24];
    z[20]=z[24] + z[20];
    z[20]=z[6]*z[20];
    z[24]= - z[11] + z[112];
    z[24]=z[7]*z[24];
    z[24]=static_cast<T>(79)+ z[24];
    z[20]=n<T>(1,2)*z[24] + z[20];
    z[20]=z[6]*z[20];
    z[20]=n<T>(1,2)*z[23] + z[20];
    z[20]=z[5]*z[20];
    z[23]=5*z[10] - z[99] + z[50];
    z[23]=z[13]*z[23];
    z[24]=z[116] + n<T>(13,3)*z[16] - z[104];
    z[24]=z[11]*z[24];
    z[25]= - n<T>(313,4)*z[13] - 188*z[11];
    z[25]=z[3]*z[25];
    z[26]= - 11*z[11] + n<T>(69,4)*z[6];
    z[26]=z[2]*z[26];

    r += n<T>(77,18) + z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + 
      z[24] + n<T>(1,9)*z[25] + z[26] - n<T>(27,4)*z[58];
 
    return r;
}

template double qg_2lNLC_r195(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r195(const std::array<dd_real,31>&);
#endif
