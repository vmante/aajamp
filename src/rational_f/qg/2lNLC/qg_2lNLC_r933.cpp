#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r933(const std::array<T,31>& k) {
  T z[46];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[2];
    z[7]=k[9];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[6];
    z[11]=k[7];
    z[12]=k[13];
    z[13]=k[3];
    z[14]=5*z[5];
    z[15]=2*z[7];
    z[16]= - z[14] + z[15];
    z[16]=z[16]*z[15];
    z[17]=3*z[13];
    z[18]=npow(z[10],2);
    z[19]=z[18]*z[11];
    z[20]= - z[19]*z[17];
    z[21]=npow(z[12],2);
    z[22]=5*z[21];
    z[23]=z[11]*z[12];
    z[16]=z[20] + 13*z[23] + z[16] + z[22];
    z[16]=z[4]*z[16];
    z[20]=z[8]*z[12];
    z[24]=z[11]*z[8];
    z[25]=9*z[20] + 29*z[24];
    z[25]=z[25]*z[18];
    z[26]=npow(z[11],2);
    z[27]=z[8]*z[18];
    z[27]= - 12*z[27] - 9*z[5] - 20*z[11];
    z[27]=z[4]*z[27];
    z[25]=z[27] - 2*z[26] + z[25];
    z[25]=z[2]*z[25];
    z[26]=2*z[12];
    z[27]=9*z[4] - z[26] - 11*z[11];
    z[28]=4*z[2];
    z[27]=z[28]*z[27];
    z[29]=3*z[21];
    z[27]=z[27] + z[29];
    z[27]=z[18]*z[27];
    z[30]=z[22]*z[11];
    z[31]=4*z[5];
    z[32]=z[31]*npow(z[7],2);
    z[30]=z[30] + z[32];
    z[33]=24*z[19] - z[30];
    z[33]=z[4]*z[33];
    z[27]=z[33] + z[27];
    z[27]=z[9]*z[27];
    z[33]=z[21]*z[13];
    z[34]=z[21]*z[8];
    z[34]= - 6*z[33] - 11*z[34];
    z[34]=z[34]*z[18];
    z[16]=z[27] + z[25] + z[16] + z[34] - z[30];
    z[16]=z[9]*z[16];
    z[25]=z[5]*z[3];
    z[14]= - z[7]*z[14];
    z[14]=4*z[23] + z[29] + z[25] + z[14];
    z[14]=z[4]*z[14];
    z[27]=44*z[11];
    z[30]=z[2]*z[4];
    z[34]=z[30]*z[9];
    z[35]=npow(z[10],3);
    z[36]= - z[35]*z[34]*z[27];
    z[37]=z[29]*z[8];
    z[35]=z[35]*z[2];
    z[38]=z[35]*z[37];
    z[36]=z[38] + z[36];
    z[36]=z[9]*z[36];
    z[38]= - z[11]*z[29];
    z[32]= - z[32] + z[38];
    z[32]=z[4]*z[32];
    z[38]=npow(z[8],2);
    z[39]= - z[38]*z[22];
    z[40]=npow(z[24],2);
    z[40]=2*z[40];
    z[39]=z[39] - z[40];
    z[35]=z[39]*z[35];
    z[32]=z[36] + z[32] + z[35];
    z[32]=z[9]*z[32];
    z[35]=z[3]*z[6];
    z[36]=z[35] - 1;
    z[39]=4*z[7];
    z[41]= - z[36]*z[39];
    z[41]= - z[3] + z[41];
    z[41]=z[7]*z[41];
    z[42]=4*z[25];
    z[43]= - 4*z[11] + z[3] - 10*z[5];
    z[43]=z[4]*z[43];
    z[41]=z[43] - z[42] + z[41];
    z[41]=z[2]*z[41];
    z[42]= - z[1]*z[30]*z[42];
    z[14]=z[42] + z[32] + z[14] + z[41];
    z[14]=z[1]*z[14];
    z[32]=3*z[5];
    z[41]= - static_cast<T>(2)- z[35];
    z[41]=z[41]*z[32];
    z[42]=z[3]*npow(z[6],2);
    z[43]=z[6] - z[42];
    z[43]=z[43]*z[15];
    z[35]=z[35] + 1;
    z[43]=z[43] - z[35];
    z[43]=z[43]*z[15];
    z[44]=z[38]*z[11];
    z[38]=z[12]*z[38];
    z[38]=z[38] - 9*z[44];
    z[38]=z[38]*z[18];
    z[45]=z[8]*z[3];
    z[45]=static_cast<T>(11)+ z[45];
    z[45]=z[4]*z[45];
    z[38]=z[45] + z[38] - 14*z[11] - z[12] + z[43] + z[3] + z[41];
    z[38]=z[2]*z[38];
    z[22]=z[8]*z[22];
    z[22]=8*z[33] + z[22];
    z[22]=z[8]*z[22];
    z[29]=npow(z[13],2)*z[29];
    z[22]=z[40] + z[29] + z[22];
    z[18]=z[22]*z[18];
    z[22]=z[25] - 2*z[21];
    z[22]=z[8]*z[22];
    z[22]=z[22] - z[26] + z[15] + 2*z[3] + z[5];
    z[22]=z[4]*z[22];
    z[26]=z[35]*z[15];
    z[26]=z[26] - z[3] - z[31];
    z[15]=z[26]*z[15];
    z[26]=9*z[21];
    z[14]=z[14] + z[16] + z[38] + z[22] + z[18] + 7*z[23] + z[26] - 3*
    z[25] + z[15];
    z[14]=z[1]*z[14];
    z[15]= - static_cast<T>(37)+ 4*z[24];
    z[15]=z[11]*z[15];
    z[15]=30*z[4] + z[15] + z[37] - z[32] - 5*z[12];
    z[15]=z[2]*z[15];
    z[16]=z[39] - z[5];
    z[16]=z[16]*z[7];
    z[18]=z[16] + 18*z[23];
    z[18]=z[4]*z[18];
    z[22]= - z[32] - z[27];
    z[22]=z[22]*z[30];
    z[18]=z[18] + z[22];
    z[18]=z[9]*z[18];
    z[22]=2*z[5];
    z[27]=z[22] + 3*z[7];
    z[27]=9*z[11] + 2*z[27] - 7*z[12];
    z[27]=z[4]*z[27];
    z[15]=z[18] + z[15] + z[27] + 14*z[23] + z[16] + z[26];
    z[15]=z[9]*z[15];
    z[16]= - z[6] - 3*z[42];
    z[16]=z[5]*z[16];
    z[18]= - 2*z[6] - z[42];
    z[18]=z[7]*z[18];
    z[23]=3*z[8];
    z[23]= - z[4]*z[23];
    z[16]=z[23] + 19*z[24] - z[20] + z[18] - static_cast<T>(5)+ z[16];
    z[16]=z[2]*z[16];
    z[18]= - z[36]*z[22];
    z[23]=static_cast<T>(2)- z[20];
    z[23]=2*z[23] - z[24];
    z[23]=z[11]*z[23];
    z[18]=z[23] + z[3] + z[18];
    z[23]=z[6] + z[42];
    z[23]=z[23]*z[39];
    z[26]= - z[6]*z[32];
    z[23]=z[23] - 2*z[36] + z[26];
    z[23]=z[7]*z[23];
    z[21]=2*z[25] - 13*z[21];
    z[21]=z[8]*z[21];
    z[25]=z[12]*z[13];
    z[26]=static_cast<T>(1)- z[25];
    z[26]=z[12]*z[26];
    z[22]=z[22] + z[12];
    z[22]=z[8]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[4]*z[22];
    z[14]=z[14] + z[15] + z[16] + z[22] + z[21] + 9*z[26] + z[23] + 2*
    z[18];
    z[14]=z[1]*z[14];
    z[15]=5*z[11];
    z[16]=z[10]*z[24];
    z[16]= - z[15] + z[16];
    z[16]=z[10]*z[16];
    z[18]=z[4]*z[10];
    z[21]= - z[10]*z[8];
    z[21]=static_cast<T>(5)+ z[21];
    z[21]=z[21]*z[18];
    z[16]=z[16] + z[21];
    z[21]=2*z[2];
    z[16]=z[16]*z[21];
    z[22]= - z[34] + 2*z[4];
    z[22]=z[19]*z[22];
    z[15]= - z[15] + z[10];
    z[15]=z[15]*z[18];
    z[15]= - z[19] + z[15];
    z[15]=z[2]*z[15];
    z[15]=z[15] + z[22];
    z[22]=2*z[9];
    z[15]=z[15]*z[22];
    z[23]=2*z[10];
    z[26]=z[11]*z[13];
    z[27]= - static_cast<T>(2)- z[26];
    z[27]=z[27]*z[23];
    z[27]=13*z[11] + z[27];
    z[27]=z[27]*z[18];
    z[15]=z[15] + z[16] + 4*z[19] + z[27];
    z[15]=z[9]*z[15];
    z[16]=2*z[8];
    z[19]=z[16] + z[13];
    z[19]=z[19]*z[23];
    z[27]=static_cast<T>(13)- z[19];
    z[27]=z[10]*z[11]*z[27];
    z[18]=3*z[18];
    z[26]= - static_cast<T>(3)- z[26];
    z[26]=z[26]*z[18];
    z[29]= - z[10]*z[44];
    z[29]=5*z[24] + z[29];
    z[29]=z[10]*z[29];
    z[18]= - z[8]*z[18];
    z[18]=z[29] + z[18];
    z[18]=z[18]*z[21];
    z[15]=z[15] + z[18] + z[27] + z[26];
    z[15]=z[9]*z[15];
    z[18]=z[24] - 1;
    z[18]=z[18]*z[19];
    z[19]= - z[17] - 13*z[8];
    z[19]=z[11]*z[19];
    z[18]=z[18] + static_cast<T>(4)+ z[19];
    z[18]=z[10]*z[18];
    z[19]= - z[23]*z[44];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[19]*z[21];
    z[21]=z[12] + 3*z[11];
    z[15]=z[15] + z[19] - 6*z[4] + 4*z[21] + z[18];
    z[15]=z[15]*z[22];
    z[18]=z[17] + 4*z[8];
    z[18]=z[18]*z[24];
    z[16]=z[16] - z[6];
    z[19]= - z[44] + z[16];
    z[19]=z[23]*z[13]*z[19];
    z[18]=z[19] + z[18] - 8*z[8] - z[17] + 4*z[6];
    z[18]=z[18]*z[23];
    z[19]= - z[20] + static_cast<T>(3)- z[25];
    z[16]=z[16]*z[28];
    z[17]= - z[17] - 16*z[8];
    z[17]=z[11]*z[17];

    r += z[14] + z[15] + z[16] + z[17] + z[18] + 4*z[19];
 
    return r;
}

template double qg_2lNLC_r933(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r933(const std::array<dd_real,31>&);
#endif
