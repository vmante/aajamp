#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r773(const std::array<T,31>& k) {
  T z[71];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[8];
    z[8]=k[12];
    z[9]=k[5];
    z[10]=k[2];
    z[11]=k[14];
    z[12]=k[13];
    z[13]=k[9];
    z[14]=k[3];
    z[15]=npow(z[11],2);
    z[16]=2*z[15];
    z[17]=npow(z[9],2);
    z[18]=npow(z[3],4);
    z[19]=npow(z[10],2);
    z[20]= - z[18]*z[19]*z[17]*z[16];
    z[21]=z[7]*z[8];
    z[20]=z[20] + z[21];
    z[20]=z[6]*z[20];
    z[21]= - z[7] - 5*z[8];
    z[21]=z[7]*z[21];
    z[22]=z[7]*z[10];
    z[23]= - static_cast<T>(1)- z[22];
    z[23]=z[7]*z[23];
    z[23]=z[8] + z[23];
    z[23]=z[5]*z[23];
    z[21]=z[21] + z[23];
    z[23]=2*z[5];
    z[21]=z[21]*z[23];
    z[24]=npow(z[7],2);
    z[25]=z[24]*z[8];
    z[20]=z[20] - 3*z[25] + z[21];
    z[20]=z[6]*z[20];
    z[21]=npow(z[5],2);
    z[26]=z[19]*z[21];
    z[27]=z[21]*z[2];
    z[28]=2*z[21];
    z[29]=z[10]*z[28];
    z[27]=z[29] - z[27];
    z[27]=z[2]*z[27];
    z[27]= - z[26] + z[27];
    z[29]=npow(z[2],2);
    z[30]=npow(z[6],2);
    z[27]=z[29]*z[27]*z[30]*z[18];
    z[31]=z[24]*z[9];
    z[32]=z[18]*z[31];
    z[33]=z[6]*z[5];
    z[34]= - z[28] - 5*z[33];
    z[35]=z[18]*z[2];
    z[34]=z[34]*z[35];
    z[32]= - 2*z[32] + 5*z[34];
    z[34]=npow(z[2],3);
    z[32]=z[32]*z[34];
    z[36]=z[6]*z[9];
    z[37]=z[7]*z[9];
    z[38]=z[36] + z[37];
    z[39]=z[18]*z[38];
    z[40]=2*z[6];
    z[41]=z[5] + z[40];
    z[35]=z[41]*z[35];
    z[35]=3*z[39] + 4*z[35];
    z[34]=z[35]*z[34];
    z[34]=z[31] + z[34];
    z[34]=z[4]*z[34];
    z[35]=z[9]*z[8];
    z[39]= - static_cast<T>(2)+ z[35];
    z[39]=z[39]*z[24];
    z[32]=z[34] + z[39] + z[32];
    z[32]=z[4]*z[32];
    z[34]=npow(z[2],4);
    z[39]=z[21]*z[6];
    z[18]=z[18]*z[34]*z[39];
    z[41]=npow(z[12],2);
    z[42]= - z[12] - z[8];
    z[42]=z[7]*z[42];
    z[42]= - z[41] + z[42];
    z[42]=z[7]*z[42];
    z[43]=z[41]*z[5];
    z[18]=z[32] + 22*z[18] + z[42] + z[43];
    z[18]=z[4]*z[18];
    z[32]=2*z[8];
    z[42]= - z[32] - z[5];
    z[42]=z[33]*z[24]*z[42];
    z[44]=z[21]*z[25];
    z[45]=z[2]*z[3];
    z[45]=npow(z[4],3)*npow(z[45],6);
    z[25]= - z[25] + z[45];
    z[25]=z[1]*z[39]*z[25];
    z[25]=z[25] + z[44] + z[42];
    z[42]=7*z[33];
    z[44]=z[28] + z[42];
    z[44]=z[2]*z[44];
    z[44]=z[31] + z[44];
    z[45]=npow(z[3],5);
    z[34]=z[4]*z[34]*z[45]*z[44];
    z[44]=z[45]*npow(z[2],5)*z[39];
    z[34]= - 10*z[44] + z[34];
    z[34]=z[34]*npow(z[4],2);
    z[25]=z[34] + 2*z[25];
    z[25]=z[1]*z[25];
    z[34]=4*z[8];
    z[44]=z[12] + z[34];
    z[44]=z[7]*z[44];
    z[45]=z[8]*z[23];
    z[44]=z[45] + z[41] + z[44];
    z[45]=z[5]*z[7];
    z[44]=z[44]*z[45];
    z[18]=z[25] + z[18] + 2*z[27] + z[44] + z[20];
    z[18]=z[1]*z[18];
    z[20]=npow(z[3],3);
    z[25]=z[20]*z[31];
    z[27]=z[20]*z[9];
    z[44]= - z[30]*z[27];
    z[46]=29*z[5] - z[6];
    z[46]=z[6]*z[46];
    z[46]=22*z[21] + z[46];
    z[46]=z[2]*z[20]*z[46];
    z[25]=z[46] + z[25] + z[44];
    z[25]=z[2]*z[25];
    z[44]= - z[21] + z[33];
    z[44]=z[44]*z[40];
    z[46]=z[12]*z[24];
    z[44]=z[46] + z[44];
    z[25]=2*z[44] + z[25];
    z[25]=z[2]*z[25];
    z[44]=16*z[5];
    z[46]=3*z[7];
    z[47]= - 18*z[6] + z[46] - z[44];
    z[47]=z[2]*z[47];
    z[47]=z[47] - 2*z[37];
    z[47]=z[20]*z[47];
    z[48]=4*z[21];
    z[49]= - 3*z[27] + z[48];
    z[49]=z[6]*z[49];
    z[47]=z[49] + z[47];
    z[47]=z[2]*z[47];
    z[49]=z[5]*z[12];
    z[50]= - z[12] + z[46];
    z[50]=z[7]*z[50];
    z[50]=z[50] + z[49];
    z[47]=z[47] + 2*z[50] + 9*z[33];
    z[47]=z[2]*z[47];
    z[50]= - z[31] - z[40];
    z[39]=z[39]*z[2];
    z[42]= - z[42] - 2*z[39];
    z[42]=z[2]*z[42];
    z[42]=4*z[50] + z[42];
    z[42]=z[2]*z[42];
    z[42]= - 3*z[38] + z[42];
    z[42]=z[4]*z[42];
    z[50]=static_cast<T>(5)+ 3*z[35];
    z[50]=z[50]*z[37];
    z[50]=z[50] + static_cast<T>(4)- z[35];
    z[50]=z[7]*z[50];
    z[42]=z[42] + z[47] + z[50] + 5*z[6];
    z[42]=z[4]*z[42];
    z[47]=npow(z[9],3);
    z[50]=z[20]*z[47];
    z[50]=3*z[50] - 3;
    z[50]=z[8]*z[50];
    z[51]=2*z[7];
    z[52]= - static_cast<T>(3)- z[35];
    z[52]=z[52]*z[51];
    z[53]=4*z[12];
    z[50]=z[52] + z[53] + z[50];
    z[50]=z[7]*z[50];
    z[52]=5*z[11];
    z[54]=z[52] - z[53];
    z[54]=z[5]*z[54];
    z[55]= - z[5] + z[40];
    z[55]=z[55]*z[40];
    z[25]=z[42] + z[25] + z[55] + z[50] + z[54];
    z[25]=z[4]*z[25];
    z[42]=2*z[10];
    z[50]=npow(z[13],2);
    z[54]= - z[50]*z[42];
    z[55]=z[50]*z[19];
    z[55]=static_cast<T>(1)+ z[55];
    z[55]=z[55]*z[23];
    z[56]=4*z[7];
    z[57]=6*z[8];
    z[54]=z[55] + z[56] + z[54] + z[57] - z[53] + 9*z[13];
    z[45]=z[54]*z[45];
    z[54]=z[19]*z[20];
    z[55]=2*z[11];
    z[58]=z[54]*z[55];
    z[27]=z[10]*z[11]*z[27];
    z[27]=z[58] + z[27];
    z[58]=4*z[9];
    z[27]=z[27]*z[58];
    z[26]=z[51]*z[26];
    z[59]=static_cast<T>(1)+ 2*z[35];
    z[59]=z[59]*z[7];
    z[26]=z[26] + z[59] - z[8] + z[27];
    z[26]=z[6]*z[26];
    z[27]= - z[9]*z[54]*z[16];
    z[60]=z[23] + z[32] - 5*z[7];
    z[60]=z[60]*z[23];
    z[61]=z[8]*z[13];
    z[59]= - 26*z[8] - z[59];
    z[59]=z[7]*z[59];
    z[26]=z[26] + z[60] + z[59] - 9*z[61] + z[27];
    z[26]=z[6]*z[26];
    z[27]= - z[28] + z[33];
    z[27]=z[40]*z[54]*z[27];
    z[54]= - z[24]*z[49];
    z[59]=6*z[21] - z[33];
    z[59]=z[40]*z[10]*z[59];
    z[59]=z[59] - 15*z[39];
    z[20]=z[2]*z[20]*z[59];
    z[20]=z[20] + z[54] + z[27];
    z[27]=2*z[2];
    z[20]=z[20]*z[27];
    z[24]=z[24]*z[32];
    z[18]=z[18] + z[25] + z[20] + z[26] + z[24] + z[45];
    z[18]=z[1]*z[18];
    z[20]=npow(z[3],2);
    z[24]=z[19]*z[20];
    z[25]=static_cast<T>(1)- z[24];
    z[26]=z[46] - z[23];
    z[45]=z[19]*z[5];
    z[26]=z[26]*z[45];
    z[54]=z[35] + 1;
    z[59]=z[54]*z[9];
    z[60]=z[59] + z[10];
    z[60]=z[60]*z[7];
    z[62]=z[20]*z[10];
    z[63]=4*z[62];
    z[64]= - z[8] - z[63];
    z[64]=z[9]*z[64];
    z[25]=z[26] + z[60] + 6*z[25] + z[64];
    z[25]=z[6]*z[25];
    z[26]=z[11]*z[62];
    z[26]=z[26] + z[50] - 5*z[61];
    z[61]=z[50]*z[32];
    z[64]=5*z[20];
    z[65]= - z[11]*z[64];
    z[61]=z[61] + z[65];
    z[61]=z[9]*z[61];
    z[26]=2*z[26] + z[61];
    z[26]=z[9]*z[26];
    z[61]=z[7]*z[19];
    z[61]=z[10] + z[61];
    z[61]=z[5]*z[61];
    z[61]=z[61] - z[22] + static_cast<T>(6)+ 5*z[24];
    z[61]=z[61]*z[23];
    z[65]=4*z[11];
    z[24]= - z[65]*z[24];
    z[60]= - z[60] - static_cast<T>(16)- 25*z[35];
    z[60]=z[7]*z[60];
    z[24]=z[25] + z[61] + z[60] + z[26] + z[24] + z[57] - z[52] - 12*
    z[13];
    z[24]=z[6]*z[24];
    z[25]=2*z[20];
    z[26]=z[53] + z[8];
    z[26]=z[26]*z[37]*z[25];
    z[52]=7*z[12];
    z[60]=z[52] - z[51];
    z[60]=z[7]*z[60];
    z[61]=z[5]*z[62];
    z[60]=20*z[61] + z[16] + z[60];
    z[60]=z[5]*z[60];
    z[61]=3*z[5];
    z[63]= - z[63] - z[61];
    z[66]=4*z[5];
    z[63]=z[63]*z[66];
    z[67]=z[20]*z[9];
    z[62]=13*z[5] + 3*z[62] + z[67];
    z[62]=z[6]*z[62];
    z[62]=z[63] + z[62];
    z[62]=z[6]*z[62];
    z[63]=z[7]*z[12];
    z[68]= - 3*z[41] + z[63];
    z[68]=z[7]*z[68];
    z[69]=28*z[5];
    z[70]= - z[20]*z[69];
    z[68]=z[68] + z[70];
    z[68]=z[5]*z[68];
    z[40]=z[20]*z[40];
    z[64]= - z[5]*z[64];
    z[40]=z[64] + z[40];
    z[40]=z[6]*z[40];
    z[40]=z[68] + z[40];
    z[40]=z[2]*z[40];
    z[26]=z[40] + z[62] + z[26] + z[60];
    z[26]=z[2]*z[26];
    z[40]=z[41] - z[25];
    z[40]=3*z[40] - z[63];
    z[40]=z[7]*z[40];
    z[60]= - z[41] + 8*z[20];
    z[60]=z[60]*z[61];
    z[62]=z[20] + 8*z[21];
    z[62]=3*z[62] - 8*z[33];
    z[62]=z[6]*z[62];
    z[40]=z[62] + z[40] + z[60];
    z[40]=z[2]*z[40];
    z[60]=8*z[5];
    z[62]=z[60] - z[65] + z[52];
    z[62]=z[5]*z[62];
    z[52]=9*z[7] - z[52] - 3*z[67];
    z[52]=z[7]*z[52];
    z[63]= - z[67] + z[66];
    z[63]=4*z[63] - 9*z[6];
    z[63]=z[6]*z[63];
    z[40]=z[40] + z[63] + z[52] + z[62];
    z[40]=z[2]*z[40];
    z[52]=2*z[12];
    z[62]=z[52] - z[46];
    z[62]=z[62]*z[51];
    z[63]= - z[52] - z[61];
    z[63]=z[63]*z[23];
    z[62]= - 18*z[39] - 43*z[33] + z[63] - z[20] + z[62];
    z[62]=z[2]*z[62];
    z[63]=12*z[6] + 3*z[31] + z[23];
    z[64]=6*z[39] + 21*z[33];
    z[28]=z[28] + z[64];
    z[28]=z[2]*z[28];
    z[28]=2*z[63] + z[28];
    z[28]=z[2]*z[28];
    z[38]=9*z[38];
    z[28]=z[38] + z[28];
    z[28]=z[4]*z[28];
    z[63]=z[14]*z[20];
    z[63]=z[63] - z[67];
    z[68]= - static_cast<T>(5)- 9*z[37];
    z[68]=z[7]*z[68];
    z[28]=z[28] + z[62] - 28*z[6] - z[60] + 4*z[63] + z[68];
    z[28]=z[2]*z[28];
    z[62]=z[54]*z[17]*z[46];
    z[63]=2*z[9];
    z[68]= - static_cast<T>(2)- z[35];
    z[68]=z[68]*z[63];
    z[62]=z[68] + z[62];
    z[62]=z[7]*z[62];
    z[28]=z[62] - 3*z[36] + z[28];
    z[28]=z[4]*z[28];
    z[62]= - z[32] + z[67];
    z[62]=z[9]*z[62];
    z[67]=static_cast<T>(3)- z[35];
    z[67]=z[67]*z[37];
    z[62]=z[67] - static_cast<T>(5)+ 3*z[62];
    z[62]=z[7]*z[62];
    z[67]=6*z[11];
    z[68]=static_cast<T>(1)- z[36];
    z[68]=z[6]*z[68];
    z[28]=z[28] + z[40] + z[68] + z[66] + z[67] + z[62];
    z[28]=z[4]*z[28];
    z[40]= - z[52] + z[8];
    z[25]=z[40]*z[17]*z[25];
    z[40]=z[50]*z[10];
    z[25]= - z[51] + z[25] + z[34] - z[40];
    z[25]=z[7]*z[25];
    z[52]=z[13] + z[53];
    z[52]=z[8]*z[52];
    z[25]=z[52] + z[25];
    z[20]= - 2*z[50] - 3*z[20];
    z[20]=z[10]*z[20];
    z[20]=2*z[13] + z[20];
    z[20]=z[10]*z[20];
    z[52]=z[42]*z[13];
    z[62]= - static_cast<T>(1)+ z[52];
    z[62]=z[62]*z[22];
    z[20]=z[62] - static_cast<T>(2)+ z[20];
    z[20]=z[20]*z[23];
    z[62]=6*z[13] + z[40];
    z[62]=z[62]*z[42];
    z[62]=z[22] - static_cast<T>(13)+ z[62];
    z[62]=z[7]*z[62];
    z[68]=z[65] + z[13];
    z[20]=z[20] + 2*z[68] + z[62];
    z[20]=z[5]*z[20];
    z[18]=z[18] + z[28] + z[26] + z[24] + 2*z[25] + z[20];
    z[18]=z[1]*z[18];
    z[20]= - z[50]*z[54];
    z[24]=z[14]*z[50];
    z[24]=z[24] + z[53];
    z[24]=z[8]*z[24];
    z[20]=z[24] + z[20];
    z[20]=z[9]*z[20];
    z[16]=z[16] - z[50];
    z[16]=z[10]*z[16];
    z[24]=8*z[12];
    z[25]=z[13]*z[14];
    z[26]=static_cast<T>(1)+ z[25];
    z[26]=z[13]*z[26];
    z[16]=z[20] + z[16] - z[34] + z[24] - z[67] + z[26];
    z[20]=5*z[13];
    z[20]= - z[35]*z[20];
    z[26]= - z[55] - z[13];
    z[15]=z[10]*z[15];
    z[15]=z[20] + 4*z[15] + 5*z[26] + z[57];
    z[15]=z[9]*z[15];
    z[20]= - z[19]*z[61];
    z[26]=z[10]*z[65];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[9]*z[26];
    z[20]=z[20] - z[42] + z[26];
    z[20]=z[6]*z[20];
    z[26]= - z[66] + z[46];
    z[19]=z[19]*z[26];
    z[19]=28*z[10] + z[19];
    z[19]=z[5]*z[19];
    z[26]=10*z[11];
    z[28]= - z[26] - 7*z[13];
    z[28]=z[10]*z[28];
    z[34]= - 7*z[10] - 9*z[59];
    z[34]=z[7]*z[34];
    z[15]=z[20] + z[19] + z[34] + z[15] - static_cast<T>(3)+ z[28];
    z[15]=z[6]*z[15];
    z[19]= - 21*z[21] + 2*z[33];
    z[19]=z[6]*z[19];
    z[20]= - z[7]*z[41];
    z[19]=z[19] + z[20] + z[43];
    z[19]=z[19]*z[27];
    z[20]=z[55] - z[12];
    z[20]=3*z[20] - z[69];
    z[20]=z[5]*z[20];
    z[28]=3*z[12] - z[56];
    z[28]=z[7]*z[28];
    z[34]=6*z[6];
    z[35]= - 43*z[5] + z[34];
    z[35]=z[6]*z[35];
    z[19]=z[19] + z[35] + z[28] + z[20];
    z[19]=z[2]*z[19];
    z[20]=static_cast<T>(2)- z[37];
    z[20]=z[7]*z[20];
    z[20]= - 7*z[5] + z[55] + z[20];
    z[28]= - static_cast<T>(2)+ z[36];
    z[28]=z[6]*z[28];
    z[20]=2*z[20] + z[28];
    z[19]=2*z[20] + z[19];
    z[19]=z[2]*z[19];
    z[20]= - z[12] + z[7];
    z[20]=z[7]*z[20];
    z[28]=z[12] + z[60];
    z[28]=z[5]*z[28];
    z[20]=z[20] + z[28];
    z[20]=24*z[39] + 2*z[20] + 59*z[33];
    z[20]=z[2]*z[20];
    z[28]= - static_cast<T>(2)+ 7*z[37];
    z[28]=z[7]*z[28];
    z[20]=z[20] + 41*z[6] + z[28] + 24*z[5];
    z[20]=z[2]*z[20];
    z[28]=z[17]*z[7];
    z[35]=5*z[9] - z[28];
    z[35]=z[7]*z[35];
    z[20]=z[20] + 6*z[36] + static_cast<T>(1)+ z[35];
    z[20]=z[2]*z[20];
    z[31]= - z[34] - z[31] - z[23];
    z[34]= - z[48] - z[64];
    z[34]=z[2]*z[34];
    z[31]=4*z[31] + z[34];
    z[31]=z[2]*z[31];
    z[31]= - z[38] + z[31];
    z[29]=z[4]*z[31]*z[29];
    z[31]=z[7]*z[47];
    z[17]= - z[17] + z[31];
    z[17]=z[7]*z[54]*z[17];
    z[17]=z[29] + z[20] + z[58] + z[17];
    z[17]=z[4]*z[17];
    z[20]=7*z[11];
    z[29]=z[9]*z[20];
    z[28]= - z[9] + z[28];
    z[28]=z[7]*z[28];
    z[17]=z[17] + z[19] + 4*z[36] + z[28] - static_cast<T>(13)+ z[29];
    z[17]=z[4]*z[17];
    z[19]=static_cast<T>(2)+ z[25];
    z[19]=z[13]*z[19];
    z[19]= - z[40] + z[65] + z[19];
    z[19]=z[10]*z[19];
    z[19]=z[19] + static_cast<T>(5)- z[25];
    z[28]=z[10]*z[13];
    z[29]= - static_cast<T>(1)+ z[28];
    z[22]=z[29]*z[22];
    z[29]= - static_cast<T>(2)+ z[25];
    z[28]=z[29]*z[28];
    z[25]= - 2*z[25] + z[28];
    z[25]=z[10]*z[25];
    z[25]=z[14] + z[25];
    z[23]=z[25]*z[23];
    z[19]=z[23] + 2*z[19] + 9*z[22];
    z[19]=z[5]*z[19];
    z[22]= - z[46]*z[49];
    z[23]=36*z[21] - 13*z[33];
    z[23]=z[6]*z[23];
    z[21]=z[21]*z[30];
    z[25]=z[7]*z[43];
    z[21]=z[25] + z[21];
    z[21]=z[21]*z[27];
    z[21]=z[21] + z[22] + z[23];
    z[21]=z[2]*z[21];
    z[20]=10*z[5] - z[20] + z[51];
    z[20]=z[5]*z[20];
    z[22]= - z[5]*z[10];
    z[22]= - static_cast<T>(2)+ z[22];
    z[22]=z[22]*z[66];
    z[23]=8*z[10] - z[45];
    z[23]=z[5]*z[23];
    z[23]= - static_cast<T>(4)+ z[23];
    z[23]=z[6]*z[23];
    z[22]=z[22] + z[23];
    z[22]=z[6]*z[22];
    z[23]=z[8] + z[7];
    z[23]=z[7]*z[23];
    z[20]=z[22] + z[23] + z[20];
    z[20]=2*z[20] + z[21];
    z[20]=z[2]*z[20];
    z[21]=8*z[9];
    z[22]= - z[12] + z[8];
    z[22]=z[22]*z[21];
    z[22]=z[37] + z[22] - static_cast<T>(9)- z[52];
    z[22]=z[7]*z[22];
    z[15]=z[18] + z[17] + z[20] + z[15] + z[19] + 2*z[16] + z[22];
    z[15]=z[1]*z[15];
    z[16]=z[66] - z[26];
    z[16]=z[10]*z[16];
    z[17]= - z[32] + z[11] + z[24];
    z[17]=z[17]*z[63];
    z[18]=z[10] + z[9];
    z[18]=z[18]*z[51];
    z[19]=3*z[11] - z[53];
    z[19]=10*z[6] - z[44] + 2*z[19] + 7*z[7];
    z[19]=z[2]*z[19];
    z[20]= - z[14]*z[24];
    z[21]=z[10] + z[21];
    z[21]=z[6]*z[21];
    z[22]= - z[14] + z[2];
    z[22]=z[4]*z[22];

    r += static_cast<T>(1)+ z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + 4*
      z[22];
 
    return r;
}

template double qg_2lNLC_r773(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r773(const std::array<dd_real,31>&);
#endif
