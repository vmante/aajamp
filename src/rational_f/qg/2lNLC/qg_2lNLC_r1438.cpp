#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1438(const std::array<T,31>& k) {
  T z[103];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[8];
    z[5]=k[11];
    z[6]=k[13];
    z[7]=k[7];
    z[8]=k[12];
    z[9]=k[14];
    z[10]=k[4];
    z[11]=k[10];
    z[12]=k[15];
    z[13]=k[3];
    z[14]=k[2];
    z[15]=8*z[5];
    z[16]=2*z[4];
    z[17]= - z[16] + z[7];
    z[17]=3*z[17] - z[15];
    z[18]=4*z[5];
    z[17]=z[17]*z[18];
    z[19]=3*z[8];
    z[20]=2*z[12];
    z[21]= - z[20] - z[19];
    z[21]=z[9]*z[21];
    z[22]=npow(z[5],2);
    z[23]=z[22]*z[2];
    z[24]=z[23]*z[4];
    z[25]=28*z[24];
    z[26]=z[7]*z[8];
    z[27]=npow(z[3],2);
    z[17]=z[25] + z[21] + z[17] + 5*z[27] - 6*z[26];
    z[17]=z[2]*z[17];
    z[21]=9*z[8];
    z[28]=6*z[7];
    z[29]=z[27]*z[13];
    z[30]=4*z[4];
    z[17]=z[17] - 5*z[9] + z[15] - z[28] - z[30] - 12*z[29] + z[20] + 
    z[21];
    z[17]=z[2]*z[17];
    z[31]=n<T>(16,3)*z[11];
    z[29]=3*z[29] + z[31] + z[19];
    z[32]=2*z[5];
    z[33]=z[32] + z[8];
    z[33]=z[33]*z[32];
    z[34]=4*z[11];
    z[35]=5*z[4];
    z[36]= - z[34] - z[35];
    z[36]=z[9]*z[36];
    z[36]=z[36] + z[27] - z[33];
    z[36]=z[10]*z[36];
    z[37]= - z[27] + 8*z[22];
    z[38]=3*z[2];
    z[37]=z[37]*z[38];
    z[39]=9*z[4];
    z[29]=z[36] + z[37] + z[9] - 18*z[5] + 2*z[29] + z[39];
    z[29]=z[10]*z[29];
    z[36]=2*z[11];
    z[37]=3*z[4];
    z[40]=z[36] + z[37];
    z[41]=6*z[5];
    z[42]= - 4*z[23] + z[41] - z[40];
    z[42]=z[10]*z[42];
    z[43]=3*z[23];
    z[44]=z[43] + z[4] - z[32];
    z[44]=z[2]*z[44];
    z[45]=z[11]*z[13];
    z[44]= - n<T>(2,3)*z[45] + z[44];
    z[42]=4*z[44] + z[42];
    z[42]=z[10]*z[42];
    z[44]=z[5] + z[4];
    z[46]=npow(z[2],2);
    z[47]=z[44]*z[46];
    z[48]=2*z[2];
    z[49]=z[48]*z[22];
    z[50]=3*z[5];
    z[51]= - z[4] + z[50] - z[49];
    z[52]=z[2]*z[51];
    z[52]=static_cast<T>(1)+ z[52];
    z[52]=z[10]*z[52];
    z[52]=z[52] + 2*z[13] + z[47];
    z[52]=z[10]*z[52];
    z[53]= - z[5]*z[4];
    z[53]=z[53] + z[24];
    z[54]=npow(z[2],4);
    z[53]=z[53]*z[54];
    z[55]=npow(z[13],2);
    z[53]= - z[55] + z[53];
    z[52]=2*z[53] + z[52];
    z[53]=2*z[6];
    z[52]=z[52]*z[53];
    z[56]=z[37]*z[23];
    z[57]= - z[37] - z[5];
    z[57]=z[5]*z[57];
    z[56]=z[57] + z[56];
    z[56]=z[56]*z[48];
    z[56]=z[56] - z[4] + z[5];
    z[56]=z[2]*z[56];
    z[56]= - n<T>(7,3) + z[56];
    z[56]=z[2]*z[56];
    z[57]=z[55]*z[11];
    z[58]= - 4*z[13] - z[57];
    z[56]=n<T>(1,3)*z[58] + z[56];
    z[42]=z[52] + 2*z[56] + z[42];
    z[42]=z[42]*z[53];
    z[52]=z[55]*z[27];
    z[56]=z[45] - 1;
    z[52]=n<T>(2,3)*z[56] + z[52];
    z[17]=z[42] + z[29] + 7*z[52] + z[17];
    z[17]=z[6]*z[17];
    z[29]=n<T>(1,3)*z[11];
    z[42]=z[27]*z[29];
    z[52]=z[27]*z[4];
    z[42]=z[42] + z[52];
    z[58]=n<T>(7,3)*z[11];
    z[59]=z[58] - z[16];
    z[60]=npow(z[9],2);
    z[61]= - z[59]*z[60];
    z[42]=2*z[42] + z[61];
    z[61]=2*z[10];
    z[42]=z[42]*z[61];
    z[62]=n<T>(4,3)*z[27];
    z[63]=z[62]*z[45];
    z[64]= - z[8] + z[41];
    z[64]=z[5]*z[64];
    z[64]=z[63] + z[64];
    z[65]= - z[27]*z[32];
    z[65]= - z[52] + z[65];
    z[65]=z[65]*z[38];
    z[66]=8*z[9] + n<T>(34,3)*z[11] + z[37];
    z[66]=z[9]*z[66];
    z[42]=z[42] + z[65] + 2*z[64] + z[66];
    z[42]=z[10]*z[42];
    z[62]=z[57]*z[62];
    z[64]=n<T>(11,3)*z[11];
    z[62]=z[62] + z[8] - z[20] - z[64];
    z[65]=z[18]*z[4];
    z[66]=z[27] + z[65];
    z[66]=z[5]*z[66];
    z[67]=z[8] + z[12];
    z[68]=2*z[67];
    z[69]= - z[60]*z[68];
    z[66]=z[69] + z[52] + z[66];
    z[69]=4*z[2];
    z[66]=z[66]*z[69];
    z[70]=3*z[7];
    z[71]= - z[4] + z[70];
    z[71]=2*z[71] - 7*z[5];
    z[71]=z[71]*z[32];
    z[72]=3*z[26];
    z[71]= - z[72] + z[71];
    z[73]= - n<T>(40,3)*z[9] + 12*z[12] + z[8];
    z[73]=z[9]*z[73];
    z[66]=z[66] + 2*z[71] + z[73];
    z[66]=z[2]*z[66];
    z[71]=n<T>(1,3)*z[9];
    z[73]=2*z[7];
    z[17]=z[17] + z[42] + z[66] - z[71] + z[32] - z[73] + 2*z[62] - 
    z[35];
    z[17]=z[6]*z[17];
    z[42]=npow(z[3],3);
    z[62]=z[48]*z[42];
    z[66]=z[58] + z[37];
    z[74]=2*z[9];
    z[66]=z[66]*z[74];
    z[75]=z[42]*z[13];
    z[33]= - z[62] + z[66] + z[75] + z[33];
    z[33]=z[10]*z[33];
    z[66]=2*z[75] - 3*z[22];
    z[76]=z[42]*z[2];
    z[66]=2*z[66] - z[76];
    z[66]=z[66]*z[48];
    z[77]=z[49] + z[16] - z[5];
    z[77]=z[10]*z[77];
    z[78]= - z[2]*z[44];
    z[77]=z[78] + z[77];
    z[77]=z[77]*z[53];
    z[78]=8*z[4];
    z[79]=n<T>(7,3)*z[9];
    z[80]=5*z[8];
    z[31]=z[77] + z[33] + z[66] - z[79] - z[18] - z[78] - z[31] + z[80];
    z[31]=z[10]*z[31];
    z[33]=4*z[7];
    z[66]= - z[23]*z[33];
    z[77]=2*z[26];
    z[81]=n<T>(2,3)*z[12] + z[19];
    z[81]=z[9]*z[81];
    z[66]=z[66] + z[81] + 4*z[22] - 5*z[75] + z[77];
    z[66]=z[2]*z[66];
    z[75]=z[80] - z[32];
    z[80]=z[55]*z[42];
    z[81]=n<T>(11,3)*z[9];
    z[66]=z[66] + z[81] + z[16] + 6*z[80] - z[75];
    z[66]=z[2]*z[66];
    z[80]=npow(z[13],3);
    z[82]= - z[80]*z[42];
    z[83]=static_cast<T>(5)- 2*z[45];
    z[31]=z[66] + n<T>(2,3)*z[83] + z[82] + z[31];
    z[31]=z[31]*z[53];
    z[66]=n<T>(20,3)*z[11] + z[37];
    z[66]=z[66]*z[60];
    z[82]=10*z[5];
    z[83]=z[82]*z[42];
    z[84]=z[42]*z[4];
    z[85]= - z[84] + z[83];
    z[85]=z[2]*z[85];
    z[86]=z[22]*z[8];
    z[87]=4*z[86];
    z[66]=z[85] - z[87] + z[66];
    z[66]=z[10]*z[66];
    z[85]=z[8] - z[18];
    z[85]=z[85]*z[41];
    z[83]=7*z[84] - z[83];
    z[83]=z[83]*z[46];
    z[88]= - n<T>(73,3)*z[9] - n<T>(64,3)*z[11] - 21*z[4];
    z[88]=z[9]*z[88];
    z[66]=z[66] + z[83] + z[85] + z[88];
    z[66]=z[10]*z[66];
    z[83]=19*z[8];
    z[85]=n<T>(20,3)*z[12] + z[83];
    z[85]=z[85]*z[60];
    z[88]=z[5]*z[42];
    z[88]=z[84] + z[88];
    z[88]=z[2]*z[88];
    z[89]=z[22]*z[7];
    z[85]=22*z[88] - 24*z[89] + z[85];
    z[85]=z[2]*z[85];
    z[88]= - z[70] + z[15];
    z[88]=z[88]*z[32];
    z[88]=7*z[26] + z[88];
    z[90]=4*z[12];
    z[91]=n<T>(77,3)*z[9] - z[90] - 17*z[8];
    z[91]=z[9]*z[91];
    z[85]=z[85] + 2*z[88] + z[91];
    z[85]=z[2]*z[85];
    z[31]=z[31] + z[66] + z[85] + n<T>(29,3)*z[9] + 24*z[5] + z[78] + n<T>(22,3)
   *z[11] - 25*z[8];
    z[31]=z[6]*z[31];
    z[66]= - 8*z[67] + n<T>(31,3)*z[9];
    z[66]=z[66]*z[60];
    z[67]= - z[42]*z[32];
    z[67]= - 15*z[84] + z[67];
    z[67]=z[2]*z[5]*z[67];
    z[78]=npow(z[9],3);
    z[84]=z[78]*z[12];
    z[67]=n<T>(31,3)*z[84] + z[67];
    z[67]=z[2]*z[67];
    z[66]=z[67] - 14*z[89] + z[66];
    z[66]=z[66]*z[48];
    z[67]= - n<T>(19,3)*z[9] - z[64] - z[37];
    z[67]=z[67]*z[60];
    z[85]=z[78]*z[11];
    z[88]=z[10]*z[85];
    z[67]=n<T>(4,3)*z[88] + 6*z[86] + z[67];
    z[67]=z[67]*z[61];
    z[88]=14*z[5];
    z[91]=z[88] - 13*z[8] - 14*z[7];
    z[91]=z[5]*z[91];
    z[91]=14*z[26] + z[91];
    z[92]=n<T>(13,3)*z[11];
    z[93]=z[20] + z[92];
    z[93]=n<T>(35,3)*z[9] + 2*z[93] + z[39];
    z[93]=z[9]*z[93];
    z[31]=z[31] + z[67] + z[66] + 2*z[91] + z[93];
    z[31]=z[6]*z[31];
    z[34]= - z[34] - z[37];
    z[34]=z[34]*z[60];
    z[66]=npow(z[3],4);
    z[67]=z[66]*z[88];
    z[91]=z[66]*z[4];
    z[67]=z[91] + z[67];
    z[67]=z[67]*z[46];
    z[34]=z[67] + z[87] + z[34];
    z[34]=z[10]*z[34];
    z[67]=4*z[8];
    z[87]= - z[67] + z[50];
    z[87]=z[87]*z[18];
    z[93]=n<T>(25,3)*z[9];
    z[40]=4*z[40] + z[93];
    z[40]=z[9]*z[40];
    z[94]=npow(z[2],3);
    z[95]=z[94]*z[91];
    z[34]=z[34] + 8*z[95] + z[87] + z[40];
    z[34]=z[10]*z[34];
    z[40]=z[55]*z[66];
    z[87]=z[66]*z[2];
    z[95]=z[13]*z[66];
    z[95]=z[95] - z[87];
    z[95]=z[95]*z[38];
    z[96]=z[8] - z[32];
    z[96]=z[5]*z[96];
    z[97]= - z[9]*z[37];
    z[95]=z[95] + z[97] - z[40] + z[96];
    z[95]=z[10]*z[95];
    z[96]=2*z[66];
    z[97]= - z[80]*z[96];
    z[98]=z[13]*z[87];
    z[98]=z[40] + z[98];
    z[98]=z[98]*z[38];
    z[99]=z[9] - z[8];
    z[95]=z[95] + z[98] + z[97] + z[44] + z[99];
    z[95]=z[10]*z[95];
    z[97]= - z[9]*z[8];
    z[40]=z[40] + z[97];
    z[40]=z[2]*z[40];
    z[40]=z[40] - z[99];
    z[40]=z[2]*z[40];
    z[97]= - npow(z[13],4)*z[66];
    z[40]=z[95] + z[97] + z[40];
    z[40]=z[40]*z[53];
    z[95]=z[22]*z[33];
    z[97]=n<T>(4,3)*z[12];
    z[98]=7*z[8];
    z[100]= - z[97] - z[98];
    z[100]=z[100]*z[60];
    z[101]=z[5]*z[66];
    z[101]=z[91] + z[101];
    z[101]=z[101]*z[46];
    z[95]=18*z[101] + z[95] + z[100];
    z[95]=z[2]*z[95];
    z[100]=z[7] - z[5];
    z[100]=z[5]*z[100];
    z[100]= - z[26] + z[100];
    z[93]=12*z[8] - z[93];
    z[93]=z[9]*z[93];
    z[93]=z[95] + 4*z[100] + z[93];
    z[93]=z[2]*z[93];
    z[95]=n<T>(2,3)*z[9];
    z[100]= - z[29] + z[8];
    z[50]= - z[95] - z[50] + 2*z[100] - z[4];
    z[34]=z[40] + z[34] + 2*z[50] + z[93];
    z[34]=z[34]*z[53];
    z[40]=n<T>(5,3)*z[11];
    z[50]= - z[78]*z[40];
    z[93]=z[96]*z[22];
    z[96]= - z[46]*z[93];
    z[50]=z[50] + z[96];
    z[50]=z[10]*z[50];
    z[96]=n<T>(8,3)*z[11];
    z[100]=z[96] + z[37];
    z[101]=n<T>(17,3)*z[9];
    z[100]=2*z[100] + z[101];
    z[100]=z[100]*z[60];
    z[93]= - z[94]*z[93];
    z[50]=z[50] + z[93] - 12*z[86] + z[100];
    z[50]=z[50]*z[61];
    z[93]= - z[101] + z[20] + z[83];
    z[93]=z[93]*z[60];
    z[100]= - z[66]*z[41];
    z[91]= - 35*z[91] + z[100];
    z[91]=z[5]*z[91]*z[46];
    z[91]= - n<T>(17,3)*z[84] + z[91];
    z[91]=z[2]*z[91];
    z[89]=z[91] + 12*z[89] + z[93];
    z[89]=z[89]*z[48];
    z[15]= - z[15] + 14*z[8] + 9*z[7];
    z[15]=z[5]*z[15];
    z[15]= - 9*z[26] + z[15];
    z[91]= - 6*z[4] - z[64] - z[98];
    z[79]=2*z[91] - z[79];
    z[79]=z[9]*z[79];
    z[15]=z[34] + z[50] + z[89] + 4*z[15] + z[79];
    z[15]=z[6]*z[15];
    z[34]=z[22]*z[4];
    z[50]=8*z[34];
    z[79]=z[66]*z[54]*z[50];
    z[89]=z[7] + z[8];
    z[89]=z[89]*z[5];
    z[77]= - z[77] + z[89];
    z[77]=z[77]*z[88];
    z[91]= - z[67] - z[37];
    z[91]=z[91]*z[60];
    z[77]=z[79] + z[77] + z[91];
    z[15]=2*z[77] + z[15];
    z[15]=z[6]*z[15];
    z[77]=npow(z[3],5);
    z[79]=z[94]*z[77];
    z[91]=z[39] + z[32];
    z[91]=z[91]*z[5];
    z[93]= - z[91]*z[79];
    z[84]=n<T>(1,3)*z[84] + z[93];
    z[84]=z[84]*z[48];
    z[93]= - z[22]*z[73];
    z[95]= - z[98] + z[95];
    z[95]=z[95]*z[60];
    z[84]=z[84] + z[93] + z[95];
    z[84]=z[84]*z[48];
    z[93]= - z[54]*z[77]*z[22];
    z[95]=z[78]*z[29];
    z[79]= - z[22]*z[79];
    z[79]=z[95] + z[79];
    z[79]=z[10]*z[79];
    z[95]=n<T>(2,3)*z[11];
    z[71]= - z[71] - z[95] - z[4];
    z[71]=z[71]*z[60];
    z[71]=z[79] + z[93] + 3*z[86] + z[71];
    z[71]=z[10]*z[71];
    z[79]=2*z[8];
    z[86]= - z[22]*z[79];
    z[93]=z[77]*z[5];
    z[100]=z[77]*z[4];
    z[101]=z[100] + 7*z[93];
    z[101]=z[101]*z[94];
    z[102]=z[4]*z[60];
    z[86]=z[101] + z[86] + z[102];
    z[86]=z[10]*z[86];
    z[93]=z[93] + z[100];
    z[100]=z[93]*z[54];
    z[101]=z[5]*z[8];
    z[102]= - z[4] - z[9];
    z[102]=z[9]*z[102];
    z[86]=z[86] + 3*z[100] + z[101] + z[102];
    z[86]=z[10]*z[86];
    z[93]=z[93]*z[94];
    z[100]=z[60]*z[8];
    z[93]=z[100] + 5*z[93];
    z[93]=z[2]*z[93];
    z[19]= - z[19] + z[9];
    z[19]=z[9]*z[19];
    z[19]=z[19] + z[93];
    z[19]=z[2]*z[19];
    z[19]=z[86] + z[19] - z[99];
    z[19]=z[19]*z[53];
    z[21]= - z[81] + z[16] + n<T>(4,3)*z[11] + z[21];
    z[21]=z[9]*z[21];
    z[75]= - z[33] - z[75];
    z[75]=z[5]*z[75];
    z[81]=4*z[26];
    z[75]=z[81] + z[75];
    z[19]=z[19] + 4*z[71] + z[84] + 2*z[75] + z[21];
    z[19]=z[19]*z[53];
    z[21]=npow(z[2],5);
    z[71]=z[77]*z[21]*z[34];
    z[67]= - z[67] - z[70];
    z[67]=z[67]*z[32];
    z[67]=15*z[26] + z[67];
    z[67]=z[67]*z[18];
    z[75]=z[83] + z[30];
    z[75]=z[75]*z[60];
    z[19]=z[19] + 28*z[71] + z[67] + z[75];
    z[19]=z[6]*z[19];
    z[67]=z[26]*z[22];
    z[19]=28*z[67] + z[19];
    z[19]=z[6]*z[19];
    z[54]= - z[10]*z[54];
    z[54]= - z[21] + z[54];
    z[71]=npow(z[3],6);
    z[22]=z[61]*z[71]*z[22]*z[54];
    z[54]=7*z[4];
    z[75]= - z[54] - z[32];
    z[21]=z[5]*z[21]*z[71]*z[75];
    z[75]=z[60]*z[79];
    z[21]=z[75] + z[21];
    z[21]=z[2]*z[21];
    z[75]= - z[79] + z[9];
    z[75]=z[9]*z[75];
    z[21]=z[22] + z[75] + z[21];
    z[21]=z[21]*z[53];
    z[22]= - z[72] + z[89];
    z[22]=z[22]*z[18];
    z[75]= - z[60]*z[98];
    z[71]=z[71]*npow(z[2],6)*z[34];
    z[21]=z[21] + 12*z[71] + z[22] + z[75];
    z[21]=z[6]*z[21];
    z[22]=12*z[67];
    z[21]= - z[22] + z[21];
    z[21]=z[21]*npow(z[6],2);
    z[71]=z[2]*z[3];
    z[71]=z[34]*npow(z[71],7);
    z[71]=z[100] + 2*z[71];
    z[71]=z[6]*z[71];
    z[67]=2*z[67] + z[71];
    z[71]=2*z[1];
    z[67]=z[67]*npow(z[6],3)*z[71];
    z[21]=z[21] + z[67];
    z[21]=z[21]*z[71];
    z[67]=npow(z[7],2);
    z[71]=z[67]*z[60];
    z[75]=z[37]*z[71];
    z[83]=npow(z[7],3);
    z[84]=z[11]*z[12];
    z[86]=z[83]*z[84];
    z[77]=z[77]*npow(z[10],5)*z[86];
    z[19]=z[21] + z[19] + z[75] + n<T>(4,3)*z[77];
    z[19]=z[1]*z[19];
    z[21]= - z[66]*z[83]*z[45];
    z[75]=z[83]*z[12];
    z[77]=z[75]*z[87];
    z[21]=z[21] + z[77];
    z[77]=z[20] + z[11];
    z[77]=z[77]*z[73];
    z[77]= - 7*z[84] + z[77];
    z[66]=z[10]*z[67]*z[66]*z[77];
    z[21]=2*z[21] + z[66];
    z[21]=z[10]*z[21];
    z[66]=3*z[67];
    z[77]= - z[85]*z[66];
    z[21]=z[77] + n<T>(1,3)*z[21];
    z[21]=z[10]*z[21];
    z[77]= - z[4] - z[74];
    z[71]=z[77]*z[71];
    z[21]=3*z[71] + z[21];
    z[21]=z[21]*z[61];
    z[39]= - z[67]*z[39];
    z[71]=z[16] - z[70];
    z[77]=z[9]*z[7];
    z[71]=z[71]*z[77];
    z[39]=z[39] + z[71];
    z[39]=z[9]*z[39];
    z[71]=6*z[2];
    z[78]= - z[67]*z[78]*z[71];
    z[15]=z[19] + z[15] + z[21] + z[78] - z[22] + z[39];
    z[15]=z[1]*z[15];
    z[19]=z[42]*z[45];
    z[21]=z[7]*z[84];
    z[19]=z[19] - n<T>(1,3)*z[21];
    z[21]=z[20] + z[37];
    z[22]= - z[21]*z[76];
    z[19]=z[22] + 4*z[19];
    z[19]=z[67]*z[19];
    z[22]=6*z[11] + z[4];
    z[22]=z[22]*z[66];
    z[39]= - z[77]*z[96];
    z[22]=z[22] + z[39];
    z[22]=z[22]*z[60];
    z[39]=7*z[11];
    z[76]=z[33] - 10*z[12] - z[39];
    z[76]=z[7]*z[76];
    z[76]=13*z[84] + z[76];
    z[78]=n<T>(2,3)*z[7];
    z[42]=z[10]*z[78]*z[42]*z[76];
    z[19]=z[42] + z[22] + z[19];
    z[19]=z[10]*z[19];
    z[22]= - z[7]*z[12];
    z[22]=z[84] + z[22];
    z[22]=z[22]*z[67];
    z[42]= - z[29] + z[4];
    z[42]=2*z[42] + 27*z[7];
    z[42]=z[7]*z[42];
    z[42]=z[42] - n<T>(70,3)*z[77];
    z[42]=z[9]*z[42];
    z[76]=z[4]*z[67];
    z[42]=18*z[76] + z[42];
    z[42]=z[9]*z[42];
    z[19]=z[19] + n<T>(8,3)*z[22] + z[42];
    z[19]=z[10]*z[19];
    z[22]=z[97] + z[37];
    z[22]=z[7]*z[22];
    z[22]= - n<T>(4,3)*z[84] + z[22];
    z[22]=z[7]*z[22];
    z[42]=z[8]*z[13];
    z[76]= - static_cast<T>(3)+ z[42];
    z[76]=z[7]*z[76];
    z[76]= - z[79] + z[76];
    z[76]=z[5]*z[76];
    z[76]=z[81] + z[76];
    z[76]=z[76]*z[32];
    z[22]=z[22] + z[76];
    z[28]=z[28] - z[96] - z[54];
    z[28]=z[7]*z[28];
    z[76]=z[8] + z[4];
    z[76]=2*z[76] + n<T>(19,3)*z[7];
    z[76]=z[9]*z[76];
    z[28]=z[28] + z[76];
    z[28]=z[9]*z[28];
    z[76]=z[20]*z[14];
    z[81]= - z[60]*z[76];
    z[81]=z[81] + z[24];
    z[62]=z[81]*z[62];
    z[66]=z[66] - n<T>(31,3)*z[77];
    z[66]=z[66]*z[60];
    z[62]=z[62] - n<T>(2,3)*z[75] + z[66];
    z[62]=z[62]*z[48];
    z[15]=z[15] + z[31] + z[19] + z[62] + 2*z[22] + z[28];
    z[15]=z[1]*z[15];
    z[19]= - z[16] + z[90] + z[64];
    z[19]=z[27]*z[19];
    z[22]=z[20] + z[29];
    z[22]=z[22]*z[33];
    z[28]=10*z[84];
    z[22]=z[22] - z[28] - 3*z[27];
    z[22]=z[7]*z[22];
    z[19]=2*z[19] + z[22];
    z[19]=z[7]*z[19];
    z[22]= - z[36] - z[4];
    z[22]=z[22]*z[67];
    z[31]=z[59]*z[77];
    z[22]=9*z[22] + 2*z[31];
    z[22]=z[9]*z[22];
    z[31]=z[84]*z[27];
    z[33]=z[86]*z[10];
    z[19]=4*z[33] + z[22] - 14*z[31] + z[19];
    z[19]=z[10]*z[19];
    z[22]=3*z[12];
    z[59]=z[78] - z[37] - z[22] - z[95];
    z[59]=z[7]*z[59];
    z[62]=3*z[84];
    z[59]=z[59] + z[62] - z[63];
    z[59]=z[59]*z[73];
    z[63]=5*z[14];
    z[31]=z[31]*z[63];
    z[31]=z[31] + z[59];
    z[59]=z[27]*z[21];
    z[64]=z[67]*z[90];
    z[59]=z[64] + z[59];
    z[59]=z[2]*z[7]*z[59];
    z[64]= - 18*z[7] + z[95] - z[4];
    z[64]=z[7]*z[64];
    z[64]=z[64] + 32*z[77];
    z[64]=z[9]*z[64];
    z[19]=z[19] + z[59] + 2*z[31] + z[64];
    z[19]=z[10]*z[19];
    z[31]= - 18*z[9] + n<T>(29,3)*z[7];
    z[59]=z[22] + z[79] + z[31];
    z[59]=z[59]*z[74];
    z[64]=z[12]*z[14];
    z[66]=8*z[64];
    z[81]=z[27]*z[66];
    z[81]=z[81] - z[26];
    z[59]=3*z[81] + z[59];
    z[59]=z[9]*z[59];
    z[27]=z[27]*z[20];
    z[22]= - z[60]*z[22];
    z[22]=z[27] + z[22];
    z[22]=z[9]*z[22];
    z[27]= - z[5]*z[52];
    z[22]=z[27] + z[22];
    z[22]=z[22]*z[69];
    z[21]=z[21]*z[67];
    z[22]=z[22] + z[59] - z[21] + 4*z[34];
    z[22]=z[2]*z[22];
    z[27]=static_cast<T>(2)- z[42];
    z[27]=z[27]*z[70];
    z[27]= - z[41] + z[79] + z[27];
    z[27]=z[27]*z[32];
    z[41]=z[8] - z[12];
    z[42]=z[11] - z[41];
    z[35]=4*z[42] + z[35];
    z[35]=z[7]*z[35];
    z[42]=5*z[12] - 3*z[11];
    z[31]=2*z[42] - z[31];
    z[31]=z[9]*z[31];
    z[15]=z[15] + z[17] + z[19] + z[22] + z[31] + z[27] - z[28] + z[35];
    z[15]=z[1]*z[15];
    z[17]= - 19*z[4] - z[18];
    z[17]=z[5]*z[17];
    z[17]=z[26] + z[17];
    z[18]=z[20] + z[8];
    z[18]=z[9]*z[18];
    z[17]=32*z[24] + 2*z[17] + z[18];
    z[17]=z[2]*z[17];
    z[17]=z[17] + n<T>(5,3)*z[9] - z[32] - z[73] + 2*z[41] + z[54];
    z[17]=z[2]*z[17];
    z[18]=z[40] - z[30];
    z[19]=z[18]*z[61];
    z[19]=z[19] + 1;
    z[19]=z[9]*z[19];
    z[22]= - z[58] - z[41];
    z[19]= - 12*z[23] + z[82] - z[73] + 2*z[22] + z[4] + z[19];
    z[19]=z[10]*z[19];
    z[22]=z[23]*z[30];
    z[22]= - z[91] + z[22];
    z[22]=z[2]*z[22];
    z[22]=2*z[44] + z[22];
    z[22]=z[22]*z[94];
    z[22]= - z[55] + z[22];
    z[22]=z[2]*z[22];
    z[24]= - 7*z[13] - z[47];
    z[24]=z[2]*z[24];
    z[26]=5*z[5];
    z[27]=z[26] - z[49];
    z[28]= - z[10]*z[27]*z[46];
    z[24]=z[28] + 3*z[55] + z[24];
    z[24]=z[10]*z[24];
    z[22]=z[24] + 3*z[80] + z[22];
    z[22]=z[22]*z[53];
    z[24]=z[48]*z[34];
    z[28]= - z[30] - z[5];
    z[28]=z[5]*z[28];
    z[24]=z[28] + z[24];
    z[24]=z[24]*z[71];
    z[24]=z[24] + z[54] + z[26];
    z[24]=z[2]*z[24];
    z[24]=static_cast<T>(2)+ z[24];
    z[24]=z[24]*z[48];
    z[24]=n<T>(1,3)*z[13] + z[24];
    z[24]=z[2]*z[24];
    z[26]=z[26] - z[43];
    z[26]=z[26]*z[69];
    z[26]= - n<T>(17,3) + z[26];
    z[26]=z[2]*z[26];
    z[27]= - z[2]*z[27];
    z[27]= - n<T>(1,3)*z[56] + z[27];
    z[27]=z[27]*z[61];
    z[26]=z[27] + z[26] - n<T>(4,3)*z[57] - 3*z[13];
    z[26]=z[10]*z[26];
    z[27]= - z[80]*z[36];
    z[27]=7*z[55] + z[27];
    z[22]=z[22] + z[26] + n<T>(1,3)*z[27] + z[24];
    z[22]=z[22]*z[53];
    z[23]= - 8*z[23] - z[4] + z[88];
    z[23]=z[23]*z[38];
    z[24]=z[29] - z[51];
    z[24]=z[24]*z[61];
    z[23]=z[24] + z[23] - static_cast<T>(3)+ n<T>(10,3)*z[45];
    z[23]=z[10]*z[23];
    z[24]= - 47*z[4] - z[82];
    z[24]=z[5]*z[24];
    z[24]=z[24] + z[25];
    z[24]=z[2]*z[24];
    z[16]=z[16] + z[5];
    z[16]=6*z[16] + z[24];
    z[16]=z[16]*z[48];
    z[16]=static_cast<T>(9)+ z[16];
    z[16]=z[2]*z[16];
    z[24]= - 11*z[13] + 8*z[57];
    z[16]=z[22] + z[23] + n<T>(1,3)*z[24] + z[16];
    z[16]=z[6]*z[16];
    z[22]=static_cast<T>(1)- 10*z[45];
    z[16]=z[16] + z[19] + n<T>(1,3)*z[22] + z[17];
    z[16]=z[6]*z[16];
    z[17]=z[20] + z[40];
    z[19]= - static_cast<T>(4)+ z[45];
    z[19]=z[19]*z[78];
    z[17]=z[19] + 4*z[17] + z[37];
    z[17]=z[7]*z[17];
    z[17]= - 9*z[84] + z[17];
    z[17]=z[7]*z[17];
    z[19]= - z[12] - z[29];
    z[19]=z[19]*z[73];
    z[19]=z[62] + z[19];
    z[19]=z[19]*z[67];
    z[19]=z[19] - z[33];
    z[19]=z[19]*z[61];
    z[18]= - z[18]*z[77];
    z[22]= - z[48]*z[75];
    z[17]=z[19] + z[22] + z[17] + z[18];
    z[17]=z[17]*z[61];
    z[18]= - 6*z[12] - z[92];
    z[19]=static_cast<T>(3)- n<T>(8,3)*z[45];
    z[19]=z[7]*z[19];
    z[18]=z[19] + 2*z[18] - z[4];
    z[18]=z[7]*z[18];
    z[19]=z[48]*z[21];
    z[17]=z[17] + z[19] - 9*z[77] + 24*z[84] + z[18];
    z[17]=z[10]*z[17];
    z[18]= - z[68] - z[37];
    z[18]=z[7]*z[18];
    z[19]=z[9]*z[64];
    z[19]=6*z[19] - z[90] + z[8];
    z[19]=z[19]*z[74];
    z[19]= - z[72] + z[19];
    z[19]=z[9]*z[19];
    z[19]=z[50] + z[19];
    z[19]=z[2]*z[19];
    z[21]=static_cast<T>(1)- z[76];
    z[21]=z[9]*z[21];
    z[21]=14*z[21] + 28*z[12] - n<T>(17,3)*z[7];
    z[21]=z[9]*z[21];
    z[18]=z[19] + z[21] + z[18] - z[65];
    z[18]=z[2]*z[18];
    z[19]=z[13]*z[79];
    z[19]=z[19] - n<T>(1,3) + 4*z[45];
    z[19]=z[7]*z[19];
    z[21]= - static_cast<T>(3)+ 13*z[64];
    z[21]=z[21]*z[74];
    z[22]=static_cast<T>(6)- 5*z[64];
    z[22]=z[11]*z[22];
    z[22]=z[8] - 9*z[12] + z[22];
    z[15]=z[15] + z[16] + z[17] + z[18] + z[21] + 2*z[22] + z[19];
    z[15]=z[1]*z[15];
    z[16]=3*z[6] - z[7] + z[20] - z[39];
    z[16]=z[10]*z[16];
    z[17]= - z[12] + z[9];
    z[17]=z[17]*z[69];
    z[18]=z[11]*z[63];
    z[16]=z[17] + z[18] - static_cast<T>(1)- z[66] + z[16];

    r += z[15] + 2*z[16];
 
    return r;
}

template double qg_2lNLC_r1438(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1438(const std::array<dd_real,31>&);
#endif
