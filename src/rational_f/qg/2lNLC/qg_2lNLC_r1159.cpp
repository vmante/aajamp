#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1159(const std::array<T,31>& k) {
  T z[156];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[19];
    z[6]=k[9];
    z[7]=k[11];
    z[8]=k[15];
    z[9]=k[5];
    z[10]=k[6];
    z[11]=k[8];
    z[12]=k[14];
    z[13]=k[2];
    z[14]=k[10];
    z[15]=k[3];
    z[16]=k[4];
    z[17]=k[26];
    z[18]=k[29];
    z[19]=npow(z[1],3);
    z[20]=4*z[19];
    z[21]=npow(z[1],2);
    z[22]=z[21]*z[16];
    z[23]=z[20] - z[22];
    z[24]=npow(z[1],4);
    z[25]=13*z[24];
    z[26]=z[19]*z[16];
    z[27]= - z[25] + 7*z[26];
    z[28]=z[3]*z[27];
    z[23]=4*z[23] + z[28];
    z[28]=n<T>(1,3)*z[3];
    z[23]=z[23]*z[28];
    z[29]=n<T>(4,3)*z[10];
    z[30]= - z[22]*z[29];
    z[31]=npow(z[16],2);
    z[30]=3*z[31] + z[30];
    z[30]=z[10]*z[30];
    z[32]=4*z[21];
    z[33]= - z[11]*z[32];
    z[30]=z[33] + n<T>(16,3)*z[1] + z[30];
    z[33]=npow(z[10],2);
    z[34]=z[33]*z[21];
    z[35]=2*z[10];
    z[36]= - z[13]*z[35];
    z[36]= - n<T>(5,3)*z[34] + z[36];
    z[36]=z[13]*z[36];
    z[30]=2*z[30] + z[36];
    z[30]=z[13]*z[30];
    z[36]=z[16]*z[1];
    z[37]=2*z[36];
    z[38]=3*z[21];
    z[39]=z[38] - z[37];
    z[40]=z[10]*z[21]*z[31];
    z[41]=npow(z[16],3);
    z[42]=4*z[41];
    z[40]= - z[42] + n<T>(13,3)*z[40];
    z[40]=z[10]*z[40];
    z[43]=10*z[21];
    z[44]=7*z[36];
    z[45]=z[43] - z[44];
    z[45]=z[16]*z[45];
    z[46]=3*z[19];
    z[45]= - z[46] + z[45];
    z[47]=2*z[2];
    z[45]=z[45]*z[47];
    z[23]=z[45] + z[30] + z[23] + 2*z[39] + z[40];
    z[23]=z[8]*z[23];
    z[30]=2*z[19];
    z[39]=z[33]*z[30]*z[31];
    z[40]=z[41] + z[39];
    z[45]=3*z[10];
    z[40]=z[40]*z[45];
    z[48]= - z[33]*z[26];
    z[48]= - z[31] + z[48];
    z[48]=z[10]*z[48];
    z[49]=2*z[1];
    z[50]=2*z[21];
    z[51]=z[11]*z[50];
    z[48]=z[51] - z[49] + z[48];
    z[51]=z[13]*z[10];
    z[52]=z[16]*z[51];
    z[48]=3*z[48] + z[52];
    z[48]=z[13]*z[48];
    z[52]=n<T>(2,3)*z[36];
    z[40]=z[48] + z[52] + z[40];
    z[40]=z[13]*z[40];
    z[48]=n<T>(17,3)*z[36];
    z[53]=z[48] - z[38];
    z[54]=2*z[16];
    z[55]=z[53]*z[54];
    z[56]=z[46]*z[33];
    z[57]= - z[41]*z[56];
    z[58]=npow(z[16],4);
    z[57]= - z[58] + z[57];
    z[57]=z[10]*z[57];
    z[59]=z[24]*z[3];
    z[60]=n<T>(7,3)*z[59];
    z[61]= - z[16]*z[60];
    z[61]= - z[26] + z[61];
    z[61]=z[3]*z[61];
    z[62]=z[50] - z[36];
    z[63]=z[62]*z[16];
    z[63]=z[63] - z[19];
    z[64]=z[16]*z[63]*z[2];
    z[40]=6*z[64] + z[40] + z[61] + z[55] + z[57];
    z[40]=z[8]*z[40];
    z[39]=z[14]*z[39];
    z[55]=z[41]*z[14];
    z[39]=z[55] + z[39];
    z[39]=z[39]*z[45];
    z[45]=z[14]*z[16];
    z[57]= - z[19]*z[33]*z[45];
    z[61]=z[31]*z[14];
    z[57]= - z[61] + z[57];
    z[57]=z[10]*z[57];
    z[61]=z[49]*z[14];
    z[64]=z[50]*z[14];
    z[65]=z[11]*z[64];
    z[57]=z[65] - z[61] + z[57];
    z[65]=z[45]*z[51];
    z[57]=3*z[57] + z[65];
    z[57]=z[13]*z[57];
    z[65]=z[14]*z[52];
    z[39]=z[57] + z[65] + z[39];
    z[39]=z[13]*z[39];
    z[57]= - z[19] - z[60];
    z[57]=z[3]*z[57];
    z[53]=z[57] + 2*z[53];
    z[53]=z[45]*z[53];
    z[56]= - z[55]*z[56];
    z[57]=z[58]*z[14];
    z[56]= - z[57] + z[56];
    z[56]=z[10]*z[56];
    z[58]=z[63]*z[45];
    z[60]=z[2]*z[58];
    z[39]=z[40] + 6*z[60] + z[39] + z[56] + z[53];
    z[39]=z[18]*z[39];
    z[40]=z[21]*z[9];
    z[53]=z[30] + z[40];
    z[56]=z[40] + z[19];
    z[60]=z[56]*z[12];
    z[63]= - z[9]*z[60];
    z[63]=z[63] - z[53];
    z[65]=2*z[12];
    z[63]=z[63]*z[65];
    z[63]= - z[21] + z[63];
    z[66]=2*z[22];
    z[67]= - z[19] - z[66];
    z[68]= - z[45]*z[46];
    z[67]=n<T>(2,3)*z[67] + z[68];
    z[67]=z[14]*z[67];
    z[68]=z[45]*z[24];
    z[27]= - 7*z[68] + z[27];
    z[69]=n<T>(1,3)*z[14];
    z[27]=z[3]*z[27]*z[69];
    z[27]=z[27] + 2*z[63] + z[67];
    z[27]=z[3]*z[27];
    z[63]=npow(z[12],2);
    z[67]=z[19]*z[9];
    z[70]=z[63]*z[67];
    z[71]=npow(z[14],2);
    z[72]= - z[71]*z[26];
    z[72]= - 4*z[70] + z[72];
    z[72]=z[10]*z[72];
    z[73]=z[21]*z[12];
    z[74]=z[21]*z[14];
    z[72]=z[72] + 8*z[73] - n<T>(23,3)*z[74];
    z[72]=z[10]*z[72];
    z[75]=npow(z[45],2);
    z[72]= - 3*z[75] + z[72];
    z[72]=z[10]*z[72];
    z[76]= - z[71]*z[49];
    z[77]=z[63]*z[1];
    z[76]= - z[77] + z[76];
    z[78]=z[1] + z[73];
    z[78]=z[12]*z[78];
    z[79]= - z[1] + z[64];
    z[79]=z[14]*z[79];
    z[78]=z[78] + z[79];
    z[79]=4*z[11];
    z[78]=z[78]*z[79];
    z[80]= - static_cast<T>(2)+ z[45];
    z[80]=z[14]*z[80]*z[51];
    z[72]=z[80] + z[78] + 4*z[76] + z[72];
    z[72]=z[13]*z[72];
    z[76]=z[9]*z[1];
    z[78]=z[76] + z[21];
    z[80]=3*z[78];
    z[81]= - z[12]*z[80];
    z[81]=z[49] + z[81];
    z[81]=z[81]*z[65];
    z[82]=z[14]*z[44];
    z[82]= - 8*z[1] + z[82];
    z[82]=z[82]*z[69];
    z[81]=z[82] - static_cast<T>(3)+ z[81];
    z[75]=z[75]*z[19];
    z[82]=npow(z[9],2);
    z[83]=z[63]*z[82];
    z[84]=z[83]*z[30];
    z[85]=z[84] + z[75];
    z[85]=z[10]*z[85];
    z[86]=z[45]*z[21];
    z[87]=z[40]*z[12];
    z[88]= - z[87] + n<T>(4,3)*z[86];
    z[85]=2*z[88] + z[85];
    z[85]=z[85]*z[35];
    z[88]=2*z[31];
    z[89]=z[88] + z[55];
    z[90]=3*z[14];
    z[89]=z[89]*z[90];
    z[85]=z[89] + z[85];
    z[85]=z[10]*z[85];
    z[89]=z[12]*z[46];
    z[89]= - z[21] + z[89];
    z[89]=z[89]*z[65];
    z[89]= - 6*z[74] + z[1] + z[89];
    z[91]=2*z[11];
    z[89]=z[89]*z[91];
    z[72]=z[72] + z[89] + 2*z[81] + z[85];
    z[72]=z[13]*z[72];
    z[81]=z[40] - z[19];
    z[85]=z[12]*z[9];
    z[89]=z[81]*z[85];
    z[92]=3*z[40];
    z[89]=z[89] - z[30] + z[92];
    z[89]=z[89]*z[65];
    z[93]=8*z[21];
    z[94]=5*z[36];
    z[95]=z[93] - z[94];
    z[95]=z[16]*z[95];
    z[58]=2*z[58] - z[46] + z[95];
    z[95]=2*z[14];
    z[58]=z[58]*z[95];
    z[96]=9*z[21];
    z[97]=2*z[76];
    z[58]=z[58] + z[89] - 4*z[36] + z[96] - z[97];
    z[58]=z[58]*z[47];
    z[89]=z[76] - z[21];
    z[98]=z[50] - z[76];
    z[99]= - z[9]*z[98];
    z[99]= - z[19] + z[99];
    z[99]=z[12]*z[99];
    z[99]=z[99] + z[89];
    z[99]=z[99]*z[65];
    z[99]=z[99] + z[49] + z[9];
    z[100]=5*z[21];
    z[48]= - z[100] + z[48];
    z[48]=z[48]*z[45];
    z[101]=7*z[21];
    z[48]=z[48] - z[101] + 6*z[36];
    z[48]=z[14]*z[48];
    z[48]=2*z[99] + z[48];
    z[99]=z[71]*z[10];
    z[41]=z[19]*z[99]*z[41];
    z[102]=z[74]*z[31];
    z[103]=n<T>(7,3)*z[102] - z[41];
    z[103]=z[10]*z[103];
    z[42]= - z[42] - z[57];
    z[42]=z[14]*z[42];
    z[42]=z[42] + z[103];
    z[42]=z[10]*z[42];
    z[57]=z[24]*z[12];
    z[103]= - z[30] + z[57];
    z[103]=z[103]*z[65];
    z[98]=z[103] + z[98];
    z[98]=z[98]*z[79];
    z[23]=z[39] + z[23] + z[58] + z[72] + z[98] + z[27] + 2*z[48] + 
    z[42];
    z[23]=z[18]*z[23];
    z[27]=z[67] - z[24];
    z[39]=z[27]*z[9];
    z[42]=z[3]*npow(z[1],6);
    z[48]=npow(z[1],5);
    z[39]=z[48] + z[39] + z[42];
    z[42]=6*z[6];
    z[58]= - z[39]*z[42];
    z[72]=z[48]*z[3];
    z[98]=z[72] + z[24];
    z[103]=15*z[19];
    z[104]=11*z[40];
    z[105]= - z[103] + z[104];
    z[105]=z[9]*z[105];
    z[58]=z[58] + z[105] + 15*z[98];
    z[58]=z[6]*z[58];
    z[98]=7*z[76];
    z[105]=15*z[21];
    z[106]=z[105] - z[98];
    z[106]=z[9]*z[106];
    z[58]=z[58] - 15*z[59] - z[103] + z[106];
    z[103]=2*z[6];
    z[58]=z[58]*z[103];
    z[106]=z[32] + 17*z[76];
    z[106]=z[106]*z[82];
    z[107]=z[97] + z[21];
    z[108]=npow(z[9],3);
    z[109]=z[107]*z[108];
    z[110]=5*z[12];
    z[111]= - z[110]*z[109];
    z[106]=z[106] + z[111];
    z[106]=z[12]*z[106];
    z[111]=z[82]*z[1];
    z[106]= - 8*z[111] + z[106];
    z[106]=z[12]*z[106];
    z[112]=npow(z[76],4);
    z[113]=z[63]*z[112];
    z[114]=npow(z[76],5);
    z[115]=z[10]*npow(z[12],3);
    z[116]=5*z[115];
    z[117]= - z[114]*z[116];
    z[113]=13*z[113] + z[117];
    z[113]=z[10]*z[113];
    z[117]=z[108]*z[19];
    z[118]=z[12]*z[117];
    z[113]= - 9*z[118] + n<T>(2,3)*z[113];
    z[113]=z[113]*z[35];
    z[118]=z[82]*z[21];
    z[119]=z[40]*z[16];
    z[113]=z[113] + n<T>(31,3)*z[118] + z[119];
    z[113]=z[10]*z[113];
    z[120]=z[16]*z[82];
    z[113]=z[120] + z[113];
    z[113]=z[10]*z[113];
    z[120]=7*z[19];
    z[121]=z[120] - z[22];
    z[122]=z[24] - z[26];
    z[123]=2*z[3];
    z[122]=z[122]*z[123];
    z[121]=7*z[121] + z[122];
    z[121]=z[121]*z[28];
    z[122]=n<T>(5,3)*z[36];
    z[58]=z[58] + z[121] + z[113] + n<T>(4,3)*z[106] - z[122] + n<T>(47,3)*z[21]
    - 13*z[76];
    z[106]=2*z[7];
    z[58]=z[58]*z[106];
    z[113]=z[24]*z[9];
    z[113]=z[113] - z[48];
    z[121]=12*z[6];
    z[124]= - z[113]*z[121];
    z[125]=3*z[59];
    z[126]=z[9]*z[125];
    z[124]=z[124] + z[126] - 30*z[24] + 13*z[67];
    z[124]=z[3]*z[124];
    z[124]= - 12*z[40] + z[124];
    z[124]=z[6]*z[124];
    z[126]=z[24] + 2*z[67];
    z[126]=z[126]*z[123];
    z[126]=z[126] + 29*z[19] + z[40];
    z[126]=z[3]*z[126];
    z[124]=z[124] + z[126] - z[38] + 11*z[76];
    z[124]=z[124]*z[103];
    z[83]=z[19]*z[83];
    z[126]=z[108]*z[24];
    z[127]=z[115]*z[126];
    z[83]=z[83] - n<T>(5,3)*z[127];
    z[83]=z[83]*z[35];
    z[83]=n<T>(1,3)*z[87] + z[83];
    z[127]=4*z[10];
    z[83]=z[83]*z[127];
    z[83]= - 13*z[16] + z[83];
    z[83]=z[10]*z[83];
    z[128]=z[24]*z[82]*z[115];
    z[128]= - 31*z[70] + 20*z[128];
    z[128]=z[10]*z[128];
    z[129]=z[12]*z[38];
    z[128]=z[129] + n<T>(1,3)*z[128];
    z[128]=z[10]*z[128];
    z[85]=static_cast<T>(1)- z[85];
    z[85]=5*z[85] + z[128];
    z[85]=z[10]*z[85];
    z[51]=z[9]*z[63]*z[51];
    z[51]=z[85] + z[51];
    z[51]=z[13]*z[51];
    z[85]=6*z[12];
    z[128]= - z[76]*z[85];
    z[128]=n<T>(1,3)*z[1] + z[128];
    z[128]=z[128]*z[65];
    z[51]=2*z[51] + z[83] + static_cast<T>(13)+ z[128];
    z[51]=z[13]*z[51];
    z[83]=z[92] + z[66];
    z[83]=z[16]*z[83];
    z[83]=z[118] + z[83];
    z[83]=z[10]*z[83];
    z[128]=2*z[9];
    z[129]= - z[128] - z[16];
    z[129]=z[16]*z[129];
    z[129]= - z[82] + z[129];
    z[129]=z[16]*z[129];
    z[83]=z[129] + z[83];
    z[83]=z[83]*z[35];
    z[129]=3*z[76];
    z[44]=z[83] - z[44] + z[32] - z[129];
    z[44]=z[44]*z[47];
    z[83]=z[50] + z[76];
    z[108]=z[108]*z[1];
    z[130]=z[110]*z[108];
    z[130]= - 4*z[111] + z[130];
    z[130]=z[12]*z[130];
    z[130]=z[130] - z[83];
    z[130]=z[12]*z[130];
    z[131]=z[63]*z[117];
    z[132]=z[112]*z[116];
    z[131]= - 8*z[131] + z[132];
    z[131]=z[131]*z[35];
    z[132]=z[82]*z[73];
    z[131]=11*z[132] + z[131];
    z[29]=z[131]*z[29];
    z[131]=8*z[9];
    z[132]=7*z[16];
    z[133]=z[131] + z[132];
    z[133]=z[16]*z[133];
    z[29]=z[133] + z[29];
    z[29]=z[10]*z[29];
    z[133]=z[19]*z[3];
    z[134]= - 13*z[21] + z[133];
    z[134]=z[3]*z[134];
    z[135]=79*z[1] - 68*z[9];
    z[43]=z[11]*z[43];
    z[29]=z[58] + z[44] + z[51] + z[43] + z[124] + z[134] + z[29] + n<T>(8,3)
   *z[130] + n<T>(1,3)*z[135] - 18*z[16];
    z[29]=z[8]*z[29];
    z[43]=z[40]*z[33];
    z[44]=z[3]*z[78];
    z[44]=z[44] + z[49] + z[43];
    z[51]=3*z[3];
    z[44]=z[44]*z[51];
    z[58]= - z[40]*z[51];
    z[58]=z[58] + z[100] - z[129];
    z[58]=z[3]*z[58];
    z[124]=z[3]*z[81];
    z[124]=z[21] + z[124];
    z[124]=z[124]*z[121];
    z[130]=3*z[1];
    z[58]=z[124] - z[130] + z[58];
    z[58]=z[58]*z[103];
    z[124]=25*z[1] - 9*z[74];
    z[124]=z[14]*z[124];
    z[44]=z[58] + z[44] + static_cast<T>(1)+ z[124];
    z[44]=z[6]*z[44];
    z[58]=z[33]*z[74];
    z[124]=z[71]*z[1];
    z[58]= - 6*z[124] + z[58];
    z[134]=19*z[1];
    z[135]=z[38]*z[14];
    z[136]= - z[134] + z[135];
    z[136]=z[14]*z[136]*z[103];
    z[58]=3*z[58] + z[136];
    z[58]=z[6]*z[58];
    z[136]=npow(z[10],3);
    z[137]=z[136]*z[19];
    z[71]= - z[71]*z[137];
    z[138]=z[14]*z[1];
    z[139]=4*z[6];
    z[140]=z[139]*z[138];
    z[140]=z[124] + z[140];
    z[140]=z[140]*z[103];
    z[71]=z[71] + z[140];
    z[140]=3*z[6];
    z[71]=z[71]*z[140];
    z[71]=z[99] + z[71];
    z[71]=z[13]*z[71];
    z[140]=n<T>(5,3)*z[21];
    z[140]=z[99]*z[140];
    z[141]= - static_cast<T>(1)- 3*z[45];
    z[141]=z[14]*z[141];
    z[140]=z[141] + z[140];
    z[140]=z[10]*z[140];
    z[58]=z[71] + z[140] + z[58];
    z[58]=z[13]*z[58];
    z[71]= - z[45]*z[50];
    z[75]= - z[10]*z[75];
    z[71]=z[71] + z[75];
    z[71]=z[10]*z[71];
    z[75]= - z[31] - z[55];
    z[75]=z[14]*z[75];
    z[71]=z[75] + n<T>(7,3)*z[71];
    z[71]=z[10]*z[71];
    z[75]= - z[3]*z[89];
    z[75]=z[75] + n<T>(4,3)*z[1] - z[43];
    z[75]=z[3]*z[75];
    z[140]= - z[101] + z[94];
    z[140]=z[140]*z[69];
    z[141]=5*z[1];
    z[140]= - z[141] + z[140];
    z[140]=z[14]*z[140];
    z[71]=z[75] + z[71] - static_cast<T>(1)+ z[140];
    z[71]=z[2]*z[71];
    z[75]=z[90]*z[31];
    z[140]=z[54] + z[75];
    z[140]=z[14]*z[140];
    z[142]=z[22]*z[99];
    z[140]=z[140] + n<T>(11,3)*z[142];
    z[140]=z[10]*z[140];
    z[142]=z[30]*z[3];
    z[125]=z[6]*z[125];
    z[125]= - z[142] + z[125];
    z[125]=z[6]*z[125];
    z[143]=z[3]*z[21];
    z[125]=z[143] + z[125];
    z[125]=z[125]*z[139];
    z[143]=z[3]*z[1];
    z[125]=z[125] - z[61] + z[143];
    z[125]=z[125]*z[106];
    z[143]=z[38] + z[76];
    z[144]=3*z[67];
    z[145]= - z[144] + z[26];
    z[145]=z[16]*z[145]*z[136];
    z[146]=3*z[36];
    z[145]=z[145] - z[146] + z[143];
    z[147]=4*z[4];
    z[145]=z[145]*z[147];
    z[148]=3*z[22];
    z[149]=2*z[40] - z[148];
    z[150]=2*z[33];
    z[149]=z[149]*z[150];
    z[151]=9*z[1];
    z[145]=z[145] + z[151] + z[149];
    z[149]=2*z[4];
    z[145]=z[145]*z[149];
    z[43]= - z[130] - z[43];
    z[43]=z[6]*z[43];
    z[43]=z[145] + static_cast<T>(4)+ z[43];
    z[43]=z[43]*z[149];
    z[145]= - z[33]*z[100];
    z[152]=z[16]*z[30];
    z[152]= - z[67] + z[152];
    z[152]=z[152]*z[136];
    z[152]= - z[130] + z[152];
    z[152]=z[152]*z[147];
    z[145]=z[145] + z[152];
    z[145]=z[145]*z[149];
    z[34]=z[6]*z[34];
    z[34]=z[34] + z[145];
    z[34]=z[4]*z[34];
    z[137]=z[15]*npow(z[4],3)*z[137];
    z[34]=z[34] + 8*z[137];
    z[34]=z[15]*z[34];
    z[137]=static_cast<T>(11)+ 37*z[138];
    z[137]=z[137]*z[69];
    z[61]= - z[3]*z[61];
    z[34]=2*z[34] + z[43] + z[125] + z[71] + z[58] + z[44] + z[61] + 
    z[137] + z[140];
    z[34]=z[15]*z[34];
    z[43]=n<T>(1,3)*z[76];
    z[44]= - z[21] - z[43];
    z[44]=z[9]*z[44];
    z[58]=z[136]*z[67];
    z[61]=z[31]*z[58];
    z[71]=z[16]*z[76];
    z[44]= - n<T>(1,3)*z[61] + z[44] + z[71];
    z[44]=z[44]*z[79];
    z[61]= - z[40] + z[22];
    z[71]=z[33]*z[16];
    z[61]=z[61]*z[71];
    z[79]=n<T>(7,3)*z[21];
    z[44]=z[44] + n<T>(1,3)*z[61] - z[122] + z[79] + z[76];
    z[44]=z[44]*z[91];
    z[61]= - z[26] - n<T>(1,3)*z[24] - z[67];
    z[122]=20*z[12];
    z[61]=z[61]*z[122];
    z[125]=23*z[19];
    z[61]=z[61] - n<T>(17,3)*z[22] + z[125] - n<T>(25,3)*z[40];
    z[61]=z[12]*z[61];
    z[137]=38*z[36] - 79*z[21] + 19*z[76];
    z[61]=n<T>(1,3)*z[137] + z[61];
    z[61]=z[61]*z[65];
    z[137]= - z[19]*z[31]*z[122];
    z[138]=40*z[19] - 23*z[22];
    z[138]=z[16]*z[138];
    z[137]=z[138] + z[137];
    z[137]=z[12]*z[137];
    z[138]= - 26*z[21] + n<T>(37,3)*z[36];
    z[138]=z[16]*z[138];
    z[137]=n<T>(1,3)*z[137] + 9*z[19] + z[138];
    z[137]=z[12]*z[137];
    z[138]=z[22] - z[19];
    z[140]= - z[138]*z[31]*z[110];
    z[145]= - z[100] + z[146];
    z[145]=z[16]*z[145];
    z[145]=z[30] + z[145];
    z[54]=z[145]*z[54];
    z[54]=z[54] + z[140];
    z[54]=z[12]*z[54];
    z[140]=z[1]*z[31];
    z[54]=z[140] + z[54];
    z[54]=z[14]*z[54];
    z[54]=z[54] + z[137] - z[38] + n<T>(11,3)*z[36];
    z[54]=z[54]*z[95];
    z[43]= - z[21] + z[43];
    z[137]=z[33]*z[118];
    z[140]= - n<T>(7,3)*z[19] + z[111];
    z[140]=z[3]*z[140];
    z[43]=z[140] + 25*z[43] + z[137];
    z[43]=z[3]*z[43];
    z[41]=n<T>(2,3)*z[102] - z[41];
    z[41]=z[41]*z[35];
    z[137]=4*z[9];
    z[140]= - z[137] - 3*z[16];
    z[140]=z[16]*z[140];
    z[41]=z[41] + z[140] + z[55];
    z[41]=z[10]*z[41];
    z[140]=z[65]*z[9];
    z[145]= - z[140] - 1;
    z[145]=z[65]*z[81]*z[145];
    z[152]=z[19]*z[14];
    z[153]=z[133] + z[152];
    z[145]=z[145] - n<T>(7,3)*z[153];
    z[145]=z[6]*z[145];
    z[153]=z[16] + z[9];
    z[41]=z[44] + z[145] + z[43] + z[41] + z[54] + z[61] - n<T>(35,3)*z[1]
    + 9*z[153];
    z[41]=z[2]*z[41];
    z[43]=z[89]*z[9];
    z[44]=z[43] + z[19];
    z[54]= - z[16] + z[9];
    z[54]=z[44]*z[54];
    z[61]=z[126]*z[16];
    z[145]=z[112] + z[61];
    z[153]=npow(z[10],4);
    z[145]=z[145]*z[153];
    z[154]=z[24]*z[16];
    z[155]= - z[48] - z[154];
    z[155]=z[3]*z[155];
    z[114]= - npow(z[10],5)*z[114];
    z[109]= - z[109] + z[114];
    z[109]=z[11]*z[109];
    z[54]=z[109] + z[155] + z[145] - z[24] + z[54];
    z[54]=z[7]*z[54];
    z[109]=z[82]*z[154];
    z[109]=z[126] + z[109];
    z[109]=z[16]*z[109];
    z[109]=z[112] + z[109];
    z[109]=z[109]*z[153];
    z[108]=z[108] + z[109];
    z[108]=z[11]*z[108];
    z[58]= - z[58]*z[88];
    z[88]= - z[2]*z[3]*z[113];
    z[109]=z[16]*z[129];
    z[54]=z[54] + z[88] + z[108] + z[59] + z[58] + z[109] + z[81];
    z[54]=z[54]*z[147];
    z[58]=5*z[117];
    z[88]=4*z[26];
    z[108]=z[82]*z[88];
    z[108]=z[58] + z[108];
    z[108]=z[108]*z[136];
    z[109]=5*z[76];
    z[113]= - z[50] - z[109];
    z[113]=z[113]*z[82];
    z[114]=z[153]*z[112];
    z[113]=z[113] - 3*z[114];
    z[113]=z[11]*z[113];
    z[114]=4*z[16];
    z[126]= - z[89]*z[114];
    z[129]=9*z[24];
    z[145]=z[129] + z[88];
    z[145]=z[3]*z[145];
    z[108]=3*z[113] + z[145] + z[108] + 9*z[44] + z[126];
    z[108]=z[7]*z[108];
    z[113]=z[19]*z[82];
    z[126]=z[16]*z[67];
    z[113]=z[113] + z[126];
    z[113]=z[16]*z[113];
    z[58]=z[58] + z[113];
    z[58]=z[58]*z[136];
    z[113]=z[16]*z[109];
    z[58]=z[58] + 6*z[111] + z[113];
    z[58]=z[11]*z[58];
    z[113]=5*z[40];
    z[126]=z[113] - z[22];
    z[71]=z[126]*z[71];
    z[126]= - z[129] + 8*z[67];
    z[126]=z[3]*z[126];
    z[113]=z[113] + z[126];
    z[113]=z[2]*z[113];
    z[126]=8*z[133];
    z[54]=z[54] + z[108] + z[113] + z[58] - z[126] + z[71] - z[80] + 
    z[94];
    z[54]=z[54]*z[149];
    z[58]=z[21] - z[36];
    z[58]=z[12]*z[16]*z[58];
    z[71]=6*z[118] - n<T>(5,3)*z[119];
    z[71]=z[71]*z[33];
    z[58]=z[71] + z[58] + z[146] + z[21] + n<T>(23,3)*z[76];
    z[58]=z[11]*z[58];
    z[71]= - z[21] - z[109];
    z[71]=z[9]*z[71];
    z[80]=4*z[117];
    z[94]= - z[136]*z[80];
    z[71]=z[71] + z[94];
    z[71]=z[71]*z[91];
    z[91]=z[82]*z[38];
    z[94]=2*z[119];
    z[91]=z[91] + z[94];
    z[91]=z[91]*z[33];
    z[66]= - z[120] - z[66];
    z[66]=z[3]*z[66];
    z[66]=z[71] + z[66] + z[91] + 7*z[89] - z[37];
    z[66]=z[66]*z[106];
    z[71]=z[21] - n<T>(8,3)*z[76];
    z[91]=18*z[19] - n<T>(35,3)*z[40];
    z[91]=z[3]*z[91];
    z[81]= - z[6]*z[81];
    z[71]=z[81] + z[91] + 4*z[71] + z[36];
    z[71]=z[2]*z[71];
    z[81]=z[93] - z[60];
    z[81]=z[3]*z[81];
    z[91]=z[6]*z[143];
    z[108]= - z[49] - n<T>(5,3)*z[9];
    z[54]=z[54] + z[66] + z[71] + z[58] + z[91] + z[81] - z[73] + 4*
    z[108] - z[16];
    z[54]=z[54]*z[149];
    z[58]=z[90]*z[24];
    z[66]=z[24] + z[144];
    z[66]=z[66]*z[121];
    z[66]=z[66] - z[58] + 8*z[57] - 13*z[19] - 44*z[40];
    z[66]=z[6]*z[66];
    z[57]= - z[46] + z[57];
    z[71]=4*z[12];
    z[57]=z[57]*z[71];
    z[81]= - z[14]*z[24];
    z[81]=z[120] + z[81];
    z[81]=z[14]*z[81];
    z[91]=19*z[21];
    z[57]=z[66] + z[81] + z[57] + z[91] + 15*z[76];
    z[57]=z[57]*z[103];
    z[66]=z[73]*z[31];
    z[81]= - 4*z[66] - z[102];
    z[81]=z[10]*z[81];
    z[108]= - z[137] - z[16];
    z[108]=z[16]*z[108];
    z[81]=z[108] + z[81];
    z[81]=z[10]*z[81];
    z[108]=z[119]*z[33];
    z[62]=z[108] - z[76] + z[62];
    z[113]=n<T>(8,3)*z[11];
    z[62]=z[62]*z[113];
    z[129]= - z[141] + z[137];
    z[129]=7*z[129] + 17*z[16];
    z[133]=9*z[36];
    z[101]= - z[101] + z[133];
    z[101]=z[101]*z[65];
    z[136]=11*z[21];
    z[37]=z[136] + z[37];
    z[37]=4*z[37] - 5*z[152];
    z[37]=z[37]*z[69];
    z[37]=z[62] + z[57] + z[81] + z[37] + n<T>(1,3)*z[129] + z[101];
    z[37]=z[11]*z[37];
    z[57]=29*z[21];
    z[62]=z[57] - 6*z[152];
    z[62]=z[14]*z[62];
    z[81]=z[21]*z[6];
    z[101]=z[1] - 6*z[73];
    z[62]=36*z[81] + 2*z[101] + z[62];
    z[62]=z[6]*z[62];
    z[81]=z[89]*z[85];
    z[81]=z[1] + z[81];
    z[81]=z[12]*z[81];
    z[89]= - z[1] + z[135];
    z[89]=z[89]*z[90];
    z[84]= - z[10]*z[84];
    z[84]=z[87] + z[84];
    z[84]=z[84]*z[33];
    z[62]=z[62] + z[84] + z[89] + static_cast<T>(1)+ z[81];
    z[62]=z[62]*z[103];
    z[70]=z[35]*z[70];
    z[70]=z[70] - z[73] - z[74];
    z[70]=z[70]*z[150];
    z[49]=z[49] - z[135];
    z[49]=z[14]*z[49];
    z[81]= - z[74]*z[121];
    z[84]= - z[1]*z[65];
    z[49]=z[81] + z[84] + z[49];
    z[49]=z[49]*z[139];
    z[49]=z[49] + z[70] - 4*z[77] - 5*z[124];
    z[49]=z[6]*z[49];
    z[70]=z[46]*z[14];
    z[77]= - z[32] + z[70];
    z[77]=z[14]*z[77];
    z[81]=z[152]*z[121];
    z[84]=4*z[73];
    z[77]=z[81] + z[84] + z[77];
    z[77]=z[6]*z[77];
    z[50]=z[12]*z[50];
    z[50]=z[130] + z[50];
    z[50]=z[12]*z[50];
    z[81]= - z[130] + 4*z[74];
    z[81]=z[14]*z[81];
    z[50]=z[77] + z[50] + z[81];
    z[50]=z[50]*z[103];
    z[77]= - z[84] + 5*z[74];
    z[77]=z[77]*z[33];
    z[50]=z[50] + z[124] + z[77];
    z[50]=z[11]*z[50];
    z[77]=6*z[21];
    z[81]=z[63]*z[77];
    z[73]= - 7*z[73] + z[135];
    z[73]=z[14]*z[73];
    z[73]=z[81] + z[73];
    z[73]=z[10]*z[73];
    z[81]= - static_cast<T>(1)- z[45];
    z[81]=z[81]*z[90];
    z[84]= - static_cast<T>(1)+ z[140];
    z[84]=z[12]*z[84];
    z[73]=z[73] + z[84] + z[81];
    z[73]=z[10]*z[73];
    z[81]=z[13]*z[99];
    z[49]=z[81] + z[50] + z[73] + z[49];
    z[49]=z[13]*z[49];
    z[50]=z[19]*z[85];
    z[50]=z[136] + z[50];
    z[73]=10*z[19];
    z[58]= - z[73] + z[58];
    z[58]=z[14]*z[58];
    z[81]=36*z[6];
    z[81]= - z[19]*z[81];
    z[50]=z[81] + 2*z[50] + z[58];
    z[50]=z[6]*z[50];
    z[58]=z[19]*z[65];
    z[58]= - z[21] + z[58];
    z[81]=3*z[12];
    z[58]=z[58]*z[81];
    z[50]=z[50] - 8*z[74] - z[1] + z[58];
    z[50]=z[50]*z[103];
    z[58]=z[22]*z[65];
    z[58]=z[58] - z[86];
    z[58]=z[58]*z[127];
    z[58]=z[16] + z[58];
    z[58]=z[10]*z[58];
    z[74]= - z[79] - z[36];
    z[74]=z[14]*z[74];
    z[74]=z[141] + z[74];
    z[74]=z[14]*z[74];
    z[79]=z[113] - 18*z[12];
    z[79]=z[1]*z[79];
    z[50]=z[50] + z[58] + z[74] - n<T>(31,3) + z[79];
    z[50]=z[11]*z[50];
    z[58]= - n<T>(47,3)*z[40] - z[148];
    z[58]=z[58]*z[63];
    z[32]= - z[45]*z[32];
    z[45]=z[12]*z[22];
    z[32]=7*z[45] + z[32];
    z[32]=z[14]*z[32];
    z[32]=z[58] + z[32];
    z[32]=z[32]*z[35];
    z[45]=z[132] + z[75];
    z[45]=z[14]*z[45];
    z[58]= - z[131] + z[16];
    z[58]=z[12]*z[58];
    z[32]=z[32] + z[45] - static_cast<T>(2)+ z[58];
    z[32]=z[10]*z[32];
    z[45]= - z[76]*z[110];
    z[45]=n<T>(17,3)*z[1] + z[45];
    z[45]=z[12]*z[45];
    z[45]=n<T>(127,3) + 8*z[45];
    z[45]=z[12]*z[45];
    z[58]=z[36]*z[12];
    z[74]=n<T>(25,3)*z[1] + 18*z[58];
    z[74]=z[12]*z[74];
    z[58]=n<T>(91,3)*z[1] + 17*z[58];
    z[58]=z[14]*z[58];
    z[58]=z[58] + n<T>(20,3) + z[74];
    z[58]=z[14]*z[58];
    z[32]=z[49] + z[50] + z[62] + z[32] + z[45] + z[58];
    z[32]=z[13]*z[32];
    z[45]=z[125] + z[104];
    z[45]=z[9]*z[45];
    z[49]=z[67] + 2*z[24];
    z[49]=z[49]*z[9];
    z[49]=z[49] + z[48];
    z[50]= - z[49]*z[42];
    z[45]=z[50] + 12*z[24] + z[45];
    z[45]=z[6]*z[45];
    z[50]=17*z[21];
    z[58]= - z[50] - z[98];
    z[58]=z[9]*z[58];
    z[45]=z[45] - z[73] + z[58];
    z[45]=z[45]*z[103];
    z[58]=z[138]*z[95];
    z[62]= - z[10]*z[38];
    z[62]=z[16] + z[62];
    z[62]=z[10]*z[82]*z[62];
    z[45]=z[45] + z[62] + z[58] + z[36] + z[77] + z[109];
    z[45]=z[11]*z[45];
    z[58]= - z[19] + z[43];
    z[62]= - z[16]*z[107];
    z[58]=4*z[58] + z[62];
    z[62]=z[19] - z[111];
    z[62]=z[62]*z[128];
    z[62]=z[24] + z[62];
    z[74]=z[9]*z[78];
    z[74]=n<T>(1,3)*z[19] + z[74];
    z[74]=z[16]*z[74];
    z[62]=n<T>(1,3)*z[62] + z[74];
    z[62]=z[62]*z[110];
    z[58]=n<T>(2,3)*z[58] + z[62];
    z[58]=z[58]*z[65];
    z[52]=z[58] + z[52] + n<T>(8,3)*z[21] - z[76];
    z[52]=z[52]*z[65];
    z[58]= - z[82]*z[26];
    z[58]=z[80] + z[58];
    z[58]=z[58]*z[63];
    z[61]= - z[112] + z[61];
    z[61]=z[61]*z[116];
    z[58]=2*z[58] + z[61];
    z[35]=z[58]*z[35];
    z[58]= - 11*z[118] + z[94];
    z[58]=z[12]*z[58];
    z[35]=z[58] + z[35];
    z[35]=z[35]*z[33];
    z[35]=z[35] - z[1];
    z[58]=3*z[24];
    z[61]=z[58] + z[72];
    z[42]=z[61]*z[42];
    z[42]=z[42] - 25*z[19] - 9*z[59];
    z[42]=z[6]*z[42];
    z[61]=z[3]*z[46];
    z[61]=z[93] + z[61];
    z[42]=2*z[61] + z[42];
    z[42]=z[42]*z[103];
    z[61]=z[142] - z[100];
    z[28]=z[61]*z[28];
    z[38]=z[38] - z[36];
    z[38]=z[38]*z[95];
    z[35]=z[45] + z[42] - z[28] + z[38] + z[52] + z[16] + n<T>(2,3)*z[35];
    z[35]=z[35]*z[106];
    z[25]=z[25] + 5*z[26];
    z[25]=z[25]*z[14];
    z[25]=z[25] - 20*z[22] - 49*z[19];
    z[25]=z[25]*z[69];
    z[38]=z[100] - 8*z[36] + n<T>(22,3)*z[76];
    z[25]=z[25] - n<T>(23,3)*z[108] + 2*z[38];
    z[38]=z[105] + n<T>(26,3)*z[76];
    z[38]=z[38]*z[16];
    z[42]= - z[88] + n<T>(7,3)*z[68];
    z[42]=z[42]*z[14];
    z[38]=z[38] - z[42];
    z[42]= - 8*z[56] + z[38];
    z[42]=z[3]*z[42];
    z[42]=z[42] - z[25];
    z[42]=z[3]*z[42];
    z[45]=z[83]*z[9];
    z[45]=z[45] + z[19];
    z[52]=z[45]*z[11];
    z[25]=n<T>(8,3)*z[52] - z[25];
    z[25]=z[11]*z[25];
    z[61]=z[43] - z[30];
    z[62]=z[67] + z[24];
    z[68]=8*z[8];
    z[69]=z[68]*z[62];
    z[38]=z[69] + z[38] + n<T>(8,3)*z[61];
    z[61]=z[11] + z[3];
    z[38]=z[61]*z[38];
    z[45]= - z[3]*z[45];
    z[45]=z[45] - z[52];
    z[45]=z[2]*z[9]*z[45];
    z[38]=n<T>(8,3)*z[45] + z[38];
    z[38]=z[17]*z[38];
    z[45]=z[97] + z[100];
    z[45]=n<T>(1,3)*z[45];
    z[52]= - n<T>(4,3)*z[52] + z[45];
    z[52]=z[9]*z[52];
    z[52]=z[19] + z[52];
    z[52]=z[11]*z[52];
    z[45]=z[9]*z[45];
    z[45]=z[19] + z[45];
    z[45]=z[3]*z[45];
    z[45]=z[45] + z[52];
    z[45]=z[45]*z[47];
    z[47]=z[3]*z[62];
    z[47]=z[47] + z[53];
    z[47]=z[3]*z[47];
    z[52]=z[11]*z[53];
    z[47]=z[47] + z[52];
    z[47]=z[47]*z[68];
    z[25]=z[38] + z[47] + z[45] + z[42] + z[25];
    z[25]=z[17]*z[25];
    z[38]= - z[56]*z[140];
    z[38]=z[38] - 5*z[19] - z[92];
    z[38]=z[38]*z[65];
    z[42]= - z[30] - 7*z[40];
    z[42]=z[42]*z[123];
    z[45]=16*z[76];
    z[38]=z[42] + n<T>(46,3)*z[152] + z[38] - 35*z[21] - z[45];
    z[38]=z[3]*z[38];
    z[42]=6*z[19] + z[40];
    z[47]= - z[137]*z[60];
    z[52]= - z[67]*z[51];
    z[42]=z[52] + 3*z[42] + z[47];
    z[42]=z[3]*z[42];
    z[47]= - z[53]*z[71];
    z[27]=z[3]*z[27];
    z[27]= - z[30] + z[27];
    z[27]=z[27]*z[121];
    z[27]=z[27] + z[42] + z[70] + z[50] + z[47];
    z[27]=z[27]*z[103];
    z[30]= - z[12]*z[44];
    z[30]=z[21] + z[30];
    z[30]=z[30]*z[71];
    z[30]= - z[134] + z[30];
    z[42]= - z[57] + 11*z[152];
    z[42]=z[14]*z[42];
    z[27]=z[27] + z[38] + 2*z[30] + z[42];
    z[27]=z[6]*z[27];
    z[30]= - z[118] - z[119];
    z[30]=z[30]*z[33];
    z[30]=z[30] - z[36] + z[96] + z[76];
    z[30]=z[12]*z[30];
    z[19]=n<T>(8,3)*z[19] - z[22];
    z[26]=z[26]*z[110];
    z[19]=2*z[19] + z[26];
    z[19]=z[14]*z[19];
    z[26]=z[120] + 5*z[22];
    z[26]=z[12]*z[26];
    z[19]=z[26] + z[19];
    z[19]=z[14]*z[19];
    z[26]=8*z[152] + z[50] + z[45];
    z[26]=z[3]*z[26];
    z[33]=z[151] + n<T>(16,3)*z[9];
    z[19]=n<T>(2,3)*z[26] + z[19] + 2*z[33] + z[30];
    z[19]=z[3]*z[19];
    z[26]=n<T>(43,3)*z[21] - z[133];
    z[26]=z[16]*z[26]*z[65];
    z[26]=z[26] + 43*z[21] - n<T>(107,3)*z[36];
    z[26]=z[12]*z[26];
    z[30]=30*z[21] - 29*z[36];
    z[30]=z[16]*z[30];
    z[30]=z[30] + 10*z[66];
    z[30]=z[12]*z[30];
    z[33]=z[50] - 76*z[36];
    z[30]=n<T>(1,3)*z[33] + z[30];
    z[30]=z[14]*z[30];
    z[33]= - n<T>(37,3)*z[1] - z[114];
    z[26]=z[30] + 2*z[33] + z[26];
    z[26]=z[14]*z[26];
    z[30]= - z[66] + z[102];
    z[30]=z[14]*z[30];
    z[33]=z[63]*z[118];
    z[38]=z[117]*z[115];
    z[30]=n<T>(40,3)*z[38] - n<T>(16,3)*z[33] + 7*z[30];
    z[30]=z[10]*z[30];
    z[31]= - 5*z[31] - z[55];
    z[31]=z[14]*z[31];
    z[30]=z[30] + z[31] + z[137] + 5*z[16];
    z[30]=z[10]*z[30];
    z[31]=z[46] - z[40];
    z[31]=z[31]*z[9];
    z[33]=z[51]*z[48];
    z[31]=z[31] - z[33] - z[58];
    z[33]=z[147]*z[39];
    z[31]=3*z[31] + z[33];
    z[33]=z[7] - z[2];
    z[31]=z[4]*z[33]*z[31];
    z[20]=4*z[59] + z[20] - z[92];
    z[20]=z[20]*z[33];
    z[20]=2*z[20] + z[31];
    z[20]=z[20]*z[149];
    z[31]=z[120] - z[40];
    z[31]=z[31]*z[9];
    z[38]=z[49]*z[110];
    z[24]= - z[31] + z[38] - 8*z[24];
    z[24]=z[24]*z[65];
    z[24]=z[24] + z[73] - z[40];
    z[24]=z[24]*z[65];
    z[24]=z[24] + z[91];
    z[28]=z[28] + z[64];
    z[28]= - z[15]*z[28];
    z[24]=z[28] - z[126] - n<T>(1,3)*z[24];
    z[24]=z[33]*z[24];
    z[20]=z[20] + z[24];
    z[20]=z[5]*z[20];
    z[21]= - 70*z[36] + 100*z[21] - 91*z[76];
    z[22]=z[43] - z[22];
    z[22]=z[22]*z[122];
    z[21]=n<T>(1,3)*z[21] + z[22];
    z[21]=z[21]*z[65];
    z[21]=z[21] - 15*z[9] - 31*z[16];
    z[21]=z[12]*z[21];

    r +=  - n<T>(26,3) + z[19] + 2*z[20] + z[21] + z[23] + z[25] + z[26] + 
      z[27] + z[29] + z[30] + z[32] + z[34] + z[35] + z[37] + z[41] + 
      z[54];
 
    return r;
}

template double qg_2lNLC_r1159(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1159(const std::array<dd_real,31>&);
#endif
