#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r455(const std::array<T,31>& k) {
  T z[135];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[8];
    z[8]=k[12];
    z[9]=k[3];
    z[10]=k[5];
    z[11]=k[13];
    z[12]=k[2];
    z[13]=k[14];
    z[14]=k[9];
    z[15]=k[21];
    z[16]=npow(z[1],5);
    z[17]=2*z[16];
    z[18]=npow(z[4],2);
    z[19]=z[18]*z[17];
    z[20]= - z[16]*z[18];
    z[21]=npow(z[1],3);
    z[20]=z[21] + z[20];
    z[22]=z[10]*z[4];
    z[20]=z[20]*z[22];
    z[19]=z[20] - z[21] + z[19];
    z[19]=z[10]*z[19];
    z[20]=z[21]*z[2];
    z[23]= - z[4]*z[16];
    z[19]=z[19] + z[20] + z[23];
    z[19]=z[8]*z[19];
    z[23]=npow(z[10],2);
    z[24]=z[23]*z[7];
    z[25]=npow(z[1],4);
    z[26]=npow(z[2],2);
    z[27]= - z[3]*z[25]*z[26]*z[24];
    z[28]=z[26]*z[21];
    z[29]=z[20]*z[10];
    z[30]=z[28] - z[29];
    z[31]=2*z[10];
    z[30]=z[30]*z[31];
    z[27]=z[30] + z[27];
    z[30]=npow(z[3],3);
    z[27]=z[27]*z[30];
    z[32]=npow(z[1],2);
    z[33]=z[32]*z[2];
    z[34]=z[33] - z[21];
    z[35]=z[20] - z[25];
    z[36]=3*z[4];
    z[37]=z[35]*z[36];
    z[38]=z[25]*z[4];
    z[39]=z[21] + z[38];
    z[39]=z[4]*z[39];
    z[40]=z[2]*z[1];
    z[39]= - z[40] + z[39];
    z[41]=3*z[10];
    z[39]=z[39]*z[41];
    z[42]=z[26]*z[32];
    z[43]=z[7]*z[42];
    z[19]=z[27] + z[19] + z[43] + z[39] + 2*z[34] + z[37];
    z[19]=z[11]*z[19];
    z[27]=2*z[2];
    z[37]=z[27]*z[32];
    z[39]=2*z[25];
    z[43]=3*z[20];
    z[44]= - z[39] + z[43];
    z[44]=z[4]*z[44];
    z[45]=z[25]*z[18];
    z[46]=3*z[32];
    z[45]=z[46] + z[45];
    z[47]=z[23]*z[6];
    z[45]=z[45]*z[47];
    z[48]=5*z[21];
    z[44]=z[45] + z[44] - z[48] + z[37];
    z[44]=z[4]*z[44];
    z[45]=2*z[40];
    z[49]=z[45] - z[46];
    z[50]=z[49]*z[2];
    z[51]=2*z[32];
    z[52]=z[51] - z[40];
    z[53]= - z[52]*z[27];
    z[53]=z[21] + z[53];
    z[53]=z[2]*z[53];
    z[53]=z[25] + z[53];
    z[53]=z[4]*z[53];
    z[49]= - z[49]*z[26];
    z[49]= - z[25] + z[49];
    z[49]=z[5]*z[49];
    z[54]=z[10]*z[40];
    z[49]=z[49] - 12*z[54] + z[50] + z[53];
    z[49]=z[7]*z[49];
    z[53]=z[32]*z[10];
    z[54]=25*z[33] - 8*z[53];
    z[54]=z[10]*z[54];
    z[29]=7*z[28] - 8*z[29];
    z[55]=z[7]*z[10];
    z[29]=z[3]*z[29]*z[55];
    z[29]=z[29] - 6*z[42] + z[54];
    z[54]=npow(z[3],2);
    z[29]=z[29]*z[54];
    z[56]=z[40] - z[32];
    z[57]=z[56]*z[27];
    z[52]=z[52]*z[2];
    z[58]=z[52] - z[21];
    z[59]=z[4]*z[2];
    z[60]=2*z[59];
    z[61]=z[60]*z[58];
    z[62]=z[61] + z[48] + z[57];
    z[62]=z[5]*z[62];
    z[63]=4*z[38];
    z[64]=z[48] - z[63];
    z[64]=z[4]*z[64];
    z[65]=3*z[21];
    z[63]=z[65] + z[63];
    z[63]=z[4]*z[63];
    z[63]=z[32] + z[63];
    z[63]=z[63]*z[22];
    z[63]=z[63] - z[32] + z[64];
    z[63]=z[10]*z[63];
    z[64]=z[46]*z[2];
    z[66]=z[64] + 8*z[21];
    z[67]=z[5]*z[43];
    z[63]=z[67] + z[63] - z[66];
    z[63]=z[8]*z[63];
    z[67]=2*z[21];
    z[68]= - z[67] + 3*z[38];
    z[68]=z[4]*z[68];
    z[69]=5*z[32];
    z[68]=z[69] + z[68];
    z[68]=z[4]*z[68];
    z[68]= - 30*z[1] + z[68];
    z[68]=z[10]*z[68];
    z[19]=z[19] + z[29] + z[63] + z[49] + z[62] + z[68] - 32*z[32] + 23*
    z[40] + z[44];
    z[19]=z[11]*z[19];
    z[29]=12*z[32];
    z[44]=5*z[40];
    z[49]=z[29] - z[44];
    z[49]=z[2]*z[49];
    z[62]=7*z[21];
    z[49]= - z[61] - z[62] + z[49];
    z[49]=z[4]*z[49];
    z[61]=z[67]*z[10];
    z[63]=npow(z[6],2);
    z[68]= - z[63]*z[61];
    z[70]=7*z[32];
    z[71]=3*z[40];
    z[72]=z[70] - z[71];
    z[73]= - z[2]*z[72];
    z[74]=4*z[21];
    z[73]=z[74] + z[73];
    z[73]=z[5]*z[73];
    z[75]=z[67] - z[33];
    z[76]=z[75]*z[2];
    z[76]=z[76] - z[25];
    z[77]= - z[4] + z[5];
    z[76]=z[7]*z[76]*z[77];
    z[77]=z[10]*z[1];
    z[49]=z[76] + z[73] + z[68] - 36*z[77] + z[49] - 28*z[32] + 45*z[40]
   ;
    z[49]=z[7]*z[49];
    z[68]=z[46]*z[26];
    z[73]=z[32]*z[5];
    z[76]=npow(z[2],3);
    z[78]= - z[76]*z[73];
    z[78]= - z[68] + z[78];
    z[79]=2*z[5];
    z[78]=z[78]*z[79];
    z[78]=z[78] - z[21] - 13*z[33];
    z[78]=z[5]*z[78];
    z[80]=z[65]*z[4];
    z[81]=4*z[32];
    z[82]= - z[81] - z[80];
    z[82]=z[4]*z[82];
    z[83]=3*z[1];
    z[82]= - z[83] + z[82];
    z[82]=z[82]*z[31];
    z[82]=z[82] + z[69];
    z[82]=z[4]*z[82];
    z[82]=z[1] + z[82];
    z[82]=z[10]*z[82];
    z[84]=13*z[32];
    z[85]=4*z[40];
    z[86]=z[21]*z[4];
    z[78]=z[78] + z[82] + 20*z[86] + z[84] - z[85];
    z[78]=z[8]*z[78];
    z[82]=z[44] - z[81];
    z[82]=z[82]*z[2];
    z[82]=z[82] + z[21];
    z[87]=z[26]*z[4];
    z[88]=2*z[87];
    z[89]= - z[56]*z[88];
    z[89]=z[89] + z[82];
    z[89]=z[4]*z[89];
    z[90]=z[73]*z[26];
    z[91]= - z[64] - z[90];
    z[91]=z[91]*z[79];
    z[72]=z[91] - 2*z[72] + z[89];
    z[72]=z[5]*z[72];
    z[89]=23*z[33] - 11*z[53];
    z[89]=z[10]*z[89];
    z[89]= - 21*z[42] + z[89];
    z[89]=z[7]*z[89];
    z[91]=z[27]*z[21];
    z[92]=npow(z[7],2);
    z[93]= - z[3]*z[23]*z[92]*z[91];
    z[94]= - z[5]*z[68];
    z[89]=z[93] + z[94] + z[89];
    z[89]=z[89]*z[54];
    z[93]=z[71] - z[69];
    z[94]=z[21]*z[59];
    z[94]=z[65] + z[94];
    z[94]=z[4]*z[94];
    z[94]= - 8*z[93] + z[94];
    z[94]=z[4]*z[94];
    z[95]=z[74] + z[38];
    z[95]=z[4]*z[95];
    z[96]=z[67]*z[4];
    z[97]= - z[32] - z[96];
    z[97]=z[4]*z[97];
    z[97]= - z[1] + z[97];
    z[97]=z[97]*z[31];
    z[95]=z[97] + z[32] + z[95];
    z[95]=z[4]*z[95];
    z[97]=10*z[1];
    z[95]= - z[97] + z[95];
    z[95]=z[10]*z[95];
    z[95]= - z[51] + z[95];
    z[95]=z[6]*z[95];
    z[98]= - z[32] - z[86];
    z[98]=z[98]*z[36];
    z[99]=5*z[1];
    z[98]= - z[99] + z[98];
    z[98]=z[4]*z[98];
    z[98]=static_cast<T>(11)+ z[98];
    z[41]=z[98]*z[41];
    z[98]=31*z[1];
    z[100]=z[98] - 23*z[2];
    z[19]=z[19] + z[89] + z[78] + z[49] + z[72] + z[95] + z[41] + 2*
    z[100] + z[94];
    z[19]=z[11]*z[19];
    z[41]=2*z[6];
    z[49]= - z[77]*z[41];
    z[72]=z[47]*z[3];
    z[78]= - z[65]*z[72];
    z[89]=z[51]*z[10];
    z[78]=z[89] + z[78];
    z[78]=z[78]*z[54];
    z[72]= - z[12]*z[72];
    z[49]=z[72] + z[49] + z[78];
    z[49]=z[12]*z[49];
    z[72]=z[67] + z[64];
    z[72]=z[4]*z[72];
    z[78]=z[67]*z[5];
    z[94]=z[59]*z[78];
    z[95]=z[46]*z[4];
    z[100]=7*z[1] + z[95];
    z[100]=z[10]*z[100];
    z[49]=z[49] + z[94] + z[100] + z[72] + z[81] + z[40];
    z[49]=z[13]*z[49];
    z[72]=z[47]*z[32];
    z[94]= - z[90] - z[72] + z[33] - z[53];
    z[94]=z[11]*z[94];
    z[100]=z[5]*z[1];
    z[101]=3*z[100];
    z[102]= - z[26]*z[101];
    z[94]=z[102] + z[94];
    z[94]=z[11]*z[94];
    z[102]=3*z[2];
    z[94]=z[94] + z[1] - z[102];
    z[103]=z[28]*z[5];
    z[104]=z[103]*z[4];
    z[105]=z[75]*z[60];
    z[106]=5*z[33];
    z[105]=z[104] + z[106] + z[105];
    z[105]=z[5]*z[105];
    z[34]=z[4]*z[34];
    z[34]=z[105] - 4*z[34] + z[46] + 8*z[40];
    z[34]=z[5]*z[34];
    z[105]=z[54]*z[47]*z[81];
    z[107]=z[3]*z[6];
    z[108]=z[53]*z[107];
    z[109]=z[46]*z[107];
    z[110]=z[6]*z[31];
    z[109]=z[110] + z[109];
    z[109]=z[12]*z[109];
    z[47]=z[10] + 2*z[47];
    z[47]=z[109] + 2*z[47] + 17*z[108];
    z[47]=z[3]*z[47];
    z[108]=4*z[6];
    z[109]=z[1]*z[108];
    z[109]=static_cast<T>(5)+ z[109];
    z[47]=2*z[109] + z[100] + z[47];
    z[47]=z[12]*z[47];
    z[109]=z[71] - z[51];
    z[110]= - z[4]*z[109];
    z[111]=3*z[77];
    z[112]=z[32] + z[111];
    z[112]=z[6]*z[112];
    z[113]=z[77] + z[32];
    z[114]=z[8]*z[113];
    z[115]=z[4]*z[1];
    z[115]=static_cast<T>(3)+ z[115];
    z[115]=z[10]*z[115];
    z[34]=z[49] + z[47] + z[105] - 8*z[114] + z[34] + z[112] + z[115] + 
    z[110] + 2*z[94];
    z[34]=z[13]*z[34];
    z[47]=npow(z[2],4);
    z[49]= - z[10]*z[16]*z[47]*z[92];
    z[94]=npow(z[5],2);
    z[105]=z[107]*z[94];
    z[107]=4*z[105];
    z[110]= - npow(z[40],6)*z[107];
    z[112]= - z[8]*z[7]*npow(z[77],5);
    z[49]=z[110] + z[112] + z[49];
    z[110]=npow(z[4],3);
    z[49]=z[110]*z[49];
    z[112]=7*z[6];
    z[114]= - z[110]*z[112];
    z[115]=z[6]*z[18];
    z[115]= - 2*z[110] + 9*z[115];
    z[115]=z[5]*z[115];
    z[114]=z[114] + z[115];
    z[114]=z[79]*z[114]*npow(z[40],5);
    z[49]=z[114] + z[49];
    z[49]=z[3]*z[49];
    z[114]=npow(z[40],4);
    z[115]=z[110]*z[114];
    z[116]=npow(z[59],3);
    z[117]=3*z[25];
    z[118]=z[116]*z[117];
    z[119]= - z[10]*z[118];
    z[115]= - 8*z[115] + z[119];
    z[115]=z[115]*z[41];
    z[119]=z[18]*z[108];
    z[119]= - z[110] + z[119];
    z[120]=2*z[4];
    z[121]= - z[6]*z[120];
    z[121]=z[18] + z[121];
    z[121]=z[5]*z[121];
    z[119]=11*z[119] + 18*z[121];
    z[114]=z[5]*z[114]*z[119];
    z[119]=z[2] - z[10];
    z[119]=z[10]*z[119];
    z[119]= - z[26] + z[119];
    z[121]=z[110]*z[25];
    z[119]=z[10]*z[121]*z[119];
    z[119]= - z[118] + z[119];
    z[119]=z[10]*z[119];
    z[122]=z[76]*z[18];
    z[123]=z[122]*z[25];
    z[124]=z[7]*z[31]*z[123];
    z[119]=z[119] + z[124];
    z[119]=z[7]*z[119];
    z[124]=npow(z[10],3);
    z[125]=z[124]*z[8];
    z[126]=z[25]*z[2];
    z[92]=z[92]*z[126]*z[125];
    z[127]=z[7]*z[18];
    z[127]=z[110] + z[127];
    z[127]=z[127]*npow(z[77],4);
    z[92]=z[92] + z[127];
    z[127]=2*z[8];
    z[92]=z[92]*z[127];
    z[49]=z[49] + z[92] + z[119] + z[115] + z[114];
    z[49]=z[3]*z[49];
    z[92]=z[21]*z[10];
    z[114]=z[27]*z[92];
    z[114]=11*z[28] + z[114];
    z[115]=z[18]*z[10];
    z[114]=z[114]*z[115];
    z[119]=z[86]*z[76];
    z[128]=z[22]*z[28];
    z[129]= - z[119] - z[128];
    z[129]=z[6]*z[129];
    z[130]=z[122]*z[21];
    z[130]=35*z[130];
    z[114]=3*z[129] + z[130] + z[114];
    z[114]=z[6]*z[114];
    z[129]=z[76]*z[21];
    z[131]=z[129]*z[6];
    z[119]= - 18*z[119] + 19*z[131];
    z[119]=z[119]*z[79];
    z[132]=z[76]*z[6];
    z[133]=z[86]*z[132];
    z[119]=z[119] + z[130] - 43*z[133];
    z[119]=z[5]*z[119];
    z[130]=z[18]*z[28];
    z[133]=z[23]*z[18];
    z[134]=z[21]*z[133];
    z[130]=6*z[130] + z[134];
    z[130]=z[10]*z[130];
    z[122]= - z[65]*z[122];
    z[128]= - z[7]*z[128];
    z[122]=z[128] + z[122] + z[130];
    z[122]=z[7]*z[122];
    z[128]=z[124]*z[86];
    z[130]=z[20]*z[24];
    z[91]= - z[91] + z[92];
    z[134]=z[23]*z[8];
    z[91]=z[91]*z[134];
    z[91]=2*z[91] + 5*z[128] + 6*z[130];
    z[91]=z[7]*z[91];
    z[124]=z[18]*z[124]*z[67];
    z[91]=z[124] + z[91];
    z[91]=z[8]*z[91];
    z[124]=z[92] - z[20];
    z[128]=z[10]*z[124];
    z[128]=z[128] + z[28];
    z[128]=z[110]*z[128];
    z[130]=z[10]*z[128];
    z[116]= - z[116]*z[65];
    z[116]=z[116] + z[130];
    z[49]=z[49] + z[91] + z[122] + z[119] + 2*z[116] + z[114];
    z[49]=z[3]*z[49];
    z[91]=z[87]*z[32];
    z[114]=39*z[91];
    z[116]=z[59]*z[53];
    z[119]=z[6]*z[10];
    z[122]=z[33]*z[119];
    z[116]=z[122] - z[114] - 19*z[116];
    z[116]=z[6]*z[116];
    z[112]=z[42]*z[112];
    z[90]=38*z[90] - z[114] + z[112];
    z[90]=z[5]*z[90];
    z[112]= - z[59]*z[69];
    z[114]=z[51]*z[22];
    z[112]=z[112] + z[114];
    z[112]=z[10]*z[112];
    z[91]=8*z[91] + z[112];
    z[112]=z[53]*z[7];
    z[114]=z[27]*z[112];
    z[91]=3*z[91] + z[114];
    z[91]=z[7]*z[91];
    z[114]= - z[4]*z[23]*z[81];
    z[122]= - z[64] + z[53];
    z[122]=z[122]*z[55];
    z[114]=z[114] + n<T>(1,2)*z[122];
    z[122]=z[33] - z[89];
    z[122]=z[8]*z[122]*z[31];
    z[114]=3*z[114] + z[122];
    z[114]=z[8]*z[114];
    z[42]=z[18]*z[42];
    z[81]=z[81]*z[133];
    z[42]=z[49] + z[114] + z[91] + z[90] + z[116] + 11*z[42] + z[81];
    z[42]=z[3]*z[42];
    z[49]= - z[102] - z[88];
    z[49]=z[4]*z[49];
    z[81]=static_cast<T>(4)- z[59];
    z[81]=z[4]*z[81];
    z[81]=z[81] + z[115];
    z[81]=z[10]*z[81];
    z[49]=z[81] + n<T>(3,2) + z[49];
    z[49]=z[10]*z[49];
    z[81]=2*z[26];
    z[90]=z[76]*z[4];
    z[91]=z[81] - z[90];
    z[91]=z[91]*z[120];
    z[114]= - z[26]*z[115];
    z[91]=z[91] + z[114];
    z[91]=z[10]*z[91];
    z[114]= - z[4]*z[47];
    z[114]=4*z[76] + z[114];
    z[114]=z[4]*z[114];
    z[91]=z[91] - n<T>(13,2)*z[26] + z[114];
    z[91]=z[6]*z[91];
    z[114]=z[36] + z[115];
    z[114]=z[10]*z[114];
    z[114]= - n<T>(5,2) + z[114];
    z[114]=z[114]*z[23];
    z[116]=static_cast<T>(1)- z[22];
    z[116]=z[116]*z[31];
    z[116]= - n<T>(1,2)*z[2] + z[116];
    z[24]=z[116]*z[24];
    z[24]=z[114] + z[24];
    z[24]=z[8]*z[24];
    z[114]=6*z[26] - z[90];
    z[114]=z[4]*z[114];
    z[116]= - z[26] - z[132];
    z[122]=n<T>(1,2)*z[5];
    z[116]=z[116]*z[122];
    z[22]=z[59] - z[22];
    z[22]=z[22]*z[31];
    z[22]=z[22] + n<T>(3,2)*z[2] - z[88];
    z[22]=z[22]*z[55];
    z[22]=z[42] + z[24] + z[22] + z[116] + z[91] + z[49] - n<T>(17,2)*z[2]
    + z[114];
    z[22]=z[3]*z[22];
    z[24]=z[3]*z[47]*z[110]*z[94]*z[17];
    z[42]=z[5]*z[123];
    z[42]=z[118] - 8*z[42];
    z[42]=z[5]*z[42];
    z[47]= - z[121]*z[125];
    z[24]=z[24] + z[42] + z[47];
    z[24]=z[3]*z[24];
    z[42]= - z[26]*z[18]*z[65];
    z[42]=z[42] + 5*z[104];
    z[42]=z[5]*z[42];
    z[47]=z[133]*z[8];
    z[49]= - z[74]*z[47];
    z[24]=z[24] + z[49] + 3*z[42] - z[128];
    z[24]=z[3]*z[24];
    z[42]=5*z[4] + z[8];
    z[42]=z[127]*z[53]*z[42];
    z[49]=z[37] - 5*z[53];
    z[49]=z[18]*z[49];
    z[55]=z[59]*z[46];
    z[88]=z[33]*z[5];
    z[55]=z[55] - 7*z[88];
    z[55]=z[55]*z[79];
    z[24]=z[24] + z[42] + z[55] + z[49];
    z[24]=z[3]*z[24];
    z[42]=z[31]*z[18];
    z[49]= - static_cast<T>(3)+ z[60];
    z[49]=z[4]*z[49];
    z[49]=z[49] - z[42];
    z[49]=z[10]*z[49];
    z[42]= - z[4] - z[42];
    z[42]=z[42]*z[134];
    z[55]=z[2] + z[87];
    z[55]=z[4]*z[55];
    z[24]=z[24] + z[42] + z[49] - static_cast<T>(2)+ z[55];
    z[24]=z[3]*z[24];
    z[42]= - z[106] + 6*z[53];
    z[49]= - z[3]*z[103];
    z[42]=3*z[42] + z[49];
    z[42]=z[42]*z[54];
    z[49]= - z[92] + 4*z[20];
    z[49]=z[10]*z[49];
    z[49]= - z[28] + z[49];
    z[49]=z[49]*z[30];
    z[49]=z[49] - z[111] + z[109];
    z[49]=z[11]*z[49];
    z[55]=z[109]*z[79];
    z[42]=z[49] + z[42] + 35*z[1] + z[55];
    z[42]=z[11]*z[42];
    z[49]=z[46] + z[88];
    z[49]=z[49]*z[79];
    z[55]= - z[54]*z[64];
    z[49]=z[55] + 15*z[1] + z[49];
    z[49]=z[5]*z[49];
    z[42]=z[42] - static_cast<T>(25)+ z[49];
    z[42]=z[11]*z[42];
    z[49]=z[21]*z[5];
    z[55]= - z[49]*z[27]*z[3];
    z[60]=9*z[32];
    z[55]= - z[60] + z[55];
    z[55]=z[55]*z[54];
    z[91]=z[124]*z[30];
    z[91]=z[83] + 2*z[91];
    z[91]=z[11]*z[91];
    z[55]=z[91] + z[100] + z[55];
    z[55]=z[11]*z[55];
    z[91]=4*z[1];
    z[103]= - z[91] - z[73];
    z[103]=z[103]*z[94];
    z[104]= - z[11]*z[21];
    z[104]= - z[49] + z[104];
    z[30]=z[11]*z[30]*z[104];
    z[104]=2*z[1];
    z[106]=z[104]*npow(z[5],3);
    z[30]=z[106] + z[30];
    z[30]=z[9]*z[30];
    z[30]=z[30] + 2*z[103] + z[55];
    z[30]=z[11]*z[30];
    z[55]=static_cast<T>(1)- z[59];
    z[55]=z[4]*z[55];
    z[47]=z[47] + z[55] + z[115];
    z[47]=z[3]*z[47];
    z[30]= - z[106] + z[47] + z[30];
    z[30]=z[9]*z[30];
    z[47]=z[26]*z[83];
    z[55]= - z[1]*z[90];
    z[47]=z[47] + z[55];
    z[47]=z[47]*z[120];
    z[47]= - 9*z[40] + z[47];
    z[47]=z[4]*z[47];
    z[55]=11*z[1];
    z[103]=z[51]*z[5];
    z[47]=z[103] + z[55] + z[47];
    z[47]=z[5]*z[47];
    z[114]= - z[1]*z[87];
    z[114]=z[45] + z[114];
    z[114]=z[114]*z[18];
    z[47]=z[47] + static_cast<T>(1)+ 3*z[114];
    z[47]=z[5]*z[47];
    z[110]=z[110]*z[77];
    z[114]= - z[1]*z[18];
    z[114]=z[114] - z[110];
    z[114]=z[114]*z[31];
    z[115]=z[104]*z[8];
    z[116]=z[79] + 19*z[4];
    z[116]=z[1]*z[116];
    z[114]= - z[115] + z[114] + static_cast<T>(2)+ z[116];
    z[114]=z[8]*z[114];
    z[116]=z[4]*z[40];
    z[116]= - z[104] + z[116];
    z[116]=z[116]*z[120];
    z[116]=static_cast<T>(7)+ z[116];
    z[116]=z[4]*z[116];
    z[24]=z[30] + z[42] + z[24] + z[114] + z[47] + z[116] - 2*z[110];
    z[24]=z[9]*z[24];
    z[30]=7*z[40];
    z[42]=z[60] - z[30];
    z[42]=z[2]*z[42];
    z[47]=z[67] + z[50];
    z[47]=z[47]*z[27];
    z[47]= - z[25] + z[47];
    z[47]=z[4]*z[47];
    z[42]=z[47] - z[48] + z[42];
    z[42]=z[4]*z[42];
    z[42]=3*z[93] + z[42];
    z[42]=z[4]*z[42];
    z[47]=z[18]*z[77];
    z[48]=z[46] - z[40];
    z[50]= - z[4]*z[48];
    z[93]=8*z[1];
    z[50]= - z[93] + z[50];
    z[50]=z[4]*z[50];
    z[50]=z[50] - z[47];
    z[50]=z[10]*z[50];
    z[42]=z[50] + z[99] + z[42];
    z[42]=z[10]*z[42];
    z[50]=z[85] - z[60];
    z[85]=z[2]*z[50];
    z[114]=z[2]*z[48];
    z[114]= - z[65] + z[114];
    z[114]=z[2]*z[114];
    z[114]=z[25] + z[114];
    z[114]=z[114]*z[120];
    z[116]=6*z[21];
    z[85]=z[114] + z[116] + z[85];
    z[85]=z[4]*z[85];
    z[114]=z[67] + z[53];
    z[114]=z[114]*z[10];
    z[114]=z[114] + z[25];
    z[114]=z[114]*z[6];
    z[118]= - z[70] - z[77];
    z[118]=z[10]*z[118];
    z[118]=z[114] - z[116] + z[118];
    z[118]=z[6]*z[118];
    z[121]= - z[74] + z[33];
    z[121]=z[121]*z[79];
    z[42]=z[121] + z[118] + z[42] + z[85] - z[50];
    z[42]=z[7]*z[42];
    z[50]=z[45] - z[32];
    z[85]=z[102]*z[56];
    z[102]=z[21] + z[85];
    z[102]=z[4]*z[102];
    z[102]= - 2*z[50] + z[102];
    z[102]=z[102]*z[36];
    z[102]=28*z[1] + z[102];
    z[102]=z[4]*z[102];
    z[118]=3*z[110];
    z[121]=z[56]*z[36];
    z[121]=z[1] + z[121];
    z[121]=z[121]*z[18];
    z[121]=z[121] - z[118];
    z[121]=z[10]*z[121];
    z[102]=z[121] - n<T>(9,2) + z[102];
    z[102]=z[10]*z[102];
    z[121]=z[69] + z[45];
    z[121]=z[2]*z[121];
    z[121]= - z[74] + z[121];
    z[121]=z[4]*z[121];
    z[123]=11*z[32];
    z[121]=z[121] + z[123] - 37*z[40];
    z[121]=z[4]*z[121];
    z[124]= - z[39] - z[92];
    z[124]=z[6]*z[124];
    z[125]= - z[10]*z[46];
    z[74]=z[124] - z[74] + z[125];
    z[74]=z[6]*z[74];
    z[74]=z[74] + 8*z[32] + n<T>(9,2)*z[77];
    z[74]=z[6]*z[74];
    z[124]= - z[6]*z[16];
    z[124]=z[25] + z[124];
    z[124]=z[6]*z[124];
    z[124]=n<T>(7,2)*z[21] + z[124];
    z[124]=z[6]*z[124];
    z[125]= - z[25]*z[41];
    z[125]=z[21] + z[125];
    z[125]=z[5]*z[125];
    z[124]=z[125] + n<T>(13,2)*z[32] + z[124];
    z[124]=z[5]*z[124];
    z[42]=z[42] + z[124] + z[74] + z[102] + z[121] - n<T>(81,2)*z[1] + z[2];
    z[42]=z[7]*z[42];
    z[74]=z[32]*z[4];
    z[102]= - z[83] - z[74];
    z[102]=z[102]*z[36];
    z[102]=z[102] - z[47];
    z[102]=z[10]*z[102];
    z[121]= - 17*z[32] - z[80];
    z[121]=z[4]*z[121];
    z[102]=z[102] + 16*z[1] + z[121];
    z[102]=z[10]*z[102];
    z[121]=z[69] + z[40];
    z[124]= - z[62] - z[38];
    z[124]=z[4]*z[124];
    z[102]=z[102] + 6*z[121] + z[124];
    z[102]=z[10]*z[102];
    z[121]=z[65] + z[53];
    z[121]=z[121]*z[10];
    z[121]=z[121] + z[117];
    z[121]=z[121]*z[10];
    z[121]=z[121] + z[16];
    z[121]=z[121]*z[6];
    z[124]= - z[123] - z[77];
    z[124]=z[10]*z[124];
    z[124]= - 19*z[21] + z[124];
    z[124]=z[10]*z[124];
    z[124]=z[121] - 9*z[25] + z[124];
    z[124]=z[6]*z[124];
    z[43]= - z[25] - z[43];
    z[43]=z[43]*z[79];
    z[43]=z[43] + z[124] + z[102] + 6*z[75] + z[38];
    z[43]=z[7]*z[43];
    z[75]= - z[16]*z[41];
    z[39]=z[75] + z[39] + z[20];
    z[39]=z[5]*z[39];
    z[75]=11*z[21] - z[33];
    z[102]= - z[6]*npow(z[1],6);
    z[17]=z[17] + z[102];
    z[17]=z[6]*z[17];
    z[17]=n<T>(1,2)*z[25] + z[17];
    z[17]=z[6]*z[17];
    z[17]=z[39] + n<T>(1,2)*z[75] + z[17];
    z[17]=z[5]*z[17];
    z[39]=z[91] - z[95];
    z[39]=z[39]*z[18];
    z[39]=z[39] - z[118];
    z[39]=z[10]*z[39];
    z[75]=z[51] - z[86];
    z[75]=z[4]*z[75];
    z[75]=20*z[1] + z[75];
    z[75]=z[4]*z[75];
    z[39]=z[75] + z[39];
    z[39]=z[10]*z[39];
    z[75]=z[4]*z[123];
    z[39]=z[39] - n<T>(49,2)*z[1] + z[75];
    z[39]=z[10]*z[39];
    z[75]= - z[65] - z[89];
    z[31]=z[75]*z[31];
    z[75]= - z[117] - z[92];
    z[75]=z[10]*z[75];
    z[75]= - 3*z[16] + z[75];
    z[75]=z[6]*z[75];
    z[31]=z[31] + z[75];
    z[31]=z[6]*z[31];
    z[75]=45*z[32] + 11*z[77];
    z[75]=z[10]*z[75];
    z[75]=39*z[21] + z[75];
    z[31]=n<T>(1,2)*z[75] + z[31];
    z[31]=z[6]*z[31];
    z[75]=11*z[40];
    z[92]= - 41*z[32] - z[75];
    z[17]=z[43] + z[17] + z[31] + z[39] + n<T>(1,2)*z[92] - z[96];
    z[17]=z[7]*z[17];
    z[31]=z[77] + z[51];
    z[31]=z[4]*z[31];
    z[31]=z[1] + z[31];
    z[31]=z[10]*z[31];
    z[31]=z[31] + z[86] - z[32] - z[71];
    z[31]=z[10]*z[31];
    z[39]= - z[4]*z[77];
    z[39]=z[39] + z[104] - z[95];
    z[39]=z[10]*z[39];
    z[43]=z[46] + z[40];
    z[39]=z[39] + 2*z[43] - z[80];
    z[39]=z[10]*z[39];
    z[38]=z[39] + z[116] - z[38];
    z[38]=z[10]*z[38];
    z[39]= - z[5]*z[126];
    z[35]=z[39] - z[121] - 2*z[35] + z[38];
    z[35]=z[7]*z[35];
    z[38]=z[5]*z[20];
    z[31]=z[35] + z[38] + z[114] - z[21] + z[31];
    z[31]=z[7]*z[31];
    z[35]=z[77] - z[32];
    z[31]=z[31] - z[88] - z[35];
    z[31]=z[31]*z[127];
    z[38]= - z[32]*z[76];
    z[38]=z[38] - z[131];
    z[38]=z[5]*z[38];
    z[39]=z[21] - z[64];
    z[39]=z[2]*z[39];
    z[43]= - z[28]*z[108];
    z[38]=z[38] + z[43] + z[25] + z[39];
    z[39]=z[79]*z[6];
    z[38]=z[38]*z[39];
    z[16]=z[16] + z[126];
    z[16]=z[6]*z[16];
    z[16]=z[16] - z[25] - 14*z[20];
    z[16]=z[6]*z[16];
    z[43]=z[65] - 31*z[33];
    z[16]=n<T>(1,2)*z[43] + z[16];
    z[16]=z[6]*z[16];
    z[16]=z[38] - 10*z[32] + z[16];
    z[16]=z[5]*z[16];
    z[38]=5*z[110];
    z[43]=z[91] + z[95];
    z[18]=z[43]*z[18];
    z[18]=2*z[18] + z[38];
    z[18]=z[10]*z[18];
    z[43]=z[4]*z[69];
    z[43]= - 43*z[1] + z[43];
    z[43]=z[4]*z[43];
    z[18]=z[18] + n<T>(1,2) + z[43];
    z[18]=z[10]*z[18];
    z[43]=z[61] + z[117] + z[20];
    z[43]=z[6]*z[43];
    z[43]=z[43] - z[66];
    z[43]=z[6]*z[43];
    z[61]= - 31*z[32] - 25*z[40];
    z[43]=z[43] + n<T>(1,2)*z[61] - 12*z[77];
    z[43]=z[6]*z[43];
    z[16]=z[31] + z[17] + z[16] + z[43] + z[18] + z[1] - 36*z[74];
    z[16]=z[8]*z[16];
    z[17]= - z[2]*z[50];
    z[17]=z[21] + z[17];
    z[17]=z[6]*z[17];
    z[17]= - 3*z[56] + z[17];
    z[17]=z[6]*z[17];
    z[17]= - n<T>(47,2)*z[1] + z[17];
    z[17]=z[6]*z[17];
    z[18]= - z[32] - z[45];
    z[31]= - z[26]*z[99];
    z[43]= - z[1]*z[132];
    z[31]=z[31] + z[43];
    z[31]=z[6]*z[31];
    z[18]=2*z[18] + z[31];
    z[18]=z[5]*z[18];
    z[31]=z[40] + z[32];
    z[41]=z[31]*z[41];
    z[18]=z[41] + z[18];
    z[18]=z[5]*z[18];
    z[17]=z[18] - n<T>(3,2) + z[17];
    z[17]=z[5]*z[17];
    z[18]= - z[6]*z[25];
    z[18]= - z[67] + z[18];
    z[18]=z[6]*z[18];
    z[18]=n<T>(11,2)*z[32] + z[18];
    z[18]=z[6]*z[18];
    z[41]= - z[6]*z[67];
    z[41]= - z[32] + z[41];
    z[41]=z[5]*z[41];
    z[43]=9*z[1];
    z[18]=z[41] - z[43] + z[18];
    z[18]=z[5]*z[18];
    z[41]=z[21] + z[53];
    z[41]=z[6]*z[41];
    z[41]=z[41] - z[46] - z[77];
    z[41]=z[6]*z[41];
    z[61]=z[46]*z[5];
    z[41]= - z[61] + z[104] + z[41];
    z[41]=z[7]*z[41];
    z[64]=z[21]*z[6];
    z[64]=z[64] + z[51];
    z[64]=z[64]*z[6];
    z[66]= - n<T>(21,2)*z[1] - z[64];
    z[66]=z[6]*z[66];
    z[18]=z[41] + z[18] - static_cast<T>(3)+ z[66];
    z[18]=z[7]*z[18];
    z[41]=z[28]*z[105];
    z[37]= - z[37] - z[53];
    z[37]=z[37]*z[63];
    z[66]=z[6]*z[33];
    z[66]=n<T>(21,2)*z[66] - 20*z[88];
    z[66]=z[5]*z[66];
    z[74]= - z[6]*z[112];
    z[37]= - 20*z[41] + z[74] + z[37] + z[66];
    z[37]=z[3]*z[37];
    z[41]=n<T>(9,2)*z[2] - 4*z[10];
    z[41]=z[6]*z[41];
    z[66]=z[6]*z[26];
    z[66]=z[2] + z[66];
    z[66]=z[5]*z[66];
    z[37]=z[37] + z[66] + n<T>(7,2) + z[41];
    z[37]=z[3]*z[37];
    z[41]=z[32]*z[63];
    z[66]=z[6]*z[32];
    z[66]= - n<T>(9,2)*z[66] + 4*z[73];
    z[66]=z[5]*z[66];
    z[20]=z[20]*z[107];
    z[20]=z[20] + z[41] + z[66];
    z[20]=z[3]*z[20];
    z[41]=z[6]*z[2];
    z[41]= - static_cast<T>(1)- z[41];
    z[41]=z[41]*z[122];
    z[20]=z[20] - z[6] + z[41];
    z[20]=z[3]*z[20];
    z[41]=z[7]*z[32];
    z[41]=z[41] - z[104];
    z[41]=z[63]*z[41];
    z[64]= - z[103] + n<T>(11,2)*z[1] - z[64];
    z[64]=z[5]*z[6]*z[64];
    z[41]=z[64] + z[41];
    z[41]=z[7]*z[41];
    z[64]=z[6]*z[31];
    z[64]=z[83] + z[64];
    z[64]=z[64]*z[63];
    z[39]=z[1]*z[39];
    z[39]=z[64] + z[39];
    z[39]=z[5]*z[39];
    z[20]=z[20] + z[39] + z[41];
    z[20]=z[12]*z[20];
    z[39]=z[94]*z[54]*z[69];
    z[39]=z[106] + z[39];
    z[39]=z[9]*z[39];
    z[41]= - z[56]*z[63];
    z[41]= - n<T>(35,2) + z[41];
    z[41]=z[6]*z[41];
    z[17]=z[20] + z[39] + z[37] + z[18] + z[41] + z[17];
    z[17]=z[12]*z[17];
    z[18]=z[100]*z[9];
    z[18]=z[18] - z[103];
    z[20]= - z[46] + z[49];
    z[20]=z[7]*z[20];
    z[37]=z[7]*z[73];
    z[37]= - z[100] + z[37];
    z[39]=2*z[12];
    z[37]=z[37]*z[39];
    z[20]=z[37] + z[20] - z[1] + z[18];
    z[20]=z[12]*z[20];
    z[37]= - z[21] + z[53];
    z[37]=z[7]*z[37];
    z[23]=z[23]*z[1];
    z[23]=z[23] - z[72];
    z[41]= - z[8]*z[23];
    z[53]=z[6]*z[89];
    z[64]=z[8]*z[77];
    z[64]=z[1] + z[64];
    z[64]=z[9]*z[64];
    z[20]=z[20] + z[64] + z[41] + z[37] - z[77] + z[53];
    z[20]=z[14]*z[20];
    z[37]=z[5]*z[65];
    z[41]=z[7]*z[21];
    z[35]=z[41] + z[37] - z[35];
    z[35]=z[7]*z[35];
    z[37]=z[69] - 2*z[77];
    z[37]=z[6]*z[37];
    z[20]=z[20] + z[35] - 8*z[73] - z[93] + z[37];
    z[35]=z[51] - 9*z[77];
    z[35]=z[35]*z[119];
    z[37]=3*z[11];
    z[23]=z[23]*z[37];
    z[23]=z[23] + z[78] + z[35] - z[69] - 18*z[77];
    z[23]=z[11]*z[23];
    z[35]= - 3*z[113] - z[49];
    z[35]=z[35]*z[37];
    z[35]=z[35] + z[43] - z[103];
    z[35]=z[11]*z[35];
    z[18]=z[83] - z[18];
    z[18]=z[9]*z[18]*npow(z[11],2);
    z[18]=3*z[18] + z[35] + z[101] + z[115];
    z[18]=z[9]*z[18];
    z[29]=z[29] + 7*z[77];
    z[29]=z[10]*z[29];
    z[35]= - z[67]*z[119];
    z[29]=z[35] + z[62] + z[29];
    z[29]=z[6]*z[29];
    z[35]= - z[69] - z[111];
    z[29]=2*z[35] + z[29];
    z[29]=z[8]*z[29];
    z[35]=z[12]*z[7];
    z[35]=5*z[35] - 5;
    z[35]=z[100]*z[35];
    z[37]= - z[1] + z[61];
    z[41]= - z[7]*z[49];
    z[37]=2*z[37] + z[41];
    z[37]=z[7]*z[37];
    z[41]= - z[6]*z[83];
    z[35]=z[37] + z[41] + z[35];
    z[35]=z[35]*z[39];
    z[18]=z[35] + z[18] + z[23] + z[29] + 2*z[20];
    z[18]=z[14]*z[18];
    z[20]=z[31]*z[2];
    z[23]= - z[10]*z[50];
    z[23]=z[23] + z[21] - z[20];
    z[23]=z[6]*z[23];
    z[23]=z[23] - z[45] - z[113];
    z[23]=z[6]*z[23];
    z[29]=z[58]*z[2];
    z[35]= - z[6]*z[29];
    z[35]=z[85] + z[35];
    z[35]=z[6]*z[35];
    z[35]=z[45] + z[35];
    z[35]=z[5]*z[35];
    z[28]=z[10]*z[28];
    z[28]=z[129] + z[28];
    z[28]=z[3]*z[28]*z[63];
    z[33]=z[10]*z[33];
    z[33]=z[68] + z[33];
    z[33]=z[6]*z[33];
    z[28]=z[33] + z[28];
    z[28]=z[28]*z[54];
    z[33]=z[13]*z[113];
    z[37]= - static_cast<T>(1)- z[100];
    z[37]=z[9]*z[37];
    z[23]=2*z[33] + z[37] + z[28] + z[35] + z[27] + z[23];
    z[23]=z[15]*z[23];
    z[27]= - z[69] + z[30];
    z[27]=z[2]*z[27];
    z[27]=z[21] + z[27];
    z[27]=z[4]*z[27];
    z[28]=z[50]*z[36];
    z[28]= - z[1] + z[28];
    z[28]=z[10]*z[28];
    z[33]=z[67] - z[52];
    z[33]=z[6]*z[33];
    z[27]=z[33] + z[28] + z[27] + z[51] - z[30];
    z[27]=z[6]*z[27];
    z[28]=43*z[32] - 71*z[40];
    z[28]=z[2]*z[28];
    z[30]= - z[60] + z[75];
    z[30]=z[2]*z[30];
    z[30]=z[67] + z[30];
    z[33]=4*z[59];
    z[30]=z[30]*z[33];
    z[28]=z[30] - z[116] + z[28];
    z[28]=z[4]*z[28];
    z[28]=z[28] + z[51] + 29*z[40];
    z[28]=z[4]*z[28];
    z[30]=z[2]*z[56];
    z[30]=z[21] + 9*z[30];
    z[30]=z[30]*z[120];
    z[30]=z[30] + z[69] - 16*z[40];
    z[30]=z[4]*z[30];
    z[30]=13*z[1] + z[30];
    z[30]=z[4]*z[30];
    z[30]= - z[47] + static_cast<T>(3)+ z[30];
    z[30]=z[10]*z[30];
    z[27]=z[27] + z[30] + z[28] - 44*z[1] + 25*z[2];
    z[27]=z[6]*z[27];
    z[28]=18*z[40];
    z[30]=z[123] - z[28];
    z[30]=z[2]*z[30];
    z[30]= - z[67] + z[30];
    z[30]=z[30]*z[26];
    z[35]=z[82]*z[90];
    z[30]=z[30] + z[35];
    z[30]=z[4]*z[30];
    z[35]= - z[84] + 30*z[40];
    z[35]=z[35]*z[26];
    z[30]=z[35] + z[30];
    z[30]=z[30]*z[120];
    z[35]=3*z[26];
    z[31]= - z[31]*z[35];
    z[35]=z[58]*z[87];
    z[31]=z[31] - 5*z[35];
    z[31]=z[6]*z[31];
    z[35]=40*z[40];
    z[36]= - z[46] - z[35];
    z[36]=z[2]*z[36];
    z[30]=z[31] + z[30] + z[67] + z[36];
    z[30]=z[6]*z[30];
    z[31]= - z[76]*z[51];
    z[36]= - z[58]*z[90];
    z[31]=z[31] + z[36];
    z[31]=z[6]*z[31];
    z[36]= - z[48]*z[81];
    z[26]=z[1]*z[26];
    z[26]= - z[21] + z[26];
    z[26]=z[26]*z[87];
    z[26]=z[31] + z[36] + z[26];
    z[26]=z[6]*z[26];
    z[26]=z[57] + z[26];
    z[26]=z[5]*z[26];
    z[31]=z[32] - z[44];
    z[31]=z[31]*z[81];
    z[36]= - z[32] + z[71];
    z[36]=z[36]*z[90];
    z[31]=z[31] + z[36];
    z[31]=z[4]*z[31];
    z[36]= - z[51] + 15*z[40];
    z[36]=z[2]*z[36];
    z[31]=z[36] + z[31];
    z[31]=z[4]*z[31];
    z[36]= - z[32] - z[44];
    z[31]=2*z[36] + z[31];
    z[26]=z[26] + 2*z[31] + z[30];
    z[26]=z[5]*z[26];
    z[30]=57*z[32] - 92*z[40];
    z[30]=z[2]*z[30];
    z[21]=10*z[21];
    z[30]= - z[21] + z[30];
    z[30]=z[2]*z[30];
    z[31]= - 29*z[32] + 36*z[40];
    z[31]=z[2]*z[31];
    z[31]=z[62] + z[31];
    z[31]=z[31]*z[87];
    z[30]=z[30] + z[31];
    z[30]=z[4]*z[30];
    z[31]= - z[46] + 56*z[40];
    z[31]=z[2]*z[31];
    z[21]=z[30] - z[21] + z[31];
    z[21]=z[4]*z[21];
    z[30]= - z[58]*z[59];
    z[20]= - z[20] + z[30];
    z[25]=z[25] - z[29];
    z[25]=z[6]*z[25];
    z[20]=5*z[20] + z[25];
    z[20]=z[6]*z[20];
    z[25]= - 19*z[32] + z[71];
    z[20]=z[20] + n<T>(3,2)*z[25] + z[21];
    z[20]=z[6]*z[20];
    z[21]=z[60] - z[35];
    z[21]=z[2]*z[21];
    z[25]= - z[70] + z[28];
    z[25]=z[25]*z[87];
    z[21]=z[21] + z[25];
    z[21]=z[4]*z[21];
    z[25]=z[32] + 6*z[40];
    z[21]=4*z[25] + z[21];
    z[21]=z[4]*z[21];
    z[20]=z[26] + z[20] + z[21] - z[55] + n<T>(39,2)*z[2];
    z[20]=z[5]*z[20];
    z[21]=z[109]*z[33];
    z[21]= - 9*z[50] + z[21];
    z[21]=z[4]*z[21];
    z[25]= - z[98] + 6*z[2];
    z[21]=2*z[25] + z[21];
    z[21]=z[4]*z[21];
    z[25]=6*z[32] - z[44];
    z[25]=z[4]*z[25];
    z[25]=z[97] + z[25];
    z[25]=z[4]*z[25];
    z[25]= - static_cast<T>(8)+ z[25];
    z[25]=z[4]*z[25];
    z[25]=z[25] + z[38];
    z[25]=z[10]*z[25];

    r += n<T>(31,2) + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22]
       + 4*z[23] + z[24] + z[25] + z[27] + z[34] + z[42];
 
    return r;
}

template double qg_2lNLC_r455(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r455(const std::array<dd_real,31>&);
#endif
