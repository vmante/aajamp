#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1287(const std::array<T,31>& k) {
  T z[168];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[10];
    z[9]=k[15];
    z[10]=k[5];
    z[11]=k[6];
    z[12]=k[14];
    z[13]=k[26];
    z[14]=k[2];
    z[15]=k[3];
    z[16]=k[4];
    z[17]=k[29];
    z[18]=k[16];
    z[19]=npow(z[1],4);
    z[20]=z[19]*z[10];
    z[21]=npow(z[1],5);
    z[22]=z[20] - z[21];
    z[23]=10*z[5];
    z[24]= - z[22]*z[23];
    z[25]=26*z[19];
    z[26]=npow(z[1],3);
    z[27]=z[26]*z[10];
    z[24]=z[24] - z[25] + 25*z[27];
    z[24]=z[5]*z[24];
    z[28]=npow(z[1],2);
    z[29]=z[28]*z[10];
    z[24]=z[24] + 27*z[26] - 19*z[29];
    z[30]=4*z[5];
    z[24]=z[24]*z[30];
    z[31]=3*z[28];
    z[32]=z[10]*z[1];
    z[33]=z[31] + 4*z[32];
    z[33]=z[10]*z[33];
    z[33]= - z[26] + z[33];
    z[34]=2*z[28];
    z[35]=z[34] + z[32];
    z[36]=z[35]*z[10];
    z[37]=z[36] + z[26];
    z[38]=11*z[13];
    z[37]=z[37]*z[38];
    z[39]= - z[10]*z[37];
    z[33]=3*z[33] + z[39];
    z[33]=z[13]*z[33];
    z[39]=2*z[32];
    z[40]=z[15]*z[1];
    z[24]=z[33] + 13*z[40] + z[24] - 43*z[28] - z[39];
    z[24]=z[4]*z[24];
    z[33]=3*z[29];
    z[41]= - 13*z[26] + z[33];
    z[41]=z[10]*z[41];
    z[42]=z[27] - z[19];
    z[42]=z[42]*z[10];
    z[42]=z[42] + z[21];
    z[43]=5*z[5];
    z[44]= - z[42]*z[43];
    z[45]=13*z[19];
    z[41]=z[44] + z[45] + z[41];
    z[41]=z[41]*z[30];
    z[44]=6*z[28];
    z[46]=z[44] + z[32];
    z[46]=z[10]*z[46];
    z[41]=z[41] - 51*z[26] + 4*z[46];
    z[41]=z[5]*z[41];
    z[46]=npow(z[1],6);
    z[47]= - z[46]*z[43];
    z[48]=13*z[21];
    z[47]=z[48] + z[47];
    z[47]=z[5]*z[47];
    z[47]= - z[45] + z[47];
    z[47]=z[5]*z[47];
    z[49]=5*z[26];
    z[47]=z[49] + z[47];
    z[50]=5*z[28];
    z[51]= - z[50] - z[40];
    z[51]=z[15]*z[51];
    z[47]=4*z[47] + z[51];
    z[47]=z[4]*z[47];
    z[51]=2*z[26];
    z[52]=z[51] + z[29];
    z[53]=2*z[52];
    z[54]=3*z[26];
    z[55]=z[54] + z[29];
    z[55]=z[55]*z[10];
    z[55]=z[55] + 3*z[19];
    z[56]= - z[12]*z[55];
    z[56]=z[53] + z[56];
    z[57]=4*z[12];
    z[56]=z[56]*z[57];
    z[58]=2*z[40];
    z[41]=z[56] + z[47] + z[58] + 4*z[35] + z[41];
    z[47]=2*z[3];
    z[41]=z[41]*z[47];
    z[56]=2*z[12];
    z[59]=z[56]*z[19];
    z[60]=2*z[19];
    z[61]= - z[13]*z[21];
    z[61]=z[60] + z[61];
    z[61]=z[13]*z[61];
    z[61]=z[59] - z[49] + z[61];
    z[61]=z[61]*z[56];
    z[62]=z[19]*z[13];
    z[62]=z[62] - z[26];
    z[63]=2*z[13];
    z[64]=z[62]*z[63];
    z[61]=z[61] + z[64] + z[31] + z[58];
    z[64]=npow(z[13],2);
    z[65]=z[26]*z[64];
    z[65]=z[1] + z[65];
    z[66]=z[26]*z[57];
    z[67]=7*z[28];
    z[66]= - z[67] + z[66];
    z[66]=z[12]*z[66];
    z[65]=2*z[65] + z[66];
    z[66]=3*z[11];
    z[68]=npow(z[15],2);
    z[69]=z[66]*z[68];
    z[70]=z[13]*z[28];
    z[70]=z[1] + z[70];
    z[70]=z[13]*z[70];
    z[71]=z[64]*z[16];
    z[72]=z[1]*z[71];
    z[70]=z[70] + z[72];
    z[70]=z[16]*z[70];
    z[65]=4*z[70] + 2*z[65] - z[69];
    z[65]=z[16]*z[65];
    z[70]= - z[34] + z[40];
    z[70]=z[15]*z[70];
    z[72]= - z[12]*z[21];
    z[60]=z[60] + z[72];
    z[60]=z[60]*z[57];
    z[60]=z[60] - z[54] + z[70];
    z[60]=z[60]*z[47];
    z[70]=npow(z[11],2);
    z[72]=z[70]*z[28];
    z[73]=z[72]*z[68];
    z[60]=z[65] + z[60] + 2*z[61] - z[73];
    z[60]=z[8]*z[60];
    z[61]=2*z[10];
    z[65]=z[61] - z[15];
    z[65]=z[65]*z[15];
    z[74]=npow(z[10],2);
    z[65]=z[65] - z[74];
    z[75]=z[15]*z[10];
    z[76]=2*z[74];
    z[75]=z[75] - z[76];
    z[75]=z[75]*z[15];
    z[77]=npow(z[10],3);
    z[75]=z[75] + z[77];
    z[75]=z[75]*z[4];
    z[75]=z[75] - z[65];
    z[78]=7*z[29];
    z[79]=z[28]*z[15];
    z[80]=z[78] + z[79];
    z[80]=z[80]*z[15];
    z[81]=z[74]*z[28];
    z[80]=z[80] - 8*z[81];
    z[82]=z[11]*z[4];
    z[80]=z[80]*z[82];
    z[75]= - z[80] + 3*z[75];
    z[75]=z[75]*z[11];
    z[80]=z[26]*z[5];
    z[83]=z[80]*z[74];
    z[84]=z[26] - 12*z[29];
    z[84]=z[10]*z[84];
    z[84]=z[84] + 19*z[83];
    z[85]=2*z[5];
    z[84]=z[84]*z[85];
    z[84]=z[84] - z[54] + 14*z[29];
    z[84]=z[5]*z[84];
    z[86]= - z[81] + z[83];
    z[87]=22*z[5];
    z[86]=z[86]*z[87];
    z[88]=12*z[28];
    z[89]=11*z[32];
    z[90]=z[88] - z[89];
    z[90]=z[10]*z[90];
    z[91]=12*z[26];
    z[86]=z[86] + z[91] + z[90];
    z[86]=z[13]*z[86];
    z[90]=z[28] - z[39];
    z[84]=z[86] + 6*z[90] + z[84];
    z[84]=z[13]*z[84];
    z[55]= - z[13]*z[55];
    z[53]=z[53] + z[55];
    z[53]=z[53]*z[63];
    z[55]= - z[7]*z[81];
    z[55]=z[91] + z[55];
    z[55]=z[12]*z[55];
    z[86]=16*z[28];
    z[90]=z[74]*z[1];
    z[92]= - z[7]*z[90];
    z[53]=z[55] + z[53] - z[86] + z[92];
    z[53]=z[53]*z[56];
    z[55]=2*z[15];
    z[92]=z[55] - z[10];
    z[66]=z[92]*z[66];
    z[92]=23*z[28];
    z[93]=7*z[32];
    z[94]=z[92] - z[93];
    z[94]=z[13]*z[94];
    z[95]=10*z[1];
    z[94]= - z[95] + z[94];
    z[94]=z[13]*z[94];
    z[96]=3*z[1];
    z[97]= - z[96]*z[71];
    z[94]=z[97] + z[94] + z[66];
    z[94]=z[16]*z[94];
    z[97]=z[74]*z[26];
    z[98]=npow(z[5],2);
    z[99]=z[97]*z[98];
    z[100]=3*z[32];
    z[101]=z[100] - z[28];
    z[102]=z[101]*z[61];
    z[102]= - 2*z[99] + z[26] + z[102];
    z[102]=z[5]*z[102];
    z[103]=z[32] - z[28];
    z[102]=2*z[103] + z[102];
    z[102]=z[7]*z[102];
    z[104]=7*z[1];
    z[105]=z[28]*z[5];
    z[106]=2*z[7];
    z[107]=z[106]*z[1];
    z[108]=static_cast<T>(1)+ z[107];
    z[108]=z[15]*z[108];
    z[24]=z[60] + z[94] + z[41] - z[75] + z[53] + z[24] + z[84] + z[108]
    + z[102] - z[104] + z[105];
    z[24]=z[2]*z[24];
    z[41]=5*z[7];
    z[22]=z[22]*z[41];
    z[22]=z[22] + z[45] - 10*z[27];
    z[53]=4*z[7];
    z[22]=z[22]*z[53];
    z[60]=28*z[28] - z[32];
    z[60]=z[10]*z[60];
    z[22]=z[22] - 56*z[26] + z[60];
    z[22]=z[7]*z[22];
    z[60]=z[39] + z[28];
    z[84]=z[60]*z[61];
    z[94]=z[32] + z[28];
    z[102]=3*z[94];
    z[108]=z[102]*z[10];
    z[108]=z[108] + z[26];
    z[108]=z[108]*z[10];
    z[109]= - z[12]*z[108];
    z[109]=z[84] + z[109];
    z[109]=z[109]*z[57];
    z[110]=npow(z[32],3);
    z[111]=z[110]*z[56];
    z[112]=npow(z[32],4);
    z[113]=z[112]*z[11];
    z[114]=npow(z[12],2);
    z[115]= - z[114]*z[113];
    z[115]=z[111] + z[115];
    z[116]=4*z[11];
    z[115]=z[115]*z[116];
    z[115]= - 3*z[81] + z[115];
    z[115]=z[115]*z[70];
    z[117]=z[86] - z[93];
    z[46]=z[46]*z[41];
    z[48]=z[48] - z[46];
    z[48]=z[7]*z[48];
    z[48]= - z[45] + z[48];
    z[48]=z[7]*z[48];
    z[118]=6*z[26];
    z[48]=z[118] + z[48];
    z[48]=z[4]*z[48];
    z[22]=z[115] + z[109] + 4*z[48] + 2*z[117] + z[22];
    z[22]=z[22]*z[47];
    z[48]= - z[64]*z[96];
    z[109]=z[11]*z[64]*z[27];
    z[115]= - z[13]*z[34];
    z[109]=z[115] + z[109];
    z[115]=4*z[70];
    z[109]=z[109]*z[115];
    z[117]=z[4]*z[1];
    z[119]= - z[117] + z[72];
    z[119]=z[119]*z[47];
    z[120]=npow(z[11],3);
    z[121]=z[120]*z[26];
    z[122]=z[121]*z[71];
    z[48]= - 4*z[122] + z[119] + z[48] + z[109];
    z[48]=z[16]*z[48];
    z[109]=z[61]*z[28];
    z[119]=z[13]*z[109];
    z[122]=z[97]*z[11];
    z[123]= - z[64]*z[122];
    z[119]=z[119] + z[123];
    z[119]=z[119]*z[115];
    z[123]=z[109]*z[70];
    z[124]=z[28]*z[4];
    z[125]= - z[96] - z[124];
    z[125]=3*z[125] + z[123];
    z[125]=z[125]*z[47];
    z[126]=z[67] - 23*z[32];
    z[126]=z[13]*z[126];
    z[127]=4*z[1];
    z[126]=z[127] + z[126];
    z[126]=z[13]*z[126];
    z[128]=z[5]*z[1];
    z[129]=2*z[128];
    z[130]=z[129] - 5;
    z[131]= - z[4]*z[96];
    z[48]=z[48] + z[125] + z[119] + z[131] + z[126] + z[130];
    z[48]=z[16]*z[48];
    z[119]=z[29] - z[26];
    z[125]=z[119]*z[10];
    z[126]=z[125] + 3*z[83];
    z[131]=z[5]*z[126];
    z[132]= - z[32] + 4*z[28];
    z[133]= - z[132]*z[61];
    z[131]=z[131] + z[54] + z[133];
    z[131]=z[131]*z[85];
    z[91]= - z[91] + z[29];
    z[91]=z[10]*z[91];
    z[91]=z[91] - 8*z[83];
    z[91]=z[5]*z[91];
    z[133]=20*z[7];
    z[134]= - z[83]*z[133];
    z[91]=z[91] + z[134];
    z[91]=z[91]*z[106];
    z[91]=z[91] - z[32] + z[131];
    z[91]=z[7]*z[91];
    z[126]=z[126]*z[85];
    z[131]=14*z[28];
    z[134]= - z[131] + z[100];
    z[134]=z[10]*z[134];
    z[126]=z[134] + z[126];
    z[126]=z[5]*z[126];
    z[134]=z[94]*z[10];
    z[135]=z[27] + z[83];
    z[135]=z[135]*z[87];
    z[136]=11*z[26];
    z[135]=z[135] - z[136] + 12*z[134];
    z[135]=z[13]*z[135];
    z[137]=17*z[32];
    z[126]=z[135] + z[126] - z[50] - z[137];
    z[126]=z[13]*z[126];
    z[135]=26*z[26];
    z[138]=z[135] - z[29];
    z[138]=z[10]*z[138];
    z[139]=10*z[7];
    z[42]=z[42]*z[139];
    z[25]=z[42] - z[25] + z[138];
    z[25]=z[7]*z[25];
    z[42]= - z[67] - z[32];
    z[42]=z[10]*z[42];
    z[25]=z[25] + z[135] + z[42];
    z[25]=z[25]*z[53];
    z[42]=z[31] - z[32];
    z[42]=z[10]*z[42];
    z[42]=4*z[26] + z[42];
    z[52]= - z[10]*z[52];
    z[52]= - z[19] + z[52];
    z[52]=z[52]*z[38];
    z[42]=3*z[42] + z[52];
    z[42]=z[13]*z[42];
    z[52]=13*z[28];
    z[135]= - z[52] - z[32];
    z[25]=z[42] + 3*z[135] + z[25];
    z[25]=z[4]*z[25];
    z[42]= - z[13]*z[108];
    z[42]=z[84] + z[42];
    z[42]=z[42]*z[63];
    z[84]=z[10]*z[80];
    z[84]=8*z[90] + z[84];
    z[84]=z[12]*z[84];
    z[108]=9*z[32];
    z[42]=z[84] + z[42] - z[108] + z[80];
    z[42]=z[42]*z[56];
    z[84]=3*z[12];
    z[135]= - z[63] - z[84];
    z[135]=z[81]*z[135];
    z[138]= - z[12]*z[113];
    z[138]=z[138] + z[110];
    z[138]=z[64]*z[138];
    z[140]=z[13] + z[12];
    z[140]=z[140]*z[111];
    z[138]=z[140] + z[138];
    z[138]=z[11]*z[138];
    z[135]=z[138] + z[135];
    z[135]=z[135]*z[115];
    z[138]=z[34] - z[32];
    z[138]= - z[138]*z[85];
    z[22]=z[48] + z[22] + z[135] + z[42] + z[25] + z[126] + z[91] + 
    z[138] - z[1] + z[61];
    z[22]=z[9]*z[22];
    z[25]=z[54]*z[12];
    z[42]=z[67] - z[25];
    z[42]=z[12]*z[42];
    z[42]= - z[127] + z[42];
    z[48]=z[70]*z[33];
    z[42]=2*z[42] + z[48];
    z[42]=z[16]*z[42];
    z[48]=z[56]*z[26];
    z[91]=z[48] - z[34];
    z[126]=z[120]*z[97];
    z[135]=z[126] - z[32] - z[91];
    z[135]=z[16]*z[135];
    z[138]=z[90] + z[26];
    z[135]=z[135] - z[138];
    z[140]=3*z[18];
    z[135]=z[135]*z[140];
    z[141]=z[81]*z[70];
    z[142]= - z[7]*z[138];
    z[42]=z[135] + z[42] + z[141] + z[142] - z[48] + z[132];
    z[42]=z[18]*z[42];
    z[132]= - z[60]*z[77]*z[43];
    z[135]= - z[50] - z[89];
    z[76]=z[135]*z[76];
    z[76]=z[76] + z[132];
    z[76]=z[76]*z[30];
    z[132]=30*z[28];
    z[135]= - z[132] - 77*z[32];
    z[135]=z[10]*z[135];
    z[76]=z[76] - z[26] + z[135];
    z[76]=z[5]*z[76];
    z[135]=11*z[19];
    z[142]= - z[135] - 5*z[27];
    z[20]=2*z[21] + z[20];
    z[20]=z[20]*z[41];
    z[20]=2*z[142] + z[20];
    z[20]=z[20]*z[53];
    z[132]=z[132] + z[32];
    z[132]=z[10]*z[132];
    z[20]=z[20] + 77*z[26] + z[132];
    z[20]=z[7]*z[20];
    z[112]=z[98]*z[112];
    z[132]=npow(z[5],3);
    z[142]=5*z[132];
    z[143]= - z[11]*npow(z[32],5)*z[142];
    z[112]= - 12*z[112] + z[143];
    z[112]=z[11]*z[112];
    z[143]=11*z[5];
    z[144]= - z[110]*z[143];
    z[112]=z[144] + z[112];
    z[112]=z[112]*z[116];
    z[112]= - 17*z[81] + z[112];
    z[112]=z[112]*z[70];
    z[116]= - z[52] - 12*z[32];
    z[20]=z[112] + z[20] + 3*z[116] + z[76];
    z[20]=z[20]*z[47];
    z[76]=11*z[64];
    z[112]=z[97]*z[76];
    z[116]=z[132]*z[19];
    z[144]=z[77]*z[116];
    z[145]=40*z[11];
    z[146]=z[144]*z[145];
    z[112]=z[146] + 20*z[99] + z[112];
    z[112]=z[11]*z[112];
    z[146]= - 6*z[3] - z[30];
    z[146]=z[29]*z[146];
    z[147]=z[13]*z[78];
    z[112]=z[112] + z[147] + z[146];
    z[112]=z[70]*z[112];
    z[146]=z[32]*z[5];
    z[147]=z[127] + z[146];
    z[147]=z[147]*z[85];
    z[76]=z[103]*z[76];
    z[148]=z[80]*z[84];
    z[148]= - 7*z[105] + z[148];
    z[148]=z[148]*z[56];
    z[76]=z[148] + z[147] + z[76] + z[112];
    z[76]=z[16]*z[76];
    z[112]=z[41]*z[21];
    z[147]=7*z[19] - z[112];
    z[147]=z[147]*z[106];
    z[147]= - 9*z[26] + z[147];
    z[147]=z[147]*z[53];
    z[46]= - 12*z[21] + z[46];
    z[46]=z[7]*z[46];
    z[46]=z[135] + z[46];
    z[46]=z[7]*z[46];
    z[46]= - z[49] + z[46];
    z[46]=z[3]*z[46];
    z[135]= - z[26]*z[38];
    z[135]= - z[67] + z[135];
    z[135]=z[13]*z[135];
    z[148]=z[3]*z[44];
    z[135]=z[135] + z[148];
    z[135]=z[16]*z[135];
    z[148]=z[13]*z[26];
    z[46]=z[135] + 8*z[46] - 14*z[148] - z[28] + z[147];
    z[46]=z[8]*z[46];
    z[135]=z[81]*z[5];
    z[147]= - z[13]*z[81];
    z[147]=12*z[135] + z[147];
    z[113]=z[142]*z[113];
    z[142]=7*z[98];
    z[148]=z[110]*z[142];
    z[148]=z[148] + z[113];
    z[148]=z[11]*z[148];
    z[147]=3*z[147] + 8*z[148];
    z[147]=z[147]*z[70];
    z[148]=2*z[1];
    z[149]=z[74]*z[148];
    z[77]=z[77]*z[128];
    z[77]=z[149] + z[77];
    z[23]=z[77]*z[23];
    z[23]=z[89] + z[23];
    z[23]=z[23]*z[30];
    z[77]=z[19]*z[7];
    z[89]= - 40*z[77] + 50*z[26] + z[29];
    z[89]=z[7]*z[89];
    z[149]=20*z[28];
    z[89]=z[89] - z[149] - z[32];
    z[89]=z[89]*z[106];
    z[150]=z[77]*z[84];
    z[151]=z[54]*z[7];
    z[150]=z[150] + z[80] - z[151];
    z[150]=z[150]*z[56];
    z[152]= - z[50] + 6*z[32];
    z[152]=z[13]*z[152];
    z[20]=z[42] + z[46] + z[76] + z[20] + z[147] + z[150] + z[152] + 
    z[89] - z[1] + z[23];
    z[20]=z[6]*z[20];
    z[23]=z[31] + z[32];
    z[42]=z[4]*z[23];
    z[46]=z[34]*z[70];
    z[76]= - z[16]*z[46];
    z[42]=z[76] + z[123] + z[127] + z[42];
    z[42]=z[16]*z[42];
    z[76]=z[81]*z[12];
    z[89]=z[109] + z[76];
    z[147]= - z[16]*z[148];
    z[147]=z[147] + z[31] - z[39];
    z[147]=z[16]*z[147];
    z[150]=z[16]*z[1];
    z[152]=z[28] - z[150];
    z[153]=npow(z[16],2);
    z[154]=z[153]*z[8];
    z[152]=z[152]*z[154];
    z[89]=z[152] + 2*z[89] + z[147];
    z[89]=z[2]*z[89];
    z[147]=z[100] + z[28];
    z[147]=z[147]*z[10];
    z[152]=z[94]*z[74];
    z[155]=z[152]*z[4];
    z[147]=z[147] + z[155] - z[26];
    z[155]=z[147]*z[56];
    z[156]=z[103]*z[10];
    z[59]=z[59] - z[51] - z[156];
    z[59]=z[6]*z[59];
    z[157]=z[4]*z[35]*z[61];
    z[158]=z[121]*z[16];
    z[159]= - z[124] - z[158];
    z[153]=z[153]*z[9];
    z[159]=z[159]*z[153];
    z[42]=z[59] + z[89] + z[159] + z[42] + z[155] + z[32] + z[157];
    z[59]=3*z[17];
    z[42]=z[42]*z[59];
    z[89]=z[81]*z[84];
    z[89]= - z[134] + z[89];
    z[89]=z[89]*z[56];
    z[134]= - z[94]*z[61];
    z[134]= - z[26] + z[134];
    z[134]=z[5]*z[134];
    z[154]= - z[127]*z[154];
    z[155]=8*z[32];
    z[89]=z[154] - 6*z[150] + z[89] + z[134] - z[28] - z[155];
    z[89]=z[2]*z[89];
    z[134]=z[105]*z[70];
    z[154]=z[61]*z[134];
    z[157]=z[128]*z[4];
    z[134]=z[157] - z[134];
    z[157]=z[16]*z[134];
    z[117]=z[117] - 4*z[72];
    z[117]=z[16]*z[117];
    z[117]=z[117] + z[1] - 9*z[124];
    z[117]=z[9]*z[117];
    z[124]=2*z[105];
    z[159]=z[124] + z[1];
    z[160]= - z[4]*z[159];
    z[117]=z[117] + z[157] + z[154] + z[160] - static_cast<T>(13)- 4*z[128];
    z[117]=z[16]*z[117];
    z[154]=2*z[94];
    z[138]=z[138]*z[5];
    z[157]=z[19]*z[12];
    z[157]=z[157] - z[26];
    z[160]=z[12]*z[157];
    z[161]=z[26]*z[8];
    z[160]=z[161] + 6*z[160] + z[154] + z[138];
    z[160]=z[6]*z[160];
    z[147]=z[147]*z[84];
    z[101]=z[101]*z[10];
    z[162]= - z[4]*z[101];
    z[147]=z[147] - z[108] + z[162];
    z[147]=z[147]*z[56];
    z[162]= - z[135] - z[76];
    z[163]=2*z[70];
    z[162]=z[162]*z[163];
    z[164]=z[31] + z[80];
    z[165]= - z[32] + z[164];
    z[165]=z[4]*z[165];
    z[166]=11*z[1];
    z[100]= - z[5]*z[100];
    z[167]= - z[8]*z[28];
    z[42]=z[42] + z[160] + z[89] + z[167] + z[162] + z[147] + 2*z[165]
    + z[100] + z[166] + 10*z[10] + z[117];
    z[42]=z[17]*z[42];
    z[89]=z[79] - z[109];
    z[100]=z[15]*z[89];
    z[117]=z[15]*z[27];
    z[117]= - 2*z[97] + z[117];
    z[117]=z[15]*z[117];
    z[117]=z[110] + z[117];
    z[82]=z[117]*z[82];
    z[82]=z[82] + z[81] + z[100];
    z[82]=z[82]*z[70];
    z[100]= - z[120]*z[27];
    z[100]= - z[1] + z[100];
    z[100]=2*z[100] - z[158];
    z[100]=z[16]*z[100];
    z[35]=z[100] - z[126] - z[35];
    z[35]=z[16]*z[35];
    z[100]=z[12]*z[26]*z[61];
    z[35]=z[35] + z[36] + z[100];
    z[35]=z[9]*z[35];
    z[100]=9*z[28];
    z[117]=5*z[32];
    z[120]=z[100] + z[117];
    z[120]=z[10]*z[120];
    z[117]= - z[28] - z[117];
    z[117]=z[15]*z[117];
    z[117]=z[117] + z[54] + z[120];
    z[117]=z[4]*z[117];
    z[72]=z[72]*z[16];
    z[120]= - z[70]*z[29];
    z[120]= - z[1] + z[120];
    z[120]=3*z[120] - z[72];
    z[120]=z[16]*z[120];
    z[126]=z[68]*z[121];
    z[91]=z[126] + z[40] - z[91];
    z[91]=z[16]*z[91];
    z[126]=z[1]*z[68];
    z[91]=z[91] + z[26] + z[126];
    z[91]=z[8]*z[91];
    z[39]=z[39] + z[31];
    z[126]=z[27] + z[19];
    z[147]=z[126]*z[4];
    z[147]=z[147] + z[51];
    z[158]=z[147]*z[56];
    z[35]=z[35] + z[91] + z[120] + z[82] + z[158] + z[117] - z[40] + 
    z[39];
    z[35]=z[35]*z[140];
    z[82]=z[96] + z[123];
    z[72]=2*z[82] + 5*z[72];
    z[72]=z[16]*z[72];
    z[36]=z[51] + z[36];
    z[36]=z[7]*z[36];
    z[82]=z[29] + z[26];
    z[91]=z[10]*z[25];
    z[91]=z[91] - z[82];
    z[91]=z[91]*z[56];
    z[36]=z[72] - z[141] + z[91] - 2*z[60] + z[36];
    z[36]=z[9]*z[36];
    z[60]=z[50] - z[25];
    z[60]=z[12]*z[60];
    z[60]= - z[148] + z[60];
    z[60]=2*z[60] + z[69];
    z[60]=z[16]*z[60];
    z[69]=z[57] + z[7];
    z[69]=z[26]*z[69];
    z[72]=z[40]*z[7];
    z[91]= - z[148] + z[72];
    z[91]=z[15]*z[91];
    z[60]=z[60] + z[73] + z[91] - z[50] + z[69];
    z[60]=z[8]*z[60];
    z[69]=z[7]*z[34];
    z[69]= - z[72] + z[96] + z[69];
    z[69]=z[15]*z[69];
    z[73]= - z[26]*z[106];
    z[69]=z[69] + z[73] - 8*z[28] + z[32];
    z[69]=z[4]*z[69];
    z[73]=z[54] - z[29];
    z[91]= - z[4]*z[73];
    z[117]=z[147]*z[84];
    z[91]=z[91] + z[117];
    z[91]=z[91]*z[56];
    z[117]=z[7]*z[102];
    z[117]=z[117] + z[1] + 3*z[10];
    z[66]=static_cast<T>(2)- z[66];
    z[66]=z[16]*z[66];
    z[120]= - static_cast<T>(3)- z[107];
    z[120]=z[15]*z[120];
    z[35]=z[35] + z[36] + z[60] + z[66] + z[75] + z[91] + z[69] + 2*
    z[117] + 3*z[120];
    z[35]=z[18]*z[35];
    z[36]=13*z[1];
    z[60]= - z[36] + 16*z[105];
    z[60]=z[5]*z[60];
    z[66]=z[159]*z[133];
    z[60]=z[60] + z[66];
    z[60]=z[7]*z[60];
    z[66]=z[148]*z[98];
    z[69]=z[41]*z[128];
    z[66]=z[66] + z[69];
    z[69]= - z[66]*z[53];
    z[75]=z[132]*z[96];
    z[69]=z[75] + z[69];
    z[69]=z[15]*z[69];
    z[75]=3*z[105];
    z[91]=z[166] + z[75];
    z[117]=2*z[98];
    z[91]=z[91]*z[117];
    z[60]=z[69] - z[91] + z[60];
    z[60]=z[15]*z[60];
    z[69]=4*z[80];
    z[120]= - z[28] - z[69];
    z[120]=z[120]*z[85];
    z[123]= - z[34] - z[80];
    z[123]=z[123]*z[133];
    z[140]=21*z[1];
    z[120]=z[123] + z[140] + z[120];
    z[120]=z[7]*z[120];
    z[123]=3*z[80];
    z[141]=z[123] + 29*z[28];
    z[147]=z[5]*z[141];
    z[147]=z[140] + z[147];
    z[147]=z[5]*z[147];
    z[60]=z[60] + z[147] + z[120];
    z[60]=z[55]*z[7]*z[60];
    z[120]=7*z[80] + 32*z[28];
    z[147]= - z[120]*z[85];
    z[158]= - z[131] + 15*z[80];
    z[158]=z[158]*z[106];
    z[140]=z[158] - z[140] + z[147];
    z[140]=z[7]*z[140];
    z[60]=z[60] - z[129] + z[140];
    z[60]=z[15]*z[60];
    z[129]=z[40]*z[132];
    z[91]= - z[91] + 3*z[129];
    z[91]=z[91]*z[55];
    z[140]=z[141]*z[85];
    z[140]=39*z[1] + z[140];
    z[140]=z[5]*z[140];
    z[91]=z[140] + z[91];
    z[91]=z[15]*z[91];
    z[120]= - z[5]*z[120];
    z[120]= - z[104] + z[120];
    z[91]=2*z[120] + z[91];
    z[91]=z[15]*z[91];
    z[87]=z[164]*z[87];
    z[120]=z[98]*z[40];
    z[140]= - z[5]*z[159];
    z[120]=z[140] + z[120];
    z[120]=z[15]*z[120];
    z[87]=22*z[120] + z[104] + z[87];
    z[87]=z[15]*z[87];
    z[120]= - z[28] - z[80];
    z[87]=44*z[120] + z[87];
    z[87]=z[15]*z[87];
    z[87]=33*z[26] + z[87];
    z[87]=z[13]*z[87];
    z[87]=z[87] + z[91] + 46*z[28] + 11*z[80];
    z[87]=z[13]*z[87];
    z[91]= - z[21]*z[139];
    z[91]=17*z[19] + z[91];
    z[91]=z[91]*z[53];
    z[91]= - 49*z[26] + z[91];
    z[91]=z[7]*z[91];
    z[120]=z[77] - z[26];
    z[140]=z[120]*z[106];
    z[140]=z[28] + z[140];
    z[140]=z[140]*z[139];
    z[140]=z[140] - z[72];
    z[140]=z[15]*z[140];
    z[141]= - z[157]*z[57];
    z[91]=z[141] + z[140] + z[88] + z[91];
    z[91]=z[91]*z[47];
    z[140]=z[67] - 4*z[40];
    z[140]=z[140]*z[64];
    z[141]= - z[12]*z[80];
    z[124]=z[124] + z[141];
    z[124]=z[124]*z[56];
    z[71]= - z[127]*z[71];
    z[71]=z[71] + z[124] - z[128] + z[140];
    z[71]=z[16]*z[71];
    z[69]=19*z[28] + z[69];
    z[77]= - 7*z[26] + 8*z[77];
    z[77]=z[77]*z[139];
    z[69]=3*z[69] + z[77];
    z[69]=z[7]*z[69];
    z[62]= - z[13]*z[62];
    z[62]=z[80] + z[62];
    z[62]=z[62]*z[57];
    z[60]=z[71] + z[91] + z[62] + z[87] + z[60] - 4*z[105] + z[69];
    z[60]=z[8]*z[60];
    z[62]=z[12]*z[122];
    z[62]= - z[109] + z[62];
    z[62]=z[62]*z[70];
    z[69]=z[12]*z[103];
    z[62]=z[62] - z[1] + z[69];
    z[48]= - z[161] - z[28] + z[48];
    z[48]=z[6]*z[48];
    z[69]=z[31]*z[70]*z[16];
    z[71]=z[28] + z[150];
    z[71]=z[8]*z[71];
    z[77]=z[121]*z[153];
    z[48]=z[48] + z[77] + z[71] + 2*z[62] + z[69];
    z[48]=z[48]*z[59];
    z[59]=z[84]*z[103];
    z[62]= - z[127] + z[59];
    z[62]=z[12]*z[62];
    z[69]=z[54]*z[74];
    z[71]=z[11]*z[114]*z[69];
    z[77]=z[12]*z[29];
    z[71]= - 5*z[77] + z[71];
    z[71]=z[71]*z[70];
    z[77]=z[8]*z[96];
    z[62]=z[77] + z[71] + static_cast<T>(5)+ z[62];
    z[25]=z[25] + z[28];
    z[71]=z[25]*z[56];
    z[77]= - z[8]*z[31];
    z[71]=z[71] + z[77];
    z[71]=z[6]*z[71];
    z[48]=z[48] + 2*z[62] + z[71];
    z[48]=z[17]*z[48];
    z[62]=npow(z[7],2);
    z[71]= - z[28]*z[62];
    z[25]=z[12]*z[7]*z[25];
    z[77]=z[19]*z[106];
    z[77]= - z[26] + z[77];
    z[77]=z[77]*z[41];
    z[77]= - z[28] + z[77];
    z[77]=z[8]*z[77]*z[106];
    z[25]=z[77] + z[71] + z[25];
    z[25]=z[6]*z[25];
    z[71]=z[122]*z[114];
    z[77]=z[12]*z[109];
    z[77]=z[77] - z[71];
    z[77]=z[163]*z[77];
    z[77]=z[77] - static_cast<T>(1)+ z[107];
    z[77]=z[7]*z[77];
    z[84]=z[28] - z[151];
    z[84]=z[84]*z[139];
    z[84]=z[1] + z[84];
    z[84]=z[7]*z[84];
    z[44]=z[7]*z[44];
    z[44]= - z[1] + z[44];
    z[44]=z[44]*z[62];
    z[58]= - npow(z[7],3)*z[58];
    z[44]=z[44] + z[58];
    z[44]=z[15]*z[44];
    z[44]=z[84] + 5*z[44];
    z[44]=z[8]*z[44];
    z[58]=z[7]*z[59];
    z[59]= - z[1]*z[53];
    z[58]=z[59] + z[58];
    z[58]=z[58]*z[56];
    z[59]=z[12]*z[78];
    z[59]=z[59] - 4*z[71];
    z[59]=z[59]*z[163];
    z[59]= - static_cast<T>(5)+ z[59];
    z[59]=z[9]*z[59];
    z[25]=z[48] + 2*z[25] + z[59] + 4*z[44] + z[58] + z[77];
    z[25]=z[14]*z[25];
    z[44]=z[19]*z[85];
    z[44]= - z[49] + z[44];
    z[48]=20*z[5];
    z[44]=z[44]*z[48];
    z[44]=z[44] + 61*z[28] + z[32];
    z[44]=z[5]*z[44];
    z[49]= - z[86] - z[108];
    z[49]=z[10]*z[49];
    z[58]= - z[19] - z[125];
    z[58]=z[58]*z[139];
    z[49]=z[58] + 16*z[26] + z[49];
    z[49]=z[49]*z[53];
    z[49]=z[49] - 47*z[28] - z[137];
    z[49]=z[7]*z[49];
    z[58]=z[156] + z[26];
    z[59]= - z[58]*z[133];
    z[59]=z[59] + z[149] - 21*z[32];
    z[59]=z[7]*z[59];
    z[59]= - z[95] + z[59];
    z[59]=z[15]*z[59]*z[106];
    z[37]= - z[154] + z[37];
    z[37]=z[37]*z[63];
    z[62]=14*z[1];
    z[37]=z[37] + z[59] + z[49] - z[62] + z[44];
    z[37]=z[4]*z[37];
    z[44]= - z[10]*z[123];
    z[49]=z[15]*z[80];
    z[44]=z[44] + z[49];
    z[44]=z[15]*z[44];
    z[44]=2*z[83] + z[44];
    z[44]=z[44]*z[38];
    z[49]= - z[15]*z[136];
    z[49]= - 4*z[27] + z[49];
    z[59]=z[98]*z[15];
    z[49]=z[49]*z[59];
    z[44]=z[44] + 15*z[99] + z[49];
    z[44]=z[13]*z[15]*z[44];
    z[38]= - z[38]*z[65]*z[98]*z[19];
    z[49]= - z[116]*z[65];
    z[38]=19*z[49] + z[38];
    z[38]=z[13]*z[38];
    z[49]=z[65]*z[116]*z[7];
    z[38]=z[49] + z[38];
    z[38]=z[11]*z[68]*z[38];
    z[49]=z[98]*z[7];
    z[65]=50*z[132] - z[49];
    z[27]=z[27]*z[65];
    z[49]= - 20*z[132] + z[49];
    z[65]=z[26]*z[15];
    z[49]=z[49]*z[65];
    z[27]=z[49] + z[27];
    z[27]=z[15]*z[27];
    z[49]=z[132]*z[97];
    z[27]= - 30*z[49] + z[27];
    z[27]=z[27]*z[55];
    z[49]= - z[64]*z[111];
    z[27]=z[38] + z[49] + z[27] + z[44];
    z[27]=z[11]*z[27];
    z[38]=z[15]*z[67];
    z[38]= - 44*z[29] + z[38];
    z[38]=z[15]*z[38];
    z[38]=33*z[81] + z[38];
    z[38]=z[13]*z[38];
    z[44]=z[5]*z[29];
    z[49]=z[15]*z[105];
    z[44]=64*z[44] - 39*z[49];
    z[44]=z[15]*z[44];
    z[38]=z[38] - 11*z[135] + z[44];
    z[38]=z[13]*z[38];
    z[44]= - z[7] + z[63];
    z[44]=z[56]*z[44];
    z[44]=z[44] - 10*z[98];
    z[44]=z[81]*z[44];
    z[29]=z[98]*z[29];
    z[49]=z[28]*z[98];
    z[67]=z[7]*z[75];
    z[49]=62*z[49] + z[67];
    z[49]=z[15]*z[49];
    z[29]= - 68*z[29] + z[49];
    z[29]=z[15]*z[29];
    z[49]=z[7]*z[135];
    z[27]=2*z[27] + z[38] + z[29] + z[49] + z[44];
    z[27]=z[27]*z[70];
    z[29]=z[142] - z[114];
    z[29]=z[110]*z[29];
    z[29]=z[113] + z[29];
    z[29]=z[11]*z[29];
    z[38]=z[81]*z[43];
    z[29]=z[29] + z[38] + z[76];
    z[29]=z[29]*z[115];
    z[38]=z[10]*z[58];
    z[38]= - z[19] + z[38];
    z[38]=z[38]*z[43];
    z[38]=13*z[58] + z[38];
    z[30]=z[38]*z[30];
    z[38]= - z[131] + 15*z[32];
    z[30]=3*z[38] + z[30];
    z[30]=z[5]*z[30];
    z[21]= - z[21]*z[43];
    z[21]=z[45] + z[21];
    z[21]=z[5]*z[21];
    z[21]= - z[136] + z[21];
    z[21]=z[5]*z[21];
    z[38]= - 8*z[19] + z[112];
    z[38]=z[7]*z[38];
    z[38]=z[118] + z[38];
    z[38]=z[7]*z[38];
    z[21]=z[38] + z[28] + z[21];
    z[38]=z[120]*z[41];
    z[38]=z[34] + z[38];
    z[38]=z[38]*z[53];
    z[38]=z[38] + z[72];
    z[38]=z[15]*z[38];
    z[21]=4*z[21] + z[38];
    z[21]=z[4]*z[21];
    z[38]=z[39]*z[10];
    z[38]=z[38] + z[51];
    z[39]= - z[38]*z[56];
    z[39]=z[102] + z[39];
    z[39]=z[39]*z[57];
    z[40]=z[40] - z[94];
    z[41]=z[106]*z[40];
    z[21]=z[29] + z[39] + z[21] + z[30] + 9*z[1] + z[61] + z[41];
    z[21]=z[21]*z[47];
    z[29]=z[96]*z[74];
    z[30]=z[26] - z[29];
    z[30]=z[5]*z[30];
    z[30]=z[30] - 25*z[28] - 34*z[32];
    z[30]=z[30]*z[85];
    z[30]= - 45*z[1] + z[30];
    z[30]=z[5]*z[30];
    z[39]=z[128]*z[74];
    z[31]= - z[31] - z[137];
    z[31]=3*z[31] + 8*z[39];
    z[31]=z[5]*z[31];
    z[39]=z[34] + z[39];
    z[39]=z[39]*z[133];
    z[31]=z[39] - 41*z[1] + z[31];
    z[31]=z[31]*z[106];
    z[30]=z[31] + static_cast<T>(1)+ z[30];
    z[30]=z[7]*z[30];
    z[31]=z[94]*z[5];
    z[39]=15*z[1] - 8*z[31];
    z[39]=z[5]*z[39];
    z[41]=z[102]*z[5];
    z[44]= - z[148] - z[41];
    z[44]=z[44]*z[133];
    z[39]=3*z[39] + z[44];
    z[39]=z[7]*z[39];
    z[44]=z[66]*z[106];
    z[45]=z[132]*z[1];
    z[44]= - z[45] + z[44];
    z[44]=z[15]*z[44];
    z[47]=23*z[1];
    z[41]=z[47] + z[41];
    z[41]=z[41]*z[117];
    z[39]=6*z[44] + z[41] + z[39];
    z[39]=z[7]*z[39];
    z[39]=60*z[45] + z[39];
    z[39]=z[39]*z[55];
    z[36]= - z[36] - 25*z[146];
    z[36]=z[5]*z[36];
    z[36]=static_cast<T>(3)+ z[36];
    z[41]=8*z[5];
    z[36]=z[36]*z[41];
    z[30]=z[39] + z[36] + z[30];
    z[30]=z[15]*z[30];
    z[36]=19*z[26];
    z[29]= - z[36] - z[29];
    z[29]=z[5]*z[29];
    z[29]=z[29] - 76*z[28] - 43*z[32];
    z[29]=z[29]*z[98];
    z[31]=91*z[1] + 66*z[31];
    z[31]=z[31]*z[98];
    z[31]=z[31] - 66*z[129];
    z[31]=z[15]*z[31];
    z[29]=z[31] - static_cast<T>(5)+ z[29];
    z[29]=z[29]*z[55];
    z[31]=z[5]*z[10];
    z[39]= - z[82]*z[31];
    z[39]=z[39] + z[26] - z[90];
    z[39]=z[39]*z[143];
    z[41]=3*z[103];
    z[44]=z[41] - z[138];
    z[44]=z[44]*z[143];
    z[40]= - z[40]*z[59];
    z[40]=66*z[40] - z[104] + z[44];
    z[40]=z[15]*z[40];
    z[39]=z[40] + 35*z[94] + z[39];
    z[39]=z[39]*z[63];
    z[40]=z[52] + z[93];
    z[40]=z[10]*z[40];
    z[36]= - z[36] - z[33];
    z[36]=z[36]*z[31];
    z[36]=z[36] + 18*z[26] + z[40];
    z[36]=z[5]*z[36];
    z[36]= - z[41] + z[36];
    z[36]=z[36]*z[85];
    z[29]=z[39] + z[29] + z[36] + z[62] + 39*z[10];
    z[29]=z[13]*z[29];
    z[36]= - z[38]*z[63];
    z[36]=z[102] + z[36];
    z[36]=z[36]*z[63];
    z[38]=z[5]*z[73];
    z[39]= - z[7]*z[101];
    z[38]=z[38] + z[39];
    z[38]=z[4]*z[38];
    z[39]= - z[5]*z[126];
    z[40]=z[7]*z[152];
    z[39]=z[39] + z[40];
    z[39]=z[4]*z[39];
    z[23]=z[10]*z[23];
    z[23]= - z[54] + z[23];
    z[23]=z[7]*z[23];
    z[23]=3*z[39] - 2*z[80] + z[23];
    z[23]=z[12]*z[23];
    z[39]= - z[7]*z[32];
    z[23]=z[23] + z[38] + z[36] - z[105] + z[39];
    z[23]=z[23]*z[56];
    z[36]=z[10]*z[54];
    z[36]=z[36] - z[65];
    z[36]=z[15]*z[36];
    z[36]= - z[69] + z[36];
    z[36]=z[145]*z[132]*z[36];
    z[38]=z[98]*z[89];
    z[39]= - z[78] + 4*z[79];
    z[39]=z[39]*z[64];
    z[36]=z[36] + 20*z[38] + z[39];
    z[36]=z[36]*z[70];
    z[38]= - z[5]*z[58];
    z[38]=z[38] - z[103];
    z[38]=z[38]*z[85];
    z[38]= - z[1] + z[38];
    z[38]=z[38]*z[43];
    z[39]=z[11]*z[144];
    z[39]=z[99] + z[39];
    z[39]=z[11]*z[39];
    z[40]=z[61]*z[105];
    z[39]=z[40] + 5*z[39];
    z[39]=z[39]*z[163];
    z[19]= - z[5]*z[19];
    z[19]=z[26] + z[19];
    z[19]=z[19]*z[43];
    z[19]= - z[34] + z[19];
    z[34]=z[85]*z[4];
    z[19]=z[19]*z[34];
    z[19]=z[39] + z[38] + z[19];
    z[19]=z[3]*z[19];
    z[38]= - z[64]*z[46];
    z[39]= - z[3]*z[134];
    z[38]=z[38] + z[39];
    z[38]=z[16]*z[38];
    z[39]= - z[1] + z[105];
    z[34]=z[39]*z[34];
    z[39]= - z[5]*z[130];
    z[19]=2*z[38] + 4*z[19] + z[36] + z[34] + z[39] - 8*z[13];
    z[19]=z[16]*z[19];
    z[34]=z[88] + 19*z[32];
    z[34]=z[10]*z[34];
    z[34]=z[34] + 8*z[135];
    z[34]=z[5]*z[34];
    z[36]=z[51] + z[135];
    z[36]=z[36]*z[133];
    z[34]=z[36] + z[34] - z[100] + z[32];
    z[34]=z[34]*z[106];
    z[28]=z[28] + z[93];
    z[28]=z[10]*z[28];
    z[33]=z[26] - z[33];
    z[31]=z[33]*z[31];
    z[26]=z[31] - z[26] + z[28];
    z[26]=z[26]*z[85];
    z[26]=z[26] + z[50] - z[32];
    z[26]=z[5]*z[26];
    z[26]=z[34] + z[26] + 5*z[1] + z[10];
    z[26]=z[7]*z[26];
    z[28]=z[119]*z[48];
    z[28]=z[28] + z[92] - z[155];
    z[28]=z[28]*z[85];
    z[28]=z[28] - z[47] - 42*z[10];
    z[28]=z[5]*z[28];
    z[19]=z[25] + z[42] + z[20] + z[35] + z[24] + z[22] + z[60] + z[19]
    + z[21] + z[27] + z[23] + z[37] + z[29] + z[30] + z[26] - static_cast<T>(11)+ 
    z[28];

    r += 2*z[19];
 
    return r;
}

template double qg_2lNLC_r1287(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1287(const std::array<dd_real,31>&);
#endif
