#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r342(const std::array<T,31>& k) {
  T z[115];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[7];
    z[9]=k[5];
    z[10]=k[13];
    z[11]=k[2];
    z[12]=k[14];
    z[13]=k[9];
    z[14]=k[3];
    z[15]=k[24];
    z[16]=n<T>(1,2)*z[3];
    z[17]= - z[16] - z[8];
    z[18]=n<T>(1,2)*z[8];
    z[19]=npow(z[12],2);
    z[17]=z[18]*z[19]*z[17];
    z[20]=npow(z[4],2);
    z[21]=n<T>(9,4)*z[4];
    z[21]=z[3]*z[21];
    z[21]=z[20] + z[21];
    z[21]=z[3]*z[21];
    z[22]=z[5]*z[8];
    z[23]= - z[16]*z[22];
    z[24]=npow(z[8],2);
    z[25]=z[24]*z[3];
    z[21]=z[23] + z[21] - n<T>(1,4)*z[25];
    z[21]=z[5]*z[21];
    z[23]=2*z[3];
    z[26]=z[4]*z[2];
    z[27]=z[26]*z[23];
    z[28]=z[20]*z[2];
    z[27]= - n<T>(1,2)*z[28] + z[27];
    z[27]=z[3]*z[27];
    z[29]=n<T>(1,4)*z[4];
    z[30]=npow(z[10],3);
    z[31]=z[30]*z[29];
    z[32]=n<T>(1,4)*z[19];
    z[33]= - z[6]*z[32]*z[25];
    z[17]=z[33] + z[31] + z[21] + z[27] + z[17];
    z[17]=z[6]*z[17];
    z[21]=3*z[2];
    z[27]=n<T>(1,2)*z[4];
    z[31]=n<T>(1,2)*z[5];
    z[33]= - z[10] - z[31] - z[21] - z[27];
    z[34]=npow(z[10],2);
    z[35]=n<T>(1,2)*z[34];
    z[33]=z[33]*z[35];
    z[36]= - z[2]*z[24];
    z[36]= - z[28] + z[36];
    z[37]=n<T>(3,8)*z[20] + z[24];
    z[38]=3*z[5];
    z[37]=z[37]*z[38];
    z[33]=z[33] + n<T>(1,2)*z[36] + z[37];
    z[33]=z[6]*z[33];
    z[36]=n<T>(43,2)*z[4];
    z[37]= - n<T>(187,3)*z[2] + z[36];
    z[37]=z[37]*z[29];
    z[39]=z[8]*z[4];
    z[40]=6*z[39];
    z[37]=z[37] + z[40];
    z[37]=z[8]*z[37];
    z[41]=n<T>(1,4)*z[5];
    z[42]= - z[34]*z[41];
    z[43]=z[20]*z[5];
    z[37]=z[42] + n<T>(9,8)*z[43] + z[28] + z[37];
    z[37]=z[9]*z[37];
    z[40]= - n<T>(25,8)*z[20] - z[40];
    z[42]=z[8]*z[14];
    z[40]=z[40]*z[42];
    z[43]=z[5]*z[11];
    z[44]=z[43]*z[32];
    z[45]=n<T>(1,2)*z[14];
    z[46]=z[30]*z[45];
    z[33]=z[37] + z[33] + z[46] + z[40] + z[44];
    z[33]=z[9]*z[33];
    z[37]=n<T>(1,2)*z[2];
    z[40]=z[37]*z[24];
    z[44]=n<T>(3,2)*z[8];
    z[46]= - z[5]*z[44];
    z[46]=z[24] + z[46];
    z[46]=z[5]*z[46];
    z[47]= - z[2] - z[4];
    z[47]=n<T>(1,2)*z[47] + z[10];
    z[47]=z[47]*z[35];
    z[46]=z[47] + z[40] + 7*z[46];
    z[46]=z[6]*z[46];
    z[47]=z[30]*z[14];
    z[46]=z[47] + z[46];
    z[46]=z[6]*z[46];
    z[48]=npow(z[11],2);
    z[49]=z[19]*z[48]*z[31];
    z[50]=npow(z[14],2);
    z[51]= - z[50]*z[30];
    z[33]=z[33] + z[46] + z[49] + z[51];
    z[33]=z[9]*z[33];
    z[46]= - 37*z[3] - 17*z[8];
    z[46]=z[46]*z[41];
    z[49]=npow(z[3],2);
    z[51]=n<T>(7,4)*z[49];
    z[52]=4*z[8];
    z[53]=n<T>(9,2)*z[3] + z[52];
    z[53]=z[8]*z[53];
    z[46]=z[46] + z[51] + z[53];
    z[46]=z[6]*z[46];
    z[53]=z[49]*z[11];
    z[54]=z[3]*z[11];
    z[55]=z[5]*z[54];
    z[46]=z[46] - n<T>(7,2)*z[53] + 4*z[55];
    z[46]=z[5]*z[46];
    z[55]=z[16]*z[14];
    z[56]= - z[24]*z[55];
    z[57]=z[3]*z[14];
    z[58]=3*z[57];
    z[59]=z[10]*z[14];
    z[60]=z[58] - z[59];
    z[60]=z[60]*z[34];
    z[46]=n<T>(1,4)*z[60] + z[56] + z[46];
    z[46]=z[6]*z[46];
    z[56]=n<T>(7,2)*z[49];
    z[56]=z[48]*z[56];
    z[60]=z[48]*z[3];
    z[61]= - z[5]*z[60];
    z[56]=z[56] + z[61];
    z[56]=z[5]*z[56];
    z[61]=z[50]*z[3];
    z[62]=z[50]*z[10];
    z[63]=3*z[61] - z[62];
    z[63]=z[63]*z[34];
    z[56]=z[56] + z[63];
    z[46]=n<T>(1,2)*z[56] + z[46];
    z[46]=z[6]*z[46];
    z[56]=npow(z[14],3);
    z[63]=n<T>(1,2)*z[10];
    z[64]=z[56]*z[63];
    z[65]=z[56]*z[3];
    z[64]=z[65] + z[64];
    z[64]=z[64]*z[34];
    z[33]=z[33] + z[64] + z[46];
    z[33]=z[33]*npow(z[7],3);
    z[46]=z[4]*z[15];
    z[64]=n<T>(1,4)*z[10];
    z[66]= - z[4]*z[64];
    z[66]=z[66] + 4*z[46] + n<T>(5,4)*z[39];
    z[66]=z[10]*z[66];
    z[67]=n<T>(1,4)*z[8];
    z[68]= - z[20]*z[67];
    z[66]=z[68] + z[66];
    z[66]=z[10]*z[66];
    z[68]=n<T>(3,2)*z[4];
    z[69]=z[24]*z[68];
    z[70]= - z[46] - n<T>(3,4)*z[39];
    z[70]=z[10]*z[70];
    z[69]=z[69] + z[70];
    z[69]=z[69]*z[34];
    z[70]=npow(z[13],2);
    z[71]=2*z[70];
    z[72]=z[71]*z[20];
    z[73]= - z[5]*z[72];
    z[69]=z[73] + z[69];
    z[69]=z[9]*z[69];
    z[73]= - z[19] - z[26];
    z[73]=z[8]*z[73];
    z[74]=3*z[28];
    z[73]= - z[74] + z[73];
    z[73]=z[73]*z[67];
    z[75]=4*z[13];
    z[76]= - z[75] + z[37];
    z[76]=z[4]*z[76];
    z[76]= - z[71] + z[76];
    z[77]=z[5]*z[4];
    z[76]=z[76]*z[77];
    z[66]=z[69] + z[66] + z[73] + z[76];
    z[66]=z[9]*z[66];
    z[69]=3*z[3];
    z[73]= - z[31] + z[69] - z[67];
    z[73]=z[8]*z[73];
    z[76]=5*z[4];
    z[78]= - z[76] - 17*z[13] - n<T>(121,2)*z[2];
    z[78]=z[78]*z[29];
    z[79]=z[2]*z[11];
    z[80]=static_cast<T>(1)- z[79];
    z[80]=z[80]*z[23];
    z[81]= - 53*z[2] + 35*z[4];
    z[80]=n<T>(1,8)*z[81] + z[80];
    z[80]=z[3]*z[80];
    z[73]=z[78] + z[80] + z[73];
    z[73]=z[5]*z[73];
    z[78]=3*z[15];
    z[80]= - z[78] + z[29];
    z[80]=z[4]*z[80];
    z[81]=n<T>(5,2)*z[13];
    z[82]= - z[57]*z[81];
    z[82]=z[10] + z[4] + z[82];
    z[82]=z[82]*z[64];
    z[81]=z[3]*z[81];
    z[80]=z[82] - n<T>(1,12)*z[39] + z[80] + z[81];
    z[80]=z[10]*z[80];
    z[81]=z[1]*z[5];
    z[81]= - 2*z[81] + 2;
    z[81]=z[49]*z[26]*z[81];
    z[82]=npow(z[5],2);
    z[83]=z[8]*z[3];
    z[84]=z[82]*npow(z[6],4)*z[83];
    z[85]=npow(z[9],2);
    z[86]= - z[8]*z[85]*z[28];
    z[87]=z[2]*z[30]*npow(z[6],2);
    z[86]= - n<T>(1,4)*z[87] + z[86];
    z[85]=z[86]*z[85];
    z[84]=n<T>(25,4)*z[84] + z[85];
    z[84]=z[84]*npow(z[7],4);
    z[85]= - z[2] + z[4];
    z[85]=z[85]*z[23];
    z[85]= - n<T>(69,8)*z[26] + z[85];
    z[85]=z[3]*z[85];
    z[85]=n<T>(1,4)*z[28] + z[85];
    z[85]=z[5]*z[85];
    z[81]=z[84] + z[85] + z[81];
    z[81]=z[1]*z[81];
    z[84]=z[3]*z[12];
    z[85]= - 3*z[84] - z[19] - n<T>(23,3)*z[26];
    z[85]=z[85]*z[67];
    z[21]= - z[70]*z[21];
    z[21]=z[21] + z[28];
    z[86]=z[70]*z[11];
    z[87]= - n<T>(5,4)*z[13] + 3*z[86];
    z[87]=z[2]*z[87];
    z[87]=z[87] + n<T>(61,4)*z[26];
    z[88]=z[2]*z[23];
    z[87]=n<T>(1,2)*z[87] + z[88];
    z[87]=z[3]*z[87];
    z[17]=z[81] + z[33] + z[66] + z[17] + z[80] + z[73] + z[85] + n<T>(1,2)*
    z[21] + z[87];
    z[17]=z[1]*z[17];
    z[21]=n<T>(1,2)*z[12];
    z[33]=z[21] + n<T>(1,8)*z[2];
    z[66]=4*z[15];
    z[73]= - n<T>(43,8)*z[8] - z[66] + n<T>(1,4)*z[13] + z[33];
    z[73]=z[5]*z[73];
    z[80]=n<T>(5,4)*z[4];
    z[81]=2*z[15];
    z[85]=z[81] - n<T>(1,8)*z[12];
    z[85]=n<T>(1,8)*z[8] + 3*z[85] - z[80];
    z[85]=z[10]*z[85];
    z[52]= - n<T>(41,12)*z[4] - z[52];
    z[52]=z[8]*z[52];
    z[52]=z[85] + z[73] + z[52] - z[32] - 14*z[46];
    z[52]=z[10]*z[52];
    z[73]=n<T>(5,8)*z[2];
    z[85]=z[73] + z[15];
    z[87]=n<T>(3,8)*z[12];
    z[88]= - n<T>(11,8)*z[8] - z[87] - n<T>(5,8)*z[13] - z[85];
    z[88]=z[5]*z[88];
    z[88]=z[88] + 6*z[46] + n<T>(29,4)*z[39];
    z[88]=z[10]*z[88];
    z[89]= - z[32] - z[24];
    z[89]=z[5]*z[89];
    z[90]=z[4]*z[24];
    z[88]=z[88] - 6*z[90] + z[89];
    z[88]=z[10]*z[88];
    z[89]=z[39]*z[2];
    z[90]= - 5*z[28] - z[89];
    z[90]=z[90]*z[18];
    z[91]=z[2] + z[13];
    z[92]=z[91]*z[27];
    z[92]= - 5*z[70] + z[92];
    z[92]=z[4]*z[92]*z[31];
    z[72]=z[88] + z[92] + z[72] + z[90];
    z[72]=z[9]*z[72];
    z[88]=z[12] + z[68];
    z[88]=z[88]*z[18];
    z[90]=3*z[4];
    z[92]= - n<T>(65,6)*z[2] + z[90];
    z[92]=z[4]*z[92];
    z[88]=z[88] + n<T>(21,4)*z[19] + z[92];
    z[88]=z[8]*z[88];
    z[92]=3*z[8];
    z[93]=z[92]*z[5];
    z[94]=5*z[13];
    z[95]=n<T>(17,8)*z[4] - z[94] - n<T>(61,8)*z[2];
    z[95]=z[4]*z[95];
    z[95]= - z[93] - z[71] + z[95];
    z[95]=z[5]*z[95];
    z[96]=z[70]*z[37];
    z[97]=2*z[13];
    z[98]=z[97] + z[2];
    z[98]=z[4]*z[98];
    z[98]=z[70] + z[98];
    z[98]=z[4]*z[98];
    z[52]=z[72] + z[52] + z[95] + z[88] + z[96] + 2*z[98];
    z[52]=z[9]*z[52];
    z[72]=n<T>(25,8)*z[2];
    z[88]=6*z[15];
    z[95]= - z[88] - z[72];
    z[95]=z[4]*z[95];
    z[96]=5*z[5] + n<T>(43,3)*z[2] + z[4];
    z[96]=n<T>(1,2)*z[96] + z[10];
    z[96]=z[96]*z[63];
    z[98]=n<T>(37,6)*z[2] + z[8];
    z[98]=z[8]*z[98];
    z[99]=z[4] - 13*z[8];
    z[99]=n<T>(5,4)*z[99] + 6*z[5];
    z[99]=z[5]*z[99];
    z[95]=z[96] + z[99] + z[95] + z[98];
    z[95]=z[6]*z[95];
    z[66]= - n<T>(49,8)*z[4] + z[66] + z[72];
    z[66]=z[4]*z[66];
    z[72]=n<T>(13,3)*z[2];
    z[96]=n<T>(3,4)*z[10];
    z[98]= - z[96] - z[72] + n<T>(3,4)*z[4];
    z[98]=z[10]*z[98];
    z[99]=59*z[4];
    z[100]= - 35*z[2] + z[99];
    z[100]=n<T>(5,12)*z[100] + 6*z[8];
    z[100]=z[8]*z[100];
    z[101]= - 12*z[15] + n<T>(19,8)*z[12];
    z[101]=z[5]*z[101];
    z[66]=z[98] + z[101] + z[66] + z[100];
    z[66]=z[9]*z[66];
    z[98]=z[4]*z[14];
    z[100]= - n<T>(61,24)*z[98] - 6*z[42];
    z[100]=z[8]*z[100];
    z[101]= - z[81] + z[12];
    z[101]=z[101]*z[43];
    z[102]=z[19]*z[11];
    z[103]=z[14]*z[34];
    z[66]=z[66] + z[95] + z[103] + 8*z[101] + 21*z[102] + z[100];
    z[66]=z[9]*z[66];
    z[95]=13*z[5];
    z[100]=n<T>(3,2)*z[3];
    z[101]=z[95] - z[100] - 31*z[8];
    z[101]=z[101]*z[31];
    z[103]=n<T>(1,4)*z[3];
    z[104]= - z[63] - n<T>(8,3)*z[2] - z[103];
    z[104]=z[10]*z[104];
    z[105]=n<T>(5,2)*z[8];
    z[106]=z[105] - n<T>(5,3)*z[2] - n<T>(11,8)*z[3];
    z[106]=z[8]*z[106];
    z[101]=z[104] + z[101] + z[51] + z[106];
    z[101]=z[6]*z[101];
    z[104]=n<T>(9,4)*z[57] + 5*z[42];
    z[104]=z[8]*z[104];
    z[53]= - 7*z[53] + z[104];
    z[104]= - n<T>(17,4)*z[54] - 2*z[43];
    z[104]=z[5]*z[104];
    z[106]=3*z[59];
    z[107]=z[57] - z[106];
    z[107]=z[107]*z[64];
    z[53]=z[101] + z[107] + n<T>(1,2)*z[53] + z[104];
    z[53]=z[6]*z[53];
    z[51]=z[48]*z[51];
    z[101]=z[16] - z[78] - n<T>(5,8)*z[12];
    z[104]=z[48]*z[5];
    z[101]=z[101]*z[104];
    z[107]=z[16]*z[50];
    z[108]=3*z[62];
    z[109]= - z[107] - z[108];
    z[109]=z[109]*z[63];
    z[51]=z[66] + z[53] + z[109] + z[51] + z[101];
    z[51]=z[51]*npow(z[7],2);
    z[53]=n<T>(1,2) + z[79];
    z[53]=z[53]*z[16];
    z[66]=z[71]*z[11];
    z[101]=n<T>(15,4)*z[13] - z[66];
    z[101]=z[11]*z[101];
    z[101]=n<T>(61,8) + z[101];
    z[101]=z[2]*z[101];
    z[53]=z[53] + n<T>(15,4)*z[4] + z[101] - 5*z[86] - n<T>(5,4)*z[12] - z[78]
    + z[94];
    z[53]=z[3]*z[53];
    z[94]=z[19]*z[3];
    z[101]=z[12]*z[83];
    z[101]=n<T>(25,4)*z[94] + z[101];
    z[101]=z[8]*z[101];
    z[109]=z[24]*z[16];
    z[93]=z[3]*z[93];
    z[93]=z[109] + z[93];
    z[93]=z[5]*z[93];
    z[109]=n<T>(3,2)*z[84];
    z[110]= - z[10]*z[109];
    z[94]= - z[94] + z[110];
    z[94]=z[94]*z[64];
    z[93]=z[94] + z[101] + z[93];
    z[93]=z[6]*z[93];
    z[94]=3*z[12] + z[16];
    z[94]=z[8]*z[94];
    z[94]=z[94] + 23*z[19] + n<T>(11,2)*z[84];
    z[94]=z[8]*z[94];
    z[101]= - z[19] + n<T>(21,4)*z[26];
    z[101]=z[3]*z[101];
    z[74]=z[94] - z[74] + z[101];
    z[36]=z[36] + 11*z[3];
    z[36]=z[3]*z[36];
    z[36]=n<T>(17,2)*z[20] + z[36];
    z[94]= - n<T>(45,2)*z[3] + z[8];
    z[94]=z[8]*z[94];
    z[36]=n<T>(1,2)*z[36] + z[94];
    z[94]=z[5]*z[3];
    z[36]=n<T>(1,2)*z[36] - z[94];
    z[36]=z[5]*z[36];
    z[94]=n<T>(3,2)*z[10];
    z[94]=z[12]*z[94];
    z[94]=z[19] + z[94];
    z[94]=z[94]*z[64];
    z[36]=z[93] + z[94] + n<T>(1,2)*z[74] + z[36];
    z[36]=z[6]*z[36];
    z[74]=n<T>(3,2)*z[2];
    z[93]= - z[48]*z[74];
    z[93]=2*z[11] + z[93];
    z[93]=z[3]*z[93];
    z[94]=n<T>(45,4) + z[79];
    z[93]=n<T>(1,2)*z[94] + z[93];
    z[93]=z[3]*z[93];
    z[94]=z[2]*z[60];
    z[94]= - static_cast<T>(1)+ z[94];
    z[94]=z[94]*z[31];
    z[93]=z[94] + n<T>(13,4)*z[8] + z[93] + n<T>(13,8)*z[4] - 8*z[2] + z[21] - 
    z[78] - 7*z[13];
    z[93]=z[5]*z[93];
    z[94]=z[14]*z[13];
    z[101]=5*z[94];
    z[110]=n<T>(13,2) + z[101];
    z[110]=z[110]*z[57];
    z[111]=static_cast<T>(1)- z[101];
    z[110]= - z[106] + n<T>(3,2)*z[111] + z[110];
    z[110]=z[110]*z[64];
    z[111]= - n<T>(23,2) - 19*z[94];
    z[103]=z[111]*z[103];
    z[111]=n<T>(1,4)*z[12];
    z[68]=z[110] - n<T>(41,12)*z[8] + z[103] + z[68] - n<T>(11,12)*z[2] + z[111]
    - 9*z[15] + n<T>(43,8)*z[13];
    z[68]=z[10]*z[68];
    z[75]= - z[75] + n<T>(3,2)*z[86];
    z[75]=z[2]*z[75];
    z[103]=n<T>(7,2)*z[4];
    z[110]=z[81] + n<T>(3,2)*z[13];
    z[110]= - z[103] + 3*z[110] + n<T>(161,24)*z[2];
    z[110]=z[4]*z[110];
    z[112]=4*z[4];
    z[113]=5*z[12];
    z[114]= - z[113] - n<T>(23,2)*z[2];
    z[114]=n<T>(3,4)*z[8] - n<T>(13,4)*z[3] + n<T>(1,2)*z[114] + z[112];
    z[114]=z[8]*z[114];
    z[17]=z[17] + z[51] + z[52] + z[36] + z[68] + z[93] + z[114] + z[53]
    + z[110] - z[19] + z[75];
    z[17]=z[1]*z[17];
    z[36]=z[81] + n<T>(1,8)*z[13];
    z[33]=z[105] + z[36] + z[33];
    z[33]=z[33]*z[38];
    z[51]=n<T>(5,2)*z[91] + z[5];
    z[51]=z[51]*z[64];
    z[33]=z[51] + z[33] - 12*z[46] - n<T>(161,12)*z[39];
    z[33]=z[10]*z[33];
    z[46]=z[80] + z[97] + n<T>(7,8)*z[2];
    z[46]=z[4]*z[46];
    z[46]= - n<T>(3,2)*z[24] + z[32] + z[46];
    z[46]=z[5]*z[46];
    z[51]=n<T>(7,2)*z[13] + 5*z[2];
    z[52]=z[14]*z[71];
    z[51]=n<T>(1,2)*z[51] + z[52];
    z[51]=z[4]*z[51];
    z[52]=n<T>(5,2)*z[70];
    z[51]=z[52] + z[51];
    z[51]=z[4]*z[51];
    z[53]= - n<T>(131,3)*z[2] + n<T>(55,4)*z[4];
    z[53]=z[4]*z[53];
    z[53]=z[53] + n<T>(39,2)*z[39];
    z[53]=z[53]*z[18];
    z[68]= - 11*z[28] - z[89];
    z[68]=z[9]*z[68]*z[67];
    z[33]=z[68] + z[33] + z[46] + z[51] + z[53];
    z[33]=z[9]*z[33];
    z[28]= - z[28] - z[40];
    z[20]=n<T>(1,4)*z[20] - z[24];
    z[20]=5*z[20] + 9*z[22];
    z[20]=z[5]*z[20];
    z[46]= - n<T>(25,2)*z[2] + z[90];
    z[46]=n<T>(1,4)*z[46] - z[10];
    z[46]=z[46]*z[34];
    z[51]=z[30]*z[6];
    z[53]= - z[29]*z[51];
    z[20]=z[53] + z[46] + n<T>(3,2)*z[28] + z[20];
    z[20]=z[6]*z[20];
    z[28]=z[11]*z[32];
    z[28]= - z[38] + n<T>(45,4)*z[8] + z[112] + n<T>(5,4)*z[2] + z[28] + 7*z[36]
    + n<T>(17,4)*z[12];
    z[28]=z[5]*z[28];
    z[32]= - n<T>(29,2)*z[12] - 27*z[2];
    z[36]=n<T>(33,2) - 2*z[98];
    z[36]=z[4]*z[36];
    z[46]=static_cast<T>(8)- n<T>(1,2)*z[98];
    z[46]=z[8]*z[46];
    z[32]=z[46] + n<T>(1,2)*z[32] + z[36];
    z[32]=z[8]*z[32];
    z[21]=z[21] - z[81];
    z[36]=z[13] + z[21];
    z[46]= - static_cast<T>(5)- n<T>(3,2)*z[94];
    z[46]=n<T>(5,2)*z[46] - z[59];
    z[46]=z[46]*z[63];
    z[36]=z[46] - n<T>(11,4)*z[5] - n<T>(37,12)*z[8] + z[76] + 3*z[36] - n<T>(25,12)
   *z[2];
    z[36]=z[10]*z[36];
    z[46]= - z[13]*z[74];
    z[53]=z[70]*z[14];
    z[68]= - n<T>(33,4)*z[4] + n<T>(15,2)*z[53] + n<T>(37,24)*z[2] + 13*z[15] + n<T>(21,4)*z[13];
    z[68]=z[4]*z[68];
    z[20]=z[33] + z[20] + z[36] + z[28] + z[32] + z[68] + z[46] + z[52]
    + 19*z[19];
    z[20]=z[9]*z[20];
    z[28]=n<T>(11,4)*z[8];
    z[32]=z[28] + 6*z[3];
    z[32]=z[32]*z[5];
    z[28]=9*z[3] - z[28];
    z[28]=z[8]*z[28];
    z[28]=z[32] - n<T>(17,4)*z[49] + z[28];
    z[28]=z[5]*z[28];
    z[33]= - n<T>(15,4)*z[84] - z[83];
    z[33]=z[8]*z[33];
    z[36]= - z[10] + z[4] + z[100];
    z[36]=z[10]*z[36];
    z[36]=z[109] + z[36];
    z[36]=z[36]*z[64];
    z[46]=z[5]*z[83];
    z[25]= - z[25] - 25*z[46];
    z[25]=z[5]*z[25];
    z[46]= - z[4]*z[30];
    z[25]=z[25] + z[46];
    z[46]=n<T>(1,4)*z[6];
    z[25]=z[25]*z[46];
    z[25]=z[25] + z[36] + z[33] + z[28];
    z[25]=z[6]*z[25];
    z[28]= - static_cast<T>(1)- 11*z[57];
    z[28]=n<T>(1,8)*z[28] + z[59];
    z[28]=z[10]*z[28];
    z[21]=z[28] - n<T>(17,12)*z[8] + z[16] - 3*z[21] + n<T>(7,12)*z[2];
    z[21]=z[10]*z[21];
    z[28]= - z[74] + z[81] + n<T>(9,4)*z[12];
    z[23]=3*z[28] - z[23];
    z[23]=z[3]*z[23];
    z[28]= - n<T>(15,2) + z[57];
    z[28]=z[8]*z[28];
    z[28]=z[28] + n<T>(23,2)*z[3] - n<T>(27,2)*z[12] + 11*z[2];
    z[28]=z[28]*z[18];
    z[33]= - static_cast<T>(19)+ n<T>(11,2)*z[54];
    z[33]=z[3]*z[33];
    z[36]= - static_cast<T>(5)- z[54];
    z[36]=z[5]*z[36];
    z[33]=z[36] - n<T>(19,2)*z[8] + n<T>(49,4)*z[4] + z[33];
    z[33]=z[33]*z[31];
    z[36]= - z[78] - n<T>(31,24)*z[2];
    z[36]=z[4]*z[36];
    z[21]=z[25] + z[21] + z[33] + z[28] + z[23] + n<T>(39,2)*z[19] + z[36];
    z[21]=z[6]*z[21];
    z[23]=z[66] - z[87] - z[78] - n<T>(11,4)*z[13];
    z[23]=z[11]*z[23];
    z[25]=z[11]*z[13];
    z[28]=static_cast<T>(17)+ n<T>(9,2)*z[25];
    z[28]=z[28]*z[79];
    z[33]= - n<T>(33,4)*z[13] + 7*z[86];
    z[33]=z[33]*z[45];
    z[23]=2*z[54] + z[33] + n<T>(1,4)*z[28] + n<T>(25,8) + z[23];
    z[23]=z[3]*z[23];
    z[28]=z[111] + z[88] - n<T>(25,8)*z[13];
    z[28]=z[11]*z[28];
    z[33]= - z[2]*z[48];
    z[33]=z[60] - n<T>(1,2)*z[11] + z[33];
    z[33]=z[33]*z[100];
    z[36]= - z[16]*z[104];
    z[28]=z[36] + z[33] - n<T>(11,4)*z[79] + n<T>(9,2) + z[28];
    z[28]=z[5]*z[28];
    z[33]= - z[42] - z[58] + n<T>(377,12) + z[98];
    z[33]=z[33]*z[18];
    z[36]=n<T>(23,2) + 9*z[94];
    z[36]=z[36]*z[55];
    z[49]= - static_cast<T>(19)- z[101];
    z[49]=z[49]*z[107];
    z[58]=static_cast<T>(7)+ n<T>(15,2)*z[94];
    z[58]=z[14]*z[58];
    z[49]=z[58] + z[49];
    z[49]=n<T>(1,2)*z[49] + z[62];
    z[49]=z[10]*z[49];
    z[58]=static_cast<T>(1)- n<T>(3,4)*z[94];
    z[36]=z[49] + 13*z[58] + z[36];
    z[36]=z[36]*z[63];
    z[19]=z[52] - z[19];
    z[19]=z[11]*z[19];
    z[25]= - n<T>(139,6) - 9*z[25];
    z[25]=z[25]*z[37];
    z[49]=z[15] + z[13];
    z[52]=n<T>(47,24) + z[94];
    z[52]=z[4]*z[52];
    z[17]=z[17] + z[20] + z[21] + z[36] + z[28] + z[33] + z[23] + z[52]
    + n<T>(11,2)*z[53] + z[25] + z[19] + 3*z[49] - z[113];
    z[17]=z[1]*z[17];
    z[19]= - z[24]*z[38];
    z[20]= - z[34]*z[37];
    z[19]=z[20] + z[40] + z[19];
    z[20]=n<T>(1,2)*z[6];
    z[19]=z[19]*z[20];
    z[21]= - z[85]*z[90];
    z[23]=n<T>(10,3)*z[2];
    z[25]=z[23] - z[44];
    z[25]=z[8]*z[25];
    z[28]= - static_cast<T>(2)- n<T>(9,4)*z[59];
    z[28]=z[10]*z[28];
    z[28]= - z[31] + z[28];
    z[28]=z[10]*z[28];
    z[19]=z[19] + z[28] + n<T>(9,4)*z[77] + z[21] + z[25];
    z[19]=z[6]*z[19];
    z[21]=n<T>(43,3) - n<T>(5,2)*z[98];
    z[21]=z[21]*z[27];
    z[25]=n<T>(1,2) - z[98];
    z[25]=z[25]*z[92];
    z[28]= - z[23] + n<T>(19,8)*z[4];
    z[28]=z[4]*z[28];
    z[28]=z[28] + n<T>(3,2)*z[39];
    z[28]=z[9]*z[28];
    z[21]=z[28] + z[25] - z[23] + z[21];
    z[21]=z[8]*z[21];
    z[23]= - z[103] - z[78] + z[23];
    z[23]=z[4]*z[23];
    z[25]=static_cast<T>(1)+ z[59];
    z[28]=z[2] + z[96];
    z[28]=z[6]*z[28];
    z[25]=z[28] + n<T>(3,4)*z[25];
    z[25]=z[34]*z[25];
    z[21]=z[23] + z[25] + z[21];
    z[21]=z[9]*z[21];
    z[23]=z[50]*z[27];
    z[23]= - z[14] + z[23];
    z[23]=z[23]*z[92];
    z[25]=n<T>(151,4) - 23*z[98];
    z[23]=n<T>(1,6)*z[25] + z[23];
    z[23]=z[8]*z[23];
    z[25]=5*z[15] + 12*z[12];
    z[25]=2*z[25] - n<T>(15,2)*z[102];
    z[25]=z[25]*z[43];
    z[27]= - z[14] - 9*z[62];
    z[27]=z[27]*z[63];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[63];
    z[19]=z[21] + z[19] + z[27] + z[25] + z[23] - z[78] - n<T>(47,6)*z[4];
    z[19]=z[9]*z[19];
    z[21]= - z[72] - 15*z[8];
    z[21]=z[21]*z[18];
    z[23]=9*z[5];
    z[25]=n<T>(59,2)*z[8] - z[23];
    z[25]=z[5]*z[25];
    z[27]=z[10] - z[37] - z[5];
    z[27]=z[10]*z[27];
    z[21]=z[27] + z[21] + z[25];
    z[22]= - z[24] + 2*z[22];
    z[22]=z[6]*z[22]*z[38];
    z[21]=n<T>(1,2)*z[21] + z[22];
    z[21]=z[6]*z[21];
    z[22]=z[14] + n<T>(9,2)*z[62];
    z[22]=z[10]*z[22];
    z[22]= - static_cast<T>(5)+ z[22];
    z[22]=z[22]*z[63];
    z[24]=z[14]*z[92];
    z[24]= - n<T>(23,12) + z[24];
    z[24]=z[8]*z[24];
    z[21]=z[21] + z[22] - n<T>(7,2)*z[5] - z[73] + z[24];
    z[21]=z[6]*z[21];
    z[22]=z[92]*z[50];
    z[24]= - n<T>(67,6)*z[14] + z[22];
    z[24]=z[24]*z[18];
    z[25]=z[81] + z[113];
    z[25]=z[11]*z[25];
    z[25]= - static_cast<T>(14)+ 3*z[25];
    z[25]=z[25]*z[43];
    z[27]=z[56]*z[10];
    z[28]= - z[50] + z[27];
    z[28]=z[10]*z[28];
    z[28]= - z[14] + n<T>(9,2)*z[28];
    z[28]=z[28]*z[63];
    z[33]=z[15] + 2*z[12];
    z[33]=z[11]*z[33];
    z[33]= - n<T>(1,3) + 6*z[33];
    z[19]=z[19] + z[21] + z[28] + z[25] + 2*z[33] + z[24];
    z[19]=z[9]*z[19];
    z[21]=z[27] - 3*z[50] + z[65];
    z[21]=z[21]*z[63];
    z[21]=z[21] - z[14] + z[107];
    z[21]=z[21]*z[63];
    z[24]=z[92] + 7*z[3];
    z[25]= - z[5]*z[24];
    z[25]=n<T>(21,2)*z[83] + z[25];
    z[25]=z[5]*z[25];
    z[28]=z[82]*z[6];
    z[33]=z[28]*z[92]*z[3];
    z[25]=z[25] + z[33];
    z[25]=z[25]*z[46];
    z[33]= - n<T>(17,8)*z[3] - z[8];
    z[36]=static_cast<T>(2)+ n<T>(5,4)*z[54];
    z[36]=z[5]*z[36];
    z[33]=3*z[33] + z[36];
    z[33]=z[5]*z[33];
    z[25]=z[25] + n<T>(15,8)*z[83] + z[33];
    z[25]=z[6]*z[25];
    z[33]= - 5*z[11] - z[60];
    z[33]=z[33]*z[41];
    z[33]=z[33] + static_cast<T>(7)+ n<T>(37,8)*z[54];
    z[33]=z[5]*z[33];
    z[36]= - static_cast<T>(1)- z[55];
    z[36]=z[8]*z[36];
    z[36]= - n<T>(37,2)*z[3] + 9*z[36];
    z[25]=z[25] + n<T>(1,4)*z[36] + z[33];
    z[25]=z[6]*z[25];
    z[33]=n<T>(1,8)*z[3];
    z[36]=27*z[11] + 23*z[14];
    z[36]=z[36]*z[33];
    z[40]= - 4*z[11] - n<T>(7,8)*z[60];
    z[40]=z[5]*z[40];
    z[21]=z[25] + z[21] + z[40] + n<T>(19,12)*z[42] + static_cast<T>(5)+ z[36];
    z[21]=z[6]*z[21];
    z[25]=z[56]*z[16];
    z[25]=z[25] - z[50];
    z[36]=npow(z[14],4);
    z[40]=z[36]*z[3];
    z[40]= - z[40] + n<T>(9,2)*z[56];
    z[41]=z[36]*z[10];
    z[46]=z[41] - z[40];
    z[46]=z[10]*z[46];
    z[46]=z[46] + z[25];
    z[46]=z[10]*z[46];
    z[49]=n<T>(5,2)*z[48];
    z[52]= - n<T>(17,2)*z[11] - 3*z[14];
    z[52]=z[14]*z[52];
    z[52]= - z[49] + z[52];
    z[52]=z[52]*z[16];
    z[46]=z[46] + z[52] - n<T>(11,2)*z[11] - n<T>(13,3)*z[14];
    z[21]=n<T>(1,2)*z[46] + z[21];
    z[21]=z[6]*z[21];
    z[39]=z[9]*z[39];
    z[39]=z[39] - z[4];
    z[46]=n<T>(2,3)*z[2];
    z[52]= - z[46] + n<T>(9,8)*z[4];
    z[39]=z[52]*z[39];
    z[52]=n<T>(43,3) - n<T>(9,2)*z[98];
    z[29]=z[52]*z[29];
    z[29]= - z[46] + z[29];
    z[29]=z[8]*z[29];
    z[37]=z[37]*z[51];
    z[30]=z[30] + z[37];
    z[20]=z[30]*z[20];
    z[20]=z[20] + n<T>(1,4)*z[47] + z[29] + z[39];
    z[20]=z[9]*z[20];
    z[29]=z[8]*z[2];
    z[26]= - z[26] + z[29];
    z[29]=n<T>(3,4) - 2*z[59];
    z[29]=z[29]*z[34];
    z[26]= - z[51] + n<T>(2,3)*z[26] + z[29];
    z[26]=z[6]*z[26];
    z[29]=static_cast<T>(59)- 43*z[98];
    z[29]=z[8]*z[29];
    z[29]= - z[99] + z[29];
    z[30]= - z[62] + n<T>(3,4)*z[14];
    z[37]=z[30]*z[34];
    z[20]=z[20] + z[26] + n<T>(1,24)*z[29] + z[37];
    z[20]=z[9]*z[20];
    z[26]= - z[15] - z[12];
    z[26]=z[26]*z[48]*z[38];
    z[29]= - n<T>(11,4)*z[14] + z[108];
    z[29]=z[29]*z[34];
    z[34]= - n<T>(1,2) + z[106];
    z[34]=z[34]*z[35];
    z[35]= - z[8]*z[46];
    z[34]=z[35] + z[34];
    z[34]=z[6]*z[34];
    z[29]=z[34] - n<T>(4,3)*z[8] + z[29];
    z[29]=z[6]*z[29];
    z[34]= - 5*z[50] + 3*z[27];
    z[34]=z[10]*z[34];
    z[34]=z[45] + z[34];
    z[34]=z[34]*z[63];
    z[20]=z[20] + z[29] + z[34] - n<T>(2,3)*z[42] + z[26];
    z[20]=z[9]*z[20];
    z[26]= - z[44] + z[5];
    z[26]=z[5]*z[26];
    z[29]= - z[8]*z[28];
    z[26]=z[26] + z[29];
    z[26]=z[6]*z[26];
    z[29]=n<T>(5,6)*z[8] + z[23];
    z[30]=z[10]*z[30];
    z[30]=n<T>(1,4) + z[30];
    z[30]=z[10]*z[30];
    z[26]=n<T>(3,2)*z[26] + n<T>(1,4)*z[29] + z[30];
    z[26]=z[6]*z[26];
    z[29]=n<T>(15,4)*z[50] - 2*z[27];
    z[29]=z[10]*z[29];
    z[29]=n<T>(1,4)*z[14] + z[29];
    z[29]=z[10]*z[29];
    z[30]= - static_cast<T>(1)- z[42];
    z[26]=z[26] + n<T>(11,24)*z[30] + z[29];
    z[26]=z[6]*z[26];
    z[29]=3*z[56] - z[41];
    z[29]=z[10]*z[29];
    z[29]= - n<T>(1,2)*z[50] + z[29];
    z[29]=z[10]*z[29];
    z[20]=z[20] + z[26] + n<T>(5,24)*z[14] + z[29];
    z[20]=z[9]*z[20];
    z[26]=z[11] - z[45];
    z[26]=z[14]*z[26];
    z[26]=n<T>(3,4)*z[48] + z[26];
    z[26]=z[26]*z[57];
    z[16]=z[63] + z[16];
    z[16]=z[16]*npow(z[14],5);
    z[16]= - 3*z[36] + z[16];
    z[16]=z[10]*z[16];
    z[16]=n<T>(1,2)*z[56] + z[16];
    z[16]=z[10]*z[16];
    z[29]=n<T>(13,6)*z[11] + z[14];
    z[29]=z[14]*z[29];
    z[16]=z[16] + z[29] + z[26];
    z[16]=z[20] + n<T>(1,2)*z[16] + z[21];
    z[16]=z[7]*z[16];
    z[20]= - z[50] + z[65];
    z[21]= - n<T>(3,2)*z[41] + z[40];
    z[21]=z[10]*z[21];
    z[20]=n<T>(1,2)*z[20] + z[21];
    z[20]=z[10]*z[20];
    z[21]=z[14]*z[11];
    z[21]= - z[49] + z[21];
    z[21]=z[3]*z[21];
    z[23]= - z[48]*z[23];
    z[26]=n<T>(125,2)*z[11] - 37*z[14];
    z[20]=z[20] + z[23] + z[22] + n<T>(1,6)*z[26] + z[21];
    z[21]= - n<T>(21,4)*z[3] - z[8];
    z[21]=z[21]*z[44];
    z[21]=z[21] + z[32];
    z[21]=z[5]*z[21];
    z[22]=z[83]*z[28];
    z[21]=z[21] - n<T>(13,4)*z[22];
    z[21]=z[6]*z[21];
    z[18]= - z[24]*z[18];
    z[22]= - static_cast<T>(11)- n<T>(13,2)*z[54];
    z[22]=z[22]*z[31];
    z[23]=n<T>(37,8)*z[3] + 5*z[8];
    z[22]=3*z[23] + z[22];
    z[22]=z[5]*z[22];
    z[18]=z[21] + z[18] + z[22];
    z[18]=z[6]*z[18];
    z[21]=9*z[14] - z[61];
    z[21]=z[8]*z[21];
    z[21]=z[21] + n<T>(269,6) + 5*z[57];
    z[21]=z[21]*z[67];
    z[22]= - static_cast<T>(257)- 43*z[54];
    z[23]=z[60] + n<T>(9,2)*z[11];
    z[23]=z[5]*z[23];
    z[22]=n<T>(1,4)*z[22] + z[23];
    z[22]=z[22]*z[31];
    z[23]= - z[14]*z[63];
    z[23]=z[23] + n<T>(3,2) - z[57];
    z[23]=z[23]*z[63];
    z[18]=z[18] + z[23] + z[22] + 5*z[3] + z[21];
    z[18]=z[6]*z[18];
    z[21]= - z[50]*z[44];
    z[21]=z[21] + n<T>(1,3)*z[14] + z[107];
    z[21]=z[8]*z[21];
    z[22]= - n<T>(3,4)*z[27] - z[25];
    z[22]=z[10]*z[22];
    z[23]=5*z[14] - z[61];
    z[22]=n<T>(1,2)*z[23] + z[22];
    z[22]=z[10]*z[22];
    z[23]=173*z[11] - 3*z[60];
    z[23]=z[5]*z[23];
    z[18]=z[18] + z[22] + n<T>(1,8)*z[23] + z[21] - n<T>(50,3) - n<T>(3,2)*z[57];
    z[18]=z[6]*z[18];
    z[16]=z[16] + z[19] + n<T>(1,2)*z[20] + z[18];
    z[16]=z[7]*z[16];
    z[18]=z[79] + z[59];
    z[19]= - 13*z[11] - 11*z[14];
    z[19]=z[19]*z[33];
    z[20]=z[81] + n<T>(39,8)*z[12];
    z[20]= - n<T>(35,6)*z[10] + n<T>(55,2)*z[5] - n<T>(67,6)*z[8] + z[69] + 3*z[20]
    + n<T>(121,12)*z[2];
    z[20]=z[6]*z[20];
    z[21]=n<T>(47,6)*z[10] - z[95] + n<T>(605,24)*z[8] - n<T>(55,6)*z[4] - n<T>(133,24)*
    z[2] - z[88] - n<T>(67,8)*z[12];
    z[21]=z[9]*z[21];
    z[22]= - z[78] - n<T>(25,4)*z[12];
    z[22]=z[11]*z[22];

    r += n<T>(397,24) + z[16] + z[17] - n<T>(14,3)*z[18] + z[19] + z[20] + 
      z[21] + z[22] - n<T>(65,12)*z[42] - n<T>(83,8)*z[43];
 
    return r;
}

template double qg_2lNLC_r342(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r342(const std::array<dd_real,31>&);
#endif
