#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r113(const std::array<T,31>& k) {
  T z[146];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[12];
    z[9]=k[13];
    z[10]=k[8];
    z[11]=k[2];
    z[12]=k[14];
    z[13]=k[9];
    z[14]=k[3];
    z[15]=npow(z[1],3);
    z[16]=z[15]*z[2];
    z[17]=npow(z[3],2);
    z[18]=z[16]*z[17];
    z[19]=7*z[18];
    z[20]=npow(z[2],2);
    z[21]=3*z[20];
    z[22]=z[21] - z[19];
    z[22]=z[3]*z[22];
    z[23]=2*z[7];
    z[24]=z[3]*z[2];
    z[25]= - z[24]*z[23];
    z[22]=z[22] + z[25];
    z[22]=z[7]*z[22];
    z[25]=z[15]*npow(z[3],3);
    z[26]=z[25]*z[20];
    z[27]=z[2]*z[1];
    z[28]=2*z[27];
    z[29]= - z[28] + z[26];
    z[22]=4*z[29] + z[22];
    z[22]=z[22]*z[23];
    z[29]=z[4]*z[7];
    z[30]=z[29] - 1;
    z[31]=npow(z[1],4);
    z[30]=z[31]*z[30];
    z[32]=z[15]*z[7];
    z[30]= - z[32] + z[16] + z[30];
    z[30]=z[4]*z[30];
    z[33]=npow(z[1],2);
    z[34]=z[33]*z[20];
    z[35]=2*z[20];
    z[36]=z[3]*z[1];
    z[36]=npow(z[36],4);
    z[37]= - z[36]*z[35];
    z[38]=z[20]*z[3];
    z[39]= - z[7]*z[38];
    z[37]=z[37] + z[39];
    z[37]=z[7]*z[37];
    z[39]=z[20]*z[1];
    z[37]= - z[39] + z[37];
    z[37]=z[37]*z[23];
    z[34]=z[34] + z[37];
    z[34]=z[10]*z[34];
    z[37]=z[27] - z[33];
    z[40]=2*z[2];
    z[41]=z[37]*z[40];
    z[42]=7*z[15];
    z[22]=z[34] + 3*z[30] + z[22] + z[42] + z[41];
    z[22]=z[9]*z[22];
    z[30]=3*z[7];
    z[34]=z[4]*z[31]*z[30];
    z[43]=2*z[31];
    z[44]= - z[7]*z[42];
    z[34]=z[34] + z[44] - z[43] + 3*z[16];
    z[34]=z[4]*z[34];
    z[44]=z[33]*z[2];
    z[45]=2*z[44];
    z[46]=3*z[15];
    z[47]=z[33]*z[7];
    z[34]=z[34] + n<T>(19,2)*z[47] - z[46] - z[45];
    z[34]=z[4]*z[34];
    z[48]=z[44] - z[15];
    z[49]=z[48]*z[4];
    z[50]= - z[49] + 5*z[37];
    z[50]=z[2]*z[50];
    z[51]= - z[38]*z[23];
    z[51]=z[51] - 8*z[27] + n<T>(7,2)*z[26];
    z[51]=z[7]*z[51];
    z[50]=z[51] + z[50];
    z[50]=z[10]*z[50];
    z[51]=z[24]*z[33];
    z[52]=5*z[20] - 8*z[51];
    z[52]=z[3]*z[52];
    z[53]=3*z[2];
    z[54]=z[33]*z[3];
    z[55]= - z[53] + z[54];
    z[56]=z[30]*z[3];
    z[55]=z[55]*z[56];
    z[57]=7*z[1];
    z[52]=z[55] + z[57] + z[52];
    z[52]=z[7]*z[52];
    z[55]=z[33]*z[10];
    z[58]=z[55]*z[20];
    z[58]= - z[58] + z[48] - z[47];
    z[59]=3*z[12];
    z[60]=z[58]*z[59];
    z[61]=2*z[15];
    z[62]=z[61] - z[44];
    z[62]=z[62]*z[2];
    z[62]=z[62] - z[31];
    z[62]=z[62]*z[4];
    z[63]= - z[62] + z[15] - z[39];
    z[63]=z[5]*z[63];
    z[64]=z[17]*z[33];
    z[65]=z[64]*z[20];
    z[66]=n<T>(7,2)*z[33];
    z[67]=5*z[27];
    z[68]= - z[66] + z[67];
    z[22]=z[22] + z[63] + z[60] + z[50] + z[34] + z[52] + 3*z[68] - n<T>(5,2)
   *z[65];
    z[22]=z[9]*z[22];
    z[34]=3*z[48];
    z[48]=z[16] - 9*z[32];
    z[48]=z[4]*z[48];
    z[48]=z[48] - z[34] + 8*z[47];
    z[48]=z[4]*z[48];
    z[50]=2*z[33];
    z[52]=z[7]*z[1];
    z[48]=z[48] - n<T>(9,4)*z[52] - z[50] + n<T>(33,4)*z[27];
    z[48]=z[4]*z[48];
    z[60]=n<T>(1,2)*z[27];
    z[63]=z[60] - z[50];
    z[68]= - n<T>(1,4)*z[65] + z[63];
    z[69]=z[17]*z[7];
    z[70]=n<T>(1,2)*z[69];
    z[71]=z[44]*z[70];
    z[72]=5*z[33];
    z[73]=z[72] - z[28];
    z[73]=z[2]*z[73];
    z[73]= - z[46] + z[73];
    z[73]=z[4]*z[73];
    z[68]=z[73] + 3*z[68] + z[71];
    z[68]=z[10]*z[68];
    z[71]=n<T>(1,2)*z[12];
    z[58]=z[58]*z[71];
    z[73]=z[10]*z[1];
    z[35]=z[73]*z[35];
    z[74]=n<T>(1,2)*z[52];
    z[35]=z[58] + z[35] + z[74] - z[63];
    z[35]=z[35]*z[59];
    z[58]=3*z[27];
    z[63]=z[50] + z[58];
    z[63]=z[2]*z[63];
    z[49]= - z[40]*z[49];
    z[49]=z[49] - z[46] + z[63];
    z[49]=z[4]*z[49];
    z[63]=z[21]*z[64];
    z[49]=z[49] - z[50] - z[63];
    z[49]=z[5]*z[49];
    z[75]=n<T>(83,2)*z[1] - 5*z[2];
    z[76]=6*z[24];
    z[77]=n<T>(11,2) - z[76];
    z[77]=z[7]*z[77];
    z[22]=z[22] + z[49] + z[35] + z[68] + z[48] + z[77] + n<T>(1,2)*z[75] + 
    z[38];
    z[22]=z[9]*z[22];
    z[35]=z[7]*z[3];
    z[48]=9*z[35];
    z[49]=10*z[64] + z[48];
    z[49]=z[7]*z[49];
    z[68]= - z[30]*z[25];
    z[68]= - z[1] + z[68];
    z[75]=z[12]*z[7];
    z[68]=z[68]*z[75];
    z[49]=z[68] + 6*z[55] - z[57] + z[49];
    z[49]=z[12]*z[49];
    z[68]= - 5*z[25] - z[56];
    z[68]=z[7]*z[68];
    z[77]=npow(z[7],2);
    z[71]=z[71]*z[77];
    z[78]= - z[36]*z[71];
    z[79]=3*z[1];
    z[68]=z[78] - 3*z[55] + z[79] + z[68];
    z[68]=z[12]*z[68];
    z[78]=3*z[69];
    z[80]=8*z[3] + z[78];
    z[80]=z[80]*z[30];
    z[68]=z[68] + 3*z[73] - n<T>(1,2)*z[64] + z[80];
    z[68]=z[12]*z[68];
    z[80]=z[69] + z[3];
    z[81]= - z[80]*z[75];
    z[81]=z[81] + z[3] + 2*z[69];
    z[81]=z[12]*z[81];
    z[81]= - z[17] + z[81];
    z[71]=z[17]*z[71];
    z[71]= - z[69] + z[71];
    z[71]=z[12]*z[71];
    z[71]=n<T>(1,2)*z[17] + z[71];
    z[71]=z[11]*z[12]*z[71];
    z[71]=2*z[81] + z[71];
    z[71]=z[11]*z[71];
    z[81]=3*z[24];
    z[82]= - static_cast<T>(7)+ z[81];
    z[82]=z[3]*z[82];
    z[71]=z[71] + z[82] - z[78];
    z[78]=3*z[64];
    z[82]=z[78] - 5*z[73];
    z[83]=z[25]*z[2];
    z[84]=z[5]*z[83];
    z[82]=n<T>(1,2)*z[82] - 4*z[84];
    z[82]=z[5]*z[82];
    z[68]=z[82] + z[68] + 3*z[71];
    z[68]=z[11]*z[68];
    z[71]=z[44]*z[17];
    z[82]= - z[27] + z[26];
    z[82]=z[5]*z[82];
    z[82]=12*z[82] - 10*z[55] - z[1] + n<T>(11,2)*z[71];
    z[82]=z[5]*z[82];
    z[84]=n<T>(109,4)*z[2] - 6*z[38];
    z[84]=z[3]*z[84];
    z[49]=z[68] + z[82] + z[49] + n<T>(3,2)*z[73] - z[48] + n<T>(5,2) + z[84];
    z[49]=z[11]*z[49];
    z[68]=z[33] + 4*z[27];
    z[82]= - 2*z[26] + z[68];
    z[82]=z[7]*z[82];
    z[84]=npow(z[24],3);
    z[85]=z[84]*z[15];
    z[86]=z[85] - z[15];
    z[87]=16*z[27];
    z[88]= - 17*z[33] + z[87];
    z[88]=z[2]*z[88];
    z[89]=3*z[33];
    z[90]=z[89] - z[27];
    z[90]=z[2]*z[90];
    z[90]= - z[61] + z[90];
    z[90]=z[2]*z[90];
    z[90]=z[90] - z[32];
    z[91]=4*z[4];
    z[90]=z[90]*z[91];
    z[82]=z[90] + z[82] + z[88] - 6*z[86];
    z[82]=z[4]*z[82];
    z[86]=z[38]*z[33];
    z[88]=npow(z[2],3);
    z[90]=3*z[88] + 13*z[86];
    z[90]=z[3]*z[90];
    z[92]=z[21] + n<T>(13,2)*z[51];
    z[92]=z[3]*z[92];
    z[92]= - n<T>(15,2)*z[1] + z[92];
    z[92]=z[7]*z[92];
    z[93]=27*z[27];
    z[94]=13*z[33];
    z[82]=z[82] + z[92] + z[90] + z[94] - z[93];
    z[82]=z[4]*z[82];
    z[90]=67*z[33] - 93*z[27];
    z[90]=z[2]*z[90];
    z[92]=13*z[15];
    z[90]=39*z[85] - z[92] + z[90];
    z[95]= - 33*z[33] + 28*z[27];
    z[95]=z[2]*z[95];
    z[95]=10*z[15] + z[95];
    z[95]=z[2]*z[95];
    z[96]=6*z[27];
    z[97]=z[94] - z[96];
    z[97]=z[2]*z[97];
    z[97]= - z[42] + z[97];
    z[97]=z[4]*z[97]*z[20];
    z[98]=z[27]*z[3];
    z[99]=npow(z[98],4);
    z[95]=z[97] + z[95] - 6*z[99];
    z[95]=z[4]*z[95];
    z[90]=n<T>(1,2)*z[90] + z[95];
    z[90]=z[4]*z[90];
    z[95]=7*z[33];
    z[97]= - z[95] + z[96];
    z[97]=z[2]*z[97];
    z[97]=z[61] + z[97];
    z[97]=z[97]*z[20];
    z[100]=z[50] - z[27];
    z[101]=z[2]*z[100];
    z[101]= - z[15] + z[101];
    z[101]=z[4]*z[101]*z[88];
    z[98]= - npow(z[98],5);
    z[97]=z[101] + z[97] + z[98];
    z[97]=z[4]*z[97];
    z[98]=10*z[33];
    z[101]=z[98] - 13*z[27];
    z[101]=z[2]*z[101];
    z[101]= - z[61] + z[101];
    z[101]=z[2]*z[101];
    z[97]=z[97] + z[101] + 4*z[99];
    z[97]=z[4]*z[97];
    z[101]=7*z[27];
    z[102]= - z[89] + z[101];
    z[102]=z[102]*z[40];
    z[103]= - z[84]*z[42];
    z[97]=z[97] + z[102] + z[103];
    z[97]=z[5]*z[97];
    z[102]=31*z[33];
    z[103]= - z[102] + 111*z[27];
    z[103]=n<T>(1,2)*z[103] - 41*z[65];
    z[104]=z[15]*z[10];
    z[90]=2*z[97] - n<T>(35,4)*z[104] + n<T>(1,2)*z[103] + z[90];
    z[90]=z[5]*z[90];
    z[97]=73*z[33] + 21*z[27];
    z[103]=z[33] + z[52];
    z[103]=z[7]*z[103];
    z[105]=15*z[15];
    z[103]= - z[105] + z[103];
    z[103]=z[10]*z[103];
    z[106]=9*z[15];
    z[107]=z[106] + 5*z[44];
    z[108]=z[31]*z[10];
    z[109]= - 9*z[108] + z[107];
    z[110]=n<T>(5,2)*z[5];
    z[109]=z[109]*z[110];
    z[97]=z[109] + n<T>(3,2)*z[103] + n<T>(1,2)*z[97] + 21*z[52];
    z[103]= - z[46] + z[47];
    z[103]=z[7]*z[103];
    z[103]=z[103] - 9*z[31];
    z[109]=n<T>(1,2)*z[10];
    z[103]=z[103]*z[109];
    z[111]=z[16] + z[31];
    z[112]=npow(z[1],5);
    z[113]= - z[10]*z[112];
    z[113]=z[113] + z[111];
    z[113]=z[113]*z[110];
    z[103]=z[113] + z[103] + n<T>(1,2)*z[107] + 2*z[47];
    z[103]=z[8]*z[103];
    z[97]=n<T>(1,2)*z[97] + z[103];
    z[97]=z[8]*z[97];
    z[103]=z[30]*z[33];
    z[107]=z[61] + z[103];
    z[107]=z[7]*z[107];
    z[113]=z[31] - 4*z[32];
    z[113]=z[113]*z[29];
    z[107]=z[107] + z[113];
    z[107]=z[4]*z[107];
    z[113]=2*z[1];
    z[114]=z[77]*z[113];
    z[107]=z[114] + z[107];
    z[107]=z[4]*z[107];
    z[114]= - z[15]*z[77];
    z[115]=z[77]*z[4];
    z[116]=z[31]*z[115];
    z[114]=z[114] + z[116];
    z[114]=z[4]*z[114];
    z[116]=z[77]*z[33];
    z[114]=z[116] + z[114];
    z[114]=z[4]*z[114];
    z[117]=z[1]*z[77];
    z[114]=z[117] + z[114];
    z[114]=z[9]*z[114];
    z[117]=z[1] + z[71];
    z[117]=z[7]*z[117];
    z[107]=z[114] + z[117] + z[107];
    z[107]=z[9]*z[107];
    z[114]=3*z[3];
    z[117]=z[88]*z[114];
    z[117]= - n<T>(61,2)*z[20] + z[117];
    z[118]=n<T>(1,2)*z[3];
    z[117]=z[117]*z[118];
    z[119]=n<T>(3,2)*z[52];
    z[120]= - z[33] + z[119];
    z[120]=z[120]*z[109];
    z[121]=z[69]*z[33];
    z[122]=4*z[1] + n<T>(11,2)*z[121];
    z[122]=z[7]*z[122];
    z[123]=4*z[33];
    z[122]=z[123] + z[122];
    z[122]=z[12]*z[122];
    z[124]= - n<T>(25,2)*z[2] + 3*z[38];
    z[124]=z[3]*z[124];
    z[124]= - static_cast<T>(19)+ z[124];
    z[124]=z[7]*z[124];
    z[49]=z[49] + z[97] + z[107] + z[90] + z[122] + z[120] + z[82] + n<T>(1,2)*z[124] + z[117] + 34*z[1] - n<T>(15,2)*z[2];
    z[49]=z[6]*z[49];
    z[82]=z[42]*z[3];
    z[82]=z[82] + z[20];
    z[82]=z[82]*z[3];
    z[90]=4*z[2];
    z[82]=z[82] - z[90];
    z[82]=z[82]*z[3];
    z[97]=z[24] - 4;
    z[97]=z[97]*z[3];
    z[97]=z[97] - z[69];
    z[97]=z[97]*z[7];
    z[82]=z[82] - z[97];
    z[82]=z[82]*z[7];
    z[97]=z[42]*z[24];
    z[97]=z[97] + z[88];
    z[97]=z[97]*z[3];
    z[97]=z[97] - 4*z[20];
    z[97]=z[97]*z[3];
    z[107]=6*z[1];
    z[82]=z[82] - z[97] + z[107];
    z[97]= - z[7]*z[82];
    z[117]=z[58] - z[33];
    z[120]=5*z[15];
    z[122]=z[120]*z[17];
    z[124]= - z[20]*z[122];
    z[125]=2*z[88];
    z[124]=z[125] + z[124];
    z[124]=z[3]*z[124];
    z[97]=z[97] + 2*z[117] + z[124];
    z[97]=z[7]*z[97];
    z[124]=4*z[3];
    z[126]=z[124] + z[69];
    z[126]=z[7]*z[126];
    z[36]=7*z[36] + z[126];
    z[36]=z[7]*z[36];
    z[36]=z[107] + z[36];
    z[36]=z[7]*z[36];
    z[36]=z[50] + z[36];
    z[36]=z[36]*z[115];
    z[36]=z[36] + z[97] - z[41] + z[85];
    z[41]=3*z[10];
    z[36]=z[36]*z[41];
    z[97]=n<T>(1,2)*z[20];
    z[126]=z[97] + z[33];
    z[127]=21*z[3];
    z[128]=z[126]*z[127];
    z[128]= - n<T>(113,4)*z[2] + z[128];
    z[128]=z[3]*z[128];
    z[129]= - static_cast<T>(19)+ 7*z[24];
    z[129]=z[129]*z[3];
    z[130]=7*z[69];
    z[129]=z[129] - z[130];
    z[131]=n<T>(3,2)*z[7];
    z[132]= - z[129]*z[131];
    z[128]=z[128] + z[132];
    z[128]=z[7]*z[128];
    z[102]= - z[102] + z[21];
    z[102]=z[102]*z[24];
    z[102]=21*z[20] + z[102];
    z[102]=z[3]*z[102];
    z[102]= - 19*z[1] + z[102];
    z[102]=n<T>(1,2)*z[102] + z[128];
    z[102]=z[7]*z[102];
    z[128]= - 19*z[3] - z[130];
    z[128]=z[7]*z[128];
    z[128]= - n<T>(29,2)*z[25] + z[128];
    z[128]=z[7]*z[128];
    z[130]=n<T>(9,2)*z[1];
    z[128]=z[130] + z[128];
    z[128]=z[128]*z[30];
    z[128]=n<T>(49,2)*z[33] + z[128];
    z[128]=z[7]*z[128];
    z[132]=n<T>(7,2)*z[15];
    z[128]=z[132] + z[128];
    z[133]=n<T>(1,2)*z[4];
    z[128]=z[128]*z[133];
    z[134]=z[88]*z[3];
    z[36]=z[36] + z[128] + z[102] + n<T>(3,2)*z[134] - z[72] + n<T>(17,2)*z[27];
    z[36]=z[10]*z[36];
    z[72]=z[61] + z[44];
    z[72]=z[2]*z[72];
    z[102]= - z[7]*z[61];
    z[72]=z[102] + z[31] + z[72];
    z[102]=z[7]*z[31];
    z[102]=z[112] + z[102];
    z[102]=z[102]*z[23];
    z[128]= - z[112]*z[115];
    z[102]=z[102] + z[128];
    z[102]=z[4]*z[102];
    z[128]= - z[31] + z[32];
    z[128]=z[7]*z[128];
    z[102]=z[102] - z[112] + 4*z[128];
    z[102]=z[4]*z[102];
    z[72]=2*z[72] + z[102];
    z[72]=z[9]*z[72];
    z[102]=z[30]*z[15];
    z[112]= - z[43] - z[102];
    z[112]=z[7]*z[112];
    z[43]=z[115]*z[43];
    z[43]=z[112] + z[43];
    z[112]=2*z[4];
    z[43]=z[43]*z[112];
    z[128]=14*z[15] - 5*z[47];
    z[128]=z[7]*z[128];
    z[43]=z[128] + z[43];
    z[43]=z[4]*z[43];
    z[128]=z[123] + z[27];
    z[128]=z[128]*z[23];
    z[135]=z[61] + z[47];
    z[136]= - z[7]*z[135];
    z[136]= - z[31] + z[136];
    z[136]=z[136]*z[59];
    z[43]=z[72] + z[136] + z[43] + z[128] - z[120] - z[45];
    z[43]=z[9]*z[43];
    z[72]=2*z[52];
    z[128]= - z[72] + 8*z[33] + z[27];
    z[136]= - z[15]*z[115];
    z[116]=z[116] + z[136];
    z[116]=z[4]*z[116];
    z[98]= - z[98] + z[119];
    z[98]=z[98]*z[30];
    z[98]=6*z[116] + z[120] + z[98];
    z[98]=z[4]*z[98];
    z[116]=z[123] + z[74];
    z[116]=z[7]*z[116];
    z[136]=n<T>(1,2)*z[47];
    z[137]= - z[15] - z[136];
    z[137]=z[7]*z[137];
    z[31]= - n<T>(1,2)*z[31] + z[137];
    z[31]=z[12]*z[31];
    z[31]=z[31] + z[132] + z[116];
    z[31]=z[31]*z[59];
    z[31]=z[43] + z[31] + 2*z[128] + z[98];
    z[31]=z[9]*z[31];
    z[43]=4*z[69];
    z[98]=7*z[3] + z[43];
    z[98]=z[98]*z[30];
    z[98]=n<T>(35,2)*z[64] + z[98];
    z[98]=z[7]*z[98];
    z[98]=n<T>(51,2)*z[1] + z[98];
    z[98]=z[7]*z[98];
    z[116]=z[123] - z[52];
    z[116]=z[7]*z[116];
    z[128]=z[123] + z[52];
    z[132]=z[128]*z[115];
    z[116]=z[116] + z[132];
    z[116]=z[4]*z[116];
    z[98]=z[116] - z[66] + z[98];
    z[98]=z[4]*z[98];
    z[116]=n<T>(3,2)*z[17];
    z[132]= - z[15]*z[20]*z[116];
    z[132]= - z[88] + z[132];
    z[132]=z[3]*z[132];
    z[137]=n<T>(3,2)*z[20];
    z[138]= - z[3]*z[61];
    z[138]= - z[137] + z[138];
    z[138]=z[3]*z[138];
    z[138]= - z[53] + z[138];
    z[138]=z[3]*z[138];
    z[139]= - static_cast<T>(1)- n<T>(3,2)*z[24];
    z[139]=z[3]*z[139];
    z[139]=z[139] - z[70];
    z[139]=z[7]*z[139];
    z[138]=z[138] + z[139];
    z[138]=z[7]*z[138];
    z[139]= - z[24]*z[46];
    z[140]=n<T>(1,2)*z[88];
    z[139]= - z[140] + z[139];
    z[139]=z[3]*z[139];
    z[139]= - z[21] + z[139];
    z[139]=z[3]*z[139];
    z[138]=z[138] - n<T>(3,2)*z[1] + z[139];
    z[138]=z[7]*z[138];
    z[139]= - z[27] + n<T>(1,2)*z[33];
    z[139]=3*z[139];
    z[132]=z[138] + z[139] + z[132];
    z[132]=z[7]*z[132];
    z[34]= - z[34] - z[85];
    z[34]=n<T>(1,2)*z[34] + z[132];
    z[34]=z[12]*z[34];
    z[132]=7*z[20];
    z[138]=11*z[33] + z[132];
    z[138]=z[3]*z[138];
    z[141]=13*z[2];
    z[138]=z[141] + z[138];
    z[138]=z[3]*z[138];
    z[142]=5*z[69];
    z[143]=static_cast<T>(7)+ 11*z[24];
    z[143]=z[3]*z[143];
    z[143]=z[143] + z[142];
    z[143]=z[7]*z[143];
    z[138]=z[138] + z[143];
    z[138]=z[7]*z[138];
    z[143]=9*z[33] + z[20];
    z[143]=z[143]*z[24];
    z[143]=z[132] + z[143];
    z[143]=z[3]*z[143];
    z[144]=9*z[1];
    z[138]=z[138] + z[144] + z[143];
    z[138]=z[7]*z[138];
    z[138]=z[138] + z[101] + z[134];
    z[34]=n<T>(1,2)*z[138] + z[34];
    z[34]=z[34]*z[59];
    z[138]=z[44] + z[15];
    z[143]=z[33] + z[27];
    z[145]= - z[1] - z[83];
    z[145]=z[7]*z[145];
    z[143]=3*z[143] + z[145];
    z[143]=z[7]*z[143];
    z[145]=5*z[5];
    z[111]=z[111]*z[145];
    z[111]=z[111] + 9*z[138] + z[143];
    z[111]=z[10]*z[111];
    z[121]= - z[1] - z[121];
    z[121]=z[7]*z[121];
    z[111]=z[121] + z[111];
    z[111]=z[8]*z[111];
    z[121]=z[5]*z[16];
    z[121]=n<T>(1,4)*z[121] + n<T>(3,2)*z[138];
    z[121]=z[10]*z[121];
    z[121]=z[33] + z[121];
    z[121]=z[121]*z[145];
    z[138]=13*z[1];
    z[143]= - n<T>(95,4)*z[3] - 18*z[69];
    z[143]=z[7]*z[143];
    z[143]= - n<T>(107,4) + z[143];
    z[143]=z[7]*z[143];
    z[31]=n<T>(1,2)*z[111] + z[31] + z[121] + z[34] + z[36] + z[98] - z[138]
    + z[143];
    z[31]=z[8]*z[31];
    z[34]= - z[21] + 14*z[18];
    z[34]=z[3]*z[34];
    z[36]=6*z[2] - z[122];
    z[36]=z[3]*z[36];
    z[36]=z[36] - z[35];
    z[36]=z[7]*z[36];
    z[98]=10*z[1];
    z[34]=z[36] - z[98] + z[34];
    z[23]=z[34]*z[23];
    z[34]= - z[87] + 19*z[33];
    z[23]=z[23] - 4*z[26] - z[34];
    z[23]=z[9]*z[23];
    z[36]= - z[20] + 3*z[51];
    z[36]=z[36]*z[124];
    z[87]=n<T>(43,2)*z[54];
    z[111]=20*z[2] - z[87];
    z[111]=z[3]*z[111];
    z[111]=z[111] - 7*z[35];
    z[111]=z[7]*z[111];
    z[121]= - z[5]*z[26];
    z[23]=z[23] + z[121] + z[111] - z[144] + z[36];
    z[23]=z[9]*z[23];
    z[36]= - z[113] - z[38];
    z[36]=z[5]*z[36];
    z[23]=z[23] + z[36] - z[48] - n<T>(79,4) + 10*z[24];
    z[23]=z[9]*z[23];
    z[36]= - z[7]*z[80];
    z[36]= - n<T>(7,2)*z[64] + z[36];
    z[36]=z[36]*z[30];
    z[36]= - n<T>(7,2)*z[1] + z[36];
    z[48]= - z[1]*z[115];
    z[48]= - z[72] + z[48];
    z[48]=z[4]*z[48];
    z[36]=n<T>(3,2)*z[36] + z[48];
    z[36]=z[8]*z[36];
    z[48]= - static_cast<T>(1)+ z[24];
    z[48]=z[3]*z[48];
    z[48]=z[48] - z[69];
    z[48]=z[7]*z[48];
    z[64]=z[2] - z[38];
    z[64]=z[3]*z[64];
    z[48]=z[48] - n<T>(7,2) + z[64];
    z[64]=z[27] - z[52];
    z[64]=z[4]*z[64];
    z[64]= - z[79] + z[64];
    z[64]=z[4]*z[64];
    z[36]=z[36] + n<T>(9,2)*z[48] + z[64];
    z[36]=z[4]*z[36];
    z[48]=19*z[2];
    z[64]=z[48] - n<T>(45,2)*z[38];
    z[64]=z[3]*z[64];
    z[80]=z[126]*z[24];
    z[80]= - z[97] + z[80];
    z[80]=z[3]*z[80];
    z[111]=n<T>(1,2)*z[1];
    z[80]= - z[111] + z[80];
    z[80]=z[4]*z[80];
    z[64]=15*z[80] - static_cast<T>(19)+ z[64];
    z[80]= - z[33] - z[20];
    z[80]=z[80]*z[24];
    z[80]=n<T>(3,4)*z[20] + z[80];
    z[80]=z[3]*z[80];
    z[115]=z[15]*z[38];
    z[121]=npow(z[2],4);
    z[115]=n<T>(1,2)*z[121] + z[115];
    z[115]=z[3]*z[115];
    z[115]= - z[140] + z[115];
    z[115]=z[3]*z[115];
    z[115]= - z[60] + z[115];
    z[115]=z[115]*z[133];
    z[80]=z[115] - n<T>(1,4)*z[1] + z[80];
    z[80]=z[80]*z[145];
    z[64]=n<T>(1,2)*z[64] + z[80];
    z[64]=z[5]*z[64];
    z[80]= - static_cast<T>(19)+ 45*z[24];
    z[80]=z[80]*z[118];
    z[115]=z[33] + z[21];
    z[115]=z[3]*z[115];
    z[115]= - n<T>(3,2)*z[2] + z[115];
    z[124]=z[5]*z[3];
    z[115]=z[115]*z[124];
    z[80]=z[80] + 5*z[115];
    z[80]=z[5]*z[80];
    z[80]= - 9*z[17] + z[80];
    z[115]=n<T>(1,4) - z[24];
    z[115]=z[115]*z[124];
    z[115]= - n<T>(3,4)*z[17] + z[115];
    z[115]=z[5]*z[115];
    z[126]=npow(z[5],2);
    z[140]=z[126]*z[11];
    z[143]=z[17]*z[140];
    z[115]=z[115] + n<T>(1,4)*z[143];
    z[115]=z[11]*z[115];
    z[80]=n<T>(1,2)*z[80] + 5*z[115];
    z[80]=z[11]*z[80];
    z[19]=z[20] - z[19];
    z[19]=z[3]*z[19];
    z[115]= - z[53] + z[122];
    z[115]=z[3]*z[115];
    z[115]=2*z[115] + z[56];
    z[115]=z[7]*z[115];
    z[19]=z[115] + z[98] + z[19];
    z[98]=2*z[9];
    z[19]=z[19]*z[98];
    z[87]= - z[141] + z[87];
    z[87]=z[3]*z[87];
    z[18]=z[20] - 4*z[18];
    z[18]=z[3]*z[18];
    z[18]=z[113] + z[18];
    z[18]=z[5]*z[18];
    z[18]=z[19] + z[18] + z[87] + 15*z[35];
    z[18]=z[9]*z[18];
    z[19]=5*z[54];
    z[87]= - z[90] + z[19];
    z[87]=z[87]*z[124];
    z[18]=z[18] + 12*z[3] + z[87];
    z[18]=z[9]*z[18];
    z[87]= - z[17]*z[46];
    z[87]=z[40] + z[87];
    z[87]=z[87]*z[124];
    z[90]=z[40] - z[122];
    z[90]=z[3]*z[90];
    z[90]=z[90] - z[56];
    z[90]=z[90]*z[98];
    z[87]=z[90] - 9*z[3] + z[87];
    z[87]=z[9]*z[87];
    z[87]= - 3*z[124] + z[87];
    z[87]=z[9]*z[87];
    z[90]=z[3]*z[98];
    z[90]=z[124] + z[90];
    z[90]=z[14]*z[90]*npow(z[9],2);
    z[87]=z[87] + z[90];
    z[87]=z[14]*z[87];
    z[18]=z[87] + 2*z[124] + z[18];
    z[18]=z[14]*z[18];
    z[87]= - n<T>(17,2) + 9*z[24];
    z[87]=z[3]*z[87];
    z[18]=z[18] + z[80] + z[23] + z[64] + z[87] - n<T>(9,2)*z[69] + z[36];
    z[18]=z[14]*z[18];
    z[23]= - z[15]*z[109];
    z[23]=z[33] + z[23];
    z[23]=z[23]*z[145];
    z[36]=z[55] - z[1];
    z[23]=n<T>(1,2)*z[36] + z[23];
    z[23]=z[11]*z[23];
    z[64]=z[10]*z[120];
    z[23]=z[23] - z[50] + z[64];
    z[23]=z[5]*z[23];
    z[23]= - z[55] + z[23];
    z[23]=z[11]*z[23];
    z[64]= - z[120] + z[47];
    z[64]=z[64]*z[109];
    z[80]= - z[8]*z[103];
    z[87]=z[8]*z[102];
    z[87]=z[136] + z[87];
    z[87]=z[6]*z[87];
    z[79]=z[79]*z[5];
    z[90]=z[140]*z[1];
    z[98]= - z[79] - n<T>(5,2)*z[90];
    z[98]=z[11]*z[98];
    z[102]= - z[8]*z[52];
    z[102]= - z[1] + z[102];
    z[98]=3*z[102] + z[98];
    z[98]=z[14]*z[98];
    z[23]=z[98] + z[87] + z[23] + z[80] - z[89] + z[64];
    z[23]=z[13]*z[23];
    z[64]= - z[33]*z[145];
    z[64]=z[64] - z[130] - 5*z[55];
    z[64]=z[5]*z[64];
    z[36]= - z[12]*z[36];
    z[36]=z[73] + z[36];
    z[36]=z[36]*z[59];
    z[73]= - z[73]*z[110];
    z[36]=z[36] + z[73];
    z[36]=z[11]*z[36];
    z[73]=z[50] + z[52];
    z[80]= - z[10]*z[61];
    z[80]=z[80] + z[73];
    z[80]=z[12]*z[80];
    z[55]=z[80] + z[1] + 4*z[55];
    z[55]=z[55]*z[59];
    z[36]=z[36] + z[55] + z[64];
    z[36]=z[11]*z[36];
    z[55]=z[73]*z[7];
    z[55]=z[55] + z[15];
    z[64]= - z[108] + z[55];
    z[64]=z[12]*z[64];
    z[73]=z[10]*z[46];
    z[64]=z[64] + z[73] + z[128];
    z[64]=z[64]*z[59];
    z[73]=z[7]*z[128];
    z[55]=z[55]*z[75];
    z[46]=z[55] + z[46] + z[73];
    z[46]=z[12]*z[46];
    z[46]=z[46] - z[33] - z[72];
    z[55]=3*z[8];
    z[46]=z[46]*z[55];
    z[52]= - z[95] - z[52];
    z[55]= - z[135]*z[55];
    z[52]=n<T>(1,2)*z[52] + z[55];
    z[52]=z[6]*z[52];
    z[55]= - z[33] - z[74];
    z[55]=z[10]*z[55];
    z[72]=z[79] + 5*z[90];
    z[72]=z[14]*z[72];
    z[23]=z[23] + z[72] + z[52] + z[36] + z[46] + z[64] - z[107] + z[55]
   ;
    z[23]=z[13]*z[23];
    z[36]=z[97] + z[50];
    z[46]= - z[3]*z[36];
    z[46]= - z[40] + z[46];
    z[46]=z[3]*z[46];
    z[52]= - static_cast<T>(1)- z[24];
    z[52]=z[3]*z[52];
    z[52]=z[52] - z[70];
    z[52]=z[7]*z[52];
    z[46]=z[46] + z[52];
    z[46]=z[46]*z[30];
    z[52]= - z[20] - z[51];
    z[52]=z[52]*z[114];
    z[55]=n<T>(11,2)*z[1];
    z[46]=z[46] - z[55] + z[52];
    z[46]=z[7]*z[46];
    z[52]= - n<T>(1,2)*z[15] + z[44];
    z[52]=3*z[52] + z[62];
    z[52]=z[52]*z[41];
    z[62]=z[44] + z[47];
    z[16]=z[16] + n<T>(1,2)*z[32];
    z[16]=z[4]*z[16];
    z[16]=n<T>(3,2)*z[62] + z[16];
    z[16]=z[12]*z[16];
    z[32]=z[105] - 11*z[44];
    z[32]=n<T>(1,2)*z[32] + z[47];
    z[32]=z[4]*z[32];
    z[16]=z[16] + z[52] + z[32] + z[46] - n<T>(3,2)*z[65] + 22*z[33] - n<T>(35,2)
   *z[27];
    z[16]=z[12]*z[16];
    z[32]= - z[89] + z[28];
    z[32]=z[2]*z[32];
    z[32]=z[15] + z[32];
    z[32]=z[32]*z[112];
    z[32]=z[32] - z[65] + z[66] - z[96];
    z[32]=z[32]*z[41];
    z[46]=n<T>(1,2)*z[38];
    z[47]=z[53] + z[46];
    z[47]=z[47]*z[114];
    z[52]=n<T>(5,2)*z[69];
    z[62]=n<T>(7,2) + z[81];
    z[62]=z[3]*z[62];
    z[62]=z[62] + z[52];
    z[62]=z[62]*z[30];
    z[47]=z[62] + n<T>(41,2) + z[47];
    z[47]=z[7]*z[47];
    z[34]= - z[119] - z[34];
    z[34]=z[4]*z[34];
    z[16]=z[16] + z[32] + z[34] + z[47] + n<T>(3,2)*z[38] - 32*z[1] + 23*
    z[2];
    z[16]=z[12]*z[16];
    z[32]=z[38]*z[42];
    z[32]=z[121] + z[32];
    z[32]=z[32]*z[118];
    z[32]= - z[125] + z[32];
    z[32]=z[3]*z[32];
    z[32]=z[32] - z[117];
    z[34]=z[82]*z[30];
    z[32]=5*z[32] + z[34];
    z[29]=z[32]*z[29];
    z[32]=5*z[88];
    z[34]=z[32] - 9*z[86];
    z[34]=z[34]*z[118];
    z[36]=z[36]*z[24];
    z[36]= - z[137] + z[36];
    z[36]=z[3]*z[36];
    z[36]= - z[1] + z[36];
    z[36]=z[7]*z[36];
    z[29]=z[29] + z[36] + z[34] + z[68];
    z[29]=z[10]*z[29];
    z[34]= - n<T>(17,2)*z[20] + 9*z[134];
    z[34]=z[3]*z[34];
    z[36]= - z[2] + z[46];
    z[36]=z[36]*z[56];
    z[42]=7*z[2];
    z[34]=z[36] + z[34] - z[55] + z[42];
    z[36]= - n<T>(29,2)*z[33] - z[132];
    z[36]=z[3]*z[36];
    z[36]=z[48] + z[36];
    z[36]=z[3]*z[36];
    z[46]=z[7]*z[129];
    z[36]=z[36] + z[46];
    z[36]=z[36]*z[131];
    z[46]=20*z[33] + n<T>(39,4)*z[20];
    z[46]=z[46]*z[24];
    z[46]= - n<T>(105,4)*z[20] + z[46];
    z[46]=z[3]*z[46];
    z[36]=z[36] + z[57] + z[46];
    z[36]=z[7]*z[36];
    z[46]=n<T>(15,2)*z[33] - z[101];
    z[47]= - z[123] - n<T>(15,4)*z[20];
    z[47]=z[47]*z[38];
    z[47]=n<T>(35,4)*z[88] + z[47];
    z[47]=z[3]*z[47];
    z[36]=z[36] + n<T>(3,2)*z[46] + z[47];
    z[36]=z[4]*z[36];
    z[29]=z[29] + n<T>(1,2)*z[34] + z[36];
    z[29]=z[10]*z[29];
    z[34]=z[106] - 7*z[44];
    z[34]=z[34]*z[133];
    z[36]=n<T>(13,2)*z[33];
    z[34]=z[34] + z[36] - z[58];
    z[34]=z[2]*z[34];
    z[15]=z[12]*z[4]*z[15]*z[97];
    z[15]=z[15] + z[34];
    z[15]=z[12]*z[15];
    z[34]= - 43*z[33] + z[93];
    z[34]=z[2]*z[34];
    z[34]=z[92] + z[34];
    z[34]=z[34]*z[133];
    z[15]=z[15] + z[34] + z[63] + n<T>(43,2)*z[33] - 30*z[27];
    z[15]=z[12]*z[15];
    z[34]=z[50] - z[67];
    z[34]=z[2]*z[34];
    z[44]=z[84]*z[61];
    z[34]=z[34] + z[44];
    z[44]= - z[89] + z[67];
    z[44]=z[44]*z[20];
    z[37]=z[37]*z[4];
    z[46]= - z[88]*z[37];
    z[44]=z[46] + z[44] - z[99];
    z[44]=z[4]*z[44];
    z[34]=2*z[34] + z[44];
    z[34]=z[34]*z[112];
    z[32]=z[32] - 33*z[86];
    z[32]=z[32]*z[118];
    z[32]=n<T>(5,4)*z[104] + z[34] + z[32] - z[36] + 25*z[27];
    z[32]=z[5]*z[32];
    z[28]= - z[33] + z[28];
    z[28]=z[28]*z[40];
    z[34]= - z[20]*z[37];
    z[28]=z[34] + z[28] - z[85];
    z[28]=z[28]*z[91];
    z[34]=z[94] - n<T>(95,2)*z[27];
    z[36]=n<T>(5,2)*z[88] + 12*z[86];
    z[36]=z[3]*z[36];
    z[28]=z[28] + n<T>(1,2)*z[34] + z[36];
    z[28]=z[4]*z[28];
    z[34]=z[88] - z[86];
    z[34]=z[3]*z[34];
    z[34]= - z[139] + z[34];
    z[36]=n<T>(5,2)*z[10];
    z[34]=z[34]*z[36];
    z[15]=z[32] + z[15] + z[34] + z[28] + n<T>(5,2)*z[38] + 28*z[1] - n<T>(31,2)
   *z[2];
    z[15]=z[5]*z[15];
    z[19]= - z[53] - z[19];
    z[19]=z[3]*z[19];
    z[28]=n<T>(1,2)*z[24];
    z[32]= - static_cast<T>(1)- z[28];
    z[32]=z[3]*z[32];
    z[32]=z[32] - z[70];
    z[32]=z[32]*z[30];
    z[19]=z[19] + z[32];
    z[19]=z[7]*z[19];
    z[32]=n<T>(1,2)*z[26];
    z[34]= - z[32] - z[100];
    z[34]=z[34]*z[41];
    z[37]= - z[27]*z[59];
    z[19]=z[37] + z[34] + z[19] + 22*z[1] - n<T>(9,2)*z[71];
    z[19]=z[12]*z[19];
    z[28]=static_cast<T>(6)+ z[28];
    z[28]=z[3]*z[28];
    z[28]=z[28] + z[52];
    z[28]=z[28]*z[30];
    z[34]=z[20] - n<T>(3,2)*z[51];
    z[34]=z[3]*z[34];
    z[34]=n<T>(5,2)*z[1] + z[34];
    z[34]=z[34]*z[41];
    z[37]= - static_cast<T>(41)+ z[81];
    z[19]=z[19] + z[34] + n<T>(1,2)*z[37] + z[28];
    z[19]=z[12]*z[19];
    z[28]= - z[2] + 3*z[54];
    z[28]=z[28]*z[114];
    z[34]= - z[17]*z[77];
    z[28]=z[28] + z[34];
    z[34]= - z[7]*z[25];
    z[34]= - z[83] + z[34];
    z[37]=n<T>(3,2)*z[12];
    z[34]=z[34]*z[37];
    z[40]= - z[1] + z[83];
    z[40]=z[10]*z[40];
    z[28]=z[34] + n<T>(1,2)*z[28] + z[40];
    z[28]=z[12]*z[28];
    z[34]=static_cast<T>(5)- z[24];
    z[34]=z[34]*z[118];
    z[40]= - z[2] + n<T>(3,2)*z[54];
    z[40]=z[40]*z[10]*z[3];
    z[28]=z[28] + z[40] + z[34] + z[142];
    z[28]=z[12]*z[28];
    z[28]= - z[116] + z[28];
    z[34]=15*z[2];
    z[41]=z[34] - 13*z[54];
    z[41]=z[3]*z[41];
    z[44]=z[111] - z[83];
    z[46]=5*z[10];
    z[44]=z[44]*z[46];
    z[41]=z[41] + z[44];
    z[41]=z[5]*z[41];
    z[40]=z[114] - z[40];
    z[44]= - z[12]*z[83];
    z[44]=z[78] + z[44];
    z[44]=z[44]*z[59];
    z[40]=z[41] + 5*z[40] + z[44];
    z[40]=z[5]*z[40];
    z[25]=z[109]*z[25];
    z[25]=z[25] - z[3];
    z[35]= - z[35]*z[37];
    z[35]=z[35] - n<T>(3,2)*z[69] - z[25];
    z[35]=z[12]*z[35];
    z[35]=z[116] + z[35];
    z[35]=z[35]*z[59];
    z[25]=z[25]*z[126];
    z[25]=z[35] + n<T>(5,2)*z[25];
    z[25]=z[11]*z[25];
    z[25]=z[25] + 3*z[28] + n<T>(1,2)*z[40];
    z[25]=z[11]*z[25];
    z[28]= - z[12]*z[39];
    z[26]=z[28] + z[58] + z[26];
    z[26]=z[12]*z[26];
    z[26]=z[26] + 11*z[1] - 5*z[71];
    z[26]=z[26]*z[37];
    z[28]=z[32] + z[33] - z[60];
    z[28]=z[28]*z[36];
    z[32]= - n<T>(15,2)*z[20] + 17*z[51];
    z[32]=z[3]*z[32];
    z[28]=z[28] - z[138] + z[32];
    z[28]=z[5]*z[28];
    z[32]= - z[20] + n<T>(5,4)*z[51];
    z[32]=z[3]*z[32];
    z[32]= - n<T>(3,4)*z[1] + z[32];
    z[32]=z[32]*z[46];
    z[33]=static_cast<T>(21)- 25*z[24];
    z[26]=z[28] + z[26] + n<T>(1,2)*z[33] + z[32];
    z[26]=z[5]*z[26];
    z[17]=z[17]*z[21];
    z[17]= - static_cast<T>(7)+ z[17];
    z[21]=z[97] - z[51];
    z[21]=z[3]*z[21];
    z[21]=z[1] + z[21];
    z[21]=z[10]*z[21];
    z[17]=n<T>(1,4)*z[17] + z[21];
    z[17]=z[10]*z[17];
    z[21]=n<T>(7,2) - z[81];
    z[21]=z[21]*z[118];
    z[28]=6*z[69];
    z[17]=z[25] + z[26] + z[19] + z[17] + z[21] - z[28];
    z[17]=z[11]*z[17];
    z[19]= - z[42] + 4*z[38];
    z[19]=z[19]*z[114];
    z[21]=static_cast<T>(7)- 4*z[24];
    z[21]=z[3]*z[21];
    z[21]=z[21] + z[43];
    z[21]=z[21]*z[30];
    z[19]=z[21] + n<T>(35,2) + z[19];
    z[19]=z[7]*z[19];
    z[21]= - z[65] + z[89] + z[27];
    z[24]= - z[27] + z[128];
    z[24]=z[7]*z[24];
    z[24]= - z[45] + z[24];
    z[24]=z[4]*z[24];
    z[21]=2*z[21] + z[24];
    z[21]=z[4]*z[21];
    z[24]= - z[88]*z[118];
    z[20]=z[20] + z[24];
    z[20]=z[20]*z[127];
    z[19]=z[21] + z[19] + z[20] + n<T>(59,4)*z[1] - z[34];
    z[19]=z[4]*z[19];
    z[20]= - n<T>(11,4) - z[76];
    z[20]=z[3]*z[20];
    z[20]=z[20] - z[28];
    z[20]=z[7]*z[20];
    z[21]= - n<T>(95,2)*z[2] + 33*z[38];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(21,2) + z[21];

    r += z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + n<T>(1,2)*z[21] + 
      z[22] + z[23] + z[29] + z[31] + z[49];
 
    return r;
}

template double qg_2lNLC_r113(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r113(const std::array<dd_real,31>&);
#endif
