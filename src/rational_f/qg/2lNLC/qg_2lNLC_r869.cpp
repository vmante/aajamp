#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r869(const std::array<T,31>& k) {
  T z[100];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[2];
    z[7]=k[6];
    z[8]=k[4];
    z[9]=k[3];
    z[10]=k[9];
    z[11]=k[7];
    z[12]=k[5];
    z[13]=k[14];
    z[14]=k[13];
    z[15]=4*z[4];
    z[16]=3*z[10];
    z[17]=z[15] + z[16];
    z[18]=npow(z[10],2);
    z[19]=2*z[18];
    z[20]=z[17]*z[19];
    z[21]=npow(z[10],3);
    z[22]=z[21]*z[12];
    z[23]=z[22]*z[15];
    z[24]=npow(z[4],2);
    z[25]=z[24]*z[2];
    z[20]=z[23] + z[25] + z[20];
    z[20]=z[12]*z[20];
    z[23]=2*z[24];
    z[26]=z[2]*z[4];
    z[27]=z[11] + 6*z[4];
    z[28]=8*z[10] + z[27];
    z[28]=z[10]*z[28];
    z[20]=z[20] + z[28] - z[23] + n<T>(11,2)*z[26];
    z[20]=z[12]*z[20];
    z[28]=3*z[3];
    z[29]=2*z[11];
    z[30]=z[29] + z[28];
    z[31]=3*z[8];
    z[30]=z[30]*z[31];
    z[32]=z[12]*z[11];
    z[33]=4*z[32];
    z[34]=z[6]*z[3];
    z[30]=z[30] + z[33] - n<T>(27,2)*z[34];
    z[30]=z[8]*z[30];
    z[35]=2*z[13];
    z[36]=npow(z[12],2);
    z[37]=z[36]*z[35];
    z[38]=z[16] + z[2];
    z[39]=8*z[13];
    z[40]=z[39] + z[38];
    z[40]=z[12]*z[40];
    z[41]=2*z[2];
    z[42]=n<T>(11,2)*z[3] - z[10] + 9*z[13] + z[41];
    z[42]=z[6]*z[42];
    z[40]=z[40] + z[42];
    z[40]=z[6]*z[40];
    z[30]=z[30] + z[37] + z[40];
    z[37]=npow(z[7],2);
    z[30]=z[30]*z[37];
    z[40]=2*z[4];
    z[42]=z[22]*z[40];
    z[17]= - z[17]*z[18];
    z[17]=z[17] - z[42];
    z[17]=z[12]*z[17];
    z[43]=2*z[10];
    z[44]=5*z[10];
    z[45]= - z[11] - z[44];
    z[45]=z[45]*z[43];
    z[17]=z[45] + z[17];
    z[17]=z[12]*z[17];
    z[45]=z[40] - z[11];
    z[17]=z[17] + 2*z[45] - 9*z[10];
    z[17]=z[12]*z[17];
    z[45]=3*z[11];
    z[44]= - z[45] + z[44];
    z[46]=z[12]*z[10];
    z[47]= - z[11] + z[16];
    z[47]=z[47]*z[46];
    z[44]=2*z[44] + z[47];
    z[44]=z[12]*z[44];
    z[44]=static_cast<T>(13)+ z[44];
    z[44]=z[12]*z[44];
    z[47]=z[8]*z[11];
    z[48]=4*z[47];
    z[49]=3*z[32];
    z[50]=static_cast<T>(2)- z[49];
    z[50]=3*z[50] - z[48];
    z[50]=z[8]*z[50];
    z[44]=z[44] + z[50];
    z[44]=z[5]*z[44];
    z[50]=npow(z[6],2);
    z[51]= - 13*z[8] - 7*z[12] + 8*z[6];
    z[51]=z[8]*z[51];
    z[51]= - z[50] + z[51];
    z[51]=z[51]*z[37];
    z[52]=z[8]*z[40];
    z[17]=z[44] + 2*z[51] + z[52] - static_cast<T>(1)+ z[17];
    z[17]=z[5]*z[17];
    z[44]=npow(z[3],3);
    z[51]=z[44]*z[6];
    z[52]=npow(z[2],2);
    z[53]=z[51]*z[52];
    z[54]=n<T>(5,2)*z[2];
    z[55]=z[44]*z[54];
    z[55]=z[55] + z[53];
    z[55]=z[6]*z[55];
    z[56]=z[3]*z[52];
    z[55]=z[56] + z[55];
    z[55]=z[6]*z[55];
    z[56]=n<T>(13,2)*z[3];
    z[57]=z[2]*z[56];
    z[55]=z[57] + z[55];
    z[55]=z[6]*z[55];
    z[57]=n<T>(5,2)*z[3];
    z[58]= - z[4]*z[57];
    z[58]= - z[24] + z[58];
    z[58]=z[8]*z[58];
    z[59]=8*z[11];
    z[60]=5*z[2];
    z[61]=n<T>(1,2)*z[3];
    z[17]=z[17] + z[30] + z[58] + z[55] + z[20] + z[61] - z[16] + z[60]
    + z[59] + z[35] - n<T>(13,2)*z[4];
    z[17]=z[5]*z[17];
    z[20]=3*z[4] + z[29];
    z[20]=z[20]*z[43];
    z[30]=z[15] + z[11];
    z[30]=z[30]*z[18];
    z[55]=z[4]*z[22];
    z[30]=z[30] + z[55];
    z[30]=z[12]*z[30];
    z[20]=z[20] + z[30];
    z[20]=z[12]*z[20];
    z[30]=5*z[11];
    z[20]=z[20] + z[15] + z[30];
    z[20]=z[12]*z[20];
    z[55]=4*z[6];
    z[58]=19*z[12] - z[55];
    z[58]=z[6]*z[58];
    z[62]=z[8] - z[6];
    z[62]= - 29*z[12] - 12*z[62];
    z[62]=z[8]*z[62];
    z[58]=z[62] - 20*z[36] + z[58];
    z[58]=z[8]*z[58];
    z[62]=z[6]*z[12];
    z[63]=2*z[36] - z[62];
    z[63]=z[6]*z[63];
    z[64]=npow(z[12],3);
    z[63]= - z[64] + z[63];
    z[58]=3*z[63] + z[58];
    z[63]=npow(z[7],3);
    z[58]=z[58]*z[63];
    z[65]=z[4] + z[29];
    z[65]=z[8]*z[65];
    z[20]=z[58] + z[20] + z[65];
    z[20]=z[5]*z[20];
    z[58]=z[3] - z[11];
    z[65]= - z[8]*z[58];
    z[65]=z[65] + z[32] + z[34];
    z[31]=z[65]*z[31];
    z[65]=z[36]*z[11];
    z[66]= - z[3]*z[50];
    z[31]=z[31] + z[65] + z[66];
    z[66]=2*z[8];
    z[31]=z[31]*z[66];
    z[67]=z[36]*z[10];
    z[68]=z[6]*z[46];
    z[68]= - z[67] + z[68];
    z[68]=z[6]*z[68];
    z[31]=z[68] + z[31];
    z[31]=z[31]*z[63];
    z[31]=z[31] - z[4] - z[11];
    z[27]= - z[27]*z[18];
    z[27]=z[27] - z[42];
    z[27]=z[12]*z[27];
    z[68]= - z[40] - z[11];
    z[68]=z[68]*z[16];
    z[27]=z[68] + z[27];
    z[27]=z[12]*z[27];
    z[20]=z[20] + z[27] + 2*z[31];
    z[20]=z[5]*z[20];
    z[27]=3*z[2];
    z[31]=z[27]*z[24];
    z[68]= - z[4] + z[11];
    z[68]=z[68]*z[19];
    z[42]= - z[42] + z[31] + z[68];
    z[42]=z[12]*z[42];
    z[68]=z[36]*z[18];
    z[69]=npow(z[3],2);
    z[70]=z[50]*z[69];
    z[71]=z[68] - n<T>(5,2)*z[70];
    z[71]=z[6]*z[71];
    z[72]=z[8]*z[3];
    z[73]= - z[11] + 4*z[3];
    z[73]=z[73]*z[72];
    z[74]=z[69]*z[6];
    z[73]= - n<T>(21,2)*z[74] + z[73];
    z[73]=z[8]*z[73];
    z[73]=9*z[70] + z[73];
    z[73]=z[8]*z[73];
    z[71]=z[71] + z[73];
    z[71]=z[71]*z[63];
    z[73]= - z[24] + n<T>(7,2)*z[26];
    z[75]=7*z[2];
    z[76]= - 9*z[4] + z[75];
    z[76]=z[76]*z[61];
    z[77]= - z[24]*z[72];
    z[20]=z[20] + z[71] + z[77] + z[42] + 3*z[73] + z[76];
    z[20]=z[5]*z[20];
    z[42]=z[44]*z[50];
    z[71]=z[44]*z[8];
    z[73]= - n<T>(7,2)*z[51] + z[71];
    z[73]=z[8]*z[73];
    z[42]=n<T>(9,2)*z[42] + z[73];
    z[42]=z[8]*z[42];
    z[73]=z[44]*npow(z[6],3);
    z[42]= - n<T>(5,2)*z[73] + z[42];
    z[42]=z[8]*z[42];
    z[73]=z[64]*z[30];
    z[48]=13*z[32] + z[48];
    z[48]=z[8]*z[48];
    z[48]=14*z[65] + z[48];
    z[48]=z[8]*z[48];
    z[48]=z[73] + z[48];
    z[65]=z[5]*z[8];
    z[48]=z[48]*z[65];
    z[73]=4*z[11];
    z[76]=z[3]*npow(z[8],4)*z[73];
    z[48]=z[76] + z[48];
    z[48]=z[5]*z[48];
    z[44]=n<T>(1,2)*z[44];
    z[44]=npow(z[6],4)*z[44];
    z[42]=z[48] + z[44] + z[42];
    z[44]=npow(z[7],4);
    z[42]=z[44]*z[42];
    z[48]= - z[24] + n<T>(9,2)*z[26];
    z[48]=z[3]*z[48];
    z[42]=z[31] + z[48] + z[42];
    z[42]=z[5]*z[42];
    z[48]=z[41]*z[21];
    z[76]= - z[3]*z[48];
    z[77]=z[21]*z[2];
    z[78]=z[74]*z[77];
    z[76]=z[76] + z[78];
    z[78]=2*z[6];
    z[76]=z[76]*z[78];
    z[79]=npow(z[9],2);
    z[80]=z[79]*z[18]*z[70];
    z[81]=z[25]*z[11];
    z[82]= - npow(z[12],4)*z[81];
    z[80]=z[82] + z[80];
    z[44]=z[80]*z[44];
    z[80]=z[1]*z[5];
    z[80]=z[80] - 1;
    z[25]=z[25]*z[3]*z[80];
    z[25]=z[42] + z[44] + z[76] + z[48] + z[25];
    z[25]=z[1]*z[25];
    z[42]= - z[9]*z[18];
    z[44]=z[3]*z[9];
    z[76]=z[10]*z[44];
    z[42]=z[42] + z[76];
    z[42]=z[3]*z[42];
    z[76]=z[2] + z[13];
    z[80]=z[18]*z[12];
    z[82]= - z[76]*z[80];
    z[83]=z[27] + z[3];
    z[84]=z[83]*z[74];
    z[42]= - n<T>(1,2)*z[84] + z[42] + z[82];
    z[42]=z[6]*z[42];
    z[82]=z[79]*z[43];
    z[84]= - z[69]*z[82];
    z[68]=z[2]*z[68];
    z[42]=z[42] + z[84] + z[68];
    z[42]=z[6]*z[42];
    z[68]=z[52]*z[32];
    z[84]= - z[2] - z[3];
    z[84]=z[84]*z[74];
    z[71]=n<T>(1,2)*z[71];
    z[84]=z[71] - 7*z[68] + n<T>(3,2)*z[84];
    z[84]=z[8]*z[84];
    z[85]=z[41]*z[24];
    z[86]= - z[36]*z[85];
    z[87]=z[2] + z[61];
    z[70]=z[87]*z[70];
    z[70]=z[84] + z[86] + 3*z[70];
    z[70]=z[8]*z[70];
    z[84]=z[29]*z[24];
    z[86]=z[11]*z[4];
    z[87]=z[24] - n<T>(9,2)*z[86];
    z[87]=z[2]*z[87];
    z[87]=z[84] + z[87];
    z[87]=z[12]*z[87];
    z[88]=z[11]*z[9];
    z[89]= - z[24]*z[88];
    z[87]=z[89] + z[87];
    z[87]=z[87]*z[36];
    z[42]=z[70] + z[87] + z[42];
    z[42]=z[42]*z[63];
    z[63]=z[45]*z[13];
    z[70]=n<T>(1,2)*z[2];
    z[87]=z[3]*z[70];
    z[87]=z[87] - z[19] - z[63] - 4*z[26];
    z[87]=z[3]*z[87];
    z[89]=z[41] + z[16];
    z[89]=z[89]*z[19];
    z[90]=5*z[3];
    z[91]=z[18]*z[90];
    z[89]=z[89] + z[91];
    z[89]=z[3]*z[89];
    z[38]= - z[3]*z[38]*z[18];
    z[38]=z[77] + z[38];
    z[91]=2*z[34];
    z[38]=z[38]*z[91];
    z[38]=z[38] - 4*z[77] + z[89];
    z[38]=z[6]*z[38];
    z[61]=z[26]*z[61];
    z[61]= - z[85] + z[61];
    z[61]=z[61]*z[72];
    z[77]= - z[23] - n<T>(3,2)*z[86];
    z[77]=z[2]*z[77];
    z[85]=z[29]*z[13];
    z[89]=z[11] - z[41];
    z[89]=z[10]*z[89];
    z[89]= - z[85] + z[89];
    z[89]=z[10]*z[89];
    z[92]= - z[11]*z[13];
    z[93]=z[2]*z[43];
    z[92]=z[92] + z[93];
    z[92]=z[92]*z[18];
    z[81]= - z[81] + z[92];
    z[81]=z[12]*z[81];
    z[20]=z[25] + z[20] + z[42] + z[61] + z[38] + z[81] + z[87] + z[77]
    + z[89];
    z[20]=z[1]*z[20];
    z[25]=6*z[13];
    z[38]=z[4]*z[9];
    z[42]=z[38] - 1;
    z[61]= - z[11]*z[42];
    z[77]=z[38] + 2;
    z[81]=z[10]*z[77];
    z[87]=z[9]*z[16];
    z[87]= - static_cast<T>(1)+ z[87];
    z[87]=z[3]*z[87];
    z[61]=z[87] + z[81] + z[54] + z[61] - z[25] + z[4];
    z[61]=z[3]*z[61];
    z[81]=3*z[13];
    z[87]=z[43] - z[81] + z[40];
    z[87]=z[10]*z[87];
    z[85]= - z[85] + z[87];
    z[85]=z[10]*z[85];
    z[87]=z[31]*z[11];
    z[89]=z[2] + z[4];
    z[92]=2*z[21];
    z[93]=z[89]*z[92];
    z[93]= - z[87] + z[93];
    z[93]=z[12]*z[93];
    z[94]= - z[11]*z[41];
    z[94]= - n<T>(15,2)*z[86] + z[94];
    z[94]=z[2]*z[94];
    z[84]=z[93] + z[85] + z[84] + z[94];
    z[84]=z[12]*z[84];
    z[85]= - z[9]*z[45];
    z[85]=z[85] + z[44];
    z[85]=z[3]*z[85];
    z[93]= - z[4] + z[41];
    z[93]=z[12]*z[93]*z[41];
    z[83]= - z[3]*z[83];
    z[83]= - z[52] + z[83];
    z[83]=z[83]*z[78];
    z[94]=2*z[14];
    z[95]= - z[94] + z[27];
    z[95]=z[2]*z[95];
    z[96]=z[11] + z[70];
    z[96]=3*z[96] + z[3];
    z[96]=z[3]*z[96];
    z[95]=z[95] + z[96];
    z[95]=z[8]*z[95];
    z[83]=z[95] + z[83] + z[85] + z[93];
    z[83]=z[8]*z[83];
    z[85]=z[43]*z[9];
    z[93]=z[85] - z[44];
    z[95]=2*z[3];
    z[93]=z[93]*z[95];
    z[96]= - z[35] + z[2];
    z[96]=z[96]*z[46];
    z[97]=z[81] + z[41];
    z[97]=z[10]*z[97];
    z[98]=n<T>(3,2)*z[2];
    z[99]=z[98] + z[10];
    z[99]=3*z[99] + z[3];
    z[99]=z[3]*z[99];
    z[97]=z[97] + z[99];
    z[97]=z[6]*z[97];
    z[93]=z[97] + z[93] + z[96];
    z[93]=z[6]*z[93];
    z[96]=n<T>(7,2)*z[11];
    z[97]=z[40] - z[96];
    z[97]=z[2]*z[97];
    z[97]=z[97] - 3*z[24] + 7*z[86];
    z[97]=z[12]*z[97];
    z[99]=z[11]*z[38];
    z[97]= - n<T>(5,2)*z[99] + z[97];
    z[97]=z[12]*z[97];
    z[99]=z[79]*z[69];
    z[83]=z[83] + z[93] + z[99] + z[97];
    z[37]=z[83]*z[37];
    z[83]=z[10]*z[9];
    z[93]=static_cast<T>(5)+ 6*z[83];
    z[93]=z[93]*z[18];
    z[93]= - z[52] + z[93];
    z[93]=z[3]*z[93];
    z[97]=4*z[21];
    z[93]= - z[97] + z[93];
    z[93]=z[3]*z[93];
    z[48]=z[53] + z[48] + z[93];
    z[48]=z[6]*z[48];
    z[53]= - static_cast<T>(3)- 5*z[83];
    z[53]=z[3]*z[53]*z[43];
    z[53]=z[53] + z[52] + 5*z[18];
    z[53]=z[3]*z[53];
    z[93]=z[12]*z[2];
    z[97]= - z[93]*z[97];
    z[48]=z[48] + z[97] + z[92] + z[53];
    z[48]=z[6]*z[48];
    z[53]= - z[11]*z[77];
    z[77]= - z[9]*z[40];
    z[77]= - static_cast<T>(1)+ z[77];
    z[77]=z[10]*z[77];
    z[15]=z[77] + z[53] - z[13] - z[15];
    z[15]=z[10]*z[15];
    z[53]=z[63] - z[26];
    z[53]=z[3]*z[53];
    z[31]= - z[31] + z[53];
    z[31]=z[31]*z[66];
    z[53]=z[4]*z[45];
    z[63]=z[2] - 8*z[4] - n<T>(13,2)*z[11];
    z[63]=z[2]*z[63];
    z[15]=z[20] + z[17] + z[37] + z[31] + z[48] + z[84] + z[61] + z[15]
    + z[63] - z[23] + z[53];
    z[15]=z[1]*z[15];
    z[17]=4*z[13];
    z[20]=n<T>(3,2)*z[11];
    z[31]=z[17] - z[20];
    z[37]=n<T>(1,2)*z[11];
    z[48]= - z[44]*z[37];
    z[31]=z[48] + 3*z[31] - z[54];
    z[31]=z[3]*z[31];
    z[48]= - z[14] - z[35];
    z[48]=z[48]*z[29];
    z[53]=5*z[4];
    z[61]= - z[27] - z[53] + 7*z[11];
    z[61]=z[2]*z[61];
    z[63]=z[2]*z[11];
    z[63]= - z[24] + z[63];
    z[63]=z[63]*z[93];
    z[77]=z[3]*z[98];
    z[77]=z[52] + z[77];
    z[77]=z[77]*z[74];
    z[84]=z[2]*z[69];
    z[77]=z[84] + z[77];
    z[77]=z[6]*z[77];
    z[72]=z[72]*z[11];
    z[84]= - z[72]*z[25];
    z[31]=z[84] + z[77] + 6*z[63] + z[31] + z[48] + z[61];
    z[31]=z[8]*z[31];
    z[48]=4*z[10];
    z[61]= - z[53] - z[48];
    z[61]=z[10]*z[61];
    z[63]=z[4]*z[21]*z[36];
    z[26]=z[63] - n<T>(1,2)*z[26] + z[61];
    z[26]=z[12]*z[26];
    z[26]=z[26] - z[43] - z[70] - z[45] + z[17] - z[53];
    z[26]=z[12]*z[26];
    z[53]=z[2] - z[3];
    z[53]=z[53]*z[69];
    z[61]= - z[27]*z[51];
    z[53]=5*z[53] + z[61];
    z[53]=z[6]*z[53];
    z[61]=z[27]*z[3];
    z[53]=z[61] + z[53];
    z[53]=z[6]*z[53];
    z[63]=z[19] + 3*z[22];
    z[63]=z[12]*z[63];
    z[39]=n<T>(1,2)*z[53] + z[63] + n<T>(15,2)*z[3] - z[48] + z[39] + z[54];
    z[39]=z[6]*z[39];
    z[48]= - z[45] - z[10];
    z[48]=z[12]*z[48];
    z[48]=static_cast<T>(1)+ z[48];
    z[48]=z[48]*z[36];
    z[46]=static_cast<T>(1)+ z[46];
    z[46]=z[46]*z[62];
    z[53]=static_cast<T>(1)- 6*z[32];
    z[53]=z[12]*z[53];
    z[54]=z[49] + 1;
    z[62]= - z[8]*z[54];
    z[53]=z[62] + z[53] + z[6];
    z[53]=z[8]*z[53];
    z[46]=z[53] + z[48] + z[46];
    z[46]=z[5]*z[46];
    z[48]= - z[72] - z[11] + z[95];
    z[48]=z[48]*z[66];
    z[48]=z[48] - z[91] + z[54];
    z[48]=z[8]*z[48];
    z[53]=z[45] + z[80];
    z[53]=z[12]*z[53];
    z[53]= - static_cast<T>(3)+ z[53];
    z[53]=z[12]*z[53];
    z[54]=z[10] - 3*z[80];
    z[54]=z[12]*z[54];
    z[54]=static_cast<T>(3)+ z[54];
    z[54]=z[6]*z[54];
    z[46]=z[46] + z[48] + z[53] + z[54];
    z[46]=z[5]*z[46];
    z[45]=z[45] - z[95];
    z[45]=z[3]*z[45];
    z[45]= - z[71] + z[45] + n<T>(3,2)*z[51];
    z[45]=z[8]*z[45];
    z[45]=z[45] + n<T>(9,2)*z[74] - 9*z[3] - n<T>(3,2)*z[4] - z[59];
    z[45]=z[8]*z[45];
    z[26]=z[46] + z[45] + z[39] + static_cast<T>(3)+ z[26];
    z[26]=z[5]*z[26];
    z[39]= - z[40] - z[2];
    z[39]=z[39]*z[21];
    z[39]= - z[87] + z[39];
    z[39]=z[12]*z[39];
    z[45]=z[24]*z[73];
    z[23]=z[23] - n<T>(21,2)*z[86];
    z[23]=z[2]*z[23];
    z[42]=z[42]*z[43];
    z[42]= - z[27] + z[42];
    z[42]=z[42]*z[18];
    z[23]=z[39] + z[42] + z[45] + z[23];
    z[23]=z[12]*z[23];
    z[39]=static_cast<T>(10)- z[38];
    z[39]=z[4]*z[39];
    z[39]= - z[94] + z[39];
    z[39]=z[11]*z[39];
    z[42]= - z[73] - z[89];
    z[42]=z[42]*z[41];
    z[38]=3*z[38];
    z[45]= - z[4]*z[79];
    z[45]=z[9] + z[45];
    z[45]=z[10]*z[45];
    z[45]=z[45] + static_cast<T>(1)- z[38];
    z[45]=z[10]*z[45];
    z[46]= - z[13] - z[40];
    z[45]=z[45] + 2*z[46] - z[2];
    z[45]=z[45]*z[43];
    z[23]=z[23] + z[45] + z[42] - 5*z[24] + z[39];
    z[23]=z[12]*z[23];
    z[24]=static_cast<T>(1)+ z[85];
    z[24]=z[10]*z[24];
    z[24]= - z[41] + z[24];
    z[24]=z[10]*z[24];
    z[39]=z[79]*z[10];
    z[42]= - 2*z[9] - z[39];
    z[42]=z[42]*z[43];
    z[42]= - static_cast<T>(1)+ z[42];
    z[42]=z[10]*z[42];
    z[42]=z[98] + z[42];
    z[42]=z[3]*z[42];
    z[24]=z[24] + z[42];
    z[24]=z[3]*z[24];
    z[42]=z[2] - z[10];
    z[19]=z[42]*z[19];
    z[21]= - z[21]*z[93];
    z[42]= - z[52] - z[61];
    z[42]=z[42]*z[74];
    z[19]=z[42] + z[21] + z[19] + z[24];
    z[19]=z[6]*z[19];
    z[21]= - 9*z[9] - z[82];
    z[21]=z[10]*z[21];
    z[21]=static_cast<T>(2)+ z[21];
    z[21]=z[10]*z[21];
    z[24]=8*z[9] + 5*z[39];
    z[24]=z[10]*z[24];
    z[24]=static_cast<T>(1)+ z[24];
    z[24]=z[3]*z[24];
    z[21]=z[24] + z[21] - z[25] + z[75];
    z[21]=z[3]*z[21];
    z[24]= - z[43] - z[81] + z[2];
    z[18]=z[24]*z[18];
    z[22]=z[41]*z[22];
    z[18]=z[18] + z[22];
    z[18]=z[12]*z[18];
    z[22]=static_cast<T>(1)+ z[83];
    z[22]=z[10]*z[22];
    z[22]=2*z[76] + z[22];
    z[22]=z[22]*z[43];
    z[18]=z[19] + z[18] + z[21] + z[52] + z[22];
    z[18]=z[6]*z[18];
    z[19]=5*z[9] + z[39];
    z[19]=z[19]*z[43];
    z[16]= - z[79]*z[16];
    z[16]= - n<T>(7,2)*z[9] + z[16];
    z[16]=z[3]*z[16];
    z[16]=z[16] + z[19] + static_cast<T>(4)+ n<T>(1,2)*z[88];
    z[16]=z[3]*z[16];
    z[17]= - z[40] + z[14] + z[17];
    z[19]=static_cast<T>(7)- z[38];
    z[19]=z[19]*z[37];
    z[21]= - 4*z[9] - z[39];
    z[21]=z[21]*z[43];
    z[21]= - static_cast<T>(11)+ z[21];
    z[21]=z[10]*z[21];
    z[15]=z[15] + z[26] + z[31] + z[18] + z[23] + z[16] + z[21] - 12*
    z[2] + 2*z[17] + z[19];
    z[15]=z[1]*z[15];
    z[16]= - z[11] - z[27];
    z[16]=n<T>(1,2)*z[16] - z[95];
    z[16]=z[3]*z[16];
    z[17]=2*z[51];
    z[18]= - z[96] - z[2];
    z[18]=z[2]*z[18];
    z[16]= - z[71] + z[17] + 4*z[68] + z[18] + z[16];
    z[16]=z[8]*z[16];
    z[18]=z[2] + z[95];
    z[18]=z[18]*z[28];
    z[18]= - 3*z[51] + z[52] + z[18];
    z[18]=z[6]*z[18];
    z[19]=z[20] - z[2];
    z[20]=z[12]*z[19]*z[27];
    z[21]= - 17*z[11] + z[60];
    z[22]=z[9]*z[29];
    z[22]= - n<T>(5,2) + z[22];
    z[22]=z[3]*z[22];
    z[16]=z[16] + z[18] + z[20] + n<T>(1,2)*z[21] + z[22];
    z[16]=z[8]*z[16];
    z[18]=z[4]*z[93];
    z[18]= - z[75] + z[18];
    z[20]=n<T>(1,2)*z[12];
    z[18]=z[18]*z[20];
    z[21]= - z[70] - z[95];
    z[21]=z[21]*z[28];
    z[17]=z[21] + z[17];
    z[17]=z[6]*z[17];
    z[17]=z[17] + z[2] + z[56];
    z[17]=z[6]*z[17];
    z[21]=2*z[44];
    z[22]=static_cast<T>(5)- z[21];
    z[16]=z[16] + z[17] + 2*z[22] + z[18];
    z[16]=z[8]*z[16];
    z[17]= - z[69]*z[88];
    z[18]=z[11]*z[27];
    z[17]=z[18] + z[17];
    z[17]=n<T>(1,2)*z[17] - z[68];
    z[17]=z[8]*z[17];
    z[18]=n<T>(3,2)*z[88];
    z[22]= - z[18] + z[21];
    z[22]=z[3]*z[22];
    z[19]= - z[19]*z[93];
    z[23]=z[11] - z[2];
    z[17]=z[17] + z[19] + 3*z[23] + z[22];
    z[17]=z[8]*z[17];
    z[19]= - z[9]*z[69];
    z[19]=z[70] + z[19];
    z[19]=z[6]*z[19];
    z[22]=n<T>(3,2)*z[44];
    z[19]=z[19] + n<T>(1,2)*z[93] - static_cast<T>(2)+ z[22];
    z[17]=3*z[19] + z[17];
    z[17]=z[8]*z[17];
    z[19]=z[74]*z[9];
    z[22]=static_cast<T>(1)- z[22];
    z[22]=3*z[22] + 2*z[19];
    z[22]=z[6]*z[22];
    z[17]=z[22] + z[17];
    z[17]=z[8]*z[17];
    z[19]=3*z[44] - z[19];
    z[19]=z[19]*z[50];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[7]*z[17];
    z[19]=2*z[69];
    z[22]= - z[19] + n<T>(1,2)*z[51];
    z[22]=z[22]*z[6];
    z[23]=n<T>(7,2)*z[3];
    z[24]= - z[10]*z[93];
    z[24]= - z[22] - z[23] + z[24];
    z[24]=z[6]*z[24];
    z[25]=z[2]*z[67];
    z[21]=z[24] + z[25] - n<T>(3,2) + z[21];
    z[21]=z[6]*z[21];
    z[24]=z[12]*z[4];
    z[24]=static_cast<T>(1)+ z[24];
    z[20]=z[24]*z[20];
    z[16]=z[17] + z[16] + z[20] + z[21];
    z[16]=z[7]*z[16];
    z[17]=static_cast<T>(3)- z[32];
    z[17]=z[12]*z[17];
    z[17]=z[17] - z[78];
    z[20]= - z[47] + static_cast<T>(4)- z[49];
    z[20]=z[8]*z[20];
    z[17]=3*z[17] + z[20];
    z[17]=z[8]*z[17];
    z[20]=z[32] - 6;
    z[21]= - z[20]*z[36];
    z[24]=9*z[12];
    z[25]= - z[24] + z[55];
    z[25]=z[6]*z[25];
    z[17]=z[17] + z[21] + z[25];
    z[17]=z[8]*z[17];
    z[21]=3*z[36];
    z[25]=3*z[12] - z[6];
    z[25]=z[6]*z[25];
    z[25]= - z[21] + z[25];
    z[25]=z[6]*z[25];
    z[17]=z[17] + z[64] + z[25];
    z[17]=z[17]*z[65];
    z[25]=4*z[58] - z[72];
    z[25]=z[8]*z[25];
    z[25]=z[25] - 6*z[34] + static_cast<T>(13)- 5*z[32];
    z[25]=z[8]*z[25];
    z[26]=2*z[12];
    z[20]= - z[20]*z[26];
    z[27]= - static_cast<T>(15)+ 4*z[34];
    z[27]=z[6]*z[27];
    z[20]=z[25] + z[20] + z[27];
    z[20]=z[8]*z[20];
    z[25]=static_cast<T>(7)- z[34];
    z[25]=z[6]*z[25];
    z[24]= - z[24] + z[25];
    z[24]=z[6]*z[24];
    z[20]=z[20] + z[21] + z[24];
    z[20]=z[8]*z[20];
    z[21]=z[26] - z[6];
    z[21]=z[6]*z[21];
    z[21]= - z[36] + z[21];
    z[21]=z[6]*z[21];
    z[17]=z[17] + z[21] + z[20];
    z[17]=z[5]*z[17];
    z[19]=z[19] - z[51];
    z[19]=z[6]*z[19];
    z[20]= - z[11] - z[3];
    z[20]=z[3]*z[20];
    z[20]=z[20] + z[51];
    z[20]=2*z[20] - z[71];
    z[20]=z[8]*z[20];
    z[19]=z[20] + 3*z[19] - 6*z[11] + z[90];
    z[19]=z[8]*z[19];
    z[20]= - 3*z[69] + z[51];
    z[20]=z[6]*z[20];
    z[20]= - z[28] + z[20];
    z[20]=z[20]*z[78];
    z[19]=z[19] + z[20] + n<T>(27,2) - z[33];
    z[19]=z[8]*z[19];
    z[20]=z[90] - z[22];
    z[20]=z[6]*z[20];
    z[20]= - n<T>(23,2) + z[20];
    z[20]=z[6]*z[20];
    z[19]=z[19] + 5*z[12] + z[20];
    z[19]=z[8]*z[19];
    z[20]=z[10] + z[2];
    z[20]=z[20]*z[12];
    z[21]= - z[91] + static_cast<T>(4)- z[20];
    z[21]=z[6]*z[21];
    z[20]= - static_cast<T>(2)+ z[20];
    z[20]=z[12]*z[20];
    z[20]=z[20] + z[21];
    z[20]=z[6]*z[20];
    z[17]=2*z[17] + z[20] + z[19];
    z[17]=z[7]*z[17];
    z[17]=z[66] - z[26] - n<T>(25,2)*z[6] + z[17];
    z[17]=z[5]*z[17];
    z[19]= - z[94] - z[81];
    z[19]= - z[23] + 8*z[2] + 2*z[19] + z[30];
    z[19]=z[8]*z[19];
    z[20]=z[2] + z[30] + z[35] - n<T>(11,2)*z[4];
    z[20]=z[12]*z[20];
    z[21]= - z[57] - z[43] + 12*z[13] - n<T>(13,2)*z[2];
    z[21]=z[6]*z[21];
    z[22]= - z[9]*z[94];

    r +=  - static_cast<T>(1)+ z[15] + z[16] + z[17] - z[18] + z[19] + z[20] + z[21]
       + z[22];
 
    return r;
}

template double qg_2lNLC_r869(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r869(const std::array<dd_real,31>&);
#endif
