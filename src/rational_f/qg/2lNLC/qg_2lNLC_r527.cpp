#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r527(const std::array<T,31>& k) {
  T z[142];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[13];
    z[5]=k[15];
    z[6]=k[8];
    z[7]=k[14];
    z[8]=k[10];
    z[9]=k[12];
    z[10]=k[3];
    z[11]=k[6];
    z[12]=k[2];
    z[13]=k[5];
    z[14]=k[4];
    z[15]=k[26];
    z[16]=k[16];
    z[17]=z[4]*z[10];
    z[18]=5*z[17];
    z[19]=z[2]*z[10];
    z[20]=z[18] - z[19];
    z[21]=npow(z[11],3);
    z[22]=n<T>(1,2)*z[21];
    z[20]=z[20]*z[22];
    z[23]=2*z[14];
    z[24]=z[23]*z[21];
    z[25]=z[4] + z[2];
    z[25]=z[25]*z[24];
    z[26]=npow(z[7],2);
    z[27]=z[26]*z[9];
    z[28]=z[26]*z[4];
    z[29]=n<T>(1,2)*z[28];
    z[30]=z[26]*z[2];
    z[20]=z[25] + z[20] - z[27] + z[29] - z[30];
    z[20]=z[14]*z[20];
    z[25]=z[2]*z[7];
    z[31]=n<T>(1,2)*z[4];
    z[32]= - z[7]*z[31];
    z[33]=z[7] + n<T>(3,2)*z[6];
    z[33]=z[9]*z[33];
    z[20]=z[20] + z[33] + z[32] - z[25];
    z[20]=z[14]*z[20];
    z[32]=3*z[12];
    z[33]=npow(z[10],2);
    z[34]=z[32]*z[33];
    z[35]=z[21]*z[34];
    z[36]=z[33]*z[21];
    z[37]=z[6] + z[7];
    z[38]=z[37]*z[9];
    z[39]=z[14]*z[38];
    z[36]=z[39] + z[6] - 3*z[36];
    z[36]=z[14]*z[36];
    z[39]=z[6]*z[12];
    z[40]=z[3]*z[12];
    z[35]=z[36] + z[35] - z[40] + z[39];
    z[35]=z[8]*z[35];
    z[36]=n<T>(3,2)*z[12];
    z[41]=z[33]*z[4];
    z[42]=z[41] - z[10] + z[36];
    z[43]=5*z[3];
    z[42]=z[42]*z[43];
    z[42]=z[42] - static_cast<T>(4)+ z[18];
    z[42]=z[3]*z[42];
    z[44]=n<T>(3,2)*z[10];
    z[45]=npow(z[12],2);
    z[46]=z[45]*z[44];
    z[47]=npow(z[10],3);
    z[46]=2*z[47] + z[46];
    z[46]=z[3]*z[46];
    z[48]=z[47]*z[4];
    z[46]= - n<T>(5,2)*z[48] + z[46];
    z[46]=z[46]*z[21];
    z[49]=z[45]*z[3];
    z[50]=z[12] - z[49];
    z[50]=z[3]*z[50];
    z[50]=n<T>(1,2) + z[50];
    z[50]=z[6]*z[50];
    z[20]=z[35] + z[20] + z[46] - n<T>(3,2)*z[9] + z[50] - z[31] + z[42];
    z[20]=z[8]*z[20];
    z[35]=2*z[26];
    z[42]=npow(z[6],2);
    z[46]=2*z[6];
    z[50]=z[46] - z[15];
    z[51]=z[9]*z[50];
    z[51]=z[51] - z[35] + z[42];
    z[52]=2*z[9];
    z[51]=z[51]*z[52];
    z[53]=2*z[10];
    z[54]=npow(z[2],2);
    z[55]=z[53]*z[54];
    z[56]=z[21]*z[55];
    z[57]=2*z[4];
    z[58]=z[57]*z[7];
    z[59]= - z[58] + n<T>(3,2)*z[25];
    z[60]=z[21]*z[14];
    z[61]= - z[59]*z[60];
    z[56]=z[56] + z[61];
    z[56]=z[14]*z[56];
    z[51]=z[56] + z[51] + z[28] - 15*z[30];
    z[51]=z[14]*z[51];
    z[56]= - z[47]*z[57];
    z[61]=z[53]*z[12];
    z[62]= - z[33] - z[61];
    z[62]=z[12]*z[62];
    z[62]=n<T>(1,2)*z[47] + z[62];
    z[62]=z[3]*z[62];
    z[56]=z[56] + z[62];
    z[56]=z[56]*z[21];
    z[62]=7*z[4];
    z[63]=3*z[17];
    z[64]= - static_cast<T>(4)+ z[63];
    z[64]=z[3]*z[64];
    z[56]=z[56] + z[62] + z[64];
    z[56]=z[3]*z[56];
    z[64]=n<T>(7,2)*z[3];
    z[65]=7*z[7];
    z[66]=2*z[2];
    z[67]=n<T>(17,2)*z[6] - z[66] - z[65] - 6*z[15] + z[64];
    z[67]=z[9]*z[67];
    z[68]=3*z[40];
    z[69]= - static_cast<T>(1)+ z[68];
    z[69]=z[69]*z[43];
    z[69]=z[69] + z[46];
    z[69]=z[6]*z[69];
    z[70]=4*z[4];
    z[71]=z[70]*z[15];
    z[72]=z[7]*z[4];
    z[20]=z[20] + z[51] + z[67] + z[69] - 26*z[25] - n<T>(5,2)*z[72] - z[71]
    + z[56];
    z[20]=z[8]*z[20];
    z[51]=z[21]*z[45];
    z[56]=z[21]*z[12];
    z[67]=z[56] - z[60];
    z[69]=3*z[14];
    z[67]=z[67]*z[69];
    z[67]=z[67] - z[2] - z[51];
    z[67]=z[14]*z[67];
    z[73]=z[45]*z[7];
    z[74]=z[73] + z[12];
    z[75]=z[74]*z[6];
    z[67]=z[67] - static_cast<T>(1)+ z[75];
    z[67]=z[5]*z[67];
    z[76]=n<T>(1,2)*z[73];
    z[77]=z[32] - z[76];
    z[77]=z[7]*z[77];
    z[77]=static_cast<T>(2)+ z[77];
    z[77]=z[6]*z[77];
    z[78]=npow(z[14],3);
    z[79]=z[78]*z[21];
    z[80]=3*z[2];
    z[81]=z[80]*z[79];
    z[82]=z[56] - z[24];
    z[83]=z[14]*z[82];
    z[84]=4*z[2];
    z[83]=z[83] - z[84] - z[9];
    z[83]=z[14]*z[83];
    z[85]=z[39] - 1;
    z[83]=z[83] + z[85];
    z[83]=z[8]*z[83];
    z[86]=4*z[9];
    z[87]=z[7]*z[12];
    z[88]=n<T>(1,2)*z[87];
    z[89]=static_cast<T>(1)+ z[88];
    z[89]=z[7]*z[89];
    z[67]=z[67] + z[83] + z[81] - z[86] + z[77] + z[89] - 9*z[2];
    z[67]=z[5]*z[67];
    z[77]= - static_cast<T>(1)- 2*z[87];
    z[77]=z[7]*z[77];
    z[77]=z[46] - z[57] + z[77];
    z[77]=z[6]*z[77];
    z[81]=11*z[6];
    z[83]=n<T>(5,2)*z[7];
    z[89]= - z[52] + z[81] + 38*z[3] - z[83];
    z[89]=z[9]*z[89];
    z[90]=z[32]*z[21];
    z[24]=z[90] - z[24];
    z[24]=z[24]*z[69];
    z[24]=z[24] + 3*z[9] - 4*z[51];
    z[24]=z[14]*z[24];
    z[24]=z[24] + z[85];
    z[24]=z[8]*z[24];
    z[69]=z[66]*z[79];
    z[84]=z[84] + z[6];
    z[24]=z[24] + z[69] + 2*z[84] + 9*z[9];
    z[24]=z[8]*z[24];
    z[69]=3*z[7];
    z[84]=9*z[3];
    z[85]= - 13*z[4] + z[84];
    z[85]=n<T>(1,2)*z[85] + z[69];
    z[85]=z[2]*z[85];
    z[90]=2*z[54];
    z[79]= - z[90]*z[79];
    z[91]=z[3]*z[4];
    z[24]=z[67] + z[24] + z[79] + z[89] + z[77] + z[85] + n<T>(13,2)*z[91]
    - z[58];
    z[24]=z[5]*z[24];
    z[58]=npow(z[10],4);
    z[67]= - z[58]*z[57];
    z[77]= - z[12]*z[33];
    z[77]= - z[47] + z[77];
    z[77]=z[12]*z[77];
    z[77]=z[58] + z[77];
    z[79]=n<T>(1,2)*z[3];
    z[77]=z[77]*z[79];
    z[67]=z[67] + z[77];
    z[77]=npow(z[11],4);
    z[67]=z[3]*z[67]*z[77];
    z[78]=z[78]*z[77];
    z[85]= - z[72] + z[25];
    z[85]=z[85]*z[78];
    z[27]=z[85] - z[28] + z[27];
    z[85]=n<T>(1,2)*z[14];
    z[27]=z[27]*z[85];
    z[89]= - z[33]*z[77]*z[49];
    z[92]=npow(z[14],2);
    z[93]=z[92]*z[77];
    z[94]=z[33]*z[2];
    z[95]=z[94]*z[93];
    z[89]=z[89] + z[95];
    z[89]=z[8]*z[89];
    z[95]=npow(z[3],2);
    z[96]=static_cast<T>(1)- z[17];
    z[96]=z[96]*z[95];
    z[97]=z[6]*z[3];
    z[98]= - static_cast<T>(1)- n<T>(7,2)*z[40];
    z[98]=z[98]*z[97];
    z[99]=z[52]*z[6];
    z[27]=z[89] + z[27] + z[67] - z[99] + n<T>(5,2)*z[96] + z[98];
    z[27]=z[8]*z[27];
    z[67]=n<T>(1,2)*z[7];
    z[89]= - z[57] + z[67];
    z[89]=z[7]*z[89];
    z[96]=z[6]*z[4];
    z[89]= - n<T>(7,2)*z[96] - n<T>(3,2)*z[91] + z[89];
    z[89]=z[9]*z[89];
    z[98]=z[30] - z[28];
    z[100]=z[14]*z[11];
    z[101]=npow(z[100],4);
    z[102]=z[98]*z[101];
    z[103]= - z[95]*z[57];
    z[62]= - z[62] + z[84];
    z[62]=z[6]*z[62]*z[79];
    z[27]=z[27] + n<T>(1,2)*z[102] + z[89] + z[62] + z[103] - z[29];
    z[27]=z[8]*z[27];
    z[62]=z[95]*z[4];
    z[89]=z[62] + z[28];
    z[103]=z[3] + z[4];
    z[104]= - z[103]*z[97];
    z[105]=z[7] + z[4];
    z[105]=z[105]*z[7];
    z[106]= - z[105] - z[96];
    z[106]=z[9]*z[106];
    z[107]=z[11]*z[10];
    z[107]= - z[62]*npow(z[107],5);
    z[104]=z[107] + z[106] + z[104] + z[89];
    z[104]=z[8]*z[104];
    z[106]=npow(z[4],2);
    z[107]=z[106] + z[91];
    z[97]=z[107]*z[97];
    z[108]=z[6] + z[3];
    z[109]=z[108]*z[9];
    z[110]=z[106]*z[109];
    z[97]=z[104] + z[97] + z[110];
    z[97]=z[8]*z[97];
    z[104]=npow(z[8],2);
    z[110]=z[9]*z[104]*z[28];
    z[111]=z[62]*z[2];
    z[112]= - z[6]*z[28];
    z[112]=z[111] + z[112];
    z[113]=npow(z[5],2);
    z[112]=z[112]*z[113];
    z[110]=z[110] + z[112];
    z[110]=z[1]*z[110];
    z[112]=z[6]*z[2];
    z[107]= - z[3]*z[107]*z[112];
    z[114]=z[2]*z[3];
    z[115]= - z[114] - z[112];
    z[106]=z[115]*z[106]*z[9];
    z[97]=z[110] + z[97] + z[107] + z[106];
    z[106]=n<T>(1,2)*z[89];
    z[107]=z[5] + z[8];
    z[110]=z[9]*z[6];
    z[107]=z[110]*z[107];
    z[115]= - z[57] - z[79];
    z[115]=z[115]*z[114];
    z[116]=z[67] + z[57];
    z[116]=z[116]*z[7];
    z[117]=z[6]*z[116];
    z[107]=z[117] + z[106] + z[115] + z[107];
    z[107]=z[5]*z[107];
    z[104]=z[104]*z[110];
    z[104]=z[107] - n<T>(5,2)*z[111] + z[104];
    z[104]=z[5]*z[104];
    z[97]=z[104] + n<T>(1,2)*z[97];
    z[97]=z[1]*z[97];
    z[104]=5*z[4];
    z[107]=z[104] + z[3];
    z[107]=z[107]*z[114];
    z[115]=z[42] + z[110];
    z[115]=z[9]*z[115];
    z[117]=z[6]*z[72];
    z[107]=z[115] + z[117] - z[62] + z[107];
    z[103]=z[103]*z[80];
    z[115]=3*z[4];
    z[117]= - static_cast<T>(3)- z[87];
    z[117]=z[7]*z[117];
    z[117]= - z[115] + z[117];
    z[117]=z[6]*z[117];
    z[118]= - z[3]*z[115];
    z[119]= - z[115] - z[7];
    z[119]=z[7]*z[119];
    z[103]=z[117] + z[103] + z[118] + z[119];
    z[101]=z[101]*z[2];
    z[117]=z[9] - z[6];
    z[101]=z[101] - z[117];
    z[118]=z[101]*z[8];
    z[101]=z[5]*z[101];
    z[101]=z[101] + z[118] + n<T>(1,2)*z[103] + z[110];
    z[101]=z[5]*z[101];
    z[99]=z[99] + z[118];
    z[99]=z[8]*z[99];
    z[99]=z[101] + 2*z[107] + z[99];
    z[99]=z[5]*z[99];
    z[101]=z[2]*z[4];
    z[103]=z[4] - z[7];
    z[103]=z[7]*z[103];
    z[103]= - z[109] + z[96] - z[101] + z[91] + z[103];
    z[103]=z[5]*z[103];
    z[107]=z[62] - z[28];
    z[118]=z[95]*z[9];
    z[103]=z[103] + z[118] - z[107];
    z[119]=n<T>(1,2)*z[5];
    z[103]=z[119]*z[103];
    z[120]=z[112] + z[54];
    z[121]=4*z[6];
    z[122]=z[121]*z[120];
    z[123]= - z[112]*z[86];
    z[123]=z[123] - z[29] - z[122];
    z[123]=z[9]*z[123];
    z[103]=z[123] + z[103];
    z[103]=z[13]*z[77]*z[103];
    z[77]=z[77]*z[26];
    z[123]=z[12]*z[113]*z[77];
    z[103]= - n<T>(7,2)*z[123] + z[103];
    z[103]=z[13]*z[103];
    z[77]= - z[45]*z[77];
    z[93]=z[5]*z[2]*z[93];
    z[77]=z[77] + z[93];
    z[77]=z[77]*z[113];
    z[77]=z[77] + z[103];
    z[77]=z[13]*z[77];
    z[93]=z[95]*z[2];
    z[103]= - z[93] + z[107];
    z[78]=z[2]*z[78];
    z[78]=z[110] + z[78];
    z[123]=2*z[5];
    z[78]=z[78]*z[123];
    z[78]=n<T>(1,2)*z[103] + z[78];
    z[78]=z[78]*z[113];
    z[77]=z[78] + z[77];
    z[77]=z[13]*z[77];
    z[78]=n<T>(7,2)*z[4];
    z[103]=z[78] - z[3];
    z[103]=z[3]*z[103];
    z[103]=z[103] - z[26];
    z[103]=z[2]*z[103];
    z[29]=z[29] + z[103];
    z[29]=z[6]*z[29];
    z[103]=z[2]*z[91];
    z[103]= - z[28] + z[103];
    z[124]=7*z[112];
    z[125]=z[4]*z[124];
    z[103]=3*z[103] + z[125];
    z[125]=n<T>(1,2)*z[9];
    z[103]=z[103]*z[125];
    z[126]=n<T>(1,2)*z[6];
    z[102]= - z[126]*z[102];
    z[27]=z[97] + z[77] + z[99] + z[27] + z[102] + z[103] + 4*z[111] + 
    z[29];
    z[27]=z[1]*z[27];
    z[29]=z[82]*z[23];
    z[29]=z[29] - z[66] - z[117];
    z[29]=z[5]*z[29];
    z[77]=3*z[73];
    z[82]=z[21]*z[77];
    z[97]=3*z[109];
    z[99]=z[4] + z[67];
    z[99]=z[7]*z[99];
    z[102]=z[4] - n<T>(3,2)*z[3];
    z[102]=z[2]*z[102];
    z[29]=z[29] + z[82] - z[97] + z[96] + z[102] - z[91] + z[99];
    z[29]=z[5]*z[29];
    z[82]=3*z[6];
    z[91]= - z[15] + z[82];
    z[91]=z[91]*z[52];
    z[96]=13*z[95];
    z[91]=z[91] + z[96] + 4*z[42];
    z[91]=z[9]*z[91];
    z[99]= - 3*z[62] + 7*z[93];
    z[102]=13*z[26];
    z[51]=z[51]*z[102];
    z[92]=z[92]*z[21];
    z[103]= - z[90]*z[92];
    z[29]=z[29] + z[103] + z[51] + n<T>(1,2)*z[99] + z[91];
    z[29]=z[5]*z[29];
    z[51]= - z[60] - z[114] + z[110];
    z[51]=z[5]*z[51];
    z[91]=z[21]*z[87];
    z[99]=z[14]*z[2];
    z[103]=z[99]*z[22];
    z[51]=z[51] + z[103] + n<T>(23,2)*z[91] - z[118] + z[106] - z[93];
    z[51]=z[5]*z[51];
    z[56]=z[56]*z[102];
    z[51]=z[56] + z[51];
    z[51]=z[5]*z[51];
    z[56]=z[4] + z[80];
    z[56]=z[56]*z[66];
    z[91]=10*z[2];
    z[93]= - z[91] + z[6];
    z[93]=z[93]*z[121];
    z[102]=z[80] + z[46];
    z[102]=z[102]*z[52];
    z[56]=z[102] + z[93] + z[56] - z[95] - z[116];
    z[56]=z[9]*z[56];
    z[56]= - z[122] + z[56];
    z[56]=z[56]*z[21];
    z[93]=z[2] - z[7];
    z[102]=3*z[3];
    z[82]= - z[82] - z[102] + z[93];
    z[82]=z[82]*z[125];
    z[103]= - z[2] + z[108];
    z[103]=z[57]*z[103];
    z[106]=z[57] + z[7];
    z[106]=z[7]*z[106];
    z[82]=z[82] + z[106] + z[103];
    z[82]=z[82]*z[21];
    z[103]=13*z[7];
    z[106]=z[103] - z[2];
    z[22]=z[5]*z[106]*z[22];
    z[22]=z[82] + z[22];
    z[22]=z[5]*z[22];
    z[22]=z[56] + z[22];
    z[22]=z[13]*z[22];
    z[56]=n<T>(1,2)*z[95];
    z[82]=z[10]*z[56];
    z[106]=z[66]*z[10];
    z[108]= - z[9]*z[106];
    z[82]=z[108] + z[82] - z[55];
    z[82]=z[9]*z[82];
    z[108]= - z[62]*z[44];
    z[82]=z[108] + z[82];
    z[82]=z[82]*z[21];
    z[108]=n<T>(1,2)*z[26];
    z[109]=2*z[42];
    z[111]= - z[15]*z[52];
    z[111]=z[111] + z[108] - z[109];
    z[111]=z[9]*z[111];
    z[111]=z[122] + z[111];
    z[60]=z[111]*z[60];
    z[22]=z[22] + z[51] + z[82] + z[60];
    z[22]=z[13]*z[22];
    z[51]=z[120]*z[6];
    z[60]=z[66]*z[6];
    z[82]=z[2]*z[15];
    z[82]=z[82] - z[60];
    z[82]=z[9]*z[82];
    z[82]= - z[51] + z[82];
    z[82]=z[82]*z[52];
    z[111]=z[6] - z[15];
    z[116]= - z[111]*npow(z[9],2);
    z[116]= - z[51] + z[116];
    z[116]=z[116]*z[92];
    z[117]= - z[112]*z[56];
    z[22]=z[22] + z[29] + 2*z[116] + z[117] + z[82];
    z[22]=z[13]*z[22];
    z[29]=z[59]*z[92];
    z[29]=z[29] + z[28] + 4*z[30];
    z[29]=z[14]*z[29];
    z[59]=n<T>(11,2)*z[7];
    z[82]= - z[4] - z[43];
    z[82]=4*z[82] - z[59];
    z[82]=z[7]*z[82];
    z[29]=z[29] + n<T>(7,2)*z[114] - n<T>(27,2)*z[95] + z[82];
    z[29]=z[6]*z[29];
    z[82]=20*z[3];
    z[92]=4*z[16];
    z[114]=z[82] - z[92];
    z[116]=z[114] + n<T>(9,2)*z[4];
    z[117]=2*z[7];
    z[120]= - z[117] - z[116];
    z[120]=z[7]*z[120];
    z[122]=4*z[15];
    z[127]= - n<T>(9,2)*z[3] - z[122] + z[31];
    z[127]=z[2]*z[127];
    z[128]=z[111]*z[52];
    z[120]=z[128] - 18*z[112] + z[120] + z[127];
    z[120]=z[9]*z[120];
    z[21]=z[47]*z[21]*z[62];
    z[21]=z[107] + z[21];
    z[127]=4*z[7];
    z[43]= - z[43] - z[127];
    z[43]=z[43]*z[127];
    z[128]= - 15*z[4] - z[3];
    z[128]=z[3]*z[128];
    z[43]=z[43] - z[71] + z[128];
    z[43]=z[2]*z[43];
    z[20]=z[27] + z[22] + z[24] + z[20] + z[120] + z[43] + n<T>(3,2)*z[21]
    + z[29];
    z[20]=z[1]*z[20];
    z[21]=z[37]*z[52];
    z[22]= - z[28] + 7*z[30];
    z[22]=z[22]*z[85];
    z[24]=npow(z[11],2);
    z[27]=z[4] - n<T>(7,2)*z[7];
    z[27]=z[7]*z[27];
    z[21]=z[22] - n<T>(27,2)*z[24] + z[21] + z[27] + z[25];
    z[21]=z[14]*z[21];
    z[22]=z[44] + 17*z[12];
    z[22]=z[22]*z[24];
    z[27]=z[46] + z[2];
    z[21]=z[21] + z[22] + z[4] - z[127] + z[27];
    z[21]=z[14]*z[21];
    z[22]=z[53] - z[12];
    z[22]=z[22]*z[40];
    z[29]=z[45]*z[6];
    z[43]=z[14]*z[7];
    z[43]=static_cast<T>(1)+ z[43];
    z[43]=z[14]*z[43];
    z[22]=z[43] + z[29] - z[53] + z[22];
    z[22]=z[8]*z[22];
    z[43]= - z[47]*z[104];
    z[44]= - 5*z[10] + z[36];
    z[44]=z[12]*z[44];
    z[43]=z[43] + 5*z[33] + z[44];
    z[43]=z[3]*z[43];
    z[43]=z[43] - 10*z[41] + n<T>(15,2)*z[10] - 8*z[12];
    z[43]=z[3]*z[43];
    z[36]=z[36] - z[49];
    z[36]=z[6]*z[36];
    z[44]=7*z[12];
    z[120]= - n<T>(1,2)*z[10] - z[12];
    z[120]=z[120]*z[44];
    z[120]=n<T>(3,2)*z[33] + z[120];
    z[120]=z[120]*z[24];
    z[128]= - static_cast<T>(1)- 7*z[17];
    z[21]=z[22] + z[21] + z[120] + z[36] + z[19] + n<T>(1,2)*z[128] + z[43];
    z[21]=z[8]*z[21];
    z[22]=z[10]*z[16];
    z[36]= - z[22] - z[63];
    z[36]=n<T>(5,2)*z[39] - n<T>(17,2)*z[19] + 4*z[36] - z[88];
    z[36]=z[36]*z[24];
    z[43]=5*z[2];
    z[63]= - z[16] - z[57];
    z[37]=z[43] + 2*z[63] - z[37];
    z[37]=z[37]*z[24];
    z[63]=3*z[28];
    z[120]= - z[63] + 47*z[30];
    z[128]= - z[9]*z[111];
    z[128]= - z[26] + z[128];
    z[128]=z[128]*z[52];
    z[37]=2*z[37] + n<T>(1,2)*z[120] + z[128];
    z[37]=z[14]*z[37];
    z[120]=n<T>(7,2)*z[6];
    z[128]=z[120] + z[122];
    z[129]= - z[83] + z[128];
    z[129]=z[9]*z[129];
    z[130]=z[70] - 25*z[7];
    z[130]=z[7]*z[130];
    z[36]=z[37] + z[36] + z[129] + z[130] + 64*z[25];
    z[36]=z[14]*z[36];
    z[37]= - z[16] - z[70];
    z[37]=z[33]*z[37];
    z[129]=9*z[12];
    z[130]=n<T>(11,2)*z[10] + z[129];
    z[130]=z[12]*z[130];
    z[130]=4*z[33] + z[130];
    z[130]=z[3]*z[130];
    z[37]= - n<T>(5,2)*z[29] + z[76] + 2*z[37] + z[130];
    z[37]=z[37]*z[24];
    z[130]= - z[33]*z[57];
    z[130]=z[130] + z[53] - 13*z[12];
    z[130]=z[3]*z[130];
    z[131]=static_cast<T>(11)- 4*z[17];
    z[130]=2*z[131] + z[130];
    z[130]=z[3]*z[130];
    z[131]=z[46]*z[12];
    z[132]=38*z[12] + 13*z[49];
    z[132]=z[3]*z[132];
    z[132]=z[131] + n<T>(35,2) + z[132];
    z[132]=z[6]*z[132];
    z[133]=n<T>(3,2)*z[19];
    z[134]=z[3]*z[10];
    z[135]=z[133] + n<T>(7,2) - 5*z[134];
    z[135]=z[9]*z[135];
    z[136]=2*z[15];
    z[137]=z[16] - z[136];
    z[138]=z[10]*z[15];
    z[139]=8*z[138];
    z[140]= - static_cast<T>(21)+ z[139];
    z[140]=z[4]*z[140];
    z[21]=z[21] + z[36] + z[37] + z[135] + z[132] + 25*z[2] - n<T>(119,2)*
    z[7] + z[130] + 2*z[137] + z[140];
    z[21]=z[8]*z[21];
    z[36]=z[80]*z[14];
    z[37]=z[36] - 2;
    z[123]=z[37]*z[123];
    z[130]=z[24]*z[12];
    z[132]=z[130] + z[9];
    z[88]=static_cast<T>(5)- z[88];
    z[88]=z[7]*z[88];
    z[88]=z[123] - z[120] + z[43] - 13*z[3] + z[88] - 5*z[132];
    z[88]=z[5]*z[88];
    z[120]=2*z[16];
    z[123]= - z[120] + z[43];
    z[123]=z[123]*z[23];
    z[123]=z[123] + 3*z[39] + n<T>(1,2)*z[40] - 88*z[87];
    z[123]=z[24]*z[123];
    z[132]=6*z[7];
    z[135]= - z[66] + z[132] - z[70] - n<T>(23,2)*z[3];
    z[135]=z[2]*z[135];
    z[137]= - z[52] + 14*z[6] + z[80] + 68*z[3] - z[59];
    z[137]=z[9]*z[137];
    z[84]=z[57] + z[84];
    z[140]=2*z[3];
    z[84]=z[84]*z[140];
    z[65]=z[57] + z[65];
    z[65]=z[7]*z[65];
    z[141]=z[4] + z[6];
    z[141]=z[141]*z[46];
    z[65]=z[88] + z[137] + z[141] + z[135] + z[84] + z[65] + z[123];
    z[65]=z[5]*z[65];
    z[84]=29*z[2];
    z[88]=3*z[15];
    z[123]=10*z[16] + z[88];
    z[123]= - z[86] + n<T>(51,2)*z[6] + z[84] + n<T>(3,2)*z[7] + 2*z[123] + z[3]
   ;
    z[123]=z[9]*z[123];
    z[135]=z[140] + z[31];
    z[137]= - 42*z[2] + z[120] + z[135];
    z[137]=z[6]*z[137];
    z[140]=6*z[54];
    z[141]= - z[4] - z[79];
    z[141]=z[3]*z[141];
    z[123]=z[123] + z[137] + z[140] + z[108] - z[71] + z[141];
    z[123]=z[123]*z[24];
    z[137]= - z[4] - 7*z[3];
    z[137]=z[3]*z[137];
    z[105]=z[137] - z[105];
    z[135]=z[2]*z[135];
    z[81]= - 19*z[3] - z[81];
    z[81]=z[81]*z[125];
    z[125]= - z[6]*z[31];
    z[137]= - z[3] + z[80];
    z[137]=z[5]*z[137];
    z[81]=z[137] + z[81] + z[125] + n<T>(1,2)*z[105] + z[135] - 8*z[24];
    z[81]=z[5]*z[81];
    z[50]=z[50]*z[52];
    z[50]=z[50] + 15*z[95] + z[109];
    z[50]=z[9]*z[50];
    z[105]= - z[132] + z[16] + z[3];
    z[105]=2*z[105] - n<T>(7,2)*z[2];
    z[105]=z[105]*z[24];
    z[50]=z[81] + z[105] - z[62] + z[50];
    z[50]=z[5]*z[50];
    z[81]=z[15]*z[80];
    z[81]=z[81] - z[124];
    z[81]=z[81]*z[52];
    z[105]=n<T>(3,2)*z[28];
    z[81]=z[81] + z[105] - 8*z[51];
    z[81]=z[9]*z[81];
    z[107]= - 7*z[118] + z[107];
    z[113]=z[113]*z[13];
    z[113]=n<T>(1,2)*z[113];
    z[107]=z[107]*z[113];
    z[50]=z[107] + z[50] + z[81] + z[123];
    z[50]=z[13]*z[50];
    z[81]= - z[127] - z[115] - z[114];
    z[81]=z[81]*z[117];
    z[107]=z[66] - z[136] - z[104];
    z[107]=z[107]*z[66];
    z[114]= - 14*z[2] + z[6];
    z[114]=z[114]*z[121];
    z[27]= - z[15] + z[27];
    z[27]=z[27]*z[86];
    z[27]=z[27] + z[114] + z[107] - z[96] + z[81];
    z[27]=z[9]*z[27];
    z[81]=n<T>(7,2)*z[19];
    z[96]=n<T>(5,2)*z[134];
    z[107]= - z[81] - 12*z[22] - z[96];
    z[107]=z[9]*z[107];
    z[114]=8*z[15];
    z[123]=z[17]*z[114];
    z[125]=z[79]*z[10];
    z[127]=z[57]*z[10];
    z[132]=z[127] - z[125];
    z[132]=z[3]*z[132];
    z[135]=z[26]*z[12];
    z[107]=z[107] - z[55] + n<T>(31,2)*z[135] + z[123] + z[132];
    z[107]=z[107]*z[24];
    z[64]=z[64] - 6*z[2];
    z[64]=z[2]*z[64];
    z[56]= - 6*z[112] + z[56] + z[64];
    z[56]=z[6]*z[56];
    z[64]=5*z[6];
    z[123]=z[57] + n<T>(13,2)*z[2];
    z[123]=z[123]*z[64];
    z[83]= - n<T>(15,2)*z[6] - z[114] + z[83];
    z[83]=z[9]*z[83];
    z[83]=z[83] - z[140] + z[123];
    z[114]=z[24]*z[14];
    z[83]=z[83]*z[114];
    z[27]=z[50] + z[65] + z[83] + z[107] + z[27] + z[56] + z[62] + 
    z[105];
    z[27]=z[13]*z[27];
    z[50]=z[24]*z[45];
    z[56]=5*z[12];
    z[65]= - z[56] + z[73];
    z[65]=z[65]*z[67];
    z[67]=z[130] - z[114];
    z[83]=7*z[2] - z[9];
    z[67]=3*z[83] + 10*z[67];
    z[67]=z[14]*z[67];
    z[83]=z[45]*z[121];
    z[105]= - static_cast<T>(5)+ 9*z[99];
    z[105]=z[14]*z[105];
    z[83]=z[105] - z[12] + z[83];
    z[83]=z[8]*z[83];
    z[105]= - static_cast<T>(1)+ z[36];
    z[105]=z[14]*z[105];
    z[74]=z[105] - z[74];
    z[74]=z[5]*z[74];
    z[65]=z[74] + z[83] + z[67] - 3*z[50] - n<T>(3,2)*z[75] - static_cast<T>(27)+ z[65];
    z[65]=z[5]*z[65];
    z[67]=19*z[114] - 29*z[130] - z[84] + 21*z[9];
    z[67]=z[14]*z[67];
    z[74]=static_cast<T>(3)+ z[99];
    z[74]=z[14]*z[74];
    z[74]= - z[12] + z[74];
    z[74]=z[8]*z[74];
    z[50]=z[74] + z[67] + 8*z[50] + static_cast<T>(113)+ 23*z[39];
    z[50]=z[8]*z[50];
    z[29]=3*z[29];
    z[67]= - z[29] - n<T>(1,2)*z[49] - 38*z[73];
    z[67]=z[67]*z[24];
    z[74]= - static_cast<T>(12)+ z[87];
    z[74]=z[74]*z[117];
    z[75]=2*z[73];
    z[83]= - n<T>(9,2)*z[12] - z[75];
    z[83]=z[7]*z[83];
    z[83]=z[131] + static_cast<T>(11)+ z[83];
    z[83]=z[6]*z[83];
    z[105]=z[24]*z[99];
    z[105]= - z[90] + 17*z[105];
    z[105]=z[14]*z[105];
    z[50]=z[65] + z[50] + z[105] + z[67] + 67*z[9] + z[83] - 31*z[2] + 
   n<T>(145,2)*z[3] + z[74];
    z[50]=z[5]*z[50];
    z[65]=z[66] - z[115] + z[117];
    z[65]=z[46]*z[7]*z[65];
    z[67]= - z[88] + z[121];
    z[67]=z[67]*z[52];
    z[67]=z[67] - 9*z[26] + z[109];
    z[67]=z[9]*z[67];
    z[74]=z[26]*z[32];
    z[83]=n<T>(11,2)*z[87];
    z[105]= - z[6]*z[83];
    z[55]=z[105] + z[74] + z[55];
    z[55]=z[55]*z[24];
    z[74]= - n<T>(21,2)*z[2] + z[69] - z[92] + z[31];
    z[74]=z[6]*z[74];
    z[74]= - z[90] + z[74];
    z[74]=z[74]*z[24];
    z[63]=z[63] - 11*z[30];
    z[63]=z[63]*z[126];
    z[63]=z[63] + z[74];
    z[63]=z[14]*z[63];
    z[28]=z[63] + z[55] + z[67] + z[65] - z[28] + 20*z[30];
    z[28]=z[14]*z[28];
    z[30]=z[33]*z[104];
    z[55]=z[12] - z[10];
    z[63]=n<T>(1,2)*z[12];
    z[65]= - z[55]*z[63];
    z[65]= - z[33] + z[65];
    z[65]=z[3]*z[65];
    z[30]=z[30] + z[65];
    z[30]=z[3]*z[30];
    z[65]= - z[33]*z[71];
    z[67]=20*z[49];
    z[74]= - z[67] - z[77];
    z[74]=z[7]*z[74];
    z[77]=3*z[49];
    z[90]= - z[77] + n<T>(5,2)*z[73];
    z[90]=z[6]*z[90];
    z[30]=z[90] + z[74] + z[65] + z[30];
    z[24]=z[30]*z[24];
    z[30]=n<T>(13,2) - 8*z[40];
    z[30]=5*z[30] - n<T>(9,2)*z[87];
    z[30]=z[7]*z[30];
    z[65]= - z[16] + z[104];
    z[74]= - static_cast<T>(2)- n<T>(27,2)*z[40];
    z[74]=z[3]*z[74];
    z[30]=z[46] - n<T>(89,2)*z[2] + z[30] + 2*z[65] + z[74];
    z[30]=z[6]*z[30];
    z[65]=8*z[4] + z[92] - z[15];
    z[74]= - static_cast<T>(7)- 3*z[134];
    z[74]=z[3]*z[74];
    z[90]=static_cast<T>(33)+ z[134];
    z[90]=z[2]*z[90];
    z[65]=n<T>(57,2)*z[6] + z[90] - n<T>(47,2)*z[7] + 2*z[65] + z[74];
    z[65]=z[9]*z[65];
    z[74]=16*z[4];
    z[90]= - static_cast<T>(7)+ z[17];
    z[90]=z[3]*z[90];
    z[90]= - z[74] + n<T>(5,2)*z[90];
    z[90]=z[3]*z[90];
    z[105]= - n<T>(95,2)*z[7] - 100*z[3] + z[92] + z[31];
    z[105]=z[7]*z[105];
    z[107]=z[102] - z[15] + z[70];
    z[107]=2*z[107] + 53*z[7];
    z[107]=z[2]*z[107];
    z[109]=z[4]*z[15];
    z[109]=24*z[109];
    z[20]=z[20] + z[27] + z[50] + z[21] + z[28] + z[24] + z[65] + z[30]
    + z[107] + z[105] - z[109] + z[90];
    z[20]=z[1]*z[20];
    z[21]=z[126] + z[2] - z[69] + z[122] + n<T>(17,2)*z[3];
    z[21]=z[9]*z[21];
    z[24]=z[87] - z[37];
    z[24]=z[5]*z[24];
    z[27]=n<T>(19,2) - 3*z[87];
    z[27]=z[7]*z[27];
    z[24]=z[24] - n<T>(5,2)*z[6] - n<T>(3,2)*z[2] + z[79] + z[27];
    z[24]=z[5]*z[24];
    z[27]= - z[31] + z[3];
    z[27]=z[27]*z[102];
    z[28]=z[120] - z[4];
    z[30]=n<T>(13,2)*z[7];
    z[37]=2*z[28] + z[30];
    z[37]=z[7]*z[37];
    z[50]=z[7] - n<T>(5,2)*z[4];
    z[50]=z[2]*z[50];
    z[65]= - z[6]*z[57];
    z[21]=z[24] + z[21] + z[65] + z[50] + z[37] - z[71] + z[27];
    z[21]=z[5]*z[21];
    z[24]=z[91] + z[122] + z[31];
    z[24]=z[2]*z[24];
    z[27]= - z[59] - z[116];
    z[27]=z[7]*z[27];
    z[37]=z[64] - z[15] + z[43];
    z[37]=z[37]*z[52];
    z[43]= - 79*z[2] + 8*z[6];
    z[43]=z[6]*z[43];
    z[24]=z[37] + z[43] + z[24] - 14*z[95] + z[27];
    z[24]=z[9]*z[24];
    z[27]=z[15]*z[66];
    z[27]=z[27] - z[124];
    z[27]=z[9]*z[27];
    z[27]= - 5*z[51] + z[27];
    z[27]=z[27]*z[52];
    z[37]= - z[62] + 9*z[118];
    z[43]=z[108] - z[97];
    z[43]=z[5]*z[43];
    z[37]=n<T>(1,2)*z[37] + z[43];
    z[37]=z[5]*z[37];
    z[43]= - z[118] + z[89];
    z[43]=z[43]*z[113];
    z[27]=z[43] + z[27] + z[37];
    z[27]=z[13]*z[27];
    z[21]=z[27] + z[21] + z[24] + n<T>(1,2)*z[62] - 10*z[51];
    z[21]=z[13]*z[21];
    z[24]=static_cast<T>(4)- z[36];
    z[24]=z[24]*z[23];
    z[24]=z[24] - z[32] + z[73];
    z[24]=z[5]*z[24];
    z[27]=static_cast<T>(3)- 11*z[40];
    z[37]=15*z[12] - z[76];
    z[37]=z[7]*z[37];
    z[24]=z[24] - n<T>(5,2)*z[99] - z[131] + n<T>(1,2)*z[27] + z[37];
    z[24]=z[5]*z[24];
    z[27]= - static_cast<T>(17)+ 8*z[87];
    z[27]=z[27]*z[117];
    z[37]= - static_cast<T>(4)+ 7*z[40];
    z[37]=z[3]*z[37];
    z[43]=z[14]*z[54];
    z[24]=z[24] + 4*z[43] + 11*z[9] - z[46] - n<T>(9,2)*z[2] + z[27] + 
    z[136] + z[37];
    z[24]=z[5]*z[24];
    z[27]= - static_cast<T>(2)- z[19];
    z[27]=z[27]*z[52];
    z[37]=z[70] + 12*z[16] + 5*z[15];
    z[43]= - static_cast<T>(27)- z[125];
    z[43]=z[3]*z[43];
    z[50]=n<T>(127,2) - z[106];
    z[50]=z[2]*z[50];
    z[27]=z[27] + n<T>(79,2)*z[6] + z[50] - 15*z[7] + 2*z[37] + z[43];
    z[27]=z[9]*z[27];
    z[37]= - z[88] + z[46];
    z[37]=z[9]*z[37];
    z[35]=z[37] - z[35] - z[42];
    z[35]=z[9]*z[35];
    z[35]=3*z[51] + z[35];
    z[35]=z[35]*z[23];
    z[37]= - static_cast<T>(12)- z[17];
    z[37]=z[3]*z[37];
    z[37]= - z[74] + z[37];
    z[37]=z[3]*z[37];
    z[42]=z[7] - z[82] + 8*z[16] - z[104];
    z[42]=z[7]*z[42];
    z[43]=z[91] - z[78] + z[7];
    z[43]=z[2]*z[43];
    z[50]=static_cast<T>(2)+ z[68];
    z[50]=z[3]*z[50];
    z[50]=z[50] - n<T>(141,2)*z[2];
    z[50]=z[6]*z[50];
    z[21]=z[21] + z[24] + z[35] + z[27] + z[50] + z[43] + z[42] - z[109]
    + z[37];
    z[21]=z[13]*z[21];
    z[24]= - z[7]*z[80];
    z[24]= - z[72] + z[24];
    z[27]= - z[14]*z[98];
    z[24]=3*z[24] + z[27];
    z[24]=z[24]*z[85];
    z[27]= - z[115] + 19*z[7];
    z[24]=z[24] + n<T>(1,2)*z[27] - z[2];
    z[24]=z[14]*z[24];
    z[17]=z[24] - z[39] + z[133] - z[83] + n<T>(25,2) - z[17];
    z[17]=z[14]*z[17];
    z[24]=z[58]*z[104];
    z[24]=z[24] - 5*z[47] + z[34];
    z[24]=z[24]*z[79];
    z[27]=7*z[10] - z[63];
    z[27]=z[12]*z[27];
    z[24]=z[24] + n<T>(15,2)*z[48] - n<T>(9,2)*z[33] + z[27];
    z[24]=z[3]*z[24];
    z[27]= - z[33] + z[61];
    z[27]=z[27]*z[40];
    z[34]=z[10] + z[12];
    z[34]= - z[14] + 2*z[34] - z[94];
    z[34]=z[14]*z[34];
    z[35]= - 4*z[10] - z[12];
    z[35]=z[12]*z[35];
    z[27]=z[34] + z[27] + 2*z[33] + z[35];
    z[27]=z[8]*z[27];
    z[34]= - z[33]*z[66];
    z[17]=z[27] + z[17] + z[34] + z[24] + 6*z[41] - n<T>(19,2)*z[10] - 
    z[129];
    z[17]=z[8]*z[17];
    z[24]=n<T>(13,2)*z[4];
    z[27]=z[30] + z[92] - z[24];
    z[27]=z[7]*z[27];
    z[30]=z[80] - z[57];
    z[30]=z[14]*z[30]*z[26];
    z[27]= - z[30] + z[38] + z[27] - n<T>(45,2)*z[25];
    z[27]=z[14]*z[27];
    z[34]= - z[16] + z[15];
    z[35]=static_cast<T>(39)- 7*z[87];
    z[35]=z[7]*z[35];
    z[37]= - static_cast<T>(39)- z[106];
    z[37]=z[2]*z[37];
    z[27]=z[27] - 6*z[6] + z[37] + z[35] + 2*z[34] + 21*z[4];
    z[27]=z[14]*z[27];
    z[31]=z[47]*z[31];
    z[34]=11*z[12];
    z[35]= - z[10] - z[34];
    z[35]=z[12]*z[35];
    z[31]=z[31] + z[35] - 3*z[33];
    z[31]=z[3]*z[31];
    z[31]=z[31] + n<T>(11,2)*z[41] - 3*z[10] + z[34];
    z[31]=z[3]*z[31];
    z[34]=static_cast<T>(6)- z[138];
    z[34]=z[34]*z[127];
    z[34]=z[34] + static_cast<T>(4)- z[138];
    z[35]=z[63] + 25*z[49];
    z[35]=z[6]*z[35];
    z[37]=z[3]*z[33];
    z[37]=z[37] - z[94];
    z[37]=z[37]*z[52];
    z[17]=z[17] + z[27] + z[37] + z[35] + 14*z[19] - n<T>(47,2)*z[87] + 2*
    z[34] + z[31];
    z[17]=z[8]*z[17];
    z[27]=static_cast<T>(7)- 6*z[99];
    z[27]=z[14]*z[27];
    z[27]=z[32] + z[27];
    z[27]=z[14]*z[27];
    z[27]= - 4*z[45] + z[27];
    z[27]=z[8]*z[27];
    z[31]=static_cast<T>(5)- z[36];
    z[31]=z[14]*z[31];
    z[31]= - z[12] + z[31];
    z[31]=z[14]*z[31];
    z[31]= - z[45] + z[31];
    z[31]=z[5]*z[31];
    z[32]=23*z[12];
    z[34]=static_cast<T>(32)- 15*z[99];
    z[34]=z[14]*z[34];
    z[27]=z[31] + z[27] + z[34] - z[29] - z[32] + z[75];
    z[27]=z[5]*z[27];
    z[29]= - z[14]*z[66];
    z[29]=static_cast<T>(7)+ z[29];
    z[29]=z[14]*z[29];
    z[29]= - z[56] + z[29];
    z[29]=z[8]*z[29];
    z[29]=z[29] - static_cast<T>(100)+ 19*z[99];
    z[29]=z[14]*z[29];
    z[31]=z[45]*z[46];
    z[31]=z[32] + z[31];
    z[29]=4*z[31] + z[29];
    z[29]=z[8]*z[29];
    z[31]= - 33*z[12] + z[73];
    z[31]=z[31]*z[117];
    z[32]=z[54]*z[23];
    z[32]=z[2] + z[32];
    z[32]=z[32]*z[23];
    z[34]=n<T>(11,2)*z[12] - 9*z[73];
    z[34]=z[6]*z[34];
    z[27]=z[27] + z[29] + z[32] + z[34] + z[31] + n<T>(113,2) + 25*z[40];
    z[27]=z[5]*z[27];
    z[24]=z[66] - 12*z[7] + z[122] - z[24];
    z[24]=z[2]*z[24];
    z[29]= - static_cast<T>(15)+ z[87];
    z[29]=z[7]*z[29];
    z[29]=n<T>(75,2)*z[2] + z[29] + z[120] - z[115];
    z[29]=z[6]*z[29];
    z[31]=z[92] + z[115];
    z[32]= - z[7] + z[31];
    z[32]=z[7]*z[32];
    z[25]=z[30] + z[32] - n<T>(11,2)*z[25];
    z[25]=z[6]*z[25];
    z[30]= - z[111]*z[86];
    z[26]= - z[26] + z[30];
    z[26]=z[9]*z[26];
    z[25]=z[26] + z[25];
    z[25]=z[14]*z[25];
    z[26]= - z[7] - z[128];
    z[26]=z[9]*z[26];
    z[28]= - 4*z[28] + n<T>(69,2)*z[7];
    z[28]=z[7]*z[28];
    z[24]=z[25] + z[26] + z[29] + z[28] + z[24];
    z[24]=z[14]*z[24];
    z[25]=z[44] - z[77];
    z[25]=z[3]*z[25];
    z[26]= - z[76] + n<T>(49,2)*z[12] - z[67];
    z[26]=z[7]*z[26];
    z[25]=z[26] + n<T>(27,2) + z[25];
    z[25]=z[6]*z[25];
    z[26]= - z[16]*z[53];
    z[19]= - 5*z[19] - z[96] - static_cast<T>(33)+ z[26];
    z[19]=z[9]*z[19];
    z[26]=n<T>(9,2) + z[139];
    z[26]=z[26]*z[115];
    z[28]= - z[33]*z[70];
    z[29]=z[10] - 37*z[12];
    z[28]=n<T>(1,2)*z[29] + z[28];
    z[28]=z[3]*z[28];
    z[18]=z[28] - static_cast<T>(13)+ z[18];
    z[18]=z[3]*z[18];
    z[28]= - 17*z[87] + static_cast<T>(33)- 40*z[40];
    z[28]=z[28]*z[117];
    z[29]=5*z[16] + z[122];
    z[30]=n<T>(77,2) - z[106];
    z[30]=z[2]*z[30];
    z[17]=z[20] + z[21] + z[27] + z[17] + z[24] + z[19] + z[25] + z[30]
    + z[28] + z[18] + 4*z[29] + z[26];
    z[17]=z[1]*z[17];
    z[18]= - z[106] + 3*z[22];
    z[19]=z[18] + 2;
    z[20]=2*z[11];
    z[21]=z[19]*z[20];
    z[24]= - z[66]*z[100];
    z[25]= - 6*z[16] - z[15];
    z[21]=z[24] + z[21] + z[121] + z[80] + z[103] + 2*z[25] - 9*z[4];
    z[21]=z[14]*z[21];
    z[24]= - z[16] + z[80];
    z[24]=z[6]*z[24];
    z[25]= - z[16] + z[2];
    z[25]=z[5]*z[25];
    z[24]=z[25] + z[24] - z[110];
    z[24]=z[100]*z[24];
    z[19]= - z[9]*z[19];
    z[25]=z[120] + z[2];
    z[19]=z[19] + z[25];
    z[19]=z[11]*z[19];
    z[19]=z[19] + z[24];
    z[24]=z[101] - z[72];
    z[26]=n<T>(1,2)*z[24] - 4*z[112];
    z[26]=z[9]*z[26];
    z[27]=z[9]*z[93];
    z[24]=z[27] - z[24];
    z[24]=z[24]*z[119];
    z[24]=z[26] + z[24];
    z[24]=z[13]*z[11]*z[24];
    z[26]=z[46] + z[25];
    z[26]=z[9]*z[26];
    z[26]= - z[60] + z[26];
    z[26]=z[26]*z[20];
    z[24]=z[26] + z[24];
    z[24]=z[13]*z[24];
    z[19]=2*z[19] + z[24];
    z[19]=z[13]*z[19];
    z[24]=z[16]*z[33];
    z[24]=z[24] - z[94];
    z[24]=z[9]*z[24];
    z[18]=z[24] + static_cast<T>(1)- z[18];
    z[18]=z[18]*z[20];
    z[24]=z[88] + z[31];
    z[26]=z[16] - z[66];
    z[26]=z[6]*z[26]*z[100];
    z[25]= - z[11]*z[25];
    z[25]=z[25] + z[26];
    z[25]=z[25]*z[23];
    z[26]= - z[16]*z[100];
    z[26]=z[11] + z[26];
    z[26]=z[26]*z[23];
    z[26]=n<T>(13,2) + z[26];
    z[26]=z[5]*z[26];
    z[18]=z[19] + z[26] + z[25] + z[18] - 32*z[9] - 7*z[6] + z[84] - n<T>(33,2)*z[7] + 4*z[24] - z[3];
    z[18]=z[13]*z[18];
    z[19]= - static_cast<T>(2)- z[22];
    z[19]=z[10]*z[19];
    z[19]=z[19] + z[94];
    z[19]=z[19]*z[20];
    z[24]= - z[22] + z[106];
    z[24]=z[24]*z[100];
    z[19]=2*z[24] + n<T>(27,2) + z[19];
    z[19]=z[14]*z[19];
    z[24]=z[11]*z[61];
    z[19]=z[19] + z[24] + 11*z[10] - n<T>(47,2)*z[12];
    z[19]=z[8]*z[19];
    z[24]= - z[16] - z[15];
    z[24]=z[10]*z[24];
    z[24]=z[39] + z[24];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[10]*z[22];
    z[22]= - z[94] + z[22] - z[12];
    z[22]=z[22]*z[20];
    z[25]= - z[2]*z[100];
    z[25]=z[20] + z[25];
    z[23]=z[25]*z[23];
    z[20]= - z[12]*z[20];
    z[20]=z[23] - n<T>(155,2) + z[20];
    z[20]=z[14]*z[20];
    z[20]=116*z[12] + z[20];
    z[20]=z[5]*z[20];
    z[23]=z[3]*z[55];

    r += n<T>(13,2) + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + 4*
      z[23] + 6*z[24] - z[81] - n<T>(65,2)*z[87] - z[127];
 
    return r;
}

template double qg_2lNLC_r527(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r527(const std::array<dd_real,31>&);
#endif
