#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r60(const std::array<T,31>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[12];
    z[9]=k[13];
    z[10]=k[8];
    z[11]=k[14];
    z[12]=k[9];
    z[13]=k[2];
    z[14]=k[3];
    z[15]=n<T>(3,2)*z[9];
    z[16]= - z[9] + z[8] + n<T>(1,2)*z[10];
    z[16]=z[16]*z[15];
    z[17]=z[8]*z[12];
    z[18]=n<T>(1,2)*z[8];
    z[19]=z[18] - z[12];
    z[20]=z[10]*z[19];
    z[16]=z[16] - z[17] + z[20];
    z[16]=z[9]*z[16];
    z[20]=z[8]*z[7];
    z[21]=z[20] + 1;
    z[22]=n<T>(1,2)*z[9];
    z[23]= - z[21]*z[22];
    z[23]=z[23] + z[8] + z[10];
    z[23]=z[9]*z[23];
    z[24]=z[10]*z[8];
    z[25]=n<T>(1,4)*z[10];
    z[26]= - z[4]*z[25];
    z[23]=z[26] - z[24] + z[23];
    z[23]=z[4]*z[23];
    z[16]=z[16] + 3*z[23];
    z[23]=z[4]*z[2];
    z[26]= - z[10]*z[13];
    z[26]= - n<T>(11,2)*z[23] + n<T>(9,2) + z[26];
    z[26]=z[5]*z[26];
    z[27]=z[4]*npow(z[2],2);
    z[28]=z[27] - z[2];
    z[29]=z[5]*z[28];
    z[29]=z[29] - n<T>(1,2) + z[23];
    z[29]=z[6]*z[29];
    z[26]=z[26] + 3*z[29];
    z[26]=z[6]*z[26];
    z[29]=z[24] + n<T>(1,2)*z[17];
    z[30]=z[4]*z[11];
    z[31]=n<T>(1,4)*z[12];
    z[32]= - n<T>(3,8)*z[10] + n<T>(1,8)*z[8] - z[11] - z[31] + 2*z[4];
    z[32]=z[5]*z[32];
    z[26]=n<T>(1,8)*z[26] + z[32] - n<T>(1,2)*z[29] - 2*z[30];
    z[26]=z[6]*z[26];
    z[32]=z[9]*z[20];
    z[32]= - z[8] + z[32];
    z[32]=z[32]*z[22];
    z[24]=z[24] + z[32];
    z[24]=z[4]*z[9]*z[24];
    z[18]= - npow(z[9],3)*z[18];
    z[18]=z[18] + z[24];
    z[24]= - z[12] - z[8];
    z[24]=z[10]*z[24];
    z[17]= - z[17] + z[24];
    z[17]=z[5]*z[9]*z[17];
    z[17]=3*z[18] + z[17];
    z[18]= - n<T>(1,4)*z[29] - z[30];
    z[18]=z[5]*z[18];
    z[24]=n<T>(3,8)*z[6];
    z[29]= - z[5]*z[23];
    z[29]= - z[4] + z[29];
    z[29]=z[29]*z[24];
    z[32]=z[5]*z[4];
    z[29]=z[32] + z[29];
    z[29]=z[6]*z[29];
    z[18]=z[18] + n<T>(1,2)*z[29];
    z[18]=z[6]*z[18];
    z[17]=n<T>(1,8)*z[17] + z[18];
    z[17]=z[1]*z[17];
    z[18]=z[19]*z[25];
    z[18]=z[18] - z[30];
    z[18]=z[5]*z[18];
    z[16]=z[17] + z[26] + n<T>(1,4)*z[16] + z[18];
    z[16]=z[1]*z[16];
    z[17]=z[12]*z[7];
    z[18]= - n<T>(1,4)*z[20] + n<T>(3,2) - z[17];
    z[18]=z[10]*z[18];
    z[19]=z[10]*z[2];
    z[26]= - z[19] - 1;
    z[26]=z[7]*z[26];
    z[26]=z[2] + z[14] + z[26];
    z[22]=z[26]*z[22];
    z[22]=z[22] - n<T>(1,2) - z[19];
    z[15]=z[22]*z[15];
    z[17]= - n<T>(3,4) - z[17];
    z[17]=z[8]*z[17];
    z[15]=z[15] + z[17] + z[18];
    z[15]=z[9]*z[15];
    z[17]= - z[12] + n<T>(3,4)*z[8];
    z[17]=z[10]*z[17];
    z[15]=z[17] + z[15];
    z[17]= - n<T>(3,2) - z[20];
    z[17]=n<T>(3,8)*z[23] + n<T>(3,4)*z[17];
    z[17]=z[10]*z[17];
    z[18]= - z[19] + z[21];
    z[18]=z[9]*z[18];
    z[17]=n<T>(3,4)*z[18] - z[11] + z[17];
    z[17]=z[4]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[17]= - z[2] + n<T>(1,2)*z[13];
    z[18]=n<T>(1,2)*z[27] + z[17];
    z[18]=z[5]*z[18];
    z[17]= - z[2]*z[17];
    z[19]=n<T>(1,2)*z[4];
    z[20]= - npow(z[2],3)*z[19];
    z[17]=z[17] + z[20];
    z[17]=z[5]*z[17];
    z[17]= - n<T>(1,2)*z[28] + z[17];
    z[17]=z[6]*z[17];
    z[17]=z[18] + z[17];
    z[17]=z[17]*z[24];
    z[18]=z[31] + z[25];
    z[20]= - z[21]*z[18];
    z[18]= - n<T>(1,2)*z[11] - z[18];
    z[18]=z[13]*z[18];
    z[18]= - z[23] + static_cast<T>(1)+ z[18];
    z[18]=z[5]*z[18];
    z[21]= - z[7]*z[11];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[4]*z[21];
    z[17]=z[17] + z[18] + z[21] - z[11] + z[20];
    z[17]=z[6]*z[17];
    z[18]=z[2]*z[11];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[19];
    z[19]= - z[12]*z[13]*z[25];
    z[18]=z[18] - z[11] + z[19];
    z[18]=z[5]*z[18];
    z[15]=z[16] + z[17] + n<T>(1,2)*z[15] + z[18];

    r += z[15]*npow(z[3],2)*npow(z[1],2);
 
    return r;
}

template double qg_2lNLC_r60(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r60(const std::array<dd_real,31>&);
#endif
