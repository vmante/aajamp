#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1301(const std::array<T,31>& k) {
  T z[113];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[5];
    z[4]=k[6];
    z[5]=k[14];
    z[6]=k[15];
    z[7]=k[30];
    z[8]=k[4];
    z[9]=k[7];
    z[10]=k[10];
    z[11]=k[29];
    z[12]=k[8];
    z[13]=k[26];
    z[14]=k[12];
    z[15]=k[9];
    z[16]=k[3];
    z[17]=k[13];
    z[18]=k[23];
    z[19]=k[20];
    z[20]=npow(z[3],2);
    z[21]=npow(z[1],2);
    z[22]=z[20]*z[21];
    z[23]=z[3]*z[1];
    z[24]=npow(z[23],3);
    z[25]=z[24]*z[14];
    z[26]=z[9]*z[24];
    z[26]= - z[25] + 2*z[26];
    z[26]=z[4]*z[26];
    z[26]=41*z[22] + 3*z[26];
    z[27]=npow(z[4],2);
    z[26]=z[26]*z[27];
    z[28]=3*z[23];
    z[29]=2*z[21];
    z[30]=z[28] + z[29];
    z[31]= - z[3]*z[30];
    z[32]=npow(z[1],3);
    z[33]=2*z[32];
    z[31]= - z[33] + z[31];
    z[34]=3*z[14];
    z[31]=z[31]*z[34];
    z[35]=z[23] + z[21];
    z[36]=3*z[3];
    z[37]=z[35]*z[36];
    z[37]=z[32] + z[37];
    z[37]=z[9]*z[37];
    z[38]=npow(z[4],3);
    z[39]=z[38]*z[24];
    z[40]=z[20]*z[1];
    z[41]= - 13*z[40] - 15*z[39];
    z[41]=z[5]*z[41];
    z[26]=z[41] + z[26] + 6*z[37] + 22*z[23] + z[31];
    z[26]=z[5]*z[26];
    z[31]=z[22]*z[14];
    z[37]=z[21]*z[8];
    z[41]=z[14]*z[3];
    z[42]= - z[41]*z[37];
    z[42]=2*z[31] + z[42];
    z[43]=z[21]*z[3];
    z[44]=n<T>(23,3)*z[43] - 9*z[37];
    z[44]=z[8]*z[44];
    z[44]= - 6*z[22] + z[44];
    z[44]=z[9]*z[44];
    z[42]=3*z[42] + z[44];
    z[42]=z[42]*z[27];
    z[44]=npow(z[8],2);
    z[45]=z[4]*z[9];
    z[46]=z[32]*z[44]*z[45];
    z[47]=z[3] + z[8];
    z[47]=z[47]*z[46];
    z[48]=z[29]*z[8];
    z[49]= - z[43] - z[48];
    z[49]=z[8]*z[49];
    z[47]=z[49] + z[47];
    z[47]=z[47]*z[27];
    z[49]=z[8]*z[1];
    z[50]=2*z[49];
    z[51]=z[50] + z[23];
    z[50]=z[50] - z[21];
    z[52]=2*z[23];
    z[53]= - z[52] - z[50];
    z[53]=z[8]*z[53];
    z[53]=z[43] + z[53];
    z[53]=z[9]*z[53];
    z[47]=z[47] + z[53] + z[51];
    z[53]=n<T>(4,3)*z[6];
    z[47]=z[47]*z[53];
    z[54]=n<T>(7,3)*z[21];
    z[55]=z[54] - z[28];
    z[55]=z[3]*z[55];
    z[55]=n<T>(22,3)*z[32] + z[55];
    z[55]=z[14]*z[55];
    z[56]=4*z[23];
    z[55]=z[55] + n<T>(41,3)*z[21] + z[56];
    z[55]=z[14]*z[55];
    z[57]=21*z[49] - 18*z[21] - n<T>(53,3)*z[23];
    z[57]=z[9]*z[57];
    z[58]=z[43] + z[32];
    z[59]=z[58]*z[3];
    z[60]=z[20]*z[14];
    z[61]=z[32]*z[60];
    z[61]=z[59] + z[61];
    z[61]=z[14]*z[61];
    z[62]=z[32]*z[15];
    z[63]=z[60]*z[62];
    z[61]=z[61] + z[63];
    z[61]=z[15]*z[61];
    z[63]=3*z[21];
    z[64]=z[63] - z[23];
    z[64]=z[64]*z[41];
    z[65]=6*z[23];
    z[61]=z[61] + z[65] + z[64];
    z[61]=z[15]*z[61];
    z[64]= - n<T>(11,3)*z[21] - z[65];
    z[64]=z[14]*z[64];
    z[64]= - n<T>(14,3)*z[1] + z[64];
    z[64]=z[14]*z[64];
    z[64]=static_cast<T>(6)+ z[64];
    z[64]=z[8]*z[64];
    z[66]= - z[1] - n<T>(46,3)*z[3];
    z[26]=z[47] + z[26] + z[61] + z[42] + z[57] + z[64] + 2*z[66] + 
    z[55];
    z[26]=z[6]*z[26];
    z[42]=z[34]*z[21];
    z[47]=npow(z[3],3);
    z[55]=z[47]*z[42];
    z[55]=z[22] + z[55];
    z[57]=z[9]*z[14];
    z[55]=z[55]*z[57];
    z[61]=z[37] - z[43];
    z[58]= - z[58]*z[41];
    z[58]=z[58] + z[61];
    z[64]=4*z[5];
    z[58]=z[58]*z[64];
    z[66]=2*z[43];
    z[67]=z[66] - z[31];
    z[67]=z[67]*z[34];
    z[68]=z[41]*z[21];
    z[69]=z[52] + z[68];
    z[70]=z[34]*z[8];
    z[69]=z[69]*z[70];
    z[71]=z[43]*z[8];
    z[72]=2*z[22] - z[71];
    z[72]=z[72]*z[27];
    z[73]=z[22]*z[9];
    z[74]=z[43] - z[73];
    z[75]=2*z[15];
    z[74]=z[74]*z[75];
    z[55]=z[58] + z[74] + z[72] + z[55] + z[69] + z[56] + z[67];
    z[58]=2*z[17];
    z[55]=z[55]*z[58];
    z[67]=z[22]*z[27];
    z[69]=3*z[67];
    z[72]=2*z[1];
    z[74]=z[72]*z[20];
    z[76]=z[9]*z[74];
    z[77]=z[22]*z[75];
    z[77]=z[40] + z[77];
    z[77]=z[77]*z[75];
    z[78]= - z[22]*z[64];
    z[78]=z[40] + z[78];
    z[79]=2*z[5];
    z[78]=z[78]*z[79];
    z[76]=z[78] + z[77] + z[76] - z[69];
    z[76]=z[6]*z[76];
    z[77]=z[67] - z[21];
    z[78]= - z[15]*z[74];
    z[43]=z[43]*z[9];
    z[78]=z[78] - z[43] + z[56] - z[77];
    z[78]=z[78]*z[75];
    z[80]=z[49] + z[29] - z[23];
    z[81]=3*z[41];
    z[82]=z[81]*z[35];
    z[61]=z[61]*z[64];
    z[61]=z[61] + z[82] - 4*z[80];
    z[61]=z[61]*z[79];
    z[64]=z[60]*z[1];
    z[80]=4*z[21];
    z[83]=9*z[64] + z[80] + z[23];
    z[83]=z[14]*z[83];
    z[84]=z[14]*z[35];
    z[84]=z[1] + z[84];
    z[84]=z[84]*z[34];
    z[84]=static_cast<T>(4)+ z[84];
    z[84]=z[8]*z[84];
    z[85]=z[21] - z[28];
    z[86]=2*z[3];
    z[85]=z[85]*z[86];
    z[87]=z[14]*z[1]*z[47];
    z[85]=z[85] - 9*z[87];
    z[85]=z[14]*z[85];
    z[87]=7*z[23];
    z[85]= - z[87] + z[85];
    z[85]=z[9]*z[85];
    z[88]=z[72] - z[3];
    z[55]=z[55] + z[76] + z[61] + z[78] + z[85] + z[84] + 4*z[88] + 
    z[83];
    z[55]=z[17]*z[55];
    z[61]=z[23] - z[21];
    z[76]=z[32]*z[5];
    z[78]=z[76] + z[61];
    z[83]=z[4]*z[1];
    z[84]=npow(z[83],3);
    z[85]=z[84]*z[20];
    z[88]=3*z[85];
    z[78]=z[88] - 8*z[78];
    z[89]=z[17]*z[8];
    z[78]=z[78]*z[89];
    z[90]=z[27]*z[21];
    z[91]=z[90]*z[3];
    z[92]=z[8]*z[91];
    z[93]=4*z[32];
    z[94]=z[93] - z[37];
    z[94]=z[94]*z[79];
    z[51]=z[78] + z[94] - 5*z[92] - 8*z[21] + z[51];
    z[51]=z[17]*z[51];
    z[78]=z[49] - z[21];
    z[56]= - z[56] - z[78];
    z[92]= - z[22] + z[71];
    z[92]=z[92]*z[27];
    z[56]=8*z[92] + 4*z[56];
    z[56]=z[9]*z[56];
    z[92]=z[32] - z[66];
    z[92]=z[15]*z[92];
    z[92]=z[92] - z[29] + z[28];
    z[92]=z[92]*z[75];
    z[94]=z[37] - z[32];
    z[95]=z[9]*z[94];
    z[96]=3*z[32];
    z[97]= - npow(z[1],4)*z[75];
    z[97]=z[96] + z[97];
    z[97]=z[97]*z[75];
    z[95]=z[97] - z[63] + 4*z[95];
    z[95]=z[5]*z[95];
    z[97]=6*z[7];
    z[98]=z[32]*z[97];
    z[99]=z[96]*z[5];
    z[100]= - z[15]*z[93];
    z[98]=z[98] + z[100] - z[99];
    z[98]=z[7]*z[98];
    z[51]=z[98] + z[51] + z[95] + z[92] + z[1] - z[86] + z[56];
    z[51]=z[12]*z[51];
    z[56]= - z[23]*z[34];
    z[56]=z[91] - z[72] + z[56];
    z[56]=z[56]*z[97];
    z[92]=z[15]*z[1];
    z[95]= - 2*z[90] + z[92];
    z[95]=z[15]*z[95];
    z[98]=z[90] + z[92];
    z[98]=z[7]*z[98];
    z[95]=z[95] + 3*z[98];
    z[98]=2*z[2];
    z[95]=z[95]*z[98];
    z[100]= - z[72]*z[60];
    z[100]=z[23] + z[100];
    z[100]=z[14]*z[100];
    z[100]= - z[72] + z[100];
    z[100]=z[100]*z[34];
    z[101]= - z[90]*z[81];
    z[56]=z[95] + z[56] - z[92] + z[101] - static_cast<T>(5)+ z[100];
    z[56]=z[7]*z[56];
    z[95]=npow(z[14],2);
    z[100]=z[4]*z[32]*z[20]*z[95];
    z[68]=z[68] + z[100];
    z[68]=z[68]*z[27];
    z[100]= - z[92] - z[84];
    z[100]=z[60]*z[100];
    z[101]= - z[95]*z[40];
    z[102]=4*z[1];
    z[100]=z[102] + z[101] + z[100];
    z[100]=z[15]*z[100];
    z[101]=z[14]*z[23];
    z[101]=z[72] + z[101];
    z[101]=z[101]*z[34];
    z[68]=z[100] + z[101] + z[68];
    z[68]=z[15]*z[68];
    z[100]=z[52] + z[21];
    z[101]=z[100]*z[75];
    z[103]=3*z[1];
    z[101]=z[101] - z[103] - z[91];
    z[101]=z[15]*z[101];
    z[104]= - z[52] - z[85];
    z[104]=z[15]*z[104];
    z[104]= - z[91] + z[104];
    z[104]=z[104]*z[58];
    z[101]=z[104] + static_cast<T>(2)+ z[101];
    z[58]=z[101]*z[58];
    z[46]=z[48] - z[46];
    z[46]=z[46]*z[27];
    z[48]=z[90]*z[2];
    z[101]=z[9]*z[49];
    z[46]= - z[48] + z[46] - z[1] + z[101];
    z[101]=n<T>(2,3)*z[10];
    z[46]=z[46]*z[101];
    z[104]=z[90]*z[8];
    z[105]=z[104] - z[1];
    z[106]=7*z[9];
    z[105]=z[106]*z[105];
    z[105]= - static_cast<T>(13)+ z[105];
    z[48]=z[75]*z[48];
    z[72]=z[15]*z[72];
    z[46]=z[46] + z[48] + n<T>(1,3)*z[105] + z[72];
    z[48]=2*z[10];
    z[46]=z[46]*z[48];
    z[72]=z[60]*z[103];
    z[72]= - n<T>(17,3)*z[23] + z[72];
    z[72]=z[14]*z[72];
    z[72]=n<T>(5,3)*z[1] + z[72];
    z[105]=2*z[14];
    z[72]=z[72]*z[105];
    z[107]=z[95]*z[85];
    z[72]= - n<T>(19,3)*z[107] + n<T>(19,3) + z[72];
    z[72]=z[9]*z[72];
    z[107]= - z[1]*npow(z[15],2);
    z[108]=z[17]*z[15]*z[3]*z[84];
    z[107]=z[107] + z[108];
    z[107]=z[16]*z[17]*z[107];
    z[46]=4*z[107] + z[46] + z[58] + z[68] + z[56] + z[72];
    z[46]=z[16]*z[46];
    z[56]=z[96]*z[8];
    z[58]=z[56]*z[20];
    z[68]=7*z[45];
    z[72]=z[68]*npow(z[23],4);
    z[58]= - 7*z[24] + z[58] + z[72];
    z[107]= - z[58]*z[38];
    z[108]=5*z[23];
    z[109]=z[80] + z[108];
    z[109]=z[109]*z[36];
    z[93]=z[109] + z[93];
    z[109]= - z[3]*z[9];
    z[109]=z[109] + 1;
    z[109]=z[93]*z[109];
    z[110]=z[8]*z[30];
    z[107]=z[107] - 3*z[110] + z[109];
    z[107]=z[12]*z[107];
    z[56]= - z[60]*z[56];
    z[72]= - z[14]*z[72];
    z[56]=z[72] + 7*z[25] + z[56];
    z[56]=z[56]*z[38];
    z[72]=z[93]*z[14];
    z[109]= - z[30]*z[70];
    z[93]=z[93]*z[41];
    z[110]= - z[9]*z[93];
    z[56]=z[107] + z[56] + z[110] + z[72] + z[109];
    z[56]=z[13]*z[56];
    z[58]= - z[95]*z[58];
    z[25]=z[9]*z[25];
    z[25]= - 15*z[25] + z[58];
    z[25]=z[4]*z[25];
    z[58]= - z[37]*z[81];
    z[25]=z[25] + 8*z[31] + z[58];
    z[25]=z[25]*z[27];
    z[58]=z[24]*z[45];
    z[81]= - z[36]*z[37];
    z[81]= - 15*z[58] + 8*z[22] + z[81];
    z[81]=z[81]*z[27];
    z[107]=z[63] + 14*z[23];
    z[109]=z[100]*z[3];
    z[110]=12*z[109];
    z[111]= - z[9]*z[110];
    z[112]=3*z[49];
    z[81]=z[81] + z[111] - z[112] + z[107];
    z[81]=z[12]*z[81];
    z[72]=z[72] + z[107];
    z[72]=z[14]*z[72];
    z[93]= - z[110] - z[93];
    z[57]=z[93]*z[57];
    z[30]= - z[14]*z[30];
    z[30]= - z[1] + z[30];
    z[30]=z[30]*z[70];
    z[25]=z[56] + z[81] + z[25] + z[57] + z[72] + z[30];
    z[25]=z[13]*z[25];
    z[30]=z[35]*z[60];
    z[35]= - z[3]*z[35];
    z[35]= - z[30] + z[32] + z[35];
    z[35]=z[35]*z[75];
    z[35]=z[35] - z[43] - z[82] - z[63] - z[108];
    z[35]=z[35]*z[75];
    z[43]=6*z[21];
    z[56]= - z[43] - z[108];
    z[56]=z[3]*z[56];
    z[33]= - z[33] + z[56];
    z[33]=z[33]*z[34];
    z[33]=z[112] + z[33] + z[80] + z[87];
    z[33]=z[9]*z[33];
    z[56]= - z[34]*z[58];
    z[56]= - z[31] + z[56];
    z[56]=z[56]*z[27];
    z[57]=z[74] + z[73];
    z[57]=z[57]*z[75];
    z[37]= - z[66] - z[37];
    z[37]=z[9]*z[37];
    z[37]=z[57] + z[65] + z[37];
    z[37]=z[37]*z[79];
    z[57]= - z[21] - z[28];
    z[57]=z[14]*z[57];
    z[60]=8*z[1];
    z[33]=z[37] + z[35] + 2*z[56] + z[33] + z[57] + z[60] + 7*z[3];
    z[33]=z[5]*z[33];
    z[35]=npow(z[83],4)*z[47]*z[5];
    z[37]=z[35] - z[88] + z[61];
    z[37]=z[5]*z[37];
    z[47]=z[21]*z[15];
    z[56]= - z[47] - z[1] + z[91];
    z[37]=2*z[56] + z[37];
    z[37]=z[37]*z[97];
    z[35]=12*z[35] + 3*z[61];
    z[56]= - 32*z[85] + z[35];
    z[56]=z[5]*z[56];
    z[57]= - z[1] + 12*z[91];
    z[56]=2*z[57] + z[56];
    z[56]=z[5]*z[56];
    z[57]= - z[15]*z[80];
    z[57]=5*z[1] + z[57];
    z[57]=z[15]*z[57];
    z[37]=z[37] + z[56] - static_cast<T>(4)+ z[57];
    z[37]=z[7]*z[37];
    z[56]=z[75]*z[32];
    z[56]=z[56] - z[21];
    z[57]=z[15]*z[56];
    z[65]= - z[21] + z[99];
    z[65]=z[5]*z[65];
    z[66]=z[62] + z[76];
    z[66]=z[66]*z[97];
    z[57]=z[66] + z[57] + z[65];
    z[57]=z[7]*z[57];
    z[56]= - z[5]*z[56];
    z[65]=z[29]*z[15];
    z[56]=z[56] - z[1] + z[65];
    z[56]=z[15]*z[56];
    z[66]=z[6]*npow(z[5],2)*z[96];
    z[56]=z[57] + 2*z[56] + z[66];
    z[56]=z[12]*z[56];
    z[36]= - z[36]*z[90];
    z[57]= - z[61]*z[75];
    z[36]=z[57] - z[1] + z[36];
    z[36]=z[15]*z[36];
    z[57]=z[5]*z[75]*z[85];
    z[36]=z[36] + z[57];
    z[36]=z[5]*z[36];
    z[36]=z[15] + z[36];
    z[35]= - 34*z[85] + z[35];
    z[35]=z[5]*z[35];
    z[57]= - z[1] + 8*z[91];
    z[35]=3*z[57] + z[35];
    z[35]=z[5]*z[35];
    z[53]=z[104]*z[53];
    z[35]=z[35] + z[53];
    z[35]=z[6]*z[35];
    z[35]=z[56] + z[37] + 2*z[36] + z[35];
    z[35]=z[2]*z[35];
    z[36]=z[44]*z[21];
    z[37]=z[32]*npow(z[8],3);
    z[53]=z[68]*z[37];
    z[36]= - z[53] + 13*z[36];
    z[53]=n<T>(1,3)*z[27];
    z[36]=z[36]*z[53];
    z[53]=z[98]*z[104];
    z[56]=z[78]*z[8];
    z[56]=z[56] + n<T>(1,3)*z[32];
    z[57]=z[106]*z[56];
    z[36]= - z[53] + z[36] + z[57];
    z[53]=z[45]*npow(z[49],4);
    z[53]= - z[53] + 2*z[37];
    z[38]=n<T>(1,3)*z[38];
    z[38]=z[53]*z[38];
    z[53]=z[2]*z[44]*z[84];
    z[57]=z[9]*z[8];
    z[56]=z[56]*z[57];
    z[38]= - n<T>(1,3)*z[53] + z[38] + z[56];
    z[53]=z[32]*z[14];
    z[56]=n<T>(1,3)*z[53];
    z[61]=z[56] + z[49];
    z[61]=z[61]*z[8];
    z[61]=z[61] - z[38];
    z[66]=z[61]*z[48];
    z[68]=z[21] + z[56];
    z[68]=z[14]*z[68];
    z[68]= - z[102] + 5*z[68];
    z[68]=z[8]*z[68];
    z[66]=z[66] - n<T>(7,3)*z[53] + z[68] + z[36];
    z[66]=z[10]*z[66];
    z[68]=n<T>(4,3)*z[53] + z[49];
    z[68]=z[8]*z[68];
    z[38]=z[68] - z[38];
    z[38]=z[6]*z[38];
    z[61]=z[10]*z[61];
    z[38]=z[61] + z[38];
    z[61]=2*z[11];
    z[38]=z[38]*z[61];
    z[68]=z[63] - z[56];
    z[68]=z[14]*z[68];
    z[68]= - z[102] + z[68];
    z[68]=z[8]*z[68];
    z[36]= - z[56] + z[68] + z[36];
    z[36]=z[6]*z[36];
    z[36]=z[38] + z[66] + z[36];
    z[36]=z[36]*z[61];
    z[38]=z[41]*z[63];
    z[56]= - 3*z[40] - z[39];
    z[56]=z[5]*z[56];
    z[38]=z[56] + z[67] + z[38] + z[100];
    z[38]=z[38]*z[97];
    z[56]=5*z[21] + z[28];
    z[41]=z[56]*z[41];
    z[39]= - 11*z[40] - 5*z[39];
    z[39]=z[5]*z[39];
    z[39]=z[39] + z[41] + z[21] + 11*z[23];
    z[24]=z[4]*z[24]*z[34];
    z[24]=35*z[22] + z[24];
    z[24]=z[24]*z[27];
    z[24]=z[24] + 3*z[39];
    z[24]=z[5]*z[24];
    z[39]= - z[43] - z[23];
    z[39]=z[3]*z[39];
    z[39]=z[39] + 2*z[30];
    z[39]=z[14]*z[39];
    z[39]=z[39] - z[108] - z[77];
    z[34]=z[34]*z[39];
    z[39]=z[47] + z[1] - 4*z[3];
    z[24]=z[38] + z[24] + z[34] + 5*z[39];
    z[24]=z[7]*z[24];
    z[34]= - z[44]*z[29];
    z[37]=z[45]*z[37];
    z[34]=z[34] + z[37];
    z[34]=z[34]*z[27];
    z[37]= - z[50]*z[57];
    z[38]=z[1] + z[104];
    z[38]=z[2]*z[38];
    z[34]=z[38] + z[34] + z[37] + z[21] + z[49];
    z[34]=z[34]*z[101];
    z[37]=z[44]*z[90];
    z[37]=z[50] - z[37];
    z[37]=n<T>(7,3)*z[37];
    z[37]=z[9]*z[37];
    z[38]= - static_cast<T>(1)- z[92];
    z[38]=z[38]*z[98];
    z[39]=z[14]*z[21];
    z[34]=z[34] + z[38] - z[65] + n<T>(13,3)*z[8] - z[1] + n<T>(4,3)*z[39] + 
    z[37];
    z[34]=z[34]*z[48];
    z[37]=z[15]*z[22];
    z[37]=z[109] + z[37];
    z[37]=z[15]*z[37];
    z[20]= - z[20]*z[62];
    z[20]= - z[59] + z[20];
    z[20]=z[15]*z[20];
    z[20]= - z[40] + z[20];
    z[20]=z[6]*z[20];
    z[38]=z[15]*z[40];
    z[38]=z[38] - z[52] + z[85];
    z[38]=z[15]*z[38];
    z[38]=z[38] + z[103] - z[91];
    z[38]=z[16]*z[38];
    z[20]=z[38] + z[20] + z[37] + z[23] + z[69];
    z[20]=z[15]*z[20];
    z[37]=3*z[22] - z[71];
    z[37]=z[37]*z[27];
    z[38]=z[85]*z[89];
    z[39]= - z[6]*z[40];
    z[28]=z[38] + z[39] + z[28] + z[37];
    z[28]=z[17]*z[28];
    z[20]=z[28] + z[103] - z[86] + z[20];
    z[20]=z[18]*z[20];
    z[28]= - z[21] + z[52];
    z[28]=z[3]*z[28];
    z[28]=z[28] - z[31];
    z[28]=z[14]*z[28];
    z[37]= - z[15]*z[31];
    z[28]=z[37] - z[29] + z[28];
    z[28]=z[15]*z[28];
    z[29]=z[21] - z[64];
    z[29]=z[14]*z[29];
    z[37]= - z[27]*z[31];
    z[38]=z[9]*z[80];
    z[28]=z[28] + z[37] + z[38] + 6*z[1] + z[29];
    z[28]=z[15]*z[28];
    z[29]=46*z[21] + 41*z[23];
    z[29]=z[3]*z[29];
    z[29]=n<T>(1,3)*z[29] - 6*z[30];
    z[29]=z[14]*z[29];
    z[23]=22*z[21] + 115*z[23];
    z[23]=n<T>(1,3)*z[23] + z[29];
    z[23]=z[14]*z[23];
    z[29]=z[60] + 65*z[3];
    z[23]= - n<T>(31,3)*z[8] + n<T>(1,3)*z[29] + z[23];
    z[23]=z[9]*z[23];
    z[22]=n<T>(22,3)*z[58] - n<T>(16,3)*z[22];
    z[22]=z[95]*z[22];
    z[29]=z[9]*z[31];
    z[22]=29*z[29] + z[22];
    z[22]=z[22]*z[27];
    z[21]= - z[21] - z[53];
    z[21]=z[14]*z[21];
    z[27]= - z[6]*z[32];
    z[27]=z[53] + z[27];
    z[27]=z[11]*z[27];
    z[21]=z[27] + z[21];
    z[21]=z[8]*z[21];
    z[27]=z[6]*z[94];
    z[21]=z[27] + z[53] + z[21];
    z[21]=z[11]*z[21];
    z[21]=z[21] - z[47] + z[1] - z[42];
    z[21]=z[19]*z[21];
    z[27]= - z[54] - z[108];
    z[27]=z[14]*z[27];
    z[29]= - 29*z[1] - 16*z[3];
    z[27]=n<T>(1,3)*z[29] + z[27];
    z[27]=z[27]*z[105];
    z[20]=4*z[21] + z[20] + z[25] + z[46] + z[36] + z[34] + z[35] + 
    z[51] + z[24] + z[55] + z[26] + z[33] + z[28] + z[22] + z[23] + static_cast<T>(7)+ 
    z[27];

    r += 2*z[20];
 
    return r;
}

template double qg_2lNLC_r1301(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1301(const std::array<dd_real,31>&);
#endif
