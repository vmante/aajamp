#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r282(const std::array<T,31>& k) {
  T z[101];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[5];
    z[4]=k[6];
    z[5]=k[14];
    z[6]=k[15];
    z[7]=k[8];
    z[8]=k[10];
    z[9]=k[12];
    z[10]=k[4];
    z[11]=k[7];
    z[12]=k[3];
    z[13]=k[13];
    z[14]=k[21];
    z[15]=k[9];
    z[16]=npow(z[7],2);
    z[17]=z[16]*z[9];
    z[18]=z[17]*z[3];
    z[19]=z[7] - z[9];
    z[20]= - z[7]*z[19];
    z[20]=z[20] - z[18];
    z[20]=z[3]*z[20];
    z[21]=3*z[11];
    z[22]=npow(z[4],2);
    z[23]=z[22]*z[2];
    z[24]=z[9] + z[14] - z[13];
    z[25]= - z[2]*z[16];
    z[26]=2*z[22];
    z[26]= - z[10]*z[26];
    z[20]=z[20] + z[26] - z[21] + 7*z[23] + 2*z[24] + z[25];
    z[20]=z[3]*z[20];
    z[24]=4*z[8];
    z[25]= - z[14]*z[24];
    z[26]=8*z[11];
    z[27]=z[8]*z[26];
    z[25]=z[27] - z[22] + z[25];
    z[25]=z[10]*z[25];
    z[27]=z[13] + z[14];
    z[28]=z[9] - z[27];
    z[25]=z[25] + 5*z[11] + n<T>(8,3)*z[8] + 2*z[28] - 3*z[23];
    z[25]=z[10]*z[25];
    z[28]=npow(z[2],2);
    z[29]=z[28]*z[8];
    z[30]= - n<T>(4,3)*z[2] + z[29];
    z[30]=z[8]*z[30];
    z[30]=n<T>(8,3) + z[30];
    z[31]=npow(z[8],2);
    z[32]=z[31]*z[10];
    z[32]=n<T>(13,3)*z[8] - z[32];
    z[32]=z[10]*z[32];
    z[30]=2*z[30] + z[32];
    z[30]=z[10]*z[30];
    z[32]=z[2]*z[5];
    z[33]= - n<T>(8,3) - z[32];
    z[33]=z[2]*z[33];
    z[30]=z[30] + z[33] - n<T>(8,3)*z[29];
    z[30]=z[6]*z[30];
    z[33]=z[22]*z[28];
    z[34]=4*z[5];
    z[35]=npow(z[5],2);
    z[36]=z[35]*z[2];
    z[37]= - z[34] - z[36];
    z[37]=z[2]*z[37];
    z[38]=3*z[7];
    z[39]=npow(z[2],3);
    z[40]=z[38]*z[39];
    z[41]= - n<T>(16,3)*z[28] - z[40];
    z[41]=z[7]*z[41];
    z[41]=4*z[29] - 8*z[2] + z[41];
    z[41]=z[8]*z[41];
    z[42]=z[28]*z[7];
    z[43]= - z[2] - z[42];
    z[43]=z[7]*z[43];
    z[20]=z[30] + z[20] + z[25] + z[41] + 9*z[33] + z[43] - static_cast<T>(3)+ z[37];
    z[20]=z[6]*z[20];
    z[25]=2*z[2];
    z[30]= - z[7]*z[25];
    z[37]=2*z[14];
    z[41]=z[37] - 17*z[11];
    z[41]=z[10]*z[41];
    z[30]=z[41] - 9*z[32] + z[30];
    z[30]=z[22]*z[30];
    z[41]=n<T>(1,3)*z[7];
    z[43]=n<T>(167,2)*z[9] - 10*z[7];
    z[43]=z[43]*z[41];
    z[44]=5*z[5];
    z[45]= - z[22]*z[44];
    z[45]= - n<T>(4,3)*z[17] + z[45];
    z[45]=z[3]*z[45];
    z[46]=4*z[9];
    z[47]=z[46]*z[15];
    z[48]=npow(z[11],2);
    z[30]=z[45] + 6*z[48] + z[47] + z[43] + z[30];
    z[30]=z[3]*z[30];
    z[43]=2*z[8];
    z[45]= - z[43]*z[42];
    z[49]= - z[5] - z[36];
    z[49]=z[2]*z[49];
    z[50]= - n<T>(59,6)*z[2] - 7*z[42];
    z[50]=z[7]*z[50];
    z[33]=z[45] - n<T>(13,2)*z[33] + z[50] - n<T>(31,2) + z[49];
    z[33]=z[8]*z[33];
    z[45]=6*z[14];
    z[49]=z[22]*z[45];
    z[50]=2*z[31];
    z[51]=7*z[22] - z[50];
    z[51]=z[8]*z[51];
    z[52]=z[43]*z[11];
    z[53]= - 29*z[22] - z[52];
    z[53]=z[11]*z[53];
    z[49]=z[53] + z[49] + z[51];
    z[49]=z[10]*z[49];
    z[51]=n<T>(7,2)*z[9];
    z[45]= - 6*z[8] - n<T>(7,2)*z[23] - z[45] - z[51];
    z[45]=z[8]*z[45];
    z[53]=19*z[8];
    z[54]=4*z[11];
    z[55]=z[53] + z[54];
    z[55]=z[11]*z[55];
    z[45]=z[49] + z[45] + z[55];
    z[45]=z[10]*z[45];
    z[49]=npow(z[5],3);
    z[55]=3*z[49];
    z[56]=z[55]*z[2];
    z[57]= - 7*z[35] - z[56];
    z[57]=z[2]*z[57];
    z[58]=7*z[5];
    z[59]=z[28]*z[58];
    z[59]=z[59] + 3*z[42];
    z[59]=z[59]*z[22];
    z[60]= - 5*z[15] - z[27];
    z[61]=z[7]*z[2];
    z[62]=static_cast<T>(17)- 8*z[61];
    z[62]=z[7]*z[62];
    z[20]=z[20] + z[30] + z[45] + 9*z[11] + z[33] + z[59] + n<T>(2,3)*z[62]
    + z[57] - n<T>(27,2)*z[9] + 2*z[60] - z[44];
    z[20]=z[6]*z[20];
    z[30]=n<T>(14,3)*z[9];
    z[33]=n<T>(4,3)*z[7];
    z[44]= - z[13] + n<T>(7,3)*z[9];
    z[44]=z[3]*z[7]*z[44];
    z[44]=z[44] + z[33] + z[13] - z[30];
    z[44]=z[3]*z[44];
    z[45]=npow(z[4],3);
    z[57]=z[39]*z[45];
    z[59]= - z[2] + z[42];
    z[59]=n<T>(4,3)*z[59] + z[29];
    z[59]=z[8]*z[59];
    z[60]=4*z[61];
    z[62]= - static_cast<T>(11)+ z[60];
    z[63]= - z[9] - z[8];
    z[63]=z[10]*z[63];
    z[44]=z[44] + n<T>(7,3)*z[63] + z[59] + n<T>(1,3)*z[62] + z[57];
    z[59]=2*z[6];
    z[44]=z[44]*z[59];
    z[62]=z[28]*z[45];
    z[63]=8*z[5];
    z[64]= - z[62]*z[63];
    z[65]= - z[9] - z[7];
    z[65]=z[7]*z[65];
    z[64]=z[65] + z[64];
    z[65]=z[45]*z[37];
    z[66]=z[45]*z[11];
    z[67]=3*z[66];
    z[65]=z[65] - z[67];
    z[68]=npow(z[10],2);
    z[65]=z[65]*z[68];
    z[69]=z[25]*z[45];
    z[70]= - z[5]*z[69];
    z[70]= - z[17] + z[70];
    z[71]=3*z[3];
    z[70]=z[70]*z[71];
    z[64]=z[70] + 2*z[64] + z[65];
    z[64]=z[3]*z[64];
    z[65]=z[43]*z[45]*z[2];
    z[70]=z[45]*z[8];
    z[72]=z[14]*z[45];
    z[72]=z[72] + z[70];
    z[67]=2*z[72] - z[67];
    z[67]=z[10]*z[67];
    z[65]=z[65] + z[67];
    z[65]=z[10]*z[65];
    z[67]=z[14] - 2*z[62];
    z[67]=z[8]*z[67];
    z[52]=z[67] - z[52];
    z[52]=2*z[52] + z[65];
    z[52]=z[10]*z[52];
    z[57]= - z[34]*z[57];
    z[65]=z[29]*z[7];
    z[67]= - static_cast<T>(1)+ z[61];
    z[67]=n<T>(8,3)*z[67] - z[65];
    z[67]=z[67]*z[43];
    z[72]=n<T>(1,3)*z[9];
    z[73]=z[14] - z[72];
    z[74]=n<T>(2,3) - z[61];
    z[74]=z[7]*z[74];
    z[44]=z[44] + z[64] + z[52] - z[54] + z[67] + z[57] + 2*z[73] + 
    z[74];
    z[44]=z[6]*z[44];
    z[52]=n<T>(15,2)*z[7];
    z[57]=2*z[5];
    z[64]=z[52] - z[36] - z[57] - n<T>(9,2)*z[9];
    z[64]=z[8]*z[64];
    z[67]=z[31]*z[45];
    z[73]=3*z[70] + 8*z[66];
    z[73]=z[11]*z[73];
    z[73]=6*z[67] + z[73];
    z[73]=z[10]*z[73];
    z[67]=z[2]*z[67];
    z[67]= - 12*z[67] + z[73];
    z[67]=z[10]*z[67];
    z[73]=6*z[31];
    z[73]=z[62]*z[73];
    z[74]=z[48]*z[8];
    z[67]=z[67] + z[73] + z[74];
    z[67]=z[10]*z[67];
    z[73]=z[48]*z[68];
    z[75]=z[48]*z[10];
    z[76]=z[36] + 2*z[75];
    z[76]=z[3]*z[76];
    z[73]=z[76] + 10*z[73];
    z[73]=z[45]*z[73];
    z[76]=2*z[35];
    z[62]=z[76]*z[62];
    z[62]= - n<T>(17,3)*z[17] + z[62] + z[73];
    z[62]=z[3]*z[62];
    z[73]=n<T>(45,2)*z[9] - n<T>(13,3)*z[7];
    z[73]=z[7]*z[73];
    z[77]=3*z[8];
    z[78]= - z[77] + z[11];
    z[78]=z[11]*z[78];
    z[44]=z[44] + z[62] + z[67] + z[78] + z[73] + z[64];
    z[44]=z[6]*z[44];
    z[62]=npow(z[4],4);
    z[64]= - z[57]*z[39]*z[62];
    z[67]=7*z[9];
    z[73]= - z[13] + z[67];
    z[73]=z[7]*z[73];
    z[64]=z[73] + z[64];
    z[73]=2*z[3];
    z[64]=z[64]*z[73];
    z[78]=npow(z[2],4);
    z[79]= - z[5]*z[78]*z[62];
    z[80]= - static_cast<T>(7)+ z[60];
    z[65]=n<T>(1,3)*z[80] - z[65];
    z[65]=z[65]*z[43];
    z[80]=z[8]*z[68];
    z[80]= - z[29] + z[80];
    z[80]=z[10]*z[62]*z[80];
    z[30]= - z[8]*z[30];
    z[30]=z[30] + z[80];
    z[30]=z[10]*z[30];
    z[80]= - z[67] + n<T>(11,3)*z[7];
    z[30]=z[64] + z[30] + z[65] + 2*z[80] + z[79];
    z[30]=z[6]*z[30];
    z[64]=z[25] - z[10];
    z[64]=z[64]*z[10];
    z[64]=z[64] - z[28];
    z[65]= - z[68]*z[64]*z[31]*z[62];
    z[79]=z[28]*z[3];
    z[80]=z[79] + z[39];
    z[80]=z[80]*z[76]*z[62];
    z[80]= - z[17] + z[80];
    z[71]=z[80]*z[71];
    z[19]=z[19]*z[8];
    z[67]= - z[67] - z[7];
    z[67]=z[7]*z[67];
    z[30]=z[30] + z[71] + 5*z[65] + z[67] + n<T>(2,3)*z[19];
    z[30]=z[6]*z[30];
    z[65]=npow(z[3],2);
    z[67]=z[65]*z[49];
    z[71]=z[67]*z[62]*z[28];
    z[80]=z[7]*z[9];
    z[81]=z[80]*z[8];
    z[82]= - 10*z[17] + n<T>(41,2)*z[81];
    z[30]=z[30] + n<T>(1,3)*z[82] + z[71];
    z[30]=z[6]*z[30];
    z[71]=npow(z[10],3);
    z[82]=z[16]*z[62]*z[71];
    z[82]= - z[17] + z[82];
    z[82]=z[48]*z[82];
    z[83]=npow(z[11],3);
    z[84]=z[83]*z[62];
    z[85]=z[9]*z[12];
    z[86]= - z[85]*z[84];
    z[84]=z[84]*z[7];
    z[87]=z[10]*z[84];
    z[86]=z[86] + z[87];
    z[87]=n<T>(11,3)*z[11];
    z[88]=2*z[9];
    z[89]=z[88] - z[7];
    z[89]=z[89]*z[87];
    z[89]=z[80] + z[89];
    z[89]=z[11]*z[89];
    z[90]=npow(z[9],2);
    z[91]=z[90]*z[7];
    z[89]= - z[91] + z[89];
    z[62]=z[3]*z[11]*z[62]*z[89];
    z[62]=n<T>(11,3)*z[86] + z[62];
    z[62]=z[3]*z[62];
    z[84]=z[68]*z[84];
    z[62]= - n<T>(11,3)*z[84] + z[62];
    z[62]=z[62]*z[73];
    z[62]=z[62] + z[82];
    z[62]=z[3]*z[62];
    z[82]=npow(z[4],5);
    z[31]= - z[71]*z[64]*z[82]*z[31];
    z[64]=z[78]*z[82];
    z[39]=z[82]*z[39];
    z[71]=z[3]*z[39];
    z[64]=z[64] + z[71];
    z[64]=z[73]*z[35]*z[64];
    z[19]=z[80] + n<T>(1,3)*z[19];
    z[19]=z[64] + 14*z[19] + z[31];
    z[19]=z[6]*z[19];
    z[31]=2*z[49];
    z[39]= - z[65]*z[31]*z[39];
    z[64]= - z[80]*z[24];
    z[19]=z[19] + z[39] - z[17] + z[64];
    z[19]=z[19]*npow(z[6],2);
    z[39]=z[82]*z[83]*npow(z[3],5)*z[80];
    z[64]= - z[78]*npow(z[4],6)*z[67];
    z[64]=n<T>(14,3)*z[81] + z[64];
    z[64]=z[1]*z[64]*npow(z[6],3);
    z[19]=z[64] - n<T>(22,3)*z[39] + z[19];
    z[19]=z[1]*z[19];
    z[39]=z[11]*z[17];
    z[19]=z[19] + z[30] + z[39] + z[62];
    z[19]=z[1]*z[19];
    z[30]=z[45]*z[85];
    z[39]=z[45]*z[12];
    z[62]= - z[80] - z[39];
    z[64]=n<T>(22,3)*z[11];
    z[62]=z[62]*z[64];
    z[65]=3*z[17];
    z[67]=npow(z[13],2);
    z[71]=z[67]*z[9];
    z[30]=z[62] - 4*z[30] - z[65] + z[31] + z[71];
    z[30]=z[11]*z[30];
    z[62]=z[90]*z[45];
    z[73]=2*z[12];
    z[78]= - z[73]*z[62];
    z[30]=z[78] + z[30];
    z[30]=z[11]*z[30];
    z[78]=3*z[67];
    z[82]=2*z[90];
    z[83]= - z[78] - z[82];
    z[84]=z[45]*z[7];
    z[83]=z[83]*z[84];
    z[86]=25*z[84] - 22*z[66];
    z[86]=z[86]*z[48];
    z[83]=z[83] + n<T>(2,3)*z[86];
    z[83]=z[10]*z[83];
    z[86]=4*z[90];
    z[89]=n<T>(7,3)*z[80];
    z[92]=z[86] + z[89];
    z[92]=z[92]*z[45];
    z[93]=z[9] - n<T>(8,3)*z[7];
    z[93]=z[93]*z[45];
    z[93]=z[93] + n<T>(22,3)*z[66];
    z[94]=2*z[11];
    z[93]=z[93]*z[94];
    z[92]=z[92] + z[93];
    z[92]=z[11]*z[92];
    z[93]=2*z[7];
    z[62]=z[93]*z[62];
    z[62]=z[62] + z[92];
    z[62]=z[3]*z[62];
    z[69]=z[49]*z[69];
    z[30]=z[62] + z[83] + z[69] + z[30];
    z[30]=z[3]*z[30];
    z[62]= - z[90] + n<T>(4,3)*z[80];
    z[62]=z[62]*z[93];
    z[69]=4*z[7];
    z[83]=z[9] - z[69];
    z[83]=z[11]*z[7]*z[83];
    z[62]=z[83] + z[62] - z[49] + z[71];
    z[62]=z[11]*z[62];
    z[83]=3*z[16];
    z[45]= - z[45]*z[83];
    z[92]= - 7*z[84] + 44*z[66];
    z[95]=n<T>(1,3)*z[11];
    z[92]=z[92]*z[95];
    z[45]=z[45] + z[92];
    z[45]=z[10]*z[11]*z[45];
    z[66]=z[66]*z[12];
    z[66]=n<T>(11,3)*z[66];
    z[92]=z[55] + z[66];
    z[92]=z[92]*z[48];
    z[45]=2*z[92] + z[45];
    z[45]=z[10]*z[45];
    z[30]=z[30] + z[62] + z[45];
    z[30]=z[3]*z[30];
    z[39]=z[43]*z[39];
    z[39]= - z[66] + z[55] + z[39];
    z[39]=z[39]*z[94];
    z[45]= - z[12]*z[70];
    z[45]= - z[49] + z[45];
    z[45]=z[8]*z[45];
    z[39]=z[45] + z[39];
    z[39]=z[11]*z[39];
    z[45]=z[49]*z[43];
    z[45]= - z[84] + z[45];
    z[45]=z[45]*z[75];
    z[39]=z[39] + z[45];
    z[39]=z[10]*z[39];
    z[45]= - z[49] - z[91];
    z[45]=2*z[45] + z[81];
    z[45]=z[8]*z[45];
    z[62]=5*z[35];
    z[66]= - z[8]*z[62];
    z[66]= - z[31] + z[66];
    z[66]=z[11]*z[66];
    z[39]=z[39] + z[45] + z[66];
    z[39]=z[10]*z[39];
    z[45]=z[90] + z[89];
    z[45]=z[7]*z[45];
    z[45]=z[45] - z[49] - z[71];
    z[66]=z[8] - z[7] + z[13] - z[51];
    z[66]=z[7]*z[66];
    z[70]=z[49]*z[2];
    z[66]=z[76] + z[70] + z[66];
    z[66]=z[8]*z[66];
    z[72]=z[94] + z[69] - z[13] - z[72];
    z[72]=z[7]*z[72];
    z[81]=z[8] - z[9];
    z[81]=z[13]*z[81];
    z[72]= - z[62] + z[72] + z[81];
    z[72]=z[11]*z[72];
    z[19]=z[19] + z[44] + z[30] + z[39] + z[72] + 2*z[45] + z[66];
    z[19]=z[1]*z[19];
    z[30]=z[22]*z[7];
    z[39]=z[55] - z[30];
    z[44]=z[62] + 8*z[22];
    z[44]=z[8]*z[44];
    z[45]= - z[35] + n<T>(2,3)*z[22];
    z[66]= - z[8]*z[5];
    z[45]=2*z[45] + z[66];
    z[45]=z[45]*z[54];
    z[39]=z[45] + 2*z[39] + z[44];
    z[39]=z[11]*z[39];
    z[44]= - z[35]*z[75]*z[24];
    z[45]=9*z[13];
    z[54]=z[30]*z[45];
    z[66]=z[22]*z[50];
    z[39]=z[44] + z[39] + z[54] + z[66];
    z[39]=z[10]*z[39];
    z[44]=z[22]*z[12];
    z[54]=5*z[7];
    z[63]=n<T>(74,3)*z[44] - z[63] - z[54];
    z[63]=z[11]*z[63];
    z[66]=2*z[13];
    z[72]=z[66] - z[54];
    z[72]=z[7]*z[72];
    z[81]= - 5*z[44] - z[66] + 19*z[5];
    z[81]=z[8]*z[81];
    z[63]=z[63] + z[72] + z[81];
    z[63]=z[11]*z[63];
    z[72]=z[25] - z[12];
    z[81]= - z[72]*z[22]*z[43];
    z[84]=z[66] - z[7];
    z[92]= - z[51] - z[84];
    z[92]=z[7]*z[92];
    z[81]=z[81] + z[35] + z[92];
    z[81]=z[8]*z[81];
    z[92]=z[73]*z[67];
    z[96]=z[22]*z[92];
    z[39]=z[39] + z[63] + z[81] + z[96] - 6*z[91] - z[55] - 2*z[71];
    z[39]=z[10]*z[39];
    z[63]=n<T>(44,3)*z[11];
    z[81]= - z[13] - z[57];
    z[44]=z[63] - n<T>(34,3)*z[44] + 2*z[81] - z[41];
    z[44]=z[11]*z[44];
    z[81]= - 26*z[9] - 11*z[7];
    z[81]=z[81]*z[41];
    z[91]= - z[22]*z[85];
    z[96]=2*z[67];
    z[97]= - z[13] + z[46];
    z[97]=z[9]*z[97];
    z[44]=z[44] + z[91] + z[81] + z[97] - z[96] - z[62];
    z[44]=z[11]*z[44];
    z[81]=z[46] - z[38];
    z[81]=z[7]*z[81];
    z[91]=22*z[11];
    z[97]=n<T>(2,3)*z[9] - z[7];
    z[97]=z[97]*z[91];
    z[98]= - z[13]*z[46];
    z[81]=z[97] + n<T>(50,3)*z[22] + z[98] + z[81];
    z[81]=z[11]*z[81];
    z[97]= - 6*z[90] + z[89];
    z[97]=z[7]*z[97];
    z[98]=6*z[9];
    z[99]=z[98] - z[7];
    z[99]=z[99]*z[22];
    z[81]=z[81] + z[99] + z[97] + z[71] + 6*z[49];
    z[81]=z[11]*z[81];
    z[97]=3*z[13];
    z[99]= - z[97] + n<T>(5,2)*z[9];
    z[99]=z[7]*z[99];
    z[78]=z[99] - z[78] - z[86];
    z[78]=z[78]*z[22];
    z[86]=z[91]*z[80];
    z[65]= - z[65] - z[86];
    z[99]=z[48]*z[3];
    z[65]=z[65]*z[99];
    z[65]=z[65] + z[78] + z[81];
    z[65]=z[3]*z[65];
    z[78]=n<T>(3,2)*z[9];
    z[81]= - z[78] - z[84];
    z[81]=z[81]*z[54];
    z[81]=z[81] + z[67] + z[76];
    z[81]=z[81]*z[22];
    z[84]= - n<T>(59,3)*z[22] - z[76] + z[83];
    z[84]=z[84]*z[94];
    z[30]=z[84] + 12*z[49] - n<T>(5,3)*z[30];
    z[30]=z[11]*z[30];
    z[30]=z[81] + z[30];
    z[30]=z[10]*z[30];
    z[49]= - z[35]*z[25];
    z[81]=z[12]*z[67];
    z[83]= - z[2]*z[83];
    z[49]=z[83] + z[81] + z[49];
    z[49]=z[49]*z[22];
    z[81]=3*z[90];
    z[83]=z[81] + n<T>(14,3)*z[80];
    z[83]=z[83]*z[93];
    z[30]=z[65] + z[30] + z[44] + z[49] + z[83] - z[55] - z[71];
    z[30]=z[3]*z[30];
    z[44]=z[25]*z[15];
    z[49]=static_cast<T>(1)- z[44];
    z[49]=z[49]*z[61];
    z[23]=z[72]*z[23];
    z[65]=npow(z[12],2);
    z[71]=z[65]*z[13];
    z[72]=z[71] - z[12];
    z[83]= - z[2] - z[72];
    z[83]=z[83]*z[43];
    z[84]=z[66]*z[12];
    z[90]= - static_cast<T>(1)+ z[84];
    z[23]=z[83] + z[23] + 2*z[90] + z[49];
    z[23]=z[8]*z[23];
    z[49]=z[2]*z[15];
    z[83]= - n<T>(5,2) + 4*z[49];
    z[83]=3*z[83] - 5*z[61];
    z[83]=z[7]*z[83];
    z[90]=3*z[36];
    z[100]=13*z[5];
    z[23]=z[23] + z[83] - z[90] + z[88] - z[66] - z[100];
    z[23]=z[8]*z[23];
    z[57]= - z[88] + z[97] - z[57];
    z[57]=z[57]*z[88];
    z[22]=z[65]*z[22]*z[96];
    z[38]=z[63] + z[38] - z[98] - 13*z[13] + 18*z[5] - 8*z[8];
    z[38]=z[11]*z[38];
    z[63]=z[70] + z[67];
    z[70]=4*z[14];
    z[83]=z[70] - z[58];
    z[83]=z[5]*z[83];
    z[88]= - 7*z[15] - z[13];
    z[88]=n<T>(28,3)*z[7] + 2*z[88] - n<T>(19,2)*z[9];
    z[88]=z[7]*z[88];
    z[19]=z[19] + z[20] + z[30] + z[39] + z[38] + z[23] + z[22] + z[88]
    + z[57] + z[83] - 6*z[63];
    z[19]=z[1]*z[19];
    z[20]=8*z[9];
    z[22]= - z[97] + z[20];
    z[22]=z[9]*z[22];
    z[23]= - z[98] - z[41];
    z[23]=z[7]*z[23];
    z[30]=static_cast<T>(4)- z[85];
    z[30]=z[30]*z[64];
    z[30]=z[30] + z[46] - 19*z[7];
    z[30]=z[11]*z[30];
    z[22]=z[30] + z[22] + z[23];
    z[22]=z[11]*z[22];
    z[23]= - z[81] + n<T>(1,3)*z[80];
    z[23]=z[23]*z[93];
    z[30]=n<T>(4,3)*z[9] - z[7];
    z[30]=z[30]*z[91];
    z[38]=5*z[9] - z[7];
    z[38]=z[7]*z[38];
    z[30]=z[38] + z[30];
    z[30]=z[11]*z[30];
    z[23]=z[23] + z[30];
    z[23]=z[11]*z[23];
    z[17]= - z[17] - z[86];
    z[17]=z[17]*z[99];
    z[17]=z[23] + z[17];
    z[17]=z[3]*z[17];
    z[23]=z[81] + z[89];
    z[23]=z[7]*z[23];
    z[23]=z[55] + z[23];
    z[30]=z[7]*z[91];
    z[30]=z[16] + z[30];
    z[30]=z[30]*z[75];
    z[17]=z[17] + z[30] + 2*z[23] + z[22];
    z[17]=z[3]*z[17];
    z[22]= - z[35] + n<T>(11,3)*z[16];
    z[23]=z[7] - z[26];
    z[23]=z[23]*z[87];
    z[22]=2*z[22] + z[23];
    z[22]=z[11]*z[22];
    z[23]= - z[67] - z[82];
    z[23]=z[7]*z[23];
    z[23]=z[31] + z[23];
    z[16]=z[16]*z[75];
    z[16]= - 4*z[16] + 3*z[23] + z[22];
    z[16]=z[10]*z[16];
    z[22]=5*z[13];
    z[20]= - z[20] + z[22] - z[34];
    z[20]=z[9]*z[20];
    z[23]=z[41] - z[22] - z[51];
    z[23]=z[7]*z[23];
    z[26]=static_cast<T>(2)- z[85];
    z[26]=z[9]*z[26];
    z[26]=z[26] + z[97] - z[58];
    z[30]= - z[12]*z[91];
    z[30]=static_cast<T>(115)+ z[30];
    z[30]=z[30]*z[95];
    z[26]=z[30] + 2*z[26] + z[41];
    z[26]=z[11]*z[26];
    z[30]=z[70] - 9*z[5];
    z[30]=z[5]*z[30];
    z[16]=z[17] + z[16] + z[26] + z[23] - z[56] + z[20] - 9*z[67] + 
    z[30];
    z[16]=z[3]*z[16];
    z[17]=z[13]*z[12];
    z[20]=static_cast<T>(1)- z[17];
    z[23]= - z[2] + z[72];
    z[23]=z[8]*z[23];
    z[20]=3*z[20] + z[23];
    z[20]=z[8]*z[20];
    z[20]=z[20] - z[54] + z[100] + z[37] + z[97];
    z[20]=z[20]*z[43];
    z[23]=z[12]*z[87];
    z[23]= - static_cast<T>(8)+ z[23];
    z[23]=z[23]*z[94];
    z[26]=z[8]*z[12];
    z[30]= - static_cast<T>(12)+ z[26];
    z[30]=z[8]*z[30];
    z[23]=z[23] + z[30] - z[33] + z[45] - 29*z[5];
    z[23]=z[11]*z[23];
    z[30]=z[43] + z[5] + z[93];
    z[30]=z[30]*z[94];
    z[31]= - z[13] + z[93];
    z[31]=z[7]*z[31];
    z[33]=17*z[5];
    z[38]=z[13] - z[33];
    z[38]=z[8]*z[38];
    z[30]=z[30] + z[38] - z[76] + z[31];
    z[30]=z[11]*z[30];
    z[31]=z[7]*z[13];
    z[38]=z[31] + z[50];
    z[38]=z[8]*z[38];
    z[39]=z[5]*z[43]*z[75];
    z[30]=z[39] + z[38] + z[30];
    z[30]=z[10]*z[30];
    z[38]=z[67] + z[35];
    z[39]=4*z[13];
    z[41]= - z[39] - z[51];
    z[41]=3*z[41] - 8*z[7];
    z[41]=z[7]*z[41];
    z[22]=z[9]*z[22];
    z[20]=z[30] + z[23] + z[20] + z[41] + 2*z[38] + z[22];
    z[20]=z[10]*z[20];
    z[22]=z[37] - z[77];
    z[22]=z[8]*z[22];
    z[23]= - z[11]*z[24];
    z[22]=z[22] + z[23];
    z[22]=z[10]*z[22];
    z[23]=z[8]*z[2];
    z[30]= - n<T>(4,3) - 3*z[23];
    z[30]=z[8]*z[30];
    z[22]=z[22] + z[94] - z[37] + z[30];
    z[22]=z[10]*z[22];
    z[30]=n<T>(5,3)*z[2];
    z[38]= - z[30] + 3*z[29];
    z[38]=z[8]*z[38];
    z[38]=static_cast<T>(2)+ z[38];
    z[22]=2*z[38] + z[22];
    z[22]=z[10]*z[22];
    z[38]= - n<T>(4,3) - z[23];
    z[38]=z[10]*z[38];
    z[30]=z[38] + z[30] + z[29];
    z[30]=z[8]*z[30];
    z[30]= - n<T>(4,3) + z[30];
    z[30]=z[10]*z[30];
    z[38]=z[2] - z[29];
    z[30]=n<T>(4,3)*z[38] + z[30];
    z[30]=z[10]*z[30];
    z[38]=static_cast<T>(1)- z[32];
    z[38]=z[38]*z[28];
    z[41]= - z[5] + z[36];
    z[41]=z[41]*z[79];
    z[30]=z[41] + z[38] + z[30];
    z[30]=z[30]*z[59];
    z[38]=z[62] + z[56];
    z[38]=z[2]*z[38];
    z[38]= - z[100] + z[38];
    z[38]=z[2]*z[38];
    z[21]= - z[37] + z[21];
    z[41]=2*z[10];
    z[21]=z[21]*z[41];
    z[21]=z[21] + static_cast<T>(2)+ z[38];
    z[21]=z[3]*z[21];
    z[38]= - z[58] - z[36];
    z[38]=z[2]*z[38];
    z[38]=static_cast<T>(10)+ z[38];
    z[38]=z[2]*z[38];
    z[40]=n<T>(8,3)*z[28] + z[40];
    z[40]=z[8]*z[40];
    z[21]=z[30] + z[21] + z[22] + z[40] + z[38] - z[42];
    z[21]=z[6]*z[21];
    z[22]=n<T>(73,6)*z[9] + z[7];
    z[22]=z[7]*z[22];
    z[18]=z[18] - z[48] + z[47] + z[22];
    z[18]=z[3]*z[18];
    z[22]= - z[35] + z[56];
    z[22]=z[22]*z[25];
    z[27]=2*z[15] - z[27];
    z[30]=n<T>(79,6) + z[61];
    z[30]=z[7]*z[30];
    z[18]=z[18] - 14*z[75] + 13*z[11] + z[30] + z[22] - z[9] + 2*z[27]
    - 11*z[5];
    z[18]=z[3]*z[18];
    z[22]=static_cast<T>(2)- z[23];
    z[22]=z[8]*z[22];
    z[22]=3*z[14] + z[22];
    z[22]=z[22]*z[43];
    z[27]=npow(z[8],3);
    z[27]=2*z[27] + z[74];
    z[27]=z[10]*z[27];
    z[30]= - z[53] - 12*z[11];
    z[30]=z[11]*z[30];
    z[22]=z[27] + z[22] + z[30];
    z[22]=z[10]*z[22];
    z[27]= - z[37] - z[13];
    z[30]= - n<T>(1,2) - 8*z[23];
    z[30]=z[8]*z[30];
    z[22]=z[22] + 16*z[11] + z[30] + 2*z[27] + n<T>(1,2)*z[9];
    z[22]=z[10]*z[22];
    z[27]=39*z[2] + 49*z[42];
    z[27]=n<T>(1,2)*z[27] + 6*z[29];
    z[27]=z[8]*z[27];
    z[30]= - 8*z[15] - z[90];
    z[30]=z[2]*z[30];
    z[35]=n<T>(17,6)*z[2] + 6*z[42];
    z[35]=z[7]*z[35];
    z[18]=z[21] + z[18] + z[22] + z[27] + z[35] + n<T>(22,3) + z[30];
    z[18]=z[6]*z[18];
    z[21]=npow(z[12],3);
    z[22]=z[13]*z[21];
    z[27]=z[2]*z[12];
    z[22]=z[27] - z[65] + z[22];
    z[22]=z[22]*z[43];
    z[27]= - z[65]*z[66];
    z[27]=z[12] + z[27];
    z[30]= - static_cast<T>(3)+ z[49];
    z[30]=z[30]*z[25];
    z[22]=z[22] + 3*z[27] + z[30];
    z[22]=z[8]*z[22];
    z[27]=static_cast<T>(7)+ z[44];
    z[27]=z[27]*z[60];
    z[30]=z[73]*z[14];
    z[22]=z[22] + z[27] - 11*z[32] + 6*z[17] + static_cast<T>(7)- z[30];
    z[22]=z[8]*z[22];
    z[27]= - z[12]*z[64];
    z[26]=z[27] - z[26] + static_cast<T>(20)+ 3*z[85];
    z[26]=z[11]*z[26];
    z[27]= - static_cast<T>(5)+ z[17];
    z[27]=z[27]*z[66];
    z[27]= - 3*z[9] + z[27] + z[33];
    z[35]=z[49] - z[61];
    z[35]=n<T>(37,6) - 12*z[35];
    z[35]=z[7]*z[35];
    z[16]=z[19] + z[18] + z[16] + z[20] + z[26] + z[22] + z[35] + 2*
    z[27] - z[36];
    z[16]=z[1]*z[16];
    z[18]=4*z[67];
    z[19]= - z[18] - n<T>(3,2)*z[80];
    z[19]=z[4]*z[19];
    z[20]=z[4]*z[7];
    z[22]= - z[67]*z[20];
    z[26]=z[48]*z[4];
    z[27]= - z[7]*z[26];
    z[22]=z[22] + z[27];
    z[22]=z[22]*z[41];
    z[19]=z[22] + z[19] - z[26];
    z[19]=z[10]*z[19];
    z[22]= - z[92] - z[78];
    z[22]=z[4]*z[22];
    z[27]=z[9]*z[65];
    z[27]= - z[73] + z[27];
    z[35]=z[11]*z[4];
    z[27]=z[27]*z[35];
    z[37]=z[4]*z[85];
    z[27]=z[37] + z[27];
    z[27]=z[11]*z[27];
    z[37]= - z[9]*z[73];
    z[37]=static_cast<T>(1)+ z[37];
    z[37]=z[37]*z[35];
    z[38]= - z[4]*z[9];
    z[37]=z[38] + z[37];
    z[37]=z[11]*z[37];
    z[38]=z[3]*z[9]*z[26];
    z[37]=z[37] + z[38];
    z[37]=z[3]*z[37];
    z[19]=z[37] + z[19] + z[22] + z[27];
    z[19]=z[3]*z[19];
    z[22]=z[67] + z[31];
    z[22]=z[4]*z[22];
    z[20]= - z[20] + z[35];
    z[20]=z[11]*z[20];
    z[20]=z[22] + z[20];
    z[20]=z[10]*z[20];
    z[22]=static_cast<T>(1)+ z[84];
    z[22]=z[22]*z[39];
    z[22]=z[22] - n<T>(3,2)*z[7];
    z[22]=z[4]*z[22];
    z[27]=z[4]*z[12];
    z[31]=z[94]*z[27];
    z[31]= - 3*z[4] + z[31];
    z[31]=z[11]*z[31];
    z[20]=4*z[20] + z[22] + z[31];
    z[20]=z[10]*z[20];
    z[18]=z[65]*z[18];
    z[22]=z[25]*z[5];
    z[18]=z[22] - n<T>(3,2) + z[18];
    z[18]=z[4]*z[18];
    z[22]=z[35]*z[65];
    z[25]=z[22] + n<T>(56,3) - z[27];
    z[25]=z[11]*z[25];
    z[18]=z[19] + z[20] + z[25] + z[18] + z[52] - n<T>(13,2)*z[9] - z[13] - 
    z[33];
    z[18]=z[3]*z[18];
    z[19]=n<T>(7,2)*z[4];
    z[20]=z[19]*z[8];
    z[25]=z[75]*z[4];
    z[20]=z[20] + z[25];
    z[31]= - static_cast<T>(2)- z[17];
    z[31]=z[4]*z[31]*z[66];
    z[33]=z[65]*z[4];
    z[37]=z[43]*z[33];
    z[37]= - 5*z[27] + z[37];
    z[37]=z[11]*z[37];
    z[38]=z[8]*z[27];
    z[38]=z[4] + z[38];
    z[37]=4*z[38] + z[37];
    z[37]=z[11]*z[37];
    z[31]=z[37] + z[31] - z[20];
    z[31]=z[10]*z[31];
    z[37]=z[14] + 6*z[13];
    z[37]= - z[69] + 2*z[37] - z[58];
    z[24]= - z[33]*z[24];
    z[22]= - z[22] + z[24] - n<T>(20,3) + 7*z[27];
    z[22]=z[11]*z[22];
    z[24]= - z[12] - z[71];
    z[24]=z[24]*z[39];
    z[24]=n<T>(1,2) + z[24];
    z[24]=z[4]*z[24];
    z[27]= - 8*z[12] + 7*z[2];
    z[27]=z[4]*z[27];
    z[27]= - n<T>(11,2) + z[27];
    z[27]=z[8]*z[27];
    z[22]=z[31] + z[22] + z[27] + 2*z[37] + z[24];
    z[22]=z[10]*z[22];
    z[24]=z[4]*z[2];
    z[27]= - static_cast<T>(2)+ z[32];
    z[27]=z[27]*z[24];
    z[31]=z[4]*z[94];
    z[25]=z[31] - z[25];
    z[25]=z[25]*z[68];
    z[25]=z[25] + static_cast<T>(6)+ z[27];
    z[27]=z[34] - z[36];
    z[27]=z[27]*z[24];
    z[26]= - z[68]*z[26];
    z[26]=z[27] + z[26];
    z[26]=z[3]*z[26];
    z[25]=2*z[25] + z[26];
    z[25]=z[3]*z[25];
    z[19]= - z[29]*z[19];
    z[20]=4*z[35] - z[20];
    z[20]=z[10]*z[20];
    z[23]= - n<T>(19,2) + 7*z[23];
    z[23]=z[4]*z[23];
    z[20]=z[20] + z[23];
    z[20]=z[10]*z[20];
    z[19]=z[20] + z[19] + static_cast<T>(23)+ n<T>(15,2)*z[24];
    z[19]=z[10]*z[19];
    z[20]=n<T>(7,2)*z[2];
    z[23]= - z[4]*z[28];
    z[19]=z[25] + z[19] - z[20] + z[23];
    z[19]=z[6]*z[19];
    z[21]= - z[21]*z[96];
    z[21]=z[21] + n<T>(1,2)*z[2];
    z[21]=z[4]*z[21];
    z[20]=4*z[12] - z[20];
    z[20]=z[2]*z[20];
    z[20]=2*z[65] + z[20];
    z[20]=z[4]*z[20];
    z[20]=z[20] + z[12] - n<T>(5,2)*z[2];
    z[20]=z[8]*z[20];
    z[23]= - n<T>(43,3)*z[12] + z[33];
    z[23]=z[11]*z[23];

    r +=  - n<T>(43,6) + z[16] + 10*z[17] + z[18] + z[19] + z[20] + z[21]
       + z[22] + z[23] - z[30] + 24*z[32] + 11*z[61];
 
    return r;
}

template double qg_2lNLC_r282(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r282(const std::array<dd_real,31>&);
#endif
