#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1442(const std::array<T,31>& k) {
  T z[60];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=k[7];
    z[11]=k[13];
    z[12]=k[2];
    z[13]=k[25];
    z[14]=z[5]*z[2];
    z[15]=z[14]*z[7];
    z[16]=z[5] + z[2];
    z[17]=z[15] - z[16];
    z[18]=4*z[4];
    z[17]=z[17]*z[18];
    z[19]=z[3]*z[16];
    z[17]=z[17] + 19*z[14] - n<T>(23,3)*z[19];
    z[17]=z[4]*z[17];
    z[19]=z[14]*z[3];
    z[17]=n<T>(53,3)*z[19] + z[17];
    z[17]=z[4]*z[17];
    z[20]=n<T>(1,3)*z[3];
    z[21]= - z[16]*z[20];
    z[21]=z[14] + z[21];
    z[21]=z[21]*z[18];
    z[19]=n<T>(23,3)*z[19] + z[21];
    z[21]=npow(z[4],2);
    z[19]=z[19]*z[21];
    z[22]=z[3]*npow(z[4],3);
    z[23]=n<T>(4,3)*z[22];
    z[24]=z[1]*z[14]*z[23];
    z[19]=z[19] + z[24];
    z[19]=z[1]*z[19];
    z[16]= - z[6]*z[16]*z[23];
    z[16]=z[19] + z[17] + z[16];
    z[16]=z[1]*z[16];
    z[17]=npow(z[9],3);
    z[19]=z[4]*z[7];
    z[23]=z[17]*npow(z[19],2);
    z[24]=z[7]*z[10];
    z[25]=z[10]*z[8];
    z[26]=z[24] - z[25];
    z[27]=z[26]*z[23];
    z[16]=z[27] - z[16];
    z[27]=n<T>(1,3)*z[5];
    z[28]=n<T>(1,3)*z[11];
    z[29]=z[27] - z[28];
    z[30]=z[10]*z[11];
    z[31]=z[30] - z[14];
    z[32]=n<T>(1,3)*z[7];
    z[33]= - z[31]*z[32];
    z[33]=z[33] - z[2] - z[29];
    z[33]=z[33]*z[19];
    z[15]=5*z[15];
    z[34]=n<T>(4,3)*z[11];
    z[35]= - n<T>(50,3)*z[5] - 19*z[2] - z[34];
    z[33]=n<T>(4,3)*z[33] + n<T>(1,3)*z[35] + z[15];
    z[33]=z[4]*z[33];
    z[35]=2*z[11];
    z[36]=z[35]*z[10];
    z[37]= - z[36] + 47*z[14];
    z[38]=5*z[2];
    z[39]= - n<T>(29,9)*z[5] - z[38] + z[34];
    z[39]=z[3]*z[39];
    z[33]=z[33] + n<T>(2,9)*z[37] + z[39];
    z[33]=z[4]*z[33];
    z[37]=n<T>(16,3)*z[5];
    z[39]= - z[37] - z[38] + n<T>(8,3)*z[11];
    z[39]=z[3]*z[39];
    z[29]= - z[2] + z[29];
    z[18]=z[29]*z[18];
    z[18]=z[39] + z[18];
    z[18]=z[18]*z[21];
    z[29]=z[5] + z[11];
    z[22]=z[29]*z[22];
    z[39]=z[5]*z[10];
    z[40]=z[39]*z[3];
    z[41]=z[40]*z[6];
    z[17]=z[17]*z[41];
    z[17]=4*z[22] - 53*z[17];
    z[22]=n<T>(1,3)*z[6];
    z[17]=z[17]*z[22];
    z[23]=z[2]*z[23];
    z[17]=z[17] + z[18] - 4*z[23];
    z[17]=z[17]*z[22];
    z[18]=4*z[10];
    z[23]=17*z[2] + z[18];
    z[23]=z[23]*z[3]*z[5];
    z[16]=z[17] + n<T>(2,9)*z[23] + z[33] - n<T>(1,3)*z[16];
    z[16]=z[1]*z[16];
    z[14]= - z[36] + 11*z[14];
    z[14]=z[7]*z[14];
    z[17]=n<T>(2,9)*z[11];
    z[23]=n<T>(1,3)*z[10];
    z[14]=n<T>(1,9)*z[14] - n<T>(25,9)*z[5] + z[23] - z[38] + z[17];
    z[14]=z[7]*z[14];
    z[33]=2*z[10];
    z[42]= - z[2] + z[33];
    z[42]=z[7]*z[42];
    z[42]= - static_cast<T>(4)+ z[42];
    z[42]=z[42]*z[19];
    z[14]=n<T>(4,9)*z[42] - n<T>(2,9) + z[14];
    z[14]=z[4]*z[14];
    z[42]= - n<T>(47,3)*z[5] - 29*z[2] - n<T>(2,3)*z[11];
    z[14]=z[14] - n<T>(13,9)*z[3] + n<T>(1,3)*z[42] + z[15];
    z[14]=z[4]*z[14];
    z[15]=z[7]*z[2];
    z[42]= - z[34]*z[15];
    z[43]=z[13]*z[25];
    z[42]=z[43] + z[42];
    z[42]=z[42]*z[32];
    z[43]=npow(z[12],2);
    z[44]=z[43]*z[5];
    z[45]=z[11]*npow(z[8],2);
    z[46]=z[45] + n<T>(4,3)*z[44];
    z[47]=2*z[3];
    z[46]=z[46]*z[47];
    z[47]= - z[25] + 4*z[24];
    z[47]=z[47]*z[32];
    z[48]=z[4]*npow(z[7],2);
    z[47]=z[47] - z[48];
    z[47]=z[4]*z[47];
    z[42]=z[47] + z[42] + z[46];
    z[46]=npow(z[9],2);
    z[42]=z[42]*z[46];
    z[47]=2*z[2];
    z[49]= - z[19]*z[47];
    z[50]=n<T>(4,3)*z[5];
    z[51]=7*z[2];
    z[49]=z[49] - z[50] - z[51] - z[28];
    z[52]=2*z[4];
    z[49]=z[49]*z[52];
    z[38]= - n<T>(23,3)*z[5] - z[38] + 4*z[11];
    z[38]=z[3]*z[38];
    z[38]=z[38] + z[49];
    z[38]=z[4]*z[38];
    z[49]=5*z[25];
    z[53]=z[5]*z[12];
    z[54]=z[11]*z[8];
    z[55]= - 26*z[53] + n<T>(16,3)*z[54] + z[49];
    z[55]=z[3]*z[55];
    z[56]=z[2]*z[13];
    z[57]= - z[56] - n<T>(16,3)*z[39];
    z[57]=z[7]*z[57];
    z[55]=z[57] + z[55];
    z[55]=z[55]*z[46];
    z[38]=z[55] - n<T>(29,3)*z[40] + z[38];
    z[55]=z[11]*z[2];
    z[57]= - 4*z[55] - 65*z[39];
    z[58]=5*z[10];
    z[59]=z[34] - z[58];
    z[59]=n<T>(2,3)*z[59] + 13*z[5];
    z[59]=z[3]*z[59];
    z[57]=n<T>(1,9)*z[57] + z[59];
    z[57]=z[57]*z[46];
    z[29]=z[29]*z[3];
    z[21]=z[21]*z[29];
    z[21]=n<T>(2,3)*z[21] + z[57];
    z[21]=z[6]*z[21];
    z[21]=n<T>(1,3)*z[38] + z[21];
    z[21]=z[6]*z[21];
    z[38]= - z[2]*z[35];
    z[30]=z[38] - z[30];
    z[38]=10*z[2] - n<T>(13,3)*z[10];
    z[38]=z[5]*z[38];
    z[30]=n<T>(2,3)*z[30] + z[38];
    z[38]=z[2]*z[12];
    z[57]=static_cast<T>(50)+ 13*z[38];
    z[57]=z[5]*z[57];
    z[57]=n<T>(1,9)*z[57] - n<T>(4,9)*z[10] - n<T>(13,9)*z[2] + z[35];
    z[57]=z[3]*z[57];
    z[14]=z[16] + z[21] + z[42] + z[14] + n<T>(1,3)*z[30] + z[57];
    z[14]=z[1]*z[14];
    z[16]=z[51] - z[34];
    z[21]=z[48]*z[2];
    z[15]= - 13*z[15] - n<T>(4,3)*z[21];
    z[15]=z[4]*z[15];
    z[15]=z[15] + n<T>(5,3)*z[5] - z[16];
    z[15]=z[4]*z[15];
    z[30]=z[55] + z[36];
    z[36]=19*z[10];
    z[42]= - z[47] - z[11];
    z[42]= - 118*z[5] + 2*z[42] + z[36];
    z[42]=z[42]*z[20];
    z[29]=z[29]*z[52];
    z[29]=11*z[40] + z[29];
    z[29]=z[6]*z[29];
    z[15]=n<T>(5,3)*z[29] + z[15] + z[42] + n<T>(2,3)*z[30] + 17*z[39];
    z[15]=z[15]*z[22];
    z[29]= - z[13]*z[35];
    z[30]=z[10]*z[34];
    z[34]=n<T>(4,3)*z[10] + n<T>(2,3)*z[2] + z[11];
    z[34]=z[5]*z[34];
    z[29]=2*z[34] + z[30] + z[56] + z[29];
    z[29]=z[29]*z[32];
    z[30]=n<T>(4,3)*z[7];
    z[31]= - z[31]*z[30];
    z[16]=z[31] + z[50] - 2*z[16] + z[10];
    z[16]=z[7]*z[16];
    z[31]= - 11*z[2] + z[33];
    z[31]=z[31]*z[32];
    z[31]=z[31] - n<T>(19,3) - z[25];
    z[31]=z[7]*z[31];
    z[33]= - static_cast<T>(2)+ z[26];
    z[33]=z[33]*z[48];
    z[31]=z[31] + n<T>(4,3)*z[33];
    z[31]=z[4]*z[31];
    z[16]=z[31] + z[16] - n<T>(47,3) - z[25];
    z[16]=z[4]*z[16];
    z[31]= - static_cast<T>(1)+ n<T>(1,3)*z[38];
    z[31]= - n<T>(2,3)*z[25] + 4*z[31] - n<T>(11,3)*z[54];
    z[33]=z[2]*z[43];
    z[33]=71*z[12] + 8*z[33];
    z[27]=z[33]*z[27];
    z[27]=2*z[31] + z[27];
    z[27]=z[27]*z[20];
    z[14]=z[14] + z[15] + n<T>(1,3)*z[16] + z[27] + z[29] - n<T>(28,9)*z[5] + 11.
   /9.*z[10] + z[17] - n<T>(2,3)*z[13] - 3*z[2];
    z[14]=z[1]*z[14];
    z[15]= - z[11]*z[32];
    z[15]=z[15] - n<T>(1,3) + z[54];
    z[15]=z[7]*z[15];
    z[16]=z[8] - z[12];
    z[15]=z[15] + n<T>(2,3)*z[16] - z[45];
    z[17]=npow(z[8],3);
    z[27]=z[17]*z[35];
    z[29]=5*z[12];
    z[31]=2*z[8];
    z[33]=z[29] - z[31];
    z[33]=z[8]*z[33];
    z[34]=2*z[43];
    z[33]=z[27] + z[34] + z[33];
    z[33]=z[33]*z[20];
    z[15]=2*z[15] + z[33];
    z[33]=n<T>(4,3)*z[9];
    z[15]=z[15]*z[33];
    z[38]= - z[56] + n<T>(2,3)*z[55];
    z[38]=z[7]*z[38];
    z[18]=z[38] - z[37] - z[18] - z[2] - n<T>(10,3)*z[11];
    z[18]=z[7]*z[18];
    z[37]=2*z[25];
    z[38]= - static_cast<T>(19)+ 7*z[54];
    z[38]=n<T>(1,3)*z[38] + z[37];
    z[18]=z[18] + 2*z[38] + n<T>(23,3)*z[53];
    z[38]=4*z[8];
    z[42]= - n<T>(7,9)*z[44] - n<T>(2,3)*z[45] + n<T>(23,9)*z[12] + z[38];
    z[42]=z[3]*z[42];
    z[15]=z[15] + n<T>(2,3)*z[21] + n<T>(1,3)*z[18] + z[42];
    z[15]=z[9]*z[15];
    z[18]=2*z[44] + z[45] - z[29] - z[38];
    z[18]=z[18]*z[20];
    z[21]= - z[32]*z[55];
    z[21]=z[21] + z[11] + n<T>(2,3)*z[5];
    z[21]=z[7]*z[21];
    z[18]=z[18] + z[21] - n<T>(4,3)*z[53] + static_cast<T>(2)- z[54];
    z[18]=z[18]*z[33];
    z[21]=z[39]*z[30];
    z[29]=n<T>(43,3)*z[53] - n<T>(43,3) - z[49];
    z[29]=z[29]*z[20];
    z[18]=z[18] + z[29] + z[21] + 3*z[10] - n<T>(64,9)*z[5];
    z[18]=z[9]*z[18];
    z[21]= - z[39]*z[32];
    z[29]= - 5*z[53] + static_cast<T>(4)+ z[25];
    z[29]=z[29]*z[20];
    z[23]= - z[23] + z[5];
    z[21]=z[29] + 2*z[23] + z[21];
    z[23]=4*z[9];
    z[21]=z[21]*z[23];
    z[29]=z[36] - 59*z[5];
    z[29]=z[3]*z[29];
    z[29]=35*z[39] + z[29];
    z[21]=n<T>(1,3)*z[29] + z[21];
    z[21]=z[9]*z[21];
    z[29]= - z[10] + 4*z[5];
    z[29]=z[3]*z[29];
    z[29]= - 2*z[39] + z[29];
    z[23]=z[29]*z[23];
    z[23]=23*z[40] + z[23];
    z[23]=z[9]*z[23];
    z[29]=z[46]*z[41];
    z[23]=z[23] - 4*z[29];
    z[23]=z[23]*z[22];
    z[21]=z[21] + z[23];
    z[21]=z[21]*z[22];
    z[18]=z[18] + z[21];
    z[18]=z[6]*z[18];
    z[21]=z[2] - z[11];
    z[21]=n<T>(19,3)*z[3] + n<T>(44,3)*z[5] + n<T>(4,3)*z[21] - z[58];
    z[15]=z[18] + n<T>(2,3)*z[21] + z[15];
    z[15]=z[6]*z[15];
    z[18]= - z[13] + z[28];
    z[21]= - z[8]*z[13];
    z[21]=static_cast<T>(4)+ z[21];
    z[21]=z[10]*z[21];
    z[22]=z[13]*z[24];
    z[18]=z[22] + 2*z[18] + z[21];
    z[18]=z[7]*z[18];
    z[21]= - z[8]*z[35];
    z[21]=static_cast<T>(1)+ z[21];
    z[18]=z[18] + n<T>(5,3)*z[21] - 4*z[25];
    z[18]=z[7]*z[18];
    z[21]= - 7*z[12] + z[31];
    z[21]=z[8]*z[21];
    z[21]= - z[27] + n<T>(1,3)*z[43] + z[21];
    z[21]=z[3]*z[21];
    z[22]=z[12] - z[38];
    z[22]=5*z[22] + 14*z[45];
    z[18]=z[21] + n<T>(1,3)*z[22] + z[18];
    z[21]= - z[32]*z[54];
    z[22]=n<T>(1,3)*z[8];
    z[21]=z[21] - z[22] + z[45];
    z[21]=z[7]*z[21];
    z[23]= - z[12] + z[31];
    z[22]=z[23]*z[22];
    z[16]= - z[8]*z[16];
    z[16]= - z[34] + z[16];
    z[16]=z[8]*z[16];
    z[23]=z[11]*npow(z[8],4);
    z[16]=z[16] + z[23];
    z[16]=z[16]*z[20];
    z[17]= - z[11]*z[17];
    z[16]=z[16] + z[21] + z[22] + z[17];
    z[16]=z[16]*z[33];
    z[17]= - n<T>(1,3) + z[26];
    z[17]=z[17]*z[48];
    z[16]=z[16] + n<T>(1,3)*z[18] + z[17];
    z[16]=z[9]*z[16];
    z[17]= - z[12]*z[51];
    z[17]= - 20*z[54] - static_cast<T>(22)+ z[17];
    z[17]= - n<T>(31,3)*z[53] + n<T>(1,3)*z[17] - z[37];
    z[18]= - 2*z[12] - 11*z[8];
    z[18]=z[18]*z[20];
    z[20]=z[47] + 7*z[11];
    z[20]=n<T>(10,9)*z[5] + n<T>(1,9)*z[20] + z[10];
    z[20]=z[7]*z[20];
    z[14]=z[14] + z[15] + z[16] - 6*z[19] + z[18] + n<T>(1,3)*z[17] + 2*
    z[20];

    r += 4*z[14];
 
    return r;
}

template double qg_2lNLC_r1442(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1442(const std::array<dd_real,31>&);
#endif
