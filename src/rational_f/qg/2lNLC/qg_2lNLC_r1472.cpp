#include "qg_2lNLC_rf_decl.hpp"

template<class T>
T qg_2lNLC_r1472(const std::array<T,31>& k) {
  T z[51];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[5];
    z[4]=k[6];
    z[5]=k[14];
    z[6]=k[15];
    z[7]=k[4];
    z[8]=k[8];
    z[9]=k[13];
    z[10]=k[9];
    z[11]=k[10];
    z[12]=k[3];
    z[13]=k[7];
    z[14]=k[12];
    z[15]=n<T>(1,3)*z[13];
    z[16]=2*z[5];
    z[17]=npow(z[4],2);
    z[18]= - z[2]*z[17];
    z[18]= - z[16] + z[18] + z[15];
    z[19]=npow(z[5],2);
    z[18]=z[18]*z[19];
    z[20]=z[6]*z[2];
    z[21]=z[17]*z[16]*z[20];
    z[18]=z[18] + z[21];
    z[21]=z[14]*z[13];
    z[22]=3*z[21];
    z[23]= - z[17] + z[22];
    z[24]=npow(z[9],2);
    z[23]=z[23]*z[24];
    z[25]=z[14]*z[6];
    z[26]= - z[10]*z[14];
    z[26]=4*z[25] + z[26];
    z[26]=z[26]*npow(z[10],2);
    z[27]=z[17]*z[5];
    z[27]=n<T>(11,3)*z[27];
    z[28]=z[6]*z[27];
    z[23]=n<T>(4,3)*z[26] + z[28] + z[23];
    z[26]=2*z[3];
    z[23]=z[23]*z[26];
    z[28]=z[8]*z[13];
    z[29]=2*z[8];
    z[30]=z[6]*z[29];
    z[30]=z[28] + z[30];
    z[30]=z[14]*z[30];
    z[31]=5*z[12];
    z[32]=5*z[7] + z[31];
    z[32]=z[17]*z[32];
    z[32]= - 3*z[14] + 2*z[13] + z[32];
    z[32]=z[9]*z[32];
    z[22]= - z[22] + z[32];
    z[32]=2*z[9];
    z[22]=z[22]*z[32];
    z[33]=n<T>(4,3)*z[10];
    z[34]=4*z[8];
    z[35]= - z[34] - z[10];
    z[35]=z[35]*z[33];
    z[35]=9*z[25] + z[35];
    z[36]=2*z[10];
    z[35]=z[35]*z[36];
    z[18]=z[23] + z[35] + z[22] + 11*z[18] + 6*z[30];
    z[18]=z[3]*z[18];
    z[22]=z[7]*z[13];
    z[23]=npow(z[7],2);
    z[30]= - z[5]*z[13]*z[23];
    z[30]= - z[22] + z[30];
    z[30]=z[5]*z[30];
    z[30]= - z[13] + z[30];
    z[30]=z[5]*z[30];
    z[35]=z[6]*z[13];
    z[30]=z[35] + z[30];
    z[30]=z[11]*z[30];
    z[35]= - z[22]*z[16];
    z[35]= - z[13] + z[35];
    z[35]=z[35]*z[19];
    z[30]=z[35] + z[30];
    z[35]=z[11]*z[8];
    z[37]=n<T>(1,3)*z[11];
    z[38]=z[8] - z[37];
    z[38]=z[6]*z[38];
    z[38]=z[38] + z[28] + z[35];
    z[39]=2*z[14];
    z[38]=z[38]*z[39];
    z[40]=npow(z[2],2);
    z[41]= - z[40]*z[35];
    z[42]=z[2]*z[29];
    z[41]=z[42] + z[41];
    z[42]=n<T>(1,3)*z[10];
    z[41]=z[41]*z[42];
    z[43]=z[35]*z[2];
    z[41]=z[41] - z[8] + z[43];
    z[41]=z[41]*z[36];
    z[35]=z[41] + z[35] - z[25];
    z[35]=z[35]*z[36];
    z[41]=z[40]*z[6];
    z[44]=npow(z[5],3);
    z[45]=z[44]*z[41];
    z[23]=z[23]*npow(z[9],3)*z[29];
    z[23]=n<T>(11,3)*z[45] + z[23];
    z[23]=z[1]*npow(z[3],2)*z[23]*npow(z[4],4);
    z[45]=npow(z[12],2);
    z[46]=npow(z[4],3);
    z[47]=z[45]*z[46];
    z[48]=z[46]*z[7];
    z[49]=z[12]*z[48];
    z[49]= - z[14] + 2*z[47] + z[49];
    z[49]=z[7]*z[49];
    z[50]=z[46]*npow(z[12],3);
    z[49]= - static_cast<T>(1)+ z[50] + z[49];
    z[49]=z[9]*z[49];
    z[49]= - z[14] + z[49];
    z[49]=z[9]*z[49];
    z[49]= - z[21] + z[49];
    z[49]=z[49]*z[32];
    z[23]=z[23] + z[35] + z[49] + n<T>(11,3)*z[30] + z[38];
    z[30]=z[46]*z[12];
    z[35]= - z[21] + z[30] + 2*z[48];
    z[32]=z[35]*z[32];
    z[35]= - z[8]*z[48];
    z[32]=z[35] + z[32];
    z[32]=z[32]*z[24];
    z[35]=z[19]*z[46];
    z[38]=z[20]*z[35];
    z[46]=npow(z[10],3);
    z[49]=z[46]*z[25];
    z[32]=n<T>(4,3)*z[49] - n<T>(22,3)*z[38] + z[32];
    z[32]=z[32]*z[26];
    z[30]= - 2*z[30] - z[48];
    z[30]=z[7]*z[30];
    z[30]= - z[47] + z[30];
    z[30]=2*z[30] + z[14];
    z[30]=z[9]*z[30];
    z[30]=z[21] + z[30];
    z[24]=z[30]*z[24];
    z[30]= - z[35]*z[41];
    z[35]=z[13]*z[44];
    z[30]= - n<T>(2,3)*z[35] + z[30];
    z[35]=z[8]*z[46];
    z[24]=z[32] - n<T>(8,3)*z[35] + 11*z[30] + 4*z[24];
    z[24]=z[3]*z[24];
    z[23]=z[24] + 2*z[23];
    z[23]=z[1]*z[23];
    z[24]=z[5]*z[7];
    z[24]=z[24] + 1;
    z[30]=z[22] - 1;
    z[32]=11*z[5];
    z[24]=z[32]*z[30]*z[24];
    z[24]= - z[29] + z[24];
    z[24]=z[11]*z[24];
    z[27]=z[40]*z[27];
    z[35]=static_cast<T>(9)- 11*z[22];
    z[35]=z[11]*z[35];
    z[27]=z[35] + z[27] - n<T>(11,3)*z[13] + z[34];
    z[27]=z[6]*z[27];
    z[35]=3*z[2];
    z[38]=z[8]*z[40];
    z[38]= - z[35] - n<T>(1,3)*z[38];
    z[38]=z[11]*z[38];
    z[40]=z[40]*z[11];
    z[41]= - z[2] + 2*z[40];
    z[41]=z[41]*z[42];
    z[44]=z[8]*z[2];
    z[46]=static_cast<T>(1)+ 5*z[44];
    z[38]=z[41] + n<T>(1,3)*z[46] + z[38];
    z[41]=4*z[10];
    z[38]=z[38]*z[41];
    z[38]=z[38] + 4*z[14] - 3*z[6] + z[34] - z[43];
    z[36]=z[38]*z[36];
    z[16]= - z[7]*z[16];
    z[16]=z[16] - static_cast<T>(1)+ n<T>(4,3)*z[22];
    z[16]=z[5]*z[16];
    z[15]=z[15] + z[16];
    z[15]=z[15]*z[32];
    z[16]=z[7]*z[8];
    z[38]= - n<T>(4,3) - z[16];
    z[38]=z[11]*z[38];
    z[43]=z[7]*z[37];
    z[43]=static_cast<T>(2)+ z[43];
    z[43]=z[6]*z[43];
    z[38]=z[43] + 3*z[8] + z[38];
    z[38]=z[38]*z[39];
    z[43]=z[7]*z[12];
    z[43]=z[43] + z[45];
    z[17]= - z[17]*z[43];
    z[17]= - static_cast<T>(2)+ z[17];
    z[45]=3*z[9];
    z[46]=z[12]*z[45];
    z[17]=2*z[17] + z[46];
    z[17]=z[9]*z[17];
    z[17]= - z[13] + z[17];
    z[46]=4*z[9];
    z[17]=z[17]*z[46];
    z[47]=z[13]*z[34];
    z[15]=z[23] + z[18] + z[36] + z[17] + z[38] + z[27] + z[24] + z[47]
    + z[15];
    z[15]=z[1]*z[15];
    z[17]=z[14]*z[12];
    z[18]= - z[10]*z[12];
    z[18]=z[18] - static_cast<T>(5)+ 4*z[17];
    z[18]=z[10]*z[18];
    z[18]=z[18] + z[39] + z[29] + 5*z[6];
    z[18]=z[18]*z[33];
    z[23]=z[34]*z[6];
    z[23]=z[23] + z[28];
    z[24]=z[14]*z[23];
    z[27]= - z[10]*z[17];
    z[27]= - 5*z[14] + z[27];
    z[27]=z[27]*z[42];
    z[25]=z[25] + z[27];
    z[25]=z[25]*z[41];
    z[21]= - z[9]*z[21];
    z[21]=z[25] + z[24] + z[21];
    z[21]=z[3]*z[21];
    z[24]= - z[5]*z[2];
    z[24]=n<T>(1,3) + z[24];
    z[19]=z[24]*z[19];
    z[24]=z[14]*z[8];
    z[25]=z[12] + z[7];
    z[25]=z[9]*z[25];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[9]*z[25];
    z[25]= - z[13] + 6*z[25];
    z[25]=z[9]*z[25];
    z[18]=z[21] + z[18] + z[25] + z[24] + 11*z[19] + z[23];
    z[18]=z[18]*z[26];
    z[19]= - z[2] - z[40];
    z[19]=z[42]*z[12]*z[19];
    z[21]=z[12] - z[2];
    z[23]=n<T>(1,3)*z[2] + 2*z[12];
    z[23]=z[11]*z[2]*z[23];
    z[19]=z[19] + n<T>(5,3)*z[21] + z[23];
    z[19]=z[10]*z[19];
    z[20]=z[20] - static_cast<T>(7)+ z[44];
    z[21]= - z[11]*z[12];
    z[17]=z[19] + n<T>(2,3)*z[17] + z[21] - n<T>(1,3)*z[20];
    z[17]=z[10]*z[17];
    z[19]=static_cast<T>(2)- z[22];
    z[19]=z[7]*z[19];
    z[19]= - z[2] + z[19];
    z[19]=z[19]*z[32];
    z[19]=static_cast<T>(4)+ z[19];
    z[19]=z[19]*z[37];
    z[20]=n<T>(11,3)*z[22];
    z[21]= - static_cast<T>(6)+ z[20];
    z[21]=z[7]*z[21];
    z[21]=z[35] + z[21];
    z[21]=z[11]*z[21];
    z[20]=z[21] + static_cast<T>(3)+ z[20];
    z[20]=z[6]*z[20];
    z[16]= - n<T>(2,3) - z[16];
    z[16]=z[16]*z[39];
    z[21]= - z[43]*z[45];
    z[21]=z[21] + z[31] + 2*z[7];
    z[21]=z[9]*z[21];
    z[21]= - static_cast<T>(2)+ z[21];
    z[21]=z[21]*z[46];
    z[22]=z[5]*z[30];
    z[15]=z[15] + z[18] + 8*z[17] + z[21] + z[16] + z[20] + z[19] - 
    z[29] - n<T>(11,3)*z[22];
    z[15]=z[1]*z[15];
    z[16]=z[6] - z[5];
    z[17]= - z[7] + 2*z[2];
    z[17]= - z[17]*z[16];
    z[16]= - n<T>(11,3)*z[16] - z[9];
    z[16]=z[16]*z[26];
    z[18]=z[12]*z[46];

    r += z[15] + z[16] + n<T>(11,3)*z[17] + z[18];
 
    return r;
}

template double qg_2lNLC_r1472(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qg_2lNLC_r1472(const std::array<dd_real,31>&);
#endif
