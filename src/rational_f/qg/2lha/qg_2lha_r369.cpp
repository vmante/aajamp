#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r369(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[5];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[3];
    z[7]=k[12];
    z[8]=k[9];
    z[9]=npow(z[6],2);
    z[10]=npow(z[2],2);
    z[10]=z[9] - z[10];
    z[10]=z[5] + n<T>(1,2)*z[10];
    z[10]=z[2]*z[10];
    z[11]= - static_cast<T>(1)+ z[9];
    z[10]=z[10] + n<T>(1,2)*z[11] + z[5];
    z[10]=z[7]*z[10];
    z[11]=n<T>(1,2)*z[5];
    z[12]=z[11] + 1;
    z[12]=z[12]*z[5];
    z[13]=z[6] - 1;
    z[14]=npow(z[5],2);
    z[15]=n<T>(1,2)*z[14];
    z[16]= - z[15] + z[2];
    z[16]=z[2]*z[16];
    z[10]=z[10] + z[16] - n<T>(1,2)*z[13] - z[12];
    z[10]=z[7]*z[10];
    z[16]=n<T>(1,2)*z[2];
    z[17]= - static_cast<T>(1)- z[6];
    z[18]= - n<T>(5,4) + z[5];
    z[18]=z[5]*z[18];
    z[17]= - z[16] + n<T>(1,2)*z[17] + z[18];
    z[17]=z[2]*z[17];
    z[18]= - n<T>(5,2) + 3*z[5];
    z[18]=z[5]*z[18];
    z[18]=z[18] - z[13];
    z[10]=z[10] + n<T>(1,2)*z[18] + z[17];
    z[10]=z[7]*z[10];
    z[17]= - static_cast<T>(1)+ n<T>(1,2)*z[6];
    z[18]= - n<T>(3,2) + z[6];
    z[18]=z[5]*z[18];
    z[18]=z[18] - z[17];
    z[18]=z[5]*z[18];
    z[19]=z[16] - n<T>(1,2)*z[1];
    z[19]=z[3]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[2]*z[19];
    z[18]=z[19] + n<T>(1,2) + z[18];
    z[10]=n<T>(1,2)*z[18] + z[10];
    z[18]=z[5] - 1;
    z[13]= - z[6]*z[13];
    z[13]=z[13] + z[18];
    z[19]= - static_cast<T>(3)- z[6];
    z[19]=z[6]*z[19];
    z[19]=z[19] + z[5];
    z[20]= - z[16] - n<T>(1,2) - z[6];
    z[20]=z[2]*z[20];
    z[19]=n<T>(1,2)*z[19] + z[20];
    z[19]=z[2]*z[19];
    z[19]=z[19] + z[5] - n<T>(1,2) - z[9];
    z[19]=z[2]*z[19];
    z[13]=n<T>(1,2)*z[13] + z[19];
    z[13]=z[7]*z[13];
    z[19]=z[11] - 1;
    z[20]= - z[19]*z[11];
    z[20]=z[2] + z[20] + n<T>(1,4) + z[6];
    z[20]=z[2]*z[20];
    z[15]=z[20] - z[15] - n<T>(1,2) + z[6];
    z[15]=z[2]*z[15];
    z[20]=n<T>(1,2) - z[12];
    z[13]=z[13] + n<T>(1,2)*z[20] + z[15];
    z[13]=z[7]*z[13];
    z[15]=n<T>(3,2)*z[5];
    z[20]=z[18]*z[15];
    z[20]=static_cast<T>(1)+ z[20];
    z[21]=z[5] - 3;
    z[22]=z[2]*z[21];
    z[19]=n<T>(1,8)*z[22] + z[19];
    z[19]=z[5]*z[19];
    z[19]=n<T>(1,2) + z[19];
    z[19]=z[2]*z[19];
    z[13]=n<T>(1,2)*z[13] + n<T>(1,4)*z[20] + z[19];
    z[13]=z[7]*z[13];
    z[19]= - z[21]*z[11];
    z[20]=z[2] + 1;
    z[21]= - z[1] + z[20];
    z[21]=z[3]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[16];
    z[19]=z[21] - static_cast<T>(1)+ z[19];
    z[19]=z[2]*z[19];
    z[17]=z[5]*z[17]*z[18];
    z[17]=z[17] + z[19];
    z[13]=n<T>(1,4)*z[17] + z[13];
    z[13]=z[4]*z[13];
    z[10]=n<T>(1,2)*z[10] + z[13];
    z[10]=z[4]*z[10];
    z[9]= - z[9]*z[8]*z[20];
    z[9]=z[5] + z[9];
    z[9]=z[7]*z[9];
    z[13]=z[6]*z[8];
    z[9]=z[9] + z[13] - z[12];
    z[9]=z[7]*z[9];
    z[12]=z[13]*z[20];
    z[13]= - static_cast<T>(1)+ z[15];
    z[13]=z[5]*z[13];
    z[9]=z[9] + z[13] + z[12];
    z[9]=z[7]*z[9];
    z[12]= - z[5]*z[1];
    z[12]=static_cast<T>(1)+ z[12];
    z[11]=z[12]*z[11];
    z[12]=z[14]*z[16];
    z[9]=z[9] + z[12] - z[8] + z[11];
    z[9]=n<T>(1,4)*z[9] + z[10];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r369(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r369(const std::array<dd_real,30>&);
#endif
