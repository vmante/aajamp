#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1517(const std::array<T,30>& k) {
  T z[34];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=k[9];
    z[8]=k[11];
    z[9]=k[3];
    z[10]=k[12];
    z[11]=z[10]*z[9];
    z[12]=z[9]*z[8];
    z[13]= - z[12] + z[11];
    z[14]=n<T>(1,4)*z[5];
    z[13]=z[13]*z[14];
    z[15]=n<T>(1,2)*z[12];
    z[16]= - static_cast<T>(5)+ z[15];
    z[13]=z[13] + n<T>(1,2)*z[16] + z[11];
    z[13]=z[5]*z[13];
    z[16]=n<T>(1,4)*z[12];
    z[13]=z[13] + n<T>(3,2)*z[11] - static_cast<T>(5)- z[16];
    z[13]=z[5]*z[13];
    z[17]=z[12] - 1;
    z[18]=n<T>(1,4)*z[6];
    z[19]=z[17]*z[18];
    z[20]=n<T>(1,2)*z[4];
    z[21]=n<T>(1,2)*z[9];
    z[22]=z[21]*z[10];
    z[23]= - static_cast<T>(3)+ z[22];
    z[23]=z[23]*z[20];
    z[13]=z[23] + z[13] + z[19] + z[11] - static_cast<T>(5)+ z[16];
    z[16]=n<T>(1,4)*z[7];
    z[13]=z[13]*z[16];
    z[23]=static_cast<T>(3)- z[21];
    z[23]=z[10]*z[23];
    z[24]=z[10] - z[8];
    z[25]=z[5]*z[24];
    z[23]=z[25] + z[12] + z[23];
    z[23]=z[5]*z[23];
    z[21]=z[21] - 1;
    z[21]=z[21]*z[10];
    z[23]=n<T>(1,8)*z[23] - n<T>(3,8)*z[21] + static_cast<T>(1)- n<T>(1,16)*z[12];
    z[23]=z[5]*z[23];
    z[25]=n<T>(1,8)*z[6];
    z[17]= - z[17]*z[25];
    z[26]=static_cast<T>(1)- n<T>(3,2)*z[9];
    z[26]=z[10]*z[26];
    z[17]=z[17] + static_cast<T>(3)+ n<T>(1,4)*z[26];
    z[11]=static_cast<T>(11)- z[11];
    z[11]=z[4]*z[11];
    z[11]=z[13] + n<T>(1,16)*z[11] + n<T>(1,2)*z[17] + z[23];
    z[11]=z[7]*z[11];
    z[13]= - static_cast<T>(9)+ z[15];
    z[13]=z[19] + n<T>(1,2)*z[13] + z[21];
    z[17]=n<T>(1,2)*z[10];
    z[19]=z[17] - z[8];
    z[19]=z[19]*z[5];
    z[21]= - static_cast<T>(1)+ n<T>(1,8)*z[9];
    z[21]=z[10]*z[21];
    z[12]= - z[19] - n<T>(3,8)*z[12] + z[21];
    z[12]=z[5]*z[12];
    z[21]= - static_cast<T>(7)+ z[22];
    z[21]=z[4]*z[21];
    z[12]=n<T>(1,4)*z[21] + n<T>(1,2)*z[13] + z[12];
    z[11]=n<T>(1,4)*z[12] + z[11];
    z[11]=z[7]*z[11];
    z[12]= - 3*z[8] + z[10];
    z[12]=z[5]*z[12];
    z[12]=z[12] + z[15] + z[10];
    z[12]=n<T>(1,2)*z[12] + z[4];
    z[11]=n<T>(1,8)*z[12] + z[11];
    z[11]=z[7]*z[11];
    z[12]=31*z[4];
    z[13]=n<T>(19,2) + z[5];
    z[13]=z[5]*z[13];
    z[13]=static_cast<T>(19)+ z[13];
    z[13]=3*z[13] + z[12];
    z[15]=n<T>(1,2)*z[2];
    z[12]= - 9*z[6] - z[12];
    z[12]=z[12]*z[15];
    z[21]=static_cast<T>(7)+ n<T>(31,2)*z[4];
    z[12]=3*z[21] + z[12];
    z[12]=z[2]*z[12];
    z[21]= - 93*z[4] - n<T>(213,2) - 23*z[5];
    z[12]=n<T>(1,2)*z[21] + z[12];
    z[12]=z[2]*z[12];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[13]=n<T>(9,2)*z[4];
    z[21]=5*z[5];
    z[22]= - z[13] - n<T>(19,2) - z[21];
    z[23]=9*z[4];
    z[26]=z[6] - z[23];
    z[26]=z[26]*z[15];
    z[13]=static_cast<T>(5)+ z[13];
    z[13]=3*z[13] + z[26];
    z[13]=z[2]*z[13];
    z[13]=3*z[22] + z[13];
    z[13]=z[13]*z[15];
    z[22]=z[21] + n<T>(29,2);
    z[26]=n<T>(1,2)*z[5];
    z[27]=z[22]*z[26];
    z[13]=z[13] + n<T>(9,4)*z[4] + static_cast<T>(7)+ z[27];
    z[13]=z[13]*z[16];
    z[27]=n<T>(1,4)*z[2];
    z[28]=z[6] + 19*z[4];
    z[28]=z[28]*z[27];
    z[28]=z[28] - static_cast<T>(11)- n<T>(57,4)*z[4];
    z[28]=z[2]*z[28];
    z[29]=n<T>(57,2)*z[4] + n<T>(185,4) + 17*z[5];
    z[28]=n<T>(1,2)*z[29] + z[28];
    z[28]=z[28]*z[15];
    z[29]=z[5] + n<T>(9,2);
    z[30]=z[29]*z[5];
    z[13]=z[13] + z[28] - n<T>(19,8)*z[4] - n<T>(47,8) - z[30];
    z[13]=z[7]*z[13];
    z[12]=n<T>(1,4)*z[12] + z[13];
    z[12]=z[7]*z[12];
    z[13]=3*z[4];
    z[28]= - n<T>(1,2) - z[13];
    z[31]=2*z[6] + z[13];
    z[31]=z[2]*z[31];
    z[28]=3*z[28] + z[31];
    z[28]=z[2]*z[28];
    z[31]=n<T>(99,8) + z[5];
    z[23]=z[28] + n<T>(1,2)*z[31] + z[23];
    z[23]=z[2]*z[23];
    z[28]= - static_cast<T>(29)- 7*z[5];
    z[12]=z[12] + z[23] + n<T>(1,8)*z[28] - z[13];
    z[12]=z[7]*z[12];
    z[23]=z[4] + z[6];
    z[23]=z[23]*z[27];
    z[23]=z[23] - z[4];
    z[28]=z[23]*z[2];
    z[31]= - static_cast<T>(1)- z[4];
    z[31]=n<T>(3,2)*z[31] - z[28];
    z[31]=z[31]*z[15];
    z[20]=z[31] + z[20] + static_cast<T>(1)+ z[26];
    z[20]=z[2]*z[20];
    z[31]= - static_cast<T>(3)- z[5];
    z[31]=z[5]*z[31];
    z[31]= - z[4] - static_cast<T>(3)+ z[31];
    z[20]=n<T>(1,8)*z[31] + z[20];
    z[20]=z[7]*z[20];
    z[29]=z[29]*z[26];
    z[31]=n<T>(5,4)*z[4];
    z[29]=z[31] + static_cast<T>(3)+ z[29];
    z[31]=static_cast<T>(1)+ z[31];
    z[28]=3*z[31] + n<T>(5,2)*z[28];
    z[28]=z[2]*z[28];
    z[31]=n<T>(5,2)*z[4];
    z[28]=z[28] - z[31] - static_cast<T>(4)- n<T>(3,2)*z[5];
    z[28]=z[2]*z[28];
    z[20]=z[20] + n<T>(1,2)*z[29] + z[28];
    z[20]=z[7]*z[20];
    z[28]=5*z[2];
    z[23]=z[23]*z[28];
    z[28]=5*z[4];
    z[29]= - static_cast<T>(3)- z[28];
    z[29]=n<T>(3,2)*z[29] - z[23];
    z[29]=z[2]*z[29];
    z[32]=static_cast<T>(2)+ z[26];
    z[29]=z[29] + 3*z[32] + z[28];
    z[29]=z[2]*z[29];
    z[32]= - static_cast<T>(9)- z[5];
    z[32]=z[32]*z[26];
    z[32]= - z[28] - static_cast<T>(9)+ z[32];
    z[20]=z[20] + n<T>(1,4)*z[32] + z[29];
    z[20]=z[7]*z[20];
    z[29]=static_cast<T>(1)+ z[31];
    z[23]=3*z[29] + z[23];
    z[23]=z[2]*z[23];
    z[23]=z[23] - z[28] - static_cast<T>(4)- z[26];
    z[23]=z[2]*z[23];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=3*z[14] + z[31];
    z[14]=z[20] + n<T>(1,2)*z[14] + z[23];
    z[14]=z[7]*z[14];
    z[20]= - static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[20]=z[20]*z[4];
    z[23]=z[5]*z[18];
    z[23]=z[20] + z[23] + n<T>(1,4) - z[6];
    z[23]=z[23]*z[15];
    z[29]=n<T>(1,2)*z[1];
    z[32]=z[29] - 2;
    z[32]=z[32]*z[4];
    z[23]= - z[32] + z[23];
    z[23]=z[2]*z[23];
    z[33]= - n<T>(1,4) + z[20];
    z[23]=3*z[33] + z[23];
    z[23]=z[2]*z[23];
    z[23]=z[23] + static_cast<T>(1)- z[32];
    z[23]=z[2]*z[23];
    z[20]= - n<T>(3,4) + z[20];
    z[14]=z[14] + n<T>(1,2)*z[20] + z[23];
    z[14]=z[3]*z[14];
    z[20]=n<T>(11,2) - z[1];
    z[13]=z[20]*z[13];
    z[20]= - z[2] - n<T>(7,2) + z[1];
    z[20]=z[4]*z[20];
    z[23]=z[5]*z[6];
    z[20]=3*z[23] + static_cast<T>(3)- n<T>(17,2)*z[6] + z[20];
    z[20]=z[2]*z[20];
    z[13]=z[20] - static_cast<T>(1)+ z[13];
    z[13]=z[2]*z[13];
    z[20]= - n<T>(37,2) + 3*z[1];
    z[20]=z[4]*z[20];
    z[13]=z[13] - static_cast<T>(7)+ z[20];
    z[13]=z[2]*z[13];
    z[20]=n<T>(13,2) - z[1];
    z[20]=z[4]*z[20];
    z[13]=z[13] + static_cast<T>(5)+ z[20];
    z[12]=z[14] + n<T>(1,8)*z[13] + z[12];
    z[12]=z[3]*z[12];
    z[13]= - n<T>(11,4) - z[5];
    z[13]=z[13]*z[21];
    z[13]= - n<T>(51,4) + z[13];
    z[14]=n<T>(7,4) + z[5];
    z[20]= - static_cast<T>(5)- z[18];
    z[20]=n<T>(1,4)*z[20] - z[4];
    z[20]=z[2]*z[20];
    z[14]=z[20] + n<T>(5,2)*z[14] + 2*z[4];
    z[14]=z[2]*z[14];
    z[13]=z[14] + n<T>(1,4)*z[13] - z[4];
    z[13]=z[7]*z[13];
    z[14]=n<T>(55,4)*z[4] + static_cast<T>(9)+ z[18];
    z[14]=z[2]*z[14];
    z[14]=z[14] - 3*z[22] - n<T>(55,2)*z[4];
    z[14]=z[14]*z[15];
    z[20]=z[30] + n<T>(23,4);
    z[14]=z[14] + 3*z[20] + n<T>(55,8)*z[4];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[13]=z[7]*z[13];
    z[14]= - n<T>(35,2)*z[4] - n<T>(7,2) - z[6];
    z[14]=z[2]*z[14];
    z[14]=z[14] + 35*z[4] + n<T>(69,2) + z[21];
    z[14]=z[14]*z[27];
    z[22]= - n<T>(21,2) - z[5];
    z[22]=z[5]*z[22];
    z[13]=z[13] + z[14] - n<T>(35,8)*z[4] - static_cast<T>(8)+ n<T>(3,8)*z[22];
    z[13]=z[7]*z[13];
    z[14]=static_cast<T>(23)+ z[21];
    z[14]=n<T>(1,4)*z[14] + z[28];
    z[22]=z[6] + z[31];
    z[22]=z[2]*z[22];
    z[22]=z[22] - n<T>(17,8) - z[28];
    z[22]=z[2]*z[22];
    z[13]=z[13] + n<T>(1,2)*z[14] + z[22];
    z[13]=z[7]*z[13];
    z[14]= - static_cast<T>(3)+ z[1];
    z[14]= - z[15] + n<T>(1,4)*z[14];
    z[14]=z[4]*z[14];
    z[14]=n<T>(7,4)*z[23] + n<T>(5,4) - 3*z[6] + z[14];
    z[14]=z[2]*z[14];
    z[15]=static_cast<T>(3)- z[29];
    z[15]=z[4]*z[15];
    z[14]=z[15] + z[14];
    z[14]=z[2]*z[14];
    z[15]= - static_cast<T>(7)+ z[1];
    z[15]=z[4]*z[15];
    z[15]= - static_cast<T>(5)+ z[15];
    z[14]=n<T>(1,4)*z[15] + z[14];
    z[12]=z[12] + n<T>(1,4)*z[14] + z[13];
    z[12]=z[3]*z[12];
    z[13]=z[24]*z[26];
    z[13]=z[13] - n<T>(1,2)*z[8] + z[10];
    z[13]=z[5]*z[13];
    z[13]=z[13] + static_cast<T>(13)+ z[17];
    z[13]=z[13]*z[26];
    z[14]=n<T>(69,8)*z[4];
    z[15]= - z[14] - static_cast<T>(5)+ z[25];
    z[15]=z[2]*z[15];
    z[13]=z[15] + z[14] + static_cast<T>(15)+ z[13];
    z[14]=n<T>(7,2)*z[4];
    z[15]= - static_cast<T>(15)+ n<T>(1,2)*z[6];
    z[15]= - z[14] + n<T>(1,2)*z[15] - z[21];
    z[15]=z[2]*z[15];
    z[17]=n<T>(5,2) + z[5];
    z[17]=z[5]*z[17];
    z[17]=n<T>(9,4) + z[17];
    z[14]=z[15] + 5*z[17] + z[14];
    z[14]=z[14]*z[16];
    z[15]=n<T>(37,4)*z[4] + 3*z[5] + static_cast<T>(13)- z[18];
    z[15]=z[15]*z[27];
    z[14]=z[14] + z[15] - n<T>(37,16)*z[4] - z[20];
    z[14]=z[7]*z[14];
    z[13]=n<T>(1,4)*z[13] + z[14];
    z[13]=z[7]*z[13];
    z[14]= - n<T>(1,2)*z[24] - z[19];
    z[14]=z[5]*z[14];
    z[15]=7*z[4];
    z[16]=z[6] + z[15];
    z[16]=z[2]*z[16];
    z[14]=z[16] - z[15] - static_cast<T>(7)+ z[14];
    z[13]=n<T>(1,8)*z[14] + z[13];
    z[13]=z[7]*z[13];
    z[14]= - z[27] - n<T>(1,8);
    z[14]=z[4]*z[14];
    z[15]= - n<T>(5,4)*z[6] + static_cast<T>(1)- n<T>(1,4)*z[8];
    z[14]=n<T>(1,2)*z[15] + z[23] + z[14];
    z[14]=z[2]*z[14];
    z[15]=n<T>(3,2)*z[4] + n<T>(3,2) + z[19];
    z[14]=n<T>(1,4)*z[15] + z[14];
    z[12]=z[12] + n<T>(1,4)*z[14] + z[13];
    z[12]=z[3]*z[12];
    z[13]=z[8] + z[6];
    z[13]=n<T>(1,2)*z[13] + z[23];

    r += z[11] + z[12] + n<T>(1,16)*z[13];
 
    return r;
}

template double qg_2lha_r1517(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1517(const std::array<dd_real,30>&);
#endif
