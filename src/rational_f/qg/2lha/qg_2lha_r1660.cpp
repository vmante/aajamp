#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1660(const std::array<T,30>& k) {
  T z[5];
  T r = 0;

    z[1]=k[7];
    z[2]=k[14];
    z[3]=k[9];
    z[4]= - z[1] - z[3];

    r += n<T>(1,6)*z[4]*z[2];
 
    return r;
}

template double qg_2lha_r1660(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1660(const std::array<dd_real,30>&);
#endif
