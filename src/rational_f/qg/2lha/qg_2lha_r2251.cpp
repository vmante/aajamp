#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2251(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=k[9];
    z[7]=k[11];
    z[8]=2*z[2];
    z[9]=z[3]*z[2];
    z[10]=2*z[9] - static_cast<T>(1)+ z[8];
    z[10]=z[3]*z[10];
    z[8]=z[8] + z[9];
    z[8]=z[7]*z[8];
    z[8]=z[8] - 4*z[9] + static_cast<T>(1)- 6*z[2];
    z[8]=z[3]*z[8];
    z[8]=static_cast<T>(2)+ z[8];
    z[8]=z[7]*z[8];
    z[8]=z[8] - static_cast<T>(4)+ 3*z[10];
    z[8]=z[7]*z[8];
    z[10]= - static_cast<T>(3)+ z[1];
    z[9]=z[10]*z[9];
    z[9]=z[9] - 3*z[2] + static_cast<T>(2)- z[1];
    z[9]=z[3]*z[9];
    z[8]=z[8] + static_cast<T>(3)+ z[9];
    z[8]=z[4]*z[8];
    z[9]=npow(z[3],2);
    z[10]=static_cast<T>(2)- z[9];
    z[11]= - static_cast<T>(4)+ z[9];
    z[11]=z[7]*z[11];
    z[10]=3*z[10] + z[11];
    z[10]=z[7]*z[10];
    z[10]=z[10] - static_cast<T>(2)+ 3*z[9];
    z[10]=z[7]*z[10];
    z[8]=z[8] - z[9] + z[10];
    z[8]=z[4]*z[8];
    z[9]=2*z[5];
    z[10]= - static_cast<T>(3)- z[9];
    z[10]=z[6]*z[10];
    z[9]=static_cast<T>(1)+ z[9];
    z[9]=z[6]*z[9];
    z[9]=static_cast<T>(2)+ z[9];
    z[9]=z[7]*z[9];
    z[9]=z[9] - static_cast<T>(2)+ z[10];
    z[9]=z[7]*z[9];
    z[9]=2*z[6] + z[9];
    z[9]=z[7]*z[9];
    z[8]=z[9] + z[8];

    r += 2*z[8];
 
    return r;
}

template double qg_2lha_r2251(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2251(const std::array<dd_real,30>&);
#endif
