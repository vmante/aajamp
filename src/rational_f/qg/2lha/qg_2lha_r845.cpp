#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r845(const std::array<T,30>& k) {
  T z[34];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[4];
    z[6]=k[24];
    z[7]=k[3];
    z[8]=k[27];
    z[9]=k[12];
    z[10]=n<T>(1,9)*z[8];
    z[11]=n<T>(403,2)*z[8];
    z[12]=static_cast<T>(41)- z[11];
    z[12]=z[12]*z[10];
    z[13]=n<T>(1,2)*z[3];
    z[14]= - static_cast<T>(295)+ 563*z[8];
    z[14]=n<T>(1,9)*z[14] + n<T>(9,2)*z[9];
    z[14]=z[14]*z[13];
    z[12]=z[14] + z[9] - n<T>(11,2) + z[12];
    z[12]=z[3]*z[12];
    z[14]=n<T>(9,2)*z[6];
    z[15]=n<T>(3,2) - z[9];
    z[15]=z[15]*z[14];
    z[15]=z[9] + z[15];
    z[15]=z[6]*z[15];
    z[16]=npow(z[8],2);
    z[17]=9*z[6];
    z[18]= - n<T>(11,2) - z[17];
    z[18]=z[6]*z[18];
    z[18]=z[18] - static_cast<T>(5)+ n<T>(9,2)*z[8];
    z[18]=z[4]*z[18];
    z[12]=z[12] + z[18] + n<T>(403,36)*z[16] + z[15];
    z[15]=n<T>(1,4)*z[2];
    z[12]=z[12]*z[15];
    z[18]=n<T>(9,4)*z[8];
    z[19]=z[4]*z[6];
    z[20]= - static_cast<T>(1)- n<T>(17,16)*z[6];
    z[20]=z[6]*z[20];
    z[20]=n<T>(1,16)*z[19] - z[18] + z[20];
    z[20]=z[4]*z[20];
    z[21]= - static_cast<T>(1)+ z[14];
    z[21]=z[21]*z[14];
    z[22]=n<T>(1,2)*z[9];
    z[23]=9*z[8];
    z[21]=z[21] - z[23] - z[22];
    z[24]= - static_cast<T>(3)+ n<T>(565,72)*z[8];
    z[24]=n<T>(1,2)*z[24] - n<T>(38,3)*z[3];
    z[24]=z[3]*z[24];
    z[21]=n<T>(1,4)*z[21] + z[24];
    z[21]=z[3]*z[21];
    z[24]=n<T>(1,8)*z[6];
    z[25]=z[6]*z[9];
    z[26]= - z[9] - 5*z[25];
    z[26]=z[26]*z[24];
    z[12]=z[12] + z[21] + z[26] + z[20];
    z[12]=z[2]*z[12];
    z[20]=3*z[6];
    z[21]= - static_cast<T>(1)- z[20];
    z[21]=z[21]*z[14];
    z[26]=5*z[9];
    z[27]=z[26] + n<T>(563,9)*z[8];
    z[28]=z[27]*z[13];
    z[29]=static_cast<T>(9)- n<T>(403,18)*z[8];
    z[29]=z[8]*z[29];
    z[28]=z[28] + z[29] + z[9];
    z[28]=z[3]*z[28];
    z[29]= - z[23] + n<T>(7,2)*z[9];
    z[21]=z[28] + z[21] - z[29];
    z[21]=z[3]*z[21];
    z[28]=n<T>(1,2)*z[6];
    z[30]= - 7*z[9] - 9*z[25];
    z[30]=z[30]*z[28];
    z[31]= - static_cast<T>(1)- z[17];
    z[31]=z[6]*z[31];
    z[31]=z[23] + z[31];
    z[31]=z[4]*z[31];
    z[21]=z[21] + z[30] + z[31];
    z[21]=z[2]*z[21];
    z[27]=z[3]*z[27];
    z[16]= - n<T>(403,9)*z[16] + z[27];
    z[16]=z[16]*z[13];
    z[16]=z[16] + z[14] + z[29];
    z[16]=z[3]*z[16];
    z[27]=z[23] - z[6];
    z[27]=z[27]*z[4];
    z[27]=z[27] - n<T>(7,2)*z[25];
    z[16]=z[21] + z[16] - z[27];
    z[16]=z[2]*z[16];
    z[21]=z[3]*z[27];
    z[16]=z[21] + z[16];
    z[16]=z[5]*z[16];
    z[21]=n<T>(1,2)*z[4];
    z[27]=static_cast<T>(3)- n<T>(295,36)*z[4];
    z[27]=z[27]*z[21];
    z[29]=z[3]*z[4];
    z[30]=n<T>(38,9)*z[29];
    z[31]=static_cast<T>(2)+ z[4];
    z[31]=z[31]*z[30];
    z[27]=z[27] + z[31];
    z[27]=z[3]*z[27];
    z[31]=n<T>(1,8)*z[4];
    z[18]= - z[31] + z[18] + z[6];
    z[18]=z[4]*z[18];
    z[18]=z[27] + n<T>(1,8)*z[25] + z[18];
    z[18]=z[3]*z[18];
    z[27]=npow(z[6],2);
    z[32]= - z[27] - z[19];
    z[32]=z[4]*z[32];
    z[27]=z[9]*z[27];
    z[27]=z[27] + z[32];
    z[12]=n<T>(1,8)*z[16] + z[12] + n<T>(1,16)*z[27] + z[18];
    z[12]=z[5]*z[12];
    z[16]= - z[22] - z[20];
    z[18]=n<T>(9,4)*z[6];
    z[16]=z[16]*z[18];
    z[27]=z[23] - z[1];
    z[32]=n<T>(421,72)*z[4] - n<T>(1,4)*z[27] + z[17];
    z[32]=z[4]*z[32];
    z[16]=z[16] + z[32];
    z[32]= - 2*z[4] - 1;
    z[33]=z[1] - 1;
    z[29]=z[29]*z[33]*z[32];
    z[32]= - n<T>(823,8) + 38*z[1];
    z[32]=z[4]*z[32];
    z[32]=n<T>(509,16) + z[32];
    z[32]=z[4]*z[32];
    z[29]=z[32] + 38*z[29];
    z[29]=z[3]*z[29];
    z[16]=n<T>(1,2)*z[16] + n<T>(1,9)*z[29];
    z[16]=z[3]*z[16];
    z[29]=n<T>(1,2) + z[6];
    z[29]=z[29]*z[31];
    z[23]= - z[23] - z[1];
    z[31]= - n<T>(19,16) - z[6];
    z[31]=z[6]*z[31];
    z[23]=z[29] + n<T>(1,8)*z[23] + z[31];
    z[23]=z[4]*z[23];
    z[29]= - static_cast<T>(1)+ z[7];
    z[11]=z[29]*z[11];
    z[11]=static_cast<T>(1)+ z[11];
    z[10]=z[11]*z[10];
    z[11]=static_cast<T>(1)+ z[7];
    z[11]=3*z[11] - z[9];
    z[11]=z[11]*z[14];
    z[29]=n<T>(11,2)*z[9];
    z[11]=z[11] + static_cast<T>(9)+ z[29];
    z[11]=z[6]*z[11];
    z[10]=z[11] - n<T>(1,2) + z[10];
    z[11]= - static_cast<T>(295)+ n<T>(563,2)*z[8];
    z[11]=n<T>(3,2)*z[9] + n<T>(1,9)*z[11] + z[1];
    z[11]=z[11]*z[13];
    z[13]= - static_cast<T>(5)- z[14];
    z[13]=z[6]*z[13];
    z[13]= - static_cast<T>(5)+ z[13];
    z[13]=z[4]*z[13];
    z[10]=z[11] + n<T>(1,2)*z[10] + z[13];
    z[10]=z[10]*z[15];
    z[11]=static_cast<T>(27)- z[29];
    z[11]=z[6]*z[11];
    z[11]=z[22] + z[11];
    z[11]=z[11]*z[24];
    z[13]= - n<T>(617,9)*z[3] - 3*z[9] + z[27];
    z[13]=z[3]*z[13];
    z[10]=z[10] + n<T>(1,8)*z[13] + z[11] + z[23];
    z[10]=z[2]*z[10];
    z[11]=n<T>(1,16) + z[6];
    z[11]=z[6]*z[11];
    z[11]=z[11] - n<T>(5,4)*z[19];
    z[11]=z[4]*z[11];
    z[13]= - z[9] + 11*z[25];
    z[13]=z[6]*z[13];
    z[10]=z[12] + z[10] + z[16] + n<T>(1,16)*z[13] + z[11];
    z[10]=z[5]*z[10];
    z[11]=z[9] - 1;
    z[12]=z[9] - z[25];
    z[12]=z[6]*z[12];
    z[13]=z[1] - z[11];
    z[13]=z[2]*z[13];
    z[11]=z[13] + z[12] - z[11];
    z[12]= - static_cast<T>(1)+ z[6];
    z[12]=z[12]*z[28];
    z[13]=z[1] + n<T>(1,2);
    z[13]=z[13]*z[1];
    z[14]=z[28] + n<T>(1,2) + z[13];
    z[14]=z[4]*z[14];
    z[15]=z[33]*z[1];
    z[12]=z[14] + z[12] - n<T>(1,2) + z[15];
    z[12]=z[4]*z[12];
    z[14]=z[3]*z[15];
    z[11]=z[14] + z[12] + n<T>(1,2)*z[11];
    z[11]=z[2]*z[11];
    z[12]= - n<T>(27,2) + z[26];
    z[12]=z[6]*z[12];
    z[12]= - z[26] + z[12];
    z[12]=z[6]*z[12];
    z[14]=static_cast<T>(19)+ 17*z[6];
    z[14]=z[6]*z[14];
    z[14]=z[14] - 19*z[19];
    z[14]=z[14]*z[21];
    z[11]=z[11] + z[12] + z[14];
    z[12]=n<T>(277,18) - z[13];
    z[12]=z[12]*z[21];
    z[13]= - n<T>(143,9) - z[15];
    z[12]=z[12] + n<T>(1,2)*z[13] + z[17];
    z[12]=z[4]*z[12];
    z[13]= - z[1]*z[20];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[13]*z[18];
    z[12]=z[13] + z[12];
    z[13]=z[4] - 1;
    z[14]=z[1] - 2;
    z[14]=z[14]*z[1];
    z[14]=z[14] + 1;
    z[13]=z[30]*z[14]*z[13];
    z[14]=n<T>(179,3) - z[1];
    z[14]=z[1]*z[14];
    z[14]= - n<T>(22,3) + n<T>(1,8)*z[14];
    z[14]=z[4]*z[14];
    z[14]= - n<T>(355,48)*z[33] + z[14];
    z[14]=z[4]*z[14];
    z[13]=z[14] + z[13];
    z[13]=z[3]*z[13];
    z[12]=n<T>(1,4)*z[12] + z[13];
    z[12]=z[3]*z[12];

    r += z[10] + n<T>(1,8)*z[11] + z[12];
 
    return r;
}

template double qg_2lha_r845(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r845(const std::array<dd_real,30>&);
#endif
