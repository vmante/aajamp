#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r505(const std::array<T,30>& k) {
  T z[62];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[14];
    z[6]=k[9];
    z[7]=k[3];
    z[8]=k[12];
    z[9]=k[15];
    z[10]=k[13];
    z[11]=n<T>(1,2)*z[4];
    z[12]=z[11] + 1;
    z[13]=z[12]*z[11];
    z[13]=z[13] + n<T>(5,3);
    z[14]=z[11] + 3;
    z[15]=n<T>(3,2)*z[5];
    z[16]=z[15] - z[14];
    z[16]=z[5]*z[16];
    z[16]=z[16] + z[13];
    z[16]=z[5]*z[16];
    z[17]=z[4] + n<T>(1,2);
    z[18]=z[17]*z[11];
    z[18]=z[18] - 1;
    z[18]=n<T>(1,2)*z[18];
    z[16]= - z[18] + z[16];
    z[19]=n<T>(1,2)*z[5];
    z[16]=z[16]*z[19];
    z[20]=z[11] + 5;
    z[21]= - z[4]*z[20];
    z[22]=3*z[4];
    z[23]=z[5]*z[22];
    z[21]=z[21] + z[23];
    z[21]=z[5]*z[21];
    z[23]=n<T>(1,4)*z[4];
    z[24]=z[23] - n<T>(5,3);
    z[25]=z[24]*z[4];
    z[21]= - z[25] + z[21];
    z[26]=npow(z[5],2);
    z[21]=z[21]*z[26];
    z[27]=npow(z[4],2);
    z[28]=z[27]*z[5];
    z[29]=n<T>(1,3)*z[27];
    z[30]= - z[29] + n<T>(1,4)*z[28];
    z[31]=z[1]*npow(z[5],3);
    z[30]=z[30]*z[31];
    z[21]=n<T>(1,4)*z[21] + z[30];
    z[21]=z[1]*z[21];
    z[16]=z[16] + z[21];
    z[16]=z[1]*z[16];
    z[21]=z[2] + 1;
    z[30]=z[21]*z[5];
    z[32]= - n<T>(1,3)*z[30] + n<T>(3,4) + z[2];
    z[32]=z[5]*z[32];
    z[32]=z[32] + z[11] - z[2];
    z[32]=z[5]*z[32];
    z[33]=n<T>(1,3)*z[2];
    z[34]= - n<T>(7,3) - z[4];
    z[32]=z[32] + n<T>(1,2)*z[34] + z[33];
    z[34]=z[23] + 1;
    z[35]= - z[34] + z[5];
    z[26]=z[26]*z[4]*z[35];
    z[31]=z[29]*z[31];
    z[26]=z[26] + z[31];
    z[31]=n<T>(1,2)*z[1];
    z[26]=z[26]*z[31];
    z[35]=z[19] - z[34];
    z[35]=z[5]*z[35];
    z[12]=z[12]*z[4];
    z[36]=static_cast<T>(1)+ z[12];
    z[35]=n<T>(1,2)*z[36] + z[35];
    z[35]=z[5]*z[35];
    z[26]=z[35] + z[26];
    z[26]=z[1]*z[26];
    z[26]=n<T>(1,2)*z[32] + z[26];
    z[32]=n<T>(1,2)*z[6];
    z[26]=z[26]*z[32];
    z[35]=z[4] + n<T>(3,2);
    z[35]= - z[33] + n<T>(1,2)*z[35];
    z[36]=n<T>(13,2) + 7*z[2];
    z[30]=n<T>(1,3)*z[36] - z[30];
    z[30]=z[5]*z[30];
    z[37]=z[4] - n<T>(17,6);
    z[38]= - n<T>(1,2)*z[37] + n<T>(5,3)*z[2];
    z[30]=z[30] - z[38];
    z[30]=z[5]*z[30];
    z[30]=z[30] - z[35];
    z[39]=n<T>(1,4)*z[5];
    z[30]=z[30]*z[39];
    z[39]= - n<T>(5,8) - z[2];
    z[39]=z[39]*z[33];
    z[40]=z[4] + 1;
    z[39]= - n<T>(1,4)*z[40] + z[39];
    z[39]=z[8]*z[39];
    z[41]=z[4] + 3;
    z[42]= - z[4]*z[41];
    z[42]=n<T>(19,6) + z[42];
    z[16]=z[26] + z[16] + z[30] + z[39] + n<T>(1,8)*z[42] + z[33];
    z[16]=z[16]*z[32];
    z[26]=n<T>(1,2)*z[2];
    z[30]=z[26]*z[27];
    z[39]=z[4] - n<T>(1,2);
    z[42]=z[39]*z[4];
    z[42]=z[42] + z[30];
    z[43]=z[4] - 1;
    z[44]=z[43]*z[11];
    z[45]= - z[8]*z[44];
    z[46]= - static_cast<T>(1)- z[8];
    z[46]=z[46]*z[32];
    z[45]=z[46] + z[45] - z[42];
    z[45]=z[7]*z[45];
    z[46]= - static_cast<T>(1)- z[22];
    z[46]=z[46]*z[23];
    z[47]=z[27]*z[2];
    z[48]= - z[27] - n<T>(1,4)*z[47];
    z[48]=z[2]*z[48];
    z[49]=n<T>(1,4)*z[6];
    z[45]=n<T>(1,2)*z[45] + z[49] + z[46] + z[48];
    z[45]=z[7]*z[45];
    z[46]=n<T>(1,2)*z[3];
    z[48]=z[46]*z[27];
    z[50]=npow(z[2],2);
    z[51]=z[48]*z[50];
    z[52]= - static_cast<T>(1)- n<T>(3,2)*z[4];
    z[52]=z[4]*z[52];
    z[52]=z[52] - z[47];
    z[52]=z[2]*z[52];
    z[52]=z[52] - z[51];
    z[45]=n<T>(1,2)*z[52] + z[45];
    z[45]=z[9]*z[45];
    z[52]= - static_cast<T>(7)- z[4];
    z[52]=z[4]*z[52];
    z[52]=z[52] - n<T>(11,3)*z[47];
    z[52]=z[52]*z[26];
    z[53]=7*z[4];
    z[54]= - static_cast<T>(1)+ z[53];
    z[52]=n<T>(1,3)*z[54] + z[52];
    z[52]=z[52]*z[26];
    z[52]=n<T>(5,3) + z[52];
    z[54]= - static_cast<T>(1)+ n<T>(1,12)*z[4];
    z[54]=z[54]*z[4];
    z[55]=n<T>(1,6)*z[47];
    z[56]= - z[54] + z[55];
    z[56]=z[2]*z[56];
    z[56]=n<T>(5,6) + z[56];
    z[56]=z[2]*z[56];
    z[56]= - n<T>(7,12) + z[56];
    z[56]=z[56]*z[26];
    z[56]= - n<T>(1,6)*z[3] + n<T>(1,3) + z[56];
    z[56]=z[3]*z[56];
    z[52]=n<T>(1,2)*z[52] + z[56];
    z[52]=z[52]*z[46];
    z[56]=n<T>(1,8)*z[4];
    z[57]=n<T>(7,2) + z[4];
    z[57]=z[57]*z[56];
    z[58]=n<T>(1,3)*z[4];
    z[59]= - static_cast<T>(5)- z[23];
    z[59]=z[59]*z[58];
    z[59]=z[59] - n<T>(5,4)*z[47];
    z[60]=n<T>(1,4)*z[2];
    z[59]=z[59]*z[60];
    z[45]=z[45] + z[52] + z[59] + n<T>(1,3) + z[57];
    z[34]=z[34]*z[53];
    z[52]=n<T>(41,2) + 11*z[4];
    z[52]=z[4]*z[52];
    z[52]=z[52] + n<T>(73,4)*z[47];
    z[52]=z[2]*z[52];
    z[34]=z[34] + z[52];
    z[34]=z[34]*z[33];
    z[52]=static_cast<T>(3)- z[58];
    z[52]=z[52]*z[23];
    z[57]=n<T>(17,4) + z[58];
    z[57]=z[4]*z[57];
    z[57]=z[57] + n<T>(23,12)*z[47];
    z[57]=z[2]*z[57];
    z[52]=z[57] + n<T>(5,3) + z[52];
    z[52]=z[52]*z[50];
    z[57]= - n<T>(3,2) - z[58];
    z[57]=z[4]*z[57];
    z[57]=z[57] - n<T>(5,12)*z[47];
    z[57]=z[2]*z[57];
    z[54]=z[57] - n<T>(13,12) + z[54];
    z[57]=npow(z[2],3);
    z[54]=z[3]*z[54]*z[57];
    z[52]=z[52] + z[54];
    z[52]=z[3]*z[52];
    z[34]=z[52] + z[4] + z[34];
    z[52]=z[40]*z[4];
    z[54]=z[52] + z[30];
    z[54]=z[54]*z[2];
    z[12]=z[12] + n<T>(1,2);
    z[54]=z[54] + z[12];
    z[58]=n<T>(1,4)*z[3];
    z[54]=z[54]*z[58]*npow(z[2],4);
    z[59]=n<T>(3,4)*z[4];
    z[41]= - z[41]*z[59];
    z[41]=z[41] - z[47];
    z[41]=z[2]*z[41];
    z[61]= - static_cast<T>(1)+ z[23];
    z[61]=z[4]*z[61];
    z[41]=z[41] - n<T>(5,4) + z[61];
    z[41]=z[41]*z[57];
    z[41]=z[41] + z[54];
    z[41]=z[3]*z[41];
    z[61]= - z[52] - z[47];
    z[61]=z[2]*z[61];
    z[61]= - z[4] + 3*z[61];
    z[61]=z[61]*z[50];
    z[41]=n<T>(5,4)*z[61] + z[41];
    z[40]=z[40]*z[11];
    z[17]=z[4]*z[17];
    z[17]=z[17] + z[30];
    z[17]=z[2]*z[17];
    z[17]=z[40] + z[17];
    z[17]=z[17]*z[57];
    z[17]=z[17] + z[54];
    z[17]=z[8]*z[17];
    z[17]=n<T>(1,4)*z[41] + z[17];
    z[17]=z[8]*z[17];
    z[17]=n<T>(1,8)*z[34] + z[17];
    z[17]=z[8]*z[17];
    z[34]=z[37]*z[23];
    z[37]=n<T>(5,3) - z[59];
    z[37]=z[4]*z[37];
    z[37]=z[37] + z[47];
    z[37]=z[37]*z[26];
    z[40]= - z[2]*z[11];
    z[39]=z[40] + z[39];
    z[39]=z[3]*z[39];
    z[34]=z[39] + z[37] + n<T>(1,3) + z[34];
    z[37]=npow(z[3],2);
    z[34]=z[34]*z[37];
    z[39]=z[58] - 1;
    z[40]=z[37]*z[27]*z[39];
    z[41]= - z[27] + z[48];
    z[48]=n<T>(1,3)*z[3];
    z[41]=z[41]*z[48];
    z[28]=z[58]*z[28];
    z[28]=z[41] + z[28];
    z[28]=z[5]*z[28];
    z[28]=n<T>(1,3)*z[40] + z[28];
    z[28]=z[5]*z[28];
    z[40]=npow(z[3],3);
    z[29]= - z[40]*z[29];
    z[28]=z[29] + z[28];
    z[28]=z[5]*z[28];
    z[29]=z[43]*z[4];
    z[41]= - z[29] + z[30];
    z[33]=z[41]*z[33];
    z[41]= - n<T>(1,3) + z[11];
    z[41]=z[4]*z[41];
    z[33]=z[33] + n<T>(1,6) + z[41];
    z[33]=z[33]*z[40];
    z[28]=n<T>(1,2)*z[33] + z[28];
    z[28]=z[1]*z[28];
    z[24]= - z[24]*z[23];
    z[33]=n<T>(1,16)*z[4];
    z[40]= - static_cast<T>(1)- z[33];
    z[40]=z[4]*z[40];
    z[41]=z[3]*z[23];
    z[40]=z[40] + z[41];
    z[40]=z[3]*z[40];
    z[24]=z[24] + z[40];
    z[24]=z[3]*z[24];
    z[20]= - z[20]*z[11];
    z[40]=z[3]*z[4];
    z[20]=z[20] + z[40];
    z[20]=z[3]*z[20];
    z[15]=z[15]*z[3];
    z[40]=z[4]*z[15];
    z[20]=z[20] + z[40];
    z[20]=z[20]*z[19];
    z[20]=z[24] + z[20];
    z[20]=z[5]*z[20];
    z[24]= - z[3]*z[22];
    z[24]= - z[25] + z[24];
    z[24]=z[24]*z[37];
    z[20]=n<T>(1,4)*z[24] + z[20];
    z[20]=z[5]*z[20];
    z[20]=z[28] + n<T>(1,2)*z[34] + z[20];
    z[20]=z[1]*z[20];
    z[24]=z[4] - z[55];
    z[24]=z[24]*z[26];
    z[24]=z[24] + n<T>(7,12) - z[4];
    z[24]=z[24]*z[60];
    z[24]=z[58] + z[24] - n<T>(1,3) + z[56];
    z[24]=z[3]*z[24];
    z[25]=n<T>(17,6) + z[4];
    z[25]=z[4]*z[25];
    z[25]=z[25] + n<T>(5,6)*z[47];
    z[25]=z[25]*z[26];
    z[25]=z[25] - n<T>(5,3) - z[23];
    z[24]=n<T>(1,4)*z[25] + z[24];
    z[24]=z[3]*z[24];
    z[25]= - z[56] + z[39];
    z[25]=z[3]*z[25];
    z[13]=n<T>(1,2)*z[13] + z[25];
    z[13]=z[3]*z[13];
    z[14]=z[3] - z[14];
    z[14]=z[3]*z[14];
    z[14]=z[14] + z[15];
    z[14]=z[14]*z[19];
    z[13]=z[13] + z[14];
    z[13]=z[5]*z[13];
    z[14]=n<T>(7,6) - z[3];
    z[14]=z[3]*z[14];
    z[14]= - z[18] + z[14];
    z[14]=z[14]*z[46];
    z[13]=z[14] + z[13];
    z[13]=z[5]*z[13];
    z[13]=z[20] + z[24] + z[13];
    z[13]=z[13]*z[31];
    z[14]=z[2]*z[42];
    z[14]=z[44] + z[14];
    z[15]=z[8]*z[2];
    z[14]=z[14]*z[15];
    z[18]= - z[43]*z[22];
    z[18]=z[18] - n<T>(11,4)*z[47];
    z[18]=z[2]*z[18];
    z[20]=static_cast<T>(3)- z[23];
    z[20]=z[4]*z[20];
    z[18]=z[20] + z[18];
    z[14]=n<T>(1,8)*z[18] + z[14];
    z[14]=z[8]*z[14];
    z[14]=n<T>(1,4)*z[42] + z[14];
    z[14]=z[8]*z[14];
    z[18]= - n<T>(3,4) + z[15];
    z[18]=z[8]*z[21]*z[18];
    z[18]=n<T>(1,4) + z[18];
    z[18]=z[8]*z[18];
    z[20]= - static_cast<T>(3)- z[60];
    z[20]=z[2]*z[20];
    z[20]= - n<T>(11,4) + z[20];
    z[20]=z[20]*npow(z[8],2)*z[49];
    z[18]=z[18] + z[20];
    z[18]=z[18]*z[32];
    z[20]=z[29] + z[30];
    z[20]=z[2]*z[20];
    z[22]=z[26] + 1;
    z[24]=z[2]*z[22];
    z[24]=n<T>(1,2) + z[24];
    z[24]=z[6]*z[24];
    z[24]=z[21] + z[24];
    z[24]=z[6]*z[24];
    z[25]= - static_cast<T>(1)+ z[11];
    z[25]=z[4]*z[25];
    z[20]=z[24] + z[25] + z[20];
    z[20]=z[7]*z[20]*npow(z[8],3);
    z[14]=n<T>(1,4)*z[20] + z[14] + z[18];
    z[14]=z[7]*z[14];
    z[18]= - n<T>(5,3) + z[53];
    z[18]=z[18]*z[23];
    z[20]=n<T>(1,12) + 5*z[4];
    z[20]=z[4]*z[20];
    z[20]=z[20] + n<T>(19,4)*z[47];
    z[20]=z[2]*z[20];
    z[18]=z[18] + z[20];
    z[11]=static_cast<T>(3)- z[11];
    z[11]=z[11]*z[56];
    z[20]=static_cast<T>(1)- 17*z[4];
    z[20]=z[20]*z[33];
    z[20]=z[20] - z[47];
    z[20]=z[2]*z[20];
    z[11]=z[11] + z[20];
    z[11]=z[2]*z[11];
    z[20]=z[27] + z[30];
    z[20]=z[2]*z[20];
    z[20]=n<T>(1,2)*z[27] + z[20];
    z[20]=z[8]*z[20]*z[50];
    z[11]=z[11] + n<T>(3,2)*z[20];
    z[11]=z[8]*z[11];
    z[20]= - static_cast<T>(3)- z[26];
    z[15]=z[20]*z[15];
    z[20]=n<T>(5,2) + z[2];
    z[20]=z[6]*z[20];
    z[15]=z[20] + z[15] - n<T>(1,12)*z[2] + n<T>(5,12) + z[4];
    z[15]=z[6]*z[15];
    z[11]=n<T>(1,8)*z[15] + n<T>(1,8)*z[18] + z[11];
    z[11]=z[8]*z[11];
    z[11]=z[14] + z[11];
    z[11]=z[7]*z[11];
    z[14]=z[3]*z[22];
    z[14]=z[14] - n<T>(17,8) - z[2];
    z[14]=z[14]*z[48];
    z[14]= - n<T>(1,2)*z[35] + z[14];
    z[14]=z[14]*z[46];
    z[15]=z[21]*z[3];
    z[18]=n<T>(1,2)*z[36] - z[15];
    z[18]=z[18]*z[48];
    z[15]= - z[19]*z[15];
    z[15]=z[18] + z[15];
    z[15]=z[15]*z[19];
    z[18]= - z[21]*z[58];
    z[18]=z[18] + n<T>(17,16) + z[2];
    z[18]=z[18]*z[48];
    z[18]= - n<T>(1,4)*z[38] + z[18];
    z[18]=z[3]*z[18];
    z[15]=z[18] + z[15];
    z[15]=z[5]*z[15];
    z[14]=z[14] + z[15];
    z[14]=z[14]*z[19];
    z[15]=z[2]*z[52];
    z[12]=z[7]*z[12];
    z[12]=z[12] + z[15] + z[51];
    z[12]=z[10]*z[12];

    r += z[11] + n<T>(1,3)*z[12] + z[13] + z[14] + z[16] + z[17] + n<T>(1,2)*
      z[45];
 
    return r;
}

template double qg_2lha_r505(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r505(const std::array<dd_real,30>&);
#endif
