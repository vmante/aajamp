#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r103(const std::array<T,30>& k) {
  T z[35];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[2];
    z[7]=k[11];
    z[8]=k[24];
    z[9]=k[3];
    z[10]=k[4];
    z[11]=k[9];
    z[12]=k[14];
    z[13]=npow(z[6],2);
    z[14]= - n<T>(563,9) + n<T>(125,2)*z[6];
    z[14]=z[14]*z[13];
    z[15]=z[6] - 1;
    z[16]=npow(z[6],3);
    z[17]=z[16]*z[8];
    z[18]=z[15]*z[17];
    z[14]=z[14] - n<T>(403,18)*z[18];
    z[18]=n<T>(1,2)*z[8];
    z[14]=z[14]*z[18];
    z[19]=n<T>(1,2)*z[4];
    z[20]=static_cast<T>(17)+ n<T>(35,2)*z[9];
    z[20]=z[20]*z[19];
    z[21]= - n<T>(991,2)*z[6] - static_cast<T>(23)- n<T>(481,4)*z[9];
    z[21]=z[6]*z[21];
    z[20]=n<T>(1,3)*z[21] + z[20] - n<T>(23,3) - n<T>(107,4)*z[9];
    z[14]=n<T>(1,3)*z[20] + z[14];
    z[20]=z[13]*z[2];
    z[21]=n<T>(41,24) - 3*z[6];
    z[21]=z[21]*z[20];
    z[22]= - 47*z[12] + 7;
    z[22]=z[9]*z[22];
    z[22]= - n<T>(5,4) + z[22];
    z[22]=z[12]*z[22];
    z[14]=z[21] + n<T>(1,2)*z[14] + n<T>(1,9)*z[22];
    z[16]=z[16]*z[2];
    z[16]= - n<T>(23,16)*z[16] + n<T>(563,48)*z[17];
    z[15]= - z[15]*z[16];
    z[21]=n<T>(1,2)*z[9];
    z[22]=z[21] + z[6];
    z[22]=z[6]*z[22];
    z[22]=z[21] + z[22];
    z[22]=z[6]*z[22];
    z[23]= - n<T>(67,6)*z[12] + n<T>(79,6) - 2*z[4];
    z[23]=z[9]*z[23];
    z[15]=n<T>(79,3)*z[22] + z[23] + z[15];
    z[22]=n<T>(1,3)*z[7];
    z[15]=z[15]*z[22];
    z[14]=n<T>(1,2)*z[14] + z[15];
    z[14]=z[7]*z[14];
    z[15]= - n<T>(53,3) + 9*z[4];
    z[23]=n<T>(13,3)*z[11];
    z[15]=z[23] + n<T>(1,2)*z[15];
    z[24]=n<T>(1,2)*z[6];
    z[25]= - static_cast<T>(1)+ z[24];
    z[25]=z[8]*z[25]*z[13];
    z[26]=static_cast<T>(361)- 281*z[6];
    z[26]=z[6]*z[26];
    z[25]=n<T>(403,9)*z[25] + n<T>(1,9)*z[26] - z[15];
    z[25]=z[25]*z[18];
    z[26]= - n<T>(803,4) - 91*z[9];
    z[27]=n<T>(1,3)*z[9];
    z[28]= - n<T>(11,4) - z[27];
    z[28]=z[4]*z[28];
    z[25]=z[25] + n<T>(67,12)*z[6] + n<T>(1,9)*z[26] + z[28];
    z[26]=n<T>(13,4)*z[11];
    z[28]=n<T>(317,2) + 121*z[9];
    z[28]=n<T>(1,3)*z[28] - z[26];
    z[29]=n<T>(47,3)*z[12];
    z[30]= - n<T>(1,2) - z[9];
    z[30]=z[30]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[30];
    z[30]=n<T>(1,3)*z[12];
    z[28]=z[28]*z[30];
    z[31]=z[12] - 1;
    z[32]= - n<T>(37,2) + 25*z[6];
    z[32]=z[6]*z[32];
    z[32]=n<T>(1,12)*z[32] + z[31];
    z[32]=z[2]*z[32];
    z[14]=z[14] + n<T>(1,2)*z[32] + n<T>(1,4)*z[25] + z[28];
    z[14]=z[7]*z[14];
    z[25]=z[8]*z[6];
    z[15]=n<T>(403,18)*z[25] + z[15];
    z[15]=z[15]*z[18];
    z[25]=n<T>(13,6)*z[11];
    z[28]=z[9] - static_cast<T>(1)- z[1];
    z[28]=z[5]*z[28];
    z[28]= - static_cast<T>(1)+ n<T>(5,3)*z[28];
    z[15]=z[15] + z[25] + n<T>(1,4)*z[28] + n<T>(1,3)*z[4];
    z[28]= - static_cast<T>(1)- z[9];
    z[28]=z[28]*z[29];
    z[32]=n<T>(451,4) + 47*z[9];
    z[28]=z[28] + n<T>(1,3)*z[32] - z[26];
    z[28]=z[28]*z[30];
    z[32]= - z[6] - static_cast<T>(5)- z[1];
    z[32]=n<T>(1,24)*z[32] + z[12];
    z[32]=z[2]*z[32];
    z[15]=z[32] + n<T>(1,2)*z[15] + z[28];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[14]=z[3]*z[14];
    z[15]=n<T>(9,2)*z[4] + z[23];
    z[15]=z[15]*z[18];
    z[28]=n<T>(1,2)*z[5];
    z[32]=n<T>(1,3)*z[1];
    z[33]= - n<T>(1,2) - z[32];
    z[33]=z[5]*z[33];
    z[33]= - n<T>(1,3) + z[33];
    z[33]=z[4]*z[33];
    z[15]=z[15] + z[25] + z[28] + z[33];
    z[25]=n<T>(1,6)*z[2];
    z[31]=z[1] + z[31];
    z[31]=z[31]*z[25];
    z[33]=z[5]*z[1];
    z[33]= - n<T>(5,4) + z[33];
    z[34]=static_cast<T>(1)+ n<T>(1,6)*z[12];
    z[34]=z[12]*z[34];
    z[31]=z[31] + n<T>(1,6)*z[33] + z[34];
    z[31]=z[2]*z[31];
    z[33]= - n<T>(47,3) + n<T>(1,2)*z[11];
    z[33]=z[12]*z[33];
    z[26]=z[33] + n<T>(47,3) - z[26];
    z[26]=z[26]*z[30];
    z[15]=z[31] + n<T>(1,2)*z[15] + z[26];
    z[24]= - z[27] - z[24];
    z[24]=z[6]*z[24];
    z[24]= - n<T>(1,6)*z[9] + z[24];
    z[16]=79*z[24] + z[16];
    z[16]=z[7]*z[16];
    z[13]= - 565*z[13] + 403*z[17];
    z[13]=z[8]*z[13];
    z[17]=z[20] - z[4];
    z[20]=static_cast<T>(44)+ n<T>(481,16)*z[9];
    z[13]=z[16] - n<T>(23,2)*z[12] + n<T>(1,48)*z[13] + n<T>(1027,24)*z[6] + n<T>(1,3)*
    z[20] + n<T>(5,4)*z[17];
    z[13]=z[13]*z[22];
    z[16]=13*z[11];
    z[17]=static_cast<T>(1)- z[21];
    z[17]=z[17]*z[16];
    z[20]= - n<T>(631,6) + 13*z[4];
    z[17]=n<T>(1,2)*z[20] + z[17];
    z[20]=n<T>(1,6)*z[6] - 5*z[4] - n<T>(23,6)*z[11];
    z[18]=z[20]*z[18];
    z[17]=n<T>(1,3)*z[17] + z[18];
    z[18]=n<T>(139,3) - n<T>(49,8)*z[11];
    z[18]=n<T>(1,2)*z[18] - z[29];
    z[18]=z[18]*z[30];
    z[20]=17*z[12] - static_cast<T>(9)+ n<T>(5,3)*z[6];
    z[20]=z[2]*z[20];
    z[13]=z[13] + n<T>(1,16)*z[20] + n<T>(1,4)*z[17] + z[18];
    z[13]=z[7]*z[13];
    z[13]=z[14] + n<T>(1,2)*z[15] + z[13];
    z[13]=z[3]*z[13];
    z[14]= - n<T>(3,4) - z[32];
    z[14]=z[5]*z[14];
    z[14]= - n<T>(7,4) + z[14];
    z[14]=z[14]*z[19];
    z[15]=z[28]*z[12];
    z[17]=z[5] - z[15];
    z[17]=z[17]*z[30];
    z[18]=z[12]*z[5];
    z[19]=z[1] - 1;
    z[19]= - z[4]*z[19];
    z[18]=z[19] - z[18];
    z[18]=z[18]*z[25];
    z[14]=z[18] + z[14] + z[17];
    z[14]=z[2]*z[14];
    z[17]=z[4]*z[5];
    z[18]=z[11]*z[5];
    z[19]=z[17] - n<T>(1,3)*z[18];
    z[17]= - z[17] + z[18];
    z[20]=static_cast<T>(1)+ n<T>(1,2)*z[10];
    z[17]=z[8]*z[20]*z[17];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[15]= - z[11]*z[15];
    z[15]=z[18] + z[15];
    z[15]=z[15]*z[30];
    z[14]=z[14] + n<T>(1,4)*z[17] + z[15];
    z[15]=7*z[20] + z[28];
    z[15]=z[4]*z[15];
    z[17]=static_cast<T>(1)- z[28];
    z[17]=z[12]*z[17];
    z[15]=z[17] - n<T>(7,2) + z[15];
    z[15]=z[2]*z[15];
    z[17]= - static_cast<T>(7)- z[28];
    z[17]=z[17]*z[12]*z[11];
    z[15]=z[17] + z[15];
    z[17]=z[5] + static_cast<T>(5)+ 7*z[10];
    z[17]=z[4]*z[17];
    z[17]=n<T>(1,3) + z[17];
    z[18]= - static_cast<T>(1)- n<T>(11,8)*z[10];
    z[18]=n<T>(1,3)*z[18] - n<T>(1,16)*z[5];
    z[18]=z[11]*z[18];
    z[17]=n<T>(1,16)*z[17] + z[18];
    z[17]=z[8]*z[17];
    z[18]=z[9]*z[10];
    z[19]= - z[23]*z[18];
    z[19]=z[19] + 13*z[9] - n<T>(433,9) - 9*z[10];
    z[19]=z[11]*z[19];
    z[19]=static_cast<T>(9)+ z[19];
    z[18]= - z[11]*z[18];
    z[18]=z[9] + z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(1,4)*z[19] + n<T>(79,9)*z[18];
    z[18]=z[7]*z[18];
    z[16]= - n<T>(1,2) - z[16];
    z[16]=z[11]*z[16];
    z[15]=n<T>(1,2)*z[18] + n<T>(1,24)*z[16] + z[17] + n<T>(1,8)*z[15];
    z[15]=z[7]*z[15];

    r += z[13] + n<T>(1,2)*z[14] + z[15];
 
    return r;
}

template double qg_2lha_r103(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r103(const std::array<dd_real,30>&);
#endif
