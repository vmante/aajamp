#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1439(const std::array<T,30>& k) {
  T z[30];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[9];
    z[7]=k[6];
    z[8]=k[13];
    z[9]=k[10];
    z[10]=k[14];
    z[11]=k[8];
    z[12]=k[12];
    z[13]=n<T>(1,4)*z[2];
    z[14]=z[2]*z[4];
    z[15]=n<T>(5,2) - z[4] - n<T>(1,2)*z[14];
    z[15]=z[15]*z[13];
    z[16]=n<T>(1,8)*z[12];
    z[17]=n<T>(1,2)*z[7];
    z[18]=z[4]*z[7];
    z[15]=z[15] - n<T>(1,4)*z[18] + z[17] - static_cast<T>(1)- z[16];
    z[15]=z[8]*z[15];
    z[19]= - static_cast<T>(1)+ 3*z[4];
    z[20]=z[19]*z[13];
    z[21]=z[20] - n<T>(5,4) + z[4];
    z[22]=n<T>(1,2)*z[2];
    z[21]=z[21]*z[22];
    z[23]=n<T>(1,4)*z[7];
    z[24]=z[23] - 1;
    z[25]=n<T>(1,2)*z[11];
    z[15]=z[15] + z[21] - z[25] + z[16] - z[24] + n<T>(1,2)*z[18];
    z[15]=z[8]*z[15];
    z[16]=z[23] + z[25] + n<T>(1,2) - z[5];
    z[16]=z[6]*z[16];
    z[21]=z[18]*z[6];
    z[23]=z[6]*z[7];
    z[23]=z[23] + n<T>(1,2)*z[21];
    z[25]=n<T>(1,2)*z[4];
    z[23]=z[23]*z[25];
    z[26]=z[1] + 1;
    z[27]=z[26]*z[1];
    z[27]=z[27] + 1;
    z[28]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[29]=z[28]*z[4];
    z[27]= - z[29] + n<T>(1,2)*z[27];
    z[27]=z[27]*z[2];
    z[19]= - 3*z[1] - z[19];
    z[19]=n<T>(1,2)*z[19] + z[27];
    z[13]=z[19]*z[13];
    z[13]=z[15] + z[13] + z[23] - static_cast<T>(1)+ z[16];
    z[15]=3*z[7];
    z[16]= - static_cast<T>(1)+ z[15];
    z[16]=z[6]*z[16];
    z[16]=z[21] - z[7] + z[16];
    z[16]=z[16]*z[25];
    z[19]=n<T>(3,2)*z[7] - static_cast<T>(1)- z[5];
    z[19]=z[6]*z[19];
    z[16]=z[19] + z[16];
    z[16]=z[4]*z[16];
    z[19]=z[17] - n<T>(1,2) - z[5];
    z[19]=z[6]*z[19];
    z[16]=z[27] + z[16] + z[19] - z[28];
    z[19]=z[18] - z[14];
    z[19]=z[8]*z[19];
    z[18]=n<T>(1,4)*z[19] + z[20] - n<T>(3,4)*z[18] - z[24];
    z[18]=z[8]*z[18];
    z[16]=n<T>(1,2)*z[16] + z[18];
    z[16]=z[3]*z[16];
    z[13]=n<T>(1,2)*z[13] + z[16];
    z[13]=z[3]*z[13];
    z[16]=z[4] + 1;
    z[18]=n<T>(13,3)*z[10];
    z[19]= - z[18]*z[16];
    z[20]=n<T>(1,3)*z[2];
    z[21]= - n<T>(19,2)*z[26] + z[4];
    z[21]=z[21]*z[20];
    z[19]=z[21] + n<T>(3,2) + z[19];
    z[19]=z[19]*z[22];
    z[21]=n<T>(1,3)*z[14] - n<T>(3,2) - n<T>(7,3)*z[4];
    z[21]=z[21]*z[22];
    z[22]=z[11] + n<T>(1,4)*z[12];
    z[21]=z[21] - z[17] + z[22];
    z[21]=z[8]*z[21];
    z[23]=n<T>(1,2) + z[4];
    z[24]=n<T>(19,4) - z[4];
    z[24]=z[2]*z[24];
    z[23]=n<T>(7,2)*z[23] + z[24];
    z[23]=z[23]*z[20];
    z[21]=z[21] + z[23] - n<T>(5,2)*z[7] - z[22];
    z[21]=z[8]*z[21];
    z[15]= - z[15] + n<T>(13,6)*z[10];
    z[15]= - z[15]*z[16];
    z[15]=z[11] + static_cast<T>(1)+ z[15];
    z[15]=z[6]*z[15];
    z[15]=z[21] + z[19] - z[17] + z[15];
    z[13]=n<T>(1,4)*z[15] + z[13];
    z[13]=z[3]*z[13];
    z[15]= - n<T>(7,3) + 3*z[11];
    z[16]=n<T>(1,4)*z[9];
    z[15]=z[15]*z[16];
    z[15]=z[15] + z[11];
    z[14]=z[14]*z[9];
    z[16]=z[4]*z[9];
    z[17]= - n<T>(1,2)*z[14] - 5*z[9] + n<T>(7,2)*z[16];
    z[19]=n<T>(1,6)*z[2];
    z[17]=z[17]*z[19];
    z[17]=z[17] - z[15];
    z[17]=z[8]*z[17];
    z[16]=11*z[9] - 7*z[16];
    z[16]=n<T>(1,2)*z[16] + z[14];
    z[16]=z[16]*z[19];
    z[15]=z[17] + z[16] + z[15];
    z[15]=z[8]*z[15];
    z[14]= - z[14] - 13*z[10] - z[9];
    z[14]=z[14]*z[20];
    z[16]= - z[18] + 7*z[7];
    z[16]=z[6]*z[16];
    z[14]=z[16] + z[14];
    z[14]=n<T>(1,4)*z[14] + z[15];

    r += z[13] + n<T>(1,2)*z[14];
 
    return r;
}

template double qg_2lha_r1439(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1439(const std::array<dd_real,30>&);
#endif
