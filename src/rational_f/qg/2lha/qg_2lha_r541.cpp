#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r541(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[11];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=k[6];
    z[8]=z[6] - 1;
    z[9]=n<T>(1,2)*z[5];
    z[10]= - z[6]*z[9];
    z[10]=z[10] - z[8];
    z[10]=z[5]*z[10];
    z[11]=z[5] + 1;
    z[12]= - z[2] + z[11];
    z[12]=z[4]*z[12];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[2]*z[12];
    z[12]=z[12] - static_cast<T>(1)- z[1];
    z[12]=z[2]*z[12];
    z[12]=z[8] + z[12];
    z[13]= - static_cast<T>(1)+ n<T>(1,2)*z[6];
    z[13]=z[5]*z[13];
    z[12]=z[13] + z[1] + n<T>(1,2)*z[12];
    z[12]=z[2]*z[12];
    z[8]= - z[1] - z[8];
    z[8]=z[12] + n<T>(1,2)*z[8] + z[10];
    z[8]=z[3]*z[8];
    z[10]=n<T>(1,2)*z[1];
    z[9]=z[10] - z[9];
    z[12]=static_cast<T>(1)+ z[9];
    z[13]=z[2]*npow(z[4],2);
    z[11]=z[11]*z[4];
    z[14]= - static_cast<T>(3)+ z[11];
    z[14]=z[4]*z[14];
    z[14]=z[14] - z[13];
    z[14]=z[2]*z[14];
    z[15]=n<T>(3,4) + z[5];
    z[15]=z[4]*z[15];
    z[14]=n<T>(1,4)*z[14] + static_cast<T>(1)+ z[15];
    z[14]=z[2]*z[14];
    z[10]= - z[5] - static_cast<T>(3)- z[10];
    z[10]=n<T>(1,2)*z[10] + z[14];
    z[10]=z[2]*z[10];
    z[8]=z[8] + n<T>(1,2)*z[12] + z[10];
    z[8]=z[3]*z[8];
    z[10]=n<T>(1,2) + z[5];
    z[10]=z[4]*z[10];
    z[10]= - n<T>(1,2) + z[10];
    z[10]=z[4]*z[10];
    z[10]=z[10] - z[13];
    z[10]=z[2]*z[10];
    z[10]=z[10] + n<T>(1,2) + z[11];
    z[10]=z[2]*z[10];
    z[9]=z[10] - n<T>(3,2) + z[6] + z[9];
    z[8]=n<T>(1,2)*z[9] + z[8];
    z[8]=z[3]*z[8];
    z[9]=z[5] - z[2];
    z[9]=z[9]*z[13];
    z[10]=z[5]*z[7];
    z[9]=z[9] - static_cast<T>(1)+ z[10];
    z[8]=n<T>(1,4)*z[9] + z[8];

    r += n<T>(1,4)*z[8];
 
    return r;
}

template double qg_2lha_r541(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r541(const std::array<dd_real,30>&);
#endif
