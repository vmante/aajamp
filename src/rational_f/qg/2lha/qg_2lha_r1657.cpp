#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1657(const std::array<T,30>& k) {
  T z[49];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[9];
    z[4]=k[7];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[11];
    z[8]=k[13];
    z[9]=k[12];
    z[10]=n<T>(1,2)*z[2];
    z[11]=n<T>(3,2)*z[2];
    z[12]=static_cast<T>(1)- z[11];
    z[12]=z[12]*z[10];
    z[13]=z[9]*npow(z[2],2);
    z[14]=n<T>(1,2)*z[9];
    z[15]=static_cast<T>(1)- z[14];
    z[15]=z[15]*z[13];
    z[12]=z[12] + z[15];
    z[12]=z[9]*z[12];
    z[15]=z[4] - 1;
    z[16]=n<T>(1,4)*z[8];
    z[15]=z[15]*z[16];
    z[17]=3*z[4];
    z[15]=z[15] - z[17];
    z[18]=static_cast<T>(1)- z[9];
    z[18]=z[18]*z[13];
    z[19]=static_cast<T>(1)- z[10];
    z[19]=z[2]*z[19];
    z[18]=z[18] + z[19] - z[7] + z[4];
    z[18]=z[18]*z[14];
    z[19]=n<T>(1,4)*z[2];
    z[20]= - z[19] - n<T>(11,4);
    z[20]=z[7]*z[20];
    z[18]=z[18] + z[20] + z[15];
    z[20]=n<T>(1,2)*z[6];
    z[18]=z[18]*z[20];
    z[21]=z[2] - 3;
    z[22]=n<T>(1,8)*z[2];
    z[23]=z[21]*z[22];
    z[24]=z[5] - 1;
    z[24]=z[24]*z[8];
    z[25]=z[5]*z[7];
    z[26]=n<T>(3,4)*z[4];
    z[27]= - z[7] - z[26];
    z[12]=z[18] + z[12] + z[23] + z[24] + n<T>(1,2)*z[27] + z[25];
    z[12]=z[6]*z[12];
    z[18]=z[1] + 3;
    z[23]=z[18]*z[19];
    z[23]=z[23] - n<T>(1,2) - z[1];
    z[23]=z[2]*z[23];
    z[27]=static_cast<T>(3)- z[9];
    z[13]=z[27]*z[13];
    z[27]=static_cast<T>(1)- n<T>(7,2)*z[2];
    z[27]=z[2]*z[27];
    z[13]=z[27] + z[13];
    z[13]=z[13]*z[14];
    z[13]=z[23] + z[13];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[13]= - static_cast<T>(3)- z[10];
    z[13]=z[13]*z[10];
    z[23]=3*z[8];
    z[13]=z[13] - n<T>(17,4) - z[23];
    z[27]=static_cast<T>(5)+ z[8];
    z[28]= - static_cast<T>(1)- n<T>(3,8)*z[2];
    z[28]=z[2]*z[28];
    z[27]=n<T>(1,4)*z[27] + z[28];
    z[27]=z[9]*z[27];
    z[13]=n<T>(1,2)*z[13] + z[27];
    z[13]=z[9]*z[13];
    z[27]=z[2] + 3;
    z[27]=z[27]*z[10];
    z[28]=z[27] + 1;
    z[29]=z[23] - z[28];
    z[13]=n<T>(1,4)*z[29] + z[13];
    z[13]=z[9]*z[13];
    z[29]=npow(z[9],3);
    z[30]=z[6]*z[29]*z[16];
    z[31]=n<T>(1,4)*z[9];
    z[32]=static_cast<T>(1)+ z[23];
    z[32]=z[32]*z[31];
    z[32]= - z[8] + z[32];
    z[33]=npow(z[9],2);
    z[32]=z[32]*z[33];
    z[32]=z[32] + z[30];
    z[32]=z[6]*z[32];
    z[34]= - n<T>(5,2) - 9*z[8];
    z[35]=n<T>(3,4)*z[8];
    z[36]=z[35] + 1;
    z[37]=z[9]*z[36];
    z[34]=n<T>(1,4)*z[34] + z[37];
    z[34]=z[9]*z[34];
    z[34]=z[8] + z[34];
    z[34]=z[9]*z[34];
    z[32]=z[34] + z[32];
    z[32]=z[6]*z[32];
    z[34]=z[2]*z[7];
    z[37]= - 3*z[7] - z[34];
    z[37]=z[37]*z[19];
    z[38]=n<T>(1,2)*z[8];
    z[37]=z[37] - z[7] - z[38];
    z[13]=z[32] + n<T>(1,2)*z[37] + z[13];
    z[13]=z[6]*z[13];
    z[32]=z[34] + z[7];
    z[37]=z[5] - 3;
    z[39]= - z[8]*z[37];
    z[39]=z[39] + 3*z[25] + z[32];
    z[40]=z[11]*z[9];
    z[41]= - n<T>(5,2) - z[2];
    z[41]=z[41]*z[40];
    z[42]= - static_cast<T>(9)- z[8];
    z[43]=n<T>(1,4) + z[2];
    z[43]=z[2]*z[43];
    z[41]=z[41] + n<T>(1,4)*z[42] + z[43];
    z[41]=z[9]*z[41];
    z[42]=n<T>(3,4)*z[2];
    z[36]=z[41] + z[42] - z[36];
    z[36]=z[9]*z[36];
    z[13]=z[13] + n<T>(1,4)*z[39] + z[36];
    z[13]=z[13]*z[20];
    z[36]=3*z[2];
    z[39]=n<T>(5,2)*z[2];
    z[41]=static_cast<T>(3)+ z[39];
    z[41]=z[41]*z[36];
    z[43]= - static_cast<T>(7)- z[36];
    z[43]=z[43]*z[36];
    z[43]= - static_cast<T>(5)+ z[43];
    z[43]=z[43]*z[14];
    z[41]=z[43] - n<T>(1,2) + z[41];
    z[41]=z[9]*z[41];
    z[43]=z[2] - n<T>(1,2);
    z[44]= - z[43]*z[36];
    z[41]=z[41] + z[44] - static_cast<T>(3)- z[8];
    z[41]=z[41]*z[31];
    z[44]=z[16] - n<T>(3,8);
    z[44]=z[4]*z[44];
    z[13]=z[13] + z[41] - z[22] + static_cast<T>(1)+ z[44];
    z[13]=z[6]*z[13];
    z[41]= - static_cast<T>(3)- 7*z[2];
    z[41]=z[41]*z[10];
    z[41]= - static_cast<T>(1)+ z[41];
    z[44]= - n<T>(13,2) - z[36];
    z[44]=z[44]*z[10];
    z[44]= - static_cast<T>(1)+ z[44];
    z[44]=z[44]*z[14];
    z[45]=n<T>(21,8) + 2*z[2];
    z[45]=z[2]*z[45];
    z[44]=z[44] + n<T>(7,16) + z[45];
    z[44]=z[9]*z[44];
    z[41]=n<T>(1,2)*z[41] + z[44];
    z[41]=z[9]*z[41];
    z[43]=z[43]*z[42];
    z[44]= - n<T>(1,8) + z[1];
    z[44]=z[4]*z[44];
    z[43]=z[43] + static_cast<T>(1)+ z[44];
    z[13]=z[13] + n<T>(1,2)*z[43] + z[41];
    z[13]=z[6]*z[13];
    z[41]=static_cast<T>(1)+ z[8];
    z[27]=3*z[41] + z[27];
    z[41]=z[19] + 1;
    z[43]=z[41]*z[2];
    z[44]=z[5] - n<T>(1,2);
    z[43]=z[43] + n<T>(3,2)*z[44] - z[8];
    z[43]=z[9]*z[43];
    z[27]=n<T>(1,2)*z[27] + z[43];
    z[27]=z[9]*z[27];
    z[43]=z[10] + 1;
    z[45]=z[43]*z[10];
    z[27]=z[27] - z[8] + z[45];
    z[27]=z[9]*z[27];
    z[45]=z[43]*z[34];
    z[46]=z[7] + z[8];
    z[45]=n<T>(1,2)*z[46] + z[45];
    z[27]=n<T>(1,2)*z[45] + z[27];
    z[45]=z[8] + n<T>(1,4);
    z[46]= - z[9]*z[45];
    z[35]=z[35] + z[46];
    z[35]=z[35]*z[33];
    z[30]=z[35] - z[30];
    z[30]=z[30]*z[20];
    z[35]=static_cast<T>(1)+ n<T>(3,2)*z[8];
    z[46]=n<T>(1,4)*z[5] - z[35];
    z[46]=z[46]*z[14];
    z[45]=z[46] + z[45];
    z[45]=z[9]*z[45];
    z[45]= - n<T>(3,8)*z[8] + z[45];
    z[45]=z[9]*z[45];
    z[30]=z[45] + z[30];
    z[30]=z[6]*z[30];
    z[27]=n<T>(1,2)*z[27] + z[30];
    z[27]=z[6]*z[27];
    z[30]=5*z[2];
    z[45]=static_cast<T>(19)+ z[30];
    z[45]=z[45]*z[10];
    z[46]=static_cast<T>(1)+ n<T>(3,2)*z[5];
    z[38]=z[45] + 5*z[46] - z[38];
    z[38]=z[38]*z[33];
    z[45]=z[5] + 1;
    z[46]=z[45] + z[2];
    z[38]=z[38] - z[46];
    z[38]=z[9]*z[38];
    z[32]=z[38] - z[25] - z[32];
    z[27]=n<T>(1,4)*z[32] + z[27];
    z[27]=z[6]*z[27];
    z[32]= - static_cast<T>(13)- z[30];
    z[32]=z[32]*z[10];
    z[38]=n<T>(5,4) + z[5];
    z[39]=static_cast<T>(9)+ z[39];
    z[39]=z[2]*z[39];
    z[38]=5*z[38] + z[39];
    z[38]=z[9]*z[38];
    z[32]=z[38] + z[32] - static_cast<T>(5)- z[16];
    z[32]=z[32]*z[14];
    z[32]=z[32] - z[46];
    z[32]=z[9]*z[32];
    z[27]=z[32] + z[27];
    z[27]=z[27]*z[20];
    z[32]=n<T>(1,2)*z[4];
    z[38]=z[4]*z[16];
    z[38]=z[11] + z[38] + static_cast<T>(3)- z[32];
    z[39]=static_cast<T>(1)+ z[30];
    z[39]=z[39]*z[10];
    z[16]=z[39] - 3*z[45] - z[16];
    z[39]=static_cast<T>(17)+ z[30];
    z[39]=z[39]*z[19];
    z[46]=static_cast<T>(1)+ n<T>(5,8)*z[5];
    z[39]=3*z[46] + z[39];
    z[39]=z[39]*z[14];
    z[46]=n<T>(5,4)*z[2];
    z[47]= - static_cast<T>(3)- z[46];
    z[47]=z[2]*z[47];
    z[39]=z[39] - n<T>(15,8) + z[47];
    z[39]=z[9]*z[39];
    z[16]=n<T>(1,4)*z[16] + z[39];
    z[16]=z[9]*z[16];
    z[16]=z[27] + n<T>(1,4)*z[38] + z[16];
    z[16]=z[6]*z[16];
    z[27]= - static_cast<T>(11)- z[30];
    z[27]=z[27]*z[10];
    z[27]= - static_cast<T>(3)+ z[27];
    z[38]=n<T>(11,2) + 3*z[5];
    z[39]=static_cast<T>(1)+ n<T>(5,16)*z[2];
    z[39]=z[2]*z[39];
    z[38]=n<T>(1,8)*z[38] + z[39];
    z[38]=z[9]*z[38];
    z[27]=n<T>(3,8)*z[27] + z[38];
    z[27]=z[9]*z[27];
    z[38]=z[11] + 1;
    z[39]=z[38]*z[46];
    z[39]=z[39] - z[45];
    z[27]=n<T>(1,2)*z[39] + z[27];
    z[27]=z[9]*z[27];
    z[39]=z[2] - 1;
    z[46]=z[1] - z[39];
    z[46]=z[46]*z[19];
    z[47]=z[1] - n<T>(1,2);
    z[48]=z[4]*z[47];
    z[16]=z[16] + z[27] + z[46] + static_cast<T>(1)+ n<T>(1,4)*z[48];
    z[16]=z[6]*z[16];
    z[27]=z[2]*z[38];
    z[27]= - n<T>(1,2)*z[45] + z[27];
    z[38]=n<T>(1,2)*z[5];
    z[28]=z[38] + z[28];
    z[28]=z[28]*z[31];
    z[45]=z[43]*z[2];
    z[28]=z[28] - n<T>(1,2) - z[45];
    z[28]=z[9]*z[28];
    z[27]=n<T>(1,2)*z[27] + z[28];
    z[27]=z[9]*z[27];
    z[18]= - z[18]*z[10];
    z[28]=n<T>(3,2) + z[1];
    z[28]=z[1]*z[28];
    z[18]=z[18] + n<T>(1,2) + z[28];
    z[18]=z[2]*z[18];
    z[28]=n<T>(3,2) - z[1];
    z[28]=z[1]*z[28];
    z[28]= - n<T>(1,2) + z[28];
    z[28]=z[4]*z[28];
    z[18]=z[18] + static_cast<T>(3)+ z[28];
    z[18]=n<T>(1,4)*z[18] + z[27];
    z[16]=n<T>(1,2)*z[18] + z[16];
    z[16]=z[3]*z[16];
    z[18]=z[30] + 3;
    z[11]= - z[18]*z[11];
    z[27]= - z[43]*z[36];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[9]*z[27];
    z[28]=static_cast<T>(7)+ n<T>(11,2)*z[2];
    z[28]=z[2]*z[28];
    z[27]=z[27] + n<T>(3,2) + z[28];
    z[27]=z[9]*z[27];
    z[11]=z[27] - static_cast<T>(1)+ z[11];
    z[11]=z[9]*z[11];
    z[27]=z[47]*z[1];
    z[28]=n<T>(7,2) + z[1];
    z[28]=z[2]*z[28];
    z[28]=z[28] + n<T>(1,2) + z[27];
    z[28]=z[2]*z[28];
    z[27]=n<T>(1,2) - z[27];
    z[27]=z[4]*z[27];
    z[11]=z[11] + z[27] + z[28];
    z[11]=z[16] + n<T>(1,8)*z[11] + z[13];
    z[11]=z[3]*z[11];
    z[13]=n<T>(1,2)*z[7];
    z[16]= - n<T>(7,2)*z[25] - z[17] + static_cast<T>(3)+ z[13];
    z[17]=static_cast<T>(5)+ z[36];
    z[17]=z[17]*z[10];
    z[17]=z[17] - z[35];
    z[27]=z[19]*z[9];
    z[28]=static_cast<T>(11)+ 9*z[2];
    z[28]=z[28]*z[27];
    z[30]= - n<T>(1,2) - z[36];
    z[30]=z[2]*z[30];
    z[28]=z[30] + z[28];
    z[28]=z[9]*z[28];
    z[17]=n<T>(3,4)*z[17] + z[28];
    z[17]=z[9]*z[17];
    z[28]=z[38] - 1;
    z[30]=n<T>(5,8)*z[4] - z[28];
    z[30]=z[8]*z[30];
    z[35]= - static_cast<T>(1)- n<T>(1,4)*z[7];
    z[35]=z[2]*z[35];
    z[16]=z[17] + z[35] + n<T>(1,2)*z[16] + z[30];
    z[17]= - z[2]*z[21];
    z[17]=z[23] + z[17];
    z[21]=z[9]*z[2];
    z[30]=static_cast<T>(1)+ z[42];
    z[30]=z[30]*z[21];
    z[17]=n<T>(1,4)*z[17] + z[30];
    z[17]=z[9]*z[17];
    z[30]=15*z[8];
    z[35]= - static_cast<T>(1)- z[30];
    z[36]=static_cast<T>(3)+ z[19];
    z[36]=z[2]*z[36];
    z[35]=n<T>(1,2)*z[35] + z[36];
    z[17]=n<T>(1,2)*z[35] + z[17];
    z[17]=z[9]*z[17];
    z[34]=z[41]*z[34];
    z[30]=13*z[7] + z[30];
    z[30]=n<T>(1,4)*z[30] + z[34];
    z[17]=n<T>(1,2)*z[30] + z[17];
    z[30]=n<T>(5,2) - z[9];
    z[30]=z[33]*z[30];
    z[20]= - z[29]*z[20];
    z[20]=z[30] + z[20];
    z[20]=z[6]*z[8]*z[20];
    z[29]=static_cast<T>(1)- n<T>(1,8)*z[9];
    z[29]=z[9]*z[29];
    z[29]= - n<T>(23,16) + z[29];
    z[29]=z[9]*z[8]*z[29];
    z[20]=z[29] + n<T>(1,4)*z[20];
    z[20]=z[6]*z[20];
    z[17]=n<T>(1,2)*z[17] + z[20];
    z[17]=z[6]*z[17];
    z[16]=n<T>(1,2)*z[16] + z[17];
    z[16]=z[6]*z[16];
    z[17]= - static_cast<T>(11)- 21*z[2];
    z[17]=z[17]*z[10];
    z[20]=static_cast<T>(5)+ n<T>(9,2)*z[2];
    z[20]=z[20]*z[21];
    z[17]=z[17] + z[20];
    z[17]=z[9]*z[17];
    z[20]=static_cast<T>(7)+ n<T>(33,2)*z[2];
    z[20]=z[2]*z[20];
    z[20]= - static_cast<T>(3)+ z[20];
    z[17]=n<T>(1,2)*z[20] + z[17];
    z[17]=z[17]*z[14];
    z[20]=static_cast<T>(1)- z[32];
    z[21]= - z[42] - n<T>(5,4) - z[1];
    z[21]=z[2]*z[21];
    z[17]=z[17] + n<T>(1,2)*z[20] + z[21];
    z[16]=n<T>(1,2)*z[17] + z[16];
    z[16]=z[6]*z[16];
    z[17]=static_cast<T>(9)+ 25*z[2];
    z[17]=z[17]*z[10];
    z[17]= - static_cast<T>(1)+ z[17];
    z[20]=static_cast<T>(1)+ z[2];
    z[20]=z[20]*z[40];
    z[18]= - z[2]*z[18];
    z[18]=z[18] + z[20];
    z[18]=z[9]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[17]=z[17]*z[14];
    z[18]= - static_cast<T>(11)- 3*z[1];
    z[18]=z[18]*z[22];
    z[20]= - n<T>(3,8) - z[1];
    z[20]=z[1]*z[20];
    z[18]=z[18] - n<T>(3,8) + z[20];
    z[18]=z[2]*z[18];
    z[20]= - n<T>(7,8) + z[1];
    z[20]=z[1]*z[20];
    z[20]= - n<T>(1,8) + z[20];
    z[20]=z[4]*z[20];
    z[17]=z[17] + z[18] + n<T>(1,4) + z[20];
    z[16]=n<T>(1,2)*z[17] + z[16];
    z[11]=n<T>(1,2)*z[16] + z[11];
    z[11]=z[3]*z[11];
    z[11]=n<T>(1,4)*z[12] + z[11];
    z[11]=z[3]*z[11];
    z[12]=z[28]*z[5];
    z[12]=z[12] + n<T>(1,2);
    z[16]=z[8]*z[12];
    z[16]=z[16] - z[1] + z[28];
    z[16]=z[5]*z[16];
    z[17]=z[37]*z[5];
    z[17]=z[17] + 3;
    z[18]=z[17]*z[5];
    z[18]=z[18] - 1;
    z[18]=z[18]*z[8];
    z[20]= - z[38]*z[18];
    z[21]=n<T>(1,2)*z[1] - z[28];
    z[21]=z[21]*npow(z[5],2);
    z[20]=z[21] + z[20];
    z[20]=z[2]*z[20];
    z[16]=z[20] + z[16];
    z[16]=z[2]*z[16];
    z[20]= - z[1] + z[5];
    z[16]=n<T>(1,2)*z[20] + z[16];
    z[16]=z[2]*z[16];
    z[20]=z[7]*z[1];
    z[20]=z[20] - z[25];
    z[21]=z[39]*z[2];
    z[21]=z[21] + 1;
    z[29]= - z[2]*z[5]*z[21];
    z[25]=z[25] + z[29];
    z[14]=z[25]*z[14];
    z[14]=z[14] + n<T>(1,2)*z[20] + z[16];
    z[12]= - z[12]*z[23];
    z[16]=n<T>(3,2) - z[5];
    z[16]=z[5]*z[16];
    z[16]=z[16] - z[18];
    z[16]=z[2]*z[16];
    z[12]=z[16] + z[12] - z[44];
    z[12]=z[12]*z[19];
    z[16]= - static_cast<T>(1)+ z[26];
    z[12]=z[12] + n<T>(1,2)*z[16] - z[24];
    z[12]=z[2]*z[12];
    z[16]=z[4] - z[17];
    z[16]=z[16]*z[22];
    z[16]=z[16] - z[32] - z[28];
    z[16]=z[2]*z[8]*z[16];
    z[15]= - n<T>(1,2)*z[15] + z[16];
    z[15]=z[2]*z[15];
    z[10]= - z[10]*z[39];
    z[10]= - static_cast<T>(1)+ z[10];
    z[10]=z[27]*z[4]*z[10];
    z[10]=z[15] + z[10];
    z[10]=z[6]*z[10];
    z[15]= - z[2]*z[21];
    z[15]=z[7] + z[15];
    z[15]=z[15]*z[31];
    z[10]=z[10] + z[15] + z[13] + z[12];
    z[10]=z[6]*z[10];
    z[10]=n<T>(1,4)*z[14] + z[10];

    r += n<T>(1,4)*z[10] + z[11];
 
    return r;
}

template double qg_2lha_r1657(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1657(const std::array<dd_real,30>&);
#endif
