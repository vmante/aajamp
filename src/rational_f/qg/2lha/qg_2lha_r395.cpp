#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r395(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[14];
    z[6]=k[9];
    z[7]=k[3];
    z[8]=k[12];
    z[9]=k[6];
    z[10]=k[27];
    z[11]=k[11];
    z[12]=npow(z[2],2);
    z[13]= - static_cast<T>(25)- 47*z[2];
    z[13]=z[13]*z[12];
    z[14]=z[2] + 1;
    z[15]=z[3]*npow(z[2],3);
    z[16]=z[14]*z[15];
    z[13]=z[13] - 13*z[16];
    z[13]=z[3]*z[13];
    z[16]=z[2]*npow(z[10],2);
    z[17]=n<T>(1,2)*z[16];
    z[18]=n<T>(1,2)*z[10];
    z[19]=static_cast<T>(5)- z[18];
    z[19]=z[10]*z[19];
    z[19]=z[19] - z[17];
    z[19]=z[2]*z[19];
    z[20]= - n<T>(155,8) + 11*z[10];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[20]=n<T>(1,3)*z[2];
    z[19]=z[19]*z[20];
    z[21]= - static_cast<T>(5)- 47*z[7];
    z[19]=n<T>(1,16)*z[21] + z[19];
    z[19]=z[2]*z[19];
    z[21]=z[7] - 1;
    z[22]=n<T>(1,6)*z[6];
    z[23]= - z[21]*z[22];
    z[24]=n<T>(3,4) - z[7];
    z[24]=z[7]*z[24];
    z[24]=5*z[24] - n<T>(5,3) + z[11];
    z[13]=n<T>(1,48)*z[13] + z[23] + n<T>(1,4)*z[24] + z[19];
    z[19]=n<T>(1,3)*z[10];
    z[23]=z[10]*z[20];
    z[23]=z[23] - n<T>(7,8) + z[19];
    z[23]=z[2]*z[23];
    z[24]=7*z[7];
    z[25]= - n<T>(31,3) - z[24];
    z[23]=n<T>(1,8)*z[25] + z[23];
    z[23]=z[2]*z[23];
    z[24]= - static_cast<T>(41)- z[24];
    z[24]=z[7]*z[24];
    z[23]=n<T>(1,24)*z[24] + z[23];
    z[23]=z[2]*z[23];
    z[24]=n<T>(1,24)*z[15];
    z[14]=z[14]*z[24];
    z[25]=npow(z[7],2);
    z[14]=z[14] - n<T>(17,24)*z[25] + z[23];
    z[14]=z[8]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[13]=z[8]*z[13];
    z[14]=7*z[2];
    z[23]=n<T>(1,4) + z[2];
    z[23]=z[23]*z[14];
    z[26]=z[12]*z[3];
    z[27]=n<T>(1,2) + z[2];
    z[27]=z[27]*z[26];
    z[23]=z[23] + n<T>(13,4)*z[27];
    z[23]=z[3]*z[23];
    z[27]=n<T>(5,2) - z[10];
    z[27]=z[10]*z[27];
    z[17]=z[27] - z[17];
    z[17]=z[17]*z[20];
    z[27]=n<T>(21,4) + n<T>(11,3)*z[10];
    z[17]=n<T>(1,2)*z[27] + z[17];
    z[27]=n<T>(1,2)*z[2];
    z[17]=z[17]*z[27];
    z[18]=z[22] - n<T>(1,2)*z[7] + static_cast<T>(1)+ z[18];
    z[28]=n<T>(1,2)*z[6];
    z[18]=z[18]*z[28];
    z[29]= - n<T>(79,12) + z[9];
    z[29]=n<T>(1,2)*z[29] - z[10];
    z[30]=static_cast<T>(1)- n<T>(1,8)*z[9];
    z[30]=z[7]*z[30];
    z[13]=z[13] + n<T>(1,12)*z[23] + z[18] + z[17] + n<T>(1,4)*z[29] + z[30];
    z[13]=z[8]*z[13];
    z[17]=n<T>(1,3)*z[6];
    z[18]=npow(z[1],2);
    z[23]= - z[18]*z[17];
    z[29]=z[1] + n<T>(1,2);
    z[29]=z[29]*z[1];
    z[23]=z[29] + z[23];
    z[23]=z[6]*z[23];
    z[30]=z[5]*z[6];
    z[31]= - z[18]*z[30];
    z[23]=z[23] + z[31];
    z[23]=z[5]*z[23];
    z[31]=n<T>(1,3)*z[1];
    z[32]=z[31] - 1;
    z[33]=z[1]*z[28];
    z[33]=z[33] + z[32];
    z[33]=z[33]*z[28];
    z[23]=z[33] + z[23];
    z[23]=z[5]*z[23];
    z[33]=n<T>(1,2) - z[6];
    z[33]=z[33]*z[28];
    z[23]=z[33] + z[23];
    z[23]=z[5]*z[23];
    z[33]=n<T>(1,2)*z[9];
    z[23]=z[23] - static_cast<T>(3)+ z[33];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[18]*z[27];
    z[34]=z[18]*z[5];
    z[35]=n<T>(1,3)*z[34];
    z[18]=z[18] - z[35];
    z[36]=n<T>(1,2)*z[5];
    z[18]=z[18]*z[36];
    z[18]=n<T>(1,3)*z[27] + z[18];
    z[18]=z[3]*z[18];
    z[27]=n<T>(1,2)*z[1];
    z[37]=n<T>(1,4) + z[1];
    z[37]=z[37]*z[27];
    z[35]=z[37] - z[35];
    z[35]=z[5]*z[35];
    z[35]=n<T>(1,12)*z[1] + z[35];
    z[35]=z[5]*z[35];
    z[37]=z[1] - n<T>(13,8)*z[2];
    z[37]=z[2]*z[37];
    z[37]= - n<T>(5,8)*z[1] + z[37];
    z[18]=z[18] + n<T>(1,3)*z[37] + z[35];
    z[18]=z[3]*z[18];
    z[29]=z[29] - z[34];
    z[29]=z[5]*z[29];
    z[29]=n<T>(1,2)*z[32] + z[29];
    z[29]=z[5]*z[29];
    z[29]=n<T>(1,4) + z[29];
    z[29]=z[5]*z[29];
    z[32]=3*z[2];
    z[34]=static_cast<T>(1)- z[32];
    z[29]=n<T>(1,4)*z[34] + z[29];
    z[18]=n<T>(1,2)*z[29] + z[18];
    z[18]=z[3]*z[18];
    z[29]=n<T>(1,4)*z[6];
    z[34]= - z[10]*z[29];
    z[13]=z[13] + n<T>(1,4)*z[18] + z[34] - n<T>(1,12)*z[16] + z[19] + n<T>(1,8)*
    z[23];
    z[13]=z[4]*z[13];
    z[18]=z[33] + 1;
    z[18]=z[18]*z[7];
    z[23]=z[9] + 7;
    z[18]= - z[18] + n<T>(1,2)*z[23];
    z[16]= - z[10] - z[16];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(1,8) + z[16];
    z[16]=z[16]*z[20];
    z[16]=z[16] - z[18];
    z[20]=static_cast<T>(5)+ n<T>(7,2)*z[7];
    z[21]=z[6]*z[21];
    z[21]=n<T>(5,6)*z[21] + n<T>(1,6)*z[20];
    z[21]=z[7]*z[21];
    z[19]=z[12]*z[19];
    z[19]=n<T>(7,8)*z[7] + z[19];
    z[19]=z[2]*z[19];
    z[19]=z[24] + z[19] + z[21];
    z[19]=z[8]*z[19];
    z[21]= - n<T>(13,3) + n<T>(5,4)*z[7];
    z[21]=z[7]*z[21];
    z[21]=n<T>(29,12) + z[21];
    z[23]= - z[7]*z[17];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[21]=z[6]*z[21];
    z[12]= - z[12] - n<T>(1,2)*z[15];
    z[12]=z[3]*z[12];
    z[12]=z[19] + n<T>(13,48)*z[12] + n<T>(1,4)*z[16] + z[21];
    z[12]=z[8]*z[12];
    z[15]= - z[32] + z[26];
    z[15]=z[3]*z[15];
    z[16]=z[2]*z[10];
    z[16]=z[16] + n<T>(7,12);
    z[19]=n<T>(21,4) + z[10];
    z[19]=n<T>(3,8)*z[6] + n<T>(1,4)*z[19] - z[7];
    z[19]=z[6]*z[19];
    z[12]=z[12] + n<T>(1,16)*z[15] - n<T>(1,2)*z[16] + z[19];
    z[12]=z[8]*z[12];
    z[15]=z[1] + 1;
    z[19]=n<T>(1,8) + z[31];
    z[19]=z[6]*z[19];
    z[19]= - n<T>(1,3)*z[15] + z[19];
    z[19]=z[19]*z[29];
    z[21]=z[31] + n<T>(1,16);
    z[23]=n<T>(1,12)*z[6];
    z[24]= - z[1]*z[23];
    z[24]=z[24] + z[21];
    z[24]=z[6]*z[24];
    z[26]=z[1]*z[30];
    z[24]=z[24] - n<T>(1,4)*z[26];
    z[24]=z[5]*z[24];
    z[19]=z[19] + z[24];
    z[19]=z[5]*z[19];
    z[24]=n<T>(1,16)*z[6];
    z[26]=n<T>(1,3) - n<T>(3,2)*z[6];
    z[26]=z[26]*z[24];
    z[19]=z[26] + z[19];
    z[19]=z[5]*z[19];
    z[26]=n<T>(1,8) + z[1];
    z[31]= - z[5]*z[31];
    z[26]=n<T>(1,2)*z[26] + z[31];
    z[26]=z[5]*z[26];
    z[31]=n<T>(1,8) - z[1];
    z[26]=n<T>(1,6)*z[31] + z[26];
    z[26]=z[5]*z[26];
    z[31]= - static_cast<T>(1)+ z[27];
    z[31]=z[31]*z[27];
    z[27]= - z[5]*z[27];
    z[27]=z[1] + z[27];
    z[27]=z[5]*z[27];
    z[27]=z[31] + z[27];
    z[27]=z[3]*z[27];
    z[31]=n<T>(7,8)*z[2] - n<T>(1,2) + z[1];
    z[26]=n<T>(1,3)*z[27] + n<T>(1,6)*z[31] + z[26];
    z[26]=z[3]*z[26];
    z[27]=z[5]*z[1];
    z[21]= - n<T>(1,4)*z[27] + z[21];
    z[21]=z[5]*z[21];
    z[15]= - n<T>(1,12)*z[15] + z[21];
    z[15]=z[5]*z[15];
    z[15]=n<T>(1,48) + z[15];
    z[15]=z[5]*z[15];
    z[15]=n<T>(1,2)*z[26] + n<T>(1,4) + z[15];
    z[15]=z[3]*z[15];
    z[21]=n<T>(3,2) - z[10];
    z[21]=z[21]*z[29];
    z[12]=z[13] + z[12] + z[15] + z[21] + z[19];
    z[12]=z[4]*z[12];
    z[13]= - static_cast<T>(1)+ z[6];
    z[13]=z[13]*z[17];
    z[15]=static_cast<T>(5)- z[6];
    z[15]=z[15]*z[17];
    z[15]=z[15] - z[30];
    z[15]=z[15]*z[36];
    z[13]=z[13] + z[15];
    z[13]=z[5]*z[13];
    z[15]= - static_cast<T>(5)- z[6];
    z[15]=z[15]*z[22];
    z[13]=z[15] + z[13];
    z[13]=z[5]*z[13];
    z[15]=n<T>(5,3) - z[5];
    z[15]=z[15]*z[36];
    z[15]= - n<T>(1,3) + z[15];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(5,6) + z[15];
    z[15]=z[5]*z[15];
    z[17]=z[3]*z[5];
    z[19]=static_cast<T>(1)- z[5];
    z[19]=z[19]*z[17];
    z[21]=n<T>(1,2) - n<T>(1,3)*z[5];
    z[21]=z[5]*z[21];
    z[21]= - n<T>(1,6) + z[21];
    z[21]=z[5]*z[21];
    z[19]=z[21] + n<T>(1,6)*z[19];
    z[19]=z[3]*z[19];
    z[15]=z[15] + z[19];
    z[15]=z[3]*z[15];
    z[13]=z[13] + z[15];
    z[14]= - static_cast<T>(17)- z[14];
    z[14]=z[23]*z[25]*z[14];
    z[15]=z[2]*z[7];
    z[19]=z[7]*z[20];
    z[14]=z[14] - n<T>(1,3)*z[19] - n<T>(7,4)*z[15];
    z[14]=z[8]*z[6]*z[14];
    z[15]=n<T>(67,3)*z[7] - z[15];
    z[15]=z[6]*z[15];
    z[15]=n<T>(1,8)*z[15] - n<T>(59,24)*z[2] + z[18];
    z[15]=z[15]*z[28];
    z[14]=z[15] + z[14];
    z[14]=z[8]*z[14];
    z[15]=z[17] + z[30];
    z[15]=n<T>(5,12)*z[15];
    z[15]=z[2]*z[15];
    z[16]= - z[24] + z[16];
    z[16]=z[6]*z[16];
    z[14]=z[14] + z[16] + z[15];
    z[14]=z[8]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[14];

    r += z[12] + n<T>(1,2)*z[13];
 
    return r;
}

template double qg_2lha_r395(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r395(const std::array<dd_real,30>&);
#endif
