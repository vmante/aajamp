#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1696(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[24];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[9];
    z[8]=k[27];
    z[9]=k[14];
    z[10]=z[9] - 1;
    z[11]=static_cast<T>(1)+ z[1];
    z[11]=z[1]*z[11];
    z[12]= - z[4]*npow(z[1],3);
    z[11]=z[12] + z[11] - z[10];
    z[11]=z[2]*z[11];
    z[11]=z[11] - static_cast<T>(1)- 2*z[1];
    z[11]=z[2]*z[11];
    z[12]= - z[5] + z[6];
    z[13]=n<T>(1,4)*z[8];
    z[12]=z[13]*z[12];
    z[14]=z[8] - z[7];
    z[15]=z[5]*z[6];
    z[14]=z[3]*z[15]*z[14];
    z[15]=z[7]*z[15];
    z[15]=n<T>(3,2)*z[15] - n<T>(11,2)*z[5] + static_cast<T>(5)+ n<T>(7,2)*z[6];
    z[15]=z[7]*z[15];
    z[15]=z[15] + z[8];
    z[11]=n<T>(1,4)*z[14] + z[11] - static_cast<T>(2)+ z[12] + n<T>(1,2)*z[15];
    z[11]=z[3]*z[11];
    z[12]=2*z[9];
    z[14]= - static_cast<T>(1)+ z[1];
    z[14]=z[1]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[4]*z[14];
    z[15]=z[9] + z[4];
    z[15]=z[7]*z[15];
    z[14]=z[15] + z[14] + static_cast<T>(1)- z[12];
    z[14]=z[2]*z[14];
    z[15]=z[4]*z[1];
    z[12]=z[4] + z[12] - static_cast<T>(2)+ z[5];
    z[12]=z[7]*z[12];
    z[12]=z[14] + z[12] + static_cast<T>(1)+ z[15];
    z[12]=z[2]*z[12];
    z[14]= - static_cast<T>(1)- z[6];
    z[14]=z[4]*z[14];
    z[14]=z[14] + n<T>(3,4)*z[7];
    z[14]=z[7]*z[14];
    z[11]=z[11] + z[12] + z[14] - z[13] + z[4];
    z[11]=z[3]*z[11];
    z[10]=z[10] - z[4];
    z[10]=z[10]*z[7];
    z[12]=z[10] + z[4] - z[9];
    z[12]=z[2]*z[12];
    z[10]=z[10] + z[12];
    z[10]=z[2]*z[10];

    r += z[10] + z[11];
 
    return r;
}

template double qg_2lha_r1696(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1696(const std::array<dd_real,30>&);
#endif
