#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r752(const std::array<T,30>& k) {
  T z[45];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[27];
    z[7]=k[11];
    z[8]=k[24];
    z[9]=k[5];
    z[10]=k[9];
    z[11]=n<T>(1,2)*z[4];
    z[12]= - n<T>(1,3) - z[4];
    z[12]=z[12]*z[11];
    z[13]=z[4] - n<T>(1,2);
    z[14]=n<T>(1,3)*z[2];
    z[15]=z[13]*z[14];
    z[12]=z[12] + z[15];
    z[15]=npow(z[2],2);
    z[12]=z[12]*z[15];
    z[16]=n<T>(1,2)*z[2];
    z[17]=z[4] + 1;
    z[18]=z[16] - z[17];
    z[18]=z[18]*z[2];
    z[19]=z[11] + 1;
    z[20]=z[19]*z[4];
    z[21]=z[20] + n<T>(1,2);
    z[18]=z[18] + z[21];
    z[18]=z[18]*z[3]*npow(z[2],4);
    z[22]=n<T>(1,4)*z[2];
    z[23]=z[4] - z[22];
    z[23]=z[23]*z[14];
    z[24]= - n<T>(1,3) - z[11];
    z[24]=z[4]*z[24];
    z[24]=n<T>(1,6) + z[24];
    z[23]=n<T>(1,2)*z[24] + z[23];
    z[24]=npow(z[2],3);
    z[23]=z[23]*z[24];
    z[23]=z[23] - n<T>(1,6)*z[18];
    z[23]=z[3]*z[23];
    z[25]=z[17]*z[4];
    z[26]=z[5]*z[2]*z[25];
    z[12]= - n<T>(1,12)*z[26] + n<T>(1,2)*z[12] + z[23];
    z[12]=z[8]*z[12];
    z[23]=z[17]*z[11];
    z[26]= - static_cast<T>(1)- z[23];
    z[26]=z[4]*z[26];
    z[26]= - static_cast<T>(1)+ z[26];
    z[27]=static_cast<T>(161)+ n<T>(401,2)*z[4];
    z[27]=z[4]*z[27];
    z[13]=z[2]*z[13];
    z[13]= - n<T>(241,18)*z[13] - static_cast<T>(9)+ n<T>(1,18)*z[27];
    z[13]=z[2]*z[13];
    z[13]=9*z[26] + z[13];
    z[26]=n<T>(1,3)*z[4];
    z[27]=n<T>(563,2)*z[4];
    z[28]=static_cast<T>(349)+ z[27];
    z[28]=z[28]*z[26];
    z[28]=n<T>(45,2) + z[28];
    z[29]=n<T>(1207,16)*z[2] - n<T>(403,4) - 181*z[4];
    z[29]=z[2]*z[29];
    z[28]=n<T>(1,8)*z[28] + n<T>(1,9)*z[29];
    z[28]=z[28]*z[24];
    z[28]=z[28] + n<T>(563,72)*z[18];
    z[28]=z[3]*z[28];
    z[29]=161*z[2] - static_cast<T>(41)- 563*z[4];
    z[29]=z[29]*z[14];
    z[27]=static_cast<T>(215)+ z[27];
    z[27]=z[4]*z[27];
    z[27]= - n<T>(565,6) + z[27];
    z[27]=n<T>(1,2)*z[27] + z[29];
    z[27]=z[27]*z[15];
    z[27]=n<T>(1,12)*z[27] + z[28];
    z[27]=z[3]*z[27];
    z[20]=n<T>(3,2) + z[20];
    z[20]=z[20]*z[11];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[4]*z[20];
    z[20]=n<T>(1,2) + z[20];
    z[20]=z[10]*z[20];
    z[21]=z[10]*z[4]*z[21];
    z[21]= - z[23] + z[21];
    z[21]=z[5]*z[21];
    z[12]=n<T>(403,12)*z[12] + n<T>(9,4)*z[21] + n<T>(9,2)*z[20] + n<T>(1,4)*z[13] + 
    z[27];
    z[12]=z[8]*z[12];
    z[13]=npow(z[4],3);
    z[20]=z[13]*z[10];
    z[21]=npow(z[4],2);
    z[27]= - z[21] + z[20];
    z[27]=z[10]*z[27];
    z[28]=z[13]*z[6];
    z[29]=z[21] - z[28];
    z[29]=z[7]*z[29];
    z[30]=z[6]*z[20];
    z[27]=z[29] + z[27] + z[30];
    z[29]=n<T>(1,4)*z[6];
    z[27]=z[29]*z[27];
    z[30]=z[2] - n<T>(1,2);
    z[30]=z[30]*z[2];
    z[31]= - z[11] + z[30];
    z[31]=z[2]*z[31];
    z[31]= - n<T>(1,2)*z[21] + z[31];
    z[31]=z[10]*z[31];
    z[32]=n<T>(1,4)*z[4];
    z[33]=z[2] - z[4];
    z[34]=n<T>(1,4) - z[33];
    z[34]=z[2]*z[34];
    z[34]=n<T>(1,4) + z[34];
    z[34]=z[2]*z[34];
    z[31]=n<T>(1,2)*z[31] + z[32] + z[34];
    z[31]=z[10]*z[31];
    z[34]=z[4] + n<T>(1,2);
    z[35]=z[34] - z[16];
    z[36]= - z[2]*z[35];
    z[23]=z[23] + z[36];
    z[23]=z[5]*z[23];
    z[36]= - z[17]*z[32];
    z[37]=z[4]*z[34];
    z[37]= - n<T>(1,2) + z[37];
    z[38]= - z[4] + z[16];
    z[38]=z[2]*z[38];
    z[37]=n<T>(1,2)*z[37] + z[38];
    z[37]=z[2]*z[37];
    z[23]=n<T>(1,2)*z[23] + z[31] + z[36] + z[37] + z[27];
    z[23]=z[9]*z[23];
    z[27]=static_cast<T>(2885)+ 1171*z[4];
    z[27]=z[27]*z[11];
    z[27]=static_cast<T>(605)+ z[27];
    z[31]= - n<T>(137,2) - n<T>(283,3)*z[4];
    z[33]=n<T>(19,4) + z[33];
    z[33]=z[2]*z[33];
    z[31]=n<T>(1,2)*z[31] + z[33];
    z[31]=z[2]*z[31];
    z[27]=n<T>(1,18)*z[27] + z[31];
    z[31]= - static_cast<T>(3191)- n<T>(3013,2)*z[4];
    z[31]=z[31]*z[26];
    z[31]= - n<T>(1123,2) + z[31];
    z[33]=static_cast<T>(727)+ n<T>(599,6)*z[4];
    z[33]=z[4]*z[33];
    z[33]=n<T>(3941,6) + z[33];
    z[36]= - n<T>(4747,8) - 179*z[4];
    z[36]=n<T>(1,9)*z[36] + n<T>(13,4)*z[2];
    z[36]=z[2]*z[36];
    z[33]=n<T>(1,6)*z[33] + z[36];
    z[33]=z[2]*z[33];
    z[31]=n<T>(1,12)*z[31] + z[33];
    z[33]= - static_cast<T>(7)- 2*z[4];
    z[33]=z[4]*z[33];
    z[33]= - static_cast<T>(5)+ z[33];
    z[36]=8*z[4];
    z[37]= - 4*z[2] + static_cast<T>(11)+ z[36];
    z[37]=z[2]*z[37];
    z[33]=2*z[33] + z[37];
    z[33]=z[33]*z[14];
    z[37]=z[4] + 2;
    z[38]=z[37]*z[4];
    z[38]=z[38] + 1;
    z[33]=z[33] + z[38];
    z[39]=n<T>(38,3)*z[3];
    z[33]=z[33]*z[39];
    z[31]=n<T>(1,2)*z[31] + z[33];
    z[31]=z[3]*z[31];
    z[27]=n<T>(1,4)*z[27] + z[31];
    z[27]=z[3]*z[27];
    z[31]=static_cast<T>(943)+ n<T>(599,2)*z[4];
    z[31]=z[4]*z[31];
    z[31]=n<T>(1975,2) + z[31];
    z[31]=z[31]*z[32];
    z[31]=static_cast<T>(86)+ z[31];
    z[33]=static_cast<T>(353)+ n<T>(617,2)*z[4];
    z[33]=n<T>(1,9)*z[33] - z[16];
    z[33]=z[33]*z[22];
    z[40]= - static_cast<T>(36)- n<T>(1207,72)*z[4];
    z[40]=z[4]*z[40];
    z[33]=z[33] - n<T>(1385,72) + z[40];
    z[33]=z[2]*z[33];
    z[40]=z[14] - z[17];
    z[40]=z[2]*z[40];
    z[38]=z[40] + z[38];
    z[38]=z[2]*z[38];
    z[26]= - static_cast<T>(1)- z[26];
    z[26]=z[4]*z[26];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[4]*z[26];
    z[26]=z[38] - n<T>(1,3) + z[26];
    z[26]=z[26]*z[39];
    z[26]=z[26] + n<T>(1,9)*z[31] + z[33];
    z[26]=z[3]*z[26];
    z[31]=static_cast<T>(32)+ n<T>(295,24)*z[4];
    z[31]=z[4]*z[31];
    z[31]=n<T>(383,24) + z[31];
    z[30]=n<T>(1,3)*z[31] - n<T>(1,8)*z[30];
    z[30]=z[2]*z[30];
    z[31]= - n<T>(2081,4) - 143*z[4];
    z[31]=z[4]*z[31];
    z[31]= - n<T>(1133,2) + z[31];
    z[31]=z[4]*z[31];
    z[31]= - n<T>(757,4) + z[31];
    z[26]=z[26] + n<T>(1,36)*z[31] + z[30];
    z[26]=z[3]*z[26];
    z[30]=n<T>(133,18) - z[4];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(31,18) + z[30];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(11,18) + z[30];
    z[26]=n<T>(1,8)*z[30] + z[26];
    z[26]=z[10]*z[26];
    z[30]=5*z[4];
    z[31]=z[17]*z[30];
    z[31]=n<T>(9,2) + z[31];
    z[31]=z[2]*z[31];
    z[33]=n<T>(23,4) + z[30];
    z[33]=z[4]*z[33];
    z[33]=n<T>(167,9) + z[33];
    z[33]=z[4]*z[33];
    z[31]=z[31] + n<T>(335,36) + z[33];
    z[26]=z[26] + n<T>(1,4)*z[31] + z[27];
    z[26]=z[10]*z[26];
    z[27]=n<T>(1,2)*z[6];
    z[31]=z[27]*z[21];
    z[33]=9*z[4];
    z[38]= - static_cast<T>(17)- z[33];
    z[38]=z[38]*z[11];
    z[38]= - static_cast<T>(9)+ z[38];
    z[38]=z[38]*z[31];
    z[40]=n<T>(43,2) + 13*z[4];
    z[40]=z[4]*z[40];
    z[40]=n<T>(45,4) + z[40];
    z[40]=z[4]*z[40];
    z[38]=z[40] + z[38];
    z[38]=z[6]*z[38];
    z[35]=z[35]*z[16];
    z[40]=z[17]*z[31];
    z[41]=n<T>(3,2)*z[4];
    z[42]= - static_cast<T>(1)- z[41];
    z[42]=z[4]*z[42];
    z[40]=z[42] + z[40];
    z[42]=z[5]*z[6];
    z[40]=z[40]*z[42];
    z[43]= - n<T>(19,4) - 7*z[4];
    z[43]=z[4]*z[43];
    z[35]=n<T>(9,2)*z[40] + z[38] + z[43] + z[35];
    z[38]=n<T>(1,4)*z[5];
    z[35]=z[35]*z[38];
    z[40]= - static_cast<T>(3)+ 3*z[2];
    z[43]= - n<T>(13,2)*z[4] + z[40];
    z[43]=z[2]*z[43];
    z[43]=n<T>(7,2)*z[25] + z[43];
    z[24]=z[43]*z[24];
    z[18]=z[24] + n<T>(5,2)*z[18];
    z[18]=z[3]*z[18];
    z[24]= - z[16] + n<T>(5,2) - z[33];
    z[24]=z[24]*z[16];
    z[43]=z[4] - n<T>(1,4);
    z[44]=z[4]*z[43];
    z[44]= - n<T>(5,4) + z[44];
    z[24]=3*z[44] + z[24];
    z[15]=z[24]*z[15];
    z[15]=z[15] + z[18];
    z[18]=n<T>(1,4)*z[3];
    z[15]=z[15]*z[18];
    z[18]=z[34]*z[33];
    z[18]= - static_cast<T>(5)+ z[18];
    z[24]=n<T>(9,4)*z[4];
    z[34]= - n<T>(9,8)*z[2] + static_cast<T>(1)+ z[24];
    z[34]=z[2]*z[34];
    z[18]=n<T>(1,4)*z[18] + z[34];
    z[18]=z[18]*z[16];
    z[34]= - n<T>(49,16) - z[4];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(49,16) + z[34];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(9,8) + z[34];
    z[34]=z[4]*z[34];
    z[28]=z[34] - n<T>(1,16)*z[28];
    z[28]=z[6]*z[28];
    z[34]=n<T>(33,16) + z[4];
    z[34]=z[4]*z[34];
    z[34]=n<T>(17,16) + z[34];
    z[34]=z[4]*z[34];
    z[15]=z[35] + z[28] + z[15] + z[34] + z[18];
    z[15]=z[7]*z[15];
    z[18]=z[3]*npow(z[4],4);
    z[28]=3*z[13] - z[18];
    z[28]=z[28]*z[29];
    z[29]=z[13]*z[3];
    z[28]=z[28] - n<T>(9,4)*z[21] + z[29];
    z[28]=z[6]*z[28];
    z[22]=z[28] + z[22] + z[43];
    z[28]=static_cast<T>(1)- z[41];
    z[28]=z[4]*z[28];
    z[34]= - z[16] + static_cast<T>(1)- z[4];
    z[34]=z[2]*z[34];
    z[28]=z[34] - n<T>(1,2) + z[28];
    z[34]= - static_cast<T>(4)+ z[2];
    z[34]=z[34]*z[14];
    z[34]=static_cast<T>(2)+ z[34];
    z[34]=z[2]*z[34];
    z[34]= - n<T>(4,3) + z[34];
    z[34]=z[2]*z[34];
    z[34]=n<T>(1,3) + z[34];
    z[34]=z[3]*z[34];
    z[35]= - static_cast<T>(1)+ z[14];
    z[35]=z[2]*z[35];
    z[35]=static_cast<T>(1)+ z[35];
    z[35]=z[2]*z[35];
    z[35]= - n<T>(1,3) + z[35];
    z[34]=n<T>(617,16)*z[35] + 38*z[34];
    z[34]=z[3]*z[34];
    z[28]=n<T>(27,8)*z[28] + n<T>(1,3)*z[34];
    z[28]=z[3]*z[28];
    z[31]=z[4] - z[31];
    z[31]=z[6]*z[31];
    z[34]=z[6]*z[4];
    z[34]=z[34] - 1;
    z[27]=z[5]*z[34]*z[27];
    z[27]=z[27] - n<T>(1,2) + 3*z[31];
    z[27]=z[5]*z[27];
    z[22]=n<T>(27,8)*z[27] + z[28] + n<T>(27,4)*z[22];
    z[22]=z[1]*z[22];
    z[27]= - n<T>(313,9) + n<T>(9,2)*z[4];
    z[27]=z[27]*z[11];
    z[28]=static_cast<T>(443)+ n<T>(653,2)*z[4];
    z[28]=n<T>(1,3)*z[28] - 67*z[2];
    z[28]=z[28]*z[14];
    z[27]=z[28] - n<T>(283,9) + z[27];
    z[16]=z[27]*z[16];
    z[27]= - static_cast<T>(1)+ z[11];
    z[24]=z[27]*z[24];
    z[24]= - n<T>(85,9) + z[24];
    z[24]=z[4]*z[24];
    z[16]=z[16] + n<T>(41,18) + z[24];
    z[24]= - static_cast<T>(15)- n<T>(599,9)*z[4];
    z[27]= - n<T>(295,48)*z[2] + n<T>(38,3) + n<T>(121,16)*z[4];
    z[27]=z[2]*z[27];
    z[24]=n<T>(1,8)*z[24] + n<T>(5,3)*z[27];
    z[24]=z[2]*z[24];
    z[27]= - static_cast<T>(335)- n<T>(3049,8)*z[4];
    z[24]=n<T>(1,18)*z[27] + z[24];
    z[24]=z[2]*z[24];
    z[27]= - z[2] + z[37];
    z[27]=z[2]*z[27];
    z[27]=static_cast<T>(2)+ z[27];
    z[14]=z[27]*z[14];
    z[27]= - n<T>(4,3) - z[4];
    z[14]=2*z[27] + z[14];
    z[14]=z[2]*z[14];
    z[27]=static_cast<T>(7)+ z[36];
    z[14]=n<T>(1,3)*z[27] + z[14];
    z[14]=z[2]*z[14];
    z[14]=z[14] - n<T>(2,3) - z[4];
    z[14]=z[14]*z[39];
    z[27]=n<T>(1385,16) + 152*z[4];
    z[14]=z[14] + n<T>(1,9)*z[27] + z[24];
    z[14]=z[3]*z[14];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[3]*z[14];
    z[16]=static_cast<T>(1)+ n<T>(9,16)*z[4];
    z[16]=z[16]*z[21];
    z[24]=static_cast<T>(1)- z[32];
    z[24]=z[24]*z[29];
    z[16]=z[16] + n<T>(1,4)*z[24];
    z[24]= - n<T>(5,2) - z[4];
    z[24]=z[4]*z[24];
    z[24]= - n<T>(59,2) + z[24];
    z[24]=z[4]*z[24];
    z[24]=static_cast<T>(9)+ z[24];
    z[24]=z[4]*z[24];
    z[24]=z[24] + n<T>(1,2)*z[20];
    z[24]=z[10]*z[24];
    z[13]= - z[17]*z[13];
    z[13]=z[13] - z[18];
    z[13]=27*z[13] + z[20];
    z[13]=z[6]*z[13];
    z[13]=n<T>(1,16)*z[13] + 9*z[16] + n<T>(1,8)*z[24];
    z[13]=z[6]*z[13];
    z[16]=z[19]*z[21];
    z[18]= - n<T>(37,2) - z[33];
    z[18]=z[18]*z[11];
    z[18]= - static_cast<T>(9)+ z[18];
    z[20]=z[21]*z[10];
    z[18]=z[18]*z[20];
    z[16]=n<T>(27,2)*z[16] + z[18];
    z[16]=z[6]*z[16];
    z[18]=static_cast<T>(9)- z[11];
    z[18]=z[18]*z[20];
    z[24]=static_cast<T>(29)+ 19*z[4];
    z[24]=z[4]*z[24];
    z[24]=static_cast<T>(27)+ z[24];
    z[24]=z[4]*z[24];
    z[18]=z[24] + z[18];
    z[18]=z[10]*z[18];
    z[18]= - n<T>(99,2)*z[4] + z[18];
    z[16]=n<T>(1,2)*z[18] + z[16];
    z[16]=z[6]*z[16];
    z[17]=z[17]*z[20];
    z[17]=n<T>(3,2)*z[21] + z[17];
    z[17]=z[6]*z[17];
    z[18]= - z[10]*z[25];
    z[17]=z[17] - n<T>(11,2)*z[4] + z[18];
    z[17]=z[6]*z[17];
    z[18]=z[34]*z[42];
    z[17]= - n<T>(3,2)*z[18] + n<T>(3,2) + z[17];
    z[18]=n<T>(9,2)*z[5];
    z[17]=z[17]*z[18];
    z[18]= - z[30] - z[40];
    z[16]=z[17] + n<T>(9,4)*z[18] + z[16];
    z[16]=z[16]*z[38];
    z[17]= - z[19]*z[30];
    z[11]=n<T>(3,2)*z[2] - static_cast<T>(3)- z[11];
    z[11]=z[2]*z[11];
    z[11]=z[11] + n<T>(3,2) + z[17];

    r += n<T>(9,8)*z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[22] + 
      n<T>(1,4)*z[23] + z[26];
 
    return r;
}

template double qg_2lha_r752(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r752(const std::array<dd_real,30>&);
#endif
