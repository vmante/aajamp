#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r823(const std::array<T,30>& k) {
  T z[21];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[11];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[10];
    z[8]=k[20];
    z[9]=z[6] + 1;
    z[10]=z[9]*z[5]*z[6];
    z[10]=z[10] - 1;
    z[11]= - z[10]*npow(z[4],2);
    z[12]=z[4] - 1;
    z[12]=z[12]*z[4];
    z[13]=z[6]*z[12];
    z[11]=3*z[13] + z[11];
    z[13]=n<T>(1,2)*z[5];
    z[11]=z[11]*z[13];
    z[14]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[14]=z[14]*z[4];
    z[15]=static_cast<T>(1)+ z[14];
    z[15]=z[4]*z[15];
    z[16]=n<T>(1,2) - z[1];
    z[16]=z[6]*z[16];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[6]*z[16];
    z[11]=z[11] + z[15] + z[16];
    z[11]=z[5]*z[11];
    z[15]=z[1] - 1;
    z[16]=n<T>(1,2)*z[6];
    z[17]=z[15]*z[16];
    z[18]=z[1]*z[3];
    z[19]=z[15]*z[4];
    z[20]=z[19] + 1;
    z[20]=z[1]*z[20];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[4]*z[20];
    z[11]=z[11] + z[17] - z[18] + n<T>(1,2)*z[20];
    z[10]= - z[4]*z[10];
    z[9]=3*z[4] - z[9];
    z[9]=z[6]*z[9];
    z[9]=z[9] + z[10];
    z[9]=z[9]*z[13];
    z[10]=z[15]*z[6];
    z[9]=z[9] - z[10] + n<T>(1,2)*z[18] + z[14];
    z[9]=z[5]*z[9];
    z[14]=z[19] + z[3] - z[18];
    z[14]=z[1]*z[14];
    z[9]=n<T>(1,2)*z[14] + z[9];
    z[9]=z[2]*z[9];
    z[9]=n<T>(1,2)*z[11] + z[9];
    z[9]=z[2]*z[9];
    z[11]= - z[12] - z[16];
    z[11]=z[6]*z[11];
    z[13]=z[13]*npow(z[6],2);
    z[14]=z[12]*z[13];
    z[11]=z[11] + z[14];
    z[11]=z[5]*z[11];
    z[10]=z[10] + 1;
    z[14]=z[6]*z[10];
    z[12]=z[12] + z[14];
    z[11]=n<T>(1,2)*z[12] + z[11];
    z[9]=n<T>(1,2)*z[11] + z[9];
    z[9]=z[2]*z[9];
    z[11]=z[8] - 1;
    z[12]=z[11] + z[4];
    z[12]=z[4]*z[12];
    z[11]=z[7]*z[11];
    z[11]=z[12] - z[11];
    z[11]=z[8]*z[11];
    z[12]= - z[7]*z[16];
    z[12]=z[12] + z[11];
    z[12]=z[6]*z[12];
    z[13]= - z[11]*z[13];
    z[12]=z[12] + z[13];
    z[12]=z[5]*z[12];
    z[10]=z[6]*z[7]*z[10];
    z[10]=z[10] - z[11];
    z[10]=n<T>(1,2)*z[10] + z[12];
    z[9]=n<T>(1,2)*z[10] + z[9];

    r += n<T>(1,16)*z[9];
 
    return r;
}

template double qg_2lha_r823(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r823(const std::array<dd_real,30>&);
#endif
