#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1463(const std::array<T,30>& k) {
  T z[73];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[2];
    z[8]=k[7];
    z[9]=k[11];
    z[10]=k[24];
    z[11]=k[14];
    z[12]=k[10];
    z[13]=k[9];
    z[14]=k[5];
    z[15]= - z[1] + 1;
    z[16]=3*z[6];
    z[15]=z[16]*z[15];
    z[15]=n<T>(17,2) + z[15];
    z[15]=z[1]*z[15];
    z[15]=z[15] - static_cast<T>(17)+ z[6];
    z[17]=n<T>(1,8)*z[1];
    z[15]=z[15]*z[17];
    z[18]=3*z[14];
    z[19]=n<T>(59,2) - z[6];
    z[15]=z[15] + n<T>(1,8)*z[19] + z[18];
    z[19]=npow(z[11],4);
    z[20]=z[19]*z[2];
    z[21]=9*z[20];
    z[22]=n<T>(1,2)*z[14];
    z[23]= - n<T>(1,2) - 18*z[11];
    z[23]=z[11]*z[23];
    z[23]= - n<T>(1,2) + z[23];
    z[23]=z[11]*z[23];
    z[23]=z[21] + z[22] + z[23];
    z[23]=z[2]*z[23];
    z[24]=z[11] - 1;
    z[25]=2*z[11];
    z[26]= - z[24]*z[25];
    z[26]=n<T>(13,4) + z[26];
    z[27]=3*z[11];
    z[26]=z[26]*z[27];
    z[26]=static_cast<T>(1)+ z[26];
    z[26]=z[11]*z[26];
    z[28]=n<T>(13,16) - z[14];
    z[23]=z[23] + n<T>(3,4)*z[28] + z[26];
    z[23]=z[2]*z[23];
    z[26]=n<T>(7,3)*z[12];
    z[28]=z[26] - n<T>(39,4);
    z[29]=npow(z[10],2);
    z[28]=z[29] + n<T>(1,4)*z[28];
    z[30]=6*z[11];
    z[31]= - n<T>(13,2) + z[30];
    z[31]=z[11]*z[31];
    z[31]= - n<T>(1,2) + z[31];
    z[31]=z[11]*z[31];
    z[23]=z[23] + n<T>(1,2)*z[28] + z[31];
    z[23]=z[2]*z[23];
    z[31]=n<T>(3,4)*z[8];
    z[32]=z[31] + z[10];
    z[33]=n<T>(1,2)*z[6];
    z[34]= - static_cast<T>(71)+ z[33];
    z[34]=n<T>(1,8)*z[34] - z[18] - z[32];
    z[35]=n<T>(1,2)*z[9];
    z[36]= - n<T>(13,4) - n<T>(5,3)*z[2];
    z[36]=z[36]*z[35];
    z[36]=z[36] + n<T>(25,8)*z[2] + n<T>(57,8) + z[14];
    z[36]=z[36]*z[35];
    z[34]=z[36] - n<T>(125,96)*z[2] + n<T>(1,2)*z[34] - z[11];
    z[34]=z[9]*z[34];
    z[36]=n<T>(1,3)*z[8];
    z[37]=z[36]*z[12];
    z[38]=n<T>(3,2) + z[12];
    z[38]=n<T>(1,2)*z[38] + z[37];
    z[39]=n<T>(1,2)*z[8];
    z[38]=z[38]*z[39];
    z[40]=z[11] - n<T>(5,2);
    z[40]=z[40]*z[11];
    z[41]=n<T>(1,2)*z[10];
    z[42]=z[41] - 1;
    z[42]=z[42]*z[10];
    z[15]=z[34] + z[23] - z[40] + z[38] + n<T>(1,4)*z[15] - z[42];
    z[15]=z[4]*z[15];
    z[23]=n<T>(17,4) - z[8];
    z[23]=z[23]*z[36];
    z[23]= - n<T>(1,8) + z[23];
    z[23]=z[8]*z[23];
    z[34]=n<T>(49,8) + z[6];
    z[34]=z[6]*z[34];
    z[34]=n<T>(79,12) + z[34];
    z[38]= - n<T>(1,2) + z[10];
    z[38]=z[10]*z[38];
    z[43]=n<T>(3,2) - z[11];
    z[43]=z[11]*z[43];
    z[23]=n<T>(97,96)*z[2] + z[43] + z[23] + 3*z[38] + n<T>(1,4)*z[34] + z[14];
    z[34]=n<T>(2,3)*z[9];
    z[38]= - static_cast<T>(1)+ 2*z[2];
    z[38]=z[38]*z[34];
    z[43]= - n<T>(125,6) - z[6];
    z[43]=n<T>(1,4)*z[43] - z[14];
    z[38]=z[38] - n<T>(269,96)*z[2] + z[25] + n<T>(1,4)*z[43] - z[32];
    z[38]=z[9]*z[38];
    z[43]=n<T>(1,4)*z[8];
    z[44]=3*z[8];
    z[45]=static_cast<T>(13)- z[44];
    z[45]=z[45]*z[43];
    z[46]= - static_cast<T>(11)+ z[6];
    z[46]=z[6]*z[46];
    z[46]=n<T>(31,3) + n<T>(1,4)*z[46];
    z[46]=n<T>(1,2)*z[46] + z[14];
    z[47]=static_cast<T>(5)- z[10];
    z[47]=z[10]*z[47];
    z[45]=z[45] + n<T>(1,2)*z[46] + z[47];
    z[38]=z[38] + n<T>(31,48)*z[2] + n<T>(1,2)*z[45] + z[40];
    z[38]=z[9]*z[38];
    z[23]=n<T>(1,2)*z[23] + z[38];
    z[23]=z[9]*z[23];
    z[38]=n<T>(3,2)*z[1];
    z[40]= - n<T>(1,2) - z[6];
    z[40]=z[40]*z[38];
    z[40]=z[40] + n<T>(17,4) + z[16];
    z[40]=z[17]*z[40];
    z[45]= - n<T>(7,2) - z[6];
    z[40]=z[40] + n<T>(5,16)*z[45];
    z[40]=z[6]*z[40];
    z[45]=5*z[12];
    z[46]=z[45] + n<T>(3,2);
    z[46]=n<T>(1,2)*z[46];
    z[47]= - n<T>(3,2) - z[26];
    z[47]=z[8]*z[47];
    z[47]= - z[46] + z[47];
    z[47]=z[47]*z[39];
    z[48]= - static_cast<T>(1)- z[41];
    z[48]=z[10]*z[48];
    z[40]=z[47] + z[48] - z[22] - static_cast<T>(1)+ z[40];
    z[28]= - z[18] - z[28];
    z[47]=static_cast<T>(1)- z[25];
    z[47]=z[47]*z[27];
    z[47]=n<T>(47,4) + z[47];
    z[47]=z[11]*z[47];
    z[47]=static_cast<T>(1)+ z[47];
    z[47]=z[11]*z[47];
    z[48]= - static_cast<T>(38)+ z[27];
    z[48]=z[11]*z[48];
    z[48]= - static_cast<T>(1)+ z[48];
    z[48]=z[11]*z[48];
    z[48]= - static_cast<T>(1)+ z[48];
    z[48]=z[11]*z[48];
    z[48]=27*z[20] + z[14] + z[48];
    z[48]=z[2]*z[48];
    z[28]=z[48] + n<T>(1,4)*z[28] + z[47];
    z[28]=z[2]*z[28];
    z[47]= - static_cast<T>(4)+ z[27];
    z[47]=z[47]*z[25];
    z[47]=n<T>(3,2) + z[47];
    z[48]=npow(z[11],2);
    z[47]=z[47]*z[48];
    z[15]=z[15] + z[23] + z[28] + n<T>(1,2)*z[40] + z[47];
    z[15]=z[4]*z[15];
    z[23]= - n<T>(39,4) - z[6];
    z[23]=z[6]*z[23];
    z[23]=z[26] + z[23];
    z[16]= - n<T>(17,2) - z[16];
    z[16]=z[6]*z[16];
    z[26]=npow(z[6],2)*z[38];
    z[16]=z[16] + z[26];
    z[16]=z[1]*z[16];
    z[26]=static_cast<T>(17)+ n<T>(5,2)*z[6];
    z[26]=z[6]*z[26];
    z[16]=z[26] + z[16];
    z[16]=z[16]*z[17];
    z[26]=npow(z[8],2);
    z[28]=n<T>(1,2)*z[12] + z[37];
    z[28]=z[28]*z[26];
    z[16]=z[28] + z[16] + n<T>(1,4)*z[23] + z[14];
    z[23]=n<T>(1,2)*z[26];
    z[28]=n<T>(1,4) - z[36];
    z[28]=z[28]*z[23];
    z[38]=5*z[11];
    z[40]=n<T>(1,2)*z[11];
    z[47]=static_cast<T>(1)- z[40];
    z[47]=z[47]*z[38];
    z[49]=n<T>(1,2)*z[29];
    z[50]=n<T>(3,4)*z[6];
    z[51]=static_cast<T>(11)+ z[50];
    z[51]=z[6]*z[51];
    z[51]= - n<T>(49,12) + z[51];
    z[28]=z[47] + z[28] + z[49] + n<T>(1,8)*z[51] + z[14];
    z[47]=z[31] - 1;
    z[47]=z[47]*z[39];
    z[42]=z[47] + z[42];
    z[47]= - static_cast<T>(1)+ n<T>(1,8)*z[6];
    z[47]=z[6]*z[47];
    z[47]= - n<T>(101,24) + z[47];
    z[18]=n<T>(1,2)*z[47] - z[18];
    z[47]= - n<T>(11,2) + z[11];
    z[47]=z[11]*z[47];
    z[18]=z[47] + n<T>(1,2)*z[18] - z[42];
    z[47]=static_cast<T>(29)- z[6];
    z[47]=n<T>(1,8)*z[47] + z[14];
    z[47]=n<T>(1,2)*z[47] - z[32];
    z[51]=n<T>(1,3)*z[9];
    z[47]= - z[51] + n<T>(1,2)*z[47] + z[11];
    z[47]=z[9]*z[47];
    z[18]=n<T>(1,2)*z[18] + z[47];
    z[18]=z[9]*z[18];
    z[18]=n<T>(1,2)*z[28] + z[18];
    z[18]=z[9]*z[18];
    z[28]=n<T>(1,4)*z[14];
    z[47]= - static_cast<T>(1)- z[11];
    z[47]=z[11]*z[47];
    z[47]= - n<T>(1,4) + 9*z[47];
    z[47]=z[11]*z[47];
    z[47]= - n<T>(1,4) + z[47];
    z[47]=z[11]*z[47];
    z[47]=z[21] + z[28] + z[47];
    z[47]=z[2]*z[47];
    z[52]=static_cast<T>(4)- z[11];
    z[52]=z[52]*z[27];
    z[52]=n<T>(1,2) + z[52];
    z[52]=z[11]*z[52];
    z[52]=n<T>(1,2) + z[52];
    z[52]=z[11]*z[52];
    z[47]=z[47] - z[22] + z[52];
    z[47]=z[2]*z[47];
    z[52]=z[24]*z[27];
    z[52]= - n<T>(1,4) + z[52];
    z[52]=z[11]*z[52];
    z[52]= - n<T>(1,4) + z[52];
    z[52]=z[11]*z[52];
    z[16]=z[18] + z[47] + n<T>(1,4)*z[16] + z[52];
    z[16]=z[4]*z[16];
    z[18]= - static_cast<T>(5)- z[30];
    z[18]=z[18]*z[25];
    z[18]= - n<T>(1,4) + z[18];
    z[18]=z[11]*z[18];
    z[18]= - n<T>(1,4) + z[18];
    z[18]=z[11]*z[18];
    z[25]=18*z[20];
    z[18]=z[25] + z[28] + z[18];
    z[18]=z[2]*z[18];
    z[28]= - static_cast<T>(1)+ z[8];
    z[28]=z[28]*z[31];
    z[47]= - n<T>(3,2) + z[10];
    z[47]=z[10]*z[47];
    z[52]=n<T>(3,2)*z[8];
    z[53]=n<T>(4,3)*z[9] + z[52] - n<T>(87,32) + 2*z[10];
    z[53]=z[9]*z[53];
    z[28]=z[53] + z[28] + n<T>(101,96) + z[47];
    z[28]=z[9]*z[28];
    z[47]= - n<T>(3,16) + z[36];
    z[47]=z[47]*z[26];
    z[53]=n<T>(49,48) - 3*z[29];
    z[28]=z[28] + n<T>(1,4)*z[53] + z[47];
    z[28]=z[9]*z[28];
    z[47]= - 2*z[19] + z[20];
    z[47]=z[2]*z[47];
    z[47]=z[19] + z[47];
    z[47]=z[4]*z[47];
    z[20]=z[47] - z[19] + z[20];
    z[20]=z[5]*z[20];
    z[37]= - n<T>(3,8)*z[12] - z[37];
    z[37]=z[37]*z[26];
    z[47]= - n<T>(7,12)*z[12] - z[14];
    z[53]=static_cast<T>(1)+ z[27];
    z[53]=z[11]*z[53];
    z[53]=n<T>(1,4) + z[53];
    z[53]=z[11]*z[53];
    z[53]=n<T>(1,4) + z[53];
    z[53]=z[11]*z[53];
    z[16]=3*z[20] + z[16] + z[28] + z[18] + z[53] + n<T>(1,4)*z[47] + z[37];
    z[16]=z[4]*z[16];
    z[18]=z[29] + n<T>(3,4)*z[26];
    z[20]=n<T>(1,2)*z[7];
    z[28]=z[18]*z[20];
    z[37]= - static_cast<T>(1)- z[44];
    z[37]=z[37]*z[43];
    z[37]= - z[28] - z[29] + z[37];
    z[37]=z[7]*z[37];
    z[47]=static_cast<T>(1)- n<T>(9,2)*z[8];
    z[47]=z[47]*z[43];
    z[53]=static_cast<T>(1)- n<T>(3,2)*z[10];
    z[53]=z[10]*z[53];
    z[54]=npow(z[13],2);
    z[55]=z[54]*z[2];
    z[56]=n<T>(1,2)*z[55];
    z[57]= - z[13] - z[56];
    z[57]=z[2]*z[57];
    z[37]=n<T>(51,8)*z[57] + z[37] + z[47] - n<T>(137,96) + z[53];
    z[47]=n<T>(1,3)*z[7];
    z[53]= - static_cast<T>(2)- z[7];
    z[53]=z[53]*z[47];
    z[57]=4*z[13];
    z[58]= - z[57] - z[55];
    z[58]=z[2]*z[58];
    z[58]= - static_cast<T>(1)+ z[58];
    z[58]=z[2]*z[58];
    z[59]=z[7] + 1;
    z[58]= - 2*z[59] + z[58];
    z[60]=n<T>(1,3)*z[2];
    z[58]=z[58]*z[60];
    z[53]=z[58] - static_cast<T>(1)+ z[53];
    z[53]=z[9]*z[53];
    z[58]= - z[20] - 1;
    z[58]=z[32]*z[58];
    z[58]=n<T>(37,32) + z[58];
    z[58]=z[7]*z[58];
    z[61]=n<T>(11,8) - z[32];
    z[62]=107*z[13] + 35*z[55];
    z[62]=z[2]*z[62];
    z[62]=n<T>(33,2) + z[62];
    z[62]=z[2]*z[62];
    z[53]=z[53] + n<T>(1,16)*z[62] + n<T>(3,2)*z[61] + z[58];
    z[53]=z[9]*z[53];
    z[37]=n<T>(1,2)*z[37] + z[53];
    z[37]=z[9]*z[37];
    z[53]= - n<T>(5,8) - z[8];
    z[53]=z[53]*z[26];
    z[58]=npow(z[8],3);
    z[61]=z[58]*z[7];
    z[49]= - n<T>(1,6)*z[61] - z[49] + n<T>(1,3)*z[53];
    z[49]=z[7]*z[49];
    z[53]=n<T>(1,2)*z[13];
    z[62]= - n<T>(1,6) - z[8];
    z[62]=z[8]*z[62];
    z[62]= - n<T>(1,12) + z[62];
    z[62]=z[8]*z[62];
    z[62]=z[62] + z[10] - z[53];
    z[49]=n<T>(1,2)*z[62] + z[49];
    z[37]=n<T>(1,2)*z[49] + z[37];
    z[37]=z[9]*z[37];
    z[49]=z[8]*z[12];
    z[62]=n<T>(7,4)*z[12] + z[49];
    z[62]=z[62]*z[26];
    z[63]=z[12]*z[58]*z[20];
    z[62]=z[62] + z[63];
    z[62]=z[62]*z[47];
    z[63]=n<T>(5,3)*z[12];
    z[49]=z[63] + z[49];
    z[49]=z[8]*z[49];
    z[64]=n<T>(5,6)*z[12];
    z[49]=z[64] + z[49];
    z[49]=z[49]*z[39];
    z[49]=z[49] + z[62];
    z[62]=2*z[13];
    z[65]=z[62] + 1;
    z[66]= - z[27] - z[65];
    z[66]=z[11]*z[66];
    z[66]=z[62] + z[66];
    z[48]=z[66]*z[48];
    z[16]=z[37] + z[21] + n<T>(1,2)*z[49] + z[48] + z[16];
    z[16]=z[5]*z[16];
    z[21]=z[14] + 1;
    z[37]=n<T>(1,4)*z[13];
    z[48]=z[21]*z[37];
    z[49]=z[32]*z[7];
    z[66]=z[49] - n<T>(2,3) + z[32];
    z[66]=z[7]*z[66];
    z[66]=z[66] - n<T>(1,96) + z[32];
    z[66]=z[7]*z[66];
    z[67]= - n<T>(325,2) - 71*z[13];
    z[67]=z[13]*z[67];
    z[67]=n<T>(1,12)*z[67] + z[55];
    z[67]=z[2]*z[67];
    z[68]= - static_cast<T>(103)+ n<T>(461,3)*z[13];
    z[67]=n<T>(1,8)*z[68] + z[67];
    z[67]=z[2]*z[67];
    z[67]=z[67] + n<T>(293,24)*z[7] + n<T>(83,6) + z[13];
    z[68]=n<T>(1,4)*z[2];
    z[67]=z[67]*z[68];
    z[69]=z[2]*z[13];
    z[70]=z[69] + 1;
    z[71]= - z[62] + z[70];
    z[71]=z[2]*z[71];
    z[71]=z[71] - z[59];
    z[71]=z[2]*z[71];
    z[72]=npow(z[7],2);
    z[71]=z[71] - static_cast<T>(1)+ z[72];
    z[71]=z[2]*z[71];
    z[59]=z[7]*z[59];
    z[59]=static_cast<T>(1)+ z[59];
    z[59]=z[7]*z[59];
    z[59]=z[71] + static_cast<T>(1)+ z[59];
    z[34]=z[59]*z[34];
    z[34]=z[34] + z[67] + z[66] + z[48] + n<T>(31,48) + z[32];
    z[34]=z[9]*z[34];
    z[28]=z[28] + z[42];
    z[28]=z[7]*z[28];
    z[42]= - static_cast<T>(7)+ z[44];
    z[42]=z[42]*z[43];
    z[44]=z[10] - 3;
    z[44]=z[44]*z[10];
    z[42]=z[42] + n<T>(5,12) + z[44];
    z[28]=n<T>(1,2)*z[42] + z[28];
    z[28]=z[7]*z[28];
    z[42]= - static_cast<T>(5)+ z[52];
    z[42]=z[42]*z[43];
    z[43]=n<T>(121,2) + 83*z[13];
    z[43]=z[43]*z[37];
    z[43]=z[43] - z[55];
    z[43]=z[2]*z[43];
    z[52]=static_cast<T>(3)- n<T>(5,2)*z[13];
    z[43]=n<T>(3,4)*z[52] + z[43];
    z[43]=z[43]*z[68];
    z[41]= - static_cast<T>(2)+ z[41];
    z[41]=z[10]*z[41];
    z[28]=z[34] + z[43] + z[28] + z[42] - z[48] - n<T>(23,48) + z[41];
    z[28]=z[9]*z[28];
    z[34]= - n<T>(5,4) + z[8];
    z[34]=z[34]*z[26];
    z[34]=z[34] + z[61];
    z[34]=z[34]*z[47];
    z[41]= - n<T>(1,2) - z[10];
    z[41]=z[10]*z[41];
    z[42]= - n<T>(3,4) + z[36];
    z[42]=z[8]*z[42];
    z[42]= - n<T>(5,24) + z[42];
    z[42]=z[8]*z[42];
    z[34]=z[34] + z[41] + z[42];
    z[34]=z[34]*z[20];
    z[41]= - n<T>(3,2) - z[14];
    z[41]=z[41]*z[37];
    z[42]= - static_cast<T>(29)- 51*z[13];
    z[42]=z[42]*z[69];
    z[43]=n<T>(1,4) - z[10];
    z[43]=z[10]*z[43];
    z[48]= - n<T>(13,4) + z[8];
    z[48]=z[8]*z[48];
    z[48]= - n<T>(5,8) + z[48];
    z[48]=z[8]*z[48];
    z[28]=z[28] + n<T>(1,16)*z[42] + z[34] + n<T>(1,6)*z[48] + z[41] - n<T>(41,96)
    + z[43];
    z[28]=z[9]*z[28];
    z[34]=z[12]*z[26]*z[47];
    z[41]=n<T>(3,4) + z[12];
    z[41]=z[8]*z[41];
    z[41]=n<T>(7,6)*z[12] + z[41];
    z[41]=z[8]*z[41];
    z[34]=z[34] - z[29] + z[41];
    z[34]=z[7]*z[34];
    z[41]=n<T>(3,2) + z[63];
    z[41]=z[8]*z[41];
    z[41]=z[46] + z[41];
    z[41]=z[8]*z[41];
    z[42]=static_cast<T>(1)- z[10];
    z[42]=z[10]*z[42];
    z[34]=z[34] + z[41] - z[53] + z[64] + z[42];
    z[41]= - z[7]*z[13];
    z[41]=z[62] + z[41];
    z[42]=z[65]*z[7];
    z[43]= - z[62] + z[42];
    z[46]= - z[7]*z[27];
    z[43]=2*z[43] + z[46];
    z[43]=z[11]*z[43];
    z[41]=2*z[41] + z[43];
    z[41]=z[11]*z[41];
    z[37]=z[37] + z[41];
    z[37]=z[11]*z[37];
    z[41]=static_cast<T>(4)- 3*z[7];
    z[41]=z[41]*z[27];
    z[41]= - static_cast<T>(20)+ z[41];
    z[41]=z[11]*z[41];
    z[41]= - n<T>(1,2) + z[41];
    z[41]=z[11]*z[41];
    z[41]= - n<T>(1,2) + z[41];
    z[41]=z[11]*z[41];
    z[25]=z[25] + z[22] + z[41];
    z[25]=z[2]*z[25];
    z[15]=z[16] + z[15] + z[28] + z[25] + n<T>(1,4)*z[34] + z[37];
    z[15]=z[5]*z[15];
    z[16]=3*z[3];
    z[25]= - n<T>(1,2) - z[16];
    z[25]=3*z[25] - n<T>(149,6)*z[13];
    z[25]=z[25]*z[53];
    z[25]= - static_cast<T>(1)+ z[25];
    z[28]=z[54] - n<T>(1,4)*z[55];
    z[28]=z[2]*z[28];
    z[25]=n<T>(1,4)*z[25] + z[28];
    z[25]=z[2]*z[25];
    z[28]=static_cast<T>(1)+ n<T>(33,2)*z[13];
    z[28]=n<T>(5,4)*z[28] + z[7];
    z[25]=n<T>(1,2)*z[28] + z[25];
    z[25]=z[2]*z[25];
    z[28]= - static_cast<T>(5)- z[6];
    z[34]= - n<T>(47,48) - z[49];
    z[34]=z[7]*z[34];
    z[34]= - n<T>(13,16) + z[34];
    z[34]=z[7]*z[34];
    z[34]= - n<T>(5,16) + z[34];
    z[34]=z[7]*z[34];
    z[28]=n<T>(1,16)*z[28] + z[34];
    z[34]=static_cast<T>(21)- n<T>(149,3)*z[13];
    z[37]=n<T>(25,4)*z[13] - z[70];
    z[37]=z[2]*z[37];
    z[34]=z[37] + n<T>(1,4)*z[34] + z[7];
    z[34]=z[2]*z[34];
    z[37]= - n<T>(17,4) - z[7];
    z[37]=z[7]*z[37];
    z[34]=z[34] - n<T>(15,2) + z[37];
    z[34]=z[34]*z[68];
    z[37]= - npow(z[7],4)*z[51];
    z[28]=z[37] + z[34] + n<T>(1,2)*z[28] + z[11];
    z[28]=z[9]*z[28];
    z[34]= - static_cast<T>(7)+ z[33];
    z[34]=z[6]*z[34];
    z[34]= - n<T>(1,2) + z[34];
    z[21]=z[13]*z[21];
    z[21]=n<T>(1,8)*z[34] + z[21];
    z[34]=n<T>(1,4)*z[7];
    z[18]= - z[18]*z[34];
    z[18]=z[18] + z[10] + n<T>(5,8)*z[8];
    z[18]=z[7]*z[18];
    z[18]=n<T>(57,64) + z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(5,16) + z[18];
    z[18]=z[7]*z[18];
    z[37]=n<T>(1,2) + z[11];
    z[37]=z[37]*z[40];
    z[18]=z[28] + n<T>(1,2)*z[25] + z[37] + n<T>(1,4)*z[21] + z[18];
    z[18]=z[9]*z[18];
    z[21]=z[58]*z[47];
    z[25]= - z[21] + z[29] + n<T>(5,4)*z[26];
    z[25]=z[25]*z[20];
    z[25]=z[25] - z[32];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(61,96) + z[25];
    z[25]=z[7]*z[25];
    z[26]=static_cast<T>(3)+ 5*z[6];
    z[26]=z[26]*z[33];
    z[26]= - static_cast<T>(11)+ z[26];
    z[28]= - n<T>(39,8) - z[14];
    z[28]=z[13]*z[28];
    z[26]=n<T>(1,8)*z[26] + z[28];
    z[28]=n<T>(3,2)*z[13];
    z[32]=z[28] - 1;
    z[33]=z[32]*z[13];
    z[37]= - z[33] + z[56];
    z[37]=z[2]*z[37];
    z[40]=9*z[3];
    z[41]=z[40] + 23;
    z[43]=z[3]*z[41];
    z[43]=61*z[13] - n<T>(141,2) + z[43];
    z[43]=z[13]*z[43];
    z[37]=z[37] + static_cast<T>(1)+ n<T>(1,8)*z[43];
    z[37]=z[2]*z[37];
    z[24]=z[11]*z[24];
    z[24]=z[37] + n<T>(3,2)*z[24] + n<T>(1,2)*z[26] + z[25];
    z[18]=n<T>(1,2)*z[24] + z[18];
    z[18]=z[9]*z[18];
    z[24]= - static_cast<T>(1)- n<T>(51,32)*z[1];
    z[24]=z[3]*z[24];
    z[24]=n<T>(85,32) + z[24];
    z[24]=z[3]*z[24];
    z[22]=n<T>(1,4)*z[54] + z[22] + z[24];
    z[24]= - static_cast<T>(3)+ z[11];
    z[24]=z[24]*z[27];
    z[24]= - n<T>(1,4) + z[24];
    z[24]=z[11]*z[24];
    z[24]= - n<T>(1,4) + z[24];
    z[24]=z[11]*z[24];
    z[25]=npow(z[3],2);
    z[26]=n<T>(17,64)*z[25];
    z[37]=z[26] + 3*z[19];
    z[37]=z[2]*z[37];
    z[22]=z[37] + n<T>(1,2)*z[22] + z[24];
    z[22]=z[2]*z[22];
    z[24]=n<T>(3,4) - z[13];
    z[24]=z[24]*z[53];
    z[29]=z[29] - z[14];
    z[37]=n<T>(107,16) + z[29];
    z[43]=static_cast<T>(1)+ n<T>(51,64)*z[1];
    z[43]=z[1]*z[43];
    z[43]=n<T>(33,64) + z[43];
    z[43]=z[3]*z[43];
    z[43]=z[43] - static_cast<T>(2)- n<T>(85,32)*z[1];
    z[43]=z[3]*z[43];
    z[30]=n<T>(37,4) - z[30];
    z[30]=z[11]*z[30];
    z[30]=n<T>(1,2) + z[30];
    z[30]=z[11]*z[30];
    z[22]=z[22] + z[30] + z[24] + n<T>(1,4)*z[37] + z[43];
    z[22]=z[2]*z[22];
    z[24]= - n<T>(47,2) + z[12];
    z[24]= - z[44] - n<T>(93,16)*z[1] + n<T>(7,12)*z[24] - z[14];
    z[30]=static_cast<T>(81)+ n<T>(85,2)*z[1];
    z[30]=z[1]*z[30];
    z[30]=n<T>(99,2) + z[30];
    z[37]= - static_cast<T>(1)- n<T>(17,32)*z[1];
    z[37]=z[1]*z[37];
    z[37]= - n<T>(33,32) + z[37];
    z[37]=z[3]*z[1]*z[37];
    z[30]=n<T>(1,16)*z[30] + z[37];
    z[30]=z[3]*z[30];
    z[37]=z[13] - n<T>(3,2);
    z[28]=z[37]*z[28];
    z[24]=z[28] + n<T>(1,2)*z[24] + z[30];
    z[28]= - static_cast<T>(4)+ n<T>(7,2)*z[11];
    z[28]=z[11]*z[28];
    z[22]=z[22] + n<T>(1,2)*z[24] + z[28];
    z[22]=z[2]*z[22];
    z[24]=n<T>(17,2)*z[1];
    z[28]=static_cast<T>(75)- z[24];
    z[28]=z[28]*z[17];
    z[28]=z[28] - n<T>(7,16) - z[14];
    z[30]=17*z[1];
    z[43]=z[30] + 15;
    z[44]=n<T>(1,2)*z[1];
    z[43]=z[43]*z[44];
    z[43]=z[43] + 9;
    z[46]=n<T>(1,8)*z[3];
    z[47]= - z[43]*z[46];
    z[48]=n<T>(9,4) - z[13];
    z[48]=z[13]*z[48];
    z[28]=z[48] + z[47] + n<T>(1,2)*z[28] - z[10];
    z[22]=z[22] + n<T>(1,2)*z[28] - z[11];
    z[22]=z[2]*z[22];
    z[28]= - static_cast<T>(3)+ z[13];
    z[28]=z[28]*z[53];
    z[47]=static_cast<T>(1)+ z[12];
    z[47]=z[47]*z[36];
    z[48]=n<T>(21,4) + z[12];
    z[49]=npow(z[1],2);
    z[49]= - static_cast<T>(25)+ 3*z[49];
    z[49]=z[1]*z[49];
    z[28]=z[47] + z[28] + n<T>(1,16)*z[49] + n<T>(1,2)*z[48] - z[14];
    z[47]=n<T>(1,2) - z[3];
    z[47]=z[2]*z[47];
    z[47]=9*z[47] + n<T>(115,2) + z[40];
    z[47]=z[47]*z[68];
    z[48]= - static_cast<T>(13)+ z[60];
    z[48]=z[2]*z[48];
    z[48]=n<T>(13,4) + z[48];
    z[35]=z[48]*z[35];
    z[35]=z[35] + z[47] - n<T>(7,4) + z[14];
    z[47]=n<T>(1,4)*z[9];
    z[35]=z[35]*z[47];
    z[22]=z[35] + n<T>(1,4)*z[28] + z[22];
    z[22]=z[4]*z[22];
    z[28]= - static_cast<T>(5)- z[30];
    z[28]=z[3]*z[28];
    z[28]=n<T>(51,2) + z[28];
    z[28]=z[28]*z[46];
    z[28]=z[14] + z[28];
    z[28]=3*z[28] + z[54];
    z[25]=n<T>(17,32)*z[25] + 12*z[19];
    z[25]=z[2]*z[25];
    z[35]= - static_cast<T>(7)+ z[27];
    z[35]=z[11]*z[35];
    z[35]= - n<T>(3,4) + 4*z[35];
    z[35]=z[11]*z[35];
    z[35]= - n<T>(3,4) + z[35];
    z[35]=z[11]*z[35];
    z[25]=z[25] + n<T>(1,4)*z[28] + z[35];
    z[25]=z[2]*z[25];
    z[28]=static_cast<T>(5)+ z[24];
    z[28]=z[1]*z[28];
    z[28]=static_cast<T>(3)+ z[28];
    z[28]=z[28]*z[16];
    z[35]=153*z[1];
    z[48]= - n<T>(71,2) - z[35];
    z[28]=n<T>(1,2)*z[48] + z[28];
    z[28]=z[3]*z[28];
    z[28]=n<T>(1,4)*z[28] + n<T>(73,8) + z[29];
    z[28]=n<T>(1,2)*z[28] - z[33];
    z[29]=n<T>(81,4) - 16*z[11];
    z[29]=z[11]*z[29];
    z[29]=n<T>(3,4) + z[29];
    z[29]=z[11]*z[29];
    z[25]=z[25] + n<T>(1,2)*z[28] + z[29];
    z[25]=z[2]*z[25];
    z[28]= - z[1]*z[43];
    z[28]=static_cast<T>(9)+ z[28];
    z[28]=z[3]*z[28];
    z[29]=static_cast<T>(71)+ z[35];
    z[29]=z[1]*z[29];
    z[28]=z[28] + static_cast<T>(23)+ n<T>(1,4)*z[29];
    z[28]=z[28]*z[46];
    z[29]= - n<T>(33,4)*z[1] - n<T>(33,4) - z[14];
    z[28]=z[28] + n<T>(1,2)*z[29] + z[10];
    z[29]=n<T>(3,4)*z[13];
    z[33]= - static_cast<T>(1)+ z[29];
    z[33]=z[13]*z[33];
    z[35]= - n<T>(21,4) + z[38];
    z[35]=z[11]*z[35];
    z[25]=z[25] + z[35] + n<T>(1,2)*z[28] + z[33];
    z[25]=z[2]*z[25];
    z[28]=n<T>(39,2) - 7*z[1];
    z[17]=z[28]*z[17];
    z[17]=z[17] - z[14] - n<T>(7,2) - z[12];
    z[28]= - n<T>(1,2) - z[12];
    z[28]=z[28]*z[39];
    z[33]=static_cast<T>(1)- z[53];
    z[33]=z[13]*z[33];
    z[17]=z[28] + n<T>(1,2)*z[17] + z[33];
    z[28]=n<T>(1,2)*z[3];
    z[33]= - z[41]*z[28];
    z[33]=z[13] + static_cast<T>(21)+ z[33];
    z[35]=z[2]*z[53];
    z[35]=z[35] - z[37];
    z[35]=z[2]*z[35];
    z[33]=n<T>(1,2)*z[33] + z[35];
    z[33]=z[2]*z[33];
    z[35]= - n<T>(13,2) + z[40];
    z[35]=z[2]*z[35];
    z[35]= - n<T>(151,2) + z[35];
    z[35]=z[35]*z[68];
    z[37]=n<T>(43,4) - z[60];
    z[37]=z[2]*z[37];
    z[37]= - n<T>(13,4) + z[37];
    z[37]=z[9]*z[37];
    z[35]=z[37] + z[35] - n<T>(3,4) - z[14];
    z[35]=z[9]*z[35];
    z[33]=z[35] + z[33] + n<T>(19,2) + z[14];
    z[33]=z[33]*z[47];
    z[17]=z[22] + z[33] + z[25] + n<T>(1,2)*z[17] - z[11];
    z[17]=z[4]*z[17];
    z[22]=z[6] - 1;
    z[25]= - z[22]*z[50];
    z[33]=n<T>(29,4) + z[45];
    z[25]=n<T>(1,3)*z[33] + z[25];
    z[33]=static_cast<T>(1)- z[24];
    z[33]=z[33]*z[44];
    z[33]= - static_cast<T>(5)+ z[33];
    z[33]=z[1]*z[33];
    z[33]=n<T>(19,2) + z[33];
    z[33]=z[33]*z[28];
    z[24]= - static_cast<T>(5)+ z[24];
    z[24]=z[1]*z[24];
    z[24]=z[33] + n<T>(19,2) + z[24];
    z[24]=z[24]*z[28];
    z[33]=z[28] + 1;
    z[16]=z[33]*z[16];
    z[16]= - n<T>(17,2) + z[16];
    z[16]=z[16]*z[29];
    z[29]=z[3]*z[33];
    z[16]=z[16] - 7*z[29] + static_cast<T>(5)- z[14];
    z[16]=z[13]*z[16];
    z[22]=z[6]*z[22];
    z[22]= - static_cast<T>(13)+ z[22];
    z[22]=z[1]*z[22];
    z[16]=z[16] + z[24] + n<T>(3,16)*z[22] + n<T>(1,2)*z[25] + z[14];
    z[22]=n<T>(5,2) + z[12];
    z[24]=z[1]*z[31];
    z[22]=n<T>(1,3)*z[22] + z[24];
    z[22]=z[8]*z[22];
    z[21]= - z[23] + z[21];
    z[21]=z[7]*z[21];
    z[23]= - static_cast<T>(1)+ z[13];
    z[23]=z[14]*z[23];
    z[21]=z[21] + z[22] + z[23];
    z[20]=z[21]*z[20];
    z[21]=n<T>(1,4) + z[12];
    z[21]=z[21]*z[36];
    z[16]=z[20] + n<T>(1,2)*z[16] + z[21];
    z[20]=static_cast<T>(1)- n<T>(51,2)*z[1];
    z[20]=z[20]*z[28];
    z[20]=static_cast<T>(17)+ z[20];
    z[20]=z[20]*z[46];
    z[20]=z[14] + z[20];
    z[21]=static_cast<T>(5)- 2*z[7];
    z[21]=z[21]*z[27];
    z[21]= - static_cast<T>(19)+ z[21];
    z[21]=z[11]*z[21];
    z[21]= - n<T>(1,2) + z[21];
    z[21]=z[11]*z[21];
    z[21]= - n<T>(1,2) + z[21];
    z[21]=z[11]*z[21];
    z[19]=z[26] + 9*z[19];
    z[19]=z[2]*z[19];
    z[19]=z[19] + n<T>(1,2)*z[20] + z[21];
    z[19]=z[2]*z[19];
    z[20]= - static_cast<T>(1)+ n<T>(51,4)*z[1];
    z[20]=z[1]*z[20];
    z[20]=static_cast<T>(5)+ z[20];
    z[20]=z[20]*z[28];
    z[20]=z[20] + static_cast<T>(5)- z[30];
    z[20]=z[20]*z[28];
    z[21]=z[7] - 1;
    z[22]= - z[14]*z[21];
    z[20]=z[20] + n<T>(39,16) + z[22];
    z[22]= - static_cast<T>(4)+ z[7];
    z[22]=z[7]*z[22];
    z[22]=static_cast<T>(3)+ z[22];
    z[22]=z[22]*z[27];
    z[23]= - static_cast<T>(2)+ z[7];
    z[22]=10*z[23] + z[22];
    z[22]=z[11]*z[22];
    z[23]=static_cast<T>(43)+ z[7];
    z[22]=n<T>(1,4)*z[23] + z[22];
    z[22]=z[11]*z[22];
    z[22]=z[34] + z[22];
    z[22]=z[11]*z[22];
    z[19]=z[19] + n<T>(1,4)*z[20] + z[22];
    z[19]=z[2]*z[19];
    z[20]= - z[42] + static_cast<T>(1)+ z[57];
    z[20]=z[7]*z[20];
    z[21]=z[7]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[27];
    z[20]=z[21] + z[20] - static_cast<T>(5)- z[62];
    z[20]=z[11]*z[20];
    z[21]=n<T>(1,4) - z[62];
    z[21]=z[7]*z[21];
    z[20]=z[20] + z[21] + n<T>(7,2) + z[62];
    z[20]=z[11]*z[20];
    z[21]= - z[39] - z[32];
    z[21]=z[7]*z[21];
    z[21]=z[21] - static_cast<T>(7)+ z[13];
    z[20]=n<T>(1,4)*z[21] + z[20];
    z[20]=z[11]*z[20];

    r += z[15] + n<T>(1,2)*z[16] + z[17] + z[18] + z[19] + z[20];
 
    return r;
}

template double qg_2lha_r1463(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1463(const std::array<dd_real,30>&);
#endif
