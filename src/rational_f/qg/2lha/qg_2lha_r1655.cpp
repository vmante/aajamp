#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1655(const std::array<T,30>& k) {
  T z[69];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[9];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=k[12];
    z[9]=k[13];
    z[10]=k[11];
    z[11]=npow(z[3],2);
    z[12]= - static_cast<T>(3)+ z[3];
    z[12]=z[12]*z[11];
    z[13]=npow(z[3],3);
    z[14]=z[13]*z[6];
    z[15]=n<T>(1,2)*z[3];
    z[16]= - static_cast<T>(1)+ z[15];
    z[16]=z[16]*z[14];
    z[12]=n<T>(1,2)*z[12] + z[16];
    z[12]=z[6]*z[12];
    z[16]= - static_cast<T>(1)+ n<T>(1,4)*z[3];
    z[16]=z[3]*z[16];
    z[12]=z[16] + z[12];
    z[12]=z[6]*z[12];
    z[16]=n<T>(1,2)*z[8];
    z[17]=z[6]*z[3];
    z[18]= - n<T>(1,2) + z[17];
    z[18]=z[18]*z[16];
    z[19]=z[11] + z[14];
    z[19]=z[6]*z[19];
    z[19]=z[15] + z[19];
    z[20]=n<T>(1,2)*z[1];
    z[19]=z[19]*z[20];
    z[12]=z[19] + z[18] + n<T>(1,4) + z[12];
    z[12]=z[2]*z[12];
    z[18]=z[5] - 1;
    z[19]=z[18]*z[5];
    z[21]=z[19] - 1;
    z[22]=npow(z[5],2);
    z[23]=n<T>(1,2)*z[22];
    z[24]=z[21]*z[23];
    z[25]= - static_cast<T>(3)- z[3];
    z[25]=z[3]*z[25];
    z[25]=n<T>(1,2) + z[25];
    z[25]=z[25]*z[11];
    z[26]=npow(z[3],4);
    z[27]=z[26]*z[6];
    z[24]= - 3*z[27] + z[24] + z[25];
    z[24]=z[6]*z[24];
    z[25]=3*z[3];
    z[28]=z[15] + 1;
    z[28]=z[28]*z[3];
    z[29]=n<T>(7,4) - z[28];
    z[29]=z[29]*z[25];
    z[30]=n<T>(5,2)*z[5];
    z[31]=z[30] + 1;
    z[31]=z[31]*z[5];
    z[32]=static_cast<T>(3)- z[31];
    z[32]=z[5]*z[32];
    z[32]=n<T>(21,4) + z[32];
    z[32]=z[5]*z[32];
    z[29]=z[32] + z[29];
    z[24]=n<T>(1,2)*z[29] + z[24];
    z[24]=z[6]*z[24];
    z[29]=11*z[5];
    z[32]= - static_cast<T>(1)- z[29];
    z[32]=z[5]*z[32];
    z[32]=n<T>(3,2) + z[32];
    z[32]=z[32]*z[23];
    z[32]=z[32] - z[28];
    z[12]=z[12] + n<T>(1,2)*z[32] + z[24];
    z[24]= - static_cast<T>(3)+ n<T>(7,2)*z[5];
    z[24]=z[24]*z[5];
    z[32]=n<T>(1,2) - z[24];
    z[32]=z[32]*z[22];
    z[33]=npow(z[5],3);
    z[34]=z[18]*z[33];
    z[35]=n<T>(1,2)*z[6];
    z[36]= - z[35]*z[34];
    z[32]=z[32] + z[36];
    z[32]=z[6]*z[32];
    z[32]= - n<T>(51,2)*z[34] + 5*z[32];
    z[32]=z[6]*z[32];
    z[36]=5*z[5];
    z[37]= - n<T>(13,2) + z[36];
    z[37]=z[5]*z[37];
    z[37]=n<T>(3,2) + z[37];
    z[37]=z[37]*z[22];
    z[38]=z[6]*z[34];
    z[37]=z[37] + n<T>(7,4)*z[38];
    z[37]=z[6]*z[37];
    z[38]=n<T>(9,2)*z[5];
    z[39]= - static_cast<T>(7)+ z[38];
    z[39]=z[5]*z[39];
    z[39]=n<T>(9,4) + z[39];
    z[39]=z[5]*z[39];
    z[39]=n<T>(1,4) + z[39];
    z[39]=z[5]*z[39];
    z[37]=z[39] + z[37];
    z[37]=z[6]*z[37];
    z[39]=z[5] - n<T>(3,2);
    z[40]=z[39]*z[22];
    z[40]=n<T>(1,2) + z[40];
    z[40]=z[5]*z[40];
    z[37]=z[40] + z[37];
    z[37]=z[37]*z[16];
    z[40]=n<T>(17,8)*z[5];
    z[41]=static_cast<T>(2)- z[40];
    z[41]=z[5]*z[41];
    z[41]=n<T>(3,8) + z[41];
    z[41]=z[5]*z[41];
    z[41]= - n<T>(1,4) + z[41];
    z[41]=z[5]*z[41];
    z[32]=z[37] + z[41] + n<T>(1,4)*z[32];
    z[32]=z[8]*z[32];
    z[37]= - n<T>(1,8) + z[5];
    z[37]=z[5]*z[37];
    z[37]= - n<T>(3,4) + z[37];
    z[37]=z[37]*z[22];
    z[41]=n<T>(1,4)*z[34];
    z[42]= - z[6]*z[41];
    z[37]=z[37] + z[42];
    z[37]=z[6]*z[37];
    z[42]=3*z[5];
    z[43]=13*z[5];
    z[44]= - static_cast<T>(5)+ z[43];
    z[44]=z[44]*z[42];
    z[44]= - n<T>(25,2) + z[44];
    z[44]=z[5]*z[44];
    z[44]= - n<T>(19,2) + z[44];
    z[44]=z[5]*z[44];
    z[44]=z[44] - 7*z[3];
    z[37]=n<T>(1,8)*z[44] + z[37];
    z[37]=z[6]*z[37];
    z[44]= - n<T>(7,2) + z[29];
    z[44]=z[5]*z[44];
    z[44]= - n<T>(9,4) + z[44];
    z[44]=z[44]*z[22];
    z[44]=n<T>(3,4) + z[44];
    z[32]=z[32] + n<T>(1,4)*z[44] + z[37];
    z[32]=z[8]*z[32];
    z[37]=n<T>(1,2)*z[5];
    z[44]=z[18]*z[37];
    z[44]=z[44] - 1;
    z[44]=z[44]*z[5];
    z[44]=z[44] - 1;
    z[45]=z[5]*z[44];
    z[46]= - static_cast<T>(1)+ n<T>(3,4)*z[11];
    z[46]=z[3]*z[46];
    z[45]=z[27] + z[45] + z[46];
    z[45]=z[6]*z[45];
    z[46]=z[5] + n<T>(5,2);
    z[46]=z[46]*z[5];
    z[46]=z[46] + n<T>(3,4);
    z[46]=z[46]*z[5];
    z[46]=z[46] - 1;
    z[47]= - z[5]*z[46];
    z[48]=static_cast<T>(1)+ n<T>(3,2)*z[3];
    z[48]=z[3]*z[48];
    z[47]=z[47] + z[48];
    z[48]=z[22] - 1;
    z[48]=z[48]*z[37];
    z[48]=z[48] - 1;
    z[49]=z[5]*z[48];
    z[28]=z[49] - z[28];
    z[28]=z[1]*z[28];
    z[28]=z[28] + n<T>(1,2)*z[47] + z[45];
    z[20]=z[28]*z[20];
    z[12]=z[20] + z[32] + n<T>(1,2)*z[12];
    z[12]=z[2]*z[12];
    z[20]=z[37] - 1;
    z[20]=z[20]*z[5];
    z[28]=z[20] + n<T>(1,2);
    z[32]=z[22]*z[3];
    z[45]=z[28]*z[32];
    z[47]=n<T>(1,4)*z[5];
    z[49]=7*z[5];
    z[50]= - static_cast<T>(11)+ z[49];
    z[50]=z[50]*z[47];
    z[50]=static_cast<T>(1)+ z[50];
    z[50]=z[50]*z[22];
    z[50]=z[50] - z[45];
    z[50]=z[3]*z[50];
    z[38]=static_cast<T>(5)- z[38];
    z[38]=z[5]*z[38];
    z[38]= - static_cast<T>(1)+ z[38];
    z[38]=z[38]*z[23];
    z[38]=z[38] + z[50];
    z[38]=z[3]*z[38];
    z[50]=n<T>(3,2)*z[5];
    z[51]=z[50] - 1;
    z[51]=z[51]*z[33];
    z[38]=z[51] + z[38];
    z[38]=z[3]*z[38];
    z[52]=z[39]*z[5];
    z[52]=z[52] + n<T>(1,2);
    z[53]=z[52]*z[22];
    z[28]= - z[8]*z[28]*z[23];
    z[28]=z[53] + z[28];
    z[28]=z[8]*z[28];
    z[24]= - n<T>(1,2) - z[24];
    z[24]=z[24]*z[23];
    z[24]=z[24] + z[28];
    z[24]=z[8]*z[24];
    z[24]=z[38] + z[24];
    z[28]=z[5] - 2;
    z[28]=z[28]*z[5];
    z[28]=z[28] + 1;
    z[28]=z[28]*z[32];
    z[38]=z[5] - 5;
    z[53]= - z[38]*z[47];
    z[53]= - static_cast<T>(1)+ z[53];
    z[53]=z[53]*z[23];
    z[53]=z[53] + z[28];
    z[53]=z[3]*z[53];
    z[54]=n<T>(1,2)*z[33];
    z[55]=static_cast<T>(1)- z[47];
    z[55]=z[55]*z[54];
    z[53]=z[55] + z[53];
    z[53]=z[3]*z[53];
    z[55]=z[5] - 3;
    z[56]=z[55]*z[5];
    z[57]=z[56] - 3;
    z[57]=z[57]*z[5];
    z[57]=z[57] - 3;
    z[58]=n<T>(1,8)*z[5];
    z[59]= - z[57]*z[58];
    z[53]=z[59] + z[53];
    z[53]=z[3]*z[53];
    z[59]=n<T>(1,2)*z[34];
    z[60]= - z[59] - z[45];
    z[60]=z[3]*z[60];
    z[61]=npow(z[5],4);
    z[62]=n<T>(1,2)*z[61];
    z[60]= - z[62] + z[60];
    z[60]=z[3]*z[60];
    z[63]=z[5] + 1;
    z[64]=z[63]*z[5];
    z[65]=z[64] + 1;
    z[65]=z[65]*z[5];
    z[65]=z[65] + 1;
    z[66]= - z[65]*z[37];
    z[60]=z[66] + z[60];
    z[60]=z[1]*z[60]*z[15];
    z[53]=z[53] + z[60];
    z[53]=z[1]*z[53];
    z[60]= - static_cast<T>(9)+ z[36];
    z[60]=z[60]*z[47];
    z[60]=static_cast<T>(1)+ z[60];
    z[60]=z[60]*z[23];
    z[45]=z[60] - z[45];
    z[45]=z[45]*z[25];
    z[60]=n<T>(1,4)*z[22];
    z[66]= - static_cast<T>(1)- z[20];
    z[66]=z[66]*z[60];
    z[45]=z[66] + z[45];
    z[45]=z[3]*z[45];
    z[19]=z[19] + 1;
    z[19]=z[19]*z[5];
    z[19]=z[19] + 1;
    z[66]= - z[19]*z[58];
    z[45]=z[66] + z[45];
    z[45]=z[3]*z[45];
    z[45]=z[45] + z[53];
    z[45]=z[1]*z[45];
    z[53]=19*z[5];
    z[66]=static_cast<T>(31)- z[53];
    z[66]=z[66]*z[47];
    z[66]= - static_cast<T>(3)+ z[66];
    z[66]=z[66]*z[23];
    z[28]=z[66] + z[28];
    z[28]=z[3]*z[28];
    z[66]= - static_cast<T>(2)+ n<T>(13,8)*z[5];
    z[66]=z[5]*z[66];
    z[66]=n<T>(1,2) + z[66];
    z[66]=z[66]*z[22];
    z[28]=z[66] + z[28];
    z[28]=z[3]*z[28];
    z[28]= - z[41] + z[28];
    z[28]=z[3]*z[28];
    z[28]=z[28] + z[45];
    z[28]=z[1]*z[28];
    z[45]= - n<T>(5,2) + z[5];
    z[45]=z[45]*z[37];
    z[45]=static_cast<T>(1)+ z[45];
    z[45]=z[5]*z[45];
    z[45]= - n<T>(1,4) + z[45];
    z[45]=z[5]*z[45];
    z[56]= - static_cast<T>(3)- z[56];
    z[56]=z[5]*z[56];
    z[56]=static_cast<T>(1)+ z[56];
    z[56]=z[8]*z[56]*z[58];
    z[45]=z[45] + z[56];
    z[45]=z[8]*z[45];
    z[56]=static_cast<T>(13)- z[49];
    z[56]=z[5]*z[56];
    z[56]= - static_cast<T>(7)+ z[56];
    z[56]=z[5]*z[56];
    z[56]=static_cast<T>(1)+ z[56];
    z[56]=z[56]*z[58];
    z[45]=z[56] + z[45];
    z[45]=z[8]*z[45];
    z[56]=z[5]*z[57];
    z[25]=z[56] - z[25];
    z[56]=z[5]*z[65];
    z[56]=z[56] + z[3];
    z[56]=z[1]*z[56];
    z[25]=n<T>(1,2)*z[25] + z[56];
    z[25]=z[1]*z[25];
    z[19]=z[5]*z[19];
    z[19]=z[19] + z[3];
    z[19]=n<T>(1,2)*z[19] + z[25];
    z[19]=z[1]*z[19];
    z[19]=z[34] + z[19];
    z[19]=z[1]*z[19];
    z[25]=z[52]*z[23];
    z[19]=n<T>(1,4)*z[19] + z[25] + z[45];
    z[19]=z[2]*z[19];
    z[19]=z[19] + n<T>(1,2)*z[24] + z[28];
    z[19]=z[4]*z[19];
    z[24]=n<T>(1,4) + z[31];
    z[24]=z[24]*z[22];
    z[25]=z[37]*z[3];
    z[28]=static_cast<T>(7)+ z[36];
    z[28]=z[28]*z[25];
    z[29]=static_cast<T>(9)+ z[29];
    z[29]=z[29]*z[33];
    z[31]=z[61]*z[6];
    z[29]=z[29] - z[31];
    z[29]=z[29]*z[35];
    z[24]=z[29] + 9*z[24] + z[28];
    z[24]=z[6]*z[24];
    z[28]= - n<T>(3,2) + z[43];
    z[28]=z[5]*z[28];
    z[28]= - n<T>(1,4) + z[28];
    z[28]=z[28]*z[22];
    z[24]=z[28] + z[24];
    z[28]= - n<T>(1,8) - 4*z[5];
    z[28]=z[28]*z[33];
    z[29]=static_cast<T>(3)- z[36];
    z[29]=z[29]*z[33];
    z[29]=z[29] - z[31];
    z[29]=z[6]*z[29];
    z[28]=z[28] + n<T>(1,8)*z[29];
    z[28]=z[6]*z[28];
    z[29]=n<T>(1,8)*z[33];
    z[43]=static_cast<T>(1)- 49*z[5];
    z[43]=z[43]*z[29];
    z[28]=z[43] + z[28];
    z[28]=z[6]*z[28];
    z[43]=z[42] - 1;
    z[45]=z[43]*z[33];
    z[52]=z[45] + z[31];
    z[52]=z[52]*z[35];
    z[56]= - static_cast<T>(1)+ z[36];
    z[56]=z[56]*z[33];
    z[56]=z[56] + z[52];
    z[56]=z[6]*z[56];
    z[45]=n<T>(7,2)*z[45] + z[56];
    z[45]=z[6]*z[45];
    z[56]= - static_cast<T>(5)+ 9*z[5];
    z[56]=z[5]*z[56];
    z[56]= - n<T>(1,2) + z[56];
    z[56]=z[56]*z[22];
    z[45]=z[56] + z[45];
    z[45]=z[45]*z[35];
    z[56]=z[5] - n<T>(1,2);
    z[57]=z[5]*z[56];
    z[57]= - n<T>(1,2) + z[57];
    z[57]=z[57]*z[22];
    z[45]=z[57] + z[45];
    z[45]=z[45]*z[16];
    z[57]=17*z[5];
    z[58]= - static_cast<T>(1)- z[57];
    z[58]=z[58]*z[37];
    z[58]=static_cast<T>(1)+ z[58];
    z[58]=z[58]*z[60];
    z[28]=z[45] + z[58] + z[28];
    z[28]=z[8]*z[28];
    z[24]=n<T>(1,4)*z[24] + z[28];
    z[24]=z[8]*z[24];
    z[28]=n<T>(3,4)*z[5];
    z[45]=z[28] - 1;
    z[58]=z[45]*z[5];
    z[60]=z[58] + n<T>(1,4);
    z[32]=z[60]*z[32];
    z[66]=3*z[32];
    z[18]=z[36]*z[18];
    z[18]=z[18] + 1;
    z[67]=z[18]*z[23];
    z[68]=z[67] - z[66];
    z[68]=z[3]*z[68];
    z[21]= - z[5]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[47];
    z[21]=z[21] + z[68];
    z[21]=z[3]*z[21];
    z[48]= - z[48]*z[37];
    z[21]=z[48] + z[21];
    z[21]=z[3]*z[21];
    z[34]=z[34]*z[3];
    z[43]=z[43]*z[54];
    z[43]=z[43] - z[34];
    z[43]=z[43]*z[13]*z[35];
    z[26]=z[1]*z[60]*z[22]*z[26];
    z[21]=z[26] + z[21] + z[43];
    z[21]=z[1]*z[21];
    z[26]= - n<T>(1,4) + 2*z[5];
    z[26]=z[5]*z[26];
    z[26]=n<T>(1,8) + z[26];
    z[26]=z[26]*z[22];
    z[30]=static_cast<T>(1)- z[30];
    z[30]=z[5]*z[30];
    z[30]= - n<T>(3,8) + z[30];
    z[30]=z[5]*z[30];
    z[30]= - n<T>(3,8) + z[30];
    z[30]=z[5]*z[30];
    z[30]=z[30] + z[34];
    z[30]=z[3]*z[30];
    z[26]=z[26] + z[30];
    z[26]=z[3]*z[26];
    z[30]= - z[44]*z[37];
    z[26]=z[30] + z[26];
    z[26]=z[3]*z[26];
    z[30]=z[37] + 1;
    z[43]=z[30]*z[5];
    z[44]=n<T>(1,2) + z[43];
    z[44]=z[44]*z[22];
    z[48]=z[3]*z[5];
    z[60]= - z[65]*z[48];
    z[44]=z[44] + z[60];
    z[44]=z[3]*z[44];
    z[44]=z[62] + z[44];
    z[44]=z[44]*z[11]*z[35];
    z[26]=z[26] + z[44];
    z[26]=z[6]*z[26];
    z[18]= - z[18]*z[22];
    z[18]=z[18] + z[66];
    z[18]=z[3]*z[18];
    z[44]= - static_cast<T>(5)+ n<T>(27,4)*z[5];
    z[44]=z[5]*z[44];
    z[44]= - n<T>(1,4) + z[44];
    z[44]=z[5]*z[44];
    z[44]= - n<T>(3,4) + z[44];
    z[44]=z[44]*z[37];
    z[18]=z[44] + z[18];
    z[18]=z[3]*z[18];
    z[44]=z[46]*z[47];
    z[18]=z[44] + z[18];
    z[18]=z[3]*z[18];
    z[18]=z[21] + z[18] + z[26];
    z[18]=z[1]*z[18];
    z[21]= - static_cast<T>(5)+ z[50];
    z[21]=z[5]*z[21];
    z[21]= - n<T>(11,2) + z[21];
    z[21]=z[21]*z[22];
    z[26]= - z[5]*z[38];
    z[26]=static_cast<T>(11)+ z[26];
    z[26]=z[5]*z[26];
    z[26]=static_cast<T>(17)+ z[26];
    z[26]=z[26]*z[25];
    z[21]=z[21] + z[26];
    z[21]=z[3]*z[21];
    z[26]=z[42] - 5;
    z[26]=z[26]*z[54];
    z[21]= - z[26] + z[21];
    z[28]=z[28] + 1;
    z[28]=z[28]*z[5];
    z[38]= - n<T>(3,4) - z[28];
    z[38]=z[38]*z[22];
    z[42]=n<T>(3,2) + z[43];
    z[42]=z[42]*z[37];
    z[42]=static_cast<T>(1)+ z[42];
    z[42]=z[42]*z[48];
    z[38]=z[38] + z[42];
    z[38]=z[3]*z[38];
    z[42]=z[50] + 1;
    z[42]=z[42]*z[54];
    z[38]=z[42] + z[38];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(1,4)*z[61] + z[38];
    z[38]=z[6]*z[38];
    z[21]=n<T>(1,4)*z[21] + z[38];
    z[17]=z[21]*z[17];
    z[21]=z[58] + n<T>(5,4);
    z[21]=z[21]*z[22];
    z[20]=n<T>(5,2) + z[20];
    z[20]=z[20]*z[37];
    z[20]=static_cast<T>(3)+ z[20];
    z[20]=z[20]*z[48];
    z[20]= - z[21] + z[20];
    z[20]=z[20]*z[15];
    z[17]=z[20] + z[17];
    z[17]=z[6]*z[17];
    z[20]=z[22] + n<T>(5,2);
    z[20]=z[20]*z[5];
    z[20]=z[20] + n<T>(5,2);
    z[38]=z[20]*z[48];
    z[17]= - n<T>(1,4)*z[38] + z[17];
    z[17]=z[6]*z[17];
    z[38]= - z[45]*z[33];
    z[43]= - z[61]*z[35];
    z[38]=z[38] + z[43];
    z[38]=z[6]*z[38];
    z[38]=n<T>(3,2)*z[33] + z[38];
    z[38]=z[6]*z[38];
    z[43]=z[30]*z[54];
    z[38]=z[43] + z[38];
    z[43]=npow(z[6],2);
    z[38]=z[38]*z[43];
    z[44]=z[51] + z[52];
    z[44]=z[6]*z[44];
    z[44]=z[59] + z[44];
    z[44]=z[44]*npow(z[6],3)*z[16];
    z[38]=z[38] + z[44];
    z[38]=z[8]*z[38];
    z[26]=z[26] + z[31];
    z[26]=z[26]*z[35];
    z[21]=z[21] + z[26];
    z[21]=z[6]*z[21];
    z[20]=z[20]*z[37];
    z[20]=z[20] + z[21];
    z[20]=z[6]*z[20];
    z[20]=z[20] + z[38];
    z[16]=z[20]*z[16];
    z[14]=3*z[11] + z[14];
    z[14]=z[6]*z[14];
    z[14]=n<T>(7,2)*z[3] + z[14];
    z[20]=z[3] - 1;
    z[14]=z[2]*z[43]*z[20]*z[14];
    z[13]= - n<T>(17,8)*z[13] - z[27];
    z[13]=z[6]*z[13];
    z[13]= - n<T>(3,2)*z[11] + z[13];
    z[13]=z[6]*z[13];
    z[20]=z[8] - z[3];
    z[13]=z[13] - n<T>(5,8)*z[20];
    z[13]=z[6]*z[13];
    z[13]=n<T>(1,4)*z[14] + z[13];
    z[13]=z[2]*z[13];
    z[13]=z[13] + z[17] + z[16];
    z[13]=z[9]*z[13];
    z[14]=z[55]*z[47];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[5]*z[14];
    z[14]= - n<T>(1,4) + z[14];
    z[14]=z[5]*z[14];
    z[16]=z[65]*z[25];
    z[17]=n<T>(3,4) - z[22];
    z[17]=z[5]*z[17];
    z[17]=n<T>(3,2) + z[17];
    z[17]=z[5]*z[17];
    z[16]=z[17] + z[16];
    z[16]=z[3]*z[16];
    z[14]=z[14] + z[16];
    z[14]=z[3]*z[14];
    z[16]=z[5] + n<T>(1,2);
    z[17]=z[5]*z[16];
    z[17]=n<T>(1,2) + z[17];
    z[17]=z[17]*z[23];
    z[14]=z[17] + z[14];
    z[14]=z[3]*z[14];
    z[17]= - z[63]*z[50];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[22];
    z[20]=n<T>(5,4) + z[28];
    z[20]=z[5]*z[20];
    z[20]=n<T>(3,2) + z[20];
    z[20]=z[20]*z[48];
    z[17]=z[17] + z[20];
    z[17]=z[3]*z[17];
    z[17]=z[42] + z[17];
    z[11]=z[6]*z[17]*z[11];
    z[11]=z[14] + z[11];
    z[11]=z[6]*z[11];
    z[14]= - static_cast<T>(3)+ z[49];
    z[14]=z[5]*z[14];
    z[14]=n<T>(3,2) + z[14];
    z[14]=z[5]*z[14];
    z[14]=n<T>(3,2) + z[14];
    z[14]=z[14]*z[37];
    z[14]=z[14] - z[34];
    z[14]=z[3]*z[14];
    z[17]=static_cast<T>(1)- z[53];
    z[17]=z[17]*z[37];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[5]*z[17];
    z[17]=static_cast<T>(3)+ z[17];
    z[17]=z[17]*z[37];
    z[14]=z[17] + z[14];
    z[14]=z[14]*z[15];
    z[15]= - n<T>(5,4) + z[5];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(19,16) + z[15];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(21,16) + z[15];
    z[15]=z[5]*z[15];
    z[14]=z[15] + z[14];
    z[14]=z[3]*z[14];
    z[11]=z[14] + z[11];
    z[11]=z[6]*z[11];
    z[14]= - static_cast<T>(5)- n<T>(57,2)*z[5];
    z[14]=z[14]*z[33];
    z[15]= - n<T>(13,2) - z[53];
    z[15]=z[15]*z[33];
    z[15]=z[15] - n<T>(5,2)*z[31];
    z[15]=z[6]*z[15];
    z[14]=z[14] + z[15];
    z[14]=z[14]*z[35];
    z[15]=z[56]*z[33];
    z[17]= - z[8]*z[41];
    z[15]=z[15] + z[17];
    z[15]=z[4]*z[15];
    z[17]=n<T>(1,4) - z[36];
    z[17]=z[17]*z[33];
    z[14]=z[15] + z[17] + z[14];
    z[15]=static_cast<T>(2)+ z[40];
    z[15]=z[15]*z[33];
    z[17]=z[30]*z[33];
    z[17]=z[17] - n<T>(1,8)*z[31];
    z[17]=z[6]*z[17];
    z[15]=z[15] + z[17];
    z[15]=z[6]*z[15];
    z[17]=static_cast<T>(9)+ z[57];
    z[17]=z[17]*z[29];
    z[15]=z[17] + z[15];
    z[15]=z[6]*z[15];
    z[16]=z[16]*z[54];
    z[15]=z[16] + z[15];
    z[15]=z[8]*z[15];
    z[14]=z[15] + n<T>(1,2)*z[14];
    z[14]=z[14]*npow(z[8],2);
    z[15]= - n<T>(1,8)*z[4] + n<T>(5,8);
    z[15]=z[61]*z[15];
    z[16]=n<T>(23,8)*z[61] + z[31];
    z[16]=z[6]*z[16];
    z[16]=n<T>(21,8)*z[61] + z[16];
    z[16]=z[6]*z[16];
    z[15]=z[16] + z[15];
    z[15]=z[7]*z[15]*npow(z[8],3);
    z[14]=z[15] + z[14];
    z[14]=z[7]*z[14];
    z[15]=z[67] - z[32];
    z[15]=z[3]*z[15];
    z[16]=static_cast<T>(9)- n<T>(25,2)*z[5];
    z[16]=z[5]*z[16];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[5]*z[16];
    z[16]=n<T>(1,2) + z[16];
    z[16]=z[16]*z[47];
    z[15]=z[16] + z[15];
    z[15]=z[3]*z[15];
    z[16]= - static_cast<T>(1)+ n<T>(9,4)*z[5];
    z[16]=z[5]*z[16];
    z[16]= - n<T>(1,8) + z[16];
    z[16]=z[5]*z[16];
    z[16]=n<T>(1,8) + z[16];
    z[16]=z[16]*z[37];
    z[15]=z[16] + z[15];
    z[15]=z[3]*z[15];
    z[16]=z[6]*z[63];
    z[16]=z[39] + z[16];
    z[16]=z[6]*z[16];
    z[16]= - n<T>(3,2) + z[16];
    z[16]=z[48]*z[16];
    z[17]=z[5] + z[3];
    z[20]= - z[64] - z[3];
    z[20]=z[6]*z[20];
    z[17]=n<T>(3,2)*z[17] + z[20];
    z[17]=z[6]*z[17];
    z[20]=n<T>(3,4)*z[3];
    z[17]= - z[20] + z[17];
    z[17]=z[2]*z[17];
    z[16]=z[17] + z[16];
    z[16]=z[6]*z[16];
    z[17]= - z[6]*z[22];
    z[17]=z[50] + z[17];
    z[17]=z[6]*z[17];
    z[17]= - n<T>(3,4) + z[17];
    z[17]=z[8]*z[17];
    z[16]=z[17] + z[20] + z[16];
    z[16]=z[10]*z[16];

    r += z[11] + z[12] + z[13] + z[14] + z[15] + n<T>(1,4)*z[16] + z[18] + 
      z[19] + z[24];
 
    return r;
}

template double qg_2lha_r1655(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1655(const std::array<dd_real,30>&);
#endif
