#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1929(const std::array<T,30>& k) {
  T z[22];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[12];
    z[7]=z[1] - 1;
    z[8]= - z[4]*z[6];
    z[8]=static_cast<T>(1)+ z[8];
    z[8]=z[4]*z[8];
    z[8]=z[8] + z[7];
    z[9]=n<T>(1,2)*z[4];
    z[8]=z[8]*z[9];
    z[10]=n<T>(1,2)*z[5];
    z[11]=z[4] - z[7];
    z[11]=z[11]*z[10];
    z[11]=z[11] - static_cast<T>(1)+ n<T>(3,2)*z[1];
    z[11]=z[5]*z[11];
    z[12]=n<T>(1,2)*z[3];
    z[13]=z[12]*npow(z[1],3);
    z[14]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[14]=z[1]*z[14];
    z[8]=z[11] + z[8] - z[13] + n<T>(1,2) + z[14];
    z[8]=z[2]*z[8];
    z[11]=z[1] + n<T>(1,2);
    z[14]= - z[11]*z[12];
    z[15]=z[9]*npow(z[6],2);
    z[16]=z[15] - n<T>(5,2)*z[6] - z[3];
    z[16]=z[16]*z[9];
    z[14]=z[16] + static_cast<T>(1)+ z[14];
    z[14]=z[4]*z[14];
    z[16]=z[3] + z[6];
    z[17]=z[16]*z[4];
    z[18]=n<T>(3,2)*z[16];
    z[17]=z[17] + z[18];
    z[19]=z[17]*npow(z[4],2);
    z[20]= - static_cast<T>(1)- z[4];
    z[20]=z[20]*z[10];
    z[19]=z[20] + n<T>(1,2)*z[19] + static_cast<T>(1)- n<T>(1,4)*z[3];
    z[19]=z[5]*z[19];
    z[20]=n<T>(1,2) - 3*z[1];
    z[20]=z[1]*z[20];
    z[13]=z[13] + n<T>(1,2) + z[20];
    z[13]=z[3]*z[13];
    z[7]=n<T>(3,2)*z[7] + z[13];
    z[7]=z[8] + z[19] + n<T>(1,2)*z[7] + z[14];
    z[7]=z[2]*z[7];
    z[8]=z[3] - 3;
    z[13]=z[8]*z[3];
    z[14]=z[6] + 3;
    z[14]=z[14]*z[6];
    z[13]=z[13] - z[14];
    z[9]=z[13]*z[9];
    z[13]=z[3] - 1;
    z[13]=z[13]*z[3];
    z[19]=z[13] - z[6];
    z[9]=z[9] + z[19];
    z[17]=z[17]*z[4];
    z[16]=n<T>(1,2)*z[16];
    z[20]= - z[16] - z[17];
    z[20]=z[5]*z[20];
    z[20]=z[20] - z[9];
    z[20]=z[4]*z[20];
    z[21]=npow(z[3],2);
    z[20]= - n<T>(1,2)*z[21] + z[20];
    z[20]=z[5]*z[20];
    z[8]=z[8]*z[12];
    z[8]=z[15] - z[6] + z[8];
    z[8]=z[4]*z[8];
    z[11]=z[1]*z[11];
    z[11]=n<T>(1,2) + z[11];
    z[11]=z[3]*z[11];
    z[11]= - n<T>(5,2)*z[1] + z[11];
    z[11]=z[3]*z[11];
    z[8]=z[20] + z[8] + n<T>(1,2) + z[11];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[2]*z[7];
    z[8]=z[4]*z[9];
    z[9]=z[18] + z[17];
    z[9]=z[4]*z[9];
    z[9]=z[16] + z[9];
    z[9]=z[5]*z[9];
    z[8]=z[9] + n<T>(1,2)*z[19] + z[8];
    z[8]=z[5]*z[8];
    z[9]=z[6] + 1;
    z[9]=z[9]*z[6];
    z[11]=z[9] - z[13];
    z[11]=z[4]*z[11];
    z[11]= - z[21] + z[11];
    z[8]=n<T>(1,2)*z[11] + z[8];
    z[8]=z[5]*z[8];
    z[11]=static_cast<T>(1)+ z[1];
    z[11]=z[3]*z[11];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[11]*z[12];
    z[8]=z[11] + z[8];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[2]*z[7];
    z[8]=z[3]*z[6];
    z[11]=z[8] - z[6];
    z[13]= - z[11]*z[12];
    z[14]=z[14] - z[8];
    z[12]=z[4]*z[14]*z[12];
    z[11]= - z[3]*z[11];
    z[11]=z[11] + z[12];
    z[11]=z[4]*z[11];
    z[12]=n<T>(3,2)*z[8];
    z[14]= - z[4]*z[8];
    z[14]= - z[12] + z[14];
    z[14]=z[4]*z[14];
    z[12]= - z[12] + z[14];
    z[12]=z[4]*z[12];
    z[12]= - n<T>(1,2)*z[8] + z[12];
    z[12]=z[5]*z[12];
    z[11]=z[12] + z[13] + z[11];
    z[11]=z[5]*z[11];
    z[8]= - z[9] + z[8];
    z[8]=z[8]*z[4]*z[3];
    z[9]=z[6]*z[21];
    z[8]=z[9] + z[8];
    z[8]=n<T>(1,2)*z[8] + z[11];
    z[8]=z[8]*z[10];
    z[7]=z[8] + z[7];

    r += n<T>(1,4)*z[7];
 
    return r;
}

template double qg_2lha_r1929(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1929(const std::array<dd_real,30>&);
#endif
