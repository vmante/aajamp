#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2125(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[3];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[12];
    z[6]=k[11];
    z[7]=k[4];
    z[8]=k[7];
    z[9]=n<T>(1,2)*z[4];
    z[10]= - static_cast<T>(1)- n<T>(1,2)*z[1];
    z[10]=z[10]*z[9];
    z[11]=z[1] - 1;
    z[12]=n<T>(3,2)*z[4];
    z[13]= - z[12] + 3*z[6];
    z[13]=z[11]*z[13];
    z[14]=z[8] - 1;
    z[13]= - z[14] + z[13];
    z[13]=z[2]*z[13];
    z[10]=n<T>(1,2)*z[13] + z[10] + n<T>(3,2)*z[6] - n<T>(1,2) + z[8];
    z[10]=z[2]*z[10];
    z[12]=z[11]*z[12];
    z[13]= - z[6]*z[11];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[13]=z[2]*z[13];
    z[12]=z[13] + z[12] - z[6] + static_cast<T>(1)- n<T>(3,2)*z[8];
    z[12]=z[2]*z[12];
    z[13]= - z[1] + static_cast<T>(3)- z[7];
    z[9]=z[13]*z[9];
    z[13]=static_cast<T>(1)- n<T>(1,2)*z[7];
    z[13]=z[8]*z[13];
    z[9]=z[12] + z[9] - n<T>(1,2) + z[13];
    z[9]=z[5]*z[9];
    z[9]=z[9] + n<T>(1,4)*z[4] + z[10];
    z[9]=z[5]*z[9];
    z[10]= - n<T>(1,2)*z[6] + n<T>(1,3)*z[3];
    z[11]=z[2]*z[11]*z[10];
    z[10]=z[11] + z[10];
    z[10]=z[2]*z[10];
    z[9]=z[10] + z[9];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r2125(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2125(const std::array<dd_real,30>&);
#endif
