#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1664(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[12];
    z[6]=k[9];
    z[7]=npow(z[3],2);
    z[8]=4*z[3];
    z[9]=static_cast<T>(3)- z[8];
    z[9]=z[9]*z[7];
    z[10]=2*z[3];
    z[11]=z[10] - 5;
    z[11]=z[11]*z[3];
    z[12]=z[11] + 4;
    z[12]=z[12]*z[3];
    z[12]=z[12] - 1;
    z[12]=z[12]*z[2];
    z[9]=z[9] - z[12];
    z[9]=z[2]*z[9];
    z[13]=npow(z[2],2);
    z[14]=z[13]*z[4]*z[3];
    z[15]=static_cast<T>(7)- 6*z[3];
    z[15]=z[3]*z[15];
    z[15]= - static_cast<T>(2)+ z[15];
    z[15]=z[15]*z[14];
    z[14]=z[2] + z[14];
    z[16]=z[10] - 3;
    z[16]=z[16]*z[3];
    z[16]=z[16] + 1;
    z[14]=z[5]*z[16]*z[14];
    z[9]=z[14] + z[9] + z[15];
    z[9]=z[5]*z[9];
    z[10]=z[10] - 1;
    z[10]=z[10]*z[7];
    z[14]= - static_cast<T>(1)+ z[3];
    z[8]=z[14]*z[8];
    z[8]=static_cast<T>(1)+ z[8];
    z[8]=z[8]*z[2]*z[3];
    z[8]= - z[10] + z[8];
    z[8]=z[4]*z[8];
    z[7]= - static_cast<T>(1)+ 2*z[7];
    z[7]=z[7]*z[3];
    z[8]=z[8] + z[7] + z[12];
    z[8]=z[2]*z[8];
    z[12]=z[1] + z[4];
    z[12]=z[12]*z[10];
    z[11]=static_cast<T>(2)+ z[11];
    z[11]=z[3]*z[11];
    z[11]=z[11] + z[12];
    z[11]=z[1]*z[13]*z[11];
    z[10]=z[4]*z[10];
    z[7]= - z[7] + z[10];
    z[7]=z[6]*z[7];

    r += z[7] + z[8] + z[9] + z[11];
 
    return r;
}

template double qg_2lha_r1664(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1664(const std::array<dd_real,30>&);
#endif
