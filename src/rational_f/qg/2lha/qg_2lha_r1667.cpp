#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1667(const std::array<T,30>& k) {
  T z[10];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[9];
    z[6]=z[1]*npow(z[2],2);
    z[7]=2*z[2];
    z[8]=static_cast<T>(3)+ z[7];
    z[8]=z[2]*z[8];
    z[8]=z[8] - 2*z[6];
    z[8]=z[1]*z[8];
    z[9]= - static_cast<T>(1)+ z[4];
    z[9]=z[2]*z[9];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[2]*z[9];
    z[6]=z[9] + z[6];
    z[6]=z[1]*z[6];
    z[7]= - z[4]*z[7];
    z[6]=z[7] + z[6];
    z[6]=z[1]*z[6];
    z[7]= - z[5]*npow(z[4],2);
    z[6]=z[7] + z[6];
    z[6]=z[3]*z[6];
    z[7]=z[4]*z[5];
    z[6]=2*z[6] + 3*z[7] + z[8];
    z[6]=z[3]*z[6];
    z[6]= - z[5] + z[6];

    r += 16*z[6]*z[3];
 
    return r;
}

template double qg_2lha_r1667(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1667(const std::array<dd_real,30>&);
#endif
