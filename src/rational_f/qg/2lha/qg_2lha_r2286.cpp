#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2286(const std::array<T,30>& k) {
  T z[22];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[2];
    z[6]=k[11];
    z[7]=k[3];
    z[8]=k[9];
    z[9]=npow(z[4],2)*z[2]*z[3];
    z[10]=z[3] - 1;
    z[11]= - z[10]*z[9];
    z[12]=z[5] + 2;
    z[13]=z[7]*z[8];
    z[14]=z[13] + 1;
    z[15]= - z[8] + z[14];
    z[15]=z[7]*z[15];
    z[16]=2*z[3];
    z[17]=static_cast<T>(5)- z[16];
    z[17]=z[3]*z[17];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[4]*z[17];
    z[11]=z[11] + z[17] + z[15] + z[12];
    z[11]=z[2]*z[11];
    z[15]= - z[5]*z[12];
    z[17]=static_cast<T>(4)- z[3];
    z[17]=z[3]*z[17];
    z[11]=z[11] + z[17] - static_cast<T>(3)+ z[15];
    z[11]=z[6]*z[11];
    z[11]= - z[8] + z[13] + z[11];
    z[15]=5*z[3];
    z[17]= - static_cast<T>(9)+ z[15];
    z[17]=z[17]*z[16];
    z[10]=z[4]*z[10];
    z[10]= - 4*z[10] + static_cast<T>(5)+ z[17];
    z[10]=z[4]*z[10];
    z[17]= - static_cast<T>(6)+ 7*z[3];
    z[17]=z[17]*z[9];
    z[18]= - z[7]*z[14];
    z[10]=z[17] + z[10] + z[18] - static_cast<T>(3)- z[5];
    z[10]=z[2]*z[10];
    z[17]=z[7] - 1;
    z[18]=2*z[4];
    z[17]=z[17]*z[18];
    z[19]=static_cast<T>(4)+ z[5];
    z[19]=z[5]*z[19];
    z[20]=3*z[3];
    z[21]= - static_cast<T>(10)+ z[20];
    z[21]=z[3]*z[21];
    z[10]=z[10] - z[17] + z[21] + static_cast<T>(7)+ z[19] + 2*z[11];
    z[10]=z[6]*z[10];
    z[11]= - static_cast<T>(3)+ z[15];
    z[11]=z[11]*z[18];
    z[15]=static_cast<T>(9)- 8*z[3];
    z[15]=z[3]*z[15];
    z[11]=z[11] - static_cast<T>(1)+ z[15];
    z[11]=z[4]*z[11];
    z[15]=static_cast<T>(2)- z[20];
    z[15]=z[15]*z[9];
    z[11]=3*z[15] + z[11] + z[14];
    z[11]=z[2]*z[11];
    z[14]=z[7] + z[17];
    z[14]=z[4]*z[14];
    z[15]=static_cast<T>(3)- z[3];
    z[15]=z[3]*z[15];
    z[10]=z[10] + z[11] + z[14] + z[15] - z[13] - z[12];
    z[10]=z[6]*z[10];
    z[11]= - static_cast<T>(3)+ z[1];
    z[11]=z[11]*z[16];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[4]*z[11];
    z[12]= - static_cast<T>(1)+ z[16];
    z[12]=z[3]*z[12];
    z[11]=z[12] + z[11];
    z[11]=z[4]*z[11];
    z[12]=static_cast<T>(4)- z[1];
    z[12]=z[3]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[9]=z[12]*z[9];
    z[9]=z[11] + z[9];
    z[9]=z[2]*z[9];
    z[11]= - z[7] + static_cast<T>(2)- z[1];
    z[11]=z[4]*z[11];
    z[11]= - z[7] + z[11];
    z[11]=z[4]*z[11];

    r +=  - static_cast<T>(1)+ z[8] + z[9] + z[10] + z[11];
 
    return r;
}

template double qg_2lha_r2286(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2286(const std::array<dd_real,30>&);
#endif
