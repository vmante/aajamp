#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1628(const std::array<T,30>& k) {
  T z[51];
  T r = 0;

    z[1]=k[2];
    z[2]=k[3];
    z[3]=k[5];
    z[4]=k[14];
    z[5]=k[9];
    z[6]=k[7];
    z[7]=k[17];
    z[8]=k[4];
    z[9]=k[11];
    z[10]=k[12];
    z[11]=n<T>(9,8)*z[10];
    z[12]=z[11] + 1;
    z[12]=z[12]*z[10];
    z[13]=static_cast<T>(1)+ n<T>(9,8)*z[9];
    z[13]=z[13]*z[9];
    z[12]=z[12] + z[13];
    z[14]=n<T>(9,2)*z[9];
    z[15]=static_cast<T>(31)+ z[14];
    z[15]=z[9]*z[15];
    z[16]=static_cast<T>(1)+ n<T>(63,2)*z[10];
    z[16]=z[10]*z[16];
    z[15]=z[15] + z[16];
    z[16]=n<T>(1,4)*z[3];
    z[15]=z[15]*z[16];
    z[17]=n<T>(9,4)*z[9];
    z[18]=z[17] + 2;
    z[18]=z[18]*z[9];
    z[19]=n<T>(9,4)*z[10];
    z[20]=static_cast<T>(2)+ z[19];
    z[20]=z[10]*z[20];
    z[20]=z[18] + z[20];
    z[20]=z[3]*z[20];
    z[21]=npow(z[9],2);
    z[22]=z[21]*z[2];
    z[20]= - n<T>(9,2)*z[22] + z[20];
    z[20]=z[8]*z[20];
    z[23]=z[6] - 1;
    z[24]=z[4] - n<T>(1,2);
    z[24]=z[24]*z[4];
    z[25]=9*z[22];
    z[26]= - 5*z[9] + z[25];
    z[26]=z[2]*z[26];
    z[15]=z[20] + z[15] + n<T>(3,4)*z[26] - z[24] - n<T>(3,2)*z[23] + z[12];
    z[15]=z[8]*z[15];
    z[20]=n<T>(1,4)*z[10];
    z[26]=9*z[10];
    z[27]=z[26] - 1;
    z[28]=z[27]*z[20];
    z[29]= - n<T>(31,4) + z[26];
    z[29]=z[10]*z[29];
    z[29]=z[29] + static_cast<T>(5)+ z[17];
    z[29]=z[3]*z[29];
    z[24]=z[24] + z[6];
    z[24]=z[24]*z[1];
    z[30]=z[4] - 1;
    z[31]=z[30]*z[4];
    z[32]=z[2]*z[14];
    z[15]=z[15] + z[29] + z[28] + z[32] + z[24] - n<T>(3,2)*z[31] + z[17] - 
   n<T>(3,4) - z[6];
    z[15]=z[8]*z[15];
    z[12]=z[12]*z[3];
    z[17]=z[13] + n<T>(9,8)*z[22];
    z[17]=z[2]*z[17];
    z[28]=z[2] + 1;
    z[29]=z[28]*z[2];
    z[29]=z[29] + 1;
    z[32]=z[29]*z[11];
    z[32]=z[32] + z[28];
    z[32]=z[10]*z[32];
    z[13]=z[12] + z[32] + z[13] + z[17];
    z[13]=z[8]*z[13];
    z[17]=z[29]*z[26];
    z[32]=z[17] - z[28];
    z[32]=z[10]*z[32];
    z[33]=z[27]*z[10];
    z[34]=9*z[9];
    z[35]=z[34] + z[33];
    z[35]=z[3]*z[35];
    z[36]=z[9]*z[28];
    z[32]=z[35] + 9*z[36] + z[32];
    z[13]=n<T>(1,2)*z[32] + z[13];
    z[13]=z[8]*z[13];
    z[32]=n<T>(9,2)*z[10];
    z[35]=z[29]*z[32];
    z[35]= - 5*z[28] + z[35];
    z[36]=n<T>(3,2)*z[10];
    z[35]=z[35]*z[36];
    z[37]=n<T>(3,2)*z[3];
    z[38]=z[32] - 5;
    z[39]=z[38]*z[10];
    z[40]=n<T>(5,2) + z[39];
    z[40]=z[40]*z[37];
    z[41]=n<T>(1,4)*z[4];
    z[42]=z[41] + z[6];
    z[13]=z[13] + z[40] + z[35] + n<T>(15,2) - z[42];
    z[13]=z[8]*z[13];
    z[35]=2*z[6];
    z[40]=n<T>(1,2)*z[4];
    z[43]=z[35] + z[40];
    z[44]=z[1]*z[43];
    z[17]= - 19*z[28] + z[17];
    z[45]=n<T>(1,2)*z[10];
    z[17]=z[17]*z[45];
    z[46]=z[1] + n<T>(3,2);
    z[47]= - static_cast<T>(19)+ z[26];
    z[47]=z[10]*z[47];
    z[47]=5*z[46] + z[47];
    z[48]=n<T>(1,2)*z[3];
    z[47]=z[47]*z[48];
    z[49]=z[6] - 5;
    z[13]=z[13] + z[47] + z[17] + z[44] - 2*z[49] - z[40];
    z[13]=z[8]*z[13];
    z[17]= - z[1]*z[42];
    z[17]=z[17] + z[43];
    z[17]=z[1]*z[17];
    z[29]=z[29]*z[19];
    z[28]= - 7*z[28] + z[29];
    z[28]=z[28]*z[45];
    z[29]=5*z[1];
    z[43]=static_cast<T>(1)+ z[1];
    z[43]=z[43]*z[29];
    z[43]=n<T>(19,2) + z[43];
    z[44]= - static_cast<T>(7)+ z[19];
    z[44]=z[10]*z[44];
    z[43]=n<T>(1,2)*z[43] + z[44];
    z[43]=z[43]*z[48];
    z[13]=z[13] + z[43] + z[28] + z[17] + n<T>(39,8) - z[42];
    z[13]=z[5]*z[13];
    z[17]=z[2]*npow(z[10],2);
    z[17]=z[22] + z[17];
    z[17]=n<T>(9,8)*z[17] - z[12];
    z[17]=z[8]*z[17];
    z[28]=n<T>(1,2)*z[9];
    z[42]=3*z[9];
    z[43]= - static_cast<T>(1)- z[42];
    z[43]=z[43]*z[28];
    z[43]=z[43] - 3*z[22];
    z[44]=n<T>(3,2)*z[2];
    z[43]=z[43]*z[44];
    z[45]= - static_cast<T>(1)+ z[2];
    z[19]=z[45]*z[19];
    z[19]=z[19] - static_cast<T>(2)- n<T>(13,4)*z[2];
    z[19]=z[10]*z[19];
    z[45]=3*z[10];
    z[47]= - static_cast<T>(1)- z[32];
    z[47]=z[47]*z[45];
    z[50]= - static_cast<T>(13)- z[14];
    z[50]=z[9]*z[50];
    z[47]=z[50] + z[47];
    z[47]=z[47]*z[48];
    z[17]=z[17] + z[47] + z[19] - z[18] + z[43];
    z[17]=z[8]*z[17];
    z[18]=z[4] + 1;
    z[19]=z[18]*z[41];
    z[43]= - static_cast<T>(5)- z[34];
    z[47]=n<T>(11,2) - z[26];
    z[47]=z[10]*z[47];
    z[43]=n<T>(1,2)*z[43] + z[47];
    z[37]=z[43]*z[37];
    z[43]= - z[2]*z[34];
    z[27]= - 7*z[2] - z[27];
    z[27]=z[10]*z[27];
    z[17]=z[17] + z[37] + n<T>(3,4)*z[27] + z[43] - z[19] - n<T>(27,4)*z[9] + n<T>(5,2) - z[6];
    z[17]=z[8]*z[17];
    z[27]=z[1] - 1;
    z[37]=npow(z[4],2);
    z[27]=z[37]*z[27];
    z[43]= - static_cast<T>(3)- z[2];
    z[36]=z[43]*z[36];
    z[43]=n<T>(1,2)*z[2];
    z[36]=z[36] + static_cast<T>(5)- z[43];
    z[36]=z[36]*z[45];
    z[27]=z[36] - n<T>(25,2) + z[27];
    z[36]= - n<T>(7,2) - z[1];
    z[47]=static_cast<T>(17)- n<T>(45,4)*z[10];
    z[47]=z[10]*z[47];
    z[36]=n<T>(5,2)*z[36] + z[47];
    z[36]=z[3]*z[36];
    z[17]=z[17] + n<T>(1,2)*z[27] + z[36];
    z[17]=z[8]*z[17];
    z[27]=z[30]*z[40];
    z[30]=z[30]*z[41];
    z[36]=z[6] - z[30];
    z[36]=z[1]*z[36];
    z[36]=z[36] - z[35] + z[27];
    z[36]=z[1]*z[36];
    z[41]= - static_cast<T>(3)- z[1];
    z[29]=z[41]*z[29];
    z[32]=static_cast<T>(11)- z[32];
    z[32]=z[32]*z[45];
    z[29]=z[32] - n<T>(39,2) + z[29];
    z[29]=z[29]*z[16];
    z[32]= - static_cast<T>(1)- z[43];
    z[26]=z[32]*z[26];
    z[26]=z[26] + static_cast<T>(19)+ 5*z[2];
    z[26]=z[26]*z[20];
    z[13]=z[13] + z[17] + z[29] + z[26] + z[36] - z[30] + z[49];
    z[13]=z[5]*z[13];
    z[17]=z[7] - z[6];
    z[17]=z[1]*z[17];
    z[17]=n<T>(1,2)*z[17] + z[6] + z[27];
    z[17]=z[1]*z[17];
    z[20]=z[38]*z[20];
    z[11]= - static_cast<T>(2)+ z[11];
    z[11]=z[11]*z[45];
    z[26]=n<T>(5,2) + z[1];
    z[26]=z[1]*z[26];
    z[11]=z[11] + n<T>(15,4) + z[26];
    z[11]=z[3]*z[11];
    z[26]=n<T>(3,2) - z[4];
    z[26]=z[4]*z[26];
    z[26]=z[26] + static_cast<T>(3)- z[6];
    z[11]=z[13] + z[15] + z[11] + z[20] + n<T>(1,2)*z[26] + z[17];
    z[11]=z[5]*z[11];
    z[13]=static_cast<T>(1)- 3*z[4];
    z[13]=z[13]*z[40];
    z[12]= - z[12] + n<T>(27,4)*z[22] - z[35] + z[13];
    z[12]=z[8]*z[12];
    z[13]=static_cast<T>(23)+ z[34];
    z[13]=z[13]*z[28];
    z[13]=z[13] - z[25];
    z[13]=z[13]*z[43];
    z[15]= - z[42] - z[1];
    z[15]=3*z[15] - z[33];
    z[15]=z[15]*z[16];
    z[17]=z[6] + n<T>(13,8);
    z[20]=static_cast<T>(1)- n<T>(3,2)*z[4];
    z[20]=z[4]*z[20];
    z[12]=z[12] + z[15] + z[13] + z[24] + z[20] - z[17];
    z[12]=z[8]*z[12];
    z[13]=npow(z[1],2);
    z[15]=z[3]*z[13];
    z[15]=z[15] - z[39] - static_cast<T>(5)- n<T>(9,2)*z[1];
    z[15]=z[3]*z[15];
    z[15]=z[15] - n<T>(3,2) - z[31];
    z[11]=z[11] + n<T>(1,4)*z[15] + z[12];
    z[11]=z[5]*z[11];
    z[12]=z[44] - z[46];
    z[12]=z[12]*z[16];
    z[15]=z[43] - n<T>(1,2) - z[6];
    z[15]=z[1]*z[15];
    z[12]=z[12] + z[15] + z[17];
    z[12]=z[3]*z[12];
    z[15]=z[2]*z[19];
    z[16]= - z[23]*z[48];
    z[16]=z[16] - n<T>(1,4)*z[2] + n<T>(1,2) + z[35];
    z[16]=z[3]*z[16];
    z[17]=z[4] + n<T>(1,2);
    z[19]= - z[4]*z[17];
    z[15]=z[16] + z[19] + z[15];
    z[15]=z[8]*z[15];
    z[16]=z[1]*z[4];
    z[17]=z[17]*z[16];
    z[16]=z[16]*z[18];
    z[18]= - n<T>(27,4)*z[21] - z[16];
    z[18]=z[18]*z[43];
    z[12]=z[15] + z[12] + z[18] - n<T>(1,2)*z[37] + z[17];
    z[12]=z[8]*z[12];
    z[15]= - n<T>(9,2)*z[21] + z[16];
    z[15]=z[1]*z[15];
    z[14]= - z[14] + z[15];
    z[14]=z[2]*z[14];
    z[15]= - z[1]*z[44];
    z[15]= - z[13] + z[15];
    z[15]=z[3]*z[15];
    z[13]=n<T>(9,2) - z[13];
    z[13]=z[2]*z[13];
    z[13]=z[15] + n<T>(3,2) + z[13];
    z[13]=z[3]*z[13];
    z[13]=z[14] + z[13];

    r += z[11] + z[12] + n<T>(1,4)*z[13];
 
    return r;
}

template double qg_2lha_r1628(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1628(const std::array<dd_real,30>&);
#endif
