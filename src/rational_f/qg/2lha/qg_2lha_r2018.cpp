#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2018(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[5];
    z[7]=k[27];
    z[8]=n<T>(3,2)*z[6];
    z[9]=n<T>(1,2)*z[3];
    z[10]=static_cast<T>(1)+ z[9];
    z[10]=z[3]*z[10];
    z[10]=z[10] - z[8];
    z[10]=z[2]*z[10];
    z[11]=npow(z[3],2);
    z[11]=n<T>(1,2)*z[11];
    z[12]= - n<T>(1,2) + z[7];
    z[12]=z[6]*z[12];
    z[12]= - n<T>(1,4)*z[7] + z[12];
    z[12]=z[6]*z[12];
    z[10]=z[10] - z[11] + z[12];
    z[10]=z[2]*z[10];
    z[12]=z[6]*z[7];
    z[13]= - n<T>(1,2) - z[7];
    z[13]=z[13]*z[12];
    z[14]=npow(z[7],2);
    z[13]=z[14] + z[13];
    z[15]=n<T>(1,2)*z[6];
    z[13]=z[13]*z[15];
    z[10]=z[13] + z[10];
    z[10]=z[2]*z[10];
    z[13]=z[6] + 1;
    z[16]= - z[15]*z[14]*z[13];
    z[12]=z[7] + z[12];
    z[12]=z[2]*z[6]*z[12];
    z[12]=z[16] + z[12];
    z[12]=z[12]*npow(z[2],2);
    z[8]= - z[8]*z[13]*npow(z[7],4);
    z[8]=z[8] + z[12];
    z[8]=z[4]*z[8];
    z[12]=z[7]*z[5];
    z[13]=n<T>(1,2)*z[5];
    z[16]= - static_cast<T>(1)+ z[13];
    z[16]=z[16]*z[12];
    z[13]=z[13] + z[16];
    z[16]=3*z[7];
    z[13]=z[13]*z[16];
    z[13]=n<T>(1,2) + z[13];
    z[13]=z[6]*z[13]*z[14];
    z[17]=npow(z[7],3);
    z[13]=n<T>(5,2)*z[17] + z[13];
    z[13]=z[13]*z[15];
    z[8]=n<T>(1,2)*z[8] + z[13] + z[10];
    z[8]=z[4]*z[8];
    z[10]=static_cast<T>(1)+ z[12];
    z[10]=z[10]*z[14];
    z[13]=z[5] - 1;
    z[17]=z[7]*z[13];
    z[17]=static_cast<T>(1)+ z[17];
    z[16]=z[16]*z[17]*npow(z[5],2);
    z[13]=z[16] + z[13];
    z[13]=z[7]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[7]*z[13]*z[15];
    z[10]=z[10] + z[13];
    z[10]=z[10]*z[15];
    z[13]= - n<T>(3,2) + z[1];
    z[13]=z[13]*z[9];
    z[15]= - n<T>(1,2) - z[3];
    z[16]=z[1] - 1;
    z[15]=z[2]*z[16]*z[15];
    z[13]=z[15] + static_cast<T>(1)+ z[13];
    z[13]=z[3]*z[13];
    z[13]= - z[6] + z[13];
    z[13]=z[2]*z[13];
    z[15]= - static_cast<T>(1)+ z[7];
    z[15]=z[6]*z[15];
    z[15]= - z[7] + z[15];
    z[15]=z[6]*z[15];
    z[11]= - z[11] + z[15];
    z[11]=n<T>(1,2)*z[11] + z[13];
    z[11]=z[2]*z[11];
    z[9]= - z[12]*z[9];
    z[9]= - z[3] + z[9];
    z[9]=z[9]*z[14];
    z[8]=z[8] + z[11] + z[9] + z[10];
    z[8]=z[4]*z[8];
    z[9]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[9]=z[9]*z[1];
    z[9]=z[9] + n<T>(1,2);
    z[9]=z[2]*z[9];
    z[9]=n<T>(1,4)*z[16] + z[9];
    z[10]=z[3] - 1;
    z[9]=z[2]*z[10]*z[9];
    z[9]= - n<T>(1,4)*z[10] + z[9];
    z[9]=z[2]*z[3]*z[9];

    r += z[8] + z[9];
 
    return r;
}

template double qg_2lha_r2018(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2018(const std::array<dd_real,30>&);
#endif
