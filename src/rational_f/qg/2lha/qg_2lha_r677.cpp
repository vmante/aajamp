#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r677(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[29];
    z[5]=k[4];
    z[6]=k[28];
    z[7]=k[3];
    z[8]=k[13];
    z[9]=k[27];
    z[10]=k[12];
    z[11]=k[11];
    z[12]=k[8];
    z[13]=k[9];
    z[14]=k[10];
    z[15]=k[5];
    z[16]=k[21];
    z[17]=k[6];
    z[18]=z[12] - n<T>(1,2);
    z[19]=n<T>(1,2)*z[13];
    z[20]= - z[19] - z[18];
    z[20]=z[13]*z[20];
    z[21]=z[11] + z[12];
    z[22]=z[12] - z[14];
    z[22]=z[13]*z[22];
    z[22]=z[22] + z[14] - z[21];
    z[23]=n<T>(1,2)*z[2];
    z[22]=z[22]*z[23];
    z[18]=z[22] + z[20] + z[18];
    z[18]=z[2]*z[18];
    z[20]=z[12] - 1;
    z[22]=z[13] - 1;
    z[24]=z[12] + z[22];
    z[24]=z[13]*z[24];
    z[24]=z[24] - z[20];
    z[25]=npow(z[13],2);
    z[26]=z[5]*z[10];
    z[25]=z[26] - static_cast<T>(1)+ z[25];
    z[26]=n<T>(1,2)*z[5];
    z[25]=z[25]*z[26];
    z[18]=z[25] + n<T>(1,2)*z[24] + z[18];
    z[18]=z[3]*z[18];
    z[24]=n<T>(1,4)*z[2];
    z[25]=npow(z[11],2);
    z[27]= - z[25]*z[24];
    z[20]=z[13]*z[20];
    z[20]=z[27] + z[20] - z[21];
    z[20]=z[2]*z[20];
    z[21]=npow(z[10],2);
    z[27]=z[21]*z[26];
    z[27]=z[27] - z[10] - z[13];
    z[27]=z[27]*z[26];
    z[28]= - z[12] - n<T>(1,4)*z[13];
    z[28]=z[13]*z[28];
    z[18]=z[18] + z[27] + z[20] + z[28] + n<T>(1,4) + z[12];
    z[18]=z[3]*z[18];
    z[20]= - z[25]*z[23];
    z[25]= - z[14] + z[10];
    z[27]=n<T>(1,4)*z[14] - z[12];
    z[27]=z[13]*z[27];
    z[28]= - n<T>(1,4)*z[21] + z[13];
    z[28]=z[5]*z[28];
    z[18]=z[18] + z[28] + z[20] + z[27] - n<T>(3,4)*z[11] + n<T>(1,4)*z[25] + 
    z[12];
    z[18]=z[3]*z[18];
    z[20]=3*z[8];
    z[25]=n<T>(1,2)*z[6];
    z[27]= - static_cast<T>(1)- z[25];
    z[27]=z[27]*z[20];
    z[28]=static_cast<T>(1)+ n<T>(1,2)*z[16];
    z[28]=z[17]*z[28];
    z[20]=z[20] - z[17];
    z[29]=z[15]*z[20];
    z[27]=n<T>(1,2)*z[29] + z[27] + z[28];
    z[27]=z[15]*z[27];
    z[28]=z[6]*z[10];
    z[21]=z[21] + z[28];
    z[21]=z[6]*z[21];
    z[29]= - z[8]*npow(z[6],2);
    z[21]=z[21] + z[29];
    z[21]=z[5]*z[21];
    z[29]=z[16] - n<T>(3,2)*z[11];
    z[29]=z[11]*z[29];
    z[21]= - z[28] + z[29] + z[27] + z[21];
    z[27]=z[11]*z[16];
    z[29]=npow(z[16],2);
    z[27]=z[29] + z[27];
    z[27]=z[11]*z[27];
    z[29]= - z[17]*z[29];
    z[20]= - z[20]*npow(z[15],2);
    z[20]=z[20] + z[29] + z[27];
    z[20]=z[7]*z[20];
    z[27]= - z[22]*z[12]*z[9];
    z[29]=n<T>(1,4)*z[16];
    z[30]= - z[17]*z[29];
    z[31]=z[9] + n<T>(1,4)*z[6];
    z[31]=z[8]*z[31];
    z[18]=z[18] + n<T>(1,8)*z[20] + z[31] + z[30] + z[27] + n<T>(1,4)*z[21];
    z[20]= - n<T>(5,8)*z[15] + n<T>(1,4)*z[8];
    z[21]=z[29] - z[9];
    z[27]=z[7]*z[16];
    z[29]= - n<T>(1,8)*z[27] + n<T>(5,8);
    z[29]=z[11]*z[29];
    z[29]=z[29] - z[21] + z[20];
    z[29]=z[7]*z[29];
    z[30]=n<T>(3,4)*z[6];
    z[31]=n<T>(3,8)*z[15];
    z[32]=z[2] - z[13] + z[31] - z[30] - n<T>(1,8)*z[16] + z[11];
    z[32]=z[2]*z[32];
    z[33]= - n<T>(1,8)*z[8] + z[9] - n<T>(3,8)*z[6];
    z[33]=z[1]*z[33];
    z[34]=z[7]*z[9];
    z[35]=n<T>(1,4) + z[34];
    z[35]=z[7]*z[35];
    z[35]=n<T>(3,2) + z[35];
    z[35]=z[13]*z[35];
    z[20]=z[32] + z[35] + z[29] + z[33] - z[30] - n<T>(3,4) + z[9] - z[20];
    z[29]=n<T>(3,16)*z[15];
    z[30]= - static_cast<T>(1)+ z[6];
    z[30]=z[30]*z[29];
    z[25]=z[8]*z[25];
    z[25]=z[28] + z[25];
    z[25]=z[1]*z[25];
    z[28]= - z[6]*z[29];
    z[28]= - z[19] - static_cast<T>(1)+ z[28];
    z[28]=z[2]*z[28];
    z[32]=z[7] - n<T>(5,2);
    z[32]=z[32]*z[13];
    z[33]=z[6]*z[31];
    z[33]=z[33] - z[32];
    z[33]=z[33]*z[26];
    z[25]=z[33] + z[28] - z[32] + n<T>(1,8)*z[25] - static_cast<T>(1)+ z[30];
    z[25]=z[5]*z[25];
    z[20]=n<T>(1,2)*z[20] + z[25];
    z[20]=z[5]*z[20];
    z[25]= - z[1] - 1;
    z[25]=z[9]*z[25];
    z[25]=z[34] + n<T>(9,8) + z[25];
    z[25]=z[8]*z[25];
    z[28]=z[16] + n<T>(1,2)*z[11];
    z[25]= - z[31] + n<T>(1,4)*z[28] + z[25];
    z[25]=z[7]*z[25];
    z[30]= - z[9] - n<T>(1,16)*z[8];
    z[30]=z[1]*z[30];
    z[21]=n<T>(1,2)*z[25] + z[30] + z[29] - n<T>(9,16)*z[8] - n<T>(1,16)*z[11] + n<T>(1,16) - z[21];
    z[21]=z[7]*z[21];
    z[25]=n<T>(3,8)*z[1];
    z[29]= - static_cast<T>(3)+ z[6];
    z[29]=z[29]*z[25];
    z[29]=static_cast<T>(1)+ z[29];
    z[30]= - static_cast<T>(1)+ n<T>(1,4)*z[7];
    z[30]=z[30]*z[19];
    z[27]= - n<T>(1,2)*z[27] - n<T>(3,2);
    z[27]=z[15]*z[27];
    z[27]=z[27] + z[28];
    z[27]=z[7]*z[27];
    z[28]=z[1]*z[6];
    z[28]=static_cast<T>(7)- 3*z[28];
    z[27]=5*z[13] + n<T>(1,2)*z[28] + z[27];
    z[27]=z[2]*z[27];
    z[20]=z[20] + n<T>(1,8)*z[27] + z[30] + n<T>(1,2)*z[29] + z[21];
    z[21]=z[19] - 1;
    z[27]= - z[2]*z[21];
    z[28]=z[5]*z[19];
    z[27]=z[28] + z[27] + z[22];
    z[27]=z[5]*z[27];
    z[19]=z[19] + z[1];
    z[28]=n<T>(1,2)*z[1];
    z[29]=static_cast<T>(1)+ z[28];
    z[29]=z[2]*z[29];
    z[29]=z[29] - n<T>(1,2) - z[19];
    z[29]=z[2]*z[29];
    z[22]=z[1] + z[22];
    z[22]=z[27] + n<T>(1,2)*z[22] + z[29];
    z[22]=z[22]*z[26];
    z[27]=z[23] - 1;
    z[27]=z[27]*z[2];
    z[29]=z[1]*z[27];
    z[29]=z[28] + z[29];
    z[29]=z[2]*z[29];
    z[22]=z[29] + z[22];
    z[22]=z[3]*z[22];
    z[29]= - n<T>(5,2) + z[19];
    z[27]=n<T>(1,4)*z[29] - z[27];
    z[27]=z[2]*z[27];
    z[29]=n<T>(1,2) - z[19];
    z[22]=z[22] + n<T>(1,4)*z[29] + z[27];
    z[21]=z[21]*z[24];
    z[24]=n<T>(1,4)*z[10] - z[13];
    z[24]=z[24]*z[26];
    z[26]= - z[28] + n<T>(1,2);
    z[26]=z[10]*z[26];
    z[26]=static_cast<T>(5)+ z[26];
    z[21]=z[24] + z[21] + n<T>(1,8)*z[26] - z[13];
    z[21]=z[5]*z[21];
    z[24]=static_cast<T>(5)+ z[11];
    z[24]=n<T>(1,2)*z[24] - z[1];
    z[23]=z[24]*z[23];
    z[19]=z[23] - n<T>(1,4) + z[19];
    z[19]=z[2]*z[19];
    z[19]=z[19] - n<T>(9,8)*z[13] + static_cast<T>(1)- z[25];
    z[19]=n<T>(1,2)*z[19] + z[21];
    z[19]=z[5]*z[19];
    z[19]=z[19] + n<T>(1,2)*z[22];
    z[19]=z[3]*z[19];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[4]*z[19];

    r += n<T>(1,4)*z[18] + z[19];
 
    return r;
}

template double qg_2lha_r677(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r677(const std::array<dd_real,30>&);
#endif
