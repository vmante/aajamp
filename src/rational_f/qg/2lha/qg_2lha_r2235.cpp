#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2235(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[14];
    z[7]=k[9];
    z[8]=k[13];
    z[9]=z[4] + n<T>(3,2);
    z[10]=n<T>(1,4)*z[6];
    z[11]= - z[9]*z[10];
    z[12]=n<T>(1,8)*z[5];
    z[13]=static_cast<T>(2)- n<T>(5,4)*z[8];
    z[14]=z[8] - static_cast<T>(1)- z[1];
    z[14]=z[3]*z[14];
    z[11]=n<T>(5,12)*z[14] + z[11] + n<T>(1,3)*z[13] + z[12];
    z[11]=z[3]*z[11];
    z[10]=z[10] - 1;
    z[10]=z[10]*z[6];
    z[12]= - z[12] + z[10];
    z[13]=z[5]*z[4];
    z[13]=z[6] - static_cast<T>(1)+ z[13];
    z[13]=z[2]*z[13];
    z[11]=n<T>(1,12)*z[13] + n<T>(1,3)*z[12] + z[11];
    z[11]=z[2]*z[11];
    z[12]=z[5]*z[8];
    z[13]= - z[7]*z[4];
    z[13]=z[13] - z[12];
    z[13]=z[3]*z[13];
    z[14]=z[12] - n<T>(1,2)*z[7];
    z[9]=z[9]*z[6]*z[7];
    z[9]=n<T>(5,6)*z[13] + n<T>(5,3)*z[14] - n<T>(1,2)*z[9];
    z[9]=z[3]*z[9];
    z[10]=z[7]*z[10];
    z[10]= - n<T>(5,4)*z[12] + z[10];

    r += n<T>(1,2)*z[9] + n<T>(1,3)*z[10] + z[11];
 
    return r;
}

template double qg_2lha_r2235(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2235(const std::array<dd_real,30>&);
#endif
