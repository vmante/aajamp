#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r659(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[11];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=k[9];
    z[11]=n<T>(1,2)*z[5];
    z[12]=static_cast<T>(3)+ n<T>(5,2)*z[5];
    z[12]=z[12]*z[11];
    z[13]=npow(z[5],2);
    z[14]=n<T>(1,4)*z[1];
    z[15]=z[13]*z[14];
    z[16]=n<T>(3,4) + z[5];
    z[16]=z[5]*z[16];
    z[15]=z[16] + z[15];
    z[15]=z[1]*z[15];
    z[12]=z[12] + z[15];
    z[12]=z[1]*z[12];
    z[15]=npow(z[2],2);
    z[16]=n<T>(1,2)*z[4];
    z[17]=npow(z[7],2);
    z[18]=z[17]*z[15]*z[16];
    z[19]=n<T>(3,2) + z[5];
    z[19]=z[19]*z[11];
    z[20]=static_cast<T>(9)+ 5*z[7];
    z[21]=n<T>(1,2)*z[7];
    z[22]=z[20]*z[21];
    z[23]=z[17]*z[2];
    z[24]=3*z[23];
    z[25]=z[22] - z[24];
    z[25]=z[2]*z[25];
    z[25]= - z[7] + z[25];
    z[26]=n<T>(1,2)*z[2];
    z[25]=z[25]*z[26];
    z[12]=z[18] + z[25] + z[19] + z[12];
    z[12]=z[4]*z[12];
    z[18]= - static_cast<T>(3)- z[5];
    z[19]=n<T>(1,2)*z[1];
    z[25]=z[19] + 1;
    z[27]=z[25]*z[5];
    z[28]= - n<T>(3,4) - z[27];
    z[28]=z[1]*z[28];
    z[18]=n<T>(1,2)*z[18] + z[28];
    z[18]=z[1]*z[18];
    z[20]= - z[7]*z[20];
    z[20]=z[20] + z[24];
    z[20]=z[20]*z[26];
    z[28]=static_cast<T>(5)+ z[7];
    z[28]=z[7]*z[28];
    z[20]=z[20] + n<T>(7,4) + z[28];
    z[20]=z[2]*z[20];
    z[28]=z[7] + n<T>(1,2);
    z[20]=z[20] - z[28];
    z[20]=z[2]*z[20];
    z[12]=z[12] + z[20] - n<T>(3,4) + z[18];
    z[12]=z[12]*z[16];
    z[18]=static_cast<T>(1)+ z[11];
    z[18]=z[18]*z[11];
    z[13]=z[13]*z[19];
    z[20]=static_cast<T>(1)+ n<T>(3,2)*z[5];
    z[20]=z[5]*z[20];
    z[13]=z[20] + z[13];
    z[13]=z[13]*z[19];
    z[20]=static_cast<T>(1)+ n<T>(3,4)*z[5];
    z[29]=z[5]*z[20];
    z[13]=z[29] + z[13];
    z[13]=z[1]*z[13];
    z[29]=z[17]*npow(z[2],3);
    z[13]=n<T>(1,4)*z[29] + z[18] + z[13];
    z[13]=z[4]*z[13];
    z[18]=n<T>(3,2)*z[1];
    z[29]= - z[18] - 3;
    z[29]=z[5]*z[29];
    z[29]= - static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[19];
    z[20]=z[29] - z[20];
    z[20]=z[1]*z[20];
    z[29]=3*z[7];
    z[30]=z[29] + 5;
    z[30]=z[30]*z[7];
    z[24]=z[30] - z[24];
    z[24]=z[2]*z[24];
    z[24]= - z[29] + z[24];
    z[15]=z[24]*z[15];
    z[13]=z[13] + n<T>(1,4)*z[15] - n<T>(1,2) + z[20];
    z[13]=z[13]*z[16];
    z[15]=n<T>(5,2) + z[1];
    z[15]=z[15]*z[19];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[1]*z[15];
    z[15]=n<T>(1,4) + z[15];
    z[20]= - z[30] + n<T>(3,2)*z[23];
    z[24]=n<T>(1,4)*z[2];
    z[20]=z[20]*z[24];
    z[31]=n<T>(3,8)*z[7];
    z[32]=static_cast<T>(2)+ z[31];
    z[32]=z[7]*z[32];
    z[20]=z[20] + n<T>(1,2) + z[32];
    z[20]=z[2]*z[20];
    z[20]= - n<T>(3,4)*z[28] + z[20];
    z[20]=z[2]*z[20];
    z[25]=z[25]*z[1];
    z[25]=z[25] + n<T>(1,2);
    z[28]=n<T>(1,2)*z[25];
    z[20]= - z[28] + z[20];
    z[20]=z[2]*z[20];
    z[13]=z[13] + n<T>(1,2)*z[15] + z[20];
    z[13]=z[4]*z[13];
    z[15]=n<T>(1,4)*z[7];
    z[20]= - static_cast<T>(13)- z[29];
    z[20]=z[20]*z[15];
    z[29]=z[30] - z[23];
    z[29]=z[29]*z[24];
    z[20]=z[29] - static_cast<T>(1)+ z[20];
    z[20]=z[2]*z[20];
    z[29]=z[7] + 11;
    z[30]=z[29]*z[21];
    z[18]=z[30] + static_cast<T>(5)+ z[18];
    z[18]=n<T>(1,2)*z[18] + z[20];
    z[18]=z[18]*z[26];
    z[20]= - n<T>(7,2) - z[1];
    z[20]=z[20]*z[14];
    z[18]=z[18] - z[31] - static_cast<T>(1)+ z[20];
    z[18]=z[2]*z[18];
    z[20]=static_cast<T>(1)+ z[14];
    z[20]=z[1]*z[20];
    z[20]=n<T>(5,4) + z[20];
    z[20]=z[1]*z[20];
    z[20]=n<T>(1,2) + z[20];
    z[18]=n<T>(1,2)*z[20] + z[18];
    z[18]=z[2]*z[18];
    z[14]= - z[25]*z[14];
    z[13]=z[13] + z[14] + z[18];
    z[13]=z[3]*z[13];
    z[14]=z[22] - z[23];
    z[14]=z[14]*z[26];
    z[18]= - n<T>(9,2) - z[7];
    z[18]=z[7]*z[18];
    z[14]=z[14] - n<T>(7,4) + z[18];
    z[14]=z[2]*z[14];
    z[18]=z[7]*z[29];
    z[18]=z[18] + static_cast<T>(13)+ 5*z[1];
    z[14]=n<T>(1,4)*z[18] + z[14];
    z[14]=z[14]*z[26];
    z[18]= - static_cast<T>(1)- n<T>(3,8)*z[1];
    z[18]=z[1]*z[18];
    z[14]=z[14] - z[15] - n<T>(7,8) + z[18];
    z[14]=z[2]*z[14];
    z[15]=static_cast<T>(3)+ z[1];
    z[15]=z[1]*z[15];
    z[15]=static_cast<T>(3)+ z[15];
    z[15]=z[1]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[12]=z[13] + z[12] + n<T>(1,8)*z[15] + z[14];
    z[12]=z[3]*z[12];
    z[13]=z[21] + 1;
    z[14]=z[13]*z[7];
    z[15]=z[14] - n<T>(3,4)*z[23];
    z[15]=z[2]*z[15];
    z[17]=z[24]*z[17];
    z[18]=z[4]*z[17];
    z[15]=z[15] + z[18];
    z[15]=z[15]*z[16];
    z[18]=z[1]*z[27];
    z[11]=z[11] + z[18];
    z[18]=z[5] + 1;
    z[11]=z[18]*z[11];
    z[18]= - z[14] + n<T>(3,8)*z[23];
    z[18]=z[2]*z[18];
    z[20]=z[21] + 3;
    z[20]=z[20]*z[7];
    z[20]=z[20] + n<T>(3,2);
    z[18]=n<T>(1,4)*z[20] + z[18];
    z[18]=z[2]*z[18];
    z[11]=z[15] + n<T>(1,4)*z[11] + z[18];
    z[11]=z[4]*z[11];
    z[14]=z[14] - z[17];
    z[14]=z[2]*z[14];
    z[14]= - n<T>(1,2)*z[20] + z[14];
    z[14]=z[2]*z[14];
    z[13]=z[14] + z[19] + z[13];
    z[13]=z[2]*z[13];
    z[13]= - z[28] + z[13];
    z[11]=z[12] + n<T>(1,2)*z[13] + z[11];
    z[11]=z[11]*npow(z[3],2);
    z[12]=npow(z[10],2);
    z[13]=z[12]*z[9];
    z[12]= - z[6]*z[12];
    z[12]= - z[13] + z[12];
    z[12]=z[12]*z[16];
    z[14]=static_cast<T>(3)- z[10];
    z[14]=z[6]*z[10]*z[14];
    z[12]=z[12] - z[13] + n<T>(1,2)*z[14];
    z[12]=z[4]*z[12];
    z[14]= - n<T>(3,2) + z[10];
    z[14]=z[6]*z[14];
    z[12]=z[12] - n<T>(1,2)*z[13] + z[14];
    z[12]=z[4]*z[12];
    z[13]= - z[8] + z[1];
    z[13]=z[6]*z[13];
    z[12]=n<T>(1,2)*z[13] + z[12];

    r += z[11] + n<T>(1,8)*z[12];
 
    return r;
}

template double qg_2lha_r659(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r659(const std::array<dd_real,30>&);
#endif
