#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2408(const std::array<T,30>& k) {
  T z[32];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[4];
    z[6]=k[11];
    z[7]=k[12];
    z[8]=npow(z[6],2);
    z[9]=npow(z[7],2);
    z[8]=z[8] + z[9];
    z[10]=n<T>(1,2)*z[5];
    z[11]=z[8]*z[10];
    z[12]=n<T>(1,2)*z[6];
    z[13]=static_cast<T>(3)+ z[12];
    z[13]=z[6]*z[13];
    z[9]=z[11] + z[9] + z[13];
    z[9]=z[9]*z[10];
    z[11]=n<T>(1,2)*z[7];
    z[13]= - static_cast<T>(5)+ z[11];
    z[13]=z[13]*z[11];
    z[14]=z[6] - 1;
    z[9]=z[9] + z[13] - z[14];
    z[9]=z[5]*z[9];
    z[13]=z[6] + z[7];
    z[15]=z[13]*z[5];
    z[16]=3*z[6];
    z[17]=z[15] + z[16];
    z[18]=z[17]*z[10];
    z[19]=n<T>(1,4)*z[7];
    z[18]=z[18] + n<T>(7,4)*z[6] + static_cast<T>(1)+ z[19];
    z[18]=z[5]*z[18];
    z[20]=n<T>(1,2)*z[3];
    z[21]=z[1] - 1;
    z[22]= - z[5] + z[21];
    z[22]=z[22]*z[20];
    z[23]=z[1] + 1;
    z[22]=z[22] + n<T>(1,2)*z[23] + z[5];
    z[22]=z[3]*z[22];
    z[24]=n<T>(3,2)*z[6] + z[23];
    z[18]=z[22] + n<T>(1,2)*z[24] + z[18];
    z[18]=z[3]*z[18];
    z[22]=z[12] - z[7] + z[23];
    z[9]=z[18] + n<T>(1,2)*z[22] + z[9];
    z[9]=z[3]*z[9];
    z[18]=3*z[2];
    z[22]=z[7]*z[2];
    z[24]=z[18] - z[22];
    z[24]=z[24]*z[11];
    z[25]=z[12]*z[7];
    z[26]= - static_cast<T>(1)+ z[7];
    z[26]=z[26]*z[25];
    z[23]= - z[2]*z[23];
    z[23]=z[26] + z[23] + z[24];
    z[24]=z[6]*z[7];
    z[26]= - static_cast<T>(3)+ z[7];
    z[26]=z[7]*z[26];
    z[26]= - z[24] - z[18] + z[26];
    z[26]=z[6]*z[26];
    z[27]=z[2]*z[8];
    z[28]= - z[5]*z[27];
    z[29]=z[2] - z[22];
    z[29]=z[7]*z[29];
    z[26]=z[28] + 3*z[29] + z[26];
    z[28]=n<T>(1,4)*z[5];
    z[26]=z[26]*z[28];
    z[29]=static_cast<T>(1)+ z[11];
    z[29]=z[29]*z[24];
    z[30]=z[11]*z[2];
    z[31]=z[30] - z[2];
    z[31]=z[31]*z[7];
    z[26]=z[26] + z[29] - z[2] - n<T>(3,2)*z[31];
    z[26]=z[5]*z[26];
    z[29]=n<T>(3,2) + z[5];
    z[29]=z[5]*z[29];
    z[29]=n<T>(9,2) + z[29];
    z[29]=z[10]*z[24]*z[29];
    z[15]= - n<T>(3,2)*z[13] - z[15];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(9,2)*z[13] + z[15];
    z[15]=z[15]*z[10];
    z[13]=z[15] - z[13];
    z[13]=z[3]*z[13];
    z[13]=z[13] + z[24] + z[29];
    z[13]=z[4]*z[13];
    z[9]=z[13] + z[9] + n<T>(1,2)*z[23] + z[26];
    z[9]=z[4]*z[9];
    z[13]=z[17]*z[5];
    z[13]=z[13] + z[16];
    z[15]= - static_cast<T>(1)- z[13];
    z[15]=z[15]*z[10];
    z[17]= - n<T>(1,2)*z[1] - z[5];
    z[17]=z[3]*z[17];
    z[15]=z[17] + z[15] - static_cast<T>(1)- z[12];
    z[15]=z[3]*z[15];
    z[17]= - z[12] + z[21];
    z[23]=z[12] + z[7];
    z[23]=z[23]*z[5];
    z[14]=z[7] - z[14];
    z[14]=n<T>(1,2)*z[14] - z[23];
    z[14]=z[5]*z[14];
    z[14]=z[15] + n<T>(1,2)*z[17] + z[14];
    z[14]=z[3]*z[14];
    z[8]=z[8]*z[28];
    z[15]=z[11] - 1;
    z[15]=z[15]*z[7];
    z[17]=z[12] - 1;
    z[17]=z[17]*z[6];
    z[8]=z[8] + z[15] - z[17];
    z[8]=z[8]*z[5];
    z[15]= - static_cast<T>(11)+ z[7];
    z[15]=z[15]*z[11];
    z[17]= - n<T>(9,2) - z[6];
    z[17]=z[6]*z[17];
    z[15]=z[17] + static_cast<T>(3)+ z[15];
    z[15]=n<T>(1,2)*z[15] + z[8];
    z[15]=z[5]*z[15];
    z[14]=z[14] + z[15] + n<T>(1,4)*z[6] + z[1] - z[19];
    z[14]=z[3]*z[14];
    z[15]=z[27]*z[28];
    z[17]=z[12]*z[2];
    z[19]=z[17] - z[2];
    z[19]=z[19]*z[6];
    z[15]=z[31] + z[15] - z[19];
    z[15]=z[15]*z[5];
    z[19]=z[30] - z[18];
    z[19]=z[19]*z[7];
    z[19]=z[19] + z[18];
    z[26]=z[25] + z[2] + n<T>(5,4)*z[7];
    z[26]=z[6]*z[26];
    z[26]= - z[15] - n<T>(1,2)*z[19] + z[26];
    z[26]=z[5]*z[26];
    z[27]= - z[2]*z[1];
    z[9]=z[9] + z[14] + z[26] - n<T>(1,4)*z[24] + z[27] + z[30];
    z[9]=z[4]*z[9];
    z[14]=z[17] - z[18];
    z[14]=z[14]*z[6];
    z[14]=z[14] + z[19];
    z[14]=z[15] + n<T>(1,2)*z[14];
    z[14]=z[14]*z[5];
    z[15]=5*z[7] + z[16];
    z[15]=z[15]*z[10];
    z[15]=z[15] + z[12] + z[21];
    z[13]=z[5]*z[13];
    z[13]=z[13] + static_cast<T>(1)+ z[6];
    z[13]=z[13]*z[20];
    z[16]=z[23] - n<T>(1,2) + z[6];
    z[16]=z[5]*z[16];
    z[13]=z[13] + z[12] + z[16];
    z[13]=z[3]*z[13];
    z[13]=n<T>(1,2)*z[15] + z[13];
    z[13]=z[3]*z[13];
    z[15]= - static_cast<T>(3)+ z[11];
    z[15]=z[7]*z[15];
    z[16]= - static_cast<T>(3)+ z[12];
    z[16]=z[6]*z[16];
    z[15]=z[16] + static_cast<T>(3)+ z[15];
    z[8]=n<T>(1,2)*z[15] + z[8];
    z[8]=z[5]*z[8];
    z[15]=static_cast<T>(3)+ z[6];
    z[12]=z[15]*z[12];
    z[15]= - static_cast<T>(1)+ 3*z[1];
    z[12]=z[12] + z[15];
    z[8]=z[13] + n<T>(1,2)*z[12] + z[8];
    z[8]=z[3]*z[8];
    z[12]=n<T>(1,2)*z[2];
    z[13]= - z[25] - z[12] - z[7];
    z[13]=z[6]*z[13];
    z[15]=z[15]*z[2];
    z[13]=z[13] - z[15] + z[22];
    z[8]=z[9] + z[8] + n<T>(1,2)*z[13] - z[14];
    z[8]=z[4]*z[8];
    z[9]=z[2] - 1;
    z[13]=z[9]*z[22];
    z[16]=z[9]*z[17];
    z[16]=z[13] + z[16];
    z[16]=z[5]*z[16];
    z[19]=z[6]*z[2];
    z[12]=z[16] + z[12] - z[19];
    z[12]=z[5]*z[12];
    z[9]=z[9]*z[19];
    z[9]=z[13] + z[9];
    z[9]=z[5]*z[9];
    z[9]= - 3*z[19] + z[9];
    z[9]=z[9]*npow(z[5],2);
    z[9]= - z[6] + z[9];
    z[9]=z[9]*z[20];
    z[9]=z[12] + z[9];
    z[9]=z[3]*z[9];
    z[12]= - static_cast<T>(5)+ z[18];
    z[12]=z[12]*z[22];
    z[13]= - static_cast<T>(3)+ z[2];
    z[13]=z[13]*z[19];
    z[12]=z[12] + z[13];
    z[10]=z[12]*z[10];
    z[12]= - z[2]*z[21];
    z[10]=z[10] + z[12] - z[17];
    z[9]=n<T>(1,2)*z[10] + z[9];
    z[9]=z[3]*z[9];
    z[10]=npow(z[2],2)*z[11];
    z[10]= - n<T>(3,2)*z[19] - z[15] + z[10];
    z[9]=z[9] + n<T>(1,2)*z[10] - z[14];
    z[9]=z[3]*z[9];
    z[8]=z[9] + z[8];

    r += n<T>(1,4)*z[8];
 
    return r;
}

template double qg_2lha_r2408(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2408(const std::array<dd_real,30>&);
#endif
