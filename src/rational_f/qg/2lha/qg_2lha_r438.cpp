#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r438(const std::array<T,30>& k) {
  T z[49];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[14];
    z[6]=k[9];
    z[7]=k[3];
    z[8]=k[12];
    z[9]=k[6];
    z[10]=k[27];
    z[11]=k[11];
    z[12]=z[2] - 1;
    z[12]=z[12]*z[2];
    z[13]=z[4] + 1;
    z[14]=z[12] + z[13];
    z[15]=z[14]*z[7];
    z[16]=z[12] + 1;
    z[16]=z[16]*z[2];
    z[16]=z[16] - z[13];
    z[15]=z[15] + z[16];
    z[17]= - z[6]*z[15];
    z[18]=z[9]*z[6];
    z[19]=npow(z[2],4);
    z[20]=z[19]*z[18];
    z[17]=z[20] + z[17];
    z[17]=z[10]*z[17];
    z[20]=npow(z[2],3);
    z[21]=z[6]*z[19];
    z[21]= - z[20] + z[21];
    z[21]=z[21]*z[18];
    z[17]=z[21] + z[17];
    z[21]=npow(z[2],2);
    z[22]=5*z[4];
    z[23]=n<T>(17,2) + z[22];
    z[23]=z[4]*z[23];
    z[23]=5*z[21] + n<T>(17,4) + z[23];
    z[24]=n<T>(1,4)*z[6];
    z[16]= - z[16]*z[24];
    z[16]=n<T>(1,3)*z[23] + z[16];
    z[23]=n<T>(1,2)*z[6];
    z[16]=z[16]*z[23];
    z[25]=n<T>(1,3)*z[2];
    z[26]=npow(z[4],2);
    z[27]=z[25]*z[26];
    z[28]= - n<T>(1,3) + z[4];
    z[28]=z[4]*z[28];
    z[28]=z[28] + z[27];
    z[28]=z[28]*z[20];
    z[29]=z[13]*z[4];
    z[30]=z[26]*z[2];
    z[31]=z[29] + z[30];
    z[32]=n<T>(1,3)*z[8];
    z[33]=z[31]*z[32];
    z[34]=z[19]*z[33];
    z[28]=z[28] + z[34];
    z[34]=n<T>(5,2)*z[8];
    z[28]=z[28]*z[34];
    z[14]= - z[14]*z[24];
    z[35]=n<T>(1,4)*z[2];
    z[36]= - n<T>(37,4) - z[22];
    z[36]=z[4]*z[36];
    z[36]= - static_cast<T>(5)+ z[36];
    z[14]=z[14] + n<T>(1,3)*z[36] + z[35];
    z[14]=z[6]*z[14];
    z[14]=n<T>(5,3)*z[29] + z[14];
    z[36]=n<T>(1,2)*z[7];
    z[14]=z[14]*z[36];
    z[37]= - n<T>(1,2) + z[4];
    z[37]=z[2]*z[4]*z[37];
    z[37]=n<T>(1,2)*z[26] + z[37];
    z[37]=z[2]*z[37];
    z[37]= - n<T>(1,2)*z[29] + z[37];
    z[14]=z[14] + z[28] + n<T>(5,3)*z[37] + z[16] + n<T>(1,8)*z[17];
    z[16]=n<T>(1,2)*z[10];
    z[14]=z[14]*z[16];
    z[17]= - z[9]*z[19];
    z[15]=z[17] + z[15];
    z[15]=z[15]*z[16];
    z[17]= - z[4] - z[2];
    z[17]=z[17]*z[36];
    z[19]=n<T>(1,2)*z[9];
    z[28]=z[20]*z[19];
    z[36]=z[4] + n<T>(1,2);
    z[15]=z[15] + z[28] + z[17] + z[36];
    z[15]=z[15]*z[16];
    z[16]=n<T>(1,2)*z[2];
    z[17]=z[16]*z[26];
    z[28]=z[8]*z[31];
    z[28]=n<T>(1,2)*z[28] - z[29] - z[17];
    z[28]=z[28]*z[34];
    z[31]=static_cast<T>(1)+ n<T>(5,4)*z[4];
    z[31]=z[4]*z[31];
    z[15]=z[15] + z[28] + z[31] - z[35];
    z[15]=z[11]*z[15];
    z[28]= - z[36]*z[22];
    z[28]=z[28] + z[27];
    z[28]=z[2]*z[28];
    z[31]=n<T>(1,3)*z[4];
    z[34]= - n<T>(19,4) + z[22];
    z[34]=z[34]*z[31];
    z[36]=n<T>(13,2) - z[22];
    z[36]=z[4]*z[36];
    z[37]= - static_cast<T>(5)- n<T>(7,4)*z[4];
    z[37]=z[6]*z[37];
    z[36]=z[37] - static_cast<T>(1)+ z[36];
    z[37]=n<T>(1,3)*z[6];
    z[36]=z[36]*z[37];
    z[15]=z[15] + z[36] + z[28] - n<T>(3,2) + z[34];
    z[28]=n<T>(19,4)*z[4];
    z[34]=static_cast<T>(7)- z[28];
    z[34]=z[4]*z[34];
    z[34]=z[34] - n<T>(11,4)*z[30];
    z[36]=z[25] - 5;
    z[38]=z[36]*z[24];
    z[38]= - n<T>(7,3) + z[38];
    z[38]=z[6]*z[38];
    z[34]=n<T>(1,3)*z[34] + z[38];
    z[38]=n<T>(35,6) - 9*z[4];
    z[38]=z[4]*z[38];
    z[38]=z[38] - n<T>(19,6)*z[30];
    z[38]=z[2]*z[38];
    z[39]=7*z[4];
    z[40]=static_cast<T>(11)- z[39];
    z[40]=z[4]*z[40];
    z[38]=n<T>(5,6)*z[40] + z[38];
    z[40]= - static_cast<T>(11)- 7*z[2];
    z[41]= - n<T>(3,4) - z[25];
    z[41]=z[2]*z[41];
    z[41]= - n<T>(5,12) + z[41];
    z[41]=z[6]*z[41];
    z[40]=n<T>(5,48)*z[40] + z[41];
    z[40]=z[6]*z[40];
    z[38]=n<T>(1,8)*z[38] + z[40];
    z[38]=z[8]*z[38];
    z[40]=z[2] + 1;
    z[41]=z[40]*z[23];
    z[42]=static_cast<T>(1)+ z[41];
    z[42]=z[6]*z[42];
    z[43]=n<T>(1,2)*z[4];
    z[44]= - static_cast<T>(1)+ z[43];
    z[44]=z[4]*z[44];
    z[17]=z[42] + z[44] + z[17];
    z[17]=z[8]*z[17];
    z[42]=npow(z[6],2);
    z[42]=z[42] + z[26];
    z[42]=n<T>(1,2)*z[42];
    z[17]=z[42] + z[17];
    z[17]=z[7]*z[17];
    z[17]=n<T>(1,6)*z[17] + n<T>(1,4)*z[34] + z[38];
    z[17]=z[7]*z[17];
    z[34]= - n<T>(23,2) + z[22];
    z[34]=z[34]*z[31];
    z[38]=n<T>(11,2)*z[4];
    z[44]=n<T>(17,3) + z[38];
    z[44]=z[4]*z[44];
    z[44]=z[44] + n<T>(13,6)*z[30];
    z[44]=z[44]*z[16];
    z[34]=z[34] + z[44];
    z[44]=static_cast<T>(1)- n<T>(151,48)*z[4];
    z[44]=z[4]*z[44];
    z[44]=z[44] - n<T>(27,16)*z[30];
    z[44]=z[2]*z[44];
    z[45]=static_cast<T>(5)- z[39];
    z[45]=z[4]*z[45];
    z[44]=n<T>(5,48)*z[45] + z[44];
    z[44]=z[2]*z[44];
    z[45]=z[4] - 1;
    z[46]=z[45]*z[4];
    z[47]= - n<T>(25,48) - z[2];
    z[47]=z[2]*z[47];
    z[47]=n<T>(35,48) + z[47];
    z[47]=z[6]*z[47];
    z[44]=z[47] + n<T>(35,48)*z[46] + z[44];
    z[44]=z[8]*z[44];
    z[47]=static_cast<T>(4)+ n<T>(1,8)*z[2];
    z[48]=static_cast<T>(13)+ 9*z[2];
    z[48]=z[6]*z[48];
    z[47]=n<T>(1,3)*z[47] + n<T>(1,16)*z[48];
    z[47]=z[6]*z[47];
    z[17]=z[17] + z[44] + n<T>(1,4)*z[34] + z[47];
    z[17]=z[8]*z[17];
    z[34]=5*z[30];
    z[28]= - z[28] + z[34];
    z[44]=n<T>(5,3) - n<T>(3,2)*z[4];
    z[44]=z[44]*z[23];
    z[28]=n<T>(1,3)*z[28] + z[44];
    z[17]=n<T>(1,2)*z[28] + z[17];
    z[17]=z[7]*z[17];
    z[28]=z[45]*z[31];
    z[28]=z[33] + z[28] + z[30];
    z[28]=z[8]*z[28];
    z[33]= - z[13]*z[31];
    z[44]=z[26] + z[27];
    z[44]=z[2]*z[44];
    z[28]=z[28] + z[33] + z[44];
    z[33]=n<T>(7,16)*z[4];
    z[44]=z[33] + 2;
    z[45]= - z[23] + z[44];
    z[45]=z[6]*z[45];
    z[47]=n<T>(17,4) + 5*z[2];
    z[48]=z[6]*z[40];
    z[48]=z[48] - z[47];
    z[48]=z[9]*z[48]*z[24];
    z[41]=z[9]*z[41];
    z[41]= - z[6] + z[41];
    z[41]=z[5]*z[41];
    z[41]=z[41] + z[45] + z[48];
    z[45]=n<T>(1,3)*z[5];
    z[41]=z[41]*z[45];
    z[48]= - n<T>(11,4) + z[22];
    z[48]=z[48]*z[31];
    z[48]= - static_cast<T>(1)+ z[48];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[33]*z[37];
    z[33]=n<T>(1,4)*z[48] + z[33];
    z[33]=z[6]*z[33];
    z[37]=z[16] + n<T>(1,3);
    z[48]= - n<T>(3,16) - z[25];
    z[48]=z[6]*z[48];
    z[48]=n<T>(1,2)*z[37] + z[48];
    z[18]=z[48]*z[18];
    z[18]=z[41] + z[18] + z[33] + n<T>(5,4)*z[28];
    z[18]=z[5]*z[18];
    z[28]=n<T>(37,8) + z[4];
    z[28]=z[28]*z[31];
    z[28]=z[28] + n<T>(9,16)*z[30];
    z[28]=z[2]*z[28];
    z[33]=n<T>(47,4) + z[22];
    z[28]=n<T>(1,12)*z[33] + z[28];
    z[28]=z[28]*z[21];
    z[33]= - static_cast<T>(13)- z[38];
    z[33]=z[4]*z[33];
    z[33]=z[33] - n<T>(11,2)*z[30];
    z[33]=z[33]*z[25];
    z[13]= - n<T>(5,2)*z[13] + z[33];
    z[13]=z[8]*z[13]*z[20];
    z[13]=z[28] + n<T>(1,8)*z[13];
    z[13]=z[8]*z[13];
    z[20]=z[40]*z[19];
    z[20]=z[20] - 1;
    z[28]=z[5]*z[20];
    z[33]=z[9]*z[47];
    z[28]=z[28] - n<T>(1,4)*z[33] + z[44];
    z[28]=z[28]*z[45];
    z[33]=z[37]*z[19];
    z[37]= - static_cast<T>(2)- n<T>(31,16)*z[4];
    z[28]=z[28] + n<T>(1,3)*z[37] + z[33];
    z[28]=z[5]*z[28];
    z[33]= - static_cast<T>(1)- z[16];
    z[33]=z[33]*z[19];
    z[37]=n<T>(1,2)*z[5];
    z[20]=z[20]*z[37];
    z[20]=z[20] + static_cast<T>(1)+ z[33];
    z[20]=z[5]*z[20];
    z[33]= - static_cast<T>(1)+ z[19];
    z[20]=n<T>(1,2)*z[33] + z[20];
    z[20]=z[3]*z[20];
    z[27]= - z[4] - z[27];
    z[27]=z[2]*z[27];
    z[33]= - static_cast<T>(2)- n<T>(17,16)*z[4];
    z[27]=n<T>(1,3)*z[33] + z[27];
    z[27]=z[2]*z[27];
    z[33]= - n<T>(1,24) + z[4];
    z[13]=n<T>(1,3)*z[20] + z[28] + n<T>(1,48)*z[9] + z[13] + n<T>(1,2)*z[33] + 
    z[27];
    z[13]=z[3]*z[13];
    z[20]=z[29]*z[6];
    z[20]= - z[26] + z[20] + z[30];
    z[20]=n<T>(5,6)*z[20];
    z[27]= - z[6]*z[31];
    z[27]=z[4] + z[27];
    z[27]=z[27]*z[23];
    z[28]=z[31]*z[5];
    z[29]= - z[6]*z[28];
    z[27]=z[27] + z[29];
    z[27]=z[5]*z[27];
    z[27]= - z[20] + z[27];
    z[27]=z[5]*z[27];
    z[29]=z[43] - z[28];
    z[29]=z[29]*npow(z[5],2);
    z[28]=z[4] - z[28];
    z[28]=z[28]*z[37];
    z[33]=z[16] - 1;
    z[37]=z[4]*z[33];
    z[37]=n<T>(1,2) + z[37];
    z[28]=n<T>(1,3)*z[37] + z[28];
    z[28]=z[3]*z[28];
    z[28]=z[29] + z[28];
    z[28]=z[3]*z[28];
    z[20]=z[28] + z[20] + z[27];
    z[20]=z[1]*z[20];
    z[27]=static_cast<T>(1)+ n<T>(1,4)*z[4];
    z[27]=z[27]*z[39];
    z[27]=z[27] + n<T>(55,16)*z[30];
    z[27]=z[27]*z[25];
    z[28]=n<T>(11,8) - n<T>(5,3)*z[4];
    z[28]=z[4]*z[28];
    z[28]=n<T>(3,4) + z[28];
    z[27]=n<T>(1,2)*z[28] + z[27];
    z[27]=z[2]*z[27];
    z[28]=n<T>(1,4) - n<T>(5,3)*z[2];
    z[28]=z[2]*z[28];
    z[28]= - n<T>(47,12) + z[28];
    z[28]=z[28]*z[24];
    z[29]= - n<T>(41,4) - 31*z[4];
    z[29]=z[4]*z[29];
    z[29]=z[29] - n<T>(89,4)*z[30];
    z[25]=z[29]*z[25];
    z[25]= - n<T>(15,4)*z[4] + z[25];
    z[21]=z[25]*z[21];
    z[21]= - n<T>(35,12)*z[26] + z[21];
    z[21]=z[8]*z[21];
    z[22]=static_cast<T>(47)- z[22];
    z[22]=z[4]*z[22];
    z[21]=n<T>(1,4)*z[21] + z[28] + n<T>(1,48)*z[22] + z[27];
    z[21]=z[8]*z[21];
    z[22]=z[46] + z[34];
    z[22]=z[22]*z[35];
    z[22]=z[39] + z[22];
    z[16]= - n<T>(5,3) - z[16];
    z[16]=z[2]*z[16];
    z[16]=n<T>(17,6) + z[16];
    z[16]=z[16]*z[23];
    z[25]= - static_cast<T>(7)+ z[35];
    z[16]=n<T>(1,3)*z[25] + z[16];
    z[16]=z[6]*z[16];
    z[16]=n<T>(1,3)*z[22] + z[16];
    z[22]= - static_cast<T>(5)+ z[43];
    z[22]=z[22]*z[31];
    z[25]=z[36]*z[23];
    z[25]=n<T>(5,3) + z[25];
    z[25]=z[6]*z[25];
    z[22]=z[25] + z[22] + n<T>(3,2)*z[30];
    z[25]=z[6] - z[4];
    z[26]=z[25]*z[8];
    z[27]=z[42] + z[26];
    z[27]=z[7]*z[27];
    z[22]=n<T>(1,3)*z[27] + n<T>(1,4)*z[22] - z[26];
    z[22]=z[7]*z[22];
    z[16]=z[22] + n<T>(1,2)*z[16] + z[26];
    z[16]=z[7]*z[16];
    z[12]=static_cast<T>(7)- z[12];
    z[12]=z[2]*z[12];
    z[12]= - n<T>(1,3) + z[12];
    z[12]=z[12]*z[23];
    z[12]= - 3*z[33] + z[12];
    z[12]=z[12]*z[24];
    z[22]= - z[25]*z[32];
    z[23]= - static_cast<T>(1)+ n<T>(11,8)*z[2];
    z[23]=z[4]*z[23];
    z[12]=z[16] + z[22] + n<T>(1,3)*z[23] + z[12];
    z[12]=z[12]*z[19];

    r += z[12] + z[13] + z[14] + n<T>(1,4)*z[15] + z[17] + z[18] + n<T>(1,2)*
      z[20] + z[21];
 
    return r;
}

template double qg_2lha_r438(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r438(const std::array<dd_real,30>&);
#endif
