#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2303(const std::array<T,30>& k) {
  T z[34];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[13];
    z[5]=k[11];
    z[6]=k[4];
    z[7]=k[24];
    z[8]=k[3];
    z[9]=k[9];
    z[10]=n<T>(1,2)*z[2];
    z[11]=z[10] + 1;
    z[12]=z[11]*z[10];
    z[12]=z[12] + n<T>(3,4);
    z[13]=z[2] + 1;
    z[14]=z[13]*z[2];
    z[15]=z[14] + 1;
    z[15]=z[15]*z[2];
    z[16]=z[4] - 1;
    z[15]=z[15] - z[16];
    z[17]=n<T>(1,4)*z[3];
    z[18]= - z[15]*z[17];
    z[18]=z[18] - z[4] + z[12];
    z[18]=z[3]*z[18];
    z[19]=z[9] - 1;
    z[20]=z[8]*z[9];
    z[21]=z[19]*z[20];
    z[22]= - n<T>(1,2)*z[21] + static_cast<T>(1)- n<T>(3,2)*z[9];
    z[22]=z[8]*z[22];
    z[23]=z[4] - n<T>(1,2);
    z[24]=z[23] - z[7];
    z[25]=n<T>(1,2)*z[4];
    z[26]=z[24]*z[25];
    z[27]=z[2]*z[7];
    z[28]=z[27] - static_cast<T>(1)+ z[7];
    z[28]=z[28]*z[10];
    z[29]=n<T>(1,4)*z[4];
    z[30]=z[29] - 1;
    z[30]=z[30]*z[4];
    z[12]=z[12] + z[30];
    z[12]=z[12]*z[3];
    z[30]=z[4] - 3;
    z[31]= - z[4]*z[30];
    z[31]= - static_cast<T>(1)+ z[31];
    z[31]=n<T>(1,2)*z[31] + z[12];
    z[31]=z[3]*z[31];
    z[32]=z[8]*npow(z[9],2);
    z[33]= - z[9] - n<T>(1,4)*z[32];
    z[33]=z[8]*z[33];
    z[31]=z[31] - n<T>(1,4) + 3*z[33];
    z[31]=z[6]*z[31];
    z[33]=n<T>(1,2)*z[7];
    z[18]=n<T>(1,2)*z[31] + z[18] + z[28] + z[26] + z[33] + z[22];
    z[18]=z[6]*z[18];
    z[22]= - z[9] - n<T>(1,2)*z[32];
    z[22]=z[8]*z[22];
    z[22]=n<T>(1,2) + z[22];
    z[22]=z[8]*z[22];
    z[13]=z[22] + z[13];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[6]*z[12];
    z[13]=z[20] - z[19];
    z[13]=z[13]*npow(z[8],2);
    z[13]=z[13] - z[4];
    z[19]=n<T>(1,2)*z[3];
    z[15]= - z[15]*z[19];
    z[22]= - static_cast<T>(1)- z[8];
    z[22]=n<T>(1,2)*z[22] - z[2];
    z[22]=z[2]*z[22];
    z[12]=z[12] + z[15] + n<T>(1,2)*z[13] + z[22];
    z[12]=z[6]*z[12];
    z[13]=z[19]*npow(z[2],4);
    z[15]=npow(z[2],3);
    z[22]=z[15] + z[13];
    z[12]=n<T>(1,2)*z[22] + z[12];
    z[12]=z[5]*z[12];
    z[22]=npow(z[2],2);
    z[26]= - static_cast<T>(1)- z[27];
    z[26]=z[26]*z[22];
    z[13]= - z[15] + z[13];
    z[13]=z[13]*z[19];
    z[13]=z[26] + z[13];
    z[12]=z[12] + n<T>(1,2)*z[13] + z[18];
    z[12]=z[5]*z[12];
    z[13]= - n<T>(1,2) - z[9];
    z[13]=z[13]*z[20];
    z[18]=n<T>(1,2) - z[9];
    z[13]=5*z[18] + 3*z[13];
    z[14]=z[14] - z[16];
    z[14]=z[3]*z[14];
    z[18]=n<T>(1,2) + z[4];
    z[18]=z[4]*z[18];
    z[18]=n<T>(3,2)*z[2] + n<T>(1,2) + z[18];
    z[14]=n<T>(1,2)*z[18] + z[14];
    z[14]=z[3]*z[14];
    z[18]= - z[30]*z[25];
    z[11]=z[18] - z[11];
    z[11]=z[3]*z[11];
    z[11]= - n<T>(1,2) + z[11];
    z[11]=z[11]*z[19];
    z[11]= - z[9] + z[11];
    z[11]=z[6]*z[11];
    z[18]= - z[4]*z[24];
    z[11]=z[11] + z[14] + z[27] + n<T>(1,2)*z[13] + z[18];
    z[11]=z[6]*z[11];
    z[13]=z[8]*z[16];
    z[13]=static_cast<T>(3)+ z[13];
    z[13]=z[4]*z[13];
    z[13]=z[13] - 3*z[9] - z[21];
    z[14]=z[3]*z[15];
    z[14]= - z[22] - n<T>(3,2)*z[14];
    z[14]=z[14]*z[19];
    z[15]=z[7]*z[22];
    z[11]=z[11] + z[14] + n<T>(1,2)*z[13] + z[15];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[5]*z[11];
    z[12]=z[6]*z[9];
    z[12]=z[12] - 1;
    z[12]=z[33]*z[12];
    z[10]= - z[10] + z[16];
    z[10]=z[3]*z[10];
    z[13]= - z[4]*z[23];
    z[10]=z[10] - n<T>(1,2) + z[13];
    z[10]=z[10]*z[17];
    z[13]=z[9]*z[7];
    z[14]=npow(z[4],2);
    z[10]=z[10] + n<T>(1,8)*z[14] + z[13] + z[12];
    z[10]=z[6]*z[10];
    z[12]= - z[1] - z[8];
    z[12]=z[12]*z[29];
    z[13]=z[8] - 1;
    z[13]=z[7]*z[13];
    z[12]=z[12] - n<T>(1,2) + z[13];
    z[12]=z[4]*z[12];
    z[13]= - n<T>(1,2)*z[1] + z[2];
    z[13]=z[3]*z[13];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[19]*z[2]*z[13];
    z[14]= - n<T>(3,4)*z[9] + n<T>(3,4) + z[7];
    z[14]=z[9]*z[14];
    z[12]=z[13] + z[14] + z[12];

    r += z[10] + z[11] + n<T>(1,2)*z[12];
 
    return r;
}

template double qg_2lha_r2303(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2303(const std::array<dd_real,30>&);
#endif
