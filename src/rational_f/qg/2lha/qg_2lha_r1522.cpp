#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1522(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[11];
    z[7]=k[12];
    z[8]=n<T>(9,2)*z[5];
    z[9]=z[6] + z[7];
    z[8]=z[9]*z[8];
    z[8]= - static_cast<T>(5)+ z[8];
    z[9]=5*z[2];
    z[10]=static_cast<T>(3)- z[2];
    z[10]=z[10]*z[9];
    z[11]=9*z[5];
    z[12]=npow(z[6],2);
    z[11]=z[11]*z[12];
    z[13]=9*z[7];
    z[14]=z[13] - z[6];
    z[15]=z[11] + z[14];
    z[16]=n<T>(1,2)*z[4];
    z[15]=z[5]*z[15]*z[16];
    z[8]=z[15] + 3*z[8] + z[10];
    z[8]=z[8]*z[16];
    z[10]=z[1] - 1;
    z[15]=z[10]*z[2];
    z[17]=static_cast<T>(5)- 3*z[1];
    z[17]=n<T>(1,2)*z[17] + z[15];
    z[17]=z[17]*z[9];
    z[18]=z[5]*z[7];
    z[18]= - static_cast<T>(5)+ n<T>(9,2)*z[18];
    z[8]=z[8] + n<T>(3,2)*z[18] + z[17];
    z[8]=z[4]*z[8];
    z[17]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[17]=z[17]*z[1];
    z[17]=z[17] + n<T>(1,2);
    z[17]=z[17]*z[2];
    z[19]= - z[17] - z[10];
    z[19]=z[19]*z[9];
    z[20]=z[13]*z[5];
    z[21]= - static_cast<T>(19)+ z[20];
    z[8]=z[8] + n<T>(1,4)*z[21] + z[19];
    z[8]=z[3]*z[8];
    z[19]=n<T>(5,2)*z[10] + 3*z[17];
    z[19]=z[19]*z[9];
    z[21]=n<T>(47,2) - z[20];
    z[8]=z[8] + n<T>(1,2)*z[21] + z[19];
    z[11]=n<T>(1,2)*z[14] + z[11];
    z[11]=z[4]*z[11];
    z[11]=z[11] + 9*z[6] + z[13] + 5;
    z[14]= - static_cast<T>(1)+ n<T>(9,8)*z[6];
    z[14]=z[14]*z[6];
    z[14]=z[14] - n<T>(9,4)*z[7];
    z[19]=z[12]*z[5];
    z[21]= - n<T>(9,8)*z[19] + z[14];
    z[21]=z[5]*z[21];
    z[22]= - static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[22]=z[22]*z[9];
    z[11]=z[22] + z[21] + n<T>(1,4)*z[11];
    z[11]=z[4]*z[11];
    z[21]= - n<T>(5,4)*z[15] - n<T>(9,4) + z[1];
    z[21]=z[21]*z[9];
    z[11]=z[11] + z[21] + n<T>(9,8)*z[7] - z[18];
    z[11]=z[4]*z[11];
    z[8]=z[11] + n<T>(1,2)*z[8];
    z[8]=z[3]*z[8];
    z[11]=n<T>(5,4)*z[2];
    z[21]=static_cast<T>(1)- z[2];
    z[11]=z[21]*z[11];
    z[12]=z[12]*z[4];
    z[11]=n<T>(9,8)*z[12] + z[11] - n<T>(9,4)*z[19] + z[14];
    z[11]=z[4]*z[11];
    z[13]= - z[13] + z[18];
    z[14]=static_cast<T>(5)- z[1];
    z[14]=n<T>(1,4)*z[14] + z[15];
    z[14]=z[14]*z[9];
    z[11]=z[11] + n<T>(1,4)*z[13] + z[14];
    z[11]=z[4]*z[11];
    z[13]= - n<T>(3,2)*z[17] - z[10];
    z[13]=z[13]*z[9];
    z[14]= - static_cast<T>(37)+ z[20];
    z[8]=z[8] + z[11] + n<T>(1,8)*z[14] + z[13];
    z[8]=z[3]*z[8];
    z[10]=n<T>(1,2)*z[10] + z[17];
    z[10]=z[10]*z[9];
    z[11]= - static_cast<T>(1)- z[15];
    z[9]=z[11]*z[9];
    z[11]=z[12] - z[7];
    z[9]=z[9] - n<T>(9,2)*z[11];
    z[9]=z[9]*z[16];
    z[9]=z[9] + n<T>(9,4) + z[10];
    z[8]=n<T>(1,2)*z[9] + z[8];

    r += z[8]*z[3];
 
    return r;
}

template double qg_2lha_r1522(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1522(const std::array<dd_real,30>&);
#endif
