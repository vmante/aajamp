#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1722(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=z[4]*z[6];
    z[8]= - static_cast<T>(1)+ z[5];
    z[8]=z[6]*z[8];
    z[8]=n<T>(3322,45) + z[8];
    z[8]=z[8]*z[7];
    z[9]=z[5] + 1;
    z[10]=z[9]*z[6];
    z[11]=z[1]*npow(z[2],2);
    z[12]=2*z[2] - z[11];
    z[12]=z[1]*z[12];
    z[8]=z[8] + 2*z[10] - static_cast<T>(2)+ n<T>(3547,45)*z[12];
    z[8]=z[4]*z[8];
    z[12]=npow(z[1],2);
    z[13]= - 3*z[2] + z[11];
    z[13]=z[13]*z[12];
    z[14]= - z[6]*z[5];
    z[14]=static_cast<T>(3)+ z[14];
    z[14]=z[14]*z[7];
    z[10]=static_cast<T>(1)- z[10];
    z[10]=3*z[10] + z[14];
    z[10]=z[4]*z[10];
    z[10]=z[13] + z[10];
    z[10]=z[4]*z[10];
    z[13]=z[2] + 1;
    z[13]=z[13]*z[2];
    z[13]=z[13] - z[11];
    z[14]= - z[13]*npow(z[1],3);
    z[10]=z[14] + z[10];
    z[10]=z[3]*z[10];
    z[12]=z[13]*z[12];
    z[8]=z[10] + n<T>(3592,45)*z[12] + z[8];
    z[8]=z[3]*z[8];
    z[10]= - static_cast<T>(1766)- n<T>(3547,3)*z[2];
    z[10]=z[2]*z[10];
    z[10]=z[10] + n<T>(3547,3)*z[11];
    z[10]=z[1]*z[10];
    z[11]=n<T>(1,2)*z[6];
    z[9]= - z[9]*z[11];
    z[12]=static_cast<T>(1)- n<T>(1,2)*z[5];
    z[12]=z[6]*z[12];
    z[12]= - n<T>(3457,30) + z[12];
    z[7]=z[12]*z[7];
    z[7]=z[8] + z[7] + z[9] + n<T>(1,2) + n<T>(1,15)*z[10];
    z[7]=z[3]*z[7];
    z[8]=n<T>(1751,45) - z[11];
    z[8]=z[6]*z[8];
    z[7]=z[8] + z[7];

    r += z[7]*z[3];
 
    return r;
}

template double qg_2lha_r1722(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1722(const std::array<dd_real,30>&);
#endif
