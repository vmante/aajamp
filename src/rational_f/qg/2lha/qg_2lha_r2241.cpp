#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2241(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[9];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[7];
    z[8]=k[14];
    z[9]=k[10];
    z[10]=z[3] - n<T>(1,2);
    z[11]=z[6] - z[3];
    z[11]=z[5]*z[11];
    z[11]=z[11] - z[6] - z[10];
    z[12]=n<T>(1,3)*z[5];
    z[11]=z[11]*z[12];
    z[13]=z[3]*z[4];
    z[14]= - n<T>(1,3) + z[13];
    z[11]=n<T>(1,2)*z[14] + z[11];
    z[14]=n<T>(1,2)*z[5];
    z[15]= - z[14] + 1;
    z[15]=z[6]*z[15];
    z[10]=z[10] + z[15];
    z[10]=z[5]*z[10];
    z[13]= - static_cast<T>(1)- z[13];
    z[13]=z[3]*z[13];
    z[13]=z[13] - z[6] + static_cast<T>(1)+ z[1];
    z[10]=n<T>(1,2)*z[13] + z[10];
    z[10]=z[2]*z[10];
    z[10]=n<T>(1,2)*z[11] + n<T>(1,3)*z[10];
    z[10]=z[2]*z[10];
    z[11]=z[5]*z[3];
    z[11]=n<T>(1,2) + z[11];
    z[11]=z[11]*z[12];
    z[11]= - n<T>(1,2)*z[4] + z[11];
    z[10]=n<T>(1,2)*z[11] + z[10];
    z[10]=z[2]*z[10];
    z[11]=z[7] + z[4];
    z[11]=z[8]*z[11];
    z[12]=z[9]*z[14];
    z[11]=n<T>(1,3)*z[11] + z[12];

    r += z[10] + n<T>(1,2)*z[11];
 
    return r;
}

template double qg_2lha_r2241(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2241(const std::array<dd_real,30>&);
#endif
