#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r499(const std::array<T,30>& k) {
  T z[35];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[5];
    z[6]=k[3];
    z[7]=k[9];
    z[8]=k[12];
    z[9]=z[2] + 1;
    z[10]=npow(z[2],2);
    z[11]=z[9]*z[10];
    z[12]=z[10]*z[4];
    z[13]=static_cast<T>(1)+ n<T>(3,8)*z[2];
    z[13]=z[2]*z[13];
    z[13]=n<T>(3,8) + z[13];
    z[13]=z[13]*z[12];
    z[11]=n<T>(5,8)*z[11] + z[13];
    z[11]=z[4]*z[11];
    z[13]=npow(z[2],3);
    z[14]=n<T>(1,2)*z[4];
    z[15]=z[13]*z[14];
    z[16]= - static_cast<T>(1)+ z[10];
    z[16]=z[16]*z[15];
    z[17]=npow(z[2],4);
    z[16]=z[17] + z[16];
    z[16]=z[4]*z[16];
    z[18]=n<T>(1,2)*z[2];
    z[19]=z[18] + 1;
    z[20]=z[19]*z[2];
    z[20]=z[20] + n<T>(1,2);
    z[20]=z[20]*z[4];
    z[20]=z[20] + z[9];
    z[20]=z[4]*z[17]*z[20];
    z[17]=z[20] + n<T>(1,2)*z[17];
    z[20]= - z[8]*z[17];
    z[16]=z[20] + n<T>(1,2)*z[13] + z[16];
    z[20]=n<T>(1,2)*z[8];
    z[16]=z[16]*z[20];
    z[11]=z[16] + n<T>(1,4)*z[10] + z[11];
    z[11]=z[8]*z[11];
    z[16]=n<T>(3,2)*z[2];
    z[21]=z[16] + 1;
    z[22]=z[21]*z[13];
    z[23]=n<T>(3,4)*z[2];
    z[24]=z[23] + 1;
    z[24]=z[24]*z[2];
    z[24]=z[24] + n<T>(1,4);
    z[25]=z[13]*z[4];
    z[26]=z[24]*z[25];
    z[22]=z[22] + z[26];
    z[22]=z[4]*z[22];
    z[17]= - z[17]*z[20];
    z[17]=z[17] + n<T>(3,4)*z[13] + z[22];
    z[17]=z[8]*z[17];
    z[22]=n<T>(1,2) - z[2];
    z[22]=z[22]*z[18];
    z[26]=z[2] + n<T>(1,2);
    z[27]= - z[26]*z[15];
    z[27]= - z[13] + z[27];
    z[27]=z[4]*z[27];
    z[17]=z[17] + z[22] + z[27];
    z[17]=z[3]*z[17];
    z[22]=z[12]*z[9];
    z[27]= - 5*z[10] - 3*z[22];
    z[27]=z[27]*z[14];
    z[27]= - z[2] + z[27];
    z[11]=n<T>(1,2)*z[17] + n<T>(1,4)*z[27] + z[11];
    z[17]=z[12] + z[2];
    z[27]= - z[17]*z[14];
    z[15]= - z[10] - z[15];
    z[15]=z[4]*z[15];
    z[15]= - z[18] + z[15];
    z[15]=z[3]*z[15];
    z[15]=z[27] + z[15];
    z[15]=z[1]*z[15];
    z[11]=n<T>(1,8)*z[15] + n<T>(1,2)*z[11];
    z[11]=z[3]*z[11];
    z[15]=static_cast<T>(5)+ n<T>(17,2)*z[2];
    z[15]=z[2]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[15]*z[14];
    z[27]=3*z[2];
    z[15]=z[15] - n<T>(1,2) + z[27];
    z[28]=n<T>(1,4)*z[4];
    z[15]=z[28]*z[10]*z[15];
    z[29]= - static_cast<T>(3)- 5*z[2];
    z[13]=z[29]*z[13];
    z[29]= - static_cast<T>(1)- n<T>(5,8)*z[2];
    z[29]=z[2]*z[29];
    z[29]= - n<T>(3,8) + z[29];
    z[25]=z[29]*z[25];
    z[13]=n<T>(1,8)*z[13] + z[25];
    z[25]=z[8]*z[4];
    z[13]=z[13]*z[25];
    z[13]=z[15] + z[13];
    z[13]=z[8]*z[13];
    z[15]=n<T>(1,4)*z[2];
    z[29]=z[2] - 1;
    z[30]= - z[29]*z[15];
    z[31]=z[4]*z[2];
    z[23]=static_cast<T>(1)- z[23];
    z[23]=z[2]*z[23];
    z[23]=n<T>(1,4) + z[23];
    z[23]=z[23]*z[31];
    z[23]=z[30] + z[23];
    z[23]=z[23]*z[14];
    z[13]=z[23] + z[13];
    z[13]=z[8]*z[13];
    z[23]=static_cast<T>(1)+ n<T>(5,2)*z[2];
    z[23]=z[23]*z[31];
    z[23]= - z[27] + z[23];
    z[23]=z[23]*z[28];
    z[30]= - static_cast<T>(1)- n<T>(7,8)*z[2];
    z[30]=z[2]*z[30];
    z[30]= - n<T>(1,8) + z[30];
    z[30]=z[30]*z[31];
    z[32]=static_cast<T>(1)+ 7*z[2];
    z[33]=z[2]*z[32];
    z[30]=n<T>(1,8)*z[33] + z[30];
    z[25]=z[30]*z[25];
    z[23]=z[23] + z[25];
    z[25]=npow(z[8],2);
    z[23]=z[23]*z[25];
    z[30]= - z[9]*z[14];
    z[30]=static_cast<T>(1)+ z[30];
    z[33]=z[6]*npow(z[8],3);
    z[34]=z[33]*z[2];
    z[30]=z[14]*z[30]*z[34];
    z[23]=z[23] + z[30];
    z[23]=z[6]*z[23];
    z[30]= - z[21]*z[18];
    z[12]=3*z[12];
    z[12]=z[26]*z[12];
    z[12]=z[30] + z[12];
    z[12]=z[4]*z[12];
    z[26]=3*z[10];
    z[24]= - z[8]*z[24]*npow(z[4],2)*z[26];
    z[12]=z[12] + z[24];
    z[12]=z[8]*z[12];
    z[16]=static_cast<T>(1)- z[16];
    z[16]=z[16]*z[31];
    z[16]=n<T>(1,2)*z[29] + z[16];
    z[16]=z[16]*z[14];
    z[12]=z[16] + z[12];
    z[12]=z[12]*z[20];
    z[12]=z[12] + z[23];
    z[12]=z[6]*z[12];
    z[16]=z[4] - z[29];
    z[21]=z[21]*z[8]*z[2];
    z[16]=n<T>(1,2)*z[16] + z[21];
    z[16]=z[16]*z[20];
    z[20]=z[18]*z[8];
    z[21]= - z[32]*z[20];
    z[21]=z[27] + z[21];
    z[21]=z[21]*z[25];
    z[21]=n<T>(1,2)*z[21] - z[34];
    z[21]=z[6]*z[21];
    z[16]=z[16] + z[21];
    z[16]=z[6]*z[16];
    z[9]= - z[9]*z[18]*z[33];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[2]*z[15]*z[25];
    z[9]=z[15] + z[9];
    z[9]=z[6]*z[9];
    z[9]= - z[20] + z[9];
    z[9]=z[6]*z[9];
    z[14]=z[14] + 1;
    z[14]=z[14]*z[4];
    z[14]=z[14] + n<T>(1,2);
    z[9]=n<T>(1,2)*z[14] + z[9];
    z[9]=z[7]*z[9];
    z[14]= - z[20] - z[14];
    z[9]=z[9] + n<T>(1,2)*z[14] + z[16];
    z[9]=z[7]*z[9];
    z[14]= - z[1]*z[17];
    z[10]=z[14] + z[10] + z[22];
    z[10]=z[5]*z[4]*z[10];
    z[14]= - z[19]*z[31];
    z[14]=n<T>(1,2) + z[14];
    z[14]=z[14]*z[28];
    z[9]=n<T>(1,16)*z[10] + n<T>(1,2)*z[9] + z[12] + z[14] + z[13] + z[11];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r499(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r499(const std::array<dd_real,30>&);
#endif
