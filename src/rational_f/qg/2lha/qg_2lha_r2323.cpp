#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2323(const std::array<T,30>& k) {
  T z[64];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[7];
    z[8]=k[2];
    z[9]=k[11];
    z[10]=k[24];
    z[11]=k[14];
    z[12]=k[9];
    z[13]=k[10];
    z[14]=k[18];
    z[15]=k[21];
    z[16]=k[6];
    z[17]=npow(z[9],3);
    z[18]=5*z[9];
    z[19]= - n<T>(11,2) + z[18];
    z[19]=z[19]*z[17];
    z[20]=z[11]*z[12];
    z[21]=npow(z[12],2);
    z[21]= - n<T>(1,2)*z[21] + z[20];
    z[22]=npow(z[11],2);
    z[21]=z[21]*z[22];
    z[23]=npow(z[9],4);
    z[24]=z[23]*z[5];
    z[25]=5*z[24];
    z[19]=z[21] + z[19] - z[25];
    z[21]=npow(z[9],2);
    z[26]= - static_cast<T>(11)+ z[18];
    z[26]=z[26]*z[21];
    z[27]=z[17]*z[5];
    z[28]=5*z[27];
    z[26]=z[26] - z[28];
    z[29]=z[21]*z[5];
    z[30]= - n<T>(5,3) + z[9];
    z[30]=z[9]*z[30];
    z[30]=z[30] - z[29];
    z[31]=z[5]*z[9];
    z[32]= - z[31] - n<T>(3,2) + z[9];
    z[32]=z[10]*z[32];
    z[30]=n<T>(1,2)*z[30] + z[32];
    z[30]=z[10]*z[30];
    z[26]=n<T>(1,6)*z[26] + z[30];
    z[26]=z[10]*z[26];
    z[30]=n<T>(1,3)*z[5];
    z[32]= - z[21]*z[30];
    z[33]=n<T>(1,3)*z[9];
    z[34]=z[33] - n<T>(1,2);
    z[34]=z[34]*z[9];
    z[22]= - n<T>(1,6)*z[22] + z[34] + z[32];
    z[22]=z[7]*z[22];
    z[32]=static_cast<T>(1)- 7*z[9];
    z[32]=z[32]*z[21];
    z[28]=z[32] + z[28];
    z[32]=npow(z[11],3);
    z[28]=n<T>(1,2)*z[28] - z[32];
    z[22]=n<T>(1,3)*z[28] + z[22];
    z[28]=n<T>(1,2)*z[7];
    z[22]=z[22]*z[28];
    z[35]=z[10]*z[9];
    z[35]=n<T>(1,2)*z[21] + z[35];
    z[35]=z[10]*z[35];
    z[35]=n<T>(5,6)*z[17] + z[35];
    z[35]=z[10]*z[35];
    z[36]=z[7]*z[21];
    z[36]= - n<T>(5,2)*z[17] + z[36];
    z[36]=z[7]*z[36];
    z[37]=n<T>(5,3)*z[23];
    z[35]=n<T>(1,6)*z[36] + z[37] + z[35];
    z[35]=z[8]*z[35];
    z[36]= - z[2]*z[37];
    z[19]=z[35] + z[36] + z[22] + z[26] + n<T>(1,3)*z[19];
    z[19]=z[8]*z[19];
    z[22]=z[9]*z[4];
    z[26]=n<T>(1,2)*z[4];
    z[35]=z[26] - 1;
    z[36]=z[22]*z[35];
    z[38]=z[5]*z[36];
    z[39]=n<T>(1,2)*z[9];
    z[40]=z[39]*z[5];
    z[41]=z[9] + z[40];
    z[41]=z[5]*z[41];
    z[41]=z[39] + z[41];
    z[41]=z[12]*z[41];
    z[42]=z[22] - z[4];
    z[43]=npow(z[4],2);
    z[44]=z[43]*z[2];
    z[45]=n<T>(1,2)*z[44];
    z[38]=z[45] + z[41] - n<T>(1,2)*z[42] + z[38];
    z[38]=z[15]*z[38];
    z[41]= - n<T>(1,6) + z[9];
    z[41]=z[41]*z[39];
    z[46]=n<T>(1,2)*z[5];
    z[47]=z[46]*z[21];
    z[48]= - n<T>(1,12) + z[9];
    z[48]=z[9]*z[48];
    z[48]=z[48] + z[47];
    z[48]=z[5]*z[48];
    z[41]=z[41] + z[48];
    z[41]=z[12]*z[41];
    z[48]=n<T>(1,6) + z[4];
    z[48]=z[4]*z[48];
    z[48]=z[48] - z[22];
    z[48]=z[9]*z[48];
    z[48]= - n<T>(13,6)*z[43] + z[48];
    z[49]=z[4] - 1;
    z[50]=z[49]*z[4];
    z[36]= - n<T>(7,12)*z[50] + z[36];
    z[36]=z[36]*z[31];
    z[51]=z[5]*z[50]*z[7];
    z[36]=z[38] - n<T>(7,12)*z[51] + z[41] + n<T>(1,2)*z[48] + z[36];
    z[36]=z[15]*z[36];
    z[38]= - n<T>(1,2) - z[42];
    z[38]=z[9]*z[38];
    z[38]=z[38] - z[29];
    z[41]=n<T>(1,4) + z[9];
    z[41]=z[9]*z[41];
    z[41]=z[41] + z[47];
    z[41]=z[5]*z[41];
    z[42]=n<T>(1,2) + z[9];
    z[42]=z[9]*z[42];
    z[42]= - n<T>(1,2) + z[42];
    z[41]=n<T>(1,2)*z[42] + z[41];
    z[41]=z[12]*z[41];
    z[38]=n<T>(1,2)*z[38] + z[41];
    z[38]=z[16]*z[38];
    z[19]=z[38] + z[19] + z[36];
    z[36]=n<T>(1,4)*z[4];
    z[38]=n<T>(13,3) - z[36];
    z[38]=z[38]*z[33];
    z[41]=n<T>(1,3)*z[4];
    z[42]=z[41] - 1;
    z[47]=z[4]*z[42];
    z[47]=n<T>(35,9) + z[47];
    z[38]=n<T>(1,4)*z[47] + z[38];
    z[38]=z[9]*z[38];
    z[47]=z[4] + 1;
    z[48]=z[47]*z[4];
    z[38]=n<T>(1,24)*z[48] + z[38];
    z[38]=z[9]*z[38];
    z[52]=npow(z[10],2);
    z[53]=z[5] + 1;
    z[54]=z[53]*z[10];
    z[55]= - n<T>(1,2) - z[54];
    z[55]=z[55]*z[52];
    z[56]=n<T>(1,18)*z[43];
    z[38]=n<T>(1,6)*z[55] + n<T>(47,24)*z[27] + z[56] + z[38];
    z[55]=z[53]*z[52];
    z[55]=n<T>(1,24)*z[55] + n<T>(1,3)*z[21] + n<T>(1,16)*z[29];
    z[55]=z[12]*z[55];
    z[38]=n<T>(1,2)*z[38] + z[55];
    z[38]=z[12]*z[38];
    z[55]= - n<T>(3,2) - z[18];
    z[55]=z[55]*z[17];
    z[25]=z[55] - z[25];
    z[17]= - n<T>(5,4)*z[17] + z[27];
    z[55]=n<T>(1,3)*z[12];
    z[17]=z[17]*z[55];
    z[57]= - z[12]*z[24];
    z[23]=z[23] + z[57];
    z[23]=z[2]*z[23];
    z[17]=n<T>(5,12)*z[23] + n<T>(1,4)*z[25] + z[17];
    z[17]=z[12]*z[17];
    z[23]=z[43]*npow(z[10],3);
    z[23]=z[37] + n<T>(3,2)*z[23];
    z[25]=n<T>(1,8)*z[43];
    z[32]=z[32]*z[25];
    z[17]=z[32] + n<T>(1,4)*z[23] + z[17];
    z[17]=z[2]*z[17];
    z[23]= - n<T>(1,9) + n<T>(3,2)*z[4];
    z[23]=z[23]*z[26];
    z[32]=z[26] - n<T>(1,3);
    z[37]=z[32]*z[18];
    z[57]=n<T>(5,3)*z[4];
    z[58]= - static_cast<T>(3)- z[57];
    z[58]=z[4]*z[58];
    z[58]=n<T>(1,6) + z[58];
    z[37]=n<T>(1,2)*z[58] + z[37];
    z[37]=z[9]*z[37];
    z[23]=z[23] + z[37];
    z[23]=z[9]*z[23];
    z[23]=z[56] + z[23];
    z[23]=z[9]*z[23];
    z[37]=z[43]*z[5];
    z[58]=n<T>(1,4)*z[37];
    z[59]= - static_cast<T>(1)- n<T>(3,4)*z[4];
    z[59]=z[4]*z[59];
    z[59]= - z[58] + n<T>(1,4) + z[59];
    z[59]=z[10]*z[59];
    z[59]=n<T>(9,8)*z[43] + z[59];
    z[59]=z[59]*z[52];
    z[23]=z[59] + z[23] - n<T>(5,6)*z[24];
    z[24]=n<T>(1,2) - z[54];
    z[24]=z[24]*z[52];
    z[52]=n<T>(1,2)*z[10];
    z[54]= - static_cast<T>(1)+ z[54];
    z[54]=z[6]*z[54]*z[52];
    z[24]=z[24] + z[54];
    z[24]=z[6]*z[24];
    z[54]=static_cast<T>(3)+ z[4];
    z[54]=z[54]*z[26];
    z[54]=z[54] + z[37];
    z[54]=z[11]*z[54];
    z[54]= - n<T>(3,4)*z[43] + z[54];
    z[54]=z[11]*z[54];
    z[54]= - z[43] + z[54];
    z[54]=z[11]*z[54];
    z[17]=z[17] + n<T>(1,24)*z[24] + n<T>(1,4)*z[54] + n<T>(1,2)*z[23] + z[38];
    z[17]=z[2]*z[17];
    z[23]=z[46]*z[43];
    z[24]=z[26] + 1;
    z[38]= - z[4]*z[24];
    z[38]= - z[23] - static_cast<T>(1)+ z[38];
    z[38]=z[5]*z[38];
    z[54]=z[4] + n<T>(1,2);
    z[53]=z[12]*z[53];
    z[38]=n<T>(1,2)*z[53] + z[38] - z[54];
    z[38]=z[38]*z[55];
    z[53]= - n<T>(11,3) - z[4];
    z[53]=z[53]*z[26];
    z[53]=z[53] - z[37];
    z[38]=n<T>(1,2)*z[53] + z[38];
    z[53]=z[49]*z[5];
    z[55]=z[53]*z[41];
    z[55]=z[55] + n<T>(1,3)*z[43];
    z[59]= - static_cast<T>(1)+ z[55];
    z[59]=z[5]*z[59];
    z[59]=z[59] + z[42];
    z[60]=n<T>(1,4)*z[12];
    z[59]=z[59]*z[60];
    z[61]=z[4] + z[58];
    z[61]=z[5]*z[61];
    z[59]=z[59] + n<T>(1,2)*z[47] + z[61];
    z[59]=z[11]*z[59];
    z[38]=n<T>(1,2)*z[38] + z[59];
    z[38]=z[11]*z[38];
    z[59]=static_cast<T>(7)- n<T>(5,2)*z[9];
    z[59]=z[9]*z[59];
    z[59]= - n<T>(13,2) + z[59];
    z[59]=z[40]*z[43]*z[59];
    z[61]=n<T>(1,3) - z[4];
    z[61]=z[4]*z[61];
    z[22]=z[61] - n<T>(5,2)*z[22];
    z[22]=z[9]*z[22];
    z[61]=n<T>(5,3) + n<T>(9,2)*z[4];
    z[61]=z[4]*z[61];
    z[22]=z[61] + z[22];
    z[22]=z[22]*z[39];
    z[61]= - static_cast<T>(1)- n<T>(31,12)*z[4];
    z[61]=z[4]*z[61];
    z[22]=z[59] + z[61] + z[22];
    z[59]=z[35]*z[4];
    z[61]=n<T>(1,2) + n<T>(7,3)*z[59];
    z[61]=z[61]*z[39];
    z[62]=static_cast<T>(1)- n<T>(17,12)*z[50];
    z[61]=n<T>(1,3)*z[62] + z[61];
    z[62]=static_cast<T>(1)- z[30];
    z[60]=z[62]*z[60];
    z[62]=z[9]*z[30];
    z[60]=z[60] + n<T>(1,2)*z[61] + z[62];
    z[60]=z[12]*z[60];
    z[22]=z[38] + n<T>(1,2)*z[22] + z[60];
    z[38]=n<T>(1,2)*z[11];
    z[22]=z[22]*z[38];
    z[60]=z[4] - n<T>(5,2);
    z[61]=n<T>(1,6)*z[4];
    z[62]= - z[60]*z[61];
    z[62]=static_cast<T>(1)+ z[62];
    z[61]= - static_cast<T>(1)+ z[61];
    z[61]=z[4]*z[61];
    z[61]=n<T>(13,6) + z[61];
    z[61]=z[61]*z[39];
    z[61]=n<T>(1,3)*z[62] + z[61];
    z[62]=z[58] - static_cast<T>(1)+ z[43];
    z[62]=z[62]*z[30];
    z[55]=n<T>(1,2) - z[55];
    z[55]=z[5]*z[55];
    z[63]=z[41] - n<T>(1,2);
    z[55]=z[55] - z[63];
    z[55]=z[11]*z[55];
    z[32]=z[55] + z[62] + z[32];
    z[32]=z[32]*z[38];
    z[55]=n<T>(1,4)*z[43];
    z[62]= - z[55] + z[9];
    z[62]=z[62]*z[30];
    z[32]=z[32] + n<T>(1,2)*z[61] + z[62];
    z[32]=z[11]*z[32];
    z[61]=n<T>(1,2)*z[43];
    z[62]=z[61] - 1;
    z[46]=z[62]*z[46];
    z[53]= - z[26]*z[53];
    z[53]=z[53] - z[62];
    z[53]=z[5]*z[53];
    z[53]=z[53] - z[35];
    z[38]=z[53]*z[38];
    z[38]=z[38] + z[46] + z[35];
    z[38]=z[11]*z[38];
    z[34]= - z[49]*z[34];
    z[34]=z[34] - z[63];
    z[42]=z[42]*z[26];
    z[42]=z[42] + n<T>(1,3);
    z[46]= - z[9]*z[42];
    z[53]=n<T>(1,2) + z[59];
    z[46]=n<T>(1,6)*z[53] + z[46];
    z[46]=z[46]*z[31];
    z[34]=n<T>(1,3)*z[38] + n<T>(1,2)*z[34] + z[46];
    z[34]=z[34]*z[28];
    z[36]=z[36] + 1;
    z[38]= - z[36]*z[41];
    z[46]=z[4] - n<T>(11,2);
    z[53]= - z[46]*z[41];
    z[53]= - n<T>(3,2) + z[53];
    z[53]=z[9]*z[53];
    z[53]=n<T>(1,6)*z[48] + z[53];
    z[53]=z[53]*z[39];
    z[38]=z[53] - n<T>(1,2) + z[38];
    z[38]=z[9]*z[38];
    z[42]=z[42]*z[18];
    z[53]=z[42] - n<T>(5,6) - z[59];
    z[53]=z[9]*z[53];
    z[53]= - n<T>(1,6)*z[50] + z[53];
    z[40]=z[53]*z[40];
    z[53]=n<T>(1,6)*z[43];
    z[38]=z[40] + z[53] + z[38];
    z[32]=z[34] + n<T>(1,2)*z[38] + z[32];
    z[32]=z[32]*z[28];
    z[34]=z[41] + 1;
    z[38]=z[34]*z[26];
    z[38]= - z[39] + n<T>(5,3) + z[38];
    z[38]=z[38]*z[39];
    z[36]= - z[4]*z[36];
    z[36]= - n<T>(7,4) + z[36];
    z[36]= - n<T>(1,12)*z[37] + n<T>(1,3)*z[36] + z[38];
    z[23]=z[23] + z[48];
    z[23]=z[23]*z[30];
    z[34]=z[34]*z[4];
    z[34]=z[34] + n<T>(1,3);
    z[23]=z[23] + n<T>(1,2)*z[34];
    z[23]=z[23]*z[5];
    z[23]=z[23] + n<T>(1,6)*z[47];
    z[23]=z[23]*z[10];
    z[34]=static_cast<T>(1)+ n<T>(5,6)*z[4];
    z[34]=z[4]*z[34];
    z[34]=n<T>(5,6)*z[37] + n<T>(1,2) + z[34];
    z[34]=z[5]*z[34];
    z[34]=z[34] + n<T>(1,3) + z[26];
    z[34]=n<T>(1,4)*z[34] - z[23];
    z[34]=z[10]*z[34];
    z[34]=n<T>(1,2)*z[36] + z[34];
    z[34]=z[34]*z[52];
    z[36]= - z[37] + n<T>(1,2) - z[48];
    z[36]=z[5]*z[36];
    z[35]=z[36] - z[35];
    z[35]=n<T>(1,3)*z[35] + z[23];
    z[36]=n<T>(1,4)*z[10];
    z[35]=z[35]*z[36];
    z[38]=n<T>(5,2)*z[4];
    z[40]= - z[9]*z[41];
    z[40]=z[38] + z[40];
    z[40]=z[9]*z[40];
    z[40]= - n<T>(11,2)*z[4] + z[40];
    z[40]=z[40]*z[39];
    z[40]=z[40] - n<T>(1,4) + z[57];
    z[47]= - z[9]*z[55];
    z[47]=z[43] + z[47];
    z[47]=z[47]*z[33];
    z[47]= - z[61] + z[47];
    z[47]=z[47]*z[33];
    z[25]=z[25] + z[47];
    z[25]=z[5]*z[25];
    z[25]=z[35] + n<T>(1,6)*z[40] + z[25];
    z[25]=z[6]*z[25];
    z[35]=z[54]*z[41];
    z[40]=z[43]*z[33];
    z[47]= - static_cast<T>(1)- n<T>(35,18)*z[4];
    z[47]=z[4]*z[47];
    z[40]=z[47] + z[40];
    z[40]=z[9]*z[40];
    z[35]=z[35] + n<T>(1,8)*z[40];
    z[35]=z[9]*z[35];
    z[40]= - static_cast<T>(1)+ z[33];
    z[21]=z[21]*z[40];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[5]*z[43]*z[21];
    z[40]=static_cast<T>(1)- n<T>(19,3)*z[4];
    z[40]=z[4]*z[40];
    z[21]=n<T>(1,2)*z[25] + z[34] + n<T>(1,24)*z[21] + n<T>(1,48)*z[40] + z[35];
    z[21]=z[6]*z[21];
    z[25]= - z[49]*z[39];
    z[25]= - n<T>(1,3)*z[24] + z[25];
    z[25]=z[9]*z[25];
    z[34]=z[4] - 3;
    z[34]=z[34]*z[26];
    z[34]=z[34] + 1;
    z[34]=z[34]*z[9];
    z[35]= - z[34] + static_cast<T>(1)+ z[59];
    z[35]=z[9]*z[35];
    z[35]= - z[61] + z[35];
    z[35]=z[5]*z[35];
    z[39]= - static_cast<T>(7)- 3*z[4];
    z[39]=z[4]*z[39];
    z[39]=static_cast<T>(3)+ z[39];
    z[25]=z[35] + n<T>(1,4)*z[39] + z[25];
    z[35]= - n<T>(3,4) + z[4];
    z[35]=z[4]*z[35];
    z[34]=z[58] + z[35] - z[34];
    z[34]=z[5]*z[34];
    z[35]=z[49]*z[9];
    z[39]= - z[35] - static_cast<T>(3)+ 5*z[4];
    z[34]=n<T>(1,2)*z[39] + z[34];
    z[34]=z[10]*z[34];
    z[25]=n<T>(1,2)*z[25] + z[34];
    z[25]=z[25]*z[52];
    z[34]=n<T>(1,8)*z[4];
    z[39]= - static_cast<T>(3)- z[41];
    z[39]=z[39]*z[34];
    z[40]=static_cast<T>(17)- z[4];
    z[40]=z[40]*z[26];
    z[35]= - n<T>(5,2)*z[35] - static_cast<T>(7)+ z[40];
    z[35]=z[9]*z[35];
    z[35]=n<T>(1,12)*z[35] + n<T>(1,3) + z[39];
    z[35]=z[9]*z[35];
    z[39]= - z[42] + n<T>(1,2) + n<T>(5,3)*z[59];
    z[29]=z[39]*z[29];
    z[25]=z[25] + n<T>(1,4)*z[29] + n<T>(7,24)*z[43] + z[35];
    z[25]=z[10]*z[25];
    z[24]= - z[24]*z[41];
    z[24]= - n<T>(1,6)*z[37] + n<T>(1,2) + z[24];
    z[24]=z[5]*z[24];
    z[23]= - z[23] - n<T>(1,3)*z[60] + z[24];
    z[23]=z[23]*z[36];
    z[24]=n<T>(11,2) - z[43];
    z[29]= - static_cast<T>(2)+ n<T>(1,16)*z[50];
    z[29]=n<T>(1,3)*z[29] + n<T>(1,16)*z[9];
    z[29]=z[9]*z[29];
    z[35]= - z[53] - 3*z[9];
    z[35]=z[5]*z[35];
    z[23]=z[23] + n<T>(1,8)*z[35] + n<T>(1,24)*z[24] + z[29];
    z[23]=z[10]*z[23];
    z[24]= - n<T>(65,3) - n<T>(7,2)*z[43];
    z[29]=static_cast<T>(1)- z[34];
    z[29]=z[4]*z[29];
    z[29]= - n<T>(1,6)*z[9] + n<T>(7,4) + z[29];
    z[29]=z[9]*z[29];
    z[24]=n<T>(1,8)*z[24] + z[29];
    z[24]=z[9]*z[24];
    z[29]=static_cast<T>(1)+ z[38];
    z[29]=z[4]*z[29];
    z[24]= - n<T>(1,8)*z[27] + n<T>(1,12)*z[29] + z[24];
    z[27]= - n<T>(1,4) - z[30];
    z[27]=z[27]*z[52];
    z[29]=n<T>(1,16) + z[30];
    z[29]=z[9]*z[29];
    z[27]=z[27] + z[29];
    z[27]=z[12]*z[27];
    z[23]=z[27] + n<T>(1,3)*z[24] + z[23];
    z[23]=z[12]*z[23];
    z[24]= - static_cast<T>(23)+ 29*z[4];
    z[24]=z[4]*z[24];
    z[24]=z[24] - z[37];
    z[27]=z[44]*z[3];
    z[24]=n<T>(11,3)*z[27] - n<T>(1,3)*z[44] + n<T>(1,6)*z[24] + z[51];
    z[24]=z[13]*z[24];
    z[29]=n<T>(1,9)*z[4];
    z[30]= - n<T>(149,2) + 7*z[4];
    z[30]=z[30]*z[29];
    z[30]=static_cast<T>(5)+ z[30];
    z[18]= - z[49]*z[18];
    z[34]=n<T>(25,3) + z[4];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(29,3) + z[34];
    z[18]=n<T>(1,2)*z[34] + z[18];
    z[18]=z[18]*z[33];
    z[18]=n<T>(1,2)*z[30] + z[18];
    z[18]=z[9]*z[18];
    z[30]=n<T>(73,4) + 11*z[4];
    z[29]=z[30]*z[29];
    z[18]=z[29] + z[18];
    z[18]=z[9]*z[18];
    z[18]= - z[53] + z[18];
    z[29]=n<T>(11,4) + 3*z[59];
    z[29]=n<T>(1,2)*z[29] - z[42];
    z[29]=z[9]*z[29];
    z[29]=n<T>(1,18)*z[50] + z[29];
    z[29]=z[9]*z[29];
    z[29]=z[56] + z[29];
    z[29]=z[29]*z[31];
    z[18]=n<T>(1,2)*z[18] + z[29];
    z[29]=z[11] - 1;
    z[29]=z[29]*z[7];
    z[20]=z[29] + z[20];
    z[29]=static_cast<T>(1)- z[20];
    z[30]= - z[59] + z[45];
    z[30]=z[2]*z[30];
    z[29]=n<T>(1,2)*z[29] + z[30];
    z[29]=z[3]*z[29];
    z[30]= - z[4]*z[46];
    z[20]=z[30] - z[20];
    z[20]=n<T>(1,9)*z[20] + z[45];
    z[20]=n<T>(1,2)*z[20] + n<T>(1,9)*z[29];
    z[20]=z[3]*z[20];
    z[26]= - z[26] - n<T>(1,9)*z[37];
    z[26]=z[6]*z[26];
    z[26]=n<T>(1,9)*z[43] + z[26];
    z[26]=z[6]*z[26];
    z[28]= - z[45] - z[4] - z[28];
    z[28]=z[28]*npow(z[3],2);
    z[26]=n<T>(1,8)*z[26] + n<T>(1,9)*z[28];
    z[26]=z[1]*z[26];
    z[27]= - z[27] - z[50] + z[44];
    z[27]=z[14]*z[27];

    r += z[17] + n<T>(1,2)*z[18] + n<T>(1,4)*z[19] + z[20] + z[21] + z[22] + 
      z[23] + n<T>(1,6)*z[24] + z[25] + z[26] + n<T>(5,12)*z[27] + z[32];
 
    return r;
}

template double qg_2lha_r2323(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2323(const std::array<dd_real,30>&);
#endif
