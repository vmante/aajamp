#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r748(const std::array<T,30>& k) {
  T z[10];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=z[3] - 1;
    z[6]=z[1] - 1;
    z[6]=z[6]*z[3];
    z[7]= - static_cast<T>(1)- n<T>(3,4)*z[6];
    z[7]=z[4]*z[7];
    z[7]= - n<T>(1,2)*z[5] + z[7];
    z[7]=z[4]*z[7];
    z[8]= - static_cast<T>(5)- 3*z[6];
    z[8]=z[4]*z[8];
    z[8]=z[8] - z[5];
    z[8]=z[4]*z[8];
    z[9]= - static_cast<T>(1)- n<T>(1,2)*z[6];
    z[9]=z[2]*z[9]*npow(z[4],2);
    z[8]=n<T>(1,2)*z[8] + z[9];
    z[9]=n<T>(1,2)*z[2];
    z[8]=z[8]*z[9];
    z[7]=z[7] + z[8];
    z[7]=z[2]*z[7];
    z[6]= - static_cast<T>(1)- z[6];
    z[6]=z[4]*z[6];
    z[5]=z[6] - z[5];
    z[5]=z[4]*z[5];
    z[5]=n<T>(1,4)*z[5] + z[7];

    r += z[9]*z[5];
 
    return r;
}

template double qg_2lha_r748(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r748(const std::array<dd_real,30>&);
#endif
