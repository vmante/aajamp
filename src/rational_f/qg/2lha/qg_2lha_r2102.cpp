#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2102(const std::array<T,30>& k) {
  T z[68];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[9];
    z[5]=k[17];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=k[5];
    z[9]=k[20];
    z[10]=k[15];
    z[11]=k[8];
    z[12]=k[12];
    z[13]=k[10];
    z[14]=n<T>(1,3)*z[6];
    z[15]=z[14] + 1;
    z[16]=npow(z[6],2);
    z[17]=z[16]*z[2];
    z[18]=z[15]*z[17];
    z[19]=n<T>(1,3)*z[16];
    z[20]=z[6] - 1;
    z[21]= - z[20]*z[19];
    z[22]=npow(z[6],3);
    z[23]=z[22]*z[2];
    z[24]=2*z[22] - z[23];
    z[25]=n<T>(1,3)*z[8];
    z[24]=z[24]*z[25];
    z[21]=z[24] + z[21] + z[18];
    z[21]=z[8]*z[21];
    z[24]=z[6]*z[20];
    z[24]= - static_cast<T>(5)+ z[24];
    z[24]=z[24]*z[14];
    z[15]=z[15]*z[6];
    z[15]=z[15] + 1;
    z[15]=z[15]*z[6];
    z[15]=z[15] + n<T>(1,3);
    z[26]=z[15]*z[2];
    z[24]= - z[26] - static_cast<T>(1)+ z[24];
    z[24]=z[4]*z[24];
    z[21]=z[24] + z[21];
    z[24]=2*z[6];
    z[27]=static_cast<T>(7)- z[24];
    z[27]=z[27]*z[16]*z[25];
    z[28]= - n<T>(7,6) - z[6];
    z[28]=z[6]*z[28];
    z[27]=z[28] + z[27];
    z[27]=z[8]*z[27];
    z[28]=z[4]*z[6];
    z[29]=z[6] + 1;
    z[30]=z[28]*z[29];
    z[31]=13*z[6];
    z[32]=npow(z[8],2);
    z[33]=z[32]*z[7];
    z[34]= - z[33]*z[31];
    z[35]=z[16]*z[8];
    z[36]=7*z[6] - 17*z[35];
    z[36]=z[8]*z[36];
    z[34]=z[36] + z[34];
    z[34]=z[7]*z[34];
    z[27]=n<T>(1,6)*z[34] + z[27] + n<T>(3,2) + z[30];
    z[27]=z[7]*z[27];
    z[34]=z[6] - 2;
    z[36]=z[34]*z[35];
    z[36]=z[6] + z[36];
    z[36]=z[8]*z[36];
    z[37]= - z[6] + 2*z[35];
    z[37]=z[37]*z[8];
    z[38]=z[6]*z[33];
    z[38]=z[37] + z[38];
    z[38]=z[7]*z[38];
    z[36]=z[36] + z[38];
    z[36]=z[7]*z[36];
    z[38]=z[6] + 2;
    z[39]=z[38]*z[6];
    z[39]=z[39] + 1;
    z[40]= - z[39]*z[28];
    z[38]=z[38]*z[16];
    z[41]=z[22]*z[8];
    z[38]=z[38] - 2*z[41];
    z[38]=z[8]*z[38];
    z[36]=z[36] + z[38] - static_cast<T>(1)+ z[40];
    z[36]=z[7]*z[36];
    z[38]=z[6] + 3;
    z[40]= - z[38]*z[16];
    z[40]=z[40] + z[41];
    z[40]=z[8]*z[40];
    z[38]=z[38]*z[6];
    z[38]=z[38] + 3;
    z[38]=z[38]*z[6];
    z[38]=z[38] + 1;
    z[42]=z[4]*z[38];
    z[36]=z[36] + z[40] - static_cast<T>(1)+ z[42];
    z[36]=z[9]*z[36];
    z[40]=2*z[2];
    z[42]=n<T>(19,2) + z[40];
    z[21]=z[36] + z[27] + n<T>(1,3)*z[42] + 2*z[21];
    z[21]=z[9]*z[21];
    z[27]=4*z[17];
    z[36]=n<T>(1,2) + z[24];
    z[36]=z[6]*z[36];
    z[36]= - z[35] + z[36] - z[27];
    z[36]=z[36]*z[25];
    z[42]=z[24] - 1;
    z[43]= - z[42]*z[14];
    z[44]=z[39]*z[2];
    z[43]=n<T>(4,3)*z[44] + static_cast<T>(1)+ z[43];
    z[43]=z[4]*z[43];
    z[45]=n<T>(4,3)*z[6];
    z[46]=z[33]*z[45];
    z[37]=z[46] - n<T>(1,2) + n<T>(2,3)*z[37];
    z[37]=z[7]*z[37];
    z[46]= - static_cast<T>(2)- z[2];
    z[21]=z[21] + z[37] + z[36] + n<T>(4,3)*z[46] + z[43];
    z[21]=z[9]*z[21];
    z[36]=n<T>(1,2)*z[6];
    z[37]=n<T>(5,3)*z[6];
    z[43]= - static_cast<T>(1)+ z[37];
    z[43]=z[43]*z[36];
    z[46]= - n<T>(1,2) + z[14];
    z[46]=z[46]*z[17];
    z[43]=z[43] + z[46];
    z[43]=z[2]*z[43];
    z[43]=n<T>(1,4) + z[43];
    z[43]=z[2]*z[43];
    z[46]=z[14]*z[2];
    z[47]=z[20]*z[46];
    z[48]= - n<T>(1,2) + z[37];
    z[47]=n<T>(1,2)*z[48] + z[47];
    z[48]=npow(z[2],2);
    z[47]=z[47]*z[48];
    z[49]= - n<T>(7,4) + z[2];
    z[49]=z[49]*z[48];
    z[50]=npow(z[2],3);
    z[51]=z[1]*z[50];
    z[49]=z[49] - n<T>(1,2)*z[51];
    z[49]=z[1]*z[49];
    z[51]=z[6] - n<T>(1,2);
    z[52]=z[2]*z[51];
    z[52]=n<T>(5,2) + z[52];
    z[52]=z[2]*z[52];
    z[52]= - n<T>(5,4) + z[52];
    z[52]=z[2]*z[52];
    z[49]=z[52] + z[49];
    z[52]=n<T>(1,3)*z[1];
    z[49]=z[49]*z[52];
    z[47]=z[47] + z[49];
    z[47]=z[1]*z[47];
    z[43]=z[43] + z[47];
    z[43]=z[1]*z[43];
    z[47]=n<T>(1,3)*z[2];
    z[49]=z[34]*z[22]*z[47];
    z[53]=n<T>(1,2)*z[16];
    z[37]= - n<T>(3,2) + z[37];
    z[37]=z[37]*z[53];
    z[37]=z[37] + z[49];
    z[37]=z[2]*z[37];
    z[37]=z[36] + z[37];
    z[37]=z[2]*z[37];
    z[49]=n<T>(5,6)*z[6];
    z[54]=static_cast<T>(1)- z[49];
    z[54]=z[54]*z[22];
    z[55]=npow(z[6],4);
    z[56]=z[47]*z[55];
    z[57]=n<T>(5,2) - z[6];
    z[57]=z[57]*z[56];
    z[54]=z[54] + z[57];
    z[54]=z[2]*z[54];
    z[57]=npow(z[6],5);
    z[58]=z[57]*z[47];
    z[58]=z[58] + n<T>(1,2)*z[55];
    z[58]=z[58]*z[2];
    z[59]=n<T>(1,2)*z[22];
    z[58]=z[58] - z[59];
    z[60]=n<T>(1,2)*z[8];
    z[61]= - z[58]*z[60];
    z[54]=z[61] - n<T>(3,4)*z[16] + z[54];
    z[61]=z[8]*z[2];
    z[54]=z[54]*z[61];
    z[62]= - z[41] + 3*z[16];
    z[62]=z[62]*z[60];
    z[63]=n<T>(1,2)*z[7];
    z[62]=z[62] + z[63] - z[6];
    z[62]=z[62]*z[9];
    z[37]=n<T>(1,2)*z[62] + z[43] + z[54] - n<T>(1,4) + z[37];
    z[37]=z[3]*z[37];
    z[43]=n<T>(5,2)*z[16];
    z[54]=z[43] - z[41];
    z[54]=z[54]*z[8];
    z[64]=n<T>(3,2)*z[6];
    z[54]= - z[54] - z[63] + z[62] + z[64];
    z[62]=n<T>(1,2)*z[9];
    z[62]=z[54]*z[62];
    z[63]=z[55]*z[2];
    z[65]=n<T>(1,3)*z[22] - z[63];
    z[65]=z[2]*z[65];
    z[43]=z[43] + z[65];
    z[65]=n<T>(1,2)*z[2];
    z[43]=z[43]*z[65];
    z[56]= - z[59] - z[56];
    z[56]=z[56]*z[61];
    z[43]=z[43] + z[56];
    z[43]=z[8]*z[43];
    z[56]=static_cast<T>(1)- n<T>(5,6)*z[2];
    z[56]=z[56]*z[48];
    z[59]=z[50]*z[52];
    z[56]=z[56] + z[59];
    z[56]=z[1]*z[56];
    z[20]= - z[20]*z[65];
    z[20]= - n<T>(5,3) + z[20];
    z[20]=z[2]*z[20];
    z[20]=n<T>(2,3) + z[20];
    z[20]=z[2]*z[20];
    z[20]=z[20] + z[56];
    z[20]=z[1]*z[20];
    z[56]=static_cast<T>(2)- z[36];
    z[46]=z[56]*z[46];
    z[56]=static_cast<T>(1)- n<T>(7,3)*z[6];
    z[46]=n<T>(1,2)*z[56] + z[46];
    z[46]=z[2]*z[46];
    z[46]= - n<T>(1,4) + z[46];
    z[46]=z[2]*z[46];
    z[20]=z[46] + z[20];
    z[20]=z[1]*z[20];
    z[46]=n<T>(2,3)*z[6];
    z[56]=n<T>(1,2) - z[46];
    z[56]=z[6]*z[56];
    z[18]=z[56] + n<T>(1,2)*z[18];
    z[18]=z[2]*z[18];
    z[56]=3*z[6];
    z[59]= - static_cast<T>(1)- z[56];
    z[18]=n<T>(1,4)*z[59] + z[18];
    z[18]=z[2]*z[18];
    z[18]=z[37] + z[62] + z[20] + z[43] + n<T>(1,4) + z[18];
    z[18]=z[3]*z[18];
    z[20]=z[57]*z[2];
    z[37]=n<T>(5,2)*z[55] + z[20];
    z[37]=z[37]*z[48];
    z[20]=n<T>(7,2)*z[55] + z[20];
    z[20]=z[2]*z[20];
    z[43]=n<T>(5,2)*z[22];
    z[20]=z[43] + z[20];
    z[20]=z[2]*z[20];
    z[57]=5*z[16];
    z[20]= - z[57] + z[20];
    z[20]=z[20]*z[60];
    z[20]=z[20] - z[57] + z[37];
    z[20]=z[20]*z[25];
    z[37]=z[2]*z[58];
    z[37]= - n<T>(7,6)*z[16] + z[37];
    z[57]= - static_cast<T>(1)- n<T>(13,6)*z[8];
    z[57]=z[7]*z[57]*z[60];
    z[58]=z[8]*z[6];
    z[59]= - z[6] - z[58];
    z[59]=z[8]*z[59];
    z[57]=z[57] - n<T>(1,4)*z[6] + n<T>(5,3)*z[59];
    z[57]=z[7]*z[57];
    z[32]=npow(z[7],3)*z[32]*z[10];
    z[20]=n<T>(1,4)*z[32] + z[57] + n<T>(1,2)*z[37] + z[20];
    z[20]=z[12]*z[20];
    z[37]= - n<T>(5,3)*z[22] - z[63];
    z[37]=z[2]*z[37];
    z[37]=z[53] + z[37];
    z[37]=z[2]*z[37];
    z[37]=z[64] + z[37];
    z[43]= - z[43] - z[63];
    z[43]=z[8]*z[43]*z[47];
    z[37]=n<T>(1,2)*z[37] + z[43];
    z[37]=z[8]*z[37];
    z[43]=z[33] - z[60];
    z[53]=npow(z[7],2);
    z[43]=z[43]*z[53];
    z[32]=z[43] - n<T>(1,2)*z[32];
    z[32]=z[32]*z[10];
    z[43]= - z[55]*z[65];
    z[43]= - n<T>(2,3)*z[22] + z[43];
    z[43]=z[2]*z[43];
    z[43]=n<T>(5,12)*z[16] + z[43];
    z[43]=z[2]*z[43];
    z[55]= - n<T>(1,4) + 5*z[58];
    z[55]=z[55]*z[25];
    z[55]=n<T>(5,3)*z[33] + n<T>(1,4) + z[55];
    z[55]=z[7]*z[55];
    z[20]=z[20] - n<T>(1,2)*z[32] + z[55] + z[37] + z[49] + z[43];
    z[20]=z[12]*z[20];
    z[37]= - static_cast<T>(19)- z[6];
    z[37]=z[37]*z[14];
    z[43]=z[6] + 5;
    z[49]=z[43]*z[24];
    z[49]=static_cast<T>(9)+ z[49];
    z[49]=z[2]*z[49];
    z[55]= - z[2] + 1;
    z[57]=z[24] + 5;
    z[57]=z[57]*z[6];
    z[59]=z[57] + 3;
    z[55]=z[4]*z[59]*z[55];
    z[37]=z[55] + z[49] - n<T>(15,2) + z[37];
    z[37]=z[4]*z[48]*z[37];
    z[46]=z[46] + 3;
    z[49]=5*z[6];
    z[55]= - static_cast<T>(9)- z[49];
    z[55]=z[2]*z[55];
    z[55]=2*z[46] + z[55];
    z[55]=z[2]*z[55];
    z[59]=z[6] + n<T>(1,3);
    z[55]=z[55] + z[59];
    z[55]=z[2]*z[55];
    z[37]=z[55] + z[37];
    z[37]=z[4]*z[37];
    z[55]=4*z[6];
    z[61]= - static_cast<T>(7)- z[55];
    z[61]=z[2]*z[61];
    z[61]= - z[45] + z[61];
    z[61]=z[2]*z[61];
    z[61]= - z[45] + z[61];
    z[61]=z[2]*z[61];
    z[62]=n<T>(7,3) + z[24];
    z[50]=z[4]*z[62]*z[50];
    z[50]=z[61] + z[50];
    z[50]=z[4]*z[50];
    z[61]=z[24] + 7;
    z[62]=z[2]*z[61];
    z[62]=z[45] + z[62];
    z[62]=z[2]*z[62];
    z[62]=n<T>(1,3)*z[42] + z[62];
    z[62]=z[2]*z[62];
    z[50]=z[62] + z[50];
    z[50]=z[4]*z[50];
    z[62]= - n<T>(2,3) - z[2];
    z[62]=z[2]*z[62];
    z[62]= - n<T>(1,3) + z[62];
    z[62]=z[62]*z[40];
    z[63]=n<T>(2,3)*z[4];
    z[66]= - static_cast<T>(1)- z[2];
    z[66]=z[2]*z[66];
    z[66]= - static_cast<T>(1)+ z[66];
    z[66]=z[66]*z[63];
    z[67]=n<T>(5,3) + z[40];
    z[67]=z[2]*z[67];
    z[66]=z[66] + n<T>(4,3) + z[67];
    z[67]=z[4]*z[2];
    z[66]=z[66]*z[67];
    z[62]=z[62] + z[66];
    z[62]=z[4]*z[62];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[40]*z[48];
    z[40]=n<T>(1,3)*z[40] + z[62];
    z[40]=z[1]*z[40];
    z[48]=n<T>(1,3) - n<T>(5,2)*z[48];
    z[48]=z[2]*z[48];
    z[40]=z[40] + z[48] + z[50];
    z[40]=z[1]*z[40];
    z[48]= - n<T>(23,6) + 7*z[2];
    z[48]=z[48]*z[65];
    z[48]= - n<T>(1,3) + z[48];
    z[48]=z[2]*z[48];
    z[37]=z[40] + z[48] + z[37];
    z[37]=z[1]*z[37];
    z[40]=static_cast<T>(38)+ z[31];
    z[40]=z[6]*z[40];
    z[40]=static_cast<T>(25)+ z[40];
    z[48]= - static_cast<T>(8)- z[56];
    z[48]=z[6]*z[48];
    z[48]= - static_cast<T>(5)+ z[48];
    z[48]=z[2]*z[48];
    z[40]=n<T>(1,3)*z[40] + z[48];
    z[40]=z[2]*z[40];
    z[48]= - static_cast<T>(2)+ z[2];
    z[48]=z[2]*z[48];
    z[48]=z[48] + 1;
    z[46]=z[46]*z[6];
    z[50]=z[46] + 4;
    z[50]=z[50]*z[6];
    z[50]=z[50] + n<T>(5,3);
    z[48]=z[4]*z[50]*z[48];
    z[50]= - z[6]*z[61];
    z[50]= - static_cast<T>(5)+ z[50];
    z[40]=z[48] + n<T>(2,3)*z[50] + z[40];
    z[40]=z[40]*z[67];
    z[31]=n<T>(31,2) + z[31];
    z[31]=z[31]*z[47];
    z[31]=z[31] - n<T>(41,6) - z[49];
    z[31]=z[2]*z[31];
    z[48]=n<T>(11,2) + z[24];
    z[31]=n<T>(1,3)*z[48] + z[31];
    z[31]=z[2]*z[31];
    z[31]=z[31] + z[40];
    z[31]=z[4]*z[31];
    z[40]= - static_cast<T>(13)+ z[16];
    z[40]=z[40]*z[47];
    z[40]=z[40] + static_cast<T>(5)+ z[14];
    z[40]=z[2]*z[40];
    z[40]= - n<T>(5,6) + z[40];
    z[40]=z[40]*z[65];
    z[31]=z[37] + z[40] + z[31];
    z[31]=z[1]*z[31];
    z[37]=n<T>(5,2)*z[6];
    z[40]=static_cast<T>(2)- z[37];
    z[40]=z[6]*z[40];
    z[40]=n<T>(13,2) + z[40];
    z[40]=z[40]*z[25];
    z[40]=n<T>(1,2)*z[29] + z[40];
    z[40]=z[8]*z[40];
    z[47]=static_cast<T>(1)- z[64];
    z[47]=z[47]*z[33];
    z[40]=z[40] + z[47];
    z[40]=z[7]*z[40];
    z[47]=z[24] - 5;
    z[48]=z[47]*z[14];
    z[48]=n<T>(7,2) + z[48];
    z[48]=z[48]*z[58];
    z[49]=n<T>(23,6) - z[6];
    z[49]=z[6]*z[49];
    z[48]=z[49] + z[48];
    z[48]=z[8]*z[48];
    z[30]=z[40] + z[30] + z[48];
    z[30]=z[7]*z[30];
    z[40]=z[29]*z[16];
    z[48]= - static_cast<T>(3)+ z[24];
    z[48]=z[48]*z[35];
    z[48]= - z[40] + z[48];
    z[48]=z[8]*z[48];
    z[49]=static_cast<T>(4)- z[6];
    z[49]=z[6]*z[49];
    z[49]= - static_cast<T>(3)+ z[49];
    z[49]=z[49]*z[58];
    z[49]= - z[24] + z[49];
    z[49]=z[8]*z[49];
    z[34]= - z[6]*z[34];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[34]*z[33];
    z[34]=z[49] + z[34];
    z[34]=z[7]*z[34];
    z[40]=z[4]*z[40];
    z[34]=z[34] + z[40] + z[48];
    z[34]=z[7]*z[34];
    z[40]=z[22] - z[41];
    z[40]=z[8]*z[40];
    z[41]= - z[4]*z[22];
    z[34]=z[34] + z[41] + z[40];
    z[34]=z[10]*z[34];
    z[40]=z[6] + 4;
    z[40]=z[40]*z[16];
    z[40]=z[40] - z[23];
    z[41]=n<T>(5,2) - z[45];
    z[41]=z[41]*z[16];
    z[23]=z[41] + n<T>(2,3)*z[23];
    z[23]=z[8]*z[23];
    z[23]=n<T>(2,3)*z[40] + z[23];
    z[23]=z[8]*z[23];
    z[40]= - z[40]*z[63];
    z[23]=z[34] + z[30] + z[40] + z[23];
    z[23]=z[10]*z[23];
    z[30]=z[6]*z[47];
    z[30]= - 4*z[35] + z[30] - z[27];
    z[30]=z[8]*z[30];
    z[27]= - z[57] + z[27];
    z[27]=z[4]*z[27];
    z[27]=z[27] + z[30];
    z[30]= - static_cast<T>(11)+ z[55];
    z[30]=z[30]*z[58];
    z[30]=z[30] + n<T>(1,2) - z[24];
    z[25]=z[30]*z[25];
    z[30]=z[47]*z[33];
    z[25]=n<T>(2,3)*z[30] - n<T>(1,2) + z[25];
    z[25]=z[7]*z[25];
    z[23]=z[23] + n<T>(1,3)*z[27] + z[25];
    z[23]=z[10]*z[23];
    z[25]=z[9]*z[54];
    z[22]= - z[22]*z[60];
    z[16]=z[16] + z[22];
    z[16]=z[8]*z[16];
    z[16]=z[25] - z[36] + z[16];
    z[16]=z[13]*z[16];
    z[22]= - z[8] + z[33];
    z[22]=z[22]*z[53];
    z[22]=n<T>(1,2)*z[22] - z[32];
    z[22]=z[11]*z[22];
    z[16]=z[16] + z[22];
    z[22]= - z[43]*z[14];
    z[14]= - n<T>(3,2) - z[14];
    z[14]=z[6]*z[14];
    z[14]= - n<T>(7,6) + z[14];
    z[14]=z[2]*z[14];
    z[14]=z[14] + n<T>(7,3) + z[46];
    z[14]=z[2]*z[14];
    z[14]=z[14] - static_cast<T>(2)+ z[22];
    z[14]=z[2]*z[14];
    z[22]=z[36] + 1;
    z[22]=z[22]*z[6];
    z[22]=z[22] + n<T>(1,2);
    z[25]= - 5*z[22] + z[44];
    z[25]=z[2]*z[25];
    z[25]=2*z[39] + z[25];
    z[25]=z[2]*z[25];
    z[26]= - z[26] + z[38];
    z[26]=z[2]*z[26];
    z[26]=z[26] - z[38];
    z[26]=z[2]*z[26];
    z[15]=z[26] + z[15];
    z[15]=z[4]*z[15];
    z[15]=z[15] + z[25] - z[22];
    z[15]=z[4]*z[15];
    z[14]=z[15] + n<T>(1,6)*z[29] + z[14];
    z[14]=z[4]*z[14];
    z[15]=z[4] - 2;
    z[15]=z[15]*z[4];
    z[15]=z[15] + 1;
    z[15]=z[15]*z[1];
    z[22]= - 2*z[15] - 4*z[28] + z[42];
    z[22]=z[4]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[52];
    z[25]=z[4]*z[59];
    z[22]=z[22] - n<T>(1,3) + z[25];
    z[22]=z[22]*npow(z[1],2);
    z[24]= - static_cast<T>(1)- z[24];
    z[24]=z[4]*z[24];
    z[15]= - z[15] + static_cast<T>(1)+ z[24];
    z[15]=z[5]*z[15]*npow(z[1],3);
    z[15]=z[22] + n<T>(1,3)*z[15];
    z[15]=z[5]*z[15];
    z[19]=z[51]*z[19];
    z[19]=n<T>(1,2) + z[19];
    z[19]=z[2]*z[19];
    z[22]= - static_cast<T>(1)+ z[37];
    z[22]=z[6]*z[22];
    z[22]= - n<T>(11,2) + z[22];
    z[19]=n<T>(1,6)*z[22] + z[19];
    z[19]=z[2]*z[19];
    z[22]=n<T>(13,2) - z[6];
    z[19]=n<T>(1,6)*z[22] + z[19];
    z[19]=z[2]*z[19];
    z[17]=n<T>(7,4)*z[6] + z[17];
    z[17]=n<T>(1,3)*z[17] - n<T>(1,4)*z[35];
    z[17]=z[8]*z[17];
    z[22]= - n<T>(1,3) + n<T>(1,4)*z[58];
    z[22]=z[8]*z[22];
    z[22]=z[22] + n<T>(7,12)*z[33];
    z[22]=z[7]*z[22];

    r += n<T>(3,4) + z[14] + z[15] + n<T>(1,2)*z[16] + z[17] + z[18] + z[19] + 
      z[20] + z[21] + z[22] + z[23] + z[31];
 
    return r;
}

template double qg_2lha_r2102(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2102(const std::array<dd_real,30>&);
#endif
