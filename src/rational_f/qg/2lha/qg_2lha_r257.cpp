#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r257(const std::array<T,30>& k) {
  T z[42];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[11];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=k[5];
    z[9]=k[14];
    z[10]=k[9];
    z[11]=n<T>(1,2)*z[10];
    z[12]= - static_cast<T>(1)+ z[11];
    z[12]=z[12]*z[11];
    z[13]=n<T>(1,2)*z[8];
    z[12]=z[12] - z[13] + n<T>(1,4) + z[9];
    z[12]=z[2]*z[12];
    z[14]=z[11] - z[9];
    z[15]=n<T>(1,4)*z[4];
    z[12]=z[12] - z[15] + n<T>(1,4) - z[14];
    z[12]=z[3]*z[12];
    z[16]=n<T>(1,2) - z[4];
    z[16]=z[16]*z[15];
    z[17]=n<T>(1,4)*z[8];
    z[18]= - n<T>(9,2) + z[4];
    z[18]=z[4]*z[18];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[17];
    z[19]=z[10] - 1;
    z[20]=z[19]*z[11];
    z[21]=n<T>(3,2)*z[8];
    z[22]=z[21] - z[9];
    z[20]=z[20] - z[22];
    z[23]=n<T>(1,2)*z[2];
    z[20]=z[20]*z[23];
    z[12]=z[12] + z[20] - n<T>(3,4)*z[10] + z[18] + z[16] - n<T>(1,8) + z[9];
    z[12]=z[3]*z[12];
    z[16]=z[10]*z[19];
    z[16]=z[16] + z[22];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[16]=n<T>(1,2)*z[3];
    z[18]=npow(z[2],2);
    z[19]= - n<T>(19,6) - z[2];
    z[19]=z[2]*z[19];
    z[19]=n<T>(1,6) + z[19];
    z[19]=z[19]*z[18]*z[16];
    z[20]=3*z[2];
    z[24]=n<T>(53,6) + z[20];
    z[24]=z[24]*z[23];
    z[24]=n<T>(1,3) + z[24];
    z[24]=z[2]*z[24];
    z[19]=z[24] + z[19];
    z[19]=z[3]*z[19];
    z[24]=z[7] - 1;
    z[25]=npow(z[7],2);
    z[26]=static_cast<T>(1)- z[25];
    z[26]=z[10]*z[26];
    z[26]=z[26] + z[24];
    z[27]=z[20] - z[7];
    z[28]= - n<T>(17,3) - z[27];
    z[28]=z[28]*z[23];
    z[19]=z[19] + n<T>(5,3)*z[26] + z[28];
    z[26]= - n<T>(41,3) - n<T>(7,4)*z[2];
    z[26]=z[2]*z[26];
    z[26]=n<T>(91,12) + z[26];
    z[26]=z[26]*z[18];
    z[28]=n<T>(31,3) + z[2];
    z[28]=z[28]*z[23];
    z[28]= - n<T>(17,3) + z[28];
    z[29]=npow(z[2],3);
    z[30]=z[29]*z[3];
    z[28]=z[28]*z[30];
    z[26]=z[26] + z[28];
    z[26]=z[26]*z[16];
    z[28]= - n<T>(2,3) + z[2];
    z[28]=z[28]*z[29];
    z[31]=z[2] - 1;
    z[32]=npow(z[2],4);
    z[33]=z[32]*z[3];
    z[34]=z[31]*z[33];
    z[28]=z[28] - n<T>(1,3)*z[34];
    z[28]=z[3]*z[28];
    z[28]= - n<T>(2,3)*z[29] + z[28];
    z[34]=5*z[5];
    z[28]=z[28]*z[34];
    z[35]=static_cast<T>(17)- n<T>(3,2)*z[7];
    z[35]=n<T>(1,4)*z[35] + z[2];
    z[35]=z[35]*z[18];
    z[26]=z[28] + z[35] + z[26];
    z[26]=z[5]*z[26];
    z[19]=n<T>(1,2)*z[19] + z[26];
    z[19]=z[5]*z[19];
    z[26]= - static_cast<T>(1)+ n<T>(1,2)*z[4];
    z[28]= - z[23] - z[14] + z[26];
    z[28]=z[28]*z[16];
    z[14]=z[28] + n<T>(1,16)*z[2] + n<T>(7,8)*z[4] - n<T>(7,16) - z[14];
    z[14]=z[3]*z[14];
    z[28]=n<T>(5,3)*z[10];
    z[24]= - z[24]*z[28];
    z[35]= - static_cast<T>(1)- n<T>(13,6)*z[7];
    z[24]=n<T>(1,2)*z[35] + z[24];
    z[24]=z[10]*z[24];
    z[24]=n<T>(5,8) + z[24];
    z[14]=z[19] + n<T>(1,2)*z[24] + z[14];
    z[14]=z[5]*z[14];
    z[19]= - n<T>(11,4) - z[2];
    z[19]=z[19]*z[18];
    z[24]=n<T>(11,3) + z[2];
    z[24]=z[24]*z[30];
    z[19]=z[19] + n<T>(1,4)*z[24];
    z[19]=z[19]*z[16];
    z[24]=n<T>(1,2)*z[7];
    z[35]=n<T>(1,3) + z[7];
    z[35]=z[35]*z[24];
    z[35]= - n<T>(1,3) + z[35];
    z[35]=z[10]*z[35];
    z[35]=z[35] + n<T>(1,3) - z[24];
    z[36]=static_cast<T>(17)- 3*z[7];
    z[20]=n<T>(1,2)*z[36] + z[20];
    z[20]=z[2]*z[20];
    z[36]= - z[32]*z[16];
    z[36]=2*z[29] + z[36];
    z[36]=z[3]*z[36];
    z[36]= - z[18] + n<T>(1,3)*z[36];
    z[34]=z[36]*z[34];
    z[19]=z[34] + z[19] + 5*z[35] + n<T>(1,4)*z[20];
    z[19]=z[5]*z[19];
    z[20]= - static_cast<T>(1)+ n<T>(1,4)*z[7];
    z[34]= - z[7]*z[20];
    z[34]= - n<T>(1,2) + z[34];
    z[34]=z[34]*z[28];
    z[35]=2*z[7];
    z[34]=z[34] - n<T>(23,12) + z[35];
    z[34]=z[10]*z[34];
    z[36]=n<T>(1,8)*z[2];
    z[37]= - static_cast<T>(1)+ z[36];
    z[37]=z[2]*z[37];
    z[26]=z[37] + n<T>(1,2)*z[9] + z[26];
    z[26]=z[3]*z[26];
    z[37]=n<T>(3,2)*z[2];
    z[38]= - z[37] + static_cast<T>(1)+ z[10];
    z[26]=n<T>(1,2)*z[38] + z[26];
    z[26]=z[26]*z[16];
    z[38]=z[7] + n<T>(5,2);
    z[19]=z[19] + z[26] + n<T>(1,8)*z[38] + z[34];
    z[19]=z[5]*z[19];
    z[26]=z[15] - 1;
    z[26]=z[26]*z[4];
    z[34]= - n<T>(3,2) + z[9];
    z[34]=z[36] + z[10] + n<T>(3,2)*z[34] - z[26];
    z[34]=z[3]*z[34];
    z[39]= - n<T>(7,4) + 5*z[10];
    z[34]=n<T>(1,2)*z[39] + z[34];
    z[34]=z[3]*z[34];
    z[39]=n<T>(3,2)*z[10];
    z[20]= - z[39] - z[20];
    z[20]=z[10]*z[20];
    z[20]=z[20] + z[34];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[5]*z[19];
    z[20]=z[24] - 1;
    z[34]=z[7]*z[20]*z[28];
    z[40]=n<T>(29,3) - 17*z[7];
    z[34]=n<T>(1,4)*z[40] + z[34];
    z[34]=z[10]*z[34];
    z[40]=z[10]*z[7];
    z[41]= - static_cast<T>(1)- z[7];
    z[41]=z[41]*z[40];
    z[41]=z[7] + z[41];
    z[41]=z[5]*z[41];
    z[24]=z[24] + 3;
    z[34]=n<T>(10,3)*z[41] - n<T>(1,4)*z[24] + z[34];
    z[34]=z[5]*z[34];
    z[20]= - z[10]*z[20];
    z[20]=n<T>(1,2)*z[38] + z[20];
    z[20]=z[10]*z[20];
    z[38]=n<T>(1,2) - z[10];
    z[38]=z[3]*z[38];
    z[20]=z[20] + z[38];
    z[20]=n<T>(1,2)*z[20] + z[34];
    z[20]=z[5]*z[20];
    z[34]=z[3]*z[10];
    z[38]= - n<T>(5,2) - z[3];
    z[38]=z[38]*z[34];
    z[41]=npow(z[10],2);
    z[38]=z[41] + z[38];
    z[20]=n<T>(1,4)*z[38] + z[20];
    z[20]=z[5]*z[20];
    z[24]=z[40] - z[24];
    z[38]=n<T>(1,4)*z[10];
    z[24]=z[24]*z[38];
    z[25]= - z[25]*z[11];
    z[25]=z[35] + z[25];
    z[25]=z[5]*z[25]*z[28];
    z[24]=z[24] + z[25];
    z[24]=z[5]*z[24];
    z[25]= - n<T>(1,2)*z[41] + z[34];
    z[24]=n<T>(1,4)*z[25] + z[24];
    z[24]=z[6]*z[24]*npow(z[5],2);
    z[20]=z[20] + z[24];
    z[20]=z[6]*z[20];
    z[24]= - static_cast<T>(5)+ z[4];
    z[24]=z[24]*z[15];
    z[24]=static_cast<T>(1)+ z[24];
    z[13]=z[24]*z[13];
    z[24]= - z[22]*z[23];
    z[13]=z[24] + z[11] + z[9] + z[13];
    z[13]=z[3]*z[13];
    z[24]=z[39] + z[22];
    z[13]=n<T>(1,2)*z[24] + z[13];
    z[13]=z[3]*z[13];
    z[13]= - n<T>(1,4)*z[41] + z[13];
    z[13]=z[20] + n<T>(1,2)*z[13] + z[19];
    z[13]=z[6]*z[13];
    z[12]=z[13] + n<T>(1,2)*z[12] + z[14];
    z[12]=z[6]*z[12];
    z[13]=n<T>(31,3) + z[27];
    z[13]=z[13]*z[29];
    z[14]=static_cast<T>(17)+ z[2];
    z[14]=z[2]*z[14];
    z[14]= - static_cast<T>(37)+ z[14];
    z[14]=z[2]*z[14];
    z[14]=static_cast<T>(19)+ z[14];
    z[14]=z[14]*z[16];
    z[16]= - n<T>(41,3) - z[37];
    z[16]=z[2]*z[16];
    z[14]=z[14] + n<T>(91,6) + z[16];
    z[14]=z[14]*z[30];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[14]=z[31]*z[32];
    z[16]=static_cast<T>(1)- z[23];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[16]*z[33];
    z[14]=z[14] + z[16];
    z[14]=z[3]*z[14];
    z[14]= - n<T>(1,2)*z[32] + z[14];
    z[14]=z[5]*z[14];
    z[13]=n<T>(1,4)*z[13] + n<T>(5,3)*z[14];
    z[13]=z[5]*z[13];
    z[14]=n<T>(53,3) + 5*z[2];
    z[14]=z[14]*z[36];
    z[16]= - n<T>(29,4) - z[2];
    z[16]=z[2]*z[16];
    z[16]=n<T>(31,2) + z[16];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(29,4) + z[16];
    z[16]=z[3]*z[16];
    z[14]=n<T>(1,4)*z[16] - n<T>(7,3) + z[14];
    z[14]=z[3]*z[14];
    z[16]= - n<T>(13,2) + z[7];
    z[16]=n<T>(1,4)*z[16] - z[2];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[18]*z[14];
    z[13]=z[13] + z[14];
    z[13]=z[5]*z[13];
    z[14]=n<T>(17,6) + z[2];
    z[14]=z[14]*z[23];
    z[16]=n<T>(25,6) + z[2];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(31,3) + z[16];
    z[16]=z[2]*z[16];
    z[16]=n<T>(13,6) + z[16];
    z[16]=z[3]*z[16]*z[23];
    z[18]= - n<T>(7,3) - z[2];
    z[18]=z[2]*z[18];
    z[18]=n<T>(5,6) + z[18];
    z[18]=z[2]*z[18];
    z[16]=z[18] + z[16];
    z[16]=z[3]*z[16];
    z[14]=z[14] + z[16];
    z[13]=n<T>(1,4)*z[14] + z[13];
    z[13]=z[5]*z[13];
    z[14]=z[8] + 1;
    z[11]= - z[14]*z[11];
    z[11]=z[11] + static_cast<T>(1)+ z[21];
    z[11]=z[10]*z[11];
    z[11]=z[11] - z[8] + n<T>(3,4) + z[9];
    z[11]=z[2]*z[11];
    z[14]=z[14]*z[10];
    z[16]=z[8] - z[14];
    z[11]=n<T>(1,2)*z[16] + z[11];
    z[16]=static_cast<T>(1)- n<T>(1,2)*z[1];
    z[16]=n<T>(1,2)*z[16] - z[22];
    z[14]= - n<T>(1,4)*z[14] + n<T>(3,4) + z[8];
    z[14]=z[10]*z[14];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[3]*z[2]*z[14];
    z[11]=n<T>(1,2)*z[11] + z[14];
    z[11]=z[3]*z[11];
    z[14]= - z[1]*z[15];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[4]*z[14];
    z[15]= - n<T>(1,2) + z[26];
    z[15]=z[8]*z[15];
    z[14]=z[15] + z[9] + z[14];
    z[15]= - n<T>(5,6)*z[10] + n<T>(1,3) - z[17];
    z[15]=z[10]*z[15];
    z[11]=z[11] + n<T>(1,2)*z[14] + z[15];

    r += n<T>(1,2)*z[11] + z[12] + z[13];
 
    return r;
}

template double qg_2lha_r257(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r257(const std::array<dd_real,30>&);
#endif
