#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1224(const std::array<T,30>& k) {
  T z[55];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[27];
    z[8]=k[6];
    z[9]=k[10];
    z[10]=k[14];
    z[11]=k[9];
    z[12]=k[3];
    z[13]=k[11];
    z[14]=npow(z[12],3);
    z[15]=n<T>(1,4)*z[11];
    z[16]= - z[14]*z[15];
    z[17]=npow(z[12],2);
    z[16]= - z[17] + z[16];
    z[18]=n<T>(1,3)*z[11];
    z[16]=z[16]*z[18];
    z[19]=n<T>(1,2)*z[12];
    z[20]= - n<T>(1,3) - z[19];
    z[16]=n<T>(1,2)*z[20] + z[16];
    z[20]=2*z[7];
    z[21]=z[1]*z[7];
    z[22]=z[20] + z[21];
    z[23]= - n<T>(7,4) + z[22];
    z[24]=n<T>(1,3)*z[1];
    z[23]=z[23]*z[24];
    z[25]=n<T>(1,3)*z[7];
    z[16]=z[23] + n<T>(7,2)*z[16] + z[25];
    z[16]=z[5]*z[16];
    z[23]= - z[11]*z[14];
    z[23]= - n<T>(29,12)*z[17] + z[23];
    z[23]=z[11]*z[23];
    z[26]=4*z[7];
    z[27]= - 2*z[21] - n<T>(5,4) - z[26];
    z[27]=z[27]*z[24];
    z[28]=n<T>(2,3)*z[7];
    z[29]= - static_cast<T>(5)- 17*z[12];
    z[16]=z[16] + z[27] - z[28] + n<T>(1,12)*z[29] + z[23];
    z[16]=z[5]*z[16];
    z[23]= - static_cast<T>(1)+ n<T>(1,4)*z[8];
    z[27]=n<T>(1,2)*z[1];
    z[29]=z[27] + 1;
    z[29]=z[29]*z[1];
    z[29]=z[29] + n<T>(1,2);
    z[30]=n<T>(1,3)*z[8];
    z[23]=z[30]*z[29]*z[23];
    z[31]=n<T>(1,2)*z[5];
    z[32]=z[31] - 1;
    z[33]=n<T>(1,3)*z[5];
    z[32]=z[33]*z[32];
    z[32]=z[32] + n<T>(1,2);
    z[32]=z[29]*z[32];
    z[23]=z[23] + n<T>(1,2)*z[32];
    z[23]=z[23]*z[3];
    z[32]= - 4*z[21] - n<T>(29,4) - 8*z[7];
    z[32]=z[1]*z[32];
    z[26]=z[32] - n<T>(29,4) - z[26];
    z[32]=z[1]*z[25];
    z[32]=z[32] + static_cast<T>(1)+ z[28];
    z[32]=z[1]*z[32];
    z[32]=z[32] + z[25] + static_cast<T>(1)- n<T>(17,24)*z[12];
    z[32]=z[8]*z[32];
    z[26]=n<T>(1,3)*z[26] + z[32];
    z[26]=z[8]*z[26];
    z[14]=z[14]*npow(z[11],2);
    z[14]= - n<T>(1,2)*z[14] + static_cast<T>(1)+ z[19];
    z[22]=n<T>(17,12) + z[22];
    z[22]=z[1]*z[22];
    z[14]=z[23] + z[26] + z[16] + z[22] + n<T>(17,12)*z[14] + z[7];
    z[14]=z[6]*z[14];
    z[16]=n<T>(1,2)*z[11];
    z[22]= - z[17]*z[16];
    z[22]= - n<T>(31,3)*z[12] + z[22];
    z[22]=z[11]*z[22];
    z[22]= - n<T>(59,6) + z[22];
    z[26]=npow(z[7],2);
    z[32]=z[26]*z[1];
    z[34]=n<T>(1,6)*z[32];
    z[35]=static_cast<T>(1)- z[25];
    z[35]=z[7]*z[35];
    z[35]=z[35] - z[34];
    z[35]=z[1]*z[35];
    z[36]=static_cast<T>(1)- n<T>(1,6)*z[7];
    z[36]=z[7]*z[36];
    z[22]=z[35] + n<T>(1,8)*z[22] + z[36];
    z[22]=z[22]*z[31];
    z[35]=z[17]*z[11];
    z[36]=static_cast<T>(5)+ n<T>(1,12)*z[12];
    z[36]=z[12]*z[36];
    z[36]=z[36] + n<T>(23,6)*z[35];
    z[36]=z[11]*z[36];
    z[37]=n<T>(1,3)*z[12];
    z[38]=static_cast<T>(5)+ z[37];
    z[36]=n<T>(1,4)*z[38] + z[36];
    z[38]=n<T>(1,2)*z[7];
    z[39]=static_cast<T>(2)+ z[38];
    z[39]=z[39]*z[25];
    z[40]=z[27]*z[7];
    z[41]= - n<T>(1,2) + z[7];
    z[41]=z[41]*z[40];
    z[42]=n<T>(7,4) + z[7];
    z[42]=z[7]*z[42];
    z[41]=z[41] + n<T>(1,16) + z[42];
    z[41]=z[41]*z[24];
    z[22]=z[22] + z[41] + n<T>(1,4)*z[36] + z[39];
    z[22]=z[5]*z[22];
    z[36]= - static_cast<T>(11)- z[38];
    z[36]=z[36]*z[25];
    z[39]= - static_cast<T>(7)- z[7];
    z[39]=z[39]*z[40];
    z[41]= - n<T>(29,2) - z[7];
    z[41]=z[7]*z[41];
    z[39]=z[39] - n<T>(43,4) + z[41];
    z[39]=z[39]*z[24];
    z[41]=n<T>(33,2) + z[13];
    z[41]=z[12]*z[41];
    z[41]= - n<T>(299,12) + z[41];
    z[36]=z[39] + n<T>(1,4)*z[41] + z[36];
    z[39]=n<T>(1,2)*z[8];
    z[36]=z[36]*z[39];
    z[41]=n<T>(5,3)*z[12];
    z[42]=z[41] + 1;
    z[43]=z[9]*z[12];
    z[42]=z[42]*z[43];
    z[41]=z[42] - n<T>(43,2) + z[41];
    z[44]=n<T>(7,4) + z[25];
    z[44]=z[44]*z[21];
    z[28]=n<T>(27,4) + z[28];
    z[28]=z[7]*z[28];
    z[28]=z[44] + n<T>(39,16) + z[28];
    z[28]=z[1]*z[28];
    z[44]=static_cast<T>(5)+ z[25];
    z[44]=z[7]*z[44];
    z[28]=z[36] + z[28] + z[44] - n<T>(1,4)*z[41];
    z[28]=z[8]*z[28];
    z[36]=z[1] + 1;
    z[33]= - z[36]*z[33];
    z[41]= - n<T>(1,3) - z[27];
    z[41]=z[1]*z[41];
    z[41]=n<T>(1,6) + z[41];
    z[33]=n<T>(1,2)*z[41] + z[33];
    z[33]=z[5]*z[33];
    z[29]= - z[29]*z[30];
    z[41]=n<T>(5,3) + z[27];
    z[41]=z[1]*z[41];
    z[29]=z[29] + n<T>(7,6) + z[41];
    z[29]=z[29]*z[39];
    z[41]=npow(z[1],2);
    z[44]= - static_cast<T>(1)+ z[41];
    z[23]= - 13*z[23] + z[29] + n<T>(1,6)*z[44] + z[33];
    z[29]=n<T>(1,4)*z[3];
    z[23]=z[23]*z[29];
    z[33]=static_cast<T>(1)- n<T>(11,6)*z[12];
    z[33]=z[12]*z[33];
    z[33]=z[33] + n<T>(167,12)*z[35];
    z[33]=z[11]*z[33];
    z[35]= - n<T>(59,2) - n<T>(11,3)*z[12];
    z[33]=n<T>(1,2)*z[35] + z[33];
    z[35]=z[16]*z[42];
    z[42]= - static_cast<T>(5)- z[38];
    z[42]=z[7]*z[42];
    z[33]=z[35] + n<T>(1,4)*z[33] + z[42];
    z[35]= - static_cast<T>(1)- z[38];
    z[35]=z[35]*z[40];
    z[40]= - static_cast<T>(3)- z[38];
    z[40]=z[7]*z[40];
    z[35]=z[35] - n<T>(11,48) + z[40];
    z[35]=z[1]*z[35];
    z[14]=z[14] + z[23] + z[28] + z[22] + n<T>(1,2)*z[33] + z[35];
    z[14]=z[6]*z[14];
    z[22]= - n<T>(85,3) - z[13];
    z[23]= - n<T>(83,3) - 5*z[7];
    z[23]=z[7]*z[23];
    z[22]=n<T>(1,2)*z[22] + z[23];
    z[23]=static_cast<T>(1)+ n<T>(13,3)*z[12];
    z[23]=n<T>(1,2)*z[23] + n<T>(1,3)*z[43];
    z[23]=z[9]*z[23];
    z[28]=z[26]*z[27];
    z[33]= - n<T>(7,2) - z[7];
    z[33]=z[7]*z[33];
    z[33]= - z[28] - n<T>(7,8) + 3*z[33];
    z[33]=z[1]*z[33];
    z[22]=z[33] + n<T>(1,2)*z[22] + z[23];
    z[23]=n<T>(1,8)*z[12];
    z[33]= - n<T>(77,6) - 3*z[13];
    z[33]=z[33]*z[23];
    z[35]= - static_cast<T>(1)+ n<T>(5,2)*z[12];
    z[35]=z[35]*z[43];
    z[40]=n<T>(37,6) + z[7];
    z[38]=z[40]*z[38];
    z[32]=n<T>(1,12)*z[32] + static_cast<T>(1)+ z[38];
    z[32]=z[1]*z[32];
    z[38]=static_cast<T>(4)+ n<T>(5,12)*z[7];
    z[38]=z[7]*z[38];
    z[32]=z[32] + n<T>(1,6)*z[35] + z[38] + n<T>(10,3) + z[33];
    z[32]=z[8]*z[32];
    z[22]=n<T>(1,2)*z[22] + z[32];
    z[22]=z[8]*z[22];
    z[32]=n<T>(1,4) + z[1];
    z[32]=z[32]*z[24];
    z[32]= - n<T>(1,4) + z[32];
    z[33]=z[5] + 1;
    z[33]=z[9]*z[33];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[5]*z[33];
    z[33]=n<T>(23,3)*z[36] + z[33];
    z[35]=n<T>(1,4)*z[5];
    z[33]=z[33]*z[35];
    z[32]=7*z[32] + z[33];
    z[32]=z[5]*z[32];
    z[33]=n<T>(1,2) - z[1];
    z[33]=z[1]*z[33];
    z[33]=n<T>(3,2) + z[33];
    z[32]=n<T>(5,2)*z[33] + z[32];
    z[33]= - n<T>(89,6) + z[1];
    z[33]=z[1]*z[33];
    z[33]= - n<T>(95,6) + z[33];
    z[36]=static_cast<T>(11)- z[1];
    z[36]=z[1]*z[36];
    z[36]=static_cast<T>(1)+ n<T>(1,12)*z[36];
    z[36]=z[8]*z[36];
    z[33]=n<T>(1,4)*z[33] + z[36];
    z[33]=z[8]*z[33];
    z[32]=n<T>(1,2)*z[32] + z[33];
    z[29]=z[32]*z[29];
    z[32]=n<T>(1,8)*z[5];
    z[33]=n<T>(3,2)*z[5] + n<T>(3,2);
    z[33]=z[9]*z[33];
    z[33]=n<T>(13,3) + z[33];
    z[33]=z[33]*z[32];
    z[36]=n<T>(25,16) - z[1];
    z[33]=n<T>(1,3)*z[36] + z[33];
    z[33]=z[5]*z[33];
    z[36]=n<T>(11,4) + z[1];
    z[36]=z[1]*z[36];
    z[36]= - n<T>(17,16) + z[36];
    z[33]=n<T>(1,3)*z[36] + z[33];
    z[36]=n<T>(7,8)*z[1];
    z[38]=static_cast<T>(1)+ z[36];
    z[38]=z[1]*z[38];
    z[38]= - n<T>(1,8) + z[38];
    z[38]=z[38]*z[39];
    z[36]= - static_cast<T>(2)- z[36];
    z[36]=z[1]*z[36];
    z[36]=z[38] + n<T>(13,32) + z[36];
    z[36]=z[36]*z[30];
    z[29]=z[29] + n<T>(1,2)*z[33] + z[36];
    z[29]=z[3]*z[29];
    z[33]=z[11] - n<T>(13,3);
    z[33]=z[12]*z[33];
    z[33]= - static_cast<T>(1)+ z[33];
    z[33]=z[11]*z[33];
    z[33]=z[23] + z[33];
    z[36]= - z[43]*z[18];
    z[33]=n<T>(1,2)*z[33] + z[36];
    z[36]=n<T>(1,2)*z[9];
    z[33]=z[33]*z[36];
    z[38]=z[7] - n<T>(1,3);
    z[38]=z[38]*z[7];
    z[39]=static_cast<T>(1)- z[19];
    z[39]=z[11]*z[39];
    z[39]= - static_cast<T>(1)+ n<T>(11,4)*z[39];
    z[40]=static_cast<T>(3)+ z[12];
    z[40]=z[9]*z[40];
    z[39]=n<T>(1,8)*z[40] + n<T>(1,3)*z[39] - z[38];
    z[40]= - n<T>(1,2) - z[7];
    z[40]=z[7]*z[40];
    z[28]=z[40] + z[28];
    z[28]=z[28]*z[24];
    z[40]=n<T>(3,16)*z[9] - n<T>(7,12)*z[11] + z[7];
    z[40]=z[5]*z[40];
    z[28]=z[40] + n<T>(1,2)*z[39] + z[28];
    z[28]=z[28]*z[31];
    z[39]=z[20] + n<T>(23,4);
    z[39]=z[39]*z[25];
    z[20]=n<T>(11,4) + z[20];
    z[20]=z[20]*z[25];
    z[20]=z[20] - n<T>(1,4)*z[21];
    z[20]=z[1]*z[20];
    z[40]= - n<T>(53,6)*z[11] + n<T>(43,12);
    z[40]=z[12]*z[40];
    z[40]= - static_cast<T>(1)+ z[40];
    z[40]=z[11]*z[40];
    z[40]=n<T>(23,6) + z[40];
    z[14]=z[14] + z[29] + z[22] + z[28] + z[20] + z[33] + n<T>(1,8)*z[40] + 
    z[39];
    z[14]=z[6]*z[14];
    z[20]=npow(z[2],2);
    z[22]=z[20]*z[9];
    z[28]=n<T>(1,2)*z[2];
    z[29]= - z[31] - z[28] - n<T>(7,3)*z[22];
    z[29]=z[9]*z[29];
    z[29]= - n<T>(1,2) + z[29];
    z[29]=z[29]*z[32];
    z[32]=n<T>(1,3)*z[10];
    z[33]=n<T>(1,2)*z[10];
    z[40]=z[33] - 1;
    z[40]=z[40]*z[10];
    z[42]=n<T>(1,2) + z[40];
    z[42]=z[42]*z[32];
    z[44]=n<T>(1,3) - z[28];
    z[44]=z[2]*z[44];
    z[22]=z[44] + n<T>(11,48)*z[22];
    z[22]=z[9]*z[22];
    z[44]=n<T>(1,4)*z[2];
    z[45]= - n<T>(5,3) - z[44];
    z[22]=z[29] - n<T>(23,48)*z[1] + z[22] + n<T>(1,4)*z[45] + z[42];
    z[22]=z[5]*z[22];
    z[29]=n<T>(1,3)*z[2];
    z[42]=z[29] - 1;
    z[45]=z[42]*z[20];
    z[46]=z[20]*z[10];
    z[47]= - z[46] + 3*z[20];
    z[47]=z[47]*z[33];
    z[45]=z[45] + z[47];
    z[47]=npow(z[2],3);
    z[48]=z[47]*z[10];
    z[49]=n<T>(1,4)*z[48];
    z[50]=z[47] - z[49];
    z[32]=z[50]*z[32];
    z[32]= - n<T>(1,4)*z[47] + z[32];
    z[32]=z[9]*z[32];
    z[32]=n<T>(1,2)*z[45] + z[32];
    z[32]=z[9]*z[32];
    z[45]=n<T>(1,2) - z[29];
    z[45]=z[45]*z[20];
    z[50]=n<T>(1,3)*z[9];
    z[51]=z[47]*z[50];
    z[45]=z[45] + z[51];
    z[45]=z[9]*z[45];
    z[51]=npow(z[9],2);
    z[47]=z[5]*z[47]*z[51];
    z[52]= - static_cast<T>(1)+ z[10];
    z[52]=z[10]*z[52];
    z[47]=z[52] - z[47];
    z[45]=z[45] + n<T>(1,6)*z[47];
    z[45]=z[45]*z[31];
    z[47]=z[2] - n<T>(1,3);
    z[40]= - z[47]*z[40];
    z[52]= - z[2]*z[42];
    z[52]= - static_cast<T>(1)+ z[52];
    z[52]=z[2]*z[52];
    z[52]=n<T>(1,3) + z[52];
    z[40]=n<T>(1,2)*z[52] + z[40];
    z[32]=z[45] + n<T>(1,2)*z[40] + z[32];
    z[32]=z[3]*z[32];
    z[40]=n<T>(1,12)*z[2];
    z[45]=5*z[2];
    z[52]=static_cast<T>(1)- z[45];
    z[52]=z[52]*z[40];
    z[53]= - z[20]*z[33];
    z[54]=n<T>(1,8) + z[2];
    z[54]=z[2]*z[54];
    z[53]=z[54] + z[53];
    z[53]=z[10]*z[53];
    z[52]=z[52] + z[53];
    z[52]=z[10]*z[52];
    z[53]=n<T>(1,16) + z[29];
    z[53]=z[53]*z[20];
    z[53]=z[53] - n<T>(1,6)*z[48];
    z[53]=z[10]*z[53];
    z[53]=n<T>(5,48)*z[20] + z[53];
    z[53]=z[10]*z[53];
    z[53]= - n<T>(1,6)*z[20] + z[53];
    z[53]=z[9]*z[53];
    z[54]= - static_cast<T>(5)+ n<T>(19,2)*z[2];
    z[54]=z[2]*z[54];
    z[52]=z[53] + n<T>(1,24)*z[54] + z[52];
    z[52]=z[9]*z[52];
    z[47]= - z[47]*z[33];
    z[47]=z[47] - n<T>(17,48) + z[2];
    z[47]=z[10]*z[47];
    z[53]=n<T>(19,4) - 7*z[2];
    z[47]=n<T>(1,12)*z[53] + z[47];
    z[47]=z[10]*z[47];
    z[41]=z[41]*z[4];
    z[53]= - n<T>(11,2) + z[41];
    z[30]=z[53]*z[30];
    z[30]=z[30] + n<T>(15,2) - z[41];
    z[41]=n<T>(1,8)*z[8];
    z[30]=z[30]*z[41];
    z[28]=static_cast<T>(1)- z[28];
    z[28]=z[2]*z[28];
    z[28]= - n<T>(1,2) + z[28];
    z[42]= - z[4]*z[42];
    z[42]= - n<T>(5,2) + z[42];
    z[42]=z[1]*z[42];
    z[42]=z[42] + static_cast<T>(7)- n<T>(23,6)*z[2];
    z[42]=z[1]*z[42];
    z[22]=z[32] + z[30] + z[22] + n<T>(1,8)*z[42] + z[52] + n<T>(5,12)*z[28] + 
    z[47];
    z[22]=z[3]*z[22];
    z[28]= - static_cast<T>(1)- n<T>(5,2)*z[2];
    z[28]=z[28]*z[29];
    z[30]=3*z[46];
    z[32]=static_cast<T>(1)+ z[45];
    z[32]=z[2]*z[32];
    z[32]=z[32] - z[30];
    z[32]=z[32]*z[33];
    z[28]=z[32] - n<T>(1,4) + z[28];
    z[28]=z[10]*z[28];
    z[32]=static_cast<T>(1)+ n<T>(17,6)*z[2];
    z[28]=n<T>(1,4)*z[32] + z[28];
    z[28]=z[10]*z[28];
    z[28]= - n<T>(11,24)*z[2] + z[28];
    z[32]=n<T>(1,8)*z[2];
    z[42]= - static_cast<T>(1)+ n<T>(5,6)*z[2];
    z[42]=z[42]*z[32];
    z[46]=n<T>(1,8) + z[29];
    z[46]=z[46]*z[20];
    z[46]=z[46] - z[49];
    z[46]=z[10]*z[46];
    z[42]=z[42] + z[46];
    z[42]=z[10]*z[42];
    z[42]=z[32] + z[42];
    z[42]=z[9]*z[10]*z[42];
    z[28]=n<T>(1,2)*z[28] + z[42];
    z[28]=z[9]*z[28];
    z[42]=z[9]*z[2];
    z[46]=static_cast<T>(1)- n<T>(19,3)*z[2];
    z[46]=n<T>(1,2)*z[46] - z[42];
    z[46]=z[46]*z[36];
    z[42]= - n<T>(3,2) - n<T>(11,3)*z[42];
    z[42]=z[9]*z[42]*z[31];
    z[47]= - n<T>(7,3) + z[10];
    z[47]=z[10]*z[47];
    z[47]=n<T>(5,3) + z[47];
    z[47]=z[10]*z[47];
    z[47]= - n<T>(1,3) + z[47];
    z[47]=z[10]*z[47];
    z[42]=z[42] + z[46] - n<T>(13,6) + z[47];
    z[42]=z[42]*z[35];
    z[46]= - static_cast<T>(1)+ n<T>(7,4)*z[1];
    z[46]=z[24]*z[4]*z[46];
    z[47]=n<T>(3,2) - n<T>(7,3)*z[1];
    z[47]=z[1]*z[4]*z[47];
    z[47]= - n<T>(3,2) + z[47];
    z[41]=z[47]*z[41];
    z[41]=z[41] - n<T>(1,8) + z[46];
    z[41]=z[8]*z[41];
    z[46]=3*z[2];
    z[47]=static_cast<T>(1)- z[46];
    z[47]=z[47]*z[33];
    z[46]=z[47] - n<T>(13,12) + z[46];
    z[46]=z[10]*z[46];
    z[45]=n<T>(19,8) - z[45];
    z[45]=n<T>(1,3)*z[45] + z[46];
    z[45]=z[45]*z[33];
    z[29]=z[45] + n<T>(9,16) - z[29];
    z[29]=z[10]*z[29];
    z[40]=z[40] + n<T>(5,24);
    z[40]=z[4]*z[40];
    z[45]=n<T>(7,12)*z[4];
    z[46]= - z[1]*z[45];
    z[40]=z[46] + static_cast<T>(1)+ z[40];
    z[27]=z[40]*z[27];
    z[40]= - n<T>(1,3) + z[44];
    z[22]=z[22] + z[41] + z[42] + z[27] + z[28] + n<T>(1,2)*z[40] + z[29];
    z[22]=z[3]*z[22];
    z[27]=z[2]*z[11];
    z[28]=static_cast<T>(3)- z[16];
    z[28]=z[28]*z[27];
    z[29]=z[11] - 3*z[27];
    z[29]=z[29]*z[33];
    z[40]= - n<T>(13,2) + z[11];
    z[40]=z[11]*z[40];
    z[28]=z[29] + n<T>(1,6)*z[40] + z[28];
    z[28]=z[10]*z[28];
    z[29]=z[11] - n<T>(5,3);
    z[40]=z[29]*z[27];
    z[41]=n<T>(1,8)*z[11];
    z[42]=n<T>(19,3) - 3*z[11];
    z[42]=z[42]*z[41];
    z[28]=z[28] + z[42] + z[40];
    z[28]=z[28]*z[33];
    z[42]= - n<T>(1,3) - z[15];
    z[42]=z[42]*z[27];
    z[28]=z[28] + n<T>(9,16)*z[11] + z[42];
    z[28]=z[10]*z[28];
    z[42]=n<T>(7,6) + z[13];
    z[44]= - n<T>(5,8) + z[18];
    z[44]=z[11]*z[44];
    z[42]=n<T>(1,2)*z[42] + z[44];
    z[22]=z[22] + n<T>(1,2)*z[42] + z[28];
    z[28]= - z[11] + z[25];
    z[28]=z[28]*z[31];
    z[28]=z[28] - n<T>(3,2) - z[25];
    z[28]=z[7]*z[28];
    z[42]=npow(z[43],2);
    z[28]= - n<T>(1,12)*z[42] + z[16] + z[28];
    z[28]=z[28]*z[31];
    z[42]= - static_cast<T>(1)+ z[12];
    z[42]=z[42]*z[15];
    z[44]=n<T>(19,2) + z[12];
    z[44]=z[44]*z[37];
    z[17]=z[17]*z[9];
    z[44]=z[44] - n<T>(5,2)*z[17];
    z[44]=z[9]*z[44];
    z[23]= - z[13]*z[23];
    z[46]=n<T>(4,3) + n<T>(1,4)*z[7];
    z[46]=z[7]*z[46];
    z[23]=z[28] + z[34] + n<T>(1,16)*z[44] + z[46] + z[42] + n<T>(2,3) + z[23];
    z[23]=z[5]*z[23];
    z[28]=n<T>(1,4)*z[12];
    z[34]= - static_cast<T>(5)- n<T>(7,2)*z[4];
    z[34]=z[34]*z[28];
    z[34]=z[34] - static_cast<T>(1)+ n<T>(23,16)*z[4];
    z[34]=z[34]*z[37];
    z[42]= - static_cast<T>(1)+ z[45];
    z[17]=z[42]*z[17];
    z[17]=z[34] + n<T>(1,4)*z[17];
    z[17]=z[17]*z[36];
    z[34]= - z[45] + n<T>(11,6) + z[13];
    z[19]=z[34]*z[19];
    z[34]=n<T>(1,3)*z[4];
    z[19]=z[19] - n<T>(41,16) + z[34];
    z[42]= - static_cast<T>(4)- z[7];
    z[42]=z[7]*z[42];
    z[42]= - n<T>(7,16)*z[4] + z[42];
    z[42]=z[42]*z[24];
    z[25]= - n<T>(5,2) - z[25];
    z[25]=z[7]*z[25];
    z[17]=z[23] + z[42] + z[17] + n<T>(1,2)*z[19] + z[25];
    z[17]=z[8]*z[17];
    z[19]=z[26]*z[24];
    z[23]= - n<T>(17,6)*z[43] + n<T>(7,4) + z[12];
    z[23]=z[23]*z[36];
    z[24]=z[7]*z[11];
    z[25]=z[12]*z[51];
    z[25]=z[24] - n<T>(7,12)*z[25];
    z[25]=z[5]*z[25];
    z[23]=z[25] - z[19] + z[23] - n<T>(1,2)*z[13] - z[38];
    z[23]=z[23]*z[35];
    z[25]=n<T>(1,6) + z[13];
    z[25]=z[25]*z[28];
    z[26]=n<T>(7,2) + z[13];
    z[25]=z[25] + n<T>(1,4)*z[26] - z[34];
    z[26]= - n<T>(17,4) - z[4];
    z[26]=z[26]*z[37];
    z[26]= - n<T>(7,24)*z[43] - n<T>(9,16) + z[26];
    z[26]=z[26]*z[36];
    z[28]=static_cast<T>(1)+ z[7];
    z[21]=z[28]*z[21];
    z[17]=z[17] + z[23] + n<T>(2,3)*z[21] + z[26] + n<T>(1,2)*z[25] + z[39];
    z[17]=z[8]*z[17];
    z[21]=z[11] - 1;
    z[16]= - z[21]*z[16];
    z[23]=n<T>(17,12) - z[11];
    z[23]=z[23]*z[27];
    z[16]=z[16] + z[23];
    z[23]= - n<T>(1,3) + z[15];
    z[23]=z[11]*z[23];
    z[23]=z[23] + n<T>(1,2)*z[40];
    z[23]=z[2]*z[23];
    z[25]=static_cast<T>(5)- z[11];
    z[25]=z[25]*z[27];
    z[25]=z[11] + z[25];
    z[25]=z[2]*z[25];
    z[26]= - z[11]*z[30];
    z[25]=z[25] + z[26];
    z[25]=z[25]*z[33];
    z[23]=z[25] - z[15] + z[23];
    z[23]=z[10]*z[23];
    z[16]=n<T>(1,2)*z[16] + z[23];
    z[16]=z[10]*z[16];
    z[23]=static_cast<T>(1)- z[15];
    z[23]=z[23]*z[27];
    z[23]=z[41] + n<T>(1,3)*z[23];
    z[20]=z[23]*z[20];
    z[15]= - z[15]*z[48];
    z[15]=z[20] + z[15];
    z[15]=z[10]*z[15];
    z[20]=n<T>(5,3) + z[11];
    z[20]=z[20]*z[27];
    z[20]= - z[11] + n<T>(1,2)*z[20];
    z[20]=z[20]*z[32];
    z[15]=z[20] + z[15];
    z[15]=z[10]*z[15];
    z[20]=z[21]*z[27];
    z[15]= - n<T>(1,8)*z[20] + z[15];
    z[15]=z[10]*z[15];
    z[20]=n<T>(7,4) - z[11];
    z[20]=z[20]*z[18];
    z[20]= - n<T>(1,4) + z[20];
    z[15]=n<T>(1,2)*z[20] + z[15];
    z[15]=z[15]*z[36];
    z[20]=static_cast<T>(1)- n<T>(5,16)*z[11];
    z[20]=z[11]*z[20];
    z[15]=z[15] + n<T>(1,4)*z[16] - n<T>(73,96) + z[20];
    z[15]=z[9]*z[15];
    z[16]= - static_cast<T>(5)- z[11];
    z[16]=z[16]*z[18];
    z[20]= - static_cast<T>(7)+ z[11];
    z[18]=z[20]*z[18];
    z[20]=z[10]*z[11];
    z[18]=z[18] + z[20];
    z[18]=z[10]*z[18];
    z[20]= - z[11]*z[29];
    z[18]=z[20] + z[18];
    z[18]=z[10]*z[18];
    z[20]= - n<T>(1,3) + z[11];
    z[20]=z[11]*z[20];
    z[18]=z[20] + z[18];
    z[18]=z[10]*z[18];
    z[20]=static_cast<T>(1)- 7*z[9];
    z[20]=z[20]*z[50];
    z[16]=z[20] + z[16] + z[18];
    z[16]=n<T>(1,2)*z[16] - z[19];
    z[18]=z[24] - n<T>(11,24)*z[51];
    z[18]=z[5]*z[18];
    z[16]=n<T>(1,2)*z[16] + z[18];
    z[16]=z[16]*z[31];
    z[18]=z[45] + z[7];
    z[18]=z[1]*z[18];

    r += z[14] + z[15] + z[16] + z[17] + n<T>(1,4)*z[18] + n<T>(1,2)*z[22];
 
    return r;
}

template double qg_2lha_r1224(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1224(const std::array<dd_real,30>&);
#endif
