#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2156(const std::array<T,30>& k) {
  T z[30];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[28];
    z[7]=k[3];
    z[8]=k[4];
    z[9]=k[10];
    z[10]=k[18];
    z[11]=k[11];
    z[12]=k[14];
    z[13]=k[9];
    z[14]=n<T>(3,2)*z[6];
    z[15]=z[1]*z[3];
    z[16]= - z[12] - z[11];
    z[16]=z[7]*z[16];
    z[16]=z[14] + n<T>(3,2)*z[16] + n<T>(1,2)*z[15] + n<T>(5,2) - z[11];
    z[16]=z[5]*z[16];
    z[17]=n<T>(1,2)*z[6];
    z[18]=3*z[6];
    z[19]=z[18] + n<T>(3,2)*z[9] - static_cast<T>(5)- n<T>(1,2)*z[1];
    z[19]=z[19]*z[17];
    z[20]=3*z[12];
    z[21]= - n<T>(7,2) - z[20];
    z[22]=z[20] + n<T>(5,2)*z[11];
    z[22]=z[7]*z[22];
    z[16]=z[16] + z[19] + z[22] + n<T>(1,2)*z[21] + 3*z[11];
    z[19]=n<T>(1,2)*z[5];
    z[16]=z[16]*z[19];
    z[21]=n<T>(7,3)*z[4];
    z[22]=n<T>(11,3) - z[10];
    z[22]=n<T>(1,2)*z[22] + z[21];
    z[22]=z[7]*z[22];
    z[23]=n<T>(1,3)*z[7];
    z[24]=n<T>(3,2) - z[23];
    z[24]=z[8]*z[24];
    z[23]= - z[23] + z[24];
    z[24]=n<T>(1,2)*z[9];
    z[23]=z[23]*z[24];
    z[22]=z[23] - z[8] - n<T>(7,6) + z[22];
    z[22]=z[22]*z[24];
    z[23]=n<T>(3,4)*z[9];
    z[25]=npow(z[8],2);
    z[26]= - z[25]*z[23];
    z[26]=z[26] - n<T>(3,4) + z[8];
    z[26]=z[9]*z[26];
    z[25]=z[9]*z[25];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[14];
    z[27]= - static_cast<T>(1)- z[15];
    z[25]=z[25] + n<T>(1,4)*z[27] + z[26];
    z[25]=z[25]*z[17];
    z[26]=z[10] - z[20];
    z[26]=n<T>(1,2)*z[26] - z[11];
    z[26]=z[7]*z[26];
    z[21]=z[26] + n<T>(3,2)*z[12] - n<T>(1,2)*z[10] + z[21];
    z[26]=z[15]*z[4];
    z[26]=n<T>(1,8)*z[26];
    z[27]=n<T>(1,2)*z[8];
    z[28]=z[27] - 1;
    z[28]=z[11]*z[28];
    z[16]=z[16] + z[25] + z[22] + z[26] + z[28] + n<T>(1,2)*z[21];
    z[16]=z[2]*z[16];
    z[21]=z[8] - 1;
    z[22]=z[21]*z[23];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[9]*z[22];
    z[21]= - z[21]*z[24];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[18];
    z[23]=n<T>(1,4)*z[15];
    z[21]=z[21] + z[23] + z[22];
    z[21]=z[21]*z[17];
    z[22]=z[9] + 3;
    z[25]= - z[22]*z[18];
    z[28]=static_cast<T>(7)+ 3*z[9];
    z[29]=z[9]*z[28];
    z[29]=z[29] + static_cast<T>(21)+ z[1];
    z[25]=n<T>(1,2)*z[29] + z[25];
    z[25]=z[25]*z[17];
    z[22]= - z[22]*z[14];
    z[15]=z[22] - n<T>(9,2) - z[15];
    z[15]=z[5]*z[15];
    z[15]=z[15] + z[25] + n<T>(7,4) + z[20];
    z[15]=z[15]*z[19];
    z[19]=z[9]*z[8];
    z[19]=static_cast<T>(2)+ n<T>(7,8)*z[19];
    z[19]=z[9]*z[19];
    z[15]=z[16] + z[15] + z[21] + n<T>(1,3)*z[19] - z[26] - n<T>(1,2)*z[11] - n<T>(3,4)*z[12] + n<T>(1,4)*z[10] - n<T>(2,3)*z[4];
    z[15]=z[2]*z[15];
    z[16]=z[8]*z[13];
    z[19]= - z[13] + z[16] - 1;
    z[19]=z[19]*z[27];
    z[20]=z[24]*z[13];
    z[21]=n<T>(1,2)*z[13];
    z[19]=z[19] + z[20] + z[21] + 1;
    z[14]=z[19]*z[14];
    z[20]=n<T>(1,2) + z[7];
    z[20]=z[13]*z[20];
    z[14]=z[14] - n<T>(3,4)*z[16] + z[20] + static_cast<T>(1)+ z[23];
    z[14]=z[5]*z[14];
    z[16]=z[19]*z[18];
    z[18]=z[8]*z[21];
    z[19]= - z[9]*z[13]*z[28];
    z[16]=z[16] + n<T>(1,4)*z[19] + z[18] - n<T>(5,4) - z[13];
    z[16]=z[16]*z[17];
    z[14]=z[14] - z[13] + z[16];
    z[14]=z[5]*z[14];

    r += z[14] + z[15];
 
    return r;
}

template double qg_2lha_r2156(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2156(const std::array<dd_real,30>&);
#endif
