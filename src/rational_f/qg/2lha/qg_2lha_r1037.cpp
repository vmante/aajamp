#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1037(const std::array<T,30>& k) {
  T z[27];
  T r = 0;

    z[1]=k[3];
    z[2]=k[9];
    z[3]=k[12];
    z[4]=k[4];
    z[5]=k[6];
    z[6]=k[10];
    z[7]=k[11];
    z[8]=k[27];
    z[9]=n<T>(1,4)*z[6];
    z[10]=n<T>(1,3)*z[6];
    z[11]=z[10] + 1;
    z[9]=z[9]*z[11];
    z[12]=n<T>(1,3)*z[3];
    z[13]= - n<T>(1,2) - z[6];
    z[13]=z[13]*z[12];
    z[13]=z[9] + z[13];
    z[14]=npow(z[3],2);
    z[13]=z[13]*z[14];
    z[15]=z[3]*z[6];
    z[16]=z[6] + 1;
    z[17]=z[15]*z[16];
    z[10]=z[10] + n<T>(1,4);
    z[10]=z[10]*z[6];
    z[10]= - z[10] + n<T>(1,6)*z[17];
    z[10]=z[10]*z[3];
    z[18]=n<T>(1,2)*z[6];
    z[19]=z[15] - z[18];
    z[19]=z[19]*z[16];
    z[20]=z[18] + 1;
    z[21]=z[20]*z[6];
    z[21]=z[21] + n<T>(1,2);
    z[22]=z[21]*z[8];
    z[23]= - n<T>(1,2)*z[19] - z[22];
    z[24]=n<T>(1,3)*z[8];
    z[23]=z[23]*z[24];
    z[23]= - z[10] + z[23];
    z[25]=n<T>(1,2)*z[8];
    z[23]=z[23]*z[25];
    z[26]=n<T>(1,2) - z[6];
    z[26]=z[26]*z[14];
    z[17]= - z[17] - z[22];
    z[17]=z[8]*z[17];
    z[17]=z[26] + z[17];
    z[17]=z[2]*z[17];
    z[13]=n<T>(1,12)*z[17] + z[13] + z[23];
    z[13]=z[2]*z[13];
    z[17]=n<T>(1,6)*z[6];
    z[17]= - z[16]*z[17];
    z[11]=z[11]*z[18];
    z[15]=n<T>(1,3)*z[15];
    z[11]=z[11] - z[15];
    z[11]=z[3]*z[11];
    z[11]=z[17] + z[11];
    z[17]=n<T>(1,2)*z[3];
    z[23]=static_cast<T>(5)+ z[6];
    z[23]=n<T>(1,2)*z[23] - z[3];
    z[17]=z[23]*z[17];
    z[17]=z[17] - z[20];
    z[17]=z[3]*z[17];
    z[17]=n<T>(1,4)*z[16] + z[17];
    z[20]= - static_cast<T>(1)+ n<T>(1,4)*z[3];
    z[12]=z[20]*z[12];
    z[12]=n<T>(1,4) + z[12];
    z[20]=n<T>(1,2)*z[7];
    z[12]=z[12]*z[20];
    z[12]=n<T>(1,3)*z[17] + z[12];
    z[12]=z[7]*z[12];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[5]*z[11];
    z[12]= - z[3]*z[18];
    z[12]=z[6] + z[12];
    z[12]=z[3]*z[12];
    z[12]= - z[18] + z[12];
    z[12]=z[16]*z[12];
    z[17]= - z[3] + 1;
    z[17]=z[18]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[20];
    z[12]=z[17] + z[12];
    z[12]=z[7]*z[12];
    z[9]= - z[9] + z[15];
    z[9]=z[9]*z[14];
    z[9]=z[11] + z[9] + n<T>(1,6)*z[12];
    z[9]=z[5]*z[9];
    z[11]=z[7]*z[6]*z[16];
    z[10]=z[10] + n<T>(1,12)*z[11];
    z[10]=z[7]*z[10];
    z[11]=z[21]*z[7];
    z[11]= - z[11] + z[19];
    z[11]=z[11]*z[20];
    z[12]=z[7]*z[22];
    z[11]=z[11] + z[12];
    z[11]=z[11]*z[24];
    z[10]=z[10] + z[11];
    z[10]=z[10]*z[25];
    z[11]=npow(z[6],2);
    z[12]=z[18] - z[7];
    z[12]=z[7]*z[12];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[4]*z[11]*npow(z[5],2);
    z[12]=z[1]*npow(z[3],3)*npow(z[2],2);

    r += z[9] + z[10] + n<T>(1,12)*z[11] - n<T>(1,6)*z[12] + z[13];
 
    return r;
}

template double qg_2lha_r1037(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1037(const std::array<dd_real,30>&);
#endif
