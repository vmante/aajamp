#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1903(const std::array<T,30>& k) {
  T z[19];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[6];
    z[6]=k[21];
    z[7]=k[9];
    z[8]=k[13];
    z[9]=k[4];
    z[10]=k[12];
    z[11]=k[28];
    z[12]= - z[3] + z[5];
    z[13]=z[4] - 1;
    z[12]=z[6]*z[13]*z[4]*z[12];
    z[14]=z[8] - z[5];
    z[14]=z[7]*z[13]*z[14];
    z[15]=z[10]*z[9];
    z[16]=static_cast<T>(1)+ z[9];
    z[16]=z[16]*z[15];
    z[17]=z[9]*z[8];
    z[18]= - z[8] - z[17];
    z[18]=z[9]*z[18];
    z[16]=z[18] + z[16];
    z[16]=z[11]*z[16];
    z[12]=z[17] + z[12] + z[14] + z[16];
    z[14]=n<T>(7,4)*z[3] - n<T>(3,4)*z[8];
    z[13]=z[13]*z[14];
    z[14]=static_cast<T>(1)- z[15];
    z[14]=z[2]*z[14];
    z[16]=static_cast<T>(1)- z[2];
    z[16]=z[1]*z[3]*z[16];
    z[12]=z[16] + z[14] + n<T>(1,4)*z[15] - static_cast<T>(1)+ z[13] + n<T>(3,4)*z[12];

    r += n<T>(1,2)*z[12];
 
    return r;
}

template double qg_2lha_r1903(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1903(const std::array<dd_real,30>&);
#endif
