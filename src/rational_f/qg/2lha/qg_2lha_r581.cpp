#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r581(const std::array<T,30>& k) {
  T z[5];
  T r = 0;

    z[1]=k[4];
    z[2]=k[6];
    z[3]=k[9];
    z[4]= - static_cast<T>(1)- n<T>(1,2)*z[1];
    z[4]=z[1]*z[4];
    z[4]= - n<T>(1,2) + z[4];

    r += n<T>(1,4)*z[4]*npow(z[3],2)*z[2]*z[1];
 
    return r;
}

template double qg_2lha_r581(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r581(const std::array<dd_real,30>&);
#endif
