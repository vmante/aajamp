#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r954(const std::array<T,30>& k) {
  T z[43];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[5];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[4];
    z[9]=k[11];
    z[10]=k[3];
    z[11]=k[10];
    z[12]=k[23];
    z[13]=k[15];
    z[14]=k[12];
    z[15]=k[13];
    z[16]=k[27];
    z[17]=k[24];
    z[18]=k[20];
    z[19]=z[9] - 1;
    z[19]=z[19]*z[9];
    z[20]=n<T>(37,4)*z[15] + n<T>(17,6) - z[16];
    z[20]=n<T>(3,8)*z[19] + n<T>(1,3)*z[20] - n<T>(31,16)*z[14];
    z[21]=n<T>(1,4)*z[5];
    z[22]= - n<T>(203,8)*z[15] + n<T>(299,12) - z[16];
    z[22]= - n<T>(1,12)*z[4] + n<T>(1,3)*z[22] - n<T>(59,8)*z[14];
    z[22]=z[22]*z[21];
    z[23]=n<T>(2,3)*z[4];
    z[20]=z[22] - z[2] + n<T>(1,4)*z[20] - z[23];
    z[20]=z[5]*z[20];
    z[22]=npow(z[14],2);
    z[24]=npow(z[9],2);
    z[25]=z[24] + n<T>(29,2)*z[22];
    z[26]=11*z[4] + 65*z[15] - z[25];
    z[26]=z[5]*z[26];
    z[25]=z[26] - z[25];
    z[26]=z[8]*z[5];
    z[25]=z[25]*z[26];
    z[27]=n<T>(1,3)*z[7];
    z[28]= - n<T>(13,2)*z[4] - n<T>(17,3) - n<T>(37,2)*z[15];
    z[28]=n<T>(1,2)*z[28] + z[7];
    z[28]=z[28]*z[27];
    z[24]=n<T>(1,8)*z[24];
    z[28]=z[24] + z[28];
    z[29]=z[2]*z[7];
    z[20]=n<T>(1,32)*z[25] + z[20] + n<T>(1,4)*z[28] + z[29];
    z[20]=z[8]*z[20];
    z[25]=n<T>(1,4)*z[7];
    z[28]=z[25] + 1;
    z[28]=z[28]*z[29];
    z[30]=n<T>(89,6) - 7*z[7];
    z[30]=z[30]*z[27];
    z[30]=z[24] + z[30];
    z[30]=n<T>(1,4)*z[30] - z[28];
    z[30]=z[2]*z[30];
    z[31]=n<T>(1,8) + z[2];
    z[31]=z[2]*z[31];
    z[31]=n<T>(247,144) + z[31];
    z[21]=z[31]*z[21];
    z[31]=n<T>(581,12) + z[4];
    z[32]= - n<T>(89,72) + z[2];
    z[32]=z[2]*z[32];
    z[21]=z[21] + n<T>(1,24)*z[31] + z[32];
    z[21]=z[5]*z[21];
    z[31]=5*z[4];
    z[32]= - z[19] + z[31];
    z[33]= - n<T>(11,3) - 13*z[4];
    z[33]=n<T>(1,8)*z[33] + z[7];
    z[33]=z[33]*z[27];
    z[20]=z[20] + z[21] + z[30] + n<T>(1,16)*z[32] + z[33];
    z[20]=z[8]*z[20];
    z[21]=static_cast<T>(1)- z[1];
    z[21]=z[21]*z[31];
    z[21]=z[21] - static_cast<T>(11)+ 15*z[1];
    z[30]=3*z[7];
    z[32]=static_cast<T>(17)- z[31];
    z[32]=n<T>(1,2)*z[32] - z[30];
    z[32]=z[7]*z[32];
    z[21]=n<T>(1,2)*z[21] + z[32];
    z[32]=5*z[1];
    z[33]=static_cast<T>(7)+ z[32];
    z[34]=n<T>(11,2) - 9*z[7];
    z[34]=z[7]*z[34];
    z[33]=n<T>(1,2)*z[33] + z[34];
    z[34]=n<T>(1,2)*z[7];
    z[35]= - static_cast<T>(1)+ z[30];
    z[35]=z[35]*z[34];
    z[35]= - static_cast<T>(1)+ z[35];
    z[35]=z[2]*z[35];
    z[33]=n<T>(1,2)*z[33] + z[35];
    z[33]=z[2]*z[33];
    z[35]=z[32] - 1;
    z[36]= - n<T>(7,2) + z[30];
    z[36]=z[7]*z[36];
    z[36]= - n<T>(1,2)*z[35] + z[36];
    z[33]=n<T>(3,2)*z[36] + z[33];
    z[33]=z[2]*z[33];
    z[21]=n<T>(1,2)*z[21] + z[33];
    z[33]= - z[30] - static_cast<T>(3)- z[31];
    z[36]=n<T>(1,8)*z[7];
    z[33]=z[33]*z[36];
    z[37]=static_cast<T>(1)+ n<T>(3,8)*z[7];
    z[37]=z[37]*z[29];
    z[38]=z[2] - 1;
    z[39]=z[5]*z[38];
    z[33]= - n<T>(5,8)*z[39] + z[33] + z[37];
    z[37]= - z[5] + 1;
    z[37]=z[5]*z[37];
    z[37]= - z[7] + z[37];
    z[39]=n<T>(1,4)*z[8];
    z[40]=z[31] + 11*z[15];
    z[37]=z[39]*z[40]*z[37];
    z[41]=z[31] + n<T>(17,2);
    z[42]= - n<T>(3,2)*z[7] - z[41];
    z[42]=z[7]*z[42];
    z[41]= - n<T>(5,2)*z[5] + z[41];
    z[41]=z[5]*z[41];
    z[37]=z[37] + z[42] + z[41];
    z[37]=z[37]*z[39];
    z[33]=3*z[33] + z[37];
    z[33]=z[8]*z[33];
    z[37]= - n<T>(9,2)*z[7] + n<T>(11,2) - z[31];
    z[37]=z[7]*z[37];
    z[31]=z[37] - n<T>(17,2) + z[31];
    z[37]=z[7] + 1;
    z[39]= - z[37]*z[30];
    z[39]=static_cast<T>(1)+ z[39];
    z[39]=z[2]*z[39];
    z[41]=static_cast<T>(1)+ n<T>(9,4)*z[7];
    z[41]=z[7]*z[41];
    z[39]=n<T>(3,8)*z[39] + n<T>(1,2) + z[41];
    z[39]=z[2]*z[39];
    z[31]=z[33] + n<T>(1,4)*z[31] + z[39];
    z[31]=z[8]*z[31];
    z[21]=n<T>(1,4)*z[21] + z[31];
    z[21]=z[3]*z[21];
    z[31]= - n<T>(9,2) + z[32];
    z[31]=z[4]*z[31];
    z[33]=19*z[4];
    z[39]= - n<T>(109,3) + z[33];
    z[39]=n<T>(1,4)*z[39] + n<T>(7,3)*z[7];
    z[39]=z[7]*z[39];
    z[31]=z[39] + n<T>(1,2)*z[31] - z[35];
    z[35]=z[9] - n<T>(19,2);
    z[32]=z[32] - z[35];
    z[39]=n<T>(53,8) - z[7];
    z[39]=z[39]*z[27];
    z[32]=n<T>(1,4)*z[32] + z[39];
    z[39]= - n<T>(1,4) - 17*z[7];
    z[39]=z[39]*z[27];
    z[41]= - static_cast<T>(7)+ z[9];
    z[39]=n<T>(1,2)*z[41] + z[39];
    z[41]=npow(z[7],2);
    z[42]=z[2]*z[41];
    z[39]=n<T>(1,4)*z[39] + z[42];
    z[39]=z[2]*z[39];
    z[32]=n<T>(1,2)*z[32] + z[39];
    z[32]=z[2]*z[32];
    z[31]=n<T>(1,4)*z[31] + z[32];
    z[32]=43*z[15];
    z[33]= - z[32] + z[33];
    z[33]=n<T>(1,4)*z[33] + z[30];
    z[33]=z[33]*z[36];
    z[32]=z[32] - 29*z[14];
    z[32]= - n<T>(19,2)*z[4] + n<T>(1,2)*z[32] - z[9];
    z[36]= - n<T>(11,32)*z[4] - n<T>(1,16)*z[9] - n<T>(29,32)*z[14] + static_cast<T>(1)- n<T>(65,32)*
    z[15];
    z[36]=z[5]*z[36];
    z[32]=n<T>(1,16)*z[32] + z[36];
    z[32]=z[5]*z[32];
    z[36]=z[8]*z[40]*npow(z[5],2);
    z[32]=n<T>(1,16)*z[36] + z[33] + z[32];
    z[32]=z[8]*z[32];
    z[25]=static_cast<T>(1)- z[25];
    z[25]=z[25]*z[29];
    z[33]=n<T>(33,2)*z[4] + n<T>(371,6) + 3*z[9];
    z[36]= - n<T>(15,4) - z[2];
    z[36]=z[5]*z[36];
    z[33]=n<T>(1,8)*z[36] + n<T>(1,16)*z[33] - z[2];
    z[33]=z[5]*z[33];
    z[36]=n<T>(25,12)*z[7] - n<T>(17,3) + n<T>(57,16)*z[4];
    z[36]=z[7]*z[36];
    z[36]=n<T>(1,8)*z[9] + z[36];
    z[25]=z[32] + z[33] + n<T>(1,2)*z[36] + z[25];
    z[25]=z[8]*z[25];
    z[32]= - static_cast<T>(1)- n<T>(5,8)*z[7];
    z[32]=z[32]*z[29];
    z[33]=static_cast<T>(3)+ n<T>(1,2)*z[9];
    z[36]=n<T>(83,32) - z[27];
    z[36]=z[7]*z[36];
    z[32]=z[32] + n<T>(1,8)*z[33] + z[36];
    z[32]=z[2]*z[32];
    z[33]= - static_cast<T>(53)+ n<T>(57,2)*z[4];
    z[33]=n<T>(1,2)*z[33] + n<T>(23,3)*z[7];
    z[33]=z[7]*z[33];
    z[33]=z[33] - n<T>(25,2)*z[4] - z[35];
    z[35]= - n<T>(31,16) + z[2];
    z[35]=z[2]*z[35];
    z[35]=n<T>(15,16) + z[35];
    z[35]=z[5]*z[35];
    z[25]=z[25] + z[35] + n<T>(1,8)*z[33] + z[32];
    z[25]=z[8]*z[25];
    z[21]=z[21] + n<T>(1,2)*z[31] + z[25];
    z[21]=z[3]*z[21];
    z[25]=z[37]*z[29];
    z[31]= - static_cast<T>(3)+ z[7];
    z[31]=z[7]*z[31];
    z[24]=z[25] + z[24] + z[31];
    z[24]=z[2]*z[24];
    z[25]=n<T>(1,2)*z[19];
    z[31]= - static_cast<T>(3)- z[25];
    z[30]=n<T>(65,24) - z[30];
    z[30]=z[7]*z[30];
    z[24]=z[24] + n<T>(1,4)*z[31] + z[30];
    z[24]=z[2]*z[24];
    z[30]= - z[4] + static_cast<T>(7)+ z[9];
    z[31]=n<T>(1,2) - n<T>(13,3)*z[4];
    z[31]=n<T>(1,4)*z[31] + z[7];
    z[31]=z[7]*z[31];
    z[32]=static_cast<T>(3)- z[2];
    z[32]=z[2]*z[32];
    z[32]= - n<T>(41,144) + z[32];
    z[32]=z[2]*z[32];
    z[32]= - n<T>(247,144) + z[32];
    z[32]=z[5]*z[32];
    z[24]=z[32] + z[24] + n<T>(1,8)*z[30] + z[31];
    z[20]=z[21] + n<T>(1,4)*z[24] + z[20];
    z[20]=z[3]*z[20];
    z[21]=npow(z[2],2);
    z[24]=z[7] - 1;
    z[30]=z[24]*z[21]*z[34];
    z[31]= - n<T>(5,9) + 27*z[4];
    z[32]=z[38]*z[2];
    z[33]=n<T>(247,144) - z[32];
    z[33]=z[2]*z[33];
    z[33]=n<T>(121,144) + z[33];
    z[33]=z[5]*z[33];
    z[21]=z[33] + n<T>(1,16)*z[31] + z[21];
    z[31]=n<T>(1,2)*z[5];
    z[21]=z[21]*z[31];
    z[33]= - z[27] + static_cast<T>(1)- n<T>(83,96)*z[4];
    z[33]=z[7]*z[33];
    z[19]=z[21] + z[30] + n<T>(1,16)*z[19] + z[33];
    z[21]=n<T>(1,3)*z[4];
    z[30]=n<T>(17,3) + n<T>(203,2)*z[15];
    z[33]=n<T>(29,4) - 13*z[14];
    z[33]=z[14]*z[33];
    z[30]=z[21] + z[25] + n<T>(1,3)*z[30] + z[33];
    z[30]=z[5]*z[30];
    z[30]=z[30] + n<T>(17,9) - n<T>(23,4)*z[22];
    z[30]= - z[2] + n<T>(1,16)*z[30];
    z[30]=z[5]*z[30];
    z[30]=z[30] - n<T>(17,144)*z[7] + z[29];
    z[30]=z[8]*z[30];
    z[33]=n<T>(5,3) + n<T>(7,8)*z[15];
    z[25]=n<T>(77,24)*z[4] - z[25] + n<T>(1,3)*z[33] - z[14];
    z[35]=n<T>(1,2)*z[2];
    z[36]= - n<T>(17,18) + z[2];
    z[36]=z[36]*z[35];
    z[37]= - n<T>(155,32)*z[15] + n<T>(665,96) - z[16];
    z[36]=z[36] - n<T>(27,32)*z[4] + n<T>(1,3)*z[37] - n<T>(35,16)*z[14];
    z[36]=z[36]*z[31];
    z[25]=z[36] + n<T>(1,8)*z[25] + z[32];
    z[25]=z[5]*z[25];
    z[28]=z[28] - z[7];
    z[28]=z[28]*z[2];
    z[32]= - n<T>(83,8)*z[4] - z[33];
    z[32]=n<T>(1,4)*z[32] - z[7];
    z[32]=z[7]*z[32];
    z[25]=z[30] + z[25] + n<T>(1,6)*z[32] - z[28];
    z[25]=z[8]*z[25];
    z[19]=z[20] + n<T>(1,2)*z[19] + z[25];
    z[19]=z[3]*z[19];
    z[20]=z[12] - n<T>(19,36);
    z[25]=z[20] + z[29];
    z[30]=n<T>(1,3)*z[11];
    z[32]=static_cast<T>(1)- z[34];
    z[32]=z[7]*z[32];
    z[25]=n<T>(1,18)*z[32] - z[30] + n<T>(1,2)*z[25];
    z[25]=z[25]*z[35];
    z[32]=n<T>(155,16)*z[15] - n<T>(7,24) + z[16];
    z[33]=n<T>(47,32) - z[17];
    z[33]=z[11]*z[33];
    z[32]=n<T>(37,96)*z[10] + n<T>(1,4)*z[32] + z[33];
    z[33]=n<T>(17,64) - z[17];
    z[33]=z[33]*z[21];
    z[36]=n<T>(23,2) - 3*z[14];
    z[36]=z[14]*z[36];
    z[25]=z[25] + z[33] + n<T>(1,3)*z[32] + n<T>(1,32)*z[36];
    z[25]=z[5]*z[25];
    z[32]=z[9]*z[18];
    z[33]=z[18] - 1;
    z[33]=z[33]*z[18];
    z[32]=z[32] + z[33];
    z[32]=z[32]*z[9];
    z[36]= - n<T>(1,2) + 2*z[17];
    z[36]=n<T>(1,3)*z[36] - n<T>(1,16)*z[33];
    z[36]=z[11]*z[36];
    z[23]=z[17]*z[23];
    z[22]=z[25] + z[28] + z[23] + n<T>(1,16)*z[32] + n<T>(17,64)*z[22] + n<T>(1,4)*
    z[12] + z[36];
    z[22]=z[5]*z[22];
    z[21]=z[21]*z[17];
    z[23]=z[21] + n<T>(1,32)*z[32];
    z[25]=n<T>(1,32)*z[33];
    z[28]=n<T>(1,2) - z[17];
    z[28]=n<T>(1,3)*z[28] + z[25];
    z[28]=z[11]*z[28];
    z[24]=n<T>(1,72)*z[24];
    z[32]= - z[29]*z[24];
    z[20]=z[32] - n<T>(1,4)*z[20] + z[28] - z[23] - n<T>(1,72)*z[7];
    z[20]=z[5]*z[20];
    z[24]=z[7]*z[24];
    z[20]=z[20] + z[24] - z[29];
    z[20]=z[5]*z[20];
    z[24]= - z[5]*z[7];
    z[24]=z[41] + z[24];
    z[24]=z[24]*z[26];
    z[20]=z[20] + n<T>(1,72)*z[24];
    z[20]=z[8]*z[20];
    z[20]=z[22] + z[20];
    z[20]=z[8]*z[20];
    z[22]=z[13] - 1;
    z[24]=z[22]*z[6];
    z[26]= - n<T>(17,24) + z[16];
    z[28]=z[13]*z[24];
    z[28]=static_cast<T>(3)+ n<T>(11,2)*z[28];
    z[28]=z[10]*z[28];
    z[24]=n<T>(1,8)*z[28] + n<T>(7,48)*z[15] + n<T>(1,3)*z[26] - n<T>(1,2)*z[24];
    z[24]=z[10]*z[24];
    z[26]=z[10]*z[13];
    z[22]=z[22]*z[26];
    z[22]= - n<T>(11,8)*z[22] - n<T>(17,8) + z[13];
    z[22]=z[10]*z[22];
    z[28]=z[14]*z[10];
    z[26]=static_cast<T>(7)- n<T>(11,4)*z[26];
    z[26]=z[26]*z[28];
    z[22]=n<T>(1,2)*z[26] - n<T>(9,4) + z[22];
    z[22]=z[14]*z[22];
    z[26]= - n<T>(1,8)*z[11] - n<T>(7,16)*z[15] - n<T>(13,16) - z[16];
    z[22]=n<T>(1,2)*z[22] + n<T>(1,3)*z[26] + z[24];
    z[24]=n<T>(37,2) + 5*z[6];
    z[24]=n<T>(1,9)*z[24];
    z[26]=z[10]*z[6];
    z[32]= - z[24] - n<T>(3,2)*z[26];
    z[32]=z[10]*z[32];
    z[32]=n<T>(1,4)*z[32] + n<T>(1,24)*z[11] + n<T>(127,144)*z[6] - n<T>(5,12) - z[12];
    z[33]=n<T>(1,3)*z[6];
    z[36]= - n<T>(1,2) - z[33];
    z[36]=z[7]*z[36];
    z[36]= - n<T>(1,2) + z[36];
    z[36]=z[36]*z[29];
    z[34]=z[34] + z[36];
    z[34]=z[2]*z[34];
    z[32]=n<T>(1,2)*z[32] + z[34];
    z[32]=z[2]*z[32];
    z[22]=n<T>(1,2)*z[22] + z[32];
    z[22]=z[22]*z[31];
    z[24]=z[24] + 3*z[26];
    z[26]= - n<T>(1,4) - z[33];
    z[26]=z[7]*z[26];
    z[26]=n<T>(1,4) + z[26];
    z[26]=z[26]*z[29];
    z[24]=n<T>(1,16)*z[24] + z[26];
    z[24]=z[2]*z[24];
    z[26]= - n<T>(31,32) + z[17];
    z[26]=z[26]*z[30];
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=z[14]*z[28];
    z[21]=z[22] + z[24] + z[21] + n<T>(11,64)*z[28] - n<T>(3,32)*z[10] + n<T>(3,32)
    + z[26];
    z[21]=z[5]*z[21];
    z[22]= - z[41]*z[33];
    z[22]= - n<T>(3,16)*z[6] + z[22];
    z[22]=z[22]*z[35];
    z[24]= - n<T>(1,3)*z[17] + z[25];
    z[24]=z[11]*z[24];
    z[25]=z[4]*z[27];

    r += z[19] + z[20] + z[21] + z[22] - z[23] + z[24] + z[25];
 
    return r;
}

template double qg_2lha_r954(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r954(const std::array<dd_real,30>&);
#endif
