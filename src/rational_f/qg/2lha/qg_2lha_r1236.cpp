#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1236(const std::array<T,30>& k) {
  T z[99];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[27];
    z[6]=k[13];
    z[7]=k[28];
    z[8]=k[6];
    z[9]=k[8];
    z[10]=k[3];
    z[11]=k[2];
    z[12]=k[11];
    z[13]=k[14];
    z[14]=k[9];
    z[15]=k[10];
    z[16]=k[5];
    z[17]=k[18];
    z[18]=z[8] - 3;
    z[19]=n<T>(1,2)*z[8];
    z[20]=z[18]*z[19];
    z[21]=z[8] - 1;
    z[22]=z[21]*z[8];
    z[23]=npow(z[8],2);
    z[24]=z[23]*z[2];
    z[25]=n<T>(3,2)*z[8] - z[24];
    z[25]=z[2]*z[25];
    z[25]= - n<T>(3,2)*z[22] + z[25];
    z[26]=n<T>(1,2)*z[2];
    z[25]=z[25]*z[26];
    z[27]=z[19] - 1;
    z[28]=z[27]*z[8];
    z[25]=z[28] + z[25];
    z[25]=z[2]*z[25];
    z[25]= - z[20] + z[25];
    z[29]=z[26] - 1;
    z[30]=z[29]*z[26];
    z[31]=n<T>(1,4)*z[8];
    z[32]=z[31] - 1;
    z[33]=z[32]*z[8];
    z[30]=z[33] + z[30] + n<T>(3,4);
    z[34]=z[30]*z[4];
    z[35]=npow(z[2],2);
    z[36]=z[35]*z[10];
    z[25]=n<T>(1,8)*z[36] + n<T>(1,2)*z[25] + z[34];
    z[37]=z[23]*z[26];
    z[38]=z[8] - n<T>(3,2);
    z[38]=z[38]*z[8];
    z[38]=z[38] + z[37];
    z[38]=z[38]*z[26];
    z[33]=z[38] + z[33] + n<T>(1,2);
    z[33]=z[33]*z[2];
    z[33]=z[33] - n<T>(1,4)*z[21];
    z[33]=z[33]*z[35];
    z[38]= - n<T>(1,2)*z[35] + z[36];
    z[39]=n<T>(1,2)*z[10];
    z[38]=z[38]*z[39];
    z[38]= - z[33] + z[38];
    z[40]=n<T>(1,2)*z[14];
    z[38]=z[38]*z[40];
    z[25]=3*z[25] + z[38];
    z[38]=3*z[8];
    z[41]= - static_cast<T>(7)+ z[38];
    z[41]=z[41]*z[31];
    z[42]=z[8] - n<T>(1,2);
    z[43]=z[42]*z[8];
    z[44]=n<T>(1,4) - z[43];
    z[44]=z[2]*z[44];
    z[41]=z[41] + z[44];
    z[41]=z[2]*z[41];
    z[18]=z[18]*z[8];
    z[41]= - z[18] + z[41];
    z[18]=z[18] + z[2];
    z[18]= - z[34] + n<T>(1,4)*z[18];
    z[44]=n<T>(1,2)*z[1];
    z[45]= - z[18]*z[44];
    z[46]=z[10]*z[2];
    z[41]=z[45] + n<T>(1,16)*z[46] + n<T>(1,4)*z[41] + z[34];
    z[41]=z[1]*z[41];
    z[25]=n<T>(1,2)*z[25] + 3*z[41];
    z[41]=3*z[5];
    z[25]=z[25]*z[41];
    z[45]=n<T>(1,16)*z[8];
    z[47]=static_cast<T>(143)- 45*z[8];
    z[47]=z[47]*z[45];
    z[48]=n<T>(19,8) - z[2];
    z[48]=z[48]*z[26];
    z[47]=n<T>(17,4)*z[34] + z[48] - static_cast<T>(4)+ z[47];
    z[47]=z[4]*z[47];
    z[48]=11*z[8];
    z[49]= - static_cast<T>(13)+ z[48];
    z[49]=z[8]*z[49];
    z[49]=z[49] + 5*z[24];
    z[49]=z[49]*z[26];
    z[50]= - n<T>(17,2) + z[38];
    z[50]=z[8]*z[50];
    z[49]=z[49] + n<T>(5,2) + z[50];
    z[49]=z[2]*z[49];
    z[49]= - n<T>(3,2)*z[21] + z[49];
    z[49]=z[2]*z[49];
    z[50]=n<T>(3,2)*z[46];
    z[51]= - n<T>(23,3)*z[35] + z[50];
    z[51]=z[10]*z[51];
    z[52]=npow(z[10],2);
    z[53]=z[14]*z[35]*z[52];
    z[49]= - n<T>(3,2)*z[53] + z[49] + z[51];
    z[51]=n<T>(1,8)*z[14];
    z[49]=z[49]*z[51];
    z[53]=n<T>(1,4) - z[2];
    z[53]=z[53]*z[26];
    z[54]=static_cast<T>(97)- 31*z[8];
    z[54]=z[8]*z[54];
    z[53]=n<T>(17,2)*z[34] + z[53] - static_cast<T>(4)+ n<T>(1,8)*z[54];
    z[53]=z[4]*z[53];
    z[54]=z[1]*z[4];
    z[18]=z[18]*z[54];
    z[55]= - static_cast<T>(4)+ n<T>(7,4)*z[8];
    z[55]=z[55]*z[8];
    z[56]=static_cast<T>(1)+ n<T>(1,8)*z[43];
    z[56]=z[2]*z[56];
    z[18]= - n<T>(17,4)*z[18] + z[53] + z[55] + z[56];
    z[18]=z[1]*z[18];
    z[53]=13*z[8];
    z[56]=static_cast<T>(17)- z[53];
    z[56]=z[56]*z[31];
    z[56]=static_cast<T>(3)+ z[56];
    z[57]= - n<T>(35,8) + z[8];
    z[57]=z[8]*z[57];
    z[57]=n<T>(19,8)*z[24] - n<T>(19,16) + z[57];
    z[57]=z[2]*z[57];
    z[56]=n<T>(1,4)*z[56] + z[57];
    z[56]=z[56]*z[26];
    z[18]=z[25] + z[18] + z[49] + n<T>(3,32)*z[46] + z[47] + z[55] + z[56];
    z[18]=z[5]*z[18];
    z[25]=n<T>(5,3)*z[8];
    z[47]=static_cast<T>(13)- n<T>(25,2)*z[8];
    z[47]=z[47]*z[25];
    z[49]=n<T>(19,3)*z[8];
    z[55]= - n<T>(1,2) + z[49];
    z[55]=z[8]*z[55];
    z[55]= - n<T>(29,6)*z[24] + n<T>(25,12) + z[55];
    z[55]=z[2]*z[55];
    z[47]=z[47] + z[55];
    z[55]=n<T>(1,3)*z[8];
    z[32]=z[32]*z[55];
    z[56]=n<T>(1,6)*z[2];
    z[57]=z[56]*z[29];
    z[32]=z[57] + z[32] + n<T>(1,4);
    z[32]=z[32]*z[4];
    z[57]=35*z[32];
    z[58]=static_cast<T>(233)- n<T>(229,3)*z[8];
    z[58]=z[58]*z[19];
    z[58]= - static_cast<T>(55)+ z[58];
    z[59]=n<T>(101,12) - z[2];
    z[59]=z[2]*z[59];
    z[58]=n<T>(1,2)*z[58] + z[59];
    z[58]=n<T>(1,2)*z[58] + z[57];
    z[58]=z[4]*z[58];
    z[59]=n<T>(1,3)*z[2];
    z[60]=n<T>(7,8) + z[2];
    z[60]=z[60]*z[59];
    z[61]= - n<T>(195,4) + n<T>(71,3)*z[8];
    z[61]=z[8]*z[61];
    z[61]=n<T>(65,6) + z[61];
    z[58]=z[58] + n<T>(1,2)*z[61] + z[60];
    z[58]=z[4]*z[58];
    z[60]=static_cast<T>(5)- z[38];
    z[60]=z[60]*z[31];
    z[61]=n<T>(5,3) - z[8];
    z[61]=z[8]*z[61];
    z[61]=z[61] - n<T>(7,6)*z[24];
    z[61]=z[2]*z[61];
    z[60]=z[61] + static_cast<T>(3)+ z[60];
    z[60]=z[2]*z[60];
    z[61]=z[2] + n<T>(1,2);
    z[62]=n<T>(3,2)*z[10];
    z[63]=z[61]*z[62];
    z[60]=z[63] - n<T>(3,2) + z[60];
    z[63]=n<T>(3,4) - z[59];
    z[63]=z[14]*z[63]*z[46];
    z[60]=n<T>(1,2)*z[60] + z[63];
    z[60]=z[14]*z[60];
    z[47]=z[60] + n<T>(1,4)*z[47] + z[58];
    z[58]=static_cast<T>(155)- 51*z[8];
    z[58]=z[58]*z[31];
    z[60]=n<T>(19,6) - z[2];
    z[60]=z[60]*z[26];
    z[58]=z[60] - n<T>(43,3) + z[58];
    z[57]=n<T>(1,2)*z[58] + z[57];
    z[57]=z[4]*z[57];
    z[58]= - n<T>(139,2) + n<T>(107,3)*z[8];
    z[58]=z[8]*z[58];
    z[58]=n<T>(35,6)*z[2] + n<T>(25,6) + z[58];
    z[57]=n<T>(1,8)*z[58] + z[57];
    z[57]=z[4]*z[57];
    z[58]=n<T>(3,2)*z[2];
    z[60]=z[55] - 1;
    z[63]=z[60]*z[8];
    z[64]= - z[58] - n<T>(1,3) - n<T>(11,2)*z[63];
    z[32]=n<T>(1,4)*z[64] + 5*z[32];
    z[32]=z[4]*z[32];
    z[32]= - n<T>(1,24) + z[32];
    z[32]=z[32]*z[54];
    z[43]=n<T>(1,4) - n<T>(13,3)*z[43];
    z[32]=n<T>(7,2)*z[32] + n<T>(1,4)*z[43] + z[57];
    z[32]=z[1]*z[32];
    z[18]=z[18] + n<T>(1,2)*z[47] + z[32];
    z[18]=z[5]*z[18];
    z[32]=5*z[8];
    z[43]=static_cast<T>(7)- z[32];
    z[43]=z[43]*z[31];
    z[43]= - n<T>(29,3) + z[43];
    z[47]= - n<T>(1,3) - z[31];
    z[47]=z[8]*z[47];
    z[47]=n<T>(1,2) + z[47];
    z[47]=n<T>(1,2)*z[47] + z[24];
    z[47]=z[2]*z[47];
    z[43]=n<T>(1,6)*z[43] + z[47];
    z[47]=z[2] + 1;
    z[57]=n<T>(1,3)*z[4];
    z[64]=z[47]*z[57];
    z[65]=5*z[2];
    z[66]= - z[47]*z[65];
    z[66]= - static_cast<T>(1)+ z[66];
    z[66]=z[66]*z[59];
    z[66]=z[66] - static_cast<T>(1)- z[55];
    z[66]=n<T>(1,2)*z[66] + z[64];
    z[67]=n<T>(1,2)*z[4];
    z[66]=z[66]*z[67];
    z[68]=n<T>(5,3) - z[19];
    z[68]=z[8]*z[68];
    z[69]= - n<T>(25,2) + z[2];
    z[69]=z[2]*z[69];
    z[68]=z[69] - static_cast<T>(59)+ n<T>(7,2)*z[68];
    z[66]=n<T>(1,3)*z[68] + z[66];
    z[66]=z[66]*z[67];
    z[43]=n<T>(1,3)*z[43] + z[66];
    z[66]= - static_cast<T>(1)+ n<T>(19,4)*z[8];
    z[66]=z[66]*z[55];
    z[68]=z[2]*z[8];
    z[69]= - static_cast<T>(1)+ n<T>(29,12)*z[8];
    z[69]=z[69]*z[68];
    z[66]=z[66] + z[69];
    z[69]=7*z[8];
    z[70]=static_cast<T>(5)- z[69];
    z[70]=z[8]*z[70];
    z[70]= - n<T>(25,3) + z[70];
    z[71]= - n<T>(1217,3) + 131*z[2];
    z[71]=z[2]*z[71];
    z[71]= - static_cast<T>(379)+ z[71];
    z[71]=z[4]*z[71];
    z[71]=n<T>(1,24)*z[71] - n<T>(827,72)*z[2] - n<T>(31,9) + n<T>(5,8)*z[8];
    z[71]=z[4]*z[71];
    z[70]=n<T>(1,8)*z[70] + z[71];
    z[70]=z[4]*z[70];
    z[66]=n<T>(1,2)*z[66] + z[70];
    z[70]=n<T>(1,3)*z[23];
    z[71]= - z[70] - z[24];
    z[72]=static_cast<T>(241)- 43*z[2];
    z[73]=z[47]*z[4];
    z[74]=z[2]*z[73];
    z[72]=n<T>(1,16)*z[72] + 58*z[74];
    z[74]=n<T>(1,9)*z[4];
    z[72]=z[72]*z[74];
    z[75]=z[8] - n<T>(11,4);
    z[72]= - n<T>(1,4)*z[75] + z[72];
    z[72]=z[4]*z[72];
    z[72]=n<T>(17,48)*z[8] + z[72];
    z[72]=z[4]*z[72];
    z[71]=n<T>(1,4)*z[71] + z[72];
    z[71]=z[10]*z[71];
    z[66]=n<T>(1,2)*z[66] + z[71];
    z[71]=n<T>(1,3)*z[10];
    z[66]=z[66]*z[71];
    z[72]=z[55]*z[2];
    z[76]=n<T>(1,3) - z[31];
    z[76]=z[76]*z[72];
    z[77]=n<T>(13,18) - z[8];
    z[77]=z[8]*z[77];
    z[77]=n<T>(1,2) + z[77];
    z[76]=n<T>(1,4)*z[77] + z[76];
    z[77]=n<T>(1391,8) - 31*z[2];
    z[77]=z[77]*z[59];
    z[77]=n<T>(907,8) + z[77];
    z[77]=n<T>(1,3)*z[77] + n<T>(1,4)*z[73];
    z[77]=z[77]*z[67];
    z[78]= - static_cast<T>(17)+ z[69];
    z[78]=z[8]*z[78];
    z[78]=n<T>(13,2) + z[78];
    z[77]=z[77] + n<T>(1,8)*z[78] + n<T>(34,9)*z[2];
    z[77]=z[77]*z[57];
    z[66]=z[66] + n<T>(1,2)*z[76] + z[77];
    z[66]=z[10]*z[66];
    z[76]= - n<T>(5,6) + z[8];
    z[76]=z[8]*z[76];
    z[76]=z[76] - n<T>(7,12)*z[24];
    z[77]= - n<T>(13,3) - z[26];
    z[78]=static_cast<T>(103)+ 175*z[2];
    z[78]=z[2]*z[78];
    z[78]= - static_cast<T>(1)+ n<T>(1,72)*z[78];
    z[78]=z[4]*z[78];
    z[77]=n<T>(1,4)*z[77] + z[78];
    z[77]=z[4]*z[77];
    z[78]=n<T>(11,2)*z[8];
    z[79]= - n<T>(5,3) - z[78];
    z[77]=n<T>(1,8)*z[79] + z[77];
    z[77]=z[4]*z[77];
    z[79]=z[26] + 1;
    z[80]=z[79]*z[2];
    z[80]=z[80] + n<T>(1,2);
    z[81]=n<T>(29,3)*z[4];
    z[82]=z[80]*z[81];
    z[82]=n<T>(5,4)*z[47] + z[82];
    z[82]=z[4]*z[82];
    z[82]=static_cast<T>(1)+ z[82];
    z[82]=z[82]*z[57];
    z[82]=z[31] + z[82];
    z[82]=z[4]*z[82];
    z[83]=n<T>(1,12)*z[23];
    z[82]= - z[83] + z[82];
    z[82]=z[10]*z[82];
    z[76]=z[82] + n<T>(1,4)*z[76] + z[77];
    z[76]=z[76]*z[71];
    z[77]=n<T>(1,8)*z[68];
    z[82]= - static_cast<T>(7)+ 25*z[8];
    z[82]=z[82]*z[77];
    z[75]= - z[8]*z[75];
    z[75]=z[75] + z[82];
    z[82]=n<T>(1,36)*z[2];
    z[84]= - static_cast<T>(103)- n<T>(205,12)*z[2];
    z[84]=z[2]*z[84];
    z[84]= - n<T>(763,12) + z[84];
    z[84]=z[4]*z[84];
    z[84]=n<T>(1,36)*z[84] - z[82] + n<T>(1,9) + n<T>(3,16)*z[8];
    z[84]=z[4]*z[84];
    z[75]=z[76] + n<T>(1,18)*z[75] + z[84];
    z[75]=z[10]*z[75];
    z[76]=z[31]*z[2];
    z[84]=static_cast<T>(1)- n<T>(11,6)*z[8];
    z[84]=z[84]*z[76];
    z[85]=n<T>(551,9) - z[19];
    z[86]=n<T>(295,36) + z[2];
    z[86]=z[2]*z[86];
    z[85]=n<T>(1,4)*z[85] + z[86];
    z[85]=z[85]*z[57];
    z[86]= - n<T>(1,4) - z[55];
    z[86]=z[8]*z[86];
    z[86]=n<T>(1,8) + z[86];
    z[84]=z[85] + n<T>(1,3)*z[86] + z[84];
    z[75]=n<T>(1,2)*z[84] + z[75];
    z[75]=z[10]*z[75];
    z[84]= - n<T>(7,6) + z[8];
    z[84]=z[84]*z[19];
    z[84]= - n<T>(47,9) + z[84];
    z[85]=n<T>(1,6)*z[8];
    z[49]= - static_cast<T>(5)+ z[49];
    z[49]=z[49]*z[85];
    z[49]=z[49] + z[79];
    z[86]=n<T>(1,4)*z[2];
    z[49]=z[49]*z[86];
    z[87]=z[4]*z[8];
    z[49]= - n<T>(1,24)*z[87] + n<T>(1,3)*z[84] + z[49];
    z[49]=n<T>(1,2)*z[49] + z[75];
    z[49]=z[14]*z[49];
    z[43]=z[49] + n<T>(1,2)*z[43] + z[66];
    z[43]=z[14]*z[43];
    z[49]=n<T>(5,2)*z[8];
    z[66]= - static_cast<T>(1)+ z[49];
    z[66]=z[66]*z[32];
    z[75]= - z[23]*z[59];
    z[66]=z[66] + z[75];
    z[75]=n<T>(1,8)*z[2];
    z[66]=z[66]*z[75];
    z[84]=static_cast<T>(149)- n<T>(199,3)*z[8];
    z[84]=z[84]*z[19];
    z[88]=z[55]*z[4];
    z[89]= - static_cast<T>(199)+ n<T>(137,2)*z[8];
    z[89]=z[89]*z[88];
    z[84]=z[84] + z[89];
    z[84]=z[4]*z[84];
    z[89]=static_cast<T>(17)- n<T>(55,2)*z[8];
    z[89]=z[8]*z[89];
    z[89]=z[89] - 17*z[24];
    z[90]=z[23]*z[4];
    z[91]= - n<T>(335,3)*z[23] + 41*z[90];
    z[91]=z[4]*z[91];
    z[91]=89*z[23] + z[91];
    z[91]=z[91]*z[67];
    z[89]=n<T>(1,3)*z[89] + z[91];
    z[91]=n<T>(1,8)*z[10];
    z[89]=z[89]*z[91];
    z[92]=static_cast<T>(2)+ n<T>(31,4)*z[28];
    z[66]=z[89] + n<T>(1,8)*z[84] + n<T>(1,3)*z[92] + z[66];
    z[66]=z[10]*z[66];
    z[84]=z[42]*z[68];
    z[89]=z[8] - 2;
    z[89]=z[89]*z[8];
    z[92]=z[84] + n<T>(3,2) + z[89];
    z[92]=z[2]*z[92];
    z[92]=z[92] - z[27];
    z[92]=z[2]*z[92];
    z[93]=z[38] - z[24];
    z[93]=z[2]*z[93];
    z[93]= - static_cast<T>(3)+ z[93];
    z[94]=z[26]*z[10];
    z[93]=z[93]*z[94];
    z[95]=z[26]*z[22];
    z[95]=z[95] - n<T>(5,8) + z[89];
    z[95]=z[95]*z[59];
    z[96]= - n<T>(7,6) + z[63];
    z[95]=n<T>(1,2)*z[96] + z[95];
    z[95]=z[2]*z[95];
    z[95]= - n<T>(13,24) + z[95];
    z[95]=z[14]*z[95];
    z[92]=z[95] + z[93] - n<T>(1,2) + z[92];
    z[92]=z[13]*z[92];
    z[93]= - static_cast<T>(1)+ z[57];
    z[93]=z[67]*z[23]*z[93];
    z[95]=z[8] + n<T>(1,2);
    z[96]=z[8]*z[95];
    z[96]=z[96] - z[37];
    z[93]=n<T>(1,3)*z[96] + z[93];
    z[93]=z[93]*z[39];
    z[96]=z[84] + n<T>(1,2) - z[22];
    z[97]=z[4]*z[28];
    z[96]=n<T>(1,2)*z[96] + z[97];
    z[93]=n<T>(1,3)*z[96] + z[93];
    z[93]=z[10]*z[93];
    z[96]=static_cast<T>(31)- z[53];
    z[96]=z[96]*z[55];
    z[96]=n<T>(19,2) + z[96];
    z[72]=z[72]*z[42];
    z[97]=static_cast<T>(1)- n<T>(2,3)*z[8];
    z[97]=z[8]*z[97];
    z[97]= - z[72] - n<T>(1,48) + z[97];
    z[97]=z[2]*z[97];
    z[96]=n<T>(1,12)*z[96] + z[97];
    z[96]=z[2]*z[96];
    z[80]=z[80]*z[51];
    z[97]=n<T>(169,8) - z[28];
    z[80]=z[80] + n<T>(1,18)*z[97] + z[96];
    z[80]=z[14]*z[80];
    z[96]=n<T>(1,4)*z[24];
    z[97]=n<T>(7,12) - z[8];
    z[97]=z[8]*z[97];
    z[97]=z[97] - z[96];
    z[97]=z[2]*z[97];
    z[53]=static_cast<T>(19)- z[53];
    z[53]=z[8]*z[53];
    z[53]= - static_cast<T>(5)+ z[53];
    z[53]=n<T>(1,12)*z[53] + z[97];
    z[53]=z[2]*z[53];
    z[53]=z[92] + z[80] + 13*z[93] + n<T>(19,12) + z[53];
    z[53]=z[13]*z[53];
    z[80]=n<T>(37,4) - z[48];
    z[80]=z[8]*z[80];
    z[80]=z[84] + static_cast<T>(1)+ z[80];
    z[80]=z[80]*z[59];
    z[92]=n<T>(19,3) - z[38];
    z[92]=z[8]*z[92];
    z[92]= - n<T>(35,3) + z[92];
    z[93]= - n<T>(7,3) + n<T>(3,4)*z[8];
    z[93]=z[8]*z[93];
    z[93]=n<T>(43,3) + z[93];
    z[93]=z[4]*z[93];
    z[80]=z[93] + n<T>(1,4)*z[92] + z[80];
    z[92]= - static_cast<T>(31)+ z[69];
    z[92]=z[92]*z[85];
    z[92]= - static_cast<T>(5)+ z[92];
    z[93]=z[21]*z[55];
    z[97]=n<T>(1,6)*z[24];
    z[93]=z[97] + n<T>(1,16) + z[93];
    z[93]=z[2]*z[93];
    z[92]=n<T>(1,24)*z[92] + z[93];
    z[92]=z[2]*z[92];
    z[93]= - n<T>(1,9)*z[22] - z[79];
    z[93]=z[93]*z[26];
    z[98]= - n<T>(7,4) - z[28];
    z[93]=n<T>(1,9)*z[98] + z[93];
    z[93]=z[93]*z[40];
    z[98]=n<T>(1,9)*z[28];
    z[92]=z[93] + z[92] - n<T>(1,8) + z[98];
    z[92]=z[14]*z[92];
    z[53]=z[53] + z[92] + n<T>(1,4)*z[80] + z[66];
    z[53]=z[13]*z[53];
    z[66]= - n<T>(65,3) + z[69];
    z[66]=z[66]*z[31];
    z[61]=z[61]*z[56];
    z[61]=z[61] + n<T>(5,3) + z[66];
    z[61]=n<T>(1,4)*z[61] - z[34];
    z[61]=z[4]*z[61];
    z[22]=static_cast<T>(11)+ z[22];
    z[22]=n<T>(1,3)*z[22] - z[2];
    z[22]=n<T>(1,16)*z[22] + z[61];
    z[22]=z[4]*z[22];
    z[61]=n<T>(23,16) - z[8];
    z[61]=z[61]*z[55];
    z[66]=n<T>(7,6)*z[2] - n<T>(1,3) + n<T>(5,2)*z[63];
    z[66]=n<T>(1,4)*z[66] - z[34];
    z[66]=z[4]*z[66];
    z[66]=n<T>(1,12)*z[28] + z[66];
    z[66]=z[1]*z[66]*z[67];
    z[22]=z[66] + z[22] - n<T>(1,8) + z[61];
    z[22]=z[1]*z[22];
    z[61]= - static_cast<T>(7)- z[78];
    z[61]=z[61]*z[19];
    z[61]= - static_cast<T>(5)+ z[61];
    z[66]= - static_cast<T>(19)+ 17*z[8];
    z[66]=z[66]*z[31];
    z[66]=static_cast<T>(1)+ z[66];
    z[66]=z[2]*z[66];
    z[61]=n<T>(1,3)*z[61] + z[66];
    z[30]=z[30]*z[67];
    z[66]= - static_cast<T>(1)+ z[59];
    z[66]=z[66]*z[75];
    z[75]= - n<T>(25,8) + z[8];
    z[75]=z[8]*z[75];
    z[75]=n<T>(11,8) + z[75];
    z[66]= - z[30] + n<T>(1,3)*z[75] + z[66];
    z[66]=z[4]*z[66];
    z[75]=n<T>(31,3) - z[38];
    z[75]=z[75]*z[19];
    z[58]=z[58] - n<T>(5,3) + z[75];
    z[58]=n<T>(1,8)*z[58] + z[66];
    z[58]=z[4]*z[58];
    z[22]=z[22] + n<T>(1,12)*z[61] + z[58];
    z[58]=n<T>(61,4) - z[69];
    z[58]=z[58]*z[55];
    z[58]= - n<T>(13,4) + z[58];
    z[61]=static_cast<T>(1)- z[25];
    z[61]=z[8]*z[61];
    z[61]=n<T>(1,2) + z[61];
    z[61]=z[61]*z[86];
    z[58]=n<T>(1,3)*z[58] + z[61];
    z[58]=z[58]*z[26];
    z[49]= - static_cast<T>(4)+ z[49];
    z[49]=z[49]*z[55];
    z[49]=n<T>(1,6)*z[84] + n<T>(9,16) + z[49];
    z[49]=z[2]*z[49];
    z[61]=n<T>(1,9)*z[8];
    z[66]= - static_cast<T>(31)+ n<T>(23,2)*z[8];
    z[66]=z[66]*z[61];
    z[66]=n<T>(13,4) + z[66];
    z[49]=n<T>(1,2)*z[66] + z[49];
    z[49]=z[2]*z[49];
    z[66]=z[21]*z[68];
    z[75]= - z[66] - n<T>(17,8) - 2*z[89];
    z[75]=z[75]*z[59];
    z[78]=z[63] + n<T>(13,12);
    z[75]=z[75] - z[78];
    z[75]=z[2]*z[75];
    z[75]= - n<T>(1,24) + z[75];
    z[75]=z[13]*z[75];
    z[80]=n<T>(13,8) - z[28];
    z[49]=z[75] + n<T>(1,18)*z[80] + z[49];
    z[49]=z[13]*z[49];
    z[49]=z[49] + z[58] + n<T>(3,16) + z[98];
    z[49]=z[13]*z[49];
    z[58]=z[60]*z[31];
    z[58]= - z[34] + n<T>(5,12)*z[2] - n<T>(1,3) + z[58];
    z[58]=z[4]*z[58];
    z[75]= - n<T>(5,3) - z[63];
    z[30]= - z[30] + n<T>(1,4)*z[75] + z[59];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(5,24) + z[30];
    z[30]=z[1]*z[30];
    z[75]=z[27]*z[55];
    z[30]=z[30] + z[75] + z[58];
    z[30]=z[1]*z[30];
    z[34]= - z[34] + z[56] + n<T>(1,6) + z[63];
    z[34]=z[4]*z[34];
    z[58]=static_cast<T>(13)- z[32];
    z[58]=z[8]*z[58];
    z[58]= - static_cast<T>(11)+ z[58];
    z[58]=z[2]*z[58];
    z[58]= - z[28] + z[58];
    z[34]=n<T>(1,6)*z[58] + z[34];
    z[30]=n<T>(1,2)*z[34] + z[30];
    z[34]=z[75] + n<T>(3,16);
    z[58]= - n<T>(1,12)*z[66] - z[34];
    z[58]=z[2]*z[58];
    z[75]= - n<T>(7,6) - z[63];
    z[58]=n<T>(1,4)*z[75] + z[58];
    z[58]=z[2]*z[58];
    z[58]= - n<T>(1,48) + z[58];
    z[58]=z[13]*z[58];
    z[34]=z[2]*z[34];
    z[34]=n<T>(1,2)*z[78] + z[34];
    z[34]=z[2]*z[34];
    z[34]=z[58] + n<T>(1,48) + z[34];
    z[34]=z[13]*z[34];
    z[30]=n<T>(1,4)*z[30] + z[34];
    z[30]=z[3]*z[30];
    z[22]=z[30] + n<T>(1,2)*z[22] + z[49];
    z[22]=z[3]*z[22];
    z[30]= - static_cast<T>(1)- n<T>(5,4)*z[8];
    z[30]=z[30]*z[69];
    z[34]=n<T>(17,2) + z[69];
    z[34]=z[8]*z[34];
    z[34]=z[34] - n<T>(17,4)*z[24];
    z[34]=z[2]*z[34];
    z[30]=z[34] - n<T>(17,4) + z[30];
    z[34]=z[23] - n<T>(1,6)*z[90];
    z[34]=z[4]*z[34];
    z[30]=n<T>(1,3)*z[30] + n<T>(7,2)*z[34];
    z[30]=z[10]*z[30];
    z[34]= - n<T>(19,9) - z[19];
    z[34]=z[34]*z[68];
    z[49]=n<T>(37,9) + z[19];
    z[49]=z[8]*z[49];
    z[34]=z[34] + n<T>(179,36) + z[49];
    z[34]=z[2]*z[34];
    z[49]= - n<T>(61,9) - z[8];
    z[49]=z[8]*z[49];
    z[49]= - n<T>(65,9) + z[49];
    z[34]=n<T>(1,2)*z[49] + z[34];
    z[49]=n<T>(13,9) + z[31];
    z[49]=z[49]*z[87];
    z[30]=n<T>(1,6)*z[30] + n<T>(1,2)*z[34] + z[49];
    z[30]=z[10]*z[30];
    z[34]=z[35] - z[36];
    z[36]=n<T>(1,4)*z[10];
    z[34]=z[34]*z[36];
    z[34]=z[33] + z[34];
    z[34]=z[34]*z[41];
    z[49]=n<T>(7,2) - z[8];
    z[49]=z[8]*z[49];
    z[49]=z[49] - z[24];
    z[49]=z[2]*z[49];
    z[58]= - n<T>(7,2) + z[32];
    z[49]=n<T>(1,2)*z[58] + z[49];
    z[49]=z[49]*z[35];
    z[58]= - z[2]*z[47];
    z[58]=z[58] + z[94];
    z[58]=z[58]*z[62];
    z[34]=z[34] + z[49] + z[58];
    z[49]=n<T>(1,2)*z[5];
    z[34]=z[34]*z[49];
    z[58]=z[24] - n<T>(83,9) - z[69];
    z[58]=z[2]*z[58];
    z[58]=n<T>(55,3) + z[58];
    z[58]=z[58]*z[86];
    z[58]=n<T>(1,9) + z[58];
    z[30]=z[34] + n<T>(1,2)*z[58] + z[30];
    z[30]=z[12]*z[30];
    z[34]= - n<T>(11,4) + z[32];
    z[34]=z[8]*z[34];
    z[34]= - z[76] - n<T>(1,2) + z[34];
    z[34]=z[34]*z[56];
    z[58]=n<T>(1,8) - z[8];
    z[58]=z[8]*z[58];
    z[34]=z[34] + n<T>(77,72) + z[58];
    z[34]=z[2]*z[34];
    z[58]= - n<T>(19,24) + z[8];
    z[58]=z[8]*z[58];
    z[62]=n<T>(43,24) - z[8];
    z[62]=z[8]*z[62];
    z[62]=n<T>(115,24) + z[62];
    z[62]=z[4]*z[62];
    z[30]=z[30] + z[62] + z[34] - n<T>(65,12) + z[58];
    z[34]= - n<T>(463,2) - 95*z[8];
    z[34]=z[34]*z[55];
    z[34]= - static_cast<T>(25)+ z[34];
    z[58]= - static_cast<T>(1)- z[25];
    z[58]=z[8]*z[58];
    z[58]=z[58] - z[97];
    z[58]=z[58]*z[26];
    z[62]=static_cast<T>(5)+ n<T>(107,36)*z[8];
    z[62]=z[8]*z[62];
    z[58]=z[58] + n<T>(5,4) + z[62];
    z[58]=z[2]*z[58];
    z[62]= - static_cast<T>(187)- 91*z[8];
    z[62]=z[62]*z[88];
    z[75]=static_cast<T>(181)+ n<T>(281,3)*z[8];
    z[75]=z[8]*z[75];
    z[62]=z[75] + z[62];
    z[62]=z[4]*z[62];
    z[34]=n<T>(1,12)*z[62] + n<T>(1,6)*z[34] + z[58];
    z[58]=static_cast<T>(1)+ n<T>(19,6)*z[8];
    z[58]=z[8]*z[58];
    z[58]=z[58] - z[37];
    z[58]=z[2]*z[58];
    z[62]= - n<T>(19,3) - n<T>(61,4)*z[8];
    z[62]=z[8]*z[62];
    z[62]= - static_cast<T>(1)+ z[62];
    z[58]=n<T>(1,2)*z[62] + z[58];
    z[62]= - n<T>(163,32)*z[23] + z[90];
    z[62]=z[4]*z[62];
    z[62]=n<T>(157,16)*z[23] + z[62];
    z[62]=z[62]*z[57];
    z[58]=n<T>(1,4)*z[58] + z[62];
    z[58]=z[58]*z[71];
    z[34]=n<T>(1,8)*z[34] + z[58];
    z[34]=z[10]*z[34];
    z[58]= - n<T>(17,8) + z[8];
    z[58]=z[8]*z[58];
    z[58]=z[58] + n<T>(3,4)*z[24];
    z[58]=z[2]*z[58];
    z[62]= - static_cast<T>(7)+ z[8];
    z[62]=z[8]*z[62];
    z[62]=n<T>(7,2) + z[62];
    z[58]=n<T>(1,4)*z[62] + z[58];
    z[58]=z[2]*z[58];
    z[58]= - n<T>(1,8)*z[21] + z[58];
    z[62]=3*z[2];
    z[58]=z[58]*z[62];
    z[75]=n<T>(9,2) + n<T>(13,3)*z[2];
    z[75]=z[2]*z[75];
    z[50]=z[75] - z[50];
    z[50]=z[50]*z[36];
    z[50]=z[58] + z[50];
    z[58]= - static_cast<T>(1)+ z[39];
    z[58]=z[36]*z[35]*z[58];
    z[33]= - z[33] + z[58];
    z[33]=z[33]*z[41];
    z[33]=n<T>(1,2)*z[50] + z[33];
    z[33]=z[5]*z[33];
    z[41]=n<T>(65,2) - z[69];
    z[41]=z[41]*z[31];
    z[41]= - n<T>(19,4)*z[24] + static_cast<T>(1)+ z[41];
    z[41]=z[2]*z[41];
    z[50]=static_cast<T>(1)+ n<T>(11,4)*z[8];
    z[41]=n<T>(1,2)*z[50] + z[41];
    z[41]=z[41]*z[59];
    z[50]= - static_cast<T>(3)- n<T>(11,6)*z[2];
    z[50]=z[50]*z[36];
    z[41]=z[50] + n<T>(3,2) + z[41];
    z[33]=n<T>(1,2)*z[41] + z[33];
    z[33]=z[33]*z[49];
    z[41]=z[2] - 1;
    z[49]=z[3]*z[41];
    z[30]=n<T>(1,12)*z[49] + z[33] + z[34] + n<T>(1,4)*z[30];
    z[30]=z[12]*z[30];
    z[33]=z[41]*z[2];
    z[33]=z[33] - z[21];
    z[34]=z[33]*z[67];
    z[20]=z[20] - z[29];
    z[20]=z[20]*z[4];
    z[49]=n<T>(1,2) + z[20];
    z[49]=z[1]*z[49];
    z[34]=z[49] + z[34] + z[19] - z[84];
    z[34]=z[1]*z[34];
    z[49]=z[28] + z[37];
    z[49]=z[49]*z[35];
    z[50]=npow(z[2],3);
    z[58]= - z[50]*z[67];
    z[73]=z[73]*z[50];
    z[75]=z[35] + z[73];
    z[75]=z[75]*z[40];
    z[34]=z[34] + z[75] + z[49] + z[58];
    z[49]=n<T>(1,2)*z[7];
    z[34]=z[34]*z[49];
    z[58]=n<T>(7,3)*z[8];
    z[42]= - z[42]*z[58];
    z[33]=z[33]*z[4];
    z[75]=7*z[2];
    z[78]=z[75] - static_cast<T>(13)+ 19*z[8];
    z[78]=n<T>(1,6)*z[78] + z[33];
    z[78]=z[4]*z[78];
    z[42]=z[78] - static_cast<T>(1)+ z[42];
    z[78]= - z[27]*z[69];
    z[78]= - n<T>(1,2) + z[78];
    z[20]=n<T>(1,6)*z[78] + z[20];
    z[20]=z[20]*z[54];
    z[20]=n<T>(1,2)*z[42] + z[20];
    z[20]=z[20]*z[44];
    z[42]=n<T>(1,6)*z[35];
    z[44]=static_cast<T>(1)- z[65];
    z[44]=z[44]*z[42];
    z[44]=z[44] + z[73];
    z[44]=z[4]*z[44];
    z[44]=z[56] + z[44];
    z[78]=n<T>(1,4)*z[14];
    z[44]=z[44]*z[78];
    z[80]= - static_cast<T>(1)+ n<T>(7,8)*z[8];
    z[80]=z[8]*z[80];
    z[80]=z[80] + n<T>(1,8)*z[24];
    z[80]=z[80]*z[59];
    z[50]=z[50]*z[4];
    z[42]= - z[42] - z[50];
    z[84]=n<T>(1,4)*z[4];
    z[42]=z[42]*z[84];
    z[28]=z[28] + n<T>(1,2);
    z[28]=z[28]*z[3]*npow(z[1],2);
    z[20]=z[34] - n<T>(7,12)*z[28] + z[20] + z[44] + z[80] + z[42];
    z[20]=z[20]*z[49];
    z[34]= - static_cast<T>(1)+ n<T>(133,2)*z[8];
    z[34]=z[34]*z[55];
    z[34]=n<T>(1,2) + z[34];
    z[42]= - static_cast<T>(3)- z[85];
    z[42]=z[42]*z[76];
    z[34]=n<T>(1,3)*z[34] + z[42];
    z[42]=n<T>(1201,9) - 85*z[8];
    z[42]=z[42]*z[31];
    z[44]= - static_cast<T>(793)+ n<T>(43,2)*z[2];
    z[44]=z[44]*z[82];
    z[42]=z[44] - n<T>(109,9) + z[42];
    z[44]=z[35] - 1;
    z[49]=z[44] - z[89];
    z[80]=z[4]*z[49];
    z[42]=n<T>(1,2)*z[42] + n<T>(29,9)*z[80];
    z[42]=z[4]*z[42];
    z[80]= - n<T>(6761,3) + 3061*z[8];
    z[80]=z[80]*z[19];
    z[80]= - n<T>(809,3)*z[2] - n<T>(89,3) + z[80];
    z[42]=n<T>(1,48)*z[80] + z[42];
    z[42]=z[4]*z[42];
    z[80]=static_cast<T>(107)- n<T>(5753,4)*z[8];
    z[61]=z[80]*z[61];
    z[61]= - n<T>(5,2) + z[61];
    z[42]=n<T>(1,8)*z[61] + z[42];
    z[42]=z[42]*z[57];
    z[61]= - n<T>(23,12)*z[23] - z[24];
    z[80]= - static_cast<T>(1)+ z[23];
    z[82]=z[26] + n<T>(1,3);
    z[89]=z[2]*z[82];
    z[80]=n<T>(1,6)*z[80] + z[89];
    z[89]=29*z[4];
    z[80]=z[80]*z[89];
    z[92]= - n<T>(103,3)*z[2] + n<T>(181,3) - 215*z[23];
    z[80]=n<T>(1,16)*z[92] + z[80];
    z[80]=z[80]*z[57];
    z[92]= - static_cast<T>(1)+ n<T>(193,12)*z[8];
    z[92]=z[8]*z[92];
    z[92]=n<T>(17,12) + z[92];
    z[80]=n<T>(1,4)*z[92] + z[80];
    z[80]=z[4]*z[80];
    z[92]=static_cast<T>(5)- n<T>(121,3)*z[8];
    z[92]=z[8]*z[92];
    z[80]=n<T>(1,48)*z[92] + z[80];
    z[80]=z[4]*z[80];
    z[61]=n<T>(1,4)*z[61] + z[80];
    z[61]=z[61]*z[71];
    z[34]=z[61] + n<T>(1,4)*z[34] + z[42];
    z[34]=z[10]*z[34];
    z[42]= - static_cast<T>(5)+ n<T>(41,24)*z[8];
    z[42]=z[42]*z[19];
    z[61]= - n<T>(7,8) + z[8];
    z[61]=z[8]*z[61];
    z[61]=z[61] + n<T>(1,16)*z[24];
    z[61]=z[2]*z[61];
    z[42]=z[42] + z[61];
    z[42]=z[2]*z[42];
    z[61]=n<T>(25,4) - z[55];
    z[61]=z[8]*z[61];
    z[42]=z[61] + z[42];
    z[24]= - n<T>(19,9)*z[23] - z[24];
    z[24]=z[24]*z[26];
    z[61]= - z[23] + z[87];
    z[61]=z[4]*z[61];
    z[24]=n<T>(7,18)*z[61] + n<T>(31,9)*z[23] + z[24];
    z[24]=z[24]*z[36];
    z[36]=n<T>(1,18)*z[8];
    z[61]= - static_cast<T>(5)+ z[36];
    z[61]=z[8]*z[61];
    z[61]=z[61] + z[88];
    z[61]=z[61]*z[84];
    z[24]=z[24] + n<T>(1,3)*z[42] + z[61];
    z[24]=z[10]*z[24];
    z[42]= - z[58] + z[87];
    z[42]=z[42]*z[84];
    z[61]=z[77] - static_cast<T>(1)+ n<T>(17,8)*z[8];
    z[61]=z[2]*z[61];
    z[61]=z[61] - n<T>(41,12) - z[8];
    z[61]=z[2]*z[61];
    z[42]=z[42] + z[61] + n<T>(43,4) + z[55];
    z[24]=n<T>(1,6)*z[42] + z[24];
    z[42]=z[58] - z[68];
    z[42]=z[2]*z[42];
    z[42]= - z[55] + n<T>(1,16)*z[42];
    z[42]=z[2]*z[42];
    z[61]=z[70] - z[96];
    z[61]=z[2]*z[61];
    z[61]= - z[83] + z[61];
    z[61]=z[2]*z[61];
    z[61]= - z[70] + z[61];
    z[61]=z[61]*z[91];
    z[68]=z[87] - z[8];
    z[42]=z[61] + z[42] - n<T>(1,12)*z[68];
    z[42]=z[42]*z[71];
    z[61]=n<T>(5,3) - z[26];
    z[61]=z[2]*z[61];
    z[61]= - n<T>(25,3) + z[61];
    z[61]=z[61]*z[59];
    z[61]=static_cast<T>(3)+ z[61];
    z[68]=z[10]*z[4];
    z[68]=z[40] - n<T>(29,4) + z[68];
    z[68]=z[14]*z[68];
    z[42]=n<T>(1,36)*z[68] + n<T>(1,16)*z[61] + z[42];
    z[42]=z[15]*z[42];
    z[61]=n<T>(7,3) - z[4];
    z[61]=z[61]*z[67];
    z[61]= - static_cast<T>(31)+ z[61];
    z[68]=static_cast<T>(5)- z[57];
    z[68]=z[4]*z[68];
    z[70]=z[10]*npow(z[4],2);
    z[68]=z[68] - n<T>(7,18)*z[70];
    z[68]=z[10]*z[68];
    z[61]=n<T>(1,3)*z[61] + z[68];
    z[68]=n<T>(1,3)*z[14];
    z[70]= - n<T>(1,16)*z[10] - n<T>(1,16);
    z[70]=z[4]*z[70];
    z[70]=n<T>(2,3) + z[70];
    z[70]=z[70]*z[68];
    z[61]=n<T>(1,8)*z[61] + z[70];
    z[61]=z[14]*z[61];
    z[70]=z[40] - 1;
    z[70]=z[70]*z[14];
    z[77]=z[40]*z[13];
    z[70]=z[70] + z[77];
    z[77]=z[13]*z[70];
    z[80]=n<T>(1,2)*z[13];
    z[83]=z[80] - 1;
    z[84]=z[13]*z[83];
    z[84]=n<T>(1,2) + z[84];
    z[84]=z[3]*z[84];
    z[77]=z[77] + z[84];
    z[24]=z[42] + n<T>(1,2)*z[24] + z[61] + n<T>(1,9)*z[77];
    z[24]=z[15]*z[24];
    z[42]=npow(z[13],2);
    z[61]=n<T>(1,2) - z[13];
    z[61]=z[61]*z[42];
    z[77]=static_cast<T>(1)- z[13];
    z[77]=z[3]*z[77]*z[80];
    z[61]=z[61] + z[77];
    z[77]=z[11]*npow(z[12],2);
    z[61]= - n<T>(1,6)*z[77] + n<T>(1,12)*z[61];
    z[61]=z[3]*z[61];
    z[77]=z[14] - 1;
    z[51]=z[77]*z[51];
    z[84]=static_cast<T>(1)- n<T>(13,12)*z[14];
    z[84]=z[13]*z[84];
    z[51]=z[51] + z[84];
    z[42]=z[51]*z[42];
    z[51]= - n<T>(1,2) + z[3];
    z[84]=z[3]*z[2];
    z[84]= - n<T>(83,48) + z[84];
    z[84]=z[12]*z[84];
    z[51]=n<T>(1,2)*z[51] + z[84];
    z[84]=n<T>(1,3)*z[12];
    z[51]=z[51]*z[84];
    z[42]=z[51] + z[42] + z[61];
    z[42]=z[11]*z[42];
    z[40]= - z[47]*z[40];
    z[51]= - static_cast<T>(13)- z[2];
    z[40]=n<T>(1,3)*z[51] + z[40];
    z[40]=z[14]*z[40];
    z[40]=n<T>(1,3) + z[40];
    z[51]=static_cast<T>(13)+ 11*z[2];
    z[51]=z[14]*z[51];
    z[51]=n<T>(1,6)*z[51] - z[62] + z[10];
    z[51]=z[13]*z[51];
    z[40]=n<T>(1,2)*z[40] + z[51];
    z[40]=z[13]*z[40];
    z[47]=z[14]*z[47];
    z[41]=n<T>(1,2)*z[41] + z[47];
    z[41]=z[14]*z[41];
    z[41]= - n<T>(1,3) + z[41];
    z[40]=n<T>(1,4)*z[41] + z[40];
    z[40]=z[13]*z[40];
    z[41]= - n<T>(1,3) + z[2];
    z[47]=z[65] + 1;
    z[51]=z[13]*z[47];
    z[51]=n<T>(1,6)*z[51] - n<T>(1,6) - z[2];
    z[51]=z[13]*z[51];
    z[41]=n<T>(1,8)*z[41] + z[51];
    z[41]=z[13]*z[41];
    z[51]=n<T>(1,3) + z[2];
    z[51]=z[13]*z[51];
    z[47]= - n<T>(1,3)*z[47] + z[51];
    z[47]=z[13]*z[47];
    z[47]=z[26] + z[47];
    z[47]=z[3]*z[47];
    z[41]=n<T>(1,4)*z[47] + n<T>(1,12) + z[41];
    z[41]=z[3]*z[41];
    z[47]= - static_cast<T>(23)+ n<T>(139,6)*z[2];
    z[47]=n<T>(1,2)*z[47] - n<T>(19,3)*z[10];
    z[44]= - z[3]*z[44];
    z[44]=n<T>(1,2)*z[47] + z[44];
    z[44]=z[12]*z[44];
    z[47]= - n<T>(25,3) + n<T>(7,2)*z[2];
    z[44]=z[44] + n<T>(1,4)*z[47] - z[10];
    z[47]= - z[3]*z[79];
    z[44]=z[47] + n<T>(1,2)*z[44];
    z[44]=z[44]*z[84];
    z[40]=z[42] + z[44] + z[41] + n<T>(1,12) + z[40];
    z[41]= - static_cast<T>(1)+ z[68];
    z[41]=z[14]*z[41];
    z[42]=z[13]*z[68];
    z[41]=z[41] + z[42];
    z[41]=z[41]*z[80];
    z[42]= - static_cast<T>(1)+ n<T>(1,3)*z[13];
    z[42]=z[42]*z[80];
    z[42]= - n<T>(1,3)*z[29] + z[42];
    z[42]=z[3]*z[42];
    z[41]=z[41] + z[42];
    z[41]=z[15]*z[41];
    z[42]=z[3]*z[83];
    z[42]=z[70] + z[42];
    z[42]=z[13]*z[42];
    z[41]=n<T>(1,3)*z[42] + z[41];
    z[41]=z[15]*z[41];
    z[40]=n<T>(1,3)*z[41] + n<T>(1,2)*z[40];
    z[40]=z[11]*z[40];
    z[41]= - n<T>(1,4) + z[8];
    z[41]=z[41]*z[55];
    z[42]= - static_cast<T>(1)- z[8];
    z[42]=z[42]*z[19];
    z[42]=static_cast<T>(1)+ z[42];
    z[42]=z[42]*z[56];
    z[44]= - static_cast<T>(5)+ z[58];
    z[44]=z[8]*z[44];
    z[44]=static_cast<T>(3)+ z[44];
    z[47]= - static_cast<T>(1)- z[63];
    z[47]=z[4]*z[47];
    z[44]=n<T>(1,2)*z[44] + z[47];
    z[44]=z[4]*z[44];
    z[47]=n<T>(11,3) - z[38];
    z[47]=z[8]*z[47];
    z[47]=n<T>(1,3) + z[47];
    z[44]=n<T>(1,2)*z[47] + z[44];
    z[44]=z[44]*z[67];
    z[41]=z[44] + z[42] - n<T>(1,4) + z[41];
    z[42]=n<T>(5,2) - z[58];
    z[42]=z[42]*z[19];
    z[44]=z[55] - n<T>(1,2);
    z[47]=z[44]*z[87];
    z[42]=z[42] + z[47];
    z[42]=z[4]*z[42];
    z[38]= - n<T>(11,6) + z[38];
    z[38]=z[38]*z[19];
    z[38]=z[38] + z[42];
    z[38]=z[38]*z[67];
    z[42]=n<T>(1,4) + z[8];
    z[42]=z[8]*z[42];
    z[42]=z[42] - z[96];
    z[47]=n<T>(7,2)*z[23] - z[90];
    z[47]=z[47]*z[57];
    z[47]= - n<T>(3,2)*z[23] + z[47];
    z[47]=z[47]*z[67];
    z[42]=n<T>(1,3)*z[42] + z[47];
    z[42]=z[42]*z[39];
    z[47]=z[95]*z[76];
    z[47]=z[47] - n<T>(1,8) - z[23];
    z[38]=z[42] + n<T>(1,3)*z[47] + z[38];
    z[38]=z[10]*z[38];
    z[42]= - static_cast<T>(1)- z[59];
    z[42]=n<T>(1,2)*z[42] + z[64];
    z[42]=z[4]*z[42];
    z[42]=z[42] - z[82];
    z[42]=z[4]*z[42];
    z[42]=n<T>(1,2) + z[42];
    z[42]=z[42]*z[78];
    z[47]=z[11]*z[77];
    z[38]=n<T>(1,12)*z[47] + z[42] + n<T>(1,2)*z[41] + z[38];
    z[38]=z[16]*z[38];
    z[41]= - static_cast<T>(4)+ z[8];
    z[41]=z[41]*z[55];
    z[42]= - static_cast<T>(2)+ z[2];
    z[42]=z[42]*z[59];
    z[41]=z[42] + static_cast<T>(1)+ z[41];
    z[41]=z[41]*z[89];
    z[42]= - static_cast<T>(2075)- 307*z[2];
    z[42]=z[42]*z[26];
    z[47]= - n<T>(4055,2) + 727*z[8];
    z[47]=z[8]*z[47];
    z[42]=z[42] + n<T>(3529,2) + z[47];
    z[41]=n<T>(1,24)*z[42] + z[41];
    z[41]=z[4]*z[41];
    z[42]=static_cast<T>(245)- n<T>(1261,8)*z[8];
    z[25]=z[42]*z[25];
    z[25]= - n<T>(791,12)*z[2] - n<T>(91,8) + z[25];
    z[25]=n<T>(1,4)*z[25] + z[41];
    z[25]=z[25]*z[57];
    z[41]=z[19] - 2;
    z[41]=z[41]*z[55];
    z[29]=z[29]*z[59];
    z[29]=z[41] + z[29] + n<T>(1,2);
    z[41]=z[29]*z[81];
    z[42]= - n<T>(223,18)*z[2] - static_cast<T>(1)- n<T>(241,6)*z[63];
    z[41]=n<T>(1,8)*z[42] + z[41];
    z[41]=z[4]*z[41];
    z[41]= - n<T>(1,16) + z[41];
    z[41]=z[4]*z[41];
    z[21]=z[21]*z[31];
    z[41]=z[21] + z[41];
    z[41]=z[1]*z[41];
    z[42]= - static_cast<T>(59)+ n<T>(487,2)*z[8];
    z[42]=z[42]*z[55];
    z[42]= - n<T>(1,2) + z[42];
    z[25]=z[41] + n<T>(1,8)*z[42] + z[25];
    z[25]=z[57]*z[25];
    z[41]=static_cast<T>(115)- 419*z[8];
    z[41]=z[41]*z[55];
    z[41]=static_cast<T>(1)+ z[41];
    z[42]=z[49]*z[89];
    z[47]= - n<T>(1007,2) + 443*z[8];
    z[47]=z[8]*z[47];
    z[47]= - n<T>(163,2)*z[2] + n<T>(121,2) + z[47];
    z[42]=n<T>(1,8)*z[47] + z[42];
    z[42]=z[42]*z[57];
    z[41]=n<T>(1,16)*z[41] + z[42];
    z[41]=z[41]*z[57];
    z[42]=static_cast<T>(1)- z[32];
    z[42]=z[42]*z[45];
    z[41]=z[42] + z[41];
    z[41]=z[4]*z[41];
    z[42]=z[19] + 1;
    z[31]= - z[42]*z[31];
    z[31]=z[31] + z[41];
    z[31]=z[31]*z[71];
    z[41]=static_cast<T>(1)- z[85];
    z[41]=z[41]*z[19];
    z[41]= - n<T>(1,3) + z[41];
    z[25]=z[31] + n<T>(1,4)*z[41] + z[25];
    z[25]=z[1]*z[25];
    z[26]=n<T>(1,6)*z[33] - n<T>(1,3)*z[27] - z[26];
    z[26]=z[4]*z[26];
    z[26]=z[55] + z[26];
    z[27]=z[27]*z[19];
    z[27]= - static_cast<T>(1)+ z[27];
    z[19]= - z[60]*z[19];
    z[19]=z[56] - n<T>(1,3) + z[19];
    z[19]=z[4]*z[19];
    z[19]=n<T>(1,3)*z[27] + n<T>(5,2)*z[19];
    z[19]=z[19]*z[54];
    z[19]=n<T>(1,2)*z[26] + z[19];
    z[19]=z[1]*z[19];
    z[26]=n<T>(5,4) + z[2];
    z[26]=z[26]*z[35];
    z[26]=z[26] + n<T>(1,4)*z[73];
    z[26]=z[4]*z[26];
    z[27]= - static_cast<T>(7)- z[65];
    z[27]=z[27]*z[86];
    z[26]=n<T>(7,4)*z[46] + z[27] + z[26];
    z[26]=z[26]*z[68];
    z[27]= - 5*z[35] - z[50];
    z[27]=z[4]*z[27];
    z[27]=z[75] + z[27];
    z[27]=n<T>(1,3)*z[27] + z[10];
    z[19]=z[19] + n<T>(1,4)*z[27] + z[26];
    z[19]=z[6]*z[19];
    z[26]= - z[10]*z[42];
    z[26]=z[26] - static_cast<T>(17)+ z[48];
    z[26]=z[85]*z[26];
    z[21]= - z[1]*z[21];
    z[21]=z[21] + static_cast<T>(1)+ z[26];
    z[21]=z[1]*z[21];
    z[21]=z[28] + z[21];
    z[26]=z[15]*z[52]*z[23];
    z[27]= - z[10]*z[36];
    z[27]=z[27] - static_cast<T>(1)+ n<T>(11,9)*z[23];
    z[27]=z[27]*z[39];
    z[21]=n<T>(11,18)*z[26] + z[27] + n<T>(1,3)*z[21];
    z[21]=z[9]*z[21];
    z[26]=z[26] - z[66];
    z[23]=z[23] + z[37];
    z[23]=z[23]*z[71];
    z[27]= - z[8]*z[44];
    z[23]=z[23] + z[27] - z[72];
    z[23]=z[10]*z[23];
    z[23]=z[23] - n<T>(1,6)*z[26];
    z[23]=z[17]*z[23];
    z[26]=n<T>(11,4) - n<T>(13,3)*z[8];
    z[26]=z[26]*z[32];
    z[27]= - n<T>(5,4) + z[69];
    z[27]=z[8]*z[27];
    z[27]= - n<T>(5,2) + z[27];
    z[27]=z[27]*z[59];
    z[26]=z[27] - n<T>(31,18) + z[26];
    z[27]=z[29]*z[89];
    z[28]= - static_cast<T>(917)- n<T>(307,2)*z[2];
    z[28]=z[28]*z[56];
    z[29]= - n<T>(1181,3) + n<T>(559,4)*z[8];
    z[29]=z[8]*z[29];
    z[28]=z[28] + n<T>(3511,12) + z[29];
    z[27]=n<T>(1,4)*z[28] + z[27];
    z[27]=z[27]*z[74];
    z[28]=n<T>(12193,9) - n<T>(1219,2)*z[8];
    z[28]=z[8]*z[28];
    z[28]= - n<T>(10211,18) + z[28];
    z[29]=static_cast<T>(17)- n<T>(43,108)*z[2];
    z[29]=z[2]*z[29];
    z[28]=n<T>(1,12)*z[28] + z[29];
    z[27]=n<T>(1,4)*z[28] + z[27];
    z[27]=z[4]*z[27];
    z[28]= - static_cast<T>(5947)+ n<T>(8647,2)*z[8];
    z[28]=z[8]*z[28];
    z[28]=n<T>(7,2) + n<T>(1,27)*z[28];
    z[28]=n<T>(1,4)*z[28] + n<T>(139,27)*z[2];
    z[27]=n<T>(1,4)*z[28] + z[27];
    z[27]=z[4]*z[27];

    r += z[18] + n<T>(1,4)*z[19] + z[20] + z[21] + z[22] + n<T>(5,2)*z[23] + 
      z[24] + z[25] + n<T>(1,8)*z[26] + z[27] + z[30] + z[34] + n<T>(1,2)*z[38]
       + z[40] + z[43] + z[53];
 
    return r;
}

template double qg_2lha_r1236(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1236(const std::array<dd_real,30>&);
#endif
