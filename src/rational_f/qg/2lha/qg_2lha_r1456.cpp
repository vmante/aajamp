#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1456(const std::array<T,30>& k) {
  T z[43];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[12];
    z[6]=k[4];
    z[7]=k[2];
    z[8]=k[7];
    z[9]=k[11];
    z[10]=k[24];
    z[11]=k[9];
    z[12]=k[14];
    z[13]=k[10];
    z[14]=n<T>(1,2)*z[1];
    z[15]= - static_cast<T>(3)- z[14];
    z[15]=z[15]*z[14];
    z[16]=n<T>(1,2)*z[2];
    z[17]=n<T>(3,2)*z[2] + n<T>(11,2) - z[1];
    z[17]=z[17]*z[16];
    z[18]= - static_cast<T>(1)+ z[1];
    z[18]=z[18]*z[14];
    z[19]=z[16] + n<T>(1,2) - z[1];
    z[19]=z[2]*z[19];
    z[18]=z[18] + z[19];
    z[19]=z[16]*z[3];
    z[18]=z[18]*z[19];
    z[15]=z[18] + z[17] - static_cast<T>(1)+ z[15];
    z[17]=n<T>(1,2)*z[3];
    z[15]=z[15]*z[17];
    z[18]=n<T>(1,4)*z[12];
    z[20]= - static_cast<T>(23)+ 3*z[12];
    z[20]=z[20]*z[18];
    z[21]= - static_cast<T>(1)+ n<T>(3,2)*z[12];
    z[22]=z[12]*z[21];
    z[22]=n<T>(1,2) + z[22];
    z[22]=z[22]*z[16];
    z[23]=z[5]*z[6];
    z[24]=static_cast<T>(1)+ 7*z[1];
    z[24]=z[24]*z[23];
    z[25]=n<T>(7,3) + z[14];
    z[25]=z[1]*z[25];
    z[24]=n<T>(1,6)*z[24] - n<T>(11,2)*z[6] - n<T>(1,3) + z[25];
    z[25]=n<T>(1,4)*z[5];
    z[24]=z[24]*z[25];
    z[26]=n<T>(1,8)*z[8] + n<T>(1,3)*z[13];
    z[27]=z[11]*z[12];
    z[28]=n<T>(1,4)*z[27];
    z[15]=z[24] - z[28] + z[15] + z[22] + z[20] + static_cast<T>(1)+ z[26];
    z[20]=z[6]*z[12];
    z[22]=n<T>(1,2)*z[12];
    z[24]=static_cast<T>(7)- z[22];
    z[24]=z[24]*z[20];
    z[29]= - static_cast<T>(1)- n<T>(23,6)*z[6];
    z[29]=7*z[29] - n<T>(41,12)*z[23];
    z[29]=z[29]*z[25];
    z[30]= - 5*z[20] + n<T>(37,6)*z[23];
    z[31]=n<T>(1,2)*z[9];
    z[30]=z[30]*z[31];
    z[24]=z[30] + z[29] + n<T>(3,4)*z[24] - n<T>(1,8)*z[12] + n<T>(1,3) - n<T>(1,4)*z[8]
   ;
    z[24]=z[9]*z[24];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[20];
    z[20]=n<T>(1,8)*z[5];
    z[29]=static_cast<T>(3)+ n<T>(85,6)*z[6];
    z[29]=z[29]*z[20];
    z[29]=z[29] + n<T>(151,48) + 4*z[6];
    z[29]=z[5]*z[29];
    z[27]=n<T>(1,8)*z[27];
    z[30]=z[3]*z[2];
    z[32]=n<T>(19,4)*z[12] - static_cast<T>(1)+ n<T>(3,4)*z[8];
    z[18]=z[24] + z[29] + z[27] - n<T>(1,4)*z[30] + n<T>(1,2)*z[32] + 3*z[18];
    z[18]=z[9]*z[18];
    z[24]=n<T>(1,2)*z[6];
    z[29]=z[24] + 1;
    z[32]=z[29]*z[6];
    z[32]=z[32] + n<T>(1,2);
    z[23]=z[32]*z[23];
    z[33]=static_cast<T>(9)+ 7*z[6];
    z[24]=z[33]*z[24];
    z[24]=z[23] + static_cast<T>(1)+ z[24];
    z[24]=z[5]*z[24];
    z[33]= - static_cast<T>(17)+ 35*z[2];
    z[33]=n<T>(1,2)*z[33] - 13*z[6];
    z[34]=z[6] + n<T>(1,2);
    z[35]= - z[6]*z[34];
    z[35]=n<T>(1,2) + z[35];
    z[35]=z[11]*z[35];
    z[24]=n<T>(3,2)*z[24] + n<T>(1,2)*z[33] + z[35];
    z[33]= - static_cast<T>(1)+ z[2];
    z[33]=z[33]*z[16];
    z[29]=z[29] - z[16];
    z[35]=z[6]*z[29];
    z[33]=z[33] + z[35];
    z[35]=z[11]*z[6];
    z[32]= - z[32]*z[35];
    z[32]=3*z[33] + z[32];
    z[23]=n<T>(1,2)*z[32] - z[23];
    z[32]=3*z[10];
    z[23]=z[23]*z[32];
    z[23]=z[23] + n<T>(1,2)*z[24] - z[9];
    z[24]=n<T>(3,2)*z[10];
    z[23]=z[23]*z[24];
    z[33]=3*z[5];
    z[36]= - n<T>(5,4) - z[6];
    z[36]=z[6]*z[36];
    z[36]= - n<T>(1,4) + z[36];
    z[36]=z[36]*z[33];
    z[37]= - n<T>(53,4) - 11*z[6];
    z[36]=n<T>(1,2)*z[37] + z[36];
    z[36]=z[5]*z[36];
    z[34]=z[11]*z[34];
    z[34]=n<T>(35,2) + z[34];
    z[34]=n<T>(1,2)*z[34] + z[36];
    z[36]=2*z[9];
    z[37]=n<T>(1,4)*z[11];
    z[38]=n<T>(9,4)*z[5] + static_cast<T>(5)+ z[37];
    z[38]=n<T>(1,2)*z[38] - z[36];
    z[38]=z[9]*z[38];
    z[23]=z[23] + n<T>(1,2)*z[34] + z[38];
    z[23]=z[10]*z[23];
    z[15]=z[23] + n<T>(1,2)*z[15] + z[18];
    z[15]=z[4]*z[15];
    z[18]= - static_cast<T>(1)- z[12];
    z[18]=z[18]*z[22];
    z[18]=z[18] + static_cast<T>(3)+ z[8];
    z[23]=z[16] + 1;
    z[23]=z[23]*z[3];
    z[34]= - static_cast<T>(203)- 41*z[5];
    z[34]=z[5]*z[34];
    z[38]=n<T>(37,6)*z[5] - n<T>(7,2) - 5*z[12];
    z[38]=z[9]*z[38];
    z[18]=z[38] + n<T>(1,24)*z[34] + n<T>(3,2)*z[18] + z[23];
    z[18]=z[18]*z[31];
    z[34]=static_cast<T>(17)+ n<T>(89,6)*z[5];
    z[34]=z[34]*z[20];
    z[38]=n<T>(1,4)*z[3];
    z[39]=z[38] + 1;
    z[39]=z[39]*z[3];
    z[39]=z[39] - z[12];
    z[40]= - static_cast<T>(1)- n<T>(3,2)*z[8];
    z[18]=z[18] + z[34] - z[28] + n<T>(1,2)*z[40] - z[39];
    z[18]=z[9]*z[18];
    z[28]=z[6] + n<T>(3,2);
    z[28]=z[28]*z[6];
    z[28]=z[28] + n<T>(1,2);
    z[34]=z[28]*z[25];
    z[40]=z[6] + n<T>(5,8);
    z[34]=z[34] + z[40];
    z[33]=z[34]*z[33];
    z[34]=n<T>(1,2)*z[11];
    z[41]= - z[5] - z[34];
    z[28]=z[28]*z[41];
    z[41]=z[16] - 1;
    z[42]=n<T>(1,4)*z[6] - z[41];
    z[28]=3*z[42] + z[28];
    z[28]=z[28]*z[32];
    z[32]=3*z[9];
    z[42]= - static_cast<T>(1)- z[6];
    z[42]=z[11]*z[42];
    z[42]= - n<T>(49,2) + 5*z[42];
    z[28]=z[28] + z[32] + n<T>(1,4)*z[42] + z[33];
    z[24]=z[28]*z[24];
    z[28]= - z[5]*z[40];
    z[28]= - n<T>(1,4) + z[28];
    z[28]=z[5]*z[28];
    z[28]=z[37] + z[28];
    z[33]=6*z[9] - 2*z[5] - static_cast<T>(5)- z[34];
    z[33]=z[9]*z[33];
    z[24]=z[24] + n<T>(3,2)*z[28] + z[33];
    z[24]=z[10]*z[24];
    z[21]=z[21]*z[22];
    z[21]=z[21] + n<T>(1,4) - z[26];
    z[22]= - static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[22]=z[1]*z[22];
    z[26]=n<T>(1,4)*z[2] + static_cast<T>(1)- z[14];
    z[26]=z[2]*z[26];
    z[22]=z[26] + static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[38];
    z[26]=z[2] - z[1];
    z[22]=z[22] + static_cast<T>(1)+ n<T>(3,16)*z[26];
    z[22]=z[3]*z[22];
    z[14]= - static_cast<T>(5)+ z[14];
    z[26]= - n<T>(13,3) + n<T>(7,4)*z[1];
    z[26]=z[5]*z[26];
    z[14]=n<T>(1,2)*z[14] + z[26];
    z[14]=z[14]*z[25];
    z[14]=z[15] + z[24] + z[18] + z[14] + z[27] + n<T>(1,2)*z[21] + z[22];
    z[14]=z[4]*z[14];
    z[15]=z[30] + 3*z[2];
    z[18]=z[15]*z[3];
    z[21]= - static_cast<T>(3)- 7*z[2];
    z[22]= - static_cast<T>(9)- 19*z[2];
    z[22]=z[6]*z[22];
    z[21]= - z[18] + 3*z[21] + z[22];
    z[21]=z[21]*z[34];
    z[22]=n<T>(1,2)*z[7];
    z[24]=z[22] - z[41];
    z[21]= - 27*z[24] + z[21];
    z[25]=n<T>(1,8)*z[3];
    z[15]=z[15]*z[25];
    z[26]= - static_cast<T>(3)- z[2];
    z[27]= - n<T>(9,4) - z[2];
    z[27]=z[6]*z[27];
    z[26]=z[15] + n<T>(3,4)*z[26] + z[27];
    z[26]=z[5]*z[26];
    z[21]=n<T>(1,4)*z[21] + z[26];
    z[21]=z[10]*z[21];
    z[26]=z[2] - 3;
    z[26]=z[26]*z[17];
    z[26]=z[26] + z[2] - n<T>(7,2);
    z[27]=z[3]*z[26];
    z[28]=z[6]*z[2];
    z[18]= - z[18] + 15*z[2] + 17*z[28];
    z[18]=z[18]*z[34];
    z[30]=z[6] + static_cast<T>(33)- 17*z[2];
    z[18]=z[18] + n<T>(1,2)*z[30] + z[27];
    z[18]=z[11]*z[18];
    z[18]=n<T>(69,2) + z[18];
    z[27]=n<T>(3,2) + z[2];
    z[30]=n<T>(9,8) + z[2];
    z[30]=z[6]*z[30];
    z[15]= - z[15] + n<T>(3,4)*z[27] + z[30];
    z[27]=n<T>(1,2)*z[5];
    z[15]=z[15]*z[27];
    z[26]= - z[26]*z[25];
    z[30]= - n<T>(21,8) + z[2];
    z[15]=z[15] + z[26] + n<T>(1,2)*z[30] - z[6];
    z[15]=z[5]*z[15];
    z[22]=z[22] + 1;
    z[26]= - z[22]*z[32];
    z[15]=z[21] + z[26] + n<T>(1,8)*z[18] + z[15];
    z[15]=z[10]*z[15];
    z[18]=z[19] - n<T>(1,2) + z[2];
    z[18]=z[18]*z[25];
    z[18]=z[18] + z[29];
    z[18]=z[5]*z[18];
    z[19]=static_cast<T>(1)+ n<T>(3,4)*z[3];
    z[19]=z[19]*z[3];
    z[19]=z[19] - n<T>(3,2);
    z[19]=n<T>(1,2)*z[19];
    z[18]= - z[19] + z[18];
    z[18]=z[5]*z[18];
    z[21]=n<T>(3,8)*z[3];
    z[25]= - static_cast<T>(1)- z[17];
    z[25]=z[25]*z[21];
    z[25]=z[25] + n<T>(31,16) + z[6];
    z[25]=z[11]*z[25];
    z[19]=z[19] + z[25];
    z[19]=z[11]*z[19];
    z[25]= - static_cast<T>(2)- z[7];
    z[25]=z[25]*z[36];
    z[25]=z[25] - n<T>(11,8)*z[5] + static_cast<T>(2)+ n<T>(21,8)*z[11];
    z[25]=z[9]*z[25];
    z[15]=z[15] + z[25] + z[19] + z[18];
    z[15]=z[10]*z[15];
    z[18]=185*z[6] + static_cast<T>(341)- 127*z[2];
    z[19]=5*z[2] + n<T>(101,2)*z[28];
    z[19]=z[11]*z[19];
    z[18]=n<T>(1,12)*z[19] + n<T>(1,24)*z[18] - z[23];
    z[18]=z[11]*z[18];
    z[19]=static_cast<T>(1)- n<T>(1,3)*z[2];
    z[16]=z[19]*z[16];
    z[19]=npow(z[2],2)*z[35];
    z[16]=n<T>(1,6)*z[19] + z[16] + n<T>(1,3)*z[28];
    z[16]=z[11]*z[16];
    z[16]=n<T>(1,3)*z[24] + z[16];
    z[16]=z[9]*z[16];
    z[19]= - z[8]*z[22];
    z[16]=7*z[16] + z[18] - n<T>(155,24) + z[19];
    z[16]=z[16]*z[31];
    z[18]= - z[38] + n<T>(107,48) - z[6];
    z[18]=z[11]*z[18];
    z[18]=z[18] + n<T>(7,8) + z[39];
    z[18]=z[11]*z[18];
    z[19]=z[5]*z[17];
    z[19]= - z[3] + z[19];
    z[19]=z[19]*z[20];
    z[17]=3*z[8] + z[17];
    z[16]=z[16] + z[19] + n<T>(1,8)*z[17] + z[18];
    z[16]=z[9]*z[16];
    z[17]=static_cast<T>(1)+ z[3];
    z[17]=z[17]*z[21];
    z[17]=z[17] - z[5];
    z[17]=z[17]*z[27];

    r += z[14] + z[15] + z[16] + z[17];
 
    return r;
}

template double qg_2lha_r1456(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1456(const std::array<dd_real,30>&);
#endif
