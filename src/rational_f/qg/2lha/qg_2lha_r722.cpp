#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r722(const std::array<T,30>& k) {
  T z[47];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[3];
    z[7]=k[27];
    z[8]=k[13];
    z[9]=k[12];
    z[10]=k[28];
    z[11]=k[10];
    z[12]=k[9];
    z[13]=k[5];
    z[14]=n<T>(1,2)*z[4];
    z[15]=static_cast<T>(1)+ z[6];
    z[15]=z[6]*z[15];
    z[15]= - n<T>(3,4)*z[12] + static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[14];
    z[16]=n<T>(1,4)*z[5];
    z[17]=z[5]*z[11];
    z[18]=n<T>(3,2)*z[17] + n<T>(7,2) + z[11];
    z[18]=z[18]*z[16];
    z[19]=z[1]*z[4];
    z[20]=z[6] + n<T>(11,8);
    z[21]= - z[4]*z[20];
    z[21]=n<T>(1,2)*z[19] - n<T>(9,8) + z[21];
    z[21]=z[1]*z[21];
    z[22]=7*z[19];
    z[23]= - static_cast<T>(19)- 7*z[6];
    z[23]=z[4]*z[23];
    z[23]= - static_cast<T>(27)+ z[23];
    z[23]=n<T>(1,2)*z[23] + z[22];
    z[24]=3*z[4];
    z[25]=z[2]*z[24];
    z[23]=n<T>(1,2)*z[23] + z[25];
    z[25]=n<T>(1,4)*z[2];
    z[23]=z[23]*z[25];
    z[26]=n<T>(1,4)*z[12];
    z[15]=z[23] + z[21] + z[18] + z[15] - z[26] + z[20];
    z[15]=z[2]*z[15];
    z[18]=7*z[12];
    z[20]= - static_cast<T>(3)- z[18];
    z[20]=z[4]*z[20];
    z[21]=z[24]*z[1];
    z[23]=z[21] + static_cast<T>(5)+ 13*z[4];
    z[23]=z[1]*z[23];
    z[20]=z[23] + z[20] - static_cast<T>(5)+ z[11];
    z[23]=7*z[17] + static_cast<T>(7)- 3*z[11];
    z[23]=z[23]*z[16];
    z[27]=z[24] - 1;
    z[28]=n<T>(1,2)*z[27] - z[21];
    z[28]=z[2]*z[28];
    z[29]= - static_cast<T>(1)- n<T>(3,4)*z[11];
    z[29]=z[12]*z[29];
    z[20]=z[28] + z[23] + z[29] + n<T>(3,4)*z[20];
    z[23]=n<T>(1,2)*z[2];
    z[20]=z[20]*z[23];
    z[28]=3*z[12];
    z[29]= - static_cast<T>(1)+ n<T>(7,2)*z[12];
    z[30]=z[4]*z[29];
    z[30]=z[30] + n<T>(1,2) + z[28];
    z[31]=z[24] + n<T>(1,2);
    z[32]=3*z[31];
    z[33]=9*z[4];
    z[34]=n<T>(1,2) - z[33];
    z[34]=z[1]*z[34];
    z[34]= - z[32] + z[34];
    z[35]=n<T>(1,2)*z[1];
    z[34]=z[34]*z[35];
    z[36]=n<T>(3,2)*z[12];
    z[37]=static_cast<T>(1)+ z[36];
    z[37]=z[5]*z[37];
    z[20]=z[20] + z[34] + 3*z[30] + z[37];
    z[20]=z[2]*z[20];
    z[30]=static_cast<T>(11)- 21*z[12];
    z[30]=z[30]*z[24];
    z[30]=z[30] + static_cast<T>(37)- 105*z[12];
    z[34]=z[13] - z[28];
    z[34]=z[5]*z[34];
    z[29]= - 7*z[29] + z[34];
    z[29]=z[5]*z[29];
    z[29]=n<T>(1,2)*z[30] + z[29];
    z[30]=n<T>(1,4)*z[1];
    z[34]= - static_cast<T>(1)+ z[33];
    z[34]=3*z[34] + z[5];
    z[34]=z[34]*z[30];
    z[37]=n<T>(1,2)*z[5];
    z[38]=n<T>(1,4)*z[4];
    z[39]= - static_cast<T>(3)- z[38];
    z[34]=z[34] + 3*z[39] + z[37];
    z[34]=z[1]*z[34];
    z[20]=z[20] + n<T>(1,2)*z[29] + z[34];
    z[20]=z[2]*z[20];
    z[29]=13*z[12];
    z[34]=z[12] - z[13];
    z[39]=z[34]*z[5];
    z[40]=static_cast<T>(1)- z[13];
    z[40]=z[39] + n<T>(1,2)*z[40] + z[29];
    z[40]=z[5]*z[40];
    z[40]=z[40] - static_cast<T>(27)+ 43*z[12];
    z[40]=z[40]*z[37];
    z[41]=z[37] - 1;
    z[41]=z[41]*z[5];
    z[42]=n<T>(1,2) - z[24];
    z[42]=3*z[42] + z[41];
    z[42]=z[42]*z[35];
    z[43]=n<T>(17,4) + z[4];
    z[41]=z[42] + 3*z[43] + z[41];
    z[41]=z[1]*z[41];
    z[42]=n<T>(3,2)*z[4];
    z[43]= - n<T>(9,2) + z[18];
    z[43]=z[43]*z[42];
    z[20]=z[20] + z[41] + z[40] + z[43] - n<T>(57,4) + 26*z[12];
    z[20]=z[2]*z[20];
    z[40]= - z[24] - z[11];
    z[41]=z[12] - 1;
    z[40]=z[41]*z[40];
    z[27]=z[21] - z[27];
    z[27]=z[1]*z[27];
    z[43]=z[17] + static_cast<T>(1)- z[11];
    z[43]=z[5]*z[43];
    z[27]=z[27] + z[43] - static_cast<T>(1)+ z[40];
    z[25]=z[27]*z[25];
    z[27]=z[41]*z[4];
    z[40]=z[19] - z[4];
    z[43]=static_cast<T>(2)- n<T>(15,4)*z[40];
    z[43]=z[1]*z[43];
    z[25]=z[25] + z[43] + 2*z[41] + n<T>(15,4)*z[27];
    z[25]=z[2]*z[25];
    z[43]= - 7*z[41] - 5*z[27];
    z[44]=5*z[4];
    z[45]=5*z[19] - static_cast<T>(7)- z[44];
    z[45]=z[45]*z[35];
    z[46]= - z[5]*z[41];
    z[43]=z[45] + n<T>(1,2)*z[43] + z[46];
    z[25]=3*z[43] + z[25];
    z[25]=z[2]*z[25];
    z[43]=2*z[12];
    z[45]=z[5]*z[43];
    z[45]=n<T>(23,2)*z[41] + z[45];
    z[45]=z[5]*z[45];
    z[46]=static_cast<T>(17)- n<T>(15,2)*z[40];
    z[46]=z[1]*z[46];
    z[25]=z[25] + z[46] + z[45] + 17*z[41] + n<T>(15,2)*z[27];
    z[25]=z[2]*z[25];
    z[45]= - 47*z[41] - 15*z[27];
    z[39]= - n<T>(21,2)*z[12] - z[39];
    z[39]=z[5]*z[39];
    z[39]= - n<T>(51,2)*z[41] + z[39];
    z[39]=z[5]*z[39];
    z[40]= - static_cast<T>(47)+ 15*z[40];
    z[40]=z[40]*z[35];
    z[39]=z[40] + n<T>(1,2)*z[45] + z[39];
    z[25]=n<T>(1,2)*z[39] + z[25];
    z[25]=z[2]*z[25];
    z[39]=z[34]*z[16];
    z[39]=z[12] + z[39];
    z[39]=z[5]*z[39];
    z[39]=n<T>(3,2)*z[41] + z[39];
    z[39]=z[5]*z[39];
    z[38]= - n<T>(1,4)*z[19] + static_cast<T>(1)+ z[38];
    z[38]=z[1]*z[38];
    z[27]=z[38] + z[39] + n<T>(1,4)*z[27] + z[41];
    z[25]=3*z[27] + z[25];
    z[25]=z[3]*z[25];
    z[27]=z[5]*z[9];
    z[27]=z[27] - 1;
    z[38]=z[27]*z[5];
    z[38]=z[38] + 1;
    z[38]=z[38]*z[5];
    z[38]=z[38] - 1;
    z[38]=z[38]*z[5];
    z[39]= - z[38] - static_cast<T>(1)+ n<T>(9,2)*z[4];
    z[39]=z[39]*z[35];
    z[40]=z[24] + 13;
    z[38]=z[39] - n<T>(3,4)*z[40] - z[38];
    z[38]=z[38]*z[35];
    z[39]= - z[9]*z[16];
    z[39]=z[39] + n<T>(1,4) - z[34];
    z[39]=z[5]*z[39];
    z[41]=3*z[13];
    z[45]= - n<T>(43,2)*z[12] - static_cast<T>(1)- z[41];
    z[39]=n<T>(1,4)*z[45] + z[39];
    z[39]=z[5]*z[39];
    z[45]=83*z[12];
    z[46]=static_cast<T>(65)- z[45];
    z[39]=n<T>(1,8)*z[46] + z[39];
    z[39]=z[5]*z[39];
    z[46]=static_cast<T>(5)- z[18];
    z[46]=z[4]*z[46];
    z[46]=z[46] + static_cast<T>(15)- 23*z[12];
    z[20]=z[25] + z[20] + z[38] + n<T>(3,8)*z[46] + z[39];
    z[20]=z[3]*z[20];
    z[25]=static_cast<T>(1)- 5*z[12];
    z[25]=z[25]*z[42];
    z[38]=static_cast<T>(7)+ 5*z[17];
    z[38]=z[38]*z[37];
    z[39]=static_cast<T>(5)+ z[11];
    z[42]=n<T>(1,2)*z[11];
    z[46]= - static_cast<T>(3)- z[42];
    z[46]=z[12]*z[46];
    z[25]=z[38] + z[25] + n<T>(1,2)*z[39] + z[46];
    z[38]= - static_cast<T>(5)- z[24];
    z[39]=z[2]*z[4];
    z[21]=z[39] + n<T>(1,2)*z[38] - z[21];
    z[38]=n<T>(3,4)*z[2];
    z[21]=z[21]*z[38];
    z[39]=static_cast<T>(1)+ z[24];
    z[39]=2*z[39] + n<T>(3,4)*z[19];
    z[39]=z[1]*z[39];
    z[21]=z[21] + n<T>(1,2)*z[25] + z[39];
    z[21]=z[2]*z[21];
    z[25]= - static_cast<T>(1)+ n<T>(15,4)*z[12];
    z[24]=z[25]*z[24];
    z[25]=n<T>(1,2)*z[13];
    z[28]=z[25] + z[28];
    z[28]=z[5]*z[28];
    z[39]= - n<T>(1,2) - z[33];
    z[39]=z[39]*z[35];
    z[32]= - z[32] + z[39];
    z[32]=z[32]*z[35];
    z[21]=z[21] + z[32] + z[28] + z[24] - static_cast<T>(1)+ n<T>(49,4)*z[12];
    z[21]=z[2]*z[21];
    z[24]=static_cast<T>(1)- n<T>(5,2)*z[12];
    z[24]=z[24]*z[33];
    z[28]= - z[13] - z[18];
    z[28]=z[5]*z[28];
    z[28]=z[28] - 45*z[12] + static_cast<T>(13)- z[41];
    z[28]=z[28]*z[37];
    z[32]= - z[5] + static_cast<T>(1)+ z[33];
    z[32]=z[32]*z[35];
    z[32]=z[32] - static_cast<T>(9)+ z[37];
    z[32]=z[1]*z[32];
    z[33]=static_cast<T>(23)- z[45];
    z[24]=z[32] + z[28] + n<T>(1,2)*z[33] + z[24];
    z[21]=n<T>(1,2)*z[24] + z[21];
    z[21]=z[2]*z[21];
    z[24]=npow(z[9],2);
    z[28]=z[24]*z[5];
    z[32]=z[9] - z[28];
    z[32]=z[32]*npow(z[5],2);
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[5]*z[32];
    z[32]=z[32] + z[40];
    z[33]= - z[24]*z[16];
    z[33]=z[9] + z[33];
    z[33]=z[5]*z[33];
    z[33]= - n<T>(3,4) + z[33];
    z[33]=z[5]*z[33];
    z[33]=n<T>(1,2) + z[33];
    z[33]=z[5]*z[33];
    z[31]= - n<T>(1,2)*z[31] + z[33];
    z[31]=z[1]*z[31];
    z[31]=n<T>(1,2)*z[32] + z[31];
    z[31]=z[31]*z[35];
    z[32]= - n<T>(1,2) + z[12];
    z[32]=z[32]*z[44];
    z[29]=z[32] - n<T>(11,2) + z[29];
    z[24]=z[24]*z[37];
    z[32]= - z[24] - z[9] + z[34];
    z[32]=z[32]*z[16];
    z[32]=z[32] + n<T>(11,4)*z[12] + n<T>(3,8) + z[13];
    z[32]=z[5]*z[32];
    z[32]=z[32] - static_cast<T>(5)+ n<T>(17,2)*z[12];
    z[32]=z[5]*z[32];
    z[20]=z[20] + z[21] + z[31] + n<T>(3,4)*z[29] + z[32];
    z[20]=z[3]*z[20];
    z[21]=static_cast<T>(1)+ 35*z[12];
    z[14]=z[21]*z[14];
    z[21]=15*z[17] + static_cast<T>(23)+ 5*z[11];
    z[21]=z[21]*z[37];
    z[22]= - z[22] + static_cast<T>(29)+ 55*z[4];
    z[22]=z[22]*z[35];
    z[29]=static_cast<T>(31)+ z[11];
    z[31]= - static_cast<T>(13)- z[42];
    z[31]=z[12]*z[31];
    z[21]=z[22] + z[21] - z[14] + n<T>(1,2)*z[29] + z[31];
    z[22]=z[38] - 1;
    z[22]=z[4]*z[22];
    z[19]= - z[19] - n<T>(3,2) + z[22];
    z[19]=z[2]*z[19];
    z[19]=n<T>(1,4)*z[21] + 3*z[19];
    z[19]=z[19]*z[23];
    z[21]=25*z[12];
    z[14]=z[14] - static_cast<T>(1)+ z[21];
    z[22]= - static_cast<T>(1)+ z[25];
    z[22]=n<T>(1,2)*z[22] + z[43];
    z[22]=z[5]*z[22];
    z[23]=7*z[4];
    z[25]=static_cast<T>(1)+ z[23];
    z[25]=z[25]*z[35];
    z[29]= - static_cast<T>(17)- 19*z[4];
    z[25]=z[25] + n<T>(1,2)*z[29] + z[5];
    z[25]=z[25]*z[30];
    z[14]=z[19] + z[25] + n<T>(1,4)*z[14] + z[22];
    z[14]=z[2]*z[14];
    z[19]= - 3*z[9] + z[28];
    z[19]=z[19]*z[37];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[5]*z[19];
    z[22]= - static_cast<T>(1)- n<T>(7,2)*z[4];
    z[19]=n<T>(1,2)*z[22] + z[19];
    z[19]=z[1]*z[19];
    z[22]=static_cast<T>(5)+ z[23];
    z[23]= - n<T>(1,2)*z[9] - z[27];
    z[23]=z[5]*z[23];
    z[23]= - n<T>(1,2) + z[23];
    z[23]=z[5]*z[23];
    z[19]=z[19] + n<T>(1,4)*z[22] + z[23];
    z[19]=z[1]*z[19];
    z[18]=static_cast<T>(1)- z[18];
    z[18]=z[18]*z[44];
    z[21]=static_cast<T>(3)- z[21];
    z[18]=3*z[21] + z[18];
    z[21]= - z[24] - z[13] - z[36];
    z[21]=z[5]*z[21];
    z[21]=z[21] + static_cast<T>(3)- n<T>(23,2)*z[12];
    z[21]=z[5]*z[21];
    z[18]=z[19] + n<T>(1,4)*z[18] + z[21];
    z[14]=z[20] + n<T>(1,4)*z[18] + z[14];
    z[14]=z[3]*z[14];
    z[18]=z[6]*z[7];
    z[19]=z[18]*z[8];
    z[20]= - n<T>(9,4) + z[7];
    z[20]=z[8]*z[20];
    z[20]=z[20] - z[19];
    z[20]=z[6]*z[20];
    z[21]= - static_cast<T>(1)+ z[8];
    z[20]=n<T>(9,4)*z[21] + z[20];
    z[20]=z[20]*npow(z[6],2);
    z[21]=n<T>(1,2)*z[6];
    z[22]=static_cast<T>(5)- z[21];
    z[22]=z[22]*z[26];
    z[21]=static_cast<T>(3)- z[21];
    z[21]=z[21]*z[26];
    z[18]=z[18] + 1;
    z[23]=z[7] - z[18];
    z[23]=z[23]*npow(z[6],3);
    z[21]=z[21] - n<T>(1,8) + z[23];
    z[21]=z[4]*z[21];
    z[17]=z[17] - static_cast<T>(7)+ z[11];
    z[23]=z[6]*z[8];
    z[17]= - z[23] + z[8] + n<T>(1,8)*z[17];
    z[17]=z[6]*z[17];
    z[17]=n<T>(1,2)*z[12] + z[17];
    z[17]=z[5]*z[17];
    z[17]=z[17] + z[21] + z[22] - n<T>(1,8) + z[20];
    z[20]=z[8]*z[10];
    z[21]=z[9]*z[10];
    z[20]=z[20] - z[21];
    z[22]= - z[5]*z[20];
    z[24]=static_cast<T>(1)- n<T>(1,2)*z[10];
    z[24]=z[8]*z[24];
    z[22]=z[22] + n<T>(1,2)*z[21] + z[24];
    z[22]=z[5]*z[22];
    z[24]=n<T>(1,2)*z[8];
    z[25]=z[24] - 1;
    z[22]=z[22] - z[23] + z[25];
    z[16]=z[22]*z[16];
    z[22]=npow(z[10],2);
    z[21]= - z[22] - z[21];
    z[21]=z[9]*z[21];
    z[22]=z[8]*z[22];
    z[21]=z[21] + z[22];
    z[21]=z[5]*z[21];
    z[20]= - 3*z[20] + z[21];
    z[20]=z[20]*z[37];
    z[20]=z[8] + z[20];
    z[20]=z[20]*z[37];
    z[21]= - n<T>(1,4)*z[8] - z[19];
    z[21]=z[6]*z[21];
    z[22]= - z[6]*z[18];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[4]*z[22];
    z[20]=z[20] + z[22] - n<T>(1,4) + z[21];
    z[20]=z[20]*z[35];
    z[21]=n<T>(5,2) - z[7];
    z[21]=z[21]*z[24];
    z[19]=z[21] + z[19];
    z[19]=z[6]*z[19];
    z[19]= - n<T>(5,4)*z[25] + z[19];
    z[19]=z[6]*z[19];
    z[18]= - n<T>(1,2)*z[7] + z[18];
    z[18]=z[6]*z[18];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[6]*z[18];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[4]*z[18];
    z[16]=z[20] + z[16] + z[18] + n<T>(5,8) + z[19];
    z[16]=z[1]*z[16];

    r += z[14] + z[15] + z[16] + n<T>(1,2)*z[17];
 
    return r;
}

template double qg_2lha_r722(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r722(const std::array<dd_real,30>&);
#endif
