#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2022(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=static_cast<T>(4)- z[1];
    z[6]=z[1]*z[6];
    z[7]= - z[4] - static_cast<T>(3)+ 2*z[1];
    z[7]=z[4]*z[7];
    z[6]=z[7] - static_cast<T>(3)+ z[6];
    z[6]=z[4]*z[6];
    z[7]=z[1] - 2;
    z[7]=z[7]*z[1];
    z[7]=z[7] + 1;
    z[6]=z[6] - z[7];
    z[6]=z[3]*z[6];
    z[8]=z[1] - 1;
    z[9]= - 2*z[8] + z[4];
    z[9]=z[4]*z[9];
    z[6]=z[6] + z[9] + z[7];
    z[6]=z[3]*z[6];
    z[9]=npow(z[4],2);
    z[10]=3*z[8];
    z[11]= - z[10] + 2*z[4];
    z[11]=z[11]*z[9];
    z[10]= - z[10] + z[4];
    z[10]=z[4]*z[10];
    z[7]=3*z[7] + z[10];
    z[7]=z[4]*z[7];
    z[10]=z[1] - 3;
    z[10]=z[10]*z[1];
    z[10]=z[10] + 3;
    z[10]=z[10]*z[1];
    z[10]=z[10] - 1;
    z[7]=z[7] - z[10];
    z[7]=z[3]*z[7];
    z[7]=z[7] + z[11] + z[10];
    z[7]=z[3]*z[7];
    z[8]=z[5]*z[8];
    z[10]= - static_cast<T>(2)+ z[5];
    z[10]=z[4]*z[5]*z[10];
    z[10]=3*z[8] + z[10];
    z[9]=z[10]*z[9];
    z[7]=z[9] + z[7];
    z[7]=z[2]*z[7];
    z[9]=z[4] - z[1];
    z[9]=z[9]*npow(z[5],2);
    z[10]= - static_cast<T>(1)+ z[5];
    z[10]=z[5]*z[10];
    z[10]=z[10] - z[9];
    z[10]=z[4]*z[10];
    z[8]=2*z[8] + z[10];
    z[8]=z[4]*z[8];
    z[6]=z[7] + z[8] + z[6];
    z[6]=z[2]*z[6];
    z[7]= - z[4]*z[9];
    z[6]=z[7] + z[6];

    r += z[6]*npow(z[2],2);
 
    return r;
}

template double qg_2lha_r2022(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2022(const std::array<dd_real,30>&);
#endif
