#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1301(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[5];
    z[5]=k[13];
    z[6]=k[9];
    z[7]=k[4];
    z[8]=static_cast<T>(1)+ z[4];
    z[9]=n<T>(1,2)*z[5];
    z[8]=z[9]*z[7]*z[8];
    z[10]=n<T>(1,2)*z[4];
    z[11]=z[3] + z[7];
    z[8]=z[8] - z[10] - n<T>(1,2) - z[11];
    z[8]=z[5]*z[8];
    z[12]=z[4]*z[6];
    z[13]=z[12] + z[6];
    z[14]=z[3]*z[6];
    z[15]=z[14] + static_cast<T>(1)+ z[6];
    z[15]=z[3]*z[15];
    z[15]=z[15] + z[7] - z[1] + z[13];
    z[8]=n<T>(1,2)*z[15] + z[8];
    z[8]=z[2]*z[8];
    z[13]= - 3*z[14] - n<T>(1,2) - z[13];
    z[14]= - n<T>(3,2)*z[7] + z[3] + 1;
    z[15]=n<T>(1,2) - z[7];
    z[15]=z[4]*z[15];
    z[14]=n<T>(1,2)*z[14] + z[15];
    z[14]=z[5]*z[14];
    z[15]=z[7] + 1;
    z[16]=z[3] + z[15];
    z[16]=n<T>(3,2)*z[16] + z[4];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[5]*z[14];
    z[8]=z[8] + n<T>(1,2)*z[13] + z[14];
    z[8]=z[2]*z[8];
    z[13]= - static_cast<T>(1)+ z[4];
    z[14]=n<T>(3,2)*z[3];
    z[16]= - z[14] - static_cast<T>(1)+ n<T>(1,2)*z[7];
    z[16]=n<T>(1,2)*z[16] - z[4];
    z[16]=z[5]*z[16];
    z[13]=n<T>(1,2)*z[13] + z[16];
    z[13]=z[5]*z[13];
    z[12]=z[6] - z[12];
    z[8]=z[8] + n<T>(1,2)*z[12] + z[13];
    z[8]=z[2]*z[8];
    z[12]= - static_cast<T>(1)+ z[6];
    z[10]=z[12]*z[10];
    z[11]= - z[3]*z[11];
    z[11]=z[11] + z[15];
    z[11]=z[4]*z[11];
    z[11]=z[14] + z[11];
    z[9]=z[11]*z[9];
    z[11]= - n<T>(1,2)*z[15] - z[3];
    z[11]=z[4]*z[11];
    z[9]=z[9] + n<T>(3,4) + z[11];
    z[9]=z[5]*z[9];
    z[8]=z[8] + z[10] + z[9];

    r += n<T>(1,2)*z[8];
 
    return r;
}

template double qg_2lha_r1301(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1301(const std::array<dd_real,30>&);
#endif
