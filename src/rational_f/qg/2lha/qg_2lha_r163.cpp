#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r163(const std::array<T,30>& k) {
  T z[31];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=k[10];
    z[8]=k[5];
    z[9]=k[12];
    z[10]=k[14];
    z[11]=n<T>(1,2)*z[6];
    z[12]=z[11]*z[4];
    z[13]= - z[12] + z[6];
    z[14]=z[11] - 1;
    z[13]=z[14]*z[13];
    z[15]=n<T>(1,4)*z[1];
    z[16]=npow(z[9],2);
    z[13]=z[15] + n<T>(1,8)*z[16] - n<T>(1,16) + z[10] + z[13];
    z[13]=z[4]*z[13];
    z[16]=n<T>(5,4)*z[6];
    z[14]= - z[14]*z[16];
    z[17]=n<T>(1,2)*z[1];
    z[18]= - static_cast<T>(1)+ z[17];
    z[18]=z[1]*z[18];
    z[14]=z[14] + z[18] - z[9] - n<T>(3,4) + z[10];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[13]=z[2]*z[13];
    z[14]=n<T>(1,4)*z[9];
    z[18]=static_cast<T>(1)- n<T>(3,2)*z[9];
    z[18]=z[18]*z[14];
    z[19]=n<T>(5,8)*z[6];
    z[20]=z[6] - 1;
    z[21]= - z[20]*z[19];
    z[22]=n<T>(1,2)*z[4];
    z[23]=z[22]*npow(z[6],2);
    z[24]= - n<T>(1,2) + z[6];
    z[24]=z[6]*z[24];
    z[24]= - z[23] - n<T>(1,4) + z[24];
    z[24]=z[4]*z[24];
    z[13]=z[13] + z[24] + z[21] + z[18] - n<T>(9,16) + z[10];
    z[13]=z[2]*z[13];
    z[18]=n<T>(71,3) + 3*z[1];
    z[15]=z[18]*z[15];
    z[18]=n<T>(5,2)*z[6];
    z[15]=z[18] - static_cast<T>(1)+ z[15];
    z[21]=n<T>(5,2) - 9*z[6];
    z[24]= - n<T>(3,8)*z[7] + z[6];
    z[24]=z[4]*z[24];
    z[21]=n<T>(1,4)*z[21] + z[24];
    z[21]=z[4]*z[21];
    z[15]=n<T>(1,2)*z[15] + z[21];
    z[15]=z[4]*z[15];
    z[21]= - n<T>(43,3) - z[1];
    z[21]=z[1]*z[21];
    z[21]= - static_cast<T>(9)+ z[21];
    z[21]=z[21]*z[17];
    z[24]= - static_cast<T>(1)+ z[9];
    z[25]=z[4]*z[7];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[4]*z[25];
    z[21]=n<T>(3,2)*z[25] + 3*z[24] + z[21];
    z[21]=z[4]*z[21];
    z[24]=npow(z[1],2);
    z[25]= - n<T>(77,3) - z[1];
    z[25]=z[1]*z[25];
    z[25]=n<T>(77,3) + z[25];
    z[25]=z[25]*z[24];
    z[21]=n<T>(1,2)*z[25] + z[21];
    z[21]=z[2]*z[21];
    z[25]=z[1] + n<T>(37,3);
    z[26]=n<T>(1,4)*z[24];
    z[27]=z[25]*z[26];
    z[15]=n<T>(1,4)*z[21] + z[27] + z[15];
    z[15]=z[2]*z[15];
    z[21]=n<T>(1,3)*z[6];
    z[27]=37*z[5];
    z[28]= - static_cast<T>(25)+ z[27];
    z[28]=z[6]*z[28];
    z[28]= - static_cast<T>(175)+ z[28];
    z[28]=z[28]*z[21];
    z[28]= - static_cast<T>(1)+ z[28];
    z[29]=z[4]*z[6];
    z[30]= - static_cast<T>(1)+ 3*z[7];
    z[30]=n<T>(1,4)*z[30] - z[6];
    z[30]=z[30]*z[29];
    z[28]=n<T>(1,4)*z[28] + z[30];
    z[28]=z[4]*z[28];
    z[30]=static_cast<T>(43)+ z[27];
    z[30]=z[30]*z[21];
    z[25]=z[30] - z[25];
    z[25]=n<T>(1,2)*z[25] + z[28];
    z[25]=z[4]*z[25];
    z[25]= - z[26] + z[25];
    z[26]= - z[5]*z[21];
    z[26]=static_cast<T>(1)+ z[26];
    z[26]=z[26]*z[29];
    z[28]= - static_cast<T>(1)- z[5];
    z[28]=z[6]*z[28];
    z[26]=z[26] + static_cast<T>(1)+ z[28];
    z[26]=z[26]*npow(z[4],2);
    z[28]=z[4] - static_cast<T>(1)+ z[1];
    z[28]=z[2]*z[28];
    z[28]=static_cast<T>(1)- z[28];
    z[28]=z[28]*npow(z[1],3);
    z[24]= - z[4]*z[24];
    z[24]=z[24] - n<T>(1,3)*z[28];
    z[24]=z[2]*z[24];
    z[24]=z[26] + z[24];
    z[24]=z[3]*z[24];
    z[15]=5*z[24] + n<T>(1,2)*z[25] + z[15];
    z[15]=z[3]*z[15];
    z[24]=3*z[9];
    z[25]=n<T>(59,6) + z[1];
    z[25]=z[1]*z[25];
    z[25]= - n<T>(107,6) + z[25];
    z[25]=z[1]*z[25];
    z[18]= - z[18] + z[25] - n<T>(1,2) + z[24];
    z[24]=static_cast<T>(7)- z[24];
    z[24]=z[24]*z[14];
    z[25]= - z[29] + z[20];
    z[22]=z[25]*z[22];
    z[19]=z[22] + z[19] - n<T>(3,8)*z[1] + z[24] - n<T>(5,4) + z[10];
    z[19]=z[4]*z[19];
    z[18]=n<T>(1,4)*z[18] + z[19];
    z[18]=z[2]*z[18];
    z[19]= - static_cast<T>(1)+ 9*z[9];
    z[22]= - n<T>(29,12) - z[1];
    z[22]=z[1]*z[22];
    z[19]= - n<T>(15,4)*z[6] + n<T>(1,2)*z[19] + z[22];
    z[22]= - n<T>(3,2) - z[6];
    z[22]=z[6]*z[22];
    z[22]=z[23] + n<T>(3,4)*z[7] + z[22];
    z[22]=z[4]*z[22];
    z[16]=static_cast<T>(7)+ z[16];
    z[16]=z[6]*z[16];
    z[16]=n<T>(3,4) + z[16];
    z[16]=n<T>(1,2)*z[16] + z[22];
    z[16]=z[4]*z[16];
    z[16]=z[18] + n<T>(1,2)*z[19] + z[16];
    z[16]=z[2]*z[16];
    z[18]=static_cast<T>(11)- n<T>(37,4)*z[5];
    z[18]=z[18]*z[21];
    z[18]=n<T>(51,4) + z[18];
    z[18]=z[6]*z[18];
    z[18]=n<T>(1,2) + z[18];
    z[19]=static_cast<T>(1)- n<T>(3,2)*z[7];
    z[19]=n<T>(1,2)*z[19] + z[6];
    z[19]=z[19]*z[29];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[4]*z[18];
    z[19]= - static_cast<T>(31)- z[27];
    z[19]=z[6]*z[19];
    z[19]=n<T>(1,6)*z[19] + n<T>(37,6) + z[1];
    z[16]=z[16] + n<T>(1,4)*z[19] + z[18];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[3]*z[15];
    z[16]= - static_cast<T>(1)- n<T>(37,12)*z[6];
    z[16]=z[6]*z[16];
    z[12]=z[16] + z[12];
    z[12]=z[15] + n<T>(1,4)*z[12] + z[13];
    z[12]=z[3]*z[12];
    z[13]=z[20]*z[11];
    z[15]=n<T>(1,2)*z[8];
    z[16]=z[15] - z[10];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[9]*z[8]*z[14];
    z[13]=z[13] + z[14] - z[16];
    z[13]=z[4]*z[13];
    z[14]=z[8] + 1;
    z[11]=z[14]*z[11];
    z[14]= - z[11] + static_cast<T>(1)+ n<T>(3,2)*z[8];
    z[14]=z[6]*z[14];
    z[18]=n<T>(1,2)*z[9];
    z[13]=z[13] + z[14] - z[17] + z[18] - z[8] + n<T>(5,8) + z[10];
    z[13]=z[2]*z[13];
    z[14]=static_cast<T>(1)+ z[15];
    z[14]=z[14]*z[18];
    z[14]= - z[8] + z[14];
    z[14]=z[9]*z[14];
    z[11]= - z[11] + n<T>(1,2) + z[8];
    z[11]=z[6]*z[11];
    z[11]=z[13] + z[23] + z[11] + z[14] - z[16];
    z[11]=z[2]*z[11];
    z[11]=n<T>(1,2)*z[11] + z[12];

    r += n<T>(1,2)*z[11];
 
    return r;
}

template double qg_2lha_r163(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r163(const std::array<dd_real,30>&);
#endif
