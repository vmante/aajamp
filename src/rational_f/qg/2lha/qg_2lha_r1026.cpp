#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1026(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[3];
    z[6]=k[10];
    z[7]=k[9];
    z[8]=z[5] + 1;
    z[9]=n<T>(1,2)*z[6];
    z[8]=z[8]*z[9];
    z[10]=z[5] - n<T>(3,2);
    z[8]=z[8] - z[10];
    z[8]=z[8]*z[9];
    z[11]=n<T>(1,4)*z[7];
    z[12]=z[6]*z[5];
    z[13]= - z[12]*z[11];
    z[13]=z[13] - z[5] + z[8];
    z[13]=z[7]*z[13];
    z[14]=z[7]*z[5];
    z[14]=z[12] - z[14];
    z[14]=z[2]*z[14];
    z[15]= - z[7]*z[12];
    z[14]=z[15] + z[14];
    z[14]=z[4]*z[14];
    z[13]=z[14] - static_cast<T>(1)+ z[13];
    z[14]=n<T>(1,2)*z[7];
    z[15]= - z[14] + 1;
    z[15]=z[5]*z[15];
    z[15]=n<T>(3,2) + z[15];
    z[15]=z[15]*z[14];
    z[8]=z[15] - z[8] + static_cast<T>(3)+ z[1];
    z[11]=z[11] - n<T>(1,4)*z[6];
    z[15]=z[5] - n<T>(1,2);
    z[11]=z[15]*z[11];
    z[16]=z[5] - z[1];
    z[11]= - static_cast<T>(1)+ z[11] + n<T>(1,2)*z[16];
    z[11]=z[2]*z[11];
    z[8]=n<T>(1,2)*z[8] + z[11];
    z[8]=z[2]*z[8];
    z[8]=z[8] + n<T>(1,2)*z[13];
    z[8]=z[4]*z[8];
    z[11]=z[5]*z[9];
    z[11]=z[11] - z[15];
    z[11]=z[11]*z[9];
    z[13]=z[5] - 1;
    z[16]=z[13]*z[14];
    z[15]=z[16] - z[15];
    z[15]=z[15]*z[14];
    z[16]=z[1] + n<T>(1,2);
    z[11]=z[15] + z[11] - z[16];
    z[11]=z[2]*z[11];
    z[15]=z[6] + 1;
    z[17]= - static_cast<T>(1)+ z[7];
    z[17]=z[7]*z[17];
    z[17]=z[17] + z[15];
    z[11]=n<T>(1,2)*z[17] + z[11];
    z[11]=z[2]*z[11];
    z[17]=static_cast<T>(1)+ z[9];
    z[17]=z[17]*z[7]*z[6];
    z[15]=z[17] - z[15];
    z[14]=z[15]*z[14];
    z[11]=z[14] + z[11];
    z[8]=n<T>(1,2)*z[11] + z[8];
    z[8]=z[4]*z[8];
    z[11]= - static_cast<T>(1)- n<T>(3,2)*z[3];
    z[11]=z[11]*z[12];
    z[10]=z[3]*z[10];
    z[10]=z[11] + z[10];
    z[9]=z[10]*z[9];
    z[10]=npow(z[7],2);
    z[11]=z[13]*z[10];
    z[12]=z[3]*z[16];
    z[9]= - n<T>(1,4)*z[11] + z[12] + z[9];
    z[9]=z[2]*z[9];
    z[11]=npow(z[6],2);
    z[10]= - z[11] - n<T>(1,2)*z[10];
    z[9]=n<T>(1,2)*z[10] + z[9];
    z[9]=z[2]*z[9];

    r += z[8] + n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r1026(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1026(const std::array<dd_real,30>&);
#endif
