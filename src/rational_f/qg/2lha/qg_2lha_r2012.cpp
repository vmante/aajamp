#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2012(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[9];
    z[4]=k[7];
    z[5]=k[17];
    z[6]=k[4];
    z[7]=k[11];
    z[8]=k[12];
    z[9]=k[15];
    z[10]=k[20];
    z[11]=z[6]*z[3];
    z[12]=z[10] - z[9];
    z[13]=z[12]*z[11];
    z[14]=z[3]*z[10];
    z[13]=z[14] + n<T>(1,2)*z[13];
    z[13]=z[6]*z[13];
    z[15]=n<T>(1,2)*z[6];
    z[15]=z[15]*z[12];
    z[16]= - z[15] - z[10] + z[7];
    z[16]=z[6]*z[16];
    z[17]=z[8] - z[7] + z[12];
    z[17]=z[2]*z[17]*npow(z[6],2);
    z[16]=z[16] + n<T>(1,2)*z[17];
    z[16]=z[2]*z[16];
    z[14]= - z[7] + z[14];
    z[13]=z[16] + n<T>(1,2)*z[14] + z[13];
    z[13]=z[4]*z[13];
    z[14]=z[9] - z[8];
    z[15]= - z[15] - z[14];
    z[15]=z[2]*z[15];
    z[15]=static_cast<T>(1)+ 3*z[15];
    z[15]=z[2]*z[6]*z[15];
    z[16]= - static_cast<T>(1)+ z[1];
    z[16]=z[3]*z[16];
    z[13]=3*z[13] + z[15] + z[16] - z[11];
    z[13]=z[4]*z[13];
    z[12]= - z[6]*z[12];
    z[12]=z[12] - z[14];
    z[12]=z[2]*z[12];
    z[14]=z[3]*z[1];
    z[15]=static_cast<T>(1)- z[14];
    z[15]=z[3]*z[15];
    z[11]=n<T>(3,2)*z[12] + z[15] + z[11];
    z[11]=z[2]*z[11];
    z[12]=z[1] - z[14];
    z[12]=z[3]*z[5]*z[12];
    z[11]=z[13] + z[12] + z[11];

    r += n<T>(1,4)*z[11];
 
    return r;
}

template double qg_2lha_r2012(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2012(const std::array<dd_real,30>&);
#endif
