#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r971(const std::array<T,30>& k) {
  T z[68];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[6];
    z[7]=k[11];
    z[8]=k[9];
    z[9]=k[14];
    z[10]=k[24];
    z[11]=k[3];
    z[12]=k[5];
    z[13]=k[27];
    z[14]=k[13];
    z[15]=n<T>(3,2)*z[7];
    z[16]=n<T>(1,4)*z[6];
    z[17]=n<T>(7,2)*z[10];
    z[18]= - z[17] - z[15] - n<T>(29,2)*z[13] - n<T>(13,2)*z[5] - z[16] + n<T>(403,9)
    + n<T>(1,4)*z[14];
    z[18]=n<T>(1,2)*z[18] + n<T>(197,9)*z[2];
    z[18]=z[12]*z[18];
    z[19]=static_cast<T>(17)+ 7*z[10];
    z[20]=n<T>(1,2)*z[10];
    z[21]=z[19]*z[20];
    z[22]=n<T>(1,2)*z[6];
    z[23]=n<T>(1,2)*z[14];
    z[24]=z[22] - z[23];
    z[25]= - static_cast<T>(1)+ z[24];
    z[26]=5*z[5];
    z[18]=z[18] - z[21] - z[7] - n<T>(11,2)*z[13] + n<T>(1,2)*z[25] - z[26];
    z[25]=n<T>(1,2)*z[12];
    z[18]=z[18]*z[25];
    z[27]=n<T>(1,2)*z[7];
    z[28]= - n<T>(197,9) - z[5];
    z[29]= - static_cast<T>(1)+ n<T>(7,8)*z[13];
    z[29]=z[13]*z[29];
    z[30]=n<T>(7,8)*z[10];
    z[31]= - static_cast<T>(1)+ z[30];
    z[31]=z[10]*z[31];
    z[28]=z[31] - z[27] + n<T>(1,2)*z[28] + z[29];
    z[28]=z[12]*z[28];
    z[29]=n<T>(1,2)*z[5];
    z[31]=z[29] + z[27];
    z[32]=n<T>(7,4)*z[10];
    z[33]=z[32] - 1;
    z[34]=z[33]*z[10];
    z[35]=n<T>(7,4)*z[13];
    z[36]= - static_cast<T>(1)+ z[35];
    z[36]=z[13]*z[36];
    z[28]=z[28] + z[34] + z[36] - z[31];
    z[28]=z[28]*z[25];
    z[36]=npow(z[13],2);
    z[37]=n<T>(7,16)*z[36];
    z[38]=npow(z[8],2);
    z[28]=z[28] + z[37] - n<T>(47,9)*z[38];
    z[28]=z[4]*z[28];
    z[39]=z[34] - z[27];
    z[40]=z[35] - n<T>(7,4)*z[5] + z[9] - z[39];
    z[41]= - static_cast<T>(1)- z[24];
    z[42]=n<T>(94,3)*z[8];
    z[41]=n<T>(1,4)*z[41] - z[42];
    z[41]=z[8]*z[41];
    z[43]=z[38]*z[2];
    z[18]=z[28] + z[18] + n<T>(94,9)*z[43] + n<T>(1,2)*z[40] + z[41];
    z[18]=z[4]*z[18];
    z[28]= - z[9] - z[39];
    z[39]=n<T>(1,9)*z[8];
    z[40]= - n<T>(179,4) + 470*z[8];
    z[40]=z[40]*z[39];
    z[28]= - n<T>(47,9)*z[43] + n<T>(1,2)*z[28] + z[40];
    z[28]=z[2]*z[28];
    z[40]=n<T>(3,2)*z[6];
    z[41]= - n<T>(1261,9) + z[40];
    z[41]=n<T>(21,2)*z[10] + n<T>(5,2)*z[7] + n<T>(1,2)*z[41] - z[26];
    z[44]=n<T>(103,9)*z[2];
    z[41]=n<T>(1,2)*z[41] + z[44];
    z[45]=n<T>(1,2)*z[2];
    z[46]= - static_cast<T>(1)- z[45];
    z[46]=z[46]*z[44];
    z[47]=3*z[5];
    z[48]=z[47] - n<T>(5,4);
    z[46]= - n<T>(1,4)*z[48] + z[46];
    z[46]=z[12]*z[46];
    z[41]=n<T>(1,2)*z[41] + z[46];
    z[41]=z[12]*z[41];
    z[46]=n<T>(1,2)*z[8];
    z[49]=3*z[6];
    z[50]=n<T>(1261,9) - z[49];
    z[50]=n<T>(1,4)*z[50] - n<T>(1175,9)*z[8];
    z[50]=z[50]*z[46];
    z[51]=n<T>(3,4)*z[7];
    z[52]=z[51] + z[9];
    z[53]=n<T>(11,8) - z[5];
    z[54]=static_cast<T>(9)+ z[17];
    z[54]=z[10]*z[54];
    z[18]=z[18] + z[41] + z[28] + z[50] + n<T>(3,8)*z[54] + n<T>(1,2)*z[53] + 
    z[52];
    z[18]=z[4]*z[18];
    z[28]=static_cast<T>(5)+ z[32];
    z[28]=z[28]*z[20];
    z[41]=n<T>(47,9)*z[8];
    z[50]=static_cast<T>(1)- 4*z[8];
    z[50]=z[50]*z[41];
    z[34]= - z[34] - z[9] - z[7];
    z[34]=n<T>(1,2)*z[34] + z[50];
    z[34]=z[2]*z[34];
    z[50]=47*z[8];
    z[53]= - n<T>(349,12) + z[50];
    z[53]=z[8]*z[53];
    z[28]=z[34] + n<T>(5,3)*z[53] + z[28] + n<T>(5,4) + z[7];
    z[28]=z[2]*z[28];
    z[34]= - static_cast<T>(5)+ z[6];
    z[34]= - n<T>(3,4)*z[10] - z[27] + n<T>(3,8)*z[34] + z[9];
    z[53]=n<T>(421,4) - 103*z[2];
    z[53]=z[2]*z[53];
    z[53]=n<T>(1,4) + n<T>(1,9)*z[53];
    z[53]=z[53]*z[25];
    z[54]=static_cast<T>(27)- z[16];
    z[54]=n<T>(3,2)*z[54] - n<T>(517,9)*z[8];
    z[54]=z[8]*z[54];
    z[18]=z[18] + z[53] + z[28] + n<T>(1,2)*z[34] + z[54];
    z[18]=z[4]*z[18];
    z[28]=n<T>(1,3)*z[8];
    z[34]=235*z[8];
    z[53]= - n<T>(1871,12) + z[34];
    z[53]=z[53]*z[28];
    z[38]= - n<T>(94,9)*z[38] - z[52];
    z[38]=z[2]*z[38];
    z[52]=n<T>(9,4)*z[10];
    z[54]=z[52] + z[7];
    z[55]=n<T>(11,4) - z[9];
    z[38]=z[38] + z[53] + n<T>(1,2)*z[55] + z[54];
    z[38]=z[2]*z[38];
    z[53]=n<T>(3661,12) - 376*z[8];
    z[53]=z[53]*z[28];
    z[55]=n<T>(1,4)*z[7];
    z[56]=3*z[9];
    z[57]=n<T>(1,2)*z[1];
    z[58]= - static_cast<T>(3)+ z[57];
    z[38]=z[38] + z[53] - z[52] + z[55] + n<T>(1,2)*z[58] + z[56];
    z[38]=z[2]*z[38];
    z[53]=9*z[10];
    z[58]=3*z[7] - n<T>(797,9) - 7*z[5];
    z[58]=n<T>(403,9)*z[2] + n<T>(1,2)*z[58] + z[53];
    z[59]= - z[27] - n<T>(103,9) - n<T>(3,2)*z[5];
    z[59]=n<T>(1,2)*z[59] - z[44];
    z[59]=z[12]*z[59];
    z[58]=n<T>(1,2)*z[58] + z[59];
    z[58]=z[12]*z[58];
    z[59]=z[5] + n<T>(9,4)*z[13];
    z[60]=z[59] + z[54];
    z[61]=n<T>(403,18) - z[60];
    z[61]=z[61]*z[25];
    z[60]=z[61] - n<T>(403,36) - z[60];
    z[60]=z[12]*z[60];
    z[61]=94*z[8];
    z[62]=n<T>(385,4) + z[61];
    z[62]=z[62]*z[39];
    z[59]=z[9] - z[59];
    z[59]=z[60] + n<T>(1,2)*z[59] + z[62];
    z[59]=z[4]*z[59];
    z[60]= - n<T>(385,6) - z[61];
    z[60]=z[60]*z[28];
    z[60]= - z[9] + z[60];
    z[60]=z[2]*z[60];
    z[62]= - static_cast<T>(5)- z[1];
    z[62]=z[5]*z[62];
    z[62]=n<T>(13,2) + z[62];
    z[62]=n<T>(1,2)*z[62] + z[9];
    z[63]=n<T>(779,36) + z[50];
    z[63]=z[8]*z[63];
    z[58]=z[59] + z[58] + z[60] + z[63] + n<T>(1,2)*z[62] + z[54];
    z[58]=z[4]*z[58];
    z[59]=329*z[8];
    z[60]=n<T>(38,3) - z[59];
    z[60]=z[60]*z[28];
    z[62]=n<T>(385,12) + z[61];
    z[62]=z[62]*z[28];
    z[62]=z[9] + z[62];
    z[62]=z[2]*z[62];
    z[63]=n<T>(13,4) + z[9];
    z[54]=z[62] + z[60] + n<T>(1,2)*z[63] + z[54];
    z[54]=z[2]*z[54];
    z[34]= - n<T>(331,8) + z[34];
    z[34]=z[34]*z[28];
    z[60]= - n<T>(1,2) - z[2];
    z[60]=z[60]*z[44];
    z[60]=z[60] + n<T>(53,3) + z[51];
    z[60]=z[12]*z[60];
    z[62]=z[55] + z[9];
    z[63]= - n<T>(3,2) + z[1];
    z[34]=z[58] + z[60] + z[54] + z[34] - n<T>(27,8)*z[10] + n<T>(1,4)*z[63] - 
    z[62];
    z[34]=z[4]*z[34];
    z[54]=n<T>(1,2) - z[1];
    z[54]= - z[7] + n<T>(1,2)*z[54] - z[56];
    z[58]= - n<T>(895,2) + 517*z[8];
    z[58]=z[58]*z[39];
    z[34]=z[34] + z[38] + n<T>(1,2)*z[54] + z[58];
    z[34]=z[4]*z[34];
    z[38]= - static_cast<T>(2)- z[8];
    z[38]=z[38]*z[41];
    z[54]= - n<T>(103,9) - z[31];
    z[54]=z[54]*z[25];
    z[54]=z[54] - z[51] + n<T>(103,9) - n<T>(3,4)*z[5];
    z[54]=z[12]*z[54];
    z[58]=z[1] + 3;
    z[60]=n<T>(1,4)*z[58];
    z[63]= - z[5]*z[60];
    z[38]=z[54] + z[38] + z[63] - z[9];
    z[38]=z[4]*z[38];
    z[54]=z[60] + z[51];
    z[63]=188*z[8];
    z[64]= - static_cast<T>(179)- z[63];
    z[64]=z[64]*z[39];
    z[65]=n<T>(94,9)*z[8];
    z[66]=static_cast<T>(1)+ z[8];
    z[66]=z[66]*z[65];
    z[66]=z[9] + z[66];
    z[66]=z[2]*z[66];
    z[67]= - n<T>(206,9)*z[2] + n<T>(206,9) + z[7];
    z[67]=z[12]*z[67];
    z[38]=z[38] + z[67] + 2*z[66] + z[64] - z[56] + z[54];
    z[38]=z[4]*z[38];
    z[54]=z[54] + z[56];
    z[64]= - static_cast<T>(3)+ n<T>(188,3)*z[8];
    z[64]=z[8]*z[64];
    z[43]= - n<T>(94,3)*z[43] + z[64] + z[54];
    z[43]=z[2]*z[43];
    z[42]=static_cast<T>(3)- z[42];
    z[42]=z[8]*z[42];
    z[38]=z[38] + z[43] + z[42] - z[54];
    z[38]=z[4]*z[38];
    z[42]=static_cast<T>(197)- z[63];
    z[42]=z[42]*z[28];
    z[43]= - static_cast<T>(1)+ z[8];
    z[43]=z[43]*z[65];
    z[43]= - z[9] + z[43];
    z[64]=z[55]*z[2];
    z[43]=2*z[43] - z[64];
    z[43]=z[2]*z[43];
    z[42]=z[43] + z[42] + z[54];
    z[42]=z[2]*z[42];
    z[43]= - z[7] - z[58];
    z[54]= - static_cast<T>(103)+ z[61];
    z[54]=z[8]*z[54];
    z[42]=z[42] + n<T>(1,2)*z[43] + n<T>(2,3)*z[54];
    z[42]=z[2]*z[42];
    z[43]=static_cast<T>(215)- z[63];
    z[43]=z[43]*z[39];
    z[38]=z[38] + z[42] + z[43] + z[60] - z[9];
    z[38]=z[4]*z[38];
    z[42]=static_cast<T>(2)- z[8];
    z[42]=z[42]*z[41];
    z[43]=z[51] - z[9];
    z[42]=z[64] + z[42] - n<T>(215,36) - z[43];
    z[42]=z[2]*z[42];
    z[54]= - static_cast<T>(385)+ z[63];
    z[54]=z[54]*z[39];
    z[58]=n<T>(851,9) + z[1];
    z[42]=z[42] + z[54] + z[51] + n<T>(1,4)*z[58] - z[56];
    z[42]=z[2]*z[42];
    z[51]=static_cast<T>(197)- z[61];
    z[51]=z[51]*z[28];
    z[54]=3*z[1];
    z[58]= - n<T>(421,3) - z[54];
    z[42]=z[42] + z[51] - z[55] + n<T>(1,4)*z[58] + z[56];
    z[42]=z[2]*z[42];
    z[51]= - static_cast<T>(403)+ z[63];
    z[51]=z[51]*z[39];
    z[54]=n<T>(833,9) + z[54];
    z[42]=z[42] + z[51] + n<T>(1,4)*z[54] - z[9];
    z[42]=z[2]*z[42];
    z[51]=z[8] - z[9];
    z[54]=z[51]*z[2];
    z[58]=z[54] - z[51];
    z[60]=n<T>(1,2)*z[4];
    z[63]= - z[51]*z[60];
    z[58]=2*z[58] + z[63];
    z[58]=z[4]*z[58];
    z[63]=2*z[51];
    z[64]=z[63] - z[54];
    z[64]=z[2]*z[64];
    z[64]=z[64] - z[51];
    z[58]=3*z[64] + z[58];
    z[58]=z[4]*z[58];
    z[64]=3*z[51];
    z[54]= - z[64] + z[54];
    z[54]=z[2]*z[54];
    z[54]=z[64] + z[54];
    z[54]=z[2]*z[54];
    z[54]=z[54] - z[51];
    z[54]=2*z[54] + z[58];
    z[54]=z[4]*z[54];
    z[58]= - z[51]*z[45];
    z[58]=z[63] + z[58];
    z[58]=z[2]*z[58];
    z[58]= - z[64] + z[58];
    z[58]=z[2]*z[58];
    z[58]=z[63] + z[58];
    z[58]=z[2]*z[58];
    z[51]=z[54] - n<T>(1,2)*z[51] + z[58];
    z[51]=z[3]*z[51];
    z[54]=static_cast<T>(103)- z[50];
    z[54]=z[54]*z[39];
    z[58]= - n<T>(103,9) - z[57];
    z[38]=z[51] + z[38] + z[42] + n<T>(1,2)*z[58] + z[54];
    z[38]=z[3]*z[38];
    z[42]=n<T>(47,3)*z[8];
    z[51]=n<T>(5,3) - z[8];
    z[51]=z[51]*z[42];
    z[52]= - z[52] + z[9] + z[27];
    z[52]=z[52]*z[45];
    z[15]=n<T>(9,2)*z[10] - z[15] - n<T>(725,36) - z[9];
    z[15]=z[52] + n<T>(1,2)*z[15] + z[51];
    z[15]=z[2]*z[15];
    z[51]=n<T>(713,6) + z[1];
    z[52]= - n<T>(1853,24) + z[50];
    z[52]=z[8]*z[52];
    z[15]=z[15] + z[52] - n<T>(9,8)*z[10] + n<T>(1,4)*z[51] + z[43];
    z[15]=z[2]*z[15];
    z[43]= - z[27] + z[56] - n<T>(701,12) - z[1];
    z[51]=n<T>(913,12) - z[50];
    z[51]=z[8]*z[51];
    z[15]=z[15] + n<T>(1,2)*z[43] + z[51];
    z[15]=z[2]*z[15];
    z[43]= - n<T>(1799,24) + z[50];
    z[28]=z[43]*z[28];
    z[43]=n<T>(689,18) + z[1];
    z[43]=n<T>(1,2)*z[43] - z[9];
    z[15]=z[38] + z[34] + z[15] + n<T>(1,2)*z[43] + z[28];
    z[15]=z[3]*z[15];
    z[28]=npow(z[10],2);
    z[34]=z[28]*z[2];
    z[19]=z[10]*z[19];
    z[19]=n<T>(7,8)*z[34] - n<T>(1,4)*z[19] + z[9] - z[55];
    z[19]=z[19]*z[45];
    z[38]=static_cast<T>(13)+ z[17];
    z[38]=z[10]*z[38];
    z[43]=n<T>(1853,4) - z[59];
    z[43]=z[8]*z[43];
    z[19]=z[19] + n<T>(1,18)*z[43] + n<T>(1,8)*z[38] - n<T>(35,6) - z[9];
    z[19]=z[2]*z[19];
    z[38]=n<T>(277,3) - z[57];
    z[38]=z[10] + n<T>(1,4)*z[38] + z[62];
    z[43]= - n<T>(1745,4) + z[59];
    z[43]=z[43]*z[39];
    z[19]=z[19] + n<T>(1,2)*z[38] + z[43];
    z[19]=z[2]*z[19];
    z[38]=static_cast<T>(1)- z[57];
    z[38]=z[38]*z[16];
    z[43]=n<T>(823,9) - z[22];
    z[43]=n<T>(1,2)*z[43] - n<T>(329,9)*z[8];
    z[43]=z[8]*z[43];
    z[38]=z[43] + z[38] - n<T>(35,3) + n<T>(1,4)*z[1];
    z[15]=z[15] + z[18] + n<T>(1,2)*z[38] + z[19];
    z[15]=z[3]*z[15];
    z[18]=n<T>(1,2)*z[13];
    z[19]= - static_cast<T>(1)+ n<T>(21,2)*z[13];
    z[19]=z[19]*z[18];
    z[38]= - static_cast<T>(15)+ z[17];
    z[38]=z[38]*z[20];
    z[19]=z[38] - z[7] + z[19] - z[47] - n<T>(197,9) + z[24];
    z[19]=z[19]*z[25];
    z[24]=z[6] - z[14];
    z[38]=n<T>(1,2)*z[24];
    z[43]= - z[38] - z[5];
    z[35]=static_cast<T>(1)+ z[35];
    z[35]=z[13]*z[35];
    z[50]= - static_cast<T>(1)+ n<T>(21,4)*z[10];
    z[50]=z[10]*z[50];
    z[19]=z[19] + z[50] + z[27] + n<T>(1,2)*z[43] + z[35];
    z[19]=z[19]*z[25];
    z[27]=z[10] + z[13];
    z[31]= - z[31] - z[27];
    z[35]=npow(z[12],2);
    z[31]=z[31]*z[35]*z[60];
    z[24]=n<T>(1,8)*z[24] + z[41];
    z[24]=z[8]*z[24];
    z[19]=z[31] + z[19] - z[37] + z[24];
    z[19]=z[4]*z[19];
    z[24]=static_cast<T>(1)- 21*z[10];
    z[24]=z[24]*z[20];
    z[31]=n<T>(3,2)*z[14];
    z[24]=z[24] + z[7] - n<T>(5,2)*z[13] - 17*z[5] - z[40] + n<T>(295,9) - z[31]
   ;
    z[37]= - static_cast<T>(1)+ z[2];
    z[37]=z[2]*z[37];
    z[24]=n<T>(1,2)*z[24] + z[37];
    z[37]=n<T>(197,9) - z[45];
    z[37]=z[37]*z[45];
    z[40]= - n<T>(11,8)*z[13] - n<T>(5,8)*z[5] + static_cast<T>(1)+ n<T>(1,16)*z[14];
    z[37]=3*z[40] + z[37];
    z[37]=z[12]*z[37];
    z[24]=n<T>(1,4)*z[24] + z[37];
    z[24]=z[12]*z[24];
    z[37]=z[2]*z[8];
    z[40]=n<T>(1,4)*z[37];
    z[43]=n<T>(1,4) - z[41];
    z[43]=z[8]*z[43];
    z[43]=z[43] - z[40];
    z[43]=z[2]*z[43];
    z[50]=static_cast<T>(1)- z[17];
    z[50]=z[10]*z[50];
    z[50]=z[50] - z[5] - n<T>(3,8)*z[13];
    z[51]=n<T>(7,2)*z[6] - n<T>(295,9) + z[23];
    z[51]=n<T>(1,8)*z[51] + n<T>(188,9)*z[8];
    z[51]=z[8]*z[51];
    z[19]=z[19] + z[24] + z[43] + n<T>(1,2)*z[50] + z[51];
    z[19]=z[4]*z[19];
    z[20]= - z[33]*z[20];
    z[24]=n<T>(1,3) - z[8];
    z[24]=z[24]*z[42];
    z[20]= - z[40] + z[20] + z[24];
    z[20]=z[2]*z[20];
    z[24]=static_cast<T>(1)- z[5];
    z[33]=n<T>(1,4) - z[44];
    z[33]=z[2]*z[33];
    z[24]=n<T>(1,2)*z[24] + z[33];
    z[24]=z[12]*z[24];
    z[33]= - n<T>(367,18) + z[2];
    z[33]=z[2]*z[33];
    z[33]=z[33] + n<T>(13,2) - z[5];
    z[24]=n<T>(1,2)*z[33] + z[24];
    z[24]=z[24]*z[25];
    z[33]= - n<T>(5,2)*z[10] + z[7] + n<T>(9,2) - z[6];
    z[42]= - n<T>(275,9) + z[6];
    z[42]=n<T>(1,2)*z[42] + n<T>(235,9)*z[8];
    z[42]=z[8]*z[42];
    z[19]=z[19] + z[24] + z[20] + n<T>(1,8)*z[33] + z[42];
    z[19]=z[4]*z[19];
    z[20]= - z[21] - n<T>(125,9) + z[7];
    z[21]=static_cast<T>(113)- z[61];
    z[21]=z[21]*z[39];
    z[24]=n<T>(7,16)*z[34] - n<T>(1,8)*z[7] - z[10];
    z[24]=z[2]*z[24];
    z[20]=z[24] + n<T>(1,8)*z[20] + z[21];
    z[20]=z[2]*z[20];
    z[21]= - n<T>(1565,9) + z[49];
    z[21]=n<T>(1,16)*z[21] + z[65];
    z[21]=z[8]*z[21];
    z[24]=3*z[10];
    z[33]=z[24] + n<T>(259,9) - z[6];
    z[15]=z[15] + z[19] + z[20] + n<T>(1,16)*z[33] + z[21];
    z[15]=z[3]*z[15];
    z[19]=z[11] - 1;
    z[20]=z[13]*z[11];
    z[21]=z[19]*z[20];
    z[33]= - n<T>(7,2)*z[21] - n<T>(1,2) - 5*z[11];
    z[18]=z[33]*z[18];
    z[33]=z[11] + 1;
    z[32]= - z[33]*z[32];
    z[32]=z[32] - n<T>(5,2);
    z[32]=z[11]*z[32];
    z[32]=n<T>(7,4) - z[5] + z[32];
    z[32]=z[10]*z[32];
    z[34]=z[10] + 1;
    z[39]= - z[8]*z[34];
    z[37]=z[37] - z[8];
    z[42]=n<T>(3,2) + z[37];
    z[42]=z[2]*z[42];
    z[18]=z[42] + z[39] + z[32] + z[18] - n<T>(3,4)*z[11] - z[48];
    z[32]=z[23]*z[11];
    z[39]= - z[14] + z[32] - n<T>(3,2);
    z[42]=3*z[11];
    z[43]=z[39]*z[42];
    z[44]=n<T>(1,2)*z[11];
    z[48]=z[44] - 1;
    z[49]=z[48]*z[11];
    z[49]=z[49] + n<T>(1,2);
    z[20]=z[49]*z[20];
    z[19]=15*z[19] + 7*z[20];
    z[19]=z[13]*z[19];
    z[49]=static_cast<T>(15)+ z[14];
    z[19]=z[19] + z[43] + n<T>(3,2)*z[49] - z[26];
    z[26]=z[11]*z[33];
    z[33]=npow(z[11],3)*z[30];
    z[26]=z[33] + z[26] + static_cast<T>(1)- z[29];
    z[26]=z[10]*z[26];
    z[29]= - z[34]*z[46];
    z[19]=z[29] + n<T>(1,4)*z[19] + z[26];
    z[26]= - z[40] - z[48];
    z[26]=z[2]*z[26];
    z[19]=n<T>(1,2)*z[19] + z[26];
    z[19]=z[12]*z[19];
    z[18]=n<T>(1,4)*z[18] + z[19];
    z[18]=z[12]*z[18];
    z[19]=z[24] + 1;
    z[26]= - npow(z[2],2);
    z[26]=z[26] - z[19];
    z[26]=z[8]*z[26];
    z[29]=static_cast<T>(1)+ z[44];
    z[29]=z[29]*z[17];
    z[29]=z[29] + 1;
    z[29]=z[11]*z[29];
    z[29]=n<T>(5,2) - z[47] + z[29];
    z[29]=z[10]*z[29];
    z[33]= - n<T>(7,2) - z[14];
    z[34]=static_cast<T>(1)+ n<T>(3,4)*z[14];
    z[34]=z[11]*z[34];
    z[40]=n<T>(1,4)*z[11];
    z[43]= - static_cast<T>(1)+ z[40];
    z[43]=z[11]*z[43];
    z[43]=n<T>(3,4) + z[43];
    z[43]=z[13]*z[43];
    z[43]=7*z[43] + static_cast<T>(7)+ z[11];
    z[43]=z[13]*z[43];
    z[26]=z[29] + z[43] + n<T>(3,2)*z[33] + z[34] + z[26];
    z[25]=z[26]*z[25];
    z[26]= - static_cast<T>(3)+ n<T>(7,2)*z[13];
    z[26]=z[13]*z[26];
    z[26]=z[26] - static_cast<T>(1)+ z[31];
    z[29]=z[7]*z[13];
    z[26]=n<T>(1,2)*z[26] + z[29];
    z[31]=z[37]*z[45];
    z[30]=z[30] - n<T>(3,4) - z[5];
    z[30]=z[10]*z[30];
    z[33]= - static_cast<T>(1)+ z[13];
    z[33]=n<T>(1,2)*z[33] - z[10];
    z[33]=z[8]*z[33];
    z[25]=z[25] + z[31] + z[33] + n<T>(1,2)*z[26] + z[30];
    z[25]=z[12]*z[25];
    z[26]= - z[38] - 7*z[36];
    z[17]= - z[47] + z[17];
    z[17]=z[10]*z[17];
    z[30]= - z[13] - z[24];
    z[30]=z[8]*z[30];
    z[17]=z[30] + z[17] + n<T>(1,2)*z[26] - z[29];
    z[17]=z[12]*z[17];
    z[26]=z[10]*z[5];
    z[30]=z[13] - z[10];
    z[30]=z[8]*z[30];
    z[17]=z[17] + z[30] + z[29] - z[26];
    z[17]=z[12]*z[17];
    z[27]= - z[8]*z[27];
    z[26]=z[27] - z[29] - z[26];
    z[26]=z[4]*z[26]*z[35];
    z[17]=z[17] + z[26];
    z[17]=z[17]*z[60];
    z[19]=3*z[13] - z[22] - z[23] + z[19];
    z[19]=n<T>(1,4)*z[19] - z[41];
    z[19]=z[8]*z[19];
    z[17]=z[17] + z[19] + z[25];
    z[17]=z[17]*z[60];
    z[19]=n<T>(7,2)*z[28] - z[23] - z[5];
    z[16]=n<T>(3,2)*z[10] + n<T>(47,9) - z[16];
    z[16]=n<T>(1,2)*z[16] - z[41];
    z[16]=z[8]*z[16];
    z[16]=z[17] + z[18] + n<T>(1,8)*z[19] + z[16];
    z[16]=z[4]*z[16];
    z[17]=z[14] + 5;
    z[17]= - z[5] + n<T>(1,2)*z[17];
    z[18]=z[11]*z[39];
    z[18]=z[18] + z[17];
    z[18]=z[18]*z[40];
    z[19]=n<T>(1,4) + z[11];
    z[19]=z[2]*z[19];
    z[19]=n<T>(5,4)*z[11] + z[19];
    z[19]=z[19]*z[45];
    z[18]=z[19] + z[18] - z[20];
    z[18]=z[12]*z[18];
    z[17]= - z[32] + z[17];
    z[17]=z[17]*z[44];
    z[19]=static_cast<T>(1)+ z[42];
    z[19]=n<T>(1,4)*z[19] - z[2];
    z[19]=z[2]*z[19];
    z[17]=z[19] + z[17] + z[21];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[17]=z[12]*z[17];
    z[18]= - static_cast<T>(3)+ 7*z[28];
    z[18]=z[2]*z[18];
    z[18]=z[18] - static_cast<T>(1)- z[53];
    z[19]=n<T>(179,9) + z[24] - z[22];
    z[19]=n<T>(1,4)*z[19] - z[41];
    z[19]=z[8]*z[19];
    z[17]=z[17] + z[19] + n<T>(1,8)*z[18];

    r += z[15] + z[16] + n<T>(1,2)*z[17];
 
    return r;
}

template double qg_2lha_r971(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r971(const std::array<dd_real,30>&);
#endif
