#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1754(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[14];
    z[6]=k[4];
    z[7]=k[9];
    z[8]=k[12];
    z[9]=z[3] - n<T>(9,4);
    z[10]=npow(z[3],2);
    z[11]=z[10]*z[4];
    z[9]=z[9]*z[11];
    z[12]=n<T>(1,2)*z[3];
    z[13]= - n<T>(15,4) + z[3];
    z[13]=z[13]*z[12];
    z[13]=z[13] - z[9];
    z[13]=z[4]*z[13];
    z[14]=z[3] - n<T>(3,2);
    z[15]=z[14]*z[3];
    z[15]=z[15] - n<T>(3,2);
    z[16]=z[4]*z[3];
    z[15]=z[15]*z[16];
    z[17]=3*z[3];
    z[18]= - static_cast<T>(1)+ z[12];
    z[18]=z[18]*z[17];
    z[18]=z[18] + z[15];
    z[18]=z[4]*z[18];
    z[19]=z[3] + 1;
    z[18]=n<T>(3,4)*z[19] + z[18];
    z[20]=n<T>(1,2)*z[2];
    z[18]=z[18]*z[20];
    z[21]=n<T>(3,2) + z[3];
    z[13]=z[18] + n<T>(1,4)*z[21] + z[13];
    z[13]=z[2]*z[13];
    z[18]=npow(z[3],3);
    z[21]=z[18]*z[4];
    z[22]=n<T>(7,2)*z[10] - z[21];
    z[22]=z[4]*z[22];
    z[23]=n<T>(5,4)*z[3];
    z[22]= - z[23] + z[22];
    z[24]=z[19]*z[3];
    z[25]=n<T>(3,2)*z[24] + z[9];
    z[25]=z[4]*z[25];
    z[26]= - static_cast<T>(3)- 25*z[3];
    z[25]=n<T>(1,4)*z[26] + z[25];
    z[25]=z[2]*z[25];
    z[22]=3*z[22] + z[25];
    z[22]=z[22]*z[20];
    z[25]=z[10] - z[21];
    z[26]= - n<T>(13,2)*z[10] + z[21];
    z[27]=n<T>(1,2)*z[4];
    z[26]=z[26]*z[27];
    z[28]=static_cast<T>(2)+ n<T>(5,2)*z[3];
    z[28]=z[3]*z[28];
    z[26]=z[28] + z[26];
    z[26]=z[2]*z[26];
    z[25]=2*z[25] + z[26];
    z[25]=z[2]*z[25];
    z[26]=n<T>(1,2)*z[10];
    z[28]= - n<T>(17,4) - z[3];
    z[28]=z[28]*z[26];
    z[28]=z[28] + z[21];
    z[28]=z[2]*z[28];
    z[18]=n<T>(1,2)*z[18];
    z[28]= - z[18] + z[28];
    z[28]=z[2]*z[28];
    z[18]=z[1]*npow(z[2],2)*z[18];
    z[18]=z[28] + z[18];
    z[18]=z[1]*z[18];
    z[18]=z[25] + z[18];
    z[18]=z[1]*z[18];
    z[18]=z[22] + z[18];
    z[18]=z[1]*z[18];
    z[22]=z[26] - z[21];
    z[25]=n<T>(3,2)*z[4];
    z[26]=z[22]*z[25];
    z[13]=z[18] - z[26] + z[13];
    z[13]=z[1]*z[13];
    z[18]=n<T>(9,2) - z[3];
    z[18]=z[18]*z[12];
    z[9]=z[18] + z[9];
    z[9]=z[4]*z[9];
    z[18]= - n<T>(13,2) + 5*z[3];
    z[18]=z[18]*z[12];
    z[15]=z[18] - z[15];
    z[15]=z[4]*z[15];
    z[18]= - n<T>(3,4) + z[3];
    z[18]=z[3]*z[18];
    z[18]= - n<T>(9,4) + z[18];
    z[18]=z[18]*z[16];
    z[28]= - static_cast<T>(1)+ z[3];
    z[28]=z[3]*z[28];
    z[18]=n<T>(3,2)*z[28] + z[18];
    z[18]=z[4]*z[18];
    z[28]=n<T>(3,4)*z[3];
    z[18]=z[28] + z[18];
    z[18]=z[2]*z[18];
    z[15]=z[18] + z[23] + z[15];
    z[15]=z[2]*z[15];
    z[9]=z[15] + z[12] + z[9];
    z[12]=3*z[4];
    z[15]=n<T>(5,2) - 7*z[3];
    z[15]=z[3]*z[15];
    z[15]= - z[12] + n<T>(7,2) + z[15];
    z[15]=z[4]*z[15];
    z[18]=static_cast<T>(3)- z[24];
    z[18]=z[3]*z[18];
    z[18]=static_cast<T>(3)+ z[18];
    z[18]=z[18]*z[27];
    z[18]=z[18] + static_cast<T>(1)- z[10];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(1,2)*z[19] + z[18];
    z[18]=z[2]*z[18];
    z[19]=static_cast<T>(1)+ z[4];
    z[23]=static_cast<T>(1)+ z[27];
    z[23]=z[4]*z[23];
    z[23]=n<T>(1,2) + z[23];
    z[23]=z[2]*z[23];
    z[19]=n<T>(1,2)*z[19] + z[23];
    z[19]=z[1]*z[19];
    z[15]=n<T>(3,2)*z[19] + n<T>(3,4)*z[18] + n<T>(1,4)*z[15] - n<T>(5,8) - z[3];
    z[15]=z[2]*z[15];
    z[17]= - z[25] + static_cast<T>(1)- z[17];
    z[15]=n<T>(1,2)*z[17] + z[15];
    z[15]=z[5]*z[15];
    z[17]= - n<T>(3,2) - 2*z[3];
    z[17]=z[17]*z[10];
    z[17]=z[17] + 2*z[21];
    z[17]=z[4]*z[17];
    z[18]=z[28] - z[11];
    z[18]=z[7]*z[18];
    z[19]= - n<T>(3,8) + z[3];
    z[19]=z[3]*z[19];
    z[17]=z[18] + z[19] + z[17];
    z[17]=z[4]*z[17];
    z[17]= - n<T>(1,4)*z[3] + z[17];
    z[17]=z[7]*z[17];
    z[12]=z[22]*z[12];
    z[18]=n<T>(3,4)*z[10] - z[21];
    z[19]=npow(z[4],2);
    z[18]=z[7]*z[18]*z[19];
    z[12]=z[12] + z[18];
    z[18]=n<T>(1,2)*z[7];
    z[12]=z[6]*z[12]*z[18];
    z[12]=z[12] - z[26] + z[17];
    z[12]=z[6]*z[12];
    z[17]=n<T>(1,4) - z[3];
    z[17]=z[3]*z[17];
    z[11]=z[17] + z[11];
    z[11]=z[4]*z[11];
    z[17]=n<T>(3,4) - z[16];
    z[17]=z[17]*z[18];
    z[11]=z[17] - n<T>(1,4)*z[5] + n<T>(3,4)*z[14] + z[11];
    z[11]=z[7]*z[11];
    z[10]= - z[10]*z[19]*z[20];
    z[10]= - z[16] + z[10];
    z[10]=z[2]*z[10];
    z[10]= - n<T>(1,2) + z[10];
    z[14]=z[3] - 3;
    z[10]=z[8]*z[14]*z[10];

    r += n<T>(1,2)*z[9] + n<T>(1,4)*z[10] + z[11] + z[12] + z[13] + z[15];
 
    return r;
}

template double qg_2lha_r1754(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1754(const std::array<dd_real,30>&);
#endif
