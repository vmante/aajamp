#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2098(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[3];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[15];
    z[10]=k[12];
    z[11]=n<T>(1,2)*z[4];
    z[12]=npow(z[2],3);
    z[13]=z[11]*z[12];
    z[14]=n<T>(1,2)*z[1];
    z[15]=z[14]*z[12];
    z[16]=n<T>(3,2)*z[2];
    z[17]=z[16] - 1;
    z[18]=npow(z[2],2);
    z[19]= - z[17]*z[18];
    z[19]=z[15] + z[19] - z[13];
    z[19]=z[1]*z[19];
    z[20]=n<T>(5,2)*z[2];
    z[21]=z[20] - 3;
    z[21]=z[21]*z[18];
    z[22]=z[21] - z[13];
    z[22]=z[4]*z[22];
    z[23]= - static_cast<T>(23)+ 17*z[2];
    z[23]=z[2]*z[23];
    z[23]=static_cast<T>(7)+ z[23];
    z[23]=z[2]*z[23];
    z[19]=3*z[19] + n<T>(1,4)*z[23] + z[22];
    z[19]=z[19]*z[14];
    z[22]=n<T>(1,2)*z[18];
    z[23]=3*z[2];
    z[24]= - n<T>(5,2) + z[23];
    z[24]=z[24]*z[22];
    z[25]=z[2] - n<T>(1,2);
    z[26]=z[18]*z[4];
    z[27]=z[25]*z[26];
    z[24]= - z[15] + z[24] + z[27];
    z[24]=z[24]*z[14];
    z[27]=z[23] - 5;
    z[28]=n<T>(1,2)*z[2];
    z[29]= - z[27]*z[28];
    z[29]= - static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[28];
    z[30]=z[2] - 1;
    z[31]=z[30]*z[18];
    z[32]=n<T>(1,4)*z[4];
    z[33]= - z[32]*z[31];
    z[34]=z[2] - n<T>(3,2);
    z[35]= - z[2]*z[34];
    z[35]= - n<T>(1,2) + z[35];
    z[35]=z[2]*z[35];
    z[33]=z[35] + z[33];
    z[33]=z[4]*z[33];
    z[24]=z[24] + z[29] + z[33];
    z[24]=z[1]*z[24];
    z[29]=z[2] - n<T>(5,2);
    z[33]=z[29]*z[28];
    z[33]=z[33] + 1;
    z[33]=z[33]*z[2];
    z[33]=z[33] - n<T>(1,4);
    z[35]=z[11] + 1;
    z[36]=z[4]*z[35];
    z[36]=n<T>(1,2) + z[36];
    z[33]=z[33]*z[36];
    z[24]=z[24] + z[33];
    z[33]=z[4]*z[28];
    z[33]=z[2] + z[33];
    z[33]=z[4]*z[33];
    z[33]=z[28] + z[33];
    z[36]=z[28] - 1;
    z[37]=z[36]*z[2];
    z[38]=z[37] + n<T>(1,2);
    z[33]=z[38]*z[33];
    z[38]= - static_cast<T>(1)- z[4];
    z[38]=z[31]*z[38];
    z[39]=z[12]*z[1];
    z[38]=3*z[38] + z[39];
    z[38]=z[1]*z[38];
    z[33]=3*z[33] + n<T>(1,4)*z[38];
    z[33]=z[1]*z[33];
    z[38]= - static_cast<T>(3)- z[4];
    z[38]=z[4]*z[38];
    z[38]= - static_cast<T>(3)+ z[38];
    z[40]=z[2] - 3;
    z[40]=z[40]*z[2];
    z[40]=z[40] + 3;
    z[40]=z[40]*z[2];
    z[40]=z[40] - 1;
    z[38]=z[4]*z[40]*z[38];
    z[38]=z[38] - z[40];
    z[33]=n<T>(1,4)*z[38] + z[33];
    z[33]=z[5]*z[33];
    z[24]=3*z[24] + z[33];
    z[24]=z[5]*z[24];
    z[33]=z[11]*z[31]*z[35];
    z[35]= - static_cast<T>(1)+ n<T>(3,8)*z[2];
    z[35]=z[35]*z[2];
    z[38]= - n<T>(3,4) - z[35];
    z[38]=z[2]*z[38];
    z[33]=z[33] + n<T>(1,8) + z[38];
    z[33]=z[4]*z[33];
    z[37]= - static_cast<T>(3)- 5*z[37];
    z[37]=z[2]*z[37];
    z[37]=n<T>(1,2) + z[37];
    z[19]=z[24] + z[19] + n<T>(1,4)*z[37] + z[33];
    z[19]=z[5]*z[19];
    z[24]=z[2] - n<T>(7,4);
    z[24]=z[24]*z[18];
    z[33]=z[32]*z[12];
    z[37]= - z[24] + z[33];
    z[37]=z[4]*z[37];
    z[21]=z[21] - z[39];
    z[21]=z[1]*z[21];
    z[12]=z[12]*z[4];
    z[38]=static_cast<T>(5)- z[16];
    z[38]=z[2]*z[38];
    z[38]= - static_cast<T>(3)+ z[38];
    z[38]=z[2]*z[38];
    z[21]=z[21] + z[38] + n<T>(3,2)*z[12];
    z[21]=z[21]*z[14];
    z[38]=n<T>(11,2) - z[23];
    z[38]=z[38]*z[28];
    z[38]= - static_cast<T>(1)+ z[38];
    z[21]=z[21] + n<T>(1,2)*z[38] + z[37];
    z[21]=z[1]*z[21];
    z[37]= - n<T>(5,4) + z[2];
    z[37]=z[37]*z[18];
    z[13]=z[37] - z[13];
    z[13]=z[4]*z[13];
    z[37]= - static_cast<T>(1)+ n<T>(9,8)*z[2];
    z[37]=z[2]*z[37];
    z[13]=z[37] + z[13];
    z[13]=z[4]*z[13];
    z[13]=n<T>(1,4)*z[30] + z[13];
    z[13]=z[4]*z[13];
    z[15]= - z[24] + z[15];
    z[15]=z[1]*z[15];
    z[24]= - static_cast<T>(5)+ z[2];
    z[24]=z[2]*z[24];
    z[24]=n<T>(9,2) + z[24];
    z[24]=z[24]*z[28];
    z[15]=z[15] + z[24] - z[12];
    z[15]=z[15]*z[14];
    z[24]=z[29]*z[18];
    z[24]=z[24] - z[12];
    z[24]=z[24]*z[11];
    z[15]=z[15] + z[24] + n<T>(5,8) + z[35];
    z[15]=z[1]*z[15];
    z[24]=z[27]*z[22];
    z[24]=z[24] - z[12];
    z[24]=z[24]*z[11];
    z[27]=n<T>(3,4)*z[2];
    z[35]=z[27] - 1;
    z[37]=z[2]*z[35];
    z[24]=z[37] + z[24];
    z[24]=z[4]*z[24];
    z[15]=z[15] + n<T>(1,4)*z[36] + z[24];
    z[15]=z[1]*z[15];
    z[13]=z[13] + z[15];
    z[13]=z[3]*z[13];
    z[15]= - z[35]*z[18];
    z[15]=z[15] - z[33];
    z[15]=z[4]*z[15];
    z[24]= - z[34]*z[27];
    z[15]=z[24] + z[15];
    z[15]=z[4]*z[15];
    z[13]=z[13] + z[21] - n<T>(1,8)*z[30] + z[15];
    z[15]=n<T>(1,2)*z[3];
    z[13]=z[13]*z[15];
    z[21]=n<T>(1,2)*z[10];
    z[24]=npow(z[7],2);
    z[27]=z[21]*z[24];
    z[30]= - z[24] + z[27];
    z[30]=z[6]*z[10]*z[30];
    z[33]=z[21]*z[7];
    z[30]=z[33] + z[30];
    z[34]=npow(z[6],2);
    z[30]=z[30]*z[34];
    z[35]=z[9]*npow(z[6],3);
    z[27]=z[27]*z[35];
    z[27]=z[30] + z[27];
    z[27]=z[9]*z[27];
    z[30]=z[24]*z[6];
    z[36]=n<T>(1,2)*z[7];
    z[37]= - z[36] + z[30];
    z[37]=z[37]*z[34];
    z[35]=z[24]*z[35];
    z[35]=z[37] - n<T>(1,2)*z[35];
    z[35]=z[9]*z[35];
    z[30]=z[7] - z[30];
    z[30]=z[30]*z[34];
    z[30]=n<T>(1,2)*z[30] + z[35];
    z[30]=z[8]*z[30];
    z[27]=z[27] + z[30];
    z[30]= - n<T>(5,3) - z[2];
    z[30]=z[30]*z[28];
    z[22]= - z[29]*z[22];
    z[22]=z[22] + z[12];
    z[22]=z[4]*z[22];
    z[22]=z[30] + z[22];
    z[22]=z[4]*z[22];
    z[29]=z[18] + z[12];
    z[29]=z[4]*z[29];
    z[30]= - n<T>(1,2) + z[23];
    z[30]=z[30]*z[18];
    z[30]=z[30] - z[39];
    z[30]=z[1]*z[30];
    z[23]=static_cast<T>(1)- z[23];
    z[23]=z[2]*z[23];
    z[23]=n<T>(1,2) + z[23];
    z[23]=z[2]*z[23];
    z[23]=z[30] + z[23] + z[29];
    z[14]=z[23]*z[14];
    z[23]=z[2]*z[25];
    z[23]= - n<T>(1,2) + z[23];
    z[23]=z[23]*z[28];
    z[14]=z[14] + z[22] - static_cast<T>(1)+ z[23];
    z[22]=3*z[12];
    z[23]= - z[18] + z[22];
    z[23]=z[4]*z[23];
    z[23]= - n<T>(7,2)*z[2] + z[23];
    z[23]=z[4]*z[23];
    z[23]= - n<T>(1,2) + z[23];
    z[23]=z[23]*z[32];
    z[25]= - n<T>(5,2)*z[31] + z[12];
    z[25]=z[25]*z[11];
    z[29]= - z[2]*z[17];
    z[25]=z[29] + z[25];
    z[25]=z[4]*z[25];
    z[17]= - n<T>(1,4)*z[17] + z[25];
    z[25]=npow(z[4],2);
    z[17]=z[3]*z[17]*z[25];
    z[17]=z[23] + z[17];
    z[17]=z[3]*z[17];
    z[16]=z[16] + z[26];
    z[16]=z[4]*z[16];
    z[16]=n<T>(1,2) + z[16];
    z[16]=z[16]*z[25];
    z[23]=z[12] + n<T>(3,2)*z[18];
    z[23]=z[23]*z[4];
    z[28]=z[28] + z[23];
    z[15]=z[28]*npow(z[4],3)*z[15];
    z[15]=z[16] + z[15];
    z[15]=z[3]*z[15];
    z[16]=z[4]*z[2];
    z[16]=n<T>(3,2) + z[16];
    z[16]=z[16]*z[25];
    z[15]=z[16] + z[15];
    z[15]=z[15]*z[36];
    z[16]=z[18]*z[25];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[16]*z[11];
    z[15]=z[15] + z[16] + z[17];
    z[15]=z[15]*z[36];
    z[16]= - 5*z[18] - z[22];
    z[16]=z[4]*z[16];
    z[16]=n<T>(17,6)*z[2] + z[16];
    z[16]=z[4]*z[16];
    z[16]=n<T>(37,6) + z[16];
    z[16]=z[16]*z[32];
    z[17]= - z[20] - z[26];
    z[17]=z[17]*z[11];
    z[17]=n<T>(1,3) + z[17];
    z[20]=z[25]*z[7];
    z[17]=z[17]*z[20];
    z[16]=z[16] + z[17];
    z[16]=z[7]*z[16];
    z[17]=n<T>(5,2)*z[18] + z[12];
    z[17]=z[17]*z[11];
    z[17]= - n<T>(1,3)*z[2] + z[17];
    z[17]=z[4]*z[17];
    z[17]= - n<T>(41,12) + z[17];
    z[17]=z[17]*z[25];
    z[22]=n<T>(7,2)*z[18] + z[12];
    z[22]=z[4]*z[22];
    z[22]=n<T>(11,6)*z[2] + z[22];
    z[22]=z[4]*z[22];
    z[22]= - n<T>(15,2) + z[22];
    z[20]=z[22]*z[20];
    z[17]=z[17] + n<T>(1,4)*z[20];
    z[17]=z[7]*z[17];
    z[20]= - n<T>(13,6)*z[2] + z[23];
    z[11]=z[20]*z[11];
    z[11]= - n<T>(7,3) + z[11];
    z[11]=z[11]*z[25];
    z[11]=n<T>(1,2)*z[11] + z[17];
    z[11]=z[10]*z[11];
    z[12]= - z[18] - n<T>(3,4)*z[12];
    z[12]=z[4]*z[12];
    z[12]=n<T>(23,24)*z[2] + z[12];
    z[12]=z[4]*z[12];
    z[12]=n<T>(5,3) + z[12];
    z[12]=z[4]*z[12];
    z[11]=z[11] + z[12] + z[16];
    z[11]=z[11]*z[21];
    z[12]=n<T>(1,3)*z[7];
    z[16]=z[7]*z[4];
    z[17]= - static_cast<T>(1)+ n<T>(41,2)*z[16];
    z[17]=z[17]*z[12];
    z[17]=static_cast<T>(1)+ z[17];
    z[18]= - n<T>(5,3)*z[4] - n<T>(27,16)*z[16];
    z[18]=z[7]*z[18];
    z[18]= - z[32] + z[18];
    z[18]=z[10]*z[18];
    z[17]=n<T>(1,4)*z[17] + z[18];
    z[17]=z[10]*z[17];
    z[16]=static_cast<T>(1)- n<T>(5,8)*z[16];
    z[12]=z[16]*z[12];
    z[16]= - static_cast<T>(1)- n<T>(13,6)*z[7];
    z[16]=z[16]*z[33];
    z[16]=n<T>(5,3)*z[24] + z[16];
    z[16]=z[10]*z[16];
    z[16]= - n<T>(7,12)*z[24] + z[16];
    z[16]=z[6]*z[16];
    z[18]=z[1]*npow(z[3],2);
    z[12]=z[16] + z[17] + n<T>(1,16)*z[18] + z[12];
    z[12]=z[6]*z[12];
    z[11]=z[12] + z[11] + z[15] + z[13] + n<T>(1,4)*z[14] + z[19] + n<T>(1,2)*
    z[27];

    r += n<T>(1,2)*z[11];
 
    return r;
}

template double qg_2lha_r2098(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2098(const std::array<dd_real,30>&);
#endif
