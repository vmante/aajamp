#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r402(const std::array<T,30>& k) {
  T z[53];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[10];
    z[6]=k[5];
    z[7]=k[3];
    z[8]=k[12];
    z[9]=k[6];
    z[10]=k[9];
    z[11]=k[8];
    z[12]=k[27];
    z[13]=k[13];
    z[14]=k[14];
    z[15]=npow(z[12],3);
    z[16]=3*z[15];
    z[17]=static_cast<T>(7)- n<T>(9,2)*z[12];
    z[17]=z[17]*z[16];
    z[18]=npow(z[2],3);
    z[19]= - static_cast<T>(47)- 65*z[13];
    z[19]=z[19]*z[18];
    z[20]=npow(z[9],2);
    z[21]=z[20]*z[6];
    z[22]=n<T>(15,32)*z[21];
    z[23]=npow(z[12],4);
    z[24]=n<T>(27,2)*z[23];
    z[25]=z[24]*z[7];
    z[24]= - z[4]*z[24];
    z[17]=z[24] - z[25] - z[22] + z[17] + n<T>(1,48)*z[19];
    z[17]=z[4]*z[17];
    z[19]=npow(z[12],2);
    z[24]=3*z[12];
    z[26]=static_cast<T>(1)+ z[24];
    z[26]=z[26]*z[19];
    z[27]= - n<T>(301,3) - 255*z[13];
    z[28]=z[2]*z[1];
    z[27]=n<T>(1,4)*z[27] - n<T>(47,3)*z[28];
    z[28]=npow(z[2],2);
    z[27]=z[27]*z[28];
    z[29]=n<T>(15,4)*z[6];
    z[30]= - static_cast<T>(1)+ n<T>(23,8)*z[13];
    z[31]= - z[29] + z[30];
    z[31]=z[9]*z[31];
    z[32]=n<T>(15,2)*z[6];
    z[31]=z[32] + z[31];
    z[33]=n<T>(1,2)*z[9];
    z[31]=z[31]*z[33];
    z[26]=z[31] + n<T>(1,8)*z[27] + n<T>(1,3)*z[6] + z[26];
    z[27]=z[15]*z[7];
    z[31]=static_cast<T>(1)- z[12];
    z[31]=z[31]*z[27];
    z[17]=z[17] + n<T>(1,2)*z[26] + 27*z[31];
    z[26]=n<T>(1,2)*z[4];
    z[17]=z[17]*z[26];
    z[31]=7*z[12];
    z[34]= - n<T>(9,2) + z[31];
    z[34]=z[34]*z[19];
    z[25]= - z[25] + 3*z[34] + n<T>(17,24)*z[20];
    z[34]=n<T>(1,2)*z[7];
    z[25]=z[25]*z[34];
    z[35]=n<T>(1,3)*z[13];
    z[36]= - static_cast<T>(1)+ z[13];
    z[36]=z[36]*z[35];
    z[37]= - n<T>(361,32) - z[1];
    z[37]=z[6]*z[37];
    z[37]=n<T>(13,8) + z[37];
    z[36]=n<T>(1,2)*z[37] + z[36];
    z[37]= - static_cast<T>(1)- n<T>(29,16)*z[6];
    z[38]=n<T>(1,3) + n<T>(33,64)*z[6];
    z[38]=z[3]*z[38];
    z[37]=n<T>(1,3)*z[37] + z[38];
    z[37]=z[3]*z[37];
    z[38]=47*z[2];
    z[39]= - static_cast<T>(1)- z[1];
    z[39]=z[39]*z[38];
    z[39]=z[39] - n<T>(113,4) - 251*z[13];
    z[39]=z[2]*z[39];
    z[40]=23*z[13];
    z[41]=n<T>(7,3) + 3*z[6];
    z[41]=5*z[41] - z[40];
    z[42]= - n<T>(15,16)*z[6] + z[30];
    z[42]=z[9]*z[42];
    z[41]=n<T>(1,4)*z[41] + z[42];
    z[42]=n<T>(1,4)*z[9];
    z[41]=z[41]*z[42];
    z[43]= - static_cast<T>(5)+ n<T>(13,2)*z[12];
    z[43]=z[12]*z[43];
    z[17]=z[17] + z[25] + z[41] + n<T>(1,96)*z[39] + n<T>(1,2)*z[37] + n<T>(1,6)*
    z[36] + z[43];
    z[17]=z[4]*z[17];
    z[25]=z[3] - 1;
    z[36]=npow(z[3],2);
    z[37]=z[25]*z[36];
    z[39]=27*z[15] + n<T>(19,12)*z[20];
    z[41]=n<T>(1,4)*z[7];
    z[39]=z[39]*z[41];
    z[43]=npow(z[13],2);
    z[44]= - 5*z[11] + z[13];
    z[44]=n<T>(1,6)*z[44] + 4*z[12];
    z[44]=z[12]*z[44];
    z[33]=static_cast<T>(1)- z[33];
    z[33]=z[9]*z[33];
    z[33]=z[39] + n<T>(1,12)*z[33] + n<T>(1,6)*z[37] + n<T>(1,18)*z[43] + z[44];
    z[33]=z[7]*z[33];
    z[37]=static_cast<T>(2)- n<T>(3,2)*z[12];
    z[37]=z[37]*z[24];
    z[39]=9*z[19];
    z[44]= - z[39] + n<T>(65,32)*z[28];
    z[26]=z[44]*z[26];
    z[45]= - static_cast<T>(1)+ n<T>(65,4)*z[2];
    z[45]=z[2]*z[45];
    z[26]=z[26] + n<T>(1,16)*z[45] + static_cast<T>(1)+ z[37];
    z[26]=z[4]*z[26];
    z[26]=z[26] + 2*z[7] + n<T>(63,32)*z[2] - n<T>(3559,128) - z[24];
    z[26]=z[4]*z[26];
    z[37]=static_cast<T>(15)+ n<T>(13,3)*z[3];
    z[37]=z[3]*z[37];
    z[37]= - n<T>(2891,6) + z[37];
    z[45]= - n<T>(5983,384) + z[7];
    z[45]=z[7]*z[45];
    z[26]=z[26] + n<T>(1,64)*z[37] + z[45];
    z[26]=z[4]*z[26];
    z[37]= - static_cast<T>(1)+ n<T>(7,8)*z[11];
    z[37]=z[7]*z[37];
    z[37]=n<T>(2287,96) + z[37];
    z[37]=z[7]*z[37];
    z[37]= - n<T>(13,48) + z[37];
    z[37]=z[7]*z[37];
    z[25]= - n<T>(13,48)*z[25] + z[37];
    z[37]= - n<T>(65,32)*z[2] + 9*z[12];
    z[45]= - n<T>(5,16) - z[37];
    z[45]=z[4]*z[45];
    z[45]=z[45] - n<T>(15,16)*z[7] + n<T>(2063,64) - z[37];
    z[45]=z[4]*z[45];
    z[46]=static_cast<T>(247)- n<T>(15,2)*z[7];
    z[46]=z[7]*z[46];
    z[46]=n<T>(6329,24) + z[46];
    z[45]=n<T>(1,8)*z[46] + z[45];
    z[45]=z[4]*z[45];
    z[46]=5*z[7];
    z[47]=n<T>(587,4) - z[46];
    z[47]=z[7]*z[47];
    z[47]=n<T>(1069,2) + z[47];
    z[47]=z[7]*z[47];
    z[47]= - n<T>(13,6)*z[3] + z[47];
    z[45]=n<T>(1,16)*z[47] + z[45];
    z[45]=z[4]*z[45];
    z[25]=n<T>(1,2)*z[25] + z[45];
    z[25]=z[8]*z[25];
    z[45]=7*z[10];
    z[47]=static_cast<T>(71)+ 13*z[3];
    z[47]=z[3]*z[47];
    z[47]=z[45] - static_cast<T>(5)+ z[47];
    z[48]=5*z[9];
    z[47]=n<T>(1,12)*z[47] - z[48];
    z[45]= - n<T>(2641,2) - z[45];
    z[45]=n<T>(1,12)*z[45] + z[48];
    z[49]=z[7]*z[9];
    z[50]=z[49] - z[9];
    z[50]=n<T>(1,3) - n<T>(1,16)*z[11] - n<T>(5,16)*z[50];
    z[50]=z[7]*z[50];
    z[45]=n<T>(1,16)*z[45] + z[50];
    z[45]=z[7]*z[45];
    z[25]=z[25] + z[26] + n<T>(1,16)*z[47] + z[45];
    z[25]=z[8]*z[25];
    z[16]= - z[16] + n<T>(7,6)*z[18];
    z[16]=z[16]*z[4];
    z[24]=n<T>(11,2) - z[24];
    z[24]=z[24]*z[19];
    z[26]=n<T>(169,16) + 7*z[2];
    z[26]=z[26]*z[28];
    z[24]=z[16] + z[24] + n<T>(1,6)*z[26];
    z[24]=z[4]*z[24];
    z[26]= - n<T>(3,2) + z[12];
    z[26]=z[12]*z[26];
    z[45]=static_cast<T>(13)+ n<T>(41,2)*z[2];
    z[45]=z[2]*z[45];
    z[24]=z[24] + n<T>(13,96)*z[45] - n<T>(29,32) + z[26];
    z[24]=z[4]*z[24];
    z[26]=n<T>(1,3)*z[3];
    z[45]= - n<T>(19,4) + z[3];
    z[45]=z[45]*z[26];
    z[45]=n<T>(125,64) + z[45];
    z[45]=z[3]*z[45];
    z[24]=z[24] - n<T>(29,32)*z[7] + n<T>(521,192)*z[2] + n<T>(581,96) + z[45];
    z[24]=z[4]*z[24];
    z[45]= - static_cast<T>(1)+ z[42];
    z[45]=z[45]*z[48];
    z[47]=z[20]*z[7];
    z[50]=n<T>(13,3) - z[48];
    z[50]=z[9]*z[50];
    z[50]=z[50] + n<T>(5,2)*z[47];
    z[50]=z[50]*z[34];
    z[51]=n<T>(1,3) + n<T>(3,8)*z[11];
    z[45]=z[50] + 7*z[51] + z[45];
    z[45]=z[45]*z[41];
    z[50]=n<T>(1,16)*z[10];
    z[51]=static_cast<T>(5)+ n<T>(7,4)*z[10];
    z[51]=z[51]*z[50];
    z[52]= - n<T>(317,64) + z[3];
    z[52]=z[3]*z[52];
    z[52]=n<T>(317,64) + z[52];
    z[52]=z[3]*z[52];
    z[51]=n<T>(17,8)*z[9] + z[51] - n<T>(133,128) + z[52];
    z[24]=z[25] + z[24] + n<T>(1,3)*z[51] + z[45];
    z[24]=z[8]*z[24];
    z[25]= - n<T>(1,3) - z[40];
    z[30]=z[9]*z[30];
    z[25]=n<T>(1,2)*z[25] + z[30];
    z[30]=n<T>(1,8)*z[9];
    z[25]=z[25]*z[30];
    z[45]=static_cast<T>(3)+ n<T>(17,2)*z[13];
    z[51]=n<T>(127,128) - z[26];
    z[51]=z[3]*z[51];
    z[51]= - n<T>(253,384) + z[51];
    z[51]=z[3]*z[51];
    z[17]=z[24] + z[17] + z[33] + z[25] - n<T>(47,96)*z[2] + n<T>(1,32)*z[45] + 
    z[51];
    z[17]=z[5]*z[17];
    z[23]=z[4]*z[23];
    z[15]= - n<T>(27,4)*z[23] - z[22] + 9*z[15] - n<T>(47,96)*z[18];
    z[15]=z[4]*z[15];
    z[18]=z[3] - n<T>(1,2);
    z[22]=n<T>(1,3)*z[36];
    z[18]=z[18]*z[22];
    z[23]=static_cast<T>(1)- z[1];
    z[23]=z[23]*z[38];
    z[23]= - n<T>(19,4) + z[23];
    z[23]=z[23]*z[28];
    z[24]=n<T>(23,4)*z[13] - static_cast<T>(1)- z[29];
    z[24]=z[9]*z[24];
    z[24]=z[32] + z[24];
    z[24]=z[24]*z[30];
    z[25]=n<T>(9,2)*z[27];
    z[15]=z[15] - z[25] + z[24] + n<T>(1,96)*z[23] - n<T>(19,4)*z[19] + z[18];
    z[15]=z[4]*z[15];
    z[23]= - n<T>(37,3) + 25*z[9];
    z[23]=z[23]*z[30];
    z[24]=z[11] - n<T>(11,8);
    z[23]= - n<T>(5,4)*z[47] + z[23] + z[24];
    z[23]=z[7]*z[23];
    z[27]=n<T>(7,3)*z[10];
    z[29]= - static_cast<T>(1)- n<T>(1,4)*z[10];
    z[29]=z[29]*z[27];
    z[29]=n<T>(1559,4) + z[29];
    z[30]=n<T>(41,6) - z[48];
    z[30]=z[9]*z[30];
    z[29]=n<T>(1,4)*z[29] + z[30];
    z[23]=n<T>(1,2)*z[29] + z[23];
    z[23]=z[7]*z[23];
    z[29]=static_cast<T>(7)+ n<T>(13,12)*z[3];
    z[29]=z[3]*z[29];
    z[30]= - static_cast<T>(3)+ z[9];
    z[30]=z[9]*z[30];
    z[29]=n<T>(5,4)*z[30] + n<T>(7,16)*z[10] + n<T>(5,3) + n<T>(1,4)*z[29];
    z[30]=z[4]*z[44];
    z[30]=z[30] - n<T>(69,32)*z[2] + n<T>(21,8) + 17*z[12];
    z[30]=z[4]*z[30];
    z[32]=n<T>(1289,16) + z[46];
    z[30]=n<T>(1,4)*z[32] + z[30];
    z[30]=z[4]*z[30];
    z[23]=z[30] + n<T>(1,2)*z[29] + z[23];
    z[29]= - static_cast<T>(5)+ 7*z[11];
    z[30]= - n<T>(1,4)*z[29] + z[48];
    z[30]=z[30]*z[34];
    z[32]=n<T>(1,3)*z[10];
    z[33]= - n<T>(187,4) + z[32];
    z[30]=z[30] + n<T>(7,8)*z[33] - z[48];
    z[30]=z[7]*z[30];
    z[27]= - n<T>(31,2) - z[27];
    z[27]=n<T>(1,4)*z[27] + z[48];
    z[27]=n<T>(1,2)*z[27] + z[30];
    z[27]=z[7]*z[27];
    z[27]= - n<T>(13,48)*z[3] + z[27];
    z[30]= - n<T>(47,4) - 15*z[7];
    z[33]= - n<T>(5,8) - z[37];
    z[33]=z[4]*z[33];
    z[30]=n<T>(1,16)*z[30] + z[33];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(1057,32)*z[7] + z[30];
    z[30]=z[4]*z[30];
    z[27]=n<T>(1,2)*z[27] + z[30];
    z[27]=z[8]*z[27];
    z[23]=n<T>(1,2)*z[23] + z[27];
    z[23]=z[8]*z[23];
    z[27]= - static_cast<T>(7)+ 23*z[11];
    z[30]= - static_cast<T>(1)+ n<T>(19,4)*z[9];
    z[30]=z[9]*z[30];
    z[30]= - n<T>(19,24)*z[47] - n<T>(1,16)*z[27] + n<T>(1,3)*z[30];
    z[30]=z[30]*z[34];
    z[28]=z[39] + n<T>(1,48)*z[28];
    z[16]=n<T>(1,2)*z[28] + z[16];
    z[16]=z[4]*z[16];
    z[16]=z[16] - n<T>(127,192)*z[2] - n<T>(11,16) - z[12];
    z[16]=z[4]*z[16];
    z[28]= - n<T>(221,64) + z[3];
    z[28]=z[3]*z[28];
    z[28]=n<T>(41,64) + z[28];
    z[28]=z[28]*z[26];
    z[33]=static_cast<T>(5)+ n<T>(7,2)*z[10];
    z[33]=z[10]*z[33];
    z[36]=static_cast<T>(1)- n<T>(19,8)*z[9];
    z[36]=z[9]*z[36];
    z[16]=z[23] + z[16] + z[30] + n<T>(1,6)*z[36] + n<T>(1,48)*z[33] - n<T>(87,16)
    + z[28];
    z[16]=z[8]*z[16];
    z[23]=n<T>(1,9)*z[43] + z[39];
    z[23]=n<T>(1,2)*z[23] + z[18];
    z[23]=z[7]*z[23];
    z[28]=z[35] + static_cast<T>(1)- n<T>(17,8)*z[11];
    z[28]=n<T>(1,6)*z[28] + z[31];
    z[30]=n<T>(221,128) - z[3];
    z[30]=z[3]*z[30];
    z[30]=n<T>(1,4) + z[30];
    z[30]=z[30]*z[26];
    z[33]=n<T>(11,3) + n<T>(23,2)*z[13];
    z[33]=z[9]*z[33];
    z[33]= - z[40] + z[33];
    z[33]=z[9]*z[33];
    z[15]=z[17] + z[16] + z[15] + z[23] + n<T>(1,16)*z[33] + n<T>(47,192)*z[2]
    + n<T>(1,2)*z[28] + z[30];
    z[15]=z[5]*z[15];
    z[16]=n<T>(5,4)*z[10];
    z[17]=z[16] - z[24];
    z[17]=z[10]*z[17];
    z[23]=z[9]*z[10];
    z[24]=5*z[10];
    z[28]=n<T>(37,6) + z[24];
    z[28]=z[10]*z[28];
    z[28]=z[28] - n<T>(25,2)*z[23];
    z[28]=z[28]*z[42];
    z[30]=npow(z[10],2);
    z[33]= - n<T>(1,2)*z[30] + z[23];
    z[33]=z[33]*z[49];
    z[17]=n<T>(5,4)*z[33] + z[17] + z[28];
    z[17]=z[7]*z[17];
    z[28]= - static_cast<T>(1559)- n<T>(3631,3)*z[10];
    z[28]=z[28]*z[50];
    z[33]=n<T>(1,2)*z[10];
    z[35]=n<T>(5,2)*z[10];
    z[36]= - n<T>(41,3) - z[35];
    z[36]=z[36]*z[33];
    z[37]=5*z[23];
    z[36]=z[36] + z[37];
    z[36]=z[9]*z[36];
    z[28]=z[28] + z[36];
    z[17]=n<T>(1,2)*z[28] + z[17];
    z[17]=z[7]*z[17];
    z[28]=n<T>(1,2)*z[29] - z[24];
    z[28]=z[28]*z[33];
    z[28]=z[28] - z[37];
    z[28]=z[28]*z[34];
    z[29]=n<T>(119,8) + n<T>(25,3)*z[10];
    z[29]=z[10]*z[29];
    z[28]=z[28] + n<T>(11,4)*z[29] + z[37];
    z[28]=z[7]*z[28];
    z[29]=n<T>(31,8)*z[10] - z[37];
    z[28]=n<T>(1,2)*z[29] + z[28];
    z[28]=z[7]*z[28];
    z[29]=z[30]*z[7];
    z[36]=1051*z[10] + 349*z[29];
    z[34]=z[36]*z[34];
    z[34]=z[34] - 5*z[4];
    z[36]=n<T>(1,8)*z[4];
    z[34]=z[34]*z[36];
    z[28]=z[28] + z[34];
    z[28]=z[8]*z[28];
    z[34]=3*z[10] - z[23];
    z[34]=z[34]*z[42];
    z[32]= - z[32] + z[34];
    z[34]=static_cast<T>(1)- n<T>(121,4)*z[10];
    z[29]=5*z[34] - n<T>(477,4)*z[29];
    z[29]=z[29]*z[36];
    z[17]=z[28] + z[29] + n<T>(5,2)*z[32] + z[17];
    z[17]=z[8]*z[17];
    z[27]=z[27]*z[50];
    z[28]= - n<T>(19,3) + n<T>(15,2)*z[10];
    z[28]=z[28]*z[23];
    z[29]=n<T>(1,3) - z[35];
    z[29]=z[10]*z[29];
    z[28]=z[29] + n<T>(1,4)*z[28];
    z[28]=z[9]*z[28];
    z[29]=n<T>(19,3) - 15*z[10];
    z[29]=z[29]*z[23];
    z[29]=5*z[30] + n<T>(1,2)*z[29];
    z[29]=z[9]*z[29];
    z[30]=z[7]*npow(z[23],2);
    z[29]=z[29] + n<T>(5,2)*z[30];
    z[29]=z[29]*z[41];
    z[27]=z[29] + z[27] + z[28];
    z[27]=z[7]*z[27];
    z[24]=n<T>(19,3) - z[24];
    z[23]=z[24]*z[23];
    z[16]= - n<T>(1,3) + z[16];
    z[16]=z[10]*z[16];
    z[16]=z[16] + n<T>(1,8)*z[23];
    z[16]=z[9]*z[16];
    z[23]=n<T>(7,2)*z[14];
    z[24]=n<T>(7,8)*z[10];
    z[28]=z[24] + z[23] - z[12];
    z[28]=z[10]*z[28];
    z[23]=z[2]*z[23];
    z[23]=z[23] + z[28];
    z[23]=z[4]*z[23];
    z[28]=z[14] - z[3];
    z[28]=z[2]*z[28];
    z[29]=n<T>(505,32)*z[10] + n<T>(547,48) + z[14];
    z[29]=z[10]*z[29];
    z[16]=z[17] + z[23] + z[27] + z[16] + z[28] + z[29];
    z[16]=z[8]*z[16];
    z[17]=npow(z[1],2);
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[26];
    z[23]=static_cast<T>(1)- n<T>(1,3)*z[1];
    z[17]=n<T>(1,2)*z[23] + z[17];
    z[17]=z[3]*z[17];
    z[17]=n<T>(1,3) + z[17];
    z[17]=z[3]*z[17];
    z[23]=n<T>(515,36) + 7*z[14];
    z[23]=z[23]*z[14];
    z[17]= - n<T>(1,8)*z[23] + z[17];
    z[17]=z[2]*z[17];
    z[16]=z[17] + z[16];
    z[17]= - z[3]*z[1];
    z[17]=n<T>(1,4) + z[17];
    z[17]=z[17]*z[22];
    z[22]=z[4]*npow(z[3],3);
    z[17]=n<T>(1,6)*z[22] + z[17];
    z[17]=z[2]*z[17];
    z[22]=z[19]*z[35];
    z[25]=z[10]*z[25];
    z[17]=z[25] - n<T>(15,64)*z[21] + z[22] + z[17];
    z[17]=z[4]*z[17];
    z[21]=n<T>(17,24)*z[11] - n<T>(1,3) - n<T>(1,4)*z[23];
    z[22]= - z[14]*z[24];
    z[21]=z[22] + n<T>(1,2)*z[21] - z[31];
    z[21]=z[21]*z[33];
    z[20]=z[13]*z[20];
    z[19]=z[7]*z[10]*z[19];

    r += z[15] + n<T>(1,2)*z[16] + z[17] + z[18] - n<T>(9,2)*z[19] + n<T>(23,64)*
      z[20] + z[21];
 
    return r;
}

template double qg_2lha_r402(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r402(const std::array<dd_real,30>&);
#endif
