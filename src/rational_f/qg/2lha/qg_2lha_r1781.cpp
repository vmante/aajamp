#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1781(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[10];
    z[5]=k[9];
    z[6]=k[13];
    z[7]=k[17];
    z[8]=k[7];
    z[9]=k[14];
    z[10]=z[9]*z[8];
    z[11]=static_cast<T>(1)- n<T>(1,3)*z[9];
    z[11]=z[5]*z[11];
    z[12]= - static_cast<T>(1)+ n<T>(1,3)*z[1];
    z[13]=z[1]*z[5]*z[12];
    z[11]=z[13] - n<T>(1,3)*z[10] + z[11];
    z[13]=z[1] - 1;
    z[14]=z[2] + 1;
    z[15]=n<T>(1,6)*z[4];
    z[13]=z[1]*z[15]*z[14]*z[13];
    z[14]=n<T>(1,2)*z[1];
    z[12]= - z[12]*z[14];
    z[12]=z[13] - n<T>(1,3) + z[12];
    z[12]=z[4]*z[12];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[3]*z[11];
    z[12]=z[14] - static_cast<T>(1)+ n<T>(1,2)*z[9];
    z[12]=z[5]*z[12];
    z[12]=n<T>(1,2)*z[10] + z[12];
    z[13]=static_cast<T>(5)+ 7*z[2];
    z[13]=z[13]*z[14];
    z[13]= - z[2] + z[13];
    z[13]=z[13]*z[15];
    z[13]=z[13] + n<T>(2,3) - n<T>(1,4)*z[1];
    z[13]=z[4]*z[13];
    z[11]=z[11] + n<T>(1,2)*z[12] + z[13];
    z[11]=z[3]*z[11];
    z[12]=static_cast<T>(1)- z[9];
    z[12]=z[5]*z[12];
    z[10]= - z[10] + z[12];
    z[12]=static_cast<T>(3)+ z[7];
    z[13]=n<T>(1,2)*z[7];
    z[15]=n<T>(7,3) + z[13];
    z[15]=n<T>(1,2)*z[15] - z[6];
    z[15]=z[2]*z[15];
    z[12]=z[15] + n<T>(1,4)*z[12] - z[6];
    z[12]=z[4]*z[12];
    z[15]= - n<T>(1,3) + z[7];
    z[12]=z[12] + n<T>(1,4)*z[15] - z[6];
    z[12]=z[4]*z[12];
    z[10]=z[11] + n<T>(1,4)*z[10] + z[12];
    z[10]=z[3]*z[10];
    z[11]=static_cast<T>(1)- z[6];
    z[11]=z[6]*z[11];
    z[11]=z[13] + n<T>(1,3)*z[11];
    z[11]=z[2]*z[11];
    z[12]=n<T>(1,6)*z[1];
    z[13]=npow(z[6],2);
    z[12]= - z[13]*z[12];
    z[11]=z[12] + z[6] + n<T>(1,2)*z[11];
    z[11]=z[4]*z[11];
    z[12]= - z[13]*z[14];
    z[12]=2*z[6] + z[12];
    z[11]=n<T>(1,3)*z[12] + z[11];
    z[11]=z[4]*z[11];

    r += z[10] + z[11];
 
    return r;
}

template double qg_2lha_r1781(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1781(const std::array<dd_real,30>&);
#endif
