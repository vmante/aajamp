#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1080(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[6];
    z[5]=k[8];
    z[6]=k[3];
    z[7]=k[9];
    z[8]=z[1] + 1;
    z[9]=z[2] - 2;
    z[9]= - z[9]*z[8];
    z[10]=z[7]*z[2];
    z[11]= - static_cast<T>(1)- z[2];
    z[11]=z[6]*z[11]*npow(z[7],2);
    z[11]= - 3*z[10] + z[11];
    z[11]=z[6]*z[11];
    z[12]=2*z[2];
    z[11]=z[11] + static_cast<T>(1)- z[12];
    z[11]=z[6]*z[11];
    z[13]=z[6] - 1;
    z[14]=z[1] - z[13];
    z[14]=z[4]*z[14];
    z[8]= - 3*z[8] + z[14];
    z[8]=z[4]*z[8];
    z[8]=z[8] + z[11] + z[9];
    z[8]=z[3]*z[8];
    z[9]=z[6]*z[7];
    z[11]=static_cast<T>(2)+ z[2];
    z[11]=z[7]*z[11];
    z[11]= - n<T>(3,2) + 2*z[11];
    z[11]=z[11]*z[9];
    z[11]=z[11] - n<T>(3,2) + 4*z[10];
    z[11]=z[6]*z[11];
    z[14]=7*z[1];
    z[15]=11*z[6] - static_cast<T>(11)- z[14];
    z[15]=z[4]*z[15];
    z[14]=n<T>(1,2)*z[15] + static_cast<T>(13)+ z[14];
    z[14]=z[4]*z[14];
    z[8]=2*z[8] + z[14] + z[11] - n<T>(3,2)*z[1] - n<T>(11,2) + z[12];
    z[8]=z[3]*z[8];
    z[11]= - static_cast<T>(3)- z[2];
    z[11]=z[7]*z[11];
    z[11]=static_cast<T>(3)+ z[11];
    z[9]=z[11]*z[9];
    z[11]=9*z[1];
    z[12]= - 23*z[6] + static_cast<T>(23)+ z[11];
    z[12]=z[4]*z[12];
    z[11]=z[12] - static_cast<T>(43)- z[11];
    z[11]=z[4]*z[11];
    z[8]=z[8] + n<T>(1,4)*z[11] + z[9] + static_cast<T>(3)- z[10];
    z[8]=z[3]*z[8];
    z[9]= - n<T>(3,2) + z[7];
    z[9]=z[7]*z[9];
    z[10]= - n<T>(1,4)*z[1] + z[13];
    z[10]=z[4]*z[10];
    z[10]=n<T>(3,2) + z[10];
    z[10]=z[4]*z[10];
    z[8]=z[8] + z[9] + 3*z[10];
    z[8]=z[3]*z[8];
    z[9]= - z[1]*z[5];
    z[9]= - n<T>(13,4)*z[6] + n<T>(13,4) + z[9];
    z[9]=z[4]*z[9];
    z[9]= - n<T>(13,4) + z[9];
    z[9]=z[4]*z[9];

    r += z[8] + n<T>(1,3)*z[9];
 
    return r;
}

template double qg_2lha_r1080(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1080(const std::array<dd_real,30>&);
#endif
