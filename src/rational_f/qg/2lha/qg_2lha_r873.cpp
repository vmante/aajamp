#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r873(const std::array<T,30>& k) {
  T z[37];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[11];
    z[7]=k[9];
    z[8]=k[3];
    z[9]=k[5];
    z[10]=k[8];
    z[11]=k[15];
    z[12]=z[6] + 3;
    z[13]=n<T>(1,4)*z[6];
    z[14]= - z[12]*z[13];
    z[15]=n<T>(1,2)*z[5];
    z[16]=n<T>(3,2) + z[5];
    z[16]=z[16]*z[15];
    z[14]=z[16] - static_cast<T>(1)+ z[14];
    z[14]=z[9]*z[14];
    z[16]=static_cast<T>(3)+ n<T>(5,2)*z[5];
    z[16]=z[16]*z[15];
    z[17]=z[6] + n<T>(3,2);
    z[17]=z[17]*z[6];
    z[18]=n<T>(1,8)*z[2];
    z[14]=z[14] + z[18] + z[16] - n<T>(1,16) - z[17];
    z[14]=z[9]*z[14];
    z[16]= - static_cast<T>(1)- z[7];
    z[16]=z[16]*z[18];
    z[19]=static_cast<T>(7)+ 13*z[7];
    z[16]=z[16] + n<T>(1,16)*z[19];
    z[16]=z[7]*z[16];
    z[19]=3*z[5];
    z[20]=z[19] + 1;
    z[21]=n<T>(1,2) - z[19];
    z[21]=z[9]*z[21];
    z[21]=z[21] - z[20];
    z[21]=z[9]*z[21];
    z[22]=n<T>(1,2)*z[7];
    z[23]=static_cast<T>(1)+ z[22];
    z[23]=z[7]*z[23];
    z[21]=z[23] + z[21];
    z[23]=n<T>(1,8)*z[4];
    z[21]=z[21]*z[23];
    z[24]=static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[25]=z[5]*z[24];
    z[25]=n<T>(3,4) + z[25];
    z[25]=z[5]*z[25];
    z[14]=z[21] + z[14] + z[25] + z[16];
    z[14]=z[4]*z[14];
    z[16]=3*z[7];
    z[21]=n<T>(1,4) - z[7];
    z[21]=z[21]*z[16];
    z[25]=npow(z[7],2);
    z[26]=npow(z[6],2);
    z[27]=n<T>(1,8)*z[25] - z[26];
    z[27]=z[2]*z[27];
    z[21]=z[21] + z[27];
    z[27]=n<T>(1,2)*z[2];
    z[21]=z[21]*z[27];
    z[28]= - static_cast<T>(11)+ n<T>(23,2)*z[7];
    z[28]=z[28]*z[22];
    z[28]= - static_cast<T>(3)+ z[28];
    z[29]=z[6] + 1;
    z[30]=n<T>(1,2)*z[6];
    z[31]=z[29]*z[30];
    z[31]= - z[15] - static_cast<T>(2)+ z[31];
    z[31]=z[9]*z[31];
    z[32]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[33]=z[32]*z[5];
    z[14]=z[14] + z[31] + z[21] - z[33] + n<T>(1,4)*z[28] + z[17];
    z[14]=z[4]*z[14];
    z[17]=z[30] + 1;
    z[17]=z[17]*z[6];
    z[21]=static_cast<T>(1)+ z[15];
    z[21]=z[5]*z[21];
    z[21]= - z[17] + z[21];
    z[28]=n<T>(1,2)*z[9];
    z[21]=z[21]*z[28];
    z[30]=static_cast<T>(1)+ n<T>(3,4)*z[6];
    z[30]=z[30]*z[6];
    z[31]=static_cast<T>(1)+ n<T>(3,4)*z[5];
    z[31]=z[5]*z[31];
    z[21]=z[21] - z[30] + z[31];
    z[21]=z[9]*z[21];
    z[31]=z[1] + 3;
    z[34]=z[31]*z[15];
    z[34]=static_cast<T>(1)+ z[34];
    z[34]=z[34]*z[15];
    z[21]=z[34] + z[21];
    z[21]=z[4]*z[21];
    z[34]=z[29]*z[13];
    z[34]= - n<T>(1,4)*z[5] - static_cast<T>(1)+ z[34];
    z[34]=z[9]*z[34];
    z[30]=z[30] - n<T>(1,2);
    z[21]=z[21] + 3*z[34] - n<T>(3,2)*z[33] + z[7] + z[30];
    z[21]=z[4]*z[21];
    z[33]=2*z[7];
    z[34]=z[26]*npow(z[2],2);
    z[30]= - n<T>(1,4)*z[34] - z[33] + z[30];
    z[30]=z[2]*z[30];
    z[34]=n<T>(5,2) + z[1];
    z[21]=z[21] + z[30] - z[17] + n<T>(1,2)*z[34] + z[33];
    z[21]=z[4]*z[21];
    z[30]=static_cast<T>(7)+ 3*z[6];
    z[30]=z[30]*z[13];
    z[34]=z[26]*z[2];
    z[29]=z[29]*z[6];
    z[35]= - 3*z[29] + z[34];
    z[36]=n<T>(1,4)*z[2];
    z[35]=z[35]*z[36];
    z[30]=z[35] + z[30] + z[7] - n<T>(1,2);
    z[30]=z[2]*z[30];
    z[35]= - static_cast<T>(5)- z[6];
    z[35]=z[35]*z[13];
    z[24]=z[30] + z[35] - z[33] + z[24];
    z[24]=z[2]*z[24];
    z[21]=z[21] + z[24] + z[13] - n<T>(1,2)*z[32] + z[7];
    z[21]=z[3]*z[21];
    z[24]=11*z[7];
    z[30]= - static_cast<T>(27)+ z[24];
    z[30]=z[7]*z[30];
    z[12]= - z[6]*z[12];
    z[12]=z[12] + n<T>(1,4)*z[30] + z[31];
    z[24]=static_cast<T>(19)- z[24];
    z[24]=z[24]*z[22];
    z[24]= - static_cast<T>(3)+ z[24];
    z[30]=z[7] - 1;
    z[31]=z[7]*z[30];
    z[31]=n<T>(11,4)*z[31] - 5*z[29];
    z[31]=n<T>(1,2)*z[31] + z[34];
    z[31]=z[31]*z[27];
    z[32]=static_cast<T>(2)+ z[6];
    z[32]=z[6]*z[32];
    z[24]=z[31] + n<T>(1,4)*z[24] + z[32];
    z[24]=z[2]*z[24];
    z[12]=z[21] + z[14] + n<T>(1,4)*z[12] + z[24];
    z[12]=z[3]*z[12];
    z[14]=static_cast<T>(1)- 5*z[7];
    z[21]=z[2]*z[30];
    z[14]=5*z[14] + z[21];
    z[14]=z[7]*z[14];
    z[21]=z[27] - n<T>(5,2) - 9*z[5];
    z[24]=z[18] + static_cast<T>(1)- n<T>(9,4)*z[5];
    z[24]=z[9]*z[24];
    z[21]=n<T>(1,8)*z[21] + z[24];
    z[21]=z[9]*z[21];
    z[24]=npow(z[5],2);
    z[30]=3*z[24];
    z[31]=z[30] + n<T>(1,2);
    z[32]= - z[9]*z[31];
    z[30]=z[32] - static_cast<T>(1)- z[30];
    z[30]=z[9]*z[30];
    z[32]=z[22] - 1;
    z[33]= - z[7]*z[32];
    z[30]=z[33] + z[30];
    z[23]=z[30]*z[23];
    z[14]=z[23] + n<T>(1,16)*z[14] + z[21];
    z[21]=n<T>(1,2)*z[4];
    z[14]=z[14]*z[21];
    z[23]=z[5] + 1;
    z[30]=z[23]*z[15];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[7]*z[16];
    z[16]=z[16] - z[26];
    z[16]=z[16]*z[27];
    z[33]=static_cast<T>(1)- n<T>(57,16)*z[7];
    z[33]=z[7]*z[33];
    z[16]=z[16] + z[33] + z[30];
    z[23]=z[23]*z[5];
    z[30]=z[36] + z[23] + n<T>(1,2) - z[17];
    z[13]=n<T>(1,4)*z[23] - static_cast<T>(1)- z[13];
    z[13]=z[9]*z[13];
    z[13]=n<T>(1,2)*z[30] + z[13];
    z[13]=z[9]*z[13];
    z[13]=z[14] + n<T>(1,2)*z[16] + z[13];
    z[13]=z[4]*z[13];
    z[14]=z[26]*z[27];
    z[16]= - static_cast<T>(23)+ 33*z[7];
    z[16]=z[16]*z[7];
    z[14]=z[14] + n<T>(1,16)*z[16] - z[29];
    z[14]=z[2]*z[14];
    z[16]= - static_cast<T>(1)- n<T>(1,8)*z[16];
    z[14]=z[14] + n<T>(1,2)*z[16] + z[17];
    z[12]=z[12] + n<T>(1,2)*z[14] + z[13];
    z[12]=z[3]*z[12];
    z[13]= - z[32]*z[22];
    z[14]=static_cast<T>(1)- z[19];
    z[14]=z[14]*z[19];
    z[14]=z[27] - static_cast<T>(1)+ z[14];
    z[14]=z[14]*z[28];
    z[14]=z[14] - z[31];
    z[14]=z[9]*z[14];
    z[16]=npow(z[9],2);
    z[17]= - z[25] - z[16];
    z[23]=n<T>(1,4)*z[4];
    z[17]=z[17]*z[23];
    z[13]=z[17] + z[13] + z[14];
    z[13]=z[4]*z[13];
    z[14]=static_cast<T>(1)+ n<T>(11,2)*z[7];
    z[14]=z[14]*z[22];
    z[17]=n<T>(9,2) - 7*z[5];
    z[17]=n<T>(3,2)*z[17] + z[2];
    z[17]=z[9]*z[17];
    z[17]= - n<T>(1,2)*z[20] + z[17];
    z[17]=z[9]*z[17];
    z[13]=z[13] + z[14] + z[17];
    z[13]=z[13]*z[23];
    z[14]= - static_cast<T>(1)+ n<T>(11,16)*z[7];
    z[14]=z[7]*z[14];
    z[13]=z[13] + z[14] + n<T>(15,16)*z[9];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[3]*z[12];
    z[13]=z[11] - 1;
    z[14]=n<T>(1,2)*z[8];
    z[17]=z[14]*z[13];
    z[17]=z[17] + 1;
    z[17]=z[11]*z[17];
    z[17]=n<T>(3,2) + z[17];
    z[20]=z[8]*z[11];
    z[20]= - static_cast<T>(1)+ n<T>(1,4)*z[20];
    z[15]=z[20]*z[15];
    z[15]=z[15] + n<T>(1,4)*z[17];
    z[15]=z[8]*z[15];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[19];
    z[13]=z[13]*z[10];
    z[13]=3*z[13];
    z[17]= - z[11]*z[13];
    z[17]= - n<T>(1,2) + z[17];
    z[17]=z[17]*z[14];
    z[13]=z[17] - n<T>(1,4) - z[13];
    z[13]=z[8]*z[13];
    z[13]=n<T>(19,2) + z[13];
    z[14]=z[14] + 1;
    z[14]=z[10]*z[14];
    z[14]=n<T>(3,2) + z[14];
    z[14]=z[8]*z[14];
    z[14]=static_cast<T>(3)+ z[14];
    z[14]=z[14]*z[18];
    z[13]=z[14] + n<T>(1,4)*z[13] + z[15];
    z[13]=z[9]*z[13];
    z[14]= - z[5]*z[8];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[14]*z[19];
    z[15]= - static_cast<T>(1)+ z[8];
    z[17]=z[8] + 1;
    z[18]= - z[10]*z[17];
    z[18]= - n<T>(3,2) + z[18];
    z[18]=z[2]*z[18];
    z[14]=z[18] + n<T>(1,2)*z[15] + z[14];
    z[13]=n<T>(1,8)*z[14] + z[13];
    z[13]=z[9]*z[13];
    z[14]=static_cast<T>(1)- n<T>(7,4)*z[5];
    z[14]=z[5]*z[14];
    z[14]= - n<T>(1,8)*z[17] + z[14];
    z[14]=3*z[14] + z[27];
    z[14]=z[9]*z[14];
    z[14]= - n<T>(9,4)*z[24] + z[14];
    z[14]=z[9]*z[14];
    z[15]= - z[16]*z[21];
    z[14]=z[14] + z[15];
    z[14]=z[14]*z[21];
    z[15]=z[2]*z[10];
    z[13]=z[14] + n<T>(1,16)*z[15] + z[13];

    r += z[12] + n<T>(1,4)*z[13];
 
    return r;
}

template double qg_2lha_r873(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r873(const std::array<dd_real,30>&);
#endif
