#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r269(const std::array<T,30>& k) {
  T z[60];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[17];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[7];
    z[9]=k[12];
    z[10]=k[6];
    z[11]=k[11];
    z[12]=k[24];
    z[13]=k[5];
    z[14]=k[14];
    z[15]=k[9];
    z[16]=npow(z[12],2);
    z[17]=n<T>(1,2)*z[16];
    z[18]=9*z[12];
    z[19]= - static_cast<T>(25)+ z[18];
    z[19]=z[19]*z[17];
    z[20]=z[2]*npow(z[12],3);
    z[21]=9*z[20];
    z[19]=z[19] + z[21];
    z[19]=z[2]*z[19];
    z[22]=n<T>(5,2)*z[14];
    z[23]=z[22] - 3;
    z[23]=z[23]*z[14];
    z[24]= - static_cast<T>(1)+ z[13];
    z[25]=static_cast<T>(13)- 177*z[12];
    z[25]=z[12]*z[25];
    z[19]=n<T>(3,2)*z[19] + n<T>(1,8)*z[25] + n<T>(1,2)*z[24] + z[23];
    z[19]=z[2]*z[19];
    z[24]=3*z[9];
    z[25]= - n<T>(41,2) - 5*z[9];
    z[25]=z[25]*z[24];
    z[25]= - 17*z[15] + n<T>(113,2) + z[25];
    z[26]=3*z[12];
    z[27]=7*z[15] - n<T>(19,2) + 9*z[9];
    z[27]=z[27]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[27];
    z[27]=n<T>(1,2)*z[12];
    z[25]=z[25]*z[27];
    z[28]=n<T>(1,3)*z[1];
    z[29]=n<T>(35,6) + z[13];
    z[29]=n<T>(1,2)*z[29] + z[28];
    z[29]=z[9]*z[29];
    z[29]=z[29] - z[28] - static_cast<T>(5)- n<T>(7,4)*z[13];
    z[29]=z[9]*z[29];
    z[30]=n<T>(5,2)*z[15];
    z[31]=n<T>(1,2)*z[6];
    z[32]=z[31] + n<T>(7,8);
    z[32]=z[13]*z[32];
    z[33]=n<T>(11,4) - z[10];
    z[34]= - z[10]*z[28];
    z[35]=5*z[14];
    z[36]=n<T>(3,2) + z[35];
    z[36]=z[14]*z[36];
    z[19]=z[19] + z[25] - z[30] + z[29] + z[36] + z[34] + n<T>(1,3)*z[33] + 
    z[32];
    z[25]= - static_cast<T>(3)- z[13];
    z[25]=z[25]*z[31];
    z[25]= - z[13] + z[25];
    z[29]= - n<T>(1,2) + z[9];
    z[29]=z[9]*z[29];
    z[29]=z[29] - z[15];
    z[32]=n<T>(1,2)*z[15];
    z[33]=z[32] + z[9];
    z[34]=z[33] - n<T>(3,4);
    z[36]= - z[12]*z[34];
    z[29]=n<T>(1,4)*z[29] + z[36];
    z[36]=9*z[16];
    z[29]=z[29]*z[36];
    z[37]= - z[11]*z[31];
    z[29]=z[29] + z[37];
    z[29]=z[5]*z[29];
    z[25]=z[29] + n<T>(1,2)*z[25] - z[23];
    z[29]= - n<T>(17,2) - z[9];
    z[29]=z[29]*z[24];
    z[29]= - 11*z[15] + n<T>(47,2) + z[29];
    z[34]=z[34]*z[26];
    z[29]=n<T>(1,4)*z[29] + z[34];
    z[29]=z[12]*z[29];
    z[34]= - static_cast<T>(1)+ n<T>(3,2)*z[9];
    z[34]=z[9]*z[34];
    z[29]=n<T>(3,4)*z[29] - n<T>(21,16)*z[15] + n<T>(9,32) + z[34];
    z[29]=z[12]*z[29];
    z[34]=n<T>(9,4)*z[15];
    z[37]=n<T>(1,2)*z[2];
    z[38]=z[37] - z[34] - n<T>(23,24) + z[6];
    z[39]=npow(z[2],3);
    z[40]=z[39]*z[11];
    z[41]=n<T>(58,9)*z[40];
    z[42]=n<T>(103,6) + z[2];
    z[42]=z[2]*z[42];
    z[42]=n<T>(53,12) + z[42];
    z[42]=z[2]*z[42];
    z[42]=n<T>(53,24) + z[42];
    z[42]=n<T>(1,2)*z[42] - z[41];
    z[42]=z[11]*z[42];
    z[43]=n<T>(43,16) - z[2];
    z[42]=n<T>(1,3)*z[43] + z[42];
    z[42]=z[11]*z[42];
    z[38]=n<T>(1,4)*z[38] + z[42];
    z[38]=z[11]*z[38];
    z[42]=n<T>(1,2) - z[12];
    z[42]=z[42]*z[16];
    z[42]=z[42] - z[20];
    z[42]=z[2]*z[42];
    z[25]=z[38] + n<T>(27,16)*z[42] + z[29] + n<T>(1,4)*z[25];
    z[25]=z[5]*z[25];
    z[29]=n<T>(1,2) + z[14];
    z[29]=z[29]*z[35];
    z[38]=n<T>(1,8)*z[2];
    z[42]=n<T>(59,3) + z[2];
    z[42]=z[42]*z[38];
    z[43]= - n<T>(67,4) - z[6];
    z[44]=static_cast<T>(71)- n<T>(7,2)*z[9];
    z[44]=z[9]*z[44];
    z[29]=z[42] + n<T>(27,8)*z[15] + n<T>(1,12)*z[44] + n<T>(1,4)*z[43] + z[29];
    z[42]=npow(z[2],2);
    z[43]=n<T>(1955,9) + 7*z[2];
    z[43]=z[2]*z[43];
    z[43]= - n<T>(1223,18) + z[43];
    z[43]=z[43]*z[42];
    z[44]=n<T>(2,3) - z[2];
    z[40]=z[44]*z[40];
    z[40]=n<T>(1,8)*z[43] + n<T>(29,3)*z[40];
    z[40]=z[11]*z[40];
    z[43]= - n<T>(727,9) - n<T>(5,2)*z[2];
    z[43]=z[2]*z[43];
    z[43]= - n<T>(275,36) + z[43];
    z[43]=z[43]*z[37];
    z[44]= - n<T>(17,24) + z[14];
    z[43]=z[43] + 13*z[44] - n<T>(7,3)*z[9];
    z[40]=n<T>(1,2)*z[43] + z[40];
    z[40]=z[11]*z[40];
    z[29]=n<T>(1,2)*z[29] + z[40];
    z[29]=z[11]*z[29];
    z[19]=z[25] + n<T>(1,4)*z[19] + z[29];
    z[19]=z[5]*z[19];
    z[25]=z[13] + 1;
    z[29]=n<T>(1,4)*z[25] + z[28];
    z[29]=z[9]*z[29];
    z[40]=n<T>(7,2)*z[15] - n<T>(13,2) + z[24];
    z[40]=z[40]*z[26];
    z[43]= - static_cast<T>(29)- z[24];
    z[43]=z[9]*z[43];
    z[43]=static_cast<T>(47)+ z[43];
    z[40]=z[40] + n<T>(1,2)*z[43] + z[15];
    z[40]=z[40]*z[27];
    z[43]=n<T>(1,2)*z[13];
    z[44]=static_cast<T>(1)- z[43];
    z[44]=z[6]*z[44];
    z[44]= - n<T>(29,6) + z[44];
    z[45]=z[1]*z[31];
    z[45]=z[45] - static_cast<T>(1)+ n<T>(7,4)*z[6];
    z[45]=z[1]*z[45];
    z[46]= - static_cast<T>(9)+ z[35];
    z[46]=z[14]*z[46];
    z[29]=z[40] - z[30] + z[29] + z[46] + n<T>(1,2)*z[44] + z[45];
    z[30]=n<T>(3,2)*z[12];
    z[40]=static_cast<T>(71)+ 27*z[12];
    z[40]=z[12]*z[40];
    z[40]=static_cast<T>(3)+ z[40];
    z[40]=z[40]*z[30];
    z[44]=5*z[12];
    z[45]= - n<T>(3,2) - z[44];
    z[45]=z[45]*z[16];
    z[45]=z[45] + z[20];
    z[45]=z[2]*z[45];
    z[40]=n<T>(27,2)*z[45] + static_cast<T>(1)+ z[40];
    z[45]=n<T>(1,4)*z[2];
    z[40]=z[40]*z[45];
    z[46]= - n<T>(5,2) + z[14];
    z[46]=z[46]*z[35];
    z[47]=n<T>(13,3) + z[13];
    z[46]=n<T>(1,4)*z[47] + z[46];
    z[47]= - static_cast<T>(5)- n<T>(237,16)*z[12];
    z[47]=z[12]*z[47];
    z[40]=z[40] + n<T>(1,2)*z[46] + z[47];
    z[40]=z[2]*z[40];
    z[29]=n<T>(1,2)*z[29] + z[40];
    z[40]=n<T>(1,2)*z[9];
    z[46]= - static_cast<T>(3)- z[40];
    z[46]=z[46]*z[24];
    z[46]=n<T>(35,2) + z[46];
    z[47]=z[33] - n<T>(3,2);
    z[30]=z[47]*z[30];
    z[30]=z[30] + n<T>(1,4)*z[46] - z[15];
    z[30]=z[30]*z[26];
    z[46]= - static_cast<T>(1)+ z[24];
    z[46]=z[9]*z[46];
    z[46]= - 21*z[15] - static_cast<T>(9)+ n<T>(5,2)*z[46];
    z[30]=n<T>(1,4)*z[46] + z[30];
    z[30]=z[30]*z[27];
    z[46]= - n<T>(1,2) - z[12];
    z[46]=z[46]*z[16];
    z[46]=z[20] + z[46];
    z[46]=z[43] + z[23] + n<T>(27,2)*z[46];
    z[46]=z[46]*z[37];
    z[48]=static_cast<T>(1)+ n<T>(5,2)*z[13];
    z[49]= - n<T>(1,2) + z[13];
    z[49]=z[6]*z[49];
    z[48]=n<T>(1,2)*z[48] + z[49];
    z[49]= - n<T>(7,2) + z[35];
    z[49]=z[14]*z[49];
    z[48]=n<T>(1,2)*z[48] + z[49];
    z[49]=n<T>(1,4)*z[9];
    z[50]=n<T>(1,3) + z[43];
    z[50]=z[50]*z[49];
    z[50]=z[50] - n<T>(1,3) - n<T>(9,16)*z[13];
    z[50]=z[9]*z[50];
    z[30]=z[46] + z[30] + n<T>(1,2)*z[48] + z[50];
    z[46]= - n<T>(37,6)*z[9] - n<T>(53,6) + 15*z[14];
    z[48]= - n<T>(103,9) - z[37];
    z[48]=z[2]*z[48];
    z[48]= - n<T>(53,12) + z[48];
    z[48]=z[2]*z[48];
    z[48]= - n<T>(53,12) + z[48];
    z[48]=z[2]*z[48];
    z[46]=n<T>(1,2)*z[46] + z[48];
    z[48]=n<T>(29,9)*z[11];
    z[50]=z[48]*npow(z[2],4);
    z[46]=n<T>(1,2)*z[46] + z[50];
    z[51]=n<T>(1,2)*z[11];
    z[46]=z[46]*z[51];
    z[52]=static_cast<T>(3)+ n<T>(5,8)*z[14];
    z[52]=z[14]*z[52];
    z[53]= - static_cast<T>(1)+ n<T>(11,3)*z[9];
    z[53]=z[9]*z[53];
    z[54]= - n<T>(43,8) + z[2];
    z[54]=z[2]*z[54];
    z[46]=z[46] + n<T>(1,6)*z[54] + n<T>(1,16)*z[53] - n<T>(23,12) + z[52];
    z[46]=z[11]*z[46];
    z[52]= - static_cast<T>(1)+ z[35];
    z[52]=z[14]*z[52];
    z[52]= - n<T>(1,3) + n<T>(3,2)*z[52];
    z[53]=n<T>(4,3) - n<T>(3,8)*z[9];
    z[53]=z[9]*z[53];
    z[54]=n<T>(11,6) - z[2];
    z[54]=z[2]*z[54];
    z[46]=z[46] + n<T>(1,16)*z[54] - n<T>(9,16)*z[15] + n<T>(1,4)*z[52] + z[53];
    z[46]=z[11]*z[46];
    z[25]= - z[25]*z[31];
    z[52]= - static_cast<T>(1)+ z[9];
    z[52]=z[52]*z[40];
    z[52]=z[52] - z[15];
    z[47]= - z[12]*z[47];
    z[47]=n<T>(1,2)*z[52] + z[47];
    z[47]=z[47]*z[36];
    z[25]=z[25] + z[47];
    z[25]=z[5]*z[25];
    z[25]=n<T>(1,8)*z[25] + n<T>(1,2)*z[30] + z[46];
    z[25]=z[5]*z[25];
    z[30]=n<T>(55,3) + z[24];
    z[30]=z[30]*z[49];
    z[46]= - n<T>(23,6) - z[2];
    z[46]=z[2]*z[46];
    z[46]=n<T>(83,6) + z[46];
    z[46]=z[46]*z[37];
    z[35]= - n<T>(11,2) + z[35];
    z[35]=z[14]*z[35];
    z[30]=z[46] + n<T>(9,2)*z[15] + z[30] + n<T>(17,12) + z[35];
    z[35]=z[2] - 1;
    z[35]=z[35]*z[50];
    z[46]= - n<T>(94,9) - z[45];
    z[46]=z[2]*z[46];
    z[46]=n<T>(485,72) + z[46];
    z[46]=z[46]*z[39];
    z[46]=z[46] + z[35];
    z[46]=z[11]*z[46];
    z[47]=n<T>(1259,36) + z[2];
    z[47]=z[47]*z[37];
    z[47]= - n<T>(79,9) + z[47];
    z[47]=z[2]*z[47];
    z[47]= - n<T>(35,24) + z[47];
    z[47]=z[2]*z[47];
    z[49]= - n<T>(67,6)*z[9] - n<T>(35,6) + 17*z[14];
    z[47]=n<T>(1,4)*z[49] + z[47];
    z[46]=n<T>(1,2)*z[47] + z[46];
    z[46]=z[11]*z[46];
    z[30]=n<T>(1,4)*z[30] + z[46];
    z[30]=z[11]*z[30];
    z[25]=z[25] + n<T>(1,2)*z[29] + z[30];
    z[25]=z[5]*z[25];
    z[29]= - static_cast<T>(55)- n<T>(27,2)*z[12];
    z[29]=z[29]*z[26];
    z[29]= - n<T>(181,4) + z[29];
    z[29]=z[12]*z[29];
    z[30]=n<T>(89,2) + 45*z[12];
    z[17]=z[30]*z[17];
    z[17]=z[17] - z[21];
    z[30]=3*z[2];
    z[17]=z[17]*z[30];
    z[17]=z[17] + n<T>(1,2) + z[29];
    z[17]=z[2]*z[17];
    z[29]=static_cast<T>(79)+ n<T>(105,2)*z[12];
    z[29]=z[29]*z[26];
    z[29]=z[29] + n<T>(113,6) - z[1];
    z[17]=n<T>(1,2)*z[29] + z[17];
    z[17]=z[17]*z[45];
    z[29]=3*z[4];
    z[45]=z[43] - n<T>(43,3) + z[29];
    z[46]=n<T>(1,2)*z[14];
    z[47]= - static_cast<T>(11)+ z[22];
    z[47]=z[47]*z[46];
    z[49]=z[13] + 7;
    z[49]= - 3*z[14] + n<T>(1,2)*z[49];
    z[52]=n<T>(1,4)*z[15];
    z[53]= - z[49]*z[52];
    z[54]=n<T>(3,4)*z[1] - n<T>(3,2);
    z[54]=z[4]*z[54];
    z[54]=n<T>(5,3) + z[54];
    z[54]=z[1]*z[54];
    z[17]=z[17] - n<T>(217,16)*z[12] + z[53] + z[47] + n<T>(1,4)*z[45] + z[54];
    z[17]=z[2]*z[17];
    z[45]=n<T>(1,3)*z[10];
    z[47]=z[29] - n<T>(19,8) - z[45];
    z[45]= - z[29] + static_cast<T>(1)- z[45];
    z[53]= - n<T>(3,4)*z[4] - n<T>(1,4) + z[10];
    z[53]=z[6]*z[53];
    z[45]=n<T>(1,2)*z[45] + z[53];
    z[45]=z[1]*z[45];
    z[45]=n<T>(1,2)*z[47] + z[45];
    z[45]=z[1]*z[45];
    z[47]=n<T>(19,4)*z[15] - n<T>(31,4) + z[24];
    z[47]=z[47]*z[27];
    z[17]=z[17] + z[47] + n<T>(5,4)*z[15] - n<T>(1,4)*z[14] - n<T>(5,16) + z[45];
    z[45]=n<T>(23,2) - z[14];
    z[24]= - z[34] + n<T>(1,2)*z[45] - z[24];
    z[34]=static_cast<T>(7)+ n<T>(9,8)*z[12];
    z[34]=z[34]*z[26];
    z[34]=n<T>(55,4) + z[34];
    z[34]=z[12]*z[34];
    z[45]= - n<T>(61,4) - z[18];
    z[45]=z[45]*z[16];
    z[47]=n<T>(9,2)*z[20];
    z[45]=z[45] + z[47];
    z[53]=n<T>(3,4)*z[2];
    z[45]=z[45]*z[53];
    z[34]=z[45] - n<T>(3,8) + z[34];
    z[34]=z[2]*z[34];
    z[45]= - static_cast<T>(37)- n<T>(51,4)*z[12];
    z[45]=z[45]*z[26];
    z[45]= - n<T>(1303,18) + z[45];
    z[34]=n<T>(1,4)*z[45] + z[34];
    z[34]=z[2]*z[34];
    z[45]=n<T>(145,9) + z[18];
    z[34]=n<T>(11,8)*z[45] + z[34];
    z[34]=z[2]*z[34];
    z[34]= - n<T>(149,144) + z[34];
    z[34]=z[2]*z[34];
    z[24]=n<T>(1,2)*z[24] + z[34];
    z[34]=n<T>(17,2)*z[12];
    z[45]=static_cast<T>(71)+ z[34];
    z[45]=z[45]*z[26];
    z[45]=n<T>(535,2) + z[45];
    z[54]= - static_cast<T>(53)- n<T>(51,2)*z[12];
    z[54]=z[54]*z[27];
    z[55]=z[16]*z[2];
    z[56]=static_cast<T>(1)+ n<T>(51,8)*z[55];
    z[54]=z[54] + z[56];
    z[54]=z[2]*z[54];
    z[45]=n<T>(1,4)*z[45] + z[54];
    z[45]=z[2]*z[45];
    z[54]= - static_cast<T>(331)- 107*z[12];
    z[45]=n<T>(1,4)*z[54] + z[45];
    z[45]=z[2]*z[45];
    z[45]=n<T>(251,12) + z[45];
    z[45]=z[45]*z[42];
    z[54]=37*z[12];
    z[57]=n<T>(37,2)*z[12];
    z[58]= - static_cast<T>(1)+ z[57];
    z[58]=z[2]*z[58];
    z[58]=z[58] - n<T>(475,6) - z[54];
    z[58]=z[58]*z[38];
    z[58]=z[58] + static_cast<T>(15)+ n<T>(37,16)*z[12];
    z[58]=z[2]*z[58];
    z[58]= - n<T>(239,48) + z[58];
    z[58]=z[58]*z[39];
    z[59]= - static_cast<T>(1)+ z[37];
    z[59]=z[2]*z[59];
    z[59]=n<T>(1,2) + z[59];
    z[59]=z[59]*z[50];
    z[58]=z[58] + z[59];
    z[58]=z[11]*z[58];
    z[45]=n<T>(1,4)*z[45] + z[58];
    z[45]=z[11]*z[45];
    z[24]=n<T>(1,2)*z[24] + z[45];
    z[24]=z[11]*z[24];
    z[17]=z[25] + n<T>(1,2)*z[17] + z[24];
    z[17]=z[3]*z[17];
    z[24]=n<T>(1,4)*z[6];
    z[25]=static_cast<T>(3)- z[13];
    z[25]=z[25]*z[24];
    z[45]= - n<T>(19,3) + z[43];
    z[45]=n<T>(1,2)*z[45] + z[28];
    z[45]=z[9]*z[45];
    z[58]=n<T>(29,4) + z[10];
    z[59]= - n<T>(5,2) - 11*z[10];
    z[59]=z[6] + n<T>(1,3)*z[59] + n<T>(9,2)*z[4];
    z[59]=z[1]*z[59];
    z[25]=z[45] + z[46] + z[59] + n<T>(1,3)*z[58] + z[25];
    z[45]=n<T>(1,4)*z[12];
    z[46]=19*z[15] - n<T>(41,2) + 17*z[9];
    z[46]=z[46]*z[45];
    z[58]=n<T>(1,8)*z[15];
    z[59]=static_cast<T>(1)+ z[58];
    z[59]=z[15]*z[59];
    z[25]=z[46] + n<T>(1,2)*z[25] + z[59];
    z[46]= - n<T>(17,4) + z[4];
    z[29]= - z[1]*z[29];
    z[29]=z[29] + 3*z[46] + z[43];
    z[43]= - z[49]*z[32];
    z[22]= - static_cast<T>(7)+ z[22];
    z[22]=z[14]*z[22];
    z[22]=z[43] + n<T>(1,2)*z[29] + z[22];
    z[29]= - n<T>(17,4) - z[18];
    z[29]=z[29]*z[16];
    z[29]=z[29] + z[47];
    z[29]=z[29]*z[30];
    z[43]= - static_cast<T>(1)+ 21*z[12];
    z[43]=z[12]*z[43];
    z[29]=z[29] + static_cast<T>(1)+ n<T>(11,4)*z[43];
    z[29]=z[29]*z[38];
    z[22]=z[29] + n<T>(1,4)*z[22] - z[44];
    z[22]=z[2]*z[22];
    z[29]=static_cast<T>(3)- z[54];
    z[29]=z[2]*z[29];
    z[29]=z[29] + n<T>(2975,18) + z[54];
    z[29]=z[2]*z[29];
    z[29]= - n<T>(2285,18) + z[29];
    z[29]=z[29]*z[39];
    z[29]=n<T>(1,8)*z[29] - z[35];
    z[29]=z[11]*z[29];
    z[35]=17*z[12];
    z[43]=static_cast<T>(45)+ z[35];
    z[43]=z[12]*z[43];
    z[43]=n<T>(3,8)*z[43] - z[56];
    z[43]=z[2]*z[43];
    z[35]=z[43] - n<T>(2153,36) - z[35];
    z[35]=z[2]*z[35];
    z[35]=n<T>(2125,72) + z[35];
    z[43]=n<T>(1,2)*z[42];
    z[35]=z[35]*z[43];
    z[29]=z[35] + z[29];
    z[29]=z[11]*z[29];
    z[18]=static_cast<T>(23)+ z[18];
    z[18]=z[18]*z[16];
    z[18]=z[18] - z[21];
    z[18]=z[18]*z[30];
    z[35]= - n<T>(175,2) - 123*z[12];
    z[35]=z[12]*z[35];
    z[35]=static_cast<T>(1)+ z[35];
    z[18]=n<T>(1,2)*z[35] + z[18];
    z[18]=z[18]*z[38];
    z[18]=z[18] + n<T>(121,9) + n<T>(185,32)*z[12];
    z[18]=z[2]*z[18];
    z[18]= - n<T>(773,288) + z[18];
    z[18]=z[2]*z[18];
    z[35]=static_cast<T>(29)- n<T>(73,2)*z[9];
    z[35]=n<T>(1,3)*z[35] - 9*z[15];
    z[18]=z[29] + n<T>(1,8)*z[35] + z[18];
    z[18]=z[11]*z[18];
    z[17]=z[17] + z[19] + z[18] + n<T>(1,2)*z[25] + z[22];
    z[17]=z[3]*z[17];
    z[18]=n<T>(1,2)*z[7];
    z[19]=z[18] + 1;
    z[22]=3*z[7];
    z[25]= - z[19]*z[22];
    z[25]= - n<T>(7,2) + z[25];
    z[25]=z[9]*z[25];
    z[25]=n<T>(3,4)*z[25] - static_cast<T>(9)- n<T>(7,16)*z[7];
    z[25]=z[9]*z[25];
    z[29]=static_cast<T>(23)+ n<T>(11,2)*z[7];
    z[35]= - n<T>(17,4) - z[7];
    z[35]=z[7]*z[35];
    z[35]= - n<T>(13,4) + z[35];
    z[35]=z[15]*z[35];
    z[29]=n<T>(1,4)*z[29] + z[35];
    z[29]=z[15]*z[29];
    z[35]=static_cast<T>(35)+ 13*z[7];
    z[18]=z[35]*z[18];
    z[18]=static_cast<T>(29)+ z[18];
    z[18]=z[9]*z[18];
    z[35]= - static_cast<T>(1)+ 5*z[7];
    z[18]=n<T>(3,2)*z[35] + z[18];
    z[35]= - n<T>(47,4) - 7*z[7];
    z[35]=z[7]*z[35];
    z[35]=n<T>(1,2) + z[35];
    z[35]=z[15]*z[35];
    z[18]=n<T>(1,2)*z[18] + z[35];
    z[18]=z[18]*z[27];
    z[18]=z[18] + z[29] - n<T>(37,16) + z[25];
    z[18]=z[12]*z[18];
    z[25]=npow(z[9],2);
    z[27]=static_cast<T>(11)+ z[22];
    z[27]=z[27]*z[25];
    z[29]=z[24] + 1;
    z[35]=z[15]*z[6];
    z[35]=n<T>(3,8)*z[35] + z[29];
    z[35]=z[15]*z[35];
    z[24]=z[24] + z[10];
    z[18]=n<T>(15,8)*z[55] + z[18] + z[35] + n<T>(3,16)*z[27] - n<T>(1,2)*z[23] - n<T>(1,4)*z[13] - z[24];
    z[23]=n<T>(1,3)*z[15];
    z[27]=static_cast<T>(29)- 16*z[7];
    z[27]=z[27]*z[23];
    z[27]=z[27] + 35;
    z[27]=z[7]*z[27];
    z[27]= - n<T>(151,24) + z[27];
    z[27]=z[27]*z[23];
    z[35]=z[7] + 1;
    z[44]=z[35]*z[15];
    z[44]= - static_cast<T>(1)+ z[44];
    z[46]=n<T>(58,9)*z[11];
    z[44]=z[46]*z[7]*z[44];
    z[46]=z[7] - n<T>(9,2);
    z[27]=z[44] + n<T>(1,8)*z[46] + z[27];
    z[27]=z[11]*z[27];
    z[44]= - z[32] - 1;
    z[44]=z[7]*z[44];
    z[44]=n<T>(23,2) + z[44];
    z[44]=z[44]*z[58];
    z[27]=z[44] + z[27];
    z[27]=z[11]*z[27];
    z[44]=z[6] + z[32];
    z[44]=z[44]*z[58];
    z[27]=z[44] + z[27];
    z[27]=z[11]*z[27];
    z[44]=z[46]*z[58];
    z[46]=npow(z[7],2)*z[32];
    z[46]= - 2*z[7] + z[46];
    z[46]=z[15]*z[46]*z[48];
    z[44]=z[44] + z[46];
    z[44]=z[5]*z[44]*npow(z[11],3);
    z[33]= - z[12]*z[33];
    z[33]=n<T>(1,4)*z[25] + z[33];
    z[33]=z[33]*z[36];
    z[36]=npow(z[15],2)*z[31];
    z[33]=z[36] + z[33];
    z[27]=z[44] + n<T>(1,8)*z[33] + z[27];
    z[27]=z[5]*z[27];
    z[19]=z[19]*z[7];
    z[33]= - static_cast<T>(1)- z[19];
    z[33]=z[33]*z[40];
    z[36]=n<T>(1,4)*z[7];
    z[33]=z[33] - static_cast<T>(3)- z[36];
    z[33]=z[9]*z[33];
    z[33]= - static_cast<T>(1)+ z[33];
    z[40]=3*z[15];
    z[44]= - n<T>(1,2) - z[19];
    z[44]=z[44]*z[40];
    z[44]=z[44] + n<T>(11,2) + 9*z[7];
    z[44]=z[15]*z[44];
    z[33]=3*z[33] + z[44];
    z[44]=n<T>(3,2) + z[19];
    z[44]=z[9]*z[44];
    z[35]= - n<T>(3,2)*z[35] + z[44];
    z[19]=n<T>(3,4) + z[19];
    z[19]=z[15]*z[19];
    z[19]=n<T>(1,2)*z[35] + z[19];
    z[19]=z[19]*z[26];
    z[19]=n<T>(1,2)*z[33] + z[19];
    z[19]=z[12]*z[19];
    z[33]=static_cast<T>(7)+ n<T>(3,2)*z[7];
    z[25]=z[33]*z[25];
    z[25]=z[25] + z[40];
    z[19]=n<T>(1,4)*z[25] + z[19];
    z[19]=z[19]*z[26];
    z[25]= - static_cast<T>(1)- z[40];
    z[25]=z[52]*z[6]*z[25];
    z[19]= - n<T>(27,4)*z[20] + z[25] + z[19];
    z[25]= - n<T>(559,3) - 367*z[7];
    z[25]=z[7]*z[25];
    z[25]=n<T>(383,3) + z[25];
    z[25]=z[25]*z[32];
    z[25]=z[25] - n<T>(271,3) + n<T>(367,2)*z[7];
    z[26]= - n<T>(385,6) + z[22];
    z[26]=n<T>(1,2)*z[26] - z[30];
    z[26]=z[2]*z[26];
    z[25]=n<T>(1,6)*z[25] + z[26];
    z[26]=z[11]*z[42];
    z[25]=n<T>(1,4)*z[25] + n<T>(29,3)*z[26];
    z[25]=z[11]*z[25];
    z[26]= - static_cast<T>(247)- n<T>(85,2)*z[7];
    z[26]=z[26]*z[36];
    z[26]=static_cast<T>(29)+ z[26];
    z[23]=z[26]*z[23];
    z[26]=n<T>(335,3) - 79*z[7];
    z[23]=n<T>(1,8)*z[26] + z[23];
    z[23]=z[15]*z[23];
    z[23]=n<T>(25,4) + z[23];
    z[23]=n<T>(1,6)*z[23] + z[25];
    z[23]=z[11]*z[23];
    z[25]= - n<T>(3,16)*z[15] + n<T>(1,16)*z[7] - z[29];
    z[25]=z[15]*z[25];
    z[23]=z[23] - n<T>(1,16) + z[25];
    z[23]=z[11]*z[23];
    z[19]=z[27] + n<T>(1,4)*z[19] + z[23];
    z[19]=z[5]*z[19];
    z[23]= - n<T>(457,18)*z[7] + n<T>(137,6) + z[6];
    z[25]= - n<T>(85,36) + z[7];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(55,18) + z[25];
    z[25]=z[15]*z[25];
    z[23]=n<T>(1,4)*z[23] + z[25];
    z[23]=z[15]*z[23];
    z[25]= - n<T>(15,2) - z[7];
    z[23]=z[37] + n<T>(1,4)*z[25] + z[23];
    z[25]=static_cast<T>(5)+ z[7];
    z[26]= - static_cast<T>(1)- n<T>(1,6)*z[7];
    z[26]=z[7]*z[26];
    z[26]= - n<T>(161,3) + 197*z[26];
    z[26]=z[15]*z[26];
    z[25]=n<T>(197,6)*z[25] + z[26];
    z[26]=n<T>(997,9) + z[30];
    z[26]=z[2]*z[26];
    z[25]=n<T>(1,3)*z[25] + z[26];
    z[26]= - n<T>(929,6) + z[22];
    z[26]=n<T>(1,8)*z[26] - z[2];
    z[26]=z[26]*z[42];
    z[26]=z[26] + z[41];
    z[26]=z[11]*z[26];
    z[25]=n<T>(1,8)*z[25] + z[26];
    z[25]=z[11]*z[25];
    z[23]=n<T>(1,2)*z[23] + z[25];
    z[23]=z[11]*z[23];
    z[18]=z[19] + n<T>(1,2)*z[18] + z[23];
    z[18]=z[5]*z[18];
    z[19]=n<T>(1,3)*z[8];
    z[23]=z[1]*z[8];
    z[25]=n<T>(5,4)*z[14] - n<T>(1,4) + z[19];
    z[25]=z[14]*z[25];
    z[23]=z[25] + n<T>(1,6)*z[23] + n<T>(1,8)*z[13] - z[19] - n<T>(1,3) + z[24];
    z[24]=n<T>(83,3) + z[22];
    z[24]=z[7]*z[24];
    z[24]= - static_cast<T>(71)+ z[24];
    z[24]=z[24]*z[52];
    z[21]= - n<T>(31,2)*z[16] + z[21];
    z[21]=z[21]*z[53];
    z[21]=z[21] + static_cast<T>(1)+ n<T>(45,8)*z[12];
    z[21]=z[2]*z[21];
    z[25]= - n<T>(503,18) - z[7];
    z[21]=n<T>(1,2)*z[25] + z[21];
    z[21]=z[2]*z[21];
    z[21]=z[21] + z[24] + n<T>(73,12)*z[9] - n<T>(3,4)*z[7] - n<T>(37,6) + 13*z[14];
    z[24]= - n<T>(29,4)*z[12] + z[56];
    z[24]=z[2]*z[24];
    z[24]=n<T>(953,24) + z[24];
    z[24]=z[24]*z[43];
    z[25]= - static_cast<T>(3)+ z[57];
    z[25]=z[2]*z[25];
    z[25]=z[25] - n<T>(775,9) + z[7];
    z[25]=z[25]*z[39];
    z[25]=n<T>(1,4)*z[25] + z[50];
    z[25]=z[11]*z[25];
    z[24]=z[24] + z[25];
    z[24]=z[11]*z[24];
    z[21]=n<T>(1,2)*z[21] + z[24];
    z[21]=z[21]*z[51];
    z[24]=z[28]*z[8];
    z[25]=z[24] - z[19];
    z[26]= - n<T>(5,8) - z[25];
    z[26]=z[1]*z[26];
    z[25]= - n<T>(7,8) - z[25];
    z[25]=z[7]*z[25];
    z[27]= - n<T>(359,3) - 5*z[13];
    z[25]=z[25] + n<T>(1,8)*z[27] + z[26];
    z[24]=n<T>(9,8) + z[24];
    z[24]=z[1]*z[24];
    z[19]= - n<T>(9,8)*z[7] + z[24] - n<T>(9,8) - z[19];
    z[19]=z[19]*z[36];
    z[24]=n<T>(39,4) + z[28];
    z[24]=z[1]*z[24];
    z[19]=z[19] + n<T>(1,8)*z[24] - n<T>(1,3) + n<T>(1,16)*z[13];
    z[19]=z[9]*z[19];
    z[19]=n<T>(1,4)*z[25] + z[19];
    z[19]=z[9]*z[19];
    z[24]= - static_cast<T>(7)- z[7];
    z[22]=z[24]*z[22];
    z[22]= - static_cast<T>(17)+ z[22];
    z[22]=z[22]*z[52];
    z[24]=static_cast<T>(7)- z[7];
    z[25]=n<T>(15,4) + z[7];
    z[25]=z[7]*z[25];
    z[25]=n<T>(51,4) + z[25];
    z[25]=z[9]*z[25];
    z[22]=z[22] + n<T>(1,4)*z[24] + z[25];
    z[22]=z[22]*z[45];
    z[24]= - n<T>(119,9) - z[31];
    z[24]=z[15]*z[24];
    z[24]=z[24] + n<T>(19,3)*z[14] + n<T>(5,9) - z[31];
    z[24]=z[24]*z[58];
    z[16]= - 5*z[16] + 3*z[20];
    z[16]=z[2]*z[16];
    z[16]=n<T>(9,2)*z[16] - static_cast<T>(1)+ z[34];
    z[16]=z[16]*z[38];

    r += z[16] + z[17] + z[18] + z[19] + z[21] + z[22] + n<T>(1,2)*z[23] + 
      z[24];
 
    return r;
}

template double qg_2lha_r269(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r269(const std::array<dd_real,30>&);
#endif
