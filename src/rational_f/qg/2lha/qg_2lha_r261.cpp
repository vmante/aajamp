#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r261(const std::array<T,30>& k) {
  T z[60];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[6];
    z[8]=k[12];
    z[9]=k[11];
    z[10]=k[24];
    z[11]=k[14];
    z[12]=k[4];
    z[13]=k[5];
    z[14]=k[9];
    z[15]=k[10];
    z[16]=n<T>(1,4)*z[3];
    z[17]=n<T>(3,2)*z[3];
    z[18]= - static_cast<T>(5)- z[17];
    z[18]=z[18]*z[16];
    z[19]=n<T>(2,3)*z[9];
    z[20]=z[10] + n<T>(3,4)*z[3];
    z[21]=z[19] + z[20];
    z[22]= - n<T>(71,16) - z[21];
    z[22]=z[9]*z[22];
    z[23]=n<T>(1,2)*z[10];
    z[24]=z[23] + 1;
    z[24]=z[24]*z[10];
    z[18]=z[22] + z[18] - n<T>(13,32) - z[24];
    z[18]=z[9]*z[18];
    z[22]=npow(z[3],2);
    z[25]=n<T>(1,3)*z[3];
    z[26]= - n<T>(7,4) - z[25];
    z[26]=z[26]*z[22];
    z[18]=n<T>(1,2)*z[26] + z[18];
    z[18]=z[9]*z[18];
    z[26]=npow(z[10],2);
    z[27]=z[26] + n<T>(3,4)*z[22];
    z[28]=n<T>(9,32) + z[21];
    z[28]=z[9]*z[28];
    z[28]=n<T>(1,2)*z[27] + z[28];
    z[28]=z[9]*z[28];
    z[29]=npow(z[3],3);
    z[28]=n<T>(1,6)*z[29] + z[28];
    z[30]=z[2]*z[9];
    z[28]=z[28]*z[30];
    z[31]=z[5]*npow(z[9],3);
    z[32]=2*z[9];
    z[33]=n<T>(21,32) + z[32];
    z[33]=z[33]*z[31];
    z[29]=n<T>(1,12)*z[29];
    z[18]=z[28] + z[33] - z[29] + z[18];
    z[18]=z[2]*z[18];
    z[28]=n<T>(1,12)*z[3];
    z[33]=n<T>(1,2) + 25*z[3];
    z[33]=z[33]*z[28];
    z[34]= - n<T>(1,2) + z[10];
    z[34]=z[10]*z[34];
    z[35]=3*z[10];
    z[36]=n<T>(65,8)*z[9] + n<T>(13,4)*z[3] + n<T>(229,24) + z[35];
    z[36]=z[9]*z[36];
    z[33]=z[36] + z[33] + n<T>(5,32) + z[34];
    z[34]=n<T>(1,2)*z[9];
    z[33]=z[33]*z[34];
    z[36]=n<T>(1,3)*z[9];
    z[37]= - n<T>(529,32) - 4*z[9];
    z[37]=z[37]*z[36];
    z[37]= - n<T>(13,32) + z[37];
    z[38]=npow(z[9],2);
    z[37]=z[37]*z[38];
    z[39]=n<T>(4,3)*z[9];
    z[40]=n<T>(3,8) + z[39];
    z[40]=z[40]*z[31];
    z[41]=z[11] - 1;
    z[42]=z[41]*npow(z[11],3);
    z[43]=3*z[42];
    z[37]=z[40] + z[43] + z[37];
    z[37]=z[5]*z[37];
    z[40]=z[11] - 2;
    z[40]=z[40]*z[11];
    z[44]=z[40] + 1;
    z[45]=npow(z[11],2);
    z[44]=z[44]*z[45];
    z[46]= - z[26] + n<T>(11,12)*z[22];
    z[18]=z[18] + z[37] + z[33] + n<T>(1,4)*z[46] + 3*z[44];
    z[18]=z[2]*z[18];
    z[33]=n<T>(1,8)*z[9];
    z[37]=static_cast<T>(13)+ n<T>(187,12)*z[9];
    z[37]=z[9]*z[37];
    z[37]= - n<T>(11,8) + z[37];
    z[37]=z[37]*z[33];
    z[47]=n<T>(1,32)*z[38];
    z[48]=static_cast<T>(3)- z[9];
    z[48]=z[48]*z[47];
    z[42]=9*z[42];
    z[48]= - z[42] + z[48];
    z[48]=z[5]*z[48];
    z[49]=static_cast<T>(25)- 12*z[11];
    z[49]=z[11]*z[49];
    z[49]= - n<T>(53,4) + z[49];
    z[49]=z[11]*z[49];
    z[49]=n<T>(1,4) + z[49];
    z[49]=z[11]*z[49];
    z[37]=z[48] + z[37] - n<T>(1,4)*z[13] + z[49];
    z[37]=z[5]*z[37];
    z[48]=z[1] + n<T>(1,2);
    z[49]=z[48]*z[25];
    z[50]=n<T>(1,3) + z[1];
    z[49]=n<T>(1,4)*z[50] + z[49];
    z[49]=z[3]*z[49];
    z[49]=n<T>(5,8) + z[49];
    z[49]=z[3]*z[49];
    z[50]=static_cast<T>(1)- z[10];
    z[50]=z[10]*z[50];
    z[50]=n<T>(1,8) + z[50];
    z[49]=n<T>(1,2)*z[50] + z[49];
    z[50]=3*z[11];
    z[51]=z[50] - 7;
    z[52]=z[11]*z[51];
    z[52]= - n<T>(81,4) - 4*z[52];
    z[52]=z[11]*z[52];
    z[52]=n<T>(9,2) + z[52];
    z[52]=z[11]*z[52];
    z[53]=n<T>(3,4)*z[10];
    z[54]= - n<T>(485,96)*z[9] - n<T>(17,16)*z[3] - n<T>(4,3) - z[53];
    z[54]=z[9]*z[54];
    z[18]=z[18] + z[37] + z[54] + n<T>(1,2)*z[49] + z[52];
    z[18]=z[2]*z[18];
    z[37]=n<T>(5,4) + z[40];
    z[37]=z[37]*z[50];
    z[37]= - n<T>(5,4) + z[37];
    z[37]=z[11]*z[37];
    z[49]=n<T>(1,16)*z[9];
    z[52]=n<T>(13,4) - n<T>(95,3)*z[9];
    z[52]=z[52]*z[49];
    z[54]= - static_cast<T>(29)+ 61*z[9];
    z[54]=z[9]*z[54];
    z[54]= - static_cast<T>(1)+ z[54];
    z[54]=z[54]*z[49];
    z[55]=z[41]*z[11];
    z[56]=z[55] + z[13];
    z[54]=z[54] + z[56];
    z[57]=n<T>(1,2)*z[5];
    z[54]=z[54]*z[57];
    z[58]=z[26] + n<T>(1,8) - z[13];
    z[37]=z[54] + z[52] + n<T>(1,4)*z[58] + z[37];
    z[37]=z[5]*z[37];
    z[52]=z[25]*z[1];
    z[54]= - n<T>(11,6) - z[1];
    z[54]=z[1]*z[54];
    z[54]= - n<T>(7,4) + z[54];
    z[54]=n<T>(1,2)*z[54] - z[52];
    z[54]=z[3]*z[54];
    z[54]=n<T>(3,8) + z[54];
    z[54]=z[3]*z[54];
    z[54]=z[54] - n<T>(13,96) + z[10];
    z[58]= - static_cast<T>(22)+ 9*z[11];
    z[58]=z[11]*z[58];
    z[58]=static_cast<T>(18)+ z[58];
    z[58]=z[11]*z[58];
    z[58]= - static_cast<T>(6)+ z[58];
    z[58]=z[11]*z[58];
    z[18]=z[18] + z[37] + n<T>(403,192)*z[9] + n<T>(1,2)*z[54] + z[58];
    z[18]=z[2]*z[18];
    z[21]=n<T>(67,48) + z[21];
    z[21]=z[9]*z[21];
    z[37]=static_cast<T>(1)+ n<T>(3,8)*z[3];
    z[37]=z[3]*z[37];
    z[21]=z[21] + z[37] + n<T>(5,32) + z[24];
    z[21]=z[9]*z[21];
    z[24]=n<T>(31,8) + z[3];
    z[24]=z[24]*z[22];
    z[24]=n<T>(1,2)*z[26] + n<T>(1,3)*z[24];
    z[21]=n<T>(1,2)*z[24] + z[21];
    z[21]=z[9]*z[21];
    z[24]=n<T>(1,4)*z[27];
    z[27]= - n<T>(3,16) - z[20];
    z[27]=n<T>(1,2)*z[27] - z[36];
    z[27]=z[9]*z[27];
    z[27]= - z[24] + z[27];
    z[27]=z[9]*z[27];
    z[27]= - z[29] + z[27];
    z[27]=z[27]*z[30];
    z[37]= - n<T>(3,16) - z[19];
    z[37]=z[37]*z[31];
    z[21]=z[27] + z[37] + z[29] + z[21];
    z[21]=z[2]*z[21];
    z[17]= - static_cast<T>(19)- z[17];
    z[16]=z[17]*z[16];
    z[17]= - static_cast<T>(5)- z[23];
    z[17]=z[10]*z[17];
    z[16]=z[16] - n<T>(209,96) + z[17];
    z[17]= - n<T>(29,6) - z[20];
    z[17]=n<T>(1,2)*z[17] - z[36];
    z[17]=z[9]*z[17];
    z[16]=n<T>(1,2)*z[16] + z[17];
    z[16]=z[9]*z[16];
    z[17]= - n<T>(35,2) - z[3];
    z[17]=z[3]*z[17];
    z[17]= - n<T>(17,2) + z[17];
    z[17]=z[17]*z[28];
    z[23]= - n<T>(1,2) - z[10];
    z[23]=z[10]*z[23];
    z[16]=z[16] + z[17] - n<T>(1,64) + z[23];
    z[16]=z[9]*z[16];
    z[17]=n<T>(41,32) + z[19];
    z[17]=z[9]*z[17];
    z[17]=n<T>(1,8) + z[17];
    z[17]=z[17]*z[38];
    z[19]= - n<T>(3,32) - z[36];
    z[19]=z[19]*z[31];
    z[17]=z[17] + z[19];
    z[17]=z[5]*z[17];
    z[19]=n<T>(1,2)*z[1];
    z[23]=z[19] + 1;
    z[27]= - z[3]*z[23];
    z[27]= - n<T>(11,4) + z[27];
    z[22]=z[27]*z[22];
    z[16]=z[21] + z[17] + n<T>(1,6)*z[22] + z[16];
    z[16]=z[2]*z[16];
    z[17]=n<T>(1,3) - 3*z[9];
    z[17]=z[9]*z[17];
    z[17]=n<T>(3,2) + 11*z[17];
    z[17]=z[17]*z[49];
    z[21]= - static_cast<T>(1)+ z[36];
    z[21]=z[21]*z[47];
    z[21]=z[43] + z[21];
    z[21]=z[5]*z[21];
    z[17]=z[21] + 6*z[44] + z[17];
    z[17]=z[5]*z[17];
    z[21]=n<T>(107,48)*z[9] + n<T>(11,4)*z[3] + n<T>(115,24) + z[35];
    z[21]=z[21]*z[34];
    z[22]=static_cast<T>(2)+ z[53];
    z[22]=z[10]*z[22];
    z[27]=static_cast<T>(5)+ n<T>(13,8)*z[3];
    z[27]=z[3]*z[27];
    z[21]=z[21] + n<T>(1,2)*z[27] + n<T>(29,96) + z[22];
    z[21]=z[9]*z[21];
    z[22]=n<T>(1,4)*z[1];
    z[27]=static_cast<T>(1)+ z[22];
    z[23]=z[23]*z[1];
    z[28]=n<T>(1,2) + z[23];
    z[28]=z[3]*z[28];
    z[27]=n<T>(13,2)*z[27] + z[28];
    z[27]=z[3]*z[27];
    z[27]=n<T>(1,2) + z[27];
    z[27]=z[27]*z[25];
    z[27]=z[27] - n<T>(3,32) + z[26];
    z[28]=z[11] - 3;
    z[28]=z[28]*z[11];
    z[37]=static_cast<T>(3)+ z[28];
    z[37]=z[11]*z[37];
    z[37]= - static_cast<T>(1)+ z[37];
    z[37]=z[37]*z[50];
    z[16]=z[16] + z[17] + z[21] + n<T>(1,2)*z[27] + z[37];
    z[16]=z[2]*z[16];
    z[17]=n<T>(21,2) - 31*z[9];
    z[17]=z[9]*z[17];
    z[17]=n<T>(1,2) + z[17];
    z[17]=z[17]*z[33];
    z[17]=z[17] - z[56];
    z[21]=n<T>(1,4)*z[5];
    z[17]=z[17]*z[21];
    z[27]= - n<T>(13,2) - 6*z[40];
    z[27]=z[11]*z[27];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[11]*z[27];
    z[37]=n<T>(167,3) - z[9];
    z[37]=z[9]*z[37];
    z[37]=n<T>(61,6) + z[37];
    z[37]=z[9]*z[37];
    z[17]=z[17] + n<T>(1,32)*z[37] - n<T>(1,32) + z[27];
    z[17]=z[5]*z[17];
    z[27]= - z[48]*z[52];
    z[37]= - n<T>(17,6) - z[1];
    z[37]=z[1]*z[37];
    z[37]= - n<T>(5,2) + z[37];
    z[27]=n<T>(1,2)*z[37] + z[27];
    z[27]=z[3]*z[27];
    z[37]= - n<T>(7,2) + z[1];
    z[27]=n<T>(1,2)*z[37] + z[27];
    z[27]=z[3]*z[27];
    z[37]= - static_cast<T>(1)- n<T>(3,2)*z[10];
    z[37]=z[10]*z[37];
    z[27]=z[27] + n<T>(1,32)*z[1] - n<T>(149,96) + z[37];
    z[37]= - n<T>(71,24)*z[9] - n<T>(15,4)*z[3] - n<T>(155,96) - z[35];
    z[37]=z[37]*z[34];
    z[44]= - n<T>(73,4) - 6*z[28];
    z[44]=z[11]*z[44];
    z[44]=n<T>(27,4) + z[44];
    z[44]=z[11]*z[44];
    z[16]=z[16] + z[17] + z[37] + n<T>(1,2)*z[27] + z[44];
    z[16]=z[2]*z[16];
    z[17]=n<T>(25,3) - n<T>(17,2)*z[9];
    z[17]=z[17]*z[33];
    z[27]=n<T>(1,2)*z[55];
    z[37]=static_cast<T>(1)- n<T>(15,8)*z[9];
    z[37]=z[9]*z[37];
    z[37]=n<T>(95,96) + z[37];
    z[37]=z[9]*z[37];
    z[37]=z[27] + z[37];
    z[37]=z[37]*z[57];
    z[26]= - n<T>(7,3) + z[26];
    z[17]=z[37] + z[17] + n<T>(1,4)*z[26] + z[40];
    z[17]=z[5]*z[17];
    z[26]=npow(z[1],2);
    z[37]=z[26]*z[3];
    z[23]=n<T>(7,8) + z[23];
    z[23]=z[1]*z[23];
    z[23]=z[23] + n<T>(1,6)*z[37];
    z[23]=z[3]*z[23];
    z[23]=z[23] + n<T>(7,4) - z[26];
    z[23]=z[3]*z[23];
    z[35]=n<T>(113,48)*z[1] + n<T>(319,48) + z[35];
    z[23]=n<T>(1,2)*z[35] + z[23];
    z[28]=n<T>(13,4) + z[28];
    z[28]=z[11]*z[28];
    z[28]= - n<T>(7,4) + z[28];
    z[28]=z[28]*z[50];
    z[16]=z[16] + z[17] + n<T>(253,192)*z[9] + n<T>(1,2)*z[23] + z[28];
    z[16]=z[2]*z[16];
    z[17]=z[8] - 3;
    z[23]=n<T>(1,2)*z[8];
    z[28]= - z[23]*z[17];
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=z[13]*z[28];
    z[35]=z[41]*z[50];
    z[41]= - z[6]*z[22];
    z[41]=z[41] - static_cast<T>(1)+ n<T>(3,4)*z[1];
    z[41]=z[6]*z[41];
    z[28]=z[35] + z[41] + z[28];
    z[17]=z[8]*z[17];
    z[17]=n<T>(15,2)*z[17] + static_cast<T>(31)- n<T>(5,2)*z[15];
    z[17]=n<T>(5,2)*z[55] + n<T>(1,16)*z[17] + z[6];
    z[41]= - static_cast<T>(9)+ z[23];
    z[41]=z[8]*z[41];
    z[41]=n<T>(9,2) + z[41];
    z[44]=n<T>(3,2) + z[11];
    z[44]=z[11]*z[44];
    z[41]=n<T>(1,16)*z[41] + z[44];
    z[44]=z[8] + 31;
    z[44]= - z[11] + n<T>(1,32)*z[44];
    z[44]=z[44]*z[9];
    z[41]=n<T>(1,2)*z[41] - z[44];
    z[41]=z[9]*z[41];
    z[17]=n<T>(1,2)*z[17] + z[41];
    z[17]=z[9]*z[17];
    z[41]=npow(z[6],2);
    z[47]=z[41]*z[22];
    z[48]=5*z[15];
    z[52]=z[48] - z[9];
    z[52]=z[52]*z[33];
    z[52]= - z[6] + z[52];
    z[52]=z[9]*z[52];
    z[53]=z[15]*z[31];
    z[47]= - n<T>(1,8)*z[53] + z[47] + z[52];
    z[21]=z[47]*z[21];
    z[17]=z[21] + n<T>(1,4)*z[28] + z[17];
    z[17]=z[5]*z[17];
    z[21]=n<T>(25,4) - z[8];
    z[21]=n<T>(7,4)*z[21] - z[6];
    z[21]= - z[44] + n<T>(1,4)*z[21] + z[40];
    z[21]=z[9]*z[21];
    z[28]=n<T>(1,4)*z[6];
    z[44]=npow(z[1],3);
    z[47]=z[44]*z[28];
    z[52]=z[1] - 1;
    z[47]=z[47] - z[52];
    z[47]=z[6]*z[47];
    z[53]=n<T>(13,3) + n<T>(7,2)*z[8];
    z[53]=n<T>(1,2)*z[53] + z[1];
    z[47]=n<T>(1,2)*z[53] + z[47];
    z[17]=z[17] + z[21] + n<T>(1,4)*z[47] + z[40];
    z[17]=z[5]*z[17];
    z[21]=z[7] - 1;
    z[40]=n<T>(1,2)*z[6];
    z[44]= - z[21]*z[44]*z[40];
    z[47]=z[19] + z[21];
    z[47]=z[47]*z[26];
    z[44]=z[47] + z[44];
    z[44]=z[6]*z[44];
    z[47]= - n<T>(211,8) + 7*z[7];
    z[47]=z[1]*z[47];
    z[47]= - n<T>(59,8) + z[47];
    z[44]=n<T>(1,3)*z[47] + z[44];
    z[47]=n<T>(1,2)*z[7];
    z[53]= - static_cast<T>(1)+ z[47];
    z[53]=z[1]*z[53];
    z[53]= - n<T>(1,2) + z[53];
    z[37]=z[53]*z[37];
    z[53]= - n<T>(1,2) + z[1];
    z[53]=z[1]*z[53];
    z[37]=z[53] + z[37];
    z[37]=z[3]*z[37];
    z[53]=n<T>(1,16)*z[14];
    z[54]= - static_cast<T>(19)+ 9*z[14];
    z[54]=z[54]*z[53];
    z[37]=z[54] + n<T>(1,4)*z[44] + z[37];
    z[16]=z[16] + n<T>(1,2)*z[37] + z[17];
    z[16]=z[4]*z[16];
    z[17]=n<T>(1,4)*z[9];
    z[37]=n<T>(31,8)*z[9] - n<T>(15,4) + z[6];
    z[37]=z[37]*z[17];
    z[44]= - static_cast<T>(1)+ n<T>(3,4)*z[6];
    z[44]=z[44]*z[6];
    z[54]= - n<T>(143,3) + 25*z[15];
    z[37]=z[37] + n<T>(1,64)*z[54] - z[44];
    z[37]=z[9]*z[37];
    z[54]=static_cast<T>(1)- n<T>(7,8)*z[15];
    z[54]=z[49] + n<T>(1,2)*z[54] - z[6];
    z[54]=z[9]*z[54];
    z[55]= - static_cast<T>(1)+ n<T>(3,2)*z[6];
    z[55]=z[55]*z[6];
    z[54]=z[55] + z[54];
    z[54]=z[9]*z[54];
    z[56]=z[15]*z[33];
    z[56]=z[6] + z[56];
    z[56]=z[9]*z[56];
    z[56]= - z[41] + z[56];
    z[56]=z[5]*z[56]*z[34];
    z[58]=n<T>(1,2)*z[41];
    z[54]=z[56] - z[58] + z[54];
    z[54]=z[54]*z[57];
    z[22]=z[22] - 1;
    z[56]= - z[6]*z[22];
    z[56]= - static_cast<T>(1)+ z[56];
    z[56]=z[6]*z[56];
    z[27]=z[56] - z[27];
    z[27]=z[54] + n<T>(1,2)*z[27] + z[37];
    z[27]=z[5]*z[27];
    z[37]=n<T>(7,3)*z[7];
    z[48]=n<T>(23,6) - z[48];
    z[48]=n<T>(1,4)*z[48] + z[37];
    z[54]= - n<T>(7,8) + 5*z[13];
    z[56]=n<T>(7,8) - z[13];
    z[56]=z[8]*z[56];
    z[54]=n<T>(1,2)*z[54] + z[56];
    z[54]=z[8]*z[54];
    z[48]=z[54] + n<T>(1,2)*z[48] - z[13];
    z[52]= - z[52]*z[19];
    z[52]= - static_cast<T>(1)+ z[52];
    z[52]=z[52]*z[40];
    z[52]=z[52] + static_cast<T>(1)- n<T>(1,8)*z[1];
    z[52]=z[6]*z[52];
    z[48]=n<T>(1,2)*z[48] + z[52];
    z[40]=z[40] - 1;
    z[40]=z[40]*z[6];
    z[52]= - n<T>(61,2) + z[8];
    z[52]=z[8]*z[52];
    z[52]=n<T>(193,3) + z[52];
    z[52]= - z[53] + n<T>(1,16)*z[52] + z[40];
    z[53]= - static_cast<T>(31)- n<T>(3,2)*z[8];
    z[53]=n<T>(1,16)*z[53] + z[50];
    z[53]=z[9]*z[53];
    z[54]= - n<T>(7,4) + 2*z[11];
    z[54]=z[11]*z[54];
    z[52]=z[53] + n<T>(1,2)*z[52] + z[54];
    z[52]=z[9]*z[52];
    z[53]= - n<T>(3,4) + z[11];
    z[53]=z[11]*z[53];
    z[27]=z[27] + z[52] + n<T>(1,2)*z[48] + z[53];
    z[27]=z[5]*z[27];
    z[21]=z[21]*z[26]*z[28];
    z[26]=n<T>(13,2) - z[37];
    z[28]= - static_cast<T>(1)- z[47];
    z[28]=z[1]*z[28];
    z[21]=z[21] + n<T>(1,2)*z[26] + z[28];
    z[26]=static_cast<T>(1)+ z[7];
    z[26]=z[26]*z[19];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[1]*z[26];
    z[26]=n<T>(1,2) + z[26];
    z[28]=static_cast<T>(1)- n<T>(3,4)*z[7];
    z[28]=z[1]*z[28];
    z[28]=n<T>(1,2) + z[28];
    z[28]=z[3]*z[1]*z[28];
    z[26]=n<T>(1,2)*z[26] + z[28];
    z[26]=z[3]*z[26];
    z[28]=n<T>(1,8)*z[14];
    z[37]= - static_cast<T>(7)+ n<T>(9,2)*z[14];
    z[37]=z[37]*z[28];
    z[16]=z[16] + z[18] + z[27] + z[37] + n<T>(1,4)*z[21] + z[26];
    z[16]=z[4]*z[16];
    z[18]=static_cast<T>(1)- z[19];
    z[18]=z[18]*z[58];
    z[19]= - z[23] + 1;
    z[19]=z[13]*z[19];
    z[19]= - n<T>(7,8) + z[19];
    z[19]=z[8]*z[19];
    z[21]=z[1]*npow(z[8],2);
    z[23]=z[12]*z[13];
    z[18]=z[18] + z[23] + z[19] + n<T>(7,16)*z[21];
    z[19]=n<T>(1,2)*z[12];
    z[21]=z[19]*z[7];
    z[23]= - static_cast<T>(1)+ n<T>(3,2)*z[7];
    z[23]=z[1]*z[23];
    z[23]= - z[21] - n<T>(1,2) + z[23];
    z[23]=z[3]*z[23];
    z[26]= - n<T>(1,2) - z[7];
    z[26]=z[1]*z[26];
    z[21]=z[23] + z[26] + z[21];
    z[21]=z[3]*z[21];
    z[23]=n<T>(127,8) + z[12];
    z[26]=z[6] - 1;
    z[27]= - z[6]*z[26];
    z[37]= - n<T>(51,8) + z[40];
    z[37]=z[14]*z[37];
    z[23]=z[37] + n<T>(1,2)*z[23] + z[27];
    z[27]=n<T>(1,2)*z[14];
    z[23]=z[23]*z[27];
    z[18]=z[23] + n<T>(1,2)*z[18] + z[21];
    z[21]=z[14]*z[12];
    z[23]= - n<T>(71,3) + 35*z[12];
    z[23]=z[23]*z[21];
    z[37]=static_cast<T>(185)- n<T>(679,2)*z[12];
    z[23]=n<T>(1,3)*z[37] + z[23];
    z[23]=z[14]*z[23];
    z[37]=3*z[12];
    z[47]=z[37] - 25;
    z[23]=n<T>(1,2)*z[47] + z[23];
    z[48]=z[12] + 1;
    z[52]=z[48]*z[14];
    z[53]=static_cast<T>(1)- z[52];
    z[53]=z[39]*z[12]*z[53];
    z[23]=n<T>(1,16)*z[23] + z[53];
    z[23]=z[9]*z[23];
    z[53]=7*z[12];
    z[54]= - z[53] + static_cast<T>(25)+ 7*z[15];
    z[56]=static_cast<T>(1)- z[19];
    z[56]=z[14]*z[56];
    z[54]=z[56] + n<T>(1,16)*z[54] + z[6];
    z[54]=z[54]*z[27];
    z[23]=z[54] + z[23];
    z[23]=z[9]*z[23];
    z[54]= - z[55] + z[27];
    z[54]=z[54]*z[27];
    z[23]=z[54] + z[23];
    z[23]=z[9]*z[23];
    z[47]= - z[15] + z[47];
    z[21]=n<T>(1,8)*z[47] + z[21];
    z[47]=n<T>(1,4)*z[14];
    z[21]=z[21]*z[47];
    z[54]=4*z[12];
    z[56]=npow(z[12],2);
    z[57]= - z[14]*z[56];
    z[57]=z[54] + z[57];
    z[57]=z[14]*z[57]*z[36];
    z[21]=z[21] + z[57];
    z[21]=z[9]*z[21];
    z[57]= - z[6] - z[27];
    z[57]=z[57]*z[47];
    z[21]=z[57] + z[21];
    z[21]=z[9]*z[21];
    z[57]=z[41]*z[47];
    z[21]=z[57] + z[21];
    z[21]=z[9]*z[21];
    z[57]=z[41]*npow(z[14],2);
    z[21]=n<T>(1,8)*z[57] + z[21];
    z[21]=z[5]*z[21];
    z[57]=z[14]*z[6];
    z[59]=n<T>(1,2) - z[6];
    z[59]=z[59]*z[57];
    z[59]=z[58] + z[59];
    z[27]=z[59]*z[27];
    z[21]=z[21] + z[27] + z[23];
    z[21]=z[5]*z[21];
    z[23]=static_cast<T>(83)- n<T>(51,2)*z[12];
    z[23]=z[12]*z[23];
    z[23]= - n<T>(149,6) + z[23];
    z[23]=z[23]*z[47];
    z[27]= - static_cast<T>(193)+ 245*z[12];
    z[23]=z[23] + n<T>(1,8)*z[27] - z[6];
    z[23]=z[14]*z[23];
    z[27]=107*z[12];
    z[47]=n<T>(437,6) + z[27];
    z[47]=z[12]*z[47];
    z[47]= - n<T>(149,3) + z[47];
    z[47]=z[14]*z[47];
    z[27]=z[47] + n<T>(205,6) - z[27];
    z[27]=z[27]*z[17];
    z[47]=n<T>(15,2) - z[12];
    z[23]=z[27] + n<T>(3,8)*z[47] + z[23];
    z[23]=z[23]*z[17];
    z[27]=z[53] - static_cast<T>(13)- 27*z[15];
    z[27]= - n<T>(3,4)*z[14] + n<T>(1,64)*z[27] + z[44];
    z[27]=z[14]*z[27];
    z[23]=z[23] + n<T>(1,64) + z[27];
    z[23]=z[9]*z[23];
    z[26]=z[26]*z[57];
    z[27]=static_cast<T>(1)- 3*z[6];
    z[27]=z[6]*z[27];
    z[26]=z[27] + 3*z[26];
    z[26]=z[14]*z[26];
    z[26]=z[26] - z[13] + z[58];
    z[27]= - z[12]*z[35];
    z[27]= - n<T>(1,4) + z[27];
    z[27]=z[11]*z[27];
    z[27]=n<T>(1,4) + z[27];
    z[27]=z[11]*z[27];
    z[21]=z[21] + z[23] + n<T>(1,4)*z[26] + z[27];
    z[21]=z[5]*z[21];
    z[23]=n<T>(3,2) - z[6];
    z[23]=z[23]*z[57];
    z[26]=static_cast<T>(1)+ n<T>(5,4)*z[15];
    z[23]=z[23] + n<T>(3,8)*z[26] + z[55];
    z[23]=z[14]*z[23];
    z[22]=z[22]*z[41];
    z[26]=z[12] - 1;
    z[27]= - z[13]*z[26];
    z[22]=z[22] + z[27];
    z[22]=n<T>(1,2)*z[22] + z[23];
    z[23]=51*z[12];
    z[27]=static_cast<T>(61)- z[23];
    z[27]=z[27]*z[28];
    z[28]=5*z[12];
    z[35]=n<T>(19,8) - z[28];
    z[27]=z[27] + n<T>(1,2)*z[35] - z[40];
    z[27]=z[14]*z[27];
    z[35]= - n<T>(7,2) - z[23];
    z[35]=z[12]*z[35];
    z[35]=n<T>(157,2) + z[35];
    z[35]=z[14]*z[35];
    z[23]=z[35] - n<T>(95,2) + z[23];
    z[23]=z[23]*z[33];
    z[23]=z[23] - n<T>(139,96) + z[27];
    z[23]=z[23]*z[34];
    z[27]=static_cast<T>(1)- z[56];
    z[27]=z[27]*z[50];
    z[35]=static_cast<T>(1)+ z[37];
    z[35]=z[12]*z[35];
    z[27]=z[27] - static_cast<T>(6)+ z[35];
    z[27]=z[11]*z[27];
    z[35]=static_cast<T>(11)- z[28];
    z[27]=n<T>(1,4)*z[35] + z[27];
    z[27]=z[11]*z[27];
    z[27]=n<T>(1,4)*z[48] + z[27];
    z[27]=z[11]*z[27];
    z[21]=z[21] + z[23] + n<T>(1,2)*z[22] + z[27];
    z[21]=z[5]*z[21];
    z[22]=n<T>(73,3) + n<T>(3,4)*z[12];
    z[22]=z[22]*z[34];
    z[22]=z[22] + n<T>(11,8) + z[3];
    z[22]=z[9]*z[22];
    z[22]=z[22] + z[46];
    z[22]=z[22]*z[17];
    z[23]= - n<T>(9,16) - z[20];
    z[23]=n<T>(1,2)*z[23] - z[36];
    z[23]=z[9]*z[23];
    z[23]= - z[24] + z[23];
    z[23]=z[9]*z[23];
    z[23]= - z[29] + z[23];
    z[23]=z[23]*z[30];
    z[24]= - n<T>(3,4) - z[39];
    z[24]=z[24]*z[31];
    z[22]=z[23] + z[22] + z[24];
    z[22]=z[2]*z[22];
    z[23]=n<T>(1,16)*z[38];
    z[24]=static_cast<T>(43)+ n<T>(3,2)*z[12];
    z[24]=z[9]*z[24];
    z[24]=n<T>(5,2) + z[24];
    z[24]=z[24]*z[23];
    z[24]= - z[43] + z[24];
    z[27]= - n<T>(9,16) - z[32];
    z[27]=z[27]*z[31];
    z[24]=3*z[24] + z[27];
    z[24]=z[5]*z[24];
    z[27]=2*z[14];
    z[29]= - z[27] - z[51];
    z[29]=z[11]*z[29];
    z[30]= - static_cast<T>(2)+ z[14];
    z[29]=2*z[30] + z[29];
    z[29]=z[29]*z[45];
    z[30]= - n<T>(211,2) - z[37];
    z[30]=z[30]*z[33];
    z[20]=z[30] - n<T>(1,4) + z[20];
    z[17]=z[20]*z[17];
    z[17]=z[22] + z[24] + z[29] + z[17];
    z[17]=z[2]*z[17];
    z[20]=static_cast<T>(2)+ z[37];
    z[20]=z[20]*z[50];
    z[20]=z[20] - static_cast<T>(14)- 9*z[12];
    z[20]=z[11]*z[20];
    z[20]=n<T>(17,2) + z[20];
    z[20]=z[11]*z[20];
    z[20]= - n<T>(1,2) + z[20];
    z[20]=z[11]*z[20];
    z[22]= - n<T>(109,2) - z[37];
    z[22]=z[9]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[49];
    z[24]=static_cast<T>(13)+ n<T>(9,2)*z[12];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(3,2) + z[24];
    z[23]=z[24]*z[23];
    z[23]=z[42] + z[23];
    z[23]=z[5]*z[23];
    z[20]=z[23] + z[22] + n<T>(1,2)*z[13] + z[20];
    z[20]=z[5]*z[20];
    z[22]= - z[26]*z[50];
    z[22]=z[22] + 4*z[52] - static_cast<T>(7)+ z[54];
    z[22]=z[11]*z[22];
    z[22]=z[22] - 6*z[52] + static_cast<T>(5)- z[12];
    z[22]=z[11]*z[22];
    z[23]=2*z[12];
    z[24]=n<T>(17,8) + z[23];
    z[24]=z[14]*z[24];
    z[22]=z[22] + z[24] - static_cast<T>(1)- n<T>(1,8)*z[3];
    z[22]=z[11]*z[22];
    z[24]= - n<T>(1,2) + z[3];
    z[24]=z[3]*z[24];
    z[24]=n<T>(5,2) + z[24];
    z[24]=z[24]*z[25];
    z[25]=z[14]*z[15];
    z[24]= - n<T>(5,16)*z[25] - z[13] + z[24];
    z[17]=z[17] + z[20] + n<T>(245,192)*z[9] + n<T>(1,4)*z[24] + z[22];
    z[17]=z[2]*z[17];
    z[20]=z[50] - z[27];
    z[22]=z[12] + 2;
    z[22]=z[22]*z[12];
    z[22]=z[22] + 1;
    z[20]=z[22]*z[20];
    z[24]= - static_cast<T>(12)- z[28];
    z[24]=z[12]*z[24];
    z[20]= - static_cast<T>(7)+ z[24] + z[20];
    z[20]=z[11]*z[20];
    z[24]=n<T>(25,4) + z[23];
    z[24]=z[12]*z[24];
    z[22]=z[14]*z[22];
    z[20]=z[20] + 4*z[22] + n<T>(23,4) + z[24];
    z[20]=z[11]*z[20];
    z[22]= - n<T>(17,4) - z[23];
    z[22]=z[12]*z[22];
    z[22]= - n<T>(9,4) + z[22];
    z[22]=z[14]*z[22];
    z[19]=z[20] + z[22] - static_cast<T>(2)- z[19];
    z[19]=z[11]*z[19];
    z[20]= - z[12]*z[48];
    z[20]= - n<T>(31,4) + z[20];
    z[20]=z[14]*z[20];
    z[20]=z[20] - n<T>(1,4)*z[8] + z[12];
    z[20]=n<T>(1,8)*z[20] + z[11];
    z[20]=z[9]*z[20];

    r += z[16] + z[17] + n<T>(1,2)*z[18] + z[19] + z[20] + z[21];
 
    return r;
}

template double qg_2lha_r261(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r261(const std::array<dd_real,30>&);
#endif
