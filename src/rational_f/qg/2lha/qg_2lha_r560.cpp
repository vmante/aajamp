#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r560(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[28];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[3];
    z[9]=k[5];
    z[10]=k[21];
    z[11]=k[10];
    z[12]=k[9];
    z[13]=k[11];
    z[14]=static_cast<T>(1)- z[4];
    z[14]=z[12]*z[14];
    z[15]= - 3*z[12] + static_cast<T>(3)+ n<T>(5,2)*z[6];
    z[15]=z[10]*z[15];
    z[14]=z[14] + z[15];
    z[15]=n<T>(1,2)*z[6];
    z[16]= - z[13] - n<T>(1,2)*z[11];
    z[16]=z[16]*z[15];
    z[17]=z[1] - 1;
    z[18]=z[17] + z[12];
    z[19]=3*z[5];
    z[20]= - z[18]*z[19];
    z[21]= - static_cast<T>(1)+ n<T>(3,2)*z[1];
    z[20]=n<T>(1,2)*z[21] + z[20];
    z[20]=z[5]*z[20];
    z[18]=z[18]*z[5];
    z[21]=z[17]*z[4];
    z[21]=z[18] - z[21];
    z[22]=z[12]*z[4];
    z[23]=z[21] - z[22];
    z[24]=n<T>(1,2)*z[3];
    z[25]=z[23]*z[24];
    z[26]=static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[26]=z[4]*z[26];
    z[14]=z[25] + z[20] + z[26] + static_cast<T>(1)+ z[16] + n<T>(1,2)*z[14];
    z[14]=z[3]*z[14];
    z[16]=z[6] - 1;
    z[20]=z[12] + z[16];
    z[20]=z[20]*z[10];
    z[25]=z[4] - z[11];
    z[25]=z[25]*z[12];
    z[16]= - z[11]*z[16];
    z[16]=z[20] + z[25] + z[16] - z[21];
    z[21]=n<T>(1,4)*z[3];
    z[16]=z[16]*z[21];
    z[25]=z[25] - n<T>(3,2)*z[4] + z[13] + z[11];
    z[20]= - n<T>(3,2)*z[20] - n<T>(11,4) + z[12];
    z[20]=z[10]*z[20];
    z[18]=n<T>(3,2)*z[18];
    z[26]=z[18] + n<T>(5,4) - z[12];
    z[26]=z[5]*z[26];
    z[16]=z[16] + z[26] + n<T>(1,2)*z[25] + z[20];
    z[16]=z[3]*z[16];
    z[20]=n<T>(1,4)*z[9];
    z[25]=n<T>(1,4)*z[4];
    z[26]= - z[25] - z[20];
    z[27]=z[8] + 1;
    z[26]=z[27]*z[26];
    z[27]=z[4] + z[9];
    z[28]=n<T>(1,4)*z[12];
    z[29]=z[27]*z[28];
    z[26]=z[29] + static_cast<T>(1)+ z[26];
    z[29]=3*z[10];
    z[26]=z[26]*z[29];
    z[27]= - z[12]*z[27];
    z[27]=z[27] + z[9] + n<T>(5,2)*z[4];
    z[26]=n<T>(1,2)*z[27] + z[26];
    z[26]=z[10]*z[26];
    z[27]= - z[28] - n<T>(1,4)*z[6];
    z[30]=z[9] + z[11];
    z[27]=z[30]*z[27];
    z[31]=n<T>(1,4)*z[11];
    z[20]=z[20] - static_cast<T>(1)+ z[31] + z[27];
    z[20]=z[20]*z[19];
    z[27]=n<T>(1,2)*z[9];
    z[32]=z[30]*z[12];
    z[32]=z[32] - z[11] + z[27];
    z[20]=n<T>(1,2)*z[32] + z[20];
    z[20]=z[5]*z[20];
    z[16]=z[16] + z[26] + z[20];
    z[16]=z[2]*z[16];
    z[20]=z[9]*z[8];
    z[26]= - static_cast<T>(3)+ n<T>(5,2)*z[8];
    z[26]=z[4]*z[26];
    z[20]=3*z[22] + z[26] - n<T>(21,2) + z[20];
    z[22]=npow(z[8],2);
    z[26]= - z[22]*z[27];
    z[27]=3*z[8];
    z[26]=z[27] + z[26];
    z[32]= - z[22]*z[25];
    z[26]=z[32] + n<T>(1,2)*z[26] - z[6];
    z[26]=z[26]*z[29];
    z[20]=n<T>(1,2)*z[20] + z[26];
    z[20]=z[10]*z[20];
    z[26]=z[30]*z[15];
    z[26]=static_cast<T>(1)+ z[26];
    z[15]=z[26]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[19];
    z[19]= - z[31] - z[9];
    z[19]=z[6]*z[19];
    z[15]=z[15] - n<T>(5,4) + z[19];
    z[15]=z[5]*z[15];
    z[19]= - 3*z[4] + 7*z[13] + z[9];
    z[14]=z[16] + z[14] + z[15] + n<T>(1,4)*z[19] + z[20];
    z[14]=z[2]*z[14];
    z[15]=z[1]*z[7];
    z[16]= - static_cast<T>(1)- z[15];
    z[16]=z[6]*z[16];
    z[19]=z[1] + 1;
    z[20]=z[4]*z[19];
    z[16]= - n<T>(1,2)*z[20] + n<T>(3,2)*z[17] + z[16];
    z[18]=z[18] - n<T>(3,4)*z[19] + z[12];
    z[18]=z[5]*z[18];
    z[19]= - z[23]*z[21];
    z[20]=n<T>(1,4) + z[6];
    z[20]=z[12]*z[20];
    z[16]=z[19] + z[18] + n<T>(1,2)*z[16] + z[20];
    z[16]=z[16]*z[24];
    z[18]=z[27]*z[13];
    z[19]= - static_cast<T>(13)- z[18];
    z[19]=z[8]*z[19];
    z[18]=static_cast<T>(7)+ z[18];
    z[18]=z[6]*z[18];
    z[20]= - z[4]*z[27];
    z[18]=z[20] + z[18] + static_cast<T>(3)+ z[19];
    z[19]= - z[6]*z[8];
    z[19]=z[22] + z[19];
    z[19]=z[19]*z[29];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[10]*z[18];
    z[19]= - z[6] + z[4];
    z[19]=z[19]*z[28];
    z[20]=z[6]*z[7];
    z[15]=z[20] - static_cast<T>(1)+ z[15];
    z[15]=z[6]*z[15];
    z[15]=z[15] - z[17];
    z[17]= - z[5]*z[12];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[5]*z[15];
    z[17]=z[8]*z[13];
    z[20]= - z[7] - 5*z[13];
    z[20]=z[6]*z[20];

    r += n<T>(1,2) + n<T>(1,2)*z[14] + n<T>(3,4)*z[15] + z[16] + z[17] + n<T>(1,4)*
      z[18] + z[19] + n<T>(1,8)*z[20] + z[25];
 
    return r;
}

template double qg_2lha_r560(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r560(const std::array<dd_real,30>&);
#endif
