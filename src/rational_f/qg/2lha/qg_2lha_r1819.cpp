#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1819(const std::array<T,30>& k) {
  T z[48];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[14];
    z[5]=k[17];
    z[6]=k[9];
    z[7]=k[3];
    z[8]=k[6];
    z[9]=k[15];
    z[10]=k[12];
    z[11]=k[11];
    z[12]=k[13];
    z[13]=k[7];
    z[14]=z[1] - 5;
    z[15]=n<T>(3,2)*z[4];
    z[16]= - z[14]*z[15];
    z[17]=n<T>(1,2)*z[1];
    z[18]= - static_cast<T>(1)- z[17];
    z[18]=z[5]*z[18];
    z[19]=z[12]*z[13];
    z[16]=z[16] + z[18] + n<T>(7,4)*z[19];
    z[18]=z[5] - n<T>(7,3);
    z[19]=n<T>(1,3)*z[12];
    z[20]=z[19] + n<T>(1,4)*z[18];
    z[20]=z[20]*z[8];
    z[21]=npow(z[10],2);
    z[22]= - z[4] + 3*z[21];
    z[23]=n<T>(3,4)*z[2];
    z[22]=z[22]*z[23];
    z[23]=3*z[10];
    z[24]=z[4] + z[11];
    z[25]= - static_cast<T>(1)+ z[24];
    z[25]=z[25]*z[23];
    z[25]=z[25] + 9*z[4] + static_cast<T>(19)- z[11];
    z[25]=z[10]*z[25];
    z[26]= - static_cast<T>(2)+ n<T>(3,4)*z[9];
    z[27]= - n<T>(7,8)*z[13] + z[26];
    z[27]=z[9]*z[27];
    z[16]= - z[22] + n<T>(1,4)*z[25] - z[20] + n<T>(1,2)*z[16] + z[27];
    z[16]=z[2]*z[16];
    z[25]=n<T>(3,4)*z[4];
    z[14]= - z[14]*z[25];
    z[27]=n<T>(2,3)*z[12];
    z[18]= - n<T>(1,2)*z[18] - z[27];
    z[18]=z[8]*z[18];
    z[28]=7*z[4] - n<T>(7,2) - 3*z[11];
    z[29]=n<T>(3,2)*z[11] + z[15] - 1;
    z[30]=z[10]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[30];
    z[30]=n<T>(3,2)*z[10];
    z[28]=z[28]*z[30];
    z[30]=n<T>(1,2)*z[5];
    z[14]=z[16] + z[28] + z[18] - n<T>(21,8)*z[9] + z[14] + n<T>(1,4)*z[11] + 29.
   /12.*z[12] - z[30] - static_cast<T>(4)+ n<T>(3,4)*z[1];
    z[14]=z[2]*z[14];
    z[16]=z[8] - 1;
    z[18]=n<T>(1,2)*z[8];
    z[16]=z[16]*z[18];
    z[28]=z[9] + n<T>(1,2);
    z[28]=z[28]*z[9];
    z[16]=z[16] - z[28];
    z[31]=z[16]*z[2];
    z[32]= - static_cast<T>(1)+ z[18];
    z[32]=z[32]*z[18];
    z[33]=static_cast<T>(1)- z[9];
    z[33]=z[10]*z[33];
    z[33]= - z[28] + z[33];
    z[34]=n<T>(1,2)*z[10];
    z[33]=z[33]*z[34];
    z[32]=n<T>(1,2)*z[31] + z[33] - z[28] + z[32];
    z[33]=n<T>(3,2)*z[7];
    z[32]=z[32]*z[33];
    z[35]=9*z[9];
    z[36]=static_cast<T>(5)- z[35];
    z[36]=z[9]*z[36];
    z[31]=3*z[31] + 15*z[21] + z[36] + 5*z[8];
    z[31]=z[2]*z[31];
    z[36]=3*z[8];
    z[37]=static_cast<T>(19)- z[36];
    z[37]=z[37]*z[18];
    z[38]=3*z[9];
    z[39]=n<T>(23,2) - z[38];
    z[39]=z[9]*z[39];
    z[31]=z[31] + z[39] + z[37];
    z[37]=z[38] + 5;
    z[39]=n<T>(1,2)*z[9];
    z[40]=z[37]*z[39];
    z[41]= - static_cast<T>(1)+ z[40];
    z[42]=n<T>(1,4)*z[9];
    z[43]=static_cast<T>(1)+ z[42];
    z[43]=z[43]*z[23];
    z[41]=n<T>(1,2)*z[41] + z[43];
    z[41]=z[10]*z[41];
    z[31]=z[32] + z[41] + n<T>(1,4)*z[31];
    z[31]=z[7]*z[31];
    z[32]=n<T>(1,3)*z[8];
    z[41]= - static_cast<T>(5)- z[32];
    z[41]=z[41]*z[18];
    z[43]=static_cast<T>(1)+ n<T>(9,2)*z[9];
    z[43]=z[9]*z[43];
    z[44]= - n<T>(21,2)*z[10] - static_cast<T>(5)- n<T>(7,4)*z[9];
    z[44]=z[10]*z[44];
    z[41]=z[44] + z[43] + z[41];
    z[26]= - z[9]*z[26];
    z[43]=static_cast<T>(2)- n<T>(11,24)*z[8];
    z[43]=z[8]*z[43];
    z[26]=n<T>(21,4)*z[21] + z[26] + z[43];
    z[26]=z[2]*z[26];
    z[43]=static_cast<T>(3)+ z[39];
    z[43]=z[43]*z[38];
    z[44]=n<T>(13,3)*z[8];
    z[45]=static_cast<T>(9)- z[44];
    z[45]=z[8]*z[45];
    z[43]= - n<T>(45,4)*z[10] + z[43] + n<T>(1,4)*z[45];
    z[26]=n<T>(1,2)*z[43] + z[26];
    z[26]=z[2]*z[26];
    z[26]=z[31] + n<T>(1,2)*z[41] + z[26];
    z[26]=z[7]*z[26];
    z[31]=z[37]*z[42];
    z[37]= - n<T>(19,2) - 9*z[10];
    z[37]=z[37]*z[34];
    z[41]=z[21]*z[2];
    z[41]=n<T>(9,4)*z[41];
    z[31]=z[41] + z[37] + z[31] - z[32];
    z[31]=z[2]*z[31];
    z[32]= - static_cast<T>(11)+ z[35];
    z[32]=z[9]*z[32];
    z[35]=static_cast<T>(5)- z[23];
    z[35]=z[10]*z[35];
    z[32]=5*z[35] + z[36] + static_cast<T>(9)+ z[32];
    z[31]=n<T>(1,4)*z[32] + z[31];
    z[31]=z[2]*z[31];
    z[32]=3*z[6];
    z[35]=z[32] - z[11];
    z[37]=z[44] - n<T>(21,2)*z[9] - static_cast<T>(3)+ n<T>(29,6)*z[12] + z[35];
    z[42]=n<T>(7,2) + z[23];
    z[42]=z[10]*z[42];
    z[26]=z[26] + z[31] + n<T>(1,4)*z[37] + z[42];
    z[26]=z[7]*z[26];
    z[31]=3*z[4];
    z[37]= - static_cast<T>(1)- 11*z[11];
    z[37]=n<T>(1,2)*z[37] + z[31];
    z[42]=n<T>(1,2)*z[4];
    z[43]=z[42] - 1;
    z[44]=n<T>(1,2)*z[11] + z[43];
    z[44]=z[44]*z[23];
    z[37]=n<T>(1,2)*z[37] + z[44];
    z[37]=z[10]*z[37];
    z[44]=z[1] - 1;
    z[45]=z[44]*z[42];
    z[17]=z[17] - 1;
    z[45]=z[45] - z[17];
    z[46]=n<T>(3,2)*z[6];
    z[45]=z[45]*z[46];
    z[44]=z[44]*z[25];
    z[47]=n<T>(5,4)*z[11];
    z[14]=z[26] + z[14] + z[37] - z[20] - z[45] + z[44] + z[47] - n<T>(3,2)*
    z[17] + z[19];
    z[14]=z[3]*z[14];
    z[20]= - static_cast<T>(1)- z[1];
    z[20]=z[20]*z[30];
    z[17]= - z[17]*z[31];
    z[17]= - 5*z[9] + z[17] + n<T>(15,4)*z[12] + static_cast<T>(3)+ z[20];
    z[20]=n<T>(5,4) - z[5];
    z[20]=n<T>(1,2)*z[20] - z[27];
    z[20]=z[8]*z[20];
    z[15]=z[47] - z[15];
    z[24]=z[24]*z[10];
    z[26]=n<T>(3,2)*z[24] + static_cast<T>(2)- z[15];
    z[26]=z[10]*z[26];
    z[17]= - z[22] + z[26] + n<T>(1,2)*z[17] + z[20];
    z[17]=z[2]*z[17];
    z[20]=z[10]*z[9];
    z[20]=z[20] + z[28] + z[18];
    z[20]=z[10]*z[20];
    z[16]=z[20] - z[16];
    z[16]=z[16]*z[33];
    z[20]= - static_cast<T>(3)- z[9];
    z[20]=z[20]*z[23];
    z[22]= - static_cast<T>(11)- z[38];
    z[22]=z[9]*z[22];
    z[20]=z[20] + z[22] - n<T>(15,2)*z[8];
    z[20]=z[20]*z[34];
    z[22]= - n<T>(19,4) - z[38];
    z[22]=z[9]*z[22];
    z[26]= - n<T>(19,4) + z[36];
    z[26]=z[8]*z[26];
    z[16]=z[16] + z[20] + z[22] + z[26];
    z[16]=z[7]*z[16];
    z[20]=n<T>(17,2) + z[38];
    z[20]=z[9]*z[20];
    z[22]=n<T>(27,2) - n<T>(7,3)*z[8];
    z[22]=z[8]*z[22];
    z[26]=static_cast<T>(11)+ 15*z[10];
    z[28]=n<T>(27,2)*z[8] + n<T>(19,2)*z[9] + z[26];
    z[28]=z[10]*z[28];
    z[20]=z[28] + z[20] + z[22];
    z[22]=npow(z[8],2);
    z[28]=npow(z[9],2);
    z[21]=n<T>(3,2)*z[21] - 3*z[28] + n<T>(31,12)*z[22];
    z[21]=z[2]*z[21];
    z[16]=z[16] + n<T>(1,2)*z[20] + z[21];
    z[20]=n<T>(1,2)*z[7];
    z[16]=z[16]*z[20];
    z[21]=static_cast<T>(3)- n<T>(35,24)*z[8];
    z[21]=z[8]*z[21];
    z[28]= - static_cast<T>(2)- n<T>(3,4)*z[10];
    z[28]=z[10]*z[28];
    z[21]=z[41] + z[28] + z[40] + z[21];
    z[21]=z[2]*z[21];
    z[28]= - static_cast<T>(1)+ n<T>(5,2)*z[12];
    z[28]= - z[39] + 3*z[28] + z[35];
    z[35]= - static_cast<T>(17)- n<T>(21,2)*z[8];
    z[35]=n<T>(1,2)*z[35] - z[23];
    z[35]=z[10]*z[35];
    z[37]=static_cast<T>(3)- n<T>(17,12)*z[8];
    z[37]=z[8]*z[37];
    z[28]=z[35] + n<T>(1,2)*z[28] + z[37];
    z[16]=z[16] + n<T>(1,2)*z[28] + z[21];
    z[16]=z[7]*z[16];
    z[21]=z[36] - z[31] + static_cast<T>(13)- 15*z[11];
    z[28]=z[29]*z[23];
    z[21]=n<T>(1,2)*z[21] + z[28];
    z[21]=z[21]*z[34];
    z[28]=n<T>(11,6) - z[5];
    z[28]=n<T>(1,2)*z[28] - z[27];
    z[28]=z[8]*z[28];
    z[29]=static_cast<T>(5)+ z[5];
    z[14]=z[14] + z[16] + z[17] + z[21] + z[28] - z[45] + n<T>(1,4)*z[29] + 
    z[27] + z[15];
    z[14]=z[3]*z[14];
    z[15]=z[42]*z[13];
    z[16]= - z[6]*z[43];
    z[16]= - z[15] + z[16];
    z[17]=n<T>(1,3)*z[4];
    z[21]=z[17]*z[13];
    z[28]= - static_cast<T>(1)+ z[17];
    z[28]=z[6]*z[28];
    z[28]=z[21] + z[28];
    z[28]=z[28]*z[18];
    z[16]=n<T>(1,3)*z[16] + z[28];
    z[16]=z[2]*z[16];
    z[28]=n<T>(1,3)*z[6];
    z[29]= - n<T>(7,4) - z[4];
    z[29]=z[29]*z[28];
    z[31]=n<T>(1,2)*z[6];
    z[17]= - n<T>(1,4) + z[17];
    z[17]=z[17]*z[31];
    z[15]=z[15] - static_cast<T>(2)- n<T>(1,2)*z[13];
    z[15]=n<T>(1,3)*z[15] + z[17];
    z[15]=z[8]*z[15];
    z[17]=n<T>(13,2) + z[13];
    z[15]=z[16] + z[15] + z[29] - z[21] - z[19] + n<T>(1,3)*z[17] - n<T>(1,4)*
    z[5];
    z[15]=z[8]*z[15];
    z[16]=z[9]*z[6];
    z[17]=z[31] + z[16];
    z[17]=z[17]*z[9];
    z[21]= - z[6]*z[18];
    z[29]= - z[10]*z[16];
    z[21]=z[29] - z[17] + z[21];
    z[21]=z[10]*z[21];
    z[29]=z[8]*z[6];
    z[31]= - z[6] + z[29];
    z[31]=z[31]*z[18];
    z[17]=z[21] - z[17] + z[31];
    z[17]=z[17]*z[33];
    z[21]=z[32] + z[16];
    z[21]=z[21]*z[23];
    z[31]=z[38]*z[6];
    z[32]=11*z[6] + z[31];
    z[32]=z[9]*z[32];
    z[21]=z[21] + z[32] + n<T>(15,2)*z[29];
    z[21]=z[21]*z[34];
    z[32]=n<T>(19,4)*z[6];
    z[33]=z[32] + z[31];
    z[33]=z[9]*z[33];
    z[32]=z[32] - 3*z[29];
    z[32]=z[8]*z[32];
    z[17]=z[17] + z[21] + z[33] + z[32];
    z[17]=z[7]*z[17];
    z[21]= - z[6]*z[26];
    z[21]= - n<T>(27,2)*z[29] - n<T>(19,2)*z[16] + z[21];
    z[21]=z[10]*z[21];
    z[26]= - n<T>(29,2)*z[6] - z[31];
    z[26]=z[9]*z[26];
    z[31]= - n<T>(39,2)*z[6] + n<T>(25,3)*z[29];
    z[31]=z[8]*z[31];
    z[21]=z[21] + z[26] + z[31];
    z[22]= - z[2]*z[22]*z[28];
    z[17]=z[17] + n<T>(1,2)*z[21] + z[22];
    z[17]=z[17]*z[20];
    z[20]= - static_cast<T>(3)+ n<T>(91,6)*z[6];
    z[21]=n<T>(3,4) - z[6];
    z[21]=z[8]*z[21];
    z[20]=n<T>(1,4)*z[20] + z[21];
    z[20]=z[8]*z[20];
    z[21]=17*z[6] + n<T>(21,2)*z[29];
    z[22]=z[6]*z[23];
    z[21]=n<T>(1,2)*z[21] + z[22];
    z[21]=z[21]*z[34];
    z[22]= - z[28] + z[29];
    z[18]=z[2]*z[22]*z[18];
    z[16]=z[17] + z[18] + z[21] + z[20] + n<T>(13,8)*z[16] + z[27] + n<T>(3,4)*
    z[6];
    z[16]=z[7]*z[16];
    z[17]=z[24] - z[29];
    z[17]= - z[46] - z[11] - z[25] + n<T>(3,4)*z[17];
    z[17]=z[10]*z[17];
    z[18]= - static_cast<T>(5)- z[13];
    z[18]=n<T>(1,3)*z[18] + z[30];
    z[20]= - static_cast<T>(5)+ n<T>(11,4)*z[4];
    z[20]=z[20]*z[28];
    z[21]=z[4]*z[13];

    r += z[14] + z[15] + z[16] + z[17] + n<T>(1,2)*z[18] + z[19] + z[20] + 
      n<T>(1,6)*z[21];
 
    return r;
}

template double qg_2lha_r1819(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1819(const std::array<dd_real,30>&);
#endif
