#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1474(const std::array<T,30>& k) {
  T z[54];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[13];
    z[4]=k[12];
    z[5]=k[2];
    z[6]=k[7];
    z[7]=k[11];
    z[8]=k[24];
    z[9]=k[4];
    z[10]=k[3];
    z[11]=k[9];
    z[12]=k[5];
    z[13]=k[14];
    z[14]=k[6];
    z[15]=z[3] + 1;
    z[16]=n<T>(1,2)*z[3];
    z[17]=z[15]*z[16];
    z[18]=n<T>(1,2)*z[9];
    z[19]=npow(z[3],2);
    z[20]=z[18]*z[19];
    z[21]=z[3] + n<T>(1,4);
    z[21]=z[21]*z[3];
    z[22]=z[21] + z[20];
    z[22]=z[9]*z[22];
    z[17]=z[17] + z[22];
    z[17]=z[9]*z[17];
    z[22]=3*z[3];
    z[23]=z[22] + 1;
    z[23]=z[23]*z[3];
    z[24]=z[19]*z[9];
    z[23]=z[23] + z[24];
    z[23]=z[23]*z[9];
    z[25]=z[15]*z[22];
    z[23]=z[23] + z[25];
    z[23]=z[23]*z[9];
    z[25]=z[3] + 3;
    z[25]=z[25]*z[3];
    z[23]=z[23] + z[25];
    z[23]=z[23]*z[9];
    z[23]=z[23] + z[3];
    z[25]=n<T>(1,4)*z[8];
    z[26]= - z[23]*z[25];
    z[27]=n<T>(1,4)*z[3];
    z[17]=z[26] + z[27] + z[17];
    z[17]=z[8]*z[17];
    z[26]=n<T>(1,2)*z[19];
    z[28]=z[26]*z[7];
    z[28]=z[28] - z[19];
    z[29]=z[7]*z[28];
    z[29]=z[29] - z[20];
    z[29]=z[9]*z[29];
    z[30]=n<T>(1,2)*z[7];
    z[31]=z[7]*z[3];
    z[32]= - z[3] + z[31];
    z[32]=z[32]*z[30];
    z[29]=z[32] + z[29];
    z[17]=n<T>(1,2)*z[29] + z[17];
    z[17]=z[4]*z[17];
    z[29]=n<T>(7,2)*z[3];
    z[32]= - static_cast<T>(1)+ z[29];
    z[32]=z[32]*z[16];
    z[33]=z[3] - n<T>(3,2);
    z[34]= - z[33]*z[16];
    z[34]=z[34] - z[31];
    z[34]=z[7]*z[34];
    z[32]=z[32] + z[34];
    z[32]=z[7]*z[32];
    z[34]=z[3] - n<T>(1,2);
    z[35]= - z[3]*z[34];
    z[17]=z[17] + z[35] + z[32];
    z[32]=n<T>(1,2)*z[8];
    z[23]=z[23]*z[32];
    z[21]=z[21] + n<T>(1,4)*z[24];
    z[21]=z[21]*z[9];
    z[35]=n<T>(5,2)*z[3];
    z[36]=z[35] + 1;
    z[36]=z[36]*z[16];
    z[21]=z[21] + z[36];
    z[21]=z[21]*z[9];
    z[36]=z[3] + n<T>(1,2);
    z[36]=z[36]*z[16];
    z[21]=z[23] - z[21] - z[36];
    z[37]=z[8]*z[21];
    z[24]=n<T>(3,2)*z[24];
    z[38]= - z[19] - z[24];
    z[38]=z[9]*z[38];
    z[38]=z[26] + z[38];
    z[37]=n<T>(1,2)*z[38] + z[37];
    z[37]=z[37]*z[32];
    z[38]=z[19]*z[7];
    z[39]=n<T>(5,2)*z[19] - z[38];
    z[39]=z[39]*z[30];
    z[39]= - z[19] + z[39];
    z[39]=z[7]*z[39];
    z[39]=z[26] + z[39];
    z[39]=z[9]*z[39];
    z[17]=z[37] + z[39] + n<T>(1,2)*z[17];
    z[17]=z[4]*z[17];
    z[37]=npow(z[7],2);
    z[39]= - z[19] + 7*z[37];
    z[39]=z[39]*z[18];
    z[40]=static_cast<T>(1)- z[22];
    z[40]=z[3]*z[40];
    z[41]=static_cast<T>(1)+ 3*z[7];
    z[42]=z[7]*z[41];
    z[39]=z[39] + z[40] + z[42];
    z[40]=n<T>(1,4)*z[11];
    z[39]=z[39]*z[40];
    z[42]=n<T>(13,6)*z[7] - n<T>(9,8) + z[3];
    z[42]=z[7]*z[42];
    z[36]= - z[36] + z[42];
    z[36]=z[7]*z[36];
    z[42]=z[19] - z[37];
    z[43]=npow(z[7],3);
    z[44]=z[43]*z[9];
    z[42]=n<T>(1,2)*z[42] + z[44];
    z[42]=z[11]*z[42];
    z[42]= - z[43] + z[42];
    z[42]=z[10]*z[42];
    z[45]=n<T>(7,6)*z[44];
    z[36]=n<T>(1,2)*z[42] + z[39] - z[45] + n<T>(5,8)*z[19] + z[36];
    z[36]=z[11]*z[36];
    z[39]=n<T>(1,2)*z[43];
    z[42]=n<T>(1,3)*z[19];
    z[46]=npow(z[8],2)*z[42];
    z[36]= - z[39] + z[46] + z[36];
    z[36]=z[10]*z[36];
    z[46]=z[3] - 1;
    z[47]=z[3]*z[46];
    z[47]=z[47] - z[31];
    z[47]=z[7]*z[47];
    z[47]= - z[26] + z[47];
    z[47]=z[7]*z[47];
    z[48]= - static_cast<T>(3)+ z[3];
    z[48]=z[48]*z[31];
    z[49]=z[16] - 1;
    z[50]=z[49]*z[3];
    z[48]=z[48] - n<T>(1,2) - z[50];
    z[48]=z[48]*z[37];
    z[48]=z[48] - z[44];
    z[48]=z[9]*z[48];
    z[47]=z[47] + z[48];
    z[48]=z[7] + 1;
    z[51]=z[7]*z[48];
    z[51]=n<T>(1,2) + z[51];
    z[51]=z[51]*z[30];
    z[41]=z[41]*z[37];
    z[41]=z[41] + z[44];
    z[41]=z[41]*z[18];
    z[52]=static_cast<T>(1)+ n<T>(3,2)*z[7];
    z[52]=z[7]*z[52];
    z[52]=n<T>(1,4) + z[52];
    z[52]=z[7]*z[52];
    z[41]=z[52] + z[41];
    z[41]=z[9]*z[41];
    z[41]=z[51] + z[41];
    z[41]=z[11]*z[41];
    z[41]=n<T>(1,2)*z[47] + z[41];
    z[41]=z[14]*z[41];
    z[47]=n<T>(1,3)*z[3];
    z[51]=static_cast<T>(1)+ n<T>(5,4)*z[3];
    z[51]=z[51]*z[47];
    z[52]=n<T>(1,3)*z[7];
    z[53]=static_cast<T>(5)- n<T>(17,2)*z[3];
    z[53]=z[53]*z[52];
    z[51]=z[53] - n<T>(9,8) + z[51];
    z[51]=z[7]*z[51];
    z[51]=n<T>(17,12)*z[19] + z[51];
    z[51]=z[7]*z[51];
    z[17]=z[17] - z[45] - n<T>(3,8)*z[19] + z[51] + z[36] + z[41];
    z[21]= - z[21]*z[32];
    z[36]=z[46]*z[31];
    z[41]= - z[19] + z[20];
    z[41]=z[9]*z[41];
    z[21]=z[21] + n<T>(3,4)*z[41] - n<T>(17,8)*z[19] + z[36];
    z[21]=z[8]*z[21];
    z[36]=n<T>(13,24) - z[7];
    z[36]=z[36]*z[37];
    z[18]= - z[43]*z[18];
    z[18]=z[18] + n<T>(1,8)*z[19] + z[36];
    z[18]=z[9]*z[18];
    z[36]= - npow(z[9],2);
    z[36]=static_cast<T>(3)+ z[36];
    z[20]=z[36]*z[20];
    z[20]= - z[23] + z[19] + z[20];
    z[20]=z[8]*z[20];
    z[22]=z[34]*z[22];
    z[20]=z[20] + z[24] + z[22] + n<T>(5,2)*z[7];
    z[20]=z[20]*z[40];
    z[22]=z[3]*z[33];
    z[23]= - n<T>(19,12) - z[3];
    z[23]=z[7]*z[23];
    z[22]=z[22] + z[23];
    z[22]=z[7]*z[22];
    z[23]=static_cast<T>(1)- n<T>(3,2)*z[3];
    z[23]=z[3]*z[23];
    z[22]=z[23] + z[22];
    z[18]=z[20] + z[21] + n<T>(1,2)*z[22] + z[18];
    z[18]=z[18]*z[40];
    z[15]=z[15]*z[3];
    z[20]=z[3]*z[37];
    z[20]= - z[15] + z[20];
    z[21]=z[9]*z[7];
    z[22]=z[28]*z[21];
    z[23]= - z[10]*z[26];
    z[20]=z[23] + n<T>(1,2)*z[20] + z[22];
    z[20]=z[13]*z[20];
    z[22]= - n<T>(5,4)*z[19] + z[38];
    z[22]=z[7]*z[22];
    z[22]= - z[26] + z[22];
    z[22]=z[22]*z[21];
    z[20]=z[22] + z[20];
    z[22]= - z[46]*z[16];
    z[23]=z[49]*z[31];
    z[22]=z[22] + z[23];
    z[22]=z[11]*z[22];
    z[23]= - static_cast<T>(5)- z[47];
    z[23]=z[23]*z[27];
    z[24]=n<T>(5,6) - z[3];
    z[24]=z[24]*z[16];
    z[24]=z[24] + n<T>(5,3)*z[31];
    z[24]=z[7]*z[24];
    z[24]=n<T>(1,6)*z[19] + z[24];
    z[24]=z[7]*z[24];
    z[26]=z[19]*z[10];
    z[20]= - n<T>(5,4)*z[26] + z[22] + z[23] + z[24] + n<T>(5,3)*z[20];
    z[20]=z[13]*z[20];
    z[22]=n<T>(1,2) - z[52];
    z[22]=z[22]*z[37];
    z[23]=z[7] - 1;
    z[23]=z[23]*z[7];
    z[24]=z[37]*z[9];
    z[27]=z[23] - z[24];
    z[28]=z[8]*z[27];
    z[22]=n<T>(1,12)*z[28] + z[22] + n<T>(1,3)*z[44];
    z[22]=z[8]*z[22];
    z[28]=z[48]*z[37];
    z[28]=z[28] - z[44];
    z[27]=z[6]*z[27];
    z[27]=n<T>(1,2)*z[28] + z[27];
    z[27]=z[6]*z[27];
    z[25]=z[37]*z[25];
    z[25]= - z[43] + z[25];
    z[25]=z[8]*z[25];
    z[28]=z[6]*z[37];
    z[28]=z[39] + z[28];
    z[28]=z[6]*z[28];
    z[25]=z[25] + n<T>(1,2)*z[28];
    z[25]=z[5]*z[25];
    z[22]=n<T>(1,3)*z[25] + z[22] + n<T>(1,6)*z[27];
    z[22]=z[5]*z[22];
    z[25]=z[47] - 1;
    z[27]=z[25]*z[16];
    z[27]=z[27] + n<T>(1,3);
    z[28]=z[27]*z[7];
    z[33]=z[49]*z[47];
    z[34]=z[33] - z[28];
    z[34]=z[34]*z[21];
    z[36]=z[46]*z[7];
    z[37]= - n<T>(1,6)*z[36] - n<T>(1,3) + z[16];
    z[37]=z[7]*z[37];
    z[34]=z[34] - n<T>(1,6)*z[15] + z[37];
    z[32]=z[34]*z[32];
    z[34]= - n<T>(5,3) - z[16];
    z[16]=z[34]*z[16];
    z[34]=n<T>(1,3)*z[36];
    z[16]=z[34] + n<T>(2,3) + z[16];
    z[16]=z[7]*z[16];
    z[37]=n<T>(7,4) + 2*z[3];
    z[37]=z[3]*z[37];
    z[37]= - n<T>(1,2) + z[37];
    z[16]=n<T>(1,3)*z[37] + z[16];
    z[16]=z[7]*z[16];
    z[37]= - static_cast<T>(1)- 7*z[50];
    z[25]=z[3]*z[25];
    z[25]=n<T>(2,3) + z[25];
    z[25]=z[7]*z[25];
    z[25]=n<T>(1,6)*z[37] + z[25];
    z[25]=z[25]*z[24];
    z[16]=z[32] + z[25] - z[42] + z[16];
    z[16]=z[8]*z[16];
    z[25]= - z[46]*z[35];
    z[25]= - static_cast<T>(1)+ z[25];
    z[32]=n<T>(1,3) + z[19];
    z[32]=n<T>(1,2)*z[32] - z[34];
    z[30]=z[32]*z[30];
    z[25]=n<T>(1,3)*z[25] + z[30];
    z[25]=z[7]*z[25];
    z[28]= - z[28] - n<T>(1,2) - z[33];
    z[28]=z[28]*z[24];
    z[25]=z[25] + z[28];
    z[28]= - z[36] + z[46];
    z[28]=z[7]*z[28];
    z[24]= - z[27]*z[24];
    z[24]=n<T>(1,6)*z[28] + z[24];
    z[24]=z[6]*z[24];
    z[24]=n<T>(1,2)*z[25] + z[24];
    z[24]=z[6]*z[24];
    z[25]=z[15] - z[31];
    z[25]=z[7]*z[25];
    z[27]= - static_cast<T>(1)- z[29];
    z[27]=z[3]*z[27];
    z[23]=n<T>(1,2) + z[23];
    z[23]=z[11]*z[23];
    z[23]=z[23] + z[27] + z[25];
    z[23]=z[7]*z[23];
    z[15]=z[26] + z[15];
    z[25]= - n<T>(7,2)*z[19] + z[38];
    z[25]=z[7]*z[25];
    z[25]=5*z[19] + z[25];
    z[21]=z[25]*z[21];
    z[15]=z[21] + z[23] + n<T>(5,2)*z[15];
    z[15]=z[12]*z[15];
    z[21]= - 3*z[19] + z[26];
    z[21]=z[2]*z[21];
    z[23]=z[4] - z[2];
    z[19]=z[1]*z[19]*z[23];
    z[19]=z[21] + z[19];

    r += n<T>(1,8)*z[15] + z[16] + n<T>(1,4)*z[17] + z[18] + n<T>(1,16)*z[19] + n<T>(1,2)*z[20] + z[22] + z[24];
 
    return r;
}

template double qg_2lha_r1474(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1474(const std::array<dd_real,30>&);
#endif
