#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r994(const std::array<T,30>& k) {
  T z[65];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[24];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[9];
    z[8]=k[10];
    z[9]=k[3];
    z[10]=k[27];
    z[11]=k[15];
    z[12]=k[12];
    z[13]=k[20];
    z[14]=n<T>(1,4)*z[8];
    z[15]= - n<T>(5,4) - z[13];
    z[15]=z[15]*z[14];
    z[16]= - static_cast<T>(5)- 3*z[9];
    z[17]=n<T>(1,2)*z[9];
    z[18]= - static_cast<T>(1)- z[17];
    z[18]=z[4]*z[9]*z[18];
    z[16]=n<T>(1,4)*z[16] + z[18];
    z[16]=z[4]*z[16];
    z[18]=n<T>(1,2)*z[1];
    z[19]=3*z[8];
    z[20]= - n<T>(1,2) - z[19];
    z[20]=z[20]*z[18];
    z[20]=z[20] + z[17] - n<T>(5,4) + 7*z[8];
    z[21]=n<T>(1,8)*z[1];
    z[20]=z[20]*z[21];
    z[22]= - n<T>(11,4) + z[12];
    z[23]=n<T>(7,8) - z[11];
    z[23]=z[9]*z[23];
    z[24]=n<T>(1,4)*z[3];
    z[25]=z[13]*z[24];
    z[15]=z[20] + z[25] + z[16] + n<T>(1,4)*z[23] + z[15] + n<T>(3,8)*z[22] + 
    z[11];
    z[15]=z[6]*z[15];
    z[16]= - static_cast<T>(1)+ z[12];
    z[20]=static_cast<T>(1)+ n<T>(3,4)*z[13];
    z[20]=z[20]*z[13];
    z[22]=z[8]*z[20];
    z[16]=n<T>(1,2)*z[16] + z[22];
    z[22]=z[3]*z[13];
    z[20]= - z[20] - n<T>(3,4)*z[22];
    z[23]=n<T>(1,2)*z[3];
    z[20]=z[20]*z[23];
    z[25]=z[8] - n<T>(1,2);
    z[26]=n<T>(1,4)*z[1];
    z[27]= - z[25]*z[26];
    z[28]=z[4] + n<T>(1,2);
    z[28]=z[28]*z[4];
    z[16]=z[27] + z[20] + n<T>(1,2)*z[16] - z[28];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[6]*z[15];
    z[16]=n<T>(1,8)*z[7];
    z[20]= - static_cast<T>(11)- n<T>(31,2)*z[7];
    z[20]=z[20]*z[16];
    z[27]=n<T>(3,4)*z[3];
    z[29]= - static_cast<T>(7)+ z[27];
    z[29]=z[29]*z[24];
    z[30]=npow(z[4],2);
    z[31]=3*z[30];
    z[20]=z[29] - z[31] + z[20];
    z[29]=z[3] - 1;
    z[32]=n<T>(1,16)*z[3];
    z[33]=z[29]*z[32];
    z[33]=z[33] - z[30];
    z[34]=5*z[7];
    z[35]=n<T>(7,2) + z[34];
    z[35]=z[35]*z[16];
    z[35]=z[35] - z[33];
    z[35]=z[1]*z[35];
    z[36]=z[7] + 1;
    z[37]=n<T>(1,2)*z[7];
    z[38]=z[36]*z[37];
    z[39]=z[23] + z[4];
    z[38]=z[38] + z[39];
    z[40]=z[37] + 1;
    z[41]=n<T>(3,2)*z[7];
    z[42]=z[40]*z[41];
    z[43]= - z[42] - z[39];
    z[43]=z[1]*z[43];
    z[44]= - static_cast<T>(1)+ z[1];
    z[44]=z[6]*z[44];
    z[38]=3*z[44] + n<T>(3,2)*z[38] + z[43];
    z[43]=n<T>(1,2)*z[2];
    z[38]=z[38]*z[43];
    z[44]=n<T>(5,2) + 7*z[1];
    z[44]=z[6]*z[44];
    z[44]=z[44] - 5*z[1] - n<T>(9,2)*z[3] + n<T>(25,2) + 3*z[4];
    z[44]=z[6]*z[44];
    z[20]=z[38] + n<T>(1,8)*z[44] + n<T>(1,2)*z[20] + z[35];
    z[20]=z[2]*z[20];
    z[35]=n<T>(3,2)*z[3];
    z[38]=z[35] + 1;
    z[44]=n<T>(1,8)*z[3];
    z[45]=z[38]*z[44];
    z[46]=n<T>(1,2)*z[6];
    z[47]= - n<T>(5,2) + 3*z[12];
    z[48]= - z[3] + n<T>(1,2)*z[47] - 3*z[11];
    z[48]=n<T>(1,2)*z[48] - z[1];
    z[48]=z[48]*z[46];
    z[49]= - z[11] - n<T>(9,4) + z[12];
    z[50]=n<T>(3,2)*z[4];
    z[51]=z[50] - 1;
    z[52]=z[4]*z[51];
    z[45]=z[48] + z[45] + n<T>(1,4)*z[49] + z[52];
    z[45]=z[6]*z[45];
    z[35]=z[35] + 5;
    z[35]=z[35]*z[32];
    z[48]=n<T>(1,4)*z[7];
    z[49]=n<T>(11,4)*z[7];
    z[52]=z[11] + z[49];
    z[52]=z[52]*z[48];
    z[53]=npow(z[7],2);
    z[54]=z[1]*z[53];
    z[20]=z[20] + z[45] - n<T>(3,16)*z[54] + z[35] + 2*z[30] + z[52];
    z[20]=z[2]*z[20];
    z[45]=n<T>(1,2)*z[4];
    z[52]=z[45] + 1;
    z[27]=z[27] - z[11] + n<T>(1,2)*z[12] - z[52];
    z[54]=n<T>(1,4)*z[6];
    z[27]=z[27]*z[54];
    z[55]= - n<T>(1,2) + z[12];
    z[55]=n<T>(1,2)*z[55] + z[11];
    z[56]= - n<T>(1,2) + 2*z[4];
    z[56]=z[4]*z[56];
    z[57]=z[44] - 1;
    z[58]= - z[3]*z[57];
    z[27]=z[27] + z[58] + n<T>(1,4)*z[55] + z[56];
    z[27]=z[6]*z[27];
    z[40]=z[40]*z[37];
    z[55]=z[40] - z[39];
    z[56]=z[46] + n<T>(1,2) - z[39];
    z[56]=z[6]*z[56];
    z[55]=n<T>(1,2)*z[55] + z[56];
    z[55]=z[2]*z[55];
    z[56]= - n<T>(5,4)*z[7] - n<T>(1,2) - z[11];
    z[56]=z[56]*z[48];
    z[27]=z[55] + z[27] + z[56] - z[33];
    z[27]=z[2]*z[27];
    z[55]= - static_cast<T>(3)- z[3];
    z[55]=z[55]*z[44];
    z[56]=z[4] + 1;
    z[56]= - z[4]*z[56];
    z[47]=z[55] + z[56] + n<T>(1,4)*z[47] + z[11];
    z[47]=z[47]*z[46];
    z[55]=z[12] + 1;
    z[56]= - z[11] + z[55];
    z[31]=z[47] - z[35] + n<T>(1,4)*z[56] - z[31];
    z[31]=z[6]*z[31];
    z[47]=n<T>(3,4)*z[7] - static_cast<T>(1)+ z[11];
    z[47]=z[47]*z[48];
    z[27]=z[27] + z[47] + z[31];
    z[27]=z[2]*z[27];
    z[31]=npow(z[10],2);
    z[47]=z[31]*z[17];
    z[56]= - z[10] + z[47];
    z[56]=z[7]*z[9]*z[56];
    z[58]=npow(z[9],2);
    z[59]=z[58]*npow(z[10],3);
    z[59]=z[59] - z[10];
    z[56]=z[56] + z[59];
    z[56]=z[7]*z[56];
    z[47]=z[10] + z[47];
    z[60]=z[3]*z[9];
    z[47]=z[47]*z[60];
    z[47]=z[47] - z[59];
    z[47]=z[3]*z[47];
    z[59]=z[1]*z[8];
    z[61]= - static_cast<T>(1)+ z[8];
    z[61]=n<T>(1,2)*z[61] + z[59];
    z[61]=z[6]*z[61];
    z[61]=z[8] + z[61];
    z[61]=z[6]*z[61];
    z[47]=z[61] + z[56] + z[47];
    z[56]=z[23] - 1;
    z[56]=z[56]*z[44];
    z[56]=z[56] + n<T>(1,4)*z[12] - z[30];
    z[56]=z[56]*z[46];
    z[56]=z[56] + n<T>(1,8)*z[12] + z[33];
    z[56]=z[6]*z[56];
    z[61]=z[46] + 1;
    z[61]=z[2]*z[46]*z[39]*z[61];
    z[56]=z[56] + z[61];
    z[56]=z[56]*npow(z[2],2);
    z[61]=z[5]*z[8]*npow(z[6],2);
    z[47]= - n<T>(1,32)*z[61] + n<T>(1,16)*z[47] + z[56];
    z[47]=z[5]*z[47];
    z[56]=z[13] + n<T>(5,2);
    z[56]=z[56]*z[13];
    z[61]=n<T>(1,2) - z[56];
    z[61]=z[61]*z[14];
    z[61]= - n<T>(1,8)*z[9] + z[61] + n<T>(11,8) - z[11];
    z[56]=z[56] + z[22];
    z[32]=z[56]*z[32];
    z[56]=n<T>(1,16)*z[1];
    z[62]= - z[18] + n<T>(5,2);
    z[62]=z[8]*z[62];
    z[62]=static_cast<T>(1)+ z[62];
    z[62]=z[62]*z[56];
    z[32]=z[62] + z[32] + n<T>(1,4)*z[61] - z[30];
    z[32]=z[6]*z[32];
    z[61]=z[8] + 1;
    z[59]= - n<T>(3,2)*z[61] - z[59];
    z[32]=n<T>(1,16)*z[59] + z[32];
    z[32]=z[6]*z[32];
    z[31]=z[31]*z[9];
    z[59]=z[10] - 1;
    z[62]=z[59]*z[10];
    z[31]=z[62] + n<T>(3,2)*z[31];
    z[31]=z[31]*z[17];
    z[31]=z[31] + z[10];
    z[62]=z[9]*z[10];
    z[63]= - n<T>(3,2)*z[62] + z[59];
    z[63]=z[9]*z[63]*z[23];
    z[63]=z[63] - n<T>(1,2) + z[31];
    z[63]=z[3]*z[63];
    z[64]=n<T>(1,2) - z[62];
    z[64]=z[7]*z[64];
    z[31]=z[64] + n<T>(3,2) - z[31];
    z[31]=z[7]*z[31];
    z[31]=z[63] - z[14] + z[31];
    z[27]=z[47] + z[27] + n<T>(1,8)*z[31] + z[32];
    z[27]=z[5]*z[27];
    z[31]=z[60] + 1;
    z[32]= - z[1]*z[31];
    z[32]=z[32] - n<T>(5,2)*z[62] + z[59];
    z[32]=z[24]*z[32];
    z[47]=n<T>(5,4)*z[62] + static_cast<T>(1)- n<T>(1,2)*z[10];
    z[47]=n<T>(1,2)*z[47] + z[4];
    z[47]=z[7]*z[47];
    z[32]=z[14] + z[47] + z[32];
    z[15]=z[27] + z[20] + n<T>(1,4)*z[32] + z[15];
    z[15]=z[5]*z[15];
    z[20]= - z[11] + n<T>(1,2)*z[55];
    z[27]=n<T>(5,4)*z[8];
    z[32]=3*z[20] - z[27];
    z[47]= - z[4]*npow(z[9],3);
    z[47]= - n<T>(9,4)*z[58] + z[47];
    z[47]=z[4]*z[47];
    z[19]= - static_cast<T>(1)- z[19];
    z[17]=3*z[19] - z[17];
    z[17]=z[17]*z[26];
    z[19]=n<T>(1,4) + z[8];
    z[17]=z[17] + 3*z[19] - n<T>(11,8)*z[9];
    z[17]=z[17]*z[18];
    z[19]= - static_cast<T>(9)+ z[12];
    z[19]=n<T>(1,4)*z[19] + z[11];
    z[19]=z[9]*z[19];
    z[17]=z[17] + z[47] + n<T>(1,2)*z[32] + z[19];
    z[17]=z[6]*z[17];
    z[19]=z[18] + n<T>(9,2) - 13*z[8];
    z[19]=z[19]*z[21];
    z[32]=n<T>(3,2) + z[12];
    z[47]=z[13] + n<T>(3,4);
    z[55]=z[8]*z[47];
    z[58]=static_cast<T>(1)+ z[9];
    z[58]=z[4]*z[58];
    z[58]=z[58] + 1;
    z[58]=z[9]*z[58];
    z[58]=n<T>(5,4) + z[58];
    z[58]=z[4]*z[58];
    z[17]=z[17] + z[19] - z[22] + z[58] + z[55] + n<T>(1,4)*z[32] - z[11];
    z[17]=z[6]*z[17];
    z[19]=z[47]*z[13];
    z[32]= - n<T>(1,4) - z[19];
    z[32]=z[32]*z[14];
    z[19]=z[19] + z[22];
    z[19]=z[19]*z[24];
    z[22]=npow(z[3],2);
    z[47]=z[22]*z[1];
    z[55]=z[9] - 3;
    z[55]=z[55]*z[3];
    z[55]=z[55] - 1;
    z[58]=z[3]*z[55];
    z[58]=z[58] + z[47];
    z[58]=z[1]*z[58];
    z[31]= - z[3]*z[31];
    z[31]=z[31] + z[58];
    z[31]=z[31]*z[56];
    z[56]=n<T>(3,8)*z[7];
    z[58]= - z[56] + n<T>(3,16) + z[4];
    z[58]=z[7]*z[58];
    z[17]=z[17] + z[31] + z[19] + z[58] + z[32] - z[30];
    z[19]= - n<T>(3,8) - z[7];
    z[19]=z[19]*z[37];
    z[31]=z[22]*npow(z[1],2);
    z[19]= - n<T>(1,32)*z[31] + z[35] + z[30] + z[19];
    z[19]=z[1]*z[19];
    z[31]= - static_cast<T>(3)+ z[24];
    z[31]=z[31]*z[24];
    z[32]= - static_cast<T>(3)- z[34];
    z[32]=z[7]*z[32];
    z[32]=n<T>(1,16)*z[32] - z[33];
    z[32]=z[1]*z[32];
    z[33]=static_cast<T>(1)+ n<T>(21,16)*z[7];
    z[33]=z[7]*z[33];
    z[28]=z[32] + z[31] - z[28] + z[33];
    z[28]=z[1]*z[28];
    z[31]=z[36]*z[41];
    z[32]=z[42] - z[39];
    z[32]=z[1]*z[32];
    z[32]=z[32] - z[31] + z[39];
    z[32]=z[1]*z[32];
    z[33]=n<T>(3,4)*z[53];
    z[32]=z[33] + z[32];
    z[32]=z[32]*z[43];
    z[35]=n<T>(3,2) + z[1];
    z[35]=z[1]*z[35];
    z[35]= - n<T>(5,2) + z[35];
    z[35]=z[35]*z[54];
    z[36]= - n<T>(3,16) - z[7];
    z[36]=z[7]*z[36];
    z[28]=z[32] + z[35] + z[28] + n<T>(5,16)*z[3] + n<T>(1,4)*z[4] + z[36];
    z[28]=z[2]*z[28];
    z[32]=n<T>(11,2) + 13*z[7];
    z[32]=z[32]*z[16];
    z[35]= - n<T>(1,4) - z[3];
    z[35]=z[35]*z[24];
    z[32]=z[35] - z[4] + z[32];
    z[35]=static_cast<T>(3)+ z[1];
    z[35]=z[1]*z[35];
    z[35]= - n<T>(5,4) + z[35];
    z[35]=z[35]*z[46];
    z[26]=z[35] + z[26] + n<T>(9,8) + z[3];
    z[26]=z[26]*z[46];
    z[19]=z[28] + z[26] + n<T>(1,2)*z[32] + z[19];
    z[19]=z[2]*z[19];
    z[15]=z[15] + n<T>(1,2)*z[17] + z[19];
    z[15]=z[5]*z[15];
    z[17]=z[13] - 1;
    z[19]=z[9]*z[13];
    z[17]=z[17]*z[19];
    z[19]=static_cast<T>(1)- z[19];
    z[19]=z[19]*z[24];
    z[19]=z[19] - n<T>(1,4)*z[17] - n<T>(1,4) + z[13];
    z[19]=z[3]*z[19];
    z[17]=z[14]*z[17];
    z[26]=z[4] + n<T>(5,8);
    z[28]= - z[56] + z[26];
    z[28]=z[7]*z[28];
    z[32]= - n<T>(1,4) - z[13];
    z[32]=z[8]*z[32];
    z[17]=z[19] + z[28] - z[45] + z[32] + z[17];
    z[19]=z[55]*z[23];
    z[19]=z[19] + z[47];
    z[19]=z[19]*z[21];
    z[14]=z[19] - z[48] + z[14] - z[30];
    z[14]=z[1]*z[14];
    z[19]= - z[61]*z[37];
    z[19]=z[19] + z[25];
    z[19]=z[1]*z[19];
    z[19]= - n<T>(7,4)*z[8] + z[19];
    z[19]=z[1]*z[19];
    z[21]= - z[7]*z[61];
    z[21]=z[8] + z[21];
    z[21]=z[1]*z[21];
    z[21]=z[9] + z[21];
    z[21]=z[1]*z[21];
    z[21]=z[9] + z[21];
    z[21]=z[21]*z[18];
    z[20]=z[9]*z[20];
    z[20]=z[20] + z[21];
    z[20]=z[6]*z[20];
    z[19]=z[20] + z[27] + z[19];
    z[19]=z[19]*z[46];
    z[14]=z[19] + n<T>(1,2)*z[17] + z[14];
    z[17]= - n<T>(1,2) - z[3];
    z[17]=z[17]*z[44];
    z[17]=n<T>(1,16)*z[47] - z[30] + z[17];
    z[17]=z[1]*z[17];
    z[19]=static_cast<T>(13)+ 3*z[3];
    z[19]=z[19]*z[44];
    z[19]=z[19] + z[50] - z[7];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[1]*z[17];
    z[19]= - z[29]*z[44];
    z[20]= - static_cast<T>(1)+ z[4];
    z[20]=z[4]*z[20];
    z[21]=n<T>(1,2) - z[7];
    z[21]=z[7]*z[21];
    z[17]=z[17] + z[19] + n<T>(5,8)*z[21] + n<T>(1,4) + z[20];
    z[17]=z[1]*z[17];
    z[19]=n<T>(3,2) + z[34];
    z[19]=z[19]*z[37];
    z[19]=z[19] - z[4] - z[38];
    z[20]= - n<T>(7,2) + z[1];
    z[20]=z[1]*z[20];
    z[20]=n<T>(5,2) + z[20];
    z[20]=z[20]*z[54];
    z[17]=z[20] + n<T>(1,4)*z[19] + z[17];
    z[19]= - z[57]*z[23];
    z[20]= - z[30] + n<T>(1,16)*z[22];
    z[20]=z[20]*z[18];
    z[21]=z[4]*z[26];
    z[16]=z[20] + z[19] + z[21] - z[16];
    z[16]=z[1]*z[16];
    z[19]= - static_cast<T>(1)- n<T>(11,8)*z[7];
    z[19]=z[19]*z[48];
    z[20]= - static_cast<T>(5)+ z[24];
    z[20]=z[20]*z[44];
    z[21]= - z[4]*z[52];
    z[16]=z[16] + z[20] + z[19] + n<T>(1,8) + z[21];
    z[16]=z[1]*z[16];
    z[19]=static_cast<T>(1)+ z[49];
    z[19]=z[7]*z[19];
    z[19]=z[23] + z[19] + z[51];
    z[16]=n<T>(1,4)*z[19] + z[16];
    z[16]=z[1]*z[16];
    z[18]=z[18] - 1;
    z[18]=z[39]*z[18];
    z[18]= - z[40] + z[18];
    z[18]=z[1]*z[18];
    z[19]=z[31] + z[39];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[18]=z[1]*z[18];
    z[18]= - z[33] + z[18];
    z[18]=z[1]*z[18];
    z[19]= - static_cast<T>(1)+ z[7];
    z[19]=z[19]*z[48];
    z[18]=z[19] + z[18];
    z[18]=z[18]*z[43];
    z[19]=static_cast<T>(1)- z[49];
    z[19]=z[7]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[16]=z[18] + n<T>(1,8)*z[19] + z[16];
    z[16]=z[2]*z[16];
    z[16]=n<T>(1,2)*z[17] + z[16];
    z[16]=z[2]*z[16];

    r += n<T>(1,2)*z[14] + z[15] + z[16];
 
    return r;
}

template double qg_2lha_r994(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r994(const std::array<dd_real,30>&);
#endif
