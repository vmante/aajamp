#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r718(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[27];
    z[7]=k[12];
    z[8]=k[6];
    z[9]=k[11];
    z[10]=k[21];
    z[11]=k[9];
    z[12]=n<T>(1,3)*z[6];
    z[13]= - n<T>(25,2) + z[6];
    z[13]=z[13]*z[12];
    z[14]=npow(z[6],2);
    z[15]=n<T>(1,2)*z[1];
    z[16]=z[14]*z[15];
    z[13]=z[13] + z[16];
    z[13]=z[1]*z[13];
    z[16]=n<T>(1,3)*z[11];
    z[17]=z[16] + n<T>(1,2);
    z[17]=z[17]*z[11];
    z[18]=z[12] - 1;
    z[19]= - z[6]*z[18];
    z[19]= - z[17] - static_cast<T>(1)+ z[19];
    z[20]=n<T>(1,2)*z[6];
    z[21]=z[4]*z[20];
    z[13]=z[21] + n<T>(1,2)*z[19] + z[13];
    z[19]=n<T>(1,2)*z[4];
    z[13]=z[13]*z[19];
    z[21]=n<T>(1,8)*z[11];
    z[22]=n<T>(11,3)*z[11];
    z[23]= - static_cast<T>(1)- z[22];
    z[23]=z[23]*z[21];
    z[24]= - n<T>(1,4)*z[7] - 7*z[6];
    z[24]=z[24]*z[15];
    z[24]=z[24] + n<T>(11,3) + n<T>(3,8)*z[7];
    z[24]=z[24]*z[15];
    z[25]=n<T>(1,2)*z[9];
    z[26]=z[25] - z[10];
    z[27]=3*z[26];
    z[28]=z[2]*z[27];
    z[17]=z[28] - n<T>(3,2) + z[17];
    z[28]=n<T>(1,4)*z[2];
    z[17]=z[17]*z[28];
    z[29]=static_cast<T>(1)+ z[7];
    z[13]=z[13] + z[17] + z[24] + z[23] + n<T>(1,8)*z[29] - z[12];
    z[13]=z[4]*z[13];
    z[17]= - z[10] + n<T>(3,2)*z[9];
    z[17]=z[17]*z[2];
    z[23]=n<T>(1,2)*z[10];
    z[24]= - z[17] + z[25] - n<T>(5,3) - z[23];
    z[29]=n<T>(1,2)*z[2];
    z[24]=z[24]*z[29];
    z[30]=static_cast<T>(1)+ n<T>(3,8)*z[11];
    z[30]=z[11]*z[30];
    z[24]=z[24] + n<T>(29,24)*z[1] - n<T>(5,6) + z[30];
    z[24]=z[2]*z[24];
    z[30]= - n<T>(17,16) - n<T>(2,3)*z[11];
    z[30]=z[11]*z[30];
    z[31]= - static_cast<T>(43)+ n<T>(23,2)*z[1];
    z[31]=z[1]*z[31];
    z[13]=z[13] + z[24] + n<T>(1,24)*z[31] + n<T>(73,48) + z[30];
    z[13]=z[4]*z[13];
    z[24]=z[11] - n<T>(29,12);
    z[24]=z[24]*z[11];
    z[30]=7*z[1];
    z[31]=z[30] + 5;
    z[31]=z[31]*z[1];
    z[24]= - z[24] - n<T>(17,12) + n<T>(1,12)*z[31];
    z[32]=z[2]*z[24];
    z[33]= - n<T>(29,6) + 2*z[11];
    z[33]=z[11]*z[33];
    z[31]=z[32] - n<T>(1,6)*z[31] + n<T>(17,6) + z[33];
    z[31]=z[2]*z[31];
    z[32]=z[15] + 1;
    z[33]=n<T>(7,6)*z[1];
    z[33]=z[32]*z[33];
    z[32]= - z[1]*z[6]*z[32];
    z[32]= - z[20] + z[32];
    z[32]=z[4]*z[32];
    z[34]= - n<T>(1,4) - z[16];
    z[34]=z[11]*z[34];
    z[32]=n<T>(7,6)*z[32] + z[33] + n<T>(7,12) + z[34];
    z[32]=z[4]*z[32];
    z[34]=z[11] - n<T>(5,6);
    z[34]=z[34]*z[11];
    z[33]=z[34] + z[33] - n<T>(1,6);
    z[34]=z[2] - 1;
    z[33]=z[33]*z[34];
    z[32]=z[32] + z[33];
    z[32]=z[4]*z[32];
    z[24]=z[32] + z[31] + z[24];
    z[24]=z[4]*z[24];
    z[31]=z[1] - 2;
    z[32]=n<T>(1,3)*z[1];
    z[33]=z[31]*z[32];
    z[34]=z[11] - 4;
    z[35]=z[34]*z[16];
    z[33]=z[35] + z[33] + 1;
    z[35]=z[2]*z[33];
    z[31]=z[31]*z[1];
    z[34]=z[34]*z[11];
    z[31]=z[34] + z[31] + 3;
    z[34]=z[35] - z[31];
    z[34]=z[2]*z[34];
    z[31]=z[34] + z[31];
    z[31]=z[2]*z[31];
    z[24]=z[24] + z[31] - z[33];
    z[24]=z[3]*z[24];
    z[31]=z[2]*z[9];
    z[33]=z[31] - static_cast<T>(1)- z[9];
    z[33]=z[33]*z[29];
    z[34]=n<T>(25,6)*z[1];
    z[35]= - static_cast<T>(1)+ z[10];
    z[35]=z[11]*z[35];
    z[33]=z[33] - z[34] + z[35] + n<T>(31,6) - z[10];
    z[33]=z[2]*z[33];
    z[35]= - static_cast<T>(13)+ n<T>(31,2)*z[11];
    z[35]=z[11]*z[35];
    z[35]= - n<T>(101,2) + z[35];
    z[34]=static_cast<T>(21)- z[34];
    z[34]=z[1]*z[34];
    z[34]=n<T>(1,3)*z[35] + z[34];
    z[33]=n<T>(1,2)*z[34] + z[33];
    z[33]=z[33]*z[29];
    z[34]=n<T>(1,4)*z[11];
    z[35]=static_cast<T>(59)- 31*z[11];
    z[35]=z[35]*z[34];
    z[35]=static_cast<T>(5)+ z[35];
    z[36]=n<T>(1,4)*z[1];
    z[37]= - static_cast<T>(17)+ n<T>(25,3)*z[1];
    z[37]=z[37]*z[36];
    z[33]=z[33] + n<T>(1,3)*z[35] + z[37];
    z[33]=z[2]*z[33];
    z[35]=static_cast<T>(13)- n<T>(25,2)*z[1];
    z[35]=z[35]*z[15];
    z[37]= - static_cast<T>(17)+ n<T>(31,4)*z[11];
    z[37]=z[11]*z[37];
    z[35]=z[35] + n<T>(5,4) + z[37];
    z[33]=n<T>(1,6)*z[35] + z[33];
    z[18]= - z[18]*z[20];
    z[35]=n<T>(1,6)*z[1];
    z[35]= - z[14]*z[35];
    z[37]=n<T>(1,2) - z[12];
    z[37]=z[6]*z[37];
    z[35]=z[37] + z[35];
    z[35]=z[1]*z[35];
    z[18]=z[18] + z[35];
    z[18]=z[4]*z[18];
    z[35]=n<T>(1,2)*z[11];
    z[37]=static_cast<T>(3)+ n<T>(13,3)*z[11];
    z[37]=z[37]*z[35];
    z[38]=n<T>(11,3)*z[6];
    z[37]=z[37] - z[38] - static_cast<T>(1)+ z[7];
    z[39]=z[38] - static_cast<T>(1)+ n<T>(5,2)*z[7];
    z[38]=n<T>(3,4)*z[7] + z[38];
    z[38]=z[1]*z[38];
    z[38]=n<T>(1,2)*z[39] + z[38];
    z[38]=z[1]*z[38];
    z[18]=z[18] + n<T>(1,2)*z[37] + z[38];
    z[18]=z[4]*z[18];
    z[37]=static_cast<T>(1)- 37*z[1];
    z[37]=z[37]*z[15];
    z[38]= - static_cast<T>(13)+ n<T>(83,4)*z[11];
    z[38]=z[11]*z[38];
    z[37]=z[37] + n<T>(25,4) + z[38];
    z[38]=13*z[11];
    z[39]= - static_cast<T>(1)- z[38];
    z[39]=z[39]*z[16];
    z[40]=z[26]*npow(z[2],2);
    z[39]=z[40] - z[1] - static_cast<T>(1)+ z[39];
    z[39]=z[2]*z[39];
    z[37]=n<T>(1,3)*z[37] + z[39];
    z[18]=n<T>(1,2)*z[37] + z[18];
    z[18]=z[18]*z[19];
    z[37]= - static_cast<T>(1)+ z[38];
    z[37]=z[37]*z[35];
    z[37]= - static_cast<T>(11)+ z[37];
    z[25]= - z[31] + z[25] - n<T>(1,2) + z[10];
    z[25]=z[2]*z[25];
    z[25]=z[25] + n<T>(1,3)*z[37] - n<T>(5,2)*z[1];
    z[25]=z[25]*z[28];
    z[37]=n<T>(67,3) - 19*z[11];
    z[37]=z[37]*z[35];
    z[37]=n<T>(5,3) + z[37];
    z[38]=static_cast<T>(1)- n<T>(7,8)*z[1];
    z[38]=z[1]*z[38];
    z[25]=z[25] + n<T>(1,4)*z[37] + z[38];
    z[25]=z[2]*z[25];
    z[37]= - static_cast<T>(3)+ z[30];
    z[37]=z[37]*z[36];
    z[22]= - n<T>(23,4) + z[22];
    z[22]=z[11]*z[22];
    z[22]=z[37] + n<T>(5,4) + z[22];
    z[18]=z[18] + n<T>(1,2)*z[22] + z[25];
    z[18]=z[4]*z[18];
    z[18]=z[24] + n<T>(1,2)*z[33] + z[18];
    z[18]=z[3]*z[18];
    z[22]=n<T>(3,2)*z[31] - z[9] + n<T>(17,6) - z[10];
    z[22]=z[22]*z[29];
    z[24]= - static_cast<T>(5)+ z[10];
    z[23]= - static_cast<T>(1)- z[23];
    z[23]=z[11]*z[23];
    z[22]=z[22] + n<T>(11,6)*z[1] + n<T>(1,2)*z[24] + z[23];
    z[22]=z[2]*z[22];
    z[23]=n<T>(7,3)*z[11];
    z[24]=n<T>(15,2) + z[23];
    z[24]=z[11]*z[24];
    z[25]= - n<T>(13,2) - n<T>(7,3)*z[1];
    z[25]=z[1]*z[25];
    z[24]=z[25] - n<T>(5,6) + z[24];
    z[22]=n<T>(1,4)*z[24] + z[22];
    z[22]=z[2]*z[22];
    z[24]= - n<T>(5,2) + z[30];
    z[24]=z[24]*z[32];
    z[23]= - n<T>(9,2) - z[23];
    z[23]=z[11]*z[23];
    z[23]=z[24] + n<T>(31,6) + z[23];
    z[22]=n<T>(1,4)*z[23] + z[22];
    z[13]=z[18] + n<T>(1,2)*z[22] + z[13];
    z[13]=z[3]*z[13];
    z[18]=z[4]*z[11];
    z[18]=z[18] - 1;
    z[18]=z[20]*z[18];
    z[20]=static_cast<T>(1)- n<T>(1,2)*z[5];
    z[20]=z[6]*z[20];
    z[20]= - z[34] - n<T>(1,2) + z[20];
    z[20]=z[11]*z[20];
    z[22]= - z[14]*z[32];
    z[18]=z[22] + z[20] + z[18];
    z[18]=z[18]*z[19];
    z[19]= - n<T>(1,4) + z[5];
    z[19]=z[6]*z[19];
    z[19]=n<T>(41,4) + z[19];
    z[19]=z[19]*z[12];
    z[14]= - z[14]*z[36];
    z[14]=z[19] + z[14];
    z[14]=z[1]*z[14];
    z[19]= - static_cast<T>(1)+ z[8];
    z[20]= - n<T>(1,4)*z[8] + z[26];
    z[20]=z[5]*z[20];
    z[19]=n<T>(1,4)*z[19] + z[20];
    z[20]=static_cast<T>(3)- z[5];
    z[20]=z[6]*z[20];
    z[20]= - n<T>(13,12)*z[11] + n<T>(1,2) + z[20];
    z[20]=z[20]*z[34];
    z[22]=z[27] - z[11];
    z[22]=z[22]*z[28];
    z[23]=npow(z[5],2);
    z[24]=z[23]*z[6];
    z[25]= - n<T>(1,12)*z[24] - n<T>(3,4) - z[5];
    z[25]=z[6]*z[25];
    z[14]=z[18] + z[22] + z[14] + z[20] + n<T>(1,4)*z[19] + z[25];
    z[14]=z[4]*z[14];
    z[18]=z[5] - 1;
    z[18]=z[18]*z[24];
    z[19]= - static_cast<T>(1)+ n<T>(11,6)*z[5];
    z[19]=z[5]*z[19];
    z[19]=z[19] + n<T>(1,6)*z[18];
    z[19]=z[6]*z[19];
    z[20]=n<T>(49,8) - z[11];
    z[16]=z[20]*z[16];
    z[20]=n<T>(19,2)*z[5] + n<T>(1,2) - z[7];
    z[16]=z[16] + n<T>(1,4)*z[20] + z[19];
    z[19]=z[5] - n<T>(1,2);
    z[20]=z[6]*z[5];
    z[19]=z[19]*z[20];
    z[22]= - n<T>(1,6)*z[19] + n<T>(1,3) - n<T>(5,4)*z[5];
    z[22]=z[6]*z[22];
    z[24]=static_cast<T>(1)+ n<T>(1,4)*z[20];
    z[12]=z[1]*z[24]*z[12];
    z[24]=z[7] + z[8];
    z[12]=z[12] + z[22] - n<T>(5,3) - n<T>(1,16)*z[24];
    z[12]=z[1]*z[12];
    z[22]=z[26]*z[5];
    z[24]= - z[35] - z[22] + n<T>(1,4)*z[9] - n<T>(7,6) - z[10];
    z[25]=z[10] - n<T>(3,4)*z[9];
    z[25]=z[2]*z[25];
    z[24]=n<T>(1,2)*z[24] + z[25];
    z[24]=z[2]*z[24];
    z[12]=z[14] + z[24] + n<T>(1,2)*z[16] + z[12];
    z[12]=z[4]*z[12];
    z[14]=static_cast<T>(31)- z[11];
    z[14]=z[14]*z[21];
    z[14]=z[14] + z[23] + z[18];
    z[16]=static_cast<T>(1)+ z[20];
    z[15]=z[16]*z[15];
    z[15]=z[15] - z[19] - n<T>(7,8) - z[5];
    z[15]=z[1]*z[15];
    z[14]=n<T>(1,2)*z[14] + z[15];
    z[15]= - n<T>(13,6) - z[9];
    z[16]=z[11]*z[10];
    z[15]=z[17] + z[16] + n<T>(1,2)*z[15] + z[22];
    z[15]=z[2]*z[15];
    z[16]= - n<T>(11,2) - 7*z[5];
    z[16]=n<T>(1,2)*z[16] + z[30];
    z[15]=n<T>(1,3)*z[16] + z[15];
    z[15]=z[15]*z[28];

    r += z[12] + z[13] + n<T>(1,3)*z[14] + z[15];
 
    return r;
}

template double qg_2lha_r718(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r718(const std::array<dd_real,30>&);
#endif
