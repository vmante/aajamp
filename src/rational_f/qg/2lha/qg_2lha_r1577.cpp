#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1577(const std::array<T,30>& k) {
  T z[73];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[10];
    z[6]=k[9];
    z[7]=k[11];
    z[8]=k[3];
    z[9]=k[12];
    z[10]=k[6];
    z[11]=npow(z[6],2);
    z[12]=n<T>(1,4) + z[6];
    z[12]=z[12]*z[11];
    z[13]=n<T>(1,4)*z[4];
    z[14]=npow(z[6],3);
    z[15]=z[14]*z[13];
    z[12]=z[12] + z[15];
    z[12]=z[4]*z[12];
    z[15]=n<T>(1,2)*z[6];
    z[16]=3*z[6];
    z[17]=n<T>(1,2) + z[16];
    z[17]=z[6]*z[17];
    z[17]= - n<T>(1,4) + z[17];
    z[17]=z[17]*z[15];
    z[12]=z[17] + z[12];
    z[12]=z[4]*z[12];
    z[17]= - n<T>(1,4) + z[6];
    z[17]=z[17]*z[11];
    z[12]=z[17] + z[12];
    z[12]=z[4]*z[12];
    z[17]=z[16] - 1;
    z[18]=z[17]*z[11];
    z[19]= - n<T>(1,2) + z[16];
    z[19]=z[19]*z[11];
    z[20]=z[14]*z[4];
    z[19]=z[19] + z[20];
    z[19]=z[4]*z[19];
    z[18]=z[18] + z[19];
    z[18]=z[4]*z[18];
    z[19]=z[6] - n<T>(1,2);
    z[21]=z[19]*z[11];
    z[18]=z[21] + z[18];
    z[21]=n<T>(1,4)*z[8];
    z[18]=z[18]*z[21];
    z[22]=n<T>(1,4)*z[6];
    z[23]=z[6] - 1;
    z[24]=z[23]*z[6];
    z[25]=n<T>(1,2) + z[24];
    z[25]=z[25]*z[22];
    z[12]=z[18] - n<T>(1,8)*z[7] + z[25] + z[12];
    z[12]=z[8]*z[12];
    z[18]=z[15]*z[4];
    z[25]=z[6] + z[18];
    z[25]=z[4]*z[25];
    z[25]=z[15] + z[25];
    z[26]=n<T>(1,4)*z[3];
    z[27]=npow(z[4],2);
    z[25]=z[25]*z[27]*z[26];
    z[28]=z[15] - 1;
    z[29]= - z[28]*z[22];
    z[30]=z[11]*z[4];
    z[31]=z[11] + n<T>(3,8)*z[30];
    z[31]=z[4]*z[31];
    z[31]=n<T>(3,4)*z[11] + z[31];
    z[31]=z[4]*z[31];
    z[31]=z[22] + z[31];
    z[31]=z[4]*z[31];
    z[32]=z[18] + z[23];
    z[32]=z[4]*z[32];
    z[32]=z[32] + z[28];
    z[33]=n<T>(1,4)*z[7];
    z[32]=z[32]*z[33];
    z[12]=z[12] + z[32] + z[25] + z[29] + z[31];
    z[12]=z[9]*z[12];
    z[25]=z[6]*z[17];
    z[25]= - static_cast<T>(3)+ z[25];
    z[25]=z[25]*z[15];
    z[29]=n<T>(3,2)*z[11];
    z[31]= - z[23]*z[29];
    z[20]=z[31] - z[20];
    z[20]=z[4]*z[20];
    z[20]=z[25] + z[20];
    z[20]=z[4]*z[20];
    z[25]= - n<T>(5,2) + z[16];
    z[25]=z[6]*z[25];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[16];
    z[25]=static_cast<T>(1)+ z[25];
    z[20]=n<T>(1,2)*z[25] + z[20];
    z[25]=n<T>(1,2)*z[4];
    z[20]=z[20]*z[25];
    z[31]=5*z[6];
    z[32]=z[31] - n<T>(11,2);
    z[34]=z[32]*z[22];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[6]*z[34];
    z[12]=z[12] + z[20] + n<T>(1,2) + z[34];
    z[20]=static_cast<T>(5)- z[16];
    z[20]=z[20]*z[15];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[20]*z[11];
    z[34]=static_cast<T>(7)- n<T>(17,2)*z[6];
    z[34]=z[34]*z[14];
    z[35]=npow(z[6],4);
    z[36]=z[35]*z[4];
    z[34]=n<T>(1,2)*z[34] - z[36];
    z[34]=z[4]*z[34];
    z[20]=n<T>(9,2)*z[20] + z[34];
    z[20]=z[20]*z[25];
    z[34]=static_cast<T>(6)- n<T>(19,8)*z[6];
    z[34]=z[6]*z[34];
    z[34]= - n<T>(39,8) + z[34];
    z[34]=z[6]*z[34];
    z[34]=n<T>(5,4) + z[34];
    z[34]=z[6]*z[34];
    z[20]=z[34] + z[20];
    z[20]=z[4]*z[20];
    z[34]=static_cast<T>(17)- z[31];
    z[34]=z[6]*z[34];
    z[34]= - static_cast<T>(21)+ z[34];
    z[34]=z[6]*z[34];
    z[34]=static_cast<T>(11)+ z[34];
    z[34]=z[34]*z[15];
    z[34]= - static_cast<T>(1)+ z[34];
    z[20]=n<T>(1,4)*z[34] + z[20];
    z[20]=z[20]*npow(z[4],4);
    z[34]=z[13]*z[35];
    z[37]=z[23]*z[14];
    z[38]= - z[37] - z[34];
    z[38]=z[4]*z[38];
    z[39]=z[28]*z[6];
    z[40]=z[39] + n<T>(1,2);
    z[41]=z[40]*z[11];
    z[42]=3*z[41];
    z[38]= - z[42] + z[38];
    z[38]=z[4]*z[38];
    z[43]=z[6] - 3;
    z[43]=z[43]*z[6];
    z[43]=z[43] + 3;
    z[44]=z[43]*z[6];
    z[44]=z[44] - 1;
    z[45]=z[44]*z[6];
    z[38]= - z[45] + z[38];
    z[38]=z[4]*z[38];
    z[46]=z[22] - 1;
    z[47]=z[46]*z[6];
    z[47]=z[47] + n<T>(3,2);
    z[47]=z[47]*z[6];
    z[48]=z[47] - 1;
    z[48]=z[48]*z[6];
    z[48]=z[48] + n<T>(1,4);
    z[38]=z[38] - z[48];
    z[38]=z[7]*z[38]*npow(z[4],5);
    z[20]=z[20] + n<T>(1,2)*z[38];
    z[20]=z[7]*z[20];
    z[38]=static_cast<T>(31)- 23*z[6];
    z[38]=z[6]*z[38];
    z[38]= - n<T>(19,2) + z[38];
    z[38]=z[38]*z[11];
    z[49]=n<T>(3,2)*z[6];
    z[50]=z[49] - 1;
    z[51]=z[50]*z[14];
    z[51]= - 3*z[51] - z[36];
    z[51]=z[4]*z[51];
    z[38]=z[38] + 3*z[51];
    z[38]=z[38]*z[13];
    z[51]=static_cast<T>(9)- n<T>(35,8)*z[6];
    z[51]=z[6]*z[51];
    z[51]= - n<T>(45,8) + z[51];
    z[51]=z[6]*z[51];
    z[51]=static_cast<T>(1)+ z[51];
    z[51]=z[6]*z[51];
    z[38]=z[51] + z[38];
    z[38]=z[4]*z[38];
    z[51]=n<T>(5,2)*z[6];
    z[52]=static_cast<T>(7)- z[51];
    z[52]=z[6]*z[52];
    z[52]= - n<T>(27,4) + z[52];
    z[52]=z[6]*z[52];
    z[52]=n<T>(5,2) + z[52];
    z[52]=z[6]*z[52];
    z[52]= - n<T>(1,4) + z[52];
    z[38]=n<T>(1,2)*z[52] + z[38];
    z[38]=z[38]*npow(z[4],3);
    z[20]=z[38] + z[20];
    z[20]=z[7]*z[20];
    z[32]= - z[6]*z[32];
    z[32]= - n<T>(5,4) + z[32];
    z[32]=z[32]*z[11];
    z[38]=13*z[6];
    z[52]=static_cast<T>(7)- z[38];
    z[52]=z[52]*z[14];
    z[53]=3*z[36];
    z[52]=z[52] - z[53];
    z[52]=z[52]*z[13];
    z[32]=z[32] + z[52];
    z[32]=z[4]*z[32];
    z[52]=z[31] - 9;
    z[54]= - z[6]*z[52];
    z[54]= - n<T>(9,2) + z[54];
    z[54]=z[6]*z[54];
    z[54]=n<T>(1,2) + z[54];
    z[54]=z[54]*z[15];
    z[32]=z[54] + z[32];
    z[32]=z[32]*z[25];
    z[54]=n<T>(5,8)*z[6];
    z[55]=z[54] - 1;
    z[55]=z[55]*z[6];
    z[55]=z[55] + n<T>(3,8);
    z[55]=z[55]*z[11];
    z[32]=z[55] + z[32];
    z[32]=z[4]*z[32];
    z[56]=n<T>(1,8)*z[6];
    z[57]=7*z[6];
    z[58]= - static_cast<T>(17)+ z[57];
    z[58]=z[6]*z[58];
    z[58]=static_cast<T>(13)+ z[58];
    z[58]=z[6]*z[58];
    z[58]= - static_cast<T>(3)+ z[58];
    z[58]=z[58]*z[56];
    z[32]=z[58] + z[32];
    z[32]=z[4]*z[32];
    z[58]=z[35]*z[25];
    z[59]=static_cast<T>(1)- z[51];
    z[59]=z[59]*z[14];
    z[59]=z[59] - z[58];
    z[59]=z[59]*z[13];
    z[60]=n<T>(5,4)*z[6];
    z[61]=static_cast<T>(1)- z[60];
    z[61]=z[6]*z[61];
    z[61]= - n<T>(1,8) + z[61];
    z[61]=z[61]*z[11];
    z[59]=z[61] + z[59];
    z[59]=z[4]*z[59];
    z[61]=z[51] - 3;
    z[61]=z[61]*z[6];
    z[62]=z[61] + n<T>(3,4);
    z[63]=n<T>(1,2)*z[11];
    z[64]= - z[62]*z[63];
    z[59]=z[64] + z[59];
    z[59]=z[4]*z[59];
    z[55]= - z[55] + z[59];
    z[55]=z[4]*z[55];
    z[55]= - n<T>(1,4)*z[41] + z[55];
    z[55]=z[8]*z[55];
    z[59]=z[44]*z[22];
    z[64]=z[40]*z[30];
    z[45]= - z[45] - z[64];
    z[45]=z[45]*z[25];
    z[45]=z[45] - z[48];
    z[65]=n<T>(1,2)*z[3];
    z[45]=z[45]*z[65];
    z[20]=z[55] + z[20] + z[45] + z[59] + z[32];
    z[20]=z[10]*z[20];
    z[32]=static_cast<T>(199)- 57*z[6];
    z[32]=z[6]*z[32];
    z[32]= - static_cast<T>(255)+ z[32];
    z[32]=z[6]*z[32];
    z[32]=static_cast<T>(141)+ z[32];
    z[32]=z[32]*z[22];
    z[45]=static_cast<T>(7)- z[16];
    z[45]=z[6]*z[45];
    z[45]= - static_cast<T>(5)+ z[45];
    z[45]=z[6]*z[45];
    z[45]=static_cast<T>(1)+ z[45];
    z[45]=z[4]*z[45]*z[51];
    z[32]=z[45] - static_cast<T>(7)+ z[32];
    z[32]=z[32]*z[25];
    z[45]=z[49] - 7;
    z[45]=z[45]*z[6];
    z[45]=z[45] + 13;
    z[45]=z[45]*z[22];
    z[45]=z[45] - 3;
    z[55]=z[6]*z[45];
    z[59]=z[4]*z[6];
    z[66]=z[44]*z[59];
    z[67]= - static_cast<T>(4)+ z[6];
    z[67]=z[6]*z[67];
    z[67]=static_cast<T>(6)+ z[67];
    z[67]=z[6]*z[67];
    z[67]= - static_cast<T>(4)+ z[67];
    z[67]=z[6]*z[67];
    z[67]=static_cast<T>(1)+ z[67];
    z[66]=2*z[67] + z[66];
    z[66]=z[4]*z[66];
    z[67]=z[6] - 5;
    z[68]=z[67]*z[6];
    z[68]=z[68] + 10;
    z[68]=z[68]*z[6];
    z[68]=z[68] - 10;
    z[68]=z[68]*z[6];
    z[68]=z[68] + 4;
    z[66]=z[66] + z[68];
    z[66]=z[3]*z[66];
    z[32]=z[66] + z[32] - n<T>(73,8) - 9*z[55];
    z[32]=z[3]*z[32];
    z[55]= - n<T>(33,4) + z[31];
    z[55]=z[6]*z[55];
    z[55]=n<T>(7,2) + z[55];
    z[55]=z[6]*z[55];
    z[55]= - n<T>(1,4) + z[55];
    z[55]=z[55]*z[59];
    z[66]= - n<T>(207,2) + 35*z[6];
    z[66]=z[6]*z[66];
    z[66]=n<T>(207,2) + z[66];
    z[66]=z[6]*z[66];
    z[66]= - n<T>(73,2) + z[66];
    z[66]=z[6]*z[66];
    z[66]=n<T>(3,2) + z[66];
    z[55]=n<T>(1,4)*z[66] + z[55];
    z[55]=z[4]*z[55];
    z[66]= - n<T>(33,2) + 4*z[6];
    z[66]=z[6]*z[66];
    z[66]=n<T>(107,4) + z[66];
    z[66]=z[6]*z[66];
    z[66]= - n<T>(85,4) + z[66];
    z[66]=z[6]*z[66];
    z[32]=z[32] + z[55] + n<T>(19,4) + z[66];
    z[32]=z[3]*z[32];
    z[55]=15*z[6];
    z[66]=static_cast<T>(49)- z[55];
    z[66]=z[6]*z[66];
    z[66]= - n<T>(137,2) + z[66];
    z[66]=z[6]*z[66];
    z[66]=static_cast<T>(41)+ z[66];
    z[66]=z[66]*z[15];
    z[66]= - static_cast<T>(3)+ z[66];
    z[69]=n<T>(9,2) - z[31];
    z[69]=z[6]*z[69];
    z[69]= - n<T>(1,2) + z[69];
    z[69]=z[69]*z[30];
    z[70]=n<T>(73,2) - z[55];
    z[70]=z[6]*z[70];
    z[70]= - n<T>(47,2) + z[70];
    z[70]=z[6]*z[70];
    z[70]=n<T>(5,2) + z[70];
    z[70]=z[6]*z[70];
    z[70]=static_cast<T>(5)+ z[70];
    z[69]=n<T>(1,2)*z[70] + z[69];
    z[69]=z[4]*z[69];
    z[66]=n<T>(1,2)*z[66] + z[69];
    z[32]=n<T>(1,2)*z[66] + z[32];
    z[32]=z[3]*z[32];
    z[45]=z[45]*z[16];
    z[66]=z[67]*z[15];
    z[66]=z[66] + 5;
    z[66]=z[66]*z[6];
    z[66]=z[66] - 5;
    z[67]= - z[6]*z[66];
    z[69]=static_cast<T>(2)- z[15];
    z[69]=z[6]*z[69];
    z[69]= - static_cast<T>(3)+ z[69];
    z[69]=z[6]*z[69];
    z[69]=static_cast<T>(2)+ z[69];
    z[69]=z[6]*z[69];
    z[69]= - n<T>(1,2) + z[69];
    z[69]=z[4]*z[69];
    z[67]=z[69] - static_cast<T>(2)+ z[67];
    z[67]=z[3]*z[67];
    z[67]=z[67] + n<T>(23,8) + z[45];
    z[69]=n<T>(15,2)*z[6];
    z[70]= - static_cast<T>(23)+ z[69];
    z[70]=z[70]*z[15];
    z[70]=static_cast<T>(12)+ z[70];
    z[70]=z[6]*z[70];
    z[70]= - n<T>(9,2) + z[70];
    z[70]=z[6]*z[70];
    z[70]=n<T>(1,4) + z[70];
    z[70]=z[4]*z[70];
    z[67]=z[70] + 3*z[67];
    z[67]=z[3]*z[67];
    z[55]=static_cast<T>(67)- z[55];
    z[55]=z[55]*z[56];
    z[55]= - static_cast<T>(13)+ z[55];
    z[55]=z[6]*z[55];
    z[55]=n<T>(15,2) + z[55];
    z[55]=z[6]*z[55];
    z[70]=n<T>(21,2) - z[31];
    z[70]=z[70]*z[15];
    z[70]= - static_cast<T>(3)+ z[70];
    z[70]=z[6]*z[70];
    z[70]=n<T>(1,4) + z[70];
    z[70]=z[6]*z[70];
    z[70]=n<T>(13,8) + z[70];
    z[70]=z[4]*z[70];
    z[55]=z[67] + z[70] - n<T>(13,8) + z[55];
    z[55]=z[3]*z[55];
    z[55]= - n<T>(3,4) + z[55];
    z[55]=z[3]*z[55];
    z[67]=static_cast<T>(19)- z[31];
    z[67]=z[67]*z[15];
    z[67]= - static_cast<T>(13)+ z[67];
    z[67]=z[6]*z[67];
    z[67]=static_cast<T>(7)+ z[67];
    z[67]=z[6]*z[67];
    z[67]= - n<T>(19,4) + z[67];
    z[68]=z[3]*z[68];
    z[67]=z[68] + n<T>(1,2)*z[67] + z[4];
    z[67]=z[3]*z[67];
    z[67]= - n<T>(3,4) + z[67];
    z[68]=npow(z[3],2);
    z[67]=z[67]*z[68];
    z[70]=npow(z[3],3);
    z[71]=z[13] - 1;
    z[72]=z[3]*z[71];
    z[72]= - n<T>(1,4) + z[72];
    z[72]=z[2]*z[72]*z[70];
    z[67]=z[67] + z[72];
    z[67]=z[2]*z[67];
    z[55]=z[55] + z[67];
    z[55]=z[2]*z[55];
    z[32]=z[55] - n<T>(1,4) + z[32];
    z[32]=z[2]*z[32];
    z[55]= - static_cast<T>(19)+ n<T>(29,4)*z[6];
    z[55]=z[6]*z[55];
    z[55]=n<T>(65,4) + z[55];
    z[55]=z[6]*z[55];
    z[55]= - n<T>(9,2) + z[55];
    z[55]=z[55]*z[15];
    z[67]= - static_cast<T>(2)+ z[60];
    z[67]=z[6]*z[67];
    z[67]=n<T>(3,4) + z[67];
    z[67]=z[67]*z[30];
    z[55]=z[55] + z[67];
    z[55]=z[4]*z[55];
    z[67]= - n<T>(51,2) + z[57];
    z[67]=z[6]*z[67];
    z[67]=n<T>(69,2) + z[67];
    z[67]=z[6]*z[67];
    z[67]= - n<T>(41,2) + z[67];
    z[67]=z[6]*z[67];
    z[67]=n<T>(9,2) + z[67];
    z[55]=n<T>(1,2)*z[67] + z[55];
    z[55]=z[4]*z[55];
    z[67]= - z[44]*z[49];
    z[64]=z[67] - z[64];
    z[64]=z[64]*z[25];
    z[48]= - 3*z[48] + z[64];
    z[48]=z[4]*z[48];
    z[64]= - z[66]*z[15];
    z[48]=z[48] - static_cast<T>(1)+ z[64];
    z[48]=z[3]*z[48];
    z[45]=z[48] + z[55] + n<T>(25,8) + z[45];
    z[45]=z[3]*z[45];
    z[48]=static_cast<T>(61)- n<T>(55,2)*z[6];
    z[48]=z[6]*z[48];
    z[48]= - n<T>(83,2) + z[48];
    z[48]=z[48]*z[22];
    z[48]=static_cast<T>(2)+ z[48];
    z[48]=z[6]*z[48];
    z[55]= - z[62]*z[30];
    z[48]=z[48] + z[55];
    z[48]=z[4]*z[48];
    z[55]=static_cast<T>(20)- n<T>(51,8)*z[6];
    z[55]=z[6]*z[55];
    z[55]= - n<T>(179,8) + z[55];
    z[55]=z[6]*z[55];
    z[55]=n<T>(41,4) + z[55];
    z[55]=z[6]*z[55];
    z[48]=z[48] - n<T>(3,2) + z[55];
    z[48]=z[4]*z[48];
    z[55]=n<T>(63,8) - 2*z[6];
    z[55]=z[6]*z[55];
    z[55]= - n<T>(95,8) + z[55];
    z[55]=z[6]*z[55];
    z[55]=n<T>(67,8) + z[55];
    z[55]=z[6]*z[55];
    z[45]=z[45] + z[48] - n<T>(13,8) + z[55];
    z[45]=z[3]*z[45];
    z[48]=z[52]*z[31];
    z[48]=n<T>(43,2) + z[48];
    z[48]=z[6]*z[48];
    z[48]= - n<T>(5,2) + z[48];
    z[48]=z[48]*z[22];
    z[51]= - static_cast<T>(2)+ z[51];
    z[51]=z[6]*z[51];
    z[51]=n<T>(1,4) + z[51];
    z[30]=z[51]*z[30];
    z[30]=z[48] + z[30];
    z[30]=z[4]*z[30];
    z[48]= - static_cast<T>(19)+ z[69];
    z[48]=z[48]*z[16];
    z[48]=n<T>(103,2) + z[48];
    z[48]=z[48]*z[22];
    z[48]= - static_cast<T>(5)+ z[48];
    z[48]=z[6]*z[48];
    z[30]=z[30] + n<T>(3,4) + z[48];
    z[30]=z[4]*z[30];
    z[48]= - n<T>(43,2) + z[57];
    z[48]=z[6]*z[48];
    z[48]=n<T>(55,2) + z[48];
    z[48]=z[6]*z[48];
    z[48]= - n<T>(83,4) + z[48];
    z[48]=z[6]*z[48];
    z[48]=n<T>(13,2) + z[48];
    z[30]=z[45] + n<T>(1,4)*z[48] + z[30];
    z[30]=z[3]*z[30];
    z[45]=static_cast<T>(1)- z[31];
    z[45]=z[45]*z[14]*z[25];
    z[48]=z[31] - n<T>(27,4);
    z[48]=z[48]*z[6];
    z[51]= - n<T>(5,4) - z[48];
    z[51]=z[51]*z[11];
    z[45]=z[51] + z[45];
    z[45]=z[4]*z[45];
    z[51]=n<T>(17,2) - z[31];
    z[51]=z[6]*z[51];
    z[51]= - n<T>(25,4) + z[51];
    z[51]=z[6]*z[51];
    z[51]=static_cast<T>(1)+ z[51];
    z[51]=z[6]*z[51];
    z[45]=z[45] + n<T>(3,4) + z[51];
    z[45]=z[4]*z[45];
    z[31]=static_cast<T>(11)- z[31];
    z[31]=z[6]*z[31];
    z[31]= - n<T>(27,2) + z[31];
    z[31]=z[31]*z[15];
    z[31]=static_cast<T>(5)+ z[31];
    z[31]=z[31]*z[15];
    z[31]=z[31] + z[45];
    z[30]=z[32] + n<T>(1,2)*z[31] + z[30];
    z[30]=z[2]*z[30];
    z[31]= - static_cast<T>(5)+ n<T>(13,4)*z[6];
    z[31]=z[6]*z[31];
    z[31]=n<T>(3,2) + z[31];
    z[31]=z[31]*z[63];
    z[32]= - static_cast<T>(1)+ n<T>(11,8)*z[6];
    z[32]=z[32]*z[14];
    z[32]=z[32] + n<T>(3,8)*z[36];
    z[32]=z[4]*z[32];
    z[31]=z[31] + z[32];
    z[31]=z[4]*z[31];
    z[32]=z[60] - 3;
    z[32]=z[32]*z[6];
    z[45]=n<T>(5,4) + z[32];
    z[45]=z[45]*z[63];
    z[31]=z[45] + z[31];
    z[31]=z[4]*z[31];
    z[45]= - static_cast<T>(1)- z[61];
    z[31]=n<T>(1,4)*z[45] + z[31];
    z[31]=z[4]*z[31];
    z[45]=n<T>(1,4)*z[2];
    z[51]= - n<T>(3,4) - z[39];
    z[31]= - z[45] + n<T>(1,2)*z[51] + z[31];
    z[31]=z[27]*z[31];
    z[51]=3*z[37] + z[36];
    z[51]=z[51]*z[25];
    z[42]=z[42] + z[51];
    z[42]=z[4]*z[42];
    z[43]=z[43]*z[15];
    z[43]= - static_cast<T>(1)+ z[43];
    z[43]=z[6]*z[43];
    z[42]=z[43] + z[42];
    z[42]=z[4]*z[42];
    z[17]= - n<T>(1,2)*z[17] + z[42];
    z[17]=z[4]*z[17];
    z[17]=z[17] - z[50];
    z[17]=z[4]*z[17];
    z[42]=n<T>(1,2)*z[23];
    z[43]=z[2] + static_cast<T>(1)+ z[4];
    z[43]=z[2]*z[43];
    z[17]=n<T>(1,2)*z[43] - z[42] + z[17];
    z[17]=z[33]*z[27]*z[17];
    z[17]=z[17] + z[31];
    z[17]=z[7]*z[17];
    z[31]=static_cast<T>(1)+ z[27];
    z[31]=z[31]*z[65];
    z[33]=z[4] - 1;
    z[43]=z[2] - z[33];
    z[43]=z[65]*z[43];
    z[43]=static_cast<T>(1)+ z[43];
    z[43]=z[2]*z[43];
    z[31]=z[43] + z[31] - z[33];
    z[31]=z[2]*z[31];
    z[18]= - z[18] - z[19];
    z[18]=z[4]*z[18];
    z[18]=z[18] - z[19];
    z[18]=z[4]*z[18];
    z[18]=z[18] - z[19];
    z[18]=z[4]*z[18];
    z[18]= - z[42] + z[18];
    z[18]=z[3]*z[18];
    z[18]=z[18] + z[31];
    z[31]=n<T>(21,8) + z[48];
    z[31]=z[31]*z[11];
    z[33]= - static_cast<T>(9)+ z[38];
    z[33]=z[33]*z[14];
    z[33]=z[33] + z[53];
    z[33]=z[33]*z[13];
    z[31]=z[31] + z[33];
    z[31]=z[31]*z[25];
    z[32]=n<T>(19,8) + z[32];
    z[32]=z[6]*z[32];
    z[32]= - n<T>(3,4) + z[32];
    z[32]=z[6]*z[32];
    z[31]=z[32] + z[31];
    z[31]=z[4]*z[31];
    z[16]=n<T>(13,2) - z[16];
    z[16]=z[6]*z[16];
    z[16]= - static_cast<T>(5)+ z[16];
    z[16]=z[6]*z[16];
    z[16]=static_cast<T>(3)+ z[16];
    z[16]=n<T>(1,8)*z[16] + z[31];
    z[16]=z[16]*z[27];
    z[16]=z[17] + n<T>(1,8) + z[16] + n<T>(1,8)*z[18];
    z[16]=z[7]*z[16];
    z[17]= - static_cast<T>(2)+ z[54];
    z[17]=z[6]*z[17];
    z[17]=n<T>(3,4) + z[17];
    z[17]=z[17]*z[11];
    z[18]= - static_cast<T>(3)+ n<T>(7,2)*z[6];
    z[18]=z[18]*z[14];
    z[18]=z[18] + z[36];
    z[18]=z[18]*z[13];
    z[17]=z[17] + z[18];
    z[17]=z[4]*z[17];
    z[18]=static_cast<T>(11)- z[57];
    z[18]=z[18]*z[15];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[22];
    z[17]=z[18] + z[17];
    z[17]=z[4]*z[17];
    z[18]=z[37] + z[58];
    z[18]=z[4]*z[18];
    z[31]= - static_cast<T>(1)+ z[39];
    z[31]=z[31]*z[11];
    z[18]=z[31] + z[18];
    z[13]=z[18]*z[13];
    z[18]= - static_cast<T>(1)+ n<T>(3,4)*z[6];
    z[31]=z[18]*z[6];
    z[13]= - z[31] + z[13];
    z[13]=z[4]*z[13];
    z[32]=static_cast<T>(5)- z[49];
    z[32]=z[32]*z[15];
    z[32]= - static_cast<T>(1)+ z[32];
    z[13]=n<T>(1,2)*z[32] + z[13];
    z[13]=z[4]*z[13];
    z[13]=n<T>(1,4)*z[19] + z[13];
    z[13]=z[7]*z[13];
    z[19]=z[23]*z[22];
    z[13]=z[13] + z[19] + z[17];
    z[13]=z[7]*z[27]*z[13];
    z[17]= - static_cast<T>(1)- z[15];
    z[17]=z[17]*z[14];
    z[17]=z[17] + z[58];
    z[17]=z[4]*z[17];
    z[19]=static_cast<T>(1)+ z[6];
    z[19]=z[6]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[19]*z[63];
    z[17]=z[19] + z[17];
    z[17]=z[4]*z[17];
    z[17]= - n<T>(1,2)*z[35] + z[17];
    z[17]=z[4]*z[17];
    z[17]=n<T>(1,2)*z[37] + z[17];
    z[19]=z[46]*z[14];
    z[19]=z[19] + z[34];
    z[19]=z[4]*z[19];
    z[23]= - z[28]*z[29];
    z[19]=z[23] + z[19];
    z[19]=z[4]*z[19];
    z[19]=z[31] + z[19];
    z[19]=z[4]*z[19];
    z[19]= - z[22] + z[19];
    z[19]=z[19]*z[27]*npow(z[7],2);
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[17]=z[8]*z[17];
    z[19]= - n<T>(7,2) + z[6];
    z[19]=z[19]*z[11];
    z[23]=5*z[14] - z[36];
    z[23]=z[23]*z[25];
    z[19]=z[19] + z[23];
    z[19]=z[4]*z[19];
    z[23]=n<T>(3,2) - z[6];
    z[23]=z[6]*z[23];
    z[19]=z[23] + z[19];
    z[19]=z[4]*z[19];
    z[19]= - n<T>(1,4) + z[19];
    z[19]=z[4]*z[19];
    z[19]=z[45] + n<T>(1,4) + z[19];
    z[13]=n<T>(1,2)*z[17] + n<T>(1,4)*z[19] + z[13];
    z[13]=z[7]*z[13];
    z[14]= - z[18]*z[14];
    z[14]=z[14] - z[58];
    z[14]=z[4]*z[14];
    z[17]=static_cast<T>(1)- z[57];
    z[17]=z[6]*z[17];
    z[17]= - n<T>(7,2) + z[17];
    z[11]=z[17]*z[11];
    z[11]=n<T>(1,4)*z[11] + z[14];
    z[11]=z[4]*z[11];
    z[14]= - static_cast<T>(1)- z[49];
    z[14]=z[6]*z[14];
    z[14]= - n<T>(1,2) + z[14];
    z[14]=z[6]*z[14];
    z[14]=n<T>(1,2) + z[14];
    z[14]=z[14]*z[15];
    z[11]=z[14] + z[11];
    z[11]=z[4]*z[11];
    z[14]= - n<T>(1,2) - z[6];
    z[14]=z[6]*z[14];
    z[14]=n<T>(3,4) + z[14];
    z[14]=z[6]*z[14];
    z[14]=n<T>(1,2) + z[14];
    z[14]=z[14]*z[15];
    z[11]=z[14] + z[11];
    z[11]=n<T>(1,2)*z[11] + z[13];
    z[11]=z[8]*z[11];
    z[13]= - static_cast<T>(7)+ z[15];
    z[13]=z[6]*z[13];
    z[13]=n<T>(25,2) + z[13];
    z[13]=z[6]*z[13];
    z[13]= - n<T>(25,4) + z[13];
    z[13]=z[13]*z[22];
    z[14]= - n<T>(3,8) - z[31];
    z[14]=z[6]*z[14];
    z[14]=z[14] - n<T>(1,8)*z[59];
    z[14]=z[4]*z[14];
    z[13]=z[13] + z[14];
    z[13]=z[4]*z[13];
    z[14]= - static_cast<T>(9)+ z[6];
    z[14]=z[6]*z[14];
    z[14]=static_cast<T>(19)+ z[14];
    z[14]=z[6]*z[14];
    z[14]= - n<T>(27,2) + z[14];
    z[14]=z[14]*z[56];
    z[17]=z[40]*z[59];
    z[17]=z[17] + z[44];
    z[17]=z[17]*z[25];
    z[17]=z[17] - n<T>(3,4) + z[47];
    z[17]=z[3]*z[17];
    z[13]=z[17] + z[14] + z[13];
    z[13]=z[3]*z[13];
    z[14]=static_cast<T>(7)+ z[24];
    z[14]=z[14]*z[15];
    z[14]= - static_cast<T>(19)+ z[14];
    z[14]=z[6]*z[14];
    z[17]= - static_cast<T>(23)+ 13*z[4];
    z[17]=z[17]*z[25];
    z[14]=z[17] + n<T>(31,2) + z[14];
    z[14]=z[14]*z[68];
    z[17]= - static_cast<T>(3)- z[6];
    z[17]=z[17]*z[15];
    z[17]=static_cast<T>(15)+ z[17];
    z[17]=z[6]*z[17];
    z[17]= - static_cast<T>(35)+ z[17];
    z[17]=z[17]*z[15];
    z[17]=static_cast<T>(11)+ z[17];
    z[18]= - n<T>(23,8) + z[4];
    z[18]=z[4]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[17]=z[17]*z[70];
    z[18]= - static_cast<T>(3)+ z[15];
    z[18]=z[6]*z[18];
    z[18]=n<T>(15,2) + z[18];
    z[15]=z[18]*z[15];
    z[15]= - static_cast<T>(5)+ z[15];
    z[15]=z[6]*z[15];
    z[18]=z[4]*z[71];
    z[15]=z[18] + n<T>(5,2) + z[15];
    z[18]=npow(z[3],4);
    z[15]=z[2]*z[15]*z[18];
    z[15]=z[17] + z[15];
    z[15]=z[2]*z[15];
    z[14]=n<T>(1,4)*z[14] + z[15];
    z[14]=z[2]*z[14];
    z[15]= - static_cast<T>(1)- z[39];
    z[15]=z[6]*z[15];
    z[15]= - n<T>(7,2) + z[15];
    z[15]=z[15]*z[22];
    z[17]= - static_cast<T>(9)+ 11*z[4];
    z[17]=z[4]*z[17];
    z[15]=n<T>(1,8)*z[17] + static_cast<T>(1)+ z[15];
    z[15]=z[3]*z[15];
    z[14]=z[15] + z[14];
    z[14]=z[2]*z[14];
    z[15]= - static_cast<T>(1)+ 5*z[4];
    z[15]=z[15]*z[25];
    z[17]=z[6]*z[40];
    z[17]= - n<T>(1,2) + z[17];
    z[17]=z[6]*z[17];
    z[15]=z[15] + n<T>(1,2) + z[17];
    z[14]=n<T>(1,4)*z[15] + z[14];
    z[14]=z[2]*z[14];
    z[15]=z[8]*z[7];
    z[15]=z[15] - 1;
    z[15]=z[41]*z[15];
    z[15]=n<T>(1,2)*z[27] + z[15];
    z[15]=z[15]*z[21];
    z[14]=z[14] + z[15];
    z[14]=z[5]*z[14];
    z[15]=n<T>(3,4) - z[3];
    z[15]=z[15]*z[70];
    z[17]=z[18]*z[45];
    z[15]=z[15] + z[17];
    z[15]=z[2]*z[15];
    z[17]= - n<T>(3,2) + z[3];
    z[17]=z[3]*z[17];
    z[17]=n<T>(1,2) + z[17];
    z[17]=z[17]*z[68];
    z[15]=n<T>(3,2)*z[17] + z[15];
    z[15]=z[2]*z[15];
    z[17]=n<T>(9,4) - z[3];
    z[17]=z[3]*z[17];
    z[17]= - n<T>(5,4) + z[17];
    z[17]=z[3]*z[17];
    z[17]=n<T>(1,4) + z[17];
    z[17]=z[3]*z[17];
    z[15]=z[17] + z[15];
    z[15]=z[2]*z[15];
    z[17]= - static_cast<T>(3)+ z[3];
    z[17]=z[3]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[65];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[65];
    z[15]=z[17] + z[15];
    z[15]=z[2]*z[15];
    z[17]=static_cast<T>(1)+ z[3];
    z[17]=z[17]*z[26];
    z[15]=z[17] + z[15];
    z[15]=z[1]*z[15];

    r += z[11] + n<T>(1,2)*z[12] + z[13] + z[14] + z[15] + z[16] + z[20] + 
      z[30];
 
    return r;
}

template double qg_2lha_r1577(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1577(const std::array<dd_real,30>&);
#endif
