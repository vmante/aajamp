#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r554(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[9];
    z[7]=k[3];
    z[8]=k[13];
    z[9]=k[6];
    z[10]=k[21];
    z[11]=npow(z[5],2);
    z[12]=npow(z[3],2);
    z[13]=z[11] + n<T>(35,4)*z[12];
    z[14]=z[6]*z[5];
    z[15]=n<T>(1,2)*z[6];
    z[16]=z[15] - 1;
    z[17]= - z[5] + z[16];
    z[17]=z[17]*z[14];
    z[18]=z[11] - z[14];
    z[18]=z[4]*z[18]*z[15];
    z[17]=z[18] + z[17] + z[13];
    z[18]=5*z[3];
    z[19]=n<T>(5,2) - z[3];
    z[19]=z[19]*z[18];
    z[20]=npow(z[6],2);
    z[21]= - 15*z[12] + z[20];
    z[22]=n<T>(1,2)*z[4];
    z[21]=z[21]*z[22];
    z[16]=z[16]*z[6];
    z[19]=z[21] + z[19] - z[16];
    z[21]=n<T>(5,8)*z[3];
    z[23]= - static_cast<T>(1)- z[21];
    z[24]=static_cast<T>(5)- z[6];
    z[24]=z[6]*z[24];
    z[25]= - 15*z[3] + z[16];
    z[25]=z[4]*z[25];
    z[23]=n<T>(1,4)*z[25] + 3*z[23] + n<T>(1,8)*z[24];
    z[24]=z[6] - 1;
    z[25]=n<T>(1,4)*z[6];
    z[26]=z[25]*z[4];
    z[27]=z[24]*z[26];
    z[28]= - n<T>(21,8) + z[6];
    z[28]=z[6]*z[28];
    z[27]=z[27] + n<T>(9,4) + z[28];
    z[27]=z[2]*z[27];
    z[23]=n<T>(1,2)*z[23] + z[27];
    z[23]=z[2]*z[23];
    z[19]=n<T>(1,8)*z[19] + z[23];
    z[19]=z[2]*z[19];
    z[11]=z[25]*z[11]*z[24];
    z[23]=static_cast<T>(5)- z[3];
    z[23]=z[23]*z[18];
    z[24]=z[24]*z[6];
    z[27]=z[4]*z[12];
    z[23]= - 5*z[27] + z[23] - z[24];
    z[27]= - z[4] - 1;
    z[28]=n<T>(5,2)*z[3];
    z[27]=z[28]*z[27];
    z[27]= - z[16] - static_cast<T>(3)+ z[27];
    z[27]=z[2]*z[27];
    z[23]=n<T>(1,4)*z[23] + z[27];
    z[23]=z[2]*z[23];
    z[23]=n<T>(15,4)*z[12] + z[23];
    z[23]=z[2]*z[23];
    z[27]=z[2]*z[3];
    z[12]=n<T>(1,2)*z[12] + z[27];
    z[12]=z[1]*z[12]*npow(z[2],2);
    z[11]=n<T>(5,2)*z[12] + z[11] + z[23];
    z[11]=z[1]*z[11];
    z[11]=n<T>(1,4)*z[11] + n<T>(1,8)*z[17] + z[19];
    z[11]=z[1]*z[11];
    z[12]=static_cast<T>(31)- 13*z[6];
    z[12]=z[12]*z[15];
    z[15]=z[22]*z[20];
    z[17]=n<T>(19,2) - 7*z[6];
    z[17]=z[6]*z[17];
    z[17]=z[17] - z[15];
    z[17]=z[4]*z[17];
    z[12]=z[17] - static_cast<T>(9)+ z[12];
    z[12]=z[2]*z[12];
    z[17]= - n<T>(29,2) + 5*z[6];
    z[17]=z[17]*z[25];
    z[19]= - static_cast<T>(1)+ n<T>(7,2)*z[6];
    z[19]=z[19]*z[26];
    z[12]=n<T>(1,4)*z[12] + z[19] + static_cast<T>(3)+ z[17];
    z[12]=z[2]*z[12];
    z[17]=z[6] - static_cast<T>(9)- z[28];
    z[19]= - n<T>(35,4)*z[3] - z[6];
    z[19]=z[4]*z[19];
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[12]=n<T>(1,4)*z[17] + z[12];
    z[12]=z[2]*z[12];
    z[17]= - n<T>(3,4) + z[7];
    z[17]=z[3]*z[17];
    z[17]=n<T>(5,4) + z[17];
    z[17]=z[17]*z[18];
    z[18]=z[5]*z[7];
    z[19]=static_cast<T>(1)+ z[18];
    z[19]=z[5]*z[19];
    z[17]= - z[14] + z[17] + n<T>(3,4)*z[19];
    z[13]= - n<T>(1,2)*z[13] + z[14];
    z[13]=z[4]*z[13];
    z[13]=n<T>(1,2)*z[17] + z[13];
    z[11]=z[11] + n<T>(1,4)*z[13] + z[12];
    z[11]=z[1]*z[11];
    z[12]=n<T>(1,2)*z[7];
    z[13]=z[12]*npow(z[10],2);
    z[13]=z[13] + z[10];
    z[14]=n<T>(1,4)*z[7];
    z[13]=z[13]*z[14];
    z[14]=z[10]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[3]*z[14]*z[12];
    z[14]=z[14] - static_cast<T>(1)+ z[13];
    z[14]=z[3]*z[14];
    z[13]= - z[9]*z[13];
    z[13]=z[13] + z[14];
    z[14]= - z[8] + n<T>(1,4)*z[9];
    z[17]=z[14]*z[12];
    z[19]= - n<T>(1,2) - z[9];
    z[17]=z[17] + n<T>(1,4)*z[19] + z[8];
    z[17]=z[7]*z[17];
    z[19]=z[9] + 1;
    z[19]= - z[8] + n<T>(1,4)*z[19];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[5]*z[17];
    z[14]=z[7]*z[14];
    z[14]=z[17] + z[14] - z[19];
    z[17]=3*z[5];
    z[14]=z[14]*z[17];
    z[13]=5*z[13] + z[14];
    z[13]=z[4]*z[13];
    z[14]=z[8] - n<T>(5,4);
    z[19]=z[7]*z[8];
    z[23]= - n<T>(5,4) - z[19];
    z[23]=z[7]*z[23];
    z[23]=z[23] + z[14];
    z[12]= - z[12] + 1;
    z[12]=z[8]*z[12];
    z[12]= - n<T>(5,8) + z[12];
    z[12]=z[7]*z[12];
    z[12]= - n<T>(1,2)*z[14] + z[12];
    z[12]=z[12]*z[18];
    z[12]=n<T>(1,2)*z[23] + z[12];
    z[12]=z[12]*z[17];
    z[14]= - static_cast<T>(1)+ z[7];
    z[14]=z[3]*z[14];
    z[14]=z[14] + 1;
    z[14]=z[7]*z[14];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[14]*z[21];
    z[17]=z[19] - z[8];
    z[12]=z[13] + z[12] + z[14] + n<T>(5,2) + 3*z[17];
    z[13]=static_cast<T>(7)- 3*z[6];
    z[13]=z[13]*z[25];
    z[14]=z[20]*z[4];
    z[17]=static_cast<T>(3)- n<T>(5,2)*z[6];
    z[17]=z[6]*z[17];
    z[17]=z[17] - z[14];
    z[17]=z[17]*z[22];
    z[15]=z[24] + z[15];
    z[15]=z[4]*z[15];
    z[15]=z[15] + n<T>(1,2) + z[16];
    z[15]=z[2]*z[15];
    z[13]=z[15] + z[17] - static_cast<T>(1)+ z[13];
    z[13]=z[2]*z[13];
    z[15]= - static_cast<T>(1)+ n<T>(3,4)*z[6];
    z[15]=z[6]*z[15];
    z[13]=3*z[13] + n<T>(3,4)*z[14] + n<T>(1,4) + z[15];
    z[13]=z[2]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[11]=n<T>(1,2)*z[12] + z[11];

    r += n<T>(1,2)*z[11];
 
    return r;
}

template double qg_2lha_r554(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r554(const std::array<dd_real,30>&);
#endif
