#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2140(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=z[4]*z[6];
    z[8]= - static_cast<T>(3)- z[5];
    z[8]=z[6]*z[8];
    z[8]=n<T>(221,6) + z[8];
    z[8]=z[8]*z[7];
    z[9]=z[6]*z[5];
    z[8]=z[8] + n<T>(221,6) - z[9];
    z[10]= - n<T>(1583,2) - 221*z[1];
    z[11]=z[4] - 1;
    z[12]=n<T>(1,2)*z[1] - z[11];
    z[12]=z[2]*z[12];
    z[10]=n<T>(1,2)*z[10] + 221*z[12];
    z[12]=n<T>(1,9)*z[2];
    z[10]=z[10]*z[12];
    z[8]=n<T>(1,2)*z[8] + z[10];
    z[10]=static_cast<T>(1)+ z[5];
    z[10]=z[6]*z[10];
    z[10]= - n<T>(221,18) + z[10];
    z[7]=z[10]*z[7];
    z[7]=n<T>(1,2)*z[7] - n<T>(221,36) + z[9];
    z[7]=z[4]*z[7];
    z[9]= - z[1] + z[11];
    z[9]=z[2]*z[9];
    z[9]=n<T>(221,4)*z[9] + static_cast<T>(115)+ n<T>(221,2)*z[1];
    z[9]=z[9]*z[12];
    z[10]= - n<T>(221,18)*z[1] - n<T>(239,18) + z[5];
    z[7]=z[9] + n<T>(1,2)*z[10] + z[7];
    z[7]=z[3]*z[7];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[8]= - n<T>(221,18) + z[6];
    z[8]=z[6]*z[8];
    z[9]=z[2]*z[11];
    z[9]=n<T>(1,2) + n<T>(1,3)*z[9];
    z[9]=z[2]*z[9];
    z[8]=z[8] + n<T>(221,3)*z[9];
    z[7]=n<T>(1,4)*z[8] + z[7];

    r += n<T>(1,4)*z[7]*z[3];
 
    return r;
}

template double qg_2lha_r2140(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2140(const std::array<dd_real,30>&);
#endif
