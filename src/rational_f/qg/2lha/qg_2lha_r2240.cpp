#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2240(const std::array<T,30>& k) {
  T z[22];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[2];
    z[7]=k[11];
    z[8]=k[3];
    z[9]=k[10];
    z[10]=k[9];
    z[11]=k[4];
    z[12]=k[21];
    z[13]=k[14];
    z[14]=n<T>(5,3)*z[7];
    z[15]= - static_cast<T>(1)+ z[11];
    z[15]=z[2]*z[15];
    z[15]=z[4] + z[15];
    z[15]=z[15]*z[14];
    z[16]=z[11]*z[12];
    z[17]=n<T>(1,3) - z[4];
    z[18]=static_cast<T>(11)+ z[11];
    z[18]=z[2]*z[18];
    z[15]=z[15] + n<T>(1,6)*z[18] + 5*z[17] - n<T>(3,2)*z[16];
    z[15]=z[7]*z[15];
    z[17]=n<T>(1,2) - z[8];
    z[18]=z[9]*z[8];
    z[17]=n<T>(5,4)*z[1] + n<T>(1,4)*z[17] + z[18];
    z[17]=z[3]*z[17];
    z[18]=n<T>(3,8)*z[12];
    z[19]=static_cast<T>(1)- n<T>(1,2)*z[1];
    z[19]=z[4]*z[19];
    z[20]= - z[18] + n<T>(1,3)*z[9];
    z[20]=z[20]*z[11];
    z[21]=z[2]*z[20];
    z[15]=n<T>(1,4)*z[15] + z[21] + n<T>(1,3)*z[17] + n<T>(5,6)*z[19] + n<T>(2,3)*z[9]
    - z[18];
    z[15]=z[5]*z[15];
    z[17]=z[11] + 1;
    z[17]=z[18]*z[17];
    z[18]=n<T>(3,4)*z[3] - n<T>(1,12)*z[11] - n<T>(7,3) - n<T>(1,4)*z[4];
    z[18]=z[2]*z[18];
    z[19]=n<T>(1,2) - n<T>(1,3)*z[11];
    z[19]=z[2]*z[19];
    z[19]= - n<T>(1,3) + z[19];
    z[19]=z[7]*z[19];
    z[17]=n<T>(5,2)*z[19] + n<T>(1,2)*z[18] + n<T>(1,3) + z[17];
    z[17]=z[7]*z[17];
    z[18]=z[1] - 1;
    z[19]=z[4]*z[18];
    z[21]= - static_cast<T>(3)+ z[1];
    z[21]=z[3]*z[21];
    z[19]=z[19] - z[21];
    z[19]= - z[20] - n<T>(1,8)*z[19];
    z[19]=z[2]*z[19];
    z[20]= - z[9] - n<T>(1,2)*z[3];
    z[15]=z[15] + z[17] + n<T>(1,3)*z[20] + z[19];
    z[15]=z[5]*z[15];
    z[17]=n<T>(3,2)*z[10];
    z[16]= - z[16]*z[17];
    z[17]= - n<T>(1,6) - z[13] - n<T>(3,2)*z[12];
    z[17]=z[10]*z[17];
    z[19]=z[3]*z[10];
    z[20]=z[13]*z[19];
    z[21]= - n<T>(3,2)*z[3] - 1;
    z[21]=z[13]*z[21];
    z[21]=n<T>(17,6) + z[21];
    z[21]=z[2]*z[21];
    z[16]=z[21] - n<T>(3,2)*z[20] + z[17] + z[16];
    z[17]= - static_cast<T>(1)+ z[8];
    z[17]=z[10]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[20]=z[11] - z[6];
    z[20]= - static_cast<T>(1)+ n<T>(1,2)*z[20];
    z[20]=z[2]*z[20];
    z[17]=n<T>(1,2)*z[17] + z[20];
    z[14]=z[17]*z[14];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[7]*z[14];
    z[16]=npow(z[13],2);
    z[17]=z[16]*z[19];
    z[19]= - z[4]*z[1];
    z[18]=z[13] + z[18];
    z[18]=z[3]*z[18];
    z[18]=z[19] + z[18];
    z[18]=z[2]*z[18];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[3]*z[16];
    z[16]=z[16] + z[18];
    z[16]=z[2]*z[16];
    z[16]=z[17] + z[16];
    z[14]=n<T>(1,6)*z[16] + z[14];

    r += n<T>(1,2)*z[14] + z[15];
 
    return r;
}

template double qg_2lha_r2240(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2240(const std::array<dd_real,30>&);
#endif
