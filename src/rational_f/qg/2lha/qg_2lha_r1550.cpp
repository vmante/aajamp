#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1550(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[3];
    z[8]=k[9];
    z[9]=k[11];
    z[10]=3*z[5];
    z[11]=z[7] - 1;
    z[12]= - z[10] + z[1] + z[11];
    z[13]=n<T>(1,2)*z[5];
    z[12]=z[12]*z[13];
    z[14]=n<T>(3,2)*z[7];
    z[15]=z[1] - 1;
    z[16]=z[5] - n<T>(1,2)*z[15] - z[7];
    z[16]=z[5]*z[16];
    z[16]= - z[14] + z[16];
    z[16]=z[6]*z[5]*z[16];
    z[12]=z[16] - z[7] + z[12];
    z[12]=z[6]*z[12];
    z[16]=n<T>(1,2)*z[1];
    z[17]=z[16] - 1;
    z[18]=z[17]*z[16];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[3]*z[18];
    z[18]= - static_cast<T>(3)+ z[18];
    z[18]=z[5]*z[18];
    z[19]=n<T>(7,4)*z[7];
    z[20]= - z[4]*z[19];
    z[12]=z[12] + z[18] + z[20] + n<T>(1,4)*z[9] + z[15];
    z[15]=n<T>(1,2)*z[6];
    z[12]=z[12]*z[15];
    z[18]=n<T>(1,2)*z[9];
    z[20]=z[18] - 1;
    z[21]= - z[2]*z[20];
    z[18]=z[21] + z[1] - z[18];
    z[21]=static_cast<T>(7)- z[1];
    z[17]= - z[2]*z[17];
    z[17]=n<T>(1,2)*z[21] + z[17];
    z[17]=z[4]*z[17];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[18]=z[2] - 1;
    z[21]=n<T>(1,2)*z[4];
    z[22]=z[18]*z[21];
    z[23]= - static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[24]=z[23]*z[1];
    z[24]=z[24] + n<T>(11,4);
    z[24]=z[24]*z[22];
    z[16]=static_cast<T>(3)- z[16];
    z[16]=z[1]*z[16];
    z[16]= - n<T>(19,2) + z[16];
    z[23]= - z[2]*z[23];
    z[16]=z[24] + n<T>(1,4)*z[16] + z[23];
    z[16]=z[3]*z[16];
    z[23]=n<T>(1,8)*z[7];
    z[24]=static_cast<T>(3)+ z[2];
    z[24]=z[4]*z[24];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[23];
    z[25]=n<T>(1,4)*z[5];
    z[26]=static_cast<T>(1)+ n<T>(3,2)*z[9];
    z[27]= - z[3] - z[26];
    z[27]=z[27]*z[25];
    z[12]=z[12] + z[27] + z[24] + n<T>(1,2)*z[17] + z[16];
    z[16]=npow(z[7],2);
    z[17]= - n<T>(9,4) + z[7];
    z[17]=z[17]*z[16];
    z[24]=n<T>(1,2)*z[7];
    z[27]=z[24] - 3;
    z[28]=z[27]*z[16];
    z[29]=n<T>(3,4)*z[16];
    z[30]= - z[5]*z[29];
    z[28]=z[28] + z[30];
    z[28]=z[28]*z[13];
    z[17]=z[17] + z[28];
    z[17]=z[5]*z[17];
    z[28]=z[11]*z[16];
    z[17]=n<T>(3,2)*z[28] + z[17];
    z[17]=z[5]*z[17];
    z[28]=static_cast<T>(1)+ n<T>(1,4)*z[4];
    z[28]=z[7]*z[28];
    z[28]= - n<T>(3,8) + z[28];
    z[28]=z[28]*z[16];
    z[17]=z[28] + z[17];
    z[17]=z[6]*z[17];
    z[28]= - static_cast<T>(15)- n<T>(11,2)*z[4];
    z[28]=z[28]*z[24];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=z[28]*z[24];
    z[30]=z[14] - 1;
    z[31]=z[30]*z[7];
    z[32]=z[5]*z[7];
    z[32]=z[31] - z[32];
    z[32]=z[5]*z[32];
    z[33]=static_cast<T>(3)- z[7];
    z[33]=z[7]*z[33];
    z[32]=z[33] + z[32];
    z[32]=z[32]*z[25];
    z[33]=3*z[7];
    z[34]=n<T>(5,4) - z[33];
    z[34]=z[7]*z[34];
    z[32]=z[34] + z[32];
    z[32]=z[5]*z[32];
    z[17]=z[17] + z[28] + z[32];
    z[17]=z[6]*z[17];
    z[28]=z[13] + 1;
    z[32]=z[28] - z[24];
    z[32]=z[32]*z[5];
    z[34]= - z[32] + z[11];
    z[34]=z[5]*z[34];
    z[34]=z[34] - static_cast<T>(1)+ 11*z[7];
    z[34]=z[5]*z[34];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[7]*z[21];
    z[21]=z[34] - n<T>(1,2) + 21*z[21];
    z[17]=n<T>(1,4)*z[21] + z[17];
    z[17]=z[6]*z[17];
    z[21]= - z[32] - 1;
    z[21]=z[9]*z[21];
    z[32]=z[7]*z[9];
    z[21]=z[32] + z[21];
    z[21]=z[5]*z[21];
    z[34]=3*z[3];
    z[21]=z[21] - static_cast<T>(5)+ z[34];
    z[21]=z[21]*z[25];
    z[25]= - static_cast<T>(7)- n<T>(17,4)*z[4];
    z[18]=z[18]*z[4];
    z[35]= - n<T>(5,8)*z[18] + n<T>(13,8) - z[2];
    z[35]=z[3]*z[35];
    z[17]=z[17] + z[21] + n<T>(1,2)*z[25] + z[35];
    z[21]=npow(z[7],3);
    z[25]= - static_cast<T>(5)- z[4];
    z[25]=z[25]*z[21];
    z[35]=5*z[21];
    z[21]= - z[5]*z[21];
    z[21]= - z[35] + z[21];
    z[21]=z[21]*z[13];
    z[21]= - z[35] + z[21];
    z[21]=z[5]*z[21];
    z[21]= - z[35] + z[21];
    z[21]=z[5]*z[21];
    z[21]=n<T>(1,2)*z[25] + z[21];
    z[21]=z[21]*z[15];
    z[25]=n<T>(15,4) + z[4];
    z[25]=z[25]*z[16];
    z[35]=npow(z[5],2);
    z[36]=z[16]*z[35];
    z[37]=5*z[16];
    z[36]=z[37] - n<T>(1,2)*z[36];
    z[36]=z[36]*z[13];
    z[36]=z[37] + z[36];
    z[36]=z[5]*z[36];
    z[21]=z[21] + z[25] + z[36];
    z[21]=z[6]*z[21];
    z[25]= - n<T>(17,2) - 3*z[4];
    z[23]=z[25]*z[23];
    z[25]=z[13]*z[7];
    z[36]= - z[7] - z[25];
    z[36]=z[5]*z[36];
    z[36]= - z[33] + z[36];
    z[37]=n<T>(1,8)*z[5];
    z[36]=z[36]*z[37];
    z[36]= - z[7] + z[36];
    z[36]=z[5]*z[36];
    z[21]=n<T>(1,4)*z[21] + z[23] + z[36];
    z[21]=z[6]*z[21];
    z[23]= - z[35]*z[32]*z[28];
    z[23]=z[23] + static_cast<T>(3)- z[3];
    z[23]=z[23]*z[13];
    z[22]=z[22] - n<T>(3,2) + z[2];
    z[22]=z[3]*z[22];
    z[22]=z[23] + n<T>(1,2)*z[22] + n<T>(9,4) + z[4];
    z[21]=n<T>(1,4)*z[22] + z[21];
    z[21]=z[8]*z[21];
    z[17]=n<T>(1,2)*z[17] + z[21];
    z[17]=z[8]*z[17];
    z[21]= - z[7]*z[27];
    z[21]= - n<T>(1,2) + z[21];
    z[21]=z[21]*z[33];
    z[22]= - n<T>(3,2) + z[7];
    z[22]=z[7]*z[22];
    z[22]=z[22] - z[25];
    z[10]=z[22]*z[10];
    z[22]=static_cast<T>(9)- z[24];
    z[22]=z[7]*z[22];
    z[22]= - n<T>(9,2) + z[22];
    z[22]=z[7]*z[22];
    z[10]=z[22] + z[10];
    z[10]=z[5]*z[10];
    z[10]=z[21] + z[10];
    z[10]=z[5]*z[10];
    z[21]= - static_cast<T>(3)- z[4];
    z[21]=z[21]*z[24];
    z[21]=static_cast<T>(3)+ z[21];
    z[16]=z[21]*z[16];
    z[10]=z[16] + z[10];
    z[10]=z[10]*z[15];
    z[16]=5*z[4];
    z[21]=n<T>(17,2) + z[16];
    z[21]=z[7]*z[21];
    z[21]= - static_cast<T>(5)+ z[21];
    z[21]=z[21]*z[24];
    z[22]=z[28] - z[33];
    z[23]= - z[5]*z[22];
    z[23]=z[23] - n<T>(1,2) - z[31];
    z[23]=z[23]*z[13];
    z[25]= - n<T>(7,2) + z[7];
    z[25]=z[7]*z[25];
    z[23]=z[25] + z[23];
    z[23]=z[5]*z[23];
    z[10]=z[10] + z[21] + z[23];
    z[10]=z[6]*z[10];
    z[19]=z[13] + z[19] + z[20];
    z[19]=z[5]*z[19];
    z[20]= - z[14] - static_cast<T>(1)+ z[3];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[5]*z[19];
    z[20]= - static_cast<T>(13)- 11*z[4];
    z[20]=z[7]*z[20];
    z[19]=z[19] + static_cast<T>(1)+ n<T>(5,4)*z[20];
    z[10]=n<T>(1,2)*z[19] + z[10];
    z[10]=z[6]*z[10];
    z[19]=z[5]*z[9];
    z[20]=z[9]*z[14];
    z[20]=z[20] + z[19];
    z[20]=z[5]*z[20];
    z[20]=z[20] + static_cast<T>(3)- 7*z[3];
    z[20]=z[5]*z[20];
    z[21]=n<T>(21,2)*z[18] - n<T>(47,2) + 13*z[2];
    z[21]=z[3]*z[21];
    z[20]=z[20] + z[21] + n<T>(63,2) + 29*z[4];
    z[10]=n<T>(1,4)*z[20] + z[10];
    z[10]=n<T>(1,4)*z[10] + z[17];
    z[10]=z[8]*z[10];
    z[17]=n<T>(1,4)*z[7];
    z[20]=static_cast<T>(1)- z[17];
    z[20]=z[20]*z[33];
    z[21]= - z[22]*z[13];
    z[20]=z[21] - n<T>(1,4) + z[20];
    z[20]=z[5]*z[20];
    z[11]= - z[11]*z[14];
    z[11]=z[11] + z[20];
    z[11]=z[5]*z[11];
    z[11]= - z[29] + z[11];
    z[11]=z[11]*z[15];
    z[14]= - static_cast<T>(1)- n<T>(3,2)*z[4];
    z[14]=z[14]*z[17];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[7]*z[14];
    z[17]=static_cast<T>(1)+ z[24];
    z[17]=z[7]*z[17];
    z[17]=n<T>(1,2) + z[17];
    z[20]=n<T>(3,4)*z[5] - z[30];
    z[20]=z[5]*z[20];
    z[17]=n<T>(1,2)*z[17] + z[20];
    z[13]=z[17]*z[13];
    z[11]=z[11] + z[14] + z[13];
    z[11]=z[11]*z[15];
    z[13]= - z[34] + static_cast<T>(11)- z[9];
    z[13]=n<T>(7,4)*z[5] + n<T>(1,2)*z[13] - z[33];
    z[13]=z[13]*z[37];
    z[14]=n<T>(3,8) + z[4];
    z[14]=z[7]*z[14];
    z[11]=z[11] + z[13] + n<T>(1,32) + z[14];
    z[11]=z[6]*z[11];
    z[13]= - z[9]*z[24];
    z[13]=n<T>(3,8)*z[19] + z[13] + n<T>(1,2)*z[26] + z[3];
    z[13]=z[5]*z[13];
    z[14]=static_cast<T>(23)- 11*z[2];
    z[14]=n<T>(1,4)*z[14] - 3*z[18];
    z[14]=z[3]*z[14];
    z[15]= - static_cast<T>(3)- z[16];
    z[13]=z[13] + n<T>(5,4)*z[15] + z[14];
    z[10]=z[10] + n<T>(1,4)*z[13] + z[11];
    z[10]=z[8]*z[10];

    r += z[10] + n<T>(1,4)*z[12];
 
    return r;
}

template double qg_2lha_r1550(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1550(const std::array<dd_real,30>&);
#endif
