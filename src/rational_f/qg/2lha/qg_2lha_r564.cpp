#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r564(const std::array<T,30>& k) {
  T z[25];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[5];
    z[9]=k[9];
    z[10]=n<T>(1,2)*z[2];
    z[11]=5*z[6];
    z[12]=n<T>(5,2)*z[2] - n<T>(3,2) - z[11];
    z[12]=z[12]*z[10];
    z[13]=static_cast<T>(3)+ n<T>(5,4)*z[6];
    z[13]=z[6]*z[13];
    z[14]=z[10] - z[6];
    z[15]=z[4]*z[14];
    z[12]=n<T>(3,2)*z[15] + z[13] + z[12];
    z[12]=z[4]*z[12];
    z[13]=n<T>(1,2)*z[6];
    z[15]= - static_cast<T>(3)- z[11];
    z[15]=z[15]*z[13];
    z[16]=static_cast<T>(5)+ z[6];
    z[16]=z[16]*z[13];
    z[14]=z[2]*z[14];
    z[14]=z[16] + z[14];
    z[14]=z[2]*z[14];
    z[12]=z[12] + z[15] + z[14];
    z[12]=z[4]*z[12];
    z[14]=npow(z[2],2);
    z[15]=npow(z[6],2);
    z[16]= - z[15] + z[14];
    z[16]=z[2]*z[16];
    z[16]=n<T>(5,2)*z[15] + z[16];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[12]=z[8]*z[12];
    z[16]=z[6] + n<T>(1,2);
    z[17]=z[16]*z[6];
    z[18]= - n<T>(1,2) + z[17];
    z[19]=n<T>(11,2) - z[6];
    z[19]=n<T>(1,2)*z[19] - z[2];
    z[19]=z[2]*z[19];
    z[20]=z[6] + n<T>(3,2);
    z[21]=n<T>(1,2)*z[20] - z[2];
    z[21]=z[4]*z[21];
    z[18]=z[21] + n<T>(3,2)*z[18] + z[19];
    z[18]=z[4]*z[18];
    z[19]=3*z[6];
    z[21]= - n<T>(5,2) - z[19];
    z[21]=z[21]*z[13];
    z[22]=z[6] + 1;
    z[23]=z[6]*z[22];
    z[24]=n<T>(3,2) - z[6];
    z[24]=z[2]*z[24];
    z[23]=z[23] + z[24];
    z[23]=z[2]*z[23];
    z[12]=z[12] + z[18] + z[21] + z[23];
    z[12]=z[8]*z[12];
    z[18]=z[20]*z[13];
    z[21]=z[4]*z[18];
    z[20]= - z[6]*z[20];
    z[20]=z[20] + z[21];
    z[20]=z[4]*z[20];
    z[21]=3*z[15];
    z[23]=z[4]*z[15];
    z[23]= - z[21] + z[23];
    z[23]=z[4]*z[23];
    z[21]=z[21] + z[23];
    z[21]=z[4]*z[21];
    z[21]= - z[15] + z[21];
    z[21]=z[8]*z[21];
    z[18]=n<T>(3,4)*z[21] + z[18] + z[20];
    z[18]=z[8]*z[18];
    z[20]=static_cast<T>(1)- z[4];
    z[21]=n<T>(3,2)*z[6];
    z[23]=z[21] - 1;
    z[20]=z[23]*z[20];
    z[20]= - n<T>(3,4)*z[1] + n<T>(1,2)*z[20];
    z[20]=z[6]*z[20];
    z[18]=z[18] + z[20];
    z[18]=z[7]*z[18];
    z[20]= - n<T>(1,4)*z[4] + n<T>(5,4)*z[2];
    z[19]= - n<T>(1,4) + z[19] - z[20];
    z[19]=z[4]*z[19];
    z[11]= - n<T>(11,4)*z[2] + n<T>(1,2) + z[11];
    z[11]=z[2]*z[11];
    z[11]=z[18] + z[12] + z[19] + z[11] + n<T>(3,4) - z[17];
    z[12]=static_cast<T>(1)+ z[21];
    z[17]= - n<T>(3,4)*z[2] + n<T>(1,4) + z[6];
    z[17]=z[2]*z[17];
    z[12]=n<T>(1,2)*z[12] + z[17];
    z[12]=z[2]*z[12];
    z[17]=z[22] - z[10];
    z[17]=z[17]*z[2];
    z[16]=z[17] - z[16];
    z[16]=z[2]*z[16];
    z[15]=n<T>(1,2)*z[15];
    z[16]= - z[15] + z[16];
    z[16]=z[3]*z[16];
    z[12]=z[16] - z[13] + z[12];
    z[12]=z[3]*z[12];
    z[10]=z[10] - 1;
    z[10]=z[10]*z[2];
    z[16]=z[6] - n<T>(1,2);
    z[18]=z[10] - z[16];
    z[18]=z[2]*z[18];
    z[18]=z[6] + z[18];
    z[18]=z[3]*z[18];
    z[19]=static_cast<T>(1)- z[6];
    z[21]=n<T>(7,4)*z[2] - n<T>(9,4) - z[6];
    z[21]=z[2]*z[21];
    z[18]=z[18] + n<T>(1,2)*z[19] + z[21];
    z[18]=z[3]*z[18];
    z[19]=static_cast<T>(1)- z[2];
    z[10]= - n<T>(1,2) - z[10];
    z[10]=z[3]*z[10];
    z[10]=n<T>(1,2)*z[19] + z[10];
    z[10]=z[3]*z[10];
    z[10]=n<T>(1,8) + z[10];
    z[10]=z[1]*z[10];
    z[10]=z[10] + z[18] - n<T>(3,4) - z[6] + z[20];
    z[10]=z[1]*z[10];
    z[18]=static_cast<T>(1)+ z[13];
    z[18]=z[6]*z[18];
    z[17]= - z[17] + n<T>(1,2) + z[18];
    z[17]=z[3]*z[17];
    z[18]= - z[6] + z[2] + 3;
    z[19]=z[2]*z[18];
    z[17]=z[17] + n<T>(1,4)*z[19] - z[22];
    z[17]=z[3]*z[2]*z[17];
    z[14]= - z[18]*z[14];
    z[18]= - z[8]*npow(z[2],3);
    z[14]=z[14] + z[18];
    z[14]=z[8]*z[14];
    z[14]=n<T>(1,4)*z[14] + z[17];
    z[14]=z[9]*z[14];
    z[17]= - z[4] - z[16];
    z[17]=z[4]*z[17];
    z[18]=n<T>(1,2)*z[4];
    z[19]=z[1]*z[18];
    z[17]=z[19] + z[13] + z[17];
    z[17]=z[1]*z[17];
    z[16]=z[18] + z[16];
    z[16]=z[4]*z[16];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[6]*z[13];
    z[13]=z[13] + z[16];
    z[13]=z[4]*z[13];
    z[13]=z[17] - z[15] + z[13];
    z[13]=z[5]*z[13];
    z[10]=n<T>(1,4)*z[13] + z[14] + z[10] + z[12] + n<T>(1,2)*z[11];

    r += n<T>(1,2)*z[10];
 
    return r;
}

template double qg_2lha_r564(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r564(const std::array<dd_real,30>&);
#endif
