#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r335(const std::array<T,30>& k) {
  T z[19];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[5];
    z[4]=k[10];
    z[5]=k[3];
    z[6]=k[12];
    z[7]=k[9];
    z[8]=k[11];
    z[9]=z[5] - 1;
    z[9]=z[9]*z[5];
    z[10]=npow(z[2],2);
    z[10]= - z[10] + z[8] + z[9];
    z[10]=z[6]*z[10];
    z[11]=3*z[5];
    z[12]=z[11] - 1;
    z[13]=n<T>(1,2)*z[2];
    z[14]=npow(z[8],2);
    z[10]=z[10] - z[13] - n<T>(1,2)*z[14] - z[12];
    z[10]=z[6]*z[10];
    z[11]=z[11] - 5;
    z[10]= - n<T>(1,2)*z[11] + z[10];
    z[10]=z[6]*z[10];
    z[10]=static_cast<T>(1)+ z[10];
    z[14]=z[8] - 1;
    z[15]=n<T>(1,2)*z[8];
    z[16]= - z[14]*z[15];
    z[17]=z[2] + z[5];
    z[18]=static_cast<T>(3)- z[8];
    z[18]=z[8]*z[18];
    z[17]= - static_cast<T>(1)+ z[18] + 5*z[17];
    z[17]=z[17]*z[13];
    z[18]=n<T>(3,2)*z[5];
    z[16]=z[17] + z[18] - static_cast<T>(1)+ z[16];
    z[14]= - z[9] + z[14];
    z[17]= - static_cast<T>(1)- z[5];
    z[17]=z[5]*z[17];
    z[17]=z[8] + z[17];
    z[13]= - z[5] - z[13];
    z[13]=z[2]*z[13];
    z[13]=n<T>(1,2)*z[17] + z[13];
    z[13]=z[2]*z[13];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[13]=z[6]*z[13];
    z[13]=n<T>(1,2)*z[16] + z[13];
    z[13]=z[6]*z[13];
    z[14]=z[15] - 1;
    z[14]=z[14]*z[8];
    z[15]=n<T>(3,2)*z[2] + z[18] - n<T>(1,2) - z[14];
    z[15]=z[2]*z[15];
    z[15]=n<T>(1,2)*z[12] + z[15];
    z[13]=n<T>(1,2)*z[15] + z[13];
    z[13]=z[6]*z[13];
    z[14]=z[14] + z[2] + n<T>(3,2) - z[1];
    z[14]=z[3]*z[14];
    z[14]= - n<T>(5,2) + z[14];
    z[14]=z[2]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[13]=z[4]*z[13];
    z[10]=n<T>(1,2)*z[10] + z[13];
    z[10]=z[4]*z[10];
    z[9]= - z[6]*z[9];
    z[9]=z[9] + z[12];
    z[9]=z[6]*z[7]*z[9];
    z[11]=z[7]*z[11];
    z[9]=n<T>(1,2)*z[11] + z[9];
    z[9]=z[6]*z[9];
    z[9]= - z[7] + z[9];
    z[9]=n<T>(1,2)*z[9] + z[10];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r335(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r335(const std::array<dd_real,30>&);
#endif
