#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r206(const std::array<T,30>& k) {
  T z[46];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[3];
    z[6]=k[20];
    z[7]=k[6];
    z[8]=k[11];
    z[9]=k[14];
    z[10]=k[9];
    z[11]=k[12];
    z[12]=k[4];
    z[13]=k[24];
    z[14]=z[4] + 1;
    z[15]=n<T>(1,2)*z[4];
    z[16]=z[14]*z[15];
    z[17]=n<T>(1,2)*z[2];
    z[18]=npow(z[4],2);
    z[19]=z[17]*z[18];
    z[20]=z[4] + n<T>(1,2);
    z[21]= - z[4]*z[20];
    z[21]=z[21] + z[19];
    z[21]=z[2]*z[21];
    z[16]=z[16] + z[21];
    z[21]=n<T>(1,2)*z[3];
    z[16]=z[16]*npow(z[2],3)*z[21];
    z[22]=5*z[4];
    z[23]=z[18]*z[2];
    z[24]= - z[22] + 3*z[23];
    z[24]=z[2]*z[24];
    z[24]=z[24] + z[14];
    z[24]=z[2]*z[24];
    z[25]=z[4] - 1;
    z[24]=z[24] + z[25];
    z[26]=z[15] - 1;
    z[27]=z[4]*z[26];
    z[27]=z[27] + z[19];
    z[27]=z[2]*z[27];
    z[25]=z[25]*z[4];
    z[28]=z[25] + 1;
    z[27]=n<T>(1,2)*z[28] + z[27];
    z[27]=z[5]*z[27];
    z[24]=n<T>(1,2)*z[24] + z[27];
    z[27]=n<T>(1,2)*z[5];
    z[24]=z[24]*z[27];
    z[29]=n<T>(3,4)*z[4];
    z[30]= - static_cast<T>(1)- z[29];
    z[30]=z[4]*z[30];
    z[30]=z[30] + n<T>(3,4)*z[23];
    z[30]=z[2]*z[30];
    z[29]=z[29] + z[30];
    z[30]=npow(z[2],2);
    z[29]=z[29]*z[30];
    z[16]=z[24] + z[29] + z[16];
    z[24]=5*z[8];
    z[16]=z[16]*z[24];
    z[29]=3*z[4];
    z[31]=z[26]*z[29];
    z[32]=n<T>(71,2) + 29*z[4];
    z[32]=z[32]*z[15];
    z[32]=z[32] - 19*z[23];
    z[33]=n<T>(1,3)*z[2];
    z[32]=z[32]*z[33];
    z[31]=z[32] + n<T>(11,12) + z[31];
    z[31]=z[2]*z[31];
    z[32]=static_cast<T>(5)+ n<T>(47,2)*z[4];
    z[32]=z[4]*z[32];
    z[32]=z[32] - n<T>(47,4)*z[23];
    z[32]=z[2]*z[32];
    z[34]= - static_cast<T>(5)- n<T>(47,4)*z[4];
    z[34]=z[4]*z[34];
    z[32]=z[34] + z[32];
    z[34]=n<T>(1,3)*z[3];
    z[30]=z[32]*z[30]*z[34];
    z[32]=11*z[4];
    z[35]=z[32] - n<T>(29,3)*z[23];
    z[17]=z[35]*z[17];
    z[35]=n<T>(11,2) - z[29];
    z[35]=z[4]*z[35];
    z[17]=z[17] - n<T>(11,3) + z[35];
    z[17]=z[17]*z[27];
    z[35]=npow(z[5],2);
    z[36]=z[35]*z[10];
    z[16]=z[16] - n<T>(3,2)*z[36] + z[17] + z[30] + n<T>(5,4) + z[31];
    z[16]=z[8]*z[16];
    z[17]=n<T>(1,2)*z[12];
    z[30]=z[10]*z[5];
    z[31]=static_cast<T>(1)+ z[30];
    z[31]=z[31]*z[17];
    z[37]=z[27] - 1;
    z[31]=z[31] + n<T>(3,2)*z[30] - z[37];
    z[31]=z[12]*z[31];
    z[38]=z[4] + 3;
    z[39]=z[38]*z[30];
    z[40]=z[15] + 1;
    z[41]=z[40]*z[5];
    z[41]=z[41] - n<T>(1,2);
    z[31]=z[31] + n<T>(1,2)*z[39] - z[41];
    z[31]=z[10]*z[31];
    z[42]=z[12] + 3;
    z[42]=z[42]*z[12];
    z[43]=z[30]*z[42];
    z[39]=z[39] + z[43];
    z[43]=n<T>(1,2)*z[13];
    z[39]=z[39]*z[43];
    z[31]=z[39] + z[31];
    z[31]=z[13]*z[31];
    z[17]= - z[17] + z[37];
    z[17]=z[12]*z[17];
    z[37]= - z[38] - z[42];
    z[37]=z[43]*z[5]*z[37];
    z[17]=z[37] + z[17] + z[41];
    z[17]=z[13]*z[17];
    z[37]=z[8]*z[5];
    z[38]=z[18]*z[5];
    z[39]= - z[4] - z[38];
    z[39]=z[39]*z[37];
    z[41]= - z[5]*z[4];
    z[39]=z[39] - static_cast<T>(1)+ z[41];
    z[39]=z[39]*z[24];
    z[39]=z[12] + static_cast<T>(1)+ z[39];
    z[17]=n<T>(1,2)*z[39] + z[17];
    z[17]=z[11]*z[17];
    z[39]= - static_cast<T>(25)- 13*z[4];
    z[39]=z[4]*z[39];
    z[39]=z[39] + 31*z[23];
    z[39]=z[2]*z[39];
    z[29]=n<T>(13,3) - z[29];
    z[29]=z[4]*z[29];
    z[17]=z[17] + z[31] + n<T>(1,6)*z[39] - n<T>(25,6) + z[29];
    z[29]=z[32] + n<T>(31,2);
    z[31]=static_cast<T>(1)+ n<T>(11,12)*z[4];
    z[31]=z[31]*z[2];
    z[29]= - z[31] + n<T>(1,12)*z[29];
    z[29]=n<T>(1,2)*z[29];
    z[39]=z[2]*z[4];
    z[41]= - n<T>(1,4)*z[39] + n<T>(1,3) + z[15];
    z[41]=z[2]*z[41];
    z[41]=z[41] - n<T>(1,3) - n<T>(1,4)*z[4];
    z[41]=z[3]*z[41];
    z[41]=z[29] + z[41];
    z[41]=z[3]*z[41];
    z[42]=static_cast<T>(1)+ n<T>(5,4)*z[4];
    z[39]= - z[39] + n<T>(7,3)*z[42];
    z[39]=z[39]*z[2];
    z[42]=static_cast<T>(13)+ n<T>(23,3)*z[4];
    z[39]=z[39] - n<T>(1,4)*z[42];
    z[42]=z[15]*z[2];
    z[43]=z[42] - z[14];
    z[43]=z[43]*z[2];
    z[40]=z[43] + z[40];
    z[43]=z[40]*z[34];
    z[43]=n<T>(1,2)*z[39] + z[43];
    z[43]=z[43]*z[21];
    z[44]=n<T>(1,3)*z[9];
    z[40]=z[40]*z[44];
    z[45]=z[3]*z[40];
    z[43]=z[43] + z[45];
    z[43]=z[9]*z[43];
    z[41]=z[41] + z[43];
    z[41]=z[9]*z[41];
    z[39]=n<T>(1,4)*z[39] + z[40];
    z[39]=z[9]*z[39];
    z[29]=z[29] + z[39];
    z[29]=z[9]*z[29];
    z[32]= - static_cast<T>(23)- z[32];
    z[31]=n<T>(1,12)*z[32] + z[31];
    z[31]=n<T>(1,2)*z[31] + z[40];
    z[31]=z[9]*z[31];
    z[32]=n<T>(7,2) + n<T>(5,3)*z[4];
    z[31]=n<T>(1,4)*z[32] + z[31];
    z[31]=z[10]*z[31];
    z[32]= - static_cast<T>(1)- n<T>(13,8)*z[4];
    z[29]=n<T>(1,2)*z[31] + n<T>(1,6)*z[32] + z[29];
    z[29]=z[10]*z[29];
    z[31]= - static_cast<T>(13)+ n<T>(17,2)*z[5];
    z[31]=z[10]*z[31];
    z[31]=n<T>(11,2) + z[31];
    z[31]=n<T>(1,3)*z[31] + n<T>(5,2)*z[37];
    z[24]= - z[30]*z[24];
    z[32]= - static_cast<T>(11)+ z[30];
    z[32]=z[10]*z[32];
    z[24]=n<T>(1,3)*z[32] + z[24];
    z[24]=z[12]*z[24];
    z[24]=n<T>(1,16)*z[24] + n<T>(1,8)*z[31];
    z[24]=z[8]*z[24];
    z[31]=static_cast<T>(1)- z[21];
    z[31]=z[31]*z[21];
    z[32]= - static_cast<T>(1)+ n<T>(1,4)*z[3];
    z[32]=z[3]*z[32];
    z[37]=z[9]*z[21];
    z[32]=z[32] + z[37];
    z[32]=z[9]*z[32];
    z[31]=z[31] + z[32];
    z[31]=z[31]*z[44];
    z[32]= - static_cast<T>(1)+ n<T>(1,2)*z[9];
    z[32]=z[9]*z[32];
    z[32]=n<T>(1,2) + z[32];
    z[32]=z[32]*z[44];
    z[37]= - static_cast<T>(1)+ z[44];
    z[37]=z[9]*z[37];
    z[37]=static_cast<T>(1)+ z[37];
    z[37]=z[10]*z[37];
    z[32]=n<T>(1,4)*z[37] - n<T>(1,4) + z[32];
    z[32]=z[10]*z[32];
    z[24]=z[31] + z[32] + z[24];
    z[24]=z[12]*z[24];
    z[31]= - static_cast<T>(7)+ 17*z[4];
    z[31]=z[4]*z[31];
    z[31]=z[31] - n<T>(17,2)*z[23];
    z[31]=z[31]*z[33];
    z[32]=static_cast<T>(3)- n<T>(17,6)*z[4];
    z[32]=z[4]*z[32];
    z[31]=z[32] + z[31];
    z[32]=z[4] - z[42];
    z[32]=z[2]*z[32];
    z[32]= - z[15] + z[32];
    z[32]=z[32]*z[34];
    z[31]=n<T>(1,4)*z[31] + z[32];
    z[31]=z[31]*z[21];
    z[32]=2*z[4];
    z[37]=n<T>(11,4) - z[32];
    z[37]=z[4]*z[37];
    z[37]=z[37] + n<T>(7,8)*z[23];
    z[19]= - z[18] + z[19];
    z[19]=z[19]*z[34];
    z[19]= - n<T>(3,2)*z[18] + z[19];
    z[19]=z[1]*z[19];
    z[19]=n<T>(1,4)*z[19] + n<T>(3,8)*z[38] + n<T>(1,3)*z[37] + z[31];
    z[19]=z[1]*z[19];
    z[31]= - static_cast<T>(1)+ z[5];
    z[30]=z[31]*z[30];
    z[25]=z[35]*z[25];
    z[25]=z[25] + z[36];
    z[25]=z[8]*z[25];
    z[28]=z[5]*z[28];
    z[25]=z[25] + z[30] + z[28] - z[4] + z[3];
    z[28]= - z[18]*z[27];
    z[28]= - z[4] + z[28];
    z[28]=z[5]*z[28];
    z[21]=z[27] + static_cast<T>(1)+ z[21];
    z[18]=z[18]*z[1];
    z[21]=z[21]*z[18];
    z[26]=z[26] - z[3];
    z[26]=z[4]*z[26];
    z[21]=z[21] + z[28] + z[26];
    z[21]=z[1]*z[21];
    z[21]=n<T>(1,2)*z[25] + z[21];
    z[21]=z[6]*z[21];
    z[15]= - z[15] - z[23];
    z[15]=z[5]*z[15];
    z[25]= - static_cast<T>(1)+ z[3];
    z[25]=z[4]*z[25];
    z[18]= - z[3]*z[18];
    z[18]=2*z[25] + z[18];
    z[18]=z[1]*z[18];
    z[18]=z[18] - z[5] + static_cast<T>(2)- z[3];
    z[18]=z[7]*z[18];
    z[15]=z[15] + z[18];
    z[18]=static_cast<T>(1)- 4*z[4];
    z[18]=z[4]*z[18];
    z[18]=z[18] + 2*z[23];
    z[18]=z[18]*z[33];
    z[23]= - n<T>(13,8) + z[32];
    z[23]=z[4]*z[23];
    z[18]=z[18] + n<T>(1,2) + n<T>(1,3)*z[23];
    z[18]=z[2]*z[18];
    z[20]=z[42] - z[20];
    z[20]=z[2]*z[20];
    z[14]=n<T>(1,2)*z[14] + z[20];
    z[14]=z[14]*z[34];
    z[20]= - static_cast<T>(11)+ z[22];
    z[14]=z[14] + n<T>(1,24)*z[20] + z[18];
    z[14]=z[3]*z[14];

    r += z[14] + n<T>(1,3)*z[15] + n<T>(1,4)*z[16] + n<T>(1,8)*z[17] + z[19] + n<T>(3,4)
      *z[21] + z[24] + z[29] + z[41];
 
    return r;
}

template double qg_2lha_r206(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r206(const std::array<dd_real,30>&);
#endif
