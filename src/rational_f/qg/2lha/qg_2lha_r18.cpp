#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r18(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[24];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[27];
    z[7]=z[5] - 1;
    z[8]=n<T>(1,2)*z[3];
    z[9]=z[8] + z[6];
    z[9]= - z[9]*z[7];
    z[10]=z[4]*z[5];
    z[11]= - z[6] - z[3];
    z[11]=z[11]*z[10];
    z[9]=n<T>(1,2)*z[11] + z[9];
    z[9]=z[4]*z[9];
    z[11]=z[3]*z[1];
    z[7]=z[3] - z[7];
    z[7]=z[5]*z[7];
    z[7]=z[7] - static_cast<T>(1)+ z[11];
    z[7]=n<T>(1,2)*z[7] + z[9];
    z[7]=z[4]*z[7];
    z[9]= - static_cast<T>(1)- z[1];
    z[9]=z[1]*z[9];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[9]*z[8];
    z[11]=n<T>(1,2)*z[1];
    z[12]=z[8] + static_cast<T>(1)+ z[11] - n<T>(1,2)*z[5];
    z[12]=z[5]*z[12];
    z[7]=z[7] + z[9] + z[12];
    z[9]=z[1] - 1;
    z[12]=npow(z[5],2);
    z[13]= - z[4]*z[6];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[4]*z[13];
    z[12]=z[13] + z[12] + z[9];
    z[13]=n<T>(1,6)*z[4];
    z[12]=z[12]*z[13];
    z[14]= - static_cast<T>(1)+ z[11];
    z[14]=z[1]*z[14];
    z[8]= - npow(z[1],3)*z[8];
    z[8]=z[8] + n<T>(1,2) + z[14];
    z[9]=z[5]*z[9];
    z[9]= - n<T>(1,6)*z[9] - n<T>(1,3) + z[11];
    z[9]=z[5]*z[9];
    z[8]=z[12] + n<T>(1,3)*z[8] + z[9];
    z[8]=z[2]*z[8];
    z[7]=n<T>(1,3)*z[7] + z[8];
    z[7]=z[2]*z[7];
    z[8]= - z[10] + 1;
    z[9]=z[3] - z[6];
    z[8]=z[9]*z[8];
    z[9]= - static_cast<T>(1)- z[9];
    z[9]=z[5]*z[9];
    z[8]=z[9] + z[8];
    z[8]=z[8]*z[13];
    z[7]=z[8] + z[7];

    r += n<T>(5,3)*z[7];
 
    return r;
}

template double qg_2lha_r18(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r18(const std::array<dd_real,30>&);
#endif
