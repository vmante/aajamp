#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r408(const std::array<T,30>& k) {
  T z[32];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[9];
    z[4]=k[12];
    z[5]=k[10];
    z[6]=k[8];
    z[7]=k[5];
    z[8]=k[11];
    z[9]=k[14];
    z[10]=n<T>(1,4)*z[8];
    z[11]= - static_cast<T>(1)+ z[10];
    z[11]=z[8]*z[11];
    z[11]=z[11] - n<T>(1,2);
    z[11]=z[7]*z[11];
    z[12]=z[9] + n<T>(1,4);
    z[11]=z[12] + z[11];
    z[11]=z[2]*z[11];
    z[13]=z[7] + 1;
    z[14]=n<T>(1,2)*z[3];
    z[13]=z[13]*z[14];
    z[15]=z[13] - static_cast<T>(1)- n<T>(3,2)*z[7];
    z[15]=z[3]*z[15];
    z[16]=z[3] - 1;
    z[17]=z[14]*z[1];
    z[18]= - z[16]*z[17];
    z[19]=z[7] + n<T>(1,2);
    z[20]=z[8] - 1;
    z[21]=z[20]*z[8];
    z[11]=z[18] + z[15] + z[11] - n<T>(1,2)*z[21] + z[19];
    z[11]=z[5]*z[11];
    z[13]=z[13] - z[19];
    z[13]=z[3]*z[13];
    z[15]=npow(z[8],2);
    z[18]=n<T>(1,2)*z[1];
    z[19]=z[18]*npow(z[3],2);
    z[11]=z[11] - z[19] - n<T>(1,2)*z[15] + z[13];
    z[11]=z[5]*z[11];
    z[13]=3*z[8];
    z[22]=static_cast<T>(7)- z[13];
    z[22]=z[22]*z[10];
    z[23]=n<T>(1,4)*z[2];
    z[24]=3*z[2];
    z[25]= - n<T>(77,6) - z[24];
    z[25]=z[25]*z[23];
    z[22]=z[25] + z[22] - n<T>(13,8) + z[9];
    z[22]=z[2]*z[22];
    z[25]=z[1]*z[3];
    z[26]= - static_cast<T>(1)- z[24];
    z[26]=z[25] + n<T>(1,2)*z[26] - z[3];
    z[26]=z[1]*z[26];
    z[27]=n<T>(5,4)*z[3];
    z[28]= - n<T>(95,12) - z[24];
    z[28]=z[2]*z[28];
    z[26]=z[26] - z[27] + n<T>(15,4) + z[28];
    z[26]=z[26]*z[18];
    z[28]=static_cast<T>(1)- z[13];
    z[10]=z[28]*z[10];
    z[28]=n<T>(5,8)*z[3];
    z[10]=z[26] + z[28] + z[22] + z[10] - n<T>(21,8) + z[9];
    z[10]=z[5]*z[10];
    z[22]= - static_cast<T>(1)+ n<T>(3,2)*z[6];
    z[26]=n<T>(1,2)*z[22];
    z[29]=n<T>(3,2) + z[3];
    z[29]=z[3]*z[29];
    z[29]= - z[19] - z[26] + z[29];
    z[29]=z[1]*z[29];
    z[30]= - static_cast<T>(7)- z[27];
    z[30]=z[3]*z[30];
    z[30]=z[30] + n<T>(29,3) - z[2];
    z[29]=n<T>(1,2)*z[30] + z[29];
    z[29]=z[1]*z[29];
    z[30]=n<T>(1,2)*z[8];
    z[31]= - static_cast<T>(1)- n<T>(3,2)*z[8];
    z[30]=z[31]*z[30];
    z[31]=n<T>(9,8) - z[2];
    z[31]=z[2]*z[31];
    z[10]=z[10] + z[29] + n<T>(15,8)*z[3] + z[31] + z[30] + z[12];
    z[10]=z[5]*z[10];
    z[12]= - static_cast<T>(27)- n<T>(37,3)*z[2];
    z[12]=z[3]*z[12];
    z[12]= - n<T>(29,3) + n<T>(1,4)*z[12];
    z[12]=z[12]*z[14];
    z[26]=z[26] - z[3];
    z[26]=z[26]*z[25];
    z[12]=z[12] + z[26];
    z[12]=z[1]*z[12];
    z[26]=n<T>(37,6)*z[2];
    z[29]= - static_cast<T>(1)- z[26];
    z[29]=z[3]*z[29];
    z[29]= - z[2] + z[29];
    z[10]=z[10] + n<T>(1,4)*z[29] + z[12];
    z[12]= - n<T>(77,3) + z[24];
    z[12]=z[12]*z[23];
    z[12]= - n<T>(5,2)*z[3] + static_cast<T>(1)+ z[12];
    z[23]= - static_cast<T>(23)+ 9*z[3];
    z[29]= - static_cast<T>(1)+ 3*z[6];
    z[30]=n<T>(1,8)*z[29] - z[3];
    z[30]=z[1]*z[30];
    z[23]=n<T>(1,4)*z[23] + z[30];
    z[23]=z[1]*z[23];
    z[12]=n<T>(1,2)*z[12] + z[23];
    z[12]=z[1]*z[12];
    z[23]=n<T>(1,2)*z[2];
    z[30]=n<T>(83,3) + z[2];
    z[30]=z[2]*z[30];
    z[30]=n<T>(43,3) + z[30];
    z[30]=z[30]*z[23];
    z[30]=z[13] + z[30];
    z[30]=z[2]*z[30];
    z[20]=3*z[20] + z[30];
    z[22]=z[23] - z[22];
    z[22]=z[1]*z[22];
    z[24]=n<T>(95,3) + z[24];
    z[24]=z[2]*z[24];
    z[24]=n<T>(7,3) + z[24];
    z[22]=n<T>(1,2)*z[24] + z[22];
    z[18]=z[22]*z[18];
    z[22]=n<T>(43,3) + n<T>(3,4)*z[2];
    z[22]=z[2]*z[22];
    z[22]=n<T>(59,12) + z[22];
    z[22]=z[2]*z[22];
    z[18]=z[18] + n<T>(3,2) + z[22];
    z[18]=z[1]*z[18];
    z[18]=n<T>(1,2)*z[20] + z[18];
    z[20]=n<T>(1,2)*z[5];
    z[18]=z[18]*z[20];
    z[22]=npow(z[2],2);
    z[24]=n<T>(23,3) + z[2];
    z[24]=z[24]*z[22];
    z[13]=z[13] + z[24];
    z[12]=z[18] + n<T>(1,4)*z[13] + z[12];
    z[12]=z[5]*z[12];
    z[13]=static_cast<T>(31)+ n<T>(37,2)*z[2];
    z[13]=z[3]*z[13];
    z[13]=static_cast<T>(23)+ n<T>(1,3)*z[13];
    z[13]=z[13]*z[14];
    z[18]= - n<T>(1,4)*z[29] + z[3];
    z[18]=z[18]*z[25];
    z[13]=z[13] + z[18];
    z[13]=z[1]*z[13];
    z[18]= - static_cast<T>(1)+ z[26];
    z[18]=z[3]*z[18];
    z[13]=z[18] + z[13];
    z[13]=z[1]*z[13];
    z[13]=n<T>(1,4)*z[22] + z[13];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[13]=npow(z[2],3);
    z[18]=z[2] + 1;
    z[24]=z[18]*z[13];
    z[26]=n<T>(1,3)*z[1];
    z[29]= - z[2] - z[26];
    z[29]=z[1]*z[29];
    z[22]= - z[22] + z[29];
    z[22]=z[1]*z[18]*z[22];
    z[22]= - n<T>(1,3)*z[24] + z[22];
    z[20]=z[22]*z[20];
    z[22]=z[23] + z[26];
    z[24]=npow(z[1],2);
    z[22]=z[22]*z[24];
    z[13]=z[20] - n<T>(1,6)*z[13] + z[22];
    z[13]=z[5]*z[13];
    z[18]= - z[18]*z[14];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[25];
    z[20]= - z[2]*z[14];
    z[18]=z[20] + n<T>(1,3)*z[18];
    z[18]=z[18]*z[24];
    z[13]=z[18] + z[13];
    z[13]=z[4]*z[13];
    z[12]=n<T>(1,2)*z[12] + 5*z[13];
    z[12]=z[4]*z[12];
    z[10]=n<T>(1,4)*z[10] + z[12];
    z[10]=z[4]*z[10];
    z[12]=z[14] - 1;
    z[13]=z[12]*z[28];
    z[14]=z[17] - z[3];
    z[12]=z[12]*z[14];
    z[14]= - static_cast<T>(5)+ z[2];
    z[12]=n<T>(1,8)*z[14] + z[12];
    z[12]=z[1]*z[12];
    z[14]=z[21] + n<T>(1,8) + z[9];
    z[15]=z[15] + z[2];
    z[15]= - static_cast<T>(1)+ z[9] + n<T>(1,8)*z[15];
    z[15]=z[2]*z[15];
    z[12]=z[12] + z[13] + n<T>(1,2)*z[14] + z[15];
    z[12]=z[5]*z[12];
    z[13]=z[16]*z[27];
    z[14]= - n<T>(1,2) + z[8];
    z[14]=z[8]*z[14];
    z[13]=z[13] + z[23] - n<T>(1,2) + z[14];
    z[14]=n<T>(1,2) - z[3];
    z[14]=z[3]*z[14];
    z[14]=z[19] + n<T>(1,8) + z[14];
    z[14]=z[1]*z[14];
    z[12]=z[12] + n<T>(1,2)*z[13] + z[14];
    z[12]=z[5]*z[12];
    z[13]=static_cast<T>(1)+ n<T>(37,12)*z[3];
    z[13]=z[3]*z[13];
    z[13]=z[13] - z[17];
    z[12]=n<T>(1,4)*z[13] + z[12];
    z[10]=n<T>(1,2)*z[12] + z[10];
    z[10]=z[4]*z[10];

    r += z[10] + n<T>(1,4)*z[11];
 
    return r;
}

template double qg_2lha_r408(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r408(const std::array<dd_real,30>&);
#endif
