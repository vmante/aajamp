#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r638(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[28];
    z[7]=k[13];
    z[8]=k[11];
    z[9]=k[9];
    z[10]=k[3];
    z[11]=n<T>(1,2)*z[2];
    z[12]=npow(z[8],2);
    z[13]=z[12]*z[2];
    z[14]= - z[8] - z[13];
    z[14]=z[14]*z[11];
    z[15]=npow(z[5],2);
    z[16]=z[15]*z[1];
    z[17]=n<T>(1,4) + z[5];
    z[17]=z[5]*z[17];
    z[17]=z[17] + n<T>(3,4)*z[16];
    z[17]=z[4]*z[17];
    z[18]=z[1]*z[5];
    z[19]=npow(z[9],2);
    z[20]= - static_cast<T>(1)- z[5];
    z[14]=z[17] + z[14] + n<T>(1,4)*z[19] + n<T>(1,2)*z[20] - z[18];
    z[14]=z[4]*z[14];
    z[17]=z[9] - 1;
    z[20]=n<T>(1,2)*z[9];
    z[21]=z[17]*z[20];
    z[22]=n<T>(1,2)*z[8];
    z[23]=static_cast<T>(1)- z[8];
    z[23]=z[23]*z[22];
    z[23]=z[23] + z[13];
    z[23]=z[2]*z[23];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[2]*z[23];
    z[24]=z[1] - 1;
    z[21]=z[23] + z[21] + z[24];
    z[14]=n<T>(1,2)*z[21] + z[14];
    z[21]=npow(z[2],2);
    z[23]= - z[21]*z[22];
    z[25]=z[1] + 3;
    z[25]=n<T>(1,2)*z[25];
    z[26]=z[9] - 3;
    z[26]=z[26]*z[9];
    z[23]=z[23] - z[25] - z[26];
    z[23]=z[2]*z[23];
    z[17]=z[9]*z[17];
    z[27]=z[5] + z[18];
    z[27]=z[4]*z[27];
    z[17]=z[27] + z[17] - static_cast<T>(1)- z[1];
    z[27]=n<T>(1,2)*z[4];
    z[17]=z[17]*z[27];
    z[28]= - n<T>(5,2) + z[9];
    z[28]=z[9]*z[28];
    z[17]=z[17] + z[23] + z[25] + z[28];
    z[17]=z[17]*z[27];
    z[23]= - static_cast<T>(5)+ z[9];
    z[23]=z[23]*z[20];
    z[25]=z[2] - 1;
    z[25]=z[8]*z[25];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[11];
    z[23]=z[25] + z[23] + n<T>(5,2) - z[1];
    z[23]=z[23]*z[11];
    z[25]=n<T>(9,2) - z[9];
    z[25]=z[25]*z[20];
    z[23]=z[23] + z[25] - n<T>(7,4) + z[1];
    z[23]=z[2]*z[23];
    z[25]=n<T>(3,2) - z[1];
    z[28]= - static_cast<T>(1)+ n<T>(1,4)*z[9];
    z[28]=z[9]*z[28];
    z[17]=z[17] + z[23] + n<T>(1,2)*z[25] + z[28];
    z[17]=z[3]*z[17];
    z[23]= - static_cast<T>(11)+ 3*z[9];
    z[20]=z[23]*z[20];
    z[23]=static_cast<T>(3)- z[8];
    z[23]=z[8]*z[23];
    z[23]=z[23] + z[13];
    z[23]=z[23]*z[11];
    z[22]=z[23] + static_cast<T>(1)- z[22];
    z[22]=z[2]*z[22];
    z[20]=z[22] - z[1] + z[20];
    z[20]=z[2]*z[20];
    z[20]=z[20] - n<T>(3,2)*z[26] + z[24];
    z[15]=z[15] + z[16];
    z[15]=z[15]*z[27];
    z[15]=z[15] - z[19] + z[5] - n<T>(1,2)*z[18];
    z[15]=z[4]*z[15];
    z[16]= - static_cast<T>(1)+ z[19];
    z[13]=n<T>(1,4)*z[13];
    z[18]= - z[8] - z[13];
    z[18]=z[2]*z[18];
    z[16]=n<T>(1,2)*z[16] + z[18];
    z[11]=z[16]*z[11];
    z[16]=static_cast<T>(1)- n<T>(5,8)*z[9];
    z[16]=z[9]*z[16];
    z[11]=n<T>(1,4)*z[15] + z[11] - n<T>(1,2) + z[16];
    z[11]=z[4]*z[11];
    z[11]=z[17] + n<T>(1,4)*z[20] + z[11];
    z[11]=z[3]*z[11];
    z[11]=n<T>(1,2)*z[14] + z[11];
    z[11]=z[3]*z[11];
    z[14]=z[6]*z[7];
    z[15]=z[5]*z[6];
    z[14]=z[14] - z[15];
    z[16]=npow(z[6],2);
    z[15]= - z[16] - z[15];
    z[15]=z[5]*z[15];
    z[16]=z[7]*z[16];
    z[15]=z[16] + z[15];
    z[15]=z[1]*z[15];
    z[15]=z[15] - z[14];
    z[15]=z[4]*z[15];
    z[14]= - z[1]*z[14];
    z[13]=n<T>(3,4)*z[15] - z[13] + n<T>(1,2)*z[7] + z[14];
    z[13]=z[4]*z[13];
    z[12]=z[12]*z[21];
    z[14]=z[1] - z[10];
    z[14]=z[7]*z[14];
    z[12]=n<T>(1,2)*z[12] - static_cast<T>(1)+ z[14];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[11]=n<T>(1,2)*z[12] + z[11];

    r += n<T>(1,2)*z[11];
 
    return r;
}

template double qg_2lha_r638(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r638(const std::array<dd_real,30>&);
#endif
