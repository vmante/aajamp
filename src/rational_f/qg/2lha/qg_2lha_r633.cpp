#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r633(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[9];
    z[7]=z[1] + 1;
    z[8]=n<T>(1,2)*z[4];
    z[9]=npow(z[5],2);
    z[10]= - z[8]*z[9]*z[7];
    z[11]=z[1]*z[5];
    z[10]=z[10] - z[5] + n<T>(1,2)*z[11];
    z[10]=z[4]*z[10];
    z[10]=z[10] + n<T>(3,2) - z[6];
    z[10]=z[4]*z[10];
    z[12]=z[2] - 1;
    z[13]=n<T>(1,2)*z[1];
    z[14]=z[12]*z[13];
    z[15]=z[6] - z[12];
    z[15]=z[2]*z[15];
    z[10]=z[10] + z[14] - z[6] + z[15];
    z[14]=z[6] - 1;
    z[15]=z[1] - z[14];
    z[15]=z[12]*z[15];
    z[16]= - z[5] - z[11];
    z[16]=z[4]*z[16];
    z[7]=z[16] + z[7];
    z[7]=z[4]*z[7];
    z[7]=z[7] + z[15];
    z[7]=z[7]*z[8];
    z[8]=z[1] + z[14];
    z[14]= - static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[14]=z[14]*z[2];
    z[14]=z[14] + n<T>(1,2);
    z[8]=z[14]*z[8];
    z[7]=z[7] + z[8];
    z[7]=z[3]*z[7];
    z[7]=n<T>(1,2)*z[10] + z[7];
    z[7]=z[3]*z[7];
    z[8]= - z[9]*z[13];
    z[9]= - n<T>(1,2) - z[5];
    z[9]=z[5]*z[9];
    z[8]=z[9] + z[8];
    z[8]=z[4]*z[8];
    z[8]=z[8] + n<T>(1,2) + z[11];
    z[8]=z[4]*z[8];
    z[9]= - z[1] - z[12];
    z[8]=n<T>(1,2)*z[9] + z[8];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[8]= - static_cast<T>(1)- z[5];
    z[8]=z[4]*z[5]*z[8];
    z[8]=static_cast<T>(1)+ z[8];
    z[7]=n<T>(1,4)*z[8] + z[7];

    r += n<T>(1,16)*z[7];
 
    return r;
}

template double qg_2lha_r633(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r633(const std::array<dd_real,30>&);
#endif
