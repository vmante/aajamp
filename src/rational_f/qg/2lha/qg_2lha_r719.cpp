#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r719(const std::array<T,30>& k) {
  T z[40];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[27];
    z[7]=k[6];
    z[8]=k[11];
    z[9]=k[9];
    z[10]=npow(z[9],2);
    z[11]=npow(z[6],2);
    z[12]=z[11] - z[10];
    z[13]=z[11]*z[1];
    z[14]= - static_cast<T>(1)- z[6];
    z[14]=z[6]*z[14];
    z[14]=z[14] - n<T>(3,2)*z[13];
    z[14]=z[1]*z[14];
    z[12]=n<T>(1,2)*z[12] + z[14];
    z[14]=n<T>(1,2)*z[4];
    z[12]=z[12]*z[14];
    z[15]=n<T>(1,2)*z[1];
    z[16]=z[1]*z[6];
    z[17]=z[16] + z[6];
    z[17]=static_cast<T>(1)+ n<T>(3,2)*z[17];
    z[17]=z[17]*z[15];
    z[18]=npow(z[8],2);
    z[19]=n<T>(1,2)*z[2];
    z[20]=z[18]*z[19];
    z[21]= - z[8] - z[20];
    z[22]=n<T>(3,4)*z[2];
    z[21]=z[21]*z[22];
    z[21]=z[21] - static_cast<T>(1)+ z[10];
    z[22]=n<T>(1,4)*z[2];
    z[21]=z[21]*z[22];
    z[23]=n<T>(1,2)*z[6];
    z[24]=n<T>(3,4) - z[9];
    z[24]=z[9]*z[24];
    z[12]=z[12] + z[21] + z[17] - z[23] + z[24];
    z[12]=z[4]*z[12];
    z[17]=n<T>(1,2)*z[9];
    z[21]=3*z[9];
    z[24]= - n<T>(11,4) + z[21];
    z[24]=z[24]*z[17];
    z[25]=n<T>(1,2)*z[8];
    z[26]=z[25] - 1;
    z[26]=z[26]*z[8];
    z[27]=z[18]*z[2];
    z[28]=n<T>(3,4)*z[27];
    z[29]= - z[26] + z[28];
    z[29]=z[2]*z[29];
    z[30]= - static_cast<T>(1)- n<T>(3,4)*z[8];
    z[29]=n<T>(1,2)*z[30] + z[29];
    z[29]=z[29]*z[19];
    z[30]=z[15] - 1;
    z[24]=z[29] + z[24] + z[30];
    z[24]=z[2]*z[24];
    z[29]=5*z[9];
    z[31]=n<T>(3,2) - z[9];
    z[31]=z[31]*z[29];
    z[31]= - n<T>(1,2) + z[31];
    z[32]=z[1] + n<T>(1,4);
    z[32]=z[32]*z[1];
    z[24]=z[24] + n<T>(1,2)*z[31] - z[32];
    z[12]=n<T>(1,2)*z[24] + z[12];
    z[12]=z[4]*z[12];
    z[24]=n<T>(1,4)*z[1];
    z[31]=static_cast<T>(3)- z[1];
    z[31]=z[31]*z[24];
    z[33]= - n<T>(1,2) + z[8];
    z[33]=z[8]*z[33];
    z[28]=z[33] - z[28];
    z[28]=z[2]*z[28];
    z[33]=z[8] + 1;
    z[34]= - z[8]*z[33];
    z[34]=static_cast<T>(11)+ z[34];
    z[28]=n<T>(1,4)*z[34] + z[28];
    z[28]=z[28]*z[19];
    z[34]= - static_cast<T>(3)+ z[8];
    z[28]=z[28] + n<T>(1,8)*z[34] - z[1];
    z[28]=z[28]*z[19];
    z[34]= - n<T>(17,8) + z[9];
    z[34]=z[9]*z[34];
    z[28]=z[28] + z[31] - n<T>(1,4) + z[34];
    z[28]=z[2]*z[28];
    z[31]= - static_cast<T>(1)+ z[1];
    z[31]=z[31]*z[24];
    z[34]=z[9] - n<T>(9,4);
    z[34]=z[34]*z[9];
    z[28]=z[28] + z[31] - n<T>(1,4) - z[34];
    z[12]=n<T>(1,2)*z[28] + z[12];
    z[28]=z[15] + 1;
    z[31]=z[28]*z[1];
    z[35]= - n<T>(7,4) + z[21];
    z[35]=z[9]*z[35];
    z[35]= - static_cast<T>(1)+ z[35];
    z[36]=z[8]*npow(z[2],3);
    z[35]= - n<T>(1,16)*z[36] + n<T>(1,4)*z[35] + z[31];
    z[35]=z[2]*z[35];
    z[36]= - static_cast<T>(1)- z[9];
    z[36]=z[36]*z[17];
    z[36]=static_cast<T>(1)+ z[36];
    z[37]= - z[6]*z[31];
    z[37]= - z[23] + z[37];
    z[37]=z[4]*z[37];
    z[36]=z[37] + n<T>(1,2)*z[36] + z[31];
    z[36]=z[4]*z[36];
    z[37]=static_cast<T>(1)- n<T>(3,2)*z[9];
    z[37]=z[9]*z[37];
    z[37]=n<T>(1,2) + z[37];
    z[31]=z[36] + z[35] + n<T>(1,2)*z[37] - z[31];
    z[31]=z[4]*z[31];
    z[35]=npow(z[2],2);
    z[36]=z[2] - 1;
    z[36]=z[8]*z[36];
    z[36]= - n<T>(1,2) + z[36];
    z[36]=z[36]*z[35];
    z[37]=z[1] + n<T>(1,2);
    z[38]=z[37]*z[1];
    z[39]=n<T>(13,2) - z[21];
    z[39]=z[9]*z[39];
    z[39]= - n<T>(13,4) + z[39];
    z[36]=n<T>(1,4)*z[36] + n<T>(1,2)*z[39] + z[38];
    z[36]=z[36]*z[19];
    z[34]=n<T>(5,4) + z[34];
    z[34]=z[36] + n<T>(3,2)*z[34] - z[38];
    z[34]=z[2]*z[34];
    z[36]=n<T>(1,4)*z[9];
    z[38]=static_cast<T>(7)- z[21];
    z[38]=z[38]*z[36];
    z[37]=z[37]*z[15];
    z[31]=z[31] + z[34] + z[37] - static_cast<T>(1)+ z[38];
    z[31]=z[4]*z[31];
    z[34]= - z[19] + 1;
    z[34]=z[8]*z[34];
    z[34]=n<T>(1,2) + z[34];
    z[34]=z[2]*z[34];
    z[28]=z[34] - z[25] - z[28];
    z[28]=z[28]*z[19];
    z[34]= - n<T>(15,4) + z[9];
    z[34]=z[9]*z[34];
    z[37]= - n<T>(5,4) + z[1];
    z[37]=z[1]*z[37];
    z[28]=z[28] + z[37] + static_cast<T>(3)+ z[34];
    z[28]=z[2]*z[28];
    z[34]=3*z[1];
    z[37]=n<T>(7,4) - z[1];
    z[37]=z[37]*z[34];
    z[38]=n<T>(23,2) - z[21];
    z[38]=z[9]*z[38];
    z[28]=z[28] + z[37] - n<T>(17,2) + z[38];
    z[28]=z[2]*z[28];
    z[21]= - n<T>(47,4) + z[21];
    z[21]=z[9]*z[21];
    z[37]= - n<T>(23,4) + z[34];
    z[37]=z[1]*z[37];
    z[21]=z[28] + z[37] + n<T>(35,4) + z[21];
    z[21]=z[21]*z[22];
    z[28]= - z[30]*z[15];
    z[36]=static_cast<T>(1)- z[36];
    z[36]=z[9]*z[36];
    z[21]=z[31] + z[21] + z[28] - n<T>(3,4) + z[36];
    z[21]=z[3]*z[21];
    z[28]=n<T>(1,2) + z[9];
    z[28]=z[28]*z[17];
    z[31]= - z[6] + z[16];
    z[31]=z[31]*z[15];
    z[36]=z[11]*z[15];
    z[37]=z[11] + z[36];
    z[37]=z[1]*z[37];
    z[37]=n<T>(1,2)*z[11] + z[37];
    z[37]=z[37]*z[14];
    z[28]=z[37] + z[31] - z[6] + z[28];
    z[28]=z[4]*z[28];
    z[20]= - 3*z[8] - z[20];
    z[20]=z[20]*z[35];
    z[31]=n<T>(3,16) - z[9];
    z[31]=z[9]*z[31];
    z[20]=n<T>(1,16)*z[20] - n<T>(1,4) + z[31];
    z[20]=z[2]*z[20];
    z[31]= - static_cast<T>(5)+ n<T>(13,2)*z[9];
    z[31]=z[9]*z[31];
    z[31]=n<T>(5,2) + z[31];
    z[24]=static_cast<T>(1)- z[24];
    z[24]=z[1]*z[24];
    z[20]=z[28] + z[20] + n<T>(1,4)*z[31] + z[24];
    z[20]=z[4]*z[20];
    z[24]=static_cast<T>(5)- z[8];
    z[24]=z[8]*z[24];
    z[24]=z[24] + z[27];
    z[24]=z[2]*z[24];
    z[24]= - 3*z[33] + z[24];
    z[28]=n<T>(1,8)*z[2];
    z[24]=z[24]*z[28];
    z[31]= - n<T>(7,8) + z[9];
    z[31]=z[9]*z[31];
    z[24]=z[24] - z[1] - n<T>(3,8) + z[31];
    z[24]=z[2]*z[24];
    z[31]=n<T>(61,4) - 9*z[9];
    z[31]=z[9]*z[31];
    z[31]= - n<T>(9,2) + z[31];
    z[24]=z[24] + n<T>(1,2)*z[31] + z[34];
    z[24]=z[24]*z[19];
    z[31]= - static_cast<T>(1)+ z[17];
    z[31]=z[9]*z[31];
    z[31]=static_cast<T>(3)+ 7*z[31];
    z[20]=z[20] + z[24] + n<T>(1,2)*z[31] - z[1];
    z[14]=z[20]*z[14];
    z[20]= - static_cast<T>(3)+ z[9];
    z[20]=z[20]*z[29];
    z[24]=static_cast<T>(7)+ z[1];
    z[24]=z[1]*z[24];
    z[20]=z[24] - n<T>(3,4) + z[20];
    z[24]= - z[18]*z[22];
    z[24]=z[26] + z[24];
    z[24]=z[2]*z[24];
    z[26]= - static_cast<T>(1)+ n<T>(1,4)*z[8];
    z[31]= - z[8]*z[26];
    z[24]=z[24] + n<T>(5,4) + z[31];
    z[24]=z[24]*z[19];
    z[24]=z[24] + static_cast<T>(1)- n<T>(9,4)*z[1];
    z[24]=z[2]*z[24];
    z[20]=n<T>(1,2)*z[20] + z[24];
    z[20]=z[2]*z[20];
    z[24]=n<T>(31,2) - z[29];
    z[24]=z[9]*z[24];
    z[20]=z[20] - z[32] - n<T>(19,4) + z[24];
    z[20]=z[20]*z[28];
    z[24]= - static_cast<T>(1)+ n<T>(5,16)*z[9];
    z[24]=z[9]*z[24];
    z[28]=z[1]*z[30];
    z[14]=n<T>(1,2)*z[21] + z[14] + z[20] + n<T>(1,8)*z[28] + n<T>(7,16) + z[24];
    z[14]=z[3]*z[14];
    z[12]=n<T>(1,2)*z[12] + z[14];
    z[12]=z[3]*z[12];
    z[14]=z[5] - 1;
    z[20]=z[7]*z[14];
    z[10]=z[10] - static_cast<T>(1)+ z[20];
    z[20]=npow(z[5],2);
    z[11]=z[20]*z[11];
    z[10]=z[11] + n<T>(1,2)*z[10];
    z[11]=n<T>(1,4) - z[5];
    z[11]=z[6]*z[11];
    z[11]= - n<T>(1,2) + z[11];
    z[11]=z[6]*z[11];
    z[11]=z[11] + n<T>(3,4)*z[13];
    z[11]=z[1]*z[11];
    z[13]=n<T>(3,2)*z[27];
    z[21]= - z[5]*z[25];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[8]*z[21];
    z[21]=z[21] - z[13];
    z[21]=z[2]*z[21];
    z[24]=z[4]*z[36];
    z[10]=z[24] + n<T>(1,16)*z[21] + n<T>(1,4)*z[10] + z[11];
    z[10]=z[4]*z[10];
    z[11]= - z[14]*z[23];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[6]*z[20]*z[11];
    z[21]= - n<T>(1,2) + z[9];
    z[21]=z[21]*z[17];
    z[23]=n<T>(1,2)*z[5];
    z[11]=z[21] - z[23] + z[11];
    z[21]=z[5] - n<T>(1,2);
    z[24]=z[6]*z[5];
    z[21]=z[21]*z[24];
    z[25]=z[21] - n<T>(1,2) + 3*z[5];
    z[25]=z[6]*z[25];
    z[28]=static_cast<T>(3)+ n<T>(1,2)*z[7];
    z[25]=n<T>(1,2)*z[28] + z[25];
    z[28]= - static_cast<T>(1)- n<T>(1,4)*z[24];
    z[16]=z[28]*z[16];
    z[16]=n<T>(1,2)*z[25] + z[16];
    z[16]=z[1]*z[16];
    z[25]=z[8]*z[14];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[8]*z[25];
    z[25]=z[25] + 3*z[27];
    z[25]=z[25]*z[22];
    z[25]=z[25] - static_cast<T>(1)- n<T>(1,8)*z[8];
    z[22]=z[25]*z[22];
    z[10]=z[10] + z[22] + n<T>(1,2)*z[11] + z[16];
    z[10]=z[4]*z[10];
    z[11]=z[6]*z[14];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[20]*z[11];
    z[14]= - static_cast<T>(1)+ z[9];
    z[14]=z[14]*z[17];
    z[11]=z[14] + z[11];
    z[14]=static_cast<T>(1)+ z[24];
    z[14]=z[14]*z[15];
    z[14]=z[14] - z[21] - n<T>(1,4) - z[5];
    z[14]=z[1]*z[14];
    z[15]=static_cast<T>(1)- z[23];
    z[15]=z[15]*z[18];
    z[13]=z[15] - z[13];
    z[13]=z[13]*z[19];
    z[13]=z[13] - z[26];
    z[13]=z[13]*z[19];
    z[15]= - static_cast<T>(1)- z[5];
    z[13]=z[13] + n<T>(1,2)*z[15] + z[1];
    z[13]=z[13]*z[19];
    z[11]=z[13] + n<T>(1,2)*z[11] + z[14];
    z[10]=n<T>(1,2)*z[11] + z[10];

    r += n<T>(1,2)*z[10] + z[12];
 
    return r;
}

template double qg_2lha_r719(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r719(const std::array<dd_real,30>&);
#endif
