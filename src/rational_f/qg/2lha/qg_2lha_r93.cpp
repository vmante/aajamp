#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r93(const std::array<T,30>& k) {
  T z[28];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[20];
    z[6]=k[2];
    z[7]=k[11];
    z[8]=k[3];
    z[9]=k[14];
    z[10]=k[9];
    z[11]=k[12];
    z[12]=k[4];
    z[13]=k[24];
    z[14]=n<T>(1,3)*z[9];
    z[15]=z[6] - 1;
    z[16]=z[14]*z[15];
    z[16]= - z[16] - n<T>(5,8) + n<T>(1,3)*z[6];
    z[17]= - z[9]*z[16];
    z[17]= - n<T>(7,24) + z[17];
    z[18]=n<T>(1,2)*z[9];
    z[17]=z[17]*z[18];
    z[19]=z[18] - 1;
    z[19]=z[9]*z[19];
    z[19]=n<T>(1,2) + z[19];
    z[19]=z[3]*z[15]*z[19];
    z[20]=n<T>(3,8)*z[5];
    z[21]= - n<T>(7,8) - z[2];
    z[22]=n<T>(1,4)*z[7];
    z[23]=z[6]*z[22];
    z[17]=n<T>(1,6)*z[19] + z[17] + z[23] + n<T>(1,3)*z[21] + z[20];
    z[17]=z[3]*z[17];
    z[19]=z[7]*npow(z[6],2);
    z[19]=n<T>(2,3)*z[6] - n<T>(5,16)*z[19];
    z[19]=z[7]*z[15]*z[19];
    z[21]=n<T>(1,8) + z[2];
    z[21]=z[1]*z[21];
    z[21]=n<T>(17,16) + z[21];
    z[23]= - z[1]*z[20];
    z[19]=z[19] - n<T>(17,48)*z[6] + n<T>(1,3)*z[21] + z[23];
    z[19]=z[3]*z[19];
    z[21]= - static_cast<T>(1)- z[1];
    z[21]=z[21]*z[20];
    z[23]=n<T>(1,2)*z[8];
    z[24]= - static_cast<T>(1)+ z[11];
    z[24]=z[24]*z[23];
    z[23]= - z[23] - z[6];
    z[23]=z[6]*z[23];
    z[23]=z[24] + z[23];
    z[23]=z[7]*z[23];
    z[24]=z[5]*z[8];
    z[25]=z[24] - 1;
    z[25]=3*z[25];
    z[23]=5*z[23] - z[25] + n<T>(7,3)*z[6];
    z[26]=n<T>(1,8)*z[7];
    z[23]=z[23]*z[26];
    z[19]=z[19] + z[23] + n<T>(1,3) + z[21];
    z[19]=z[4]*z[19];
    z[21]=n<T>(1,6)*z[10];
    z[21]=z[15]*z[21];
    z[16]=z[21] - z[16];
    z[16]=z[9]*z[16];
    z[21]= - static_cast<T>(1)+ z[10];
    z[16]=n<T>(7,24)*z[21] + z[16];
    z[16]=z[18]*z[10]*z[16];
    z[21]=z[13] - 1;
    z[21]=z[21]*z[13];
    z[23]=z[10]*z[13];
    z[27]=z[21] + z[23];
    z[27]=z[10]*z[27];
    z[21]= - z[11]*z[21];
    z[21]=z[21] + z[27];
    z[27]=n<T>(5,2)*z[7];
    z[15]=z[8] + z[11] + z[15];
    z[15]=z[15]*z[27];
    z[15]=z[15] - n<T>(11,3) + 3*z[24];
    z[15]=z[15]*z[26];
    z[15]=z[19] + z[17] + z[16] + z[15] + n<T>(1,3)*z[2] + z[20] + n<T>(1,16)*
    z[21];
    z[15]=z[4]*z[15];
    z[16]=z[10]*z[12];
    z[17]= - z[25] + n<T>(1,6)*z[16];
    z[17]=z[10]*z[17];
    z[16]=static_cast<T>(1)- z[16];
    z[16]=z[16]*z[27];
    z[16]=z[17] + z[16];
    z[16]=z[7]*z[16];
    z[17]=static_cast<T>(1)+ n<T>(1,2)*z[12];
    z[19]=z[17]*z[23];
    z[17]=z[17]*z[13];
    z[17]=z[17] - n<T>(1,2);
    z[17]=z[17]*z[13];
    z[19]=z[17] + z[19];
    z[19]=z[10]*z[19];
    z[17]= - z[11]*z[17];
    z[16]=z[16] + z[17] + z[19];
    z[17]=z[12] + 1;
    z[17]=z[17]*z[22];
    z[19]=n<T>(1,3)*z[10];
    z[20]=static_cast<T>(1)- z[19];
    z[20]=n<T>(1,2)*z[20] - z[14];
    z[20]=z[18]*z[20];
    z[20]=z[20] - z[17];
    z[20]=z[10]*z[20];
    z[21]= - static_cast<T>(1)+ n<T>(1,2)*z[10];
    z[19]=z[21]*z[19];
    z[19]=z[19] + z[20];
    z[19]=z[9]*z[19];
    z[14]=n<T>(1,2) - z[14];
    z[14]=z[14]*z[18];
    z[18]=static_cast<T>(1)- z[9];
    z[18]=z[3]*z[18];
    z[14]=n<T>(1,12)*z[18] + z[14] - n<T>(1,3) - z[17];
    z[14]=z[9]*z[14];
    z[14]=z[22] + z[14];
    z[14]=z[3]*z[14];

    r += z[14] + z[15] + n<T>(1,8)*z[16] + z[19];
 
    return r;
}

template double qg_2lha_r93(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r93(const std::array<dd_real,30>&);
#endif
