#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1059(const std::array<T,30>& k) {
  T z[28];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[27];
    z[7]=k[3];
    z[8]=k[9];
    z[9]=k[4];
    z[10]=k[10];
    z[11]=k[5];
    z[12]=n<T>(1,4)*z[7];
    z[13]=n<T>(1,2)*z[10];
    z[14]= - n<T>(1,3) - z[13];
    z[14]=z[14]*z[12];
    z[15]=n<T>(1,8)*z[10];
    z[16]=z[6] - z[3];
    z[17]=n<T>(1,3) + n<T>(5,8)*z[5] - n<T>(1,8)*z[16];
    z[17]=z[1]*z[17];
    z[18]=z[7] - n<T>(1,2);
    z[19]= - z[7]*z[18];
    z[19]=n<T>(1,2) + z[19];
    z[19]=z[8]*z[19];
    z[14]=n<T>(1,12)*z[19] + z[14] + z[15] - n<T>(7,24) + z[17];
    z[14]=z[2]*z[14];
    z[17]=n<T>(1,2)*z[3];
    z[19]=n<T>(1,2)*z[6];
    z[20]= - z[19] + z[17] + n<T>(13,3) - n<T>(1,2)*z[11];
    z[21]=n<T>(1,3)*z[7];
    z[22]=n<T>(11,4) - z[7];
    z[22]=z[7]*z[22];
    z[22]= - n<T>(17,8) + z[22];
    z[22]=z[22]*z[21];
    z[22]=n<T>(1,8) + z[22];
    z[22]=z[8]*z[22];
    z[23]=n<T>(43,24) - z[7];
    z[23]=z[7]*z[23];
    z[22]=z[22] - n<T>(1,24) + z[23];
    z[22]=z[8]*z[22];
    z[23]=n<T>(1,4)*z[6];
    z[24]=z[23] - n<T>(1,3);
    z[25]= - n<T>(1,4)*z[3] - n<T>(5,4)*z[5] + z[24];
    z[25]=z[1]*z[25];
    z[26]= - n<T>(2,3) + z[15];
    z[26]=z[7]*z[26];
    z[14]=z[14] + z[22] + z[26] - z[15] + n<T>(1,4)*z[20] + z[25];
    z[14]=z[2]*z[14];
    z[15]= - n<T>(13,2) + 5*z[7];
    z[15]=z[15]*z[21];
    z[15]=n<T>(1,2) + z[15];
    z[15]=z[8]*z[15];
    z[20]= - n<T>(1,2) + z[1];
    z[22]=n<T>(5,3) + z[13];
    z[22]=z[7]*z[22];
    z[25]=z[2]*z[1];
    z[15]= - n<T>(5,6)*z[25] + z[15] + z[22] + n<T>(5,3)*z[20] - z[13];
    z[15]=z[2]*z[15];
    z[20]=z[9] - 1;
    z[22]= - n<T>(5,2)*z[7] - n<T>(5,3)*z[20] - z[13];
    z[22]=z[7]*z[22];
    z[25]=z[7] + z[9];
    z[25]= - z[8]*z[25]*npow(z[7],2);
    z[22]=n<T>(5,6)*z[25] + z[13] + z[22];
    z[22]=z[8]*z[22];
    z[25]= - z[1] - z[20];
    z[25]=n<T>(1,2)*z[25] - z[7];
    z[15]=z[15] + n<T>(5,3)*z[25] + z[22];
    z[15]=z[4]*z[15];
    z[22]= - static_cast<T>(3)- z[10];
    z[22]=z[22]*z[12];
    z[25]=static_cast<T>(7)- z[9];
    z[25]=n<T>(1,3)*z[25] - z[13];
    z[25]=z[7]*z[25];
    z[13]=z[13] + z[25];
    z[25]=n<T>(1,2)*z[8];
    z[13]=z[13]*z[25];
    z[26]=n<T>(1,4)*z[10];
    z[27]=static_cast<T>(1)+ z[11];
    z[13]=z[13] + z[22] + z[26] + n<T>(1,4)*z[27] + n<T>(1,3)*z[9];
    z[13]=z[8]*z[13];
    z[22]=z[20]*z[6];
    z[20]=z[3]*z[20];
    z[16]=5*z[5] - z[16];
    z[16]=z[1]*z[16];
    z[16]=z[16] - z[22] - static_cast<T>(3)+ z[20];
    z[13]=z[15] + n<T>(1,4)*z[16] + z[13];
    z[13]=z[14] + n<T>(1,2)*z[13];
    z[13]=z[4]*z[13];
    z[14]=n<T>(1,2)*z[7];
    z[15]= - n<T>(7,2) + z[7];
    z[15]=z[15]*z[14];
    z[16]= - n<T>(3,2) + z[21];
    z[14]=z[16]*z[14];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(5,12) + z[14];
    z[14]=z[8]*z[14];
    z[14]=z[14] + z[15] + static_cast<T>(1)- z[23];
    z[14]=z[8]*z[14];
    z[15]=n<T>(1,2)*z[5];
    z[16]= - z[17] + n<T>(1,3) - z[15];
    z[16]=z[1]*z[16];
    z[17]=z[26] - n<T>(1,3);
    z[20]= - z[7]*z[17];
    z[14]=z[14] + z[20] + n<T>(1,2)*z[16] + z[24];
    z[14]=z[2]*z[14];
    z[16]= - static_cast<T>(11)+ z[6];
    z[16]=n<T>(1,2)*z[16] + z[7];
    z[20]= - static_cast<T>(1)+ z[12];
    z[20]=z[7]*z[20];
    z[20]=n<T>(3,4) + z[20];
    z[20]=z[8]*z[20];
    z[16]=n<T>(1,2)*z[16] + z[20];
    z[16]=z[8]*z[16];
    z[20]=z[5] + z[3];
    z[20]=z[1]*z[20];
    z[20]= - z[10] + z[20] - z[3] + z[5] + static_cast<T>(1)+ 3*z[11];
    z[14]=z[14] + n<T>(1,4)*z[20] + z[16];
    z[14]=z[2]*z[14];
    z[12]=z[12] + z[17];
    z[12]=z[8]*z[12];
    z[16]= - z[9]*z[23];
    z[12]=z[12] + z[26] + z[16] + n<T>(1,2) - z[11];
    z[12]=z[8]*z[12];
    z[12]=z[14] - n<T>(1,4)*z[5] + z[12];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[4]*z[12];
    z[13]=static_cast<T>(1)- z[22];
    z[13]=z[13]*z[25];
    z[14]=z[9] + z[18];
    z[14]=z[10]*z[14];
    z[13]=z[13] - z[19] + z[14];
    z[13]=z[2]*z[13];
    z[13]=z[13] + n<T>(3,2)*z[8] + z[10] - z[11] - z[15];
    z[13]=z[2]*z[13];
    z[14]=z[8]*z[11];
    z[13]=z[14] + z[13];

    r += z[12] + n<T>(1,4)*z[13];
 
    return r;
}

template double qg_2lha_r1059(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1059(const std::array<dd_real,30>&);
#endif
