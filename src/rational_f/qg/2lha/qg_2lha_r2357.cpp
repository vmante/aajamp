#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2357(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[11];
    z[7]=k[12];
    z[8]=z[3] - 1;
    z[9]=npow(z[3],2);
    z[8]=z[8]*z[9];
    z[10]= - z[4]*z[8];
    z[11]= - n<T>(1,2)*z[1] + n<T>(1,2);
    z[12]=z[3] - 3;
    z[12]=z[12]*z[3];
    z[12]=z[12] + 3;
    z[12]=z[12]*z[3];
    z[12]=z[12] - 1;
    z[11]=z[12]*z[11];
    z[12]=n<T>(1,2)*z[3];
    z[13]=z[12] - 1;
    z[13]=z[13]*z[3];
    z[13]=z[13] + n<T>(1,2);
    z[14]=z[13]*z[3];
    z[15]=z[4]*z[14];
    z[11]=z[15] + z[11];
    z[11]=z[2]*z[11];
    z[15]=static_cast<T>(7)- 3*z[3];
    z[15]=z[3]*z[15];
    z[15]= - static_cast<T>(5)+ z[15];
    z[15]=z[3]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[10]=z[11] + n<T>(1,4)*z[15] + z[10];
    z[10]=z[2]*z[1]*z[10];
    z[11]= - static_cast<T>(1)+ n<T>(3,2)*z[3];
    z[9]=z[11]*z[9];
    z[11]=n<T>(1,2)*z[4];
    z[15]=npow(z[3],3);
    z[16]=z[15]*z[11];
    z[9]=z[9] + z[16];
    z[9]=z[9]*z[11];
    z[11]= - static_cast<T>(1)+ n<T>(3,4)*z[3];
    z[11]=z[3]*z[11];
    z[11]=n<T>(1,4) + z[11];
    z[11]=z[3]*z[11];
    z[9]=z[11] + z[9];
    z[9]=z[4]*z[9];
    z[11]=z[13]*z[12];
    z[9]=z[11] + z[9];
    z[9]=z[7]*z[9];
    z[11]=z[15]*z[4];
    z[8]=z[11] + z[8];
    z[11]=z[5]*z[15];
    z[11]=z[11] - z[8];
    z[12]=n<T>(1,4)*z[6];
    z[11]=z[12]*z[11]*npow(z[4],2);
    z[8]= - z[4]*z[8];
    z[8]= - z[14] + z[8];
    z[8]=z[9] + n<T>(1,2)*z[8] + z[10] + z[11];

    r += n<T>(3,2)*z[8];
 
    return r;
}

template double qg_2lha_r2357(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2357(const std::array<dd_real,30>&);
#endif
