#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1206(const std::array<T,30>& k) {
  T z[48];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[6];
    z[5]=k[3];
    z[6]=k[2];
    z[7]=k[7];
    z[8]=k[11];
    z[9]=k[9];
    z[10]=k[10];
    z[11]=k[14];
    z[12]=k[5];
    z[13]=n<T>(5,8)*z[10];
    z[14]=3*z[12];
    z[15]= - z[13] + n<T>(1709,96) - z[14];
    z[16]=z[1] + 1;
    z[17]=n<T>(13,3)*z[3];
    z[18]=z[16]*z[17];
    z[19]= - n<T>(995,48)*z[1] - n<T>(143,24) - z[12];
    z[19]=n<T>(1,2)*z[19] + z[18];
    z[19]=z[3]*z[19];
    z[20]= - n<T>(81,32)*z[8] + n<T>(1271,96)*z[1] - n<T>(21,2)*z[11] - n<T>(503,32) + 
    z[14];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[3]*z[19];
    z[20]=n<T>(1,4)*z[9];
    z[21]=n<T>(9,4)*z[11];
    z[22]=static_cast<T>(11)- z[21];
    z[22]=z[11]*z[22];
    z[23]=n<T>(4,3)*z[8];
    z[24]=n<T>(51,16) + z[23];
    z[24]=z[8]*z[24];
    z[15]=z[19] + z[24] - z[20] + n<T>(13,48)*z[1] + n<T>(1,2)*z[15] + z[22];
    z[15]=z[3]*z[15];
    z[19]=n<T>(55,32)*z[1];
    z[22]=n<T>(1,2)*z[9];
    z[24]=9*z[11];
    z[25]= - static_cast<T>(23)+ z[24];
    z[25]=z[11]*z[25];
    z[25]=z[22] - z[19] + n<T>(1,2)*z[25] + z[13] - n<T>(115,24) + z[12];
    z[26]= - static_cast<T>(1)+ n<T>(9,2)*z[11];
    z[26]=z[26]*z[11];
    z[27]=npow(z[9],2);
    z[28]=5*z[10];
    z[29]=n<T>(59,2) - z[28];
    z[29]=n<T>(3,2)*z[27] + n<T>(1,16)*z[29] - z[26];
    z[30]=n<T>(5,32)*z[10];
    z[31]=n<T>(2,3)*z[8];
    z[32]= - n<T>(5,64) - z[31];
    z[32]=z[8]*z[32];
    z[32]=z[30] + z[32];
    z[32]=z[2]*z[32];
    z[33]=z[23] + n<T>(21,32);
    z[33]=z[33]*z[8];
    z[29]=z[32] + n<T>(1,2)*z[29] + z[33];
    z[29]=z[2]*z[29];
    z[23]= - n<T>(123,64) - z[23];
    z[23]=z[8]*z[23];
    z[15]=z[15] + z[29] + n<T>(1,2)*z[25] + z[23];
    z[15]=z[4]*z[15];
    z[23]=n<T>(5,4) + z[10];
    z[23]=z[23]*z[28];
    z[23]= - n<T>(51,8) + z[23];
    z[23]=n<T>(1,4)*z[23] + z[26];
    z[25]=n<T>(1,32)*z[8];
    z[29]= - n<T>(237,2) - n<T>(115,3)*z[8];
    z[29]=z[29]*z[25];
    z[23]=z[29] + n<T>(1,2)*z[23] - z[27];
    z[29]= - n<T>(3,2) - z[10];
    z[29]=z[29]*z[30];
    z[32]=z[8] + n<T>(5,64);
    z[34]=npow(z[8],2);
    z[35]= - z[32]*z[34];
    z[36]=npow(z[10],2);
    z[35]=n<T>(5,32)*z[36] + z[35];
    z[35]=z[2]*z[35];
    z[37]=z[8]*z[32];
    z[37]=n<T>(43,128) + z[37];
    z[37]=z[8]*z[37];
    z[29]=z[35] + z[29] + z[37];
    z[29]=z[2]*z[29];
    z[23]=n<T>(1,2)*z[23] + z[29];
    z[23]=z[2]*z[23];
    z[29]=static_cast<T>(1)+ n<T>(1,2)*z[10];
    z[35]=z[29]*z[10];
    z[37]=n<T>(53,2) - z[24];
    z[37]=z[11]*z[37];
    z[19]= - z[19] + z[37] - n<T>(5,2)*z[35] - n<T>(109,32) - z[12];
    z[37]=n<T>(879,2) + n<T>(307,3)*z[8];
    z[37]=z[37]*z[25];
    z[19]=z[37] + n<T>(1,2)*z[19] - z[9];
    z[37]=z[10] + 3;
    z[38]=n<T>(5,4)*z[10];
    z[39]=z[37]*z[38];
    z[40]=5*z[12];
    z[39]=z[39] + n<T>(307,96) + z[40];
    z[41]= - n<T>(1801,2) - n<T>(563,3)*z[8];
    z[25]=z[41]*z[25];
    z[41]= - n<T>(169,4) + z[24];
    z[41]=z[11]*z[41];
    z[25]=z[25] + n<T>(3,2)*z[9] - n<T>(55,64)*z[1] + n<T>(1,2)*z[39] + z[41];
    z[39]=n<T>(26,3)*z[3];
    z[41]= - z[16]*z[39];
    z[42]=n<T>(2641,48)*z[1] + n<T>(41,4) + z[14];
    z[41]=n<T>(1,4)*z[42] + z[41];
    z[41]=z[3]*z[41];
    z[42]=7*z[12];
    z[43]=n<T>(293,16) - z[42];
    z[41]=z[41] + n<T>(419,64)*z[8] - n<T>(191,48)*z[1] + n<T>(1,4)*z[43] + 14*z[11]
   ;
    z[41]=z[3]*z[41];
    z[25]=n<T>(1,2)*z[25] + z[41];
    z[25]=z[3]*z[25];
    z[15]=z[15] + z[25] + n<T>(1,2)*z[19] + z[23];
    z[15]=z[4]*z[15];
    z[19]= - n<T>(177,16)*z[8] - 15*z[11] + n<T>(1435,48) + z[12];
    z[19]=n<T>(1,2)*z[19] - z[17];
    z[19]=z[3]*z[19];
    z[23]= - n<T>(1135,32) - z[14];
    z[21]=static_cast<T>(23)- z[21];
    z[21]=z[11]*z[21];
    z[25]=static_cast<T>(149)+ n<T>(81,8)*z[8];
    z[25]=z[8]*z[25];
    z[19]=z[19] + n<T>(1,8)*z[25] + n<T>(1,2)*z[23] + z[21];
    z[19]=z[3]*z[19];
    z[21]=z[38] + n<T>(1315,96) + z[14];
    z[23]=3*z[11];
    z[25]=n<T>(9,8)*z[11];
    z[41]= - static_cast<T>(4)+ z[25];
    z[41]=z[41]*z[23];
    z[43]= - n<T>(285,128) - z[31];
    z[43]=z[8]*z[43];
    z[43]= - n<T>(369,32) + z[43];
    z[43]=z[8]*z[43];
    z[19]=n<T>(1,2)*z[19] + z[43] + z[20] + n<T>(1,4)*z[21] + z[41];
    z[19]=z[3]*z[19];
    z[21]= - n<T>(11,4) + z[35];
    z[21]= - n<T>(7,2)*z[27] + n<T>(5,8)*z[21] + z[26];
    z[35]=n<T>(1,3)*z[8];
    z[41]= - n<T>(5,128) - z[35];
    z[41]=z[41]*z[34];
    z[36]=n<T>(5,64)*z[36];
    z[41]=z[36] + z[41];
    z[41]=z[2]*z[41];
    z[43]=n<T>(21,64) + z[31];
    z[43]=z[8]*z[43];
    z[43]=n<T>(3,16) + z[43];
    z[43]=z[8]*z[43];
    z[41]=z[41] - z[30] + z[43];
    z[41]=z[2]*z[41];
    z[43]=z[31] + n<T>(123,128);
    z[43]=z[43]*z[8];
    z[43]=z[43] + n<T>(77,64);
    z[43]=z[43]*z[8];
    z[21]=z[41] + n<T>(1,4)*z[21] - z[43];
    z[21]=z[2]*z[21];
    z[38]=z[38] + z[9];
    z[41]=static_cast<T>(17)- z[24];
    z[41]=z[11]*z[41];
    z[41]=z[41] + n<T>(13,24) - z[12] - z[38];
    z[44]=n<T>(51,32) + z[31];
    z[44]=z[8]*z[44];
    z[44]=n<T>(319,64) + z[44];
    z[44]=z[8]*z[44];
    z[19]=z[19] + z[21] + n<T>(1,4)*z[41] + z[44];
    z[19]=z[4]*z[19];
    z[21]=z[8] + n<T>(15,128);
    z[41]=z[34]*z[2];
    z[21]=z[21]*z[41];
    z[33]= - n<T>(3,8) - z[33];
    z[33]=z[8]*z[33];
    z[33]=z[21] + n<T>(3,8)*z[27] + z[33];
    z[33]=z[2]*z[33];
    z[38]= - z[3]*z[38];
    z[38]=z[38] + n<T>(1,2) - z[9];
    z[38]=z[3]*z[38];
    z[38]=z[38] + z[9] + n<T>(55,32) - z[26];
    z[19]=z[19] + z[33] + z[43] + n<T>(1,4)*z[38];
    z[19]=z[4]*z[19];
    z[33]=n<T>(157,3)*z[9] + n<T>(51,2) + z[28];
    z[22]=z[33]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[33]=z[27]*z[2];
    z[38]=n<T>(57,32) + 11*z[9];
    z[38]=z[9]*z[38];
    z[38]=z[38] + n<T>(41,16)*z[33];
    z[38]=z[2]*z[38];
    z[22]=n<T>(1,4)*z[22] + z[38];
    z[38]=z[2]*z[9];
    z[43]=static_cast<T>(373)+ 955*z[9];
    z[43]=n<T>(1,12)*z[43] - 51*z[38];
    z[43]=z[2]*z[43];
    z[44]= - static_cast<T>(201)+ 401*z[9];
    z[43]=n<T>(1,4)*z[44] + z[43];
    z[44]=13*z[3];
    z[45]=n<T>(1,2)*z[2];
    z[46]= - n<T>(1,3) - z[45];
    z[46]=z[2]*z[46];
    z[46]=n<T>(1,6) + z[46];
    z[46]=z[46]*z[44];
    z[43]=n<T>(1,16)*z[43] + z[46];
    z[43]=z[3]*z[43];
    z[22]=n<T>(1,2)*z[22] + z[43];
    z[22]=z[3]*z[22];
    z[43]= - static_cast<T>(1)+ 3*z[9];
    z[46]=z[43]*z[20];
    z[22]=z[46] + z[22];
    z[22]=z[3]*z[22];
    z[46]=n<T>(421,4) + 67*z[9];
    z[46]=z[9]*z[46];
    z[46]=n<T>(1,3)*z[46] - 17*z[33];
    z[46]=z[46]*z[45];
    z[47]= - n<T>(185,8) + n<T>(59,3)*z[9];
    z[47]=z[9]*z[47];
    z[46]=z[47] + z[46];
    z[47]= - z[9] - z[38];
    z[47]=z[2]*z[47]*z[39];
    z[46]=n<T>(1,8)*z[46] + z[47];
    z[46]=z[3]*z[46];
    z[43]= - z[9]*z[43];
    z[43]=z[43] - z[33];
    z[43]=n<T>(1,4)*z[43] + z[46];
    z[43]=z[3]*z[43];
    z[43]= - n<T>(1,4)*z[27] + z[43];
    z[43]=z[3]*z[43];
    z[46]=z[27] + z[33];
    z[47]= - z[27] - n<T>(1,2)*z[33];
    z[47]=z[2]*z[47];
    z[47]= - n<T>(1,2)*z[27] + z[47];
    z[47]=z[47]*z[17];
    z[46]=n<T>(1,4)*z[46] + z[47];
    z[46]=z[3]*z[46];
    z[46]=n<T>(1,8)*z[27] + z[46];
    z[46]=z[46]*npow(z[3],2);
    z[47]=npow(z[4],2)*z[33];
    z[46]=z[46] - n<T>(1,8)*z[47];
    z[46]=z[5]*z[46];
    z[47]=z[3] - 1;
    z[47]=z[9]*z[47];
    z[47]= - z[33] + z[47];
    z[33]=z[4]*z[33];
    z[33]=n<T>(1,4)*z[47] + z[33];
    z[33]=z[4]*z[33];
    z[33]=z[46] + z[43] + n<T>(1,2)*z[33];
    z[33]=z[5]*z[33];
    z[43]=static_cast<T>(2)+ z[6];
    z[43]=z[43]*z[35];
    z[46]=static_cast<T>(21)+ n<T>(5,2)*z[6];
    z[43]=n<T>(1,64)*z[46] + z[43];
    z[43]=z[8]*z[43];
    z[43]=n<T>(3,16) + z[43];
    z[43]=z[8]*z[43];
    z[46]=z[9] - 1;
    z[46]=z[46]*z[9];
    z[19]=z[33] + z[19] + z[22] - z[21] + n<T>(1,8)*z[46] + z[43];
    z[19]=z[5]*z[19];
    z[21]= - z[37]*z[28];
    z[21]=n<T>(191,4) + z[21];
    z[21]=n<T>(1,4)*z[21] - 21*z[9];
    z[21]=z[9]*z[21];
    z[22]= - static_cast<T>(19)- 41*z[9];
    z[22]=z[2]*z[22]*z[20];
    z[21]=z[21] + z[22];
    z[22]=static_cast<T>(73)- 129*z[9];
    z[22]=n<T>(1,4)*z[22] + 41*z[38];
    z[22]=z[2]*z[22];
    z[33]=static_cast<T>(75)- 433*z[9];
    z[22]=n<T>(1,2)*z[33] + z[22];
    z[33]= - npow(z[2],2);
    z[33]=z[33] + 1;
    z[18]=z[33]*z[18];
    z[33]=static_cast<T>(253)+ n<T>(325,4)*z[1];
    z[33]= - n<T>(17,2)*z[2] + n<T>(1,12)*z[33] - z[9];
    z[33]=z[2]*z[33];
    z[33]=z[33] - z[9] + n<T>(5,3) - n<T>(217,16)*z[1];
    z[18]=n<T>(1,4)*z[33] + z[18];
    z[18]=z[3]*z[18];
    z[18]=n<T>(1,16)*z[22] + z[18];
    z[18]=z[3]*z[18];
    z[18]=n<T>(1,4)*z[21] + z[18];
    z[18]=z[3]*z[18];
    z[21]= - static_cast<T>(1)- n<T>(3,2)*z[6];
    z[22]= - static_cast<T>(2)- 3*z[6];
    z[22]=z[8]*z[22];
    z[21]=n<T>(5,32)*z[21] + z[22];
    z[21]=z[8]*z[21];
    z[21]= - n<T>(43,64) + z[21];
    z[21]=z[8]*z[21];
    z[22]=z[32]*z[41];
    z[21]=z[21] + 3*z[22];
    z[21]=z[2]*z[21];
    z[22]= - z[46] + n<T>(31,32) - z[26];
    z[32]=z[6] + 1;
    z[33]=z[8]*z[6];
    z[37]=z[32]*z[33];
    z[32]=z[6]*z[32];
    z[32]=static_cast<T>(81)+ 5*z[32];
    z[32]=n<T>(1,64)*z[32] + z[37];
    z[32]=z[8]*z[32];
    z[37]=static_cast<T>(247)+ 43*z[6];
    z[32]=n<T>(1,128)*z[37] + z[32];
    z[32]=z[8]*z[32];
    z[15]=z[19] + z[15] + z[18] + z[21] + n<T>(1,4)*z[22] + z[32];
    z[15]=z[5]*z[15];
    z[18]=z[12] + 1;
    z[18]=z[18]*z[9];
    z[19]=static_cast<T>(973)+ 277*z[1];
    z[19]=z[1]*z[19];
    z[19]=static_cast<T>(27)+ n<T>(1,24)*z[19];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(17,4)*z[16] + n<T>(1,2)*z[19] - z[18];
    z[16]=z[2]*z[16];
    z[19]= - static_cast<T>(13)+ n<T>(139,3)*z[1];
    z[19]=z[1]*z[19];
    z[16]=z[16] - z[18] + n<T>(1,8)*z[19] - n<T>(65,12) + z[14];
    z[19]=z[1] + 2;
    z[19]=z[19]*z[1];
    z[19]=z[19] + 1;
    z[21]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[21]=z[21]*z[1];
    z[21]=z[21] + n<T>(1,2);
    z[22]= - z[2]*z[21];
    z[22]=z[22] + z[19];
    z[22]=z[2]*z[22];
    z[22]=n<T>(1,3)*z[22] - z[21];
    z[22]=z[22]*z[44];
    z[16]=n<T>(1,4)*z[16] + z[22];
    z[16]=z[3]*z[16];
    z[22]=n<T>(1,16)*z[1];
    z[32]=n<T>(55,2)*z[1];
    z[37]=n<T>(7,3) + z[32];
    z[37]=z[37]*z[22];
    z[37]=z[37] + n<T>(1249,24) - z[40];
    z[38]= - static_cast<T>(49)+ n<T>(89,8)*z[1];
    z[38]=n<T>(41,8)*z[2] + n<T>(1,2)*z[38] + z[18];
    z[38]=z[38]*z[45];
    z[18]=z[38] + n<T>(1,2)*z[37] + z[18];
    z[16]=n<T>(1,2)*z[18] + z[16];
    z[16]=z[3]*z[16];
    z[18]=n<T>(609,16) - z[12];
    z[18]=z[18]*z[20];
    z[37]= - n<T>(63,16) + z[12];
    z[38]= - n<T>(19,4) + 9*z[9];
    z[38]=z[2]*z[38];
    z[16]=z[16] + n<T>(1,4)*z[38] - n<T>(289,64)*z[8] + z[18] - n<T>(57,64)*z[1] + 1.
   /2.*z[37] - z[24];
    z[16]=z[3]*z[16];
    z[18]= - n<T>(707,24) + z[14];
    z[37]= - n<T>(241,3) - z[32];
    z[37]=z[1]*z[37];
    z[18]=n<T>(1,32)*z[37] + n<T>(1,2)*z[18] - z[23];
    z[37]=n<T>(1,48)*z[1];
    z[38]=static_cast<T>(7)- n<T>(113,2)*z[1];
    z[38]=z[38]*z[37];
    z[14]=z[38] + n<T>(191,8) - z[14];
    z[17]= - z[21]*z[17];
    z[21]=n<T>(179,3) + 37*z[1];
    z[21]=z[1]*z[21];
    z[21]=n<T>(5,16)*z[21] + n<T>(97,12) + z[12];
    z[17]=n<T>(1,4)*z[21] + z[17];
    z[17]=z[3]*z[17];
    z[14]=n<T>(1,4)*z[14] + z[17];
    z[14]=z[3]*z[14];
    z[14]=z[14] + n<T>(1,2)*z[18] - z[31];
    z[14]=z[3]*z[14];
    z[17]=n<T>(37,6) - z[12];
    z[17]=n<T>(57,32)*z[1] + n<T>(1,2)*z[17] + z[23];
    z[18]= - z[27] + z[26] - n<T>(27,16);
    z[18]=n<T>(1,4)*z[18];
    z[21]=z[18] - z[35];
    z[21]=z[2]*z[21];
    z[14]=z[14] + z[21] + n<T>(1,2)*z[17] + z[31];
    z[14]=z[4]*z[14];
    z[17]=n<T>(1,2) + z[8];
    z[17]=z[17]*z[35];
    z[17]= - n<T>(1,3)*z[41] - z[18] + z[17];
    z[17]=z[2]*z[17];
    z[18]= - static_cast<T>(155)+ 113*z[1];
    z[18]=z[18]*z[37];
    z[18]=z[18] - n<T>(323,4) + z[42];
    z[19]=z[19]*z[39];
    z[21]= - static_cast<T>(827)- 555*z[1];
    z[21]=z[1]*z[21];
    z[19]=z[19] + n<T>(1,64)*z[21] - static_cast<T>(5)- n<T>(3,4)*z[12];
    z[19]=z[3]*z[19];
    z[18]=n<T>(1,4)*z[18] + z[19];
    z[18]=z[3]*z[18];
    z[19]=n<T>(349,3) + z[32];
    z[19]=z[19]*z[22];
    z[19]=z[19] + 17*z[11] + n<T>(793,12) - z[40];
    z[18]=z[18] + n<T>(1,4)*z[19] + n<T>(5,3)*z[8];
    z[18]=z[3]*z[18];
    z[19]= - n<T>(57,16)*z[1] - 11*z[11] - n<T>(63,8) + z[12];
    z[14]=z[14] + z[18] + z[17] + n<T>(1,4)*z[19] - z[8];
    z[14]=z[4]*z[14];
    z[17]=n<T>(1,2)*z[6];
    z[18]=n<T>(15,64) - z[7];
    z[18]=z[18]*z[17];
    z[19]=z[6]*z[7];
    z[19]=z[19] + 3;
    z[21]=n<T>(1,3)*z[7];
    z[22]= - z[21] + z[19];
    z[22]=z[22]*z[33];
    z[18]=z[22] + z[18] - n<T>(3,4) + z[21];
    z[18]=z[8]*z[18];
    z[18]=n<T>(19,128) + z[18];
    z[18]=z[8]*z[18];
    z[22]= - n<T>(1,2) - z[10];
    z[22]=z[22]*z[30];
    z[23]=z[21]*z[6];
    z[23]=z[23] + 1;
    z[26]= - z[8]*z[23];
    z[26]= - n<T>(5,128) + z[26];
    z[26]=z[26]*z[34];
    z[26]=z[36] + z[26];
    z[26]=z[2]*z[26];
    z[18]=z[26] + z[22] + z[18];
    z[18]=z[2]*z[18];
    z[22]= - n<T>(19,32) - z[7];
    z[22]=z[6]*z[22];
    z[22]=z[22] - n<T>(81,32) + z[21];
    z[19]=n<T>(2,3)*z[7] - z[19];
    z[19]=z[6]*z[19];
    z[19]=z[21] + z[19];
    z[19]=z[6]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[8]*z[19];
    z[26]= - n<T>(15,128) + z[7];
    z[26]=z[6]*z[26];
    z[26]=z[26] + n<T>(3,2) - n<T>(4,3)*z[7];
    z[26]=z[6]*z[26];
    z[19]=z[19] - n<T>(97,96) + z[26];
    z[19]=z[8]*z[19];
    z[19]=n<T>(1,2)*z[22] + z[19];
    z[19]=z[8]*z[19];
    z[22]=static_cast<T>(1)+ z[10];
    z[22]=z[22]*z[28];
    z[22]=n<T>(1,2) + z[22];
    z[18]=z[18] + n<T>(1,16)*z[22] + z[19];
    z[18]=z[2]*z[18];
    z[19]=z[29]*z[28];
    z[22]=n<T>(49,2) - z[19];
    z[22]=z[22]*z[20];
    z[19]=z[22] - z[24] - n<T>(13,2) + z[19];
    z[19]=z[19]*z[20];
    z[20]=n<T>(5,64) - z[7];
    z[17]=z[20]*z[17];
    z[17]=z[17] - n<T>(3,4) + z[7];
    z[17]=z[6]*z[17];
    z[20]=n<T>(43,16) - z[21];
    z[17]=n<T>(1,2)*z[20] + z[17];
    z[17]=z[6]*z[17];
    z[20]= - z[21] + z[23];
    z[20]=z[6]*z[20];
    z[20]= - z[21] + z[20];
    z[20]=z[6]*z[20];
    z[20]=z[20] + z[21] - 1;
    z[20]=z[20]*z[33];
    z[22]=static_cast<T>(2)- z[7];
    z[17]=z[20] + n<T>(1,3)*z[22] + z[17];
    z[17]=z[8]*z[17];
    z[20]=n<T>(19,64) + z[7];
    z[20]=z[6]*z[20];
    z[20]=n<T>(81,32) + z[20];
    z[20]=z[6]*z[20];
    z[20]=z[20] + n<T>(171,32) + z[7];
    z[17]=n<T>(1,2)*z[20] + z[17];
    z[17]=z[8]*z[17];
    z[20]= - static_cast<T>(1)- n<T>(3,4)*z[10];
    z[13]=z[20]*z[13];
    z[20]= - z[25] + n<T>(21,8) - z[7];
    z[20]=z[11]*z[20];
    z[22]= - n<T>(3,16) - z[21];
    z[22]=z[6]*z[22];

    r +=  - n<T>(45,64) - n<T>(55,128)*z[1] + z[13] + z[14] + z[15] + z[16] + 
      z[17] + z[18] + z[19] + z[20] + z[21] + z[22];
 
    return r;
}

template double qg_2lha_r1206(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1206(const std::array<dd_real,30>&);
#endif
