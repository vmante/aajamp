#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1782(const std::array<T,30>& k) {
  T z[9];
  T r = 0;

    z[1]=k[4];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[14];
    z[5]=k[9];
    z[6]=z[4]*z[5];
    z[7]=static_cast<T>(1)- z[1];
    z[7]=z[1]*z[7];
    z[7]=z[4] - static_cast<T>(1)+ z[7];
    z[7]=z[2]*z[7];
    z[7]=z[6] + z[7];
    z[7]=z[3]*z[7];
    z[8]= - static_cast<T>(1)- z[1];
    z[8]=n<T>(1,3)*z[8] + z[4];
    z[8]=z[2]*z[8];
    z[7]=n<T>(2,3)*z[7] + z[6] + z[8];
    z[7]=z[3]*z[7];
    z[8]= - n<T>(2,3) + z[4];
    z[8]=z[2]*z[8];
    z[6]=z[7] + z[6] + z[8];

    r += n<T>(1,9)*z[6]*z[3];
 
    return r;
}

template double qg_2lha_r1782(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1782(const std::array<dd_real,30>&);
#endif
