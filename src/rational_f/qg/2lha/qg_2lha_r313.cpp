#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r313(const std::array<T,30>& k) {
  T z[10];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[5];
    z[4]=k[10];
    z[5]=k[9];
    z[6]=static_cast<T>(3)- z[2];
    z[6]=z[2]*z[6];
    z[6]= - static_cast<T>(5)+ z[6];
    z[7]=n<T>(7,2) - z[5];
    z[7]=z[5]*z[7];
    z[6]=n<T>(1,2)*z[6] + z[7];
    z[7]= - static_cast<T>(1)- 3*z[1];
    z[8]=z[2] - z[1];
    z[9]= - static_cast<T>(1)+ z[8];
    z[9]=z[2]*z[9];
    z[9]=n<T>(1,2)*z[9] + n<T>(1,2) + z[1];
    z[9]=z[2]*z[9];
    z[7]=n<T>(1,2)*z[7] + z[9];
    z[9]= - n<T>(1,4)*z[5] + 1;
    z[9]=z[1]*z[9];
    z[9]=n<T>(1,4) + z[9];
    z[9]=z[5]*z[9];
    z[7]=n<T>(1,2)*z[7] + z[9];
    z[7]=z[3]*z[7];
    z[6]=n<T>(1,2)*z[6] + z[7];
    z[6]=z[4]*z[6];
    z[7]=z[2]*z[8];
    z[7]= - static_cast<T>(1)+ z[7];
    z[8]=n<T>(1,2)*z[5];
    z[9]= - z[8] + n<T>(3,2);
    z[9]=z[1]*z[9];
    z[9]=static_cast<T>(1)+ z[9];
    z[9]=z[5]*z[9];
    z[7]=n<T>(1,2)*z[7] + z[9];
    z[7]=z[3]*z[7];
    z[9]=static_cast<T>(1)- n<T>(3,8)*z[5];
    z[9]=z[5]*z[9];
    z[6]=n<T>(1,2)*z[6] + n<T>(1,4)*z[7] - n<T>(1,4) + z[9];
    z[6]=z[4]*z[6];
    z[7]=static_cast<T>(1)- z[8];
    z[7]=z[5]*z[7];
    z[8]=z[3]*z[8];
    z[7]=z[7] + z[8];

    r += z[6] + n<T>(1,4)*z[7];
 
    return r;
}

template double qg_2lha_r313(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r313(const std::array<dd_real,30>&);
#endif
