#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r521(const std::array<T,30>& k) {
  T z[87];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[6];
    z[5]=k[13];
    z[6]=k[2];
    z[7]=k[10];
    z[8]=k[14];
    z[9]=k[7];
    z[10]=k[5];
    z[11]=k[9];
    z[12]=k[8];
    z[13]=k[11];
    z[14]=k[3];
    z[15]=k[27];
    z[16]=n<T>(1,2)*z[2];
    z[17]=npow(z[7],2);
    z[18]=z[16]*z[17];
    z[19]=11*z[7];
    z[20]=z[7] - 1;
    z[21]=z[20]*z[19];
    z[21]=z[21] - z[18];
    z[22]=n<T>(1,3)*z[2];
    z[21]=z[21]*z[22];
    z[23]=n<T>(1,3)*z[7];
    z[24]=static_cast<T>(1)+ n<T>(23,2)*z[7];
    z[24]=z[24]*z[23];
    z[21]=z[21] - n<T>(7,2) + z[24];
    z[24]=npow(z[2],3);
    z[25]=n<T>(1,4)*z[24];
    z[21]=z[21]*z[25];
    z[26]=z[7] + 1;
    z[27]=z[26]*z[7];
    z[28]=z[18] + z[27];
    z[28]=z[28]*z[2];
    z[29]=n<T>(1,2)*z[7];
    z[30]=z[29] + 1;
    z[31]=z[30]*z[7];
    z[32]=z[31] + n<T>(1,2);
    z[28]=z[28] + z[32];
    z[33]=z[3]*npow(z[2],4);
    z[34]=z[28]*z[33];
    z[21]=z[21] + z[34];
    z[21]=z[3]*z[21];
    z[35]=z[17]*z[2];
    z[36]=n<T>(43,4)*z[7];
    z[37]= - static_cast<T>(1)- z[36];
    z[37]=z[7]*z[37];
    z[37]=z[37] - n<T>(11,2)*z[35];
    z[37]=z[37]*z[22];
    z[37]=n<T>(3,2) + z[37];
    z[37]=z[2]*z[37];
    z[37]=n<T>(1,4) + z[37];
    z[38]=n<T>(1,4)*z[2];
    z[37]=z[37]*z[38];
    z[21]=z[37] + z[21];
    z[37]=z[18] + z[7];
    z[37]=z[37]*z[2];
    z[39]=z[7] - n<T>(1,2);
    z[40]= - z[37] + z[39];
    z[40]=z[2]*z[40];
    z[37]= - n<T>(1,2) - z[37];
    z[37]=z[6]*z[37];
    z[37]=z[37] + static_cast<T>(1)+ z[40];
    z[37]=z[6]*z[37];
    z[40]= - z[31] - z[18];
    z[40]=z[2]*z[40];
    z[40]= - n<T>(1,2) + z[40];
    z[40]=z[2]*z[40];
    z[40]=n<T>(1,2) + z[40];
    z[40]=z[2]*z[40];
    z[37]=z[37] - n<T>(1,2) + z[40];
    z[37]=z[2]*z[37];
    z[37]=z[34] + z[37];
    z[37]=z[9]*z[37];
    z[40]= - n<T>(23,16) - z[7];
    z[40]=z[7]*z[40];
    z[40]=z[40] - n<T>(43,32)*z[35];
    z[40]=z[40]*z[22];
    z[41]= - n<T>(1,16) + z[23];
    z[40]=n<T>(1,2)*z[41] + z[40];
    z[40]=z[2]*z[40];
    z[41]=z[35] + z[7];
    z[42]=z[41]*z[2];
    z[43]=z[42]*z[6];
    z[40]=z[40] - n<T>(1,6)*z[43];
    z[40]=z[6]*z[40];
    z[21]=n<T>(1,3)*z[37] + n<T>(1,2)*z[21] + z[40];
    z[21]=z[9]*z[21];
    z[37]= - n<T>(1,2) - z[7];
    z[37]=z[37]*z[23];
    z[40]= - static_cast<T>(1)- n<T>(49,3)*z[7];
    z[40]=z[7]*z[40];
    z[40]=z[40] - 11*z[35];
    z[40]=z[2]*z[40];
    z[37]=n<T>(1,16)*z[40] - n<T>(3,8) + z[37];
    z[37]=z[2]*z[37];
    z[37]= - z[23] + z[37];
    z[40]=n<T>(3,2)*z[35];
    z[43]=z[7] - 5;
    z[44]=z[7]*z[43];
    z[44]=z[44] - z[40];
    z[44]=z[2]*z[44];
    z[45]=n<T>(5,2)*z[7];
    z[46]= - static_cast<T>(1)+ z[45];
    z[46]=z[7]*z[46];
    z[44]=z[44] - n<T>(7,2) + z[46];
    z[25]=z[44]*z[25];
    z[25]=z[25] + z[34];
    z[25]=z[3]*z[25];
    z[44]=n<T>(5,4) + z[7];
    z[44]=z[7]*z[44];
    z[44]=z[44] + z[35];
    z[44]=z[44]*z[16];
    z[46]=n<T>(1,8) + z[7];
    z[46]=z[7]*z[46];
    z[44]=z[44] + n<T>(9,8) + z[46];
    z[46]=npow(z[2],2);
    z[47]=n<T>(1,2)*z[46];
    z[44]=z[44]*z[47];
    z[25]=z[44] + z[25];
    z[25]=z[3]*z[25];
    z[44]= - n<T>(1,32) - z[23];
    z[44]=z[7]*z[44];
    z[44]=z[44] - n<T>(9,32)*z[35];
    z[44]=z[2]*z[44];
    z[48]=n<T>(1,6)*z[7];
    z[49]=n<T>(1,3)*z[8];
    z[50]=n<T>(1,4) - z[49];
    z[50]=z[8]*z[50];
    z[44]=z[50] + z[48] + z[44];
    z[44]=z[6]*z[44];
    z[50]=z[2] + 1;
    z[51]=z[49]*z[50];
    z[22]= - n<T>(1,2) - z[22];
    z[22]=n<T>(1,2)*z[22] + z[51];
    z[22]=z[8]*z[22];
    z[21]=z[21] + z[44] + z[22] + n<T>(1,2)*z[37] + z[25];
    z[21]=z[9]*z[21];
    z[22]=npow(z[3],2);
    z[25]= - static_cast<T>(41)- n<T>(65,8)*z[2];
    z[25]=z[2]*z[25];
    z[25]= - n<T>(415,12) + z[25];
    z[25]=z[25]*z[22];
    z[37]=n<T>(47,3) + 7*z[2];
    z[37]=z[37]*z[16];
    z[37]=n<T>(13,3) + z[37];
    z[44]=npow(z[3],3);
    z[37]=z[37]*z[44];
    z[52]=z[2] + 2;
    z[53]=z[52]*z[2];
    z[53]=z[53] + 1;
    z[54]=z[14]*z[53]*npow(z[3],4);
    z[37]=n<T>(7,8)*z[37] - n<T>(1,3)*z[54];
    z[37]=z[14]*z[37];
    z[25]=n<T>(1,4)*z[25] + z[37];
    z[25]=z[14]*z[25];
    z[37]=n<T>(1,3)*z[3];
    z[54]=n<T>(227,16) + z[2];
    z[54]=z[2]*z[54];
    z[54]=n<T>(91,4) + z[54];
    z[54]=z[54]*z[37];
    z[25]=z[54] + z[25];
    z[25]=z[14]*z[25];
    z[54]= - n<T>(259,6) + z[31];
    z[49]= - z[6]*z[49];
    z[25]=z[49] + z[25] + z[51] + n<T>(1,16)*z[54] - n<T>(2,3)*z[2];
    z[25]=z[11]*z[25];
    z[49]=4*z[2];
    z[51]=z[50]*z[3];
    z[54]= - z[51]*z[49];
    z[55]=static_cast<T>(559)+ 547*z[2];
    z[54]=n<T>(1,32)*z[55] + z[54];
    z[54]=z[54]*z[37];
    z[55]= - static_cast<T>(1)- n<T>(3,4)*z[2];
    z[54]=n<T>(3,8)*z[55] + z[54];
    z[54]=z[54]*z[22];
    z[55]=static_cast<T>(1)+ n<T>(7,4)*z[2];
    z[51]=n<T>(1,8)*z[55] - n<T>(2,3)*z[51];
    z[44]=z[44]*z[14];
    z[51]=z[51]*z[44];
    z[51]=z[54] + z[51];
    z[51]=z[14]*z[51];
    z[54]=static_cast<T>(1)- z[38];
    z[55]=n<T>(1003,6) + 163*z[2];
    z[55]=z[2]*z[55];
    z[55]= - n<T>(19,2) + z[55];
    z[55]=z[3]*z[55];
    z[55]=z[55] - static_cast<T>(109)- n<T>(247,2)*z[2];
    z[55]=z[3]*z[55];
    z[54]=3*z[54] + z[55];
    z[54]=z[3]*z[54];
    z[51]=n<T>(1,16)*z[54] + z[51];
    z[51]=z[14]*z[51];
    z[54]=5*z[2];
    z[55]= - n<T>(49,2) - 13*z[2];
    z[55]=z[55]*z[54];
    z[55]=static_cast<T>(19)+ z[55];
    z[55]=z[3]*z[55];
    z[56]= - n<T>(11,3) - z[7];
    z[55]=z[55] + n<T>(1,2)*z[56] + n<T>(113,3)*z[2];
    z[56]=n<T>(1,2)*z[3];
    z[55]=z[55]*z[56];
    z[55]=z[55] + n<T>(3,2) + z[2];
    z[51]=n<T>(1,8)*z[55] + z[51];
    z[51]=z[14]*z[51];
    z[55]=static_cast<T>(1)+ n<T>(3,4)*z[7];
    z[56]= - z[7]*z[55];
    z[56]=n<T>(29,3)*z[2] + n<T>(685,24) + z[56];
    z[57]=z[2]*z[50];
    z[57]= - n<T>(19,8) + n<T>(7,3)*z[57];
    z[57]=z[3]*z[57];
    z[56]=n<T>(1,2)*z[56] + z[57];
    z[49]=n<T>(31,3) + z[49];
    z[49]=z[2]*z[49];
    z[53]=z[8]*z[53];
    z[49]= - 2*z[53] + n<T>(19,3) + z[49];
    z[49]=z[8]*z[49];
    z[53]= - n<T>(95,12) - 2*z[2];
    z[53]=z[2]*z[53];
    z[49]=z[49] - static_cast<T>(6)+ z[53];
    z[49]=z[8]*z[49];
    z[50]=z[8]*z[50];
    z[50]=4*z[50] - n<T>(25,3) - 6*z[2];
    z[50]=z[8]*z[50];
    z[50]=2*z[52] + z[50];
    z[50]=z[8]*z[50];
    z[52]=z[8] - 1;
    z[52]=z[52]*z[6];
    z[53]=npow(z[8],2)*z[52];
    z[50]= - 2*z[53] + n<T>(1,8) + z[50];
    z[50]=z[6]*z[50];
    z[25]=z[25] + z[50] + z[51] + n<T>(1,4)*z[56] + z[49];
    z[25]=z[11]*z[25];
    z[49]= - static_cast<T>(205)+ 1093*z[7];
    z[49]=z[49]*z[48];
    z[50]= - n<T>(3,2) + n<T>(25,3)*z[7];
    z[50]=z[50]*z[19];
    z[50]=z[50] - n<T>(23,2)*z[35];
    z[50]=z[2]*z[50];
    z[49]=z[50] - static_cast<T>(9)+ z[49];
    z[49]=z[49]*z[16];
    z[50]=3*z[7];
    z[51]= - z[20]*z[50]*z[44];
    z[53]= - static_cast<T>(7)+ z[7];
    z[53]=z[7]*z[53]*z[22];
    z[51]=z[53] + z[51];
    z[51]=z[14]*z[51];
    z[43]= - z[3]*z[43]*z[29];
    z[43]=z[43] + z[51];
    z[43]=z[14]*z[43];
    z[43]= - z[7] + z[43];
    z[51]=n<T>(1,2)*z[14];
    z[43]=z[43]*z[51];
    z[53]=7*z[22] - 3*z[44];
    z[53]=z[14]*z[53];
    z[53]= - n<T>(11,2)*z[3] + z[53];
    z[51]=z[53]*z[51];
    z[51]=static_cast<T>(1)+ z[51];
    z[51]=z[14]*z[51];
    z[51]=n<T>(1,4)*z[6] - n<T>(1,4) + z[51];
    z[51]=z[11]*z[51];
    z[43]=z[43] + z[51];
    z[43]=z[12]*z[43];
    z[51]=z[24]*z[3];
    z[53]= - z[46] + n<T>(1,2)*z[51];
    z[53]=z[3]*z[53];
    z[53]=z[16] + z[53];
    z[53]=z[3]*z[53];
    z[56]=n<T>(1,2)*z[4];
    z[57]= - static_cast<T>(1)+ z[4];
    z[57]=z[5]*z[57]*z[56];
    z[53]=7*z[53] + z[57];
    z[53]=z[1]*z[53];
    z[57]= - n<T>(67,2) + n<T>(115,3)*z[7];
    z[57]=z[7]*z[57];
    z[43]=z[53] + z[43] + z[57] + z[49];
    z[49]=4*z[7];
    z[53]=21*z[7];
    z[57]= - static_cast<T>(4)- z[53];
    z[57]=z[57]*z[49];
    z[58]=9*z[7];
    z[59]= - static_cast<T>(1)- z[58];
    z[59]=z[7]*z[59];
    z[59]=z[59] - z[35];
    z[60]=3*z[2];
    z[59]=z[59]*z[60];
    z[57]=z[57] + z[59];
    z[57]=z[2]*z[57];
    z[59]=30*z[7];
    z[61]= - static_cast<T>(7)- z[59];
    z[61]=z[61]*z[49];
    z[57]=z[61] + z[57];
    z[57]=z[2]*z[57];
    z[61]= - static_cast<T>(20)- 81*z[7];
    z[61]=z[7]*z[61];
    z[57]=z[57] + static_cast<T>(4)+ z[61];
    z[57]=z[2]*z[57];
    z[61]=2*z[7];
    z[62]=7*z[7];
    z[63]=static_cast<T>(2)+ z[62];
    z[63]=z[63]*z[61];
    z[64]=6*z[7];
    z[65]=z[64] + 1;
    z[65]=z[65]*z[7];
    z[65]=z[65] + z[35];
    z[66]=z[2]*z[65];
    z[63]=z[63] + z[66];
    z[63]=z[2]*z[63];
    z[66]=8*z[7];
    z[67]=static_cast<T>(3)+ z[66];
    z[67]=z[67]*z[61];
    z[63]=z[67] + z[63];
    z[63]=z[2]*z[63];
    z[67]=static_cast<T>(4)+ z[58];
    z[67]=z[7]*z[67];
    z[63]=z[63] - static_cast<T>(1)+ z[67];
    z[63]=z[2]*z[63];
    z[67]=z[61] + 1;
    z[67]=z[67]*z[7];
    z[63]=z[67] + z[63];
    z[68]=3*z[8];
    z[63]=z[63]*z[68];
    z[53]= - static_cast<T>(5)- z[53];
    z[53]=z[7]*z[53];
    z[53]=z[63] + z[53] + z[57];
    z[53]=z[8]*z[53];
    z[57]=n<T>(17,2) + 173*z[7];
    z[57]=z[57]*z[29];
    z[63]=67*z[7];
    z[69]=static_cast<T>(5)+ z[63];
    z[69]=z[69]*z[50];
    z[69]=z[69] + 35*z[35];
    z[69]=z[2]*z[69];
    z[70]=static_cast<T>(35)+ 407*z[7];
    z[70]=z[7]*z[70];
    z[69]=z[70] + z[69];
    z[69]=z[69]*z[38];
    z[57]=z[69] - static_cast<T>(1)+ z[57];
    z[57]=z[2]*z[57];
    z[28]=z[28]*z[3];
    z[69]=n<T>(5,2)*z[35];
    z[70]=z[50] + 1;
    z[71]=z[70]*z[7];
    z[72]=z[71] + z[69];
    z[72]=z[2]*z[72];
    z[73]= - static_cast<T>(1)+ z[29];
    z[73]=z[7]*z[73];
    z[72]=z[72] - n<T>(3,2) + z[73];
    z[72]=n<T>(1,2)*z[72] + z[28];
    z[72]=z[3]*z[72];
    z[73]= - static_cast<T>(5)+ 107*z[7];
    z[73]=z[7]*z[73];
    z[73]=static_cast<T>(1)+ z[73];
    z[53]=z[53] + z[72] + n<T>(1,4)*z[73] + z[57];
    z[53]=z[8]*z[53];
    z[57]=z[7] + 3;
    z[72]=z[57]*z[7];
    z[73]= - z[72] + z[40];
    z[73]=z[2]*z[73];
    z[74]= - static_cast<T>(7)- z[45];
    z[74]=z[7]*z[74];
    z[73]=z[73] - n<T>(9,2) + z[74];
    z[74]=z[61]*z[26];
    z[74]=z[74] + z[35];
    z[74]=z[74]*z[2];
    z[75]=z[7] + 2;
    z[76]=z[75]*z[7];
    z[74]=z[74] + z[76] + 1;
    z[76]=z[3]*z[74];
    z[73]=n<T>(1,2)*z[73] + z[76];
    z[73]=z[3]*z[73];
    z[76]=z[20]*z[29];
    z[76]=z[76] - 1;
    z[77]= - n<T>(1,4) - z[7];
    z[77]=z[77]*z[50];
    z[78]=n<T>(5,4)*z[35];
    z[77]=z[77] - z[78];
    z[77]=z[2]*z[77];
    z[73]=z[73] - n<T>(3,2)*z[76] + z[77];
    z[73]=z[3]*z[73];
    z[77]=19*z[7];
    z[79]=static_cast<T>(7)- z[77];
    z[79]=z[79]*z[50];
    z[79]= - static_cast<T>(1)+ z[79];
    z[80]=31*z[7];
    z[81]=static_cast<T>(1)- z[80];
    z[81]=z[7]*z[81];
    z[81]=z[81] + z[35];
    z[81]=z[81]*z[16];
    z[82]=63*z[7];
    z[83]=n<T>(5,2) - z[82];
    z[83]=z[7]*z[83];
    z[81]=z[83] + z[81];
    z[81]=z[2]*z[81];
    z[83]=static_cast<T>(11)- 75*z[7];
    z[83]=z[7]*z[83];
    z[81]=z[83] + z[81];
    z[81]=z[2]*z[81];
    z[79]=n<T>(1,2)*z[79] + z[81];
    z[53]=z[53] + n<T>(1,2)*z[79] + z[73];
    z[53]=z[8]*z[53];
    z[55]= - z[55]*z[50];
    z[73]=static_cast<T>(17)+ z[77];
    z[73]=z[7]*z[73];
    z[73]=z[73] + 47*z[35];
    z[73]=z[73]*z[38];
    z[55]=z[55] + z[73];
    z[73]=7*z[35];
    z[77]=static_cast<T>(7)+ n<T>(127,2)*z[7];
    z[77]=z[7]*z[77];
    z[77]=z[77] + z[73];
    z[77]=z[2]*z[77];
    z[79]= - static_cast<T>(17)+ n<T>(559,16)*z[7];
    z[79]=z[7]*z[79];
    z[77]=z[79] + n<T>(3,8)*z[77];
    z[77]=z[2]*z[77];
    z[79]= - n<T>(967,3) + 201*z[7];
    z[79]=z[7]*z[79];
    z[77]=n<T>(1,16)*z[79] + z[77];
    z[77]=z[2]*z[77];
    z[79]=z[20]*z[7];
    z[77]= - n<T>(19,16)*z[79] + z[77];
    z[81]= - 2*z[17] - z[35];
    z[81]=z[2]*z[81];
    z[81]= - z[17] + z[81];
    z[81]=z[3]*z[81]*z[46];
    z[77]=n<T>(1,2)*z[77] + 2*z[81];
    z[77]=z[3]*z[77];
    z[81]= - static_cast<T>(21)- n<T>(499,4)*z[7];
    z[81]=z[7]*z[81];
    z[81]=z[81] - n<T>(69,2)*z[35];
    z[81]=z[2]*z[81];
    z[83]=n<T>(239,2) - n<T>(283,3)*z[7];
    z[83]=z[7]*z[83];
    z[81]=z[83] + z[81];
    z[81]=z[2]*z[81];
    z[83]=static_cast<T>(111)- n<T>(95,12)*z[7];
    z[83]=z[7]*z[83];
    z[81]=z[83] + z[81];
    z[77]=n<T>(1,16)*z[81] + z[77];
    z[77]=z[3]*z[77];
    z[55]=n<T>(1,16)*z[55] + z[77];
    z[55]=z[3]*z[55];
    z[77]= - n<T>(547,8) + 65*z[7];
    z[77]=z[77]*z[23];
    z[81]=n<T>(415,12)*z[17] + z[73];
    z[81]=z[81]*z[16];
    z[77]=z[77] + z[81];
    z[77]=z[2]*z[77];
    z[81]=n<T>(1,8)*z[7];
    z[83]= - n<T>(559,3) + z[82];
    z[83]=z[83]*z[81];
    z[77]=z[83] + z[77];
    z[83]=static_cast<T>(1)- z[61];
    z[83]=z[7]*z[83];
    z[83]=z[83] - z[35];
    z[83]=z[2]*z[83];
    z[83]= - z[79] + z[83];
    z[84]=n<T>(4,3)*z[3];
    z[83]=z[2]*z[83]*z[84];
    z[77]=n<T>(1,4)*z[77] + z[83];
    z[77]=z[3]*z[77];
    z[83]=25*z[7];
    z[84]=static_cast<T>(9)- z[83];
    z[84]=z[7]*z[84];
    z[84]=z[84] - 23*z[35];
    z[84]=z[84]*z[38];
    z[85]=n<T>(1,4)*z[7];
    z[86]=static_cast<T>(3)- z[85];
    z[86]=z[7]*z[86];
    z[84]=z[86] + z[84];
    z[77]=n<T>(1,8)*z[84] + z[77];
    z[22]=z[77]*z[22];
    z[77]= - static_cast<T>(7)+ z[19];
    z[77]=z[7]*z[77];
    z[77]=z[77] + z[73];
    z[77]=z[77]*z[38];
    z[84]= - static_cast<T>(1)+ z[85];
    z[84]=z[7]*z[84];
    z[77]=z[84] + z[77];
    z[20]= - z[20]*z[61];
    z[20]=z[20] - z[35];
    z[20]=z[2]*z[20];
    z[84]=z[7] - 2;
    z[84]=z[84]*z[7];
    z[20]= - z[84] + z[20];
    z[20]=z[20]*z[37];
    z[20]=n<T>(1,8)*z[77] + z[20];
    z[20]=z[20]*z[44];
    z[20]=z[22] + z[20];
    z[20]=z[14]*z[20];
    z[20]=z[55] + z[20];
    z[20]=z[14]*z[20];
    z[22]=static_cast<T>(93)- n<T>(1213,3)*z[7];
    z[22]=z[22]*z[85];
    z[37]= - n<T>(111,4) - 121*z[7];
    z[37]=z[7]*z[37];
    z[37]=z[37] - n<T>(69,4)*z[35];
    z[37]=z[2]*z[37];
    z[22]=z[37] - static_cast<T>(7)+ z[22];
    z[22]=z[2]*z[22];
    z[37]=static_cast<T>(323)- z[83];
    z[37]=z[37]*z[48];
    z[22]=z[22] - static_cast<T>(1)+ z[37];
    z[22]=z[2]*z[22];
    z[37]= - n<T>(19,2) - z[50];
    z[37]=z[7]*z[37];
    z[22]=z[37] + z[22];
    z[37]=static_cast<T>(7)+ n<T>(423,8)*z[7];
    z[37]=z[7]*z[37];
    z[37]=z[37] + n<T>(7,2)*z[35];
    z[37]=z[2]*z[37];
    z[44]=n<T>(17,2) + 111*z[7];
    z[44]=z[7]*z[44];
    z[44]=n<T>(7,2) + z[44];
    z[37]=n<T>(3,4)*z[44] + z[37];
    z[37]=z[2]*z[37];
    z[44]= - n<T>(143,12) + z[82];
    z[44]=z[7]*z[44];
    z[44]=n<T>(9,2) + z[44];
    z[37]=n<T>(1,2)*z[44] + z[37];
    z[37]=z[37]*z[46];
    z[37]=n<T>(19,8)*z[17] + z[37];
    z[44]=z[67] + z[35];
    z[48]= - z[2]*z[44];
    z[48]= - z[27] + z[48];
    z[48]=z[48]*z[51];
    z[37]=n<T>(1,4)*z[37] + n<T>(4,3)*z[48];
    z[37]=z[3]*z[37];
    z[22]=n<T>(1,8)*z[22] + z[37];
    z[22]=z[3]*z[22];
    z[37]=5*z[7];
    z[48]= - n<T>(27,2) + n<T>(5,3)*z[7];
    z[48]=z[48]*z[37];
    z[48]=static_cast<T>(7)+ z[48];
    z[51]=static_cast<T>(2)+ n<T>(237,64)*z[7];
    z[51]=z[7]*z[51];
    z[51]=z[51] + n<T>(47,32)*z[35];
    z[51]=z[2]*z[51];
    z[48]=n<T>(1,32)*z[48] + z[51];
    z[48]=z[2]*z[48];
    z[51]= - static_cast<T>(163)- z[63];
    z[51]=z[7]*z[51];
    z[22]=z[22] + n<T>(1,192)*z[51] + z[48];
    z[22]=z[3]*z[22];
    z[48]=n<T>(13,2)*z[79] - z[35];
    z[48]=z[2]*z[48];
    z[51]= - static_cast<T>(3)+ z[45];
    z[51]=z[7]*z[51];
    z[48]=z[51] + z[48];
    z[20]=z[20] + n<T>(1,16)*z[48] + z[22];
    z[20]=z[14]*z[20];
    z[22]=static_cast<T>(11)+ z[59];
    z[22]=z[22]*z[61];
    z[48]=9*z[2];
    z[51]=z[65]*z[48];
    z[51]=32*z[71] + z[51];
    z[51]=z[2]*z[51];
    z[22]=z[22] + z[51];
    z[22]=z[2]*z[22];
    z[51]= - static_cast<T>(1)- z[49];
    z[51]=z[7]*z[51];
    z[51]=z[51] - z[35];
    z[51]=z[51]*z[60];
    z[51]= - 8*z[67] + z[51];
    z[51]=z[2]*z[51];
    z[55]= - static_cast<T>(5)- z[66];
    z[55]=z[7]*z[55];
    z[51]=z[55] + z[51];
    z[51]=z[2]*z[51];
    z[51]=z[51] + static_cast<T>(1)- z[84];
    z[51]=z[51]*z[68];
    z[55]= - static_cast<T>(17)+ z[58];
    z[55]=z[7]*z[55];
    z[22]=z[51] + z[22] - static_cast<T>(4)+ z[55];
    z[22]=z[8]*z[22];
    z[51]=n<T>(7,4) - z[7];
    z[51]=z[51]*z[58];
    z[55]= - static_cast<T>(1)- n<T>(15,2)*z[7];
    z[55]=z[55]*z[50];
    z[55]=z[55] - z[73];
    z[54]=z[55]*z[54];
    z[55]= - n<T>(23,2) - 95*z[7];
    z[55]=z[7]*z[55];
    z[54]=z[55] + z[54];
    z[54]=z[54]*z[16];
    z[22]=z[22] + z[54] + static_cast<T>(1)+ z[51];
    z[22]=z[8]*z[22];
    z[51]= - n<T>(3,2) + z[7];
    z[51]=z[51]*z[50];
    z[54]= - static_cast<T>(1)+ n<T>(31,2)*z[7];
    z[54]=z[7]*z[54];
    z[54]=z[54] - z[35];
    z[54]=z[54]*z[16];
    z[55]= - n<T>(5,4) + z[19];
    z[55]=z[7]*z[55];
    z[54]=z[55] + z[54];
    z[54]=z[2]*z[54];
    z[22]=z[22] + z[51] + z[54];
    z[22]=z[8]*z[22];
    z[51]= - static_cast<T>(4)- z[50];
    z[49]=z[51]*z[49];
    z[51]=z[71] + z[35];
    z[48]= - z[51]*z[48];
    z[48]=z[49] + z[48];
    z[48]=z[2]*z[48];
    z[49]=z[75]*z[61];
    z[44]=z[44]*z[60];
    z[44]=z[49] + z[44];
    z[44]=z[2]*z[44];
    z[44]= - z[7] + z[44];
    z[44]=z[44]*z[68];
    z[44]=z[44] + z[64] + z[48];
    z[44]=z[8]*z[44];
    z[48]=n<T>(5,4) + z[61];
    z[48]=z[48]*z[50];
    z[48]=z[48] + n<T>(35,4)*z[35];
    z[48]=z[2]*z[48];
    z[44]=z[44] - z[50] + z[48];
    z[44]=z[8]*z[44];
    z[48]=z[41]*z[38];
    z[44]=z[48] + z[44];
    z[44]=z[8]*z[44];
    z[42]= - npow(z[8],3)*z[42]*z[52];
    z[42]=z[44] + 3*z[42];
    z[42]=z[6]*z[42];
    z[44]=static_cast<T>(3)+ 17*z[7];
    z[44]=z[7]*z[44];
    z[44]=z[44] + 19*z[35];
    z[48]=n<T>(1,8)*z[2];
    z[44]=z[44]*z[48];
    z[44]= - z[7] + z[44];
    z[22]=z[42] + n<T>(1,8)*z[44] + z[22];
    z[22]=z[6]*z[22];
    z[42]=static_cast<T>(31)+ z[58];
    z[42]=z[42]*z[29];
    z[42]=static_cast<T>(11)+ z[42];
    z[44]=n<T>(5,8) - z[7];
    z[44]=z[7]*z[44];
    z[44]=z[44] - n<T>(17,8)*z[35];
    z[44]=z[2]*z[44];
    z[42]= - n<T>(13,4)*z[28] + n<T>(1,4)*z[42] + z[44];
    z[42]=z[3]*z[42];
    z[44]=static_cast<T>(13)+ z[80];
    z[44]=z[44]*z[29];
    z[44]= - static_cast<T>(9)+ z[44];
    z[49]=n<T>(23,8) + z[58];
    z[49]=z[7]*z[49];
    z[49]=z[49] + n<T>(15,8)*z[35];
    z[49]=z[2]*z[49];
    z[44]=n<T>(1,4)*z[44] + z[49];
    z[42]=n<T>(1,2)*z[44] + z[42];
    z[42]=z[3]*z[42];
    z[44]= - static_cast<T>(1)+ z[17];
    z[44]=z[44]*z[16];
    z[49]=n<T>(3,2) + z[7];
    z[49]=z[7]*z[49];
    z[52]=z[14]*z[31];
    z[44]= - z[52] + z[49] + z[44];
    z[49]= - 9*z[27] - z[78];
    z[49]=z[2]*z[49];
    z[28]=n<T>(13,2)*z[28] - n<T>(31,2)*z[32] + z[49];
    z[28]=z[3]*z[28];
    z[28]=n<T>(9,4)*z[6] + z[28] + n<T>(9,2)*z[44];
    z[32]=n<T>(1,4)*z[13];
    z[28]=z[28]*z[32];
    z[40]= - z[40] + n<T>(1,2) - z[17];
    z[44]=z[27]*z[14];
    z[28]=z[28] + n<T>(9,16)*z[44] + n<T>(9,8)*z[40] + z[42];
    z[28]=z[28]*z[32];
    z[32]= - static_cast<T>(1)- z[38];
    z[32]=z[32]*z[35];
    z[40]=static_cast<T>(1)+ n<T>(3,2)*z[7];
    z[40]=z[40]*z[7];
    z[42]=n<T>(1,2) - z[40];
    z[32]=n<T>(1,2)*z[42] + z[32];
    z[32]=z[32]*z[24];
    z[32]=z[32] - n<T>(1,2)*z[34];
    z[32]=z[3]*z[32];
    z[42]= - z[70]*z[29];
    z[39]=z[7]*z[39]*z[2];
    z[42]=z[42] - z[39];
    z[42]=z[42]*z[47];
    z[44]= - z[38]*z[44];
    z[32]=z[44] + z[42] + z[32];
    z[32]=z[15]*z[32];
    z[42]=z[50] + z[35];
    z[42]=z[42]*z[16];
    z[42]=z[42] - z[76];
    z[42]=z[42]*z[24];
    z[34]=z[42] - z[34];
    z[34]=z[3]*z[34];
    z[42]=n<T>(1,4) + z[61];
    z[42]=z[7]*z[42];
    z[42]=z[42] + n<T>(3,4)*z[35];
    z[42]=z[2]*z[42];
    z[44]= - static_cast<T>(1)+ n<T>(3,2)*z[27];
    z[42]=n<T>(1,2)*z[44] + z[42];
    z[42]=z[42]*z[46];
    z[34]=z[42] + z[34];
    z[34]=z[3]*z[34];
    z[39]=n<T>(3,4)*z[17] + z[39];
    z[39]=z[2]*z[39];
    z[42]=z[14]*z[26]*z[85];
    z[32]=z[32] + z[42] + z[39] + z[34];
    z[32]=z[15]*z[32];
    z[34]= - static_cast<T>(37)- 29*z[7];
    z[34]=z[34]*z[85];
    z[39]= - static_cast<T>(1)- n<T>(29,4)*z[7];
    z[39]=z[7]*z[39];
    z[39]=z[39] - z[35];
    z[39]=z[2]*z[39];
    z[34]=z[34] + z[39];
    z[34]=z[34]*z[38];
    z[39]=z[7] + n<T>(3,4);
    z[34]=z[34] - z[39];
    z[34]=z[2]*z[34];
    z[42]=static_cast<T>(1)+ n<T>(17,8)*z[7];
    z[42]=z[7]*z[42];
    z[42]=z[42] + z[35];
    z[42]=z[2]*z[42];
    z[42]=n<T>(13,8)*z[7] + z[42];
    z[42]=z[2]*z[42];
    z[41]= - z[6]*z[41]*z[16];
    z[41]=z[42] + z[41];
    z[41]=z[6]*z[41];
    z[42]=z[26]*z[50];
    z[42]=z[42] + z[35];
    z[42]=z[42]*z[16];
    z[44]=z[30]*z[50];
    z[42]=z[42] + z[44] + 1;
    z[42]=z[42]*z[2];
    z[44]= - static_cast<T>(3)- z[72];
    z[44]=n<T>(1,2)*z[44] - z[42];
    z[44]=z[44]*z[46];
    z[47]=z[14]*z[47];
    z[44]=z[44] + z[47];
    z[44]=z[44]*z[56];
    z[47]=n<T>(11,2) + z[37];
    z[47]=z[7]*z[47];
    z[47]=z[47] + z[69];
    z[47]=z[47]*z[16];
    z[49]=static_cast<T>(3)+ n<T>(5,4)*z[7];
    z[49]=z[7]*z[49];
    z[47]=z[47] + n<T>(3,2) + z[49];
    z[47]=z[2]*z[47];
    z[30]=n<T>(1,2)*z[30] + z[47];
    z[30]=z[2]*z[30];
    z[47]=z[38]*z[14];
    z[30]=z[44] + z[30] - z[47];
    z[30]=z[4]*z[30];
    z[44]= - z[13]*z[17]*z[46];
    z[47]=z[7] + 3*z[35];
    z[47]=z[2]*z[47];
    z[44]=z[47] + z[44];
    z[44]=z[13]*z[44];
    z[30]=n<T>(1,8)*z[44] + n<T>(3,4)*z[30] + z[34] + n<T>(1,2)*z[41];
    z[30]=z[10]*z[30];
    z[19]= - static_cast<T>(131)+ z[19];
    z[19]=z[19]*z[23];
    z[19]=n<T>(1,2) + z[19];
    z[34]= - static_cast<T>(15)- n<T>(607,8)*z[7];
    z[34]=z[7]*z[34];
    z[34]=z[34] - n<T>(23,4)*z[35];
    z[34]=z[2]*z[34];
    z[41]= - static_cast<T>(127)- n<T>(361,3)*z[7];
    z[41]=z[7]*z[41];
    z[41]= - n<T>(37,2) + z[41];
    z[34]=n<T>(1,2)*z[41] + z[34];
    z[34]=z[2]*z[34];
    z[19]=n<T>(1,4)*z[19] + z[34];
    z[19]=z[2]*z[19];
    z[19]= - n<T>(7,4)*z[17] + z[19];
    z[19]=z[2]*z[19];
    z[34]=static_cast<T>(25)+ z[36];
    z[34]=z[34]*z[29];
    z[19]=z[34] + z[19];
    z[34]=static_cast<T>(313)+ 515*z[7];
    z[23]=z[34]*z[23];
    z[23]=static_cast<T>(7)+ z[23];
    z[34]=n<T>(21,4) + n<T>(151,3)*z[7];
    z[34]=z[7]*z[34];
    z[34]=z[34] + n<T>(7,4)*z[35];
    z[34]=z[2]*z[34];
    z[23]=n<T>(1,2)*z[23] + z[34];
    z[23]=z[23]*z[48];
    z[34]=n<T>(14,3) + n<T>(149,32)*z[7];
    z[34]=z[7]*z[34];
    z[23]=z[23] + n<T>(11,48) + z[34];
    z[23]=z[23]*z[46];
    z[23]= - n<T>(19,32)*z[17] + z[23];
    z[23]=z[2]*z[23];
    z[33]=z[74]*z[33];
    z[23]= - n<T>(1,3)*z[33] - n<T>(19,32)*z[27] + z[23];
    z[23]=z[3]*z[23];
    z[19]=n<T>(1,8)*z[19] + z[23];
    z[19]=z[3]*z[19];
    z[23]= - static_cast<T>(5)+ n<T>(7,4)*z[7];
    z[23]=z[23]*z[81];
    z[33]=n<T>(37,2) + 41*z[7];
    z[33]=z[33]*z[50];
    z[33]=z[33] + n<T>(47,2)*z[35];
    z[33]=z[2]*z[33];
    z[34]=static_cast<T>(2)- n<T>(19,192)*z[7];
    z[34]=z[7]*z[34];
    z[33]=n<T>(1,32)*z[33] + static_cast<T>(1)+ z[34];
    z[33]=z[2]*z[33];
    z[34]=n<T>(17,8) + 13*z[7];
    z[34]=z[7]*z[34];
    z[34]=n<T>(17,8) + z[34];
    z[33]=n<T>(1,24)*z[34] + z[33];
    z[33]=z[2]*z[33];
    z[19]=z[19] + z[23] + z[33];
    z[19]=z[3]*z[19];
    z[23]= - z[26]*z[29];
    z[16]= - z[51]*z[16];
    z[16]= - z[40] + z[16];
    z[16]=z[2]*z[16];
    z[16]=z[23] + z[16];
    z[16]=z[14]*z[16];
    z[23]= - static_cast<T>(3)- z[29];
    z[23]=z[23]*z[29];
    z[23]= - static_cast<T>(1)+ z[23];
    z[26]=z[7]*z[39];
    z[26]=z[26] + n<T>(3,8)*z[35];
    z[26]=z[2]*z[26];
    z[26]=n<T>(3,4)*z[27] + z[26];
    z[26]=z[2]*z[26];
    z[26]= - n<T>(1,4)*z[70] + z[26];
    z[26]=z[2]*z[26];
    z[16]=z[16] + n<T>(1,2)*z[23] + z[26];
    z[16]=z[16]*z[56];
    z[23]= - static_cast<T>(5)- z[62];
    z[23]=z[23]*z[85];
    z[23]=z[23] - z[35];
    z[23]=z[2]*z[23];
    z[26]= - n<T>(1,8) - z[17];
    z[23]=n<T>(1,2)*z[26] + z[23];
    z[23]=z[2]*z[23];
    z[18]=z[17] + z[18];
    z[18]=z[2]*z[18];
    z[18]=n<T>(1,2)*z[17] + z[18];
    z[18]=z[14]*z[18];
    z[26]=static_cast<T>(5)+ z[7];
    z[26]=z[7]*z[26];
    z[26]=n<T>(3,4) + z[26];
    z[16]=z[16] + n<T>(3,2)*z[18] + n<T>(1,4)*z[26] + z[23];
    z[16]=z[16]*z[56];
    z[18]=z[37] + 1;
    z[23]= - z[7]*z[18]*z[46];
    z[17]= - z[9]*z[17]*z[24];
    z[17]=z[23] + z[17];
    z[17]=z[9]*z[17];
    z[23]=z[57]*z[29];
    z[23]=z[42] + static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[56];
    z[18]= - z[18]*z[38];
    z[18]=z[18] - static_cast<T>(3)- z[45];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(1,4) + z[18];
    z[18]=z[2]*z[18];
    z[24]= - static_cast<T>(11)- z[37];
    z[24]=z[7]*z[24];
    z[24]= - static_cast<T>(5)+ z[24];
    z[18]=z[23] + n<T>(1,4)*z[24] + z[18];
    z[18]=z[18]*z[56];
    z[17]=z[18] + n<T>(1,8)*z[17] + n<T>(3,8) + z[31];
    z[17]=z[5]*z[17];

    r += z[16] + n<T>(1,4)*z[17] + z[19] + z[20] + z[21] + z[22] + z[25] + 
      z[28] + n<T>(1,2)*z[30] + z[32] + n<T>(1,16)*z[43] + z[53];
 
    return r;
}

template double qg_2lha_r521(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r521(const std::array<dd_real,30>&);
#endif
