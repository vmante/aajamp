#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r769(const std::array<T,30>& k) {
  T z[81];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[3];
    z[7]=k[27];
    z[8]=k[6];
    z[9]=k[11];
    z[10]=k[24];
    z[11]=k[9];
    z[12]=k[14];
    z[13]=k[10];
    z[14]=k[5];
    z[15]=k[21];
    z[16]=z[4] + n<T>(1,2);
    z[17]=npow(z[4],2);
    z[18]=z[16]*z[17];
    z[19]=npow(z[4],3);
    z[20]= - n<T>(47,4) - z[4];
    z[20]=z[20]*z[19];
    z[21]=z[3]*npow(z[4],4);
    z[20]=z[20] - n<T>(1,4)*z[21];
    z[20]=z[3]*z[20];
    z[22]=3*z[6];
    z[23]=9*z[6];
    z[24]= - z[23] + static_cast<T>(3)+ 49*z[4];
    z[24]=z[24]*z[22];
    z[25]= - n<T>(41,2) - 181*z[4];
    z[25]=z[4]*z[25];
    z[24]=z[25] + z[24];
    z[25]=n<T>(1,4)*z[6];
    z[24]=z[24]*z[25];
    z[26]= - 69*z[19] - z[21];
    z[26]=z[3]*z[26];
    z[26]=159*z[17] + z[26];
    z[27]= - n<T>(107,2)*z[4] + z[23];
    z[27]=z[6]*z[27];
    z[26]=n<T>(1,2)*z[26] + z[27];
    z[26]=z[1]*z[26];
    z[18]=n<T>(1,4)*z[26] + z[24] + n<T>(65,4)*z[18] + z[20];
    z[20]=n<T>(3,2)*z[1];
    z[18]=z[18]*z[20];
    z[24]=n<T>(1,2)*z[4];
    z[26]=z[24] + 1;
    z[27]=z[26]*z[17];
    z[28]=z[16]*z[4];
    z[29]=z[6]*z[4];
    z[30]=z[28] - z[29];
    z[30]=z[6]*z[30];
    z[30]=z[27] + z[30];
    z[30]=z[6]*z[30];
    z[31]=z[4] + 1;
    z[32]=z[31]*z[19];
    z[30]= - n<T>(1,2)*z[32] + z[30];
    z[30]=z[6]*z[30];
    z[32]=z[4] - 1;
    z[33]= - z[32]*z[19];
    z[33]=z[33] + z[21];
    z[30]=n<T>(1,2)*z[33] + z[30];
    z[33]=z[31]*z[17];
    z[34]=n<T>(1,2)*z[6];
    z[35]=z[33]*z[34];
    z[36]=z[26]*z[4];
    z[37]= - n<T>(3,2) - z[36];
    z[37]=z[37]*z[17];
    z[37]=z[37] + z[35];
    z[38]=npow(z[6],2);
    z[39]=z[38]*z[11];
    z[37]=z[37]*z[39];
    z[30]=3*z[30] + z[37];
    z[37]=5*z[4];
    z[40]=static_cast<T>(1)+ z[37];
    z[40]=z[40]*z[17];
    z[41]=7*z[4];
    z[42]= - static_cast<T>(1)- z[41];
    z[42]=z[4]*z[42];
    z[42]=z[42] + 3*z[29];
    z[42]=z[6]*z[42];
    z[40]=z[40] + z[42];
    z[40]=z[40]*z[34];
    z[42]=3*z[17] - z[29];
    z[42]=z[6]*z[42];
    z[42]=z[42] - 3*z[19] + z[21];
    z[43]=n<T>(1,2)*z[1];
    z[42]=z[42]*z[43];
    z[44]= - z[26]*z[19];
    z[40]=z[42] + z[40] + z[44] + z[21];
    z[20]=z[40]*z[20];
    z[40]= - n<T>(3,4) - z[36];
    z[40]=z[40]*z[17];
    z[40]=z[40] + z[35];
    z[42]=z[38]*z[9];
    z[40]=z[40]*z[42];
    z[20]=z[20] + n<T>(1,2)*z[30] + z[40];
    z[20]=z[7]*z[20];
    z[30]=17*z[4];
    z[40]= - n<T>(43,2) + z[30];
    z[40]=z[40]*z[24];
    z[44]=71*z[4];
    z[45]= - static_cast<T>(9)- z[44];
    z[23]=n<T>(1,2)*z[45] + z[23];
    z[23]=z[6]*z[23];
    z[23]=z[40] + z[23];
    z[23]=z[6]*z[23];
    z[40]=static_cast<T>(31)+ n<T>(71,4)*z[4];
    z[40]=z[40]*z[17];
    z[23]=z[40] + z[23];
    z[23]=z[23]*z[25];
    z[40]=static_cast<T>(1)- n<T>(17,16)*z[4];
    z[40]=z[4]*z[40];
    z[40]= - n<T>(3,2) + z[40];
    z[40]=z[40]*z[17];
    z[45]= - n<T>(25,8) - z[4];
    z[45]=z[45]*z[19];
    z[45]=z[45] - n<T>(1,8)*z[21];
    z[46]=n<T>(1,2)*z[3];
    z[45]=z[45]*z[46];
    z[23]=z[23] + z[40] + z[45];
    z[40]=n<T>(293,2) + 97*z[4];
    z[40]=z[40]*z[24];
    z[40]=static_cast<T>(27)+ z[40];
    z[40]=z[4]*z[40];
    z[45]= - n<T>(7,2) - z[37];
    z[45]=z[45]*z[29];
    z[40]=z[40] + n<T>(9,2)*z[45];
    z[40]=z[6]*z[40];
    z[45]= - static_cast<T>(43)- z[30];
    z[45]=z[4]*z[45];
    z[45]= - static_cast<T>(33)+ z[45];
    z[45]=z[4]*z[45];
    z[45]= - n<T>(9,2) + z[45];
    z[45]=z[4]*z[45];
    z[40]=z[45] + z[40];
    z[40]=z[6]*z[40];
    z[45]=static_cast<T>(1)+ z[36];
    z[45]=z[45]*z[17];
    z[35]=z[45] - z[35];
    z[35]=z[35]*z[42];
    z[35]=z[40] + n<T>(9,2)*z[35];
    z[35]=z[9]*z[35];
    z[40]= - static_cast<T>(21)- n<T>(5,2)*z[36];
    z[40]=z[4]*z[40];
    z[40]=n<T>(9,4) + z[40];
    z[40]=z[40]*z[24];
    z[42]=z[29]*z[31];
    z[45]=static_cast<T>(4)+ n<T>(23,8)*z[4];
    z[45]=z[4]*z[45];
    z[45]=n<T>(45,8) + z[45];
    z[45]=z[4]*z[45];
    z[45]=z[45] - n<T>(9,8)*z[42];
    z[45]=z[6]*z[45];
    z[40]=z[40] + z[45];
    z[40]=z[6]*z[40];
    z[39]=z[17]*z[39];
    z[39]=z[40] + n<T>(9,8)*z[39];
    z[39]=z[11]*z[39];
    z[18]=n<T>(9,2)*z[20] + z[18] + n<T>(1,4)*z[35] + 3*z[23] + z[39];
    z[18]=z[7]*z[18];
    z[20]=n<T>(549,4) + z[44];
    z[20]=z[20]*z[24];
    z[20]=static_cast<T>(39)+ z[20];
    z[20]=z[4]*z[20];
    z[20]=static_cast<T>(9)+ z[20];
    z[23]= - n<T>(117,16) - z[41];
    z[23]=z[4]*z[23];
    z[35]=n<T>(3,8) + z[4];
    z[35]=z[6]*z[35];
    z[23]=n<T>(3,2)*z[35] - n<T>(3,2) + z[23];
    z[22]=z[23]*z[22];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[20]=z[6]*z[20];
    z[22]=n<T>(29,2) + z[37];
    z[22]=z[22]*z[24];
    z[22]=static_cast<T>(7)+ z[22];
    z[22]=z[4]*z[22];
    z[22]=n<T>(9,4) + z[22];
    z[22]=z[4]*z[22];
    z[23]=n<T>(3,2)*z[4];
    z[35]=z[23] + 1;
    z[39]=z[35]*z[29];
    z[40]= - n<T>(23,2) - z[41];
    z[40]=z[4]*z[40];
    z[40]= - n<T>(45,8) + z[40];
    z[40]=z[4]*z[40];
    z[39]=z[40] + n<T>(9,4)*z[39];
    z[39]=z[6]*z[39];
    z[22]=z[22] + z[39];
    z[22]=z[9]*z[22]*z[34];
    z[39]=n<T>(1,4)*z[4];
    z[40]= - static_cast<T>(31)- n<T>(21,2)*z[4];
    z[40]=z[4]*z[40];
    z[40]= - n<T>(63,2) + z[40];
    z[40]=z[40]*z[39];
    z[40]= - static_cast<T>(5)+ z[40];
    z[40]=z[4]*z[40];
    z[20]=z[22] + z[20] - n<T>(9,4) + z[40];
    z[20]=z[9]*z[20];
    z[22]=z[4] - 3;
    z[40]= - z[4]*z[22];
    z[40]=n<T>(3,2) + z[40];
    z[40]=z[40]*z[17];
    z[44]=z[21] + z[19];
    z[45]=z[44]*z[46];
    z[40]=z[40] + z[45];
    z[45]=n<T>(3,2)*z[3];
    z[40]=z[40]*z[45];
    z[47]= - static_cast<T>(257)- n<T>(193,2)*z[4];
    z[47]=z[47]*z[39];
    z[48]=n<T>(151,4)*z[6] + n<T>(29,8) - 55*z[4];
    z[48]=z[6]*z[48];
    z[47]=z[47] + z[48];
    z[47]=z[6]*z[47];
    z[48]=23*z[4];
    z[49]=n<T>(113,4) + z[48];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(9,4) + z[49];
    z[49]=z[4]*z[49];
    z[40]=z[47] + z[49] + z[40];
    z[47]= - z[31]*z[41];
    z[47]= - n<T>(9,2) + z[47];
    z[47]=z[47]*z[34];
    z[49]=z[4] + n<T>(3,2);
    z[50]=z[4]*z[49];
    z[50]=static_cast<T>(27)+ n<T>(5,2)*z[50];
    z[50]=z[4]*z[50];
    z[47]=z[47] - static_cast<T>(9)+ z[50];
    z[47]=z[6]*z[47];
    z[50]=static_cast<T>(29)- n<T>(41,2)*z[4];
    z[50]=z[4]*z[50];
    z[50]=static_cast<T>(9)+ z[50];
    z[47]=n<T>(1,2)*z[50] + z[47];
    z[50]= - n<T>(9,4) - z[4];
    z[50]=z[11]*z[50]*z[29];
    z[47]=n<T>(1,2)*z[47] + z[50];
    z[47]=z[11]*z[47];
    z[50]=47*z[4];
    z[51]=n<T>(219,2) + z[50];
    z[51]=z[51]*z[17];
    z[52]=n<T>(3,2)*z[21];
    z[53]=z[19] + z[52];
    z[53]=z[3]*z[53];
    z[51]=n<T>(1,4)*z[51] + z[53];
    z[51]=z[3]*z[51];
    z[53]= - n<T>(75,2) - 409*z[4];
    z[53]=z[53]*z[39];
    z[51]=z[53] + z[51];
    z[53]=3*z[21];
    z[54]=z[19] + z[53];
    z[54]=z[3]*z[54];
    z[54]=n<T>(291,2)*z[17] + z[54];
    z[54]=z[3]*z[54];
    z[54]=n<T>(151,2)*z[6] - 225*z[4] + z[54];
    z[54]=z[1]*z[54];
    z[55]= - 30*z[6] + n<T>(61,16) + 82*z[4];
    z[55]=z[6]*z[55];
    z[51]=n<T>(1,8)*z[54] + n<T>(1,2)*z[51] + z[55];
    z[51]=z[1]*z[51];
    z[18]=z[18] + z[51] + z[20] + n<T>(1,2)*z[40] + z[47];
    z[18]=z[7]*z[18];
    z[20]=n<T>(1,3)*z[4];
    z[40]= - static_cast<T>(2959)- n<T>(851,2)*z[4];
    z[40]=z[40]*z[20];
    z[40]= - n<T>(3697,4) + z[40];
    z[47]=z[4] + 2;
    z[47]=z[47]*z[4];
    z[47]=z[47] + 1;
    z[51]=3*z[3];
    z[54]= - z[47]*z[51];
    z[55]=n<T>(403,2) + n<T>(206,3)*z[4];
    z[55]=z[4]*z[55];
    z[54]=z[54] + n<T>(797,6) + z[55];
    z[54]=z[3]*z[54];
    z[40]=n<T>(1,6)*z[40] + z[54];
    z[40]=z[3]*z[40];
    z[54]=static_cast<T>(339)+ n<T>(1549,9)*z[4];
    z[40]=n<T>(1,8)*z[54] + z[40];
    z[40]=z[3]*z[40];
    z[54]=z[31]*z[4];
    z[55]= - n<T>(11,2) - z[54];
    z[40]=n<T>(1,4)*z[55] + z[40];
    z[40]=z[3]*z[40];
    z[55]=z[31]*z[3];
    z[56]=2*z[55];
    z[57]= - n<T>(317,6) - 40*z[4];
    z[57]=n<T>(5,3)*z[57] + z[56];
    z[57]=z[3]*z[57];
    z[58]=215*z[4];
    z[59]=n<T>(1871,2) + z[58];
    z[57]=n<T>(1,18)*z[59] + z[57];
    z[57]=z[3]*z[57];
    z[57]= - n<T>(7,8) + z[57];
    z[59]=npow(z[3],2);
    z[57]=z[57]*z[59];
    z[60]=npow(z[3],3);
    z[61]=z[60]*z[2];
    z[62]=n<T>(197,9) - z[46];
    z[62]=z[3]*z[62];
    z[62]= - n<T>(1,4) + z[62];
    z[62]=z[62]*z[61];
    z[57]=z[57] + z[62];
    z[57]=z[2]*z[57];
    z[40]=z[40] + z[57];
    z[40]=z[2]*z[40];
    z[57]=n<T>(779,4) + 16*z[4];
    z[57]=z[57]*z[41];
    z[57]=n<T>(10519,4) + z[57];
    z[57]=z[57]*z[20];
    z[57]=n<T>(919,2) + z[57];
    z[62]= - n<T>(119,2) - n<T>(32,3)*z[4];
    z[62]=z[62]*z[20];
    z[62]= - static_cast<T>(29)+ z[62];
    z[62]=z[4]*z[62];
    z[62]= - n<T>(229,18) + z[62];
    z[63]=z[4] + 3;
    z[64]=z[63]*z[4];
    z[65]=z[64] + 3;
    z[66]=z[65]*z[4];
    z[66]=z[66] + 1;
    z[67]=z[66]*z[3];
    z[68]=2*z[67];
    z[62]=7*z[62] + z[68];
    z[62]=z[3]*z[62];
    z[57]=n<T>(1,3)*z[57] + z[62];
    z[57]=z[3]*z[57];
    z[62]= - static_cast<T>(1387)- n<T>(2605,6)*z[4];
    z[62]=z[62]*z[20];
    z[62]= - static_cast<T>(337)+ z[62];
    z[57]=n<T>(1,4)*z[62] + z[57];
    z[57]=z[3]*z[57];
    z[62]=n<T>(295,9) + z[4];
    z[62]=z[4]*z[62];
    z[62]=n<T>(523,9) + z[62];
    z[57]=n<T>(1,4)*z[62] + z[57];
    z[57]=z[3]*z[57];
    z[62]= - z[31]*z[37];
    z[62]=static_cast<T>(7)+ z[62];
    z[40]=z[40] + n<T>(1,8)*z[62] + z[57];
    z[40]=z[2]*z[40];
    z[57]=94*z[4];
    z[62]=static_cast<T>(443)+ z[57];
    z[62]=z[62]*z[20];
    z[62]=static_cast<T>(214)+ z[62];
    z[62]=z[4]*z[62];
    z[62]=n<T>(293,3) + z[62];
    z[69]= - n<T>(1289,4) - z[57];
    z[69]=z[4]*z[69];
    z[69]= - n<T>(725,2) + z[69];
    z[69]=z[69]*z[20];
    z[70]=static_cast<T>(1)+ z[20];
    z[70]=z[4]*z[70];
    z[70]=static_cast<T>(1)+ z[70];
    z[70]=z[4]*z[70];
    z[70]=n<T>(1,3) + z[70];
    z[70]=z[3]*z[70];
    z[69]=n<T>(188,3)*z[70] - n<T>(179,4) + z[69];
    z[69]=z[3]*z[69];
    z[62]=n<T>(1,3)*z[62] + z[69];
    z[62]=z[3]*z[62];
    z[69]= - static_cast<T>(79)- n<T>(161,6)*z[4];
    z[69]=z[4]*z[69];
    z[69]= - n<T>(277,6) + z[69];
    z[62]=n<T>(1,6)*z[69] + z[62];
    z[62]=z[3]*z[62];
    z[57]=n<T>(913,4) + z[57];
    z[57]=z[57]*z[20];
    z[47]=z[47]*z[3];
    z[57]= - n<T>(94,3)*z[47] + n<T>(179,4) + z[57];
    z[57]=z[3]*z[57];
    z[69]= - n<T>(349,2) - z[50];
    z[69]=z[4]*z[69];
    z[69]= - n<T>(293,2) + z[69];
    z[57]=n<T>(1,9)*z[69] + z[57];
    z[57]=z[57]*z[59];
    z[69]=n<T>(188,3)*z[55] - n<T>(179,4) - n<T>(94,3)*z[4];
    z[69]=z[69]*z[60];
    z[70]=npow(z[3],4);
    z[71]=z[70]*z[2];
    z[69]=z[69] - n<T>(47,3)*z[71];
    z[69]=z[2]*z[69];
    z[57]=z[57] + n<T>(1,3)*z[69];
    z[57]=z[2]*z[57];
    z[57]=z[62] + z[57];
    z[57]=z[2]*z[57];
    z[62]= - static_cast<T>(4)- z[4];
    z[62]=z[62]*z[20];
    z[62]= - static_cast<T>(2)+ z[62];
    z[62]=z[4]*z[62];
    z[62]= - n<T>(4,3) + z[62];
    z[62]=z[4]*z[62];
    z[62]= - n<T>(1,3) + z[62];
    z[62]=z[3]*z[62];
    z[69]=n<T>(185,4) + n<T>(94,9)*z[4];
    z[69]=z[4]*z[69];
    z[69]=n<T>(913,12) + z[69];
    z[69]=z[4]*z[69];
    z[69]=n<T>(1987,36) + z[69];
    z[69]=z[4]*z[69];
    z[62]=n<T>(47,3)*z[62] + n<T>(179,12) + z[69];
    z[62]=z[3]*z[62];
    z[69]= - n<T>(179,2) - n<T>(47,3)*z[4];
    z[69]=z[4]*z[69];
    z[69]= - n<T>(1085,6) + z[69];
    z[69]=z[4]*z[69];
    z[69]= - n<T>(935,6) + z[69];
    z[69]=z[4]*z[69];
    z[69]= - n<T>(293,6) + z[69];
    z[62]=n<T>(1,3)*z[69] + z[62];
    z[62]=z[3]*z[62];
    z[69]=static_cast<T>(635)+ 161*z[4];
    z[69]=z[4]*z[69];
    z[69]=static_cast<T>(751)+ z[69];
    z[69]=z[4]*z[69];
    z[69]=static_cast<T>(277)+ z[69];
    z[62]=n<T>(1,36)*z[69] + z[62];
    z[62]=z[3]*z[62];
    z[69]= - static_cast<T>(20)- 19*z[4];
    z[69]=z[4]*z[69];
    z[69]=n<T>(1,8) + z[69];
    z[57]=z[57] + n<T>(1,9)*z[69] + z[62];
    z[57]=z[11]*z[57];
    z[62]=n<T>(4949,3) + 373*z[4];
    z[62]=z[62]*z[24];
    z[62]=n<T>(3445,3) + z[62];
    z[62]=z[4]*z[62];
    z[62]=static_cast<T>(85)+ n<T>(1,6)*z[62];
    z[69]=z[24] + 2;
    z[69]=z[69]*z[4];
    z[69]=z[69] + 3;
    z[69]=z[69]*z[4];
    z[69]=z[69] + 2;
    z[69]=z[69]*z[4];
    z[69]=z[69] + n<T>(1,2);
    z[69]=z[69]*z[3];
    z[72]=n<T>(457,18) + z[4];
    z[72]=z[4]*z[72];
    z[72]=n<T>(421,6) + z[72];
    z[72]=z[4]*z[72];
    z[72]=n<T>(409,6) + z[72];
    z[72]=z[4]*z[72];
    z[72]= - z[69] + n<T>(403,18) + z[72];
    z[72]=z[3]*z[72];
    z[73]= - n<T>(703,9) - z[4];
    z[73]=z[4]*z[73];
    z[73]= - n<T>(3059,12) + z[73];
    z[73]=z[4]*z[73];
    z[73]= - n<T>(559,2) + z[73];
    z[73]=z[4]*z[73];
    z[73]= - n<T>(3661,36) + z[73];
    z[72]=n<T>(1,2)*z[73] + z[72];
    z[72]=z[3]*z[72];
    z[62]=n<T>(1,2)*z[62] + z[72];
    z[62]=z[3]*z[62];
    z[72]= - static_cast<T>(218)- n<T>(617,8)*z[4];
    z[72]=z[4]*z[72];
    z[72]= - n<T>(613,4) + z[72];
    z[62]=n<T>(1,9)*z[72] + z[62];
    z[62]=z[3]*z[62];
    z[72]=3*z[4];
    z[73]= - z[31]*z[72];
    z[73]=n<T>(377,9) + z[73];
    z[73]=z[73]*z[24];
    z[73]= - n<T>(275,9) + z[73];
    z[74]= - n<T>(9,8) - z[54];
    z[74]=z[6]*z[74];
    z[40]=z[57] + z[74] + z[40] + n<T>(1,4)*z[73] + z[62];
    z[40]=z[11]*z[40];
    z[57]=static_cast<T>(1)+ n<T>(5,4)*z[4];
    z[57]=z[57]*z[37];
    z[62]=z[4] - n<T>(1,2);
    z[73]=n<T>(1,2)*z[2];
    z[74]= - z[73] + z[62];
    z[74]=z[2]*z[74];
    z[75]=z[73] - z[4];
    z[75]= - z[6]*z[75];
    z[57]=z[75] + z[57] + z[74];
    z[57]=z[57]*z[34];
    z[74]= - n<T>(19,2) - z[37];
    z[74]=z[4]*z[74];
    z[74]= - n<T>(9,2) + z[74];
    z[74]=z[74]*z[24];
    z[75]=static_cast<T>(1)- n<T>(7,8)*z[4];
    z[75]=z[4]*z[75];
    z[76]=n<T>(5,2)*z[2] - n<T>(13,4) - z[37];
    z[76]=z[2]*z[76];
    z[75]=z[76] + n<T>(13,8) + z[75];
    z[75]=z[2]*z[75];
    z[57]=z[57] + z[74] + z[75];
    z[57]=z[57]*z[34];
    z[74]=z[36] + n<T>(1,2);
    z[75]= - z[74]*z[51];
    z[76]=11*z[4];
    z[77]=static_cast<T>(7)+ z[76];
    z[75]=n<T>(1,4)*z[77] + z[75];
    z[75]=z[3]*z[75];
    z[75]=n<T>(11,8) + z[75];
    z[75]=z[3]*z[75];
    z[77]=3*z[55];
    z[78]= - n<T>(5,4) + z[77];
    z[78]=z[78]*z[59];
    z[78]=z[78] - z[61];
    z[78]=z[78]*z[73];
    z[75]=z[75] + z[78];
    z[75]=z[75]*z[73];
    z[78]= - static_cast<T>(21)- n<T>(29,2)*z[4];
    z[78]=z[4]*z[78];
    z[78]= - n<T>(13,2) + z[78];
    z[78]=n<T>(1,2)*z[78] + z[67];
    z[78]=z[3]*z[78];
    z[79]=z[72] + 7;
    z[78]=z[78] - z[79];
    z[80]=n<T>(1,4)*z[3];
    z[78]=z[78]*z[80];
    z[75]=z[75] + static_cast<T>(2)+ z[78];
    z[75]=z[2]*z[75];
    z[78]=static_cast<T>(5)- n<T>(11,2)*z[4];
    z[78]=z[4]*z[78];
    z[78]=n<T>(17,2) + z[78];
    z[79]=z[4]*z[79];
    z[79]=static_cast<T>(5)+ z[79];
    z[79]=z[4]*z[79];
    z[79]=static_cast<T>(1)+ z[79];
    z[79]=z[3]*z[79];
    z[78]=n<T>(1,2)*z[78] + z[79];
    z[78]=z[3]*z[78];
    z[76]=z[78] - n<T>(27,2) - z[76];
    z[75]=n<T>(1,4)*z[76] + z[75];
    z[75]=z[2]*z[75];
    z[76]=static_cast<T>(5)+ z[72];
    z[76]=z[76]*z[24];
    z[76]=static_cast<T>(1)+ z[76];
    z[76]=z[3]*z[4]*z[76];
    z[78]=n<T>(19,4)*z[4];
    z[79]=static_cast<T>(13)+ z[78];
    z[79]=z[4]*z[79];
    z[79]=n<T>(31,4) + z[79];
    z[76]=n<T>(1,2)*z[79] + z[76];
    z[75]=n<T>(1,2)*z[76] + z[75];
    z[75]=z[2]*z[75];
    z[33]=n<T>(1,4)*z[33] + z[75];
    z[33]=z[2]*z[33];
    z[33]=z[33] + z[57];
    z[33]=z[9]*z[33];
    z[57]=z[74]*z[3];
    z[75]=n<T>(3,2)*z[57] - static_cast<T>(4)- n<T>(7,2)*z[4];
    z[75]=z[3]*z[75];
    z[75]=static_cast<T>(1)+ z[75];
    z[75]=z[75]*z[59];
    z[76]=n<T>(11,2) - z[77];
    z[76]=z[76]*z[60];
    z[76]=z[76] + z[71];
    z[77]=n<T>(1,4)*z[2];
    z[76]=z[76]*z[77];
    z[75]=z[75] + z[76];
    z[75]=z[2]*z[75];
    z[76]=static_cast<T>(27)+ n<T>(23,2)*z[4];
    z[76]=z[4]*z[76];
    z[67]= - z[67] + n<T>(31,2) + z[76];
    z[67]=z[3]*z[67];
    z[67]= - n<T>(29,2)*z[31] + z[67];
    z[67]=z[3]*z[67];
    z[67]= - static_cast<T>(7)+ z[67];
    z[67]=z[67]*z[80];
    z[67]=z[67] + z[75];
    z[67]=z[2]*z[67];
    z[75]= - static_cast<T>(11)- z[72];
    z[75]=z[4]*z[75];
    z[75]= - static_cast<T>(13)+ z[75];
    z[75]=z[4]*z[75];
    z[75]= - static_cast<T>(5)+ z[75];
    z[75]=z[75]*z[80];
    z[76]=static_cast<T>(7)+ n<T>(27,8)*z[4];
    z[76]=z[4]*z[76];
    z[75]=z[75] + n<T>(27,8) + z[76];
    z[75]=z[3]*z[75];
    z[76]=n<T>(27,4) + z[4];
    z[75]=n<T>(1,4)*z[76] + z[75];
    z[75]=z[3]*z[75];
    z[67]=z[67] - n<T>(1,16) + z[75];
    z[67]=z[2]*z[67];
    z[75]=n<T>(13,2) + z[72];
    z[75]=z[75]*z[72];
    z[75]= - n<T>(11,2) + z[75];
    z[76]= - static_cast<T>(5)- z[23];
    z[76]=z[4]*z[76];
    z[76]= - static_cast<T>(5)+ z[76];
    z[76]=z[4]*z[76];
    z[76]= - n<T>(3,2) + z[76];
    z[76]=z[3]*z[76];
    z[75]=n<T>(1,4)*z[75] + z[76];
    z[75]=z[3]*z[75];
    z[76]=21*z[4];
    z[79]=static_cast<T>(19)+ z[76];
    z[75]=n<T>(1,4)*z[79] + z[75];
    z[67]=n<T>(1,2)*z[75] + z[67];
    z[67]=z[2]*z[67];
    z[75]=static_cast<T>(17)+ n<T>(43,4)*z[4];
    z[75]=z[4]*z[75];
    z[75]=n<T>(3,2) + z[75];
    z[79]= - z[4]*z[57];
    z[75]=n<T>(1,2)*z[75] + z[79];
    z[67]=n<T>(1,2)*z[75] + z[67];
    z[67]=z[2]*z[67];
    z[75]=z[77] - z[16];
    z[75]=z[2]*z[75];
    z[79]=n<T>(23,2)*z[2] + n<T>(17,2) + z[48];
    z[79]=n<T>(1,2)*z[79] + z[6];
    z[79]=z[79]*z[34];
    z[80]= - n<T>(85,4) - z[76];
    z[80]=z[4]*z[80];
    z[75]=z[79] + 13*z[75] - n<T>(45,8) + z[80];
    z[34]=z[75]*z[34];
    z[75]=static_cast<T>(41)+ z[76];
    z[75]=z[75]*z[24];
    z[75]=static_cast<T>(11)+ z[75];
    z[75]=z[4]*z[75];
    z[75]=static_cast<T>(9)+ z[75];
    z[33]=z[33] + z[34] + n<T>(1,4)*z[75] + z[67];
    z[33]=z[9]*z[33];
    z[34]=n<T>(427,3) - z[4];
    z[67]= - n<T>(421,3) + z[72];
    z[67]=z[3]*z[67];
    z[34]=n<T>(1,2)*z[34] + z[67];
    z[34]=z[3]*z[34];
    z[34]= - n<T>(9,4) + z[34];
    z[34]=z[34]*z[59];
    z[67]= - n<T>(439,2) + 851*z[3];
    z[67]=z[67]*z[60];
    z[67]=z[67] - 215*z[71];
    z[67]=z[2]*z[67];
    z[34]=z[34] + n<T>(1,9)*z[67];
    z[34]=z[34]*z[73];
    z[67]=z[24] - 1;
    z[75]=z[67]*z[72];
    z[75]=n<T>(833,18) + z[75];
    z[75]=z[3]*z[75];
    z[76]=z[39] - 3;
    z[76]=z[76]*z[4];
    z[75]=z[75] - n<T>(415,12) - z[76];
    z[75]=z[3]*z[75];
    z[79]=n<T>(7,2) - z[4];
    z[75]=n<T>(1,2)*z[79] + z[75];
    z[75]=z[3]*z[75];
    z[75]= - n<T>(23,4) + z[75];
    z[75]=z[3]*z[75];
    z[34]=z[75] + z[34];
    z[34]=z[2]*z[34];
    z[75]=z[67]*z[4];
    z[79]=n<T>(3,2) + z[75];
    z[79]=z[3]*z[79];
    z[67]=z[79] + z[67];
    z[67]=z[67]*z[60];
    z[22]=z[3]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[60];
    z[22]=z[22] + z[71];
    z[22]=z[22]*z[73];
    z[22]=z[67] + z[22];
    z[22]=z[2]*z[22];
    z[67]=z[32]*z[4];
    z[79]=z[67] + 1;
    z[80]=z[4]*z[79];
    z[80]= - static_cast<T>(1)+ z[80];
    z[80]=z[3]*z[80];
    z[80]=z[80] + z[79];
    z[80]=z[80]*z[60];
    z[22]=n<T>(1,2)*z[80] + z[22];
    z[22]=z[1]*z[22];
    z[79]=z[79]*z[23];
    z[79]= - n<T>(103,9) + z[79];
    z[79]=z[3]*z[79];
    z[80]=static_cast<T>(11)- z[4];
    z[80]=z[4]*z[80];
    z[80]= - static_cast<T>(11)+ z[80];
    z[80]=z[4]*z[80];
    z[80]=n<T>(403,9) + z[80];
    z[79]=n<T>(1,4)*z[80] + z[79];
    z[79]=z[3]*z[79];
    z[76]= - n<T>(5,4) - z[76];
    z[76]=n<T>(1,2)*z[76] + z[79];
    z[76]=z[3]*z[76];
    z[79]=n<T>(23,2) - 35*z[4];
    z[76]=n<T>(1,2)*z[79] + z[76];
    z[76]=z[3]*z[76];
    z[22]=z[22] + z[34] + n<T>(97,8) + z[76];
    z[22]=z[22]*z[43];
    z[34]=z[62]*z[4];
    z[76]=n<T>(403,18) + z[34];
    z[76]=z[76]*z[24];
    z[76]= - static_cast<T>(1)+ z[76];
    z[79]=static_cast<T>(2)- z[39];
    z[79]=z[4]*z[79];
    z[79]= - n<T>(1003,36) + z[79];
    z[79]=z[4]*z[79];
    z[80]=n<T>(206,9) + n<T>(3,4)*z[67];
    z[80]=z[4]*z[80];
    z[80]=n<T>(103,9) + z[80];
    z[80]=z[3]*z[80];
    z[79]=z[80] - n<T>(689,72) + z[79];
    z[79]=z[3]*z[79];
    z[76]=n<T>(1,2)*z[76] + z[79];
    z[76]=z[3]*z[76];
    z[79]= - static_cast<T>(1)- n<T>(33,2)*z[4];
    z[79]=z[4]*z[79];
    z[79]= - n<T>(39,4) + z[79];
    z[76]=n<T>(1,4)*z[79] + z[76];
    z[76]=z[3]*z[76];
    z[79]= - static_cast<T>(995)- 439*z[4];
    z[80]= - n<T>(1621,4) - 206*z[4];
    z[80]=z[3]*z[80];
    z[79]=n<T>(1,4)*z[79] + z[80];
    z[79]=z[3]*z[79];
    z[79]=n<T>(197,2) + z[79];
    z[79]=z[79]*z[59];
    z[80]=n<T>(385,9) - z[4];
    z[80]=z[3]*z[80];
    z[80]=n<T>(905,18) + z[80];
    z[80]=z[80]*z[60];
    z[71]=z[80] + z[71];
    z[71]=z[71]*z[77];
    z[71]=n<T>(1,9)*z[79] + z[71];
    z[71]=z[2]*z[71];
    z[79]=static_cast<T>(821)+ 833*z[4];
    z[79]=z[3]*z[79];
    z[79]=n<T>(1,12)*z[79] + static_cast<T>(8)- n<T>(161,36)*z[4];
    z[79]=z[3]*z[79];
    z[26]= - n<T>(797,36)*z[26] + z[79];
    z[26]=z[3]*z[26];
    z[26]= - n<T>(7,8) + z[26];
    z[26]=z[3]*z[26];
    z[26]=z[26] + z[71];
    z[26]=z[2]*z[26];
    z[23]= - n<T>(415,3) + z[23];
    z[23]=z[23]*z[24];
    z[23]= - n<T>(412,9) + z[23];
    z[23]=z[3]*z[23];
    z[71]=n<T>(1603,9) - z[4];
    z[71]=z[4]*z[71];
    z[71]=n<T>(599,9) + z[71];
    z[23]=n<T>(1,4)*z[71] + z[23];
    z[23]=z[3]*z[23];
    z[71]=n<T>(197,9) + z[4];
    z[71]=z[4]*z[71];
    z[71]=n<T>(421,9) + z[71];
    z[23]=n<T>(1,4)*z[71] + z[23];
    z[23]=z[3]*z[23];
    z[71]=n<T>(53,4) - 9*z[4];
    z[23]=n<T>(1,4)*z[71] + z[23];
    z[23]=z[3]*z[23];
    z[23]=z[26] + n<T>(169,16) + z[23];
    z[23]=z[2]*z[23];
    z[26]= - n<T>(159,2) + z[58];
    z[22]=z[22] - n<T>(159,8)*z[6] + z[23] + n<T>(1,8)*z[26] + z[76];
    z[22]=z[1]*z[22];
    z[23]= - n<T>(9,4) - z[55];
    z[23]=z[23]*z[59];
    z[26]=z[60]*z[73];
    z[23]=z[23] + z[26];
    z[23]=z[2]*z[23];
    z[26]=static_cast<T>(3)+ z[24];
    z[26]=n<T>(5,2)*z[26] + z[57];
    z[26]=z[3]*z[26];
    z[26]= - n<T>(11,4) + z[26];
    z[26]=z[3]*z[26];
    z[23]=z[26] + z[23];
    z[23]=z[2]*z[23];
    z[26]= - n<T>(17,4) + z[4];
    z[26]=z[4]*z[26];
    z[26]= - n<T>(21,4) + z[26];
    z[26]=z[3]*z[26];
    z[26]=z[26] + n<T>(31,8) + z[37];
    z[26]=z[3]*z[26];
    z[23]=z[26] + z[23];
    z[23]=z[2]*z[23];
    z[26]=static_cast<T>(9)- 13*z[4];
    z[26]=z[26]*z[45];
    z[26]=11*z[62] + z[26];
    z[23]=n<T>(1,4)*z[26] + z[23];
    z[23]=z[2]*z[23];
    z[26]=z[4] + n<T>(1,4);
    z[45]= - z[26]*z[24];
    z[23]=z[23] + static_cast<T>(2)+ z[45];
    z[23]=z[2]*z[23];
    z[36]= - n<T>(9,2) - z[36];
    z[36]=z[36]*z[24];
    z[36]= - static_cast<T>(4)+ z[36];
    z[36]=z[4]*z[36];
    z[45]=z[24]*z[6];
    z[57]= - z[74]*z[45];
    z[36]=z[57] - static_cast<T>(2)+ z[36];
    z[36]=z[11]*z[36];
    z[57]=z[74]*z[46];
    z[57]= - z[4] + z[57];
    z[57]=z[3]*z[57];
    z[58]=z[59]*z[73];
    z[55]=n<T>(1,2) - z[55];
    z[55]=z[3]*z[55];
    z[55]=z[55] + z[58];
    z[55]=z[55]*z[73];
    z[55]=z[57] + z[55];
    z[55]=z[2]*z[55];
    z[57]=z[4]*z[35];
    z[57]= - n<T>(1,2) + z[57];
    z[57]=z[3]*z[57];
    z[57]=z[57] - z[62];
    z[55]=n<T>(1,2)*z[57] + z[55];
    z[55]=z[2]*z[55];
    z[57]=static_cast<T>(1)+ z[72];
    z[57]=z[57]*z[39];
    z[55]=z[57] + z[55];
    z[57]=npow(z[2],2);
    z[55]=z[55]*z[57];
    z[58]=z[77]*z[42];
    z[55]=z[55] + z[58];
    z[55]=z[10]*z[55];
    z[58]=z[31]*z[24];
    z[62]=static_cast<T>(2)+ z[58];
    z[62]=z[4]*z[62];
    z[23]=n<T>(11,2)*z[55] + z[36] + z[23] + static_cast<T>(2)+ z[62];
    z[23]=z[10]*z[23];
    z[36]= - z[65]*z[17];
    z[55]= - z[63]*z[19];
    z[21]=z[55] - z[21];
    z[21]=z[3]*z[21];
    z[21]=z[36] + z[21];
    z[21]=z[3]*z[21];
    z[36]= - z[4]*z[66];
    z[21]=z[36] + z[21];
    z[21]=z[21]*z[46];
    z[36]=z[74]*z[29];
    z[55]= - z[74]*z[17];
    z[55]=z[55] - z[36];
    z[55]=z[10]*z[55];
    z[62]=z[74]*z[4];
    z[21]=z[55] - z[62] + z[21];
    z[55]= - static_cast<T>(5)- z[4];
    z[55]=z[55]*z[19];
    z[53]=z[55] - z[53];
    z[53]=z[53]*z[46];
    z[27]= - z[27] + z[53];
    z[27]=z[27]*z[59];
    z[44]= - z[44]*z[60]*z[43];
    z[27]=z[27] + z[44];
    z[27]=z[27]*z[43];
    z[43]= - static_cast<T>(1)- z[39];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(5,4) + z[43];
    z[43]=z[43]*z[17];
    z[44]= - n<T>(7,2) - z[4];
    z[19]=z[44]*z[19];
    z[19]=z[19] - z[52];
    z[19]=z[19]*z[46];
    z[19]=z[43] + z[19];
    z[19]=z[3]*z[19];
    z[43]= - z[74]*z[24];
    z[19]=z[43] + z[19];
    z[19]=z[3]*z[19];
    z[19]=z[19] + z[27];
    z[19]=z[1]*z[19];
    z[19]=z[19] + n<T>(1,2)*z[21];
    z[19]=z[5]*z[19];
    z[21]=z[35]*z[24];
    z[21]=z[21] - z[29];
    z[21]=z[21]*z[38];
    z[27]=z[17]*npow(z[6],3);
    z[29]=z[27]*z[9];
    z[35]= - z[21] + n<T>(1,2)*z[29];
    z[35]=z[9]*z[35];
    z[21]=z[8]*z[21];
    z[27]= - z[8]*z[27];
    z[27]=z[29] + z[27];
    z[27]=z[15]*z[27];
    z[21]=n<T>(1,2)*z[27] + z[35] + z[21];
    z[21]=z[15]*z[21];
    z[27]=static_cast<T>(7)- z[72];
    z[27]=z[27]*z[45];
    z[29]= - static_cast<T>(3)+ z[24];
    z[29]=z[4]*z[29];
    z[27]=z[29] + z[27];
    z[29]=z[62] - z[36];
    z[29]=z[11]*z[29];
    z[35]=z[1]*z[4];
    z[27]= - n<T>(3,4)*z[35] + n<T>(1,2)*z[27] + z[29];
    z[27]=z[8]*z[27];
    z[21]=z[27] + z[21];
    z[27]=n<T>(107,4) - z[50];
    z[27]=z[27]*z[20];
    z[29]= - static_cast<T>(367)- n<T>(557,4)*z[4];
    z[29]=z[29]*z[37];
    z[29]= - n<T>(2291,2) + z[29];
    z[35]=static_cast<T>(412)+ n<T>(827,4)*z[4];
    z[35]=z[4]*z[35];
    z[35]=static_cast<T>(206)+ z[35];
    z[35]=z[3]*z[35];
    z[29]=n<T>(1,6)*z[29] + z[35];
    z[29]=z[3]*z[29];
    z[27]=z[29] + n<T>(137,4) + z[27];
    z[27]=z[3]*z[27];
    z[29]= - n<T>(29,4) - z[37];
    z[29]=z[29]*z[30];
    z[29]= - n<T>(287,4) + z[29];
    z[27]=n<T>(1,6)*z[29] + z[27];
    z[29]=n<T>(1,3)*z[3];
    z[27]=z[27]*z[29];
    z[30]=z[3]*z[49];
    z[30]= - n<T>(13,4) + z[30];
    z[30]=z[30]*z[60];
    z[35]= - z[70]*z[73];
    z[30]=z[30] + z[35];
    z[30]=z[2]*z[30];
    z[35]= - n<T>(215,3) - z[64];
    z[35]=z[35]*z[46];
    z[35]=z[35] - n<T>(125,9) + z[78];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(833,72) + z[35];
    z[35]=z[35]*z[59];
    z[30]=z[35] + z[30];
    z[30]=z[30]*z[73];
    z[35]=n<T>(827,4) + n<T>(412,3)*z[4];
    z[29]=z[35]*z[29];
    z[35]=n<T>(707,9) - z[72];
    z[35]=z[4]*z[35];
    z[35]=n<T>(367,18) + z[35];
    z[29]=n<T>(1,4)*z[35] + z[29];
    z[29]=z[3]*z[29];
    z[35]=static_cast<T>(7)+ n<T>(977,9)*z[4];
    z[29]=n<T>(1,8)*z[35] + z[29];
    z[29]=z[3]*z[29];
    z[29]= - n<T>(383,144) + z[29];
    z[29]=z[3]*z[29];
    z[29]=z[29] + z[30];
    z[29]=z[2]*z[29];
    z[30]=103*z[4];
    z[35]= - n<T>(1645,4) - z[30];
    z[20]=z[35]*z[20];
    z[20]= - static_cast<T>(103)+ z[20];
    z[20]=z[3]*z[20];
    z[30]=n<T>(1243,8) - z[30];
    z[30]=z[4]*z[30];
    z[20]=z[20] + n<T>(337,8) + n<T>(1,9)*z[30];
    z[20]=z[3]*z[20];
    z[30]=n<T>(61,2) - 233*z[4];
    z[30]=z[4]*z[30];
    z[30]=n<T>(637,4) + z[30];
    z[20]=n<T>(1,36)*z[30] + z[20];
    z[20]=z[3]*z[20];
    z[30]=n<T>(311,2) + 707*z[4];
    z[20]=n<T>(1,72)*z[30] + z[20];
    z[20]=z[3]*z[20];
    z[20]=z[29] - n<T>(25,16) + z[20];
    z[20]=z[2]*z[20];
    z[29]= - static_cast<T>(31)- n<T>(67,2)*z[4];
    z[20]=z[20] + n<T>(1,8)*z[29] + z[27];
    z[20]=z[2]*z[20];
    z[27]=z[32]*z[24];
    z[27]= - static_cast<T>(3)+ z[27];
    z[27]=z[4]*z[27];
    z[27]= - n<T>(5,2) + z[27];
    z[27]=z[4]*z[27];
    z[29]= - n<T>(5,2) - z[4];
    z[29]=z[4]*z[29];
    z[29]= - n<T>(3,2) + z[29];
    z[29]=z[4]*z[29];
    z[29]=n<T>(1,2) + z[29];
    z[29]=z[4]*z[29];
    z[29]=z[69] + n<T>(1,2) + z[29];
    z[29]=z[3]*z[29];
    z[27]=z[29] - n<T>(1,2) + z[27];
    z[27]=z[3]*z[27];
    z[29]= - n<T>(1,2) + z[54];
    z[29]=z[4]*z[29];
    z[27]=z[27] - n<T>(1,2) + z[29];
    z[27]=z[3]*z[27];
    z[27]=z[58] + z[27];
    z[27]=z[3]*z[27];
    z[29]=2*z[4];
    z[30]= - z[56] - n<T>(7,2) - z[29];
    z[30]=z[3]*z[30];
    z[30]=z[30] - z[49];
    z[30]=z[30]*z[60];
    z[32]=static_cast<T>(1)+ z[46];
    z[32]=z[3]*z[32];
    z[32]=n<T>(1,2) + z[32];
    z[32]=z[32]*z[61];
    z[30]=z[30] + z[32];
    z[30]=z[2]*z[30];
    z[31]=n<T>(3,2)*z[31] + z[47];
    z[31]=z[31]*z[51];
    z[31]=z[31] + static_cast<T>(1)+ z[28];
    z[31]=z[3]*z[31];
    z[31]= - static_cast<T>(1)+ z[31];
    z[31]=z[3]*z[31];
    z[31]= - n<T>(1,2) + z[31];
    z[31]=z[3]*z[31];
    z[30]=z[31] + z[30];
    z[30]=z[2]*z[30];
    z[29]=n<T>(3,2) + z[29];
    z[29]=z[4]*z[29];
    z[29]= - static_cast<T>(3)+ z[29];
    z[29]=z[4]*z[29];
    z[29]= - z[68] - n<T>(5,2) + z[29];
    z[29]=z[3]*z[29];
    z[31]=n<T>(3,2) - z[4];
    z[31]=z[4]*z[31];
    z[31]=static_cast<T>(3)+ z[31];
    z[31]=z[4]*z[31];
    z[29]=z[29] + n<T>(1,2) + z[31];
    z[29]=z[3]*z[29];
    z[29]=z[29] + n<T>(3,2) - z[67];
    z[29]=z[3]*z[29];
    z[29]=n<T>(1,2) + z[29];
    z[29]=z[3]*z[29];
    z[29]=z[29] + z[30];
    z[29]=z[2]*z[29];
    z[27]=z[27] + z[29];
    z[27]=z[12]*z[27];
    z[16]=z[16]*z[24];
    z[24]=z[73] - z[26];
    z[24]=z[2]*z[24];
    z[26]= - z[6] + static_cast<T>(1)- z[2];
    z[25]=z[26]*z[25];
    z[16]=z[25] + z[16] + z[24];
    z[16]=z[6]*z[16];
    z[24]=z[75] + z[45];
    z[24]=z[6]*z[24];
    z[24]= - z[34] + z[24];
    z[24]=z[6]*z[24];
    z[17]=n<T>(1,2)*z[17] + z[24];
    z[17]=z[8]*z[17];
    z[24]=z[2]*z[4];
    z[17]=z[17] - z[28] + z[24];
    z[16]=z[16] + n<T>(1,2)*z[17];
    z[16]=z[14]*z[16];
    z[17]= - static_cast<T>(5)- z[41];
    z[17]=z[4]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[46];
    z[24]=z[59]*z[2];
    z[25]=static_cast<T>(1)- z[54];
    z[25]=z[25]*z[24];
    z[17]=z[17] + z[25];
    z[17]=z[17]*z[77];
    z[25]= - n<T>(3,4) - z[4];
    z[25]=z[4]*z[25];
    z[17]=z[25] + z[17];
    z[17]=z[2]*z[17];
    z[24]= - z[46] - z[24];
    z[24]=z[11]*z[24]*z[57];
    z[17]=n<T>(1,4)*z[24] + z[17] - n<T>(3,8)*z[42];
    z[17]=z[13]*z[17];
    z[24]= - static_cast<T>(7)- z[37];
    z[24]=z[24]*z[48];
    z[24]=static_cast<T>(103)+ z[24];
    z[25]=static_cast<T>(403)- z[4];
    z[25]=z[4]*z[25];
    z[25]=n<T>(188,3) + n<T>(1,8)*z[25];
    z[25]=z[4]*z[25];
    z[26]= - n<T>(103,3) + z[39];
    z[26]=z[4]*z[26];
    z[26]= - n<T>(412,9) + z[26];
    z[26]=z[4]*z[26];
    z[26]= - n<T>(103,6) + z[26];
    z[26]=z[3]*z[26];
    z[25]=z[26] + n<T>(1799,72) + z[25];
    z[25]=z[3]*z[25];
    z[26]= - n<T>(737,12) + z[4];
    z[26]=z[4]*z[26];
    z[26]= - n<T>(617,9) + z[26];
    z[26]=z[4]*z[26];
    z[26]= - n<T>(787,18) + z[26];
    z[25]=n<T>(1,4)*z[26] + z[25];
    z[25]=z[3]*z[25];
    z[26]= - n<T>(9,8) + z[4];
    z[26]=z[4]*z[26];
    z[26]= - n<T>(3,16) + z[26];
    z[26]=z[4]*z[26];
    z[25]=z[25] + n<T>(323,72) + z[26];
    z[25]=z[3]*z[25];
    z[26]=n<T>(97,8)*z[6] - n<T>(109,16)*z[2] + n<T>(91,8) - z[4];
    z[26]=z[6]*z[26];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + n<T>(1,4)*z[21] + z[22] + 
      z[23] + n<T>(1,16)*z[24] + z[25] + z[26] + z[27] + z[33] + z[40];
 
    return r;
}

template double qg_2lha_r769(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r769(const std::array<dd_real,30>&);
#endif
