#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1365(const std::array<T,30>& k) {
  T z[40];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[7];
    z[8]=k[2];
    z[9]=k[11];
    z[10]=k[5];
    z[11]=k[9];
    z[12]=z[9] - 1;
    z[13]=n<T>(1,2)*z[9];
    z[14]=z[12]*z[13];
    z[15]=z[9] - 3;
    z[16]=z[15]*z[13];
    z[17]=static_cast<T>(1)+ z[16];
    z[18]=z[5]*z[9];
    z[17]=z[17]*z[18];
    z[19]=z[12]*npow(z[9],2);
    z[20]=z[19]*z[5];
    z[21]=z[2]*z[20];
    z[14]=z[21] + z[14] + z[17];
    z[14]=z[2]*z[14];
    z[17]=z[18] + 1;
    z[21]=z[9] - n<T>(1,2);
    z[21]=z[21]*z[9];
    z[21]=z[21] - n<T>(3,2);
    z[22]=n<T>(1,2)*z[10];
    z[17]=z[22]*z[21]*z[17];
    z[15]=z[15]*z[9];
    z[21]=z[9]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[5]*z[21];
    z[21]=z[21] + static_cast<T>(3)+ z[15];
    z[23]=n<T>(1,2)*z[3];
    z[24]= - z[12]*z[23];
    z[14]=z[24] + z[17] + n<T>(1,2)*z[21] + z[14];
    z[14]=z[11]*z[14];
    z[17]=z[11] - 1;
    z[17]=z[22]*z[17];
    z[21]=n<T>(11,4) - z[9];
    z[21]=z[9]*z[21];
    z[21]= - n<T>(7,4) + z[21];
    z[21]=z[21]*z[13];
    z[22]=z[12]*z[18];
    z[15]=z[22] - static_cast<T>(1)- z[15];
    z[22]=n<T>(1,8)*z[7];
    z[15]=z[15]*z[22];
    z[12]= - z[9]*z[12]*z[22];
    z[12]= - z[19] + z[12];
    z[12]=z[8]*z[12];
    z[12]=z[12] + z[15] + z[21] + z[20] + z[17];
    z[12]=z[8]*z[12];
    z[15]=z[4] - n<T>(1,2);
    z[17]=z[15]*z[4];
    z[20]= - z[9] + z[15];
    z[20]=z[9]*z[20];
    z[20]=z[20] + n<T>(11,4) - z[17];
    z[20]=z[20]*z[13];
    z[21]=z[4] - n<T>(3,4);
    z[24]=z[4]*z[21];
    z[12]=z[12] + z[14] + z[20] - n<T>(19,8) + z[24];
    z[14]=z[9]*z[4];
    z[20]=5*z[4];
    z[24]=z[20] - z[14];
    z[25]=n<T>(1,4)*z[9];
    z[24]=z[24]*z[25];
    z[26]=npow(z[4],2);
    z[27]=z[26]*z[9];
    z[28]=static_cast<T>(3)- z[13];
    z[28]=z[28]*z[27];
    z[28]= - n<T>(5,2)*z[26] + z[28];
    z[28]=z[5]*z[28];
    z[24]=n<T>(1,4)*z[28] - z[4] + z[24];
    z[24]=z[5]*z[24];
    z[28]=n<T>(3,2)*z[4];
    z[29]=z[26]*z[5];
    z[30]=z[28] + z[29];
    z[30]=z[5]*z[30];
    z[31]=n<T>(1,2)*z[1];
    z[32]=z[31]*z[26];
    z[33]=npow(z[5],2)*z[32];
    z[34]=n<T>(3,4)*z[29];
    z[35]= - z[4] - z[34];
    z[35]=z[5]*z[35];
    z[33]=z[35] + z[33];
    z[33]=z[1]*z[33];
    z[30]=z[33] + n<T>(1,2) + z[30];
    z[30]=z[30]*z[31];
    z[33]=static_cast<T>(1)- z[25];
    z[33]=z[9]*z[33];
    z[33]= - n<T>(3,4) + z[33];
    z[24]=z[30] + n<T>(1,2)*z[33] + z[24];
    z[24]=z[6]*z[24];
    z[30]= - static_cast<T>(11)+ 13*z[4];
    z[33]=z[20] + 1;
    z[35]= - n<T>(1,4)*z[33] + z[9];
    z[35]=z[9]*z[35];
    z[30]=n<T>(1,4)*z[30] + z[35];
    z[30]=z[30]*z[13];
    z[21]=z[24] + z[30] - z[21];
    z[24]= - static_cast<T>(19)- z[20];
    z[24]=z[4]*z[24];
    z[24]=n<T>(1,8)*z[24] + z[14];
    z[24]=z[24]*z[13];
    z[30]=n<T>(5,8) + z[4];
    z[30]=z[4]*z[30];
    z[24]=z[30] + z[24];
    z[24]=z[9]*z[24];
    z[30]= - n<T>(9,2) + z[9];
    z[25]=z[25]*z[30];
    z[25]=static_cast<T>(2)+ z[25];
    z[25]=z[9]*z[26]*z[25];
    z[25]= - n<T>(11,8)*z[26] + z[25];
    z[25]=z[5]*z[25];
    z[30]= - static_cast<T>(5)- 11*z[4];
    z[30]=z[4]*z[30];
    z[24]=z[25] + n<T>(1,16)*z[30] + z[24];
    z[24]=z[5]*z[24];
    z[25]=n<T>(1,2)*z[4];
    z[30]=n<T>(1,4)*z[4];
    z[35]= - static_cast<T>(1)- z[30];
    z[35]=z[35]*z[25];
    z[35]=z[35] - z[29];
    z[35]=z[5]*z[35];
    z[34]= - z[1]*z[34];
    z[34]=z[34] + z[30] + z[35];
    z[34]=z[1]*z[34];
    z[35]=static_cast<T>(1)+ z[28];
    z[35]=z[4]*z[35];
    z[35]=z[35] + n<T>(7,2)*z[29];
    z[35]=z[5]*z[35];
    z[36]=n<T>(3,4)*z[4];
    z[35]=z[36] + z[35];
    z[34]=n<T>(1,2)*z[35] + z[34];
    z[34]=z[34]*z[31];
    z[21]=z[34] + z[24] + n<T>(1,2)*z[21];
    z[21]=z[6]*z[21];
    z[24]=z[4] + 1;
    z[34]=z[4]*z[24];
    z[34]=z[34] - z[14];
    z[34]=z[9]*z[34];
    z[35]= - static_cast<T>(1)+ n<T>(5,2)*z[4];
    z[30]=z[35]*z[30];
    z[19]=z[30] + z[19];
    z[19]=z[5]*z[19];
    z[30]= - z[4]*z[33];
    z[30]=z[30] - z[29];
    z[33]=n<T>(1,4)*z[2];
    z[30]=z[30]*z[33];
    z[35]= - n<T>(11,2) + z[4];
    z[35]=z[4]*z[35];
    z[35]= - n<T>(1,2) + z[35];
    z[19]=z[30] + z[19] + n<T>(1,2)*z[35] + z[34];
    z[30]=n<T>(1,2)*z[2];
    z[19]=z[19]*z[30];
    z[30]=z[4] - 1;
    z[34]=z[25]*z[30];
    z[35]=z[26]*z[2];
    z[32]=z[32] - z[35] + z[34] + z[29];
    z[32]=z[1]*z[32];
    z[37]=n<T>(1,2)*z[35];
    z[38]=n<T>(3,4) + z[4];
    z[38]=z[4]*z[38];
    z[38]=z[38] + z[37];
    z[38]=z[2]*z[38];
    z[39]= - z[4]*z[30];
    z[32]=z[32] + z[38] - n<T>(7,4)*z[29] + n<T>(1,4) + z[39];
    z[32]=z[32]*z[31];
    z[38]= - z[13] + n<T>(1,4) + z[4];
    z[38]=z[38]*z[14];
    z[17]= - n<T>(7,2)*z[17] + z[38];
    z[13]=z[17]*z[13];
    z[17]= - n<T>(7,2) + z[9];
    z[17]=z[17]*z[27];
    z[17]=n<T>(9,2)*z[26] + z[17];
    z[17]=z[17]*z[18];
    z[18]= - n<T>(1,8) + z[4];
    z[18]=z[4]*z[18];
    z[13]=n<T>(1,4)*z[17] + z[13] + n<T>(1,4) + z[18];
    z[13]=z[5]*z[13];
    z[17]= - z[37] - z[26] - n<T>(1,2)*z[29];
    z[17]=z[2]*z[17];
    z[18]=z[5] - 1;
    z[26]=z[4] + n<T>(1,2);
    z[26]=z[26]*z[4];
    z[18]=z[26]*z[18];
    z[17]=z[17] + n<T>(1,2) + z[18];
    z[17]=z[17]*z[33];
    z[18]=z[26] - z[14];
    z[18]=z[9]*z[18];
    z[18]=z[18] + n<T>(3,2) - z[26];
    z[13]=z[17] + n<T>(1,4)*z[18] + z[13];
    z[13]=z[10]*z[13];
    z[17]=z[28] - 1;
    z[18]= - z[4]*z[17];
    z[18]=z[18] + z[37];
    z[18]=z[2]*z[18];
    z[26]=static_cast<T>(1)- z[20];
    z[18]=n<T>(1,2)*z[26] + z[18];
    z[18]=z[2]*z[18];
    z[18]=z[18] - z[24];
    z[18]=z[2]*z[18];
    z[14]=z[18] + z[14] - z[24];
    z[18]=static_cast<T>(1)- z[36];
    z[18]=z[4]*z[18];
    z[18]=z[18] + n<T>(3,4)*z[35];
    z[18]=z[2]*z[18];
    z[26]= - z[4] - z[35];
    z[26]=z[1]*z[26];
    z[15]=n<T>(1,4)*z[26] - n<T>(1,2)*z[15] + z[18];
    z[15]=z[1]*z[15];
    z[18]=3*z[4];
    z[26]= - n<T>(5,2) + z[18];
    z[26]=z[4]*z[26];
    z[26]=z[26] - n<T>(3,2)*z[35];
    z[26]=z[2]*z[26];
    z[26]=z[26] - static_cast<T>(1)+ n<T>(7,2)*z[4];
    z[26]=z[2]*z[26];
    z[24]=z[26] + z[24];
    z[15]=n<T>(1,2)*z[24] + z[15];
    z[15]=z[1]*z[15];
    z[14]=n<T>(1,2)*z[14] + z[15];
    z[14]=z[14]*z[23];
    z[15]= - static_cast<T>(3)+ z[4];
    z[15]=z[15]*z[25];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[9]*z[15];
    z[23]=n<T>(5,2) - z[4];
    z[23]=z[4]*z[23];
    z[15]=z[15] - n<T>(3,2) + z[23];
    z[15]=z[9]*z[15];
    z[15]=z[34] + z[15];
    z[15]=z[5]*z[15];
    z[16]=z[16] - z[31] + 1;
    z[16]=z[30]*z[16];
    z[15]=z[15] + z[16];
    z[15]=z[15]*z[22];
    z[16]=n<T>(5,2) + z[4];
    z[16]=z[4]*z[16];
    z[17]= - z[9]*z[17];
    z[16]=z[17] - n<T>(23,16) + z[16];
    z[16]=z[9]*z[16];
    z[17]= - n<T>(9,4) - z[20];
    z[17]=z[4]*z[17];
    z[17]=n<T>(7,8) + z[17];
    z[16]=n<T>(1,2)*z[17] + z[16];
    z[16]=z[9]*z[16];
    z[17]=n<T>(1,2) + z[18];
    z[17]=z[4]*z[17];
    z[16]=n<T>(3,8)*z[17] + z[16];
    z[16]=z[5]*z[16];

    r += n<T>(1,2)*z[12] + z[13] + z[14] + z[15] + z[16] + z[19] + z[21] + 
      z[32];
 
    return r;
}

template double qg_2lha_r1365(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1365(const std::array<dd_real,30>&);
#endif
