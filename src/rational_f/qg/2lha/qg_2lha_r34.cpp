#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r34(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[27];
    z[5]=k[6];
    z[6]=k[3];
    z[7]=k[9];
    z[8]=npow(z[6],2);
    z[9]=npow(z[7],2);
    z[10]= - z[8]*z[9];
    z[11]=npow(z[5],2);
    z[10]=z[11] - static_cast<T>(1)- z[10];
    z[12]=n<T>(1,3)*z[7];
    z[8]= - z[8]*z[12];
    z[8]= - z[6] + z[8];
    z[8]=z[7]*z[8];
    z[8]= - n<T>(1,3) + n<T>(1,2)*z[8];
    z[8]=z[2]*z[8];
    z[13]=n<T>(1,3)*z[5];
    z[14]=z[13] - 1;
    z[15]=n<T>(1,2)*z[5];
    z[14]=z[15]*z[14];
    z[16]= - static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[14]= - z[14] + n<T>(1,3)*z[16];
    z[17]=z[1] + 1;
    z[14]=z[17]*z[14];
    z[17]=z[4]*z[14];
    z[8]=z[17] + z[8] - n<T>(1,6)*z[10];
    z[8]=z[3]*z[8];
    z[10]=z[15] - 1;
    z[15]=z[1] + n<T>(5,2);
    z[15]=z[13]*z[15]*z[10];
    z[17]=n<T>(1,2)*z[4];
    z[14]=z[14]*z[17];
    z[14]=z[14] + n<T>(1,4) + z[15];
    z[14]=z[4]*z[14];
    z[15]=z[7]*z[6];
    z[15]=static_cast<T>(1)+ z[15];
    z[12]=z[2]*z[15]*z[12];
    z[15]=z[6]*z[9];
    z[12]=z[15] + z[12];
    z[8]=z[8] + z[14] + n<T>(1,4)*z[12] + n<T>(1,3)*z[11];
    z[8]=z[3]*z[8];
    z[12]=static_cast<T>(1)- z[5];
    z[12]=z[12]*z[13];
    z[13]=static_cast<T>(1)+ n<T>(1,3)*z[1];
    z[10]=z[5]*z[13]*z[10];
    z[10]=n<T>(1,3) + z[10];
    z[10]=z[10]*z[17];
    z[10]=z[12] + z[10];
    z[10]=z[4]*z[10];
    z[9]= - n<T>(1,2)*z[9] - z[11];
    z[8]=z[8] + n<T>(1,6)*z[9] + z[10];
    z[8]=z[3]*z[8];
    z[9]=z[5]*z[16];
    z[9]=static_cast<T>(1)+ z[9];
    z[9]=z[4]*z[5]*z[9];
    z[9]=n<T>(1,2)*z[11] + z[9];
    z[9]=z[4]*z[9];
    z[8]=n<T>(1,6)*z[9] + z[8];

    r += n<T>(1,2)*z[8];
 
    return r;
}

template double qg_2lha_r34(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r34(const std::array<dd_real,30>&);
#endif
