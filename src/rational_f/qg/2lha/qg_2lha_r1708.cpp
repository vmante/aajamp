#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1708(const std::array<T,30>& k) {
  T z[19];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=k[12];
    z[8]=k[14];
    z[9]=z[7] - 1;
    z[10]=n<T>(1,3)*z[7];
    z[11]= - z[9]*z[10];
    z[11]=z[1] + z[11];
    z[12]=n<T>(1,3)*z[8];
    z[13]=static_cast<T>(1)+ z[8];
    z[13]=z[13]*z[12];
    z[11]=n<T>(1,2)*z[11] + z[13];
    z[11]=z[4]*z[11];
    z[13]=z[10] - 1;
    z[14]= - static_cast<T>(1)+ 3*z[1];
    z[14]=z[1]*z[14];
    z[14]=z[14] + z[13];
    z[11]=z[11] + n<T>(1,2)*z[14] + z[12];
    z[11]=z[2]*z[11];
    z[14]=z[4] - z[1];
    z[15]=n<T>(1,2)*z[8];
    z[11]=n<T>(1,2)*z[11] + z[15] + n<T>(1,12)*z[7] - n<T>(1,3) - n<T>(1,4)*z[14];
    z[11]=z[2]*z[11];
    z[14]= - static_cast<T>(1)- z[1];
    z[14]=z[1]*z[14];
    z[14]=z[14] + z[13];
    z[14]=n<T>(1,2)*z[14] + z[12];
    z[14]=z[4]*z[14];
    z[16]=npow(z[1],3);
    z[14]= - n<T>(1,2)*z[16] + z[14];
    z[14]=z[2]*z[14];
    z[16]=npow(z[1],2);
    z[17]=n<T>(1,2) + z[1];
    z[17]=z[4]*z[17];
    z[14]=z[14] - n<T>(1,3)*z[16] + z[17];
    z[14]=z[2]*z[14];
    z[16]=z[4]*z[6];
    z[17]=5*z[5];
    z[18]= - z[6]*z[17];
    z[18]=static_cast<T>(13)+ z[18];
    z[18]=z[18]*z[16];
    z[17]= - n<T>(13,2) - z[17];
    z[17]=z[6]*z[17];
    z[17]=n<T>(1,2)*z[18] + static_cast<T>(5)+ z[17];
    z[17]=z[4]*z[17];
    z[14]=n<T>(1,3)*z[17] + z[14];
    z[14]=z[3]*z[14];
    z[17]= - static_cast<T>(5)+ n<T>(1,2)*z[5];
    z[17]=z[6]*z[17];
    z[17]= - n<T>(5,2) + n<T>(1,3)*z[17];
    z[16]=z[17]*z[16];
    z[17]=static_cast<T>(5)+ z[5];
    z[17]=z[6]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[16]=n<T>(1,6)*z[17] + z[16];
    z[11]=z[14] + n<T>(1,2)*z[16] + z[11];
    z[11]=z[3]*z[11];
    z[14]= - static_cast<T>(1)+ n<T>(1,2)*z[7];
    z[10]=z[14]*z[10];
    z[10]=n<T>(1,2) + z[10];
    z[14]=z[8] - 1;
    z[16]=z[14]*z[12];
    z[10]=n<T>(1,2)*z[10] + z[16];
    z[10]=z[4]*z[10];
    z[16]=z[15] - 1;
    z[12]=z[16]*z[12];
    z[10]=z[10] - n<T>(1,2)*z[13] + z[12];
    z[10]=z[2]*z[10];
    z[12]=z[14]*z[8];
    z[13]=npow(z[7],2);
    z[17]=static_cast<T>(1)- z[13];
    z[17]=n<T>(1,4)*z[17] + z[12];
    z[10]=n<T>(1,3)*z[17] + z[10];
    z[10]=z[2]*z[10];
    z[17]=npow(z[6],2);
    z[10]=z[11] + n<T>(1,12)*z[17] + z[10];
    z[10]=z[3]*z[10];
    z[11]=z[4]*z[14]*z[15];
    z[14]=z[8]*z[16];
    z[9]=z[11] + n<T>(1,4)*z[9] + z[14];
    z[9]=z[2]*z[9];
    z[11]=n<T>(1,2)*z[13] + z[12];
    z[9]=n<T>(1,2)*z[11] + z[9];
    z[9]=z[2]*z[9];
    z[9]=n<T>(1,3)*z[9] + z[10];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r1708(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1708(const std::array<dd_real,30>&);
#endif
