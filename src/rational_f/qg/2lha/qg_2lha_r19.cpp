#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r19(const std::array<T,30>& k) {
  T z[30];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[24];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=k[3];
    z[8]= - z[2] + 2;
    z[8]=z[4]*z[8];
    z[9]=z[5]*z[4];
    z[8]=2*z[9] + static_cast<T>(1)+ z[8];
    z[8]=z[2]*z[8];
    z[10]=z[4] + 2;
    z[11]=z[9] + 1;
    z[12]= - 2*z[4] - z[11];
    z[12]=z[5]*z[12];
    z[8]=z[8] + z[12] - z[1] - z[10];
    z[8]=z[2]*z[8];
    z[12]= - static_cast<T>(1)+ 2*z[1];
    z[13]=static_cast<T>(4)- z[6];
    z[13]=z[6]*z[13];
    z[8]=z[8] + 2*z[12] + z[13];
    z[12]=n<T>(1,3)*z[2];
    z[8]=z[8]*z[12];
    z[13]=n<T>(4,3) - z[1];
    z[14]= - n<T>(11,3) + z[6];
    z[14]=z[6]*z[14];
    z[15]= - n<T>(8,3) + z[6];
    z[15]=z[6]*z[15];
    z[15]=static_cast<T>(2)+ z[15];
    z[15]=z[5]*z[15];
    z[8]=z[8] + z[15] + 2*z[13] + z[14];
    z[8]=z[2]*z[8];
    z[13]=z[5]*z[6];
    z[14]=n<T>(4,3) - z[6];
    z[14]=z[14]*z[13];
    z[15]=n<T>(7,3) - z[6];
    z[15]=z[6]*z[15];
    z[15]= - n<T>(4,3) + z[15];
    z[14]=2*z[15] + z[14];
    z[14]=z[5]*z[14];
    z[15]= - static_cast<T>(7)+ 4*z[1];
    z[16]=n<T>(10,3) - z[6];
    z[16]=z[6]*z[16];
    z[8]=z[8] + z[14] + n<T>(1,3)*z[15] + z[16];
    z[8]=z[2]*z[8];
    z[14]=n<T>(1,3)*z[5];
    z[15]=npow(z[6],2);
    z[16]=z[14]*z[15];
    z[17]=z[6] - 1;
    z[17]=z[17]*z[6];
    z[16]=z[16] + z[17];
    z[18]=z[5]*z[16];
    z[19]= - static_cast<T>(2)+ z[6];
    z[19]=z[6]*z[19];
    z[18]=z[18] + static_cast<T>(1)+ z[19];
    z[18]=z[5]*z[18];
    z[19]=static_cast<T>(2)- z[1];
    z[20]=n<T>(1,3)*z[6];
    z[21]= - static_cast<T>(1)+ z[20];
    z[21]=z[6]*z[21];
    z[8]=z[8] + z[18] + n<T>(1,3)*z[19] + z[21];
    z[8]=z[3]*z[8];
    z[18]=2*z[5];
    z[19]= - z[15]*z[18];
    z[21]=static_cast<T>(5)- n<T>(13,2)*z[6];
    z[21]=z[6]*z[21];
    z[19]=z[21] + z[19];
    z[19]=z[5]*z[19];
    z[21]=static_cast<T>(11)- 7*z[6];
    z[21]=z[6]*z[21];
    z[19]=z[19] - static_cast<T>(4)+ z[21];
    z[19]=z[19]*z[14];
    z[21]= - n<T>(5,2) + z[1];
    z[22]=static_cast<T>(2)- n<T>(5,6)*z[6];
    z[22]=z[6]*z[22];
    z[8]=z[8] + z[19] + n<T>(1,3)*z[21] + z[22];
    z[19]=npow(z[4],2);
    z[21]=n<T>(1,2)*z[19];
    z[22]=z[21]*z[2];
    z[23]=z[19]*z[5];
    z[10]= - z[4]*z[10];
    z[10]=z[22] + z[10] - z[23];
    z[10]=z[2]*z[10];
    z[24]=n<T>(1,2)*z[5];
    z[25]=z[24]*z[19];
    z[26]=static_cast<T>(5)+ z[4];
    z[26]=z[4]*z[26];
    z[26]=z[26] + z[25];
    z[26]=z[5]*z[26];
    z[27]=n<T>(1,2)*z[4];
    z[28]=static_cast<T>(2)+ z[27];
    z[28]=z[4]*z[28];
    z[10]=z[10] + z[26] + n<T>(5,2) + z[28];
    z[10]=z[10]*z[12];
    z[26]= - static_cast<T>(5)- z[1];
    z[11]= - z[4] - z[11];
    z[11]=z[5]*z[11];
    z[10]=z[10] + n<T>(1,3)*z[26] + z[11];
    z[10]=z[2]*z[10];
    z[11]=static_cast<T>(8)- n<T>(5,2)*z[6];
    z[11]=z[11]*z[20];
    z[26]=static_cast<T>(1)- z[17];
    z[26]=z[5]*z[26];
    z[10]=z[10] + n<T>(2,3)*z[26] + z[1] + z[11];
    z[10]=z[10]*z[12];
    z[11]=5*z[6];
    z[26]= - static_cast<T>(14)+ z[11];
    z[20]=z[26]*z[20];
    z[20]=z[20] + n<T>(5,3) - z[1];
    z[26]=2*z[6];
    z[28]=z[26] - 1;
    z[13]=z[28]*z[13];
    z[29]= - n<T>(5,3) + z[6];
    z[29]=z[6]*z[29];
    z[13]=n<T>(2,9)*z[13] + n<T>(5,9) + z[29];
    z[13]=z[5]*z[13];
    z[10]=z[10] + n<T>(1,3)*z[20] + z[13];
    z[10]=z[2]*z[10];
    z[8]=z[10] + n<T>(1,3)*z[8];
    z[8]=z[3]*z[8];
    z[10]=n<T>(1,3)*z[4];
    z[13]=static_cast<T>(4)+ z[4];
    z[13]=z[13]*z[10];
    z[13]=z[13] + z[25];
    z[13]=z[5]*z[13];
    z[20]=z[4] + 1;
    z[20]= - z[20]*z[10];
    z[20]=static_cast<T>(1)+ z[20];
    z[18]= - z[19]*z[18];
    z[18]=z[22] - z[4] + z[18];
    z[18]=z[18]*z[12];
    z[13]=z[18] + n<T>(1,2)*z[20] + z[13];
    z[13]=z[2]*z[13];
    z[18]= - z[9] - n<T>(2,3) - z[27];
    z[18]=z[5]*z[18];
    z[19]= - static_cast<T>(5)+ z[4];
    z[13]=z[13] + n<T>(1,6)*z[19] + z[18];
    z[13]=z[13]*z[12];
    z[16]=n<T>(1,3) - z[16];
    z[16]=z[16]*z[14];
    z[18]=n<T>(1,2) - n<T>(2,9)*z[6];
    z[18]=z[6]*z[18];
    z[13]=z[13] + z[16] + n<T>(1,18) + z[18];
    z[13]=z[2]*z[13];
    z[16]=z[28]*z[26];
    z[18]=z[5]*z[15];
    z[16]=z[16] + z[18];
    z[16]=z[5]*z[16];
    z[11]= - n<T>(11,2) + z[11];
    z[11]=z[6]*z[11];
    z[11]=z[16] + static_cast<T>(1)+ z[11];
    z[11]=z[5]*z[11];
    z[16]= - n<T>(7,2) + z[26];
    z[16]=z[6]*z[16];
    z[11]=z[11] + n<T>(1,2) + z[16];
    z[8]=z[8] + n<T>(1,9)*z[11] + z[13];
    z[8]=z[3]*z[8];
    z[11]=z[4]*z[7];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[11]*z[27];
    z[13]=z[7]*z[27];
    z[13]= - static_cast<T>(1)+ z[13];
    z[9]=z[13]*z[9];
    z[9]=z[11] + z[9];
    z[9]=z[9]*z[14];
    z[11]=static_cast<T>(1)+ z[27];
    z[10]=z[11]*z[10];
    z[10]=z[10] + z[25];
    z[10]=z[5]*z[10];
    z[11]=z[21] - z[23];
    z[11]=z[11]*z[12];
    z[10]=z[11] - n<T>(1,6)*z[4] + z[10];
    z[10]=z[2]*z[10];
    z[9]=z[9] + z[10];
    z[9]=z[2]*z[9];
    z[10]= - z[15]*z[24];
    z[11]=n<T>(1,2) - z[6];
    z[11]=z[6]*z[11];
    z[10]=z[11] + z[10];
    z[10]=z[5]*z[10];
    z[10]= - n<T>(1,2)*z[17] + z[10];
    z[9]=n<T>(1,3)*z[10] + z[9];
    z[8]=n<T>(1,3)*z[9] + z[8];

    r += 5*z[8];
 
    return r;
}

template double qg_2lha_r19(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r19(const std::array<dd_real,30>&);
#endif
