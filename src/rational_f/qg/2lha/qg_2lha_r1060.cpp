#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1060(const std::array<T,30>& k) {
  T z[9];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[9];
    z[4]=k[12];
    z[5]=k[6];
    z[6]=n<T>(1,2)*z[3];
    z[7]=static_cast<T>(3)+ z[2];
    z[7]=z[3]*z[1]*z[7];
    z[7]=z[2] + z[7];
    z[7]=z[7]*z[6];
    z[7]=z[5] + z[7];
    z[8]= - static_cast<T>(1)- z[2];
    z[6]=z[8]*npow(z[1],2)*z[6];
    z[8]= - z[1]*z[2];
    z[6]=z[8] + z[6];
    z[6]=z[3]*z[6];
    z[8]= - z[2] + static_cast<T>(1)- z[5];
    z[6]=n<T>(1,2)*z[8] + z[6];
    z[6]=z[4]*z[6];
    z[6]=n<T>(1,2)*z[7] + z[6];
    z[6]=z[4]*z[6];
    z[7]=npow(z[3],2);
    z[6]= - n<T>(1,4)*z[7] + z[6];

    r += n<T>(1,2)*z[6]*z[4];
 
    return r;
}

template double qg_2lha_r1060(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1060(const std::array<dd_real,30>&);
#endif
