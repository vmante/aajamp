#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1811(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[9];
    z[6]=k[13];
    z[7]=z[1]*npow(z[6],2);
    z[8]= - static_cast<T>(1)+ z[6];
    z[8]=z[2]*z[6]*z[8];
    z[8]=z[7] + z[8];
    z[9]=static_cast<T>(2)+ z[1];
    z[9]=z[1]*z[9];
    z[10]=static_cast<T>(1)+ 2*z[1];
    z[11]=z[2] + z[10];
    z[11]=z[2]*z[11];
    z[9]=z[9] + z[11];
    z[9]=z[2]*z[9];
    z[11]=npow(z[1],2);
    z[9]=z[11] + z[9];
    z[9]=z[4]*z[9];
    z[10]= - 2*z[2] - z[10];
    z[10]=z[2]*z[10];
    z[9]=z[9] - z[1] + z[10];
    z[9]=z[4]*z[9];
    z[9]=z[2] + z[9];
    z[10]=2*z[4];
    z[9]=z[9]*z[10];
    z[8]=n<T>(3,8)*z[8] + z[9];
    z[8]=z[3]*z[8];
    z[9]=npow(z[2],2);
    z[9]= - z[11] + z[9];
    z[9]=z[9]*z[10];
    z[12]=3*z[1];
    z[9]=z[9] + z[12] - z[2];
    z[9]=z[4]*z[9];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[4]*z[9];
    z[7]=n<T>(1,2)*z[6] + z[7];
    z[7]=z[8] + n<T>(3,8)*z[7] + z[9];
    z[7]=z[3]*z[7];
    z[8]=z[11]*z[10];
    z[8]= - z[12] + z[8];
    z[8]=z[4]*z[8];
    z[8]=static_cast<T>(1)+ z[8];
    z[8]=z[4]*z[5]*z[8];

    r += z[7] + z[8];
 
    return r;
}

template double qg_2lha_r1811(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1811(const std::array<dd_real,30>&);
#endif
