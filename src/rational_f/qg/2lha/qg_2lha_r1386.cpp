#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1386(const std::array<T,30>& k) {
  T z[28];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[10];
    z[6]=k[3];
    z[7]=k[14];
    z[8]=k[9];
    z[9]=k[13];
    z[10]=n<T>(1,2)*z[1];
    z[11]=n<T>(1,3)*z[5] + n<T>(1,3) - n<T>(1,2)*z[7];
    z[11]=z[11]*z[10];
    z[12]=n<T>(1,3)*z[9];
    z[13]=static_cast<T>(1)+ z[5];
    z[13]=z[13]*z[12];
    z[13]=z[13] - n<T>(1,2) - z[5];
    z[14]=n<T>(1,2)*z[9];
    z[13]=z[13]*z[14];
    z[15]=npow(z[2],2);
    z[16]=npow(z[1],3);
    z[17]=z[16]*z[15];
    z[18]=n<T>(1,4) + z[5];
    z[11]=n<T>(1,6)*z[17] + z[13] + n<T>(1,3)*z[18] + z[11];
    z[11]=z[2]*z[11];
    z[13]=z[2]*npow(z[1],4);
    z[17]=z[16] - n<T>(1,2)*z[13];
    z[17]=z[2]*z[17];
    z[18]=z[6] - 1;
    z[19]=z[18]*z[9];
    z[20]=z[19]*z[6];
    z[21]=z[6]*z[8];
    z[22]=z[21] - z[8];
    z[23]=z[6]*z[22];
    z[24]= - z[3]*z[13];
    z[17]=z[24] + z[17] + z[23] - z[20];
    z[23]=n<T>(1,2)*z[3];
    z[17]=z[17]*z[23];
    z[20]=z[6] + z[20];
    z[20]=z[9]*z[20];
    z[20]= - z[21] + z[20];
    z[13]=n<T>(1,4)*z[16] - n<T>(1,3)*z[13];
    z[13]=z[13]*z[15];
    z[13]=z[17] + n<T>(1,2)*z[20] + z[13];
    z[13]=z[13]*z[23];
    z[15]=n<T>(1,2)*z[5];
    z[16]= - z[15] - 1;
    z[16]=z[18]*z[16];
    z[17]=n<T>(7,6)*z[6];
    z[20]=z[17] - 1;
    z[24]= - z[6]*z[20];
    z[24]= - n<T>(1,2) + z[24];
    z[24]=z[24]*z[15];
    z[25]=n<T>(1,2) - n<T>(1,3)*z[6];
    z[25]=z[6]*z[25];
    z[24]=z[24] - n<T>(1,2) + z[25];
    z[24]=z[9]*z[24];
    z[16]=z[24] + z[16];
    z[16]=z[16]*z[14];
    z[24]= - static_cast<T>(1)+ n<T>(1,8)*z[5];
    z[25]=z[1]*z[8]*z[7];
    z[11]=z[13] + z[11] + z[16] + n<T>(1,3)*z[24] - n<T>(1,4)*z[25];
    z[13]=npow(z[6],2);
    z[16]=static_cast<T>(1)+ z[22];
    z[16]=z[16]*z[13];
    z[24]=z[1] - static_cast<T>(1)- z[6];
    z[24]=z[1]*z[24];
    z[25]=static_cast<T>(5)- 3*z[6];
    z[25]=z[9]*z[25];
    z[16]=z[25] + z[24] - static_cast<T>(3)+ z[16];
    z[24]=z[1] + 1;
    z[25]=z[24]*z[1];
    z[25]=z[25] + 1;
    z[25]=z[25]*z[1];
    z[26]=z[9] - 1;
    z[25]=z[25] - z[26];
    z[27]=z[2]*z[25];
    z[16]=n<T>(1,2)*z[16] + z[27];
    z[16]=z[3]*z[16];
    z[18]=z[19] - z[18];
    z[18]=z[9]*z[18];
    z[19]=n<T>(1,2)*z[2];
    z[19]=z[25]*z[19];
    z[10]=z[10] + 1;
    z[19]=z[19] + n<T>(3,2)*z[9] - z[10];
    z[19]=z[2]*z[19];
    z[21]= - z[8] - z[21];
    z[21]=z[6]*z[21];
    z[16]=z[16] + z[19] + z[18] + n<T>(1,2)*z[21] - z[10];
    z[16]=z[16]*z[23];
    z[18]= - n<T>(1,2) - z[9];
    z[18]=z[9]*z[18];
    z[18]= - n<T>(5,6)*z[24] + z[18];
    z[19]=n<T>(1,3)*z[2];
    z[19]=z[25]*z[19];
    z[21]=n<T>(1,3)*z[1];
    z[25]= - static_cast<T>(1)+ z[21];
    z[25]=z[1]*z[25];
    z[25]=n<T>(11,3)*z[9] - n<T>(7,3) + z[25];
    z[19]=n<T>(1,4)*z[25] + z[19];
    z[19]=z[2]*z[19];
    z[18]=n<T>(1,4)*z[18] + z[19];
    z[18]=z[2]*z[18];
    z[19]=static_cast<T>(1)- n<T>(7,8)*z[6];
    z[25]=n<T>(1,4)*z[9];
    z[27]=z[6]*z[25];
    z[19]=n<T>(1,3)*z[19] + z[27];
    z[19]=z[9]*z[19];
    z[22]= - n<T>(1,6) + z[22];
    z[16]=z[16] + z[18] + n<T>(1,4)*z[22] + z[19];
    z[16]=z[3]*z[16];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[17]*z[15];
    z[18]=z[9]*z[5];
    z[19]= - z[20]*z[18];
    z[17]=z[17] + z[19];
    z[17]=z[9]*z[17];
    z[19]= - z[8] + n<T>(7,6)*z[5];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[19]=z[1]*z[5];
    z[20]=z[5] + n<T>(5,3)*z[19];
    z[22]=n<T>(5,2)*z[5] - z[18];
    z[22]=z[9]*z[22];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[22]= - n<T>(7,2)*z[5] + z[18];
    z[22]=z[22]*z[12];
    z[27]=z[5] + n<T>(1,3)*z[19];
    z[27]=z[1]*z[27];
    z[27]=n<T>(5,3)*z[5] + z[27];
    z[22]=n<T>(1,2)*z[27] + z[22];
    z[22]=z[2]*z[22];
    z[20]=n<T>(1,4)*z[20] + z[22];
    z[20]=z[2]*z[20];
    z[16]=z[16] + n<T>(1,2)*z[17] + z[20];
    z[17]=z[14] - 1;
    z[20]=z[17]*z[14];
    z[22]=z[10]*z[1];
    z[22]=z[22] + n<T>(3,2);
    z[25]=z[25] - 1;
    z[27]=z[25]*z[9];
    z[22]=z[27] + n<T>(1,2)*z[22];
    z[22]=z[22]*z[2];
    z[24]= - n<T>(1,2)*z[24] - z[22];
    z[24]=z[2]*z[24];
    z[20]=z[20] + z[24];
    z[13]= - z[8]*z[13];
    z[24]=z[9] - 3;
    z[27]=z[9]*z[24];
    z[13]=z[27] + z[13] - z[1];
    z[13]=n<T>(1,4)*z[13] - z[22];
    z[13]=z[3]*z[13];
    z[13]=n<T>(1,2)*z[20] + z[13];
    z[13]=z[13]*z[23];
    z[10]=z[10]*z[21];
    z[20]= - n<T>(1,2) - z[10];
    z[21]= - z[25]*z[12];
    z[20]=n<T>(1,2)*z[20] + z[21];
    z[20]=z[2]*z[20];
    z[21]=z[24]*z[14];
    z[22]= - static_cast<T>(1)- n<T>(5,2)*z[1];
    z[21]=n<T>(1,3)*z[22] + z[21];
    z[20]=n<T>(1,8)*z[21] + z[20];
    z[20]=z[2]*z[20];
    z[17]=z[17]*z[9];
    z[17]=z[17] + n<T>(7,12);
    z[20]= - n<T>(1,4)*z[17] + z[20];
    z[20]=z[2]*z[20];
    z[21]=z[9]*z[26];
    z[21]= - z[8] + n<T>(7,6)*z[21];
    z[13]=z[13] + n<T>(1,8)*z[21] + z[20];
    z[13]=z[3]*z[13];
    z[10]=z[5]*z[10];
    z[10]=z[15] + z[10];
    z[15]= - z[5] + n<T>(1,4)*z[18];
    z[12]=z[15]*z[12];
    z[10]=n<T>(1,2)*z[10] + z[12];
    z[10]=z[2]*z[10];
    z[12]=3*z[5] - z[18];
    z[12]=z[12]*z[14];
    z[14]=z[5] + n<T>(5,2)*z[19];
    z[12]=n<T>(1,3)*z[14] + z[12];
    z[10]=n<T>(1,8)*z[12] + z[10];
    z[10]=z[2]*z[10];
    z[12]=z[5]*z[17];
    z[10]=n<T>(1,4)*z[12] + z[10];
    z[10]=z[2]*z[10];
    z[12]=z[5] - z[18];
    z[12]=z[9]*z[12];
    z[10]=z[13] + n<T>(7,48)*z[12] + z[10];
    z[10]=z[4]*z[10];
    z[10]=n<T>(1,2)*z[16] + z[10];
    z[10]=z[4]*z[10];

    r += z[10] + n<T>(1,2)*z[11];
 
    return r;
}

template double qg_2lha_r1386(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1386(const std::array<dd_real,30>&);
#endif
