#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1635(const std::array<T,30>& k) {
  T z[35];
  T r = 0;

    z[1]=k[2];
    z[2]=k[3];
    z[3]=k[5];
    z[4]=k[9];
    z[5]=k[7];
    z[6]=k[4];
    z[7]=k[11];
    z[8]=k[6];
    z[9]=k[13];
    z[10]=k[12];
    z[11]=n<T>(1,2)*z[5];
    z[12]=n<T>(1,2)*z[8];
    z[13]=static_cast<T>(1)- z[12];
    z[13]=z[13]*z[11];
    z[14]=n<T>(1,4)*z[1];
    z[15]=static_cast<T>(1)- z[5];
    z[15]=z[15]*z[14];
    z[16]=static_cast<T>(1)- n<T>(3,2)*z[10];
    z[17]= - n<T>(3,2) - z[1];
    z[17]=n<T>(1,2)*z[17] + z[10];
    z[17]=z[4]*z[17];
    z[16]=n<T>(7,4)*z[16] + z[17];
    z[16]=z[4]*z[16];
    z[17]=9*z[10];
    z[18]= - n<T>(1,2) + z[17];
    z[16]=n<T>(1,4)*z[18] + z[16];
    z[16]=z[4]*z[16];
    z[18]=z[2] - 1;
    z[19]=n<T>(1,2)*z[1];
    z[20]=z[18]*z[19];
    z[21]=n<T>(1,2)*z[2];
    z[22]= - static_cast<T>(1)+ z[21];
    z[22]=z[2]*z[22];
    z[20]=z[20] + n<T>(1,2) + z[22];
    z[22]=n<T>(1,2)*z[3];
    z[20]=z[20]*z[22];
    z[23]=n<T>(5,8)*z[10];
    z[24]=3*z[7];
    z[25]= - z[24] - static_cast<T>(1)+ 7*z[8];
    z[26]=z[2]*z[8];
    z[27]=n<T>(3,8)*z[26] - n<T>(3,8) - z[8];
    z[27]=z[2]*z[27];
    z[13]=z[20] + z[16] - z[23] + z[15] + z[13] + n<T>(1,8)*z[25] + z[27];
    z[13]=z[3]*z[13];
    z[15]=z[8] - n<T>(3,2)*z[9];
    z[15]=z[15]*z[21];
    z[16]=3*z[10] - n<T>(3,2)*z[1] + n<T>(11,2) + z[7];
    z[20]=n<T>(5,4)*z[10] - static_cast<T>(1)- z[19];
    z[20]=z[4]*z[20];
    z[20]=z[20] - n<T>(5,2)*z[10] + n<T>(9,8) - z[1];
    z[20]=z[4]*z[20];
    z[16]=n<T>(1,2)*z[16] + z[20];
    z[16]=z[4]*z[16];
    z[12]=z[12] + 1;
    z[20]=z[8] - 1;
    z[25]=z[20]*z[5];
    z[27]=n<T>(3,4)*z[9];
    z[13]=z[13] + z[16] - n<T>(1,4)*z[10] - z[25] + z[15] - n<T>(11,4)*z[7] + 
    z[27] + z[12];
    z[13]=z[3]*z[13];
    z[15]=z[9] + n<T>(5,2)*z[7];
    z[15]=z[15]*z[21];
    z[16]=z[10]*z[9];
    z[28]= - static_cast<T>(1)+ z[9];
    z[28]=z[5]*z[28];
    z[28]=z[28] - z[16];
    z[29]=n<T>(1,2)*z[4];
    z[28]=z[28]*z[29];
    z[30]= - static_cast<T>(1)- z[9];
    z[30]=z[5]*z[30];
    z[15]=z[28] + n<T>(5,4)*z[16] + n<T>(3,4)*z[30] + z[15] + n<T>(1,4)*z[7] - n<T>(3,4)
    - z[9];
    z[15]=z[15]*z[29];
    z[27]=z[27] - z[8];
    z[28]=z[27] + n<T>(3,4);
    z[30]= - z[28]*z[11];
    z[31]=n<T>(1,2)*z[16];
    z[32]=n<T>(25,8) + z[8];
    z[32]=z[7]*z[32];
    z[15]=z[15] - z[31] + z[30] + n<T>(7,8)*z[9] + z[32];
    z[15]=z[4]*z[15];
    z[30]= - z[26] - static_cast<T>(1)+ 3*z[8];
    z[21]=z[30]*z[21];
    z[20]=z[20]*z[11];
    z[20]=z[20] + z[21] + static_cast<T>(1)- n<T>(3,2)*z[8];
    z[20]=z[3]*z[20];
    z[11]=z[11] - 1;
    z[11]=z[8]*z[11];
    z[21]= - n<T>(1,2) + z[10];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(7,4)*z[10] + z[21];
    z[21]=z[4]*z[21];
    z[21]=n<T>(3,2)*z[21] + z[7] + n<T>(9,8)*z[10];
    z[21]=z[4]*z[21];
    z[11]=n<T>(1,4)*z[20] + z[21] + n<T>(1,2)*z[26] + z[11];
    z[11]=z[3]*z[11];
    z[20]=z[5]*z[28];
    z[20]=z[20] - z[27];
    z[21]=n<T>(1,4)*z[4];
    z[27]= - static_cast<T>(5)+ z[17];
    z[27]=z[27]*z[21];
    z[27]=z[27] - n<T>(25,8)*z[10] - static_cast<T>(1)+ n<T>(1,8)*z[7];
    z[27]=z[4]*z[27];
    z[27]=z[27] + n<T>(15,4)*z[7] + z[10];
    z[27]=z[4]*z[27];
    z[11]=z[11] + n<T>(1,2)*z[20] + z[27];
    z[11]=z[3]*z[11];
    z[20]=3*z[9];
    z[27]=3*z[16];
    z[28]=z[7] + z[9];
    z[30]= - z[2]*z[28];
    z[30]= - z[27] + z[30] + z[20] - z[7];
    z[30]=z[4]*z[30];
    z[32]= - 5*z[9] - 13*z[7];
    z[30]=z[30] + n<T>(1,2)*z[32] + 5*z[16];
    z[30]=z[30]*z[29];
    z[32]=z[7]*z[8];
    z[30]=z[30] + z[32] - z[16];
    z[30]=z[30]*z[29];
    z[33]=static_cast<T>(1)+ z[22];
    z[34]=z[10] + z[7];
    z[33]=z[3]*z[34]*z[33];
    z[31]= - z[6]*z[31];
    z[31]=z[31] + z[33];
    z[31]=z[31]*npow(z[4],3);
    z[27]= - z[27] + z[28];
    z[27]=z[4]*z[27];
    z[27]=n<T>(5,2)*z[16] + z[27];
    z[28]=npow(z[4],2);
    z[27]=z[27]*z[28];
    z[27]=n<T>(1,2)*z[27] + z[31];
    z[31]=n<T>(1,2)*z[6];
    z[27]=z[27]*z[31];
    z[31]=z[4]*z[10];
    z[31]= - n<T>(7,8)*z[34] + z[31];
    z[31]=z[3]*z[31];
    z[33]= - z[7] + 7*z[10];
    z[33]=z[4]*z[33];
    z[33]=z[33] - 9*z[7] - 5*z[10];
    z[31]=n<T>(1,4)*z[33] + z[31];
    z[28]=z[3]*z[28]*z[31];
    z[27]=z[27] + z[30] + z[28];
    z[27]=z[6]*z[27];
    z[16]= - z[32] + n<T>(1,4)*z[16];
    z[11]=z[27] + z[11] + n<T>(1,2)*z[16] + z[15];
    z[11]=z[6]*z[11];
    z[15]=static_cast<T>(3)- z[9];
    z[16]=n<T>(1,2) + z[8];
    z[16]=z[7]*z[16];
    z[27]=n<T>(1,2)*z[9];
    z[28]=z[27] - z[7];
    z[28]=z[2]*z[28];
    z[15]=z[28] + n<T>(1,2)*z[15] + z[16];
    z[16]=z[4] + n<T>(1,2);
    z[28]=z[1]*z[5];
    z[16]=z[28]*z[16];
    z[30]=3*z[5];
    z[16]= - z[30] + z[16];
    z[16]=z[16]*z[29];
    z[15]=z[16] + n<T>(1,2)*z[15] + z[25];
    z[15]=z[4]*z[15];
    z[16]=z[26]*z[7];
    z[25]= - n<T>(9,2) - z[8];
    z[25]=z[7]*z[25];
    z[25]= - z[16] - n<T>(1,4)*z[9] + z[25];
    z[11]=z[11] + z[13] + n<T>(1,2)*z[25] + z[15];
    z[11]=z[6]*z[11];
    z[13]=n<T>(1,4)*z[2];
    z[15]=static_cast<T>(5)- 3*z[2];
    z[15]=z[15]*z[13];
    z[25]= - z[19] + static_cast<T>(1)- z[13];
    z[25]=z[1]*z[25];
    z[26]=z[10] - 1;
    z[31]= - static_cast<T>(1)- z[1];
    z[31]=z[1]*z[31];
    z[31]=z[31] + z[26];
    z[31]=z[4]*z[31];
    z[32]=z[1] - z[26];
    z[31]=n<T>(7,2)*z[32] + z[31];
    z[31]=z[4]*z[31];
    z[17]=z[17] - static_cast<T>(9)- z[1];
    z[17]=n<T>(1,2)*z[17] + z[31];
    z[17]=z[17]*z[29];
    z[18]= - z[18] - z[1];
    z[18]=z[3]*z[14]*z[2]*z[18];
    z[31]= - static_cast<T>(1)- z[13];
    z[31]=z[10]*z[31];
    z[15]=z[18] + z[17] + z[31] + z[15] + z[25];
    z[15]=z[15]*z[22];
    z[17]=static_cast<T>(7)+ z[20];
    z[18]=z[8] - z[9];
    z[18]=z[2]*z[18];
    z[17]=n<T>(3,2)*z[18] + n<T>(1,2)*z[17] - z[24];
    z[13]=z[17]*z[13];
    z[17]= - static_cast<T>(1)+ z[1];
    z[17]=z[1]*z[17];
    z[17]=z[17] + z[26];
    z[17]=z[17]*z[21];
    z[18]=n<T>(1,8)*z[1];
    z[20]= - static_cast<T>(1)- z[18];
    z[20]=z[1]*z[20];
    z[17]=z[17] - z[23] + n<T>(5,8) + z[20];
    z[17]=z[4]*z[17];
    z[20]=static_cast<T>(3)- z[1];
    z[20]=z[20]*z[18];
    z[17]=z[17] + n<T>(1,2)*z[10] + static_cast<T>(1)+ z[20];
    z[17]=z[4]*z[17];
    z[20]=z[30] - z[28];
    z[21]=static_cast<T>(5)+ z[20];
    z[18]=z[21]*z[18];
    z[21]= - static_cast<T>(3)+ z[8];
    z[21]=z[5]*z[21];
    z[12]=z[15] + z[17] + z[18] + n<T>(1,8)*z[21] + z[13] + n<T>(3,8)*z[7] - 
    z[12];
    z[12]=z[3]*z[12];
    z[13]=5*z[7];
    z[15]= - n<T>(3,2)*z[16] + z[27] - z[13];
    z[15]=z[2]*z[15];
    z[13]=z[15] + n<T>(1,2) + z[13];
    z[15]=5*z[5] + z[28];
    z[15]=z[15]*z[19];
    z[16]=z[5] - z[28];
    z[16]=z[4]*z[1]*z[16];
    z[15]=z[16] - z[30] + z[15];
    z[15]=z[15]*z[29];
    z[14]= - z[20]*z[14];
    z[16]= - n<T>(1,2) + z[8];
    z[16]=z[5]*z[16];
    z[14]=z[15] + z[16] + z[14];
    z[14]=z[14]*z[29];
    z[15]= - z[1]*z[7];
    z[11]=z[11] + z[12] + z[14] + n<T>(1,4)*z[13] + z[15];

    r += n<T>(1,2)*z[11];
 
    return r;
}

template double qg_2lha_r1635(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1635(const std::array<dd_real,30>&);
#endif
