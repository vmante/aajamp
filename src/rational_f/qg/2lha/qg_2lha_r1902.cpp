#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1902(const std::array<T,30>& k) {
  T z[10];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[9];
    z[5]=k[4];
    z[6]=k[12];
    z[7]= - z[5]*z[6];
    z[7]=static_cast<T>(1)+ z[7];
    z[7]=z[5]*z[7];
    z[7]=z[4] - static_cast<T>(1)+ z[7];
    z[8]=z[1]*z[3];
    z[9]=n<T>(1,2)*z[8] + n<T>(1,2) - z[4];
    z[9]=z[1]*z[9];
    z[7]=n<T>(1,2)*z[7] + z[9];
    z[7]=z[2]*z[7];
    z[7]=z[7] - n<T>(1,2)*z[4] + z[8];
    z[7]=z[2]*z[7];
    z[7]=z[7] + n<T>(1,2)*z[3];

    r += n<T>(3,4)*z[7];
 
    return r;
}

template double qg_2lha_r1902(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1902(const std::array<dd_real,30>&);
#endif
