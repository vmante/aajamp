#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1442(const std::array<T,30>& k) {
  T z[62];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[7];
    z[8]=k[2];
    z[9]=k[11];
    z[10]=k[10];
    z[11]=k[9];
    z[12]=k[5];
    z[13]=k[21];
    z[14]=k[18];
    z[15]=k[14];
    z[16]=k[6];
    z[17]=npow(z[6],2);
    z[18]=z[17]*z[1];
    z[19]= - static_cast<T>(1)- n<T>(1,6)*z[6];
    z[19]=z[6]*z[19];
    z[19]=z[19] + n<T>(1,12)*z[18];
    z[19]=z[1]*z[19];
    z[20]=n<T>(1,16)*z[7];
    z[21]=n<T>(37,9) - z[10];
    z[21]=z[7]*z[21];
    z[21]=z[21] - n<T>(37,9) + n<T>(1,4)*z[10];
    z[21]=z[21]*z[20];
    z[22]= - z[13] + n<T>(101,96)*z[10];
    z[21]=z[21] - z[22];
    z[21]=z[7]*z[21];
    z[23]=n<T>(1,3)*z[2];
    z[24]=z[23] - n<T>(1,3);
    z[24]=z[12]*z[24];
    z[25]=npow(z[13],3);
    z[26]=n<T>(3,4)*z[25];
    z[27]=n<T>(1,3)*z[6];
    z[28]=n<T>(5,2) + z[27];
    z[28]=z[6]*z[28];
    z[29]=n<T>(109,16) + 5*z[10];
    z[29]=z[10]*z[29];
    z[19]=z[21] - z[26] + n<T>(1,54)*z[29] + n<T>(5,32)*z[19] + n<T>(1,8)*z[28] + 
    z[24];
    z[19]=z[5]*z[19];
    z[21]=n<T>(1,3)*z[15];
    z[24]= - static_cast<T>(1)+ n<T>(1,4)*z[15];
    z[24]=z[24]*z[21];
    z[28]= - n<T>(229,128) + z[14];
    z[29]= - n<T>(1,12)*z[1] + n<T>(1,12);
    z[29]=z[6]*z[29];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[1]*z[29];
    z[30]=n<T>(3,2)*z[13];
    z[31]=static_cast<T>(5)- z[30];
    z[31]=z[13]*z[31];
    z[24]=z[31] + z[24] - n<T>(725,288)*z[10] + n<T>(9,16)*z[16] + n<T>(5,16)*z[29]
    - n<T>(1,32)*z[6] + n<T>(1,9)*z[28] - n<T>(17,16)*z[12];
    z[28]=z[2]*npow(z[3],2);
    z[26]= - z[26] + n<T>(5,768)*z[28];
    z[28]=n<T>(2,9)*z[3];
    z[29]=n<T>(125,6) + z[1];
    z[29]=n<T>(5,128)*z[29] + z[28];
    z[29]=z[3]*z[29];
    z[29]= - n<T>(47,192) + z[29];
    z[31]=n<T>(1,3)*z[3];
    z[29]=z[29]*z[31];
    z[29]=z[29] + n<T>(1,3)*z[12] - z[26];
    z[29]=z[2]*z[29];
    z[32]= - static_cast<T>(1)+ n<T>(1,2)*z[15];
    z[33]=n<T>(1,6)*z[15];
    z[32]=z[32]*z[33];
    z[32]=z[32] - n<T>(127,128) - n<T>(1,9)*z[14];
    z[34]=n<T>(7,16) + n<T>(5,27)*z[10];
    z[34]=z[10]*z[34];
    z[34]=z[34] - n<T>(19,96)*z[12] + z[32];
    z[35]= - n<T>(125,3) - z[1];
    z[35]=z[1]*z[35];
    z[35]= - n<T>(19,32)*z[10] + n<T>(23,9) + n<T>(5,64)*z[35];
    z[36]= - z[1]*z[28];
    z[35]=n<T>(1,4)*z[35] + z[36];
    z[35]=z[3]*z[35];
    z[36]= - n<T>(119,96)*z[10] + n<T>(47,96)*z[1] + n<T>(155,32) + n<T>(1,3)*z[14];
    z[35]=n<T>(1,2)*z[36] + z[35];
    z[35]=z[35]*z[31];
    z[36]=npow(z[13],2);
    z[29]=z[29] + z[35] + n<T>(1,2)*z[34] - 2*z[36];
    z[29]=z[2]*z[29];
    z[34]=n<T>(61,3) + n<T>(1,2)*z[1];
    z[35]= - 5*z[34] - 19*z[10];
    z[28]=n<T>(1,128)*z[35] - z[28];
    z[28]=z[28]*z[31];
    z[35]= - n<T>(187,9) + z[10];
    z[37]=n<T>(37,9)*z[1] - z[10];
    z[37]=z[7]*z[37];
    z[35]=n<T>(1,4)*z[35] + z[37];
    z[35]=z[35]*z[20];
    z[37]=npow(z[11],2);
    z[23]=z[23] - 1;
    z[38]=z[2]*z[23];
    z[38]=n<T>(11,9) + z[38];
    z[38]=z[2]*z[38];
    z[38]= - n<T>(5,9) + z[38];
    z[38]=z[38]*z[37];
    z[19]=z[19] + n<T>(1,4)*z[38] + z[29] + z[35] + n<T>(1,2)*z[24] + z[28];
    z[19]=z[4]*z[19];
    z[24]= - static_cast<T>(1)+ z[30];
    z[24]=z[24]*z[36];
    z[17]=z[24] + n<T>(301,432)*z[10] - n<T>(5,192)*z[18] + n<T>(5,96)*z[17] + n<T>(15,32)*z[12] + z[32];
    z[18]=n<T>(4,9)*z[3];
    z[24]=n<T>(61,3) + z[1];
    z[24]=n<T>(5,128)*z[24] + z[18];
    z[24]=z[3]*z[24];
    z[24]= - n<T>(47,192) + z[24];
    z[24]=z[24]*z[31];
    z[24]=z[24] + n<T>(2,3)*z[12] - z[26];
    z[24]=z[2]*z[24];
    z[26]=n<T>(1,2)*z[10];
    z[28]=3*z[10];
    z[29]= - n<T>(37,9) + z[28];
    z[29]=z[7]*z[29];
    z[29]= - z[26] + z[29];
    z[29]=z[29]*z[20];
    z[22]=z[29] + z[22];
    z[22]=z[7]*z[22];
    z[29]=npow(z[10],2);
    z[29]=z[12] - n<T>(5,18)*z[29];
    z[22]=z[22] + n<T>(1,3)*z[29] + n<T>(3,2)*z[25];
    z[22]=z[5]*z[22];
    z[29]=z[1] - 1;
    z[18]= - z[29]*z[18];
    z[30]= - z[1]*z[34];
    z[30]=n<T>(407,9) + z[30];
    z[18]=n<T>(5,128)*z[30] + z[18];
    z[18]=z[3]*z[18];
    z[30]=static_cast<T>(209)+ n<T>(47,2)*z[1];
    z[18]=n<T>(1,96)*z[30] + z[18];
    z[18]=z[18]*z[31];
    z[29]=z[7]*z[29];
    z[28]= - n<T>(37,9)*z[29] - n<T>(35,36) + z[28];
    z[28]=z[7]*z[28];
    z[26]= - z[26] + z[28];
    z[20]=z[26]*z[20];
    z[26]=n<T>(1,2)*z[2];
    z[28]=z[26] - 1;
    z[29]=z[2]*z[28];
    z[29]=n<T>(5,6) + z[29];
    z[29]=z[29]*z[37];
    z[17]=z[19] + z[22] + n<T>(1,6)*z[29] + z[24] + z[20] + n<T>(1,2)*z[17] + 
    z[18];
    z[17]=z[4]*z[17];
    z[18]=n<T>(1,2)*z[13];
    z[19]=z[18] - 1;
    z[20]=3*z[13];
    z[19]=z[19]*z[20];
    z[22]=17*z[16];
    z[24]=n<T>(1,2)*z[6];
    z[29]=z[24] + n<T>(49,4) + n<T>(95,3)*z[12];
    z[29]=n<T>(1,2)*z[29] - z[22];
    z[29]=n<T>(1,2)*z[29] + z[21];
    z[29]=n<T>(1,4)*z[29] + z[19];
    z[30]= - static_cast<T>(91)- 11*z[6];
    z[30]=z[30]*z[27];
    z[30]=z[30] + n<T>(355,27) - 19*z[12];
    z[30]=n<T>(1,2)*z[30] + 9*z[16];
    z[32]=z[21] - 1;
    z[32]=z[32]*z[15];
    z[30]=n<T>(1,8)*z[30] + z[32];
    z[34]=n<T>(1,12)*z[7];
    z[35]=n<T>(7,3)*z[7];
    z[37]= - static_cast<T>(1)- z[35];
    z[37]=z[7]*z[37];
    z[37]=n<T>(5,8) + z[37];
    z[37]=z[37]*z[34];
    z[38]= - static_cast<T>(5)+ z[20];
    z[38]=z[13]*z[38];
    z[38]=static_cast<T>(1)+ n<T>(1,4)*z[38];
    z[38]=z[13]*z[38];
    z[30]=z[37] + n<T>(1,4)*z[30] + z[38];
    z[30]=z[5]*z[30];
    z[37]=37*z[2];
    z[38]=z[23]*z[37];
    z[38]=n<T>(301,9) + z[38];
    z[38]=z[2]*z[38];
    z[38]= - n<T>(79,9) + z[38];
    z[39]=n<T>(1,2)*z[11];
    z[38]=z[38]*z[39];
    z[40]= - n<T>(397,3) + n<T>(143,2)*z[2];
    z[40]=z[2]*z[40];
    z[40]=n<T>(397,6) + z[40];
    z[38]=n<T>(1,3)*z[40] + z[38];
    z[38]=z[11]*z[38];
    z[40]=n<T>(15,4)*z[3];
    z[41]=z[40] + 29;
    z[41]=z[41]*z[3];
    z[42]=n<T>(203,6) - z[41];
    z[42]=z[2]*z[42];
    z[38]=z[42] + z[38];
    z[42]=n<T>(29,64) + n<T>(2,27)*z[3];
    z[42]=z[3]*z[42];
    z[29]=z[30] + n<T>(3,32)*z[7] + n<T>(1,2)*z[29] + z[42] + n<T>(1,32)*z[38];
    z[29]=z[4]*z[29];
    z[30]=n<T>(785,128) + n<T>(4,3)*z[3];
    z[30]=z[30]*z[31];
    z[30]=z[30] + n<T>(227,64);
    z[30]=z[30]*z[31];
    z[38]= - n<T>(1,8) - z[6];
    z[27]=z[38]*z[27];
    z[38]=n<T>(397,54) - 3*z[12];
    z[27]=n<T>(25,4)*z[16] + n<T>(5,8)*z[38] + z[27];
    z[38]=z[20] + 1;
    z[42]= - z[13]*z[38];
    z[42]=static_cast<T>(1)+ z[42];
    z[42]=z[13]*z[42];
    z[27]=n<T>(1,2)*z[27] + z[42];
    z[35]=n<T>(11,8) - z[35];
    z[35]=z[7]*z[35];
    z[35]=n<T>(23,8) + z[35];
    z[35]=z[35]*z[34];
    z[28]=z[28]*z[37];
    z[28]=z[28] + n<T>(79,6);
    z[42]=n<T>(1,3)*z[11];
    z[43]=z[28]*z[42];
    z[43]=z[43] - n<T>(119,9) + 15*z[2];
    z[43]=z[11]*z[43];
    z[44]=n<T>(9,2)*z[13];
    z[45]=static_cast<T>(5)- z[44];
    z[45]=z[45]*z[18];
    z[45]= - static_cast<T>(1)+ z[45];
    z[45]=z[13]*z[45];
    z[46]=static_cast<T>(1)+ n<T>(7,2)*z[7];
    z[46]=z[7]*z[46];
    z[46]= - n<T>(5,16) + z[46];
    z[46]=z[7]*z[46];
    z[45]=n<T>(1,6)*z[46] - n<T>(9,32)*z[16] + z[45];
    z[45]=z[5]*z[45];
    z[27]=z[29] + z[45] + n<T>(1,16)*z[43] + z[35] + n<T>(1,4)*z[27] - z[30];
    z[27]=z[4]*z[27];
    z[29]=n<T>(1,4)*z[6];
    z[35]=n<T>(27,4)*z[16];
    z[43]= - n<T>(11,8) + z[12];
    z[43]= - z[35] + n<T>(11,3)*z[43] - z[29];
    z[45]=11*z[12];
    z[46]= - n<T>(65,4) - z[45];
    z[29]=z[35] + n<T>(1,3)*z[46] - z[29];
    z[46]=n<T>(3,4)*z[7];
    z[21]=z[46] - z[21];
    z[29]=z[20] + n<T>(1,2)*z[29] + z[21];
    z[29]=z[5]*z[29];
    z[47]=n<T>(293,2) - 155*z[2];
    z[29]=n<T>(1,72)*z[47] + z[29];
    z[29]=z[4]*z[29];
    z[47]=static_cast<T>(17)- z[40];
    z[47]=z[47]*z[26];
    z[48]=z[11]*z[2];
    z[49]= - n<T>(79,3) + z[37];
    z[49]=z[49]*z[48];
    z[50]=9*z[13];
    z[51]=n<T>(65,3) - n<T>(81,2)*z[16];
    z[51]= - n<T>(9,4)*z[7] + n<T>(1,4)*z[51] - z[50];
    z[51]=z[5]*z[51];
    z[21]=z[29] + z[51] + n<T>(1,12)*z[49] + z[47] - z[20] + n<T>(1,2)*z[43] + 
    z[21];
    z[21]=z[4]*z[21];
    z[29]=static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[43]= - z[26] - z[29];
    z[47]=npow(z[2],2);
    z[49]= - npow(z[2],3)*z[42];
    z[49]= - z[47] + z[49];
    z[39]=z[49]*z[39];
    z[39]=n<T>(1,3)*z[43] + z[39];
    z[39]=z[5]*z[39];
    z[43]=z[26] - z[29];
    z[43]=z[2]*z[43];
    z[49]=z[8] + 1;
    z[51]=z[49]*z[8];
    z[51]=z[51] + 1;
    z[43]=n<T>(1,2)*z[51] + z[43];
    z[47]=z[47]*z[11];
    z[23]=z[23]*z[47];
    z[52]=n<T>(1,2)*z[4];
    z[53]= - n<T>(1,3)*z[4] + 1;
    z[53]=z[5]*z[53];
    z[53]= - n<T>(1,3) + z[2] + z[53];
    z[53]=z[53]*z[52];
    z[23]=z[53] + z[39] + n<T>(1,3)*z[43] + n<T>(1,2)*z[23];
    z[23]=z[9]*z[23];
    z[39]= - n<T>(53,4) - z[45];
    z[39]=n<T>(1,3)*z[39] + z[35];
    z[43]=n<T>(1,4)*z[2];
    z[53]= - n<T>(949,36)*z[2] + n<T>(91,9) + n<T>(15,2)*z[3];
    z[53]=z[53]*z[43];
    z[37]=n<T>(205,6) - z[37];
    z[37]=z[37]*z[47];
    z[37]=n<T>(1,24)*z[37] + z[53] + n<T>(1,2)*z[39] + z[20];
    z[37]=z[11]*z[37];
    z[39]= - static_cast<T>(1)+ n<T>(27,2)*z[16];
    z[39]=n<T>(1,16)*z[47] + z[43] + n<T>(1,4)*z[39] + z[20];
    z[39]=z[11]*z[39];
    z[43]=z[5]*z[11];
    z[47]=z[43] - 1;
    z[53]=z[13] + n<T>(9,8)*z[16];
    z[54]=z[53]*z[47];
    z[55]= - static_cast<T>(1)+ 27*z[16];
    z[55]=n<T>(1,8)*z[55] + z[20];
    z[55]=z[11]*z[55];
    z[54]=z[55] + z[54];
    z[54]=z[5]*z[54];
    z[39]=z[39] + z[54];
    z[54]=3*z[7];
    z[55]=z[54]*z[29];
    z[56]= - n<T>(19,6) + z[55];
    z[39]=n<T>(1,2)*z[56] + 3*z[39];
    z[39]=z[5]*z[39];
    z[56]= - z[51]*z[54];
    z[57]=static_cast<T>(173)+ n<T>(275,2)*z[8];
    z[56]= - n<T>(71,4)*z[2] + n<T>(1,18)*z[57] + z[56];
    z[21]=n<T>(11,3)*z[23] + z[21] + z[39] + n<T>(1,4)*z[56] + z[37];
    z[21]=z[9]*z[21];
    z[23]=z[3] + 3;
    z[37]=n<T>(15,32)*z[3];
    z[23]=z[23]*z[37];
    z[39]=3*z[36];
    z[23]=z[23] - z[39];
    z[56]=5*z[16];
    z[57]=17*z[12];
    z[58]= - n<T>(169,36) + z[57];
    z[58]=n<T>(1,4)*z[58] + z[56];
    z[59]=n<T>(1,8)*z[2];
    z[60]=n<T>(1405,36) - z[41];
    z[60]=z[60]*z[59];
    z[40]=n<T>(37,3)*z[2] - n<T>(133,9) + z[40];
    z[40]=z[40]*z[48];
    z[40]=n<T>(1,8)*z[40] + z[60] + n<T>(1,2)*z[58] - z[23];
    z[40]=z[11]*z[40];
    z[58]=9*z[36];
    z[48]=n<T>(1,96)*z[48];
    z[60]= - z[48] + z[58] - n<T>(157,96) + z[56];
    z[60]=z[11]*z[60];
    z[55]= - n<T>(1,2) + z[55];
    z[61]=n<T>(1,4)*z[7];
    z[55]=z[55]*z[61];
    z[39]=z[39]*z[47];
    z[47]=n<T>(5,2)*z[16] + z[58];
    z[47]=z[11]*z[47];
    z[39]=z[47] + z[39];
    z[39]=z[5]*z[39];
    z[39]=z[39] + z[60] - 3*z[53] + z[55];
    z[39]=z[5]*z[39];
    z[47]=z[51]*z[7];
    z[51]= - n<T>(3,2)*z[47] + static_cast<T>(7)+ n<T>(11,2)*z[8];
    z[51]=z[7]*z[51];
    z[51]= - n<T>(347,72) + z[51];
    z[21]=z[21] + z[39] + n<T>(1,4)*z[51] + z[40];
    z[39]=static_cast<T>(3)+ z[6];
    z[39]=z[39]*z[24];
    z[39]=z[39] + n<T>(241,6) - z[57];
    z[39]= - z[32] + n<T>(1,4)*z[39] - z[56];
    z[40]=n<T>(1,8)*z[7];
    z[51]= - static_cast<T>(17)+ z[54];
    z[51]=z[51]*z[40];
    z[41]= - n<T>(57,2) + z[41];
    z[41]=z[41]*z[59];
    z[23]=z[41] + z[51] + n<T>(1,2)*z[39] + z[23];
    z[39]= - n<T>(239,6) - z[45];
    z[35]=n<T>(1,3)*z[39] + z[35];
    z[39]= - n<T>(7,3) + n<T>(15,8)*z[3];
    z[26]=z[39]*z[26];
    z[39]=n<T>(79,3) - n<T>(37,2)*z[2];
    z[39]=z[2]*z[39];
    z[39]= - n<T>(79,6) + z[39];
    z[39]=z[11]*z[39];
    z[26]=n<T>(1,24)*z[39] + z[26] - z[37] + n<T>(1,2)*z[35] + z[20];
    z[35]=n<T>(97,2) + 95*z[12];
    z[24]=static_cast<T>(7)+ z[24];
    z[24]=z[6]*z[24];
    z[24]=n<T>(1,3)*z[35] + z[24];
    z[24]=n<T>(1,2)*z[24] - z[22];
    z[33]=static_cast<T>(1)- z[33];
    z[33]=z[15]*z[33];
    z[24]=n<T>(1,4)*z[24] + z[33];
    z[33]= - static_cast<T>(1)+ z[46];
    z[33]=z[33]*z[61];
    z[19]=z[33] + n<T>(1,2)*z[24] + z[19];
    z[19]=z[5]*z[19];
    z[19]=n<T>(1,2)*z[26] + z[19];
    z[19]=z[19]*z[52];
    z[24]=static_cast<T>(1)- n<T>(3,4)*z[13];
    z[20]=z[24]*z[20];
    z[24]=static_cast<T>(1)- n<T>(9,8)*z[7];
    z[24]=z[24]*z[61];
    z[22]= - n<T>(97,24) + z[22];
    z[20]=z[24] + n<T>(1,8)*z[22] + z[20];
    z[20]=z[5]*z[20];
    z[22]= - z[28]*z[48];
    z[24]= - n<T>(1,9) - n<T>(37,64)*z[2];
    z[24]=z[2]*z[24];
    z[22]=z[22] + n<T>(79,192) + z[24];
    z[22]=z[11]*z[22];
    z[19]=z[19] + z[20] + n<T>(1,4)*z[23] + z[22];
    z[19]=z[4]*z[19];
    z[19]=z[19] + n<T>(1,4)*z[21];
    z[19]=z[9]*z[19];
    z[20]=3*z[43] - 3;
    z[20]=z[25]*z[20];
    z[21]=static_cast<T>(1)+ z[50];
    z[21]=z[11]*z[21]*z[36];
    z[20]=z[21] + z[20];
    z[21]=n<T>(1,2)*z[5];
    z[20]=z[20]*z[21];
    z[22]=n<T>(1,3)*z[7];
    z[23]=z[29]*z[7];
    z[24]= - static_cast<T>(5)- 7*z[23];
    z[24]=z[24]*z[22];
    z[24]= - n<T>(7,4)*z[15] + z[24];
    z[24]=z[24]*z[22];
    z[25]=n<T>(7,3)*z[15];
    z[26]=n<T>(1,2)*z[16] - z[25];
    z[28]=static_cast<T>(1)+ z[44];
    z[28]=z[13]*z[28];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=z[13]*z[28];
    z[26]= - n<T>(19,36)*z[11] + n<T>(1,4)*z[26] + z[28];
    z[26]=z[11]*z[26];
    z[20]=z[20] + z[26] - n<T>(3,2)*z[36] + z[24];
    z[20]=z[20]*z[21];
    z[21]=n<T>(353,18) + 15*z[12];
    z[21]=n<T>(1,4)*z[21] + z[16];
    z[21]=n<T>(1,2)*z[21] - z[25];
    z[18]=z[38]*z[18];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[13]*z[18];
    z[18]=n<T>(1,4)*z[21] + z[18];
    z[21]= - static_cast<T>(71)- n<T>(149,6)*z[3];
    z[21]=z[3]*z[21];
    z[21]=n<T>(125,2) + z[21];
    z[21]=z[11]*z[21];
    z[18]=n<T>(1,576)*z[21] + n<T>(1,2)*z[18] + z[30];
    z[18]=z[11]*z[18];
    z[21]=static_cast<T>(23)+ 79*z[8];
    z[21]=n<T>(1,8)*z[21] + 7*z[47];
    z[21]=z[21]*z[22];
    z[22]=n<T>(11,12) - 7*z[15];
    z[21]=n<T>(1,2)*z[22] + z[21];
    z[21]=z[21]*z[34];
    z[18]=z[19] + z[27] + z[20] + z[21] + z[18];
    z[18]=z[9]*z[18];
    z[19]=static_cast<T>(2)- z[1];
    z[19]=n<T>(1,3)*z[19] + z[32];
    z[20]=2*z[3];
    z[19]=z[19]*z[20];
    z[21]=z[15] - 2;
    z[21]=z[21]*z[15];
    z[19]=z[19] + static_cast<T>(1)+ z[21];
    z[19]=z[3]*z[19];
    z[22]=z[15] - 1;
    z[22]=z[22]*z[15];
    z[19]=z[22] + z[19];
    z[19]=z[3]*z[19];
    z[24]= - z[10]*z[29];
    z[25]=z[1] - z[49];
    z[25]=z[7]*z[25];
    z[24]=n<T>(37,18)*z[25] - n<T>(113,72) + z[24];
    z[24]=z[24]*z[40];
    z[25]= - n<T>(17,4) + 19*z[15];
    z[25]=z[25]*z[15];
    z[26]= - n<T>(3,8)*z[10] - n<T>(1,9)*z[25];
    z[19]=z[24] + n<T>(1,8)*z[26] + n<T>(1,9)*z[19];
    z[19]=z[7]*z[19];
    z[20]=z[32]*z[20];
    z[20]=z[21] + z[20];
    z[20]=z[3]*z[20];
    z[20]=z[22] + z[20];
    z[20]=z[20]*z[31];
    z[21]=static_cast<T>(1)+ n<T>(2,3)*z[3];
    z[21]=z[3]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[3]*z[15]*z[21];
    z[21]= - n<T>(19,8)*z[15] + z[21];
    z[21]=z[21]*z[42];
    z[20]=z[21] + z[20] - z[12] - n<T>(1,24)*z[25];
    z[20]=z[20]*z[42];
    z[21]=npow(z[3],3);
    z[21]=z[12] + n<T>(2,9)*z[21];
    z[22]= - n<T>(3,8) - z[23];
    z[22]=z[5]*npow(z[7],2)*z[10]*z[22];

    r += z[17] + z[18] + z[19] + z[20] + n<T>(1,3)*z[21] + n<T>(1,8)*z[22];
 
    return r;
}

template double qg_2lha_r1442(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1442(const std::array<dd_real,30>&);
#endif
