#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r584(const std::array<T,30>& k) {
  T z[10];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=n<T>(1,4)*z[4];
    z[6]= - static_cast<T>(3)+ z[1];
    z[6]=z[4]*z[6];
    z[6]=z[6] + static_cast<T>(3)+ z[1];
    z[6]=z[6]*z[5];
    z[7]=z[4] - 1;
    z[8]=z[1] - 1;
    z[9]= - z[3]*z[7]*z[8]*z[4];
    z[6]=z[6] + z[9];
    z[6]=z[3]*z[6];
    z[5]= - z[7]*z[5];
    z[7]= - n<T>(1,2) + z[3];
    z[7]=z[3]*z[7];
    z[7]= - n<T>(1,2) + z[7];
    z[9]=n<T>(1,2)*z[2];
    z[7]=z[9]*z[7]*npow(z[4],2);
    z[5]=z[7] + z[5] + z[6];
    z[5]=z[9]*z[3]*z[5];
    z[6]= - static_cast<T>(1)+ n<T>(1,2)*z[4];
    z[6]=z[6]*z[4];
    z[6]=z[6] + n<T>(1,2);
    z[7]=z[8]*z[6];
    z[8]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[8]=z[8]*z[1];
    z[8]=z[8] + n<T>(1,2);
    z[6]=z[3]*z[8]*z[6];
    z[6]=n<T>(1,2)*z[7] + z[6];
    z[6]=z[6]*npow(z[3],2);
    z[5]=z[6] + z[5];

    r += z[5]*z[2];
 
    return r;
}

template double qg_2lha_r584(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r584(const std::array<dd_real,30>&);
#endif
