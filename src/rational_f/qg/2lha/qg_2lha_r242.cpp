#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r242(const std::array<T,30>& k) {
  T z[49];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[3];
    z[6]=k[20];
    z[7]=k[6];
    z[8]=k[11];
    z[9]=k[14];
    z[10]=k[9];
    z[11]=k[4];
    z[12]=npow(z[4],2);
    z[13]=z[12]*z[2];
    z[14]=n<T>(3,2)*z[13];
    z[15]= - n<T>(9,4) - n<T>(17,3)*z[4];
    z[15]=z[4]*z[15];
    z[15]=z[15] + z[14];
    z[15]=z[2]*z[15];
    z[16]=n<T>(3,2) + n<T>(5,3)*z[4];
    z[16]=z[4]*z[16];
    z[16]=n<T>(1,6) + z[16];
    z[15]=n<T>(5,2)*z[16] + z[15];
    z[16]=npow(z[2],2);
    z[15]=z[15]*z[16];
    z[17]=n<T>(13,6)*z[4];
    z[18]=static_cast<T>(1)+ z[17];
    z[18]=z[4]*z[18];
    z[18]=z[18] - n<T>(13,12)*z[13];
    z[18]=z[2]*z[18];
    z[19]=n<T>(13,12)*z[4];
    z[20]= - static_cast<T>(1)- z[19];
    z[20]=z[4]*z[20];
    z[18]=z[18] + n<T>(1,12) + z[20];
    z[20]=npow(z[2],3);
    z[18]=z[3]*z[18]*z[20];
    z[15]=z[15] + z[18];
    z[15]=z[3]*z[15];
    z[18]=n<T>(1,3)*z[4];
    z[21]=5*z[4];
    z[22]= - n<T>(53,2) - z[21];
    z[22]=z[22]*z[18];
    z[14]=z[22] + z[14];
    z[22]=n<T>(1,2)*z[2];
    z[14]=z[14]*z[22];
    z[23]=n<T>(7,6) - z[4];
    z[23]=z[4]*z[23];
    z[14]=z[14] + n<T>(23,12) + z[23];
    z[14]=z[2]*z[14];
    z[23]=z[4] + n<T>(1,2);
    z[24]=z[4] + n<T>(3,2);
    z[25]= - z[10]*z[24];
    z[14]=z[25] + z[15] + z[14] + z[23];
    z[15]=3*z[4];
    z[25]=z[23]*z[15];
    z[25]=z[25] - n<T>(5,4)*z[13];
    z[25]=z[25]*z[22];
    z[26]= - static_cast<T>(1)- n<T>(7,8)*z[4];
    z[26]=z[4]*z[26];
    z[25]=z[25] - n<T>(1,8) + z[26];
    z[25]=z[25]*z[20];
    z[26]=z[22]*z[12];
    z[27]=z[4] + 1;
    z[28]=z[27]*z[4];
    z[29]=z[26] - z[28];
    z[29]=z[29]*z[2];
    z[30]=n<T>(1,2)*z[4];
    z[31]=z[30] + 1;
    z[31]=z[31]*z[4];
    z[31]=z[31] + n<T>(1,2);
    z[29]=z[29] + z[31];
    z[29]=z[29]*npow(z[2],4);
    z[32]=z[3]*z[29];
    z[25]=z[25] + n<T>(1,8)*z[32];
    z[25]=z[3]*z[25];
    z[32]=z[4] + 3;
    z[32]=z[32]*z[4];
    z[33]= - z[32] + z[13];
    z[33]=z[33]*z[22];
    z[33]=z[33] + z[27];
    z[20]=z[33]*z[20];
    z[33]=n<T>(1,2)*z[3];
    z[29]=z[33]*z[29];
    z[20]=z[20] + z[29];
    z[20]=z[8]*z[20];
    z[29]=static_cast<T>(17)+ 9*z[4];
    z[29]=z[4]*z[29];
    z[29]=z[29] - n<T>(15,2)*z[13];
    z[29]=z[29]*z[22];
    z[29]=z[29] - static_cast<T>(3)- n<T>(11,2)*z[4];
    z[29]=z[29]*z[16];
    z[20]=z[20] + n<T>(1,4)*z[29] + z[25];
    z[20]=z[8]*z[20];
    z[14]=n<T>(1,4)*z[14] + z[20];
    z[14]=z[8]*z[14];
    z[20]=n<T>(1,3)*z[3];
    z[25]=n<T>(1,4)*z[3];
    z[29]= - static_cast<T>(1)+ z[25];
    z[29]=z[3]*z[29];
    z[29]=n<T>(5,4) + z[29];
    z[29]=z[29]*z[20];
    z[34]= - n<T>(7,2) + z[3];
    z[20]=z[34]*z[20];
    z[34]=z[33]*z[9];
    z[20]=z[20] + z[34];
    z[35]=n<T>(1,2)*z[9];
    z[20]=z[20]*z[35];
    z[20]=z[29] + z[20];
    z[20]=z[9]*z[20];
    z[29]=z[33] - 1;
    z[36]= - z[3]*z[29];
    z[36]= - n<T>(1,2) + z[36];
    z[36]=z[3]*z[36];
    z[20]=n<T>(1,6)*z[36] + z[20];
    z[20]=z[9]*z[20];
    z[36]=n<T>(1,12) - z[10];
    z[36]=z[10]*z[36];
    z[37]=n<T>(1,4) + z[10];
    z[37]=z[8]*z[37];
    z[36]=n<T>(1,2)*z[36] + z[37];
    z[36]=z[8]*z[36];
    z[37]=n<T>(1,8)*z[10];
    z[38]=static_cast<T>(7)+ 5*z[10];
    z[38]=z[38]*z[37];
    z[39]= - static_cast<T>(1)+ z[10];
    z[39]=z[8]*z[39];
    z[38]=z[38] + z[39];
    z[39]=npow(z[8],2);
    z[38]=z[38]*z[39];
    z[40]=z[5]*npow(z[8],3);
    z[41]= - z[10]*z[40];
    z[38]=z[38] + z[41];
    z[38]=z[5]*z[38];
    z[36]=n<T>(1,2)*z[36] + z[38];
    z[36]=z[5]*z[36];
    z[38]=npow(z[10],2);
    z[41]=z[8]*z[10];
    z[42]= - n<T>(1,16)*z[38] + z[41];
    z[42]=z[42]*z[39];
    z[38]=z[38]*z[40];
    z[38]=z[42] + n<T>(1,4)*z[38];
    z[38]=z[5]*z[38];
    z[37]= - z[39]*z[37];
    z[37]=z[37] + z[38];
    z[37]=z[5]*z[37];
    z[37]= - n<T>(1,3)*z[41] + z[37];
    z[37]=z[11]*z[37];
    z[38]= - n<T>(7,3) + z[9];
    z[38]=z[9]*z[38];
    z[38]=n<T>(5,3) + z[38];
    z[38]=z[9]*z[38];
    z[38]= - n<T>(1,3) + z[38];
    z[38]=z[9]*z[38];
    z[39]= - static_cast<T>(1)+ n<T>(1,3)*z[9];
    z[39]=z[9]*z[39];
    z[39]=static_cast<T>(1)+ z[39];
    z[39]=z[9]*z[39];
    z[39]= - n<T>(1,3) + z[39];
    z[39]=z[10]*z[39];
    z[38]=z[38] + z[39];
    z[38]= - n<T>(1,3) + n<T>(1,4)*z[38];
    z[38]=z[10]*z[38];
    z[39]=static_cast<T>(1)- n<T>(11,8)*z[10];
    z[39]=z[8]*z[39];
    z[20]=z[37] + z[36] + n<T>(1,3)*z[39] + z[20] + z[38];
    z[20]=z[11]*z[20];
    z[36]=n<T>(13,2)*z[4];
    z[37]= - static_cast<T>(7)- z[36];
    z[37]=z[37]*z[30];
    z[38]=n<T>(1,4)*z[4];
    z[39]=static_cast<T>(25)+ 17*z[4];
    z[39]=z[39]*z[38];
    z[39]=z[39] - z[13];
    z[39]=z[39]*z[22];
    z[37]=z[39] + static_cast<T>(5)+ z[37];
    z[39]=n<T>(1,3)*z[2];
    z[37]=z[37]*z[39];
    z[40]=static_cast<T>(5)+ n<T>(13,3)*z[4];
    z[40]=z[40]*z[38];
    z[40]= - n<T>(7,3) + z[40];
    z[37]=n<T>(1,2)*z[40] + z[37];
    z[40]=n<T>(1,6)*z[4];
    z[42]= - static_cast<T>(19)- 11*z[4];
    z[42]=z[42]*z[40];
    z[43]=n<T>(1,4) - 7*z[4];
    z[43]=z[4]*z[43];
    z[43]=z[43] + n<T>(5,2)*z[13];
    z[43]=z[2]*z[43];
    z[44]=z[4] - 1;
    z[44]=z[44]*z[4];
    z[43]=z[44] + z[43];
    z[43]=z[43]*z[39];
    z[45]=n<T>(41,12) + z[15];
    z[45]=z[4]*z[45];
    z[43]=z[43] - n<T>(1,3) + z[45];
    z[43]=z[2]*z[43];
    z[42]=z[43] - static_cast<T>(1)+ z[42];
    z[43]=static_cast<T>(1)+ n<T>(3,2)*z[4];
    z[43]=z[4]*z[43];
    z[43]=z[43] - z[26];
    z[43]=z[43]*z[22];
    z[45]=n<T>(3,4)*z[4];
    z[46]= - static_cast<T>(1)- z[45];
    z[46]=z[4]*z[46];
    z[43]=z[43] - n<T>(1,4) + z[46];
    z[43]=z[2]*z[43];
    z[43]=n<T>(1,2)*z[31] + z[43];
    z[43]=z[3]*z[43];
    z[42]=n<T>(1,4)*z[42] + z[43];
    z[42]=z[3]*z[42];
    z[37]=n<T>(1,2)*z[37] + z[42];
    z[37]=z[3]*z[37];
    z[39]=z[39]*z[12];
    z[42]= - static_cast<T>(1)- n<T>(21,16)*z[4];
    z[42]=z[4]*z[42];
    z[42]=z[42] + z[39];
    z[42]=z[2]*z[42];
    z[43]=13*z[4];
    z[46]=static_cast<T>(21)+ z[43];
    z[46]=z[4]*z[46];
    z[42]=z[42] + static_cast<T>(1)+ n<T>(1,8)*z[46];
    z[42]=z[2]*z[42];
    z[46]=z[28] - z[39];
    z[46]=z[46]*z[22];
    z[46]=z[46] - z[31];
    z[46]=z[46]*z[2];
    z[47]=z[18] + 1;
    z[47]=z[47]*z[4];
    z[47]=z[47] + 1;
    z[46]=z[46] + n<T>(1,2)*z[47];
    z[47]=z[46]*z[33];
    z[48]= - static_cast<T>(13)- n<T>(31,6)*z[4];
    z[48]=z[4]*z[48];
    z[48]= - n<T>(21,2) + z[48];
    z[42]=z[47] + n<T>(1,8)*z[48] + z[42];
    z[42]=z[3]*z[42];
    z[47]=n<T>(37,3) + n<T>(19,2)*z[4];
    z[47]=z[47]*z[30];
    z[45]=z[45] + n<T>(1,3);
    z[45]=z[45]*z[2]*z[4];
    z[47]=z[47] + n<T>(5,3) - n<T>(5,2)*z[45];
    z[47]=z[47]*z[2];
    z[48]=n<T>(19,3) + n<T>(23,8)*z[4];
    z[48]=z[48]*z[4];
    z[47]=z[47] - z[48] - n<T>(103,24);
    z[47]=n<T>(1,2)*z[47];
    z[42]= - z[47] + z[42];
    z[42]=z[3]*z[42];
    z[36]=z[36] + 5;
    z[36]=z[36]*z[38];
    z[36]=z[36] - z[39];
    z[36]=z[36]*z[2];
    z[48]=z[15] + 5;
    z[48]=z[48]*z[30];
    z[48]=z[48] + 1;
    z[36]= - z[36] + n<T>(3,2)*z[48];
    z[36]=z[36]*z[2];
    z[48]=static_cast<T>(5)+ n<T>(23,12)*z[4];
    z[48]=z[48]*z[4];
    z[48]=z[48] + n<T>(17,4);
    z[36]=z[36] - n<T>(1,2)*z[48];
    z[48]=z[3]*z[46];
    z[48]=z[48] + z[36];
    z[48]=z[3]*z[48];
    z[27]=z[15]*z[27];
    z[27]=z[27] - z[13];
    z[27]=z[27]*z[22];
    z[27]=z[27] - 3*z[31];
    z[27]=z[27]*z[2];
    z[31]=z[32] + 3;
    z[27]=z[27] + n<T>(1,2)*z[31];
    z[31]=z[27]*z[34];
    z[31]=z[48] + z[31];
    z[31]=z[9]*z[31];
    z[31]=z[42] + z[31];
    z[31]=z[9]*z[31];
    z[32]= - n<T>(7,3) - n<T>(15,8)*z[4];
    z[32]=z[4]*z[32];
    z[32]=n<T>(5,4)*z[45] - n<T>(7,12) + z[32];
    z[32]=z[2]*z[32];
    z[34]= - n<T>(3,4) - z[4];
    z[34]=z[4]*z[34];
    z[34]=z[34] + z[39];
    z[34]=z[2]*z[34];
    z[42]=z[24]*z[4];
    z[42]=z[42] + n<T>(1,2);
    z[34]=z[34] + z[42];
    z[34]=z[2]*z[34];
    z[45]= - n<T>(3,4) - z[18];
    z[45]=z[4]*z[45];
    z[34]=z[34] - n<T>(1,2) + z[45];
    z[34]=z[3]*z[34];
    z[45]=n<T>(23,3) + n<T>(15,4)*z[4];
    z[45]=z[4]*z[45];
    z[45]=n<T>(55,12) + z[45];
    z[32]=z[34] + n<T>(1,4)*z[45] + z[32];
    z[32]=z[3]*z[32];
    z[34]=z[4] + n<T>(43,48);
    z[34]=z[34]*z[4];
    z[34]=z[34] - n<T>(1,4);
    z[34]=z[34]*z[2];
    z[45]=z[4] + n<T>(85,48);
    z[45]=z[45]*z[4];
    z[34]=z[34] - z[45] - n<T>(41,48);
    z[32]=z[32] + z[34];
    z[32]=z[3]*z[32];
    z[31]=z[32] + z[31];
    z[31]=z[9]*z[31];
    z[32]=static_cast<T>(9)+ z[21];
    z[32]=z[32]*z[38];
    z[45]= - static_cast<T>(1)- n<T>(5,4)*z[4];
    z[45]=z[4]*z[45]*z[22];
    z[32]=z[45] + static_cast<T>(1)+ z[32];
    z[32]=z[2]*z[32];
    z[45]=z[9]*z[46];
    z[46]= - static_cast<T>(7)- n<T>(5,2)*z[4];
    z[46]=z[4]*z[46];
    z[46]= - n<T>(13,2) + z[46];
    z[32]=z[45] + n<T>(1,4)*z[46] + z[32];
    z[32]=z[32]*z[35];
    z[45]=n<T>(5,2) + z[4];
    z[45]=z[45]*z[30];
    z[22]= - z[42]*z[22];
    z[22]=z[32] + z[22] + static_cast<T>(1)+ z[45];
    z[22]=z[9]*z[22];
    z[17]= - static_cast<T>(5)- z[17];
    z[17]=z[4]*z[17];
    z[17]= - static_cast<T>(5)+ z[17];
    z[17]=n<T>(1,8)*z[17] + z[22];
    z[17]=z[10]*z[17];
    z[22]=z[27]*z[35];
    z[22]=z[22] + z[36];
    z[22]=z[9]*z[22];
    z[22]= - z[47] + z[22];
    z[22]=z[9]*z[22];
    z[22]=z[22] + z[34];
    z[22]=z[9]*z[22];
    z[27]=static_cast<T>(5)+ z[43];
    z[27]=z[4]*z[27];
    z[27]= - static_cast<T>(5)+ z[27];
    z[17]=z[17] + n<T>(1,48)*z[27] + z[22];
    z[17]=z[10]*z[17];
    z[22]=z[44] + 1;
    z[27]=z[4] - z[13];
    z[27]=z[2]*z[27];
    z[24]=n<T>(3,2)*z[10] + z[24];
    z[24]=z[10]*z[24];
    z[32]=z[10] - n<T>(5,2)*z[41];
    z[32]=z[5]*z[32];
    z[24]=z[32] + z[24] + n<T>(5,6)*z[27] + z[22];
    z[15]=z[15] - z[13];
    z[15]=z[2]*z[15];
    z[15]= - static_cast<T>(3)+ z[15];
    z[15]=z[2]*z[15];
    z[15]=n<T>(5,2)*z[10] - n<T>(5,2) + z[15];
    z[27]=n<T>(1,4)*z[13];
    z[32]= - z[4] + z[27];
    z[32]=z[2]*z[32];
    z[32]=n<T>(3,2) + z[32];
    z[16]=z[8]*z[32]*z[16];
    z[15]=n<T>(1,4)*z[15] + z[16];
    z[15]=z[8]*z[15];
    z[15]=z[15] + n<T>(1,4)*z[24];
    z[15]=z[8]*z[15];
    z[15]=z[39] + z[15];
    z[15]=z[5]*z[15];
    z[16]=n<T>(19,2) - z[21];
    z[16]=z[16]*z[18];
    z[16]=z[16] + z[13];
    z[16]=z[2]*z[16];
    z[21]= - n<T>(11,6) + z[4];
    z[21]=z[4]*z[21];
    z[16]=z[16] + n<T>(13,3) + z[21];
    z[14]=z[20] + z[15] + z[14] + z[17] + z[31] + n<T>(1,4)*z[16] + z[37];
    z[15]= - static_cast<T>(1)+ n<T>(5,12)*z[4];
    z[15]=z[4]*z[15];
    z[15]=z[15] + n<T>(1,12)*z[13];
    z[15]=z[2]*z[15];
    z[16]=static_cast<T>(1)- z[19];
    z[16]=z[4]*z[16];
    z[15]=z[16] + z[15];
    z[15]=z[2]*z[15];
    z[15]=n<T>(7,12)*z[12] + z[15];
    z[16]= - z[4]*z[23];
    z[16]=z[16] + z[39];
    z[16]=z[2]*z[16];
    z[16]=z[28] + z[16];
    z[16]=z[2]*z[16];
    z[17]= - n<T>(1,2) - z[18];
    z[17]=z[4]*z[17];
    z[16]=z[17] + z[16];
    z[16]=z[3]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[3]*z[15];
    z[16]=static_cast<T>(5)+ z[38];
    z[16]=z[16]*z[18];
    z[17]= - static_cast<T>(1)+ z[40];
    z[17]=z[4]*z[17];
    z[17]=z[17] - z[27];
    z[17]=z[2]*z[17];
    z[15]=z[15] + z[16] + z[17];
    z[15]=z[15]*z[25];
    z[16]=z[26] - z[12];
    z[17]=z[2]*z[16];
    z[19]=n<T>(1,2)*z[12];
    z[17]=z[19] + z[17];
    z[20]=z[12] - z[39];
    z[20]=z[2]*z[20];
    z[20]= - z[12] + z[20];
    z[20]=z[2]*z[20];
    z[20]=n<T>(1,3)*z[12] + z[20];
    z[20]=z[20]*z[33];
    z[17]=n<T>(1,3)*z[17] + z[20];
    z[17]=z[3]*z[17];
    z[16]=n<T>(1,3)*z[16] + z[17];
    z[16]=z[3]*z[16];
    z[16]=z[19] + z[16];
    z[16]=z[1]*z[16];
    z[17]= - n<T>(1,4) + z[18];
    z[17]=z[4]*z[17];
    z[18]=z[5]*z[12];
    z[13]=n<T>(1,4)*z[16] - n<T>(1,8)*z[18] + z[15] + z[17] - n<T>(5,24)*z[13];
    z[13]=z[1]*z[13];
    z[15]= - static_cast<T>(1)- z[33];
    z[15]=z[12]*z[15];
    z[16]=z[19]*z[5];
    z[15]= - z[16] + z[15];
    z[15]=z[1]*z[15];
    z[17]=z[3] + static_cast<T>(1)- z[30];
    z[17]=z[4]*z[17];
    z[16]=z[4] + z[16];
    z[16]=z[5]*z[16];
    z[15]=z[15] + z[16] + z[17];
    z[15]=z[1]*z[15];
    z[16]= - z[44] - z[10];
    z[16]=z[8]*z[16];
    z[16]= - z[10] + z[16];
    z[16]=z[5]*z[16];
    z[16]=z[16] + z[10] - z[22];
    z[16]=z[5]*z[16];
    z[16]=z[16] + z[4] - z[3];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[6]*z[15];
    z[12]=z[1]*z[12]*z[33];
    z[16]=static_cast<T>(1)- z[3];
    z[16]=z[4]*z[16];
    z[12]=z[12] + z[16];
    z[12]=z[1]*z[12];
    z[12]=z[12] + n<T>(1,2)*z[5] + z[29];
    z[12]=z[7]*z[12];

    r += n<T>(1,3)*z[12] + z[13] + n<T>(1,2)*z[14] + n<T>(1,4)*z[15];
 
    return r;
}

template double qg_2lha_r242(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r242(const std::array<dd_real,30>&);
#endif
