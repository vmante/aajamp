#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1180(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[27];
    z[7]=k[6];
    z[8]=k[3];
    z[9]=k[9];
    z[10]=k[5];
    z[11]=k[10];
    z[12]=k[11];
    z[13]=k[14];
    z[14]=z[2]*z[5];
    z[15]=static_cast<T>(1)+ z[14];
    z[15]=z[15]*npow(z[2],2);
    z[15]=z[15] + static_cast<T>(1)+ z[10];
    z[16]=n<T>(1,2)*z[8];
    z[17]= - z[16] + n<T>(11,2) - n<T>(241,9)*z[2];
    z[17]=z[8]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[17]=npow(z[8],2);
    z[18]= - static_cast<T>(107)- 49*z[2];
    z[18]=z[9]*z[18]*z[17];
    z[15]=n<T>(1,2)*z[15] + n<T>(1,9)*z[18];
    z[15]=z[9]*z[15];
    z[18]=n<T>(1,2)*z[5];
    z[19]=z[18] + z[3];
    z[20]=n<T>(1,2)*z[2];
    z[21]= - z[5]*z[20];
    z[21]=z[21] - n<T>(143,9) + z[19];
    z[21]=z[21]*z[20];
    z[19]=z[19]*z[20];
    z[22]=z[5] + n<T>(1,2);
    z[22]=n<T>(1,2)*z[22];
    z[19]=z[19] - z[22] - z[3];
    z[19]=z[1]*z[19];
    z[23]=z[1] + 1;
    z[20]=z[20] - 1;
    z[24]=z[23]*z[20];
    z[25]=z[6]*z[24];
    z[26]=n<T>(259,9) - z[18];
    z[15]=n<T>(11,2)*z[25] + z[15] - n<T>(1,4)*z[8] + z[19] + z[21] + n<T>(1,2)*
    z[26] - z[3];
    z[19]=3*z[3];
    z[21]= - z[10] + z[19] + z[18];
    z[25]=z[9]*z[8];
    z[26]=z[23]*z[6];
    z[27]=z[19] - n<T>(223,9) + n<T>(3,2)*z[5];
    z[27]=z[1]*z[27];
    z[27]=n<T>(33,2)*z[26] - n<T>(9,2)*z[25] + z[27] - n<T>(607,9) + z[21];
    z[28]= - z[12] + n<T>(1,4)*z[10];
    z[29]= - n<T>(5,4)*z[13] - n<T>(455,36) - z[28];
    z[29]=z[29]*z[16];
    z[30]=n<T>(223,9) - z[5];
    z[30]=n<T>(1,2)*z[30] - z[3];
    z[30]=z[1]*z[30];
    z[30]=z[30] - z[3];
    z[29]= - n<T>(11,8)*z[26] + z[29] + n<T>(58,9) + n<T>(1,8)*z[10] + n<T>(1,4)*z[30];
    z[29]=z[7]*z[29];
    z[27]=n<T>(1,4)*z[27] + z[29];
    z[27]=z[7]*z[27];
    z[29]=z[2] - 2;
    z[29]=z[29]*z[23];
    z[30]= - static_cast<T>(1)+ 2*z[2];
    z[30]=z[8]*z[30];
    z[29]=z[30] + z[29];
    z[17]=z[2]*z[17];
    z[30]=static_cast<T>(1)+ z[2];
    z[30]=z[9]*z[30]*npow(z[8],3);
    z[17]=z[17] + n<T>(1,3)*z[30];
    z[17]=z[9]*z[17];
    z[30]=z[8] - z[23];
    z[30]=z[7]*z[30];
    z[30]=n<T>(1,3)*z[30] + z[23];
    z[30]=z[7]*z[30];
    z[17]=z[30] + n<T>(1,3)*z[29] + z[17];
    z[17]=z[4]*z[17];
    z[15]=n<T>(29,3)*z[17] + n<T>(1,2)*z[15] + z[27];
    z[15]=z[4]*z[15];
    z[17]=z[5] + 1;
    z[27]=n<T>(1,2)*z[17] + z[19];
    z[29]=n<T>(1,2)*z[1];
    z[27]=z[27]*z[29];
    z[27]=z[27] - n<T>(9,2)*z[13] + n<T>(5,2)*z[3] - n<T>(5,4)*z[10] - n<T>(257,36) + 
    z[12];
    z[30]=static_cast<T>(1)+ n<T>(1,4)*z[13];
    z[31]=3*z[13];
    z[30]=z[30]*z[31];
    z[32]=n<T>(5,2)*z[10] + n<T>(437,18) - 9*z[12];
    z[32]=n<T>(1,4)*z[32] + z[30];
    z[32]=z[8]*z[32];
    z[33]=n<T>(1,2)*z[6];
    z[23]= - z[23]*z[33];
    z[23]=z[23] + n<T>(73,2) + 15*z[1];
    z[34]=n<T>(1,4)*z[6];
    z[23]=z[23]*z[34];
    z[35]= - static_cast<T>(1)+ z[8];
    z[35]=z[9]*z[35];
    z[23]=z[23] + n<T>(9,4)*z[35] + n<T>(1,2)*z[27] + z[32];
    z[23]=z[7]*z[23];
    z[22]= - z[22] - z[19];
    z[22]=z[22]*z[29];
    z[27]= - z[5] + n<T>(7,4)*z[10] + n<T>(811,36) - z[12];
    z[26]=n<T>(3,8)*z[26] - static_cast<T>(19)- n<T>(15,2)*z[1];
    z[26]=z[6]*z[26];
    z[19]=z[23] + z[26] + n<T>(17,4)*z[9] + z[22] + n<T>(19,4)*z[13] + n<T>(1,2)*
    z[27] - z[19];
    z[19]=z[7]*z[19];
    z[18]= - z[18] + z[3];
    z[18]=z[18]*z[29];
    z[22]=static_cast<T>(1)+ n<T>(1,3)*z[2];
    z[22]=z[22]*z[25];
    z[23]=n<T>(107,9) - z[14];
    z[23]=z[2]*z[23];
    z[23]=z[8] + z[23] - static_cast<T>(19)- z[10];
    z[22]=n<T>(1,2)*z[23] + n<T>(49,3)*z[22];
    z[22]=z[9]*z[22];
    z[17]=5*z[3] + z[17];
    z[23]= - z[2]*z[3];
    z[17]=z[22] + z[18] + n<T>(1,2)*z[17] + z[23];
    z[18]=z[24]*z[34];
    z[22]=n<T>(19,2) - z[2];
    z[18]=z[18] + n<T>(3,4)*z[22] + z[1];
    z[18]=z[6]*z[18];
    z[17]=z[19] + n<T>(1,2)*z[17] + z[18];
    z[15]=n<T>(1,2)*z[17] + z[15];
    z[15]=z[4]*z[15];
    z[17]= - n<T>(5,2) - z[13];
    z[17]=z[17]*z[31];
    z[18]=z[1] + 3;
    z[19]= - z[18]*z[33];
    z[22]=z[9] - z[12];
    z[19]=z[19] + n<T>(43,2) - z[22];
    z[19]=z[6]*z[19];
    z[23]=static_cast<T>(3)- z[11];
    z[24]= - n<T>(9,2) - z[13];
    z[24]=z[9]*z[24];
    z[17]=z[19] + z[24] + z[17] + n<T>(1,2)*z[23] + z[12] + z[21];
    z[19]= - n<T>(3,2) - z[13];
    z[19]=z[19]*z[31];
    z[19]=z[19] - z[10] + n<T>(1,4) + 3*z[12];
    z[16]=z[19]*z[16];
    z[19]=n<T>(1,2)*z[9];
    z[21]=static_cast<T>(5)+ n<T>(1,2)*z[13] - n<T>(9,2)*z[8];
    z[21]=z[21]*z[19];
    z[23]=n<T>(1,2)*z[10];
    z[24]=z[23] - z[12];
    z[16]=z[21] + z[16] + z[30] - n<T>(3,4)*z[3] - n<T>(29,8) + z[24];
    z[18]=z[6]*z[18];
    z[18]=n<T>(1,16)*z[18] - static_cast<T>(5)+ n<T>(1,4)*z[22];
    z[18]=z[6]*z[18];
    z[16]=n<T>(1,2)*z[16] + z[18];
    z[16]=z[7]*z[16];
    z[16]=n<T>(1,4)*z[17] + z[16];
    z[16]=z[7]*z[16];
    z[17]=z[23] - static_cast<T>(1)+ z[11];
    z[14]= - n<T>(49,9)*z[9] + n<T>(1,4)*z[14] + n<T>(1,2)*z[17] + z[31];
    z[14]=z[9]*z[14];
    z[17]=z[9]*z[2];
    z[17]= - 3*z[17] + z[6];
    z[17]=z[17]*z[33];
    z[14]=z[17] - n<T>(1,2)*z[3] + z[14];
    z[14]=z[15] + n<T>(1,4)*z[14] + z[16];
    z[14]=z[4]*z[14];
    z[15]=z[2] - 1;
    z[15]=z[15]*z[22];
    z[16]=z[20]*z[33];
    z[15]=z[16] + n<T>(29,4) + z[15];
    z[15]=z[6]*z[15];
    z[16]=n<T>(3,2)*z[13];
    z[17]= - z[19] - z[16];
    z[18]=z[13] + 1;
    z[17]=z[18]*z[17];
    z[18]=3*z[11];
    z[20]= - z[18] + z[24];
    z[16]=z[16] + 1;
    z[21]=z[13]*z[16];
    z[20]=n<T>(1,2)*z[20] + z[21];
    z[20]=z[8]*z[20];
    z[15]=z[15] + z[20] + z[17] - z[28];
    z[15]=z[7]*z[15];
    z[16]=z[19] + z[16];
    z[16]=z[13]*z[16];
    z[17]= - z[18] + z[10];
    z[18]=npow(z[6],2);
    z[15]=z[15] + n<T>(1,2)*z[18] + n<T>(1,4)*z[17] + z[16];
    z[15]=z[7]*z[15];

    r += z[14] + n<T>(1,4)*z[15];
 
    return r;
}

template double qg_2lha_r1180(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1180(const std::array<dd_real,30>&);
#endif
