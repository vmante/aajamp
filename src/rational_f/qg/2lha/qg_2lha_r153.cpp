#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r153(const std::array<T,30>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[6];
    z[5]=k[11];
    z[6]=k[3];
    z[7]=k[5];
    z[8]=k[4];
    z[9]=k[9];
    z[10]=n<T>(1,3)*z[2];
    z[11]=n<T>(1,2)*z[6];
    z[12]=n<T>(1,5)*z[2] - n<T>(1,5) + z[11];
    z[12]=z[12]*z[10];
    z[13]=z[6] - 1;
    z[14]=z[13]*z[6];
    z[12]=n<T>(1,10)*z[14] + z[12];
    z[15]=49*z[5];
    z[16]=npow(z[2],2);
    z[12]=z[12]*z[16]*z[15];
    z[17]=n<T>(1,2)*z[2];
    z[18]= - 463*z[2] + static_cast<T>(463)- 997*z[6];
    z[18]=z[18]*z[17];
    z[19]=static_cast<T>(267)- n<T>(703,3)*z[6];
    z[19]=z[6]*z[19];
    z[18]=z[19] + z[18];
    z[18]=z[2]*z[18];
    z[12]=n<T>(1,20)*z[18] + z[12];
    z[12]=z[5]*z[12];
    z[18]=2923*z[2] - static_cast<T>(2923)+ 4193*z[6];
    z[18]=z[2]*z[18];
    z[14]=113*z[14] + n<T>(1,4)*z[18];
    z[12]=n<T>(1,60)*z[14] + z[12];
    z[12]=z[5]*z[12];
    z[14]= - static_cast<T>(899)+ n<T>(703,2)*z[6];
    z[14]=z[6]*z[14];
    z[18]=n<T>(73,2)*z[2] - static_cast<T>(73)+ n<T>(899,15)*z[6];
    z[18]=z[2]*z[18];
    z[14]=z[18] + n<T>(73,2) + n<T>(1,15)*z[14];
    z[14]=z[14]*z[16];
    z[13]= - z[17] - z[13];
    z[13]=z[2]*z[13];
    z[16]=static_cast<T>(1)- z[11];
    z[16]=z[6]*z[16];
    z[13]=z[13] - n<T>(1,2) + z[16];
    z[13]=z[5]*z[13]*npow(z[2],3);
    z[13]=n<T>(1,4)*z[14] + n<T>(49,15)*z[13];
    z[13]=z[5]*z[13];
    z[14]=static_cast<T>(929)- 113*z[6];
    z[14]=z[14]*z[11];
    z[14]= - static_cast<T>(457)+ z[14];
    z[16]= - n<T>(457,2)*z[2] + static_cast<T>(457)- n<T>(929,4)*z[6];
    z[16]=z[2]*z[16];
    z[14]=n<T>(1,2)*z[14] + z[16];
    z[14]=z[2]*z[14];
    z[13]=n<T>(1,15)*z[14] + z[13];
    z[13]=z[5]*z[13];
    z[14]= - static_cast<T>(929)- n<T>(1441,8)*z[1];
    z[16]=z[6]*z[7];
    z[18]=n<T>(179,2)*z[16];
    z[19]=n<T>(1441,3) - z[18];
    z[19]=z[6]*z[19];
    z[14]=n<T>(929,6)*z[2] + n<T>(1,3)*z[14] + n<T>(1,4)*z[19];
    z[14]=z[2]*z[14];
    z[19]=static_cast<T>(1)- n<T>(1,2)*z[4];
    z[19]=z[1]*z[19];
    z[19]=z[19] - z[6];
    z[19]=static_cast<T>(929)+ n<T>(1441,2)*z[19];
    z[14]=n<T>(1,6)*z[19] + z[14];
    z[13]=n<T>(1,20)*z[14] + z[13];
    z[13]=z[3]*z[13];
    z[12]=z[12] + z[13];
    z[13]=179*z[16];
    z[14]= - static_cast<T>(1)- z[4];
    z[14]=z[13] + n<T>(1441,3)*z[14] - 179*z[7];
    z[14]=z[6]*z[14];
    z[13]= - static_cast<T>(139)- z[13];
    z[13]=z[2]*z[13];
    z[13]=z[14] + z[13];
    z[14]= - static_cast<T>(1)+ n<T>(1441,1024)*z[4];
    z[13]=n<T>(1,3)*z[14] + n<T>(1,1024)*z[13];
    z[12]=n<T>(1,5)*z[13] + n<T>(1,32)*z[12];
    z[12]=z[3]*z[12];
    z[13]=npow(z[6],2);
    z[14]= - static_cast<T>(49)- n<T>(409,4)*z[8];
    z[14]=z[9]*z[14];
    z[14]=n<T>(703,4) + z[14];
    z[14]=z[14]*z[13];
    z[10]= - z[6] - z[10];
    z[10]=z[2]*z[10];
    z[10]= - z[13] + z[10];
    z[10]=z[2]*z[10];
    z[13]=n<T>(1,3)*z[9];
    z[16]= - z[8]*npow(z[6],3)*z[13];
    z[10]=z[16] + z[10];
    z[10]=z[10]*z[15];
    z[15]=z[6] + z[17];
    z[15]=z[2]*z[15];
    z[10]=z[10] + n<T>(1,3)*z[14] + 49*z[15];
    z[10]=z[5]*z[10];
    z[14]= - static_cast<T>(409)+ n<T>(281,2)*z[8];
    z[13]=z[14]*z[13];
    z[13]= - n<T>(159,2) + z[13];
    z[11]=z[13]*z[11];
    z[11]=z[11] - n<T>(49,3)*z[2];
    z[10]=n<T>(1,2)*z[11] + z[10];
    z[10]=z[5]*z[10];
    z[11]=z[18] + n<T>(281,3)*z[9] - n<T>(179,2)*z[7];
    z[10]=n<T>(1,8)*z[11] + z[10];
    z[10]=n<T>(1,320)*z[10] + z[12];

    r += n<T>(1,2)*z[10];
 
    return r;
}

template double qg_2lha_r153(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r153(const std::array<dd_real,30>&);
#endif
