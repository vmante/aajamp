#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r615(const std::array<T,30>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[27];
    z[7]=k[12];
    z[8]=k[28];
    z[9]=k[13];
    z[10]=k[9];
    z[11]=k[3];
    z[12]=z[10] - 3;
    z[13]=z[2] - 3;
    z[13]=z[13]*z[2];
    z[13]=z[13] + 3;
    z[14]=z[13]*z[2];
    z[14]=z[14] - 1;
    z[15]=n<T>(1,2)*z[10];
    z[14]=z[15]*z[14]*z[12];
    z[16]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[13]= - z[2]*z[16]*z[13];
    z[13]=z[14] + z[13] + z[16];
    z[14]= - z[15] + 1;
    z[14]=z[10]*z[14];
    z[14]= - n<T>(1,2) + z[14];
    z[16]=n<T>(1,2)*z[2];
    z[17]=z[16] - 1;
    z[17]=z[17]*z[2];
    z[17]=z[17] + n<T>(1,2);
    z[14]=z[17]*z[14];
    z[17]=z[10] - 1;
    z[18]=z[2] - 1;
    z[17]=z[10]*z[18]*z[17];
    z[19]=npow(z[10],2);
    z[20]=z[19]*z[5];
    z[17]=3*z[17] - z[20];
    z[17]=z[5]*z[17];
    z[14]=3*z[14] + n<T>(1,4)*z[17];
    z[14]=z[5]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[13]=z[3]*z[13];
    z[14]=3*z[4];
    z[17]= - static_cast<T>(13)- z[14];
    z[17]=z[1]*z[17];
    z[17]=z[17] + static_cast<T>(17)+ z[14];
    z[21]=z[1] - 1;
    z[22]=n<T>(1,4)*z[4];
    z[21]=z[22]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[2]*z[21];
    z[17]=n<T>(1,4)*z[17] + z[21];
    z[17]=z[2]*z[17];
    z[21]=n<T>(3,2)*z[4];
    z[23]=static_cast<T>(13)+ z[21];
    z[23]=z[1]*z[23];
    z[23]=z[23] - static_cast<T>(23)- z[21];
    z[17]=n<T>(1,2)*z[23] + z[17];
    z[17]=z[2]*z[17];
    z[23]= - static_cast<T>(13)- z[4];
    z[23]=z[1]*z[23];
    z[23]=z[23] + static_cast<T>(25)+ z[4];
    z[17]=n<T>(1,4)*z[23] + z[17];
    z[14]= - static_cast<T>(55)- z[14];
    z[23]=static_cast<T>(1)+ z[22];
    z[23]=z[2]*z[23];
    z[14]=n<T>(1,4)*z[14] + z[23];
    z[14]=z[2]*z[14];
    z[21]=static_cast<T>(49)+ z[21];
    z[14]=n<T>(1,2)*z[21] + z[14];
    z[14]=z[2]*z[14];
    z[21]= - static_cast<T>(47)- z[4];
    z[14]=n<T>(1,4)*z[21] + z[14];
    z[21]=static_cast<T>(5)- z[16];
    z[21]=z[2]*z[21];
    z[21]= - n<T>(17,2) + z[21];
    z[21]=z[21]*z[16];
    z[21]=static_cast<T>(2)+ z[21];
    z[21]=z[10]*z[21];
    z[14]=n<T>(1,2)*z[14] + z[21];
    z[14]=z[10]*z[14];
    z[21]= - n<T>(21,4) + z[2];
    z[23]=static_cast<T>(4)- n<T>(9,4)*z[2];
    z[23]=z[10]*z[23];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[21]=z[10]*z[21];
    z[23]=z[1] + 1;
    z[24]=z[6]*z[23];
    z[24]=z[19] + z[24];
    z[24]=z[5]*z[24];
    z[21]=z[24] + z[21] - z[23];
    z[21]=z[5]*z[21];
    z[24]= - static_cast<T>(13)+ 3*z[2];
    z[24]=z[24]*z[16];
    z[24]=static_cast<T>(5)+ z[24];
    z[24]=z[10]*z[24];
    z[25]=n<T>(75,8) - z[2];
    z[25]=z[2]*z[25];
    z[24]=z[24] - n<T>(67,8) + z[25];
    z[24]=z[10]*z[24];
    z[25]= - z[16] - n<T>(11,4) - z[1];
    z[25]=z[2]*z[25];
    z[21]=z[21] + z[24] + z[25] + n<T>(13,4) + z[1];
    z[21]=z[5]*z[21];
    z[13]=3*z[13] + z[21] + n<T>(1,2)*z[17] + z[14];
    z[13]=z[3]*z[13];
    z[14]=z[2] + 3;
    z[17]= - static_cast<T>(13)+ z[2];
    z[17]=z[10]*z[17];
    z[17]=z[17] + z[14];
    z[17]=z[17]*z[15];
    z[21]= - z[7] - z[6];
    z[21]=z[1]*z[21];
    z[17]=z[17] + z[21] - z[7] + 5*z[6];
    z[21]=npow(z[6],2);
    z[23]= - z[21]*z[23];
    z[19]=z[23] - n<T>(1,4)*z[19];
    z[19]=z[5]*z[19];
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[17]=z[5]*z[17];
    z[19]= - z[2]*z[14];
    z[23]=static_cast<T>(7)+ z[16];
    z[23]=z[2]*z[23];
    z[23]= - n<T>(25,2) + z[23];
    z[23]=z[10]*z[23];
    z[19]=z[23] + n<T>(31,2) + z[19];
    z[19]=z[10]*z[19];
    z[23]= - n<T>(9,2) - z[2];
    z[19]=n<T>(3,2)*z[23] + z[19];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[5]*z[17];
    z[19]=n<T>(7,8)*z[1];
    z[17]=z[17] - static_cast<T>(1)+ z[19];
    z[14]=z[4] + z[14];
    z[14]=z[14]*z[16];
    z[14]=z[14] - n<T>(35,2) - z[4];
    z[14]=z[2]*z[14];
    z[23]= - static_cast<T>(1)- z[2];
    z[23]=z[2]*z[23];
    z[23]=static_cast<T>(15)+ z[23];
    z[23]=z[2]*z[23];
    z[23]= - static_cast<T>(13)+ z[23];
    z[23]=z[23]*z[15];
    z[24]=static_cast<T>(31)+ z[4];
    z[14]=z[23] + n<T>(1,2)*z[24] + z[14];
    z[23]=n<T>(1,4)*z[10];
    z[14]=z[14]*z[23];
    z[22]= - z[2]*z[22];
    z[22]=z[22] + static_cast<T>(3)+ n<T>(1,2)*z[4];
    z[22]=z[22]*z[16];
    z[22]=z[22] - n<T>(7,16)*z[1] - static_cast<T>(1)- n<T>(1,8)*z[4];
    z[22]=z[2]*z[22];
    z[13]=z[13] + z[14] + z[22] + n<T>(1,2)*z[17];
    z[13]=z[3]*z[13];
    z[14]=npow(z[2],2);
    z[17]=static_cast<T>(3)+ z[14];
    z[17]=z[17]*z[15];
    z[17]=z[17] - z[18];
    z[17]=z[17]*z[15];
    z[18]= - n<T>(11,2) + z[7];
    z[22]=n<T>(5,4)*z[7] - 3*z[6];
    z[22]=z[1]*z[22];
    z[17]=z[17] + n<T>(1,4)*z[18] + z[22];
    z[18]=z[10]*z[12];
    z[18]=z[20] + z[18];
    z[22]=npow(z[7],2);
    z[24]= - n<T>(3,8)*z[22] + z[21];
    z[24]=z[1]*z[24];
    z[22]= - n<T>(3,4)*z[22] + z[6];
    z[18]=n<T>(1,2)*z[22] + z[24] - n<T>(1,8)*z[18];
    z[18]=z[5]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[17]=z[5]*z[17];
    z[18]=static_cast<T>(3)- z[14];
    z[18]=z[18]*z[15];
    z[14]=z[18] - static_cast<T>(1)+ n<T>(1,2)*z[14];
    z[14]=z[10]*z[14];
    z[18]=z[2] + z[1];
    z[18]= - static_cast<T>(9)+ 7*z[18];
    z[14]=n<T>(1,4)*z[18] + z[14];
    z[13]=z[13] + n<T>(1,4)*z[14] + z[17];
    z[13]=z[3]*z[13];
    z[12]= - z[23]*z[12];
    z[12]= - n<T>(1,4)*z[20] + z[12];
    z[12]=z[9]*z[12];
    z[14]=z[9]*z[8];
    z[17]=z[7]*z[8];
    z[14]=z[14] - z[17];
    z[18]=npow(z[8],2);
    z[17]=z[18] + z[17];
    z[17]=z[7]*z[17];
    z[18]= - z[9]*z[18];
    z[17]=z[18] + z[17];
    z[17]=z[17]*z[19];
    z[12]=z[17] + n<T>(7,8)*z[14] - z[21] + z[12];
    z[12]=z[5]*z[12];
    z[14]=n<T>(3,2)*z[14] - z[21];
    z[14]=z[1]*z[14];
    z[16]=z[10]*z[16];
    z[16]=z[9] + z[16];
    z[15]=z[16]*z[15];
    z[16]=static_cast<T>(1)+ z[7];
    z[16]=z[7]*z[16];
    z[16]= - 5*z[9] + n<T>(1,2)*z[16];
    z[17]=z[6]*z[11];
    z[18]=n<T>(3,2) + z[17];
    z[18]=z[6]*z[18];
    z[12]=z[12] + z[15] + z[14] + n<T>(1,4)*z[16] + z[18];
    z[12]=z[5]*z[12];
    z[14]= - n<T>(1,2)*z[9] + z[6];
    z[14]=z[1]*z[14];
    z[14]=z[17] - z[14];
    z[15]=z[9]*z[11];
    z[15]= - n<T>(7,2) + 3*z[15];
    z[14]=n<T>(1,2)*z[15] - 3*z[14];
    z[12]=n<T>(1,2)*z[14] + z[12];

    r += n<T>(1,2)*z[12] + z[13];
 
    return r;
}

template double qg_2lha_r615(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r615(const std::array<dd_real,30>&);
#endif
