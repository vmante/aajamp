#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1125(const std::array<T,30>& k) {
  T z[42];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[6];
    z[5]=k[2];
    z[6]=k[7];
    z[7]=k[11];
    z[8]=k[3];
    z[9]=k[9];
    z[10]=k[10];
    z[11]=k[5];
    z[12]=k[14];
    z[13]=n<T>(1,32)*z[7];
    z[14]= - static_cast<T>(259)- n<T>(209,2)*z[7];
    z[14]=z[14]*z[13];
    z[15]=n<T>(1,2)*z[8];
    z[16]=npow(z[9],2);
    z[17]=z[15]*z[16];
    z[18]= - n<T>(1,2) + z[9];
    z[18]=z[9]*z[18];
    z[18]=z[18] - z[17];
    z[18]=z[8]*z[18];
    z[19]=static_cast<T>(1)+ n<T>(1,2)*z[10];
    z[20]=z[19]*z[10];
    z[21]=z[9] - 1;
    z[22]=z[21]*z[9];
    z[23]= - n<T>(9,4) + z[11];
    z[24]=n<T>(9,2)*z[12];
    z[25]= - static_cast<T>(11)+ z[24];
    z[25]=z[12]*z[25];
    z[14]=z[18] - z[22] + z[14] + n<T>(15,8)*z[20] + n<T>(1,2)*z[23] + z[25];
    z[18]=n<T>(3,2) - z[9];
    z[18]=z[9]*z[18];
    z[17]=z[18] + z[17];
    z[17]=z[8]*z[17];
    z[18]=static_cast<T>(1)+ n<T>(1,4)*z[10];
    z[18]=z[18]*z[10];
    z[23]=n<T>(5,2)*z[18];
    z[17]=z[17] + z[22] + static_cast<T>(1)- z[23];
    z[17]=z[8]*z[17];
    z[22]=z[10] + 3;
    z[25]=n<T>(5,8)*z[10];
    z[22]=z[22]*z[25];
    z[26]=3*z[9];
    z[27]=113*z[1];
    z[28]=static_cast<T>(1519)- z[27];
    z[17]=z[17] - z[26] + n<T>(403,32)*z[7] - z[22] + n<T>(61,2)*z[12] + n<T>(1,96)*
    z[28] - 3*z[11];
    z[28]=n<T>(5,4)*z[10];
    z[29]= - static_cast<T>(1)+ z[28];
    z[30]=n<T>(1,2)*z[9];
    z[31]= - z[8]*z[30];
    z[29]=z[31] + n<T>(1,2)*z[29] + z[9];
    z[29]=z[8]*z[29];
    z[31]= - n<T>(89,3) + n<T>(185,2)*z[1];
    z[31]=n<T>(1,8)*z[31] + z[11];
    z[32]=z[1] + 1;
    z[33]=z[3]*z[32];
    z[29]= - n<T>(13,2)*z[33] + n<T>(1,2)*z[31] + z[29];
    z[29]=z[3]*z[29];
    z[17]=n<T>(1,4)*z[17] + z[29];
    z[17]=z[3]*z[17];
    z[14]=n<T>(1,2)*z[14] + z[17];
    z[14]=z[3]*z[14];
    z[17]=n<T>(1,2)*z[12];
    z[29]=static_cast<T>(31)- z[24];
    z[29]=z[29]*z[17];
    z[31]=static_cast<T>(419)+ n<T>(81,2)*z[7];
    z[13]=z[31]*z[13];
    z[31]=n<T>(1,4)*z[9];
    z[33]=z[8]*z[31];
    z[13]=z[33] - z[9] + z[13] - z[25] + z[29] - n<T>(1319,192) - z[11];
    z[13]=z[8]*z[13];
    z[29]=n<T>(19,3) - n<T>(185,4)*z[1];
    z[33]= - n<T>(177,16)*z[7] - 15*z[12] + n<T>(1019,48) + z[11];
    z[33]=z[8]*z[33];
    z[29]=z[33] + n<T>(1,4)*z[29] - z[11];
    z[33]= - z[8] + z[32];
    z[33]=z[3]*z[33];
    z[29]=n<T>(1,2)*z[29] + n<T>(13,3)*z[33];
    z[29]=z[3]*z[29];
    z[33]=n<T>(5,16)*z[10];
    z[27]= - static_cast<T>(1237)+ z[27];
    z[13]=z[29] + z[13] + z[30] - n<T>(81,64)*z[7] + z[33] - n<T>(21,4)*z[12] + 
   n<T>(1,192)*z[27] + z[11];
    z[13]=z[3]*z[13];
    z[27]=9*z[12];
    z[29]= - static_cast<T>(17)+ z[27];
    z[29]=z[12]*z[29];
    z[23]=z[23] + z[29] - n<T>(13,24) + z[11];
    z[29]= - z[21]*z[30];
    z[34]=z[16]*z[8];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[9]*z[26];
    z[26]=z[26] - z[34];
    z[35]=n<T>(1,8)*z[8];
    z[26]=z[26]*z[35];
    z[36]=n<T>(2,3)*z[7];
    z[37]= - n<T>(51,32) - z[36];
    z[37]=z[7]*z[37];
    z[37]= - n<T>(319,64) + z[37];
    z[37]=z[7]*z[37];
    z[23]=z[26] + z[29] + n<T>(1,4)*z[23] + z[37];
    z[23]=z[8]*z[23];
    z[26]=static_cast<T>(23)- z[27];
    z[17]=z[26]*z[17];
    z[26]=n<T>(43,3) + n<T>(11,2)*z[1];
    z[17]= - z[28] + z[17] + n<T>(5,16)*z[26] - z[11];
    z[21]=z[21]*z[31];
    z[26]=n<T>(123,128) + z[36];
    z[26]=z[7]*z[26];
    z[13]=n<T>(1,2)*z[13] + z[23] + z[21] + n<T>(1,4)*z[17] + z[26];
    z[13]=z[3]*z[13];
    z[17]=z[19]*z[28];
    z[21]=n<T>(1,2)*z[2];
    z[23]=z[21]*npow(z[10],2);
    z[20]=z[20] - z[23];
    z[26]=n<T>(5,8)*z[2];
    z[20]=z[20]*z[26];
    z[24]=z[24] - 1;
    z[24]=z[24]*z[12];
    z[27]=z[24] - n<T>(55,32);
    z[20]=z[20] - z[17] - z[27];
    z[29]=n<T>(1,3)*z[7];
    z[37]=z[2] - 2;
    z[38]=z[2]*z[37];
    z[38]=static_cast<T>(2)+ z[38];
    z[38]=z[38]*z[29];
    z[39]= - static_cast<T>(21)+ n<T>(5,2)*z[2];
    z[40]=z[2]*z[39];
    z[40]=n<T>(123,2) + z[40];
    z[38]=n<T>(1,64)*z[40] + z[38];
    z[38]=z[7]*z[38];
    z[40]= - n<T>(77,4) + 3*z[2];
    z[40]=n<T>(1,16)*z[40];
    z[38]= - z[40] + z[38];
    z[38]=z[7]*z[38];
    z[41]= - 3*z[16] + z[34];
    z[35]=z[41]*z[35];
    z[20]=z[35] + n<T>(1,2)*z[16] + n<T>(1,4)*z[20] + z[38];
    z[20]=z[8]*z[20];
    z[33]= - z[2]*z[33];
    z[24]=z[33] + z[25] - n<T>(59,32) + z[24];
    z[25]=z[37]*z[29];
    z[25]=n<T>(1,64)*z[39] + z[25];
    z[25]=z[7]*z[25];
    z[16]=n<T>(1,4)*z[16];
    z[13]=z[13] + z[20] - z[16] + n<T>(1,4)*z[24] + z[25];
    z[13]=z[4]*z[13];
    z[20]= - n<T>(3,2) - z[10];
    z[20]=z[20]*z[28];
    z[24]=static_cast<T>(1)+ z[10];
    z[24]=z[10]*z[24];
    z[23]=z[24] - z[23];
    z[23]=z[23]*z[26];
    z[20]=z[23] + z[20] - z[27];
    z[23]=z[2] - 1;
    z[23]=z[23]*z[36];
    z[24]=5*z[2];
    z[25]=static_cast<T>(21)- z[24];
    z[25]=n<T>(1,64)*z[25] - z[23];
    z[25]=z[7]*z[25];
    z[25]=n<T>(3,16) + z[25];
    z[25]=z[7]*z[25];
    z[25]=n<T>(1,8)*z[34] + z[25] - z[16];
    z[25]=z[8]*z[25];
    z[23]=z[2]*z[23];
    z[24]= - n<T>(47,3) + z[24];
    z[24]=z[2]*z[24];
    z[24]=n<T>(371,3) + z[24];
    z[23]=n<T>(1,128)*z[24] + z[23];
    z[23]=z[7]*z[23];
    z[23]= - z[40] + z[23];
    z[23]=z[7]*z[23];
    z[13]=z[13] + z[14] + z[25] + z[16] + n<T>(1,4)*z[20] + z[23];
    z[13]=z[4]*z[13];
    z[14]=n<T>(47,3) + 5*z[5];
    z[16]=static_cast<T>(1)- n<T>(1,3)*z[5];
    z[16]=z[6]*z[16];
    z[20]=n<T>(1,3)*z[6];
    z[23]= - n<T>(5,32) + z[20];
    z[23]=z[2]*z[23];
    z[14]=z[23] + n<T>(1,64)*z[14] + z[16];
    z[16]= - z[6]*z[5];
    z[16]= - static_cast<T>(2)+ z[16];
    z[23]=z[2]*z[6];
    z[16]=2*z[16] + z[23];
    z[16]=z[2]*z[16];
    z[23]=static_cast<T>(1)+ z[5];
    z[24]=npow(z[5],2);
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[6]*z[24];
    z[16]=z[16] + 2*z[23] + z[24];
    z[16]=z[16]*z[29];
    z[14]=n<T>(1,2)*z[14] + z[16];
    z[14]=z[7]*z[14];
    z[14]=z[14] + n<T>(3,16) + z[20];
    z[14]=z[7]*z[14];
    z[16]=n<T>(17,2)*z[2];
    z[20]=n<T>(7,3) - z[16];
    z[20]=z[9]*z[20];
    z[20]= - n<T>(43,24) + z[20];
    z[20]=z[20]*z[30];
    z[20]=z[20] + z[34];
    z[20]=z[8]*z[20];
    z[23]=5*z[10];
    z[24]= - 17*z[2] + n<T>(297,8) - z[23];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(91,24) + z[24];
    z[20]=n<T>(1,2)*z[24] + z[20];
    z[20]=z[8]*z[20];
    z[16]= - z[16] + static_cast<T>(29)- n<T>(139,24)*z[1];
    z[24]= - static_cast<T>(1)- z[11];
    z[24]=z[9]*z[24];
    z[16]=z[20] + n<T>(1,2)*z[16] + z[24];
    z[20]= - z[21] + 1;
    z[20]=z[32]*z[20];
    z[21]= - static_cast<T>(1)- z[2];
    z[21]=z[21]*z[34];
    z[24]= - z[9]*z[2];
    z[21]=z[24] + n<T>(1,3)*z[21];
    z[15]=z[21]*z[15];
    z[21]=n<T>(1,2) - z[2];
    z[15]=n<T>(1,3)*z[21] + z[15];
    z[15]=z[8]*z[15];
    z[15]=n<T>(1,3)*z[20] + z[15];
    z[15]=z[3]*z[15];
    z[15]=n<T>(1,4)*z[16] + 13*z[15];
    z[15]=z[3]*z[15];
    z[16]=n<T>(41,8)*z[2] + z[22] - n<T>(413,16) + z[11];
    z[16]=z[9]*z[16];
    z[20]=41*z[2] + static_cast<T>(101)- z[23];
    z[20]=z[20]*z[31];
    z[18]=n<T>(1,8) + z[18];
    z[18]=5*z[18] + z[20];
    z[18]=z[18]*z[30];
    z[18]=z[18] - z[34];
    z[18]=z[8]*z[18];
    z[16]=z[18] + n<T>(21,16) + z[16];
    z[15]=n<T>(1,4)*z[16] + z[15];
    z[15]=z[3]*z[15];
    z[16]= - z[17] + n<T>(1,8) + 3*z[12];
    z[17]=z[19]*z[23];
    z[17]= - n<T>(41,2) + z[17];
    z[17]=z[17]*z[31];
    z[16]=3*z[16] + z[17];
    z[16]=z[16]*z[31];
    z[17]=z[12] - z[7];
    z[17]=z[6]*z[17];
    z[15]=z[15] + z[16] + z[17];
    z[15]=z[3]*z[15];
    z[16]=n<T>(5,128) + z[29];
    z[16]=z[8]*z[16]*npow(z[7],2);

    r += z[13] + z[14] + z[15] + z[16];
 
    return r;
}

template double qg_2lha_r1125(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1125(const std::array<dd_real,30>&);
#endif
