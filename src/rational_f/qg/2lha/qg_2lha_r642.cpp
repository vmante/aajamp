#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r642(const std::array<T,30>& k) {
  T z[45];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[28];
    z[8]=k[13];
    z[9]=k[11];
    z[10]=k[10];
    z[11]=k[5];
    z[12]=k[9];
    z[13]=k[17];
    z[14]=k[3];
    z[15]=k[6];
    z[16]=k[21];
    z[17]=n<T>(1,2)*z[8];
    z[18]=z[6] + 1;
    z[19]=z[6]*z[18];
    z[20]=z[14]*npow(z[16],2);
    z[20]=z[16] + n<T>(1,2)*z[20];
    z[21]=z[14]*z[15]*z[20];
    z[19]=z[21] - z[17] + z[10] + n<T>(1,2)*z[19];
    z[21]=n<T>(1,4)*z[14];
    z[20]= - z[20]*z[21];
    z[22]=n<T>(1,2)*z[9];
    z[23]= - z[16]*z[21];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[14]*z[23]*z[22];
    z[20]=z[23] + static_cast<T>(1)+ z[20];
    z[20]=z[9]*z[20];
    z[23]=z[8] + z[15];
    z[21]=z[23]*z[21];
    z[24]=z[21] - z[17] + static_cast<T>(1)- n<T>(1,2)*z[15];
    z[24]=z[14]*z[24];
    z[23]= - static_cast<T>(1)+ n<T>(1,4)*z[23];
    z[24]=z[24] + z[23];
    z[25]=n<T>(1,2)*z[11];
    z[24]=z[24]*z[25];
    z[21]=z[24] + z[21] - z[23];
    z[21]=z[11]*z[21];
    z[23]=z[8]*z[7];
    z[24]=z[6]*z[7];
    z[23]=z[23] - z[24];
    z[26]=npow(z[7],2);
    z[24]=z[26] + z[24];
    z[24]=z[6]*z[24];
    z[26]= - z[8]*z[26];
    z[24]=z[24] + z[26];
    z[24]=z[1]*z[24];
    z[24]=z[24] + z[23];
    z[24]=z[5]*z[24];
    z[26]=n<T>(1,2)*z[1];
    z[23]=z[23]*z[26];
    z[19]=n<T>(1,8)*z[24] + z[21] + z[23] + n<T>(1,4)*z[19] + z[20];
    z[20]=n<T>(1,3)*z[5];
    z[19]=z[19]*z[20];
    z[21]=n<T>(1,2)*z[5];
    z[23]= - n<T>(1,2)*z[2] + z[21];
    z[24]=npow(z[9],2);
    z[23]=z[24]*z[23];
    z[27]= - static_cast<T>(5)+ z[9];
    z[27]=z[27]*z[22];
    z[28]=z[12] - 1;
    z[29]= - z[12]*z[28];
    z[23]=z[29] + z[27] + z[4] + z[23];
    z[23]=z[2]*z[23];
    z[27]=n<T>(1,2)*z[12];
    z[29]=n<T>(1,3)*z[12];
    z[30]= - z[29] - n<T>(1,3) - z[4];
    z[30]=z[30]*z[27];
    z[31]=z[27] + 1;
    z[32]=z[31]*z[29];
    z[33]=n<T>(1,3)*z[10];
    z[34]= - z[33] + z[9];
    z[32]=n<T>(1,2)*z[34] + z[32];
    z[32]=z[5]*z[32];
    z[34]=n<T>(1,3)*z[1];
    z[35]= - z[4]*z[34];
    z[23]=n<T>(1,6)*z[23] + z[32] + z[30] + z[35] + n<T>(2,3) + n<T>(1,4)*z[9];
    z[23]=z[2]*z[23];
    z[30]=z[12] - z[11];
    z[30]=z[30]*z[5];
    z[32]=z[30] - z[11];
    z[35]= - n<T>(1,3) + z[27];
    z[35]=z[12]*z[35];
    z[32]=z[35] - n<T>(2,3)*z[32];
    z[32]=z[5]*z[32];
    z[35]= - static_cast<T>(11)+ z[26];
    z[36]= - n<T>(1,2) + n<T>(2,3)*z[1];
    z[36]=z[4]*z[36];
    z[37]=n<T>(7,12)*z[12] + n<T>(13,12) + z[4];
    z[37]=z[12]*z[37];
    z[23]=z[23] + z[32] + z[37] + n<T>(1,6)*z[35] + z[36];
    z[23]=z[2]*z[23];
    z[32]= - z[2] + 1;
    z[32]=z[22]*z[32];
    z[35]=z[1] - 1;
    z[36]=n<T>(1,2)*z[4];
    z[37]=z[36]*z[35];
    z[37]=z[37] - 1;
    z[38]= - z[10] + z[9];
    z[38]=z[38]*z[21];
    z[39]= - z[10] - z[4];
    z[39]=n<T>(1,2)*z[39] - z[12];
    z[39]=z[12]*z[39];
    z[32]=z[38] + z[39] + n<T>(1,2)*z[10] - z[37] + z[32];
    z[38]=n<T>(1,3)*z[2];
    z[32]=z[32]*z[38];
    z[39]=z[4] + n<T>(5,3)*z[12];
    z[39]=z[39]*z[27];
    z[40]=z[5]*z[12];
    z[41]=z[12] + 1;
    z[42]=z[41]*z[40];
    z[32]=z[32] + n<T>(2,3)*z[42] + z[39] - n<T>(1,6)*z[1] + z[37];
    z[32]=z[2]*z[32];
    z[39]=2*z[11];
    z[42]= - static_cast<T>(2)- z[12];
    z[42]=z[12]*z[42];
    z[42]=z[39] + z[42];
    z[42]=z[42]*z[20];
    z[43]=z[1] + 1;
    z[44]=z[41]*z[12];
    z[42]=z[42] - n<T>(1,6)*z[43] - z[44];
    z[42]=z[5]*z[42];
    z[36]= - n<T>(2,3)*z[12] - n<T>(1,3) - z[36];
    z[36]=z[12]*z[36];
    z[32]=z[32] + z[42] + z[36] + z[34] - z[37];
    z[32]=z[2]*z[32];
    z[36]=z[21]*z[6]*z[43];
    z[42]=n<T>(1,2)*z[43];
    z[31]=z[12]*z[31];
    z[31]=z[36] + z[31] - z[42] - z[11];
    z[31]=z[5]*z[31];
    z[31]=z[31] + z[42] + z[44];
    z[31]=z[5]*z[31];
    z[36]=z[4] + z[41];
    z[36]=z[36]*z[27];
    z[31]=z[31] + z[36] - z[26] + z[37];
    z[31]=n<T>(1,3)*z[31] + z[32];
    z[31]=z[3]*z[31];
    z[32]=npow(z[12],2);
    z[36]=n<T>(1,3)*z[32];
    z[37]= - z[6]*z[26];
    z[41]=z[5]*z[43]*npow(z[6],2);
    z[37]=n<T>(1,6)*z[41] + z[37] - z[36];
    z[37]=z[37]*z[21];
    z[41]= - static_cast<T>(1)- n<T>(5,6)*z[12];
    z[41]=z[41]*z[27];
    z[42]= - n<T>(1,2) + z[1];
    z[42]=n<T>(1,2)*z[42] - z[11];
    z[37]=z[37] + n<T>(1,3)*z[42] + z[41];
    z[37]=z[5]*z[37];
    z[41]=z[35]*z[4];
    z[42]=static_cast<T>(7)- z[26];
    z[42]=n<T>(1,2)*z[42] - z[41];
    z[43]= - z[27] - n<T>(11,6) - z[4];
    z[43]=z[43]*z[27];
    z[23]=z[31] + z[23] + z[37] + n<T>(1,3)*z[42] + z[43];
    z[23]=z[3]*z[23];
    z[31]=z[10] + 7*z[9];
    z[30]= - 2*z[30] - z[12] + n<T>(1,4)*z[31] + z[39];
    z[30]=z[30]*z[20];
    z[31]= - n<T>(5,2) + z[9];
    z[31]=z[31]*z[22];
    z[27]= - z[28]*z[27];
    z[27]=z[27] + z[31] + z[4];
    z[31]=n<T>(1,2)*z[24] + z[36];
    z[21]=z[31]*z[21];
    z[31]=z[2]*z[24];
    z[21]= - n<T>(1,4)*z[31] + n<T>(1,3)*z[27] + z[21];
    z[21]=z[2]*z[21];
    z[27]= - static_cast<T>(1)- z[34];
    z[27]=z[4]*z[27];
    z[27]=z[27] + n<T>(1,3)*z[9] + static_cast<T>(1)- z[33];
    z[31]=n<T>(5,4)*z[4];
    z[33]= - z[31] - static_cast<T>(1)+ n<T>(1,4)*z[10];
    z[33]=z[33]*z[29];
    z[21]=z[21] + z[30] + n<T>(1,4)*z[27] + z[33];
    z[21]=z[2]*z[21];
    z[18]=z[5]*z[18];
    z[18]=z[18] + z[35];
    z[18]=z[6]*z[18];
    z[18]=z[32] - static_cast<T>(1)+ z[18];
    z[18]=z[5]*z[18];
    z[18]= - z[1] + z[41] + z[18];
    z[27]=n<T>(1,4)*z[12] + static_cast<T>(2)+ z[31];
    z[27]=z[12]*z[27];
    z[18]=z[27] - static_cast<T>(1)+ n<T>(1,4)*z[18];
    z[18]=z[23] + n<T>(1,3)*z[18] + z[21];
    z[18]=z[3]*z[18];
    z[21]=static_cast<T>(1)- z[14];
    z[21]=z[9]*z[21];
    z[21]=z[21] - 1;
    z[21]=z[14]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[22];
    z[22]=z[17]*z[14];
    z[21]=z[21] + z[22] + static_cast<T>(1)+ z[8];
    z[22]= - z[8] + z[22];
    z[22]=z[14]*z[22];
    z[17]=z[17] + z[22];
    z[22]=z[11]*z[14];
    z[17]=z[17]*z[22];
    z[23]=npow(z[14],2);
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[8]*z[23];
    z[17]=n<T>(1,2)*z[23] + z[17];
    z[23]=n<T>(1,3)*z[11];
    z[17]=z[17]*z[23];
    z[26]= - z[8]*z[26];
    z[27]= - z[4]*z[29];
    z[17]=z[27] + z[17] + n<T>(1,3)*z[21] + z[26];
    z[21]=static_cast<T>(1)+ z[11];
    z[21]=z[21]*z[25];
    z[26]=npow(z[13],2);
    z[27]=npow(z[11],2);
    z[30]=n<T>(1,2)*z[27];
    z[26]=z[26] - z[30];
    z[26]=z[2]*z[26]*z[28];
    z[25]=z[25] + z[13];
    z[25]=z[25]*z[12];
    z[21]=z[26] - z[25] + z[13] + z[21];
    z[21]=z[12]*z[21];
    z[24]=z[27] + n<T>(7,4)*z[24];
    z[24]=n<T>(1,2)*z[24];
    z[25]= - z[30] + z[25];
    z[25]=z[25]*z[40];
    z[21]=z[25] - z[24] + z[21];
    z[21]=z[21]*z[38];
    z[25]=z[40]*z[39];
    z[26]=z[12]*z[11];
    z[24]=z[25] + z[24] + z[26];
    z[20]=z[24]*z[20];
    z[22]= - static_cast<T>(1)- z[22];
    z[22]=z[22]*z[23];
    z[23]=n<T>(1,4) - n<T>(1,3)*z[14];
    z[23]=z[9]*z[23];
    z[23]= - n<T>(5,12) + z[23];
    z[23]=z[9]*z[23];
    z[22]=z[22] + z[23] + n<T>(1,6)*z[4];
    z[23]=z[11]*z[29];
    z[20]=z[21] + z[20] + n<T>(1,2)*z[22] + z[23];
    z[20]=z[2]*z[20];

    r += n<T>(1,4)*z[17] + z[18] + z[19] + z[20];
 
    return r;
}

template double qg_2lha_r642(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r642(const std::array<dd_real,30>&);
#endif
