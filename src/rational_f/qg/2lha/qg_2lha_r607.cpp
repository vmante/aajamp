#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r607(const std::array<T,30>& k) {
  T z[8];
  T r = 0;

    z[1]=k[1];
    z[2]=k[13];
    z[3]=k[3];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=static_cast<T>(3)- z[5];
    z[6]=z[5]*z[6];
    z[7]= - z[4]*npow(z[5],2);
    z[6]=z[6] + z[7];
    z[6]=z[4]*z[6];
    z[6]=n<T>(1,2)*z[6] - n<T>(3,2) + z[5];
    z[6]=z[4]*z[6];
    z[7]= - z[3] + z[1];
    z[6]=n<T>(1,2)*z[7] + z[6];

    r += n<T>(1,4)*z[6]*z[2];
 
    return r;
}

template double qg_2lha_r607(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r607(const std::array<dd_real,30>&);
#endif
