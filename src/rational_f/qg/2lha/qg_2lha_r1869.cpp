#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1869(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[29];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=k[8];
    z[11]=k[11];
    z[12]=k[14];
    z[13]= - n<T>(11,2) - z[1];
    z[13]=3*z[13] + n<T>(23,6)*z[8];
    z[14]=3*z[9];
    z[15]= - z[14] + static_cast<T>(3)- z[8];
    z[15]=z[5]*z[15];
    z[13]=n<T>(3,4)*z[15] + n<T>(1,2)*z[13] + z[9];
    z[13]=z[5]*z[13];
    z[15]=n<T>(21,4)*z[1];
    z[16]=n<T>(7,4)*z[8];
    z[17]= - z[9] - 1;
    z[17]=z[11]*z[17];
    z[18]=z[3]*z[1];
    z[13]=z[13] - z[16] - n<T>(1,4)*z[18] + z[15] + n<T>(5,2) + z[17];
    z[17]=z[5]*z[8];
    z[19]=z[1] + 1;
    z[19]=z[19]*z[3];
    z[17]=z[17] + n<T>(29,2) + z[19];
    z[20]=n<T>(1,2)*z[9];
    z[21]=static_cast<T>(5)+ z[3];
    z[21]=z[21]*z[20];
    z[22]=z[6]*z[9];
    z[23]=n<T>(9,2)*z[22];
    z[17]=z[23] + z[21] + n<T>(1,3)*z[17];
    z[21]=n<T>(1,4)*z[6];
    z[17]=z[17]*z[21];
    z[24]=n<T>(1,8)*z[5];
    z[25]=z[5]*z[9];
    z[26]=9*z[25] - 31*z[9] - static_cast<T>(9)+ n<T>(1,3)*z[8];
    z[26]=z[26]*z[24];
    z[27]= - static_cast<T>(1)+ z[1];
    z[27]=z[3]*z[27];
    z[27]=z[27] + n<T>(13,3) + 9*z[1];
    z[17]=z[17] + z[26] + n<T>(1,8)*z[27] + z[9];
    z[17]=z[6]*z[17];
    z[13]=n<T>(1,2)*z[13] + z[17];
    z[13]=z[4]*z[13];
    z[17]=n<T>(1,2)*z[8];
    z[26]=n<T>(3,2)*z[5];
    z[27]=static_cast<T>(3)- z[1];
    z[27]= - z[26] + n<T>(105,2)*z[9] + 3*z[27] + z[17];
    z[28]=n<T>(1,2)*z[5];
    z[27]=z[27]*z[28];
    z[29]=n<T>(1,2)*z[3];
    z[30]= - static_cast<T>(1)- n<T>(1,2)*z[1];
    z[30]=z[30]*z[29];
    z[31]=static_cast<T>(13)- z[29];
    z[31]=z[31]*z[20];
    z[15]=z[27] + z[31] + z[30] + static_cast<T>(13)+ z[15];
    z[27]=static_cast<T>(3)+ 13*z[9];
    z[22]=n<T>(3,4)*z[22] + n<T>(1,4)*z[27] + z[25];
    z[22]=z[6]*z[22];
    z[27]=n<T>(1,4)*z[3];
    z[30]=static_cast<T>(15)- z[27];
    z[30]=z[30]*z[20];
    z[31]=57*z[9] + static_cast<T>(3)+ z[8];
    z[24]=z[31]*z[24];
    z[22]=n<T>(3,2)*z[22] + z[24] + z[30] + n<T>(1,8)*z[19] + static_cast<T>(5)+ n<T>(9,8)*z[1];
    z[22]=z[6]*z[22];
    z[15]=n<T>(1,2)*z[15] + z[22];
    z[15]=z[6]*z[15];
    z[22]=z[9] - z[8];
    z[22]=z[22]*z[5];
    z[24]=5*z[9];
    z[22]=z[22] - z[24] + 3*z[8];
    z[30]=n<T>(1,4)*z[5];
    z[31]=z[22]*z[30];
    z[31]=z[31] + n<T>(5,2)*z[9] - static_cast<T>(1)- n<T>(3,4)*z[8];
    z[32]=3*z[5];
    z[31]=z[31]*z[32];
    z[33]=n<T>(57,2)*z[9] + static_cast<T>(21)+ z[8];
    z[31]=n<T>(1,2)*z[33] + z[31];
    z[31]=z[5]*z[31];
    z[33]= - static_cast<T>(1)- z[11];
    z[33]=z[9]*z[33];
    z[33]= - static_cast<T>(1)+ z[33];
    z[31]=n<T>(1,2)*z[33] + z[31];
    z[15]=n<T>(1,2)*z[31] + z[15];
    z[15]=z[6]*z[15];
    z[22]=z[22]*z[28];
    z[22]=z[22] - n<T>(3,2)*z[8] + z[24];
    z[22]=z[22]*z[32];
    z[22]=z[22] + z[8] - n<T>(3,2)*z[9];
    z[22]=z[5]*z[22];
    z[31]=static_cast<T>(1)- z[11];
    z[31]=z[9]*z[31];
    z[22]=z[22] - z[11] + z[31];
    z[15]=n<T>(1,4)*z[22] + z[15];
    z[15]=z[4]*z[15];
    z[22]= - static_cast<T>(9)- z[3];
    z[22]=n<T>(1,2)*z[22] + z[24];
    z[22]=z[9]*z[22];
    z[24]=npow(z[9],2);
    z[31]=npow(z[3],2)*z[24]*z[6];
    z[33]=z[24]*z[3];
    z[34]=z[33] + n<T>(1,2)*z[31];
    z[34]=z[6]*z[34];
    z[22]=z[22] + z[34];
    z[21]=z[22]*z[21];
    z[22]= - static_cast<T>(11)+ z[29];
    z[34]=static_cast<T>(5)- z[11];
    z[34]=z[11]*z[34];
    z[34]= - static_cast<T>(1)+ n<T>(1,4)*z[34];
    z[34]=z[9]*z[34];
    z[22]=n<T>(1,4)*z[22] + z[34];
    z[22]=z[9]*z[22];
    z[34]=z[9] - 1;
    z[34]=z[9]*z[34];
    z[34]=z[34] + z[8];
    z[34]=z[10]*z[34];
    z[30]=z[34]*z[30];
    z[35]=n<T>(1,2) + z[9];
    z[35]=z[9]*z[35];
    z[30]=z[35] + z[30];
    z[30]=z[30]*z[26];
    z[15]=z[15] + z[21] + z[30] + n<T>(1,8)*z[8] + z[22];
    z[15]=z[7]*z[15];
    z[21]= - static_cast<T>(13)- z[27];
    z[21]=z[9]*z[21];
    z[21]= - z[23] - n<T>(9,4)*z[25] - n<T>(9,2) + z[21];
    z[21]=z[6]*z[21];
    z[22]=n<T>(9,2) - 7*z[9];
    z[22]=z[22]*z[28];
    z[23]= - z[29] - n<T>(9,2);
    z[23]=z[1]*z[23];
    z[27]= - static_cast<T>(15)+ z[29];
    z[27]=z[9]*z[27];
    z[21]=z[21] + z[22] + z[27] - n<T>(1,4)*z[8] - static_cast<T>(13)+ z[23];
    z[21]=z[6]*z[21];
    z[22]=z[14] + z[19] - static_cast<T>(29)- 21*z[1];
    z[23]=n<T>(17,4) + z[1];
    z[16]= - n<T>(57,4)*z[9] + 3*z[23] + z[16];
    z[14]=z[8] - z[14];
    z[14]=z[5]*z[14];
    z[14]=n<T>(3,8)*z[14] + n<T>(19,4)*z[9] - n<T>(3,4) - z[8];
    z[14]=z[5]*z[14];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[5]*z[14];
    z[14]=n<T>(1,2)*z[21] + n<T>(1,4)*z[22] + z[14];
    z[14]=z[6]*z[14];
    z[16]=z[8] + 1;
    z[21]=3*z[11] - z[16];
    z[22]= - static_cast<T>(1)+ n<T>(3,2)*z[11];
    z[22]=z[9]*z[22];
    z[21]=n<T>(1,2)*z[21] + z[22];
    z[22]=z[26] - n<T>(17,4);
    z[22]=z[8]*z[22];
    z[20]=z[20] - static_cast<T>(3)+ z[22];
    z[20]=z[20]*z[28];
    z[20]=z[20] + n<T>(1,4)*z[9] + n<T>(21,4) + 2*z[8];
    z[20]=z[5]*z[20];
    z[14]=z[14] + n<T>(1,2)*z[21] + z[20];
    z[14]=z[4]*z[14];
    z[20]= - z[5]*z[34];
    z[20]= - 3*z[24] + z[20];
    z[21]= - z[33] - z[31];
    z[21]=z[6]*z[21];
    z[20]=3*z[20] + z[21];
    z[14]=z[15] + n<T>(1,8)*z[20] + z[14];
    z[14]=z[7]*z[14];
    z[15]= - z[3] - z[8];
    z[15]=z[12]*z[15];
    z[13]=z[14] + n<T>(1,12)*z[15] + z[13];
    z[13]=z[7]*z[13];
    z[14]=static_cast<T>(1)- z[2];
    z[14]=z[14]*z[18];
    z[14]=z[17] + z[14] + n<T>(25,2) + z[2];
    z[15]= - n<T>(1,2) + z[9];
    z[15]=z[15]*z[32];
    z[14]=n<T>(1,3)*z[14] + z[15];
    z[15]= - z[19] - z[16];
    z[15]= - n<T>(3,8)*z[25] + n<T>(1,12)*z[15] + z[9];
    z[15]=z[6]*z[15];
    z[14]=n<T>(1,4)*z[14] + z[15];
    z[14]=z[4]*z[14];

    r += z[13] + z[14];
 
    return r;
}

template double qg_2lha_r1869(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1869(const std::array<dd_real,30>&);
#endif
