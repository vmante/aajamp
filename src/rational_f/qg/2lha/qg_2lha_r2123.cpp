#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2123(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[14];
    z[7]=k[9];
    z[8]=k[6];
    z[9]=z[3]*z[8];
    z[10]= - static_cast<T>(1)- z[4];
    z[10]=z[7]*z[10];
    z[11]= - z[8] + static_cast<T>(1)- z[4];
    z[11]=z[2]*z[11];
    z[10]=z[11] + z[9] + z[10];
    z[10]=z[5]*z[10];
    z[11]=z[4] - n<T>(1,2);
    z[12]=z[6]*z[7];
    z[13]=z[11]*z[12];
    z[11]= - z[6]*z[11];
    z[11]=z[11] + n<T>(5,3)*z[8] - n<T>(1,2)*z[3];
    z[11]=z[2]*z[11];
    z[11]=z[13] - z[11];
    z[13]= - z[9] + n<T>(1,2)*z[7];
    z[10]=n<T>(5,6)*z[10] + n<T>(5,3)*z[13] - n<T>(1,2)*z[11];
    z[10]=z[5]*z[10];
    z[11]=z[7] - n<T>(1,4)*z[12];
    z[11]=z[6]*z[11];
    z[9]=n<T>(5,4)*z[9] + z[11];
    z[11]=static_cast<T>(1)- n<T>(1,4)*z[6];
    z[11]=z[6]*z[11];
    z[12]=z[3]*z[1];
    z[12]=z[12] - z[6];
    z[12]=z[2]*z[12];
    z[11]=n<T>(1,12)*z[12] + n<T>(1,8)*z[3] + n<T>(1,3)*z[11];
    z[11]=z[2]*z[11];

    r += n<T>(1,3)*z[9] + n<T>(1,2)*z[10] + z[11];
 
    return r;
}

template double qg_2lha_r2123(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2123(const std::array<dd_real,30>&);
#endif
