#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1998(const std::array<T,30>& k) {
  T z[39];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[11];
    z[7]=k[3];
    z[8]=k[21];
    z[9]=k[9];
    z[10]=n<T>(1,2)*z[1];
    z[11]=z[10] + 1;
    z[12]=z[11]*z[10];
    z[13]=z[9] - 3;
    z[14]=n<T>(1,2)*z[9];
    z[15]=z[13]*z[14];
    z[15]= - z[12] + static_cast<T>(1)+ z[15];
    z[16]=3*z[1];
    z[17]=z[11]*z[16];
    z[18]=npow(z[2],2);
    z[19]= - z[2] + 1;
    z[19]=z[6]*z[19];
    z[19]=n<T>(1,2) + z[19];
    z[19]=z[19]*z[18];
    z[20]=z[11]*z[1];
    z[21]=z[20] + n<T>(1,2);
    z[19]=z[19] - z[21];
    z[19]=z[2]*z[19];
    z[22]=n<T>(3,2)*z[9];
    z[23]=static_cast<T>(5)- z[22];
    z[23]=z[9]*z[23];
    z[17]=z[19] + z[17] - n<T>(7,2) + z[23];
    z[19]=n<T>(1,2)*z[2];
    z[17]=z[17]*z[19];
    z[15]=3*z[15] + z[17];
    z[15]=z[15]*z[19];
    z[17]=n<T>(1,2)*z[5];
    z[23]=z[10]*z[5];
    z[24]=z[5] + z[23];
    z[24]=z[1]*z[24];
    z[24]=z[17] + z[24];
    z[24]=z[4]*z[24];
    z[24]=z[24] - z[21];
    z[24]=z[4]*z[24];
    z[25]=npow(z[9],2);
    z[26]=static_cast<T>(1)- z[25];
    z[27]= - z[2]*z[21];
    z[24]=z[24] + z[27] + n<T>(1,2)*z[26] + z[20];
    z[26]=n<T>(1,2)*z[4];
    z[24]=z[24]*z[26];
    z[27]=3*z[9];
    z[28]=z[27] - 5;
    z[29]=z[28]*z[14];
    z[29]=static_cast<T>(1)+ z[29];
    z[30]=n<T>(1,2)*z[6];
    z[31]=npow(z[2],3)*z[30];
    z[31]=z[31] - z[21];
    z[31]=z[31]*z[19];
    z[20]=z[31] + n<T>(1,2)*z[29] + z[20];
    z[20]=z[2]*z[20];
    z[29]= - static_cast<T>(1)+ n<T>(3,4)*z[9];
    z[31]=z[29]*z[9];
    z[12]=z[24] + z[20] - z[12] - n<T>(1,4) - z[31];
    z[12]=z[12]*z[26];
    z[20]=n<T>(1,4)*z[1];
    z[24]=z[11]*z[20];
    z[26]= - static_cast<T>(1)+ n<T>(3,8)*z[9];
    z[26]=z[26]*z[9];
    z[12]=z[12] + z[15] + z[24] - n<T>(5,8) - z[26];
    z[12]=z[4]*z[12];
    z[15]=z[2]*z[30];
    z[15]=z[15] - n<T>(1,2) - z[6];
    z[15]=z[2]*z[15];
    z[11]=z[15] + z[30] + z[11];
    z[11]=z[2]*z[11];
    z[11]=z[11] - z[21];
    z[11]=z[11]*z[19];
    z[15]=n<T>(1,4)*z[9];
    z[21]= - static_cast<T>(5)+ z[9];
    z[21]=z[21]*z[15];
    z[24]= - n<T>(1,2) + z[1];
    z[24]=z[1]*z[24];
    z[11]=z[11] + z[24] + static_cast<T>(1)+ z[21];
    z[11]=z[11]*z[19];
    z[21]=static_cast<T>(7)- z[22];
    z[21]=z[9]*z[21];
    z[21]= - n<T>(11,2) + z[21];
    z[22]=static_cast<T>(1)- n<T>(3,4)*z[1];
    z[22]=z[1]*z[22];
    z[11]=z[11] + n<T>(1,4)*z[21] + z[22];
    z[11]=z[2]*z[11];
    z[21]= - static_cast<T>(13)+ z[27];
    z[14]=z[21]*z[14];
    z[14]=static_cast<T>(5)+ z[14];
    z[21]= - n<T>(7,4) + z[1];
    z[21]=z[1]*z[21];
    z[14]=n<T>(1,2)*z[14] + z[21];
    z[11]=n<T>(1,2)*z[14] + z[11];
    z[11]=z[2]*z[11];
    z[14]=static_cast<T>(1)- z[10];
    z[14]=z[14]*z[10];
    z[15]=z[15] - 1;
    z[21]= - z[9]*z[15];
    z[14]=z[14] - n<T>(3,4) + z[21];
    z[11]=z[12] + n<T>(1,2)*z[14] + z[11];
    z[11]=z[3]*z[11];
    z[12]=n<T>(15,4) - z[9];
    z[12]=z[12]*z[27];
    z[14]=static_cast<T>(5)- 9*z[1];
    z[14]=z[14]*z[20];
    z[12]=z[14] - n<T>(19,4) + z[12];
    z[14]=5*z[1];
    z[21]=9*z[6];
    z[22]=static_cast<T>(19)+ z[21];
    z[22]=n<T>(1,2)*z[22] + z[14];
    z[24]=npow(z[6],2);
    z[32]=z[24]*z[2];
    z[33]=n<T>(1,4)*z[32];
    z[34]=static_cast<T>(3)+ z[30];
    z[34]=z[6]*z[34];
    z[34]=z[34] - z[33];
    z[34]=z[2]*z[34];
    z[35]= - static_cast<T>(21)- z[6];
    z[35]=z[6]*z[35];
    z[35]= - static_cast<T>(11)+ z[35];
    z[34]=n<T>(1,4)*z[35] + z[34];
    z[34]=z[2]*z[34];
    z[22]=n<T>(1,2)*z[22] + z[34];
    z[22]=z[2]*z[22];
    z[34]= - static_cast<T>(25)- z[16];
    z[34]=z[34]*z[10];
    z[34]= - static_cast<T>(1)+ z[34];
    z[22]=n<T>(1,2)*z[34] + z[22];
    z[22]=z[22]*z[19];
    z[15]=z[15]*z[27];
    z[34]=static_cast<T>(5)+ z[16];
    z[34]=z[1]*z[34];
    z[15]=z[22] + n<T>(3,8)*z[34] + n<T>(1,4) + z[15];
    z[15]=z[2]*z[15];
    z[12]=n<T>(1,2)*z[12] + z[15];
    z[12]=z[2]*z[12];
    z[15]= - n<T>(7,2) + z[9];
    z[15]=z[15]*z[27];
    z[22]= - static_cast<T>(5)+ z[16];
    z[22]=z[22]*z[10];
    z[15]=z[22] + n<T>(11,2) + z[15];
    z[12]=n<T>(1,4)*z[15] + z[12];
    z[15]=z[1]*z[5];
    z[22]=static_cast<T>(1)+ n<T>(3,2)*z[5];
    z[22]=3*z[22] + n<T>(7,2)*z[15];
    z[22]=z[1]*z[22];
    z[34]=npow(z[5],2);
    z[35]= - z[34]*z[10];
    z[36]=z[5] + 3;
    z[37]= - z[5]*z[36];
    z[35]=z[37] + z[35];
    z[35]=z[1]*z[35];
    z[37]= - static_cast<T>(3)- z[17];
    z[37]=z[5]*z[37];
    z[35]=z[37] + z[35];
    z[35]=z[4]*z[35];
    z[22]=z[35] + z[22] + z[36];
    z[22]=z[4]*z[22];
    z[25]=3*z[25];
    z[16]= - n<T>(13,2) - z[16];
    z[16]=z[1]*z[16];
    z[35]=z[1] + 1;
    z[36]=z[2]*z[35];
    z[16]=z[22] + 3*z[36] + z[16] - n<T>(7,2) + z[25];
    z[22]=n<T>(1,8)*z[4];
    z[16]=z[16]*z[22];
    z[33]=z[6] - z[33];
    z[33]=z[33]*z[18];
    z[33]=n<T>(3,2)*z[35] + z[33];
    z[36]=n<T>(1,4)*z[2];
    z[33]=z[33]*z[36];
    z[36]= - static_cast<T>(17)- z[14];
    z[36]=z[1]*z[36];
    z[31]=z[33] + n<T>(1,16)*z[36] - n<T>(3,4) - z[31];
    z[31]=z[2]*z[31];
    z[14]=static_cast<T>(11)+ z[14];
    z[14]=z[14]*z[20];
    z[33]= - static_cast<T>(5)+ n<T>(9,2)*z[9];
    z[33]=z[9]*z[33];
    z[14]=z[14] + n<T>(3,2) + z[33];
    z[14]=z[16] + n<T>(1,4)*z[14] + z[31];
    z[14]=z[4]*z[14];
    z[16]= - n<T>(21,4) - z[1];
    z[16]=z[16]*z[20];
    z[31]=n<T>(1,8)*z[6];
    z[33]= - static_cast<T>(1)- z[31];
    z[33]=z[6]*z[33];
    z[33]=z[33] + n<T>(1,8)*z[32];
    z[33]=z[2]*z[33];
    z[36]=static_cast<T>(1)+ n<T>(13,4)*z[6];
    z[33]=n<T>(1,4)*z[36] + z[33];
    z[33]=z[2]*z[33];
    z[33]=n<T>(3,8)*z[35] + z[33];
    z[33]=z[2]*z[33];
    z[16]=z[33] + z[16] - n<T>(5,16) + z[26];
    z[16]=z[2]*z[16];
    z[26]=n<T>(5,2) - z[9];
    z[26]=z[9]*z[26];
    z[26]= - static_cast<T>(1)+ z[26];
    z[33]=static_cast<T>(3)+ z[1];
    z[33]=z[1]*z[33];
    z[26]=3*z[26] + z[33];
    z[16]=n<T>(1,2)*z[26] + z[16];
    z[16]=z[2]*z[16];
    z[26]=9*z[9];
    z[33]= - n<T>(41,2) + z[26];
    z[33]=z[9]*z[33];
    z[33]=n<T>(19,2) + z[33];
    z[35]= - n<T>(9,4) - z[1];
    z[35]=z[1]*z[35];
    z[33]=n<T>(1,2)*z[33] + z[35];
    z[14]=z[14] + n<T>(1,4)*z[33] + z[16];
    z[14]=z[4]*z[14];
    z[11]=z[11] + n<T>(1,2)*z[12] + z[14];
    z[11]=z[3]*z[11];
    z[12]=5*z[5];
    z[14]=static_cast<T>(17)- z[12];
    z[14]=n<T>(1,2)*z[14] - z[25];
    z[16]=3*z[5];
    z[33]=n<T>(13,2) + z[16];
    z[15]=n<T>(1,2)*z[33] + z[15];
    z[15]=z[1]*z[15];
    z[12]= - static_cast<T>(13)- z[12];
    z[12]=z[5]*z[12];
    z[33]= - z[1]*z[34];
    z[12]=z[12] + z[33];
    z[12]=z[12]*z[20];
    z[33]= - n<T>(17,4) - z[5];
    z[33]=z[5]*z[33];
    z[12]=z[33] + z[12];
    z[12]=z[4]*z[12];
    z[12]=z[12] + n<T>(1,2)*z[14] + z[15];
    z[12]=z[4]*z[12];
    z[14]=z[9] - 1;
    z[15]=z[14]*z[27];
    z[33]=n<T>(13,2)*z[1] + n<T>(17,2) + z[15];
    z[35]=z[8] - z[6];
    z[35]=n<T>(1,2)*z[35] - z[32];
    z[35]=z[35]*z[18];
    z[33]=n<T>(1,4)*z[33] + z[35];
    z[33]=z[33]*z[19];
    z[35]=n<T>(1,8)*z[1];
    z[36]= - static_cast<T>(7)- n<T>(3,2)*z[1];
    z[36]=z[36]*z[35];
    z[37]=static_cast<T>(1)- n<T>(9,8)*z[9];
    z[37]=z[9]*z[37];
    z[12]=n<T>(1,4)*z[12] + z[33] + z[36] - n<T>(1,2) + z[37];
    z[12]=z[4]*z[12];
    z[33]=z[1] + 11;
    z[36]= - z[33]*z[10];
    z[28]=z[9]*z[28];
    z[28]=z[36] - n<T>(9,4) + z[28];
    z[36]=n<T>(1,16)*z[6];
    z[37]= - static_cast<T>(25)- z[21];
    z[37]=z[37]*z[36];
    z[37]=z[37] + z[32];
    z[37]=z[2]*z[37];
    z[38]= - z[8] + n<T>(9,2)*z[6];
    z[37]=n<T>(1,4)*z[38] + z[37];
    z[37]=z[2]*z[37];
    z[38]=n<T>(13,4)*z[1] + n<T>(19,4) - z[9];
    z[37]=n<T>(1,4)*z[38] + z[37];
    z[37]=z[2]*z[37];
    z[28]=n<T>(1,4)*z[28] + z[37];
    z[28]=z[2]*z[28];
    z[37]=n<T>(35,2) - z[26];
    z[37]=z[9]*z[37];
    z[38]=n<T>(9,2) + z[1];
    z[38]=z[1]*z[38];
    z[37]=z[38] - static_cast<T>(5)+ z[37];
    z[12]=z[12] + n<T>(1,8)*z[37] + z[28];
    z[12]=z[4]*z[12];
    z[28]= - static_cast<T>(15)- z[10];
    z[20]=z[28]*z[20];
    z[21]=static_cast<T>(29)+ z[21];
    z[21]=z[21]*z[31];
    z[21]=z[21] - z[32];
    z[21]=z[2]*z[21];
    z[28]= - static_cast<T>(5)- z[31];
    z[28]=z[6]*z[28];
    z[21]=z[21] - n<T>(21,8) + z[28];
    z[21]=z[2]*z[21];
    z[28]=static_cast<T>(1)- z[8];
    z[28]=z[9]*z[28];
    z[28]=n<T>(15,4)*z[1] + n<T>(7,4)*z[6] + z[28] + n<T>(27,4) + z[8];
    z[21]=n<T>(1,2)*z[28] + z[21];
    z[21]=z[2]*z[21];
    z[28]=z[9] - n<T>(1,2);
    z[20]=z[21] + z[20] - z[28];
    z[20]=z[2]*z[20];
    z[21]= - static_cast<T>(7)+ z[27];
    z[21]=z[9]*z[21];
    z[37]=n<T>(15,2) + z[1];
    z[37]=z[1]*z[37];
    z[21]=z[37] - n<T>(5,2) + z[21];
    z[20]=n<T>(1,4)*z[21] + z[20];
    z[20]=z[2]*z[20];
    z[21]= - z[13]*z[27];
    z[37]=npow(z[1],2);
    z[21]= - n<T>(1,2)*z[37] - n<T>(5,2) + z[21];
    z[20]=n<T>(1,4)*z[21] + z[20];
    z[11]=z[11] + n<T>(1,2)*z[20] + z[12];
    z[11]=z[3]*z[11];
    z[12]=13*z[6];
    z[20]=static_cast<T>(25)+ z[12];
    z[20]=z[20]*z[30];
    z[20]=z[20] - 9*z[32];
    z[20]=z[20]*z[19];
    z[20]=z[20] - n<T>(25,4)*z[6] - n<T>(17,4) + z[8];
    z[19]=z[20]*z[19];
    z[20]=n<T>(1,4)*z[6];
    z[21]=n<T>(9,2) - z[8];
    z[37]=static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[37]=z[9]*z[37];
    z[19]=z[19] + z[35] + z[20] + n<T>(1,2)*z[21] + z[37];
    z[19]=z[2]*z[19];
    z[21]= - z[1] + z[30] - n<T>(1,2) - z[27];
    z[19]=n<T>(1,4)*z[21] + z[19];
    z[19]=z[2]*z[19];
    z[14]= - z[14]*z[30];
    z[13]=z[9]*z[13];
    z[10]=z[10] + z[13] + z[14];
    z[10]=n<T>(1,4)*z[10] + z[19];
    z[13]= - static_cast<T>(5)+ z[26];
    z[13]=z[13]*z[30];
    z[14]=n<T>(1,2) + z[16];
    z[14]=z[1]*z[14];
    z[13]=z[14] + z[13] + z[25] + n<T>(11,2) - z[5];
    z[14]=n<T>(5,2)*z[6];
    z[19]=z[14] + z[17] + z[9];
    z[19]=z[9]*z[19];
    z[21]= - static_cast<T>(1)- z[16];
    z[21]=z[21]*z[23];
    z[16]= - n<T>(11,2) - z[16];
    z[16]=z[5]*z[16];
    z[16]=z[21] + z[16] + z[19];
    z[16]=z[16]*z[22];
    z[19]= - n<T>(9,8)*z[32] + n<T>(3,4)*z[8] - z[6];
    z[18]=z[19]*z[18];
    z[13]=z[16] + n<T>(1,8)*z[13] + z[18];
    z[13]=z[4]*z[13];
    z[16]=z[20] + z[8] + z[28];
    z[12]= - static_cast<T>(9)- z[12];
    z[12]=z[12]*z[31];
    z[12]=n<T>(9,2)*z[32] - z[8] + z[12];
    z[12]=z[2]*z[12];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[12]=z[2]*z[12];
    z[16]= - 5*z[6] + z[33];
    z[12]=n<T>(1,8)*z[16] + z[12];
    z[12]=z[2]*z[12];
    z[15]= - n<T>(1,2) + z[15];
    z[16]=z[6]*z[29];
    z[15]= - z[1] + n<T>(1,2)*z[15] + z[16];
    z[12]=n<T>(1,2)*z[15] + z[12];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[4]*z[12];
    z[10]=z[11] + n<T>(1,2)*z[10] + z[12];
    z[10]=z[3]*z[10];
    z[11]=z[5] - 1;
    z[12]= - z[17]*z[7]*z[11];
    z[12]=static_cast<T>(3)+ z[12];
    z[13]=n<T>(1,4)*z[5];
    z[15]=z[13] - 1;
    z[16]=n<T>(1,2)*z[7];
    z[17]=z[16] + z[15];
    z[17]=z[17]*z[27];
    z[16]= - static_cast<T>(1)- z[16];
    z[16]=z[5]*z[16];
    z[16]=n<T>(1,2) + z[16];
    z[16]=z[16]*z[30];
    z[12]=z[16] + n<T>(1,2)*z[12] + z[17];
    z[12]=z[6]*z[12];
    z[16]=z[5] - z[27];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[16]=z[7]*z[8];
    z[17]= - z[31] + n<T>(9,8);
    z[17]=z[7]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[6]*z[17];
    z[17]=z[17] + z[8] - z[16];
    z[18]=5*z[7];
    z[19]= - n<T>(7,2) + z[18];
    z[19]=z[6]*z[19];
    z[19]=n<T>(13,2) + z[19];
    z[19]=z[19]*z[31];
    z[19]=2*z[32] - z[8] + z[19];
    z[19]=z[2]*z[19];
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[17]=z[2]*z[17];
    z[13]= - z[11]*z[13];
    z[15]=z[15]*z[27];
    z[19]= - z[5]*z[20];
    z[13]=z[19] + z[15] - z[7] + z[13];
    z[13]=z[6]*z[13];
    z[13]=z[13] + z[16] - n<T>(3,4)*z[34];
    z[14]= - z[7]*z[14];
    z[14]= - static_cast<T>(7)+ z[14];
    z[14]=z[14]*z[30];
    z[14]=3*z[8] + z[14];
    z[14]=n<T>(1,4)*z[14] - z[32];
    z[14]=z[2]*z[14];
    z[13]=n<T>(1,4)*z[13] + z[14];
    z[13]=z[4]*z[13];
    z[12]=z[13] + n<T>(1,4)*z[12] + z[17];
    z[12]=z[4]*z[12];
    z[13]=z[7] + 1;
    z[14]=z[13]*z[30];
    z[14]=z[14] - static_cast<T>(1)- n<T>(5,2)*z[7];
    z[14]=z[14]*z[30];
    z[15]= - z[9]*z[8];
    z[14]=z[14] + z[15] + n<T>(1,4) + z[16];
    z[15]=static_cast<T>(7)- z[18];
    z[15]=z[6]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[20];
    z[15]=z[8] + z[15];
    z[15]=n<T>(1,4)*z[15] - z[32];
    z[15]=z[2]*z[15];
    z[14]=n<T>(1,4)*z[14] + z[15];
    z[14]=z[2]*z[14];
    z[15]=z[13]*z[24];
    z[14]=n<T>(1,16)*z[15] + z[14];
    z[14]=z[2]*z[14];
    z[11]= - z[6]*z[13]*z[11];
    z[13]=z[9]*z[5];
    z[11]=z[13] + z[11];
    z[11]=z[11]*z[36];

    r += z[10] + z[11] + z[12] + z[14];
 
    return r;
}

template double qg_2lha_r1998(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1998(const std::array<dd_real,30>&);
#endif
