#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2270(const std::array<T,30>& k) {
  T z[34];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[12];
    z[6]=k[2];
    z[7]=k[7];
    z[8]=k[11];
    z[9]=k[21];
    z[10]=k[10];
    z[11]=k[18];
    z[12]=k[9];
    z[13]=k[14];
    z[14]=k[4];
    z[15]=n<T>(3,2)*z[3];
    z[16]=n<T>(1,2)*z[14];
    z[17]=3*z[9];
    z[18]= - z[17] - z[15] + static_cast<T>(5)+ z[16];
    z[18]=z[9]*z[18];
    z[19]= - z[13] - z[5];
    z[19]=3*z[19] - z[7];
    z[19]=z[19]*z[16];
    z[20]=n<T>(3,2)*z[9];
    z[21]=n<T>(1,2)*z[7];
    z[19]= - z[20] + z[19] + z[5] + z[21];
    z[19]=z[8]*z[19];
    z[22]=3*z[5];
    z[23]=3*z[13];
    z[24]=z[23] + n<T>(5,2)*z[5];
    z[24]=z[14]*z[24];
    z[18]=z[19] + n<T>(1,2)*z[18] + z[24] - z[21] - n<T>(7,2) - z[22];
    z[19]=n<T>(1,2)*z[8];
    z[18]=z[18]*z[19];
    z[24]=n<T>(3,4)*z[9];
    z[25]=z[3]*npow(z[2],2);
    z[26]=static_cast<T>(1)- z[25];
    z[26]=z[26]*z[24];
    z[27]=z[14]*z[7];
    z[28]=static_cast<T>(1)+ z[27];
    z[25]=n<T>(3,8)*z[25] + n<T>(3,8) + 2*z[2];
    z[25]=z[3]*z[25];
    z[25]=z[26] + n<T>(1,8)*z[28] + z[25];
    z[25]=z[9]*z[25];
    z[26]=n<T>(1,4)*z[7];
    z[28]= - z[10]*z[26];
    z[28]= - z[5] + z[28];
    z[16]=z[28]*z[16];
    z[28]=n<T>(1,2)*z[11];
    z[29]= - n<T>(7,3)*z[10] - static_cast<T>(3)+ z[28];
    z[30]=z[2] - z[1];
    z[31]= - n<T>(7,2) - z[30];
    z[31]=z[3]*z[31];
    z[29]=n<T>(1,12)*z[31] + n<T>(1,2)*z[29];
    z[29]=z[2]*z[29];
    z[31]=n<T>(1,2)*z[1];
    z[29]= - n<T>(2,3) + z[31] + z[29];
    z[29]=z[3]*z[29];
    z[28]=n<T>(3,2)*z[13] - n<T>(31,12)*z[10] + static_cast<T>(1)+ z[28];
    z[31]=static_cast<T>(1)- z[31];
    z[31]=z[5]*z[31];
    z[32]=z[23] - z[11];
    z[33]=z[2]*z[32];
    z[16]=z[18] + z[25] + z[29] + z[16] + n<T>(1,4)*z[33] + n<T>(1,2)*z[28] + 
    z[31];
    z[16]=z[4]*z[16];
    z[18]=z[2] + 1;
    z[15]=z[18]*z[15];
    z[15]=static_cast<T>(5)+ z[15];
    z[15]=z[3]*z[15];
    z[15]= - n<T>(1,2)*z[27] + z[15];
    z[25]=n<T>(1,2)*z[3];
    z[18]= - z[18]*z[25];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[18];
    z[15]=z[9]*z[15];
    z[18]=z[10]*z[27];
    z[28]= - static_cast<T>(3)+ n<T>(7,3)*z[30];
    z[28]=z[3]*z[28];
    z[28]= - static_cast<T>(3)+ z[28];
    z[28]=z[3]*z[28];
    z[18]=z[18] + z[28];
    z[28]=n<T>(19,6)*z[10] + z[32];
    z[15]=z[15] + n<T>(1,2)*z[28] - z[5] + n<T>(1,4)*z[18];
    z[18]= - 3*z[7] - z[22] + static_cast<T>(11)- z[23];
    z[22]=z[3] + 3;
    z[20]=z[22]*z[20];
    z[18]=z[20] + n<T>(1,2)*z[18] + z[27];
    z[18]=z[18]*z[19];
    z[19]=z[22]*z[17];
    z[20]= - static_cast<T>(7)- 3*z[3];
    z[20]=z[3]*z[20];
    z[20]=z[20] - static_cast<T>(21)- z[14];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[20]=n<T>(1,4)*z[9];
    z[19]=z[19]*z[20];
    z[18]=z[18] + z[19] + z[21] - static_cast<T>(1)+ n<T>(5,4)*z[5];
    z[18]=z[8]*z[18];
    z[15]=z[16] + n<T>(1,2)*z[15] + z[18];
    z[15]=z[4]*z[15];
    z[16]=z[14]*z[12];
    z[18]=3*z[12];
    z[19]=z[18] + z[16] - 1;
    z[19]=z[19]*z[14];
    z[21]=z[3]*z[12];
    z[19]=z[19] + z[21] + z[18];
    z[17]= - z[19]*z[17];
    z[21]=z[3]*z[18];
    z[21]=7*z[12] + z[21];
    z[21]=z[21]*z[25];
    z[22]=n<T>(1,2) + z[12];
    z[16]=z[17] + z[21] + 3*z[22] + z[16];
    z[16]=z[16]*z[20];
    z[17]= - z[19]*z[24];
    z[19]=static_cast<T>(1)+ n<T>(1,2)*z[6];
    z[19]=z[7]*z[19];
    z[19]= - n<T>(1,2) + z[19];
    z[20]= - n<T>(5,4) - z[2];
    z[20]=z[12]*z[20];
    z[18]= - z[7] - z[18];
    z[18]=z[14]*z[18];
    z[17]=z[17] + n<T>(1,4)*z[18] + n<T>(1,2)*z[19] + z[20];
    z[17]=z[8]*z[17];
    z[16]=z[17] + z[16] - z[26] + z[12];
    z[16]=z[8]*z[16];

    r += z[15] + z[16];
 
    return r;
}

template double qg_2lha_r2270(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2270(const std::array<dd_real,30>&);
#endif
