#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r572(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[28];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[11];
    z[8]=k[24];
    z[9]=k[21];
    z[10]=k[9];
    z[11]=k[27];
    z[12]=k[18];
    z[13]=z[1] - 1;
    z[14]=3*z[4];
    z[13]=z[13]*z[14];
    z[15]=3*z[9];
    z[16]=z[13] + n<T>(17,6) + z[15];
    z[17]=n<T>(1,4)*z[7];
    z[18]= - z[17] - n<T>(1,3)*z[8];
    z[18]=z[2]*z[18];
    z[16]=n<T>(1,2)*z[16] + 5*z[18];
    z[16]=z[2]*z[16];
    z[16]=z[16] - z[13] - n<T>(7,3) + n<T>(5,4)*z[1];
    z[16]=z[2]*z[16];
    z[18]= - z[9] + z[4];
    z[18]=z[2]*z[18];
    z[18]=n<T>(3,2)*z[18] + n<T>(7,2) - z[14];
    z[18]=z[2]*z[18];
    z[19]= - n<T>(25,6) + z[14];
    z[20]=static_cast<T>(1)- z[2];
    z[20]=z[10]*z[20];
    z[18]=n<T>(7,6)*z[20] + n<T>(1,2)*z[19] + z[18];
    z[18]=z[10]*z[18];
    z[19]=n<T>(1,4)*z[1];
    z[20]= - z[6]*z[19];
    z[21]=z[5]*z[11];
    z[20]= - n<T>(1,3)*z[21] + n<T>(1,3) + z[20];
    z[20]=z[5]*z[20];
    z[19]=z[20] - n<T>(1,3) + z[19];
    z[20]= - z[15] + n<T>(5,2)*z[7];
    z[21]=z[2]*z[20];
    z[21]=n<T>(5,3) + n<T>(1,2)*z[21];
    z[21]=z[2]*z[21];
    z[22]=static_cast<T>(1)+ n<T>(7,3)*z[10];
    z[22]=z[10]*z[22];
    z[19]=n<T>(1,2)*z[22] + z[21] + 5*z[19];
    z[19]=z[5]*z[19];
    z[21]=n<T>(11,3) - 5*z[1];
    z[13]=n<T>(1,2)*z[21] + z[13];
    z[13]=z[19] + z[18] + n<T>(1,2)*z[13] + z[16];
    z[13]=z[3]*z[13];
    z[15]= - 5*z[7] + z[15] - z[14];
    z[16]=n<T>(5,3)*z[8];
    z[15]=n<T>(1,2)*z[15] - z[16];
    z[15]=z[2]*z[15];
    z[15]= - z[16] + z[15];
    z[15]=z[2]*z[15];
    z[18]=static_cast<T>(1)+ z[14];
    z[13]=z[13] + z[15] + n<T>(1,2)*z[18] - z[16];
    z[15]=z[16] + z[20];
    z[15]=z[2]*z[15];
    z[15]=z[15] - n<T>(5,3) + z[12];
    z[18]=n<T>(1,2)*z[5];
    z[19]= - z[11] - z[8];
    z[19]=z[19]*z[18];
    z[21]=n<T>(1,2)*z[8];
    z[22]=z[21] + z[11];
    z[19]=z[19] - z[22];
    z[19]=z[10]*z[19];
    z[19]=z[19] + z[22];
    z[19]=z[5]*z[19];
    z[22]=n<T>(1,2) + 5*z[8];
    z[22]=n<T>(1,2)*z[22] - z[10];
    z[22]=z[10]*z[22];
    z[15]=n<T>(5,3)*z[19] + n<T>(1,2)*z[15] + n<T>(1,3)*z[22];
    z[15]=z[15]*z[18];
    z[16]=z[16] + n<T>(5,6) - z[14];
    z[19]=n<T>(2,3) + n<T>(3,4)*z[4];
    z[19]=z[2]*z[19];
    z[16]= - n<T>(11,48)*z[10] + n<T>(1,4)*z[16] + z[19];
    z[16]=z[10]*z[16];
    z[13]=z[15] + z[16] + n<T>(1,4)*z[13];
    z[13]=z[3]*z[13];
    z[15]=n<T>(1,4)*z[10];
    z[16]= - z[11] + z[8];
    z[15]=z[16]*z[15];
    z[16]=n<T>(5,3) - n<T>(1,4)*z[11];
    z[16]=z[11]*z[16];
    z[19]=n<T>(1,4)*z[8];
    z[22]= - n<T>(5,3) + z[19];
    z[22]=z[8]*z[22];
    z[15]=z[15] + z[16] + z[22];
    z[15]=z[10]*z[15];
    z[16]=z[7]*npow(z[11],2);
    z[22]= - z[6]*npow(z[8],2);
    z[16]=z[16] + z[22];
    z[15]=n<T>(1,4)*z[16] + z[15];
    z[15]=z[15]*z[18];
    z[16]= - n<T>(5,3) - z[17];
    z[16]=z[11]*z[16];
    z[17]= - z[21] + n<T>(1,4);
    z[17]=z[6]*z[17];
    z[17]=n<T>(5,3) + z[17];
    z[17]=z[8]*z[17];
    z[16]=z[17] + z[16] + n<T>(5,4)*z[7] - z[12] - n<T>(3,2)*z[9];
    z[17]=z[10] - n<T>(23,6) + z[8];
    z[17]=z[19]*z[17];
    z[18]= - static_cast<T>(1)+ n<T>(23,8)*z[11];
    z[17]=n<T>(1,3)*z[18] + z[17];
    z[17]=z[10]*z[17];
    z[15]=z[15] + n<T>(1,2)*z[16] + z[17];
    z[15]=z[5]*z[15];
    z[16]=z[8] - 1;
    z[17]= - z[21]*z[6]*z[16];
    z[14]= - z[14] - z[20];
    z[14]=z[2]*z[14];
    z[14]=z[17] + z[14];
    z[16]=z[10] + z[16];
    z[16]=z[19]*z[16];
    z[16]=static_cast<T>(1)+ n<T>(3,2)*z[4] + z[16];
    z[16]=z[10]*z[16];
    z[14]=n<T>(1,2)*z[14] + z[16];
    z[14]=n<T>(1,2)*z[14] + z[15];

    r += z[13] + n<T>(1,2)*z[14];
 
    return r;
}

template double qg_2lha_r572(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r572(const std::array<dd_real,30>&);
#endif
