#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r318(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[3];
    z[2]=k[6];
    z[3]=k[9];
    z[4]=k[12];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[4];
    z[8]=k[5];
    z[9]=k[11];
    z[10]=k[13];
    z[11]=k[17];
    z[12]=k[7];
    z[13]=k[14];
    z[14]=n<T>(1,3)*z[10];
    z[15]=z[7]*z[8];
    z[16]= - z[14] + n<T>(5,16)*z[15];
    z[17]=n<T>(1,4)*z[8];
    z[18]=n<T>(3,4) + z[11];
    z[18]= - z[17] + n<T>(1,4)*z[18] - z[16];
    z[18]=z[7]*z[18];
    z[19]=z[8] - n<T>(1,2);
    z[20]=n<T>(1,2)*z[15] + z[19];
    z[20]=z[7]*z[20];
    z[20]=z[20] - static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[20]=z[7]*z[20];
    z[20]= - n<T>(1,2) + z[20];
    z[20]=z[2]*z[20];
    z[21]=n<T>(1,16)*z[8];
    z[22]= - n<T>(17,6) + z[11];
    z[14]=n<T>(1,8)*z[20] + z[18] + n<T>(1,4)*z[22] + z[14] + z[21];
    z[14]=z[2]*z[14];
    z[18]=n<T>(1,2)*z[13];
    z[20]=n<T>(1,2)*z[3];
    z[22]= - z[20] + static_cast<T>(3)- z[18];
    z[22]=z[3]*z[22];
    z[23]=n<T>(1,6)*z[2];
    z[24]=3*z[13] - n<T>(49,3) - z[9];
    z[25]=z[7]*z[13];
    z[22]=z[23] + z[22] + n<T>(1,2)*z[24] + z[25];
    z[24]=7*z[6];
    z[25]= - static_cast<T>(5)+ z[3];
    z[25]=z[3]*z[25];
    z[25]=z[25] + n<T>(43,3) + z[24];
    z[26]=n<T>(1,3)*z[2];
    z[27]= - n<T>(5,2) - z[2];
    z[27]=z[27]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[27];
    z[27]= - n<T>(5,4) + z[2];
    z[27]=z[27]*z[26];
    z[28]=z[6] + n<T>(1,2);
    z[29]=3*z[6];
    z[30]=z[28]*z[29];
    z[31]=z[26] - n<T>(3,2);
    z[31]=z[31]*z[2];
    z[32]=z[30] - z[31];
    z[33]=n<T>(1,2)*z[1];
    z[32]=z[32]*z[33];
    z[34]= - static_cast<T>(5)- z[29];
    z[34]=z[6]*z[34];
    z[27]=z[32] + n<T>(1,2)*z[34] + z[27];
    z[27]=z[1]*z[27];
    z[25]=n<T>(1,2)*z[25] + z[27];
    z[25]=z[1]*z[25];
    z[22]=n<T>(1,2)*z[22] + z[25];
    z[25]=n<T>(1,2)*z[9];
    z[27]=z[25] + z[18];
    z[32]=n<T>(17,3) + z[27];
    z[34]=n<T>(1,2)*z[7];
    z[32]=z[32]*z[34];
    z[35]=n<T>(1,4)*z[3];
    z[27]=n<T>(5,12)*z[2] + z[35] + z[32] - n<T>(5,3) + z[27];
    z[32]=n<T>(3,2)*z[6];
    z[36]= - n<T>(11,12)*z[2] + z[35] - n<T>(11,3) - z[32];
    z[37]=n<T>(3,4)*z[6];
    z[38]=z[37] + z[26];
    z[38]=z[1]*z[38];
    z[36]=n<T>(1,2)*z[36] + z[38];
    z[36]=z[1]*z[36];
    z[38]=static_cast<T>(29)- 17*z[7];
    z[38]= - z[26] + n<T>(1,3)*z[38] - z[3];
    z[36]=n<T>(1,4)*z[38] + z[36];
    z[36]=z[1]*z[36];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[7]*z[34];
    z[38]=z[33] - n<T>(1,2) + z[7];
    z[38]=z[1]*z[38];
    z[34]=z[34] + z[38];
    z[34]=z[1]*z[34];
    z[38]=npow(z[7],2);
    z[34]= - n<T>(1,2)*z[38] + z[34];
    z[34]=z[4]*z[34];
    z[27]=n<T>(5,6)*z[34] + n<T>(1,2)*z[27] + z[36];
    z[27]=z[4]*z[27];
    z[22]=n<T>(1,2)*z[22] + z[27];
    z[22]=z[4]*z[22];
    z[27]=static_cast<T>(3)+ 7*z[12];
    z[27]=n<T>(1,2)*z[27] - z[29];
    z[27]=z[6]*z[27];
    z[34]=z[10]*z[12];
    z[36]=n<T>(1,4)*z[9];
    z[38]=static_cast<T>(1)- z[36];
    z[38]=z[8]*z[38];
    z[27]=z[27] + z[18] + z[38] + z[11] - n<T>(7,2)*z[34];
    z[27]=z[7]*z[27];
    z[34]= - static_cast<T>(1)- z[25];
    z[34]=z[8]*z[34];
    z[25]=z[13] + z[34] + z[25] - static_cast<T>(3)- n<T>(53,6)*z[10];
    z[24]=z[27] + n<T>(1,2)*z[25] + z[24];
    z[25]= - static_cast<T>(1)+ n<T>(1,4)*z[13];
    z[27]=z[25] + z[35];
    z[17]=z[17] - z[27];
    z[17]=z[3]*z[17];
    z[17]=n<T>(1,2)*z[24] + z[17];
    z[24]=z[2] - 1;
    z[34]=n<T>(1,2)*z[2];
    z[24]=z[24]*z[34];
    z[28]=z[28]*z[6];
    z[24]=z[24] - z[28];
    z[24]=z[24]*z[1];
    z[28]=z[7]*z[30];
    z[38]= - static_cast<T>(5)+ z[29];
    z[38]=z[6]*z[38];
    z[28]=z[38] + z[28];
    z[38]= - static_cast<T>(5)+ n<T>(3,2)*z[7];
    z[39]=static_cast<T>(1)- n<T>(3,4)*z[7];
    z[39]=z[2]*z[39];
    z[38]=n<T>(1,2)*z[38] + z[39];
    z[38]=z[2]*z[38];
    z[28]= - n<T>(3,2)*z[24] + n<T>(1,2)*z[28] + z[38];
    z[28]=z[1]*z[28];
    z[38]=n<T>(13,4)*z[7];
    z[39]= - z[38] - n<T>(7,4) - z[29];
    z[39]=z[6]*z[39];
    z[40]=static_cast<T>(1)+ 25*z[7];
    z[40]=z[2]*z[40];
    z[38]=n<T>(1,24)*z[40] + n<T>(1,3) - z[38];
    z[38]=z[2]*z[38];
    z[28]=z[28] + z[38] + z[39];
    z[28]=z[28]*z[33];
    z[14]=z[22] + z[28] + n<T>(1,2)*z[17] + z[14];
    z[14]=z[5]*z[14];
    z[17]=static_cast<T>(1)+ z[3];
    z[17]=z[17]*z[35];
    z[22]=n<T>(23,2) - z[2];
    z[22]=z[22]*z[23];
    z[17]=z[22] + z[17] + z[36] + z[25];
    z[22]=z[3] - 3;
    z[25]= - z[22]*z[20];
    z[28]=n<T>(47,2) - 5*z[2];
    z[28]=z[28]*z[26];
    z[25]=z[28] + z[29] + z[25];
    z[28]= - z[32] + z[31];
    z[28]=z[1]*z[28];
    z[25]=n<T>(1,2)*z[25] + z[28];
    z[25]=z[25]*z[33];
    z[28]= - n<T>(13,2) + z[2];
    z[28]=z[28]*z[26];
    z[29]=z[3] - 1;
    z[25]=z[25] - n<T>(1,2)*z[29] + z[28];
    z[25]=z[1]*z[25];
    z[28]= - n<T>(5,3)*z[1] + n<T>(17,6);
    z[28]=z[2]*z[28];
    z[28]= - n<T>(5,3) + z[20] + z[28];
    z[28]=z[28]*z[33];
    z[31]=n<T>(5,3) - z[3];
    z[28]=z[28] + n<T>(1,2)*z[31] - z[26];
    z[28]=z[1]*z[28];
    z[31]=z[3] - z[2];
    z[28]=n<T>(1,4)*z[31] + z[28];
    z[28]=z[4]*z[28];
    z[17]=z[28] + n<T>(1,2)*z[17] + z[25];
    z[17]=z[4]*z[17];
    z[25]=z[37] + 2;
    z[25]=z[25]*z[6];
    z[28]=static_cast<T>(2)- n<T>(17,12)*z[2];
    z[28]=z[2]*z[28];
    z[24]=n<T>(3,4)*z[24] + z[25] + z[28];
    z[24]=z[1]*z[24];
    z[28]= - static_cast<T>(31)+ n<T>(41,2)*z[2];
    z[28]=z[28]*z[26];
    z[31]=z[3]*z[22];
    z[28]=z[28] - n<T>(13,2)*z[6] + z[31];
    z[24]=n<T>(1,4)*z[28] + z[24];
    z[24]=z[1]*z[24];
    z[27]= - z[3]*z[27];
    z[28]=n<T>(23,8) - 2*z[2];
    z[28]=z[28]*z[26];
    z[17]=z[17] + z[24] + z[27] + z[28];
    z[17]=z[4]*z[17];
    z[19]=z[15] + z[19];
    z[19]=z[7]*z[19];
    z[19]=static_cast<T>(1)+ n<T>(1,8)*z[19];
    z[19]=z[2]*z[19];
    z[16]=z[19] + z[21] - n<T>(5,3) + n<T>(1,4)*z[11] - z[16];
    z[16]=z[2]*z[16];
    z[19]= - z[9]*z[21];
    z[21]= - z[13] + z[8] - z[22];
    z[21]=z[3]*z[21];
    z[22]=z[1]*npow(z[2],2);
    z[14]=z[14] + z[17] - n<T>(11,12)*z[22] + z[16] + n<T>(1,8)*z[21] - n<T>(2,3)*
    z[10] + z[19];
    z[14]=z[5]*z[14];
    z[16]=n<T>(1,3)*z[3];
    z[17]=n<T>(31,4) + 2*z[3];
    z[17]=z[17]*z[16];
    z[19]=z[2]*z[3];
    z[21]=n<T>(1,4)*z[19];
    z[22]= - n<T>(41,6) - z[3];
    z[22]=z[22]*z[21];
    z[17]=z[17] + z[22];
    z[17]=z[2]*z[17];
    z[22]=z[3]*z[30];
    z[24]= - n<T>(3,2) - z[16];
    z[24]=z[24]*z[19];
    z[24]=n<T>(3,2)*z[3] + z[24];
    z[24]=z[2]*z[24];
    z[22]=z[22] + z[24];
    z[22]=z[1]*z[22];
    z[24]=n<T>(17,3) + z[3];
    z[24]=z[24]*z[21];
    z[27]= - static_cast<T>(2)- z[16];
    z[27]=z[3]*z[27];
    z[24]=z[27] + z[24];
    z[24]=z[2]*z[24];
    z[25]= - z[3]*z[25];
    z[22]=n<T>(1,4)*z[22] + z[25] + z[24];
    z[22]=z[1]*z[22];
    z[24]=n<T>(13,4)*z[6] + z[16];
    z[24]=z[24]*z[20];
    z[17]=z[22] + z[24] + z[17];
    z[17]=z[1]*z[17];
    z[22]= - static_cast<T>(47)- 13*z[3];
    z[22]=z[22]*z[20];
    z[22]=z[22] + 5*z[19];
    z[22]=z[22]*z[23];
    z[23]= - z[32] - z[3];
    z[23]=z[3]*z[23];
    z[22]=z[23] + z[22];
    z[16]=n<T>(3,4) + z[16];
    z[16]=z[3]*z[16];
    z[16]=z[16] - n<T>(1,6)*z[19];
    z[16]=z[2]*z[16];
    z[23]=z[3]*z[37];
    z[16]=z[23] + z[16];
    z[16]=z[1]*z[16];
    z[16]=n<T>(1,2)*z[22] + z[16];
    z[16]=z[1]*z[16];
    z[22]=z[29]*z[20];
    z[23]=static_cast<T>(13)+ z[20];
    z[23]=z[23]*z[20];
    z[23]=z[23] - z[19];
    z[23]=z[23]*z[26];
    z[16]=z[16] + z[22] + z[23];
    z[16]=z[1]*z[16];
    z[22]= - n<T>(23,3) + z[3];
    z[22]=z[22]*z[20];
    z[22]=z[22] + n<T>(1,3)*z[19];
    z[22]=z[22]*z[34];
    z[22]=z[3] + z[22];
    z[23]=npow(z[3],2);
    z[24]=n<T>(1,2)*z[23];
    z[25]=z[24] + z[19];
    z[25]=z[1]*z[25];
    z[26]=static_cast<T>(1)- z[20];
    z[26]=z[3]*z[26];
    z[25]=z[26] + z[25];
    z[25]= - n<T>(17,2)*z[19] + 5*z[25];
    z[25]=z[25]*z[33];
    z[25]=z[25] - n<T>(5,2)*z[3] + z[19];
    z[26]=n<T>(1,3)*z[1];
    z[25]=z[25]*z[26];
    z[21]=z[21] + z[25];
    z[21]=z[4]*z[21];
    z[16]=z[21] + n<T>(1,2)*z[22] + z[16];
    z[16]=z[4]*z[16];
    z[21]=static_cast<T>(2)+ z[35];
    z[21]=z[21]*z[19];
    z[22]= - n<T>(23,8) - z[3];
    z[22]=z[3]*z[22];
    z[21]=z[22] + z[21];
    z[21]=z[2]*z[21];
    z[21]= - z[24] + z[21];
    z[16]=z[16] + n<T>(1,3)*z[21] + z[17];
    z[16]=z[4]*z[16];
    z[17]=z[13] - 1;
    z[21]=z[12]*z[17];
    z[22]=z[21]*z[18];
    z[24]=z[13]*z[17];
    z[24]=static_cast<T>(1)+ z[24];
    z[25]= - static_cast<T>(1)+ z[18];
    z[25]=z[3]*z[25];
    z[24]=n<T>(1,2)*z[24] + z[25];
    z[24]=z[3]*z[24];
    z[22]=z[22] + z[24];
    z[17]= - z[17]*z[18];
    z[17]= - static_cast<T>(1)+ z[17];
    z[18]=n<T>(1,3)*z[13];
    z[24]=static_cast<T>(1)- z[18];
    z[20]=z[24]*z[20];
    z[17]=n<T>(1,3)*z[17] + z[20];
    z[17]=z[3]*z[17];
    z[18]= - z[21]*z[18];
    z[15]=z[18] + n<T>(1,8)*z[15];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[2]*z[15];
    z[15]=n<T>(1,3)*z[22] + z[15];
    z[15]=z[2]*z[15];
    z[17]=n<T>(1,3) - z[3];
    z[17]=z[17]*z[19];
    z[17]=n<T>(1,3)*z[23] + z[17];
    z[17]=z[2]*z[17];
    z[18]=npow(z[19],2)*z[26];
    z[17]=z[17] + z[18];
    z[17]=z[17]*z[33];

    r += z[14] + z[15] + z[16] + z[17];
 
    return r;
}

template double qg_2lha_r318(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r318(const std::array<dd_real,30>&);
#endif
