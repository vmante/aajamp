#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r68(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[10];
    z[6]=k[9];
    z[7]=z[1] - 1;
    z[8]=z[4]*z[5];
    z[9]=z[8] + z[7];
    z[9]=z[2]*z[9];
    z[9]=n<T>(1,2)*z[9] - n<T>(1,2) - z[8];
    z[9]=z[2]*z[9];
    z[10]=z[8] + 1;
    z[10]=z[10]*z[4];
    z[7]= - z[1]*z[7];
    z[7]=z[7] - z[10];
    z[7]=z[2]*z[7];
    z[7]=z[7] + z[1] + z[10];
    z[7]=z[2]*z[7];
    z[10]=z[5]*z[6];
    z[11]= - npow(z[4],2)*z[10];
    z[7]=z[11] + z[7];
    z[7]=z[3]*z[7];
    z[8]=z[6]*z[8];
    z[7]=z[7] + z[8] + z[9];
    z[7]=z[3]*z[7];
    z[8]=z[2]*z[5];
    z[8]= - z[10] + z[8];
    z[7]=n<T>(1,2)*z[8] + z[7];

    r += n<T>(1,4)*z[7]*z[3];
 
    return r;
}

template double qg_2lha_r68(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r68(const std::array<dd_real,30>&);
#endif
