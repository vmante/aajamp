#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r363(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[9];
    z[6]=k[7];
    z[7]=k[11];
    z[8]=n<T>(1,2)*z[2];
    z[9]=z[2]*z[6];
    z[10]=z[7] + z[9];
    z[10]=z[10]*z[8];
    z[11]=n<T>(1,2)*z[7];
    z[12]=static_cast<T>(1)+ n<T>(1,2)*z[9];
    z[12]=z[2]*z[12];
    z[12]=z[11] + z[12];
    z[13]=z[3]*z[2];
    z[12]=z[12]*z[13];
    z[10]=z[12] + z[10] - static_cast<T>(1)+ z[5];
    z[10]=z[3]*z[10];
    z[12]=n<T>(1,2)*z[6];
    z[12]= - npow(z[2],2)*z[12];
    z[12]=z[12] + z[1] - z[11];
    z[12]=z[2]*z[12];
    z[9]= - z[9] - static_cast<T>(3)- z[6];
    z[8]=z[9]*z[8];
    z[9]=z[11] + z[1];
    z[8]=z[8] - static_cast<T>(1)- z[9];
    z[8]=z[2]*z[8];
    z[8]=z[8] + n<T>(1,2) - z[9];
    z[8]=z[8]*z[13];
    z[8]=z[12] + z[8];
    z[8]=z[3]*z[8];
    z[9]= - z[2]*z[1]*z[5];
    z[8]=z[9] + z[8];
    z[8]=z[4]*z[8];
    z[8]=z[8] + z[5] + z[10];

    r += n<T>(1,8)*z[8]*z[4];
 
    return r;
}

template double qg_2lha_r363(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r363(const std::array<dd_real,30>&);
#endif
