#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2340(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[14];
    z[7]=k[13];
    z[8]=npow(z[1],2);
    z[9]=n<T>(1,2)*z[8];
    z[10]=npow(z[1],3);
    z[11]=n<T>(1,3)*z[10];
    z[12]= - z[2]*z[11];
    z[12]= - z[9] + z[12];
    z[12]=z[2]*z[12];
    z[13]=n<T>(1,3)*z[7];
    z[14]=z[13] - n<T>(1,2);
    z[14]=z[14]*z[7];
    z[15]=z[4]*z[6];
    z[16]=n<T>(1,2)*z[15];
    z[17]=n<T>(1,2)*z[6];
    z[18]=n<T>(1,2)*z[1];
    z[12]=z[12] + z[16] - z[18] + z[17] - n<T>(1,2) - z[14];
    z[12]=z[2]*z[12];
    z[19]=z[5]*z[6];
    z[20]=z[4]*z[5];
    z[21]=z[6]*z[20];
    z[21]=z[19] + z[21];
    z[12]=n<T>(1,2)*z[21] + z[12];
    z[21]=n<T>(1,3)*z[1];
    z[22]=static_cast<T>(19)+ 23*z[1];
    z[22]=z[22]*z[21];
    z[22]=static_cast<T>(5)+ z[22];
    z[22]=z[1]*z[22];
    z[23]= - n<T>(7,4) + z[7];
    z[23]=z[7]*z[23];
    z[23]=n<T>(11,4) + z[23];
    z[24]= - n<T>(2,3) - z[18];
    z[24]=z[1]*z[24];
    z[24]= - n<T>(1,2) + z[24];
    z[24]=z[4]*z[24];
    z[22]=z[24] + n<T>(1,3)*z[23] + n<T>(1,4)*z[22];
    z[22]=z[4]*z[22];
    z[23]=z[1] + 1;
    z[24]=z[23]*z[1];
    z[25]=z[24] + 1;
    z[25]=z[25]*z[1];
    z[26]=z[25] + 1;
    z[26]=z[26]*z[1];
    z[27]=z[7] - 1;
    z[28]=z[26] - z[27];
    z[18]=z[18] + 1;
    z[18]=z[18]*z[21];
    z[18]=z[18] + n<T>(1,2);
    z[18]=z[18]*z[1];
    z[29]=static_cast<T>(5)- z[7];
    z[29]=z[7]*z[29];
    z[29]= - static_cast<T>(2)+ n<T>(1,2)*z[29];
    z[29]=n<T>(1,3)*z[29] - z[18];
    z[29]=z[4]*z[29];
    z[28]=n<T>(2,3)*z[28] + z[29];
    z[28]=z[4]*z[28];
    z[29]=npow(z[1],5);
    z[29]=n<T>(1,2)*z[29];
    z[28]= - z[29] + z[28];
    z[28]=z[2]*z[28];
    z[30]=npow(z[1],4);
    z[22]=z[28] - n<T>(5,4)*z[30] + z[22];
    z[22]=z[2]*z[22];
    z[23]=z[23] - z[6];
    z[15]=n<T>(1,3)*z[15] - z[23];
    z[28]=n<T>(1,2)*z[4];
    z[15]=z[15]*z[28];
    z[28]=z[6] + n<T>(1,2);
    z[31]=static_cast<T>(1)+ n<T>(7,4)*z[1];
    z[31]=z[1]*z[31];
    z[15]=z[15] + n<T>(1,2)*z[28] + z[31];
    z[15]=z[4]*z[15];
    z[31]= - z[7] + z[6];
    z[15]=z[22] + z[15] + n<T>(1,6)*z[31] - z[10];
    z[15]=z[2]*z[15];
    z[22]=n<T>(1,2) + n<T>(1,3)*z[6];
    z[22]=z[22]*z[20];
    z[31]=z[6] + 1;
    z[32]=z[5]*z[31];
    z[22]=z[22] - n<T>(1,2) + z[32];
    z[22]=z[4]*z[22];
    z[28]=z[28]*z[5];
    z[22]=z[22] + z[28] + z[1];
    z[22]=z[4]*z[22];
    z[9]=z[22] + n<T>(1,3)*z[19] - z[9];
    z[22]= - static_cast<T>(1)- z[26];
    z[22]=z[1]*z[22];
    z[22]=z[22] + z[27];
    z[18]=n<T>(2,3) + z[18];
    z[18]=z[1]*z[18];
    z[26]= - static_cast<T>(1)+ n<T>(1,6)*z[7];
    z[26]=z[7]*z[26];
    z[18]=z[18] + n<T>(5,6) + z[26];
    z[18]=z[4]*z[18];
    z[18]=n<T>(1,3)*z[22] + z[18];
    z[18]=z[4]*z[18];
    z[22]=npow(z[1],6);
    z[18]=n<T>(1,6)*z[22] + z[18];
    z[18]=z[2]*z[18];
    z[22]= - static_cast<T>(5)- 7*z[1];
    z[21]=z[22]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[1]*z[21];
    z[21]= - n<T>(1,3) + z[21];
    z[21]=z[1]*z[21];
    z[21]=z[21] + n<T>(1,3) - z[7];
    z[22]=static_cast<T>(1)+ n<T>(2,3)*z[1];
    z[22]=z[1]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[1]*z[22];
    z[22]=n<T>(2,3) + z[22];
    z[22]=z[4]*z[22];
    z[21]=n<T>(1,2)*z[21] + z[22];
    z[21]=z[4]*z[21];
    z[18]=z[18] + z[29] + z[21];
    z[18]=z[2]*z[18];
    z[21]= - n<T>(1,2) - n<T>(4,3)*z[1];
    z[21]=z[21]*z[8];
    z[22]=n<T>(1,2) + z[24];
    z[22]=z[4]*z[22];
    z[21]=z[22] + n<T>(1,6) + z[21];
    z[21]=z[4]*z[21];
    z[22]=n<T>(1,2)*z[30];
    z[18]=z[18] + z[22] + z[21];
    z[18]=z[2]*z[18];
    z[21]=n<T>(1,3)*z[20] - n<T>(1,3) + z[5];
    z[21]=z[4]*z[21];
    z[21]=z[21] + z[5] + z[1];
    z[21]=z[4]*z[21];
    z[21]=z[21] + n<T>(1,3)*z[5] - z[8];
    z[21]=z[4]*z[21];
    z[11]=z[11] + z[21];
    z[11]=n<T>(1,2)*z[11] + z[18];
    z[11]=z[3]*z[11];
    z[9]=z[11] + n<T>(1,2)*z[9] + z[15];
    z[9]=z[3]*z[9];
    z[11]=z[16] - z[23];
    z[11]=z[4]*z[11];
    z[8]=z[11] + n<T>(3,2)*z[8] + z[14] + z[17];
    z[11]=n<T>(3,4) - z[13];
    z[11]=z[7]*z[11];
    z[11]= - n<T>(3,4)*z[24] - n<T>(3,4) + z[11];
    z[11]=z[4]*z[11];
    z[13]= - z[25] + z[27];
    z[13]=z[4]*z[13];
    z[13]=z[22] + n<T>(1,3)*z[13];
    z[13]=z[2]*z[13];
    z[10]=z[13] + z[10] + z[11];
    z[10]=z[2]*z[10];
    z[8]=n<T>(1,2)*z[8] + z[10];
    z[8]=z[2]*z[8];
    z[10]=z[31]*z[20];
    z[10]=n<T>(1,2)*z[10] - n<T>(1,2) + z[28];
    z[10]=z[4]*z[10];
    z[11]=z[19] + z[1];
    z[10]=n<T>(1,2)*z[11] + z[10];
    z[8]=z[9] + n<T>(1,2)*z[10] + z[8];
    z[8]=z[3]*z[8];

    r += z[8] + n<T>(1,2)*z[12];
 
    return r;
}

template double qg_2lha_r2340(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2340(const std::array<dd_real,30>&);
#endif
