#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r87(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=k[2];
    z[7]=k[11];
    z[8]=k[9];
    z[9]=npow(z[8],2);
    z[10]=n<T>(1,2)*z[9];
    z[11]=npow(z[4],2);
    z[12]=z[10] + z[11];
    z[12]=z[12]*z[5];
    z[13]=z[8] - 1;
    z[14]=n<T>(1,2)*z[8];
    z[15]=z[13]*z[14];
    z[16]=static_cast<T>(1)- n<T>(3,2)*z[1];
    z[16]=z[16]*z[11];
    z[15]= - z[12] + z[15] + z[16];
    z[16]=z[14] - 1;
    z[17]=z[16]*z[14];
    z[18]=n<T>(3,2)*z[4];
    z[19]=static_cast<T>(1)- z[2];
    z[19]=z[19]*npow(z[1],2)*z[18];
    z[20]=z[1]*z[2];
    z[19]=z[19] - n<T>(3,2) + z[20];
    z[20]=n<T>(1,2)*z[4];
    z[19]=z[19]*z[20];
    z[21]=z[5] - 1;
    z[22]= - z[1] + z[21];
    z[22]=z[11]*z[22];
    z[23]=z[13]*z[8];
    z[22]= - z[23] + z[22];
    z[24]=n<T>(1,4)*z[5];
    z[22]=z[22]*z[24];
    z[17]=z[22] + z[19] + z[17] - n<T>(7,4) + z[2];
    z[17]=z[3]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[3]*z[15];
    z[17]=static_cast<T>(1)+ 5*z[4];
    z[17]=z[17]*z[20];
    z[19]=n<T>(3,2) + z[8];
    z[19]=z[8]*z[19];
    z[12]= - z[12] + z[19] + z[17];
    z[12]=z[12]*z[24];
    z[17]=n<T>(1,4)*z[8];
    z[19]= - static_cast<T>(3)- z[14];
    z[19]=z[19]*z[17];
    z[22]=n<T>(1,2) - z[4];
    z[22]=z[22]*z[20];
    z[12]=z[12] + z[22] + static_cast<T>(1)+ z[19];
    z[12]=z[5]*z[12];
    z[19]=z[4] - z[8];
    z[21]= - z[24]*z[19]*z[21];
    z[22]=n<T>(1,4)*z[4];
    z[21]=z[21] + z[22] - z[6] - z[17];
    z[24]=n<T>(1,2)*z[5];
    z[21]=z[21]*z[24];
    z[25]=n<T>(1,2) - z[6];
    z[25]=z[6]*z[25];
    z[21]=z[21] + z[25] - n<T>(1,8)*z[19];
    z[21]=z[3]*z[21];
    z[25]= - static_cast<T>(3)+ z[4];
    z[25]=z[25]*z[22];
    z[25]=z[25] + z[6] + n<T>(3,4)*z[8];
    z[12]=z[21] + n<T>(1,2)*z[25] + z[12];
    z[12]=z[3]*z[12];
    z[21]=z[4]*z[8];
    z[10]= - z[10] + z[21];
    z[10]=z[10]*z[5];
    z[25]=z[8] - n<T>(1,2);
    z[25]=z[25]*z[8];
    z[10]=z[10] + z[25] - n<T>(5,2)*z[21];
    z[10]=z[24]*z[4]*z[10];
    z[26]= - static_cast<T>(1)- z[14];
    z[26]=z[26]*z[14];
    z[26]=z[26] + z[21];
    z[26]=z[4]*z[26];
    z[10]=z[10] - z[8] + z[26];
    z[10]=z[5]*z[10];
    z[26]=3*z[8] - z[21];
    z[26]=z[26]*z[22];
    z[10]=z[26] + z[10];
    z[14]=z[14]*z[4];
    z[26]= - z[5]*z[14];
    z[26]=z[21] + z[26];
    z[26]=z[5]*z[26];
    z[27]=z[24] - 1;
    z[28]=z[5]*z[27];
    z[28]=n<T>(1,2) + z[28];
    z[19]=z[3]*z[19]*z[28];
    z[19]=z[19] - z[14] + z[26];
    z[19]=z[7]*z[5]*z[19];
    z[10]=z[19] + n<T>(1,2)*z[10] + z[12];
    z[10]=z[7]*z[10];
    z[12]= - static_cast<T>(1)+ 3*z[4];
    z[12]=z[12]*z[20];
    z[19]=z[9] - n<T>(3,2)*z[11];
    z[19]=z[19]*z[24];
    z[12]=z[19] - z[25] + z[12];
    z[12]=z[5]*z[12];
    z[18]=static_cast<T>(1)- z[18];
    z[18]=z[4]*z[18];
    z[18]=z[18] - static_cast<T>(1)+ z[23];
    z[12]=n<T>(1,2)*z[18] + z[12];
    z[11]=z[11]*z[27];
    z[18]=z[16]*z[8];
    z[11]=z[18] + z[11];
    z[11]=z[11]*z[24];
    z[19]= - static_cast<T>(1)+ z[22];
    z[19]=z[4]*z[19];
    z[11]=z[11] + z[19] + n<T>(1,2) - z[18];
    z[11]=z[11]*z[24];
    z[17]=z[16]*z[17];
    z[11]=z[11] + z[20] + z[17] - n<T>(5,4) + 2*z[6];
    z[11]=z[3]*z[11];
    z[11]=n<T>(1,2)*z[12] + z[11];
    z[11]=z[3]*z[11];
    z[12]=n<T>(3,2)*z[21];
    z[13]= - z[13]*z[12];
    z[9]=z[9] + z[13];
    z[9]=z[4]*z[9];
    z[13]=npow(z[21],2)*z[24];
    z[9]=z[9] + z[13];
    z[9]=z[9]*z[24];
    z[12]=z[16]*z[12];
    z[12]= - z[25] + z[12];
    z[12]=z[4]*z[12];
    z[9]=z[12] + z[9];
    z[9]=z[5]*z[9];
    z[12]=static_cast<T>(3)- z[8];
    z[12]=z[12]*z[14];
    z[12]=z[23] + z[12];
    z[12]=z[4]*z[12];
    z[12]= - z[8] + z[12];
    z[9]=n<T>(1,2)*z[12] + z[9];
    z[9]=z[10] + n<T>(1,2)*z[9] + z[11];
    z[9]=z[7]*z[9];

    r += z[9] + n<T>(1,2)*z[15];
 
    return r;
}

template double qg_2lha_r87(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r87(const std::array<dd_real,30>&);
#endif
