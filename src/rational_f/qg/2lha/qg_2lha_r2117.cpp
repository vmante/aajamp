#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2117(const std::array<T,30>& k) {
  T z[7];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[3];
    z[5]= - z[4] + z[1];
    z[5]=z[3]*z[5];
    z[6]= - z[3]*z[1];
    z[6]=z[6] + static_cast<T>(1)- z[4];
    z[6]=z[2]*z[6];
    z[5]=z[6] - n<T>(3,2) + z[5];

    r += z[5]*z[3]*z[2];
 
    return r;
}

template double qg_2lha_r2117(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2117(const std::array<dd_real,30>&);
#endif
