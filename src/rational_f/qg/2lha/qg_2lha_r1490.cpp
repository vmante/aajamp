#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1490(const std::array<T,30>& k) {
  T z[25];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[2];
    z[6]=k[11];
    z[7]=k[4];
    z[8]=k[9];
    z[9]=3*z[8];
    z[10]=static_cast<T>(3)- z[3];
    z[10]=z[3]*z[10];
    z[10]=z[10] + 9*z[8] - static_cast<T>(5)- z[2];
    z[10]=z[4]*z[10];
    z[10]=z[10] - static_cast<T>(1)- z[9];
    z[11]=z[7]*z[4];
    z[12]=z[11]*z[8];
    z[13]= - static_cast<T>(1)+ 7*z[8];
    z[13]=z[4]*z[13];
    z[13]= - z[8] + z[13];
    z[13]=n<T>(1,2)*z[13] + z[12];
    z[13]=z[7]*z[13];
    z[10]=n<T>(1,2)*z[10] + z[13];
    z[10]=z[7]*z[10];
    z[13]=z[2] + 3;
    z[14]=n<T>(1,2)*z[8];
    z[15]=z[13]*z[14];
    z[16]=z[5] + 1;
    z[17]=npow(z[3],2);
    z[18]= - n<T>(5,2) + z[3];
    z[18]=z[3]*z[18];
    z[18]=n<T>(5,2)*z[8] + z[18];
    z[18]=z[4]*z[18];
    z[10]=z[10] + z[18] - z[17] - z[15] + n<T>(3,2)*z[16] + z[2];
    z[10]=z[7]*z[10];
    z[18]=z[3] - z[8];
    z[19]=n<T>(1,2)*z[4];
    z[18]=z[18]*z[19];
    z[19]=n<T>(1,2)*z[3];
    z[20]=z[19] - z[14];
    z[21]=z[2] + 1;
    z[20]=z[21]*z[20];
    z[22]=npow(z[5],2);
    z[23]=n<T>(1,2)*z[2];
    z[24]= - z[5]*z[23];
    z[10]=z[10] - z[18] - z[22] + z[24] + z[20];
    z[20]= - static_cast<T>(1)- 3*z[5];
    z[20]=z[20]*z[23];
    z[24]= - z[2]*z[21];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[14];
    z[16]= - z[5]*z[16];
    z[16]= - z[18] + n<T>(3,2)*z[3] + z[24] + z[20] - static_cast<T>(1)+ z[16];
    z[9]= - n<T>(1,2)*z[13] + z[9];
    z[9]=z[4]*z[9];
    z[9]= - z[15] + z[9];
    z[13]=z[12] - z[8];
    z[15]= - n<T>(1,4) + z[8];
    z[15]=z[4]*z[15];
    z[13]=z[15] + n<T>(1,4)*z[13];
    z[13]=z[7]*z[13];
    z[9]=n<T>(1,2)*z[9] + z[13];
    z[9]=z[7]*z[9];
    z[13]=z[23] + 1;
    z[13]= - z[2]*z[13];
    z[13]= - n<T>(3,2) + z[13];
    z[13]=z[8]*z[13];
    z[13]=z[13] + n<T>(1,2)*z[5] + z[21];
    z[15]=n<T>(1,4)*z[3];
    z[18]= - static_cast<T>(1)+ z[15];
    z[18]=z[3]*z[18];
    z[18]=z[8] + z[18];
    z[18]=z[4]*z[18];
    z[9]=z[9] + n<T>(1,2)*z[13] + z[18];
    z[9]=z[7]*z[9];
    z[9]=n<T>(1,2)*z[16] + z[9];
    z[9]=z[7]*z[9];
    z[13]=npow(z[5],3);
    z[16]=z[2]*z[22];
    z[13]=z[13] + z[16];
    z[9]=n<T>(1,4)*z[13] + z[9];
    z[9]=z[6]*z[9];
    z[9]=n<T>(1,2)*z[10] + z[9];
    z[9]=z[6]*z[9];
    z[10]= - static_cast<T>(1)- z[14];
    z[13]= - n<T>(1,4) + z[3];
    z[13]=z[3]*z[13];
    z[16]=n<T>(7,4) - z[3];
    z[16]=z[3]*z[16];
    z[16]=z[16] - static_cast<T>(1)+ n<T>(7,4)*z[8];
    z[16]=z[4]*z[16];
    z[10]=z[16] + n<T>(1,2)*z[10] + z[13];
    z[13]=z[19] - 1;
    z[15]=z[13]*z[15];
    z[15]=z[8] + z[15];
    z[15]=z[4]*z[15];
    z[12]=n<T>(3,8)*z[12] - n<T>(1,8)*z[8] + z[15];
    z[12]=z[7]*z[12];
    z[10]=n<T>(1,2)*z[10] + z[12];
    z[10]=z[7]*z[10];
    z[12]= - z[21]*z[17];
    z[15]= - static_cast<T>(1)+ z[3];
    z[15]=z[3]*z[15];
    z[15]=z[8] + z[15];
    z[15]=z[4]*z[15];
    z[12]=z[15] + z[5] + z[12];
    z[9]=z[9] + n<T>(1,4)*z[12] + z[10];
    z[9]=z[6]*z[9];
    z[10]= - static_cast<T>(1)+ z[2];
    z[10]=z[3]*z[10];
    z[10]=static_cast<T>(1)+ z[10];
    z[10]=z[10]*z[19];
    z[11]=z[14]*z[11];
    z[12]=z[3]*z[13];
    z[12]=z[8] + z[12];
    z[12]=z[4]*z[12];
    z[10]=z[11] + z[10] + z[12];
    z[10]=z[7]*z[10];
    z[11]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[12]=z[23] - z[11];
    z[12]=z[2]*z[12];
    z[12]=z[12] - z[11];
    z[12]=z[3]*z[12];
    z[13]= - z[1] + z[21];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[3]*z[12];
    z[11]=z[3]*z[11];
    z[11]=n<T>(1,2) + z[11];
    z[11]=z[3]*z[11];
    z[11]=z[14] + z[11];
    z[11]=z[4]*z[11];
    z[10]=z[10] + z[12] + z[11];

    r += z[9] + n<T>(1,4)*z[10];
 
    return r;
}

template double qg_2lha_r1490(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1490(const std::array<dd_real,30>&);
#endif
