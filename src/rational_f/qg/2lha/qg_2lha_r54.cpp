#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r54(const std::array<T,30>& k) {
  T z[7];
  T r = 0;

    z[1]=k[3];
    z[2]=k[5];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=n<T>(1,2)*z[4];
    z[6]=static_cast<T>(3)- z[4];
    z[6]=z[6]*z[5];
    z[6]=z[6] - static_cast<T>(1)- n<T>(1,2)*z[1];
    z[6]=z[2]*z[6];
    z[6]=n<T>(1,2) + z[6];
    z[6]=z[3]*z[6];
    z[5]=static_cast<T>(1)- z[5];
    z[5]=z[4]*z[5];
    z[5]= - n<T>(1,2) + z[5];
    z[5]=z[2]*z[5];
    z[5]=z[5] + z[6];

    r += n<T>(1,4)*z[5]*z[3];
 
    return r;
}

template double qg_2lha_r54(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r54(const std::array<dd_real,30>&);
#endif
