#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r82(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=k[5];
    z[7]=k[11];
    z[8]=k[9];
    z[9]=n<T>(1,4)*z[5];
    z[10]=z[5]*z[6];
    z[11]=z[10] - z[6];
    z[12]=static_cast<T>(5)- z[11];
    z[9]=z[12]*z[9];
    z[12]= - z[6] - static_cast<T>(7)- 11*z[1];
    z[13]= - z[6] + n<T>(1,2)*z[10];
    z[14]=n<T>(7,2) - z[13];
    z[14]=z[5]*z[14];
    z[12]=n<T>(1,2)*z[12] + z[14];
    z[12]=z[5]*z[12];
    z[14]=static_cast<T>(1)- z[2];
    z[14]=z[14]*npow(z[1],2);
    z[12]=n<T>(5,2)*z[14] + z[12];
    z[14]=n<T>(1,8)*z[4];
    z[12]=z[12]*z[14];
    z[15]= - static_cast<T>(1)+ n<T>(5,8)*z[2];
    z[15]=z[1]*z[15];
    z[9]=z[12] + z[9] - n<T>(1,16) + z[15];
    z[9]=z[4]*z[9];
    z[10]=static_cast<T>(1)- z[10];
    z[9]=n<T>(3,16)*z[10] + z[9];
    z[9]=z[3]*z[9];
    z[10]=static_cast<T>(3)- z[13];
    z[10]=z[5]*z[10];
    z[10]=z[10] - static_cast<T>(3)- n<T>(5,2)*z[1] - n<T>(1,2)*z[6];
    z[10]=z[10]*z[14];
    z[10]=z[10] + static_cast<T>(1)- n<T>(1,4)*z[11];
    z[10]=z[4]*z[10];
    z[11]= - z[7] - n<T>(3,8)*z[6];
    z[9]=z[9] + n<T>(1,2)*z[11] + z[10];
    z[9]=z[3]*z[9];
    z[10]=z[7]*z[8];

    r += z[9] + n<T>(1,2)*z[10];
 
    return r;
}

template double qg_2lha_r82(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r82(const std::array<dd_real,30>&);
#endif
