#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2008(const std::array<T,30>& k) {
  T z[19];
  T r = 0;

    z[1]=k[3];
    z[2]=k[5];
    z[3]=k[8];
    z[4]=k[15];
    z[5]=k[12];
    z[6]=k[4];
    z[7]=k[10];
    z[8]=k[20];
    z[9]=k[11];
    z[10]=k[7];
    z[11]=k[9];
    z[12]=npow(z[2],2);
    z[13]=z[12]*z[1];
    z[14]=z[5]*z[13];
    z[15]=z[5]*z[2];
    z[13]=z[2] - z[13];
    z[13]=z[3]*z[13];
    z[13]=z[13] - z[15] + z[14];
    z[13]=z[4]*z[1]*z[13];
    z[12]=z[12]*z[6];
    z[16]=z[12] - z[2];
    z[17]=z[16]*z[9];
    z[16]=z[7]*z[16];
    z[16]= - z[17] + z[16];
    z[16]=z[8]*z[6]*z[16];
    z[13]=z[13] + z[16];
    z[16]=z[3] - z[7];
    z[18]=n<T>(3,4)*z[11] - n<T>(3,4);
    z[16]=z[18]*z[2]*z[16];
    z[15]=z[6]*z[15];
    z[18]=z[6]*z[2];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[9]*z[18];
    z[15]=z[15] + z[18];
    z[15]=z[10]*z[15];
    z[12]=n<T>(7,4)*z[2] - z[12];
    z[12]=z[5]*z[12];
    z[12]=z[15] - n<T>(7,4)*z[14] + z[12] - n<T>(1,4)*z[17] + n<T>(3,4)*z[13] + 
    z[16];

    r += n<T>(1,2)*z[12];
 
    return r;
}

template double qg_2lha_r2008(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2008(const std::array<dd_real,30>&);
#endif
