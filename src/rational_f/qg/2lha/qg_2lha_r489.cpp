#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r489(const std::array<T,30>& k) {
  T z[93];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[6];
    z[7]=k[10];
    z[8]=k[14];
    z[9]=k[11];
    z[10]=k[9];
    z[11]=k[3];
    z[12]=k[27];
    z[13]=k[13];
    z[14]=npow(z[4],2);
    z[15]=n<T>(3,2)*z[14];
    z[16]=z[4] + 1;
    z[17]=z[16]*z[15];
    z[18]= - static_cast<T>(35)- 12*z[4];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(57,2) + z[18];
    z[18]=z[18]*z[14];
    z[18]=n<T>(11,2) + z[18];
    z[18]=z[7]*z[18];
    z[17]=z[17] + z[18];
    z[17]=z[7]*z[17];
    z[18]=n<T>(1,2)*z[4];
    z[19]=z[18] + 1;
    z[20]=z[19]*z[4];
    z[21]=z[20] + n<T>(1,2);
    z[22]=z[21]*z[7];
    z[23]=n<T>(1,2)*z[16];
    z[24]= - z[23] + z[22];
    z[24]=z[7]*z[24];
    z[25]=n<T>(1,2)*z[10];
    z[26]=z[16]*z[25];
    z[24]=z[24] + z[26];
    z[24]=z[11]*z[24];
    z[26]=n<T>(9,2)*z[4];
    z[27]=z[26] + 10;
    z[27]=z[27]*z[4];
    z[27]=z[27] + n<T>(11,2);
    z[28]=3*z[4];
    z[29]= - n<T>(23,2) - z[28];
    z[29]=z[4]*z[29];
    z[29]= - static_cast<T>(14)+ z[29];
    z[29]=z[4]*z[29];
    z[29]= - n<T>(11,2) + z[29];
    z[29]=z[7]*z[29];
    z[29]=z[29] + z[27];
    z[29]=z[7]*z[29];
    z[27]=z[27]*z[10];
    z[24]=z[24] + z[29] - z[27];
    z[24]=z[11]*z[24];
    z[17]=z[17] + z[24];
    z[17]=z[11]*z[17];
    z[24]=npow(z[14],2);
    z[29]=z[21]*z[24];
    z[30]=z[7]*z[29];
    z[31]=z[24]*z[16];
    z[30]=z[30] + z[31];
    z[30]=z[30]*z[7];
    z[32]=n<T>(1,2)*z[24];
    z[30]=z[30] + z[32];
    z[33]=z[30]*z[3];
    z[33]=n<T>(3,4)*z[33];
    z[34]=npow(z[4],3);
    z[35]= - static_cast<T>(18)- n<T>(25,2)*z[4];
    z[35]=z[35]*z[34];
    z[36]=13*z[4];
    z[37]= - n<T>(63,2) - z[36];
    z[37]=z[4]*z[37];
    z[37]= - n<T>(37,2) + z[37];
    z[37]=z[37]*z[14];
    z[37]= - n<T>(11,2) + z[37];
    z[37]=z[4]*z[37];
    z[37]= - n<T>(11,2) + z[37];
    z[37]=z[7]*z[37];
    z[35]=z[37] - n<T>(11,2) + z[35];
    z[35]=z[7]*z[35];
    z[22]=z[22] + z[16];
    z[22]=z[22]*z[7];
    z[22]=z[22] + n<T>(1,2);
    z[37]=z[8]*z[22];
    z[38]=n<T>(1,2)*z[34];
    z[17]=z[17] - z[33] + n<T>(13,4)*z[37] + z[38] + z[35];
    z[17]=z[5]*z[17];
    z[35]=29*z[4];
    z[37]=z[35] + 39;
    z[39]=static_cast<T>(31)+ n<T>(21,2)*z[4];
    z[39]=z[4]*z[39];
    z[39]=n<T>(41,2) + z[39];
    z[39]=z[7]*z[39];
    z[39]=z[39] - z[37];
    z[40]=n<T>(1,4)*z[7];
    z[39]=z[39]*z[40];
    z[27]=n<T>(1,4)*z[37] + z[27];
    z[27]=z[10]*z[27];
    z[37]=n<T>(1,2)*z[7];
    z[41]=z[16]*z[7];
    z[42]=static_cast<T>(3)- z[41];
    z[42]=z[42]*z[37];
    z[43]=z[16]*z[10];
    z[44]= - n<T>(3,2) - z[43];
    z[44]=z[10]*z[44];
    z[42]=z[42] + z[44];
    z[44]=n<T>(1,2)*z[11];
    z[42]=z[42]*z[44];
    z[27]=z[42] + z[39] + z[27];
    z[27]=z[11]*z[27];
    z[39]=31*z[4];
    z[42]= - n<T>(89,2) - z[39];
    z[42]=z[4]*z[42];
    z[45]=static_cast<T>(85)+ n<T>(81,2)*z[4];
    z[45]=z[4]*z[45];
    z[45]=static_cast<T>(35)+ z[45];
    z[45]=z[4]*z[45];
    z[45]= - n<T>(19,2) + z[45];
    z[45]=z[7]*z[45];
    z[42]=z[45] - static_cast<T>(13)+ z[42];
    z[42]=z[7]*z[42];
    z[45]=static_cast<T>(45)+ z[35];
    z[45]=z[4]*z[45];
    z[45]=static_cast<T>(13)+ z[45];
    z[45]=z[10]*z[45];
    z[42]=z[42] + z[45];
    z[27]=n<T>(1,2)*z[42] + z[27];
    z[27]=z[11]*z[27];
    z[42]= - static_cast<T>(53)- 11*z[4];
    z[42]=z[42]*z[18];
    z[42]= - static_cast<T>(21)+ z[42];
    z[42]=z[7]*z[42];
    z[45]=21*z[4];
    z[42]=z[42] - n<T>(73,2) - z[45];
    z[42]=z[7]*z[42];
    z[42]= - n<T>(31,2) + z[42];
    z[46]=3*z[8];
    z[47]=z[22]*z[46];
    z[42]=n<T>(1,4)*z[42] + z[47];
    z[42]=z[8]*z[42];
    z[47]=z[34]*z[7];
    z[48]=static_cast<T>(1)+ n<T>(5,4)*z[4];
    z[49]=z[4]*z[48];
    z[49]= - n<T>(1,4) + z[49];
    z[49]=z[49]*z[47];
    z[49]=n<T>(3,2)*z[24] + z[49];
    z[49]=z[7]*z[49];
    z[33]= - z[33] + n<T>(1,4)*z[34] + z[49];
    z[49]=n<T>(1,2)*z[3];
    z[33]=z[33]*z[49];
    z[39]=n<T>(33,2) + z[39];
    z[39]=z[39]*z[14];
    z[50]=61*z[4];
    z[51]=static_cast<T>(99)+ z[50];
    z[51]=z[4]*z[51];
    z[51]=n<T>(75,4) + z[51];
    z[51]=z[4]*z[51];
    z[51]= - n<T>(3,2) + z[51];
    z[51]=z[4]*z[51];
    z[51]=n<T>(71,4) + z[51];
    z[51]=z[7]*z[51];
    z[39]=z[51] + n<T>(45,2) + z[39];
    z[39]=z[7]*z[39];
    z[39]= - n<T>(5,4)*z[14] + z[39];
    z[17]=z[17] + z[27] + z[33] + n<T>(1,2)*z[39] + z[42];
    z[17]=z[5]*z[17];
    z[27]=z[16]*z[14];
    z[33]=z[14]*z[7];
    z[39]=5*z[4];
    z[42]= - static_cast<T>(1)- z[39];
    z[42]=z[4]*z[42];
    z[42]= - static_cast<T>(1)+ z[42];
    z[42]=z[42]*z[33];
    z[27]= - n<T>(9,2)*z[27] + z[42];
    z[27]=z[7]*z[27];
    z[42]=7*z[4];
    z[51]=static_cast<T>(1)+ z[42];
    z[51]=z[51]*z[38];
    z[52]=3*z[7];
    z[53]=z[52]*z[31];
    z[51]=z[51] + z[53];
    z[51]=z[7]*z[51];
    z[53]=z[4] - 1;
    z[54]=z[53]*z[4];
    z[54]=z[54] + 1;
    z[55]=z[4]*z[54];
    z[55]= - static_cast<T>(1)+ z[55];
    z[51]=n<T>(1,2)*z[55] + z[51];
    z[51]=z[3]*z[51];
    z[55]= - n<T>(1,2) + z[39];
    z[55]=z[4]*z[55];
    z[27]=z[51] + z[27] + n<T>(1,2) + z[55];
    z[51]=n<T>(1,16)*z[6];
    z[55]= - static_cast<T>(1)+ z[3];
    z[55]=z[55]*z[51];
    z[27]=z[55] + n<T>(1,8)*z[27];
    z[27]=z[3]*z[27];
    z[55]=z[4] + 5;
    z[55]=z[55]*z[10];
    z[55]=z[55] + 1;
    z[55]=z[55]*z[10];
    z[56]=z[53]*z[7];
    z[57]=z[56] + 1;
    z[57]=z[57]*z[7];
    z[55]=z[55] - z[57];
    z[57]=npow(z[10],2);
    z[58]=npow(z[7],2);
    z[57]=z[57] + z[58];
    z[59]=z[10] - z[7];
    z[59]=z[59]*z[6];
    z[60]= - n<T>(1,2)*z[57] - z[59];
    z[60]=z[60]*z[44];
    z[60]=z[60] + n<T>(1,4)*z[55] + z[59];
    z[60]=z[11]*z[60];
    z[61]=static_cast<T>(275)+ 239*z[4];
    z[62]= - static_cast<T>(19)- n<T>(65,4)*z[4];
    z[62]=z[4]*z[62];
    z[62]= - n<T>(1,4) + z[62];
    z[62]=z[7]*z[62];
    z[61]=n<T>(1,12)*z[61] + z[62];
    z[61]=z[61]*z[37];
    z[62]= - static_cast<T>(133)- 121*z[4];
    z[63]= - n<T>(23,2) - z[4];
    z[63]=z[4]*z[63];
    z[63]= - static_cast<T>(12)+ z[63];
    z[63]=z[10]*z[63];
    z[62]=n<T>(1,12)*z[62] + z[63];
    z[62]=z[10]*z[62];
    z[59]=z[60] - n<T>(1,2)*z[59] + z[61] + z[62];
    z[59]=z[11]*z[59];
    z[60]=n<T>(5,2)*z[4];
    z[61]= - static_cast<T>(6)- z[60];
    z[61]=z[4]*z[61];
    z[61]= - n<T>(7,2) + z[61];
    z[61]=z[61]*z[52];
    z[61]=z[61] - static_cast<T>(16)- z[36];
    z[61]=z[7]*z[61];
    z[62]=z[4] + 2;
    z[63]=z[4]*z[62];
    z[63]=static_cast<T>(1)+ z[63];
    z[63]=z[7]*z[63];
    z[63]=2*z[16] + z[63];
    z[63]=z[7]*z[63];
    z[63]=static_cast<T>(1)+ z[63];
    z[63]=z[63]*z[46];
    z[61]=z[63] - n<T>(11,2) + z[61];
    z[61]=z[8]*z[61];
    z[63]=static_cast<T>(149)+ 69*z[4];
    z[63]=z[4]*z[63];
    z[63]=static_cast<T>(103)+ z[63];
    z[63]=z[63]*z[37];
    z[63]=z[63] + static_cast<T>(55)+ n<T>(61,2)*z[4];
    z[63]=z[7]*z[63];
    z[63]=static_cast<T>(13)+ z[63];
    z[61]=n<T>(1,4)*z[63] + z[61];
    z[61]=z[8]*z[61];
    z[63]=n<T>(1,3)*z[4];
    z[64]=static_cast<T>(43)- n<T>(79,2)*z[4];
    z[64]=z[64]*z[63];
    z[64]= - n<T>(59,2) + z[64];
    z[65]= - n<T>(541,12) - 47*z[4];
    z[65]=z[4]*z[65];
    z[65]= - n<T>(13,6) + z[65];
    z[65]=z[4]*z[65];
    z[65]= - n<T>(121,12) + z[65];
    z[65]=z[7]*z[65];
    z[64]=n<T>(1,2)*z[64] + z[65];
    z[64]=z[7]*z[64];
    z[65]=n<T>(1,4)*z[4];
    z[64]=z[65] + z[64];
    z[66]=n<T>(3,4)*z[4];
    z[67]= - n<T>(29,3) - z[66];
    z[67]=z[4]*z[67];
    z[67]=n<T>(13,12) + z[67];
    z[67]=z[67]*z[25];
    z[17]=z[17] + z[59] + z[67] + n<T>(1,2)*z[64] + z[61] + z[27];
    z[17]=z[5]*z[17];
    z[27]=z[18] - 1;
    z[59]= - z[27]*z[34];
    z[61]=z[34]*z[16];
    z[64]=n<T>(3,2)*z[7];
    z[67]=z[64]*z[61];
    z[59]=z[59] + z[67];
    z[59]=z[7]*z[59];
    z[38]= - z[38] + z[59];
    z[59]=z[29]*z[6];
    z[59]=z[59] - z[31];
    z[67]=z[6]*z[10];
    z[68]= - z[59]*z[67];
    z[69]= - z[10]*z[32];
    z[38]=z[68] + 27*z[38] + z[69];
    z[68]=static_cast<T>(1)- n<T>(19,2)*z[4];
    z[68]=z[68]*z[18];
    z[69]=5*z[7];
    z[70]= - static_cast<T>(11)- z[69];
    z[70]=z[70]*z[37];
    z[68]=z[70] - static_cast<T>(3)+ z[68];
    z[68]=z[10]*z[68];
    z[70]=z[16]*z[4];
    z[71]=z[70] + 1;
    z[72]=n<T>(1,2) - z[14];
    z[72]=z[7]*z[72];
    z[72]=n<T>(1,2)*z[71] + z[72];
    z[72]=z[7]*z[72];
    z[73]=z[7] + 1;
    z[74]=z[73]*z[7];
    z[75]=z[37] + 1;
    z[76]=z[75]*z[7];
    z[76]=z[76] + n<T>(1,2);
    z[77]=z[10]*z[76];
    z[77]= - n<T>(3,2)*z[74] + z[77];
    z[78]=n<T>(9,2)*z[11];
    z[77]=z[77]*z[78];
    z[68]=z[77] + n<T>(27,2)*z[72] + z[68];
    z[68]=z[11]*z[68];
    z[70]=static_cast<T>(3)- z[70];
    z[70]=z[70]*z[33];
    z[70]=z[14] + z[70];
    z[70]=z[7]*z[70];
    z[68]=n<T>(27,4)*z[70] + z[68];
    z[68]=z[11]*z[68];
    z[30]=z[30]*z[5];
    z[38]=n<T>(27,2)*z[30] + n<T>(1,2)*z[38] + z[68];
    z[68]=n<T>(1,2)*z[12];
    z[38]=z[38]*z[68];
    z[70]=z[21]*z[34];
    z[29]= - z[10]*z[29];
    z[29]=5*z[70] + z[29];
    z[70]=n<T>(1,4)*z[6];
    z[29]=z[29]*z[70];
    z[72]=n<T>(1,4)*z[10];
    z[31]=z[31]*z[72];
    z[29]=z[29] - z[61] + z[31];
    z[29]=z[29]*z[67];
    z[31]= - static_cast<T>(31)+ 45*z[4];
    z[31]=z[31]*z[14];
    z[77]= - static_cast<T>(19)- n<T>(17,8)*z[4];
    z[77]=z[4]*z[77];
    z[77]= - n<T>(57,8) + z[77];
    z[77]=z[77]*z[33];
    z[31]=n<T>(1,8)*z[31] + z[77];
    z[31]=z[7]*z[31];
    z[31]=3*z[14] + z[31];
    z[77]= - z[10]*z[24];
    z[77]=3*z[34] + z[77];
    z[77]=z[10]*z[77];
    z[29]=z[29] + 3*z[31] + n<T>(1,8)*z[77];
    z[31]=25*z[7];
    z[77]=static_cast<T>(69)+ z[31];
    z[77]=z[77]*z[37];
    z[77]=z[77] + static_cast<T>(21)+ n<T>(73,2)*z[4];
    z[79]=z[40] + 1;
    z[79]=z[79]*z[7];
    z[80]=static_cast<T>(1)+ n<T>(17,2)*z[4];
    z[80]=z[4]*z[80];
    z[80]= - n<T>(3,2) + z[80];
    z[80]=n<T>(1,2)*z[80] - z[79];
    z[80]=z[10]*z[80];
    z[81]=z[74]*z[78];
    z[77]=z[81] + n<T>(1,2)*z[77] + z[80];
    z[77]=z[10]*z[77];
    z[80]=27*z[4];
    z[81]= - static_cast<T>(65)- z[80];
    z[82]= - n<T>(35,2) + z[80];
    z[82]=z[7]*z[82];
    z[81]=n<T>(1,2)*z[81] + z[82];
    z[81]=z[81]*z[64];
    z[77]=z[81] + z[77];
    z[81]=n<T>(1,4)*z[11];
    z[77]=z[77]*z[81];
    z[82]=static_cast<T>(1)- z[66];
    z[82]=z[82]*z[28];
    z[83]= - static_cast<T>(49)- 23*z[7];
    z[83]=z[83]*z[40];
    z[82]=z[83] - n<T>(13,2) + z[82];
    z[82]=z[82]*z[25];
    z[83]= - n<T>(1,2) + z[42];
    z[83]=z[4]*z[83];
    z[83]=static_cast<T>(19)+ z[83];
    z[84]= - static_cast<T>(1)+ n<T>(45,16)*z[4];
    z[84]=z[4]*z[84];
    z[84]= - n<T>(81,16) + z[84];
    z[84]=z[4]*z[84];
    z[84]=n<T>(13,8) + z[84];
    z[84]=z[7]*z[84];
    z[83]=n<T>(1,8)*z[83] + z[84];
    z[83]=z[83]*z[52];
    z[77]=z[77] + z[83] + z[82];
    z[77]=z[11]*z[77];
    z[82]= - n<T>(31,4) - z[36];
    z[82]=z[82]*z[34];
    z[66]=z[66] + 1;
    z[66]=z[66]*z[4];
    z[83]=z[66] + n<T>(1,4);
    z[84]=z[83]*z[47];
    z[82]=z[82] - n<T>(21,2)*z[84];
    z[82]=z[7]*z[82];
    z[82]=n<T>(17,4)*z[30] - n<T>(41,8)*z[34] + z[82];
    z[82]=z[5]*z[82];
    z[29]=z[38] + n<T>(3,2)*z[82] + n<T>(1,2)*z[29] + z[77];
    z[29]=z[12]*z[29];
    z[38]=z[28] + 5;
    z[77]=z[38]*z[14];
    z[82]= - z[10]*z[19]*z[34];
    z[82]=n<T>(1,4)*z[77] + z[82];
    z[82]=z[82]*z[25];
    z[84]=z[4] + 3;
    z[85]=z[84]*z[18];
    z[85]=static_cast<T>(1)+ z[85];
    z[85]=z[85]*z[34]*z[25];
    z[86]=n<T>(3,8)*z[4];
    z[87]=z[86] + 1;
    z[87]=z[87]*z[4];
    z[87]=z[87] + n<T>(3,8);
    z[88]= - z[87]*z[14];
    z[85]=z[88] + z[85];
    z[67]=z[85]*z[67];
    z[67]=z[82] + z[67];
    z[67]=z[6]*z[67];
    z[82]=static_cast<T>(163)+ n<T>(131,2)*z[4];
    z[82]=z[4]*z[82];
    z[82]= - n<T>(51,2) + z[82];
    z[82]=z[4]*z[82];
    z[82]=static_cast<T>(27)+ z[82];
    z[82]=z[82]*z[37];
    z[85]=23*z[4];
    z[88]= - n<T>(13,2) - z[85];
    z[88]=z[4]*z[88];
    z[82]=z[82] + n<T>(37,2) + z[88];
    z[82]=z[7]*z[82];
    z[88]= - static_cast<T>(31)- 15*z[7];
    z[88]=z[88]*z[37];
    z[89]=static_cast<T>(11)+ z[60];
    z[89]=z[4]*z[89];
    z[88]=z[88] + static_cast<T>(1)+ z[89];
    z[88]=z[10]*z[88];
    z[26]=z[88] - z[26] + z[82];
    z[26]=n<T>(1,2)*z[26] + z[67];
    z[50]= - static_cast<T>(37)+ z[50];
    z[67]=n<T>(35,4) - z[7];
    z[67]=z[7]*z[67];
    z[50]=n<T>(1,4)*z[50] + z[67];
    z[67]= - static_cast<T>(3)- z[7];
    z[67]=z[67]*z[40];
    z[82]= - static_cast<T>(2)+ z[4];
    z[82]=z[4]*z[82];
    z[67]=z[67] - n<T>(1,2) + z[82];
    z[67]=z[10]*z[67];
    z[50]=n<T>(1,4)*z[50] + z[67];
    z[50]=z[10]*z[50];
    z[67]=n<T>(1,2) - z[7];
    z[67]=z[67]*z[52];
    z[82]= - static_cast<T>(3)+ 7*z[74];
    z[82]=z[82]*z[25];
    z[67]=z[67] + z[82];
    z[67]=z[11]*z[67];
    z[82]=static_cast<T>(89)- n<T>(99,2)*z[4];
    z[82]=z[4]*z[82];
    z[82]= - n<T>(21,2) + z[82];
    z[82]=z[82]*z[37];
    z[82]=z[82] - static_cast<T>(43)- n<T>(71,4)*z[4];
    z[82]=z[82]*z[40];
    z[50]=n<T>(3,4)*z[67] + z[82] + z[50];
    z[50]=z[11]*z[50];
    z[67]= - n<T>(19,2) - 20*z[4];
    z[67]=z[67]*z[34];
    z[82]= - static_cast<T>(7)- n<T>(13,2)*z[4];
    z[82]=z[4]*z[82];
    z[82]= - n<T>(1,2) + z[82];
    z[47]=z[82]*z[47];
    z[47]=z[67] + n<T>(7,4)*z[47];
    z[47]=z[7]*z[47];
    z[30]=n<T>(33,4)*z[30] - n<T>(69,8)*z[34] + z[47];
    z[30]=z[5]*z[30];
    z[47]=static_cast<T>(11)+ n<T>(73,4)*z[4];
    z[47]=z[4]*z[47];
    z[47]=n<T>(13,4) + z[47];
    z[47]=z[47]*z[52];
    z[67]=static_cast<T>(87)+ 311*z[4];
    z[47]=n<T>(1,4)*z[67] + z[47];
    z[33]=z[47]*z[33];
    z[33]=n<T>(45,2)*z[14] + z[33];
    z[30]=n<T>(1,4)*z[33] + z[30];
    z[30]=z[5]*z[30];
    z[26]=z[29] + z[30] + n<T>(1,2)*z[26] + z[50];
    z[26]=z[12]*z[26];
    z[29]= - static_cast<T>(127)- 49*z[4];
    z[29]=z[29]*z[40];
    z[29]= - static_cast<T>(35)+ z[29];
    z[30]=n<T>(1,3)*z[7];
    z[29]=z[29]*z[30];
    z[33]=z[4] + n<T>(1,2);
    z[47]=z[34]*z[6];
    z[50]= - z[33]*z[47];
    z[50]=n<T>(3,2)*z[34] + z[50];
    z[50]=z[6]*z[50];
    z[67]= - n<T>(19,4) - z[4];
    z[67]=z[4]*z[67];
    z[29]=n<T>(1,2)*z[50] + z[29] - n<T>(1,6) + z[67];
    z[50]=z[4] + n<T>(5,4);
    z[67]= - z[11]*z[75];
    z[67]=z[67] + n<T>(49,8)*z[7] - z[50];
    z[67]=z[30]*z[67];
    z[82]=z[4]*z[33];
    z[82]= - n<T>(1,2)*z[47] - n<T>(1,2) + z[82];
    z[82]=z[82]*z[70];
    z[88]= - n<T>(83,3) - 9*z[4];
    z[67]=z[82] + n<T>(1,8)*z[88] + z[67];
    z[67]=z[11]*z[67];
    z[22]=z[22]*z[5];
    z[82]=static_cast<T>(59)+ n<T>(49,4)*z[4];
    z[82]=z[4]*z[82];
    z[82]=n<T>(187,4) + z[82];
    z[82]=z[82]*z[30];
    z[88]=static_cast<T>(109)+ 63*z[4];
    z[82]=n<T>(1,4)*z[88] + z[82];
    z[82]=z[7]*z[82];
    z[88]=z[11]*z[70];
    z[82]=z[88] + n<T>(35,3) + z[82];
    z[82]=n<T>(1,2)*z[82] - 5*z[22];
    z[82]=z[5]*z[82];
    z[29]=z[82] + n<T>(1,2)*z[29] + z[67];
    z[59]=z[59]*z[6];
    z[32]=z[59] + z[32];
    z[32]=n<T>(1,4)*z[32];
    z[59]=z[4] + n<T>(1,4);
    z[59]=z[59]*z[4];
    z[67]=n<T>(7,4) + z[7];
    z[67]=z[7]*z[67];
    z[82]=z[11]*z[76];
    z[67]= - n<T>(9,4)*z[82] + z[67] + n<T>(3,4) + z[59];
    z[82]=npow(z[11],2);
    z[67]=z[67]*z[82];
    z[67]= - z[32] + z[67];
    z[67]=z[67]*z[68];
    z[68]=n<T>(3,2)*z[4];
    z[88]=z[68] + 1;
    z[89]= - z[88]*z[34];
    z[83]=z[83]*z[47];
    z[83]=z[89] + z[83];
    z[83]=z[6]*z[83];
    z[83]=n<T>(3,4)*z[34] + z[83];
    z[89]=static_cast<T>(7)+ z[64];
    z[89]=z[89]*z[40];
    z[90]=7*z[7];
    z[91]= - n<T>(45,2) - z[90];
    z[91]=z[91]*z[40];
    z[91]=z[91] - n<T>(33,8) - z[4];
    z[91]=z[91]*z[44];
    z[92]=n<T>(1,8) + z[4];
    z[92]=z[4]*z[92];
    z[89]=z[91] + z[89] + n<T>(11,8) + z[92];
    z[89]=z[11]*z[89];
    z[67]=z[67] + n<T>(1,4)*z[83] + z[89];
    z[67]=z[12]*z[67];
    z[83]=n<T>(1,3)*z[3];
    z[89]=static_cast<T>(1)- z[14];
    z[89]=z[89]*z[83];
    z[48]=n<T>(3,2)*z[48] + z[89];
    z[89]= - n<T>(1,6)*z[2] + z[63];
    z[89]=z[3]*z[89];
    z[89]= - n<T>(1,4) + z[89];
    z[89]=z[2]*z[89];
    z[48]=n<T>(1,2)*z[48] + z[89];
    z[48]=z[2]*z[48];
    z[29]=z[48] + n<T>(1,2)*z[29] + z[67];
    z[29]=z[9]*z[29];
    z[48]= - n<T>(19,4) - z[63];
    z[48]=z[7]*z[48];
    z[48]=z[48] + n<T>(7,2) + z[63];
    z[48]=z[7]*z[48];
    z[67]=z[73]*z[11];
    z[89]=z[30]*z[67];
    z[14]=z[14]*z[6];
    z[91]= - z[4] + z[14];
    z[91]=z[6]*z[91];
    z[48]=z[89] + n<T>(1,8)*z[91] + n<T>(1,3) + z[48];
    z[48]=z[11]*z[48];
    z[89]=static_cast<T>(11)+ n<T>(161,8)*z[4];
    z[89]=z[89]*z[30];
    z[91]= - static_cast<T>(21)+ n<T>(17,3)*z[4];
    z[89]=n<T>(1,8)*z[91] + z[89];
    z[89]=z[7]*z[89];
    z[91]=z[53]*z[83];
    z[33]= - n<T>(3,2)*z[14] + z[33];
    z[33]=z[33]*z[70];
    z[92]=n<T>(151,2) + z[4];
    z[33]=z[48] + z[33] + z[91] + n<T>(1,12)*z[92] + z[89];
    z[48]=z[76]*z[78];
    z[76]= - static_cast<T>(4)- n<T>(17,8)*z[7];
    z[76]=z[7]*z[76];
    z[48]=z[48] + z[76] - n<T>(15,8) - z[59];
    z[48]=z[48]*z[82];
    z[32]=z[32] + z[48];
    z[32]=z[12]*z[32];
    z[47]=z[21]*z[47];
    z[47]=z[61] - n<T>(5,4)*z[47];
    z[47]=z[6]*z[47];
    z[47]= - n<T>(3,8)*z[34] + z[47];
    z[48]=n<T>(1,2) - z[4];
    z[48]=z[48]*z[28];
    z[48]= - n<T>(31,4) + z[48];
    z[59]=static_cast<T>(75)+ n<T>(53,2)*z[7];
    z[59]=z[59]*z[40];
    z[61]=z[11]*z[74];
    z[59]=n<T>(9,8)*z[61] + z[59] + n<T>(99,8) + z[4];
    z[44]=z[59]*z[44];
    z[59]=2*z[7];
    z[61]= - n<T>(47,8) - z[59];
    z[61]=z[7]*z[61];
    z[44]=z[44] + n<T>(1,2)*z[48] + z[61];
    z[44]=z[11]*z[44];
    z[32]=z[32] + n<T>(1,2)*z[47] + z[44];
    z[32]=z[12]*z[32];
    z[14]=z[87]*z[14];
    z[14]= - n<T>(1,8)*z[77] + z[14];
    z[14]=z[6]*z[14];
    z[44]= - n<T>(11,4) - z[7];
    z[44]=z[44]*z[52];
    z[47]=z[4] - n<T>(3,2);
    z[48]= - z[4]*z[47];
    z[14]=z[14] + z[44] - n<T>(39,4) + z[48];
    z[31]=static_cast<T>(53)+ z[31];
    z[31]=z[7]*z[31];
    z[31]=z[31] + static_cast<T>(57)+ z[39];
    z[44]=static_cast<T>(2)+ n<T>(5,16)*z[7];
    z[44]=z[11]*z[7]*z[44];
    z[31]=n<T>(1,8)*z[31] + z[44];
    z[31]=z[11]*z[31];
    z[14]=z[32] + n<T>(1,2)*z[14] + z[31];
    z[14]=z[12]*z[14];
    z[31]= - static_cast<T>(45)+ 17*z[4];
    z[31]=z[31]*z[18];
    z[31]= - static_cast<T>(31)+ z[31];
    z[31]=z[7]*z[31];
    z[31]=z[31] - n<T>(121,2) - z[45];
    z[31]=z[7]*z[31];
    z[22]=31*z[22] - n<T>(59,2) + z[31];
    z[31]=n<T>(1,4)*z[5];
    z[22]=z[22]*z[31];
    z[32]= - static_cast<T>(25)- n<T>(161,16)*z[4];
    z[32]=z[4]*z[32];
    z[32]=n<T>(103,16) + z[32];
    z[32]=z[32]*z[30];
    z[44]=n<T>(69,2) - z[39];
    z[32]=n<T>(1,4)*z[44] + z[32];
    z[32]=z[7]*z[32];
    z[22]=z[22] - z[51] + n<T>(175,48) + z[32];
    z[22]=z[5]*z[22];
    z[19]= - z[3]*z[19];
    z[32]=z[2]*z[49];
    z[19]=z[32] + n<T>(1,4) + z[19];
    z[32]=n<T>(1,3)*z[2];
    z[19]=z[19]*z[32];
    z[14]=z[29] + z[19] + z[14] + n<T>(1,2)*z[33] + z[22];
    z[14]=z[9]*z[14];
    z[19]= - n<T>(31,4) - z[39];
    z[22]= - z[4]*z[50];
    z[22]=n<T>(7,2) + z[22];
    z[22]=z[7]*z[22];
    z[19]=n<T>(1,3)*z[19] + z[22];
    z[19]=z[19]*z[37];
    z[22]=z[8]*z[7];
    z[29]=static_cast<T>(1)+ n<T>(9,8)*z[7];
    z[29]=z[7]*z[29];
    z[29]= - z[22] - n<T>(9,4) + z[29];
    z[29]=z[8]*z[29];
    z[33]=n<T>(7,3) - n<T>(23,4)*z[7];
    z[33]=z[7]*z[33];
    z[33]=n<T>(9,2) + z[33];
    z[29]=n<T>(1,2)*z[33] + z[29];
    z[29]=z[8]*z[29];
    z[33]=z[52] - z[22];
    z[44]=n<T>(1,2)*z[8];
    z[33]=z[33]*z[44];
    z[48]=z[7]*z[27];
    z[33]=z[33] + n<T>(1,2) + z[48];
    z[33]=z[3]*z[33];
    z[19]=z[33] + z[29] - n<T>(1,3) + z[19];
    z[19]=z[3]*z[19];
    z[29]= - static_cast<T>(13)+ z[39];
    z[29]=z[29]*z[37];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[7]*z[29];
    z[33]=z[52]*z[8];
    z[48]=n<T>(17,2)*z[7];
    z[50]= - static_cast<T>(3)+ z[48];
    z[50]=z[7]*z[50];
    z[50]= - z[33] - static_cast<T>(17)+ z[50];
    z[50]=z[8]*z[50];
    z[51]=n<T>(43,3) - n<T>(87,4)*z[7];
    z[51]=z[7]*z[51];
    z[50]=z[50] + n<T>(41,2) + z[51];
    z[50]=z[50]*z[44];
    z[51]= - static_cast<T>(1)+ z[69];
    z[51]=z[7]*z[51];
    z[50]=z[50] - static_cast<T>(1)+ n<T>(5,2)*z[51];
    z[50]=z[8]*z[50];
    z[19]=z[19] + z[29] + z[50];
    z[19]=z[19]*z[49];
    z[29]= - z[27]*z[63];
    z[29]= - n<T>(1,2) + z[29];
    z[29]=z[7]*z[29];
    z[29]= - n<T>(1,3)*z[53] + z[29];
    z[29]=z[7]*z[29];
    z[29]= - n<T>(1,6) + z[29];
    z[50]=n<T>(1,4)*z[8];
    z[51]=z[50]*z[58];
    z[61]=z[58] - z[51];
    z[76]=n<T>(1,3)*z[8];
    z[61]=z[61]*z[76];
    z[29]=n<T>(1,2)*z[29] + z[61];
    z[29]=z[3]*z[29];
    z[27]=z[27]*z[58];
    z[61]= - z[58]*z[76];
    z[30]= - static_cast<T>(1)- z[30];
    z[30]=z[7]*z[30];
    z[30]=z[30] + z[61];
    z[30]=z[30]*z[44];
    z[61]=z[64] + 1;
    z[61]=z[61]*z[7];
    z[30]=z[61] + z[30];
    z[30]=z[8]*z[30];
    z[27]=z[29] + z[27] + z[30];
    z[27]=z[3]*z[27];
    z[29]= - static_cast<T>(2)- n<T>(5,3)*z[7];
    z[29]=z[7]*z[29];
    z[29]=z[29] - z[51];
    z[29]=z[8]*z[29];
    z[30]=n<T>(7,2) + z[69];
    z[30]=z[7]*z[30];
    z[29]=z[30] + z[29];
    z[29]=z[8]*z[29];
    z[30]= - n<T>(3,2) - 4*z[7];
    z[30]=z[7]*z[30];
    z[29]=z[30] + z[29];
    z[29]=z[8]*z[29];
    z[27]=z[27] + n<T>(3,2)*z[58] + z[29];
    z[27]=z[3]*z[27];
    z[29]=z[8]*z[53];
    z[29]= - z[47] + z[29];
    z[29]=z[8]*z[29];
    z[29]= - n<T>(1,2) + z[29];
    z[29]=z[46]*z[58]*z[29];
    z[30]=static_cast<T>(1)+ n<T>(4,3)*z[7];
    z[30]=z[7]*z[30];
    z[30]=z[30] - z[51];
    z[30]=z[8]*z[30];
    z[30]= - z[74] + z[30];
    z[47]=npow(z[8],2);
    z[30]=z[30]*z[47];
    z[51]=z[10]*z[58]*npow(z[8],3);
    z[30]=z[30] - n<T>(1,12)*z[51];
    z[30]=z[10]*z[30];
    z[27]=z[27] + z[29] + z[30];
    z[27]=z[2]*z[27];
    z[29]=2*z[4];
    z[30]=n<T>(9,2) + z[29];
    z[30]=z[4]*z[30];
    z[30]= - static_cast<T>(2)+ z[30];
    z[30]=z[30]*z[52];
    z[30]=z[30] + n<T>(5,2) - z[28];
    z[30]=z[7]*z[30];
    z[51]= - static_cast<T>(3)- z[29];
    z[51]=z[4]*z[51];
    z[51]=static_cast<T>(1)+ z[51];
    z[51]=z[7]*z[51];
    z[51]=z[51] + z[53];
    z[51]=z[51]*z[33];
    z[30]=z[30] + z[51];
    z[30]=z[8]*z[30];
    z[51]=static_cast<T>(1)- n<T>(17,2)*z[56];
    z[51]=z[51]*z[37];
    z[30]=z[51] + z[30];
    z[30]=z[8]*z[30];
    z[22]= - z[22] + n<T>(3,2) + z[79];
    z[22]=z[22]*z[47]*z[72];
    z[47]=n<T>(7,2)*z[7];
    z[51]=static_cast<T>(9)- z[47];
    z[51]=z[7]*z[51];
    z[51]= - z[33] - static_cast<T>(5)+ z[51];
    z[51]=z[8]*z[51];
    z[58]= - n<T>(5,3) + n<T>(33,4)*z[7];
    z[58]=z[7]*z[58];
    z[51]=z[51] + n<T>(21,2) + z[58];
    z[51]=z[51]*z[50];
    z[51]=z[51] - static_cast<T>(1)- z[74];
    z[51]=z[8]*z[51];
    z[22]=z[51] + z[22];
    z[22]=z[10]*z[22];
    z[19]=z[27] + z[19] + z[30] + z[22];
    z[19]=z[2]*z[19];
    z[22]=static_cast<T>(15)- z[37];
    z[22]=z[22]*z[37];
    z[22]=z[22] - static_cast<T>(23)- z[45];
    z[27]=n<T>(7,6) + z[4];
    z[27]=11*z[27] - z[47];
    z[27]=n<T>(1,2)*z[27] - z[8];
    z[27]=z[8]*z[27];
    z[22]=n<T>(1,2)*z[22] + z[27];
    z[22]=z[8]*z[22];
    z[27]= - z[75]*z[37];
    z[27]=z[27] + z[88];
    z[30]=z[40] - n<T>(1,3) - z[68];
    z[30]=n<T>(1,2)*z[30] - z[76];
    z[30]=z[8]*z[30];
    z[27]=n<T>(1,2)*z[27] + z[30];
    z[27]=z[10]*z[27];
    z[30]= - n<T>(25,8) + z[7];
    z[30]=z[30]*z[37];
    z[22]=z[27] + z[22] + z[30] + n<T>(22,3) + n<T>(43,8)*z[4];
    z[22]=z[8]*z[22];
    z[22]= - static_cast<T>(2)- n<T>(3,8)*z[74] + z[22];
    z[22]=z[10]*z[22];
    z[27]= - static_cast<T>(37)- z[37];
    z[27]=z[27]*z[40];
    z[30]=z[48] + n<T>(77,6) + z[39];
    z[30]=n<T>(1,2)*z[30] - z[8];
    z[30]=z[8]*z[30];
    z[27]=z[30] + z[27] - n<T>(15,2) - z[29];
    z[27]=z[8]*z[27];
    z[30]=n<T>(1,8)*z[7];
    z[39]=n<T>(85,2) - z[90];
    z[39]=z[39]*z[30];
    z[47]=n<T>(1,8)*z[4];
    z[27]=z[27] + z[39] + n<T>(4,3) - z[47];
    z[27]=z[8]*z[27];
    z[39]=n<T>(9,2)*z[7] + n<T>(31,3) + z[28];
    z[39]=n<T>(1,4)*z[39] - n<T>(2,3)*z[8];
    z[39]=z[8]*z[39];
    z[39]=z[39] - z[59] - static_cast<T>(2)- z[65];
    z[39]=z[8]*z[39];
    z[48]=n<T>(7,2) + z[20];
    z[48]=z[48]*z[40];
    z[51]=n<T>(3,4) - z[76];
    z[51]=z[8]*z[51];
    z[51]= - n<T>(5,12) + z[51];
    z[51]=z[3]*z[51];
    z[39]=z[51] + z[39] + n<T>(1,3) + z[48];
    z[39]=z[3]*z[39];
    z[48]= - static_cast<T>(9)+ z[18];
    z[48]=z[48]*z[47];
    z[48]=static_cast<T>(1)+ z[48];
    z[48]=z[7]*z[48];
    z[51]=n<T>(5,3)*z[4];
    z[58]= - static_cast<T>(19)+ z[51];
    z[48]=n<T>(1,16)*z[58] + z[48];
    z[48]=z[7]*z[48];
    z[27]=z[39] + z[27] - n<T>(1,12) + z[48];
    z[27]=z[3]*z[27];
    z[39]=z[4] + 6;
    z[39]=z[39]*z[4];
    z[39]=z[39] + 10;
    z[48]= - z[4]*z[39];
    z[48]= - n<T>(5,2) + z[48];
    z[48]=z[48]*z[52];
    z[58]=static_cast<T>(10)+ z[28];
    z[58]=z[4]*z[58];
    z[58]=static_cast<T>(2)+ z[58];
    z[48]=2*z[58] + z[48];
    z[48]=z[7]*z[48];
    z[29]= - z[62]*z[29];
    z[58]=z[4] + 4;
    z[58]=z[58]*z[4];
    z[58]=z[58] + 5;
    z[58]=z[58]*z[4];
    z[59]=static_cast<T>(1)+ z[58];
    z[59]=z[7]*z[59];
    z[29]=z[29] + z[59];
    z[29]=z[29]*z[33];
    z[29]=z[48] + z[29];
    z[29]=z[8]*z[29];
    z[33]=z[85] + n<T>(143,2);
    z[33]=z[33]*z[4];
    z[48]=n<T>(33,2) + z[33];
    z[48]=z[48]*z[40];
    z[48]=z[48] - static_cast<T>(6)- n<T>(31,4)*z[4];
    z[48]=z[7]*z[48];
    z[29]=z[48] + z[29];
    z[29]=z[8]*z[29];
    z[48]= - static_cast<T>(5)- z[80];
    z[48]=z[48]*z[30];
    z[48]=static_cast<T>(2)+ z[48];
    z[48]=z[7]*z[48];
    z[19]=z[19] + z[27] + z[22] + z[48] + z[29];
    z[19]=z[2]*z[19];
    z[22]= - n<T>(1,3) + z[18];
    z[22]=z[22]*z[65];
    z[24]= - z[24]*z[52];
    z[24]= - z[34] + z[24];
    z[24]=z[7]*z[24];
    z[27]= - n<T>(25,3) - z[18];
    z[27]=z[27]*z[65];
    z[29]=z[16]*z[8];
    z[27]=n<T>(2,3)*z[29] - n<T>(5,3) + z[27];
    z[27]=z[8]*z[27];
    z[34]=static_cast<T>(1)+ z[47];
    z[34]=z[4]*z[34];
    z[27]=z[27] + n<T>(4,3) + z[34];
    z[27]=z[8]*z[27];
    z[34]= - static_cast<T>(11)- z[42];
    z[34]=n<T>(1,4)*z[34] + z[29];
    z[34]=z[34]*z[76];
    z[47]=z[4] + n<T>(7,3);
    z[34]=n<T>(1,4)*z[47] + z[34];
    z[34]=z[3]*z[34];
    z[22]=z[34] + z[27] + n<T>(1,16)*z[24] - n<T>(1,3) + z[22];
    z[22]=z[3]*z[22];
    z[24]= - n<T>(1,2) + z[28];
    z[24]=z[24]*z[18];
    z[27]= - static_cast<T>(1)+ z[28];
    z[27]=z[27]*z[18];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[4]*z[27];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[7]*z[27];
    z[24]=z[27] - static_cast<T>(1)+ z[24];
    z[24]=z[7]*z[24];
    z[27]= - n<T>(11,3) - z[42];
    z[24]=n<T>(1,4)*z[27] + z[24];
    z[27]= - n<T>(55,3) - z[4];
    z[27]=z[4]*z[27];
    z[27]= - n<T>(29,3) + z[27];
    z[27]=n<T>(1,4)*z[27] + z[29];
    z[27]=z[8]*z[27];
    z[34]=static_cast<T>(4)+ z[86];
    z[34]=z[4]*z[34];
    z[27]=z[27] - z[30] + n<T>(11,6) + z[34];
    z[27]=z[8]*z[27];
    z[34]= - n<T>(13,3) - z[4];
    z[34]=z[4]*z[34];
    z[34]=n<T>(25,6) + z[34];
    z[48]=n<T>(5,2) + z[7];
    z[48]=z[7]*z[48];
    z[34]=n<T>(1,2)*z[34] + z[48];
    z[27]=n<T>(1,4)*z[34] + z[27];
    z[27]=z[8]*z[27];
    z[22]=z[22] + n<T>(1,4)*z[24] + z[27];
    z[22]=z[3]*z[22];
    z[24]=z[84]*z[4];
    z[24]=z[24] + 3;
    z[24]=z[24]*z[4];
    z[24]=z[24] + 1;
    z[24]=z[24]*z[50];
    z[27]=n<T>(17,8) + n<T>(2,3)*z[4];
    z[27]=z[27]*z[4];
    z[27]=z[27] + n<T>(9,4);
    z[27]=z[27]*z[4];
    z[24]= - z[24] + z[27] + n<T>(19,24);
    z[24]=z[24]*z[8];
    z[27]=z[4] + n<T>(91,24);
    z[27]=z[27]*z[4];
    z[27]=z[27] + n<T>(55,12);
    z[27]=z[27]*z[4];
    z[27]=z[27] + n<T>(43,24);
    z[24]=z[24] - n<T>(1,2)*z[27];
    z[24]=z[24]*z[8];
    z[27]=n<T>(7,4)*z[4];
    z[34]=z[27] + 3;
    z[34]=z[34]*z[4];
    z[34]=z[34] + n<T>(7,4);
    z[24]=z[24] + n<T>(1,4)*z[34];
    z[24]=z[24]*z[8];
    z[34]=z[63] + 1;
    z[34]=z[34]*z[4];
    z[34]=z[34] + 1;
    z[34]=z[34]*z[4];
    z[34]=z[34] + n<T>(1,3);
    z[48]=z[34]*z[50];
    z[50]= - n<T>(15,16) - z[63];
    z[50]=z[4]*z[50];
    z[50]= - n<T>(7,8) + z[50];
    z[50]=z[4]*z[50];
    z[48]=z[48] - n<T>(13,48) + z[50];
    z[48]=z[8]*z[48];
    z[50]=n<T>(5,2) + z[4];
    z[50]=z[4]*z[50];
    z[50]=n<T>(9,4) + z[50];
    z[50]=z[4]*z[50];
    z[50]=n<T>(3,4) + z[50];
    z[48]=n<T>(1,2)*z[50] + z[48];
    z[48]=z[8]*z[48];
    z[50]= - n<T>(7,3) - z[65];
    z[50]=z[4]*z[50];
    z[50]= - n<T>(5,2) + z[50];
    z[50]=z[4]*z[50];
    z[50]= - static_cast<T>(3)+ z[50];
    z[50]=z[4]*z[50];
    z[50]= - n<T>(31,12) + z[50];
    z[48]=n<T>(1,4)*z[50] + z[48];
    z[48]=z[10]*z[48];
    z[20]=z[20] + n<T>(5,2);
    z[48]=z[48] + n<T>(1,4)*z[20] - z[24];
    z[48]=z[10]*z[48];
    z[34]=z[34]*z[44];
    z[50]= - n<T>(19,16) - z[63];
    z[50]=z[4]*z[50];
    z[50]= - n<T>(11,8) + z[50];
    z[50]=z[4]*z[50];
    z[50]=z[34] - n<T>(25,48) + z[50];
    z[50]=z[8]*z[50];
    z[59]=static_cast<T>(7)+ n<T>(31,8)*z[4];
    z[59]=z[4]*z[59];
    z[59]=n<T>(25,8) + z[59];
    z[50]=n<T>(1,6)*z[59] + z[50];
    z[50]=z[8]*z[50];
    z[34]=z[34] - z[21];
    z[34]=z[8]*z[34];
    z[34]=z[34] + n<T>(1,3) + z[18];
    z[34]=z[34]*z[49];
    z[27]= - static_cast<T>(1)- z[27];
    z[27]=z[34] + n<T>(1,6)*z[27] + z[50];
    z[27]=z[3]*z[27];
    z[24]=z[27] + n<T>(1,12)*z[16] - z[24];
    z[24]=z[3]*z[24];
    z[27]=n<T>(7,8)*z[4];
    z[34]= - static_cast<T>(1)- z[27];
    z[24]=z[24] + n<T>(1,3)*z[34] + z[48];
    z[24]=z[6]*z[24];
    z[34]=z[42] + 15;
    z[34]=z[34]*z[65];
    z[48]=z[21]*z[8];
    z[34]=z[34] + static_cast<T>(2)- n<T>(3,2)*z[48];
    z[34]=z[34]*z[8];
    z[49]=static_cast<T>(3)+ n<T>(7,6)*z[4];
    z[49]=z[49]*z[4];
    z[34]=z[34] - z[49] - n<T>(11,6);
    z[34]=z[34]*z[8];
    z[49]=z[63] + n<T>(5,4);
    z[49]=z[49]*z[4];
    z[49]=z[49] + n<T>(17,12);
    z[34]=z[34] + n<T>(1,4)*z[49];
    z[34]=z[34]*z[8];
    z[49]=n<T>(11,2) + z[28];
    z[49]=z[4]*z[49];
    z[49]=n<T>(5,2) + z[49];
    z[49]=n<T>(1,2)*z[49] - z[48];
    z[49]=z[8]*z[49];
    z[38]= - z[4]*z[38];
    z[38]= - n<T>(5,2) + z[38];
    z[38]=n<T>(1,2)*z[38] + z[49];
    z[38]=z[8]*z[38];
    z[20]=z[38] + z[20];
    z[20]=z[20]*z[25];
    z[25]=static_cast<T>(1)+ n<T>(1,6)*z[4];
    z[25]=z[4]*z[25];
    z[25]= - n<T>(1,6) + z[25];
    z[20]=z[20] + n<T>(1,2)*z[25] + z[34];
    z[20]=z[10]*z[20];
    z[25]=n<T>(19,8) + z[4];
    z[25]=z[4]*z[25];
    z[25]= - z[48] + n<T>(11,8) + z[25];
    z[25]=z[8]*z[25];
    z[38]= - static_cast<T>(7)- z[51];
    z[38]=z[4]*z[38];
    z[38]= - n<T>(29,6) + z[38];
    z[25]=n<T>(1,4)*z[38] + z[25];
    z[25]=z[8]*z[25];
    z[21]= - z[21]*z[44];
    z[38]=static_cast<T>(1)+ z[65];
    z[38]=z[4]*z[38];
    z[21]=z[21] + n<T>(3,4) + z[38];
    z[21]=z[8]*z[21];
    z[21]= - z[23] + z[21];
    z[21]=z[3]*z[21];
    z[23]=n<T>(13,3) + z[42];
    z[21]=z[21] + n<T>(1,16)*z[23] + z[25];
    z[21]=z[3]*z[21];
    z[23]=static_cast<T>(7)- z[4];
    z[21]=z[21] + n<T>(1,24)*z[23] + z[34];
    z[21]=z[3]*z[21];
    z[23]=z[71]*z[7];
    z[25]=static_cast<T>(3)- n<T>(7,3)*z[4];
    z[25]=n<T>(1,4)*z[25] - z[23];
    z[20]=z[24] + z[21] + n<T>(1,4)*z[25] + z[20];
    z[20]=z[6]*z[20];
    z[21]= - static_cast<T>(3)+ z[18];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(7,2) + z[21];
    z[21]=z[10]*z[21];
    z[21]=z[21] + z[53];
    z[21]=z[10]*z[21];
    z[21]= - z[56] + z[21];
    z[21]=z[6]*z[21];
    z[24]=static_cast<T>(1)+ z[43];
    z[24]=z[10]*z[24];
    z[24]= - z[7] + z[24];
    z[24]=z[6]*z[24];
    z[24]=z[24] - z[57];
    z[24]=z[11]*z[24];
    z[21]=z[24] + z[21] + z[55];
    z[21]=z[81]*z[21];
    z[24]= - z[71]*z[37];
    z[24]=static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[37];
    z[25]= - static_cast<T>(2)- z[18];
    z[25]=z[10]*z[25];
    z[25]= - n<T>(1,8)*z[16] + z[25];
    z[25]=z[10]*z[25];
    z[34]= - static_cast<T>(5)+ z[18];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(11,2) + z[34];
    z[38]= - static_cast<T>(1)+ z[63];
    z[38]=z[4]*z[38];
    z[38]=static_cast<T>(3)+ z[38];
    z[38]=z[4]*z[38];
    z[38]=n<T>(13,3) + z[38];
    z[38]=z[10]*z[38];
    z[34]=n<T>(1,3)*z[34] + z[38];
    z[34]=z[10]*z[34];
    z[23]= - z[23] + z[34];
    z[23]=z[23]*z[70];
    z[21]=z[21] + z[23] + z[24] + z[25];
    z[21]=z[6]*z[21];
    z[23]=z[73]*z[52];
    z[23]= - n<T>(11,4) + z[23];
    z[23]=z[10]*z[23];
    z[18]= - static_cast<T>(5)- z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(11,4) + z[18];
    z[18]=z[7]*z[18];
    z[18]=z[18] + z[23];
    z[18]=n<T>(1,2)*z[18] + z[21];
    z[18]=z[11]*z[18];
    z[21]= - z[4]*z[47];
    z[21]= - n<T>(5,3) + z[21];
    z[21]=n<T>(13,4)*z[21] + z[29];
    z[21]=z[8]*z[21];
    z[23]=static_cast<T>(16)+ n<T>(55,8)*z[4];
    z[23]=z[4]*z[23];
    z[21]=z[21] - z[30] + n<T>(34,3) + z[23];
    z[21]=z[8]*z[21];
    z[23]= - n<T>(295,3) - z[35];
    z[23]=z[4]*z[23];
    z[23]= - n<T>(461,6) + z[23];
    z[23]=n<T>(1,2)*z[23] - z[61];
    z[21]=n<T>(1,4)*z[23] + z[21];
    z[21]=z[8]*z[21];
    z[16]=z[16]*z[76];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=n<T>(1,4)*z[15] + z[16];
    z[15]=z[8]*z[15];
    z[15]=z[15] - z[30] - n<T>(1,2) - z[66];
    z[15]=z[8]*z[15];
    z[16]= - static_cast<T>(1)- n<T>(3,8)*z[7];
    z[16]=z[16]*z[37];
    z[23]=n<T>(2,3) + z[86];
    z[23]=z[4]*z[23];
    z[15]=z[15] + z[16] + n<T>(169,24) + z[23];
    z[15]=z[10]*z[15];
    z[16]= - n<T>(61,6) + z[45];
    z[23]= - static_cast<T>(1)- n<T>(11,4)*z[7];
    z[23]=z[7]*z[23];
    z[16]=n<T>(1,2)*z[16] + z[23];
    z[15]=z[15] + n<T>(1,4)*z[16] + z[21];
    z[15]=z[10]*z[15];
    z[16]= - z[39]*z[28];
    z[21]=static_cast<T>(3)+ z[60];
    z[21]=z[21]*z[52];
    z[16]=z[21] - static_cast<T>(2)+ z[16];
    z[16]=z[7]*z[16];
    z[21]=z[58] - z[41];
    z[21]=z[7]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[46];
    z[16]=z[21] + static_cast<T>(4)+ z[16];
    z[16]=z[8]*z[16];
    z[21]= - n<T>(69,2)*z[41] + n<T>(41,2) + z[33];
    z[21]=z[21]*z[40];
    z[16]=z[16] - static_cast<T>(1)+ z[21];
    z[16]=z[8]*z[16];
    z[21]= - z[54] + z[6];
    z[21]=z[31]*z[21];
    z[23]= - n<T>(1,3) + z[4];
    z[21]=z[21] + n<T>(1,4)*z[23] - z[3];
    z[21]=z[21]*npow(z[3],2);
    z[23]= - n<T>(1,4) - z[3];
    z[23]=z[3]*z[23];
    z[23]= - n<T>(7,4) + z[23];
    z[23]=z[6]*z[23]*z[83];
    z[24]= - n<T>(1,12) + z[3];
    z[24]=z[3]*z[24];
    z[24]=n<T>(7,12) + z[24];
    z[24]=z[3]*z[24];
    z[23]=z[24] + z[23];
    z[23]=z[6]*z[23];
    z[24]=npow(z[3],3)*z[32];
    z[21]=z[24] + z[23] + z[21];
    z[21]=z[1]*z[21];
    z[23]=z[53]*z[37];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[7]*z[23];
    z[24]=z[37]*z[67];
    z[23]=z[24] - n<T>(1,2) + z[23];
    z[23]=z[11]*z[23];
    z[24]= - z[37]*z[4]*z[73];
    z[23]=z[24] + z[23];
    z[23]=z[13]*z[23];
    z[24]= - n<T>(103,6) - z[36];
    z[25]=static_cast<T>(5)- z[27];
    z[25]=z[4]*z[25];
    z[25]=n<T>(33,4) + z[25];
    z[25]=z[7]*z[25];
    z[24]=n<T>(1,2)*z[24] + z[25];
    z[24]=z[7]*z[24];
    z[24]= - n<T>(1,3) + z[24];

    r += z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + n<T>(1,4)*
      z[21] + z[22] + n<T>(1,3)*z[23] + n<T>(1,2)*z[24] + z[26];
 
    return r;
}

template double qg_2lha_r489(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r489(const std::array<dd_real,30>&);
#endif
