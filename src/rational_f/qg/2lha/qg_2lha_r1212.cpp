#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1212(const std::array<T,30>& k) {
  T z[56];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[6];
    z[6]=k[8];
    z[7]=k[28];
    z[8]=k[27];
    z[9]=k[2];
    z[10]=k[10];
    z[11]=k[14];
    z[12]=k[9];
    z[13]=k[11];
    z[14]=k[3];
    z[15]=z[2]*z[5];
    z[16]=n<T>(1,2)*z[15];
    z[17]=n<T>(1,2)*z[5];
    z[18]=z[17] - 1;
    z[19]=z[16] + z[18];
    z[20]=z[19]*z[3];
    z[21]=z[12]*z[19];
    z[21]=z[21] + z[20];
    z[21]=z[11]*z[21];
    z[22]=n<T>(1,2)*z[12];
    z[19]=z[19]*z[22];
    z[23]=n<T>(17,16)*z[5] - static_cast<T>(2)+ n<T>(5,4)*z[15];
    z[19]=z[19] - z[23];
    z[19]=z[12]*z[19];
    z[20]=n<T>(1,2)*z[20] - z[23];
    z[20]=z[3]*z[20];
    z[19]=z[21] + z[19] + z[20];
    z[20]=n<T>(1,3)*z[11];
    z[19]=z[19]*z[20];
    z[21]=n<T>(1,3)*z[15];
    z[23]=static_cast<T>(2)- n<T>(5,4)*z[5];
    z[23]=z[23]*z[21];
    z[24]= - static_cast<T>(1)+ n<T>(5,6)*z[5];
    z[25]= - z[21] + n<T>(1,3) - n<T>(3,16)*z[5];
    z[25]=z[12]*z[25];
    z[23]=z[25] + n<T>(1,4)*z[24] + z[23];
    z[23]=z[12]*z[23];
    z[24]=static_cast<T>(1)- n<T>(1,3)*z[4];
    z[25]=n<T>(1,2)*z[4];
    z[26]=npow(z[5],2);
    z[24]=z[25]*z[26]*z[24];
    z[27]=n<T>(1,2)*z[2];
    z[28]=z[27]*z[26];
    z[29]= - n<T>(1,2) - z[5];
    z[29]=z[5]*z[29];
    z[29]=z[29] + z[28];
    z[24]=n<T>(1,3)*z[29] + z[24];
    z[24]=z[14]*z[24];
    z[29]=n<T>(1,4)*z[15];
    z[30]= - z[29] - z[18];
    z[31]=n<T>(1,3)*z[3];
    z[30]=z[30]*z[31];
    z[32]= - static_cast<T>(2)+ n<T>(5,8)*z[5];
    z[29]=z[30] + n<T>(1,3)*z[32] + z[29];
    z[29]=z[3]*z[29];
    z[30]=z[5] - n<T>(1,2);
    z[32]= - z[30]*z[15];
    z[33]=z[5] - 1;
    z[34]=z[5]*z[33];
    z[32]=z[32] - n<T>(1,2) + z[34];
    z[34]=z[18]*z[5];
    z[35]= - z[4]*z[34];
    z[32]=n<T>(1,2)*z[32] + z[35];
    z[19]=z[19] + z[29] + n<T>(5,2)*z[24] + n<T>(5,3)*z[32] + z[23];
    z[19]=z[11]*z[19];
    z[23]=5*z[2];
    z[24]=z[23]*z[26];
    z[29]=static_cast<T>(5)+ n<T>(13,3)*z[5];
    z[29]=z[5]*z[29];
    z[29]=z[29] + z[24];
    z[29]=z[29]*z[27];
    z[32]=n<T>(1,3)*z[5];
    z[35]=static_cast<T>(1)+ z[32];
    z[35]=z[5]*z[35];
    z[29]=z[35] + z[29];
    z[35]=9*z[26] + z[24];
    z[35]=z[14]*z[35];
    z[36]=z[32]*z[4];
    z[37]=n<T>(1,3)*z[12];
    z[38]= - z[4]*z[37];
    z[29]=n<T>(1,8)*z[35] + z[38] + n<T>(1,4)*z[29] + z[36];
    z[35]=n<T>(1,2)*z[14];
    z[29]=z[29]*z[35];
    z[38]=z[14]*z[12];
    z[39]=static_cast<T>(1)- z[12];
    z[39]=z[39]*z[38];
    z[40]=n<T>(1,2)*z[8];
    z[41]=z[12] - z[38];
    z[41]=z[41]*z[40];
    z[42]= - static_cast<T>(1)+ z[22];
    z[42]=z[12]*z[42];
    z[39]=z[41] + z[42] + n<T>(1,2)*z[39];
    z[39]=z[8]*z[39];
    z[41]=n<T>(11,3) - n<T>(5,2)*z[2];
    z[41]=z[3]*z[41];
    z[39]=z[39] + z[41];
    z[41]=n<T>(1,4)*z[2];
    z[42]= - 3*z[5] + n<T>(5,2)*z[15];
    z[42]=z[42]*z[41];
    z[36]= - z[36] + z[42] - n<T>(3,8) + z[32];
    z[42]= - n<T>(7,16)*z[12] + static_cast<T>(1)+ z[25];
    z[42]=z[42]*z[37];
    z[43]= - static_cast<T>(29)+ 7*z[12];
    z[43]=z[12]*z[43];
    z[44]=z[3] + z[12];
    z[45]=z[11]*z[44];
    z[43]=7*z[45] + z[43] - 29*z[3];
    z[43]=z[11]*z[43];
    z[29]=n<T>(1,48)*z[43] + z[29] + n<T>(1,2)*z[36] + z[42] + n<T>(1,8)*z[39];
    z[29]=z[10]*z[29];
    z[36]=n<T>(1,4)*z[5];
    z[39]=static_cast<T>(19)+ 61*z[5];
    z[39]=z[39]*z[36];
    z[42]=n<T>(1,4)*z[4];
    z[43]=z[26] - 1;
    z[45]=19*z[43] - 7*z[2];
    z[45]=z[45]*z[42];
    z[45]=z[45] + static_cast<T>(1)- n<T>(53,4)*z[26];
    z[45]=z[4]*z[45];
    z[39]=z[39] + z[45];
    z[45]=n<T>(5,3)*z[5];
    z[46]= - z[45] - z[16];
    z[46]=z[2]*z[46];
    z[47]=static_cast<T>(13)+ 9*z[2];
    z[47]=z[47]*z[25];
    z[46]=z[47] + n<T>(17,6)*z[5] + z[46];
    z[46]=z[12]*z[46];
    z[47]= - static_cast<T>(1)+ n<T>(5,4)*z[2];
    z[48]= - n<T>(25,48) - z[2];
    z[48]=z[2]*z[48];
    z[48]=n<T>(35,48) + z[48];
    z[48]=z[4]*z[48];
    z[47]=n<T>(1,3)*z[47] + z[48];
    z[47]=z[4]*z[47];
    z[16]= - 11*z[5] + z[16];
    z[16]=n<T>(1,8)*z[46] + n<T>(1,24)*z[16] + z[47];
    z[16]=z[12]*z[16];
    z[46]=5*z[5];
    z[47]= - z[46] + z[21];
    z[48]=n<T>(1,3)*z[2];
    z[49]= - static_cast<T>(5)+ z[48];
    z[50]= - n<T>(3,4) - z[48];
    z[50]=z[2]*z[50];
    z[50]= - n<T>(5,12) + z[50];
    z[50]=z[4]*z[50];
    z[49]=n<T>(1,16)*z[49] + z[50];
    z[49]=z[4]*z[49];
    z[47]=n<T>(1,16)*z[47] + z[49];
    z[47]=z[12]*z[47];
    z[49]= - static_cast<T>(5)- z[2];
    z[49]=z[4]*z[49];
    z[49]=n<T>(1,3) + n<T>(1,8)*z[49];
    z[49]=z[4]*z[49];
    z[49]=z[32] + z[49];
    z[47]=n<T>(1,2)*z[49] + z[47];
    z[47]=z[12]*z[47];
    z[49]=z[2] + 1;
    z[50]=z[49]*z[4];
    z[51]=static_cast<T>(1)+ z[50];
    z[51]=z[4]*z[51];
    z[51]=z[5] + z[51];
    z[51]=z[14]*z[51]*npow(z[12],2);
    z[47]=z[47] + n<T>(1,12)*z[51];
    z[47]=z[14]*z[47];
    z[16]=z[47] + n<T>(1,12)*z[39] + z[16];
    z[16]=z[14]*z[16];
    z[39]= - n<T>(5,3) + z[5];
    z[39]=z[39]*z[17];
    z[21]= - z[30]*z[21];
    z[21]=z[39] + z[21];
    z[39]=z[36] - 1;
    z[39]=z[39]*z[32];
    z[27]=z[27] - 1;
    z[47]=z[27]*z[2];
    z[39]=z[39] + n<T>(1,4) + n<T>(1,6)*z[47];
    z[39]=z[39]*z[4];
    z[47]=z[49]*z[41];
    z[49]=n<T>(11,4) - z[5];
    z[49]=z[5]*z[49];
    z[47]=z[47] - n<T>(5,4) + z[49];
    z[47]=n<T>(1,3)*z[47] + z[39];
    z[47]=z[4]*z[47];
    z[21]=n<T>(1,2)*z[21] + z[47];
    z[47]=z[2] - 1;
    z[49]=z[47]*z[2];
    z[49]=z[49] + 1;
    z[51]= - z[12]*z[49];
    z[51]=z[2] + z[51];
    z[51]=z[51]*z[38];
    z[38]= - z[49]*z[38];
    z[52]=z[15] - 1;
    z[52]=z[52]*z[2];
    z[52]=z[52] + 1;
    z[52]=z[52]*z[2];
    z[52]=z[52] - 1;
    z[52]=z[52]*z[2];
    z[52]=z[52] + 1;
    z[53]=z[12]*z[52];
    z[38]=z[53] + z[38];
    z[38]=z[8]*z[38];
    z[38]=z[51] + z[38];
    z[45]= - n<T>(1,4) - z[45];
    z[45]=z[45]*z[15];
    z[51]=z[5] + 1;
    z[45]=n<T>(5,3)*z[51] + z[45];
    z[53]=npow(z[2],2);
    z[45]=z[45]*z[53];
    z[54]=n<T>(1,4)*z[12];
    z[55]=z[52]*z[54];
    z[45]=z[55] - n<T>(1,4) + z[45];
    z[45]=z[45]*z[54];
    z[21]=5*z[21] + z[45] + n<T>(1,16)*z[38];
    z[21]=z[8]*z[21];
    z[38]=z[32] - 1;
    z[38]=z[38]*z[5];
    z[38]=z[38] + z[48];
    z[38]= - z[39] + n<T>(1,4)*z[38];
    z[39]=z[8]*z[38];
    z[45]= - static_cast<T>(25)+ n<T>(61,2)*z[5];
    z[32]=z[45]*z[32];
    z[45]=n<T>(17,3)*z[5];
    z[55]=n<T>(19,2) - z[45];
    z[55]=z[5]*z[55];
    z[55]= - n<T>(11,6)*z[2] - n<T>(23,6) + z[55];
    z[55]=z[4]*z[55];
    z[32]=z[32] + z[55];
    z[32]=n<T>(1,8)*z[32] - 5*z[39];
    z[32]=z[4]*z[32];
    z[39]=static_cast<T>(7)- n<T>(13,2)*z[5];
    z[39]=z[5]*z[39];
    z[39]=static_cast<T>(11)+ z[39];
    z[23]=n<T>(1,2)*z[39] - z[23];
    z[39]= - n<T>(23,4) + z[2];
    z[39]=z[39]*z[48];
    z[55]= - static_cast<T>(61)+ 19*z[5];
    z[55]=z[5]*z[55];
    z[55]=static_cast<T>(7)+ n<T>(1,6)*z[55];
    z[39]=n<T>(1,2)*z[55] + z[39];
    z[39]=z[4]*z[39];
    z[23]=n<T>(1,3)*z[23] + z[39];
    z[23]=z[23]*z[42];
    z[23]=n<T>(1,3) + z[23];
    z[23]=z[3]*z[23];
    z[39]= - static_cast<T>(3)+ z[5];
    z[17]=z[39]*z[17];
    z[17]=z[17] - z[27];
    z[17]=z[4]*z[17];
    z[39]=n<T>(17,4) + z[34];
    z[17]=n<T>(1,3)*z[39] + n<T>(5,2)*z[17];
    z[39]=z[1]*z[3];
    z[17]=z[17]*z[42]*z[39];
    z[17]=z[17] + z[23] + z[32];
    z[17]=z[1]*z[17];
    z[23]=z[5] + z[47];
    z[23]=z[23]*z[25];
    z[32]= - z[4] - z[3];
    z[47]=z[34] + n<T>(1,2);
    z[32]=z[1]*z[47]*z[32];
    z[30]= - z[5]*z[30];
    z[23]=z[32] + z[30] + z[23];
    z[23]=z[1]*z[23];
    z[30]=z[34] + z[28];
    z[30]=z[2]*z[30];
    z[32]= - z[53]*z[25];
    z[34]=z[53]*z[50];
    z[34]=z[2] + z[34];
    z[22]=z[34]*z[22];
    z[22]=z[23] + z[22] + z[30] + z[32];
    z[22]=z[7]*z[22];
    z[23]= - n<T>(29,4) + z[46];
    z[23]=z[5]*z[23];
    z[23]=z[23] + z[24];
    z[23]=z[23]*z[48];
    z[24]= - n<T>(5,2) - n<T>(19,3)*z[2];
    z[24]=z[2]*z[24];
    z[24]= - n<T>(47,6) + z[24];
    z[24]=z[24]*z[25];
    z[25]=z[5] - z[15];
    z[25]=z[2]*z[25];
    z[25]=7*z[5] + z[25];
    z[25]=z[25]*z[41];
    z[30]= - static_cast<T>(5)- z[36];
    z[25]=n<T>(1,3)*z[30] + z[25];
    z[25]=z[12]*z[25];
    z[23]=z[25] + z[24] + z[23] + n<T>(3,2) + z[5];
    z[23]=z[23]*z[54];
    z[24]=z[14]*z[49];
    z[24]=z[24] - z[52];
    z[24]=z[8]*z[24];
    z[25]=z[5]*npow(z[2],3);
    z[30]= - z[14]*z[2];
    z[24]=z[24] + z[30] + static_cast<T>(1)+ z[25];
    z[24]=z[8]*z[24];
    z[15]= - 5*z[15] + static_cast<T>(9)+ z[46];
    z[15]=z[2]*z[15];
    z[25]=z[4]*z[46];
    z[15]=z[24] + z[25] - 5*z[51] + z[15];
    z[24]=z[5]*z[51];
    z[24]=z[24] - z[28];
    z[24]=z[2]*z[24];
    z[25]= - static_cast<T>(1)- n<T>(3,2)*z[5];
    z[25]=z[5]*z[25];
    z[24]=z[24] - n<T>(1,2) + z[25];
    z[25]=static_cast<T>(1)- z[42];
    z[25]=z[4]*z[26]*z[25];
    z[24]=n<T>(1,2)*z[24] + z[25];
    z[24]=z[14]*z[24];
    z[25]= - static_cast<T>(1)+ z[14];
    z[25]=z[25]*z[40];
    z[25]=z[25] + static_cast<T>(1)- z[35];
    z[25]=z[8]*z[25];
    z[25]= - n<T>(1,2) + z[25];
    z[25]=z[10]*z[25];
    z[15]=n<T>(1,2)*z[25] + 5*z[24] + n<T>(1,4)*z[15];
    z[15]=z[13]*z[15];
    z[24]= - z[4]*z[38];
    z[18]=z[3]*z[18];
    z[18]=n<T>(1,6)*z[18] - n<T>(1,48)*z[33] + z[24];
    z[18]=z[3]*z[18];
    z[24]=static_cast<T>(1)- z[37];
    z[24]=z[12]*z[24];
    z[25]=static_cast<T>(1)- z[31];
    z[25]=z[3]*z[25];
    z[24]=z[24] + z[25];
    z[20]= - z[44]*z[20];
    z[20]=n<T>(1,2)*z[24] + z[20];
    z[20]=z[11]*z[20];
    z[24]=npow(z[3],2);
    z[20]=n<T>(1,2)*z[24] + z[20];
    z[20]=z[11]*z[20];
    z[25]=z[27]*z[24];
    z[20]=n<T>(1,3)*z[25] + z[20];
    z[20]=z[10]*z[20];
    z[20]= - n<T>(5,8)*z[13] + n<T>(1,6)*z[24] + z[20];
    z[20]=z[9]*z[20];
    z[24]=z[10]*npow(z[14],2);
    z[24]=z[24] + z[14];
    z[24]=z[26]*z[24];
    z[25]=z[5] - 2;
    z[25]=z[25]*z[5];
    z[25]=z[25] + 1;
    z[27]=z[25]*z[39];
    z[27]=z[27] + z[43];
    z[27]=z[1]*z[27];
    z[24]=z[27] + z[24];
    z[24]=z[6]*z[24];
    z[27]=static_cast<T>(1)+ n<T>(11,3)*z[5];
    z[27]=z[5]*z[27];
    z[26]=z[2]*z[26];
    z[26]= - 3*z[26] + n<T>(5,2) + z[27];
    z[25]= - z[53] + z[25];
    z[25]=z[4]*z[25];
    z[27]=n<T>(23,4) - z[45];
    z[27]=z[5]*z[27];
    z[27]=n<T>(19,12)*z[2] - n<T>(1,12) + z[27];
    z[25]=n<T>(1,4)*z[27] + n<T>(2,3)*z[25];
    z[25]=z[4]*z[25];

    r += n<T>(1,4)*z[15] + z[16] + z[17] + z[18] + z[19] + n<T>(1,2)*z[20] + 
      z[21] + n<T>(3,4)*z[22] + z[23] + n<T>(1,3)*z[24] + z[25] + n<T>(1,8)*z[26]
       + z[29];
 
    return r;
}

template double qg_2lha_r1212(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1212(const std::array<dd_real,30>&);
#endif
