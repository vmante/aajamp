#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1916(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[4];
    z[6]=z[5] + 2;
    z[7]=z[6]*z[5];
    z[7]=z[7] + 1;
    z[8]=z[7]*z[4];
    z[9]= - static_cast<T>(4)- z[5];
    z[9]=z[5]*z[9];
    z[9]=z[8] - static_cast<T>(3)+ z[9];
    z[9]=z[4]*z[9];
    z[10]=2*z[5];
    z[11]=z[1] - 2;
    z[9]=z[9] + z[10] - z[11];
    z[12]=z[5] + 1;
    z[13]=z[12]*z[4];
    z[13]=z[13] - 3;
    z[14]=z[10] - z[13];
    z[14]=z[4]*z[14];
    z[14]=z[14] + z[1] - z[6];
    z[15]= - static_cast<T>(3)+ z[4];
    z[15]=z[4]*z[15];
    z[15]=z[15] - z[11];
    z[15]=z[2]*z[15];
    z[14]=3*z[14] + z[15];
    z[14]=z[2]*z[14];
    z[9]=3*z[9] + z[14];
    z[9]=z[2]*z[9];
    z[14]=z[5] + 3;
    z[14]=z[14]*z[5];
    z[14]=z[14] + 3;
    z[14]=z[14]*z[5];
    z[14]=z[14] + 1;
    z[14]=z[14]*z[4];
    z[7]=z[14] - 3*z[7];
    z[7]=z[7]*z[4];
    z[7]=z[7] + 3*z[5];
    z[9]=z[9] + z[11] - z[7];
    z[9]=z[3]*z[9];
    z[8]=3*z[12] - z[8];
    z[8]=z[4]*z[8];
    z[8]=z[8] + z[1];
    z[11]=z[4]*z[13];
    z[11]=z[2] + z[11] - z[1] - z[12];
    z[11]=z[2]*z[11];
    z[8]=z[11] - z[10] - static_cast<T>(1)+ 2*z[8];
    z[8]=z[2]*z[8];
    z[7]=z[9] + z[8] + static_cast<T>(1)- z[1] + z[7];
    z[7]=z[3]*z[7];
    z[6]=z[2] - z[6];
    z[6]=z[2]*z[6];
    z[6]=z[7] + static_cast<T>(1)+ z[6];

    r += z[6]*npow(z[3],2);
 
    return r;
}

template double qg_2lha_r1916(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1916(const std::array<dd_real,30>&);
#endif
