#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1404(const std::array<T,30>& k) {
  T z[56];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[11];
    z[8]=k[4];
    z[9]=k[14];
    z[10]=k[9];
    z[11]=k[10];
    z[12]=7*z[1];
    z[13]=static_cast<T>(11)+ z[12];
    z[13]=z[1]*z[13];
    z[13]=static_cast<T>(13)+ z[13];
    z[14]=n<T>(7,2)*z[5] - n<T>(11,2) - z[12];
    z[14]=z[5]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[14]=n<T>(1,6)*z[5];
    z[13]=z[13]*z[14];
    z[15]=n<T>(1,3)*z[1];
    z[14]=z[14] - n<T>(3,8) - z[15];
    z[14]=z[5]*z[14];
    z[16]=n<T>(3,4) + z[15];
    z[16]=z[1]*z[16];
    z[16]=z[16] + static_cast<T>(1)+ n<T>(7,12)*z[11];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[16]=npow(z[5],2);
    z[14]=z[6]*z[14]*z[16];
    z[13]=z[13] + z[14];
    z[13]=z[6]*z[13];
    z[14]=n<T>(1,4)*z[10];
    z[17]=5*z[10];
    z[18]=n<T>(67,3) - z[17];
    z[18]=z[18]*z[14];
    z[19]=n<T>(1,3)*z[10];
    z[20]= - n<T>(23,2) + z[10];
    z[20]=z[20]*z[19];
    z[21]=z[9]*z[10];
    z[20]=z[20] + z[21];
    z[20]=z[9]*z[20];
    z[18]=z[18] + z[20];
    z[20]=n<T>(1,2)*z[9];
    z[18]=z[18]*z[20];
    z[22]=z[10] - n<T>(11,6);
    z[22]=z[22]*z[10];
    z[18]=z[22] + z[18];
    z[18]=z[9]*z[18];
    z[23]=n<T>(1,3)*z[5];
    z[24]=5*z[1];
    z[25]=n<T>(5,2)*z[5] - n<T>(7,4) - z[24];
    z[25]=z[25]*z[23];
    z[26]=n<T>(1,2) + z[24];
    z[26]=z[1]*z[26];
    z[25]=z[25] + static_cast<T>(1)+ n<T>(1,6)*z[26];
    z[26]=13*z[10];
    z[27]= - static_cast<T>(1)- z[26];
    z[27]=z[10]*z[27];
    z[13]=z[18] + n<T>(1,24)*z[27] + n<T>(1,2)*z[25] + z[13];
    z[18]=n<T>(5,2)*z[10];
    z[25]= - n<T>(11,3) + z[10];
    z[25]=z[25]*z[18];
    z[27]=3*z[9];
    z[28]=z[27]*z[10];
    z[29]=static_cast<T>(9)- z[10];
    z[29]=z[10]*z[29];
    z[29]=z[29] - z[28];
    z[29]=z[9]*z[29];
    z[25]=z[25] + z[29];
    z[25]=z[25]*z[20];
    z[22]= - z[22] + z[25];
    z[22]=z[9]*z[22];
    z[25]=n<T>(1,2)*z[5];
    z[29]=z[1] - z[25];
    z[22]=n<T>(1,2)*z[29] + z[22];
    z[29]=n<T>(43,3) - z[17];
    z[29]=z[29]*z[14];
    z[30]= - n<T>(13,2) + z[10];
    z[30]=z[10]*z[30];
    z[30]=z[30] + z[28];
    z[30]=z[9]*z[30];
    z[29]=z[29] + z[30];
    z[30]=npow(z[9],2);
    z[29]=z[29]*z[30];
    z[29]=n<T>(1,6) + z[29];
    z[31]=static_cast<T>(1)- z[14];
    z[31]=z[31]*z[19];
    z[32]=n<T>(1,4)*z[9];
    z[33]= - z[10]*z[32];
    z[31]=z[31] + z[33];
    z[33]=npow(z[9],3);
    z[31]=z[2]*z[31]*z[33];
    z[29]=n<T>(1,4)*z[29] + z[31];
    z[29]=z[2]*z[29];
    z[22]=n<T>(1,2)*z[22] + z[29];
    z[22]=z[2]*z[22];
    z[13]=n<T>(1,2)*z[13] + z[22];
    z[13]=z[4]*z[13];
    z[22]=3*z[10];
    z[29]= - static_cast<T>(1)+ n<T>(3,8)*z[10];
    z[29]=z[29]*z[22];
    z[31]=n<T>(15,2) - z[10];
    z[31]=z[10]*z[31];
    z[31]=z[31] - z[28];
    z[31]=z[31]*z[20];
    z[29]=z[29] + z[31];
    z[29]=z[9]*z[29];
    z[31]=n<T>(19,4) - z[22];
    z[31]=z[31]*z[14];
    z[29]=z[31] + z[29];
    z[29]=z[9]*z[29];
    z[31]= - static_cast<T>(5)+ z[10];
    z[31]=z[10]*z[31];
    z[28]=z[31] + z[28];
    z[28]=z[9]*z[28];
    z[31]=z[10] - n<T>(5,3);
    z[31]=z[31]*z[10];
    z[28]= - z[31] + z[28];
    z[34]=n<T>(1,4)*z[2];
    z[28]=z[28]*z[30]*z[34];
    z[28]=z[28] - n<T>(5,12) + z[29];
    z[28]=z[2]*z[28];
    z[29]= - static_cast<T>(3)- n<T>(7,6)*z[11];
    z[29]=z[23] + n<T>(1,4)*z[29] - z[15];
    z[29]=z[5]*z[29];
    z[35]=n<T>(17,2)*z[1] + static_cast<T>(11)+ n<T>(23,2)*z[11];
    z[29]=n<T>(1,24)*z[35] + z[29];
    z[29]=z[6]*z[5]*z[29];
    z[35]=z[5] - z[1];
    z[36]= - n<T>(59,16) - z[11] + n<T>(13,4)*z[35];
    z[23]=z[36]*z[23];
    z[24]=static_cast<T>(3)+ z[24];
    z[23]=z[29] + n<T>(1,16)*z[24] + z[23];
    z[23]=z[6]*z[23];
    z[24]=n<T>(1,2)*z[10];
    z[29]=n<T>(37,3) - n<T>(7,2)*z[10];
    z[29]=z[29]*z[24];
    z[21]=n<T>(3,2)*z[21];
    z[36]= - static_cast<T>(5)+ z[24];
    z[36]=z[10]*z[36];
    z[36]=z[36] + z[21];
    z[36]=z[9]*z[36];
    z[29]=z[29] + z[36];
    z[29]=z[9]*z[29];
    z[36]= - n<T>(19,12) + z[10];
    z[18]=z[36]*z[18];
    z[18]=z[18] + z[29];
    z[18]=z[18]*z[20];
    z[29]= - z[6] + z[10];
    z[36]=z[5] - 1;
    z[29]=z[7]*z[36]*z[29];
    z[29]=z[29] - n<T>(1,4) + 3*z[35];
    z[35]=n<T>(1,8)*z[10];
    z[37]=n<T>(13,6) - z[17];
    z[37]=z[37]*z[35];
    z[13]=z[13] + z[28] + z[18] + z[37] + z[23] + n<T>(1,4)*z[29];
    z[13]=z[4]*z[13];
    z[18]=n<T>(1,2)*z[8];
    z[23]=z[18] - 1;
    z[28]=z[7]*z[8];
    z[23]=z[23]*z[28];
    z[29]=n<T>(1,4) + z[23];
    z[29]=z[7]*z[29];
    z[29]= - n<T>(1,24) + z[29];
    z[29]=z[7]*z[29];
    z[37]=npow(z[7],2);
    z[38]= - n<T>(1,2) - z[28];
    z[38]=z[38]*z[37];
    z[39]=n<T>(1,2)*z[2];
    z[40]=npow(z[7],3)*z[39];
    z[38]=z[38] + z[40];
    z[38]=z[2]*z[38];
    z[29]=z[29] + z[38];
    z[29]=z[2]*z[29];
    z[38]=z[8] - 1;
    z[38]=z[38]*z[28];
    z[40]=npow(z[8],2);
    z[41]=n<T>(1,2) + z[40];
    z[41]=n<T>(1,2)*z[41] + z[38];
    z[41]=z[7]*z[41];
    z[42]= - n<T>(1,2) + 3*z[8];
    z[41]=n<T>(1,2)*z[42] + z[41];
    z[41]=z[7]*z[41];
    z[42]=z[20] - 1;
    z[43]= - z[42]*z[27];
    z[43]= - n<T>(5,3) + z[43];
    z[43]=z[9]*z[43];
    z[43]=n<T>(3,2) + z[43];
    z[43]=z[9]*z[43];
    z[44]=z[11]*z[18];
    z[44]= - static_cast<T>(5)+ z[44];
    z[29]=z[29] + z[43] + n<T>(1,6)*z[44] + z[41];
    z[29]=z[29]*z[39];
    z[41]=n<T>(5,2) - z[9];
    z[41]=z[41]*z[20];
    z[41]= - static_cast<T>(1)+ z[41];
    z[41]=z[41]*z[27];
    z[41]=n<T>(19,16) + z[41];
    z[41]=z[9]*z[41];
    z[43]= - static_cast<T>(5)+ z[27];
    z[43]=z[9]*z[43];
    z[43]=n<T>(5,3) + z[43];
    z[43]=z[43]*z[30];
    z[44]= - z[7]*z[34];
    z[43]=z[44] + n<T>(7,12) + z[43];
    z[43]=z[43]*z[34];
    z[44]= - n<T>(7,4) + n<T>(5,3)*z[1];
    z[41]=z[43] + n<T>(1,4)*z[44] + z[41];
    z[41]=z[2]*z[41];
    z[43]= - n<T>(13,2) + z[27];
    z[43]=z[9]*z[43];
    z[43]=n<T>(43,12) + z[43];
    z[43]=z[43]*z[30];
    z[44]= - n<T>(1,4) - z[1];
    z[43]=n<T>(1,3)*z[44] + z[43];
    z[44]=n<T>(1,3) - z[32];
    z[33]=z[44]*z[33];
    z[33]= - n<T>(1,12) + z[33];
    z[33]=z[2]*z[33];
    z[33]=n<T>(1,4)*z[43] + z[33];
    z[33]=z[2]*z[33];
    z[43]=z[9] - 3;
    z[27]= - z[43]*z[27];
    z[27]= - n<T>(55,6) + z[27];
    z[27]=z[9]*z[27];
    z[27]=n<T>(11,3) + z[27];
    z[27]=z[9]*z[27];
    z[44]=n<T>(1,2)*z[1];
    z[45]=n<T>(1,3) - z[44];
    z[45]=z[1]*z[45];
    z[27]=z[27] - n<T>(1,2) + z[45];
    z[27]=n<T>(1,4)*z[27] + z[33];
    z[27]=z[2]*z[27];
    z[33]=npow(z[1],2);
    z[33]=n<T>(11,6) + z[33];
    z[45]= - n<T>(23,6) + z[9];
    z[45]=z[9]*z[45];
    z[45]=n<T>(67,12) + z[45];
    z[45]=z[9]*z[45];
    z[45]= - n<T>(11,3) + z[45];
    z[45]=z[9]*z[45];
    z[33]=n<T>(1,2)*z[33] + z[45];
    z[27]=n<T>(1,4)*z[33] + z[27];
    z[27]=z[4]*z[27];
    z[33]=n<T>(31,2) - 11*z[1];
    z[45]= - static_cast<T>(5)+ n<T>(3,2)*z[9];
    z[45]=z[9]*z[45];
    z[45]=n<T>(37,6) + z[45];
    z[45]=z[9]*z[45];
    z[45]= - n<T>(95,24) + z[45];
    z[45]=z[9]*z[45];
    z[33]=n<T>(1,12)*z[33] + z[45];
    z[27]=z[27] + n<T>(1,2)*z[33] + z[41];
    z[27]=z[4]*z[27];
    z[33]=7*z[11];
    z[41]=z[6]*z[11];
    z[45]= - z[33] + n<T>(13,4)*z[41];
    z[46]=n<T>(1,3)*z[6];
    z[45]=z[45]*z[46];
    z[47]=n<T>(7,3)*z[6];
    z[48]=z[11] - n<T>(1,2)*z[41];
    z[48]=z[48]*z[47];
    z[48]= - z[11] + z[48];
    z[48]=z[8]*z[48];
    z[45]=z[48] + n<T>(5,4)*z[11] + z[45];
    z[45]=z[8]*z[45];
    z[48]=7*z[6];
    z[49]=static_cast<T>(17)- z[48];
    z[45]=n<T>(1,6)*z[49] + z[45];
    z[49]= - static_cast<T>(1)+ n<T>(1,4)*z[6];
    z[49]=z[49]*z[6];
    z[49]=z[49] + n<T>(3,4);
    z[49]=z[49]*z[8];
    z[50]=z[6] - 1;
    z[51]=n<T>(1,2)*z[50];
    z[49]=z[49] + z[51];
    z[49]=z[49]*z[28];
    z[52]=npow(z[6],2);
    z[53]=z[6] - 3;
    z[53]=z[53]*z[6];
    z[54]=static_cast<T>(3)+ z[53];
    z[54]=z[8]*z[54];
    z[52]= - z[52] + z[54];
    z[52]=z[8]*z[52];
    z[52]=z[52] - z[50];
    z[52]=n<T>(1,8)*z[52] + z[49];
    z[52]=z[7]*z[52];
    z[54]=n<T>(1,2)*z[6];
    z[55]= - static_cast<T>(1)+ z[54];
    z[47]=z[55]*z[47];
    z[47]=static_cast<T>(1)+ z[47];
    z[47]=z[8]*z[47];
    z[53]=n<T>(9,2) + z[53];
    z[47]=n<T>(1,2)*z[53] + z[47];
    z[47]=z[8]*z[47];
    z[47]=z[51] + z[47];
    z[47]=n<T>(1,4)*z[47] + z[52];
    z[47]=z[7]*z[47];
    z[51]=n<T>(101,4) + 5*z[8];
    z[52]=static_cast<T>(3)+ z[8];
    z[52]=z[9]*z[52];
    z[52]=z[52] - n<T>(17,2) - n<T>(7,3)*z[8];
    z[52]=z[9]*z[52];
    z[51]=n<T>(1,3)*z[51] + z[52];
    z[51]=z[9]*z[51];
    z[52]= - n<T>(55,4) - z[8];
    z[51]=n<T>(1,3)*z[52] + z[51];
    z[51]=z[51]*z[32];
    z[27]=z[27] + z[29] + z[51] + n<T>(1,4)*z[45] + z[47];
    z[29]=static_cast<T>(1)- z[46];
    z[29]=z[29]*z[54];
    z[29]= - n<T>(1,3) + z[29];
    z[29]=z[8]*z[29];
    z[29]= - n<T>(5,4)*z[50] + z[29];
    z[29]=z[8]*z[29];
    z[29]=z[29] + z[49];
    z[29]=z[7]*z[29];
    z[45]= - z[11] + n<T>(1,3)*z[41];
    z[45]=z[45]*z[54];
    z[46]=n<T>(1,3)*z[11];
    z[45]=z[46] + z[45];
    z[45]=z[45]*z[40];
    z[29]=z[29] - n<T>(7,6) + z[45];
    z[23]=n<T>(5,2)*z[8] + z[23];
    z[23]=z[7]*z[23];
    z[45]=z[37]*z[39];
    z[47]= - n<T>(5,3) - z[28];
    z[47]=z[7]*z[47];
    z[45]=z[47] + z[45];
    z[45]=z[2]*z[45];
    z[23]=z[45] + n<T>(5,3) + z[23];
    z[23]=z[2]*z[23];
    z[45]=n<T>(1,3)*z[8];
    z[47]=n<T>(5,2) - z[45];
    z[47]=z[8]*z[47];
    z[38]=z[47] + z[38];
    z[38]=z[7]*z[38];
    z[40]=z[11]*z[40];
    z[40]=n<T>(1,2) + z[40];
    z[23]=z[23] + n<T>(1,3)*z[40] + z[38];
    z[38]=z[42]*z[9];
    z[40]= - n<T>(7,12) - z[38];
    z[40]=z[9]*z[40];
    z[23]=z[40] + n<T>(1,8)*z[23];
    z[23]=z[2]*z[23];
    z[40]=z[44] - 1;
    z[47]=z[40]*z[1];
    z[47]=z[47] + n<T>(3,2);
    z[49]=z[32] - 1;
    z[49]=z[49]*z[9];
    z[47]=z[49] + n<T>(1,2)*z[47];
    z[15]=z[15]*z[40];
    z[15]=z[15] + n<T>(1,2);
    z[15]=n<T>(1,3)*z[49] + n<T>(1,2)*z[15];
    z[49]= - z[2]*z[15];
    z[49]=z[49] + z[47];
    z[49]=z[2]*z[49];
    z[47]=z[49] - z[47];
    z[47]=z[2]*z[47];
    z[15]=z[47] + z[15];
    z[15]=z[4]*z[15];
    z[43]=z[43]*z[20];
    z[40]=z[43] - z[40];
    z[43]=z[39] - 1;
    z[43]=z[2]*z[43];
    z[43]=n<T>(1,2) + z[43];
    z[40]=z[40]*z[43];
    z[15]=z[15] + z[40];
    z[15]=z[4]*z[15];
    z[40]=z[45] + 1;
    z[40]=z[40]*z[20];
    z[43]=z[40] - static_cast<T>(1)- n<T>(1,6)*z[8];
    z[43]=z[9]*z[43];
    z[47]= - n<T>(1,2) - z[38];
    z[47]=z[2]*z[47];
    z[43]=z[47] + n<T>(1,2) + z[43];
    z[15]=n<T>(1,2)*z[43] + z[15];
    z[15]=z[3]*z[15];
    z[40]=z[40] - n<T>(21,16) - z[45];
    z[40]=z[9]*z[40];
    z[43]=n<T>(53,8) + z[8];
    z[40]=n<T>(1,6)*z[43] + z[40];
    z[40]=z[9]*z[40];
    z[15]=z[15] + z[23] + n<T>(1,4)*z[29] + z[40];
    z[12]=static_cast<T>(1)- z[12];
    z[23]=n<T>(5,12) + z[38];
    z[23]=z[9]*z[23];
    z[29]= - z[2] + 1;
    z[29]=z[7]*z[29];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[2]*z[29];
    z[12]=n<T>(1,16)*z[29] + n<T>(1,48)*z[12] + z[23];
    z[12]=z[2]*z[12];
    z[23]=static_cast<T>(5)+ z[44];
    z[29]=z[9] - n<T>(21,8);
    z[29]=z[29]*z[9];
    z[38]= - n<T>(9,4) - z[29];
    z[38]=z[9]*z[38];
    z[12]=z[12] + n<T>(1,8)*z[23] + z[38];
    z[12]=z[2]*z[12];
    z[23]= - n<T>(17,2) + z[1];
    z[38]=z[9] - n<T>(13,4);
    z[38]=z[38]*z[9];
    z[40]=n<T>(11,3) + z[38];
    z[40]=z[9]*z[40];
    z[23]=n<T>(1,6)*z[23] + z[40];
    z[12]=n<T>(1,2)*z[23] + z[12];
    z[23]= - n<T>(11,2) + z[1];
    z[23]=z[23]*z[44];
    z[23]= - static_cast<T>(1)+ z[23];
    z[29]=n<T>(43,24) + z[29];
    z[29]=z[9]*z[29];
    z[23]=n<T>(1,6)*z[23] + z[29];
    z[29]= - z[42]*z[30];
    z[30]= - static_cast<T>(1)+ z[1];
    z[29]=n<T>(1,2)*z[30] + z[29];
    z[29]=z[2]*z[29];
    z[23]=n<T>(1,2)*z[23] + n<T>(1,3)*z[29];
    z[23]=z[23]*z[39];
    z[29]= - n<T>(43,12) - z[38];
    z[29]=z[29]*z[32];
    z[30]= - n<T>(1,2) - z[1];
    z[30]=z[1]*z[30];
    z[30]=static_cast<T>(1)+ n<T>(1,8)*z[30];
    z[23]=z[23] + n<T>(1,3)*z[30] + z[29];
    z[23]=z[2]*z[23];
    z[29]=n<T>(5,2) + z[1];
    z[29]=z[29]*z[44];
    z[29]= - static_cast<T>(5)+ z[29];
    z[30]= - n<T>(31,8) + z[9];
    z[30]=z[9]*z[30];
    z[30]=n<T>(43,8) + z[30];
    z[30]=z[9]*z[30];
    z[29]=n<T>(1,2)*z[29] + z[30];
    z[23]=n<T>(1,12)*z[29] + z[23];
    z[23]=z[4]*z[23];
    z[12]=n<T>(1,2)*z[12] + z[23];
    z[12]=z[4]*z[12];
    z[12]=z[12] + n<T>(1,2)*z[15];
    z[12]=z[3]*z[12];
    z[12]=n<T>(1,2)*z[27] + z[12];
    z[12]=z[3]*z[12];
    z[15]=z[16]*z[10];
    z[23]=static_cast<T>(1)+ z[5];
    z[23]=z[5]*z[23];
    z[23]=z[23] + n<T>(5,4)*z[15];
    z[23]=z[10]*z[23];
    z[27]= - z[16]*z[24];
    z[27]= - z[5] + z[27];
    z[27]=z[10]*z[27];
    z[27]=n<T>(3,2) + z[27];
    z[27]=z[8]*z[27];
    z[29]= - n<T>(3,2) + z[6];
    z[29]=z[6]*z[29];
    z[23]=n<T>(1,4)*z[27] + z[23] + static_cast<T>(1)+ n<T>(3,2)*z[29];
    z[18]=z[23]*z[18];
    z[23]=z[25] - 1;
    z[15]= - z[23]*z[15];
    z[27]=npow(z[5],3)*z[14];
    z[16]=z[16] + z[27];
    z[16]=z[10]*z[16];
    z[25]=static_cast<T>(1)+ z[25];
    z[16]=n<T>(1,2)*z[25] + z[16];
    z[16]=z[8]*z[16];
    z[25]= - z[5]*z[36];
    z[25]=static_cast<T>(1)+ z[25];
    z[15]=z[16] + z[15] + n<T>(1,2)*z[25] - z[6];
    z[15]=z[15]*z[28];
    z[16]=static_cast<T>(5)+ z[5];
    z[16]=z[5]*z[16]*z[35];
    z[25]=n<T>(3,8) - z[5];
    z[25]=z[6]*z[25];
    z[15]=z[15] + z[18] + z[16] + n<T>(1,4)*z[23] + z[25];
    z[15]=z[7]*z[15];
    z[16]=z[22] - 1;
    z[16]=z[5]*z[16];
    z[16]= - static_cast<T>(3)+ z[16];
    z[16]=z[16]*z[24];
    z[18]= - n<T>(1,2) + z[5];
    z[18]=z[6]*z[18];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[6]*z[18];
    z[16]=3*z[18] + z[16];
    z[18]=static_cast<T>(13)- n<T>(17,2)*z[5];
    z[23]= - z[10]*z[5];
    z[18]=n<T>(1,6)*z[18] + z[23];
    z[18]=z[18]*z[24];
    z[23]= - static_cast<T>(1)+ n<T>(3,4)*z[6];
    z[23]=z[6]*z[23];
    z[18]=z[18] - n<T>(5,24) + z[23];
    z[23]=z[45]*z[10];
    z[18]=n<T>(1,4)*z[18] + z[23];
    z[18]=z[8]*z[18];
    z[15]=n<T>(1,2)*z[15] + n<T>(1,8)*z[16] + z[18];
    z[15]=z[7]*z[15];
    z[16]=static_cast<T>(3)- z[10];
    z[16]=z[16]*z[24];
    z[18]=static_cast<T>(3)- z[24];
    z[18]=z[10]*z[18];
    z[18]=z[18] - z[21];
    z[18]=z[9]*z[18];
    z[18]=z[31] + z[18];
    z[18]=z[9]*z[18];
    z[16]=z[16] + z[18];
    z[16]=z[9]*z[16];
    z[18]=z[5] + z[8];
    z[18]=z[18]*z[28];
    z[18]= - n<T>(1,4) + z[18];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(1,2) + z[18];
    z[18]=z[7]*z[18];
    z[21]=n<T>(1,2) - z[28];
    z[21]=z[2]*z[21]*z[37];
    z[16]=z[21] + z[18] + z[16];
    z[16]=z[16]*z[34];
    z[18]= - n<T>(65,2) - 19*z[11];
    z[21]=n<T>(23,2) + z[33];
    z[21]=z[5]*z[21];
    z[18]=n<T>(1,2)*z[18] + z[21];
    z[21]=static_cast<T>(1)+ z[11];
    z[25]= - n<T>(41,6) - z[33];
    z[25]=n<T>(1,2)*z[25] + z[5];
    z[25]=z[5]*z[25];
    z[21]=n<T>(9,4)*z[21] + z[25];
    z[21]=z[6]*z[21];
    z[18]=n<T>(1,3)*z[18] + z[21];
    z[18]=z[6]*z[18];
    z[17]=n<T>(41,6) - z[17];
    z[17]=z[17]*z[24];
    z[21]=static_cast<T>(17)+ n<T>(11,2)*z[11];
    z[17]=z[17] + n<T>(1,6)*z[21] + z[18];
    z[18]=z[46] - n<T>(1,4)*z[41];
    z[18]=z[18]*z[48];
    z[18]= - n<T>(1,2)*z[11] + z[18];
    z[21]=static_cast<T>(1)- z[35];
    z[19]=z[21]*z[19];
    z[18]=n<T>(1,4)*z[18] + z[19];
    z[18]=z[8]*z[18];
    z[19]= - static_cast<T>(7)+ z[10];
    z[19]=z[19]*z[23];
    z[21]=z[8]*z[10];
    z[21]=z[22] + z[21];
    z[21]=z[9]*z[21];
    z[22]= - n<T>(17,2) + z[10];
    z[22]=z[10]*z[22];
    z[19]=z[21] + z[22] + z[19];
    z[19]=z[9]*z[19];
    z[21]=n<T>(101,3) - z[26];
    z[21]=z[21]*z[14];
    z[22]= - z[8]*z[31];
    z[19]=z[19] + z[21] + z[22];
    z[19]=z[19]*z[32];
    z[21]= - n<T>(1,3) + z[10];
    z[14]=z[8]*z[21]*z[14];
    z[21]= - n<T>(55,48) + z[10];
    z[21]=z[10]*z[21];
    z[14]=z[19] + z[21] + z[14];
    z[14]=z[14]*z[20];

    r += z[12] + n<T>(1,2)*z[13] + z[14] + z[15] + z[16] + n<T>(1,8)*z[17] + 
      z[18];
 
    return r;
}

template double qg_2lha_r1404(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1404(const std::array<dd_real,30>&);
#endif
