#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1043(const std::array<T,30>& k) {
  T z[19];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[28];
    z[7]=k[3];
    z[8]=k[4];
    z[9]=k[9];
    z[10]=k[11];
    z[11]=n<T>(1,2)*z[3];
    z[12]=n<T>(1,2)*z[1];
    z[13]= - static_cast<T>(5)- z[12];
    z[13]=z[13]*z[11];
    z[14]=z[7]*z[9];
    z[15]=static_cast<T>(11)- 7*z[10];
    z[15]=n<T>(1,2)*z[15] - z[6];
    z[16]= - z[6]*z[12];
    z[13]=z[13] + z[16] - z[14] + n<T>(1,2)*z[15] + z[9];
    z[15]= - static_cast<T>(1)+ z[10];
    z[16]=n<T>(3,2) + z[1];
    z[16]=z[3]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[5]*z[15];
    z[13]=n<T>(1,2)*z[13] + z[15];
    z[13]=z[5]*z[13];
    z[15]=static_cast<T>(1)+ 3*z[10];
    z[15]=z[7]*z[15];
    z[16]=z[4]*z[12];
    z[17]=z[6] - z[4];
    z[17]=z[17]*z[1];
    z[17]=z[17] - n<T>(1,2);
    z[18]=z[3]*z[17];
    z[15]=z[18] + z[16] + z[6] + n<T>(1,2)*z[15];
    z[16]=static_cast<T>(5)+ z[1];
    z[11]=z[16]*z[11];
    z[16]=static_cast<T>(1)- n<T>(7,2)*z[10];
    z[16]=z[7]*z[16];
    z[18]=static_cast<T>(1)+ z[6];
    z[18]=z[1]*z[18];
    z[11]=z[11] + z[18] - static_cast<T>(3)+ z[16];
    z[16]=z[7]*z[10];
    z[18]= - static_cast<T>(1)- z[1];
    z[18]=z[3]*z[18];
    z[16]=z[16] + z[18];
    z[16]=z[5]*z[16];
    z[11]=n<T>(1,2)*z[11] + z[16];
    z[11]=z[5]*z[11];
    z[11]=n<T>(1,2)*z[15] + z[11];
    z[11]=z[2]*z[11];
    z[15]=n<T>(1,4)*z[3];
    z[16]= - z[17]*z[15];
    z[11]=n<T>(1,2)*z[11] + z[13] + z[16] + n<T>(3,8)*z[10] + n<T>(1,8) - z[4];
    z[11]=z[2]*z[11];
    z[13]= - z[6]*z[8];
    z[13]= - static_cast<T>(5)+ z[13];
    z[13]=n<T>(1,2)*z[13] + z[9];
    z[13]=z[9]*z[13];
    z[13]=z[15] + z[13] + n<T>(1,2)*z[6];
    z[15]= - static_cast<T>(1)- z[8];
    z[15]=z[9]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[14]=z[15]*z[14];
    z[14]=z[14] + static_cast<T>(1)- z[9];
    z[12]= - z[12] - static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[12]=z[3]*z[12];
    z[12]=n<T>(1,2)*z[14] + z[12];
    z[12]=z[5]*z[12];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[5]*z[12];
    z[11]=z[12] + z[11];

    r += n<T>(1,2)*z[11];
 
    return r;
}

template double qg_2lha_r1043(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1043(const std::array<dd_real,30>&);
#endif
