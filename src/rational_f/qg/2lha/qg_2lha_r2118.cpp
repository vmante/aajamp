#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2118(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[3];
    z[2]=k[6];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[9];
    z[6]= - z[2]*z[4];
    z[6]= - z[3] + z[6];
    z[7]=2*z[4];
    z[8]=z[7] - 3;
    z[8]=z[8]*z[4];
    z[8]=z[8] + 1;
    z[6]=z[8]*z[6];
    z[8]= - static_cast<T>(3)+ 4*z[4];
    z[9]=npow(z[4],2);
    z[10]=z[8]*z[9];
    z[6]=z[10] + z[6];
    z[6]=z[2]*z[6];
    z[10]=static_cast<T>(2)- z[4];
    z[10]=z[4]*z[10];
    z[10]= - static_cast<T>(1)+ z[10];
    z[10]=z[10]*z[7];
    z[11]= - static_cast<T>(5)+ z[7];
    z[11]=z[4]*z[11];
    z[11]=static_cast<T>(4)+ z[11];
    z[11]=z[4]*z[11];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[3]*z[11];
    z[10]=z[10] + z[11];
    z[10]=z[1]*z[10]*npow(z[2],2);
    z[6]=z[10] + z[6];
    z[6]=z[3]*z[6];
    z[7]=z[7] - 1;
    z[7]=z[7]*z[9];
    z[9]=2*z[9];
    z[10]= - static_cast<T>(1)+ z[4];
    z[10]=z[2]*z[10]*z[9];
    z[8]= - z[4]*z[8];
    z[8]= - static_cast<T>(1)+ z[8];
    z[8]=z[4]*z[8];
    z[8]=z[8] + z[10];
    z[8]=z[2]*z[8];
    z[10]=z[1]*z[7];
    z[9]=static_cast<T>(1)- z[9];
    z[9]=z[4]*z[9];
    z[9]=z[9] + z[10];
    z[9]=z[5]*z[9];

    r += z[6] + z[7] + z[8] + z[9];
 
    return r;
}

template double qg_2lha_r2118(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2118(const std::array<dd_real,30>&);
#endif
