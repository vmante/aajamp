#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1358(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[2];
    z[6]=k[4];
    z[7]=k[11];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=n<T>(1,2)*z[3];
    z[11]=5*z[9];
    z[12]= - static_cast<T>(3)- z[11];
    z[12]=z[12]*z[10];
    z[11]=z[11] + 1;
    z[13]=n<T>(1,2)*z[6];
    z[14]=z[11]*z[13];
    z[12]=z[14] + z[9] + z[12];
    z[14]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[15]=n<T>(5,2)*z[9];
    z[16]=z[15] + z[14];
    z[17]=n<T>(1,2) + z[9];
    z[18]= - n<T>(5,8)*z[9] - static_cast<T>(1)- n<T>(3,8)*z[1];
    z[18]=z[3]*z[18];
    z[17]=n<T>(1,4)*z[17] + z[18];
    z[17]=z[3]*z[17];
    z[18]=npow(z[3],2);
    z[19]= - static_cast<T>(1)+ 3*z[18];
    z[20]=z[19]*z[2];
    z[21]=z[20] - z[6];
    z[16]=n<T>(1,4)*z[16] + z[17] + n<T>(1,8)*z[21];
    z[16]=z[2]*z[16];
    z[17]=z[6] + 1;
    z[21]=z[17]*z[7];
    z[12]=z[16] + n<T>(1,2)*z[12] - z[21];
    z[12]=z[2]*z[12];
    z[16]=z[15]*z[6];
    z[16]=z[16] + z[9];
    z[16]=z[16]*z[6];
    z[15]=z[16] - z[15];
    z[16]=z[6] + n<T>(1,2);
    z[22]= - z[21] - z[16];
    z[22]=z[6]*z[22];
    z[22]=n<T>(1,2) + z[22];
    z[22]=z[7]*z[22];
    z[22]=n<T>(1,2)*z[15] + z[22];
    z[12]=n<T>(1,2)*z[22] + z[12];
    z[12]=z[4]*z[12];
    z[22]= - static_cast<T>(5)- 3*z[1];
    z[22]=z[3]*z[22];
    z[22]=n<T>(1,2)*z[11] + z[22];
    z[22]=z[22]*z[10];
    z[11]=z[6]*z[11];
    z[11]= - n<T>(1,4)*z[11] + z[22] - n<T>(1,2)*z[9] + z[14];
    z[22]=static_cast<T>(3)+ z[3];
    z[22]=n<T>(1,2)*z[22] + z[6];
    z[22]=n<T>(1,2)*z[22] + z[21];
    z[22]=z[7]*z[22];
    z[11]=n<T>(1,4)*z[20] + n<T>(1,2)*z[11] + z[22];
    z[11]=z[2]*z[11];
    z[20]=z[17]*z[13];
    z[16]=z[6]*z[16];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[7]*z[16];
    z[16]=z[20] + z[16];
    z[16]=z[7]*z[16];
    z[11]=z[12] + z[11] - n<T>(1,4)*z[15] + z[16];
    z[11]=z[4]*z[11];
    z[10]= - z[13] - static_cast<T>(1)- z[10];
    z[10]=z[8]*z[10];
    z[10]= - z[21] + z[10];
    z[10]=z[7]*z[10];
    z[12]= - z[2]*npow(z[7],2)*z[8]*z[17];
    z[10]=z[12] + z[10] + n<T>(1,4)*z[19];
    z[10]=z[2]*z[10];
    z[12]= - n<T>(3,2)*z[1] + z[8] - 1;
    z[12]=z[12]*z[18];
    z[12]=z[12] - z[8] + z[14];
    z[13]=z[5] - z[6];
    z[13]=z[6]*z[13];
    z[13]=z[13] + static_cast<T>(1)+ z[5];
    z[13]=z[7]*z[13];
    z[13]= - n<T>(1,2)*z[17] + z[13];
    z[13]=z[7]*z[13];
    z[10]=z[10] + n<T>(1,2)*z[12] + z[13];
    z[10]=n<T>(1,2)*z[10] + z[11];

    r += n<T>(1,2)*z[10];
 
    return r;
}

template double qg_2lha_r1358(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1358(const std::array<dd_real,30>&);
#endif
