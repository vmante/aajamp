#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1480(const std::array<T,30>& k) {
  T z[70];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[2];
    z[6]=k[7];
    z[7]=k[6];
    z[8]=k[11];
    z[9]=k[24];
    z[10]=k[4];
    z[11]=k[14];
    z[12]=k[9];
    z[13]=k[10];
    z[14]=k[12];
    z[15]=n<T>(1,2)*z[1];
    z[16]=n<T>(1,2) + z[1];
    z[16]=z[16]*z[15];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[3]*z[16];
    z[17]=n<T>(1,4)*z[13];
    z[18]=3*z[3];
    z[19]=n<T>(23,6) - z[18];
    z[19]=z[19]*z[17];
    z[20]=11*z[1];
    z[21]= - static_cast<T>(23)- z[20];
    z[16]=z[19] + n<T>(1,8)*z[21] + z[16];
    z[19]=n<T>(1,2)*z[13];
    z[21]=n<T>(7,2) + z[18];
    z[21]=z[21]*z[19];
    z[22]=n<T>(1,2)*z[9];
    z[23]=11*z[9];
    z[24]=static_cast<T>(1)+ z[23];
    z[24]=z[24]*z[22];
    z[25]= - z[1]*z[18];
    z[21]=z[24] + z[21] + n<T>(11,2) + z[25];
    z[24]=npow(z[9],2);
    z[25]=n<T>(1,4)*z[12];
    z[26]=z[13]*z[25];
    z[26]=z[26] + z[3] - n<T>(11,4)*z[24];
    z[27]=n<T>(1,2)*z[2];
    z[26]=z[26]*z[27];
    z[28]= - static_cast<T>(1)- n<T>(3,8)*z[13];
    z[28]=z[12]*z[28];
    z[21]=z[26] + n<T>(1,4)*z[21] + z[28];
    z[21]=z[2]*z[21];
    z[26]=n<T>(3,8)*z[12];
    z[28]=static_cast<T>(5)+ z[13];
    z[28]=z[28]*z[26];
    z[16]=z[21] + z[28] + n<T>(1,2)*z[16] - z[9];
    z[16]=z[2]*z[16];
    z[21]=n<T>(3,2)*z[12];
    z[28]=z[6] - z[21];
    z[29]=n<T>(47,2) - z[2];
    z[29]=z[2]*z[29];
    z[29]=n<T>(13,2) + z[29];
    z[29]=z[2]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[29];
    z[29]=npow(z[2],2);
    z[30]=5*z[2];
    z[31]= - static_cast<T>(3)- z[30];
    z[31]=z[31]*z[29];
    z[32]=z[2] + 1;
    z[32]=z[32]*z[11];
    z[33]=npow(z[2],3)*z[32];
    z[31]=z[31] + 2*z[33];
    z[33]=3*z[11];
    z[31]=z[31]*z[33];
    z[28]=n<T>(1,2)*z[28] + z[31];
    z[28]=z[11]*z[28];
    z[31]=9*z[12];
    z[34]=n<T>(253,9) + z[31];
    z[34]=z[34]*z[27];
    z[34]=z[34] - n<T>(73,6) - z[31];
    z[34]=z[34]*z[27];
    z[35]=n<T>(1,2)*z[6];
    z[36]=z[9] - z[35] + n<T>(5,4)*z[11];
    z[37]=z[7] - z[14];
    z[38]=n<T>(7,2) - z[37];
    z[34]=z[34] + n<T>(9,4)*z[12] + n<T>(1,2)*z[38] + z[36];
    z[38]=n<T>(9,4) - n<T>(29,9)*z[2];
    z[38]=z[2]*z[38];
    z[38]= - n<T>(9,8) + z[38];
    z[38]=z[8]*z[38];
    z[34]=n<T>(1,2)*z[34] + z[38];
    z[34]=z[8]*z[34];
    z[38]=n<T>(1,4)*z[7];
    z[39]=static_cast<T>(3)- z[15];
    z[39]=z[39]*z[38];
    z[40]= - z[9] - static_cast<T>(3)- z[19];
    z[40]=z[40]*z[25];
    z[41]=z[9]*z[14];
    z[42]= - static_cast<T>(23)- n<T>(7,2)*z[1];
    z[42]=n<T>(1,8)*z[42] - 2*z[13];
    z[16]=z[34] + z[28] + z[16] + z[40] - n<T>(1,4)*z[41] + n<T>(1,3)*z[42] + 
    z[39];
    z[16]=z[4]*z[16];
    z[28]=z[12]*z[13];
    z[28]=z[28] - n<T>(11,2)*z[24] + z[18] - z[19];
    z[34]=n<T>(1,4)*z[2];
    z[28]=z[28]*z[34];
    z[39]=n<T>(5,8) - z[1];
    z[39]=z[3]*z[39];
    z[18]=n<T>(1,6) + z[18];
    z[18]=z[13]*z[18];
    z[40]= - static_cast<T>(1)- z[19];
    z[40]=z[12]*z[40];
    z[18]=z[28] + z[40] - z[9] + n<T>(1,8)*z[18] + n<T>(33,16) + z[39];
    z[18]=z[2]*z[18];
    z[28]=n<T>(9,2)*z[7];
    z[20]=static_cast<T>(37)- z[20];
    z[39]= - n<T>(1,2) + z[1];
    z[39]=z[1]*z[39];
    z[39]= - n<T>(3,2) + z[39];
    z[39]=z[3]*z[39];
    z[20]= - z[28] + n<T>(23,6)*z[13] + n<T>(1,4)*z[20] + z[39];
    z[39]=n<T>(141,2) - z[30];
    z[39]=z[39]*z[27];
    z[40]=n<T>(3,4)*z[12];
    z[39]=z[39] + z[40] + n<T>(15,4) - z[6];
    z[32]=z[29]*z[32];
    z[42]= - static_cast<T>(17)- 38*z[2];
    z[42]=z[2]*z[42];
    z[32]=z[42] + 21*z[32];
    z[32]=z[11]*z[32];
    z[32]=n<T>(1,2)*z[39] + z[32];
    z[32]=z[11]*z[32];
    z[37]=n<T>(11,2) + z[37];
    z[39]= - static_cast<T>(25)- 253*z[2];
    z[39]=z[2]*z[39];
    z[37]=n<T>(1,36)*z[39] + n<T>(1,2)*z[37] - z[36];
    z[39]=n<T>(58,9)*z[2];
    z[42]= - n<T>(9,4) + z[39];
    z[42]=z[2]*z[42];
    z[42]=n<T>(9,8) + z[42];
    z[42]=z[8]*z[42];
    z[37]=n<T>(1,2)*z[37] + z[42];
    z[37]=z[8]*z[37];
    z[31]=static_cast<T>(1)- z[31];
    z[31]=z[31]*z[27];
    z[31]=z[31] + n<T>(53,9) + n<T>(27,2)*z[12];
    z[31]=z[31]*z[27];
    z[42]=z[7] - n<T>(73,3) + z[14];
    z[31]= - n<T>(7,4)*z[11] + z[31] - n<T>(9,2)*z[12] + z[6] + n<T>(1,2)*z[42] + 
    z[9];
    z[31]=n<T>(1,2)*z[31] + z[37];
    z[31]=z[8]*z[31];
    z[37]=n<T>(1,3)*z[6];
    z[42]=static_cast<T>(2)- n<T>(13,8)*z[1];
    z[42]=z[42]*z[37];
    z[43]= - z[9] + n<T>(5,2) + z[13];
    z[43]=z[43]*z[25];
    z[44]=static_cast<T>(2)- n<T>(1,4)*z[14];
    z[44]=z[9]*z[44];
    z[16]=z[16] + z[31] + z[32] + z[18] + z[43] + z[42] + n<T>(1,4)*z[20] + 
    z[44];
    z[16]=z[4]*z[16];
    z[18]=n<T>(58,9)*z[8];
    z[20]= - static_cast<T>(1)+ 2*z[2];
    z[20]=z[20]*z[18];
    z[31]=n<T>(13,12)*z[6];
    z[32]=z[9] + z[31] + n<T>(13,4)*z[11];
    z[42]=n<T>(329,6) + z[7];
    z[20]=z[20] - n<T>(349,72)*z[2] + n<T>(1,4)*z[42] - z[32];
    z[20]=z[8]*z[20];
    z[42]= - n<T>(43,9) + z[7];
    z[43]=static_cast<T>(41)+ z[23];
    z[43]=z[9]*z[43];
    z[42]=5*z[42] + z[43];
    z[43]=n<T>(17,8)*z[6];
    z[44]= - static_cast<T>(1)+ z[43];
    z[44]=z[44]*z[37];
    z[45]=n<T>(9,4) - z[11];
    z[45]=z[45]*z[33];
    z[20]=z[20] + z[45] - n<T>(217,72)*z[2] + n<T>(1,4)*z[42] + z[44];
    z[20]=z[8]*z[20];
    z[42]=n<T>(5,4)*z[2];
    z[44]=n<T>(67,6) - 27*z[7];
    z[45]= - n<T>(1,2) - z[9];
    z[45]=z[9]*z[45];
    z[46]=n<T>(7,3)*z[6];
    z[47]= - n<T>(9,4) - z[46];
    z[47]=z[6]*z[47];
    z[44]=z[42] + z[47] + n<T>(1,2)*z[44] + 33*z[45];
    z[45]=static_cast<T>(11)- 6*z[11];
    z[45]=z[11]*z[45];
    z[45]= - n<T>(37,8) + z[45];
    z[45]=z[11]*z[45];
    z[20]=z[20] + n<T>(1,4)*z[44] + z[45];
    z[20]=z[8]*z[20];
    z[44]=n<T>(3,2)*z[14];
    z[45]=n<T>(11,2)*z[9];
    z[47]=z[45] + static_cast<T>(7)- z[44];
    z[47]=z[9]*z[47];
    z[48]=n<T>(1,12)*z[6];
    z[49]=11*z[13];
    z[50]= - static_cast<T>(31)- z[49];
    z[50]=z[50]*z[48];
    z[51]= - static_cast<T>(1)- z[45];
    z[51]=z[9]*z[51];
    z[51]= - z[25] + z[51] + n<T>(11,8) + z[13];
    z[51]=z[2]*z[51];
    z[52]=z[12]*z[9];
    z[49]=n<T>(25,2) + z[49];
    z[49]=n<T>(1,3)*z[49] - z[7];
    z[47]=z[51] - n<T>(3,2)*z[52] + z[50] + n<T>(1,4)*z[49] + z[47];
    z[49]=3*z[2];
    z[50]=n<T>(35,2) - z[49];
    z[50]=z[50]*z[34];
    z[51]= - static_cast<T>(1)+ z[30];
    z[51]=z[2]*z[51];
    z[51]= - static_cast<T>(4)+ z[51];
    z[51]=z[11]*z[51];
    z[51]=z[51] + static_cast<T>(5)- 7*z[2];
    z[51]=z[2]*z[51];
    z[51]=static_cast<T>(2)+ z[51];
    z[51]=z[51]*z[33];
    z[50]=z[51] + z[50] - z[26] - n<T>(65,8) + z[6];
    z[50]=z[11]*z[50];
    z[51]=3*z[7];
    z[53]=n<T>(13,2) + 3*z[14];
    z[53]=n<T>(15,4)*z[11] - z[42] + n<T>(17,12)*z[6] - n<T>(13,2)*z[9] + n<T>(1,2)*
    z[53] + z[51];
    z[54]= - n<T>(89,6) - z[14];
    z[36]=n<T>(533,36)*z[2] + n<T>(1,2)*z[54] - z[7] - z[36];
    z[39]=n<T>(9,8) - z[39];
    z[39]=z[8]*z[39];
    z[36]=n<T>(1,2)*z[36] + z[39];
    z[36]=z[8]*z[36];
    z[36]=n<T>(1,2)*z[53] + z[36];
    z[36]=z[8]*z[36];
    z[36]=z[36] + n<T>(1,2)*z[47] + z[50];
    z[36]=z[4]*z[36];
    z[39]=2*z[6];
    z[47]=z[39] - z[12];
    z[50]=z[47] - 3;
    z[29]=2*z[50] + 39*z[29];
    z[29]=z[11]*z[29];
    z[53]=4*z[12];
    z[54]= - static_cast<T>(7)+ z[6];
    z[54]=z[6]*z[54];
    z[29]=z[29] - 33*z[2] + z[53] + static_cast<T>(4)+ z[54];
    z[29]=z[11]*z[29];
    z[39]=n<T>(9,4) - z[39];
    z[39]=z[6]*z[39];
    z[29]=z[29] - z[42] - n<T>(13,8)*z[12] + n<T>(17,8) + z[39];
    z[29]=z[11]*z[29];
    z[39]=5*z[6];
    z[42]=static_cast<T>(9)+ n<T>(23,3)*z[13];
    z[42]=n<T>(1,2)*z[42] + z[39];
    z[54]=n<T>(1,4)*z[6];
    z[42]=z[42]*z[54];
    z[55]=n<T>(1,2)*z[12];
    z[56]= - n<T>(3,4) - z[9];
    z[56]=z[56]*z[55];
    z[57]=n<T>(5,2)*z[13];
    z[58]=11*z[24];
    z[59]= - z[57] + z[58];
    z[59]=z[2]*z[59];
    z[60]=static_cast<T>(11)- 7*z[13];
    z[60]=n<T>(1,4)*z[60] + z[7];
    z[61]=n<T>(11,8)*z[9];
    z[62]=z[61] - static_cast<T>(1)- n<T>(1,2)*z[14];
    z[62]=z[9]*z[62];
    z[20]=z[36] + z[20] + z[29] + n<T>(1,8)*z[59] + z[56] + z[42] + n<T>(1,4)*
    z[60] + z[62];
    z[20]=z[4]*z[20];
    z[29]=n<T>(1,3)*z[5];
    z[36]= - static_cast<T>(2)- z[5];
    z[36]=z[36]*z[29];
    z[42]=z[2]*npow(z[12],2);
    z[53]= - z[53] - z[42];
    z[53]=z[2]*z[53];
    z[53]= - static_cast<T>(1)+ z[53];
    z[53]=z[2]*z[53];
    z[56]=z[5] + 1;
    z[53]= - 2*z[56] + z[53];
    z[59]=n<T>(1,3)*z[2];
    z[53]=z[53]*z[59];
    z[36]=z[53] - static_cast<T>(1)+ z[36];
    z[36]=z[8]*z[36];
    z[53]=n<T>(1,2)*z[5];
    z[60]=z[53] + 1;
    z[62]=z[60]*z[29];
    z[62]=z[62] + n<T>(1,2);
    z[63]=z[6]*z[62];
    z[64]=n<T>(29,2)*z[12] + n<T>(41,9)*z[42];
    z[64]=z[64]*z[34];
    z[64]=z[64] + n<T>(29,9) + z[55];
    z[64]=z[2]*z[64];
    z[65]=3*z[9];
    z[66]=n<T>(125,9) - z[65];
    z[67]=z[53]*z[9];
    z[68]= - z[67] + n<T>(247,72) - z[9];
    z[68]=z[5]*z[68];
    z[69]=n<T>(3,4) - z[7];
    z[69]=z[12]*z[69];
    z[36]=n<T>(29,3)*z[36] + z[64] + z[69] - n<T>(13,4)*z[63] + n<T>(1,2)*z[66] + 
    z[68];
    z[36]=z[8]*z[36];
    z[63]=z[24]*z[53];
    z[64]= - static_cast<T>(1)+ z[9];
    z[64]=z[9]*z[64];
    z[63]=z[64] + z[63];
    z[64]=11*z[5];
    z[63]=z[63]*z[64];
    z[66]= - static_cast<T>(7)+ n<T>(33,2)*z[9];
    z[66]=z[9]*z[66];
    z[63]=z[63] + z[66] + n<T>(113,12) + 9*z[7];
    z[43]=z[62]*z[43];
    z[62]=n<T>(47,8) + 4*z[5];
    z[43]=n<T>(1,3)*z[62] + z[43];
    z[43]=z[6]*z[43];
    z[62]=z[51] - 1;
    z[66]= - z[62]*z[40];
    z[51]=z[51] + n<T>(479,18)*z[12];
    z[42]=n<T>(1,2)*z[51] + n<T>(31,9)*z[42];
    z[42]=z[42]*z[27];
    z[36]=z[36] + z[42] + z[66] + n<T>(1,4)*z[63] + z[43];
    z[36]=z[8]*z[36];
    z[42]=z[6]*z[60];
    z[42]= - n<T>(19,2) - 11*z[42];
    z[42]=z[42]*z[37];
    z[43]=z[24]*z[5];
    z[51]=11*z[43];
    z[42]=z[42] - 7*z[9] + z[51];
    z[63]=n<T>(37,24) - z[7];
    z[63]=z[12]*z[63];
    z[36]=z[36] + n<T>(1,8)*z[42] + z[63];
    z[36]=z[8]*z[36];
    z[42]=n<T>(1,2)*z[11];
    z[63]=n<T>(37,4) - z[33];
    z[63]=z[63]*z[42];
    z[66]=n<T>(271,18) - z[7];
    z[32]=n<T>(1,2)*z[66] - z[32];
    z[66]=n<T>(29,9)*z[8];
    z[32]=n<T>(1,2)*z[32] - z[66];
    z[32]=z[8]*z[32];
    z[61]=static_cast<T>(1)+ z[61];
    z[61]=z[9]*z[61];
    z[68]=n<T>(17,6)*z[6];
    z[69]=static_cast<T>(5)+ z[68];
    z[69]=z[6]*z[69];
    z[32]=z[32] + z[63] + n<T>(1,8)*z[69] + n<T>(1,4)*z[62] + z[61];
    z[32]=z[8]*z[32];
    z[61]= - static_cast<T>(1)- n<T>(11,8)*z[6];
    z[61]=z[61]*z[37];
    z[58]= - z[58] - n<T>(3,2) - z[7];
    z[58]=n<T>(1,4)*z[58] + z[61];
    z[61]=n<T>(3,2) - z[11];
    z[61]=z[11]*z[61];
    z[61]= - n<T>(11,8) + z[61];
    z[61]=z[61]*z[33];
    z[32]=z[32] + n<T>(1,2)*z[58] + z[61];
    z[32]=z[8]*z[32];
    z[58]=z[6]*z[13];
    z[57]=z[57] - z[58];
    z[48]=z[57]*z[48];
    z[57]= - static_cast<T>(1)- z[65];
    z[57]=z[57]*z[55];
    z[61]=n<T>(3,4)*z[13];
    z[62]=z[14] + z[61];
    z[44]=static_cast<T>(1)- z[44];
    z[44]=z[9]*z[44];
    z[44]=z[57] + z[48] + n<T>(1,2)*z[62] + z[44];
    z[48]= - static_cast<T>(5)+ 4*z[2];
    z[48]=z[48]*z[49];
    z[48]=z[48] + z[47];
    z[48]=z[11]*z[48];
    z[57]=2*z[12];
    z[62]= - static_cast<T>(5)+ z[6];
    z[62]=z[6]*z[62];
    z[62]=static_cast<T>(15)+ z[62];
    z[48]=z[48] - n<T>(9,2)*z[2] + n<T>(1,2)*z[62] + z[57];
    z[48]=z[11]*z[48];
    z[62]=n<T>(7,2) - z[6];
    z[62]=z[62]*z[35];
    z[48]=z[48] - z[34] - z[12] - static_cast<T>(1)+ z[62];
    z[48]=z[11]*z[48];
    z[32]=z[32] + n<T>(1,2)*z[44] + z[48];
    z[32]=z[4]*z[32];
    z[44]= - n<T>(15,2) - n<T>(17,3)*z[6];
    z[44]=z[44]*z[54];
    z[28]=static_cast<T>(1)- z[28];
    z[48]= - static_cast<T>(3)- z[45];
    z[48]=z[9]*z[48];
    z[62]=2*z[9];
    z[63]=n<T>(116,9)*z[8] + n<T>(13,6)*z[6] + z[62] - n<T>(271,24) + z[7];
    z[63]=z[8]*z[63];
    z[28]=z[63] + z[44] + n<T>(1,2)*z[28] + z[48];
    z[28]=z[8]*z[28];
    z[24]=n<T>(33,2)*z[24] + n<T>(3,4) + z[7];
    z[44]=n<T>(1,3) + n<T>(11,16)*z[6];
    z[44]=z[6]*z[44];
    z[24]=z[28] + n<T>(1,4)*z[24] + z[44];
    z[24]=z[8]*z[24];
    z[28]=z[41] + z[52];
    z[41]= - n<T>(5,3)*z[13] + z[58];
    z[41]=z[41]*z[35];
    z[41]=z[41] - z[61] - z[28];
    z[44]=3*z[12];
    z[48]=z[44] - 1;
    z[52]= - static_cast<T>(2)+ z[35];
    z[52]=z[6]*z[52];
    z[47]=21*z[2] - static_cast<T>(15)+ z[47];
    z[47]=z[11]*z[47];
    z[47]=z[47] + z[52] + z[48];
    z[47]=z[11]*z[47];
    z[52]= - n<T>(3,2) - z[6];
    z[52]=z[6]*z[52];
    z[52]= - n<T>(1,2) + z[52];
    z[47]=z[47] + n<T>(1,2)*z[52] - z[57];
    z[47]=z[11]*z[47];
    z[24]=z[32] + z[24] + n<T>(1,4)*z[41] + z[47];
    z[24]=z[4]*z[24];
    z[32]=n<T>(1,2)*z[7];
    z[41]=n<T>(1,2) - z[7];
    z[41]=z[12]*z[41];
    z[41]=z[32] + z[41];
    z[47]=z[7] + z[12];
    z[47]=z[47]*z[27];
    z[41]=3*z[41] + z[47];
    z[41]=z[8]*z[41];
    z[47]=static_cast<T>(1)- 7*z[7];
    z[47]=z[12]*z[47];
    z[47]=z[7] + z[47];
    z[41]=n<T>(1,2)*z[47] + z[41];
    z[41]=z[8]*z[41];
    z[47]= - z[7]*z[40];
    z[41]=z[47] + z[41];
    z[41]=z[8]*z[41];
    z[47]=z[6] - 3;
    z[52]=z[49] + 2*z[47] - z[12];
    z[52]=z[11]*z[52];
    z[58]= - static_cast<T>(1)+ z[35];
    z[58]=z[6]*z[58];
    z[52]=z[52] + z[57] + n<T>(3,2) + z[58];
    z[52]=z[11]*z[52];
    z[52]= - z[12] + z[52];
    z[52]=z[11]*z[52];
    z[28]= - n<T>(1,4)*z[28] + z[52];
    z[28]=z[4]*z[28];
    z[52]=static_cast<T>(1)- z[6];
    z[52]=z[52]*z[35];
    z[50]= - z[11]*z[50];
    z[50]=z[50] + z[52] - z[12];
    z[50]=z[50]*npow(z[11],2);
    z[28]=z[50] + z[28];
    z[28]=z[4]*z[28];
    z[50]=n<T>(1,4) - z[7];
    z[50]=z[12]*z[50];
    z[50]=z[38] + z[50];
    z[50]=z[8]*z[50];
    z[52]= - z[7]*z[55];
    z[50]=z[52] + z[50];
    z[50]=z[50]*npow(z[8],2);
    z[52]= - z[10]*z[7]*npow(z[8],3)*z[25];
    z[50]=z[50] + z[52];
    z[50]=z[10]*z[50];
    z[28]=z[50] + n<T>(1,2)*z[41] + z[28];
    z[28]=z[10]*z[28];
    z[41]=static_cast<T>(1)+ 3*z[6];
    z[41]=z[41]*z[35];
    z[50]=n<T>(1,2) - z[12];
    z[50]=z[12]*z[50];
    z[41]=z[41] + z[50];
    z[47]=z[6]*z[47];
    z[48]=z[12]*z[48];
    z[47]= - 9*z[47] + z[48];
    z[48]= - n<T>(5,4)*z[12] + static_cast<T>(9)- n<T>(17,4)*z[6];
    z[48]=z[11]*z[48];
    z[47]=n<T>(1,8)*z[47] + z[48];
    z[47]=z[11]*z[47];
    z[41]=n<T>(3,4)*z[41] + z[47];
    z[41]=z[11]*z[41];
    z[37]= - z[37]*z[13]*z[60];
    z[17]=z[17] + z[37];
    z[17]=z[17]*z[54];
    z[26]=z[26] - n<T>(1,8)*z[7] - z[62];
    z[26]=z[12]*z[26];
    z[17]=z[28] + z[24] + z[36] + z[41] + z[17] + z[26];
    z[17]=z[10]*z[17];
    z[24]= - static_cast<T>(1)- n<T>(47,12)*z[5];
    z[24]=n<T>(11,3)*z[24] + z[55];
    z[26]=z[2]*z[12];
    z[28]= - n<T>(119,8) - n<T>(58,3)*z[12];
    z[28]=z[28]*z[26];
    z[36]=static_cast<T>(107)- 239*z[12];
    z[28]=n<T>(1,24)*z[36] + z[28];
    z[28]=z[28]*z[59];
    z[24]=n<T>(1,2)*z[24] + z[28];
    z[24]=z[2]*z[24];
    z[28]=z[56]*z[5];
    z[28]=z[28] + 1;
    z[36]=z[28]*z[5];
    z[36]=z[36] + 1;
    z[31]=z[36]*z[31];
    z[37]=z[7] - 1;
    z[41]= - z[37]*z[25];
    z[47]=z[26] + static_cast<T>(1)- z[57];
    z[47]=z[2]*z[47];
    z[47]=z[47] - z[56];
    z[47]=z[2]*z[47];
    z[48]=npow(z[5],2);
    z[47]=z[47] - static_cast<T>(1)+ z[48];
    z[47]=z[2]*z[47];
    z[47]=z[47] + z[36];
    z[18]=z[47]*z[18];
    z[47]=z[5]*z[9];
    z[50]=z[47] - n<T>(293,24) + z[9];
    z[50]=z[5]*z[50];
    z[50]=z[50] - n<T>(307,24) + z[9];
    z[50]=z[5]*z[50];
    z[18]=z[18] + z[24] + z[41] + z[31] + z[50] - n<T>(107,8) + z[9];
    z[18]=z[8]*z[18];
    z[24]= - static_cast<T>(19)- n<T>(61,2)*z[5];
    z[24]=z[24]*z[29];
    z[29]= - z[36]*z[68];
    z[24]=z[29] - n<T>(5,2) + z[24];
    z[24]=z[24]*z[54];
    z[29]=n<T>(11,2)*z[43];
    z[31]=static_cast<T>(5)- z[45];
    z[31]=z[9]*z[31];
    z[31]=z[31] - z[29];
    z[31]=z[5]*z[31];
    z[36]= - static_cast<T>(7)- z[23];
    z[36]=z[9]*z[36];
    z[36]=n<T>(553,18) + z[36];
    z[31]=n<T>(1,2)*z[36] + z[31];
    z[31]=z[31]*z[53];
    z[36]=static_cast<T>(3)- 5*z[7];
    z[36]=z[36]*z[25];
    z[41]=41*z[12];
    z[43]= - static_cast<T>(43)+ z[41];
    z[43]=z[43]*z[26];
    z[45]= - n<T>(91,9) + 33*z[12];
    z[43]=n<T>(1,2)*z[45] + n<T>(1,9)*z[43];
    z[43]=z[43]*z[27];
    z[45]= - static_cast<T>(6)- n<T>(11,4)*z[9];
    z[45]=z[9]*z[45];
    z[18]=z[18] + z[43] + z[36] + z[24] + z[31] + n<T>(679,72) + z[45];
    z[18]=z[8]*z[18];
    z[24]= - n<T>(29,4) + z[23];
    z[22]=z[24]*z[22];
    z[24]= - n<T>(31,2) + z[23];
    z[24]=z[24]*z[47];
    z[28]=z[28]*z[46];
    z[31]=5*z[5];
    z[36]=static_cast<T>(7)+ z[31];
    z[28]=n<T>(1,4)*z[36] + z[28];
    z[28]=z[28]*z[54];
    z[36]=n<T>(1,9)*z[12];
    z[43]=n<T>(121,8) + 31*z[12];
    z[43]=z[43]*z[36];
    z[32]=z[32] + z[43];
    z[32]=z[2]*z[32];
    z[38]=static_cast<T>(1)- z[38];
    z[38]=z[12]*z[38];
    z[18]=z[18] + z[32] + n<T>(7,2)*z[38] + z[28] + n<T>(1,4)*z[24] + z[22] - n<T>(1,3) + 2*z[7];
    z[18]=z[8]*z[18];
    z[22]= - static_cast<T>(9)+ z[31];
    z[22]=z[22]*z[35];
    z[24]= - static_cast<T>(9)+ z[64];
    z[24]=z[24]*z[55];
    z[28]=3*z[5];
    z[31]=static_cast<T>(4)- z[28];
    z[22]=24*z[2] + z[24] + 3*z[31] + z[22];
    z[22]=z[11]*z[22];
    z[24]= - static_cast<T>(5)+ z[28];
    z[24]=z[24]*z[54];
    z[24]=z[24] + static_cast<T>(7)- z[28];
    z[24]=z[6]*z[24];
    z[31]=z[5] - 1;
    z[32]= - z[31]*z[40];
    z[32]=z[32] - z[60];
    z[32]=z[12]*z[32];
    z[22]=z[22] + z[32] - static_cast<T>(12)+ z[24];
    z[22]=z[11]*z[22];
    z[24]=z[53] - 1;
    z[32]= - z[24]*z[39];
    z[38]=n<T>(3,4)*z[5];
    z[32]=z[32] - n<T>(13,3) + z[38];
    z[32]=z[6]*z[32];
    z[39]=z[24]*z[44];
    z[38]=z[39] + n<T>(23,3) + z[38];
    z[38]=z[12]*z[38];
    z[32]=z[38] - static_cast<T>(1)+ z[32];
    z[22]=n<T>(1,2)*z[32] + z[22];
    z[22]=z[11]*z[22];
    z[23]=static_cast<T>(9)+ z[23];
    z[23]=z[9]*z[23];
    z[23]=z[51] - z[19] + z[23];
    z[32]= - static_cast<T>(3)+ n<T>(5,24)*z[5];
    z[32]=z[32]*z[35];
    z[38]=z[5]*z[13];
    z[32]=z[32] - n<T>(1,24)*z[38] - n<T>(2,3) - z[19];
    z[32]=z[6]*z[32];
    z[38]= - n<T>(11,3) - z[7];
    z[38]=z[40] + n<T>(1,4)*z[38] - 4*z[9];
    z[38]=z[12]*z[38];
    z[17]=z[17] + z[20] + z[18] + z[22] + z[38] + n<T>(1,8)*z[23] + z[32];
    z[17]=z[10]*z[17];
    z[18]=z[9] - z[29];
    z[18]=z[18]*z[53];
    z[18]=z[18] + n<T>(187,72) - z[9];
    z[18]=z[5]*z[18];
    z[20]=z[6]*npow(z[5],3);
    z[20]= - n<T>(17,24)*z[20] - n<T>(1,2) + n<T>(1,3)*z[48];
    z[20]=z[6]*z[20];
    z[22]=n<T>(149,6) - z[7];
    z[22]=z[22]*z[55];
    z[23]= - n<T>(187,2) + z[41];
    z[23]=z[23]*z[36];
    z[32]=z[2]*z[55];
    z[23]=z[32] - n<T>(3,2) + z[23];
    z[23]=z[23]*z[27];
    z[18]=z[23] + z[22] + z[20] + z[18] + static_cast<T>(5)- z[9];
    z[20]=z[9] + z[29];
    z[20]=z[20]*z[53];
    z[20]=z[20] - n<T>(347,24) + z[9];
    z[20]=z[5]*z[20];
    z[20]=z[20] - static_cast<T>(3)+ z[9];
    z[20]=z[5]*z[20];
    z[22]=npow(z[5],4);
    z[23]=z[22]*z[6];
    z[27]= - static_cast<T>(1)+ n<T>(29,6)*z[5];
    z[27]=z[5]*z[27];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[5]*z[27];
    z[27]=n<T>(17,12)*z[23] - static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[35];
    z[29]= - z[37]*z[55];
    z[20]=z[29] + z[27] + z[20] - n<T>(21,4) + z[9];
    z[27]= - n<T>(383,18)*z[26] + n<T>(9,2) - z[48];
    z[27]=z[27]*z[34];
    z[22]= - z[22]*z[66];
    z[29]=n<T>(79,9) - z[67];
    z[29]=z[5]*z[29];
    z[29]=n<T>(1,2) + z[29];
    z[29]=z[5]*z[29];
    z[29]=n<T>(1,2) + z[29];
    z[29]=z[5]*z[29];
    z[22]=z[22] - n<T>(13,8)*z[11] + z[27] - n<T>(13,24)*z[23] + n<T>(1,2) + z[29];
    z[22]=z[8]*z[22];
    z[23]=n<T>(289,8) - 29*z[12];
    z[23]=z[23]*z[36];
    z[23]= - n<T>(1,8)*z[26] - n<T>(1,8) + z[23];
    z[23]=z[2]*z[23];
    z[26]=static_cast<T>(3)+ 7*z[5];
    z[26]=n<T>(1,2)*z[26] - n<T>(55,9)*z[12];
    z[23]=n<T>(1,4)*z[26] + z[23];
    z[23]=z[2]*z[23];
    z[26]=n<T>(17,4) - z[33];
    z[26]=z[26]*z[42];
    z[20]=z[22] + z[26] + n<T>(1,2)*z[20] + z[23];
    z[20]=z[8]*z[20];
    z[22]=n<T>(13,2) - z[33];
    z[22]=z[11]*z[22];
    z[22]= - n<T>(19,4) + z[22];
    z[22]=z[11]*z[22];
    z[18]=z[20] + n<T>(1,2)*z[18] + z[22];
    z[18]=z[8]*z[18];
    z[20]=static_cast<T>(5)- z[53];
    z[20]=z[5]*z[20];
    z[20]= - n<T>(1,2) + z[20];
    z[20]=z[20]*z[35];
    z[22]=static_cast<T>(11)- n<T>(13,2)*z[5];
    z[22]=z[5]*z[22];
    z[22]= - n<T>(13,2) + z[22];
    z[22]=z[22]*z[55];
    z[23]=z[30] + static_cast<T>(8)- z[28];
    z[23]=z[23]*z[49];
    z[26]= - static_cast<T>(5)+ z[5];
    z[26]=z[5]*z[26];
    z[26]=static_cast<T>(3)+ z[26];
    z[20]=z[23] + z[22] + 3*z[26] + z[20];
    z[20]=z[11]*z[20];
    z[22]=z[53] - 3;
    z[23]= - z[22]*z[28];
    z[24]=z[5]*z[24];
    z[24]=n<T>(1,2) + z[24];
    z[24]=z[24]*z[44];
    z[23]=z[24] - n<T>(7,2) + z[23];
    z[23]=z[23]*z[25];
    z[24]= - static_cast<T>(19)+ z[53];
    z[24]=z[5]*z[24];
    z[22]= - z[5]*z[22];
    z[22]= - n<T>(1,2) + z[22];
    z[22]=z[6]*z[22];
    z[22]=z[22] + n<T>(5,2) + z[24];
    z[22]=z[22]*z[54];
    z[20]=z[20] - 23*z[2] + z[23] + z[22] - static_cast<T>(11)+ 8*z[5];
    z[20]=z[11]*z[20];
    z[22]= - static_cast<T>(13)+ 47*z[5];
    z[23]= - static_cast<T>(2)+ n<T>(1,8)*z[5];
    z[23]=z[5]*z[23];
    z[23]=n<T>(3,8) + z[23];
    z[23]=z[6]*z[23];
    z[22]=n<T>(1,24)*z[22] + z[23];
    z[22]=z[6]*z[22];
    z[21]=z[31]*z[21];
    z[23]=static_cast<T>(11)- n<T>(31,4)*z[5];
    z[21]=n<T>(1,3)*z[23] + z[21];
    z[21]=z[21]*z[55];
    z[23]=n<T>(29,2) + z[5];
    z[20]=z[20] - n<T>(3,4)*z[2] + z[21] + n<T>(1,4)*z[23] + z[22];
    z[20]=z[11]*z[20];
    z[21]=static_cast<T>(5)- n<T>(17,12)*z[1];
    z[21]=z[5]*z[21];
    z[15]=z[21] - static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[35];
    z[21]= - static_cast<T>(1)+ z[1];
    z[15]=z[15] + n<T>(1,2)*z[21] - n<T>(5,3)*z[5];
    z[15]=z[6]*z[15];
    z[21]=n<T>(3,2) - z[1];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(7,6)*z[13] + n<T>(15,4) + z[21];
    z[15]=z[15] + n<T>(1,2)*z[21] + z[7];
    z[21]= - z[7] - n<T>(193,9) - z[13];
    z[21]=n<T>(151,72)*z[12] + n<T>(1,8)*z[21] - z[62];
    z[21]=z[12]*z[21];
    z[22]=z[13]*z[55];
    z[19]=z[22] + z[3] - z[19];
    z[19]=z[19]*z[34];

    r += n<T>(1,2)*z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21];
 
    return r;
}

template double qg_2lha_r1480(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1480(const std::array<dd_real,30>&);
#endif
