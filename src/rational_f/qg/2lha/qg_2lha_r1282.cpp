#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1282(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[9];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=z[2] - n<T>(3,2);
    z[7]=z[7]*z[2];
    z[7]=z[7] + n<T>(1,2);
    z[8]=n<T>(1,2)*z[5];
    z[7]=z[7]*z[8];
    z[8]=z[2] - n<T>(3,4);
    z[9]=npow(z[2],2);
    z[8]=z[8]*z[9];
    z[10]= - z[8] + z[7];
    z[10]=z[5]*z[10];
    z[11]=npow(z[2],3);
    z[11]=n<T>(1,2)*z[11];
    z[10]=z[11] + z[10];
    z[10]=z[3]*z[10];
    z[7]=z[2]*z[7];
    z[7]= - z[8] + z[7];
    z[7]=z[5]*z[7];
    z[7]=z[11] + z[7];
    z[7]=z[6]*z[7];
    z[8]=z[2] - n<T>(1,2);
    z[12]= - z[8]*z[9];
    z[8]=z[2]*z[8];
    z[8]= - static_cast<T>(1)+ z[8];
    z[8]=z[2]*z[8];
    z[8]=n<T>(1,2) + z[8];
    z[8]=z[5]*z[8];
    z[8]=z[12] + z[8];
    z[12]=z[3]*z[11];
    z[9]= - z[9] + z[12];
    z[9]=z[3]*z[9];
    z[9]=z[9] + n<T>(1,2)*z[2];
    z[9]=z[4]*z[9];
    z[11]= - z[1]*z[11];
    z[7]=z[11] + z[9] + z[7] + n<T>(1,2)*z[8] + z[10];

    r += n<T>(3,64)*z[7];
 
    return r;
}

template double qg_2lha_r1282(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1282(const std::array<dd_real,30>&);
#endif
