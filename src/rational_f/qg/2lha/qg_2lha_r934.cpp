#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r934(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[9];
    z[4]=k[7];
    z[5]=k[24];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=k[27];
    z[9]=k[12];
    z[10]=n<T>(1,2)*z[8];
    z[11]=n<T>(403,2)*z[8];
    z[12]=n<T>(83,3) - z[11];
    z[12]=z[12]*z[10];
    z[12]=n<T>(53,3) + z[12];
    z[13]=7*z[9];
    z[14]= - n<T>(599,3) + n<T>(563,2)*z[8];
    z[14]=n<T>(1,3)*z[14] + z[13];
    z[15]=n<T>(1,2)*z[4];
    z[14]=z[14]*z[15];
    z[16]=n<T>(3,2)*z[9];
    z[12]=z[14] + n<T>(1,3)*z[12] + z[16];
    z[12]=z[4]*z[12];
    z[14]=9*z[5];
    z[17]=n<T>(1,2)*z[9];
    z[18]=static_cast<T>(1)- z[17];
    z[18]=z[18]*z[14];
    z[18]=z[9] + z[18];
    z[19]=3*z[5];
    z[18]=z[18]*z[19];
    z[20]=npow(z[8],2);
    z[21]=n<T>(403,9)*z[20];
    z[18]=z[18] + static_cast<T>(1)+ z[21];
    z[22]=n<T>(3,2)*z[5];
    z[23]= - static_cast<T>(7)- z[14];
    z[23]=z[23]*z[22];
    z[24]=n<T>(9,2)*z[8];
    z[23]=z[23] - static_cast<T>(5)+ z[24];
    z[23]=z[3]*z[23];
    z[12]=z[12] + n<T>(1,2)*z[18] + z[23];
    z[18]=n<T>(1,4)*z[2];
    z[12]=z[12]*z[18];
    z[23]=9*z[8];
    z[25]= - static_cast<T>(19)- n<T>(73,2)*z[5];
    z[25]=z[5]*z[25];
    z[26]=n<T>(1,2)*z[5];
    z[27]= - z[3]*z[26];
    z[25]=z[27] - z[23] + z[25];
    z[25]=z[3]*z[25];
    z[27]=z[5]*z[9];
    z[28]=z[9] - n<T>(35,2)*z[27];
    z[28]=z[5]*z[28];
    z[25]=z[28] + z[25];
    z[28]=n<T>(403,9)*z[8];
    z[29]= - static_cast<T>(9)- z[28];
    z[10]=z[29]*z[10];
    z[29]= - static_cast<T>(1)+ z[19];
    z[29]=z[29]*z[14];
    z[10]=z[29] + z[17] + static_cast<T>(1)+ z[10];
    z[29]=n<T>(7,2)*z[9];
    z[30]=n<T>(205,3) + n<T>(349,2)*z[8];
    z[30]=n<T>(1,3)*z[30] + z[29];
    z[30]=n<T>(1,4)*z[30] - n<T>(152,9)*z[4];
    z[30]=z[4]*z[30];
    z[10]=n<T>(1,4)*z[10] + z[30];
    z[10]=z[4]*z[10];
    z[10]=z[12] + n<T>(1,8)*z[25] + z[10];
    z[10]=z[2]*z[10];
    z[12]=5*z[9];
    z[25]=z[12] + n<T>(563,9)*z[8];
    z[30]=z[25]*z[15];
    z[31]=static_cast<T>(9)- n<T>(403,18)*z[8];
    z[31]=z[8]*z[31];
    z[30]=z[30] + z[31] + z[9];
    z[30]=z[4]*z[30];
    z[31]=z[19] + 1;
    z[32]=z[5]*z[31];
    z[30]=z[30] - n<T>(9,2)*z[32] + z[23] - z[29];
    z[30]=z[4]*z[30];
    z[32]= - z[9]*z[14];
    z[32]= - z[13] + z[32];
    z[32]=z[32]*z[26];
    z[33]= - static_cast<T>(1)- z[14];
    z[33]=z[5]*z[33];
    z[33]=z[23] + z[33];
    z[33]=z[3]*z[33];
    z[30]=z[30] + z[32] + z[33];
    z[30]=z[2]*z[30];
    z[32]= - z[17] - z[3];
    z[33]=npow(z[5],2);
    z[32]=z[33]*z[32];
    z[34]=z[28] - 9;
    z[34]=z[34]*z[8];
    z[25]=z[25]*z[4];
    z[35]=z[25] - z[34] + z[9];
    z[35]=z[4]*z[35];
    z[35]= - n<T>(27,2)*z[33] + z[35];
    z[35]=z[4]*z[35];
    z[30]=z[30] + 9*z[32] + z[35];
    z[30]=z[2]*z[30];
    z[25]= - z[21] + z[25];
    z[25]=z[25]*npow(z[4],2);
    z[25]=n<T>(1,2)*z[25] + z[30];
    z[25]=z[7]*z[25];
    z[30]= - static_cast<T>(19)+ z[6];
    z[30]=z[5]*z[30];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[30]*z[26];
    z[32]=z[6] - 1;
    z[35]=z[32]*z[26];
    z[35]=static_cast<T>(1)+ z[35];
    z[35]=z[3]*z[35];
    z[30]=z[30] + z[35];
    z[35]=n<T>(1,4)*z[3];
    z[30]=z[30]*z[35];
    z[36]= - static_cast<T>(1)- n<T>(1,8)*z[6];
    z[27]=z[36]*z[27];
    z[27]= - n<T>(1,8)*z[9] + z[27];
    z[27]=z[5]*z[27];
    z[27]=z[27] + z[30];
    z[30]=z[21] + 27*z[33];
    z[33]= - static_cast<T>(1)+ n<T>(143,9)*z[3];
    z[33]=z[3]*z[33];
    z[30]=n<T>(1,4)*z[30] + z[33];
    z[33]= - static_cast<T>(125)- n<T>(599,4)*z[3];
    z[33]=z[3]*z[33];
    z[33]=n<T>(45,8)*z[8] + n<T>(1,9)*z[33];
    z[36]=static_cast<T>(2)+ z[3];
    z[36]=z[3]*z[36];
    z[36]= - static_cast<T>(1)+ z[36];
    z[36]=z[4]*z[36];
    z[33]=n<T>(1,2)*z[33] + n<T>(38,9)*z[36];
    z[33]=z[4]*z[33];
    z[30]=n<T>(1,4)*z[30] + z[33];
    z[30]=z[4]*z[30];
    z[10]=n<T>(1,8)*z[25] + z[10] + n<T>(1,2)*z[27] + z[30];
    z[10]=z[7]*z[10];
    z[20]=z[20]*z[6];
    z[25]= - n<T>(45,4) + z[28];
    z[25]=z[8]*z[25];
    z[25]= - n<T>(403,36)*z[20] + n<T>(1,4) + z[25];
    z[25]=z[6]*z[25];
    z[27]= - n<T>(77,3) - z[11];
    z[27]=z[8]*z[27];
    z[27]=n<T>(187,2) + z[27];
    z[25]= - z[1] + n<T>(1,6)*z[27] + z[25];
    z[27]=static_cast<T>(1)+ n<T>(5,8)*z[6];
    z[30]=static_cast<T>(3)+ z[6];
    z[30]=z[6]*z[30];
    z[30]= - z[9] + static_cast<T>(3)+ z[30];
    z[30]=z[5]*z[30];
    z[27]=n<T>(9,8)*z[30] + 3*z[27] + n<T>(11,8)*z[9];
    z[27]=z[27]*z[19];
    z[30]=n<T>(3,4)*z[5];
    z[33]= - static_cast<T>(13)- z[14];
    z[33]=z[33]*z[30];
    z[36]=n<T>(1,2)*z[1];
    z[37]=z[36] + 1;
    z[33]= - 5*z[37] + z[33];
    z[33]=z[3]*z[33];
    z[38]=3*z[9];
    z[39]=563*z[8];
    z[40]= - static_cast<T>(599)+ z[39];
    z[40]= - n<T>(143,9)*z[1] + n<T>(1,12)*z[40] + z[38];
    z[15]=z[40]*z[15];
    z[15]=z[15] + z[33] + n<T>(1,2)*z[25] + z[27];
    z[15]=z[2]*z[15];
    z[25]= - z[31]*z[35];
    z[27]= - static_cast<T>(23)- n<T>(93,4)*z[5];
    z[27]=z[5]*z[27];
    z[24]=z[25] + z[27] - static_cast<T>(5)- z[24];
    z[24]=z[3]*z[24];
    z[25]=static_cast<T>(5)+ z[6];
    z[13]=n<T>(9,2)*z[25] - z[13];
    z[13]=z[13]*z[22];
    z[13]=z[13] + n<T>(27,2) + z[12];
    z[13]=z[5]*z[13];
    z[25]=n<T>(347,6) + 215*z[8];
    z[16]=n<T>(1,3)*z[25] - z[16];
    z[25]= - n<T>(617,2) - n<T>(599,3)*z[1];
    z[25]=z[4]*z[25];
    z[16]=n<T>(1,6)*z[25] + n<T>(1,2)*z[16] - z[1];
    z[16]=z[4]*z[16];
    z[25]= - static_cast<T>(9)- n<T>(403,36)*z[8];
    z[25]=z[8]*z[25];
    z[13]=z[16] + z[24] + z[25] + z[13];
    z[13]=n<T>(1,2)*z[13] + z[15];
    z[15]=n<T>(1,2)*z[2];
    z[13]=z[13]*z[15];
    z[16]=n<T>(1,4)*z[9];
    z[24]= - static_cast<T>(3)+ z[6];
    z[24]=z[24]*z[16];
    z[25]= - n<T>(5,4) - z[6];
    z[25]=z[9]*z[25];
    z[25]=n<T>(9,4) + z[25];
    z[25]=z[25]*z[19];
    z[24]=z[24] + z[25];
    z[24]=z[5]*z[24];
    z[25]=5*z[6];
    z[27]= - static_cast<T>(13)- z[25];
    z[27]=z[27]*z[19];
    z[27]=z[27] - static_cast<T>(69)- z[6];
    z[31]=n<T>(1,4)*z[5];
    z[27]=z[27]*z[31];
    z[33]= - static_cast<T>(1)+ 7*z[6];
    z[33]=z[33]*z[19];
    z[33]= - n<T>(403,9) + z[33];
    z[33]=z[33]*z[35];
    z[23]=z[33] + z[27] + n<T>(1,4) + z[23];
    z[23]=z[3]*z[23];
    z[16]=z[23] + z[16] + z[24];
    z[23]= - n<T>(1,2) + z[1];
    z[24]= - static_cast<T>(1015)+ n<T>(1207,2)*z[1];
    z[24]=z[3]*z[24];
    z[23]=n<T>(509,2)*z[23] + z[24];
    z[23]=z[3]*z[23];
    z[23]= - n<T>(565,4) + z[23];
    z[24]= - n<T>(2,3) - z[3];
    z[24]=z[3]*z[24];
    z[24]= - n<T>(1,3) + z[24];
    z[27]=z[1] - 1;
    z[24]=z[4]*z[27]*z[24];
    z[23]=n<T>(1,12)*z[23] + 38*z[24];
    z[23]=z[4]*z[23];
    z[24]=z[1] - 3;
    z[33]=z[24]*z[22];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[33]*z[14];
    z[38]= - n<T>(113,9)*z[8] - z[38];
    z[33]=n<T>(5,2)*z[38] + z[33];
    z[38]=n<T>(2639,2) - 295*z[1];
    z[38]=z[3]*z[38];
    z[38]=n<T>(1,18)*z[38] - n<T>(527,36) + z[1];
    z[38]=z[3]*z[38];
    z[33]=n<T>(1,2)*z[33] + z[38];
    z[23]=n<T>(1,4)*z[33] + n<T>(1,3)*z[23];
    z[23]=z[4]*z[23];
    z[10]=z[10] + z[13] + n<T>(1,4)*z[16] + z[23];
    z[10]=z[7]*z[10];
    z[13]=z[6]*z[21];
    z[13]=z[13] - static_cast<T>(1)- z[34];
    z[16]=n<T>(1,2)*z[6];
    z[13]=z[13]*z[16];
    z[21]= - static_cast<T>(125)+ 161*z[8];
    z[13]=z[36] - z[29] + n<T>(1,9)*z[21] + z[13];
    z[21]= - n<T>(45,2) + z[28];
    z[21]=z[8]*z[21];
    z[20]=z[21] - n<T>(403,18)*z[20];
    z[20]=z[6]*z[20];
    z[11]= - static_cast<T>(79)- z[11];
    z[11]=z[8]*z[11];
    z[11]=n<T>(151,2) + z[11];
    z[11]=n<T>(1,9)*z[11] + z[20];
    z[11]=z[6]*z[11];
    z[20]= - static_cast<T>(401)+ z[39];
    z[21]=z[1] - n<T>(401,18) - z[6];
    z[21]=z[1]*z[21];
    z[11]=z[21] - n<T>(9,2)*z[9] + n<T>(1,18)*z[20] + z[11];
    z[20]=static_cast<T>(1)+ n<T>(5,4)*z[6];
    z[20]=z[6]*z[20];
    z[20]=n<T>(3,4) + z[20];
    z[21]=static_cast<T>(1)+ z[6];
    z[21]=z[6]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[6]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=3*z[21] - z[9];
    z[21]=z[5]*z[21];
    z[20]=n<T>(9,4)*z[21] + 9*z[20] + z[12];
    z[20]=z[5]*z[20];
    z[21]= - static_cast<T>(19)- z[14];
    z[21]=z[21]*z[26];
    z[23]=z[1] + 1;
    z[21]= - 5*z[23] + z[21];
    z[21]=z[3]*z[21];
    z[11]=z[21] + n<T>(1,2)*z[11] + z[20];
    z[11]=z[11]*z[18];
    z[16]=static_cast<T>(1)+ z[16];
    z[12]=static_cast<T>(9)- z[12];
    z[12]=z[5]*z[12];
    z[12]=n<T>(3,16)*z[12] + n<T>(9,8)*z[16] + z[9];
    z[12]=z[5]*z[12];
    z[16]= - z[22] - z[37];
    z[18]=n<T>(1,2)*z[3];
    z[16]=z[16]*z[18];
    z[20]= - static_cast<T>(1)- z[30];
    z[20]=z[5]*z[20];
    z[16]=z[16] - n<T>(9,2) + 13*z[20];
    z[16]=z[16]*z[35];
    z[20]=z[4]*z[1];
    z[11]=z[11] - n<T>(313,72)*z[20] + z[16] + n<T>(1,8)*z[13] + z[12];
    z[11]=z[2]*z[11];
    z[12]= - n<T>(2,3) + z[3];
    z[12]=z[3]*z[12];
    z[12]= - n<T>(1,3) + z[12];
    z[13]=z[1] - 2;
    z[13]=z[13]*z[1];
    z[13]=z[13] + 1;
    z[16]=n<T>(38,3)*z[4];
    z[12]=z[16]*z[13]*z[12];
    z[13]=static_cast<T>(40)- n<T>(617,72)*z[1];
    z[13]=z[1]*z[13];
    z[13]= - n<T>(2263,72) + z[13];
    z[13]=z[3]*z[13];
    z[13]= - n<T>(805,36)*z[27] + z[13];
    z[13]=z[3]*z[13];
    z[12]=z[12] - n<T>(725,144)*z[27] + z[13];
    z[12]=z[4]*z[12];
    z[13]=z[36] - 1;
    z[13]=z[13]*z[1];
    z[20]=z[5]*z[13];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[20]*z[14];
    z[20]=n<T>(5,2) + z[20];
    z[21]= - n<T>(3091,2) + 313*z[1];
    z[28]=n<T>(425,6) - 44*z[1];
    z[28]=z[3]*z[28];
    z[21]=n<T>(1,24)*z[21] + z[28];
    z[21]=z[3]*z[21];
    z[12]=z[12] + n<T>(3,8)*z[20] + n<T>(1,3)*z[21];
    z[12]=z[4]*z[12];
    z[20]=11*z[6];
    z[21]= - static_cast<T>(3)+ z[20];
    z[17]=z[21]*z[17];
    z[21]= - static_cast<T>(1)- n<T>(7,2)*z[6];
    z[21]=z[9]*z[21];
    z[21]=z[21] + n<T>(9,2)*z[1];
    z[19]=z[21]*z[19];
    z[17]=z[17] + z[19];
    z[17]=z[5]*z[17];
    z[19]= - static_cast<T>(1)- 9*z[9];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[19]= - static_cast<T>(7)- z[20];
    z[19]=z[19]*z[30];
    z[19]=z[19] - n<T>(141,4) - z[25];
    z[19]=z[19]*z[31];
    z[20]= - static_cast<T>(1)+ 13*z[6];
    z[20]=z[5]*z[20];
    z[20]= - n<T>(67,9) + n<T>(3,8)*z[20];
    z[20]=z[20]*z[18];
    z[19]=z[20] + n<T>(28,9) + z[19];
    z[19]=z[3]*z[19];
    z[10]=z[10] + z[11] + z[12] + n<T>(1,8)*z[17] + z[19];
    z[10]=z[7]*z[10];
    z[11]=z[1] + n<T>(1,2);
    z[11]=z[11]*z[1];
    z[12]= - n<T>(1,2) - z[11];
    z[12]=z[1]*z[12];
    z[12]= - z[26] - n<T>(1,2) + z[12];
    z[12]=z[3]*z[12];
    z[17]=z[26] - 1;
    z[17]=z[17]*z[5];
    z[19]=static_cast<T>(1)+ n<T>(3,2)*z[1];
    z[19]=z[1]*z[19];
    z[12]=z[12] - z[17] + static_cast<T>(1)+ z[19];
    z[12]=z[3]*z[12];
    z[19]=z[9] - 1;
    z[20]= - z[32]*z[19];
    z[21]= - z[1] + z[32];
    z[21]=z[1]*z[21];
    z[20]=z[21] + z[20];
    z[21]=z[23]*z[1];
    z[19]=z[21] - z[19];
    z[15]=z[15]*z[6]*z[19];
    z[17]=z[9]*z[17];
    z[12]=z[15] + z[12] + n<T>(1,2)*z[20] + z[17];
    z[12]=z[2]*z[12];
    z[15]=z[21] + z[6];
    z[17]=z[25] - n<T>(1,2);
    z[17]=z[17]*z[9];
    z[15]= - z[17] + n<T>(27,2)*z[15];
    z[15]=z[5]*z[15];
    z[15]=z[15] - 9*z[1] + n<T>(45,2) + z[17];
    z[15]=z[5]*z[15];
    z[17]=19*z[6];
    z[19]= - static_cast<T>(1)- 17*z[6];
    z[19]=z[5]*z[19];
    z[19]=z[19] - static_cast<T>(71)- z[17];
    z[19]=z[5]*z[19];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[5]*z[17];
    z[17]= - n<T>(115,9) + z[17];
    z[17]=z[3]*z[17];
    z[17]=z[17] - n<T>(47,9) + z[19];
    z[17]=z[17]*z[18];
    z[12]=z[12] + z[17] + z[15] + static_cast<T>(5)- z[36];
    z[15]=static_cast<T>(2)- z[3];
    z[15]=z[3]*z[15];
    z[15]=z[15] - 1;
    z[17]= - static_cast<T>(1)+ n<T>(1,3)*z[1];
    z[17]=z[17]*z[1];
    z[17]=z[17] + 1;
    z[17]=z[17]*z[1];
    z[17]=z[17] - n<T>(1,3);
    z[15]=z[16]*z[17]*z[15];
    z[16]=n<T>(2765,18) - z[1];
    z[16]=z[16]*z[36];
    z[16]= - n<T>(1369,9) + z[16];
    z[16]=z[1]*z[16];
    z[16]=n<T>(2729,36) + z[16];
    z[17]= - n<T>(425,9) + z[36];
    z[17]=z[1]*z[17];
    z[17]=n<T>(1673,18) + z[17];
    z[17]=z[1]*z[17];
    z[17]= - n<T>(104,9) + n<T>(1,4)*z[17];
    z[17]=z[3]*z[17];
    z[16]=n<T>(1,4)*z[16] + z[17];
    z[16]=z[3]*z[16];
    z[13]= - n<T>(1,2) - z[13];
    z[13]=z[15] + n<T>(355,24)*z[13] + z[16];
    z[13]=z[4]*z[13];
    z[15]=z[27]*npow(z[1],2)*z[22];
    z[16]= - z[1]*z[24];
    z[15]=z[16] + z[15];
    z[14]=z[15]*z[14];
    z[14]= - n<T>(367,18)*z[27] + z[14];
    z[15]=n<T>(1655,9) - 3*z[1];
    z[15]=z[1]*z[15];
    z[11]= - n<T>(85,9) + n<T>(1,8)*z[11];
    z[11]=z[1]*z[11];
    z[11]=n<T>(1333,144) + z[11];
    z[11]=z[3]*z[11];
    z[11]=z[11] - n<T>(122,9) + n<T>(1,16)*z[15];
    z[11]=z[3]*z[11];
    z[11]=z[13] + n<T>(1,8)*z[14] + z[11];
    z[11]=z[4]*z[11];

    r += z[10] + z[11] + n<T>(1,8)*z[12];
 
    return r;
}

template double qg_2lha_r934(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r934(const std::array<dd_real,30>&);
#endif
