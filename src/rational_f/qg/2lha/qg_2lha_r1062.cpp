#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1062(const std::array<T,30>& k) {
  T z[24];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[6];
    z[8]=k[3];
    z[9]=k[9];
    z[10]=k[10];
    z[11]=k[14];
    z[12]=z[8] + 1;
    z[12]=z[12]*z[10];
    z[12]=z[12] + 3;
    z[13]=n<T>(3,4)*z[10];
    z[12]=z[12]*z[13];
    z[13]=z[10]*z[8];
    z[14]= - z[8]*z[5];
    z[14]=z[14] - n<T>(3,2)*z[13];
    z[15]=n<T>(1,2)*z[9];
    z[14]=z[14]*z[15];
    z[16]=static_cast<T>(3)- z[5];
    z[14]=z[14] + z[12] + n<T>(1,2)*z[16] - z[8];
    z[14]=z[9]*z[14];
    z[16]=z[8] - n<T>(1,2);
    z[17]=n<T>(3,2)*z[10];
    z[18]= - z[16]*z[17];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[7]*z[18];
    z[12]= - z[12] + z[18];
    z[12]=z[7]*z[12];
    z[18]=z[1] + 1;
    z[19]=n<T>(1,2)*z[7];
    z[20]=z[18]*z[19];
    z[21]=z[7] - 3;
    z[21]=z[21]*z[20];
    z[22]= - static_cast<T>(1)+ n<T>(1,2)*z[5];
    z[18]=z[18]*z[22];
    z[21]=z[21] - z[18];
    z[21]=z[21]*z[3];
    z[22]= - static_cast<T>(1)+ z[19];
    z[22]=z[7]*z[22];
    z[22]= - z[21] + 3*z[22] + n<T>(1,2) - z[1];
    z[22]=z[3]*z[22];
    z[12]=z[22] + z[12] - static_cast<T>(1)+ z[14];
    z[14]= - static_cast<T>(1)+ z[5];
    z[14]=z[8]*z[14];
    z[14]=z[14] - z[13];
    z[22]=z[5] - n<T>(1,2);
    z[23]=z[9]*z[22]*npow(z[8],2);
    z[14]=3*z[14] + z[23];
    z[14]=z[14]*z[15];
    z[15]=static_cast<T>(1)+ z[13];
    z[15]=3*z[15] - z[19];
    z[15]=z[15]*z[19];
    z[14]= - z[21] + z[15] + z[14] - n<T>(5,4) + z[5];
    z[14]=z[6]*z[14];
    z[12]=n<T>(1,2)*z[12] + z[14];
    z[12]=z[6]*z[12];
    z[14]=3*z[10];
    z[15]=static_cast<T>(1)+ n<T>(1,2)*z[10];
    z[14]=z[14]*z[15];
    z[21]=z[5]*z[11];
    z[21]=z[21] + 1;
    z[23]= - z[14] - z[21];
    z[15]=z[15]*z[17];
    z[15]= - n<T>(1,3)*z[22] + z[15];
    z[15]=z[9]*z[15];
    z[15]=n<T>(1,2)*z[23] + z[15];
    z[15]=z[9]*z[15];
    z[17]=npow(z[10],2);
    z[22]=z[8]*z[17];
    z[22]= - static_cast<T>(1)+ 3*z[22];
    z[22]=z[22]*z[19];
    z[14]=z[14] + z[22];
    z[14]=z[14]*z[19];
    z[14]=z[15] + z[14];
    z[15]= - static_cast<T>(1)+ n<T>(1,3)*z[7];
    z[20]=z[15]*z[20];
    z[18]= - n<T>(1,3)*z[18] + z[20];
    z[18]=z[3]*z[18];
    z[15]=z[15]*z[7];
    z[20]= - static_cast<T>(1)- z[5];
    z[20]=z[15] + n<T>(1,3)*z[20] - z[1];
    z[18]=n<T>(1,4)*z[20] + z[18];
    z[18]=z[3]*z[18];
    z[20]=static_cast<T>(3)+ z[1];
    z[22]= - static_cast<T>(1)- n<T>(1,2)*z[1];
    z[22]=z[7]*z[22];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[20]=z[7]*z[20];
    z[20]= - n<T>(1,2)*z[21] + z[20];
    z[18]=n<T>(1,2)*z[20] + z[18];
    z[18]=z[3]*z[18];
    z[12]=z[12] + n<T>(1,2)*z[14] + z[18];
    z[12]=z[6]*z[12];
    z[14]=z[1]*z[4];
    z[18]= - static_cast<T>(1)+ z[14];
    z[18]=z[18]*z[19];
    z[18]=z[18] + n<T>(1,2) - z[14];
    z[18]=z[7]*z[18];
    z[18]=z[18] + n<T>(1,3) + n<T>(1,2)*z[14];
    z[15]= - z[14]*z[15];
    z[20]= - static_cast<T>(1)+ n<T>(1,3)*z[2];
    z[20]=z[4]*z[20];
    z[20]=n<T>(1,3) + z[20];
    z[20]=z[1]*z[20];
    z[15]=z[15] + n<T>(1,3) + z[20];
    z[15]=z[3]*z[15];
    z[15]=n<T>(1,2)*z[18] + z[15];
    z[15]=z[3]*z[15];
    z[18]=n<T>(1,2) + z[14];
    z[18]=z[7]*z[18];
    z[14]= - z[14] + z[18];
    z[14]=z[14]*z[19];
    z[18]=z[11] + n<T>(1,2);
    z[19]=z[11]*z[18];
    z[14]=z[15] + n<T>(1,3)*z[19] + z[14];
    z[14]=z[3]*z[14];
    z[15]= - static_cast<T>(1)- z[4];
    z[13]=z[15]*z[13];
    z[15]=z[4]*z[16];
    z[13]=n<T>(1,2)*z[13] + z[15];
    z[13]=z[10]*z[13];
    z[13]=z[4] + z[13];
    z[13]=z[7]*z[13];
    z[13]= - n<T>(1,2)*z[17] + z[13];
    z[13]=z[7]*z[13];
    z[15]=z[18] + z[9];
    z[15]=z[9]*z[11]*z[15];
    z[13]=z[14] + n<T>(1,3)*z[15] + n<T>(3,4)*z[13];
    z[12]=n<T>(1,2)*z[13] + z[12];

    r += n<T>(1,8)*z[12];
 
    return r;
}

template double qg_2lha_r1062(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1062(const std::array<dd_real,30>&);
#endif
