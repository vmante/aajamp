#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1604(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[4];
    z[2]=k[6];
    z[3]=k[9];
    z[4]=k[11];
    z[5]=k[5];
    z[6]=k[12];
    z[7]=k[7];
    z[8]= - static_cast<T>(1)+ 3*z[3];
    z[9]=npow(z[3],3);
    z[10]=n<T>(1,2)*z[9];
    z[8]=z[8]*z[10];
    z[11]=n<T>(1,2)*z[3];
    z[12]=z[11] - 3;
    z[13]=z[9]*z[4];
    z[14]= - z[12]*z[13];
    z[14]= - z[8] + z[14];
    z[14]=z[4]*z[14];
    z[14]= - z[10] + z[14];
    z[14]=z[4]*z[14];
    z[15]= - static_cast<T>(1)+ n<T>(3,2)*z[3];
    z[15]=z[15]*z[9];
    z[16]= - z[15] + z[13];
    z[16]=z[4]*z[16];
    z[16]= - z[15] + z[16];
    z[17]=z[5]*z[4];
    z[16]=z[16]*z[17];
    z[18]=npow(z[3],4);
    z[19]=z[18]*z[4];
    z[20]= - n<T>(1,2)*z[18] - z[19];
    z[21]=npow(z[4],2);
    z[20]=z[20]*z[21];
    z[19]= - z[18] - z[19];
    z[19]=z[4]*z[19];
    z[19]= - z[18] + z[19];
    z[22]=n<T>(1,2)*z[4];
    z[19]=z[5]*z[19]*z[22];
    z[19]=z[20] + z[19];
    z[19]=z[1]*z[19];
    z[14]=z[19] + z[14] + z[16];
    z[16]=n<T>(1,2)*z[1];
    z[14]=z[16]*z[14];
    z[19]=z[11] - 1;
    z[20]=z[19]*z[3];
    z[23]=z[20] - n<T>(3,4);
    z[24]=npow(z[3],2);
    z[23]=z[23]*z[24];
    z[25]=z[22]*z[24];
    z[26]=z[3] - 3;
    z[27]=z[26]*z[25];
    z[27]= - z[23] + z[27];
    z[27]=z[4]*z[27];
    z[28]=z[3] - n<T>(1,2);
    z[29]=z[28]*z[24];
    z[30]=n<T>(1,2)*z[29];
    z[27]= - z[30] + z[27];
    z[27]=z[4]*z[27];
    z[31]= - static_cast<T>(1)+ n<T>(3,4)*z[3];
    z[31]=z[31]*z[3];
    z[31]=z[31] + n<T>(3,8);
    z[31]=z[31]*z[24];
    z[32]=z[24]*z[4];
    z[33]= - n<T>(1,4)*z[4] - n<T>(1,4) + z[3];
    z[33]=z[33]*z[32];
    z[33]= - z[31] + z[33];
    z[33]=z[33]*z[17];
    z[14]=z[14] + z[27] + z[33];
    z[14]=z[1]*z[5]*z[14];
    z[27]=z[11] + 1;
    z[27]=z[27]*z[3];
    z[33]=z[27] + n<T>(7,4);
    z[34]=z[4]*z[3];
    z[35]= - z[33]*z[34];
    z[36]=3*z[4];
    z[37]= - z[36] + z[17];
    z[37]=z[5]*z[37];
    z[37]=7*z[4] + z[37];
    z[38]=n<T>(1,4)*z[5];
    z[37]=z[37]*z[38];
    z[39]=z[3]*z[33];
    z[39]=n<T>(11,4) + z[39];
    z[39]=z[3]*z[39];
    z[40]=static_cast<T>(1)- z[38];
    z[40]=z[5]*z[40];
    z[40]= - n<T>(11,4) + z[40];
    z[40]=z[5]*z[40];
    z[39]=z[39] + z[40];
    z[39]=z[2]*z[39];
    z[35]=z[39] + z[35] + z[37];
    z[35]=z[7]*z[35];
    z[37]= - z[19]*z[34];
    z[39]=z[3] + n<T>(1,2);
    z[39]=z[39]*z[3];
    z[39]=z[39] - n<T>(5,2);
    z[39]=z[39]*z[3];
    z[37]=z[39] + z[37];
    z[37]=z[4]*z[37];
    z[27]= - n<T>(1,4) - z[27];
    z[27]=z[3]*z[27];
    z[27]=n<T>(3,2) + z[27];
    z[27]=z[3]*z[27];
    z[27]=z[27] + z[37];
    z[27]=z[4]*z[27];
    z[28]=z[3]*z[28];
    z[28]=n<T>(1,2) + z[28];
    z[28]=z[3]*z[28];
    z[25]=z[28] - z[25];
    z[17]=z[25]*z[17];
    z[17]=z[27] + z[17];
    z[17]=z[5]*z[17];
    z[17]=z[17] + z[35];
    z[25]=z[26]*z[11];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[34];
    z[23]= - z[23] + z[25];
    z[23]=z[23]*z[21];
    z[12]= - z[3]*z[12];
    z[12]= - static_cast<T>(3)+ z[12];
    z[12]=z[12]*z[32];
    z[8]= - z[8] + z[12];
    z[8]=z[8]*z[21];
    z[12]= - z[19]*z[13];
    z[13]=n<T>(1,4)*z[18];
    z[12]= - z[13] + z[12];
    z[12]=z[12]*z[21];
    z[13]= - z[1]*npow(z[4],3)*z[13];
    z[12]=z[12] + z[13];
    z[12]=z[1]*z[12];
    z[8]=n<T>(1,2)*z[8] + z[12];
    z[8]=z[1]*z[8];
    z[8]=z[23] + z[8];
    z[8]=z[1]*z[8];
    z[12]= - z[33]*z[24];
    z[13]= - n<T>(1,2) - z[20];
    z[13]=z[4]*z[13];
    z[13]=z[39] + z[13];
    z[13]=z[4]*z[13];
    z[12]=z[12] + z[13];
    z[12]=z[4]*z[12];
    z[13]= - z[36] + z[5];
    z[13]=z[13]*z[38];
    z[19]=z[3] - 1;
    z[21]= - z[4]*z[19];
    z[21]=n<T>(7,2)*z[3] + z[21];
    z[21]=z[4]*z[21];
    z[13]=z[13] - n<T>(11,4)*z[3] + z[21];
    z[13]=z[5]*z[13];
    z[12]=z[12] + z[13];
    z[8]=n<T>(1,2)*z[12] + z[8];
    z[8]=z[2]*z[8];
    z[9]=z[9]*z[22];
    z[12]= - z[5]*z[15];
    z[10]= - z[10] + z[12];
    z[10]=z[5]*z[10];
    z[12]= - z[18]*npow(z[5],2)*z[16];
    z[9]=z[12] + z[9] + z[10];
    z[9]=z[9]*z[16];
    z[10]= - z[5]*z[31];
    z[10]= - z[30] + z[10];
    z[10]=z[5]*z[10];
    z[12]=z[22]*z[29];
    z[9]=z[9] + z[12] + z[10];
    z[9]=z[1]*z[9];
    z[10]=z[19]*z[3];
    z[10]=z[10] + n<T>(1,2);
    z[10]=z[10]*z[11];
    z[11]=z[4]*z[10];
    z[12]= - n<T>(3,4) - z[20];
    z[12]=z[3]*z[12];
    z[12]=n<T>(1,4) + z[12];
    z[12]=z[5]*z[3]*z[12];
    z[10]= - z[10] + z[12];
    z[10]=z[5]*z[10];
    z[10]=z[11] + z[10];
    z[9]=n<T>(1,2)*z[10] + z[9];
    z[9]=z[6]*z[9];
    z[8]=z[9] + z[8] + z[14] + n<T>(1,2)*z[17];

    r += n<T>(1,4)*z[8];
 
    return r;
}

template double qg_2lha_r1604(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1604(const std::array<dd_real,30>&);
#endif
