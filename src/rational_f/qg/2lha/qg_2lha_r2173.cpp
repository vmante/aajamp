#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2173(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[6];
    z[6]=k[2];
    z[7]=k[11];
    z[8]=k[3];
    z[9]=k[9];
    z[10]=k[27];
    z[11]=n<T>(1,4)*z[4];
    z[12]=z[11] - 1;
    z[13]=n<T>(1,3)*z[4];
    z[12]=z[12]*z[13];
    z[14]= - n<T>(1,2) - z[12];
    z[14]=z[4]*z[14];
    z[14]=n<T>(1,4) + z[14];
    z[14]=z[5]*z[14];
    z[15]=n<T>(1,2)*z[4];
    z[16]=static_cast<T>(2)- z[15];
    z[16]=z[16]*z[13];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[4]*z[16];
    z[16]=n<T>(1,2) + z[16];
    z[16]=z[5]*z[16];
    z[16]=n<T>(1,6) + z[16];
    z[16]=z[8]*z[16];
    z[14]=z[16] + n<T>(1,12) + z[14];
    z[16]=z[8]*z[5];
    z[14]=z[14]*z[16];
    z[12]= - n<T>(1,4) - z[12];
    z[12]=z[5]*z[12];
    z[12]= - n<T>(1,6) + z[12];
    z[17]=static_cast<T>(5)- z[4];
    z[17]=z[17]*z[15];
    z[17]= - static_cast<T>(5)+ z[17];
    z[17]=z[17]*z[13];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[5]*z[17];
    z[17]=n<T>(1,2) + z[17];
    z[17]=z[5]*z[17];
    z[17]=n<T>(1,6) + z[17];
    z[17]=z[8]*z[17];
    z[12]=5*z[12] + z[17];
    z[12]=z[8]*z[12];
    z[17]=z[4] - 1;
    z[12]=n<T>(7,12)*z[17] + z[12];
    z[12]=z[7]*z[12];
    z[18]=z[4] - 3;
    z[19]=z[18]*z[15];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[5]*z[19];
    z[20]=static_cast<T>(2)- n<T>(11,12)*z[4];
    z[20]=z[4]*z[20];
    z[12]=n<T>(1,2)*z[12] + z[14] + n<T>(5,4)*z[19] - n<T>(11,24) + z[20];
    z[12]=z[7]*z[12];
    z[14]=npow(z[5],2);
    z[19]=z[14]*z[8];
    z[14]= - n<T>(1,2)*z[14] - z[19];
    z[20]=n<T>(1,3)*z[8];
    z[14]=z[14]*z[20];
    z[21]= - n<T>(1,3) - n<T>(1,2)*z[5];
    z[16]=z[21]*z[16];
    z[21]=static_cast<T>(1)+ z[5];
    z[16]=n<T>(5,6)*z[21] + z[16];
    z[16]=z[8]*z[16];
    z[16]=n<T>(7,12) + z[16];
    z[16]=z[7]*z[16];
    z[21]=n<T>(13,3) - 5*z[5];
    z[14]=z[16] + n<T>(1,4)*z[21] + z[14];
    z[14]=z[7]*z[14];
    z[16]=z[17]*z[15];
    z[17]=z[17]*npow(z[4],2);
    z[21]=z[17]*z[8];
    z[16]=z[16] + z[21];
    z[22]=npow(z[7],2);
    z[19]= - n<T>(5,2)*z[5] + z[19];
    z[19]=z[8]*z[19];
    z[19]= - n<T>(7,2) + z[19];
    z[19]=z[19]*z[22];
    z[19]= - z[17] + n<T>(1,3)*z[19];
    z[23]=n<T>(1,2)*z[2];
    z[19]=z[19]*z[23];
    z[14]=z[19] + n<T>(1,3)*z[16] + z[14];
    z[14]=z[14]*z[23];
    z[16]=static_cast<T>(5)- 3*z[4];
    z[16]=z[16]*z[15];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[16]*z[15];
    z[18]=z[18]*z[4];
    z[18]=z[18] + 3;
    z[18]=z[18]*z[4];
    z[18]=z[18] - 1;
    z[19]=n<T>(1,4)*z[5];
    z[18]=z[18]*z[19];
    z[19]= - n<T>(5,2) + z[4];
    z[19]=z[4]*z[19];
    z[19]=static_cast<T>(2)+ z[19];
    z[19]=z[4]*z[19];
    z[19]= - z[18] - n<T>(1,2) + z[19];
    z[19]=z[5]*z[19];
    z[24]=z[17]*z[23];
    z[25]=static_cast<T>(1)- z[23];
    z[25]=z[25]*z[24];
    z[16]=z[25] + z[16] + z[19];
    z[16]=z[3]*z[16];
    z[19]= - n<T>(5,3) + z[4];
    z[19]=z[19]*z[15];
    z[19]=n<T>(1,3) + z[19];
    z[19]=z[4]*z[19];
    z[25]=z[15] - 1;
    z[26]=z[25]*z[4];
    z[26]=z[26] + n<T>(1,2);
    z[27]=z[26]*z[5];
    z[28]= - z[13]*z[27];
    z[19]=z[19] + z[28];
    z[19]=z[5]*z[19];
    z[27]= - z[15]*z[27];
    z[28]= - n<T>(7,4) + z[4];
    z[28]=z[4]*z[28];
    z[28]=n<T>(3,4) + z[28];
    z[28]=z[4]*z[28];
    z[27]=z[28] + z[27];
    z[27]=z[5]*z[27];
    z[28]= - n<T>(1,2) + z[4];
    z[28]=z[4]*z[28];
    z[28]= - n<T>(1,2) + z[28];
    z[28]=z[4]*z[28];
    z[24]=z[28] - z[24];
    z[23]=z[24]*z[23];
    z[23]=z[23] - n<T>(3,4)*z[17] + z[27];
    z[23]=z[3]*z[23];
    z[24]=n<T>(1,6)*z[2] - n<T>(1,3);
    z[24]=z[17]*z[24];
    z[19]=z[23] + z[19] + z[24];
    z[19]=z[1]*z[19];
    z[23]=n<T>(13,6) - z[4];
    z[15]=z[23]*z[15];
    z[15]= - n<T>(2,3) + z[15];
    z[15]=z[4]*z[15];
    z[15]=z[18] + n<T>(1,12) + z[15];
    z[15]=z[5]*z[15];
    z[18]=z[20]*z[17];
    z[23]=n<T>(1,2) - z[13];
    z[23]=z[4]*z[23];
    z[23]= - n<T>(1,6) + z[23];
    z[23]=z[4]*z[23];
    z[18]=z[23] + z[18];
    z[23]=n<T>(1,2)*z[8];
    z[18]=z[18]*z[23];
    z[24]= - static_cast<T>(1)+ n<T>(5,8)*z[4];
    z[24]=z[4]*z[24];
    z[27]=z[2]*z[21];
    z[18]= - n<T>(1,4)*z[27] + z[24] + z[18];
    z[18]=z[2]*z[18];
    z[24]=z[26]*z[4];
    z[21]=z[24] + z[21];
    z[20]=z[21]*z[20];
    z[21]= - static_cast<T>(3)+ n<T>(7,6)*z[4];
    z[21]=z[4]*z[21];
    z[21]=n<T>(11,6) + z[21];
    z[20]=n<T>(1,2)*z[21] + z[20];
    z[18]=n<T>(1,2)*z[20] + z[18];
    z[18]=z[9]*z[18];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[4]*z[13];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[4]*z[13];
    z[13]= - n<T>(1,3) + z[13];
    z[13]=z[5]*z[13];
    z[13]= - z[24] + z[13];
    z[13]=z[5]*z[13];
    z[13]=n<T>(1,6)*z[17] + z[13];
    z[13]=z[8]*z[13];
    z[17]=z[25] + z[23];
    z[20]=z[7] - z[9];
    z[17]=z[10]*z[2]*z[17]*z[20];
    z[20]= - n<T>(7,4)*z[6] + n<T>(7,2)*z[2];
    z[20]=z[22]*z[20];
    z[21]= - static_cast<T>(7)- 5*z[8];
    z[21]=z[7]*z[21];
    z[21]= - static_cast<T>(1)+ n<T>(1,4)*z[21];
    z[21]=z[7]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[6]*z[20];
    z[11]= - n<T>(1,3) + z[11];
    z[11]=z[4]*z[11];
    z[11]=n<T>(1,12) + z[11];
    z[11]=z[4]*z[11];

    r += z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + n<T>(3,4)*z[17] + 
      z[18] + z[19] + n<T>(1,6)*z[20];
 
    return r;
}

template double qg_2lha_r2173(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2173(const std::array<dd_real,30>&);
#endif
