#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1000(const std::array<T,30>& k) {
  T z[35];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[3];
    z[7]=k[5];
    z[8]=k[10];
    z[9]=k[27];
    z[10]=k[20];
    z[11]=z[10] - n<T>(5,2);
    z[12]=n<T>(1,2)*z[10];
    z[13]=npow(z[7],2);
    z[14]=z[11]*z[13]*z[12];
    z[15]=npow(z[9],2);
    z[16]=z[15]*z[6];
    z[17]=z[9] - 1;
    z[18]=z[17]*z[9];
    z[16]=z[18] + n<T>(3,2)*z[16];
    z[18]=n<T>(1,2)*z[6];
    z[16]=z[16]*z[18];
    z[16]=z[16] + z[9];
    z[19]=z[6]*z[9];
    z[20]=n<T>(3,2)*z[19] - z[17];
    z[20]=z[6]*z[20];
    z[21]=z[10]*z[13];
    z[20]=z[20] + z[21];
    z[21]=n<T>(1,2)*z[3];
    z[20]=z[20]*z[21];
    z[14]=z[20] + z[14] + n<T>(1,2) - z[16];
    z[14]=z[3]*z[14];
    z[20]= - n<T>(1,2) + z[19];
    z[20]=z[5]*z[20];
    z[16]=z[20] - n<T>(3,2) + z[16];
    z[16]=z[5]*z[16];
    z[20]=z[18] - n<T>(3,2);
    z[11]= - z[10]*z[11];
    z[11]= - n<T>(9,2) + z[11];
    z[11]=z[8]*z[11];
    z[11]=z[11] + z[20];
    z[11]=z[7]*z[11];
    z[22]=static_cast<T>(1)+ z[8];
    z[11]=n<T>(3,2)*z[22] + z[11];
    z[22]=n<T>(1,2)*z[7];
    z[11]=z[11]*z[22];
    z[23]=z[13]*z[8];
    z[24]=n<T>(1,2)*z[1];
    z[25]=z[24]*z[23];
    z[26]= - static_cast<T>(1)+ n<T>(3,2)*z[8];
    z[26]=z[7]*z[26];
    z[26]=z[8] + z[26];
    z[26]=z[7]*z[26];
    z[25]=z[26] + z[25];
    z[25]=z[25]*z[24];
    z[11]=z[14] + z[25] + z[11] + n<T>(1,4)*z[8] + z[16];
    z[14]=npow(z[5],2);
    z[16]= - z[14] - z[13];
    z[25]=z[7] + n<T>(1,2);
    z[26]=z[3]*z[7];
    z[27]= - z[25]*z[26];
    z[28]=z[7] + 1;
    z[28]=z[28]*z[7];
    z[27]=z[28] + z[27];
    z[27]=z[3]*z[27];
    z[16]=3*z[16] + z[27];
    z[25]=z[25]*z[3];
    z[27]= - static_cast<T>(1)- z[13];
    z[27]=n<T>(1,2)*z[27] - z[25];
    z[27]=z[3]*z[27];
    z[29]=n<T>(5,2)*z[5];
    z[30]=static_cast<T>(3)+ z[29];
    z[30]=z[5]*z[30];
    z[31]=3*z[7];
    z[32]= - n<T>(5,2) + z[31];
    z[32]=z[7]*z[32];
    z[27]=z[27] + z[30] + z[32];
    z[30]=n<T>(1,2)*z[5];
    z[32]= - static_cast<T>(1)- z[30];
    z[32]=z[32]*z[30];
    z[25]= - z[25] + z[32] + z[7];
    z[25]=z[2]*z[25];
    z[25]=n<T>(1,4)*z[27] + z[25];
    z[25]=z[2]*z[25];
    z[16]=n<T>(1,8)*z[16] + z[25];
    z[16]=z[2]*z[16];
    z[15]=z[15]*z[18];
    z[25]=z[9] - z[15];
    z[25]=z[5]*z[6]*z[25];
    z[27]=npow(z[6],2)*npow(z[9],3);
    z[27]=z[27] - z[9];
    z[25]=z[25] - z[27];
    z[25]=z[5]*z[25];
    z[32]=static_cast<T>(1)- z[8];
    z[32]=z[32]*z[22];
    z[32]= - z[8] + z[32];
    z[32]=z[7]*z[32];
    z[15]= - z[9] - z[15];
    z[15]=z[3]*z[6]*z[15];
    z[15]=z[15] + z[27];
    z[15]=z[3]*z[15];
    z[27]= - z[1]*z[23];
    z[15]=z[15] + z[27] + z[25] + z[32];
    z[25]=z[22] + 1;
    z[25]=z[25]*z[26];
    z[26]=z[28] + z[25];
    z[27]=n<T>(1,4)*z[3];
    z[26]=z[26]*z[27];
    z[25]=z[2]*z[25];
    z[25]=z[26] + z[25];
    z[25]=z[25]*npow(z[2],2);
    z[23]=z[4]*z[23];
    z[15]=n<T>(1,8)*z[23] + n<T>(1,4)*z[15] + z[25];
    z[23]=n<T>(1,2)*z[4];
    z[15]=z[15]*z[23];
    z[11]=z[15] + n<T>(1,4)*z[11] + z[16];
    z[11]=z[4]*z[11];
    z[15]=n<T>(3,2)*z[5];
    z[16]=z[15] + 1;
    z[25]=z[5]*z[16];
    z[26]= - static_cast<T>(1)+ z[31];
    z[26]=z[7]*z[26];
    z[25]=z[25] + z[26];
    z[25]=z[1]*z[25];
    z[26]= - static_cast<T>(3)+ z[7];
    z[26]=z[7]*z[26];
    z[28]=n<T>(1,2) + z[31];
    z[28]=z[3]*z[28];
    z[26]=z[28] - static_cast<T>(1)+ z[26];
    z[26]=z[26]*z[21];
    z[28]= - static_cast<T>(1)- n<T>(11,2)*z[5];
    z[28]=z[5]*z[28];
    z[32]=static_cast<T>(1)- z[7];
    z[32]=z[7]*z[32];
    z[25]=z[26] + z[25] + z[28] + n<T>(3,2)*z[32];
    z[26]= - static_cast<T>(1)- z[5];
    z[26]=z[26]*z[15];
    z[28]=z[1]*z[5];
    z[16]=z[16]*z[28];
    z[32]=z[1] - n<T>(3,2);
    z[32]=z[32]*z[3];
    z[16]= - z[32] + z[26] + z[16];
    z[16]=z[2]*z[16];
    z[26]= - z[32] + z[31] - z[1];
    z[26]=z[3]*z[26];
    z[32]=static_cast<T>(9)+ n<T>(31,2)*z[5];
    z[32]=z[5]*z[32];
    z[26]=z[26] + z[32] - 9*z[7];
    z[32]=n<T>(1,4)*z[5];
    z[33]=5*z[5];
    z[34]= - n<T>(7,2) - z[33];
    z[34]=z[34]*z[32];
    z[34]=z[34] + z[7];
    z[34]=z[1]*z[34];
    z[16]=n<T>(1,2)*z[16] + z[34] + n<T>(1,8)*z[26];
    z[16]=z[2]*z[16];
    z[16]=n<T>(1,4)*z[25] + z[16];
    z[16]=z[2]*z[16];
    z[17]=n<T>(5,2)*z[19] - z[17];
    z[12]=z[12] - 1;
    z[25]= - 3*z[12] - z[22];
    z[25]=z[7]*z[10]*z[25];
    z[26]= - z[10]*z[31];
    z[31]=z[1]*z[6];
    z[26]=z[26] + z[31];
    z[26]=z[26]*z[21];
    z[17]=z[26] + z[24] + n<T>(1,2)*z[17] + z[25];
    z[17]=z[17]*z[27];
    z[25]=n<T>(1,2) - z[8];
    z[13]=z[25]*z[13]*z[24];
    z[25]= - z[6] + n<T>(1,2) + 3*z[8];
    z[22]=z[25]*z[22];
    z[22]=z[22] - n<T>(1,2) - z[8];
    z[22]=z[7]*z[22];
    z[13]=z[22] + z[13];
    z[22]=n<T>(1,4)*z[1];
    z[13]=z[13]*z[22];
    z[25]= - static_cast<T>(3)+ z[10];
    z[25]=z[8]*z[25];
    z[20]=z[25] + z[20];
    z[20]=z[7]*z[20];
    z[25]=z[10]*z[12];
    z[25]=static_cast<T>(1)+ n<T>(3,4)*z[25];
    z[25]=z[8]*z[25];
    z[20]=n<T>(1,8)*z[20] + n<T>(1,4) + z[25];
    z[20]=z[7]*z[20];
    z[19]= - n<T>(5,4)*z[19] - static_cast<T>(1)+ n<T>(1,2)*z[9];
    z[19]=z[5]*z[19];
    z[19]= - n<T>(1,2)*z[8] + z[19];
    z[11]=z[11] + z[16] + z[17] + z[13] + n<T>(1,4)*z[19] + z[20];
    z[11]=z[11]*z[23];
    z[13]=n<T>(1,2) - 13*z[5];
    z[13]=z[13]*z[32];
    z[13]=z[13] + z[7];
    z[16]=n<T>(1,8) + z[5];
    z[16]=z[16]*z[28];
    z[17]=npow(z[1],2);
    z[19]=static_cast<T>(1)+ z[17];
    z[19]=z[19]*z[22];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[3]*z[19];
    z[19]=z[19] - z[24] + n<T>(3,4) - z[7];
    z[19]=z[19]*z[27];
    z[13]=z[19] + n<T>(1,2)*z[13] + z[16];
    z[15]=static_cast<T>(1)- z[15];
    z[15]=z[15]*z[28];
    z[16]=z[1] - 1;
    z[19]=z[16]*z[3];
    z[20]=3*z[5];
    z[23]=z[20] - 1;
    z[25]=z[5]*z[23];
    z[15]= - z[19] + z[25] + z[15];
    z[15]=z[1]*z[15];
    z[14]= - n<T>(3,2)*z[14] + z[15];
    z[14]=z[2]*z[14];
    z[15]=static_cast<T>(1)+ z[33];
    z[15]=z[15]*z[22];
    z[15]=z[15] + static_cast<T>(1)- n<T>(21,4)*z[5];
    z[15]=z[22]*z[15];
    z[15]=z[15] - n<T>(1,16) + z[5];
    z[15]=z[5]*z[15];
    z[19]= - z[1]*z[19];
    z[19]=z[19] - static_cast<T>(3)- z[17];
    z[19]=z[3]*z[19];
    z[14]=n<T>(1,4)*z[14] + n<T>(1,16)*z[19] + z[15];
    z[14]=z[2]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[13]=z[2]*z[13];
    z[14]=static_cast<T>(1)+ z[1];
    z[14]=z[14]*z[22];
    z[15]= - z[1] + static_cast<T>(3)- z[6];
    z[15]=z[1]*z[15];
    z[15]=z[6] + z[15];
    z[15]=z[15]*z[22];
    z[15]=z[10] + z[15];
    z[15]=z[3]*z[15];
    z[19]=z[10] - n<T>(7,4);
    z[22]=z[19] + z[7];
    z[22]=z[10]*z[22];
    z[14]=z[15] + z[14] + z[22];
    z[14]=z[14]*z[21];
    z[15]=static_cast<T>(1)+ z[18];
    z[15]=z[7]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[15]*z[24];
    z[18]=z[7]*z[6];
    z[15]=z[15] - n<T>(1,4)*z[18] - n<T>(1,4) - z[8];
    z[15]=z[24]*z[15];
    z[12]= - z[8]*z[12];
    z[12]=z[15] + n<T>(1,4) + z[12];
    z[12]=z[7]*z[12];
    z[15]=n<T>(1,2) + z[20];
    z[15]=z[15]*z[30];
    z[18]= - z[10]*z[19];
    z[18]= - n<T>(7,4) + z[18];
    z[18]=z[8]*z[18];
    z[15]=z[18] + z[15];
    z[12]=z[14] + n<T>(1,2)*z[15] + z[12];
    z[11]=z[11] + n<T>(1,4)*z[12] + z[13];
    z[11]=z[4]*z[11];
    z[12]=z[16]*z[17];
    z[13]=z[24] - 1;
    z[13]=z[13]*z[1];
    z[14]=n<T>(1,2) - z[13];
    z[14]=z[1]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[3]*z[14]*z[24];
    z[12]=z[14] + static_cast<T>(1)+ n<T>(1,4)*z[12];
    z[12]=z[3]*z[12];
    z[14]=n<T>(1,2) - z[5];
    z[14]=z[14]*z[29];
    z[15]= - n<T>(9,2) + z[33];
    z[15]=z[15]*z[30];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[1]*z[15];
    z[12]=z[12] + z[15] - static_cast<T>(1)+ z[14];
    z[13]=z[13] + n<T>(1,2);
    z[13]=z[13]*z[21]*z[17];
    z[14]=static_cast<T>(5)- z[20];
    z[14]=z[14]*z[30];
    z[15]= - static_cast<T>(1)+ z[30];
    z[15]=z[15]*z[28];
    z[14]=z[14] + z[15];
    z[14]=z[14]*z[24];
    z[15]= - static_cast<T>(1)+ n<T>(3,4)*z[5];
    z[15]=z[5]*z[15];
    z[14]=z[15] + z[14];
    z[14]=z[1]*z[14];
    z[15]=static_cast<T>(1)- z[5];
    z[15]=z[15]*z[32];
    z[14]=z[13] + z[15] + z[14];
    z[14]=z[2]*z[14];
    z[15]= - static_cast<T>(1)+ z[17];
    z[15]=z[1]*z[15];
    z[13]=z[15] + z[13];
    z[13]=z[13]*z[27];
    z[15]=static_cast<T>(3)- n<T>(11,4)*z[5];
    z[15]=z[5]*z[15];
    z[15]= - static_cast<T>(1)+ z[15];
    z[16]= - static_cast<T>(1)+ n<T>(11,16)*z[5];
    z[16]=z[5]*z[16];
    z[16]=n<T>(1,4) + z[16];
    z[16]=z[1]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[1]*z[15];
    z[16]= - static_cast<T>(1)+ n<T>(11,8)*z[5];
    z[16]=z[5]*z[16];
    z[16]=n<T>(1,2) + z[16];
    z[13]=z[14] + z[13] + n<T>(1,2)*z[16] + z[15];
    z[13]=z[2]*z[13];
    z[12]=n<T>(1,4)*z[12] + z[13];
    z[12]=z[2]*z[12];
    z[13]=z[10] - 1;
    z[14]=z[6]*z[10];
    z[15]= - z[13]*z[14];
    z[16]=static_cast<T>(1)- z[6];
    z[16]=n<T>(1,2)*z[16] - z[1];
    z[16]=z[16]*z[17];
    z[16]=z[16] + static_cast<T>(1)- z[14];
    z[16]=z[3]*z[16];
    z[15]=z[16] + n<T>(1,2)*z[17] + z[15] - static_cast<T>(1)- z[10];
    z[15]=z[3]*z[15];
    z[14]=static_cast<T>(1)+ z[14];
    z[13]=z[13]*z[8]*z[14];
    z[14]=z[23]*z[30];
    z[13]=z[15] + z[14] + z[13];
    z[12]=n<T>(1,8)*z[13] + z[12];

    r += z[11] + n<T>(1,2)*z[12];
 
    return r;
}

template double qg_2lha_r1000(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1000(const std::array<dd_real,30>&);
#endif
