#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r349(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[9];
    z[4]=k[12];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=n<T>(1,2)*z[2];
    z[8]=npow(z[3],2);
    z[9]=z[8]*z[7];
    z[10]=n<T>(1,2)*z[3];
    z[11]=static_cast<T>(1)+ z[10];
    z[11]=z[3]*z[11];
    z[9]=z[11] + z[9];
    z[9]=z[1]*z[9];
    z[11]=z[2]*z[3];
    z[9]=z[11] + z[9];
    z[9]=z[1]*z[9];
    z[12]=npow(z[2],2);
    z[13]=z[2] + n<T>(1,2)*z[1];
    z[13]=z[1]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[13]=z[2] + 1;
    z[12]=z[5]*z[13]*z[12];
    z[14]=z[1] + z[2];
    z[15]= - z[1]*z[14];
    z[12]=z[15] + z[12];
    z[12]=z[5]*z[12];
    z[9]=z[9] + z[12];
    z[9]=z[4]*z[9];
    z[12]= - static_cast<T>(1)- n<T>(5,8)*z[3];
    z[12]=z[3]*z[12];
    z[8]=z[2]*z[8];
    z[8]=z[12] - n<T>(3,8)*z[8];
    z[8]=z[1]*z[8];
    z[12]=static_cast<T>(3)+ 5*z[2];
    z[12]= - z[5]*z[12]*z[14];
    z[14]= - static_cast<T>(1)+ z[3] + n<T>(3,2)*z[2];
    z[12]=n<T>(1,8)*z[12] + n<T>(1,4)*z[14] + z[1];
    z[12]=z[5]*z[12];
    z[11]=z[3] - n<T>(3,2)*z[11];
    z[8]=z[9] + z[12] + n<T>(1,4)*z[11] + z[8];
    z[8]=z[4]*z[8];
    z[9]= - static_cast<T>(1)+ z[10];
    z[9]=z[3]*z[9];
    z[7]= - z[3] - z[7];
    z[7]=z[5]*z[7];
    z[7]=n<T>(1,2)*z[7] + n<T>(1,12) + z[9];
    z[7]=z[5]*z[7];
    z[9]= - n<T>(1,3) + 3*z[3];
    z[9]=z[3]*z[9];
    z[7]=n<T>(1,4)*z[9] + z[7];
    z[7]=n<T>(1,2)*z[7] + z[8];
    z[7]=z[4]*z[7];
    z[8]=npow(z[5],2)*z[6]*z[13];

    r += z[7] + n<T>(1,6)*z[8];
 
    return r;
}

template double qg_2lha_r349(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r349(const std::array<dd_real,30>&);
#endif
