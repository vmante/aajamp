#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r473(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[4];
    z[4]=k[12];
    z[5]=k[29];
    z[6]=k[7];
    z[7]=k[2];
    z[8]=k[5];
    z[9]=k[10];
    z[10]=k[11];
    z[11]=k[9];
    z[12]=n<T>(1,2)*z[2];
    z[13]=z[12]*z[9];
    z[14]=3*z[2];
    z[15]=z[10] - n<T>(5,2);
    z[15]=z[15]*z[10];
    z[16]= - static_cast<T>(1)- z[15];
    z[16]=z[13] + n<T>(1,2)*z[16] + z[14];
    z[16]=z[9]*z[16];
    z[17]=13*z[11] - static_cast<T>(25)- z[8];
    z[18]=z[10] + n<T>(1,2);
    z[19]=z[10]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[17]=z[2]*z[17];
    z[19]=z[1] - 1;
    z[19]=z[19]*z[6];
    z[20]=n<T>(1,4)*z[8];
    z[21]=5*z[1];
    z[22]=n<T>(1,2)*z[10];
    z[23]=z[22] + n<T>(15,4) + z[6];
    z[23]=z[10]*z[23];
    z[16]=z[16] + z[17] + z[23] + z[19] - n<T>(7,2)*z[11] + z[21] + static_cast<T>(7)+ 
    z[20];
    z[17]=n<T>(1,2)*z[1];
    z[23]=z[17] - 1;
    z[24]=n<T>(1,2)*z[6];
    z[25]=z[23]*z[24];
    z[18]=z[6] + z[18];
    z[18]=z[18]*z[22];
    z[26]=n<T>(1,2)*z[9];
    z[27]=n<T>(1,2) - z[14];
    z[27]=z[27]*z[26];
    z[15]=z[27] - n<T>(11,2)*z[2] + static_cast<T>(5)- n<T>(1,4)*z[15];
    z[15]=z[9]*z[15];
    z[27]=z[2]*z[11];
    z[28]=static_cast<T>(1)+ n<T>(3,2)*z[9];
    z[28]=z[9]*z[28];
    z[28]=n<T>(1,8)*z[6] + z[28];
    z[28]=z[3]*z[28];
    z[15]=z[28] + z[15] - n<T>(1,4)*z[27] + z[18] + z[25] + static_cast<T>(1)+ n<T>(3,4)*z[11];
    z[15]=z[3]*z[15];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[5]*z[15];
    z[16]=z[10]*z[6];
    z[18]=z[16] - z[24];
    z[18]=z[18]*z[10];
    z[25]= - z[8] - z[6];
    z[25]=z[25]*z[26];
    z[25]= - z[18] + z[25];
    z[25]=z[9]*z[25];
    z[28]=n<T>(1,2)*z[3];
    z[28]=z[28]*npow(z[9],2);
    z[29]=npow(z[6],2);
    z[30]= - z[29]*z[28];
    z[25]=z[25] + z[30];
    z[15]=n<T>(1,4)*z[25] + z[15];
    z[25]=n<T>(11,2) + z[11];
    z[25]=z[25]*z[27];
    z[30]=3*z[11];
    z[31]=z[11] + 1;
    z[32]=z[2]*z[31];
    z[32]=z[32] - static_cast<T>(5)- z[11];
    z[32]=z[9]*z[32];
    z[25]=z[32] + z[25] - n<T>(13,2) - z[30];
    z[25]=z[9]*z[25];
    z[32]=z[3]*z[6];
    z[32]=static_cast<T>(3)+ z[32];
    z[28]=z[32]*z[28];
    z[32]=npow(z[11],2);
    z[33]=z[2]*z[32];
    z[16]=z[28] + z[25] + z[33] + n<T>(3,2)*z[11] - z[16];
    z[25]=static_cast<T>(61)- 35*z[2];
    z[25]=z[25]*z[12];
    z[25]= - static_cast<T>(1)+ z[25];
    z[28]=n<T>(1,4)*z[2];
    z[25]=z[25]*z[28];
    z[33]=z[2] - 3;
    z[34]= - z[33]*z[12];
    z[34]=static_cast<T>(3)+ z[34];
    z[35]=z[9]*z[2];
    z[34]=z[34]*z[35];
    z[25]=z[25] + z[34];
    z[25]=z[9]*z[25];
    z[34]= - static_cast<T>(73)- 15*z[2];
    z[36]=static_cast<T>(5)+ z[14];
    z[36]=z[9]*z[36];
    z[34]=n<T>(1,4)*z[34] + z[36];
    z[34]=z[9]*z[34];
    z[34]= - n<T>(25,2) + z[34];
    z[36]=z[3]*z[9];
    z[37]= - n<T>(25,4) + z[9];
    z[37]=z[37]*z[36];
    z[34]=n<T>(1,2)*z[34] + z[37];
    z[34]=z[3]*z[34];
    z[37]= - static_cast<T>(19)- z[2];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[9]*z[14];
    z[14]=n<T>(1,4)*z[37] + 3*z[14];
    z[14]=z[9]*z[14];
    z[37]=static_cast<T>(1)+ z[1];
    z[37]=z[6]*z[37];
    z[37]=n<T>(35,2)*z[2] + z[37] - n<T>(81,2) - 25*z[1];
    z[14]=n<T>(1,2)*z[37] + z[14];
    z[14]=n<T>(1,2)*z[14] + z[34];
    z[14]=z[3]*z[14];
    z[34]=n<T>(15,4)*z[27] - static_cast<T>(5)- n<T>(11,4)*z[11];
    z[34]=z[34]*z[12];
    z[37]=n<T>(7,2) + z[1];
    z[34]=5*z[37] + z[34];
    z[34]=z[2]*z[34];
    z[37]=n<T>(15,2)*z[1];
    z[14]=z[14] + z[25] + z[34] - static_cast<T>(7)- z[37];
    z[14]=z[3]*z[14];
    z[25]=npow(z[2],2);
    z[34]=11*z[2];
    z[38]=static_cast<T>(17)- z[34];
    z[38]=z[38]*z[25];
    z[25]=z[25]*z[9];
    z[39]= - z[33]*z[25];
    z[38]=n<T>(1,4)*z[38] + z[39];
    z[38]=z[38]*z[26];
    z[39]=static_cast<T>(15)+ z[22];
    z[40]=n<T>(31,8)*z[27] - static_cast<T>(5)- n<T>(27,8)*z[11];
    z[40]=z[2]*z[40];
    z[39]=n<T>(1,2)*z[39] + z[40];
    z[39]=z[2]*z[39];
    z[14]=z[14] + z[39] + z[38];
    z[14]=z[5]*z[14];
    z[38]=static_cast<T>(3)+ z[1];
    z[39]=static_cast<T>(9)+ 13*z[2];
    z[39]=z[9]*z[39];
    z[38]=z[39] + 3*z[38] + z[2];
    z[39]=n<T>(9,4) + z[2];
    z[39]=z[9]*z[39];
    z[39]=n<T>(3,4)*z[36] + n<T>(3,4) + z[39];
    z[39]=z[3]*z[39];
    z[38]=n<T>(1,4)*z[38] + z[39];
    z[38]=z[3]*z[38];
    z[23]= - z[28] - z[23];
    z[23]=z[2]*z[23];
    z[39]=n<T>(3,4)*z[9];
    z[40]=static_cast<T>(1)+ 5*z[2];
    z[40]=z[40]*z[39];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[38] + z[40] + n<T>(3,2)*z[17] + z[23];
    z[17]=z[3]*z[17];
    z[23]=z[27] - z[11];
    z[28]= - z[23]*z[28];
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=z[2]*z[28];
    z[28]=n<T>(3,2) + z[28];
    z[28]=z[2]*z[28];
    z[33]=z[33]*z[2];
    z[33]=z[33] + 3;
    z[38]=z[33]*z[12];
    z[38]=static_cast<T>(3)+ z[38];
    z[13]=z[38]*z[13];
    z[13]=z[17] + z[28] + z[13];
    z[13]=z[3]*z[13];
    z[17]=z[33]*z[25];
    z[23]= - z[23]*npow(z[2],3);
    z[17]=z[23] + z[17];
    z[13]=n<T>(1,4)*z[17] + z[13];
    z[13]=z[5]*z[13];
    z[17]=n<T>(1,2)*z[11];
    z[23]= - n<T>(1,2)*z[27] + static_cast<T>(1)+ z[17];
    z[23]=z[2]*z[23];
    z[23]= - n<T>(3,2) + z[23];
    z[23]=z[9]*z[23];
    z[23]=z[23] - n<T>(3,2)*z[36];
    z[13]=n<T>(1,2)*z[23] + z[13];
    z[13]=z[4]*z[13];
    z[13]=5*z[13] + n<T>(1,4)*z[16] + z[14];
    z[13]=z[4]*z[13];
    z[14]= - static_cast<T>(1)+ z[12];
    z[14]=z[14]*z[35];
    z[16]= - n<T>(49,2) - z[10];
    z[23]= - n<T>(53,4) + z[34];
    z[23]=z[2]*z[23];
    z[14]=n<T>(9,2)*z[14] + n<T>(1,4)*z[16] + z[23];
    z[14]=z[9]*z[14];
    z[16]=n<T>(1,2)*z[8];
    z[23]= - static_cast<T>(25)+ z[16];
    z[23]=n<T>(1,2)*z[23] + z[21];
    z[17]= - z[27] - static_cast<T>(5)- z[17];
    z[17]=z[2]*z[17];
    z[28]= - z[6]*z[1];
    z[14]=z[14] + z[17] - n<T>(3,2)*z[10] + n<T>(1,2)*z[23] + z[28];
    z[17]=static_cast<T>(25)- z[19];
    z[17]=n<T>(1,2)*z[17] - z[27];
    z[19]= - n<T>(7,2) - z[2];
    z[19]=z[19]*z[39];
    z[23]= - static_cast<T>(1)- n<T>(1,16)*z[2];
    z[19]=5*z[23] + z[19];
    z[19]=z[9]*z[19];
    z[23]=n<T>(9,2) - 5*z[9];
    z[23]=z[23]*z[36];
    z[17]=n<T>(3,8)*z[23] + n<T>(1,8)*z[17] + z[19];
    z[17]=z[3]*z[17];
    z[14]=n<T>(1,2)*z[14] + z[17];
    z[14]=z[3]*z[14];
    z[17]= - z[21] - static_cast<T>(5)+ n<T>(1,8)*z[8];
    z[19]= - n<T>(7,2) - z[10];
    z[19]=z[10]*z[19];
    z[21]=static_cast<T>(5)- n<T>(11,2)*z[11];
    z[21]=z[2]*z[21];
    z[17]=n<T>(3,4)*z[21] + n<T>(1,8)*z[19] + n<T>(1,2)*z[17] + z[30];
    z[17]=z[2]*z[17];
    z[19]=z[10] - 1;
    z[21]=n<T>(45,2) - 7*z[2];
    z[21]=z[2]*z[21];
    z[21]=3*z[25] + z[21] - z[19];
    z[21]=z[9]*z[21];
    z[23]= - n<T>(5,4)*z[10] + static_cast<T>(1)+ z[37];
    z[14]=z[14] + n<T>(1,8)*z[21] + n<T>(1,2)*z[23] + z[17];
    z[14]=z[5]*z[14];
    z[17]= - z[24]*z[19];
    z[19]=z[31]*z[11];
    z[21]= - static_cast<T>(1)- z[19];
    z[21]=z[21]*z[26];
    z[17]=z[21] + z[20] - z[19] + z[17];
    z[17]=z[9]*z[17];
    z[18]= - z[32] + z[18];
    z[19]=z[29]*npow(z[36],2);
    z[17]=n<T>(1,4)*z[19] + n<T>(1,2)*z[18] + z[17];
    z[13]=z[13] + n<T>(1,4)*z[17] + z[14];
    z[13]=z[4]*z[13];
    z[13]=n<T>(1,2)*z[15] + z[13];
    z[13]=z[4]*z[13];
    z[14]= - static_cast<T>(5)+ z[7];
    z[15]= - static_cast<T>(1)- n<T>(3,2)*z[7];
    z[15]=z[6]*z[15];
    z[17]= - z[6] - 1;
    z[17]=z[7]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[10]*z[17];
    z[14]=z[17] + n<T>(1,2)*z[14] + z[15];
    z[14]=z[14]*z[22];
    z[15]= - static_cast<T>(1)+ n<T>(1,2)*z[7];
    z[17]= - z[15]*z[24];
    z[18]= - z[7]*z[20];
    z[17]=z[26] + z[17] + static_cast<T>(1)+ z[18];
    z[17]=z[9]*z[17];
    z[15]=z[6]*z[15];
    z[18]= - z[6]*z[7];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[10]*z[18];
    z[15]=z[18] + n<T>(1,2) + z[15];
    z[15]=z[10]*z[15];
    z[15]= - z[24] + z[15];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[3]*z[15];
    z[17]=z[10] - n<T>(1,2);
    z[18]= - z[10]*z[17];
    z[16]=z[16] + z[18];
    z[12]=z[16]*z[12];
    z[16]=z[17]*z[22];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[9]*z[16];
    z[17]= - z[7] - 1;
    z[17]=z[8]*z[17];
    z[17]=static_cast<T>(7)+ z[17];
    z[17]=n<T>(1,2)*z[17] - z[11];
    z[18]=z[1] + z[7];
    z[18]=static_cast<T>(1)- n<T>(1,4)*z[18];
    z[18]=z[6]*z[18];
    z[12]=z[15] + z[16] + z[12] + z[14] + n<T>(1,2)*z[17] + z[18];
    z[12]=z[5]*z[12];
    z[12]=n<T>(1,4)*z[12] + z[13];

    r += n<T>(1,2)*z[12];
 
    return r;
}

template double qg_2lha_r473(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r473(const std::array<dd_real,30>&);
#endif
