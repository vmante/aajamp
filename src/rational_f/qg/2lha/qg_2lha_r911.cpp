#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r911(const std::array<T,30>& k) {
  T z[44];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[11];
    z[5]=k[24];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[10];
    z[9]=k[3];
    z[10]=k[20];
    z[11]=z[5] + 1;
    z[12]=3*z[5];
    z[13]=z[11]*z[12];
    z[14]=n<T>(1,2)*z[7];
    z[15]=static_cast<T>(5)- z[4];
    z[15]=z[15]*z[14];
    z[16]= - static_cast<T>(1)+ z[4];
    z[16]=z[4]*z[16];
    z[13]=z[15] + n<T>(9,4)*z[16] + n<T>(5,6) + z[13];
    z[13]=z[13]*z[14];
    z[15]=n<T>(1,4)*z[4];
    z[16]=n<T>(3,2)*z[4];
    z[17]= - static_cast<T>(1)- z[16];
    z[17]=z[17]*z[15];
    z[18]=n<T>(1,2)*z[5];
    z[19]=static_cast<T>(1)- z[18];
    z[19]=z[5]*z[19];
    z[17]=z[17] - n<T>(5,12) + z[19];
    z[17]=z[7]*z[17];
    z[19]=npow(z[4],2);
    z[20]=npow(z[5],2);
    z[17]=z[17] - 3*z[20] - z[19];
    z[17]=z[7]*z[17];
    z[21]=npow(z[3],2);
    z[17]= - n<T>(1,3)*z[21] + z[17];
    z[17]=z[6]*z[17];
    z[22]=n<T>(5,2) + 11*z[3];
    z[23]=n<T>(1,6)*z[3];
    z[22]=z[22]*z[23];
    z[24]=npow(z[7],2);
    z[25]=n<T>(1,4)*z[24];
    z[26]=z[21] + z[25];
    z[27]=n<T>(1,3)*z[1];
    z[26]=z[26]*z[27];
    z[13]=z[17] + z[26] + z[13] - z[22] + 2*z[20] + z[19];
    z[13]=z[6]*z[13];
    z[17]=z[24]*z[27];
    z[26]=n<T>(3,2)*z[3];
    z[28]= - n<T>(1,3) + z[26];
    z[28]=z[3]*z[28];
    z[29]=n<T>(1,4)*z[7];
    z[30]=n<T>(1,3) + z[29];
    z[30]=z[7]*z[30];
    z[17]=z[17] + z[30] + z[28] + z[20] + z[19];
    z[17]=z[1]*z[17];
    z[28]=n<T>(1,2) - z[4];
    z[28]=z[28]*z[16];
    z[28]= - z[5] + z[28];
    z[30]=n<T>(1,3)*z[3];
    z[31]=n<T>(5,4) - 8*z[3];
    z[31]=z[31]*z[30];
    z[32]= - z[29] - n<T>(7,4) + z[4];
    z[32]=z[32]*z[14];
    z[13]=z[13] + z[17] + z[32] + n<T>(1,2)*z[28] + z[31];
    z[13]=z[6]*z[13];
    z[17]=z[20] + n<T>(1,4)*z[19];
    z[28]=n<T>(1,3)*z[7];
    z[31]=n<T>(5,4) + 2*z[7];
    z[31]=z[31]*z[28];
    z[32]=z[24] + z[21];
    z[33]=z[1]*z[32];
    z[22]=n<T>(1,12)*z[33] + z[31] - z[22] + z[17];
    z[22]=z[1]*z[22];
    z[31]=n<T>(1,2)*z[4];
    z[33]= - n<T>(5,2) + z[4];
    z[33]=z[33]*z[31];
    z[34]=2*z[5];
    z[35]=static_cast<T>(5)+ z[34];
    z[35]=z[5]*z[35];
    z[36]=z[15] - z[5];
    z[37]=n<T>(5,6) - z[36];
    z[37]=z[7]*z[37];
    z[33]=z[37] + z[33] - n<T>(3,4) + z[35];
    z[33]=z[7]*z[33];
    z[35]=n<T>(1,6) - z[17];
    z[35]=z[35]*z[14];
    z[35]=z[35] - z[17];
    z[35]=z[7]*z[35];
    z[35]=n<T>(1,12)*z[21] + z[35];
    z[35]=z[6]*z[35];
    z[37]=n<T>(1,6)*z[1];
    z[32]= - z[32]*z[37];
    z[38]=n<T>(3,4) + z[3];
    z[38]=z[3]*z[38];
    z[32]=z[35] + z[32] + z[33] + z[38] + z[17];
    z[32]=z[6]*z[32];
    z[33]=n<T>(5,3)*z[3];
    z[35]=static_cast<T>(1)+ n<T>(7,4)*z[3];
    z[35]=z[35]*z[33];
    z[38]=n<T>(3,4)*z[4];
    z[12]=n<T>(1,8)*z[7] + z[38] + n<T>(4,3) - z[12];
    z[12]=z[7]*z[12];
    z[39]= - static_cast<T>(4)- n<T>(3,2)*z[5];
    z[39]=z[5]*z[39];
    z[40]=static_cast<T>(1)- n<T>(3,8)*z[4];
    z[40]=z[4]*z[40];
    z[12]=z[32] + z[22] + z[12] + z[35] + z[39] + z[40];
    z[12]=z[6]*z[12];
    z[22]=n<T>(5,2)*z[3];
    z[32]= - static_cast<T>(1)+ z[22];
    z[32]=z[32]*z[30];
    z[32]=z[28] + z[32] + z[17];
    z[32]=z[1]*z[32];
    z[35]=static_cast<T>(3)- z[4];
    z[35]=z[35]*z[15];
    z[39]= - static_cast<T>(3)- z[5];
    z[39]=z[5]*z[39];
    z[40]=n<T>(19,12) - 4*z[3];
    z[40]=z[3]*z[40];
    z[32]=z[32] - n<T>(1,12)*z[7] + z[40] + z[39] + z[35];
    z[32]=z[1]*z[32];
    z[35]=n<T>(1,2)*z[3];
    z[39]=n<T>(1,2) + n<T>(19,3)*z[3];
    z[39]=z[39]*z[35];
    z[12]=z[12] + z[32] + z[39] - z[36] - z[29];
    z[12]=z[6]*z[12];
    z[32]=z[3] + 1;
    z[39]= - z[32]*z[23];
    z[34]=z[34] - z[31];
    z[40]=z[36] + n<T>(1,6);
    z[41]= - z[7]*z[40];
    z[41]=z[41] + n<T>(1,6) + z[34];
    z[41]=z[7]*z[41];
    z[39]=z[39] + z[41];
    z[39]=z[6]*z[39];
    z[14]=z[14] - 1;
    z[41]=z[14]*z[28];
    z[42]=n<T>(1,3) + z[35];
    z[42]=z[3]*z[42];
    z[41]=z[42] + z[41];
    z[41]=z[1]*z[41];
    z[42]= - n<T>(13,12) - z[3];
    z[42]=z[3]*z[42];
    z[43]=z[28] + z[4] + n<T>(31,12) - 4*z[5];
    z[43]=z[7]*z[43];
    z[39]=z[39] + z[41] + z[43] + z[42] - z[34];
    z[39]=z[6]*z[39];
    z[41]= - n<T>(1,3) - z[3];
    z[41]=z[3]*z[41];
    z[28]=z[41] + z[28];
    z[41]=n<T>(1,2)*z[1];
    z[28]=z[28]*z[41];
    z[22]=n<T>(2,3) + z[22];
    z[22]=z[3]*z[22];
    z[22]=z[28] + n<T>(5,6)*z[7] + z[22] - z[34];
    z[22]=z[1]*z[22];
    z[28]=2*z[3];
    z[42]= - n<T>(5,4) - z[28];
    z[42]=z[3]*z[42];
    z[22]=z[39] + z[22] - z[7] - 3*z[36] + z[42];
    z[22]=z[6]*z[22];
    z[37]=z[21]*z[37];
    z[28]=n<T>(19,12) - z[28];
    z[28]=z[3]*z[28];
    z[28]=z[37] + z[28] - z[34];
    z[28]=z[1]*z[28];
    z[37]=n<T>(7,2)*z[3];
    z[39]= - n<T>(5,3) + z[37];
    z[39]=z[3]*z[39];
    z[28]=z[28] + z[39] + z[34];
    z[28]=z[1]*z[28];
    z[39]=n<T>(1,4) - 5*z[3];
    z[39]=z[39]*z[30];
    z[22]=z[22] + z[39] + z[28];
    z[22]=z[6]*z[22];
    z[28]= - z[1] + 1;
    z[32]=z[32]*z[3];
    z[32]=z[32] - z[7];
    z[28]=z[32]*z[28];
    z[32]=static_cast<T>(1)+ z[35];
    z[32]=z[3]*z[32];
    z[14]=z[7]*z[14];
    z[14]=z[32] + z[14];
    z[14]=z[6]*z[14];
    z[14]=n<T>(1,2)*z[14] + z[28];
    z[14]=z[6]*z[14];
    z[28]= - static_cast<T>(1)+ z[41];
    z[28]=z[1]*z[28];
    z[28]=n<T>(1,2) + z[28];
    z[28]=z[21]*z[28];
    z[14]=n<T>(1,3)*z[14] + z[28];
    z[14]=z[6]*z[14];
    z[28]=z[3] - 1;
    z[32]=z[28]*z[30];
    z[39]= - z[1]*z[32];
    z[28]=z[28]*z[3];
    z[39]=z[28] + z[39];
    z[39]=z[1]*z[39];
    z[39]= - z[28] + z[39];
    z[39]=z[1]*z[39];
    z[14]=z[14] + z[32] + z[39];
    z[14]=z[6]*z[14];
    z[32]=z[35] - 1;
    z[32]=z[32]*z[3];
    z[32]=z[32] + n<T>(1,2);
    z[39]=z[32]*z[41];
    z[42]=z[3] - 2;
    z[42]=z[42]*z[3];
    z[42]=z[42] + 1;
    z[39]=z[39] - z[42];
    z[27]=z[39]*z[27];
    z[27]=z[27] + z[32];
    z[27]=z[1]*z[27];
    z[27]= - n<T>(1,3)*z[42] + z[27];
    z[27]=z[1]*z[27];
    z[14]=z[14] + n<T>(1,6)*z[32] + z[27];
    z[14]=z[2]*z[14];
    z[27]= - n<T>(7,3) + z[3];
    z[27]=z[27]*z[35];
    z[32]= - z[1]*z[36];
    z[27]=z[32] + z[27] - n<T>(1,12) - z[34];
    z[27]=z[1]*z[27];
    z[32]=n<T>(11,2) - 3*z[3];
    z[32]=z[32]*z[35];
    z[27]=z[27] + z[32] + n<T>(1,4) - z[36];
    z[27]=z[1]*z[27];
    z[26]= - static_cast<T>(2)+ z[26];
    z[26]=z[3]*z[26];
    z[26]=z[27] - n<T>(1,4) + z[26];
    z[26]=z[1]*z[26];
    z[27]=n<T>(5,6) - z[3];
    z[27]=z[3]*z[27];
    z[27]=n<T>(1,6) + z[27];
    z[14]=z[14] + z[22] + n<T>(1,2)*z[27] + z[26];
    z[14]=z[2]*z[14];
    z[22]=z[31] - 1;
    z[26]=z[22]*z[31];
    z[17]= - z[17]*z[41];
    z[27]=static_cast<T>(2)+ z[5];
    z[27]=z[5]*z[27];
    z[17]=z[17] + z[27] + z[26];
    z[17]=z[1]*z[17];
    z[26]=static_cast<T>(1)- z[15];
    z[26]=z[26]*z[31];
    z[27]=7*z[3];
    z[32]= - static_cast<T>(13)+ z[27];
    z[23]=z[32]*z[23];
    z[18]= - static_cast<T>(2)- z[18];
    z[18]=z[5]*z[18];
    z[17]=z[17] + z[23] + z[26] - n<T>(1,4) + z[18];
    z[17]=z[1]*z[17];
    z[18]=n<T>(17,2) - z[27];
    z[18]=z[18]*z[30];
    z[17]=z[17] + n<T>(1,2) + z[18];
    z[17]=z[1]*z[17];
    z[18]= - static_cast<T>(2)+ z[37];
    z[18]=z[18]*z[30];
    z[12]=z[14] + z[12] + z[17] - n<T>(1,4) + z[18];
    z[12]=z[2]*z[12];
    z[14]= - static_cast<T>(1)- z[4];
    z[14]=z[14]*z[38];
    z[17]= - n<T>(19,2) + z[27];
    z[17]=z[17]*z[30];
    z[11]=z[5]*z[11];
    z[11]=z[17] + z[14] + z[11] - z[29] - n<T>(1,6);
    z[14]= - z[20] - n<T>(5,4)*z[19];
    z[14]=z[14]*z[41];
    z[14]=z[14] + z[5] + z[19];
    z[14]=z[1]*z[14];
    z[11]=n<T>(1,2)*z[11] + z[14];
    z[11]=z[1]*z[11];
    z[14]=static_cast<T>(5)- z[27];
    z[14]=z[14]*z[30];
    z[14]=z[29] + z[14] + z[40];
    z[11]=z[12] + z[13] + n<T>(1,2)*z[14] + z[11];
    z[11]=z[2]*z[11];
    z[12]=z[5]*z[9];
    z[13]=static_cast<T>(1)+ z[9];
    z[13]=z[13]*z[12];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[5]*z[13];
    z[14]=n<T>(1,2)*z[8];
    z[17]=z[14] - 1;
    z[18]=z[4]*z[10];
    z[23]=z[10]*z[8];
    z[26]= - npow(z[9],3)*z[20];
    z[26]=z[26] + z[9] - n<T>(1,4)*z[8];
    z[26]=z[7]*z[26];
    z[13]=z[26] - z[18] + z[13] - n<T>(3,2)*z[17] + z[23];
    z[13]=z[7]*z[13];
    z[26]=3*z[10];
    z[27]=n<T>(3,2)*z[8] - z[23];
    z[27]=z[27]*z[26];
    z[27]= - z[8] + z[27];
    z[30]= - n<T>(3,2) + z[10];
    z[30]=z[10]*z[30];
    z[18]=z[30] + z[18];
    z[18]=z[18]*z[16];
    z[30]= - n<T>(1,2) + z[3];
    z[30]=z[30]*z[33];
    z[32]=static_cast<T>(1)+ z[14];
    z[32]=z[32]*z[24];
    z[25]= - z[1]*z[8]*z[25];
    z[25]=z[32] + z[25];
    z[25]=z[1]*z[25];
    z[13]=z[25] + z[13] + z[30] + z[18] + n<T>(1,2)*z[27] - z[20];
    z[18]=static_cast<T>(5)- z[26];
    z[18]=z[10]*z[18];
    z[25]= - z[4]*z[26];
    z[18]=z[18] + z[25];
    z[18]=z[18]*z[38];
    z[25]= - 5*z[8] + 3*z[23];
    z[25]=z[10]*z[25];
    z[18]=z[18] - z[20] + z[8] + n<T>(3,4)*z[25];
    z[25]=n<T>(1,2)*z[23];
    z[17]= - z[25] + z[17];
    z[27]= - static_cast<T>(1)- n<T>(1,2)*z[9];
    z[12]=z[27]*z[12];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[5]*z[12];
    z[15]=z[10]*z[15];
    z[12]=z[15] + n<T>(1,2)*z[17] + z[12];
    z[12]=z[7]*z[12];
    z[12]=n<T>(1,2)*z[18] + z[12];
    z[12]=z[7]*z[12];
    z[15]=z[8] - z[25];
    z[15]=z[15]*z[26];
    z[15]= - z[8] + z[15];
    z[17]=n<T>(1,2)*z[10] + z[22];
    z[17]=z[38]*z[10]*z[17];
    z[15]=z[17] + n<T>(1,4)*z[15] - z[20];
    z[15]=z[6]*z[15]*z[24];
    z[12]=z[15] + n<T>(5,12)*z[21] + z[12];
    z[12]=z[6]*z[12];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[6]*z[12];
    z[13]=z[29]*z[8];
    z[15]= - z[1]*z[19];
    z[15]=z[15] - z[13] - z[20] + z[31];
    z[15]=z[1]*z[15];
    z[17]= - z[10] + 1;
    z[18]=n<T>(3,2)*z[9];
    z[17]=z[18]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[10]*z[17];
    z[19]= - z[10]*z[9];
    z[19]=static_cast<T>(1)+ z[19];
    z[16]=z[19]*z[16];
    z[16]=z[16] - n<T>(3,2) + z[17];
    z[16]=z[16]*z[31];
    z[17]=z[23]*z[18];
    z[18]= - static_cast<T>(1)- z[18];
    z[18]=z[8]*z[18];
    z[17]=z[18] + z[17];
    z[17]=z[10]*z[17];
    z[14]=z[14] + z[17];
    z[13]=z[15] + z[13] + n<T>(5,6)*z[28] + z[16] + n<T>(1,2)*z[14] + z[5];

    r += z[11] + z[12] + n<T>(1,2)*z[13];
 
    return r;
}

template double qg_2lha_r911(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r911(const std::array<dd_real,30>&);
#endif
