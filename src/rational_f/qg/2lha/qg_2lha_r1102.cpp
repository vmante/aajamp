#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1102(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[10];
    z[8]=k[9];
    z[9]=n<T>(1,4)*z[7];
    z[10]= - static_cast<T>(1)+ n<T>(3,2)*z[6];
    z[11]= - z[6]*z[10];
    z[11]=z[4] - static_cast<T>(1)+ z[11];
    z[11]=z[11]*z[9];
    z[12]=n<T>(1,2)*z[4];
    z[13]=z[12]*z[5];
    z[13]=z[13] - 1;
    z[13]=z[13]*z[4];
    z[14]= - static_cast<T>(3)+ z[6];
    z[11]=z[11] + n<T>(1,4)*z[14] - z[13];
    z[11]=z[7]*z[11];
    z[14]= - static_cast<T>(1)- 3*z[6];
    z[14]=z[14]*z[9];
    z[14]=z[14] + n<T>(1,2);
    z[14]=z[6]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[5]*z[14];
    z[15]= - n<T>(1,2) + z[5];
    z[15]=z[4]*z[15];
    z[10]=z[15] - n<T>(1,2)*z[10] + z[14];
    z[10]=z[7]*z[10];
    z[14]= - z[1] + z[5];
    z[14]=z[14]*z[12];
    z[15]=n<T>(1,2) + z[1];
    z[14]=z[14] + n<T>(1,2)*z[15] - z[5];
    z[14]=z[4]*z[14];
    z[15]= - z[1]*z[3];
    z[15]=3*z[5] - static_cast<T>(1)+ z[15];
    z[10]=n<T>(1,2)*z[10] + n<T>(1,8)*z[15] + z[14];
    z[10]=z[2]*z[10];
    z[12]=z[1]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[4]*z[12];
    z[10]=z[10] + z[11] + n<T>(3,8) + z[12];
    z[10]=z[2]*z[10];
    z[11]=z[8]*z[13];
    z[9]= - z[9]*z[4]*z[8];
    z[9]=z[11] + z[9];
    z[9]=z[7]*z[9];
    z[9]=z[9] + z[10];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r1102(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1102(const std::array<dd_real,30>&);
#endif
