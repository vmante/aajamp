#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1216(const std::array<T,30>& k) {
  T z[53];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[27];
    z[6]=k[6];
    z[7]=k[2];
    z[8]=k[11];
    z[9]=k[10];
    z[10]=k[14];
    z[11]=k[9];
    z[12]=k[3];
    z[13]=n<T>(1,3)*z[6];
    z[14]=n<T>(1,4)*z[6];
    z[15]=z[14] - 1;
    z[16]=z[13]*z[15];
    z[17]=n<T>(1,2)*z[2];
    z[18]=z[17] - 1;
    z[19]=z[18]*z[2];
    z[20]=n<T>(1,2) + n<T>(1,3)*z[19];
    z[16]=z[16] + n<T>(1,2)*z[20];
    z[16]=z[16]*z[3];
    z[20]=z[6] - 2;
    z[20]=z[20]*z[6];
    z[21]=npow(z[2],2);
    z[22]=z[21] - 1;
    z[23]= - z[20] + z[22];
    z[19]=z[19] + n<T>(3,2);
    z[15]=z[15]*z[6];
    z[15]=z[15] + n<T>(1,2)*z[19];
    z[24]=n<T>(27,4)*z[5];
    z[25]= - z[15]*z[24];
    z[23]=n<T>(31,4)*z[16] + n<T>(14,3)*z[23] + z[25];
    z[23]=z[4]*z[23];
    z[15]=z[15]*z[5];
    z[25]= - static_cast<T>(3)+ z[2];
    z[25]=z[2]*z[25];
    z[25]=static_cast<T>(11)+ z[25];
    z[26]= - n<T>(25,8) + z[6];
    z[26]=z[6]*z[26];
    z[25]= - n<T>(3,2)*z[15] + n<T>(1,8)*z[25] + z[26];
    z[25]=z[5]*z[25];
    z[26]= - n<T>(113,3) + 37*z[2];
    z[27]= - n<T>(563,6) + 75*z[6];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(1,2)*z[26] + z[27];
    z[28]= - n<T>(5,3) + n<T>(7,4)*z[2];
    z[28]=z[2]*z[28];
    z[28]= - n<T>(43,12) + z[28];
    z[29]=static_cast<T>(1)- n<T>(3,8)*z[6];
    z[29]=z[6]*z[29];
    z[28]=n<T>(1,2)*z[28] + 7*z[29];
    z[29]=n<T>(1,2)*z[3];
    z[28]=z[28]*z[29];
    z[20]=z[10]*z[20];
    z[30]=n<T>(1,4)*z[2];
    z[31]= - n<T>(4,3) - z[30];
    z[31]=z[2]*z[31];
    z[31]=n<T>(59,24) + z[31];
    z[31]=z[11]*z[31];
    z[20]=z[23] + z[28] + n<T>(9,2)*z[25] + z[31] + n<T>(1,8)*z[27] + z[20];
    z[20]=z[4]*z[20];
    z[23]=17*z[2];
    z[25]=static_cast<T>(1)+ z[23];
    z[25]=z[2]*z[25];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[11]*z[25];
    z[23]= - z[23] + z[25];
    z[23]=z[11]*z[23];
    z[25]=z[5]*z[11];
    z[27]=static_cast<T>(1)- 19*z[2];
    z[27]=z[2]*z[27];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[25];
    z[23]=z[23] + z[27];
    z[23]=z[5]*z[23];
    z[27]=npow(z[6],2);
    z[28]=z[27]*z[10];
    z[31]= - 3*z[27] + z[28];
    z[32]=3*z[10];
    z[31]=z[31]*z[32];
    z[33]= - n<T>(59,3) - z[17];
    z[33]=z[2]*z[33];
    z[33]= - n<T>(115,6) + z[33];
    z[33]=z[11]*z[33];
    z[26]= - n<T>(1,4)*z[26] + z[33];
    z[33]=n<T>(1,2)*z[11];
    z[26]=z[26]*z[33];
    z[26]=z[31] + z[26];
    z[31]=n<T>(2,3) + z[2];
    z[31]=z[2]*z[31];
    z[31]= - n<T>(1,3) + z[31];
    z[31]=z[11]*z[31];
    z[28]=n<T>(3,2)*z[28] + 7*z[31];
    z[28]=z[4]*z[28];
    z[26]=n<T>(1,2)*z[26] + z[28];
    z[26]=z[4]*z[26];
    z[28]=z[2] - 1;
    z[28]=z[28]*z[2];
    z[28]=z[28] + 1;
    z[31]=n<T>(1,2)*z[6];
    z[34]= - z[28]*z[31];
    z[35]=z[2] - n<T>(1,2);
    z[34]=z[34] + z[35];
    z[34]=z[6]*z[34];
    z[34]= - n<T>(1,2) + z[34];
    z[34]=z[10]*z[34];
    z[36]= - z[6]*z[18];
    z[36]=n<T>(1,2) + z[36];
    z[36]=z[6]*z[36];
    z[34]=z[36] + z[34];
    z[34]=z[34]*z[32];
    z[36]=z[17] + 1;
    z[37]=z[36]*z[2];
    z[37]=z[37] + n<T>(1,2);
    z[38]= - z[11]*z[37]*z[31];
    z[39]=z[2] + 1;
    z[40]=z[39]*z[6];
    z[38]= - z[40] + z[38];
    z[38]=z[11]*z[38];
    z[40]=z[4]*z[11];
    z[41]=static_cast<T>(2)+ z[2];
    z[41]=z[2]*z[41];
    z[41]=static_cast<T>(1)+ z[41];
    z[40]=z[12]*z[41]*npow(z[40],2);
    z[23]=n<T>(7,3)*z[40] + z[26] + n<T>(1,16)*z[23] + z[34] + n<T>(1,12)*z[38];
    z[23]=z[12]*z[23];
    z[26]=npow(z[2],3);
    z[34]=z[26]*z[6];
    z[38]=z[34]*z[39];
    z[40]=9*z[38];
    z[41]= - static_cast<T>(17)+ z[2];
    z[41]=z[2]*z[41];
    z[41]= - static_cast<T>(9)+ z[41];
    z[41]=z[41]*z[21];
    z[41]=z[41] + z[40];
    z[41]=z[6]*z[41];
    z[42]=static_cast<T>(9)- z[2];
    z[42]=z[42]*z[21];
    z[41]=z[42] + z[41];
    z[42]=n<T>(1,8)*z[5];
    z[41]=z[41]*z[42];
    z[43]=static_cast<T>(3)- n<T>(1,8)*z[2];
    z[43]=z[43]*z[21];
    z[34]=z[43] - n<T>(7,8)*z[34];
    z[34]=z[6]*z[34];
    z[43]=7*z[2];
    z[44]= - n<T>(19,2) - z[43];
    z[44]=z[2]*z[44];
    z[44]=n<T>(1,2) + z[44];
    z[34]=z[41] + n<T>(1,4)*z[44] + z[34];
    z[34]=z[5]*z[34];
    z[41]=5*z[2];
    z[44]= - static_cast<T>(41)+ z[41];
    z[44]=z[2]*z[44];
    z[44]=static_cast<T>(27)+ n<T>(1,3)*z[44];
    z[44]=z[44]*z[31];
    z[45]=n<T>(41,2) - z[41];
    z[44]=n<T>(1,3)*z[45] + z[44];
    z[44]=z[6]*z[44];
    z[44]=n<T>(5,6) + z[44];
    z[45]= - n<T>(121,8) + 5*z[4];
    z[27]=z[4]*z[27]*z[45];
    z[45]= - n<T>(1,8) - z[2];
    z[45]=z[2]*z[45];
    z[45]=n<T>(1,8) + z[45];
    z[45]=z[5]*z[45];
    z[45]=z[2] + z[45];
    z[45]=z[5]*z[45];
    z[27]=n<T>(1,3)*z[27] + n<T>(1,4)*z[44] + z[45];
    z[27]=z[12]*z[27];
    z[44]= - z[28]*z[14];
    z[45]= - static_cast<T>(7)+ n<T>(11,8)*z[2];
    z[45]=z[2]*z[45];
    z[45]=n<T>(35,4) + z[45];
    z[44]=n<T>(1,3)*z[45] + z[44];
    z[44]=z[6]*z[44];
    z[45]=n<T>(5,3)*z[2];
    z[46]=n<T>(17,2) + z[45];
    z[46]=z[46]*z[17];
    z[46]=n<T>(1,3) + z[46];
    z[29]=z[46]*z[29];
    z[46]= - n<T>(19,3) + z[31];
    z[46]=z[4]*z[46]*z[31];
    z[47]=static_cast<T>(31)+ z[2];
    z[27]=z[27] + z[46] + z[29] + z[34] + n<T>(1,12)*z[47] + z[44];
    z[29]=n<T>(1,2)*z[8];
    z[27]=z[27]*z[29];
    z[34]= - n<T>(7,2) + z[41];
    z[44]=static_cast<T>(1)+ n<T>(7,4)*z[10];
    z[46]=z[10]*z[44];
    z[47]=z[10] - z[28];
    z[47]=z[3]*z[47];
    z[34]=z[47] + n<T>(1,2)*z[34] + z[46];
    z[46]=n<T>(1,3)*z[3];
    z[34]=z[34]*z[46];
    z[47]=n<T>(1,3)*z[10];
    z[44]=z[44]*z[47];
    z[48]=z[11]*z[10];
    z[44]=z[44] + n<T>(1,4)*z[48];
    z[44]=z[11]*z[44];
    z[34]=z[44] + z[34];
    z[34]=z[9]*z[34];
    z[44]=z[47] - n<T>(1,2);
    z[49]=npow(z[10],2);
    z[50]= - z[44]*z[49];
    z[51]=n<T>(1,2)*z[10];
    z[52]=static_cast<T>(1)- z[47];
    z[52]=z[52]*z[51];
    z[18]=z[52] + n<T>(1,3)*z[18];
    z[18]=z[3]*z[18];
    z[18]=z[50] + z[18];
    z[18]=z[3]*z[18];
    z[44]= - z[44] - n<T>(1,6)*z[11];
    z[44]=z[11]*z[49]*z[44];
    z[18]=z[44] + z[18];
    z[18]=z[9]*z[18];
    z[44]= - z[3]*z[2];
    z[44]=n<T>(5,4) + z[44];
    z[44]=z[44]*z[46];
    z[50]=z[7]*npow(z[3],2);
    z[44]=n<T>(1,6)*z[50] + z[44];
    z[44]=z[8]*z[44];
    z[18]=z[18] + z[44];
    z[18]=z[7]*z[18];
    z[44]= - n<T>(19,2) - z[10];
    z[44]=z[44]*z[48];
    z[48]=n<T>(11,4) - z[47];
    z[48]=z[10]*z[48];
    z[48]=n<T>(7,4) + z[48];
    z[48]=z[10]*z[48];
    z[44]=z[48] + n<T>(1,6)*z[44];
    z[44]=z[11]*z[44];
    z[48]=static_cast<T>(1)- z[51];
    z[48]=z[3]*z[48]*z[47];
    z[50]= - n<T>(1,4) - z[47];
    z[50]=z[10]*z[50];
    z[50]=n<T>(3,4) + z[50];
    z[50]=z[10]*z[50];
    z[48]=z[50] + z[48];
    z[48]=z[3]*z[48];
    z[22]=z[22]*z[46];
    z[50]= - static_cast<T>(5)- n<T>(41,6)*z[2];
    z[22]=n<T>(1,2)*z[50] + z[22];
    z[22]=z[3]*z[22];
    z[22]=n<T>(11,12) + z[22];
    z[22]=z[22]*z[29];
    z[18]=z[18] + n<T>(1,2)*z[34] + z[22] + z[48] - 3*z[49] + z[44];
    z[18]=z[7]*z[18];
    z[22]=z[6]*z[2];
    z[34]=z[22]*z[39];
    z[44]=z[2] - z[34];
    z[48]=z[11] - 1;
    z[49]= - z[10] - z[48];
    z[49]=z[11]*z[49];
    z[44]= - z[25] + n<T>(11,3)*z[44] + z[49];
    z[37]=z[37]*z[6];
    z[37]=z[37] - n<T>(1,2)*z[39];
    z[49]=z[6]*z[37];
    z[48]= - z[11]*z[48];
    z[25]=z[48] - z[25];
    z[25]=z[5]*z[25];
    z[25]= - n<T>(5,3)*z[49] + n<T>(1,2)*z[25];
    z[25]=z[12]*z[25];
    z[45]= - n<T>(1,2) - z[45];
    z[45]=z[2]*z[45];
    z[45]= - z[51] + n<T>(1,2) + z[45];
    z[45]=z[3]*z[45];
    z[48]= - static_cast<T>(1)+ z[5];
    z[48]=z[12]*z[48];
    z[48]=z[48] + 1;
    z[48]=z[5]*z[48];
    z[48]= - static_cast<T>(1)+ z[48];
    z[29]=z[48]*z[29];
    z[25]=z[29] + z[25] + n<T>(1,2)*z[44] + z[45];
    z[25]=z[9]*z[25];
    z[29]=z[37]*z[47];
    z[44]= - n<T>(121,12) - z[41];
    z[44]=z[2]*z[44];
    z[44]= - n<T>(13,12) + z[44];
    z[44]=n<T>(1,4)*z[44] + z[34];
    z[44]=z[6]*z[44];
    z[45]= - n<T>(19,3) + z[2];
    z[44]=z[29] + n<T>(1,8)*z[45] + z[44];
    z[44]=z[10]*z[44];
    z[45]=2*z[2];
    z[47]= - static_cast<T>(1)- z[45];
    z[47]=z[47]*z[22];
    z[48]=n<T>(97,48) + z[2];
    z[48]=z[2]*z[48];
    z[47]=z[47] + n<T>(1,48) + z[48];
    z[47]=z[6]*z[47];
    z[43]=n<T>(97,6) - z[43];
    z[43]=z[44] + n<T>(1,8)*z[43] + z[47];
    z[43]=z[10]*z[43];
    z[44]= - n<T>(7,4) - z[2];
    z[44]=z[2]*z[44];
    z[44]= - n<T>(3,4) + z[44];
    z[44]=z[6]*z[44];
    z[44]=n<T>(25,6)*z[39] + z[44];
    z[44]=n<T>(1,2)*z[44] + z[29];
    z[44]=z[10]*z[44];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[2]*z[30];
    z[30]=n<T>(5,4) + z[30];
    z[30]=z[2]*z[30];
    z[30]=n<T>(1,2) + z[30];
    z[30]=z[30]*z[31];
    z[30]=z[44] + z[30] + n<T>(23,8) - n<T>(11,3)*z[2];
    z[30]=z[30]*z[33];
    z[33]=static_cast<T>(7)- 47*z[2];
    z[33]=n<T>(1,6)*z[33] + 9*z[34];
    z[33]=z[6]*z[33];
    z[44]= - static_cast<T>(8)- n<T>(23,8)*z[2];
    z[30]=z[30] + z[43] + n<T>(1,3)*z[44] + n<T>(1,8)*z[33];
    z[30]=z[11]*z[30];
    z[33]=static_cast<T>(39)+ z[2];
    z[33]=z[33]*z[17];
    z[33]=static_cast<T>(9)+ z[33];
    z[33]=z[33]*z[17];
    z[43]=z[21]*z[6];
    z[44]= - static_cast<T>(9)- z[41];
    z[44]=z[44]*z[43];
    z[33]=z[33] + z[44];
    z[14]=z[33]*z[14];
    z[26]=z[26] - z[38];
    z[26]=z[11]*z[26];
    z[33]=n<T>(1,16) + z[45];
    z[33]=z[2]*z[33];
    z[14]=n<T>(1,16)*z[26] + z[14] - n<T>(1,16) + z[33];
    z[14]=z[11]*z[14];
    z[26]= - static_cast<T>(37)- z[2];
    z[26]=z[26]*z[17];
    z[26]= - static_cast<T>(9)+ z[26];
    z[26]=z[26]*z[21];
    z[26]=z[26] + z[40];
    z[26]=z[6]*z[26];
    z[33]=static_cast<T>(9)+ z[17];
    z[33]=z[33]*z[21];
    z[26]=z[33] + z[26];
    z[26]=z[11]*z[26];
    z[33]=z[2]*z[35];
    z[33]=n<T>(1,2) + z[33];
    z[33]=z[6]*z[33];
    z[19]=z[33] - z[19];
    z[19]=z[6]*z[19];
    z[19]= - n<T>(1,2)*z[21] + z[19];
    z[19]=27*z[19] + z[26];
    z[19]=z[19]*z[42];
    z[21]=static_cast<T>(11)- 5*z[6];
    z[21]=z[6]*z[21];
    z[21]= - z[2] + z[21];
    z[14]=z[19] + n<T>(9,16)*z[21] + z[14];
    z[14]=z[5]*z[14];
    z[19]=n<T>(23,12) + z[2];
    z[19]=z[2]*z[19];
    z[19]= - n<T>(13,12) + z[19];
    z[19]=n<T>(1,2)*z[19] - z[34];
    z[19]=z[6]*z[19];
    z[21]=n<T>(17,3) + z[2];
    z[19]=n<T>(1,4)*z[21] + z[19];
    z[19]=n<T>(1,2)*z[19] + z[29];
    z[19]=z[10]*z[19];
    z[21]= - z[39]*z[31];
    z[26]=z[10]*z[37];
    z[21]=z[26] + z[21] + z[36];
    z[21]=z[3]*z[21];
    z[26]=static_cast<T>(1)- 71*z[2];
    z[22]=n<T>(1,24)*z[26] + z[22];
    z[22]=z[6]*z[22];
    z[26]=n<T>(1,6) + z[41];
    z[22]=n<T>(1,4)*z[26] + z[22];
    z[19]=n<T>(1,6)*z[21] + n<T>(1,2)*z[22] + z[19];
    z[19]=z[10]*z[19];
    z[21]=2*z[6];
    z[22]= - n<T>(43,16) + z[21];
    z[22]=z[22]*z[13];
    z[19]= - n<T>(7,16)*z[2] + z[22] + z[19];
    z[19]=z[3]*z[19];
    z[16]= - 27*z[15] + 31*z[16];
    z[16]=z[4]*z[16];
    z[22]=z[6] - 3;
    z[22]=z[22]*z[6];
    z[22]=z[22] + z[2];
    z[15]=n<T>(1,4)*z[22] - z[15];
    z[15]=z[15]*z[24];
    z[13]=static_cast<T>(1)- z[13];
    z[13]=z[13]*z[21];
    z[21]= - n<T>(1,3) - z[41];
    z[13]=n<T>(1,8)*z[21] + z[13];
    z[13]=z[3]*z[13];
    z[13]=n<T>(1,4)*z[16] + z[15] + z[13];
    z[13]=z[4]*z[13];
    z[15]=z[22]*npow(z[5],2);
    z[15]=27*z[15] - z[46];
    z[13]=n<T>(1,16)*z[15] + z[13];
    z[13]=z[1]*z[13];
    z[15]=n<T>(61,3) - n<T>(77,2)*z[6];
    z[15]=z[15]*z[31];
    z[15]= - n<T>(1,3) + z[15];
    z[16]= - z[39]*z[17];
    z[16]=z[16] + z[43];
    z[16]=z[6]*z[16];
    z[16]=z[16] + z[35];
    z[16]=z[16]*z[32];
    z[17]= - z[6]*z[28];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[6]*z[17];
    z[16]=z[16] + static_cast<T>(1)+ z[17];
    z[16]=z[10]*z[16];

    r += z[13] + z[14] + n<T>(1,4)*z[15] + z[16] + n<T>(1,2)*z[18] + z[19] + 
      z[20] + z[23] + n<T>(1,8)*z[25] + z[27] + z[30];
 
    return r;
}

template double qg_2lha_r1216(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1216(const std::array<dd_real,30>&);
#endif
