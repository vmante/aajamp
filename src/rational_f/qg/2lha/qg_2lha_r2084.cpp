#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2084(const std::array<T,30>& k) {
  T z[35];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[5];
    z[7]=k[12];
    z[8]=npow(z[2],3);
    z[9]=z[8]*z[4];
    z[10]=npow(z[2],2);
    z[11]=z[9] + z[10];
    z[12]= - z[4]*z[11];
    z[12]=z[2] + z[12];
    z[12]=z[4]*z[12];
    z[13]= - z[9] - 3*z[10];
    z[13]=z[4]*z[13];
    z[14]=3*z[2];
    z[13]= - z[14] + z[13];
    z[13]=z[4]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[6]*z[13];
    z[12]=z[13] + static_cast<T>(1)+ z[12];
    z[12]=z[6]*z[12];
    z[12]= - z[3] + z[12];
    z[12]=z[4]*z[12];
    z[13]= - z[2] + z[3];
    z[15]=npow(z[4],2);
    z[13]=z[5]*z[15]*z[13];
    z[12]=z[13] + z[12];
    z[12]=z[7]*z[12];
    z[13]=npow(z[3],2);
    z[16]= - static_cast<T>(1)- z[4];
    z[16]=z[16]*z[13];
    z[12]=z[16] + z[12];
    z[16]=z[2] - 1;
    z[17]=z[16]*z[8];
    z[18]=n<T>(1,2)*z[4];
    z[19]=z[17]*z[18];
    z[20]= - static_cast<T>(1)+ n<T>(3,2)*z[2];
    z[21]=z[20]*z[10];
    z[21]=z[21] + z[19];
    z[21]=z[4]*z[21];
    z[22]=n<T>(1,2)*z[2];
    z[23]= - static_cast<T>(1)+ z[14];
    z[23]=z[23]*z[22];
    z[21]=z[23] + z[21];
    z[21]=z[4]*z[21];
    z[21]=z[22] + z[21];
    z[21]=z[6]*z[21]*z[18];
    z[23]=npow(z[2],4);
    z[24]=z[23]*z[4];
    z[25]=n<T>(1,4)*z[24];
    z[26]=z[8] + z[25];
    z[26]=z[4]*z[26];
    z[26]=n<T>(7,8)*z[10] + z[26];
    z[26]=z[4]*z[26];
    z[27]=n<T>(1,8)*z[2];
    z[26]=z[27] + z[26];
    z[26]=z[4]*z[26];
    z[21]=z[26] + z[21];
    z[21]=z[6]*z[21];
    z[26]=n<T>(1,2)*z[10];
    z[28]= - z[15]*z[26];
    z[29]=z[18] + 1;
    z[29]=z[29]*z[4];
    z[30]=n<T>(1,2) + z[29];
    z[13]=z[30]*z[13];
    z[13]=z[28] + z[13];
    z[13]=z[5]*z[13];
    z[12]=n<T>(1,4)*z[13] + z[21] + n<T>(1,8)*z[12];
    z[12]=z[7]*z[12];
    z[13]=z[2] - n<T>(3,2);
    z[21]=z[13]*z[8];
    z[28]= - z[23]*z[18];
    z[21]=z[21] + z[28];
    z[21]=z[4]*z[21];
    z[28]=z[18]*z[8];
    z[30]= - z[13]*z[10];
    z[28]=z[30] + z[28];
    z[28]=z[4]*z[28];
    z[30]=z[2] - 3;
    z[31]= - z[30]*z[22];
    z[28]=z[31] + z[28];
    z[31]=n<T>(1,2)*z[3];
    z[28]=z[28]*z[31];
    z[32]=z[16]*z[10];
    z[21]=z[28] + z[32] + z[21];
    z[21]=z[3]*z[21];
    z[28]=z[2] - n<T>(5,2);
    z[33]=z[28]*z[8];
    z[30]= - z[30]*z[10];
    z[30]=z[30] + z[9];
    z[30]=z[30]*z[31];
    z[34]=z[8]*z[31];
    z[34]= - z[23] + z[34];
    z[34]=z[1]*z[34];
    z[30]=z[34] + z[30] + z[33] - z[24];
    z[30]=z[31]*z[30];
    z[33]=static_cast<T>(1)- n<T>(1,2)*z[5];
    z[23]=z[5]*z[23]*z[33];
    z[23]=z[23] + z[30];
    z[23]=z[1]*z[23];
    z[16]=z[9]*z[16];
    z[30]=z[17] + z[16];
    z[30]=z[5]*z[30];
    z[30]= - n<T>(3,2)*z[17] + z[30];
    z[30]=z[5]*z[30];
    z[21]=z[23] + z[21] + z[30];
    z[21]=z[1]*z[21];
    z[23]=z[14] - n<T>(7,2);
    z[30]=z[23]*z[10];
    z[17]= - z[17] + z[25];
    z[17]=z[4]*z[17];
    z[17]= - n<T>(1,4)*z[30] + z[17];
    z[17]=z[4]*z[17];
    z[17]=z[27] + z[17];
    z[17]=z[4]*z[17];
    z[20]=z[20]*z[22];
    z[25]= - n<T>(3,4) + z[2];
    z[25]=z[25]*z[10];
    z[25]=z[25] - n<T>(1,4)*z[9];
    z[25]=z[4]*z[25];
    z[20]=z[20] + z[25];
    z[20]=z[20]*z[15]*z[31];
    z[17]=z[17] + z[20];
    z[17]=z[3]*z[17];
    z[20]=z[22] - 1;
    z[25]=z[20]*z[10];
    z[19]=z[25] + z[19];
    z[19]=z[4]*z[19];
    z[19]= - z[22] + z[19];
    z[15]=z[19]*z[15];
    z[19]=n<T>(1,4)*z[3];
    z[11]= - z[11]*npow(z[4],3)*z[19];
    z[11]=z[15] + z[11];
    z[11]=z[3]*z[11];
    z[15]=z[10]*z[4];
    z[15]= - z[2] - z[15];
    z[15]=z[15]*z[18];
    z[11]=z[15] + z[11];
    z[11]=z[6]*z[11];
    z[15]= - z[26] - z[9];
    z[15]=z[15]*z[18];
    z[11]=n<T>(1,2)*z[11] + z[15] + z[17];
    z[11]=z[6]*z[11];
    z[15]=static_cast<T>(1)- z[10];
    z[15]=z[15]*z[26];
    z[15]=z[15] - z[16];
    z[15]=z[4]*z[15];
    z[16]=z[29]*z[10];
    z[16]=z[16] + z[26];
    z[17]= - z[2]*z[29];
    z[17]= - z[22] + z[17];
    z[17]=z[3]*z[17];
    z[17]=z[17] - z[16];
    z[17]=z[17]*z[31];
    z[22]=z[2]*z[28];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[26];
    z[15]=z[17] + z[22] + z[15];
    z[17]=z[20]*z[2];
    z[17]=z[17] + n<T>(1,2);
    z[16]= - z[5]*z[17]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[5]*z[15];
    z[14]= - n<T>(5,2) + z[14];
    z[10]=z[14]*z[10];
    z[8]=z[23]*z[8];
    z[8]=z[8] - z[24];
    z[8]=z[4]*z[8];
    z[8]=z[10] + z[8];
    z[8]=z[4]*z[8];
    z[9]= - 3*z[32] + z[9];
    z[9]=z[9]*z[18];
    z[10]= - z[2]*z[13];
    z[9]=z[10] + z[9];
    z[9]=z[4]*z[9];
    z[10]=static_cast<T>(1)+ z[2];
    z[9]=n<T>(1,2)*z[10] + z[9];
    z[9]=z[3]*z[9];
    z[10]= - n<T>(1,2) + z[2];
    z[10]=z[2]*z[10];
    z[8]=z[9] + z[10] + z[8];
    z[8]=z[8]*z[19];
    z[8]=n<T>(1,2)*z[21] + z[12] + z[15] + z[8] + z[11];

    r += n<T>(1,2)*z[8];
 
    return r;
}

template double qg_2lha_r2084(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2084(const std::array<dd_real,30>&);
#endif
