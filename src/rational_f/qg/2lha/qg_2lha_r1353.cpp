#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1353(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[9];
    z[7]=k[13];
    z[8]=k[14];
    z[9]=n<T>(5,4)*z[7];
    z[10]=z[9] + 1;
    z[11]=n<T>(1,3)*z[7];
    z[10]=z[10]*z[11];
    z[11]=z[8]*z[7];
    z[12]=n<T>(1,4)*z[7];
    z[13]=z[12] + n<T>(5,3);
    z[14]=z[13] - z[8];
    z[14]=z[14]*z[11];
    z[14]= - z[10] + n<T>(1,2)*z[14];
    z[14]=z[8]*z[14];
    z[15]=npow(z[7],2);
    z[14]=n<T>(11,24)*z[15] + z[14];
    z[14]=z[8]*z[14];
    z[16]=n<T>(1,4)*z[3];
    z[17]=n<T>(1,3)*z[8];
    z[18]=z[16] - z[17];
    z[19]= - z[18]*z[16];
    z[20]=n<T>(1,2)*z[15];
    z[21]= - z[8]*z[20];
    z[21]=z[15] + z[21];
    z[21]=z[8]*z[21];
    z[21]= - z[20] + z[21];
    z[22]=n<T>(1,2)*z[4];
    z[23]=z[22]*npow(z[8],2);
    z[21]=z[21]*z[23];
    z[14]=z[21] + z[19] - n<T>(11,48)*z[15] + z[14];
    z[14]=z[4]*z[14];
    z[19]=z[7] - n<T>(1,2);
    z[21]=n<T>(3,2)*z[7];
    z[24]=z[19]*z[21];
    z[25]= - z[17] + n<T>(1,3) + z[12];
    z[25]=z[8]*z[25];
    z[20]= - z[20] + z[25];
    z[20]=z[8]*z[20];
    z[20]= - n<T>(3,4)*z[3] + z[24] + z[20];
    z[24]=n<T>(1,2)*z[7];
    z[25]= - n<T>(1,3) - n<T>(3,8)*z[7];
    z[25]=z[25]*z[24];
    z[26]= - n<T>(1,6)*z[8] + n<T>(1,3) + n<T>(1,16)*z[7];
    z[11]=z[26]*z[11];
    z[11]=z[25] + z[11];
    z[11]=z[8]*z[11];
    z[25]=z[15]*z[8];
    z[17]=static_cast<T>(1)- z[17];
    z[17]=z[17]*z[25];
    z[17]= - z[15] + z[17];
    z[17]=z[8]*z[17];
    z[17]=n<T>(1,3)*z[15] + z[17];
    z[17]=z[4]*z[17];
    z[11]=n<T>(1,4)*z[17] + n<T>(3,16)*z[15] + z[11];
    z[11]=z[4]*z[11];
    z[11]=n<T>(1,4)*z[20] + z[11];
    z[11]=z[6]*z[11];
    z[17]=z[9] + n<T>(1,3);
    z[20]=z[17]*z[12];
    z[26]=z[7] + n<T>(5,3);
    z[26]=z[26]*z[24];
    z[26]=z[26] + n<T>(1,3);
    z[27]=n<T>(1,8)*z[7];
    z[28]=z[27] + n<T>(1,3);
    z[29]= - n<T>(1,4)*z[8] + z[28];
    z[29]=z[8]*z[29];
    z[29]= - n<T>(1,4)*z[26] + z[29];
    z[29]=z[8]*z[29];
    z[20]=z[20] + z[29];
    z[20]=z[8]*z[20];
    z[29]=static_cast<T>(1)- 5*z[7];
    z[29]=z[29]*z[27];
    z[30]=z[7] - 1;
    z[18]= - n<T>(1,2)*z[30] - z[18];
    z[16]=z[18]*z[16];
    z[11]=z[11] + z[14] + z[16] + z[29] + z[20];
    z[11]=z[6]*z[11];
    z[14]=npow(z[3],2);
    z[16]= - z[15] - z[14];
    z[16]=z[16]*z[22];
    z[18]=static_cast<T>(1)- 3*z[7];
    z[18]=z[7]*z[18];
    z[20]=3*z[3];
    z[22]=static_cast<T>(1)+ z[20];
    z[22]=z[3]*z[22];
    z[16]=z[16] + z[18] + z[22];
    z[16]=z[6]*z[16];
    z[18]= - n<T>(1,4) + z[20];
    z[18]=z[3]*z[18];
    z[18]=z[24] + z[18];
    z[18]=z[3]*z[18];
    z[20]=z[15] - z[14];
    z[22]=npow(z[3],3);
    z[29]=z[22]*z[4];
    z[20]=n<T>(1,2)*z[20] + z[29];
    z[20]=z[6]*z[20];
    z[20]= - z[22] + z[20];
    z[20]=z[5]*z[20];
    z[16]=z[20] + n<T>(1,2)*z[16] + 3*z[29] + n<T>(5,4)*z[15] + z[18];
    z[16]=z[6]*z[16];
    z[16]= - z[22] + z[16];
    z[16]=z[5]*z[16];
    z[18]=static_cast<T>(1)- n<T>(11,3)*z[7];
    z[18]=z[18]*z[24];
    z[20]=n<T>(1,3)*z[2];
    z[19]=n<T>(7,2)*z[19] - z[2];
    z[19]=z[19]*z[20];
    z[18]=z[18] + z[19];
    z[18]=z[2]*z[18];
    z[19]=z[3]*z[2];
    z[22]=npow(z[2],2);
    z[19]=z[19] + n<T>(1,4)*z[22];
    z[29]= - z[1] + z[4];
    z[14]=z[14]*z[19]*z[29];
    z[19]=n<T>(1,4)*z[2];
    z[29]=static_cast<T>(5)- z[2];
    z[29]=z[29]*z[19];
    z[31]=z[2] - 1;
    z[32]= - z[3]*z[31];
    z[29]=z[29] + z[32];
    z[29]=z[3]*z[29];
    z[22]=n<T>(5,6)*z[22] + z[29];
    z[22]=z[3]*z[22];
    z[14]=z[22] + z[14];
    z[14]=z[1]*z[14];
    z[14]=z[14] + z[16] + n<T>(13,12)*z[15] + z[18];
    z[16]=z[20] - 1;
    z[18]=z[27] - z[16];
    z[22]=n<T>(1,2)*z[2];
    z[18]=z[22]*z[7]*z[18];
    z[10]= - z[10] + z[18];
    z[10]=z[2]*z[10];
    z[13]=z[13]*z[24];
    z[18]= - z[7]*z[20];
    z[13]=z[13] + z[18];
    z[13]=z[2]*z[13];
    z[18]=z[8]*z[2];
    z[27]= - z[24]*z[18];
    z[13]=z[13] + z[27];
    z[13]=z[8]*z[13];
    z[10]=z[10] + z[13];
    z[10]=z[8]*z[10];
    z[13]= - n<T>(11,8)*z[7] + z[31];
    z[13]=z[2]*z[7]*z[13];
    z[13]=n<T>(11,4)*z[15] + z[13];
    z[27]=n<T>(1,6)*z[2];
    z[13]=z[13]*z[27];
    z[10]=z[13] + z[10];
    z[10]=z[8]*z[10];
    z[13]=z[7] - 3;
    z[13]=z[13]*z[24];
    z[13]=z[13] + 1;
    z[29]=z[13]*z[22];
    z[31]=z[24] - 1;
    z[31]=z[31]*z[7];
    z[29]=z[29] - n<T>(1,4) - z[31];
    z[29]=z[29]*z[22];
    z[13]=z[2]*z[13];
    z[13]=n<T>(1,2) + z[13];
    z[13]=z[3]*z[13];
    z[13]=z[29] + z[13];
    z[13]=z[3]*z[13];
    z[29]= - z[7]*z[30];
    z[31]= - n<T>(1,2) - z[31];
    z[31]=z[2]*z[31];
    z[29]=z[29] + z[31];
    z[29]=z[29]*z[22];
    z[31]=z[20]*z[8];
    z[29]=z[29] + z[31];
    z[13]=n<T>(1,2)*z[29] + z[13];
    z[29]=n<T>(1,2)*z[3];
    z[13]=z[13]*z[29];
    z[32]=z[2]*z[16]*z[15];
    z[25]= - z[22]*z[25];
    z[25]= - z[32] + z[25];
    z[25]=z[8]*z[25];
    z[15]= - z[15] - z[32];
    z[15]=z[15]*z[22];
    z[15]=z[15] + z[25];
    z[15]=z[15]*z[23];
    z[10]=z[15] + z[10] + z[13];
    z[10]=z[4]*z[10];
    z[13]=z[17]*z[24];
    z[15]= - static_cast<T>(1)- n<T>(5,2)*z[7];
    z[15]=n<T>(1,2)*z[15] + z[2];
    z[15]=z[15]*z[20];
    z[13]=z[13] + z[15];
    z[13]=z[13]*z[22];
    z[12]=z[12] - z[16];
    z[12]=z[2]*z[12];
    z[12]=z[12] - z[26];
    z[12]=z[12]*z[19];
    z[15]= - z[27] + z[28];
    z[15]=z[2]*z[15];
    z[15]=z[15] - n<T>(1,4)*z[18];
    z[15]=z[8]*z[15];
    z[12]=z[12] + z[15];
    z[12]=z[8]*z[12];
    z[12]=z[13] + z[12];
    z[12]=z[8]*z[12];
    z[13]=z[30]*z[2];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[7]*z[9];
    z[9]= - n<T>(5,6)*z[13] + n<T>(1,4) + z[9];
    z[9]=z[2]*z[9];
    z[13]=z[21] - 1;
    z[15]= - z[7]*z[13];
    z[9]=z[31] + z[15] + z[9];
    z[15]= - n<T>(3,2) - z[7];
    z[15]=z[15]*z[24];
    z[16]=z[30]*z[19];
    z[15]=z[16] + static_cast<T>(1)+ z[15];
    z[15]=z[2]*z[15];
    z[16]=static_cast<T>(1)+ z[21];
    z[16]=z[7]*z[16];
    z[15]=z[15] - n<T>(3,4) + z[16];
    z[16]=z[30]*z[22];
    z[13]=z[16] - z[13];
    z[13]=z[3]*z[13];
    z[13]=n<T>(1,2)*z[15] + z[13];
    z[13]=z[3]*z[13];
    z[9]=n<T>(1,2)*z[9] + z[13];
    z[9]=z[9]*z[29];
    z[9]=z[11] + z[10] + z[9] + z[12] + n<T>(1,4)*z[14];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r1353(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1353(const std::array<dd_real,30>&);
#endif
