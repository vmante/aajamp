#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2051(const std::array<T,30>& k) {
  T z[37];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[24];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[12];
    z[9]=k[27];
    z[10]=z[6] + 3;
    z[11]=n<T>(1,2)*z[6];
    z[12]=z[10]*z[11];
    z[12]=z[12] + 1;
    z[13]=z[3]*z[6];
    z[14]=z[12]*z[13];
    z[15]=z[11] + 1;
    z[15]=z[15]*z[6];
    z[16]=z[14] - z[15];
    z[16]=z[16]*z[3];
    z[17]=z[6] - 1;
    z[18]=z[17]*z[11];
    z[19]=static_cast<T>(1)- z[18];
    z[19]=z[6]*z[19];
    z[19]=z[19] + z[16];
    z[19]=z[3]*z[19];
    z[20]=z[15] + n<T>(1,2);
    z[20]=z[20]*z[13];
    z[21]=z[6] + 1;
    z[22]=z[21]*z[6];
    z[23]=z[20] - n<T>(3,4)*z[22];
    z[23]=z[23]*z[3];
    z[24]=n<T>(3,2) + z[6];
    z[24]=z[6]*z[24];
    z[24]=static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[11];
    z[24]=z[24] + z[23];
    z[24]=z[3]*z[24];
    z[25]=n<T>(1,4)*z[6];
    z[24]= - z[25] + z[24];
    z[24]=z[7]*z[24];
    z[19]=z[19] + z[24];
    z[19]=z[7]*z[19];
    z[24]=z[21]*z[13];
    z[26]=3*z[6];
    z[27]=static_cast<T>(5)+ z[26];
    z[27]=n<T>(1,2)*z[27] + z[24];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(5,2) + z[27];
    z[28]=n<T>(1,2)*z[3];
    z[27]=z[27]*z[28];
    z[19]=z[27] + z[19];
    z[27]=z[21]*z[11];
    z[29]= - z[27] + z[20];
    z[29]=z[3]*z[29];
    z[30]=npow(z[6],2);
    z[29]= - n<T>(1,2)*z[30] + z[29];
    z[29]=z[7]*z[29];
    z[31]= - static_cast<T>(1)+ z[11];
    z[31]=z[6]*z[31];
    z[14]=z[31] + 3*z[14];
    z[14]=z[3]*z[14];
    z[14]=3*z[29] - z[11] + z[14];
    z[14]=z[7]*z[14];
    z[29]=n<T>(3,2)*z[24] - n<T>(7,2) - 5*z[6];
    z[29]=z[3]*z[29];
    z[14]=z[14] + n<T>(1,2) + z[29];
    z[29]=z[11]*z[3];
    z[12]=z[12]*z[29];
    z[31]= - n<T>(1,2) - z[6];
    z[31]=z[6]*z[31];
    z[31]=z[31] + z[20];
    z[32]=n<T>(1,2)*z[7];
    z[31]=z[31]*z[32];
    z[33]= - n<T>(1,4) - z[6];
    z[33]=z[6]*z[33];
    z[12]=z[31] + z[33] + z[12];
    z[12]=z[7]*z[12];
    z[31]=z[21]*z[3];
    z[33]=z[11]*z[31];
    z[33]=z[6] + z[33];
    z[12]=n<T>(1,2)*z[33] + z[12];
    z[33]=3*z[4];
    z[12]=z[12]*z[33];
    z[12]=n<T>(1,4)*z[14] + z[12];
    z[12]=z[4]*z[12];
    z[12]=n<T>(1,2)*z[19] + z[12];
    z[12]=z[4]*z[12];
    z[14]= - static_cast<T>(1)- z[18];
    z[14]=z[6]*z[14];
    z[18]=z[30]*z[32];
    z[19]=z[21]*z[18];
    z[14]=z[14] + z[19];
    z[14]=z[7]*z[14];
    z[17]=z[6]*z[17];
    z[17]= - static_cast<T>(5)+ z[17];
    z[17]=z[6]*z[17];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[3]*z[17];
    z[17]=static_cast<T>(3)+ z[17];
    z[14]=n<T>(1,2)*z[17] + z[14];
    z[14]=z[4]*z[14];
    z[17]=z[6] - 3;
    z[19]= - z[17]*z[11];
    z[19]=static_cast<T>(3)+ z[19];
    z[19]=z[6]*z[19];
    z[15]=z[15] + 1;
    z[34]=z[7]*z[6];
    z[35]=z[15]*z[34];
    z[19]=z[19] + z[35];
    z[19]=z[19]*z[32];
    z[17]=z[17]*z[25];
    z[35]=static_cast<T>(1)+ z[17];
    z[13]=z[35]*z[13];
    z[13]=z[13] + z[19];
    z[13]=z[8]*z[13];
    z[10]=z[3]*z[10];
    z[10]= - static_cast<T>(3)+ z[10];
    z[10]=z[3]*z[10];
    z[10]=z[13] + z[10] + z[14];
    z[13]=npow(z[6],3);
    z[14]=z[13]*z[7];
    z[13]=z[14] + z[13];
    z[13]=z[13]*z[2];
    z[19]=z[25] + 1;
    z[18]=z[19]*z[18];
    z[18]=n<T>(1,4)*z[13] + z[30] + z[18];
    z[18]=z[18]*z[8]*z[7];
    z[35]= - z[21]*z[28];
    z[35]=z[35] + n<T>(1,4) - z[6];
    z[35]=z[3]*z[35];
    z[14]= - 3*z[30] + z[14];
    z[14]=z[14]*z[32];
    z[14]=z[6] + z[14];
    z[36]=n<T>(1,2)*z[4];
    z[14]=z[14]*z[36];
    z[14]=z[35] + z[14] + z[18];
    z[14]=z[2]*z[14];
    z[10]=n<T>(1,2)*z[10] + z[14];
    z[10]=z[2]*z[10];
    z[14]= - z[15]*z[11];
    z[18]=z[6]*z[19];
    z[18]= - n<T>(1,2) + z[18];
    z[18]=z[18]*z[34];
    z[14]=z[14] + z[18];
    z[14]=z[7]*z[14];
    z[18]= - static_cast<T>(1)+ z[25];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(1,2) + z[18];
    z[18]=z[7]*z[30]*z[18];
    z[13]= - z[32]*z[13];
    z[13]=z[18] + z[13];
    z[13]=z[2]*z[13];
    z[15]=z[15]*z[29];
    z[13]=z[13] + z[15] + z[14];
    z[13]=z[2]*z[13];
    z[14]=z[30]*z[31];
    z[14]=z[26] + z[14];
    z[14]=z[14]*z[32];
    z[14]= - z[20] + z[14];
    z[14]=z[14]*z[32];
    z[13]=z[14] + z[13];
    z[13]=z[9]*z[13];
    z[14]=npow(z[7],2);
    z[15]= - z[14]*z[26];
    z[18]=z[31] - 1;
    z[19]=z[18]*z[3];
    z[15]=z[19] + z[15];
    z[26]= - z[7]*z[27];
    z[26]= - z[6] + z[26];
    z[26]=z[7]*z[26];
    z[18]= - n<T>(1,2)*z[18] + z[26];
    z[18]=z[18]*z[33];
    z[15]=n<T>(1,4)*z[15] + z[18];
    z[15]=z[15]*npow(z[4],2);
    z[18]=npow(z[4],3);
    z[18]=n<T>(3,2)*z[18];
    z[14]= - z[5]*z[6]*z[14]*z[18];
    z[19]=npow(z[8],2)*z[19];
    z[14]=z[14] + z[15] + n<T>(1,4)*z[19];
    z[14]=z[5]*z[14];
    z[15]= - static_cast<T>(1)+ z[3];
    z[15]=z[3]*z[15];
    z[15]=z[15] + z[36];
    z[15]=z[15]*npow(z[2],2);
    z[15]=z[18] + z[15];
    z[15]=z[1]*z[15];
    z[13]=z[15] + z[13] + z[14];
    z[14]=static_cast<T>(1)- z[17];
    z[14]=z[6]*z[14];
    z[14]=z[14] + z[16];
    z[14]=z[3]*z[14];
    z[15]=static_cast<T>(1)+ z[22];
    z[15]=z[15]*z[25];
    z[15]=z[15] + z[23];
    z[15]=z[3]*z[15];
    z[15]=z[25] + z[15];
    z[15]=z[7]*z[15];
    z[14]=z[14] + z[15];
    z[14]=z[7]*z[14];
    z[15]=z[22] - z[20];
    z[15]=z[3]*z[15];
    z[11]= - z[11] + z[15];
    z[11]=z[7]*z[11];
    z[11]= - z[16] + z[11];
    z[11]=z[7]*z[11];
    z[15]= - z[6] - z[24];
    z[15]=z[15]*z[28];
    z[11]=z[15] + z[11];
    z[15]=n<T>(1,2)*z[8];
    z[11]=z[11]*z[15];
    z[16]= - n<T>(5,2)*z[21] + z[24];
    z[16]=z[3]*z[16];
    z[16]=n<T>(5,2) + z[16];
    z[16]=z[16]*z[28];
    z[11]=z[11] + z[16] + z[14];
    z[11]=z[11]*z[15];

    r += z[10] + z[11] + z[12] + n<T>(1,2)*z[13];
 
    return r;
}

template double qg_2lha_r2051(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2051(const std::array<dd_real,30>&);
#endif
