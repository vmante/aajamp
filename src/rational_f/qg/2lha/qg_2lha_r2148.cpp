#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2148(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[6];
    z[5]=k[3];
    z[6]=k[9];
    z[7]=n<T>(1,2)*z[2] - 1;
    z[8]=z[1] + 1;
    z[7]=z[8]*z[7];
    z[9]=static_cast<T>(1)+ z[2];
    z[10]=npow(z[6],2);
    z[9]=z[5]*z[10]*z[9];
    z[11]=z[2]*z[6];
    z[9]=3*z[11] + z[9];
    z[12]=n<T>(1,2)*z[5];
    z[9]=z[9]*z[12];
    z[9]=z[9] - n<T>(1,2) + z[2];
    z[9]=z[5]*z[9];
    z[13]=z[5] - z[8];
    z[13]=z[4]*z[13];
    z[8]=3*z[8] + z[13];
    z[8]=z[4]*z[8];
    z[7]=n<T>(1,2)*z[8] + z[9] + z[7];
    z[7]=z[3]*z[7];
    z[8]=z[2] + 3;
    z[8]=z[8]*z[10];
    z[9]= - z[8]*z[12];
    z[9]= - z[11] + z[9];
    z[9]=z[5]*z[9];
    z[12]=static_cast<T>(3)- z[2];
    z[9]=n<T>(1,2)*z[12] + z[9];
    z[12]=z[5] - 1;
    z[13]=n<T>(1,2)*z[1] - z[12];
    z[13]=z[4]*z[13];
    z[13]=z[13] - n<T>(9,4) - z[1];
    z[13]=z[4]*z[13];
    z[7]=z[7] + n<T>(1,2)*z[9] + z[13];
    z[7]=z[3]*z[7];
    z[8]=z[5]*z[8];
    z[8]=z[11] + z[8];
    z[9]=z[4]*z[12];
    z[9]=n<T>(3,2) + z[9];
    z[9]=z[4]*z[9];
    z[8]=n<T>(1,4)*z[8] + z[9];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[7]= - n<T>(1,8)*z[10] + z[7];

    r += n<T>(3,4)*z[7]*z[3];
 
    return r;
}

template double qg_2lha_r2148(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2148(const std::array<dd_real,30>&);
#endif
