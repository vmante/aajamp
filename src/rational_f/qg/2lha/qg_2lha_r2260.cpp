#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2260(const std::array<T,30>& k) {
  T z[21];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[24];
    z[5]=k[4];
    z[6]=k[3];
    z[7]=k[9];
    z[8]=k[13];
    z[9]=npow(z[3],2);
    z[10]=3*z[3];
    z[11]= - static_cast<T>(5)+ z[10];
    z[11]=z[4]*z[11]*z[9];
    z[12]=3*z[4] - z[2];
    z[13]=npow(z[3],3);
    z[12]=z[1]*z[13]*z[12];
    z[11]=z[11] + z[12];
    z[12]=n<T>(1,2)*z[3];
    z[14]=z[12] - 1;
    z[15]= - z[14]*z[9];
    z[16]=n<T>(1,2)*z[5];
    z[17]=z[13]*z[16];
    z[15]=z[15] + z[17];
    z[15]=z[2]*z[15];
    z[17]=z[5]*z[4];
    z[17]= - n<T>(3,4)*z[17] + 1;
    z[17]=z[13]*z[17];
    z[11]=n<T>(1,2)*z[15] + z[17] + n<T>(1,4)*z[11];
    z[11]=z[1]*z[11];
    z[15]=static_cast<T>(1)- n<T>(1,4)*z[5];
    z[15]=z[7]*z[15];
    z[15]=z[15] + n<T>(1,4) + z[10];
    z[15]=z[9]*z[15];
    z[17]=z[7]*z[5];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[6]*z[17];
    z[17]=z[17] + 3*z[5];
    z[17]=z[13]*z[17];
    z[15]=z[17] + z[15];
    z[15]=z[7]*z[15];
    z[15]= - z[13] + z[15];
    z[15]=z[6]*z[15];
    z[11]=z[11] + z[15];
    z[15]=n<T>(1,2)*z[8];
    z[17]=z[3] - 1;
    z[15]=z[15]*z[17];
    z[18]= - static_cast<T>(1)+ n<T>(3,2)*z[3];
    z[15]=z[15] - z[18];
    z[15]=z[8]*z[15]*z[9];
    z[19]= - n<T>(1,2) + z[3];
    z[19]=z[19]*z[9];
    z[19]=z[19] + z[15];
    z[19]=z[5]*z[19];
    z[20]= - static_cast<T>(1)+ z[8];
    z[14]=z[14]*z[3];
    z[14]=z[14] + n<T>(1,2);
    z[14]=z[14]*z[3]*z[20];
    z[14]=z[19] + z[14];
    z[14]=z[2]*z[14];
    z[19]=z[4]*z[3];
    z[19]= - z[19] + n<T>(1,2)*z[9];
    z[20]=z[5] + 1;
    z[19]=z[19]*z[20];
    z[20]= - z[7]*z[12];
    z[19]=z[20] + z[19];
    z[19]=z[7]*z[19];
    z[14]=z[14] + z[19];
    z[18]= - z[18]*z[9];
    z[17]=z[17]*z[8];
    z[12]=z[12]*z[17];
    z[12]=z[18] + z[12];
    z[12]=z[8]*z[12];
    z[18]= - n<T>(3,8) + z[3];
    z[9]=z[18]*z[9];
    z[10]=static_cast<T>(11)- z[10];
    z[10]=z[3]*z[10];
    z[10]= - static_cast<T>(5)+ z[10];
    z[10]=z[3]*z[10];
    z[10]=z[10] - 3*z[17];
    z[10]=z[8]*z[10];
    z[17]= - static_cast<T>(1)+ n<T>(3,8)*z[3];
    z[17]=z[3]*z[17];
    z[17]=n<T>(1,4) + z[17];
    z[17]=z[3]*z[17];
    z[10]=z[17] + n<T>(1,8)*z[10];
    z[10]=z[4]*z[10];
    z[15]= - z[13] - z[15];
    z[15]=z[4]*z[15];
    z[13]=z[13] + n<T>(3,2)*z[15];
    z[13]=z[13]*z[16];
    z[9]=z[13] + z[10] + z[9] + z[12] + n<T>(1,4)*z[14] + n<T>(1,2)*z[11];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r2260(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2260(const std::array<dd_real,30>&);
#endif
