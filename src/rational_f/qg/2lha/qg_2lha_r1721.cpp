#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1721(const std::array<T,30>& k) {
  T z[21];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[24];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[9];
    z[8]=k[14];
    z[9]=npow(z[1],2);
    z[10]=z[1]*z[4];
    z[11]=z[10] - z[4];
    z[12]= - static_cast<T>(5)+ n<T>(1,8)*z[11];
    z[12]=z[1]*z[12];
    z[12]=static_cast<T>(5)+ z[12];
    z[12]=z[12]*z[9];
    z[13]=z[8] - 1;
    z[14]= - static_cast<T>(1)- 21*z[1];
    z[14]=z[1]*z[14];
    z[14]=z[14] + z[13];
    z[14]=z[5]*z[14];
    z[12]=z[12] + n<T>(1,8)*z[14];
    z[12]=z[2]*z[12];
    z[14]=static_cast<T>(41)- z[10];
    z[14]=z[14]*z[9];
    z[15]=n<T>(1,2)*z[5];
    z[16]=z[15] - 1;
    z[16]=z[16]*z[5];
    z[16]=z[16] + n<T>(1,2);
    z[16]=z[16]*z[7];
    z[17]=z[15]*z[16];
    z[18]=n<T>(1,4)*z[5] - n<T>(1,8) + 5*z[1];
    z[18]=z[5]*z[18];
    z[12]=z[12] + z[17] + n<T>(1,8)*z[14] + z[18];
    z[12]=z[2]*z[12];
    z[14]= - 41*z[5] + static_cast<T>(21)+ n<T>(41,2)*z[6];
    z[14]=z[14]*z[15];
    z[17]=npow(z[5],2);
    z[18]= - static_cast<T>(1)+ z[6];
    z[18]=z[7]*z[18]*z[17];
    z[14]=z[14] + 5*z[18];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(41,4)*z[5] + z[14];
    z[17]=3*z[17];
    z[18]=z[5] - 1;
    z[19]= - z[6] + z[18];
    z[19]=z[19]*z[17];
    z[20]= - z[7]*z[6]*npow(z[5],3);
    z[19]=z[19] + z[20];
    z[19]=z[7]*z[19];
    z[18]=z[1] + z[18];
    z[18]=z[2]*z[18];
    z[18]=z[18] - 1;
    z[18]=z[18]*npow(z[1],3);
    z[9]=z[5]*z[9];
    z[9]= - 3*z[9] + z[18];
    z[9]=z[2]*z[9];
    z[9]=z[9] + z[17] + z[19];
    z[9]=z[3]*z[9];
    z[9]=n<T>(5,2)*z[9] + n<T>(1,2)*z[14] + z[12];
    z[9]=z[3]*z[9];
    z[12]= - n<T>(13,2) - z[10];
    z[12]=z[1]*z[12];
    z[12]= - z[15] + z[12] - static_cast<T>(1)+ n<T>(3,2)*z[8];
    z[11]=n<T>(21,2) - z[11];
    z[11]=z[1]*z[11];
    z[11]= - static_cast<T>(9)+ z[11];
    z[11]=z[1]*z[11];
    z[14]= - n<T>(3,2) + z[8];
    z[14]=z[5]*z[14];
    z[11]=z[14] + static_cast<T>(1)+ z[11];
    z[11]=n<T>(1,2)*z[11] - z[16];
    z[14]=n<T>(1,2)*z[2];
    z[11]=z[11]*z[14];
    z[11]=z[11] + n<T>(1,2)*z[12] - z[16];
    z[11]=z[2]*z[11];
    z[12]=static_cast<T>(7)+ 3*z[6];
    z[12]=n<T>(1,2)*z[12] + 7*z[5];
    z[16]=static_cast<T>(5)- n<T>(1,2)*z[6];
    z[16]=z[16]*z[7]*z[5];
    z[12]=n<T>(1,2)*z[12] + z[16];
    z[12]=z[7]*z[12];
    z[12]= - n<T>(3,4) + z[12];
    z[9]=z[9] + n<T>(1,2)*z[12] + z[11];
    z[9]=z[3]*z[9];
    z[11]= - z[4] - z[13];
    z[12]= - z[4] + n<T>(1,2)*z[10];
    z[12]=z[1]*z[12];
    z[15]=z[8]*z[15];
    z[16]= - static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[17]=z[5] + n<T>(1,2)*z[4] + z[16];
    z[17]=z[7]*z[17];
    z[11]=z[17] + z[15] + n<T>(1,2)*z[11] + z[12];
    z[11]=z[11]*z[14];
    z[10]=z[10] + z[8];
    z[10]=static_cast<T>(1)+ 5*z[10];
    z[12]=n<T>(3,4)*z[5] + n<T>(1,4)*z[4] + z[16];
    z[12]=z[7]*z[12];
    z[10]=z[11] + n<T>(1,4)*z[10] + z[12];
    z[10]=z[2]*z[10];
    z[11]=z[6] + 1;
    z[12]=n<T>(5,4)*z[4];
    z[11]= - z[11]*z[12];
    z[11]= - n<T>(1,2)*z[7] + z[11] - z[13];
    z[11]=z[7]*z[11];
    z[10]=z[10] + z[12] + z[11];
    z[9]=n<T>(1,2)*z[10] + z[9];
    z[9]=z[3]*z[9];
    z[10]=z[13] - z[4];
    z[10]=z[10]*z[7];
    z[11]=z[10] + z[4] - z[8];
    z[11]=z[2]*z[11];
    z[10]=z[10] + z[11];
    z[10]=z[2]*z[10];

    r += z[9] + n<T>(1,8)*z[10];
 
    return r;
}

template double qg_2lha_r1721(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1721(const std::array<dd_real,30>&);
#endif
