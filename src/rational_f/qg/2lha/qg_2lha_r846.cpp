#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r846(const std::array<T,30>& k) {
  T z[56];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[14];
    z[5]=k[11];
    z[6]=k[4];
    z[7]=k[24];
    z[8]=k[5];
    z[9]=k[3];
    z[10]=k[27];
    z[11]=k[13];
    z[12]=k[12];
    z[13]=k[6];
    z[14]=n<T>(1,2)*z[1];
    z[15]=n<T>(1,2)*z[5];
    z[16]=z[14] + z[15];
    z[17]=n<T>(1,2)*z[8];
    z[18]=n<T>(13,2) + z[11];
    z[18]= - z[1] - n<T>(13,2)*z[10] + n<T>(1,2)*z[18] - 3*z[12];
    z[18]=z[18]*z[17];
    z[19]=n<T>(7,4)*z[10];
    z[20]=static_cast<T>(1)- z[11];
    z[21]=n<T>(7,4)*z[7];
    z[22]= - static_cast<T>(1)- z[21];
    z[22]=z[7]*z[22];
    z[18]=z[18] + z[22] + z[19] + n<T>(1,4)*z[20] - z[12] + z[16];
    z[18]=z[8]*z[18];
    z[20]=n<T>(1,2)*z[13];
    z[22]= - z[20] + 3*z[7];
    z[23]=5*z[10];
    z[24]=n<T>(1,2)*z[11];
    z[25]=z[24] - 1;
    z[26]= - z[23] + z[25] - z[22];
    z[26]=n<T>(1,2)*z[26] - z[1];
    z[27]=n<T>(47,9)*z[3];
    z[26]=n<T>(1,2)*z[26] + z[27];
    z[26]=z[3]*z[26];
    z[28]=npow(z[7],2);
    z[29]=z[5]*z[10];
    z[30]= - z[29] - n<T>(7,2)*z[28];
    z[18]=z[18] + n<T>(1,2)*z[30] + z[26];
    z[26]=z[11] - z[13];
    z[30]=7*z[7];
    z[31]= - static_cast<T>(19)+ z[30];
    z[31]=z[7]*z[31];
    z[31]=z[26] - z[31];
    z[32]= - n<T>(5,2) + 7*z[10];
    z[32]=z[10]*z[32];
    z[16]=n<T>(1,4)*z[32] - z[12] - z[16] - n<T>(1,8)*z[31];
    z[16]=z[16]*z[17];
    z[31]=n<T>(1,2)*z[7];
    z[32]=static_cast<T>(1)+ n<T>(21,8)*z[7];
    z[32]=z[32]*z[31];
    z[33]=z[12] + 1;
    z[34]=n<T>(1,4)*z[1];
    z[16]=z[16] + z[34] + z[32] + z[15] + n<T>(1,4)*z[33] + z[10];
    z[16]=z[8]*z[16];
    z[32]=n<T>(1,2)*z[12];
    z[35]=z[32] + z[15];
    z[36]=z[7] + z[10];
    z[37]= - z[36] - z[35];
    z[37]=z[8]*z[37];
    z[33]=z[37] + z[15] + n<T>(1,2)*z[33] + z[36];
    z[33]=z[8]*z[33];
    z[37]=z[7]*z[12];
    z[37]=z[37] + z[29];
    z[38]=z[36] + 1;
    z[38]=z[38]*z[3];
    z[38]=z[38] + z[37];
    z[33]= - n<T>(1,2)*z[38] + z[33];
    z[39]=n<T>(1,2)*z[6];
    z[33]=z[33]*z[39];
    z[40]=z[31] + n<T>(1,2) + z[14] + z[10];
    z[40]=z[40]*z[3];
    z[41]=z[32]*z[7];
    z[40]=z[40] + z[41] + z[29];
    z[16]=z[33] - n<T>(1,2)*z[40] + z[16];
    z[16]=z[6]*z[16];
    z[16]=n<T>(1,2)*z[18] + z[16];
    z[16]=z[6]*z[16];
    z[18]=z[15] + n<T>(103,9);
    z[33]=n<T>(103,9)*z[1];
    z[41]= - z[33] - z[12] - z[18];
    z[41]=z[8]*z[41];
    z[42]=n<T>(3,2)*z[5];
    z[43]=z[14] - 1;
    z[41]=z[41] + n<T>(27,4)*z[7] + z[42] - n<T>(3,2)*z[12] - z[43];
    z[41]=z[41]*z[17];
    z[32]=z[32] + z[4];
    z[44]=n<T>(9,4)*z[7];
    z[45]=z[44] + z[5];
    z[46]=z[45] - z[32];
    z[47]=z[1] - 2;
    z[48]=z[3]*z[47];
    z[34]=z[34] - n<T>(94,9)*z[48];
    z[34]=z[3]*z[34];
    z[45]=n<T>(9,4)*z[10] + z[45] + z[12];
    z[48]=n<T>(197,18) - z[45];
    z[48]=z[8]*z[48];
    z[45]=z[48] + n<T>(1,2) - z[45];
    z[45]=z[45]*z[17];
    z[48]= - n<T>(1,4) + z[27];
    z[48]=z[3]*z[48];
    z[45]=z[48] + z[45];
    z[45]=z[6]*z[45];
    z[34]=z[45] + z[41] + n<T>(1,2)*z[46] + z[34];
    z[34]=z[6]*z[34];
    z[41]=n<T>(9,8)*z[7];
    z[45]=z[15] + z[4];
    z[46]=z[41] + z[45];
    z[46]=z[1]*z[46];
    z[48]=47*z[3];
    z[49]=n<T>(1,3)*z[1];
    z[50]= - static_cast<T>(2)+ z[49];
    z[50]=z[1]*z[50];
    z[50]=n<T>(5,3) + z[50];
    z[50]=z[50]*z[48];
    z[51]= - n<T>(617,2) + 367*z[1];
    z[50]=n<T>(1,12)*z[51] + z[50];
    z[51]=n<T>(1,3)*z[3];
    z[50]=z[50]*z[51];
    z[18]= - z[33] + z[18];
    z[18]=z[8]*z[18];
    z[52]=z[4] + n<T>(1,4)*z[5];
    z[18]=z[34] + z[18] + z[50] + z[46] - z[44] + n<T>(1,4) - z[52];
    z[18]=z[6]*z[18];
    z[34]= - n<T>(103,9) - z[35];
    z[34]=z[34]*z[17];
    z[34]=z[34] + n<T>(103,9) - z[35];
    z[34]=z[8]*z[34];
    z[46]=n<T>(1,9)*z[3];
    z[50]= - n<T>(197,2) - z[48];
    z[50]=z[50]*z[46];
    z[32]=z[34] - n<T>(1,2)*z[32] + z[50];
    z[32]=z[6]*z[32];
    z[34]=z[1] - 1;
    z[50]=z[34]*z[48];
    z[50]=z[50] - n<T>(97,2) + 50*z[1];
    z[50]=z[50]*z[51];
    z[42]= - n<T>(103,3)*z[1] + n<T>(103,3) + z[42];
    z[42]=z[42]*z[17];
    z[53]=z[43]*z[4];
    z[32]=z[32] + z[42] + z[50] + z[15] + n<T>(1,4) + z[53];
    z[32]=z[6]*z[32];
    z[42]=z[5] + n<T>(1,2);
    z[50]=z[1]*z[4];
    z[54]=z[50] + z[42];
    z[54]=z[1]*z[54];
    z[42]=z[54] - z[4] - z[42];
    z[47]=z[47]*z[1];
    z[47]=z[47] + 1;
    z[54]=n<T>(47,3)*z[3];
    z[54]= - z[47]*z[54];
    z[55]=npow(z[1],2);
    z[55]=static_cast<T>(1)- z[55];
    z[54]=n<T>(1,2)*z[55] + z[54];
    z[54]=z[3]*z[54];
    z[32]=z[32] + n<T>(1,2)*z[42] + z[54];
    z[32]=z[6]*z[32];
    z[42]=z[1]*z[53];
    z[53]= - z[1]*z[43];
    z[53]= - n<T>(1,2) + z[53];
    z[53]=z[3]*z[53];
    z[42]=z[53] + n<T>(1,2)*z[4] + z[42];
    z[53]=z[34]*z[3];
    z[54]=z[53] + z[4] - z[50];
    z[55]=z[4] - z[3];
    z[55]=z[6]*z[55];
    z[54]=3*z[54] + z[55];
    z[54]=z[54]*z[39];
    z[42]=3*z[42] + z[54];
    z[42]=z[6]*z[42];
    z[54]=3*z[4];
    z[50]=z[54] - z[50];
    z[50]=z[1]*z[50];
    z[50]= - z[54] + z[50];
    z[50]=z[1]*z[50];
    z[54]= - static_cast<T>(3)+ z[1];
    z[54]=z[1]*z[54];
    z[54]=static_cast<T>(3)+ z[54];
    z[54]=z[1]*z[54];
    z[54]= - static_cast<T>(1)+ z[54];
    z[54]=z[3]*z[54];
    z[50]=z[54] + z[4] + z[50];
    z[42]=n<T>(1,2)*z[50] + z[42];
    z[42]=z[2]*z[42];
    z[43]= - z[45]*z[43];
    z[43]=n<T>(1,4) + z[43];
    z[43]=z[1]*z[43];
    z[50]= - static_cast<T>(1)- z[45];
    z[43]=n<T>(1,2)*z[50] + z[43];
    z[43]=z[1]*z[43];
    z[49]= - static_cast<T>(1)+ z[49];
    z[49]=z[1]*z[49];
    z[49]=static_cast<T>(1)+ z[49];
    z[49]=z[1]*z[49];
    z[49]= - n<T>(1,3) + z[49];
    z[48]=z[49]*z[48];
    z[49]=n<T>(97,2) - n<T>(47,3)*z[1];
    z[49]=z[1]*z[49];
    z[49]= - static_cast<T>(50)+ z[49];
    z[49]=z[1]*z[49];
    z[48]=z[48] + n<T>(103,6) + z[49];
    z[48]=z[48]*z[51];
    z[32]=z[42] + z[32] + z[48] + n<T>(1,4) + z[43];
    z[32]=z[2]*z[32];
    z[42]= - z[4] + z[44];
    z[42]=z[42]*z[14];
    z[41]=z[42] - z[41] + n<T>(1,4) + z[52];
    z[41]=z[1]*z[41];
    z[42]=n<T>(1387,8) - 94*z[1];
    z[42]=z[1]*z[42];
    z[43]=z[3]*z[47];
    z[42]=94*z[43] - n<T>(635,8) + z[42];
    z[42]=z[42]*z[46];
    z[43]= - n<T>(1,2) - z[45];
    z[18]=z[32] + z[18] + z[42] + n<T>(1,2)*z[43] + z[41];
    z[18]=z[2]*z[18];
    z[26]= - n<T>(9,2)*z[12] + n<T>(197,9) + n<T>(1,4)*z[26];
    z[23]= - z[14] - z[21] - n<T>(3,4)*z[5] + n<T>(1,2)*z[26] - z[23];
    z[23]=z[8]*z[23];
    z[25]=z[25] - z[20];
    z[26]= - static_cast<T>(11)- z[30];
    z[26]=z[7]*z[26];
    z[26]=z[26] + z[10];
    z[15]=z[15] - n<T>(7,2)*z[12] - z[25] + n<T>(3,4)*z[26];
    z[15]=n<T>(1,2)*z[15] + z[23];
    z[15]=z[15]*z[17];
    z[23]= - static_cast<T>(1)+ n<T>(7,8)*z[10];
    z[23]=z[10]*z[23];
    z[26]=n<T>(7,8)*z[7];
    z[32]= - static_cast<T>(1)+ z[26];
    z[32]=z[7]*z[32];
    z[23]=z[32] + z[23] - z[35];
    z[23]=z[8]*z[23];
    z[32]=n<T>(7,4)*z[28];
    z[35]=npow(z[10],2);
    z[35]=n<T>(7,4)*z[35];
    z[41]=z[32] + static_cast<T>(1)+ z[35];
    z[23]=n<T>(1,2)*z[41] + z[23];
    z[23]=z[8]*z[23];
    z[23]= - n<T>(1,2)*z[3] + z[23];
    z[23]=z[23]*z[39];
    z[25]=n<T>(1,4)*z[25] - z[27];
    z[25]=z[3]*z[25];
    z[15]=z[23] + z[15] - n<T>(7,16)*z[28] + z[25];
    z[15]=z[6]*z[15];
    z[23]=z[28]*z[1];
    z[25]= - z[5] + n<T>(7,2)*z[23];
    z[28]=static_cast<T>(15)+ z[30];
    z[28]=z[7]*z[28];
    z[28]=z[28] - z[25];
    z[39]= - n<T>(5,2) + z[1];
    z[39]=z[39]*z[27];
    z[41]=n<T>(67,9) - z[20];
    z[39]=n<T>(1,2)*z[41] + z[39];
    z[39]=z[3]*z[39];
    z[41]=z[12] - n<T>(1,4);
    z[33]= - n<T>(1,2)*z[41] - z[33];
    z[33]=z[8]*z[33];
    z[42]= - z[12] - n<T>(161,9) + z[20];
    z[33]=z[33] + n<T>(7,2)*z[7] + n<T>(1,2)*z[42] + z[5];
    z[33]=z[33]*z[17];
    z[15]=z[15] + z[33] + n<T>(1,8)*z[28] + z[39];
    z[15]=z[6]*z[15];
    z[28]=static_cast<T>(27)+ z[30];
    z[28]=z[28]*z[31];
    z[25]=z[28] - z[25];
    z[25]=z[1]*z[25];
    z[28]= - static_cast<T>(1)+ z[13];
    z[25]=z[25] - n<T>(3,2)*z[7] + n<T>(1,2)*z[28] - z[5];
    z[20]= - n<T>(179,6)*z[1] + n<T>(73,3) - z[20];
    z[20]=n<T>(1,2)*z[20] + n<T>(47,3)*z[53];
    z[20]=z[3]*z[20];
    z[28]=z[8]*z[34];
    z[20]=n<T>(1,8)*z[28] + n<T>(1,4)*z[25] + z[20];
    z[15]=z[18] + n<T>(1,2)*z[20] + z[15];
    z[15]=z[2]*z[15];
    z[18]= - n<T>(179,9) - z[22];
    z[18]=n<T>(1,4)*z[18] + z[27];
    z[18]=z[3]*z[18];
    z[20]=9*z[7] - 7*z[23];
    z[22]=z[8]*z[14];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[8]*z[22];
    z[18]=n<T>(1,4)*z[22] + n<T>(1,8)*z[20] + z[18];
    z[15]=z[15] + n<T>(1,2)*z[18] + z[16];
    z[15]=z[2]*z[15];
    z[16]=z[9] - 1;
    z[18]=z[16]*z[10];
    z[20]=static_cast<T>(1)- z[18];
    z[19]=z[20]*z[19];
    z[20]=z[9]*z[21];
    z[20]=z[20] - z[41];
    z[20]=z[7]*z[20];
    z[21]= - z[7] - z[1];
    z[21]=z[3]*z[21];
    z[19]=z[21] + z[20] + z[19] - static_cast<T>(1)- z[24];
    z[19]=z[8]*z[19];
    z[20]=z[10] + z[1];
    z[20]=z[3]*z[20];
    z[19]=z[19] + z[20] + z[29] + z[32];
    z[19]=z[19]*z[17];
    z[20]= - z[35] - z[29];
    z[21]= - z[12] + z[26];
    z[21]=z[7]*z[21];
    z[14]= - z[14] - n<T>(1,2)*z[10] - z[7];
    z[14]=z[3]*z[14];
    z[14]=z[14] + n<T>(1,2)*z[20] + z[21];
    z[14]=z[8]*z[14];
    z[14]=z[14] + z[40];
    z[14]=z[8]*z[14];
    z[20]= - z[3]*z[36];
    z[20]=z[20] - z[37];
    z[20]=z[8]*z[20];
    z[20]=z[20] + z[38];
    z[20]=z[6]*z[20]*z[17];
    z[14]=z[14] + z[20];
    z[14]=z[6]*z[14];
    z[14]=z[19] + z[14];
    z[14]=z[6]*z[14];
    z[16]= - z[11]*z[16];
    z[16]=static_cast<T>(5)+ z[16];
    z[16]=n<T>(1,2)*z[16] - z[12];
    z[16]=z[1] + n<T>(1,2)*z[16] + z[18];
    z[16]=z[8]*z[16];
    z[18]=n<T>(3,2) - z[12];
    z[16]=n<T>(1,2)*z[18] + z[16];
    z[16]=z[16]*z[17];
    z[14]=z[16] + z[14];

    r += n<T>(1,2)*z[14] + z[15];
 
    return r;
}

template double qg_2lha_r846(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r846(const std::array<dd_real,30>&);
#endif
