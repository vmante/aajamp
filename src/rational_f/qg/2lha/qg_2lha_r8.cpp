#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r8(const std::array<T,30>& k) {
  T z[21];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=n<T>(1,3)*z[1];
    z[8]=npow(z[2],2);
    z[9]=z[7]*z[8];
    z[10]= - z[2] + z[9];
    z[10]=z[1]*z[10];
    z[10]=static_cast<T>(1)+ z[10];
    z[10]=z[1]*z[10];
    z[11]=z[6]*z[5];
    z[12]= - static_cast<T>(2)+ z[5];
    z[12]=z[12]*z[11];
    z[13]=n<T>(1,3) - z[5];
    z[12]=2*z[13] + n<T>(1,3)*z[12];
    z[13]=z[4]*z[6];
    z[12]=z[12]*z[13];
    z[14]=z[5] - n<T>(2,3);
    z[15]=n<T>(1,3) + z[5];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(2,3) + z[15];
    z[15]=z[6]*z[15];
    z[10]=z[12] + z[10] + z[15] - z[14];
    z[10]=z[4]*z[10];
    z[8]=z[8]*z[1];
    z[12]=2*z[2];
    z[15]= - static_cast<T>(5)- z[12];
    z[15]=z[2]*z[15];
    z[15]=z[15] + 2*z[8];
    z[15]=z[1]*z[15];
    z[15]=n<T>(2,3)*z[15] + static_cast<T>(2)+ n<T>(5,3)*z[2];
    z[16]=npow(z[1],2);
    z[15]=z[15]*z[16];
    z[10]=z[15] + z[10];
    z[10]=z[4]*z[10];
    z[15]= - static_cast<T>(1)- z[5];
    z[11]=z[15]*z[11];
    z[11]=z[5] + z[11];
    z[15]=npow(z[5],2);
    z[17]= - z[6]*z[15];
    z[17]=4*z[5] + z[17];
    z[17]=z[17]*z[13];
    z[11]=4*z[11] + z[17];
    z[11]=z[4]*z[11];
    z[17]=4*z[2] - z[8];
    z[7]=z[17]*z[7];
    z[7]= - static_cast<T>(2)+ z[7];
    z[7]=z[7]*z[16];
    z[7]=z[7] + n<T>(1,3)*z[11];
    z[7]=z[4]*z[7];
    z[11]=static_cast<T>(1)+ n<T>(1,3)*z[2];
    z[11]=z[2]*z[11];
    z[9]=z[11] - z[9];
    z[9]=z[1]*z[9];
    z[11]=z[2] + 1;
    z[9]= - n<T>(2,3)*z[11] + z[9];
    z[17]=npow(z[1],3);
    z[9]=z[9]*z[17];
    z[7]=2*z[9] + z[7];
    z[7]=z[4]*z[7];
    z[9]=z[11]*z[12];
    z[9]=z[9] - z[8];
    z[9]=z[1]*z[9];
    z[18]=z[2] + 2;
    z[19]=z[18]*z[2];
    z[9]=z[9] - static_cast<T>(1)- z[19];
    z[9]=z[9]*npow(z[1],4);
    z[7]=n<T>(1,3)*z[9] + z[7];
    z[7]=z[3]*z[7];
    z[9]= - n<T>(5,3) - z[12];
    z[9]=z[2]*z[9];
    z[9]=z[9] + z[8];
    z[9]=z[1]*z[9];
    z[20]=n<T>(5,3) + z[2];
    z[20]=z[2]*z[20];
    z[9]=z[9] + n<T>(2,3) + z[20];
    z[9]=z[9]*z[17];
    z[7]=z[7] + z[9] + z[10];
    z[7]=z[3]*z[7];
    z[9]=z[19] - z[8];
    z[10]=2*z[1];
    z[9]=z[9]*z[10];
    z[9]=z[9] - z[18];
    z[9]=z[1]*z[9];
    z[10]=z[5] - 1;
    z[15]=static_cast<T>(1)- z[15];
    z[15]=z[6]*z[15];
    z[9]=z[9] + z[15] + z[10];
    z[15]=n<T>(1,2)*z[5];
    z[17]=static_cast<T>(2)- z[15];
    z[17]=z[5]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[6]*z[17];
    z[14]=n<T>(1,3)*z[17] + z[14];
    z[14]=z[14]*z[13];
    z[9]=n<T>(1,3)*z[9] + z[14];
    z[9]=z[4]*z[9];
    z[14]=n<T>(2,3) + z[2];
    z[14]=z[14]*z[12];
    z[14]=z[14] - z[8];
    z[14]=z[1]*z[14];
    z[17]= - n<T>(4,3) - z[2];
    z[17]=z[2]*z[17];
    z[14]=z[14] - n<T>(1,2) + z[17];
    z[14]=z[14]*z[16];
    z[7]=z[7] + z[14] + z[9];
    z[7]=z[3]*z[7];
    z[9]= - static_cast<T>(1)- z[12];
    z[9]=z[2]*z[9];
    z[8]=z[9] + z[8];
    z[8]=z[1]*z[8];
    z[9]=z[2]*z[11];
    z[8]=z[8] + n<T>(1,2) + z[9];
    z[8]=z[1]*z[8];
    z[9]= - z[6]*z[10];
    z[9]= - z[15] + z[9];
    z[9]=z[9]*z[13];
    z[8]=z[8] + z[9];
    z[7]=n<T>(1,3)*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[8]=static_cast<T>(1)- z[6];
    z[8]=z[6]*z[8];

    r += z[7] + n<T>(1,6)*z[8];
 
    return r;
}

template double qg_2lha_r8(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r8(const std::array<dd_real,30>&);
#endif
