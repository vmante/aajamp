#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1920(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[11];
    z[5]=k[21];
    z[6]=k[5];
    z[7]=k[9];
    z[8]=k[28];
    z[9]=k[12];
    z[10]=z[5] - z[4];
    z[10]=z[10]*z[2];
    z[11]=z[7]*z[8];
    z[11]=z[10] + z[11];
    z[12]=z[5] - z[8];
    z[13]= - static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[13]=z[7]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[1]*z[12];
    z[12]=z[12] + z[8] - z[11];
    z[12]=z[1]*z[12];
    z[13]=z[7] - 1;
    z[14]= - z[2]*z[9];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[2]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[2]*z[14];
    z[14]=z[14] - z[13];
    z[10]= - z[10] + static_cast<T>(1)+ z[5];
    z[15]= - static_cast<T>(1)- n<T>(1,2)*z[5];
    z[15]=z[7]*z[15];
    z[10]=n<T>(1,2)*z[10] + z[15];
    z[10]=z[1]*z[10];
    z[10]=z[10] + n<T>(1,2)*z[2] + z[13];
    z[10]=z[1]*z[10];
    z[10]=n<T>(1,2)*z[14] + z[10];
    z[10]=z[3]*z[10];
    z[13]=static_cast<T>(1)+ z[8];
    z[13]=z[7]*z[13];
    z[13]= - z[8] + z[13];
    z[10]=z[10] + n<T>(1,2)*z[13] + z[12];
    z[10]=z[3]*z[10];
    z[12]=z[8]*z[6];
    z[13]=n<T>(1,2)*z[12];
    z[14]=z[6] - z[13];
    z[14]=z[7]*z[14];
    z[13]=z[13] + z[14];
    z[13]=z[1]*z[13];
    z[12]= - z[2]*z[12];
    z[12]=z[12] - z[6] + z[5];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[1]*z[12];
    z[10]=z[10] - n<T>(1,2)*z[11] + z[12];

    r += n<T>(3,4)*z[10];
 
    return r;
}

template double qg_2lha_r1920(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1920(const std::array<dd_real,30>&);
#endif
