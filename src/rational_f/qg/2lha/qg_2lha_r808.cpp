#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r808(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[2];
    z[2]=k[3];
    z[3]=k[5];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=z[5] - z[1];
    z[8]=static_cast<T>(1)- z[7];
    z[9]=n<T>(1,2)*z[5];
    z[10]=npow(z[3],2);
    z[8]=z[9]*z[10]*z[8];
    z[11]= - z[10]*z[7];
    z[12]=static_cast<T>(1)+ z[3];
    z[12]=z[3]*z[12];
    z[11]=z[12] + z[11];
    z[9]=z[11]*z[9];
    z[11]=z[10]*z[1];
    z[12]=z[3] - 1;
    z[13]=z[12]*z[3];
    z[9]=z[9] + z[13] - z[11];
    z[9]=z[6]*z[5]*z[9];
    z[14]=z[1]*z[3];
    z[12]=z[12]*z[14];
    z[15]=n<T>(1,2)*z[2];
    z[11]=z[15]*z[11];
    z[11]=z[12] + z[11];
    z[11]=z[2]*z[11];
    z[12]=z[3] - n<T>(1,2);
    z[16]= - z[1]*z[12];
    z[11]=z[16] + z[11];
    z[11]=z[4]*z[11];
    z[14]=z[3] - z[14];
    z[12]=z[12]*z[14];
    z[7]= - z[2] + z[7];
    z[7]=z[10]*z[7];
    z[7]= - z[13] + z[7];
    z[7]=z[7]*z[15];
    z[7]=z[11] + z[9] + z[7] + z[8] + z[12];

    r += n<T>(1,9)*z[7];
 
    return r;
}

template double qg_2lha_r808(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r808(const std::array<dd_real,30>&);
#endif
