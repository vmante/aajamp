#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1752(const std::array<T,30>& k) {
  T z[22];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[24];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[9];
    z[8]=z[7]*z[6];
    z[9]=z[6] + 1;
    z[10]= - z[9]*z[8];
    z[10]=z[6] + z[10];
    z[11]=z[5]*z[7];
    z[12]=z[7]*npow(z[6],2);
    z[12]=5*z[6] - n<T>(3,2)*z[12];
    z[12]=z[12]*z[11];
    z[10]=5*z[10] + z[12];
    z[10]=z[10]*npow(z[5],2);
    z[12]=n<T>(1,2)*z[4];
    z[13]=z[12] + 1;
    z[14]=n<T>(1,2)*z[5];
    z[15]= - z[14] + z[13];
    z[15]=z[2]*z[15];
    z[15]=z[15] + static_cast<T>(4)+ z[4];
    z[15]=z[2]*z[15];
    z[16]=z[1]*npow(z[2],2);
    z[17]=z[12]*z[16];
    z[18]=z[4] + 1;
    z[19]= - z[2]*z[18];
    z[19]= - z[4] + z[19];
    z[19]=z[2]*z[19];
    z[17]=z[19] + z[17];
    z[17]=z[1]*z[17];
    z[15]=z[17] + z[12] + z[15];
    z[15]=z[1]*z[15];
    z[17]= - static_cast<T>(1)+ z[5];
    z[17]=z[2]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[15]=3*z[17] + z[15];
    z[15]=z[1]*z[15];
    z[15]= - 6*z[5] + z[15];
    z[15]=z[15]*npow(z[1],2);
    z[10]=z[10] + z[15];
    z[10]=z[3]*z[10];
    z[15]=z[2]*z[4];
    z[13]= - z[13]*z[15];
    z[17]=npow(z[4],2);
    z[19]=n<T>(1,2)*z[17];
    z[13]= - z[19] + z[13];
    z[13]=z[2]*z[13];
    z[20]=n<T>(1,4)*z[17];
    z[21]=z[20]*z[16];
    z[13]=z[13] + z[21];
    z[13]=z[1]*z[13];
    z[18]=z[4]*z[18];
    z[21]=static_cast<T>(5)+ z[12];
    z[21]=z[4]*z[21];
    z[21]=n<T>(7,2) + z[21];
    z[21]=z[2]*z[21];
    z[18]=z[18] + z[21];
    z[18]=z[2]*z[18];
    z[18]=z[19] + z[18];
    z[13]=n<T>(1,2)*z[18] + z[13];
    z[13]=z[1]*z[13];
    z[18]=n<T>(3,2)*z[4];
    z[14]= - z[18] - static_cast<T>(2)+ z[14];
    z[14]=z[2]*z[14];
    z[14]=z[14] - n<T>(11,2) - z[4];
    z[14]=z[2]*z[14];
    z[13]=z[13] + z[12] + z[14];
    z[13]=z[1]*z[13];
    z[14]=n<T>(1,2)*z[2];
    z[19]=static_cast<T>(9)- 5*z[5];
    z[19]=z[19]*z[14];
    z[13]=z[13] + static_cast<T>(2)+ z[19];
    z[13]=z[1]*z[13];
    z[13]=n<T>(7,2)*z[5] + z[13];
    z[13]=z[1]*z[13];
    z[19]= - static_cast<T>(1)+ n<T>(1,4)*z[6];
    z[8]=z[19]*z[8];
    z[8]=3*z[8] + static_cast<T>(2)- n<T>(9,2)*z[6];
    z[8]=z[8]*z[11];
    z[19]=z[6] - 1;
    z[21]=z[6]*z[19];
    z[21]= - static_cast<T>(2)+ z[21];
    z[21]=z[7]*z[21];
    z[8]=z[8] + z[21] + static_cast<T>(2)- z[6];
    z[8]=z[5]*z[8];
    z[8]=z[10] + z[8] + z[13];
    z[8]=z[3]*z[8];
    z[10]=n<T>(3,2) - z[4];
    z[10]=z[10]*z[12];
    z[13]= - static_cast<T>(2)- n<T>(3,4)*z[4];
    z[13]=z[4]*z[13];
    z[13]= - n<T>(3,4) + z[13];
    z[13]=z[2]*z[13];
    z[10]=z[10] + z[13];
    z[10]=z[2]*z[10];
    z[13]=n<T>(1,2) + z[4];
    z[13]=z[4]*z[13];
    z[21]= - z[1]*z[20];
    z[13]=z[13] + z[21];
    z[13]=z[13]*z[16];
    z[10]=z[13] + z[20] + z[10];
    z[10]=z[1]*z[10];
    z[13]=static_cast<T>(7)- z[4];
    z[16]=z[18] + 1;
    z[18]=z[2]*z[16];
    z[13]=n<T>(1,4)*z[13] + z[18];
    z[13]=z[2]*z[13];
    z[10]=z[10] + n<T>(1,4)*z[4] + z[13];
    z[10]=z[1]*z[10];
    z[13]= - n<T>(1,2) - 3*z[2];
    z[10]=n<T>(1,2)*z[13] + z[10];
    z[10]=z[1]*z[10];
    z[13]=n<T>(1,2)*z[6];
    z[18]=z[13] + 1;
    z[18]=z[18]*z[6];
    z[18]=z[18] + n<T>(1,2);
    z[18]=z[18]*z[7];
    z[9]=z[18] - n<T>(1,2)*z[9];
    z[18]=z[7]*z[19];
    z[13]=z[13] + z[18];
    z[11]=z[13]*z[11];
    z[8]=z[8] + z[10] + n<T>(3,2)*z[11] - z[9];
    z[8]=z[3]*z[8];
    z[10]= - static_cast<T>(1)+ z[4];
    z[10]=z[10]*z[12];
    z[11]=z[16]*z[15];
    z[12]=n<T>(1,2) - z[2];
    z[12]=z[1]*z[17]*z[12];
    z[10]=z[12] + z[10] + z[11];
    z[10]=z[1]*z[2]*z[10];
    z[11]= - z[4]*z[5];
    z[11]= - static_cast<T>(3)+ z[11];
    z[11]=z[11]*z[15];
    z[12]= - z[5]*z[17];
    z[11]=z[12] + z[11];
    z[11]=z[11]*z[14];
    z[10]=z[10] - z[4] + z[11];
    z[11]=n<T>(1,2)*z[1];
    z[10]=z[10]*z[11];
    z[9]=z[4]*z[9];
    z[11]= - static_cast<T>(1)+ z[7];
    z[11]=z[7]*z[11];

    r += z[8] + z[9] + z[10] + n<T>(3,4)*z[11];
 
    return r;
}

template double qg_2lha_r1752(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1752(const std::array<dd_real,30>&);
#endif
