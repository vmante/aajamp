#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2384(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[24];
    z[5]=k[4];
    z[6]=k[3];
    z[7]=k[11];
    z[8]=n<T>(3,2)*z[3];
    z[9]=z[8] - 1;
    z[10]=npow(z[3],2);
    z[11]=z[9]*z[10];
    z[12]=n<T>(1,2)*z[5];
    z[13]=npow(z[3],3);
    z[14]=z[13]*z[12];
    z[11]=z[11] + z[14];
    z[11]=z[5]*z[11];
    z[14]= - static_cast<T>(2)+ z[8];
    z[14]=z[3]*z[14];
    z[14]=n<T>(1,2) + z[14];
    z[14]=z[3]*z[14];
    z[11]=z[14] + z[11];
    z[11]=z[5]*z[11];
    z[14]=n<T>(1,2)*z[3];
    z[15]=z[14] - 1;
    z[15]=z[15]*z[3];
    z[15]=z[15] + n<T>(1,2);
    z[16]=z[3]*z[15];
    z[11]=z[16] + z[11];
    z[11]=z[5]*z[11];
    z[16]=z[3] - 1;
    z[17]=z[16]*z[3];
    z[18]=n<T>(1,2) + 3*z[17];
    z[19]=n<T>(1,2)*z[10];
    z[20]=z[18]*z[19];
    z[21]=z[3] - n<T>(1,2);
    z[22]=z[21]*z[13];
    z[23]=npow(z[3],4);
    z[24]=z[23]*z[5];
    z[22]=z[22] + n<T>(1,4)*z[24];
    z[22]=z[5]*z[22];
    z[20]=z[20] + z[22];
    z[20]=z[5]*z[20];
    z[22]=z[3] - n<T>(3,2);
    z[25]=z[22]*z[3];
    z[26]=z[25] + n<T>(1,2);
    z[27]=z[26]*z[10];
    z[20]=z[27] + z[20];
    z[20]=z[5]*z[20];
    z[27]=z[15]*z[19];
    z[20]=z[27] + z[20];
    z[20]=z[6]*z[20];
    z[11]=z[20] + n<T>(1,2) + z[11];
    z[11]=z[6]*z[11];
    z[18]=z[18]*z[14];
    z[20]=z[21]*z[10];
    z[27]=z[13]*z[5];
    z[20]=z[20] + n<T>(1,4)*z[27];
    z[20]=z[5]*z[20];
    z[18]=z[18] + z[20];
    z[18]=z[5]*z[18];
    z[20]=z[3]*z[26];
    z[18]=z[20] + z[18];
    z[18]=z[5]*z[18];
    z[20]=z[15]*z[14];
    z[18]=z[20] + z[18];
    z[18]=z[6]*z[18];
    z[20]=z[3] - 3;
    z[28]= - z[20]*z[14];
    z[29]=z[12]*z[10];
    z[30]=z[29] + z[17];
    z[30]=z[30]*z[5];
    z[31]=n<T>(1,2)*z[16];
    z[32]= - z[31] + z[30];
    z[32]=z[5]*z[32];
    z[33]=z[3] - 2;
    z[33]=z[33]*z[3];
    z[32]=z[32] - n<T>(1,2) - z[33];
    z[32]=z[5]*z[32];
    z[18]=z[18] + z[32] - static_cast<T>(1)+ z[28];
    z[18]=z[6]*z[18];
    z[28]=z[14]*z[5];
    z[28]=z[28] + z[21];
    z[28]=z[28]*z[5];
    z[32]=z[28] + z[21];
    z[32]=z[5]*z[32];
    z[21]=z[32] + z[21];
    z[21]=z[5]*z[21];
    z[21]=z[31] + z[21];
    z[18]=n<T>(1,2)*z[21] + z[18];
    z[18]=z[4]*z[18];
    z[21]=static_cast<T>(1)+ z[30];
    z[21]=z[5]*z[21];
    z[30]=z[20]*z[3];
    z[21]=z[21] - static_cast<T>(1)- z[30];
    z[21]=z[21]*z[12];
    z[32]=n<T>(1,4)*z[3];
    z[34]=z[32] - 1;
    z[34]=z[34]*z[3];
    z[35]=z[34] + n<T>(1,2);
    z[11]=z[18] + z[11] + z[21] - z[35];
    z[11]=z[4]*z[11];
    z[8]=z[8] - 5;
    z[14]=z[8]*z[14];
    z[14]=z[14] + 3;
    z[14]=z[14]*z[3];
    z[14]=z[14] - n<T>(3,2);
    z[14]=z[14]*z[3];
    z[14]=z[14] + n<T>(1,4);
    z[18]=z[10]*z[5];
    z[21]=static_cast<T>(1)+ z[33];
    z[21]=z[21]*z[18];
    z[33]= - z[5]*z[14];
    z[34]=z[34] + n<T>(3,2);
    z[34]=z[34]*z[3];
    z[34]=z[34] - 1;
    z[34]=z[34]*z[3];
    z[34]=z[34] + n<T>(1,4);
    z[36]=3*z[34];
    z[33]= - z[36] + z[33];
    z[33]=z[2]*z[33];
    z[21]=z[33] + z[21] + z[14];
    z[21]=z[2]*z[21];
    z[33]=n<T>(1,2)*z[4];
    z[37]= - static_cast<T>(1)+ z[5];
    z[37]=z[37]*z[33];
    z[37]=static_cast<T>(1)+ z[37];
    z[37]=z[37]*z[33];
    z[38]=z[34]*npow(z[2],2);
    z[39]=npow(z[4],2);
    z[38]=z[38] - n<T>(1,4)*z[39];
    z[38]=z[1]*z[38];
    z[21]=z[38] + z[21] + z[37];
    z[21]=z[1]*z[21];
    z[37]= - n<T>(7,2) + 2*z[3];
    z[37]=z[37]*z[3];
    z[37]=z[37] + n<T>(3,2);
    z[38]=z[37]*z[10];
    z[9]=z[9]*z[27];
    z[9]=z[38] + z[9];
    z[9]=z[5]*z[9];
    z[37]= - z[37]*z[18];
    z[39]=n<T>(5,2) - z[3];
    z[39]=z[3]*z[39];
    z[39]= - n<T>(29,2) + 7*z[39];
    z[39]=z[3]*z[39];
    z[39]=n<T>(9,2) + z[39];
    z[39]=z[3]*z[39];
    z[39]= - n<T>(1,2) + z[39];
    z[37]=n<T>(1,2)*z[39] + z[37];
    z[37]=z[5]*z[37];
    z[8]=z[8]*z[3];
    z[8]=z[8] + 6;
    z[8]=z[8]*z[3];
    z[8]=z[8] - 3;
    z[39]= - static_cast<T>(2)+ n<T>(3,4)*z[3];
    z[39]=z[39]*z[3];
    z[39]=z[39] + n<T>(7,4);
    z[39]=z[39]*z[3];
    z[39]=z[39] - n<T>(1,2);
    z[40]=z[5]*z[39];
    z[40]=z[40] + z[8];
    z[40]=z[3]*z[40];
    z[40]=n<T>(1,2) + z[40];
    z[40]=z[5]*z[40];
    z[36]=z[36] + z[40];
    z[36]=z[2]*z[36];
    z[8]= - z[3]*z[8];
    z[8]=z[36] + z[37] - n<T>(1,2) + z[8];
    z[8]=z[2]*z[8];
    z[36]=npow(z[5],2);
    z[37]= - static_cast<T>(1)- z[36];
    z[37]=n<T>(1,2)*z[37] - z[6];
    z[37]=z[4]*z[37];
    z[37]= - z[5] + z[37];
    z[33]=z[37]*z[33];
    z[30]=z[30] + n<T>(13,4);
    z[30]=z[30]*z[3];
    z[30]=z[30] - n<T>(3,2);
    z[30]=z[30]*z[3];
    z[8]=z[21] + z[33] + z[8] + z[30] + z[9];
    z[8]=z[1]*z[8];
    z[9]= - static_cast<T>(23)+ 11*z[3];
    z[9]=z[3]*z[9];
    z[9]=static_cast<T>(15)+ z[9];
    z[9]=z[3]*z[9];
    z[9]= - static_cast<T>(3)+ z[9];
    z[9]=z[9]*z[32];
    z[21]=z[26]*z[18];
    z[9]=z[9] + z[21];
    z[9]=z[5]*z[9];
    z[21]= - n<T>(27,2) + 5*z[3];
    z[21]=z[3]*z[21];
    z[21]=n<T>(25,2) + z[21];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(9,2) + z[21];
    z[21]=z[3]*z[21];
    z[21]=n<T>(1,2) + z[21];
    z[9]=n<T>(1,2)*z[21] + z[9];
    z[9]=z[5]*z[9];
    z[15]= - z[15]*z[29];
    z[21]=z[39]*z[3];
    z[15]= - z[21] + z[15];
    z[15]=z[5]*z[15];
    z[15]=z[15] - z[14];
    z[15]=z[5]*z[15];
    z[15]=z[15] - z[34];
    z[15]=z[2]*z[15];
    z[9]=z[15] + z[9] + z[14];
    z[9]=z[2]*z[9];
    z[14]= - z[22]*z[10];
    z[14]=z[14] - n<T>(5,2)*z[27];
    z[14]=z[14]*z[36];
    z[15]=n<T>(1,2)*z[13] - z[24];
    z[22]=z[36]*z[6];
    z[15]=z[15]*z[22];
    z[14]=n<T>(1,2)*z[14] + z[15];
    z[14]=z[6]*z[14];
    z[15]= - z[20]*z[10];
    z[15]=z[15] - n<T>(3,2)*z[27];
    z[15]=z[5]*z[15];
    z[15]=z[25] + z[15];
    z[15]=z[15]*z[36];
    z[20]= - z[23]*z[12];
    z[20]=z[13] + z[20];
    z[20]=z[5]*z[20];
    z[19]= - z[19] + z[20];
    z[19]=z[19]*z[22];
    z[15]=z[15] + z[19];
    z[15]=z[6]*z[15];
    z[19]= - z[25] - n<T>(3,4)*z[18];
    z[19]=z[5]*z[19];
    z[19]=z[19] - z[35];
    z[19]=z[19]*z[36];
    z[15]=z[19] + n<T>(1,2)*z[15];
    z[15]=z[6]*z[15];
    z[19]= - z[31] - z[28];
    z[19]=n<T>(1,4)*z[1] + n<T>(1,2)*z[19];
    z[19]=z[36]*z[19];
    z[15]=z[15] + z[19];
    z[15]=z[7]*z[15];
    z[17]= - z[18] - static_cast<T>(1)- z[17];
    z[17]=z[17]*z[36];
    z[14]=z[15] + n<T>(1,4)*z[17] + z[14];
    z[14]=z[7]*z[14];
    z[15]= - n<T>(9,2) - 7*z[25];
    z[10]=z[15]*z[10];
    z[15]=z[16]*z[27];
    z[10]=z[10] - 3*z[15];
    z[10]=z[10]*z[12];
    z[12]=n<T>(9,4) - z[3];
    z[12]=z[3]*z[12];
    z[12]= - n<T>(7,4) + z[12];
    z[12]=z[3]*z[12];
    z[12]=n<T>(7,4) + 3*z[12];
    z[12]=z[3]*z[12];
    z[10]=z[12] + z[10];
    z[10]=z[5]*z[10];
    z[12]=z[16]*z[13];
    z[12]=n<T>(3,2)*z[12] + z[24];
    z[12]=z[5]*z[12];
    z[12]=z[38] + z[12];
    z[12]=z[5]*z[12];
    z[12]=z[21] + z[12];
    z[12]=z[6]*z[12];

    r += z[8] + z[9] + z[10] + z[11] + z[12] + z[14] - z[30];
 
    return r;
}

template double qg_2lha_r2384(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2384(const std::array<dd_real,30>&);
#endif
