#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1572(const std::array<T,30>& k) {
  T z[56];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[9];
    z[9]=k[3];
    z[10]=k[11];
    z[11]=npow(z[8],2);
    z[12]=n<T>(5,2)*z[8];
    z[13]=z[12] - 3;
    z[13]=z[13]*z[8];
    z[14]= - static_cast<T>(1)- z[13];
    z[14]=z[14]*z[11];
    z[15]=5*z[8];
    z[16]=z[15] - 3;
    z[17]=npow(z[8],3);
    z[16]=z[16]*z[17];
    z[18]=npow(z[8],4);
    z[19]=z[18]*z[5];
    z[20]= - z[16] - z[19];
    z[21]=n<T>(1,4)*z[5];
    z[20]=z[20]*z[21];
    z[14]=z[14] + z[20];
    z[14]=z[5]*z[14];
    z[20]=n<T>(1,2)*z[8];
    z[22]=static_cast<T>(9)- z[15];
    z[22]=z[22]*z[20];
    z[22]= - static_cast<T>(3)+ z[22];
    z[22]=z[8]*z[22];
    z[22]=n<T>(3,4) + z[22];
    z[22]=z[8]*z[22];
    z[14]=z[22] + z[14];
    z[14]=z[5]*z[14];
    z[22]=n<T>(5,4)*z[8];
    z[23]=static_cast<T>(3)- z[22];
    z[23]=z[8]*z[23];
    z[23]= - static_cast<T>(3)+ z[23];
    z[23]=z[8]*z[23];
    z[23]=n<T>(3,2) + z[23];
    z[23]=z[8]*z[23];
    z[14]=z[23] + z[14];
    z[14]=z[5]*z[14];
    z[23]=z[8] - 3;
    z[24]=n<T>(1,4)*z[8];
    z[25]= - z[23]*z[24];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[8]*z[25];
    z[25]=n<T>(3,4) + z[25];
    z[25]=z[8]*z[25];
    z[14]=z[14] - n<T>(1,4) + z[25];
    z[14]=z[5]*z[14];
    z[25]=z[20] - 1;
    z[26]=z[25]*z[8];
    z[26]=z[26] + n<T>(1,2);
    z[26]=z[26]*z[11];
    z[27]=5*z[26];
    z[28]=z[8] - 1;
    z[29]=z[28]*z[17];
    z[30]= - 5*z[29] - z[19];
    z[30]=z[30]*z[21];
    z[30]= - z[27] + z[30];
    z[30]=z[5]*z[30];
    z[31]=z[23]*z[8];
    z[31]=z[31] + 3;
    z[31]=z[31]*z[8];
    z[31]=z[31] - 1;
    z[32]= - z[31]*z[12];
    z[30]=z[32] + z[30];
    z[30]=z[5]*z[30];
    z[32]=z[24] - 1;
    z[33]=z[32]*z[8];
    z[33]=z[33] + n<T>(3,2);
    z[33]=z[33]*z[8];
    z[33]=z[33] - 1;
    z[34]= - z[33]*z[15];
    z[30]=z[30] - static_cast<T>(1)+ z[34];
    z[30]=z[5]*z[30];
    z[34]=z[8] - 5;
    z[35]=z[34]*z[20];
    z[35]=z[35] + 5;
    z[35]=z[35]*z[8];
    z[36]=z[35] - 5;
    z[36]=z[36]*z[20];
    z[36]=z[36] + 1;
    z[30]=z[30] - z[36];
    z[37]=npow(z[5],2);
    z[38]=z[37]*z[7];
    z[30]=z[30]*z[38];
    z[14]=z[14] + z[30];
    z[14]=z[6]*z[14];
    z[25]=z[25]*z[11];
    z[23]= - z[23]*z[17];
    z[23]=z[23] - z[19];
    z[23]=z[23]*z[21];
    z[23]=z[25] + z[23];
    z[23]=z[5]*z[23];
    z[25]=z[8] - n<T>(3,2);
    z[30]= - z[25]*z[20];
    z[23]=z[30] + z[23];
    z[23]=z[5]*z[23];
    z[30]= - z[19] + 3*z[17];
    z[30]=z[30]*z[5];
    z[39]=3*z[11];
    z[30]=z[30] - z[39];
    z[30]=z[30]*z[5];
    z[30]=z[30] + z[8];
    z[40]=z[9]*z[30];
    z[28]=z[28] - z[40];
    z[23]=z[23] + n<T>(1,4)*z[28];
    z[23]=z[37]*z[23];
    z[28]=n<T>(1,2)*z[5];
    z[18]=z[18]*z[28];
    z[40]=z[29] + z[18];
    z[40]=z[5]*z[40];
    z[40]=z[26] + z[40];
    z[41]=n<T>(1,2)*z[4];
    z[42]=npow(z[5],5);
    z[40]=z[40]*z[42]*z[41];
    z[23]=z[40] + z[23];
    z[23]=z[10]*z[23];
    z[14]=z[14] + z[23];
    z[23]=static_cast<T>(209)- 63*z[8];
    z[23]=z[8]*z[23];
    z[23]= - static_cast<T>(249)+ z[23];
    z[23]=z[8]*z[23];
    z[23]=static_cast<T>(123)+ z[23];
    z[23]=z[23]*z[24];
    z[23]= - static_cast<T>(5)+ z[23];
    z[40]=static_cast<T>(17)- n<T>(15,2)*z[8];
    z[40]=z[8]*z[40];
    z[40]= - n<T>(23,2) + z[40];
    z[40]=z[40]*z[20];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[5]*z[8]*z[40];
    z[23]=n<T>(1,2)*z[23] + z[40];
    z[23]=z[5]*z[23];
    z[40]=static_cast<T>(26)- n<T>(49,8)*z[8];
    z[40]=z[8]*z[40];
    z[40]= - n<T>(171,4) + z[40];
    z[40]=z[8]*z[40];
    z[40]=n<T>(67,2) + z[40];
    z[40]=z[8]*z[40];
    z[23]=z[23] - static_cast<T>(8)+ z[40];
    z[23]=z[5]*z[23];
    z[40]=z[31]*z[8];
    z[43]=n<T>(3,2)*z[5];
    z[44]=z[43]*z[40];
    z[33]=z[33]*z[8];
    z[33]=z[33] + n<T>(1,4);
    z[44]=13*z[33] + z[44];
    z[44]=z[5]*z[44];
    z[44]=n<T>(21,2)*z[36] + z[44];
    z[44]=z[5]*z[44];
    z[45]=z[20] - 3;
    z[45]=z[45]*z[8];
    z[45]=z[45] + n<T>(15,2);
    z[46]=z[45]*z[20];
    z[46]=z[46] - 5;
    z[46]=z[46]*z[8];
    z[46]=z[46] + n<T>(5,2);
    z[44]=3*z[46] + z[44];
    z[44]=z[3]*z[44];
    z[47]= - static_cast<T>(23)+ n<T>(9,2)*z[8];
    z[47]=z[47]*z[8];
    z[47]=z[47] + n<T>(95,2);
    z[47]=z[47]*z[20];
    z[47]=z[47] - 25;
    z[47]=z[47]*z[8];
    z[48]= - n<T>(13,2) - z[47];
    z[23]=z[44] + n<T>(3,4)*z[48] + z[23];
    z[23]=z[3]*z[23];
    z[44]=15*z[8];
    z[48]= - n<T>(7,2) + z[8];
    z[48]=z[48]*z[44];
    z[48]=n<T>(137,2) + z[48];
    z[48]=z[8]*z[48];
    z[48]= - n<T>(81,2) + z[48];
    z[48]=z[8]*z[48];
    z[48]=n<T>(7,2) + z[48];
    z[25]=z[25]*z[8];
    z[25]=z[25] + n<T>(1,2);
    z[25]=z[25]*z[11];
    z[49]=z[28]*z[25];
    z[50]= - n<T>(21,8) + z[8];
    z[50]=z[8]*z[50];
    z[50]=n<T>(9,4) + z[50];
    z[50]=z[8]*z[50];
    z[50]= - n<T>(5,8) + z[50];
    z[50]=z[8]*z[50];
    z[49]=z[50] + z[49];
    z[50]=5*z[5];
    z[49]=z[49]*z[50];
    z[48]=n<T>(1,4)*z[48] + z[49];
    z[48]=z[5]*z[48];
    z[49]=z[8] - n<T>(17,4);
    z[49]=z[49]*z[8];
    z[49]=z[49] + 7;
    z[49]=z[49]*z[8];
    z[49]=z[49] - n<T>(11,2);
    z[49]=z[49]*z[8];
    z[23]=z[23] + z[48] + n<T>(1,4) + z[49];
    z[23]=z[3]*z[23];
    z[48]=n<T>(1,4) + z[5];
    z[23]=n<T>(1,4)*z[48] + z[23];
    z[23]=z[3]*z[23];
    z[48]= - static_cast<T>(55)+ 13*z[8];
    z[48]=z[48]*z[20];
    z[48]=static_cast<T>(45)+ z[48];
    z[48]=z[8]*z[48];
    z[48]= - static_cast<T>(35)+ z[48];
    z[48]=z[8]*z[48];
    z[31]=z[31]*z[15];
    z[51]=z[5]*z[31];
    z[48]=z[51] + n<T>(11,2) + z[48];
    z[48]=z[5]*z[48];
    z[48]=z[48] + n<T>(5,2) + z[47];
    z[51]=static_cast<T>(4)- z[8];
    z[51]=z[8]*z[51];
    z[51]= - static_cast<T>(6)+ z[51];
    z[51]=z[8]*z[51];
    z[51]=static_cast<T>(4)+ z[51];
    z[51]=z[8]*z[51];
    z[51]= - static_cast<T>(1)+ z[51];
    z[51]=z[5]*z[51];
    z[51]= - n<T>(11,2)*z[36] + z[51];
    z[51]=z[5]*z[51];
    z[45]=z[45]*z[8];
    z[45]=z[45] - 10;
    z[45]=z[45]*z[8];
    z[45]=z[45] + 5;
    z[51]=z[51] - z[45];
    z[51]=z[3]*z[51];
    z[48]=n<T>(1,4)*z[48] + z[51];
    z[48]=z[3]*z[48];
    z[51]=z[43] + 1;
    z[52]=n<T>(1,4)*z[51];
    z[48]=z[52] + z[48];
    z[48]=z[3]*z[48];
    z[48]=n<T>(1,16) + z[48];
    z[48]=z[3]*z[48];
    z[46]=n<T>(1,2)*z[46];
    z[53]=z[5]*z[36];
    z[53]=z[46] + z[53];
    z[53]=z[3]*z[53];
    z[54]=z[28] + 1;
    z[53]=n<T>(1,2)*z[54] + z[53];
    z[53]=z[3]*z[53];
    z[53]=n<T>(1,8) + z[53];
    z[55]=npow(z[3],2);
    z[53]=z[2]*z[53]*z[55];
    z[48]=z[48] + z[53];
    z[48]=z[2]*z[48];
    z[23]=z[23] + z[48];
    z[23]=z[2]*z[23];
    z[48]= - static_cast<T>(1)- n<T>(5,4)*z[5];
    z[48]=z[3]*z[48];
    z[48]=z[48] + z[51];
    z[48]=z[3]*z[48];
    z[48]= - z[52] + z[48];
    z[41]=z[48]*z[55]*z[41];
    z[43]=static_cast<T>(1)- z[43];
    z[48]=static_cast<T>(2)+ n<T>(11,8)*z[5];
    z[48]=z[3]*z[48];
    z[43]=n<T>(1,4)*z[43] + z[48];
    z[43]=z[3]*z[43];
    z[43]= - n<T>(1,8) + z[43];
    z[43]=z[43]*z[55];
    z[48]=npow(z[3],3);
    z[52]= - z[3]*z[54];
    z[52]= - n<T>(1,2) + z[52];
    z[52]=z[2]*z[52]*z[48];
    z[43]=z[43] + n<T>(1,2)*z[52];
    z[43]=z[2]*z[43];
    z[52]=n<T>(1,2) + z[5];
    z[53]= - static_cast<T>(1)- n<T>(7,8)*z[5];
    z[53]=z[3]*z[53];
    z[52]=n<T>(1,2)*z[52] + z[53];
    z[52]=z[3]*z[52];
    z[53]=n<T>(1,2) - z[5];
    z[52]=n<T>(1,4)*z[53] + 3*z[52];
    z[52]=z[52]*z[55];
    z[43]=z[52] + z[43];
    z[43]=z[2]*z[43];
    z[52]= - n<T>(1,4) + z[3];
    z[52]=z[52]*z[48];
    z[53]=n<T>(1,8) - z[3];
    z[53]=z[53]*z[48];
    z[54]=z[2]*npow(z[3],4);
    z[53]=z[53] + n<T>(1,4)*z[54];
    z[53]=z[2]*z[53];
    z[52]=n<T>(3,2)*z[52] + z[53];
    z[52]=z[2]*z[52];
    z[53]= - n<T>(1,2) + z[3];
    z[53]=z[4]*z[53];
    z[53]=n<T>(1,4)*z[53] + n<T>(3,8) - z[3];
    z[48]=z[48]*z[53];
    z[48]=z[52] + z[48];
    z[48]=z[1]*z[48];
    z[52]=static_cast<T>(2)+ n<T>(17,8)*z[5];
    z[52]=z[3]*z[52];
    z[51]= - n<T>(5,4)*z[51] + z[52];
    z[51]=z[3]*z[51];
    z[52]=static_cast<T>(1)+ n<T>(7,2)*z[5];
    z[51]=n<T>(1,8)*z[52] + z[51];
    z[51]=z[3]*z[51];
    z[52]=n<T>(1,16)*z[5];
    z[51]= - z[52] + z[51];
    z[51]=z[3]*z[51];
    z[53]=z[6]*z[38];
    z[41]=n<T>(1,2)*z[48] + n<T>(1,16)*z[53] + z[43] + z[51] + z[41];
    z[41]=z[1]*z[41];
    z[43]= - static_cast<T>(251)+ 59*z[8];
    z[43]=z[43]*z[20];
    z[43]=static_cast<T>(207)+ z[43];
    z[43]=z[8]*z[43];
    z[43]= - static_cast<T>(163)+ z[43];
    z[43]=z[8]*z[43];
    z[43]=n<T>(87,2) + z[43];
    z[44]= - static_cast<T>(23)+ z[44];
    z[44]=z[44]*z[24];
    z[44]=static_cast<T>(2)+ z[44];
    z[48]=z[11]*z[5];
    z[44]=z[44]*z[48];
    z[51]= - static_cast<T>(17)+ n<T>(27,4)*z[8];
    z[51]=z[8]*z[51];
    z[51]=n<T>(55,4) + z[51];
    z[51]=z[8]*z[51];
    z[51]= - n<T>(7,2) + z[51];
    z[51]=z[8]*z[51];
    z[44]=n<T>(3,2)*z[51] + z[44];
    z[44]=z[5]*z[44];
    z[51]= - n<T>(339,4) + 25*z[8];
    z[51]=z[8]*z[51];
    z[51]=n<T>(417,4) + z[51];
    z[51]=z[8]*z[51];
    z[51]= - n<T>(217,4) + z[51];
    z[51]=z[8]*z[51];
    z[51]=n<T>(39,4) + z[51];
    z[44]=n<T>(1,2)*z[51] + z[44];
    z[44]=z[5]*z[44];
    z[43]=n<T>(1,4)*z[43] + z[44];
    z[43]=z[5]*z[43];
    z[44]=static_cast<T>(2)- z[8];
    z[44]=z[8]*z[44];
    z[44]= - static_cast<T>(1)+ z[44];
    z[44]=z[44]*z[48];
    z[40]= - n<T>(11,4)*z[40] + z[44];
    z[40]=z[5]*z[40];
    z[44]=static_cast<T>(2)- z[20];
    z[44]=z[8]*z[44];
    z[44]= - static_cast<T>(3)+ z[44];
    z[44]=z[8]*z[44];
    z[44]=static_cast<T>(2)+ z[44];
    z[44]=z[8]*z[44];
    z[44]= - n<T>(1,2) + z[44];
    z[40]=7*z[44] + z[40];
    z[40]=z[5]*z[40];
    z[40]= - n<T>(17,2)*z[36] + z[40];
    z[40]=z[5]*z[40];
    z[40]=z[40] - z[45];
    z[40]=z[3]*z[40];
    z[44]=n<T>(47,2) + 3*z[47];
    z[40]=z[40] + n<T>(1,4)*z[44] + z[43];
    z[40]=z[3]*z[40];
    z[43]=n<T>(3,8)*z[8];
    z[44]=35*z[8];
    z[45]=static_cast<T>(71)- z[44];
    z[45]=z[45]*z[43];
    z[45]= - static_cast<T>(16)+ z[45];
    z[45]=z[8]*z[45];
    z[45]=n<T>(5,2) + z[45];
    z[45]=z[8]*z[45];
    z[51]=n<T>(21,4) - z[15];
    z[51]=z[8]*z[51];
    z[51]= - static_cast<T>(1)+ z[51];
    z[48]=z[51]*z[48];
    z[45]=z[45] + z[48];
    z[45]=z[5]*z[45];
    z[48]=static_cast<T>(87)- n<T>(125,4)*z[8];
    z[48]=z[8]*z[48];
    z[48]= - n<T>(337,4) + z[48];
    z[48]=z[8]*z[48];
    z[48]=n<T>(65,2) + z[48];
    z[48]=z[48]*z[20];
    z[45]=z[45] - static_cast<T>(2)+ z[48];
    z[45]=z[5]*z[45];
    z[48]=static_cast<T>(503)- 143*z[8];
    z[48]=z[48]*z[20];
    z[48]= - static_cast<T>(329)+ z[48];
    z[48]=z[8]*z[48];
    z[48]=static_cast<T>(193)+ z[48];
    z[48]=z[8]*z[48];
    z[48]= - static_cast<T>(29)+ z[48];
    z[45]=n<T>(1,8)*z[48] + z[45];
    z[45]=z[5]*z[45];
    z[48]=n<T>(17,2) - 2*z[8];
    z[48]=z[8]*z[48];
    z[48]= - static_cast<T>(14)+ z[48];
    z[48]=z[8]*z[48];
    z[48]=static_cast<T>(11)+ z[48];
    z[48]=z[8]*z[48];
    z[40]=z[40] + z[45] - n<T>(7,4) + z[48];
    z[40]=z[3]*z[40];
    z[45]=z[8] - n<T>(1,2);
    z[48]=z[17]*z[5];
    z[51]=z[45]*z[48];
    z[25]=n<T>(5,2)*z[25] + z[51];
    z[25]=z[25]*z[50];
    z[50]=n<T>(23,2)*z[8];
    z[51]= - static_cast<T>(25)+ z[50];
    z[51]=z[51]*z[15];
    z[51]=n<T>(177,2) + z[51];
    z[51]=z[8]*z[51];
    z[51]= - static_cast<T>(21)+ z[51];
    z[51]=z[51]*z[24];
    z[25]=z[51] + z[25];
    z[25]=z[25]*z[28];
    z[51]= - n<T>(45,4) + 4*z[8];
    z[51]=z[8]*z[51];
    z[51]=n<T>(179,16) + z[51];
    z[51]=z[8]*z[51];
    z[51]= - n<T>(75,16) + z[51];
    z[51]=z[8]*z[51];
    z[25]=z[25] + n<T>(5,16) + z[51];
    z[25]=z[5]*z[25];
    z[51]= - static_cast<T>(3)+ n<T>(7,8)*z[8];
    z[51]=z[51]*z[8];
    z[51]=z[51] + n<T>(15,4);
    z[51]=z[51]*z[8];
    z[51]=z[51] - 2;
    z[51]=z[51]*z[8];
    z[25]=z[40] + z[25] + n<T>(1,16) + z[51];
    z[25]=z[3]*z[25];
    z[22]=z[22] - 1;
    z[40]=z[22]*z[48];
    z[53]=n<T>(1,4)*z[11];
    z[54]=static_cast<T>(41)- n<T>(49,2)*z[8];
    z[54]=z[8]*z[54];
    z[54]= - n<T>(33,2) + z[54];
    z[53]=z[54]*z[53];
    z[53]=z[53] - z[40];
    z[53]=z[53]*z[28];
    z[54]=n<T>(61,4) - 6*z[8];
    z[54]=z[8]*z[54];
    z[54]= - n<T>(25,2) + z[54];
    z[54]=z[8]*z[54];
    z[54]=n<T>(13,4) + z[54];
    z[54]=z[8]*z[54];
    z[53]=z[54] + z[53];
    z[53]=z[5]*z[53];
    z[54]=static_cast<T>(20)- n<T>(47,8)*z[8];
    z[54]=z[8]*z[54];
    z[54]= - n<T>(99,4) + z[54];
    z[54]=z[8]*z[54];
    z[54]=static_cast<T>(13)+ z[54];
    z[54]=z[8]*z[54];
    z[53]=z[53] - n<T>(19,8) + z[54];
    z[53]=z[5]*z[53];
    z[50]=static_cast<T>(49)- z[50];
    z[50]=z[8]*z[50];
    z[50]= - static_cast<T>(81)+ z[50];
    z[50]=z[50]*z[24];
    z[50]=static_cast<T>(16)+ z[50];
    z[50]=z[8]*z[50];
    z[50]=z[53] - n<T>(9,2) + z[50];
    z[50]=z[5]*z[50];
    z[53]=z[28]*z[29];
    z[27]=z[27] + z[53];
    z[27]=z[5]*z[27];
    z[27]=z[31] + z[27];
    z[27]=z[27]*z[21];
    z[27]=5*z[33] + z[27];
    z[27]=z[5]*z[27];
    z[27]=n<T>(5,2)*z[36] + z[27];
    z[27]=z[5]*z[27];
    z[27]=z[46] + z[27];
    z[27]=z[3]*z[27];
    z[31]= - n<T>(17,2) - z[47];
    z[27]=z[27] + n<T>(1,4)*z[31] + z[50];
    z[27]=z[3]*z[27];
    z[16]=z[16]*z[21];
    z[31]= - static_cast<T>(8)+ n<T>(95,16)*z[8];
    z[31]=z[8]*z[31];
    z[31]=n<T>(39,16) + z[31];
    z[31]=z[31]*z[11];
    z[31]=z[31] + z[16];
    z[31]=z[5]*z[31];
    z[33]=n<T>(1,16)*z[8];
    z[36]= - static_cast<T>(377)+ 181*z[8];
    z[36]=z[8]*z[36];
    z[36]=static_cast<T>(241)+ z[36];
    z[36]=z[8]*z[36];
    z[36]= - static_cast<T>(45)+ z[36];
    z[36]=z[36]*z[33];
    z[31]=z[36] + z[31];
    z[31]=z[5]*z[31];
    z[36]= - static_cast<T>(243)+ n<T>(173,2)*z[8];
    z[36]=z[8]*z[36];
    z[36]=static_cast<T>(237)+ z[36];
    z[36]=z[8]*z[36];
    z[36]= - static_cast<T>(91)+ z[36];
    z[36]=z[8]*z[36];
    z[36]=n<T>(21,2) + z[36];
    z[31]=n<T>(1,8)*z[36] + z[31];
    z[31]=z[5]*z[31];
    z[36]= - static_cast<T>(293)+ 83*z[8];
    z[36]=z[36]*z[33];
    z[36]=static_cast<T>(24)+ z[36];
    z[36]=z[8]*z[36];
    z[36]= - static_cast<T>(14)+ z[36];
    z[36]=z[8]*z[36];
    z[31]=z[31] + n<T>(19,8) + z[36];
    z[31]=z[5]*z[31];
    z[27]=z[27] + z[31] + n<T>(9,8) + z[49];
    z[27]=z[3]*z[27];
    z[31]=n<T>(1,2)*z[11];
    z[36]=static_cast<T>(23)- n<T>(45,2)*z[8];
    z[36]=z[8]*z[36];
    z[36]= - n<T>(19,4) + z[36];
    z[36]=z[36]*z[31];
    z[12]=z[12] - 1;
    z[46]= - z[12]*z[48];
    z[36]=z[36] + z[46];
    z[36]=z[5]*z[36];
    z[46]=static_cast<T>(67)- n<T>(165,4)*z[8];
    z[46]=z[8]*z[46];
    z[46]= - n<T>(123,4) + z[46];
    z[46]=z[8]*z[46];
    z[46]=n<T>(7,2) + z[46];
    z[46]=z[46]*z[20];
    z[36]=z[46] + z[36];
    z[36]=z[5]*z[36];
    z[46]=static_cast<T>(171)- 77*z[8];
    z[46]=z[8]*z[46];
    z[46]= - n<T>(245,2) + z[46];
    z[46]=z[46]*z[20];
    z[46]=static_cast<T>(15)+ z[46];
    z[46]=z[8]*z[46];
    z[46]= - n<T>(3,4) + z[46];
    z[36]=n<T>(1,2)*z[46] + z[36];
    z[36]=z[5]*z[36];
    z[46]=static_cast<T>(103)- n<T>(73,2)*z[8];
    z[46]=z[8]*z[46];
    z[46]= - static_cast<T>(101)+ z[46];
    z[46]=z[8]*z[46];
    z[46]=static_cast<T>(39)+ z[46];
    z[46]=z[8]*z[46];
    z[46]= - static_cast<T>(3)+ z[46];
    z[36]=n<T>(1,4)*z[46] + z[36];
    z[36]=z[36]*z[28];
    z[27]=z[27] + z[36] - n<T>(3,16) - z[51];
    z[27]=z[3]*z[27];
    z[36]= - n<T>(7,2) + z[15];
    z[36]=z[8]*z[36];
    z[36]=n<T>(3,8) + z[36];
    z[36]=z[36]*z[11];
    z[46]= - static_cast<T>(3)+ 11*z[8];
    z[46]=z[46]*z[17];
    z[46]=z[46] + z[19];
    z[46]=z[5]*z[46];
    z[36]=z[36] + n<T>(1,8)*z[46];
    z[36]=z[5]*z[36];
    z[44]= - static_cast<T>(41)+ z[44];
    z[44]=z[8]*z[44];
    z[44]=n<T>(23,2) + z[44];
    z[44]=z[8]*z[44];
    z[44]= - n<T>(1,2) + z[44];
    z[44]=z[44]*z[24];
    z[36]=z[44] + z[36];
    z[36]=z[5]*z[36];
    z[44]= - static_cast<T>(27)+ n<T>(65,4)*z[8];
    z[44]=z[8]*z[44];
    z[44]=n<T>(51,4) + z[44];
    z[44]=z[8]*z[44];
    z[44]= - n<T>(3,2) + z[44];
    z[20]=z[44]*z[20];
    z[20]=z[20] + z[36];
    z[20]=z[5]*z[20];
    z[36]=n<T>(1,8)*z[8];
    z[44]= - static_cast<T>(67)+ 31*z[8];
    z[44]=z[8]*z[44];
    z[44]=static_cast<T>(45)+ z[44];
    z[44]=z[8]*z[44];
    z[44]= - static_cast<T>(9)+ z[44];
    z[44]=z[44]*z[36];
    z[20]=z[44] + z[20];
    z[20]=z[20]*z[28];
    z[43]= - static_cast<T>(1)+ z[43];
    z[43]=z[8]*z[43];
    z[43]=n<T>(7,8) + z[43];
    z[43]=z[8]*z[43];
    z[43]= - n<T>(1,4) + z[43];
    z[43]=z[8]*z[43];
    z[20]=z[27] + z[43] + z[20];
    z[20]=z[4]*z[20];
    z[27]=static_cast<T>(2)- n<T>(5,16)*z[8];
    z[27]=z[8]*z[27];
    z[27]= - n<T>(21,16) + z[27];
    z[27]=z[27]*z[11];
    z[16]=z[27] - z[16];
    z[16]=z[5]*z[16];
    z[27]= - z[8]*z[34];
    z[27]= - static_cast<T>(13)+ z[27];
    z[27]=z[8]*z[27];
    z[27]=static_cast<T>(9)+ z[27];
    z[27]=z[27]*z[33];
    z[16]=z[27] + z[16];
    z[16]=z[16]*npow(z[5],3);
    z[27]= - static_cast<T>(1)+ z[36];
    z[27]=z[8]*z[27];
    z[27]=n<T>(7,8) + z[27];
    z[27]=z[27]*z[11];
    z[27]=z[27] + z[40];
    z[27]=z[27]*npow(z[5],4);
    z[29]=z[3]*z[42]*z[29];
    z[27]=z[27] - n<T>(1,4)*z[29];
    z[27]=z[3]*z[27];
    z[16]=z[16] + n<T>(1,2)*z[27];
    z[16]=z[3]*z[16];
    z[27]=n<T>(5,8)*z[8];
    z[29]= - static_cast<T>(3)+ z[27];
    z[29]=z[8]*z[29];
    z[29]=n<T>(21,16) + z[29];
    z[29]=z[29]*z[11];
    z[12]=z[12]*z[17];
    z[28]=z[28]*z[12];
    z[28]=z[29] + z[28];
    z[28]=z[5]*z[28];
    z[29]=z[32]*z[15];
    z[29]=n<T>(39,4) + z[29];
    z[29]=z[8]*z[29];
    z[29]= - n<T>(9,2) + z[29];
    z[29]=z[29]*z[24];
    z[28]=z[29] + z[28];
    z[28]=z[5]*z[28];
    z[29]= - n<T>(11,2) + z[35];
    z[29]=z[8]*z[29];
    z[29]=n<T>(5,2) + z[29];
    z[28]=n<T>(1,8)*z[29] + z[28];
    z[28]=z[28]*z[37];
    z[16]=z[28] + z[16];
    z[16]=z[3]*z[16];
    z[28]=n<T>(1,4) + z[8];
    z[28]=z[28]*z[39];
    z[15]= - static_cast<T>(3)- z[15];
    z[15]=z[15]*z[17];
    z[15]=z[15] + z[19];
    z[15]=z[15]*z[21];
    z[15]=z[28] + z[15];
    z[15]=z[5]*z[15];
    z[17]= - static_cast<T>(1)- 9*z[8];
    z[17]=z[17]*z[24];
    z[15]=z[17] + z[15];
    z[15]=z[5]*z[15];
    z[15]=n<T>(1,2)*z[45] + z[15];
    z[15]=z[15]*z[37];
    z[15]=n<T>(1,4)*z[15] + z[16];
    z[15]=z[7]*z[15];
    z[12]=z[12] + z[18];
    z[12]=z[12]*z[21];
    z[16]=z[22]*z[8];
    z[16]=z[16] + n<T>(1,8);
    z[16]=z[16]*z[11];
    z[12]=z[16] + z[12];
    z[12]=z[12]*z[5];
    z[13]=z[13] + n<T>(3,4);
    z[13]=z[13]*z[31];
    z[12]=z[12] + z[13];
    z[12]=z[12]*z[5];
    z[13]=z[27] - 1;
    z[13]=z[13]*z[8];
    z[13]=z[13] + n<T>(3,8);
    z[11]=z[13]*z[11];
    z[11]=z[12] + z[11];
    z[11]=z[11]*z[5];
    z[11]=z[11] + n<T>(1,4)*z[26];
    z[12]= - z[6] - z[4];
    z[11]=z[11]*z[12];
    z[12]=z[30]*z[38];
    z[11]= - n<T>(1,8)*z[12] + z[11];
    z[11]=z[9]*z[11];

    r += n<T>(1,2)*z[11] + n<T>(1,4)*z[14] + z[15] + z[20] + z[23] + z[25] + 
      z[41] + z[52];
 
    return r;
}

template double qg_2lha_r1572(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1572(const std::array<dd_real,30>&);
#endif
