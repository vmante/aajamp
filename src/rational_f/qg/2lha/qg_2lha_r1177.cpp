#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1177(const std::array<T,30>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[6];
    z[7]=k[8];
    z[8]=k[3];
    z[9]=k[9];
    z[10]=k[10];
    z[11]=k[11];
    z[12]=k[14];
    z[13]=z[1] + 1;
    z[14]=n<T>(1,2)*z[3];
    z[15]= - z[13]*z[14];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[3]*z[15];
    z[16]=z[11] + n<T>(17,2);
    z[17]=3*z[16] - z[5];
    z[18]= - z[8]*z[10];
    z[15]=3*z[15] + n<T>(11,12)*z[1] + n<T>(1,2)*z[17] + z[18];
    z[17]=n<T>(1,4)*z[3];
    z[18]=z[13]*z[3];
    z[19]= - static_cast<T>(3)+ z[18];
    z[17]=z[19]*z[17];
    z[19]=static_cast<T>(1)+ n<T>(1,4)*z[11];
    z[19]=z[11]*z[19];
    z[19]=n<T>(23,8) + z[19];
    z[19]=z[8]*z[19];
    z[17]=z[17] - n<T>(11,24)*z[1] - n<T>(1,4)*z[16] + z[19];
    z[17]=z[6]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[6]*z[15];
    z[17]=z[8]*z[9];
    z[19]=static_cast<T>(17)+ 5*z[2];
    z[19]=z[9]*z[19];
    z[19]=n<T>(1,2)*z[19] - static_cast<T>(1)+ z[10];
    z[19]=z[19]*z[17];
    z[20]=n<T>(1,2)*z[2];
    z[21]=z[20]*z[5];
    z[22]=z[21] - 1;
    z[19]=z[22] + z[19];
    z[20]=z[20] - 1;
    z[23]=z[20]*z[18];
    z[24]= - z[23] - n<T>(1,2) + z[1];
    z[24]=z[24]*z[14];
    z[22]= - z[2]*z[22];
    z[22]= - n<T>(1,2) + z[22];
    z[22]=z[9]*z[22];
    z[25]=z[1]*z[5];
    z[15]=z[15] + z[24] - n<T>(1,4)*z[25] + z[22] + n<T>(1,2)*z[19];
    z[19]=z[2]*z[5];
    z[22]= - z[19] - static_cast<T>(15)+ z[5];
    z[22]=z[2]*z[22];
    z[24]=static_cast<T>(1)+ z[19];
    z[24]=z[9]*z[24]*npow(z[2],2);
    z[22]=z[24] + z[22] + n<T>(71,3) - z[5];
    z[24]= - n<T>(29,6) - 3*z[2];
    z[24]=z[9]*z[24];
    z[24]=n<T>(7,12) + z[24];
    z[17]=z[24]*z[17];
    z[24]=static_cast<T>(1)- n<T>(27,2)*z[2];
    z[24]=z[9]*z[24];
    z[24]=n<T>(7,6) + z[24];
    z[17]=n<T>(1,2)*z[24] + z[17];
    z[17]=z[8]*z[17];
    z[24]=n<T>(1,2)*z[1];
    z[21]=z[21] + n<T>(7,6) - z[5];
    z[21]=z[21]*z[24];
    z[17]= - z[23] + z[21] + n<T>(1,4)*z[22] + z[17];
    z[21]=n<T>(1,3)*z[1] + n<T>(1,3);
    z[20]=z[20]*z[21];
    z[21]=z[9]*z[2];
    z[22]=static_cast<T>(1)+ z[2];
    z[22]=z[8]*z[22]*npow(z[9],2);
    z[21]=z[21] + n<T>(1,3)*z[22];
    z[21]=z[8]*z[21];
    z[22]= - n<T>(1,2) + z[2];
    z[21]=n<T>(1,3)*z[22] + n<T>(1,2)*z[21];
    z[21]=z[8]*z[21];
    z[22]=z[8] - z[13];
    z[22]=z[6]*z[22];
    z[13]=n<T>(1,3)*z[22] + z[13];
    z[22]=n<T>(1,2)*z[6];
    z[13]=z[13]*z[22];
    z[13]=z[13] + z[21] + z[20];
    z[13]=z[4]*z[13];
    z[20]= - static_cast<T>(47)+ z[5];
    z[21]= - n<T>(29,3) + n<T>(3,2)*z[5];
    z[21]=z[1]*z[21];
    z[20]= - 3*z[18] + n<T>(1,2)*z[20] + z[21];
    z[16]=z[8]*z[16];
    z[16]=z[18] - z[16];
    z[18]=n<T>(29,3) - z[5];
    z[18]=z[1]*z[18];
    z[16]=n<T>(1,16)*z[18] + static_cast<T>(1)+ n<T>(1,8)*z[16];
    z[16]=z[6]*z[16];
    z[16]=n<T>(1,8)*z[20] + z[16];
    z[16]=z[6]*z[16];
    z[13]=n<T>(11,4)*z[13] + n<T>(1,4)*z[17] + z[16];
    z[13]=z[4]*z[13];
    z[13]=n<T>(1,4)*z[15] + z[13];
    z[13]=z[4]*z[13];
    z[15]=n<T>(1,2)*z[11];
    z[16]= - static_cast<T>(1)- z[11];
    z[16]=z[16]*z[15];
    z[17]=n<T>(1,2)*z[10];
    z[16]=z[16] - n<T>(1,3) + z[17];
    z[16]=z[8]*z[16];
    z[18]=z[14] - 1;
    z[20]=z[18]*z[3];
    z[21]=z[24] + 1;
    z[23]= - z[21]*z[20];
    z[24]=z[11] + n<T>(1,3) - z[10];
    z[16]=z[23] - n<T>(1,8)*z[1] + n<T>(1,4)*z[24] + z[16];
    z[16]=z[6]*z[16];
    z[23]=npow(z[10],2);
    z[15]= - static_cast<T>(1)- z[15];
    z[15]=z[11]*z[15];
    z[15]=z[15] - static_cast<T>(1)+ n<T>(1,2)*z[23];
    z[21]=z[21]*z[3];
    z[24]= - static_cast<T>(3)- z[1];
    z[21]=n<T>(1,2)*z[24] + z[21];
    z[21]=z[3]*z[21];
    z[15]=z[16] + n<T>(1,2)*z[15] + z[21];
    z[15]=z[6]*z[15];
    z[16]=z[23] - z[19];
    z[19]=n<T>(1,3)*z[9];
    z[21]= - n<T>(19,4) - z[2];
    z[21]=z[21]*z[19];
    z[16]=z[21] + static_cast<T>(1)- n<T>(1,4)*z[16];
    z[16]=z[9]*z[16];
    z[14]= - z[18]*z[14];
    z[14]=z[15] + z[16] + z[14];
    z[13]=n<T>(1,4)*z[14] + z[13];
    z[13]=z[4]*z[13];
    z[14]=static_cast<T>(3)+ 5*z[7];
    z[14]=z[14]*z[17];
    z[14]= - z[7] + z[14];
    z[14]=z[10]*z[14];
    z[15]=npow(z[11],2);
    z[14]=z[14] + z[15];
    z[14]=z[8]*z[14];
    z[16]= - static_cast<T>(1)+ n<T>(5,2)*z[10];
    z[16]=z[7]*z[16];
    z[17]=z[1]*z[7];
    z[14]=n<T>(1,2)*z[17] + z[14] + z[16];
    z[16]=z[17]*z[20];
    z[14]=n<T>(1,2)*z[14] + z[16];
    z[14]=z[6]*z[14];
    z[15]=n<T>(3,2)*z[23] + z[15];
    z[16]=z[17]*z[3];
    z[17]=z[17] - z[16];
    z[17]=z[3]*z[17];
    z[14]=z[14] + n<T>(1,2)*z[15] + z[17];
    z[14]=z[14]*z[22];
    z[15]=z[12] - 1;
    z[17]=z[15] + z[9];
    z[17]=z[19]*z[12]*z[17];
    z[15]=z[12]*z[15];
    z[15]=n<T>(1,3)*z[15] + n<T>(1,4)*z[16];
    z[15]=z[3]*z[15];
    z[14]=z[14] + z[17] + z[15];

    r += z[13] + n<T>(1,4)*z[14];
 
    return r;
}

template double qg_2lha_r1177(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1177(const std::array<dd_real,30>&);
#endif
