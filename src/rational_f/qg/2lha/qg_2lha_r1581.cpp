#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1581(const std::array<T,30>& k) {
  T z[63];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[3];
    z[8]=k[5];
    z[9]=k[9];
    z[10]=k[11];
    z[11]=z[9] - n<T>(3,2);
    z[12]=npow(z[9],3);
    z[13]= - z[11]*z[12];
    z[14]=npow(z[9],2);
    z[15]=z[14]*z[5];
    z[16]= - static_cast<T>(9)+ n<T>(11,2)*z[9];
    z[16]=z[9]*z[16];
    z[16]=n<T>(7,2) + z[16];
    z[16]=z[16]*z[15];
    z[13]=z[13] + n<T>(1,2)*z[16];
    z[16]=npow(z[5],2);
    z[13]=z[13]*z[16];
    z[17]=z[9] - 1;
    z[18]=z[17]*z[12];
    z[19]=z[18]*z[5];
    z[20]=npow(z[9],4);
    z[21]= - z[20] + n<T>(5,2)*z[19];
    z[21]=z[21]*z[16];
    z[22]=npow(z[5],3);
    z[23]=z[22]*z[20];
    z[24]=n<T>(1,2)*z[4];
    z[25]=z[24]*z[23];
    z[21]=z[21] + z[25];
    z[21]=z[21]*z[24];
    z[13]=z[13] + z[21];
    z[13]=z[4]*z[13];
    z[21]=5*z[9];
    z[25]= - static_cast<T>(3)+ z[21];
    z[25]=z[25]*z[14];
    z[26]=3*z[9];
    z[27]= - static_cast<T>(5)+ z[26];
    z[27]=z[27]*z[21];
    z[27]=static_cast<T>(17)+ z[27];
    z[27]=z[9]*z[27];
    z[27]= - static_cast<T>(3)+ z[27];
    z[28]=n<T>(1,2)*z[9];
    z[29]=z[28]*z[5];
    z[27]=z[27]*z[29];
    z[25]=z[25] + z[27];
    z[27]=n<T>(1,2)*z[16];
    z[25]=z[25]*z[27];
    z[13]=z[25] + z[13];
    z[13]=z[13]*z[24];
    z[25]=n<T>(3,4)*z[9];
    z[30]=z[25] - 1;
    z[31]=z[30]*z[21];
    z[31]=static_cast<T>(3)+ z[31];
    z[31]=z[9]*z[31];
    z[31]= - static_cast<T>(1)+ z[31];
    z[31]=z[31]*z[29];
    z[30]=z[9]*z[30];
    z[30]=n<T>(1,4) + z[30];
    z[30]=z[9]*z[30];
    z[30]=z[30] + z[31];
    z[30]=z[30]*z[16];
    z[13]=z[30] + z[13];
    z[13]=z[4]*z[13];
    z[30]=static_cast<T>(9)- z[21];
    z[30]=z[9]*z[30];
    z[30]= - static_cast<T>(5)+ z[30];
    z[30]=z[9]*z[30];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[30]*z[28];
    z[31]=n<T>(1,4)*z[9];
    z[32]=11*z[9];
    z[33]= - static_cast<T>(15)+ z[32];
    z[33]=z[33]*z[31];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[33]*z[15];
    z[30]=z[30] + z[33];
    z[30]=z[30]*z[27];
    z[13]=z[30] + z[13];
    z[13]=z[4]*z[13];
    z[30]=z[28] - 1;
    z[30]=z[30]*z[9];
    z[33]=z[30] + n<T>(1,2);
    z[15]=z[33]*z[15];
    z[34]=static_cast<T>(7)- z[26];
    z[34]=z[9]*z[34];
    z[34]= - static_cast<T>(5)+ z[34];
    z[34]=z[9]*z[34];
    z[34]=static_cast<T>(1)+ z[34];
    z[34]=z[9]*z[34];
    z[34]=z[34] + n<T>(5,2)*z[15];
    z[34]=z[34]*z[27];
    z[13]=z[34] + z[13];
    z[13]=z[4]*z[13];
    z[34]=n<T>(1,4)*z[4];
    z[35]=z[23]*z[34];
    z[36]=z[22]*z[12];
    z[37]=z[9] - n<T>(1,2);
    z[38]= - z[37]*z[36];
    z[38]=z[38] - z[35];
    z[38]=z[4]*z[38];
    z[39]=z[22]*z[14];
    z[40]=n<T>(1,2)*z[39];
    z[41]=n<T>(5,2)*z[9];
    z[42]=z[41] - 3;
    z[43]= - z[9]*z[42];
    z[43]= - n<T>(1,2) + z[43];
    z[43]=z[43]*z[40];
    z[38]=z[43] + z[38];
    z[38]=z[4]*z[38];
    z[43]=z[37]*z[39];
    z[38]=z[43] + z[38];
    z[38]=z[4]*z[38];
    z[43]= - static_cast<T>(1)+ n<T>(5,4)*z[9];
    z[36]=z[43]*z[36];
    z[36]=z[36] + z[38];
    z[36]=z[4]*z[36];
    z[38]=z[11]*z[9];
    z[43]=n<T>(1,2) + z[38];
    z[39]=z[43]*z[39];
    z[36]=z[39] + z[36];
    z[36]=z[4]*z[36];
    z[39]=z[33]*z[40];
    z[36]=z[39] + z[36];
    z[39]=n<T>(1,2)*z[7];
    z[36]=z[36]*z[39];
    z[40]=z[31]*z[5];
    z[43]=z[9] - 3;
    z[43]=z[43]*z[9];
    z[43]=z[43] + 3;
    z[43]=z[43]*z[9];
    z[44]=z[43] - 1;
    z[45]=z[44]*z[40];
    z[46]=z[44]*z[9];
    z[45]= - z[46] + z[45];
    z[27]=z[45]*z[27];
    z[45]=z[20]*z[4];
    z[45]= - z[45] + 3*z[12];
    z[45]=z[45]*z[4];
    z[47]=3*z[14];
    z[45]=z[45] - z[47];
    z[45]=z[45]*z[4];
    z[45]=z[45] + z[9];
    z[48]=npow(z[4],2);
    z[49]=z[48]*z[6];
    z[50]=z[45]*z[49];
    z[51]=n<T>(1,4)*z[5];
    z[52]=z[51] - 1;
    z[52]=z[52]*z[5];
    z[52]=z[52] + n<T>(3,2);
    z[53]=z[5]*z[52];
    z[53]=z[34] - n<T>(3,4) + z[53];
    z[54]=n<T>(1,2)*z[8];
    z[53]=z[53]*z[54];
    z[13]=z[36] + z[53] + n<T>(1,8)*z[50] + z[27] + z[13];
    z[13]=z[13]*z[39];
    z[27]=n<T>(1,2)*z[14];
    z[36]=z[33]*z[49]*z[27];
    z[50]=z[9] - 5;
    z[50]=z[50]*z[28];
    z[50]=z[50] + 5;
    z[50]=z[50]*z[9];
    z[50]=z[50] - 5;
    z[50]=z[50]*z[28];
    z[50]=z[50] + 1;
    z[53]=z[44]*z[34];
    z[36]=z[36] + z[53] - z[50];
    z[36]=z[3]*z[36];
    z[53]= - n<T>(1,4)*z[7] - z[50];
    z[53]=z[8]*z[53];
    z[55]=z[3]*z[50];
    z[53]=z[55] + n<T>(1,2) + z[53];
    z[53]=z[2]*z[53];
    z[36]=z[36] + z[53];
    z[53]=5*z[15];
    z[55]= - n<T>(13,2) + z[21];
    z[55]=z[55]*z[12];
    z[55]=n<T>(1,2)*z[55] - z[53];
    z[55]=z[5]*z[55];
    z[55]= - n<T>(3,4)*z[20] + z[55];
    z[55]=z[5]*z[55];
    z[56]=5*z[5];
    z[57]= - z[56]*z[18];
    z[57]=3*z[20] + z[57];
    z[57]=z[57]*z[16];
    z[23]= - z[4]*z[23];
    z[23]=z[57] + z[23];
    z[23]=z[23]*z[34];
    z[23]=z[55] + z[23];
    z[23]=z[4]*z[23];
    z[55]= - static_cast<T>(31)+ z[32];
    z[55]=z[9]*z[55];
    z[55]=static_cast<T>(21)+ z[55];
    z[55]=z[55]*z[14];
    z[57]=z[5]*z[9];
    z[58]=9*z[9];
    z[59]=static_cast<T>(31)- z[58];
    z[59]=z[9]*z[59];
    z[59]= - static_cast<T>(29)+ z[59];
    z[59]=z[9]*z[59];
    z[59]=static_cast<T>(9)+ z[59];
    z[59]=z[59]*z[57];
    z[55]=z[55] + z[59];
    z[55]=z[55]*z[51];
    z[59]=z[9] - n<T>(5,2);
    z[60]= - z[59]*z[12];
    z[55]=z[60] + z[55];
    z[55]=z[5]*z[55];
    z[23]=z[55] + z[23];
    z[23]=z[23]*z[34];
    z[55]=n<T>(1,4)*z[14];
    z[60]=z[42]*z[55];
    z[61]=static_cast<T>(17)- z[58];
    z[61]=z[9]*z[61];
    z[61]= - n<T>(15,2) + z[61];
    z[61]=z[9]*z[61];
    z[62]=static_cast<T>(1)+ n<T>(5,4)*z[38];
    z[62]=z[9]*z[62];
    z[62]= - n<T>(1,8) + z[62];
    z[62]=z[5]*z[62];
    z[61]=n<T>(1,8)*z[61] + z[62];
    z[61]=z[5]*z[61];
    z[60]=z[60] + z[61];
    z[60]=z[5]*z[60];
    z[23]=z[60] + z[23];
    z[23]=z[4]*z[23];
    z[25]= - z[42]*z[25];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[9]*z[25];
    z[25]= - n<T>(15,8) + z[25];
    z[25]=z[9]*z[25];
    z[58]= - static_cast<T>(5)+ z[58];
    z[58]=z[58]*z[28];
    z[58]= - static_cast<T>(5)+ z[58];
    z[58]=z[58]*z[31];
    z[58]=static_cast<T>(1)+ z[58];
    z[58]=z[9]*z[58];
    z[58]= - n<T>(1,4) + z[58];
    z[58]=z[5]*z[58];
    z[25]=z[58] + n<T>(1,4) + z[25];
    z[25]=z[5]*z[25];
    z[58]=static_cast<T>(3)- n<T>(7,2)*z[9];
    z[58]=z[58]*z[31];
    z[25]=z[58] + z[25];
    z[58]=n<T>(1,2)*z[5];
    z[25]=z[25]*z[58];
    z[23]=z[25] + z[23];
    z[23]=z[4]*z[23];
    z[25]= - static_cast<T>(7)+ z[41];
    z[25]=z[9]*z[25];
    z[25]=static_cast<T>(5)+ z[25];
    z[25]=z[25]*z[27];
    z[27]=n<T>(47,2) - z[32];
    z[27]=z[9]*z[27];
    z[27]= - static_cast<T>(13)+ z[27];
    z[27]=z[9]*z[27];
    z[27]= - n<T>(1,2) + z[27];
    z[27]=z[9]*z[27];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=n<T>(1,2)*z[27] + z[53];
    z[27]=z[5]*z[27];
    z[25]=z[25] + z[27];
    z[25]=z[25]*z[51];
    z[23]=z[25] + z[23];
    z[23]=z[4]*z[23];
    z[25]=z[44]*z[57];
    z[27]= - z[44]*z[26];
    z[27]=z[27] + z[25];
    z[27]=z[5]*z[27];
    z[26]= - n<T>(19,2) + z[26];
    z[26]=z[9]*z[26];
    z[26]=n<T>(41,4) + z[26];
    z[26]=z[26]*z[31];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[9]*z[26];
    z[26]=n<T>(5,16)*z[27] + n<T>(1,16) + z[26];
    z[26]=z[5]*z[26];
    z[23]=z[23] - n<T>(1,16) + z[26];
    z[23]=z[4]*z[23];
    z[26]=n<T>(3,2)*z[12];
    z[27]=z[42]*z[26];
    z[27]=z[27] - z[53];
    z[27]=z[5]*z[27];
    z[27]= - n<T>(3,2)*z[20] + z[27];
    z[27]=z[5]*z[27];
    z[19]=z[20] - n<T>(5,4)*z[19];
    z[19]=z[19]*z[16];
    z[19]=z[19] - z[35];
    z[19]=z[4]*z[19];
    z[19]=z[27] + z[19];
    z[19]=z[19]*z[34];
    z[27]= - static_cast<T>(13)+ z[21];
    z[27]=z[27]*z[31];
    z[27]=static_cast<T>(2)+ z[27];
    z[27]=z[27]*z[14];
    z[25]=z[27] - n<T>(5,8)*z[25];
    z[25]=z[5]*z[25];
    z[27]=n<T>(5,8)*z[9];
    z[32]=static_cast<T>(1)- z[27];
    z[26]=z[32]*z[26];
    z[25]=z[26] + z[25];
    z[25]=z[5]*z[25];
    z[19]=z[19] + n<T>(3,16)*z[20] + z[25];
    z[19]=z[4]*z[19];
    z[25]=static_cast<T>(11)- z[41];
    z[25]=z[9]*z[25];
    z[25]= - n<T>(37,4) + z[25];
    z[25]=z[25]*z[55];
    z[26]=z[31] - 1;
    z[26]=z[26]*z[9];
    z[26]=z[26] + n<T>(3,2);
    z[26]=z[26]*z[9];
    z[26]=z[26] - 1;
    z[32]= - z[26]*z[21];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[32]*z[51];
    z[27]= - static_cast<T>(3)+ z[27];
    z[27]=z[9]*z[27];
    z[27]=n<T>(33,8) + z[27];
    z[27]=z[9]*z[27];
    z[27]= - n<T>(7,4) + z[27];
    z[27]=z[9]*z[27];
    z[27]=z[27] + z[32];
    z[27]=z[5]*z[27];
    z[25]=z[25] + z[27];
    z[25]=z[5]*z[25];
    z[21]= - static_cast<T>(11)+ z[21];
    z[12]=z[21]*z[12];
    z[12]=z[19] + n<T>(1,16)*z[12] + z[25];
    z[12]=z[4]*z[12];
    z[19]=n<T>(5,4) - z[9];
    z[19]=z[19]*z[47];
    z[21]=z[50]*z[5];
    z[25]=n<T>(3,4) - z[43];
    z[21]=3*z[25] - z[21];
    z[21]=z[5]*z[21];
    z[25]=z[9]*z[59];
    z[25]=n<T>(3,2) + z[25];
    z[25]=z[9]*z[25];
    z[21]=n<T>(9,2)*z[25] + z[21];
    z[21]=z[5]*z[21];
    z[19]=z[19] + z[21];
    z[12]=n<T>(1,4)*z[19] + z[12];
    z[12]=z[4]*z[12];
    z[19]=static_cast<T>(1)- z[30];
    z[19]=z[19]*z[31];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[9]*z[19];
    z[19]=n<T>(5,8) + z[19];
    z[19]=z[5]*z[19];
    z[21]= - static_cast<T>(1)- 3*z[30];
    z[19]=n<T>(3,4)*z[21] + z[19];
    z[19]=z[5]*z[19];
    z[21]=z[9]*z[17];
    z[19]=n<T>(9,8)*z[21] + z[19];
    z[12]=n<T>(1,2)*z[19] + z[12];
    z[12]=z[4]*z[12];
    z[19]= - z[9]*z[33];
    z[19]=n<T>(1,2) + z[19];
    z[19]=z[9]*z[19];
    z[19]= - n<T>(1,2) + z[19];
    z[19]=z[5]*z[19];
    z[19]=z[19] - z[37];
    z[12]=n<T>(1,8)*z[19] + z[12];
    z[12]=z[12]*z[49];
    z[19]= - z[33]*z[14];
    z[15]=z[19] + z[15];
    z[15]=z[15]*z[16];
    z[19]=z[22]*z[18];
    z[21]=static_cast<T>(1)+ z[5];
    z[21]=z[5]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[4]*z[5]*z[20]*z[21];
    z[19]=5*z[19] + z[21];
    z[19]=z[19]*z[34];
    z[15]=5*z[15] + z[19];
    z[15]=z[4]*z[15];
    z[19]=z[44]*z[28];
    z[21]=z[44]*z[29];
    z[21]= - z[46] + z[21];
    z[21]=z[5]*z[21];
    z[19]=z[19] + z[21];
    z[19]=z[19]*z[56];
    z[15]=z[19] + z[15];
    z[15]=z[4]*z[15];
    z[19]= - static_cast<T>(3)+ z[5];
    z[19]=z[5]*z[19];
    z[19]=static_cast<T>(3)+ z[19];
    z[21]=z[26]*z[9];
    z[25]=z[21] + n<T>(1,4);
    z[19]=z[56]*z[25]*z[19];
    z[25]=n<T>(15,4) - z[9];
    z[25]=z[9]*z[25];
    z[25]= - static_cast<T>(5)+ z[25];
    z[25]=z[9]*z[25];
    z[25]=n<T>(5,2) + z[25];
    z[25]=z[9]*z[25];
    z[15]=z[15] + z[19] - n<T>(1,4) + z[25];
    z[15]=z[15]*z[34];
    z[19]=z[5]*z[50]*z[52];
    z[15]=z[15] - n<T>(3,4)*z[50] + z[19];
    z[15]=z[8]*z[15];
    z[11]= - z[11]*z[14];
    z[14]=z[18]*z[24];
    z[11]=z[11] + z[14];
    z[11]=z[4]*z[11];
    z[11]=z[38] + z[11];
    z[11]=z[4]*z[11];
    z[14]=z[45]*z[39];
    z[11]=z[14] - n<T>(1,2)*z[17] + z[11];
    z[11]=z[48]*z[11];
    z[14]=z[20]*npow(z[4],5)*z[54];
    z[11]=z[14] + z[11];
    z[11]=z[10]*z[11];
    z[14]=z[22]*z[24];
    z[14]= - z[16] + z[14];
    z[14]=z[4]*z[14];
    z[14]=z[58] + z[14];
    z[14]=z[14]*z[49];
    z[16]= - n<T>(1,2)*z[2] + n<T>(1,2);
    z[16]=z[3]*z[16];
    z[14]=z[14] + z[16];
    z[14]=z[1]*z[14];
    z[11]=z[11] + z[14];
    z[14]= - z[9] + z[40];
    z[14]=z[5]*z[26]*z[14];
    z[14]=n<T>(3,2)*z[21] + z[14];
    z[14]=z[5]*z[14];

    r += n<T>(1,8) + n<T>(1,8)*z[11] + z[12] + z[13] + z[14] + z[15] + z[23] + 
      n<T>(1,4)*z[36];
 
    return r;
}

template double qg_2lha_r1581(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1581(const std::array<dd_real,30>&);
#endif
