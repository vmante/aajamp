#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2120(const std::array<T,30>& k) {
  T z[9];
  T r = 0;

    z[1]=k[3];
    z[2]=k[6];
    z[3]=k[9];
    z[4]=k[7];
    z[5]=k[14];
    z[6]=n<T>(1,2) - z[2];
    z[6]=z[6]*npow(z[2],2);
    z[7]=z[1]*npow(z[2],3);
    z[6]=z[6] + n<T>(1,3)*z[7];
    z[6]=z[1]*z[6];
    z[7]= - static_cast<T>(1)+ z[2];
    z[7]=z[2]*z[7];
    z[7]=n<T>(1,2) + z[7];
    z[7]=z[7]*z[2];
    z[6]=z[6] + z[7];
    z[6]=z[3]*z[6];
    z[7]= - z[4] - z[3];
    z[8]= - n<T>(1,2) + n<T>(1,3)*z[2];
    z[8]=z[8]*z[2];
    z[8]=z[8] + n<T>(1,2);
    z[7]=z[8]*z[7]*z[5]*z[2];

    r += z[6] + z[7];
 
    return r;
}

template double qg_2lha_r2120(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2120(const std::array<dd_real,30>&);
#endif
