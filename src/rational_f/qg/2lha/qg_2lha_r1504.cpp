#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1504(const std::array<T,30>& k) {
  T z[9];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[4];
    z[5]=z[4] + 1;
    z[6]=n<T>(1,2)*z[3];
    z[5]=z[5]*z[6];
    z[5]= - static_cast<T>(1)+ z[5];
    z[5]=z[3]*z[5];
    z[7]=static_cast<T>(1)- z[4];
    z[5]=n<T>(1,2)*z[7] + z[5];
    z[5]=z[3]*z[5];
    z[7]=z[1] - 1;
    z[8]= - z[4] + z[7];
    z[6]=z[8]*z[6];
    z[6]=z[6] - n<T>(3,2)*z[7] + z[4];
    z[6]=z[3]*z[6];
    z[8]=3*z[7] - z[4];
    z[6]=n<T>(1,2)*z[8] + z[6];
    z[6]=z[3]*z[6];
    z[6]= - n<T>(1,2)*z[7] + z[6];
    z[6]=z[2]*z[6];
    z[5]=z[5] + z[6];

    r += n<T>(1,2)*z[5]*z[2];
 
    return r;
}

template double qg_2lha_r1504(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1504(const std::array<dd_real,30>&);
#endif
