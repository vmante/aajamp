#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2353(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[11];
    z[7]=k[12];
    z[8]=z[4] - 1;
    z[9]=z[8]*z[7];
    z[8]= - z[6]*z[8];
    z[8]=z[9] + z[8];
    z[10]=n<T>(1,4)*z[5];
    z[8]=z[8]*z[10];
    z[8]=z[8] - static_cast<T>(1)+ n<T>(1,2)*z[9];
    z[8]=z[5]*z[8];
    z[11]=n<T>(1,2)*z[2];
    z[12]=z[1] - 1;
    z[13]= - z[5] + z[12];
    z[13]=z[2]*z[13];
    z[13]=z[13] + 3*z[5] + n<T>(5,2) - z[1];
    z[13]=z[13]*z[11];
    z[9]= - static_cast<T>(3)+ z[9];
    z[8]=z[13] + n<T>(1,4)*z[9] + z[8];
    z[8]=z[3]*z[8];
    z[9]=3*z[7];
    z[13]=n<T>(3,2) - z[4];
    z[13]=z[13]*z[9];
    z[13]=n<T>(17,2) + z[13];
    z[14]=static_cast<T>(1)- n<T>(1,2)*z[4];
    z[14]=z[7]*z[14];
    z[15]= - z[6] + z[7];
    z[15]=z[5]*z[15];
    z[14]=n<T>(3,8)*z[15] + static_cast<T>(1)+ n<T>(3,2)*z[14];
    z[14]=z[5]*z[14];
    z[15]= - 13*z[5] - static_cast<T>(19)+ 7*z[1];
    z[16]= - n<T>(3,2)*z[12] + z[5];
    z[16]=z[2]*z[16];
    z[15]=n<T>(1,2)*z[15] + 3*z[16];
    z[11]=z[15]*z[11];
    z[8]=n<T>(3,2)*z[8] + z[11] + n<T>(1,4)*z[13] + z[14];
    z[8]=z[3]*z[8];
    z[11]=n<T>(3,8)*z[7];
    z[13]= - static_cast<T>(3)+ z[4];
    z[13]=z[13]*z[11];
    z[9]=static_cast<T>(1)- z[9];
    z[9]=z[9]*z[10];
    z[10]=n<T>(3,4)*z[2];
    z[14]=3*z[12] - z[5];
    z[14]=z[14]*z[10];
    z[15]=n<T>(31,2) - 5*z[1];
    z[14]=z[14] + n<T>(1,4)*z[15] + z[5];
    z[14]=z[2]*z[14];
    z[8]=z[8] + z[14] + z[9] - static_cast<T>(1)+ z[13];
    z[8]=z[3]*z[8];
    z[9]= - z[12]*z[10];
    z[9]=z[9] - static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[9]=z[2]*z[9];

    r += z[8] + z[9] + z[11];
 
    return r;
}

template double qg_2lha_r2353(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2353(const std::array<dd_real,30>&);
#endif
