#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r706(const std::array<T,30>& k) {
  T z[37];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[3];
    z[7]=k[27];
    z[8]=k[12];
    z[9]=k[5];
    z[10]=k[9];
    z[11]=k[10];
    z[12]=z[7] + z[8];
    z[13]=n<T>(1,2)*z[1];
    z[14]=z[12]*z[13];
    z[15]=n<T>(1,2)*z[6];
    z[16]=z[15] - 1;
    z[17]=z[16]*z[7];
    z[18]=n<T>(1,2)*z[10];
    z[19]= - z[18] - static_cast<T>(1)+ z[17];
    z[19]=z[10]*z[19];
    z[20]=z[6]*z[9];
    z[21]=z[20] - z[9];
    z[22]=z[8] - 1;
    z[23]=z[15] + 1;
    z[24]=z[7]*z[23];
    z[19]=z[19] - z[14] + z[24] - n<T>(3,2)*z[22] + z[21];
    z[24]=z[18] - 1;
    z[17]=z[17] + z[24];
    z[17]=z[10]*z[17];
    z[25]=npow(z[10],2);
    z[26]=n<T>(1,2)*z[2];
    z[27]= - z[25]*z[26];
    z[28]= - z[7] + z[10];
    z[28]=z[5]*z[28]*z[18];
    z[29]=n<T>(1,2)*z[7];
    z[17]=z[28] + z[27] + z[29] + z[17];
    z[27]=n<T>(1,2)*z[5];
    z[17]=z[17]*z[27];
    z[28]=n<T>(1,4)*z[10];
    z[30]=z[28] - 1;
    z[30]=z[30]*z[10];
    z[31]=z[2]*z[28];
    z[31]=z[31] + n<T>(1,4);
    z[31]=z[9]*z[31];
    z[31]= - z[30] + z[31];
    z[31]=z[2]*z[31];
    z[17]=z[17] + n<T>(1,2)*z[19] + z[31];
    z[17]=z[5]*z[17];
    z[19]=z[10] - 1;
    z[31]=z[9]*z[19];
    z[32]=z[2]*z[31];
    z[32]=z[32] + z[9];
    z[32]=z[18]*z[32];
    z[32]= - z[9] + z[32];
    z[32]=z[2]*z[32];
    z[33]=5*z[10];
    z[20]= - z[20] - z[9] + static_cast<T>(13)+ 11*z[11];
    z[20]=z[32] + n<T>(1,2)*z[20] + z[33];
    z[20]=z[20]*z[26];
    z[32]=3*z[6];
    z[34]=3*z[1] + static_cast<T>(1)- z[32];
    z[34]=z[7]*z[34];
    z[22]=z[34] + z[22];
    z[34]=n<T>(1,4)*z[1];
    z[22]=z[22]*z[34];
    z[32]=z[32] - z[10];
    z[32]=z[32]*z[28];
    z[35]=static_cast<T>(7)+ z[8];
    z[35]=n<T>(1,2)*z[35] + z[9];
    z[16]=z[9]*z[16];
    z[16]= - n<T>(1,4)*z[8] + n<T>(1,2) + z[11] + z[16];
    z[16]=z[6]*z[16];
    z[16]=z[17] + z[20] + z[32] + z[22] + n<T>(1,2)*z[35] + z[16];
    z[16]=z[5]*z[16];
    z[17]=3*z[4];
    z[20]= - z[17] - z[31];
    z[20]=z[20]*z[26];
    z[22]=z[1]*z[4];
    z[31]=z[4] - z[9];
    z[31]=z[6]*z[31];
    z[31]=z[31] - static_cast<T>(5)+ 7*z[4];
    z[20]=z[20] + n<T>(1,2)*z[31] - z[22];
    z[20]=z[20]*z[26];
    z[15]= - z[21]*z[15];
    z[21]=n<T>(1,2) + z[4];
    z[21]=z[21]*z[13];
    z[31]= - static_cast<T>(1)- n<T>(5,2)*z[11];
    z[32]=n<T>(1,4) + z[11];
    z[32]=z[10]*z[32];
    z[15]=z[20] + z[32] + z[21] + z[15] + n<T>(1,2)*z[31] - z[4];
    z[15]=z[2]*z[15];
    z[20]=static_cast<T>(1)- z[6];
    z[20]=z[20]*npow(z[6],2)*z[29];
    z[21]= - z[7]*z[6];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[13];
    z[31]= - n<T>(1,2) + z[6];
    z[31]=z[7]*z[31];
    z[31]=static_cast<T>(1)+ z[31];
    z[31]=z[6]*z[31];
    z[21]=z[21] + z[31];
    z[21]=z[1]*z[21];
    z[23]= - z[6]*z[23];
    z[20]=z[21] + z[20] - n<T>(3,2) + z[23];
    z[21]=static_cast<T>(1)- n<T>(1,4)*z[6];
    z[21]=z[10]*z[21];
    z[15]=z[16] + z[15] + n<T>(1,2)*z[20] + z[21];
    z[14]=z[14] + static_cast<T>(1)+ z[12];
    z[14]=z[1]*z[14];
    z[16]=static_cast<T>(19)- 17*z[10];
    z[16]=z[10]*z[16];
    z[14]=z[16] + z[14] + z[29] + static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[16]=z[2]*z[10];
    z[20]= - static_cast<T>(1)+ z[33];
    z[20]=z[20]*z[16];
    z[21]= - z[1] - 1;
    z[12]=z[12]*z[21];
    z[12]= - 11*z[25] + z[12];
    z[21]=n<T>(1,4)*z[5];
    z[12]=z[12]*z[21];
    z[12]=z[12] + n<T>(1,2)*z[14] + z[20];
    z[12]=z[12]*z[27];
    z[14]= - static_cast<T>(3)- z[1];
    z[14]=z[14]*z[13];
    z[20]=static_cast<T>(89)- 35*z[10];
    z[20]=z[20]*z[28];
    z[14]=z[20] - static_cast<T>(13)+ z[14];
    z[20]=3*z[10];
    z[23]= - static_cast<T>(3)+ n<T>(7,4)*z[10];
    z[23]=z[23]*z[20];
    z[31]=static_cast<T>(5)+ z[13];
    z[32]=static_cast<T>(1)- z[25];
    z[32]=z[2]*z[32];
    z[23]=n<T>(7,8)*z[32] + n<T>(1,2)*z[31] + z[23];
    z[23]=z[2]*z[23];
    z[12]=z[12] + n<T>(1,2)*z[14] + z[23];
    z[12]=z[5]*z[12];
    z[14]= - n<T>(85,2) + 11*z[10];
    z[14]=z[14]*z[28];
    z[23]= - z[24]*z[18];
    z[31]=n<T>(3,8)*z[11];
    z[23]=z[23] - static_cast<T>(1)+ z[31];
    z[23]=z[2]*z[23];
    z[32]=n<T>(9,4) - z[10];
    z[32]=z[10]*z[32];
    z[23]=z[23] + n<T>(13,4) + z[32];
    z[23]=z[2]*z[23];
    z[32]=z[34] + 4;
    z[32]=z[32]*z[1];
    z[14]=z[23] + z[14] + n<T>(3,8) - z[32];
    z[14]=z[2]*z[14];
    z[23]=z[10] - 5;
    z[33]=n<T>(3,2)*z[10];
    z[34]= - z[23]*z[33];
    z[12]=z[12] + z[14] + z[34] - n<T>(21,8) + z[32];
    z[12]=z[5]*z[12];
    z[14]=z[10] - 3;
    z[32]=z[14]*z[18];
    z[32]=z[32] + 1;
    z[34]=n<T>(5,2) - z[10];
    z[34]=z[10]*z[34];
    z[24]=z[10]*z[24];
    z[24]=n<T>(1,2) + z[24];
    z[24]=z[2]*z[24];
    z[24]=z[24] - n<T>(3,2) + z[34];
    z[24]=z[2]*z[24];
    z[24]=z[24] + z[32];
    z[34]=z[19]*z[16];
    z[21]=z[25]*z[21];
    z[35]=n<T>(3,4)*z[10];
    z[36]= - static_cast<T>(1)+ z[35];
    z[36]=z[10]*z[36];
    z[21]=z[21] + z[36] - n<T>(3,4)*z[34];
    z[21]=z[5]*z[21];
    z[21]=n<T>(3,2)*z[24] + z[21];
    z[21]=z[5]*z[21];
    z[24]=n<T>(3,2) - z[1];
    z[24]=n<T>(1,2)*z[24] + z[30];
    z[30]=z[13] - z[32];
    z[30]=z[30]*z[26];
    z[24]=3*z[24] + z[30];
    z[24]=z[2]*z[24];
    z[23]= - z[23]*z[28];
    z[23]=z[23] - static_cast<T>(1)+ n<T>(3,4)*z[1];
    z[23]=3*z[23] + z[24];
    z[23]=z[2]*z[23];
    z[24]= - static_cast<T>(3)+ z[18];
    z[24]=z[24]*z[18];
    z[21]=z[21] + z[23] + z[24] + n<T>(5,4) - z[1];
    z[21]=z[5]*z[21];
    z[23]=z[1] - 1;
    z[23]=z[23]*z[1];
    z[23]=z[23] - z[19];
    z[24]=static_cast<T>(3)- z[2];
    z[24]=z[2]*z[24];
    z[24]= - static_cast<T>(3)+ z[24];
    z[24]=z[2]*z[23]*z[24];
    z[23]=z[24] + z[23];
    z[21]=n<T>(1,4)*z[23] + z[21];
    z[21]=z[3]*z[21];
    z[23]= - static_cast<T>(11)- 3*z[11];
    z[24]=static_cast<T>(5)- n<T>(3,4)*z[22];
    z[24]=z[1]*z[24];
    z[23]=n<T>(1,4)*z[23] + z[24];
    z[24]=static_cast<T>(1)+ z[31];
    z[24]=z[10]*z[24];
    z[23]=n<T>(1,2)*z[23] + z[24];
    z[23]=z[2]*z[23];
    z[24]= - static_cast<T>(7)+ 9*z[4];
    z[24]=z[1]*z[24];
    z[24]= - static_cast<T>(49)+ z[24];
    z[24]=z[1]*z[24];
    z[24]= - z[10] + static_cast<T>(19)+ z[24];
    z[23]=n<T>(1,8)*z[24] + z[23];
    z[23]=z[2]*z[23];
    z[24]=static_cast<T>(7)- n<T>(9,2)*z[4];
    z[24]=z[1]*z[24];
    z[24]=static_cast<T>(19)+ z[24];
    z[24]=z[1]*z[24];
    z[24]= - n<T>(13,2)*z[10] - n<T>(5,2) + z[24];
    z[23]=n<T>(1,4)*z[24] + z[23];
    z[23]=z[2]*z[23];
    z[17]= - static_cast<T>(7)+ z[17];
    z[17]=z[1]*z[17];
    z[17]= - static_cast<T>(9)+ z[17];
    z[17]=z[1]*z[17];
    z[17]=9*z[10] - static_cast<T>(3)+ z[17];
    z[12]=3*z[21] + z[12] + n<T>(1,8)*z[17] + z[23];
    z[12]=z[3]*z[12];
    z[17]= - static_cast<T>(11)+ 15*z[10];
    z[17]=z[17]*z[18];
    z[21]= - z[7] + static_cast<T>(1)- 3*z[8];
    z[23]= - z[8] + n<T>(3,2)*z[7];
    z[23]=z[1]*z[23];
    z[24]=z[2]*z[25];
    z[17]= - n<T>(3,2)*z[24] + z[17] + n<T>(1,2)*z[21] + z[23];
    z[21]= - n<T>(1,4)*z[7] + z[25];
    z[21]=z[5]*z[21];
    z[17]=n<T>(1,2)*z[17] + z[21];
    z[17]=z[17]*z[27];
    z[21]=z[8] - 3*z[7];
    z[13]=z[21]*z[13];
    z[13]=z[13] + n<T>(3,2)*z[8] - z[7];
    z[13]=z[1]*z[13];
    z[21]= - n<T>(11,2) + z[20];
    z[20]=z[21]*z[20];
    z[13]=z[20] + z[13] + z[29] + n<T>(13,2) + z[8];
    z[20]=n<T>(9,8) - z[10];
    z[20]=z[10]*z[20];
    z[16]= - n<T>(3,8)*z[16] + static_cast<T>(1)+ z[20];
    z[16]=z[2]*z[16];
    z[13]=z[17] + n<T>(1,4)*z[13] + z[16];
    z[13]=z[5]*z[13];
    z[16]= - static_cast<T>(5)+ z[1];
    z[16]=z[1]*z[16];
    z[17]= - static_cast<T>(27)+ 7*z[10];
    z[17]=z[10]*z[17];
    z[16]=z[17] - static_cast<T>(13)+ z[16];
    z[17]= - z[2]*z[19];
    z[14]=z[17] - z[14];
    z[14]=z[18]*z[14];
    z[14]=n<T>(13,2) + 5*z[11] + z[14];
    z[14]=z[14]*z[26];
    z[17]=z[10]*z[19];
    z[14]=z[14] - n<T>(5,4)*z[17] + static_cast<T>(3)- n<T>(7,4)*z[1];
    z[14]=z[2]*z[14];
    z[14]=n<T>(1,4)*z[16] + z[14];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[13]=z[5]*z[13];
    z[14]= - n<T>(5,2) + z[22];
    z[14]=3*z[14] + z[18];
    z[14]=z[2]*z[14];
    z[16]=n<T>(1,8)*z[22] + n<T>(13,8) - 2*z[4];
    z[16]=z[1]*z[16];
    z[17]=n<T>(5,4) + z[11];
    z[17]=z[10]*z[17];
    z[14]=n<T>(1,4)*z[14] + z[17] + z[16] + n<T>(19,8) - z[11];
    z[14]=z[2]*z[14];
    z[16]= - n<T>(3,2) + z[4];
    z[17]=n<T>(1,2) - z[4];
    z[17]=z[1]*z[17];
    z[16]=7*z[16] + z[17];
    z[16]=z[1]*z[16];
    z[16]=z[33] - n<T>(11,2) + z[16];
    z[14]=n<T>(1,4)*z[16] + z[14];
    z[14]=z[2]*z[14];
    z[16]= - static_cast<T>(1)+ z[4];
    z[16]=z[1]*z[16];
    z[16]=n<T>(1,8)*z[16] + static_cast<T>(1)- n<T>(1,2)*z[4];
    z[16]=z[1]*z[16];
    z[12]=z[12] + z[13] + z[14] - z[35] + n<T>(7,8) + z[16];
    z[12]=z[3]*z[12];

    r += z[12] + n<T>(1,2)*z[15];
 
    return r;
}

template double qg_2lha_r706(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r706(const std::array<dd_real,30>&);
#endif
