#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1015(const std::array<T,30>& k) {
  T z[78];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[9];
    z[8]=k[10];
    z[9]=k[11];
    z[10]=k[24];
    z[11]=k[3];
    z[12]=k[8];
    z[13]=k[25];
    z[14]=k[27];
    z[15]=k[13];
    z[16]=k[15];
    z[17]=k[12];
    z[18]=k[20];
    z[19]= - static_cast<T>(41)- n<T>(179,2)*z[8];
    z[19]=n<T>(1,3)*z[19] - n<T>(11,2)*z[11];
    z[20]=n<T>(1,3)*z[7];
    z[21]= - static_cast<T>(1)+ z[20];
    z[21]=z[7]*z[21];
    z[19]=n<T>(1,4)*z[19] + z[21];
    z[21]=n<T>(1,4)*z[2];
    z[19]=z[19]*z[21];
    z[22]=static_cast<T>(125)+ n<T>(83,3)*z[8];
    z[23]=npow(z[11],2);
    z[19]=z[19] + n<T>(1,16)*z[22] + z[23];
    z[22]=n<T>(1,3)*z[2];
    z[19]=z[19]*z[22];
    z[23]=n<T>(1,3)*z[11];
    z[24]=n<T>(37,8)*z[15];
    z[25]=z[24] + n<T>(1,2) + z[10];
    z[25]=z[25]*z[23];
    z[26]=n<T>(5,2)*z[17];
    z[27]= - static_cast<T>(3)+ z[26];
    z[27]=z[17]*z[27];
    z[27]= - n<T>(37,3)*z[15] + n<T>(13,2) + z[27];
    z[25]=n<T>(1,4)*z[27] + z[25];
    z[25]=z[11]*z[25];
    z[27]=n<T>(59,144)*z[8];
    z[28]= - z[27] + n<T>(37,12)*z[15] - n<T>(37,24) + z[17];
    z[19]=z[19] + n<T>(1,2)*z[28] + z[25];
    z[25]=n<T>(1,2)*z[6];
    z[19]=z[19]*z[25];
    z[28]=n<T>(1,16)*z[17];
    z[29]=n<T>(91,12) - z[10];
    z[29]=n<T>(1,3)*z[29] + z[28];
    z[30]=n<T>(1,8)*z[8];
    z[31]=n<T>(319,72) - z[18];
    z[31]=z[31]*z[30];
    z[32]=n<T>(1,8)*z[9];
    z[33]= - n<T>(23,8) + z[18];
    z[33]=z[33]*z[32];
    z[34]=n<T>(2,3)*z[15];
    z[35]=npow(z[17],2);
    z[36]= - n<T>(1,3) + z[35];
    z[36]=n<T>(1,4)*z[36] + z[34];
    z[36]=z[11]*z[36];
    z[37]=n<T>(1,2)*z[7];
    z[38]=z[37] - 1;
    z[39]=n<T>(1,6)*z[7];
    z[40]=z[38]*z[39];
    z[41]= - n<T>(47,4) - 71*z[8];
    z[40]=z[40] + n<T>(1,24)*z[41] - z[11];
    z[41]=7*z[7];
    z[42]=n<T>(11,3) + z[41];
    z[42]=z[2]*z[42];
    z[40]=n<T>(1,3)*z[40] + n<T>(1,32)*z[42];
    z[42]=n<T>(1,2)*z[2];
    z[40]=z[40]*z[42];
    z[43]=n<T>(1,16)*z[4];
    z[19]=z[19] + z[40] + z[36] + z[33] + z[43] + z[31] + n<T>(1,2)*z[29] - 
    z[34];
    z[19]=z[6]*z[19];
    z[29]=n<T>(13,2)*z[9];
    z[31]= - static_cast<T>(17)+ z[29];
    z[31]=z[31]*z[32];
    z[33]=n<T>(5,6)*z[7];
    z[34]=n<T>(13,2)*z[7];
    z[36]= - n<T>(5,3) - z[34];
    z[36]=z[36]*z[33];
    z[40]=npow(z[9],2);
    z[36]=z[36] - static_cast<T>(3)- n<T>(13,2)*z[40];
    z[44]=n<T>(1,8)*z[2];
    z[36]=z[36]*z[44];
    z[45]=n<T>(1,3)*z[10];
    z[46]=z[45] - n<T>(5,8);
    z[47]=n<T>(1,8)*z[1];
    z[48]=n<T>(35,9) - n<T>(3,2)*z[7];
    z[48]=z[7]*z[48];
    z[31]=z[36] + n<T>(5,16)*z[48] + z[31] - z[47] - z[46];
    z[31]=z[2]*z[31];
    z[34]= - static_cast<T>(71)+ z[34];
    z[34]=z[34]*z[37];
    z[36]=13*z[9];
    z[48]=3*z[1];
    z[49]=static_cast<T>(5)- z[48];
    z[34]=z[34] + 3*z[49] + z[36];
    z[49]=static_cast<T>(47)+ 11*z[7];
    z[49]=z[49]*z[37];
    z[49]=z[49] - z[36] + n<T>(1,2) + z[1];
    z[50]=n<T>(1,8)*z[7];
    z[51]= - static_cast<T>(5)- n<T>(19,2)*z[7];
    z[51]=z[51]*z[50];
    z[51]= - static_cast<T>(1)+ z[51];
    z[51]=z[2]*z[51];
    z[49]=n<T>(1,4)*z[49] + z[51];
    z[49]=z[2]*z[49];
    z[34]=n<T>(1,4)*z[34] + z[49];
    z[34]=z[2]*z[34];
    z[49]=5*z[4];
    z[51]=z[1] - 1;
    z[52]=z[51]*z[49];
    z[53]=5*z[7];
    z[54]= - static_cast<T>(5)+ z[4];
    z[54]=z[54]*z[53];
    z[52]=z[54] + z[52] + static_cast<T>(25)- 17*z[1];
    z[54]= - 35*z[7] + static_cast<T>(35)- 11*z[1];
    z[55]=z[53] - static_cast<T>(5)+ z[1];
    z[55]=z[2]*z[55];
    z[54]=n<T>(1,2)*z[54] + z[55];
    z[54]=z[2]*z[54];
    z[55]=15*z[7];
    z[56]=z[55] - static_cast<T>(15)+ 7*z[1];
    z[54]=n<T>(3,2)*z[56] + z[54];
    z[54]=z[2]*z[54];
    z[52]=n<T>(1,2)*z[52] + z[54];
    z[54]=n<T>(1,2)*z[3];
    z[52]=z[52]*z[54];
    z[56]= - n<T>(31,2) + 15*z[1];
    z[57]=static_cast<T>(1)- n<T>(7,4)*z[1];
    z[57]=z[4]*z[57];
    z[58]=n<T>(47,2) - 9*z[4];
    z[58]=n<T>(1,4)*z[58] - z[7];
    z[58]=z[7]*z[58];
    z[34]=z[52] + z[34] + z[58] + n<T>(1,4)*z[56] + z[57];
    z[34]=z[34]*z[54];
    z[52]=n<T>(1,4)*z[4];
    z[56]=n<T>(3,2) + z[1];
    z[56]=z[56]*z[52];
    z[57]=n<T>(35,36) - z[2];
    z[57]=z[2]*z[57];
    z[57]=n<T>(1,36) + z[57];
    z[57]=z[57]*z[25];
    z[58]=n<T>(1,2)*z[9];
    z[59]=n<T>(55,48)*z[7] - n<T>(5,12) + z[4];
    z[59]=z[7]*z[59];
    z[31]=z[34] + z[57] + z[31] + z[59] + z[58] + z[56] - z[47] + z[46];
    z[31]=z[3]*z[31];
    z[34]=n<T>(59,9) - n<T>(39,2)*z[2];
    z[34]=z[34]*z[21];
    z[34]= - n<T>(13,9) + z[34];
    z[46]=n<T>(1,8)*z[6];
    z[34]=z[34]*z[46];
    z[47]= - static_cast<T>(1)+ z[42];
    z[47]=z[2]*z[47];
    z[34]=z[34] + n<T>(7,32)*z[47] - z[32] - n<T>(13,9) - z[43];
    z[34]=z[6]*z[34];
    z[47]=z[2]*z[7];
    z[56]=n<T>(23,2)*z[40];
    z[57]=static_cast<T>(1)- z[56];
    z[59]= - n<T>(1,3) + z[50];
    z[59]=z[7]*z[59];
    z[57]= - n<T>(7,32)*z[47] + n<T>(1,4)*z[57] + z[59];
    z[57]=z[57]*z[42];
    z[59]= - n<T>(1,4) + z[9];
    z[59]=z[9]*z[59];
    z[60]=n<T>(287,6) - 37*z[4];
    z[60]=n<T>(1,2)*z[60] - 19*z[7];
    z[60]=z[7]*z[60];
    z[31]=z[31] + z[34] + z[57] + n<T>(1,24)*z[60] + n<T>(5,8)*z[59] - z[45] - 
    z[52];
    z[31]=z[3]*z[31];
    z[34]=z[38]*z[7];
    z[38]= - n<T>(1,2) - 17*z[8];
    z[38]=n<T>(1,8)*z[38] + z[34];
    z[57]=n<T>(1,9)*z[2];
    z[38]=z[38]*z[57];
    z[59]=n<T>(1,9)*z[7];
    z[60]=n<T>(11,3)*z[11] + n<T>(49,9) + 15*z[8];
    z[38]=z[38] + n<T>(1,8)*z[60] + z[59];
    z[38]=z[38]*z[21];
    z[60]=n<T>(1,4)*z[17];
    z[61]= - static_cast<T>(1)+ n<T>(5,4)*z[17];
    z[61]=z[61]*z[60];
    z[62]=n<T>(1,2)*z[8];
    z[63]= - n<T>(7,9) + n<T>(1,8)*z[18];
    z[63]=z[63]*z[62];
    z[64]=n<T>(1,16)*z[9];
    z[65]=n<T>(23,2) - z[18];
    z[65]=z[65]*z[64];
    z[66]=n<T>(1,4)*z[11];
    z[67]= - z[23] + n<T>(1,8)*z[4] + n<T>(1,3) - n<T>(25,8)*z[15];
    z[67]=z[67]*z[66];
    z[68]= - n<T>(103,16) + z[10];
    z[38]=z[38] + z[67] + z[65] - z[43] + z[63] + n<T>(25,16)*z[15] + n<T>(1,3)*
    z[68] + z[61];
    z[38]=z[6]*z[38];
    z[43]=3*z[18];
    z[61]=z[43] - 5;
    z[63]=z[61]*z[43];
    z[63]=n<T>(337,9) + z[63];
    z[63]=z[63]*z[30];
    z[65]=n<T>(3,2)*z[18];
    z[61]= - z[61]*z[65];
    z[67]=static_cast<T>(23)- n<T>(9,2)*z[18];
    z[67]=z[9]*z[67];
    z[61]=z[61] + z[67];
    z[67]=n<T>(1,4)*z[9];
    z[61]=z[61]*z[67];
    z[68]=static_cast<T>(1)+ z[7];
    z[68]=z[7]*z[68];
    z[68]= - n<T>(59,8)*z[8] + z[68];
    z[57]=z[68]*z[57];
    z[68]=3*z[35];
    z[57]=z[57] - z[59] + z[61] - n<T>(25,12)*z[4] + z[63] + n<T>(85,12)*z[15]
    - n<T>(67,72) + z[68];
    z[38]=n<T>(1,8)*z[57] + z[38];
    z[38]=z[6]*z[38];
    z[57]=n<T>(1,4)*z[7];
    z[61]= - static_cast<T>(127)+ z[53];
    z[61]=z[61]*z[57];
    z[61]=z[61] + n<T>(17,2) - z[36];
    z[55]=static_cast<T>(53)+ z[55];
    z[55]=z[55]*z[50];
    z[55]=static_cast<T>(1)+ z[55];
    z[55]=z[2]*z[55];
    z[55]=n<T>(1,2)*z[61] + z[55];
    z[55]=z[55]*z[21];
    z[61]= - static_cast<T>(1)+ z[52];
    z[61]=z[61]*z[53];
    z[63]= - static_cast<T>(13)+ 25*z[7];
    z[69]=static_cast<T>(1)- n<T>(5,2)*z[7];
    z[69]=z[2]*z[69];
    z[63]=n<T>(1,4)*z[63] + z[69];
    z[63]=z[2]*z[63];
    z[69]=static_cast<T>(7)- n<T>(5,2)*z[4];
    z[61]=z[63] + n<T>(1,2)*z[69] + z[61];
    z[61]=z[3]*z[61];
    z[63]=2*z[4];
    z[69]=n<T>(5,8)*z[7];
    z[70]= - z[69] + n<T>(7,2) - z[63];
    z[70]=z[7]*z[70];
    z[71]=static_cast<T>(31)- 37*z[2];
    z[71]=z[71]*z[42];
    z[71]=static_cast<T>(3)+ z[71];
    z[72]=n<T>(1,16)*z[6];
    z[71]=z[71]*z[72];
    z[73]=n<T>(13,4)*z[9];
    z[74]=z[73] - n<T>(19,4) + 3*z[4];
    z[55]=z[61] + z[71] + z[55] + n<T>(3,4)*z[74] + z[70];
    z[55]=z[3]*z[55];
    z[61]=n<T>(7,2)*z[7];
    z[70]=n<T>(1,3) - z[61];
    z[70]=z[70]*z[39];
    z[71]=z[21]*z[7];
    z[74]=static_cast<T>(1)- n<T>(15,4)*z[7];
    z[74]=z[74]*z[71];
    z[70]=z[74] + z[70] + static_cast<T>(1)- n<T>(13,4)*z[40];
    z[70]=z[70]*z[21];
    z[74]= - static_cast<T>(11)- n<T>(37,2)*z[2];
    z[74]=z[74]*z[42];
    z[74]= - n<T>(1,9) + z[74];
    z[74]=z[74]*z[72];
    z[75]= - n<T>(63,16)*z[9] - n<T>(7,4)*z[4] + n<T>(11,6) - z[10];
    z[76]=n<T>(1,16)*z[2];
    z[77]=n<T>(10,9) - z[76];
    z[77]=z[2]*z[77];
    z[74]=z[74] + n<T>(1,2)*z[75] + z[77];
    z[74]=z[6]*z[74];
    z[75]= - static_cast<T>(47)+ n<T>(39,2)*z[9];
    z[75]=z[75]*z[64];
    z[49]=n<T>(31,6)*z[7] + n<T>(23,12) + z[49];
    z[49]=z[49]*z[57];
    z[49]=z[55] + z[74] + z[70] + z[49] + z[75] - n<T>(9,8)*z[4] + n<T>(13,32)
    - z[45];
    z[49]=z[3]*z[49];
    z[24]= - n<T>(17,2)*z[7] - n<T>(61,8)*z[4] - n<T>(1,3) + z[24];
    z[24]=z[24]*z[20];
    z[55]= - z[56] - n<T>(3,4)*z[17] + z[4];
    z[24]=n<T>(1,2)*z[55] + z[24];
    z[55]= - n<T>(1,3) - n<T>(1,16)*z[7];
    z[55]=z[2]*z[55]*z[20];
    z[24]=n<T>(1,2)*z[24] + z[55];
    z[55]=n<T>(467,18) - z[2];
    z[55]=z[55]*z[42];
    z[55]=z[55] + z[58] + z[52] - n<T>(25,4)*z[15] + n<T>(851,36) + 3*z[17];
    z[46]=z[55]*z[46];
    z[55]=n<T>(5,16)*z[9];
    z[56]=n<T>(5,2) - 3*z[9];
    z[56]=z[56]*z[55];
    z[70]=n<T>(7,3) + 2*z[10];
    z[46]=z[46] + n<T>(1,18)*z[2] + z[56] + n<T>(109,96)*z[4] - n<T>(85,96)*z[15] + 
   n<T>(1,3)*z[70] + n<T>(7,32)*z[17];
    z[46]=z[6]*z[46];
    z[24]=z[49] + n<T>(1,2)*z[24] + z[46];
    z[24]=z[3]*z[24];
    z[46]=5*z[9];
    z[49]=static_cast<T>(13)+ z[46];
    z[49]=z[49]*z[67];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[17]*z[26];
    z[26]= - n<T>(109,3) + z[26];
    z[26]=z[21] + z[49] - n<T>(7,3)*z[4] + n<T>(1,4)*z[26] + n<T>(35,3)*z[15];
    z[26]=z[6]*z[26];
    z[49]=z[4] + z[15];
    z[26]=z[26] + z[21] + n<T>(23,4)*z[40] - n<T>(17,36) + z[35] - n<T>(5,6)*z[49];
    z[49]=n<T>(1,4)*z[6];
    z[26]=z[26]*z[49];
    z[56]=n<T>(89,12) - 7*z[4];
    z[56]=n<T>(1,4)*z[56] - z[20];
    z[56]=z[7]*z[56];
    z[70]= - static_cast<T>(7)- z[50];
    z[70]=z[70]*z[71];
    z[74]= - static_cast<T>(1)- n<T>(23,2)*z[2];
    z[72]=z[74]*z[72];
    z[72]=z[72] + n<T>(3,2)*z[2] - z[73] + n<T>(145,48) - z[4];
    z[72]=z[6]*z[72];
    z[74]= - static_cast<T>(3)+ z[4];
    z[74]=z[74]*z[37];
    z[74]=z[74] + z[47];
    z[75]=static_cast<T>(1)- z[2];
    z[75]=z[6]*z[75];
    z[74]=n<T>(5,2)*z[74] + z[75];
    z[74]=z[3]*z[74];
    z[75]= - n<T>(1,2) - z[36];
    z[56]=z[74] + z[72] + z[70] + n<T>(1,8)*z[75] + z[56];
    z[56]=z[3]*z[56];
    z[70]=static_cast<T>(15)- z[29];
    z[70]=z[70]*z[67];
    z[72]=n<T>(113,6) + z[10];
    z[72]=n<T>(27,16)*z[2] + n<T>(21,16)*z[9] + n<T>(7,12)*z[4] - n<T>(35,12)*z[15] + n<T>(1,3)*z[72] + n<T>(1,2)*z[17];
    z[72]=z[72]*z[25];
    z[74]= - n<T>(53,12) + z[10];
    z[44]=z[72] + z[44] + z[70] + n<T>(29,24)*z[4] + n<T>(5,24)*z[15] + n<T>(1,3)*
    z[74] + n<T>(5,16)*z[17];
    z[44]=z[6]*z[44];
    z[63]=n<T>(29,16)*z[7] + z[63] + n<T>(61,24) - z[15];
    z[63]=z[63]*z[20];
    z[70]= - static_cast<T>(1)+ z[57];
    z[70]=z[70]*z[47];
    z[72]=13*z[40];
    z[74]=z[17] - z[72];
    z[44]=z[56] + z[44] + n<T>(1,8)*z[70] + n<T>(1,16)*z[74] + z[63];
    z[44]=z[3]*z[44];
    z[56]=n<T>(1,2)*z[4];
    z[63]= - z[69] - z[56] + n<T>(17,48) + z[15];
    z[63]=z[63]*z[20];
    z[26]=z[44] + z[26] - n<T>(1,16)*z[47] + n<T>(3,32)*z[35] + z[63];
    z[26]=z[3]*z[26];
    z[44]= - z[4] + 3*z[15];
    z[47]= - static_cast<T>(21)+ z[35];
    z[47]=n<T>(13,16)*z[40] + n<T>(1,16)*z[47] + z[44];
    z[47]=z[6]*z[47];
    z[63]=z[52] + z[15];
    z[69]= - static_cast<T>(1)+ z[35];
    z[47]=z[47] + n<T>(13,8)*z[40] + n<T>(1,8)*z[69] - z[63];
    z[47]=z[6]*z[47];
    z[69]=static_cast<T>(15)+ z[17];
    z[69]=z[73] + n<T>(1,4)*z[69] - z[44];
    z[25]=z[69]*z[25];
    z[69]=z[15] + n<T>(3,2)*z[4];
    z[70]= - static_cast<T>(7)+ z[17];
    z[25]=z[25] + z[73] + n<T>(1,4)*z[70] + z[69];
    z[25]=z[6]*z[25];
    z[69]= - z[50] + n<T>(7,4) - z[69];
    z[69]=z[7]*z[69];
    z[70]=z[56] - 1;
    z[53]=z[70]*z[53];
    z[70]= - 5*z[70] - z[6];
    z[70]=z[6]*z[70];
    z[53]=z[53] + z[70];
    z[53]=z[53]*z[54];
    z[70]=n<T>(1,8)*z[17];
    z[25]=z[53] + z[25] + z[70] + z[69];
    z[25]=z[3]*z[25];
    z[53]=n<T>(3,16)*z[7] + n<T>(1,8) + z[63];
    z[53]=z[7]*z[53];
    z[25]=z[25] + z[47] + n<T>(1,16)*z[35] + z[53];
    z[25]=z[3]*z[25];
    z[44]= - z[6]*z[44];
    z[44]= - n<T>(1,4) + z[44];
    z[44]=z[6]*z[44];
    z[44]=z[57] + z[44];
    z[25]=n<T>(1,2)*z[44] + z[25];
    z[25]=z[3]*z[25];
    z[44]=z[4] - z[15];
    z[47]= - z[44]*z[37];
    z[53]=z[44]*z[6];
    z[63]=n<T>(1,2) - z[6];
    z[63]=z[63]*z[53];
    z[69]=z[6] - 1;
    z[53]=z[69]*z[53];
    z[69]=z[7]*z[44];
    z[53]=z[69] + z[53];
    z[53]=z[53]*z[54];
    z[47]=z[53] + z[47] + z[63];
    z[47]=z[3]*z[47];
    z[53]=npow(z[6],2);
    z[44]=z[44]*z[53];
    z[44]=n<T>(1,2)*z[44] + z[47];
    z[44]=z[44]*npow(z[3],2);
    z[47]=z[8]*z[53];
    z[44]=n<T>(1,16)*z[47] + z[44];
    z[47]=n<T>(1,2)*z[5];
    z[44]=z[44]*z[47];
    z[53]=z[59] + z[30];
    z[54]= - n<T>(1,16) - z[8];
    z[59]= - z[2]*z[30];
    z[54]=n<T>(1,9)*z[54] + z[59];
    z[54]=z[6]*z[54];
    z[54]=z[54] - z[53];
    z[54]=z[6]*z[54];
    z[59]=npow(z[7],2);
    z[54]= - n<T>(5,72)*z[59] + z[54];
    z[25]=z[44] + n<T>(1,2)*z[54] + z[25];
    z[25]=z[25]*z[47];
    z[44]= - static_cast<T>(1)+ n<T>(1,2)*z[18];
    z[47]= - z[44]*z[43];
    z[47]= - n<T>(181,18) + z[47];
    z[47]=z[47]*z[30];
    z[44]=z[44] + z[58];
    z[54]=z[9]*z[18];
    z[44]=z[44]*z[54];
    z[63]=static_cast<T>(1)+ 25*z[8];
    z[63]=n<T>(1,8)*z[63] + z[7];
    z[69]=z[8]*z[76];
    z[63]=n<T>(1,9)*z[63] + z[69];
    z[63]=z[2]*z[63];
    z[69]= - n<T>(1,3) - n<T>(35,2)*z[15];
    z[44]=z[63] - n<T>(11,48)*z[11] + n<T>(3,8)*z[44] + n<T>(7,6)*z[4] + n<T>(1,3)*z[69]
    + z[47];
    z[44]=z[6]*z[44];
    z[47]=z[2]*z[53];
    z[53]= - static_cast<T>(11)+ n<T>(41,3)*z[8];
    z[53]=n<T>(1,8)*z[53] - z[7];
    z[44]=z[44] + n<T>(1,6)*z[53] + z[47];
    z[44]=z[6]*z[44];
    z[33]=static_cast<T>(1)- z[33];
    z[33]=z[33]*z[20];
    z[33]=z[30] + z[33];
    z[33]=n<T>(1,2)*z[33] + z[44];
    z[25]=z[25] + n<T>(1,4)*z[33] + z[26];
    z[25]=z[5]*z[25];
    z[26]=n<T>(13,2)*z[4] + n<T>(23,3) - n<T>(37,2)*z[15];
    z[26]=n<T>(1,8)*z[26] + z[20];
    z[26]=z[26]*z[20];
    z[33]=z[68] - n<T>(25,18)*z[8];
    z[26]=n<T>(1,16)*z[33] + z[26];
    z[24]=z[25] + z[24] + n<T>(1,2)*z[26] + z[38];
    z[24]=z[5]*z[24];
    z[25]=z[18] - n<T>(3,2);
    z[26]= - z[25]*z[65];
    z[26]= - n<T>(13,3) + z[26];
    z[26]=z[26]*z[62];
    z[25]=z[25]*z[43];
    z[33]= - static_cast<T>(23)+ z[43];
    z[33]=z[9]*z[33];
    z[25]=z[25] + z[33];
    z[25]=z[25]*z[67];
    z[33]=z[35]*z[66];
    z[25]=z[33] + z[25] - z[17] + z[26];
    z[26]= - z[11] + 1;
    z[26]=z[15]*z[26];
    z[26]=n<T>(7,16)*z[7] + n<T>(13,16)*z[4] - n<T>(95,96) + z[26];
    z[26]=z[26]*z[20];
    z[19]=z[24] + z[31] + z[19] + n<T>(1,8)*z[25] + z[26];
    z[19]=z[5]*z[19];
    z[24]=z[13] - n<T>(1,3);
    z[25]=n<T>(1,3)*z[12];
    z[24]= - z[25] + n<T>(1,2)*z[24];
    z[26]=n<T>(1,12)*z[7];
    z[31]= - z[8] + z[7];
    z[31]=z[31]*z[26];
    z[31]=z[31] + n<T>(69,32)*z[40] + z[24];
    z[31]=z[2]*z[31];
    z[33]= - static_cast<T>(1)- n<T>(23,2)*z[9];
    z[33]=z[33]*z[32];
    z[35]=z[11]*z[40];
    z[31]=z[31] - z[39] + n<T>(23,32)*z[35] + z[33] + n<T>(1,6)*z[8] - z[24];
    z[31]=z[2]*z[31];
    z[33]=z[18] - 1;
    z[35]=z[8]*z[33]*z[65];
    z[33]= - z[33] - z[9];
    z[33]=z[33]*z[54];
    z[33]=n<T>(3,2)*z[33] - z[17] + z[35];
    z[33]=z[11]*z[33];
    z[35]= - n<T>(95,36) + z[18];
    z[30]=z[35]*z[30];
    z[35]=n<T>(3,4)*z[9] + static_cast<T>(5)- z[18];
    z[35]=z[35]*z[32];
    z[38]=z[41] - static_cast<T>(11)+ 13*z[4];
    z[38]=z[7]*z[38];
    z[30]=z[31] + n<T>(1,48)*z[38] + n<T>(1,16)*z[33] + z[35] + z[30] + n<T>(1,16)
    + z[45];
    z[31]=z[15] + z[12];
    z[23]=z[23]*z[31]*z[14];
    z[31]=n<T>(1,2)*z[16];
    z[33]=z[31] - 1;
    z[33]=z[33]*z[16];
    z[35]= - z[17]*z[31];
    z[35]= - z[33] + z[35];
    z[35]=z[35]*z[28];
    z[38]=n<T>(1,3)*z[15];
    z[43]=n<T>(101,32) - 2*z[14];
    z[43]=z[43]*z[38];
    z[33]=n<T>(35,6) + z[33];
    z[33]=n<T>(1,16)*z[33] - n<T>(2,3)*z[14];
    z[33]=z[12]*z[33];
    z[33]=z[23] + z[43] + z[33] + z[35];
    z[33]=z[11]*z[33];
    z[35]= - static_cast<T>(1)+ z[14];
    z[25]=z[35]*z[25];
    z[35]= - static_cast<T>(1)+ n<T>(5,8)*z[17];
    z[35]=z[35]*z[60];
    z[43]= - n<T>(101,16) + z[14];
    z[43]=z[43]*z[38];
    z[44]=n<T>(43,12) + z[13];
    z[25]=z[33] + z[43] + z[35] + n<T>(1,4)*z[44] + z[25];
    z[25]=z[11]*z[25];
    z[33]=z[70] - z[13];
    z[35]=n<T>(101,48)*z[15] - n<T>(59,48) + z[33];
    z[25]=n<T>(1,2)*z[35] + z[25];
    z[25]=z[11]*z[25];
    z[24]=z[24]*z[11];
    z[35]=static_cast<T>(1)+ n<T>(1,8)*z[12];
    z[43]= - n<T>(1,6)*z[35] + z[24];
    z[43]=z[11]*z[43];
    z[44]= - n<T>(5,8) + z[8];
    z[44]=z[7]*z[44];
    z[44]= - z[8] + z[44];
    z[44]=z[44]*z[71];
    z[47]=n<T>(7,32) - z[8];
    z[47]=z[7]*z[47];
    z[44]=z[44] + z[8] + z[47];
    z[22]=z[44]*z[22];
    z[22]=z[22] - n<T>(73,96) + z[43];
    z[22]=z[22]*z[42];
    z[43]= - n<T>(19,32) - z[13];
    z[43]=n<T>(1,2)*z[43] + z[24];
    z[43]=z[11]*z[43];
    z[22]=z[22] + n<T>(3,64) + z[43];
    z[22]=z[2]*z[22];
    z[43]= - static_cast<T>(1)+ z[13];
    z[22]=z[22] + n<T>(1,4)*z[43] + z[25];
    z[22]=z[6]*z[22];
    z[25]= - z[31] + z[17];
    z[25]=z[25]*z[28];
    z[28]= - n<T>(37,32) + z[14];
    z[28]=z[28]*z[38];
    z[31]= - n<T>(35,3) + z[16];
    z[31]=n<T>(1,32)*z[31] + n<T>(1,3)*z[14];
    z[31]=z[12]*z[31];
    z[23]= - z[23] + z[28] + z[31] + z[25];
    z[23]=z[11]*z[23];
    z[25]=n<T>(37,24)*z[15] - n<T>(35,24) + z[33];
    z[23]=n<T>(1,4)*z[25] + z[23];
    z[23]=z[11]*z[23];
    z[25]=n<T>(41,24) - z[8];
    z[25]=z[25]*z[50];
    z[28]=n<T>(1,3)*z[8];
    z[31]= - n<T>(3,4) + z[28];
    z[31]=z[31]*z[37];
    z[28]=z[31] - n<T>(7,16) - z[28];
    z[28]=z[28]*z[71];
    z[31]=n<T>(1,3)*z[35] + z[8];
    z[25]=z[28] + z[25] + n<T>(1,4)*z[31] - z[24];
    z[25]=z[2]*z[25];
    z[28]= - n<T>(107,24)*z[8] - n<T>(1,3) - n<T>(1,4)*z[12];
    z[24]=n<T>(1,12)*z[28] - z[24];
    z[24]=n<T>(1,2)*z[24] + z[25];
    z[24]=z[2]*z[24];
    z[25]=z[27] - n<T>(95,48) + z[13];
    z[22]=z[22] + z[24] + n<T>(1,4)*z[25] + z[23];
    z[22]=z[6]*z[22];
    z[23]=n<T>(47,2) - z[36];
    z[23]=z[23]*z[32];
    z[24]=z[72] + n<T>(7,2)*z[59];
    z[24]=z[24]*z[76];
    z[23]=z[24] + n<T>(7,8)*z[34] + z[23] + n<T>(1,4) + z[45];
    z[23]=z[23]*z[42];
    z[24]= - static_cast<T>(21)+ z[29];
    z[24]=z[24]*z[64];
    z[25]=n<T>(1,4)*z[1];
    z[27]= - n<T>(77,32) - z[10];
    z[28]= - n<T>(1,3) - n<T>(7,32)*z[7];
    z[28]=z[7]*z[28];
    z[23]=z[23] + n<T>(11,6)*z[28] + z[24] + n<T>(1,3)*z[27] + z[25];
    z[23]=z[2]*z[23];
    z[24]=n<T>(173,6) - z[41];
    z[24]=z[24]*z[26];
    z[24]=z[24] - z[55] - n<T>(5,4)*z[1] + n<T>(17,8) + z[45];
    z[23]=n<T>(1,2)*z[24] + z[23];
    z[23]=z[2]*z[23];
    z[24]=static_cast<T>(55)+ z[41];
    z[24]=z[24]*z[57];
    z[27]=npow(z[1],2);
    z[24]=z[24] + z[29] + n<T>(27,2) - z[27];
    z[28]= - static_cast<T>(31)- 49*z[7];
    z[26]=z[28]*z[26];
    z[26]=z[26] - n<T>(53,6) - z[36];
    z[28]= - static_cast<T>(1)+ z[7];
    z[28]=z[7]*z[28];
    z[28]=n<T>(7,8)*z[28] + static_cast<T>(1)+ z[73];
    z[28]=z[2]*z[28];
    z[26]=n<T>(1,2)*z[26] + z[28];
    z[26]=z[2]*z[26];
    z[24]=n<T>(1,2)*z[24] + z[26];
    z[24]=z[2]*z[24];
    z[26]= - static_cast<T>(59)+ z[41];
    z[26]=z[26]*z[57];
    z[28]= - n<T>(5,2) + z[27];
    z[26]=3*z[28] + z[26];
    z[24]=n<T>(1,2)*z[26] + z[24];
    z[24]=z[2]*z[24];
    z[26]=static_cast<T>(1)+ z[27];
    z[26]=z[4]*z[26];
    z[26]=z[26] - n<T>(1,6) - 3*z[27];
    z[27]= - n<T>(7,12)*z[7] + n<T>(11,3) - z[4];
    z[27]=z[7]*z[27];
    z[24]=z[24] + n<T>(1,2)*z[26] + z[27];
    z[26]= - z[1]*z[51];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[26]*z[52];
    z[27]= - static_cast<T>(3)+ z[56];
    z[27]=z[27]*z[37];
    z[28]= - n<T>(3,2) + z[1];
    z[28]=z[1]*z[28];
    z[26]=z[27] + z[26] + n<T>(3,2) + z[28];
    z[27]=static_cast<T>(7)- z[48];
    z[27]=z[1]*z[27];
    z[27]=z[41] - static_cast<T>(7)+ z[27];
    z[28]=static_cast<T>(9)- z[1];
    z[28]=z[1]*z[28];
    z[28]=9*z[7] - static_cast<T>(9)+ z[28];
    z[29]= - z[7] - z[51];
    z[29]=z[2]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[29];
    z[28]=z[28]*z[21];
    z[29]= - static_cast<T>(2)+ n<T>(1,2)*z[1];
    z[29]=z[1]*z[29];
    z[28]=z[28] - 2*z[7] + static_cast<T>(2)+ z[29];
    z[28]=z[2]*z[28];
    z[27]=n<T>(1,4)*z[27] + z[28];
    z[27]=z[2]*z[27];
    z[26]=n<T>(1,2)*z[26] + z[27];
    z[26]=z[3]*z[26];
    z[24]=n<T>(1,4)*z[24] + z[26];
    z[24]=z[3]*z[24];
    z[26]=n<T>(35,32)*z[7];
    z[27]=z[26] - n<T>(4,3) + n<T>(7,8)*z[4];
    z[20]=z[27]*z[20];
    z[25]= - n<T>(1,3) - z[25];
    z[25]=z[4]*z[25];
    z[25]=z[25] - n<T>(7,16) + z[1];
    z[20]=z[24] + z[23] + n<T>(1,2)*z[25] + z[20];
    z[20]=z[3]*z[20];
    z[23]=n<T>(19,2) - 23*z[9];
    z[23]=z[23]*z[32];
    z[24]=n<T>(1,2) + z[7];
    z[24]=z[7]*z[24];
    z[24]=9*z[40] + n<T>(7,4)*z[24];
    z[24]=z[24]*z[21];
    z[23]=z[24] - z[26] + z[23] - n<T>(1,4) + z[45];
    z[23]=z[2]*z[23];
    z[24]=n<T>(3,2) + z[9];
    z[24]=z[24]*z[46];
    z[25]= - n<T>(221,3) + z[41];
    z[25]=z[25]*z[39];
    z[24]=z[25] + n<T>(17,6) + z[24];
    z[23]=n<T>(1,8)*z[24] + z[23];
    z[23]=z[2]*z[23];
    z[24]= - z[61] + n<T>(25,3) - n<T>(29,8)*z[4];
    z[24]=z[24]*z[39];
    z[25]=static_cast<T>(3)- z[2];
    z[25]=z[2]*z[25];
    z[25]= - n<T>(115,9) + n<T>(7,2)*z[25];
    z[21]=z[25]*z[21];
    z[21]=n<T>(13,9) + z[21];
    z[21]=z[21]*z[49];
    z[21]=z[21] + z[23] + z[24] + n<T>(1,3)*z[4] - n<T>(7,16) - z[58] - z[45];
    z[20]=n<T>(1,2)*z[21] + z[20];
    z[20]=z[3]*z[20];

    r += z[19] + z[20] + z[22] + n<T>(1,2)*z[30];
 
    return r;
}

template double qg_2lha_r1015(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1015(const std::array<dd_real,30>&);
#endif
