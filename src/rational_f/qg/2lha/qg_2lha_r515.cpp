#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r515(const std::array<T,30>& k) {
  T z[61];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[2];
    z[5]=k[7];
    z[6]=k[11];
    z[7]=k[3];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=k[5];
    z[11]=k[14];
    z[12]=k[8];
    z[13]=5*z[11];
    z[14]=n<T>(5,4) - z[11];
    z[14]=z[14]*z[13];
    z[15]=n<T>(55,32) - z[10];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[15]=n<T>(1,4)*z[2];
    z[16]=z[11] - 1;
    z[17]=z[16]*z[11];
    z[18]=n<T>(5,3)*z[17];
    z[19]=z[18] + z[10];
    z[20]= - n<T>(155,96) - z[19];
    z[20]=z[20]*z[15];
    z[21]=n<T>(1,9)*z[6];
    z[22]=z[19] - n<T>(41,96);
    z[23]=z[4]*z[22];
    z[14]=z[20] - n<T>(49,96)*z[7] + z[21] + n<T>(1,3)*z[14] + n<T>(1,4)*z[23];
    z[14]=z[2]*z[14];
    z[20]=n<T>(1,2)*z[11];
    z[23]=z[20] - 1;
    z[24]=n<T>(5,3)*z[11];
    z[24]= - z[23]*z[24];
    z[25]=2*z[6];
    z[26]=z[25] - n<T>(613,128);
    z[27]=z[6]*z[26];
    z[27]=n<T>(47,128) + z[27];
    z[27]=z[27]*z[21];
    z[28]=z[6] - 1;
    z[29]=n<T>(2,3)*z[6];
    z[30]=z[28]*z[29];
    z[30]= - n<T>(23,64) + z[30];
    z[31]=n<T>(1,3)*z[7];
    z[30]=z[30]*z[31];
    z[32]=n<T>(1,3)*z[10];
    z[33]= - n<T>(57,16) - z[32];
    z[34]=n<T>(13,32) - z[10];
    z[34]=z[4]*z[34];
    z[14]=z[14] + z[30] + z[27] + n<T>(1,12)*z[34] + n<T>(1,8)*z[33] + z[24];
    z[14]=z[2]*z[14];
    z[20]= - static_cast<T>(1)- z[20];
    z[20]=z[20]*z[13];
    z[24]=n<T>(1,32)*z[6];
    z[27]=n<T>(169,2)*z[6];
    z[30]= - static_cast<T>(121)+ z[27];
    z[30]=z[30]*z[24];
    z[20]=z[30] - n<T>(49,96) + z[20];
    z[30]=n<T>(1,2)*z[7];
    z[33]= - static_cast<T>(19)- n<T>(123,2)*z[7];
    z[33]=z[33]*z[30];
    z[34]= - 41*z[2] + n<T>(59,6) - 123*z[7];
    z[34]=z[34]*z[15];
    z[33]=z[34] - n<T>(119,9) + z[33];
    z[34]=n<T>(1,8)*z[2];
    z[33]=z[33]*z[34];
    z[35]=n<T>(1,32)*z[7];
    z[36]= - n<T>(617,6) - 41*z[7];
    z[36]=z[7]*z[36];
    z[36]=n<T>(1343,9) + z[36];
    z[36]=z[36]*z[35];
    z[20]=z[33] + n<T>(1,3)*z[20] + z[36];
    z[20]=z[2]*z[20];
    z[33]=npow(z[11],2);
    z[33]=n<T>(281,32) - 5*z[33];
    z[36]=static_cast<T>(3)+ n<T>(169,96)*z[6];
    z[36]=z[6]*z[36];
    z[37]=n<T>(427,3) - n<T>(157,4)*z[7];
    z[37]=z[7]*z[37];
    z[37]= - n<T>(547,6) + z[37];
    z[37]=z[7]*z[37];
    z[20]=z[20] + n<T>(1,24)*z[37] + n<T>(1,3)*z[33] + z[36];
    z[20]=z[2]*z[20];
    z[23]=z[23]*z[13];
    z[27]=static_cast<T>(409)+ z[27];
    z[27]=z[27]*z[24];
    z[27]=z[27] - n<T>(617,64) - z[23];
    z[33]= - n<T>(37,3) - z[12];
    z[33]=n<T>(1,8)*z[33] + z[31];
    z[33]=z[7]*z[33];
    z[33]=n<T>(1141,144) + z[33];
    z[33]=z[33]*z[30];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[33]*z[30];
    z[20]=z[20] + n<T>(1,3)*z[27] + z[33];
    z[27]=n<T>(1,16)*z[7];
    z[33]=11*z[7];
    z[36]=n<T>(1147,12) + z[33];
    z[36]=z[36]*z[27];
    z[36]=static_cast<T>(6)+ z[36];
    z[36]=z[7]*z[36];
    z[37]=33*z[7];
    z[38]=n<T>(1025,6) + z[37];
    z[38]=z[7]*z[38];
    z[38]=n<T>(785,9) + z[38];
    z[39]=n<T>(11,4)*z[2] + n<T>(241,9) + z[33];
    z[39]=z[2]*z[39];
    z[38]=n<T>(1,2)*z[38] + z[39];
    z[39]=n<T>(1,16)*z[2];
    z[38]=z[38]*z[39];
    z[36]=z[38] + n<T>(235,192) + z[36];
    z[36]=z[2]*z[36];
    z[38]=n<T>(1,4)*z[7];
    z[40]=n<T>(1513,9) + z[33];
    z[40]=z[7]*z[40];
    z[40]=static_cast<T>(247)+ z[40];
    z[40]=z[40]*z[38];
    z[40]=static_cast<T>(57)+ z[40];
    z[41]=n<T>(1,8)*z[7];
    z[40]=z[40]*z[41];
    z[42]=z[11] + n<T>(53,32)*z[6];
    z[43]=n<T>(5,3)*z[42];
    z[40]= - z[43] + z[40];
    z[36]=n<T>(1,2)*z[40] + z[36];
    z[36]=z[2]*z[36];
    z[40]=npow(z[7],2);
    z[44]=n<T>(179,3) + n<T>(61,2)*z[7];
    z[44]=z[7]*z[44];
    z[44]=n<T>(217,2) + z[44];
    z[44]=z[44]*z[40];
    z[45]=z[42] - n<T>(85,64);
    z[44]= - 5*z[45] + n<T>(1,32)*z[44];
    z[36]=n<T>(1,3)*z[44] + z[36];
    z[36]=z[2]*z[36];
    z[44]= - static_cast<T>(2)- z[38];
    z[44]=z[44]*z[31];
    z[44]= - n<T>(1,2) + z[44];
    z[44]=z[44]*z[40];
    z[46]=z[7] + n<T>(1,2);
    z[47]= - z[15] - z[46];
    z[48]=n<T>(1,3)*z[2];
    z[47]=z[47]*z[48];
    z[49]= - n<T>(2,3) - z[30];
    z[49]=z[7]*z[49];
    z[47]=z[47] - n<T>(1,12) + z[49];
    z[47]=z[2]*z[47];
    z[49]= - static_cast<T>(1)- z[31];
    z[49]=z[7]*z[49];
    z[49]= - n<T>(1,3) + z[49];
    z[49]=z[7]*z[49];
    z[47]=z[49] + z[47];
    z[47]=z[2]*z[47];
    z[44]=z[44] + z[47];
    z[44]=z[2]*z[44];
    z[47]=z[30] + 1;
    z[49]=npow(z[7],3);
    z[47]=z[47]*z[49];
    z[44]= - n<T>(1,3)*z[47] + z[44];
    z[44]=z[2]*z[44];
    z[50]=npow(z[7],4);
    z[44]= - n<T>(1,12)*z[50] + z[44];
    z[44]=z[3]*z[44];
    z[51]=n<T>(1,3) - z[12];
    z[51]=z[7]*z[51];
    z[51]= - n<T>(253,3) + 7*z[51];
    z[51]=z[7]*z[51];
    z[51]=n<T>(425,3) + z[51];
    z[51]=z[7]*z[51];
    z[51]= - n<T>(425,3) + z[51];
    z[35]=z[51]*z[35];
    z[51]=n<T>(85,32) - z[42];
    z[35]=n<T>(5,3)*z[51] + z[35];
    z[35]=n<T>(35,3)*z[44] + n<T>(1,2)*z[35] + z[36];
    z[35]=z[3]*z[35];
    z[20]=n<T>(1,2)*z[20] + z[35];
    z[20]=z[3]*z[20];
    z[35]=n<T>(1,3)*z[6];
    z[26]= - z[26]*z[35];
    z[26]= - n<T>(101,128) + z[26];
    z[26]=z[26]*z[35];
    z[36]=n<T>(1,384)*z[7];
    z[44]= - static_cast<T>(221)+ 335*z[7];
    z[44]=z[44]*z[36];
    z[51]=n<T>(335,24)*z[2] - static_cast<T>(21)+ n<T>(335,12)*z[7];
    z[39]=z[51]*z[39];
    z[17]= - n<T>(35,288) - z[17];
    z[17]=z[39] + z[44] + n<T>(5,4)*z[17] + z[26];
    z[17]=z[2]*z[17];
    z[26]=static_cast<T>(2)- z[11];
    z[26]=z[26]*z[13];
    z[39]=4*z[6];
    z[44]=n<T>(53,32) - z[39];
    z[44]=z[44]*z[35];
    z[44]= - n<T>(7,16) + z[44];
    z[44]=z[6]*z[44];
    z[51]= - n<T>(115,3) + n<T>(47,2)*z[7];
    z[51]=z[7]*z[51];
    z[26]=n<T>(5,64)*z[51] + z[44] - n<T>(203,48) + z[26];
    z[17]=n<T>(1,3)*z[26] + z[17];
    z[17]=z[2]*z[17];
    z[26]=n<T>(1,3)*z[8];
    z[44]= - n<T>(1,2) + z[26];
    z[44]=z[44]*z[30];
    z[51]=static_cast<T>(41)+ z[12];
    z[44]=z[44] + n<T>(1,64)*z[51] - z[26];
    z[44]=z[7]*z[44];
    z[51]=n<T>(317,96) + z[8];
    z[44]=n<T>(1,6)*z[51] + z[44];
    z[44]=z[44]*z[30];
    z[51]=n<T>(1,3)*z[11];
    z[52]=z[51] - 1;
    z[52]=z[52]*z[13];
    z[53]= - n<T>(121,48) - z[52];
    z[54]= - n<T>(401,128) - z[25];
    z[54]=z[54]*z[35];
    z[54]= - n<T>(485,128) + z[54];
    z[54]=z[54]*z[35];
    z[17]=z[20] + z[17] + z[44] + n<T>(1,4)*z[53] + z[54];
    z[17]=z[3]*z[17];
    z[20]= - n<T>(43,8) + z[4];
    z[44]=n<T>(1,2)*z[4];
    z[53]=z[44] - 1;
    z[54]=z[8]*z[53];
    z[54]=z[54] + n<T>(7,2) - z[4];
    z[54]=z[8]*z[54];
    z[20]=n<T>(1,2)*z[20] + z[54];
    z[25]=n<T>(101,128) - z[25];
    z[25]=z[6]*z[25];
    z[25]=n<T>(155,128) + z[25];
    z[25]=z[25]*z[35];
    z[20]=n<T>(1,4)*z[20] + z[25];
    z[25]=n<T>(1,4)*z[8];
    z[54]=z[25] - 1;
    z[55]=z[54]*z[26];
    z[55]=n<T>(1,4) + z[55];
    z[56]=npow(z[6],2);
    z[57]=n<T>(1,9)*z[56];
    z[28]=z[28]*z[57];
    z[28]=n<T>(1,2)*z[55] + z[28];
    z[28]=z[7]*z[28];
    z[20]=n<T>(1,3)*z[20] + z[28];
    z[20]=z[7]*z[20];
    z[28]=z[10] - 1;
    z[28]=z[28]*z[4];
    z[55]= - n<T>(1,2) + z[28];
    z[55]=z[8]*z[55];
    z[55]=z[55] + n<T>(1,2) - z[10];
    z[58]=n<T>(1,4) - z[32];
    z[58]=z[4]*z[58];
    z[55]=z[58] + n<T>(1,12)*z[55];
    z[55]=z[8]*z[55];
    z[58]=n<T>(1,2)*z[10];
    z[59]=z[58] - n<T>(1,3);
    z[60]=z[4]*z[59];
    z[60]=n<T>(1,6)*z[10] + z[60];
    z[55]=n<T>(1,2)*z[60] + z[55];
    z[60]=n<T>(25,64) + z[29];
    z[60]=z[60]*z[35];
    z[60]=n<T>(13,64) + z[60];
    z[60]=z[6]*z[60];
    z[14]=z[17] + z[14] + z[20] + n<T>(1,2)*z[55] + z[60];
    z[14]=z[9]*z[14];
    z[17]=n<T>(1,2) - z[51];
    z[17]=z[17]*z[13];
    z[17]= - n<T>(307,576) + z[17];
    z[20]= - n<T>(107,32) - z[39];
    z[20]=z[20]*z[21];
    z[20]= - n<T>(55,128) + z[20];
    z[20]=z[6]*z[20];
    z[55]=static_cast<T>(539)+ 25*z[7];
    z[36]=z[55]*z[36];
    z[55]=n<T>(695,48)*z[2] - n<T>(7,4) + 15*z[7];
    z[34]=z[55]*z[34];
    z[17]=z[34] + z[36] + n<T>(1,2)*z[17] + z[20];
    z[17]=z[2]*z[17];
    z[20]=static_cast<T>(147)+ n<T>(169,3)*z[6];
    z[20]=z[20]*z[24];
    z[34]= - n<T>(527,3) + 19*z[7];
    z[34]=z[34]*z[30];
    z[34]= - n<T>(259,9) + z[34];
    z[27]=z[34]*z[27];
    z[34]= - n<T>(1009,6) - z[37];
    z[34]=z[7]*z[34];
    z[34]= - n<T>(635,18) + z[34];
    z[36]= - n<T>(181,3) - n<T>(189,2)*z[7];
    z[36]=n<T>(1,4)*z[36] - 13*z[2];
    z[36]=z[2]*z[36];
    z[34]=n<T>(1,4)*z[34] + z[36];
    z[34]=z[34]*z[15];
    z[18]=z[34] + z[27] - z[18] + z[20];
    z[18]=z[2]*z[18];
    z[20]= - n<T>(1,3) + n<T>(1,8)*z[12];
    z[27]=z[7]*z[8];
    z[34]= - n<T>(1,6)*z[27] - n<T>(13,4)*z[20] + z[26];
    z[34]=z[7]*z[34];
    z[36]= - n<T>(131,8) - z[26];
    z[34]=n<T>(1,2)*z[36] + z[34];
    z[34]=z[7]*z[34];
    z[34]=n<T>(139,16) + z[34];
    z[34]=z[7]*z[34];
    z[36]=static_cast<T>(971)+ 169*z[6];
    z[36]=z[6]*z[36];
    z[18]=z[18] + z[34] + n<T>(1,96)*z[36] - n<T>(147,16) - z[52];
    z[34]=n<T>(1021,18) + 83*z[7];
    z[34]=z[34]*z[41];
    z[36]=n<T>(1367,6) + z[37];
    z[36]=z[7]*z[36];
    z[36]=n<T>(917,9) + z[36];
    z[37]=n<T>(33,16)*z[2] + n<T>(137,9) + n<T>(11,2)*z[7];
    z[37]=z[2]*z[37];
    z[36]=n<T>(1,8)*z[36] + z[37];
    z[36]=z[2]*z[36];
    z[34]=z[34] + z[36];
    z[34]=z[34]*z[15];
    z[36]= - n<T>(547,9) - z[33];
    z[36]=z[7]*z[36];
    z[36]= - n<T>(847,9) + z[36];
    z[36]=z[36]*z[40];
    z[34]=z[34] - z[43] + n<T>(1,64)*z[36];
    z[34]=z[2]*z[34];
    z[36]=n<T>(1,2)*z[2];
    z[37]= - z[36] - z[46];
    z[37]=z[2]*z[37];
    z[37]= - z[7] + z[37];
    z[37]=z[2]*z[37];
    z[37]=z[49] + z[37];
    z[37]=z[2]*z[37];
    z[37]=z[47] + z[37];
    z[37]=z[2]*z[37];
    z[37]=z[37] + n<T>(1,2)*z[50];
    z[43]=n<T>(35,9)*z[3];
    z[37]=z[37]*z[43];
    z[46]= - n<T>(11,3) + n<T>(7,4)*z[12];
    z[47]=z[7]*z[46];
    z[47]=n<T>(29,36) + z[47];
    z[47]=z[7]*z[47];
    z[47]= - n<T>(425,12) + z[47];
    z[47]=z[47]*z[40];
    z[34]=z[37] + z[34] - n<T>(5,3)*z[45] + n<T>(1,16)*z[47];
    z[34]=z[3]*z[34];
    z[18]=n<T>(1,2)*z[18] + z[34];
    z[18]=z[3]*z[18];
    z[34]= - n<T>(721,64) - z[39];
    z[34]=z[34]*z[21];
    z[34]= - n<T>(349,128) + z[34];
    z[34]=z[6]*z[34];
    z[37]= - static_cast<T>(11)- n<T>(5,4)*z[12];
    z[37]=n<T>(5,6)*z[27] + n<T>(1,8)*z[37] - n<T>(5,3)*z[8];
    z[37]=z[7]*z[37];
    z[39]=n<T>(1235,24) + 17*z[8];
    z[37]=n<T>(1,12)*z[39] + z[37];
    z[37]=z[37]*z[38];
    z[39]=static_cast<T>(1)+ n<T>(5,6)*z[11];
    z[17]=z[18] + z[17] + z[37] + n<T>(1,2)*z[39] + z[34];
    z[17]=z[3]*z[17];
    z[16]= - z[16]*z[13];
    z[16]=n<T>(113,64) + z[16];
    z[18]=z[22]*z[44];
    z[16]=n<T>(1,3)*z[16] + z[18];
    z[18]= - n<T>(1,2) - z[6];
    z[18]=z[18]*z[21];
    z[21]=n<T>(1,3)*z[56];
    z[22]= - n<T>(1,3) + z[6];
    z[22]=z[22]*z[21];
    z[22]= - n<T>(19,64) + z[22];
    z[22]=z[7]*z[22];
    z[19]= - n<T>(269,96) - z[19];
    z[19]=n<T>(1,4)*z[19] + z[57];
    z[19]=z[2]*z[19];
    z[16]=z[19] + z[22] + n<T>(1,2)*z[16] + z[18];
    z[16]=z[2]*z[16];
    z[18]=n<T>(1,2)*z[8];
    z[19]=z[54]*z[18];
    z[22]=z[6] - n<T>(2,3);
    z[34]=z[22]*z[56];
    z[19]=z[19] + z[34];
    z[19]=z[19]*z[31];
    z[34]=z[53]*z[26];
    z[34]=z[34] + static_cast<T>(1)- n<T>(1,6)*z[4];
    z[34]=z[8]*z[34];
    z[37]=n<T>(7,3) + n<T>(1,2)*z[12];
    z[34]=n<T>(1,8)*z[37] + z[34];
    z[37]= - n<T>(187,192) - z[6];
    z[37]=z[6]*z[37];
    z[37]=n<T>(347,384) + z[37];
    z[37]=z[37]*z[35];
    z[19]=z[19] + n<T>(1,4)*z[34] + z[37];
    z[19]=z[7]*z[19];
    z[34]= - z[59]*z[44];
    z[28]= - static_cast<T>(1)+ n<T>(1,3)*z[28];
    z[28]=z[28]*z[25];
    z[37]=static_cast<T>(1)- z[58];
    z[28]=z[28] + n<T>(1,3)*z[37] + z[34];
    z[28]=z[8]*z[28];
    z[34]=n<T>(7,2) + z[10];
    z[34]=n<T>(1,4)*z[34] + z[23];
    z[28]=n<T>(1,3)*z[34] + z[28];
    z[29]=z[29] + n<T>(347,128);
    z[34]=z[29]*z[35];
    z[34]=n<T>(1,2) + z[34];
    z[34]=z[6]*z[34];
    z[14]=z[14] + z[17] + z[16] + z[19] + n<T>(1,2)*z[28] + z[34];
    z[14]=z[9]*z[14];
    z[16]= - static_cast<T>(1)- z[18];
    z[16]=z[16]*z[50]*z[18];
    z[17]= - static_cast<T>(1)- z[8];
    z[17]=z[17]*z[30];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[49]*z[8]*z[17];
    z[18]=npow(z[8],2);
    z[19]= - z[18]*z[38];
    z[19]= - z[8] + z[19];
    z[19]=z[19]*z[49];
    z[28]=npow(z[2],2);
    z[19]=z[19] - n<T>(1,4)*z[28];
    z[19]=z[2]*z[19];
    z[17]=z[17] + z[19];
    z[17]=z[2]*z[17];
    z[16]=z[16] + z[17];
    z[16]=z[16]*z[43];
    z[17]=z[18]*z[31];
    z[19]= - z[8] - z[17];
    z[19]=z[19]*z[33];
    z[19]=n<T>(13,4) + z[19];
    z[19]=z[7]*z[19];
    z[28]=n<T>(11,2)*z[2] + n<T>(33,4)*z[7] + n<T>(307,9) + n<T>(11,4)*z[1];
    z[28]=z[28]*z[36];
    z[19]=z[19] + z[28];
    z[19]=z[19]*z[36];
    z[28]=n<T>(11,16) + z[26];
    z[28]=z[28]*z[27];
    z[31]=n<T>(547,8) + 47*z[8];
    z[31]=z[8]*z[31];
    z[28]=n<T>(1,18)*z[31] + z[28];
    z[28]=z[7]*z[28];
    z[28]=n<T>(955,144)*z[8] + z[28];
    z[28]=z[28]*z[40];
    z[19]=z[28] + z[19];
    z[19]=z[19]*z[15];
    z[28]= - n<T>(1,4)*z[46] + z[26];
    z[28]=z[28]*z[27];
    z[31]= - n<T>(29,64) + 10*z[8];
    z[31]=z[8]*z[31];
    z[28]=n<T>(1,9)*z[31] + n<T>(1,4)*z[28];
    z[28]=z[7]*z[28];
    z[28]=n<T>(425,192)*z[8] + z[28];
    z[28]=z[28]*z[40];
    z[16]=z[16] + z[19] - n<T>(5,6)*z[42] + z[28];
    z[16]=z[3]*z[16];
    z[19]=static_cast<T>(1)- n<T>(1,4)*z[11];
    z[13]=z[19]*z[13];
    z[19]=static_cast<T>(281)+ n<T>(169,4)*z[6];
    z[19]=z[19]*z[24];
    z[13]=z[13] + z[19];
    z[19]= - n<T>(19,16) - z[26];
    z[19]=z[19]*z[27];
    z[24]=n<T>(503,48) + 9*z[8];
    z[24]=z[8]*z[24];
    z[19]=z[24] + z[19];
    z[19]=z[7]*z[19];
    z[24]= - static_cast<T>(1)+ n<T>(7,12)*z[8];
    z[19]=n<T>(1,4)*z[24] + z[19];
    z[19]=z[7]*z[19];
    z[24]=11*z[1];
    z[28]= - n<T>(599,12) - z[24];
    z[18]=z[18]*z[7];
    z[31]=n<T>(23,6)*z[18] - n<T>(11,2) + n<T>(23,3)*z[8];
    z[31]=z[7]*z[31];
    z[28]= - n<T>(63,8)*z[2] + n<T>(1,4)*z[28] + z[31];
    z[28]=z[28]*z[36];
    z[19]=z[19] + z[28];
    z[15]=z[19]*z[15];
    z[19]=n<T>(13,2)*z[20] - z[8];
    z[19]=z[8]*z[19];
    z[19]=z[19] + n<T>(1,6)*z[18];
    z[19]=z[7]*z[19];
    z[20]=n<T>(19,8) + n<T>(7,9)*z[8];
    z[20]=z[8]*z[20];
    z[19]=7*z[20] + z[19];
    z[19]=z[7]*z[19];
    z[19]= - n<T>(139,8)*z[8] + z[19];
    z[19]=z[19]*z[38];
    z[13]=z[16] + z[15] + n<T>(1,3)*z[13] + z[19];
    z[13]=z[3]*z[13];
    z[15]= - n<T>(167,16) - 13*z[8];
    z[15]=z[15]*z[26];
    z[16]=static_cast<T>(11)- n<T>(9,4)*z[12];
    z[16]=n<T>(1,8)*z[16] + z[8];
    z[16]=z[8]*z[16];
    z[16]=z[16] - z[17];
    z[16]=z[16]*z[30];
    z[15]=z[15] + z[16];
    z[15]=z[15]*z[30];
    z[16]= - n<T>(175,8) - 23*z[8];
    z[16]=z[16]*z[26];
    z[16]=n<T>(41,48)*z[27] + n<T>(11,8) + z[16];
    z[16]=z[16]*z[41];
    z[17]=n<T>(37,2) + z[24];
    z[16]=n<T>(15,16)*z[2] + z[16] + n<T>(1,64)*z[17] + z[26];
    z[16]=z[2]*z[16];
    z[17]=z[23] + n<T>(473,32)*z[8];
    z[19]= - z[6]*z[29];
    z[19]= - n<T>(409,64) + z[19];
    z[19]=z[6]*z[19];
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[13]=z[13] + z[16] + n<T>(1,3)*z[17] + z[15];
    z[13]=z[3]*z[13];
    z[15]=n<T>(1,3)*z[5];
    z[16]=z[4]*z[5];
    z[17]=z[16] + 1;
    z[19]=z[15] - z[17];
    z[20]= - static_cast<T>(4)+ z[5];
    z[23]=n<T>(2,3)*z[5] - z[17];
    z[23]=z[4]*z[23];
    z[20]=n<T>(1,3)*z[20] + z[23];
    z[20]=z[4]*z[20];
    z[20]=n<T>(1,3) + z[20];
    z[20]=z[6]*z[20];
    z[23]= - n<T>(4,3)*z[5] + z[17];
    z[23]=z[4]*z[23];
    z[20]=z[20] - n<T>(155,384) + z[23];
    z[20]=z[6]*z[20];
    z[20]=n<T>(1,2)*z[19] + z[20];
    z[20]=z[20]*z[35];
    z[19]= - z[4]*z[19];
    z[19]=n<T>(2,3) + z[19];
    z[19]=z[6]*z[19];
    z[23]=n<T>(1,2) + n<T>(1,2)*z[16];
    z[19]=z[19] + z[15] - z[23];
    z[19]=z[19]*z[56];
    z[24]= - z[17]*npow(z[6],3)*z[48];
    z[19]=z[19] + z[24];
    z[19]=z[19]*z[48];
    z[19]=z[19] - n<T>(19,64) + z[20];
    z[19]=z[2]*z[19];
    z[20]= - static_cast<T>(21)+ z[12];
    z[20]=n<T>(1,16)*z[20] - z[32];
    z[24]=z[4]*z[12];
    z[20]=n<T>(9,16)*z[8] - n<T>(1,128)*z[24] + n<T>(1,8)*z[20] - z[51];
    z[20]=z[8]*z[20];
    z[24]=n<T>(283,64) - z[5];
    z[23]=z[5] - z[23];
    z[23]=z[4]*z[23];
    z[23]=n<T>(1,6)*z[24] + z[23];
    z[23]=z[4]*z[23];
    z[24]=z[5] - 1;
    z[27]= - z[5] + z[17];
    z[27]=z[4]*z[27];
    z[27]=z[27] + static_cast<T>(2)- z[5];
    z[27]=z[4]*z[27];
    z[27]=z[27] + z[24];
    z[27]=z[4]*z[27];
    z[27]= - static_cast<T>(2)+ z[27];
    z[27]=z[27]*z[35];
    z[15]=z[27] + z[23] + n<T>(7,4) - z[15];
    z[15]=z[6]*z[15];
    z[17]=z[4]*z[17];
    z[17]=z[17] + n<T>(763,192) + z[5];
    z[15]=n<T>(1,2)*z[17] + z[15];
    z[15]=z[15]*z[35];
    z[17]= - n<T>(7,3) - z[12];
    z[17]=n<T>(1,8)*z[17] - z[26];
    z[17]=z[17]*z[25];
    z[21]=z[22]*z[21];
    z[17]=n<T>(1,24)*z[18] + z[17] + z[21];
    z[17]=z[7]*z[17];
    z[16]=z[24] - z[16];
    z[18]= - z[11]*z[5];
    z[16]=z[18] + n<T>(1,3)*z[16];

    r += z[13] + z[14] + z[15] + n<T>(1,3)*z[16] + z[17] + z[19] + z[20];
 
    return r;
}

template double qg_2lha_r515(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r515(const std::array<dd_real,30>&);
#endif
