#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r691(const std::array<T,30>& k) {
  T z[37];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=k[9];
    z[8]=k[3];
    z[9]=z[7] - 1;
    z[10]=z[4] + z[6];
    z[11]= - z[10]*z[9];
    z[12]=z[6] - 1;
    z[13]=z[5]*z[6];
    z[14]=z[13] - z[12];
    z[14]=z[5]*z[14];
    z[15]=z[1] - 1;
    z[16]=z[4]*z[15];
    z[16]= - n<T>(1,3) + z[16];
    z[16]=z[1]*z[16];
    z[11]=z[14] + z[16] + n<T>(1,3) + z[11];
    z[11]=z[2]*z[11];
    z[14]=z[4] - n<T>(1,6);
    z[16]=n<T>(1,2)*z[1];
    z[17]= - z[14]*z[16];
    z[18]=n<T>(1,2)*z[4];
    z[17]=z[17] + n<T>(1,3) + z[18];
    z[17]=z[1]*z[17];
    z[19]=n<T>(1,2)*z[7];
    z[20]=z[19] - n<T>(5,3) + z[4];
    z[20]=z[20]*z[19];
    z[21]=z[9]*z[19];
    z[22]=z[16] - 1;
    z[21]=z[21] + z[22];
    z[23]=n<T>(1,3)*z[5];
    z[21]=z[21]*z[23];
    z[24]= - n<T>(1,6) - z[4];
    z[11]=n<T>(1,8)*z[11] + z[21] + z[20] + n<T>(1,2)*z[24] + z[17];
    z[11]=z[2]*z[11];
    z[17]= - static_cast<T>(1)- z[4];
    z[20]=3*z[4];
    z[21]= - static_cast<T>(1)+ z[20];
    z[21]=z[1]*z[21];
    z[17]=3*z[17] + z[21];
    z[17]=z[1]*z[17];
    z[21]=z[20] - 7;
    z[24]=3*z[7];
    z[25]= - z[24] - z[21];
    z[25]=z[7]*z[25];
    z[17]=z[25] + z[20] + z[17];
    z[25]=n<T>(1,4)*z[7];
    z[26]=5*z[7];
    z[27]=n<T>(31,3) - z[26];
    z[27]=z[27]*z[25];
    z[28]=n<T>(1,4)*z[1];
    z[29]= - static_cast<T>(1)+ z[28];
    z[30]=n<T>(1,3) - z[19];
    z[30]=z[7]*z[30];
    z[30]=n<T>(1,6) + z[30];
    z[30]=z[5]*z[30];
    z[27]=z[30] + n<T>(1,3)*z[29] + z[27];
    z[27]=z[5]*z[27];
    z[11]=z[11] + n<T>(1,4)*z[17] + z[27];
    z[11]=z[2]*z[11];
    z[17]= - n<T>(1,3) + z[7];
    z[17]=z[5]*z[17]*z[19];
    z[27]= - n<T>(1,3) + z[25];
    z[27]=z[7]*z[27];
    z[27]=n<T>(1,12) + z[27];
    z[17]=7*z[27] + z[17];
    z[17]=z[5]*z[17];
    z[27]=n<T>(7,2) - 2*z[1];
    z[29]= - n<T>(25,6) + 2*z[7];
    z[29]=z[7]*z[29];
    z[17]=z[17] + n<T>(1,3)*z[27] + z[29];
    z[17]=z[5]*z[17];
    z[27]=n<T>(1,2) - z[4];
    z[27]=z[27]*z[16];
    z[27]=z[27] + n<T>(2,3) + z[18];
    z[27]=z[1]*z[27];
    z[29]=z[4] - 3;
    z[30]=n<T>(3,2)*z[7] + z[29];
    z[30]=z[30]*z[19];
    z[11]=z[11] + z[17] + z[30] - n<T>(1,2)*z[14] + z[27];
    z[11]=z[2]*z[11];
    z[14]=z[7] - 4;
    z[17]=n<T>(1,3)*z[7];
    z[14]=z[14]*z[17];
    z[27]=n<T>(1,3)*z[1];
    z[30]=z[1] - 2;
    z[31]=z[27]*z[30];
    z[14]=z[14] + z[31] + 1;
    z[31]=z[22]*z[27];
    z[31]=z[31] + n<T>(1,2);
    z[32]=z[25] - 1;
    z[32]=z[32]*z[17];
    z[31]=z[32] + n<T>(1,2)*z[31];
    z[32]=z[2]*z[31];
    z[33]=z[17] - 1;
    z[33]=z[33]*z[7];
    z[33]=z[33] - n<T>(1,3)*z[30];
    z[34]= - z[5]*z[33];
    z[32]=z[32] + z[34] - z[14];
    z[32]=z[2]*z[32];
    z[34]=z[7] - 3;
    z[34]=z[34]*z[7];
    z[30]=z[34] - z[30];
    z[34]=z[19] - 1;
    z[34]=z[34]*z[7];
    z[34]=z[34] + n<T>(1,2);
    z[35]=z[5]*z[34];
    z[35]=z[35] + z[30];
    z[35]=z[5]*z[35];
    z[22]=z[1]*z[22];
    z[36]= - static_cast<T>(2)+ z[19];
    z[36]=z[7]*z[36];
    z[22]=z[32] + z[35] + z[36] + n<T>(3,2) + z[22];
    z[22]=z[2]*z[22];
    z[32]= - z[5]*z[9]*z[17];
    z[35]=static_cast<T>(2)- z[7];
    z[35]=z[7]*z[35];
    z[32]=z[32] - static_cast<T>(1)+ z[35];
    z[32]=z[5]*z[32];
    z[30]=z[32] - z[30];
    z[30]=z[5]*z[30];
    z[14]=z[22] + z[30] - z[14];
    z[14]=z[2]*z[14];
    z[9]=z[9]*z[7];
    z[22]=npow(z[7],2);
    z[30]=z[22]*z[5];
    z[32]=z[9] + n<T>(1,4)*z[30];
    z[32]=z[32]*z[23];
    z[32]=z[32] + z[34];
    z[32]=z[5]*z[32];
    z[32]=z[32] + z[33];
    z[32]=z[5]*z[32];
    z[14]=z[14] + z[32] + z[31];
    z[14]=z[3]*z[14];
    z[31]=z[4] + n<T>(5,3);
    z[32]= - n<T>(1,3) + z[18];
    z[32]=z[1]*z[32];
    z[32]= - n<T>(1,2)*z[31] + z[32];
    z[32]=z[1]*z[32];
    z[33]=z[4] - n<T>(1,3);
    z[33]=n<T>(1,2)*z[33];
    z[34]=n<T>(11,3) - z[4];
    z[34]=n<T>(1,2)*z[34] - z[7];
    z[34]=z[7]*z[34];
    z[32]=z[34] + z[33] + z[32];
    z[34]=n<T>(7,3) - z[24];
    z[34]=z[34]*z[19];
    z[35]= - z[22]*z[23];
    z[34]=z[34] + z[35];
    z[34]=z[5]*z[34];
    z[35]=n<T>(3,2) - z[7];
    z[35]=z[35]*z[26];
    z[35]= - static_cast<T>(3)+ z[35];
    z[34]=n<T>(1,2)*z[35] + z[34];
    z[34]=z[5]*z[34];
    z[27]= - n<T>(1,2) + z[27];
    z[35]=n<T>(15,2) - n<T>(11,3)*z[7];
    z[35]=z[7]*z[35];
    z[27]=5*z[27] + z[35];
    z[27]=n<T>(1,2)*z[27] + z[34];
    z[27]=z[5]*z[27];
    z[27]=n<T>(1,2)*z[32] + z[27];
    z[11]=z[14] + n<T>(1,2)*z[27] + z[11];
    z[11]=z[3]*z[11];
    z[14]= - z[10]*z[25];
    z[27]= - z[1]*z[4];
    z[27]=z[33] + z[27];
    z[32]=n<T>(1,4)*z[2];
    z[27]=z[27]*z[32];
    z[12]= - z[4] + z[12];
    z[33]=n<T>(3,4)*z[4];
    z[34]=n<T>(1,3) + z[33];
    z[34]=z[1]*z[34];
    z[35]=n<T>(1,2)*z[5];
    z[36]=z[35] - n<T>(1,4);
    z[36]=z[6]*z[36];
    z[36]=n<T>(1,3) + z[36];
    z[36]=z[5]*z[36];
    z[12]=z[27] + z[36] + z[14] + n<T>(1,4)*z[12] + z[34];
    z[12]=z[2]*z[12];
    z[14]=z[22] + 1;
    z[14]=z[14]*z[5];
    z[27]= - n<T>(1,2) + z[1];
    z[9]=n<T>(1,4)*z[14] + n<T>(1,2)*z[27] + z[9];
    z[9]=z[9]*z[23];
    z[27]=n<T>(13,6)*z[7];
    z[20]=z[27] - static_cast<T>(5)+ z[20];
    z[20]=z[20]*z[25];
    z[33]=n<T>(1,8)*z[1] - n<T>(2,3) - z[33];
    z[33]=z[1]*z[33];
    z[9]=z[12] + z[9] + z[20] + n<T>(23,24) + z[33];
    z[9]=z[2]*z[9];
    z[12]=n<T>(7,2) - z[26];
    z[12]=z[7]*z[12];
    z[12]= - z[30] + n<T>(1,2) + z[12];
    z[12]=z[12]*z[23];
    z[20]= - n<T>(7,2) + z[1];
    z[33]= - static_cast<T>(5)+ n<T>(7,2)*z[7];
    z[34]= - z[7]*z[33];
    z[12]=z[12] + n<T>(1,3)*z[20] + z[34];
    z[12]=z[12]*z[35];
    z[20]=n<T>(1,4)*z[4];
    z[28]= - z[28] + n<T>(1,3) + z[20];
    z[28]=z[1]*z[28];
    z[21]= - n<T>(13,3)*z[7] - z[21];
    z[21]=z[21]*z[25];
    z[9]=z[9] + z[12] + z[21] + z[28] - static_cast<T>(1)+ z[20];
    z[9]=z[2]*z[9];
    z[12]= - static_cast<T>(7)+ n<T>(31,4)*z[7];
    z[12]=z[12]*z[17];
    z[20]= - n<T>(1,2) + z[7];
    z[20]=z[7]*z[20];
    z[20]=z[20] + n<T>(1,6)*z[30];
    z[20]=z[5]*z[20];
    z[12]=z[20] + n<T>(3,4) + z[12];
    z[12]=z[12]*z[35];
    z[20]=17*z[7];
    z[21]= - static_cast<T>(23)+ z[20];
    z[21]=z[21]*z[25];
    z[15]=z[21] - z[15];
    z[12]=n<T>(1,3)*z[15] + z[12];
    z[12]=z[5]*z[12];
    z[15]=z[27] + z[29];
    z[15]=z[15]*z[25];
    z[21]=npow(z[1],2);
    z[21]=z[21] - z[4];
    z[9]=z[11] + z[9] + z[12] + z[15] + n<T>(1,3) + n<T>(1,8)*z[21];
    z[9]=z[3]*z[9];
    z[11]= - n<T>(5,3) + z[24];
    z[11]=z[11]*z[19];
    z[11]=n<T>(1,6)*z[14] + z[11] - n<T>(1,3) + z[16];
    z[11]=z[11]*z[35];
    z[12]=static_cast<T>(1)- z[10];
    z[12]=z[7]*z[12];
    z[10]=z[12] - z[1] + n<T>(13,3) + z[10];
    z[12]=z[2]*z[18];
    z[12]=z[12] - z[31];
    z[12]=z[2]*z[12];
    z[14]=n<T>(7,3) + 3*z[13];
    z[14]=z[5]*z[14];
    z[10]=z[12] + n<T>(1,2)*z[10] + z[14];
    z[10]=z[10]*z[32];
    z[12]=n<T>(2,3)*z[7];
    z[14]= - n<T>(31,6) + z[4];
    z[14]=n<T>(1,4)*z[14] + z[12];
    z[14]=z[7]*z[14];
    z[15]=n<T>(7,3) + z[1];
    z[10]=z[10] + z[11] + n<T>(1,8)*z[15] + z[14];
    z[10]=z[2]*z[10];
    z[11]=n<T>(35,2) - z[20];
    z[11]=z[11]*z[17];
    z[11]= - n<T>(1,2) + z[11];
    z[14]= - z[22]*z[35];
    z[15]=n<T>(7,4) - z[26];
    z[15]=z[7]*z[15];
    z[14]=z[15] + z[14];
    z[14]=z[14]*z[23];
    z[11]=n<T>(1,2)*z[11] + z[14];
    z[11]=z[11]*z[35];
    z[14]=n<T>(25,3) - z[4];
    z[12]=n<T>(1,8)*z[14] - z[12];
    z[12]=z[7]*z[12];
    z[9]=z[9] + z[10] + z[11] - n<T>(5,12) + z[12];
    z[9]=z[3]*z[9];
    z[10]= - static_cast<T>(5)+ 7*z[7];
    z[10]=z[10]*z[17];
    z[11]=z[6]*z[8];
    z[11]=z[11] + 1;
    z[12]=n<T>(7,3)*z[22] + z[11];
    z[12]=z[12]*z[35];
    z[10]=z[12] + n<T>(1,2)*z[11] + z[10];
    z[10]=z[5]*z[10];
    z[11]=z[33]*z[17];
    z[10]=z[11] + z[10];
    z[11]=n<T>(1,2)*z[6] + z[13];
    z[11]=z[5]*z[11];
    z[11]=z[25] + z[11];
    z[11]=z[2]*z[11];
    z[10]=n<T>(1,2)*z[10] + z[11];

    r += z[9] + n<T>(1,2)*z[10];
 
    return r;
}

template double qg_2lha_r691(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r691(const std::array<dd_real,30>&);
#endif
