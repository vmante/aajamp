#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1832(const std::array<T,30>& k) {
  T z[30];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[14];
    z[5]=k[9];
    z[6]=k[3];
    z[7]=k[12];
    z[8]=k[7];
    z[9]=k[27];
    z[10]=k[11];
    z[11]=n<T>(1,3)*z[4];
    z[12]=z[11] + n<T>(1,12)*z[10];
    z[13]=z[6] - n<T>(5,12);
    z[13]=z[13]*z[6];
    z[14]= - z[8]*npow(z[2],2);
    z[14]=13*z[6] + z[14];
    z[14]=z[2]*z[14];
    z[14]=n<T>(1,12)*z[14] + z[13] + z[12];
    z[14]=z[7]*z[14];
    z[15]=n<T>(1,2)*z[4];
    z[16]= - static_cast<T>(1)+ z[11];
    z[16]=z[16]*z[15];
    z[17]=n<T>(1,4)*z[2];
    z[18]=npow(z[8],2);
    z[19]= - z[18]*z[17];
    z[19]=z[9] + z[19];
    z[19]=z[2]*z[19];
    z[19]= - static_cast<T>(5)+ z[19];
    z[20]=n<T>(1,6)*z[2];
    z[19]=z[19]*z[20];
    z[21]=n<T>(4,3)*z[6];
    z[22]=npow(z[10],2);
    z[23]=static_cast<T>(1)- n<T>(1,12)*z[22];
    z[14]=z[14] + z[19] + z[16] + n<T>(1,2)*z[23] - z[21];
    z[14]=z[7]*z[14];
    z[16]=n<T>(1,2)*z[8];
    z[19]=z[18]*z[2];
    z[23]=n<T>(1,2)*z[19] + z[9] + z[16];
    z[23]=z[2]*z[23];
    z[24]=z[4] - 1;
    z[25]=z[24]*z[4];
    z[26]=z[10] - 1;
    z[26]=z[26]*z[10];
    z[27]=static_cast<T>(1)+ z[26];
    z[23]=z[23] + n<T>(1,2)*z[27] - z[25];
    z[14]=n<T>(1,6)*z[23] + z[14];
    z[14]=z[7]*z[14];
    z[23]=static_cast<T>(1)+ z[4];
    z[23]=z[23]*z[11];
    z[27]=static_cast<T>(1)- z[8];
    z[27]=z[8]*z[27];
    z[27]=z[27] - z[19];
    z[27]=z[27]*z[17];
    z[27]=z[27] + static_cast<T>(5)- n<T>(1,4)*z[8];
    z[28]=n<T>(1,3)*z[2];
    z[27]=z[27]*z[28];
    z[29]= - static_cast<T>(1)- n<T>(1,4)*z[26];
    z[23]=z[27] + z[23] + n<T>(1,3)*z[29] + n<T>(3,2)*z[6];
    z[23]=z[2]*z[23];
    z[27]=z[24]*z[11];
    z[29]= - static_cast<T>(1)- z[10];
    z[29]=z[10]*z[29];
    z[29]=n<T>(1,6)*z[29] + z[6];
    z[23]=z[23] + n<T>(1,2)*z[29] + z[27];
    z[27]= - z[17] - n<T>(1,4);
    z[27]=z[8]*z[27];
    z[27]= - static_cast<T>(2)+ z[27];
    z[27]=z[27]*z[28];
    z[29]= - static_cast<T>(1)- n<T>(13,3)*z[6];
    z[27]=n<T>(1,4)*z[29] + z[27];
    z[27]=z[2]*z[27];
    z[29]= - n<T>(2,3) - n<T>(1,2)*z[6];
    z[29]=z[6]*z[29];
    z[12]=z[27] + z[29] + z[12];
    z[12]=z[2]*z[12];
    z[27]= - static_cast<T>(5)+ z[10];
    z[29]=n<T>(5,6) - z[6];
    z[29]=z[6]*z[29];
    z[27]=n<T>(1,6)*z[27] + z[29];
    z[12]=z[12] + n<T>(1,2)*z[27] + z[11];
    z[12]=z[7]*z[12];
    z[12]=n<T>(1,2)*z[23] + z[12];
    z[12]=z[7]*z[12];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[8]*z[16];
    z[16]=z[16] + z[19];
    z[16]=z[16]*z[17];
    z[19]= - static_cast<T>(1)+ n<T>(1,2)*z[10];
    z[19]=z[10]*z[19];
    z[19]=z[19] + z[8];
    z[16]=z[16] + n<T>(1,4)*z[19] + z[25];
    z[16]=z[16]*z[28];
    z[19]= - static_cast<T>(1)+ z[15];
    z[19]=z[19]*z[11];
    z[23]=static_cast<T>(1)+ n<T>(1,3)*z[26];
    z[12]=z[12] + z[16] + n<T>(1,4)*z[23] + z[19];
    z[12]=z[7]*z[12];
    z[16]=n<T>(1,2)*z[1];
    z[19]=z[16] - 1;
    z[19]=z[19]*z[4];
    z[23]= - static_cast<T>(3)+ z[1];
    z[23]=n<T>(1,2)*z[23] - z[19];
    z[23]=z[4]*z[23];
    z[23]=n<T>(1,2) + z[23];
    z[25]=npow(z[4],2);
    z[23]=z[23]*z[25];
    z[17]=z[24]*npow(z[4],3)*z[17];
    z[17]=z[17] - n<T>(1,24)*z[18] + z[23];
    z[17]=z[2]*z[17];
    z[23]=static_cast<T>(3)- z[16];
    z[23]=z[1]*z[23];
    z[23]= - static_cast<T>(5)+ z[23];
    z[27]= - static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[29]=z[1]*z[27];
    z[29]=n<T>(5,4) + z[29];
    z[29]=z[4]*z[29];
    z[23]=n<T>(1,2)*z[23] + z[29];
    z[23]=z[4]*z[23];
    z[16]=z[23] + n<T>(5,3) - z[16];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(5,12) + z[16];
    z[16]=z[4]*z[16];
    z[16]=z[17] - n<T>(1,24)*z[8] + z[16];
    z[16]=z[2]*z[16];
    z[17]=z[1] - n<T>(5,2);
    z[19]= - z[19] + z[17];
    z[19]=z[19]*z[15];
    z[19]=z[19] - z[27];
    z[19]=z[4]*z[19];
    z[19]= - n<T>(1,4) + z[19];
    z[19]=z[4]*z[19];
    z[12]=z[12] + z[16] - n<T>(1,24)*z[26] + z[19];
    z[12]=z[3]*z[12];
    z[16]= - static_cast<T>(1)- z[1];
    z[16]=n<T>(1,3)*z[16] + z[15];
    z[16]=z[4]*z[16];
    z[16]=n<T>(1,3)*z[17] + z[16];
    z[16]=z[16]*z[15];
    z[16]=n<T>(1,3) + z[16];
    z[16]=z[4]*z[16];
    z[17]=z[24]*z[25];
    z[18]= - n<T>(1,4)*z[18] + z[17];
    z[18]=z[18]*z[20];
    z[12]=z[12] + z[14] + z[18] - n<T>(1,24)*z[22] + z[16];
    z[12]=z[3]*z[12];
    z[14]=z[5]*z[6];
    z[16]=static_cast<T>(2)+ n<T>(5,4)*z[14];
    z[16]=z[16]*z[28];
    z[14]=z[16] + n<T>(11,12)*z[14] - n<T>(1,2) + z[21];
    z[14]=z[5]*z[14];
    z[16]=n<T>(1,2)*z[5];
    z[18]=npow(z[6],2);
    z[19]= - z[18]*z[16];
    z[13]= - z[13] + z[19];
    z[13]=z[5]*z[13];
    z[19]=z[2]*z[5];
    z[18]= - z[5]*z[18];
    z[18]= - n<T>(13,6)*z[6] + z[18];
    z[18]=z[18]*z[19];
    z[13]=z[13] + n<T>(1,2)*z[18];
    z[13]=z[7]*z[13];
    z[13]=z[13] + z[14];
    z[13]=z[7]*z[13];
    z[14]= - static_cast<T>(1)- 5*z[5];
    z[14]=z[14]*z[16];
    z[16]= - z[9]*z[19];
    z[14]=z[14] + z[16];
    z[13]=n<T>(1,6)*z[14] + z[13];
    z[13]=z[7]*z[13];
    z[14]= - static_cast<T>(1)+ z[1];
    z[11]=z[14]*z[11];
    z[11]=z[11] + static_cast<T>(1)- n<T>(1,3)*z[1];
    z[11]=z[15]*z[11];
    z[11]= - n<T>(1,3) + z[11];
    z[11]=z[4]*z[5]*z[11];
    z[14]= - z[20]*z[5]*z[17];

    r += z[11] + z[12] + z[13] + z[14];
 
    return r;
}

template double qg_2lha_r1832(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1832(const std::array<dd_real,30>&);
#endif
