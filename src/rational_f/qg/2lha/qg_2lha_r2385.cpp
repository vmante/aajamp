#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2385(const std::array<T,30>& k) {
  T z[34];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[27];
    z[7]=k[9];
    z[8]=k[3];
    z[9]=npow(z[6],2);
    z[10]=npow(z[5],2);
    z[11]= - z[9] + 3*z[10];
    z[12]=n<T>(1,2)*z[8];
    z[11]=z[11]*z[12];
    z[13]=z[6] - 1;
    z[13]=z[13]*z[6];
    z[14]=z[11] + z[13];
    z[15]=5*z[10];
    z[16]=z[15] - z[14];
    z[16]=z[8]*z[16];
    z[17]=n<T>(1,2)*z[5];
    z[18]=z[17] + 1;
    z[19]= - z[5]*z[18];
    z[20]= - n<T>(3,2) + z[2];
    z[20]=z[3]*z[20];
    z[20]=n<T>(5,2) + z[20];
    z[20]=z[3]*z[20];
    z[16]=z[20] + z[16] + z[9] + z[19];
    z[19]= - z[9] + n<T>(3,2)*z[10];
    z[19]=z[19]*z[8];
    z[13]=z[13] - z[10];
    z[20]=z[9] - z[10];
    z[21]=n<T>(1,4)*z[4];
    z[22]=z[20]*z[21];
    z[13]=z[22] + n<T>(1,2)*z[13] + z[19];
    z[13]=z[4]*z[13];
    z[13]=n<T>(1,2)*z[16] + z[13];
    z[13]=z[4]*z[13];
    z[16]=z[10]*z[8];
    z[22]=n<T>(9,4) + z[5];
    z[22]=z[5]*z[22];
    z[22]=z[22] - z[16];
    z[22]=z[8]*z[22];
    z[23]=z[2] - 3;
    z[24]=n<T>(1,2)*z[2];
    z[25]= - z[23]*z[24];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[3]*z[25];
    z[25]=z[25] + static_cast<T>(2)- n<T>(5,4)*z[2];
    z[26]=3*z[3];
    z[25]=z[25]*z[26];
    z[13]=z[13] + z[25] + z[22] - static_cast<T>(4)- z[17];
    z[13]=z[4]*z[13];
    z[22]=z[16] - z[6];
    z[15]= - z[15] + z[22];
    z[15]=z[8]*z[15];
    z[11]=z[6] - z[11];
    z[11]=z[4]*z[11];
    z[25]=n<T>(5,2)*z[5];
    z[11]=z[11] + z[15] + z[6] + z[25];
    z[11]=z[8]*z[11];
    z[15]=static_cast<T>(3)- z[3];
    z[15]=z[3]*z[15];
    z[11]=z[15] - static_cast<T>(3)+ z[11];
    z[15]=n<T>(1,2)*z[4];
    z[11]=z[11]*z[15];
    z[27]=n<T>(3,2)*z[16];
    z[28]=3*z[5];
    z[29]= - n<T>(5,2) - z[28];
    z[29]=z[5]*z[29];
    z[29]=z[29] + z[27];
    z[29]=z[8]*z[29];
    z[30]=static_cast<T>(5)+ n<T>(13,2)*z[5];
    z[29]=n<T>(1,2)*z[30] + z[29];
    z[29]=z[8]*z[29];
    z[30]=7*z[2];
    z[31]=n<T>(29,2) - z[30];
    z[32]=2*z[2];
    z[33]= - n<T>(9,4) + z[32];
    z[33]=z[3]*z[33];
    z[31]=n<T>(1,2)*z[31] + z[33];
    z[31]=z[3]*z[31];
    z[11]=z[11] + z[31] + z[29] - n<T>(33,4) + z[2];
    z[11]=z[4]*z[11];
    z[29]= - static_cast<T>(5)- n<T>(3,2)*z[5];
    z[29]=z[5]*z[29];
    z[27]=z[29] + z[27];
    z[27]=z[8]*z[27];
    z[27]=z[27] + static_cast<T>(7)+ n<T>(11,4)*z[5];
    z[27]=z[8]*z[27];
    z[29]=static_cast<T>(11)- 5*z[2];
    z[29]=z[29]*z[24];
    z[29]= - static_cast<T>(3)+ z[29];
    z[29]=z[3]*z[29];
    z[31]= - n<T>(45,4) + z[32];
    z[31]=z[2]*z[31];
    z[29]=z[29] + static_cast<T>(10)+ z[31];
    z[29]=z[3]*z[29];
    z[11]=z[11] + z[29] + z[27] - static_cast<T>(13)+ n<T>(11,2)*z[2];
    z[11]=z[4]*z[11];
    z[21]= - z[22]*npow(z[8],2)*z[21];
    z[22]=z[5] - z[16];
    z[22]=z[8]*z[22];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[8]*z[22];
    z[27]=n<T>(1,4)*z[3];
    z[29]= - static_cast<T>(1)+ z[27];
    z[29]=z[3]*z[29];
    z[21]=z[21] + z[29] + n<T>(3,2) + z[22];
    z[21]=z[4]*z[21];
    z[22]=z[12]*z[10];
    z[29]=z[5] - z[22];
    z[29]=z[8]*z[29];
    z[29]= - static_cast<T>(1)+ z[29];
    z[29]=z[8]*z[29];
    z[29]= - n<T>(1,2)*z[23] + z[29];
    z[31]=z[2] - 1;
    z[33]=z[3]*z[31];
    z[32]= - n<T>(3,4)*z[33] - static_cast<T>(3)+ z[32];
    z[32]=z[3]*z[32];
    z[21]=z[21] + 3*z[29] + z[32];
    z[21]=z[4]*z[21];
    z[28]=z[28] - z[16];
    z[28]=z[8]*z[28];
    z[28]= - static_cast<T>(4)+ z[28];
    z[28]=z[8]*z[28];
    z[29]=static_cast<T>(4)- z[2];
    z[29]=z[2]*z[29];
    z[32]= - static_cast<T>(1)+ z[24];
    z[32]=z[2]*z[32];
    z[32]=n<T>(1,2) + z[32];
    z[32]=z[3]*z[32];
    z[29]=n<T>(3,2)*z[32] - static_cast<T>(3)+ z[29];
    z[29]=z[3]*z[29];
    z[32]=3*z[2];
    z[21]=z[21] + z[29] + z[28] + n<T>(19,4) - z[32];
    z[21]=z[4]*z[21];
    z[23]= - z[2]*z[23];
    z[23]= - static_cast<T>(3)+ z[23];
    z[23]=z[2]*z[23];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[27];
    z[27]=static_cast<T>(2)- z[2];
    z[27]=z[2]*z[27];
    z[23]=z[23] - static_cast<T>(1)+ z[27];
    z[23]=z[3]*z[23];
    z[27]=n<T>(1,4)*z[16];
    z[28]=z[5] - z[27];
    z[28]=z[8]*z[28];
    z[28]= - n<T>(7,4) + z[28];
    z[28]=z[8]*z[28];
    z[21]=z[21] + z[23] - n<T>(7,4)*z[31] + z[28];
    z[21]=z[7]*z[21];
    z[23]=z[17] + 5;
    z[23]=z[23]*z[5];
    z[28]= - z[23] + z[16];
    z[28]=z[28]*z[12];
    z[28]=z[28] + static_cast<T>(5)+ n<T>(3,4)*z[5];
    z[28]=z[8]*z[28];
    z[29]= - n<T>(31,2) + z[30];
    z[29]=z[2]*z[29];
    z[29]=n<T>(17,2) + z[29];
    z[31]= - n<T>(13,4) + z[2];
    z[31]=z[2]*z[31];
    z[31]=n<T>(7,2) + z[31];
    z[31]=z[2]*z[31];
    z[31]= - n<T>(5,4) + z[31];
    z[31]=z[3]*z[31];
    z[29]=n<T>(1,2)*z[29] + z[31];
    z[29]=z[3]*z[29];
    z[31]= - static_cast<T>(13)+ 11*z[2];
    z[11]=z[21] + z[11] + z[29] + n<T>(1,2)*z[31] + z[28];
    z[11]=z[7]*z[11];
    z[14]= - 2*z[10] + z[14];
    z[14]=z[8]*z[14];
    z[21]=n<T>(1,2)*z[3];
    z[28]= - static_cast<T>(1)+ z[21];
    z[28]=z[28]*z[21];
    z[29]=n<T>(1,2)*z[6];
    z[19]=z[29] - z[19];
    z[19]=z[19]*z[15];
    z[31]=z[6] + z[17];
    z[14]=z[19] + z[28] + n<T>(1,2)*z[31] + z[14];
    z[14]=z[4]*z[14];
    z[19]= - z[22] + z[29] + 7*z[10];
    z[19]=z[19]*z[12];
    z[28]= - static_cast<T>(3)- n<T>(7,4)*z[5];
    z[28]=z[5]*z[28];
    z[19]=z[19] - z[29] + z[28];
    z[19]=z[8]*z[19];
    z[28]=static_cast<T>(9)- z[30];
    z[28]=z[28]*z[21];
    z[29]=z[32] - 11;
    z[28]=z[28] + z[29];
    z[28]=z[28]*z[21];
    z[30]=n<T>(15,2) + z[5];
    z[14]=z[14] + z[28] + n<T>(1,2)*z[30] + z[19];
    z[14]=z[4]*z[14];
    z[19]=static_cast<T>(2)+ z[25];
    z[19]=z[5]*z[19];
    z[19]=z[19] - z[22];
    z[19]=z[8]*z[19];
    z[19]=z[19] - static_cast<T>(3)- z[23];
    z[19]=z[8]*z[19];
    z[22]= - n<T>(5,2) + z[2];
    z[22]=z[2]*z[22];
    z[22]=n<T>(3,2) + z[22];
    z[22]=z[22]*z[26];
    z[23]=n<T>(43,4) - z[2];
    z[23]=z[2]*z[23];
    z[22]=z[22] - static_cast<T>(12)+ z[23];
    z[22]=z[3]*z[22];
    z[23]=n<T>(5,2)*z[2];
    z[14]=z[14] + z[22] + z[19] + n<T>(1,4)*z[5] + static_cast<T>(12)- z[23];
    z[14]=z[4]*z[14];
    z[19]=static_cast<T>(23)- 9*z[2];
    z[19]=z[19]*z[24];
    z[22]= - z[2]*z[29];
    z[22]= - static_cast<T>(13)+ z[22];
    z[22]=z[2]*z[22];
    z[22]=static_cast<T>(5)+ z[22];
    z[21]=z[22]*z[21];
    z[19]=z[21] - static_cast<T>(7)+ z[19];
    z[19]=z[3]*z[19];
    z[17]=static_cast<T>(2)+ z[17];
    z[17]=z[5]*z[17];
    z[17]=z[17] - z[27];
    z[17]=z[8]*z[17];
    z[17]=z[17] - static_cast<T>(5)- 2*z[5];
    z[17]=z[8]*z[17];
    z[21]=static_cast<T>(37)- 25*z[2];
    z[11]=z[11] + z[14] + z[19] + n<T>(1,4)*z[21] + z[17];
    z[11]=z[7]*z[11];
    z[12]= - z[18]*z[12];
    z[12]=z[12] + n<T>(7,4);
    z[12]=z[5]*z[12];
    z[12]=static_cast<T>(2)+ z[12];
    z[12]=z[8]*z[12];
    z[14]= - static_cast<T>(8)+ z[23];
    z[14]=z[2]*z[14];
    z[17]= - n<T>(9,2) + z[2];
    z[17]=z[2]*z[17];
    z[17]=static_cast<T>(6)+ z[17];
    z[17]=z[2]*z[17];
    z[17]= - n<T>(5,2) + z[17];
    z[17]=z[3]*z[17];
    z[14]=z[17] + n<T>(11,2) + z[14];
    z[14]=z[3]*z[14];
    z[11]=z[11] + z[13] + z[14] + z[12] - n<T>(25,4) + z[32];
    z[11]=z[7]*z[11];
    z[9]=z[9] - n<T>(1,2)*z[10];
    z[10]=z[1] - 1;
    z[9]=z[10]*z[9];
    z[10]= - z[4]*z[20];
    z[9]=z[10] - z[16] + z[9];
    z[9]=z[9]*z[15];
    z[10]=static_cast<T>(1)+ z[5];
    z[10]=n<T>(1,2)*z[10] - z[16];
    z[12]= - static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[13]=z[2]*z[12];
    z[13]=n<T>(3,4) + z[13];
    z[13]=z[3]*z[13];
    z[12]=z[13] + z[12];
    z[12]=z[3]*z[12];
    z[9]=z[9] + n<T>(1,2)*z[10] + z[12];
    z[9]=z[4]*z[9];
    z[10]= - z[2] + static_cast<T>(7)- z[1];
    z[10]=z[10]*z[24];
    z[12]= - static_cast<T>(3)+ n<T>(1,2)*z[1];
    z[10]=z[10] + z[12];
    z[12]=z[12] + z[24];
    z[13]= - z[2]*z[12];
    z[13]=z[13] - n<T>(9,2) + z[1];
    z[13]=z[13]*z[24];
    z[13]=z[13] + static_cast<T>(1)- n<T>(1,4)*z[1];
    z[13]=z[3]*z[13];
    z[10]=n<T>(1,2)*z[10] + z[13];
    z[10]=z[3]*z[10];
    z[13]= - z[8]*z[5];
    z[12]=z[13] - z[12];

    r += z[9] + z[10] + z[11] + n<T>(1,2)*z[12];
 
    return r;
}

template double qg_2lha_r2385(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2385(const std::array<dd_real,30>&);
#endif
