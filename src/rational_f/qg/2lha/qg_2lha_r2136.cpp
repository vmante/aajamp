#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2136(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[9];
    z[4]=k[12];
    z[5]=k[24];
    z[6]=k[6];
    z[7]=k[27];
    z[8]=k[14];
    z[9]=z[3] - z[5];
    z[10]=z[2] + 1;
    z[10]=z[1]*z[10];
    z[9]=z[4]*z[10]*z[9];
    z[11]=z[2] - 1;
    z[12]= - z[5]*z[11];
    z[10]=z[3]*z[10];
    z[10]= - 3*z[10] + static_cast<T>(3)- 7*z[2];
    z[10]=z[3]*z[10];
    z[9]=z[9] + z[12] + z[10];
    z[10]= - static_cast<T>(2)+ z[2];
    z[10]=z[7]*z[10];
    z[12]=z[7] + z[8];
    z[12]=z[12]*z[6];
    z[13]=3*z[7] - z[12];
    z[13]=z[6]*z[13];
    z[9]=z[13] + z[10] + n<T>(1,4)*z[9];
    z[9]=z[4]*z[9];
    z[10]=z[7] + 2*z[8];
    z[12]=z[12] - z[10];
    z[12]=z[6]*z[12];
    z[13]=z[7]*z[2];
    z[12]=n<T>(3,4)*z[3] + z[13] + z[12];
    z[12]=z[3]*z[12];
    z[10]=z[6]*z[10];
    z[10]= - 2*z[7] + z[10];
    z[10]=z[6]*z[10];
    z[9]=z[9] + z[10] + z[12];
    z[9]=z[4]*z[9];
    z[10]= - z[8]*npow(z[6],2);
    z[11]=z[7]*z[11];
    z[11]= - z[8] + z[11];
    z[11]=z[6]*z[11];
    z[11]=z[8] + z[11];
    z[11]=z[3]*z[6]*z[11];

    r += z[9] + z[10] + z[11];
 
    return r;
}

template double qg_2lha_r2136(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2136(const std::array<dd_real,30>&);
#endif
