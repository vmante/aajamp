#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r458(const std::array<T,30>& k) {
  T z[42];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[9];
    z[6]=k[12];
    z[7]=k[10];
    z[8]=k[27];
    z[9]=n<T>(1,2)*z[4];
    z[10]= - static_cast<T>(1)- 3*z[4];
    z[10]=z[10]*z[9];
    z[11]=npow(z[4],2);
    z[12]=z[11]*z[6];
    z[13]=static_cast<T>(1)+ z[4];
    z[13]=z[13]*z[12];
    z[10]=z[10] + z[13];
    z[10]=z[6]*z[10];
    z[13]=z[9] - 1;
    z[10]= - n<T>(1,4)*z[13] + z[10];
    z[10]=z[6]*z[10];
    z[14]=npow(z[6],2);
    z[15]=n<T>(1,2)*z[2];
    z[16]=z[14]*z[15];
    z[17]= - z[9] + 3*z[12];
    z[17]=z[17]*z[16];
    z[10]=z[10] + z[17];
    z[10]=z[2]*z[10];
    z[17]=n<T>(1,4)*z[2];
    z[13]=z[13]*z[17];
    z[18]=z[7] + n<T>(17,8);
    z[18]=z[18]*z[7];
    z[18]=z[18] + n<T>(5,8);
    z[18]=z[18]*z[4];
    z[19]=z[7] + 1;
    z[13]=z[18] - z[13] - n<T>(1,4)*z[19];
    z[18]=z[7] + 3;
    z[20]=z[4]*z[7];
    z[21]=z[18]*z[20];
    z[22]=z[7] + n<T>(7,2);
    z[22]=z[22]*z[7];
    z[21]= - z[22] - n<T>(5,2) + n<T>(1,4)*z[21];
    z[21]=z[21]*z[4];
    z[22]=z[9] + 3;
    z[22]=z[22]*z[4];
    z[23]=z[2]*z[4];
    z[22]=z[22] - z[23];
    z[22]=z[22]*z[15];
    z[21]=z[21] + z[22];
    z[22]=z[15] - 1;
    z[22]=z[15]*z[22];
    z[24]=n<T>(1,4)*z[7];
    z[25]=z[24] + 1;
    z[25]=z[25]*z[7];
    z[22]=z[22] + z[25] + n<T>(3,4);
    z[22]=z[11]*z[22];
    z[26]=z[22]*z[8];
    z[27]= - n<T>(1,2)*z[21] + z[26];
    z[27]=z[8]*z[27];
    z[27]=z[27] - z[13];
    z[27]=z[8]*z[27];
    z[28]=z[6]*npow(z[4],3);
    z[29]= - n<T>(1,4)*z[11] + z[28];
    z[16]=z[29]*z[16];
    z[29]= - n<T>(3,2)*z[11] + z[28];
    z[29]=z[6]*z[29];
    z[29]=z[9] + z[29];
    z[29]=z[6]*z[29];
    z[16]=z[29] + z[16];
    z[16]=z[2]*z[16];
    z[29]=n<T>(1,2)*z[7];
    z[30]=z[18]*z[29];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[4]*z[30];
    z[31]=n<T>(1,2)*z[8];
    z[32]=z[22]*z[31];
    z[33]= - z[2]*z[9];
    z[30]=z[32] + z[30] + z[33];
    z[30]=z[8]*z[30];
    z[28]= - n<T>(11,4)*z[11] + z[28];
    z[28]=z[6]*z[28];
    z[28]=n<T>(5,2)*z[4] + z[28];
    z[28]=z[6]*z[28];
    z[32]=z[29] + 1;
    z[33]=z[32]*z[7];
    z[28]=n<T>(3,2)*z[33] + z[28];
    z[16]=z[30] + n<T>(1,2)*z[28] + z[16];
    z[16]=z[5]*z[16];
    z[28]=n<T>(5,4) + z[7];
    z[28]=z[28]*z[9];
    z[30]= - n<T>(1,2) + z[4];
    z[30]=z[30]*z[12];
    z[34]=static_cast<T>(1)- n<T>(3,2)*z[4];
    z[34]=z[4]*z[34];
    z[30]=z[34] + z[30];
    z[30]=z[6]*z[30];
    z[28]=z[30] + z[28] - z[32];
    z[28]=z[6]*z[28];
    z[30]=n<T>(3,4)*z[7];
    z[32]=z[30] + 1;
    z[34]=n<T>(3,2)*z[7];
    z[35]= - z[32]*z[34];
    z[10]=z[16] + z[27] + z[10] + z[35] + z[28];
    z[10]=z[5]*z[10];
    z[16]=z[7] - 1;
    z[27]= - z[16]*z[29];
    z[28]=z[29] - 1;
    z[35]=z[28]*z[20];
    z[27]=z[27] + z[35];
    z[27]=z[4]*z[27];
    z[35]=npow(z[7],2);
    z[36]=n<T>(1,2)*z[35];
    z[27]=z[36] + z[27];
    z[27]=z[4]*z[27];
    z[37]=z[19]*z[29];
    z[27]= - z[37] + z[27];
    z[27]=z[6]*z[27];
    z[38]=z[29]*z[4];
    z[39]=static_cast<T>(3)- z[24];
    z[39]=z[39]*z[38];
    z[25]= - z[25] + z[39];
    z[25]=z[4]*z[25];
    z[25]=z[27] + n<T>(5,4)*z[33] + z[25];
    z[25]=z[6]*z[25];
    z[27]= - static_cast<T>(5)+ z[7];
    z[27]=z[27]*z[38];
    z[27]= - z[35] + z[27];
    z[25]=n<T>(1,4)*z[27] + z[25];
    z[25]=z[6]*z[25];
    z[27]=n<T>(1,2) + z[7];
    z[27]=z[27]*z[30];
    z[10]=z[10] + z[27] + z[25];
    z[25]=z[7] + n<T>(1,4);
    z[27]= - z[25]*z[29];
    z[29]= - z[7]*z[32];
    z[30]=z[19]*z[7];
    z[39]=z[6]*z[30];
    z[29]=z[29] + z[39];
    z[29]=z[6]*z[29];
    z[39]=z[35]*z[6];
    z[40]=z[36] + z[39];
    z[40]=z[6]*z[40];
    z[41]=n<T>(3,4)*z[35];
    z[40]= - z[41] + z[40];
    z[15]=z[40]*z[15];
    z[15]=z[15] + z[27] + z[29];
    z[15]=z[6]*z[15];
    z[27]= - static_cast<T>(1)+ n<T>(3,2)*z[35];
    z[15]=n<T>(1,4)*z[27] + z[15];
    z[15]=z[2]*z[15];
    z[27]=z[35]*z[9];
    z[29]=z[33] + n<T>(1,2);
    z[29]=z[29]*z[6];
    z[33]=static_cast<T>(3)- z[6];
    z[33]=z[33]*z[29];
    z[40]= - n<T>(3,2) - z[7];
    z[40]=z[7]*z[40];
    z[27]=z[33] + z[40] + z[27];
    z[33]= - z[6]*z[37];
    z[37]=z[35] - z[39];
    z[37]=z[37]*z[17];
    z[33]=z[37] + z[30] + z[33];
    z[33]=z[6]*z[33];
    z[37]= - z[35] - z[4];
    z[33]=n<T>(1,4)*z[37] + z[33];
    z[33]=z[2]*z[33];
    z[27]=n<T>(1,2)*z[27] + z[33];
    z[33]=n<T>(1,2)*z[3];
    z[27]=z[27]*z[33];
    z[32]= - z[32]*z[20];
    z[37]= - n<T>(1,2) - z[30];
    z[32]=n<T>(1,2)*z[37] + z[32];
    z[37]= - n<T>(9,4) - z[7];
    z[37]=z[7]*z[37];
    z[29]=z[29] - n<T>(5,4) + z[37];
    z[29]=z[6]*z[29];
    z[37]=5*z[7];
    z[39]=static_cast<T>(13)+ z[37];
    z[39]=z[7]*z[39];
    z[29]=z[29] + static_cast<T>(1)+ n<T>(1,8)*z[39];
    z[29]=z[6]*z[29];
    z[15]=z[27] + z[15] + n<T>(1,2)*z[32] + z[29];
    z[15]=z[15]*z[33];
    z[27]=z[35]*z[4];
    z[29]=n<T>(1,8) - z[7];
    z[29]=z[7]*z[29];
    z[29]=z[29] - n<T>(11,16)*z[27];
    z[29]=z[4]*z[29];
    z[32]= - static_cast<T>(3)+ z[37];
    z[32]=z[7]*z[32];
    z[32]=z[32] + z[27];
    z[12]=z[32]*z[12];
    z[12]=z[29] + n<T>(1,4)*z[12];
    z[12]=z[6]*z[12];
    z[29]=static_cast<T>(5)+ z[7];
    z[29]=z[7]*z[29];
    z[29]=z[29] + n<T>(9,2)*z[27];
    z[12]=n<T>(1,8)*z[29] + z[12];
    z[12]=z[6]*z[12];
    z[29]= - n<T>(5,2) - z[7];
    z[29]=z[7]*z[29];
    z[29]=z[29] - n<T>(11,2)*z[27];
    z[32]=z[35] + n<T>(3,4)*z[27];
    z[32]=z[6]*z[4]*z[32];
    z[29]=n<T>(1,4)*z[29] + z[32];
    z[29]=z[6]*z[29];
    z[29]=n<T>(9,16)*z[35] + z[29];
    z[29]=z[6]*z[29];
    z[27]=z[30] + 3*z[27];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(11,4)*z[35] + z[27];
    z[14]=z[27]*z[14];
    z[27]=z[2]*z[35]*npow(z[6],3);
    z[14]=z[14] + z[27];
    z[14]=z[14]*z[17];
    z[14]=z[29] + z[14];
    z[14]=z[2]*z[14];
    z[12]=z[14] - n<T>(1,8)*z[35] + z[12];
    z[12]=z[2]*z[12];
    z[14]=z[16]*z[20];
    z[16]=z[30] - 3*z[14];
    z[16]=z[4]*z[16];
    z[16]= - z[36] + z[16];
    z[17]=z[7]*z[28];
    z[14]=z[17] + z[14];
    z[11]=z[14]*z[11];
    z[11]= - z[36] + z[11];
    z[11]=z[6]*z[11];
    z[11]=n<T>(1,2)*z[16] + z[11];
    z[11]=z[6]*z[11];
    z[14]=z[25]*z[20];
    z[14]=z[41] + z[14];
    z[11]=n<T>(1,2)*z[14] + z[11];
    z[11]=z[6]*z[11];
    z[14]= - static_cast<T>(1)- z[34];
    z[14]=z[14]*z[24];
    z[11]=z[14] + z[11];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[2]*z[11];
    z[12]= - z[18]*z[38];
    z[12]=z[12] + z[19];
    z[12]=z[4]*z[12];
    z[9]= - static_cast<T>(1)- z[9];
    z[9]=z[4]*z[9];
    z[9]=z[9] + z[23];
    z[9]=z[2]*z[9];
    z[9]=z[12] + z[9];
    z[9]=z[3]*z[9];
    z[9]=n<T>(1,4)*z[9] + z[13];
    z[9]=z[3]*z[9];
    z[12]=z[22]*z[3];
    z[12]=z[12] + z[21];
    z[12]=z[12]*z[33];
    z[13]= - z[3]*z[26];
    z[12]=z[12] + z[13];
    z[12]=z[8]*z[12];
    z[9]=z[9] + z[12];
    z[9]=z[9]*z[31];
    z[12]= - static_cast<T>(1)+ z[2];
    z[12]=z[1]*z[12]*npow(z[3],2);
    z[9]=n<T>(1,16)*z[12] + z[9] + z[15] + z[11] + n<T>(1,2)*z[10];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r458(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r458(const std::array<dd_real,30>&);
#endif
