#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2242(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[9];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=2*z[5];
    z[8]= - z[6] + z[3];
    z[7]=z[8]*z[7];
    z[8]=2*z[3];
    z[9]=z[8] - static_cast<T>(1)+ 2*z[6];
    z[7]=z[7] + z[9];
    z[7]=z[5]*z[7];
    z[10]=z[5]*z[6];
    z[9]=z[10] - z[9];
    z[9]=z[5]*z[9];
    z[10]=z[3]*z[4];
    z[11]=static_cast<T>(1)+ z[10];
    z[11]=z[3]*z[11];
    z[9]=z[9] + z[11] + z[6] - static_cast<T>(1)- z[1];
    z[9]=z[2]*z[9];
    z[7]=2*z[9] + z[7] + static_cast<T>(1)- 3*z[10];
    z[7]=z[2]*z[7];
    z[8]= - z[5]*z[8];
    z[8]= - static_cast<T>(1)+ z[8];
    z[8]=z[5]*z[8];
    z[7]=z[7] + z[4] + z[8];

    r += z[7]*z[2];
 
    return r;
}

template double qg_2lha_r2242(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2242(const std::array<dd_real,30>&);
#endif
