#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r208(const std::array<T,30>& k) {
  T z[43];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[29];
    z[6]=k[6];
    z[7]=k[7];
    z[8]=k[2];
    z[9]=k[11];
    z[10]=k[4];
    z[11]=k[9];
    z[12]=k[10];
    z[13]=n<T>(3,2)*z[3];
    z[14]=n<T>(1,2)*z[10];
    z[15]=n<T>(1,4)*z[4];
    z[16]= - z[10]*z[15];
    z[16]=z[16] - z[13] - static_cast<T>(3)+ z[14];
    z[16]=z[4]*z[16];
    z[17]=z[3]*z[8];
    z[18]=n<T>(1,2)*z[8];
    z[19]=static_cast<T>(1)- z[18];
    z[19]=z[19]*z[17];
    z[20]=n<T>(1,2)*z[2];
    z[21]= - z[20] - 9;
    z[21]=z[11]*z[21];
    z[22]=z[10]*z[11];
    z[19]=3*z[4] + z[19] - z[22] + n<T>(1,2) + z[21];
    z[19]=z[19]*z[20];
    z[21]=n<T>(1,2)*z[11];
    z[23]=z[21] - 1;
    z[24]=n<T>(1,4)*z[22] + z[23];
    z[24]=z[10]*z[24];
    z[25]=n<T>(1,4)*z[8];
    z[26]= - static_cast<T>(7)+ 25*z[11];
    z[27]=n<T>(3,4)*z[8];
    z[28]=static_cast<T>(1)- z[27];
    z[28]=z[8]*z[28];
    z[28]= - n<T>(3,2) + z[28];
    z[28]=z[3]*z[28];
    z[16]=z[19] + z[16] + z[28] + z[25] + n<T>(1,4)*z[26] + z[24];
    z[16]=z[16]*z[20];
    z[19]=z[7] - 1;
    z[24]=z[19]*z[14];
    z[26]=z[8]*z[7];
    z[28]=z[10]*z[7];
    z[28]= - z[26] + static_cast<T>(3)+ z[28];
    z[28]=z[28]*z[18];
    z[28]=z[28] - static_cast<T>(1)+ z[24];
    z[28]=z[8]*z[28];
    z[24]=z[24] - 5;
    z[28]=z[28] + z[24];
    z[29]=static_cast<T>(7)- z[18];
    z[29]=z[29]*z[25];
    z[29]= - static_cast<T>(1)+ z[29];
    z[29]=z[8]*z[29];
    z[29]= - n<T>(1,4) + z[29];
    z[29]=z[3]*z[29];
    z[30]=z[14]*z[4];
    z[24]=z[30] + z[3] - z[24];
    z[15]=z[24]*z[15];
    z[15]=z[16] + z[15] + n<T>(1,4)*z[28] + z[29];
    z[15]=z[5]*z[15];
    z[16]=z[11] - 1;
    z[24]=z[16]*z[11];
    z[28]=n<T>(3,2) + z[3];
    z[28]=z[3]*z[28];
    z[28]=z[24] + z[28];
    z[28]=z[4]*z[28];
    z[29]=npow(z[11],2);
    z[31]=npow(z[3],2);
    z[28]=z[28] - z[29] + z[31];
    z[28]=z[4]*z[28];
    z[32]=z[29] + z[31];
    z[33]=npow(z[4],2);
    z[34]= - z[32]*z[33]*z[20];
    z[28]=z[28] + z[34];
    z[28]=z[2]*z[28];
    z[34]=n<T>(1,2)*z[3];
    z[35]= - static_cast<T>(3)- z[3];
    z[35]=z[35]*z[34];
    z[23]= - z[11]*z[23];
    z[23]=z[23] + z[35];
    z[23]=z[4]*z[23];
    z[35]=n<T>(1,2)*z[19] - z[3];
    z[35]=z[3]*z[35];
    z[23]=z[23] + z[24] + z[35];
    z[23]=z[4]*z[23];
    z[24]=z[18]*npow(z[7],2);
    z[35]= - 3*z[7] + z[24];
    z[35]=z[35]*z[18];
    z[35]=static_cast<T>(1)+ z[35];
    z[35]=z[3]*z[35];
    z[36]=n<T>(1,2)*z[7];
    z[35]= - z[36] + z[35];
    z[35]=z[3]*z[35];
    z[23]=z[28] + z[23] - n<T>(1,2)*z[29] + z[35];
    z[15]=n<T>(1,4)*z[23] + z[15];
    z[23]=static_cast<T>(7)- z[10];
    z[23]=z[23]*z[18];
    z[28]= - n<T>(19,2) + z[8];
    z[28]=z[28]*z[18];
    z[28]=static_cast<T>(5)+ z[28];
    z[17]=z[28]*z[17];
    z[17]=z[30] + z[17] + z[23] + static_cast<T>(1)+ z[14];
    z[23]=n<T>(1,4)*z[2];
    z[28]= - z[23] - 1;
    z[28]=z[22]*z[28];
    z[35]=3*z[11];
    z[37]=npow(z[8],2);
    z[38]=z[37]*z[3];
    z[39]=n<T>(1,4)*z[38];
    z[28]=z[39] - z[18] + z[35] + z[28];
    z[28]=z[28]*z[23];
    z[40]=static_cast<T>(7)+ z[8];
    z[40]=z[40]*z[25];
    z[40]= - static_cast<T>(3)+ z[40];
    z[40]=z[3]*z[40];
    z[40]=z[40] - static_cast<T>(1)- z[25];
    z[40]=z[25]*z[40];
    z[41]=n<T>(1,8)*z[10];
    z[42]= - static_cast<T>(1)+ n<T>(11,2)*z[11];
    z[42]=z[42]*z[41];
    z[28]=z[28] - z[11] + z[42] + z[40];
    z[28]=z[2]*z[28];
    z[17]=n<T>(1,4)*z[17] + z[28];
    z[17]=z[2]*z[17];
    z[28]= - z[4] + 1;
    z[40]=z[10] + 1;
    z[28]=z[40]*z[28];
    z[27]=z[27] + z[40];
    z[27]=z[8]*z[27];
    z[40]=static_cast<T>(3)- 7*z[8];
    z[39]=z[40]*z[39];
    z[27]=z[39] + z[27] + z[28];
    z[17]=n<T>(1,4)*z[27] + z[17];
    z[17]=z[5]*z[17];
    z[16]= - z[16]*z[21];
    z[27]=z[3] - z[11];
    z[28]=z[27]*z[4];
    z[39]= - static_cast<T>(1)- z[3];
    z[39]=z[3]*z[39];
    z[16]= - z[28] + z[16] + z[39];
    z[16]=z[4]*z[16];
    z[29]=z[29] + n<T>(1,2)*z[31];
    z[32]=z[28] + z[32];
    z[32]=z[4]*z[32];
    z[39]=z[3] - 1;
    z[39]=z[39]*z[3];
    z[40]= - z[11] - z[39];
    z[40]=z[12]*z[40];
    z[32]=n<T>(1,2)*z[40] + z[32];
    z[20]=z[32]*z[20];
    z[16]=z[20] + n<T>(1,2)*z[29] + z[16];
    z[16]=z[2]*z[16];
    z[20]=z[28] + z[35] + z[39];
    z[20]=z[4]*z[20];
    z[29]= - static_cast<T>(1)+ z[26];
    z[29]=z[29]*z[18];
    z[29]= - static_cast<T>(1)+ z[29];
    z[29]=z[3]*z[29];
    z[29]= - n<T>(3,2) + z[29];
    z[29]=z[3]*z[29];
    z[20]=z[20] + n<T>(3,2)*z[11] + z[29];
    z[16]=n<T>(1,2)*z[20] + z[16];
    z[20]= - static_cast<T>(5)+ n<T>(3,2)*z[8];
    z[20]=z[20]*z[25];
    z[29]=static_cast<T>(1)- n<T>(3,8)*z[8];
    z[29]=z[29]*z[38];
    z[32]=n<T>(3,2)*z[2] - n<T>(5,2);
    z[32]=z[22]*z[32];
    z[32]= - n<T>(3,2)*z[38] + 3*z[8] + z[32];
    z[32]=z[32]*z[23];
    z[20]=z[32] + z[20] + z[29];
    z[20]=z[2]*z[20];
    z[29]= - static_cast<T>(1)+ z[8];
    z[29]=z[29]*z[38];
    z[29]= - z[37] + z[29];
    z[20]=n<T>(5,8)*z[29] + z[20];
    z[20]=z[5]*z[20];
    z[29]=z[2]*z[4];
    z[27]=z[27]*z[29];
    z[27]=5*z[28] - 3*z[27];
    z[20]=n<T>(1,8)*z[27] + z[20];
    z[20]=z[9]*z[2]*z[20];
    z[16]=z[20] + n<T>(1,4)*z[16] + z[17];
    z[16]=z[9]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[9]*z[15];
    z[16]=z[36] - 1;
    z[17]=z[16]*z[30];
    z[20]= - n<T>(13,2) + z[7];
    z[27]=static_cast<T>(1)- n<T>(3,8)*z[7];
    z[27]=z[10]*z[27];
    z[13]=z[17] - z[13] + n<T>(1,2)*z[20] + z[27];
    z[17]=n<T>(1,2)*z[4];
    z[13]=z[13]*z[17];
    z[20]=static_cast<T>(1)+ z[8];
    z[20]=z[3]*z[20];
    z[20]=z[22] + z[20];
    z[22]=z[17] + n<T>(7,2) + 3*z[3];
    z[22]=z[22]*z[17];
    z[27]= - static_cast<T>(1)- z[4];
    z[23]=z[4]*z[27]*z[23];
    z[20]=z[23] + z[22] - n<T>(1,4) + z[11] + n<T>(1,8)*z[20];
    z[20]=z[2]*z[20];
    z[22]= - static_cast<T>(5)+ z[7];
    z[22]=n<T>(1,2)*z[22] - z[11];
    z[22]=z[22]*z[41];
    z[23]=n<T>(1,4)*z[10];
    z[27]= - z[23] - 1;
    z[27]=z[7]*z[27];
    z[26]=n<T>(3,4)*z[26] - n<T>(1,4) + z[27];
    z[26]=z[26]*z[25];
    z[27]=static_cast<T>(3)- z[36];
    z[28]= - n<T>(5,2) + z[8];
    z[28]=z[8]*z[28];
    z[28]=n<T>(9,2) + z[28];
    z[28]=z[3]*z[28];
    z[13]=z[20] + z[13] + n<T>(1,4)*z[28] + z[26] + z[22] + n<T>(1,2)*z[27] - 
    z[11];
    z[13]=z[5]*z[13];
    z[20]= - z[36] + z[3];
    z[20]=z[20]*z[34];
    z[22]=z[4]*z[3];
    z[26]=z[3] - z[16];
    z[26]=z[26]*z[22];
    z[20]=z[20] + z[26];
    z[20]=z[4]*z[20];
    z[24]=z[7] - z[24];
    z[26]= - z[2]*z[33];
    z[24]=z[26] + n<T>(3,2)*z[24];
    z[24]=z[31]*z[24];
    z[20]=z[20] + z[24];
    z[13]=n<T>(1,4)*z[20] + z[13];
    z[13]=n<T>(1,2)*z[13] + z[15];
    z[13]=z[9]*z[13];
    z[15]= - z[19]*z[23];
    z[19]=n<T>(1,2)*z[1];
    z[20]=static_cast<T>(7)+ z[19];
    z[23]= - static_cast<T>(1)+ z[1];
    z[23]=z[7]*z[23];
    z[15]=z[15] + n<T>(1,2)*z[20] + z[23];
    z[20]= - static_cast<T>(1)- z[19];
    z[20]=z[20]*z[19];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[3]*z[20];
    z[16]= - z[10]*z[16];
    z[16]= - z[1] + z[16];
    z[16]=z[16]*z[17];
    z[15]=z[16] + n<T>(1,2)*z[15] + z[20];
    z[15]=z[4]*z[15];
    z[16]= - z[25] + static_cast<T>(1)- n<T>(1,4)*z[1];
    z[16]=z[7]*z[16];
    z[18]= - static_cast<T>(1)- z[18];
    z[18]=z[3]*z[18];
    z[16]=z[18] - z[21] - n<T>(3,4) + z[16];
    z[18]= - static_cast<T>(1)- z[1];
    z[18]=z[3]*z[18]*z[19];
    z[14]=z[18] - z[14] - n<T>(1,2) + z[1];
    z[14]=z[14]*z[17];
    z[14]=z[14] - n<T>(5,8) - z[3];
    z[14]=z[14]*z[29];
    z[14]=z[14] + n<T>(1,2)*z[16] + z[15];
    z[14]=z[5]*z[14];
    z[15]=z[6] + z[7];
    z[16]= - z[3]*z[6];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[3]*z[15];
    z[16]=z[19]*z[6];
    z[16]=z[16] - 1;
    z[17]=z[3]*z[16];
    z[17]=z[7] + z[17];
    z[17]=z[17]*z[22];
    z[15]=z[15] + z[17];
    z[15]=z[4]*z[15];
    z[17]=static_cast<T>(1)+ z[7];
    z[17]=z[36]*z[6]*z[17];
    z[16]= - z[7]*z[16];
    z[16]= - z[6] + z[16];
    z[16]=z[3]*z[7]*z[16];
    z[16]=z[17] + z[16];
    z[16]=z[3]*z[16];
    z[15]=z[16] + z[15];
    z[14]=n<T>(1,4)*z[15] + z[14];

    r += z[13] + n<T>(1,4)*z[14];
 
    return r;
}

template double qg_2lha_r208(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r208(const std::array<dd_real,30>&);
#endif
