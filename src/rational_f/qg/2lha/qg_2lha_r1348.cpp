#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1348(const std::array<T,30>& k) {
  T z[27];
  T r = 0;

    z[1]=k[3];
    z[2]=k[9];
    z[3]=k[11];
    z[4]=k[13];
    z[5]=k[5];
    z[6]=k[8];
    z[7]=k[14];
    z[8]=k[4];
    z[9]=k[6];
    z[10]=k[12];
    z[11]=k[7];
    z[12]=n<T>(1,2)*z[1];
    z[13]= - static_cast<T>(3)+ z[6];
    z[13]=z[13]*z[12];
    z[14]=n<T>(1,2)*z[6];
    z[15]=static_cast<T>(5)- z[9];
    z[16]=z[8]*z[9];
    z[17]= - n<T>(1,2)*z[16] + n<T>(1,2) - z[9];
    z[17]=z[8]*z[17];
    z[13]=z[13] - z[14] + n<T>(1,2)*z[15] + z[17];
    z[15]=z[1] - 1;
    z[17]=n<T>(1,4)*z[6];
    z[18]= - z[17] - z[15];
    z[18]=z[1]*z[18];
    z[18]=z[17] + z[18];
    z[18]=z[2]*z[18];
    z[13]=n<T>(1,2)*z[13] + z[18];
    z[13]=z[2]*z[13];
    z[18]=npow(z[1],2);
    z[19]=n<T>(5,6)*z[15];
    z[20]=z[2]*z[18]*z[19];
    z[21]=n<T>(5,3)*z[1] - n<T>(5,3) - z[14];
    z[21]=z[1]*z[21];
    z[20]=z[20] + z[14] + z[21];
    z[20]=z[2]*z[20];
    z[21]=z[14] - n<T>(5,3);
    z[21]=z[21]*z[15];
    z[22]=n<T>(1,3)*z[1];
    z[23]=n<T>(7,2) - 5*z[1];
    z[23]=z[23]*z[22];
    z[23]=n<T>(1,2) + z[23];
    z[23]=z[2]*z[23];
    z[24]=z[4]*z[19];
    z[21]=z[24] + z[23] + z[21];
    z[21]=z[4]*z[21];
    z[19]=z[21] + z[19] + z[20];
    z[19]=z[3]*z[19];
    z[13]=z[19] + n<T>(1,4)*z[16] + z[13];
    z[19]=n<T>(7,3) - z[14];
    z[19]=z[19]*z[12];
    z[20]=n<T>(1,4)*z[9];
    z[21]=n<T>(1,4)*z[7];
    z[19]=z[19] + z[21] + z[17] + z[20] - n<T>(5,3) + n<T>(1,4)*z[10];
    z[23]=z[8] + 1;
    z[23]=z[23]*z[7];
    z[24]=z[10] + z[5];
    z[24]=z[8]*z[24];
    z[24]=z[23] + z[24] + z[6] + z[10];
    z[14]= - n<T>(7,3) - z[14];
    z[14]=z[1]*z[14];
    z[25]= - n<T>(7,2) + z[1];
    z[25]=z[1]*z[25];
    z[25]=n<T>(5,2) + z[25];
    z[25]=z[2]*z[25];
    z[14]=n<T>(1,12)*z[25] + n<T>(1,4)*z[14] + n<T>(1,3) + n<T>(1,8)*z[24];
    z[14]=z[4]*z[14];
    z[24]=z[1] - n<T>(5,4);
    z[24]=z[24]*z[1];
    z[25]= - n<T>(1,8) + z[24];
    z[25]=z[25]*z[22];
    z[25]=n<T>(1,8) + z[25];
    z[25]=z[2]*z[25];
    z[26]=n<T>(7,2) + 11*z[1];
    z[26]=z[1]*z[26];
    z[26]= - n<T>(29,2) + z[26];
    z[25]=n<T>(1,12)*z[26] + z[25];
    z[25]=z[2]*z[25];
    z[14]=z[14] + n<T>(1,2)*z[19] + z[25];
    z[14]=z[4]*z[14];
    z[13]=z[14] + n<T>(1,2)*z[13];
    z[13]=z[3]*z[13];
    z[14]=static_cast<T>(1)- z[12];
    z[14]=z[14]*z[22];
    z[19]=n<T>(1,2) - z[22];
    z[18]=z[19]*z[18];
    z[18]= - n<T>(1,6) + z[18];
    z[18]=z[2]*z[18];
    z[19]= - n<T>(1,3) - z[7];
    z[14]=n<T>(1,4)*z[18] + n<T>(1,8)*z[19] + z[14];
    z[14]=z[2]*z[14];
    z[18]=z[5] - z[10];
    z[19]= - z[10] - 3*z[5];
    z[19]=z[8]*z[19];
    z[19]= - z[6] + z[19] - n<T>(1,3) + z[18];
    z[19]=n<T>(1,2)*z[19] - z[23];
    z[17]=n<T>(1,3) + z[17];
    z[17]=z[1]*z[17];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[14]=n<T>(1,2)*z[17] + z[14];
    z[14]=z[4]*z[14];
    z[17]=n<T>(1,3)*z[2];
    z[19]= - n<T>(1,4) - z[24];
    z[19]=z[19]*z[17];
    z[19]=z[19] - n<T>(7,8)*z[1] + static_cast<T>(1)+ z[21];
    z[19]=z[2]*z[19];
    z[18]= - 5*z[9] + static_cast<T>(5)+ z[18];
    z[14]=z[14] + n<T>(1,8)*z[18] + z[19];
    z[14]=z[4]*z[14];
    z[15]=z[15]*z[17];
    z[16]=z[16] + z[9];
    z[16]= - static_cast<T>(3)+ n<T>(5,2)*z[16];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[2]*z[15];
    z[15]= - z[20] + z[15];
    z[13]=z[13] + n<T>(1,2)*z[15] + z[14];
    z[13]=z[3]*z[13];
    z[14]=z[7] - n<T>(7,4);
    z[14]=z[14]*z[7];
    z[15]= - z[12] - n<T>(5,4) - z[14];
    z[16]= - static_cast<T>(3)+ n<T>(7,6)*z[1];
    z[12]=z[16]*z[12];
    z[16]=n<T>(1,3)*z[7];
    z[12]=z[12] + n<T>(5,4) - z[16];
    z[12]=z[2]*z[12];
    z[12]=n<T>(1,3)*z[15] + z[12];
    z[12]=z[2]*z[12];
    z[15]=static_cast<T>(1)- z[7];
    z[15]=z[16]*z[11]*z[15];
    z[18]=n<T>(1,2)*z[5];
    z[19]= - z[21] - z[18] - z[6];
    z[19]=z[1]*z[19];
    z[12]=z[12] + z[19] + z[15] - n<T>(1,4)*z[5] + z[6];
    z[12]=z[4]*z[12];
    z[15]=n<T>(7,4)*z[1] - n<T>(11,4) + z[7];
    z[15]=z[2]*z[15];
    z[14]=z[15] - n<T>(1,2) + z[14];
    z[14]=z[14]*z[17];
    z[15]=z[16] - n<T>(1,3);
    z[15]=z[11]*z[15];
    z[15]= - n<T>(1,4) + z[15];
    z[15]=z[7]*z[15];
    z[12]=z[12] + z[14] - z[18] + z[15];
    z[12]=z[4]*z[12];
    z[14]=z[2]*z[9];
    z[12]=n<T>(7,4)*z[14] + z[12];

    r += n<T>(1,2)*z[12] + z[13];
 
    return r;
}

template double qg_2lha_r1348(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1348(const std::array<dd_real,30>&);
#endif
