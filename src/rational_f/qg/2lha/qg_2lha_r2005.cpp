#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2005(const std::array<T,30>& k) {
  T z[71];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[28];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=k[12];
    z[9]=k[11];
    z[10]=k[9];
    z[11]=k[18];
    z[12]=k[6];
    z[13]=k[21];
    z[14]=z[4] + 1;
    z[15]=n<T>(1,2)*z[4];
    z[16]= - z[14]*z[15];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(5,2) + z[16];
    z[16]=z[4]*z[16];
    z[17]=n<T>(1,2)*z[2];
    z[18]=z[4] - 1;
    z[19]= - z[4]*z[18];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[19]*z[17];
    z[20]=npow(z[4],2);
    z[21]=n<T>(1,2) + z[20];
    z[21]=z[4]*z[21];
    z[19]=z[19] + n<T>(3,2) + z[21];
    z[19]=z[2]*z[19];
    z[16]=z[19] - n<T>(3,2) + z[16];
    z[16]=z[2]*z[16];
    z[19]=static_cast<T>(3)+ z[4];
    z[19]=z[4]*z[19];
    z[19]=static_cast<T>(3)+ z[19];
    z[19]=z[4]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[16]=n<T>(1,2)*z[19] + z[16];
    z[19]=n<T>(7,2) + z[4];
    z[19]=z[19]*z[15];
    z[19]=static_cast<T>(2)+ z[19];
    z[19]=z[4]*z[19];
    z[21]=n<T>(1,4)*z[4];
    z[22]=z[21] + 1;
    z[22]=z[22]*z[4];
    z[23]=n<T>(1,4)*z[2];
    z[24]=z[23] - n<T>(3,4) - z[22];
    z[24]=z[2]*z[24];
    z[19]=z[24] + n<T>(3,4) + z[19];
    z[19]=z[2]*z[19];
    z[22]= - n<T>(3,2) - z[22];
    z[22]=z[4]*z[22];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[4]*z[22];
    z[19]=z[19] - n<T>(1,4) + z[22];
    z[19]=z[10]*z[19];
    z[14]=z[14]*z[4];
    z[22]=z[4] - n<T>(1,2);
    z[24]= - z[2]*z[22];
    z[24]=z[24] - n<T>(3,2) + z[14];
    z[24]=z[2]*z[24];
    z[25]=z[15] - 1;
    z[26]= - z[4]*z[25];
    z[24]=z[24] + n<T>(3,2) + z[26];
    z[24]=z[2]*z[24];
    z[26]=n<T>(1,2)*z[1];
    z[27]=static_cast<T>(3)- z[2];
    z[27]=z[2]*z[27];
    z[27]= - static_cast<T>(3)+ z[27];
    z[27]=z[2]*z[27];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[26];
    z[28]=z[15] + 1;
    z[29]= - z[4]*z[28];
    z[24]=z[27] + z[24] - n<T>(1,2) + z[29];
    z[24]=z[24]*z[26];
    z[16]=z[24] + n<T>(1,2)*z[16] + z[19];
    z[16]=z[5]*z[16];
    z[19]=z[3] - 1;
    z[24]= - z[19]*z[17];
    z[27]=n<T>(3,2)*z[3];
    z[29]=n<T>(5,4)*z[4];
    z[24]=z[24] - z[29] - static_cast<T>(2)+ z[27];
    z[24]=z[2]*z[24];
    z[30]=3*z[3];
    z[31]=z[29] + static_cast<T>(5)- z[30];
    z[24]=n<T>(1,2)*z[31] + z[24];
    z[24]=z[2]*z[24];
    z[31]=n<T>(3,4)*z[4];
    z[18]= - z[18]*z[31];
    z[32]=z[2]*z[3];
    z[33]= - n<T>(1,4) - z[3];
    z[33]=3*z[33] + z[32];
    z[33]=z[2]*z[33];
    z[34]= - z[21] + n<T>(1,2) + z[3];
    z[33]=3*z[34] + z[33];
    z[33]=z[2]*z[33];
    z[34]=z[3] + n<T>(3,4);
    z[18]=z[33] + z[18] - z[34];
    z[18]=z[18]*z[26];
    z[33]=n<T>(5,8)*z[4];
    z[35]=n<T>(1,2)*z[3];
    z[36]=z[35] - 1;
    z[18]=z[18] + z[24] + z[33] + z[36];
    z[18]=z[1]*z[18];
    z[14]= - static_cast<T>(1)+ n<T>(11,2)*z[14];
    z[14]=z[14]*z[15];
    z[14]=z[14] - n<T>(7,2) + z[30];
    z[24]= - static_cast<T>(3)- n<T>(19,2)*z[4];
    z[24]=z[24]*z[15];
    z[24]=z[24] + n<T>(5,2) - z[30];
    z[37]=z[3] - n<T>(1,2);
    z[38]=n<T>(1,2)*z[37] + z[4];
    z[38]=z[2]*z[38];
    z[24]=n<T>(1,2)*z[24] + z[38];
    z[24]=z[2]*z[24];
    z[14]=n<T>(1,2)*z[14] + z[24];
    z[14]=z[2]*z[14];
    z[24]= - z[32] + z[30] + z[25];
    z[24]=z[24]*z[17];
    z[25]=5*z[4];
    z[38]= - static_cast<T>(7)- z[25];
    z[38]=z[38]*z[21];
    z[27]=z[27] - 1;
    z[24]=z[24] + z[38] - z[27];
    z[24]=z[2]*z[24];
    z[38]=n<T>(1,2)*z[19];
    z[39]=n<T>(5,2) + z[4];
    z[39]=z[4]*z[39];
    z[39]=n<T>(3,2) + z[39];
    z[39]=z[4]*z[39];
    z[24]=z[24] + z[38] + z[39];
    z[24]=z[10]*z[24];
    z[39]=z[3] - n<T>(3,2);
    z[40]= - n<T>(1,2)*z[20] - z[39];
    z[14]=3*z[16] + z[18] + z[24] + n<T>(1,2)*z[40] + z[14];
    z[14]=z[5]*z[14];
    z[16]=2*z[3];
    z[18]=z[2]*z[34];
    z[18]=z[18] + z[31] - n<T>(9,8) - z[16];
    z[18]=z[2]*z[18];
    z[24]=z[22]*z[31];
    z[22]=z[17] + z[22];
    z[22]=z[1]*z[22];
    z[18]=n<T>(3,4)*z[22] + z[18] + z[24] + n<T>(3,8) + z[3];
    z[18]=z[1]*z[18];
    z[22]= - z[23] + z[29] + n<T>(1,4) - z[3];
    z[22]=z[2]*z[22];
    z[24]=z[16] - n<T>(1,2);
    z[29]= - static_cast<T>(1)- z[33];
    z[29]=z[4]*z[29];
    z[22]=z[22] + z[29] + z[24];
    z[22]=z[2]*z[22];
    z[29]=z[32] + z[21] + n<T>(3,4) - z[16];
    z[29]=z[2]*z[29];
    z[28]= - z[28]*z[15];
    z[28]=z[29] + z[28] - n<T>(3,4) + z[3];
    z[28]=z[10]*z[28];
    z[14]=z[14] + z[18] + z[28] + z[22] - n<T>(3,8)*z[4] - z[37];
    z[14]=z[5]*z[14];
    z[18]=n<T>(1,4)*z[3];
    z[22]=29*z[3];
    z[28]=static_cast<T>(55)- z[22];
    z[28]=z[28]*z[18];
    z[28]= - static_cast<T>(8)+ z[28];
    z[29]=npow(z[3],2);
    z[28]=z[28]*z[29];
    z[31]=z[15]*z[29];
    z[33]=n<T>(7,4)*z[3];
    z[34]=static_cast<T>(11)- z[33];
    z[34]=z[3]*z[34];
    z[34]= - n<T>(7,4) + z[34];
    z[34]=z[34]*z[31];
    z[28]=z[28] + z[34];
    z[34]=n<T>(1,3)*z[4];
    z[28]=z[28]*z[34];
    z[40]=z[30] - n<T>(1,2);
    z[41]=npow(z[3],3);
    z[42]=z[40]*z[41];
    z[43]=z[34]*z[41];
    z[33]= - static_cast<T>(4)+ z[33];
    z[33]=z[33]*z[43];
    z[44]=npow(z[3],4);
    z[45]=z[44]*z[2];
    z[33]= - n<T>(7,24)*z[45] + n<T>(1,2)*z[42] + z[33];
    z[33]=z[2]*z[33];
    z[42]=n<T>(1,3)*z[3];
    z[46]=static_cast<T>(19)- n<T>(65,2)*z[3];
    z[46]=z[46]*z[42];
    z[46]=n<T>(5,2) + z[46];
    z[47]=n<T>(1,4)*z[29];
    z[46]=z[46]*z[47];
    z[28]=z[33] + z[46] + z[28];
    z[28]=z[2]*z[28];
    z[33]= - static_cast<T>(7)+ n<T>(25,4)*z[3];
    z[33]=z[3]*z[33];
    z[33]= - n<T>(7,4) + z[33];
    z[33]=z[3]*z[33];
    z[33]=n<T>(5,2) + z[33];
    z[33]=z[33]*z[42];
    z[46]=z[4]*z[3];
    z[48]= - z[19]*z[46];
    z[49]= - static_cast<T>(4)+ n<T>(11,12)*z[3];
    z[49]=z[3]*z[49];
    z[49]=n<T>(17,4) + z[49];
    z[49]=z[3]*z[49];
    z[49]= - n<T>(7,6) + z[49];
    z[49]=z[3]*z[49];
    z[48]=z[49] + z[48];
    z[48]=z[4]*z[48];
    z[49]= - n<T>(19,3) + z[30];
    z[49]=z[3]*z[49];
    z[49]=n<T>(14,3) + z[49];
    z[49]=z[3]*z[49];
    z[49]= - n<T>(4,3) + z[49];
    z[49]=z[3]*z[49];
    z[48]=z[49] + z[48];
    z[48]=z[4]*z[48];
    z[28]=z[28] + z[33] + z[48];
    z[28]=z[2]*z[28];
    z[33]=z[3] - n<T>(5,2);
    z[48]=z[33]*z[3];
    z[49]=n<T>(3,2) + z[48];
    z[49]=z[49]*z[29];
    z[50]=z[29]*z[4];
    z[51]= - static_cast<T>(2)+ z[18];
    z[51]=z[3]*z[51];
    z[51]=n<T>(7,4) + z[51];
    z[51]=z[51]*z[50];
    z[49]=z[49] + z[51];
    z[49]=z[4]*z[49];
    z[51]=5*z[3];
    z[52]=z[51] - 1;
    z[53]=n<T>(1,3)*z[41];
    z[54]= - z[52]*z[53];
    z[55]=z[3] - 3;
    z[56]= - z[55]*z[41]*z[4];
    z[57]=n<T>(1,3)*z[45];
    z[54]=z[57] + z[54] + z[56];
    z[23]=z[54]*z[23];
    z[54]=n<T>(1,2)*z[29];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(1,2) + z[27];
    z[27]=z[27]*z[54];
    z[23]=z[23] + z[27] + z[49];
    z[23]=z[2]*z[23];
    z[27]=static_cast<T>(7)- z[42];
    z[27]=z[3]*z[27];
    z[27]= - static_cast<T>(13)+ z[27];
    z[27]=z[3]*z[27];
    z[27]=n<T>(19,3) + z[27];
    z[27]=z[27]*z[46];
    z[30]=static_cast<T>(17)- z[30];
    z[30]=z[3]*z[30];
    z[30]= - static_cast<T>(25)+ z[30];
    z[30]=z[3]*z[30];
    z[30]=static_cast<T>(11)+ z[30];
    z[30]=z[3]*z[30];
    z[27]=z[30] + z[27];
    z[27]=z[4]*z[27];
    z[30]=static_cast<T>(13)- z[51];
    z[30]=z[3]*z[30];
    z[30]= - static_cast<T>(11)+ z[30];
    z[30]=z[3]*z[30];
    z[30]=static_cast<T>(3)+ z[30];
    z[30]=z[3]*z[30];
    z[27]=z[30] + z[27];
    z[27]=z[4]*z[27];
    z[30]=static_cast<T>(3)- n<T>(7,3)*z[3];
    z[30]=z[3]*z[30];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[3]*z[30];
    z[30]= - n<T>(5,3) + z[30];
    z[30]=z[3]*z[30];
    z[27]=z[30] + z[27];
    z[23]=n<T>(1,4)*z[27] + z[23];
    z[23]=z[2]*z[23];
    z[27]= - static_cast<T>(11)+ z[3];
    z[27]=z[27]*z[42];
    z[27]=static_cast<T>(9)+ z[27];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(25,3) + z[27];
    z[27]=z[27]*z[35];
    z[30]=z[55]*z[3];
    z[30]=z[30] + 3;
    z[30]=z[30]*z[3];
    z[30]=z[30] - 1;
    z[49]= - z[30]*z[15];
    z[27]=z[49] + n<T>(4,3) + z[27];
    z[27]=z[4]*z[27];
    z[49]=z[3] - 5;
    z[51]=z[3]*z[49];
    z[51]=static_cast<T>(9)+ z[51];
    z[51]=z[3]*z[51];
    z[51]= - static_cast<T>(7)+ z[51];
    z[51]=z[51]*z[35];
    z[27]=z[27] + static_cast<T>(1)+ z[51];
    z[27]=z[4]*z[27];
    z[30]=z[30]*z[35];
    z[27]=z[30] + z[27];
    z[27]=z[4]*z[27];
    z[30]=z[36]*z[29];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[3]*z[30];
    z[30]= - n<T>(1,2) + z[30];
    z[23]=z[23] + n<T>(1,3)*z[30] + z[27];
    z[23]=z[10]*z[23];
    z[27]=n<T>(7,6)*z[3];
    z[30]=static_cast<T>(5)- z[27];
    z[30]=z[3]*z[30];
    z[30]= - n<T>(15,2) + z[30];
    z[30]=z[30]*z[35];
    z[30]=n<T>(7,3) + z[30];
    z[30]=z[3]*z[30];
    z[51]= - z[33]*z[35];
    z[51]= - static_cast<T>(1)+ z[51];
    z[51]=z[3]*z[51];
    z[51]=n<T>(1,4) + z[51];
    z[51]=z[51]*z[20];
    z[30]=z[51] - n<T>(1,2) + z[30];
    z[30]=z[4]*z[30];
    z[27]=static_cast<T>(3)- z[27];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(5,2) + z[27];
    z[27]=z[3]*z[27];
    z[27]=n<T>(2,3) + z[27];
    z[27]=z[3]*z[27];
    z[27]=z[27] + z[30];
    z[27]=z[4]*z[27];
    z[30]=static_cast<T>(1)- n<T>(7,12)*z[3];
    z[30]=z[30]*z[29];
    z[30]= - n<T>(2,3) + z[30];
    z[30]=z[3]*z[30];
    z[23]=z[23] + z[28] + z[27] + n<T>(1,4) + z[30];
    z[23]=z[10]*z[23];
    z[27]= - static_cast<T>(7)+ 19*z[3];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(19,4) + z[27];
    z[27]=z[27]*z[29];
    z[28]= - static_cast<T>(19)+ 17*z[3];
    z[28]=z[3]*z[28];
    z[28]=static_cast<T>(11)+ z[28];
    z[28]=z[28]*z[31];
    z[27]=z[27] + z[28];
    z[28]=7*z[3];
    z[30]=n<T>(1,3) - z[28];
    z[30]=z[30]*z[41];
    z[31]=z[42] - n<T>(1,2);
    z[51]=z[31]*z[41];
    z[56]= - z[4]*z[51];
    z[30]=z[57] + n<T>(1,4)*z[30] + z[56];
    z[30]=z[2]*z[30];
    z[27]=n<T>(1,6)*z[27] + z[30];
    z[27]=z[2]*z[27];
    z[30]=n<T>(31,3) - z[28];
    z[30]=z[3]*z[30];
    z[30]= - n<T>(31,6) + z[30];
    z[30]=z[3]*z[30];
    z[30]=n<T>(11,6) + z[30];
    z[30]=z[30]*z[18];
    z[56]=z[37]*z[46];
    z[56]=z[56] + n<T>(3,4)*z[3];
    z[57]= - z[4]*z[56];
    z[30]=z[30] + z[57];
    z[30]=z[4]*z[30];
    z[22]=static_cast<T>(23)- z[22];
    z[22]=z[3]*z[22];
    z[22]=n<T>(23,2) + z[22];
    z[22]=z[22]*z[42];
    z[22]= - n<T>(9,2) + z[22];
    z[22]=z[22]*z[18];
    z[22]=z[27] + z[22] + z[30];
    z[22]=z[2]*z[22];
    z[27]=z[3] - 2;
    z[30]=z[3]*z[27];
    z[30]=n<T>(3,4) + n<T>(2,3)*z[30];
    z[30]=z[30]*z[29];
    z[57]=z[36]*z[3];
    z[58]=z[57] + n<T>(1,2);
    z[59]=z[58]*z[46];
    z[60]=n<T>(1,4)*z[19];
    z[59]=z[59] - z[60];
    z[59]=z[59]*z[4];
    z[59]=z[59] - z[60];
    z[60]= - z[4]*z[59];
    z[30]=z[60] - n<T>(1,12) + z[30];
    z[30]=z[4]*z[30];
    z[60]= - n<T>(5,2) + z[16];
    z[60]=z[60]*z[42];
    z[60]= - n<T>(1,4) + z[60];
    z[60]=z[3]*z[60];
    z[60]=static_cast<T>(1)+ z[60];
    z[60]=z[3]*z[60];
    z[22]=z[23] + z[22] + z[30] - n<T>(1,12) + z[60];
    z[22]=z[10]*z[22];
    z[23]=z[35] - n<T>(1,3);
    z[30]=z[23]*z[29];
    z[60]=z[19]*z[41];
    z[61]=z[4]*z[60];
    z[62]=n<T>(1,6)*z[2];
    z[63]= - z[41]*z[62];
    z[30]=z[63] + z[30] + n<T>(1,6)*z[61];
    z[30]=z[2]*z[30];
    z[61]=n<T>(5,3) - z[3];
    z[61]=z[61]*z[35];
    z[61]= - n<T>(1,3) + z[61];
    z[61]=z[61]*z[29];
    z[63]=z[37]*z[43];
    z[61]=z[61] + z[63];
    z[61]=z[4]*z[61];
    z[39]= - z[3]*z[39];
    z[39]= - n<T>(1,2) + z[39];
    z[39]=z[39]*z[35];
    z[30]=z[30] + z[39] + z[61];
    z[30]=z[2]*z[30];
    z[39]=n<T>(1,3)*z[29];
    z[61]=n<T>(7,2) - z[16];
    z[61]=z[3]*z[61];
    z[61]= - static_cast<T>(1)+ z[61];
    z[61]=z[61]*z[39];
    z[63]=z[15]*z[41];
    z[64]= - n<T>(1,3) + z[3];
    z[64]=z[64]*z[63];
    z[61]=z[61] + z[64];
    z[61]=z[4]*z[61];
    z[64]= - n<T>(7,3) + z[3];
    z[64]=z[64]*z[35];
    z[64]=static_cast<T>(1)+ z[64];
    z[64]=z[3]*z[64];
    z[64]= - n<T>(1,4) + z[64];
    z[64]=z[3]*z[64];
    z[61]=z[64] + z[61];
    z[61]=z[4]*z[61];
    z[64]=z[42] - 1;
    z[65]=z[64]*z[3];
    z[66]=n<T>(3,4) + z[65];
    z[66]=z[66]*z[35];
    z[30]=z[30] + z[61] - n<T>(1,3) + z[66];
    z[30]=z[2]*z[30];
    z[61]=z[21]*z[44];
    z[66]= - z[23]*z[41];
    z[66]=z[66] + z[61];
    z[66]=z[4]*z[66];
    z[15]=z[15]*z[44];
    z[67]= - z[37]*z[41];
    z[68]=n<T>(1,4)*z[45];
    z[67]=z[68] + z[67] + z[15];
    z[69]=n<T>(1,3)*z[2];
    z[67]=z[67]*z[69];
    z[19]=z[3]*z[19];
    z[19]=n<T>(1,4) + z[19];
    z[19]=z[19]*z[54];
    z[19]=z[67] + z[19] + z[66];
    z[19]=z[2]*z[19];
    z[66]=z[34]*z[44];
    z[67]= - n<T>(1,2)*z[60] + z[66];
    z[67]=z[4]*z[67];
    z[70]= - n<T>(2,3) + z[35];
    z[70]=z[3]*z[70];
    z[70]=n<T>(1,4) + z[70];
    z[70]=z[70]*z[29];
    z[67]=z[70] + z[67];
    z[67]=z[4]*z[67];
    z[70]= - z[3]*z[31];
    z[70]= - n<T>(1,4) + z[70];
    z[70]=z[3]*z[70];
    z[70]= - n<T>(5,24) + z[70];
    z[70]=z[3]*z[70];
    z[19]=z[19] + z[70] + z[67];
    z[19]=z[2]*z[19];
    z[57]=n<T>(3,4) + z[57];
    z[57]=z[57]*z[54];
    z[27]= - z[27]*z[41];
    z[67]=z[44]*z[4];
    z[27]=z[27] + n<T>(5,4)*z[67];
    z[27]=z[27]*z[34];
    z[27]=z[57] + z[27];
    z[27]=z[4]*z[27];
    z[57]=z[36]*z[42];
    z[57]=z[57] + n<T>(1,4);
    z[57]=z[57]*z[3];
    z[57]=z[57] + n<T>(5,12);
    z[57]=z[57]*z[3];
    z[27]= - z[57] + z[27];
    z[27]=z[4]*z[27];
    z[57]= - n<T>(3,8) + z[57];
    z[19]=z[19] + n<T>(1,2)*z[57] + z[27];
    z[19]=z[1]*z[19];
    z[27]= - z[55]*z[35];
    z[27]= - n<T>(1,3) + z[27];
    z[27]=z[27]*z[29];
    z[24]=z[24]*z[43];
    z[24]=z[27] + z[24];
    z[24]=z[4]*z[24];
    z[27]=n<T>(5,4) + z[65];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(1,4) + z[27];
    z[27]=z[3]*z[27];
    z[24]=z[27] + z[24];
    z[24]=z[4]*z[24];
    z[27]= - z[64]*z[35];
    z[27]= - n<T>(2,3) + z[27];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(1,24) + z[27];
    z[27]=z[3]*z[27];
    z[24]=z[24] - n<T>(1,3) + z[27];
    z[24]=z[4]*z[24];
    z[27]=z[31]*z[35];
    z[27]=n<T>(1,3) + z[27];
    z[19]=z[19] + z[30] + n<T>(1,2)*z[27] + z[24];
    z[19]=z[1]*z[19];
    z[24]=z[64]*z[41]*z[21];
    z[27]=z[67] - z[45];
    z[27]= - z[51] - n<T>(1,6)*z[27];
    z[27]=z[27]*z[17];
    z[30]=z[18] - 1;
    z[31]=z[3]*z[30];
    z[31]=n<T>(7,8) + z[31];
    z[31]=z[31]*z[39];
    z[24]=z[27] + z[31] + z[24];
    z[24]=z[2]*z[24];
    z[27]=z[3] - n<T>(7,2);
    z[21]=z[27]*z[29]*z[21];
    z[18]=z[33]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[3]*z[18];
    z[18]=z[18] + z[21];
    z[18]=n<T>(1,3)*z[18] + z[24];
    z[18]=z[2]*z[18];
    z[21]= - z[30]*z[46];
    z[21]=z[21] - static_cast<T>(4)+ n<T>(47,16)*z[3];
    z[18]=n<T>(1,3)*z[21] + z[18];
    z[18]=z[2]*z[18];
    z[21]=static_cast<T>(97)- 25*z[3];
    z[24]=static_cast<T>(4)- n<T>(31,16)*z[3];
    z[24]=z[4]*z[24];
    z[21]=n<T>(1,16)*z[21] + z[24];
    z[18]=n<T>(1,3)*z[21] + z[18];
    z[18]=z[2]*z[18];
    z[21]= - static_cast<T>(5)- n<T>(11,2)*z[4];
    z[18]=n<T>(1,8)*z[21] + z[18];
    z[18]=z[2]*z[18];
    z[21]=z[36]*z[41];
    z[24]= - z[68] + z[21] + z[15];
    z[24]=z[24]*z[62];
    z[31]=n<T>(1,6)*z[3];
    z[33]=static_cast<T>(1)- z[31];
    z[33]=z[3]*z[33];
    z[33]= - n<T>(13,12) + z[33];
    z[33]=z[33]*z[47];
    z[36]= - z[30]*z[41];
    z[36]=z[36] - n<T>(1,8)*z[67];
    z[36]=z[36]*z[34];
    z[24]=z[24] + z[33] + z[36];
    z[24]=z[2]*z[24];
    z[33]=n<T>(13,6) - z[3];
    z[33]=z[33]*z[54];
    z[33]=z[33] - z[43];
    z[33]=z[4]*z[33];
    z[27]= - z[3]*z[27];
    z[27]=n<T>(1,4) + z[27];
    z[27]=z[27]*z[31];
    z[27]=z[27] + z[33];
    z[24]=n<T>(1,2)*z[27] + z[24];
    z[24]=z[2]*z[24];
    z[27]= - static_cast<T>(1)- z[28];
    z[27]=z[3]*z[27];
    z[27]=z[27] - n<T>(13,2)*z[50];
    z[27]=z[4]*z[27];
    z[28]= - n<T>(1,3) - n<T>(1,16)*z[3];
    z[28]=z[3]*z[28];
    z[24]=z[24] + n<T>(1,24)*z[27] + n<T>(5,6) + z[28];
    z[24]=z[2]*z[24];
    z[27]=n<T>(1,16)*z[46] + z[49];
    z[27]=z[4]*z[27];
    z[27]=z[27] - static_cast<T>(4)+ n<T>(11,16)*z[3];
    z[24]=n<T>(1,3)*z[27] + z[24];
    z[24]=z[2]*z[24];
    z[27]=static_cast<T>(4)+ n<T>(5,2)*z[4];
    z[27]=z[27]*z[34];
    z[24]=z[24] + n<T>(7,16) + z[27];
    z[24]=z[9]*z[24]*npow(z[2],2);
    z[18]=z[18] + z[24];
    z[18]=z[9]*z[18];
    z[24]=static_cast<T>(1)- n<T>(11,2)*z[3];
    z[24]=z[24]*z[29];
    z[27]= - z[52]*z[63];
    z[24]=z[24] + z[27];
    z[24]=z[24]*z[34];
    z[27]= - z[40]*z[35];
    z[24]=z[27] + z[24];
    z[24]=z[4]*z[24];
    z[27]= - static_cast<T>(1)+ 11*z[3];
    z[24]=n<T>(1,24)*z[27] + z[24];
    z[24]=z[4]*z[24];
    z[24]=n<T>(1,48) + z[24];
    z[24]=z[4]*z[24];
    z[27]= - n<T>(5,3)*z[41] - z[67];
    z[27]=z[4]*z[27];
    z[27]= - z[29] + z[27];
    z[27]=z[4]*z[27];
    z[27]=n<T>(5,4)*z[3] + z[27];
    z[27]=z[27]*z[20]*z[26];
    z[24]=z[24] + z[27];
    z[24]=z[1]*z[24];
    z[27]=z[66] + z[41];
    z[27]=z[27]*z[4];
    z[27]=z[27] + n<T>(7,6)*z[29];
    z[27]=z[27]*z[4];
    z[28]= - z[42] + z[27];
    z[28]=z[4]*z[28];
    z[28]= - n<T>(1,12) + z[28];
    z[28]=z[28]*z[20];
    z[31]=z[41] + z[15];
    z[31]=z[31]*z[34];
    z[31]=z[47] + z[31];
    z[31]=z[4]*z[31];
    z[33]=n<T>(5,12)*z[3];
    z[31]= - z[33] + z[31];
    z[36]=npow(z[4],3);
    z[31]=z[1]*z[31]*z[36];
    z[28]=z[28] + z[31];
    z[28]=z[28]*z[26];
    z[31]=z[41] + z[61];
    z[31]=z[4]*z[31];
    z[31]=n<T>(13,8)*z[29] + z[31];
    z[31]=z[4]*z[31];
    z[31]=n<T>(5,8)*z[3] + z[31];
    z[31]=z[4]*z[31];
    z[31]= - n<T>(5,16) + z[31];
    z[39]=n<T>(1,3)*z[20];
    z[31]=z[31]*z[39];
    z[40]=npow(z[1],2)*z[36]*z[5];
    z[28]=n<T>(3,16)*z[40] + z[31] + z[28];
    z[28]=z[8]*z[28];
    z[31]=z[4] + n<T>(1,2);
    z[41]=z[31] + n<T>(3,2)*z[1];
    z[41]=z[1]*z[20]*z[41];
    z[40]=z[41] - n<T>(1,2)*z[40];
    z[40]=z[40]*z[5];
    z[27]= - z[33] - z[27];
    z[27]=z[4]*z[27];
    z[27]=n<T>(1,12) + z[27];
    z[27]=z[4]*z[27];
    z[24]=z[28] - n<T>(3,8)*z[40] + z[27] + z[24];
    z[24]=z[8]*z[24];
    z[27]=static_cast<T>(1)- n<T>(29,2)*z[3];
    z[27]=z[27]*z[42];
    z[27]=n<T>(7,4) + z[27];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(37,12) + z[27];
    z[27]=z[3]*z[27];
    z[28]=z[44]*z[39];
    z[27]=z[28] + n<T>(3,4) + z[27];
    z[28]= - z[44]*z[17];
    z[28]=z[60] + z[28];
    z[28]=z[28]*z[17];
    z[33]= - z[3]*z[37];
    z[33]=n<T>(1,8) + z[33];
    z[33]=z[33]*z[29];
    z[28]=z[33] + z[28];
    z[28]=z[28]*z[17];
    z[16]=n<T>(1,4) + z[16];
    z[16]=z[3]*z[16];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[3]*z[16];
    z[16]=n<T>(19,16) + z[16];
    z[16]=z[3]*z[16];
    z[16]=z[16] + z[28];
    z[16]=z[16]*z[69];
    z[16]=n<T>(1,4)*z[27] + z[16];
    z[16]=z[2]*z[16];
    z[15]= - z[21] + z[15];
    z[15]=z[4]*z[15];
    z[15]=z[47] + z[15];
    z[15]=z[15]*z[34];
    z[21]= - static_cast<T>(1)+ n<T>(11,4)*z[3];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(11,8) + z[21];
    z[21]=z[3]*z[21];
    z[21]=n<T>(23,8) + z[21];
    z[21]=z[21]*z[42];
    z[15]=z[16] + z[15] - n<T>(3,8) + z[21];
    z[15]=z[2]*z[15];
    z[16]=static_cast<T>(3)+ n<T>(13,2)*z[4];
    z[16]=z[4]*z[16];
    z[21]=n<T>(13,2)*z[2] - static_cast<T>(3)- 13*z[4];
    z[21]=z[2]*z[21];
    z[16]=z[16] + z[21];
    z[16]=z[9]*z[16];
    z[21]= - z[2] + z[31];
    z[16]=3*z[21] + z[16];
    z[16]=z[9]*z[16];
    z[21]=z[6]*npow(z[9],2);
    z[17]= - z[4] + z[17];
    z[17]=z[17]*z[21];
    z[16]=3*z[17] - n<T>(3,2) + z[16];
    z[17]=n<T>(1,2)*z[6];
    z[16]=z[16]*z[17];
    z[27]=static_cast<T>(5)+ n<T>(31,6)*z[4];
    z[27]=z[4]*z[27];
    z[28]=n<T>(31,6)*z[2] - static_cast<T>(5)- n<T>(31,3)*z[4];
    z[28]=z[2]*z[28];
    z[27]=z[28] + n<T>(3,4) + z[27];
    z[27]=z[9]*z[27];
    z[25]=z[27] - 5*z[2] + n<T>(19,4) + z[25];
    z[25]=z[2]*z[25];
    z[27]= - static_cast<T>(3)+ z[4];
    z[25]=n<T>(1,4)*z[27] + z[25];
    z[25]=z[9]*z[25];
    z[27]= - static_cast<T>(3)+ n<T>(17,3)*z[2];
    z[27]=n<T>(1,2)*z[27] + 3*z[1];
    z[28]=z[4]*npow(z[8],2);
    z[16]=z[16] - n<T>(1,12)*z[28] + n<T>(1,2)*z[27] + z[25];
    z[16]=z[6]*z[16];
    z[25]=z[2]*z[56];
    z[25]=z[59] + z[25];
    z[25]=z[20]*z[25];
    z[27]= - z[4]*z[58];
    z[27]= - z[32] + z[38] + z[27];
    z[27]=z[11]*z[36]*z[27];
    z[25]=n<T>(1,2)*z[27] + z[25];
    z[25]=z[11]*z[25];
    z[21]= - z[20]*z[21];
    z[27]= - z[9]*z[4];
    z[21]=z[27] + z[21];
    z[27]=npow(z[6],2);
    z[21]=z[21]*z[27];
    z[20]=npow(z[6],3)*z[20]*z[13];
    z[27]=z[4]*z[27];
    z[27]=z[27] + z[20];
    z[27]=z[12]*z[27];
    z[20]= - z[9]*z[20];
    z[20]=z[27] + z[21] + z[20];
    z[21]=n<T>(3,16)*z[13];
    z[20]=z[21]*z[20];
    z[21]=static_cast<T>(2)- z[35];
    z[21]=z[21]*z[53];
    z[21]=z[21] + z[61];
    z[21]=z[4]*z[21];
    z[27]=z[30]*z[42];
    z[27]=n<T>(5,8) + z[27];
    z[27]=z[27]*z[29];
    z[21]=z[27] + z[21];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(1,12)*z[48] + z[21];
    z[21]=z[4]*z[21];
    z[26]= - z[26] - z[31];
    z[26]=z[1]*z[26];
    z[17]= - z[17] + n<T>(1,2) + z[1];
    z[17]=z[6]*z[17];
    z[17]=z[26] + z[17];
    z[17]=z[6]*z[17];
    z[26]=z[31] + z[1];
    z[26]=z[1]*z[4]*z[26];
    z[17]=z[17] + z[26] - z[40];
    z[17]=z[7]*z[17];
    z[23]= - z[3]*z[23];
    z[23]=n<T>(1,3) + z[23];
    z[23]=z[3]*z[23];
    z[23]= - n<T>(7,6) + z[23];
    z[23]=z[3]*z[23];
    z[23]=n<T>(3,8) + z[23];

    r += z[14] + z[15] + n<T>(1,4)*z[16] + n<T>(3,8)*z[17] + z[18] + z[19] + 
      z[20] + z[21] + z[22] + n<T>(1,2)*z[23] + z[24] + z[25];
 
    return r;
}

template double qg_2lha_r2005(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2005(const std::array<dd_real,30>&);
#endif
