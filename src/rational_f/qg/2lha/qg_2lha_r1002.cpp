#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1002(const std::array<T,30>& k) {
  T z[48];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[3];
    z[7]=k[5];
    z[8]=k[10];
    z[9]=k[27];
    z[10]=k[12];
    z[11]=k[20];
    z[12]=z[4] - n<T>(1,2);
    z[13]=z[12]*z[2];
    z[14]=n<T>(3,4)*z[4];
    z[15]= - z[14] - z[13];
    z[15]=z[2]*z[15];
    z[15]=n<T>(3,8) + z[15];
    z[15]=z[2]*z[15];
    z[16]=npow(z[2],3);
    z[17]=n<T>(1,2)*z[1];
    z[18]=z[16]*z[17];
    z[19]=n<T>(1,2) - z[2];
    z[19]=z[2]*z[19];
    z[19]= - n<T>(1,8) + z[19];
    z[19]=z[2]*z[19];
    z[19]=z[19] + z[18];
    z[19]=z[1]*z[19];
    z[20]=z[4] + 1;
    z[15]=z[19] - n<T>(1,8)*z[20] + z[15];
    z[15]=z[1]*z[15];
    z[19]=static_cast<T>(1)- n<T>(3,2)*z[4];
    z[19]=z[4]*z[19];
    z[19]= - static_cast<T>(1)+ z[19];
    z[21]=z[4] - 1;
    z[22]=z[21]*z[2];
    z[23]= - z[4]*z[22];
    z[19]=n<T>(1,2)*z[19] + z[23];
    z[19]=z[2]*z[19];
    z[23]=z[4] + n<T>(1,4);
    z[19]=z[19] - z[23];
    z[19]=z[2]*z[19];
    z[24]=z[4] + n<T>(1,2);
    z[25]=z[24]*z[4];
    z[26]=static_cast<T>(1)- z[25];
    z[15]=z[15] + n<T>(1,4)*z[26] + z[19];
    z[15]=z[1]*z[15];
    z[19]=npow(z[4],2);
    z[26]= - z[20]*z[19];
    z[26]= - static_cast<T>(1)+ z[26];
    z[27]=z[4] - n<T>(3,2);
    z[28]=z[27]*z[19];
    z[29]= - z[2]*z[28];
    z[14]=z[14] - 1;
    z[14]=z[14]*z[4];
    z[30]= - n<T>(3,4) - z[14];
    z[30]=z[4]*z[30];
    z[29]=z[30] + z[29];
    z[29]=z[2]*z[29];
    z[30]=n<T>(5,8) - z[4];
    z[30]=z[4]*z[30];
    z[29]=z[29] + n<T>(1,2) + z[30];
    z[29]=z[2]*z[29];
    z[15]=z[15] + n<T>(1,4)*z[26] + z[29];
    z[26]=n<T>(1,2)*z[4];
    z[29]=z[26] - 1;
    z[30]=z[29]*z[4];
    z[31]=n<T>(1,2) + z[30];
    z[31]=z[31]*z[19];
    z[32]=npow(z[4],3);
    z[33]=z[32]*z[2];
    z[34]=z[29]*z[33];
    z[31]=n<T>(3,4)*z[31] + z[34];
    z[31]=z[2]*z[31];
    z[24]=z[24]*z[19];
    z[34]=3*z[4];
    z[35]= - static_cast<T>(1)+ z[34];
    z[35]=z[35]*z[32];
    z[36]=npow(z[4],4);
    z[37]=z[36]*z[2];
    z[35]=n<T>(1,2)*z[35] + z[37];
    z[35]=z[2]*z[35];
    z[24]=z[24] + z[35];
    z[24]=z[7]*z[24];
    z[27]=z[27]*z[4];
    z[35]= - n<T>(1,2) + z[27];
    z[35]=z[35]*z[26];
    z[24]=n<T>(1,4)*z[24] + z[35] + z[31];
    z[31]=z[7]*z[2];
    z[24]=z[24]*z[31];
    z[35]=n<T>(1,2)*z[2];
    z[38]=static_cast<T>(1)+ z[2];
    z[38]=z[1]*z[38]*z[35];
    z[39]=z[26] + 1;
    z[40]= - z[2] - z[39];
    z[40]=z[2]*z[40];
    z[38]=z[38] + z[40] + z[39];
    z[38]=z[1]*z[38];
    z[13]=n<T>(3,2) - z[13];
    z[13]=z[2]*z[13];
    z[13]=z[38] - n<T>(3,2)*z[20] + z[13];
    z[13]=z[1]*z[13];
    z[22]= - z[22] + n<T>(3,2);
    z[22]=z[4]*z[22];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[2]*z[22];
    z[13]=z[22] + z[13];
    z[13]=z[13]*z[17];
    z[22]= - n<T>(3,2)*z[19] + z[33];
    z[22]=z[29]*z[22];
    z[38]=z[35]*z[36];
    z[40]= - z[32] + z[38];
    z[41]=n<T>(1,2)*z[7];
    z[40]=z[40]*z[41];
    z[22]=z[22] + z[40];
    z[22]=z[22]*z[31];
    z[28]= - z[35]*z[28];
    z[14]=z[14] + z[28];
    z[14]=z[2]*z[14];
    z[13]=z[22] + z[13] + n<T>(1,2) + z[14];
    z[13]=z[3]*z[13];
    z[13]=n<T>(1,4)*z[13] + n<T>(1,2)*z[15] + z[24];
    z[14]=n<T>(1,2)*z[3];
    z[13]=z[13]*z[14];
    z[15]=z[4] + n<T>(5,2);
    z[22]= - z[15]*z[19];
    z[24]=z[3]*z[20]*z[32];
    z[22]=z[22] + z[24];
    z[14]=z[22]*z[14];
    z[22]=n<T>(1,2)*z[19];
    z[15]=z[15]*z[22];
    z[24]= - z[5]*z[39]*z[32];
    z[15]=z[15] + z[24];
    z[15]=z[5]*z[15];
    z[24]=npow(z[3],2);
    z[28]=z[6]*z[32]*z[24];
    z[14]= - n<T>(3,4)*z[28] + z[14] + z[15];
    z[14]=z[6]*z[14];
    z[15]=z[5] - z[3];
    z[28]=z[39]*z[4];
    z[31]=z[28] + n<T>(1,2);
    z[31]= - z[19]*z[31]*z[15];
    z[39]=3*z[32];
    z[40]=z[3]*z[36];
    z[40]=z[39] + z[40];
    z[40]=z[3]*z[40];
    z[42]=z[5]*z[36];
    z[42]= - z[39] + z[42];
    z[42]=z[5]*z[42];
    z[40]=z[40] + z[42];
    z[42]=n<T>(1,2)*z[6];
    z[40]=z[40]*z[42];
    z[43]= - z[32]*z[15];
    z[40]=z[40] + z[43];
    z[40]=z[6]*z[40];
    z[15]=z[9]*npow(z[6],2)*z[36]*z[15];
    z[15]=z[40] + z[15];
    z[15]=z[9]*z[15];
    z[14]=n<T>(1,2)*z[15] + z[14] + z[31];
    z[14]=z[9]*z[14];
    z[15]=static_cast<T>(1)+ n<T>(5,2)*z[4];
    z[15]=z[15]*z[41]*z[19];
    z[31]=z[34] + 1;
    z[40]=z[31]*z[4];
    z[15]=z[15] - z[40];
    z[15]=z[15]*z[7];
    z[40]=static_cast<T>(1)+ n<T>(7,2)*z[4];
    z[15]=z[15] + n<T>(1,2)*z[40];
    z[40]=z[32]*z[7];
    z[43]=3*z[19];
    z[40]=z[40] - z[43];
    z[40]=z[40]*z[41];
    z[40]=z[40] + z[4];
    z[44]=z[3]*z[40];
    z[45]=static_cast<T>(1)- z[3];
    z[45]=z[45]*z[42];
    z[44]=z[45] + z[44] - z[15];
    z[44]=z[3]*z[44];
    z[15]= - z[42] + z[15];
    z[15]=z[8]*z[15];
    z[40]=z[40] - z[42];
    z[42]= - z[8] + z[3];
    z[40]=z[11]*z[40]*z[42];
    z[15]=z[40] + z[15] + z[44];
    z[15]=z[11]*z[15];
    z[40]=n<T>(1,4)*z[4];
    z[42]=z[20]*z[40];
    z[42]= - static_cast<T>(1)+ z[42];
    z[42]=z[4]*z[42];
    z[44]=z[29]*z[1];
    z[12]=z[12]*z[4];
    z[45]= - z[12] + z[44];
    z[45]=z[45]*z[17];
    z[42]=z[42] + z[45];
    z[45]=npow(z[7],2);
    z[42]=z[42]*z[45];
    z[20]=z[20]*z[17];
    z[20]= - z[25] + z[20];
    z[20]=z[1]*z[20];
    z[20]= - z[32] + z[20];
    z[20]=z[20]*z[24];
    z[20]=z[42] + n<T>(1,2)*z[20];
    z[20]=z[6]*z[20];
    z[14]=z[14] + z[20] + z[15];
    z[15]=n<T>(41,2) + z[34];
    z[15]=z[15]*z[26];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[40];
    z[20]=5*z[4];
    z[24]= - n<T>(7,2) + z[20];
    z[24]=z[24]*z[22];
    z[24]=z[24] - 3*z[33];
    z[24]=z[2]*z[24];
    z[42]= - static_cast<T>(3)- z[26];
    z[42]=z[42]*z[40];
    z[46]= - z[2]*z[19];
    z[42]=z[42] + z[46];
    z[42]=z[1]*z[42];
    z[15]=n<T>(3,2)*z[42] + z[15] + z[24];
    z[15]=z[1]*z[15];
    z[24]= - static_cast<T>(1)+ n<T>(11,2)*z[4];
    z[24]=z[24]*z[32];
    z[42]=3*z[2];
    z[36]= - z[36]*z[42];
    z[24]=z[24] + z[36];
    z[24]=z[24]*z[35];
    z[36]=n<T>(17,8) - z[4];
    z[36]=z[4]*z[36];
    z[36]= - n<T>(1,2) + z[36];
    z[36]=z[36]*z[19];
    z[24]=z[36] + z[24];
    z[24]=z[2]*z[24];
    z[36]= - static_cast<T>(23)- z[34];
    z[36]=z[4]*z[36];
    z[36]= - static_cast<T>(7)+ z[36];
    z[36]=z[36]*z[19];
    z[15]=z[15] + n<T>(1,16)*z[36] + z[24];
    z[15]=z[15]*z[41];
    z[24]= - n<T>(27,4) - z[20];
    z[24]=z[4]*z[24];
    z[24]=n<T>(9,4) + z[24];
    z[24]=z[24]*z[22];
    z[36]=n<T>(11,2) + z[34];
    z[36]=z[36]*z[33];
    z[24]=z[24] + z[36];
    z[24]=z[2]*z[24];
    z[36]=z[4] + 3;
    z[36]=z[36]*z[4];
    z[36]=z[36] + n<T>(9,2);
    z[36]=z[36]*z[4];
    z[46]=n<T>(5,4) - z[36];
    z[46]=z[46]*z[26];
    z[24]=z[46] + z[24];
    z[24]=z[2]*z[24];
    z[46]=static_cast<T>(1)- z[40];
    z[46]=z[4]*z[46];
    z[46]= - n<T>(1,2) + z[46];
    z[46]=z[46]*z[40];
    z[24]=z[46] + z[24];
    z[46]= - n<T>(3,8) + z[28];
    z[46]=z[46]*z[26];
    z[33]=2*z[32] - n<T>(9,4)*z[33];
    z[33]=z[2]*z[33];
    z[33]=z[46] + z[33];
    z[33]=z[2]*z[33];
    z[46]=npow(z[2],2);
    z[47]= - z[46]*z[43];
    z[29]= - n<T>(1,4)*z[29] + z[47];
    z[47]=n<T>(1,4)*z[1];
    z[29]=z[29]*z[47];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[29] + n<T>(1,16)*z[12] + z[33];
    z[12]=z[1]*z[12];
    z[12]=z[15] + n<T>(1,2)*z[24] + z[12];
    z[12]=z[7]*z[12];
    z[15]=11*z[4];
    z[24]=n<T>(73,2) + z[15];
    z[24]=z[4]*z[24];
    z[24]=n<T>(171,4) + z[24];
    z[24]=z[24]*z[40];
    z[24]=static_cast<T>(5)+ z[24];
    z[24]=z[4]*z[24];
    z[29]= - n<T>(19,2) - z[34];
    z[29]=z[4]*z[29];
    z[29]= - n<T>(21,2) + z[29];
    z[29]=z[4]*z[29];
    z[29]= - n<T>(9,2) + z[29];
    z[29]=z[4]*z[29];
    z[29]= - n<T>(1,2) + z[29];
    z[29]=z[29]*z[35];
    z[24]=z[29] + n<T>(11,16) + z[24];
    z[24]=z[2]*z[24];
    z[29]= - n<T>(33,8) - z[4];
    z[29]=z[4]*z[29];
    z[29]= - n<T>(47,8) + z[29];
    z[29]=z[4]*z[29];
    z[29]= - n<T>(27,8) + z[29];
    z[29]=z[4]*z[29];
    z[24]=z[24] - n<T>(5,8) + z[29];
    z[24]=z[2]*z[24];
    z[25]=n<T>(5,2) - z[25];
    z[25]=z[4]*z[25];
    z[25]=n<T>(5,2) + z[25];
    z[25]=z[4]*z[25];
    z[25]=n<T>(3,4) + z[25];
    z[24]=n<T>(1,4)*z[25] + z[24];
    z[25]= - n<T>(7,2) - z[34];
    z[25]=z[4]*z[25];
    z[25]= - n<T>(1,2) + z[25];
    z[25]=z[25]*z[42];
    z[29]=n<T>(29,2) + z[15];
    z[29]=z[4]*z[29];
    z[29]=n<T>(11,4) + z[29];
    z[25]=n<T>(1,2)*z[29] + z[25];
    z[25]=z[25]*z[46];
    z[29]=n<T>(1,2) + z[34];
    z[16]=z[1]*z[29]*z[16];
    z[16]=z[25] + z[16];
    z[16]=z[16]*z[47];
    z[25]=n<T>(13,2) + z[34];
    z[25]=z[25]*z[40];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[4]*z[25];
    z[25]=n<T>(1,8) + z[25];
    z[25]=z[25]*z[42];
    z[15]= - n<T>(51,2) - z[15];
    z[15]=z[4]*z[15];
    z[15]= - n<T>(69,4) + z[15];
    z[15]=z[4]*z[15];
    z[15]= - n<T>(11,4) + z[15];
    z[15]=n<T>(1,4)*z[15] + z[25];
    z[15]=z[2]*z[15];
    z[25]=n<T>(25,8) + z[4];
    z[25]=z[4]*z[25];
    z[25]=n<T>(11,4) + z[25];
    z[25]=z[4]*z[25];
    z[25]=n<T>(5,8) + z[25];
    z[15]=n<T>(1,2)*z[25] + z[15];
    z[15]=z[2]*z[15];
    z[15]=z[15] + z[16];
    z[15]=z[1]*z[15];
    z[15]=n<T>(1,2)*z[24] + z[15];
    z[15]=z[5]*z[15];
    z[16]= - n<T>(7,4) + z[4];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(1,2) + z[16];
    z[23]=z[2]*z[23];
    z[16]=n<T>(3,2)*z[16] + 5*z[23];
    z[16]=z[16]*z[46];
    z[18]= - z[31]*z[18];
    z[16]=z[16] + z[18];
    z[16]=z[16]*z[17];
    z[17]=static_cast<T>(1)+ 9*z[4];
    z[17]=z[17]*z[26];
    z[17]= - static_cast<T>(5)+ z[17];
    z[17]=z[4]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[35];
    z[18]=n<T>(1,16) - 2*z[4];
    z[18]=z[4]*z[18];
    z[18]=n<T>(23,8) + z[18];
    z[18]=z[4]*z[18];
    z[17]=z[17] + n<T>(1,2) + z[18];
    z[17]=z[2]*z[17];
    z[18]= - n<T>(9,8) - z[28];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(1,16) + z[18];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[17]=z[2]*z[17];
    z[16]=z[17] + z[16];
    z[16]=z[1]*z[16];
    z[17]=static_cast<T>(7)+ z[20];
    z[17]=z[4]*z[17];
    z[17]= - n<T>(11,4) + z[17];
    z[17]=z[4]*z[17];
    z[17]= - n<T>(21,4) + z[17];
    z[17]=z[4]*z[17];
    z[17]= - n<T>(1,2) + z[17];
    z[18]= - static_cast<T>(5)- z[34];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(3,4) + z[18];
    z[18]=z[4]*z[18];
    z[18]=n<T>(3,2) + z[18];
    z[18]=z[4]*z[18];
    z[18]=n<T>(1,4) + z[18];
    z[18]=z[2]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[17]=z[2]*z[17];
    z[18]=n<T>(17,8) + z[36];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(3,8) + z[18];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[17]=z[2]*z[17];
    z[18]=n<T>(1,2) + z[19];
    z[18]=z[4]*z[18];
    z[18]=n<T>(3,2) + z[18];
    z[17]=n<T>(1,8)*z[18] + z[17];
    z[15]=z[15] + n<T>(1,2)*z[17] + z[16];
    z[15]=z[5]*z[15];
    z[16]=z[21]*z[4];
    z[16]= - static_cast<T>(9)+ z[16];
    z[16]=z[16]*z[26];
    z[17]=z[44] + static_cast<T>(3)- z[27];
    z[17]=z[1]*z[17];
    z[16]=z[17] - static_cast<T>(3)+ z[16];
    z[16]=z[7]*z[19]*z[16];
    z[17]= - static_cast<T>(1)+ z[30];
    z[17]=z[1]*z[17]*z[40];
    z[18]=static_cast<T>(1)- n<T>(1,8)*z[27];
    z[18]=z[4]*z[18];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[4]*z[18];
    z[16]=n<T>(1,8)*z[16] + z[18] + z[17];
    z[16]=z[7]*z[16];
    z[17]= - n<T>(7,2) + z[30];
    z[17]=z[4]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[16]=n<T>(1,8)*z[17] + z[16];
    z[16]=z[8]*z[16];
    z[17]=z[32] + z[38];
    z[17]=z[2]*z[17];
    z[18]=z[39] + z[37];
    z[18]=z[2]*z[18];
    z[18]=z[43] + z[18];
    z[18]=z[18]*z[41];
    z[17]=z[18] + z[22] + z[17];
    z[17]=z[7]*z[17];
    z[18]=z[6]*z[45]*z[26];
    z[17]=z[17] + z[18];
    z[17]=z[10]*z[17];

    r += z[12] + z[13] + n<T>(1,8)*z[14] + z[15] + n<T>(1,2)*z[16] + n<T>(1,4)*
      z[17];
 
    return r;
}

template double qg_2lha_r1002(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1002(const std::array<dd_real,30>&);
#endif
