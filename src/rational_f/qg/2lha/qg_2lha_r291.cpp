#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r291(const std::array<T,30>& k) {
  T z[78];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[17];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[20];
    z[8]=k[6];
    z[9]=k[7];
    z[10]=k[4];
    z[11]=k[12];
    z[12]=k[11];
    z[13]=k[24];
    z[14]=k[5];
    z[15]=k[9];
    z[16]=2*z[5];
    z[17]=z[16] - n<T>(19,4);
    z[17]=z[17]*z[5];
    z[17]=z[17] + n<T>(7,2);
    z[18]=n<T>(1,3)*z[5];
    z[17]=z[17]*z[18];
    z[17]=z[17] - n<T>(1,4);
    z[19]=z[6]*z[5];
    z[17]=z[17]*z[19];
    z[20]=n<T>(1,2)*z[5];
    z[21]=n<T>(25,2) + z[5];
    z[21]=z[5]*z[21];
    z[21]= - static_cast<T>(1)+ n<T>(1,6)*z[21];
    z[21]=z[21]*z[20];
    z[22]=z[3]*z[5];
    z[23]=n<T>(1,4)*z[22];
    z[24]=3*z[5];
    z[25]= - n<T>(5,8) - z[24];
    z[25]=z[5]*z[25];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[23];
    z[21]=z[25] + z[21] - z[17];
    z[21]=z[3]*z[21];
    z[25]= - n<T>(13,4) + z[5];
    z[25]=z[25]*z[18];
    z[25]=n<T>(3,4) + z[25];
    z[26]=npow(z[5],2);
    z[25]=z[25]*z[26];
    z[27]=7*z[5];
    z[28]=n<T>(31,3) + z[27];
    z[28]=z[28]*z[26];
    z[29]=z[26]*z[10];
    z[28]=z[28] - n<T>(43,6)*z[29];
    z[30]=n<T>(1,16)*z[10];
    z[28]=z[28]*z[30];
    z[25]=z[25] + z[28];
    z[25]=z[15]*z[25];
    z[28]=11*z[5];
    z[31]= - static_cast<T>(5)+ z[28];
    z[31]=z[31]*z[28];
    z[32]=z[10]*z[5];
    z[31]=z[31] - n<T>(61,2)*z[32];
    z[31]=z[10]*z[31];
    z[33]=static_cast<T>(47)- n<T>(125,2)*z[5];
    z[33]=z[5]*z[33];
    z[33]= - n<T>(25,2) + z[33];
    z[33]=z[5]*z[33];
    z[31]=z[33] + z[31];
    z[17]=z[25] + z[17] + n<T>(1,24)*z[31];
    z[17]=z[15]*z[17];
    z[25]= - n<T>(11,2) + 5*z[5];
    z[25]=z[25]*z[26];
    z[31]=z[26]*z[3];
    z[33]=z[26] - z[31];
    z[34]=n<T>(1,2)*z[3];
    z[33]=z[33]*z[34];
    z[25]= - n<T>(13,2)*z[29] + z[25] + z[33];
    z[33]=static_cast<T>(3)- n<T>(41,12)*z[5];
    z[33]=z[33]*z[26];
    z[35]=npow(z[5],3);
    z[36]=z[35]*z[15];
    z[37]=z[5] - 1;
    z[38]= - 5*z[37] + n<T>(13,2)*z[10];
    z[38]=z[38]*z[36];
    z[29]=n<T>(1,12)*z[38] + z[33] + n<T>(13,8)*z[29];
    z[29]=z[10]*z[29];
    z[33]=z[20] - 1;
    z[38]=z[33]*z[5];
    z[38]=z[38] + n<T>(1,2);
    z[39]=z[38]*z[26];
    z[29]=n<T>(5,2)*z[39] + z[29];
    z[29]=z[15]*z[29];
    z[25]=n<T>(1,4)*z[25] + z[29];
    z[25]=z[12]*z[25];
    z[29]=static_cast<T>(49)- n<T>(163,2)*z[5];
    z[29]=z[5]*z[29];
    z[17]=z[25] + z[17] + n<T>(61,48)*z[32] + n<T>(1,48)*z[29] + z[21];
    z[17]=z[12]*z[17];
    z[21]=z[24] - n<T>(35,3);
    z[21]=z[21]*z[5];
    z[21]=z[21] + n<T>(49,3);
    z[21]=z[21]*z[5];
    z[21]=z[21] - n<T>(29,3);
    z[21]=z[21]*z[20];
    z[21]=z[21] + 1;
    z[25]=z[18] - 1;
    z[25]=z[25]*z[5];
    z[25]=z[25] + 1;
    z[25]=z[25]*z[5];
    z[25]=z[25] - n<T>(1,3);
    z[25]=z[25]*z[19];
    z[21]= - z[25] + n<T>(1,2)*z[21];
    z[21]=z[21]*z[6];
    z[25]= - n<T>(47,2) - 23*z[5];
    z[25]=z[25]*z[18];
    z[25]=n<T>(3,4) + z[25];
    z[25]=n<T>(1,4)*z[25] - z[21];
    z[29]=n<T>(1,4)*z[5];
    z[40]=n<T>(11,6) - z[5];
    z[40]=z[40]*z[29];
    z[40]= - n<T>(1,3) + z[40];
    z[40]=z[40]*z[20];
    z[41]= - n<T>(1,3) + z[29];
    z[41]=z[5]*z[41];
    z[41]= - n<T>(1,12) + z[41];
    z[41]=z[5]*z[41];
    z[41]=n<T>(1,6) + z[41];
    z[41]=z[41]*z[19];
    z[40]=z[40] + z[41];
    z[40]=z[3]*z[40];
    z[25]=n<T>(1,2)*z[25] + z[40];
    z[25]=z[3]*z[25];
    z[40]=z[5] - 3;
    z[41]=z[40]*z[5];
    z[41]=z[41] + 3;
    z[42]=z[41]*z[5];
    z[42]=z[42] - 1;
    z[43]=z[42]*z[19];
    z[44]=z[24] - n<T>(19,2);
    z[44]=z[44]*z[5];
    z[45]= - n<T>(13,6) - z[44];
    z[45]=z[45]*z[20];
    z[45]=z[45] + z[43];
    z[46]=n<T>(1,8)*z[5];
    z[47]= - n<T>(5,3) - z[46];
    z[32]=z[47]*z[32];
    z[32]=n<T>(1,2)*z[45] + z[32];
    z[32]=z[15]*z[32];
    z[45]=n<T>(1,2)*z[10];
    z[47]= - static_cast<T>(9)- n<T>(47,2)*z[5];
    z[47]=n<T>(1,4)*z[47] - z[10];
    z[47]=z[47]*z[45];
    z[48]=n<T>(1,4) + z[5];
    z[48]=z[5]*z[48];
    z[48]= - n<T>(1,4) + z[48];
    z[21]=z[32] + z[47] + n<T>(11,2)*z[48] + z[21];
    z[21]=z[15]*z[21];
    z[32]=n<T>(1,4)*z[10];
    z[47]=static_cast<T>(5)+ n<T>(43,6)*z[5];
    z[17]=z[17] + n<T>(1,2)*z[21] + z[32] + n<T>(1,16)*z[47] + z[25];
    z[17]=z[12]*z[17];
    z[21]=z[6] - n<T>(1,2);
    z[25]=n<T>(1,8)*z[6];
    z[47]= - z[21]*z[25];
    z[48]=n<T>(5,4) + 3*z[6];
    z[48]=z[48]*z[6];
    z[49]=npow(z[6],2);
    z[50]=z[49]*z[10];
    z[48]=z[48] + n<T>(7,4)*z[50];
    z[48]=z[48]*z[45];
    z[51]=static_cast<T>(1)+ n<T>(3,8)*z[6];
    z[51]=z[51]*z[6];
    z[48]=z[48] + z[51];
    z[51]=static_cast<T>(1)+ z[48];
    z[51]=z[51]*z[45];
    z[47]=z[51] + static_cast<T>(1)+ z[47];
    z[47]=z[10]*z[47];
    z[51]=z[45]*z[49];
    z[52]=static_cast<T>(1)+ n<T>(5,2)*z[6];
    z[52]=z[52]*z[6];
    z[52]=z[52] + z[51];
    z[52]=z[52]*z[32];
    z[53]=static_cast<T>(1)+ n<T>(9,8)*z[6];
    z[53]=z[53]*z[6];
    z[52]=z[52] + z[53] + n<T>(1,8);
    z[52]=z[52]*z[10];
    z[53]=static_cast<T>(3)+ n<T>(7,4)*z[6];
    z[53]=z[53]*z[6];
    z[53]=z[53] + n<T>(3,4);
    z[52]=z[52] + n<T>(1,2)*z[53];
    z[52]=z[52]*z[10];
    z[53]=n<T>(1,4)*z[6];
    z[54]=z[53] + 1;
    z[54]=z[54]*z[6];
    z[52]=z[52] + z[54] + n<T>(3,8);
    z[52]=z[52]*z[10];
    z[55]=z[6] + n<T>(1,2);
    z[52]=z[52] + n<T>(1,4)*z[55];
    z[55]=z[55]*z[6];
    z[56]=z[55] + n<T>(1,4)*z[50];
    z[56]=z[56]*z[45];
    z[57]=static_cast<T>(1)+ n<T>(3,4)*z[6];
    z[57]=z[57]*z[6];
    z[56]=z[56] + z[57] + n<T>(1,8);
    z[56]=z[56]*z[10];
    z[58]=z[6] + 3;
    z[58]=z[58]*z[6];
    z[58]=z[58] + 1;
    z[56]=z[56] + n<T>(1,2)*z[58];
    z[56]=z[56]*z[10];
    z[58]=z[25] + 1;
    z[58]=z[58]*z[6];
    z[56]=z[56] + z[58] + n<T>(3,4);
    z[56]=z[56]*z[10];
    z[58]=n<T>(1,2)*z[6];
    z[59]=z[58] + 1;
    z[56]=z[56] + n<T>(1,2)*z[59];
    z[56]=z[56]*z[10];
    z[56]=z[56] + n<T>(1,8);
    z[60]= - z[15]*z[56];
    z[60]=z[60] + z[52];
    z[56]=z[56]*z[13];
    z[60]=n<T>(1,2)*z[60] - z[56];
    z[60]=z[13]*z[60];
    z[61]= - z[34] - 1;
    z[61]=z[3]*z[37]*z[61];
    z[55]= - z[55] - z[51];
    z[55]=z[10]*z[55];
    z[55]= - z[6] + z[55];
    z[55]=z[10]*z[55];
    z[55]=z[49] + z[55];
    z[55]=z[10]*z[55];
    z[62]=z[6]*z[59];
    z[55]=z[62] + z[55];
    z[55]=z[10]*z[55];
    z[55]=z[58] + z[55];
    z[55]=z[15]*z[55];
    z[47]=z[60] + n<T>(1,4)*z[55] + z[47] + z[61] - z[25] - z[33];
    z[47]=z[15]*z[47];
    z[55]=z[3] + 1;
    z[60]=z[55]*z[37];
    z[61]=z[3]*z[60];
    z[61]= - z[10] - static_cast<T>(1)+ z[61];
    z[47]=n<T>(1,2)*z[61] + z[47];
    z[47]=z[13]*z[47];
    z[61]=z[5] + 1;
    z[62]= - z[61]*z[26];
    z[63]=z[35]*z[3];
    z[62]=z[62] + z[63];
    z[62]=z[3]*z[62];
    z[64]=z[37]*z[36];
    z[65]=z[3] - 1;
    z[65]=z[3]*z[65];
    z[65]=z[65] + z[15];
    z[65]=z[12]*z[65]*npow(z[5],4);
    z[66]=static_cast<T>(1)+ z[26];
    z[66]=z[5]*z[66];
    z[62]=z[65] + z[64] + z[66] + z[62];
    z[64]=z[34]*z[26];
    z[66]=static_cast<T>(1)- z[26];
    z[66]=z[66]*z[64];
    z[61]= - z[5]*z[61];
    z[61]= - static_cast<T>(1)+ z[61];
    z[61]=z[5]*z[61];
    z[61]=z[61] + z[66];
    z[61]=z[3]*z[61];
    z[66]=npow(z[3],2);
    z[67]=z[66]*z[1];
    z[68]=static_cast<T>(1)+ z[20];
    z[68]=z[5]*z[68];
    z[68]=n<T>(1,2) + z[68];
    z[68]=z[5]*z[68]*z[67];
    z[61]=z[61] + z[68];
    z[61]=z[1]*z[61];
    z[61]=n<T>(1,2)*z[62] + z[61];
    z[61]=z[7]*z[61];
    z[62]=z[24] - 19;
    z[68]= - z[62]*z[20];
    z[68]=static_cast<T>(5)+ z[68];
    z[68]=z[5]*z[68];
    z[69]=n<T>(3,2)*z[5];
    z[70]=z[69] - 5;
    z[71]=z[70]*z[31];
    z[68]=z[68] + z[71];
    z[71]=n<T>(1,4)*z[3];
    z[68]=z[68]*z[71];
    z[69]=z[69] - 11;
    z[70]=z[3]*z[70];
    z[70]= - z[69] + z[70];
    z[63]=z[70]*z[63];
    z[36]=z[69]*z[36];
    z[36]=3*z[65] + z[63] + z[36];
    z[63]=n<T>(1,4)*z[12];
    z[36]=z[36]*z[63];
    z[65]=z[69]*z[5];
    z[69]=n<T>(3,2) - z[65];
    z[69]=z[5]*z[69];
    z[69]= - static_cast<T>(5)+ z[69];
    z[23]=z[69]*z[23];
    z[69]=n<T>(3,4)*z[5];
    z[70]=z[69] - 4;
    z[70]=z[70]*z[5];
    z[72]=static_cast<T>(4)- z[70];
    z[72]=z[5]*z[72];
    z[23]=z[23] + n<T>(5,2) + z[72];
    z[23]=z[3]*z[23];
    z[72]= - static_cast<T>(2)+ n<T>(3,8)*z[5];
    z[72]=z[72]*z[5];
    z[73]= - n<T>(29,8) + z[72];
    z[73]=z[5]*z[73];
    z[73]= - n<T>(5,4) + z[73];
    z[73]=z[73]*z[67];
    z[23]=z[23] + z[73];
    z[23]=z[1]*z[23];
    z[46]=z[62]*z[46];
    z[46]=static_cast<T>(2)+ z[46];
    z[46]=z[15]*z[46]*z[26];
    z[73]= - n<T>(3,8) + z[72];
    z[73]=z[5]*z[73];
    z[23]=n<T>(3,2)*z[61] + z[23] + z[36] + z[46] + z[68] - n<T>(5,4) + z[73];
    z[23]=z[7]*z[23];
    z[36]= - static_cast<T>(1)+ n<T>(5,2)*z[5];
    z[36]=z[36]*z[22];
    z[46]= - static_cast<T>(1)+ n<T>(5,12)*z[5];
    z[46]=z[5]*z[46];
    z[36]=z[46] + z[36];
    z[36]=z[3]*z[36];
    z[46]=z[26] - z[64];
    z[46]=z[3]*z[46];
    z[46]= - n<T>(13,2)*z[26] + z[46];
    z[46]=z[12]*z[46];
    z[36]=z[46] + n<T>(61,12)*z[5] + z[36];
    z[36]=z[12]*z[36];
    z[46]=static_cast<T>(3)+ n<T>(59,3)*z[5];
    z[46]=n<T>(1,8)*z[46] + n<T>(1,3)*z[22];
    z[46]=z[3]*z[46];
    z[36]=z[36] + static_cast<T>(1)+ z[46];
    z[36]=z[12]*z[36];
    z[46]=z[12]*z[3];
    z[61]=z[55]*z[46];
    z[68]= - 3*z[66] - z[61];
    z[68]=z[12]*z[68];
    z[73]=npow(z[12],2);
    z[74]=z[73]*z[66];
    z[75]=z[2]*z[74];
    z[68]=z[68] + z[75];
    z[68]=z[2]*z[68];
    z[75]=static_cast<T>(1)+ n<T>(3,2)*z[3];
    z[75]=z[75]*z[46];
    z[68]=n<T>(1,2)*z[68] + z[66] + z[75];
    z[68]=z[2]*z[68];
    z[75]=n<T>(1,2)*z[67];
    z[68]=z[68] - z[66] - z[75];
    z[68]=z[2]*z[68];
    z[76]= - z[3] + z[67];
    z[68]=n<T>(1,2)*z[76] + z[68];
    z[68]=z[9]*z[68];
    z[76]=static_cast<T>(1)+ n<T>(7,3)*z[5];
    z[76]=z[3]*z[76];
    z[76]=n<T>(73,24) + z[76];
    z[76]=z[3]*z[76];
    z[77]= - static_cast<T>(1)- z[66];
    z[77]=z[13]*z[77];
    z[36]=z[68] + z[77] + z[76] + z[36];
    z[16]=n<T>(13,8) - z[16];
    z[16]=z[3]*z[16];
    z[16]=n<T>(13,8) + z[16];
    z[16]=z[16]*z[46];
    z[16]=n<T>(97,32)*z[66] + z[16];
    z[16]=z[16]*z[73];
    z[61]=z[66] + z[61];
    z[61]=z[13]*z[61]*z[63];
    z[68]= - z[13]*z[74];
    z[73]=z[66]*npow(z[12],3);
    z[68]= - n<T>(13,6)*z[73] + z[68];
    z[68]=z[2]*z[68];
    z[16]=n<T>(1,4)*z[68] + n<T>(1,3)*z[16] + z[61];
    z[16]=z[2]*z[16];
    z[61]= - n<T>(15,2) + n<T>(13,3)*z[5];
    z[61]=z[3]*z[61];
    z[61]= - n<T>(31,3) + z[61];
    z[61]=z[61]*z[34];
    z[68]=z[27] - z[64];
    z[68]=z[68]*z[46];
    z[61]=z[61] + z[68];
    z[61]=z[12]*z[61];
    z[61]= - n<T>(13,4)*z[66] + z[61];
    z[61]=z[12]*z[61];
    z[68]=static_cast<T>(1)- 3*z[3];
    z[68]=z[13]*z[68]*z[46];
    z[61]=z[61] + z[68];
    z[16]=n<T>(1,4)*z[61] + z[16];
    z[16]=z[2]*z[16];
    z[61]= - static_cast<T>(1)- n<T>(3,32)*z[5];
    z[61]=z[61]*z[22];
    z[61]= - n<T>(17,16)*z[5] + z[61];
    z[61]=z[3]*z[61];
    z[68]=3*z[26] - z[31];
    z[46]=z[68]*z[46];
    z[46]=z[61] + n<T>(1,8)*z[46];
    z[46]=z[12]*z[46];
    z[61]=n<T>(31,2) + z[5];
    z[61]=z[3]*z[61];
    z[61]=n<T>(29,4) + n<T>(1,3)*z[61];
    z[61]=z[3]*z[61];
    z[46]=n<T>(1,8)*z[61] + z[46];
    z[46]=z[12]*z[46];
    z[61]= - n<T>(1,2) + z[3];
    z[61]=z[13]*z[61]*z[34];
    z[16]=z[16] + z[61] + n<T>(11,32)*z[66] + z[46];
    z[16]=z[2]*z[16];
    z[16]=z[16] - n<T>(5,3)*z[67] + n<T>(1,4)*z[36];
    z[16]=z[2]*z[16];
    z[21]=z[21]*z[53];
    z[36]= - z[10]*z[48];
    z[21]=z[21] + z[36];
    z[21]=z[10]*z[21];
    z[21]=z[53] + z[21];
    z[36]= - n<T>(1,2)*z[52] + z[56];
    z[36]=z[13]*z[36];
    z[21]=n<T>(1,2)*z[21] + z[36];
    z[21]=z[13]*z[21];
    z[36]= - n<T>(3,2) - z[6];
    z[36]=z[36]*z[58];
    z[46]= - n<T>(1,4) - z[6];
    z[46]=z[6]*z[46];
    z[46]=z[46] - z[51];
    z[46]=z[10]*z[46];
    z[36]=z[36] + z[46];
    z[36]=z[10]*z[36];
    z[36]= - z[53] + z[36];
    z[36]=z[10]*z[36];
    z[46]= - z[20] - z[31];
    z[46]=z[3]*z[46];
    z[48]=z[66]*z[26];
    z[52]=n<T>(1,2)*z[12];
    z[61]= - z[48]*z[52];
    z[66]= - z[10]*z[58];
    z[46]=z[61] + z[66] - n<T>(1,2) + z[46];
    z[46]=z[46]*z[52];
    z[36]=z[46] + n<T>(3,4) + z[36];
    z[46]=static_cast<T>(1)+ n<T>(9,4)*z[6];
    z[46]=z[6]*z[46];
    z[46]=z[46] + n<T>(3,4)*z[50];
    z[46]=z[10]*z[46];
    z[46]=z[46] + n<T>(1,4) + 3*z[57];
    z[46]=z[10]*z[46];
    z[52]=n<T>(1,4) + z[54];
    z[46]=3*z[52] + z[46];
    z[46]=z[10]*z[46];
    z[46]=z[46] + n<T>(3,4) + z[6];
    z[46]=z[10]*z[46];
    z[46]=n<T>(1,4) + z[46];
    z[46]=n<T>(1,2)*z[46] - z[56];
    z[46]=z[13]*z[46];
    z[36]=n<T>(1,2)*z[36] + z[46];
    z[46]=n<T>(1,2)*z[11];
    z[36]=z[36]*z[46];
    z[52]=static_cast<T>(1)+ z[69];
    z[52]=z[3]*z[52]*z[20];
    z[52]=z[5] + z[52];
    z[52]=z[3]*z[52];
    z[48]=z[63]*z[48];
    z[48]=z[52] + z[48];
    z[48]=z[12]*z[48];
    z[52]=static_cast<T>(5)+ z[27];
    z[31]=n<T>(1,8)*z[52] + z[31];
    z[31]=z[3]*z[31];
    z[31]=n<T>(5,4) + z[31];
    z[31]=n<T>(1,2)*z[31] + z[48];
    z[31]=z[12]*z[31];
    z[48]=z[50] + z[49];
    z[48]=z[48]*z[10];
    z[52]=z[58] + z[48];
    z[52]=z[10]*z[52];
    z[52]= - n<T>(1,2) + z[52];
    z[21]=z[36] + z[21] + n<T>(1,4)*z[52] + z[31];
    z[21]=z[21]*z[46];
    z[31]=z[62]*z[29];
    z[31]=static_cast<T>(9)+ z[31];
    z[31]=z[5]*z[31];
    z[31]= - n<T>(53,8) + z[31];
    z[31]=z[5]*z[31];
    z[36]=n<T>(79,6) - z[24];
    z[36]=z[5]*z[36];
    z[36]= - n<T>(43,2) + z[36];
    z[36]=z[5]*z[36];
    z[36]=n<T>(31,2) + z[36];
    z[36]=z[5]*z[36];
    z[36]= - n<T>(25,6) + z[36];
    z[36]=z[36]*z[53];
    z[31]=z[36] + n<T>(3,2) + z[31];
    z[31]=z[6]*z[31];
    z[36]= - z[6]*z[41];
    z[36]=z[36] - z[33];
    z[36]=z[36]*z[58];
    z[40]=z[40]*z[49];
    z[40]= - n<T>(1,4)*z[40] + z[50];
    z[40]=z[10]*z[40];
    z[36]=z[36] + z[40];
    z[36]=z[36]*z[32];
    z[40]= - z[60]*z[34];
    z[31]=z[36] + z[40] - n<T>(23,24) + z[31];
    z[36]=z[29] - 1;
    z[40]=z[36]*z[5];
    z[40]=z[40] + n<T>(3,2);
    z[41]=npow(z[10],2);
    z[41]=z[40] - n<T>(1,4)*z[41];
    z[41]=z[41]*z[51];
    z[33]= - z[33]*z[20];
    z[40]= - z[5]*z[40];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[6]*z[40];
    z[33]=z[33] + z[40];
    z[33]=z[6]*z[33];
    z[33]=z[33] + z[41];
    z[32]=z[33]*z[32];
    z[18]= - z[36]*z[18];
    z[18]= - n<T>(1,2) + z[18];
    z[18]=z[5]*z[18];
    z[18]=n<T>(1,3) + z[18];
    z[18]=z[5]*z[18];
    z[18]= - n<T>(1,12) + z[18];
    z[18]=z[6]*z[18];
    z[33]= - n<T>(21,2) - z[44];
    z[33]=z[5]*z[33];
    z[33]=n<T>(9,2) + z[33];
    z[18]=n<T>(1,8)*z[33] + z[18];
    z[18]=z[6]*z[18];
    z[18]=z[32] - n<T>(23,48) + z[18];
    z[18]=z[15]*z[18];
    z[18]=n<T>(1,2)*z[31] + z[18];
    z[18]=z[15]*z[18];
    z[31]= - z[49] - n<T>(3,2)*z[50];
    z[31]=z[10]*z[31];
    z[32]= - z[6] + z[48];
    z[32]=z[11]*z[32]*z[45];
    z[31]=z[32] + z[58] + z[31];
    z[31]=z[11]*z[31];
    z[31]=z[50] + z[31];
    z[32]= - n<T>(17,4) + z[24];
    z[32]=z[5]*z[32];
    z[32]=n<T>(5,4) + z[32];
    z[32]=z[32]*z[26]*z[58];
    z[33]= - n<T>(13,4) - z[65];
    z[33]=z[5]*z[33];
    z[33]= - n<T>(11,24) + z[33];
    z[33]=z[5]*z[33];
    z[32]=z[33] + z[32];
    z[32]=z[6]*z[32];
    z[33]=n<T>(67,3) + n<T>(31,2)*z[5];
    z[32]=n<T>(1,4)*z[33] + z[32];
    z[32]=z[32]*z[71];
    z[33]= - n<T>(41,6) + z[24];
    z[33]=z[5]*z[33];
    z[33]=n<T>(23,6) + z[33];
    z[33]=z[33]*z[19];
    z[36]= - n<T>(8,3) - z[70];
    z[36]=z[5]*z[36];
    z[33]=n<T>(1,4)*z[33] + n<T>(3,8) + z[36];
    z[33]=z[6]*z[33];
    z[32]=z[32] - n<T>(41,24) + z[33];
    z[32]=z[3]*z[32];
    z[25]=z[25]*z[26];
    z[33]=n<T>(7,3) - z[24];
    z[33]=z[33]*z[25];
    z[36]= - n<T>(1,12) + z[72];
    z[36]=z[5]*z[36];
    z[33]=z[33] - n<T>(2,3) + z[36];
    z[33]=z[6]*z[33];
    z[33]=n<T>(7,8) + z[33];
    z[33]=z[33]*z[67];
    z[36]= - z[6]*z[37];
    z[36]= - static_cast<T>(1)+ z[36];
    z[36]=z[6]*z[36];
    z[31]=z[33] + n<T>(1,32)*z[36] + z[32] + n<T>(1,16)*z[31];
    z[31]=z[1]*z[31];
    z[32]= - n<T>(13,2) + z[24];
    z[32]=z[32]*z[29];
    z[32]=static_cast<T>(1)+ z[32];
    z[32]=z[5]*z[32];
    z[32]= - n<T>(1,8) + z[32];
    z[32]=z[5]*z[32];
    z[25]=z[42]*z[25];
    z[25]=z[32] + z[25];
    z[25]=z[6]*z[25];
    z[25]=n<T>(5,8)*z[35] + z[25];
    z[25]=z[3]*z[25];
    z[28]= - static_cast<T>(23)+ z[28];
    z[28]=z[5]*z[28];
    z[28]=static_cast<T>(13)+ z[28];
    z[28]=z[5]*z[28];
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=n<T>(1,2)*z[28] + z[43];
    z[28]=z[6]*z[28];
    z[26]=n<T>(5,2)*z[26] + z[28];
    z[25]=n<T>(1,4)*z[26] + z[25];
    z[25]=z[3]*z[25];
    z[26]=n<T>(1,8) - z[5];
    z[22]=z[26]*z[22];
    z[26]=static_cast<T>(1)- 13*z[5];
    z[22]=n<T>(1,8)*z[26] + z[22];
    z[22]=z[3]*z[22];
    z[26]=z[5] + z[64];
    z[26]=z[3]*z[26];
    z[26]=n<T>(1,2) + z[26];
    z[26]=z[26]*z[46];
    z[22]=z[26] - n<T>(5,8) + z[22];
    z[22]=z[11]*z[22];
    z[26]=z[42]*z[58];
    z[26]=5*z[38] + z[26];
    z[26]=z[26]*z[53];
    z[22]=z[22] + z[26] + z[25];
    z[22]=z[14]*z[22];
    z[25]=n<T>(31,3) - z[27];
    z[25]=z[25]*z[20];
    z[25]= - n<T>(5,3) + z[25];
    z[20]=z[25]*z[20];
    z[25]= - z[6]*z[39];
    z[20]=z[20] + z[25];
    z[20]=z[20]*z[58];
    z[25]=n<T>(11,12) - z[5];
    z[25]=z[5]*z[25];
    z[20]=z[25] + z[20];
    z[20]=z[3]*z[20];
    z[19]=z[38]*z[19];
    z[25]=n<T>(53,12) - z[24];
    z[25]=z[5]*z[25];
    z[25]= - n<T>(13,6) + z[25];
    z[25]=z[5]*z[25];
    z[25]=n<T>(3,4) + z[25];
    z[19]=n<T>(1,2)*z[25] + n<T>(1,3)*z[19];
    z[19]=z[6]*z[19];
    z[25]= - static_cast<T>(47)- 143*z[5];
    z[19]=n<T>(1,24)*z[25] + z[19];
    z[19]=n<T>(1,4)*z[19] + z[20];
    z[19]=z[3]*z[19];
    z[20]= - z[55]*z[34];
    z[25]=n<T>(1,2) + z[3];
    z[25]=z[3]*z[25];
    z[25]=z[25] - z[75];
    z[25]=z[1]*z[25];
    z[20]=z[20] + z[25];
    z[20]=z[2]*z[20];
    z[25]=z[59]*z[67];
    z[26]= - n<T>(3,2) - z[3];
    z[26]=z[3]*z[26];
    z[25]=z[26] + z[25];
    z[25]=z[1]*z[25];
    z[20]=z[25] + z[20];
    z[20]=z[4]*z[20];
    z[24]=n<T>(71,6) - z[24];
    z[24]=z[24]*z[29];
    z[24]= - n<T>(11,3) + z[24];
    z[24]=z[5]*z[24];
    z[24]=n<T>(35,24) + z[24];
    z[24]=z[24]*z[58];
    z[25]=n<T>(57,16) + z[72];
    z[25]=z[5]*z[25];
    z[24]=z[24] - n<T>(63,32) + z[25];
    z[24]=z[6]*z[24];
    z[25]=z[58] - z[50];
    z[25]=z[25]*z[30];
    z[26]=z[58]*z[67];
    z[26]= - z[3] + z[26];
    z[26]=z[1]*z[26];
    z[26]= - n<T>(1,2)*z[37] + z[26];
    z[26]=z[8]*z[26];

    r += n<T>(13,6) + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + n<T>(1,4)
      *z[22] + z[23] + z[24] + z[25] + n<T>(1,3)*z[26] + z[31] + n<T>(1,2)*
      z[47];
 
    return r;
}

template double qg_2lha_r291(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r291(const std::array<dd_real,30>&);
#endif
