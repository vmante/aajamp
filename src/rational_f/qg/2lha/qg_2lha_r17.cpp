#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r17(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[24];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=n<T>(1,2)*z[3];
    z[7]=z[4]*z[3];
    z[8]=2*z[7] + static_cast<T>(1)+ z[6];
    z[9]=n<T>(1,3)*z[1];
    z[10]=n<T>(1,3)*z[4];
    z[11]=z[9] - z[10];
    z[12]=npow(z[3],2);
    z[11]=z[12]*z[11];
    z[13]= - static_cast<T>(1)- n<T>(1,3)*z[3];
    z[13]=z[3]*z[13];
    z[11]=z[13] + z[11];
    z[13]=n<T>(1,2)*z[1];
    z[11]=z[11]*z[13];
    z[8]=n<T>(1,3)*z[8] + z[11];
    z[8]=z[1]*z[8];
    z[11]= - n<T>(1,2) - z[10];
    z[11]=z[5]*z[11];
    z[11]=n<T>(7,6) + z[11];
    z[11]=z[5]*z[11];
    z[8]=z[8] - n<T>(2,3) + z[11];
    z[8]=z[1]*z[8];
    z[11]=z[4] + 1;
    z[14]= - static_cast<T>(2)- z[4];
    z[14]=z[4]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[5]*z[14];
    z[14]=2*z[11] + z[14];
    z[14]=z[5]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[15]= - z[1] + 1;
    z[15]=z[3]*z[15];
    z[15]=z[7] + static_cast<T>(1)+ z[15];
    z[9]=z[15]*z[9];
    z[15]=static_cast<T>(1)- n<T>(1,3)*z[5];
    z[15]=z[5]*z[15];
    z[9]=z[9] - static_cast<T>(1)+ z[15];
    z[9]=z[1]*z[9];
    z[11]=z[11]*z[5];
    z[15]=n<T>(2,3)*z[11] - n<T>(5,3) - z[4];
    z[15]=z[5]*z[15];
    z[9]=z[9] + static_cast<T>(1)+ z[15];
    z[9]=z[1]*z[9];
    z[9]=n<T>(1,3)*z[14] + z[9];
    z[9]=z[2]*z[9];
    z[14]=n<T>(5,2) + z[4];
    z[10]=z[14]*z[10];
    z[10]=n<T>(1,2) + z[10];
    z[10]=z[5]*z[10];
    z[14]= - n<T>(5,2) - 2*z[4];
    z[10]=n<T>(1,3)*z[14] + z[10];
    z[10]=z[5]*z[10];
    z[8]=z[9] + z[8] + n<T>(1,3) + z[10];
    z[8]=z[2]*z[8];
    z[9]=z[13] - z[4];
    z[9]=z[12]*z[9];
    z[6]= - z[6] + z[9];
    z[6]=z[1]*z[6];
    z[6]=z[7] + z[6];
    z[6]=z[1]*z[6];
    z[7]=static_cast<T>(1)- z[11];
    z[7]=z[5]*z[7];
    z[6]=n<T>(1,2)*z[7] + z[6];
    z[6]=n<T>(1,3)*z[6] + z[8];
    z[6]=z[2]*z[6];
    z[7]=z[1]*z[4]*z[12];
    z[6]= - n<T>(1,6)*z[7] + z[6];

    r += n<T>(5,3)*z[6];
 
    return r;
}

template double qg_2lha_r17(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r17(const std::array<dd_real,30>&);
#endif
