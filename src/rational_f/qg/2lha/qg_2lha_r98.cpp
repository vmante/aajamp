#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r98(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[11];
    z[6]=k[3];
    z[7]=k[12];
    z[8]=k[4];
    z[9]=k[9];
    z[10]=z[6] + 1;
    z[11]=z[10]*z[6];
    z[12]=z[7] - 1;
    z[13]= - z[12]*z[11];
    z[14]=z[2]*z[3];
    z[15]=z[14] - z[3];
    z[16]= - static_cast<T>(1)- z[15];
    z[16]=z[2]*z[16];
    z[10]=z[16] + z[10];
    z[10]=z[2]*z[10];
    z[10]=z[11] + z[10];
    z[10]=z[2]*z[10];
    z[10]=z[13] + z[10];
    z[11]=n<T>(1,2)*z[5];
    z[10]=z[10]*z[11];
    z[13]=n<T>(1,2)*z[6];
    z[12]=z[12]*z[13];
    z[15]=static_cast<T>(1)+ n<T>(3,2)*z[15];
    z[15]=z[2]*z[15];
    z[15]=z[15] - static_cast<T>(1)- z[13];
    z[15]=z[2]*z[15];
    z[10]=z[10] + z[12] + z[15];
    z[10]=z[5]*z[10];
    z[12]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[12]=z[3]*z[12];
    z[12]= - z[14] - n<T>(1,2) + z[12];
    z[12]=z[2]*z[12];
    z[15]= - z[3]*z[1];
    z[15]=static_cast<T>(1)+ z[15];
    z[10]=z[10] + n<T>(1,2)*z[15] + z[12];
    z[10]=z[4]*z[10];
    z[12]= - z[9] + n<T>(1,2)*z[7];
    z[13]= - z[7]*z[13];
    z[13]=z[13] - n<T>(1,2) - z[12];
    z[15]= - z[7] - z[6];
    z[15]=z[6]*z[15];
    z[16]=static_cast<T>(3)+ z[14];
    z[16]=z[2]*z[16];
    z[16]=z[6] + z[16];
    z[16]=z[2]*z[16];
    z[15]=z[15] + z[16];
    z[15]=z[5]*z[15];
    z[14]= - static_cast<T>(1)- n<T>(1,2)*z[14];
    z[14]=z[2]*z[14];
    z[13]=n<T>(1,4)*z[15] + n<T>(1,2)*z[13] + z[14];
    z[13]=z[5]*z[13];
    z[14]=z[3] + static_cast<T>(1)+ z[7];
    z[10]=n<T>(1,2)*z[10] + n<T>(1,4)*z[14] + z[13];
    z[10]=z[4]*z[10];
    z[13]=static_cast<T>(1)+ z[8];
    z[13]=z[9]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[5]*z[6]*z[13];
    z[12]=z[13] - z[12];
    z[11]=z[12]*z[11];
    z[10]=z[11] + z[10];

    r += n<T>(1,4)*z[10];
 
    return r;
}

template double qg_2lha_r98(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r98(const std::array<dd_real,30>&);
#endif
