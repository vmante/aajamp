#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1541(const std::array<T,30>& k) {
  T z[42];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[4];
    z[5]=k[11];
    z[6]=k[3];
    z[7]=k[12];
    z[8]=9*z[5];
    z[9]=z[8] + 1;
    z[9]=z[9]*z[5];
    z[10]=9*z[7];
    z[9]=z[9] - z[10];
    z[11]=npow(z[5],2);
    z[12]=z[11]*z[6];
    z[13]=27*z[12];
    z[14]=z[11]*z[4];
    z[15]= - n<T>(9,2)*z[14] + z[13] - z[9];
    z[15]=z[4]*z[15];
    z[8]=z[8] + n<T>(1,2);
    z[8]=z[8]*z[5];
    z[16]=n<T>(9,2)*z[7];
    z[8]=z[8] - z[16];
    z[17]= - n<T>(27,4)*z[12] + z[8];
    z[18]=n<T>(1,2)*z[6];
    z[17]=z[17]*z[18];
    z[19]=n<T>(5,2)*z[2];
    z[20]=z[1] - 1;
    z[21]=z[20]*z[2];
    z[22]= - n<T>(3,2) - z[21];
    z[22]=z[22]*z[19];
    z[23]= - static_cast<T>(1)+ n<T>(9,8)*z[5];
    z[23]=z[23]*z[5];
    z[24]=n<T>(9,4)*z[7];
    z[15]=n<T>(1,4)*z[15] + z[22] + z[17] + z[24] - z[23];
    z[15]=z[4]*z[15];
    z[17]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[17]=z[17]*z[1];
    z[17]=z[17] + n<T>(1,2);
    z[22]=z[17]*z[2];
    z[25]=n<T>(3,4)*z[20] + z[22];
    z[26]=5*z[2];
    z[25]=z[25]*z[26];
    z[27]=z[6]*z[7];
    z[28]=static_cast<T>(11)- 3*z[27];
    z[25]=n<T>(1,4)*z[28] + z[25];
    z[15]=3*z[25] + z[15];
    z[15]=z[4]*z[15];
    z[25]=9*z[12];
    z[28]=z[25] - z[9];
    z[28]=z[28]*z[18];
    z[28]=z[28] - n<T>(27,2)*z[7] - 11*z[5];
    z[28]=z[6]*z[28];
    z[29]= - static_cast<T>(3)+ z[2];
    z[29]=z[29]*z[26];
    z[30]=n<T>(27,2)*z[12];
    z[31]=z[10] - z[5];
    z[32]= - z[30] - z[31];
    z[32]=z[4]*z[32]*z[18];
    z[28]=z[32] + z[29] + static_cast<T>(15)+ z[28];
    z[28]=z[4]*z[28];
    z[29]=3*z[7] + z[5];
    z[29]=z[6]*z[29];
    z[29]=z[29] - z[5];
    z[29]= - static_cast<T>(5)- z[16] + n<T>(3,2)*z[29];
    z[29]=z[6]*z[29];
    z[29]=n<T>(35,2) + z[29];
    z[32]=7*z[1];
    z[33]= - n<T>(23,2) + z[32];
    z[33]=n<T>(1,2)*z[33] - 2*z[21];
    z[33]=z[33]*z[26];
    z[28]=n<T>(1,2)*z[28] + z[33] + n<T>(3,2)*z[29] - 5*z[1];
    z[28]=z[4]*z[28];
    z[29]=3*z[6];
    z[33]=n<T>(9,2)*z[27];
    z[34]=z[33] - static_cast<T>(11)- n<T>(3,2)*z[7];
    z[34]=z[34]*z[29];
    z[34]= - 35*z[1] + n<T>(133,2) + z[34];
    z[35]=z[17]*z[26];
    z[36]=2*z[1];
    z[37]=n<T>(35,4) - z[36];
    z[37]=z[1]*z[37];
    z[35]=z[35] - n<T>(27,4) + z[37];
    z[35]=z[35]*z[26];
    z[28]=z[28] + n<T>(1,2)*z[34] + z[35];
    z[28]=z[4]*z[28];
    z[34]=static_cast<T>(1)- n<T>(1,4)*z[2];
    z[34]=z[34]*z[26];
    z[25]= - z[25] - z[31];
    z[35]=n<T>(1,8)*z[4];
    z[25]=z[25]*npow(z[6],2)*z[35];
    z[37]=z[5] + z[7];
    z[38]= - z[6]*z[37];
    z[38]=static_cast<T>(5)+ n<T>(9,2)*z[38];
    z[38]=z[6]*z[38];
    z[25]=z[25] + z[34] - n<T>(15,2) + z[38];
    z[25]=z[4]*z[25];
    z[33]=static_cast<T>(5)- z[33];
    z[29]=z[33]*z[29];
    z[29]=15*z[1] - static_cast<T>(35)+ z[29];
    z[33]=n<T>(3,4)*z[21] + n<T>(11,4) - z[36];
    z[33]=z[33]*z[26];
    z[25]=z[25] + n<T>(1,2)*z[29] + z[33];
    z[25]=z[4]*z[25];
    z[29]= - n<T>(7,2) + z[1];
    z[29]=z[1]*z[29];
    z[29]= - n<T>(3,2)*z[22] + n<T>(5,2) + z[29];
    z[29]=z[29]*z[26];
    z[33]=z[10]*z[6];
    z[34]=static_cast<T>(19)- z[33];
    z[34]=z[6]*z[34];
    z[34]= - n<T>(119,4) + z[34];
    z[25]=z[25] + z[29] + n<T>(1,2)*z[34] + 10*z[1];
    z[25]=z[4]*z[25];
    z[29]=z[1] - 3;
    z[29]=z[29]*z[1];
    z[29]=z[29] + 3;
    z[29]=z[29]*z[1];
    z[29]=z[29] - 1;
    z[34]=n<T>(1,2)*z[2];
    z[36]=z[29]*z[34];
    z[38]=3*z[17];
    z[39]=z[38] + z[36];
    z[39]=z[39]*z[26];
    z[40]= - static_cast<T>(7)+ n<T>(9,4)*z[27];
    z[41]= - z[6]*z[40];
    z[39]=z[39] + n<T>(39,4)*z[1] - n<T>(39,4) + z[41];
    z[25]=n<T>(1,2)*z[39] + z[25];
    z[25]=z[3]*z[25];
    z[33]= - static_cast<T>(37)+ z[33];
    z[33]=z[33]*z[18];
    z[33]= - 29*z[1] + static_cast<T>(29)+ z[33];
    z[29]=z[29]*z[2];
    z[39]= - 5*z[17] - z[29];
    z[39]=z[39]*z[26];
    z[25]=z[25] + z[28] + n<T>(1,2)*z[33] + z[39];
    z[25]=z[3]*z[25];
    z[9]=z[30] - z[9];
    z[9]=z[9]*z[18];
    z[18]=static_cast<T>(1)- z[34];
    z[18]=z[18]*z[19];
    z[13]= - z[13] - z[31];
    z[13]=z[13]*z[35];
    z[9]=z[13] + z[18] + z[9] - z[24] - z[5];
    z[9]=z[4]*z[9];
    z[8]= - n<T>(9,2)*z[12] + z[8];
    z[8]=z[6]*z[8];
    z[13]=static_cast<T>(13)- n<T>(9,2)*z[5];
    z[13]=z[5]*z[13];
    z[8]=z[13] + z[8];
    z[8]=z[10] + n<T>(1,4)*z[8];
    z[8]=z[6]*z[8];
    z[10]=n<T>(5,2) - z[1];
    z[10]=3*z[10] + n<T>(7,2)*z[21];
    z[10]=z[10]*z[19];
    z[13]= - static_cast<T>(5)- n<T>(1,2)*z[37];
    z[8]=z[9] + z[10] + n<T>(9,4)*z[13] + z[8];
    z[8]=z[4]*z[8];
    z[9]=z[16] - z[40];
    z[9]=z[6]*z[9];
    z[10]= - n<T>(29,4) + z[1];
    z[10]=z[1]*z[10];
    z[13]=z[1] - 2;
    z[13]=z[13]*z[1];
    z[13]=z[13] + 1;
    z[16]=z[2]*z[13];
    z[10]= - 3*z[16] + n<T>(25,4) + z[10];
    z[10]=z[10]*z[26];
    z[8]=z[8] + z[10] + n<T>(15,2)*z[1] - n<T>(51,2) + z[9];
    z[8]=z[4]*z[8];
    z[9]=z[36] + z[13];
    z[9]=z[2]*z[9];
    z[10]=static_cast<T>(8)- n<T>(9,8)*z[27];
    z[10]=z[6]*z[10];
    z[8]=z[25] + z[8] + 15*z[9] + n<T>(31,2)*z[1] - n<T>(31,2) + z[10];
    z[8]=z[3]*z[8];
    z[9]= - z[38] - z[29];
    z[9]=z[9]*z[26];
    z[8]=z[8] + z[15] + z[9] - z[32] + static_cast<T>(7)- n<T>(9,4)*z[6];
    z[8]=z[3]*z[8];
    z[9]=z[1]*z[11];
    z[9]= - z[7] + z[14] + z[9];
    z[9]= - n<T>(9,4)*z[12] + z[23] + n<T>(9,8)*z[9];
    z[9]=z[4]*z[9];
    z[10]= - n<T>(1,2)*z[20] - z[22];
    z[10]=z[10]*z[26];
    z[10]= - n<T>(9,4) + z[10];
    z[9]=n<T>(1,2)*z[10] + z[9];
    z[9]=z[4]*z[9];
    z[10]=z[36] + z[17];
    z[10]=z[10]*z[26];
    z[10]=n<T>(9,4)*z[20] + z[10];

    r += z[8] + z[9] + n<T>(1,2)*z[10];
 
    return r;
}

template double qg_2lha_r1541(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1541(const std::array<dd_real,30>&);
#endif
