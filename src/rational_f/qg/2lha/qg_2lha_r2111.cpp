#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2111(const std::array<T,30>& k) {
  T z[40];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[3];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[15];
    z[10]=k[12];
    z[11]=z[7]*z[10];
    z[12]=5*z[10];
    z[13]=static_cast<T>(4)- z[12];
    z[13]=z[13]*z[11];
    z[14]=npow(z[10],2);
    z[13]= - 6*z[14] + z[13];
    z[13]=z[7]*z[13];
    z[15]=z[3]*z[7];
    z[16]=npow(z[7],2);
    z[17]=z[16] + z[15];
    z[17]=z[3]*z[17];
    z[18]=n<T>(3,2)*z[14];
    z[13]=z[17] - z[18] + z[13];
    z[13]=z[4]*z[13];
    z[17]=2*z[10];
    z[19]= - static_cast<T>(1)+ n<T>(3,2)*z[10];
    z[19]=z[7]*z[19];
    z[20]=n<T>(5,2)*z[7];
    z[21]= - z[20] - z[3];
    z[21]=z[3]*z[21];
    z[22]=npow(z[5],2);
    z[23]=static_cast<T>(1)- 12*z[22];
    z[23]=z[5]*z[23];
    z[13]=z[13] + z[23] + z[21] + z[17] + z[19];
    z[13]=z[4]*z[13];
    z[19]=3*z[5];
    z[21]=6*z[5];
    z[23]=static_cast<T>(5)- z[21];
    z[23]=z[23]*z[19];
    z[23]=static_cast<T>(1)+ z[23];
    z[24]=2*z[5];
    z[23]=z[23]*z[24];
    z[25]=21*z[5];
    z[26]= - static_cast<T>(10)+ z[25];
    z[26]=z[26]*z[22];
    z[27]=npow(z[3],2);
    z[26]= - z[27] + z[26];
    z[26]=z[1]*z[26];
    z[28]=5*z[3];
    z[29]= - static_cast<T>(1)+ z[28];
    z[13]=z[13] + z[26] + n<T>(1,2)*z[29] + z[23];
    z[13]=z[4]*z[13];
    z[23]= - static_cast<T>(3)+ z[24];
    z[23]=z[23]*z[25];
    z[23]=static_cast<T>(20)+ z[23];
    z[23]=z[5]*z[23];
    z[26]=z[5] - 1;
    z[29]=z[26]*z[22];
    z[30]= - z[27] - 9*z[29];
    z[30]=z[1]*z[30];
    z[23]=z[30] + z[23] + static_cast<T>(1)+ n<T>(5,2)*z[3];
    z[23]=z[1]*z[23];
    z[30]=static_cast<T>(5)- z[19];
    z[30]=z[5]*z[30];
    z[30]= - n<T>(15,2) + 4*z[30];
    z[30]=z[30]*z[19];
    z[13]=z[13] + z[23] - static_cast<T>(1)+ z[30];
    z[13]=z[4]*z[13];
    z[23]=static_cast<T>(1)- n<T>(5,2)*z[10];
    z[23]=z[23]*z[11];
    z[23]= - 4*z[14] + z[23];
    z[23]=z[7]*z[23];
    z[30]=n<T>(1,2)*z[7];
    z[31]=static_cast<T>(2)+ z[30];
    z[15]=z[31]*z[15];
    z[15]=z[16] + z[15];
    z[15]=z[3]*z[15];
    z[15]=z[15] - z[18] + z[23];
    z[15]=z[4]*z[15];
    z[16]= - static_cast<T>(2)- z[20];
    z[16]=z[3]*z[16];
    z[16]= - z[20] + z[16];
    z[16]=z[3]*z[16];
    z[18]=z[11] + z[10];
    z[15]=z[15] + n<T>(5,2)*z[18] + z[16];
    z[15]=z[4]*z[15];
    z[16]= - static_cast<T>(1)+ 3*z[22];
    z[16]=z[16]*z[24];
    z[20]=z[27]*z[1];
    z[23]=2*z[20];
    z[31]=2*z[3];
    z[32]=n<T>(7,2) + z[31];
    z[32]=z[3]*z[32];
    z[15]=z[15] - z[23] + z[16] - static_cast<T>(1)+ z[32];
    z[15]=z[4]*z[15];
    z[16]=z[3] + 3;
    z[32]=3*z[3];
    z[33]=z[16]*z[32];
    z[33]=static_cast<T>(1)+ z[33];
    z[34]=15*z[5];
    z[35]=static_cast<T>(14)- z[34];
    z[35]=z[5]*z[35];
    z[35]=static_cast<T>(1)+ z[35];
    z[35]=z[5]*z[35];
    z[33]= - z[23] + n<T>(1,2)*z[33] + z[35];
    z[33]=z[1]*z[33];
    z[35]=9*z[5];
    z[36]=z[26]*z[35];
    z[36]= - static_cast<T>(2)+ z[36];
    z[36]=z[36]*z[24];
    z[32]=z[32] + 1;
    z[15]=z[15] + z[33] - n<T>(1,2)*z[32] + z[36];
    z[15]=z[4]*z[15];
    z[33]=n<T>(11,2) + z[3];
    z[33]=z[3]*z[33];
    z[36]=12*z[5];
    z[37]= - static_cast<T>(23)+ z[36];
    z[37]=z[5]*z[37];
    z[37]=static_cast<T>(10)+ z[37];
    z[37]=z[5]*z[37];
    z[23]= - z[23] + z[37] + static_cast<T>(2)+ z[33];
    z[23]=z[1]*z[23];
    z[33]=z[3] + 2;
    z[37]=z[5] - 2;
    z[38]=z[37]*z[5];
    z[39]= - static_cast<T>(29)- 30*z[38];
    z[39]=z[5]*z[39];
    z[23]=z[23] - 2*z[33] + z[39];
    z[23]=z[1]*z[23];
    z[39]=n<T>(31,2) + 18*z[38];
    z[39]=z[5]*z[39];
    z[15]=z[15] + z[23] + static_cast<T>(2)+ z[39];
    z[15]=z[4]*z[15];
    z[23]=z[27] - z[14];
    z[30]=z[30] + 1;
    z[23]=z[7]*z[30]*z[23];
    z[23]= - n<T>(1,2)*z[14] + z[23];
    z[23]=z[4]*z[23];
    z[30]= - static_cast<T>(1)- 3*z[7];
    z[30]=z[3]*z[30];
    z[30]=z[7] + z[30];
    z[30]=z[3]*z[30];
    z[18]=z[23] + z[30] + z[18];
    z[18]=z[4]*z[18];
    z[23]= - static_cast<T>(1)+ 5*z[27];
    z[18]=z[18] + n<T>(1,2)*z[23] - z[20];
    z[18]=z[4]*z[18];
    z[23]=static_cast<T>(1)+ z[31];
    z[23]=z[3]*z[23];
    z[23]=z[23] - z[20];
    z[23]=z[1]*z[23];
    z[27]=static_cast<T>(1)- z[22];
    z[27]=z[5]*z[27];
    z[18]=z[18] + z[23] - z[31] + z[27];
    z[18]=z[4]*z[18];
    z[23]=static_cast<T>(2)+ n<T>(3,2)*z[3];
    z[23]=z[3]*z[23];
    z[23]= - z[20] + n<T>(1,2) + z[23];
    z[23]=z[1]*z[23];
    z[27]=z[19] - 4;
    z[27]=z[27]*z[5];
    z[30]= - static_cast<T>(1)+ z[27];
    z[30]=z[5]*z[30];
    z[23]=z[23] + z[30] - z[32];
    z[23]=z[1]*z[23];
    z[30]=n<T>(7,2) - z[19];
    z[30]=z[5]*z[30];
    z[30]=static_cast<T>(2)+ z[30];
    z[30]=z[5]*z[30];
    z[18]=z[18] + z[23] + n<T>(1,2) + z[30];
    z[18]=z[4]*z[18];
    z[23]=z[3]*z[16];
    z[23]= - z[20] + static_cast<T>(1)+ z[23];
    z[23]=z[1]*z[23];
    z[30]= - static_cast<T>(1)- z[3];
    z[32]=static_cast<T>(8)- z[19];
    z[32]=z[5]*z[32];
    z[32]= - static_cast<T>(6)+ z[32];
    z[32]=z[5]*z[32];
    z[23]=z[23] + 3*z[30] + z[32];
    z[23]=z[1]*z[23];
    z[24]=z[24] - 5;
    z[30]=z[5]*z[24];
    z[30]=static_cast<T>(3)+ z[30];
    z[30]=z[5]*z[30];
    z[30]=static_cast<T>(1)+ z[30];
    z[23]=3*z[30] + z[23];
    z[23]=z[1]*z[23];
    z[30]=static_cast<T>(7)- z[19];
    z[30]=z[5]*z[30];
    z[30]= - static_cast<T>(3)+ z[30];
    z[30]=z[5]*z[30];
    z[18]=z[18] + z[23] - static_cast<T>(1)+ z[30];
    z[18]=z[4]*z[18];
    z[23]=n<T>(1,2)*z[20];
    z[30]= - z[3]*z[33];
    z[30]=z[30] + z[23];
    z[30]=z[1]*z[30];
    z[32]=static_cast<T>(4)+ n<T>(1,2)*z[3];
    z[32]=z[3]*z[32];
    z[30]=z[30] + n<T>(3,2) + z[32];
    z[30]=z[1]*z[30];
    z[32]= - static_cast<T>(4)+ z[5];
    z[32]=z[5]*z[32];
    z[32]=static_cast<T>(6)+ z[32];
    z[32]=z[5]*z[32];
    z[16]=z[30] - 2*z[16] + z[32];
    z[16]=z[1]*z[16];
    z[30]=n<T>(23,2) - z[19];
    z[30]=z[5]*z[30];
    z[30]= - static_cast<T>(16)+ z[30];
    z[30]=z[5]*z[30];
    z[16]=z[16] + static_cast<T>(9)+ z[30];
    z[16]=z[1]*z[16];
    z[30]= - static_cast<T>(11)+ z[19];
    z[30]=z[5]*z[30];
    z[30]=static_cast<T>(14)+ z[30];
    z[30]=z[5]*z[30];
    z[16]=z[16] - static_cast<T>(6)+ z[30];
    z[16]=z[1]*z[16];
    z[30]=n<T>(7,2) - z[5];
    z[30]=z[5]*z[30];
    z[30]= - static_cast<T>(4)+ z[30];
    z[30]=z[5]*z[30];
    z[16]=z[18] + z[16] + n<T>(3,2) + z[30];
    z[16]=z[2]*z[16];
    z[18]=z[5] - 3;
    z[30]= - z[5]*z[18];
    z[30]= - static_cast<T>(3)+ z[30];
    z[30]=z[30]*z[19];
    z[20]=n<T>(3,2)*z[20];
    z[31]= - static_cast<T>(5)- z[31];
    z[31]=z[3]*z[31];
    z[31]=z[31] + z[20];
    z[31]=z[1]*z[31];
    z[32]=static_cast<T>(13)+ z[3];
    z[32]=z[3]*z[32];
    z[32]=static_cast<T>(11)+ z[32];
    z[30]=z[31] + n<T>(1,2)*z[32] + z[30];
    z[30]=z[1]*z[30];
    z[31]= - static_cast<T>(11)- z[3];
    z[32]= - static_cast<T>(37)+ z[36];
    z[32]=z[5]*z[32];
    z[32]=static_cast<T>(39)+ z[32];
    z[32]=z[5]*z[32];
    z[30]=z[30] + n<T>(3,2)*z[31] + z[32];
    z[30]=z[1]*z[30];
    z[31]=static_cast<T>(46)- z[34];
    z[31]=z[5]*z[31];
    z[31]= - n<T>(95,2) + z[31];
    z[31]=z[5]*z[31];
    z[30]=z[30] + n<T>(33,2) + z[31];
    z[30]=z[1]*z[30];
    z[18]=z[18]*z[21];
    z[18]=n<T>(35,2) + z[18];
    z[18]=z[5]*z[18];
    z[15]=z[16] + z[15] + z[30] - n<T>(11,2) + z[18];
    z[15]=z[2]*z[15];
    z[16]=n<T>(5,2) - z[5];
    z[16]=z[5]*z[16];
    z[16]= - static_cast<T>(2)+ z[16];
    z[16]=z[16]*z[35];
    z[18]= - static_cast<T>(4)- z[3];
    z[18]=z[3]*z[18];
    z[18]=z[18] + z[20];
    z[18]=z[1]*z[18];
    z[20]=static_cast<T>(11)+ z[28];
    z[16]=z[18] + n<T>(1,2)*z[20] + z[16];
    z[16]=z[1]*z[16];
    z[18]= - static_cast<T>(53)+ z[25];
    z[18]=z[5]*z[18];
    z[18]=static_cast<T>(43)+ z[18];
    z[18]=z[5]*z[18];
    z[16]=z[16] - static_cast<T>(11)+ z[18];
    z[16]=z[1]*z[16];
    z[18]= - z[24]*z[21];
    z[18]= - n<T>(47,2) + z[18];
    z[18]=z[5]*z[18];
    z[13]=z[15] + z[13] + z[16] + n<T>(11,2) + z[18];
    z[13]=z[2]*z[13];
    z[15]=static_cast<T>(6)- n<T>(17,3)*z[10];
    z[15]=z[10]*z[15];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[7]*z[15];
    z[15]= - n<T>(16,3)*z[14] + z[15];
    z[15]=z[7]*z[15];
    z[16]=n<T>(7,6)*z[14];
    z[18]=npow(z[5],3);
    z[15]=10*z[18] - z[16] + z[15];
    z[15]=z[4]*z[15];
    z[20]= - static_cast<T>(2)+ z[19];
    z[20]=z[20]*z[22];
    z[11]=11*z[10] + 5*z[11];
    z[21]=z[1]*z[18];
    z[11]=z[15] - 9*z[21] + n<T>(1,6)*z[11] + 10*z[20];
    z[11]=z[4]*z[11];
    z[15]=z[1]*z[29];
    z[20]=n<T>(23,2) + 10*z[27];
    z[20]=z[5]*z[20];
    z[11]=z[11] - 18*z[15] - n<T>(2,3) + z[20];
    z[11]=z[4]*z[11];
    z[15]= - z[37]*z[19];
    z[15]= - n<T>(7,2) + z[15];
    z[15]=z[5]*z[15];
    z[15]=n<T>(1,2) + z[15];
    z[20]= - z[3] + z[23];
    z[20]=z[1]*z[20];
    z[15]=3*z[15] + z[20];
    z[15]=z[1]*z[15];
    z[20]=n<T>(23,2) + 10*z[38];
    z[20]=z[5]*z[20];
    z[11]=z[13] + z[11] + z[15] - n<T>(3,2) + z[20];
    z[11]=z[2]*z[11];
    z[13]=z[6]*z[9];
    z[15]= - static_cast<T>(1)+ n<T>(1,2)*z[9];
    z[15]=z[15]*z[13];
    z[15]=n<T>(10,3) + z[15];
    z[20]=npow(z[6],2);
    z[15]=z[15]*z[20];
    z[21]=z[20]*z[10];
    z[23]= - n<T>(13,3) + z[13];
    z[23]=z[23]*z[21];
    z[15]=z[15] + n<T>(1,2)*z[23];
    z[15]=z[10]*z[15];
    z[23]=z[9]*z[8];
    z[24]=z[8] - n<T>(1,2)*z[23];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(1,2)*z[8] + z[24];
    z[24]=z[6]*z[24];
    z[24]= - n<T>(7,6) + z[24];
    z[20]=z[24]*z[20];
    z[15]=z[20] + z[15];
    z[15]=z[7]*z[15];
    z[20]=n<T>(1,2)*z[6];
    z[23]=z[8] - z[23];
    z[23]=z[23]*z[20];
    z[23]=n<T>(2,3) + z[23];
    z[23]=z[6]*z[23];
    z[13]= - n<T>(1,3) + z[13];
    z[13]=z[13]*z[20];
    z[13]=z[13] - z[21];
    z[13]=z[10]*z[13];
    z[13]=z[15] + z[23] + z[13];
    z[13]=z[7]*z[13];
    z[12]=n<T>(16,3) - z[12];
    z[12]=z[10]*z[12];
    z[12]= - n<T>(3,2) + z[12];
    z[12]=z[7]*z[12];
    z[12]= - n<T>(14,3)*z[14] + z[12];
    z[12]=z[7]*z[12];
    z[14]=n<T>(1,2) - z[5];
    z[14]=z[14]*z[22];
    z[15]=z[4]*z[18];
    z[12]= - 3*z[15] + 9*z[14] - z[16] + z[12];
    z[12]=z[4]*z[12];
    z[14]=z[10]*z[6];
    z[15]=n<T>(7,3)*z[6] - 2*z[14];
    z[15]=z[15]*z[17];
    z[15]= - n<T>(7,6)*z[6] + z[15];
    z[15]=z[7]*z[15];
    z[16]=n<T>(7,2) - 10*z[14];
    z[16]=z[10]*z[16];
    z[15]=z[15] + n<T>(1,2) + n<T>(1,3)*z[16];
    z[15]=z[7]*z[15];
    z[16]= - z[26]*z[19];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[16]*z[19];
    z[17]=n<T>(5,3) - n<T>(1,2)*z[14];
    z[17]=z[10]*z[17];
    z[12]=z[12] + z[16] + z[17] + z[15];
    z[12]=z[4]*z[12];
    z[15]=n<T>(3,2) - z[5];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[15]*z[19];
    z[14]= - static_cast<T>(1)+ z[14];

    r += z[11] + z[12] + z[13] + n<T>(1,2)*z[14] + z[15];
 
    return r;
}

template double qg_2lha_r2111(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2111(const std::array<dd_real,30>&);
#endif
