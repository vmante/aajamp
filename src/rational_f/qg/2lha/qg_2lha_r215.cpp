#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r215(const std::array<T,30>& k) {
  T z[32];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[6];
    z[6]=k[3];
    z[7]=k[11];
    z[8]=k[14];
    z[9]=k[9];
    z[10]=k[4];
    z[11]=n<T>(1,3)*z[2];
    z[12]=npow(z[4],2);
    z[13]=z[11]*z[12];
    z[14]=n<T>(1,4)*z[4];
    z[15]= - n<T>(5,2) - z[4];
    z[15]=z[15]*z[14];
    z[15]=z[15] + z[13];
    z[15]=z[2]*z[15];
    z[16]=n<T>(1,2)*z[4];
    z[17]=n<T>(3,2) + z[4];
    z[17]=z[17]*z[16];
    z[17]=n<T>(5,3) + z[17];
    z[15]=n<T>(1,2)*z[17] + z[15];
    z[15]=z[2]*z[15];
    z[17]=5*z[4];
    z[18]= - static_cast<T>(3)- z[17];
    z[18]=z[18]*z[16];
    z[19]=z[12]*z[2];
    z[18]=z[18] + z[19];
    z[20]=n<T>(1,2)*z[2];
    z[18]=z[18]*z[20];
    z[21]=n<T>(3,4)*z[4];
    z[22]=z[21] + 1;
    z[22]=z[22]*z[4];
    z[18]=z[18] + n<T>(1,4) + z[22];
    z[23]=npow(z[2],3);
    z[18]=z[18]*z[23];
    z[24]=z[20]*z[12];
    z[25]=z[4] + 1;
    z[25]=z[25]*z[4];
    z[25]=z[25] - z[24];
    z[25]=z[25]*z[2];
    z[26]=z[16] + 1;
    z[27]=z[26]*z[4];
    z[25]=z[25] - z[27] - n<T>(1,2);
    z[25]=z[25]*npow(z[2],4);
    z[27]=n<T>(1,2)*z[7];
    z[28]=z[27]*z[25];
    z[18]=z[18] + z[28];
    z[18]=z[7]*z[18];
    z[28]=n<T>(29,24) + z[4];
    z[28]=z[4]*z[28];
    z[28]=z[28] - n<T>(7,12)*z[19];
    z[28]=z[2]*z[28];
    z[29]=3*z[4];
    z[30]= - n<T>(7,2) - z[29];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(9,2) + z[30];
    z[28]=n<T>(1,4)*z[30] + z[28];
    z[30]=npow(z[2],2);
    z[28]=z[28]*z[30];
    z[18]=z[28] + z[18];
    z[18]=z[7]*z[18];
    z[15]=z[15] + z[18];
    z[18]=z[7]*z[25];
    z[25]=static_cast<T>(1)- n<T>(1,12)*z[4];
    z[25]=z[4]*z[25];
    z[25]=z[25] - n<T>(7,24)*z[19];
    z[25]=z[2]*z[25];
    z[28]= - n<T>(1,3) + n<T>(3,8)*z[4];
    z[28]=z[4]*z[28];
    z[25]=z[25] - n<T>(17,24) + z[28];
    z[25]=z[25]*z[23];
    z[25]=z[25] + n<T>(1,4)*z[18];
    z[25]=z[7]*z[25];
    z[28]= - n<T>(17,4) - z[4];
    z[28]=z[4]*z[28];
    z[28]=z[28] + n<T>(5,2)*z[19];
    z[28]=z[28]*z[11];
    z[31]=n<T>(1,3) - z[16];
    z[31]=z[4]*z[31];
    z[28]=z[31] + z[28];
    z[28]=z[2]*z[28];
    z[31]=z[14] + n<T>(1,3);
    z[28]=z[28] - z[31];
    z[20]=z[28]*z[20];
    z[20]=z[20] + z[25];
    z[25]= - n<T>(1,2) - z[4];
    z[25]=z[4]*z[25];
    z[25]=z[25] + z[24];
    z[25]=z[2]*z[25];
    z[28]=n<T>(1,2)*z[12];
    z[25]=z[28] + z[25];
    z[25]=z[2]*z[25];
    z[25]=z[16] + z[25];
    z[25]=z[2]*z[25];
    z[25]=n<T>(1,2) + z[25];
    z[25]=z[2]*z[25];
    z[18]=z[25] + z[18];
    z[18]=z[3]*z[18];
    z[18]=n<T>(1,2)*z[20] + n<T>(1,3)*z[18];
    z[18]=z[3]*z[18];
    z[15]=n<T>(1,2)*z[15] + z[18];
    z[15]=z[3]*z[15];
    z[18]=z[4] + 3;
    z[20]= - z[18]*z[21];
    z[20]=z[20] + z[19];
    z[20]=z[2]*z[20];
    z[18]=n<T>(1,2)*z[18] + z[20];
    z[18]=z[2]*z[18];
    z[18]=n<T>(1,4) + z[18];
    z[20]=static_cast<T>(1)+ z[14];
    z[20]=z[4]*z[20];
    z[20]=z[20] - n<T>(3,8)*z[19];
    z[20]=z[2]*z[20];
    z[20]= - n<T>(3,4)*z[26] + z[20];
    z[20]=z[7]*z[20]*z[30];
    z[18]=n<T>(1,2)*z[18] + z[20];
    z[18]=z[7]*z[18];
    z[20]=n<T>(1,3)*z[4];
    z[21]=n<T>(3,16) + z[20];
    z[21]=z[4]*z[21];
    z[13]=z[21] - z[13];
    z[13]=z[2]*z[13];
    z[21]=static_cast<T>(7)- z[17];
    z[13]=z[18] + n<T>(1,48)*z[21] + z[13];
    z[13]=z[7]*z[13];
    z[18]=z[29] - z[19];
    z[18]=z[2]*z[18];
    z[18]= - static_cast<T>(3)+ z[18];
    z[18]=z[7]*z[18];
    z[18]=z[18] - z[4] + z[24];
    z[18]=z[2]*z[18];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[7]*z[18];
    z[21]=z[4] - z[19];
    z[18]=n<T>(5,6)*z[21] + z[18];
    z[21]=n<T>(1,4)*z[7];
    z[18]=z[18]*z[21];
    z[18]=z[18] + n<T>(1,3)*z[12];
    z[21]=n<T>(1,2)*z[6];
    z[18]=z[18]*z[21];
    z[17]=n<T>(19,2) - z[17];
    z[17]=z[4]*z[17];
    z[17]=z[17] + 5*z[19];
    z[13]=z[18] + n<T>(1,24)*z[17] + z[13];
    z[13]=z[6]*z[13];
    z[17]=static_cast<T>(1)+ n<T>(5,6)*z[4];
    z[17]=z[4]*z[17];
    z[17]=z[17] - n<T>(13,12)*z[19];
    z[17]=z[2]*z[17];
    z[18]=z[4]*z[31];
    z[17]=z[18] + z[17];
    z[17]=z[2]*z[17];
    z[18]=z[24] - z[12];
    z[18]=z[18]*z[2];
    z[25]=static_cast<T>(1)- z[16];
    z[25]=z[4]*z[25];
    z[25]=z[25] - z[18];
    z[25]=z[2]*z[25];
    z[25]= - z[4] + z[25];
    z[25]=z[3]*z[25]*z[11];
    z[17]=n<T>(1,4)*z[17] + z[25];
    z[17]=z[17]*npow(z[3],2);
    z[25]= - static_cast<T>(1)+ n<T>(5,12)*z[4];
    z[25]=z[4]*z[25];
    z[25]=z[25] - n<T>(5,24)*z[19];
    z[18]=z[28] + z[18];
    z[18]=z[2]*z[18]*npow(z[3],3);
    z[18]=z[28] + z[18];
    z[26]=n<T>(1,3)*z[1];
    z[18]=z[18]*z[26];
    z[29]=z[12]*z[6];
    z[17]=z[18] - n<T>(1,3)*z[29] + n<T>(1,2)*z[25] + z[17];
    z[17]=z[1]*z[17];
    z[18]=static_cast<T>(1)- n<T>(1,8)*z[4];
    z[18]=z[4]*z[18];
    z[12]= - z[1]*z[12];
    z[12]=z[12] + n<T>(5,2)*z[18] + z[29];
    z[12]=z[12]*z[26];
    z[18]=z[6] - 1;
    z[25]= - static_cast<T>(1)+ n<T>(5,24)*z[4];
    z[25]= - z[25]*z[18];
    z[18]=z[4]*z[18];
    z[26]=z[1]*z[28];
    z[18]=z[26] + z[18];
    z[18]=z[1]*z[18];
    z[26]= - static_cast<T>(1)+ z[21];
    z[26]=z[6]*z[26];
    z[18]=z[18] + n<T>(1,2) + z[26];
    z[18]=z[5]*z[18];
    z[12]=n<T>(1,3)*z[18] + n<T>(1,2)*z[25] + z[12];
    z[12]=z[5]*z[12];
    z[18]=z[6]*npow(z[7],3);
    z[25]= - z[7] - z[18];
    z[25]=z[25]*z[21];
    z[26]=npow(z[7],2);
    z[18]=z[18] - z[26];
    z[18]=z[18]*z[21];
    z[18]=z[18] + z[7];
    z[21]= - z[10]*z[18];
    z[21]=z[21] + z[25] - static_cast<T>(1)- z[27];
    z[21]=z[9]*z[21];
    z[18]=z[21] + z[18];
    z[18]=z[10]*z[18];
    z[21]= - z[3] - z[9];
    z[21]=z[8]*z[2]*z[21];
    z[18]=z[18] + z[21];
    z[21]= - static_cast<T>(5)- z[4];
    z[14]=z[21]*z[14];
    z[21]=n<T>(7,4) + z[4];
    z[21]=z[4]*z[21];
    z[21]=z[21] - n<T>(3,4)*z[19];
    z[21]=z[2]*z[21];
    z[14]=z[21] - static_cast<T>(1)+ z[14];
    z[14]=z[14]*z[23]*z[27];
    z[21]= - z[22] + z[24];
    z[21]=z[2]*z[21];
    z[22]=static_cast<T>(1)+ n<T>(5,16)*z[4];
    z[22]=z[4]*z[22];
    z[21]=n<T>(3,2)*z[21] + n<T>(3,4) + z[22];
    z[21]=z[21]*z[30];
    z[14]=z[21] + z[14];
    z[14]=z[7]*z[14];
    z[21]=n<T>(31,2) + 23*z[4];
    z[21]=z[4]*z[21];
    z[19]=z[21] - 13*z[19];
    z[19]=z[2]*z[19];
    z[21]= - static_cast<T>(1)- n<T>(17,16)*z[4];
    z[21]=z[4]*z[21];
    z[19]=n<T>(1,8)*z[19] - n<T>(23,16) + z[21];
    z[19]=z[19]*z[11];
    z[14]=z[14] - n<T>(1,8) + z[19];
    z[14]=z[7]*z[14];
    z[19]= - n<T>(1,2) + z[20];
    z[16]=z[19]*z[16];
    z[19]=n<T>(7,16) - z[4];
    z[19]=z[4]*z[19];
    z[19]=z[19] + z[24];
    z[11]=z[19]*z[11];
    z[19]= - z[6]*z[26];
    z[19]=z[19] - static_cast<T>(3)+ z[7];
    z[19]=z[9]*z[19];

    r += static_cast<T>(1)+ z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + n<T>(1,4)*z[18] + n<T>(1,8)*z[19];
 
    return r;
}

template double qg_2lha_r215(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r215(const std::array<dd_real,30>&);
#endif
