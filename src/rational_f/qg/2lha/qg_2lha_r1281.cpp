#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1281(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=z[3] + z[7];
    z[9]=static_cast<T>(1)+ z[8];
    z[10]=n<T>(1,2)*z[7];
    z[11]= - z[10] - z[3];
    z[11]=z[6]*z[11];
    z[9]=n<T>(1,2)*z[9] + z[11];
    z[9]=z[6]*z[9];
    z[9]= - n<T>(1,2) + z[9];
    z[10]= - z[6]*z[10];
    z[10]=z[10] - n<T>(1,2) + z[8];
    z[10]=z[6]*z[10];
    z[11]= - z[3]*z[4];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[3]*z[11];
    z[11]=z[11] - z[7] + static_cast<T>(1)+ z[1];
    z[10]=n<T>(1,2)*z[11] + z[10];
    z[10]=z[2]*z[10];
    z[9]=n<T>(1,2)*z[9] + z[10];
    z[9]=z[2]*z[9];
    z[10]=npow(z[6],2);
    z[10]=n<T>(1,4)*z[10];
    z[8]=z[8]*z[10];
    z[9]= - z[8] + z[9];
    z[9]=z[2]*z[9];
    z[8]=z[5]*z[8];
    z[8]=z[8] + z[9];

    r += n<T>(1,8)*z[8];
 
    return r;
}

template double qg_2lha_r1281(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1281(const std::array<dd_real,30>&);
#endif
