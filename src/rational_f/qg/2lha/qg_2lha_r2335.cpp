#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2335(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[2];
    z[7]=k[11];
    z[8]=k[14];
    z[9]=k[9];
    z[10]=k[10];
    z[11]=k[6];
    z[12]=k[7];
    z[13]=n<T>(1,3)*z[10];
    z[14]=n<T>(3,2)*z[8];
    z[15]= - z[13] + z[14];
    z[15]=z[2]*z[15];
    z[16]=n<T>(7,2) - z[1];
    z[16]=z[5]*z[16];
    z[17]=n<T>(1,3)*z[12];
    z[18]= - static_cast<T>(3)+ z[17];
    z[18]=z[8]*z[18];
    z[19]=static_cast<T>(5)- 7*z[8];
    z[19]=n<T>(1,2)*z[19] + z[2];
    z[19]=z[9]*z[19];
    z[15]=n<T>(1,3)*z[19] + z[15] + z[18] + z[13] + z[16];
    z[15]=z[4]*z[15];
    z[16]=z[2] - 3;
    z[18]=n<T>(1,2)*z[2];
    z[19]= - z[16]*z[18];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[9]*z[19];
    z[20]=3*z[11];
    z[21]=3*z[8];
    z[22]=n<T>(3,2) - z[5];
    z[19]=3*z[19] - 7*z[2] + z[21] + 5*z[22] + z[20];
    z[22]=n<T>(3,2)*z[11];
    z[23]= - z[21] - z[22] + static_cast<T>(3)- n<T>(13,2)*z[5];
    z[23]=z[4]*z[23];
    z[24]=z[8] + z[5];
    z[25]= - static_cast<T>(5)+ z[24];
    z[26]=n<T>(1,2)*z[11] - n<T>(3,2) + z[24];
    z[26]=z[4]*z[26];
    z[25]=z[26] + n<T>(1,2)*z[25] + z[2];
    z[25]=z[7]*z[25];
    z[19]=3*z[25] + n<T>(1,2)*z[19] + z[23];
    z[19]=z[7]*z[19];
    z[23]=static_cast<T>(5)- n<T>(11,2)*z[8];
    z[25]= - n<T>(1,3) + z[2];
    z[25]=z[2]*z[25];
    z[23]=n<T>(1,3)*z[23] + n<T>(1,4)*z[25];
    z[23]=z[9]*z[23];
    z[25]= - static_cast<T>(3)- z[17];
    z[25]=z[8]*z[25];
    z[26]=z[2]*z[21];
    z[26]= - n<T>(1,3) + z[26];
    z[26]=z[2]*z[26];
    z[15]=z[19] + z[15] + z[23] + z[26] + z[25] - z[22] + z[5] + n<T>(1,2)
    + z[17];
    z[19]= - z[5] + 1;
    z[23]=z[1] - n<T>(5,2);
    z[19]=z[23]*z[19];
    z[25]=n<T>(3,2)*z[2];
    z[26]=z[25]*z[8];
    z[27]=z[26] + z[13] - n<T>(9,2)*z[8];
    z[27]=z[2]*z[27];
    z[17]=z[17]*z[8];
    z[13]=z[27] - z[17] - z[13] + z[19];
    z[19]=n<T>(1,2) + n<T>(1,3)*z[2];
    z[19]=z[19]*z[18];
    z[19]=z[19] - static_cast<T>(1)+ n<T>(7,12)*z[8];
    z[19]=z[9]*z[19];
    z[27]=n<T>(1,2)*z[4];
    z[23]= - z[5]*z[23]*z[27];
    z[13]=z[23] + n<T>(1,2)*z[13] + z[19];
    z[13]=z[4]*z[13];
    z[19]= - n<T>(1,2) + z[10];
    z[19]=n<T>(1,3)*z[19] + z[26];
    z[19]=z[2]*z[19];
    z[23]=static_cast<T>(5)- z[10];
    z[14]=z[19] - z[14] + n<T>(1,3)*z[23] - z[1];
    z[14]=z[2]*z[14];
    z[19]=npow(z[2],2);
    z[19]=n<T>(1,2)*z[19] - n<T>(3,2) + z[8];
    z[19]=z[9]*z[19];
    z[14]=n<T>(3,2)*z[19] + z[14] - static_cast<T>(3)+ z[1];
    z[19]=z[4] + 1;
    z[23]=n<T>(9,4)*z[8] + 2*z[5];
    z[23]= - z[23]*z[19];
    z[23]=n<T>(7,2) + z[23];
    z[23]=z[4]*z[23];
    z[19]=z[24]*z[19];
    z[19]= - static_cast<T>(1)+ z[19];
    z[26]=n<T>(3,4)*z[4];
    z[19]=z[7]*z[19]*z[26];
    z[28]= - z[25] + n<T>(7,2) - z[8];
    z[19]=z[19] + n<T>(3,4)*z[28] + z[23];
    z[19]=z[7]*z[19];
    z[13]=z[19] + n<T>(1,2)*z[14] + z[13];
    z[13]=z[3]*z[13];
    z[13]=n<T>(1,2)*z[15] + z[13];
    z[13]=z[3]*z[13];
    z[14]= - z[11] + z[8];
    z[14]=z[14]*z[25];
    z[15]=z[2]*z[11];
    z[19]=n<T>(3,4)*z[15] + n<T>(11,6)*z[8] - n<T>(1,3) - z[22];
    z[19]=z[9]*z[19];
    z[23]= - z[20] + z[15];
    z[23]=z[9]*z[23];
    z[25]=z[11]*z[4]*z[9];
    z[23]=z[23] - z[25];
    z[23]=z[23]*z[26];
    z[26]= - n<T>(7,2) - z[12];
    z[14]=z[23] + z[19] + z[14] + z[17] + z[22] + n<T>(1,3)*z[26] + z[5];
    z[16]= - 7*z[11] - z[16];
    z[16]=z[9]*z[16];
    z[15]=z[16] + 5*z[11] + z[15];
    z[16]=n<T>(1,8) - z[11];
    z[16]=z[9]*z[16];
    z[16]= - n<T>(3,8)*z[25] + n<T>(1,8)*z[11] + z[16];
    z[16]=z[4]*z[16];
    z[15]=n<T>(1,8)*z[15] + z[16];
    z[15]=z[4]*z[15];
    z[16]=z[18] + 1;
    z[17]= - z[22] + z[16];
    z[17]=z[9]*z[17];
    z[19]=static_cast<T>(1)- z[20];
    z[19]=z[9]*z[19];
    z[19]= - z[25] + z[11] + z[19];
    z[19]=z[19]*z[27];
    z[16]=z[11]*z[16];
    z[16]=z[19] + z[17] + z[16];
    z[16]=z[4]*z[16];
    z[17]=z[2] - 1;
    z[19]=static_cast<T>(1)+ z[2];
    z[19]=z[2]*z[19];
    z[19]=z[19] + static_cast<T>(1)- z[11];
    z[19]=z[9]*z[19];
    z[19]=z[19] - z[17];
    z[16]=n<T>(1,2)*z[19] + z[16];
    z[16]=z[4]*z[16];
    z[19]=z[6] - 1;
    z[19]=z[19]*z[17];
    z[19]=z[24] + z[19];
    z[18]=static_cast<T>(1)- z[18];
    z[18]=z[9]*z[2]*z[18];
    z[16]=z[16] + n<T>(1,2)*z[19] + z[18];
    z[16]=z[7]*z[16];
    z[17]=z[21] - 5*z[5] - z[17];
    z[18]= - static_cast<T>(1)- z[11];
    z[19]=static_cast<T>(1)+ n<T>(1,8)*z[2];
    z[19]=z[2]*z[19];
    z[18]=n<T>(3,4)*z[18] + z[19];
    z[18]=z[9]*z[18];
    z[15]=n<T>(3,2)*z[16] + 3*z[15] + n<T>(1,4)*z[17] + z[18];
    z[15]=z[7]*z[15];

    r += z[13] + n<T>(1,2)*z[14] + z[15];
 
    return r;
}

template double qg_2lha_r2335(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2335(const std::array<dd_real,30>&);
#endif
