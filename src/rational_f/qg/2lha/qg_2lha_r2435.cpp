#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2435(const std::array<T,30>& k) {
  T z[42];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=k[12];
    z[9]=z[1] - 3;
    z[10]=n<T>(1,4)*z[1];
    z[11]= - z[9]*z[10];
    z[12]=3*z[1];
    z[13]= - static_cast<T>(7)+ z[12];
    z[13]=z[13]*z[10];
    z[14]=n<T>(1,2)*z[1];
    z[15]=z[14] - 1;
    z[16]=z[15]*z[1];
    z[16]=z[16] + n<T>(1,2);
    z[17]=z[16]*z[2];
    z[13]= - z[17] + static_cast<T>(1)+ z[13];
    z[13]=z[2]*z[13];
    z[11]=z[11] + z[13];
    z[11]=z[2]*z[11];
    z[13]=n<T>(1,2)*z[8];
    z[18]= - z[10] - z[13] - static_cast<T>(1)+ n<T>(1,2)*z[7];
    z[11]=n<T>(1,2)*z[18] + z[11];
    z[11]=z[2]*z[11];
    z[18]= - static_cast<T>(27)+ 13*z[8];
    z[18]=z[18]*z[13];
    z[18]=static_cast<T>(5)+ z[18];
    z[18]=z[5]*z[18];
    z[19]=z[14]*z[5];
    z[20]= - static_cast<T>(3)+ n<T>(13,2)*z[8];
    z[21]=z[8]*z[20];
    z[18]=z[19] + z[18] - n<T>(3,2) + z[21];
    z[12]= - n<T>(17,2) + z[12];
    z[12]=z[1]*z[12];
    z[12]= - 3*z[17] + n<T>(11,2) + z[12];
    z[21]=n<T>(1,2)*z[2];
    z[12]=z[12]*z[21];
    z[22]=n<T>(3,4)*z[1];
    z[23]=z[22] - 4;
    z[23]=z[23]*z[1];
    z[12]=z[12] - n<T>(7,2) - z[23];
    z[12]=z[2]*z[12];
    z[24]=static_cast<T>(7)- 5*z[1];
    z[12]=n<T>(1,4)*z[24] + z[12];
    z[12]=z[2]*z[12];
    z[12]=n<T>(1,4)*z[18] + z[12];
    z[12]=z[3]*z[12];
    z[18]=n<T>(1,2)*z[6];
    z[24]=z[18] - 1;
    z[25]=z[24]*z[7];
    z[25]=z[25] + n<T>(1,2);
    z[26]=z[10]*z[5];
    z[27]= - n<T>(1,4) - z[8];
    z[27]=z[8]*z[27];
    z[27]=n<T>(3,4) + z[27];
    z[27]=z[5]*z[27];
    z[11]=z[12] + z[11] + z[26] + n<T>(1,2)*z[25] + z[27];
    z[11]=z[3]*z[11];
    z[12]= - n<T>(1,2) + z[1];
    z[12]=z[1]*z[12];
    z[12]= - z[17] - n<T>(1,2) + z[12];
    z[12]=z[2]*z[12];
    z[12]=n<T>(7,4) + z[12];
    z[12]=z[2]*z[12];
    z[17]=z[7] + z[13] - n<T>(3,2);
    z[12]=z[12] - z[14] + z[17];
    z[12]=z[2]*z[12];
    z[27]=z[8] - 1;
    z[28]=n<T>(1,2)*z[5];
    z[29]= - z[27]*z[28];
    z[12]=z[12] + z[19] - z[7] + z[29];
    z[11]=n<T>(1,2)*z[12] + z[11];
    z[11]=z[3]*z[11];
    z[12]=z[5]*z[8];
    z[29]=z[13] + z[12];
    z[29]=z[5]*z[29];
    z[30]=n<T>(1,4)*z[5];
    z[31]= - static_cast<T>(1)- z[5];
    z[31]=z[2]*z[31]*z[30];
    z[29]=z[29] + z[31];
    z[31]=npow(z[2],2);
    z[29]=z[29]*z[31];
    z[32]=z[13]*z[5];
    z[33]=z[13] - 1;
    z[34]=z[33]*z[32];
    z[29]=z[34] + z[29];
    z[29]=z[2]*z[29];
    z[34]=z[33]*z[12];
    z[35]= - static_cast<T>(3)+ 7*z[8];
    z[32]=z[35]*z[32];
    z[35]= - static_cast<T>(1)+ n<T>(7,2)*z[8];
    z[35]=z[8]*z[35];
    z[32]=z[35] + z[32];
    z[35]=n<T>(1,2)*z[3];
    z[32]=z[32]*z[35];
    z[36]=n<T>(1,4)*z[2];
    z[37]=z[8]*z[36];
    z[32]=z[32] + z[34] + z[37];
    z[32]=z[3]*z[32];
    z[34]= - z[2]*z[8];
    z[34]=z[12] + z[34];
    z[34]=z[33]*z[34];
    z[32]=z[32] + z[34];
    z[32]=z[32]*z[35];
    z[34]=npow(z[8],2);
    z[37]=z[34]*z[5];
    z[38]=z[34] + z[37];
    z[38]=z[3]*z[38];
    z[38]=z[37] + z[38];
    z[38]=z[3]*z[38];
    z[39]= - z[2]*z[34];
    z[37]=z[38] + z[37] + z[39];
    z[35]=z[37]*z[35];
    z[34]=z[34]*z[28];
    z[37]=z[8] + z[12];
    z[37]=z[5]*z[37]*npow(z[2],3);
    z[34]=z[34] + z[37];
    z[34]=z[2]*z[34];
    z[34]=z[34] + z[35];
    z[34]=z[4]*z[34];
    z[29]=n<T>(1,4)*z[34] + z[29] + z[32];
    z[29]=z[4]*z[29];
    z[32]=3*z[8];
    z[34]=n<T>(5,2)*z[8];
    z[35]= - static_cast<T>(1)+ z[34];
    z[35]=z[35]*z[32];
    z[34]= - static_cast<T>(3)+ z[34];
    z[32]=z[34]*z[32];
    z[32]=static_cast<T>(1)+ z[32];
    z[32]=z[5]*z[32];
    z[32]=z[32] - n<T>(1,2) + z[35];
    z[34]=z[1] - 1;
    z[35]=z[34]*z[36];
    z[22]=z[35] + static_cast<T>(1)- z[22];
    z[22]=z[2]*z[22];
    z[22]=n<T>(3,2)*z[15] + z[22];
    z[22]=z[2]*z[22];
    z[35]=z[10] - 1;
    z[22]=z[22] - z[35];
    z[22]=z[2]*z[22];
    z[22]=n<T>(1,4)*z[32] + z[22];
    z[22]=z[3]*z[22];
    z[32]= - static_cast<T>(5)- n<T>(3,2)*z[8];
    z[32]=z[8]*z[32];
    z[32]=n<T>(3,2) + z[32];
    z[32]=z[32]*z[30];
    z[37]=z[21]*z[34];
    z[38]=z[1] - n<T>(3,2);
    z[39]=z[37] - z[38];
    z[39]=z[2]*z[39];
    z[39]=n<T>(1,2)*z[9] + z[39];
    z[39]=z[2]*z[39];
    z[40]=z[8] - 3;
    z[39]= - n<T>(1,8)*z[40] + z[39];
    z[39]=z[2]*z[39];
    z[22]=z[22] + z[32] + z[39];
    z[22]=z[3]*z[22];
    z[32]=z[13] - 3;
    z[39]=z[32]*z[8];
    z[39]=z[39] + n<T>(3,2);
    z[41]=z[39]*z[28];
    z[15]=z[37] - z[15];
    z[15]=z[2]*z[15];
    z[15]=n<T>(1,2)*z[33] + z[15];
    z[33]=3*z[2];
    z[15]=z[15]*z[33];
    z[15]= - n<T>(1,2)*z[39] + z[15];
    z[15]=z[2]*z[15];
    z[15]=z[41] + z[15];
    z[15]=n<T>(1,2)*z[15] + z[22];
    z[15]=z[3]*z[15];
    z[22]=n<T>(3,2) - z[5];
    z[22]=z[22]*z[19];
    z[33]= - static_cast<T>(2)- z[30];
    z[33]=z[5]*z[33];
    z[37]=z[1]*z[5];
    z[41]=z[5] - z[37];
    z[41]=z[2]*z[41];
    z[22]=z[41] + z[33] + z[22];
    z[22]=z[2]*z[22];
    z[20]=z[5]*z[20];
    z[20]=z[20] - z[32];
    z[20]=z[5]*z[20];
    z[32]=npow(z[5],2);
    z[33]=z[32]*z[1];
    z[20]=z[20] + z[33];
    z[20]=n<T>(1,4)*z[20] + z[22];
    z[20]=z[2]*z[20];
    z[22]=z[5] + z[39];
    z[22]=z[22]*z[30];
    z[20]=z[22] + z[20];
    z[20]=z[2]*z[20];
    z[15]=z[29] + z[20] + z[15];
    z[15]=z[4]*z[15];
    z[20]= - static_cast<T>(9)- z[5];
    z[20]=z[20]*z[30];
    z[22]= - static_cast<T>(1)- z[14];
    z[22]=z[22]*z[33];
    z[29]= - z[28] - z[37];
    z[29]=z[1]*z[29];
    z[29]=n<T>(3,2)*z[5] + z[29];
    z[29]=z[2]*z[29];
    z[20]=z[29] + z[20] + z[22];
    z[20]=z[20]*z[21];
    z[21]=n<T>(7,8)*z[8] - static_cast<T>(1)+ n<T>(1,4)*z[7];
    z[21]=z[5]*z[21];
    z[17]= - n<T>(1,2)*z[17] + z[21];
    z[17]=z[5]*z[17];
    z[17]=z[20] + z[17] + z[26];
    z[17]=z[2]*z[17];
    z[20]=z[5]*z[25];
    z[20]=z[7] + z[20];
    z[20]=z[20]*z[28];
    z[11]=z[15] + z[11] + z[20] + z[17];
    z[11]=z[4]*z[11];
    z[15]=z[32]*z[14];
    z[17]=z[24]*z[6];
    z[17]=z[17] + n<T>(1,2);
    z[17]=z[17]*z[7];
    z[20]=z[6] - 1;
    z[21]=n<T>(1,2)*z[20] + z[17];
    z[21]=z[5]*z[21];
    z[22]=z[20]*z[7];
    z[22]=z[22] + 1;
    z[21]=n<T>(1,2)*z[22] + z[21];
    z[21]=z[5]*z[21];
    z[24]=n<T>(1,2) - z[5];
    z[24]=z[24]*z[37];
    z[25]= - n<T>(1,2) - z[5];
    z[25]=z[5]*z[25];
    z[24]=z[25] + z[24];
    z[24]=z[1]*z[24];
    z[25]=npow(z[1],2);
    z[29]=z[25]*z[5];
    z[30]=z[5] - z[29];
    z[30]=z[2]*z[30];
    z[24]=z[24] + z[30];
    z[24]=z[2]*z[24];
    z[21]=z[24] + z[21] - z[15];
    z[13]=z[40]*z[13];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[5]*z[13];
    z[9]=z[9]*z[1];
    z[9]=z[9] + 3;
    z[9]=z[9]*z[1];
    z[9]=z[9] - 1;
    z[24]=z[9]*z[2];
    z[23]= - n<T>(23,4) - z[23];
    z[23]=z[1]*z[23];
    z[23]=n<T>(3,4)*z[24] + n<T>(5,2) + z[23];
    z[23]=z[2]*z[23];
    z[30]=static_cast<T>(19)- n<T>(15,2)*z[1];
    z[30]=z[1]*z[30];
    z[30]= - n<T>(23,2) + z[30];
    z[23]=n<T>(1,4)*z[30] + z[23];
    z[23]=z[2]*z[23];
    z[23]=z[23] + n<T>(5,4) - 2*z[1];
    z[23]=z[2]*z[23];
    z[30]= - n<T>(1,2) + z[8];
    z[30]=z[8]*z[30];
    z[30]= - n<T>(3,4) + z[30];
    z[13]=z[23] + z[19] + n<T>(1,2)*z[30] + z[13];
    z[13]=z[3]*z[13];
    z[19]=static_cast<T>(5)- z[1];
    z[19]=z[19]*z[10];
    z[23]=z[1] - 9;
    z[23]=z[23]*z[1];
    z[23]=z[23] + 15;
    z[23]=z[23]*z[1];
    z[23]=z[23] - 7;
    z[24]=n<T>(1,2)*z[23] - z[24];
    z[24]=z[2]*z[24];
    z[30]= - static_cast<T>(5)+ n<T>(3,2)*z[1];
    z[30]=z[1]*z[30];
    z[24]=z[24] + n<T>(7,2) + z[30];
    z[24]=z[2]*z[24];
    z[19]=z[24] - static_cast<T>(1)+ z[19];
    z[19]=z[2]*z[19];
    z[12]= - z[27]*z[12];
    z[24]=z[5] + z[26];
    z[24]=z[1]*z[24];
    z[12]=z[19] + z[12] + z[24];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[3]*z[12];
    z[13]=z[34]*z[10];
    z[19]= - z[9]*z[36];
    z[19]=z[19] - z[16];
    z[19]=z[2]*z[19];
    z[13]=z[13] + z[19];
    z[13]=z[2]*z[13];
    z[19]=z[1]*z[34];
    z[13]= - n<T>(1,8)*z[19] + z[13];
    z[13]=z[2]*z[13];
    z[19]=z[29] - z[22];
    z[12]=z[12] + n<T>(1,8)*z[19] + z[13];
    z[12]=z[3]*z[12];
    z[11]=z[11] + n<T>(1,4)*z[21] + z[12];
    z[11]=z[4]*z[11];
    z[12]=static_cast<T>(5)- z[14];
    z[12]=z[1]*z[12];
    z[12]= - n<T>(17,2) + z[12];
    z[12]=z[12]*z[10];
    z[13]= - static_cast<T>(2)+ z[14];
    z[13]=z[1]*z[13];
    z[13]=static_cast<T>(3)+ z[13];
    z[13]=z[1]*z[13];
    z[13]= - static_cast<T>(2)+ z[13];
    z[13]=z[1]*z[13];
    z[13]=n<T>(1,2) + z[13];
    z[13]=z[2]*z[13];
    z[13]=n<T>(5,4)*z[9] + z[13];
    z[13]=z[2]*z[13];
    z[12]=z[13] + static_cast<T>(1)+ z[12];
    z[12]=z[2]*z[12];
    z[13]= - z[1]*z[38];
    z[13]= - n<T>(1,2) + z[13];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[2]*z[12];
    z[13]= - static_cast<T>(5)+ z[25];
    z[10]=z[13]*z[10];
    z[10]=static_cast<T>(1)+ z[10];
    z[13]=z[35]*z[1];
    z[13]=z[13] + n<T>(3,2);
    z[13]=z[13]*z[1];
    z[13]=z[13] - 1;
    z[13]=z[13]*z[1];
    z[13]=z[13] + n<T>(1,4);
    z[13]=z[13]*z[2];
    z[14]= - z[13] - n<T>(3,4)*z[9];
    z[14]=z[2]*z[14];
    z[14]=n<T>(1,8)*z[23] + z[14];
    z[14]=z[2]*z[14];
    z[10]=n<T>(1,2)*z[10] + z[14];
    z[10]=z[2]*z[10];
    z[14]= - z[5]*npow(z[1],3);
    z[14]= - static_cast<T>(1)+ z[14];
    z[10]=n<T>(1,8)*z[14] + z[10];
    z[10]=z[3]*z[10];
    z[14]=z[25]*z[28];
    z[10]=z[10] + z[14] + z[12];
    z[10]=z[3]*z[10];
    z[9]= - n<T>(1,2)*z[9] - z[13];
    z[9]=z[2]*z[9];
    z[9]= - n<T>(1,2)*z[16] + z[9];
    z[9]=z[9]*z[31];
    z[9]=z[9] + z[10];
    z[9]=z[3]*z[9];
    z[10]= - z[20]*z[18];
    z[12]= - z[6]*z[17];
    z[10]=z[10] + z[12];
    z[10]=z[10]*z[32];
    z[12]=z[6] - z[1];
    z[12]=z[12]*z[15];
    z[10]=z[10] + z[12];

    r += z[9] + n<T>(1,4)*z[10] + z[11];
 
    return r;
}

template double qg_2lha_r2435(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2435(const std::array<dd_real,30>&);
#endif
