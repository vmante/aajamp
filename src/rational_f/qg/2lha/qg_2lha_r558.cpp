#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r558(const std::array<T,30>& k) {
  T z[24];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[13];
    z[5]=k[11];
    z[6]=k[4];
    z[7]=k[9];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=k[21];
    z[11]=z[2] - z[6];
    z[12]=npow(z[5],2);
    z[13]=z[12]*z[11];
    z[14]=n<T>(1,2)*z[7];
    z[15]=z[7] - 1;
    z[16]=z[15]*z[14];
    z[17]=static_cast<T>(5)- z[5];
    z[17]=z[5]*z[17];
    z[13]=z[17] + z[16] + z[13];
    z[13]=z[2]*z[13];
    z[16]=n<T>(1,8)*z[7];
    z[17]= - static_cast<T>(3)+ z[7];
    z[17]=z[17]*z[16];
    z[18]=z[14] - 1;
    z[19]= - z[18]*z[14];
    z[20]=3*z[5];
    z[19]= - z[20] + z[19];
    z[21]=n<T>(1,2)*z[6];
    z[19]=z[19]*z[21];
    z[13]=n<T>(1,4)*z[13] + z[19] + z[17] - static_cast<T>(1)- n<T>(3,4)*z[5];
    z[13]=z[2]*z[13];
    z[17]=z[15]*z[7];
    z[19]=static_cast<T>(1)- z[17];
    z[19]=z[6]*z[19];
    z[19]=z[17] - z[19];
    z[13]=z[13] + static_cast<T>(1)+ n<T>(5,8)*z[1] - n<T>(1,4)*z[19];
    z[13]=z[2]*z[13];
    z[18]=z[18]*z[7];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[5]*z[11];
    z[11]=z[18] - n<T>(1,2) + z[11];
    z[11]=z[2]*z[11];
    z[19]=static_cast<T>(1)+ z[1];
    z[22]=n<T>(3,2) - z[7];
    z[22]=z[7]*z[22];
    z[23]= - z[6]*z[17];
    z[11]=z[11] + z[23] + n<T>(1,2)*z[19] + z[22];
    z[11]=z[2]*z[11];
    z[19]=npow(z[7],2);
    z[22]=z[19]*z[21];
    z[17]=z[22] + static_cast<T>(1)+ z[17];
    z[17]=z[6]*z[17];
    z[11]=z[11] + z[17] + z[18] + n<T>(1,2) - z[1];
    z[11]=z[2]*z[11];
    z[14]=z[6]*z[14];
    z[14]=z[14] + z[15];
    z[14]=z[6]*z[14];
    z[17]=z[1] + z[15];
    z[11]=z[11] + n<T>(1,2)*z[17] + z[14];
    z[11]=z[3]*z[11];
    z[14]=n<T>(5,2) - z[7];
    z[14]=z[6]*z[14];
    z[14]=z[14] - n<T>(5,2)*z[1] - z[7];
    z[11]=n<T>(1,2)*z[11] + n<T>(1,4)*z[14] + z[13];
    z[11]=z[3]*z[11];
    z[13]=z[15]*z[16];
    z[14]= - 7*z[12] - z[19];
    z[14]=z[6]*z[14];
    z[15]=z[12]*z[2];
    z[16]=static_cast<T>(1)- n<T>(5,8)*z[5];
    z[16]=z[5]*z[16];
    z[13]=n<T>(7,8)*z[15] + n<T>(1,8)*z[14] + z[16] + z[13];
    z[13]=z[2]*z[13];
    z[14]= - n<T>(13,2)*z[5] + z[7];
    z[14]=z[6]*z[14];
    z[14]=z[14] - static_cast<T>(5)- n<T>(1,2)*z[5];
    z[13]=n<T>(1,4)*z[14] + z[13];
    z[13]=z[2]*z[13];
    z[14]=5*z[6] + static_cast<T>(5)+ 3*z[1];
    z[11]=z[11] + n<T>(1,8)*z[14] + z[13];
    z[11]=z[3]*z[11];
    z[13]=5*z[9] - z[4];
    z[14]=z[8]*npow(z[10],2);
    z[14]=z[10] + n<T>(3,2)*z[14];
    z[16]= - z[8]*z[9]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[16];
    z[16]=z[8]*z[10];
    z[16]= - static_cast<T>(1)+ n<T>(1,4)*z[16];
    z[16]=z[16]*z[20];
    z[14]=z[16] + n<T>(1,2)*z[14];
    z[14]=z[8]*z[14];
    z[14]= - static_cast<T>(3)+ z[14];
    z[14]=z[5]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[13]=z[13]*z[21];
    z[14]= - n<T>(3,4) + z[8];
    z[14]=z[5]*z[14];
    z[14]=n<T>(1,4) + z[14];
    z[14]=z[14]*z[20];
    z[12]= - n<T>(19,2)*z[12] - z[19];
    z[12]=z[12]*z[21];
    z[12]=n<T>(19,4)*z[15] + z[14] + z[12];
    z[12]=z[2]*z[12];
    z[14]= - static_cast<T>(1)+ z[8];
    z[14]=z[14]*z[20];
    z[14]=z[14] - 1;
    z[14]=z[8]*z[14];
    z[14]=static_cast<T>(3)+ z[14];
    z[14]=z[5]*z[14];
    z[15]=z[8] - z[1];
    z[15]= - n<T>(1,8)*z[15];
    z[15]=z[4]*z[15];
    z[12]=n<T>(1,2)*z[12] + z[13] + n<T>(1,8)*z[14] - static_cast<T>(1)+ z[15];

    r += z[11] + n<T>(1,2)*z[12];
 
    return r;
}

template double qg_2lha_r558(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r558(const std::array<dd_real,30>&);
#endif
