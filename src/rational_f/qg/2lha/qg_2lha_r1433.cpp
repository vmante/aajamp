#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1433(const std::array<T,30>& k) {
  T z[22];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=k[5];
    z[8]=k[13];
    z[9]=k[6];
    z[10]=k[8];
    z[11]=k[12];
    z[12]=n<T>(1,2)*z[6];
    z[13]=n<T>(5,2)*z[4];
    z[14]= - z[5]*z[13];
    z[14]=z[4] + z[14];
    z[12]=z[14]*z[12];
    z[14]=z[9] + z[10];
    z[15]=n<T>(9,2)*z[4] - n<T>(3,2) - z[14];
    z[16]=n<T>(1,2)*z[5];
    z[17]= - z[16] - 1;
    z[17]=z[9]*z[17];
    z[17]= - n<T>(3,4) + z[17];
    z[17]=z[5]*z[17];
    z[12]=z[12] + n<T>(1,2)*z[15] + z[17];
    z[12]=z[6]*z[12];
    z[15]=5*z[7] + z[9];
    z[15]=z[15]*z[16];
    z[17]=z[9] + z[7];
    z[15]=z[15] - z[17];
    z[15]=z[8]*z[15];
    z[18]=z[5]*z[9];
    z[14]=z[15] - z[18] - z[2] - static_cast<T>(1)- z[11] + z[14];
    z[15]=n<T>(1,2)*z[8];
    z[14]=z[14]*z[15];
    z[19]=n<T>(1,2)*z[2];
    z[20]=static_cast<T>(1)+ z[1];
    z[20]=z[20]*z[19];
    z[12]=z[14] + z[12] + n<T>(1,4)*z[18] + static_cast<T>(1)+ z[20];
    z[14]=z[18] - 1;
    z[20]= - 3*z[9] - z[14];
    z[20]=z[20]*z[16];
    z[21]=z[6]*npow(z[4],2);
    z[13]=z[21] + z[20] + static_cast<T>(1)+ z[13] - n<T>(3,2)*z[9];
    z[13]=z[16]*z[13];
    z[20]=static_cast<T>(1)- n<T>(1,2)*z[4];
    z[20]=z[4]*z[20];
    z[13]=z[20] + z[13] - n<T>(1,4)*z[17];
    z[13]=z[6]*z[13];
    z[17]= - z[8]*z[17]*z[16];
    z[20]=n<T>(1,2)*z[9];
    z[17]=z[17] + n<T>(3,2)*z[18] + z[20] - static_cast<T>(1)+ n<T>(1,2)*z[7];
    z[17]=z[17]*z[15];
    z[14]=z[14]*z[16];
    z[14]=z[14] - z[4] + static_cast<T>(1)+ n<T>(3,2)*z[1];
    z[13]=z[17] + n<T>(1,2)*z[14] + z[13];
    z[13]=z[3]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[3]*z[12];
    z[13]=z[20] + 1;
    z[14]= - z[9]*z[16];
    z[16]= - n<T>(5,2) - z[10];
    z[16]=z[6]*z[16];
    z[14]=z[16] + z[14] + n<T>(7,2)*z[7] - z[13];
    z[14]=z[6]*z[14];
    z[14]= - z[19] + z[14];
    z[13]=n<T>(5,2)*z[7] + z[13];
    z[16]= - z[5]*z[7];
    z[13]=n<T>(1,2)*z[13] + z[16];
    z[13]=z[8]*z[13];
    z[16]= - z[11] + n<T>(3,2)*z[7];
    z[13]=z[13] + n<T>(1,4)*z[2] + n<T>(1,2) - z[16];
    z[13]=z[8]*z[13];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[3]*z[12];
    z[13]= - static_cast<T>(3)- z[4];
    z[13]=z[7]*z[13]*z[15];
    z[13]=z[13] - n<T>(3,2)*z[10] + z[16];
    z[13]=z[13]*z[15];
    z[14]= - z[6]*z[7];
    z[13]=z[14] + z[13];
    z[12]=n<T>(1,2)*z[13] + z[12];

    r += n<T>(1,2)*z[12];
 
    return r;
}

template double qg_2lha_r1433(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1433(const std::array<dd_real,30>&);
#endif
