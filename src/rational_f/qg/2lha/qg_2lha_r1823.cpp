#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1823(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[6];
    z[7]=k[10];
    z[8]=k[15];
    z[9]=k[9];
    z[10]=k[12];
    z[11]=k[13];
    z[12]=k[14];
    z[13]=n<T>(1,3)*z[5];
    z[14]=z[13] + 1;
    z[15]=5*z[10];
    z[16]= - z[14]*z[15];
    z[17]=n<T>(1,3)*z[2];
    z[18]= - static_cast<T>(5)- z[17];
    z[13]=z[16] + n<T>(1,2)*z[18] - z[13];
    z[16]=n<T>(1,4)*z[10];
    z[13]=z[13]*z[16];
    z[18]=n<T>(1,2)*z[2];
    z[19]= - static_cast<T>(1)- z[18];
    z[19]=z[19]*z[15];
    z[19]=z[19] - static_cast<T>(1)+ n<T>(7,4)*z[2];
    z[19]=z[10]*z[19];
    z[20]=z[15] + 1;
    z[20]=z[4]*z[2]*z[20];
    z[21]=n<T>(1,2)*z[10];
    z[22]= - z[21]*z[20];
    z[19]=z[22] + z[18] + z[19];
    z[19]=z[4]*z[19];
    z[22]=z[11] - 1;
    z[23]=n<T>(1,3)*z[6];
    z[24]= - z[22]*z[23];
    z[25]=n<T>(1,8)*z[5];
    z[26]=3*z[8];
    z[27]=z[26] + n<T>(7,3)*z[6];
    z[27]=z[27]*z[25];
    z[28]=n<T>(3,4)*z[8];
    z[29]=n<T>(1,3)*z[11];
    z[30]= - z[29] + n<T>(3,8)*z[8];
    z[30]=z[2]*z[30];
    z[13]=n<T>(1,6)*z[19] + z[13] + z[27] + z[24] + z[28] + z[30];
    z[13]=z[4]*z[13];
    z[19]=z[6] + z[8];
    z[24]=z[5]*z[19];
    z[24]=n<T>(3,4)*z[24] + z[26] + n<T>(5,12)*z[6];
    z[27]=n<T>(1,2)*z[5];
    z[24]=z[24]*z[27];
    z[30]=n<T>(1,2)*z[3];
    z[31]= - z[30] - z[5];
    z[15]=z[31]*z[15];
    z[31]= - static_cast<T>(11)- 31*z[5];
    z[15]=n<T>(1,4)*z[31] + z[15];
    z[15]=z[10]*z[15];
    z[22]=z[22]*z[6];
    z[13]=z[13] + n<T>(1,6)*z[15] + z[24] + n<T>(9,8)*z[8] - n<T>(2,3)*z[22];
    z[13]=z[4]*z[13];
    z[15]=3*z[5];
    z[24]=z[8] + n<T>(1,2)*z[6];
    z[24]=z[24]*z[15];
    z[24]=z[24] + n<T>(9,2)*z[8] - z[23];
    z[24]=z[5]*z[24];
    z[31]= - static_cast<T>(1)+ z[8];
    z[31]=z[31]*z[15];
    z[31]= - n<T>(29,3) + z[31];
    z[27]=z[31]*z[27];
    z[31]=z[3] - 1;
    z[32]= - z[5] - z[31];
    z[32]=z[10]*z[32];
    z[32]=z[32] + z[3];
    z[27]=z[27] + n<T>(5,3)*z[32];
    z[27]=z[27]*z[16];
    z[22]=z[11] - z[22];
    z[13]=z[13] + z[27] + n<T>(1,3)*z[22] + n<T>(1,4)*z[24];
    z[13]=z[7]*z[13];
    z[22]=z[10]*z[5];
    z[14]=n<T>(5,3)*z[22] + z[14];
    z[14]=z[14]*z[16];
    z[16]= - n<T>(1,3)*z[20] + n<T>(5,2)*z[2];
    z[16]=z[10]*z[16];
    z[16]=z[17] + z[16];
    z[20]=n<T>(1,4)*z[4];
    z[16]=z[16]*z[20];
    z[22]=n<T>(2,3)*z[11];
    z[24]=n<T>(1,12)*z[5] + n<T>(7,8) - z[22];
    z[24]=z[6]*z[24];
    z[14]=z[16] + z[14] + z[28] + z[24];
    z[14]=z[4]*z[14];
    z[16]= - z[5]*z[28];
    z[16]=n<T>(5,3) + z[16];
    z[16]=z[5]*z[16];
    z[24]= - z[3] + z[5];
    z[24]=z[10]*z[24];
    z[16]=n<T>(5,6)*z[24] + n<T>(13,12)*z[31] + z[16];
    z[16]=z[16]*z[21];
    z[15]=z[19]*z[15];
    z[15]=z[15] - z[26] - n<T>(29,3)*z[6];
    z[19]= - z[15]*z[25];
    z[24]=n<T>(1,8) - z[22];
    z[24]=z[6]*z[24];
    z[13]=z[13] + z[14] + z[16] + z[19] + z[22] + z[24];
    z[13]=z[7]*z[13];
    z[14]= - z[2]*z[1];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[17]*z[3]*z[14];
    z[16]=n<T>(1,3)*z[12];
    z[19]=z[16] - 1;
    z[22]=z[19] + z[17];
    z[22]=z[2]*z[12]*z[22];
    z[19]=z[12]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[9]*z[19];
    z[19]=z[19] + z[22];
    z[19]=z[6]*z[19];
    z[22]=z[5]*z[9];
    z[23]= - z[22]*z[23];
    z[24]=z[22]*z[10];
    z[26]= - n<T>(5,3)*z[24] - z[9] - n<T>(1,3)*z[22];
    z[26]=z[10]*z[26];
    z[14]=z[26] + z[23] + z[14] + z[19];
    z[14]=z[14]*z[20];
    z[19]=z[12] - 5;
    z[20]= - z[19]*z[16];
    z[23]=n<T>(1,2) + n<T>(1,3)*z[1];
    z[23]=z[3]*z[23];
    z[20]=z[20] - n<T>(5,3) + z[23];
    z[23]= - static_cast<T>(1)+ z[1];
    z[23]=z[23]*z[30];
    z[23]=z[23] - n<T>(1,2);
    z[23]=z[1]*z[23];
    z[23]= - n<T>(1,2)*z[12] + static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[17];
    z[20]=n<T>(1,2)*z[20] + z[23];
    z[18]=z[20]*z[18];
    z[20]=n<T>(1,4)*z[9];
    z[16]=z[16] - n<T>(5,2);
    z[16]=z[16]*z[12];
    z[23]=n<T>(11,2) + z[16];
    z[23]=z[23]*z[20];
    z[26]= - static_cast<T>(1)+ z[12];
    z[17]=z[26]*z[17];
    z[16]=z[17] + n<T>(5,3) + z[16];
    z[16]=z[2]*z[16];
    z[16]=n<T>(1,4)*z[16] - z[29] + z[23];
    z[16]=z[6]*z[16];
    z[17]= - z[12]*z[19]*z[20];
    z[17]=z[11] + z[17];
    z[15]=z[25]*z[9]*z[15];
    z[19]=z[22]*z[28];
    z[19]= - n<T>(5,3)*z[9] + z[19];
    z[19]=z[5]*z[19];
    z[19]= - n<T>(5,6)*z[24] + n<T>(13,12)*z[9] + z[19];
    z[19]=z[19]*z[21];

    r += z[13] + z[14] + z[15] + z[16] + n<T>(1,3)*z[17] + z[18] + z[19];
 
    return r;
}

template double qg_2lha_r1823(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1823(const std::array<dd_real,30>&);
#endif
