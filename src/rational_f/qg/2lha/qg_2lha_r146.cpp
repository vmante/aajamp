#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r146(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[2];
    z[5]=k[11];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=k[9];
    z[9]=11*z[6] - static_cast<T>(5)- 3*z[7];
    z[10]=static_cast<T>(1)- n<T>(3,2)*z[7];
    z[10]=z[8]*z[6]*z[10];
    z[9]=n<T>(1,2)*z[9] + z[10];
    z[9]=z[8]*z[9];
    z[10]=z[6]*z[4];
    z[11]=static_cast<T>(5)- 7*z[4];
    z[11]=z[4]*z[11];
    z[11]=z[11] - 3*z[10];
    z[12]=n<T>(1,2)*z[3];
    z[11]=z[11]*z[12];
    z[13]=n<T>(3,2)*z[6];
    z[11]=z[11] - z[8] + z[13] + static_cast<T>(1)+ n<T>(3,2)*z[4];
    z[11]=z[3]*z[11];
    z[9]=z[11] + n<T>(3,2) + z[9];
    z[11]= - z[13] + static_cast<T>(1)+ z[7];
    z[11]=z[6]*z[11];
    z[13]=n<T>(1,2)*z[8];
    z[14]=z[7]*npow(z[6],2)*z[13];
    z[11]=z[11] + z[14];
    z[11]=z[8]*z[11];
    z[14]=z[4] - 1;
    z[15]=z[14] + z[6];
    z[15]=z[12]*z[15]*npow(z[4],2);
    z[10]= - z[10] + z[15];
    z[10]=z[3]*z[10];
    z[10]=z[10] - z[6] + z[11];
    z[10]=z[5]*z[10];
    z[9]=n<T>(1,4)*z[9] + z[10];
    z[9]=z[5]*z[9];
    z[10]=z[8] + n<T>(1,2)*z[14] - z[6];
    z[10]=z[10]*z[12];
    z[11]=static_cast<T>(1)- z[13];
    z[11]=z[8]*z[11];
    z[10]=z[10] - n<T>(5,6) + z[11];
    z[10]=z[3]*z[10];
    z[11]=n<T>(1,3) - 3*z[8];
    z[11]=z[8]*z[11];
    z[10]=n<T>(1,4)*z[11] + z[10];
    z[9]=n<T>(1,2)*z[10] + z[9];
    z[9]=z[5]*z[9];
    z[10]=z[6] - static_cast<T>(1)- z[1];
    z[11]=n<T>(1,3)*z[2];
    z[10]=z[11]*z[10];
    z[10]=n<T>(1,2) + z[10];
    z[10]=z[3]*z[10];
    z[10]=z[11] + z[10];
    z[10]=z[10]*z[12];

    r += z[9] + z[10];
 
    return r;
}

template double qg_2lha_r146(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r146(const std::array<dd_real,30>&);
#endif
