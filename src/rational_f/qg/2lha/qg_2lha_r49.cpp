#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r49(const std::array<T,30>& k) {
  T z[20];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[11];
    z[7]=k[12];
    z[8]=n<T>(1,2)*z[3];
    z[9]=z[8] - n<T>(2,3);
    z[9]=z[9]*z[3];
    z[9]=z[9] + n<T>(1,6);
    z[10]=npow(z[3],2);
    z[9]=z[9]*z[10];
    z[11]=npow(z[3],3);
    z[12]=n<T>(1,3) - z[8];
    z[12]=z[12]*z[11];
    z[13]=z[5]*npow(z[3],4);
    z[12]=z[12] - n<T>(1,6)*z[13];
    z[12]=z[5]*z[12];
    z[12]= - z[9] + z[12];
    z[12]=z[5]*z[12];
    z[14]=z[8] - 1;
    z[14]=z[14]*z[3];
    z[14]=z[14] + n<T>(1,2);
    z[15]=z[14]*z[10];
    z[12]= - n<T>(1,3)*z[15] + z[12];
    z[12]=z[4]*z[12];
    z[16]=z[3] - 1;
    z[10]=z[16]*z[10];
    z[16]=z[11]*z[5];
    z[17]= - z[10] - n<T>(1,2)*z[16];
    z[17]=z[5]*z[17];
    z[14]= - z[3]*z[14];
    z[14]=z[14] + z[17];
    z[17]=n<T>(1,3)*z[5];
    z[14]=z[14]*z[17];
    z[12]=z[12] + z[14];
    z[12]=z[7]*z[12];
    z[14]=z[1] - 1;
    z[18]=n<T>(1,3)*z[3];
    z[19]=z[18] - 1;
    z[19]=z[19]*z[3];
    z[19]=z[19] + 1;
    z[19]=z[19]*z[3];
    z[19]=z[19] - n<T>(1,3);
    z[8]=z[14]*z[19]*z[8];
    z[14]= - z[17]*z[15];
    z[8]=z[14] + z[8];
    z[8]=z[2]*z[8];
    z[11]=z[11] - z[13];
    z[11]=z[4]*z[11];
    z[10]=z[11] - z[10] - z[16];
    z[10]=z[6]*z[10]*npow(z[5],2);
    z[11]= - n<T>(5,2) + z[3];
    z[11]=z[3]*z[11];
    z[11]=static_cast<T>(2)+ z[11];
    z[11]=z[3]*z[11];
    z[11]= - n<T>(1,2) + z[11];
    z[11]=z[11]*z[18];
    z[9]=z[5]*z[9];

    r += z[8] + z[9] + n<T>(1,6)*z[10] + z[11] + z[12];
 
    return r;
}

template double qg_2lha_r49(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r49(const std::array<dd_real,30>&);
#endif
