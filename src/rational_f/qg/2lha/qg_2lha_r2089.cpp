#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2089(const std::array<T,30>& k) {
  T z[57];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[24];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=k[5];
    z[8]=k[12];
    z[9]=k[27];
    z[10]=n<T>(1,2)*z[5];
    z[11]= - static_cast<T>(25)- 11*z[5];
    z[11]=z[11]*z[10];
    z[12]=z[6]*z[5];
    z[13]=3*z[12];
    z[14]=z[10] + 1;
    z[14]=z[14]*z[5];
    z[15]=z[14] + n<T>(1,2);
    z[16]= - z[15]*z[13];
    z[11]=z[16] - static_cast<T>(7)+ z[11];
    z[11]=z[6]*z[11];
    z[16]=5*z[5];
    z[17]=n<T>(1,2) + z[16];
    z[17]=z[17]*z[10];
    z[18]=3*z[5];
    z[19]=z[18]*z[7];
    z[20]=z[5] + n<T>(1,4);
    z[21]=z[20]*z[19];
    z[17]=z[17] + z[21];
    z[21]=n<T>(1,2)*z[7];
    z[17]=z[17]*z[21];
    z[22]=z[16] + 1;
    z[23]=z[22]*z[10];
    z[24]=z[7]*z[5];
    z[25]=n<T>(3,2) + z[5];
    z[25]=z[5]*z[25];
    z[25]=n<T>(1,2) + z[25];
    z[25]=z[25]*z[24];
    z[23]=z[23] + z[25];
    z[23]=z[7]*z[23];
    z[25]=z[15]*z[12];
    z[26]=npow(z[5],2);
    z[23]=z[23] + n<T>(1,2)*z[26] - z[25];
    z[23]=z[3]*z[23];
    z[27]=n<T>(1,4)*z[5];
    z[28]=z[27] - 1;
    z[11]=n<T>(3,2)*z[23] + z[17] + n<T>(1,4)*z[11] - z[28];
    z[11]=z[3]*z[11];
    z[17]=static_cast<T>(11)+ z[16];
    z[17]=z[17]*z[10];
    z[17]=static_cast<T>(3)+ z[17];
    z[17]=n<T>(1,2)*z[17] - z[25];
    z[17]=z[6]*z[17];
    z[23]=z[5] + n<T>(7,4);
    z[17]=z[17] - z[23];
    z[17]=z[6]*z[17];
    z[29]=z[5] + n<T>(1,2);
    z[30]=z[29]*z[24];
    z[30]=z[5] + z[30];
    z[30]=z[7]*z[30];
    z[17]=z[17] + z[30];
    z[11]=n<T>(1,2)*z[17] + z[11];
    z[11]=z[3]*z[11];
    z[17]=z[5] + 3;
    z[30]=z[17]*z[10];
    z[31]= - static_cast<T>(1)- z[30];
    z[25]=n<T>(3,2)*z[31] - z[25];
    z[25]=z[6]*z[25];
    z[23]=z[25] + z[23];
    z[23]=z[6]*z[23];
    z[25]=npow(z[7],2);
    z[31]= - z[25]*z[10];
    z[32]=z[25] + 1;
    z[32]=z[10]*z[32];
    z[33]=z[15]*z[5];
    z[34]=npow(z[6],2)*z[33];
    z[32]=z[34] + z[32];
    z[34]=n<T>(1,2)*z[8];
    z[32]=z[32]*z[34];
    z[23]=z[32] + z[31] - n<T>(1,4) + z[23];
    z[23]=z[23]*z[34];
    z[31]=z[15]*z[6];
    z[32]=z[5] + 1;
    z[35]=z[31] - n<T>(1,2)*z[32];
    z[36]=z[6]*z[35];
    z[37]=n<T>(3,2)*z[5];
    z[38]=z[37] + 1;
    z[19]=z[38]*z[19];
    z[19]=z[10] + z[19];
    z[19]=z[7]*z[19];
    z[19]=z[36] + z[19];
    z[36]=z[18] + 1;
    z[24]=z[36]*z[24];
    z[24]=z[26] + z[24];
    z[24]=z[24]*z[21];
    z[24]=z[24] + n<T>(1,2) - z[31];
    z[39]=3*z[3];
    z[24]=z[24]*z[39];
    z[19]=n<T>(1,2)*z[19] + z[24];
    z[19]=z[3]*z[19];
    z[24]=z[25]*z[5];
    z[19]=z[24] + z[19];
    z[19]=z[3]*z[19];
    z[25]=n<T>(1,2)*z[6];
    z[35]=z[35]*npow(z[8],2)*z[25];
    z[40]=z[3]*z[32];
    z[40]=n<T>(3,2) + z[40];
    z[41]=npow(z[3],2);
    z[40]=z[41]*z[40];
    z[42]=npow(z[3],3);
    z[43]=z[42]*z[4];
    z[40]=z[40] + z[43];
    z[40]=z[4]*z[24]*z[40];
    z[19]=n<T>(3,2)*z[40] + z[19] + z[35];
    z[35]=n<T>(1,2)*z[4];
    z[19]=z[19]*z[35];
    z[11]=z[19] + z[23] + n<T>(5,8)*z[24] + z[11];
    z[11]=z[4]*z[11];
    z[19]=npow(z[5],3);
    z[23]= - z[28]*z[19];
    z[40]=n<T>(1,4)*z[7];
    z[44]=npow(z[5],4);
    z[45]= - z[44]*z[40];
    z[23]=z[23] + z[45];
    z[23]=z[7]*z[23];
    z[45]=z[5] - 3;
    z[46]=n<T>(1,4)*z[26];
    z[45]=z[45]*z[46];
    z[23]=z[45] + z[23];
    z[23]=z[3]*z[23];
    z[47]= - static_cast<T>(7)- z[10];
    z[47]=z[47]*z[19];
    z[48]=z[10] + 3;
    z[49]=z[19]*z[7];
    z[50]= - z[48]*z[49];
    z[47]=z[47] + z[50];
    z[47]=z[47]*z[40];
    z[47]= - z[19] + z[47];
    z[47]=z[8]*z[47];
    z[50]=z[5]*z[20];
    z[31]=z[31] - n<T>(3,4) + z[50];
    z[31]=z[6]*z[31];
    z[50]=z[21] + 1;
    z[50]=z[7]*z[50];
    z[50]=z[50] + n<T>(1,2);
    z[44]=z[2]*z[44]*z[50];
    z[34]= - z[34]*z[44];
    z[50]= - static_cast<T>(1)+ z[37];
    z[50]=z[5]*z[50];
    z[50]=n<T>(1,2) + z[50];
    z[23]=z[34] + z[47] + z[23] + n<T>(1,2)*z[50] + z[31];
    z[23]=z[2]*z[23];
    z[31]=3*z[26];
    z[34]=z[5] - 1;
    z[47]= - z[34]*z[31];
    z[50]= - z[32]*z[49];
    z[47]=z[47] + z[50];
    z[47]=z[7]*z[47];
    z[31]=z[31] + z[47];
    z[47]=n<T>(1,4)*z[3];
    z[31]=z[31]*z[47];
    z[50]= - static_cast<T>(2)- n<T>(5,8)*z[5];
    z[50]=z[50]*z[26];
    z[30]= - static_cast<T>(3)- z[30];
    z[30]=z[7]*z[30]*z[46];
    z[30]=z[50] + z[30];
    z[30]=z[7]*z[30];
    z[30]= - n<T>(5,4)*z[26] + z[30];
    z[30]=z[8]*z[30];
    z[37]= - z[38]*z[37];
    z[37]=static_cast<T>(5)+ z[37];
    z[50]=z[17]*z[5];
    z[51]= - static_cast<T>(2)- z[50];
    z[51]=z[6]*z[51];
    z[37]=n<T>(1,2)*z[37] + z[51];
    z[37]=z[6]*z[37];
    z[51]=z[26]*z[7];
    z[52]= - static_cast<T>(3)+ z[16];
    z[23]=z[23] + z[30] + z[31] + z[51] + n<T>(1,4)*z[52] + z[37];
    z[23]=z[2]*z[23];
    z[30]=z[5] + n<T>(7,2);
    z[31]=z[5]*z[30];
    z[31]=n<T>(5,2) + z[31];
    z[31]=z[31]*z[27];
    z[37]=z[30]*z[27];
    z[37]=z[37] + 1;
    z[37]=z[37]*z[5];
    z[37]=z[37] + n<T>(3,8);
    z[52]= - z[37]*z[13];
    z[31]=z[31] + z[52];
    z[31]=z[6]*z[31];
    z[52]=z[29]*z[26];
    z[50]=z[50] + 3;
    z[50]=z[50]*z[5];
    z[50]=z[50] + 1;
    z[53]=z[6]*z[50]*z[10];
    z[54]=z[33] - z[53];
    z[54]=z[6]*z[54];
    z[52]=z[52] + z[54];
    z[52]=z[7]*z[52];
    z[54]=n<T>(1,8)*z[5];
    z[55]=z[22]*z[54];
    z[31]=n<T>(3,4)*z[52] + z[55] + z[31];
    z[31]=z[7]*z[31];
    z[52]=z[32]*z[18];
    z[52]=static_cast<T>(1)+ z[52];
    z[52]=z[5]*z[52];
    z[55]= - z[50]*z[12];
    z[52]=z[52] + z[55];
    z[52]=z[52]*z[40];
    z[30]=z[30]*z[10];
    z[30]=z[30] + 2;
    z[30]=z[30]*z[5];
    z[30]=z[30] + n<T>(3,4);
    z[30]=z[30]*z[12];
    z[55]=n<T>(5,4) + 2*z[5];
    z[55]=z[5]*z[55];
    z[55]=n<T>(1,2) + z[55];
    z[55]=z[5]*z[55];
    z[52]=z[52] + z[55] - z[30];
    z[52]=z[7]*z[52];
    z[55]=z[27] + 1;
    z[55]=z[55]*z[5];
    z[55]=z[55] + n<T>(5,4);
    z[55]=z[55]*z[5];
    z[55]=z[55] + n<T>(1,2);
    z[56]=z[55]*z[12];
    z[45]=z[52] + z[45] - z[56];
    z[45]=z[45]*z[39];
    z[13]= - z[55]*z[13];
    z[52]= - n<T>(5,4) - z[5];
    z[52]=z[5]*z[52];
    z[52]= - n<T>(1,4) + z[52];
    z[52]=z[5]*z[52];
    z[13]=z[52] + z[13];
    z[13]=z[6]*z[13];
    z[52]=static_cast<T>(3)+ z[27];
    z[52]=z[5]*z[52];
    z[13]=z[52] + z[13];
    z[13]=z[45] + n<T>(1,2)*z[13] + z[31];
    z[13]=z[3]*z[13];
    z[31]=z[5] + n<T>(5,2);
    z[45]=z[31]*z[5];
    z[45]=z[45] + n<T>(3,2);
    z[52]=n<T>(3,4)*z[5];
    z[55]=z[45]*z[52];
    z[30]=z[30] - z[55];
    z[30]=z[30]*z[6];
    z[31]= - z[31]*z[18];
    z[31]= - static_cast<T>(5)+ z[31];
    z[31]=z[31]*z[27];
    z[31]=z[31] - z[30];
    z[31]=z[6]*z[31];
    z[55]=n<T>(1,4)*z[12];
    z[50]=z[50]*z[55];
    z[33]=z[50] - z[33];
    z[33]=z[33]*z[6];
    z[14]= - n<T>(3,2) - z[14];
    z[14]=z[5]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[14]*z[10];
    z[14]=z[14] - z[33];
    z[14]=z[6]*z[14];
    z[29]=z[29]*z[10];
    z[14]=z[29] + z[14];
    z[14]=z[7]*z[14];
    z[29]=n<T>(5,8) + z[5];
    z[29]=z[5]*z[29];
    z[14]=z[14] + z[29] + z[31];
    z[14]=z[7]*z[14];
    z[29]=z[32]*z[5];
    z[31]= - z[56] + n<T>(3,8)*z[29];
    z[31]=z[31]*z[6];
    z[50]= - n<T>(33,2) - 7*z[5];
    z[50]=z[5]*z[50];
    z[50]= - static_cast<T>(9)+ z[50];
    z[50]=n<T>(1,4)*z[50] + z[31];
    z[50]=z[6]*z[50];
    z[13]=z[13] + z[14] + n<T>(1,2)*z[17] + z[50];
    z[13]=z[3]*z[13];
    z[14]= - static_cast<T>(9)- z[16];
    z[14]=z[5]*z[14];
    z[14]= - static_cast<T>(5)+ z[14];
    z[14]=z[14]*z[54];
    z[14]=z[14] - z[30];
    z[14]=z[6]*z[14];
    z[16]= - static_cast<T>(1)- z[29];
    z[16]=z[5]*z[16];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[16]*z[54];
    z[16]=z[16] - z[33];
    z[16]=z[6]*z[16];
    z[17]= - n<T>(3,4) - z[5];
    z[17]=z[17]*z[10];
    z[16]=z[17] + z[16];
    z[16]=z[7]*z[16];
    z[14]=z[16] - n<T>(3,8)*z[5] + z[14];
    z[14]=z[7]*z[14];
    z[16]= - z[45]*z[10];
    z[17]=z[37]*z[12];
    z[16]=z[16] + z[17];
    z[16]=z[6]*z[16];
    z[15]= - z[15]*z[18];
    z[15]=z[15] + z[53];
    z[15]=z[6]*z[15];
    z[17]=z[5]*z[38];
    z[15]=z[17] + z[15];
    z[15]=z[15]*z[40];
    z[17]=z[32]*z[27];
    z[15]=z[15] + z[17] + z[16];
    z[15]=z[7]*z[15];
    z[16]= - z[32]*z[10];
    z[16]=z[16] + z[56];
    z[16]=z[6]*z[16];
    z[16]=z[46] + z[16];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[8]*z[15];
    z[16]= - z[20]*z[10];
    z[16]=z[16] + z[31];
    z[16]=z[6]*z[16];
    z[14]=z[15] + z[14] - z[52] + z[16];
    z[14]=z[8]*z[14];
    z[15]=z[32]*z[39];
    z[15]= - n<T>(5,2) + z[15];
    z[15]=z[15]*z[41]*z[35];
    z[16]=z[10] - 1;
    z[16]=z[16]*z[3];
    z[17]=z[18]*z[16];
    z[20]=z[10] - 3;
    z[17]=z[17] - z[20];
    z[17]=z[3]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[3]*z[17];
    z[15]=z[17] + z[15];
    z[16]=z[10]*z[16];
    z[17]=z[32]*z[6];
    z[29]=n<T>(7,4) - z[17];
    z[29]=z[6]*z[29];
    z[16]=z[16] + n<T>(1,2)*z[34] + z[29];
    z[16]=z[2]*z[16];
    z[29]=z[36]*z[47];
    z[30]=static_cast<T>(2)+ z[5];
    z[30]=z[6]*z[30];
    z[30]= - n<T>(7,2) + z[30];
    z[30]=z[6]*z[30];
    z[16]=z[16] + z[29] + n<T>(3,4) + z[30];
    z[16]=z[2]*z[16];
    z[29]=z[34]*z[39];
    z[29]= - n<T>(5,2) + z[29];
    z[29]=z[29]*z[41];
    z[30]=z[3]*npow(z[2],2);
    z[30]=3*z[42] + z[30];
    z[30]=z[1]*z[30];
    z[29]=z[30] + z[29] + 3*z[43];
    z[30]=z[34]*z[47];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[6]*z[25];
    z[25]=z[30] + n<T>(1,4) + z[25];
    z[25]=z[2]*z[25];
    z[25]=n<T>(3,4)*z[3] + z[25];
    z[25]=z[2]*z[25];
    z[25]=z[25] + n<T>(1,4)*z[29];
    z[25]=z[1]*z[25];
    z[15]=z[25] + n<T>(1,2)*z[15] + z[16];
    z[15]=z[1]*z[15];
    z[16]=z[46]*z[17];
    z[17]=z[19]*z[17];
    z[25]=static_cast<T>(1)- n<T>(11,2)*z[5];
    z[25]=z[5]*z[25];
    z[17]=z[25] - n<T>(1,2)*z[17];
    z[17]=z[17]*z[21];
    z[25]= - z[5]*z[28];
    z[16]=z[17] + z[25] + z[16];
    z[16]=z[7]*z[16];
    z[17]= - z[19] - z[49];
    z[17]=z[21]*z[20]*z[17];
    z[17]=z[17] + z[44];
    z[17]=z[2]*z[17];
    z[19]= - static_cast<T>(7)- z[5];
    z[19]=z[19]*z[10];
    z[19]=static_cast<T>(3)+ z[19];
    z[19]=z[7]*z[19];
    z[19]=z[48] + z[19];
    z[19]=z[19]*z[51];
    z[19]= - z[26] + z[19];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[2]*z[17];
    z[18]= - static_cast<T>(5)+ z[18];
    z[18]=z[7]*z[18]*z[27];
    z[18]= - z[5] + z[18];
    z[18]=z[7]*z[18];
    z[19]=z[24]*z[4];
    z[18]=z[18] + n<T>(3,4)*z[19];
    z[18]=z[4]*z[18];
    z[19]=z[6]*z[46];
    z[16]=z[17] + z[18] + z[19] + z[16];
    z[16]=z[9]*z[16];
    z[17]= - z[32]*z[55];
    z[12]= - z[32]*z[12];
    z[18]=z[5]*z[22];
    z[12]=z[18] + z[12];
    z[12]=z[12]*z[40];
    z[12]=z[12] + z[5] + z[17];
    z[12]=z[12]*z[21];
    z[17]=z[5]*z[48];
    z[17]=n<T>(11,4) + z[17];
    z[17]=z[6]*z[17];
    z[10]=z[17] - static_cast<T>(4)- z[10];
    z[10]=z[6]*z[10];

    r += n<T>(5,8) + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + n<T>(1,2)*
      z[16] + z[23];
 
    return r;
}

template double qg_2lha_r2089(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2089(const std::array<dd_real,30>&);
#endif
