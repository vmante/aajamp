#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1379(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[29];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[11];
    z[7]=k[4];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=n<T>(1,2)*z[7];
    z[11]=static_cast<T>(5)+ 37*z[5];
    z[11]=z[11]*z[10];
    z[12]= - z[8] + 3*z[5];
    z[13]=21*z[8];
    z[14]=z[7]*z[13];
    z[12]=n<T>(37,2)*z[12] + z[14];
    z[12]=z[4]*z[12];
    z[14]=z[5] - 1;
    z[15]=z[14] - z[2];
    z[15]=z[15]*z[7];
    z[16]=npow(z[2],2);
    z[15]=z[15] + z[16];
    z[17]= - z[4]*z[8];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[4]*z[7]*z[17];
    z[17]=z[17] + z[15];
    z[17]=z[6]*z[17];
    z[18]=37*z[14];
    z[11]=n<T>(37,2)*z[17] + z[12] - z[18] + z[11];
    z[11]=z[4]*z[11];
    z[11]= - 21*z[15] + z[11];
    z[12]=n<T>(1,2)*z[6];
    z[11]=z[11]*z[12];
    z[15]=n<T>(1,2)*z[2];
    z[17]=z[15] - 1;
    z[19]=37*z[2];
    z[17]= - z[17]*z[19];
    z[17]=n<T>(131,2) + z[17];
    z[17]=z[2]*z[17];
    z[20]= - static_cast<T>(1)+ z[2];
    z[20]=z[20]*z[19];
    z[21]=n<T>(37,2)*z[7];
    z[22]= - z[2]*z[21];
    z[20]=z[22] + 63*z[5] - static_cast<T>(63)+ z[20];
    z[20]=z[7]*z[20];
    z[17]=z[17] + z[20];
    z[20]= - z[21] - n<T>(131,2)*z[5] - z[13] + z[19];
    z[20]=z[7]*z[20];
    z[21]=z[4]*z[5];
    z[22]=z[21] + static_cast<T>(1)- n<T>(1,2)*z[8];
    z[16]=n<T>(1,4)*z[20] + n<T>(79,4)*z[5] - n<T>(37,8)*z[16] - 21*z[22];
    z[16]=z[4]*z[16];
    z[11]=z[11] + n<T>(1,4)*z[17] + z[16];
    z[11]=z[11]*z[12];
    z[12]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[15]=z[15] - z[12];
    z[15]=z[15]*z[19];
    z[16]= - static_cast<T>(5)+ 37*z[1];
    z[13]=z[15] + n<T>(1,2)*z[16] - z[13];
    z[10]= - z[10] + z[12];
    z[10]=z[5]*z[10];
    z[10]= - n<T>(1,2)*z[21] - n<T>(1,2) + z[10];
    z[10]=z[4]*z[10];
    z[12]=z[1] + z[2] + 1;
    z[12]=n<T>(37,2)*z[12] - 21*z[5];
    z[15]=n<T>(95,16) + z[9];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(37,16) + z[15];
    z[15]=z[7]*z[15];
    z[10]=n<T>(37,8)*z[10] + n<T>(1,8)*z[12] + z[15];
    z[10]=z[4]*z[10];
    z[12]=z[7]*z[9]*z[14];
    z[12]=z[12] - n<T>(21,8)*z[5] - static_cast<T>(1)- n<T>(37,16)*z[2];
    z[12]=z[7]*z[12];
    z[10]=z[11] + z[10] + n<T>(1,8)*z[13] + z[12];
    z[10]=z[3]*z[10];
    z[11]=npow(z[5],2);
    z[12]= - z[6]*z[5]*z[18];
    z[12]=n<T>(47,2)*z[11] + z[12];
    z[12]=z[6]*z[12];
    z[13]= - static_cast<T>(1)- z[5];
    z[13]=z[5]*z[13];
    z[12]=n<T>(37,2)*z[13] + z[12];
    z[12]=z[6]*z[12];
    z[11]=z[9]*z[11];
    z[10]=z[10] + z[11] + n<T>(1,8)*z[12];

    r += n<T>(1,8)*z[10];
 
    return r;
}

template double qg_2lha_r1379(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1379(const std::array<dd_real,30>&);
#endif
