#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1570(const std::array<T,30>& k) {
  T z[62];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=k[9];
    z[8]=k[11];
    z[9]=k[3];
    z[10]=k[12];
    z[11]=z[7] - n<T>(1,2);
    z[12]=npow(z[7],3);
    z[11]=z[11]*z[12];
    z[13]=n<T>(1,2)*z[5];
    z[14]=npow(z[7],4);
    z[15]=z[13]*z[14];
    z[16]= - z[11] - z[15];
    z[16]=z[5]*z[16];
    z[17]=npow(z[7],2);
    z[18]=n<T>(1,2)*z[17];
    z[19]=3*z[7];
    z[20]=static_cast<T>(1)- z[19];
    z[20]=z[7]*z[20];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[20]*z[18];
    z[16]=z[20] + z[16];
    z[16]=z[5]*z[16];
    z[20]=7*z[7];
    z[21]=n<T>(15,2) - z[20];
    z[21]=z[21]*z[17];
    z[21]=n<T>(1,2) + z[21];
    z[22]=n<T>(5,4)*z[7];
    z[23]=z[22] - 3;
    z[24]=z[3]*z[7];
    z[25]=z[23]*z[24];
    z[16]=z[16] + n<T>(1,2)*z[21] + z[25];
    z[16]=z[16]*z[13];
    z[21]=n<T>(3,2)*z[7];
    z[25]=z[21] - 1;
    z[26]=z[25]*z[12];
    z[27]=z[26] + z[15];
    z[27]=z[27]*z[5];
    z[28]=z[7] - n<T>(3,2);
    z[28]=z[28]*z[7];
    z[29]= - n<T>(1,2) - z[28];
    z[29]=z[29]*z[17];
    z[29]=z[29] - z[27];
    z[30]=npow(z[5],3);
    z[29]=z[29]*z[30];
    z[31]=z[7] - 1;
    z[32]=z[31]*z[12];
    z[33]= - z[32] - z[15];
    z[33]=z[5]*z[33];
    z[34]=n<T>(1,2)*z[7];
    z[35]=z[34] - 1;
    z[36]=z[35]*z[7];
    z[37]=z[36] + n<T>(1,2);
    z[38]=z[37]*z[17];
    z[33]= - z[38] + z[33];
    z[39]=npow(z[5],4);
    z[40]=n<T>(1,2)*z[8];
    z[33]=z[33]*z[39]*z[40];
    z[29]=z[29] + z[33];
    z[29]=z[29]*z[40];
    z[33]=n<T>(1,8)*z[7];
    z[41]=9*z[7];
    z[42]= - static_cast<T>(37)+ z[41];
    z[42]=z[7]*z[42];
    z[42]=n<T>(101,2) + z[42];
    z[42]=z[42]*z[33];
    z[43]=n<T>(1,4)*z[7];
    z[44]=5*z[7];
    z[45]= - static_cast<T>(17)+ z[44];
    z[45]=z[45]*z[43];
    z[45]=static_cast<T>(3)+ z[45];
    z[46]=n<T>(1,2)*z[3];
    z[45]=z[45]*z[46];
    z[16]=z[29] + z[16] + z[45] - static_cast<T>(2)+ z[42];
    z[16]=z[8]*z[16];
    z[29]= - n<T>(1,2) - z[7];
    z[29]=z[29]*z[34];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[17];
    z[42]=z[7] - 3;
    z[42]=z[42]*z[7];
    z[45]=z[42] + 3;
    z[45]=z[45]*z[7];
    z[45]=z[45] - 1;
    z[47]=z[45]*z[24];
    z[48]=z[19] - 5;
    z[49]=z[7]*z[48];
    z[49]=n<T>(1,4) + z[49];
    z[49]=z[7]*z[49];
    z[49]=n<T>(5,2) + z[49];
    z[49]=z[7]*z[49];
    z[49]= - n<T>(3,4) + z[49];
    z[49]=n<T>(1,2)*z[49] - z[47];
    z[49]=z[3]*z[49];
    z[29]=z[49] - n<T>(1,4) + z[29];
    z[29]=z[3]*z[29];
    z[49]=n<T>(1,4)*z[17];
    z[50]=z[7]*z[31];
    z[50]=static_cast<T>(1)+ z[50];
    z[50]=z[50]*z[49];
    z[29]=z[50] + z[29];
    z[29]=z[3]*z[29];
    z[50]=n<T>(3,4)*z[32];
    z[51]=z[17]*z[3];
    z[52]= - z[37]*z[51];
    z[52]=z[50] + z[52];
    z[52]=z[3]*z[52];
    z[52]= - n<T>(1,4)*z[12] + z[52];
    z[52]=z[3]*z[52];
    z[52]= - n<T>(1,4)*z[14] + z[52];
    z[53]=z[5]*z[3];
    z[52]=z[52]*z[53];
    z[29]=z[29] + z[52];
    z[29]=z[5]*z[29];
    z[23]=z[7]*z[23];
    z[23]=static_cast<T>(2)+ z[23];
    z[23]=z[23]*z[17];
    z[52]=z[34] - 2;
    z[52]=z[52]*z[7];
    z[52]=z[52] + 3;
    z[52]=z[52]*z[7];
    z[52]=z[52] - 2;
    z[52]=z[52]*z[7];
    z[52]=z[52] + n<T>(1,2);
    z[52]=z[52]*z[3];
    z[23]= - z[52] - n<T>(1,4) + z[23];
    z[23]=z[3]*z[23];
    z[23]= - z[50] + z[23];
    z[23]=z[5]*z[23];
    z[50]=z[7] - 5;
    z[54]=z[50]*z[34];
    z[54]=z[54] + 5;
    z[54]=z[54]*z[7];
    z[54]=z[54] - 5;
    z[55]= - z[7]*z[54];
    z[55]= - static_cast<T>(2)+ z[55];
    z[55]=z[3]*z[55];
    z[56]= - n<T>(9,2) + z[7];
    z[56]=z[7]*z[56];
    z[56]=n<T>(31,4) + z[56];
    z[56]=z[7]*z[56];
    z[56]= - n<T>(25,4) + z[56];
    z[56]=z[7]*z[56];
    z[55]=z[55] + n<T>(3,2) + z[56];
    z[55]=z[3]*z[55];
    z[50]= - z[7]*z[50];
    z[50]= - static_cast<T>(5)+ z[50];
    z[50]=z[7]*z[50];
    z[50]= - static_cast<T>(1)+ z[50];
    z[50]=z[7]*z[50];
    z[50]= - n<T>(1,2) + z[50];
    z[23]=z[23] + n<T>(1,4)*z[50] + z[55];
    z[50]=npow(z[3],2);
    z[23]=z[50]*z[23];
    z[55]= - static_cast<T>(2)+ n<T>(3,4)*z[7];
    z[55]=z[55]*z[7];
    z[56]= - n<T>(3,2) - z[55];
    z[56]=z[56]*z[17];
    z[57]= - static_cast<T>(13)+ z[19];
    z[57]=z[57]*z[34];
    z[57]=static_cast<T>(11)+ z[57];
    z[57]=z[7]*z[57];
    z[57]= - static_cast<T>(9)+ z[57];
    z[57]=z[7]*z[57];
    z[57]=static_cast<T>(3)+ z[57];
    z[57]=z[57]*z[46];
    z[56]=z[57] - n<T>(1,4) + z[56];
    z[57]=npow(z[3],3);
    z[56]=z[56]*z[57];
    z[58]=z[2]*npow(z[3],4);
    z[56]=z[56] - n<T>(1,4)*z[58];
    z[56]=z[2]*z[56];
    z[59]=n<T>(1,4)*z[8];
    z[60]=z[3]*z[59];
    z[23]=z[56] + z[60] + z[23];
    z[23]=z[2]*z[23];
    z[56]= - static_cast<T>(1)+ z[43];
    z[56]=z[56]*z[44];
    z[56]=n<T>(39,8) + z[56];
    z[56]=z[7]*z[56];
    z[56]= - n<T>(15,8) + z[56];
    z[56]=z[7]*z[56];
    z[56]=n<T>(1,2) + z[56];
    z[60]= - static_cast<T>(7)+ z[19];
    z[60]=z[7]*z[60];
    z[60]=n<T>(3,2) + z[60];
    z[60]=z[7]*z[60];
    z[60]=n<T>(15,2) + z[60];
    z[60]=z[7]*z[60];
    z[60]= - static_cast<T>(5)+ z[60];
    z[52]=n<T>(1,4)*z[60] - z[52];
    z[52]=z[3]*z[52];
    z[25]= - z[7]*z[25];
    z[25]=n<T>(13,2) + z[25];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(21,2) + z[25];
    z[25]=z[7]*z[25];
    z[25]=static_cast<T>(3)+ z[25];
    z[25]=n<T>(1,4)*z[25] + z[52];
    z[25]=z[3]*z[25];
    z[25]=n<T>(1,2)*z[56] + z[25];
    z[25]=z[3]*z[25];
    z[52]= - static_cast<T>(1)+ 7*z[3];
    z[52]=n<T>(1,2)*z[52] - z[53];
    z[52]=z[52]*z[59];
    z[23]=z[23] + z[52] + z[25] + z[29];
    z[23]=z[2]*z[23];
    z[25]= - static_cast<T>(25)+ n<T>(31,2)*z[7];
    z[25]=z[7]*z[25];
    z[25]=n<T>(19,2) + z[25];
    z[25]=z[25]*z[49];
    z[29]=static_cast<T>(2)- z[7];
    z[29]=z[7]*z[29];
    z[29]= - static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[51];
    z[25]=z[25] + z[29];
    z[25]=z[3]*z[25];
    z[29]=11*z[7];
    z[52]=n<T>(27,2) - z[29];
    z[52]=z[7]*z[52];
    z[52]= - n<T>(7,2) + z[52];
    z[52]=z[52]*z[18];
    z[25]=z[52] + z[25];
    z[25]=z[3]*z[25];
    z[52]= - static_cast<T>(11)+ 13*z[7];
    z[52]=z[7]*z[52];
    z[52]=n<T>(3,2) + z[52];
    z[52]=z[52]*z[49];
    z[25]=z[52] + z[25];
    z[25]=z[3]*z[25];
    z[52]=z[7] - n<T>(3,4);
    z[56]=z[52]*z[12];
    z[60]=z[3]*z[32];
    z[60]=z[56] - n<T>(1,4)*z[60];
    z[60]=z[3]*z[60];
    z[60]= - n<T>(3,2)*z[11] + z[60];
    z[60]=z[3]*z[60];
    z[61]= - n<T>(1,4) + z[7];
    z[61]=z[61]*z[12];
    z[60]=z[61] + z[60];
    z[53]=z[60]*z[53];
    z[25]=z[25] + z[53];
    z[25]=z[5]*z[25];
    z[53]= - static_cast<T>(37)+ 15*z[7];
    z[53]=z[7]*z[53];
    z[53]=static_cast<T>(29)+ z[53];
    z[53]=z[7]*z[53];
    z[53]= - static_cast<T>(7)+ z[53];
    z[53]=z[53]*z[43];
    z[47]=z[53] - z[47];
    z[53]=3*z[3];
    z[47]=z[47]*z[53];
    z[60]=static_cast<T>(59)- n<T>(61,2)*z[7];
    z[60]=z[7]*z[60];
    z[60]= - n<T>(67,2) + z[60];
    z[60]=z[7]*z[60];
    z[60]=static_cast<T>(5)+ z[60];
    z[60]=z[60]*z[34];
    z[47]=z[60] + z[47];
    z[47]=z[47]*z[46];
    z[60]= - static_cast<T>(6)+ n<T>(17,4)*z[7];
    z[60]=z[7]*z[60];
    z[60]=n<T>(17,8) + z[60];
    z[60]=z[7]*z[60];
    z[60]= - n<T>(1,8) + z[60];
    z[60]=z[7]*z[60];
    z[47]=z[60] + z[47];
    z[47]=z[3]*z[47];
    z[25]=z[47] + z[25];
    z[25]=z[5]*z[25];
    z[47]=static_cast<T>(25)- n<T>(19,2)*z[7];
    z[47]=z[47]*z[34];
    z[47]= - static_cast<T>(11)+ z[47];
    z[47]=z[7]*z[47];
    z[47]=n<T>(7,2) + z[47];
    z[47]=z[7]*z[47];
    z[60]= - static_cast<T>(12)+ n<T>(29,8)*z[7];
    z[60]=z[7]*z[60];
    z[60]=n<T>(57,4) + z[60];
    z[60]=z[7]*z[60];
    z[60]= - static_cast<T>(7)+ z[60];
    z[60]=z[7]*z[60];
    z[61]=static_cast<T>(4)- z[7];
    z[61]=z[7]*z[61];
    z[61]= - static_cast<T>(6)+ z[61];
    z[61]=z[7]*z[61];
    z[61]=static_cast<T>(4)+ z[61];
    z[61]=z[7]*z[61];
    z[61]= - static_cast<T>(1)+ z[61];
    z[61]=z[3]*z[61];
    z[60]=z[61] + n<T>(9,8) + z[60];
    z[60]=z[3]*z[60];
    z[47]=z[60] - n<T>(1,4) + z[47];
    z[47]=z[3]*z[47];
    z[60]=static_cast<T>(3)+ 5*z[36];
    z[60]=z[7]*z[60];
    z[60]= - n<T>(1,2) + z[60];
    z[60]=z[7]*z[60];
    z[47]=z[60] + z[47];
    z[47]=z[3]*z[47];
    z[25]=z[47] + z[25];
    z[25]=z[5]*z[25];
    z[47]= - static_cast<T>(9)+ n<T>(11,2)*z[7];
    z[47]=z[7]*z[47];
    z[47]=n<T>(7,2) + z[47];
    z[47]=z[47]*z[49];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[12];
    z[60]=z[14]*z[5];
    z[22]=z[22] + n<T>(3,8)*z[60];
    z[22]=z[5]*z[22];
    z[22]=z[47] + z[22];
    z[22]=z[5]*z[22];
    z[47]=z[7] - n<T>(5,2);
    z[47]=z[47]*z[34];
    z[47]=z[47] + 1;
    z[47]=z[47]*z[7];
    z[47]=z[47] - n<T>(1,4);
    z[47]=z[47]*z[7];
    z[22]=z[47] + z[22];
    z[22]=z[22]*z[30];
    z[30]=3*z[32] + z[60];
    z[30]=z[30]*z[13];
    z[30]=3*z[38] + z[30];
    z[30]=z[5]*z[30];
    z[38]=z[45]*z[34];
    z[30]=z[38] + z[30];
    z[30]=z[30]*z[39]*z[59];
    z[22]=z[22] + z[30];
    z[22]=z[8]*z[22];
    z[30]= - static_cast<T>(9)+ z[20];
    z[30]=z[7]*z[30];
    z[30]=n<T>(5,2) + z[30];
    z[30]=z[30]*z[49];
    z[27]=z[30] + z[27];
    z[27]=z[5]*z[27];
    z[30]=z[35]*z[19];
    z[30]=n<T>(7,4) + z[30];
    z[30]=z[7]*z[30];
    z[30]= - n<T>(1,4) + z[30];
    z[30]=z[30]*z[34];
    z[27]=z[30] + z[27];
    z[30]=npow(z[5],2);
    z[27]=z[27]*z[30];
    z[22]=z[27] + z[22];
    z[22]=z[8]*z[22];
    z[27]= - static_cast<T>(29)+ z[20];
    z[27]=z[27]*z[34];
    z[27]=static_cast<T>(23)+ z[27];
    z[27]=z[7]*z[27];
    z[27]= - static_cast<T>(17)+ z[27];
    z[27]=z[27]*z[43];
    z[35]=z[54]*z[34];
    z[35]=z[35] + 1;
    z[38]= - z[3]*z[35];
    z[27]=z[38] + static_cast<T>(1)+ z[27];
    z[27]=z[3]*z[27];
    z[38]=static_cast<T>(5)- z[21];
    z[38]=z[38]*z[34];
    z[38]= - static_cast<T>(3)+ z[38];
    z[38]=z[7]*z[38];
    z[38]=n<T>(3,2) + z[38];
    z[38]=z[38]*z[19];
    z[38]= - n<T>(1,2) + z[38];
    z[27]=n<T>(1,2)*z[38] + z[27];
    z[27]=z[3]*z[27];
    z[27]=z[47] + z[27];
    z[27]=z[3]*z[27];
    z[22]=z[22] + z[27] + z[25];
    z[22]=z[4]*z[22];
    z[20]= - z[31]*z[20];
    z[25]= - z[32] - z[60];
    z[25]=z[5]*z[25];
    z[25]= - 7*z[17] + z[25];
    z[25]=z[5]*z[25];
    z[20]=z[20] + z[25];
    z[20]=z[20]*z[59];
    z[25]=n<T>(7,2)*z[7];
    z[27]=static_cast<T>(1)- z[25];
    z[27]=z[27]*z[17];
    z[38]=z[7] - n<T>(7,4);
    z[39]= - z[38]*z[12];
    z[39]=z[39] - n<T>(1,4)*z[60];
    z[39]=z[5]*z[39];
    z[20]=z[20] + z[27] + z[39];
    z[27]=z[40]*z[30];
    z[20]=z[20]*z[27];
    z[30]=7*z[11] + z[15];
    z[30]=z[30]*z[13];
    z[39]=n<T>(17,2) - z[44];
    z[39]=z[7]*z[39];
    z[39]= - n<T>(19,8) + z[39];
    z[39]=z[39]*z[17];
    z[30]=z[39] + z[30];
    z[30]=z[30]*z[13];
    z[39]=static_cast<T>(3)- z[25];
    z[39]=z[39]*z[43];
    z[39]=static_cast<T>(1)+ z[39];
    z[39]=z[7]*z[39];
    z[39]= - n<T>(19,16) + z[39];
    z[39]=z[7]*z[39];
    z[20]=z[20] + z[39] + z[30];
    z[20]=z[8]*z[20];
    z[30]=23*z[7];
    z[39]=static_cast<T>(5)- z[30];
    z[39]=z[39]*z[12];
    z[39]=z[39] + 5*z[60];
    z[40]=n<T>(1,4)*z[5];
    z[39]=z[39]*z[40];
    z[39]= - z[56] + z[39];
    z[39]=z[5]*z[39];
    z[44]=z[48]*z[17];
    z[15]= - 7*z[12] - z[15];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(3,2)*z[44] + z[15];
    z[15]=z[15]*z[27];
    z[27]=z[7]*z[38];
    z[27]=n<T>(3,4) + z[27];
    z[27]=z[27]*z[17];
    z[15]=z[15] + z[27] + z[39];
    z[27]=npow(z[8],2);
    z[15]=z[15]*z[27];
    z[33]=static_cast<T>(1)- z[33];
    z[33]=z[33]*z[12];
    z[33]=z[33] - z[60];
    z[33]=z[5]*z[33];
    z[33]=n<T>(1,8)*z[14] + z[33];
    z[33]=z[5]*z[33];
    z[32]= - n<T>(1,8)*z[32] + z[33];
    z[33]=z[9]*npow(z[8],3);
    z[32]=z[32]*z[33];
    z[15]=n<T>(1,2)*z[15] + z[32];
    z[15]=z[9]*z[15];
    z[15]=z[20] + z[15];
    z[15]=z[9]*z[15];
    z[20]= - static_cast<T>(1)+ z[53];
    z[20]=z[20]*z[50];
    z[32]=z[3] - n<T>(1,2);
    z[32]=z[32]*z[57];
    z[38]=z[32] - n<T>(1,2)*z[58];
    z[38]=z[2]*z[38];
    z[20]=n<T>(1,4)*z[20] + z[38];
    z[20]=z[2]*z[20];
    z[38]=n<T>(1,4) - z[50];
    z[38]=z[38]*z[50];
    z[32]=z[4]*z[32];
    z[20]=n<T>(1,2)*z[32] + z[38] + z[20];
    z[20]=z[1]*z[20];
    z[24]=z[52]*z[24];
    z[32]=z[13]*z[51];
    z[24]=z[24] + z[32];
    z[24]=z[5]*z[24];
    z[28]=z[28] + n<T>(3,4);
    z[32]=z[28]*z[46];
    z[24]=z[32] + z[24];
    z[24]=z[5]*z[24];
    z[32]=z[17]*z[13];
    z[38]=z[7]*z[52];
    z[32]=z[38] + z[32];
    z[32]=z[5]*z[32];
    z[28]=n<T>(1,2)*z[28] + z[32];
    z[28]=z[8]*z[28];
    z[24]=z[24] + z[28];
    z[24]=z[10]*z[24];
    z[20]=z[24] + z[20];
    z[24]= - static_cast<T>(33)+ 25*z[7];
    z[24]=z[24]*z[43];
    z[24]=static_cast<T>(2)+ z[24];
    z[24]=z[7]*z[24];
    z[24]=n<T>(1,4) + z[24];
    z[24]=z[7]*z[24];
    z[28]=n<T>(7,4) + z[55];
    z[28]=z[7]*z[28];
    z[28]= - n<T>(1,2) + z[28];
    z[28]=z[3]*z[28]*z[19];
    z[32]= - n<T>(15,2) - 13*z[36];
    z[32]=z[7]*z[32];
    z[32]=static_cast<T>(1)+ z[32];
    z[32]=z[7]*z[32];
    z[28]=z[32] + z[28];
    z[28]=z[3]*z[28];
    z[24]=z[24] + z[28];
    z[24]=z[3]*z[24];
    z[28]=static_cast<T>(5)- n<T>(9,2)*z[7];
    z[28]=z[7]*z[28];
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=z[28]*z[17];
    z[32]=z[48]*z[34];
    z[32]=static_cast<T>(1)+ z[32];
    z[32]=z[32]*z[51];
    z[28]=z[28] + z[32];
    z[28]=z[3]*z[28];
    z[32]= - static_cast<T>(5)+ z[41];
    z[12]=z[32]*z[12];
    z[12]=n<T>(1,2)*z[12] + z[28];
    z[12]=z[3]*z[12];
    z[12]= - n<T>(3,2)*z[14] + z[12];
    z[12]=z[12]*z[13];
    z[12]=z[12] - n<T>(3,2)*z[26] + z[24];
    z[12]=z[3]*z[12];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[5]*z[11];
    z[12]= - static_cast<T>(97)+ 47*z[7];
    z[12]=z[12]*z[34];
    z[12]=static_cast<T>(27)+ z[12];
    z[12]=z[7]*z[12];
    z[12]= - n<T>(1,2) + z[12];
    z[12]=z[7]*z[12];
    z[12]= - n<T>(3,2) + z[12];
    z[13]= - static_cast<T>(11)+ z[19];
    z[13]=z[7]*z[13];
    z[13]=static_cast<T>(15)+ z[13];
    z[13]=z[7]*z[13];
    z[13]= - static_cast<T>(9)+ z[13];
    z[13]=z[13]*z[34];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[3]*z[13];
    z[14]=static_cast<T>(18)- n<T>(25,4)*z[7];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(71,4) + z[14];
    z[14]=z[7]*z[14];
    z[14]=n<T>(13,2) + z[14];
    z[14]=z[7]*z[14];
    z[13]=n<T>(3,2)*z[13] - n<T>(1,2) + z[14];
    z[13]=z[3]*z[13];
    z[12]=n<T>(1,4)*z[12] + z[13];
    z[12]=z[3]*z[12];
    z[13]=static_cast<T>(35)- z[30];
    z[13]=z[7]*z[13];
    z[13]= - n<T>(25,2) + z[13];
    z[13]=z[7]*z[13];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[7]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[12]=n<T>(1,8)*z[13] + z[12];
    z[12]=z[3]*z[12];
    z[13]= - static_cast<T>(29)+ 17*z[7];
    z[13]=z[7]*z[13];
    z[13]=n<T>(15,2) + z[13];
    z[13]=z[13]*z[17];
    z[11]=z[11] + n<T>(1,8)*z[13] + z[12];
    z[11]=z[5]*z[11];
    z[12]= - z[37]*z[18]*z[33];
    z[13]= - n<T>(11,4) + z[7];
    z[13]=z[7]*z[13];
    z[13]=n<T>(5,2) + z[13];
    z[13]=z[7]*z[13];
    z[13]= - n<T>(3,4) + z[13];
    z[13]=z[7]*z[13]*z[27];
    z[12]=z[13] + z[12];
    z[12]=z[9]*z[12];
    z[13]=static_cast<T>(11)- z[25];
    z[13]=z[13]*z[34];
    z[13]= - static_cast<T>(7)+ z[13];
    z[13]=z[7]*z[13];
    z[13]=n<T>(9,2) + z[13];
    z[13]=z[7]*z[13];
    z[13]= - n<T>(5,4) + z[13];
    z[13]=z[8]*z[13];
    z[12]=z[13] + z[12];
    z[12]=z[9]*z[12];
    z[13]= - n<T>(13,4) + z[7];
    z[13]=z[7]*z[13];
    z[13]=n<T>(19,4) + z[13];
    z[13]=z[7]*z[13];
    z[13]= - n<T>(15,4) + z[13];
    z[13]=z[7]*z[13];
    z[12]=z[12] + n<T>(5,4) + z[13];
    z[13]=z[34] + 1;
    z[13]=z[13]*z[7];
    z[14]=static_cast<T>(9)- z[13];
    z[14]=z[14]*z[43];
    z[14]= - static_cast<T>(4)+ z[14];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(3,4)*z[5] + n<T>(17,8) + z[14];
    z[14]=z[57]*z[14];
    z[17]=z[35] - z[40];
    z[17]=z[17]*z[58];
    z[14]=z[17] + z[14];
    z[14]=z[2]*z[14];
    z[17]=z[31]*z[43];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[34];
    z[17]= - static_cast<T>(2)+ z[17];
    z[17]=z[7]*z[17];
    z[17]= - n<T>(7,8)*z[5] + n<T>(3,2) + z[17];
    z[17]=z[50]*z[17];
    z[14]=z[14] + z[17];
    z[14]=z[2]*z[14];
    z[17]= - n<T>(11,2) - z[42];
    z[17]=z[17]*z[34];
    z[17]=static_cast<T>(3)+ z[17];
    z[17]=z[7]*z[17];
    z[17]= - n<T>(5,4) + z[17];
    z[17]=z[17]*z[46];
    z[14]=z[17] + z[14];
    z[14]=z[2]*z[14];
    z[12]=z[14] + n<T>(1,2)*z[12];
    z[12]=z[6]*z[12];
    z[14]= - static_cast<T>(7)+ n<T>(5,2)*z[7];
    z[14]=z[14]*z[19];
    z[14]=n<T>(37,2) + z[14];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(7,2) + z[14];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(3,2) + z[14];
    z[17]= - static_cast<T>(7)+ z[21];
    z[17]=z[7]*z[17];
    z[17]=static_cast<T>(13)+ z[17];
    z[17]=z[17]*z[34];
    z[17]= - static_cast<T>(6)+ z[17];
    z[17]=z[7]*z[17];
    z[17]=n<T>(9,4) + z[17];
    z[17]=z[3]*z[17];
    z[18]=n<T>(15,2) - 2*z[7];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(21,2) + z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(13,2) + z[18];
    z[18]=z[7]*z[18];
    z[17]=z[17] - static_cast<T>(1)+ z[18];
    z[17]=z[3]*z[17];
    z[14]=n<T>(1,4)*z[14] + z[17];
    z[14]=z[3]*z[14];
    z[17]=static_cast<T>(23)- z[29];
    z[17]=z[7]*z[17];
    z[17]= - n<T>(13,2) + z[17];
    z[17]=z[17]*z[34];
    z[17]= - static_cast<T>(7)+ z[17];
    z[17]=z[7]*z[17];
    z[17]=n<T>(1,4) + z[17];
    z[14]=n<T>(1,4)*z[17] + z[14];
    z[14]=z[3]*z[14];
    z[13]= - n<T>(13,4) + z[13];
    z[13]=z[7]*z[13];
    z[13]=n<T>(19,8) + z[13];
    z[13]=z[13]*z[34];

    r += z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + n<T>(1,2)*z[20] + 
      z[22] + z[23];
 
    return r;
}

template double qg_2lha_r1570(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1570(const std::array<dd_real,30>&);
#endif
