#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r24(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[27];
    z[7]=k[9];
    z[8]=n<T>(1,2)*z[1];
    z[9]=npow(z[6],2);
    z[10]=z[8]*z[9];
    z[11]=n<T>(1,3)*z[6];
    z[12]= - static_cast<T>(1)- z[6];
    z[12]=z[12]*z[11];
    z[12]=z[12] - z[10];
    z[12]=z[1]*z[12];
    z[13]=n<T>(1,2)*z[9];
    z[14]=npow(z[7],2);
    z[15]=z[13] - z[14];
    z[12]=n<T>(1,3)*z[15] + z[12];
    z[12]=z[4]*z[12];
    z[15]=n<T>(1,3)*z[2];
    z[16]=z[14]*z[15];
    z[17]=static_cast<T>(1)- n<T>(4,3)*z[7];
    z[17]=z[7]*z[17];
    z[18]=z[8] + n<T>(1,2);
    z[18]=z[6]*z[18];
    z[18]=n<T>(1,3) + z[18];
    z[18]=z[1]*z[18];
    z[12]=z[12] + z[18] + z[16] - z[11] + z[17];
    z[12]=z[4]*z[12];
    z[16]=5*z[7];
    z[17]=n<T>(1,3)*z[7];
    z[18]=n<T>(1,2) - z[17];
    z[18]=z[18]*z[16];
    z[19]=z[7] - 1;
    z[19]=z[19]*z[7];
    z[20]= - z[15] - n<T>(1,3) + z[19];
    z[20]=z[2]*z[20];
    z[21]=n<T>(1,3)*z[1];
    z[22]=z[2] - n<T>(1,2);
    z[23]= - z[1] + z[22];
    z[23]=z[23]*z[21];
    z[12]=z[12] + z[23] + z[20] - n<T>(1,6) + z[18];
    z[18]=n<T>(1,3)*z[4];
    z[12]=z[12]*z[18];
    z[20]=z[1] + 2;
    z[23]=z[1]*z[20];
    z[24]=z[1]*z[6];
    z[25]= - 2*z[6] - z[24];
    z[25]=z[1]*z[25];
    z[25]= - z[6] + z[25];
    z[25]=z[4]*z[25];
    z[23]=z[25] + z[23] + static_cast<T>(1)- z[14];
    z[23]=z[23]*z[18];
    z[25]=z[2] - 1;
    z[26]=z[7] - n<T>(4,3);
    z[26]=z[26]*z[7];
    z[26]=z[26] + n<T>(1,3);
    z[26]=z[26]*z[25];
    z[27]=z[25]*z[1];
    z[28]=2*z[25] + z[27];
    z[28]=z[28]*z[21];
    z[23]=z[23] + z[28] + z[26];
    z[23]=z[4]*z[23];
    z[20]=z[21]*z[20];
    z[26]=z[7] - n<T>(8,3);
    z[26]=z[26]*z[7];
    z[20]=z[20] - z[26] - n<T>(5,3);
    z[26]=z[2] - 2;
    z[26]=z[26]*z[2];
    z[26]=z[26] + 1;
    z[20]=z[26]*z[20];
    z[20]=z[23] + z[20];
    z[18]=z[20]*z[18];
    z[20]= - static_cast<T>(2)+ z[1];
    z[23]=z[15] - 1;
    z[23]=z[23]*z[2];
    z[23]=z[23] + 1;
    z[26]=z[23]*z[2];
    z[26]=z[26] - n<T>(1,3);
    z[20]=z[21]*z[26]*z[20];
    z[26]=z[7] - 4;
    z[17]=z[26]*z[17];
    z[17]=z[17] + 1;
    z[23]=z[2]*z[17]*z[23];
    z[17]=z[18] + z[20] - n<T>(1,3)*z[17] + z[23];
    z[17]=z[3]*z[17];
    z[18]=z[9] + z[10];
    z[18]=z[1]*z[18];
    z[13]=z[13] + z[18];
    z[13]=z[4]*z[13];
    z[18]= - z[6] + z[14];
    z[20]= - z[6] + z[24];
    z[20]=z[1]*z[20];
    z[13]=z[13] + 2*z[18] + z[20];
    z[13]=z[4]*z[13];
    z[18]=2*z[2];
    z[20]=static_cast<T>(1)- 2*z[7];
    z[20]=z[20]*z[18];
    z[20]=z[20] - static_cast<T>(7)+ n<T>(13,2)*z[7];
    z[20]=z[7]*z[20];
    z[23]=static_cast<T>(2)- z[8];
    z[23]=z[1]*z[23];
    z[13]=z[13] + z[23] + n<T>(5,2) + z[20];
    z[13]=z[4]*z[13];
    z[20]=n<T>(17,9) - z[7];
    z[20]=z[7]*z[20];
    z[23]= - static_cast<T>(1)+ z[19];
    z[23]=z[2]*z[23];
    z[20]=n<T>(2,9)*z[23] - n<T>(4,9) + z[20];
    z[20]=z[2]*z[20];
    z[23]=z[25]*z[21];
    z[24]= - static_cast<T>(5)+ n<T>(7,3)*z[7];
    z[24]=z[7]*z[24];
    z[24]=static_cast<T>(2)+ z[24];
    z[13]=n<T>(1,9)*z[13] + z[23] + n<T>(1,3)*z[24] + z[20];
    z[13]=z[4]*z[13];
    z[20]=npow(z[2],2);
    z[23]=static_cast<T>(1)- n<T>(2,3)*z[2];
    z[23]=z[23]*z[20];
    z[24]= - static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[24]=z[2]*z[24];
    z[24]=n<T>(1,2) + z[24];
    z[24]=z[24]*z[21];
    z[23]=z[24] - n<T>(1,3) + z[23];
    z[23]=z[1]*z[23];
    z[24]= - static_cast<T>(8)+ n<T>(5,2)*z[7];
    z[24]=z[24]*z[7];
    z[18]=z[18] - n<T>(1,2) + z[24];
    z[18]=z[2]*z[18];
    z[16]=static_cast<T>(16)- z[16];
    z[16]=z[7]*z[16];
    z[16]=z[18] - static_cast<T>(5)+ z[16];
    z[16]=z[2]*z[16];
    z[16]=z[16] + n<T>(7,2) + z[24];
    z[16]=n<T>(1,3)*z[16] + z[23];
    z[13]=z[17] + n<T>(1,3)*z[16] + z[13];
    z[13]=z[3]*z[13];
    z[16]= - n<T>(1,2) + n<T>(2,9)*z[7];
    z[16]=z[16]*z[7];
    z[17]=n<T>(1,9)*z[20] - n<T>(1,18) + z[16];
    z[17]=z[2]*z[17];
    z[15]=n<T>(1,2) - z[15];
    z[15]=z[2]*z[15];
    z[15]= - n<T>(1,6)*z[27] - n<T>(1,6) + z[15];
    z[15]=z[15]*z[21];
    z[12]=z[13] + z[12] + z[15] + z[17] - n<T>(1,18) - z[16];
    z[12]=z[3]*z[12];
    z[13]=z[5] - 1;
    z[15]=npow(z[5],2);
    z[13]=z[13]*z[15]*z[6];
    z[16]= - z[5] + z[25];
    z[16]=z[2]*z[16];
    z[16]=z[16] + z[19] + z[15] + z[13];
    z[17]=z[5] - n<T>(1,2);
    z[18]=z[6]*z[5];
    z[17]=z[17]*z[18];
    z[19]=static_cast<T>(1)+ z[18];
    z[8]=z[19]*z[8];
    z[8]=z[8] - z[17] - z[5] + z[22];
    z[8]=z[1]*z[8];
    z[8]=n<T>(1,2)*z[16] + z[8];
    z[16]=n<T>(1,2) - 2*z[5];
    z[16]=z[6]*z[16];
    z[16]= - static_cast<T>(1)+ z[16];
    z[11]=z[16]*z[11];
    z[10]=z[11] + z[10];
    z[10]=z[1]*z[10];
    z[11]=z[15]*z[9];
    z[11]=z[11] + z[14];
    z[9]=z[4]*z[9]*z[21];
    z[9]=z[9] + n<T>(1,6)*z[11] + z[10];
    z[9]=z[4]*z[9];
    z[10]= - static_cast<T>(2)- n<T>(1,2)*z[18];
    z[10]=z[10]*z[21];
    z[10]=z[10] + n<T>(1,3)*z[17] - n<T>(1,6) + z[5];
    z[10]=z[6]*z[10];
    z[10]=n<T>(1,3) + z[10];
    z[10]=z[1]*z[10];
    z[11]= - z[15] - n<T>(1,2)*z[13];
    z[11]=z[6]*z[11];
    z[13]= - n<T>(1,2) + z[7];
    z[13]=z[7]*z[13];
    z[11]= - z[2] + z[11] + z[13];
    z[9]=z[9] + n<T>(1,3)*z[11] + z[10];
    z[9]=z[4]*z[9];
    z[8]=n<T>(1,3)*z[8] + z[9];
    z[8]=n<T>(1,3)*z[8] + z[12];

    r += 5*z[8];
 
    return r;
}

template double qg_2lha_r24(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r24(const std::array<dd_real,30>&);
#endif
