#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r413(const std::array<T,30>& k) {
  T z[27];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[9];
    z[7]=k[12];
    z[8]=k[10];
    z[9]=k[8];
    z[10]=k[5];
    z[11]=k[14];
    z[12]=n<T>(1,2)*z[11];
    z[13]= - static_cast<T>(13)- z[11];
    z[13]=z[13]*z[12];
    z[14]=n<T>(1,4)*z[5];
    z[15]=7*z[5];
    z[16]= - n<T>(127,3) + z[15];
    z[16]=z[16]*z[14];
    z[17]=3*z[3];
    z[18]=static_cast<T>(23)+ z[17];
    z[18]=z[3]*z[18];
    z[13]=z[16] + n<T>(1,2)*z[18] + static_cast<T>(3)+ z[13];
    z[16]=n<T>(1,2)*z[5];
    z[18]= - n<T>(235,3) + 17*z[5];
    z[18]=z[18]*z[16];
    z[19]= - static_cast<T>(27)- z[11];
    z[19]=z[11]*z[19];
    z[20]=static_cast<T>(5)+ z[17];
    z[20]=z[3]*z[20];
    z[18]=z[18] + z[20] + n<T>(101,6) + z[19];
    z[19]=n<T>(17,16)*z[4] - n<T>(16,3) + n<T>(17,8)*z[5];
    z[19]=z[4]*z[19];
    z[18]=n<T>(1,8)*z[18] + z[19];
    z[18]=z[4]*z[18];
    z[13]=n<T>(1,4)*z[13] + z[18];
    z[13]=z[8]*z[13];
    z[18]=static_cast<T>(1)+ n<T>(1,8)*z[3];
    z[17]=z[18]*z[17];
    z[18]= - static_cast<T>(5)+ n<T>(3,2)*z[9];
    z[18]=z[18]*z[16];
    z[18]=z[18] + 33;
    z[19]=z[18]*z[14];
    z[20]=n<T>(1,2)*z[4];
    z[21]=n<T>(55,3) + z[15];
    z[21]=n<T>(1,4)*z[21] + 3*z[4];
    z[21]=z[21]*z[20];
    z[12]=static_cast<T>(5)- z[12];
    z[12]=z[11]*z[12];
    z[12]=z[13] + z[21] + z[19] + z[17] - static_cast<T>(5)+ n<T>(1,4)*z[12];
    z[12]=z[8]*z[12];
    z[13]=n<T>(7,3) - z[16];
    z[13]=z[13]*z[15];
    z[13]=n<T>(45,2) + z[13];
    z[13]=z[13]*z[16];
    z[15]=7*z[11];
    z[17]=z[15] + 9*z[3];
    z[13]=z[13] - z[17];
    z[19]=n<T>(29,3) - n<T>(21,8)*z[5];
    z[19]=z[5]*z[19];
    z[21]=n<T>(21,2)*z[5];
    z[22]= - n<T>(7,2)*z[4] + n<T>(61,3) - z[21];
    z[22]=z[4]*z[22];
    z[19]=n<T>(1,4)*z[22] - n<T>(11,8) + z[19];
    z[19]=z[4]*z[19];
    z[13]=n<T>(1,2)*z[13] + z[19];
    z[13]=z[13]*z[20];
    z[19]= - static_cast<T>(1)+ n<T>(3,4)*z[9];
    z[19]=z[5]*z[19];
    z[19]=n<T>(59,4) + z[19];
    z[19]=z[19]*z[14];
    z[19]= - static_cast<T>(4)+ z[19];
    z[19]=z[5]*z[19];
    z[13]=z[13] + z[19] - n<T>(9,4)*z[3] + static_cast<T>(4)- n<T>(7,4)*z[11];
    z[13]=z[8]*z[13];
    z[19]= - static_cast<T>(7)+ 3*z[9];
    z[22]= - z[19]*z[14];
    z[22]= - n<T>(71,3) + z[22];
    z[22]=z[22]*z[14];
    z[22]=static_cast<T>(4)+ z[22];
    z[22]=z[5]*z[22];
    z[21]= - 7*z[4] + n<T>(1,3) - z[21];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(91,2)*z[5] + z[21];
    z[21]=z[4]*z[21];
    z[13]=z[13] + n<T>(1,8)*z[21] - n<T>(1,4)*z[17] + z[22];
    z[13]=z[8]*z[13];
    z[17]=npow(z[5],2);
    z[21]=static_cast<T>(1)+ n<T>(1,3)*z[5];
    z[21]=z[21]*z[17];
    z[22]=static_cast<T>(1)+ z[5];
    z[22]=z[5]*z[22];
    z[23]=n<T>(1,3)*z[4];
    z[24]=z[23] + n<T>(1,3) + z[5];
    z[24]=z[4]*z[24];
    z[22]=z[22] + z[24];
    z[22]=z[4]*z[22];
    z[21]=z[21] + z[22];
    z[21]=z[4]*z[21];
    z[22]=npow(z[5],3);
    z[24]=n<T>(1,3)*z[22];
    z[21]=z[24] + z[21];
    z[21]=z[8]*z[21];
    z[25]=npow(z[4],2);
    z[25]= - z[17] + n<T>(1,3)*z[25];
    z[25]=z[4]*z[25];
    z[21]=z[21] - n<T>(2,3)*z[22] + z[25];
    z[21]=z[8]*z[21];
    z[22]=static_cast<T>(2)+ z[6];
    z[22]=z[6]*z[22]*z[24];
    z[24]=npow(z[6],2);
    z[25]=z[24]*z[5];
    z[26]=z[6] + n<T>(1,3)*z[25];
    z[17]=z[4]*z[26]*z[17];
    z[17]=z[21] + z[22] + z[17];
    z[17]=z[7]*z[17];
    z[21]=z[5]*z[6];
    z[19]=z[19]*z[21];
    z[22]=n<T>(71,2) + 13*z[6];
    z[22]=z[6]*z[22];
    z[19]=n<T>(1,3)*z[22] + n<T>(1,8)*z[19];
    z[19]=z[19]*z[16];
    z[19]= - 4*z[6] + z[19];
    z[19]=z[5]*z[19];
    z[22]=z[24]*z[16];
    z[22]=z[6] + z[22];
    z[22]=z[5]*z[22];
    z[22]=5*z[22] - n<T>(7,16)*z[4];
    z[22]=z[4]*z[22];
    z[13]=z[17] + z[13] + z[19] + z[22];
    z[13]=z[7]*z[13];
    z[17]= - 17*z[6] - z[18];
    z[14]=z[14]*z[17];
    z[14]=static_cast<T>(5)+ z[14];
    z[14]=z[6]*z[14];
    z[17]= - z[25] + n<T>(1,4) - z[6];
    z[17]=z[4]*z[17];
    z[12]=z[13] + z[12] + n<T>(7,4)*z[17] + z[14];
    z[12]=z[7]*z[12];
    z[13]=z[3]*z[2];
    z[14]= - z[11]*z[2];
    z[15]=7*z[6] + static_cast<T>(9)- z[15];
    z[15]=z[6]*z[15];
    z[14]= - n<T>(5,8)*z[21] + z[13] + z[14] + n<T>(1,2)*z[15];
    z[15]=z[4] + z[5];
    z[17]= - static_cast<T>(7)- z[11];
    z[17]=z[11]*z[17];
    z[17]=n<T>(61,6) + z[17];
    z[18]=n<T>(1,3)*z[3];
    z[19]=n<T>(5,8) - z[18];
    z[19]=z[3]*z[19];
    z[19]= - n<T>(1,4) + z[19];
    z[19]=z[3]*z[19];
    z[17]=n<T>(1,4)*z[17] + z[19] - n<T>(15,16)*z[15];
    z[17]=z[4]*z[17];
    z[19]=n<T>(5,16)*z[5];
    z[21]=npow(z[11],2);
    z[21]=n<T>(5,3) - z[21];
    z[22]=n<T>(1,4) - z[18];
    z[22]=z[3]*z[22];
    z[22]= - n<T>(5,4) + z[22];
    z[22]=z[3]*z[22];
    z[17]=z[17] - z[19] + n<T>(1,8)*z[21] + z[22];
    z[17]=z[8]*z[17];
    z[21]= - n<T>(5,3) + z[11];
    z[22]= - n<T>(1,4) - z[18];
    z[22]=z[3]*z[22];
    z[22]= - n<T>(3,4) + z[22];
    z[22]=z[3]*z[22];
    z[17]=z[17] - n<T>(5,8)*z[4] + z[19] + n<T>(5,4)*z[21] + z[22];
    z[17]=z[8]*z[17];
    z[12]=z[12] + n<T>(1,2)*z[14] + z[17];
    z[12]=z[7]*z[12];
    z[14]=z[3] - n<T>(1,2);
    z[17]=npow(z[3],2);
    z[14]= - z[17]*z[14]*z[15];
    z[15]= - n<T>(1,4) + z[17];
    z[15]=z[3]*z[15];
    z[14]=z[15] + z[14];
    z[15]=static_cast<T>(1)- z[3];
    z[15]=z[15]*z[17]*z[16];
    z[16]= - n<T>(5,4) + z[3];
    z[16]=z[3]*z[16];
    z[16]=n<T>(1,4) + z[16];
    z[16]=z[3]*z[16];
    z[15]=z[16] + z[15];
    z[16]= - static_cast<T>(1)- z[11];
    z[16]=z[11]*z[16];
    z[16]=n<T>(5,2) + z[16];
    z[19]= - n<T>(1,3) + n<T>(1,4)*z[10];
    z[19]=z[3]*z[19];
    z[19]=z[19] + n<T>(1,3) - n<T>(1,2)*z[10];
    z[19]=z[3]*z[19];
    z[16]=n<T>(1,4)*z[16] + z[19];
    z[16]=z[16]*z[20];
    z[15]=n<T>(1,3)*z[15] + z[16];
    z[15]=z[8]*z[15];
    z[14]=n<T>(1,3)*z[14] + z[15];
    z[14]=z[8]*z[14];
    z[15]=n<T>(1,2)*z[2];
    z[16]= - static_cast<T>(1)+ n<T>(1,3)*z[1];
    z[16]=z[16]*z[15];
    z[16]=n<T>(1,3) + z[16];
    z[19]=npow(z[1],2);
    z[19]=static_cast<T>(1)- z[19];
    z[15]=z[19]*z[15];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[18];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[3]*z[15];
    z[15]= - n<T>(1,6)*z[2] + z[15];
    z[15]=z[3]*z[15];
    z[13]=z[1]*z[13];
    z[13]= - n<T>(1,4)*z[2] + z[13];
    z[13]=z[13]*z[17];
    z[16]= - z[2]*npow(z[3],3)*z[20];
    z[13]=z[13] + z[16];
    z[13]=z[13]*z[23];

    r += z[12] + z[13] + z[14] + z[15];
 
    return r;
}

template double qg_2lha_r413(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r413(const std::array<dd_real,30>&);
#endif
