#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1699(const std::array<T,30>& k) {
  T z[25];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[24];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[9];
    z[8]=k[12];
    z[9]=k[14];
    z[10]=n<T>(1,2)*z[9];
    z[11]=z[10] - 1;
    z[10]=z[11]*z[10];
    z[12]=z[9] - 1;
    z[13]=z[12]*z[9];
    z[14]=n<T>(1,2)*z[8];
    z[15]=z[14] - 1;
    z[16]=z[8]*z[15];
    z[16]=z[13] + static_cast<T>(1)+ z[16];
    z[17]=n<T>(1,2)*z[5];
    z[16]=z[16]*z[17];
    z[18]=n<T>(1,2)*z[1];
    z[19]=npow(z[4],2);
    z[20]= - z[19]*z[18];
    z[21]=n<T>(1,2) + z[4];
    z[21]=z[4]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[1]*z[20];
    z[20]= - z[4] + z[20];
    z[20]=z[1]*z[20];
    z[10]=z[20] + z[16] + z[10] - z[15];
    z[14]=z[11] + z[14];
    z[15]=z[8] - 1;
    z[16]= - z[8]*z[15];
    z[20]=z[9] + 1;
    z[21]=z[9]*z[20];
    z[16]=z[16] + z[21];
    z[16]=z[16]*z[17];
    z[16]=z[16] + z[14];
    z[14]=z[14]*z[17];
    z[21]=n<T>(1,2)*z[4];
    z[22]=z[4]*z[18];
    z[22]=z[22] - static_cast<T>(1)- z[21];
    z[22]=z[1]*z[22];
    z[22]= - z[17] + z[22];
    z[22]=z[1]*z[22];
    z[22]= - z[17] + z[22];
    z[22]=z[1]*z[22];
    z[14]=z[14] + z[22];
    z[14]=z[3]*z[14];
    z[22]=z[19]*z[1];
    z[23]= - static_cast<T>(3)- z[4];
    z[23]=z[4]*z[23];
    z[23]=z[23] + z[22];
    z[23]=z[1]*z[23];
    z[23]=n<T>(1,4)*z[23] + n<T>(5,4) + z[4];
    z[23]=z[1]*z[23];
    z[24]= - static_cast<T>(1)+ z[5];
    z[23]=n<T>(1,4)*z[24] + z[23];
    z[23]=z[1]*z[23];
    z[14]=z[14] + n<T>(1,4)*z[16] + z[23];
    z[14]=z[3]*z[14];
    z[10]=n<T>(1,2)*z[10] + z[14];
    z[10]=z[3]*z[10];
    z[14]= - static_cast<T>(1)- z[5];
    z[14]=z[14]*z[21];
    z[14]= - static_cast<T>(1)+ z[14];
    z[16]=npow(z[4],3);
    z[14]=z[14]*z[16];
    z[23]=npow(z[4],4);
    z[24]=z[23]*z[18];
    z[14]=z[14] + z[24];
    z[14]=z[1]*z[14];
    z[24]=z[4]*z[5];
    z[24]=z[24] + static_cast<T>(1)+ z[17];
    z[24]=z[4]*z[24];
    z[24]=n<T>(1,2) + z[24];
    z[24]=z[24]*z[19];
    z[14]=z[24] + z[14];
    z[14]=z[1]*z[14];
    z[21]= - npow(z[5],2)*z[21];
    z[21]= - z[5] + z[21];
    z[21]=z[4]*z[21];
    z[21]= - static_cast<T>(1)+ n<T>(3,2)*z[21];
    z[19]=z[21]*z[19];
    z[14]=z[19] + n<T>(3,2)*z[14];
    z[14]=z[1]*z[14];
    z[12]=z[12]*z[17];
    z[12]=z[12] + z[11];
    z[12]=z[9]*z[12];
    z[12]=n<T>(1,2)*z[15] + z[12];
    z[10]=z[10] + n<T>(1,4)*z[12] + z[14];
    z[10]=z[2]*z[10];
    z[12]=npow(z[8],2);
    z[11]=z[9]*z[11];
    z[14]=z[4] - z[22];
    z[14]=z[1]*z[14];
    z[11]=z[14] - n<T>(1,4)*z[12] + z[11];
    z[14]=z[8] + z[9];
    z[15]= - z[4] - z[22];
    z[15]=z[1]*z[15];
    z[15]= - static_cast<T>(3)+ z[15];
    z[15]=z[1]*z[15];
    z[14]=z[15] + n<T>(1,2)*z[14] - z[5];
    z[15]= - z[1]*z[4];
    z[15]=static_cast<T>(3)+ z[15];
    z[15]=z[15]*z[18];
    z[15]=z[5] + z[15];
    z[15]=z[1]*z[15];
    z[15]=z[17] + z[15];
    z[15]=z[3]*z[15];
    z[14]=n<T>(1,4)*z[14] + z[15];
    z[14]=z[3]*z[14];
    z[11]=n<T>(1,2)*z[11] + z[14];
    z[11]=z[3]*z[11];
    z[12]=z[12] + z[13];
    z[13]=z[1]*z[23];
    z[13]=5*z[16] - 3*z[13];
    z[13]=z[13]*z[18];
    z[14]= - z[5]*z[16];
    z[13]=z[14] + z[13];
    z[13]=z[1]*z[13];
    z[12]=n<T>(1,4)*z[12] + z[13];
    z[10]=z[10] + n<T>(1,2)*z[12] + z[11];
    z[10]=z[2]*z[10];
    z[11]=z[6] + 1;
    z[11]=z[11]*z[7];
    z[11]=z[11] - 1;
    z[12]=z[4]*z[11];
    z[12]=z[12] + z[7];
    z[12]=z[5]*z[12];
    z[12]=z[12] - z[11];
    z[12]=z[4]*z[12];
    z[12]= - z[7] + z[12];
    z[12]=z[4]*z[12];
    z[13]=z[7]*z[6];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[3]*z[5]*z[13];
    z[11]=z[13] + z[11];
    z[11]=z[3]*z[11];
    z[13]=z[7]*z[20];
    z[11]=n<T>(1,2)*z[13] + z[11];
    z[11]=z[3]*z[11];
    z[11]=z[12] + z[11];

    r += z[10] + n<T>(1,2)*z[11];
 
    return r;
}

template double qg_2lha_r1699(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1699(const std::array<dd_real,30>&);
#endif
