#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1158(const std::array<T,30>& k) {
  T z[39];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[27];
    z[6]=k[6];
    z[7]=k[2];
    z[8]=k[10];
    z[9]=k[14];
    z[10]=k[9];
    z[11]=k[3];
    z[12]=k[11];
    z[13]=z[2]*z[6];
    z[14]=z[5] - 1;
    z[15]=z[10] + z[14];
    z[15]=z[5]*z[10]*z[15];
    z[15]=z[13] + z[15];
    z[16]=z[4] - 1;
    z[17]=n<T>(1,3)*z[6];
    z[18]= - z[17]*z[16];
    z[19]=n<T>(1,3)*z[9];
    z[20]=z[19]*z[10];
    z[21]= - static_cast<T>(1)+ n<T>(7,8)*z[9];
    z[22]=n<T>(7,8)*z[10] + z[21];
    z[22]=z[22]*z[20];
    z[21]=z[21]*z[19];
    z[23]=n<T>(1,6) + z[2];
    z[21]=n<T>(1,4)*z[23] + z[21];
    z[21]=z[3]*z[21];
    z[23]=n<T>(1,3)*z[10];
    z[24]=z[4]*z[23];
    z[15]=z[21] + z[22] + z[24] + z[18] + n<T>(1,8)*z[15];
    z[15]=z[8]*z[15];
    z[18]=npow(z[9],2);
    z[21]=n<T>(1,2) - z[19];
    z[21]=z[21]*z[18];
    z[22]=n<T>(1,2)*z[9];
    z[24]=static_cast<T>(1)- z[19];
    z[24]=z[24]*z[22];
    z[25]=n<T>(1,2)*z[2];
    z[26]=z[25] - 1;
    z[24]=n<T>(1,3)*z[26] + z[24];
    z[24]=z[3]*z[24];
    z[21]=z[21] + z[24];
    z[21]=z[3]*z[21];
    z[23]=static_cast<T>(1)- z[23];
    z[23]=n<T>(1,2)*z[23] - z[19];
    z[18]=z[18]*z[10]*z[23];
    z[18]=z[18] + z[21];
    z[18]=z[8]*z[18];
    z[21]=npow(z[3],2);
    z[18]=n<T>(1,6)*z[21] + z[18];
    z[18]=z[7]*z[18];
    z[15]=z[15] + z[18];
    z[18]=n<T>(1,2)*z[6];
    z[21]=z[18] - 1;
    z[23]=z[21] + n<T>(1,2)*z[13];
    z[22]=z[23]*z[22];
    z[24]=n<T>(1,4)*z[13];
    z[22]=z[22] - z[24] - z[21];
    z[22]=z[9]*z[22];
    z[22]=n<T>(1,2)*z[21] + z[22];
    z[22]=z[3]*z[22];
    z[27]= - n<T>(11,8) - z[6];
    z[27]=z[6]*z[27];
    z[22]=z[22] + n<T>(11,8) + z[27];
    z[27]= - static_cast<T>(29)+ 11*z[6];
    z[28]=n<T>(1,6)*z[6];
    z[27]=z[27]*z[28];
    z[27]=z[27] + 3;
    z[28]=z[2] + n<T>(7,4);
    z[29]=n<T>(1,3)*z[2];
    z[28]=z[28]*z[29];
    z[27]= - z[28] + n<T>(1,2)*z[27];
    z[28]= - z[4]*z[27];
    z[30]= - n<T>(17,3) + n<T>(9,2)*z[6];
    z[30]=z[30]*z[18];
    z[28]=z[28] + z[30] + z[2];
    z[30]=n<T>(1,4)*z[4];
    z[28]=z[28]*z[30];
    z[31]=z[23]*z[9];
    z[31]= - z[31] + n<T>(17,16)*z[6] - static_cast<T>(2)+ n<T>(5,4)*z[13];
    z[19]= - z[31]*z[19];
    z[32]= - static_cast<T>(2)+ n<T>(31,16)*z[6];
    z[19]=z[19] + n<T>(1,3)*z[32] + z[24];
    z[19]=z[9]*z[19];
    z[19]=z[19] + z[28] + n<T>(1,3)*z[22];
    z[19]=z[3]*z[19];
    z[22]= - n<T>(5,6) + z[2];
    z[24]= - n<T>(25,48) - z[2];
    z[24]=z[2]*z[24];
    z[24]=n<T>(35,48) + z[24];
    z[24]=z[4]*z[24];
    z[22]=n<T>(1,2)*z[22] + z[24];
    z[22]=z[4]*z[22];
    z[24]= - z[6] - 13*z[13];
    z[28]=static_cast<T>(13)+ 9*z[2];
    z[28]=z[4]*z[28];
    z[24]=n<T>(1,3)*z[24] + z[28];
    z[28]=n<T>(1,16)*z[10];
    z[24]=z[24]*z[28];
    z[32]=n<T>(1,12)*z[13];
    z[22]=z[24] + z[32] + z[22];
    z[22]=z[10]*z[22];
    z[24]=npow(z[6],2);
    z[25]=z[25]*z[24];
    z[33]=z[6] + n<T>(1,2);
    z[33]=z[33]*z[6];
    z[25]=z[33] - z[25];
    z[33]=n<T>(1,2)*z[4];
    z[33]=z[33]*z[24];
    z[34]=static_cast<T>(1)- n<T>(1,3)*z[4];
    z[34]=z[34]*z[33];
    z[34]= - n<T>(1,3)*z[25] + z[34];
    z[34]=z[9]*z[34];
    z[35]=z[24]*z[2];
    z[24]=5*z[24] + z[35];
    z[24]=z[8]*z[24];
    z[36]= - n<T>(3,4) - z[29];
    z[36]=z[2]*z[36];
    z[36]= - n<T>(5,12) + z[36];
    z[36]=z[4]*z[36];
    z[36]=n<T>(1,12)*z[2] + z[36];
    z[36]=z[4]*z[36];
    z[32]=z[32] + z[36];
    z[32]=z[11]*z[32]*npow(z[10],2);
    z[22]=z[32] + n<T>(1,48)*z[24] + z[22] + n<T>(5,2)*z[34];
    z[22]=z[11]*z[22];
    z[24]= - z[27]*z[30];
    z[27]=n<T>(1,4)*z[2];
    z[32]= - n<T>(7,4) + z[6];
    z[32]=z[6]*z[32];
    z[32]= - z[27] + n<T>(5,16) + z[32];
    z[24]=n<T>(1,3)*z[32] + z[24];
    z[24]=z[3]*z[24];
    z[32]= - static_cast<T>(1)+ n<T>(1,4)*z[6];
    z[32]=z[32]*z[17];
    z[26]=z[26]*z[2];
    z[26]=z[32] + n<T>(1,4) + n<T>(1,6)*z[26];
    z[26]=z[26]*z[4];
    z[17]=static_cast<T>(1)- z[17];
    z[17]=z[6]*z[17];
    z[17]=z[17] - z[29];
    z[17]=n<T>(1,4)*z[17] + z[26];
    z[17]=z[5]*z[17];
    z[17]=z[24] + 5*z[17];
    z[17]=z[1]*z[17];
    z[24]=static_cast<T>(71)- n<T>(133,2)*z[6];
    z[24]=z[24]*z[18];
    z[24]=5*z[2] - static_cast<T>(5)+ z[24];
    z[32]=n<T>(11,16) - 2*z[2];
    z[32]=z[32]*z[29];
    z[34]= - n<T>(11,6) + z[6];
    z[34]=z[6]*z[34];
    z[34]=n<T>(5,6) + z[34];
    z[32]=n<T>(11,8)*z[34] + z[32];
    z[32]=z[4]*z[32];
    z[17]=z[17] + n<T>(1,12)*z[24] + z[32];
    z[17]=z[4]*z[17];
    z[24]= - static_cast<T>(2)+ n<T>(5,4)*z[6];
    z[32]=z[6]*z[24];
    z[32]=z[32] + n<T>(5,4)*z[35];
    z[32]=z[2]*z[32];
    z[34]=n<T>(23,4)*z[6];
    z[36]=static_cast<T>(5)- z[34];
    z[32]=n<T>(1,4)*z[36] + z[32];
    z[36]=n<T>(1,4) - n<T>(5,3)*z[2];
    z[36]=z[2]*z[36];
    z[36]= - n<T>(47,12) + z[36];
    z[30]=z[36]*z[30];
    z[36]=7*z[6] + z[13];
    z[36]=z[36]*z[27];
    z[36]=z[36] - n<T>(5,3) + z[18];
    z[37]=n<T>(1,4)*z[10];
    z[36]=z[36]*z[37];
    z[30]=z[36] + n<T>(1,3)*z[32] + z[30];
    z[30]=z[10]*z[30];
    z[32]=n<T>(1,3)*z[13];
    z[24]= - z[24]*z[32];
    z[36]= - static_cast<T>(1)+ n<T>(31,12)*z[6];
    z[38]= - z[32] + n<T>(1,3) - n<T>(3,16)*z[6];
    z[38]=z[10]*z[38];
    z[24]=z[38] + n<T>(1,4)*z[36] + z[24];
    z[24]=z[10]*z[24];
    z[23]=z[10]*z[23];
    z[23]=n<T>(1,2)*z[23] - z[31];
    z[20]=z[23]*z[20];
    z[23]=z[6] - n<T>(1,2);
    z[31]= - z[23]*z[13];
    z[36]= - static_cast<T>(1)+ z[6];
    z[36]=z[6]*z[36];
    z[31]=z[31] - n<T>(1,2) + z[36];
    z[21]= - z[4]*z[6]*z[21];
    z[21]=n<T>(1,2)*z[31] + z[21];
    z[20]=z[20] + n<T>(5,3)*z[21] + z[24];
    z[20]=z[9]*z[20];
    z[21]= - n<T>(5,3) + z[6];
    z[21]=z[21]*z[18];
    z[23]= - z[23]*z[32];
    z[21]=z[21] + z[23];
    z[23]=static_cast<T>(1)+ z[2];
    z[23]=z[23]*z[27];
    z[24]=n<T>(11,4) - z[6];
    z[24]=z[6]*z[24];
    z[23]=z[23] - n<T>(5,4) + z[24];
    z[23]=n<T>(1,3)*z[23] + z[26];
    z[23]=z[4]*z[23];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[23]= - 5*z[35] + static_cast<T>(5)+ z[34];
    z[23]=z[23]*z[29];
    z[23]= - n<T>(1,4) + z[23];
    z[23]=z[2]*z[23];
    z[13]=z[13] - 1;
    z[24]=z[13]*z[2];
    z[26]=z[24] + 1;
    z[26]=z[26]*z[2];
    z[26]=z[26] - 1;
    z[27]= - z[26]*z[37];
    z[23]=z[23] + z[27];
    z[23]=z[23]*z[37];
    z[26]=z[26]*z[5];
    z[27]= - z[28]*z[26];
    z[21]=z[27] + 5*z[21] + z[23];
    z[21]=z[5]*z[21];
    z[14]= - z[8]*z[14];
    z[14]=z[14] - z[24] + z[26];
    z[14]=z[5]*z[14];
    z[16]=z[6]*z[16];
    z[13]=z[13] + z[16];
    z[13]=5*z[13] + z[14];
    z[14]=static_cast<T>(3)- z[4];
    z[14]=z[14]*z[33];
    z[14]=z[14] - z[25];
    z[14]=z[11]*z[14];
    z[13]=n<T>(1,2)*z[13] + 5*z[14];
    z[13]=z[12]*z[13];
    z[14]= - n<T>(5,6) + 3*z[6];
    z[14]=z[14]*z[18];

    r += n<T>(1,8)*z[13] + z[14] + n<T>(1,2)*z[15] + z[17] + z[19] + z[20] + 
      z[21] + z[22] + z[30];
 
    return r;
}

template double qg_2lha_r1158(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1158(const std::array<dd_real,30>&);
#endif
