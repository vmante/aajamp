#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r781(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[2];
    z[2]=k[3];
    z[3]=k[5];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[7];
    z[7]=k[9];
    z[8]=k[10];
    z[9]=5*z[2] + static_cast<T>(1)+ z[8];
    z[9]=n<T>(1,2)*z[9] - z[1];
    z[10]=z[6] - n<T>(5,2) - z[1];
    z[10]=z[6]*z[10];
    z[11]=n<T>(1,2) - z[6];
    z[11]=z[6]*z[11];
    z[11]=n<T>(1,2) + z[11];
    z[11]=z[5]*z[11];
    z[9]=z[11] + n<T>(1,2)*z[9] + z[10];
    z[9]=z[5]*z[9];
    z[10]=z[4] + n<T>(5,2);
    z[11]=z[2]*z[4];
    z[11]= - n<T>(3,2)*z[11] - z[10];
    z[11]=z[2]*z[11];
    z[11]=z[11] - static_cast<T>(3)- n<T>(1,2)*z[8] + n<T>(3,2)*z[4];
    z[11]=z[1]*z[11];
    z[12]=3*z[2];
    z[13]= - static_cast<T>(1)+ z[12];
    z[13]=z[2]*z[13];
    z[13]=z[13] - static_cast<T>(5)+ z[8];
    z[11]=n<T>(1,2)*z[13] + z[11];
    z[13]=static_cast<T>(1)+ z[1];
    z[13]=z[6]*z[13];
    z[9]=z[9] + n<T>(1,2)*z[11] + z[13];
    z[9]=z[3]*z[9];
    z[11]=z[4]*z[12];
    z[10]=z[11] + z[10];
    z[10]=z[1]*z[10];
    z[11]= - z[12] + static_cast<T>(3)- z[8];
    z[10]=n<T>(1,2)*z[11] + z[10];
    z[11]=z[6] + n<T>(1,2);
    z[11]=z[11]*z[5]*z[6];
    z[12]=z[1] - 1;
    z[12]=z[12]*z[6];
    z[13]=z[12] - 1;
    z[14]= - z[6]*z[13];
    z[14]=z[14] + z[11];
    z[14]=z[5]*z[14];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[6]*z[12];
    z[9]=z[9] + z[14] + n<T>(1,2)*z[10] + z[12];
    z[9]=z[3]*z[9];
    z[10]=z[6]*z[7]*z[13];
    z[11]= - z[7]*z[11];
    z[10]=z[10] + z[11];
    z[10]=z[5]*z[10];
    z[11]=z[1]*z[4];
    z[9]=z[9] - n<T>(3,4)*z[11] + z[10];

    r += n<T>(1,4)*z[9];
 
    return r;
}

template double qg_2lha_r781(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r781(const std::array<dd_real,30>&);
#endif
