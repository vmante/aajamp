#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r598(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[9];
    z[7]=k[3];
    z[8]=k[6];
    z[9]=k[21];
    z[10]=k[27];
    z[11]=k[12];
    z[12]=7*z[4];
    z[13]= - 7*z[1] + z[12] + 3;
    z[14]=n<T>(1,2)*z[5];
    z[15]=z[13]*z[14];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[5]*z[15];
    z[16]=npow(z[5],2);
    z[17]=npow(z[3],2);
    z[17]= - n<T>(1,2)*z[16] + z[17];
    z[17]=z[7]*z[17];
    z[13]=z[3]*z[13];
    z[13]= - static_cast<T>(1)- n<T>(1,2)*z[13];
    z[13]=z[3]*z[13];
    z[13]=z[17] + z[15] + z[13];
    z[13]=z[7]*z[13];
    z[15]=z[5]*z[4];
    z[17]=z[4] + n<T>(1,2);
    z[18]= - z[15] + z[17];
    z[18]=z[5]*z[18];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[7]*z[15]*z[14];
    z[15]=z[18] + z[15];
    z[15]=z[7]*z[15];
    z[18]=n<T>(1,2)*z[4];
    z[19]=z[5]*z[18];
    z[19]= - z[4] + z[19];
    z[19]=z[5]*z[19];
    z[15]=z[19] + z[15];
    z[15]=z[8]*z[15];
    z[19]=z[3]*z[4];
    z[20]= - n<T>(1,2) + z[19];
    z[20]=z[7]*z[20];
    z[20]=z[17] + z[20];
    z[20]=z[3]*z[20];
    z[21]=n<T>(1,2)*z[7] - z[17];
    z[21]=z[8]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[7]*z[20];
    z[21]= - z[8]*z[4];
    z[19]=z[19] + z[21];
    z[19]=z[9]*z[19]*npow(z[7],2);
    z[19]=z[19] + z[20];
    z[19]=z[9]*z[19];
    z[13]=z[19] + z[13] + z[15];
    z[15]=npow(z[4],3);
    z[19]=z[15]*z[6];
    z[20]=npow(z[4],2);
    z[21]= - z[20] + n<T>(1,2)*z[19];
    z[22]=n<T>(1,2)*z[6];
    z[21]=z[21]*z[22];
    z[23]=z[18] + 1;
    z[24]=z[23]*z[20];
    z[25]= - z[24] + n<T>(1,4)*z[19];
    z[25]=z[5]*z[6]*z[25];
    z[21]=z[21] + z[25];
    z[21]=z[5]*z[21];
    z[25]=z[5] + 1;
    z[26]=z[3]*z[5];
    z[27]=z[26]*z[15]*z[25];
    z[24]=z[5]*z[24];
    z[24]=n<T>(1,2)*z[20] + z[24];
    z[24]=z[5]*z[24];
    z[24]=z[24] + n<T>(1,4)*z[27];
    z[24]=z[3]*z[24];
    z[19]=z[5]*z[19]*z[25];
    z[19]=z[19] - z[27];
    z[19]=z[10]*z[19];
    z[19]=n<T>(1,2)*z[19] + z[21] + z[24];
    z[19]=z[10]*z[19];
    z[14]= - static_cast<T>(1)- z[14];
    z[14]=z[26]*z[20]*z[14];
    z[21]=n<T>(5,2)*z[4];
    z[24]=z[21] + 1;
    z[25]=z[24]*z[4];
    z[26]= - z[16]*z[25];
    z[14]=z[26] + z[14];
    z[14]=z[3]*z[14];
    z[26]= - n<T>(3,2) - z[4];
    z[26]=z[6]*z[26]*z[20];
    z[25]=z[25] + z[26];
    z[25]=z[6]*z[25]*z[16];
    z[14]=z[25] + z[14];
    z[14]=n<T>(1,2)*z[14] + z[19];
    z[14]=z[10]*z[14];
    z[19]=n<T>(5,2)*z[1];
    z[25]=3*z[4];
    z[26]= - z[19] - n<T>(5,2) + z[25];
    z[27]=n<T>(1,4)*z[1];
    z[26]=z[26]*z[27];
    z[26]=z[26] + n<T>(85,32) + z[4];
    z[26]=z[1]*z[26];
    z[28]= - static_cast<T>(31)- 21*z[4];
    z[29]=z[1] - z[4];
    z[30]=n<T>(11,4) - z[29];
    z[30]=z[1]*z[30];
    z[28]=n<T>(1,4)*z[28] + 3*z[30];
    z[28]=z[1]*z[28];
    z[30]=z[4] + 1;
    z[28]=n<T>(5,2)*z[30] + z[28];
    z[31]=n<T>(1,2)*z[2];
    z[28]=z[28]*z[31];
    z[32]= - n<T>(7,2) - z[25];
    z[26]=z[28] + n<T>(7,16)*z[32] + z[26];
    z[26]=z[2]*z[26];
    z[28]=5*z[1];
    z[32]=z[1] - z[17];
    z[32]=z[32]*z[28];
    z[33]=5*z[4];
    z[34]= - n<T>(11,2) - z[33];
    z[32]=n<T>(3,2)*z[34] + z[32];
    z[32]=z[1]*z[32];
    z[34]=n<T>(43,4) + z[33];
    z[34]=z[4]*z[34];
    z[32]=z[32] + n<T>(23,4) + z[34];
    z[34]=n<T>(1,2)*z[1];
    z[35]=n<T>(3,2)*z[1] - n<T>(17,4) - z[25];
    z[35]=z[35]*z[34];
    z[36]=static_cast<T>(11)+ z[25];
    z[36]=z[4]*z[36];
    z[35]=z[35] + static_cast<T>(2)+ n<T>(1,4)*z[36];
    z[35]=z[1]*z[35];
    z[23]= - z[4]*z[23];
    z[23]= - n<T>(1,2) + z[23];
    z[23]=n<T>(5,4)*z[23] + z[35];
    z[23]=z[2]*z[23];
    z[23]=n<T>(1,8)*z[32] + z[23];
    z[23]=z[2]*z[23];
    z[32]= - z[1] + n<T>(1,2)*z[24];
    z[32]=z[32]*z[1];
    z[32]=z[32] + n<T>(1,8);
    z[32]=z[32]*z[1];
    z[35]= - n<T>(3,16)*z[30] - z[32];
    z[23]=n<T>(1,2)*z[35] + z[23];
    z[23]=z[6]*z[23];
    z[35]= - n<T>(5,8)*z[1] + z[4] + n<T>(1,4);
    z[35]=z[35]*z[1];
    z[23]=z[23] + z[26] + n<T>(9,32) + z[35];
    z[23]=z[6]*z[23];
    z[26]= - static_cast<T>(1)+ z[4];
    z[26]=z[4]*z[26];
    z[36]=n<T>(13,2)*z[1] - static_cast<T>(13)+ z[4];
    z[36]=z[1]*z[36];
    z[37]= - static_cast<T>(3)+ z[1];
    z[37]=z[1]*z[37];
    z[37]=static_cast<T>(3)+ z[37];
    z[37]=z[1]*z[37];
    z[37]= - static_cast<T>(1)+ z[37];
    z[37]=z[2]*z[37];
    z[26]=5*z[37] + z[36] + n<T>(13,2) + z[26];
    z[26]=z[26]*z[31];
    z[36]=z[18] - 1;
    z[26]=z[26] + z[1] + z[36];
    z[15]= - z[2]*z[15];
    z[15]= - z[20] + z[15];
    z[15]=z[11]*z[15];
    z[15]=z[23] + n<T>(1,8)*z[15] + n<T>(1,4)*z[26];
    z[15]=z[2]*z[15];
    z[18]=z[30]*z[18];
    z[23]=n<T>(1,2) - z[1];
    z[23]=z[1]*z[23];
    z[17]=n<T>(1,2)*z[17] + z[23];
    z[17]=z[1]*z[17];
    z[17]=z[18] + z[17];
    z[17]=z[17]*z[22];
    z[18]=z[34] - z[36];
    z[18]=z[1]*z[18];
    z[18]= - n<T>(5,8) + z[18];
    z[18]=z[1]*z[18];
    z[23]= - static_cast<T>(1)- z[33];
    z[17]=z[17] + n<T>(1,8)*z[23] + z[18];
    z[17]=z[6]*z[17];
    z[18]= - z[19] + z[24];
    z[18]=z[1]*z[18];
    z[19]=static_cast<T>(1)- z[12];
    z[18]=n<T>(1,4)*z[19] + z[18];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[17]=z[5]*z[17];
    z[18]=z[27] - n<T>(1,4) + z[25];
    z[19]=n<T>(1,16)*z[30] + z[32];
    z[19]=z[19]*z[22];
    z[19]=z[19] - n<T>(3,32) - z[35];
    z[19]=z[6]*z[19];
    z[17]=n<T>(1,4)*z[17] + n<T>(1,8)*z[18] + z[19];
    z[17]=z[5]*z[17];
    z[18]=z[30] - z[1];
    z[19]= - z[18]*z[28];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[1]*z[19];
    z[18]=npow(z[1],3)*z[18]*z[2];
    z[20]=z[20]*z[5];
    z[19]= - z[20] - z[18] + z[4] + z[19];
    z[19]=z[2]*z[19];
    z[20]= - static_cast<T>(7)+ 9*z[29];
    z[20]=z[1]*z[20];
    z[19]=static_cast<T>(1)+ z[20] + z[19];
    z[19]=z[3]*z[19];
    z[12]=n<T>(7,2)*z[1] - n<T>(3,2) - z[12];
    z[12]=z[12]*npow(z[1],2);
    z[12]=n<T>(1,2)*z[12] - z[18];
    z[12]=z[2]*z[12];
    z[18]=static_cast<T>(1)- z[25];
    z[18]=3*z[18] - z[1];
    z[18]=z[1]*z[18];
    z[18]= - n<T>(1,2) + z[18];
    z[12]=n<T>(1,2)*z[18] + z[12];
    z[12]=z[12]*z[31];
    z[16]=z[16]*z[4];
    z[18]=static_cast<T>(1)- z[21];
    z[12]=n<T>(1,8)*z[19] + n<T>(3,8)*z[16] + z[12] + n<T>(1,2)*z[18] - z[1];
    z[12]=z[3]*z[12];

    r += n<T>(1,4)*z[12] + n<T>(1,16)*z[13] + n<T>(1,8)*z[14] + z[15] + z[17];
 
    return r;
}

template double qg_2lha_r598(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r598(const std::array<dd_real,30>&);
#endif
