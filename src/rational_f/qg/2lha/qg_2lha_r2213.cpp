#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2213(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[27];
    z[5]=k[6];
    z[6]=k[3];
    z[7]=k[9];
    z[8]=k[11];
    z[9]=k[14];
    z[10]=n<T>(1,2)*z[2];
    z[11]=z[10] - 1;
    z[11]=z[11]*z[2];
    z[12]=z[11] + n<T>(3,2);
    z[13]=z[12]*z[1];
    z[14]=static_cast<T>(2)- z[2];
    z[14]=z[2]*z[14];
    z[13]= - z[13] - static_cast<T>(3)+ z[14];
    z[13]=z[1]*z[13];
    z[14]=n<T>(1,2)*z[1];
    z[15]=z[14] + 1;
    z[16]=z[15]*z[1];
    z[16]=z[16] + n<T>(1,2);
    z[17]=z[16]*z[5];
    z[18]=z[1] + 2;
    z[18]=z[18]*z[1];
    z[18]=z[18] + 1;
    z[19]=2*z[18] - z[17];
    z[19]=z[5]*z[19];
    z[13]=z[19] + z[13] - z[12];
    z[13]=z[4]*z[13];
    z[19]=z[10] + 1;
    z[19]=z[19]*z[2];
    z[20]=z[6]*z[7];
    z[21]=z[20] + 1;
    z[22]=z[21]*z[6];
    z[23]=z[22] + 1;
    z[23]=z[23]*z[19];
    z[24]=n<T>(1,4)*z[6];
    z[25]=3*z[9];
    z[26]=z[25] + z[8];
    z[24]=z[24]*z[26];
    z[27]=static_cast<T>(3)- z[24];
    z[27]=z[6]*z[27];
    z[28]=3*z[1];
    z[27]= - z[28] - static_cast<T>(3)+ z[27];
    z[29]=n<T>(1,2)*z[5];
    z[27]=z[27]*z[29];
    z[30]=2*z[1] + static_cast<T>(2)- z[6];
    z[27]=2*z[30] + z[27];
    z[27]=z[5]*z[27];
    z[22]= - static_cast<T>(5)+ z[22];
    z[30]= - n<T>(5,2) + z[19];
    z[30]=z[1]*z[30];
    z[13]=z[13] + z[27] + z[30] + n<T>(1,2)*z[22] + z[23];
    z[13]=z[3]*z[13];
    z[12]=z[12]*z[16];
    z[22]= - n<T>(1,4)*z[5] + 1;
    z[22]=z[22]*z[17];
    z[22]= - n<T>(1,2)*z[12] + z[22];
    z[23]=3*z[4];
    z[22]=z[22]*z[23];
    z[27]=z[1]*z[2];
    z[11]=z[27] + n<T>(11,2) + z[11];
    z[11]=z[11]*z[14];
    z[30]=n<T>(9,2) + z[1];
    z[30]=z[1]*z[30];
    z[30]=n<T>(7,2) + z[30];
    z[30]=z[30]*z[29];
    z[31]=n<T>(3,2)*z[1];
    z[32]= - static_cast<T>(7)- z[31];
    z[32]=z[1]*z[32];
    z[30]=z[30] - n<T>(11,2) + z[32];
    z[30]=z[5]*z[30];
    z[32]=n<T>(1,4)*z[2];
    z[33]= - static_cast<T>(1)+ z[32];
    z[33]=z[2]*z[33];
    z[11]=z[22] + z[30] + z[11] + n<T>(11,4) + z[33];
    z[11]=z[4]*z[11];
    z[22]= - static_cast<T>(3)- z[10];
    z[22]=z[10]*z[21]*z[22];
    z[30]=n<T>(3,4)*z[6];
    z[26]=z[30]*z[26];
    z[30]=n<T>(3,2)*z[9];
    z[33]=z[26] - static_cast<T>(7)- z[30];
    z[33]=z[6]*z[33];
    z[33]=5*z[1] + static_cast<T>(9)+ z[33];
    z[33]=z[33]*z[29];
    z[34]=n<T>(1,2)*z[8];
    z[35]=n<T>(7,2)*z[9] + static_cast<T>(5)+ z[34];
    z[35]=z[6]*z[35];
    z[35]= - static_cast<T>(19)+ z[35];
    z[33]=z[33] + n<T>(1,2)*z[35] - 4*z[1];
    z[33]=z[5]*z[33];
    z[11]=z[13] + z[11] + z[33] + z[14] + z[22] + static_cast<T>(4)- n<T>(5,4)*z[20];
    z[11]=z[3]*z[11];
    z[13]=z[2] + n<T>(1,2);
    z[13]=z[13]*z[2];
    z[20]=n<T>(3,4)*z[27] + static_cast<T>(1)+ z[13];
    z[20]=z[1]*z[20];
    z[22]= - static_cast<T>(11)- n<T>(9,2)*z[1];
    z[22]=z[1]*z[22];
    z[17]=3*z[17] - n<T>(13,2) + z[22];
    z[17]=z[17]*z[29];
    z[22]= - n<T>(1,4) + z[2];
    z[22]=z[2]*z[22];
    z[17]=z[17] + z[20] + static_cast<T>(1)+ z[22];
    z[16]= - z[16]*z[29];
    z[16]=z[16] + z[18];
    z[16]=z[5]*z[16];
    z[12]=z[16] - z[12];
    z[12]=z[12]*z[23];
    z[12]=n<T>(1,2)*z[17] + z[12];
    z[12]=z[4]*z[12];
    z[16]= - static_cast<T>(1)- 3*z[2];
    z[16]=z[16]*z[14];
    z[13]=z[16] - n<T>(13,2) - z[13];
    z[16]=static_cast<T>(47)+ 25*z[1];
    z[17]= - n<T>(11,2) - z[28];
    z[17]=z[5]*z[17];
    z[16]=n<T>(1,4)*z[16] + z[17];
    z[16]=z[5]*z[16];
    z[13]=n<T>(1,2)*z[13] + z[16];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[4]*z[12];
    z[13]= - z[26] + static_cast<T>(5)+ z[25];
    z[13]=z[6]*z[13];
    z[16]= - static_cast<T>(17)- z[30];
    z[13]=n<T>(1,2)*z[16] + z[13];
    z[13]=n<T>(1,2)*z[13] - z[1];
    z[13]=z[5]*z[13];
    z[16]= - n<T>(19,4)*z[9] - static_cast<T>(1)- z[8];
    z[16]=z[6]*z[16];
    z[13]=z[13] + z[14] + n<T>(1,2)*z[16] + n<T>(49,8) + z[9];
    z[13]=z[5]*z[13];
    z[16]=static_cast<T>(3)+ z[2];
    z[16]=z[32]*z[16];
    z[16]=z[16] + 1;
    z[16]=z[7]*z[16];
    z[17]= - static_cast<T>(7)- z[8];
    z[11]=z[11] + z[12] + z[13] - z[30] + n<T>(1,8)*z[17] + z[16];
    z[11]=z[3]*z[11];
    z[12]= - static_cast<T>(1)- z[2];
    z[12]=z[12]*z[10];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[2]*z[12];
    z[13]=static_cast<T>(5)- z[2];
    z[13]=z[13]*z[10];
    z[13]= - z[31] - static_cast<T>(3)+ z[13];
    z[13]=z[1]*z[13];
    z[16]=z[2] - 1;
    z[16]=z[16]*z[2];
    z[15]=z[16] + z[15];
    z[15]=z[1]*z[15];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[2]*z[16];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[2]*z[16];
    z[16]=static_cast<T>(1)+ z[16];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[5]*z[15];
    z[12]=z[15] + z[13] - n<T>(3,2) + z[12];
    z[12]=z[5]*z[12];
    z[13]= - static_cast<T>(1)- z[6];
    z[13]=z[2]*z[13];
    z[13]=z[13] + z[27];
    z[13]=z[13]*z[14];
    z[12]=z[13] + z[12];
    z[12]=z[12]*z[23];
    z[13]= - n<T>(1,2) + 5*z[2];
    z[10]=z[13]*z[10];
    z[13]=static_cast<T>(1)- n<T>(3,2)*z[2];
    z[13]=z[2]*z[13];
    z[13]= - n<T>(1,2) + z[13];
    z[13]=z[2]*z[13];
    z[13]=z[13] - z[27];
    z[13]=z[5]*z[13];
    z[15]=static_cast<T>(1)+ n<T>(3,4)*z[2];
    z[15]=z[1]*z[15];
    z[10]=n<T>(3,2)*z[13] + z[15] + static_cast<T>(1)+ z[10];
    z[10]=z[5]*z[10];
    z[13]=z[21]*npow(z[2],2);
    z[10]=z[12] + z[10] + z[13] - n<T>(7,4)*z[27];
    z[10]=z[4]*z[10];
    z[12]= - n<T>(3,2) + z[2];
    z[12]=z[2]*z[12];
    z[12]=z[14] + static_cast<T>(3)+ z[12];
    z[12]=z[5]*z[12];
    z[13]= - z[1] - static_cast<T>(13)- z[2];
    z[12]=n<T>(1,4)*z[13] + z[12];
    z[12]=z[5]*z[12];
    z[13]= - z[7]*z[19];
    z[10]=z[10] + z[13] + z[12];
    z[10]=z[4]*z[10];
    z[12]=z[24] - static_cast<T>(1)- z[30];
    z[12]=z[6]*z[12];
    z[13]=static_cast<T>(5)+ z[30];
    z[12]= - z[32] + n<T>(1,2)*z[13] + z[12];
    z[12]=z[5]*z[12];
    z[13]= - static_cast<T>(7)- 5*z[9];
    z[14]=z[8] + n<T>(5,2)*z[9];
    z[14]=z[6]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[5]*z[12];
    z[13]=static_cast<T>(1)- z[7];
    z[13]=z[9]*z[13];
    z[13]=z[34] + z[13];
    z[10]=z[10] + n<T>(1,2)*z[13] + z[12];

    r += n<T>(1,2)*z[10] + z[11];
 
    return r;
}

template double qg_2lha_r2213(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2213(const std::array<dd_real,30>&);
#endif
