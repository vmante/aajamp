#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r53(const std::array<T,30>& k) {
  T z[6];
  T r = 0;

    z[1]=k[6];
    z[2]=k[8];
    z[3]=k[9];
    z[4]=n<T>(1,2)*z[1];
    z[5]= - z[2]*z[4];
    z[5]= - z[1] + z[5];
    z[5]=z[2]*z[5];
    z[4]= - z[4] + z[5];

    r += n<T>(1,4)*z[4]*npow(z[3],2);
 
    return r;
}

template double qg_2lha_r53(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r53(const std::array<dd_real,30>&);
#endif
