#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r9(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[9];
    z[4]=k[12];
    z[5]=k[10];
    z[6]=k[27];
    z[7]=k[14];
    z[8]=n<T>(1,2)*z[2];
    z[9]=z[2]*z[6];
    z[10]= - z[9] + static_cast<T>(1)- z[6];
    z[8]=z[10]*z[8];
    z[10]=n<T>(1,2)*z[3];
    z[11]= - z[7] - z[6];
    z[11]=z[11]*z[10];
    z[12]=static_cast<T>(1)+ z[1];
    z[13]=n<T>(1,2)*z[9];
    z[14]=static_cast<T>(1)- z[13];
    z[14]=z[2]*z[14];
    z[12]=n<T>(1,2)*z[12] + z[14];
    z[12]=z[2]*z[12];
    z[14]=z[7] - 1;
    z[15]=z[1] + z[14];
    z[12]=n<T>(1,2)*z[15] + z[12];
    z[12]=z[4]*z[12];
    z[15]=z[6] + z[1];
    z[15]=n<T>(1,2)*z[15];
    z[8]=z[12] + z[11] + z[8] - n<T>(1,2) + z[7] + z[15];
    z[8]=z[4]*z[8];
    z[11]= - z[3] + 1;
    z[12]=z[14] - z[6];
    z[11]=z[12]*z[11];
    z[8]=n<T>(1,2)*z[11] + z[8];
    z[8]=z[5]*z[8];
    z[11]= - z[14] - z[15];
    z[11]=z[3]*z[11];
    z[14]=z[1] + z[2] - n<T>(1,2);
    z[15]= - z[4]*z[14];
    z[11]=z[15] + z[13] + z[11];
    z[11]=z[4]*z[11];
    z[12]= - z[12]*z[10];
    z[8]=z[8] + z[12] + z[11];
    z[8]=z[5]*z[8];
    z[9]= - z[9] - z[3];
    z[9]=z[9]*z[10];
    z[11]=static_cast<T>(1)+ z[2];
    z[10]=z[10]*z[1]*z[11];
    z[10]=z[10] + z[14];
    z[10]=z[4]*z[3]*z[10];
    z[9]=z[9] + z[10];
    z[9]=z[4]*z[9];
    z[8]=z[9] + z[8];

    r += n<T>(1,12)*z[8];
 
    return r;
}

template double qg_2lha_r9(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r9(const std::array<dd_real,30>&);
#endif
