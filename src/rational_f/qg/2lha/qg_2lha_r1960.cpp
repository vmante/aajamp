#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1960(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[12];
    z[8]=k[28];
    z[9]=k[11];
    z[10]=k[5];
    z[11]=k[9];
    z[12]=z[11] - 1;
    z[13]=z[1] + 1;
    z[14]= - z[4]*z[13];
    z[14]=z[14] + z[1] - z[12];
    z[14]=z[4]*z[14];
    z[14]=z[14] - z[1] - z[12];
    z[14]=z[3]*z[14];
    z[15]=z[11] - n<T>(1,2);
    z[16]=n<T>(3,4)*z[1];
    z[17]= - z[16] - n<T>(5,4) + z[11];
    z[17]=z[4]*z[17];
    z[14]=n<T>(1,2)*z[14] + z[17] + z[16] + z[15];
    z[14]=z[3]*z[14];
    z[17]=n<T>(1,2)*z[10];
    z[18]=z[17]*z[12];
    z[19]=z[4]*z[9];
    z[20]=static_cast<T>(1)- 3*z[9];
    z[20]= - n<T>(5,2)*z[19] + n<T>(1,2)*z[20] - z[11];
    z[21]=n<T>(1,2)*z[1];
    z[22]= - z[21] + n<T>(3,2) - z[11];
    z[22]=z[3]*z[22];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[20]=z[3]*z[20];
    z[19]= - z[19] - static_cast<T>(1)- z[9];
    z[19]=z[3]*z[19];
    z[19]=n<T>(5,2)*z[9] + z[19];
    z[19]=z[3]*z[19];
    z[22]=z[2]*z[9]*npow(z[3],2);
    z[19]=z[19] + z[22];
    z[19]=z[2]*z[19];
    z[18]=n<T>(1,2)*z[19] + z[20] + z[9] + z[18];
    z[18]=z[2]*z[18];
    z[15]= - z[21] + z[15];
    z[15]=z[4]*z[15];
    z[12]=z[15] + n<T>(3,2)*z[12] + z[1];
    z[12]=z[3]*z[12];
    z[15]=n<T>(1,2)*z[4];
    z[19]= - z[15] - 1;
    z[19]=z[11]*z[19];
    z[12]=z[12] - z[16] + n<T>(1,4) + z[19];
    z[12]=z[3]*z[12];
    z[19]= - z[10] + n<T>(1,2)*z[9];
    z[19]=z[19]*z[5];
    z[17]=z[17] - z[9];
    z[17]=z[4]*z[17];
    z[12]=z[18] + z[12] + z[17] + n<T>(1,2)*z[19] + static_cast<T>(1)- n<T>(1,4)*z[9];
    z[12]=z[2]*z[12];
    z[17]= - z[11] - z[10] - z[19];
    z[17]=z[17]*z[15];
    z[12]=z[12] + z[14] + z[17] + z[16] + n<T>(1,4)*z[11] - static_cast<T>(1)- n<T>(3,4)*z[5];
    z[12]=z[2]*z[12];
    z[14]=z[6] - 1;
    z[16]=z[5]*z[6];
    z[17]=n<T>(1,2)*z[16] + n<T>(1,2) - z[6];
    z[17]=z[5]*z[17];
    z[17]=n<T>(1,2)*z[14] + z[17];
    z[17]=z[11]*z[17];
    z[14]=z[14] - z[16];
    z[16]= - z[14]*z[21];
    z[18]= - z[7] + z[6];
    z[18]=z[4]*z[18];
    z[18]=z[18] - z[7];
    z[18]=z[8]*z[18];
    z[19]= - static_cast<T>(1)+ z[8];
    z[19]=z[6]*z[19];
    z[18]=z[19] + z[18];
    z[15]=z[15]*z[1]*z[18];
    z[15]=z[15] + z[17] + z[16];
    z[15]=z[4]*z[15];
    z[16]= - npow(z[5],2);
    z[17]=z[1]*z[5];
    z[16]=z[16] + z[17];
    z[14]=z[14]*z[16];
    z[14]=n<T>(1,2)*z[14] + z[15];
    z[15]=static_cast<T>(5)+ 3*z[1];
    z[16]=z[7] - 1;
    z[15]=z[16]*z[15];
    z[17]=z[1]*z[7];
    z[18]=5*z[7] + 3*z[17];
    z[18]=z[4]*z[18];
    z[15]=z[18] + z[15];
    z[15]=z[15]*npow(z[4],2);
    z[13]=z[16]*z[13];
    z[16]=z[7] + z[17];
    z[16]=z[4]*z[16];
    z[13]=z[16] + z[13];
    z[13]=z[3]*z[13]*npow(z[4],3);
    z[13]=n<T>(1,2)*z[15] + z[13];
    z[13]=z[3]*z[13];
    z[13]=3*z[14] + z[13];
    z[12]=n<T>(1,2)*z[13] + z[12];

    r += n<T>(1,2)*z[12];
 
    return r;
}

template double qg_2lha_r1960(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1960(const std::array<dd_real,30>&);
#endif
