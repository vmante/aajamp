#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r577(const std::array<T,30>& k) {
  T z[21];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[6];
    z[6]=k[9];
    z[7]=k[3];
    z[8]=z[1] - 1;
    z[9]=z[8]*z[1];
    z[10]=z[6] - 1;
    z[9]=z[9] - z[10];
    z[11]=n<T>(1,2)*z[4];
    z[12]=z[11] + 1;
    z[12]=z[10]*z[12];
    z[12]=z[1] + z[12];
    z[12]=z[4]*z[12];
    z[12]= - n<T>(1,2)*z[9] + z[12];
    z[13]=z[10] + z[1];
    z[14]= - z[4]*z[13];
    z[14]=z[14] + z[9];
    z[15]= - z[2]*z[9];
    z[14]=3*z[14] + z[15];
    z[15]=n<T>(1,2)*z[2];
    z[14]=z[14]*z[15];
    z[12]=3*z[12] + z[14];
    z[12]=z[2]*z[12];
    z[14]=z[4]*z[6];
    z[16]= - 3*z[10] - z[14];
    z[16]=z[4]*z[16];
    z[13]= - 3*z[13] + z[16];
    z[13]=z[4]*z[13];
    z[9]=z[13] + z[9];
    z[9]=n<T>(1,2)*z[9] + z[12];
    z[9]=z[3]*z[9];
    z[12]=n<T>(1,2)*z[1];
    z[13]=n<T>(3,2)*z[1];
    z[16]=static_cast<T>(1)- z[13];
    z[16]=z[16]*z[12];
    z[16]=z[16] - static_cast<T>(1)+ n<T>(3,2)*z[6];
    z[17]=5*z[6];
    z[18]=z[14] - n<T>(7,2) + z[17];
    z[19]=n<T>(1,4)*z[4];
    z[18]=z[18]*z[19];
    z[18]=z[18] + n<T>(7,4)*z[6] + z[8];
    z[18]=z[4]*z[18];
    z[16]=n<T>(1,2)*z[16] + z[18];
    z[18]= - static_cast<T>(1)- z[12];
    z[18]=z[1]*z[18];
    z[18]=z[6] + z[18];
    z[8]=n<T>(1,2)*z[6] - z[8];
    z[8]=z[4]*z[8];
    z[20]= - n<T>(1,2) + z[1];
    z[20]=z[2]*z[20];
    z[8]=z[20] + n<T>(3,2)*z[18] + z[8];
    z[8]=z[2]*z[8];
    z[18]= - n<T>(1,2) - z[6];
    z[18]=z[18]*z[19];
    z[18]=z[18] - n<T>(1,4)*z[1] + n<T>(1,4) - z[6];
    z[18]=z[4]*z[18];
    z[19]=npow(z[1],2);
    z[19]=n<T>(1,2)*z[19] + n<T>(1,2) - z[6];
    z[8]=n<T>(1,4)*z[8] + n<T>(3,4)*z[19] + z[18];
    z[8]=z[2]*z[8];
    z[8]=n<T>(1,4)*z[9] + n<T>(1,2)*z[16] + z[8];
    z[8]=z[3]*z[8];
    z[9]= - static_cast<T>(7)- z[1];
    z[9]=z[9]*z[12];
    z[16]=3*z[6];
    z[9]=z[16] + z[9];
    z[18]= - z[11] + z[6] - z[13];
    z[18]=z[4]*z[18];
    z[13]= - z[15] + z[13] + z[4];
    z[13]=z[2]*z[13];
    z[9]=z[13] + n<T>(1,2)*z[9] + z[18];
    z[9]=z[2]*z[9];
    z[13]=static_cast<T>(1)+ z[1];
    z[13]=z[13]*z[12];
    z[13]=z[13] + static_cast<T>(1)- z[16];
    z[15]= - z[12] + n<T>(1,2) - z[17];
    z[14]=n<T>(1,2)*z[15] - z[14];
    z[14]=z[4]*z[14];
    z[9]=z[9] + n<T>(1,2)*z[13] + z[14];
    z[8]=n<T>(1,4)*z[9] + z[8];
    z[8]=z[3]*z[8];
    z[9]=z[7] - 1;
    z[13]= - z[5]*z[9];
    z[13]= - static_cast<T>(3)+ z[13];
    z[11]=z[13]*z[11];
    z[13]= - static_cast<T>(1)- n<T>(1,2)*z[5];
    z[13]=z[1]*z[13];
    z[10]=z[11] + z[13] + z[7] + z[10];
    z[10]=z[4]*z[10];
    z[9]= - n<T>(3,2)*z[2] + 3*z[4] - n<T>(1,2)*z[9] + z[1];
    z[9]=z[2]*z[9];
    z[9]=z[9] + z[10] + z[6] - z[12];
    z[8]=n<T>(1,8)*z[9] + z[8];

    r += n<T>(3,16)*z[8];
 
    return r;
}

template double qg_2lha_r577(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r577(const std::array<dd_real,30>&);
#endif
