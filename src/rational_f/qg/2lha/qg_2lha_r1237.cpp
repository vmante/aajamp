#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1237(const std::array<T,30>& k) {
  T z[69];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[4];
    z[4]=k[27];
    z[5]=k[7];
    z[6]=k[12];
    z[7]=k[6];
    z[8]=k[8];
    z[9]=k[2];
    z[10]=k[11];
    z[11]=k[9];
    z[12]=k[10];
    z[13]=k[14];
    z[14]=npow(z[3],2);
    z[15]=z[14] - 1;
    z[16]=npow(z[7],2);
    z[17]=z[16]*z[2];
    z[18]=n<T>(1,4)*z[3];
    z[19]= - z[15]*z[18]*z[17];
    z[20]=z[14]*z[7];
    z[21]=n<T>(1,2)*z[3];
    z[22]=z[21] - 1;
    z[23]= - z[22]*z[20];
    z[19]=z[23] + z[19];
    z[19]=z[2]*z[19];
    z[23]=static_cast<T>(1)- z[18];
    z[23]=z[3]*z[23];
    z[23]= - n<T>(7,4) + z[23];
    z[23]=z[3]*z[23];
    z[19]=z[19] + n<T>(5,2) + z[23];
    z[23]=n<T>(1,4)*z[2];
    z[24]=z[23] - 1;
    z[24]=z[24]*z[2];
    z[25]=npow(z[2],2);
    z[26]=z[25]*z[4];
    z[24]=z[24] - n<T>(1,2)*z[26];
    z[27]=n<T>(1,2)*z[4];
    z[24]=z[24]*z[27];
    z[24]=z[24] + z[2];
    z[28]= - z[25] + z[26];
    z[28]=z[10]*z[28];
    z[28]=n<T>(1,8)*z[28] + z[24];
    z[28]=z[4]*z[28];
    z[29]=n<T>(1,2)*z[2];
    z[28]= - z[29] + z[28];
    z[28]=z[10]*z[28];
    z[24]= - z[4]*z[24];
    z[30]=n<T>(1,2)*z[11];
    z[31]=z[2] + n<T>(1,4)*z[26];
    z[31]=z[4]*z[31];
    z[31]=n<T>(3,4) + z[31];
    z[31]=z[31]*z[30];
    z[24]=z[31] - n<T>(13,8) + z[24];
    z[24]=z[11]*z[24];
    z[19]=z[24] + n<T>(1,2)*z[19] + z[28];
    z[19]=z[12]*z[19];
    z[24]=z[21] + 3;
    z[28]=z[3]*z[24];
    z[28]=n<T>(11,2) + z[28];
    z[28]=z[28]*z[21];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=z[7]*z[28];
    z[31]=z[21] - 3;
    z[32]=z[3]*z[31];
    z[28]=z[28] + static_cast<T>(1)+ z[32];
    z[28]=z[7]*z[28];
    z[32]=z[3] - 1;
    z[33]=z[32]*z[21];
    z[34]=z[17]*z[33];
    z[28]=z[28] + z[34];
    z[28]=z[2]*z[28];
    z[34]=z[3] + 7;
    z[35]=z[34]*z[18];
    z[35]=static_cast<T>(1)+ z[35];
    z[36]=z[7]*z[3];
    z[35]=z[35]*z[36];
    z[37]=5*z[3];
    z[38]=n<T>(17,2) - z[37];
    z[28]=z[28] + n<T>(1,2)*z[38] + z[35];
    z[35]=3*z[2];
    z[38]=z[3] + 1;
    z[39]=z[38]*z[6];
    z[40]=z[39] - static_cast<T>(1)+ z[35];
    z[41]=n<T>(3,2)*z[2];
    z[42]=static_cast<T>(1)- z[41];
    z[42]=z[42]*z[29];
    z[42]=z[42] + z[26];
    z[42]=z[4]*z[42];
    z[40]=z[42] - n<T>(1,4)*z[40];
    z[40]=z[10]*z[40];
    z[42]=n<T>(3,4)*z[3];
    z[40]=z[40] - z[42] - z[2];
    z[41]=z[41] - 7;
    z[41]=z[41]*z[23];
    z[41]=z[41] - z[26];
    z[41]=z[41]*z[4];
    z[43]= - static_cast<T>(1)+ n<T>(17,2)*z[2];
    z[41]=z[41] + n<T>(1,4)*z[43];
    z[41]=z[41]*z[4];
    z[39]=n<T>(3,8)*z[39] + z[41] + n<T>(1,2)*z[40];
    z[39]=z[10]*z[39];
    z[35]=z[35] + z[26];
    z[35]=z[4]*z[35];
    z[35]=n<T>(5,2) + z[35];
    z[30]=z[35]*z[30];
    z[35]=z[6]*z[2];
    z[30]=z[30] + z[35] - n<T>(17,4) - z[41];
    z[30]=z[11]*z[30];
    z[33]= - static_cast<T>(1)+ z[33];
    z[35]=n<T>(1,8)*z[38] - z[7];
    z[35]=z[2]*z[35];
    z[33]=n<T>(1,4)*z[33] + z[35];
    z[33]=z[6]*z[33];
    z[19]=z[19] + z[30] + z[39] + n<T>(1,2)*z[28] + z[33];
    z[19]=z[12]*z[19];
    z[19]=z[19] + n<T>(1,3) + z[18];
    z[28]=7*z[3];
    z[30]= - z[32]*z[28];
    z[30]=static_cast<T>(19)+ z[30];
    z[33]=3*z[7];
    z[35]=z[32]*z[3];
    z[35]= - static_cast<T>(1)- z[35];
    z[35]=z[35]*z[33];
    z[30]=n<T>(1,6)*z[30] + z[35];
    z[30]=z[7]*z[30];
    z[35]=19*z[3];
    z[39]=n<T>(11,2) + z[35];
    z[30]=n<T>(1,3)*z[39] + z[30];
    z[39]=n<T>(1,3)*z[2];
    z[40]= - static_cast<T>(11)+ n<T>(7,4)*z[3];
    z[40]=z[3]*z[40];
    z[40]=n<T>(85,4) + z[40];
    z[40]=z[7]*z[40];
    z[40]=z[40] + static_cast<T>(11)- n<T>(7,2)*z[3];
    z[40]=z[7]*z[40];
    z[40]=n<T>(7,4) + z[40];
    z[40]=z[40]*z[39];
    z[30]=n<T>(1,2)*z[30] + z[40];
    z[30]=z[2]*z[30];
    z[40]=n<T>(29,2) - n<T>(7,3)*z[3];
    z[40]=z[40]*z[21];
    z[41]=npow(z[3],3);
    z[43]=z[41]*z[7];
    z[44]= - 7*z[14] + z[43];
    z[44]=z[7]*z[44];
    z[30]=z[30] + n<T>(3,4)*z[44] + n<T>(1,3) + z[40];
    z[40]=n<T>(1,2)*z[14];
    z[44]=static_cast<T>(5)+ z[28];
    z[44]=z[44]*z[40];
    z[43]= - z[38]*z[43];
    z[43]=z[44] + z[43];
    z[43]=z[7]*z[43];
    z[43]= - n<T>(7,4)*z[14] + z[43];
    z[44]=z[21] + 1;
    z[45]=z[44]*z[3];
    z[45]=z[45] + n<T>(1,2);
    z[46]=n<T>(1,2)*z[7];
    z[41]=z[45]*z[41]*z[46];
    z[42]=z[42] + 1;
    z[42]=z[42]*z[3];
    z[42]=z[42] + n<T>(1,4);
    z[47]=z[42]*z[14];
    z[41]=z[47] - z[41];
    z[41]=z[41]*z[7];
    z[47]=z[3] + n<T>(1,2);
    z[48]=z[47]*z[40];
    z[41]=z[41] - z[48];
    z[48]=z[3] + n<T>(1,4);
    z[48]=z[48]*z[3];
    z[48]=z[48] - n<T>(3,8);
    z[48]=z[48]*z[2];
    z[49]=n<T>(9,8)*z[14] - z[48];
    z[49]=z[2]*z[49];
    z[49]= - n<T>(9,2)*z[41] + z[49];
    z[49]=z[49]*z[27];
    z[50]=z[21]*z[2];
    z[51]= - n<T>(5,4) - z[3];
    z[51]=z[3]*z[51];
    z[51]=z[50] + n<T>(1,8) + z[51];
    z[51]=z[2]*z[51];
    z[43]=z[49] + n<T>(3,4)*z[43] + z[51];
    z[43]=z[4]*z[43];
    z[49]=z[25]*z[6];
    z[16]=z[49]*z[16];
    z[51]= - n<T>(37,6) + z[33];
    z[51]=z[7]*z[51];
    z[51]=z[51] - 21*z[17];
    z[51]=z[2]*z[51];
    z[51]=n<T>(41,6)*z[16] - n<T>(1,2) + z[51];
    z[51]=z[6]*z[51];
    z[30]=n<T>(1,8)*z[51] + n<T>(1,4)*z[30] + z[43];
    z[30]=z[10]*z[30];
    z[43]=n<T>(17,2)*z[3];
    z[51]= - static_cast<T>(7)- z[43];
    z[51]=z[3]*z[51];
    z[51]= - n<T>(1,2) + z[51];
    z[51]=z[51]*z[18];
    z[42]=z[42]*z[20];
    z[42]=z[51] + z[42];
    z[42]=z[7]*z[42];
    z[51]=n<T>(1,8)*z[3];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=z[28]*z[51];
    z[28]=z[28] + z[42];
    z[42]=3*z[3];
    z[52]=n<T>(11,8) + z[3];
    z[52]=z[52]*z[42];
    z[53]=z[2]*z[3];
    z[52]= - z[53] - n<T>(5,4) + z[52];
    z[52]=z[2]*z[52];
    z[28]=9*z[28] + z[52];
    z[41]=9*z[41];
    z[48]= - n<T>(9,4)*z[14] + z[48];
    z[48]=z[2]*z[48];
    z[48]=z[41] + z[48];
    z[48]=z[4]*z[48];
    z[28]=n<T>(1,2)*z[28] + z[48];
    z[28]=z[4]*z[28];
    z[48]=static_cast<T>(11)+ 65*z[3];
    z[48]=z[48]*z[21];
    z[35]= - static_cast<T>(7)- z[35];
    z[35]=z[35]*z[20];
    z[35]=z[48] + z[35];
    z[48]=n<T>(1,4)*z[7];
    z[35]=z[35]*z[48];
    z[52]= - n<T>(13,4) - z[42];
    z[52]=z[52]*z[29];
    z[54]=n<T>(3,4) + z[3];
    z[54]=z[3]*z[54];
    z[35]=z[52] + z[35] + n<T>(17,4) + z[54];
    z[28]=n<T>(1,2)*z[35] + z[28];
    z[28]=z[4]*z[28];
    z[35]=static_cast<T>(77)- n<T>(83,2)*z[7];
    z[35]=z[7]*z[35];
    z[35]=z[35] + n<T>(235,2)*z[17];
    z[35]=z[2]*z[35];
    z[35]=z[35] - 37*z[16];
    z[35]=z[6]*z[35];
    z[52]=n<T>(17,8) - z[7];
    z[52]=z[52]*z[33];
    z[54]= - static_cast<T>(89)+ n<T>(223,3)*z[7];
    z[54]=z[7]*z[54];
    z[54]=z[54] - n<T>(257,3)*z[17];
    z[54]=z[54]*z[23];
    z[35]=n<T>(1,6)*z[35] + z[54] + n<T>(13,8) + z[52];
    z[52]=n<T>(1,2)*z[6];
    z[35]=z[35]*z[52];
    z[54]=z[38]*z[18];
    z[34]= - z[34]*z[21];
    z[34]=n<T>(61,3) + z[34];
    z[34]=z[34]*z[18];
    z[34]= - n<T>(35,3) + z[34];
    z[34]=z[7]*z[34];
    z[34]=z[34] + n<T>(13,3) + z[54];
    z[34]=z[7]*z[34];
    z[54]=static_cast<T>(5)- z[3];
    z[34]=n<T>(1,8)*z[54] + z[34];
    z[54]=z[2]*z[7];
    z[55]=static_cast<T>(4)- n<T>(13,24)*z[3];
    z[55]=z[7]*z[55];
    z[55]=n<T>(13,24) + z[55];
    z[55]=z[55]*z[54];
    z[34]=n<T>(1,2)*z[34] + z[55];
    z[34]=z[2]*z[34];
    z[55]= - static_cast<T>(1)- z[51];
    z[55]=z[3]*z[55];
    z[55]=n<T>(1,4) + z[55];
    z[55]=z[3]*z[55];
    z[56]=n<T>(5,2)*z[3];
    z[57]= - static_cast<T>(3)+ z[56];
    z[57]=z[3]*z[57];
    z[57]=static_cast<T>(3)+ z[57];
    z[57]=z[7]*z[57];
    z[55]=z[57] - n<T>(27,8) + z[55];
    z[55]=z[7]*z[55];
    z[57]=n<T>(23,6) + z[3];
    z[57]=z[3]*z[57];
    z[57]= - static_cast<T>(25)+ z[57];
    z[55]=n<T>(1,4)*z[57] + z[55];
    z[28]=z[30] + z[35] + z[28] + n<T>(1,2)*z[55] + z[34];
    z[28]=z[10]*z[28];
    z[30]=n<T>(1,3)*z[3];
    z[34]=z[30]*z[22];
    z[34]=z[34] + n<T>(1,2);
    z[35]=z[48] - 1;
    z[35]=z[35]*z[7];
    z[34]=n<T>(1,3)*z[35] + n<T>(1,2)*z[34];
    z[55]=z[46] - 1;
    z[55]=z[55]*z[7];
    z[15]= - n<T>(1,2)*z[15] + z[55];
    z[15]=z[15]*z[39];
    z[15]=z[15] - z[34];
    z[39]=7*z[6];
    z[15]=z[15]*z[39];
    z[57]=z[7] - 4;
    z[57]=z[57]*z[7];
    z[58]=z[3] - 2;
    z[58]=z[58]*z[3];
    z[57]=z[57] + z[58] + 3;
    z[59]=2*z[4];
    z[57]=z[57]*z[59];
    z[59]=n<T>(1,3)*z[7];
    z[60]=n<T>(97,4) - 19*z[7];
    z[59]=z[60]*z[59];
    z[60]=z[30] - 7;
    z[59]=n<T>(1,4)*z[60] + z[59];
    z[59]=z[59]*z[29];
    z[61]=static_cast<T>(23)- n<T>(185,2)*z[3];
    z[61]=z[3]*z[61];
    z[61]=n<T>(149,2) + z[61];
    z[62]= - n<T>(5,3) + n<T>(13,16)*z[7];
    z[62]=z[7]*z[62];
    z[15]=z[15] + z[57] + z[59] + n<T>(1,24)*z[61] + 5*z[62];
    z[15]=z[6]*z[15];
    z[59]=n<T>(1,4)*z[54];
    z[61]= - static_cast<T>(7)+ n<T>(23,2)*z[7];
    z[61]=z[61]*z[59];
    z[62]=z[46] - 2;
    z[62]=z[62]*z[7];
    z[63]=z[22]*z[3];
    z[64]=z[63] + n<T>(3,2);
    z[62]=z[62] + z[64];
    z[65]=3*z[4];
    z[62]=z[62]*z[65];
    z[66]=z[7] - 3;
    z[66]=z[66]*z[7];
    z[67]= - n<T>(25,2)*z[66] - static_cast<T>(17)+ n<T>(9,2)*z[3];
    z[67]=n<T>(1,2)*z[67] + z[62];
    z[67]=z[4]*z[67];
    z[68]=static_cast<T>(131)- n<T>(199,2)*z[7];
    z[68]=z[7]*z[68];
    z[15]=z[15] + z[67] + z[61] + n<T>(1,12)*z[68] - static_cast<T>(3)+ n<T>(49,24)*z[3];
    z[15]=z[6]*z[15];
    z[61]= - static_cast<T>(17)- 13*z[3];
    z[61]=z[3]*z[61];
    z[61]= - static_cast<T>(3)+ z[61];
    z[61]=z[61]*z[21];
    z[67]=static_cast<T>(11)+ z[37];
    z[67]=z[67]*z[21];
    z[67]=static_cast<T>(3)+ z[67];
    z[67]=z[67]*z[20];
    z[61]=z[61] + z[67];
    z[61]=z[7]*z[61];
    z[67]=static_cast<T>(3)+ z[37];
    z[67]=z[67]*z[21];
    z[61]=z[67] + z[61];
    z[67]= - static_cast<T>(1)- z[42];
    z[67]=z[67]*z[42];
    z[67]=n<T>(17,2)*z[53] + static_cast<T>(5)+ z[67];
    z[67]=z[67]*z[29];
    z[61]=3*z[61] + z[67];
    z[67]= - static_cast<T>(1)+ n<T>(19,2)*z[3];
    z[67]=z[3]*z[67];
    z[67]=n<T>(3,2) + z[67];
    z[67]=z[2]*z[67];
    z[67]= - n<T>(9,2)*z[14] + z[67];
    z[67]=z[67]*z[29];
    z[41]=z[41] + z[67];
    z[41]=z[4]*z[41];
    z[41]=n<T>(1,2)*z[61] + z[41];
    z[27]=z[41]*z[27];
    z[41]=n<T>(3,2) + z[3];
    z[41]=z[41]*z[56];
    z[56]= - static_cast<T>(3)- z[3];
    z[56]=z[3]*z[56];
    z[56]= - n<T>(9,4) + z[56];
    z[56]=z[56]*z[36];
    z[41]=z[41] + z[56];
    z[41]=z[41]*z[46];
    z[56]= - n<T>(1,8) - z[3];
    z[56]=z[3]*z[56];
    z[61]=static_cast<T>(13)+ 37*z[3];
    z[61]=z[2]*z[61];
    z[27]=z[27] + n<T>(1,16)*z[61] + z[41] - n<T>(17,8) + z[56];
    z[27]=z[4]*z[27];
    z[41]=z[60]*z[23];
    z[56]= - static_cast<T>(5)- n<T>(143,12)*z[3];
    z[56]=z[3]*z[56];
    z[41]=z[41] + n<T>(19,3) + z[56];
    z[41]=z[2]*z[41];
    z[56]= - n<T>(1,3) - z[21];
    z[56]=z[3]*z[56];
    z[56]=n<T>(1,6) + z[56];
    z[49]=z[56]*z[49];
    z[41]=z[41] + 7*z[49];
    z[41]=z[41]*z[52];
    z[49]= - n<T>(3,4) + z[3];
    z[49]=z[3]*z[49];
    z[56]= - static_cast<T>(83)+ 95*z[3];
    z[56]=z[2]*z[56];
    z[41]=z[41] + n<T>(1,48)*z[56] - n<T>(57,8) + z[49];
    z[41]=z[6]*z[41];
    z[43]= - static_cast<T>(1)- z[43];
    z[43]=z[3]*z[43];
    z[43]=n<T>(3,2) + z[43];
    z[26]=z[43]*z[26];
    z[43]=n<T>(1,2) - z[58];
    z[43]=z[2]*z[43];
    z[26]=z[43] + n<T>(1,8)*z[26];
    z[26]=z[4]*z[26];
    z[43]= - static_cast<T>(41)- n<T>(101,4)*z[3];
    z[43]=z[43]*z[30];
    z[43]= - n<T>(21,4) + z[43];
    z[25]=z[43]*z[25];
    z[43]=z[6]*z[45]*npow(z[2],3);
    z[25]=n<T>(1,2)*z[25] - n<T>(7,3)*z[43];
    z[25]=z[25]*z[52];
    z[43]=n<T>(19,3) + z[3];
    z[43]=z[3]*z[43];
    z[43]=n<T>(43,8) + z[43];
    z[43]=z[2]*z[43];
    z[25]=z[43] + z[25];
    z[25]=z[6]*z[25];
    z[25]=z[25] - n<T>(25,6) + z[26];
    z[25]=z[11]*z[25];
    z[26]= - z[38]*z[36];
    z[26]=z[3] + z[26];
    z[26]=z[7]*z[26];
    z[26]=n<T>(97,6) + z[26];
    z[25]=z[25] + z[41] + n<T>(1,4)*z[26] + z[27];
    z[25]=z[11]*z[25];
    z[26]=z[7]*z[32];
    z[26]=z[26] - z[22];
    z[26]=z[7]*z[26];
    z[22]= - z[7]*z[22];
    z[22]=n<T>(1,2) + z[22];
    z[22]=z[22]*z[54];
    z[22]=z[22] + n<T>(1,2) + z[26];
    z[22]=z[2]*z[22];
    z[26]=z[36] - z[3];
    z[26]=z[26]*z[7];
    z[27]=static_cast<T>(1)- z[26];
    z[22]=n<T>(1,2)*z[27] + z[22];
    z[27]=z[55] - n<T>(3,4)*z[17];
    z[27]=z[2]*z[27];
    z[27]=z[27] + n<T>(1,4)*z[16];
    z[27]=z[6]*z[27];
    z[22]=n<T>(1,2)*z[22] + z[27];
    z[22]=z[13]*z[22];
    z[27]=z[18] - 3;
    z[32]= - z[27]*z[33];
    z[32]=z[32] - static_cast<T>(9)+ z[21];
    z[32]=z[7]*z[32];
    z[32]= - n<T>(1,2) + z[32];
    z[27]=z[7]*z[27];
    z[27]= - n<T>(1,4) + z[27];
    z[27]=z[27]*z[54];
    z[27]=n<T>(1,2)*z[32] + z[27];
    z[27]=z[2]*z[27];
    z[32]= - static_cast<T>(41)+ n<T>(33,2)*z[7];
    z[32]=z[7]*z[32];
    z[32]=z[32] - 31*z[17];
    z[32]=z[32]*z[29];
    z[16]=z[32] + 5*z[16];
    z[16]=z[16]*z[52];
    z[32]=static_cast<T>(121)- 69*z[7];
    z[32]=z[32]*z[46];
    z[17]=z[32] + 33*z[17];
    z[17]=z[17]*z[23];
    z[23]= - static_cast<T>(4)+ n<T>(3,2)*z[7];
    z[23]=z[7]*z[23];
    z[16]=z[16] + z[17] + n<T>(29,4) + z[23];
    z[16]=z[6]*z[16];
    z[17]= - z[33] + static_cast<T>(5)- z[18];
    z[17]=z[7]*z[17];
    z[17]= - n<T>(7,2) + z[17];
    z[23]=static_cast<T>(9)- z[26];
    z[23]=z[11]*z[23];
    z[16]=n<T>(3,2)*z[22] + n<T>(1,8)*z[23] + z[16] + n<T>(1,2)*z[17] + z[27];
    z[16]=z[13]*z[16];
    z[17]= - n<T>(37,2) - z[37];
    z[17]=z[17]*z[18];
    z[22]=n<T>(17,4) + z[37];
    z[22]=z[22]*z[21];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[7]*z[22];
    z[17]=z[22] + static_cast<T>(3)+ z[17];
    z[17]=z[7]*z[17];
    z[22]=z[38]*z[40];
    z[20]= - z[47]*z[20];
    z[20]=z[22] + z[20];
    z[20]=z[7]*z[20];
    z[14]=z[14]*z[29];
    z[14]=z[20] + z[14];
    z[20]=n<T>(9,2)*z[4];
    z[14]=z[14]*z[20];
    z[22]=static_cast<T>(3)+ n<T>(5,4)*z[3];
    z[22]=z[22]*z[21];
    z[14]=z[14] + n<T>(3,8)*z[53] + z[22] + z[17];
    z[14]=z[4]*z[14];
    z[17]=n<T>(17,4) - 2*z[3];
    z[17]=z[7]*z[17];
    z[17]=z[17] - n<T>(17,2) + z[3];
    z[17]=z[7]*z[17];
    z[14]=n<T>(3,2)*z[14] + n<T>(25,16)*z[3] + z[17];
    z[14]=z[4]*z[14];
    z[17]=static_cast<T>(13)+ z[21];
    z[17]=n<T>(1,3)*z[17] + n<T>(9,2)*z[66];
    z[22]= - z[34]*z[39];
    z[17]=z[22] + n<T>(1,4)*z[17] + z[57];
    z[17]=z[6]*z[17];
    z[22]=n<T>(5,2)*z[66];
    z[23]=z[62] - z[22] - static_cast<T>(1)- n<T>(3,2)*z[3];
    z[23]=z[4]*z[23];
    z[17]=z[17] + z[23] + n<T>(1,24) - z[55];
    z[17]=z[6]*z[17];
    z[23]=z[42] - z[36];
    z[23]=z[23]*z[46];
    z[23]= - z[50] + z[63] + z[23];
    z[20]=z[23]*z[20];
    z[20]=z[20] - n<T>(13,4)*z[3] - z[66];
    z[20]=z[20]*z[65];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[4]*z[20];
    z[23]=z[7] - 1;
    z[26]=z[23]*z[48];
    z[20]=z[26] + z[20];
    z[17]=n<T>(1,2)*z[20] + z[17];
    z[17]=z[1]*z[17];
    z[20]=z[35] + n<T>(1,2)*z[64];
    z[20]=z[20]*z[6];
    z[22]= - z[22] + z[31];
    z[22]=n<T>(1,4)*z[22] + z[20];
    z[22]=z[6]*z[22];
    z[26]= - n<T>(3,2)*z[66] - z[44];
    z[20]=n<T>(1,4)*z[26] + z[20];
    z[20]=z[6]*z[20];
    z[26]= - n<T>(3,2) - z[55];
    z[20]=n<T>(1,8)*z[26] + z[20];
    z[20]=z[1]*z[20];
    z[24]=z[24]*z[46];
    z[18]=z[24] - static_cast<T>(3)- z[18];
    z[18]=z[18]*z[48];
    z[18]=z[20] + z[18] + z[22];
    z[18]=z[5]*z[18];
    z[20]=n<T>(85,4) - z[3];
    z[20]=z[20]*z[48];
    z[20]=z[20] - n<T>(11,3) + z[51];
    z[20]=z[7]*z[20];
    z[22]= - n<T>(29,6) + z[3];
    z[22]=z[7]*z[22];
    z[21]=z[21] + z[22];
    z[21]=z[21]*z[59];
    z[22]= - n<T>(11,3) - z[3];
    z[24]= - n<T>(7,12)*z[2] - n<T>(21,4) + z[30];
    z[24]=z[10]*z[24];
    z[22]=n<T>(1,4)*z[22] + z[24];
    z[22]=z[10]*z[22];
    z[24]=z[9]*npow(z[10],2);
    z[22]=z[22] - n<T>(7,6)*z[24];
    z[22]=z[9]*z[22];
    z[23]= - z[23]*z[29];
    z[24]= - n<T>(1,2) - z[55];
    z[24]=z[1]*z[24];
    z[23]=z[23] + z[24];
    z[23]=z[8]*z[23];

    r += z[14] + z[15] + z[16] + z[17] + z[18] + n<T>(1,2)*z[19] + z[20] + 
      z[21] + n<T>(1,4)*z[22] + n<T>(1,3)*z[23] + z[25] + z[28];
 
    return r;
}

template double qg_2lha_r1237(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1237(const std::array<dd_real,30>&);
#endif
