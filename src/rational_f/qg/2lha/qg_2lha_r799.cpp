#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r799(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[4];
    z[6]=z[5] + 1;
    z[7]=n<T>(1,2)*z[1];
    z[8]= - z[7] + z[6];
    z[7]=z[7] - 1;
    z[9]=n<T>(1,2)*z[5];
    z[10]= - z[9] + z[7];
    z[11]= - z[2]*z[7];
    z[10]=3*z[10] + z[11];
    z[10]=z[2]*z[10];
    z[8]=3*z[8] + z[10];
    z[8]=z[2]*z[8];
    z[7]=z[7] - n<T>(3,2)*z[5];
    z[8]=z[8] + z[7];
    z[10]=z[5] + n<T>(3,2);
    z[11]=n<T>(1,2)*z[2];
    z[12]= - z[11] + z[10];
    z[12]=z[12]*z[11];
    z[13]=static_cast<T>(1)+ n<T>(1,4)*z[5];
    z[13]=z[13]*z[5];
    z[12]=z[12] - n<T>(3,4) - z[13];
    z[12]=z[2]*z[12];
    z[14]=z[9] + 1;
    z[15]=z[14]*z[5];
    z[15]=z[15] + n<T>(1,2);
    z[12]=n<T>(1,2)*z[15] + z[12];
    z[16]= - 3*z[6] + z[2];
    z[16]=z[16]*z[11];
    z[15]=3*z[15] + z[16];
    z[15]=z[2]*z[15];
    z[16]=z[5] + 3;
    z[17]= - z[5]*z[16];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[5]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[15]=n<T>(1,2)*z[17] + z[15];
    z[17]=n<T>(1,2)*z[4];
    z[15]=z[15]*z[17];
    z[12]=3*z[12] + z[15];
    z[12]=z[4]*z[12];
    z[8]=n<T>(1,2)*z[8] + z[12];
    z[8]=z[3]*z[8];
    z[9]= - z[16]*z[9];
    z[12]=z[14]*z[11];
    z[9]=z[12] - static_cast<T>(1)+ z[9];
    z[9]=z[2]*z[9];
    z[12]=n<T>(5,4) + z[13];
    z[12]=z[5]*z[12];
    z[9]=z[9] + n<T>(1,2) + z[12];
    z[9]=z[4]*z[9];
    z[12]= - static_cast<T>(2)- n<T>(3,4)*z[5];
    z[12]=z[5]*z[12];
    z[13]=n<T>(5,4)*z[2];
    z[14]= - z[13] + n<T>(5,2) + 2*z[5];
    z[14]=z[2]*z[14];
    z[9]=z[9] + z[14] - n<T>(5,4) + z[12];
    z[9]=z[4]*z[9];
    z[12]=z[2] - z[1] - z[5];
    z[12]=z[12]*z[11];
    z[12]=z[12] + z[1] - z[10];
    z[12]=z[2]*z[12];
    z[7]=z[12] - z[7];
    z[7]=z[8] + n<T>(1,2)*z[7] + z[9];
    z[7]=z[3]*z[7];
    z[8]=z[2] - z[6];
    z[8]=z[8]*z[11];
    z[9]= - n<T>(5,2) - z[5];
    z[9]=z[5]*z[9];
    z[10]=z[2]*z[10];
    z[9]=z[10] - n<T>(3,2) + z[9];
    z[9]=z[9]*z[17];
    z[9]=z[9] - z[13] + n<T>(5,4) + z[5];
    z[9]=z[4]*z[9];
    z[8]=z[8] + z[9];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[6]=z[4]*z[6];
    z[6]= - static_cast<T>(1)+ z[6];
    z[6]=z[4]*z[6];
    z[6]=n<T>(1,8)*z[6] + z[7];

    r += z[6]*z[3];
 
    return r;
}

template double qg_2lha_r799(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r799(const std::array<dd_real,30>&);
#endif
