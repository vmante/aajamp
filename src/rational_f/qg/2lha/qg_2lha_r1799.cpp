#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1799(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[9];
    z[6]=k[7];
    z[7]=k[11];
    z[8]=k[13];
    z[9]=z[5] - 1;
    z[10]=z[1] - 1;
    z[9]=z[9]*z[10];
    z[11]=3*z[9] - z[2];
    z[12]= - static_cast<T>(1)+ n<T>(3,2)*z[1];
    z[13]=z[7] + z[12];
    z[14]=z[2]*z[6];
    z[15]=static_cast<T>(1)+ n<T>(1,4)*z[14];
    z[15]=z[2]*z[15];
    z[13]=n<T>(1,2)*z[13] + z[15];
    z[13]=z[3]*z[2]*z[13];
    z[11]=n<T>(1,4)*z[11] + z[13];
    z[11]=z[3]*z[11];
    z[10]=3*z[10] + z[2];
    z[10]=z[5]*z[10];
    z[10]=n<T>(1,4)*z[10] + z[11];
    z[10]=z[4]*z[10];
    z[11]= - z[14] - n<T>(1,2) - z[7];
    z[11]=z[2]*z[11];
    z[9]= - n<T>(3,2)*z[9] + z[11];
    z[9]=z[3]*z[9];
    z[11]= - z[5]*z[12];
    z[9]=z[9] + n<T>(1,2) + z[11];
    z[9]=z[3]*z[9];
    z[9]= - n<T>(1,2)*z[5] + z[9];
    z[9]=n<T>(1,4)*z[9] + z[10];
    z[9]=z[4]*z[9];
    z[10]=z[2]*z[8]*npow(z[3],2);

    r += z[9] - n<T>(1,6)*z[10];
 
    return r;
}

template double qg_2lha_r1799(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1799(const std::array<dd_real,30>&);
#endif
