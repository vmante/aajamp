#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r435(const std::array<T,30>& k) {
  T z[48];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[5];
    z[4]=k[10];
    z[5]=k[17];
    z[6]=k[3];
    z[7]=k[6];
    z[8]=k[9];
    z[9]=k[12];
    z[10]=k[13];
    z[11]=k[7];
    z[12]=k[14];
    z[13]=n<T>(3,2)*z[6];
    z[14]=npow(z[4],2);
    z[15]=npow(z[6],2);
    z[16]=static_cast<T>(1)- z[15];
    z[16]=z[16]*z[14]*z[13];
    z[17]=z[6] - 1;
    z[18]=n<T>(1,2)*z[17];
    z[19]=static_cast<T>(1)- z[13];
    z[19]=z[6]*z[19];
    z[19]=n<T>(1,2) + z[19];
    z[19]=z[4]*z[19];
    z[19]= - z[18] + z[19];
    z[19]=z[4]*z[19];
    z[20]=n<T>(1,2)*z[2];
    z[21]=z[20]*z[14];
    z[22]= - z[17]*z[21];
    z[19]=z[19] + z[22];
    z[19]=z[2]*z[19];
    z[16]=z[16] + z[19];
    z[16]=z[2]*z[16];
    z[19]=n<T>(3,2)*z[17];
    z[22]=n<T>(1,2)*z[6];
    z[23]= - static_cast<T>(1)- z[22];
    z[23]=z[6]*z[23];
    z[23]=n<T>(3,2) + z[23];
    z[23]=z[4]*z[23];
    z[23]=z[19] + z[23];
    z[23]=z[4]*z[15]*z[23];
    z[16]=z[23] + z[16];
    z[16]=z[2]*z[16];
    z[23]=npow(z[6],3);
    z[24]=z[23]*z[17];
    z[25]=z[17]*z[2];
    z[23]= - z[23]*z[25];
    z[23]= - z[24] + z[23];
    z[26]=n<T>(1,2)*z[8];
    z[23]=z[23]*z[26];
    z[27]=z[15]*z[25];
    z[23]=z[23] - z[24] - n<T>(3,2)*z[27];
    z[23]=z[8]*z[23];
    z[27]=n<T>(1,2)*z[4];
    z[28]=static_cast<T>(1)- z[27];
    z[24]=z[4]*z[24]*z[28];
    z[16]=z[23] + z[24] + z[16];
    z[16]=z[9]*z[16];
    z[23]=z[27]*z[15];
    z[24]=static_cast<T>(3)- z[6];
    z[24]=z[24]*z[22];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[23];
    z[28]=z[22]*z[7];
    z[29]=z[7] + n<T>(5,4);
    z[28]=z[28] - z[29];
    z[28]=z[28]*z[6];
    z[30]=z[7] + n<T>(5,2);
    z[28]=z[28] + n<T>(1,2)*z[30];
    z[28]=z[28]*z[15];
    z[24]=z[28] + z[24];
    z[24]=z[4]*z[24];
    z[14]=z[14]*z[2];
    z[31]=z[14]*z[17];
    z[32]= - static_cast<T>(7)+ 9*z[6];
    z[32]=z[32]*z[22];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[4]*z[32];
    z[19]=z[19] + z[32];
    z[19]=z[4]*z[19];
    z[19]=z[19] + n<T>(5,2)*z[31];
    z[19]=z[19]*z[20];
    z[32]=z[4]*z[6];
    z[33]=static_cast<T>(1)+ 3*z[6];
    z[33]=z[6]*z[33];
    z[33]= - static_cast<T>(1)+ n<T>(1,4)*z[33];
    z[33]=z[33]*z[32];
    z[17]=z[17]*z[6];
    z[33]= - z[17] + z[33];
    z[33]=z[4]*z[33];
    z[19]=z[33] + z[19];
    z[19]=z[2]*z[19];
    z[33]=static_cast<T>(3)- z[22];
    z[33]=z[6]*z[33];
    z[33]=z[25] - n<T>(5,2) + z[33];
    z[33]=z[26]*z[15]*z[33];
    z[34]=z[6]*z[25];
    z[28]=z[33] - z[28] + z[34];
    z[28]=z[8]*z[28];
    z[16]=z[16] + z[28] + z[24] + z[19];
    z[16]=z[9]*z[16];
    z[19]=npow(z[7],2);
    z[24]=z[22]*z[19];
    z[28]=z[7] - n<T>(11,8);
    z[28]=z[28]*z[7];
    z[24]=z[28] - z[24];
    z[28]=n<T>(1,3)*z[6];
    z[24]=z[24]*z[28];
    z[33]=z[7] + n<T>(3,2);
    z[24]=z[24] + n<T>(1,2)*z[33];
    z[24]=z[24]*z[6];
    z[33]=n<T>(1,3)*z[7];
    z[34]=z[33] - n<T>(3,8);
    z[34]=z[34]*z[7];
    z[24]=z[24] - z[34] - 1;
    z[24]=z[24]*z[6];
    z[34]=z[7] - n<T>(5,2);
    z[35]=z[34]*z[33];
    z[36]=z[35] + n<T>(1,2);
    z[24]=z[24] + n<T>(1,2)*z[36];
    z[36]= - z[6]*z[7];
    z[29]=z[36] + z[29];
    z[29]=z[29]*z[28];
    z[29]= - n<T>(3,4) + z[29];
    z[29]=z[6]*z[29];
    z[29]=z[33] + z[29];
    z[29]=z[6]*z[29];
    z[36]=z[7] - 1;
    z[29]= - n<T>(1,3)*z[36] + z[29];
    z[29]=z[29]*z[27];
    z[29]=z[29] + z[24];
    z[29]=z[4]*z[29];
    z[17]=z[4]*z[17];
    z[17]= - z[18] - 3*z[17];
    z[17]=z[4]*z[17];
    z[17]=z[17] - n<T>(9,2)*z[31];
    z[18]=n<T>(1,4)*z[2];
    z[17]=z[17]*z[18];
    z[31]=z[7] + n<T>(1,2);
    z[37]= - z[6]*z[33];
    z[37]=z[37] + z[31];
    z[37]=z[6]*z[37];
    z[30]=z[37] - z[30];
    z[30]=z[30]*z[22];
    z[25]=n<T>(1,4)*z[25];
    z[30]= - z[25] + z[30] + static_cast<T>(1)+ n<T>(1,6)*z[7];
    z[30]=z[8]*z[6]*z[30];
    z[24]=z[30] - z[25] - z[24];
    z[24]=z[8]*z[24];
    z[16]=z[16] + z[24] + z[29] + z[17];
    z[16]=z[9]*z[16];
    z[17]=n<T>(2,3)*z[7];
    z[24]=z[17] - n<T>(1,4);
    z[24]=z[24]*z[19];
    z[25]=npow(z[7],3);
    z[29]=z[25]*z[6];
    z[24]=z[24] - n<T>(1,6)*z[29];
    z[24]=z[24]*z[6];
    z[30]=n<T>(1,2)*z[7];
    z[34]=z[7]*z[34];
    z[34]= - n<T>(7,8) + z[34];
    z[34]=z[34]*z[30];
    z[34]=z[34] - z[24];
    z[34]=z[6]*z[34];
    z[37]= - static_cast<T>(2)+ z[30];
    z[37]=z[37]*z[33];
    z[37]= - n<T>(3,4) + z[37];
    z[37]=z[7]*z[37];
    z[29]= - n<T>(13,4)*z[19] - z[29];
    z[38]=n<T>(1,6)*z[6];
    z[29]=z[29]*z[38];
    z[29]=z[37] + z[29];
    z[29]=z[6]*z[29];
    z[37]=n<T>(1,4)*z[7];
    z[38]=n<T>(9,4)*z[7];
    z[39]= - n<T>(31,3) + z[38];
    z[39]=z[39]*z[37];
    z[29]=z[39] + z[29];
    z[29]=z[6]*z[29];
    z[39]=3*z[7];
    z[40]= - n<T>(47,3) - z[39];
    z[40]=z[40]*z[37];
    z[40]= - static_cast<T>(1)+ z[40];
    z[29]=n<T>(1,4)*z[40] + z[29];
    z[29]=z[4]*z[29];
    z[40]= - n<T>(29,4) + z[7];
    z[40]=z[40]*z[33];
    z[40]= - n<T>(3,2) + z[40];
    z[29]=z[29] + n<T>(1,2)*z[40] + z[34];
    z[29]=z[4]*z[29];
    z[34]=z[25]*z[28];
    z[40]= - n<T>(15,8) + z[33];
    z[40]=z[40]*z[19];
    z[40]=z[40] - z[34];
    z[40]=z[6]*z[40];
    z[41]=n<T>(1,8)*z[7];
    z[42]= - static_cast<T>(39)+ n<T>(43,3)*z[7];
    z[42]=z[42]*z[41];
    z[40]=z[42] + z[40];
    z[40]=z[6]*z[40];
    z[42]=n<T>(9,2)*z[7];
    z[43]=n<T>(19,3) - z[42];
    z[43]=z[7]*z[43];
    z[43]= - static_cast<T>(13)+ z[43];
    z[40]=n<T>(1,4)*z[43] + z[40];
    z[40]=z[4]*z[40];
    z[13]=z[13]*z[19];
    z[43]=z[42] - 7;
    z[43]=z[43]*z[7];
    z[13]=z[43] - z[13];
    z[40]= - n<T>(1,4)*z[13] + z[40];
    z[40]=z[4]*z[40];
    z[43]=9*z[7];
    z[44]=z[43] - 11;
    z[44]=z[44]*z[7];
    z[45]=3*z[19];
    z[46]=z[6]*z[45];
    z[46]= - z[44] + z[46];
    z[46]=z[46]*z[27];
    z[46]= - z[45] + z[46];
    z[46]=z[4]*z[46];
    z[47]=z[19]*z[14];
    z[46]=z[46] - n<T>(3,2)*z[47];
    z[18]=z[46]*z[18];
    z[18]=z[40] + z[18];
    z[18]=z[18]*z[20];
    z[40]= - n<T>(3,2)*z[3] - z[5];
    z[46]=z[14] + z[4];
    z[40]=z[1]*z[46]*z[40];
    z[18]=n<T>(1,4)*z[40] + z[29] + z[18];
    z[18]=z[2]*z[18];
    z[29]=2*z[7];
    z[40]= - z[28] - n<T>(101,48) + z[29];
    z[40]=z[4]*z[40];
    z[40]=z[40] - n<T>(11,6) + z[29];
    z[40]=z[4]*z[40];
    z[46]= - n<T>(1,3) + z[7];
    z[46]=z[46]*z[14];
    z[40]=z[40] + z[46];
    z[40]=z[2]*z[40];
    z[46]=static_cast<T>(1)+ z[4];
    z[32]=z[46]*z[32];
    z[21]=z[32] + z[21];
    z[21]=z[2]*z[21];
    z[23]=z[15] + z[23];
    z[23]=z[4]*z[23];
    z[15]=z[21] + n<T>(1,2)*z[15] + z[23];
    z[15]=z[10]*z[15];
    z[21]= - n<T>(53,48)*z[6] + z[36];
    z[21]=z[4]*z[21];
    z[21]=z[21] + 2*z[36] - n<T>(77,48)*z[6];
    z[21]=z[4]*z[21];
    z[15]=n<T>(1,3)*z[15] + z[40] + z[21] - z[22] + z[36];
    z[15]=z[10]*z[15];
    z[21]=z[7] - n<T>(19,24);
    z[21]=z[21]*z[19];
    z[21]=z[21] - z[34];
    z[21]=z[21]*z[6];
    z[23]=z[29] - n<T>(37,8);
    z[23]=z[23]*z[33];
    z[23]=z[23] + n<T>(3,4);
    z[23]=z[23]*z[7];
    z[21]=z[23] - z[21];
    z[21]=z[21]*z[6];
    z[23]=5*z[7];
    z[29]= - static_cast<T>(23)- z[23];
    z[29]=z[7]*z[29];
    z[29]=n<T>(1,48)*z[29] + z[21];
    z[29]=z[6]*z[29];
    z[32]=z[7] - n<T>(1,2);
    z[28]=z[19]*z[32]*z[28];
    z[40]= - n<T>(3,4) + z[33];
    z[40]=z[7]*z[40];
    z[40]=n<T>(5,12) + z[40];
    z[40]=z[7]*z[40];
    z[40]=z[40] - z[28];
    z[40]=z[6]*z[40];
    z[46]= - static_cast<T>(3)- n<T>(5,2)*z[7];
    z[37]=z[46]*z[37];
    z[37]=z[37] + z[40];
    z[37]=z[6]*z[37];
    z[40]= - n<T>(59,3) + z[39];
    z[40]=z[40]*z[41];
    z[37]=z[40] + z[37];
    z[37]=z[6]*z[37];
    z[37]= - n<T>(7,6)*z[7] + z[37];
    z[37]=z[37]*z[27];
    z[40]= - static_cast<T>(7)+ n<T>(1,12)*z[7];
    z[40]=z[7]*z[40];
    z[40]=n<T>(1,2) + z[40];
    z[29]=z[37] + n<T>(1,4)*z[40] + z[29];
    z[29]=z[4]*z[29];
    z[37]=n<T>(5,3)*z[7];
    z[40]=z[37] - n<T>(3,2);
    z[40]=z[40]*z[7];
    z[41]=n<T>(1,2) + z[40];
    z[41]=z[7]*z[41];
    z[28]=z[41] - z[28];
    z[28]=z[28]*z[22];
    z[41]=n<T>(13,2) - z[23];
    z[41]=z[41]*z[33];
    z[41]= - n<T>(5,4) + z[41];
    z[41]=z[7]*z[41];
    z[28]=z[41] + z[28];
    z[28]=z[6]*z[28];
    z[23]= - n<T>(17,2) + z[23];
    z[23]=z[23]*z[33];
    z[23]=n<T>(9,4) + z[23];
    z[23]=z[7]*z[23];
    z[23]=z[23] + z[28];
    z[23]=z[6]*z[23];
    z[28]= - n<T>(1,2) + z[37];
    z[28]=z[28]*z[19];
    z[28]=z[28] - z[34];
    z[22]=z[28]*z[22];
    z[28]=static_cast<T>(1)- z[37];
    z[28]=z[7]*z[28];
    z[28]= - n<T>(1,4) + z[28];
    z[28]=z[7]*z[28];
    z[22]=z[28] + z[22];
    z[22]=z[6]*z[22];
    z[28]=n<T>(3,4) + z[40];
    z[28]=z[7]*z[28];
    z[22]=z[28] + z[22];
    z[22]=z[6]*z[22];
    z[28]=static_cast<T>(1)- n<T>(5,6)*z[7];
    z[28]=z[7]*z[28];
    z[28]= - n<T>(3,4) + z[28];
    z[28]=z[7]*z[28];
    z[22]=z[28] + z[22];
    z[22]=z[2]*z[22];
    z[28]=n<T>(7,2) - z[37];
    z[28]=z[7]*z[28];
    z[28]= - n<T>(7,2) + z[28];
    z[28]=z[7]*z[28];
    z[28]=n<T>(1,2) + z[28];
    z[22]=z[22] + n<T>(1,2)*z[28] + z[23];
    z[22]=z[8]*z[22];
    z[23]=n<T>(1,8) - z[17];
    z[23]=z[7]*z[23];
    z[23]=n<T>(1,2) + z[23];
    z[23]=z[7]*z[23];
    z[21]=z[23] - z[21];
    z[21]=z[6]*z[21];
    z[23]=n<T>(3,4) - z[7];
    z[23]=z[7]*z[23];
    z[23]= - n<T>(1,4) + z[23];
    z[23]=z[7]*z[23];
    z[23]=z[23] + z[24];
    z[23]=z[6]*z[23];
    z[17]= - n<T>(3,4) + z[17];
    z[17]=z[7]*z[17];
    z[17]=n<T>(1,2) + z[17];
    z[17]=z[7]*z[17];
    z[17]=z[17] + z[23];
    z[17]=z[2]*z[17];
    z[23]= - n<T>(41,24) + z[7];
    z[23]=z[7]*z[23];
    z[23]=n<T>(5,4) + z[23];
    z[23]=z[7]*z[23];
    z[17]=z[22] + z[17] + z[21] - n<T>(1,2) + z[23];
    z[17]=z[8]*z[17];
    z[21]=z[4]*z[44];
    z[19]=9*z[19] + z[21];
    z[19]=z[4]*z[19];
    z[21]=z[14]*z[45];
    z[19]=z[19] + z[21];
    z[19]=z[19]*z[20];
    z[21]= - n<T>(25,2) + z[43];
    z[21]=z[7]*z[21];
    z[22]= - static_cast<T>(11)+ z[42];
    z[22]=z[7]*z[22];
    z[22]=static_cast<T>(7)+ z[22];
    z[22]=z[4]*z[22];
    z[21]=z[21] + z[22];
    z[21]=z[4]*z[21];
    z[19]=z[19] + z[45] + z[21];
    z[19]=z[19]*z[20];
    z[21]= - static_cast<T>(11)+ z[39];
    z[21]=z[21]*z[30];
    z[21]=static_cast<T>(7)+ z[21];
    z[21]=z[21]*z[27];
    z[22]= - static_cast<T>(7)+ z[38];
    z[22]=z[7]*z[22];
    z[21]=z[21] + n<T>(11,2) + z[22];
    z[21]=z[4]*z[21];
    z[13]=z[19] + n<T>(1,2)*z[13] + z[21];
    z[13]=z[13]*z[20];
    z[19]=n<T>(3,8)*z[6] - n<T>(3,4);
    z[19]=z[7]*z[19];
    z[21]=static_cast<T>(1)- n<T>(3,8)*z[7];
    z[21]=z[4]*z[21];
    z[13]=z[13] + z[21] + static_cast<T>(1)+ z[19];
    z[13]=z[3]*z[13]*z[20];
    z[19]= - static_cast<T>(1)- z[7];
    z[19]=z[4]*z[19];
    z[19]=z[19] - z[31];
    z[19]=z[4]*z[19];
    z[21]= - static_cast<T>(1)- z[30];
    z[14]=z[21]*z[14];
    z[14]=z[19] + z[14];
    z[14]=z[2]*z[14];
    z[19]= - z[4]*z[30];
    z[19]=z[19] - z[32];
    z[19]=z[4]*z[19];
    z[14]=z[14] - n<T>(1,2)*z[36] + z[19];
    z[14]=z[5]*z[14];
    z[14]=z[14] - static_cast<T>(1)- n<T>(1,3)*z[25];
    z[19]=z[33] - n<T>(1,2);
    z[19]=z[19]*z[7];
    z[19]=z[19] + n<T>(1,2);
    z[19]=z[19]*z[7];
    z[20]=z[20]*z[19];
    z[21]=z[35] + 1;
    z[21]=z[21]*z[7];
    z[21]=z[21] - n<T>(1,2);
    z[20]=z[20] + z[21];
    z[19]=z[2]*z[19];
    z[19]=z[19] + z[21];
    z[22]=z[19]*z[26];
    z[23]=z[22] - z[20];
    z[23]=z[8]*z[23];
    z[22]=z[12]*z[22];
    z[22]=z[23] + z[22];
    z[22]=z[12]*z[22];
    z[19]=z[19]*z[12];
    z[19]=n<T>(1,2)*z[19] - z[20];
    z[19]=z[12]*z[19];
    z[19]=n<T>(1,2)*z[21] + z[19];
    z[19]=z[11]*z[19];

    r += z[13] + n<T>(1,2)*z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + 
      z[22] + z[29];
 
    return r;
}

template double qg_2lha_r435(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r435(const std::array<dd_real,30>&);
#endif
