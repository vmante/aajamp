#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r479(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[2];
    z[5]=k[10];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=k[7];
    z[11]=k[11];
    z[12]=z[8]*z[5];
    z[13]= - z[3] + z[5];
    z[13]=z[13]*z[12];
    z[14]=npow(z[5],2);
    z[15]=z[14]*z[2];
    z[16]=3*z[3];
    z[17]=z[16] - z[5];
    z[17]=z[5]*z[17];
    z[13]=z[13] + z[17] + z[15];
    z[13]=z[8]*z[13];
    z[17]=z[3] - n<T>(1,2);
    z[18]=n<T>(1,2)*z[5];
    z[19]= - 3*z[17] + z[18];
    z[19]=z[5]*z[19];
    z[20]=z[15] + z[5];
    z[21]=n<T>(1,2)*z[2];
    z[22]=z[20]*z[21];
    z[13]=z[13] + z[19] + z[22];
    z[13]=z[8]*z[13];
    z[19]=z[8]*z[3];
    z[19]= - z[16] + z[19];
    z[19]=z[8]*z[19];
    z[19]=z[16] + z[19];
    z[19]=z[8]*z[19];
    z[19]= - z[3] + z[19];
    z[19]=z[7]*z[19];
    z[22]=z[18]*z[2];
    z[23]=z[5]*z[17];
    z[13]=z[19] + z[13] + z[22] + n<T>(1,2) + z[23];
    z[13]=z[9]*z[13];
    z[19]=z[3] - 1;
    z[23]=n<T>(1,2)*z[3];
    z[24]=z[19]*z[23];
    z[25]=z[5]*z[3];
    z[26]=n<T>(1,2)*z[25];
    z[27]=z[26]*z[19];
    z[19]=z[19]*z[3];
    z[28]=z[19] + z[27];
    z[28]=z[5]*z[28];
    z[29]=npow(z[3],2);
    z[30]=z[29]*z[5];
    z[31]=z[29] + z[30];
    z[22]=z[31]*z[22];
    z[22]=z[22] + z[24] + z[28];
    z[22]=z[11]*z[22];
    z[27]=z[29] + z[27];
    z[27]=z[5]*z[27];
    z[28]=z[14]*z[29];
    z[31]=z[21]*z[28];
    z[27]=z[27] + z[31];
    z[27]=z[2]*z[27];
    z[31]=z[23] - 1;
    z[32]=z[31]*z[25];
    z[33]=n<T>(1,2)*z[29];
    z[27]=z[27] + z[33] + z[32];
    z[27]=z[2]*z[27];
    z[27]= - z[23] + z[27];
    z[27]=z[10]*z[27]*npow(z[2],2);
    z[13]=z[22] + z[13] + z[27];
    z[22]=z[3] + n<T>(1,2);
    z[27]=z[22]*z[29];
    z[32]=n<T>(1,2)*z[27];
    z[34]=n<T>(7,8) + z[3];
    z[34]=z[34]*z[30];
    z[34]= - z[32] + z[34];
    z[34]=z[5]*z[34];
    z[35]=npow(z[3],3);
    z[15]=z[15]*z[35];
    z[36]=n<T>(7,8) - z[3];
    z[28]=z[36]*z[28];
    z[28]=z[28] - z[15];
    z[28]=z[2]*z[28];
    z[28]=z[34] + z[28];
    z[28]=z[2]*z[28];
    z[34]= - n<T>(1,8) + z[3];
    z[34]=z[3]*z[34];
    z[34]=n<T>(1,4) + z[34];
    z[34]=z[34]*z[25];
    z[32]= - z[32] + z[34];
    z[32]=z[5]*z[32];
    z[34]=z[5] - 1;
    z[34]=z[35]*z[34];
    z[36]= - z[5]*z[34];
    z[36]=z[36] - z[15];
    z[36]=z[2]*z[36];
    z[37]=z[14]*z[35];
    z[36]=z[37] + z[36];
    z[36]=z[8]*z[36];
    z[28]=n<T>(1,4)*z[36] + z[32] + z[28];
    z[28]=z[8]*z[28];
    z[32]=3*z[29];
    z[36]=n<T>(5,4) - z[3];
    z[36]=z[36]*z[32];
    z[38]=n<T>(11,2) + z[16];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(9,4) + z[38];
    z[38]=z[38]*z[25];
    z[36]=z[36] + z[38];
    z[36]=z[5]*z[36];
    z[38]=n<T>(7,4) - z[3];
    z[38]=z[38]*z[30];
    z[38]= - z[35] + z[38];
    z[38]=z[5]*z[38];
    z[38]=z[38] - z[15];
    z[38]=z[2]*z[38];
    z[36]=z[36] + 3*z[38];
    z[36]=z[36]*z[21];
    z[38]=z[3] - n<T>(1,4);
    z[16]=z[38]*z[16];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[16]*z[26];
    z[39]= - n<T>(3,8) + z[3];
    z[39]=z[3]*z[39];
    z[16]=z[39] + z[16];
    z[16]=z[5]*z[16];
    z[16]=z[16] + z[36];
    z[16]=z[2]*z[16];
    z[36]= - n<T>(1,4) - z[3];
    z[36]=z[36]*z[25];
    z[36]=n<T>(13,4)*z[29] + z[36];
    z[36]=z[36]*z[18];
    z[16]=z[28] + z[36] + z[16];
    z[28]=n<T>(1,2)*z[8];
    z[16]=z[16]*z[28];
    z[36]=static_cast<T>(3)- z[3];
    z[36]=z[36]*z[29];
    z[39]=static_cast<T>(3)+ z[3];
    z[39]=z[3]*z[39];
    z[39]= - n<T>(9,4) + z[39];
    z[39]=z[39]*z[25];
    z[36]=n<T>(3,2)*z[36] + z[39];
    z[36]=z[5]*z[36];
    z[36]= - n<T>(3,4)*z[35] + z[36];
    z[39]=z[18]*z[29];
    z[40]=n<T>(21,8) - z[3];
    z[40]=z[40]*z[39];
    z[40]= - z[35] + z[40];
    z[40]=z[5]*z[40];
    z[37]= - z[21]*z[37];
    z[37]=z[40] + z[37];
    z[37]=z[2]*z[37];
    z[36]=n<T>(1,2)*z[36] + z[37];
    z[36]=z[2]*z[36];
    z[37]=n<T>(5,2) - z[3];
    z[33]=z[37]*z[33];
    z[37]=static_cast<T>(3)+ z[23];
    z[37]=z[3]*z[37];
    z[37]= - static_cast<T>(3)+ z[37];
    z[37]=z[3]*z[37];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(11,8) + z[38];
    z[38]=z[3]*z[38];
    z[38]=n<T>(5,8) + z[38];
    z[38]=z[5]*z[38];
    z[37]=z[37] + z[38];
    z[37]=z[5]*z[37];
    z[33]=z[33] + z[37];
    z[33]=n<T>(1,2)*z[33] + z[36];
    z[33]=z[2]*z[33];
    z[36]= - static_cast<T>(1)+ n<T>(3,4)*z[3];
    z[37]=z[36]*z[23];
    z[38]=n<T>(1,4) - z[29];
    z[38]=z[38]*z[18];
    z[38]=z[38] + n<T>(1,2) + z[19];
    z[38]=z[5]*z[38];
    z[37]=z[37] + z[38];
    z[33]=n<T>(1,2)*z[37] + z[33];
    z[33]=z[2]*z[33];
    z[37]=z[3] + 1;
    z[37]=z[37]*z[3];
    z[14]=z[14]*z[37];
    z[14]=z[16] + n<T>(1,16)*z[14] + z[33];
    z[14]=z[8]*z[14];
    z[16]=static_cast<T>(1)+ z[2];
    z[16]=z[27]*z[16];
    z[27]=z[35]*z[8];
    z[33]= - z[21]*z[27];
    z[16]=z[33] + z[16];
    z[16]=z[8]*z[16];
    z[22]=z[22]*z[3];
    z[33]= - z[2]*z[22];
    z[33]= - 5*z[29] + z[33];
    z[16]=n<T>(1,2)*z[33] + z[16];
    z[16]=z[8]*z[16];
    z[24]= - z[2]*z[24];
    z[16]=z[16] + z[3] + z[24];
    z[16]=z[8]*z[16];
    z[16]=n<T>(1,2)*z[17] + z[16];
    z[16]=z[7]*z[16];
    z[17]=z[32] - z[39];
    z[17]=z[5]*z[17];
    z[12]=z[34]*z[12];
    z[12]=z[17] + z[12];
    z[12]=z[8]*z[12];
    z[12]= - n<T>(3,2)*z[25] + z[12];
    z[12]=z[8]*z[12];
    z[12]=n<T>(1,2) + z[12];
    z[12]=z[8]*z[12];
    z[12]= - n<T>(1,2) + z[12];
    z[17]= - z[32] + z[27];
    z[17]=z[8]*z[17];
    z[17]=n<T>(7,2)*z[3] + z[17];
    z[17]=z[17]*z[28];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[8]*z[17];
    z[17]=n<T>(1,4) + z[17];
    z[17]=z[7]*z[17];
    z[12]=n<T>(1,2)*z[12] + z[17];
    z[12]=z[6]*z[12];
    z[17]=z[35]*z[2];
    z[17]=n<T>(5,2)*z[29] - z[17];
    z[17]=z[17]*z[21];
    z[17]= - z[3] + z[17];
    z[17]=z[2]*z[17];
    z[17]=n<T>(1,4) + z[17];
    z[17]=z[1]*z[17];
    z[12]=z[17] + z[16] + z[12];
    z[16]=n<T>(19,4) - z[3];
    z[16]=z[16]*z[29];
    z[17]=static_cast<T>(5)+ z[3];
    z[17]=z[3]*z[17];
    z[17]= - n<T>(9,2) + z[17];
    z[17]=z[17]*z[26];
    z[16]=z[16] + z[17];
    z[16]=z[5]*z[16];
    z[17]=n<T>(7,2) - z[3];
    z[17]=z[17]*z[30];
    z[17]= - 3*z[35] + z[17];
    z[17]=z[5]*z[17];
    z[15]=z[17] - z[15];
    z[15]=z[15]*z[21];
    z[15]=z[15] - z[35] + z[16];
    z[15]=z[2]*z[15];
    z[16]=z[23] - 3;
    z[17]=z[16]*z[29];
    z[15]=z[17] - z[15];
    z[17]= - static_cast<T>(1)+ n<T>(1,4)*z[22];
    z[17]=z[3]*z[17];
    z[17]=n<T>(1,2) + z[17];
    z[17]=z[17]*z[18];
    z[21]=static_cast<T>(1)+ n<T>(1,8)*z[3];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(21,16) + z[21];
    z[21]=z[3]*z[21];
    z[17]=z[21] + z[17];
    z[17]=z[5]*z[17];
    z[15]=z[17] - n<T>(1,4)*z[15];
    z[15]=z[2]*z[15];
    z[16]=z[16]*z[23];
    z[17]=static_cast<T>(1)- z[19];
    z[17]=z[5]*z[17];
    z[16]=n<T>(1,8)*z[17] + static_cast<T>(1)+ z[16];
    z[16]=z[5]*z[16];
    z[17]=z[3]*z[31];
    z[16]=n<T>(3,2)*z[17] + z[16];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[2]*z[15];
    z[16]=z[18] - z[36];
    z[15]=n<T>(1,4)*z[16] + z[15];
    z[15]=z[2]*z[15];
    z[16]= - z[37] - z[30];
    z[16]=z[5]*z[16];
    z[17]=z[2]*z[20];
    z[18]=static_cast<T>(1)- z[7];
    z[18]=z[6]*z[18];
    z[17]=z[17] + z[18];
    z[17]=z[4]*z[17];
    z[16]=z[16] + z[17];

    r += n<T>(1,4)*z[12] + n<T>(1,8)*z[13] + z[14] + z[15] + n<T>(1,16)*z[16];
 
    return r;
}

template double qg_2lha_r479(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r479(const std::array<dd_real,30>&);
#endif
