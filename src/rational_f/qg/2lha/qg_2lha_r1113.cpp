#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1113(const std::array<T,30>& k) {
  T z[31];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[12];
    z[7]=k[6];
    z[8]=k[10];
    z[9]=k[27];
    z[10]=n<T>(1,2)*z[5];
    z[11]=static_cast<T>(1)- z[5];
    z[11]=z[11]*z[10];
    z[12]=z[9]*z[5];
    z[11]=z[11] - z[12];
    z[13]=npow(z[9],2);
    z[11]=z[11]*z[13];
    z[14]=z[4] - 1;
    z[15]=n<T>(1,2)*z[4];
    z[16]=z[14]*npow(z[7],2)*z[15];
    z[11]=z[11] + z[16];
    z[11]=z[3]*z[11];
    z[16]=static_cast<T>(1)- z[10];
    z[16]=z[5]*z[16];
    z[17]= - z[9]*z[10];
    z[16]=z[16] + z[17];
    z[17]=n<T>(1,2)*z[9];
    z[16]=z[16]*z[17];
    z[18]=z[6] - 1;
    z[19]=n<T>(1,4)*z[4];
    z[20]=z[19] - 1;
    z[21]=z[4]*z[20];
    z[21]=z[21] - z[18];
    z[22]=n<T>(1,3)*z[7];
    z[21]=z[21]*z[22];
    z[23]=n<T>(1,3)*z[6];
    z[24]=z[5]*z[23];
    z[11]=n<T>(1,6)*z[11] + z[21] + z[24] + z[16];
    z[16]=n<T>(1,2)*z[8];
    z[11]=z[11]*z[16];
    z[21]=z[19] + 1;
    z[21]=z[21]*z[4];
    z[24]=z[21] + z[18];
    z[24]=z[7]*z[24];
    z[19]=n<T>(1,3) - z[19];
    z[19]=n<T>(1,6)*z[24] + n<T>(1,2)*z[19] - z[23];
    z[19]=z[7]*z[19];
    z[24]=z[10] - n<T>(1,3);
    z[25]= - z[5]*z[24];
    z[25]=z[25] - z[12];
    z[26]=n<T>(1,2)*z[13];
    z[25]=z[25]*z[26];
    z[27]= - z[15] - z[18];
    z[27]=z[7]*z[27];
    z[28]=npow(z[6],2);
    z[27]=z[28] + z[27];
    z[22]=z[27]*z[22];
    z[27]=z[5]*z[28];
    z[22]=z[22] - n<T>(1,3)*z[27] + z[25];
    z[25]=n<T>(1,2)*z[3];
    z[22]=z[22]*z[25];
    z[27]= - n<T>(1,8) + z[5];
    z[27]=z[27]*z[23];
    z[29]=n<T>(1,4)*z[5];
    z[30]=n<T>(1,3) - z[29];
    z[30]=z[5]*z[30];
    z[30]=z[30] - n<T>(7,24)*z[12];
    z[30]=z[9]*z[30];
    z[11]=z[11] + z[22] + z[19] + z[27] + z[30];
    z[11]=z[8]*z[11];
    z[19]=z[24]*z[29];
    z[22]= - z[5]*z[20];
    z[22]=n<T>(1,2) + z[22];
    z[27]=z[15] - 1;
    z[30]=z[6]*z[27];
    z[22]=n<T>(1,2)*z[22] + z[30];
    z[22]=z[22]*z[23];
    z[18]=z[6]*z[18];
    z[30]=n<T>(1,2) - z[23];
    z[30]=z[6]*z[30];
    z[30]= - n<T>(1,6) + z[30];
    z[30]=z[7]*z[30];
    z[18]=z[30] + n<T>(1,6) + z[18];
    z[30]=n<T>(1,2)*z[7];
    z[18]=z[30]*z[18];
    z[18]=z[18] + z[19] + z[22];
    z[18]=z[6]*z[18];
    z[19]=npow(z[5],2);
    z[22]=n<T>(1,2)*z[19] + z[12];
    z[22]=z[26]*z[27]*z[22];
    z[26]=static_cast<T>(1)+ z[4];
    z[19]=z[26]*z[19]*npow(z[6],3)*z[25];
    z[25]= - static_cast<T>(7)- z[4];
    z[25]=z[25]*z[10];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[29];
    z[26]= - n<T>(1,2) + z[4];
    z[26]=z[6]*z[5]*z[26];
    z[25]=z[25] + z[26];
    z[25]=z[25]*z[28];
    z[19]=z[19] + z[25] + z[22];
    z[19]=z[3]*z[19];
    z[22]=z[27]*z[13];
    z[22]= - static_cast<T>(1)+ z[22];
    z[25]=n<T>(1,3)*z[4];
    z[26]=n<T>(1,2) - z[25];
    z[15]=z[26]*z[15];
    z[26]=static_cast<T>(5)- z[6];
    z[26]=z[6]*z[26];
    z[15]=n<T>(1,12)*z[26] - n<T>(1,3) + z[15];
    z[15]=z[7]*z[15];
    z[15]=z[15] - n<T>(1,4) + z[25];
    z[15]=z[7]*z[15];
    z[15]=n<T>(1,6)*z[22] + z[15];
    z[15]=z[3]*z[15];
    z[22]=static_cast<T>(1)- z[9];
    z[22]=z[8]*z[22];
    z[22]=n<T>(1,6)*z[22] + n<T>(1,3) - z[17];
    z[22]=z[22]*z[3]*z[9];
    z[25]=z[17] - 1;
    z[26]=n<T>(1,2)*z[6];
    z[28]= - z[26] - z[25];
    z[22]=n<T>(1,3)*z[28] + z[22];
    z[16]=z[22]*z[16];
    z[14]=z[9]*z[14];
    z[22]= - npow(z[4],2)*z[30];
    z[14]=z[22] + n<T>(1,4)*z[14] - n<T>(1,2)*z[1] + z[4];
    z[14]=z[16] + n<T>(1,3)*z[14] + z[15];
    z[14]=z[2]*z[14];
    z[15]= - n<T>(7,3) + z[4];
    z[16]=static_cast<T>(3)- n<T>(7,6)*z[6];
    z[16]=z[6]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[16]= - n<T>(5,3) + z[26];
    z[16]=z[16]*z[26];
    z[21]=n<T>(7,4) - z[21];
    z[16]=n<T>(1,3)*z[21] + z[16];
    z[16]=z[7]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[7]*z[15];
    z[16]= - z[9]*z[20];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[9]*z[16];
    z[16]= - n<T>(1,2) + z[16];
    z[14]=z[14] + n<T>(1,3)*z[16] + z[15];
    z[13]=z[13]*z[3];
    z[15]= - n<T>(1,3) + z[9];
    z[15]=z[15]*z[13];
    z[16]= - n<T>(1,2) + z[9];
    z[13]=z[16]*z[13];
    z[16]=z[9]*z[25];
    z[13]=n<T>(1,3)*z[13] + n<T>(1,2) + z[16];
    z[13]=z[8]*z[13];
    z[13]=z[15] + z[13];
    z[15]=n<T>(1,2) + z[6];
    z[16]= - static_cast<T>(1)+ n<T>(7,8)*z[9];
    z[16]=z[9]*z[16];
    z[15]=n<T>(1,4)*z[15] + z[16];
    z[13]=n<T>(1,3)*z[15] + n<T>(1,4)*z[13];
    z[13]=z[8]*z[13];
    z[15]= - n<T>(7,2) + z[6];
    z[15]=z[15]*z[23];
    z[15]=n<T>(3,2) + z[15];
    z[15]=z[15]*z[26];
    z[16]=n<T>(1,3)*z[20];
    z[15]=z[16] + z[15];
    z[15]=z[7]*z[15];
    z[15]= - n<T>(1,12) + z[15];
    z[15]=z[7]*z[15];
    z[20]=z[27]*npow(z[9],3);
    z[15]= - n<T>(1,6)*z[20] + z[15];
    z[15]=z[3]*z[15];
    z[13]=z[13] + z[15] + n<T>(1,2)*z[14];
    z[13]=z[2]*z[13];
    z[10]= - z[24]*z[10];
    z[12]=z[12]*z[16];
    z[10]=z[10] + z[12];
    z[10]=z[10]*z[17];

    r += z[10] + z[11] + z[13] + z[18] + n<T>(1,3)*z[19];
 
    return r;
}

template double qg_2lha_r1113(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1113(const std::array<dd_real,30>&);
#endif
