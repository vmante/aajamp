#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2292(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[4];
    z[4]=k[3];
    z[5]=k[9];
    z[6]=k[13];
    z[7]=z[6] - 1;
    z[8]=z[1] + 1;
    z[8]= - z[1]*z[8];
    z[8]=z[8] + z[7];
    z[9]=3*z[6];
    z[10]=z[4]*z[5];
    z[11]=static_cast<T>(1)+ z[10];
    z[11]=z[4]*z[11];
    z[11]=z[11] - z[9] + static_cast<T>(1)- z[1];
    z[11]=z[4]*z[11];
    z[12]= - static_cast<T>(3)+ z[6];
    z[12]=z[6]*z[12];
    z[12]=z[4] + z[12] + static_cast<T>(2)+ z[1];
    z[12]=z[3]*z[12];
    z[8]=z[12] + 2*z[8] + z[11];
    z[8]=z[3]*z[8];
    z[11]=npow(z[1],3);
    z[8]=z[11] + z[8];
    z[8]=z[2]*z[8];
    z[11]=z[6] - z[5];
    z[12]=z[11]*npow(z[4],2);
    z[8]=z[12] - z[8];
    z[12]=2*z[6];
    z[13]=static_cast<T>(1)+ z[12];
    z[12]=z[13]*z[12];
    z[10]= - 3*z[10] - static_cast<T>(1)+ z[12];
    z[10]=z[4]*z[10];
    z[12]=z[1] - z[7];
    z[13]=static_cast<T>(2)- z[6];
    z[13]=z[6]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[3]*z[13];
    z[10]=2*z[13] + 5*z[12] + z[10];
    z[10]=z[3]*z[10];
    z[12]=npow(z[1],2);
    z[8]=z[10] - 3*z[12] - 2*z[8];
    z[8]=z[2]*z[8];
    z[10]=2*z[4];
    z[12]=npow(z[6],2);
    z[10]=z[10]*z[12];
    z[11]=3*z[11] + z[10];
    z[11]=z[4]*z[11];
    z[12]=z[5] - 4*z[12];
    z[12]=z[4]*z[12];
    z[7]=z[12] + z[7];
    z[7]=z[3]*z[7];
    z[7]=z[8] + z[7] + z[1] + z[11];
    z[7]=z[2]*z[7];
    z[8]= - z[9] - z[10];
    z[8]=z[4]*z[8];

    r +=  - static_cast<T>(1)+ z[5] + z[7] + z[8];
 
    return r;
}

template double qg_2lha_r2292(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2292(const std::array<dd_real,30>&);
#endif
