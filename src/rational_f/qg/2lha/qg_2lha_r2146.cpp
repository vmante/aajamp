#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2146(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[6];
    z[5]=k[3];
    z[6]=k[9];
    z[7]=z[1] + 1;
    z[8]=z[2] + 1;
    z[9]= - z[8]*z[7];
    z[10]=z[2]*z[6];
    z[11]= - z[6] - z[10];
    z[11]=z[5]*z[11];
    z[8]=z[11] - z[8];
    z[8]=z[5]*z[8];
    z[8]=z[8] + z[9];
    z[8]=z[5]*z[8];
    z[9]=z[1] + 2;
    z[9]=z[9]*z[1];
    z[9]=z[9] + 1;
    z[11]=z[5]*z[7];
    z[11]=z[11] - z[9];
    z[12]= - 2*z[7] + z[5];
    z[12]=z[5]*z[12];
    z[12]=z[12] + z[9];
    z[12]=z[4]*z[12];
    z[11]=3*z[11] + z[12];
    z[11]=z[4]*z[11];
    z[12]=z[2] - 2;
    z[9]= - z[12]*z[9];
    z[8]=z[11] + z[8] + z[9];
    z[8]=z[3]*z[8];
    z[9]=3*z[5];
    z[11]=2*z[1];
    z[12]= - static_cast<T>(5)- z[11];
    z[12]=z[12]*z[9];
    z[11]=z[11] + 3;
    z[13]=2*z[11] - z[9];
    z[13]=z[5]*z[13];
    z[14]= - static_cast<T>(4)- z[1];
    z[14]=z[1]*z[14];
    z[13]=z[13] - static_cast<T>(3)+ z[14];
    z[14]=2*z[4];
    z[13]=z[13]*z[14];
    z[15]=static_cast<T>(19)+ 4*z[1];
    z[15]=z[1]*z[15];
    z[12]=z[13] + z[12] + static_cast<T>(15)+ z[15];
    z[12]=z[4]*z[12];
    z[13]=3*z[2];
    z[15]=z[13] - 7;
    z[7]=z[15]*z[7];
    z[15]=z[5]*z[6];
    z[15]=z[15] + 1;
    z[13]=z[13] + 5;
    z[13]=z[5]*z[13]*z[15];
    z[7]=2*z[8] + z[12] + z[13] + z[7];
    z[7]=z[3]*z[7];
    z[8]= - static_cast<T>(3)- z[1];
    z[8]=2*z[8] + z[9];
    z[8]=z[5]*z[8];
    z[8]=z[8] + z[11];
    z[8]=z[8]*z[14];
    z[8]=z[8] + 12*z[5] - static_cast<T>(12)- 7*z[1];
    z[8]=z[4]*z[8];
    z[9]= - 4*z[6] - z[10];
    z[9]=z[5]*z[9];
    z[7]=z[7] + z[8] + z[9] + static_cast<T>(3)- z[2];
    z[7]=z[3]*z[7];
    z[8]=static_cast<T>(2)- z[5];
    z[8]=z[5]*z[8];
    z[8]= - static_cast<T>(1)+ z[8];
    z[8]=z[8]*z[14];
    z[9]=static_cast<T>(1)- z[5];
    z[8]=3*z[9] + z[8];
    z[8]=z[4]*z[8];

    r +=  - static_cast<T>(1)+ z[6] + z[7] + z[8];
 
    return r;
}

template double qg_2lha_r2146(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2146(const std::array<dd_real,30>&);
#endif
