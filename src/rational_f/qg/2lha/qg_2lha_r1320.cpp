#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1320(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[9];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=n<T>(1,2)*z[5];
    z[8]=z[7]*z[3];
    z[9]= - static_cast<T>(3)- z[5];
    z[9]=z[9]*z[8];
    z[10]= - z[6] - z[5];
    z[11]= - z[8] + static_cast<T>(1)+ z[5];
    z[11]=z[3]*z[11];
    z[10]=n<T>(1,2)*z[10] + z[11];
    z[10]=z[4]*z[10];
    z[11]=npow(z[5],2);
    z[9]=z[10] + z[9] + n<T>(3,2) + z[11];
    z[9]=z[3]*z[9];
    z[10]=z[5] - 3;
    z[12]= - z[5]*z[10];
    z[12]=z[12] - static_cast<T>(3)- z[6];
    z[9]=n<T>(1,2)*z[12] + z[9];
    z[12]=n<T>(1,2)*z[4];
    z[9]=z[9]*z[12];
    z[13]=z[5] - 1;
    z[8]=z[13]*z[8];
    z[14]=z[7] - 1;
    z[15]=z[14]*z[5];
    z[8]=z[9] + z[8] - n<T>(1,4) - z[15];
    z[7]=z[10]*z[7];
    z[9]= - static_cast<T>(1)- z[15];
    z[9]=z[3]*z[9];
    z[7]=z[9] + z[7] + static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[9]=n<T>(1,2)*z[3];
    z[10]= - n<T>(3,2) + z[5];
    z[10]=z[10]*z[9];
    z[10]=z[10] + n<T>(1,2)*z[6] - z[14];
    z[10]=z[3]*z[10];
    z[14]= - z[3] + static_cast<T>(1)+ z[6];
    z[14]=z[4]*z[14]*npow(z[3],2);
    z[10]=z[10] + n<T>(1,4)*z[14];
    z[10]=z[4]*z[10];
    z[7]=n<T>(1,2)*z[7] + z[10];
    z[7]=z[2]*z[7];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[2]*z[7];
    z[8]=z[11]*z[9];
    z[10]= - z[5]*z[13];
    z[10]=z[10] + z[8];
    z[10]=z[3]*z[10];
    z[10]=z[15] + z[10];
    z[11]=static_cast<T>(1)- n<T>(3,2)*z[5];
    z[11]=z[5]*z[11];
    z[8]=z[11] + z[8];
    z[8]=z[8]*z[9];
    z[9]= - static_cast<T>(1)+ n<T>(3,4)*z[5];
    z[9]=z[5]*z[9];
    z[8]=z[9] + z[8];
    z[8]=z[3]*z[8];
    z[9]= - n<T>(1,2) - z[15];
    z[8]=n<T>(1,2)*z[9] + z[8];
    z[8]=z[4]*z[8];
    z[8]=n<T>(1,2)*z[10] + z[8];
    z[8]=z[8]*z[12];
    z[7]=z[8] + z[7];

    r += z[7]*z[2];
 
    return r;
}

template double qg_2lha_r1320(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1320(const std::array<dd_real,30>&);
#endif
