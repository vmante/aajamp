#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1068(const std::array<T,30>& k) {
  T z[31];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[27];
    z[7]=k[3];
    z[8]=k[4];
    z[9]=k[9];
    z[10]=k[10];
    z[11]=z[10] + 7;
    z[12]=n<T>(1,3)*z[10];
    z[13]= - z[11]*z[12];
    z[14]= - static_cast<T>(1)+ z[6];
    z[14]=z[1]*z[14];
    z[15]=z[1] + n<T>(5,2);
    z[16]= - z[3]*z[15];
    z[13]=z[13] + z[16] + z[14] - n<T>(13,3) + n<T>(5,2)*z[6];
    z[14]=n<T>(1,2)*z[6];
    z[16]=n<T>(1,2)*z[3];
    z[17]=z[16] - z[14] + 1;
    z[18]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[17]=z[18]*z[17];
    z[19]=z[7] - 1;
    z[20]=n<T>(1,4)*z[9];
    z[21]=z[19]*z[20];
    z[22]=n<T>(7,2)*z[10];
    z[23]= - static_cast<T>(5)- z[22];
    z[23]=z[7]*z[23];
    z[17]=z[21] + n<T>(1,6)*z[23] - n<T>(1,12)*z[10] + z[17];
    z[17]=z[2]*z[17];
    z[21]=n<T>(1,2)*z[10];
    z[23]=z[21] - 1;
    z[24]=z[23]*z[12];
    z[25]= - n<T>(1,2) - z[24];
    z[25]=z[7]*z[25];
    z[26]= - static_cast<T>(3)- z[7];
    z[26]=z[7]*z[26];
    z[27]=n<T>(3,2) - z[7];
    z[27]=z[7]*z[27];
    z[27]= - n<T>(1,2) + z[27];
    z[27]=z[9]*z[27];
    z[26]=z[27] + static_cast<T>(3)+ z[26];
    z[27]=n<T>(1,2)*z[9];
    z[26]=z[26]*z[27];
    z[13]=z[17] + z[26] + n<T>(1,2)*z[13] + z[25];
    z[13]=z[2]*z[13];
    z[17]=n<T>(1,2)*z[7];
    z[18]=z[17] - z[18];
    z[18]=z[2]*z[18];
    z[18]=z[15] + z[18];
    z[25]=z[19]*z[27];
    z[26]=z[7]*z[10];
    z[18]=z[25] + n<T>(7,6)*z[26] + z[21] + n<T>(5,3)*z[18];
    z[18]=z[2]*z[18];
    z[25]=z[8] - 1;
    z[22]= - n<T>(5,2)*z[7] + 5*z[25] - z[22];
    z[26]=n<T>(1,3)*z[7];
    z[22]=z[22]*z[26];
    z[28]=npow(z[7],2);
    z[29]=z[9]*z[8]*z[28];
    z[22]=n<T>(5,6)*z[29] - z[21] + z[22];
    z[22]=z[9]*z[22];
    z[29]=z[1] - z[8];
    z[26]= - z[26] - static_cast<T>(1)- n<T>(1,3)*z[29];
    z[18]=z[18] + n<T>(5,2)*z[26] + z[22];
    z[18]=z[5]*z[18];
    z[11]=z[11]*z[21];
    z[22]=z[10]*z[23];
    z[22]=n<T>(5,2) + z[22];
    z[22]=z[7]*z[22];
    z[11]=z[22] - z[8] + z[11];
    z[22]= - n<T>(5,2)*z[10] - static_cast<T>(7)+ z[8];
    z[22]=n<T>(1,3)*z[22] + z[7];
    z[22]=z[7]*z[22];
    z[22]= - z[21] + z[22];
    z[22]=z[22]*z[27];
    z[11]=n<T>(1,3)*z[11] + z[22];
    z[11]=z[9]*z[11];
    z[22]=z[8] - 3;
    z[23]=n<T>(1,4)*z[6];
    z[23]=z[22]*z[23];
    z[26]=n<T>(1,4)*z[3];
    z[22]=z[1] - z[22];
    z[22]=z[22]*z[26];
    z[29]=n<T>(1,4)*z[1];
    z[30]= - z[6]*z[29];
    z[11]=z[18] + z[13] + z[11] + z[22] + z[30] + n<T>(1,3) + z[23];
    z[11]=z[5]*z[11];
    z[13]=z[10] - 1;
    z[13]=z[13]*z[10];
    z[18]=z[6]*z[8];
    z[18]=z[13] - static_cast<T>(3)+ z[18];
    z[12]=n<T>(1,2) + z[12];
    z[12]=z[12]*z[21];
    z[12]= - n<T>(1,4)*z[7] + n<T>(1,3) + z[12];
    z[12]=z[9]*z[12];
    z[12]=n<T>(1,4)*z[18] + z[12];
    z[12]=z[9]*z[12];
    z[11]=z[11] - z[16] + z[12];
    z[12]= - z[15]*z[26];
    z[15]=n<T>(1,4) + z[24];
    z[15]=z[15]*z[17];
    z[18]=z[6] + z[28];
    z[17]=z[17] - 1;
    z[17]=z[17]*z[7];
    z[22]=n<T>(1,2) + z[17];
    z[22]=z[9]*z[22];
    z[18]=n<T>(1,2)*z[18] + z[22];
    z[18]=z[18]*z[20];
    z[12]=z[18] + z[15] + n<T>(7,12)*z[10] + z[12] + n<T>(1,3) + n<T>(3,8)*z[6];
    z[12]=z[2]*z[12];
    z[15]= - static_cast<T>(1)+ z[28];
    z[15]=z[15]*z[20];
    z[14]= - static_cast<T>(1)- z[14];
    z[14]=z[15] + n<T>(1,2)*z[14] + z[7];
    z[14]=z[14]*z[27];
    z[15]=n<T>(3,4) - z[6];
    z[18]=static_cast<T>(1)+ z[29];
    z[18]=z[3]*z[18];
    z[12]=z[12] + z[14] - n<T>(1,8)*z[13] + n<T>(1,2)*z[15] + z[18];
    z[12]=z[2]*z[12];
    z[11]=z[12] + n<T>(1,2)*z[11];
    z[11]=z[5]*z[11];
    z[12]=z[1] + 1;
    z[13]=3*z[4];
    z[12]=z[12]*z[13];
    z[12]=z[12] + 1;
    z[12]=z[12]*z[16];
    z[13]= - z[13] + z[10];
    z[13]=z[13]*z[21];
    z[14]=z[4] + n<T>(1,2);
    z[15]= - z[9]*z[4]*z[19];
    z[13]=z[15] + z[13] - z[12] - z[14];
    z[15]=n<T>(1,3) - n<T>(3,2)*z[1];
    z[15]=z[4]*z[15];
    z[16]=n<T>(1,3)*z[4];
    z[18]= - n<T>(3,2) + z[16];
    z[18]=z[10]*z[18];
    z[12]=z[18] + z[12] - z[6] + z[15];
    z[15]=static_cast<T>(1)- n<T>(7,3)*z[4];
    z[15]=z[10]*z[15];
    z[15]= - z[16] + n<T>(1,4)*z[15];
    z[15]=z[10]*z[15];
    z[14]= - n<T>(1,2)*z[14] + z[15];
    z[14]=z[7]*z[14];
    z[15]=z[6]*z[25];
    z[15]= - z[4] - static_cast<T>(1)+ z[15];
    z[16]= - z[4]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[9]*z[15];
    z[12]=z[15] + n<T>(1,2)*z[12] + z[14];
    z[12]=z[2]*z[12];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[2]*z[12];

    r += z[11] + n<T>(1,4)*z[12];
 
    return r;
}

template double qg_2lha_r1068(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1068(const std::array<dd_real,30>&);
#endif
