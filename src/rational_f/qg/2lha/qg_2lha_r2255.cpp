#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2255(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[24];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=k[27];
    z[8]=k[13];
    z[9]=k[14];
    z[10]= - z[5] + z[4];
    z[11]=n<T>(1,4)*z[7];
    z[10]=z[11]*z[10];
    z[12]= - z[7] + z[6];
    z[13]=z[5]*z[4];
    z[12]=z[2]*z[13]*z[12];
    z[13]=z[6]*z[13];
    z[13]=z[13] - z[4];
    z[13]= - n<T>(7,2)*z[5] - static_cast<T>(5)- n<T>(3,2)*z[13];
    z[13]=z[6]*z[13];
    z[13]=z[13] - z[7];
    z[14]=static_cast<T>(2)+ z[1];
    z[14]=z[3]*z[14];
    z[15]=z[3] + z[9];
    z[15]=z[15]*z[8];
    z[16]= - 3*z[3] + z[15];
    z[16]=z[8]*z[16];
    z[10]=n<T>(1,4)*z[12] + z[16] + z[14] + static_cast<T>(1)+ z[10] + n<T>(1,2)*z[13];
    z[10]=z[2]*z[10];
    z[12]=z[3] + 2*z[9];
    z[13]= - z[15] + z[12];
    z[13]=z[8]*z[13];
    z[14]=static_cast<T>(1)+ z[5];
    z[14]=z[3]*z[14];
    z[13]= - n<T>(3,4)*z[6] + z[14] + z[13];
    z[13]=z[6]*z[13];
    z[12]= - z[8]*z[12];
    z[12]=2*z[3] + z[12];
    z[12]=z[8]*z[12];
    z[10]=z[10] + z[13] + z[12] + z[11] - z[3];
    z[10]=z[2]*z[10];
    z[11]=z[9] - z[3];
    z[11]=z[11]*npow(z[8],2);
    z[12]=static_cast<T>(2)+ z[5];
    z[12]=z[3]*z[12];
    z[12]=z[9] + z[12];
    z[12]=z[8]*z[12];
    z[12]= - z[9] + z[12];
    z[12]=z[6]*z[8]*z[12];

    r += z[10] + z[11] + z[12];
 
    return r;
}

template double qg_2lha_r2255(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2255(const std::array<dd_real,30>&);
#endif
