#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r782(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[8];
    z[7]=k[10];
    z[8]=z[1]*z[3];
    z[9]=z[8] - z[3];
    z[10]=z[4]*z[3];
    z[11]= - 2*z[9] + z[10];
    z[11]=z[4]*z[11];
    z[12]=z[1] - 1;
    z[13]=2*z[12] - z[4];
    z[13]=z[4]*z[13];
    z[14]=z[5]*npow(z[4],2);
    z[13]=z[13] + z[14];
    z[13]=z[5]*z[13];
    z[8]= - 2*z[3] + z[8];
    z[8]=z[1]*z[8];
    z[8]=z[13] + z[11] + z[3] + z[8];
    z[8]=z[2]*z[8];
    z[8]=z[8] - z[10] + z[9];
    z[9]=2*z[4];
    z[10]= - z[9] + static_cast<T>(1)+ 2*z[1];
    z[10]=z[5]*z[4]*z[10];
    z[9]=z[10] + z[9] + z[12];
    z[9]=z[5]*z[9];
    z[8]=z[9] + 2*z[8];
    z[8]=z[2]*z[8];
    z[9]= - z[4] + static_cast<T>(1)+ z[1];
    z[9]=z[9]*npow(z[5],2);
    z[8]=z[8] + z[3] + z[9];
    z[8]=z[2]*z[8];
    z[9]=z[4] + 1;
    z[9]=z[7]*z[9];
    z[10]=z[6] - z[7];
    z[10]=z[1]*z[10];
    z[9]=z[10] - static_cast<T>(1)+ z[9];
    z[9]=z[5]*z[9];
    z[9]= - z[7] + z[9];
    z[9]=z[5]*z[9];

    r += z[8] + n<T>(1,2)*z[9];
 
    return r;
}

template double qg_2lha_r782(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r782(const std::array<dd_real,30>&);
#endif
