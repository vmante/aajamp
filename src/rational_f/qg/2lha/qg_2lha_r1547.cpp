#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1547(const std::array<T,30>& k) {
  T z[35];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[14];
    z[6]=k[9];
    z[7]=k[4];
    z[8]=k[11];
    z[9]=k[12];
    z[10]=k[18];
    z[11]=9*z[4];
    z[12]=npow(z[9],2);
    z[11]=z[11]*z[12];
    z[13]=9*z[9];
    z[14]=static_cast<T>(7)+ z[13];
    z[14]=z[9]*z[14];
    z[14]= - z[11] - 3*z[8] + z[14];
    z[14]=z[4]*z[14];
    z[15]=n<T>(3,2)*z[9];
    z[16]=n<T>(9,4)*z[8];
    z[14]=n<T>(3,4)*z[14] - z[15] - static_cast<T>(5)+ z[16];
    z[14]=z[4]*z[14];
    z[17]=npow(z[8],2);
    z[17]=z[17] + z[12];
    z[18]=n<T>(9,4)*z[4];
    z[19]=z[17]*z[18];
    z[20]=z[16] - 2;
    z[20]=z[20]*z[8];
    z[21]= - static_cast<T>(2)+ n<T>(27,4)*z[9];
    z[21]=z[9]*z[21];
    z[21]= - z[19] + z[20] + z[21];
    z[21]=z[4]*z[21];
    z[22]=13*z[8] + 3*z[9];
    z[21]=n<T>(1,4)*z[22] + z[21];
    z[21]=z[4]*z[21];
    z[22]=z[9] + z[8];
    z[23]=z[19] + z[22];
    z[24]=z[7]*z[4];
    z[23]=z[23]*z[24];
    z[21]=z[21] + z[23];
    z[21]=z[7]*z[21];
    z[23]=n<T>(1,2)*z[5];
    z[25]=z[23] - 1;
    z[26]=z[25]*z[23];
    z[27]=2*z[3];
    z[14]=z[21] + z[14] + z[26] + n<T>(15,2) - z[27];
    z[14]=z[7]*z[14];
    z[21]=z[5] - 3;
    z[21]=z[21]*z[23];
    z[26]=3*z[3];
    z[28]= - static_cast<T>(5)+ z[26];
    z[28]=2*z[28] - z[21];
    z[28]=z[2]*z[28];
    z[29]=n<T>(9,2)*z[4];
    z[30]=z[29]*z[12];
    z[15]=static_cast<T>(11)+ z[15];
    z[15]=z[9]*z[15];
    z[15]=z[15] - z[30];
    z[15]=z[4]*z[15];
    z[31]= - static_cast<T>(7)- n<T>(1,2)*z[9];
    z[15]=5*z[31] + 3*z[15];
    z[31]=n<T>(1,2)*z[4];
    z[15]=z[15]*z[31];
    z[14]=z[14] + z[15] + z[28] + z[21] + n<T>(85,4) - 6*z[3];
    z[14]=z[7]*z[14];
    z[15]=z[11] + 9*z[8] - z[9];
    z[21]=n<T>(9,8)*z[4];
    z[28]=z[17]*z[21];
    z[22]=z[28] + z[22];
    z[22]=z[7]*z[22];
    z[15]=n<T>(1,2)*z[15] + z[22];
    z[15]=z[7]*z[15]*npow(z[4],2);
    z[22]=5*z[9];
    z[30]= - z[22] + z[30];
    z[30]=z[4]*z[30];
    z[30]=static_cast<T>(5)+ z[30];
    z[30]=z[4]*z[30];
    z[32]=n<T>(1,4)*z[5];
    z[33]=z[32] + z[3];
    z[15]=z[15] + n<T>(3,2)*z[30] - static_cast<T>(5)+ z[33];
    z[15]=z[7]*z[15];
    z[27]=z[27] + z[23];
    z[30]=static_cast<T>(5)- z[27];
    z[30]=z[2]*z[30];
    z[34]= - 19*z[9] + z[11];
    z[34]=z[34]*z[31];
    z[34]=static_cast<T>(10)+ z[34];
    z[34]=z[4]*z[34];
    z[15]=z[15] + z[34] + z[30] - n<T>(35,4) + z[27];
    z[15]=z[7]*z[15];
    z[30]=z[2]*z[33];
    z[27]=z[30] + n<T>(15,4) - z[27];
    z[27]=z[2]*z[27];
    z[30]=z[18]*z[12];
    z[34]= - 7*z[9] + z[30];
    z[34]=z[4]*z[34];
    z[34]=n<T>(39,4) + z[34];
    z[34]=z[34]*z[31];
    z[15]=z[15] + z[34] + z[27] - n<T>(15,4) + z[33];
    z[15]=z[6]*z[15];
    z[27]=z[32] - 1;
    z[27]=z[27]*z[5];
    z[32]=4*z[3];
    z[27]=z[27] - z[32];
    z[33]=z[2]*z[27];
    z[34]=static_cast<T>(2)- z[23];
    z[34]=z[5]*z[34];
    z[33]=z[33] + z[34] - n<T>(25,2) + 8*z[3];
    z[33]=z[2]*z[33];
    z[34]=37*z[9] - z[11];
    z[34]=z[34]*z[31];
    z[34]= - static_cast<T>(29)+ z[34];
    z[34]=z[34]*z[31];
    z[14]=z[15] + z[14] + z[34] + z[33] + n<T>(25,2) + z[27];
    z[14]=z[6]*z[14];
    z[15]=z[16] - 1;
    z[15]=z[15]*z[8];
    z[13]=static_cast<T>(1)- z[13];
    z[13]=z[9]*z[13];
    z[13]=z[28] - z[15] + z[13];
    z[13]=z[4]*z[13];
    z[16]= - static_cast<T>(13)+ n<T>(9,2)*z[8];
    z[16]=z[8]*z[16];
    z[27]=n<T>(9,4)*z[9];
    z[28]=static_cast<T>(1)+ z[27];
    z[28]=z[9]*z[28];
    z[16]=z[28] + static_cast<T>(1)+ n<T>(1,2)*z[16];
    z[13]=n<T>(1,2)*z[16] + z[13];
    z[13]=z[4]*z[13];
    z[16]= - z[17]*z[29];
    z[28]= - static_cast<T>(2)+ z[27];
    z[28]=z[9]*z[28];
    z[16]=z[16] + z[20] + z[28];
    z[16]=z[4]*z[16];
    z[20]=z[17]*z[24];
    z[16]=z[16] + n<T>(9,8)*z[20];
    z[16]=z[7]*z[16];
    z[20]= - static_cast<T>(7)+ z[26];
    z[13]=z[16] + n<T>(1,2)*z[20] + z[13];
    z[13]=z[7]*z[13];
    z[16]=z[5] - n<T>(3,2);
    z[16]=z[16]*z[5];
    z[20]=7*z[3];
    z[24]=z[16] + static_cast<T>(6)- z[20];
    z[24]=z[2]*z[24];
    z[26]= - static_cast<T>(7)- n<T>(9,2)*z[9];
    z[26]=z[9]*z[26];
    z[26]=z[26] + z[30];
    z[26]=z[4]*z[26];
    z[28]=static_cast<T>(17)+ z[22];
    z[26]=n<T>(1,2)*z[28] + z[26];
    z[26]=z[4]*z[26];
    z[25]=z[25]*z[5];
    z[13]=z[13] + z[26] + z[24] - z[25] - n<T>(73,4) + z[20];
    z[13]=z[7]*z[13];
    z[20]=n<T>(5,2) - z[5];
    z[20]=z[20]*z[23];
    z[24]=n<T>(13,2)*z[3];
    z[16]=z[24] - z[16];
    z[16]=z[2]*z[16];
    z[16]=z[16] + 3*z[25] + static_cast<T>(16)- 13*z[3];
    z[16]=z[2]*z[16];
    z[12]=z[12]*z[21];
    z[12]= - 8*z[9] + z[12];
    z[12]=z[4]*z[12];
    z[12]=static_cast<T>(16)+ z[12];
    z[12]=z[4]*z[12];
    z[12]=z[14] + z[13] + z[12] + z[16] + z[20] - static_cast<T>(16)+ z[24];
    z[12]=z[6]*z[12];
    z[13]=static_cast<T>(1)- z[27];
    z[13]=z[9]*z[13];
    z[13]=z[19] + z[13] - n<T>(1,4) - z[15];
    z[13]=z[4]*z[13];
    z[14]= - z[7]*z[19];
    z[15]=n<T>(3,2) - z[3];
    z[13]=z[14] + n<T>(1,2)*z[15] + z[13];
    z[13]=z[7]*z[13];
    z[14]=z[5] - n<T>(1,2);
    z[14]=z[14]*z[5];
    z[15]= - z[14] - n<T>(3,4) + z[32];
    z[15]=z[2]*z[15];
    z[11]=z[11] - n<T>(9,2) - z[22];
    z[16]=n<T>(1,4)*z[4];
    z[11]=z[11]*z[16];
    z[11]=z[13] + z[11] + z[15] + n<T>(55,8) - z[32];
    z[11]=z[7]*z[11];
    z[13]=11*z[3];
    z[15]=n<T>(3,2)*z[5];
    z[19]=static_cast<T>(2)- z[15];
    z[19]=z[5]*z[19];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(11,2)*z[3] + z[15];
    z[15]=z[2]*z[15];
    z[15]=z[15] + z[19] - n<T>(79,8) + z[13];
    z[15]=z[2]*z[15];
    z[13]=z[25] + n<T>(79,4) - z[13];
    z[19]=z[4]*z[9];
    z[19]= - n<T>(7,2) + z[19];
    z[18]=z[19]*z[18];
    z[11]=z[12] + z[11] + z[18] + n<T>(1,2)*z[13] + z[15];
    z[11]=z[6]*z[11];
    z[12]= - static_cast<T>(1)+ z[5];
    z[12]=z[12]*z[23];
    z[13]= - static_cast<T>(2)+ n<T>(1,2)*z[1];
    z[13]=z[13]*z[3];
    z[15]= - z[14] + n<T>(1,2) - z[13];
    z[15]=z[2]*z[15];
    z[18]=3*z[1];
    z[19]=static_cast<T>(13)- z[18];
    z[20]= - static_cast<T>(4)+ z[1];
    z[20]=z[3]*z[20];
    z[12]=z[15] + z[12] + n<T>(1,8)*z[19] + z[20];
    z[12]=z[2]*z[12];
    z[15]=npow(z[5],2);
    z[19]=z[15]*z[2];
    z[20]=n<T>(3,2) - z[19];
    z[20]=z[2]*z[20];
    z[20]=n<T>(9,2) + z[20];
    z[16]=z[20]*z[16];
    z[15]= - n<T>(1,2)*z[15] + z[10] + n<T>(9,4)*z[17];
    z[15]=z[7]*z[15];
    z[15]=z[15] - n<T>(3,4) + z[19];
    z[15]=z[31]*z[15];
    z[14]=z[14] - n<T>(1,4) - z[3];
    z[14]=z[2]*z[14];
    z[14]=z[14] - n<T>(9,8) + z[3] + z[15];
    z[14]=z[7]*z[14];
    z[15]= - static_cast<T>(17)+ z[18];

    r += z[11] + z[12] - z[13] + z[14] + n<T>(1,8)*z[15] + z[16];
 
    return r;
}

template double qg_2lha_r1547(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1547(const std::array<dd_real,30>&);
#endif
