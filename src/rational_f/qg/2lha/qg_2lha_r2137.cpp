#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2137(const std::array<T,30>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[6];
    z[5]=k[2];
    z[6]=k[11];
    z[7]=k[3];
    z[8]=k[9];
    z[9]=2*z[6];
    z[10]=static_cast<T>(3)+ z[9];
    z[10]=z[10]*z[9];
    z[11]=2*z[4];
    z[12]=static_cast<T>(4)- 5*z[6];
    z[12]=z[12]*z[11];
    z[10]=z[12] - static_cast<T>(7)+ z[10];
    z[10]=z[4]*z[10];
    z[12]=npow(z[4],2);
    z[13]=z[12]*z[6];
    z[14]= - static_cast<T>(6)+ 7*z[6];
    z[14]=z[14]*z[13];
    z[15]=z[2]*z[8];
    z[16]=z[15] + z[8];
    z[14]=z[14] + z[16];
    z[14]=z[7]*z[14];
    z[17]=z[2] + 1;
    z[18]=2*z[8];
    z[10]=z[14] + z[10] + z[18] + z[17];
    z[10]=z[7]*z[10];
    z[14]=z[8] - 1;
    z[17]=z[14]*z[17];
    z[11]= - static_cast<T>(3)+ z[11];
    z[19]=z[6] - 1;
    z[11]=z[4]*z[19]*z[11];
    z[13]= - z[19]*z[13];
    z[13]=z[13] - z[16];
    z[13]=z[7]*z[13];
    z[11]=z[13] + z[11] + z[17];
    z[11]=z[7]*z[11];
    z[13]=z[2] - 2;
    z[16]=z[4] - 3;
    z[17]=z[4]*z[16];
    z[17]=z[17] - z[13];
    z[17]=z[1]*z[17];
    z[11]=z[11] + z[17];
    z[11]=z[3]*z[11];
    z[17]=static_cast<T>(1)+ z[1];
    z[17]=z[2]*z[17];
    z[19]= - static_cast<T>(1)- 2*z[1];
    z[19]=z[4]*z[19];
    z[19]=z[19] + static_cast<T>(3)+ 5*z[1];
    z[19]=z[4]*z[19];
    z[10]=2*z[11] + z[10] + z[19] + z[9] + z[17] - z[18] - static_cast<T>(2)- z[1];
    z[10]=z[3]*z[10];
    z[11]=3*z[6];
    z[17]=static_cast<T>(2)- z[11];
    z[11]=z[7]*z[17]*z[12]*z[11];
    z[12]=static_cast<T>(3)- 10*z[6];
    z[12]=z[6]*z[12];
    z[17]= - static_cast<T>(1)+ z[9];
    z[17]=z[4]*z[17];
    z[12]=4*z[17] + static_cast<T>(2)+ z[12];
    z[12]=z[4]*z[12];
    z[11]=z[11] + z[12] - z[18] - z[15];
    z[11]=z[7]*z[11];
    z[12]=npow(z[6],2);
    z[15]=2*z[12];
    z[16]= - z[1] + z[16];
    z[16]=z[4]*z[16];
    z[10]=z[10] + z[11] + z[16] - z[15] + static_cast<T>(1)- z[2];
    z[10]=z[3]*z[10];
    z[11]=static_cast<T>(3)- z[2];
    z[11]=z[11]*z[9];
    z[11]= - static_cast<T>(3)+ z[11];
    z[11]=z[6]*z[11];
    z[16]=static_cast<T>(4)- z[2];
    z[16]=z[6]*z[16];
    z[16]= - static_cast<T>(2)+ z[16];
    z[16]=z[16]*z[4]*z[6];
    z[12]=z[12] + z[16];
    z[12]=z[7]*z[12];
    z[9]= - z[4]*z[9];
    z[9]=z[12] + z[9] - static_cast<T>(1)+ z[11];
    z[9]=z[4]*z[9];
    z[9]=z[15] + z[9];
    z[9]=z[7]*z[9];
    z[11]=z[5] - z[13];
    z[11]=z[6]*z[11];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[6]*z[11];

    r += z[4] + z[9] + z[10] + z[11] + z[14];
 
    return r;
}

template double qg_2lha_r2137(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2137(const std::array<dd_real,30>&);
#endif
