#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r533(const std::array<T,30>& k) {
  T z[10];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=z[4]*z[5];
    z[7]= - z[3]*z[4];
    z[7]=z[7] + z[5] - z[6];
    z[8]=n<T>(1,2) - z[4];
    z[9]=z[1] - 1;
    z[8]=z[3]*z[9]*z[8];
    z[6]=n<T>(1,2)*z[5] - z[6];
    z[6]=z[1]*z[6];
    z[6]=z[8] + z[6];
    z[6]=z[2]*z[6];
    z[6]=n<T>(1,2)*z[7] + z[6];

    r += n<T>(1,8)*z[6]*z[2];
 
    return r;
}

template double qg_2lha_r533(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r533(const std::array<dd_real,30>&);
#endif
