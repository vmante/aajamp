#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r720(const std::array<T,30>& k) {
  T z[60];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[27];
    z[7]=k[12];
    z[8]=k[11];
    z[9]=k[24];
    z[10]=k[5];
    z[11]=k[9];
    z[12]=k[14];
    z[13]=z[11] - 1;
    z[14]=n<T>(1,2)*z[10];
    z[15]=z[14]*z[13];
    z[16]=n<T>(1,2)*z[5];
    z[17]=5*z[8];
    z[18]= - z[10] + z[17];
    z[18]=z[18]*z[16];
    z[19]=npow(z[9],2);
    z[20]=z[17] + n<T>(403,18)*z[19];
    z[21]=n<T>(1,2)*z[2];
    z[20]=z[20]*z[21];
    z[22]=3*z[8];
    z[23]=npow(z[12],2);
    z[15]=z[20] - n<T>(281,18)*z[9] + z[18] - z[22] + n<T>(421,36) - z[23] + 
    z[15];
    z[15]=z[2]*z[15];
    z[18]=z[8] - z[10];
    z[20]=n<T>(1,4)*z[5];
    z[24]=z[18]*z[20];
    z[25]=n<T>(1,4)*z[10];
    z[24]=z[24] - z[25] - n<T>(403,36) - z[23];
    z[24]=z[5]*z[24];
    z[26]=z[14] + n<T>(13,3);
    z[27]=n<T>(1,2)*z[11];
    z[28]=z[26]*z[27];
    z[29]=n<T>(403,18)*z[1];
    z[30]=z[12] - 1;
    z[31]=z[30]*z[12];
    z[15]=z[15] + z[29] + n<T>(1,12)*z[9] + z[28] + z[24] + n<T>(21,4)*z[8] - 
    z[25] - n<T>(475,36) - z[31];
    z[15]=z[2]*z[15];
    z[24]=z[5]*z[6];
    z[28]=z[24] + 1;
    z[32]=n<T>(1,2)*z[6];
    z[33]=z[32] - z[28];
    z[33]=z[5]*z[33];
    z[34]=n<T>(1,2)*z[1];
    z[35]=z[28]*z[34];
    z[33]=z[35] - n<T>(1,2) + z[33];
    z[29]=z[33]*z[29];
    z[33]=11*z[8];
    z[35]= - z[33] + z[10];
    z[36]=z[7] - 1;
    z[35]=z[36]*z[35];
    z[28]= - z[6] + z[28];
    z[28]=z[5]*z[28];
    z[28]=n<T>(403,9)*z[28] + z[35];
    z[20]=z[28]*z[20];
    z[28]= - n<T>(31,3) + z[10];
    z[27]=z[28]*z[27];
    z[28]=static_cast<T>(5)- z[14];
    z[28]=z[7]*z[28];
    z[27]=z[27] + n<T>(1,6) + z[28];
    z[28]=n<T>(1,2)*z[9];
    z[27]=z[27]*z[28];
    z[35]=z[25]*z[7];
    z[36]=z[8]*z[36];
    z[37]= - n<T>(13,6)*z[11] + n<T>(13,3) + z[25];
    z[37]=z[11]*z[37];
    z[15]=z[15] + z[29] + z[27] + z[37] + z[20] - n<T>(11,4)*z[36] + z[35]
    - z[25] - n<T>(5,2) + z[31];
    z[20]=npow(z[6],2);
    z[25]=z[20]*z[5];
    z[27]= - static_cast<T>(445)+ n<T>(403,2)*z[6];
    z[27]=z[6]*z[27];
    z[18]= - n<T>(403,18)*z[25] - n<T>(1,2)*z[18] + n<T>(1,9)*z[27];
    z[18]=z[18]*z[16];
    z[27]=static_cast<T>(1)- 11*z[7];
    z[27]=z[8]*z[27];
    z[18]=z[18] + n<T>(9,4)*z[6] + n<T>(1,2)*z[27] + z[35] - z[14] - n<T>(7,3) - 
    z[23];
    z[18]=z[5]*z[18];
    z[27]=n<T>(1,9)*z[9];
    z[29]=z[9]*z[5];
    z[29]=n<T>(403,2)*z[29];
    z[35]= - static_cast<T>(161)+ z[29];
    z[35]=z[35]*z[27];
    z[13]=z[10]*z[13];
    z[36]=n<T>(9,2)*z[8];
    z[37]=n<T>(11,2)*z[8];
    z[38]=z[10] - z[37];
    z[38]=z[5]*z[38];
    z[13]=z[35] + z[38] + z[36] - n<T>(839,18) + z[13];
    z[35]=n<T>(403,4)*z[9];
    z[38]=static_cast<T>(281)+ z[35];
    z[38]=z[38]*z[27];
    z[39]=n<T>(1,4)*z[8];
    z[19]=z[19]*z[2];
    z[40]=n<T>(403,18)*z[19];
    z[38]= - z[40] + z[39] + z[38];
    z[38]=z[2]*z[38];
    z[13]=n<T>(1,2)*z[13] + z[38];
    z[13]=z[2]*z[13];
    z[38]=z[10] - 7;
    z[41]=3*z[7];
    z[38]=z[38]*z[41];
    z[42]=9*z[7];
    z[43]= - n<T>(53,3) + z[42];
    z[43]=z[5]*z[43];
    z[43]=z[43] + n<T>(1,3) - z[38];
    z[44]=n<T>(13,3)*z[5];
    z[45]=3*z[10];
    z[46]= - n<T>(65,3) + z[45];
    z[46]=n<T>(1,2)*z[46] + z[44];
    z[46]=z[11]*z[46];
    z[43]=n<T>(1,2)*z[43] + z[46];
    z[43]=z[43]*z[28];
    z[46]=static_cast<T>(305)- n<T>(403,9)*z[6];
    z[46]=z[46]*z[32];
    z[46]=z[46] + n<T>(403,9)*z[25];
    z[46]=z[46]*z[16];
    z[47]=n<T>(1,36)*z[1];
    z[48]=403*z[25];
    z[49]= - 1855*z[6] - z[48];
    z[49]=z[49]*z[47];
    z[50]=n<T>(103,4) - 11*z[6];
    z[46]=z[49] + n<T>(11,9)*z[50] + z[46];
    z[46]=z[1]*z[46];
    z[49]=n<T>(13,3)*z[11];
    z[26]= - z[49] + n<T>(13,6)*z[5] + z[26];
    z[26]=z[11]*z[26];
    z[50]= - static_cast<T>(1)+ z[45];
    z[50]=z[7]*z[50];
    z[51]=static_cast<T>(3)- n<T>(11,2)*z[7];
    z[51]=z[8]*z[51];
    z[13]=z[13] + z[46] + z[43] + z[26] + z[18] + z[51] + n<T>(1,4)*z[50] - 
    z[14] - n<T>(7,12) + z[23];
    z[14]=n<T>(7,3) - n<T>(9,4)*z[8];
    z[14]=z[6]*z[14];
    z[14]=n<T>(403,36)*z[25] + z[39] + z[14];
    z[14]=z[14]*z[16];
    z[16]=static_cast<T>(1)- z[6];
    z[16]=z[16]*z[44];
    z[18]=n<T>(1,3)*z[6];
    z[16]= - z[49] + z[16] + z[10] - z[18];
    z[16]=z[11]*z[16];
    z[25]= - n<T>(53,3) - z[38];
    z[26]= - n<T>(53,6) + z[42];
    z[26]=z[5]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[26];
    z[26]= - n<T>(11,3) + z[45];
    z[26]=n<T>(1,4)*z[26] + z[44];
    z[26]=z[11]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[26];
    z[25]=z[25]*z[28];
    z[26]= - static_cast<T>(361)+ z[29];
    z[26]=z[26]*z[27];
    z[26]=n<T>(403,6)*z[19] - z[37] + z[26];
    z[28]=n<T>(1,4)*z[2];
    z[26]=z[26]*z[28];
    z[29]=static_cast<T>(5)+ z[10];
    z[29]=z[7]*z[29];
    z[29]=n<T>(1,2)*z[29] - n<T>(9,4) - z[23];
    z[37]= - n<T>(13,3) + z[36];
    z[37]=z[37]*z[32];
    z[38]=403*z[6];
    z[39]= - static_cast<T>(1301)+ z[38];
    z[39]=z[6]*z[39];
    z[39]=n<T>(1,4)*z[39] - z[48];
    z[20]=z[20]*z[1];
    z[39]=n<T>(1,3)*z[39] + n<T>(403,4)*z[20];
    z[43]=n<T>(1,6)*z[1];
    z[39]=z[39]*z[43];
    z[45]= - n<T>(11,4)*z[7] - static_cast<T>(1)- n<T>(1,8)*z[10];
    z[45]=z[8]*z[45];
    z[14]=z[26] + z[39] + z[25] + n<T>(1,4)*z[16] + z[14] + z[37] + n<T>(1,2)*
    z[29] + z[45];
    z[16]=n<T>(1,12)*z[11];
    z[25]=13*z[11];
    z[26]= - n<T>(635,3) - z[25];
    z[26]=z[26]*z[16];
    z[29]=static_cast<T>(935)+ n<T>(137,2)*z[1];
    z[29]=z[29]*z[47];
    z[37]=7*z[8];
    z[39]=z[40] - z[37] - n<T>(563,6)*z[9];
    z[40]=npow(z[2],2);
    z[39]=z[39]*z[40];
    z[45]=z[1] + 1;
    z[45]=n<T>(31,3)*z[45];
    z[39]= - z[45] + z[39];
    z[39]=z[39]*z[28];
    z[46]=n<T>(1,2)*z[12];
    z[48]=z[46] - 1;
    z[48]=z[48]*z[12];
    z[26]=z[39] + z[29] + z[26] + n<T>(1733,72) - z[48];
    z[29]=z[1] + 2;
    z[29]=z[29]*z[1];
    z[39]=z[29] + 1;
    z[39]=n<T>(38,9)*z[39];
    z[17]=z[17] + n<T>(563,9)*z[9];
    z[50]=n<T>(1,16)*z[17];
    z[50]= - npow(z[2],3)*z[50];
    z[50]=z[39] + z[50];
    z[50]=z[2]*z[50];
    z[51]=n<T>(38,9)*z[29];
    z[52]= - n<T>(1,4)*z[12] - n<T>(38,9) + n<T>(161,36)*z[11];
    z[50]=z[50] - z[51] + z[52];
    z[50]=z[3]*z[50];
    z[26]=n<T>(1,2)*z[26] + z[50];
    z[26]=z[3]*z[26];
    z[50]=n<T>(1,3)*z[11];
    z[53]=n<T>(13,4)*z[11];
    z[54]=n<T>(38,3) + z[53];
    z[54]=z[54]*z[50];
    z[55]=n<T>(323,12)*z[1] + n<T>(47,3);
    z[55]=z[6]*z[55];
    z[56]=n<T>(301,9) - n<T>(15,2)*z[7];
    z[55]=n<T>(1,4)*z[56] + z[55];
    z[55]=z[55]*z[34];
    z[56]=n<T>(403,12)*z[19] - z[22] - n<T>(563,12)*z[9];
    z[56]=z[56]*z[28];
    z[56]= - n<T>(5,3) + z[56];
    z[56]=z[2]*z[56];
    z[57]=z[12] - n<T>(1,2);
    z[57]=z[57]*z[12];
    z[58]= - n<T>(161,18)*z[6] - n<T>(15,8)*z[7] - n<T>(17,6) + z[57];
    z[26]=z[26] + z[56] + z[55] + n<T>(1,2)*z[58] + z[54];
    z[26]=z[3]*z[26];
    z[54]=n<T>(403,3)*z[6];
    z[55]=z[54] + 53;
    z[56]=z[55]*z[18];
    z[56]=z[41] + z[56];
    z[54]=n<T>(53,2) + z[54];
    z[18]=z[54]*z[18];
    z[18]=n<T>(403,18)*z[20] + n<T>(3,2)*z[7] + z[18];
    z[18]=z[1]*z[18];
    z[54]= - z[34] - 1;
    z[54]=z[1]*z[54];
    z[54]= - n<T>(1,2) + z[54];
    z[58]=5*z[7];
    z[59]=z[58] + n<T>(563,9)*z[6];
    z[54]=z[3]*z[59]*z[54];
    z[18]=z[54] + n<T>(1,2)*z[56] + z[18];
    z[18]=z[3]*z[18];
    z[54]=z[42] + n<T>(53,3)*z[6];
    z[18]=n<T>(1,2)*z[54] + z[18];
    z[18]=z[3]*z[18];
    z[36]=z[36] + z[49];
    z[36]=z[6]*z[36];
    z[54]=n<T>(9,2)*z[7] + z[49];
    z[54]=z[9]*z[54];
    z[18]=z[18] + z[54] + z[36];
    z[18]=z[4]*z[18];
    z[36]=static_cast<T>(19)- z[10];
    z[36]=z[7]*z[36];
    z[42]=z[5]*z[42];
    z[36]=z[42] - n<T>(53,3) + z[36];
    z[42]=n<T>(49,3) + z[10];
    z[42]=n<T>(1,2)*z[42] + z[44];
    z[42]=z[11]*z[42];
    z[36]=n<T>(1,2)*z[36] + z[42];
    z[36]=z[9]*z[36];
    z[42]=z[8]*z[24];
    z[42]= - z[42] + z[7] - z[8];
    z[44]=static_cast<T>(17)+ z[10];
    z[44]=z[8]*z[44];
    z[44]= - n<T>(53,3) + z[44];
    z[44]=z[6]*z[44];
    z[42]=z[44] + 9*z[42];
    z[44]=n<T>(55,3) - z[10];
    z[32]=z[44]*z[32];
    z[24]=z[32] - n<T>(13,3)*z[24];
    z[24]=z[11]*z[24];
    z[18]=z[18] + n<T>(403,9)*z[20] + z[36] + n<T>(1,2)*z[42] + z[24];
    z[24]= - n<T>(797,2) - z[38];
    z[24]=z[6]*z[24];
    z[20]= - n<T>(403,12)*z[20] + z[7] + n<T>(1,18)*z[24];
    z[20]=z[1]*z[20];
    z[24]= - z[41] + n<T>(77,18)*z[6];
    z[24]=z[1]*z[24];
    z[24]=z[24] - n<T>(565,9)*z[6] - n<T>(31,3) - 13*z[7];
    z[24]=z[24]*z[34];
    z[24]=z[24] - n<T>(1207,36)*z[6] - n<T>(31,6) - z[58];
    z[32]=z[3]*z[39];
    z[24]=n<T>(1,4)*z[24] + z[32];
    z[24]=z[3]*z[24];
    z[32]=z[6]*z[55];
    z[20]=z[24] + n<T>(1,4)*z[20] + n<T>(1,48)*z[32] - n<T>(5,3) + n<T>(7,8)*z[7];
    z[20]=z[3]*z[20];
    z[18]=z[20] + n<T>(1,8)*z[18];
    z[18]=z[4]*z[18];
    z[14]=z[18] + n<T>(1,2)*z[14] + z[26];
    z[14]=z[4]*z[14];
    z[18]=n<T>(61,3) + z[53];
    z[18]=z[18]*z[50];
    z[20]=n<T>(33,2)*z[1];
    z[24]=n<T>(1,8)*z[1];
    z[26]=static_cast<T>(1)+ z[24];
    z[26]=z[26]*z[20];
    z[32]=403*z[9];
    z[34]=n<T>(3217,2) + z[32];
    z[34]=z[34]*z[27];
    z[36]=n<T>(403,9)*z[19];
    z[34]= - z[36] + n<T>(31,2)*z[8] + z[34];
    z[34]=z[2]*z[34];
    z[34]=z[34] - n<T>(349,3)*z[9] - n<T>(605,6) - z[37];
    z[34]=z[2]*z[34];
    z[34]= - z[45] + z[34];
    z[37]=n<T>(1,8)*z[2];
    z[34]=z[34]*z[37];
    z[37]=n<T>(881,72) + z[31];
    z[18]=z[34] + z[26] + n<T>(1,2)*z[37] + z[18];
    z[18]=z[2]*z[18];
    z[26]=n<T>(563,72)*z[9] + n<T>(38,9) + n<T>(5,8)*z[8];
    z[17]=z[17]*z[2];
    z[34]=n<T>(1,8)*z[17] - z[26];
    z[34]=z[34]*z[40];
    z[34]=z[39] + z[34];
    z[34]=z[2]*z[34];
    z[37]= - z[46] - n<T>(76,9) + n<T>(161,18)*z[11];
    z[29]=z[34] - n<T>(76,9)*z[29] - z[37];
    z[29]=z[2]*z[29];
    z[29]=z[29] + z[51] + z[37];
    z[29]=z[3]*z[29];
    z[34]= - n<T>(581,3) - z[25];
    z[34]=z[34]*z[16];
    z[20]= - n<T>(365,3) - z[20];
    z[20]=z[20]*z[24];
    z[18]=z[29] + z[18] + z[20] + z[34] + n<T>(70,9) - z[48];
    z[18]=z[3]*z[18];
    z[20]=n<T>(67,24) + z[11];
    z[20]=z[20]*z[49];
    z[24]=static_cast<T>(7)- n<T>(1049,2)*z[1];
    z[24]=z[24]*z[47];
    z[20]=z[24] + z[20] + n<T>(133,18) + z[31];
    z[24]=n<T>(3215,2) + z[32];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(403,36)*z[19] + 2*z[8] + n<T>(1,72)*z[24];
    z[24]=z[2]*z[24];
    z[22]= - n<T>(679,3) + z[22];
    z[22]=n<T>(1,2)*z[22] - n<T>(215,3)*z[9];
    z[22]=n<T>(1,8)*z[22] + z[24];
    z[22]=z[2]*z[22];
    z[24]=n<T>(23,2) - z[25];
    z[24]=z[11]*z[24];
    z[24]=n<T>(443,12)*z[1] - n<T>(41,4) + z[24];
    z[22]=n<T>(1,12)*z[24] + z[22];
    z[22]=z[2]*z[22];
    z[18]=z[18] + n<T>(1,2)*z[20] + z[22];
    z[18]=z[3]*z[18];
    z[13]=z[14] + n<T>(1,4)*z[13] + z[18];
    z[13]=z[4]*z[13];
    z[14]=n<T>(95,3) + n<T>(13,2)*z[11];
    z[14]=z[14]*z[50];
    z[18]=n<T>(1,12)*z[1];
    z[20]= - n<T>(719,2) - n<T>(457,3)*z[1];
    z[20]=z[20]*z[18];
    z[14]=z[20] + z[14] + n<T>(31,6) + z[31];
    z[20]=static_cast<T>(1933)+ z[32];
    z[20]=z[20]*z[27];
    z[20]=z[20] + n<T>(1841,9) + 17*z[8];
    z[22]= - static_cast<T>(191)- z[35];
    z[22]=z[22]*z[27];
    z[19]=n<T>(403,72)*z[19] - n<T>(17,8)*z[8] + z[22];
    z[19]=z[2]*z[19];
    z[19]=n<T>(1,8)*z[20] + z[19];
    z[19]=z[2]*z[19];
    z[20]= - n<T>(667,6)*z[1] - n<T>(401,3) - n<T>(45,2)*z[9];
    z[19]=n<T>(1,4)*z[20] + z[19];
    z[19]=z[19]*z[21];
    z[20]=static_cast<T>(49)- z[25];
    z[20]=z[11]*z[20];
    z[20]=n<T>(1,6)*z[20] - n<T>(259,36) - z[23];
    z[22]=static_cast<T>(85)+ n<T>(457,48)*z[1];
    z[22]=z[1]*z[22];
    z[19]=z[19] + n<T>(1,4)*z[20] + n<T>(1,3)*z[22];
    z[19]=z[2]*z[19];
    z[14]=n<T>(1,2)*z[14] + z[19];
    z[14]=z[2]*z[14];
    z[19]= - n<T>(527,3) - z[25];
    z[19]=z[19]*z[16];
    z[20]=static_cast<T>(13)+ n<T>(457,6)*z[1];
    z[18]=z[20]*z[18];
    z[18]=z[18] + z[19] + n<T>(113,18) - z[48];
    z[17]= - n<T>(1,16)*z[17] + z[26];
    z[17]=z[2]*z[17];
    z[19]=n<T>(38,9)*z[1];
    z[17]=z[17] - z[19] - n<T>(563,144)*z[9] - n<T>(76,9) - n<T>(5,16)*z[8];
    z[17]=z[2]*z[17];
    z[17]=z[39] + z[17];
    z[17]=z[2]*z[17];
    z[20]=npow(z[1],2);
    z[17]=z[17] - n<T>(38,3)*z[20] + z[52];
    z[17]=z[2]*z[17];
    z[20]= - n<T>(2,3) + z[1];
    z[20]=z[1]*z[20];
    z[17]=z[17] + n<T>(38,3)*z[20] - z[37];
    z[17]=z[2]*z[17];
    z[20]=static_cast<T>(1)- z[1];
    z[19]=z[20]*z[19];
    z[17]=z[17] + z[19] + z[52];
    z[17]=z[3]*z[17];
    z[14]=z[17] + n<T>(1,2)*z[18] + z[14];
    z[14]=z[3]*z[14];
    z[17]=z[30]*z[46];
    z[18]=n<T>(127,4) - z[25];
    z[16]=z[18]*z[16];
    z[18]=z[36] - z[33] - n<T>(965,9)*z[9];
    z[18]=z[18]*z[21];
    z[19]= - static_cast<T>(157)- z[32];
    z[19]=z[9]*z[19];
    z[18]=z[18] + n<T>(1,18)*z[19] + n<T>(973,9) - n<T>(5,2)*z[8];
    z[18]=z[18]*z[28];
    z[18]=z[18] - n<T>(941,72)*z[1] + n<T>(565,72)*z[9] + n<T>(5,6)*z[11] + n<T>(15,8)*
    z[8] - n<T>(2051,72) - z[57];
    z[18]=z[18]*z[21];
    z[19]=static_cast<T>(493)- n<T>(403,3)*z[1];
    z[19]=z[1]*z[19];
    z[16]=z[18] + n<T>(1,48)*z[19] + z[16] - static_cast<T>(1)+ z[17];
    z[16]=z[2]*z[16];
    z[17]= - static_cast<T>(269)+ n<T>(403,2)*z[1];
    z[17]=z[17]*z[43];
    z[18]=n<T>(161,12) + z[25];
    z[18]=z[11]*z[18];
    z[17]=z[17] + n<T>(83,4) + z[18];
    z[14]=z[14] + n<T>(1,12)*z[17] + z[16];
    z[14]=z[3]*z[14];

    r += z[13] + z[14] + n<T>(1,4)*z[15];
 
    return r;
}

template double qg_2lha_r720(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r720(const std::array<dd_real,30>&);
#endif
