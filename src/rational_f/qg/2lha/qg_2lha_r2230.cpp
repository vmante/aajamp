#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2230(const std::array<T,30>& k) {
  T z[34];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[6];
    z[6]=k[8];
    z[7]=k[3];
    z[8]=k[2];
    z[9]=k[11];
    z[10]=k[9];
    z[11]=k[14];
    z[12]=n<T>(1,3)*z[1];
    z[13]=npow(z[6],2);
    z[14]=z[12]*z[13];
    z[15]=n<T>(1,2)*z[9];
    z[16]= - n<T>(17,2) - 3*z[9];
    z[16]=z[16]*z[15];
    z[17]=npow(z[11],2);
    z[18]=static_cast<T>(1)+ z[9];
    z[18]=z[9]*z[18];
    z[18]=z[17] + z[18];
    z[19]=npow(z[9],2);
    z[20]=z[19]*z[2];
    z[18]=n<T>(3,2)*z[18] - z[20];
    z[18]=z[2]*z[18];
    z[21]=static_cast<T>(1)+ n<T>(5,4)*z[9];
    z[21]=z[21]*z[9];
    z[22]=n<T>(3,2)*z[17];
    z[21]=z[21] + z[22];
    z[23]=z[20] - z[21];
    z[23]=z[7]*z[23];
    z[24]=n<T>(3,2) - z[11];
    z[24]=z[11]*z[24];
    z[24]=n<T>(1,4) + z[24];
    z[16]=z[23] + z[18] + z[16] + 3*z[24] - z[14];
    z[16]=z[7]*z[16];
    z[18]=z[1]*z[13];
    z[18]= - n<T>(7,4)*z[6] + z[18];
    z[18]=z[18]*z[12];
    z[22]=z[22]*z[2];
    z[16]=z[16] - z[22] + static_cast<T>(1)+ z[18];
    z[18]=z[11] - 1;
    z[23]=3*z[11];
    z[18]=z[18]*z[23];
    z[12]= - z[6]*z[12];
    z[17]=3*z[17];
    z[24]= - z[17] - z[9];
    z[24]=z[2]*z[24];
    z[12]=z[24] + n<T>(5,2)*z[9] + z[12] + n<T>(19,12) + z[18];
    z[24]=n<T>(1,2)*z[2];
    z[25]=z[24]*z[19];
    z[21]= - z[25] + z[21];
    z[21]=z[21]*z[24];
    z[26]= - n<T>(1,6) - z[18];
    z[27]= - n<T>(7,4) - z[9];
    z[27]=z[9]*z[27];
    z[21]=z[21] + n<T>(1,2)*z[26] + z[27];
    z[21]=z[7]*z[21];
    z[12]=n<T>(1,2)*z[12] + z[21];
    z[12]=z[7]*z[12];
    z[21]=n<T>(1,2)*z[1];
    z[26]= - z[13]*z[21];
    z[26]=z[6] + z[26];
    z[26]=z[1]*z[26];
    z[26]= - n<T>(11,4) + z[26];
    z[22]=n<T>(1,3)*z[26] + z[22];
    z[12]=n<T>(1,2)*z[22] + z[12];
    z[12]=z[5]*z[12];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[12]=z[5]*z[12];
    z[16]=z[10] - 1;
    z[22]=z[16]*z[4];
    z[22]=z[22] + z[10];
    z[26]=z[2]*z[10];
    z[27]=z[26]*z[4];
    z[28]=z[27] + z[22];
    z[28]=z[2]*z[28];
    z[29]=z[1]*z[4];
    z[28]=z[28] + z[29] + z[16];
    z[28]=z[2]*z[28];
    z[30]=z[29] + z[4];
    z[31]=static_cast<T>(1)- z[30];
    z[31]=z[1]*z[31];
    z[28]=z[31] + z[28];
    z[28]=z[28]*z[24];
    z[31]=static_cast<T>(3)+ z[4];
    z[31]=n<T>(1,2)*z[31] + z[29];
    z[31]=z[1]*z[31];
    z[28]=z[28] + n<T>(3,2) + z[31];
    z[28]=z[2]*z[28];
    z[16]= - z[26] - z[16];
    z[16]=z[2]*z[16];
    z[16]=static_cast<T>(3)+ z[16];
    z[16]=z[16]*z[24];
    z[31]=3*z[10] + z[26];
    z[24]=z[31]*z[24];
    z[24]=z[10] + z[24];
    z[24]=z[7]*z[24];
    z[16]=z[24] + static_cast<T>(1)+ z[16];
    z[16]=z[7]*z[16];
    z[24]= - 3*z[29] - static_cast<T>(7)- z[4];
    z[24]=z[24]*z[21];
    z[16]=z[16] + z[28] - static_cast<T>(3)+ z[24];
    z[24]=static_cast<T>(11)+ z[4];
    z[24]=n<T>(1,4)*z[24] + z[29];
    z[24]=z[1]*z[24];
    z[28]= - static_cast<T>(1)- n<T>(1,4)*z[29];
    z[28]=z[1]*z[28];
    z[31]=z[15] - z[11];
    z[32]=z[7]*z[31];
    z[32]=n<T>(5,4) + z[32];
    z[32]=z[7]*z[32];
    z[28]=z[32] - n<T>(3,4) + z[28];
    z[28]=z[5]*z[28];
    z[24]=z[28] - n<T>(7,4)*z[7] + n<T>(9,4) + z[24];
    z[24]=z[5]*z[24];
    z[16]=n<T>(1,2)*z[16] + z[24];
    z[16]=z[3]*z[16];
    z[24]= - n<T>(1,2) + 3*z[4];
    z[28]=z[24]*z[21];
    z[22]= - n<T>(1,2)*z[22] - z[27];
    z[27]=npow(z[2],2);
    z[22]=z[22]*z[27];
    z[32]= - z[4] + z[29];
    z[32]=z[1]*z[32];
    z[32]= - static_cast<T>(3)+ z[32];
    z[22]=n<T>(1,2)*z[32] + z[22];
    z[32]=n<T>(3,2)*z[2];
    z[22]=z[22]*z[32];
    z[26]= - z[10] - n<T>(3,4)*z[26];
    z[26]=z[7]*z[26];
    z[22]=3*z[26] + z[22] + static_cast<T>(5)+ z[28];
    z[26]=n<T>(1,2)*z[11];
    z[28]=static_cast<T>(3)- z[26];
    z[28]=z[28]*z[23];
    z[32]=n<T>(3,2)*z[9];
    z[33]= - static_cast<T>(11)- z[32];
    z[33]=z[33]*z[15];
    z[28]=z[28] + z[33];
    z[28]=z[7]*z[28];
    z[28]=z[28] + z[32] - n<T>(25,4) - z[23];
    z[28]=z[7]*z[28];
    z[32]=static_cast<T>(7)+ n<T>(3,2)*z[29];
    z[21]=z[32]*z[21];
    z[21]=z[28] + static_cast<T>(5)+ z[21];
    z[21]=z[5]*z[21];
    z[28]= - static_cast<T>(11)- n<T>(9,2)*z[30];
    z[28]=z[1]*z[28];
    z[28]= - n<T>(49,2) + z[28];
    z[30]= - n<T>(9,4)*z[9] + static_cast<T>(2)+ n<T>(9,2)*z[11];
    z[30]=z[7]*z[30];
    z[21]=n<T>(1,2)*z[21] + n<T>(1,4)*z[28] + z[30];
    z[21]=z[5]*z[21];
    z[16]=n<T>(3,2)*z[16] + n<T>(1,2)*z[22] + z[21];
    z[16]=z[3]*z[16];
    z[21]=z[4]*z[10]*z[27];
    z[21]= - z[29] + z[21];
    z[21]=z[2]*z[21];
    z[22]=n<T>(1,2) + z[10];
    z[21]=n<T>(1,4)*z[21] + n<T>(1,2)*z[22] + z[31];
    z[22]=z[26] - 1;
    z[26]= - z[22]*z[23];
    z[27]=static_cast<T>(15)+ n<T>(11,2)*z[9];
    z[15]=z[27]*z[15];
    z[22]=z[11]*z[22];
    z[15]=9*z[22] + z[15];
    z[15]=z[7]*z[15];
    z[15]=n<T>(1,2)*z[15] - 2*z[9] + n<T>(5,8) + z[26];
    z[15]=z[7]*z[15];
    z[15]=z[15] - static_cast<T>(1)- n<T>(1,4)*z[1];
    z[15]=z[5]*z[15];
    z[22]= - n<T>(9,4) + z[11];
    z[22]=z[22]*z[23];
    z[23]=n<T>(13,2) + z[9];
    z[23]=z[9]*z[23];
    z[22]=n<T>(3,4)*z[23] + n<T>(1,8) + z[22];
    z[22]=z[7]*z[22];
    z[23]=z[1]*z[24];
    z[23]=static_cast<T>(7)+ z[23];
    z[15]=z[15] + n<T>(1,4)*z[23] + z[22];
    z[15]=z[5]*z[15];
    z[15]=z[16] + n<T>(3,2)*z[21] + z[15];
    z[15]=z[3]*z[15];
    z[16]=n<T>(1,2)*z[6];
    z[21]=z[16] - z[14];
    z[21]=z[1]*z[21];
    z[22]= - n<T>(1,2) - z[8];
    z[22]=z[9]*z[22];
    z[22]=z[22] - 1;
    z[22]=z[8]*z[22];
    z[22]= - n<T>(5,2) + z[22];
    z[22]=z[9]*z[22];
    z[18]=z[22] + z[21] + static_cast<T>(1)- z[18];
    z[21]=n<T>(1,4) + z[8];
    z[21]=z[9]*z[21];
    z[21]=n<T>(1,2) + z[21];
    z[21]=z[9]*z[21];
    z[21]=z[21] - z[25];
    z[21]=z[2]*z[21];
    z[18]=n<T>(1,2)*z[18] + z[21];
    z[16]= - z[16] - z[17];
    z[17]= - n<T>(3,2) - z[8];
    z[17]=z[9]*z[17];
    z[17]= - n<T>(3,2) + z[17];
    z[17]=z[9]*z[17];
    z[14]=z[17] + n<T>(1,2)*z[16] + z[14];
    z[13]= - n<T>(1,3)*z[13] - z[19];
    z[13]=z[7]*z[13];
    z[13]=n<T>(1,4)*z[13] + n<T>(1,2)*z[14] + z[20];
    z[13]=z[7]*z[13];

    r += z[12] + z[13] + z[15] + n<T>(1,2)*z[18];
 
    return r;
}

template double qg_2lha_r2230(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2230(const std::array<dd_real,30>&);
#endif
