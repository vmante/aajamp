#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1039(const std::array<T,30>& k) {
  T z[11];
  T r = 0;

    z[1]=k[4];
    z[2]=k[9];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[5];
    z[6]=k[6];
    z[7]=n<T>(1,2)*z[6];
    z[8]= - 3*z[5] - z[4];
    z[8]=z[8]*z[7];
    z[9]=z[2]*z[5];
    z[10]=n<T>(1,2) + z[2];
    z[10]=z[4]*z[10];
    z[8]=z[8] + z[9] + z[10];
    z[8]=z[8]*z[7];
    z[10]=z[5] + z[4];
    z[7]=z[10]*z[7];
    z[10]= - static_cast<T>(1)- n<T>(1,2)*z[2];
    z[10]=z[4]*z[10];
    z[7]=z[7] - n<T>(1,2)*z[9] + z[10];
    z[7]=z[6]*z[7];
    z[10]=static_cast<T>(1)- z[1];
    z[10]=z[2]*z[10];
    z[10]=static_cast<T>(1)+ z[10];
    z[10]=z[4]*z[10];
    z[10]= - z[2] + z[10];
    z[7]=n<T>(1,2)*z[10] + z[7];
    z[7]=z[3]*z[7];
    z[7]=z[8] + z[7];
    z[7]=z[3]*z[7];
    z[8]= - z[4]*z[2];
    z[10]=z[6]*z[5];
    z[8]=z[10] - z[9] + z[8];
    z[8]=z[6]*z[8];
    z[7]=n<T>(1,4)*z[8] + z[7];

    r += n<T>(1,4)*z[7]*z[3];
 
    return r;
}

template double qg_2lha_r1039(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1039(const std::array<dd_real,30>&);
#endif
