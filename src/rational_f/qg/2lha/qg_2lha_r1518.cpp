#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1518(const std::array<T,30>& k) {
  T z[31];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=k[9];
    z[8]=k[11];
    z[9]=k[3];
    z[10]=k[12];
    z[11]=31*z[4];
    z[12]= - n<T>(19,2) - z[5];
    z[12]=z[5]*z[12];
    z[12]= - static_cast<T>(19)+ z[12];
    z[12]=3*z[12] - z[11];
    z[13]=n<T>(1,2)*z[2];
    z[11]=9*z[6] + z[11];
    z[11]=z[11]*z[13];
    z[14]= - static_cast<T>(7)- n<T>(31,2)*z[4];
    z[11]=3*z[14] + z[11];
    z[11]=z[2]*z[11];
    z[14]=93*z[4] + n<T>(213,2) + 23*z[5];
    z[11]=n<T>(1,2)*z[14] + z[11];
    z[11]=z[2]*z[11];
    z[11]=n<T>(1,2)*z[12] + z[11];
    z[12]=n<T>(9,2)*z[4];
    z[14]=5*z[5];
    z[15]=z[12] + n<T>(19,2) + z[14];
    z[16]=9*z[4];
    z[17]= - z[6] + z[16];
    z[17]=z[17]*z[13];
    z[12]= - static_cast<T>(5)- z[12];
    z[12]=3*z[12] + z[17];
    z[12]=z[2]*z[12];
    z[12]=3*z[15] + z[12];
    z[12]=z[12]*z[13];
    z[15]=n<T>(1,2)*z[5];
    z[17]= - n<T>(29,2) - z[14];
    z[17]=z[17]*z[15];
    z[12]=z[12] - n<T>(9,4)*z[4] - static_cast<T>(7)+ z[17];
    z[12]=z[7]*z[12];
    z[17]=n<T>(1,4)*z[2];
    z[18]= - z[6] - 19*z[4];
    z[18]=z[18]*z[17];
    z[18]=z[18] + static_cast<T>(11)+ n<T>(57,4)*z[4];
    z[18]=z[2]*z[18];
    z[19]= - n<T>(57,2)*z[4] - n<T>(185,4) - 17*z[5];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[18]=z[18]*z[13];
    z[19]=z[5] + n<T>(9,2);
    z[20]=z[5]*z[19];
    z[12]=n<T>(1,4)*z[12] + z[18] + n<T>(19,8)*z[4] + n<T>(47,8) + z[20];
    z[12]=z[7]*z[12];
    z[11]=n<T>(1,4)*z[11] + z[12];
    z[11]=z[7]*z[11];
    z[12]=3*z[4];
    z[18]=n<T>(1,2) + z[12];
    z[20]= - 2*z[6] - z[12];
    z[20]=z[2]*z[20];
    z[18]=3*z[18] + z[20];
    z[18]=z[2]*z[18];
    z[20]= - n<T>(99,8) - z[5];
    z[16]=z[18] + n<T>(1,2)*z[20] - z[16];
    z[16]=z[2]*z[16];
    z[18]=7*z[5];
    z[20]=z[18] + 29;
    z[11]=z[11] + z[16] + n<T>(1,8)*z[20] + z[12];
    z[11]=z[7]*z[11];
    z[16]=z[4] + z[6];
    z[21]=z[16]*z[17];
    z[21]=z[21] - z[4];
    z[22]=z[21]*z[2];
    z[23]=n<T>(5,4)*z[4];
    z[24]= - static_cast<T>(1)- z[23];
    z[24]=3*z[24] - n<T>(5,2)*z[22];
    z[24]=z[2]*z[24];
    z[25]=n<T>(3,2)*z[5];
    z[26]=n<T>(5,2)*z[4];
    z[24]=z[24] + z[26] + static_cast<T>(4)+ z[25];
    z[24]=z[2]*z[24];
    z[19]= - z[19]*z[15];
    z[19]= - z[23] - static_cast<T>(3)+ z[19];
    z[23]=static_cast<T>(1)+ z[4];
    z[22]=n<T>(3,2)*z[23] + z[22];
    z[22]=z[22]*z[13];
    z[22]=z[22] - n<T>(1,2)*z[4] - static_cast<T>(1)- z[15];
    z[22]=z[2]*z[22];
    z[23]=static_cast<T>(3)+ z[5];
    z[23]=z[5]*z[23];
    z[23]=z[4] + static_cast<T>(3)+ z[23];
    z[22]=n<T>(1,8)*z[23] + z[22];
    z[22]=z[7]*z[22];
    z[19]=z[22] + n<T>(1,2)*z[19] + z[24];
    z[19]=z[7]*z[19];
    z[22]=5*z[2];
    z[21]=z[21]*z[22];
    z[22]=5*z[4];
    z[23]=static_cast<T>(3)+ z[22];
    z[23]=n<T>(3,2)*z[23] + z[21];
    z[23]=z[2]*z[23];
    z[24]= - static_cast<T>(2)- z[15];
    z[23]=z[23] + 3*z[24] - z[22];
    z[23]=z[2]*z[23];
    z[24]=static_cast<T>(9)+ z[5];
    z[24]=z[24]*z[15];
    z[24]=z[22] + static_cast<T>(9)+ z[24];
    z[19]=z[19] + n<T>(1,4)*z[24] + z[23];
    z[19]=z[7]*z[19];
    z[23]= - static_cast<T>(1)- z[26];
    z[21]=3*z[23] - z[21];
    z[21]=z[2]*z[21];
    z[21]=z[21] + z[22] + static_cast<T>(4)+ z[15];
    z[21]=z[2]*z[21];
    z[23]=n<T>(1,4)*z[5];
    z[24]= - static_cast<T>(1)- z[23];
    z[24]=3*z[24] - z[26];
    z[19]=z[19] + n<T>(1,2)*z[24] + z[21];
    z[19]=z[7]*z[19];
    z[21]= - static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[21]=z[21]*z[4];
    z[24]=z[5]*z[6];
    z[27]= - z[21] - n<T>(1,4)*z[24] - n<T>(1,4) + z[6];
    z[27]=z[27]*z[13];
    z[28]=n<T>(1,2)*z[1];
    z[29]=z[28] - 2;
    z[29]=z[29]*z[4];
    z[27]=z[29] + z[27];
    z[27]=z[2]*z[27];
    z[30]=n<T>(1,4) - z[21];
    z[27]=3*z[30] + z[27];
    z[27]=z[2]*z[27];
    z[27]=z[27] - static_cast<T>(1)+ z[29];
    z[27]=z[2]*z[27];
    z[21]=n<T>(3,4) - z[21];
    z[19]=z[19] + n<T>(1,2)*z[21] + z[27];
    z[19]=z[3]*z[19];
    z[21]= - n<T>(11,2) + z[1];
    z[12]=z[21]*z[12];
    z[21]=z[2] + n<T>(7,2) - z[1];
    z[21]=z[4]*z[21];
    z[21]= - 3*z[24] - static_cast<T>(3)+ n<T>(17,2)*z[6] + z[21];
    z[21]=z[2]*z[21];
    z[12]=z[21] + static_cast<T>(1)+ z[12];
    z[12]=z[2]*z[12];
    z[21]=n<T>(37,2) - 3*z[1];
    z[21]=z[4]*z[21];
    z[12]=z[12] + static_cast<T>(7)+ z[21];
    z[12]=z[2]*z[12];
    z[21]= - n<T>(13,2) + z[1];
    z[21]=z[4]*z[21];
    z[12]=z[12] - static_cast<T>(5)+ z[21];
    z[11]=z[19] + n<T>(1,8)*z[12] + z[11];
    z[11]=z[3]*z[11];
    z[12]=n<T>(71,2) + 3*z[5];
    z[12]=z[12]*z[15];
    z[19]=n<T>(35,2)*z[4];
    z[21]=z[19] + n<T>(11,2) + z[6];
    z[21]=z[2]*z[21];
    z[18]=z[21] - 35*z[4] - n<T>(79,2) - z[18];
    z[18]=z[2]*z[18];
    z[12]=z[18] + z[19] + static_cast<T>(35)+ z[12];
    z[18]=n<T>(11,4) + z[5];
    z[18]=z[18]*z[14];
    z[18]=n<T>(51,4) + z[18];
    z[19]= - n<T>(7,4) - z[5];
    z[21]=n<T>(1,4)*z[6];
    z[27]=static_cast<T>(5)+ z[21];
    z[27]=n<T>(1,4)*z[27] + z[4];
    z[27]=z[2]*z[27];
    z[19]=z[27] + n<T>(5,2)*z[19] - 2*z[4];
    z[19]=z[2]*z[19];
    z[18]=z[19] + n<T>(1,4)*z[18] + z[4];
    z[18]=z[7]*z[18];
    z[19]=n<T>(55,8)*z[4];
    z[27]= - z[19] - static_cast<T>(5)- n<T>(1,8)*z[6];
    z[27]=z[27]*z[13];
    z[19]=z[27] + z[19] + n<T>(91,8) + 4*z[5];
    z[19]=z[2]*z[19];
    z[25]= - static_cast<T>(7)- z[25];
    z[25]=z[5]*z[25];
    z[18]=z[18] + z[19] - n<T>(55,16)*z[4] - n<T>(71,8) + z[25];
    z[18]=z[7]*z[18];
    z[12]=n<T>(1,4)*z[12] + z[18];
    z[12]=z[7]*z[12];
    z[18]=n<T>(25,2) + z[5];
    z[19]= - z[26] - n<T>(1,4) - z[6];
    z[19]=z[2]*z[19];
    z[18]=z[19] + n<T>(1,4)*z[18] + z[22];
    z[18]=z[2]*z[18];
    z[19]= - n<T>(1,4)*z[20] - z[22];
    z[12]=z[12] + n<T>(1,2)*z[19] + z[18];
    z[12]=z[7]*z[12];
    z[18]=n<T>(1,4)*z[4];
    z[19]=static_cast<T>(3)- z[1];
    z[19]=z[19]*z[18];
    z[13]=z[4]*z[13];
    z[13]=z[13] + z[19] - n<T>(7,4)*z[24] - n<T>(5,4) + 3*z[6];
    z[13]=z[2]*z[13];
    z[19]= - static_cast<T>(3)+ z[28];
    z[19]=z[4]*z[19];
    z[13]=z[13] - static_cast<T>(1)+ z[19];
    z[13]=z[2]*z[13];
    z[19]=static_cast<T>(7)- z[1];
    z[19]=z[4]*z[19];
    z[19]=static_cast<T>(9)+ z[19];
    z[13]=n<T>(1,4)*z[19] + z[13];
    z[11]=z[11] + n<T>(1,4)*z[13] + z[12];
    z[11]=z[3]*z[11];
    z[12]=n<T>(1,8)*z[4];
    z[13]=static_cast<T>(1)+ n<T>(1,4)*z[8];
    z[19]=n<T>(11,4)*z[6] - z[13];
    z[20]=z[4]*z[17];
    z[19]=z[20] + z[12] + n<T>(1,2)*z[19] - z[24];
    z[19]=z[2]*z[19];
    z[20]=n<T>(1,2)*z[10];
    z[22]=z[20] - z[8];
    z[25]=z[5]*z[22];
    z[25]= - n<T>(3,2)*z[4] + n<T>(5,2) + z[25];
    z[19]=n<T>(1,4)*z[25] + z[19];
    z[25]=z[10] - z[8];
    z[25]=z[25]*z[23];
    z[25]=z[25] + z[20] - z[13];
    z[25]=z[5]*z[25];
    z[26]= - static_cast<T>(17)+ z[20];
    z[25]=n<T>(1,2)*z[26] + z[25];
    z[25]=z[5]*z[25];
    z[26]=n<T>(69,8)*z[4];
    z[27]=z[26] + z[5] + static_cast<T>(7)- n<T>(3,8)*z[6];
    z[27]=z[2]*z[27];
    z[25]=z[27] - z[26] - n<T>(67,4) + z[25];
    z[26]=n<T>(7,2)*z[4];
    z[21]=z[26] + z[14] + static_cast<T>(7)- z[21];
    z[21]=z[21]*z[17];
    z[27]= - static_cast<T>(3)- n<T>(5,4)*z[5];
    z[27]=z[5]*z[27];
    z[21]=z[21] - n<T>(7,8)*z[4] - n<T>(43,16) + z[27];
    z[21]=z[7]*z[21];
    z[27]= - static_cast<T>(9)+ n<T>(1,2)*z[6];
    z[14]= - n<T>(37,4)*z[4] + n<T>(3,2)*z[27] - z[14];
    z[14]=z[14]*z[17];
    z[17]=n<T>(19,4) + z[5];
    z[17]=z[5]*z[17];
    z[14]=z[21] + z[14] + n<T>(37,16)*z[4] + n<T>(47,8) + z[17];
    z[14]=z[7]*z[14];
    z[14]=n<T>(1,4)*z[25] + z[14];
    z[14]=z[7]*z[14];
    z[17]= - z[22]*z[15];
    z[13]=z[17] - n<T>(1,4)*z[10] + z[13];
    z[13]=z[5]*z[13];
    z[17]= - z[26] - static_cast<T>(1)- n<T>(3,2)*z[6];
    z[17]=z[2]*z[17];
    z[13]=z[17] + z[26] + n<T>(7,2) + z[13];
    z[13]=n<T>(1,4)*z[13] + z[14];
    z[13]=z[7]*z[13];
    z[11]=z[11] + n<T>(1,4)*z[19] + z[13];
    z[11]=z[3]*z[11];
    z[13]=z[9]*z[10];
    z[14]=z[8]*z[9];
    z[17]=z[13] + 3*z[14];
    z[17]=z[17]*z[15];
    z[19]=n<T>(1,2)*z[14];
    z[17]=z[17] - z[19] + static_cast<T>(5)+ z[13];
    z[17]=z[5]*z[17];
    z[17]=z[17] + static_cast<T>(7)+ z[19];
    z[17]=z[5]*z[17];
    z[19]=n<T>(1,2)*z[13];
    z[21]=static_cast<T>(3)- z[19];
    z[12]=z[21]*z[12];
    z[21]=z[14] - 1;
    z[21]=z[21]*z[6];
    z[22]=n<T>(1,16)*z[14];
    z[25]=n<T>(1,8)*z[13];
    z[12]=z[12] + n<T>(1,8)*z[17] - n<T>(1,16)*z[21] - z[22] + static_cast<T>(1)- z[25];
    z[12]=z[7]*z[12];
    z[17]=n<T>(1,4)*z[14];
    z[26]=n<T>(3,8)*z[21] + z[17] - static_cast<T>(3)+ z[25];
    z[27]=z[10] + z[8];
    z[27]=z[27]*z[15];
    z[28]=static_cast<T>(1)- z[9];
    z[28]=z[8]*z[28];
    z[27]=z[27] + n<T>(1,2)*z[28] - n<T>(1,4)*z[13] - static_cast<T>(1)+ z[10];
    z[23]=z[27]*z[23];
    z[22]=z[23] - z[22] - n<T>(1,16)*z[13] - static_cast<T>(1)+ n<T>(1,8)*z[10];
    z[22]=z[5]*z[22];
    z[13]= - static_cast<T>(11)+ z[13];
    z[13]=z[4]*z[13];
    z[12]=z[12] + n<T>(1,16)*z[13] + n<T>(1,2)*z[26] + z[22];
    z[12]=z[7]*z[12];
    z[13]= - z[10]*z[15];
    z[13]=z[13] - n<T>(3,8)*z[14] + z[25] + static_cast<T>(1)- z[20];
    z[13]=z[5]*z[13];
    z[14]= - n<T>(3,4)*z[21] + static_cast<T>(3)+ z[17];
    z[19]=static_cast<T>(7)- z[19];
    z[18]=z[19]*z[18];
    z[13]=z[18] + n<T>(1,2)*z[14] + z[13];
    z[12]=n<T>(1,4)*z[13] + z[12];
    z[12]=z[7]*z[12];
    z[13]=z[10] - 3*z[8];
    z[13]=z[13]*z[15];
    z[13]=z[13] + z[17] - z[16];
    z[12]=n<T>(1,8)*z[13] + z[12];
    z[12]=z[7]*z[12];
    z[13]=z[8] + 5*z[6];
    z[13]=n<T>(1,2)*z[13] - z[24];

    r += z[11] + z[12] + n<T>(1,16)*z[13];
 
    return r;
}

template double qg_2lha_r1518(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1518(const std::array<dd_real,30>&);
#endif
