#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r2121(const std::array<T,30>& k) {
  T z[10];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[3];
    z[5]=k[9];
    z[6]=z[1] + 1;
    z[7]= - z[4] + z[6];
    z[7]=z[2]*z[7];
    z[7]= - 2*z[6] + z[7];
    z[7]=z[2]*z[7];
    z[8]=z[4]*z[5];
    z[8]=z[8] + 1;
    z[9]=z[4]*z[8];
    z[6]=z[7] + z[9] + z[6];
    z[6]=z[3]*z[6];
    z[7]=2*z[4] - static_cast<T>(2)- z[1];
    z[7]=z[2]*z[7];
    z[7]=z[7] + n<T>(7,2) + z[1];
    z[7]=z[2]*z[7];
    z[6]=z[6] - n<T>(3,2)*z[8] + z[7];
    z[6]=z[3]*z[6];
    z[7]=z[4] - 1;
    z[7]= - z[2]*z[7];
    z[7]= - n<T>(3,2) + z[7];
    z[7]=z[2]*z[7];
    z[6]=z[6] + n<T>(1,2)*z[5] + z[7];

    r += z[6]*z[3];
 
    return r;
}

template double qg_2lha_r2121(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r2121(const std::array<dd_real,30>&);
#endif
