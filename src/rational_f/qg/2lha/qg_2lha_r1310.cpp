#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1310(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[9];
    z[4]=k[11];
    z[5]=k[13];
    z[6]=npow(z[3],2);
    z[7]=n<T>(1,2)*z[2];
    z[8]=z[6]*z[7];
    z[9]=n<T>(1,2) - z[3];
    z[9]=z[3]*z[9];
    z[9]=z[9] + z[8];
    z[9]=z[1]*z[9];
    z[10]=z[1] + z[2];
    z[11]= - static_cast<T>(1)- z[10];
    z[12]=z[7] - z[1];
    z[12]=z[5]*z[12];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[5]*z[11];
    z[12]=z[2] + 1;
    z[12]=z[3]*z[12];
    z[9]=z[11] + n<T>(1,2)*z[12] + z[9];
    z[8]= - z[1]*z[8];
    z[8]=z[8] - z[12];
    z[8]=z[1]*z[8];
    z[11]= - z[5]*z[7];
    z[11]=z[11] + z[10];
    z[11]=z[5]*z[11];
    z[7]=z[11] - z[7] + z[8];
    z[7]=z[4]*z[7];
    z[7]=n<T>(1,2)*z[9] + z[7];
    z[7]=z[4]*z[7];
    z[8]=z[10]*npow(z[5],2);
    z[6]=z[6] + z[8];
    z[6]=n<T>(1,4)*z[6] + z[7];
    z[6]=z[4]*z[6];
    z[7]=z[5]*z[1];
    z[7]=static_cast<T>(1)+ z[7];
    z[7]=z[5]*z[7];
    z[6]=n<T>(1,4)*z[7] + z[6];

    r += n<T>(1,4)*z[6];
 
    return r;
}

template double qg_2lha_r1310(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1310(const std::array<dd_real,30>&);
#endif
