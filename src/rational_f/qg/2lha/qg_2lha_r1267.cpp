#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1267(const std::array<T,30>& k) {
  T z[10];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[10];
    z[6]=2*z[2];
    z[7]= - z[6] + 2*z[1] + static_cast<T>(4)+ z[5];
    z[6]=z[7]*z[6];
    z[7]=z[1] + 2;
    z[6]=z[6] - z[5] - z[7];
    z[8]= - static_cast<T>(1)- n<T>(1,3)*z[1];
    z[8]=z[1]*z[8];
    z[9]= - n<T>(1,3)*z[2] + static_cast<T>(1)+ n<T>(2,3)*z[1];
    z[9]=z[2]*z[9];
    z[8]=z[9] + z[8] - static_cast<T>(1)- n<T>(1,3)*z[5];
    z[8]=z[3]*z[2]*z[8];
    z[6]=n<T>(1,3)*z[6] + z[8];
    z[6]=z[3]*z[6];
    z[8]=z[2] - 1;
    z[6]=z[6] + n<T>(2,3)*z[5] - z[8];
    z[6]=z[4]*z[6];
    z[7]= - z[1]*z[7];
    z[9]=z[1] + 1;
    z[9]=2*z[9] - z[2];
    z[9]=z[2]*z[9];
    z[7]=z[9] - static_cast<T>(1)+ z[7];
    z[7]=z[3]*z[7];
    z[8]=z[1] - z[8];
    z[7]=4*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[6]=z[6] - static_cast<T>(1)+ n<T>(1,3)*z[7];

    r += z[6]*z[4];
 
    return r;
}

template double qg_2lha_r1267(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1267(const std::array<dd_real,30>&);
#endif
