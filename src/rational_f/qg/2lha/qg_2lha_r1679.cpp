#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1679(const std::array<T,30>& k) {
  T z[9];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[14];
    z[4]=k[9];
    z[5]=k[4];
    z[6]=z[4] + z[2];
    z[7]=z[1] - 1;
    z[8]=z[7] - z[5];
    z[6]=z[3]*z[8]*z[6];
    z[8]=z[5] + 1;
    z[8]=z[4]*z[8];
    z[7]= - z[2]*z[7];
    z[6]=z[6] + z[8] + z[7];

    r += n<T>(1,6)*z[6];
 
    return r;
}

template double qg_2lha_r1679(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1679(const std::array<dd_real,30>&);
#endif
