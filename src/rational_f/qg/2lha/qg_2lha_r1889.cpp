#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1889(const std::array<T,30>& k) {
  T z[30];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[9];
    z[6]=k[8];
    z[7]=k[7];
    z[8]=k[14];
    z[9]=k[11];
    z[10]=n<T>(1,2)*z[4];
    z[11]=n<T>(9,2) - z[4];
    z[11]=z[4]*z[11];
    z[11]= - n<T>(47,6) + z[11];
    z[11]=z[11]*z[10];
    z[12]=npow(z[4],2);
    z[13]=n<T>(1,2)*z[12];
    z[14]=n<T>(49,2) - 13*z[4];
    z[14]=z[14]*z[13];
    z[15]=npow(z[4],3);
    z[16]=z[15]*z[2];
    z[14]=z[14] - 5*z[16];
    z[17]=n<T>(1,3)*z[2];
    z[14]=z[14]*z[17];
    z[11]=z[11] + z[14];
    z[11]=z[2]*z[11];
    z[14]=n<T>(1,3)*z[4];
    z[18]=static_cast<T>(13)+ z[10];
    z[18]=z[18]*z[14];
    z[18]= - n<T>(3,2) + z[18];
    z[18]=z[4]*z[18];
    z[19]=static_cast<T>(4)- n<T>(19,6)*z[4];
    z[19]=z[19]*z[12];
    z[19]=z[19] - n<T>(7,3)*z[16];
    z[19]=z[2]*z[19];
    z[18]=z[18] + z[19];
    z[18]=z[2]*z[18];
    z[19]=n<T>(1,3) + z[10];
    z[19]=z[4]*z[19];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[4]*z[19];
    z[18]=z[19] + z[18];
    z[18]=z[2]*z[18];
    z[19]=n<T>(1,2)*z[15];
    z[18]= - z[19] + z[18];
    z[18]=z[3]*z[18];
    z[20]= - n<T>(29,12) + z[4];
    z[20]=z[4]*z[20];
    z[20]=n<T>(3,2) + z[20];
    z[11]=z[18] + n<T>(1,2)*z[20] + z[11];
    z[11]=z[3]*z[11];
    z[18]=static_cast<T>(2)- n<T>(5,6)*z[4];
    z[18]=z[18]*z[12];
    z[20]=static_cast<T>(3)- n<T>(19,3)*z[4];
    z[20]=z[20]*z[12];
    z[20]=z[20] - n<T>(11,3)*z[16];
    z[21]=n<T>(1,2)*z[2];
    z[20]=z[20]*z[21];
    z[18]=z[18] + z[20];
    z[18]=z[2]*z[18];
    z[20]=static_cast<T>(1)+ z[4];
    z[20]=z[20]*z[13];
    z[18]=z[20] + z[18];
    z[18]=z[3]*z[18];
    z[20]=n<T>(35,12) - z[4];
    z[20]=z[4]*z[20];
    z[20]= - static_cast<T>(3)+ z[20];
    z[20]=z[20]*z[10];
    z[22]=n<T>(47,4) - z[4];
    z[22]=z[22]*z[13];
    z[22]=z[22] + z[16];
    z[22]=z[22]*z[17];
    z[18]=z[18] + z[20] + z[22];
    z[18]=z[3]*z[18];
    z[20]=z[4] + n<T>(3,2);
    z[20]=z[20]*z[12];
    z[20]=z[20] + z[16];
    z[20]=n<T>(1,2)*z[20];
    z[22]= - static_cast<T>(1)- z[21];
    z[22]=z[22]*z[16];
    z[19]= - z[19] + z[22];
    z[19]=z[3]*z[19];
    z[19]=z[20] + z[19];
    z[22]=z[1]*z[3];
    z[19]=z[19]*z[22];
    z[18]=z[18] + z[19];
    z[18]=z[1]*z[18];
    z[19]=z[12]*z[2];
    z[23]= - z[10] + z[19];
    z[22]=z[12]*z[22];
    z[24]= - z[3]*z[4];
    z[22]=z[24] + z[22];
    z[22]=z[1]*z[22];
    z[24]= - z[1]*z[12];
    z[24]=n<T>(5,2)*z[4] + z[24];
    z[24]=z[1]*z[24];
    z[24]= - n<T>(3,2) + z[24];
    z[24]=z[5]*z[24];
    z[22]=z[24] - n<T>(1,2) + z[22];
    z[22]=z[6]*z[22];
    z[11]=n<T>(3,4)*z[22] + z[18] + n<T>(3,4)*z[23] + z[11];
    z[11]=z[1]*z[11];
    z[18]= - n<T>(15,2) + n<T>(7,3)*z[4];
    z[18]=z[18]*z[10];
    z[22]=static_cast<T>(4)- n<T>(3,2)*z[4];
    z[22]=z[22]*z[12];
    z[22]=z[22] - 2*z[16];
    z[22]=z[2]*z[22];
    z[18]=z[18] + z[22];
    z[18]=z[2]*z[18];
    z[22]= - n<T>(3,4) + z[4];
    z[22]=z[4]*z[22];
    z[22]=n<T>(35,12) + z[22];
    z[18]=n<T>(1,2)*z[22] + z[18];
    z[18]=z[2]*z[18];
    z[22]=z[4] - 1;
    z[23]=z[13]*z[22];
    z[24]=static_cast<T>(3)- z[10];
    z[24]=z[24]*z[12];
    z[24]=z[24] - z[16];
    z[24]=z[2]*z[24];
    z[25]=static_cast<T>(2)+ z[4];
    z[25]=z[4]*z[25];
    z[25]= - n<T>(5,2) + z[25];
    z[25]=z[4]*z[25];
    z[24]=z[25] + z[24];
    z[24]=z[2]*z[24];
    z[25]= - n<T>(7,3) - z[4];
    z[25]=z[4]*z[25];
    z[25]=static_cast<T>(1)+ z[25];
    z[24]=n<T>(1,2)*z[25] + z[24];
    z[25]=npow(z[2],2);
    z[24]=z[24]*z[25];
    z[24]=z[23] + z[24];
    z[24]=z[3]*z[24];
    z[18]=z[24] + z[23] + z[18];
    z[18]=z[3]*z[18];
    z[24]=z[21]*z[15];
    z[26]= - n<T>(1,2) + z[4];
    z[26]=z[26]*z[12];
    z[26]=z[26] - z[24];
    z[26]=z[26]*z[17];
    z[27]=n<T>(1,4) - n<T>(4,3)*z[4];
    z[27]=z[4]*z[27];
    z[26]=z[27] + z[26];
    z[26]=z[2]*z[26];
    z[27]=static_cast<T>(1)+ n<T>(17,2)*z[4];
    z[26]=n<T>(1,12)*z[27] + z[26];
    z[26]=z[26]*z[25];
    z[27]=n<T>(1,6)*z[2];
    z[28]=static_cast<T>(5)+ z[2];
    z[15]=z[27]*z[15]*z[28];
    z[27]= - static_cast<T>(1)+ n<T>(2,3)*z[4];
    z[27]=z[4]*z[27];
    z[27]= - n<T>(1,2) + z[27];
    z[27]=z[4]*z[27];
    z[15]=z[27] + z[15];
    z[15]=z[2]*z[15];
    z[27]=n<T>(1,3) - z[4];
    z[27]=z[27]*z[10];
    z[15]=z[15] + n<T>(1,3) + z[27];
    z[27]=npow(z[2],3);
    z[15]=z[3]*z[15]*z[27];
    z[15]=z[26] + z[15];
    z[15]=z[3]*z[15];
    z[26]= - static_cast<T>(2)+ z[10];
    z[26]=z[26]*z[14];
    z[26]=n<T>(1,2) + z[26];
    z[26]=z[4]*z[26];
    z[14]= - n<T>(1,2) + z[14];
    z[14]=z[14]*z[12];
    z[14]=z[14] + n<T>(1,6)*z[16];
    z[14]=z[2]*z[14];
    z[14]=z[26] + z[14];
    z[14]=z[2]*z[14];
    z[26]=z[10] - 1;
    z[26]=z[26]*z[4];
    z[26]=z[26] + n<T>(1,2);
    z[14]= - n<T>(1,3)*z[26] + z[14];
    z[14]=z[3]*z[14]*npow(z[2],4);
    z[28]= - n<T>(31,8) + z[4];
    z[28]=z[28]*z[12];
    z[28]=z[28] + z[16];
    z[28]=z[2]*z[28];
    z[29]=static_cast<T>(19)- n<T>(23,2)*z[4];
    z[29]=z[4]*z[29];
    z[28]=n<T>(1,4)*z[29] + z[28];
    z[28]=z[28]*z[17];
    z[28]=n<T>(5,8)*z[22] + z[28];
    z[27]=z[28]*z[27];
    z[14]=z[27] + z[14];
    z[14]=z[3]*z[14];
    z[27]= - n<T>(19,4)*z[12] + z[16];
    z[27]=z[2]*z[27];
    z[27]=n<T>(13,2)*z[4] + z[27];
    z[27]=z[2]*z[27];
    z[27]= - n<T>(11,4) + z[27];
    z[25]=z[27]*z[25];
    z[14]=n<T>(1,6)*z[25] + z[14];
    z[14]=z[7]*z[14];
    z[25]=n<T>(5,4)*z[12] - z[16];
    z[25]=z[25]*z[17];
    z[25]= - n<T>(3,8)*z[4] + z[25];
    z[25]=z[2]*z[25];
    z[25]=n<T>(7,24) + z[25];
    z[25]=z[2]*z[25];
    z[14]=z[14] + z[25] + z[15];
    z[14]=z[7]*z[14];
    z[15]=n<T>(1,12) + z[4];
    z[15]=z[4]*z[15];
    z[15]=n<T>(9,4) + z[15];
    z[15]=z[15]*z[10];
    z[25]= - n<T>(11,4) + z[4];
    z[13]=z[25]*z[13];
    z[13]=z[13] - z[16];
    z[13]=z[13]*z[17];
    z[20]= - z[1]*z[20];
    z[13]=z[20] + z[15] + z[13];
    z[13]=z[1]*z[13];
    z[15]=n<T>(7,4) + 2*z[4];
    z[15]=z[15]*z[12];
    z[15]=z[15] + z[24];
    z[15]=z[15]*z[17];
    z[17]=n<T>(11,6) + z[4];
    z[17]=z[4]*z[17];
    z[17]=n<T>(7,6) + z[17];
    z[17]=z[17]*z[10];
    z[15]=z[17] + z[15];
    z[15]=z[2]*z[15];
    z[17]=n<T>(25,12) - z[4];
    z[10]=z[17]*z[10];
    z[10]=z[13] + z[10] + z[15];
    z[10]=z[1]*z[10];
    z[13]= - n<T>(13,3) + z[4];
    z[13]=z[4]*z[13];
    z[13]=z[13] + n<T>(7,3)*z[19];
    z[13]=z[2]*z[13];
    z[15]= - n<T>(9,4) - z[4];
    z[15]=z[4]*z[15];
    z[13]=z[15] + n<T>(1,4)*z[13];
    z[13]=z[2]*z[13];
    z[13]= - n<T>(7,6) + z[13];
    z[10]=n<T>(1,2)*z[13] + z[10];
    z[10]=z[5]*z[10];
    z[13]= - z[21]*z[22]*z[12];
    z[15]= - z[4]*z[26];
    z[13]=z[15] + z[13];
    z[13]=z[3]*z[13];
    z[15]=n<T>(3,2) - z[4];
    z[15]=z[4]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[4]*z[15];
    z[13]=z[13] + z[15] - z[24];
    z[13]=z[3]*z[13];
    z[13]= - z[23] + z[13];
    z[13]=z[9]*z[13];
    z[12]=n<T>(47,12)*z[12] - z[16];
    z[12]=z[2]*z[12];
    z[12]= - n<T>(11,3)*z[4] + z[12];
    z[12]=z[2]*z[12];
    z[12]=n<T>(3,4) + z[12];
    z[15]= - z[7] - z[5];
    z[16]=z[2]*z[4];
    z[16]= - static_cast<T>(13)+ 7*z[16];
    z[15]=z[8]*z[2]*z[16]*z[15];

    r += z[10] + z[11] + n<T>(1,2)*z[12] + z[13] + z[14] + n<T>(1,24)*z[15] + 
      z[18];
 
    return r;
}

template double qg_2lha_r1889(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1889(const std::array<dd_real,30>&);
#endif
