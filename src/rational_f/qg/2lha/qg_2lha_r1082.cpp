#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r1082(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[6];
    z[5]=k[3];
    z[6]=k[9];
    z[7]=n<T>(1,2)*z[2];
    z[8]=z[7] - 1;
    z[9]=z[1] + 1;
    z[10]= - z[9]*z[8];
    z[11]=npow(z[6],2);
    z[12]=z[11]*z[5];
    z[13]= - static_cast<T>(1)- z[2];
    z[13]=z[13]*z[12];
    z[14]=z[2]*z[6];
    z[13]= - 3*z[14] + z[13];
    z[13]=z[5]*z[13];
    z[13]=n<T>(1,2)*z[13] + n<T>(1,2) - z[2];
    z[13]=z[5]*z[13];
    z[15]= - z[5] + z[9];
    z[15]=z[4]*z[15];
    z[9]= - 3*z[9] + z[15];
    z[15]=n<T>(1,2)*z[4];
    z[9]=z[9]*z[15];
    z[9]=z[9] + z[13] + z[10];
    z[9]=z[3]*z[9];
    z[7]=static_cast<T>(1)+ z[7];
    z[7]=z[7]*z[12];
    z[7]=z[14] + z[7];
    z[7]=z[5]*z[7];
    z[10]=z[5] - 1;
    z[13]= - n<T>(1,2)*z[1] + z[10];
    z[13]=z[4]*z[13];
    z[13]=z[13] + n<T>(5,2) + z[1];
    z[13]=z[4]*z[13];
    z[7]=z[9] + z[13] + z[7] + z[8];
    z[7]=z[3]*z[7];
    z[8]= - static_cast<T>(3)- z[2];
    z[8]=z[8]*z[12];
    z[8]= - z[14] + z[8];
    z[9]= - z[10]*z[15];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[4]*z[9];
    z[7]=z[7] + n<T>(1,4)*z[8] + z[9];
    z[7]=z[3]*z[7];
    z[7]=n<T>(1,4)*z[11] + z[7];

    r += z[7]*z[3];
 
    return r;
}

template double qg_2lha_r1082(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r1082(const std::array<dd_real,30>&);
#endif
