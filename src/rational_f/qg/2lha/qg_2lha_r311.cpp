#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r311(const std::array<T,30>& k) {
  T z[25];
  T r = 0;

    z[1]=k[3];
    z[2]=k[6];
    z[3]=k[9];
    z[4]=k[12];
    z[5]=k[10];
    z[6]=k[4];
    z[7]=k[13];
    z[8]=n<T>(1,2)*z[2];
    z[9]= - static_cast<T>(1)- 5*z[2];
    z[9]=z[9]*z[8];
    z[10]=npow(z[3],2);
    z[11]=n<T>(1,2)*z[10];
    z[12]=npow(z[2],2);
    z[13]=z[11] + z[12];
    z[13]=z[1]*z[13];
    z[14]= - n<T>(3,2) - z[3];
    z[14]=z[3]*z[14];
    z[9]=z[13] + z[14] + z[9];
    z[13]=n<T>(1,2)*z[1];
    z[9]=z[9]*z[13];
    z[14]=n<T>(1,2)*z[3];
    z[15]=static_cast<T>(3)+ z[14];
    z[15]=z[15]*z[14];
    z[16]= - n<T>(1,2) + z[2];
    z[16]=z[2]*z[16];
    z[9]=z[9] + z[16] - static_cast<T>(1)+ z[15];
    z[9]=z[1]*z[9];
    z[15]=n<T>(1,4)*z[2];
    z[16]=static_cast<T>(3)- z[2];
    z[16]=z[16]*z[15];
    z[9]=z[9] + z[16] + z[6] - n<T>(3,4)*z[3];
    z[16]=z[2] - z[3];
    z[17]=n<T>(1,4)*z[1];
    z[18]=z[16]*z[17];
    z[19]=z[15] - 1;
    z[20]= - z[19] + n<T>(1,4)*z[3];
    z[18]=z[18] + z[20];
    z[18]=z[1]*z[18];
    z[18]=z[18] + 3*z[6] + z[20];
    z[18]=z[18]*z[13];
    z[20]=n<T>(1,2) + z[6];
    z[20]=z[6]*z[20];
    z[18]=z[18] + z[20] + n<T>(1,8)*z[16];
    z[18]=z[5]*z[18];
    z[9]=n<T>(1,2)*z[9] + z[18];
    z[9]=z[5]*z[9];
    z[18]=z[2]*z[3];
    z[11]=z[11] - z[18];
    z[11]=z[11]*z[1];
    z[20]=z[3] - n<T>(1,2);
    z[20]=z[20]*z[3];
    z[11]=z[11] - z[20] + n<T>(5,2)*z[18];
    z[11]=z[13]*z[2]*z[11];
    z[21]=static_cast<T>(1)+ z[14];
    z[21]=z[21]*z[14];
    z[21]=z[21] - z[18];
    z[21]=z[2]*z[21];
    z[11]=z[11] + z[3] + z[21];
    z[11]=z[1]*z[11];
    z[21]= - 3*z[3] + z[18];
    z[15]=z[21]*z[15];
    z[11]=z[15] + z[11];
    z[15]=z[14]*z[2];
    z[21]=z[1]*z[15];
    z[21]= - z[18] + z[21];
    z[21]=z[1]*z[21];
    z[22]=z[13] - 1;
    z[23]= - z[1]*z[22];
    z[23]= - n<T>(1,2) + z[23];
    z[16]=z[5]*z[16]*z[23];
    z[16]=z[16] + z[15] + z[21];
    z[16]=z[4]*z[1]*z[16];
    z[9]=z[16] + n<T>(1,2)*z[11] + z[9];
    z[9]=z[4]*z[9];
    z[11]=static_cast<T>(1)- 3*z[2];
    z[8]=z[11]*z[8];
    z[11]=n<T>(3,2)*z[12];
    z[16]= - z[10] + z[11];
    z[16]=z[16]*z[13];
    z[8]=z[16] + z[20] + z[8];
    z[8]=z[1]*z[8];
    z[16]= - z[12]*z[22];
    z[21]=z[14] - 1;
    z[22]=z[21]*z[3];
    z[16]= - z[22] + z[16];
    z[16]=z[16]*z[13];
    z[19]= - z[2]*z[19];
    z[16]=z[16] + z[19] + n<T>(1,2) + z[22];
    z[16]=z[1]*z[16];
    z[19]=static_cast<T>(3)- z[22];
    z[16]=z[16] + n<T>(1,2)*z[19] - z[2];
    z[16]=z[5]*z[16];
    z[19]=z[3] - 1;
    z[23]=z[19]*z[3];
    z[24]= - static_cast<T>(1)+ n<T>(3,2)*z[2];
    z[24]=z[2]*z[24];
    z[24]=z[24] - static_cast<T>(1)- z[23];
    z[8]=z[16] + n<T>(1,2)*z[24] + z[8];
    z[8]=z[5]*z[8];
    z[16]=n<T>(3,2)*z[18];
    z[24]=z[19]*z[16];
    z[24]= - z[10] + z[24];
    z[24]=z[2]*z[24];
    z[18]= - npow(z[18],2)*z[13];
    z[18]=z[24] + z[18];
    z[13]=z[18]*z[13];
    z[16]= - z[21]*z[16];
    z[16]=z[20] + z[16];
    z[16]=z[2]*z[16];
    z[13]=z[16] + z[13];
    z[13]=z[1]*z[13];
    z[16]= - static_cast<T>(3)+ z[3];
    z[15]=z[16]*z[15];
    z[15]= - z[23] + z[15];
    z[15]=z[2]*z[15];
    z[15]=z[3] + z[15];
    z[8]=z[8] + n<T>(1,2)*z[15] + z[13];
    z[8]=n<T>(1,2)*z[8] + z[9];
    z[8]=z[4]*z[8];
    z[9]=z[7] - 1;
    z[13]=n<T>(1,2)*z[9];
    z[15]=z[6]*z[13];
    z[15]=z[15] + z[9];
    z[15]=z[6]*z[15];
    z[15]=z[13] + z[15];
    z[15]=z[2]*z[15];
    z[16]= - z[6]*z[7];
    z[9]=n<T>(3,4)*z[15] + z[16] - z[9];
    z[9]=z[2]*z[9];
    z[14]=z[19]*z[14];
    z[15]= - static_cast<T>(1)- n<T>(5,2)*z[6];
    z[15]=z[15]*z[12];
    z[11]= - z[1]*z[11];
    z[11]=z[11] + z[14] + z[15];
    z[11]=z[11]*z[17];
    z[13]=z[13] - z[22];
    z[9]=z[11] + n<T>(1,4)*z[13] + z[9];
    z[9]=z[5]*z[9];
    z[10]=z[1]*z[10];
    z[10]=z[23] - z[10];
    z[11]= - n<T>(1,2) + z[7];
    z[11]=z[6]*z[11];
    z[11]=z[11] + z[7];
    z[11]= - static_cast<T>(1)+ n<T>(3,4)*z[11];
    z[11]=z[2]*z[11];
    z[11]= - z[7] + z[11];
    z[11]=z[2]*z[11];
    z[9]=z[9] + z[11] - n<T>(1,8)*z[10];
    z[9]=z[5]*z[9];
    z[10]=z[7]*z[12];

    r += z[8] + z[9] + n<T>(3,8)*z[10];
 
    return r;
}

template double qg_2lha_r311(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r311(const std::array<dd_real,30>&);
#endif
