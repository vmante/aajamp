#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r63(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[9];
    z[6]=k[5];
    z[7]=k[6];
    z[8]= - static_cast<T>(1)+ z[7];
    z[9]=z[4]*z[6];
    z[8]=n<T>(1441,3)*z[8] + 179*z[9];
    z[9]=49*z[3];
    z[10]=z[1] - static_cast<T>(1)+ z[4];
    z[10]=z[10]*npow(z[1],2)*z[9];
    z[11]= - 899*z[1] + static_cast<T>(899)- 703*z[4];
    z[11]=z[1]*z[11];
    z[10]=n<T>(1,4)*z[11] + z[10];
    z[10]=z[3]*z[10];
    z[11]=n<T>(929,2)*z[1] - n<T>(929,2) + 113*z[4];
    z[10]=n<T>(1,2)*z[11] + z[10];
    z[10]=z[3]*z[10];
    z[8]=n<T>(1,16)*z[8] + n<T>(1,3)*z[10];
    z[8]=z[2]*z[8];
    z[10]=n<T>(703,6)*z[4] + 49*z[1];
    z[11]= - z[4] - n<T>(1,2)*z[1];
    z[11]=z[3]*z[1]*z[11];
    z[10]=n<T>(1,4)*z[10] + n<T>(49,3)*z[11];
    z[10]=z[3]*z[10];
    z[10]= - n<T>(159,16) + z[10];
    z[10]=z[3]*z[10];
    z[8]=n<T>(1,2)*z[8] + n<T>(179,32)*z[6] + z[10];
    z[8]=z[2]*z[8];
    z[9]= - npow(z[4],2)*z[9];
    z[9]= - n<T>(409,4)*z[4] + z[9];
    z[9]=z[3]*z[9];
    z[9]=n<T>(281,8) + z[9];
    z[9]=z[3]*z[5]*z[9];
    z[8]=n<T>(1,6)*z[9] + z[8];

    r += n<T>(1,320)*z[8];
 
    return r;
}

template double qg_2lha_r63(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r63(const std::array<dd_real,30>&);
#endif
