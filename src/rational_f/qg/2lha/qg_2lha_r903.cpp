#include "qg_2lha_rf_decl.hpp"

template<class T>
T qg_2lha_r903(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[3];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[10];
    z[10]=n<T>(1,3)*z[5];
    z[11]=92*z[5];
    z[12]= - static_cast<T>(59)+ z[11];
    z[12]=z[12]*z[10];
    z[13]=8*z[2];
    z[14]=n<T>(11,3)*z[7];
    z[15]=2*z[1];
    z[16]= - static_cast<T>(1)+ z[15];
    z[12]= - z[13] + z[12] + 4*z[16] + z[14];
    z[16]=2*z[2];
    z[12]=z[12]*z[16];
    z[17]= - static_cast<T>(1)- z[1];
    z[18]=n<T>(531,2) - 230*z[5];
    z[18]=z[5]*z[18];
    z[12]=z[12] + z[18] + 8*z[17] - n<T>(15,2)*z[7];
    z[12]=z[2]*z[12];
    z[17]= - static_cast<T>(1)+ 2*z[7];
    z[17]=z[17]*z[14];
    z[18]=npow(z[5],2);
    z[19]=n<T>(23,3)*z[18];
    z[20]=npow(z[7],2);
    z[21]=n<T>(11,3)*z[20];
    z[22]=z[19] + static_cast<T>(4)+ z[21];
    z[22]=z[2]*z[22];
    z[23]=46*z[5];
    z[24]=static_cast<T>(19)- z[23];
    z[24]=z[5]*z[24];
    z[17]=z[22] + n<T>(5,3)*z[24] + static_cast<T>(4)+ z[17];
    z[17]=z[17]*z[16];
    z[22]=11*z[20];
    z[18]=23*z[18];
    z[24]=z[22] + z[18];
    z[25]=z[2]*z[24];
    z[26]=23*z[5];
    z[27]= - static_cast<T>(6)+ z[26];
    z[27]=z[5]*z[27];
    z[25]= - n<T>(1,3)*z[25] - z[21] + z[27];
    z[27]=n<T>(1,3)*z[4];
    z[24]=z[24]*z[27];
    z[24]=2*z[25] + z[24];
    z[24]=z[4]*z[24];
    z[25]= - n<T>(1697,4) + 575*z[5];
    z[25]=z[25]*z[10];
    z[28]=static_cast<T>(353)- z[7];
    z[28]=z[7]*z[28];
    z[17]=2*z[24] + z[17] + z[25] + static_cast<T>(24)+ n<T>(1,12)*z[28];
    z[17]=z[4]*z[17];
    z[24]=n<T>(23,3)*z[5];
    z[25]= - n<T>(59,2) + 22*z[5];
    z[25]=z[25]*z[24];
    z[28]=z[1] - 4;
    z[29]=n<T>(1,6)*z[7];
    z[12]=z[17] + z[12] + z[25] - 8*z[28] + z[29];
    z[12]=z[4]*z[12];
    z[17]=static_cast<T>(4)+ z[7];
    z[17]=z[17]*z[14];
    z[25]=z[7] - 2;
    z[30]=z[25]*z[14];
    z[31]=n<T>(10,3) + z[26];
    z[31]=z[5]*z[31];
    z[30]=z[30] + z[31];
    z[30]=z[30]*z[16];
    z[31]=11*z[7];
    z[32]=static_cast<T>(1)- z[7];
    z[32]=z[32]*z[31];
    z[33]= - static_cast<T>(11)- z[26];
    z[33]=z[5]*z[33];
    z[32]=z[32] + z[33];
    z[33]=n<T>(2,3)*z[4];
    z[32]=z[32]*z[33];
    z[34]=69*z[5];
    z[35]=n<T>(40,3) - z[34];
    z[35]=z[5]*z[35];
    z[17]=z[32] + z[30] + z[17] + z[35];
    z[17]=z[4]*z[17];
    z[30]=n<T>(13,3) - z[26];
    z[30]=z[5]*z[30];
    z[30]=z[30] + static_cast<T>(4)+ z[14];
    z[30]=z[30]*z[16];
    z[32]= - n<T>(347,3) + 161*z[5];
    z[32]=z[5]*z[32];
    z[14]=z[30] + z[32] + static_cast<T>(28)+ z[14];
    z[14]=z[2]*z[14];
    z[30]=115*z[5];
    z[32]=static_cast<T>(107)- z[30];
    z[32]=z[5]*z[32];
    z[14]=z[17] + z[14] + z[32] - static_cast<T>(36)- z[31];
    z[14]=z[4]*z[14];
    z[17]=z[1] - 2;
    z[32]=4*z[17];
    z[24]= - static_cast<T>(4)+ z[24];
    z[24]=z[5]*z[24];
    z[24]=z[32] + z[24];
    z[24]=z[24]*z[16];
    z[30]=n<T>(482,3) - z[30];
    z[30]=z[5]*z[30];
    z[24]=z[24] + 4*z[28] + z[30];
    z[24]=z[2]*z[24];
    z[28]=static_cast<T>(5)- z[15];
    z[30]= - n<T>(223,3) + z[23];
    z[30]=z[5]*z[30];
    z[28]=4*z[28] + z[30];
    z[24]=4*z[28] + z[24];
    z[24]=z[2]*z[24];
    z[28]=static_cast<T>(434)- 253*z[5];
    z[28]=z[28]*z[10];
    z[30]= - static_cast<T>(12)+ 5*z[1];
    z[14]=z[14] + z[24] + 4*z[30] + z[28];
    z[14]=z[4]*z[14];
    z[24]=z[26] - 47;
    z[28]=z[24]*z[5];
    z[28]= - z[28] + 12*z[17];
    z[24]=z[24]*z[10];
    z[24]=z[24] - z[32];
    z[30]= - z[2]*z[24];
    z[30]=z[30] - z[28];
    z[30]=z[2]*z[30];
    z[28]=z[30] + z[28];
    z[28]=z[2]*z[28];
    z[24]=z[28] + z[24];
    z[28]=z[26] - 1;
    z[28]=z[28]*z[5];
    z[28]=z[28] - z[31];
    z[30]=z[2] - 1;
    z[28]= - z[28]*z[30];
    z[25]=z[25]*z[31];
    z[31]=static_cast<T>(22)+ z[26];
    z[31]=z[5]*z[31];
    z[25]=z[25] + z[31];
    z[25]=z[4]*z[25];
    z[25]=4*z[28] + z[25];
    z[25]=z[25]*z[27];
    z[28]= - static_cast<T>(2)+ z[2];
    z[28]=z[2]*z[28];
    z[28]=z[28] + 1;
    z[31]=z[26] - 24;
    z[31]=z[31]*z[5];
    z[31]=z[31] + 12;
    z[28]=z[31]*z[28];
    z[25]=2*z[28] + z[25];
    z[25]=z[4]*z[25];
    z[24]=4*z[24] + z[25];
    z[24]=z[4]*z[24];
    z[25]=z[17]*z[1];
    z[28]=z[26] - 70;
    z[31]=z[5]*z[28];
    z[25]=z[31] + static_cast<T>(47)+ 12*z[25];
    z[28]=z[28]*z[10];
    z[31]=z[32]*z[1];
    z[28]=z[28] + z[31] + n<T>(47,3);
    z[31]= - static_cast<T>(4)+ z[2];
    z[31]=z[2]*z[28]*z[31];
    z[25]=2*z[25] + z[31];
    z[25]=z[2]*z[25];
    z[25]= - 4*z[28] + z[25];
    z[25]=z[2]*z[25];
    z[24]=z[24] + z[25] + z[28];
    z[24]=z[3]*z[24];
    z[25]=static_cast<T>(4)+ z[1];
    z[25]=z[25]*z[15];
    z[25]=n<T>(5,3) + z[25];
    z[28]=static_cast<T>(1)- z[1];
    z[28]=z[28]*z[13];
    z[31]=z[26] - n<T>(175,3);
    z[31]=z[31]*z[5];
    z[25]=z[28] + 2*z[25] + z[31];
    z[25]=z[2]*z[25];
    z[28]=z[34] - 175;
    z[28]=z[28]*z[5];
    z[32]=npow(z[1],2);
    z[32]= - static_cast<T>(29)- 6*z[32];
    z[25]=z[25] + 2*z[32] - z[28];
    z[25]=z[2]*z[25];
    z[32]= - static_cast<T>(4)+ 3*z[1];
    z[32]=z[32]*z[15];
    z[32]=static_cast<T>(37)+ z[32];
    z[25]=z[25] + 2*z[32] + z[28];
    z[25]=z[2]*z[25];
    z[17]= - z[17]*z[15];
    z[17]= - n<T>(41,3) + z[17];
    z[14]=z[24] + z[14] + z[25] + 2*z[17] - z[31];
    z[14]=z[3]*z[14];
    z[15]= - z[15] + z[2];
    z[15]=z[15]*z[13];
    z[17]= - n<T>(435,4) + n<T>(161,3)*z[5];
    z[17]=z[17]*z[5];
    z[15]=z[15] + z[17] + static_cast<T>(7)+ 32*z[1];
    z[15]=z[2]*z[15];
    z[24]= - static_cast<T>(23)- 8*z[1];
    z[25]=n<T>(435,2) - n<T>(322,3)*z[5];
    z[25]=z[5]*z[25];
    z[15]=z[15] + 2*z[24] + z[25];
    z[15]=z[2]*z[15];
    z[12]=2*z[14] + z[12] + z[15] + static_cast<T>(31)+ z[17];
    z[12]=z[3]*z[12];
    z[14]=z[7] - n<T>(89,2);
    z[15]=z[7]*z[14];
    z[17]=n<T>(281,4) - 184*z[5];
    z[17]=z[5]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[17]=static_cast<T>(2)- z[21];
    z[17]=2*z[17] + z[19];
    z[17]=z[17]*z[16];
    z[19]=z[22] - z[18];
    z[19]=z[19]*z[33];
    z[15]=z[19] + n<T>(1,3)*z[15] + z[17];
    z[15]=z[4]*z[15];
    z[14]= - z[14]*z[29];
    z[17]= - static_cast<T>(8)+ z[21];
    z[16]=z[17]*z[16];
    z[17]= - n<T>(281,12) + z[23];
    z[17]=z[5]*z[17];
    z[14]=z[16] + z[17] + static_cast<T>(8)+ z[14];
    z[14]=z[2]*z[14];
    z[16]=n<T>(559,8) - n<T>(230,3)*z[5];
    z[16]=z[5]*z[16];
    z[14]=z[15] + z[14] - n<T>(5,24)*z[7] + z[16];
    z[14]=z[4]*z[14];
    z[13]=z[30]*z[13];
    z[11]=z[11] - n<T>(1115,8);
    z[11]=z[11]*z[5];
    z[11]=z[11] + 23;
    z[11]=n<T>(1,3)*z[11];
    z[13]=z[11] + z[13];
    z[13]=z[2]*z[13];
    z[11]=z[12] + z[14] - z[11] + z[13];
    z[11]=z[3]*z[11];
    z[12]=163*z[9];
    z[13]= - n<T>(167,2) - z[12];
    z[13]=z[7]*z[13];
    z[13]=z[12] + z[13];
    z[13]=z[7]*z[13];
    z[14]=z[20]*z[9];
    z[15]=z[14]*z[2];
    z[14]=z[4]*z[14];
    z[13]= - n<T>(163,16)*z[14] + n<T>(163,8)*z[15] + n<T>(1,8)*z[13] + z[18];
    z[13]=z[13]*z[27];
    z[14]=static_cast<T>(55)+ z[12];
    z[16]=z[9] - z[6];
    z[16]= - static_cast<T>(1)- n<T>(1,2)*z[16];
    z[16]=z[7]*z[16];
    z[14]=n<T>(1,2)*z[14] + n<T>(163,3)*z[16];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(163,6)*z[9] + z[14];
    z[16]= - n<T>(185,8) + z[23];
    z[10]=z[16]*z[10];
    z[16]=static_cast<T>(167)+ z[12];
    z[16]=z[7]*z[16];
    z[12]= - z[12] + z[16];
    z[12]=z[7]*z[12];
    z[12]=z[12] - n<T>(163,2)*z[15];
    z[12]=z[2]*z[12];
    z[10]=z[13] + n<T>(1,24)*z[12] + n<T>(1,8)*z[14] + z[10];
    z[10]=z[4]*z[10];
    z[12]=163*z[8];
    z[13]=z[6]*z[12];
    z[13]= - static_cast<T>(167)+ z[13];
    z[13]=z[7]*z[13];
    z[12]= - z[12] + z[13];
    z[12]=z[2]*z[12];
    z[13]=z[9] - z[8];
    z[14]=static_cast<T>(1)- z[6];
    z[14]=z[7]*z[14];
    z[12]=n<T>(1,2)*z[12] + 163*z[14] - static_cast<T>(1)- n<T>(163,2)*z[13];
    z[12]=z[2]*z[7]*z[12];
    z[13]=static_cast<T>(1)+ z[9];
    z[13]=z[7]*z[13];
    z[13]= - z[9] + z[13];
    z[14]= - n<T>(185,8) + z[26];
    z[14]=z[5]*z[14];
    z[12]=n<T>(1,8)*z[12] + n<T>(163,16)*z[13] + z[14];

    r += z[10] + z[11] + n<T>(1,3)*z[12];
 
    return r;
}

template double qg_2lha_r903(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qg_2lha_r903(const std::array<dd_real,30>&);
#endif
