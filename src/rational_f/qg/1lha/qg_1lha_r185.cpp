#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r185(const std::array<T,31>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[3];
    z[5]=k[15];
    z[6]=k[5];
    z[7]=k[19];
    z[8]=z[7]*z[2]*z[5];
    z[9]=z[6]*z[7];
    z[10]= - static_cast<T>(3)- z[9];
    z[10]=z[5]*z[10];
    z[10]=z[10] - z[8];
    z[10]=z[2]*z[10];
    z[11]=z[4] + z[6];
    z[12]=z[5]*z[11];
    z[9]=z[10] - 3*z[12] - static_cast<T>(2)+ z[9];
    z[9]=z[2]*z[9];
    z[10]=z[5]*z[4];
    z[12]= - 3*z[6] - z[4];
    z[12]=z[12]*z[10];
    z[9]=z[9] + z[12] - z[1] - z[11];
    z[9]=z[2]*z[9];
    z[11]= - z[5]*npow(z[4],2);
    z[11]= - z[4] + z[11];
    z[11]=z[6]*z[11];
    z[9]=z[9] + z[11];
    z[9]=z[3]*z[9];
    z[8]= - 2*z[5] - z[8];
    z[8]=z[2]*z[8];
    z[8]=z[8] - static_cast<T>(1)- z[10];
    z[8]=z[2]*z[8];
    z[8]=z[8] + z[9];

    r += z[8]*z[3];
 
    return r;
}

template double qg_1lha_r185(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r185(const std::array<dd_real,31>&);
#endif
