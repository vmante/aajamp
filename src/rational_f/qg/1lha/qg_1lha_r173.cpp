#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r173(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[9];
    z[4]=k[5];
    z[5]=k[11];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=npow(z[3],2);
    z[9]=z[8]*z[2];
    z[10]=z[9] - 2*z[3];
    z[10]=z[10]*z[2];
    z[10]=z[10] + 1;
    z[11]=2*z[5];
    z[10]=z[10]*z[11];
    z[11]=2*z[2];
    z[11]=z[11]*z[8];
    z[12]=z[11] - 3*z[3];
    z[13]=z[10] - z[12];
    z[13]=z[4]*z[13];
    z[11]=5*z[3] - z[11];
    z[11]=z[2]*z[11];
    z[11]= - static_cast<T>(4)+ z[11];
    z[11]=z[2]*z[11];
    z[11]=z[7] + z[11];
    z[11]=z[5]*z[11];
    z[12]=z[2]*z[12];
    z[9]= - z[3] + z[9];
    z[9]=3*z[9] - z[10];
    z[9]=z[6]*z[9];
    z[10]=z[4] - z[2];
    z[8]=z[8]*z[10];
    z[8]=z[3] + z[8];
    z[8]=z[1]*z[8];

    r += static_cast<T>(1)+ 3*z[8] + z[9] + z[11] + z[12] + z[13];
 
    return r;
}

template double qg_1lha_r173(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r173(const std::array<dd_real,31>&);
#endif
