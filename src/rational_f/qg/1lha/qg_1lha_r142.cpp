#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r142(const std::array<T,31>& k) {
  T z[13];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[25];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[6];
    z[7]=z[1] - z[4];
    z[8]=z[3]*z[7];
    z[8]= - static_cast<T>(1)+ z[8];
    z[9]=npow(z[1],2);
    z[8]=z[9]*z[8];
    z[10]=z[5]*z[3];
    z[10]=z[10] + 1;
    z[11]=z[4]*z[6];
    z[12]=static_cast<T>(1)- z[11];
    z[12]=z[4]*z[12];
    z[12]=z[12] + 3*z[1];
    z[12]=z[3]*z[12];
    z[12]=z[12] - z[10];
    z[12]=z[5]*z[12];
    z[9]=z[3]*z[9];
    z[9]=z[12] - 3*z[9] + 2*z[1];
    z[9]=z[5]*z[9];
    z[8]=z[9] + z[8];
    z[8]=z[2]*z[8];
    z[9]=z[3]*z[1];
    z[7]=z[7]*z[9];
    z[12]= - static_cast<T>(1)+ 2*z[11];
    z[12]= - z[12]*z[10];
    z[9]= - 2*z[9] + z[12];
    z[9]=z[5]*z[9];
    z[7]=z[8] + z[9] - z[1] + z[7];
    z[7]=z[2]*z[7];
    z[8]= - z[1]*z[6];
    z[9]= - z[6]*z[10];
    z[9]=z[3] + z[9];
    z[9]=z[5]*z[9];

    r += z[7] + z[8] + z[9] + z[11];
 
    return r;
}

template double qg_1lha_r142(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r142(const std::array<dd_real,31>&);
#endif
