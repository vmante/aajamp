#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r30(const std::array<T,31>& k) {
  T z[26];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[8];
    z[4]=k[11];
    z[5]=k[16];
    z[6]=k[4];
    z[7]=k[29];
    z[8]=k[14];
    z[9]=k[3];
    z[10]=z[6]*z[9];
    z[11]=z[10]*z[4];
    z[12]=2*z[9];
    z[13]=z[11] - z[12] + z[6];
    z[14]=2*z[4];
    z[13]=z[13]*z[14];
    z[15]=z[6]*z[5];
    z[16]=z[15]*z[9];
    z[11]= - z[16] + z[11];
    z[17]=4*z[8];
    z[11]=z[11]*z[17];
    z[18]=3*z[15];
    z[19]=4*z[5];
    z[20]=z[9]*z[19];
    z[11]=z[11] + z[13] + z[20] - z[18];
    z[11]=z[8]*z[11];
    z[13]=z[1]*z[7];
    z[20]=3*z[13];
    z[21]=z[6]*z[7];
    z[22]=2*z[5];
    z[23]= - z[1]*z[22];
    z[24]=z[1] - z[6];
    z[24]=z[4]*z[24];
    z[23]=z[24] + z[21] - z[20] + z[23];
    z[23]=z[3]*z[23];
    z[24]=z[21] + 1;
    z[24]=z[24]*z[4];
    z[25]=z[7] + z[5];
    z[18]= - static_cast<T>(5)+ z[18];
    z[18]=z[8]*z[18];
    z[18]=z[23] + z[18] + 2*z[25] - z[24];
    z[18]=z[3]*z[18];
    z[20]= - z[15] + static_cast<T>(4)+ z[20];
    z[20]=z[8]*z[20];
    z[20]=z[20] + z[24] - z[7] - z[22];
    z[20]=z[3]*z[20];
    z[23]=static_cast<T>(1)- z[15];
    z[17]=z[23]*z[17];
    z[17]=z[17] - z[7] - z[19];
    z[17]=z[8]*z[17];
    z[19]=z[14]*z[7];
    z[17]=z[20] + z[19] + z[17];
    z[17]=z[3]*z[17];
    z[13]=z[15] - static_cast<T>(2)- z[13];
    z[13]=z[8]*z[13];
    z[13]=z[13] - z[7] + z[22];
    z[13]=z[8]*z[13];
    z[20]= - z[4]*z[7];
    z[13]=z[20] + z[13];
    z[13]=z[3]*z[13];
    z[20]=z[7]*npow(z[8],2);
    z[13]= - z[20] + z[13];
    z[13]=z[3]*z[13];
    z[20]=z[2]*npow(z[3],2)*z[20];
    z[13]=z[13] + z[20];
    z[13]=z[2]*z[13];
    z[13]=z[17] + z[13];
    z[13]=z[2]*z[13];
    z[17]=z[8]*z[15];
    z[17]=z[4] + 2*z[17];
    z[17]=z[8]*z[17];
    z[13]=z[13] + z[18] - z[19] + 3*z[17];
    z[13]=z[2]*z[13];
    z[17]=npow(z[1],2);
    z[18]= - z[5]*npow(z[1],3);
    z[19]= - z[6]*z[1];
    z[18]=z[19] + 2*z[17] + z[18];
    z[18]=z[4]*z[18];
    z[19]=z[1]*z[21];
    z[18]=z[19] + z[18];
    z[18]=z[3]*z[18];
    z[19]=z[5]*z[1];
    z[20]=z[17]*z[5];
    z[21]=z[1] - z[20];
    z[21]=z[4]*z[21];
    z[18]=z[18] + z[19] + z[21];
    z[18]=z[3]*z[18];
    z[19]=z[5] - z[4];
    z[11]=z[13] + z[18] + 2*z[19] + z[11];
    z[11]=z[2]*z[11];
    z[13]=npow(z[9],2);
    z[18]= - z[13] + z[10];
    z[14]=z[6]*z[18]*z[14];
    z[14]=z[14] + 2*z[13] - z[10];
    z[14]=z[4]*z[14];
    z[15]=z[13]*z[15];
    z[18]= - z[6]*z[13];
    z[19]=z[4]*npow(z[10],2);
    z[18]=z[18] + z[19];
    z[18]=z[4]*z[18];
    z[15]=z[15] + z[18];
    z[15]=z[8]*z[15];
    z[13]= - z[13]*z[22];
    z[13]=z[15] + z[14] + z[13] + z[16];
    z[13]=z[8]*z[13];
    z[10]=z[17] - 2*z[10];
    z[10]=z[4]*z[10];
    z[12]=z[12] - z[1];
    z[10]=z[10] - z[20] + z[12];
    z[10]=z[4]*z[10];
    z[12]= - z[5]*z[12];

    r += z[10] + z[11] + z[12] + z[13];
 
    return r;
}

template double qg_1lha_r30(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r30(const std::array<dd_real,31>&);
#endif
