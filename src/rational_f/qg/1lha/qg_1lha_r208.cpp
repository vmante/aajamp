#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r208(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[2];
    z[8]=k[11];
    z[9]=k[15];
    z[10]=k[14];
    z[11]=z[6]*z[9];
    z[12]=3*z[11];
    z[13]=z[5]*z[9];
    z[13]=z[13] + static_cast<T>(1)+ z[12];
    z[13]=z[5]*z[13];
    z[12]=static_cast<T>(2)+ z[12];
    z[12]=z[6]*z[12];
    z[12]=z[13] + z[1] + z[12];
    z[12]=z[5]*z[12];
    z[13]= - z[2]*z[3];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[13]*npow(z[1],2);
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[6]*z[11];
    z[11]=z[1] + z[11];
    z[11]=z[6]*z[11];
    z[11]=z[12] + z[13] + z[11];
    z[11]=z[11]*npow(z[4],2);
    z[12]=z[2]*z[10];
    z[13]=z[5]*z[10];
    z[13]=z[13] + static_cast<T>(1)- 2*z[12];
    z[13]=z[5]*z[13];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[2]*z[12];
    z[12]=z[13] + z[6] - z[7] + z[12];
    z[12]=z[8]*z[12];

    r += static_cast<T>(1)+ z[11] + z[12];
 
    return r;
}

template double qg_1lha_r208(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r208(const std::array<dd_real,31>&);
#endif
