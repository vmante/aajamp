#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r189(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[20];
    z[5]=k[3];
    z[6]=k[15];
    z[7]=npow(z[5],2);
    z[8]=z[5]*z[4];
    z[9]= - static_cast<T>(3)- z[8];
    z[9]=z[6]*z[9]*z[7];
    z[10]=3*z[8];
    z[11]= - static_cast<T>(4)- z[10];
    z[11]=z[5]*z[11];
    z[9]=z[11] + z[9];
    z[9]=z[6]*z[9];
    z[11]= - z[4]*z[1];
    z[9]=z[9] - 2*z[8] - static_cast<T>(1)+ z[11];
    z[9]=z[3]*z[9];
    z[11]=3*z[5];
    z[12]= - static_cast<T>(1)- z[8];
    z[11]=z[6]*z[12]*z[11];
    z[10]=z[10] + 1;
    z[11]= - 2*z[10] + z[11];
    z[11]=z[6]*z[11];
    z[10]= - z[6]*z[10];
    z[10]=z[10] - 3*z[4];
    z[10]=z[6]*z[10];
    z[12]=npow(z[6],2);
    z[13]= - z[2]*z[4]*z[12];
    z[10]=z[10] + z[13];
    z[10]=z[2]*z[10];
    z[13]=2*z[4];
    z[10]=z[10] - z[13] + z[11];
    z[10]=z[3]*z[10];
    z[11]=z[12]*z[8];
    z[10]=z[11] + z[10];
    z[10]=z[2]*z[10];
    z[11]=z[6]*z[4];
    z[12]=z[7]*z[11];
    z[12]=z[8] + z[12];
    z[12]=z[6]*z[12];
    z[9]=z[10] + 2*z[12] + z[9];
    z[9]=z[2]*z[9];
    z[10]=z[7]*z[13];
    z[12]=npow(z[5],3);
    z[11]=z[12]*z[11];
    z[10]=z[10] + z[11];
    z[10]=z[6]*z[10];
    z[11]= - z[6]*z[12];
    z[7]= - 2*z[7] + z[11];
    z[7]=z[6]*z[7];
    z[7]= - z[5] + z[7];
    z[7]=z[3]*z[7];

    r += z[7] + z[8] + z[9] + z[10];
 
    return r;
}

template double qg_1lha_r189(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r189(const std::array<dd_real,31>&);
#endif
