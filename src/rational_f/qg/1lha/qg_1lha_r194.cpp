#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r194(const std::array<T,31>& k) {
  T z[10];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[20];
    z[6]=npow(z[1],3);
    z[7]=z[6]*z[3];
    z[8]=npow(z[1],2);
    z[7]=z[7] - z[8];
    z[9]=z[5] - z[4];
    z[7]=z[3]*z[7]*z[9];
    z[6]= - z[4]*z[6];
    z[6]= - z[8] + z[6];
    z[6]=z[2]*z[5]*z[6]*npow(z[3],2);

    r += z[6] + z[7];
 
    return r;
}

template double qg_1lha_r194(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r194(const std::array<dd_real,31>&);
#endif
