#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r125(const std::array<T,31>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[10];
    z[4]=k[13];
    z[5]=k[7];
    z[6]=k[4];
    z[7]=z[4]*z[1];
    z[8]=z[5]*z[1];
    z[9]=z[7] - z[8];
    z[10]=2*z[2];
    z[11]=npow(z[5],2);
    z[12]= - z[11]*z[10];
    z[13]=z[5] - z[4];
    z[12]=z[12] + z[13];
    z[12]=z[6]*z[12];
    z[8]=2*z[8];
    z[14]=static_cast<T>(3)+ z[8];
    z[14]=z[5]*z[14];
    z[14]= - 3*z[4] + z[14];
    z[14]=z[2]*z[14];
    z[12]=z[12] + z[14] + z[9];
    z[12]=z[6]*z[12];
    z[14]= - z[2]*z[13];
    z[9]=z[14] - z[9];
    z[9]=z[9]*z[10];
    z[14]=z[2]*z[11];
    z[13]=z[14] - z[13];
    z[13]=z[6]*z[2]*z[13];
    z[9]=z[9] + z[13];
    z[9]=z[6]*z[9];
    z[13]=z[2]*z[4];
    z[14]= - 2*z[7] + z[13];
    z[14]=z[2]*z[14];
    z[15]=npow(z[1],2);
    z[16]=z[4]*z[15];
    z[14]=z[16] + z[14];
    z[14]=z[2]*z[14];
    z[9]=z[14] + z[9];
    z[9]=z[3]*z[9];
    z[7]=z[7] - z[13];
    z[7]=z[7]*z[10];
    z[7]=z[9] + z[7] + z[12];
    z[7]=z[3]*z[7];
    z[8]= - static_cast<T>(1)- z[8];
    z[8]=z[5]*z[8];
    z[9]=z[6]*z[11];
    z[8]=z[9] + z[4] + z[8];
    z[8]=z[6]*z[8];
    z[9]=z[5]*z[15];
    z[9]=z[1] + z[9];
    z[9]=z[5]*z[9];

    r += z[7] + z[8] + z[9] + z[13];
 
    return r;
}

template double qg_1lha_r125(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r125(const std::array<dd_real,31>&);
#endif
