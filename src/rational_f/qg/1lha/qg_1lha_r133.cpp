#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r133(const std::array<T,31>& k) {
  T z[15];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[15];
    z[5]=k[5];
    z[6]=k[19];
    z[7]=z[2]*z[6];
    z[8]=z[5]*z[6];
    z[9]=z[8] + 1;
    z[10]= - z[7] - z[9];
    z[11]=2*z[4];
    z[10]=z[11]*z[10];
    z[10]= - z[6] + z[10];
    z[10]=z[2]*z[10];
    z[12]= - z[5]*z[11];
    z[10]=z[10] + z[12] - static_cast<T>(1)+ z[8];
    z[10]=z[2]*z[10];
    z[9]= - z[9]*z[11];
    z[11]=z[2]*z[4];
    z[12]= - z[6]*z[11];
    z[9]=z[9] + z[12];
    z[9]=z[2]*z[9];
    z[12]= - static_cast<T>(4)- z[8];
    z[12]=z[5]*z[12];
    z[12]= - z[1] + z[12];
    z[12]=z[4]*z[12];
    z[9]=z[9] + z[12] - static_cast<T>(1)+ 2*z[8];
    z[9]=z[2]*z[9];
    z[12]=npow(z[5],2);
    z[13]=z[6]*z[12];
    z[14]= - z[1] - z[5];
    z[14]=z[14]*z[4]*z[5];
    z[13]=z[13] + z[14];
    z[9]=2*z[13] + z[9];
    z[9]=z[2]*z[9];
    z[13]=z[6] - z[4];
    z[12]=z[12]*z[1]*z[13];
    z[9]=z[9] + z[12];
    z[9]=z[3]*z[9];
    z[8]=z[1]*z[8];
    z[8]=z[9] + z[8] + z[10];
    z[8]=z[3]*z[8];
    z[9]= - static_cast<T>(1)- z[11];
    z[7]=z[9]*z[7];

    r += z[7] + z[8];
 
    return r;
}

template double qg_1lha_r133(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r133(const std::array<dd_real,31>&);
#endif
