#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r135(const std::array<T,31>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[5];
    z[5]=k[7];
    z[6]=k[3];
    z[7]=k[15];
    z[8]=npow(z[6],2);
    z[9]= - 4*z[6] - z[4];
    z[9]=z[4]*z[9];
    z[9]= - z[8] + z[9];
    z[9]=z[7]*z[9];
    z[9]= - z[4] + z[9];
    z[9]=z[7]*z[9];
    z[10]=z[4] + z[6];
    z[11]=2*z[7];
    z[12]=z[10]*z[11];
    z[12]=z[12] + 1;
    z[12]=z[12]*z[7];
    z[13]=npow(z[7],2);
    z[14]=z[13]*z[2];
    z[15]= - z[12] - z[14];
    z[15]=z[2]*z[15];
    z[9]=z[9] + z[15];
    z[9]=z[2]*z[9];
    z[15]=z[4]*z[6];
    z[16]= - z[8] - z[15];
    z[16]=z[4]*z[16]*z[11];
    z[16]=z[8] + z[16];
    z[16]=z[7]*z[16];
    z[9]=z[9] + z[16] + z[1] + z[10];
    z[9]=z[2]*z[9];
    z[10]=z[6] - z[1];
    z[16]= - z[5]*z[10];
    z[17]=z[4]*z[5];
    z[16]=z[17] - static_cast<T>(1)+ z[16];
    z[16]=z[4]*z[16];
    z[10]=z[16] + z[10];
    z[10]=z[4]*z[10];
    z[16]= - z[7]*npow(z[4],2);
    z[16]=z[4] + z[16];
    z[8]=z[7]*z[8]*z[16];
    z[8]=z[9] + z[10] + z[8];
    z[8]=z[3]*z[8];
    z[9]=z[11]*z[15];
    z[10]=z[6] - z[9];
    z[10]=z[7]*z[10];
    z[11]= - z[12] - 2*z[14];
    z[11]=z[2]*z[11];
    z[10]=z[11] + static_cast<T>(1)+ z[10];
    z[10]=z[2]*z[10];
    z[11]= - z[6]*z[5];
    z[11]=z[11] + z[17];
    z[11]=z[4]*z[11];
    z[8]=z[8] + z[10] + z[9] + z[1] + z[11];
    z[8]=z[3]*z[8];
    z[9]= - z[13]*npow(z[2],2);

    r += static_cast<T>(1)+ z[8] + z[9];
 
    return r;
}

template double qg_1lha_r135(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r135(const std::array<dd_real,31>&);
#endif
