#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r66(const std::array<T,31>& k) {
  T z[22];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[9];
    z[4]=k[22];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[11];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[17];
    z[11]=k[6];
    z[12]=z[10] - z[11];
    z[13]=2*z[9];
    z[12]=z[12]*z[13];
    z[14]=z[10]*z[8];
    z[15]=static_cast<T>(1)+ 3*z[14];
    z[16]=z[12] - z[15];
    z[16]=z[9]*z[16];
    z[17]=z[4]*z[11]*npow(z[9],3);
    z[16]=z[16] + z[17];
    z[16]=z[6]*z[16];
    z[17]=z[7]*z[8];
    z[18]=z[9]*z[11];
    z[19]=z[18] - 1;
    z[19]=z[19]*z[4];
    z[20]= - z[9]*z[19];
    z[12]=z[16] + z[20] - 2*z[17] + static_cast<T>(3)- z[12];
    z[12]=z[6]*z[12];
    z[16]= - z[11] + 2*z[10];
    z[20]= - z[10]*z[18];
    z[16]=2*z[16] + z[20];
    z[16]=z[9]*z[16];
    z[19]=z[13]*z[19];
    z[15]=z[19] + z[16] - z[15];
    z[15]=z[6]*z[15];
    z[16]= - z[10] + 4*z[3];
    z[19]=3*z[3];
    z[20]= - z[1]*z[19];
    z[20]=static_cast<T>(2)+ z[20];
    z[20]=z[4]*z[20];
    z[15]=z[15] + z[20] + z[16];
    z[15]=z[6]*z[15];
    z[18]=z[18] - 2;
    z[20]=z[3]*z[1];
    z[20]=z[20] + z[18];
    z[20]=z[4]*z[20];
    z[18]= - z[10]*z[18];
    z[21]= - static_cast<T>(1)- z[14];
    z[21]=z[3]*z[21];
    z[18]=z[20] + z[21] + z[18];
    z[18]=z[6]*z[18];
    z[20]=z[10] - z[4];
    z[21]=z[3] - z[11];
    z[20]=z[21]*z[20];
    z[18]=z[18] + z[20];
    z[18]=z[6]*z[18];
    z[20]= - z[7]*z[20];
    z[18]=z[18] + z[20];
    z[18]=z[5]*z[18];
    z[20]= - z[2]*z[19];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[7]*z[20];
    z[20]=z[19] + z[20];
    z[20]=z[4]*z[20];
    z[16]= - z[7]*z[16];
    z[15]=z[18] + z[15] + z[16] + z[20];
    z[15]=z[5]*z[15];
    z[16]=npow(z[2],2);
    z[18]=z[16]*z[19];
    z[20]=5*z[2];
    z[13]=z[18] - z[20] + z[13];
    z[13]=z[7]*z[13];
    z[18]=z[2] - z[1];
    z[19]= - z[18]*z[19];
    z[13]=z[13] + static_cast<T>(2)+ z[19];
    z[13]=z[4]*z[13];
    z[19]=2*z[8] + z[20];
    z[19]=z[3]*z[19];
    z[19]= - static_cast<T>(5)+ z[19];
    z[19]=z[7]*z[19];
    z[12]=z[15] + z[12] + z[13] - 5*z[3] + z[19];
    z[12]=z[5]*z[12];
    z[13]= - 3*z[2] + z[9];
    z[13]=z[9]*z[13];
    z[15]= - z[3]*npow(z[2],3);
    z[13]=z[15] + 3*z[16] + z[13];
    z[13]=z[7]*z[13];
    z[15]=z[3]*z[2];
    z[16]=z[18]*z[15];
    z[18]=2*z[2];
    z[19]=z[18] - z[9];
    z[13]=z[13] + z[16] - z[19];
    z[13]=z[4]*z[13];
    z[16]=z[18] + z[8];
    z[16]= - z[16]*z[15];
    z[18]=z[8] + z[19];
    z[16]=2*z[18] + z[16];
    z[16]=z[7]*z[16];
    z[17]=static_cast<T>(1)- z[17];
    z[17]=z[9]*z[17];
    z[14]= - z[6]*npow(z[9],2)*z[14];
    z[14]=z[14] + z[17];
    z[14]=z[6]*z[14];
    z[15]=static_cast<T>(6)+ z[15];

    r += z[12] + z[13] + z[14] + 2*z[15] + z[16];
 
    return r;
}

template double qg_1lha_r66(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r66(const std::array<dd_real,31>&);
#endif
