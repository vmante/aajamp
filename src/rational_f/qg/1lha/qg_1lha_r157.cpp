#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r157(const std::array<T,31>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[9];
    z[4]=k[30];
    z[5]=k[11];
    z[6]=k[3];
    z[7]=k[5];
    z[8]=k[4];
    z[9]=k[7];
    z[10]=k[23];
    z[11]=z[2]*z[3];
    z[12]=z[11] - 2;
    z[13]=z[9]*z[7];
    z[14]=z[13] - z[12];
    z[14]=z[4]*z[14];
    z[15]=z[10]*z[8];
    z[16]=z[15] - 1;
    z[16]=z[16]*z[9];
    z[16]=z[16] - 2*z[10];
    z[17]=z[10]*z[7];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[3]*z[17];
    z[14]=z[14] + z[17] + z[16];
    z[14]=z[6]*z[14];
    z[17]=z[12]*z[2];
    z[18]=z[17] - z[7];
    z[19]= - z[4]*z[18];
    z[12]=z[19] + z[12];
    z[12]=z[14] + 2*z[12];
    z[12]=z[5]*z[12];
    z[13]=2*z[11] - z[13] - 2;
    z[13]=z[4]*z[13];
    z[12]=z[13] + z[12] - z[16] - 2*z[3];
    z[12]=z[6]*z[12];
    z[13]=z[3]*z[1];
    z[11]=z[13] - z[11];
    z[13]= - static_cast<T>(1)- z[11];
    z[14]=z[4]*z[2];
    z[13]=z[13]*z[14];
    z[14]= - z[18]*z[14];
    z[14]=z[17] + z[14];
    z[14]=z[5]*z[14];

    r += z[11] + z[12] + z[13] + z[14] + z[15];
 
    return r;
}

template double qg_1lha_r157(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r157(const std::array<dd_real,31>&);
#endif
