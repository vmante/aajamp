#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r155(const std::array<T,31>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[3];
    z[6]=k[19];
    z[7]=k[4];
    z[8]=z[3]*z[1];
    z[9]=z[7]*z[6];
    z[10]=z[6]*z[5];
    z[11]= - 3*z[9] + z[10] - static_cast<T>(3)+ 2*z[8];
    z[11]=z[2]*z[11];
    z[9]=z[9] + 1;
    z[12]= - z[3]*z[9];
    z[12]=2*z[6] + z[12];
    z[12]=z[7]*z[12];
    z[12]=z[12] - z[10] + static_cast<T>(2)- z[8];
    z[12]=z[2]*z[12];
    z[9]=z[10] - z[9];
    z[9]=z[7]*z[9];
    z[9]=2*z[9] + z[12];
    z[9]=z[2]*z[9];
    z[12]= - npow(z[7],2)*z[10];
    z[9]=z[12] + z[9];
    z[9]=z[4]*z[9];
    z[10]= - z[7]*z[10];
    z[9]=z[9] + z[10] + z[11];
    z[9]=z[4]*z[9];
    z[10]=z[7]*z[3];

    r += static_cast<T>(1)- z[8] + z[9] + z[10];
 
    return r;
}

template double qg_1lha_r155(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r155(const std::array<dd_real,31>&);
#endif
