#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r152(const std::array<T,31>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[5];
    z[6]=k[14];
    z[7]=k[4];
    z[8]=npow(z[7],2);
    z[9]=2*z[7];
    z[10]=z[9] - z[2];
    z[10]=z[2]*z[10];
    z[10]= - z[8] + z[10];
    z[10]=z[6]*z[10];
    z[11]=z[2] - z[7];
    z[12]=2*z[6];
    z[12]=z[11]*z[12];
    z[13]=z[5]*z[6];
    z[12]= - z[13] + static_cast<T>(1)+ z[12];
    z[12]=z[5]*z[12];
    z[14]=2*z[2];
    z[9]=z[12] + z[10] - z[14] - z[1] + z[9];
    z[9]=z[5]*z[9];
    z[10]=z[1] - z[7];
    z[12]= - z[1]*z[4];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[2]*z[12];
    z[10]=2*z[10] + z[12];
    z[10]=z[2]*z[10];
    z[9]=z[10] + z[9];
    z[9]=z[3]*z[9];
    z[10]= - z[2]*z[7];
    z[8]=z[8] + z[10];
    z[8]=z[6]*z[8];
    z[10]=3*z[7] - z[14];
    z[10]=z[6]*z[10];
    z[10]=2*z[13] - static_cast<T>(1)+ z[10];
    z[10]=z[5]*z[10];
    z[11]=z[2]*z[4]*z[11];
    z[8]=z[9] + z[10] + z[8] - z[7] + z[11];
    z[8]=z[3]*z[8];
    z[9]= - z[6]*z[7];

    r += z[8] + z[9] - z[13];
 
    return r;
}

template double qg_1lha_r152(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r152(const std::array<dd_real,31>&);
#endif
