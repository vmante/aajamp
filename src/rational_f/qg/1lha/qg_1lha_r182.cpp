#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r182(const std::array<T,31>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[2];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[21];
    z[8]=z[7]*z[4];
    z[9]=3*z[8];
    z[10]=z[2]*z[1];
    z[11]=z[6]*z[7];
    z[12]=z[11] + 1;
    z[13]=z[12]*z[2];
    z[14]=z[6]*z[13];
    z[14]=z[14] + z[10] - z[9];
    z[14]=z[6]*z[14];
    z[15]=z[5]*z[2];
    z[16]=z[11]*z[15];
    z[11]=static_cast<T>(1)+ 2*z[11];
    z[11]=z[11]*z[2];
    z[17]= - z[7] + z[11];
    z[17]=z[6]*z[17];
    z[16]=z[16] - z[8] + z[17];
    z[16]=z[5]*z[16];
    z[17]=npow(z[4],2);
    z[18]=2*z[7];
    z[19]=z[17]*z[18];
    z[14]=z[16] + z[14] - z[4] + z[19];
    z[14]=z[5]*z[14];
    z[16]=3*z[7];
    z[16]=z[17]*z[16];
    z[9]= - z[9] + z[12];
    z[9]=z[6]*z[9];
    z[9]=z[9] + z[16] + z[1] - 2*z[4];
    z[9]=z[6]*z[9];
    z[12]=npow(z[1],2);
    z[16]= - z[1] + z[4];
    z[16]=z[4]*z[16];
    z[17]= - z[7]*npow(z[4],3);
    z[9]=z[14] + z[9] + z[17] + z[12] + z[16];
    z[9]=z[3]*z[9];
    z[14]=z[7]*z[15];
    z[11]=z[14] - z[18] + z[11];
    z[11]=z[5]*z[11];
    z[13]= - z[18] + z[13];
    z[13]=z[6]*z[13];
    z[8]=z[11] + z[13] + z[8] - static_cast<T>(1)+ z[10];
    z[8]=z[5]*z[8];
    z[10]=z[2]*z[12];
    z[8]=z[9] + z[10] + z[8];

    r += z[8]*z[3];
 
    return r;
}

template double qg_1lha_r182(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r182(const std::array<dd_real,31>&);
#endif
