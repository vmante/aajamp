#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r181(const std::array<T,31>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[8];
    z[4]=k[18];
    z[5]=k[2];
    z[6]=k[9];
    z[7]=k[11];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[23];
    z[11]=z[10]*npow(z[2],2);
    z[11]=z[2] + z[11];
    z[11]=z[6]*z[11];
    z[11]=z[11] - 2;
    z[12]=z[8]*z[7];
    z[11]=z[12]*z[11];
    z[13]=z[1] - z[9];
    z[13]=z[13]*z[4]*z[2];
    z[14]=z[10]*z[9]*z[2];
    z[13]=z[14] + z[13];
    z[13]=z[3]*z[13];
    z[14]= - 2*z[2] + z[9];
    z[12]=z[12] - 1;
    z[14]=z[10]*z[12]*z[14];
    z[15]=z[5]*z[7];
    z[12]=z[15] + z[12];
    z[12]=z[6]*z[12];
    z[12]=z[12] - 2*z[7];
    z[12]=z[5]*z[12];
    z[15]=z[9]*z[7];
    z[16]=z[8] + z[9];
    z[16]=z[4]*z[16];

    r += static_cast<T>(1)+ z[11] + z[12] + z[13] + z[14] + z[15] + z[16];
 
    return r;
}

template double qg_1lha_r181(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r181(const std::array<dd_real,31>&);
#endif
