#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r158(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[15];
    z[5]=k[3];
    z[6]=k[5];
    z[7]=k[13];
    z[8]=z[3]*z[2];
    z[9]= - z[1]*z[3];
    z[9]=z[9] + z[8] - 1;
    z[10]=npow(z[4],2);
    z[11]=z[10]*z[2];
    z[12]=2*z[4];
    z[13]=z[11] + z[12];
    z[13]=z[13]*z[2];
    z[13]=z[13] + 1;
    z[9]=z[13]*z[9];
    z[10]=z[10]*z[6];
    z[12]= - z[12] - z[10];
    z[12]=z[6]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[5]*z[12];
    z[10]= - z[4] - z[10];
    z[10]=z[6]*z[2]*z[10];
    z[10]=z[12] + z[10];
    z[10]=z[7]*z[10];
    z[12]=z[4] + z[11];
    z[8]=z[12]*z[8];
    z[8]= - 2*z[11] + z[8];
    z[8]=z[6]*z[8];

    r += z[8] + z[9] + z[10];
 
    return r;
}

template double qg_1lha_r158(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r158(const std::array<dd_real,31>&);
#endif
