#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r184(const std::array<T,31>& k) {
  T z[12];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[5];
    z[4]=k[15];
    z[5]=k[27];
    z[6]=k[3];
    z[7]=z[5]*z[1];
    z[8]=z[2]*z[5];
    z[7]=z[7] - z[8];
    z[9]=static_cast<T>(1)+ z[7];
    z[9]=z[2]*z[9];
    z[10]=z[3]*z[5];
    z[8]= - z[10] + static_cast<T>(1)- 2*z[8];
    z[8]=z[3]*z[8];
    z[8]=z[8] - z[6] + z[9];
    z[8]=z[3]*z[8];
    z[9]=z[1] + z[6];
    z[11]= - z[2]*z[9];
    z[8]=z[11] + z[8];
    z[8]=z[4]*z[8];
    z[7]= - z[10] + z[7];
    z[7]=z[3]*z[7];
    z[7]=z[8] + z[7] - z[2] - z[9];
    z[7]=z[4]*z[7];

    r +=  - static_cast<T>(1)+ z[7];
 
    return r;
}

template double qg_1lha_r184(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r184(const std::array<dd_real,31>&);
#endif
