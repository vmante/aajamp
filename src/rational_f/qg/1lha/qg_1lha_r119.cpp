#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r119(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[15];
    z[5]=k[5];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=npow(z[4],2);
    z[9]=z[8]*z[2];
    z[10]=2*z[4];
    z[11]=z[9] + z[10];
    z[12]=z[11]*z[2];
    z[12]=z[12] + 1;
    z[13]=2*z[3];
    z[14]= - z[12]*z[13];
    z[11]=z[14] + z[11];
    z[11]=z[2]*z[11];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[3]*z[11];
    z[14]=z[9] + z[4];
    z[14]=z[14]*z[3];
    z[15]= - z[2]*z[14];
    z[9]=z[9] + z[15];
    z[9]=z[5]*z[9]*z[13];
    z[15]=z[1]*z[12]*npow(z[3],2);
    z[9]=z[15] + z[11] + z[9];
    z[9]=z[1]*z[9];
    z[11]=npow(z[2],2);
    z[13]=z[11]*z[13];
    z[13]= - 3*z[2] + z[13];
    z[13]=z[13]*z[14];
    z[11]=z[11]*z[3];
    z[14]=z[11] - 2*z[2];
    z[14]=z[3]*z[14];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[5]*z[8]*z[14];
    z[13]=z[14] + z[10] + z[13];
    z[13]=z[5]*z[13];
    z[8]=z[8]*z[5];
    z[10]= - z[10] - z[8];
    z[10]=z[5]*z[10];
    z[10]= - static_cast<T>(1)+ z[10];
    z[10]=z[6]*z[10];
    z[8]= - z[4] - z[8];
    z[8]=z[5]*z[2]*z[8];
    z[8]=z[10] + z[8];
    z[8]=z[7]*z[8];
    z[10]= - z[2] + z[11];
    z[10]=z[3]*z[12]*z[10];

    r += static_cast<T>(1)+ z[8] + z[9] + z[10] + z[13];
 
    return r;
}

template double qg_1lha_r119(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r119(const std::array<dd_real,31>&);
#endif
