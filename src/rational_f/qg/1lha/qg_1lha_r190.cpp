#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r190(const std::array<T,31>& k) {
  T z[9];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[18];
    z[6]= - z[4]*z[2];
    z[6]= - static_cast<T>(1)+ z[6];
    z[7]=z[2]*z[5];
    z[8]=z[7] - 1;
    z[6]=z[4]*z[8]*z[3]*z[6];
    z[7]= - z[3]*z[7];
    z[7]=z[5] + z[7];
    z[7]=z[1]*z[7]*npow(z[4],2);
    z[6]=z[6] + z[7];

    r += z[6]*npow(z[1],2);
 
    return r;
}

template double qg_1lha_r190(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r190(const std::array<dd_real,31>&);
#endif
