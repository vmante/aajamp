#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r117(const std::array<T,31>& k) {
  T z[21];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[14];
    z[5]=k[17];
    z[6]=k[5];
    z[7]=k[11];
    z[8]=z[4]*z[3];
    z[9]=npow(z[8],2);
    z[10]=2*z[1];
    z[11]=z[9]*z[10];
    z[12]=2*z[8];
    z[13]=npow(z[3],2);
    z[14]= - z[13] - z[12];
    z[14]=z[4]*z[14];
    z[14]=z[14] - z[11];
    z[14]=z[1]*z[14];
    z[15]=z[13]*z[4];
    z[16]=z[11] + z[15];
    z[17]=z[2]*z[16];
    z[9]=z[9]*z[1];
    z[18]=npow(z[4],2);
    z[19]=z[18]*z[3];
    z[19]=z[9] + z[19];
    z[20]=z[6]*z[19];
    z[14]=z[20] + z[17] - z[12] + z[14];
    z[14]=z[6]*z[14];
    z[8]=z[13] - z[8];
    z[17]=z[8]*z[4];
    z[17]=z[17] + z[9];
    z[17]=z[17]*z[2];
    z[9]= - z[15] - z[9];
    z[9]=z[1]*z[9];
    z[9]=z[17] - z[13] + 4*z[9];
    z[9]=z[2]*z[9];
    z[19]=z[1]*z[19];
    z[19]=z[12] + z[19];
    z[19]=z[1]*z[19];
    z[9]=z[14] + z[9] + z[3] + z[19];
    z[9]=z[6]*z[9];
    z[12]= - 3*z[13] + z[12];
    z[12]=z[4]*z[12];
    z[11]=z[12] - z[11];
    z[11]=z[1]*z[11];
    z[8]=z[11] - z[8];
    z[8]=z[2]*z[8];
    z[11]=npow(z[1],2);
    z[12]=z[16]*z[11];
    z[8]=z[12] + z[8];
    z[8]=z[2]*z[8];
    z[12]= - z[10]*z[15];
    z[12]= - z[17] - z[13] + z[12];
    z[12]=z[2]*z[11]*z[12];
    z[14]=npow(z[1],3);
    z[13]= - z[13]*z[14];
    z[12]=z[13] + z[12];
    z[12]=z[5]*z[12];
    z[13]=z[14]*z[15];
    z[11]= - z[5]*z[11];
    z[10]= - z[10] + z[11] + z[6];
    z[10]=z[18]*z[10];
    z[10]= - z[4] + z[10];
    z[10]=z[7]*z[10]*npow(z[2],2);

    r += z[8] + z[9] + z[10] + z[12] + z[13];
 
    return r;
}

template double qg_1lha_r117(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r117(const std::array<dd_real,31>&);
#endif
