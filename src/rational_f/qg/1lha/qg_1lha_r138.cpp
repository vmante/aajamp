#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r138(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[27];
    z[8]=z[2] - z[5];
    z[9]=2*z[6];
    z[10]=z[8]*z[9];
    z[8]=z[2]*z[8];
    z[11]=z[5]*z[4];
    z[12]=static_cast<T>(2)- z[11];
    z[12]=z[5]*z[12];
    z[12]=z[12] - z[2];
    z[12]=z[6]*z[12];
    z[8]= - 2*z[8] + z[12];
    z[8]=z[6]*z[8];
    z[12]=npow(z[2],3);
    z[8]= - z[12] + z[8];
    z[8]=z[7]*z[8];
    z[12]= - z[1] + z[2];
    z[12]=z[2]*z[12];
    z[8]=z[8] + z[12] + z[10];
    z[8]=z[3]*z[8];
    z[9]= - z[11]*z[9];
    z[10]=npow(z[2],2);
    z[12]=2*z[11];
    z[13]= - static_cast<T>(1)+ z[12];
    z[13]=z[5]*z[13];
    z[13]=z[13] + z[2];
    z[13]=z[6]*z[13];
    z[10]=z[10] + z[13];
    z[10]=z[7]*z[10];
    z[8]=z[8] + z[10] - z[2] + z[9];
    z[8]=z[3]*z[8];
    z[9]= - z[4]*z[1];
    z[10]= - static_cast<T>(1)- z[11];
    z[10]=z[7]*z[5]*z[10];

    r += z[8] + z[9] + z[10] + z[12];
 
    return r;
}

template double qg_1lha_r138(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r138(const std::array<dd_real,31>&);
#endif
