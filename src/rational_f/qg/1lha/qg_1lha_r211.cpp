#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r211(const std::array<T,31>& k) {
  T z[8];
  T r = 0;

    z[1]=k[1];
    z[2]=k[9];
    z[3]=k[2];
    z[4]=k[5];
    z[5]=k[8];
    z[6]=z[3] + z[1];
    z[6]=z[2]*z[6];
    z[7]=z[5]*z[4];

    r +=  - static_cast<T>(1)+ z[6] + z[7];
 
    return r;
}

template double qg_1lha_r211(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r211(const std::array<dd_real,31>&);
#endif
