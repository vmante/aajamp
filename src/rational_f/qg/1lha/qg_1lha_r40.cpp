#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r40(const std::array<T,31>& k) {
  T z[34];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[24];
    z[4]=k[4];
    z[5]=k[6];
    z[6]=k[15];
    z[7]=k[27];
    z[8]=k[5];
    z[9]=k[3];
    z[10]=k[13];
    z[11]=npow(z[4],2);
    z[12]=npow(z[6],2);
    z[13]=z[11]*z[12];
    z[14]=3*z[6];
    z[15]=z[12]*z[4];
    z[16]= - z[14] - z[15];
    z[17]=z[11]*z[3];
    z[16]=z[16]*z[17];
    z[16]= - z[13] + z[16];
    z[16]=z[3]*z[16];
    z[18]=z[3]*z[13];
    z[18]=6*z[15] + z[18];
    z[18]=z[3]*z[18];
    z[19]=z[12]*z[8];
    z[20]=npow(z[3],2);
    z[21]=z[19]*z[20];
    z[22]=2*z[4];
    z[23]=z[22]*z[21];
    z[18]=z[18] + z[23];
    z[18]=z[8]*z[18];
    z[23]=2*z[6];
    z[24]=z[15] + z[23];
    z[25]=z[24]*z[4];
    z[26]=z[3]*z[25];
    z[27]=z[15] + z[6];
    z[26]=2*z[27] + z[26];
    z[26]=z[3]*z[26];
    z[28]=2*z[3];
    z[29]= - z[28]*z[19];
    z[26]=z[26] + z[29];
    z[26]=z[1]*z[26];
    z[16]=z[26] + z[16] + z[18];
    z[16]=z[1]*z[16];
    z[18]= - z[22] + z[1];
    z[18]=z[1]*z[18];
    z[18]=z[11] + z[18];
    z[25]=z[25] + 1;
    z[18]=z[7]*z[25]*z[18];
    z[26]=z[27]*z[11];
    z[29]=z[3]*z[19];
    z[29]=z[12] + z[29];
    z[29]=z[8]*z[29];
    z[29]=z[29] - z[27];
    z[29]=z[29]*npow(z[1],2);
    z[18]=z[18] + z[26] + z[29];
    z[18]=z[5]*z[18];
    z[26]=3*z[4];
    z[29]=z[26]*z[12];
    z[30]=z[29] + z[23];
    z[31]= - z[30]*z[22];
    z[32]=4*z[15];
    z[33]= - z[19] - z[6] - z[32];
    z[33]=z[8]*z[33];
    z[31]=z[31] + z[33];
    z[31]=z[8]*z[31];
    z[33]=z[19]*z[22];
    z[32]=z[32] + 5*z[6];
    z[32]=z[32]*z[4];
    z[32]=z[32] + 1;
    z[33]=z[33] + z[32];
    z[33]=z[1]*z[33];
    z[32]= - z[4]*z[32];
    z[31]=z[33] + z[32] + z[31];
    z[31]=z[7]*z[31];
    z[13]=z[18] + z[13] + z[31];
    z[18]=npow(z[4],3);
    z[31]=z[18]*z[3];
    z[32]= - z[12]*z[31];
    z[25]= - 5*z[25] + z[32];
    z[25]=z[3]*z[25];
    z[30]= - z[3]*z[30];
    z[32]=5*z[12];
    z[30]= - z[32] + z[30];
    z[30]=z[3]*z[30];
    z[30]=z[30] - z[21];
    z[30]=z[8]*z[30];
    z[33]= - z[27]*z[26];
    z[33]= - static_cast<T>(1)+ z[33];
    z[33]=z[3]*z[33];
    z[33]= - 10*z[27] + z[33];
    z[33]=z[3]*z[33];
    z[30]=z[30] + z[33] - 4*z[12];
    z[30]=z[8]*z[30];
    z[33]=7*z[15];
    z[25]=z[30] + z[25] - 8*z[6] - z[33];
    z[25]=z[8]*z[25];
    z[30]=z[18]*z[20];
    z[30]=z[30] - 7*z[4];
    z[30]=z[6]*z[30];
    z[25]=z[25] - static_cast<T>(4)+ z[30];
    z[25]=z[8]*z[25];
    z[19]= - z[23] - z[19];
    z[19]=z[8]*z[19];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[9]*z[19];
    z[19]=3*z[19] + z[25];
    z[19]=z[10]*z[19];
    z[23]= - z[14] - z[33];
    z[23]=z[4]*z[23];
    z[25]=z[28]*z[11];
    z[27]= - z[27]*z[25];
    z[23]=z[27] + static_cast<T>(4)+ z[23];
    z[23]=z[3]*z[23];
    z[27]=z[3]*z[6];
    z[30]=2*z[12] + z[27];
    z[28]=z[30]*z[28];
    z[21]=z[28] + z[21];
    z[21]=z[8]*z[21];
    z[28]= - z[6] - z[29];
    z[28]=z[4]*z[28];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=z[3]*z[28];
    z[15]=4*z[6] - z[15];
    z[15]=2*z[15] + z[28];
    z[15]=z[3]*z[15];
    z[15]=z[21] + z[32] + z[15];
    z[15]=z[8]*z[15];
    z[12]=z[22]*z[12];
    z[12]=z[14] + z[12];
    z[12]=z[15] + 2*z[12] + z[23];
    z[12]=z[8]*z[12];
    z[14]= - z[4] + z[25];
    z[14]=z[8]*z[14];
    z[14]=z[14] + 2*z[11] - z[31];
    z[14]=z[3]*z[14];
    z[15]= - z[3]*z[26];
    z[15]=static_cast<T>(2)+ z[15];
    z[15]=z[3]*z[15];
    z[21]=z[1]*z[20];
    z[15]=z[15] + z[21];
    z[15]=z[1]*z[15];
    z[17]=3*z[17] - 4*z[4];
    z[17]=z[3]*z[17];
    z[20]=z[20]*z[8];
    z[21]= - z[4]*z[20];
    z[15]=z[15] + z[21] + static_cast<T>(1)+ z[17];
    z[15]=z[1]*z[15];
    z[17]= - z[10]*z[18]*z[20];
    z[14]=z[17] + z[15] - z[4] + z[14];
    z[14]=z[2]*z[14];
    z[11]= - z[24]*z[11];
    z[15]=z[18]*z[27];
    z[11]=z[11] + z[15];
    z[11]=z[3]*z[11];

    r += static_cast<T>(3)+ z[11] + z[12] + 2*z[13] + z[14] + z[16] + z[19];
 
    return r;
}

template double qg_1lha_r40(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r40(const std::array<dd_real,31>&);
#endif
