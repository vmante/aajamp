#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r143(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[4];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=2*z[1];
    z[9]=npow(z[3],2);
    z[8]=z[8]*z[9];
    z[10]=z[9]*z[5];
    z[11]=z[10] - z[3];
    z[12]=z[11] - z[8];
    z[13]= - z[5]*z[12];
    z[9]=z[9]*z[1];
    z[14]=z[9] + z[3];
    z[14]=z[14]*z[1];
    z[13]= - z[14] + z[13];
    z[13]=z[6]*z[13];
    z[8]=z[13] + z[8] - z[10];
    z[8]=z[5]*z[8];
    z[10]=z[11] - z[9];
    z[10]=2*z[10];
    z[13]= - z[5]*z[10];
    z[15]=z[1]*z[3];
    z[11]= - z[7]*z[11];
    z[11]=z[11] - z[15] + z[13];
    z[11]=z[6]*z[11];
    z[10]= - z[10] + z[11];
    z[10]=z[7]*z[10];
    z[11]= - z[1] + z[7];
    z[13]=z[15] + 1;
    z[11]=z[4]*z[13]*z[11];
    z[13]=z[4]*z[14];
    z[12]=z[13] + z[12];
    z[12]=z[2]*z[12];
    z[9]=z[3] - z[9];
    z[9]=z[1]*z[9];

    r += static_cast<T>(2)+ z[8] + z[9] + z[10] + z[11] + z[12];
 
    return r;
}

template double qg_1lha_r143(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r143(const std::array<dd_real,31>&);
#endif
