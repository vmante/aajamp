#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r92(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[14];
    z[5]=k[5];
    z[6]=k[11];
    z[7]=npow(z[4],2);
    z[8]=z[3]*z[4];
    z[9]=z[7] + z[8];
    z[10]=npow(z[2],2);
    z[11]=z[10]*z[3];
    z[9]=z[9]*z[11];
    z[12]=npow(z[8],2);
    z[13]=z[12]*z[1];
    z[10]=z[10]*z[13];
    z[9]=2*z[9] + z[10];
    z[9]=z[1]*z[9];
    z[10]=3*z[7];
    z[14]=z[8] + z[10];
    z[14]=z[2]*z[14];
    z[14]= - z[4] + z[14];
    z[14]=z[3]*z[14];
    z[15]=z[1]*z[2];
    z[12]=z[12]*z[15];
    z[12]=z[14] + z[12];
    z[12]=z[1]*z[12];
    z[14]=2*z[4];
    z[8]=z[2]*z[8];
    z[8]=z[12] - z[14] + z[8];
    z[12]=z[3]*z[7];
    z[12]=4*z[12] + z[13];
    z[12]=z[1]*z[12];
    z[10]=z[10] + z[12];
    z[10]=z[5]*z[10];
    z[8]=2*z[8] + z[10];
    z[8]=z[5]*z[8];
    z[7]= - z[2]*z[7];
    z[7]=z[4] + z[7];
    z[7]=z[7]*z[15];
    z[10]=z[2]*z[4];
    z[12]=static_cast<T>(1)- z[10];
    z[12]=z[2]*z[12];
    z[10]= - z[5]*z[10];
    z[7]=z[10] + z[12] + z[7];
    z[7]=z[6]*z[7];
    z[10]=z[14] + z[3];
    z[10]=z[10]*z[11];

    r += static_cast<T>(3)+ 2*z[7] + z[8] + z[9] + z[10];
 
    return r;
}

template double qg_1lha_r92(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r92(const std::array<dd_real,31>&);
#endif
