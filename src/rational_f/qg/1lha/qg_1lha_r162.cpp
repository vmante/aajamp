#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r162(const std::array<T,31>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[3];
    z[6]=k[10];
    z[7]=k[2];
    z[8]=k[11];
    z[9]=k[4];
    z[10]=k[17];
    z[11]=k[27];
    z[12]=z[9]*z[11];
    z[13]=z[4]*z[1];
    z[14]=z[5]*z[4];
    z[15]= - z[14] + 1;
    z[15]=z[10]*z[15];
    z[15]= - z[4] + z[15];
    z[15]=z[5]*z[15];
    z[15]=z[15] - z[13] + static_cast<T>(1)+ z[12];
    z[15]=z[5]*z[15];
    z[16]= - z[2]*z[11];
    z[12]=z[16] + static_cast<T>(1)- z[12];
    z[12]=z[2]*z[12];
    z[16]=z[4]*npow(z[1],2);
    z[12]=z[12] + z[15] + z[9] - z[16];
    z[12]=z[2]*z[12];
    z[15]= - z[6] + z[10];
    z[15]=z[9]*z[15];
    z[17]= - z[9]*z[6];
    z[17]= - static_cast<T>(2)+ z[17];
    z[17]=z[5]*z[10]*z[17];
    z[15]=z[17] - static_cast<T>(2)+ z[15];
    z[15]=z[5]*z[15];
    z[15]= - 2*z[1] + z[15];
    z[15]=z[5]*z[15];
    z[12]=z[15] + z[12];
    z[12]=z[3]*z[12];
    z[14]=z[14]*z[10];
    z[14]=z[14] + z[4];
    z[15]= - z[11] + z[14];
    z[15]=z[5]*z[15];
    z[13]=z[13] + z[15];
    z[13]=z[2]*z[13];
    z[15]= - z[1]*z[6];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[5]*z[15];
    z[12]=z[12] + z[13] + z[16] + z[15];
    z[12]=z[3]*z[12];
    z[13]= - z[5]*z[14];
    z[13]=z[13] - 1;
    z[13]=z[8]*z[13];
    z[14]=z[8]*z[7];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[4]*z[14];
    z[13]=z[14] + z[13];
    z[13]=z[2]*z[13];
    z[14]=2*z[7] - z[9];
    z[14]=z[8]*z[14];
    z[15]= - z[8]*npow(z[7],2);
    z[15]= - z[1] + z[7] + z[15];
    z[15]=z[4]*z[15];

    r +=  - static_cast<T>(1)+ z[12] + z[13] + z[14] + z[15];
 
    return r;
}

template double qg_1lha_r162(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r162(const std::array<dd_real,31>&);
#endif
