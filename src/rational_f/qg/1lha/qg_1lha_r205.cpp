#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r205(const std::array<T,31>& k) {
  T z[12];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[15];
    z[5]=k[5];
    z[6]=z[3]*z[2];
    z[7]=z[6] + 1;
    z[8]=npow(z[4],2);
    z[9]=z[8]*z[2];
    z[10]=z[9] + 2*z[4];
    z[10]=z[10]*z[2];
    z[10]=z[10] + 1;
    z[7]=z[10]*z[7];
    z[8]=z[8]*z[1];
    z[9]=z[9] + z[4];
    z[11]=z[8] + 2*z[9];
    z[6]=z[6]*z[11];
    z[6]=z[6] + z[9];
    z[6]=z[1]*z[6];
    z[11]=2*z[3];
    z[9]=z[9]*z[11];
    z[8]=z[3]*z[8];
    z[8]=z[9] + z[8];
    z[8]=z[1]*z[8];
    z[9]=z[3]*z[10];
    z[8]=z[9] + z[8];
    z[8]=z[5]*z[8];

    r += z[6] + z[7] + z[8];
 
    return r;
}

template double qg_1lha_r205(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r205(const std::array<dd_real,31>&);
#endif
