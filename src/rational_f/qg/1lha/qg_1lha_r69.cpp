#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r69(const std::array<T,31>& k) {
  T z[37];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[14];
    z[7]=k[12];
    z[8]=5*z[3];
    z[9]=npow(z[2],2);
    z[10]=z[9]*z[8];
    z[11]=npow(z[7],2);
    z[12]=z[11] + 2*z[9];
    z[13]=2*z[5];
    z[14]= - z[12]*z[13];
    z[15]=z[9] + z[11];
    z[16]=2*z[4];
    z[17]= - z[15]*z[16];
    z[18]=2*z[7];
    z[19]=z[2]*z[1];
    z[20]=static_cast<T>(3)- z[19];
    z[20]=z[2]*z[20];
    z[10]=z[17] + z[14] + z[10] + z[20] - z[18];
    z[10]=z[4]*z[10];
    z[14]=11*z[11];
    z[17]=4*z[9];
    z[20]= - z[17] - z[14];
    z[20]=z[3]*z[20];
    z[21]=2*z[11];
    z[22]=z[21] + z[9];
    z[23]=z[5]*z[22];
    z[24]=z[7] - z[2];
    z[25]=9*z[24];
    z[20]=z[23] + z[25] + z[20];
    z[20]=z[5]*z[20];
    z[22]=z[22]*z[8];
    z[22]= - 12*z[24] + z[22];
    z[22]=z[3]*z[22];
    z[20]=z[22] + z[20];
    z[20]=z[5]*z[20];
    z[22]=z[15]*z[3];
    z[23]= - z[22] + z[24];
    z[26]=z[5]*z[15];
    z[23]=4*z[23] + z[26];
    z[23]=z[5]*z[23];
    z[26]=3*z[3];
    z[27]=z[15]*z[26];
    z[28]=4*z[24];
    z[27]= - z[28] + z[27];
    z[27]=z[3]*z[27];
    z[23]=z[27] + z[23];
    z[23]=z[4]*z[23];
    z[12]=z[12]*z[3];
    z[27]=3*z[24] - z[12];
    z[29]=npow(z[3],2);
    z[27]=z[27]*z[29];
    z[20]=z[23] + z[27] + z[20];
    z[20]=z[4]*z[20];
    z[23]=z[11]*z[3];
    z[27]= - z[28] + 11*z[23];
    z[27]=z[3]*z[27];
    z[8]= - z[11]*z[8];
    z[8]=z[8] + z[24];
    z[30]=z[5]*z[11];
    z[8]=2*z[8] + z[30];
    z[8]=z[5]*z[8];
    z[8]=z[27] + z[8];
    z[8]=z[5]*z[8];
    z[27]=z[23] - z[24];
    z[30]=z[27]*z[29];
    z[8]= - 2*z[30] + z[8];
    z[8]=z[5]*z[8];
    z[8]=z[8] + z[20];
    z[8]=z[4]*z[8];
    z[20]=2*z[3];
    z[31]=z[20] - z[5];
    z[32]=z[31]*z[5];
    z[27]= - z[27]*z[32];
    z[27]=z[30] + z[27];
    z[30]=3*z[5];
    z[27]=z[27]*z[30];
    z[33]=z[29] - z[32];
    z[34]=2*z[24];
    z[22]=z[22] - z[34];
    z[22]=z[4]*z[22]*z[33];
    z[22]=z[27] + z[22];
    z[22]=z[4]*z[22];
    z[27]=npow(z[3],3);
    z[33]=z[11]*z[27];
    z[35]= - z[29]*z[21];
    z[36]=z[5]*z[23];
    z[35]=z[35] + z[36];
    z[35]=z[5]*z[35];
    z[33]=z[33] + z[35];
    z[35]=npow(z[5],2);
    z[33]=z[33]*z[35];
    z[22]=3*z[33] + z[22];
    z[22]=z[4]*z[22];
    z[23]=z[23] + z[7];
    z[33]=z[23]*z[29];
    z[32]= - z[23]*z[32];
    z[32]=z[33] + z[32];
    z[32]=z[32]*npow(z[5],3);
    z[22]=z[32] + z[22];
    z[22]=z[6]*z[22];
    z[30]=4*z[3] - z[30];
    z[30]=z[5]*z[23]*z[30];
    z[30]= - z[33] + z[30];
    z[30]=z[30]*z[35];
    z[8]=z[22] + z[30] + z[8];
    z[8]=z[6]*z[8];
    z[14]=10*z[9] + z[14];
    z[14]=z[3]*z[14];
    z[17]=z[17] + 5*z[11];
    z[22]= - z[5]*z[17];
    z[14]=z[22] - z[25] + z[14];
    z[14]=z[5]*z[14];
    z[22]= - z[13] + z[26];
    z[22]=z[15]*z[22];
    z[22]= - z[34] + z[22];
    z[22]=z[4]*z[22];
    z[25]=z[34] - z[12];
    z[25]=z[25]*z[26];
    z[14]=z[22] + z[25] + z[14];
    z[14]=z[14]*z[16];
    z[22]=7*z[9];
    z[25]=z[22] + 22*z[11];
    z[25]=z[3]*z[25];
    z[30]= - z[9] - 3*z[11];
    z[13]=z[30]*z[13];
    z[13]=z[13] - 10*z[24] + z[25];
    z[13]=z[5]*z[13];
    z[17]= - z[3]*z[17];
    z[17]=6*z[24] + z[17];
    z[17]=z[17]*z[20];
    z[13]=z[17] + z[13];
    z[13]=z[5]*z[13];
    z[17]=z[26]*z[9];
    z[25]= - z[34] + z[17];
    z[25]=z[25]*z[29];
    z[13]=z[14] + z[25] + z[13];
    z[13]=z[4]*z[13];
    z[14]= - z[11]*z[20];
    z[14]=z[14] - z[2] - z[18];
    z[14]=z[14]*z[20];
    z[18]=z[2] + 6*z[23];
    z[18]=z[5]*z[18];
    z[14]=z[14] + z[18];
    z[14]=z[5]*z[14];
    z[18]=z[2]*z[29];
    z[14]=z[18] + z[14];
    z[14]=z[5]*z[14];
    z[8]=2*z[8] + z[14] + z[13];
    z[8]=z[6]*z[8];
    z[12]=z[12] - z[24];
    z[13]=5*z[9] + 4*z[11];
    z[13]=z[5]*z[13];
    z[14]=z[4]*z[15];
    z[13]=z[14] - 3*z[12] + z[13];
    z[13]=z[13]*z[16];
    z[11]=z[22] + 6*z[11];
    z[11]=z[5]*z[11];
    z[11]= - 8*z[12] + z[11];
    z[11]=z[5]*z[11];
    z[12]=z[9]*z[3];
    z[14]= - z[28] + 9*z[12];
    z[14]=z[3]*z[14];
    z[11]=z[13] + z[14] + z[11];
    z[11]=z[4]*z[11];
    z[13]= - 3*z[9] - z[21];
    z[13]=z[3]*z[13];
    z[14]= - z[2] - z[7];
    z[15]=z[5]*z[9];
    z[13]=z[15] + 2*z[14] + z[13];
    z[13]=z[5]*z[13];
    z[14]=2*z[2] + z[17];
    z[14]=z[3]*z[14];
    z[13]=z[14] + z[13];
    z[13]=z[5]*z[13];
    z[14]= - z[9]*z[27];
    z[8]=z[8] + z[11] + z[14] + z[13];
    z[8]=z[6]*z[8];
    z[11]= - static_cast<T>(1)+ z[19];
    z[11]=z[2]*z[11];
    z[11]=z[11] - z[12];
    z[11]=z[3]*z[11];
    z[9]=z[9]*z[31];
    z[12]=static_cast<T>(2)- z[19];
    z[12]=z[2]*z[12];
    z[9]=z[12] + z[9];
    z[9]=z[5]*z[9];
    z[12]= - z[2]*npow(z[1],2);
    z[12]=2*z[1] + z[12];
    z[12]=z[2]*z[12];

    r +=  - static_cast<T>(1)+ z[8] + z[9] + z[10] + z[11] + z[12];
 
    return r;
}

template double qg_1lha_r69(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r69(const std::array<dd_real,31>&);
#endif
