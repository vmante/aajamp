#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r164(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[10];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[11];
    z[7]=k[5];
    z[8]=npow(z[4],3);
    z[9]=z[8]*z[5];
    z[10]=npow(z[4],2);
    z[11]=z[9] - z[10];
    z[12]=z[1] + z[2];
    z[12]=z[8]*z[12];
    z[12]=z[11] + z[12];
    z[13]=npow(z[5],2);
    z[12]=z[13]*z[12];
    z[9]= - 2*z[10] + z[9];
    z[9]=z[5]*z[9];
    z[14]=z[8]*z[2];
    z[15]=2*z[11] + z[14];
    z[15]=z[2]*z[15];
    z[9]=z[15] + z[4] + z[9];
    z[9]=z[6]*z[13]*z[9];
    z[15]= - z[2] - z[5];
    z[15]=z[8]*z[15];
    z[15]=z[10] + z[15];
    z[15]=z[15]*npow(z[2],2);
    z[10]= - z[10] + z[14];
    z[10]=z[1]*z[2]*z[10];
    z[10]=z[15] + z[10];
    z[10]=z[3]*z[10];
    z[15]=2*z[5] + z[2];
    z[15]=z[15]*z[14];
    z[10]=z[15] + z[10];
    z[10]=z[3]*z[10]*npow(z[1],2);
    z[15]=z[7]*z[6];
    z[15]=z[15] - 1;
    z[8]=z[8]*z[15];
    z[11]= - z[11] - z[14];
    z[11]=z[6]*z[11];
    z[8]=2*z[11] + z[8];
    z[8]=z[7]*z[13]*z[8];

    r += z[8] + z[9] + z[10] + z[12];
 
    return r;
}

template double qg_1lha_r164(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r164(const std::array<dd_real,31>&);
#endif
