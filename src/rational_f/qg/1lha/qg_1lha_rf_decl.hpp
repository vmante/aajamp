#ifndef QG_1LHA_RF_DECL
#define QG_1LHA_RF_DECL

#include <array>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
T qg_1lha_r1(const std::array<T,31>&);

template <class T>
T qg_1lha_r2(const std::array<T,31>&);

template <class T>
T qg_1lha_r3(const std::array<T,31>&);

template <class T>
T qg_1lha_r4(const std::array<T,31>&);

template <class T>
T qg_1lha_r5(const std::array<T,31>&);

template <class T>
T qg_1lha_r6(const std::array<T,31>&);

template <class T>
T qg_1lha_r7(const std::array<T,31>&);

template <class T>
T qg_1lha_r8(const std::array<T,31>&);

template <class T>
T qg_1lha_r9(const std::array<T,31>&);

template <class T>
T qg_1lha_r10(const std::array<T,31>&);

template <class T>
T qg_1lha_r11(const std::array<T,31>&);

template <class T>
T qg_1lha_r12(const std::array<T,31>&);

template <class T>
T qg_1lha_r13(const std::array<T,31>&);

template <class T>
T qg_1lha_r14(const std::array<T,31>&);

template <class T>
T qg_1lha_r15(const std::array<T,31>&);

template <class T>
T qg_1lha_r16(const std::array<T,31>&);

template <class T>
T qg_1lha_r17(const std::array<T,31>&);

template <class T>
T qg_1lha_r18(const std::array<T,31>&);

template <class T>
T qg_1lha_r19(const std::array<T,31>&);

template <class T>
T qg_1lha_r20(const std::array<T,31>&);

template <class T>
T qg_1lha_r21(const std::array<T,31>&);

template <class T>
T qg_1lha_r22(const std::array<T,31>&);

template <class T>
T qg_1lha_r23(const std::array<T,31>&);

template <class T>
T qg_1lha_r24(const std::array<T,31>&);

template <class T>
T qg_1lha_r25(const std::array<T,31>&);

template <class T>
T qg_1lha_r26(const std::array<T,31>&);

template <class T>
T qg_1lha_r27(const std::array<T,31>&);

template <class T>
T qg_1lha_r28(const std::array<T,31>&);

template <class T>
T qg_1lha_r29(const std::array<T,31>&);

template <class T>
T qg_1lha_r30(const std::array<T,31>&);

template <class T>
T qg_1lha_r31(const std::array<T,31>&);

template <class T>
T qg_1lha_r32(const std::array<T,31>&);

template <class T>
T qg_1lha_r33(const std::array<T,31>&);

template <class T>
T qg_1lha_r34(const std::array<T,31>&);

template <class T>
T qg_1lha_r35(const std::array<T,31>&);

template <class T>
T qg_1lha_r36(const std::array<T,31>&);

template <class T>
T qg_1lha_r37(const std::array<T,31>&);

template <class T>
T qg_1lha_r38(const std::array<T,31>&);

template <class T>
T qg_1lha_r39(const std::array<T,31>&);

template <class T>
T qg_1lha_r40(const std::array<T,31>&);

template <class T>
T qg_1lha_r41(const std::array<T,31>&);

template <class T>
T qg_1lha_r42(const std::array<T,31>&);

template <class T>
T qg_1lha_r43(const std::array<T,31>&);

template <class T>
T qg_1lha_r44(const std::array<T,31>&);

template <class T>
T qg_1lha_r45(const std::array<T,31>&);

template <class T>
T qg_1lha_r46(const std::array<T,31>&);

template <class T>
T qg_1lha_r47(const std::array<T,31>&);

template <class T>
T qg_1lha_r48(const std::array<T,31>&);

template <class T>
T qg_1lha_r49(const std::array<T,31>&);

template <class T>
T qg_1lha_r50(const std::array<T,31>&);

template <class T>
T qg_1lha_r51(const std::array<T,31>&);

template <class T>
T qg_1lha_r52(const std::array<T,31>&);

template <class T>
T qg_1lha_r53(const std::array<T,31>&);

template <class T>
T qg_1lha_r54(const std::array<T,31>&);

template <class T>
T qg_1lha_r55(const std::array<T,31>&);

template <class T>
T qg_1lha_r56(const std::array<T,31>&);

template <class T>
T qg_1lha_r57(const std::array<T,31>&);

template <class T>
T qg_1lha_r58(const std::array<T,31>&);

template <class T>
T qg_1lha_r59(const std::array<T,31>&);

template <class T>
T qg_1lha_r60(const std::array<T,31>&);

template <class T>
T qg_1lha_r61(const std::array<T,31>&);

template <class T>
T qg_1lha_r62(const std::array<T,31>&);

template <class T>
T qg_1lha_r63(const std::array<T,31>&);

template <class T>
T qg_1lha_r64(const std::array<T,31>&);

template <class T>
T qg_1lha_r65(const std::array<T,31>&);

template <class T>
T qg_1lha_r66(const std::array<T,31>&);

template <class T>
T qg_1lha_r67(const std::array<T,31>&);

template <class T>
T qg_1lha_r68(const std::array<T,31>&);

template <class T>
T qg_1lha_r69(const std::array<T,31>&);

template <class T>
T qg_1lha_r70(const std::array<T,31>&);

template <class T>
T qg_1lha_r71(const std::array<T,31>&);

template <class T>
T qg_1lha_r72(const std::array<T,31>&);

template <class T>
T qg_1lha_r73(const std::array<T,31>&);

template <class T>
T qg_1lha_r74(const std::array<T,31>&);

template <class T>
T qg_1lha_r75(const std::array<T,31>&);

template <class T>
T qg_1lha_r76(const std::array<T,31>&);

template <class T>
T qg_1lha_r77(const std::array<T,31>&);

template <class T>
T qg_1lha_r78(const std::array<T,31>&);

template <class T>
T qg_1lha_r79(const std::array<T,31>&);

template <class T>
T qg_1lha_r80(const std::array<T,31>&);

template <class T>
T qg_1lha_r81(const std::array<T,31>&);

template <class T>
T qg_1lha_r82(const std::array<T,31>&);

template <class T>
T qg_1lha_r83(const std::array<T,31>&);

template <class T>
T qg_1lha_r84(const std::array<T,31>&);

template <class T>
T qg_1lha_r85(const std::array<T,31>&);

template <class T>
T qg_1lha_r86(const std::array<T,31>&);

template <class T>
T qg_1lha_r87(const std::array<T,31>&);

template <class T>
T qg_1lha_r88(const std::array<T,31>&);

template <class T>
T qg_1lha_r89(const std::array<T,31>&);

template <class T>
T qg_1lha_r90(const std::array<T,31>&);

template <class T>
T qg_1lha_r91(const std::array<T,31>&);

template <class T>
T qg_1lha_r92(const std::array<T,31>&);

template <class T>
T qg_1lha_r93(const std::array<T,31>&);

template <class T>
T qg_1lha_r94(const std::array<T,31>&);

template <class T>
T qg_1lha_r95(const std::array<T,31>&);

template <class T>
T qg_1lha_r96(const std::array<T,31>&);

template <class T>
T qg_1lha_r97(const std::array<T,31>&);

template <class T>
T qg_1lha_r98(const std::array<T,31>&);

template <class T>
T qg_1lha_r99(const std::array<T,31>&);

template <class T>
T qg_1lha_r100(const std::array<T,31>&);

template <class T>
T qg_1lha_r101(const std::array<T,31>&);

template <class T>
T qg_1lha_r102(const std::array<T,31>&);

template <class T>
T qg_1lha_r103(const std::array<T,31>&);

template <class T>
T qg_1lha_r104(const std::array<T,31>&);

template <class T>
T qg_1lha_r105(const std::array<T,31>&);

template <class T>
T qg_1lha_r106(const std::array<T,31>&);

template <class T>
T qg_1lha_r107(const std::array<T,31>&);

template <class T>
T qg_1lha_r108(const std::array<T,31>&);

template <class T>
T qg_1lha_r109(const std::array<T,31>&);

template <class T>
T qg_1lha_r110(const std::array<T,31>&);

template <class T>
T qg_1lha_r111(const std::array<T,31>&);

template <class T>
T qg_1lha_r112(const std::array<T,31>&);

template <class T>
T qg_1lha_r113(const std::array<T,31>&);

template <class T>
T qg_1lha_r114(const std::array<T,31>&);

template <class T>
T qg_1lha_r115(const std::array<T,31>&);

template <class T>
T qg_1lha_r116(const std::array<T,31>&);

template <class T>
T qg_1lha_r117(const std::array<T,31>&);

template <class T>
T qg_1lha_r118(const std::array<T,31>&);

template <class T>
T qg_1lha_r119(const std::array<T,31>&);

template <class T>
T qg_1lha_r120(const std::array<T,31>&);

template <class T>
T qg_1lha_r121(const std::array<T,31>&);

template <class T>
T qg_1lha_r122(const std::array<T,31>&);

template <class T>
T qg_1lha_r123(const std::array<T,31>&);

template <class T>
T qg_1lha_r124(const std::array<T,31>&);

template <class T>
T qg_1lha_r125(const std::array<T,31>&);

template <class T>
T qg_1lha_r126(const std::array<T,31>&);

template <class T>
T qg_1lha_r127(const std::array<T,31>&);

template <class T>
T qg_1lha_r128(const std::array<T,31>&);

template <class T>
T qg_1lha_r129(const std::array<T,31>&);

template <class T>
T qg_1lha_r130(const std::array<T,31>&);

template <class T>
T qg_1lha_r131(const std::array<T,31>&);

template <class T>
T qg_1lha_r132(const std::array<T,31>&);

template <class T>
T qg_1lha_r133(const std::array<T,31>&);

template <class T>
T qg_1lha_r134(const std::array<T,31>&);

template <class T>
T qg_1lha_r135(const std::array<T,31>&);

template <class T>
T qg_1lha_r136(const std::array<T,31>&);

template <class T>
T qg_1lha_r137(const std::array<T,31>&);

template <class T>
T qg_1lha_r138(const std::array<T,31>&);

template <class T>
T qg_1lha_r139(const std::array<T,31>&);

template <class T>
T qg_1lha_r140(const std::array<T,31>&);

template <class T>
T qg_1lha_r141(const std::array<T,31>&);

template <class T>
T qg_1lha_r142(const std::array<T,31>&);

template <class T>
T qg_1lha_r143(const std::array<T,31>&);

template <class T>
T qg_1lha_r144(const std::array<T,31>&);

template <class T>
T qg_1lha_r145(const std::array<T,31>&);

template <class T>
T qg_1lha_r146(const std::array<T,31>&);

template <class T>
T qg_1lha_r147(const std::array<T,31>&);

template <class T>
T qg_1lha_r148(const std::array<T,31>&);

template <class T>
T qg_1lha_r149(const std::array<T,31>&);

template <class T>
T qg_1lha_r150(const std::array<T,31>&);

template <class T>
T qg_1lha_r151(const std::array<T,31>&);

template <class T>
T qg_1lha_r152(const std::array<T,31>&);

template <class T>
T qg_1lha_r153(const std::array<T,31>&);

template <class T>
T qg_1lha_r154(const std::array<T,31>&);

template <class T>
T qg_1lha_r155(const std::array<T,31>&);

template <class T>
T qg_1lha_r156(const std::array<T,31>&);

template <class T>
T qg_1lha_r157(const std::array<T,31>&);

template <class T>
T qg_1lha_r158(const std::array<T,31>&);

template <class T>
T qg_1lha_r159(const std::array<T,31>&);

template <class T>
T qg_1lha_r160(const std::array<T,31>&);

template <class T>
T qg_1lha_r161(const std::array<T,31>&);

template <class T>
T qg_1lha_r162(const std::array<T,31>&);

template <class T>
T qg_1lha_r163(const std::array<T,31>&);

template <class T>
T qg_1lha_r164(const std::array<T,31>&);

template <class T>
T qg_1lha_r165(const std::array<T,31>&);

template <class T>
T qg_1lha_r166(const std::array<T,31>&);

template <class T>
T qg_1lha_r167(const std::array<T,31>&);

template <class T>
T qg_1lha_r168(const std::array<T,31>&);

template <class T>
T qg_1lha_r169(const std::array<T,31>&);

template <class T>
T qg_1lha_r170(const std::array<T,31>&);

template <class T>
T qg_1lha_r171(const std::array<T,31>&);

template <class T>
T qg_1lha_r172(const std::array<T,31>&);

template <class T>
T qg_1lha_r173(const std::array<T,31>&);

template <class T>
T qg_1lha_r174(const std::array<T,31>&);

template <class T>
T qg_1lha_r175(const std::array<T,31>&);

template <class T>
T qg_1lha_r176(const std::array<T,31>&);

template <class T>
T qg_1lha_r177(const std::array<T,31>&);

template <class T>
T qg_1lha_r178(const std::array<T,31>&);

template <class T>
T qg_1lha_r179(const std::array<T,31>&);

template <class T>
T qg_1lha_r180(const std::array<T,31>&);

template <class T>
T qg_1lha_r181(const std::array<T,31>&);

template <class T>
T qg_1lha_r182(const std::array<T,31>&);

template <class T>
T qg_1lha_r183(const std::array<T,31>&);

template <class T>
T qg_1lha_r184(const std::array<T,31>&);

template <class T>
T qg_1lha_r185(const std::array<T,31>&);

template <class T>
T qg_1lha_r186(const std::array<T,31>&);

template <class T>
T qg_1lha_r187(const std::array<T,31>&);

template <class T>
T qg_1lha_r188(const std::array<T,31>&);

template <class T>
T qg_1lha_r189(const std::array<T,31>&);

template <class T>
T qg_1lha_r190(const std::array<T,31>&);

template <class T>
T qg_1lha_r191(const std::array<T,31>&);

template <class T>
T qg_1lha_r192(const std::array<T,31>&);

template <class T>
T qg_1lha_r193(const std::array<T,31>&);

template <class T>
T qg_1lha_r194(const std::array<T,31>&);

template <class T>
T qg_1lha_r195(const std::array<T,31>&);

template <class T>
T qg_1lha_r196(const std::array<T,31>&);

template <class T>
T qg_1lha_r197(const std::array<T,31>&);

template <class T>
T qg_1lha_r198(const std::array<T,31>&);

template <class T>
T qg_1lha_r199(const std::array<T,31>&);

template <class T>
T qg_1lha_r200(const std::array<T,31>&);

template <class T>
T qg_1lha_r201(const std::array<T,31>&);

template <class T>
T qg_1lha_r202(const std::array<T,31>&);

template <class T>
T qg_1lha_r203(const std::array<T,31>&);

template <class T>
T qg_1lha_r204(const std::array<T,31>&);

template <class T>
T qg_1lha_r205(const std::array<T,31>&);

template <class T>
T qg_1lha_r206(const std::array<T,31>&);

template <class T>
T qg_1lha_r207(const std::array<T,31>&);

template <class T>
T qg_1lha_r208(const std::array<T,31>&);

template <class T>
T qg_1lha_r209(const std::array<T,31>&);

template <class T>
T qg_1lha_r210(const std::array<T,31>&);

template <class T>
T qg_1lha_r211(const std::array<T,31>&);

template <class T>
T qg_1lha_r212(const std::array<T,31>&);

template <class T>
T qg_1lha_r213(const std::array<T,31>&);

template <class T>
T qg_1lha_r214(const std::array<T,31>&);

template <class T>
T qg_1lha_r215(const std::array<T,31>&);

#endif /* QG_1LHA_RF_DECL_H */
