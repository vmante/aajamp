#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r183(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[5];
    z[5]=k[15];
    z[6]=k[4];
    z[7]=k[11];
    z[8]=2*z[7];
    z[9]=z[8]*z[4];
    z[10]=z[5]*z[4];
    z[11]= - z[9] - z[10];
    z[12]=4*z[7];
    z[13]=3*z[10];
    z[14]= - static_cast<T>(7)- z[13];
    z[14]=z[5]*z[14];
    z[14]=z[12] + z[14];
    z[14]=z[2]*z[14];
    z[15]= - static_cast<T>(2)- z[10];
    z[15]=z[5]*z[15];
    z[15]=z[8] + z[15];
    z[15]=z[6]*z[15];
    z[11]=z[15] + 2*z[11] + z[14];
    z[11]=z[6]*z[11];
    z[14]=z[4]*z[7];
    z[14]=z[14] + z[10];
    z[13]= - static_cast<T>(8)- z[13];
    z[13]=z[5]*z[13];
    z[13]=z[8] + z[13];
    z[13]=z[2]*z[13];
    z[13]=z[13] - static_cast<T>(3)- 4*z[14];
    z[13]=z[2]*z[13];
    z[14]= - static_cast<T>(3)+ z[9];
    z[14]=z[4]*z[14];
    z[11]=z[11] + z[14] + z[13];
    z[11]=z[6]*z[11];
    z[13]=z[2]*z[5];
    z[14]= - static_cast<T>(3)- z[10];
    z[14]=z[14]*z[13];
    z[10]=z[14] - static_cast<T>(3)- 2*z[10];
    z[10]=z[2]*z[10];
    z[10]=z[10] - 3*z[1] - z[4];
    z[10]=z[2]*z[10];
    z[10]=z[10] + z[11];
    z[10]=z[3]*z[10];
    z[11]=npow(z[5],2);
    z[14]= - z[2]*z[11];
    z[14]= - z[12] + z[14];
    z[14]=z[2]*z[14];
    z[15]= - z[6] - 2*z[2];
    z[11]=z[11]*z[15];
    z[11]= - z[12] - z[5] + z[11];
    z[11]=z[6]*z[11];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[11] + 2*z[9] + z[14];
    z[9]=z[6]*z[9];
    z[11]=static_cast<T>(1)+ z[13];
    z[11]=z[2]*z[11];
    z[9]=z[10] + z[9] + z[1] + z[11];
    z[9]=z[3]*z[9];
    z[8]=z[6]*z[8];

    r += z[8] + z[9];
 
    return r;
}

template double qg_1lha_r183(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r183(const std::array<dd_real,31>&);
#endif
