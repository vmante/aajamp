#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r111(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[9];
    z[6]=k[3];
    z[7]=npow(z[6],2);
    z[8]=npow(z[1],2);
    z[9]=2*z[6];
    z[10]=z[9] - z[2];
    z[10]=z[2]*z[10];
    z[10]=z[10] - z[7] + z[8];
    z[11]=z[3]*z[2];
    z[10]=z[10]*z[11];
    z[9]=z[9] - z[1];
    z[12]=2*z[2];
    z[13]=z[12] - z[9];
    z[13]=z[2]*z[13];
    z[14]=z[1] - z[6];
    z[15]=z[14]*z[1];
    z[10]=z[10] - z[15] + z[13];
    z[10]=z[3]*z[10];
    z[9]=z[1]*z[9];
    z[13]= - 2*z[14] - z[2];
    z[13]=z[2]*z[13];
    z[7]=z[13] - z[7] + z[9];
    z[7]=z[3]*z[7]*npow(z[2],2);
    z[9]=z[2] + 2*z[1];
    z[13]= - z[6] + z[9];
    z[13]=z[2]*z[13];
    z[13]=z[15] + z[13];
    z[12]=z[13]*z[12];
    z[7]=z[12] + z[7];
    z[7]=z[3]*z[7];
    z[9]= - z[2]*z[9];
    z[7]=z[7] - z[8] + z[9];
    z[7]=z[4]*z[7];
    z[7]=z[7] + z[10] - z[1] - z[2];
    z[7]=z[4]*z[7];
    z[9]=z[6]*z[5];
    z[10]=z[1]*z[5];
    z[9]=z[9] - z[10];
    z[12]=z[1]*z[9];
    z[8]=z[5]*z[8]*z[11];
    z[10]=z[2]*z[10];
    z[8]=z[8] + z[12] + z[10];
    z[8]=z[3]*z[8];

    r += z[7] + z[8] + z[9];
 
    return r;
}

template double qg_1lha_r111(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r111(const std::array<dd_real,31>&);
#endif
