#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r160(const std::array<T,31>& k) {
  T z[15];
  T r = 0;

    z[1]=k[2];
    z[2]=k[9];
    z[3]=k[11];
    z[4]=k[30];
    z[5]=k[3];
    z[6]=k[5];
    z[7]=k[15];
    z[8]=k[4];
    z[9]=k[21];
    z[10]=z[1]*z[3];
    z[10]=z[10] - 1;
    z[10]=z[10]*z[4]*z[2];
    z[11]=z[5]*z[2];
    z[11]= - static_cast<T>(2)+ z[11];
    z[11]=z[4]*z[11];
    z[11]= - z[2] + z[11];
    z[11]=z[3]*z[11];
    z[10]=z[11] + z[10];
    z[10]=z[1]*z[10];
    z[11]=z[4]*z[6];
    z[11]=z[11] - 1;
    z[12]=z[7]*z[11];
    z[13]= - z[6] - 2*z[5];
    z[13]=z[4]*z[13];
    z[13]=static_cast<T>(3)+ z[13];
    z[13]=z[3]*z[13];
    z[10]=z[10] + z[13] + z[2] + z[12];
    z[10]=z[1]*z[10];
    z[12]=z[6]*z[9];
    z[13]=z[9]*z[8];
    z[13]=z[12] - static_cast<T>(1)+ z[13];
    z[13]=z[7]*z[13];
    z[13]=z[9] + z[13];
    z[13]=z[5]*z[13];
    z[14]=z[7]*z[8];
    z[11]= - z[5]*z[11];
    z[11]= - z[8] + z[11];
    z[11]=z[3]*z[11];

    r += z[10] + z[11] + z[12] + z[13] + z[14];
 
    return r;
}

template double qg_1lha_r160(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r160(const std::array<dd_real,31>&);
#endif
