#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r193(const std::array<T,31>& k) {
  T z[12];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[5];
    z[5]=k[25];
    z[6]=k[4];
    z[7]=z[2]*z[5];
    z[8]=3*z[7];
    z[9]=z[5]*z[6];
    z[10]=z[8] - static_cast<T>(2)- 3*z[9];
    z[10]=z[2]*z[10];
    z[11]=z[4]*z[5];
    z[11]=z[9] + z[11] + 1;
    z[8]= - z[8] + z[11];
    z[8]=z[4]*z[8];
    z[8]=z[8] + z[1] + z[10];
    z[8]=z[4]*z[8];
    z[9]=2*z[9] - z[7] + 1;
    z[9]=z[2]*z[9];
    z[10]= - z[5]*npow(z[6],2);
    z[9]=z[9] + z[10] - z[1] - z[6];
    z[9]=z[2]*z[9];
    z[8]=z[9] + z[8];
    z[8]=z[3]*z[8];
    z[7]=z[7] - z[11];
    z[7]=z[4]*z[7];
    z[7]=z[8] - z[1] + z[7];

    r += z[7]*z[3];
 
    return r;
}

template double qg_1lha_r193(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r193(const std::array<dd_real,31>&);
#endif
