#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r108(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[9];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[6];
    z[6]=k[5];
    z[7]=2*z[1];
    z[8]=npow(z[2],2);
    z[7]=z[7]*z[8];
    z[9]=z[7] - z[2];
    z[10]=z[8]*z[1];
    z[11]=z[1]*z[2];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[5]*z[11];
    z[11]=z[11] + z[2] + z[10];
    z[11]=z[4]*z[11];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[5]*z[11];
    z[12]=npow(z[5],2);
    z[12]=3*z[8] + z[12];
    z[12]=z[6]*z[12];
    z[11]=z[12] - 3*z[9] + 2*z[11];
    z[11]=z[6]*z[11];
    z[12]=4*z[1];
    z[8]=z[12]*z[8];
    z[12]=3*z[2];
    z[13]= - z[12] + z[8];
    z[13]=z[1]*z[13];
    z[14]=2*z[4];
    z[9]=z[9]*z[14];
    z[15]= - z[6]*z[7];
    z[9]=z[15] + z[13] + z[9];
    z[9]=z[6]*z[9];
    z[8]=5*z[2] - z[8];
    z[8]=z[1]*z[8];
    z[13]=z[10] - z[2];
    z[15]= - z[13]*z[14];
    z[8]=z[15] - static_cast<T>(1)+ z[8];
    z[8]=z[4]*z[8];
    z[7]=z[12] - z[7];
    z[7]=z[1]*z[7];
    z[7]= - static_cast<T>(1)+ z[7];
    z[7]=z[1]*z[7];
    z[7]=z[9] + z[7] + z[8];
    z[7]=z[3]*z[7];
    z[8]=z[10] - 2*z[2];
    z[8]=z[8]*z[1];
    z[8]=z[8] + 1;
    z[9]=z[4]*z[13];
    z[9]=z[9] - z[8];
    z[9]=z[9]*z[14];
    z[8]=z[5]*z[8]*npow(z[4],2);
    z[8]=z[9] + z[8];
    z[8]=z[5]*z[8];
    z[9]=z[1]*z[13];

    r += static_cast<T>(2)+ z[7] + z[8] + 3*z[9] + z[11];
 
    return r;
}

template double qg_1lha_r108(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r108(const std::array<dd_real,31>&);
#endif
