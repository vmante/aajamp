#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r178(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[11];
    z[4]=k[15];
    z[5]=k[3];
    z[6]=k[14];
    z[7]=k[13];
    z[8]=k[5];
    z[9]=k[8];
    z[10]=z[2] - z[8];
    z[11]=z[8]*z[7];
    z[11]=z[11] + 1;
    z[10]=z[11]*z[10];
    z[11]=z[7] - z[6];
    z[11]=z[11]*z[8];
    z[12]=z[5]*z[6];
    z[12]=z[12] + static_cast<T>(1)+ z[11];
    z[12]=z[5]*z[12];
    z[10]=z[12] - z[1] + z[10];
    z[10]=z[3]*z[10];
    z[12]=z[2]*z[7];
    z[13]=z[8]*z[6];
    z[14]= - z[6] + 2*z[7];
    z[14]=z[5]*z[14];
    z[10]=z[10] + z[12] + z[14] - static_cast<T>(2)+ z[13];
    z[10]=z[2]*z[10];
    z[14]=z[5]*z[7];
    z[14]=z[14] - 1;
    z[11]= - z[11] + z[14];
    z[11]=z[5]*z[11];
    z[15]=static_cast<T>(1)- z[13];
    z[15]=z[8]*z[15];
    z[10]=z[11] + z[1] + z[15] + z[10];
    z[10]=z[3]*z[10];
    z[11]=z[14]*z[4];
    z[12]= - z[4]*z[12];
    z[12]= - 2*z[11] + z[12];
    z[12]=z[2]*z[12];
    z[14]= - z[4]*z[1];
    z[13]=z[9]*z[13];
    z[13]=z[13] + z[4] - z[6];
    z[13]=z[8]*z[13];
    z[11]= - z[5]*z[11];

    r += z[10] + z[11] + z[12] + z[13] + z[14];
 
    return r;
}

template double qg_1lha_r178(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r178(const std::array<dd_real,31>&);
#endif
