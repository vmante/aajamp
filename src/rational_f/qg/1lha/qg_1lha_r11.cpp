#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r11(const std::array<T,31>& k) {
  T z[34];
  T r = 0;

    z[1]=k[2];
    z[2]=k[9];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[5];
    z[6]=k[6];
    z[7]=k[26];
    z[8]=k[4];
    z[9]=3*z[5];
    z[10]=z[6]*z[7];
    z[11]=npow(z[10],2);
    z[12]=z[9]*z[11];
    z[13]=npow(z[7],2);
    z[14]=z[13] + 4*z[10];
    z[15]=2*z[6];
    z[14]=z[14]*z[15];
    z[14]=z[14] + z[12];
    z[14]=z[5]*z[14];
    z[16]=2*z[5];
    z[17]=z[11]*z[16];
    z[18]=npow(z[6],2);
    z[19]=z[18]*z[7];
    z[20]= - z[19] - z[17];
    z[21]=z[11]*z[4];
    z[20]=2*z[20] + z[21];
    z[20]=z[4]*z[20];
    z[22]=npow(z[3],2);
    z[23]=2*z[22];
    z[24]=3*z[6];
    z[25]= - z[7] + z[24];
    z[25]=z[6]*z[25];
    z[14]=z[20] + z[14] - z[23] - z[13] + z[25];
    z[14]=z[4]*z[14];
    z[20]=z[5]*z[19];
    z[25]= - 3*z[7] - 4*z[6];
    z[25]=z[6]*z[25];
    z[25]= - 6*z[20] + z[25] + z[23];
    z[25]=z[5]*z[25];
    z[26]=5*z[6];
    z[27]=2*z[3];
    z[28]=z[26] - z[27];
    z[29]=z[22]*z[1];
    z[14]=z[14] + z[25] - 8*z[29] + z[7] - z[28];
    z[14]=z[4]*z[14];
    z[25]=3*z[1];
    z[30]=z[25]*z[22];
    z[31]=z[3] - z[6];
    z[32]=z[30] - z[31];
    z[32]=z[32]*z[16];
    z[33]=7*z[3] - 10*z[29];
    z[33]=z[1]*z[33];
    z[14]=z[14] + z[33] + z[32];
    z[14]=z[4]*z[14];
    z[23]= - z[18] + z[23];
    z[23]=z[1]*z[23];
    z[32]= - z[7] - z[6];
    z[32]=z[6]*z[32];
    z[32]=z[32] - z[20];
    z[9]=z[32]*z[9];
    z[9]=z[9] + z[23] - z[28];
    z[23]=z[13] + 5*z[10];
    z[23]=z[23]*z[15];
    z[11]=z[11]*z[5];
    z[23]=z[23] + z[11];
    z[23]=z[5]*z[23];
    z[28]=z[13] + 2*z[10];
    z[28]=z[28]*z[6];
    z[11]= - z[28] - z[11];
    z[11]=2*z[11] + z[21];
    z[11]=z[4]*z[11];
    z[21]=4*z[7] + z[26];
    z[21]=z[6]*z[21];
    z[11]=z[11] + z[23] + z[13] + z[21];
    z[11]=z[4]*z[11];
    z[9]=2*z[9] + z[11];
    z[9]=z[4]*z[9];
    z[11]=z[24] + z[3];
    z[11]=2*z[11] + 13*z[29];
    z[11]=z[1]*z[11];
    z[21]=2*z[1];
    z[23]=z[21]*z[22];
    z[24]= - z[23] - z[31];
    z[24]=z[5]*z[24];
    z[9]=z[9] + 4*z[24] + static_cast<T>(1)+ z[11];
    z[9]=z[4]*z[9];
    z[11]=5*z[3];
    z[24]=z[11] - 9*z[29];
    z[24]=z[1]*z[24];
    z[16]=z[16]*z[29];
    z[16]=z[16] + static_cast<T>(1)+ z[24];
    z[16]=z[5]*z[16];
    z[24]= - z[11] + 7*z[29];
    z[24]=z[1]*z[24];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[1]*z[24];
    z[16]=z[24] + z[16];
    z[9]=2*z[16] + z[9];
    z[9]=z[4]*z[9];
    z[16]=z[6] + 2*z[7];
    z[16]=z[16]*z[6];
    z[19]= - z[4]*z[19];
    z[19]=z[19] + z[16] + 2*z[20];
    z[19]=z[4]*z[19];
    z[16]= - z[16] - z[20];
    z[16]=z[5]*z[16];
    z[18]= - z[1]*z[18];
    z[16]=z[19] + z[16] + z[18] - z[7] - z[15];
    z[16]=z[4]*z[16];
    z[15]=z[15] - z[3];
    z[15]=2*z[15] - z[29];
    z[15]=z[1]*z[15];
    z[15]=z[16] + static_cast<T>(1)+ z[15];
    z[15]=z[4]*z[15];
    z[16]=z[30] + z[27];
    z[16]=z[16]*z[1];
    z[16]=z[16] + 1;
    z[18]=z[5] - z[1];
    z[16]=z[16]*z[18];
    z[15]=z[15] + z[16];
    z[15]=z[4]*z[15];
    z[16]=z[21] - z[5];
    z[16]=z[16]*z[5];
    z[19]=npow(z[1],2);
    z[16]=z[16] - z[19];
    z[21]=z[30] - z[27];
    z[21]=z[21]*z[1];
    z[21]=z[21] - 1;
    z[21]=z[21]*z[16];
    z[15]=z[15] + z[21];
    z[15]=z[4]*z[15];
    z[21]= - z[25] + z[5];
    z[21]=z[5]*z[21];
    z[19]=3*z[19] + z[21];
    z[21]=z[29] - z[27];
    z[21]=z[21]*z[1];
    z[21]=z[21] + 1;
    z[19]=z[5]*z[21]*z[19];
    z[21]= - z[21]*npow(z[1],3);
    z[15]=z[15] + z[21] + z[19];
    z[15]=z[2]*z[15];
    z[19]= - 5*z[29] + 8*z[3];
    z[19]=z[19]*z[1];
    z[19]=z[19] - 3;
    z[16]=z[19]*z[16];
    z[9]=2*z[15] + z[9] + z[16];
    z[9]=z[2]*z[9];
    z[11]= - z[11] + 4*z[29];
    z[11]=z[11]*z[1];
    z[11]=z[11] + 1;
    z[11]=z[11]*z[18];
    z[9]=z[9] + z[14] + z[11];
    z[9]=z[2]*z[9];
    z[11]=z[28] + z[12];
    z[11]=z[5]*z[11];
    z[12]=z[6]*z[13];
    z[12]=z[12] - z[17];
    z[12]=z[4]*z[12];
    z[11]=z[12] + z[11] - z[10] + z[22];
    z[11]=z[4]*z[11];
    z[12]=z[7]*z[8];
    z[13]= - static_cast<T>(1)+ z[12];
    z[10]=z[13]*z[10];
    z[12]= - static_cast<T>(2)+ z[12];
    z[12]=z[12]*z[20];
    z[10]=z[10] + z[12];
    z[10]=z[5]*z[10];
    z[10]=z[11] + z[10] - z[3] + z[23];
    z[10]=z[4]*z[10];
    z[11]= - z[3] + z[29];
    z[11]=z[1]*z[11];

    r += z[9] + z[10] + z[11];
 
    return r;
}

template double qg_1lha_r11(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r11(const std::array<dd_real,31>&);
#endif
