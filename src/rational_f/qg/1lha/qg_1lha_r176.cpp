#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r176(const std::array<T,31>& k) {
  T z[21];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[4];
    z[6]=k[2];
    z[7]=k[28];
    z[8]=k[11];
    z[9]=k[3];
    z[10]=k[14];
    z[11]=npow(z[6],2);
    z[12]=z[5]*z[4];
    z[13]=z[12] + 1;
    z[14]=z[13]*z[5];
    z[15]= - z[6] - z[14];
    z[15]=z[5]*z[15];
    z[11]= - z[11] + z[15];
    z[11]=z[3]*z[11];
    z[14]= - 2*z[6] - z[14];
    z[11]=2*z[14] + z[11];
    z[11]=z[3]*z[11];
    z[14]=z[8]*z[9];
    z[15]=z[10]*z[9];
    z[16]=z[8]*z[10];
    z[17]=z[16]*z[5];
    z[18]=z[17] - n<T>(5,2)*z[8] - z[4] + 2*z[10];
    z[18]=z[5]*z[18];
    z[11]=z[11] + z[18] - z[14] - n<T>(21,4) - z[15];
    z[11]=z[7]*z[11];
    z[18]= - z[4]*z[1];
    z[13]=z[18] - z[13];
    z[13]=z[5]*z[13];
    z[18]= - z[4]*npow(z[1],2);
    z[13]=z[13] + z[18] - z[1] - z[6];
    z[13]=z[3]*z[13];
    z[18]=z[8]*z[6];
    z[19]= - z[4] - z[8];
    z[19]=z[5]*z[19];
    z[13]=z[13] + z[19] - static_cast<T>(4)+ z[18];
    z[13]=z[3]*z[13];
    z[18]=3*z[6] + z[5];
    z[18]=z[3]*z[18];
    z[12]=z[18] + static_cast<T>(7)- z[12];
    z[12]=z[3]*z[12];
    z[18]=z[17] - n<T>(7,4)*z[8];
    z[12]=z[12] - z[4] + 3*z[10] - z[18];
    z[12]=z[7]*z[12];
    z[19]=z[3]*z[10];
    z[20]= - z[4] - z[10];
    z[20]=z[8]*z[20];
    z[19]=z[20] + z[19];
    z[19]=z[2]*z[19];
    z[19]=z[19] - 3*z[3] + z[4] - 4*z[10];
    z[19]=z[3]*z[19];
    z[19]=z[16] + z[19];
    z[19]=z[7]*z[19];
    z[16]= - z[3]*z[16];
    z[16]=z[16] + z[19];
    z[16]=z[2]*z[16];
    z[19]=2*z[3] - z[4] + z[8];
    z[19]=z[3]*z[19];
    z[12]=z[16] + z[19] + z[12];
    z[12]=z[2]*z[12];
    z[11]=z[12] + z[11] + z[13] - n<T>(3,4)*z[8] + 2*z[17];
    z[11]=z[2]*z[11];
    z[12]= - z[10] - z[18];
    z[12]=z[5]*z[12];
    z[12]=z[12] + z[14] - n<T>(1,4) - z[15];
    z[12]=z[5]*z[12];
    z[13]=n<T>(1,2)*z[8];
    z[14]=npow(z[9],2)*z[13];
    z[14]=z[14] + n<T>(3,2)*z[9] + z[6];
    z[12]=n<T>(1,2)*z[14] + z[12];
    z[12]=z[7]*z[12];
    z[14]=n<T>(1,2)*z[9] - z[6];
    z[13]=z[14]*z[13];
    z[14]=n<T>(1,4) - z[15];
    z[14]=z[8]*z[14];
    z[14]=z[14] - z[17];
    z[14]=z[5]*z[14];

    r += static_cast<T>(1)+ z[11] + z[12] + z[13] + z[14];
 
    return r;
}

template double qg_1lha_r176(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r176(const std::array<dd_real,31>&);
#endif
