#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r200(const std::array<T,31>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[10];
    z[4]=k[4];
    z[5]=k[7];
    z[6]=k[13];
    z[7]=k[2];
    z[8]=k[11];
    z[9]=k[3];
    z[10]=k[5];
    z[11]=npow(z[10],2);
    z[12]= - 2*z[10] + z[9];
    z[12]=z[9]*z[12];
    z[13]=z[9] - z[10];
    z[14]=2*z[13] + z[4];
    z[14]=z[4]*z[14];
    z[11]=z[14] + z[11] + z[12];
    z[11]=z[8]*z[11];
    z[11]=z[11] + z[4] + z[1] + z[13];
    z[11]=z[6]*z[11];
    z[12]=z[1]*z[2];
    z[14]=z[4]*z[5];
    z[12]=z[14] + z[12];
    z[12]=z[12]*npow(z[3],2);
    z[14]=z[5] - z[3];
    z[14]=z[3]*z[14];
    z[12]=z[14] + z[12];
    z[12]=z[12]*npow(z[1],2);
    z[13]= - 2*z[4] + z[7] - z[13];
    z[13]=z[8]*z[13];

    r +=  - static_cast<T>(1)+ z[11] + z[12] + z[13];
 
    return r;
}

template double qg_1lha_r200(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r200(const std::array<dd_real,31>&);
#endif
