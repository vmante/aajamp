#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r201(const std::array<T,31>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[13];
    z[3]=k[3];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=npow(z[6],2);
    z[9]=z[8]*z[4];
    z[10]=2*z[6];
    z[11]=z[9] + z[10];
    z[11]=z[11]*z[4];
    z[11]=z[11] + 1;
    z[12]=z[9] + z[6];
    z[13]=2*z[12];
    z[14]=z[8]*z[3];
    z[14]=z[13] + z[14];
    z[14]=z[3]*z[14];
    z[14]=z[14] + z[11];
    z[14]=z[5]*z[14];
    z[15]= - z[2]*z[11];
    z[16]=z[3]*z[2];
    z[8]= - z[8]*z[16];
    z[13]= - z[2]*z[13];
    z[8]=z[13] + z[8];
    z[8]=z[3]*z[8];
    z[8]=z[14] + z[15] + z[8];
    z[8]=z[7]*z[8];
    z[11]=z[4]*z[11];
    z[13]=2*z[4];
    z[12]=z[12]*z[13];
    z[9]=z[3]*z[9];
    z[9]=z[12] + z[9];
    z[9]=z[3]*z[9];
    z[9]=z[11] + z[9];
    z[9]=z[5]*z[9];
    z[10]= - z[4]*z[10];
    z[10]= - static_cast<T>(1)+ z[10];
    z[10]=z[2]*z[10];
    z[11]= - z[6]*z[16];
    z[10]=z[10] + z[11];
    z[10]=z[3]*z[10];
    z[11]=z[4]*z[6];
    z[11]= - static_cast<T>(1)- z[11];
    z[11]=z[4]*z[11];
    z[11]= - z[1] + z[11];
    z[11]=z[2]*z[11];

    r += z[8] + z[9] + z[10] + z[11];
 
    return r;
}

template double qg_1lha_r201(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r201(const std::array<dd_real,31>&);
#endif
