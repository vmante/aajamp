#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r128(const std::array<T,31>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[4];
    z[6]=k[7];
    z[7]=k[3];
    z[8]=z[3]*z[2];
    z[9]= - static_cast<T>(1)+ 2*z[8];
    z[10]=z[1]*z[3];
    z[11]= - z[9]*z[10];
    z[8]=z[8] - 1;
    z[9]=z[3]*z[9];
    z[9]= - z[6] + z[9];
    z[9]=z[5]*z[9];
    z[9]=z[11] + 2*z[8] + z[9];
    z[9]=z[1]*z[9];
    z[11]=z[3]*npow(z[2],2);
    z[11]=z[11] - z[2];
    z[8]=z[5]*z[8];
    z[8]=z[8] - z[11];
    z[10]=z[11]*z[10];
    z[8]=2*z[8] + z[10];
    z[8]=z[1]*z[8];
    z[10]=z[2] - z[7];
    z[11]=z[6]*z[7];
    z[12]=static_cast<T>(1)- z[11];
    z[12]=z[5]*z[12];
    z[12]= - 2*z[10] + z[12];
    z[12]=z[5]*z[12];
    z[10]=z[2]*z[10];
    z[8]=z[8] + z[10] + z[12];
    z[8]=z[4]*z[8];
    z[10]=z[5]*z[6];
    z[10]=z[10] - static_cast<T>(1)- z[11];
    z[10]=z[5]*z[10];
    z[8]=z[8] + z[9] + z[7] + z[10];
    z[8]=z[4]*z[8];
    z[9]=z[1] - 2*z[5];
    z[10]=npow(z[3],2);
    z[9]=z[10]*z[9];
    z[9]= - z[6] + z[9];
    z[9]=z[1]*z[9];
    z[10]=z[5]*z[10];
    z[10]=z[6] + z[10];
    z[10]=z[5]*z[10];

    r +=  - static_cast<T>(2)+ z[8] + z[9] + z[10];
 
    return r;
}

template double qg_1lha_r128(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r128(const std::array<dd_real,31>&);
#endif
