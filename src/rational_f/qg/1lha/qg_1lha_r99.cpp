#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r99(const std::array<T,31>& k) {
  T z[33];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[16];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[15];
    z[8]=k[26];
    z[9]=k[13];
    z[10]=5*z[6];
    z[11]=z[10]*z[9];
    z[12]=z[6]*z[3];
    z[13]=z[12] - 1;
    z[14]=z[13]*z[11];
    z[14]=z[14] - static_cast<T>(7)+ 9*z[12];
    z[15]=2*z[9];
    z[14]=z[14]*z[15];
    z[16]=3*z[8];
    z[17]=z[6]*z[2];
    z[18]=z[17]*z[8];
    z[14]=z[14] + 3*z[18] + 6*z[2] + z[16] + 5*z[3];
    z[14]=z[7]*z[14];
    z[19]= - z[3] + 2*z[8];
    z[20]=3*z[2];
    z[21]= - z[19]*z[20];
    z[22]=2*z[2];
    z[23]=z[9] + z[22] + z[16] + z[3];
    z[23]=z[23]*z[15];
    z[14]=z[14] + z[21] + z[23];
    z[14]=z[7]*z[14];
    z[21]=z[3]*z[8];
    z[23]=z[2]*z[3];
    z[24]=z[8]*z[15];
    z[23]=z[24] - 4*z[21] + z[23];
    z[23]=z[9]*z[23];
    z[24]=4*z[3];
    z[25]=z[2]*z[8];
    z[26]= - z[25]*z[24];
    z[27]=5*z[12];
    z[28]= - z[9]*z[27];
    z[19]=2*z[19] + z[28];
    z[19]=z[9]*z[19];
    z[16]= - z[16] + z[3];
    z[16]=z[2]*z[16];
    z[16]=z[16] + z[19];
    z[16]=z[7]*z[16];
    z[16]=z[16] + z[26] + z[23];
    z[16]=z[7]*z[16];
    z[19]=z[8] + z[3];
    z[19]=z[9]*z[19];
    z[19]= - z[21] + z[19];
    z[19]=z[9]*z[19];
    z[23]=z[25]*z[3];
    z[19]= - z[23] + z[19];
    z[19]=z[4]*z[19]*npow(z[7],2);
    z[16]=z[16] + z[19];
    z[16]=z[4]*z[16];
    z[19]=3*z[3];
    z[26]=z[19] - z[2];
    z[26]=z[2]*z[26];
    z[28]=z[8] - z[20];
    z[28]=z[9]*z[28];
    z[26]=z[28] - 5*z[21] + z[26];
    z[26]=z[9]*z[26];
    z[14]=z[16] + z[14] - 5*z[23] + z[26];
    z[14]=z[4]*z[14];
    z[16]=z[2]*z[5];
    z[23]=static_cast<T>(5)- z[16];
    z[23]=z[23]*z[20];
    z[26]= - z[24] + z[20];
    z[26]=z[2]*z[26];
    z[21]=2*z[21] + z[26];
    z[21]=z[6]*z[21];
    z[26]=static_cast<T>(7)- z[16];
    z[26]=z[26]*z[17];
    z[26]= - 8*z[16] + z[26];
    z[26]=z[9]*z[26];
    z[21]=z[26] + z[23] + z[21];
    z[21]=z[9]*z[21];
    z[13]=z[13]*z[6];
    z[13]=z[13] + z[5];
    z[23]= - z[13]*z[11];
    z[26]=8*z[5];
    z[28]=static_cast<T>(11)- 16*z[12];
    z[28]=z[6]*z[28];
    z[23]=z[23] - z[26] + z[28];
    z[23]=z[23]*z[15];
    z[28]=3*z[5];
    z[29]=z[8] + z[19];
    z[29]=z[29]*z[28];
    z[30]=4*z[16];
    z[31]= - z[8] + z[18] + z[2];
    z[32]= - 22*z[3] - z[31];
    z[32]=z[6]*z[32];
    z[23]=z[23] + z[32] + z[29] + z[30];
    z[23]=z[7]*z[23];
    z[29]=4*z[5];
    z[32]=z[29] - z[10];
    z[32]=z[9]*z[32];
    z[32]=z[32] + z[12] - static_cast<T>(10)+ 3*z[16];
    z[15]=z[32]*z[15];
    z[15]=z[23] + z[15] + 4*z[18] + 11*z[2] + z[8] + 8*z[3];
    z[15]=z[7]*z[15];
    z[18]=z[12]*z[25];
    z[23]=2*z[3] + z[2];
    z[23]=z[2]*z[23];
    z[14]=z[14] + z[15] + z[21] + z[23] + 2*z[18];
    z[14]=z[4]*z[14];
    z[13]=z[13]*z[6];
    z[15]=npow(z[5],2);
    z[13]=z[13] - z[15];
    z[11]=z[13]*z[11];
    z[18]= - static_cast<T>(18)+ 23*z[12];
    z[18]=z[6]*z[18];
    z[18]=13*z[5] + z[18];
    z[18]=z[6]*z[18];
    z[11]=z[11] - 9*z[15] + z[18];
    z[11]=z[9]*z[11];
    z[18]=z[8] + 7*z[3];
    z[18]=z[18]*z[15];
    z[21]=z[5]*z[3];
    z[23]= - 13*z[21] + 18*z[12];
    z[23]=z[6]*z[23];
    z[25]=z[15]*z[2];
    z[11]=z[11] + z[23] + z[18] + z[25];
    z[11]=z[7]*z[11];
    z[18]= - z[6] + 2*z[5];
    z[10]= - z[18]*z[10];
    z[23]=7*z[15];
    z[10]=z[23] + z[10];
    z[10]=z[9]*z[10];
    z[32]=static_cast<T>(16)- 3*z[12];
    z[32]=z[6]*z[32];
    z[10]=z[10] + z[32] - 21*z[5] + 4*z[25];
    z[10]=z[9]*z[10];
    z[32]=16*z[3];
    z[31]= - z[32] - z[31];
    z[31]=z[6]*z[31];
    z[32]=z[8] + z[32];
    z[32]=z[5]*z[32];
    z[10]=z[11] + z[10] + z[31] + z[32] + z[30];
    z[10]=z[7]*z[10];
    z[11]=5*z[5] - z[25];
    z[11]=z[11]*z[22];
    z[30]= - static_cast<T>(5)+ 2*z[16];
    z[30]=z[30]*z[17];
    z[11]=z[11] + z[30];
    z[11]=z[6]*z[11];
    z[11]= - 7*z[25] + z[11];
    z[11]=z[9]*z[11];
    z[20]= - z[15]*z[20];
    z[20]=19*z[5] + z[20];
    z[20]=z[2]*z[20];
    z[30]=z[2] - z[3];
    z[30]=z[30]*z[17];
    z[31]= - static_cast<T>(2)+ z[16];
    z[31]=z[2]*z[31];
    z[31]=8*z[31] - 3*z[30];
    z[31]=z[6]*z[31];
    z[11]=z[11] + z[31] - static_cast<T>(1)+ z[20];
    z[11]=z[9]*z[11];
    z[20]= - z[1] + z[28];
    z[20]=z[2]*z[20];
    z[20]= - static_cast<T>(4)+ z[20];
    z[20]=z[2]*z[20];
    z[10]=z[14] + z[10] + z[11] - 2*z[30] + 6*z[3] + z[20];
    z[10]=z[4]*z[10];
    z[11]= - z[26] + 5*z[25];
    z[11]=z[2]*z[11];
    z[14]=z[16] - 1;
    z[16]=z[2]*z[14];
    z[16]= - 5*z[16] + z[30];
    z[16]=z[6]*z[16];
    z[11]=z[11] + z[16];
    z[11]=z[6]*z[11];
    z[16]= - z[5] + z[25];
    z[16]=z[16]*z[22];
    z[14]= - z[14]*z[17];
    z[14]=z[16] + z[14];
    z[14]=z[6]*z[14];
    z[16]=npow(z[5],3);
    z[17]=z[16]*z[2];
    z[15]=3*z[15];
    z[20]=z[15] - z[17];
    z[20]=z[2]*z[20];
    z[14]=z[20] + z[14];
    z[14]=z[6]*z[14];
    z[20]= - z[16]*z[22];
    z[14]=z[20] + z[14];
    z[14]=z[9]*z[14];
    z[17]=z[17] - z[23];
    z[20]= - z[2]*z[17];
    z[11]=z[14] + z[20] + z[11];
    z[11]=z[9]*z[11];
    z[14]=z[6]*z[18];
    z[14]= - z[15] + z[14];
    z[14]=z[6]*z[14];
    z[18]=2*z[16];
    z[14]=z[18] + z[14];
    z[14]=z[9]*z[14];
    z[20]= - static_cast<T>(5)+ z[12];
    z[20]=z[6]*z[20];
    z[20]=7*z[5] + z[20];
    z[20]=z[6]*z[20];
    z[14]=z[14] + z[20] + z[17];
    z[14]=z[9]*z[14];
    z[12]=static_cast<T>(5)- 6*z[12];
    z[12]=z[6]*z[12];
    z[12]= - z[29] + z[12];
    z[12]=z[6]*z[12];
    z[13]= - z[6]*z[13];
    z[13]= - z[16] + z[13];
    z[13]=z[9]*z[13];
    z[12]=z[13] + z[15] + z[12];
    z[12]=z[6]*z[12];
    z[12]= - z[18] + z[12];
    z[12]=z[9]*z[12];
    z[13]=4*z[21] - z[27];
    z[13]=z[6]*z[13];
    z[15]= - z[3]*z[15];
    z[13]=z[15] + z[13];
    z[13]=z[6]*z[13];
    z[15]=z[3]*z[18];
    z[12]=z[12] + z[15] + z[13];
    z[12]=z[7]*z[12];
    z[13]= - 7*z[21] + z[27];
    z[13]=z[6]*z[13];
    z[15]=z[3]*z[23];
    z[12]=z[12] + z[14] + z[15] + z[13];
    z[12]=z[7]*z[12];
    z[13]= - z[3]*z[1];
    z[14]=z[1] - z[29];
    z[14]=z[2]*z[14];
    z[13]=z[14] + static_cast<T>(4)+ z[13];
    z[13]=z[2]*z[13];
    z[13]=z[30] - z[24] + z[13];
    z[13]=z[6]*z[13];
    z[14]= - z[1] + z[5];
    z[14]=z[14]*z[28];
    z[15]=npow(z[1],2);
    z[14]=z[15] + z[14];
    z[14]=z[2]*z[14];
    z[15]= - z[3]*z[15];
    z[14]=z[14] - 6*z[5] + 3*z[1] + z[15];
    z[14]=z[2]*z[14];
    z[15]= - z[1]*z[19];

    r += z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + 5*z[21];
 
    return r;
}

template double qg_1lha_r99(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r99(const std::array<dd_real,31>&);
#endif
