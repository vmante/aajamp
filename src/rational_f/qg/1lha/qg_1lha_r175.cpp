#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r175(const std::array<T,31>& k) {
  T z[11];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[9];
    z[6]=k[3];
    z[7]=npow(z[4],2);
    z[8]=z[6] - z[2];
    z[8]=z[3]*z[2]*z[8]*z[7];
    z[9]=z[4]*z[2];
    z[10]=static_cast<T>(1)- z[9];
    z[10]=z[4]*z[10];
    z[10]=z[5] + z[10];
    z[10]=z[3]*z[10];
    z[7]=z[7] + z[10];
    z[7]=z[1]*z[7];
    z[9]=static_cast<T>(1)+ z[9];
    z[9]=z[4]*z[9];
    z[7]=z[7] + z[8] + z[5] + z[9];

    r += z[7]*z[3]*npow(z[1],2);
 
    return r;
}

template double qg_1lha_r175(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r175(const std::array<dd_real,31>&);
#endif
