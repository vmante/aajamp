#include "qg_1lha_rf_decl.hpp"

template<class T>
T qg_1lha_r140(const std::array<T,31>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[12];
    z[6]=k[3];
    z[7]=2*z[2];
    z[8]=npow(z[3],2);
    z[7]=z[7]*z[8];
    z[9]= - 3*z[3] + z[7];
    z[9]=z[2]*z[9];
    z[9]=static_cast<T>(1)+ z[9];
    z[9]=z[5]*z[9];
    z[10]=z[8]*z[2];
    z[11]=z[10] - z[3];
    z[12]=z[1]*z[5];
    z[13]=z[11]*z[12];
    z[9]=z[9] + z[13];
    z[9]=z[1]*z[9];
    z[8]=z[6]*z[8];
    z[8]=z[8] - 2*z[11];
    z[13]=z[5]*z[2];
    z[14]=z[13] + 1;
    z[8]=z[14]*z[8];
    z[7]=z[3] - z[7];
    z[7]=z[7]*z[12];
    z[7]=z[7] + z[8];
    z[7]=z[6]*z[7];
    z[8]=z[10] - 2*z[3];
    z[8]=z[8]*z[2];
    z[10]=static_cast<T>(1)+ z[8];
    z[10]=z[10]*z[13];
    z[12]=z[2]*z[3];
    z[11]=z[1]*z[11];
    z[11]=z[11] - static_cast<T>(1)+ z[12];
    z[11]=z[1]*z[11];
    z[12]=z[1]*z[3];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[6]*z[12];
    z[11]=z[11] + z[12];
    z[11]=z[4]*z[11];

    r += static_cast<T>(2)+ z[7] + z[8] + z[9] + z[10] + z[11];
 
    return r;
}

template double qg_1lha_r140(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qg_1lha_r140(const std::array<dd_real,31>&);
#endif
