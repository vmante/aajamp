#include "qqb_0l_rf_decl.hpp"

template<class T>
T qqb_0l_r1(const std::array<T,31>& k) {

  T s12, s13, s14, s51, s23, s24, s25, s34, s45;
  T r;

  s12 = k[1];
  s23 = k[2];
  s34 = k[3];
  s45 = k[4];
  s51 = k[5];

  s13 = s45 - s12 - s23;
  s14 = s23 - s51 - s45;
  s24 = s51 - s23 - s34;
  s25 = s34 - s12 - s51;

  r = -s12*((s13*s13 + s23*s23)/(s14*s51*s24*s25) + 
            (s14*s14 + s24*s24)/(s13*s51*s23*s25) + 
            (s51*s51 + s25*s25)/(s13*s14*s23*s24));

  return r;
}

template double qqb_0l_r1(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_0l_r1(const std::array<dd_real,31>&);
#endif
