#ifndef QQB_0L_RF_DECL
#define QQB_0L_RF_DECL

#include <array>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
T qqb_0l_r1(const std::array<T,31>&);

#endif /* QQB_0L_RF_DECL_H */
