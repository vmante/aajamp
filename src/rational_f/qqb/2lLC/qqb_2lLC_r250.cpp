#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r250(const std::array<T,31>& k) {
  T z[62];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[12];
    z[4]=k[15];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[11];
    z[8]=k[13];
    z[9]=k[10];
    z[10]=k[2];
    z[11]=k[4];
    z[12]=k[14];
    z[13]=k[3];
    z[14]=2*z[6];
    z[15]=z[11]*z[9];
    z[16]=z[15]*z[6];
    z[17]= - z[14] - z[16];
    z[17]=z[11]*z[17];
    z[18]=z[5]*z[7];
    z[19]=z[18]*z[12];
    z[20]= - z[6] + z[19];
    z[20]=z[5]*z[20];
    z[17]=z[17] + z[20];
    z[20]=2*z[4];
    z[17]=z[17]*z[20];
    z[21]=3*z[6];
    z[22]=3*z[16];
    z[23]=2*z[3];
    z[17]=z[17] + z[19] - z[22] - z[21] + z[23];
    z[17]=z[4]*z[17];
    z[24]=z[7]*z[12];
    z[17]= - z[24] + z[17];
    z[17]=z[4]*z[17];
    z[25]=z[9] - z[12];
    z[26]= - z[25]*npow(z[10],3);
    z[27]=z[26]*z[20];
    z[28]=npow(z[10],2);
    z[29]=z[28]*z[9];
    z[30]=z[10]*z[12];
    z[31]=static_cast<T>(4)+ 5*z[30];
    z[31]=z[10]*z[31];
    z[32]=5*z[3];
    z[33]= - z[8] + z[32];
    z[33]=z[5]*z[33];
    z[33]=static_cast<T>(4)+ z[33];
    z[33]=z[5]*z[33];
    z[27]=z[27] + z[33] + z[31] - z[29];
    z[27]=z[4]*z[27];
    z[31]=z[9]*z[10];
    z[33]=z[31] - static_cast<T>(1)+ z[30];
    z[34]=z[8] - 15*z[3];
    z[34]=z[5]*z[34];
    z[27]=z[27] + 2*z[33] + z[34];
    z[27]=z[27]*z[20];
    z[33]=npow(z[5],2);
    z[34]=npow(z[7],2);
    z[35]=z[34]*z[8];
    z[36]=z[33]*z[35];
    z[37]=3*z[34];
    z[38]= - z[11]*z[37];
    z[36]=z[36] - z[23] + z[38];
    z[36]=z[5]*z[36];
    z[38]=npow(z[11],2);
    z[39]=z[38]*z[34];
    z[36]=z[39] + z[36];
    z[40]=3*z[5];
    z[41]=z[5]*z[23];
    z[41]=static_cast<T>(1)+ z[41];
    z[41]=z[41]*z[40];
    z[42]=z[3] - z[8];
    z[43]=z[42]*npow(z[5],3);
    z[44]= - z[4]*z[43];
    z[41]=z[41] + z[44];
    z[41]=z[41]*z[20];
    z[36]=3*z[36] + z[41];
    z[41]=2*z[2];
    z[36]=z[36]*z[41];
    z[44]=z[7]*z[11];
    z[45]=static_cast<T>(6)- z[15];
    z[45]=z[45]*z[44];
    z[46]=static_cast<T>(7)- 5*z[15];
    z[45]=2*z[46] + 3*z[45];
    z[45]=z[7]*z[45];
    z[46]=3*z[7];
    z[47]= - z[8] - z[46];
    z[47]=z[47]*z[18];
    z[48]=6*z[3];
    z[49]=z[48] + z[12];
    z[27]=z[36] + z[27] + 2*z[47] + z[45] - 7*z[9] + z[49];
    z[27]=z[2]*z[27];
    z[36]=3*z[9];
    z[45]=z[36]*z[10];
    z[47]=4*z[3];
    z[50]= - z[5]*z[47];
    z[51]=6*z[29];
    z[52]= - z[4]*z[51];
    z[50]=z[52] + z[50] + z[30] - z[45];
    z[50]=z[4]*z[50];
    z[49]=z[50] + 4*z[9] + z[49];
    z[49]=z[49]*z[20];
    z[50]=static_cast<T>(4)- 3*z[15];
    z[50]=z[50]*z[46];
    z[50]= - 20*z[9] + z[50];
    z[50]=z[7]*z[50];
    z[27]=z[27] + z[50] + z[49];
    z[27]=z[2]*z[27];
    z[49]=2*z[44];
    z[50]= - static_cast<T>(1)- z[49];
    z[50]=z[50]*z[46];
    z[52]=z[34]*z[40];
    z[53]=z[5]*z[48];
    z[54]=z[40]*z[3];
    z[55]= - static_cast<T>(2)- z[54];
    z[55]=z[5]*z[55];
    z[55]= - z[10] + z[55];
    z[55]=z[4]*z[55];
    z[53]=z[55] + static_cast<T>(1)+ z[53];
    z[53]=z[53]*z[20];
    z[50]=z[53] + z[52] - z[23] + z[50];
    z[50]=z[50]*z[41];
    z[15]= - static_cast<T>(5)+ 2*z[15];
    z[15]=z[15]*z[46];
    z[15]=10*z[9] + z[15];
    z[15]=z[7]*z[15];
    z[52]=z[5]*z[3];
    z[53]=static_cast<T>(3)+ 10*z[52];
    z[53]=z[4]*z[53];
    z[53]= - 8*z[3] + z[53];
    z[53]=z[53]*z[20];
    z[15]=z[50] + z[15] + z[53];
    z[15]=z[2]*z[15];
    z[50]=npow(z[4],2);
    z[53]=z[50]*z[3];
    z[55]=z[9]*z[34];
    z[15]=z[15] + 9*z[55] - 8*z[53];
    z[15]=z[2]*z[15];
    z[55]= - z[34]*z[36];
    z[54]= - static_cast<T>(1)- z[54];
    z[54]=z[4]*z[54];
    z[54]=z[23] + z[54];
    z[54]=z[54]*z[20];
    z[37]=z[37] + z[54];
    z[37]=z[37]*z[41];
    z[37]=z[37] + z[55] + 10*z[53];
    z[37]=z[37]*npow(z[2],2);
    z[53]=z[23]*z[6];
    z[54]= - z[35]*z[53];
    z[50]= - z[1]*z[50]*npow(z[2],3)*z[47];
    z[37]=z[50] + z[54] + z[37];
    z[37]=z[1]*z[37];
    z[50]=z[6]*z[8];
    z[54]=z[6] - z[8];
    z[54]=z[54]*z[3];
    z[55]= - z[50] + z[54];
    z[55]=z[7]*z[55];
    z[56]=z[3]*z[50];
    z[55]=z[56] + z[55];
    z[56]=2*z[7];
    z[55]=z[55]*z[56];
    z[15]=z[37] + z[55] + z[15];
    z[15]=z[1]*z[15];
    z[37]=z[11]*z[8];
    z[55]=z[6]*z[13];
    z[37]=z[37] + z[55] - 1;
    z[37]=z[37]*z[23];
    z[57]= - static_cast<T>(3)- z[55];
    z[57]=z[57]*z[36];
    z[58]=2*z[8];
    z[57]= - z[58] + z[57] - z[37];
    z[57]=z[7]*z[57];
    z[59]=z[9]*z[6];
    z[57]= - 4*z[59] + z[57];
    z[57]=z[7]*z[57];
    z[15]=z[15] + z[27] + z[57] + z[17];
    z[15]=z[1]*z[15];
    z[17]= - z[40]*z[35];
    z[27]=z[7]*z[8];
    z[17]=z[17] - z[53] - z[27];
    z[35]=2*z[5];
    z[17]=z[17]*z[35];
    z[53]= - static_cast<T>(5)+ 6*z[44];
    z[53]=z[7]*z[53];
    z[17]=z[17] + 9*z[3] + z[53];
    z[17]=z[17]*z[35];
    z[34]=z[34]*z[5];
    z[53]=z[58]*z[34];
    z[57]=z[58] - z[7];
    z[57]=z[57]*z[7];
    z[53]= - z[57] + z[53];
    z[53]=z[5]*z[53];
    z[53]= - z[23] + z[53];
    z[53]=z[5]*z[53];
    z[53]=2*z[39] + z[53];
    z[53]=z[5]*z[53];
    z[60]=z[7] - z[3];
    z[60]=z[60]*z[38];
    z[53]=z[53] + z[60];
    z[61]=4*z[4];
    z[43]=z[43]*z[61];
    z[43]=3*z[53] + z[43];
    z[41]=z[43]*z[41];
    z[38]= - z[38]*z[46];
    z[38]= - 4*z[11] + z[38];
    z[38]=z[7]*z[38];
    z[43]=5*z[8];
    z[53]=z[43] - 7*z[3];
    z[53]=z[5]*z[53];
    z[53]= - static_cast<T>(2)+ z[53];
    z[53]=z[5]*z[53];
    z[53]= - 2*z[10] + z[53];
    z[53]=z[53]*z[20];
    z[17]=z[41] + z[53] + z[17] - static_cast<T>(2)+ z[38];
    z[17]=z[2]*z[17];
    z[38]= - z[26]*z[61];
    z[28]=z[28]*z[12];
    z[38]=z[38] - 7*z[28] + 5*z[29];
    z[38]=z[4]*z[38];
    z[41]=3*z[8];
    z[32]= - z[41] + z[32];
    z[32]=z[5]*z[32];
    z[53]=2*z[30];
    z[32]=z[38] - z[53] + z[32];
    z[32]=z[32]*z[20];
    z[25]=z[11]*z[25];
    z[25]= - z[49] - static_cast<T>(2)+ z[25];
    z[25]=z[25]*z[46];
    z[38]=z[47]*z[6];
    z[49]=z[8]*z[46];
    z[49]= - z[38] + z[49];
    z[35]=z[49]*z[35];
    z[17]=z[17] + z[32] + z[35] + z[25] - z[48] - 5*z[12] + z[36];
    z[17]=z[2]*z[17];
    z[25]=z[6] - z[9];
    z[25]=2*z[25] + z[16];
    z[25]=z[11]*z[25];
    z[25]= - static_cast<T>(5)+ 2*z[25];
    z[25]=z[11]*z[25];
    z[32]=2*z[19] - z[12] - z[56];
    z[32]=z[5]*z[32];
    z[35]=z[11]*z[6];
    z[32]=z[32] - static_cast<T>(1)+ 2*z[35];
    z[32]=z[5]*z[32];
    z[25]=z[32] + z[51] + z[25];
    z[20]=z[25]*z[20];
    z[25]=z[5]*z[46];
    z[25]=z[25] - 1;
    z[25]=z[12]*z[25];
    z[14]= - z[7] + z[14] + z[25];
    z[14]=z[5]*z[14];
    z[25]=9*z[16];
    z[32]=z[25] + 11*z[6] - 13*z[9];
    z[32]=z[11]*z[32];
    z[14]=z[20] + z[14] + z[32] + 6*z[31] - static_cast<T>(5)- z[53];
    z[14]=z[4]*z[14];
    z[20]=z[12] + z[21];
    z[14]=z[14] - z[19] + z[25] - z[47] + 2*z[20] - 11*z[9];
    z[14]=z[4]*z[14];
    z[20]=static_cast<T>(2)+ z[55];
    z[20]=z[20]*z[36];
    z[21]= - z[11]*z[58];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[7]*z[21];
    z[20]=z[21] + z[20] - z[58] - z[6] + z[37];
    z[20]=z[7]*z[20];
    z[21]=2*z[59] - z[54];
    z[25]=z[52]*z[50];
    z[14]=z[15] + z[17] + z[14] + 8*z[25] + 2*z[21] + z[20];
    z[14]=z[1]*z[14];
    z[15]=z[33]*z[24];
    z[17]=z[30] + 1;
    z[15]=z[15] - z[35] - z[17];
    z[15]=z[5]*z[15];
    z[19]=z[19] - z[12] - z[7];
    z[19]=z[5]*z[19];
    z[19]=z[19] + z[44] + z[17];
    z[19]=z[5]*z[19];
    z[20]=static_cast<T>(1)- z[35];
    z[20]=z[11]*z[20];
    z[19]=z[19] - z[10] + z[20];
    z[19]=z[5]*z[19];
    z[20]= - z[6] + 2*z[9];
    z[21]=2*z[20] - z[16];
    z[21]=z[11]*z[21];
    z[24]=7*z[31];
    z[21]=z[21] + static_cast<T>(5)- z[24];
    z[21]=z[11]*z[21];
    z[21]=z[21] - 5*z[10] + z[51];
    z[21]=z[11]*z[21];
    z[19]=z[19] + 2*z[26] + z[21];
    z[19]=z[4]*z[19];
    z[21]=z[28] + z[29];
    z[20]=4*z[20] - z[22];
    z[20]=z[11]*z[20];
    z[20]=z[20] + static_cast<T>(4)- z[24];
    z[20]=z[11]*z[20];
    z[15]=z[19] + z[15] + 2*z[21] + z[20];
    z[15]=z[4]*z[15];
    z[19]= - z[6] + z[36];
    z[19]=2*z[19] - z[22];
    z[19]=z[11]*z[19];
    z[15]=z[15] + z[19] - z[45] + z[17];
    z[15]=z[4]*z[15];
    z[17]= - static_cast<T>(1)+ z[44];
    z[17]=z[17]*z[46];
    z[19]=z[34]*z[8];
    z[20]= - z[57] + z[19];
    z[20]=z[20]*z[40];
    z[17]=z[20] + z[17] + z[43] - z[23];
    z[17]=z[5]*z[17];
    z[17]=3*z[39] + z[17];
    z[17]=z[5]*z[17];
    z[17]=3*z[60] + z[17];
    z[17]=z[2]*z[17];
    z[20]=z[43] - z[46];
    z[20]=z[7]*z[20];
    z[20]= - 6*z[19] - z[38] + z[20];
    z[20]=z[5]*z[20];
    z[20]=z[20] - z[7] + z[48] - z[41] - 4*z[6];
    z[20]=z[5]*z[20];
    z[21]= - z[11]*z[3];
    z[21]=z[21] + z[44];
    z[17]=z[17] + 3*z[21] + z[20];
    z[17]=z[2]*z[17];
    z[19]=z[27] + z[19];
    z[19]=z[19]*z[40];
    z[17]=z[17] + z[19] - 3*z[42] + z[56];
    z[17]=z[2]*z[5]*z[17];
    z[18]= - z[18] - 1;
    z[18]=z[58]*z[18];
    z[19]= - z[8]*z[44];
    z[15]=z[17] + z[15] + z[19] - z[16] + z[3] + z[9] + z[18];
    z[14]=2*z[15] + z[14];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lLC_r250(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lLC_r250(const std::array<dd_real,31>&);
#endif
