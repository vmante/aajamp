#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r217(const std::array<T,31>& k) {
  T z[125];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[10];
    z[7]=k[14];
    z[8]=k[8];
    z[9]=k[21];
    z[10]=k[9];
    z[11]=k[22];
    z[12]=k[30];
    z[13]=k[28];
    z[14]=k[2];
    z[15]=k[15];
    z[16]=k[3];
    z[17]=k[4];
    z[18]=k[5];
    z[19]=z[6]*z[1];
    z[20]=z[19]*z[17];
    z[21]=2*z[1];
    z[22]= - z[20] - z[21];
    z[23]=npow(z[15],3);
    z[22]=z[23]*z[22];
    z[24]=npow(z[1],2);
    z[25]=2*z[24];
    z[26]=z[25]*z[15];
    z[27]=3*z[1];
    z[28]= - z[27] + z[26];
    z[29]=npow(z[15],2);
    z[30]=z[29]*z[6];
    z[28]=z[28]*z[30];
    z[22]=z[28] + z[22];
    z[22]=z[17]*z[22];
    z[28]=4*z[24];
    z[31]=z[18]*z[1];
    z[32]=z[28] - z[31];
    z[33]=2*z[15];
    z[34]=z[32]*z[33];
    z[35]=7*z[1];
    z[36]= - z[35] + z[34];
    z[37]=2*z[29];
    z[36]=z[36]*z[37];
    z[38]=npow(z[1],3);
    z[39]=z[38]*z[15];
    z[40]=4*z[39];
    z[41]=15*z[24];
    z[42]=z[41] - z[40];
    z[42]=z[15]*z[42];
    z[42]= - n<T>(50,3)*z[1] + z[42];
    z[43]=z[6]*z[15];
    z[42]=z[42]*z[43];
    z[22]=4*z[22] + z[36] + z[42];
    z[22]=z[17]*z[22];
    z[36]=z[24]*z[18];
    z[42]=z[36] - z[38];
    z[44]=z[42]*z[15];
    z[44]=8*z[44];
    z[45]=2*z[31];
    z[46]=z[44] + z[41] + z[45];
    z[46]=z[15]*z[46];
    z[46]= - 14*z[1] + z[46];
    z[46]=z[15]*z[46];
    z[47]=z[24]*z[6];
    z[48]=n<T>(1,3)*z[47];
    z[49]=z[24]*z[8];
    z[50]=3*z[49];
    z[51]=z[38]*z[6];
    z[52]=z[51]*z[7];
    z[53]= - n<T>(8,3)*z[52] - z[48] - z[50];
    z[53]=z[7]*z[53];
    z[54]=n<T>(26,3)*z[24] - 3*z[39];
    z[54]=z[15]*z[54];
    z[54]= - n<T>(26,3)*z[1] + z[54];
    z[54]=z[6]*z[54];
    z[55]=z[31]*z[8];
    z[55]=z[55] - z[1];
    z[56]=6*z[8];
    z[57]=z[55]*z[56];
    z[22]=z[22] + z[53] + z[57] + z[46] + z[54];
    z[22]=z[17]*z[22];
    z[46]=5*z[38];
    z[53]=z[46] - 11*z[36];
    z[54]=n<T>(1,3)*z[3];
    z[53]=z[53]*z[54];
    z[57]=z[38]*z[18];
    z[58]=npow(z[1],4);
    z[57]=z[57] + z[58];
    z[57]=z[57]*z[3];
    z[59]=z[38] - 2*z[57];
    z[59]=z[7]*z[59];
    z[60]=3*z[38];
    z[61]=z[8]*z[60];
    z[53]=n<T>(8,3)*z[59] + z[53] - 28*z[24] + z[61];
    z[53]=z[7]*z[53];
    z[59]=z[1] - z[26];
    z[59]=z[6]*z[59];
    z[61]=z[15]*z[20];
    z[59]=z[59] + z[61];
    z[59]=z[17]*z[59];
    z[61]=z[24]*z[15];
    z[62]=z[6]*z[39];
    z[59]=z[59] - z[52] + z[62] - z[1] + 5*z[61];
    z[59]=z[17]*z[59];
    z[62]=z[36] + z[38];
    z[63]=z[62]*z[15];
    z[64]=z[31] + z[24];
    z[65]=z[38]*z[3];
    z[59]=z[59] - z[65] + z[63] + z[64];
    z[66]=n<T>(1,3)*z[36];
    z[67]=2*z[38];
    z[68]=z[67] + z[66];
    z[69]=z[15]*z[18];
    z[68]=z[68]*z[69];
    z[70]=2*z[18];
    z[71]= - z[70]*z[65];
    z[68]=z[68] + z[71];
    z[68]=z[5]*z[68];
    z[71]=4*z[38];
    z[72]=z[71] - 5*z[57];
    z[73]=n<T>(1,3)*z[7];
    z[72]=z[72]*z[73];
    z[59]=z[72] + z[68] + n<T>(1,3)*z[59];
    z[59]=z[2]*z[59];
    z[68]=z[45] - z[24];
    z[72]=z[68]*z[70];
    z[74]=z[39]*z[18];
    z[75]=4*z[74];
    z[72]= - z[75] - z[60] + z[72];
    z[72]=z[15]*z[72];
    z[76]=3*z[31];
    z[77]=n<T>(29,3)*z[24] + z[76];
    z[72]=2*z[77] + z[72];
    z[72]=z[15]*z[72];
    z[77]=8*z[31];
    z[78]=3*z[24];
    z[79]= - z[78] + z[77];
    z[79]=z[8]*z[79]*z[70];
    z[79]=z[79] - z[78] - 16*z[31];
    z[79]=z[8]*z[79];
    z[80]=4*z[36];
    z[81]=n<T>(2,3)*z[58];
    z[82]= - z[3]*z[81];
    z[82]=z[82] - z[80] - z[38];
    z[82]=z[3]*z[82];
    z[83]=z[80] - z[74];
    z[84]=z[38] + z[83];
    z[84]=z[15]*z[84];
    z[82]=z[84] + z[82];
    z[84]=2*z[5];
    z[82]=z[82]*z[84];
    z[85]=z[25] - z[39];
    z[86]=3*z[6];
    z[87]= - z[85]*z[86];
    z[88]=z[86]*z[38];
    z[89]=n<T>(13,3)*z[24];
    z[90]=z[89] + z[88];
    z[90]=z[3]*z[90];
    z[22]=4*z[59] + z[22] + z[53] + z[82] + z[90] + z[79] + z[87] - n<T>(8,3)
   *z[1] + z[72];
    z[22]=z[2]*z[22];
    z[53]=z[51] + z[24];
    z[59]=z[53]*z[6];
    z[72]=npow(z[18],2);
    z[79]=z[72]*z[15];
    z[82]=z[79]*z[1];
    z[87]=z[31] - z[82];
    z[87]=z[15]*z[87];
    z[87]=z[87] + z[59];
    z[90]=3*z[12];
    z[87]=z[87]*z[90];
    z[91]=npow(z[6],2);
    z[92]=z[91]*z[24];
    z[82]=z[45] + z[82];
    z[82]=z[82]*z[33];
    z[82]= - z[27] + z[82];
    z[82]=z[15]*z[82];
    z[82]=z[87] + z[82] + z[92];
    z[82]=z[12]*z[82];
    z[87]=5*z[24];
    z[93]=z[87] - z[88];
    z[94]=z[93]*z[6];
    z[95]=z[38]*z[8];
    z[96]=z[95] - z[24];
    z[96]=z[76] + 16*z[96];
    z[96]=z[7]*z[96];
    z[96]=z[96] - z[27] + z[94];
    z[96]=z[10]*z[96];
    z[97]= - z[24] - 4*z[95];
    z[98]=3*z[8];
    z[97]=z[97]*z[98];
    z[97]=z[35] + z[97];
    z[97]=z[7]*z[97];
    z[99]=z[47]*z[8];
    z[100]= - 9*z[1] - z[47];
    z[100]=z[6]*z[100];
    z[96]=z[96] + z[97] + z[100] + z[99];
    z[96]=z[10]*z[96];
    z[97]=2*z[47];
    z[100]= - z[1] - z[97];
    z[100]=z[12]*z[100]*z[86];
    z[101]=z[91]*z[1];
    z[100]= - z[101] + z[100];
    z[100]=z[12]*z[100];
    z[102]=6*z[47];
    z[103]=5*z[1];
    z[104]= - z[103] + z[102];
    z[104]=z[10]*z[6]*z[104];
    z[104]=z[101] + z[104];
    z[104]=z[10]*z[104];
    z[105]=npow(z[12],2);
    z[106]=npow(z[10],2);
    z[105]=z[105] - z[106];
    z[106]=3*z[16];
    z[105]=z[105]*z[106];
    z[106]=z[101]*z[105];
    z[100]=z[106] + z[100] + z[104];
    z[100]=z[16]*z[100];
    z[104]=z[49] - z[1];
    z[104]=z[104]*z[7];
    z[99]=z[104] - z[99] + z[19];
    z[104]=z[99]*z[8];
    z[106]=z[10]*z[99];
    z[106]= - 3*z[104] + 4*z[106];
    z[106]=z[10]*z[106];
    z[99]=z[29]*z[99];
    z[99]=z[106] + z[99];
    z[106]=2*z[14];
    z[99]=z[99]*z[106];
    z[107]=z[20]*z[29];
    z[108]=z[61] + z[1];
    z[109]= - z[6]*z[108]*z[33];
    z[110]= - z[24] + 2*z[39];
    z[110]=z[8]*z[110]*z[43];
    z[111]= - z[21]*z[79];
    z[76]= - z[76] + z[111];
    z[76]=z[15]*z[76];
    z[50]=z[50] - z[1] + z[76];
    z[50]=z[7]*z[15]*z[50];
    z[50]=z[99] + z[100] + z[96] + z[82] - 5*z[107] + z[50] + z[109] + 
    z[110];
    z[50]=z[50]*z[106];
    z[76]=z[25] + z[31];
    z[82]=z[76]*z[18];
    z[82]=z[82] + z[38];
    z[96]=z[82]*z[8];
    z[99]=z[51] + 4*z[96] + z[78] + z[45];
    z[100]=npow(z[8],2);
    z[109]=z[99]*z[100];
    z[110]=4*z[1];
    z[111]=z[17]*z[6]*z[21];
    z[111]=z[111] + z[110];
    z[23]=z[23]*z[111];
    z[111]=z[27] - 4*z[61];
    z[30]=z[111]*z[30];
    z[23]=z[30] + z[23];
    z[23]=z[17]*z[23];
    z[30]=z[103] - z[34];
    z[30]=z[30]*z[37];
    z[34]=7*z[24];
    z[37]= - z[34] + z[40];
    z[37]=z[15]*z[37];
    z[37]= - 26*z[1] + z[37];
    z[37]=z[37]*z[43];
    z[23]=2*z[23] + z[30] + z[37];
    z[23]=z[17]*z[23];
    z[30]=9*z[24];
    z[37]=4*z[31];
    z[40]= - z[44] - z[30] + z[37];
    z[40]=z[40]*z[29];
    z[44]=12*z[24] + z[39];
    z[44]=z[15]*z[44];
    z[44]= - 28*z[1] + z[44];
    z[44]=z[6]*z[44];
    z[111]=z[64]*npow(z[7],2);
    z[23]=z[23] + 6*z[111] - 3*z[109] + z[40] + z[44];
    z[23]=z[17]*z[23];
    z[40]=z[27] - z[61];
    z[40]=z[40]*z[43];
    z[44]=z[1]*z[29];
    z[40]=z[44] + z[40];
    z[40]=2*z[40] + z[107];
    z[40]=z[17]*z[40];
    z[32]= - z[15]*z[32];
    z[32]=z[35] + z[32];
    z[32]=z[15]*z[32];
    z[44]= - z[34] + z[39];
    z[44]=z[15]*z[44];
    z[44]=z[103] + z[44];
    z[44]=z[6]*z[44];
    z[32]=z[40] + z[32] + z[44];
    z[32]=z[17]*z[32];
    z[40]= - z[42]*z[33];
    z[40]=z[40] - z[30] + z[31];
    z[40]=z[15]*z[40];
    z[44]=z[96] + z[51] + z[76];
    z[44]=z[8]*z[44];
    z[85]= - z[6]*z[85];
    z[32]=z[32] + z[44] + z[85] + 6*z[1] + z[40];
    z[32]=z[17]*z[32];
    z[40]=z[70]*z[24];
    z[44]=z[40] + z[38];
    z[85]=z[64]*z[18];
    z[109]=z[85] - z[38];
    z[109]=z[109]*z[18];
    z[109]=z[109] - z[58];
    z[109]=z[109]*z[8];
    z[111]=z[58]*z[6];
    z[112]=z[109] - z[111] + z[44];
    z[112]=z[8]*z[112];
    z[96]=z[96]*z[72];
    z[113]=z[45] + z[24];
    z[114]=z[113]*z[18];
    z[115]=z[38] - z[114];
    z[115]=z[18]*z[115];
    z[115]=z[115] + z[96];
    z[115]=z[8]*z[115];
    z[116]=z[72]*z[1];
    z[115]=z[115] + z[116] - z[74];
    z[115]=z[5]*z[115];
    z[42]=z[74] - z[42];
    z[42]=z[15]*z[42];
    z[32]=z[32] + z[115] + z[112] + 5*z[51] + z[31] + z[42];
    z[32]=z[9]*z[32];
    z[42]=z[86]*z[58];
    z[109]= - z[42] + 12*z[109];
    z[112]=z[31] - z[24];
    z[115]=z[18]*z[112];
    z[115]= - 13*z[38] + 8*z[115] - z[109];
    z[115]=z[8]*z[115];
    z[115]=z[115] + 2*z[51] + z[34] + z[45];
    z[115]=z[8]*z[115];
    z[75]=z[75] + z[38] - z[80];
    z[75]=z[15]*z[75];
    z[75]= - z[87] + z[75];
    z[75]=z[15]*z[75];
    z[80]=6*z[96];
    z[96]=11*z[24];
    z[117]=z[96] + 13*z[31];
    z[117]=z[18]*z[117];
    z[117]= - z[67] + z[117];
    z[117]=z[18]*z[117];
    z[117]=z[117] - z[80];
    z[117]=z[8]*z[117];
    z[118]=7*z[31];
    z[119]=z[28] - z[118];
    z[119]=z[18]*z[119];
    z[117]=z[117] + z[67] + z[119];
    z[117]=z[8]*z[117];
    z[44]= - 2*z[44] + z[74];
    z[44]=z[15]*z[44];
    z[44]=z[44] + z[117];
    z[44]=z[44]*z[84];
    z[117]=z[64]*z[70];
    z[119]=z[7]*z[117];
    z[119]=z[119] + z[24] - z[37];
    z[120]=3*z[7];
    z[119]=z[119]*z[120];
    z[23]=6*z[32] + z[23] + z[119] + z[44] + z[115] - 32*z[47] + 12*z[1]
    + z[75];
    z[23]=z[9]*z[23];
    z[32]=z[67] + z[36];
    z[32]=z[32]*z[18];
    z[32]=z[32] + z[58];
    z[44]=2*z[8];
    z[75]=z[32]*z[44];
    z[75]= - 3*z[62] + z[75];
    z[75]=z[75]*z[44];
    z[115]=z[15]*z[70];
    z[115]=static_cast<T>(3)+ z[115];
    z[63]=z[115]*z[63];
    z[63]=z[87] + z[63];
    z[115]=11*z[38] + z[42];
    z[115]=z[6]*z[115];
    z[63]=z[75] + 2*z[63] + z[115];
    z[63]=z[3]*z[63];
    z[75]= - z[25]*z[79];
    z[75]= - z[36] + z[75];
    z[75]=z[75]*z[33];
    z[115]= - z[25]*z[72]*z[8];
    z[115]=z[36] + z[115];
    z[115]=z[115]*z[44];
    z[119]= - z[36]*z[120];
    z[75]=z[119] + z[115] - z[78] + z[75];
    z[75]=z[2]*z[75];
    z[115]= - z[36] + z[74];
    z[115]=z[115]*z[84];
    z[119]=z[58]*z[8];
    z[121]=z[119] - z[38];
    z[121]=3*z[36] + 8*z[121];
    z[121]=z[7]*z[121];
    z[93]=z[121] + z[115] - 8*z[95] - z[93];
    z[115]=2*z[10];
    z[93]=z[93]*z[115];
    z[121]= - z[58]*z[44];
    z[121]= - z[60] + z[121];
    z[56]=z[121]*z[56];
    z[121]=23*z[24];
    z[56]=z[56] + z[121] - z[45];
    z[56]=z[7]*z[56];
    z[122]=13*z[24];
    z[123]= - z[122] - z[88];
    z[123]=z[6]*z[123];
    z[95]=z[28] + z[95];
    z[95]=z[8]*z[95];
    z[124]= - z[39] - z[68];
    z[124]=z[124]*z[84];
    z[56]=z[93] + z[75] + z[56] + z[124] + z[63] + 8*z[95] - z[21] + 
    z[123];
    z[56]=z[10]*z[56];
    z[63]=z[8]*z[32];
    z[63]=z[63] - z[38] + z[116];
    z[63]=z[63]*z[44];
    z[63]=z[63] - z[68];
    z[63]=z[3]*z[63];
    z[68]=z[38] + z[119];
    z[68]=z[68]*z[44];
    z[75]=z[5]*z[62];
    z[68]=z[75] - z[31] + z[68];
    z[68]=z[7]*z[68];
    z[75]=z[24]*z[3];
    z[93]= - z[47] - z[75];
    z[93]=z[5]*z[93];
    z[95]= - z[3]*z[1];
    z[93]=z[93] - z[19] + z[95];
    z[93]=z[17]*z[93];
    z[95]= - z[72]*z[49];
    z[95]= - z[116] + z[95];
    z[95]=z[8]*z[95];
    z[95]=z[31] + z[95];
    z[119]=z[5]*z[36];
    z[123]=z[17]*z[1];
    z[95]=z[123] + 2*z[95] + z[119];
    z[95]=z[2]*z[95];
    z[104]=z[14]*z[104];
    z[119]=z[100]*z[67];
    z[119]= - z[1] + z[119];
    z[119]=z[7]*z[119];
    z[104]=z[104] + z[19] + z[119];
    z[104]=z[104]*z[106];
    z[106]=z[27] + z[47];
    z[71]= - z[100]*z[71];
    z[100]= - z[5]*z[25];
    z[63]=z[104] + z[95] + z[93] + z[68] + z[100] + z[63] + z[71] - 
    z[106];
    z[63]=z[11]*z[63];
    z[68]=4*z[82];
    z[68]=z[68]*z[79];
    z[71]= - z[34] - z[37];
    z[71]=z[18]*z[71];
    z[71]= - z[60] + z[71];
    z[71]=z[18]*z[71];
    z[71]=3*z[71] - z[68];
    z[71]=z[15]*z[71];
    z[93]= - z[96] - n<T>(19,3)*z[31];
    z[93]=z[93]*z[70];
    z[71]=z[71] - z[46] + z[93];
    z[71]=z[15]*z[71];
    z[71]= - z[88] - 6*z[76] + z[71];
    z[71]=z[3]*z[71];
    z[76]=z[79]*z[64];
    z[93]= - z[76] - z[82];
    z[54]=z[93]*z[54];
    z[93]=z[70]*z[61];
    z[66]= - z[67] + z[66];
    z[66]=z[66]*z[69];
    z[66]= - n<T>(7,3)*z[38] + z[66];
    z[66]=z[5]*z[66];
    z[69]=n<T>(1,3)*z[24];
    z[54]=z[66] + z[54] + z[69] + z[93];
    z[54]=z[7]*z[54];
    z[66]=z[96] + z[77];
    z[93]=z[66]*z[18];
    z[93]=z[93] + 4*z[76];
    z[93]=z[93]*z[15];
    z[95]=14*z[31];
    z[96]=z[95] + z[93];
    z[96]=z[15]*z[96];
    z[40]= - z[38] + z[40];
    z[40]=2*z[40] - z[74];
    z[40]=z[15]*z[40];
    z[100]= - z[46] + n<T>(2,3)*z[111];
    z[100]=z[6]*z[100];
    z[40]=z[100] + n<T>(8,3)*z[24] + z[40];
    z[40]=z[40]*z[84];
    z[40]=4*z[54] + z[40] + z[71] + 10*z[49] + z[96] + n<T>(26,3)*z[47];
    z[40]=z[7]*z[40];
    z[49]=z[8]*z[99];
    z[49]= - z[47] + z[49];
    z[49]=z[49]*z[98];
    z[54]=z[15]*z[1];
    z[49]=n<T>(4,3)*z[54] + z[49];
    z[49]=z[3]*z[49];
    z[29]=z[29]*z[21];
    z[26]=n<T>(37,3)*z[1] - z[26];
    z[26]=z[26]*z[43];
    z[26]=z[29] + z[26];
    z[29]= - z[55]*z[98];
    z[43]=z[27] - n<T>(2,3)*z[75];
    z[43]=z[3]*z[43];
    z[29]=z[29] + z[43];
    z[29]=z[29]*z[84];
    z[43]=z[75]*z[86];
    z[52]=z[52] - z[47];
    z[54]=8*z[52];
    z[54]=z[5]*z[54];
    z[54]=z[43] + z[54];
    z[54]=z[7]*z[54];
    z[26]=10*z[107] + z[54] + z[29] + 2*z[26] + z[49];
    z[26]=z[17]*z[26];
    z[29]= - z[87] - z[45];
    z[29]=z[18]*z[29];
    z[29]= - z[60] + z[29];
    z[29]=z[18]*z[29];
    z[49]= - z[82]*z[79];
    z[29]=z[29] + z[49];
    z[29]=z[15]*z[29];
    z[49]= - z[87] - z[31];
    z[49]=z[18]*z[49];
    z[29]= - z[111] + z[29] - z[46] + z[49];
    z[29]=z[3]*z[29];
    z[46]=z[117] + z[76];
    z[46]=z[15]*z[46];
    z[49]=4*z[116] + z[74];
    z[49]=z[5]*z[49];
    z[25]=z[49] + z[29] + z[51] + z[46] + z[25] - z[31];
    z[29]=6*z[12];
    z[25]=z[25]*z[29];
    z[46]=z[121] + z[95];
    z[46]=z[18]*z[46];
    z[46]=9*z[38] + z[46];
    z[46]=z[18]*z[46];
    z[46]=z[46] + z[68];
    z[46]=z[15]*z[46];
    z[49]=z[66]*z[70];
    z[46]=z[46] + z[60] + z[49];
    z[46]=z[15]*z[46];
    z[42]= - z[67] - z[42];
    z[42]=z[6]*z[42];
    z[42]=z[42] + z[46] + z[122] + 6*z[31];
    z[42]=z[3]*z[42];
    z[46]= - z[78] - z[118];
    z[46]=4*z[46] - z[93];
    z[46]=z[15]*z[46];
    z[37]= - z[78] - z[37];
    z[49]=z[60] - z[83];
    z[49]=z[15]*z[49];
    z[37]=2*z[37] + z[49];
    z[37]=z[37]*z[84];
    z[49]= - z[24] + z[88];
    z[49]=z[6]*z[49];
    z[25]=z[25] + z[37] + z[42] + z[49] + 8*z[1] + z[46];
    z[25]=z[12]*z[25];
    z[37]= - z[5]*z[45];
    z[21]=z[37] - z[21] - z[94];
    z[21]=z[21]*z[115];
    z[37]=10*z[1];
    z[42]=7*z[47];
    z[46]=z[37] - z[42];
    z[46]=z[6]*z[46];
    z[49]=z[53]*z[3];
    z[53]= - z[86]*z[49];
    z[35]=z[35] - z[47];
    z[35]=z[35]*z[84];
    z[21]=z[21] + z[35] + z[46] + z[53];
    z[21]=z[10]*z[21];
    z[35]= - z[6]*z[106]*z[84];
    z[35]= - z[101] + z[35];
    z[35]=z[35]*z[90];
    z[46]=z[101]*z[5];
    z[53]=5*z[46];
    z[35]=z[53] + z[35];
    z[35]=z[12]*z[35];
    z[54]=z[5]*z[6];
    z[55]=z[1] + z[102];
    z[55]=z[55]*z[54];
    z[55]=3*z[101] + z[55];
    z[55]=z[10]*z[55];
    z[53]= - z[53] + z[55];
    z[53]=z[10]*z[53];
    z[46]=z[46]*z[105];
    z[35]=z[46] + z[35] + z[53];
    z[35]=z[16]*z[35];
    z[46]=z[103] + z[97];
    z[46]=z[6]*z[46];
    z[34]=z[34] + z[51];
    z[34]=z[6]*z[34];
    z[34]=13*z[1] + z[34];
    z[34]=z[5]*z[34];
    z[34]=z[46] + z[34];
    z[34]=z[34]*z[90];
    z[46]= - 19*z[1] - 5*z[47];
    z[46]=z[46]*z[54];
    z[34]=z[34] - 5*z[101] + z[46];
    z[34]=z[12]*z[34];
    z[46]=z[86]*z[24];
    z[53]=z[1] - z[46];
    z[55]=2*z[6];
    z[53]=z[53]*z[55];
    z[66]= - z[24] - z[88];
    z[66]=z[6]*z[66];
    z[66]=z[110] + z[66];
    z[66]=z[5]*z[66];
    z[53]=z[53] + z[66];
    z[53]=z[10]*z[53];
    z[66]=z[5]*z[92];
    z[66]=z[101] + z[66];
    z[53]=5*z[66] + z[53];
    z[53]=z[10]*z[53];
    z[66]=npow(z[9],2)*z[19];
    z[34]=z[35] + z[53] + 12*z[66] + z[34];
    z[35]=2*z[16];
    z[34]=z[34]*z[35];
    z[53]= - 6*z[24] - z[51];
    z[53]=z[6]*z[53];
    z[51]= - 6*z[113] - z[51];
    z[51]=z[5]*z[51];
    z[49]=z[51] + z[49] - z[103] + z[53];
    z[29]=z[49]*z[29];
    z[37]=z[37] + z[42];
    z[37]=z[6]*z[37];
    z[42]=z[3]*z[91]*z[60];
    z[49]=11*z[1] + 4*z[47];
    z[49]=z[5]*z[49];
    z[29]=z[29] + 4*z[49] + z[37] + z[42];
    z[29]=z[12]*z[29];
    z[37]= - z[27] - z[48];
    z[37]=z[37]*z[54];
    z[20]= - z[97] - z[20];
    z[20]=z[9]*z[20];
    z[19]=4*z[19] + 3*z[20];
    z[19]=z[9]*z[19];
    z[19]=z[37] + 2*z[19];
    z[20]= - z[2]*z[43];
    z[19]=z[34] + z[21] + z[29] + 4*z[19] + z[20];
    z[19]=z[16]*z[19];
    z[20]=npow(z[1],5);
    z[21]=z[6]*z[20];
    z[21]=z[58] + z[21];
    z[21]=z[21]*z[55];
    z[21]=5*z[62] + z[21];
    z[21]=z[5]*z[21];
    z[29]= - z[87] - z[118];
    z[29]=z[18]*z[29];
    z[32]= - z[32]*z[84];
    z[29]=z[32] + z[67] + z[29];
    z[29]=z[29]*z[73];
    z[32]= - z[91]*z[81];
    z[21]=z[29] + n<T>(1,3)*z[21] + z[32] - n<T>(10,3)*z[24] - z[31];
    z[21]=z[7]*z[21];
    z[29]=z[65] - z[24];
    z[29]=z[29]*z[3];
    z[32]=z[59] - z[29];
    z[34]=n<T>(2,3)*z[5];
    z[32]=z[32]*z[34];
    z[37]= - z[64]*z[120];
    z[37]=n<T>(2,3)*z[59] + z[37];
    z[37]=z[7]*z[37];
    z[32]=z[32] + z[37];
    z[32]=z[17]*z[32];
    z[29]=n<T>(2,3)*z[29];
    z[37]= - z[8]*z[64];
    z[42]=npow(z[3],2);
    z[43]=z[42] + z[91];
    z[43]= - n<T>(1,3)*z[43];
    z[43]=z[58]*z[43];
    z[43]= - z[24] + z[43];
    z[43]=z[43]*z[84];
    z[48]=n<T>(1,3)*z[1] + z[61];
    z[21]=z[32] + z[21] + z[43] + z[29] + 4*z[48] + z[37];
    z[32]=z[58]*z[42];
    z[37]= - z[5]*z[24]*z[72];
    z[37]=z[116] + z[37];
    z[37]=z[2]*z[37];
    z[32]=z[32] + z[37];
    z[37]=z[8]*z[117];
    z[20]= - z[3]*z[20];
    z[20]=z[58] + z[20];
    z[20]=z[3]*z[20];
    z[20]=5*z[36] + 2*z[20];
    z[20]=z[20]*z[34];
    z[20]=z[20] + z[37] - z[89] - z[45] + n<T>(4,3)*z[32];
    z[20]=z[2]*z[20];
    z[32]= - z[7]*z[85];
    z[32]=z[31] + z[32];
    z[32]=z[32]*z[115];
    z[29]= - z[27] - z[29];
    z[29]=z[2]*z[29];
    z[27]=z[12]*z[27];
    z[27]=z[29] + z[27];
    z[27]=z[27]*z[35];
    z[29]=z[16]*z[31];
    z[29]=z[29] - z[85];
    z[34]=z[12] - z[2];
    z[29]=z[4]*z[34]*z[29];
    z[30]=z[12]*z[30];
    z[20]=6*z[29] + z[27] + z[32] + z[30] + 2*z[21] + z[20];
    z[20]=z[4]*z[20];
    z[21]= - z[78] - z[77];
    z[21]=z[18]*z[21];
    z[27]= - z[70]*z[39];
    z[21]=z[21] + z[27];
    z[21]=z[8]*z[21];
    z[21]=z[21] + z[77] - z[39];
    z[21]=z[8]*z[21];
    z[27]=z[87] - z[45];
    z[27]=z[18]*z[27];
    z[27]=7*z[38] + z[27];
    z[27]=z[18]*z[27];
    z[27]=z[27] + z[80];
    z[27]=z[8]*z[27];
    z[29]= - z[41] - 11*z[31];
    z[29]=z[18]*z[29];
    z[27]=z[27] - z[38] + z[29];
    z[27]=z[8]*z[27];
    z[24]=z[65] + z[24];
    z[24]=z[27] + z[118] - n<T>(2,3)*z[24];
    z[24]=z[3]*z[24];
    z[21]=z[24] + z[21] + n<T>(28,3)*z[47] + z[103] + n<T>(4,3)*z[61];
    z[21]=z[21]*z[84];
    z[24]= - z[15]*z[85];
    z[24]= - z[69] + z[24];
    z[24]=z[15]*z[24];
    z[24]=z[24] - z[46];
    z[27]=z[38] - 4*z[114] + z[109];
    z[27]=z[8]*z[27];
    z[28]=z[28] + z[88];
    z[27]=3*z[28] + z[27];
    z[27]=z[8]*z[27];
    z[24]=n<T>(4,3)*z[75] + 4*z[24] + z[27];
    z[24]=z[3]*z[24];
    z[27]= - z[78] - z[39];
    z[27]=z[6]*z[27];
    z[28]= - z[39]*z[44];
    z[27]=z[27] + z[28];
    z[27]=z[27]*z[44];
    z[28]=z[67] - z[57];
    z[28]=z[7]*z[28];
    z[29]=z[17]*z[52];
    z[30]= - z[3]*z[36];
    z[28]=z[29] + z[30] + z[28];
    z[28]=z[7]*z[28];
    z[28]=2*z[108] + z[47] + z[28];
    z[28]=z[13]*z[28];
    z[29]= - z[15]*z[112];
    z[29]=z[1] + z[29];
    z[29]=z[29]*z[33];
    z[30]=n<T>(4,3)*z[47] + n<T>(74,3)*z[1] + 3*z[61];
    z[30]=z[6]*z[30];

    r += z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26]
       + z[27] + 4*z[28] + z[29] + z[30] + z[40] + z[50] + z[56] + 6*
      z[63];
 
    return r;
}

template double qqb_2lLC_r217(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lLC_r217(const std::array<dd_real,31>&);
#endif
