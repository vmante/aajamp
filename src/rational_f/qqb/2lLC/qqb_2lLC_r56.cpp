#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r56(const std::array<T,31>& k) {
  T z[100];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[13];
    z[6]=k[26];
    z[7]=k[4];
    z[8]=k[8];
    z[9]=k[12];
    z[10]=k[5];
    z[11]=k[7];
    z[12]=k[2];
    z[13]=k[11];
    z[14]=k[21];
    z[15]=k[15];
    z[16]=k[14];
    z[17]=k[27];
    z[18]=npow(z[10],2);
    z[19]=45*z[18];
    z[20]=z[7]*z[10];
    z[21]= - z[19] + 19*z[20];
    z[21]=z[21]*z[7];
    z[22]=19*z[8];
    z[23]=npow(z[7],2);
    z[24]=z[22]*z[23];
    z[25]=z[24]*z[18];
    z[21]=z[21] - z[25];
    z[21]=z[21]*z[8];
    z[25]=57*z[7];
    z[26]= - z[25] + 83*z[10];
    z[26]=z[26]*z[7];
    z[27]= - z[21] - 51*z[18] + z[26];
    z[27]=z[8]*z[27];
    z[28]=z[20] - z[18];
    z[29]= - z[7]*z[28];
    z[30]=npow(z[10],3);
    z[29]= - z[30] + z[29];
    z[31]=38*z[11];
    z[29]=z[29]*z[31];
    z[26]=z[29] - 52*z[18] + z[26];
    z[26]=z[11]*z[26];
    z[29]=z[18]*z[7];
    z[32]=z[29] - z[30];
    z[33]= - z[5]*z[32]*z[31];
    z[26]=z[33] + z[27] + z[26];
    z[26]=z[5]*z[26];
    z[27]=npow(z[7],3);
    z[33]=z[27]*z[11];
    z[33]=z[33] - z[23];
    z[33]=z[33]*z[4];
    z[34]=z[11]*z[7];
    z[35]=2*z[7];
    z[36]=z[35] + z[10];
    z[37]=z[34]*z[36];
    z[37]=6*z[37];
    z[38]=z[4]*z[7];
    z[39]=6*z[38];
    z[40]=z[2]*z[4];
    z[41]=6*z[40];
    z[42]= - z[41] - static_cast<T>(89)- z[39];
    z[42]=z[2]*z[42];
    z[42]=z[42] + 12*z[33] + z[37] + 116*z[10] + 39*z[7];
    z[42]=z[6]*z[42];
    z[43]=2*z[10];
    z[44]=13*z[7];
    z[45]=z[43] + z[44];
    z[46]=z[7] - z[10];
    z[47]= - z[7]*z[46];
    z[47]= - z[18] + z[47];
    z[47]=z[47]*z[31];
    z[45]=3*z[45] + z[47];
    z[45]=z[11]*z[45];
    z[47]=z[24]*z[10];
    z[47]=z[47] + 45*z[20];
    z[47]=z[47]*z[8];
    z[48]= - 29*z[10] + 89*z[7];
    z[48]=2*z[48] + z[47];
    z[48]=z[8]*z[48];
    z[49]=5*z[7];
    z[50]=z[49] + z[24];
    z[51]=z[23]*z[11];
    z[52]= - 4*z[50] - 51*z[51];
    z[52]=z[4]*z[52];
    z[53]=static_cast<T>(1)- z[34];
    z[53]=z[53]*z[40];
    z[54]=z[35] - z[51];
    z[54]=z[4]*z[54];
    z[55]=z[11]*z[46];
    z[54]=z[54] - static_cast<T>(1)+ z[55];
    z[54]=z[2]*z[54];
    z[55]=z[23]*z[4];
    z[54]= - 2*z[55] + z[54];
    z[56]=z[44]*z[2];
    z[57]=z[6]*z[56];
    z[54]=38*z[54] + z[57];
    z[54]=z[13]*z[54];
    z[26]=z[54] + z[42] + z[26] + 39*z[53] + z[52] + z[45] - static_cast<T>(89)+ z[48];
    z[26]=z[13]*z[26];
    z[42]=z[18]*z[8];
    z[45]=z[42] - z[10];
    z[45]=z[45]*z[8];
    z[45]=z[45] - 3;
    z[48]=z[11]*z[8];
    z[45]=z[45]*z[48];
    z[46]=z[46]*z[8];
    z[52]=z[46] + 1;
    z[53]=npow(z[8],2);
    z[52]=z[52]*z[53];
    z[54]=z[8]*z[7];
    z[57]=z[54] - 5;
    z[58]=z[4]*z[8];
    z[57]=z[57]*z[58];
    z[45]=z[45] + z[52] - z[57];
    z[52]=2*z[5];
    z[57]=z[11]*z[10];
    z[59]=z[57] - 1;
    z[60]=z[52]*z[59];
    z[61]= - z[60] + 3*z[11] - 5*z[4];
    z[61]=z[5]*z[61];
    z[61]=z[61] + z[45];
    z[62]=19*z[1];
    z[61]=z[61]*z[62];
    z[63]=38*z[8];
    z[64]=z[28]*z[63];
    z[65]=31*z[10];
    z[66]=19*z[7];
    z[64]= - z[66] + z[64] + z[65];
    z[64]=z[64]*z[8];
    z[67]=13*z[38];
    z[68]= - z[67] - static_cast<T>(45)+ z[64];
    z[68]=z[8]*z[68];
    z[69]=z[31]*z[18];
    z[70]=7*z[10];
    z[71]= - z[70] - z[69];
    z[71]=z[11]*z[71];
    z[72]=z[18]*z[11];
    z[73]=z[72] - z[10];
    z[74]=z[5]*z[73];
    z[71]=38*z[74] - 31*z[38] + static_cast<T>(7)+ z[71];
    z[71]=z[5]*z[71];
    z[63]=z[63]*z[30];
    z[63]= - z[63] + 31*z[18];
    z[63]=z[63]*z[8];
    z[63]=z[63] - z[70];
    z[63]=z[63]*z[48];
    z[61]=z[61] + z[71] - z[63] + z[68];
    z[61]=z[1]*z[61];
    z[68]=19*z[11];
    z[71]=z[68]*z[30];
    z[74]=7*z[18] + z[71];
    z[74]=z[11]*z[74];
    z[75]= - z[11]*z[30];
    z[75]=z[18] + z[75];
    z[76]=19*z[5];
    z[75]=z[75]*z[76];
    z[77]=3*z[55];
    z[74]=z[75] - z[77] + z[44] + z[74];
    z[74]=z[74]*z[52];
    z[75]=2*z[4];
    z[78]=z[27]*z[75];
    z[79]=4*z[7];
    z[80]=3*z[10] + z[79];
    z[80]=z[7]*z[80];
    z[78]=z[78] + z[18] + z[80];
    z[80]=6*z[5];
    z[78]=z[78]*z[80];
    z[81]=3*z[40];
    z[82]=z[81] + 35;
    z[83]=3*z[38];
    z[84]=z[83] - z[82];
    z[85]=2*z[2];
    z[84]=z[84]*z[85];
    z[86]=12*z[55];
    z[87]=63*z[7];
    z[78]=z[78] + z[84] - z[86] + 65*z[10] - z[87];
    z[78]=z[14]*z[78];
    z[84]=npow(z[10],4);
    z[88]= - z[84]*z[68];
    z[88]= - 7*z[30] + z[88];
    z[88]=z[11]*z[88];
    z[89]=z[11]*z[84];
    z[89]= - z[30] + z[89];
    z[76]=z[89]*z[76];
    z[76]=z[76] + z[88] - 3*z[18] - 16*z[20];
    z[76]=z[5]*z[76];
    z[88]=6*z[18];
    z[71]=z[88] - z[71];
    z[71]=z[11]*z[71];
    z[29]=z[29]*z[5];
    z[29]=z[29] - z[18];
    z[89]=z[72]*z[2];
    z[90]= - 3*z[89] + 16*z[29];
    z[90]=z[13]*z[90];
    z[71]=z[90] + z[76] - 44*z[10] + z[71];
    z[76]=2*z[13];
    z[71]=z[71]*z[76];
    z[90]=z[8] - z[5];
    z[90]=z[1]*z[38]*z[90];
    z[91]=z[5]*z[7];
    z[90]=z[90] + z[54] + z[91];
    z[90]=z[1]*z[90];
    z[91]=z[20]*z[5];
    z[90]=z[90] + z[10] - z[91];
    z[92]=z[7] + z[10];
    z[93]=13*z[2];
    z[92]=z[92]*z[93];
    z[93]=z[14]*z[92];
    z[94]=38*z[13];
    z[95]=z[29]*z[94];
    z[90]=z[95] + z[93] + 38*z[90];
    z[90]=z[9]*z[90];
    z[93]=z[84]*z[22];
    z[93]= - z[93] + 12*z[30];
    z[93]=z[93]*z[8];
    z[93]=z[93] + 13*z[18];
    z[93]=z[93]*z[8];
    z[69]= - z[93] + z[69];
    z[69]=z[11]*z[69];
    z[95]=z[32]*z[22];
    z[95]=z[95] + 12*z[18];
    z[95]=z[95]*z[8];
    z[95]=z[95] - 25*z[7];
    z[96]=13*z[10] + z[95];
    z[96]=z[8]*z[96];
    z[61]=z[90] + z[71] + z[61] + z[78] + z[74] + 32*z[38] + z[69] + 102
    + z[96];
    z[61]=z[9]*z[61];
    z[25]=z[25] - z[70];
    z[25]=z[25]*z[7];
    z[21]=z[21] + z[88] + z[25];
    z[21]=z[8]*z[21];
    z[69]=z[20] + 2*z[18];
    z[69]=z[69]*z[7];
    z[69]=z[69] + z[30];
    z[71]=z[69]*z[31];
    z[25]=z[71] - 64*z[18] + z[25];
    z[25]=z[11]*z[25];
    z[71]=z[40] - 1;
    z[74]= - z[38] + z[71];
    z[74]=z[2]*z[74];
    z[78]=z[10] + z[79];
    z[21]=12*z[74] + z[86] + z[25] + 6*z[78] + z[21];
    z[21]=z[5]*z[21];
    z[25]= - z[79] + 3*z[51];
    z[25]=z[4]*z[25];
    z[74]=z[4]*z[11];
    z[78]= - z[7] + z[51];
    z[78]=z[78]*z[74];
    z[86]=z[11]*z[36];
    z[86]= - static_cast<T>(1)+ z[86];
    z[86]=z[11]*z[86];
    z[78]=z[86] + z[78];
    z[78]=z[2]*z[78];
    z[34]=3*z[34];
    z[25]=z[78] + z[25] + static_cast<T>(2)+ z[34];
    z[25]=z[2]*z[25];
    z[20]=z[20] + z[18];
    z[78]=z[7]*z[20];
    z[86]=z[23]*z[8];
    z[88]=z[18]*z[86];
    z[78]=z[78] + z[88];
    z[78]=z[8]*z[78];
    z[69]=z[11]*z[69];
    z[69]=z[69] - z[18] + z[78];
    z[69]=z[5]*z[69];
    z[78]=z[7]*z[36];
    z[88]=z[10]*z[86];
    z[78]=z[78] + z[88];
    z[78]=z[8]*z[78];
    z[88]=z[43] + z[7];
    z[88]=z[88]*z[7];
    z[88]=z[88] + z[18];
    z[90]=z[11]*z[88];
    z[25]=z[69] + z[25] + z[77] + z[90] + 3*z[7] + z[78];
    z[69]=19*z[14];
    z[25]=z[25]*z[69];
    z[78]=z[36]*z[68];
    z[78]=z[78] + 32;
    z[90]=npow(z[11],2);
    z[96]=z[78]*z[90];
    z[97]=z[68]*z[23];
    z[98]=z[97] + 32*z[7];
    z[99]=z[11]*z[98];
    z[99]=static_cast<T>(6)+ z[99];
    z[99]=z[99]*z[74];
    z[96]=z[96] + z[99];
    z[96]=z[2]*z[96];
    z[65]=z[65] + 107*z[7];
    z[65]=z[11]*z[65];
    z[65]= - static_cast<T>(6)+ z[65];
    z[65]=z[11]*z[65];
    z[99]=77*z[7] + 76*z[51];
    z[99]=z[11]*z[99];
    z[99]= - static_cast<T>(57)+ z[99];
    z[99]=z[4]*z[99];
    z[65]=z[96] + z[65] + z[99];
    z[65]=z[2]*z[65];
    z[44]= - 10*z[10] - z[44];
    z[44]=2*z[44] - z[47];
    z[44]=z[8]*z[44];
    z[31]=z[88]*z[31];
    z[31]=z[31] - 19*z[10] + 62*z[7];
    z[31]=z[11]*z[31];
    z[47]=2*z[50] + 57*z[51];
    z[47]=z[47]*z[75];
    z[21]=z[25] + z[21] + z[65] + z[47] + z[31] + static_cast<T>(38)+ z[44];
    z[21]=z[14]*z[21];
    z[25]= - z[10] - z[42];
    z[25]=z[25]*z[48];
    z[31]=static_cast<T>(2)- z[46];
    z[31]=z[8]*z[31];
    z[42]=static_cast<T>(3)- z[54];
    z[42]=z[4]*z[42];
    z[25]=z[42] + z[31] + z[25];
    z[25]=z[1]*z[25];
    z[31]=2*z[8];
    z[28]= - z[28]*z[31];
    z[28]=z[28] + z[10] - z[35];
    z[28]=z[8]*z[28];
    z[30]=2*z[30];
    z[31]= - z[8]*z[30];
    z[31]=z[18] + z[31];
    z[31]=z[31]*z[48];
    z[25]=z[25] - 4*z[40] + z[38] + z[31] + static_cast<T>(1)+ z[28];
    z[25]=z[1]*z[25];
    z[28]= - z[8]*z[32];
    z[20]=z[28] - z[20];
    z[20]=z[8]*z[20];
    z[28]= - z[8]*z[84];
    z[28]=z[30] + z[28];
    z[28]=z[8]*z[28];
    z[28]= - z[18] + z[28];
    z[28]=z[11]*z[28];
    z[30]=static_cast<T>(2)+ z[40];
    z[30]=z[2]*z[30];
    z[20]=z[25] + z[30] + z[28] - z[7] + z[20];
    z[25]=19*z[6];
    z[20]=z[20]*z[25];
    z[28]= - z[45]*z[62];
    z[30]= - static_cast<T>(50)- z[64];
    z[30]=z[8]*z[30];
    z[31]=static_cast<T>(37)+ 13*z[54];
    z[31]=z[4]*z[31];
    z[28]=z[28] + z[31] + z[30] + z[63];
    z[28]=z[1]*z[28];
    z[30]=z[10] - z[95];
    z[30]=z[8]*z[30];
    z[31]=12*z[7] + z[93];
    z[31]=z[11]*z[31];
    z[20]=z[20] + z[28] - z[41] + z[39] + z[31] - static_cast<T>(101)+ z[30];
    z[20]=z[6]*z[20];
    z[28]=npow(z[2],2);
    z[30]=z[71]*z[28];
    z[31]=z[1]*z[4];
    z[32]=z[31]*z[28];
    z[35]= - z[32] + 2*z[30];
    z[35]=z[35]*z[1];
    z[38]=z[40] - 2;
    z[38]=z[38]*z[2];
    z[39]=z[38] + z[10];
    z[28]=z[39]*z[28];
    z[35]=z[35] - z[28];
    z[42]= - z[35]*z[25];
    z[44]=z[40] - 3;
    z[44]=z[44]*z[2];
    z[43]=z[44] + z[43];
    z[43]=6*z[43];
    z[44]= - z[2]*z[43];
    z[45]=z[59] - z[40];
    z[45]=z[45]*z[62];
    z[46]= - static_cast<T>(12)+ 25*z[40];
    z[47]=z[2]*z[46];
    z[47]=z[47] + z[45];
    z[47]=z[1]*z[47];
    z[42]=z[42] + z[44] + z[47];
    z[42]=z[6]*z[42];
    z[30]= - z[52]*z[30];
    z[32]=z[5]*z[32];
    z[30]=z[30] + z[32];
    z[30]=z[1]*z[30];
    z[32]=z[6]*z[35];
    z[28]=z[5]*z[28];
    z[28]=z[32] + z[28] + z[30];
    z[28]=z[3]*z[28];
    z[30]=z[5]*z[2];
    z[32]= - z[46]*z[30];
    z[35]= - z[5]*z[45];
    z[32]=z[32] + z[35];
    z[32]=z[1]*z[32];
    z[35]=z[30]*z[43];
    z[28]=19*z[28] + z[42] + z[35] + z[32];
    z[28]=z[3]*z[28];
    z[32]=z[11] - z[4];
    z[32]=3*z[32] - z[60];
    z[32]=z[32]*z[62];
    z[32]=z[32] - 38*z[30] + 31*z[40] - static_cast<T>(31)- 7*z[57];
    z[32]=z[1]*z[5]*z[32];
    z[35]= - static_cast<T>(2)+ z[57];
    z[42]= - z[11] + z[75];
    z[42]=z[1]*z[42];
    z[35]=57*z[42] + 7*z[35] - 113*z[40];
    z[35]=z[1]*z[35];
    z[42]= - z[81] + z[59];
    z[42]=z[1]*z[42];
    z[43]= - static_cast<T>(1)+ 5*z[40];
    z[43]=z[2]*z[43];
    z[42]=z[43] + z[42];
    z[42]=z[1]*z[42];
    z[43]=static_cast<T>(1)- 2*z[40];
    z[43]=z[2]*z[43];
    z[43]=z[10] + z[43];
    z[43]=z[2]*z[43];
    z[42]=z[43] + z[42];
    z[25]=z[42]*z[25];
    z[42]=static_cast<T>(1)+ 37*z[40];
    z[42]=z[2]*z[42];
    z[25]=z[25] + z[35] + z[70] + z[42];
    z[25]=z[6]*z[25];
    z[30]=z[71]*z[30];
    z[35]=z[9]*z[10];
    z[25]=z[28] + 38*z[35] + z[25] - 12*z[30] + z[32];
    z[25]=z[3]*z[25];
    z[28]=z[53]*z[4];
    z[30]=npow(z[8],3);
    z[28]=z[28] - z[30];
    z[30]=z[53] + z[58];
    z[30]=z[14]*z[30];
    z[30]=z[30] + z[28];
    z[30]=z[14]*z[30];
    z[32]= - z[58]*z[76];
    z[28]=z[32] - z[28];
    z[28]=z[13]*z[28];
    z[28]=z[30] + z[28];
    z[28]=z[12]*z[28];
    z[30]=static_cast<T>(13)+ 38*z[54];
    z[30]=z[30]*z[58];
    z[32]=z[36]*z[8];
    z[35]=z[32] - 1;
    z[36]=19*z[53];
    z[35]=z[35]*z[36];
    z[30]=z[30] - z[35];
    z[35]=z[54] - 1;
    z[35]=z[35]*z[75];
    z[32]= - static_cast<T>(2)- z[32];
    z[32]=z[8]*z[32];
    z[32]=z[32] - z[35];
    z[32]=z[32]*z[69];
    z[32]=z[32] - z[30];
    z[32]=z[14]*z[32];
    z[35]=z[8] + z[35];
    z[35]=z[35]*z[94];
    z[30]=z[35] + z[30];
    z[30]=z[13]*z[30];
    z[28]=19*z[28] + z[32] + z[30];
    z[28]=z[12]*z[28];
    z[30]= - z[49] + z[86];
    z[30]=z[4]*z[30];
    z[32]=z[10] + z[86];
    z[32]=z[8]*z[32];
    z[30]=z[40] + z[32] + z[30];
    z[30]=z[30]*z[69];
    z[32]=26*z[10] + z[66] - z[24];
    z[32]=z[32]*z[8];
    z[35]=static_cast<T>(70)+ z[32];
    z[35]=z[8]*z[35];
    z[24]=z[87] - z[24];
    z[24]=z[24]*z[8];
    z[36]= - static_cast<T>(77)- z[24];
    z[36]=z[4]*z[36];
    z[30]=z[30] + z[35] + z[36];
    z[30]=z[14]*z[30];
    z[35]=z[79] - z[86];
    z[35]=z[4]*z[35];
    z[35]= - z[40] - z[54] + z[35];
    z[35]=z[35]*z[94];
    z[32]= - static_cast<T>(184)- z[32];
    z[32]=z[8]*z[32];
    z[24]=static_cast<T>(103)+ z[24];
    z[24]=z[4]*z[24];
    z[24]=z[35] + z[32] + z[24];
    z[24]=z[13]*z[24];
    z[24]=z[28] + z[30] + z[24];
    z[24]=z[12]*z[24];
    z[28]=z[81] + static_cast<T>(48)+ z[67];
    z[28]=z[28]*z[85];
    z[27]= - z[4]*z[27];
    z[23]= - z[23] + z[27];
    z[23]=z[23]*z[80];
    z[27]= - z[9]*z[92];
    z[30]=z[2]*z[10];
    z[19]= - z[19] + 32*z[30];
    z[30]=z[3]*z[19];
    z[23]=z[30] + z[27] + z[23] + z[28] + 6*z[55] - 135*z[10] - 26*z[7];
    z[23]=z[9]*z[23];
    z[27]=z[67] - z[82];
    z[27]=z[27]*z[85];
    z[28]=z[13]*z[56];
    z[27]=z[28] + z[27] + 6*z[33] + z[37] + 103*z[10] - 38*z[7];
    z[27]=z[13]*z[27];
    z[28]= - z[55] + z[39];
    z[28]=z[5]*z[28];
    z[30]= - z[11]*z[38];
    z[28]=z[30] + z[28];
    z[30]=z[8]*z[10];
    z[32]=z[34] + static_cast<T>(19)- 16*z[30];
    z[33]=z[11]*z[77];
    z[28]=2*z[32] + z[33] + 3*z[28];
    z[23]=2*z[28] + z[27] + z[23];
    z[23]=z[16]*z[23];
    z[18]= - z[5]*z[18];
    z[18]=z[18] + z[10] + 2*z[72];
    z[18]=z[68]*z[18];
    z[22]=z[10]*z[22];
    z[18]=z[41] - z[83] + static_cast<T>(13)+ z[22] + z[18];
    z[18]=z[5]*z[18];
    z[22]=static_cast<T>(41)+ 38*z[57];
    z[22]=z[11]*z[22];
    z[18]=z[18] + 10*z[4] + 57*z[8] + z[22];
    z[22]= - z[4]*z[98];
    z[22]= - z[78] + z[22];
    z[22]=z[2]*z[90]*z[22];
    z[27]= - 8*z[7] - z[97];
    z[27]=z[27]*z[74];
    z[28]=z[70] - 69*z[7];
    z[28]=z[11]*z[28];
    z[28]=static_cast<T>(32)+ z[28];
    z[28]=z[11]*z[28];
    z[22]=z[22] + z[28] + 4*z[27];
    z[22]=z[2]*z[22];
    z[27]= - 57*z[55] - z[70] + 45*z[7];
    z[27]=z[11]*z[27];
    z[19]= - z[16]*z[13]*z[19];
    z[28]= - z[10]*z[94];
    z[19]=z[19] + z[28] + z[22] + z[27];
    z[19]=z[15]*z[19];
    z[22]=z[89] + z[29];
    z[22]=z[13]*z[22];
    z[27]=z[2]*z[57];
    z[22]=z[22] - z[91] + z[27] - z[73];
    z[22]=z[13]*z[22];
    z[27]= - static_cast<T>(1)+ z[30];
    z[28]= - z[5]*z[10];
    z[22]=z[22] + 2*z[27] + z[28];
    z[22]=z[17]*z[22];
    z[27]=z[5]*z[31];

    r += 2*z[18] + z[19] + z[20] + z[21] + 32*z[22] + z[23] + z[24] + 
      z[25] + z[26] - 31*z[27] + z[61];
 
    return r;
}

template double qqb_2lLC_r56(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r56(const std::array<dd_real,31>&);
#endif
