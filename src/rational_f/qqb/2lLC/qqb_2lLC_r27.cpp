#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r27(const std::array<T,31>& k) {
  T z[49];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[5];
    z[7]=k[7];
    z[8]=k[24];
    z[9]=k[3];
    z[10]=k[9];
    z[11]=k[8];
    z[12]=k[2];
    z[13]=k[11];
    z[14]=k[14];
    z[15]=k[15];
    z[16]=k[27];
    z[17]=z[7]*z[2];
    z[17]=z[17] + 1;
    z[18]=3*z[7];
    z[19]= - z[17]*z[18];
    z[20]=z[14]*z[2];
    z[21]=npow(z[2],2);
    z[22]=z[21]*z[14];
    z[23]=z[4]*z[22];
    z[23]= - 98*z[20] - 57*z[23];
    z[23]=z[4]*z[23];
    z[24]=19*z[4];
    z[25]=z[24]*z[5];
    z[26]=z[22]*z[25];
    z[19]=z[26] + z[19] + z[23];
    z[19]=z[5]*z[19];
    z[23]=npow(z[4],2);
    z[26]=npow(z[7],2);
    z[27]=z[26] + z[23];
    z[27]=z[14]*z[27];
    z[28]= - z[5]*z[23]*z[20];
    z[28]=z[28] + z[27];
    z[29]=z[5]*z[2];
    z[29]=z[29] - 1;
    z[30]=z[29]*z[13];
    z[18]=16*z[30] + z[18] - z[24];
    z[18]=z[4]*z[18];
    z[31]=19*z[5];
    z[23]=z[2]*z[23]*z[31];
    z[18]=z[23] + z[18];
    z[18]=z[13]*z[18];
    z[18]=19*z[28] + z[18];
    z[18]=z[6]*z[18];
    z[23]=19*z[7];
    z[28]= - z[22]*z[23];
    z[28]=z[28] + static_cast<T>(19)+ 16*z[20];
    z[28]=z[7]*z[28];
    z[32]=3*z[26];
    z[33]= - z[21]*z[32];
    z[34]=z[4]*z[2];
    z[33]=z[33] - 16*z[34];
    z[33]=z[5]*z[33];
    z[28]=z[33] + z[28] - 44*z[4];
    z[28]=z[13]*z[28];
    z[33]= - static_cast<T>(1)+ z[20];
    z[33]=z[33]*z[23];
    z[33]= - 63*z[14] + z[33];
    z[33]=z[7]*z[33];
    z[34]=z[20]*z[24];
    z[34]=z[34] + 21*z[14] - z[7];
    z[34]=z[4]*z[34];
    z[18]=z[18] + z[28] + z[19] + z[33] + 3*z[34];
    z[18]=z[6]*z[18];
    z[19]=static_cast<T>(4)- 15*z[20];
    z[19]=z[3]*z[19];
    z[28]=z[3]*z[2];
    z[33]=z[7]*z[28];
    z[19]= - 8*z[33] + 2*z[19] + 3*z[14];
    z[33]=2*z[2];
    z[34]=z[33] - z[22];
    z[34]=z[3]*z[34];
    z[35]=z[21]*z[3];
    z[36]=z[2] - z[35];
    z[36]=z[7]*z[36];
    z[34]=z[36] - static_cast<T>(1)+ z[34];
    z[36]=19*z[13];
    z[34]=z[34]*z[36];
    z[19]=2*z[19] + z[34];
    z[19]=z[13]*z[19];
    z[34]=npow(z[13],2);
    z[37]=z[34]*z[7];
    z[27]= - z[37] - z[27];
    z[37]=z[34]*z[6];
    z[38]=z[4]*z[7];
    z[39]=z[38]*z[37];
    z[27]=19*z[27] - 3*z[39];
    z[27]=z[6]*z[27];
    z[39]=z[3]*z[14];
    z[40]=z[39]*z[21];
    z[41]=z[40] + 2*z[20];
    z[42]= - z[41]*z[23];
    z[43]=z[20]*z[3];
    z[44]=z[14] + z[43];
    z[42]=60*z[44] + z[42];
    z[42]=z[7]*z[42];
    z[41]= - z[41]*z[24];
    z[43]= - 11*z[14] - 10*z[43];
    z[41]=6*z[43] + z[41];
    z[41]=z[4]*z[41];
    z[40]= - z[31]*z[40];
    z[43]= - static_cast<T>(1)+ 20*z[20];
    z[43]=z[3]*z[43];
    z[40]=3*z[43] + z[40];
    z[40]=z[5]*z[40];
    z[19]=z[27] + z[19] + z[40] + z[41] - 152*z[39] + z[42];
    z[19]=z[9]*z[19];
    z[27]=z[28] - 1;
    z[27]= - z[4]*z[27];
    z[27]= - z[3] + z[27];
    z[27]=z[27]*z[31];
    z[40]=z[26]*z[4];
    z[25]= - z[7]*z[25];
    z[25]= - 16*z[40] + z[25];
    z[25]=z[6]*z[25];
    z[25]=z[25] + 16*z[38] + z[27];
    z[25]=z[1]*z[25];
    z[27]= - z[24]*z[35];
    z[41]= - static_cast<T>(11)- 30*z[28];
    z[27]=2*z[41] + z[27];
    z[27]=z[4]*z[27];
    z[35]=z[4]*z[35];
    z[35]=z[28] + z[35];
    z[35]=z[35]*z[31];
    z[32]= - z[32] + 22*z[38];
    z[32]=z[6]*z[32];
    z[41]= - 21*z[3] + 20*z[7];
    z[25]=z[25] + z[32] + z[35] + 3*z[41] + z[27];
    z[25]=z[5]*z[25];
    z[27]=19*z[3];
    z[32]=npow(z[5],2)*z[27]*z[9];
    z[25]=z[32] + z[25];
    z[25]=z[1]*z[25];
    z[32]= - z[9]*z[3];
    z[32]=z[32] + 1;
    z[32]=z[5]*z[32];
    z[35]=z[6]*z[14];
    z[41]= - z[4]*z[35];
    z[32]=z[41] + z[3] + z[32];
    z[32]=z[9]*z[32];
    z[41]=z[35] - 1;
    z[42]=z[9]*z[5];
    z[42]= - static_cast<T>(1)+ z[42];
    z[42]=z[1]*z[3]*z[42];
    z[32]=z[42] + z[32] - z[41];
    z[32]=z[10]*z[32];
    z[42]= - static_cast<T>(1)- z[28];
    z[42]=z[7]*z[42];
    z[43]=z[13]*z[35];
    z[42]=z[42] + z[43];
    z[42]=z[9]*z[42];
    z[41]=z[42] - z[28] - z[41];
    z[41]=z[15]*z[41];
    z[32]=z[32] + z[41];
    z[41]=npow(z[2],3);
    z[42]=z[41]*z[14];
    z[43]= - 3*z[42] + 2*z[21];
    z[44]=z[14]*npow(z[2],4);
    z[44]=z[44] - z[41];
    z[44]=z[44]*z[3];
    z[45]= - z[44] + z[43];
    z[45]=z[45]*z[24];
    z[43]=z[43]*z[27];
    z[43]=z[43] + 49*z[2] - 106*z[22];
    z[43]=2*z[43] + z[45];
    z[43]=z[4]*z[43];
    z[45]=z[42] - z[21];
    z[45]=z[45]*z[3];
    z[46]=z[44] + 2*z[42];
    z[47]= - z[21] + z[46];
    z[47]=z[4]*z[47];
    z[47]=z[47] + 2*z[22] + z[45];
    z[31]=z[47]*z[31];
    z[47]= - 19*z[22] + 15*z[2];
    z[48]=4*z[3];
    z[47]=z[47]*z[48];
    z[47]=z[47] - 139*z[20];
    z[31]=z[31] + z[43] + static_cast<T>(25)+ z[47];
    z[31]=z[5]*z[31];
    z[43]= - 5*z[21] + 6*z[42];
    z[43]=z[43]*z[27];
    z[46]= - z[46]*z[23];
    z[43]=z[46] + z[43] - 19*z[2] + 130*z[22];
    z[43]=z[7]*z[43];
    z[39]=z[39]*z[41];
    z[41]=z[42] + z[44];
    z[41]=z[7]*z[41];
    z[41]= - z[39] + z[41];
    z[41]=z[41]*z[36];
    z[20]=13*z[20];
    z[42]=76*z[22];
    z[44]=41*z[2] - z[42];
    z[44]=z[3]*z[44];
    z[41]=z[41] + z[43] + z[44] - static_cast<T>(32)- z[20];
    z[41]=z[13]*z[41];
    z[43]= - z[26] - z[38];
    z[43]=z[6]*z[43];
    z[40]=z[6]*z[40];
    z[38]= - z[38] + z[40];
    z[38]=z[1]*z[38];
    z[38]=z[38] - z[7] + z[43];
    z[38]=z[1]*z[38];
    z[21]=z[21]*z[7];
    z[40]=z[33] - z[21];
    z[40]=z[13]*z[40];
    z[26]= - z[2]*z[26];
    z[21]=z[2] - z[21];
    z[21]=z[13]*z[7]*z[21];
    z[21]=z[26] + z[21];
    z[21]=z[6]*z[21];
    z[17]=z[38] + z[21] + z[40] - z[17];
    z[21]=16*z[8];
    z[17]=z[17]*z[21];
    z[26]= - z[39] - z[33] - z[22];
    z[23]=z[26]*z[23];
    z[26]=3*z[2] + z[42];
    z[26]=z[3]*z[26];
    z[20]=z[23] + z[26] + static_cast<T>(35)+ z[20];
    z[20]=z[7]*z[20];
    z[22]=z[45] - z[33] + 3*z[22];
    z[22]=z[22]*z[24];
    z[22]=z[22] - static_cast<T>(28)- z[47];
    z[22]=z[4]*z[22];
    z[23]= - z[12] - z[9];
    z[23]=z[23]*z[34]*z[27];
    z[24]=z[28]*z[36];
    z[21]= - z[21] - 25*z[3] + z[24];
    z[21]=z[13]*z[21];
    z[21]=z[23] + z[21];
    z[21]=z[12]*z[21];
    z[23]= - z[8] - z[3];
    z[23]=z[1]*z[23];
    z[24]=z[16]*z[6];
    z[23]=z[24] - z[35] + z[23];
    z[23]=z[11]*z[23];
    z[24]= - z[7] + z[30];
    z[24]=z[6]*z[24];
    z[24]=z[24] - z[29];
    z[24]=z[13]*z[24];
    z[26]=z[13] + z[37];
    z[26]=z[9]*z[7]*z[26];
    z[24]=z[26] - z[5] + z[24];
    z[24]=z[6]*z[24];
    z[24]= - static_cast<T>(2)+ z[24];
    z[24]=z[16]*z[24];
    z[26]=76*z[14] - 41*z[3];

    r += z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + 32*z[23] + 16*
      z[24] + z[25] + 2*z[26] + z[31] + 6*z[32] + z[41];
 
    return r;
}

template double qqb_2lLC_r27(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r27(const std::array<dd_real,31>&);
#endif
