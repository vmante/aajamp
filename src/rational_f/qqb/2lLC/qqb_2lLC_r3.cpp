#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r3(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[15];
    z[6]=k[5];
    z[7]=k[11];
    z[8]=k[14];
    z[9]=k[9];
    z[10]=z[3]*z[1];
    z[11]=z[7]*z[6];
    z[12]= - z[1]*z[11];
    z[12]=z[6] + z[12];
    z[12]=z[8]*z[12];
    z[13]=static_cast<T>(1)+ z[10];
    z[13]=z[4]*z[2]*z[13];
    z[10]=z[13] + z[12] - static_cast<T>(1)+ z[10];
    z[10]=z[5]*z[10];
    z[12]=static_cast<T>(1)- z[11];
    z[12]=z[8]*z[12];
    z[13]= - z[2] - z[1];
    z[13]=z[4]*z[3]*z[13];
    z[10]=z[10] + z[13] + z[7] + z[12];
    z[10]=z[5]*z[10];
    z[12]=z[1]*z[9];
    z[13]=z[12] + 1;
    z[11]= - 2*z[13] + z[11];
    z[11]=z[3]*z[11];
    z[14]= - static_cast<T>(2)+ z[12];
    z[14]=z[7]*z[14];
    z[11]=z[11] - z[9] + z[14];
    z[11]=z[8]*z[11];
    z[14]=z[7]*z[2];
    z[14]=z[13] + z[14];
    z[15]=2*z[3];
    z[14]=z[14]*z[15];
    z[14]=z[14] + z[9];
    z[12]= - static_cast<T>(2)- z[12];
    z[12]=z[7]*z[12];
    z[12]=z[12] + z[14];
    z[12]=z[4]*z[12];
    z[10]=2*z[10] + z[11] + z[12];
    z[10]=z[5]*z[10];
    z[11]=z[7]*z[13];
    z[11]=z[11] - z[14];
    z[11]=z[4]*z[8]*z[11];

    r += z[10] + z[11];
 
    return r;
}

template double qqb_2lLC_r3(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r3(const std::array<dd_real,31>&);
#endif
