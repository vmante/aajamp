#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r152(const std::array<T,31>& k) {
  T z[82];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[30];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=k[5];
    z[9]=k[7];
    z[10]=k[12];
    z[11]=k[11];
    z[12]=k[4];
    z[13]=k[15];
    z[14]=k[14];
    z[15]=k[19];
    z[16]=z[14]*z[13];
    z[17]=npow(z[13],2);
    z[18]=2*z[17] + z[16];
    z[19]=2*z[14];
    z[18]=z[18]*z[19];
    z[20]=npow(z[13],3);
    z[21]=3*z[20];
    z[18]=z[21] + z[18];
    z[18]=z[14]*z[18];
    z[22]=npow(z[14],3);
    z[23]=npow(z[14],2);
    z[24]= - z[5]*z[23];
    z[22]=z[22] + z[24];
    z[24]=2*z[5];
    z[22]=z[22]*z[24];
    z[18]=z[18] + z[22];
    z[18]=z[8]*z[18];
    z[22]=z[16] - z[17];
    z[25]=z[22]*z[19];
    z[26]=z[5]*z[14];
    z[27]=z[23] + z[26];
    z[27]=z[27]*z[24];
    z[18]=z[18] + z[27] - z[21] + z[25];
    z[18]=z[8]*z[18];
    z[21]=npow(z[4],2);
    z[25]=3*z[21];
    z[27]=z[4]*z[6];
    z[28]= - static_cast<T>(4)+ z[27];
    z[28]=z[28]*z[25];
    z[29]=2*z[6];
    z[30]=z[21]*z[29];
    z[31]=z[27] - 2;
    z[32]=z[5]*z[31];
    z[30]=z[30] + z[32];
    z[30]=z[30]*z[24];
    z[32]=z[5]*z[4];
    z[33]=2*z[21];
    z[34]=z[32] + z[33];
    z[34]=z[34]*z[24];
    z[35]=npow(z[4],3);
    z[36]=3*z[35];
    z[34]=z[34] + z[36];
    z[37]=z[2]*z[34];
    z[38]=z[35]*z[12];
    z[28]=z[37] - 6*z[38] + z[28] + z[30];
    z[28]=z[11]*z[28];
    z[28]=z[28] - z[34];
    z[28]=z[2]*z[28];
    z[30]=npow(z[6],2);
    z[34]=z[30]*z[4];
    z[37]=z[34] - z[29];
    z[38]=z[4]*z[37];
    z[38]=static_cast<T>(3)+ z[38];
    z[38]=z[5]*z[38];
    z[39]=z[6] + z[34];
    z[39]=z[4]*z[39];
    z[39]=static_cast<T>(1)+ z[39];
    z[39]=z[4]*z[39];
    z[38]=z[38] + z[39] - z[19];
    z[38]=z[38]*z[24];
    z[39]=3*z[4];
    z[40]= - z[13]*z[39];
    z[40]=4*z[21] + z[40];
    z[40]=z[40]*z[17];
    z[41]=z[20]*z[33];
    z[42]=z[12]*z[41];
    z[40]=z[40] + z[42];
    z[40]=z[12]*z[40];
    z[42]=z[33] + 3*z[17];
    z[42]=z[13]*z[42];
    z[40]=z[40] + z[36] + z[42];
    z[40]=z[12]*z[40];
    z[42]=z[5]*z[6];
    z[31]=3*z[31] + z[42];
    z[31]=z[5]*z[31];
    z[43]=npow(z[5],2);
    z[44]=z[43]*z[8];
    z[45]=5*z[27];
    z[46]=static_cast<T>(3)- z[45];
    z[46]=z[4]*z[46];
    z[31]=z[44] + z[46] + z[31];
    z[46]=static_cast<T>(14)- 9*z[27];
    z[46]=z[46]*z[21];
    z[47]=z[12]*z[36];
    z[46]=z[46] + z[47];
    z[46]=z[12]*z[46];
    z[31]=z[46] + 2*z[31];
    z[31]=z[11]*z[31];
    z[46]=3*z[27];
    z[47]=static_cast<T>(8)- z[46];
    z[47]=z[47]*z[21];
    z[18]=z[28] + z[31] + z[18] + z[40] + z[38] + z[47] - z[22];
    z[18]=z[2]*z[18];
    z[22]=z[1]*z[5];
    z[28]= - z[8]*z[5];
    z[31]=z[1] - z[6] + z[8];
    z[31]=z[9]*z[31];
    z[28]=z[31] - z[22] + z[42] + z[28];
    z[28]=z[10]*z[8]*z[28];
    z[31]=3*z[8];
    z[38]=2*z[1];
    z[40]= - z[38] - z[29] + z[31];
    z[40]=z[9]*z[40];
    z[28]=z[40] + z[28];
    z[40]=z[34] + z[29];
    z[47]=z[4]*z[40];
    z[48]=z[39]*z[30];
    z[49]= - 10*z[6] - z[48];
    z[49]=z[13]*z[49];
    z[47]=z[49] + static_cast<T>(5)+ z[47];
    z[47]=z[13]*z[47];
    z[49]=z[17]*z[12];
    z[50]=z[49]*z[21];
    z[51]=2*z[27];
    z[52]=z[51] + 3;
    z[53]=z[13]*z[4];
    z[54]= - z[52]*z[53];
    z[54]= - z[21] + z[54];
    z[54]=z[13]*z[54];
    z[54]=z[54] - z[50];
    z[54]=z[12]*z[54];
    z[55]=6*z[6];
    z[56]=z[55] + z[34];
    z[57]= - z[4]*z[56];
    z[57]= - static_cast<T>(5)+ z[57];
    z[57]=z[57]*z[17];
    z[54]=z[57] + z[54];
    z[54]=z[12]*z[54];
    z[57]=z[14]*z[4];
    z[58]= - z[40]*z[57];
    z[47]=z[54] + z[47] + z[58];
    z[47]=z[12]*z[47];
    z[54]=z[30]*z[13];
    z[56]=2*z[56] - 5*z[54];
    z[56]=z[13]*z[56];
    z[58]= - z[40]*z[19];
    z[59]=z[13]*z[6];
    z[60]=z[59] - 1;
    z[61]=2*z[13];
    z[60]=z[60]*z[61];
    z[62]= - z[60] - z[49];
    z[62]=z[62]*z[31];
    z[22]=10*z[22] + z[62] + z[47] + 6*z[42] + z[58] + static_cast<T>(4)+ z[56] + 2*
    z[28];
    z[22]=z[10]*z[22];
    z[28]=5*z[6];
    z[47]=z[28] + 3*z[54];
    z[47]=z[47]*z[17];
    z[56]=z[29] + z[54];
    z[56]=z[56]*z[61];
    z[58]=z[54]*z[14];
    z[56]=z[56] + z[58];
    z[56]=z[56]*z[19];
    z[47]=z[47] + z[56];
    z[47]=z[14]*z[47];
    z[56]=4*z[6];
    z[62]=z[30]*z[14];
    z[63]=z[56] + z[62];
    z[63]=z[63]*z[23];
    z[64]=z[62] + z[6];
    z[65]= - z[64]*z[26];
    z[63]=z[63] + z[65];
    z[63]=z[63]*z[24];
    z[47]=z[47] + z[63];
    z[47]=z[8]*z[47];
    z[63]=3*z[6];
    z[65]=z[63] - 2*z[54];
    z[65]=z[13]*z[65];
    z[65]=static_cast<T>(9)+ z[65];
    z[65]=z[13]*z[65];
    z[66]=11*z[54];
    z[67]=17*z[6] + z[66];
    z[67]=z[13]*z[67];
    z[58]=z[67] + 8*z[58];
    z[58]=z[14]*z[58];
    z[67]=15*z[6];
    z[68]=z[67] + 4*z[62];
    z[68]=z[14]*z[68];
    z[64]=z[5]*z[64];
    z[64]= - 3*z[64] + static_cast<T>(4)+ z[68];
    z[64]=z[64]*z[24];
    z[47]=z[47] + z[64] + z[65] + z[58];
    z[47]=z[8]*z[47];
    z[58]=z[46] - 1;
    z[58]=z[58]*z[21];
    z[64]=z[58]*z[14];
    z[65]=z[12]*z[14];
    z[68]=z[65]*z[36];
    z[64]=z[64] + z[68];
    z[69]= - z[12]*z[64];
    z[70]=z[48] - z[56];
    z[70]=z[70]*z[4];
    z[70]=z[70] - 3;
    z[57]=z[70]*z[57];
    z[70]=z[27] - 1;
    z[71]=z[70]*z[21];
    z[71]=6*z[71];
    z[69]=z[69] + z[71] - z[57];
    z[69]=z[12]*z[69];
    z[72]=z[56] - z[34];
    z[72]=z[4]*z[72];
    z[72]=static_cast<T>(1)+ z[72];
    z[72]=z[14]*z[72];
    z[73]=7*z[27];
    z[74]= - static_cast<T>(4)+ z[73];
    z[74]=z[4]*z[74];
    z[60]=z[69] + z[72] + z[74] - z[60];
    z[60]=z[12]*z[60];
    z[69]=8*z[6];
    z[66]=z[66] - z[69] + 5*z[34];
    z[66]=z[14]*z[66];
    z[72]=z[56] - z[54];
    z[72]=z[13]*z[72];
    z[72]=static_cast<T>(6)+ z[72];
    z[74]=z[28] + 3*z[62];
    z[74]=z[74]*z[24];
    z[47]=z[47] + z[60] + z[74] + 2*z[72] + z[66];
    z[47]=z[11]*z[47];
    z[60]=npow(z[6],4);
    z[66]=z[60]*z[21];
    z[72]=15*z[30];
    z[66]= - z[72] + z[66];
    z[66]=z[4]*z[66];
    z[74]= - z[30]*z[19];
    z[75]=npow(z[6],3);
    z[76]=z[4]*z[60];
    z[76]= - 6*z[75] + z[76];
    z[76]=z[4]*z[76];
    z[76]=13*z[30] + z[76];
    z[76]=z[5]*z[76];
    z[66]=z[76] + z[74] + 28*z[6] + z[66];
    z[66]=z[66]*z[24];
    z[74]=z[29]*z[13];
    z[76]= - static_cast<T>(5)+ z[74];
    z[76]=z[76]*z[16];
    z[43]= - z[17] + z[43];
    z[43]=z[8]*z[14]*z[43];
    z[77]=z[29]*z[14];
    z[78]=z[77] - 3;
    z[78]=z[78]*z[5];
    z[79]= - 5*z[14] - z[78];
    z[79]=z[5]*z[79];
    z[43]=z[43] + z[79] + z[17] + z[76];
    z[43]=z[8]*z[43];
    z[76]=7*z[6] - z[54];
    z[16]=z[76]*z[16];
    z[76]=z[14]*z[6];
    z[79]= - 11*z[6] + z[62];
    z[79]=z[5]*z[79];
    z[79]=z[79] - static_cast<T>(11)+ 7*z[76];
    z[79]=z[5]*z[79];
    z[80]=z[59] + 1;
    z[81]=z[13]*z[80];
    z[16]=z[43] + z[79] + z[81] + z[16];
    z[16]=z[49] + 2*z[16];
    z[16]=z[8]*z[16];
    z[43]=z[54] + z[28] + z[34];
    z[43]=z[13]*z[43];
    z[79]=4*z[14];
    z[81]= - z[54]*z[79];
    z[74]=z[74] + static_cast<T>(1)+ z[27];
    z[74]=z[13]*z[74];
    z[49]=z[74] + z[49];
    z[49]=z[12]*z[49];
    z[16]=z[16] + z[49] + z[66] + z[81] + z[43] + static_cast<T>(8)- z[73];
    z[16]=z[7]*z[16];
    z[41]=z[30]*z[41];
    z[41]=z[41] + z[64];
    z[41]=z[12]*z[41];
    z[20]=z[20]*z[48];
    z[20]=z[41] + z[20] + z[57];
    z[20]=z[12]*z[20];
    z[41]= - z[56] - z[34];
    z[41]=z[13]*z[41];
    z[43]= - static_cast<T>(1)+ z[51];
    z[41]=2*z[43] + z[41];
    z[41]=z[41]*z[61];
    z[43]=2*z[4];
    z[49]=z[43]*z[30];
    z[49]=z[49] - z[28];
    z[49]=z[49]*z[4];
    z[57]= - static_cast<T>(1)+ z[49];
    z[57]=z[14]*z[57];
    z[20]=z[20] + z[57] + z[39] + z[41];
    z[20]=z[12]*z[20];
    z[41]= - z[54] - z[6] + z[48];
    z[41]=z[41]*z[61];
    z[37]= - z[14]*z[37];
    z[48]= - z[69] - z[54];
    z[48]=z[13]*z[48];
    z[48]= - static_cast<T>(9)+ z[48];
    z[48]=z[13]*z[48];
    z[48]=z[48] - z[19];
    z[48]=z[8]*z[48];
    z[39]=z[1]*z[39];
    z[20]=z[39] + z[48] + z[20] + z[37] + z[41] + static_cast<T>(2)+ z[45];
    z[20]=z[9]*z[20];
    z[37]=z[40]*z[43];
    z[39]= - static_cast<T>(1)- z[37];
    z[39]=z[4]*z[39];
    z[41]=z[2]*z[52]*z[21];
    z[45]=z[75]*z[4];
    z[48]=z[45] + z[30];
    z[52]=z[48]*z[4];
    z[57]= - z[63] + z[52];
    z[57]=z[7]*z[57]*z[43];
    z[64]= - z[7]*z[40];
    z[66]=z[27] + 3;
    z[64]=z[64] + z[66];
    z[64]=z[21]*z[64];
    z[69]= - z[2]*z[35];
    z[64]=z[69] + z[64];
    z[64]=z[1]*z[64];
    z[39]=z[64] + z[57] + z[39] + z[41];
    z[39]=z[39]*z[38];
    z[29]=z[54] - z[29];
    z[29]=z[29]*z[13];
    z[41]= - 3*z[29] - z[77];
    z[41]=z[8]*z[41];
    z[41]=z[41] - z[62] + z[56] + 7*z[54];
    z[41]=z[10]*z[41];
    z[33]= - z[60]*z[33];
    z[33]=z[72] + z[33];
    z[33]=z[4]*z[33];
    z[29]=z[31]*z[29];
    z[29]=z[29] - 14*z[6] + z[33] + 8*z[54];
    z[29]=z[7]*z[29];
    z[31]=z[48]*z[43];
    z[33]= - z[67] + z[31];
    z[33]=z[4]*z[33];
    z[48]= - z[63] - z[34];
    z[48]=z[48]*z[43];
    z[48]=static_cast<T>(3)+ z[48];
    z[48]=z[4]*z[48];
    z[48]=z[48] - 3*z[14];
    z[48]=z[2]*z[48];
    z[54]=z[8] - z[56];
    z[54]=z[14]*z[54];
    z[29]=z[41] + z[39] + z[29] + z[48] - static_cast<T>(1)+ z[33] + z[54];
    z[29]=z[3]*z[29];
    z[33]=static_cast<T>(4)+ 3*z[59];
    z[17]=z[33]*z[17];
    z[39]= - z[80]*z[61];
    z[41]=z[59]*z[14];
    z[39]=z[39] - z[41];
    z[39]=z[39]*z[19];
    z[39]= - z[17] + z[39];
    z[39]=z[14]*z[39];
    z[48]= - static_cast<T>(2)- z[76];
    z[23]=z[48]*z[23];
    z[48]=static_cast<T>(1)+ z[76];
    z[26]=z[48]*z[26];
    z[23]=z[23] + z[26];
    z[23]=z[23]*z[24];
    z[23]=z[39] + z[23];
    z[23]=z[8]*z[23];
    z[26]=static_cast<T>(2)- 9*z[59];
    z[26]=z[13]*z[26];
    z[26]=z[26] - 8*z[41];
    z[26]=z[14]*z[26];
    z[39]= - static_cast<T>(1)- z[77];
    z[39]=z[39]*z[19];
    z[39]=z[39] + z[78];
    z[39]=z[39]*z[24];
    z[17]=z[23] + z[39] + z[17] + z[26];
    z[17]=z[8]*z[17];
    z[23]= - static_cast<T>(7)+ z[37];
    z[23]=z[4]*z[23];
    z[26]= - static_cast<T>(2)+ z[49];
    z[26]=z[5]*z[26];
    z[23]=z[23] + z[26];
    z[23]=z[5]*z[23];
    z[26]=13*z[6] - z[31];
    z[26]=z[4]*z[26];
    z[31]= - z[75]*z[43];
    z[31]=7*z[30] + z[31];
    z[31]=z[4]*z[31];
    z[31]= - z[55] + z[31];
    z[31]=z[5]*z[31];
    z[26]=z[31] - static_cast<T>(3)+ z[26];
    z[26]=z[5]*z[26];
    z[26]=z[26] + z[44];
    z[26]=z[7]*z[26];
    z[23]=z[26] - z[25] + z[23];
    z[25]=z[40]*z[21];
    z[26]= - z[6] + z[34];
    z[26]=z[26]*z[32];
    z[25]=z[25] + z[26];
    z[25]=z[7]*z[25];
    z[26]= - z[66] - z[42];
    z[26]=z[21]*z[26];
    z[31]=z[5]*z[21];
    z[31]=z[35] + z[31];
    z[31]=z[2]*z[31];
    z[25]=z[25] + z[26] + z[31];
    z[25]=z[38]*z[5]*z[25];
    z[26]= - static_cast<T>(1)- z[51];
    z[26]=z[26]*z[21];
    z[31]= - z[5]*z[70]*z[43];
    z[26]=z[26] + z[31];
    z[26]=z[26]*z[24];
    z[26]=z[36] + z[26];
    z[26]=z[2]*z[26];
    z[23]=z[25] + z[26] + 2*z[23];
    z[23]=z[1]*z[23];
    z[25]= - static_cast<T>(5)+ z[46];
    z[25]=z[25]*z[53];
    z[25]= - z[21] + z[25];
    z[25]=z[13]*z[25];
    z[26]= - static_cast<T>(1)+ 4*z[59];
    z[26]=z[26]*z[50];
    z[25]=z[25] + z[26];
    z[25]=z[12]*z[25];
    z[26]= - z[27] - z[33];
    z[26]=z[13]*z[26];
    z[26]=6*z[4] + z[26];
    z[26]=z[13]*z[26];
    z[25]=z[25] + z[71] + z[26];
    z[25]=z[12]*z[25];
    z[26]=12*z[6] - z[52];
    z[26]=z[4]*z[26];
    z[27]=5*z[30] - z[45];
    z[27]=z[4]*z[27];
    z[27]= - z[28] + z[27];
    z[27]=z[5]*z[27];
    z[28]= - z[14]*z[56];
    z[26]=z[27] + z[28] + static_cast<T>(4)+ z[26];
    z[24]=z[26]*z[24];
    z[19]=z[19]*z[21];
    z[19]=z[19] + z[68] - z[36];
    z[19]=z[19]*z[12];
    z[21]=z[79]*z[4];
    z[19]=z[19] - z[21] - z[58];
    z[19]=z[19]*z[12];
    z[21]=z[51] - 3;
    z[21]=z[21]*z[4];
    z[19]=z[19] - z[21] - z[79];
    z[19]=z[19]*z[12];
    z[19]=z[19] + z[70];
    z[19]=z[19]*z[12];
    z[21]=z[65]*z[8];
    z[19]=z[19] - z[21];
    z[21]= - z[11] + z[9];
    z[19]=z[15]*z[19]*z[21];
    z[21]= - static_cast<T>(4)+ z[46];
    z[21]=z[21]*z[43];
    z[26]=10*z[59] - static_cast<T>(2)+ z[73];
    z[26]=z[13]*z[26];
    z[27]= - 9*z[70] - 11*z[59];
    z[27]=z[14]*z[27];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[23]
       + z[24] + z[25] + z[26] + z[27] + z[29] + z[47];
 
    return r;
}

template double qqb_2lLC_r152(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r152(const std::array<dd_real,31>&);
#endif
