#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r89(const std::array<T,31>& k) {
  T z[50];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[20];
    z[7]=k[5];
    z[8]=k[7];
    z[9]=k[2];
    z[10]=k[11];
    z[11]=k[3];
    z[12]=k[9];
    z[13]=k[16];
    z[14]=k[14];
    z[15]=k[19];
    z[16]=2*z[2];
    z[17]=npow(z[2],2);
    z[18]=z[17]*z[3];
    z[19]= - z[18] - z[16] - z[7];
    z[19]=z[8]*z[19];
    z[20]=z[5]*z[2];
    z[21]=2*z[20];
    z[19]=z[21] + z[19];
    z[19]=z[14]*z[19];
    z[22]=z[20]*z[3];
    z[21]=static_cast<T>(1)- z[21];
    z[21]=z[5]*z[21];
    z[23]=z[18] - z[2];
    z[23]=z[23]*z[3];
    z[24]= - static_cast<T>(2)+ z[23];
    z[24]=z[8]*z[24];
    z[19]=z[19] + z[24] + z[21] - z[22];
    z[19]=z[14]*z[19];
    z[21]=z[20] + 1;
    z[24]=z[21]*z[3];
    z[25]=z[5]*z[24];
    z[26]=z[3]*z[2];
    z[27]=z[26] + 1;
    z[28]=z[27]*z[3];
    z[29]=z[8]*z[28];
    z[30]= - z[14]*z[22];
    z[31]=z[14]*z[26];
    z[31]= - z[3] + z[31];
    z[31]=z[10]*z[31];
    z[29]=z[31] + z[30] + z[25] + z[29];
    z[30]=z[11]*z[14];
    z[29]=z[29]*z[30];
    z[31]=2*z[8];
    z[32]=z[8]*z[7];
    z[33]= - z[4]*z[32];
    z[33]=z[31] + z[33];
    z[33]=z[4]*z[33];
    z[34]=z[14]*z[18];
    z[23]=z[34] - static_cast<T>(2)- z[23];
    z[23]=z[14]*z[23];
    z[23]=2*z[28] + z[23];
    z[23]=z[10]*z[23];
    z[34]=npow(z[3],2);
    z[35]= - z[3]*z[31];
    z[19]=z[29] + z[23] + z[19] + z[33] + z[34] + z[35];
    z[19]=z[11]*z[19];
    z[23]=z[3]*npow(z[2],4);
    z[29]=npow(z[2],3);
    z[23]=z[23] + 2*z[29];
    z[33]=z[17]*z[7];
    z[33]=z[33] + z[23];
    z[33]=z[33]*z[14];
    z[23]=z[23]*z[3];
    z[35]=3*z[2];
    z[36]=z[35]*z[7];
    z[23]= - z[33] + z[36] + z[23] + 4*z[17];
    z[23]=z[23]*z[14];
    z[21]=z[21]*z[5];
    z[33]=npow(z[5],2);
    z[36]=z[33]*z[7];
    z[21]=z[21] - z[36];
    z[21]=z[21]*z[7];
    z[37]=3*z[20];
    z[21]=z[21] - z[37] + 2;
    z[21]=z[21]*z[7];
    z[38]=z[34]*z[29];
    z[21]=z[23] + z[21] - z[38] + 9*z[2];
    z[23]=z[8]*z[21];
    z[39]=2*z[5];
    z[40]=z[39] - z[36];
    z[40]=z[40]*z[7];
    z[41]=z[40] + 3;
    z[42]=npow(z[7],2);
    z[43]= - z[41]*z[42];
    z[44]=z[7]*z[5];
    z[45]=z[44] + 1;
    z[46]=npow(z[7],3)*z[45]*z[4];
    z[43]=z[43] - z[46];
    z[43]=z[4]*z[43];
    z[21]=z[43] - z[21];
    z[21]=z[10]*z[21];
    z[18]=z[18] + z[35];
    z[18]=z[18]*z[3];
    z[18]=z[18] + 2;
    z[43]= - z[8]*z[18];
    z[47]=z[42]*z[4];
    z[48]=z[8]*z[47];
    z[48]= - z[32] + z[48];
    z[48]=z[4]*z[48];
    z[49]=z[7] - z[47];
    z[49]=z[4]*z[49];
    z[18]=z[49] + z[18];
    z[18]=z[10]*z[18];
    z[18]=z[18] + z[43] + z[48];
    z[18]=z[11]*z[18];
    z[42]=z[42]*z[8];
    z[41]=z[41]*z[42];
    z[43]=z[8]*z[46];
    z[41]=z[41] + z[43];
    z[41]=z[4]*z[41];
    z[18]=z[18] + z[21] + z[41] + z[23];
    z[18]=z[15]*z[18];
    z[21]=z[33]*z[2];
    z[21]=z[21] - z[36];
    z[23]=z[32] - 1;
    z[41]=z[44] + 2;
    z[23]=z[41]*z[23];
    z[23]=z[20] + z[23];
    z[23]=z[4]*z[23];
    z[22]=z[22] - z[5];
    z[41]= - z[8]*z[44];
    z[41]=z[41] - z[22];
    z[41]=z[4]*z[41];
    z[22]=z[22]*z[3];
    z[41]=z[22] + z[41];
    z[41]=z[1]*z[41];
    z[43]=static_cast<T>(3)- z[20];
    z[43]=z[3]*z[43];
    z[46]=z[36] - z[5];
    z[46]=z[46]*z[7];
    z[48]= - static_cast<T>(2)- z[46];
    z[48]=z[8]*z[48];
    z[23]=z[41] + z[23] + z[48] + z[43] - z[21];
    z[23]=z[4]*z[23];
    z[23]= - z[33] - z[34] + z[23];
    z[23]=z[1]*z[23];
    z[39]= - z[17]*z[39];
    z[39]=z[2] + z[39];
    z[39]=z[5]*z[39];
    z[41]=z[20] - 1;
    z[43]= - z[41]*z[44];
    z[39]=z[39] + z[43];
    z[39]=z[7]*z[39];
    z[33]= - z[29]*z[33];
    z[33]=z[33] + z[39];
    z[33]=z[4]*z[33];
    z[39]=static_cast<T>(1)+ z[37];
    z[39]=z[5]*z[39];
    z[25]= - z[11]*z[25];
    z[24]=z[25] + z[39] + z[24];
    z[24]=z[11]*z[24];
    z[25]= - z[5]*z[17];
    z[25]= - z[16] + z[25];
    z[25]=z[5]*z[25];
    z[37]=static_cast<T>(1)- z[37];
    z[37]=z[37]*z[44];
    z[24]=z[24] + z[33] + z[37] + static_cast<T>(2)+ z[25];
    z[24]=z[13]*z[24];
    z[25]=z[45] - z[20];
    z[33]=z[7]*z[25];
    z[37]= - z[45]*z[42];
    z[33]=z[33] + z[37];
    z[33]=z[4]*z[33];
    z[21]=z[7]*z[21];
    z[37]= - static_cast<T>(2)+ z[46];
    z[32]=z[37]*z[32];
    z[37]=npow(z[20],2);
    z[21]=z[33] + z[32] + z[37] + z[21];
    z[21]=z[4]*z[21];
    z[32]=z[20] - 2;
    z[32]=z[32]*z[44];
    z[16]=z[38] + z[16] + 3*z[7];
    z[16]=z[8]*z[16];
    z[29]=z[29]*z[3];
    z[17]=z[29] + z[17];
    z[29]=z[2] + z[7];
    z[29]=z[7]*z[29];
    z[29]=z[29] - z[17];
    z[29]=z[8]*z[29];
    z[33]= - z[7]*z[20];
    z[29]=z[33] + z[29];
    z[29]=z[14]*z[29];
    z[16]=z[29] + z[16] + z[20] + z[32];
    z[16]=z[14]*z[16];
    z[29]= - z[40] + z[41];
    z[29]=z[7]*z[29];
    z[25]= - z[25]*z[47];
    z[25]=z[29] + z[25];
    z[25]=z[4]*z[25];
    z[17]=z[14]*z[17];
    z[17]=z[17] - z[35] - z[38];
    z[17]=z[14]*z[17];
    z[29]= - static_cast<T>(6)+ z[44];
    z[17]=z[17] + z[25] + 2*z[29] - 5*z[26];
    z[17]=z[10]*z[17];
    z[25]= - z[9] - z[11];
    z[25]=z[10]*z[25];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[34]*z[25];
    z[29]=static_cast<T>(5)+ z[26];
    z[29]=z[10]*z[29];
    z[29]= - z[13] + z[29];
    z[29]=z[3]*z[29];
    z[32]=z[10] - z[13];
    z[32]=z[12]*z[32];
    z[25]=2*z[32] + z[25] + z[29];
    z[25]=z[9]*z[25];
    z[27]=z[20] - z[27];
    z[27]=z[3]*z[27];
    z[22]= - z[1]*z[22];
    z[22]=z[22] + z[27] - z[31];
    z[22]=z[1]*z[22];
    z[27]= - z[30] - 1;
    z[27]=z[28]*z[27];
    z[26]=static_cast<T>(2)+ z[26];
    z[26]=z[14]*z[26];
    z[26]=z[26] + z[27];
    z[26]=z[11]*z[26];
    z[27]= - z[14]*z[7];
    z[22]=z[22] + z[26] - static_cast<T>(1)+ z[27];
    z[22]=z[6]*z[22];
    z[20]=static_cast<T>(2)+ z[20];
    z[20]=z[5]*z[20];
    z[26]= - 3*z[5] + z[36];
    z[26]=z[7]*z[26];
    z[26]=static_cast<T>(6)+ z[26];
    z[26]=z[8]*z[26];

    r +=  - 4*z[3] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + 
      z[22] + z[23] + z[24] + z[25] + z[26];
 
    return r;
}

template double qqb_2lLC_r89(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r89(const std::array<dd_real,31>&);
#endif
