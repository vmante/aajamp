#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r52(const std::array<T,31>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[13];
    z[8]=k[2];
    z[9]=k[11];
    z[10]=npow(z[1],2);
    z[11]=z[10]*z[6];
    z[12]=2*z[1];
    z[13]=z[2] - z[12] + z[11];
    z[13]=z[4]*z[13];
    z[14]=2*z[6];
    z[15]=z[14]*z[1];
    z[16]=z[5]*z[6];
    z[16]=z[16] - static_cast<T>(1)+ z[15];
    z[16]=z[4]*z[16];
    z[16]=z[6] + z[16];
    z[16]=z[5]*z[16];
    z[17]=z[9]*z[7];
    z[18]=z[17]*z[2];
    z[19]=z[18] + z[6];
    z[20]=z[19]*z[2];
    z[13]=z[16] + z[13] + z[15] - z[20];
    z[13]=z[5]*z[13];
    z[16]=z[1] - z[8];
    z[21]=npow(z[8],2);
    z[22]=z[21]*z[9];
    z[23]=z[16] + z[22];
    z[12]=z[2]*z[12];
    z[12]= - z[10] + z[12];
    z[12]=z[4]*z[12];
    z[24]=z[2]*z[9];
    z[25]= - static_cast<T>(1)- z[24];
    z[25]=z[2]*z[25];
    z[12]=z[13] + z[12] + z[25] + z[23];
    z[12]=z[5]*z[12];
    z[13]=z[24]*z[8];
    z[24]=z[4]*z[10];
    z[22]=z[24] - z[13] - z[8] + 2*z[22];
    z[22]=z[2]*z[22];
    z[24]= - z[9]*npow(z[8],3);
    z[16]=z[1]*z[16];
    z[12]=z[12] + z[16] + z[21] + z[24] + z[22];
    z[12]=z[3]*z[12];
    z[16]=z[14]*z[4];
    z[21]=z[16]*z[5];
    z[21]=z[21] + z[14];
    z[22]=z[6]*z[1];
    z[22]=static_cast<T>(2)- z[22];
    z[22]=z[4]*z[22];
    z[22]=z[22] - z[18] - z[21];
    z[22]=z[5]*z[22];
    z[24]=z[9]*z[8];
    z[19]= - z[9] + z[19];
    z[19]=z[2]*z[19];
    z[11]=z[11] + z[1];
    z[25]= - z[2] + z[11];
    z[25]=z[4]*z[25];
    z[19]=z[22] + z[25] + z[19] - static_cast<T>(1)+ z[24];
    z[19]=z[5]*z[19];
    z[22]= - z[2]*z[1];
    z[22]= - z[10] + z[22];
    z[22]=z[4]*z[22];
    z[12]=z[12] + z[19] + z[22] + z[13] - z[23];
    z[12]=z[3]*z[12];
    z[13]= - z[17] + z[16];
    z[13]=z[5]*z[13];
    z[15]=z[15] + 1;
    z[16]= - z[4]*z[15];
    z[13]=z[13] + z[6] + z[16];
    z[13]=z[5]*z[13];
    z[11]=2*z[11] + z[2];
    z[11]=z[4]*z[11];
    z[11]=2*z[12] + z[13] - z[20] + z[11];
    z[11]=z[3]*z[11];
    z[12]=z[17]*z[21];
    z[13]=z[14]*z[7];
    z[13]= - z[17] - z[13];
    z[13]=z[4]*z[13];
    z[12]=z[13] + z[12];
    z[12]=z[5]*z[12];
    z[13]=z[18]*z[6];
    z[14]=z[6]*z[7];
    z[13]=z[13] + z[14];
    z[15]=z[7]*z[15];
    z[15]=z[18] + z[15];
    z[15]=z[4]*z[15];
    z[12]=z[12] + z[15] - z[13];
    z[12]=z[5]*z[12];
    z[13]=z[2]*z[13];
    z[10]= - z[10]*z[14];
    z[14]= - z[1]*z[7];
    z[10]=z[14] + z[10];
    z[14]=z[2]*z[7];
    z[10]=2*z[10] - z[14];
    z[10]=z[4]*z[10];

    r += z[10] + z[11] + z[12] + z[13];
 
    return r;
}

template double qqb_2lLC_r52(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r52(const std::array<dd_real,31>&);
#endif
