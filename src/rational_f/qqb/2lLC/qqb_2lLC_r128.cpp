#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r128(const std::array<T,31>& k) {
  T z[71];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[13];
    z[8]=k[2];
    z[9]=k[10];
    z[10]=k[15];
    z[11]=k[14];
    z[12]=k[11];
    z[13]=k[3];
    z[14]=k[19];
    z[15]=3*z[12];
    z[16]=npow(z[13],2);
    z[17]=z[16]*z[11];
    z[18]= - 45*z[13] - 23*z[17];
    z[18]=z[18]*z[15];
    z[19]=10*z[6];
    z[20]=z[16]*z[9];
    z[21]= - z[13] + 2*z[20];
    z[21]=z[21]*z[19];
    z[22]=z[16]*z[6];
    z[23]= - 7*z[13] - 19*z[8];
    z[23]=2*z[23] + 21*z[22];
    z[24]=2*z[10];
    z[23]=z[23]*z[24];
    z[25]=6*z[4];
    z[26]=z[25]*z[13];
    z[27]=z[11]*z[13];
    z[28]=z[9]*z[13];
    z[29]= - static_cast<T>(27)+ 47*z[28];
    z[30]=3*z[7];
    z[31]= - z[13]*z[30];
    z[32]=149*z[9] - 73*z[11];
    z[32]=z[8]*z[32];
    z[18]=z[23] + z[26] + z[21] + z[18] + z[32] + 7*z[27] + 2*z[29] + 
    z[31];
    z[18]=z[10]*z[18];
    z[21]=25*z[4];
    z[23]= - z[30] + z[21];
    z[29]=3*z[10];
    z[23]=z[23]*z[29];
    z[31]=npow(z[10],2);
    z[32]=z[31]*z[2];
    z[33]=z[30] + 151*z[4];
    z[33]=z[33]*z[32];
    z[34]=z[6]*z[11];
    z[35]=z[12]*z[7];
    z[23]=z[33] + z[23] - 14*z[35] + z[34];
    z[33]=3*z[2];
    z[23]=z[23]*z[33];
    z[36]=8*z[7];
    z[37]=z[36] + z[21];
    z[37]=z[10]*z[37];
    z[38]=z[32]*z[4];
    z[37]=198*z[38] - 71*z[35] + 6*z[37];
    z[37]=z[2]*z[37];
    z[39]= - z[6] + z[4];
    z[37]=23*z[39] + z[37];
    z[37]=z[2]*z[37];
    z[39]=2*z[1];
    z[40]=npow(z[1],2);
    z[41]=z[40]*z[6];
    z[42]= - z[39] + z[41];
    z[42]=z[4]*z[42];
    z[43]=z[6]*z[1];
    z[44]=2*z[43];
    z[42]=z[44] + z[42];
    z[37]=23*z[42] + z[37];
    z[37]=z[3]*z[37];
    z[42]=2*z[4];
    z[45]=static_cast<T>(29)- 22*z[43];
    z[45]=z[45]*z[42];
    z[46]=npow(z[8],3);
    z[47]=z[46]*z[10];
    z[47]=22*z[47];
    z[48]=npow(z[8],2);
    z[49]=z[47] + 47*z[48];
    z[49]=z[49]*z[10];
    z[50]= - z[11]*z[49];
    z[51]=z[8]*z[11];
    z[50]= - 31*z[51] + z[50];
    z[50]=z[50]*z[29];
    z[23]=z[37] + z[23] + z[50] + z[45] - 21*z[11] - 61*z[6];
    z[23]=z[3]*z[23];
    z[37]=3*z[11];
    z[45]=z[37]*z[6];
    z[50]=z[4]*z[6];
    z[52]=26*z[50] - 10*z[35] - z[45];
    z[53]=z[48]*z[10];
    z[54]=16*z[53];
    z[55]= - z[11]*z[54];
    z[55]= - 35*z[51] + z[55];
    z[55]=z[10]*z[55];
    z[55]=z[55] + z[21] - 25*z[7] - 16*z[11];
    z[55]=z[55]*z[29];
    z[56]=z[30] + 52*z[4];
    z[56]=z[56]*z[32];
    z[23]=z[23] + 6*z[56] + 2*z[52] + z[55];
    z[23]=z[3]*z[23];
    z[52]=z[4] - z[7];
    z[55]=z[52]*z[32];
    z[52]=z[52]*z[31];
    z[52]=66*z[52];
    z[56]=npow(z[2],2)*z[52];
    z[44]= - static_cast<T>(1)+ z[44];
    z[44]=z[4]*z[44];
    z[44]=z[6] + z[44];
    z[44]=23*z[44] + z[56];
    z[44]=z[3]*z[44];
    z[44]=z[44] + 132*z[55] + z[45] - 58*z[50];
    z[44]=z[3]*z[44];
    z[44]=z[52] + z[44];
    z[44]=z[3]*z[44];
    z[45]=z[6] - z[12];
    z[52]=z[7]*z[14];
    z[55]= - z[52]*z[45];
    z[56]= - z[14] - z[7];
    z[56]=z[12]*z[56];
    z[57]=z[14] - z[7];
    z[57]=z[6]*z[57];
    z[56]=z[56] + z[57];
    z[56]=z[4]*z[56];
    z[56]= - 41*z[55] + 3*z[56];
    z[56]=z[56]*z[42];
    z[57]=z[6]*z[7];
    z[58]=2*z[57];
    z[59]=z[4]*z[12];
    z[60]= - z[58] - z[59];
    z[60]=z[60]*z[31];
    z[61]=z[55]*npow(z[4],2);
    z[50]=npow(z[3],3)*z[50];
    z[50]= - 6*z[61] + 23*z[50];
    z[50]=z[5]*z[50];
    z[44]=z[50] + z[44] + z[56] + 3*z[60];
    z[44]=z[5]*z[44];
    z[50]= - z[12]*z[17];
    z[50]=z[50] + z[27] - z[51];
    z[50]=z[10]*z[50];
    z[56]=60*z[4];
    z[60]= - static_cast<T>(3)+ 26*z[27];
    z[60]=z[12]*z[60];
    z[50]=23*z[50] + z[56] - 6*z[6] + z[60] + 15*z[7] - 49*z[11];
    z[50]=z[10]*z[50];
    z[60]=z[12]*z[11];
    z[61]= - z[4]*z[15];
    z[50]=z[50] + z[61] - 55*z[60] - 18*z[57];
    z[50]=z[10]*z[50];
    z[61]=z[7]*z[1];
    z[62]=z[13]*z[14];
    z[63]=z[61] - static_cast<T>(1)+ z[62];
    z[63]=z[6]*z[63];
    z[64]= - z[12]*z[62];
    z[63]=z[63] + z[7] + z[64];
    z[63]=z[63]*z[25];
    z[64]=27*z[14];
    z[65]= - z[64] - 50*z[7];
    z[65]=z[12]*z[65];
    z[64]=z[64] - 76*z[7];
    z[64]=z[6]*z[64];
    z[63]=z[63] + z[65] + z[64];
    z[63]=z[4]*z[63];
    z[64]= - 4*z[57] - z[59];
    z[64]=z[64]*z[32];
    z[23]=z[44] + z[23] + 3*z[64] + z[50] - 76*z[55] + z[63];
    z[23]=z[5]*z[23];
    z[17]= - 19*z[13] - 20*z[17];
    z[17]=z[17]*z[15];
    z[44]=76*z[6] - z[30];
    z[44]=z[13]*z[44];
    z[50]=71*z[22] + 43*z[13] + 23*z[8];
    z[50]=z[10]*z[50];
    z[17]=z[50] + z[26] + z[17] + 53*z[51] + 60*z[27] + static_cast<T>(115)+ z[44];
    z[17]=z[10]*z[17];
    z[44]=12*z[7] + 7*z[11];
    z[50]=static_cast<T>(1)+ 4*z[27];
    z[50]=z[12]*z[50];
    z[17]=z[17] + z[56] + z[19] + 2*z[44] + 27*z[50];
    z[17]=z[10]*z[17];
    z[19]=11*z[10];
    z[21]=z[19] + z[21];
    z[44]=z[21]*z[24];
    z[44]=z[44] + 33*z[38];
    z[50]=6*z[2];
    z[44]=z[44]*z[50];
    z[55]=198*z[10];
    z[56]=71*z[12];
    z[44]=z[44] + z[55] - z[56] + 102*z[4];
    z[44]=z[2]*z[44];
    z[63]=z[4]*z[1];
    z[44]=z[44] - static_cast<T>(5)+ 46*z[63];
    z[44]=z[2]*z[44];
    z[64]=z[48]*z[56];
    z[65]= - 71*z[8] + 5*z[1];
    z[66]=z[40]*z[4];
    z[67]=23*z[66];
    z[44]=z[44] - z[67] + z[64] + z[65];
    z[44]=z[3]*z[44];
    z[64]=88*z[10] - z[30] + 191*z[4];
    z[64]=z[64]*z[29];
    z[64]=510*z[38] + 46*z[35] + z[64];
    z[64]=z[2]*z[64];
    z[68]= - z[30] - 14*z[12];
    z[64]=z[64] + z[55] + 95*z[4] + 3*z[68] + 43*z[6];
    z[64]=z[2]*z[64];
    z[68]=22*z[1] + 7*z[41];
    z[68]=z[68]*z[42];
    z[49]=31*z[8] + z[49];
    z[49]=z[49]*z[29];
    z[69]=z[12]*z[8];
    z[70]= - static_cast<T>(13)+ 20*z[69];
    z[44]=z[44] + z[64] + z[49] + z[68] + 3*z[70] - 29*z[43];
    z[44]=z[3]*z[44];
    z[49]= - z[30] + 145*z[4];
    z[32]=z[49]*z[32];
    z[49]=44*z[10] - z[7] + 111*z[4];
    z[49]=z[10]*z[49];
    z[32]=z[32] + z[49] - 4*z[35] - 5*z[34];
    z[32]=z[32]*z[33];
    z[49]=static_cast<T>(7)+ 5*z[51];
    z[49]=z[8]*z[49];
    z[49]=5*z[49] + z[54];
    z[49]=z[10]*z[49];
    z[49]=z[49] + static_cast<T>(16)+ 29*z[51];
    z[49]=z[49]*z[29];
    z[51]= - z[30] + z[11];
    z[51]=9*z[51] - z[12];
    z[64]=static_cast<T>(49)- 20*z[43];
    z[64]=z[4]*z[64];
    z[32]=z[44] + z[32] + z[49] + z[64] + 3*z[51] + 38*z[6];
    z[32]=z[3]*z[32];
    z[44]=5*z[62];
    z[49]= - static_cast<T>(1)- z[44];
    z[49]=z[49]*z[15];
    z[44]= - static_cast<T>(7)+ z[44];
    z[44]=3*z[44] + 44*z[61];
    z[44]=z[6]*z[44];
    z[51]=z[1] - z[13];
    z[51]=z[6]*z[51];
    z[51]= - z[61] + z[51];
    z[51]=z[51]*z[25];
    z[44]=z[51] + z[44] + 44*z[7] + z[49];
    z[44]=z[4]*z[44];
    z[49]= - z[58] + z[59];
    z[51]=2*z[6];
    z[58]=22*z[4] - z[7] - z[51];
    z[24]=z[58]*z[24];
    z[24]=3*z[49] + z[24];
    z[24]=z[24]*z[29];
    z[49]=npow(z[11],2);
    z[58]=z[45]*z[49];
    z[59]=z[14]*z[58];
    z[31]= - z[31]*z[57];
    z[31]=z[31] + z[59];
    z[31]=z[31]*z[50];
    z[57]=z[37]*z[14];
    z[57]= - z[57] + 44*z[52];
    z[57]= - z[57]*z[45];
    z[35]=z[4]*z[35];
    z[24]=z[31] + z[24] + 32*z[35] + z[57];
    z[24]=z[2]*z[24];
    z[31]=3*z[14];
    z[35]=z[31] - z[36];
    z[57]=2*z[11];
    z[59]=z[37]*z[13];
    z[64]=static_cast<T>(13)- z[59];
    z[64]=z[64]*z[57];
    z[35]=7*z[35] + z[64];
    z[35]=z[6]*z[35];
    z[64]= - 21*z[14] - 43*z[11];
    z[64]=z[12]*z[64];
    z[17]=z[23] + z[32] + z[24] + z[17] + z[44] + z[35] - 6*z[49] + 
    z[64];
    z[17]=z[5]*z[17];
    z[23]=z[8]*z[9];
    z[24]=3*z[23];
    z[32]=static_cast<T>(22)+ z[24];
    z[32]=z[8]*z[32];
    z[35]=z[9]*z[47];
    z[32]=z[32] + z[35];
    z[32]=z[10]*z[32];
    z[35]=static_cast<T>(43)+ z[23];
    z[32]=3*z[35] + z[32];
    z[32]=z[32]*z[29];
    z[35]=116*z[4] + 63*z[10];
    z[35]=z[10]*z[35];
    z[35]=z[35] + 63*z[38];
    z[35]=z[35]*z[33];
    z[44]=6*z[23];
    z[49]= - static_cast<T>(25)- z[44];
    z[49]=z[12]*z[49];
    z[32]=z[35] + z[32] + z[49] + 141*z[4];
    z[32]=z[2]*z[32];
    z[35]=static_cast<T>(9)+ z[23];
    z[35]=z[35]*z[69];
    z[49]= - static_cast<T>(47)+ 44*z[23];
    z[49]=z[49]*z[48];
    z[49]=z[49] - z[47];
    z[49]=z[10]*z[49];
    z[64]=static_cast<T>(35)+ z[44];
    z[64]=z[8]*z[64];
    z[49]=z[64] + z[49];
    z[49]=z[49]*z[29];
    z[64]=z[1]*z[42];
    z[24]=z[32] + z[49] + z[64] + 12*z[35] + static_cast<T>(155)+ z[24];
    z[24]=z[2]*z[24];
    z[21]=z[10]*z[21];
    z[21]=z[21] + 11*z[38];
    z[21]=z[2]*z[21];
    z[19]=z[8]*z[19];
    z[19]=static_cast<T>(25)+ z[19];
    z[19]=z[10]*z[19];
    z[19]=z[21] + 14*z[4] + z[19];
    z[19]=z[19]*z[50];
    z[21]=z[8]*z[55];
    z[19]=z[19] + z[21] + 18*z[63] + static_cast<T>(84)- 71*z[69];
    z[19]=z[2]*z[19];
    z[21]=z[48]*z[12];
    z[19]=z[19] + z[67] + 142*z[21] + 18*z[1] + 61*z[8];
    z[19]=z[2]*z[19];
    z[32]= - z[46]*z[56];
    z[35]= - z[8]*z[65];
    z[19]=z[19] + z[32] + 23*z[40] + z[35];
    z[19]=z[3]*z[19];
    z[32]=3*z[9];
    z[35]=z[1]*z[32];
    z[35]=15*z[23] + static_cast<T>(56)+ z[35];
    z[35]=z[8]*z[35];
    z[44]= - static_cast<T>(83)- z[44];
    z[21]=z[44]*z[21];
    z[44]= - static_cast<T>(47)+ 22*z[23];
    z[44]=z[44]*z[48];
    z[44]=z[44] - z[47];
    z[44]=z[44]*z[29];
    z[19]=z[19] + z[24] + z[44] - 14*z[66] + z[21] + 10*z[1] + z[35];
    z[19]=z[3]*z[19];
    z[21]=z[9]*z[53];
    z[21]=82*z[21] + static_cast<T>(41)+ 9*z[23];
    z[21]=z[10]*z[21];
    z[21]=z[21] - z[30] + 92*z[4];
    z[21]=z[21]*z[29];
    z[24]= - 6*z[9] - 23*z[7];
    z[24]=z[12]*z[24];
    z[21]=189*z[38] + z[24] + z[21];
    z[21]=z[2]*z[21];
    z[24]=z[12]*z[23];
    z[24]=z[24] - z[9];
    z[24]= - z[30] + 2*z[24];
    z[30]=41*z[23];
    z[35]= - static_cast<T>(19)+ z[30];
    z[35]=z[8]*z[35];
    z[35]=3*z[35] - z[54];
    z[35]=z[10]*z[35];
    z[35]=z[35] + static_cast<T>(60)+ 13*z[23];
    z[35]=z[35]*z[29];
    z[21]=z[21] + z[35] + 101*z[4] + 3*z[24] - 11*z[6];
    z[21]=z[2]*z[21];
    z[15]= - z[8]*z[15];
    z[15]=z[15] + z[23] + static_cast<T>(1)- 6*z[61];
    z[24]=41*z[1] + 16*z[41];
    z[24]=z[24]*z[42];
    z[30]= - static_cast<T>(64)+ z[30];
    z[30]=z[8]*z[30];
    z[30]=z[30] - 41*z[53];
    z[29]=z[30]*z[29];
    z[15]=z[19] + z[21] + z[29] + z[24] + 3*z[15] - 14*z[43];
    z[15]=z[3]*z[15];
    z[19]=61*z[13] + 21*z[20];
    z[19]=z[19]*z[51];
    z[21]= - static_cast<T>(1)+ 9*z[28];
    z[22]=142*z[22] + 299*z[13] + 175*z[8];
    z[22]=z[10]*z[22];
    z[19]=z[22] + z[26] + z[19] + 22*z[21] + 393*z[23];
    z[19]=z[10]*z[19];
    z[21]=4*z[6];
    z[22]=static_cast<T>(7)- 3*z[28];
    z[22]=z[22]*z[21];
    z[24]= - z[32] - 2*z[7];
    z[24]=2*z[24] - 19*z[12];
    z[19]=z[19] + 72*z[4] + 3*z[24] + z[22];
    z[19]=z[10]*z[19];
    z[22]= - z[60] + z[34];
    z[24]=z[9] + 2*z[14];
    z[24]=z[24]*z[37];
    z[26]=z[9]*z[14];
    z[24]=z[24] + 29*z[26];
    z[22]=z[24]*z[22];
    z[24]=z[33]*z[26]*z[58];
    z[22]=z[24] + z[22];
    z[22]=z[2]*z[22];
    z[24]=z[28] + 1;
    z[24]=z[24]*z[37];
    z[29]=32*z[9];
    z[24]=z[24] + z[31] + z[29];
    z[24]=z[24]*z[11];
    z[26]=z[26] - z[52];
    z[24]=z[24] - 16*z[26];
    z[24]=z[24]*z[45];
    z[22]=z[24] + z[22];
    z[24]=z[6]*z[20];
    z[24]=71*z[24] + 256*z[28] + 365*z[23];
    z[24]=z[10]*z[24];
    z[26]= - static_cast<T>(3)+ 23*z[28];
    z[26]=z[6]*z[26];
    z[26]=36*z[4] + z[29] + z[26];
    z[24]=2*z[26] + z[24];
    z[24]=z[10]*z[24];
    z[26]=z[6]*z[9];
    z[24]=23*z[26] + z[24];
    z[24]=z[10]*z[24];
    z[22]=z[24] + 2*z[22];
    z[22]=z[2]*z[22];
    z[24]=z[37]*z[20];
    z[24]=z[24] - static_cast<T>(4)+ 19*z[28];
    z[24]=z[11]*z[24];
    z[26]=16*z[7];
    z[29]=static_cast<T>(4)- 13*z[28];
    z[29]=z[14]*z[29];
    z[24]=z[24] + z[26] + z[29];
    z[24]=z[6]*z[24];
    z[29]= - z[59] + static_cast<T>(17)- 16*z[28];
    z[29]=z[11]*z[29];
    z[30]= - static_cast<T>(29)+ 13*z[62];
    z[30]=z[9]*z[30];
    z[29]=z[29] - 4*z[14] + z[30];
    z[29]=z[12]*z[29];
    z[26]= - z[4]*z[26];
    z[24]=z[26] + z[29] + z[24];
    z[19]=z[22] + 2*z[24] + z[19];
    z[19]=z[2]*z[19];
    z[22]=2*z[13] - z[20];
    z[22]=z[11]*z[22];
    z[22]=58*z[23] + 32*z[22] - 6*z[28] - static_cast<T>(62)- 9*z[62];
    z[22]=z[12]*z[22];
    z[16]=z[16]*z[37];
    z[16]=z[16] - 20*z[13] + 13*z[20];
    z[16]=z[16]*z[57];
    z[20]= - static_cast<T>(10)+ 3*z[62];
    z[16]=z[16] + 3*z[20] + 32*z[61];
    z[16]=z[6]*z[16];
    z[20]= - z[40]*z[36];
    z[23]=z[39] - z[13];
    z[20]=3*z[23] + z[20];
    z[20]=z[20]*z[21];
    z[21]= - z[1]*z[25];
    z[20]=z[21] + z[20] + static_cast<T>(15)- 64*z[61];
    z[20]=z[4]*z[20];
    z[21]=6*z[27] - static_cast<T>(61)+ 52*z[28];
    z[21]=z[11]*z[21];

    r += 27*z[7] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + 
      z[21] + z[22];
 
    return r;
}

template double qqb_2lLC_r128(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r128(const std::array<dd_real,31>&);
#endif
