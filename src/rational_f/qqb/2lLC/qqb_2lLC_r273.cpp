#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r273(const std::array<T,31>& k) {
  T z[48];
  T r = 0;

    z[1]=k[1];
    z[2]=k[9];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[2];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=k[4];
    z[9]=k[7];
    z[10]=k[15];
    z[11]=k[5];
    z[12]=k[12];
    z[13]=k[14];
    z[14]=2*z[7];
    z[15]=2*z[4];
    z[16]=z[11]*z[13];
    z[17]=z[16]*z[12];
    z[18]=3*z[13];
    z[19]=z[7]*z[6];
    z[20]= - static_cast<T>(7)+ 6*z[19];
    z[20]=z[3]*z[20];
    z[20]=2*z[17] + z[20] + z[18] + z[14] + z[15] - z[9];
    z[21]=npow(z[6],3);
    z[22]=z[21]*z[7];
    z[23]=z[6] - z[5];
    z[24]= - z[6]*z[23];
    z[24]=z[24] + z[22];
    z[25]=6*z[3];
    z[24]=z[24]*z[25];
    z[26]=3*z[7];
    z[27]=z[26] - z[12];
    z[28]=3*z[11];
    z[29]=z[27]*z[28];
    z[29]=static_cast<T>(2)+ z[29];
    z[29]=z[11]*z[29];
    z[30]=npow(z[11],3);
    z[31]=z[30]*z[10];
    z[32]=4*z[31];
    z[33]=z[7] - z[12];
    z[34]=z[33]*z[32];
    z[35]=npow(z[6],2);
    z[36]=z[35]*z[7];
    z[24]=z[34] + z[29] + z[24] - 11*z[36] - 2*z[5] + 5*z[6];
    z[29]=2*z[2];
    z[24]=z[24]*z[29];
    z[34]=z[5] - 3*z[6];
    z[34]=9*z[34] + 25*z[36];
    z[34]=z[3]*z[34];
    z[37]=z[17] + z[13];
    z[38]=7*z[7];
    z[37]= - 10*z[12] - z[38] + 6*z[37];
    z[37]=z[11]*z[37];
    z[39]=npow(z[11],2);
    z[40]=z[39]*z[10];
    z[41]= - 11*z[12] + z[38];
    z[41]=z[41]*z[40];
    z[24]=z[24] + z[41] + z[37] + z[34] - static_cast<T>(5)- 19*z[19];
    z[24]=z[2]*z[24];
    z[34]=17*z[4];
    z[37]=z[34] - 13*z[9];
    z[37]=z[37]*z[39];
    z[41]=z[9] - z[4];
    z[31]=z[41]*z[31];
    z[31]=z[37] - 8*z[31];
    z[31]=z[10]*z[31];
    z[37]=7*z[12];
    z[42]= - z[37] + 13*z[4];
    z[43]=z[26] - 9*z[9] + z[42];
    z[43]=z[11]*z[43];
    z[31]=z[43] + z[31];
    z[31]=z[10]*z[31];
    z[20]=z[24] + 2*z[20] + z[31];
    z[20]=z[2]*z[20];
    z[24]=3*z[3];
    z[31]=z[9]*z[8];
    z[43]= - z[31]*z[24];
    z[44]=z[14] - z[34];
    z[45]=z[44]*z[16];
    z[46]= - z[4]*z[39]*z[18];
    z[46]= - z[31] + z[46];
    z[47]=2*z[10];
    z[46]=z[46]*z[47];
    z[43]=z[46] + z[45] + z[43] + z[12] - 3*z[9];
    z[43]=z[10]*z[43];
    z[34]= - z[34] + 8*z[7];
    z[45]=z[13]*z[34];
    z[43]=z[45] + z[43];
    z[43]=z[10]*z[43];
    z[45]=2*z[9];
    z[37]=z[38] + z[45] + z[37] - 16*z[4];
    z[37]=z[11]*z[37];
    z[46]=5*z[9];
    z[42]=z[7] + z[46] - z[42];
    z[42]=z[42]*z[39];
    z[32]=z[41]*z[32];
    z[32]=z[42] + z[32];
    z[32]=z[10]*z[32];
    z[32]=z[37] + z[32];
    z[32]=z[10]*z[32];
    z[30]= - z[33]*z[30]*z[47];
    z[27]=z[27]*z[39];
    z[27]= - 3*z[27] + z[30];
    z[27]=z[10]*z[27];
    z[30]= - z[36] + z[23];
    z[30]=z[3]*z[30];
    z[30]=static_cast<T>(1)+ z[30];
    z[27]=3*z[30] + z[27];
    z[27]=z[27]*z[29];
    z[30]=z[6]*z[4];
    z[30]= - 2*z[19] + static_cast<T>(3)+ z[30];
    z[37]=2*z[3];
    z[30]=z[30]*z[37];
    z[39]=3*z[17];
    z[27]=z[27] + z[32] + z[39] - 9*z[4] + z[30];
    z[27]=z[2]*z[27];
    z[30]=4*z[12];
    z[32]=z[46] + z[30] - z[44];
    z[32]=z[11]*z[32];
    z[41]=z[4] + z[9];
    z[40]=z[41]*z[40];
    z[32]=z[32] + 6*z[40];
    z[32]=z[10]*z[32];
    z[40]=z[45] + z[12];
    z[32]=z[32] + z[40] - z[34];
    z[32]=z[10]*z[32];
    z[34]=z[12]*z[18];
    z[41]=8*z[3];
    z[42]=z[4]*z[41];
    z[27]=z[27] + z[32] + z[34] + z[42];
    z[27]=z[2]*z[27];
    z[32]= - z[4] + z[7];
    z[32]=z[13]*z[32]*z[25];
    z[34]= - z[1]*z[3]*npow(z[2],2)*z[15];
    z[27]=z[34] + z[27] + z[32] + z[43];
    z[27]=z[1]*z[27];
    z[32]=z[9]*npow(z[8],2);
    z[34]=z[32] - z[8];
    z[25]=z[34]*z[25];
    z[42]=z[16]*z[4];
    z[43]= - z[45] + 5*z[13];
    z[43]=3*z[43] - 38*z[42];
    z[43]=z[11]*z[43];
    z[44]= - 2*z[42] - z[45] + z[13];
    z[44]=z[11]*z[44];
    z[44]=static_cast<T>(1)+ z[44];
    z[28]=z[44]*z[28];
    z[28]=2*z[34] + z[28];
    z[28]=z[28]*z[47];
    z[25]=z[28] + z[43] + z[25] + static_cast<T>(5)+ 13*z[31];
    z[25]=z[10]*z[25];
    z[28]=z[31]*z[41];
    z[25]=z[25] - 46*z[42] + z[28] + 9*z[13] - 2*z[12] + z[9];
    z[25]=z[10]*z[25];
    z[26]= - z[15] - z[26];
    z[28]= - z[12] + 4*z[4];
    z[43]=4*z[7] + z[28];
    z[43]=z[8]*z[43];
    z[43]= - z[31] + z[43];
    z[43]=z[13]*z[43];
    z[26]=2*z[26] + z[43];
    z[26]=z[3]*z[26];
    z[43]=3*z[12];
    z[44]= - z[9] - z[43] - 20*z[4];
    z[44]=z[13]*z[44];
    z[20]=z[27] + z[20] + z[25] + z[44] + z[26];
    z[20]=z[1]*z[20];
    z[25]=z[13]*z[5];
    z[26]= - 21*z[42] + 15*z[13] + 11*z[4] + 8*z[9];
    z[26]=z[11]*z[26];
    z[26]=z[26] - 9*z[25] - static_cast<T>(17)+ z[31];
    z[26]=z[11]*z[26];
    z[27]= - 3*z[42] + z[18] + z[4] + z[45];
    z[27]=z[11]*z[27];
    z[25]= - z[25] - static_cast<T>(1)+ z[31];
    z[25]=3*z[25] + z[27];
    z[25]=z[11]*z[25];
    z[27]=z[5] - z[8];
    z[25]=3*z[27] + z[25];
    z[25]=z[11]*z[25];
    z[27]=2*z[8];
    z[44]=z[27] - z[5];
    z[44]=z[44]*z[8];
    z[45]=z[9]*npow(z[8],3);
    z[44]=z[44] - z[45];
    z[25]=z[25] + z[44];
    z[25]=z[25]*z[47];
    z[44]=z[44]*z[24];
    z[25]=z[25] + z[26] + z[44] - 10*z[32] + 7*z[5] + 3*z[8];
    z[25]=z[10]*z[25];
    z[26]= - z[34]*z[41];
    z[15]=z[15] + z[9];
    z[15]=3*z[15] + 7*z[13];
    z[15]=2*z[15] - 29*z[42];
    z[15]=z[11]*z[15];
    z[32]=z[18]*z[5];
    z[15]=z[25] + z[15] + z[26] - z[32] - static_cast<T>(9)- 8*z[31];
    z[15]=z[10]*z[15];
    z[25]= - z[6]*z[12];
    z[25]=z[25] + 3*z[19];
    z[26]=z[11]*z[33];
    z[25]=3*z[25] - 2*z[26];
    z[25]=z[11]*z[25];
    z[26]=4*z[6];
    z[25]=z[25] + z[26] - 15*z[36];
    z[25]=z[11]*z[25];
    z[23]=z[23]*z[35];
    z[33]= - z[7]*npow(z[6],4);
    z[23]=z[23] + z[33];
    z[23]=z[23]*z[24];
    z[26]=z[5] - z[26];
    z[26]=z[6]*z[26];
    z[22]=z[25] + z[23] + 2*z[26] + 11*z[22];
    z[22]=z[22]*z[29];
    z[14]= - z[12] + z[14];
    z[14]=z[39] + 5*z[14] + z[18];
    z[14]=z[11]*z[14];
    z[14]=z[14] + z[32] + static_cast<T>(1)- 41*z[19];
    z[14]=z[11]*z[14];
    z[18]= - z[21]*z[38];
    z[21]=3*z[5];
    z[23]= - z[21] + 7*z[6];
    z[23]=z[6]*z[23];
    z[18]=z[23] + z[18];
    z[18]=z[18]*z[24];
    z[14]=z[22] + z[14] + z[18] + 52*z[36] - z[21] - 25*z[6];
    z[14]=z[2]*z[14];
    z[18]= - 29*z[36] - z[21] + 23*z[6];
    z[18]=z[3]*z[18];
    z[17]=z[17] - 2*z[13] + z[30] - 17*z[7];
    z[17]=z[11]*z[17];
    z[19]= - static_cast<T>(2)+ 23*z[19];
    z[14]=z[14] + z[17] + z[18] + 2*z[19] + z[32];
    z[14]=z[2]*z[14];
    z[17]=static_cast<T>(2)- z[31];
    z[18]= - z[27] - 9*z[6];
    z[18]=z[7]*z[18];
    z[17]=2*z[17] + z[18];
    z[17]=z[17]*z[37];
    z[18]= - z[43] - 14*z[4];
    z[16]=z[18]*z[16];
    z[18]=z[13]*z[8]*z[28];
    z[14]=z[20] + z[14] + z[15] + z[16] + z[17] + z[18] + 14*z[7] + 
    z[40];

    r += 2*z[14]*z[1];
 
    return r;
}

template double qqb_2lLC_r273(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lLC_r273(const std::array<dd_real,31>&);
#endif
