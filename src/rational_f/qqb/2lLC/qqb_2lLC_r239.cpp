#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r239(const std::array<T,31>& k) {
  T z[65];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[12];
    z[4]=k[15];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[14];
    z[8]=k[11];
    z[9]=k[13];
    z[10]=k[2];
    z[11]=k[4];
    z[12]=k[10];
    z[13]=npow(z[11],2);
    z[14]=z[13]*z[6];
    z[15]=2*z[14];
    z[16]=z[11]*z[12];
    z[17]=z[16] + 2;
    z[18]=z[17]*z[15];
    z[19]=z[6]*z[11];
    z[20]=z[7]*z[8];
    z[21]=z[20]*z[5];
    z[22]=z[21] - z[8];
    z[22]= - z[7] + 2*z[22];
    z[22]=z[5]*z[22];
    z[22]=z[22] - static_cast<T>(1)+ 2*z[19];
    z[22]=z[5]*z[22];
    z[23]=npow(z[10],2);
    z[24]=z[23]*z[12];
    z[25]=6*z[24];
    z[26]=static_cast<T>(5)+ 4*z[16];
    z[27]= - z[11]*z[26];
    z[18]=z[22] + z[18] + z[25] + z[27];
    z[22]=2*z[4];
    z[18]=z[18]*z[22];
    z[27]=static_cast<T>(11)+ 9*z[16];
    z[27]=z[27]*z[19];
    z[28]=3*z[5];
    z[29]=z[20]*z[28];
    z[30]=z[7] + z[8];
    z[31]=z[29] + 2*z[6] - z[30];
    z[31]=z[5]*z[31];
    z[32]=z[7]*z[10];
    z[33]=2*z[32];
    z[34]=z[12]*z[10];
    z[18]=z[18] + z[31] + z[27] - 13*z[16] - z[33] - static_cast<T>(5)+ 6*z[34];
    z[18]=z[4]*z[18];
    z[27]=7*z[12];
    z[31]=4*z[3];
    z[35]=2*z[7];
    z[36]=static_cast<T>(2)+ 5*z[16];
    z[36]=z[6]*z[36];
    z[18]=z[18] + z[29] + z[36] - z[31] - z[27] - z[35];
    z[18]=z[4]*z[18];
    z[29]=npow(z[6],2);
    z[36]=z[5]*z[3];
    z[37]=z[29]*z[36];
    z[38]=z[8]*z[9];
    z[39]=8*z[38];
    z[40]= - 37*z[3] + 18*z[6];
    z[40]=z[6]*z[40];
    z[40]=18*z[37] - z[39] + z[40];
    z[40]=z[5]*z[40];
    z[41]=8*z[8];
    z[42]=24*z[3];
    z[43]=z[41] - z[42];
    z[44]=3*z[6];
    z[45]= - static_cast<T>(5)- 4*z[19];
    z[45]=z[45]*z[44];
    z[46]=3*z[9];
    z[40]=z[40] + z[45] + z[46] - z[43];
    z[45]=2*z[5];
    z[40]=z[40]*z[45];
    z[47]=2*z[3];
    z[48]=z[47] - z[6];
    z[49]=z[48]*z[6];
    z[49]=z[49] - z[37];
    z[49]=z[49]*z[28];
    z[50]=5*z[3];
    z[49]=z[49] - z[50];
    z[51]=z[19] + 1;
    z[52]=z[51]*z[44];
    z[52]=z[49] + z[52];
    z[53]=npow(z[5],2);
    z[54]=z[52]*z[53];
    z[55]=z[3] - z[9];
    z[55]=z[55]*npow(z[5],3);
    z[56]=4*z[4];
    z[57]=z[56]*z[55];
    z[54]=3*z[54] + z[57];
    z[57]=2*z[2];
    z[54]=z[54]*z[57];
    z[58]=z[13]*z[44];
    z[58]= - 10*z[11] + z[58];
    z[58]=z[6]*z[58];
    z[59]=5*z[9] - 7*z[3];
    z[59]=z[5]*z[59];
    z[59]= - static_cast<T>(2)+ z[59];
    z[59]=z[5]*z[59];
    z[59]= - 2*z[10] + z[59];
    z[59]=z[59]*z[22];
    z[60]=z[11]*z[46];
    z[60]= - static_cast<T>(1)+ z[60];
    z[40]=z[54] + z[59] + z[40] + 2*z[60] + z[58];
    z[40]=z[2]*z[40];
    z[54]=9*z[37];
    z[58]=9*z[6];
    z[59]=17*z[3] - z[58];
    z[59]=z[6]*z[59];
    z[59]= - z[54] + 12*z[38] + z[59];
    z[59]=z[59]*z[45];
    z[60]=z[7] - z[12];
    z[61]= - z[60]*npow(z[10],3);
    z[62]=z[61]*z[56];
    z[23]=z[23]*z[7];
    z[62]=z[62] + 5*z[24] - 7*z[23];
    z[62]=z[4]*z[62];
    z[63]= - z[46] + z[50];
    z[63]=z[5]*z[63];
    z[33]=z[62] - z[33] + z[63];
    z[33]=z[33]*z[22];
    z[60]=z[11]*z[60];
    z[60]=12*z[19] + static_cast<T>(2)- 3*z[60];
    z[60]=z[6]*z[60];
    z[27]=z[40] + z[33] + z[59] + z[60] - z[42] + 5*z[7] + 16*z[8] - 
    z[27] - 6*z[9];
    z[27]=z[2]*z[27];
    z[33]= - z[61]*z[22];
    z[40]= - z[9] + z[50];
    z[40]=z[5]*z[40];
    z[40]=static_cast<T>(4)+ z[40];
    z[40]=z[5]*z[40];
    z[33]=z[33] + z[40] + 5*z[23] + 4*z[10] - z[24];
    z[33]=z[4]*z[33];
    z[40]=z[32] - static_cast<T>(1)+ z[34];
    z[42]=z[9] - 15*z[3];
    z[42]=z[5]*z[42];
    z[33]=z[33] + 2*z[40] + z[42];
    z[33]=z[33]*z[22];
    z[40]=3*z[3];
    z[42]= - z[40] + z[6];
    z[42]=z[42]*z[44];
    z[59]=2*z[38];
    z[42]= - z[59] + z[42];
    z[42]=2*z[42] + z[54];
    z[42]=z[42]*z[45];
    z[45]=z[44] + z[49];
    z[45]=z[45]*z[28];
    z[49]=z[5]*z[47];
    z[49]=static_cast<T>(1)+ z[49];
    z[49]=z[49]*z[28];
    z[55]= - z[4]*z[55];
    z[49]=z[49] + z[55];
    z[49]=z[49]*z[22];
    z[45]=z[45] + z[49];
    z[45]=z[45]*z[57];
    z[49]=3*z[19];
    z[55]=z[11]*z[7];
    z[60]= - static_cast<T>(2)- z[55];
    z[60]=z[60]*z[49];
    z[62]=static_cast<T>(2)+ 5*z[55];
    z[60]=2*z[62] + z[60];
    z[60]=z[6]*z[60];
    z[62]=9*z[7];
    z[33]=z[45] + z[33] + z[42] + z[60] - z[62] + 3*z[12] - z[43];
    z[33]=z[2]*z[33];
    z[42]=3*z[34];
    z[43]= - z[4]*z[25];
    z[43]=z[43] - 4*z[36] - z[42] + z[32];
    z[43]=z[4]*z[43];
    z[43]=z[43] + 6*z[3] + 2*z[12] + 3*z[7];
    z[43]=z[43]*z[22];
    z[45]= - static_cast<T>(2)- 3*z[55];
    z[45]=z[45]*z[44];
    z[45]=20*z[7] + z[45];
    z[45]=z[6]*z[45];
    z[33]=z[33] + z[45] + z[43];
    z[33]=z[2]*z[33];
    z[43]=z[47]*z[6];
    z[45]=z[43] - z[37];
    z[45]=z[45]*z[28];
    z[55]=3*z[36];
    z[60]= - static_cast<T>(2)- z[55];
    z[60]=z[5]*z[60];
    z[60]= - z[10] + z[60];
    z[60]=z[4]*z[60];
    z[60]=z[60] + static_cast<T>(1)+ 6*z[36];
    z[60]=z[60]*z[22];
    z[45]=z[60] - z[50] + z[45];
    z[45]=z[45]*z[57];
    z[50]=z[11]*z[35];
    z[50]=static_cast<T>(1)+ z[50];
    z[50]=z[50]*z[44];
    z[50]= - 10*z[7] + z[50];
    z[50]=z[6]*z[50];
    z[57]=8*z[3];
    z[36]=static_cast<T>(3)+ 10*z[36];
    z[36]=z[4]*z[36];
    z[36]= - z[57] + z[36];
    z[36]=z[36]*z[22];
    z[36]=z[45] + z[50] + z[36];
    z[36]=z[2]*z[36];
    z[45]=z[29]*z[62];
    z[50]=z[3]*npow(z[4],2);
    z[36]=z[36] + z[45] - 8*z[50];
    z[36]=z[2]*z[36];
    z[45]=z[9] + z[8];
    z[45]=z[45]*z[47];
    z[60]=3*z[20];
    z[45]=z[45] - z[59] + z[60];
    z[45]=z[6]*z[45];
    z[63]=z[38]*z[47];
    z[45]= - z[63] + z[45];
    z[45]=z[6]*z[45];
    z[55]= - static_cast<T>(1)- z[55];
    z[55]=z[4]*z[55];
    z[55]=z[47] + z[55];
    z[55]=z[2]*z[55]*z[56];
    z[56]=3*z[29];
    z[64]= - z[7]*z[56];
    z[55]=z[55] + z[64] + 10*z[50];
    z[55]=z[55]*npow(z[2],2);
    z[63]= - z[29]*z[63];
    z[50]=z[1]*npow(z[2],3)*z[50];
    z[50]= - 4*z[50] + z[63] + z[55];
    z[50]=z[1]*z[50];
    z[36]=z[50] + z[45] + z[36];
    z[36]=z[1]*z[36];
    z[45]= - static_cast<T>(1)- z[16];
    z[45]=z[45]*z[44];
    z[50]= - z[17]*z[19];
    z[55]= - z[6] + z[21];
    z[55]=z[5]*z[55];
    z[50]=z[50] + z[55];
    z[22]=z[50]*z[22];
    z[22]=z[22] + z[21] + z[47] + z[45];
    z[22]=z[4]*z[22];
    z[22]= - z[20] + z[22];
    z[22]=z[4]*z[22];
    z[45]=z[47]*z[9];
    z[45]=z[45] - z[59];
    z[47]= - z[47] - z[8] - z[62];
    z[47]=z[6]*z[47];
    z[47]=z[47] - z[20] + z[45];
    z[47]=z[6]*z[47];
    z[45]=z[45] - z[60];
    z[29]= - z[5]*z[45]*z[29];
    z[22]=z[36] + z[33] + z[22] + z[47] + z[29];
    z[22]=z[1]*z[22];
    z[29]= - z[11]*z[38];
    z[29]=z[29] - z[9] + z[8];
    z[29]=z[29]*z[57];
    z[33]= - z[35] + z[6];
    z[33]=z[33]*z[44];
    z[35]=z[43] - z[45];
    z[35]=z[5]*z[6]*z[35];
    z[18]=z[22] + z[27] + z[18] + z[35] + z[29] + z[33];
    z[18]=z[1]*z[18];
    z[21]=z[21] - z[30];
    z[21]=z[5]*z[21];
    z[22]=z[32] + 1;
    z[27]=z[11]*z[8];
    z[21]=z[21] + z[27] + z[22];
    z[21]=z[5]*z[21];
    z[27]=z[14] - z[11];
    z[21]=z[21] - z[10] - z[27];
    z[21]=z[5]*z[21];
    z[29]=7*z[34];
    z[26]= - z[29] + z[26];
    z[26]=z[11]*z[26];
    z[25]=z[26] - 5*z[10] + z[25];
    z[25]=z[11]*z[25];
    z[17]= - z[6]*z[17]*npow(z[11],3);
    z[17]=z[21] + z[17] - 2*z[61] + z[25];
    z[17]=z[4]*z[17];
    z[20]=z[53]*z[20];
    z[20]=z[20] - z[32] - z[51];
    z[20]=z[5]*z[20];
    z[21]=z[24] + z[23];
    z[23]=8*z[16] + static_cast<T>(4)- z[29];
    z[23]=z[11]*z[23];
    z[24]=3*z[16];
    z[25]= - static_cast<T>(4)- z[24];
    z[25]=z[25]*z[14];
    z[17]=z[17] + z[20] + z[25] + 2*z[21] + z[23];
    z[17]=z[4]*z[17];
    z[20]= - static_cast<T>(2)- z[24];
    z[20]=z[20]*z[19];
    z[17]=z[17] + z[20] + 6*z[16] - z[42] + z[22];
    z[17]=z[4]*z[17];
    z[20]=z[48]*z[44];
    z[20]= - z[20] + 3*z[37];
    z[21]=4*z[38];
    z[22]=z[21] - z[20];
    z[22]=z[22]*z[28];
    z[23]=z[51]*z[58];
    z[22]=z[22] + z[23] - 12*z[3] + z[41] - z[46];
    z[22]=z[5]*z[22];
    z[23]= - 19*z[3] + z[58];
    z[23]=z[6]*z[23];
    z[23]=z[54] - z[21] + z[23];
    z[23]=z[5]*z[23];
    z[24]= - z[8] + z[40];
    z[19]= - static_cast<T>(10)- 9*z[19];
    z[19]=z[6]*z[19];
    z[19]=z[23] + 4*z[24] + z[19];
    z[19]=z[5]*z[19];
    z[23]=2*z[9];
    z[24]=z[23] + z[52];
    z[24]=z[5]*z[24];
    z[25]= - z[13]*z[56];
    z[24]=z[25] + z[24];
    z[24]=z[5]*z[24];
    z[13]=z[13]*z[9];
    z[13]=z[13] - z[14];
    z[13]=3*z[13];
    z[14]=z[13] + z[24];
    z[14]=z[2]*z[14];
    z[24]=z[11]*z[9];
    z[15]= - z[11] + z[15];
    z[15]=z[6]*z[15];
    z[15]=z[24] + z[15];
    z[14]=z[14] + 3*z[15] + z[19];
    z[14]=z[5]*z[14];
    z[13]= - z[13] + z[14];
    z[13]=z[2]*z[13];
    z[14]= - z[6]*z[27];
    z[14]= - z[24] + z[14];
    z[13]=z[13] + 3*z[14] + z[22];
    z[13]=z[2]*z[13];
    z[14]= - z[39] + z[20];
    z[14]=z[5]*z[14];
    z[15]= - z[49] - static_cast<T>(3)- z[16];
    z[15]=z[6]*z[15];
    z[16]= - z[11]*z[21];
    z[13]=z[13] + z[17] + z[14] + z[15] + z[31] + z[16] + z[12] - z[23];
    z[13]=2*z[13] + z[18];

    r += z[13]*z[1];
 
    return r;
}

template double qqb_2lLC_r239(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lLC_r239(const std::array<dd_real,31>&);
#endif
