#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r252(const std::array<T,31>& k) {
  T z[46];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[10];
    z[7]=k[14];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[28];
    z[12]=k[15];
    z[13]=k[2];
    z[14]= - z[5] + z[4];
    z[15]=z[9]*z[6];
    z[14]=z[15]*z[14];
    z[16]=z[4]*z[5];
    z[17]= - z[10]*z[16];
    z[14]=z[17] + z[14];
    z[17]=n<T>(4,3)*z[7];
    z[14]=z[14]*z[17];
    z[18]=z[3]*z[11];
    z[19]=z[5]*z[6];
    z[20]= - 7*z[6] + 17*z[5];
    z[20]=z[4]*z[20];
    z[14]=z[14] + n<T>(1,3)*z[20] + z[18] + n<T>(7,3)*z[19];
    z[14]=z[7]*z[14];
    z[20]=n<T>(1,3)*z[16];
    z[21]=z[6]*z[20];
    z[14]=z[21] + z[14];
    z[14]=z[7]*z[14];
    z[21]=z[5]*z[3];
    z[22]= - z[21] + z[16];
    z[22]=z[10]*z[22];
    z[23]=z[3]*z[8];
    z[24]=static_cast<T>(1)+ z[23];
    z[24]=z[4]*z[24];
    z[22]=z[22] - z[3] + z[24];
    z[24]=n<T>(4,3)*z[2];
    z[22]=z[22]*z[24];
    z[25]=n<T>(7,3)*z[3] + z[5];
    z[25]=z[4]*z[25];
    z[26]=z[7]*z[3];
    z[22]=z[22] + 4*z[26] - z[21] + z[25];
    z[22]=z[2]*z[22];
    z[25]=n<T>(1,3)*z[3];
    z[26]=z[16]*z[25];
    z[27]=2*z[3];
    z[28]=npow(z[7],2);
    z[29]=z[28]*z[27];
    z[22]=z[22] + z[26] + z[29];
    z[22]=z[2]*z[22];
    z[26]= - z[6] + z[5];
    z[26]=z[4]*z[26];
    z[19]=z[19] + z[26];
    z[19]=z[19]*z[17];
    z[26]=z[16]*z[6];
    z[19]=z[26] + z[19];
    z[19]=z[19]*z[28];
    z[28]= - z[3] - z[5];
    z[28]=z[4]*z[28];
    z[28]=z[21] + z[28];
    z[28]=z[28]*z[24];
    z[29]=z[16]*z[3];
    z[28]= - z[29] + z[28];
    z[28]=z[28]*npow(z[2],2);
    z[26]=npow(z[7],3)*z[26];
    z[29]=npow(z[2],3)*z[29];
    z[26]=z[26] + z[29];
    z[26]=z[1]*z[26];
    z[19]=n<T>(4,3)*z[26] + z[19] + z[28];
    z[19]=z[1]*z[19];
    z[14]=z[19] + z[14] + z[22];
    z[14]=z[1]*z[14];
    z[19]=z[5]*z[12];
    z[22]=z[19] - z[16];
    z[26]=z[3]*z[12];
    z[28]= - z[26] + z[22];
    z[28]=z[10]*z[28];
    z[29]=2*z[4];
    z[28]=z[28] - z[3] + z[29];
    z[28]=z[10]*z[28];
    z[30]=z[4]*z[9];
    z[31]=z[3]*z[9];
    z[28]=z[28] + z[31] + z[30];
    z[28]=z[28]*z[17];
    z[18]=z[18] + n<T>(16,3)*z[16];
    z[18]=z[10]*z[18];
    z[32]= - static_cast<T>(2)- z[15];
    z[32]=z[11]*z[32];
    z[33]=n<T>(14,3) - z[15];
    z[33]=z[5]*z[33];
    z[18]=z[28] + z[18] - 7*z[4] + z[33] - z[25] + z[32];
    z[18]=z[7]*z[18];
    z[28]=2*z[12];
    z[32]=z[5]*z[28];
    z[32]= - z[26] + z[32];
    z[33]=n<T>(4,3)*z[6];
    z[34]= - z[33] - 11*z[5];
    z[34]=z[4]*z[34];
    z[18]=z[18] + n<T>(4,3)*z[32] + z[34];
    z[18]=z[7]*z[18];
    z[32]=z[15]*z[12];
    z[34]= - z[28] - z[32];
    z[34]=z[9]*z[34];
    z[22]=z[22]*z[10];
    z[35]=z[22] + z[4];
    z[36]= - z[12] - z[35];
    z[36]=z[10]*z[36];
    z[37]=z[4]*z[8];
    z[34]=z[36] + z[34] - z[37];
    z[24]=z[34]*z[24];
    z[34]=4*z[16];
    z[36]=z[21] - z[34];
    z[36]=z[10]*z[36];
    z[38]=z[10]*z[3];
    z[39]=4*z[38];
    z[40]=z[39] - static_cast<T>(3)+ z[15];
    z[40]=z[7]*z[40];
    z[41]=z[32] + z[12];
    z[24]=z[24] + z[40] + z[36] - n<T>(7,3)*z[4] + n<T>(2,3)*z[41] - 3*z[3];
    z[24]=z[2]*z[24];
    z[36]=4*z[4];
    z[40]= - z[25] + 2*z[5];
    z[40]=z[40]*z[36];
    z[42]=3*z[15];
    z[38]=2*z[38] + static_cast<T>(1)+ z[42];
    z[38]=z[7]*z[38];
    z[27]=z[27] + z[38];
    z[27]=z[7]*z[27];
    z[38]=z[12]*z[6];
    z[43]=n<T>(8,3)*z[12];
    z[44]= - z[43] - z[3];
    z[44]=z[5]*z[44];
    z[24]=z[24] + z[27] + z[40] - n<T>(4,3)*z[38] + z[44];
    z[24]=z[2]*z[24];
    z[14]=z[14] + z[18] + z[24];
    z[14]=z[1]*z[14];
    z[18]=4*z[32];
    z[24]=5*z[6];
    z[27]= - z[24] - 14*z[12];
    z[27]=n<T>(1,3)*z[27] - z[18];
    z[27]=z[9]*z[27];
    z[22]=8*z[4] - z[22] - z[12];
    z[22]=z[10]*z[22];
    z[38]=z[10]*z[4];
    z[40]=z[38] + 1;
    z[44]=n<T>(1,3)*z[37] - z[40];
    z[44]=z[10]*z[44];
    z[45]=n<T>(2,3)*z[32] + n<T>(1,3)*z[6] + z[12];
    z[45]=z[9]*z[45];
    z[45]=n<T>(2,3) + z[45];
    z[45]=z[9]*z[45];
    z[44]=z[44] + n<T>(1,3)*z[8] + z[45];
    z[44]=z[2]*z[44];
    z[22]=4*z[44] + n<T>(2,3)*z[22] + static_cast<T>(3)+ z[27];
    z[22]=z[2]*z[22];
    z[21]=z[21] - z[20];
    z[21]=z[10]*z[21];
    z[27]=z[39] + static_cast<T>(10)- z[42];
    z[27]=z[7]*z[27];
    z[23]= - static_cast<T>(5)+ z[23];
    z[23]=z[4]*z[23];
    z[23]=z[23] + z[3];
    z[18]=z[22] + z[27] + z[21] + n<T>(13,3)*z[5] + z[18] - z[6] + z[28] + 4.
   /3.*z[23];
    z[18]=z[2]*z[18];
    z[20]=z[20] + z[26] - n<T>(1,3)*z[19];
    z[21]=2*z[10];
    z[20]=z[20]*z[21];
    z[22]=z[26]*z[10];
    z[23]=z[22] + z[3];
    z[21]= - z[23]*z[21];
    z[21]=z[21] - z[30] + static_cast<T>(1)+ 2*z[31];
    z[17]=z[21]*z[17];
    z[17]=z[17] + z[20] + z[25] - z[36];
    z[17]=z[10]*z[17];
    z[17]=n<T>(1,3) + z[17];
    z[17]=z[7]*z[17];
    z[20]=static_cast<T>(1)+ n<T>(1,3)*z[15];
    z[20]=z[20]*z[36];
    z[21]=z[11] - z[43];
    z[21]=z[3]*z[21];
    z[16]=z[21] - n<T>(17,3)*z[16];
    z[16]=z[10]*z[16];
    z[21]=z[11]*z[15];
    z[27]= - n<T>(22,3) + z[15];
    z[27]=z[5]*z[27];
    z[16]=z[17] + z[16] + z[20] + z[27] - n<T>(8,3)*z[3] + n<T>(13,3)*z[6] + 
    z[21];
    z[16]=z[7]*z[16];
    z[17]=z[31] + static_cast<T>(3)+ z[15];
    z[17]=z[5]*z[17];
    z[20]=n<T>(2,3)*z[6];
    z[17]=z[17] - z[25] - z[20] - z[12];
    z[17]=z[17]*z[29];
    z[21]=z[3] - z[11] - z[33];
    z[21]=z[28]*z[21];
    z[27]= - z[6]*z[11];
    z[29]=4*z[12];
    z[33]=z[6] - z[29];
    z[33]=z[5]*z[33];
    z[14]=z[14] + z[18] + z[16] + z[17] + z[33] + z[27] + z[21];
    z[14]=z[1]*z[14];
    z[16]=z[13]*z[28];
    z[16]= - 2*z[30] + z[16] + z[31];
    z[17]= - z[34] - 5*z[26] + 4*z[19];
    z[18]=n<T>(1,3)*z[10];
    z[17]=z[17]*z[18];
    z[19]=n<T>(2,3)*z[4];
    z[21]= - z[19] + z[12] - z[25];
    z[17]=2*z[21] + z[17];
    z[17]=z[10]*z[17];
    z[21]= - z[4] + z[12] - z[23];
    z[21]=z[10]*z[21];
    z[21]=z[21] + z[31] - z[30];
    z[21]=z[7]*z[21];
    z[23]= - z[28] + z[4] + z[3];
    z[21]=n<T>(2,3)*z[21] + n<T>(1,3)*z[23] + z[22];
    z[21]=z[7]*z[21]*npow(z[10],2);
    z[16]=z[21] + n<T>(2,3)*z[16] + z[17];
    z[16]=z[7]*z[16];
    z[17]=z[6]*z[8];
    z[21]= - z[6] - z[41];
    z[21]=z[9]*z[21];
    z[21]=z[21] - static_cast<T>(1)+ z[17];
    z[21]=z[9]*z[21];
    z[21]= - z[8] + z[21];
    z[21]=z[9]*z[21];
    z[22]=z[37] - z[40];
    z[22]=z[10]*z[22];
    z[22]=z[22] + z[8] + z[9];
    z[22]=z[10]*z[22];
    z[21]=z[21] + z[22];
    z[21]=z[2]*z[21];
    z[21]=z[21] + z[8];
    z[22]=5*z[32] + z[24] + z[29];
    z[22]=z[9]*z[22];
    z[22]=n<T>(1,3)*z[22] + static_cast<T>(1)- n<T>(4,3)*z[17];
    z[22]=z[9]*z[22];
    z[23]= - static_cast<T>(1)+ z[38];
    z[18]=z[23]*z[18];
    z[18]=z[18] + z[22] + n<T>(2,3)*z[21];
    z[18]=z[2]*z[18];
    z[21]=z[10]*z[35];
    z[21]=z[37] - z[21];
    z[17]= - static_cast<T>(1)+ 4*z[17];
    z[22]= - z[6] - n<T>(4,3)*z[32];
    z[22]=z[9]*z[22];
    z[17]=z[18] + n<T>(1,3)*z[17] + 2*z[22] + n<T>(4,3)*z[21];
    z[17]=z[2]*z[17];
    z[18]= - z[13]*z[20];
    z[15]=z[15] - static_cast<T>(1)+ z[18];
    z[15]=z[28]*z[15];
    z[18]=z[9]*z[12];
    z[18]= - n<T>(2,3) - z[18];
    z[18]=z[3]*z[18];
    z[15]=z[17] + z[16] + z[19] - z[5] + z[18] - z[11] + z[20] + z[15];
    z[14]=2*z[15] + z[14];

    r += 4*z[14]*z[1];
 
    return r;
}

template double qqb_2lLC_r252(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lLC_r252(const std::array<dd_real,31>&);
#endif
