#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r91(const std::array<T,31>& k) {
  T z[113];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[30];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=k[4];
    z[9]=k[12];
    z[10]=k[20];
    z[11]=k[7];
    z[12]=k[26];
    z[13]=k[8];
    z[14]=k[23];
    z[15]=k[5];
    z[16]=k[11];
    z[17]=k[21];
    z[18]=k[15];
    z[19]=k[14];
    z[20]=k[16];
    z[21]=k[19];
    z[22]=npow(z[8],2);
    z[23]=z[22]*z[15];
    z[24]=npow(z[8],3);
    z[25]=z[23] + z[24];
    z[25]=z[25]*z[7];
    z[26]=z[24]*z[4];
    z[27]=z[15]*z[8];
    z[28]=z[25] + z[26] - 3*z[27];
    z[28]=z[7]*z[28];
    z[29]=5*z[8];
    z[30]=z[22]*z[4];
    z[31]=2*z[30];
    z[32]=5*z[15];
    z[33]=z[4]*z[8];
    z[34]=z[33] + 2;
    z[35]=z[6]*z[34];
    z[28]=z[28] + z[32] + z[35] - z[29] - z[31];
    z[28]=z[16]*z[28];
    z[35]=z[7]*z[15];
    z[36]=z[35]*z[22];
    z[37]=z[6] - z[8];
    z[38]=3*z[15];
    z[39]= - z[37]*z[38];
    z[40]=2*z[8];
    z[41]= - z[40] + z[6];
    z[41]=z[6]*z[41];
    z[39]= - z[36] + z[39] + z[22] + z[41];
    z[39]=z[19]*z[39];
    z[41]= - z[35] - 1;
    z[41]=z[24]*z[41];
    z[41]=3*z[23] + z[41];
    z[41]=z[7]*z[41];
    z[42]=2*z[22];
    z[43]=z[6]*z[8];
    z[44]=3*z[8];
    z[45]= - z[44] + z[6];
    z[45]=z[15]*z[45];
    z[41]=z[41] + z[45] + z[42] - z[43];
    z[41]=z[16]*z[41];
    z[45]=z[22]*z[7];
    z[46]=z[33] - 1;
    z[47]= - z[6]*z[46];
    z[47]= - z[45] + z[30] + z[47];
    z[48]=z[19]*z[15];
    z[49]=npow(z[6],2);
    z[50]=z[48]*z[49];
    z[51]=z[50]*z[9];
    z[39]= - z[51] + z[41] + 3*z[47] + z[39];
    z[39]=z[14]*z[39];
    z[41]=2*z[6];
    z[47]=z[41]*z[15];
    z[52]= - z[49] - z[47];
    z[52]=z[19]*z[52];
    z[53]=z[15]*z[6];
    z[54]=z[16]*z[53];
    z[52]= - z[51] + z[52] + z[54];
    z[52]=z[9]*z[52];
    z[54]=13*z[8];
    z[55]=5*z[30];
    z[56]= - z[54] - z[55];
    z[56]=z[4]*z[56];
    z[57]=z[6]*z[4];
    z[58]=z[8] + z[31];
    z[58]=z[4]*z[58];
    z[58]=static_cast<T>(7)+ z[58];
    z[58]=z[58]*z[57];
    z[59]=z[40] - z[30];
    z[59]=2*z[59] + 5*z[45];
    z[59]=z[7]*z[59];
    z[60]=z[36] + z[22];
    z[61]=2*z[27];
    z[62]=z[61] - z[60];
    z[62]=z[7]*z[62];
    z[62]=z[62] + z[37] - 6*z[15];
    z[62]=z[19]*z[62];
    z[63]=5*z[33];
    z[64]= - static_cast<T>(1)+ z[63];
    z[64]=z[4]*z[64];
    z[65]=z[7]*z[63];
    z[64]=z[64] + z[65];
    z[64]=z[1]*z[64];
    z[28]=2*z[39] + z[52] + z[64] + z[28] + z[62] + z[59] + z[58] + static_cast<T>(1)+ 
    z[56];
    z[28]=z[14]*z[28];
    z[39]=npow(z[4],3);
    z[52]=z[39]*z[6];
    z[56]=npow(z[4],2);
    z[58]=z[52] + z[56];
    z[59]=z[58]*z[6];
    z[62]=z[7]*z[59];
    z[64]=2*z[56];
    z[65]=z[39]*z[2];
    z[62]=z[65] + z[62] - z[64] - z[52];
    z[62]=z[1]*z[62];
    z[66]=z[41]*z[39];
    z[66]=z[66] + z[56];
    z[66]=z[66]*z[2];
    z[67]=z[58]*z[41];
    z[66]=z[66] - z[67];
    z[67]=z[49]*z[7];
    z[68]=z[67]*z[4];
    z[69]=npow(z[57],3);
    z[69]= - 2*z[69] + z[68];
    z[69]=z[7]*z[69];
    z[70]=3*z[4];
    z[62]=z[62] + z[69] + z[70] - z[66];
    z[62]=z[1]*z[62];
    z[69]=z[57] - 1;
    z[69]=z[69]*z[67];
    z[71]= - z[56] + z[52];
    z[71]=z[6]*z[71];
    z[71]=z[4] + z[71];
    z[71]=z[6]*z[71];
    z[71]=static_cast<T>(1)+ z[71];
    z[71]=z[6]*z[71];
    z[71]=z[71] - z[69];
    z[71]=z[7]*z[71];
    z[47]= - z[49] + z[47];
    z[47]=z[19]*z[47];
    z[72]=3*z[6];
    z[47]=z[51] - z[72] + z[47];
    z[47]=z[9]*z[47];
    z[51]=z[39]*z[49];
    z[73]= - z[4] - z[51];
    z[73]=z[6]*z[73];
    z[74]= - z[41] + z[38];
    z[74]=z[19]*z[74];
    z[75]= - z[16] - z[19] + z[4] + z[59];
    z[75]=z[2]*z[75];
    z[47]=z[47] + z[62] + z[75] + z[74] + z[71] - static_cast<T>(3)+ z[73];
    z[47]=z[3]*z[47];
    z[62]=npow(z[15],2);
    z[71]=z[62]*z[11];
    z[73]=z[15] - z[8];
    z[74]= - z[71] + z[73];
    z[74]=z[1]*z[74];
    z[75]=z[15]*z[73];
    z[76]=npow(z[15],3);
    z[77]= - z[11]*z[76];
    z[74]=z[74] + z[75] + z[77];
    z[74]=z[9]*z[74];
    z[75]=z[33] - 2;
    z[77]=z[11]*z[15];
    z[77]=z[77] + z[75];
    z[77]=z[1]*z[77];
    z[71]=z[74] + z[77] + z[40] - z[71];
    z[71]=z[9]*z[71];
    z[74]=3*z[57];
    z[68]=z[68] - static_cast<T>(1)- z[74];
    z[68]=z[7]*z[68];
    z[77]=z[11]*z[35];
    z[68]=z[77] + z[70] + z[68];
    z[68]=z[1]*z[68];
    z[77]=2*z[57];
    z[78]=static_cast<T>(1)+ z[77];
    z[78]=z[6]*z[78];
    z[69]=z[78] - z[69];
    z[69]=z[7]*z[69];
    z[78]=3*z[19];
    z[37]=z[37]*z[78];
    z[37]=z[71] + z[68] + z[37] - z[57] + z[69];
    z[37]=z[12]*z[37];
    z[68]=z[33] + 1;
    z[69]=z[68]*z[57];
    z[34]=z[69] - z[34];
    z[34]=z[6]*z[34];
    z[34]=z[34] + z[15];
    z[34]=z[19]*z[34];
    z[71]= - z[1]*z[4]*z[46];
    z[71]=z[71] + z[33];
    z[79]=3*z[7];
    z[71]=z[79]*z[71];
    z[80]= - static_cast<T>(11)+ z[33];
    z[80]=z[4]*z[80];
    z[71]= - 2*z[11] + z[80] + z[71];
    z[71]=z[1]*z[71];
    z[34]=z[71] + z[34] - static_cast<T>(3)+ z[69];
    z[34]=z[10]*z[34];
    z[69]=z[33]*z[6];
    z[69]=z[69] - z[40];
    z[69]=z[69]*z[6];
    z[69]=z[69] + z[27];
    z[69]=z[69]*z[7];
    z[71]=z[57] - z[46];
    z[71]=z[6]*z[71];
    z[80]=2*z[15];
    z[71]=z[69] - z[80] + z[8] + z[71];
    z[71]=z[7]*z[71];
    z[81]=z[2]*z[4];
    z[82]=z[3]*z[2];
    z[71]=z[82] + z[81] + z[71] - static_cast<T>(3)- z[57];
    z[71]=z[20]*z[71];
    z[28]=z[71] + z[28] + z[47] + z[37] + z[34];
    z[34]=z[30] + z[15];
    z[37]=z[40] + z[34];
    z[37]=z[18]*z[49]*z[37];
    z[47]=z[40] + z[15];
    z[71]=z[47]*z[15];
    z[71]=z[71] + z[22];
    z[82]=z[71]*z[35];
    z[83]=z[30] + z[8];
    z[83]=3*z[83];
    z[84]= - z[6]*z[68];
    z[84]=z[83] + z[84];
    z[84]=z[6]*z[84];
    z[71]= - z[37] + z[82] + z[84] + z[71];
    z[71]=z[18]*z[71];
    z[84]= - z[22] - z[27];
    z[84]=z[84]*z[35];
    z[85]=z[45]*z[62];
    z[23]=z[85] + z[23];
    z[85]= - z[13]*z[23];
    z[42]=z[85] + z[84] - z[42] - z[27];
    z[42]=z[13]*z[42];
    z[84]=z[13]*z[47];
    z[85]= - z[4] - z[13];
    z[85]=z[2]*z[85];
    z[84]=z[85] + 2*z[68] + z[84];
    z[84]=z[13]*z[84];
    z[85]=2*z[4];
    z[84]= - z[85] + z[84];
    z[84]=z[2]*z[84];
    z[86]= - z[13]*z[22];
    z[34]=z[86] - z[34];
    z[34]=z[13]*z[34];
    z[34]=z[84] + z[34] + z[63] - z[57];
    z[34]=z[2]*z[34];
    z[84]=2*z[33];
    z[86]= - static_cast<T>(1)+ z[84];
    z[86]=z[86]*z[41];
    z[87]=z[62]*z[7];
    z[34]=z[34] + z[42] + z[71] + z[87] - z[83] + z[86];
    z[42]=2*z[17];
    z[34]=z[34]*z[42];
    z[71]=z[82] + z[22];
    z[37]= - 9*z[37] + 5*z[71];
    z[71]=7*z[6];
    z[82]= - z[68]*z[71];
    z[82]=z[82] + 25*z[8] + 26*z[30];
    z[82]=z[6]*z[82];
    z[83]=z[32] + 10*z[8] - z[6];
    z[83]=z[15]*z[83];
    z[82]=z[83] + z[82] + z[37];
    z[82]=z[18]*z[82];
    z[83]= - z[54] - 15*z[30];
    z[86]=15*z[22];
    z[88]= - 17*z[8] - z[80];
    z[88]=z[15]*z[88];
    z[88]= - z[86] + z[88];
    z[88]=z[7]*z[88];
    z[89]= - static_cast<T>(7)+ 27*z[33];
    z[89]=z[6]*z[89];
    z[82]=z[82] + z[88] - z[80] + 2*z[83] + z[89];
    z[82]=z[18]*z[82];
    z[83]=3*z[22];
    z[88]=z[83] + z[27];
    z[89]=7*z[27];
    z[90]=12*z[22] + z[89];
    z[90]=z[90]*z[35];
    z[91]=9*z[13];
    z[92]=z[23]*z[91];
    z[88]=z[92] + 7*z[88] + z[90];
    z[88]=z[13]*z[88];
    z[86]=z[86] + 17*z[27];
    z[86]=z[7]*z[86];
    z[90]=19*z[8];
    z[86]=z[88] + z[86] + z[32] + z[90] + 20*z[30];
    z[86]=z[13]*z[86];
    z[88]=z[22]*z[91];
    z[88]=z[88] + z[80] - 16*z[8] + z[55];
    z[88]=z[13]*z[88];
    z[92]=37*z[33];
    z[88]=z[88] - static_cast<T>(16)- z[92];
    z[88]=z[13]*z[88];
    z[93]= - z[47]*z[91];
    z[94]= - static_cast<T>(1)- z[84];
    z[93]=5*z[94] + z[93];
    z[93]=z[13]*z[93];
    z[93]=17*z[4] + z[93];
    z[93]=z[13]*z[93];
    z[94]=5*z[4];
    z[95]=z[94] + z[91];
    z[96]=npow(z[13],2);
    z[95]=z[2]*z[95]*z[96];
    z[93]=z[93] + z[95];
    z[93]=z[2]*z[93];
    z[88]=z[93] + 19*z[4] + z[88];
    z[88]=z[2]*z[88];
    z[34]=z[34] + z[88] + z[86] + z[82] + 7*z[57] + static_cast<T>(4)- 49*z[33];
    z[34]=z[17]*z[34];
    z[77]= - z[77] - z[75];
    z[77]=z[6]*z[77];
    z[69]= - z[69] + z[8] + z[77];
    z[69]=z[69]*z[79];
    z[77]=static_cast<T>(47)+ 6*z[33];
    z[77]=z[77]*z[57];
    z[69]=z[69] + z[77] - static_cast<T>(44)- 9*z[33];
    z[69]=z[19]*z[69];
    z[34]=z[69] + z[34];
    z[69]= - 2*z[46] - z[57];
    z[69]=z[6]*z[69];
    z[77]=4*z[8];
    z[82]=19*z[15];
    z[69]= - z[82] + z[69] + z[77] - z[30];
    z[69]=z[69]*z[79];
    z[86]=5*z[6];
    z[88]=9*z[15];
    z[93]= - z[86] + z[88];
    z[95]=z[15] - z[6];
    z[97]=z[95]*z[35];
    z[93]=2*z[93] + 3*z[97];
    z[97]=z[41] - z[15];
    z[97]=z[97]*z[15];
    z[97]=z[97] - z[49];
    z[97]=z[97]*z[35];
    z[98]=z[62]*z[19];
    z[99]=z[98]*z[41];
    z[100]=z[80] - z[6];
    z[101]=z[100]*z[15];
    z[99]= - z[101] + z[97] - z[99];
    z[101]=z[99]*z[78];
    z[93]=2*z[93] + z[101];
    z[93]=z[19]*z[93];
    z[101]=2*z[98];
    z[102]=z[101]*z[49];
    z[103]=z[62]*z[6];
    z[102]=z[103] + z[102];
    z[103]=z[102]*z[78];
    z[103]=z[103] + 36*z[49] + 23*z[53];
    z[103]=z[19]*z[103];
    z[104]=3*z[33];
    z[105]=static_cast<T>(23)+ z[104];
    z[105]=2*z[105] + z[74];
    z[105]=z[6]*z[105];
    z[106]=3*z[30];
    z[103]=z[103] + 30*z[15] + z[105] + z[77] + z[106];
    z[103]=z[16]*z[103];
    z[105]=6*z[6];
    z[107]=z[8] + z[6];
    z[107]=z[107]*z[105];
    z[50]=49*z[50] + z[107] + 55*z[53];
    z[50]=z[16]*z[50];
    z[107]=14*z[22] - z[26];
    z[107]=z[4]*z[107];
    z[107]=22*z[8] + z[107];
    z[108]=6*z[30];
    z[109]= - z[90] + z[108];
    z[109]=z[4]*z[109];
    z[109]= - static_cast<T>(17)+ z[109];
    z[109]=z[6]*z[109];
    z[110]=z[19]*z[53];
    z[50]=z[50] - 49*z[110] + 6*z[107] + z[109];
    z[50]=z[18]*z[50];
    z[77]=z[77] - z[55];
    z[77]=z[4]*z[77];
    z[77]= - static_cast<T>(32)+ z[77];
    z[107]= - static_cast<T>(50)- z[104];
    z[107]=z[107]*z[57];
    z[50]=z[50] + z[103] + z[93] + z[69] + 3*z[77] + z[107];
    z[69]=6*z[18];
    z[50]=z[50]*z[69];
    z[77]=25*z[33];
    z[93]=z[18]*z[30];
    z[93]=22*z[93] - 41*z[48] + static_cast<T>(41)+ z[77];
    z[103]=3*z[18];
    z[93]=z[93]*z[103];
    z[91]=z[91]*z[16];
    z[107]= - z[22]*z[91];
    z[109]=z[15] + 14*z[8] - z[55];
    z[109]=z[16]*z[109];
    z[107]=z[107] - static_cast<T>(4)+ z[109];
    z[107]=z[13]*z[107];
    z[109]=6*z[4];
    z[110]= - static_cast<T>(51)+ 10*z[33];
    z[110]=z[16]*z[110];
    z[93]=z[107] + z[93] - z[109] + z[110];
    z[107]=6*z[13];
    z[93]=z[93]*z[107];
    z[46]= - z[48] - z[46];
    z[46]=z[18]*z[46];
    z[46]= - z[4] + z[46];
    z[110]=66*z[18];
    z[46]=z[46]*z[110];
    z[47]=z[47]*z[91];
    z[63]=static_cast<T>(1)+ z[63];
    z[63]=z[16]*z[63];
    z[47]=2*z[63] + z[47];
    z[47]=z[13]*z[47];
    z[63]=z[94]*z[16];
    z[46]=z[47] - z[63] + z[46];
    z[46]=z[46]*z[107];
    z[47]= - static_cast<T>(23)- z[92];
    z[47]=z[47]*z[64];
    z[47]=z[47] + 37*z[52];
    z[47]=z[16]*z[47];
    z[47]= - 37*z[39] + z[47];
    z[63]= - z[63] - z[91];
    z[63]=z[63]*z[96];
    z[64]=z[16]*z[39];
    z[63]=259*z[64] + 6*z[63];
    z[63]=z[2]*z[63];
    z[46]=z[63] + 7*z[47] + z[46];
    z[46]=z[2]*z[46];
    z[47]=z[101] - z[32];
    z[47]=z[47]*z[19];
    z[63]=static_cast<T>(33)+ z[47];
    z[63]=z[19]*z[63];
    z[64]= - 15*z[8] + z[30];
    z[64]=z[4]*z[64];
    z[64]=7*z[48] - static_cast<T>(7)+ z[64];
    z[64]=z[18]*z[64];
    z[96]= - static_cast<T>(16)+ z[104];
    z[96]=z[4]*z[96];
    z[63]=2*z[64] + z[96] + z[63];
    z[63]=z[18]*z[63];
    z[64]=259*z[33];
    z[96]=static_cast<T>(304)+ z[64];
    z[96]=z[96]*z[56];
    z[101]=z[56]*z[6];
    z[104]= - static_cast<T>(2)- z[92];
    z[104]=z[104]*z[101];
    z[111]=259*z[30];
    z[112]=858*z[8] + z[111];
    z[112]=z[4]*z[112];
    z[112]=static_cast<T>(311)+ z[112];
    z[112]=z[4]*z[112];
    z[104]=z[112] + 21*z[104];
    z[104]=z[16]*z[104];
    z[112]=z[19]*z[4];
    z[46]=z[46] + z[93] + 18*z[63] + z[104] + 36*z[112] + z[96] - 259*
    z[52];
    z[46]=z[2]*z[46];
    z[52]=z[85] - z[101];
    z[52]=z[6]*z[52];
    z[63]=z[81]*z[16];
    z[81]=static_cast<T>(2)- z[57];
    z[81]=z[16]*z[81];
    z[81]= - z[63] + z[4] + z[81];
    z[81]=z[2]*z[81];
    z[93]= - z[15] + z[98];
    z[93]=z[19]*z[93];
    z[96]=z[15] + z[6];
    z[104]= - z[16]*z[96];
    z[52]=z[81] + z[104] + z[93] - static_cast<T>(3)+ z[52];
    z[52]=z[2]*z[52];
    z[81]= - z[41] - z[15];
    z[81]=z[15]*z[81];
    z[93]= - z[6]*z[98];
    z[81]=z[93] + z[81] + z[97];
    z[81]=z[19]*z[81];
    z[93]=3*z[49] + z[53];
    z[93]=z[15]*z[93];
    z[97]=z[49]*z[98];
    z[93]=z[93] + z[97];
    z[93]=z[19]*z[93];
    z[93]=3*z[53] + z[93];
    z[93]=z[16]*z[93];
    z[97]= - z[94] + z[101];
    z[97]=z[6]*z[97];
    z[97]=static_cast<T>(5)+ z[97];
    z[97]=z[6]*z[97];
    z[98]=z[109] - z[101];
    z[98]=z[6]*z[98];
    z[98]= - static_cast<T>(13)+ z[98];
    z[98]=z[98]*z[49];
    z[104]=11*z[6] - z[38];
    z[104]=z[15]*z[104];
    z[98]=z[98] + z[104];
    z[98]=z[7]*z[98];
    z[52]=z[52] + z[93] + z[81] + z[98] + z[97] + z[38];
    z[81]=2*z[5];
    z[52]=z[52]*z[81];
    z[93]=z[19]*z[99];
    z[72]= - z[72] + z[80];
    z[72]=z[15]*z[72];
    z[72]=z[49] + z[72];
    z[97]=2*z[7];
    z[72]=z[72]*z[97];
    z[72]=z[93] + z[72] + z[105] + z[32];
    z[72]=z[19]*z[72];
    z[47]=static_cast<T>(3)+ z[47];
    z[47]=z[19]*z[47];
    z[93]= - static_cast<T>(2)+ z[74];
    z[93]=z[16]*z[93];
    z[47]=z[63] + z[93] + z[47] + z[4] - z[59];
    z[47]=z[2]*z[47];
    z[49]= - 2*z[49] - z[53];
    z[63]=z[19]*z[102];
    z[49]=3*z[49] + z[63];
    z[49]=z[19]*z[49];
    z[49]=z[49] - z[86] + z[15];
    z[49]=z[16]*z[49];
    z[59]= - 10*z[4] + z[59];
    z[59]=z[6]*z[59];
    z[63]=13*z[4] - z[51];
    z[63]=z[6]*z[63];
    z[63]= - static_cast<T>(22)+ z[63];
    z[63]=z[6]*z[63];
    z[63]=z[63] + z[88];
    z[63]=z[7]*z[63];
    z[47]=z[52] + z[47] + z[49] + z[72] + z[63] - static_cast<T>(2)+ z[59];
    z[49]=18*z[5];
    z[47]=z[47]*z[49];
    z[52]=z[13]*z[62];
    z[52]= - z[80] + z[52];
    z[52]=z[11]*z[52];
    z[59]=z[73]*z[13];
    z[62]=z[5]*z[80];
    z[52]=z[62] - z[59] + z[52];
    z[62]=3*z[1];
    z[52]=z[52]*z[62];
    z[43]=z[43] + z[53];
    z[42]= - z[42] + 2*z[19];
    z[42]=z[43]*z[42];
    z[38]= - z[38]*z[59];
    z[43]= - z[95]*z[80];
    z[53]=z[13]*z[76];
    z[43]=z[43] + z[53];
    z[53]=3*z[11];
    z[43]=z[43]*z[53];
    z[59]=6*z[5];
    z[63]=z[15]*z[95]*z[59];
    z[38]=z[52] + z[63] + z[43] + z[38] + z[42];
    z[38]=z[9]*z[38];
    z[42]=static_cast<T>(21)- 19*z[33];
    z[42]=z[42]*z[79];
    z[43]= - z[75]*z[107];
    z[52]= - z[13]*z[80];
    z[52]=z[52] - static_cast<T>(44)- 21*z[35];
    z[52]=z[52]*z[53];
    z[63]= - static_cast<T>(63)+ z[77];
    z[63]=z[4]*z[63];
    z[63]=z[63] + 63*z[11];
    z[63]=z[1]*z[7]*z[63];
    z[42]=z[63] - 12*z[5] + z[52] + z[43] + 82*z[4] + z[42];
    z[42]=z[1]*z[42];
    z[43]=z[80] + z[8] - 4*z[6];
    z[43]=z[19]*z[43];
    z[52]=z[17]*z[100];
    z[43]=z[43] - z[52];
    z[52]= - z[90] + 21*z[15];
    z[63]=z[52]*z[35];
    z[63]=z[82] + z[63];
    z[63]=z[16]*z[63];
    z[52]= - z[7]*z[52];
    z[52]=z[63] - static_cast<T>(23)+ z[52];
    z[40]= - z[40] + z[15];
    z[40]=z[40]*z[107];
    z[63]=21*z[87] + 29*z[6] + z[82];
    z[53]=z[63]*z[53];
    z[63]=z[5]*z[6];
    z[38]=2*z[38] + z[42] - 24*z[63] + z[53] + z[40] + 3*z[52] + 4*z[43]
   ;
    z[38]=z[9]*z[38];
    z[40]=6*z[35] + 6;
    z[40]=z[24]*z[40];
    z[42]=13*z[22];
    z[43]= - z[42] - z[89];
    z[43]=z[15]*z[43];
    z[40]=z[43] + z[40];
    z[40]=z[7]*z[40];
    z[40]=z[40] - 22*z[22] - z[89];
    z[40]=z[16]*z[40];
    z[43]= - z[60]*z[110];
    z[23]= - z[23]*z[91];
    z[23]=z[23] + z[43] + z[40] + 12*z[45] - 61*z[8] + 4*z[15];
    z[23]=z[13]*z[23];
    z[25]=z[25] - 17*z[22] + 10*z[27];
    z[25]=z[25]*z[79];
    z[25]=z[25] + 25*z[15] + 106*z[8] - z[55];
    z[25]=z[16]*z[25];
    z[40]=15*z[45] - 97*z[8] - z[106];
    z[40]=z[7]*z[40];
    z[43]=z[61] - z[36];
    z[43]=z[7]*z[43];
    z[43]= - 24*z[15] + z[43];
    z[43]=z[43]*z[78];
    z[45]= - z[83] - 44*z[27];
    z[45]=z[7]*z[45];
    z[45]=z[45] - 44*z[8] - z[106];
    z[45]=z[45]*z[103];
    z[52]=static_cast<T>(23)+ z[84];
    z[23]=z[23] + z[45] + z[25] + z[43] + 3*z[52] + z[40];
    z[23]=z[23]*z[107];
    z[25]= - z[42] - 6*z[26];
    z[25]=z[4]*z[25];
    z[25]=z[29] + z[25];
    z[25]=z[6]*z[25];
    z[29]= - z[29] + z[105];
    z[29]=2*z[29] - z[32];
    z[29]=z[15]*z[29];
    z[25]=z[29] + z[25] - z[37];
    z[25]=z[18]*z[25];
    z[29]=18*z[22] + z[26];
    z[29]=z[29]*z[70];
    z[32]=34*z[68] - z[74];
    z[32]=z[6]*z[32];
    z[37]=z[15] + z[8];
    z[40]=z[37]*z[35];
    z[25]=z[25] - 7*z[40] + 26*z[15] + z[32] + 73*z[8] + z[29];
    z[25]=z[25]*z[69];
    z[29]=z[27]*z[7];
    z[31]=z[29] + 10*z[15] - z[90] - z[31];
    z[32]=18*z[7];
    z[31]=z[31]*z[32];
    z[40]=259*z[26];
    z[42]= - 458*z[22] + z[40];
    z[42]=z[4]*z[42];
    z[42]=z[90] + z[42];
    z[42]=z[4]*z[42];
    z[43]=6*z[8];
    z[45]=z[43] + 37*z[30];
    z[52]=7*z[4];
    z[45]=z[45]*z[52];
    z[45]=static_cast<T>(67)+ z[45];
    z[45]=z[45]*z[57];
    z[42]=z[45] - static_cast<T>(116)+ z[42];
    z[42]=z[6]*z[42];
    z[45]=npow(z[8],4);
    z[53]=z[45]*z[4];
    z[55]= - 259*z[53] + 181*z[24];
    z[55]=z[55]*z[4];
    z[60]=757*z[22] - z[55];
    z[60]=z[4]*z[60];
    z[42]=468*z[15] + z[42] + 813*z[8] + z[60];
    z[42]=z[19]*z[42];
    z[37]=z[37]*z[48];
    z[48]=6*z[22];
    z[60]= - z[48] - 55*z[27];
    z[60]=z[13]*z[60];
    z[37]=z[60] + 6*z[37] - 34*z[15] + 37*z[8] + z[106];
    z[37]=z[37]*z[107];
    z[60]= - 49*z[8] - 12*z[30];
    z[60]=z[60]*z[70];
    z[60]= - static_cast<T>(26)+ z[60];
    z[60]=z[60]*z[57];
    z[61]=487*z[8] + 72*z[30];
    z[61]=z[4]*z[61];
    z[25]=z[37] + z[25] + z[42] + z[31] + z[60] + static_cast<T>(830)+ z[61];
    z[25]=z[11]*z[25];
    z[31]=z[41]*z[56];
    z[37]=z[94] - z[31];
    z[37]=z[6]*z[37];
    z[41]= - z[52] + z[31];
    z[41]=z[6]*z[41];
    z[41]=static_cast<T>(6)+ z[41];
    z[41]=z[6]*z[41];
    z[41]=z[41] - z[15];
    z[41]=z[7]*z[41];
    z[42]=z[101] - z[4];
    z[60]=z[2]*z[42];
    z[37]=2*z[60] + z[41] + static_cast<T>(2)+ z[37];
    z[37]=z[37]*z[81];
    z[41]= - z[70] + z[51];
    z[41]=z[6]*z[41];
    z[41]=static_cast<T>(1)+ z[41];
    z[41]=z[41]*z[97];
    z[37]=z[37] + z[41] + z[4] + z[66];
    z[37]=z[37]*z[49];
    z[41]= - z[7]*z[6]*z[42];
    z[42]= - z[2]*z[56];
    z[41]=z[42] + z[101] + z[41];
    z[41]=z[41]*z[81];
    z[39]= - z[39]*z[67];
    z[39]=z[41] - z[65] + z[39] + z[58];
    z[39]=z[39]*z[59];
    z[41]=z[11]*z[4];
    z[41]=25*z[56] - 12*z[41];
    z[41]=z[7]*z[41];
    z[39]=z[39] + z[41];
    z[39]=z[39]*z[62];
    z[41]= - static_cast<T>(61)- 12*z[33];
    z[41]=z[4]*z[41];
    z[41]= - z[79] + z[41] - 31*z[101];
    z[41]=z[41]*z[79];
    z[42]=z[7]*z[33];
    z[42]= - z[85] + z[42];
    z[42]=3*z[42] - 55*z[13];
    z[42]=z[13]*z[42];
    z[41]=3*z[42] - 250*z[56] + z[41];
    z[35]=2*z[35] - static_cast<T>(21)- 4*z[33];
    z[35]=z[7]*z[35];
    z[42]=z[43] + 55*z[15];
    z[42]=z[13]*z[42];
    z[42]= - static_cast<T>(39)+ z[42];
    z[42]=z[42]*z[107];
    z[35]=z[42] + 445*z[4] + 9*z[35];
    z[35]=z[11]*z[35];
    z[35]=z[39] + z[37] + z[35] + 2*z[41] + 259*z[65];
    z[35]=z[1]*z[35];
    z[36]= - z[36] + 2*z[26] + 19*z[22];
    z[37]= - 63*z[15] + 64*z[8];
    z[37]=z[37]*z[15];
    z[36]= - z[37] + 6*z[36];
    z[36]=z[36]*z[79];
    z[37]=z[4]*npow(z[8],5);
    z[37]=259*z[37] + 78*z[45];
    z[37]=z[37]*z[4];
    z[37]=z[37] + 558*z[24];
    z[37]=z[37]*z[4];
    z[27]=z[37] + 455*z[27] + 910*z[22];
    z[27]=z[27]*z[19];
    z[26]=z[48] + 37*z[26];
    z[26]=z[26]*z[4];
    z[26]=z[26] - z[44];
    z[26]=z[26]*z[4];
    z[26]=z[26] + 27;
    z[26]=z[26]*z[71];
    z[24]= - 37*z[53] + 31*z[24];
    z[24]=z[24]*z[52];
    z[24]=z[24] + 225*z[22];
    z[24]=z[24]*z[4];
    z[37]=z[76]*z[7];
    z[39]=z[96]*z[15];
    z[37]=z[37] + z[39];
    z[37]= - z[9]*z[37];
    z[24]=189*z[37] + z[27] - z[26] - z[36] + z[24] - 189*z[15] + 357*
    z[8];
    z[26]=z[11] - z[16];
    z[24]=z[21]*z[26]*z[24];
    z[26]=440*z[22] - z[40];
    z[26]=z[4]*z[26];
    z[26]= - 611*z[8] + z[26];
    z[26]=z[4]*z[26];
    z[27]=z[44] - z[111];
    z[27]=z[4]*z[27];
    z[27]= - static_cast<T>(308)+ z[27];
    z[27]=z[27]*z[57];
    z[26]=z[27] + static_cast<T>(138)+ z[26];
    z[26]=z[6]*z[26];
    z[22]= - 739*z[22] + z[55];
    z[22]=z[4]*z[22];
    z[22]=z[26] - 449*z[8] + z[22];
    z[22]=z[19]*z[22];
    z[26]=87*z[8] + 518*z[30];
    z[26]=z[4]*z[26];
    z[26]= - static_cast<T>(347)+ z[26];
    z[26]=z[26]*z[57];
    z[27]=z[30] - z[29];
    z[27]=z[27]*z[32];
    z[29]= - 268*z[8] - 143*z[30];
    z[29]=z[4]*z[29];
    z[29]= - static_cast<T>(98)+ z[29];
    z[22]=z[22] + z[27] + 4*z[29] + z[26];
    z[22]=z[16]*z[22];
    z[26]=z[54] + z[108];
    z[26]=z[26]*z[85];
    z[27]=static_cast<T>(71)+ z[92];
    z[27]=z[4]*z[27];
    z[27]=z[27] + 25*z[101];
    z[27]=z[6]*z[27];
    z[26]=z[27] - static_cast<T>(127)+ z[26];
    z[26]=z[26]*z[79];
    z[27]=static_cast<T>(30)+ z[64];
    z[27]=z[27]*z[31];
    z[29]= - static_cast<T>(29)- 500*z[33];
    z[29]=z[4]*z[29];

    r += z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + 18*z[28] + 
      z[29] + 6*z[34] + z[35] + 3*z[38] + z[46] + z[47] + z[50];
 
    return r;
}

template double qqb_2lLC_r91(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r91(const std::array<dd_real,31>&);
#endif
