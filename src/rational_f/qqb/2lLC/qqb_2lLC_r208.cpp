#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r208(const std::array<T,31>& k) {
  T z[94];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[5];
    z[9]=k[15];
    z[10]=k[8];
    z[11]=k[21];
    z[12]=k[9];
    z[13]=k[26];
    z[14]=k[2];
    z[15]=k[3];
    z[16]=k[4];
    z[17]=k[27];
    z[18]=6*z[1];
    z[19]=npow(z[1],2);
    z[20]=z[19]*z[3];
    z[21]=5*z[20];
    z[22]=z[3]*z[1];
    z[23]=z[22]*z[16];
    z[24]= - 2*z[23] + z[18] + z[21];
    z[24]=z[16]*z[24];
    z[25]=3*z[1];
    z[26]=z[25]*z[16];
    z[27]=3*z[19];
    z[28]=z[26] - z[27];
    z[28]=z[3]*z[28];
    z[28]=14*z[1] + z[28];
    z[28]=z[16]*z[28];
    z[29]=z[8]*z[1];
    z[30]=19*z[29];
    z[28]=z[28] - 14*z[19] + z[30];
    z[28]=z[16]*z[28];
    z[31]=8*z[29];
    z[32]= - 19*z[19] + z[31];
    z[32]=z[8]*z[32];
    z[28]=z[32] + z[28];
    z[28]=z[9]*z[28];
    z[32]=7*z[19];
    z[24]=z[28] + z[24] + z[32] + z[31];
    z[24]=z[9]*z[24];
    z[28]=2*z[10];
    z[31]=z[28]*z[19];
    z[33]=npow(z[8],2);
    z[34]=z[33]*z[31];
    z[35]=z[19]*z[8];
    z[34]= - z[35] + z[34];
    z[34]=z[10]*z[34];
    z[36]=z[19]*z[5];
    z[37]=2*z[8];
    z[38]= - z[37]*z[36];
    z[34]=z[34] + z[38];
    z[38]=z[19]*z[7];
    z[39]=3*z[38];
    z[40]=z[39]*z[33];
    z[40]=z[40] - z[35];
    z[40]=z[40]*z[7];
    z[41]=4*z[19];
    z[42]= - z[9]*z[33]*z[41];
    z[42]=z[35] + z[42];
    z[43]=2*z[9];
    z[42]=z[42]*z[43];
    z[34]=z[42] + 2*z[34] - z[40];
    z[34]=z[12]*z[34];
    z[42]=z[29] - z[19];
    z[44]=z[8]*z[42];
    z[45]=z[29] + z[19];
    z[46]=z[45]*z[28];
    z[47]=z[33]*z[46];
    z[44]=z[44] + z[47];
    z[44]=z[10]*z[44];
    z[47]=z[10]*z[45];
    z[47]= - z[38] + z[47];
    z[47]=z[33]*z[47];
    z[48]=z[33]*z[1];
    z[47]= - z[48] + z[47];
    z[49]=6*z[13];
    z[47]=z[47]*z[49];
    z[40]=z[47] + z[44] - z[40];
    z[40]=z[13]*z[40];
    z[44]=5*z[19];
    z[47]=3*z[29];
    z[50]=z[44] + z[47];
    z[51]=npow(z[1],3);
    z[52]=2*z[51];
    z[53]=z[52] + z[35];
    z[54]=3*z[4];
    z[55]=z[53]*z[54];
    z[55]= - 2*z[50] + z[55];
    z[55]=z[4]*z[55];
    z[56]=2*z[19];
    z[57]=z[56] - z[29];
    z[58]=z[10]*z[8]*z[57];
    z[59]=2*z[29];
    z[58]=z[58] - z[19] + z[59];
    z[58]=z[58]*z[28];
    z[60]=3*z[51];
    z[61]= - z[60] - z[35];
    z[62]=3*z[8];
    z[61]=z[61]*z[62];
    z[63]=npow(z[1],4);
    z[64]=13*z[63];
    z[61]= - z[64] + z[61];
    z[61]=z[4]*z[61];
    z[61]=9*z[53] + z[61];
    z[61]=z[4]*z[61];
    z[61]= - 17*z[19] + z[61];
    z[61]=z[5]*z[61];
    z[65]=z[63]*z[4];
    z[66]= - 13*z[51] + 7*z[65];
    z[66]=z[4]*z[66];
    z[67]=npow(z[1],5);
    z[68]=z[4]*z[67];
    z[64]=z[64] - 7*z[68];
    z[64]=z[4]*z[64];
    z[68]=6*z[51];
    z[64]= - z[68] + z[64];
    z[64]=z[5]*z[64];
    z[69]=6*z[19];
    z[64]=z[64] + z[69] + z[66];
    z[64]=z[3]*z[64];
    z[66]=z[28]*z[29];
    z[66]=z[66] - z[25];
    z[66]=z[66]*z[28];
    z[70]=z[5]*z[20];
    z[70]= - z[66] - 7*z[70];
    z[70]=z[16]*z[70];
    z[71]=8*z[1];
    z[24]=z[40] + z[34] + z[24] + z[70] + z[64] + z[61] + z[58] + z[71]
    + z[55];
    z[24]=z[2]*z[24];
    z[34]=z[27] + z[29];
    z[40]=z[34]*z[8];
    z[55]=z[52] + z[40];
    z[55]=z[4]*z[55];
    z[58]=4*z[29];
    z[55]=z[55] - z[32] - z[58];
    z[55]=z[8]*z[55];
    z[55]= - z[52] + z[55];
    z[55]=z[55]*z[54];
    z[61]=z[56] + z[29];
    z[64]=z[61]*z[8];
    z[64]=z[64] + z[51];
    z[70]=z[8]*z[64];
    z[40]=z[40] + z[60];
    z[40]=z[40]*z[8];
    z[40]=z[40] + z[63];
    z[40]=z[40]*z[4];
    z[72]= - z[62]*z[40];
    z[70]=8*z[70] + z[72];
    z[70]=z[4]*z[70];
    z[72]=z[45]*z[8];
    z[70]= - 5*z[72] + z[70];
    z[70]=z[5]*z[70];
    z[73]=z[47] + z[56];
    z[74]= - z[4]*z[72];
    z[74]=z[74] + z[73];
    z[74]=z[74]*z[54];
    z[26]= - z[3]*z[26];
    z[26]=z[26] + z[20] - 11*z[1] + z[74];
    z[26]=z[16]*z[26];
    z[74]=5*z[29];
    z[26]=z[26] + z[70] + z[55] + 11*z[19] + z[74];
    z[26]=z[26]*z[43];
    z[55]=z[28]*z[51];
    z[70]=z[55] + z[56];
    z[75]=z[63]*z[5];
    z[76]= - z[54]*z[75];
    z[77]= - z[51] + z[75];
    z[77]=z[7]*z[77];
    z[78]=z[51]*z[4];
    z[79]=9*z[78];
    z[76]=6*z[77] + z[76] + z[79] - z[70];
    z[76]=z[3]*z[76];
    z[77]=z[41] + z[29];
    z[80]=z[8]*z[77];
    z[80]=z[52] + z[80];
    z[80]=z[80]*z[54];
    z[80]=z[80] - z[50];
    z[80]=z[4]*z[80];
    z[30]=40*z[19] + z[30];
    z[30]=z[8]*z[30];
    z[30]= - 9*z[40] + 21*z[51] + z[30];
    z[30]=z[4]*z[30];
    z[40]= - z[32] - z[47];
    z[30]=2*z[40] + z[30];
    z[30]=z[5]*z[30];
    z[40]= - z[8]*z[55];
    z[40]= - z[51] + z[40];
    z[40]=z[40]*z[28];
    z[81]= - z[63]*z[54];
    z[81]=z[52] + z[81];
    z[81]=z[5]*z[81];
    z[40]=z[81] + z[79] + z[40];
    z[40]=z[7]*z[40];
    z[79]=z[71] + z[39];
    z[81]= - z[19]*z[54];
    z[81]=z[81] + z[79];
    z[81]=z[3]*z[81];
    z[82]=z[19]*npow(z[4],2);
    z[83]= - z[4]*z[38];
    z[82]=z[82] + z[83];
    z[81]=3*z[82] + z[81];
    z[81]=z[16]*z[81];
    z[82]=z[51]*npow(z[10],2);
    z[26]=z[26] + z[81] + z[76] + z[40] + z[30] - 4*z[82] - 16*z[1] + 
    z[80];
    z[26]=z[9]*z[26];
    z[30]= - z[10]*z[72];
    z[40]=3*z[5];
    z[76]= - z[64]*z[40];
    z[80]=z[40] + z[10];
    z[80]=z[45]*z[80];
    z[80]=z[20] - z[1] + z[80];
    z[80]=z[16]*z[80];
    z[81]=z[8]*z[38];
    z[30]=z[80] + z[81] + z[76] + z[30] + z[19] + z[47];
    z[30]=z[30]*z[49];
    z[76]=2*z[64];
    z[76]= - z[5]*z[76];
    z[76]=z[76] - z[45];
    z[76]=z[5]*z[76];
    z[80]=4*z[1];
    z[76]= - z[80] + z[76];
    z[76]=z[76]*z[40];
    z[81]=5*z[1];
    z[46]= - z[81] + z[46];
    z[46]=z[10]*z[46];
    z[82]=z[19]*z[10];
    z[83]=4*z[82];
    z[84]=z[5]*z[27];
    z[84]= - z[83] + z[84];
    z[84]=z[3]*z[84];
    z[46]=z[84] + z[46] + z[76];
    z[46]=z[16]*z[46];
    z[76]=z[51]*z[5];
    z[84]=z[41] - z[76];
    z[85]=z[51]*z[7];
    z[84]=2*z[84] - z[85];
    z[84]=z[3]*z[84];
    z[86]=z[7]*z[63];
    z[86]= - z[75] + z[86];
    z[86]=z[3]*z[86];
    z[87]=z[75]*z[7];
    z[86]= - z[87] + z[86];
    z[86]=z[9]*z[86];
    z[84]=z[84] + z[86];
    z[86]= - z[28]*z[72];
    z[50]=z[86] + z[50];
    z[50]=z[10]*z[50];
    z[86]=z[32] + z[29];
    z[86]=z[86]*z[8];
    z[86]=z[86] + z[68];
    z[86]=z[86]*z[5];
    z[74]=z[56] + z[74];
    z[74]=2*z[74] + z[86];
    z[74]=z[74]*z[40];
    z[73]=z[5]*z[73];
    z[73]=z[38] + z[73];
    z[73]=z[62]*z[73];
    z[73]= - z[19] - 9*z[29] + z[73];
    z[73]=z[7]*z[73];
    z[30]=z[30] + z[46] + z[73] + z[74] - 30*z[1] + z[50] + 3*z[84];
    z[30]=z[13]*z[30];
    z[42]=z[42]*z[62];
    z[46]= - z[63]*z[28];
    z[42]=z[46] + z[52] + z[42];
    z[42]=z[5]*z[42];
    z[46]=z[9]*z[85];
    z[46]=z[46] - z[38];
    z[50]=4*z[8];
    z[46]=z[50]*z[46];
    z[42]=z[42] - z[47] + z[46] + z[70];
    z[46]=2*z[12];
    z[42]=z[42]*z[46];
    z[52]=z[35] + z[51];
    z[70]=z[52]*z[54];
    z[53]= - z[8]*z[53];
    z[53]= - z[63] + z[53];
    z[53]=z[4]*z[53];
    z[53]=z[51] + z[53];
    z[53]=z[53]*z[28];
    z[53]=z[53] - z[41] + z[70];
    z[28]=z[53]*z[28];
    z[53]= - z[27] + z[29];
    z[73]=z[10]*z[68];
    z[53]=z[73] + 4*z[53] + z[70];
    z[53]=z[5]*z[53];
    z[73]=z[5]*z[45];
    z[73]=z[38] + z[73];
    z[73]=z[62]*z[73];
    z[69]= - z[69] - 11*z[29] + z[73];
    z[69]=z[7]*z[69];
    z[50]=z[9]*z[4]*z[52]*z[50];
    z[50]=z[50] + z[70] + z[85];
    z[50]=z[50]*z[43];
    z[52]=z[78] - z[19];
    z[70]=z[52]*z[3];
    z[73]=z[19]*z[4];
    z[28]=z[42] + z[50] + 2*z[70] + z[69] + z[53] + z[28] - 12*z[1] + 
    z[73];
    z[28]=z[12]*z[28];
    z[42]=z[19] - z[76];
    z[42]=z[7]*z[42];
    z[50]=n<T>(5,3)*z[36];
    z[42]=n<T>(4,3)*z[42] + z[25] + z[50];
    z[42]=z[7]*z[42];
    z[52]= - z[4]*z[52];
    z[53]= - z[10]*z[78];
    z[52]=z[52] + z[53];
    z[52]=z[10]*z[52];
    z[53]=29*z[19] - 19*z[78];
    z[69]=n<T>(1,3)*z[4];
    z[53]=z[5]*z[53]*z[69];
    z[42]=z[42] + z[52] + z[53];
    z[42]=z[3]*z[42];
    z[52]=z[64]*z[4];
    z[41]=z[52] - z[41] - z[47];
    z[41]=z[4]*z[41];
    z[53]=z[10]*z[4];
    z[74]= - z[64]*z[53];
    z[41]=z[41] + z[74];
    z[41]=z[10]*z[41];
    z[74]=z[59] + z[19];
    z[84]=z[74]*z[4];
    z[88]=z[80] + z[84];
    z[88]=z[4]*z[88];
    z[41]=z[88] + z[41];
    z[41]=z[10]*z[41];
    z[88]=2*z[52];
    z[45]=z[88] + z[45];
    z[45]=z[4]*z[45];
    z[88]=z[5]*z[88];
    z[45]=z[45] + z[88];
    z[45]=z[45]*z[40];
    z[88]=n<T>(13,3)*z[19];
    z[89]= - z[88] + z[59];
    z[89]=z[4]*z[89];
    z[89]=z[80] + z[89];
    z[89]=z[4]*z[89];
    z[45]=z[89] + z[45];
    z[45]=z[5]*z[45];
    z[89]= - n<T>(16,3)*z[19] - z[47];
    z[89]=z[4]*z[89];
    z[89]=z[25] + z[89];
    z[89]=z[4]*z[89];
    z[66]=z[89] + z[66];
    z[66]=z[7]*z[66];
    z[41]=z[42] + z[66] + z[41] + z[45];
    z[41]=z[16]*z[41];
    z[42]=npow(z[7],2);
    z[45]=z[42]*z[25];
    z[66]=z[3]*z[7];
    z[79]=z[79]*z[66];
    z[45]=z[45] + z[79];
    z[45]=z[45]*z[46];
    z[79]=z[42]*z[22];
    z[89]=13*z[79];
    z[45]= - z[89] + z[45];
    z[45]=z[12]*z[45];
    z[90]=2*z[1];
    z[91]= - z[90] - z[38];
    z[91]=z[91]*z[66];
    z[92]=z[42]*z[1];
    z[91]= - z[92] + z[91];
    z[91]=z[91]*z[49];
    z[89]= - z[89] + z[91];
    z[89]=z[13]*z[89];
    z[91]=npow(z[12],2);
    z[93]=npow(z[13],2);
    z[93]= - z[91] + z[93];
    z[79]=z[15]*z[79]*z[93];
    z[45]=6*z[79] + z[45] + z[89];
    z[45]=z[15]*z[45];
    z[42]=z[42]*z[71];
    z[71]=13*z[1];
    z[79]=z[7]*z[56];
    z[79]=z[71] + z[79];
    z[79]=z[79]*z[66];
    z[89]= - z[7]*z[1];
    z[39]=z[39] + z[1];
    z[93]=z[3]*z[39];
    z[89]=z[89] + z[93];
    z[89]=z[13]*z[89];
    z[42]=3*z[89] + z[42] + z[79];
    z[42]=z[13]*z[42];
    z[79]=25*z[1] + 4*z[38];
    z[79]=z[79]*z[66];
    z[89]= - z[81] - 8*z[38];
    z[89]=z[3]*z[89];
    z[93]=z[7]*z[90];
    z[89]=z[93] + z[89];
    z[89]=z[89]*z[46];
    z[79]=z[89] + 16*z[92] + z[79];
    z[79]=z[12]*z[79];
    z[42]=z[45] + z[79] + 2*z[42];
    z[42]=z[15]*z[42];
    z[45]=7*z[1];
    z[79]= - z[7]*z[58];
    z[21]=z[21] - z[45] + z[79];
    z[21]=z[21]*z[46];
    z[79]=z[38] + z[25];
    z[89]=z[3]*z[79];
    z[92]= - z[1] + z[38];
    z[92]=z[7]*z[92];
    z[21]=z[21] + 3*z[92] - 4*z[89];
    z[21]=z[12]*z[21];
    z[71]= - z[71] - 15*z[38];
    z[71]=z[3]*z[71];
    z[89]= - z[3]*z[27];
    z[79]=z[89] + z[79];
    z[49]=z[79]*z[49];
    z[39]=z[7]*z[39];
    z[39]=z[49] + z[39] + z[71];
    z[39]=z[13]*z[39];
    z[49]= - z[25] + n<T>(4,3)*z[38];
    z[49]=z[49]*z[66];
    z[66]=z[70] + z[1] - z[73];
    z[66]=z[2]*z[4]*z[66];
    z[21]=z[42] + 3*z[66] + z[39] + z[49] + z[21];
    z[21]=z[15]*z[21];
    z[39]= - z[76] - z[85];
    z[39]=z[3]*z[39];
    z[42]=4*z[78];
    z[49]=z[7]*z[42];
    z[39]=z[39] - z[36] + z[49];
    z[39]=z[16]*z[39];
    z[49]= - z[5]*z[67];
    z[49]=z[63] + z[49];
    z[49]=z[7]*z[49];
    z[49]=z[75] + z[49];
    z[49]=z[3]*z[49];
    z[66]= - z[5]*z[35];
    z[39]=z[39] + z[49] - z[87] + z[66] + z[19] - z[42];
    z[39]=z[6]*z[39];
    z[31]=n<T>(1,3)*z[38] + z[31] + n<T>(7,3)*z[36];
    z[31]=z[3]*z[31];
    z[36]=z[44] + z[78];
    z[36]=z[7]*z[36]*z[69];
    z[31]=z[36] + z[31];
    z[31]=z[16]*z[31];
    z[36]=z[27] - n<T>(1,3)*z[78];
    z[36]=z[4]*z[36];
    z[42]=z[54]*z[85];
    z[49]= - z[60] + n<T>(7,3)*z[75];
    z[49]=z[7]*z[49];
    z[66]= - z[5]*z[60];
    z[49]=z[66] + z[49];
    z[49]=z[3]*z[49];
    z[31]=n<T>(2,3)*z[39] + z[31] + z[49] + z[42] + z[36] + z[50];
    z[31]=z[6]*z[31];
    z[36]= - z[27] + n<T>(16,3)*z[78];
    z[36]=z[4]*z[36];
    z[39]= - z[60] + z[65];
    z[39]=z[4]*z[39];
    z[42]=z[10]*z[65];
    z[39]=z[39] + z[42];
    z[39]=z[10]*z[39];
    z[42]= - 20*z[51] - z[65];
    z[42]=z[4]*z[42];
    z[42]=n<T>(80,3)*z[19] + z[42];
    z[42]=z[5]*z[42];
    z[49]=z[5]*z[68];
    z[49]= - z[88] + z[49];
    z[49]=z[7]*z[49];
    z[18]=z[49] + z[42] + z[39] - z[18] + z[36];
    z[18]=z[3]*z[18];
    z[36]=z[19]*z[37];
    z[39]=z[72] - z[51];
    z[39]=z[39]*z[8];
    z[39]=z[39] - z[63];
    z[42]=z[10]*z[39];
    z[36]=z[42] + z[51] + z[36];
    z[36]=z[10]*z[36];
    z[42]= - z[8]*z[74];
    z[42]=z[51] + z[42];
    z[42]=z[8]*z[42];
    z[49]=z[64]*z[33];
    z[50]=z[10]*z[49];
    z[42]=z[42] + z[50];
    z[42]=z[10]*z[42];
    z[42]=z[42] + z[51] + z[48];
    z[42]=z[7]*z[42];
    z[36]=z[42] + z[36] + z[61];
    z[36]=z[10]*z[36];
    z[42]= - 5*z[23] - z[81] + z[20];
    z[42]=z[16]*z[42];
    z[23]= - z[23] - z[90] + z[20];
    z[23]=z[16]*z[23];
    z[23]=z[23] + z[57];
    z[23]=z[16]*z[23];
    z[23]=z[35] + z[23];
    z[23]=z[9]*z[23];
    z[23]=z[23] + z[42] + z[56] - z[85];
    z[23]=z[9]*z[23];
    z[35]=z[10]*z[64];
    z[35]=z[35] + z[61];
    z[35]=z[10]*z[35];
    z[35]=z[1] + z[35];
    z[35]=z[10]*z[35];
    z[42]=z[51]*z[10];
    z[42]=z[42] - z[19];
    z[50]=z[10]*z[42];
    z[50]= - z[80] + z[50];
    z[50]=z[3]*z[50];
    z[35]=z[35] + z[50];
    z[35]=z[16]*z[35];
    z[50]= - z[10]*z[63];
    z[50]=5*z[51] + z[50];
    z[50]=z[10]*z[50];
    z[50]= - 8*z[19] + z[50];
    z[50]=z[3]*z[50];
    z[22]=z[22]*z[15];
    z[23]=4*z[22] + z[23] + z[35] + z[50] + z[36];
    z[23]=z[11]*z[23];
    z[35]= - z[55] - z[47] + z[56];
    z[35]=z[5]*z[35];
    z[20]= - z[20] + z[25] + z[35];
    z[20]=z[20]*z[46];
    z[25]=z[10]*z[27];
    z[25]= - z[81] + z[25];
    z[25]=z[5]*z[25];
    z[35]=z[45] - z[82];
    z[35]=z[3]*z[35];
    z[20]=z[20] + z[25] + z[35];
    z[20]=z[12]*z[20];
    z[25]= - npow(z[9],2);
    z[25]=z[25] + z[91];
    z[35]=z[3] - z[5];
    z[36]=2*z[14];
    z[45]=z[82] - z[1];
    z[25]=z[36]*z[25]*z[35]*z[45];
    z[35]=z[40] - z[3];
    z[40]=z[82] + z[1];
    z[35]=z[40]*z[35];
    z[42]=z[3]*z[42];
    z[45]=z[5]*z[29];
    z[42]=z[42] - z[1] + z[45];
    z[42]=z[42]*z[43];
    z[35]=z[42] + z[35];
    z[35]=z[9]*z[35];
    z[22]=z[91]*z[22];
    z[20]=z[25] + 2*z[22] + z[35] + z[20];
    z[20]=z[20]*z[36];
    z[22]= - z[27] - z[59];
    z[22]=z[8]*z[22];
    z[22]= - z[51] + z[22];
    z[22]=z[8]*z[22];
    z[25]=z[33]*z[52];
    z[22]=z[22] + z[25];
    z[22]=z[4]*z[22];
    z[25]=z[61]*z[37];
    z[27]= - z[53]*z[49];
    z[22]=z[27] + z[25] + z[22];
    z[22]=z[10]*z[22];
    z[25]=z[44] + z[58];
    z[25]=z[8]*z[25];
    z[25]=z[51] + z[25];
    z[25]=z[4]*z[25];
    z[22]=z[25] + z[22];
    z[22]=z[10]*z[22];
    z[25]= - n<T>(2,3)*z[19] - z[29];
    z[27]=z[33]*z[81];
    z[27]=z[60] + z[27];
    z[27]=z[4]*z[27];
    z[25]=14*z[25] + z[27];
    z[25]=z[4]*z[25];
    z[27]= - z[62]*z[84];
    z[27]=n<T>(8,3)*z[19] + z[27];
    z[27]=z[5]*z[27];
    z[22]=z[27] + z[22] + z[81] + z[25];
    z[22]=z[7]*z[22];
    z[25]=z[77]*z[37];
    z[27]=z[4]*z[39];
    z[33]=7*z[51];
    z[25]=z[27] + z[33] + z[25];
    z[25]=z[4]*z[25];
    z[27]= - z[39]*z[53];
    z[25]=z[25] + z[27];
    z[25]=z[10]*z[25];
    z[27]=z[4]*z[48];
    z[27]= - z[32] + z[27];
    z[27]=z[4]*z[27];
    z[25]=z[27] + z[25];
    z[25]=z[10]*z[25];
    z[27]= - z[33] + 6*z[48];
    z[27]=z[4]*z[27];
    z[19]=z[27] - n<T>(43,3)*z[19] - 13*z[29];
    z[19]=z[4]*z[19];
    z[27]= - z[54]*z[86];
    z[19]=z[27] + z[19] - z[83];
    z[19]=z[5]*z[19];
    z[27]= - z[16]*z[85];
    z[27]=z[51] + z[27];
    z[27]=z[6]*z[27];
    z[29]= - z[16]*z[38];
    z[27]=z[27] - z[56] + z[29];
    z[27]=z[6]*z[27];
    z[27]=z[27] - z[38] + z[40];
    z[27]=z[17]*z[27];
    z[29]=z[4]*z[34];
    z[29]= - z[1] + z[29];
    z[29]=z[29]*z[54];

    r += z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25]
       + z[26] + 4*z[27] + z[28] + z[29] + z[30] + 2*z[31] + z[41];
 
    return r;
}

template double qqb_2lLC_r208(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lLC_r208(const std::array<dd_real,31>&);
#endif
