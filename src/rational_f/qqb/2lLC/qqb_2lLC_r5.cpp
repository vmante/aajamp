#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r5(const std::array<T,31>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[2];
    z[8]=k[11];
    z[9]=k[4];
    z[10]=z[3]*z[2];
    z[11]=z[10] - 1;
    z[12]=z[2] + z[1];
    z[11]=z[5]*z[12]*z[11];
    z[12]=z[6]*z[2];
    z[12]=z[12] + 1;
    z[13]=z[9]*z[12];
    z[13]= - z[7] + z[13];
    z[13]=z[8]*z[13];
    z[10]=z[11] + z[10] + static_cast<T>(1)+ z[13];
    z[10]=z[4]*z[10];
    z[11]=z[8]*z[6];
    z[13]= - z[9] + z[2];
    z[13]=z[13]*z[11];
    z[14]=z[1] - z[2];
    z[14]=z[3]*z[14];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[5]*z[14];
    z[10]=z[10] + z[14] - z[6] + z[13];
    z[10]=z[4]*z[10];
    z[13]=z[8]*z[2];
    z[14]= - z[6]*z[1];
    z[13]=z[13] - static_cast<T>(1)+ z[14];
    z[14]=2*z[3];
    z[13]=z[13]*z[14];
    z[12]= - z[8]*z[12];
    z[12]=z[12] + z[13];
    z[12]=z[5]*z[12];
    z[13]=z[6]*z[9];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[13]*z[3]*z[8];
    z[10]=2*z[10] + z[13] + z[12];
    z[10]=z[4]*z[10];
    z[12]= - z[2]*z[11];
    z[12]=z[6] + z[12];
    z[12]=z[12]*z[14];
    z[12]=z[11] + z[12];
    z[12]=z[5]*z[12];
    z[11]= - z[3]*z[11];

    r += z[10] + z[11] + z[12];
 
    return r;
}

template double qqb_2lLC_r5(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r5(const std::array<dd_real,31>&);
#endif
