#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r36(const std::array<T,31>& k) {
  T z[64];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[5];
    z[7]=k[7];
    z[8]=k[8];
    z[9]=k[2];
    z[10]=k[11];
    z[11]=k[21];
    z[12]=k[15];
    z[13]=k[14];
    z[14]=k[3];
    z[15]=z[6]*z[5];
    z[16]=z[15] + 1;
    z[17]=2*z[6];
    z[18]= - z[16]*z[17];
    z[19]=npow(z[9],2);
    z[20]=2*z[19];
    z[21]=z[20]*z[3];
    z[22]=5*z[9];
    z[23]= - z[11]*z[20];
    z[18]=z[23] + z[21] + z[22] + z[18];
    z[18]=z[11]*z[18];
    z[23]=z[10]*z[16];
    z[24]=2*z[7];
    z[25]=z[24] + z[23];
    z[25]=z[25]*z[17];
    z[26]=z[19]*z[10];
    z[27]=z[11]*z[19];
    z[27]= - z[26] + z[27];
    z[27]=z[8]*z[27];
    z[28]=z[9]*z[10];
    z[29]= - z[10]*z[21];
    z[18]=6*z[27] + z[18] + z[29] + z[25] + static_cast<T>(4)+ 7*z[28];
    z[18]=z[8]*z[18];
    z[16]=z[16]*z[6];
    z[21]=z[16] - z[21];
    z[21]=z[11]*z[21];
    z[25]=z[3]*z[9];
    z[21]=z[21] + 8*z[25] - static_cast<T>(5)- 4*z[15];
    z[21]=z[11]*z[21];
    z[27]=z[12]*z[5];
    z[29]=12*z[27];
    z[30]=z[10]*z[5];
    z[31]= - z[30] + z[29];
    z[31]=z[31]*z[17];
    z[32]=4*z[12];
    z[33]=z[32]*z[19];
    z[34]= - z[22] + z[33];
    z[35]=3*z[12];
    z[34]=z[34]*z[35];
    z[34]= - 2*z[28] + z[34];
    z[34]=z[3]*z[34];
    z[36]=z[7] - z[5];
    z[37]=5*z[10];
    z[18]=z[18] + z[21] + z[34] + z[31] + 24*z[12] + z[37] - 6*z[36];
    z[21]=3*z[8];
    z[18]=z[18]*z[21];
    z[16]=z[16] + z[9];
    z[16]=z[16]*z[11];
    z[31]=z[15] + 2;
    z[34]=z[16] - 3*z[31] - z[25];
    z[34]=z[11]*z[34];
    z[23]=z[6]*z[23];
    z[16]= - z[16] + z[28] + z[23];
    z[16]=z[16]*z[21];
    z[23]= - z[5]*z[37];
    z[23]=z[23] + z[29];
    z[23]=z[6]*z[23];
    z[29]=6*z[12];
    z[36]= - z[10] + z[29];
    z[38]=z[3]*z[28];
    z[16]=z[16] + z[34] + z[38] + 2*z[36] + z[23];
    z[16]=z[8]*z[16];
    z[23]=z[25] + z[31];
    z[23]=z[11]*z[23];
    z[31]=3*z[5];
    z[34]=4*z[3];
    z[23]=z[23] - z[31] - z[34];
    z[23]=z[11]*z[23];
    z[36]= - z[9]*z[32];
    z[36]=static_cast<T>(1)+ z[36];
    z[36]=z[36]*z[35];
    z[36]=z[10] + z[36];
    z[36]=z[3]*z[36];
    z[38]=z[5]*z[35];
    z[16]=z[16] + z[23] + z[38] + z[36];
    z[16]=z[16]*z[21];
    z[23]=10*z[13];
    z[36]=npow(z[12],2);
    z[38]=z[36]*z[14];
    z[39]=12*z[38];
    z[40]= - z[39] - z[23] + 19*z[12];
    z[41]=2*z[14];
    z[42]=2*z[12] - z[38];
    z[42]=z[42]*z[41];
    z[43]=npow(z[14],2);
    z[44]=z[43]*z[7];
    z[45]=z[44]*z[12];
    z[42]=z[42] + z[45];
    z[46]=3*z[7];
    z[42]=z[42]*z[46];
    z[40]=2*z[40] + z[42];
    z[40]=z[7]*z[40];
    z[42]=z[10]*z[13];
    z[47]=10*z[42];
    z[48]=z[47] - 27*z[36];
    z[40]=2*z[48] + z[40];
    z[40]=z[3]*z[40];
    z[48]=z[3]*z[7];
    z[49]=z[44] + 4*z[14];
    z[50]= - z[7]*z[49];
    z[50]= - static_cast<T>(6)+ z[50];
    z[50]=z[50]*z[48];
    z[51]=3*z[14];
    z[52]=z[51] + z[44];
    z[52]=z[7]*z[52];
    z[52]=static_cast<T>(3)+ z[52];
    z[52]=z[3]*z[52];
    z[15]=z[15]*z[7];
    z[53]=z[15] + z[7];
    z[52]=z[52] + z[53];
    z[52]=z[11]*z[52];
    z[54]=z[7]*z[5];
    z[50]=z[52] - 3*z[54] + z[50];
    z[52]=3*z[11];
    z[50]=z[50]*z[52];
    z[27]=9*z[27];
    z[55]=20*z[42];
    z[56]= - z[55] + z[27];
    z[56]=z[7]*z[56];
    z[48]= - z[2]*z[55]*z[48];
    z[16]=z[48] + z[16] + z[50] + z[56] + z[40];
    z[16]=z[2]*z[16];
    z[40]=z[44]*z[35];
    z[48]=6*z[38];
    z[23]=z[48] - z[23] - z[12];
    z[23]=z[14]*z[23];
    z[23]= - z[40] + static_cast<T>(6)+ z[23];
    z[23]=z[7]*z[23];
    z[50]=5*z[42];
    z[56]= - z[50] + 12*z[36];
    z[56]=z[56]*z[41];
    z[57]=z[42]*z[14];
    z[58]=z[57] + z[10];
    z[59]=z[14]*z[58];
    z[59]=z[28] + z[59];
    z[59]=z[3]*z[59];
    z[60]=z[4]*z[1];
    z[60]= - static_cast<T>(1)+ z[60];
    z[60]=z[5]*z[60];
    z[60]=z[60] - 2*z[10];
    z[61]=z[12]*z[9];
    z[62]= - static_cast<T>(71)+ 54*z[61];
    z[62]=z[12]*z[62];
    z[23]=16*z[59] + z[23] + z[56] + 6*z[60] + z[62];
    z[23]=z[3]*z[23];
    z[51]=z[51] + 2*z[44];
    z[51]=z[7]*z[51];
    z[56]=z[53]*z[17];
    z[49]= - z[22] - z[49];
    z[49]=z[3]*z[49];
    z[49]=z[49] + z[56] + static_cast<T>(3)+ z[51];
    z[49]=z[11]*z[49];
    z[51]= - z[14] - z[44];
    z[51]=z[7]*z[51];
    z[51]= - static_cast<T>(2)+ z[51];
    z[51]=z[51]*z[24];
    z[56]=npow(z[4],2);
    z[59]=z[56]*z[14];
    z[60]=6*z[14] + z[44];
    z[60]=z[7]*z[60];
    z[60]=static_cast<T>(11)+ z[60];
    z[60]=z[3]*z[60];
    z[49]=z[49] + z[60] - 4*z[15] + z[59] + z[51];
    z[49]=z[49]*z[52];
    z[36]= - z[36]*z[41];
    z[36]=z[12] + z[36];
    z[36]=z[14]*z[36];
    z[36]=z[36] + z[45];
    z[36]=z[36]*z[46];
    z[51]=5*z[13];
    z[36]=z[36] - 18*z[38] - z[51] + 16*z[12];
    z[36]=z[36]*z[24];
    z[60]=3*z[4];
    z[30]=z[60]*z[30];
    z[27]= - z[50] + z[27];
    z[27]=z[7]*z[27];
    z[27]=z[30] + z[27];
    z[17]=z[27]*z[17];
    z[27]= - 11*z[10] - 18*z[12];
    z[27]=z[27]*z[32];
    z[30]=3*z[13];
    z[62]= - z[30]*z[59];
    z[63]= - z[13] - 2*z[5];
    z[63]=z[4]*z[63];
    z[16]=z[16] + z[18] + z[49] + z[23] + z[17] + z[36] + z[62] + z[27]
    + 3*z[63] + 23*z[42];
    z[16]=z[2]*z[16];
    z[17]=z[6]*z[9];
    z[18]=npow(z[9],3);
    z[23]=z[18]*z[3];
    z[17]=z[17] + z[23];
    z[27]=z[19]*z[6];
    z[18]=z[27] - z[18];
    z[27]= - z[11]*z[18];
    z[27]=z[27] + z[19] - z[17];
    z[27]=z[11]*z[27];
    z[36]=z[11] - z[10];
    z[18]=z[21]*z[18]*z[36];
    z[36]= - static_cast<T>(6)+ 5*z[28];
    z[49]=4*z[1];
    z[62]= - z[7]*z[49];
    z[62]=z[62] + z[36];
    z[62]=z[6]*z[62];
    z[36]= - z[9]*z[36];
    z[23]=z[10]*z[23];
    z[18]=z[18] + z[27] + z[23] + z[62] + z[49] + z[36];
    z[18]=z[8]*z[18];
    z[23]= - z[19]*z[34];
    z[17]= - z[20] + z[17];
    z[17]=z[11]*z[17];
    z[17]=z[17] + z[23] + z[22] - z[6];
    z[17]=z[11]*z[17];
    z[23]= - 7*z[9] - z[33];
    z[23]=z[23]*z[35];
    z[27]=z[13]*z[33];
    z[33]=z[9]*z[13];
    z[27]=7*z[33] + z[27];
    z[27]=z[12]*z[27];
    z[27]=z[30] + z[27];
    z[27]=z[6]*z[27];
    z[19]=z[12]*z[19];
    z[19]=z[26] + 12*z[19];
    z[19]=z[3]*z[19];
    z[26]=z[28] - 1;
    z[24]=z[1]*z[24];
    z[17]=z[18] + z[17] + z[19] + 3*z[27] + z[24] - 4*z[26] + z[23];
    z[17]=z[17]*z[21];
    z[18]= - z[56]*z[30];
    z[19]=z[13] - z[10];
    z[19]=z[19]*z[29];
    z[19]=z[50] + z[19];
    z[19]=z[19]*z[32];
    z[21]=z[42]*z[38];
    z[18]= - 24*z[21] + z[18] + z[19];
    z[18]=z[14]*z[18];
    z[19]= - z[12] - z[38];
    z[19]=z[19]*z[41];
    z[19]=z[19] + z[45];
    z[19]=z[19]*z[46];
    z[21]=20*z[13];
    z[19]=z[19] - z[39] + 7*z[12] - z[21] + z[31];
    z[19]=z[7]*z[19];
    z[23]= - z[60] - z[51];
    z[23]=z[10]*z[23];
    z[24]=z[30]*z[4];
    z[23]= - z[24] + z[23];
    z[27]= - 6*z[61] - 7;
    z[27]=z[13]*z[27];
    z[27]= - 8*z[10] + 9*z[5] + z[27];
    z[27]=z[27]*z[35];
    z[29]=9*z[12];
    z[30]=z[54]*z[29];
    z[31]= - z[12]*z[47];
    z[30]=z[31] + z[30];
    z[30]=z[6]*z[30];
    z[18]=z[30] + z[19] + z[18] + 2*z[23] + z[27];
    z[18]=z[6]*z[18];
    z[19]=12*z[5];
    z[23]=z[29] + z[13] - z[19] - 12*z[58];
    z[23]=z[14]*z[23];
    z[27]= - z[13] + z[12];
    z[27]=z[27]*z[44];
    z[25]=z[26]*z[25];
    z[19]=z[1]*z[19];
    z[19]= - 16*z[25] + 10*z[27] + z[23] + 43*z[61] + 18*z[28] - static_cast<T>(23)+ 
    z[19];
    z[19]=z[3]*z[19];
    z[23]=z[44] - z[41];
    z[25]=z[43]*npow(z[7],2);
    z[26]= - z[5] + z[53];
    z[26]=z[6]*z[26];
    z[25]=z[25] + z[26];
    z[25]=z[6]*z[25];
    z[26]=z[14]*z[9];
    z[20]=z[20] + z[26];
    z[20]=z[3]*z[20];
    z[20]=z[20] + z[25] - z[23];
    z[20]=z[11]*z[20];
    z[23]=z[23]*z[7];
    z[23]=z[23] + 1;
    z[25]= - z[7]*z[23];
    z[15]= - z[15] + z[25] + 2*z[4] + z[59];
    z[15]=z[6]*z[15];
    z[22]= - z[22] - z[41];
    z[22]=z[3]*z[22];
    z[25]= - z[14]*z[4];
    z[15]=z[20] + z[22] + z[15] + z[25] + z[23];
    z[15]=z[15]*z[52];
    z[20]=2*z[13] - z[37];
    z[20]=5*z[20] + z[35];
    z[20]=z[20]*z[32];
    z[22]=z[12]*z[57];
    z[20]= - 56*z[22] + z[20] + z[24] + z[55];
    z[20]=z[14]*z[20];
    z[21]=z[48] + z[21] + z[35];
    z[21]=z[14]*z[21];
    z[21]= - z[40] + static_cast<T>(7)+ z[21];
    z[21]=z[7]*z[21];
    z[22]=18*z[61] + static_cast<T>(57)- 61*z[33];
    z[22]=z[12]*z[22];
    z[23]=z[60] - 16*z[13];

    r += 27*z[5] - 42*z[10] + z[15] + z[16] + z[17] + z[18] + z[19] + 
      z[20] + z[21] + z[22] + 2*z[23];
 
    return r;
}

template double qqb_2lLC_r36(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r36(const std::array<dd_real,31>&);
#endif
