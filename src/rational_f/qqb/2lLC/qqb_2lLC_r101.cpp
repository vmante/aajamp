#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r101(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[9];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[3];
    z[7]=k[14];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=2*z[7];
    z[11]=z[7]*z[6];
    z[12]=static_cast<T>(1)+ z[11];
    z[12]=z[4]*z[12];
    z[12]=z[10] + z[12];
    z[12]=z[2]*z[12];
    z[13]=z[4]*z[1];
    z[11]=z[13] + static_cast<T>(1)- z[11];
    z[11]=z[2]*z[11];
    z[13]=z[8]*z[10];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[4]*z[13];
    z[11]=z[11] + z[7] + z[13];
    z[11]=z[5]*z[11];
    z[13]=z[4]*z[7];
    z[11]=z[11] - z[13] + z[12];
    z[11]=z[3]*z[11];
    z[12]=z[7]*z[9];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[4]*z[12];
    z[12]=z[10] + z[12];
    z[12]=z[2]*z[12];
    z[10]=z[4]*z[10];
    z[10]=z[10] + z[12];
    z[10]=z[5]*z[10];

    r += z[10] + z[11];
 
    return r;
}

template double qqb_2lLC_r101(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r101(const std::array<dd_real,31>&);
#endif
