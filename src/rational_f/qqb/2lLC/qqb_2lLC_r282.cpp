#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r282(const std::array<T,31>& k) {
  T z[127];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[8];
    z[6]=k[12];
    z[7]=k[11];
    z[8]=k[13];
    z[9]=k[10];
    z[10]=k[14];
    z[11]=k[4];
    z[12]=k[3];
    z[13]=k[2];
    z[14]=k[9];
    z[15]=k[15];
    z[16]=npow(z[5],3);
    z[17]=npow(z[1],4);
    z[18]=z[17]*z[11];
    z[19]=npow(z[2],2);
    z[20]=z[19]*z[16]*z[18];
    z[21]=npow(z[2],3);
    z[22]=z[17]*z[16]*z[21];
    z[23]=n<T>(1,4)*z[22] - z[20];
    z[23]=z[11]*z[23];
    z[24]=z[2]*z[1];
    z[25]=npow(z[24],5);
    z[26]=z[25]*z[16];
    z[27]=npow(z[5],4);
    z[28]=z[3]*z[27]*npow(z[24],6);
    z[26]= - 39*z[26] + 13*z[28];
    z[28]=z[8]*z[26];
    z[29]=npow(z[1],5);
    z[30]=z[27]*z[29];
    z[31]=npow(z[2],4);
    z[32]=z[30]*z[31];
    z[33]=3*z[32];
    z[30]=z[11]*z[30]*z[21];
    z[34]= - z[33] + 13*z[30];
    z[34]=z[11]*z[34];
    z[25]=z[25]*z[27];
    z[27]=7*z[25];
    z[28]=z[27] + z[34] + z[28];
    z[34]=n<T>(1,8)*z[3];
    z[28]=z[28]*z[34];
    z[35]=npow(z[24],4);
    z[16]=z[35]*z[16];
    z[36]=2*z[16];
    z[37]=npow(z[5],2);
    z[35]=z[35]*z[37];
    z[38]=z[8]*z[35];
    z[23]=z[28] + n<T>(45,8)*z[38] - z[36] + n<T>(3,2)*z[23];
    z[23]=z[3]*z[23];
    z[28]=npow(z[1],3);
    z[38]=z[28]*z[11];
    z[39]=z[2]*z[38]*z[37];
    z[40]=z[37]*z[28];
    z[41]=z[40]*z[19];
    z[42]=z[39] - z[41];
    z[43]=3*z[11];
    z[42]=z[42]*z[43];
    z[40]=z[40]*z[21];
    z[42]=z[42] + 13*z[40];
    z[43]=z[28]*z[8];
    z[44]=z[21]*z[5];
    z[45]=z[44]*z[43];
    z[23]=z[23] + n<T>(1,8)*z[42] - 3*z[45];
    z[23]=z[3]*z[23];
    z[45]=npow(z[1],2);
    z[46]=z[19] + z[45];
    z[47]= - z[46]*z[19];
    z[48]=npow(z[2],7);
    z[49]=z[48]*z[5];
    z[50]=npow(z[2],6);
    z[51]= - z[49] + 3*z[50];
    z[51]=z[51]*z[5];
    z[52]=npow(z[2],5);
    z[53]=n<T>(7,2)*z[52];
    z[51]=z[51] - z[53];
    z[54]=n<T>(1,2)*z[5];
    z[55]= - z[51]*z[54];
    z[47]=z[47] + z[55];
    z[47]=z[5]*z[47];
    z[55]=z[45] - z[19];
    z[56]=n<T>(1,4)*z[2];
    z[55]=z[55]*z[56];
    z[57]=z[50]*z[5];
    z[58]= - z[52] + n<T>(1,2)*z[57];
    z[58]=z[58]*z[5];
    z[59]=n<T>(3,4)*z[31];
    z[60]=z[59] + z[58];
    z[60]=z[5]*z[60];
    z[55]=z[55] + z[60];
    z[55]=z[5]*z[55];
    z[60]=z[52]*z[5];
    z[61]=z[60] - z[31];
    z[61]=z[61]*z[5];
    z[61]=z[61] + n<T>(1,2)*z[21];
    z[62]=z[37]*z[11];
    z[61]=z[61]*z[62];
    z[55]=z[55] + n<T>(1,2)*z[61];
    z[55]=z[11]*z[55];
    z[47]=z[47] + z[55];
    z[55]=n<T>(1,4)*z[5];
    z[63]=z[55]*npow(z[2],8);
    z[63]=z[63] - z[48];
    z[63]=z[63]*z[5];
    z[63]=z[63] + n<T>(13,8)*z[50];
    z[63]=z[63]*z[5];
    z[63]=z[63] - n<T>(11,8)*z[52];
    z[63]=z[63]*z[5];
    z[64]=n<T>(1,2)*z[19];
    z[46]=z[46]*z[64];
    z[65]=n<T>(1,2)*z[45];
    z[66]=z[65] + z[19];
    z[67]=n<T>(1,8)*z[2];
    z[68]=z[11]*z[66]*z[67];
    z[46]=z[68] + z[46] + z[63];
    z[46]=z[8]*z[46];
    z[23]=z[23] + n<T>(1,2)*z[47] + z[46];
    z[23]=z[3]*z[23];
    z[46]=z[31]*z[5];
    z[46]=z[46] - z[21];
    z[46]=z[46]*z[5];
    z[46]=z[46] + n<T>(3,8)*z[19];
    z[46]=z[46]*z[62];
    z[47]= - z[31] + n<T>(1,2)*z[60];
    z[47]=z[47]*z[5];
    z[68]=n<T>(5,8)*z[21];
    z[69]=z[68] + z[47];
    z[69]=z[5]*z[69];
    z[70]=n<T>(1,8)*z[19];
    z[69]= - z[70] + z[69];
    z[69]=z[5]*z[69];
    z[69]=z[69] + z[46];
    z[69]=z[11]*z[69];
    z[49]= - z[49] + n<T>(7,2)*z[50];
    z[49]=z[49]*z[5];
    z[49]=z[49] - n<T>(39,8)*z[52];
    z[49]=z[49]*z[5];
    z[49]=z[49] + n<T>(27,8)*z[31];
    z[49]=z[49]*z[5];
    z[71]=n<T>(7,8)*z[21];
    z[72]=z[11]*z[70];
    z[72]=z[72] + z[71] - z[49];
    z[72]=z[8]*z[72];
    z[73]= - n<T>(3,4)*z[57] + 2*z[52];
    z[73]=z[73]*z[5];
    z[73]=z[73] - 2*z[31];
    z[73]=z[73]*z[5];
    z[71]= - z[71] - z[73];
    z[71]=z[5]*z[71];
    z[23]=z[23] + z[72] + z[71] + z[69];
    z[69]=111*z[3];
    z[23]=z[23]*z[69];
    z[71]=z[45]*z[11];
    z[72]=n<T>(1,2)*z[71];
    z[74]=n<T>(1,2)*z[28];
    z[75]=z[45]*z[2];
    z[76]=z[72] - z[74] + z[75];
    z[77]=n<T>(1,3)*z[8];
    z[76]=z[76]*z[77];
    z[78]=n<T>(1,3)*z[17];
    z[79]=z[38] + z[78];
    z[80]=z[8]*z[79];
    z[80]= - z[28] + z[80];
    z[80]=z[6]*z[80];
    z[81]=z[29]*z[8];
    z[82]=z[81] - z[78];
    z[82]=z[82]*z[6];
    z[83]=z[78]*z[8];
    z[84]=z[83] + z[82];
    z[85]=n<T>(1,2)*z[4];
    z[84]=z[84]*z[85];
    z[76]=z[84] + n<T>(1,2)*z[80] + n<T>(3,2)*z[45] + z[76];
    z[80]=z[28]*z[2];
    z[84]=z[80] - z[17];
    z[86]=z[75] - z[28];
    z[87]=z[11]*z[86];
    z[87]=z[87] + z[84];
    z[87]=z[8]*z[87];
    z[88]=z[18] + z[29];
    z[89]= - z[8]*z[88];
    z[89]=z[17] + z[89];
    z[89]=z[6]*z[89];
    z[87]=z[87] + z[89];
    z[89]=npow(z[1],6);
    z[90]=z[89]*z[8];
    z[91]=z[90] - z[29];
    z[91]=z[91]*z[6];
    z[92]=z[17]*z[2];
    z[93]=z[92] - z[29];
    z[94]=z[93]*z[77];
    z[94]= - n<T>(1,3)*z[91] + 3*z[17] + z[94];
    z[94]=z[94]*z[85];
    z[87]=z[94] + 3*z[28] + n<T>(1,6)*z[87];
    z[87]=z[7]*z[87];
    z[76]=n<T>(1,2)*z[76] + z[87];
    z[87]=7*z[7];
    z[76]=z[76]*z[87];
    z[94]=5*z[45];
    z[95]=z[94] + 11*z[24];
    z[95]=z[95]*z[2];
    z[95]=z[95] + z[28];
    z[96]=z[95]*z[5];
    z[97]=n<T>(1,4)*z[45];
    z[98]=z[97] + z[24];
    z[99]= - z[98] + n<T>(1,8)*z[96];
    z[99]=z[99]*z[5];
    z[99]=z[99] + n<T>(1,8)*z[1];
    z[62]=111*z[62];
    z[62]=z[99]*z[62];
    z[99]=n<T>(1293865,2)*z[45] + 370669*z[24];
    z[99]=z[99]*z[56];
    z[99]=76933*z[28] + z[99];
    z[100]= - 17*z[45] - 15*z[24];
    z[100]=z[2]*z[100];
    z[100]= - 9*z[28] + z[100];
    z[101]=n<T>(1,2)*z[2];
    z[100]=z[100]*z[101];
    z[100]= - z[17] + z[100];
    z[100]=z[5]*z[100];
    z[99]=n<T>(1,81)*z[99] + n<T>(111,4)*z[100];
    z[99]=z[5]*z[99];
    z[100]=578*z[45] - n<T>(371,8)*z[24];
    z[99]=n<T>(1,9)*z[100] + z[99];
    z[99]=z[5]*z[99];
    z[99]= - n<T>(3311,72)*z[1] + z[99];
    z[99]=z[5]*z[99];
    z[99]=z[99] + z[62];
    z[99]=z[11]*z[99];
    z[84]=z[84]*z[8];
    z[100]= - z[84] + z[82];
    z[93]=z[93]*z[8];
    z[91]= - z[93] + z[91];
    z[102]=n<T>(1,3)*z[4];
    z[91]=z[91]*z[102];
    z[91]=n<T>(1,2)*z[100] + z[91];
    z[100]=n<T>(211,4)*z[4];
    z[91]=z[91]*z[100];
    z[103]=z[8]*z[86];
    z[103]= - 21*z[45] - n<T>(211,12)*z[103];
    z[104]=z[17]*z[8];
    z[105]= - n<T>(7,3)*z[28] + n<T>(61,8)*z[104];
    z[105]=z[6]*z[105];
    z[91]=z[91] + n<T>(1,2)*z[103] + z[105];
    z[91]=z[91]*z[85];
    z[103]=z[24] + z[45];
    z[105]=z[103]*z[2];
    z[106]= - n<T>(162857,2)*z[28] + 58951*z[105];
    z[106]=z[2]*z[106];
    z[106]= - n<T>(298741,4)*z[17] + z[106];
    z[107]=3*z[24];
    z[108]=n<T>(7,2)*z[45] + z[107];
    z[108]=z[2]*z[108];
    z[108]=n<T>(7,2)*z[28] + z[108];
    z[108]=z[108]*z[101];
    z[108]=z[17] + z[108];
    z[108]=z[2]*z[108];
    z[108]=n<T>(1,4)*z[29] + z[108];
    z[108]=z[5]*z[108];
    z[106]=n<T>(1,81)*z[106] + n<T>(111,2)*z[108];
    z[106]=z[5]*z[106];
    z[108]=186041*z[45] - n<T>(271493,4)*z[24];
    z[108]=z[2]*z[108];
    z[108]=n<T>(1087585,4)*z[28] + z[108];
    z[106]=n<T>(1,324)*z[108] + z[106];
    z[106]=z[5]*z[106];
    z[108]=3311*z[45];
    z[109]=z[108] - n<T>(11617,2)*z[24];
    z[106]=n<T>(1,36)*z[109] + z[106];
    z[106]=z[5]*z[106];
    z[95]=z[95]*z[44];
    z[109]=n<T>(1060099,8)*z[45] + 40969*z[24];
    z[109]=z[2]*z[109];
    z[109]=n<T>(280759,4)*z[28] + z[109];
    z[110]=n<T>(1,81)*z[19];
    z[109]=z[109]*z[110];
    z[109]=z[109] + n<T>(111,8)*z[95];
    z[109]=z[5]*z[109];
    z[111]= - 118223*z[45] - n<T>(374933,4)*z[24];
    z[112]=n<T>(1,3)*z[2];
    z[111]=z[111]*z[112];
    z[111]=n<T>(5309,4)*z[28] + z[111];
    z[113]=n<T>(1,36)*z[2];
    z[111]=z[111]*z[113];
    z[109]=z[111] + z[109];
    z[109]=z[5]*z[109];
    z[111]=5309*z[45];
    z[114]= - z[111] + n<T>(98351,27)*z[24];
    z[114]=z[2]*z[114];
    z[109]=n<T>(1,48)*z[114] + z[109];
    z[109]=z[5]*z[109];
    z[114]= - 1067*z[45] + n<T>(9619,4)*z[24];
    z[115]=z[11]*z[1];
    z[109]=n<T>(2155,72)*z[115] + n<T>(1,18)*z[114] + z[109];
    z[109]=z[8]*z[109];
    z[114]=z[72] - z[28];
    z[116]=z[114]*z[8];
    z[117]=n<T>(2155,2)*z[45] + 2071*z[116];
    z[117]=z[6]*z[117];
    z[23]=z[76] + z[91] + z[23] + n<T>(1,36)*z[117] + z[109] + z[99] + n<T>(111,16)*z[1] + z[106];
    z[23]=z[7]*z[23];
    z[72]=z[72] + z[28];
    z[76]= - z[89]*z[77];
    z[91]=n<T>(1,2)*z[29];
    z[76]=z[91] + z[76];
    z[99]=487*z[8];
    z[76]=z[76]*z[99];
    z[76]= - n<T>(9,2)*z[17] + z[76];
    z[76]=z[8]*z[76];
    z[76]=n<T>(350429,90)*z[72] + z[76];
    z[106]=z[38] - z[78];
    z[81]=z[81] + z[106];
    z[18]=z[18] - z[29];
    z[90]=z[90] + z[18];
    z[109]=n<T>(2,3)*z[6];
    z[117]=z[90]*z[109];
    z[117]=z[117] + z[81];
    z[118]=14*z[6];
    z[117]=z[117]*z[118];
    z[76]=n<T>(1,8)*z[76] + z[117];
    z[76]=z[6]*z[76];
    z[117]= - z[4]*z[89];
    z[117]=z[117] - z[88];
    z[119]=n<T>(2,3)*z[7];
    z[117]=z[117]*z[119];
    z[120]=z[29]*z[4];
    z[117]=z[117] + z[120] + z[79];
    z[121]=14*z[7];
    z[117]=z[117]*z[121];
    z[122]=2*z[28];
    z[123]= - z[122] + z[71];
    z[89]= - z[89]*z[102];
    z[89]= - z[91] + z[89];
    z[91]=211*z[4];
    z[89]=z[89]*z[91];
    z[89]= - n<T>(145,2)*z[17] + z[89];
    z[124]=n<T>(1,8)*z[4];
    z[89]=z[89]*z[124];
    z[89]=z[117] + n<T>(1253,45)*z[123] + z[89];
    z[89]=z[7]*z[89];
    z[90]=z[90]*z[6];
    z[117]=z[18]*z[8];
    z[90]=z[90] - z[117];
    z[123]=n<T>(2,3)*z[10];
    z[125]=z[90]*z[123];
    z[126]=z[106]*z[8];
    z[82]=z[125] + z[126] - z[82];
    z[125]=13*z[10];
    z[82]=z[82]*z[125];
    z[18]=z[18]*z[77];
    z[18]= - n<T>(1,2)*z[106] + z[18];
    z[18]=z[18]*z[99];
    z[18]=535*z[72] + z[18];
    z[18]=z[8]*z[18];
    z[18]=n<T>(131197,72)*z[45] + z[18];
    z[88]=z[88]*z[102];
    z[79]=n<T>(1,2)*z[79] + z[88];
    z[79]=z[79]*z[91];
    z[79]=n<T>(5753,36)*z[114] + z[79];
    z[79]=z[79]*z[124];
    z[18]=z[82] + z[89] + z[79] + n<T>(1,8)*z[18] + z[76];
    z[18]=z[10]*z[18];
    z[76]=n<T>(1,2)*z[17];
    z[79]= - z[76] - z[117];
    z[79]=z[79]*z[99];
    z[72]=n<T>(10330567,540)*z[72] + z[79];
    z[72]=z[8]*z[72];
    z[72]= - n<T>(16366129,540)*z[45] + z[72];
    z[79]=z[117]*z[109];
    z[79]=z[126] + z[79];
    z[79]=z[79]*z[118];
    z[72]=n<T>(1,24)*z[72] + z[79];
    z[72]=z[6]*z[72];
    z[79]=z[10]*z[90];
    z[82]=z[6]*z[117];
    z[88]=z[8]*z[38];
    z[79]=z[79] + z[82] + z[28] + z[88];
    z[79]=z[9]*z[79];
    z[82]=z[6]*z[126];
    z[81]=z[6]*z[81];
    z[81]=z[83] + z[81];
    z[81]=z[10]*z[81];
    z[81]=z[82] + z[81];
    z[82]=z[97] - z[116];
    z[79]=n<T>(1,6)*z[79] + n<T>(1,3)*z[82] + n<T>(1,4)*z[81];
    z[79]=z[9]*z[79];
    z[81]=n<T>(1,3)*z[28];
    z[82]= - z[81] + z[71];
    z[88]= - z[38]*z[77];
    z[82]=n<T>(1,2)*z[82] + z[88];
    z[82]=z[82]*z[99];
    z[88]=n<T>(8267227,540)*z[45] - 839*z[115];
    z[82]=n<T>(1,6)*z[88] + z[82];
    z[82]=z[8]*z[82];
    z[82]=n<T>(839,3)*z[1] + z[82];
    z[81]= - z[81] - z[71];
    z[38]= - z[17] - z[38];
    z[38]=z[38]*z[102];
    z[38]=n<T>(1,2)*z[81] + z[38];
    z[81]=n<T>(211,8)*z[4];
    z[38]=z[38]*z[81];
    z[38]=z[38] + n<T>(211,16)*z[45] + n<T>(1673,45)*z[115];
    z[38]=z[4]*z[38];
    z[88]=2*z[17];
    z[89]=z[88] + z[120];
    z[89]=z[89]*z[119];
    z[90]=z[17]*z[4];
    z[89]=z[89] - z[122] - z[90];
    z[89]=z[89]*z[121];
    z[97]=z[29]*z[102];
    z[97]=z[76] + z[97];
    z[97]=z[97]*z[81];
    z[97]=n<T>(14,3)*z[28] + z[97];
    z[97]=z[4]*z[97];
    z[89]=z[89] + n<T>(1883,45)*z[45] + z[97];
    z[89]=z[7]*z[89];
    z[18]=7*z[79] + z[18] + z[89] + z[38] + n<T>(1,8)*z[82] + z[72];
    z[18]=z[9]*z[18];
    z[38]=3*z[20];
    z[72]=5*z[22] + z[38];
    z[72]=z[11]*z[72];
    z[36]=z[36] + n<T>(1,8)*z[72];
    z[36]=z[8]*z[36];
    z[20]= - n<T>(13,4)*z[22] + z[20];
    z[20]=z[11]*z[20];
    z[20]=n<T>(13,4)*z[16] + z[20];
    z[20]=z[6]*z[20];
    z[33]= - z[33] - z[30];
    z[33]=z[11]*z[33];
    z[27]= - z[27] + z[33];
    z[27]=z[8]*z[27];
    z[33]=z[32] - z[30];
    z[33]=z[11]*z[33];
    z[33]= - z[25] + z[33];
    z[33]=z[6]*z[33];
    z[27]=z[27] + 13*z[33];
    z[27]=z[27]*z[34];
    z[20]=z[27] + z[36] + n<T>(3,2)*z[20];
    z[20]=z[3]*z[20];
    z[27]= - z[8]*z[42];
    z[33]= - z[39] + 9*z[41];
    z[33]=z[33]*z[11];
    z[33]=z[33] - 15*z[40];
    z[36]=z[6]*z[33];
    z[27]=z[27] + 3*z[36];
    z[20]=n<T>(1,8)*z[27] + z[20];
    z[20]=z[3]*z[20];
    z[27]=z[55]*z[52];
    z[27]=z[27] - z[31];
    z[27]=z[27]*z[5];
    z[27]=z[27] + n<T>(13,8)*z[21];
    z[27]=z[27]*z[5];
    z[36]=11*z[19];
    z[39]=z[36] + z[45];
    z[27]=z[27] - n<T>(1,8)*z[39];
    z[27]=z[27]*z[5];
    z[27]=z[27] + n<T>(5,8)*z[2];
    z[27]=z[27]*z[11];
    z[39]=3*z[45];
    z[40]=5*z[19];
    z[41]= - z[39] - z[40];
    z[41]=z[2]*z[41];
    z[42]=z[59] - z[58];
    z[42]=z[5]*z[42];
    z[41]=n<T>(3,4)*z[41] + z[42];
    z[41]=z[5]*z[41];
    z[41]=n<T>(17,4)*z[19] + z[41];
    z[41]=n<T>(1,2)*z[41] - z[27];
    z[41]=z[11]*z[41];
    z[42]=z[45] + n<T>(3,4)*z[19];
    z[42]=z[42]*z[19];
    z[51]=z[51]*z[55];
    z[42]=z[42] + z[51];
    z[42]=z[5]*z[42];
    z[41]=z[41] - n<T>(3,4)*z[21] + z[42];
    z[41]=z[8]*z[41];
    z[42]=z[48]*z[55];
    z[42]=z[42] - z[50];
    z[42]=z[42]*z[5];
    z[42]=z[42] + n<T>(13,8)*z[52];
    z[42]=z[42]*z[5];
    z[48]=z[94] + n<T>(9,4)*z[19];
    z[48]=z[48]*z[64];
    z[48]=z[48] - z[42];
    z[48]=z[5]*z[48];
    z[51]= - z[94] - 3*z[19];
    z[51]=z[51]*z[67];
    z[50]=z[55]*z[50];
    z[50]=z[50] - z[52];
    z[50]=z[50]*z[5];
    z[52]=n<T>(7,8)*z[31] + z[50];
    z[52]=z[5]*z[52];
    z[51]=z[51] + z[52];
    z[51]=z[5]*z[51];
    z[51]=z[51] - n<T>(1,4)*z[61];
    z[51]=z[11]*z[51];
    z[52]=n<T>(3,8)*z[21];
    z[48]=z[51] - z[52] + z[48];
    z[48]=z[6]*z[48];
    z[20]=z[20] + z[48] - n<T>(1,4)*z[19] + z[41];
    z[20]=z[3]*z[20];
    z[41]=z[55]*z[31];
    z[41]=z[41] - z[21];
    z[41]=z[41]*z[5];
    z[41]=z[41] + n<T>(3,2)*z[19];
    z[41]=z[41]*z[5];
    z[41]=z[41] - z[2];
    z[41]=z[41]*z[5];
    z[41]=z[41] + n<T>(1,4);
    z[41]=z[41]*z[11];
    z[47]=z[52] - z[47];
    z[47]=z[5]*z[47];
    z[47]= - n<T>(19,8)*z[19] + z[47];
    z[47]=z[5]*z[47];
    z[47]= - z[41] + n<T>(17,8)*z[2] + z[47];
    z[47]=z[11]*z[47];
    z[48]=n<T>(11,8)*z[21] + z[73];
    z[48]=z[5]*z[48];
    z[47]=z[47] - n<T>(5,4)*z[19] + z[48];
    z[47]=z[8]*z[47];
    z[48]= - z[60] + n<T>(7,2)*z[31];
    z[48]=z[48]*z[5];
    z[51]=n<T>(23,8)*z[21];
    z[52]=z[51] - z[48];
    z[52]=z[5]*z[52];
    z[52]= - n<T>(7,8)*z[19] + z[52];
    z[52]=z[5]*z[52];
    z[46]=z[52] - z[46];
    z[46]=z[11]*z[46];
    z[52]=z[53] - z[57];
    z[52]=z[52]*z[5];
    z[52]=z[52] - n<T>(39,8)*z[31];
    z[52]=z[52]*z[5];
    z[51]=z[51] + z[52];
    z[51]=z[5]*z[51];
    z[53]=n<T>(5,8)*z[19];
    z[46]=z[46] - z[53] + z[51];
    z[46]=z[6]*z[46];
    z[20]=z[20] + z[46] - z[56] + z[47];
    z[20]=z[20]*z[69];
    z[22]=23*z[22] - z[38];
    z[22]=z[11]*z[22];
    z[26]=z[6]*z[26];
    z[30]= - 7*z[32] + z[30];
    z[30]=z[11]*z[30];
    z[25]=13*z[25] + z[30] + z[26];
    z[25]=z[3]*z[25];
    z[26]=z[6]*z[35];
    z[16]=z[25] + 45*z[26] - 39*z[16] + z[22];
    z[16]=z[16]*z[34];
    z[22]= - z[6]*z[28]*z[44];
    z[22]= - n<T>(1,8)*z[33] + z[22];
    z[16]=3*z[22] + z[16];
    z[16]=z[3]*z[16];
    z[22]=13*z[45] + z[36];
    z[22]=z[22]*z[67];
    z[25]= - n<T>(13,8)*z[31] - z[50];
    z[25]=z[5]*z[25];
    z[22]=z[22] + z[25];
    z[22]=z[5]*z[22];
    z[22]=z[27] - z[53] + z[22];
    z[22]=z[11]*z[22];
    z[25]= - z[39] - n<T>(11,8)*z[19];
    z[25]=z[25]*z[19];
    z[25]=z[25] + z[42];
    z[25]=z[5]*z[25];
    z[26]=n<T>(11,2)*z[45] + z[40];
    z[26]=z[26]*z[70];
    z[26]=z[26] + z[63];
    z[26]=z[6]*z[26];
    z[16]=z[16] + z[26] + z[22] + z[68] + z[25];
    z[16]=z[3]*z[16];
    z[22]= - n<T>(39,8)*z[21] + z[48];
    z[22]=z[5]*z[22];
    z[22]=n<T>(27,8)*z[19] + z[22];
    z[22]=z[5]*z[22];
    z[22]=z[41] - n<T>(9,8)*z[2] + z[22];
    z[22]=z[11]*z[22];
    z[25]= - n<T>(27,8)*z[21] - z[52];
    z[25]=z[5]*z[25];
    z[21]=n<T>(9,8)*z[21] - z[49];
    z[21]=z[6]*z[21];
    z[16]=z[16] + z[21] + z[22] + n<T>(9,8)*z[19] + z[25];
    z[16]=z[16]*z[69];
    z[21]=406633*z[24] + n<T>(1221937,2)*z[45];
    z[21]=z[21]*z[101];
    z[21]=z[21] + 144875*z[28];
    z[22]= - z[21]*z[110];
    z[22]=z[22] + n<T>(111,4)*z[95];
    z[22]=z[5]*z[22];
    z[25]= - n<T>(846917,2)*z[24] + 371569*z[45];
    z[25]=z[25]*z[2];
    z[25]=z[25] + n<T>(1248397,2)*z[28];
    z[26]=n<T>(1,324)*z[2];
    z[27]= - z[25]*z[26];
    z[22]=z[27] + z[22];
    z[22]=z[5]*z[22];
    z[27]= - n<T>(139673,9)*z[45] - n<T>(371,2)*z[24];
    z[27]=z[2]*z[27];
    z[27]= - n<T>(609299,18)*z[28] + z[27];
    z[22]=n<T>(1,18)*z[27] + z[22];
    z[22]=z[5]*z[22];
    z[27]=7*z[45];
    z[30]=z[27] - n<T>(1313,12)*z[24];
    z[31]=1224295*z[28] + 620693*z[75];
    z[31]=z[8]*z[31];
    z[22]=n<T>(1,648)*z[31] + n<T>(1,6)*z[30] + z[22];
    z[22]=z[6]*z[22];
    z[30]= - n<T>(1185973,2)*z[45] - 388651*z[24];
    z[30]=z[30]*z[110];
    z[31]=z[45] + z[107];
    z[32]=n<T>(333,2)*z[44];
    z[31]=z[31]*z[32];
    z[30]=z[30] + z[31];
    z[30]=z[5]*z[30];
    z[31]= - 1367593*z[45] + 774989*z[24];
    z[26]=z[31]*z[26];
    z[26]=z[26] + z[30];
    z[26]=z[5]*z[26];
    z[30]=1627*z[24];
    z[31]= - n<T>(1188799,18)*z[45] + z[30];
    z[26]=n<T>(1,18)*z[31] + z[26];
    z[26]=z[26]*z[54];
    z[31]=z[44]*z[1];
    z[32]=z[19]*z[1];
    z[33]=n<T>(316723,81)*z[32] - n<T>(333,2)*z[31];
    z[33]=z[5]*z[33];
    z[33]=n<T>(3625,18)*z[24] + z[33];
    z[33]=z[5]*z[33];
    z[34]=n<T>(3311,18)*z[1];
    z[33]= - z[34] + z[33];
    z[33]=z[5]*z[33];
    z[33]= - static_cast<T>(111)+ z[33];
    z[35]=n<T>(1,2)*z[11];
    z[33]=z[33]*z[35];
    z[36]=z[45]*z[8];
    z[22]=z[22] + n<T>(153749,81)*z[36] + z[33] + z[26] - n<T>(289,9)*z[1] + 
   n<T>(1221,8)*z[2];
    z[26]= - n<T>(1,3)*z[86] + z[84];
    z[26]=z[6]*z[26];
    z[26]= - z[45] + z[26];
    z[33]=z[75] + z[28];
    z[38]=z[33]*z[2];
    z[39]=z[38] - z[17];
    z[40]=z[93] - z[39];
    z[40]=z[6]*z[40];
    z[40]=z[40] + z[71] - z[33];
    z[40]=z[40]*z[102];
    z[26]=n<T>(1,2)*z[26] + z[40];
    z[26]=z[26]*z[81];
    z[16]=z[26] + n<T>(1,2)*z[22] + z[16];
    z[16]=z[4]*z[16];
    z[22]=z[92] + 2*z[29];
    z[26]=z[22]*z[8];
    z[40]=z[122] + z[75];
    z[41]=z[40]*z[11];
    z[26]=z[41] + z[26] + z[39];
    z[39]=z[26]*z[109];
    z[42]=4*z[28] + 5*z[75];
    z[39]=z[39] + z[104] + n<T>(1,3)*z[42] + z[71];
    z[39]=z[39]*z[118];
    z[42]=z[74]*z[2];
    z[42]=z[42] + z[17];
    z[42]=n<T>(1,2)*z[42];
    z[46]=z[101]*z[17];
    z[29]=z[46] + z[29];
    z[46]= - z[29]*z[77];
    z[46]=z[42] + z[46];
    z[46]=z[46]*z[99];
    z[47]= - 25*z[28] + n<T>(7,4)*z[75];
    z[46]=3*z[47] + z[46];
    z[46]=z[8]*z[46];
    z[47]=32795327*z[45] + 20074043*z[24];
    z[47]=n<T>(1,18)*z[47] + 336149*z[115];
    z[46]=n<T>(1,360)*z[47] + z[46];
    z[39]=n<T>(1,4)*z[46] + z[39];
    z[39]=z[6]*z[39];
    z[46]=z[88] + z[80];
    z[22]= - z[4]*z[22];
    z[22]=z[22] - z[41] - z[46];
    z[22]=z[22]*z[119];
    z[41]= - z[28] - 2*z[75];
    z[22]=z[22] + z[90] + n<T>(1,3)*z[41] + z[71];
    z[22]=z[22]*z[121];
    z[29]= - z[29]*z[102];
    z[29]= - z[42] + z[29];
    z[29]=z[4]*z[29];
    z[41]= - z[28] - n<T>(1,2)*z[75];
    z[29]=n<T>(1,6)*z[41] + z[29];
    z[29]=z[29]*z[100];
    z[41]= - 112*z[45] - n<T>(239,3)*z[24];
    z[41]=4*z[41] + n<T>(239,3)*z[115];
    z[22]=z[22] + n<T>(7,15)*z[41] + z[29];
    z[22]=z[7]*z[22];
    z[26]=z[6]*z[26];
    z[29]=z[104] + z[75];
    z[26]=z[26] + z[29];
    z[26]=z[26]*z[123];
    z[41]= - z[8]*z[46];
    z[40]=n<T>(1,3)*z[40] + z[41];
    z[40]=z[6]*z[40];
    z[26]=z[26] + z[40] - n<T>(1,3)*z[45] - z[43];
    z[26]=z[26]*z[125];
    z[40]=z[83] - z[74];
    z[40]=z[40]*z[99];
    z[41]=n<T>(677,2)*z[45] - z[40];
    z[41]=z[8]*z[41];
    z[41]=n<T>(145117,72)*z[1] + z[41];
    z[42]=z[4]*z[78];
    z[42]=z[74] + z[42];
    z[42]=z[42]*z[91];
    z[42]=n<T>(1843,144)*z[45] + z[42];
    z[42]=z[4]*z[42];
    z[22]=z[26] + z[22] + n<T>(1,4)*z[42] + n<T>(1,8)*z[41] + z[39];
    z[22]=z[10]*z[22];
    z[26]=z[8]*z[1];
    z[39]=z[9]*z[1];
    z[41]=z[39]*z[12];
    z[42]=z[41]*z[8];
    z[46]=z[6]*z[1];
    z[47]=520379*z[1] - n<T>(8675467,18)*z[36];
    z[47]=z[9]*z[47];
    z[46]= - n<T>(578759,10)*z[42] + n<T>(1,10)*z[47] + n<T>(440849,5)*z[26] + 
   n<T>(825737,18)*z[46];
    z[46]=z[12]*z[46];
    z[47]=n<T>(9726307,18)*z[45] - 361319*z[24];
    z[47]=z[8]*z[47];
    z[47]= - n<T>(13044881,18)*z[1] + z[47];
    z[48]= - n<T>(724789,2)*z[45] - 174727*z[24];
    z[48]=z[6]*z[48];
    z[47]=n<T>(1,2)*z[47] + n<T>(11,9)*z[48];
    z[48]=5420641*z[45] + n<T>(13044881,5)*z[24];
    z[49]=n<T>(363307,5)*z[45] + n<T>(202739,6)*z[24];
    z[49]=z[2]*z[49];
    z[49]=n<T>(8235389,30)*z[28] + 7*z[49];
    z[49]=z[6]*z[49];
    z[48]=n<T>(1,12)*z[48] + z[49];
    z[48]=z[10]*z[48];
    z[49]= - n<T>(11157847,2)*z[45] + 10179367*z[43];
    z[49]=z[9]*z[49];
    z[46]=z[46] + n<T>(1,90)*z[49] + n<T>(1,5)*z[47] + n<T>(1,3)*z[48];
    z[47]=n<T>(1,2)*z[1];
    z[48]= - z[47] - z[36];
    z[48]=z[9]*z[48];
    z[49]=n<T>(1,2)*z[8];
    z[50]=z[41]*z[49];
    z[48]=z[50] - z[26] + z[48];
    z[48]=z[12]*z[48];
    z[50]=z[45] + n<T>(1,2)*z[24];
    z[51]=z[8]*z[50];
    z[52]=z[8]*z[74];
    z[52]=z[45] + z[52];
    z[52]=z[9]*z[52];
    z[48]=z[48] + z[52] + z[47] + z[51];
    z[48]=z[12]*z[48];
    z[51]=z[28]*z[9];
    z[52]=z[6]*z[32];
    z[52]= - z[51] + z[24] + z[52];
    z[48]=n<T>(1,2)*z[52] + z[48];
    z[48]=z[12]*z[48];
    z[52]=z[19]*z[6];
    z[53]=z[103]*z[52];
    z[53]=z[53] - z[28] + z[105];
    z[55]=z[50]*z[19];
    z[50]=z[50]*z[2];
    z[50]=z[50] + z[74];
    z[57]= - z[50]*z[52];
    z[57]= - z[55] + z[57];
    z[57]=z[10]*z[57];
    z[48]=z[48] + n<T>(1,2)*z[53] + z[57];
    z[48]=z[14]*z[48];
    z[53]= - n<T>(3235547,3)*z[45] - n<T>(32003,2)*z[24];
    z[57]=709279*z[45] + n<T>(7110521,4)*z[24];
    z[58]=z[6]*z[2];
    z[57]=z[57]*z[58];
    z[53]=n<T>(1,2)*z[53] + n<T>(1,3)*z[57];
    z[57]=n<T>(6567103,3)*z[45] + 32003*z[24];
    z[57]=z[57]*z[113];
    z[59]= - 368431*z[45] - n<T>(7110521,27)*z[24];
    z[56]=z[59]*z[56];
    z[56]= - n<T>(709279,27)*z[28] + z[56];
    z[56]=z[56]*z[58];
    z[56]=z[57] + z[56];
    z[56]=z[10]*z[56];
    z[53]=n<T>(10442077,54)*z[51] + n<T>(1,9)*z[53] + z[56];
    z[56]=n<T>(1,2)*z[9];
    z[57]=11505899*z[1] + 32390053*z[36];
    z[57]=z[57]*z[56];
    z[26]= - n<T>(11505899,2)*z[42] + 10455059*z[26] + z[57];
    z[26]=z[12]*z[26];
    z[42]= - n<T>(32390053,2)*z[45] - 10442077*z[43];
    z[42]=z[9]*z[42];
    z[26]=z[42] + z[26];
    z[42]= - 30288373*z[45] - 9404219*z[24];
    z[42]=z[42]*z[77];
    z[42]= - 32003*z[1] + z[42];
    z[43]=z[6]*z[24];
    z[26]=n<T>(1,20)*z[42] + n<T>(825737,3)*z[43] + n<T>(1,30)*z[26];
    z[26]=z[12]*z[26];
    z[26]=n<T>(720653,27)*z[48] + n<T>(1,5)*z[53] + n<T>(1,9)*z[26];
    z[26]=z[14]*z[26];
    z[26]=n<T>(1,3)*z[46] + z[26];
    z[26]=z[14]*z[26];
    z[42]= - z[115]*z[64];
    z[42]=z[55] + z[42];
    z[42]=z[7]*z[42];
    z[43]=z[103]*z[64];
    z[46]= - z[7]*z[50]*z[19];
    z[43]=z[43] + z[46];
    z[43]=z[10]*z[43];
    z[42]=z[42] + z[43];
    z[43]= - z[115] + z[103];
    z[43]=z[11]*z[43];
    z[43]= - z[105] + z[43];
    z[46]= - z[45] + n<T>(1,2)*z[115];
    z[48]=n<T>(1,4)*z[24] + z[46];
    z[48]=z[11]*z[48];
    z[48]= - n<T>(1,2)*z[86] + z[48];
    z[48]=z[11]*z[48];
    z[48]=n<T>(1,4)*z[80] + z[48];
    z[48]=z[4]*z[48];
    z[46]=z[11]*z[46];
    z[46]=z[74] + z[46];
    z[46]=z[4]*z[46];
    z[46]=z[46] + z[45] - z[115];
    z[50]=npow(z[11],2);
    z[46]=z[56]*z[50]*z[46];
    z[42]=z[46] + n<T>(1,4)*z[43] + z[48] + n<T>(1,2)*z[42];
    z[42]=z[15]*z[42];
    z[43]=n<T>(96737,3)*z[115] - 143911*z[45] - n<T>(317771,3)*z[24];
    z[46]=n<T>(324661,96)*z[115] - n<T>(11711,3)*z[45] + n<T>(36839,64)*z[24];
    z[46]=z[11]*z[46];
    z[48]=16697*z[28] - n<T>(36839,2)*z[75];
    z[46]=n<T>(1,32)*z[48] + z[46];
    z[46]=z[4]*z[46];
    z[43]=n<T>(1,64)*z[43] + z[46];
    z[46]=n<T>(217589,5)*z[45] + n<T>(107761,3)*z[24];
    z[46]=z[46]*z[101];
    z[48]= - 706303*z[45] - n<T>(759839,2)*z[24];
    z[48]=z[48]*z[112];
    z[48]= - n<T>(217589,2)*z[28] + z[48];
    z[48]=z[7]*z[2]*z[48];
    z[46]=z[46] + n<T>(1,5)*z[48];
    z[48]=n<T>(1,32)*z[10];
    z[46]=z[46]*z[48];
    z[53]=216211*z[45] + 107761*z[24];
    z[53]=z[2]*z[53];
    z[55]=z[11]*z[24];
    z[53]=n<T>(1,64)*z[53] - n<T>(6692,5)*z[55];
    z[53]=z[7]*z[53];
    z[43]=z[46] + n<T>(1,5)*z[43] + n<T>(1,3)*z[53];
    z[46]=n<T>(23411,2)*z[45] - n<T>(324661,9)*z[115];
    z[53]= - n<T>(638987,5)*z[45] + 107761*z[115];
    z[53]=z[11]*z[53];
    z[53]=n<T>(16697,5)*z[28] + n<T>(1,6)*z[53];
    z[53]=z[53]*z[102];
    z[46]=n<T>(1,5)*z[46] + z[53];
    z[46]=z[9]*z[11]*z[46];
    z[42]=n<T>(36839,120)*z[42] + n<T>(1,3)*z[43] + n<T>(1,32)*z[46];
    z[42]=z[15]*z[42];
    z[43]= - n<T>(103627,64)*z[45] + 3346*z[115];
    z[43]=z[4]*z[43];
    z[46]=2*z[45];
    z[53]=z[46] + z[24];
    z[53]=2*z[53] - z[115];
    z[53]=z[7]*z[53];
    z[43]=3346*z[53] - n<T>(103627,64)*z[1] + z[43];
    z[53]= - 161297*z[45] - n<T>(967093,10)*z[24];
    z[53]=z[2]*z[53];
    z[53]= - n<T>(645877,10)*z[28] + z[53];
    z[53]=z[7]*z[53];
    z[53]=n<T>(1,9)*z[53] + n<T>(107761,18)*z[45] + n<T>(23411,5)*z[24];
    z[48]=z[53]*z[48];
    z[53]= - n<T>(217589,64)*z[45] + 6692*z[115];
    z[53]=z[11]*z[53];
    z[53]=n<T>(689,32)*z[28] + n<T>(1,5)*z[53];
    z[53]=z[4]*z[53];
    z[53]=z[53] + n<T>(689,64)*z[45] - n<T>(3346,5)*z[115];
    z[53]=z[9]*z[53];
    z[42]=z[42] + n<T>(1,9)*z[53] + n<T>(1,45)*z[43] + z[48];
    z[42]=z[15]*z[42];
    z[21]=n<T>(1,81)*z[21];
    z[43]=z[2]*z[96];
    z[43]= - z[21] + n<T>(111,4)*z[43];
    z[43]=z[5]*z[43];
    z[48]= - 301*z[45] + n<T>(397,2)*z[24];
    z[43]=n<T>(11,18)*z[48] + z[43];
    z[43]=z[5]*z[43];
    z[43]=n<T>(3311,36)*z[1] + z[43];
    z[43]=z[43]*z[54];
    z[43]=z[43] - z[62];
    z[43]=z[11]*z[43];
    z[21]=z[2]*z[21];
    z[48]=z[19]*z[96];
    z[21]=z[21] - n<T>(111,4)*z[48];
    z[21]=z[5]*z[21];
    z[21]=n<T>(1,324)*z[25] + z[21];
    z[21]=z[5]*z[21];
    z[25]= - z[108] - z[30];
    z[21]=n<T>(1,36)*z[25] + z[21];
    z[21]=z[5]*z[21];
    z[25]= - n<T>(7376699,1620)*z[1] - 555*z[2];
    z[21]=n<T>(1,8)*z[25] + z[21];
    z[25]=n<T>(11076127,3240)*z[45] + z[40];
    z[25]=z[8]*z[25];
    z[29]= - z[6]*z[29];
    z[29]= - z[46] + z[29];
    z[29]=z[6]*z[29];
    z[21]=n<T>(28,3)*z[29] + n<T>(1,8)*z[25] + n<T>(1,2)*z[21] + z[43];
    z[21]=z[6]*z[21];
    z[25]=9*z[2];
    z[29]=z[25] - 5*z[11];
    z[29]=z[29]*z[49];
    z[25]= - z[25] + z[11];
    z[25]=z[11]*z[25];
    z[25]=7*z[19] + z[25];
    z[25]=z[25]*z[49];
    z[25]=z[2] + z[25];
    z[25]=z[3]*z[25];
    z[25]=z[25] + static_cast<T>(1)+ z[29];
    z[25]=z[25]*z[69];
    z[29]= - z[66]*z[58];
    z[30]=z[11] - z[2];
    z[40]= - z[11]*z[30];
    z[19]=z[29] - z[19] + z[40];
    z[19]=z[3]*z[19];
    z[19]=z[19] - z[52] + z[30];
    z[19]=z[3]*z[19];
    z[19]= - n<T>(1,2) + z[19];
    z[29]=z[6]*z[86];
    z[29]=z[45] + z[29];
    z[30]=npow(z[4],2);
    z[40]=n<T>(211,3)*z[30];
    z[29]=z[29]*z[40];
    z[19]=111*z[19] + z[29];
    z[19]=z[19]*z[85];
    z[29]=z[45]*npow(z[8],2);
    z[29]=n<T>(777,2) + n<T>(487,3)*z[29];
    z[29]=z[29]*z[49];
    z[19]=z[19] + z[29] + z[25];
    z[17]=z[30]*z[17];
    z[25]= - z[27] + n<T>(211,2)*z[17];
    z[27]= - z[7]*z[78];
    z[27]=z[74] + z[27];
    z[27]=z[87]*z[27];
    z[25]=n<T>(1,6)*z[25] + z[27];
    z[25]=z[7]*z[25]*z[4]*z[6];
    z[27]= - z[2] + z[35];
    z[27]=z[8]*z[27];
    z[27]= - n<T>(1,4) + z[27];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(3,4)*z[8] + z[27];
    z[27]=z[27]*z[69];
    z[29]=z[45]*npow(z[8],3);
    z[30]=npow(z[9],2)*z[36];
    z[29]= - n<T>(487,4)*z[29] + 7*z[30];
    z[30]=n<T>(1,3)*z[9];
    z[29]=z[29]*z[30];
    z[35]=z[12]*z[8]*npow(z[3],2);
    z[27]=n<T>(111,4)*z[35] + z[27] + z[29];
    z[27]=z[12]*z[27];
    z[19]=z[27] + n<T>(1,2)*z[19] + z[25];
    z[25]=z[71] - z[28];
    z[25]=z[25]*z[8];
    z[27]=n<T>(5,2)*z[45] - z[25];
    z[27]=z[8]*z[27];
    z[27]= - n<T>(1443,2)*z[1] + n<T>(487,3)*z[27];
    z[27]=z[8]*z[27];
    z[29]= - z[28] - z[71];
    z[29]=z[4]*z[29];
    z[29]=z[45] + z[29];
    z[29]=z[29]*z[40];
    z[27]=z[27] + z[29];
    z[17]=14*z[45] + n<T>(211,8)*z[17];
    z[17]=z[17]*z[102];
    z[29]=z[119]*z[90];
    z[28]= - z[4]*z[28];
    z[28]=z[28] + z[29];
    z[28]=z[28]*z[121];
    z[17]=z[17] + z[28];
    z[17]=z[7]*z[17];
    z[25]= - z[45] + z[25];
    z[25]=z[9]*z[25];
    z[25]= - n<T>(5,2)*z[36] + z[25];
    z[25]=z[9]*z[25];
    z[17]=n<T>(7,6)*z[25] + n<T>(1,8)*z[27] + z[17];
    z[17]=z[9]*z[17];
    z[17]=z[17] + n<T>(1,2)*z[19];
    z[17]=z[12]*z[17];
    z[19]= - n<T>(280759,2)*z[45] - 126893*z[24];
    z[19]=z[19]*z[110];
    z[25]=z[98]*z[44];
    z[19]=z[19] - 111*z[25];
    z[19]=z[5]*z[19];
    z[25]= - z[111] + n<T>(343421,9)*z[24];
    z[25]=z[2]*z[25];
    z[19]=n<T>(1,72)*z[25] + z[19];
    z[19]=z[5]*z[19];
    z[25]= - n<T>(3311,2)*z[45] + 11617*z[24];
    z[19]=n<T>(1,36)*z[25] + z[19];
    z[19]=z[5]*z[19];
    z[25]= - n<T>(280759,81)*z[32] - n<T>(111,2)*z[31];
    z[25]=z[5]*z[25];
    z[25]= - n<T>(7621,18)*z[24] + z[25];
    z[25]=z[5]*z[25];
    z[25]=z[34] + z[25];
    z[25]=z[25]*z[54];
    z[25]=static_cast<T>(111)+ z[25];
    z[25]=z[11]*z[25];
    z[27]=z[8]*z[71];
    z[27]= - z[65] + z[27];
    z[27]=z[8]*z[27];
    z[28]=n<T>(173,3)*z[1] - 777*z[2];
    z[19]=n<T>(487,12)*z[27] + z[25] + n<T>(1,4)*z[28] + z[19];
    z[19]=z[8]*z[19];
    z[25]= - z[76] - z[38];
    z[25]=z[5]*z[25];
    z[25]=z[25] + z[33];
    z[25]=z[5]*z[25];
    z[25]= - z[65] + z[25];
    z[25]=z[25]*z[37];
    z[25]= - n<T>(1,2) + z[25];
    z[19]=n<T>(111,2)*z[25] + z[19];
    z[25]=z[32]*z[10];
    z[25]=z[25] - z[24];
    z[27]=z[51] - z[45] - z[25];
    z[28]=z[45]*z[9];
    z[29]=n<T>(1,2)*z[41] + z[47] - z[28];
    z[29]=z[12]*z[29];
    z[27]=n<T>(1,2)*z[27] + z[29];
    z[27]=z[14]*z[27];
    z[24]=z[24]*z[10];
    z[28]= - z[41] + z[28] - z[1] + z[24];
    z[27]=n<T>(9404219,20)*z[28] + 720653*z[27];
    z[28]=n<T>(1,9)*z[14];
    z[27]=z[27]*z[28];
    z[29]=z[10]*z[1];
    z[31]=z[29] - z[39];
    z[27]=n<T>(361319,10)*z[31] + z[27];
    z[27]=z[27]*z[28];
    z[28]=z[50]*z[39];
    z[25]=z[28] - z[115] - z[25];
    z[25]=z[15]*z[25];
    z[28]=z[115]*z[30];
    z[24]=z[28] + n<T>(1,3)*z[1] - z[24];
    z[24]=n<T>(1,2)*z[24] + n<T>(1,3)*z[25];
    z[24]=z[15]*z[24];
    z[24]= - n<T>(1,6)*z[29] + z[24];
    z[24]=z[15]*z[24];
    z[24]=z[27] + n<T>(36839,10)*z[24];
    z[24]=z[13]*z[24];

    r += z[16] + z[17] + z[18] + n<T>(1,2)*z[19] + z[20] + z[21] + z[22] + 
      z[23] + n<T>(1,16)*z[24] + n<T>(1,48)*z[26] + z[42];
 
    return r;
}

template double qqb_2lLC_r282(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lLC_r282(const std::array<dd_real,31>&);
#endif
