#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r272(const std::array<T,31>& k) {
  T z[142];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[9];
    z[4]=k[11];
    z[5]=k[12];
    z[6]=k[15];
    z[7]=k[14];
    z[8]=k[2];
    z[9]=k[5];
    z[10]=k[6];
    z[11]=k[7];
    z[12]=k[13];
    z[13]=k[10];
    z[14]=k[4];
    z[15]=k[3];
    z[16]=z[5]*z[14];
    z[17]=z[4]*z[14];
    z[18]=z[16] - z[17];
    z[19]=z[12]*z[14];
    z[20]=z[14]*z[11];
    z[21]=z[19] - z[20];
    z[22]= - 4*z[21];
    z[23]=z[22] - 7*z[18];
    z[24]=13*z[4] - 14*z[12] - 24*z[11] + 25*z[5];
    z[24]=z[9]*z[24];
    z[23]=2*z[23] + z[24];
    z[24]=npow(z[10],4);
    z[23]=z[9]*z[24]*z[23];
    z[25]=n<T>(2,3)*z[3];
    z[26]=9*z[4];
    z[27]=z[26] + z[25];
    z[28]=npow(z[3],2);
    z[27]=z[27]*z[28];
    z[29]=npow(z[6],2);
    z[30]=z[29]*z[5];
    z[31]=9*z[30];
    z[23]=z[23] + z[31] + z[27];
    z[23]=z[9]*z[23];
    z[27]=9*z[5];
    z[32]=4*z[6];
    z[33]= - z[27] + z[32];
    z[33]=z[6]*z[33];
    z[34]=z[3]*z[8];
    z[35]= - static_cast<T>(7)- n<T>(8,3)*z[34];
    z[35]=z[3]*z[35];
    z[35]= - z[26] + z[35];
    z[35]=z[3]*z[35];
    z[23]=z[23] + z[33] + z[35];
    z[33]=2*z[9];
    z[23]=z[23]*z[33];
    z[35]=2*z[3];
    z[36]=3*z[4];
    z[37]=z[36] + z[35];
    z[37]=z[3]*z[37];
    z[38]=3*z[5];
    z[39]=z[38] - z[6];
    z[39]=z[6]*z[39];
    z[37]=z[39] + z[37];
    z[39]=z[28]*z[4];
    z[39]=z[39] + z[30];
    z[40]=4*z[39];
    z[41]= - z[9]*z[40];
    z[37]=3*z[37] + z[41];
    z[37]=z[9]*z[37];
    z[41]=2*z[6];
    z[42]=z[41]*z[8];
    z[43]=static_cast<T>(3)- z[42];
    z[43]=z[6]*z[43];
    z[44]=4*z[3];
    z[45]=z[44]*z[8];
    z[46]= - static_cast<T>(3)- z[45];
    z[46]=z[3]*z[46];
    z[37]=z[37] + z[46] + z[43] - z[38] - z[4];
    z[37]=z[9]*z[37];
    z[43]= - z[29] + z[28];
    z[46]=npow(z[8],2);
    z[43]=z[46]*z[43];
    z[37]=z[37] + z[43];
    z[43]=2*z[2];
    z[37]=z[37]*z[43];
    z[47]=npow(z[8],3);
    z[48]=z[47]*z[7];
    z[49]=z[46] - n<T>(2,3)*z[48];
    z[49]=z[49]*z[44];
    z[50]=z[46]*z[7];
    z[51]=2*z[50];
    z[52]= - z[8] + z[51];
    z[49]=3*z[52] + z[49];
    z[49]=z[49]*z[35];
    z[52]=z[7]*z[8];
    z[53]=static_cast<T>(17)- 28*z[52];
    z[49]=n<T>(1,3)*z[53] + z[49];
    z[49]=z[3]*z[49];
    z[53]=n<T>(10,3)*z[7];
    z[54]=4*z[4];
    z[55]= - static_cast<T>(1)+ z[42];
    z[55]=z[6]*z[55];
    z[23]=z[37] + z[23] + z[49] + z[55] - z[54] + z[5] + z[53];
    z[23]=z[23]*z[43];
    z[37]=z[4]*z[12];
    z[49]=z[5]*z[11];
    z[55]=z[37] - z[49];
    z[56]=npow(z[9],3);
    z[24]= - z[56]*z[24]*z[55];
    z[57]=7*z[4];
    z[58]= - z[57] + n<T>(1,3)*z[3];
    z[58]=z[58]*z[28];
    z[58]= - 7*z[30] + z[58];
    z[24]=2*z[58] + 65*z[24];
    z[24]=z[24]*z[33];
    z[58]= - n<T>(11,3)*z[8] + 9*z[50];
    z[58]=z[58]*z[44];
    z[59]=2*z[52];
    z[60]=static_cast<T>(1)- z[59];
    z[58]=n<T>(59,3)*z[60] + z[58];
    z[58]=z[3]*z[58];
    z[60]=n<T>(16,3)*z[7] + z[36];
    z[58]=2*z[60] + z[58];
    z[58]=z[3]*z[58];
    z[60]=z[6] - z[5];
    z[61]=z[6]*z[60];
    z[23]=z[23] + z[24] - 6*z[61] + z[58];
    z[23]=z[2]*z[23];
    z[24]=6*z[52];
    z[58]=static_cast<T>(1)- z[24];
    z[58]=z[58]*z[35];
    z[61]=2*z[7];
    z[62]=z[61] + z[4];
    z[58]=3*z[62] + z[58];
    z[58]=z[58]*z[28];
    z[58]=5*z[30] + z[58];
    z[23]=2*z[58] + z[23];
    z[23]=z[2]*z[23];
    z[58]=z[21] + 4*z[18];
    z[62]=npow(z[10],5);
    z[58]=z[62]*z[58];
    z[63]=z[11] - z[5];
    z[64]=5*z[12];
    z[65]=5*z[4];
    z[63]=7*z[63] - z[65] + z[64];
    z[63]=z[9]*z[62]*z[63];
    z[58]=2*z[58] + z[63];
    z[63]=npow(z[9],2);
    z[58]=z[58]*z[63];
    z[58]= - 6*z[39] + z[58];
    z[58]=z[9]*z[58];
    z[66]=3*z[6];
    z[67]=7*z[5];
    z[68]=z[67] - z[66];
    z[68]=z[6]*z[68];
    z[69]=z[57] + z[44];
    z[69]=z[3]*z[69];
    z[58]=z[58] + z[68] + z[69];
    z[58]=z[9]*z[58];
    z[68]=z[4] + z[5];
    z[69]=z[6]*z[8];
    z[70]=z[69] - 1;
    z[71]= - z[6]*z[70];
    z[72]= - static_cast<T>(1)- z[34];
    z[72]=z[3]*z[72];
    z[58]=z[58] + z[72] + z[71] - z[68];
    z[58]=z[58]*z[43];
    z[71]=npow(z[9],4);
    z[62]=z[71]*z[62]*z[55];
    z[72]=z[26] - z[25];
    z[72]=z[72]*z[28];
    z[31]=24*z[62] + z[31] + z[72];
    z[31]=z[31]*z[33];
    z[62]= - z[67] + z[32];
    z[62]=z[6]*z[62];
    z[72]=n<T>(2,3)*z[8];
    z[73]=z[72] - z[50];
    z[73]=z[73]*z[44];
    z[24]=z[73] - static_cast<T>(5)+ z[24];
    z[24]=z[24]*z[35];
    z[73]=n<T>(2,3)*z[7];
    z[74]= - z[73] - z[4];
    z[24]=7*z[74] + z[24];
    z[24]=z[3]*z[24];
    z[24]=z[58] + z[31] + z[62] + z[24];
    z[24]=z[24]*z[43];
    z[31]= - static_cast<T>(1)+ n<T>(19,3)*z[52];
    z[31]=z[31]*z[44];
    z[31]=z[31] - n<T>(41,3)*z[7] - 14*z[4];
    z[31]=z[31]*z[28];
    z[24]=z[24] - 14*z[30] + z[31];
    z[24]=z[2]*z[24];
    z[31]=npow(z[3],3);
    z[58]=z[7]*z[31];
    z[24]= - 8*z[58] + z[24];
    z[24]=z[2]*z[24];
    z[58]=static_cast<T>(1)- 4*z[52];
    z[58]=z[58]*z[25];
    z[58]=z[58] + z[61] + z[36];
    z[58]=z[58]*z[28];
    z[61]=npow(z[9],5);
    z[62]= - z[61]*z[55]*npow(z[10],6);
    z[40]= - z[40] + 7*z[62];
    z[40]=z[9]*z[40];
    z[62]=2*z[5];
    z[74]=z[62] - z[6];
    z[74]=z[6]*z[74];
    z[75]=2*z[4];
    z[76]=z[75] + z[3];
    z[76]=z[3]*z[76];
    z[40]=z[40] + z[74] + z[76];
    z[40]=z[2]*z[40];
    z[40]=z[40] + 3*z[30] + z[58];
    z[40]=z[2]*z[40];
    z[58]=n<T>(5,3)*z[7];
    z[74]=z[31]*z[58];
    z[40]=z[74] + z[40];
    z[40]=z[40]*npow(z[2],2);
    z[74]=npow(z[12],3);
    z[76]=z[7]*z[5];
    z[77]=z[74]*z[76];
    z[78]=z[13]*z[77];
    z[79]=npow(z[4],3);
    z[80]=z[79]*z[7];
    z[81]=z[13]*z[11];
    z[82]= - z[81]*z[80];
    z[78]=z[78] + z[82];
    z[82]= - z[31]*z[73];
    z[39]= - z[2]*z[39];
    z[39]=z[82] + z[39];
    z[82]=2*z[1];
    z[39]=z[39]*npow(z[2],3)*z[82];
    z[39]=z[39] + n<T>(1,3)*z[78] + 2*z[40];
    z[39]=z[39]*z[82];
    z[40]=2*z[11];
    z[78]= - z[40] - z[13];
    z[78]=z[7]*z[78];
    z[78]=z[81] + z[78];
    z[82]=n<T>(2,3)*z[4];
    z[78]=z[78]*z[82];
    z[83]=z[81]*z[7];
    z[30]= - n<T>(3,2)*z[30] + z[83] + z[78];
    z[78]=npow(z[4],2);
    z[30]=z[78]*z[30];
    z[83]=z[5]*z[13];
    z[74]=z[74]*z[83];
    z[84]=n<T>(2,3)*z[12];
    z[85]=z[13] + z[62];
    z[85]=z[85]*z[84];
    z[85]= - z[83] + z[85];
    z[86]=npow(z[12],2);
    z[85]=z[7]*z[85]*z[86];
    z[24]=z[39] + z[24] - n<T>(2,3)*z[74] + z[85] + z[30];
    z[24]=z[1]*z[24];
    z[30]=z[13]*z[14];
    z[39]=z[30] - 1;
    z[74]= - z[39]*z[84];
    z[85]=n<T>(1,3)*z[13];
    z[74]=z[74] - z[85] - z[62];
    z[74]=z[12]*z[74];
    z[74]=n<T>(5,3)*z[83] + z[74];
    z[74]=z[12]*z[74];
    z[74]=4*z[31] + z[74];
    z[74]=z[7]*z[74];
    z[87]=z[11]*z[15];
    z[88]=z[87] + 2;
    z[89]=z[88]*z[13];
    z[90]= - static_cast<T>(2)- z[30];
    z[90]=z[7]*z[90];
    z[90]=z[90] + z[11] + z[89];
    z[90]=z[90]*z[82];
    z[91]=z[85] + z[11];
    z[92]=z[7]*z[91];
    z[90]=z[90] - z[81] + z[92];
    z[90]=z[4]*z[90];
    z[92]= - z[81]*z[58];
    z[90]=z[92] + z[90];
    z[90]=z[4]*z[90];
    z[92]=n<T>(3,2)*z[5];
    z[93]=z[92] - z[7];
    z[93]=z[93]*z[78];
    z[94]= - n<T>(1,2) + z[16];
    z[94]=z[94]*z[36];
    z[95]=5*z[5];
    z[94]=z[95] + z[94];
    z[96]=z[6]*z[4];
    z[94]=z[94]*z[96];
    z[93]=3*z[93] + z[94];
    z[93]=z[6]*z[93];
    z[80]= - z[11]*z[80];
    z[77]=z[77] + z[80];
    z[80]=3*z[7];
    z[94]=z[80]*z[78];
    z[97]= - z[29]*z[94];
    z[77]=n<T>(1,3)*z[77] + z[97];
    z[97]=npow(z[6],3);
    z[98]=z[97]*z[9];
    z[99]=z[98]*z[94];
    z[77]=2*z[77] - z[99];
    z[77]=z[9]*z[77];
    z[100]=2*z[12];
    z[101]=z[5]*z[39]*z[100];
    z[101]=z[83] + z[101];
    z[101]=z[101]*z[86];
    z[23]=z[24] + z[23] + z[77] + z[93] + z[90] + n<T>(1,3)*z[101] + z[74];
    z[23]=z[1]*z[23];
    z[24]=17*z[5];
    z[74]=z[24] - z[40];
    z[77]= - z[54] - z[64] - z[74];
    z[77]=z[6]*z[77];
    z[90]=n<T>(8,3)*z[3];
    z[74]= - z[90] + z[74];
    z[74]=z[7]*z[74];
    z[93]=n<T>(3,2)*z[12];
    z[101]= - z[40] - z[93];
    z[101]=z[12]*z[101];
    z[74]=z[77] + z[101] + z[74];
    z[74]=z[3]*z[74];
    z[77]=7*z[11];
    z[101]= - z[57] - z[100] - z[77] + 16*z[5];
    z[101]=z[101]*z[29];
    z[102]=n<T>(3,2)*z[86];
    z[103]=z[102]*z[11];
    z[74]=z[74] - z[103] + z[101];
    z[74]=z[3]*z[74];
    z[101]=13*z[5];
    z[104]=z[101] - z[57];
    z[105]= - z[64] - z[11] + z[104];
    z[105]=z[105]*z[29];
    z[106]= - z[5] - z[12];
    z[106]=z[6]*z[106];
    z[106]=z[76] + z[106];
    z[106]=z[3]*z[106];
    z[103]=6*z[106] + z[103] + z[105];
    z[103]=z[3]*z[103];
    z[105]=3*z[11];
    z[106]=z[105] - z[4];
    z[106]=z[106]*z[97];
    z[103]=6*z[106] + z[103];
    z[103]=z[3]*z[103];
    z[106]=z[4] - z[11];
    z[107]=z[106]*z[97];
    z[108]=z[12] - z[5];
    z[109]=z[108]*z[29];
    z[110]= - z[3]*z[109];
    z[110]= - z[107] + z[110];
    z[110]=z[110]*z[28];
    z[78]=z[78]*z[7];
    z[111]=9*z[78];
    z[97]= - z[97]*z[111];
    z[97]=z[97] + 4*z[110];
    z[97]=z[9]*z[97];
    z[110]=n<T>(19,3)*z[7];
    z[112]=z[110] + z[26];
    z[112]=z[112]*z[96];
    z[113]=18*z[78];
    z[112]= - z[113] + z[112];
    z[112]=z[112]*z[29];
    z[97]=z[97] + z[112] + z[103];
    z[97]=z[9]*z[97];
    z[103]=z[80] + z[54];
    z[103]=z[4]*z[103];
    z[112]=10*z[11] - z[36];
    z[112]=z[6]*z[112];
    z[103]=z[103] + z[112];
    z[103]=z[6]*z[103];
    z[103]= - z[94] + z[103];
    z[66]=z[103]*z[66];
    z[102]= - z[49]*z[102];
    z[86]= - z[86]*z[76];
    z[79]= - z[79]*z[73];
    z[66]=z[97] + z[74] + z[66] + z[79] + z[102] + z[86];
    z[66]=z[9]*z[66];
    z[70]=z[70]*z[41];
    z[74]=10*z[4];
    z[79]=static_cast<T>(1)+ 10*z[34];
    z[79]=z[3]*z[79];
    z[70]=z[79] + z[70] + z[38] - z[74];
    z[79]=z[38] - z[12];
    z[79]=z[79]*z[29];
    z[86]=z[36] - z[11];
    z[97]=z[86]*z[28];
    z[79]=z[79] + z[97];
    z[79]=z[79]*z[33];
    z[97]=z[95] + z[12];
    z[102]= - 3*z[97] + z[32];
    z[102]=z[6]*z[102];
    z[103]=z[65] + z[11];
    z[112]= - 3*z[103] - n<T>(44,3)*z[3];
    z[112]=z[3]*z[112];
    z[79]=z[79] + z[112] + 4*z[55] + z[102];
    z[79]=z[9]*z[79];
    z[102]= - z[106]*z[28];
    z[102]=z[109] + z[102];
    z[102]=z[9]*z[102];
    z[109]=z[6]*z[97];
    z[112]=z[3]*z[103];
    z[102]=z[102] + z[112] + z[109] - z[55];
    z[102]=z[9]*z[102];
    z[109]=3*z[3];
    z[60]=z[4] + z[60];
    z[60]=2*z[60] - z[109];
    z[60]=2*z[60] + z[102];
    z[60]=z[9]*z[60];
    z[69]=z[34] + z[69] + z[18];
    z[60]=2*z[69] + z[60];
    z[60]=z[60]*z[43];
    z[60]=z[60] + 2*z[70] + z[79];
    z[60]=z[9]*z[60];
    z[69]=z[8]*z[13];
    z[70]=n<T>(1,3)*z[69];
    z[79]=z[70] - 1;
    z[102]=z[79]*z[46];
    z[112]=z[13] - z[7];
    z[112]=z[112]*npow(z[8],4);
    z[114]=z[112]*z[6];
    z[115]= - n<T>(1,3)*z[114] - z[102] + z[48];
    z[115]=z[115]*z[41];
    z[116]=n<T>(7,3)*z[50];
    z[79]= - z[8]*z[79];
    z[79]=z[115] + z[79] + z[116];
    z[79]=z[79]*z[41];
    z[115]= - static_cast<T>(3)- z[70];
    z[115]=z[115]*z[46];
    z[117]=z[112]*z[3];
    z[115]=n<T>(1,3)*z[117] + z[115] + z[48];
    z[115]=z[115]*z[35];
    z[118]=static_cast<T>(5)+ z[70];
    z[118]=z[8]*z[118];
    z[115]=z[115] + z[118] - z[116];
    z[115]=z[115]*z[35];
    z[116]=z[17] - n<T>(2,3) - z[16];
    z[118]=n<T>(4,3)*z[7];
    z[119]= - z[14] + 4*z[8];
    z[119]=z[119]*z[118];
    z[60]=z[115] + z[79] + z[119] + z[60] + 5*z[116];
    z[60]=z[60]*z[43];
    z[79]=3*z[16];
    z[115]=3*z[17];
    z[22]= - z[115] + z[79] - z[22];
    z[116]= - 25*z[4] + 23*z[12] + 65*z[11] - 59*z[5];
    z[116]=z[9]*z[116];
    z[22]=z[116] + 6*z[22];
    z[116]=npow(z[10],3);
    z[22]=z[116]*z[22];
    z[119]= - z[67] + z[12];
    z[119]=z[119]*z[29];
    z[120]=z[11] - z[57];
    z[120]=z[120]*z[28];
    z[22]=z[120] + z[119] + z[22];
    z[22]=z[22]*z[33];
    z[97]= - z[41] + z[97];
    z[97]=z[97]*z[41];
    z[119]=static_cast<T>(3)+ z[34];
    z[119]=z[119]*z[44];
    z[103]=z[119] + z[103];
    z[103]=z[103]*z[35];
    z[22]=z[22] + z[103] - 3*z[55] + z[97];
    z[22]=z[9]*z[22];
    z[47]=z[13]*z[47];
    z[47]= - 7*z[47] + z[48];
    z[97]=n<T>(4,3)*z[6];
    z[47]=z[47]*z[97];
    z[103]=static_cast<T>(4)- n<T>(17,3)*z[69];
    z[103]=z[8]*z[103];
    z[47]=z[47] + z[103] + n<T>(5,3)*z[50];
    z[47]=z[6]*z[47];
    z[103]=z[69] + 1;
    z[47]= - 2*z[103] + z[47];
    z[47]=z[6]*z[47];
    z[119]= - static_cast<T>(6)+ z[70];
    z[119]=z[119]*z[46];
    z[119]=z[119] + n<T>(17,3)*z[48];
    z[119]=z[119]*z[44];
    z[120]=n<T>(7,3)*z[69];
    z[121]=static_cast<T>(8)- z[120];
    z[121]=z[8]*z[121];
    z[119]=z[119] + z[121] - n<T>(113,3)*z[50];
    z[119]=z[3]*z[119];
    z[70]=z[70] + 2;
    z[121]=n<T>(32,3)*z[52] + z[70];
    z[119]=2*z[121] + z[119];
    z[119]=z[3]*z[119];
    z[22]=z[60] + z[22] + z[119] + z[47] + 21*z[4] - z[5] - n<T>(20,3)*z[7];
    z[22]=z[2]*z[22];
    z[47]=z[63]*z[116]*z[55];
    z[60]=z[95] - z[12];
    z[60]=z[60]*z[29];
    z[116]=z[11] + z[36];
    z[116]=z[116]*z[28];
    z[47]=43*z[47] + z[60] + z[116];
    z[47]=z[47]*z[33];
    z[60]=z[40] + z[12];
    z[116]=n<T>(1,3)*z[7];
    z[119]= - z[60]*z[116];
    z[91]= - z[5]*z[91];
    z[121]=n<T>(2,3)*z[13];
    z[122]= - z[4]*z[121];
    z[91]=z[122] + z[91] + z[119];
    z[119]=z[46]*z[13];
    z[122]=11*z[119];
    z[51]= - z[122] + z[51];
    z[51]=z[51]*z[97];
    z[97]=5*z[69];
    z[123]=z[97] - 4;
    z[124]=n<T>(11,3)*z[52];
    z[51]=z[51] + z[124] - z[123];
    z[51]=z[6]*z[51];
    z[51]=z[51] + n<T>(7,3)*z[7] - 2*z[13] + z[5];
    z[51]=z[6]*z[51];
    z[125]=73*z[52] - static_cast<T>(38)- z[97];
    z[126]=n<T>(20,3)*z[50];
    z[127]=static_cast<T>(1)+ n<T>(2,3)*z[69];
    z[127]=z[8]*z[127];
    z[127]=z[127] - z[126];
    z[127]=z[127]*z[44];
    z[125]=n<T>(1,3)*z[125] + z[127];
    z[125]=z[3]*z[125];
    z[127]=z[121] - z[11];
    z[128]=n<T>(13,3)*z[7];
    z[129]= - z[128] + z[127];
    z[125]=z[125] + 2*z[129] + z[36];
    z[125]=z[3]*z[125];
    z[22]=z[22] + z[47] + z[125] + 2*z[91] + z[51];
    z[22]=z[2]*z[22];
    z[47]=npow(z[14],2);
    z[51]=z[5]*z[47];
    z[51]=z[14] - n<T>(1,2)*z[51];
    z[51]=z[51]*z[36];
    z[51]=z[51] - static_cast<T>(1)- 5*z[16];
    z[51]=z[4]*z[51];
    z[91]=z[47]*z[81];
    z[125]=11*z[20];
    z[91]=z[125] + 3*z[91];
    z[91]=z[91]*z[41];
    z[129]=z[13]*z[20];
    z[130]=2*z[30];
    z[131]=n<T>(3,2) + z[130];
    z[131]=z[5]*z[131];
    z[51]=z[91] + z[51] + z[93] + z[131] - 11*z[11] - 5*z[129];
    z[51]=z[6]*z[51];
    z[83]=z[83] - z[81];
    z[91]=n<T>(3,2)*z[16];
    z[129]=static_cast<T>(2)- z[91];
    z[129]=z[129]*z[36];
    z[129]= - 10*z[5] + z[129];
    z[129]=z[4]*z[129];
    z[51]=z[51] - 6*z[83] + z[129];
    z[51]=z[6]*z[51];
    z[129]=z[13]*z[15];
    z[131]=z[69] - static_cast<T>(1)- 10*z[129];
    z[132]=npow(z[15],3);
    z[133]=z[132]*z[13];
    z[134]=3*z[12];
    z[135]= - z[134]*z[133];
    z[136]=npow(z[15],2);
    z[137]=z[136]*z[13];
    z[135]=n<T>(19,3)*z[137] + z[135];
    z[135]=z[12]*z[135];
    z[131]=n<T>(28,3)*z[52] + n<T>(1,3)*z[131] + z[135];
    z[131]=z[3]*z[131];
    z[135]= - z[15]*z[93];
    z[135]=z[135] + n<T>(7,2) - n<T>(5,3)*z[129];
    z[135]=z[12]*z[135];
    z[131]=z[131] - n<T>(5,2)*z[4] - z[110] + n<T>(5,3)*z[13] + z[135];
    z[131]=z[3]*z[131];
    z[127]= - 2*z[127] + z[93];
    z[127]=z[12]*z[127];
    z[135]=8*z[11];
    z[138]=z[135] - z[24];
    z[139]= - z[7]*z[138];
    z[138]= - z[4] - z[100] + z[138];
    z[138]=z[6]*z[138];
    z[127]=z[131] + z[138] + z[127] + z[139];
    z[127]=z[3]*z[127];
    z[131]= - static_cast<T>(1)+ n<T>(1,2)*z[87];
    z[131]=z[131]*z[38];
    z[138]=n<T>(4,3)*z[4];
    z[139]=z[7]*z[14];
    z[140]=static_cast<T>(1)- z[139];
    z[140]=z[140]*z[138];
    z[141]= - n<T>(1,3) + z[30];
    z[141]=z[7]*z[141];
    z[89]=z[140] + z[141] - z[89] + z[131];
    z[89]=z[4]*z[89];
    z[131]= - z[11] - z[13];
    z[131]=z[131]*z[118];
    z[140]=n<T>(7,2)*z[49];
    z[89]=z[89] + z[131] + z[81] - z[140];
    z[89]=z[4]*z[89];
    z[131]=z[11] - z[85];
    z[131]=z[131]*z[62];
    z[141]= - z[15] + z[14];
    z[141]=z[13]*z[141]*z[84];
    z[141]=z[141] + z[85] + n<T>(5,2)*z[5];
    z[141]=z[12]*z[141];
    z[131]=z[131] + z[141];
    z[131]=z[12]*z[131];
    z[121]=z[121] + z[5];
    z[39]=z[12]*z[39];
    z[39]=2*z[121] + z[39];
    z[39]=z[12]*z[39];
    z[39]=n<T>(14,3)*z[83] + z[39];
    z[39]=z[7]*z[39];
    z[22]=z[23] + z[22] + z[66] + z[127] + z[51] + z[89] + z[131] + 
    z[39];
    z[22]=z[1]*z[22];
    z[23]=n<T>(1,3)*z[46];
    z[39]=7*z[69];
    z[51]= - static_cast<T>(1)+ z[39];
    z[51]=z[51]*z[23];
    z[66]=3*z[48];
    z[51]=n<T>(4,3)*z[114] + z[51] - z[66];
    z[51]=z[6]*z[51];
    z[83]=n<T>(8,3)*z[50];
    z[89]= - static_cast<T>(1)+ n<T>(5,3)*z[69];
    z[89]=z[8]*z[89];
    z[51]=z[51] + z[89] - z[83];
    z[51]=z[6]*z[51];
    z[89]=z[103]*z[46];
    z[66]= - n<T>(4,3)*z[117] + n<T>(7,3)*z[89] - z[66];
    z[66]=z[3]*z[66];
    z[89]=z[8]*z[103];
    z[89]= - 5*z[89] + 8*z[50];
    z[66]=n<T>(1,3)*z[89] + z[66];
    z[66]=z[3]*z[66];
    z[89]=z[14] - 7*z[8];
    z[89]=z[89]*z[116];
    z[103]=z[115] - n<T>(1,3);
    z[51]=z[66] + z[51] + z[89] + z[69] + z[79] - z[103];
    z[32]= - z[108]*z[32];
    z[66]=z[3] + z[106];
    z[66]=z[66]*z[44];
    z[32]=z[66] - 37*z[55] + z[32];
    z[32]=z[9]*z[32];
    z[45]=static_cast<T>(1)- z[45];
    z[45]=z[45]*z[90];
    z[66]=z[11] - z[67];
    z[32]=z[32] + z[45] + 45*z[4] + 5*z[66] + z[12];
    z[32]=z[9]*z[32];
    z[45]=z[106]*z[3];
    z[66]=z[108]*z[6];
    z[45]=z[45] - z[66];
    z[66]=6*z[55] - z[45];
    z[79]=4*z[9];
    z[66]=z[66]*z[79];
    z[60]=z[66] + n<T>(4,3)*z[3] - 26*z[4] + 27*z[5] - z[60];
    z[60]=z[9]*z[60];
    z[34]= - n<T>(4,3)*z[34] + n<T>(2,3) - 11*z[18];
    z[34]=2*z[34] + z[60];
    z[34]=z[9]*z[34];
    z[45]= - 5*z[55] + z[45];
    z[45]=z[9]*z[45];
    z[60]=z[4] - z[5];
    z[45]=6*z[60] + z[45];
    z[45]=z[9]*z[45];
    z[45]=8*z[18] + z[45];
    z[45]=z[45]*z[63]*z[43];
    z[60]=z[3] - z[6];
    z[60]=z[46]*z[60];
    z[66]=2*z[8];
    z[60]=z[14] - z[66] + z[60];
    z[34]=z[45] + n<T>(4,3)*z[60] + z[34];
    z[34]=z[34]*z[43];
    z[32]=z[34] + 4*z[51] + z[32];
    z[32]=z[2]*z[32];
    z[34]=8*z[20];
    z[45]= - 8*z[19] + z[34];
    z[51]=npow(z[10],2);
    z[45]=z[51]*z[45];
    z[60]=2*z[14];
    z[79]= - z[51]*z[60];
    z[79]= - z[105] + z[79];
    z[79]=z[5]*z[79];
    z[89]=z[14]*z[51];
    z[89]=z[89] + z[12];
    z[89]=z[89]*z[75];
    z[45]=z[89] + z[79] + z[45];
    z[74]=z[74] + z[134] - 43*z[11] + 30*z[5];
    z[74]=z[33]*z[51]*z[74];
    z[79]= - z[44] + z[86];
    z[79]=z[3]*z[79];
    z[86]= - z[5] + z[134];
    z[86]=z[6]*z[86];
    z[45]=z[74] + z[79] + 4*z[45] + z[86];
    z[45]=z[9]*z[45];
    z[74]= - static_cast<T>(32)+ 73*z[69];
    z[74]=z[8]*z[74];
    z[74]=z[74] - 13*z[50];
    z[79]= - static_cast<T>(1)+ z[120];
    z[46]=z[79]*z[46];
    z[46]=z[46] - n<T>(1,3)*z[48];
    z[46]=z[6]*z[46];
    z[46]=n<T>(1,3)*z[74] + 8*z[46];
    z[46]=z[6]*z[46];
    z[46]=z[46] - z[52] - n<T>(10,3) + 13*z[69];
    z[46]=z[6]*z[46];
    z[74]=z[122] + 49*z[50];
    z[79]= - z[102] - n<T>(5,3)*z[48];
    z[79]=z[3]*z[79];
    z[74]=n<T>(1,3)*z[74] + 8*z[79];
    z[74]=z[3]*z[74];
    z[70]=z[74] - n<T>(35,3)*z[52] - z[70];
    z[70]=z[3]*z[70];
    z[74]=static_cast<T>(2)+ z[20];
    z[74]=z[74]*z[118];
    z[79]= - static_cast<T>(11)+ z[30];
    z[79]=z[79]*z[138];
    z[86]=z[11] + 8*z[5];
    z[32]=z[32] + z[45] + z[70] + z[46] + z[79] + z[74] + n<T>(1,3)*z[86] + 
    z[12];
    z[32]=z[2]*z[32];
    z[45]=z[47]*z[13];
    z[46]=static_cast<T>(5)- 12*z[20];
    z[46]=z[46]*z[45];
    z[70]=static_cast<T>(3)- z[125];
    z[70]=z[14]*z[70];
    z[46]= - z[83] + n<T>(44,3)*z[119] + 4*z[70] + z[46];
    z[46]=z[6]*z[46];
    z[70]=z[20] - 1;
    z[74]= - z[70]*z[130];
    z[79]=n<T>(3,2)*z[4];
    z[86]= - z[47]*z[79];
    z[86]=z[14] + z[86];
    z[86]=z[4]*z[86];
    z[46]=z[46] + z[86] - z[124] + z[97] + z[74] + static_cast<T>(2)- z[125];
    z[46]=z[6]*z[46];
    z[74]= - n<T>(3,2)*z[19] + z[91] - z[103];
    z[74]=z[4]*z[74];
    z[86]=2*z[20];
    z[89]=z[86] + 3;
    z[90]= - z[13]*z[89];
    z[46]=z[46] + z[74] + z[73] - z[93] - z[92] - 5*z[11] + z[90];
    z[46]=z[6]*z[46];
    z[74]=z[108]*z[41];
    z[90]=z[5]*z[80];
    z[74]=z[90] + z[74];
    z[74]=z[74]*z[28];
    z[74]=2*z[107] + z[74];
    z[44]=z[74]*z[44];
    z[74]=n<T>(38,3)*z[7] + z[26];
    z[74]=z[74]*z[96];
    z[74]= - z[113] + z[74];
    z[74]=z[74]*z[29];
    z[90]= - z[98]*z[111];
    z[44]=z[90] + z[74] + z[44];
    z[44]=z[9]*z[44];
    z[74]=z[100] + z[80];
    z[74]=z[74]*z[109];
    z[24]= - z[24] + 13*z[12];
    z[24]=z[6]*z[24];
    z[24]=z[74] + 34*z[76] + z[24];
    z[24]=z[3]*z[24];
    z[74]= - z[77] + 11*z[4];
    z[74]=z[74]*z[29];
    z[24]=z[74] + z[24];
    z[24]=z[3]*z[24];
    z[74]=13*z[49] - 10*z[37];
    z[74]=z[51]*z[74];
    z[90]= - 18*z[11] - z[58];
    z[91]=9*z[17];
    z[98]= - n<T>(2,3) - z[91];
    z[98]=z[4]*z[98];
    z[90]=2*z[90] + z[98];
    z[90]=z[6]*z[90];
    z[98]=n<T>(49,3)*z[7] + z[26];
    z[98]=z[4]*z[98];
    z[90]=z[98] + z[90];
    z[90]=z[6]*z[90];
    z[90]= - z[111] + z[90];
    z[90]=z[6]*z[90];
    z[24]=z[44] + z[24] + 2*z[74] + z[90];
    z[24]=z[9]*z[24];
    z[44]=z[79] + 34*z[5] - z[93];
    z[44]=z[7]*z[44];
    z[74]=9*z[12] - z[105] - z[104];
    z[74]=z[6]*z[74];
    z[53]=z[12] + z[53];
    z[79]=static_cast<T>(1)- n<T>(16,3)*z[52];
    z[79]=z[3]*z[79];
    z[53]=5*z[53] + z[79];
    z[53]=z[3]*z[53];
    z[79]=z[11]*z[93];
    z[44]=z[53] + z[74] + z[79] + z[44];
    z[44]=z[3]*z[44];
    z[53]= - static_cast<T>(1)- z[17];
    z[53]=z[82]*z[53];
    z[53]=z[53] - n<T>(2,3)*z[11] - z[93];
    z[53]=z[7]*z[53];
    z[51]=2*z[51];
    z[74]= - z[19]*z[51];
    z[53]=z[74] + z[53];
    z[53]=z[4]*z[53];
    z[74]=z[91] + static_cast<T>(7)- 30*z[20];
    z[74]=z[74]*z[41];
    z[79]=n<T>(11,3) - 6*z[17];
    z[79]=z[4]*z[79];
    z[74]=z[74] + z[79] - 16*z[11] - z[80];
    z[74]=z[6]*z[74];
    z[79]= - z[7]*z[36];
    z[74]=z[79] + z[74];
    z[74]=z[6]*z[74];
    z[51]= - z[15]*z[51];
    z[51]=z[51] + z[93];
    z[51]=z[49]*z[51];
    z[79]=z[12]*z[76];
    z[24]=z[24] + z[44] + z[74] + z[53] + z[79] + z[51];
    z[24]=z[9]*z[24];
    z[44]=static_cast<T>(1)- z[87];
    z[44]=z[44]*z[92];
    z[51]= - static_cast<T>(13)- z[130];
    z[51]=z[51]*z[116];
    z[53]=static_cast<T>(3)+ z[87];
    z[53]=z[13]*z[53];
    z[74]= - n<T>(7,6) + z[139];
    z[74]=z[4]*z[74];
    z[44]=z[74] + z[51] + z[44] - n<T>(5,2)*z[11] + z[53];
    z[44]=z[4]*z[44];
    z[51]=17*z[136] - 38*z[133];
    z[53]=npow(z[15],4);
    z[74]=z[53]*z[13];
    z[79]= - z[132] + z[74];
    z[82]=6*z[12];
    z[79]=z[79]*z[82];
    z[51]=n<T>(1,3)*z[51] + z[79];
    z[51]=z[12]*z[51];
    z[79]= - 16*z[69] + static_cast<T>(13)- 2*z[129];
    z[79]=z[8]*z[79];
    z[51]=z[126] + z[51] + n<T>(1,3)*z[79] + z[15] + n<T>(20,3)*z[137];
    z[51]=z[3]*z[51];
    z[79]=19*z[69] + static_cast<T>(19)- z[129];
    z[90]= - n<T>(3,2)*z[136] + 2*z[133];
    z[90]=z[90]*z[134];
    z[92]= - z[15] - z[137];
    z[90]=n<T>(17,3)*z[92] + z[90];
    z[90]=z[12]*z[90];
    z[51]=z[51] - 9*z[52] + n<T>(1,3)*z[79] + z[90];
    z[51]=z[3]*z[51];
    z[79]= - static_cast<T>(7)+ 17*z[129];
    z[79]=z[12]*z[79];
    z[79]=16*z[7] - 17*z[13] + z[79];
    z[51]=z[51] + n<T>(1,3)*z[79] + z[4];
    z[51]=z[3]*z[51];
    z[79]=static_cast<T>(1)+ z[30];
    z[79]=z[79]*z[62];
    z[79]=13*z[13] + z[79];
    z[90]=z[14]*z[15];
    z[90]=z[136] + z[90];
    z[90]=z[13]*z[90];
    z[90]=z[90] - z[15] - z[14];
    z[90]=z[90]*z[84];
    z[92]= - n<T>(5,3)*z[15] - z[14];
    z[92]=z[13]*z[92];
    z[90]=z[90] + n<T>(1,3) + z[92];
    z[90]=z[12]*z[90];
    z[79]=n<T>(1,3)*z[79] + z[90];
    z[79]=z[12]*z[79];
    z[90]=z[20]*z[85];
    z[92]=n<T>(1,3)*z[5];
    z[93]=static_cast<T>(17)+ z[30];
    z[93]=z[93]*z[92];
    z[90]=z[93] + z[11] + z[90];
    z[93]=n<T>(1,3)*z[12];
    z[98]=static_cast<T>(5)- z[130];
    z[98]=z[98]*z[93];
    z[90]=2*z[90] + z[98];
    z[90]=z[7]*z[90];
    z[22]=z[22] + z[32] + z[24] + z[51] + z[46] + z[44] + z[90] + z[79]
    - n<T>(2,3)*z[81] + z[140];
    z[22]=z[1]*z[22];
    z[24]= - z[123]*z[23];
    z[24]= - n<T>(2,3)*z[114] + z[24] + z[48];
    z[24]=z[6]*z[24];
    z[32]=4*z[50];
    z[44]=z[39] - 4;
    z[46]= - z[8]*z[44];
    z[46]=z[46] + z[32];
    z[24]=n<T>(1,3)*z[46] + z[24];
    z[24]=z[24]*z[41];
    z[41]=static_cast<T>(2)- z[97];
    z[23]=z[41]*z[23];
    z[25]=z[112]*z[25];
    z[23]=z[25] + z[23] + z[48];
    z[23]=z[3]*z[23];
    z[25]= - static_cast<T>(2)+ z[39];
    z[25]=z[8]*z[25];
    z[25]=z[25] - z[32];
    z[23]=n<T>(1,3)*z[25] + z[23];
    z[23]=z[23]*z[35];
    z[25]=z[19] - static_cast<T>(8)- z[20];
    z[32]=8*z[14];
    z[39]= - z[32] - z[45];
    z[39]=z[39]*z[92];
    z[41]= - z[93] - n<T>(1,3)*z[11];
    z[41]=z[47]*z[41];
    z[41]=z[66] + z[41];
    z[41]=z[7]*z[41];
    z[32]=z[32] - z[45];
    z[46]=n<T>(1,3)*z[4];
    z[32]=z[32]*z[46];
    z[23]=z[23] + z[24] + z[32] + z[41] - n<T>(22,3)*z[69] + z[39] + z[130]
    - n<T>(1,3)*z[25];
    z[24]= - z[91] + 9*z[16] + z[21];
    z[25]=z[55]*z[9];
    z[27]=z[77] - z[27];
    z[27]= - 56*z[25] + z[4] + 6*z[27] + 17*z[12];
    z[27]=z[9]*z[27];
    z[24]=2*z[24] + z[27];
    z[24]=z[9]*z[24];
    z[27]=26*z[25] + z[75] - 11*z[12] - 19*z[11] + 28*z[5];
    z[27]=z[27]*z[33];
    z[21]=z[27] - z[21] - 37*z[18];
    z[21]=z[21]*z[63];
    z[27]=z[40] - z[38];
    z[27]= - 9*z[25] - z[4] + 3*z[27] + 4*z[12];
    z[27]=z[9]*z[27];
    z[18]=12*z[18] + z[27];
    z[18]=z[18]*z[56]*z[43];
    z[18]=z[21] + z[18];
    z[18]=z[2]*z[18];
    z[18]=z[24] + z[18];
    z[18]=z[18]*z[43];
    z[21]= - 14*z[11] + z[101];
    z[24]= - static_cast<T>(1)- z[42];
    z[24]=z[6]*z[24];
    z[21]=z[24] - z[57] + 2*z[21] - z[12];
    z[21]=n<T>(4,3)*z[21] + 51*z[25];
    z[21]=z[9]*z[21];
    z[18]=z[18] + 2*z[23] + z[21];
    z[18]=z[2]*z[18];
    z[21]=11*z[30] - z[44];
    z[23]=n<T>(4,3)*z[8];
    z[21]=z[21]*z[23];
    z[24]=5*z[14];
    z[21]=z[21] - z[24] - 9*z[45];
    z[21]=z[8]*z[21];
    z[25]= - static_cast<T>(6)+ z[125];
    z[25]=z[25]*z[47];
    z[27]=n<T>(4,3)*z[48];
    z[32]= - static_cast<T>(5)+ 6*z[20];
    z[32]=z[13]*z[32]*npow(z[14],3);
    z[21]=z[27] + z[21] + 2*z[25] + z[32];
    z[21]=z[6]*z[21];
    z[25]=z[70]*z[45];
    z[32]= - static_cast<T>(17)+ 22*z[20];
    z[32]=z[14]*z[32];
    z[38]=static_cast<T>(9)- 4*z[69];
    z[38]=z[8]*z[38];
    z[21]=z[21] + z[83] + z[38] + z[32] + 7*z[25];
    z[21]=z[6]*z[21];
    z[25]=z[89]*z[30];
    z[21]=z[21] - n<T>(4,3)*z[17] - z[59] - 9*z[69] + z[25] + n<T>(5,3) + z[34];
    z[21]=z[6]*z[21];
    z[25]=z[110] + z[36];
    z[25]=z[25]*z[96];
    z[25]= - 6*z[78] + z[25];
    z[25]=z[25]*z[29];
    z[29]=z[31]*z[76];
    z[25]= - z[99] + z[25] + 6*z[29];
    z[25]=z[9]*z[25];
    z[29]=z[40] - z[58];
    z[31]= - n<T>(31,3) - z[115];
    z[31]=z[4]*z[31];
    z[29]=2*z[29] + z[31];
    z[29]=z[6]*z[29];
    z[31]=n<T>(22,3)*z[7] + z[36];
    z[31]=z[4]*z[31];
    z[29]=z[31] + z[29];
    z[29]=z[6]*z[29];
    z[29]= - z[94] + z[29];
    z[29]=z[6]*z[29];
    z[31]=z[80] - z[5] - z[100];
    z[31]=z[31]*z[35];
    z[32]=17*z[76];
    z[31]=z[32] + z[31];
    z[28]=z[31]*z[28];
    z[25]=z[25] + z[29] + z[28];
    z[25]=z[9]*z[25];
    z[28]=z[47]*z[36];
    z[28]=n<T>(1,3)*z[14] + z[28];
    z[28]=z[4]*z[28];
    z[29]=n<T>(5,3) + 9*z[20];
    z[28]=z[28] + 2*z[29] + n<T>(1,3)*z[52];
    z[28]=z[6]*z[28];
    z[28]=z[28] - z[65] + z[135] - z[7];
    z[28]=z[6]*z[28];
    z[29]= - z[4]*z[128];
    z[28]=z[29] + z[28];
    z[28]=z[6]*z[28];
    z[29]=z[67] + z[100];
    z[31]=z[12]*z[15];
    z[31]= - static_cast<T>(1)+ z[31];
    z[31]=2*z[31] + z[52];
    z[31]=z[31]*z[109];
    z[31]=z[31] + 11*z[7] - z[29];
    z[31]=z[3]*z[31];
    z[31]=z[32] + z[31];
    z[31]=z[3]*z[31];
    z[25]=z[25] + z[28] + z[31];
    z[25]=z[9]*z[25];
    z[28]= - static_cast<T>(7)+ 15*z[20];
    z[28]=z[28]*z[60];
    z[26]= - z[47]*z[26];
    z[31]=3*z[8];
    z[26]=z[26] + z[83] + z[28] - z[31];
    z[26]=z[6]*z[26];
    z[26]=z[26] - n<T>(17,3)*z[17] + n<T>(2,3)*z[52] - n<T>(35,3) + 23*z[20];
    z[26]=z[6]*z[26];
    z[28]=z[46] + z[105] + n<T>(8,3)*z[7];
    z[26]=2*z[28] + z[26];
    z[26]=z[6]*z[26];
    z[28]= - z[132]*z[134];
    z[28]= - n<T>(8,3)*z[136] + z[28];
    z[28]=z[12]*z[28];
    z[28]= - z[83] + z[28] - z[31] + 2*z[15];
    z[28]=z[3]*z[28];
    z[31]= - z[136]*z[82];
    z[31]= - z[15] + z[31];
    z[31]=z[12]*z[31];
    z[28]=z[28] + n<T>(14,3)*z[52] - static_cast<T>(7)+ z[31];
    z[28]=z[3]*z[28];
    z[31]=n<T>(14,3)*z[7];
    z[32]= - z[15]*z[134];
    z[32]= - static_cast<T>(7)+ z[32];
    z[32]=z[12]*z[32];
    z[28]=z[28] + z[31] - 4*z[5] + z[32];
    z[28]=z[3]*z[28];
    z[32]=z[49] + n<T>(2,3)*z[76];
    z[31]= - z[64] - z[31];
    z[31]=z[4]*z[31];
    z[25]=z[25] + z[28] + z[26] + 7*z[32] + z[31];
    z[25]=z[9]*z[25];
    z[26]=z[136]*z[85];
    z[28]=z[129] + 1;
    z[31]=2*z[28] + z[69];
    z[23]=z[31]*z[23];
    z[23]=z[23] + z[15] + z[26];
    z[23]=z[8]*z[23];
    z[26]= - 17*z[132] + 19*z[74];
    z[31]= - z[13]*npow(z[15],5);
    z[31]=2*z[53] + z[31];
    z[31]=z[31]*z[134];
    z[26]=n<T>(1,3)*z[26] + z[31];
    z[26]=z[12]*z[26];
    z[31]= - z[136] - 5*z[133];
    z[23]=z[27] + z[26] + n<T>(2,3)*z[31] + z[23];
    z[23]=z[3]*z[23];
    z[26]= - 13*z[136] + 22*z[133];
    z[27]=2*z[132] - z[74];
    z[27]=z[27]*z[82];
    z[26]=n<T>(1,3)*z[26] + z[27];
    z[26]=z[12]*z[26];
    z[27]= - 7*z[28] - 2*z[69];
    z[27]=z[27]*z[72];
    z[23]=z[23] - z[83] + z[26] + z[27] - 3*z[15] - n<T>(4,3)*z[137];
    z[23]=z[3]*z[23];
    z[26]= - z[97] - static_cast<T>(7)+ 11*z[129];
    z[27]=2*z[136];
    z[28]=z[27] - z[133];
    z[28]=z[28]*z[134];
    z[28]=z[28] + 10*z[15] - n<T>(13,3)*z[137];
    z[28]=z[12]*z[28];
    z[23]=z[23] + z[59] + n<T>(2,3)*z[26] + z[28];
    z[23]=z[3]*z[23];
    z[26]= - static_cast<T>(4)+ z[20];
    z[26]=z[26]*z[30];
    z[28]= - z[60] - z[45];
    z[28]=z[5]*z[28];
    z[30]=z[12]*z[45];
    z[26]=z[30] + z[28] - static_cast<T>(2)+ z[26];
    z[26]=z[26]*z[73];
    z[28]=z[20] + static_cast<T>(5)- n<T>(2,3)*z[87];
    z[28]=z[13]*z[28];
    z[30]= - 11*z[15] - z[24];
    z[30]=z[13]*z[30];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[30]*z[84];
    z[31]=z[14] - n<T>(1,3)*z[45];
    z[31]=z[7]*z[31];
    z[31]=n<T>(2,3) + z[31];
    z[31]=z[31]*z[75];
    z[18]=z[22] + z[18] + z[25] + z[23] + z[21] + z[31] + z[26] + z[30]
    - z[62] + n<T>(22,3)*z[11] + z[28];
    z[18]=z[1]*z[18];
    z[21]=z[5]*z[10];
    z[22]= - z[88]*z[21];
    z[23]=z[49]*z[10];
    z[25]=z[37]*z[10];
    z[26]=5*z[23] - 4*z[25];
    z[26]=z[9]*z[26];
    z[17]=z[17]*z[10];
    z[28]= - z[17] - 7*z[10];
    z[28]=z[12]*z[28];
    z[30]=z[11]*z[10];
    z[22]=z[26] + 5*z[30] + z[22] + z[28];
    z[22]=z[22]*z[33];
    z[26]=z[10]*z[15];
    z[24]=z[10]*z[24];
    z[24]=z[24] - static_cast<T>(7)+ 4*z[26];
    z[24]=z[24]*z[100];
    z[20]=z[20]*z[10];
    z[28]=static_cast<T>(13)- 2*z[26];
    z[28]=z[11]*z[28];
    z[22]=z[22] + z[24] - z[95] + z[28] - 10*z[20];
    z[22]=z[9]*z[22];
    z[16]=z[16]*z[10];
    z[16]=z[16] - z[17];
    z[17]=z[19]*z[10];
    z[17]=z[17] - z[20];
    z[19]= - z[16] - 2*z[17];
    z[20]=z[23] - z[25];
    z[20]=z[20]*z[9];
    z[21]=z[20] + z[30] - z[21];
    z[23]=z[36] - z[134];
    z[23]=z[10]*z[23];
    z[21]=z[23] - 4*z[21];
    z[21]=z[9]*z[21];
    z[19]=2*z[19] + z[21];
    z[19]=z[19]*z[71];
    z[20]=z[20] + z[30];
    z[21]=z[12] - z[68];
    z[21]=z[10]*z[21];
    z[21]=z[21] + z[20];
    z[21]=z[9]*z[21];
    z[21]=z[21] + z[16] + z[17];
    z[21]=z[2]*z[21]*z[61];
    z[19]=z[19] + z[21];
    z[19]=z[19]*z[43];
    z[21]= - z[57] + z[64] - 11*z[5];
    z[21]=z[10]*z[21];
    z[21]=z[21] + 13*z[20];
    z[21]=z[9]*z[21];
    z[21]=z[21] + 13*z[17] + 3*z[16];
    z[21]=z[21]*z[56];
    z[19]=z[21] + z[19];
    z[19]=z[2]*z[19];
    z[21]=z[54] + z[29];
    z[21]=z[10]*z[21];
    z[20]=z[21] - 11*z[20];
    z[20]=z[9]*z[20];
    z[16]=z[20] - z[16] - 11*z[17];
    z[16]=z[16]*z[63];
    z[16]=z[16] + z[19];
    z[16]=z[16]*z[43];
    z[17]=z[26] - 2;
    z[19]=z[17]*z[86];
    z[17]= - z[17]*z[60];
    z[20]= - z[10]*z[27];
    z[17]=z[17] + 5*z[15] + z[20];
    z[17]=z[12]*z[17];
    z[16]=z[16] + z[22] + z[17] - z[87] + z[19];

    r += 2*z[16] + z[18];
 
    return r;
}

template double qqb_2lLC_r272(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lLC_r272(const std::array<dd_real,31>&);
#endif
