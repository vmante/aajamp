#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r168(const std::array<T,31>& k) {
  T z[62];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[5];
    z[7]=k[7];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=k[2];
    z[11]=k[3];
    z[12]=k[11];
    z[13]=k[15];
    z[14]=k[14];
    z[15]=k[19];
    z[16]=29*z[7];
    z[17]=17*z[4];
    z[18]= - z[16] - z[17];
    z[18]=z[2]*z[18];
    z[19]=npow(z[2],2);
    z[20]=z[19]*z[7];
    z[21]=npow(z[2],3);
    z[22]=z[21]*z[7];
    z[23]=z[14]*z[22];
    z[23]=2*z[20] + z[23];
    z[24]=8*z[14];
    z[23]=z[23]*z[24];
    z[18]=z[23] - static_cast<T>(32)+ z[18];
    z[18]=z[14]*z[18];
    z[23]=npow(z[14],2);
    z[25]=8*z[23];
    z[26]=z[20]*z[25];
    z[16]=z[26] + z[17] - z[16];
    z[26]=z[11]*z[14];
    z[16]=z[16]*z[26];
    z[16]=z[16] + z[17] + z[18];
    z[16]=z[11]*z[16];
    z[18]= - z[4]*z[21];
    z[18]= - z[19] + z[18];
    z[18]=z[14]*z[18];
    z[27]=z[4]*z[19];
    z[18]=z[27] + z[18];
    z[27]=z[14]*z[2];
    z[27]= - z[26] - static_cast<T>(3)+ z[27];
    z[27]=z[11]*z[27];
    z[18]=2*z[18] + z[27];
    z[18]=z[5]*z[18];
    z[27]=z[4]*z[2];
    z[18]=z[18] + static_cast<T>(4)- z[27];
    z[28]=z[19]*z[17];
    z[29]=z[14]*npow(z[2],4);
    z[30]=z[7]*z[29];
    z[30]=z[22] + z[30];
    z[31]=4*z[14];
    z[30]=z[30]*z[31];
    z[28]=z[30] + z[28] - 29*z[20];
    z[30]=2*z[14];
    z[28]=z[28]*z[30];
    z[32]= - z[14]*z[21];
    z[32]= - 2*z[19] + z[32];
    z[32]=z[32]*z[24];
    z[33]=37*z[2];
    z[32]=z[33] + z[32];
    z[32]=z[14]*z[32];
    z[25]= - z[19]*z[25];
    z[25]=static_cast<T>(37)+ z[25];
    z[25]=z[25]*z[26];
    z[25]=z[25] - static_cast<T>(6)+ z[32];
    z[25]=z[11]*z[25];
    z[29]= - z[21] - z[29];
    z[29]=z[29]*z[31];
    z[29]=29*z[19] + z[29];
    z[29]=z[14]*z[29];
    z[29]=z[33] + z[29];
    z[25]=2*z[29] + z[25];
    z[25]=z[12]*z[25];
    z[16]=z[25] + z[16] + z[28] + 17*z[18];
    z[16]=z[9]*z[16];
    z[18]=z[4]*npow(z[6],5);
    z[25]=npow(z[6],4);
    z[18]=z[18] + 5*z[25];
    z[18]=z[18]*z[4];
    z[28]=npow(z[6],3);
    z[18]=z[18] + 13*z[28];
    z[29]=23*z[4];
    z[18]=z[18]*z[29];
    z[31]= - 30*z[2] + 53*z[6];
    z[32]=2*z[2];
    z[33]=z[31]*z[32];
    z[34]=npow(z[6],2);
    z[18]=z[18] - z[33] + 207*z[34];
    z[18]=z[18]*z[5];
    z[33]=z[25]*z[4];
    z[35]=z[33] + 5*z[28];
    z[35]=z[35]*z[4];
    z[36]=9*z[34];
    z[35]=z[35] + z[36];
    z[35]=z[35]*z[29];
    z[37]=3*z[2];
    z[18]=z[18] + z[35] - z[37] + 115*z[6];
    z[35]=z[32] + z[6];
    z[38]=z[35]*npow(z[14],3);
    z[38]=8*z[38];
    z[39]= - z[21]*z[38];
    z[40]=3*z[34];
    z[41]=z[28]*z[4];
    z[42]=z[40] + z[41];
    z[42]=z[42]*z[4];
    z[43]=3*z[6];
    z[44]=z[42] + z[43];
    z[45]=z[44]*z[4];
    z[45]=z[45] + 1;
    z[45]=23*z[45];
    z[46]=z[11]*z[45];
    z[39]=z[46] + z[39] + z[18];
    z[39]=z[12]*z[39];
    z[23]=4*z[23];
    z[46]=z[23]*npow(z[2],5);
    z[47]=33*z[21] - z[46];
    z[47]=z[47]*z[30];
    z[48]=43*z[2];
    z[49]= - z[11]*z[48];
    z[47]=z[49] - 43*z[19] + z[47];
    z[47]=z[12]*z[47];
    z[46]=z[7]*z[46];
    z[46]= - 33*z[22] + z[46];
    z[46]=z[46]*z[30];
    z[49]=z[11]*z[7];
    z[48]=z[49]*z[48];
    z[46]=z[47] + z[48] + 43*z[20] + z[46];
    z[46]=z[9]*z[46];
    z[18]= - z[7]*z[18];
    z[22]=z[22]*z[38];
    z[45]= - z[49]*z[45];
    z[18]=z[46] + z[39] + z[45] + z[22] + z[18];
    z[18]=z[15]*z[18];
    z[22]=163*z[26] + 135;
    z[22]=z[6]*z[22];
    z[39]=z[34]*z[14];
    z[22]= - 17*z[39] + z[22];
    z[22]=z[12]*z[22];
    z[45]=z[14]*z[6];
    z[46]= - 59*z[6] - 97*z[2];
    z[46]=z[7]*z[46];
    z[46]=static_cast<T>(52)+ z[46];
    z[22]=z[22] + 14*z[49] + 2*z[46] - 163*z[45];
    z[22]=z[11]*z[22];
    z[46]=z[2] - z[6];
    z[47]=z[39] + z[46];
    z[48]= - z[35]*z[49];
    z[50]=z[19]*z[9];
    z[51]= - static_cast<T>(2)- z[49];
    z[51]=z[51]*z[50];
    z[48]=z[51] + z[48] - z[47];
    z[48]=z[11]*z[48];
    z[51]=npow(z[11],2);
    z[52]=z[12]*z[51]*z[39];
    z[48]=z[52] + z[48];
    z[52]=59*z[13];
    z[48]=z[48]*z[52];
    z[53]=z[49] + 1;
    z[54]=7*z[2];
    z[53]=z[54]*z[53];
    z[53]= - 38*z[20] + z[53];
    z[53]=z[11]*z[53];
    z[53]= - 38*z[19] + z[53];
    z[55]=2*z[9];
    z[53]=z[53]*z[55];
    z[22]=z[48] + z[53] + 76*z[47] + z[22];
    z[22]=z[13]*z[22];
    z[48]= - 45*z[6] - 38*z[2];
    z[48]=z[7]*z[48];
    z[53]=45*z[7] - 59*z[14];
    z[53]=z[11]*z[53];
    z[48]=z[53] + 45*z[45] - static_cast<T>(7)+ z[48];
    z[37]=z[37] + 2*z[39];
    z[45]=149*z[26] + static_cast<T>(270)- 79*z[45];
    z[45]=z[11]*z[45];
    z[37]=45*z[37] + z[45];
    z[37]=z[12]*z[37];
    z[45]= - z[7]*z[54];
    z[45]= - static_cast<T>(38)+ z[45];
    z[45]=2*z[45] - 45*z[49];
    z[45]=z[11]*z[45];
    z[20]= - 59*z[20] + z[45];
    z[20]=z[9]*z[20];
    z[20]=z[22] + z[20] + 2*z[48] + z[37];
    z[20]=z[13]*z[20];
    z[22]=z[2]*z[6];
    z[37]=z[22] - z[34];
    z[45]=z[37]*z[4];
    z[48]=2*z[6];
    z[53]= - z[45] + z[48] - z[2];
    z[53]=z[53]*z[29];
    z[53]=static_cast<T>(86)+ z[53];
    z[53]=z[4]*z[53];
    z[42]= - 4*z[6] - z[42];
    z[42]=z[4]*z[42];
    z[42]= - static_cast<T>(2)+ z[42];
    z[42]=z[7]*z[42];
    z[42]=z[53] + 23*z[42];
    z[42]=z[5]*z[42];
    z[53]=z[34] + z[41];
    z[53]=z[7]*z[53];
    z[45]=z[45] + z[53];
    z[45]=z[6] + 2*z[45];
    z[45]=z[3]*z[45];
    z[45]= - static_cast<T>(2)+ z[45];
    z[53]=npow(z[3],2);
    z[45]=z[45]*z[53];
    z[54]=z[34]*z[4];
    z[48]=z[48] + z[54];
    z[48]=z[48]*z[4];
    z[56]=static_cast<T>(2)+ z[48];
    z[56]=z[7]*z[4]*z[56];
    z[57]=z[46]*z[4];
    z[58]= - static_cast<T>(1)+ z[57];
    z[58]=z[58]*npow(z[4],2);
    z[56]=z[58] + z[56];
    z[58]=z[9]*z[17];
    z[56]=23*z[56] + z[58];
    z[56]=z[5]*z[56];
    z[54]=z[54]*z[7];
    z[57]=z[54] + static_cast<T>(1)+ z[57];
    z[58]=npow(z[3],3);
    z[59]=14*z[58];
    z[57]=z[57]*z[59];
    z[56]=z[57] + z[56];
    z[56]=z[1]*z[56];
    z[48]=z[48] + 1;
    z[57]=z[48]*z[7];
    z[60]= - z[4]*z[57];
    z[61]=z[6]*npow(z[4],3);
    z[60]=z[61] + z[60];
    z[27]=static_cast<T>(3)- z[27];
    z[27]=z[5]*z[27];
    z[27]=z[4] + z[27];
    z[27]=z[9]*z[27];
    z[27]=z[56] + 14*z[45] + 17*z[27] + 46*z[60] + z[42];
    z[27]=z[1]*z[27];
    z[42]= - z[50] + z[47];
    z[42]=z[13]*z[42];
    z[45]=z[6]*z[30];
    z[42]=z[42] - static_cast<T>(1)+ z[45];
    z[42]=z[42]*z[52];
    z[42]=z[42] + 90*z[14] - 31*z[9];
    z[42]=z[13]*z[42];
    z[45]=z[19]*z[12];
    z[47]=z[2] + z[6];
    z[50]= - z[45] - z[47];
    z[50]=z[3]*z[50];
    z[52]=z[12]*z[32];
    z[52]=static_cast<T>(1)+ z[52];
    z[50]=2*z[52] + z[50];
    z[50]=z[3]*z[50];
    z[50]= - z[12] + z[50];
    z[52]=14*z[3];
    z[50]=z[50]*z[52];
    z[56]=z[12]*z[35];
    z[56]=static_cast<T>(1)+ z[56];
    z[56]=z[3]*z[56];
    z[56]= - 2*z[12] + z[56];
    z[53]=z[56]*z[53];
    z[56]= - z[10]*z[12]*z[58];
    z[53]=z[53] + z[56];
    z[53]=z[10]*z[53];
    z[56]=z[9]*z[12];
    z[58]= - z[1]*z[59];
    z[42]=14*z[53] + z[58] + z[50] - 80*z[56] + z[42];
    z[42]=z[10]*z[42];
    z[50]= - z[6] + 2*z[11];
    z[50]=z[5]*z[50];
    z[53]=z[14] - z[9];
    z[53]=z[10]*z[53];
    z[50]=z[50] + z[53];
    z[53]=z[26] + 1;
    z[53]=z[4]*z[53];
    z[53]=z[30] + z[53];
    z[53]=z[11]*z[53];
    z[51]= - z[5]*z[51];
    z[51]=z[11] + z[51];
    z[51]=z[51]*z[55];
    z[56]=z[5]*z[11];
    z[56]= - static_cast<T>(1)+ z[56];
    z[55]=z[56]*z[55];
    z[56]= - z[1]*z[9];
    z[56]=z[56] - 2;
    z[56]=z[5]*z[56];
    z[55]=z[55] + z[4] + z[56];
    z[55]=z[1]*z[55];
    z[39]=z[6] + z[39];
    z[39]=z[4]*z[39];
    z[39]=z[55] + z[51] + z[53] + z[39] + 2*z[50];
    z[39]=z[8]*z[39];
    z[50]=z[29]*z[57];
    z[51]=3*z[22] + 2*z[34];
    z[51]=z[51]*z[7];
    z[53]=z[6] - z[51];
    z[53]=z[14]*z[53];
    z[35]=z[7]*z[35];
    z[35]= - static_cast<T>(1)+ z[35];
    z[35]=2*z[35] + z[53];
    z[35]=z[35]*z[24];
    z[35]=z[35] - z[17] + 58*z[7];
    z[35]=z[14]*z[35];
    z[38]=z[49]*z[38];
    z[35]=z[38] + z[50] + z[35];
    z[35]=z[11]*z[35];
    z[38]=z[34]*z[2];
    z[38]=z[38] - z[28];
    z[49]=z[38]*z[4];
    z[50]=z[6]*z[32];
    z[50]=z[49] - z[40] + z[50];
    z[50]=z[50]*z[29];
    z[53]= - 15*z[6] + 16*z[2];
    z[50]=5*z[53] + z[50];
    z[50]=z[4]*z[50];
    z[53]=4*z[28];
    z[55]=z[53] + z[33];
    z[55]=z[4]*z[55];
    z[36]=z[36] + z[55];
    z[36]=z[36]*z[29];
    z[31]=2*z[31] + z[36];
    z[31]=z[7]*z[31];
    z[36]=z[43] + 4*z[2];
    z[55]= - z[2]*z[36];
    z[55]= - z[34] + z[55];
    z[55]=z[4]*z[55];
    z[47]= - 3*z[47] + z[55];
    z[47]=z[14]*z[47];
    z[31]=51*z[26] + 17*z[47] + z[31] + static_cast<T>(68)+ z[50];
    z[31]=z[5]*z[31];
    z[33]=z[33] - z[38];
    z[33]=z[7]*z[33];
    z[38]= - z[5]*z[34];
    z[38]= - z[6] + z[38];
    z[38]=z[38]*z[45];
    z[33]=z[38] + z[33] - z[22] + z[49];
    z[33]=z[3]*z[33];
    z[37]= - z[5]*z[37];
    z[38]=z[32] - z[6];
    z[37]= - z[38] + z[37];
    z[37]=z[12]*z[2]*z[37];
    z[45]= - z[5]*z[22];
    z[32]=z[33] + z[37] - z[32] + z[45];
    z[32]=z[3]*z[32];
    z[33]=z[46]*z[5];
    z[37]= - z[2]*z[33];
    z[37]=z[37] + z[38];
    z[37]=z[12]*z[37];
    z[32]=z[32] + z[37] - z[33] + static_cast<T>(1)- z[54];
    z[32]=z[32]*z[52];
    z[33]= - z[4]*z[48];
    z[37]=4*z[34] + z[41];
    z[37]=z[4]*z[37];
    z[37]=5*z[6] + z[37];
    z[37]=z[4]*z[37];
    z[37]=static_cast<T>(2)+ z[37];
    z[37]=z[7]*z[37];
    z[33]=z[33] + z[37];
    z[19]=z[34] + z[19];
    z[19]=z[2]*z[19];
    z[19]=z[28] + z[19];
    z[19]=z[7]*z[19];
    z[19]= - z[34] + z[19];
    z[19]=z[14]*z[19];
    z[19]=z[19] + z[43] - z[51];
    z[19]=z[19]*z[24];
    z[17]=z[36]*z[17];
    z[24]= - 7*z[6] + 15*z[2];
    z[24]=z[7]*z[24];
    z[17]=z[19] + 3*z[24] + static_cast<T>(16)+ z[17];
    z[17]=z[14]*z[17];
    z[19]= - z[2]*z[28];
    z[19]=z[25] + z[19];
    z[19]=z[4]*z[19];
    z[24]= - z[2]*z[40];
    z[19]=z[19] + z[53] + z[24];
    z[19]=z[4]*z[19];
    z[19]=z[19] + 7*z[34] - 4*z[22];
    z[19]=z[5]*z[19];
    z[19]=z[19] + z[44];
    z[19]=z[29]*z[19];
    z[21]= - z[21]*z[23];
    z[21]= - 41*z[46] + z[21];
    z[21]=z[21]*z[30];
    z[19]= - 46*z[26] + z[21] + static_cast<T>(163)+ z[19];
    z[19]=z[12]*z[19];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + z[27] + z[31] + z[32]
       + 23*z[33] + z[35] + 17*z[39] + z[42];
 
    return r;
}

template double qqb_2lLC_r168(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r168(const std::array<dd_real,31>&);
#endif
