#include "qqb_2lLC_rf_decl.hpp"

template<class T>
T qqb_2lLC_r40(const std::array<T,31>& k) {
  T z[68];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[8];
    z[8]=k[12];
    z[9]=k[5];
    z[10]=k[7];
    z[11]=k[2];
    z[12]=k[15];
    z[13]=k[14];
    z[14]=k[11];
    z[15]=npow(z[7],2);
    z[16]=z[15]*z[5];
    z[17]=z[15]*z[8];
    z[18]= - z[16] + z[17];
    z[19]=4*z[9];
    z[18]=z[18]*z[19];
    z[20]=z[5]*z[7];
    z[21]=4*z[14];
    z[22]=z[21] + 19*z[7];
    z[22]=z[8]*z[22];
    z[18]=z[18] - 5*z[20] + z[22];
    z[22]=3*z[9];
    z[18]=z[18]*z[22];
    z[23]=2*z[5];
    z[24]=z[23] + z[14];
    z[25]=2*z[7];
    z[26]=z[25] + z[24];
    z[26]=3*z[26] + 10*z[8];
    z[18]=2*z[26] + z[18];
    z[18]=z[18]*z[22];
    z[26]=3*z[4];
    z[27]=npow(z[11],2);
    z[28]=z[27]*z[7];
    z[29]=23*z[11] + 12*z[28];
    z[29]=z[29]*z[26];
    z[30]=z[2]*z[4];
    z[31]=z[30]*z[10];
    z[32]=9*z[14];
    z[33]=25*z[4] + 26*z[10] - z[32];
    z[33]=2*z[33] - z[31];
    z[33]=z[2]*z[33];
    z[34]=3*z[30];
    z[35]=z[34] + 10;
    z[36]=3*z[2];
    z[37]=z[36]*z[8];
    z[38]=z[35]*z[37];
    z[39]=2*z[12];
    z[40]=z[2]*z[10];
    z[40]=static_cast<T>(41)+ 28*z[40];
    z[40]=z[2]*z[40];
    z[40]=31*z[11] + z[40];
    z[40]=z[40]*z[39];
    z[41]=z[7]*z[11];
    z[42]=static_cast<T>(31)- 9*z[41];
    z[18]=z[40] + z[18] + z[38] + z[33] + 4*z[42] + z[29];
    z[18]=z[12]*z[18];
    z[29]= - z[16] + 5*z[17];
    z[33]=2*z[9];
    z[29]=z[29]*z[33];
    z[38]=8*z[15];
    z[40]= - z[10] - 10*z[7];
    z[40]=z[5]*z[40];
    z[42]=4*z[5];
    z[43]=z[42] + 11*z[7] + 2*z[14];
    z[43]=z[8]*z[43];
    z[29]=z[29] + 2*z[43] + z[38] + z[40];
    z[40]=9*z[9];
    z[29]=z[29]*z[40];
    z[43]=27*z[4];
    z[44]=static_cast<T>(2)+ z[41];
    z[44]=z[44]*z[43];
    z[45]=z[5]*z[4];
    z[46]=7*z[4];
    z[47]= - z[10]*z[46];
    z[47]=z[47] - 9*z[45];
    z[47]=z[2]*z[47];
    z[48]=3*z[8];
    z[49]=static_cast<T>(28)+ 15*z[30];
    z[49]=z[49]*z[48];
    z[50]=9*z[7];
    z[51]= - 8*z[10] + 17*z[14];
    z[18]=z[18] + z[29] + z[49] + z[47] + 63*z[5] + z[44] + 2*z[51] + 
    z[50];
    z[18]=z[12]*z[18];
    z[29]= - z[16] + 3*z[17];
    z[19]=z[29]*z[19];
    z[29]=4*z[15];
    z[44]=3*z[14];
    z[47]=z[44] + 20*z[7];
    z[47]=z[8]*z[47];
    z[19]=z[19] + z[47] + z[29] + z[20];
    z[19]=z[19]*z[40];
    z[40]=6*z[8];
    z[35]=z[35]*z[40];
    z[47]=41*z[11];
    z[49]=z[47] + 18*z[28];
    z[49]=z[4]*z[49];
    z[51]=37*z[4] + 14*z[31];
    z[51]=z[2]*z[51];
    z[49]=z[49] + z[51];
    z[49]=z[49]*z[39];
    z[51]=9*z[5];
    z[52]=z[14] + 4*z[7];
    z[53]=27*z[41];
    z[54]=static_cast<T>(38)+ z[53];
    z[54]=z[4]*z[54];
    z[19]=z[49] + z[19] + z[35] - 7*z[31] + z[51] + 9*z[52] + z[54];
    z[19]=z[12]*z[19];
    z[31]=6*z[5];
    z[35]=z[31] + z[26] + z[14] + 12*z[7];
    z[35]=z[8]*z[35];
    z[49]= - z[16] + 10*z[17];
    z[33]=z[49]*z[33];
    z[49]=z[4] - z[10] - z[25];
    z[49]=z[5]*z[49];
    z[33]=z[33] + z[35] + z[38] + z[49];
    z[19]=9*z[33] + z[19];
    z[19]=z[12]*z[19];
    z[33]=z[4]*z[14];
    z[35]=2*z[45] + 10*z[15] - z[33];
    z[35]=z[8]*z[35];
    z[38]=2*z[4];
    z[49]=z[38] + z[14] + 7*z[7];
    z[49]=z[8]*z[49];
    z[52]=z[9]*z[17];
    z[29]=12*z[52] + z[29] + z[49];
    z[29]=z[12]*z[29];
    z[29]=z[35] + z[29];
    z[29]=z[12]*z[29];
    z[35]=z[30]*z[5];
    z[49]=3*z[5];
    z[52]=z[35] - z[49];
    z[54]=19*z[13];
    z[55]=z[52]*z[54];
    z[56]=89*z[45];
    z[57]=3*z[10];
    z[58]=z[57]*z[14];
    z[55]=z[55] - z[58] + z[56];
    z[59]=npow(z[13],2);
    z[55]=z[55]*z[59];
    z[60]= - z[45]*z[32];
    z[61]=z[30] - 3;
    z[62]= - z[61]*z[54];
    z[62]=z[62] - 89*z[4] + 216*z[5];
    z[62]=z[13]*z[62];
    z[63]=153*z[45];
    z[62]= - z[63] + z[62];
    z[62]=z[13]*z[62];
    z[60]=z[60] + z[62];
    z[60]=z[8]*z[60];
    z[62]=npow(z[13],3);
    z[64]=z[62]*z[8];
    z[65]=z[6]*z[64];
    z[62]=z[62] + z[65];
    z[62]= - 19*z[62];
    z[62]=z[45]*z[62];
    z[65]=z[4] - z[49];
    z[65]=z[65]*z[54];
    z[56]=z[56] + z[65];
    z[65]=z[59]*z[8];
    z[56]=z[56]*z[65];
    z[17]=npow(z[12],2)*z[17];
    z[17]=36*z[17] + z[56] + z[62];
    z[17]=z[6]*z[17];
    z[56]=z[5]*z[64]*z[9];
    z[17]=z[17] + 9*z[29] - 57*z[56] + z[55] + z[60];
    z[17]=z[6]*z[17];
    z[29]=z[23]*z[14];
    z[55]=z[15]*z[29];
    z[60]=z[14]*z[10];
    z[62]= - z[5]*z[54];
    z[62]= - z[60] + z[62];
    z[62]=z[62]*z[59];
    z[64]=55*z[5] + z[54];
    z[64]=z[64]*z[65];
    z[55]=z[64] + z[55] + z[62];
    z[55]=3*z[55] - 19*z[56];
    z[55]=z[9]*z[55];
    z[56]=z[54]*z[2];
    z[52]= - z[52]*z[56];
    z[62]=72*z[5] - z[10] + z[14];
    z[52]=z[52] + 3*z[62] - 89*z[35];
    z[52]=z[13]*z[52];
    z[62]=z[60] - 17*z[45];
    z[52]=9*z[62] + z[52];
    z[52]=z[13]*z[52];
    z[61]=z[61]*z[56];
    z[61]=z[61] - static_cast<T>(216)+ 89*z[30];
    z[61]=z[13]*z[61];
    z[62]=311*z[5];
    z[61]=z[61] + 153*z[4] - z[62];
    z[61]=z[13]*z[61];
    z[64]=z[32] + 74*z[4];
    z[64]=z[5]*z[64];
    z[61]=z[61] + 54*z[15] + z[64];
    z[61]=z[8]*z[61];
    z[64]=z[14]*z[25];
    z[64]=z[60] + z[64];
    z[64]=z[64]*z[49];
    z[66]=z[15]*z[14];
    z[64]=2*z[66] + z[64];
    z[17]=z[17] + z[19] + z[55] + z[61] + 3*z[64] + z[52];
    z[17]=z[6]*z[17];
    z[19]=6*z[14];
    z[16]=z[16]*z[19];
    z[52]=z[23] + z[13];
    z[52]=z[52]*z[65];
    z[16]=z[16] + 19*z[52];
    z[16]=z[9]*z[16];
    z[52]=2*z[15];
    z[55]=2*z[10];
    z[61]= - z[55] + z[14];
    z[52]=z[61]*z[52];
    z[60]= - z[5]*z[60];
    z[52]=z[52] + z[60];
    z[60]=z[13]*z[5];
    z[58]= - z[58] + 133*z[60];
    z[58]=z[13]*z[58];
    z[15]=11*z[15] - z[29];
    z[29]= - static_cast<T>(55)- z[56];
    z[29]=z[13]*z[29];
    z[29]= - 62*z[5] + z[29];
    z[29]=z[13]*z[29];
    z[15]=2*z[15] + z[29];
    z[15]=z[15]*z[48];
    z[15]=z[16] + z[15] + 3*z[52] + z[58];
    z[15]=z[9]*z[15];
    z[16]=z[2]*z[14];
    z[29]=z[16]*npow(z[4],2);
    z[29]=16*z[29];
    z[52]= - z[29] + 20*z[33] + z[63];
    z[52]=z[2]*z[52];
    z[44]=z[44] - z[55];
    z[44]=3*z[44];
    z[58]=57*z[35] - z[57] - 152*z[5];
    z[61]=z[13]*z[2];
    z[58]=z[58]*z[61];
    z[52]=z[58] + z[52] - z[44] - z[62];
    z[52]=z[13]*z[52];
    z[58]=z[14]*z[11];
    z[62]=z[58] - 7;
    z[63]= - z[62]*z[25];
    z[44]= - z[44] + z[63];
    z[63]=3*z[7];
    z[44]=z[44]*z[63];
    z[64]=z[7]*z[1];
    z[65]=static_cast<T>(3)+ z[64];
    z[65]=z[65]*z[63];
    z[66]=z[4]*z[1];
    z[66]=static_cast<T>(42)- 5*z[66];
    z[66]=z[5]*z[66];
    z[46]=z[66] + z[65] - z[46];
    z[65]=npow(z[2],2);
    z[66]=z[65]*z[13];
    z[67]=static_cast<T>(181)- 32*z[30];
    z[67]=z[2]*z[67];
    z[67]=z[67] + 38*z[66];
    z[67]=z[13]*z[67];
    z[30]=z[67] + static_cast<T>(277)- 87*z[30];
    z[30]=z[13]*z[30];
    z[30]=4*z[46] + z[30];
    z[30]=z[8]*z[30];
    z[46]=z[11]*z[33];
    z[46]= - 8*z[46] - z[57] + 16*z[14];
    z[38]=z[46]*z[38];
    z[46]= - z[50] + 40*z[4];
    z[46]=z[46]*z[23];
    z[15]=z[17] + z[18] + z[15] + z[30] + z[52] - z[29] + z[46] + z[44]
    + z[38];
    z[15]=z[6]*z[15];
    z[17]=5*z[7] - z[3] + z[24];
    z[17]=z[17]*z[40];
    z[18]=z[3] - z[25];
    z[18]=z[5]*z[18];
    z[17]=z[17] + 12*z[18] - 5*z[60];
    z[17]=z[17]*z[22];
    z[18]=20*z[14];
    z[24]= - z[18] + 7*z[5];
    z[24]=z[24]*z[36];
    z[24]=z[24] + static_cast<T>(32)- 63*z[41];
    z[24]=z[13]*z[24];
    z[29]=z[2]*z[3];
    z[30]=static_cast<T>(5)- 7*z[29];
    z[30]=z[30]*z[48];
    z[38]=5*z[14];
    z[41]=z[5]*z[29];
    z[44]=18*z[3];
    z[17]=z[17] + z[30] + z[24] - 33*z[41] + 69*z[5] + 36*z[7] + z[38]
    + z[44] + 31*z[10];
    z[17]=z[9]*z[17];
    z[24]=z[63] - z[3];
    z[30]= - z[24]*z[31];
    z[24]=z[14] + z[24];
    z[24]=z[24]*z[40];
    z[24]=z[24] + z[30] - z[60];
    z[22]=z[24]*z[22];
    z[24]= - 9*z[41] + z[19] + 13*z[5];
    z[30]= - 10*z[14] + z[49];
    z[30]=z[2]*z[30];
    z[30]=static_cast<T>(7)+ 2*z[30];
    z[30]=z[13]*z[30];
    z[31]=z[8]*z[29];
    z[22]=z[22] - 9*z[31] + 3*z[24] + z[30];
    z[22]=z[9]*z[22];
    z[24]=12*z[14];
    z[30]=12*z[41] - 18*z[5] - z[24] - z[44] + 59*z[10];
    z[30]=z[2]*z[30];
    z[21]=z[21] - z[49];
    z[21]=z[2]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[2]*z[21];
    z[28]=36*z[28];
    z[31]=5*z[11];
    z[21]=z[21] - z[31] - z[28];
    z[21]=z[13]*z[21];
    z[41]=14*z[10] - z[32];
    z[41]=z[2]*z[41];
    z[41]=static_cast<T>(13)+ z[41];
    z[41]=z[2]*z[41];
    z[16]=static_cast<T>(1)- z[16];
    z[16]=z[2]*z[16];
    z[16]= - z[11] + z[16];
    z[16]=z[9]*z[13]*z[16];
    z[16]=5*z[16] + z[31] + z[41];
    z[16]=z[16]*z[39];
    z[39]=static_cast<T>(1)- z[29];
    z[39]=z[39]*z[37];
    z[16]=z[16] + z[22] + z[39] + z[21] + static_cast<T>(29)+ z[30];
    z[16]=z[9]*z[16];
    z[21]=18*z[14];
    z[22]=z[51] - z[10] - z[21];
    z[22]=z[2]*z[22];
    z[22]=static_cast<T>(20)+ z[22];
    z[22]=z[2]*z[22];
    z[30]=z[65]*z[40];
    z[16]=z[30] + z[22] + 13*z[11] + z[28] + z[16];
    z[16]=z[12]*z[16];
    z[22]=z[43] + z[44];
    z[22]=z[11]*z[22];
    z[26]=2*z[3] + z[26];
    z[26]=z[26]*z[36];
    z[26]=z[26] - 27;
    z[26]=z[5]*z[26];
    z[26]=43*z[4] + 44*z[14] - z[44] + z[10] + z[26];
    z[26]=z[2]*z[26];
    z[19]=z[19] - z[5];
    z[19]=z[2]*z[19];
    z[19]= - static_cast<T>(5)+ 6*z[19];
    z[19]=z[2]*z[19];
    z[19]=z[47] + z[19];
    z[19]=z[13]*z[19];
    z[28]=static_cast<T>(19)+ z[34];
    z[28]=z[28]*z[37];
    z[16]=z[16] + z[17] + z[28] + z[19] + z[26] + z[53] + static_cast<T>(29)+ z[22];
    z[16]=z[12]*z[16];
    z[17]= - z[20]*z[21];
    z[19]=z[57] - z[54];
    z[19]=z[19]*z[59];
    z[20]= - static_cast<T>(2)- z[61];
    z[20]=z[20]*z[54];
    z[22]= - z[3] - z[5];
    z[20]=28*z[22] + z[20];
    z[20]=z[13]*z[20];
    z[21]= - z[5]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[8]*z[20];
    z[17]=z[20] + z[17] + z[19];
    z[17]=z[9]*z[17];
    z[19]=14*z[3];
    z[20]= - z[19] - z[57];
    z[21]= - z[10]*z[36];
    z[21]=z[56] + static_cast<T>(32)+ z[21];
    z[21]=z[13]*z[21];
    z[20]=z[21] - 134*z[5] + 2*z[20] - 27*z[7];
    z[20]=z[13]*z[20];
    z[21]=z[65]*z[54];
    z[21]=92*z[2] + z[21];
    z[21]=z[13]*z[21];
    z[21]=z[21] + static_cast<T>(121)+ 32*z[29];
    z[21]=z[13]*z[21];
    z[19]=z[21] + 82*z[5] + 54*z[7] + z[24] + z[19] - 9*z[10];
    z[19]=z[8]*z[19];
    z[21]=z[1]*z[55];
    z[21]=z[21] - z[62];
    z[21]=z[21]*z[25];
    z[21]= - z[32] + z[21];
    z[21]=z[21]*z[63];
    z[22]= - 18*z[7] + 32*z[3] - z[57];
    z[22]=z[5]*z[22];
    z[17]=z[17] + z[19] + z[20] + z[21] + z[22];
    z[17]=z[9]*z[17];
    z[19]=17*z[1];
    z[20]= - z[19] + z[31];
    z[20]=z[3]*z[20];
    z[21]=z[14]*z[27];
    z[21]= - z[11] + z[21];
    z[21]=z[4]*z[21];
    z[20]=16*z[21] - 28*z[58] + static_cast<T>(69)+ z[20];
    z[20]=z[4]*z[20];
    z[21]=5*z[3];
    z[22]=z[35]*z[21];
    z[21]= - z[21] + 22*z[14];
    z[21]=z[4]*z[21];
    z[19]=z[3]*z[19];
    z[19]= - static_cast<T>(65)+ z[19];
    z[19]=z[4]*z[19];
    z[19]= - 31*z[3] + z[19];
    z[19]=z[5]*z[19];
    z[19]=z[22] + z[21] + z[19];
    z[19]=z[2]*z[19];
    z[21]=4*z[3];
    z[22]=z[21] - z[38];
    z[24]=22*z[33] - 69*z[45];
    z[24]=z[2]*z[24];
    z[22]=z[24] + 175*z[5] + 8*z[22] - 19*z[4];
    z[22]=z[2]*z[22];
    z[21]=z[11]*z[21];
    z[21]= - 13*z[61] + z[22] + static_cast<T>(15)+ z[21];
    z[21]=z[13]*z[21];
    z[22]= - z[42] + z[63];
    z[22]=z[1]*z[22];
    z[22]= - static_cast<T>(20)+ z[22];
    z[24]= - static_cast<T>(32)- z[34];
    z[24]=z[24]*z[36];
    z[24]=z[24] - 32*z[66];
    z[24]=z[13]*z[24];
    z[22]=2*z[22] + z[24];
    z[22]=z[8]*z[22];
    z[24]= - 13*z[4] - 7*z[3];
    z[24]=z[1]*z[24];
    z[24]=static_cast<T>(66)+ z[24];
    z[23]=z[24]*z[23];
    z[24]= - 6*z[64] - 7;
    z[24]=z[10]*z[24];

    r += 46*z[3] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + 
      z[21] + z[22] + z[23] + z[24];
 
    return r;
}

template double qqb_2lLC_r40(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_2lLC_r40(const std::array<dd_real,31>&);
#endif
