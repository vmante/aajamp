#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r926(const std::array<T,31>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[10];
    z[8]=k[11];
    z[9]=k[15];
    z[10]=k[14];
    z[11]=k[9];
    z[12]=k[3];
    z[13]=k[5];
    z[14]=z[3]*z[4];
    z[15]=z[4]*z[13];
    z[16]=n<T>(1,2) + z[15];
    z[16]=z[16]*z[14];
    z[17]=n<T>(1,2)*z[4];
    z[18]= - z[11]*z[13];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[11]*z[18];
    z[18]= - n<T>(1,2)*z[8] + z[18] + z[17];
    z[18]=z[10]*z[18];
    z[19]=z[8] + z[7];
    z[20]=z[10] + z[3];
    z[21]= - z[20] - z[19];
    z[22]=n<T>(1,2)*z[6];
    z[21]=z[21]*z[22];
    z[23]=z[7]*z[11];
    z[24]=npow(z[11],2);
    z[25]=npow(z[4],2);
    z[16]=z[21] + z[18] + z[16] - z[25] + z[24] + n<T>(1,2)*z[23];
    z[16]=z[5]*z[16];
    z[18]=npow(z[9],2);
    z[18]=4*z[18];
    z[21]=z[3]*z[9];
    z[26]=z[7]*z[9];
    z[21]=z[21] + z[18] + z[26];
    z[21]=z[8]*z[21];
    z[27]=z[3]*z[7];
    z[28]=z[9]*z[27];
    z[21]=z[28] + z[21];
    z[21]=z[10]*z[21];
    z[28]=z[8] - z[11] - z[4];
    z[28]=z[10]*z[28];
    z[28]=z[28] - z[23] - z[14];
    z[20]=z[6]*z[20];
    z[19]= - z[5]*z[19];
    z[19]=z[19] + n<T>(5,2)*z[28] + z[20];
    z[19]=z[6]*z[19];
    z[20]=z[3]*z[25];
    z[28]=z[10]*z[24];
    z[20]=5*z[20] + 3*z[28];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[5]*z[19];
    z[20]=z[8]*z[9];
    z[18]= - z[18] + z[20];
    z[18]=z[27]*z[18];
    z[20]=z[10]*z[4];
    z[20]=5*z[20] - 3*z[25];
    z[20]=z[8]*z[20];
    z[24]=z[7]*z[24];
    z[20]= - 5*z[24] + z[20];
    z[20]=z[20]*z[22];
    z[18]=z[19] + z[20] + z[21] + z[18];
    z[18]=z[1]*z[18];
    z[19]=z[10]*z[11];
    z[14]=z[14] + z[19];
    z[19]=z[11]*z[12];
    z[19]= - n<T>(1,2) + z[19];
    z[19]=z[19]*z[23];
    z[15]= - n<T>(3,2) + z[15];
    z[15]=z[8]*z[4]*z[15];
    z[14]=z[15] + z[19] - z[25] - n<T>(1,2)*z[14];
    z[14]=z[6]*z[14];
    z[15]=4*z[26] + z[25];
    z[15]=z[3]*z[15];
    z[19]=z[25] - z[27];
    z[19]=z[8]*z[19];
    z[17]= - z[17] - 4*z[9] - z[3] - z[7];
    z[17]=z[8]*z[17];
    z[17]= - z[27] + z[17];
    z[17]=z[10]*z[17];
    z[14]=z[18] + z[16] + z[14] + z[17] + z[15] + z[19];

    r += z[14]*npow(z[2],2)*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r926(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r926(const std::array<dd_real,31>&);
#endif
