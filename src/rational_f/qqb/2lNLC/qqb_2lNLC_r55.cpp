#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r55(const std::array<T,31>& k) {
  T z[147];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[10];
    z[7]=k[14];
    z[8]=k[3];
    z[9]=k[9];
    z[10]=k[4];
    z[11]=k[15];
    z[12]=k[5];
    z[13]=k[8];
    z[14]=k[18];
    z[15]=k[29];
    z[16]=k[2];
    z[17]=k[25];
    z[18]=npow(z[1],3);
    z[19]=13*z[18];
    z[20]=npow(z[1],2);
    z[21]=z[20]*z[12];
    z[22]=z[19] + 11*z[21];
    z[23]=z[7]*z[12];
    z[22]=z[22]*z[23];
    z[24]=z[12]*z[1];
    z[25]= - n<T>(13,3)*z[20] + z[24];
    z[25]=z[12]*z[25];
    z[25]= - z[22] + n<T>(23,3)*z[18] + z[25];
    z[25]=z[7]*z[25];
    z[26]=2*z[20];
    z[27]=z[26] - z[24];
    z[28]=z[27]*z[12];
    z[29]=z[24] - z[20];
    z[30]= - z[12]*z[29];
    z[31]=4*z[18];
    z[30]=z[31] + z[30];
    z[30]=z[12]*z[30];
    z[32]=npow(z[1],4);
    z[33]=2*z[32];
    z[30]=z[33] + z[30];
    z[30]=z[7]*z[30];
    z[34]=2*z[18];
    z[30]=z[30] - z[34] - z[28];
    z[30]=z[4]*z[30];
    z[35]=n<T>(7,3)*z[20];
    z[36]=z[18]*z[6];
    z[37]=z[35] - z[36];
    z[38]=z[18]*z[3];
    z[39]=z[2]*z[34];
    z[25]=z[30] + z[39] + z[38] + 4*z[37] + z[25];
    z[25]=z[4]*z[25];
    z[30]=npow(z[12],2);
    z[37]=z[30]*z[7];
    z[39]=z[37]*z[26];
    z[40]=n<T>(5,3)*z[20];
    z[41]= - z[40] - z[24];
    z[41]=z[12]*z[41];
    z[41]=z[41] - z[39];
    z[42]=2*z[7];
    z[41]=z[41]*z[42];
    z[43]=z[21] + z[18];
    z[44]=z[43]*z[37];
    z[45]=5*z[21];
    z[46]=11*z[18];
    z[47]= - z[46] - z[45];
    z[48]=n<T>(1,3)*z[12];
    z[47]=z[47]*z[48];
    z[47]=z[47] - 2*z[44];
    z[47]=z[7]*z[47];
    z[47]=n<T>(11,3)*z[21] + z[47];
    z[47]=z[4]*z[47];
    z[49]=n<T>(8,3)*z[20];
    z[50]=z[49] + z[24];
    z[51]=2*z[6];
    z[52]=z[51]*z[32];
    z[53]=n<T>(17,3)*z[18];
    z[54]= - z[53] + z[52];
    z[54]=z[6]*z[54];
    z[41]=z[47] + z[41] + 2*z[50] + z[54];
    z[47]=2*z[9];
    z[41]=z[41]*z[47];
    z[50]=z[21] - z[18];
    z[54]=z[2]*z[50];
    z[55]=z[34]*z[7];
    z[56]= - z[3]*z[21];
    z[54]=z[54] + z[56] - z[36] + z[55];
    z[54]=z[13]*z[54];
    z[56]=n<T>(1,3)*z[20];
    z[57]= - z[56] - z[36];
    z[57]=z[57]*z[51];
    z[54]=z[54] + n<T>(17,3)*z[1] + z[57];
    z[57]=2*z[1];
    z[58]=z[37]*z[57];
    z[59]=z[32]*z[6];
    z[60]= - n<T>(55,3)*z[18] + 4*z[59];
    z[60]=z[6]*z[60];
    z[60]= - z[58] + z[60] + 6*z[20] + z[24];
    z[60]=z[5]*z[60];
    z[61]=z[24] + 10*z[20];
    z[62]=2*z[12];
    z[63]=z[62]*z[7];
    z[61]=z[61]*z[63];
    z[64]=z[20] - n<T>(4,3)*z[24];
    z[64]=7*z[64] - z[61];
    z[64]=z[7]*z[64];
    z[65]= - z[7]*z[21];
    z[65]=z[65] - z[26] - z[36];
    z[66]=2*z[3];
    z[65]=z[65]*z[66];
    z[67]=2*z[11];
    z[68]=z[63] + 1;
    z[68]=z[7]*z[68];
    z[68]=z[68] + z[67] - z[5];
    z[68]=z[50]*z[68];
    z[68]=z[26] + z[68];
    z[68]=z[2]*z[68];
    z[69]=z[62]*z[20];
    z[70]= - z[11]*z[3]*z[69];
    z[25]=z[41] + z[25] + z[68] + z[70] + z[60] + z[65] + z[64] + 2*
    z[54];
    z[25]=z[9]*z[25];
    z[41]=npow(z[1],5);
    z[54]=z[51]*z[41];
    z[60]=z[34] + 3*z[21];
    z[60]=z[60]*z[62];
    z[60]=z[54] + z[32] + z[60];
    z[60]=z[7]*z[60];
    z[64]=z[32]*z[12];
    z[65]= - z[41] - z[64];
    z[65]=z[7]*z[65];
    z[68]=n<T>(295,18)*z[32];
    z[65]= - z[68] + z[65];
    z[65]=z[3]*z[65];
    z[70]=7*z[21];
    z[60]=z[65] + z[60] - z[52] + z[18] - z[70];
    z[60]=z[3]*z[60];
    z[65]=z[18]*z[12];
    z[71]=z[65] + z[32];
    z[72]=3*z[7];
    z[71]=z[71]*z[72];
    z[71]=z[71] - z[34];
    z[73]=z[41]*z[6];
    z[74]=3*z[32];
    z[75]= - z[74] - z[73];
    z[75]=z[6]*z[75];
    z[75]=z[75] - n<T>(25,2)*z[21] + z[71];
    z[75]=z[7]*z[75];
    z[76]= - n<T>(21,2)*z[18] + z[59];
    z[76]=z[6]*z[76];
    z[60]=z[60] + z[75] + z[76] - n<T>(71,6)*z[20] - 6*z[24];
    z[60]=z[3]*z[60];
    z[75]=16*z[20];
    z[76]=z[10]*z[1];
    z[77]=7*z[76];
    z[78]= - z[75] + z[77];
    z[78]=z[10]*z[78];
    z[78]=z[46] + z[78];
    z[78]=z[10]*z[78];
    z[78]= - z[33] + z[78];
    z[78]=z[10]*z[78];
    z[79]=3*z[20];
    z[80]=z[79] - z[76];
    z[80]=z[80]*z[10];
    z[81]=3*z[18];
    z[82]=z[81] - z[80];
    z[82]=z[10]*z[82];
    z[82]= - z[32] + z[82];
    z[83]=npow(z[10],2);
    z[84]=z[83]*z[6];
    z[82]=z[82]*z[84];
    z[78]=z[78] + z[82];
    z[78]=z[6]*z[78];
    z[82]=z[26] - z[76];
    z[85]=z[82]*z[10];
    z[86]=z[85] - z[18];
    z[87]=z[83]*z[11];
    z[88]=z[86]*z[87];
    z[89]= - 21*z[20] + 17*z[76];
    z[89]=z[10]*z[89];
    z[90]=5*z[18];
    z[89]=z[90] + z[89];
    z[89]=z[10]*z[89];
    z[78]= - 10*z[88] + z[78] - z[32] + z[89];
    z[78]=z[6]*z[78];
    z[89]= - 42*z[20] + 31*z[76];
    z[89]=z[10]*z[89];
    z[91]=z[76] - z[20];
    z[92]=z[12]*z[91];
    z[78]=10*z[92] + z[46] + z[89] + z[78];
    z[78]=z[11]*z[78];
    z[89]=z[86]*z[84];
    z[92]=9*z[20];
    z[93]= - z[92] + 8*z[76];
    z[93]=z[10]*z[93];
    z[93]=z[18] + z[93];
    z[93]=z[10]*z[93];
    z[93]=z[93] - z[89];
    z[93]=z[6]*z[93];
    z[94]= - 27*z[20] + n<T>(49,2)*z[76];
    z[94]=z[10]*z[94];
    z[93]=z[93] + n<T>(19,2)*z[18] + z[94];
    z[93]=z[6]*z[93];
    z[94]=11*z[20];
    z[78]=z[78] + z[93] + n<T>(31,2)*z[24] - z[94] + 36*z[76];
    z[78]=z[11]*z[78];
    z[65]=z[65] - z[32];
    z[93]= - z[5]*z[65];
    z[71]=z[93] - z[71];
    z[71]=z[3]*z[71];
    z[93]=z[18]*z[10];
    z[95]=z[93]*z[6];
    z[96]=z[95] + z[34];
    z[96]=z[96]*z[72];
    z[97]=z[20]*z[10];
    z[98]=z[97]*z[6];
    z[71]=z[96] - z[26] - 3*z[98] + z[71];
    z[71]=z[2]*z[71];
    z[99]=3*z[76];
    z[100]= - z[20] + z[99];
    z[100]=z[10]*z[100];
    z[100]=z[18] + z[100];
    z[100]=z[6]*z[100];
    z[100]=z[100] + n<T>(13,2)*z[20] + z[77];
    z[100]=z[6]*z[100];
    z[101]=27*z[18];
    z[102]= - z[101] + 23*z[97];
    z[103]=z[97] + z[18];
    z[104]=z[10]*z[103];
    z[104]=z[32] + z[104];
    z[104]=z[6]*z[104];
    z[102]=n<T>(1,2)*z[102] + z[104];
    z[102]=z[6]*z[102];
    z[96]= - z[96] + 24*z[20] + z[102];
    z[96]=z[7]*z[96];
    z[102]= - z[12]*z[43];
    z[64]=z[41] - z[64];
    z[104]=n<T>(313,18)*z[3];
    z[64]=z[64]*z[104];
    z[64]=z[64] + n<T>(331,18)*z[32] + z[102];
    z[64]=z[3]*z[64];
    z[102]=4*z[5];
    z[65]=z[65]*z[102];
    z[105]=7*z[18];
    z[64]=z[65] - z[105] + z[64];
    z[64]=z[3]*z[64];
    z[65]=2*z[76];
    z[106]=z[65] - z[20];
    z[106]=z[106]*z[10];
    z[107]=z[6]*z[106];
    z[64]=12*z[20] + z[107] + z[64];
    z[64]=z[5]*z[64];
    z[60]=z[71] + z[78] + z[64] + z[60] + z[96] + n<T>(97,3)*z[1] + z[100];
    z[60]=z[2]*z[60];
    z[64]=2*z[10];
    z[71]=z[59]*z[64];
    z[78]=z[71] + z[33] + z[93];
    z[78]=z[6]*z[78];
    z[96]=4*z[20];
    z[100]=z[96] + z[24];
    z[100]=z[100]*z[12];
    z[107]=z[64]*z[20];
    z[78]=z[78] - z[107] - z[100];
    z[78]=z[7]*z[78];
    z[71]=z[71] - z[33] + n<T>(31,18)*z[93];
    z[71]=z[6]*z[71];
    z[108]=z[81]*z[12];
    z[54]=z[54] + 5*z[32] + z[108];
    z[54]=z[7]*z[54];
    z[109]=z[32]*z[10];
    z[110]= - z[41] + n<T>(3,2)*z[109];
    z[110]=z[3]*z[110];
    z[110]=n<T>(373,18)*z[93] + z[110];
    z[110]=z[3]*z[110];
    z[111]=6*z[18];
    z[54]=z[110] + z[54] - z[111] + z[71];
    z[54]=z[5]*z[54];
    z[71]=z[41]*z[3];
    z[110]= - z[74] + z[71];
    z[110]=z[3]*z[110];
    z[112]=npow(z[1],6);
    z[113]= - z[3]*z[112];
    z[113]=z[41] + z[113];
    z[113]=z[3]*z[113];
    z[108]=z[108] + z[113];
    z[108]=z[5]*z[108];
    z[28]=z[108] + z[110] + z[31] - z[28];
    z[28]=z[2]*z[28];
    z[108]=n<T>(119,9)*z[20] - z[36];
    z[110]=z[32]*z[3];
    z[113]= - n<T>(182,9)*z[18] - n<T>(3,2)*z[110];
    z[113]=z[3]*z[113];
    z[28]=z[28] + z[54] + z[113] + 2*z[108] + z[78];
    z[28]=z[4]*z[28];
    z[54]=z[81] + z[21];
    z[78]=n<T>(295,18)*z[6];
    z[108]=z[112]*z[78];
    z[108]=3*z[41] + z[108];
    z[108]=z[6]*z[108];
    z[108]= - n<T>(61,18)*z[32] + z[108];
    z[108]=z[6]*z[108];
    z[73]=z[32] + z[73];
    z[73]=z[7]*z[73];
    z[73]=z[73] - 2*z[54] + z[108];
    z[73]=z[7]*z[73];
    z[108]=103*z[18] - n<T>(157,3)*z[97];
    z[113]=z[109] - z[41];
    z[78]=z[113]*z[78];
    z[113]=n<T>(241,18)*z[32];
    z[78]=z[78] + z[113];
    z[114]=6*z[93] + z[78];
    z[114]=z[6]*z[114];
    z[108]=n<T>(1,6)*z[108] + z[114];
    z[108]=z[6]*z[108];
    z[109]=z[41] + z[109];
    z[104]=z[109]*z[104];
    z[104]=z[104] + z[113] + 5*z[93];
    z[104]=z[3]*z[104];
    z[109]= - 107*z[18] - 85*z[97];
    z[114]=z[32]*z[7];
    z[104]=z[104] + n<T>(1,18)*z[109] - z[114];
    z[104]=z[3]*z[104];
    z[109]= - z[110]*z[102];
    z[73]=z[109] + z[104] + z[73] - n<T>(334,9)*z[20] + z[108];
    z[73]=z[5]*z[73];
    z[104]=z[112]*npow(z[3],2);
    z[104]= - 152*z[32] + n<T>(313,2)*z[104];
    z[108]=n<T>(1,9)*z[3];
    z[104]=z[104]*z[108];
    z[109]=z[71] - z[32];
    z[112]= - z[109]*z[102];
    z[104]=z[112] + z[104] - z[52] + z[19] - z[69];
    z[104]=z[5]*z[104];
    z[109]=z[5]*z[109];
    z[109]=z[109] + z[18] - z[110];
    z[109]=z[2]*z[109];
    z[112]=5*z[20];
    z[115]=z[112] + z[24];
    z[116]=z[36] + z[115];
    z[71]= - z[32] - z[71];
    z[71]=z[3]*z[71];
    z[71]=70*z[18] + n<T>(313,2)*z[71];
    z[71]=z[71]*z[108];
    z[71]=z[109] + z[104] + 4*z[116] + z[71];
    z[71]=z[2]*z[71];
    z[104]=n<T>(197,9)*z[18] + z[97];
    z[104]=z[10]*z[104];
    z[68]=z[68] + z[93];
    z[68]=z[10]*z[68];
    z[41]= - n<T>(295,18)*z[41] + z[68];
    z[41]=z[6]*z[41];
    z[41]=z[41] + z[113] + z[104];
    z[41]=z[6]*z[41];
    z[68]=n<T>(869,9)*z[18] + 13*z[97];
    z[41]=n<T>(1,2)*z[68] + z[41];
    z[41]=z[6]*z[41];
    z[29]= - z[76] + z[29];
    z[59]= - z[18] - z[59];
    z[59]=z[7]*z[59];
    z[29]=z[59] + 2*z[29] + z[41];
    z[29]=z[7]*z[29];
    z[41]=z[93] - z[32];
    z[59]=z[41]*z[3];
    z[68]=z[105] - z[97];
    z[104]= - 3*z[59] - z[68];
    z[104]=z[3]*z[104];
    z[108]= - z[95] - z[68];
    z[108]=z[108]*z[51];
    z[59]=6*z[95] - z[59];
    z[59]=z[11]*z[59];
    z[59]=z[59] + z[108] + z[104];
    z[59]=z[11]*z[59];
    z[104]=5*z[97];
    z[108]=z[105] - z[104];
    z[109]=n<T>(1,2)*z[7];
    z[108]=z[108]*z[109];
    z[113]= - z[111] - n<T>(313,18)*z[110];
    z[113]=z[3]*z[113];
    z[108]=z[113] - n<T>(106,3)*z[20] + z[108];
    z[108]=z[3]*z[108];
    z[113]=n<T>(257,9)*z[20] - z[98];
    z[113]=z[6]*z[113];
    z[28]=z[28] + z[71] + z[59] + z[73] + z[108] + z[29] + n<T>(50,3)*z[1]
    + z[113];
    z[28]=z[4]*z[28];
    z[29]=z[80] - z[54];
    z[59]=z[29]*z[12];
    z[71]=z[34] + z[21];
    z[71]=z[71]*z[12];
    z[71]=z[71] + z[32];
    z[71]=z[71]*z[4]*z[12];
    z[59]=z[71] - z[59];
    z[73]= - z[13]*z[59];
    z[80]=z[96] - z[76];
    z[80]=z[80]*z[12];
    z[108]=z[81] - z[97];
    z[108]=z[108]*z[10];
    z[108]=z[108] - z[33];
    z[108]=z[108]*z[7];
    z[113]=z[18] + z[69];
    z[113]=z[12]*z[113];
    z[113]= - z[32] + z[113];
    z[113]=z[4]*z[113];
    z[73]=z[73] + z[113] + z[108] + z[80] + z[34] - 3*z[85];
    z[73]=z[13]*z[73];
    z[113]=z[26] + z[24];
    z[116]=z[113]*z[12];
    z[117]=z[116] + z[18];
    z[117]=z[117]*z[37];
    z[82]=z[82] + z[24];
    z[82]=z[82]*z[30];
    z[82]=z[117] - z[82];
    z[117]=z[82]*z[11];
    z[118]=z[97] - z[18];
    z[119]=z[24] - z[91];
    z[119]=z[12]*z[119];
    z[120]=z[24] + z[20];
    z[121]=z[120]*z[37];
    z[119]=z[117] - 3*z[121] + 4*z[119] + z[118];
    z[119]=z[11]*z[119];
    z[121]=4*z[24] - z[112] + z[76];
    z[121]=z[12]*z[121];
    z[104]=z[121] + z[18] - z[104];
    z[104]=z[7]*z[104];
    z[121]=z[41]*z[11];
    z[122]=z[121] + z[81] - z[21];
    z[122]=z[4]*z[122];
    z[123]=3*z[5];
    z[124]= - z[86]*z[123];
    z[73]=z[73] + z[122] + z[119] + z[124] + z[104] - 8*z[24] + z[112]
    + z[99];
    z[73]=z[14]*z[73];
    z[104]=4*z[13];
    z[59]= - z[59]*z[104];
    z[119]=9*z[76];
    z[122]= - z[75] + z[119];
    z[122]=z[10]*z[122];
    z[45]=z[34] + z[45];
    z[45]=z[12]*z[45];
    z[45]= - z[74] + z[45];
    z[45]=z[4]*z[45];
    z[45]=z[59] + z[45] + z[108] + z[70] + z[31] + z[122];
    z[45]=z[13]*z[45];
    z[59]=z[34] - z[97];
    z[70]= - z[59]*z[42];
    z[74]=z[99] - z[26];
    z[74]=z[74]*z[10];
    z[122]=z[5]*z[74];
    z[124]=z[32]*z[5];
    z[125]=z[18] + z[124];
    z[125]=z[4]*z[125];
    z[45]=z[45] + z[125] + z[122] + z[70] + 2*z[91] + z[24];
    z[45]=z[13]*z[45];
    z[70]=3*z[24];
    z[122]=z[70] + z[112] - z[99];
    z[122]=z[12]*z[122];
    z[125]=2*z[24];
    z[126]= - z[79] - z[125];
    z[126]=z[12]*z[126];
    z[126]= - z[18] + z[126];
    z[126]=z[126]*z[63];
    z[117]= - z[117] + z[126] + z[122] + z[118];
    z[117]=z[11]*z[117];
    z[122]=z[77] - z[79];
    z[126]=13*z[24];
    z[127]=z[112] + z[126];
    z[127]=z[12]*z[127];
    z[127]= - z[18] + z[127];
    z[127]=z[7]*z[127];
    z[117]=z[117] + z[127] + 2*z[122] - 15*z[24];
    z[117]=z[11]*z[117];
    z[127]= - 10*z[24] + z[94] - z[76];
    z[127]=z[7]*z[127];
    z[128]=7*z[20];
    z[129]=z[128] - z[76];
    z[129]=z[129]*z[10];
    z[129]=z[129] - z[111];
    z[130]= - z[5]*z[129];
    z[131]=6*z[76];
    z[132]=13*z[20];
    z[130]=z[130] + z[132] - z[131];
    z[130]=z[5]*z[130];
    z[121]=z[121] + z[68];
    z[121]=z[11]*z[121];
    z[133]=z[34] + z[97];
    z[134]= - z[5]*z[133];
    z[121]=z[121] - z[26] + z[134];
    z[121]=z[4]*z[121];
    z[45]=2*z[73] + z[45] + z[121] + z[117] + z[130] + 22*z[1] + z[127];
    z[45]=z[14]*z[45];
    z[73]=npow(z[6],2);
    z[117]= - z[73]*z[57];
    z[121]=z[5]*z[6];
    z[127]=z[20]*z[6];
    z[130]=6*z[127];
    z[134]= - n<T>(1,3)*z[1] - z[130];
    z[134]=z[134]*z[121];
    z[117]=z[117] + z[134];
    z[117]=z[9]*z[117];
    z[134]=z[73]*z[1];
    z[135]=7*z[134];
    z[136]=z[5]*z[135];
    z[117]=z[136] + z[117];
    z[117]=z[117]*z[47];
    z[136]=z[1]*z[121];
    z[136]= - z[134] + z[136];
    z[137]=2*z[5];
    z[136]=z[136]*z[137];
    z[138]=z[1]*npow(z[6],3);
    z[136]=z[138] + z[136];
    z[136]=z[5]*z[136];
    z[139]=z[8]*z[134]*npow(z[9],2);
    z[140]=z[102]*z[139];
    z[141]= - z[14]*npow(z[5],3)*z[57];
    z[117]=z[140] + z[117] + z[136] + z[141];
    z[117]=z[8]*z[117];
    z[136]=z[91]*z[6];
    z[136]=z[136] + z[57];
    z[140]= - z[6]*z[136];
    z[141]=z[91]*z[121];
    z[140]=z[140] + z[141];
    z[140]=z[140]*z[102];
    z[141]=n<T>(259,18)*z[20];
    z[142]=z[141] + z[65];
    z[142]=z[6]*z[142];
    z[142]=n<T>(119,6)*z[1] + z[142];
    z[142]=z[142]*z[73];
    z[140]=z[142] + z[140];
    z[140]=z[5]*z[140];
    z[142]=n<T>(5,3)*z[1];
    z[130]= - z[142] + z[130];
    z[130]=z[6]*z[130];
    z[143]=3*z[36];
    z[56]=z[56] + z[143];
    z[56]=z[56]*z[51];
    z[144]=n<T>(11,3)*z[1];
    z[56]= - z[144] + z[56];
    z[56]=z[5]*z[56];
    z[56]=z[130] + z[56];
    z[56]=z[9]*z[56];
    z[130]=7*z[1];
    z[145]= - z[130] - 12*z[127];
    z[121]=z[145]*z[121];
    z[56]=z[56] - z[135] + z[121];
    z[56]=z[56]*z[47];
    z[121]= - z[5]*z[91];
    z[121]=z[57] + z[121];
    z[121]=z[121]*npow(z[5],2);
    z[135]=z[14]*z[1]*z[123];
    z[121]=z[121] + z[135];
    z[121]=z[14]*z[121];
    z[56]=z[117] + z[56] + 4*z[121] - z[138] + z[140];
    z[56]=z[8]*z[56];
    z[117]=67*z[20] - 13*z[36];
    z[121]=n<T>(1,6)*z[6];
    z[117]=z[117]*z[121];
    z[135]=z[36] + z[20];
    z[140]=z[3]*z[135];
    z[145]=5*z[1];
    z[117]= - 5*z[140] + z[145] + z[117];
    z[117]=z[3]*z[117];
    z[140]= - 223*z[18] + 295*z[110];
    z[140]=z[3]*z[140];
    z[140]= - z[94] + n<T>(1,18)*z[140];
    z[140]=z[3]*z[140];
    z[110]= - z[90] + z[110];
    z[110]=z[3]*z[110];
    z[110]=z[26] + z[110];
    z[110]=z[4]*z[110];
    z[146]=z[1] - z[127];
    z[110]=z[110] + 2*z[146] + z[140];
    z[110]=z[4]*z[110];
    z[140]=25*z[76];
    z[146]=z[20] + z[140];
    z[146]=z[6]*z[146];
    z[146]= - 55*z[1] + z[146];
    z[121]=z[146]*z[121];
    z[110]=z[110] + z[121] + z[117];
    z[110]=z[2]*z[110];
    z[117]= - n<T>(281,3)*z[20] + n<T>(53,2)*z[76];
    z[121]=z[141] + z[76];
    z[121]=z[10]*z[121];
    z[121]= - n<T>(277,18)*z[18] + z[121];
    z[121]=z[6]*z[121];
    z[117]=n<T>(1,3)*z[117] + z[121];
    z[117]=z[6]*z[117];
    z[117]=n<T>(31,6)*z[1] + z[117];
    z[117]=z[6]*z[117];
    z[121]=z[86]*z[51];
    z[119]= - z[119] + 14*z[20];
    z[141]=z[121] + z[119];
    z[141]=z[6]*z[141];
    z[121]= - z[5]*z[121];
    z[121]=z[141] + z[121];
    z[121]=z[5]*z[121];
    z[117]=z[117] + z[121];
    z[117]=z[5]*z[117];
    z[121]=n<T>(11,3)*z[20];
    z[141]=z[121] - z[143];
    z[141]=z[6]*z[141];
    z[141]=z[1] + z[141];
    z[52]= - n<T>(1,3)*z[18] - z[52];
    z[52]=z[6]*z[52];
    z[52]=z[121] + z[52];
    z[52]=z[5]*z[52];
    z[143]=z[4]*z[26];
    z[52]=z[143] + 2*z[141] + z[52];
    z[52]=z[52]*z[47];
    z[141]=z[73]*z[92];
    z[143]=z[3]*z[127];
    z[141]=z[141] + z[143];
    z[143]=n<T>(97,3)*z[20] + 6*z[36];
    z[143]=z[6]*z[143];
    z[143]= - n<T>(37,3)*z[1] + z[143];
    z[143]=z[5]*z[143];
    z[52]=z[52] + 2*z[141] + z[143];
    z[52]=z[9]*z[52];
    z[141]= - n<T>(277,18)*z[20] - z[65];
    z[141]=z[6]*z[141];
    z[135]=z[135] - z[38];
    z[135]=z[3]*z[135];
    z[135]=z[135] - 19*z[1] + z[141];
    z[135]=z[73]*z[135];
    z[141]=z[86]*z[137];
    z[119]=z[141] - z[119];
    z[119]=z[5]*z[119];
    z[119]= - 20*z[1] + z[119];
    z[119]=z[5]*z[119];
    z[141]=z[91]*z[123];
    z[141]= - z[57] + z[141];
    z[141]=z[14]*z[141];
    z[119]=z[119] + 6*z[141];
    z[119]=z[14]*z[119];
    z[52]=z[56] + z[52] + z[119] + z[117] + z[135] + z[110];
    z[52]=z[8]*z[52];
    z[56]=z[10]*z[86];
    z[56]= - 3*z[56] + z[88];
    z[56]=z[11]*z[56];
    z[86]=z[128] - z[99];
    z[86]=z[10]*z[86];
    z[110]=z[50]*z[23];
    z[56]=z[56] + z[110] + z[80] - z[81] + z[86];
    z[56]=z[2]*z[56];
    z[80]=z[20] + z[125];
    z[80]=z[12]*z[80];
    z[39]=z[80] + z[39];
    z[39]=z[7]*z[39];
    z[80]=z[87]*z[91];
    z[86]= - z[106] + 2*z[80];
    z[86]=z[11]*z[86];
    z[69]=z[81] + z[69];
    z[69]=z[12]*z[69];
    z[44]=z[69] + z[44];
    z[44]=z[7]*z[44];
    z[69]=z[93]*z[11];
    z[44]= - z[69] + z[44] + z[54];
    z[44]=z[4]*z[44];
    z[54]=z[24] + z[76];
    z[54]=z[54]*z[12];
    z[110]=z[123]*z[54];
    z[39]=z[44] + z[56] + z[86] + z[110] + z[39] - z[99] - z[24];
    z[44]=4*z[15];
    z[39]=z[39]*z[44];
    z[56]=z[112] - z[77] - z[24];
    z[56]=z[12]*z[56];
    z[86]=z[5]*z[30]*z[65];
    z[56]=z[56] + z[86];
    z[56]=z[5]*z[56];
    z[86]=z[70] + z[96];
    z[86]=z[86]*z[12];
    z[86]=z[86] + z[18];
    z[110]= - z[7]*z[86];
    z[56]=z[56] + z[110] - 12*z[24] - z[96] - z[77];
    z[56]=z[56]*z[137];
    z[50]=z[50]*z[63];
    z[50]=z[50] + z[105] - 9*z[21];
    z[50]=z[7]*z[50];
    z[63]=4*z[76];
    z[77]= - z[92] + z[63];
    z[77]=z[10]*z[77];
    z[77]=z[90] + z[77];
    z[77]=z[77]*z[64];
    z[77]=z[77] + 5*z[88];
    z[77]=z[77]*z[67];
    z[88]=z[75] + z[76];
    z[88]=z[10]*z[88];
    z[46]=z[77] - z[46] + z[88];
    z[46]=z[11]*z[46];
    z[77]=5*z[76];
    z[46]=z[46] + z[50] - z[125] - z[75] - z[77];
    z[46]=z[2]*z[46];
    z[22]= - z[22] - z[101] - 16*z[21];
    z[22]=z[7]*z[22];
    z[50]= - z[114] + z[59];
    z[50]=z[50]*z[137];
    z[59]= - 3*z[69] + z[68];
    z[59]=z[59]*z[67];
    z[22]=z[59] + z[50] - z[96] + z[22];
    z[22]=z[4]*z[22];
    z[50]=5*z[24];
    z[59]= - z[26] - z[50];
    z[59]=3*z[59] - z[61];
    z[59]=z[7]*z[59];
    z[61]= - z[74] + 10*z[80];
    z[61]=z[11]*z[61];
    z[68]=11*z[76];
    z[61]=z[61] + z[20] - z[68];
    z[61]=z[61]*z[67];
    z[22]=z[39] + z[22] + z[46] + z[61] + z[56] + z[130] + z[59];
    z[22]=z[15]*z[22];
    z[39]= - z[96] + z[77];
    z[39]=z[6]*z[10]*z[39];
    z[39]=z[39] + z[20] + z[68];
    z[39]=z[6]*z[39];
    z[39]= - n<T>(15,2)*z[1] + z[39];
    z[39]=z[6]*z[39];
    z[46]=z[84]*z[91];
    z[56]=z[83]*z[145];
    z[56]=z[56] + 3*z[46];
    z[56]=z[6]*z[56];
    z[59]=z[87]*z[1];
    z[56]=10*z[59] - z[99] + z[56];
    z[56]=z[6]*z[56];
    z[61]= - z[7]*z[126];
    z[56]=z[61] + 13*z[1] + z[56];
    z[56]=z[11]*z[56];
    z[61]= - n<T>(13,2)*z[1] - z[127];
    z[61]=z[7]*z[61];
    z[39]=z[56] + z[39] + z[61];
    z[39]=z[11]*z[39];
    z[56]=z[6]*z[34];
    z[56]= - z[121] + z[56];
    z[56]=z[6]*z[56];
    z[58]=n<T>(1,3)*z[24] - z[58];
    z[58]=z[7]*z[58];
    z[56]=z[58] + z[142] + z[56];
    z[56]=z[56]*z[47];
    z[58]=z[20]*z[7];
    z[61]=z[58] - z[127];
    z[68]=z[61]*z[13];
    z[69]=n<T>(4,3)*z[1] - 3*z[127];
    z[69]=z[69]*z[51];
    z[74]=z[50]*z[7];
    z[77]=n<T>(7,3)*z[1] - z[74];
    z[77]=z[7]*z[77];
    z[56]=z[56] + 2*z[68] + z[69] + z[77];
    z[56]=z[9]*z[56];
    z[69]=z[144] - 4*z[127];
    z[69]=z[9]*z[6]*z[69];
    z[69]=3*z[134] + z[69];
    z[47]=z[69]*z[47];
    z[47]=4*z[139] + z[138] + z[47];
    z[47]=z[8]*z[47];
    z[59]=z[59] + z[76];
    z[59]=z[59]*z[11];
    z[37]=z[1]*z[37];
    z[37]= - z[24] + z[37];
    z[37]=z[7]*z[37];
    z[37]=z[37] - z[59];
    z[37]=z[37]*z[44];
    z[44]=z[1] - 10*z[59];
    z[44]=z[11]*z[44];
    z[59]=6*z[1] - z[74];
    z[59]=z[7]*z[59];
    z[37]=z[37] + z[59] + z[44];
    z[37]=z[15]*z[37];
    z[44]= - z[76]*z[51];
    z[44]=z[1] + z[44];
    z[44]=z[44]*z[73];
    z[51]= - z[1]*z[84];
    z[51]=z[76] + z[51];
    z[51]=z[6]*z[51];
    z[51]= - z[1] + z[51];
    z[51]=z[6]*z[51];
    z[59]=z[7]*z[1];
    z[51]=z[51] + z[59];
    z[51]=z[11]*z[51];
    z[44]=z[44] + z[51];
    z[44]=z[11]*z[44];
    z[44]= - z[138] + z[44];
    z[44]=z[16]*z[44];
    z[51]=z[136]*z[73];
    z[59]=z[6]*z[59];
    z[51]=z[51] + z[59];
    z[59]=z[67]*z[68];
    z[37]=z[44] + z[37] + z[47] + z[56] + z[59] + 2*z[51] + z[39];
    z[37]=z[16]*z[37];
    z[39]=z[125] + z[40] - z[65];
    z[39]=z[12]*z[39];
    z[21]=z[21] - z[118];
    z[21]=z[12]*z[21];
    z[40]= - z[2]*z[43]*z[30];
    z[21]=z[40] - z[93] + z[21];
    z[21]=z[4]*z[21];
    z[40]=n<T>(16,3)*z[20];
    z[44]= - z[40] - z[24];
    z[44]=z[44]*z[62];
    z[44]= - z[53] + z[44];
    z[44]=z[12]*z[44];
    z[44]=z[32] + z[44];
    z[44]=z[2]*z[44];
    z[47]=n<T>(7,3)*z[18];
    z[21]=2*z[21] + z[44] + z[39] - z[47] - z[107];
    z[21]=z[4]*z[21];
    z[39]=n<T>(5,3)*z[24];
    z[35]=z[39] - z[35] + z[65];
    z[35]=z[12]*z[35];
    z[44]= - z[54] - z[118];
    z[44]=z[3]*z[44]*z[62];
    z[35]=z[44] + 2*z[118] + z[35];
    z[35]=z[3]*z[35];
    z[44]=z[112] + 11*z[24];
    z[44]=z[44]*z[30];
    z[51]= - z[120]*npow(z[12],3)*z[66];
    z[44]=n<T>(1,3)*z[44] + z[51];
    z[44]=z[3]*z[44];
    z[51]= - z[91]*z[64];
    z[53]=z[76] - z[50];
    z[48]=z[53]*z[48];
    z[44]=z[44] + z[51] + z[48];
    z[44]=z[5]*z[44];
    z[48]= - 19*z[20] - z[76];
    z[48]=n<T>(1,3)*z[48] - z[125];
    z[48]=z[12]*z[48];
    z[48]= - z[108] + z[48] - z[111] + 7*z[85];
    z[48]=z[2]*z[48];
    z[51]=z[2]*z[12];
    z[29]= - z[29]*z[51];
    z[53]=z[2]*z[71];
    z[29]=z[29] + z[53];
    z[29]=z[29]*z[104];
    z[21]=z[29] + z[21] + z[48] + z[35] + z[44];
    z[21]=z[13]*z[21];
    z[29]= - n<T>(17,3)*z[20] - z[24];
    z[29]=z[29]*z[62];
    z[29]= - z[124] - n<T>(15,2)*z[18] + z[29];
    z[29]=z[2]*z[29];
    z[31]=z[31] + z[116];
    z[35]= - z[51] + 1;
    z[31]=z[31]*z[35];
    z[31]=z[95] + z[31];
    z[31]=z[4]*z[31];
    z[35]=z[18]*z[7];
    z[29]=z[31] + z[29] + z[35] + z[40] + z[98];
    z[29]=z[4]*z[29];
    z[31]= - z[100] + z[118];
    z[31]=z[3]*z[31];
    z[39]=z[26] + z[39];
    z[40]= - z[6]*z[118];
    z[31]=z[31] - z[35] + 2*z[39] + z[40];
    z[31]=z[3]*z[31];
    z[39]= - 31*z[24] + 35*z[20] - 73*z[76];
    z[40]=n<T>(19,3)*z[20] + n<T>(11,2)*z[24];
    z[40]=z[12]*z[40];
    z[30]=z[30]*z[3];
    z[27]= - z[27]*z[30];
    z[27]=z[40] + z[27];
    z[27]=z[3]*z[27];
    z[40]=z[7] - z[6];
    z[40]=z[118]*z[40];
    z[27]=z[27] + n<T>(1,6)*z[39] + z[40];
    z[27]=z[5]*z[27];
    z[39]=n<T>(5,2)*z[24] - 41*z[20] + n<T>(61,2)*z[76];
    z[40]= - z[5]*z[106];
    z[35]=z[40] + n<T>(1,3)*z[39] + z[35];
    z[35]=z[2]*z[35];
    z[39]=z[11]*z[55];
    z[21]=z[21] + z[29] + z[35] + z[39] + z[27] - 2*z[61] + z[31];
    z[21]=z[13]*z[21];
    z[27]=z[36]*z[83];
    z[29]=z[47] - z[97];
    z[29]=z[10]*z[29];
    z[29]= - z[27] - z[32] + z[29];
    z[29]=z[6]*z[29];
    z[31]=z[18] + n<T>(19,3)*z[97];
    z[29]=n<T>(1,2)*z[31] + z[29];
    z[29]=z[6]*z[29];
    z[31]=z[43]*z[72];
    z[29]=z[31] + z[29] - z[50] + 26*z[20] + n<T>(23,6)*z[76];
    z[29]=z[7]*z[29];
    z[31]=n<T>(49,6)*z[18] + z[107];
    z[31]=z[10]*z[31];
    z[27]=z[27] + z[33] + z[31];
    z[27]=z[6]*z[27];
    z[31]=n<T>(7,6)*z[20] + z[76];
    z[31]=z[10]*z[31];
    z[33]= - n<T>(13,6)*z[76] - z[115];
    z[33]=z[12]*z[33];
    z[27]=z[27] + z[33] - z[34] + z[31];
    z[27]=z[7]*z[27];
    z[31]=z[64]*z[18];
    z[31]=z[31] - z[32];
    z[31]=z[31]*z[6];
    z[31]=z[31] + z[107];
    z[33]= - n<T>(73,6)*z[18] - z[31];
    z[33]=z[6]*z[33];
    z[35]= - z[116] - z[133];
    z[35]=z[12]*z[35];
    z[32]=z[93] + z[32];
    z[35]=z[35] - z[32];
    z[35]=z[7]*z[35];
    z[35]= - n<T>(295,9)*z[18] + z[35];
    z[35]=z[3]*z[35];
    z[27]=z[35] + z[27] + z[33] - n<T>(415,18)*z[20] - z[70];
    z[27]=z[3]*z[27];
    z[31]= - n<T>(13,2)*z[18] + z[31];
    z[31]=z[6]*z[31];
    z[31]=z[31] + z[49] - z[76];
    z[31]=z[6]*z[31];
    z[27]=z[27] + z[29] - n<T>(25,6)*z[1] + z[31];
    z[27]=z[3]*z[27];
    z[29]=z[18] - 6*z[97];
    z[29]=z[10]*z[29];
    z[29]=z[29] - z[78];
    z[29]=z[6]*z[29];
    z[19]=z[19] - 25*z[97];
    z[31]= - z[7]*z[41];
    z[19]=z[31] + n<T>(1,2)*z[19] + z[29];
    z[19]=z[6]*z[19];
    z[19]= - n<T>(31,2)*z[20] + z[19];
    z[19]=z[7]*z[19];
    z[29]= - z[12]*z[76];
    z[29]= - n<T>(313,18)*z[103] + z[29];
    z[29]=z[12]*z[29];
    z[29]=n<T>(313,18)*z[32] + z[29];
    z[29]=z[3]*z[29];
    z[31]=143*z[18] + 94*z[97];
    z[32]=13*z[76];
    z[33]= - n<T>(439,3)*z[20] - z[32];
    z[33]=n<T>(1,6)*z[33] - 7*z[24];
    z[33]=z[12]*z[33];
    z[29]=z[29] + n<T>(2,9)*z[31] + z[33];
    z[29]=z[3]*z[29];
    z[31]=35*z[24] + n<T>(739,3)*z[20] + 79*z[76];
    z[33]=z[113]*z[62];
    z[33]=z[18] + z[33];
    z[33]=z[33]*z[42];
    z[29]=z[29] + n<T>(1,6)*z[31] + z[33];
    z[29]=z[3]*z[29];
    z[31]=z[24] + z[122];
    z[31]=z[12]*z[31];
    z[33]= - z[65]*z[30];
    z[31]=z[33] - z[34] + z[31];
    z[31]=z[31]*z[66];
    z[30]= - z[76]*z[102]*z[30];
    z[33]=z[6]*z[129];
    z[30]=z[30] + z[31] - z[96] + z[33];
    z[30]=z[5]*z[30];
    z[31]= - n<T>(79,18)*z[20] - z[76];
    z[31]=z[10]*z[31];
    z[31]=n<T>(295,18)*z[95] + n<T>(214,9)*z[18] + z[31];
    z[31]=z[6]*z[31];
    z[33]= - n<T>(11,9)*z[20] + n<T>(1,2)*z[76];
    z[31]=7*z[33] + z[31];
    z[31]=z[6]*z[31];
    z[19]=z[30] + z[29] + z[19] - z[57] + z[31];
    z[19]=z[5]*z[19];
    z[29]=z[82]*z[3];
    z[30]=z[125] - z[132] - z[76];
    z[30]=z[12]*z[30];
    z[31]=23*z[20] + 9*z[24];
    z[31]=z[12]*z[31];
    z[31]=14*z[18] + z[31];
    z[31]=z[31]*z[23];
    z[30]= - z[29] + z[31] - 3*z[118] + z[30];
    z[30]=z[3]*z[30];
    z[31]= - 36*z[20] - n<T>(53,2)*z[24];
    z[31]=z[12]*z[31];
    z[31]= - 15*z[18] + z[31];
    z[31]=z[7]*z[31];
    z[30]=z[30] + z[31] + z[32] - 24*z[24];
    z[30]=z[3]*z[30];
    z[31]=z[86]*z[23];
    z[31]=z[29] - z[31];
    z[33]=z[112] - z[63];
    z[33]=11*z[33] + 34*z[24];
    z[33]=z[12]*z[33];
    z[31]=z[33] - z[118] + 11*z[31];
    z[31]=z[3]*z[31];
    z[33]=z[75] - z[32];
    z[33]=z[10]*z[33];
    z[33]= - z[81] + z[33];
    z[33]=z[10]*z[33];
    z[33]=z[33] + 3*z[89];
    z[33]=z[6]*z[33];
    z[32]=z[112] - z[32];
    z[32]=z[10]*z[32];
    z[32]=z[32] + z[33];
    z[32]=z[6]*z[32];
    z[29]= - 20*z[46] + z[29];
    z[29]=z[11]*z[29];
    z[33]=z[132] + 25*z[24];
    z[23]=z[23] - 1;
    z[23]=z[33]*z[23];
    z[23]=z[29] + z[31] + z[32] + z[140] + z[23];
    z[23]=z[11]*z[23];
    z[29]=z[94] - 20*z[76];
    z[29]=z[10]*z[29];
    z[31]=z[79] - z[65];
    z[31]=z[10]*z[31];
    z[31]= - z[18] + z[31];
    z[31]=z[6]*z[31]*z[64];
    z[29]=z[29] + z[31];
    z[29]=z[6]*z[29];
    z[29]=z[29] - z[20] - 26*z[76];
    z[29]=z[6]*z[29];
    z[24]=15*z[20] - z[24];
    z[24]=z[24]*z[109];
    z[31]=z[5]*z[20];
    z[23]=z[23] + n<T>(50,3)*z[31] + z[30] + z[24] + n<T>(51,2)*z[1] + z[29];
    z[23]=z[11]*z[23];
    z[24]=z[20] - z[38];
    z[24]=z[8]*z[3]*z[24];
    z[24]=z[24] - z[20];
    z[24]=z[2]*z[24];
    z[29]=z[7]*z[93];
    z[29]= - z[34] + z[29];
    z[29]=z[3]*z[29];
    z[30]=z[7]*z[97];
    z[29]=z[30] + z[29];
    z[29]=z[3]*z[29];
    z[26]= - z[11]*z[26];
    z[24]=z[26] + z[29] - z[57] + z[58] + z[24];
    z[24]=z[17]*z[24];
    z[18]=n<T>(295,18)*z[18] + z[85];
    z[18]=z[6]*z[18];
    z[18]=z[18] + n<T>(29,6)*z[20] - z[131];
    z[18]=z[6]*z[18];
    z[18]= - 17*z[1] + z[18];
    z[18]=z[6]*z[18];
    z[20]=6*z[98] - n<T>(4,3)*z[20] - z[76];
    z[20]=z[6]*z[20];
    z[26]= - z[7]*z[96];
    z[20]=z[26] - n<T>(37,6)*z[1] + z[20];
    z[20]=z[7]*z[20];

    r += z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + n<T>(25,3)*z[24]
       + z[25] + z[27] + z[28] + z[37] + z[45] + z[52] + z[60];
 
    return r;
}

template double qqb_2lNLC_r55(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r55(const std::array<dd_real,31>&);
#endif
