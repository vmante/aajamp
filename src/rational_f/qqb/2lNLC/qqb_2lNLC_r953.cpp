#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r953(const std::array<T,31>& k) {
  T z[94];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[9];
    z[12]=k[15];
    z[13]=k[8];
    z[14]=npow(z[3],3);
    z[15]=z[14]*z[5];
    z[16]=npow(z[7],3);
    z[17]=z[15]*z[16];
    z[18]=npow(z[5],3);
    z[19]=z[18]*z[3];
    z[20]=z[19]*z[7];
    z[21]=npow(z[6],2);
    z[22]=z[21]*z[20];
    z[22]=z[17] + z[22];
    z[22]=z[6]*z[22];
    z[23]=z[15]*z[4];
    z[24]=z[4]*z[7];
    z[25]=z[24]*npow(z[6],3);
    z[23]=z[23] + z[25];
    z[25]=z[23]*npow(z[2],2);
    z[26]=z[19]*npow(z[4],3);
    z[27]=npow(z[24],3);
    z[28]=z[6]*z[27];
    z[25]=z[25] + z[26] + z[28];
    z[25]=z[2]*z[25];
    z[22]=z[22] + z[25];
    z[22]=z[1]*z[22];
    z[17]=z[22] + z[17] + z[26];
    z[22]=n<T>(1,3)*z[5];
    z[14]= - z[14]*z[22];
    z[25]=n<T>(1,3)*z[3];
    z[26]=z[25] - z[5];
    z[28]=npow(z[3],2);
    z[29]=z[28]*z[4];
    z[30]= - z[26]*z[29];
    z[31]=z[4] + z[7];
    z[32]=n<T>(1,3)*z[6];
    z[33]= - z[31]*z[32];
    z[33]=z[24] + z[33];
    z[33]=z[33]*z[21];
    z[14]=z[33] + z[14] + z[30];
    z[14]=z[2]*z[14];
    z[14]=z[14] + z[23];
    z[14]=z[2]*z[14];
    z[23]=n<T>(1,3)*z[16];
    z[30]=z[23] + z[18];
    z[33]=npow(z[5],2);
    z[34]=z[33]*z[3];
    z[35]= - z[34] + z[30];
    z[35]=z[4]*z[35];
    z[36]=npow(z[7],2);
    z[37]=z[36]*z[4];
    z[38]= - z[16] - z[37];
    z[38]=z[6]*z[38];
    z[35]=z[38] - z[19] + z[35];
    z[38]=npow(z[4],2);
    z[35]=z[38]*z[35];
    z[14]=z[14] + z[35];
    z[14]=z[2]*z[14];
    z[35]= - z[18]*z[25];
    z[39]=z[34] - z[18];
    z[39]= - z[7]*z[39];
    z[35]=z[35] + z[39];
    z[35]=z[6]*z[35];
    z[20]=z[20] + z[35];
    z[20]=z[6]*z[20];
    z[35]=z[28]*z[7];
    z[26]= - z[26]*z[35];
    z[26]= - z[15] + z[26];
    z[26]=z[26]*z[36];
    z[20]=z[20] + z[26] + n<T>(1,3)*z[27];
    z[20]=z[6]*z[20];
    z[14]=z[14] + z[20] + n<T>(1,3)*z[17];
    z[14]=z[1]*z[14];
    z[17]=z[5]*z[9];
    z[20]=n<T>(1,3)*z[17];
    z[26]= - static_cast<T>(1)+ z[20];
    z[26]=z[26]*z[34];
    z[26]=z[26] + z[30];
    z[26]=z[4]*z[26];
    z[19]=n<T>(2,3)*z[19];
    z[26]= - z[19] + z[26];
    z[26]=z[26]*z[38];
    z[27]=2*z[5];
    z[30]= - z[3]*z[17];
    z[30]=z[27] + z[30];
    z[30]=z[30]*z[35];
    z[30]= - 2*z[15] + z[30];
    z[35]=n<T>(1,3)*z[36];
    z[30]=z[30]*z[35];
    z[14]=z[14] + z[30] + z[26];
    z[26]=z[17] - 1;
    z[30]= - z[26]*z[25];
    z[30]= - z[5] + z[30];
    z[39]=28*z[28];
    z[30]=z[30]*z[39];
    z[40]=28*z[3];
    z[41]=z[3]*z[8];
    z[42]= - static_cast<T>(2)- z[41];
    z[42]=z[42]*z[40];
    z[43]=n<T>(241,2)*z[5];
    z[42]= - z[43] + z[42];
    z[42]=z[4]*z[42]*z[25];
    z[44]=z[7]*z[10];
    z[45]=z[44] + 1;
    z[46]=z[4]*z[8];
    z[47]=z[45] + z[46];
    z[48]=z[6]*z[47];
    z[48]= - 2*z[31] + z[48];
    z[48]=z[6]*z[48];
    z[48]= - n<T>(241,2)*z[24] + 28*z[48];
    z[48]=z[48]*z[32];
    z[30]=z[48] + z[30] + z[42];
    z[30]=z[2]*z[30];
    z[42]=3*z[5];
    z[48]=z[42] - z[3];
    z[29]=z[48]*z[29];
    z[48]= - z[7] - n<T>(2,3)*z[4];
    z[48]=z[6]*z[48];
    z[48]=3*z[24] + z[48];
    z[48]=z[48]*z[21];
    z[15]=z[48] - n<T>(2,3)*z[15] + z[29];
    z[15]=28*z[15] + z[30];
    z[15]=z[2]*z[15];
    z[29]=z[5]*z[10];
    z[30]= - static_cast<T>(4)+ n<T>(7,3)*z[29];
    z[48]=14*z[33];
    z[30]=z[30]*z[48];
    z[49]=z[3]*z[5];
    z[30]=z[30] - n<T>(241,6)*z[49];
    z[50]=28*z[36];
    z[51]=n<T>(1,3)*z[44];
    z[52]= - static_cast<T>(1)- z[51];
    z[52]=z[52]*z[50];
    z[52]=z[52] + z[30];
    z[52]=z[4]*z[52];
    z[53]= - 3*z[34] + n<T>(7,3)*z[18];
    z[54]=n<T>(2,3)*z[16];
    z[55]= - z[54] - z[53];
    z[52]=28*z[55] + z[52];
    z[52]=z[4]*z[52];
    z[55]=z[28]*z[5];
    z[55]=z[55] + z[18];
    z[55]=z[55]*z[3];
    z[56]=n<T>(241,6)*z[55];
    z[52]= - z[56] + z[52];
    z[52]=z[4]*z[52];
    z[57]=n<T>(241,6)*z[24];
    z[58]=84*z[36] - z[57];
    z[58]=z[4]*z[58];
    z[58]= - n<T>(241,6)*z[16] + z[58];
    z[58]=z[4]*z[58];
    z[21]= - z[21]*z[57];
    z[21]=z[58] + z[21];
    z[21]=z[6]*z[21];
    z[15]=z[15] + z[52] + z[21];
    z[15]=z[2]*z[15];
    z[21]= - z[42] + n<T>(2,3)*z[3];
    z[21]=z[21]*z[39];
    z[39]=z[3]*z[9];
    z[42]= - static_cast<T>(2)+ z[39];
    z[40]=z[42]*z[40];
    z[40]= - z[43] + z[40];
    z[40]=z[7]*z[40]*z[25];
    z[21]=z[21] + z[40];
    z[21]=z[7]*z[21];
    z[21]= - z[56] + z[21];
    z[21]=z[7]*z[21];
    z[40]=static_cast<T>(2)+ z[17];
    z[40]=z[40]*z[34];
    z[40]= - 2*z[18] + z[40];
    z[30]=z[7]*z[30];
    z[30]=n<T>(28,3)*z[40] + z[30];
    z[30]=z[6]*z[30];
    z[40]=z[7]*z[53];
    z[19]= - z[19] + z[40];
    z[19]=28*z[19] + z[30];
    z[19]=z[6]*z[19];
    z[30]=n<T>(1,3)*z[9];
    z[40]=z[30]*z[7];
    z[42]= - static_cast<T>(1)+ z[40];
    z[37]=z[42]*z[37];
    z[37]= - z[54] + z[37];
    z[37]=z[37]*z[38];
    z[19]=z[19] + z[21] + 28*z[37];
    z[19]=z[6]*z[19];
    z[14]=z[15] + z[19] + 28*z[14];
    z[14]=z[1]*z[14];
    z[15]=2*z[9];
    z[19]=z[15] - z[10];
    z[21]=z[5]*z[19];
    z[21]=static_cast<T>(2)+ z[21];
    z[21]=z[21]*z[33];
    z[37]=npow(z[9],2);
    z[38]=z[37]*z[5];
    z[42]= - z[15] - z[38];
    z[43]=14*z[5];
    z[42]=z[42]*z[43];
    z[42]=n<T>(353,2) + z[42];
    z[42]=z[42]*z[49];
    z[52]=npow(z[10],2);
    z[53]=z[52]*z[5];
    z[54]=2*z[10];
    z[56]=z[53] - z[54];
    z[57]=56*z[5];
    z[56]=z[56]*z[57];
    z[56]=z[56] - 325;
    z[56]=z[56]*z[5];
    z[57]=n<T>(353,2)*z[3];
    z[56]=z[56] + z[57];
    z[58]=z[7]*z[56];
    z[21]= - n<T>(353,2)*z[24] + z[58] + 28*z[21] + z[42];
    z[21]=z[21]*z[32];
    z[42]=static_cast<T>(5)+ z[17];
    z[42]=z[42]*z[33]*z[25];
    z[42]= - z[18] + z[42];
    z[58]=z[29] - n<T>(7,3);
    z[59]=7*z[33];
    z[58]=z[58]*z[59];
    z[59]=z[25]*z[5];
    z[58]=z[58] - z[59];
    z[60]=2*z[7];
    z[61]=z[58]*z[60];
    z[42]=7*z[42] + z[61];
    z[21]=4*z[42] + z[21];
    z[21]=z[6]*z[21];
    z[42]=7*z[3];
    z[61]=static_cast<T>(5)- z[39];
    z[61]=z[61]*z[42];
    z[61]=z[27] + z[61];
    z[61]=z[3]*z[61];
    z[62]=z[37]*z[3];
    z[63]=z[15] - z[62];
    z[64]=14*z[3];
    z[63]=z[63]*z[64];
    z[63]=n<T>(353,2) + z[63];
    z[63]=z[63]*z[25];
    z[65]=127*z[5];
    z[63]= - z[65] + z[63];
    z[63]=z[7]*z[63];
    z[61]=n<T>(4,3)*z[61] + z[63];
    z[61]=z[7]*z[61];
    z[63]=8*z[5];
    z[57]= - z[63] + z[57];
    z[57]=z[3]*z[57];
    z[66]=8*z[33];
    z[57]=z[66] + z[57];
    z[57]=z[3]*z[57];
    z[67]=269*z[18];
    z[57]= - z[67] + z[57];
    z[57]=n<T>(1,3)*z[57] + z[61];
    z[57]=z[7]*z[57];
    z[40]=static_cast<T>(2)- z[40];
    z[40]=z[40]*z[50];
    z[61]=4*z[9];
    z[68]=z[37]*z[7];
    z[69]= - z[61] + z[68];
    z[70]=14*z[7];
    z[69]=z[69]*z[70];
    z[69]= - n<T>(269,2) + z[69];
    z[69]=z[69]*z[24];
    z[40]=z[40] + n<T>(1,3)*z[69];
    z[40]=z[4]*z[40];
    z[40]= - n<T>(353,6)*z[16] + z[40];
    z[40]=z[4]*z[40];
    z[69]=n<T>(353,6)*z[55];
    z[21]=z[21] + z[40] + z[69] + z[57];
    z[21]=z[6]*z[21];
    z[40]=static_cast<T>(2)- z[17];
    z[40]=z[40]*z[25];
    z[40]= - z[27] + z[40];
    z[40]=z[40]*z[28];
    z[57]= - z[8]*z[25];
    z[57]= - static_cast<T>(1)+ z[57];
    z[57]=z[57]*z[42];
    z[57]= - z[22] + z[57];
    z[71]=2*z[4];
    z[57]=z[3]*z[57]*z[71];
    z[45]=2*z[45] + z[46];
    z[45]=z[45]*z[32];
    z[45]=z[45] - z[60] - n<T>(5,3)*z[4];
    z[45]=z[6]*z[45];
    z[45]= - n<T>(2,3)*z[24] + 7*z[45];
    z[45]=z[6]*z[45];
    z[40]=z[45] + 7*z[40] + z[57];
    z[45]=npow(z[8],2);
    z[57]=z[45]*z[4];
    z[60]=z[52]*z[7];
    z[72]=z[57] + z[60];
    z[73]=z[10] + z[8];
    z[74]= - 2*z[73] - z[72];
    z[74]=z[6]*z[74];
    z[74]=2*z[47] + z[74];
    z[75]=14*z[6];
    z[74]=z[74]*z[75];
    z[31]=n<T>(353,2)*z[31] + z[74];
    z[31]=z[31]*z[32];
    z[74]=z[9] + z[8];
    z[76]=2*z[74];
    z[77]=z[76] - z[38];
    z[77]=z[3]*z[77];
    z[26]= - 4*z[26] + z[77];
    z[26]=z[26]*z[64];
    z[26]=n<T>(269,2)*z[5] + z[26];
    z[26]=z[26]*z[25];
    z[77]=z[45]*z[3];
    z[78]=2*z[8];
    z[79]= - z[78] - z[77];
    z[79]=z[79]*z[64];
    z[79]=n<T>(353,2) + z[79];
    z[79]=z[79]*z[25];
    z[79]= - n<T>(353,6)*z[7] - z[65] + z[79];
    z[79]=z[4]*z[79];
    z[26]=z[31] + z[26] + z[79];
    z[26]=z[2]*z[26];
    z[26]=4*z[40] + z[26];
    z[26]=z[2]*z[26];
    z[31]=static_cast<T>(2)+ z[51];
    z[31]=z[31]*z[36];
    z[31]= - 2*z[58] + 7*z[31];
    z[40]=4*z[10];
    z[51]=z[40] + z[60];
    z[51]=z[51]*z[70];
    z[51]= - n<T>(269,2) + z[51];
    z[51]=z[7]*z[51];
    z[51]=z[51] + z[56];
    z[56]=n<T>(1,3)*z[4];
    z[51]=z[51]*z[56];
    z[31]=4*z[31] + z[51];
    z[31]=z[4]*z[31];
    z[51]= - z[63] + n<T>(269,2)*z[3];
    z[51]=z[3]*z[51];
    z[51]=z[66] + z[51];
    z[51]=z[3]*z[51];
    z[58]=n<T>(353,2)*z[16];
    z[51]= - z[58] - z[67] + z[51];
    z[31]=n<T>(1,3)*z[51] + z[31];
    z[31]=z[4]*z[31];
    z[51]=8*z[7] + n<T>(353,2)*z[4];
    z[51]=z[4]*z[51];
    z[51]=8*z[36] + z[51];
    z[51]=z[4]*z[51];
    z[63]=n<T>(1,2)*z[6];
    z[66]=269*z[7] + 353*z[4];
    z[66]=z[66]*z[63];
    z[66]= - 8*z[24] + z[66];
    z[66]=z[6]*z[66];
    z[51]=z[66] + z[58] + z[51];
    z[51]=z[51]*z[32];
    z[26]=z[26] + z[51] + z[69] + z[31];
    z[26]=z[2]*z[26];
    z[31]=n<T>(7,3)*z[10] + z[15];
    z[31]=z[5]*z[31];
    z[31]= - static_cast<T>(4)+ z[31];
    z[31]=z[31]*z[48];
    z[51]= - z[61] + z[38];
    z[51]=z[51]*z[43];
    z[51]= - n<T>(269,2) + z[51];
    z[51]=z[51]*z[59];
    z[58]=z[9] - z[10];
    z[66]=n<T>(1,3)*z[7];
    z[67]=z[58]*z[66];
    z[67]= - static_cast<T>(1)+ z[67];
    z[67]=z[67]*z[50];
    z[31]=z[67] + z[31] + z[51];
    z[31]=z[4]*z[31];
    z[20]=static_cast<T>(2)- z[20];
    z[20]=z[20]*z[34];
    z[20]= - z[23] - n<T>(4,3)*z[18] + z[20];
    z[20]=28*z[20] + z[31];
    z[20]=z[4]*z[20];
    z[20]= - z[69] + z[20];
    z[20]=z[4]*z[20];
    z[23]=2*z[17];
    z[31]=static_cast<T>(1)+ z[23];
    z[31]=z[3]*z[31];
    z[31]= - 10*z[5] + z[31];
    z[31]=z[31]*z[28];
    z[34]=z[38] + z[9];
    z[51]= - z[8] + z[34];
    z[51]=z[3]*z[51];
    z[51]= - z[23] + z[51];
    z[51]=z[51]*z[64];
    z[51]= - n<T>(353,2)*z[5] + z[51];
    z[51]=z[7]*z[3]*z[51];
    z[31]=14*z[31] + z[51];
    z[31]=z[7]*z[31];
    z[31]= - n<T>(353,2)*z[55] + z[31];
    z[31]=z[31]*z[66];
    z[14]=z[14] + z[26] + z[21] + z[31] + z[20];
    z[14]=z[1]*z[14];
    z[20]=npow(z[10],3);
    z[21]=z[20]*z[5];
    z[26]=2*z[52];
    z[21]=z[21] - z[26];
    z[31]=n<T>(14,3)*z[5];
    z[21]=z[21]*z[31];
    z[51]=n<T>(127,2)*z[10];
    z[21]=z[21] - z[51];
    z[21]=z[21]*z[5];
    z[21]=z[21] + n<T>(409,3);
    z[55]=z[20]*z[7];
    z[67]= - z[26] - z[55];
    z[69]=n<T>(14,3)*z[7];
    z[67]=z[67]*z[69];
    z[51]=z[51] + z[67];
    z[51]=z[7]*z[51];
    z[51]=z[51] + n<T>(409,6)*z[41] + z[21];
    z[51]=z[4]*z[51];
    z[67]=5*z[10];
    z[79]= - z[67] + n<T>(4,3)*z[53];
    z[80]=7*z[5];
    z[79]=z[79]*z[80];
    z[79]=z[79] - n<T>(32,3);
    z[80]= - z[5]*z[79];
    z[80]=z[80] - n<T>(88,3)*z[3];
    z[81]= - z[67] - z[60];
    z[81]=z[81]*z[70];
    z[81]= - n<T>(169,2) + z[81];
    z[81]=z[81]*z[66];
    z[51]=z[51] + 2*z[80] + z[81];
    z[51]=z[4]*z[51];
    z[80]= - static_cast<T>(80)+ n<T>(269,6)*z[29];
    z[80]=z[80]*z[33];
    z[81]=n<T>(409,2)*z[44];
    z[82]=z[81] + 176;
    z[35]=z[82]*z[35];
    z[82]=n<T>(1,6)*z[3];
    z[83]= - static_cast<T>(113)+ 269*z[41];
    z[83]=z[83]*z[82];
    z[84]=54*z[5];
    z[83]=z[84] + z[83];
    z[83]=z[3]*z[83];
    z[51]=z[51] + z[35] - z[80] + z[83];
    z[51]=z[4]*z[51];
    z[83]=z[10] - z[8];
    z[85]=z[83]*z[54];
    z[86]=npow(z[8],3);
    z[87]=z[4]*z[86];
    z[55]=z[87] + z[55] + z[45] - z[85];
    z[55]=z[6]*z[55];
    z[55]=z[55] + z[57] - z[54] - z[60];
    z[55]=z[55]*z[75];
    z[47]= - n<T>(409,2)*z[47] + z[55];
    z[47]=z[47]*z[32];
    z[55]=npow(z[9],3);
    z[87]=z[55]*z[5];
    z[88]=z[78] + z[9];
    z[88]=z[9]*z[88];
    z[88]= - z[87] + z[45] + z[88];
    z[88]=z[3]*z[88];
    z[89]= - 2*z[38] + z[74];
    z[88]=2*z[89] + z[88];
    z[88]=z[3]*z[88];
    z[89]= - static_cast<T>(127)+ n<T>(269,3)*z[17];
    z[88]=n<T>(1,2)*z[89] + n<T>(14,3)*z[88];
    z[88]=z[3]*z[88];
    z[89]= - z[3]*z[86];
    z[89]=z[45] + z[89];
    z[89]=z[89]*z[64];
    z[89]=n<T>(409,2)*z[8] + z[89];
    z[89]=z[3]*z[89];
    z[90]=n<T>(1,2)*z[29];
    z[91]=z[90] - 1;
    z[91]=409*z[91];
    z[81]=z[81] - z[91] + z[89];
    z[81]=z[81]*z[56];
    z[47]=z[47] + z[81] + z[65] + z[88];
    z[47]=z[2]*z[47];
    z[65]=z[76] + z[38];
    z[65]=z[3]*z[65];
    z[65]=z[65] + static_cast<T>(8)- z[17];
    z[65]=z[65]*z[64];
    z[65]= - n<T>(113,2)*z[5] + z[65];
    z[65]=z[3]*z[65];
    z[76]= - z[8] - 4*z[77];
    z[42]=z[76]*z[42];
    z[42]=static_cast<T>(88)+ z[42];
    z[76]=2*z[3];
    z[42]=z[42]*z[76];
    z[42]= - 176*z[7] - 815*z[5] + z[42];
    z[42]=z[4]*z[42];
    z[42]=z[65] + z[42];
    z[65]= - z[60] + z[83];
    z[57]=2*z[65] - z[57];
    z[57]=z[57]*z[32];
    z[57]=z[57] + z[46] + static_cast<T>(2)+ n<T>(5,3)*z[44];
    z[57]=z[57]*z[75];
    z[57]=z[57] + n<T>(176,3)*z[7] - n<T>(19,2)*z[4];
    z[57]=z[6]*z[57];
    z[42]=z[47] + n<T>(1,3)*z[42] + z[57];
    z[42]=z[2]*z[42];
    z[47]=n<T>(409,2)*z[17];
    z[57]=z[47] - 176;
    z[65]=n<T>(1,3)*z[33];
    z[57]=z[57]*z[65];
    z[65]=n<T>(1,2)*z[3];
    z[76]=n<T>(409,3)*z[17];
    z[81]= - static_cast<T>(127)+ z[76];
    z[81]=z[81]*z[65];
    z[88]=n<T>(176,3)*z[5];
    z[81]=z[88] + z[81];
    z[81]=z[3]*z[81];
    z[81]=z[57] + z[81];
    z[81]=z[3]*z[81];
    z[89]=n<T>(409,3)*z[46];
    z[92]= - z[89] - n<T>(409,3) - 127*z[44];
    z[92]=z[6]*z[92];
    z[92]=z[92] - n<T>(169,3)*z[7] - 19*z[4];
    z[92]=z[92]*z[63];
    z[89]=static_cast<T>(19)- z[89];
    z[89]=z[4]*z[89];
    z[89]=54*z[7] + n<T>(1,2)*z[89];
    z[89]=z[4]*z[89];
    z[35]=z[92] - z[35] + z[89];
    z[35]=z[6]*z[35];
    z[89]=127*z[18];
    z[35]=z[42] + z[35] + z[51] + z[89] + z[81];
    z[35]=z[2]*z[35];
    z[42]=z[54] + z[30];
    z[42]=z[9]*z[42];
    z[51]=n<T>(2,3)*z[52];
    z[42]=z[51] + z[42];
    z[42]=z[42]*z[48];
    z[42]=n<T>(409,6) + z[42];
    z[42]=z[5]*z[42];
    z[81]=z[37] + z[87];
    z[81]=z[81]*z[43];
    z[92]=n<T>(409,2)*z[9];
    z[81]= - z[92] + z[81];
    z[59]=z[81]*z[59];
    z[21]= - n<T>(409,6)*z[39] + z[21];
    z[21]=z[7]*z[21];
    z[81]=z[7]*z[9];
    z[93]=z[81] - 1;
    z[93]=z[93]*z[4];
    z[21]= - n<T>(409,6)*z[93] + z[21] + z[42] + z[59];
    z[21]=z[6]*z[21];
    z[23]=static_cast<T>(3)+ z[23];
    z[23]=z[23]*z[48];
    z[42]=n<T>(1,3)*z[38];
    z[59]= - z[9] + z[42];
    z[59]=z[59]*z[43];
    z[59]= - n<T>(19,2) + z[59];
    z[59]=z[59]*z[49];
    z[79]=z[79]*z[27];
    z[79]=z[79] - n<T>(19,2)*z[3];
    z[79]=z[7]*z[79];
    z[21]=z[21] - n<T>(176,3)*z[24] + z[79] + z[23] + z[59];
    z[21]=z[6]*z[21];
    z[23]=z[37]*z[25];
    z[23]= - z[9] + z[23];
    z[23]=z[23]*z[64];
    z[23]=n<T>(19,2) + z[23];
    z[23]=z[3]*z[23];
    z[24]=z[3]*z[55];
    z[24]=z[37] + z[24];
    z[24]=z[24]*z[64];
    z[24]= - z[92] + z[24];
    z[24]=z[3]*z[24];
    z[24]= - z[91] + z[24];
    z[24]=z[24]*z[66];
    z[23]=z[24] + n<T>(815,3)*z[5] + z[23];
    z[23]=z[7]*z[23];
    z[24]= - static_cast<T>(19)- n<T>(409,3)*z[39];
    z[24]=z[24]*z[65];
    z[24]=z[84] + z[24];
    z[24]=z[3]*z[24];
    z[23]=z[23] - z[80] + z[24];
    z[23]=z[7]*z[23];
    z[24]=z[9] + z[68];
    z[24]=z[24]*z[70];
    z[24]= - n<T>(113,2) + z[24];
    z[24]=z[7]*z[24];
    z[59]=4*z[37];
    z[79]=z[7]*z[55];
    z[79]= - z[59] + z[79];
    z[79]=z[79]*z[70];
    z[80]=n<T>(269,2)*z[9];
    z[79]= - z[80] + z[79];
    z[79]=z[7]*z[79];
    z[79]=n<T>(353,2) + z[79];
    z[79]=z[4]*z[79];
    z[24]=z[24] + z[79];
    z[24]=z[4]*z[24];
    z[79]=static_cast<T>(176)- n<T>(409,2)*z[81];
    z[36]=z[79]*z[36];
    z[24]=z[36] + z[24];
    z[24]=z[24]*z[56];
    z[36]=z[76] - 19;
    z[36]=z[36]*z[33];
    z[76]= - z[3]*z[76];
    z[76]= - 19*z[5] + z[76];
    z[76]=z[3]*z[76];
    z[76]= - z[36] + z[76];
    z[76]=z[3]*z[76];
    z[76]=n<T>(353,3)*z[18] + z[76];
    z[21]=z[21] + z[24] + n<T>(1,2)*z[76] + z[23];
    z[21]=z[6]*z[21];
    z[23]=static_cast<T>(28)+ z[47];
    z[23]=z[23]*z[25];
    z[24]=n<T>(19,2)*z[5];
    z[23]=z[24] + z[23];
    z[23]=z[3]*z[23];
    z[23]=n<T>(1,2)*z[36] + z[23];
    z[23]=z[3]*z[23];
    z[36]=z[30]*z[8];
    z[36]=z[36] + z[45];
    z[47]= - z[55]*z[22];
    z[47]=z[47] + z[36];
    z[47]=z[3]*z[47];
    z[42]=z[47] + z[42] - z[8] + z[30];
    z[42]=z[42]*z[64];
    z[47]=n<T>(409,6)*z[17];
    z[42]=z[47] + z[42];
    z[42]=z[3]*z[42];
    z[42]= - n<T>(409,6)*z[5] + z[42];
    z[42]=z[7]*z[42];
    z[55]=5*z[8];
    z[76]= - z[55] + z[38];
    z[76]=z[76]*z[25];
    z[76]=z[76] + n<T>(4,3) + z[17];
    z[76]=z[76]*z[64];
    z[24]= - z[24] + z[76];
    z[24]=z[3]*z[24];
    z[24]=z[24] + z[42];
    z[24]=z[7]*z[24];
    z[18]=z[24] - n<T>(353,6)*z[18] + z[23];
    z[18]=z[7]*z[18];
    z[23]=7*z[10];
    z[24]=z[23] + z[15];
    z[24]=z[9]*z[24];
    z[42]=4*z[52];
    z[24]=z[42] + z[24];
    z[24]=z[24]*z[22];
    z[76]=n<T>(4,3)*z[10];
    z[79]= - z[76] - z[9];
    z[24]=2*z[79] + z[24];
    z[24]=z[24]*z[43];
    z[24]= - n<T>(353,3) + z[24];
    z[24]=z[5]*z[24];
    z[59]= - z[59] + z[87];
    z[59]=z[59]*z[43];
    z[59]= - z[80] + z[59];
    z[59]=z[59]*z[22];
    z[59]=static_cast<T>(127)+ z[59];
    z[59]=z[3]*z[59];
    z[79]= - z[54] + z[9];
    z[79]=z[9]*z[79];
    z[79]=z[52] + z[79];
    z[79]=z[7]*z[79];
    z[84]=4*z[58];
    z[79]= - z[84] + z[79];
    z[79]=z[79]*z[69];
    z[79]= - n<T>(99,2) + z[79];
    z[79]=z[7]*z[79];
    z[24]=z[79] + z[24] + z[59];
    z[24]=z[4]*z[24];
    z[59]= - z[67] - z[61];
    z[59]=z[5]*z[59];
    z[59]=static_cast<T>(16)+ z[59];
    z[48]=z[59]*z[48];
    z[34]=z[34]*z[43];
    z[34]= - n<T>(113,2) + z[34];
    z[34]=z[34]*z[49];
    z[34]=z[48] + z[34];
    z[24]=z[24] + n<T>(1,3)*z[34] + z[50];
    z[24]=z[4]*z[24];
    z[34]=static_cast<T>(127)- z[47];
    z[34]=z[3]*z[34];
    z[34]= - z[88] + z[34];
    z[34]=z[3]*z[34];
    z[34]= - z[57] + z[34];
    z[34]=z[3]*z[34];
    z[16]=z[24] - n<T>(409,6)*z[16] - z[89] + z[34];
    z[16]=z[4]*z[16];
    z[14]=z[14] + z[35] + z[21] + z[18] + z[16];
    z[14]=z[1]*z[14];
    z[16]=z[10] + z[30];
    z[16]=z[9]*z[16];
    z[16]=z[51] + z[16];
    z[16]=z[5]*z[16];
    z[16]=4*z[16] + z[10] + z[9];
    z[16]=z[16]*z[43];
    z[16]= - n<T>(437,3) + z[16];
    z[16]=z[5]*z[16];
    z[18]=7*z[38];
    z[21]= - 41*z[9] + z[18];
    z[21]=z[3]*z[21]*z[27];
    z[24]=z[53] - z[10];
    z[24]=z[24]*z[43];
    z[34]= - z[24] - 213*z[39];
    z[34]=z[7]*z[34];
    z[35]=z[67] + z[15];
    z[35]=z[9]*z[35];
    z[35]=8*z[52] + z[35];
    z[35]=z[9]*z[35];
    z[35]=4*z[20] + z[35];
    z[35]=z[5]*z[35];
    z[35]= - z[26] + z[35];
    z[35]=z[35]*z[43];
    z[47]= - n<T>(1,2)*z[10] - z[9];
    z[35]=437*z[47] + z[35];
    z[35]=z[5]*z[35];
    z[35]= - n<T>(437,2) + z[35];
    z[35]=z[35]*z[32];
    z[16]=z[35] - 213*z[93] + z[34] + z[16] + z[21];
    z[16]=z[6]*z[16];
    z[21]=z[9] + z[62];
    z[21]=z[21]*z[64];
    z[34]=z[29] - 2;
    z[34]=213*z[34];
    z[21]=z[34] + z[21];
    z[21]=z[7]*z[21];
    z[35]= - n<T>(193,3) + 54*z[29];
    z[35]=z[35]*z[5];
    z[47]= - n<T>(2273,6) - 82*z[39];
    z[47]=z[3]*z[47];
    z[21]=z[21] + z[35] + z[47];
    z[21]=z[7]*z[21];
    z[47]=155*z[10];
    z[48]=npow(z[11],2);
    z[49]= - z[48]*z[47];
    z[49]= - 187*z[11] + z[49];
    z[50]=127*z[9];
    z[53]= - n<T>(325,6)*z[10] - z[50];
    z[53]=z[5]*z[53];
    z[53]= - static_cast<T>(9)+ z[53];
    z[53]=z[5]*z[53];
    z[49]=n<T>(2,3)*z[49] + z[53];
    z[49]=z[5]*z[49];
    z[53]=14*z[17];
    z[57]= - n<T>(2273,6) + z[53];
    z[57]=z[5]*z[57];
    z[59]=213*z[17];
    z[61]= - z[3]*z[59];
    z[57]=z[57] + z[61];
    z[57]=z[3]*z[57];
    z[61]=27*z[9];
    z[62]=z[61] + 7*z[68];
    z[62]=z[7]*z[62];
    z[62]= - static_cast<T>(34)+ z[62];
    z[62]=z[62]*z[71];
    z[67]=z[9]*z[70];
    z[67]=n<T>(911,6) + z[67];
    z[67]=z[7]*z[67];
    z[62]=z[67] + z[62];
    z[62]=z[4]*z[62];
    z[67]=npow(z[13],2);
    z[68]=n<T>(310,3)*z[67];
    z[16]=z[16] + z[62] + z[21] + z[57] - z[68] + z[49];
    z[16]=z[6]*z[16];
    z[21]=z[9]*z[74];
    z[21]=z[45] + z[21];
    z[21]=z[9]*z[21];
    z[21]=z[86] + z[21];
    z[21]=z[21]*z[25];
    z[21]=z[21] - n<T>(1,3)*z[45] + z[37];
    z[21]=z[21]*z[64];
    z[49]= - 437*z[8] - 325*z[9];
    z[21]=n<T>(1,6)*z[49] + z[21];
    z[21]=z[3]*z[21];
    z[49]=z[78] - z[10];
    z[49]=z[49]*z[10];
    z[57]= - z[45] + z[49];
    z[62]=z[40]*z[83];
    z[62]=z[62] + z[45];
    z[71]= - z[10]*z[62];
    z[71]= - z[86] + z[71];
    z[71]=z[6]*z[71];
    z[57]=2*z[57] + z[71];
    z[57]=z[57]*z[75];
    z[57]=n<T>(437,2)*z[73] + z[57];
    z[57]=z[57]*z[32];
    z[21]=z[57] - n<T>(127,2) + z[21];
    z[21]=z[2]*z[21];
    z[57]=z[45] - n<T>(1,3)*z[37];
    z[57]=z[3]*z[57];
    z[57]=z[57] - n<T>(1,3)*z[74] - z[38];
    z[57]=z[57]*z[64];
    z[57]=z[57] - n<T>(37,2) + 54*z[17];
    z[57]=z[3]*z[57];
    z[71]=z[8] - z[77];
    z[71]=z[71]*z[64];
    z[74]=213*z[44];
    z[34]=z[74] - z[34] + z[71];
    z[34]=z[4]*z[34];
    z[62]=z[62]*z[32];
    z[62]=z[62] + n<T>(1,3)*z[83] - z[72];
    z[62]=z[62]*z[75];
    z[71]=82*z[44];
    z[62]=z[62] - 14*z[46] - n<T>(55,6) - z[71];
    z[62]=z[6]*z[62];
    z[21]=z[21] + z[62] + z[34] + 281*z[5] + z[57];
    z[21]=z[2]*z[21];
    z[34]=z[10] + z[60];
    z[34]=z[34]*z[70];
    z[24]=z[34] + z[24] - 213*z[41];
    z[24]=z[4]*z[24];
    z[34]= - n<T>(911,6) + 54*z[41];
    z[34]=z[3]*z[34];
    z[57]=n<T>(503,6) - z[71];
    z[57]=z[7]*z[57];
    z[24]=z[24] + z[57] + z[35] + z[34];
    z[24]=z[4]*z[24];
    z[34]=z[6]*z[73];
    z[34]=n<T>(437,6)*z[34] - 82*z[46] + n<T>(149,3) + 14*z[44];
    z[34]=z[6]*z[34];
    z[35]=z[47]*z[67];
    z[57]=z[35] + 32*z[13];
    z[60]= - n<T>(503,6) + z[74];
    z[60]=z[7]*z[60];
    z[62]= - n<T>(2273,6) + 213*z[46];
    z[62]=z[4]*z[62];
    z[34]=z[34] + z[62] + n<T>(2,3)*z[57] + z[60];
    z[34]=z[6]*z[34];
    z[60]=325*z[8];
    z[62]=437*z[9];
    z[71]= - z[60] - z[62];
    z[71]=z[71]*z[82];
    z[71]=z[71] - n<T>(37,2) + z[53];
    z[71]=z[3]*z[71];
    z[72]=npow(z[12],2);
    z[73]=z[72]*z[9];
    z[74]=155*z[73];
    z[77]=32*z[12] - z[74];
    z[79]= - n<T>(911,6) - z[59];
    z[79]=z[5]*z[79];
    z[71]=z[71] + n<T>(2,3)*z[77] + z[79];
    z[71]=z[3]*z[71];
    z[21]=z[21] + z[34] + z[24] + z[71] - n<T>(310,3)*z[72] - 281*z[33];
    z[21]=z[2]*z[21];
    z[24]=n<T>(1,3)*z[52];
    z[34]=z[9] + n<T>(1,3)*z[10];
    z[71]= - z[9]*z[34];
    z[71]= - z[24] + z[71];
    z[15]=z[10] + z[15];
    z[15]=z[9]*z[15];
    z[15]=z[42] + z[15];
    z[15]=z[9]*z[15];
    z[15]=z[20] + z[15];
    z[15]=z[15]*z[22];
    z[15]=2*z[71] + z[15];
    z[15]=z[15]*z[43];
    z[42]=n<T>(409,2)*z[10];
    z[71]= - z[42] - 353*z[9];
    z[15]=n<T>(1,3)*z[71] + z[15];
    z[15]=z[5]*z[15];
    z[19]= - z[9]*z[19];
    z[19]= - z[52] + z[19];
    z[71]=z[9]*z[58];
    z[71]=z[52] + z[71];
    z[71]=z[9]*z[71];
    z[71]= - z[20] + z[71];
    z[71]=z[7]*z[71];
    z[19]=2*z[19] + z[71];
    z[19]=z[19]*z[69];
    z[69]=n<T>(409,3)*z[10];
    z[71]=z[69] - 99*z[9];
    z[19]=n<T>(1,2)*z[71] + z[19];
    z[19]=z[7]*z[19];
    z[15]=z[19] + n<T>(818,3) + z[15];
    z[15]=z[4]*z[15];
    z[19]=z[17]*z[84];
    z[19]=z[19] + z[23] - 5*z[9];
    z[19]=z[19]*z[31];
    z[19]= - static_cast<T>(59)+ z[19];
    z[19]=z[5]*z[19];
    z[18]=z[61] + z[18];
    z[18]=z[18]*z[27];
    z[18]= - static_cast<T>(281)+ z[18];
    z[18]=z[3]*z[18];
    z[23]=z[58]*z[81];
    z[23]=n<T>(2,3)*z[23] - z[34];
    z[23]=z[23]*z[70];
    z[23]= - static_cast<T>(59)+ z[23];
    z[23]=z[7]*z[23];
    z[15]=z[15] + z[23] + z[19] + z[18];
    z[15]=z[4]*z[15];
    z[18]=z[72]*z[47];
    z[18]=187*z[12] + z[18];
    z[19]=n<T>(437,6)*z[10];
    z[23]= - z[19] - z[50];
    z[23]=z[5]*z[23];
    z[23]=static_cast<T>(150)+ z[23];
    z[23]=z[5]*z[23];
    z[18]=n<T>(2,3)*z[18] + z[23];
    z[18]=z[5]*z[18];
    z[23]=n<T>(911,6) + z[53];
    z[23]=z[5]*z[23];
    z[27]=static_cast<T>(281)- z[59];
    z[27]=z[3]*z[27];
    z[23]=z[23] + z[27];
    z[23]=z[3]*z[23];
    z[27]=z[7]*z[58];
    z[27]=static_cast<T>(260)- n<T>(409,2)*z[27];
    z[27]=z[7]*z[27];
    z[27]=2*z[57] + z[27];
    z[27]=z[27]*z[66];
    z[15]=z[15] + z[27] + z[23] + z[68] + z[18];
    z[15]=z[4]*z[15];
    z[18]=10*z[45];
    z[23]=4*z[8];
    z[27]=z[23] - z[9];
    z[27]=z[9]*z[27];
    z[27]=z[18] + z[27];
    z[27]=z[27]*z[25];
    z[31]= - 8*z[8] + z[9];
    z[27]=z[27] + n<T>(2,3)*z[31] - z[38];
    z[27]=z[27]*z[64];
    z[27]=z[27] + n<T>(29,6) - 82*z[17];
    z[27]=z[3]*z[27];
    z[31]=z[9]*z[8];
    z[34]=2*z[45] + z[31];
    z[38]=4*z[45];
    z[57]=z[9] - z[8];
    z[57]=z[9]*z[57];
    z[57]= - z[38] + z[57];
    z[57]=z[9]*z[57];
    z[57]= - 4*z[86] + z[57];
    z[57]=z[3]*z[57];
    z[34]=4*z[34] + z[57];
    z[34]=z[34]*z[64];
    z[57]=z[60] - z[62];
    z[34]=n<T>(1,2)*z[57] + z[34];
    z[34]=z[3]*z[34];
    z[34]= - n<T>(409,2) + z[34];
    z[34]=z[34]*z[66];
    z[27]=z[34] + 213*z[5] + z[27];
    z[27]=z[7]*z[27];
    z[34]=71*z[8] - n<T>(437,3)*z[9];
    z[34]=z[34]*z[65];
    z[34]=z[34] - n<T>(325,3) + z[53];
    z[34]=z[3]*z[34];
    z[53]=z[48]*z[8];
    z[57]=155*z[53];
    z[58]= - 32*z[11] + z[57];
    z[59]=n<T>(2273,6) - z[59];
    z[59]=z[5]*z[59];
    z[34]=z[34] + n<T>(2,3)*z[58] + z[59];
    z[34]=z[3]*z[34];
    z[27]=z[27] + 68*z[33] + z[34];
    z[27]=z[7]*z[27];
    z[28]= - n<T>(310,3)*z[48] - 127*z[28];
    z[28]=z[3]*z[28];
    z[14]=z[14] + z[21] + z[16] + z[15] + z[28] + z[27];
    z[14]=z[1]*z[14];
    z[15]= - z[26] + z[37];
    z[16]=z[9]*z[10];
    z[21]=n<T>(8,3)*z[52] + z[16];
    z[21]=z[9]*z[21];
    z[27]=2*z[20];
    z[21]=z[27] + z[21];
    z[21]=z[5]*z[21];
    z[15]=n<T>(1,3)*z[15] + z[21];
    z[15]=z[15]*z[43];
    z[21]= - z[26] - z[16];
    z[21]=z[9]*z[21];
    z[21]= - z[27] + z[21];
    z[26]=z[10]*z[30];
    z[26]=z[52] + z[26];
    z[26]=z[9]*z[26];
    z[26]=n<T>(4,3)*z[20] + z[26];
    z[26]=z[9]*z[26];
    z[27]=npow(z[10],4);
    z[26]=n<T>(2,3)*z[27] + z[26];
    z[26]=z[5]*z[26];
    z[21]=n<T>(1,3)*z[21] + z[26];
    z[21]=z[21]*z[43];
    z[26]= - z[52] - z[16];
    z[21]=n<T>(437,6)*z[26] + z[21];
    z[21]=z[6]*z[21];
    z[15]=z[21] + z[15] - n<T>(1013,6)*z[10] - 96*z[9];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(87,2) + z[15];
    z[15]=z[6]*z[15];
    z[21]=n<T>(155,3)*z[10];
    z[26]= - z[48]*z[21];
    z[26]=9*z[11] + z[26];
    z[26]=z[26]*z[54];
    z[27]= - 269*z[52] - 325*z[16];
    z[27]=z[5]*z[27];
    z[28]=28*z[9];
    z[27]=n<T>(1,2)*z[27] - n<T>(83,2)*z[10] + z[28];
    z[27]=z[27]*z[22];
    z[26]=z[27] - n<T>(377,2) + z[26];
    z[26]=z[5]*z[26];
    z[27]= - z[67] + z[48];
    z[27]=z[27]*z[21];
    z[33]=41*z[13];
    z[34]=z[33] + n<T>(32,3)*z[11];
    z[27]=2*z[34] + z[27];
    z[34]=n<T>(973,2)*z[17];
    z[58]= - static_cast<T>(221)- z[34];
    z[58]=z[58]*z[25];
    z[59]=z[90] + 1;
    z[39]= - n<T>(1,2)*z[39] - z[59];
    z[39]=z[7]*z[39];
    z[60]=static_cast<T>(1415)- 973*z[81];
    z[60]=z[4]*z[60];
    z[15]=z[15] + n<T>(1,6)*z[60] + n<T>(973,3)*z[39] + z[58] + 2*z[27] + z[26];
    z[15]=z[6]*z[15];
    z[26]=z[78] - z[9];
    z[27]=z[26]*z[30];
    z[39]=z[8]*z[37];
    z[39]=z[86] + z[39];
    z[39]=z[39]*z[25];
    z[27]=z[39] - z[45] + z[27];
    z[27]=z[64]*z[9]*z[27];
    z[28]= - n<T>(325,2)*z[8] - z[28];
    z[28]=z[28]*z[30];
    z[27]=z[28] + z[27];
    z[27]=z[3]*z[27];
    z[28]= - z[45] + n<T>(2,3)*z[49];
    z[28]=z[28]*z[10];
    z[39]=n<T>(1,3)*z[86];
    z[28]=z[28] + z[39];
    z[49]=z[6]*z[10];
    z[58]=z[49] + 1;
    z[28]=z[75]*z[28]*z[58];
    z[19]=z[83]*z[19];
    z[19]=z[19] + z[28];
    z[19]=z[6]*z[19];
    z[28]=n<T>(437,3)*z[83] + z[50];
    z[19]=z[19] + n<T>(1,2)*z[28] + z[27];
    z[19]=z[2]*z[19];
    z[26]= - z[9]*z[26];
    z[26]=z[45] + z[26];
    z[27]=n<T>(2,3)*z[45] - z[31];
    z[27]=z[9]*z[27];
    z[27]= - z[39] + z[27];
    z[27]=z[3]*z[27];
    z[26]=n<T>(4,3)*z[26] + z[27];
    z[26]=z[26]*z[64];
    z[27]=n<T>(269,2)*z[8] - 176*z[9];
    z[26]=n<T>(1,3)*z[27] + z[26];
    z[26]=z[3]*z[26];
    z[27]=z[85] + z[45];
    z[28]=static_cast<T>(2)+ z[49];
    z[27]=z[75]*z[27]*z[28];
    z[27]=n<T>(139,2)*z[10] + z[27];
    z[27]=z[27]*z[32];
    z[19]=z[19] + z[27] - n<T>(581,3) + z[26];
    z[19]=z[2]*z[19];
    z[26]= - static_cast<T>(139)+ 437*z[49];
    z[26]=z[63]*z[83]*z[26];
    z[27]=z[46] + z[44];
    z[28]= - z[35] + 337*z[13];
    z[28]=z[28]*z[54];
    z[26]=z[26] + n<T>(1283,2) - z[28] - n<T>(973,2)*z[27];
    z[26]=z[26]*z[32];
    z[27]=337*z[12] + z[74];
    z[27]=z[27]*z[30];
    z[27]=static_cast<T>(88)+ z[27];
    z[39]= - 28*z[45] - n<T>(325,2)*z[31];
    z[39]=z[3]*z[39];
    z[39]=z[39] - 176*z[8] + z[80];
    z[39]=z[39]*z[25];
    z[46]=n<T>(973,6)*z[17];
    z[27]=z[39] + 2*z[27] + z[46];
    z[27]=z[3]*z[27];
    z[39]=z[67]*z[21];
    z[49]=n<T>(155,3)*z[73];
    z[39]=199*z[5] + z[49] + z[39] - n<T>(91,3)*z[13] + 82*z[12];
    z[44]=z[41] + z[44];
    z[44]= - z[59] + n<T>(1,2)*z[44];
    z[44]=z[4]*z[44];
    z[19]=z[19] + z[26] + n<T>(973,3)*z[44] + n<T>(221,3)*z[7] + 2*z[39] + z[27]
   ;
    z[19]=z[2]*z[19];
    z[26]=z[23] + z[9];
    z[26]=z[9]*z[26];
    z[26]=z[38] + z[26];
    z[27]= - n<T>(8,3)*z[45] - z[31];
    z[27]=z[9]*z[27];
    z[38]=2*z[86];
    z[27]= - z[38] + z[27];
    z[27]=z[3]*z[27];
    z[26]=n<T>(4,3)*z[26] + z[27];
    z[26]=z[26]*z[64];
    z[26]=z[26] + n<T>(207,2)*z[8] + n<T>(176,3)*z[9];
    z[26]=z[3]*z[26];
    z[27]= - z[55] - z[9];
    z[27]=z[9]*z[27];
    z[18]= - z[18] + z[27];
    z[18]=z[18]*z[30];
    z[27]=z[9]*z[36];
    z[27]=n<T>(4,3)*z[86] + z[27];
    z[27]=z[9]*z[27];
    z[36]=npow(z[8],4);
    z[27]=n<T>(2,3)*z[36] + z[27];
    z[27]=z[3]*z[27];
    z[18]=z[27] - z[38] + z[18];
    z[18]=z[18]*z[64];
    z[27]= - n<T>(71,2)*z[8] + n<T>(28,3)*z[9];
    z[27]=z[9]*z[27];
    z[18]=z[18] - n<T>(269,6)*z[45] + z[27];
    z[18]=z[3]*z[18];
    z[27]=395*z[8] + n<T>(437,2)*z[9];
    z[18]=n<T>(1,3)*z[27] + z[18];
    z[18]=z[7]*z[18];
    z[18]=z[18] - n<T>(581,3) + z[26];
    z[18]=z[7]*z[18];
    z[26]=z[13] + z[11];
    z[26]= - z[35] + 91*z[26] + z[57];
    z[26]=2*z[26] - n<T>(1415,2)*z[5];
    z[27]= - 337*z[11] - z[57];
    z[27]=z[8]*z[27];
    z[27]= - static_cast<T>(796)+ z[27];
    z[27]=2*z[27] + z[34];
    z[31]= - 269*z[45] - 325*z[31];
    z[31]=z[31]*z[25];
    z[31]=z[31] + 207*z[8] + n<T>(269,3)*z[9];
    z[31]=z[31]*z[65];
    z[27]=n<T>(1,3)*z[27] + z[31];
    z[27]=z[3]*z[27];
    z[18]=z[18] + n<T>(1,3)*z[26] + z[27];
    z[18]=z[7]*z[18];
    z[26]=z[54]*z[37];
    z[27]=z[37]*z[10];
    z[27]=z[27] + z[20];
    z[17]=z[27]*z[17];
    z[17]= - z[26] + z[17];
    z[17]=z[17]*z[43];
    z[31]=n<T>(409,2)*z[16];
    z[17]= - z[31] + z[17];
    z[17]=z[5]*z[17];
    z[27]= - z[27]*z[81];
    z[26]=z[26] + z[27];
    z[26]=z[26]*z[70];
    z[26]=z[31] + z[26];
    z[26]=z[7]*z[26];
    z[17]=z[17] + z[26];
    z[17]=z[17]*z[56];
    z[26]=z[40] - z[9];
    z[26]=z[26]*z[9];
    z[27]=z[52] - z[26];
    z[31]= - z[51] + z[16];
    z[31]=z[9]*z[31];
    z[20]=n<T>(1,3)*z[20];
    z[31]=z[20] + z[31];
    z[31]=z[5]*z[31];
    z[27]=n<T>(1,3)*z[27] + z[31];
    z[27]=z[27]*z[43];
    z[31]= - z[42] + 260*z[9];
    z[27]=n<T>(1,3)*z[31] + z[27];
    z[27]=z[5]*z[27];
    z[26]=z[52] + z[26];
    z[31]= - z[10] + z[30];
    z[31]=z[9]*z[31];
    z[24]=z[24] + z[31];
    z[24]=z[9]*z[24];
    z[20]= - z[20] + z[24];
    z[20]=z[7]*z[20];
    z[20]=n<T>(1,3)*z[26] + z[20];
    z[20]=z[20]*z[70];
    z[24]=z[69] + 37*z[9];
    z[20]=n<T>(1,2)*z[24] + z[20];
    z[20]=z[7]*z[20];
    z[17]=z[17] + z[20] - n<T>(460,3) + z[27];
    z[17]=z[4]*z[17];
    z[20]=z[72]*z[21];
    z[20]= - 9*z[12] + z[20];
    z[20]=z[20]*z[54];
    z[16]= - 28*z[52] - n<T>(325,2)*z[16];
    z[16]=z[5]*z[16];
    z[24]=7*z[9];
    z[26]=58*z[10] + z[24];
    z[16]=4*z[26] + z[16];
    z[16]=z[16]*z[22];
    z[16]=z[16] + static_cast<T>(17)+ z[20];
    z[16]=z[5]*z[16];
    z[20]=n<T>(239,2) - z[28];
    z[22]=n<T>(353,6)*z[10] + 14*z[9];
    z[22]=z[9]*z[22];
    z[22]=n<T>(14,3)*z[52] + z[22];
    z[22]=z[7]*z[22];
    z[24]= - 65*z[10] - z[24];
    z[22]=n<T>(4,3)*z[24] + z[22];
    z[22]=z[7]*z[22];
    z[20]=n<T>(1,3)*z[20] + z[22];
    z[20]=z[7]*z[20];
    z[22]=z[67] - z[72];
    z[21]=z[22]*z[21];
    z[22]= - z[33] - n<T>(32,3)*z[12];
    z[21]=2*z[22] + z[21];
    z[22]=static_cast<T>(398)- z[46];
    z[22]=z[3]*z[22];
    z[16]=z[17] + z[20] + z[22] + 2*z[21] + z[16];
    z[16]=z[4]*z[16];
    z[17]=z[72] + z[48];
    z[20]= - z[17]*z[47];
    z[21]=z[11] + z[12];
    z[20]= - 64*z[21] + z[20];
    z[22]= - static_cast<T>(144)- 127*z[29];
    z[22]=z[5]*z[22];
    z[20]=n<T>(2,3)*z[20] + z[22];
    z[20]=z[5]*z[20];
    z[22]= - n<T>(973,3)*z[5] - z[49] + n<T>(155,3)*z[53] - n<T>(91,3)*z[12] + 82*
    z[11];
    z[24]= - static_cast<T>(271)+ 127*z[41];
    z[24]=z[3]*z[24];
    z[22]=2*z[22] + z[24];
    z[22]=z[3]*z[22];
    z[14]=z[14] + z[19] + z[15] + z[16] + z[18] + z[22] + n<T>(310,3)*z[17]
    + z[20];
    z[14]=z[1]*z[14];
    z[15]= - z[13] + z[12];
    z[15]=z[15]*z[54];
    z[15]=static_cast<T>(1)+ z[15];
    z[16]= - z[12]*z[76];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[10]*z[16];
    z[16]=z[16] + z[30];
    z[16]=z[5]*z[16];
    z[17]=z[10]*z[13];
    z[18]= - static_cast<T>(1)+ n<T>(4,3)*z[17];
    z[19]= - z[10]*z[18];
    z[19]=z[19] + z[30];
    z[19]=z[7]*z[19];
    z[15]=z[19] + n<T>(2,3)*z[15] + z[16];
    z[15]=z[4]*z[15];
    z[16]=z[8]*z[11];
    z[19]=z[9]*z[12];
    z[20]= - z[16] + z[19];
    z[20]=z[3]*z[20];
    z[22]=z[10]*z[21];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[5]*z[22];
    z[20]=z[20] + z[22] - z[21];
    z[21]=static_cast<T>(1)+ z[16];
    z[21]=z[21]*z[23];
    z[21]=z[21] + z[9];
    z[21]=z[21]*z[25];
    z[16]=z[21] - n<T>(4,3)*z[16] + z[18];
    z[16]=z[7]*z[16];
    z[18]= - z[17] - z[19];
    z[17]=static_cast<T>(1)- z[17];
    z[17]=z[17]*z[40];
    z[17]= - z[8] + z[17];
    z[17]=z[17]*z[32];
    z[19]= - static_cast<T>(1)- n<T>(4,3)*z[19];
    z[19]=z[9]*z[19];
    z[19]=n<T>(1,3)*z[8] + z[19];
    z[19]=z[3]*z[19];
    z[17]=z[17] + n<T>(4,3)*z[18] + z[19];
    z[17]=z[2]*z[17];
    z[18]=z[10]*z[11];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[40];
    z[18]=z[18] + z[9];
    z[18]=z[5]*z[18];
    z[19]=z[13] - z[11];
    z[19]=z[10]*z[19];
    z[19]= - static_cast<T>(1)+ z[19];
    z[18]=4*z[19] + z[18];
    z[18]=z[18]*z[32];
    z[15]=z[17] + z[18] + z[15] + n<T>(4,3)*z[20] + z[16];
    z[14]=91*z[15] + z[14];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r953(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r953(const std::array<dd_real,31>&);
#endif
