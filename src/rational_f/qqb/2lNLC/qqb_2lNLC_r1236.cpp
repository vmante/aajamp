#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1236(const std::array<T,31>& k) {
  T z[85];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[12];
    z[10]=k[11];
    z[11]=k[13];
    z[12]=k[14];
    z[13]=k[2];
    z[14]=k[3];
    z[15]=k[19];
    z[16]=2*z[4];
    z[17]=z[10] - z[11];
    z[18]= - z[16] + 2*z[9] - z[17];
    z[19]=npow(z[3],3);
    z[18]=z[18]*z[19];
    z[20]=z[10]*z[11];
    z[21]=z[4]*z[9];
    z[22]=z[20] + z[21];
    z[23]=npow(z[8],2);
    z[24]=n<T>(1,2)*z[23];
    z[25]= - z[22]*z[24];
    z[18]=z[18] + z[25];
    z[18]=z[8]*z[18];
    z[25]=n<T>(3,2)*z[19];
    z[26]=z[22]*z[25];
    z[18]=z[26] + z[18];
    z[26]=3*z[8];
    z[18]=z[18]*z[26];
    z[27]=z[10] + z[12];
    z[28]=z[27]*z[19];
    z[29]=z[10]*z[12];
    z[30]=z[29]*z[6];
    z[31]= - z[19] - z[30];
    z[32]=n<T>(1,2)*z[6];
    z[31]=z[31]*z[32];
    z[28]=z[28] + z[31];
    z[28]=z[6]*z[28];
    z[31]= - z[29]*z[25];
    z[28]=z[31] + z[28];
    z[28]=z[6]*z[28];
    z[18]=z[18] + z[28];
    z[18]=z[7]*z[18];
    z[28]=z[12]*z[13];
    z[31]=z[19]*z[28];
    z[33]=z[4]*z[32];
    z[33]= - z[29] + z[33];
    z[33]=z[6]*z[33];
    z[31]= - n<T>(1,4)*z[31] + z[33];
    z[33]=npow(z[6],2);
    z[31]=z[31]*z[33];
    z[34]=7*z[4];
    z[35]=z[34] - z[10];
    z[36]=5*z[9];
    z[37]= - z[11] - z[36] + z[35];
    z[38]=3*z[23];
    z[37]=z[37]*z[38];
    z[39]= - z[4] - z[10];
    z[39]=z[39]*z[33];
    z[37]=z[37] + z[39];
    z[37]=z[2]*z[19]*z[37];
    z[39]=z[10] - z[9];
    z[40]= - z[8]*z[39];
    z[40]=z[21] + z[40];
    z[41]=npow(z[8],3);
    z[42]=n<T>(3,2)*z[41];
    z[40]=z[40]*z[42];
    z[18]=z[18] + n<T>(1,4)*z[37] + z[40] + z[31];
    z[18]=z[7]*z[18];
    z[31]=npow(z[15],2);
    z[37]= - z[4]*z[15];
    z[37]=z[31] + z[37];
    z[40]=z[4]*z[11];
    z[37]=z[37]*z[40];
    z[43]= - z[31]*z[20];
    z[37]=z[37] + z[43];
    z[43]=n<T>(1,2)*z[21];
    z[44]= - z[43] - z[20];
    z[45]=2*z[10];
    z[46]=n<T>(1,2)*z[9];
    z[47]=z[45] - z[46];
    z[48]=z[8]*z[47];
    z[44]=n<T>(1,2)*z[44] + z[48];
    z[44]=z[44]*z[23];
    z[37]=n<T>(1,8)*z[37] + z[44];
    z[44]=3*z[11];
    z[48]=3*z[4];
    z[49]= - z[48] + z[44] - z[39];
    z[49]=z[49]*z[23]*z[25];
    z[35]= - n<T>(1,2)*z[35] + z[6];
    z[35]=z[33]*z[19]*z[35];
    z[35]=z[49] + z[35];
    z[35]=z[2]*z[35];
    z[49]=npow(z[23],2);
    z[50]=3*z[49];
    z[51]=5*z[10];
    z[52]= - z[9] + z[51];
    z[52]=z[52]*z[50];
    z[53]=z[19]*z[13];
    z[54]=z[6]*z[48];
    z[54]=z[53] + z[54];
    z[55]=npow(z[6],3);
    z[54]=z[54]*z[55];
    z[35]=z[35] + z[52] + z[54];
    z[52]=n<T>(1,2)*z[2];
    z[35]=z[35]*z[52];
    z[54]=npow(z[13],2);
    z[56]=z[54]*z[19];
    z[57]=z[12]*z[56];
    z[58]=3*z[29];
    z[57]= - z[58] + z[57];
    z[57]=z[57]*z[33];
    z[18]=z[18] + z[35] + 3*z[37] + n<T>(1,4)*z[57];
    z[18]=z[7]*z[18];
    z[35]=npow(z[3],2);
    z[37]=npow(z[35],2);
    z[57]=2*z[37];
    z[59]= - z[22]*z[57];
    z[60]=n<T>(3,2)*z[4];
    z[61]= - n<T>(3,2)*z[9] + z[60] + z[17];
    z[61]=z[61]*z[8];
    z[62]=z[37]*z[61];
    z[59]=z[59] + z[62];
    z[59]=z[59]*z[38];
    z[62]= - z[29]*z[57];
    z[63]=z[27] - z[32];
    z[64]=z[37]*z[6];
    z[63]=z[63]*z[64];
    z[62]=z[62] + z[63];
    z[62]=z[62]*z[33];
    z[59]=z[59] + z[62];
    z[59]=z[7]*z[59];
    z[62]= - z[48] + 3*z[9];
    z[63]=z[62] + z[17];
    z[41]=3*z[41];
    z[65]=z[41]*z[37];
    z[66]=z[63]*z[65];
    z[67]= - z[10]*z[37];
    z[67]=z[67] + z[64];
    z[67]=z[67]*z[55];
    z[66]=z[66] + z[67];
    z[66]=z[2]*z[66];
    z[67]= - z[37]*z[28];
    z[68]=z[64]*z[13];
    z[67]=z[67] + z[68];
    z[67]=z[67]*z[55];
    z[66]=z[67] + z[66];
    z[59]=n<T>(1,2)*z[66] + z[59];
    z[59]=z[7]*z[59];
    z[66]=z[54]*z[37];
    z[67]=z[12]*z[66];
    z[69]= - z[54]*z[64];
    z[67]=z[67] + z[69];
    z[69]=n<T>(1,2)*z[55];
    z[67]=z[67]*z[69];
    z[70]=n<T>(1,2)*z[10];
    z[71]=z[4] - z[11];
    z[72]=z[70] - z[46] + z[71];
    z[65]=z[72]*z[65];
    z[72]= - z[4] + z[10];
    z[72]=z[72]*z[37];
    z[72]=z[72] - z[64];
    z[72]=z[72]*z[69];
    z[65]=z[65] + z[72];
    z[65]=z[2]*z[65];
    z[72]=npow(z[6],4);
    z[73]=z[13]*z[72]*z[37];
    z[65]= - n<T>(1,2)*z[73] + z[65];
    z[65]=z[2]*z[65];
    z[59]=z[59] + z[67] + z[65];
    z[59]=z[7]*z[59];
    z[65]=5*z[4];
    z[67]= - z[37]*z[65];
    z[67]=z[67] + z[64];
    z[74]=z[55]*z[2];
    z[67]=z[67]*z[74];
    z[67]=z[73] + z[67];
    z[67]=z[2]*z[67];
    z[73]=z[72]*z[66];
    z[67]=z[73] + z[67];
    z[67]=z[67]*z[52];
    z[73]=3*z[10];
    z[75]=z[49]*z[73];
    z[59]=z[59] - z[75] + z[67];
    z[59]=z[7]*z[59];
    z[67]=npow(z[3],5);
    z[76]=3*z[67];
    z[77]=z[22]*z[76];
    z[78]=z[71] + z[39];
    z[79]=z[78]*z[67];
    z[80]= - z[8]*z[79];
    z[77]=z[77] + z[80];
    z[77]=z[77]*z[41];
    z[80]= - z[29]*z[76];
    z[81]=z[6]*z[27]*z[67];
    z[80]=z[80] + z[81];
    z[80]=z[80]*z[55];
    z[77]=z[77] + z[80];
    z[77]=z[7]*z[77];
    z[80]=z[4] - z[9] - z[17];
    z[80]=z[80]*z[50]*z[67];
    z[81]=z[67]*z[72];
    z[82]=z[81]*z[10];
    z[80]=z[80] - z[82];
    z[80]=z[2]*z[80];
    z[83]= - z[28]*z[81];
    z[77]=z[77] + z[83] + z[80];
    z[77]=z[7]*z[77];
    z[79]= - z[50]*z[79];
    z[79]=z[79] + z[82];
    z[79]=z[79]*npow(z[2],2);
    z[80]=z[81]*z[54]*z[12];
    z[77]=z[77] + z[80] + z[79];
    z[77]=z[7]*z[77];
    z[79]=npow(z[2],3);
    z[80]=z[72]*z[4];
    z[82]= - z[67]*z[79]*z[80];
    z[77]=z[82] + z[77];
    z[77]=z[7]*z[77];
    z[76]= - npow(z[2],4)*z[80]*z[76];
    z[76]=z[76] + z[77];
    z[76]=z[7]*z[76];
    z[77]=z[4]*z[5];
    z[82]=z[5] - z[4];
    z[82]=z[6]*z[82];
    z[82]= - z[77] + z[82];
    z[67]=z[74]*z[67]*z[82];
    z[74]=z[5]*z[13];
    z[82]= - z[74]*z[81];
    z[67]=z[82] + z[67];
    z[82]=3*z[2];
    z[67]=z[67]*z[82];
    z[83]=z[54]*z[5];
    z[81]=z[81]*z[83];
    z[67]=z[81] + z[67];
    z[67]=z[67]*z[79];
    z[79]=z[29]*z[72];
    z[22]=z[22]*z[50];
    z[22]=z[79] + z[22];
    z[79]= - npow(z[7],6)*z[22];
    z[81]=z[77]*z[72];
    z[84]= - npow(z[2],6)*z[81];
    z[79]=z[84] + z[79];
    z[79]=z[1]*z[79]*npow(z[3],6);
    z[67]=z[79] + z[76] - z[75] + z[67];
    z[67]=z[1]*z[67];
    z[76]=z[5] - z[32];
    z[66]=z[55]*z[66]*z[76];
    z[57]= - z[77]*z[57];
    z[76]=n<T>(7,2)*z[4];
    z[79]=4*z[5] - z[76];
    z[79]=z[79]*z[37];
    z[64]=z[79] + n<T>(5,2)*z[64];
    z[64]=z[6]*z[64];
    z[57]=z[57] + z[64];
    z[57]=z[2]*z[57]*z[33];
    z[37]=z[37]*z[74];
    z[37]= - 7*z[37] - z[68];
    z[37]=z[37]*z[69];
    z[37]=z[37] + z[57];
    z[37]=z[2]*z[37];
    z[37]=z[66] + z[37];
    z[37]=z[2]*z[37];
    z[37]=z[75] + z[37];
    z[37]=z[2]*z[37];
    z[57]=z[73] + z[8];
    z[42]=z[57]*z[42];
    z[37]=n<T>(1,2)*z[67] + z[59] + z[42] + z[37];
    z[37]=z[1]*z[37];
    z[42]=n<T>(3,4)*z[4];
    z[57]=z[15]*z[14];
    z[59]=static_cast<T>(1)+ n<T>(1,4)*z[57];
    z[64]=z[5]*z[59];
    z[64]= - z[42] - z[44] + z[64];
    z[64]=z[15]*z[64];
    z[31]=n<T>(3,4)*z[31];
    z[64]=z[31] + z[64];
    z[64]=z[4]*z[64];
    z[59]= - z[15]*z[59];
    z[66]=static_cast<T>(1)- z[57];
    z[66]=z[10]*z[66];
    z[59]=n<T>(1,4)*z[66] + z[59];
    z[59]=z[5]*z[59];
    z[66]=z[15]*z[44];
    z[31]=z[66] - z[31] + z[59];
    z[31]=z[10]*z[31];
    z[31]=z[64] + z[31];
    z[59]=z[19]*z[74];
    z[64]=z[6]*z[60];
    z[53]= - z[53] + z[64];
    z[53]=z[6]*z[53];
    z[53]= - n<T>(13,4)*z[59] + z[53];
    z[53]=z[53]*z[33];
    z[59]=9*z[5] - z[34];
    z[59]=z[59]*z[19];
    z[64]=z[77]*z[32];
    z[19]=5*z[19] + z[64];
    z[19]=z[6]*z[19];
    z[19]=n<T>(1,2)*z[59] + z[19];
    z[19]=z[6]*z[19];
    z[59]= - z[77]*z[25];
    z[19]=z[59] + z[19];
    z[19]=z[2]*z[6]*z[19];
    z[59]=z[39]*z[49];
    z[19]=z[19] - n<T>(3,2)*z[59] + z[53];
    z[19]=z[2]*z[19];
    z[25]=z[25]*z[83];
    z[53]= - z[6]*z[56];
    z[25]=z[25] + z[53];
    z[25]=z[25]*z[33];
    z[47]= - z[47]*z[41];
    z[19]=z[19] + z[47] + n<T>(1,2)*z[25];
    z[19]=z[2]*z[19];
    z[25]=7*z[10];
    z[47]=z[9] - z[25];
    z[47]=n<T>(1,4)*z[47] - z[8];
    z[47]=z[8]*z[47];
    z[47]= - n<T>(1,2)*z[20] + z[47];
    z[47]=z[47]*z[26];
    z[18]=z[37] + z[18] + z[19] - n<T>(1,4)*z[30] + n<T>(1,2)*z[31] + z[47];
    z[18]=z[1]*z[18];
    z[19]=z[22]*z[7];
    z[22]= - z[17] + z[62];
    z[22]=z[8]*z[22];
    z[30]=3*z[20];
    z[22]=z[22] + 5*z[21] + z[30];
    z[22]=z[22]*z[41];
    z[31]=z[12] + z[73];
    z[31]=z[6]*z[31];
    z[31]= - 7*z[29] + z[31];
    z[31]=z[31]*z[55];
    z[22]= - 3*z[19] + z[22] + z[31];
    z[31]=n<T>(1,2)*z[7];
    z[22]=z[22]*z[31];
    z[37]=n<T>(5,2)*z[9];
    z[16]=z[70] - z[37] + z[16];
    z[16]=z[8]*z[16];
    z[30]=z[30] + n<T>(7,2)*z[21];
    z[47]=n<T>(1,2)*z[30];
    z[16]= - z[47] + z[16];
    z[16]=z[8]*z[16];
    z[53]= - z[44] - z[36];
    z[53]=z[10] + n<T>(1,2)*z[53] + z[48];
    z[56]=n<T>(1,2)*z[35];
    z[53]=z[53]*z[56];
    z[16]=z[53] + z[16];
    z[16]=z[16]*z[26];
    z[53]=n<T>(5,2)*z[10];
    z[62]=n<T>(1,2)*z[4];
    z[64]= - z[32] + z[53] + z[12] - z[62];
    z[64]=z[6]*z[64];
    z[66]= - n<T>(13,2)*z[29] - z[35];
    z[64]=n<T>(1,2)*z[66] + z[64];
    z[64]=z[6]*z[64];
    z[66]=z[27]*z[56];
    z[64]=z[66] + z[64];
    z[64]=z[6]*z[64];
    z[50]=z[50]*z[39];
    z[50]=z[50] - z[80];
    z[66]=n<T>(3,2)*z[2];
    z[50]=z[50]*z[66];
    z[67]= - z[12] - z[44];
    z[67]=z[10]*z[67];
    z[67]= - n<T>(15,4)*z[21] + z[67];
    z[67]=z[67]*z[56];
    z[16]=z[22] + z[50] + z[64] + z[67] + z[16];
    z[16]=z[7]*z[16];
    z[22]=z[11] + z[9];
    z[22]=n<T>(1,2)*z[22] - z[4];
    z[22]=z[22]*z[35];
    z[25]=z[36] - z[25];
    z[24]=z[25]*z[24];
    z[22]=z[22] + z[24];
    z[22]=z[22]*z[26];
    z[24]=n<T>(1,8)*z[35];
    z[25]=z[12] - z[44];
    z[24]=z[10]*z[25]*z[24];
    z[25]=z[65] - z[6];
    z[36]= - z[6]*z[25];
    z[36]=z[35] + z[36];
    z[36]=z[6]*z[36];
    z[64]= - z[4]*z[35];
    z[36]=z[64] + z[36];
    z[36]=z[36]*z[32];
    z[59]= - z[59] - z[80];
    z[64]=n<T>(9,2)*z[2];
    z[59]=z[59]*z[64];
    z[22]=z[59] + z[36] + z[24] + z[22];
    z[22]=z[2]*z[22];
    z[20]= - z[40] + z[20];
    z[20]=z[15]*z[20];
    z[24]=z[21]*z[35]*z[14];
    z[20]=3*z[20] + z[24];
    z[24]=n<T>(7,2)*z[9];
    z[36]=z[24] + z[11];
    z[40]= - z[60] + z[36];
    z[40]=n<T>(1,2)*z[40] - z[45];
    z[40]=z[8]*z[40];
    z[40]=z[43] + z[40];
    z[40]=z[8]*z[40];
    z[20]=n<T>(1,8)*z[20] + z[40];
    z[40]=3*z[12];
    z[43]=z[35]*z[13];
    z[51]=z[43] + z[40] + z[51];
    z[51]=n<T>(1,2)*z[51] - z[6];
    z[51]=z[6]*z[51];
    z[51]= - z[58] + z[51];
    z[51]=z[51]*z[32];
    z[16]=z[16] + z[22] + 3*z[20] + z[51];
    z[16]=z[7]*z[16];
    z[20]= - static_cast<T>(1)- n<T>(3,4)*z[57];
    z[20]=z[5]*z[20];
    z[20]=z[42] + n<T>(9,4)*z[11] + n<T>(3,4)*z[15] + z[20];
    z[20]=z[4]*z[20];
    z[22]=z[5]*z[57];
    z[22]=z[22] - z[15];
    z[22]=z[70] - z[12] + n<T>(3,2)*z[22];
    z[22]=z[22]*z[70];
    z[42]=z[11] - z[9];
    z[42]=z[8] + n<T>(1,2)*z[42] + z[10];
    z[42]=z[42]*z[26];
    z[51]=z[54]*z[6];
    z[51]= - z[51] + z[83];
    z[51]=z[35]*z[51];
    z[27]=z[27] + z[51];
    z[27]=z[27]*z[32];
    z[20]=z[27] + z[42] + z[20] + z[22];
    z[22]=z[23]*z[39];
    z[27]=z[71]*z[56];
    z[27]=z[27] + z[22];
    z[27]=z[27]*z[26];
    z[42]=z[77]*z[35];
    z[27]= - z[42] + z[27];
    z[51]=11*z[5] - z[34];
    z[51]=z[51]*z[35];
    z[54]=n<T>(5,2)*z[6] + z[5] - z[76];
    z[54]=z[6]*z[54];
    z[54]=4*z[35] + z[54];
    z[54]=z[6]*z[54];
    z[51]=n<T>(1,4)*z[51] + z[54];
    z[51]=z[6]*z[51];
    z[48]=z[5] - z[48];
    z[48]=z[6]*z[48];
    z[48]= - z[77] + z[48];
    z[48]=z[48]*z[55];
    z[54]= - z[2]*z[81];
    z[48]=z[48] + z[54];
    z[48]=z[48]*z[66];
    z[27]=z[48] + n<T>(1,2)*z[27] + z[51];
    z[27]=z[2]*z[27];
    z[42]=z[14]*z[42];
    z[48]= - z[46] + z[10];
    z[48]=z[48]*z[38];
    z[35]=z[35]*z[74];
    z[43]= - n<T>(3,2)*z[43] + z[6];
    z[43]=z[6]*z[43];
    z[35]= - 3*z[35] + z[43];
    z[35]=z[35]*z[32];
    z[27]=z[27] + z[35] + n<T>(1,8)*z[42] + z[48];
    z[27]=z[2]*z[27];
    z[16]=z[18] + z[16] + n<T>(1,2)*z[20] + z[27];
    z[16]=z[1]*z[16];
    z[18]= - z[61] + z[30];
    z[18]=z[18]*z[41];
    z[20]=z[12] + n<T>(3,2)*z[10];
    z[27]=z[6]*z[20];
    z[27]= - 4*z[29] + z[27];
    z[27]=z[27]*z[55];
    z[18]= - n<T>(3,2)*z[19] + z[18] + z[27];
    z[18]=z[7]*z[18];
    z[19]=z[76] - z[24] + z[17];
    z[19]=z[8]*z[19];
    z[19]=z[19] - z[30];
    z[19]=z[19]*z[38];
    z[24]= - static_cast<T>(1)- n<T>(1,2)*z[28];
    z[24]=z[6]*z[24];
    z[24]=z[24] + 2*z[12] + z[53];
    z[24]=z[6]*z[24];
    z[24]= - n<T>(9,2)*z[29] + z[24];
    z[24]=z[24]*z[33];
    z[27]= - z[63]*z[49];
    z[30]= - z[10]*z[72];
    z[27]=z[27] + z[30];
    z[27]=z[27]*z[66];
    z[18]=z[18] + z[27] + z[19] + z[24];
    z[18]=z[7]*z[18];
    z[19]= - z[65] + 7*z[9];
    z[24]=n<T>(1,2)*z[19] - z[10];
    z[24]=z[24]*z[41];
    z[27]=z[6] + z[4] - z[45];
    z[27]=z[27]*z[55];
    z[24]= - z[50] + z[24] + z[27];
    z[24]=z[2]*z[24];
    z[27]=n<T>(7,2)*z[10] + z[40] + z[62];
    z[30]=z[32]*z[13];
    z[32]=z[30] - static_cast<T>(1)- z[28];
    z[32]=z[6]*z[32];
    z[27]=n<T>(1,2)*z[27] + z[32];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(11,4)*z[29] + z[27];
    z[27]=z[6]*z[27];
    z[32]= - n<T>(5,4)*z[10] - n<T>(13,4)*z[4] + z[36];
    z[32]=z[8]*z[32];
    z[32]=z[47] + z[32];
    z[26]=z[32]*z[26];
    z[18]=z[18] + z[24] + z[26] + z[27];
    z[18]=z[7]*z[18];
    z[24]=z[39]*z[41];
    z[25]=z[25]*z[55];
    z[26]=z[80]*z[64];
    z[24]=z[26] + z[24] + z[25];
    z[24]=z[2]*z[24];
    z[19]=z[73] - z[11] - z[19];
    z[19]=z[19]*z[23];
    z[23]=z[34] - z[73];
    z[25]= - z[13]*z[33];
    z[23]=n<T>(1,2)*z[23] + z[25];
    z[23]=z[23]*z[33];
    z[19]=n<T>(3,2)*z[19] + z[23];
    z[19]=n<T>(1,2)*z[19] + z[24];
    z[19]=z[2]*z[19];
    z[23]= - z[12] - z[11];
    z[23]=z[10]*z[23];
    z[21]= - n<T>(3,2)*z[21] + z[23];
    z[17]=n<T>(5,2)*z[4] - z[37] + z[17];
    z[17]=z[8]*z[17];
    z[17]=n<T>(1,2)*z[21] + z[17];
    z[21]=z[6]*z[13];
    z[23]=z[21] - static_cast<T>(1)- n<T>(3,2)*z[28];
    z[23]=z[6]*z[23];
    z[20]=z[23] + z[20];
    z[20]=z[6]*z[20];
    z[17]=3*z[17] + z[20];
    z[17]=z[18] + n<T>(1,2)*z[17] + z[19];
    z[17]=z[7]*z[17];
    z[18]= - z[5] + z[60];
    z[18]=z[6]*z[18];
    z[18]=z[77] + z[18];
    z[18]=z[18]*z[55];
    z[19]=z[81]*z[52];
    z[18]=z[18] + z[19];
    z[18]=z[18]*z[82];
    z[19]= - z[34] + 5*z[5];
    z[20]= - static_cast<T>(5)+ n<T>(3,2)*z[74];
    z[20]=z[6]*z[20];
    z[20]=z[20] - z[19];
    z[20]=z[6]*z[20];
    z[20]=2*z[77] + z[20];
    z[20]=z[20]*z[33];
    z[18]=z[20] + z[18];
    z[18]=z[2]*z[18];
    z[20]=z[30] - n<T>(11,2) + 2*z[74];
    z[20]=z[6]*z[20];
    z[19]= - n<T>(1,2)*z[19] + z[20];
    z[19]=z[19]*z[33];
    z[18]=z[18] - n<T>(3,4)*z[22] + z[19];
    z[18]=z[2]*z[18];
    z[19]=z[78]*z[8];
    z[19]= - n<T>(3,2)*z[19] - z[77] + n<T>(1,4)*z[29];
    z[20]= - static_cast<T>(2)+ n<T>(3,4)*z[74];
    z[20]=z[20]*z[33];
    z[18]=z[18] + n<T>(1,2)*z[19] + z[20];
    z[18]=z[2]*z[18];
    z[19]= - z[28] + z[21];
    z[19]=z[6]*z[19];
    z[20]=z[46] + n<T>(1,2)*z[5] + z[11];
    z[22]=z[5]*z[14];
    z[22]= - n<T>(9,2) + z[22];
    z[22]=z[4]*z[22];
    z[19]=z[19] - z[10] + 3*z[20] + z[22];
    z[16]=z[16] + z[17] + n<T>(1,4)*z[19] + z[18];
    z[16]=z[1]*z[16];
    z[17]=z[60] - n<T>(3,2)*z[11];
    z[17]=z[14]*z[17];
    z[18]= - n<T>(1,2)*z[14] - z[13];
    z[18]=z[5]*z[18];
    z[17]= - z[21] + n<T>(1,2) + z[18] + z[17];
    z[18]=z[62] + z[5] - z[11];
    z[18]=n<T>(1,2)*z[18] + z[6];
    z[18]=z[18]*z[82];
    z[19]= - n<T>(15,2)*z[4] + n<T>(9,2)*z[9] + n<T>(1,2)*z[12] + z[44];
    z[19]=z[19]*z[31];
    z[17]=z[19] + n<T>(1,2)*z[17] + z[18];

    r += z[16] + n<T>(1,2)*z[17];
 
    return r;
}

template double qqb_2lNLC_r1236(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1236(const std::array<dd_real,31>&);
#endif
