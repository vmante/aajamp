#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1347(const std::array<T,31>& k) {
  T z[114];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[9];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[2];
    z[7]=k[3];
    z[8]=k[6];
    z[9]=k[10];
    z[10]=k[13];
    z[11]=k[5];
    z[12]=k[12];
    z[13]=k[4];
    z[14]=k[7];
    z[15]=k[15];
    z[16]=k[19];
    z[17]=npow(z[15],2);
    z[18]=7*z[14];
    z[19]= - z[18] + 9*z[4];
    z[19]=z[19]*z[17];
    z[20]=npow(z[2],2);
    z[21]=3*z[4];
    z[22]= - n<T>(7,3)*z[14] + z[21];
    z[22]=z[22]*z[20];
    z[23]=npow(z[2],3);
    z[24]=n<T>(1,3)*z[23];
    z[25]=npow(z[15],3);
    z[26]=z[25] - z[24];
    z[27]=z[4] - z[14];
    z[26]=z[27]*z[26];
    z[28]=z[11]*z[26];
    z[19]=4*z[28] + z[19] + z[22];
    z[19]=z[11]*z[19];
    z[22]=5*z[14];
    z[28]= - z[22] + 8*z[4];
    z[29]=7*z[2];
    z[30]= - z[29] - z[28];
    z[31]=n<T>(1,3)*z[2];
    z[30]=z[30]*z[31];
    z[32]=npow(z[8],2);
    z[33]=n<T>(11,2)*z[32];
    z[34]=z[5]*z[12];
    z[28]=z[15] - n<T>(33,4)*z[12] + n<T>(27,4)*z[10] + z[28];
    z[28]=z[15]*z[28];
    z[19]=z[19] + z[33] + z[30] - n<T>(33,4)*z[34] + z[28];
    z[19]=z[11]*z[19];
    z[28]=n<T>(15,4)*z[10];
    z[30]=n<T>(14,3)*z[4];
    z[35]=n<T>(3,2)*z[12];
    z[36]=z[32]*z[6];
    z[37]=3*z[15];
    z[38]=z[32]*z[7];
    z[39]=z[20]*z[6];
    z[40]=2*z[14];
    z[19]=z[19] - n<T>(109,2)*z[38] + n<T>(5,2)*z[36] + n<T>(8,3)*z[39] + n<T>(13,6)*
    z[2] + z[37] - n<T>(5,4)*z[5] - z[35] + z[30] + z[28] - z[40];
    z[19]=z[11]*z[19];
    z[41]=9*z[10];
    z[42]=2*z[12];
    z[43]=z[41] - z[42];
    z[44]=z[12] - z[10];
    z[45]= - z[44]*z[37];
    z[45]= - 5*z[34] + z[45];
    z[45]=z[11]*z[45];
    z[46]=5*z[5];
    z[45]=z[45] - z[46] + z[43];
    z[45]=z[11]*z[45];
    z[47]=z[6]*z[5];
    z[45]=z[45] + static_cast<T>(4)- 5*z[47];
    z[45]=z[11]*z[45];
    z[48]=6*z[7];
    z[49]=3*z[47];
    z[50]=static_cast<T>(1)+ z[49];
    z[50]=z[6]*z[50];
    z[45]=z[45] + z[50] + z[48];
    z[45]=z[11]*z[45];
    z[50]=z[9] - z[5];
    z[51]=z[6]*z[2];
    z[52]=z[50]*z[51];
    z[53]=9*z[9];
    z[54]=z[52] + z[5] + z[53];
    z[54]=z[6]*z[54];
    z[54]= - static_cast<T>(14)+ z[54];
    z[55]=npow(z[6],2);
    z[54]=z[54]*z[55];
    z[56]=z[7]*z[10];
    z[57]=z[56] - 1;
    z[58]=5*z[9];
    z[59]=z[57]*z[58];
    z[60]=7*z[10];
    z[61]= - z[60] + z[59];
    z[61]=z[7]*z[61];
    z[62]=z[6]*z[9];
    z[63]=5*z[62];
    z[61]=z[61] + static_cast<T>(2)+ z[63];
    z[61]=z[7]*z[61];
    z[63]=static_cast<T>(3)- z[63];
    z[63]=z[6]*z[63];
    z[61]=z[63] + z[61];
    z[63]=3*z[7];
    z[61]=z[61]*z[63];
    z[45]=z[45] + z[54] + z[61];
    z[45]=z[3]*z[45];
    z[54]=3*z[5];
    z[61]=n<T>(2,3)*z[2];
    z[64]=z[54] - z[61];
    z[65]=13*z[9];
    z[52]= - z[52] - z[65] + z[64];
    z[52]=z[6]*z[52];
    z[52]=n<T>(31,3) + z[52];
    z[52]=z[6]*z[52];
    z[66]=2*z[5];
    z[67]=3*z[10];
    z[68]=z[67] - z[12];
    z[69]= - z[66] + z[68];
    z[70]=z[15]*z[44];
    z[70]= - 11*z[34] - 9*z[70];
    z[70]=z[11]*z[70];
    z[69]=z[70] - z[61] + 3*z[69] + 2*z[15];
    z[69]=z[11]*z[69];
    z[70]=n<T>(4,3)*z[2];
    z[71]=9*z[5];
    z[72]= - z[71] + z[70];
    z[72]=z[6]*z[72];
    z[69]=z[69] + n<T>(14,3) + z[72];
    z[69]=z[11]*z[69];
    z[72]=z[53]*z[57];
    z[73]=10*z[10];
    z[72]= - z[73] + z[72];
    z[72]=z[72]*z[63];
    z[72]=z[72] + n<T>(5,3) + 27*z[62];
    z[72]=z[7]*z[72];
    z[45]=z[45] + z[69] + z[52] + z[72];
    z[45]=z[3]*z[45];
    z[52]=z[9]*z[10];
    z[69]=n<T>(21,4)*z[52] + 34*z[32];
    z[69]=z[69]*z[63];
    z[72]=z[9] + z[10];
    z[74]=n<T>(2,3)*z[4];
    z[36]=z[69] - n<T>(71,2)*z[36] - z[74] - n<T>(63,4)*z[72];
    z[36]=z[7]*z[36];
    z[69]=z[54] - z[9];
    z[69]=n<T>(1,4)*z[69] - z[2];
    z[69]=z[69]*z[51];
    z[72]=n<T>(5,3)*z[2];
    z[75]= - 23*z[5] + 33*z[9];
    z[69]=z[69] + n<T>(1,4)*z[75] - z[72];
    z[69]=z[6]*z[69];
    z[75]=n<T>(1,2)*z[55];
    z[76]= - z[32]*z[75];
    z[19]=z[45] + z[19] + z[36] + z[76] - n<T>(11,6) + z[69];
    z[19]=z[3]*z[19];
    z[36]=17*z[14];
    z[45]=5*z[4];
    z[69]=z[36] - z[45];
    z[69]=z[69]*z[25];
    z[76]= - z[14] + 13*z[4];
    z[76]=z[76]*z[24];
    z[76]= - z[69] + z[76];
    z[76]=z[11]*z[76];
    z[77]=6*z[15] - 22*z[14] + n<T>(17,2)*z[4];
    z[77]=z[77]*z[17];
    z[78]=2*z[2];
    z[79]=z[14] - n<T>(35,2)*z[4];
    z[79]=n<T>(1,3)*z[79] - z[78];
    z[79]=z[79]*z[20];
    z[80]=7*z[5];
    z[81]= - z[80] - n<T>(21,2)*z[10] + z[12];
    z[81]=z[81]*z[32];
    z[76]=z[76] + z[81] + z[77] + z[79];
    z[76]=z[11]*z[76];
    z[77]=n<T>(3,2)*z[4];
    z[79]=n<T>(3,4)*z[12];
    z[81]= - z[79] + z[77] + n<T>(3,4)*z[10] - 4*z[14];
    z[81]=3*z[81] + 8*z[15];
    z[81]=z[15]*z[81];
    z[82]=z[32]*z[47];
    z[83]=n<T>(87,2)*z[10] - z[12];
    z[83]=z[83]*z[38];
    z[84]= - z[40] + n<T>(25,2)*z[4];
    z[84]=n<T>(1,3)*z[84] + z[2];
    z[84]=z[2]*z[84];
    z[76]=z[76] + z[83] - n<T>(3,2)*z[82] + z[84] - n<T>(9,4)*z[34] + z[81];
    z[76]=z[11]*z[76];
    z[81]=2*z[4];
    z[28]=z[28] - z[81];
    z[28]=z[9]*z[28];
    z[33]=z[62]*z[33];
    z[65]= - 37*z[10] - z[65];
    z[38]=z[65]*z[38];
    z[28]=n<T>(3,2)*z[38] + z[28] + z[33];
    z[28]=z[7]*z[28];
    z[33]=z[46] - z[9];
    z[38]= - n<T>(1,2)*z[33] + z[2];
    z[38]=z[38]*z[51];
    z[51]=n<T>(9,4)*z[10];
    z[65]=4*z[15];
    z[82]=n<T>(1,2)*z[2];
    z[19]=z[19] + z[76] + z[28] + n<T>(1,2)*z[38] + z[82] + z[65] - n<T>(17,4)*
    z[9] + z[5] + z[12] + n<T>(10,3)*z[4] - z[51] - z[14];
    z[19]=z[3]*z[19];
    z[28]=n<T>(57,4)*z[12] - z[45] - z[51] + z[22];
    z[28]=z[28]*z[17];
    z[38]=37*z[4];
    z[51]=z[14] - z[38];
    z[51]=z[51]*z[24];
    z[51]=z[69] + z[51];
    z[76]=npow(z[8],3);
    z[83]=n<T>(1,2)*z[12];
    z[84]=z[10] + z[83];
    z[84]=9*z[84] - n<T>(31,2)*z[5];
    z[84]=z[84]*z[76];
    z[51]=n<T>(1,2)*z[51] + z[84];
    z[51]=z[11]*z[51];
    z[84]= - n<T>(47,2)*z[10] - z[12];
    z[84]=z[84]*z[63];
    z[84]=z[84] - n<T>(11,2)*z[47];
    z[84]=z[76]*z[84];
    z[85]=6*z[2];
    z[86]=z[14] + 32*z[4];
    z[86]=n<T>(1,3)*z[86] + z[85];
    z[86]=z[86]*z[20];
    z[28]=z[51] + z[28] + z[86] + z[84];
    z[28]=z[11]*z[28];
    z[51]=npow(z[7],2);
    z[84]=n<T>(333,2)*z[10] + z[12];
    z[84]=z[84]*z[76]*z[51];
    z[86]=n<T>(5,2)*z[10];
    z[87]= - z[86] + z[14];
    z[79]=z[79] + n<T>(1,2)*z[87] - z[4];
    z[79]=z[15]*z[79];
    z[87]=n<T>(1,4)*z[34];
    z[79]= - z[87] + z[79];
    z[88]=5*z[2];
    z[89]= - n<T>(1,2)*z[14] - 19*z[4];
    z[89]=n<T>(1,3)*z[89] - z[88];
    z[89]=z[2]*z[89];
    z[90]=2*z[6];
    z[91]= - z[23]*z[90];
    z[92]=z[76]*z[55];
    z[93]=n<T>(1,2)*z[5];
    z[94]=z[92]*z[93];
    z[28]=z[28] + z[84] + z[94] + z[91] + 3*z[79] + z[89];
    z[28]=z[11]*z[28];
    z[79]=2*z[11];
    z[26]=z[26]*z[79];
    z[84]=n<T>(9,2)*z[10];
    z[89]=n<T>(21,2)*z[12] - 6*z[4] - z[84] + z[40];
    z[89]=z[89]*z[17];
    z[91]=2*z[20];
    z[94]=n<T>(1,3)*z[14] - z[4];
    z[94]=z[94]*z[91];
    z[89]= - z[26] + 11*z[76] + z[89] + z[94];
    z[89]=z[11]*z[89];
    z[94]= - z[14] + 7*z[4];
    z[95]=n<T>(9,2)*z[12] - z[41] - z[94];
    z[95]=z[15]*z[95];
    z[85]=n<T>(1,3)*z[94] + z[85];
    z[85]=z[2]*z[85];
    z[94]=z[76]*z[6];
    z[96]=z[76]*z[7];
    z[85]=z[89] + 11*z[96] + 6*z[94] + z[85] - n<T>(3,2)*z[34] + z[95];
    z[85]=z[11]*z[85];
    z[89]=14*z[94] - 101*z[96];
    z[89]=z[7]*z[89];
    z[95]=n<T>(5,2)*z[5];
    z[85]=z[85] + z[89] + z[92] - 10*z[39] - z[88] - n<T>(16,3)*z[4] - z[95]
   ;
    z[85]=z[11]*z[85];
    z[88]=45*z[96] - n<T>(3,2)*z[52] - 25*z[94];
    z[88]=z[88]*z[63];
    z[89]=n<T>(4,3)*z[4];
    z[94]=n<T>(9,2)*z[9];
    z[88]=z[88] + 17*z[92] + z[89] + z[94];
    z[88]=z[7]*z[88];
    z[97]=3*z[11];
    z[44]=z[44]*z[17]*z[97];
    z[37]= - z[68]*z[37];
    z[37]=z[44] - z[34] + z[37];
    z[37]=z[11]*z[37];
    z[37]= - z[5] + z[37];
    z[37]=z[11]*z[37];
    z[44]=z[5] - z[78];
    z[44]=z[6]*z[44];
    z[37]=z[37] + static_cast<T>(3)+ z[44];
    z[37]=z[11]*z[37];
    z[44]= - z[50]*z[39];
    z[50]= - z[46] - z[9];
    z[50]=z[2]*z[50];
    z[44]=z[50] + z[44];
    z[44]=z[6]*z[44];
    z[50]=z[78] + z[5] + z[9];
    z[44]=3*z[50] + z[44];
    z[44]=z[6]*z[44];
    z[44]= - static_cast<T>(6)+ z[44];
    z[44]=z[6]*z[44];
    z[50]= - z[7]*z[9]*z[57];
    z[50]=z[50] + static_cast<T>(1)- z[62];
    z[50]=z[50]*z[63];
    z[37]=z[37] + z[44] + z[50];
    z[37]=z[3]*z[37];
    z[44]=n<T>(1,2)*z[39];
    z[33]= - z[33]*z[44];
    z[50]=z[54] + z[9];
    z[50]=n<T>(3,2)*z[50] + n<T>(22,3)*z[2];
    z[50]=z[2]*z[50];
    z[33]=z[50] + z[33];
    z[33]=z[6]*z[33];
    z[50]=3*z[9];
    z[57]=z[5] - z[50];
    z[57]=3*z[57] + z[2];
    z[33]=n<T>(1,2)*z[57] + z[33];
    z[33]=z[6]*z[33];
    z[57]=npow(z[6],3);
    z[98]=z[57]*z[76];
    z[33]=z[37] + z[85] + z[88] + 4*z[98] + n<T>(41,6) + z[33];
    z[33]=z[3]*z[33];
    z[37]=z[46] - z[50];
    z[46]=3*z[2];
    z[37]=n<T>(1,4)*z[37] - z[46];
    z[37]=z[2]*z[37];
    z[85]=z[71] - z[9];
    z[85]=z[85]*z[39];
    z[37]=z[37] + n<T>(1,4)*z[85];
    z[37]=z[6]*z[37];
    z[85]=z[76]*z[62];
    z[88]= - z[67] - z[9];
    z[88]=z[88]*z[96];
    z[85]=29*z[85] + 35*z[88];
    z[85]=z[85]*z[63];
    z[88]=z[9]*z[92];
    z[85]=z[85] - n<T>(9,2)*z[52] - 23*z[88];
    z[85]=z[7]*z[85];
    z[88]=n<T>(7,3)*z[4];
    z[28]=z[33] + z[28] + n<T>(1,2)*z[85] + z[37] - z[82] + n<T>(9,4)*z[9] + 
    z[88] - n<T>(11,4)*z[5];
    z[28]=z[3]*z[28];
    z[33]=n<T>(31,2)*z[14];
    z[37]=z[33] - z[81];
    z[37]=z[37]*z[25];
    z[85]= - z[40] + 29*z[4];
    z[24]=z[85]*z[24];
    z[85]=z[34]*z[11];
    z[76]=z[76]*z[85];
    z[24]= - n<T>(45,2)*z[76] + z[37] + z[24];
    z[24]=z[11]*z[24];
    z[37]=n<T>(7,2)*z[4];
    z[76]=9*z[12] + 6*z[14] - z[37];
    z[76]=z[76]*z[17];
    z[92]=z[40] - n<T>(59,2)*z[4];
    z[92]=n<T>(1,3)*z[92] - n<T>(5,2)*z[2];
    z[92]=z[92]*z[20];
    z[24]=z[24] + z[76] + z[92];
    z[24]=z[11]*z[24];
    z[76]=3*z[14];
    z[35]=z[35] + z[76] - z[45];
    z[92]=n<T>(1,2)*z[15];
    z[35]=z[35]*z[92];
    z[96]=z[47]*z[20];
    z[98]=n<T>(3,2)*z[5];
    z[99]=11*z[4];
    z[100]= - z[14] + z[99];
    z[100]=n<T>(8,3)*z[2] + n<T>(1,3)*z[100] - z[98];
    z[100]=z[2]*z[100];
    z[101]=z[7]*z[8];
    z[102]=npow(z[101],3)*z[52];
    z[103]=n<T>(1,3)*z[12];
    z[104]=z[103]*z[4];
    z[105]=2*z[9];
    z[106]=z[4]*z[105];
    z[24]=z[28] + z[24] + n<T>(45,2)*z[102] - z[96] + z[100] + z[35] + 
    z[106] - z[104] - z[87];
    z[24]=z[3]*z[24];
    z[28]=npow(z[8],4);
    z[35]=2*z[28];
    z[87]= - 63*z[10] + z[12];
    z[87]=z[87]*z[51]*z[35];
    z[100]=5*z[28];
    z[102]= - z[47]*z[100];
    z[106]=5*z[12];
    z[107]=z[41] - z[106];
    z[108]=z[28]*z[7];
    z[107]=z[107]*z[108];
    z[109]=z[23]*z[4];
    z[102]=z[107] + n<T>(4,3)*z[109] + z[102];
    z[107]= - 17*z[5] + z[67] + 13*z[12];
    z[107]=z[11]*z[107]*z[28];
    z[102]=2*z[102] + z[107];
    z[102]=z[11]*z[102];
    z[107]= - z[21] - z[78];
    z[91]=z[107]*z[91];
    z[107]=z[55]*z[28];
    z[110]= - z[54]*z[107];
    z[87]=z[102] + z[87] + z[91] + z[110];
    z[87]=z[11]*z[87];
    z[82]=z[74] + z[82];
    z[29]=z[82]*z[29];
    z[82]=z[57]*z[28];
    z[91]=4*z[5];
    z[102]= - z[82]*z[91];
    z[110]=z[10]*npow(z[7],3);
    z[111]=z[28]*z[110];
    z[112]=z[6]*z[23];
    z[29]=z[87] + 240*z[111] + z[102] + z[29] + n<T>(8,3)*z[112];
    z[29]=z[11]*z[29];
    z[87]=z[28]*z[63];
    z[102]=z[28]*z[6];
    z[100]=z[11]*z[100];
    z[87]=z[100] + 5*z[102] + z[87];
    z[87]=z[11]*z[87];
    z[100]=3*z[102] + 10*z[108];
    z[100]=z[7]*z[100];
    z[87]=z[87] + z[100] - z[20] + 5*z[107];
    z[87]=z[11]*z[87];
    z[100]=13*z[102];
    z[102]=z[100] - 54*z[108];
    z[102]=z[7]*z[102];
    z[102]=3*z[107] + z[102];
    z[102]=z[7]*z[102];
    z[87]=z[87] + z[102] + 4*z[39] + 5*z[82];
    z[87]=z[11]*z[87];
    z[102]=4*z[96];
    z[111]= - z[54] - z[78];
    z[46]=z[111]*z[46];
    z[46]=z[46] + z[102];
    z[46]=z[6]*z[46];
    z[111]=z[2] + z[5];
    z[46]=3*z[111] + z[46];
    z[46]=z[6]*z[46];
    z[100]= - z[100] + 20*z[108];
    z[100]=z[100]*z[63];
    z[100]=16*z[107] + z[100];
    z[100]=z[7]*z[100];
    z[100]=3*z[82] + z[100];
    z[100]=z[7]*z[100];
    z[112]=npow(z[6],4);
    z[113]=z[112]*z[28];
    z[46]=z[87] + z[100] + z[46] - 3*z[113];
    z[46]=z[3]*z[46];
    z[35]=z[62]*z[35];
    z[60]=z[60] + z[105];
    z[87]= - z[60]*z[108];
    z[35]=z[35] + z[87];
    z[35]=z[7]*z[35];
    z[87]=z[9]*z[107];
    z[35]= - 46*z[87] + 27*z[35];
    z[35]=z[7]*z[35];
    z[82]=z[9]*z[82];
    z[35]=12*z[82] + z[35];
    z[35]=z[7]*z[35];
    z[82]=n<T>(15,2)*z[5];
    z[87]= - z[82] - z[61];
    z[87]=z[87]*z[39];
    z[100]=6*z[5] + n<T>(11,2)*z[2];
    z[100]=z[2]*z[100];
    z[87]=z[100] + z[87];
    z[87]=z[6]*z[87];
    z[100]= - z[4] - z[2];
    z[29]=z[46] + z[29] + z[35] + n<T>(7,3)*z[100] + z[87];
    z[29]=z[3]*z[29];
    z[35]=npow(z[7],4);
    z[46]=z[28]*z[35]*z[52];
    z[87]=n<T>(1,3)*z[20];
    z[38]=z[38] + n<T>(31,2)*z[2];
    z[38]=z[38]*z[87];
    z[28]=z[28]*npow(z[11],2)*z[34];
    z[28]= - 21*z[109] - 65*z[28];
    z[28]=z[11]*z[28];
    z[28]=z[38] + n<T>(1,2)*z[28];
    z[28]=z[11]*z[28];
    z[38]=z[71] - z[72];
    z[38]=z[38]*z[44];
    z[44]= - n<T>(59,12)*z[2] - n<T>(11,3)*z[4] + z[93];
    z[44]=z[2]*z[44];
    z[28]=z[29] + z[28] + n<T>(105,2)*z[46] + z[44] + z[38];
    z[28]=z[3]*z[28];
    z[29]=n<T>(3,2)*z[2];
    z[38]= - z[29] - z[45] - z[5];
    z[38]=z[38]*z[20];
    z[44]=z[109]*z[11];
    z[28]=z[28] + z[38] + 9*z[44];
    z[28]=z[3]*z[28];
    z[38]=z[106] - z[80];
    z[38]=z[11]*z[38];
    z[45]=7*z[47];
    z[46]=z[10]*z[63];
    z[38]=z[38] - z[45] + z[46];
    z[38]=z[11]*z[38];
    z[46]= - z[55]*z[80];
    z[72]=z[43]*z[51];
    z[38]=z[38] + z[46] + z[72];
    z[46]=npow(z[8],5);
    z[38]=z[11]*z[46]*z[38];
    z[72]= - z[57]*z[80];
    z[72]=z[72] - 66*z[110];
    z[72]=z[46]*z[72];
    z[38]=z[38] + z[72];
    z[38]=z[11]*z[38];
    z[72]=z[112]*z[46];
    z[100]=z[5]*z[72];
    z[35]=z[10]*z[46]*z[35];
    z[35]=z[38] + 114*z[35] + z[20] + z[100];
    z[35]=z[11]*z[35];
    z[38]=4*z[2];
    z[100]= - z[80] - z[38];
    z[100]=z[2]*z[100];
    z[96]=z[100] + 6*z[96];
    z[96]=z[6]*z[96];
    z[100]=7*z[9];
    z[107]= - z[55]*z[100];
    z[108]=7*z[62];
    z[110]= - 27*z[10] - z[100];
    z[110]=z[7]*z[110];
    z[110]=z[108] + z[110];
    z[110]=z[7]*z[110];
    z[107]=z[107] + z[110];
    z[107]=z[63]*z[46]*z[107];
    z[110]=z[9]*z[57]*z[46];
    z[107]=19*z[110] + z[107];
    z[107]=z[7]*z[107];
    z[72]= - z[58]*z[72];
    z[72]=z[72] + z[107];
    z[72]=z[7]*z[72];
    z[35]=z[35] + z[72] + z[96] + z[111];
    z[35]=z[3]*z[35];
    z[70]= - z[21] - z[70];
    z[70]=z[70]*z[20];
    z[46]=z[46]*npow(z[11],3)*z[34];
    z[46]=z[109] - 6*z[46];
    z[46]=z[46]*z[79];
    z[46]=z[70] + z[46];
    z[46]=z[46]*z[79];
    z[70]=n<T>(9,2)*z[2] + z[88] + z[95];
    z[70]=z[2]*z[70];
    z[72]= - z[82] + z[61];
    z[39]=z[72]*z[39];
    z[72]=npow(z[101],5)*z[52];
    z[35]=z[35] + z[46] + 54*z[72] + z[70] + z[39];
    z[35]=z[3]*z[35];
    z[29]=z[29] + z[30] + n<T>(9,4)*z[5];
    z[29]=z[29]*z[20];
    z[29]=z[35] + z[29] - n<T>(47,6)*z[44];
    z[29]=z[3]*z[29];
    z[29]=3*z[109] + z[29];
    z[29]=z[3]*z[29];
    z[30]=npow(z[101],6)*z[52];
    z[35]=z[11]*z[8];
    z[35]=z[34]*npow(z[35],6);
    z[39]= - z[66] - z[2];
    z[39]=z[2]*z[39];
    z[30]= - 7*z[35] + 21*z[30] + z[39] + z[102];
    z[30]=z[3]*z[30];
    z[35]= - z[61] - z[81] - z[95];
    z[35]=z[35]*z[20];
    z[30]=z[30] + z[35] + n<T>(8,3)*z[44];
    z[30]=z[3]*z[30];
    z[35]=n<T>(13,6)*z[109];
    z[30]= - z[35] + z[30];
    z[30]=z[30]*npow(z[3],2);
    z[39]=z[3]*z[5]*z[20];
    z[39]=n<T>(2,3)*z[109] + z[39];
    z[39]=z[1]*z[39]*npow(z[3],3);
    z[30]=z[30] + z[39];
    z[30]=z[1]*z[30];
    z[29]=z[29] + z[30];
    z[29]=z[1]*z[29];
    z[28]=z[29] - z[35] + z[28];
    z[28]=z[1]*z[28];
    z[29]=z[12]*z[14];
    z[23]=z[11]*z[29]*z[23];
    z[30]=n<T>(2,3)*z[23];
    z[33]=z[25]*z[33];
    z[35]= - n<T>(31,2)*z[4] + z[42];
    z[35]=z[2]*z[35];
    z[35]=2*z[29] + z[35];
    z[35]=z[35]*z[87];
    z[33]= - z[30] + z[33] + z[35];
    z[33]=z[11]*z[33];
    z[35]=z[40]*z[9];
    z[39]=npow(z[13],2);
    z[44]=z[39]*z[35];
    z[46]=z[14]*z[13];
    z[66]=n<T>(17,2)*z[46];
    z[44]=z[66] + z[44];
    z[44]=z[15]*z[44];
    z[35]=z[13]*z[35];
    z[35]=z[44] + z[35] + z[22] + n<T>(9,4)*z[12];
    z[35]=z[15]*z[35];
    z[44]=z[9]*z[14];
    z[35]=z[44] + z[35];
    z[35]=z[15]*z[35];
    z[70]=z[12]*z[13];
    z[72]=z[4]*z[13];
    z[79]=z[70] - z[72];
    z[61]=z[79]*z[61];
    z[82]=n<T>(2,3)*z[12];
    z[95]= - z[61] + n<T>(1,4)*z[5] + z[21] - z[82];
    z[95]=z[2]*z[95];
    z[95]= - n<T>(1,3)*z[29] + z[95];
    z[95]=z[2]*z[95];
    z[27]= - z[16]*z[27];
    z[96]=z[103]*z[27];
    z[102]=z[27]*z[9];
    z[103]=z[102] - z[96];
    z[24]=z[28] + z[24] + z[33] + z[95] - 2*z[103] + z[35];
    z[24]=z[1]*z[24];
    z[28]=z[32]*z[34];
    z[33]=4*z[4];
    z[35]= - 31*z[14] + z[33];
    z[35]=z[35]*z[25];
    z[95]=4*z[12];
    z[107]= - n<T>(23,2)*z[4] + z[95];
    z[107]=z[2]*z[107];
    z[107]=n<T>(17,2)*z[29] + z[107];
    z[87]=z[107]*z[87];
    z[23]= - n<T>(4,3)*z[23] - n<T>(17,2)*z[28] + z[35] + z[87];
    z[23]=z[11]*z[23];
    z[35]= - 63*z[14] + z[99];
    z[87]=n<T>(23,2) - 31*z[46];
    z[87]=z[15]*z[87];
    z[35]=n<T>(1,2)*z[35] + z[87];
    z[35]=z[35]*z[17];
    z[38]= - z[79]*z[38];
    z[87]=25*z[4] - 17*z[12];
    z[38]=n<T>(1,2)*z[87] + z[38];
    z[38]=z[2]*z[38];
    z[38]= - 8*z[29] + z[38];
    z[38]=z[38]*z[31];
    z[28]=z[13]*z[28];
    z[23]=z[23] - n<T>(3,2)*z[28] + z[35] + z[38];
    z[23]=z[11]*z[23];
    z[28]=z[36]*z[39];
    z[35]=npow(z[13],3);
    z[38]=z[35]*z[14];
    z[87]=z[39] - z[38];
    z[99]=4*z[9];
    z[87]=z[87]*z[99];
    z[87]=z[87] + 11*z[13] - z[28];
    z[87]=z[15]*z[87];
    z[28]=13*z[13] - z[28];
    z[107]=n<T>(1,2)*z[9];
    z[28]=z[28]*z[107];
    z[28]=z[87] + z[28] + n<T>(39,4) - 25*z[46];
    z[28]=z[15]*z[28];
    z[87]=z[40]*z[13];
    z[87]=static_cast<T>(1)- z[87];
    z[87]=z[87]*z[99];
    z[28]=z[28] + z[87] - 15*z[14] + z[81];
    z[28]=z[15]*z[28];
    z[81]= - n<T>(1,4) + n<T>(2,3)*z[79];
    z[81]=z[2]*z[81];
    z[87]= - n<T>(13,2)*z[4] + z[95];
    z[81]=z[81] + n<T>(1,3)*z[87] + z[93];
    z[81]=z[2]*z[81];
    z[87]=z[96] + z[102];
    z[96]=z[52]*z[63];
    z[32]=z[32]*z[96];
    z[32]=2*z[87] + z[32];
    z[32]=z[7]*z[32];
    z[87]=z[4] + z[14];
    z[99]=z[12]*z[87];
    z[99]= - 10*z[27] + z[99];
    z[19]=z[24] + z[19] + z[23] + z[32] + z[81] + z[28] + n<T>(1,3)*z[99] + 
    z[44];
    z[19]=z[1]*z[19];
    z[21]=z[22] - z[21];
    z[21]=z[21]*z[17];
    z[22]=n<T>(5,3)*z[14] - z[4];
    z[22]=z[22]*z[20];
    z[21]= - z[26] + z[21] + z[22];
    z[21]=z[11]*z[21];
    z[22]=z[33] - z[18];
    z[23]= - z[65] - z[22];
    z[23]=z[15]*z[23];
    z[22]= - z[78] + z[22];
    z[22]=z[22]*z[31];
    z[21]=z[21] + z[22] - n<T>(27,2)*z[34] + z[23];
    z[21]=z[11]*z[21];
    z[22]= - z[86] + z[40];
    z[23]=z[17]*z[90];
    z[21]=z[21] + z[23] - z[65] - z[82] + 3*z[22] - z[74] - z[64];
    z[21]=z[11]*z[21];
    z[22]=9*z[85];
    z[23]= - z[22] + z[95] - z[71];
    z[23]=z[11]*z[23];
    z[24]=z[7]*z[43];
    z[26]= - static_cast<T>(4)+ 9*z[47];
    z[23]=z[23] + z[24] - z[26];
    z[23]=z[11]*z[23];
    z[24]= - z[6]*z[26];
    z[23]=z[23] + z[24] + 7*z[7];
    z[23]=z[11]*z[23];
    z[24]=static_cast<T>(4)+ z[49];
    z[24]=z[24]*z[55];
    z[26]=9*z[7];
    z[28]=static_cast<T>(1)- 6*z[56];
    z[28]=z[28]*z[26];
    z[28]=7*z[6] + z[28];
    z[28]=z[7]*z[28];
    z[23]=z[23] + z[24] + z[28];
    z[23]=z[11]*z[23];
    z[24]=3*z[62];
    z[28]=static_cast<T>(1)- z[24];
    z[28]=z[28]*z[55];
    z[32]= - z[96] + 8*z[10] + z[50];
    z[32]=z[7]*z[32];
    z[32]=z[32] - static_cast<T>(5)- z[24];
    z[32]=z[7]*z[32];
    z[33]=static_cast<T>(2)+ z[24];
    z[33]=z[6]*z[33];
    z[32]=z[33] + z[32];
    z[32]=z[32]*z[26];
    z[28]=7*z[28] + z[32];
    z[28]=z[7]*z[28];
    z[32]= - static_cast<T>(8)+ z[24];
    z[32]=z[32]*z[57];
    z[23]=z[23] + z[32] + z[28];
    z[23]=z[3]*z[23];
    z[28]= - n<T>(51,2)*z[85] - n<T>(33,2)*z[5] + z[84] + z[106];
    z[28]=z[11]*z[28];
    z[32]=z[68]*z[63];
    z[33]=static_cast<T>(11)- 15*z[47];
    z[28]=z[28] + n<T>(1,2)*z[33] + z[32];
    z[28]=z[11]*z[28];
    z[32]=n<T>(3,2)*z[6];
    z[33]=static_cast<T>(1)- z[45];
    z[33]=z[33]*z[32];
    z[43]=n<T>(5,2) - 18*z[56];
    z[43]=z[7]*z[43];
    z[28]=z[28] + z[33] + 5*z[43];
    z[28]=z[11]*z[28];
    z[33]=z[54] - 11*z[9];
    z[33]=z[6]*z[33];
    z[33]=static_cast<T>(19)+ z[33];
    z[33]=z[33]*z[75];
    z[43]=15*z[10];
    z[44]= - n<T>(13,2)*z[56] + n<T>(13,2);
    z[44]=z[9]*z[44];
    z[44]=z[43] + z[44];
    z[44]=z[7]*z[44];
    z[45]= - static_cast<T>(17)- 13*z[62];
    z[44]=n<T>(1,2)*z[45] + z[44];
    z[44]=z[44]*z[26];
    z[45]=static_cast<T>(19)+ n<T>(79,2)*z[62];
    z[45]=z[6]*z[45];
    z[44]=z[45] + z[44];
    z[44]=z[7]*z[44];
    z[23]=z[23] + z[28] + z[33] + z[44];
    z[23]=z[3]*z[23];
    z[28]= - z[43] + z[12];
    z[28]=z[28]*z[63];
    z[33]= - z[12] + n<T>(3,2)*z[10];
    z[22]= - z[22] - n<T>(7,2)*z[5] + z[33];
    z[22]=z[22]*z[97];
    z[22]=z[22] + n<T>(1,2) + z[28];
    z[22]=z[11]*z[22];
    z[28]= - z[54] + z[58];
    z[28]=z[6]*z[28];
    z[28]= - n<T>(3,2) + z[28];
    z[28]=z[6]*z[28];
    z[43]=n<T>(19,2)*z[10] - z[59];
    z[43]=z[43]*z[26];
    z[43]=z[43] - n<T>(73,2) - 30*z[62];
    z[43]=z[7]*z[43];
    z[22]=z[23] + z[22] + z[28] + z[43];
    z[22]=z[3]*z[22];
    z[23]= - n<T>(27,2)*z[10] + z[4];
    z[23]=z[9]*z[23];
    z[23]= - z[104] + z[23];
    z[23]=z[7]*z[23];
    z[23]=z[23] + n<T>(25,2)*z[9] + n<T>(5,3)*z[12] - z[89] + 21*z[10] - z[40];
    z[23]=z[7]*z[23];
    z[28]=z[15] + z[5] - z[107];
    z[28]=z[6]*z[28];
    z[21]=z[22] + z[21] + z[23] - n<T>(53,6) + z[28];
    z[21]=z[3]*z[21];
    z[22]= - z[14] - n<T>(5,3)*z[4];
    z[22]=n<T>(1,2)*z[22] + z[82];
    z[22]=z[2]*z[22];
    z[22]=n<T>(13,6)*z[29] + z[22];
    z[20]=z[22]*z[20];
    z[20]= - z[30] + n<T>(1,2)*z[69] + z[20];
    z[20]=z[11]*z[20];
    z[22]=2*z[72];
    z[23]= - z[22] - static_cast<T>(6)+ n<T>(31,2)*z[46];
    z[23]=z[15]*z[23];
    z[23]=z[23] + z[36] - z[37];
    z[17]=z[23]*z[17];
    z[23]= - n<T>(13,3)*z[12] + z[76] + z[88];
    z[23]=n<T>(1,2)*z[23] - z[61];
    z[23]=z[2]*z[23];
    z[23]= - n<T>(23,6)*z[29] + z[23];
    z[23]=z[2]*z[23];
    z[17]=z[20] + z[17] + z[23];
    z[17]=z[11]*z[17];
    z[20]=z[39]*z[14];
    z[23]= - 23*z[13] + 31*z[20];
    z[23]=z[23]*z[92];
    z[22]=z[23] - z[22] - static_cast<T>(14)+ n<T>(51,2)*z[46];
    z[22]=z[15]*z[22];
    z[18]=z[18] - z[77];
    z[18]=3*z[18] + z[22];
    z[18]=z[15]*z[18];
    z[22]=8*z[27];
    z[23]= - z[22] + 11*z[29];
    z[27]=z[61] - n<T>(3,2)*z[87] + n<T>(7,3)*z[12];
    z[27]=z[2]*z[27];
    z[25]=z[25]*z[90];
    z[17]=z[17] + z[25] + z[27] + z[18] + n<T>(1,3)*z[23] - n<T>(7,2)*z[34];
    z[17]=z[11]*z[17];
    z[18]=z[14]*npow(z[13],4);
    z[18]= - 2*z[35] + z[18];
    z[18]=z[18]*z[105];
    z[23]=11*z[39];
    z[18]=z[18] - z[23] + n<T>(17,2)*z[38];
    z[18]=z[15]*z[18];
    z[23]= - z[23] + n<T>(13,2)*z[38];
    z[23]=z[9]*z[23];
    z[18]=z[18] + z[23] - n<T>(39,2)*z[13] + 20*z[20];
    z[18]=z[15]*z[18];
    z[23]=z[39]*z[105];
    z[23]=n<T>(5,2)*z[13] + z[23];
    z[23]=z[15]*z[23];
    z[25]=z[13]*z[53];
    z[25]=static_cast<T>(7)+ z[25];
    z[23]=n<T>(1,2)*z[25] + z[23];
    z[23]=z[15]*z[23];
    z[23]=z[94] + z[23];
    z[23]=z[6]*z[23];
    z[25]= - static_cast<T>(5)+ z[66];
    z[20]= - 15*z[13] + n<T>(23,2)*z[20];
    z[20]=z[9]*z[20];
    z[18]=z[23] + z[18] + z[20] + 3*z[25] - z[72];
    z[18]=z[15]*z[18];
    z[20]=z[22] - 5*z[29];
    z[22]= - n<T>(1,2)*z[10] - z[14];
    z[22]=z[22]*z[50];
    z[23]= - z[7]*z[103];
    z[20]=z[23] + n<T>(1,3)*z[20] + z[22];
    z[20]=z[7]*z[20];
    z[22]=z[70] + 1;
    z[23]= - z[22]*z[93];
    z[25]= - z[79]*z[31];
    z[27]= - n<T>(15,2) + 7*z[46];
    z[27]=z[9]*z[27];
    z[17]=z[19] + z[21] + z[17] + z[20] + z[25] + z[27] + z[23] - n<T>(1,3)*
    z[4] + 13*z[14] + z[18] + z[33];
    z[17]=z[1]*z[17];
    z[18]= - z[5]*z[22];
    z[18]=z[67] + z[18];
    z[18]=z[8]*z[18];
    z[19]=z[85]*z[8];
    z[18]=z[18] - 5*z[19];
    z[18]=z[11]*z[18];
    z[20]=z[101]*z[10];
    z[18]=z[18] - 9*z[20] + z[8] - z[98] + z[67] - z[83];
    z[18]=z[11]*z[18];
    z[21]=z[68]*z[101];
    z[22]=z[95] - z[54];
    z[22]=z[8]*z[22];
    z[22]=z[22] - 4*z[19];
    z[22]=z[11]*z[22];
    z[23]=static_cast<T>(3)- 2*z[47];
    z[23]=z[8]*z[23];
    z[21]=z[22] + z[23] + 2*z[21];
    z[21]=z[11]*z[21];
    z[22]=z[8]*z[6];
    z[23]=static_cast<T>(2)- z[47];
    z[23]=z[23]*z[22];
    z[25]= - 33*z[10] - z[42];
    z[25]=z[25]*z[101];
    z[27]=3*z[8];
    z[25]=z[27] + z[25];
    z[25]=z[7]*z[25];
    z[21]=z[21] + z[23] + z[25];
    z[21]=z[11]*z[21];
    z[23]= - z[27] + 8*z[20];
    z[23]=z[23]*z[26];
    z[23]=2*z[22] + z[23];
    z[23]=z[7]*z[23];
    z[25]=z[55]*z[8];
    z[21]=z[21] + z[25] + z[23];
    z[21]=z[11]*z[21];
    z[23]=15*z[8] - 26*z[20];
    z[23]=z[23]*z[63];
    z[23]= - 19*z[22] + z[23];
    z[23]=z[7]*z[23];
    z[23]=z[25] + z[23];
    z[23]=z[7]*z[23];
    z[21]=z[23] + z[21];
    z[21]=z[11]*z[21];
    z[23]=z[12] - z[5];
    z[23]=z[8]*z[23];
    z[23]=z[23] - z[19];
    z[23]=z[11]*z[23];
    z[26]=z[47] - 1;
    z[28]= - z[8]*z[26];
    z[29]=z[12]*z[101];
    z[23]=z[23] + z[28] + z[29];
    z[23]=z[11]*z[23];
    z[28]= - z[26]*z[22];
    z[29]=z[67] - z[42];
    z[29]=z[29]*z[101];
    z[29]=z[8] + z[29];
    z[29]=z[7]*z[29];
    z[23]=z[23] + z[28] + z[29];
    z[23]=z[11]*z[23];
    z[28]= - z[26]*z[25];
    z[29]=z[8] - 15*z[20];
    z[29]=z[7]*z[29];
    z[29]=z[22] + z[29];
    z[29]=z[7]*z[29];
    z[23]=z[23] + z[28] + z[29];
    z[23]=z[11]*z[23];
    z[28]=z[57]*z[8];
    z[26]= - z[26]*z[28];
    z[29]=5*z[20];
    z[30]=2*z[8];
    z[31]= - z[30] + z[29];
    z[31]=z[31]*z[48];
    z[31]=z[22] + z[31];
    z[31]=z[7]*z[31];
    z[31]=z[25] + z[31];
    z[31]=z[7]*z[31];
    z[23]=z[23] + z[26] + z[31];
    z[23]=z[11]*z[23];
    z[26]=z[27] - z[29];
    z[27]=2*z[7];
    z[26]=z[26]*z[27];
    z[26]= - 3*z[22] + z[26];
    z[26]=z[26]*z[63];
    z[26]=z[25] + z[26];
    z[26]=z[7]*z[26];
    z[26]=z[28] + z[26];
    z[26]=z[7]*z[26];
    z[29]=z[112]*z[8];
    z[23]=z[23] + z[29] + z[26];
    z[23]=z[11]*z[23];
    z[26]=static_cast<T>(3)+ z[62];
    z[26]=z[26]*z[22];
    z[31]=z[52]*z[101];
    z[33]=5*z[10] + z[9];
    z[33]=z[8]*z[33];
    z[33]=z[33] - z[31];
    z[33]=z[7]*z[33];
    z[34]= - static_cast<T>(4)- z[62];
    z[34]=z[8]*z[34];
    z[33]=z[34] + z[33];
    z[33]=z[7]*z[33];
    z[26]=z[26] + z[33];
    z[26]=z[7]*z[26];
    z[33]= - static_cast<T>(2)- z[62];
    z[33]=z[33]*z[25];
    z[26]=z[33] + z[26];
    z[26]=z[26]*z[63];
    z[33]=static_cast<T>(1)+ z[62];
    z[28]=z[33]*z[28];
    z[26]=z[28] + z[26];
    z[26]=z[7]*z[26];
    z[26]=z[29] + z[26];
    z[26]=z[7]*z[26];
    z[23]=z[26] + z[23];
    z[23]=z[3]*z[23];
    z[26]=static_cast<T>(8)+ z[24];
    z[26]=z[26]*z[22];
    z[28]=3*z[31];
    z[29]=14*z[10] + z[50];
    z[29]=z[8]*z[29];
    z[29]=z[29] - z[28];
    z[29]=z[7]*z[29];
    z[24]=z[24] + 11;
    z[33]= - z[8]*z[24];
    z[29]=z[33] + z[29];
    z[29]=z[7]*z[29];
    z[26]=z[26] + z[29];
    z[26]=z[26]*z[63];
    z[24]= - z[24]*z[25];
    z[24]=z[24] + z[26];
    z[24]=z[24]*z[51];
    z[21]=z[23] + z[24] + z[21];
    z[21]=z[3]*z[21];
    z[23]= - 42*z[10] - z[106];
    z[23]=z[23]*z[101];
    z[24]=6*z[10];
    z[26]= - z[80] + z[24] + 7*z[12];
    z[26]=z[8]*z[26];
    z[26]=z[26] - 13*z[19];
    z[26]=z[11]*z[26];
    z[29]=static_cast<T>(7)- z[49];
    z[29]=z[8]*z[29];
    z[23]=z[26] + z[29] + z[23];
    z[23]=z[11]*z[23];
    z[26]=static_cast<T>(3)- z[47];
    z[26]=z[26]*z[22];
    z[29]=111*z[10] - z[42];
    z[29]=z[29]*z[101];
    z[29]= - 35*z[8] + z[29];
    z[29]=z[7]*z[29];
    z[23]=z[23] + z[26] + z[29];
    z[23]=z[11]*z[23];
    z[26]=73*z[8] - 141*z[20];
    z[26]=z[7]*z[26];
    z[26]= - 21*z[22] + z[26];
    z[26]=z[7]*z[26];
    z[23]=z[23] + z[25] + z[26];
    z[23]=z[11]*z[23];
    z[26]=static_cast<T>(39)+ z[108];
    z[26]=z[26]*z[22];
    z[29]=29*z[10] + z[100];
    z[29]=z[8]*z[29];
    z[29]=z[29] - 7*z[31];
    z[29]=z[7]*z[29];
    z[33]= - static_cast<T>(22)- z[108];
    z[33]=z[8]*z[33];
    z[29]=z[33] + z[29];
    z[29]=z[29]*z[63];
    z[26]=z[26] + z[29];
    z[26]=z[7]*z[26];
    z[25]= - 5*z[25] + z[26];
    z[25]=z[7]*z[25];
    z[21]=2*z[21] + z[25] + z[23];
    z[21]=z[3]*z[21];
    z[23]=z[8]*z[60];
    z[23]=z[23] - 2*z[31];
    z[23]=z[23]*z[63];
    z[25]= - static_cast<T>(7)- z[62];
    z[25]=z[25]*z[30];
    z[23]=z[25] + z[23];
    z[23]=z[23]*z[27];
    z[25]=static_cast<T>(5)+ z[22];
    z[23]=5*z[25] + z[23];
    z[23]=z[7]*z[23];
    z[25]=z[73] - z[12];
    z[25]=z[25]*z[101];
    z[26]= - 2*z[10] + z[12];
    z[26]=3*z[26] - z[91];
    z[26]=z[8]*z[26];
    z[19]=z[26] - 11*z[19];
    z[19]=z[11]*z[19];
    z[26]= - static_cast<T>(4)- z[47];
    z[26]=z[8]*z[26];
    z[19]=z[19] + z[26] + 3*z[25];
    z[19]=z[11]*z[19];
    z[20]=10*z[8] - 27*z[20];
    z[20]=z[20]*z[27];
    z[19]=z[19] + z[20] - static_cast<T>(6)- z[22];
    z[19]=z[11]*z[19];
    z[19]=z[21] + z[19] - z[32] + z[23];
    z[19]=z[3]*z[19];
    z[20]=z[41] + z[9];
    z[20]=z[8]*z[20];
    z[20]=z[20] - z[28];
    z[20]=z[7]*z[20];
    z[20]=z[20] - z[30] - z[24] - z[9];
    z[20]=z[7]*z[20];

    r += static_cast<T>(1)+ z[17] + z[18] + z[19] + z[20];
 
    return r;
}

template double qqb_2lNLC_r1347(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1347(const std::array<dd_real,31>&);
#endif
