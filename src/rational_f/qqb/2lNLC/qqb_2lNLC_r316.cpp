#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r316(const std::array<T,31>& k) {
  T z[132];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[8];
    z[7]=k[15];
    z[8]=k[10];
    z[9]=k[14];
    z[10]=k[5];
    z[11]=k[2];
    z[12]=k[3];
    z[13]=k[4];
    z[14]=k[9];
    z[15]=k[25];
    z[16]=npow(z[1],6);
    z[17]=z[16]*z[4];
    z[18]=npow(z[1],5);
    z[17]=z[17] + z[18];
    z[19]=npow(z[1],4);
    z[20]=z[19]*z[10];
    z[21]=z[20] - z[17];
    z[22]=n<T>(5,6)*z[3];
    z[21]=z[21]*z[22];
    z[23]=z[18]*z[4];
    z[21]=z[21] + n<T>(13,6)*z[19] + 3*z[23];
    z[21]=z[3]*z[21];
    z[24]=npow(z[1],2);
    z[25]=z[24]*z[10];
    z[26]=n<T>(5,2)*z[25];
    z[27]=npow(z[1],3);
    z[28]=8*z[27] + z[26];
    z[29]= - n<T>(11,6)*z[19] - z[23];
    z[29]=z[4]*z[29];
    z[21]=z[21] + n<T>(1,3)*z[28] + z[29];
    z[21]=z[3]*z[21];
    z[28]=5*z[27];
    z[29]= - z[28] - n<T>(49,4)*z[25];
    z[30]=z[27]*z[10];
    z[31]=z[19] - z[30];
    z[31]=z[4]*z[31];
    z[29]=n<T>(1,3)*z[29] + z[31];
    z[29]=z[4]*z[29];
    z[31]=z[27]*z[14];
    z[32]=n<T>(1,2)*z[10];
    z[33]=z[32]*z[31];
    z[34]=n<T>(1,2)*z[27];
    z[35]=2*z[24];
    z[36]=z[35]*z[10];
    z[33]=z[33] - z[34] - z[36];
    z[33]=z[14]*z[33];
    z[37]=z[19] - n<T>(3,4)*z[30];
    z[37]=z[4]*z[37];
    z[38]=z[23] + z[19];
    z[39]=n<T>(5,2)*z[30] - z[38];
    z[39]=z[3]*z[39];
    z[37]=z[37] + z[39];
    z[37]=z[2]*z[37];
    z[39]=n<T>(5,2)*z[24];
    z[21]=z[37] + z[33] + z[21] - z[39] + z[29];
    z[21]=z[2]*z[21];
    z[29]=npow(z[10],2);
    z[33]=z[29]*z[27];
    z[37]=npow(z[10],3);
    z[40]=z[37]*z[6];
    z[41]=2*z[27];
    z[42]=z[41]*z[40];
    z[42]= - z[33] + z[42];
    z[42]=z[6]*z[42];
    z[42]=z[30] + z[42];
    z[42]=z[6]*z[42];
    z[43]=z[37]*z[31];
    z[33]= - z[33] - 2*z[43];
    z[33]=z[14]*z[33];
    z[33]= - z[30] + z[33];
    z[33]=z[14]*z[33];
    z[44]=z[27]*z[9];
    z[45]=z[32]*z[44];
    z[46]=z[32]*z[27];
    z[47]=z[2]*z[46];
    z[33]=z[47] + z[33] + z[45] + z[42];
    z[33]=z[7]*z[33];
    z[42]=5*z[25];
    z[45]=7*z[27];
    z[47]=z[42] - z[45];
    z[47]=z[47]*z[10];
    z[48]=4*z[24];
    z[40]= - z[48]*z[40];
    z[40]=z[47] + z[40];
    z[40]=z[6]*z[40];
    z[40]= - z[41] + z[40];
    z[40]=z[6]*z[40];
    z[49]=3*z[27];
    z[50]=z[36] - z[49];
    z[51]=2*z[14];
    z[52]=z[51]*z[29];
    z[53]=z[50]*z[52];
    z[53]=z[47] + z[53];
    z[53]=z[14]*z[53];
    z[53]=z[53] - z[27] + 8*z[25];
    z[53]=z[14]*z[53];
    z[54]= - z[27] - n<T>(3,2)*z[25];
    z[54]=z[9]*z[54];
    z[55]= - z[45] + z[25];
    z[56]= - z[2]*z[30];
    z[55]=n<T>(1,2)*z[55] + z[56];
    z[55]=z[2]*z[55];
    z[56]=6*z[24];
    z[33]=z[33] + z[55] + z[53] + z[40] + z[56] + z[54];
    z[33]=z[7]*z[33];
    z[40]=z[10]*z[1];
    z[53]=z[40] - z[24];
    z[54]=z[35] + z[40];
    z[55]=z[54]*z[10];
    z[57]=z[55] + z[27];
    z[58]=z[3]*z[57];
    z[58]=z[53] + z[58];
    z[58]=z[6]*z[29]*z[58];
    z[59]=z[24] - n<T>(29,4)*z[40];
    z[59]=z[10]*z[59];
    z[60]=3*z[40];
    z[61]=11*z[24] + z[60];
    z[62]=n<T>(1,4)*z[10];
    z[61]=z[61]*z[62];
    z[61]=z[41] + z[61];
    z[63]=3*z[10];
    z[61]=z[3]*z[61]*z[63];
    z[58]=z[58] + z[59] + z[61];
    z[58]=z[6]*z[58];
    z[59]=12*z[24];
    z[61]= - z[59] - n<T>(151,12)*z[40];
    z[61]=z[10]*z[61];
    z[64]=n<T>(7,3)*z[27];
    z[61]= - z[64] + z[61];
    z[61]=z[3]*z[61];
    z[58]=z[58] + n<T>(1,2)*z[44] + z[61] + n<T>(5,4)*z[24] + 12*z[40];
    z[58]=z[6]*z[58];
    z[61]=z[30] - z[38];
    z[65]=n<T>(5,2)*z[3];
    z[61]=z[61]*z[65];
    z[66]=4*z[27];
    z[67]=z[19]*z[4];
    z[61]=z[61] + n<T>(13,2)*z[67] + z[66] + z[26];
    z[61]=z[3]*z[61];
    z[68]=n<T>(23,12)*z[24] + z[60];
    z[69]= - n<T>(21,8)*z[27] - z[67];
    z[69]=z[4]*z[69];
    z[61]=n<T>(1,3)*z[61] + n<T>(5,2)*z[68] + z[69];
    z[61]=z[3]*z[61];
    z[68]= - z[24]*z[63];
    z[69]=z[30] + z[19];
    z[70]= - z[4]*z[69];
    z[68]=z[70] - n<T>(13,4)*z[27] + z[68];
    z[68]=z[4]*z[68];
    z[70]=3*z[19];
    z[71]=z[70] + z[30];
    z[72]=z[4]*z[71];
    z[73]= - z[19]*z[32];
    z[73]= - z[18] + z[73];
    z[73]=z[4]*z[73];
    z[73]= - n<T>(1,2)*z[19] + z[73];
    z[73]=z[9]*z[73];
    z[72]=z[73] + z[41] + z[72];
    z[72]=z[9]*z[72];
    z[68]=z[72] - n<T>(1,4)*z[24] + z[68];
    z[68]=z[9]*z[68];
    z[72]=3*z[24];
    z[73]=z[72] - z[40];
    z[52]=z[73]*z[52];
    z[74]=2*z[40];
    z[75]=7*z[24];
    z[76]=z[75] + z[74];
    z[76]=z[10]*z[76];
    z[52]=z[52] - z[27] + z[76];
    z[52]=z[14]*z[52];
    z[76]=z[24]*z[9];
    z[77]=z[32]*z[76];
    z[52]=z[52] + z[77] + z[73];
    z[52]=z[14]*z[52];
    z[46]=z[46] + z[19];
    z[73]= - z[4]*z[46];
    z[20]=z[18] + z[20];
    z[20]=z[4]*z[20];
    z[20]=z[19] + z[20];
    z[77]=n<T>(1,2)*z[9];
    z[20]=z[20]*z[77];
    z[20]=z[20] - z[27] + z[73];
    z[20]=z[9]*z[20];
    z[73]=z[30]*z[14];
    z[78]= - z[27] + z[73];
    z[79]=n<T>(1,2)*z[14];
    z[78]=z[78]*z[79];
    z[80]=z[31]*z[29];
    z[81]=z[30] - z[80];
    z[81]=z[14]*z[81];
    z[82]=z[27]*z[4];
    z[83]=z[82]*z[10];
    z[81]=z[83] + z[81];
    z[81]=z[2]*z[81];
    z[84]=n<T>(1,2)*z[24];
    z[20]=n<T>(1,2)*z[81] + z[78] + z[84] + z[20];
    z[20]=z[5]*z[20];
    z[78]=n<T>(15,4)*z[24];
    z[81]=2*z[82];
    z[85]= - z[78] + z[81];
    z[85]=z[4]*z[85];
    z[20]=z[20] + z[33] + z[21] + z[52] + z[58] + z[68] + z[61] - n<T>(45,4)
   *z[1] + z[85];
    z[20]=z[5]*z[20];
    z[21]=n<T>(3,2)*z[27];
    z[33]=npow(z[4],2);
    z[52]= - z[33]*z[21];
    z[58]=z[27]*z[3];
    z[61]=z[84] + z[58];
    z[61]=z[3]*z[61];
    z[68]=3*z[82];
    z[85]=z[68] + z[58];
    z[86]=n<T>(1,2)*z[6];
    z[85]=z[85]*z[86];
    z[52]=z[85] + z[52] + z[61];
    z[52]=z[6]*z[52];
    z[61]=z[19]*z[9];
    z[85]=n<T>(1,2)*z[4];
    z[87]= - z[85]*z[61];
    z[87]=z[87] - z[24] - z[68];
    z[87]=z[9]*z[87];
    z[88]=35*z[24] + 27*z[58];
    z[88]=z[3]*z[88];
    z[89]=z[24]*z[4];
    z[87]=z[87] + n<T>(1,8)*z[88] + z[1] + n<T>(13,8)*z[89];
    z[87]=z[9]*z[87];
    z[88]=z[84] - z[44];
    z[88]=z[9]*z[88];
    z[90]=z[44] - z[24];
    z[91]=z[2]*z[90];
    z[88]= - n<T>(5,4)*z[91] + z[1] + n<T>(9,2)*z[88];
    z[88]=z[2]*z[88];
    z[91]=z[85]*z[27];
    z[92]=8*z[24];
    z[93]=n<T>(9,2)*z[27];
    z[94]=z[2]*z[93];
    z[94]=z[94] + z[92] + z[91];
    z[94]=z[7]*z[94];
    z[95]=z[85]*z[24];
    z[96]= - z[1] - z[95];
    z[97]=z[24]*z[2];
    z[94]=z[94] + 3*z[96] - 10*z[97];
    z[94]=z[7]*z[94];
    z[96]=n<T>(11,4)*z[24];
    z[98]=z[96] + z[82];
    z[98]=z[98]*z[85];
    z[99]= - z[28] + z[61];
    z[99]=z[9]*z[99];
    z[99]=n<T>(7,2)*z[24] + z[99];
    z[99]=z[99]*z[77];
    z[100]=3*z[1];
    z[98]=z[99] + z[100] + z[98];
    z[98]=z[5]*z[98];
    z[99]=3*z[58];
    z[81]=z[99] - z[24] - z[81];
    z[81]=z[9]*z[81];
    z[101]=n<T>(3,2)*z[82];
    z[102]= - z[5]*z[101];
    z[81]=z[102] + z[1] + z[81];
    z[81]=z[8]*z[81];
    z[52]=z[81] + z[98] + z[94] + z[88] + z[87] + z[52];
    z[52]=z[8]*z[52];
    z[81]=z[57]*z[4];
    z[87]=z[40] + z[24];
    z[88]= - z[81] + z[87];
    z[88]=z[4]*z[88];
    z[94]=z[87]*z[3];
    z[88]=z[88] - z[94];
    z[88]=z[6]*z[88];
    z[98]=z[33]*z[84];
    z[88]=z[98] + z[88];
    z[98]=npow(z[6],2);
    z[88]=z[88]*z[98];
    z[102]=z[33]*z[98]*z[34];
    z[103]=z[35]*z[7];
    z[104]= - z[2]*z[72];
    z[104]= - z[103] + n<T>(13,2)*z[1] + z[104];
    z[104]=z[7]*z[104];
    z[105]=z[2]*z[1];
    z[104]=n<T>(5,2)*z[105] + z[104];
    z[104]=z[7]*z[104];
    z[102]=z[102] + z[104];
    z[102]=z[8]*z[102];
    z[104]=npow(z[7],3);
    z[106]=2*z[1];
    z[107]=z[2]*z[104]*z[106];
    z[108]=z[7]*z[106];
    z[108]= - n<T>(3,2)*z[105] + z[108];
    z[109]=npow(z[7],2);
    z[108]=z[8]*z[108]*z[109];
    z[107]=z[107] + z[108];
    z[107]=z[13]*z[107];
    z[108]= - z[35] + z[60];
    z[110]=2*z[7];
    z[108]=z[2]*z[108]*z[110];
    z[105]=n<T>(3,4)*z[105] + z[108];
    z[105]=z[105]*z[109];
    z[88]=z[107] + z[102] + z[88] + z[105];
    z[88]=z[13]*z[88];
    z[102]=n<T>(19,4)*z[24] + z[40];
    z[105]=n<T>(1,2)*z[3];
    z[102]=z[102]*z[105];
    z[102]=z[102] + z[100] - z[95];
    z[102]=z[3]*z[102];
    z[107]=13*z[24];
    z[108]=7*z[40];
    z[111]= - z[107] - z[108];
    z[111]=z[4]*z[111];
    z[94]=z[111] + z[94];
    z[111]= - z[24]*z[32];
    z[111]= - z[27] + z[111];
    z[111]=z[9]*z[4]*z[111];
    z[94]=n<T>(1,2)*z[94] + z[111];
    z[94]=z[9]*z[94];
    z[111]=z[85]*z[1];
    z[94]=z[94] + z[111] + z[102];
    z[94]=z[9]*z[94];
    z[102]=z[29]*z[1];
    z[112]=z[102] - z[27];
    z[113]= - z[4]*z[112];
    z[114]=n<T>(5,2)*z[40];
    z[113]=z[113] - z[24] - z[114];
    z[113]=z[4]*z[113];
    z[115]=z[48] + z[40];
    z[115]=z[115]*z[10];
    z[115]=z[115] + z[49];
    z[116]=z[4]*z[10];
    z[117]=z[57]*z[116];
    z[118]=z[117] - z[115];
    z[118]=z[4]*z[118];
    z[115]=z[3]*z[115];
    z[115]=z[118] + z[115];
    z[115]=z[6]*z[115];
    z[118]=z[35] + z[114];
    z[118]=z[3]*z[118];
    z[113]=z[115] + z[113] + z[118];
    z[113]=z[6]*z[113];
    z[115]=npow(z[3],2);
    z[118]= - z[33] + z[115];
    z[118]=z[24]*z[118];
    z[119]=z[9]*z[95];
    z[113]=z[113] + z[119] + z[118];
    z[113]=z[6]*z[113];
    z[118]=n<T>(11,2)*z[1];
    z[119]=z[118] - z[76];
    z[120]=n<T>(15,2)*z[40];
    z[121]= - z[120] - z[90];
    z[122]=z[102]*z[6];
    z[121]=n<T>(1,2)*z[121] + z[122];
    z[121]=z[6]*z[121];
    z[119]=n<T>(1,2)*z[119] + z[121];
    z[119]=z[6]*z[119];
    z[121]= - n<T>(5,4)*z[122] + z[114] + z[90];
    z[121]=z[6]*z[121];
    z[121]= - n<T>(5,4)*z[1] + z[121];
    z[121]=z[2]*z[121];
    z[119]=z[119] + z[121];
    z[119]=z[2]*z[119];
    z[121]=4*z[40];
    z[123]=z[91] - z[121];
    z[124]=z[3]*z[102];
    z[124]=n<T>(5,4)*z[124] + z[84] + z[123];
    z[124]=z[3]*z[124];
    z[125]=25*z[40];
    z[126]= - 43*z[24] + z[125];
    z[126]=z[2]*z[126];
    z[127]=z[35] - z[40];
    z[128]= - z[127]*z[63];
    z[128]=z[27] + z[128];
    z[128]=z[2]*z[128];
    z[128]= - z[74] + z[128];
    z[128]=z[128]*z[110];
    z[129]=n<T>(1,2)*z[1];
    z[124]=z[128] + n<T>(1,4)*z[126] + z[129] + z[124];
    z[124]=z[7]*z[124];
    z[126]=z[24] + z[82];
    z[126]=z[3]*z[126];
    z[95]=z[95] + z[126];
    z[95]=z[3]*z[95];
    z[126]=n<T>(37,4)*z[1] - z[97];
    z[126]=z[2]*z[126];
    z[95]=z[124] + z[95] + z[126];
    z[95]=z[7]*z[95];
    z[124]=n<T>(31,4)*z[27] + z[67];
    z[124]=z[4]*z[124];
    z[126]=z[67] + z[27];
    z[128]=z[25] - z[126];
    z[130]=n<T>(5,3)*z[3];
    z[128]=z[128]*z[130];
    z[124]=z[128] + z[124] + n<T>(61,12)*z[24] + z[40];
    z[124]=z[124]*z[105];
    z[128]= - n<T>(25,4)*z[24] - z[82];
    z[128]=z[128]*z[85];
    z[124]=z[124] + z[1] + z[128];
    z[124]=z[3]*z[124];
    z[128]=z[9]*z[72];
    z[118]= - z[118] + z[128];
    z[90]=z[120] - z[90];
    z[90]=n<T>(1,2)*z[90] - z[122];
    z[90]=z[6]*z[90];
    z[90]=n<T>(1,2)*z[118] + z[90];
    z[90]=z[6]*z[90];
    z[90]=z[124] + z[90];
    z[90]=z[5]*z[90];
    z[52]=z[88] + z[52] + z[90] + z[95] + z[119] + z[94] + z[113];
    z[52]=z[13]*z[52];
    z[88]=z[72] + z[114];
    z[88]=z[88]*z[63];
    z[90]=n<T>(1,2)*z[40];
    z[94]=z[90] + z[24];
    z[95]=z[94]*z[29];
    z[113]=5*z[3];
    z[114]= - z[113]*z[95];
    z[88]=z[114] + z[88] - z[126];
    z[88]=z[88]*z[105];
    z[71]= - z[10]*z[71];
    z[71]= - 3*z[18] + z[71];
    z[71]=z[10]*z[71];
    z[16]= - z[16] + z[71];
    z[16]=z[4]*z[16];
    z[71]=2*z[19];
    z[114]= - z[10]*z[71];
    z[16]=z[16] - z[18] + z[114];
    z[114]=2*z[6];
    z[16]=z[16]*z[114];
    z[118]=z[71] + z[30];
    z[118]=z[10]*z[118];
    z[118]=z[18] + z[118];
    z[119]=5*z[4];
    z[118]=z[118]*z[119];
    z[16]=z[16] + z[118] + z[70] + 4*z[30];
    z[16]=z[6]*z[16];
    z[118]= - z[69]*z[119];
    z[16]=z[118] + z[16];
    z[16]=z[6]*z[16];
    z[50]= - z[50]*z[29];
    z[43]=z[50] + z[43];
    z[43]=z[14]*z[43];
    z[50]= - z[56] + z[40];
    z[50]=z[10]*z[50];
    z[50]=z[49] + z[50];
    z[50]=z[10]*z[50];
    z[43]=z[50] + z[43];
    z[43]=z[2]*z[43];
    z[50]=z[3]*z[35];
    z[118]= - z[14]*z[58];
    z[50]=z[50] + z[118];
    z[118]=z[37]*z[14];
    z[50]=z[50]*z[118];
    z[122]= - z[3]*z[1]*z[37];
    z[43]=z[43] + z[50] + z[36] + z[122];
    z[43]=z[43]*z[110];
    z[50]=z[94]*z[10];
    z[50]=z[50] + z[34];
    z[94]=z[50]*z[113]*z[29];
    z[110]=5*z[24];
    z[122]= - z[110] - 23*z[40];
    z[122]=z[122]*z[32];
    z[124]=9*z[27];
    z[122]=z[124] + z[122];
    z[122]=z[10]*z[122];
    z[94]=z[122] + z[94];
    z[94]=z[94]*z[105];
    z[122]= - z[110] + n<T>(9,2)*z[40];
    z[122]=z[10]*z[122];
    z[94]=z[122] + z[94];
    z[94]=z[9]*z[94];
    z[122]=z[37]*z[82]*z[51];
    z[128]=z[82] - z[58];
    z[128]=z[29]*z[128];
    z[122]=5*z[128] + z[122];
    z[122]=z[14]*z[122];
    z[128]= - z[28] + 7*z[25];
    z[131]=z[3]*z[10]*z[128];
    z[122]=z[122] + 5*z[83] + z[131];
    z[122]=z[14]*z[122];
    z[47]= - z[47] + z[80];
    z[47]=z[14]*z[47];
    z[80]= - n<T>(53,4)*z[24] + z[121];
    z[80]=z[10]*z[80];
    z[47]=z[47] + n<T>(21,2)*z[27] + z[80];
    z[47]=z[2]*z[47];
    z[16]=z[43] + z[47] + z[122] + z[16] + z[94] + z[88] + 5*z[82] + 
    z[75] - n<T>(17,2)*z[40];
    z[16]=z[7]*z[16];
    z[43]= - z[24] + z[90];
    z[43]=z[10]*z[43];
    z[43]=n<T>(5,2)*z[43] - z[126];
    z[43]=z[3]*z[43];
    z[43]=z[43] + n<T>(9,2)*z[82] + z[39] - z[121];
    z[43]=z[3]*z[43];
    z[47]= - z[128]*z[116];
    z[80]=z[89]*z[118];
    z[88]=z[3]*z[30];
    z[47]= - 4*z[80] + z[47] - 6*z[88];
    z[47]=z[14]*z[47];
    z[80]=z[36] - z[27];
    z[88]= - z[80]*z[119];
    z[94]= - z[27] + 4*z[25];
    z[94]=z[3]*z[94];
    z[47]=z[47] + z[88] + 3*z[94];
    z[47]=z[14]*z[47];
    z[88]= - 41*z[24] - 27*z[40];
    z[88]=z[88]*z[62];
    z[94]=z[25] + z[27];
    z[118]=z[94]*z[10];
    z[65]=z[118]*z[65];
    z[65]=z[65] + z[27] + z[88];
    z[65]=z[3]*z[65];
    z[88]=17*z[24] + z[108];
    z[65]=n<T>(1,2)*z[88] + z[65];
    z[65]=z[9]*z[65];
    z[88]=z[63]*z[27];
    z[88]=z[88] + z[19];
    z[119]=z[49] + z[25];
    z[119]=z[10]*z[119];
    z[119]=z[70] + z[119];
    z[119]=z[10]*z[119];
    z[119]=z[18] + z[119];
    z[119]=z[4]*z[119];
    z[119]=2*z[119] + z[88];
    z[119]=z[119]*z[114];
    z[122]= - static_cast<T>(2)- 7*z[116];
    z[122]=z[94]*z[122];
    z[119]=z[119] + z[122];
    z[119]=z[6]*z[119];
    z[122]= - z[45] + z[36];
    z[122]=z[4]*z[122];
    z[119]=z[119] + z[122] - 9*z[44];
    z[119]=z[6]*z[119];
    z[122]=z[42] - z[41];
    z[73]=z[73] - z[122];
    z[73]=z[73]*z[51];
    z[73]=z[73] - 18*z[24] + z[108];
    z[73]=z[2]*z[73];
    z[16]=z[16] + z[73] + z[47] + z[119] + z[65] + z[43] - z[1] - 5*
    z[89];
    z[16]=z[7]*z[16];
    z[43]=z[19]*z[3];
    z[47]=n<T>(23,8)*z[27] + z[43];
    z[47]=z[3]*z[47];
    z[65]=z[9]*z[23];
    z[65]= - 3*z[67] + z[65];
    z[65]=z[65]*z[77];
    z[47]=z[65] + z[47] - z[35] - n<T>(27,8)*z[82];
    z[47]=z[9]*z[47];
    z[65]=z[93] - z[43];
    z[65]=z[3]*z[65];
    z[73]=z[49] - z[43];
    z[73]=z[73]*z[86];
    z[39]=z[73] + z[65] + z[39] - z[68];
    z[39]=z[6]*z[39];
    z[65]=z[18]*z[3];
    z[68]= - z[19] + z[65];
    z[68]=z[9]*z[68];
    z[68]=z[68] + z[27] - z[43];
    z[68]=z[2]*z[68];
    z[73]=z[34] - z[43];
    z[73]=z[3]*z[73];
    z[65]=z[70] + z[65];
    z[65]=z[3]*z[65];
    z[65]= - n<T>(17,4)*z[27] + z[65];
    z[65]=z[9]*z[65];
    z[65]=z[68] + z[65] - z[96] + z[73];
    z[65]=z[2]*z[65];
    z[68]=z[49]*z[6];
    z[70]=z[2]*z[41];
    z[70]=z[70] - z[68] + 10*z[24] + z[101];
    z[70]=z[7]*z[70];
    z[73]=2*z[9];
    z[73]= - z[38]*z[73];
    z[86]=2*z[67];
    z[73]=z[73] + z[34] + z[86];
    z[73]=z[5]*z[73];
    z[86]=z[9]*z[86];
    z[73]=z[73] + z[86] + z[24] - n<T>(7,2)*z[58];
    z[73]=z[8]*z[73];
    z[43]=z[28] - z[43];
    z[43]=z[43]*z[79];
    z[43]=z[43] + n<T>(5,2)*z[58] - z[68];
    z[43]=z[14]*z[43];
    z[17]= - z[17]*z[77];
    z[17]=z[17] + n<T>(3,2)*z[19] + 2*z[23];
    z[17]=z[9]*z[17];
    z[23]=z[28] - n<T>(7,2)*z[67];
    z[17]=n<T>(1,4)*z[23] + z[17];
    z[17]=z[9]*z[17];
    z[23]= - 55*z[24] + 21*z[82];
    z[68]=5*z[31];
    z[17]=z[68] + n<T>(1,8)*z[23] + z[17];
    z[17]=z[5]*z[17];
    z[23]= - z[72] - 23*z[58];
    z[23]=z[3]*z[23];
    z[23]=n<T>(1,4)*z[23] - 17*z[1] - z[89];
    z[17]=z[73] + z[17] + z[70] + z[65] + z[43] + z[39] + n<T>(1,2)*z[23] + 
    z[47];
    z[17]=z[8]*z[17];
    z[23]=n<T>(9,4)*z[24] + z[40];
    z[23]=z[10]*z[23];
    z[23]= - n<T>(3,4)*z[27] + z[23];
    z[23]=z[10]*z[23];
    z[39]=z[29]*z[4];
    z[43]= - z[112]*z[39];
    z[23]=z[23] + z[43];
    z[23]=z[4]*z[23];
    z[43]=z[81]*z[37];
    z[47]=z[112]*z[29];
    z[47]=z[47] + z[43];
    z[47]=z[4]*z[47];
    z[65]= - z[29]*z[35];
    z[47]=z[65] + z[47];
    z[47]=z[6]*z[47];
    z[65]=z[72] + n<T>(13,4)*z[40];
    z[65]=z[10]*z[65];
    z[70]=z[19]*z[77];
    z[23]=z[47] + z[70] + z[23] - z[34] + z[65];
    z[23]=z[6]*z[23];
    z[34]=n<T>(23,2)*z[24] + n<T>(29,3)*z[40];
    z[34]=z[34]*z[32];
    z[34]=z[83] + z[64] + z[34];
    z[34]=z[4]*z[34];
    z[47]=8*z[40];
    z[23]=z[23] - n<T>(9,2)*z[44] + z[34] - n<T>(3,4)*z[24] - z[47];
    z[23]=z[6]*z[23];
    z[34]=z[10]*z[46];
    z[18]=n<T>(1,2)*z[18] + z[34];
    z[18]=z[18]*z[113];
    z[18]=z[18] - z[88];
    z[18]=z[18]*z[77];
    z[34]=z[69]*z[3];
    z[18]=z[18] - n<T>(5,4)*z[34] + z[66] + z[67];
    z[18]=z[3]*z[18];
    z[46]=z[87]*z[39];
    z[64]= - z[84] - z[40];
    z[64]=z[10]*z[64];
    z[64]=z[46] - z[64];
    z[50]=z[50]*z[39];
    z[50]=z[95] + z[50];
    z[50]=z[6]*z[50];
    z[50]=n<T>(5,2)*z[50] - z[61] + z[27] - 5*z[64];
    z[50]=z[6]*z[50];
    z[64]=5*z[40];
    z[65]=z[84] + z[64];
    z[65]=z[10]*z[65];
    z[65]= - z[27] + n<T>(3,2)*z[65];
    z[65]=z[4]*z[65];
    z[18]=z[50] + z[65] - z[35] + z[120] + z[18];
    z[18]=z[2]*z[18];
    z[22]=z[38]*z[22];
    z[22]=z[22] - n<T>(13,6)*z[67] - z[27] - z[26];
    z[22]=z[3]*z[22];
    z[26]= - n<T>(65,12)*z[27] + z[67];
    z[26]=z[4]*z[26];
    z[38]=n<T>(17,4)*z[24];
    z[22]=z[22] + z[26] + z[38] + z[121];
    z[22]=z[3]*z[22];
    z[26]=z[9]*z[30];
    z[26]= - n<T>(15,2)*z[26] + z[122];
    z[26]=z[9]*z[26];
    z[50]=z[6]*z[80];
    z[65]= - z[29]*z[44];
    z[65]= - z[30] + z[65];
    z[65]=z[9]*z[65];
    z[30]= - z[6]*z[30];
    z[30]=n<T>(3,2)*z[65] + z[30];
    z[30]=z[14]*z[30];
    z[26]=z[30] + z[50] - z[35] + z[26];
    z[26]=z[14]*z[26];
    z[30]= - z[4]*z[94];
    z[30]=z[30] + z[38] - z[60];
    z[30]=z[4]*z[30];
    z[38]=z[45] - 13*z[25];
    z[42]=z[45] + z[42];
    z[42]=z[42]*z[32];
    z[42]=z[71] + z[42];
    z[42]=z[3]*z[42];
    z[38]=n<T>(1,2)*z[38] + z[42];
    z[38]=z[3]*z[38];
    z[42]=z[69]*z[105];
    z[42]= - 11*z[27] + z[42];
    z[42]=z[9]*z[42];
    z[38]=z[42] + n<T>(43,4)*z[24] + z[38];
    z[38]=z[9]*z[38];
    z[18]=z[18] + z[26] + z[23] + z[38] + z[22] + n<T>(33,4)*z[1] + z[30];
    z[18]=z[2]*z[18];
    z[22]=z[9]*z[1];
    z[23]= - z[37]*z[22];
    z[23]=4*z[102] + z[23];
    z[26]=3*z[9];
    z[23]=z[23]*z[26];
    z[30]=9*z[40];
    z[23]=z[23] + z[48] - z[30];
    z[23]=z[14]*z[23];
    z[38]=13*z[40];
    z[42]=6*z[102];
    z[45]= - z[9]*z[42];
    z[45]=z[38] + z[45];
    z[45]=z[9]*z[45];
    z[50]=7*z[1];
    z[23]=z[23] - z[50] + z[45];
    z[23]=z[14]*z[23];
    z[45]=z[35]*z[9];
    z[65]=z[44]*z[6];
    z[69]=z[45] - z[65];
    z[69]=z[69]*z[6];
    z[70]= - z[9]*z[60];
    z[70]=z[106] + z[70];
    z[70]=z[9]*z[70];
    z[23]=z[23] + z[70] + z[69];
    z[23]=z[14]*z[23];
    z[70]=z[27]*z[6];
    z[73]=z[70] - z[35];
    z[73]=z[73]*z[6];
    z[73]=z[73] + z[1];
    z[79]= - z[14]*z[49];
    z[79]=z[24] + z[79];
    z[79]=z[14]*z[79];
    z[79]=z[79] + z[73];
    z[79]=z[14]*z[79];
    z[81]=z[70] - z[24];
    z[81]=z[81]*z[7];
    z[83]= - z[27]*z[98];
    z[83]=5*z[81] + z[129] + z[83];
    z[83]=z[7]*z[83];
    z[86]=z[106]*z[9];
    z[88]=z[8]*z[1];
    z[79]= - z[88] + z[83] - z[86] + z[79];
    z[79]=z[8]*z[79];
    z[70]=z[110] - z[70];
    z[70]=z[6]*z[70];
    z[70]= - 6*z[81] + z[106] + z[70];
    z[70]=z[8]*z[70];
    z[81]= - z[9]*z[75];
    z[65]=z[81] + 5*z[65];
    z[65]=z[6]*z[65];
    z[65]=z[70] + z[86] + z[65];
    z[65]=z[109]*z[65];
    z[70]= - z[8]*z[73];
    z[69]=z[70] + z[22] - z[69];
    z[69]=z[11]*z[104]*z[69];
    z[70]=z[104]*z[13];
    z[70]=6*z[70];
    z[73]=z[88]*z[70];
    z[65]=2*z[69] + z[73] + z[65];
    z[65]=z[11]*z[65];
    z[69]=z[75] - z[60];
    z[69]=z[9]*z[69];
    z[73]= - z[6]*z[19];
    z[73]=z[41] + z[73];
    z[73]=z[73]*z[114];
    z[73]= - 7*z[44] + z[73];
    z[73]=z[6]*z[73];
    z[69]=z[73] + z[100] + z[69];
    z[69]=z[7]*z[69];
    z[73]= - z[35] + 5*z[44];
    z[73]=z[6]*z[73];
    z[45]= - z[45] + z[73];
    z[45]=z[6]*z[45];
    z[22]=z[69] - n<T>(7,2)*z[22] + z[45];
    z[22]=z[7]*z[22];
    z[45]= - z[70] - 7*z[109];
    z[45]=z[88]*z[45];
    z[69]=4*z[1];
    z[70]= - z[104]*z[69];
    z[45]=z[70] + z[45];
    z[45]=z[13]*z[45];
    z[70]=npow(z[14],2);
    z[73]=z[24]*z[14];
    z[81]= - z[1] + 6*z[73];
    z[81]=z[81]*z[70];
    z[83]=npow(z[14],3);
    z[86]= - z[12]*z[83]*z[100];
    z[81]=z[86] + z[81];
    z[81]=z[8]*z[81];
    z[86]=z[83]*z[1];
    z[88]=4*z[86];
    z[81]= - z[88] + z[81];
    z[81]=z[12]*z[81];
    z[93]= - z[1]*npow(z[9],2);
    z[22]=z[65] + z[81] + z[45] + z[79] + z[22] + z[93] + z[23];
    z[22]=z[11]*z[22];
    z[23]= - 21*z[24] + z[58];
    z[23]=n<T>(1,2)*z[23] + 4*z[31];
    z[23]=z[14]*z[23];
    z[45]=n<T>(3,2)*z[24];
    z[65]= - z[3]*z[45];
    z[23]=z[23] + z[1] + z[65];
    z[23]=z[14]*z[23];
    z[65]=z[24] - n<T>(1,2)*z[58];
    z[65]=z[3]*z[65];
    z[79]=n<T>(3,2)*z[1];
    z[65]= - z[79] + z[65];
    z[65]=z[2]*z[65];
    z[81]= - z[24] - z[31];
    z[81]=z[14]*z[81];
    z[81]=z[100] + z[81];
    z[93]=2*z[5];
    z[81]=z[81]*z[93];
    z[94]=z[58] - z[24];
    z[94]=z[94]*z[2];
    z[95]=z[5]*z[72];
    z[95]= - z[94] + z[95];
    z[95]=z[8]*z[95];
    z[23]=n<T>(1,2)*z[95] + z[81] + z[23] + z[65];
    z[23]=z[8]*z[23];
    z[65]=z[73] - z[1];
    z[65]=z[65]*z[70];
    z[73]= - z[35] + z[31];
    z[73]=z[14]*z[73];
    z[73]=z[1] + z[73];
    z[81]=z[5]*z[14];
    z[73]=z[73]*z[81];
    z[73]=10*z[65] + z[73];
    z[73]=z[5]*z[73];
    z[73]=z[88] + z[73];
    z[73]=z[8]*z[73];
    z[83]= - z[83]*z[106];
    z[88]=z[12]*z[8]*z[86];
    z[83]=z[88] + z[83];
    z[83]=z[83]*npow(z[5],2);
    z[88]= - z[65]*z[93];
    z[88]= - 5*z[86] + z[88];
    z[88]=z[8]*z[5]*z[88];
    z[83]=z[88] + z[83];
    z[83]=z[12]*z[83];
    z[88]=z[14]*z[54];
    z[88]= - z[69] + z[88];
    z[88]=z[5]*z[88]*z[70];
    z[88]=6*z[86] + z[88];
    z[88]=z[5]*z[88];
    z[73]=z[83] + z[88] + z[73];
    z[73]=z[12]*z[73];
    z[56]= - z[56] - z[64];
    z[56]=z[14]*z[56];
    z[56]=z[50] + z[56];
    z[56]=z[56]*z[70];
    z[45]=z[45] + z[74];
    z[45]=z[14]*z[45];
    z[45]= - z[106] + z[45];
    z[45]=z[45]*z[81];
    z[45]=z[56] + z[45];
    z[45]=z[5]*z[45];
    z[56]=z[59] - z[68];
    z[56]=z[14]*z[56];
    z[59]=5*z[1];
    z[56]= - z[59] + z[56];
    z[56]=z[56]*z[81];
    z[56]= - 8*z[65] + z[56];
    z[56]=z[8]*z[56];
    z[65]=z[58]*z[85];
    z[68]= - z[89] + z[65];
    z[68]=z[3]*z[68];
    z[68]=z[111] + z[68];
    z[68]=z[68]*npow(z[2],2);
    z[45]=z[73] + z[56] + z[45] - z[86] + n<T>(5,2)*z[68];
    z[45]=z[12]*z[45];
    z[56]=z[102]*z[4];
    z[68]=z[56] + z[84] + z[60];
    z[68]=z[4]*z[68];
    z[56]= - z[56] + z[53];
    z[51]=z[56]*z[51];
    z[50]=z[51] - z[65] + z[50] + z[68];
    z[50]=z[14]*z[50];
    z[33]= - z[24]*z[33];
    z[51]= - z[84] + z[82];
    z[51]=z[3]*z[4]*z[51];
    z[33]=z[50] + z[33] + z[51];
    z[33]=z[14]*z[33];
    z[50]= - z[67]*z[130];
    z[50]=z[50] + z[72] + n<T>(49,6)*z[82];
    z[50]=z[50]*z[105];
    z[51]= - z[78] - z[82];
    z[51]=z[4]*z[51];
    z[50]=z[50] - z[69] + z[51];
    z[50]=z[3]*z[50];
    z[51]= - z[4]*z[53];
    z[53]= - z[82] - n<T>(1,4)*z[58];
    z[53]=z[3]*z[53];
    z[51]=z[53] - n<T>(3,4)*z[1] + z[51];
    z[51]=z[2]*z[51];
    z[53]=z[79] + z[89];
    z[53]=z[4]*z[53];
    z[50]=5*z[51] + z[53] + z[50];
    z[50]=z[2]*z[50];
    z[51]=n<T>(13,2)*z[24];
    z[42]=z[14]*z[42];
    z[42]=z[42] - z[51] + z[40];
    z[42]=z[14]*z[42];
    z[42]=z[59] + z[42];
    z[42]=z[14]*z[42];
    z[31]=z[40] + n<T>(1,2)*z[31];
    z[31]=z[31]*z[81];
    z[31]=z[42] + z[31];
    z[31]=z[5]*z[31];
    z[23]=z[45] + z[23] + z[31] + z[33] + z[50];
    z[23]=z[12]*z[23];
    z[31]=z[57]*z[39];
    z[33]=z[75] + z[47];
    z[33]=z[10]*z[33];
    z[33]= - z[27] + z[33];
    z[33]=z[10]*z[33];
    z[33]=2*z[33] - z[31];
    z[33]=z[4]*z[33];
    z[42]= - z[51] - z[60];
    z[42]=z[42]*z[29];
    z[42]=z[42] - 4*z[31];
    z[42]=z[42]*z[26];
    z[45]=n<T>(11,2)*z[24] + z[60];
    z[45]=z[45]*z[63];
    z[33]=z[42] + z[45] + z[33];
    z[33]=z[9]*z[33];
    z[42]=z[75] + z[121];
    z[29]=z[42]*z[29];
    z[42]=z[110] + z[121];
    z[42]=z[10]*z[42];
    z[42]=z[27] + z[42];
    z[42]=z[42]*z[39];
    z[29]=z[29] + z[42];
    z[37]= - z[54]*z[37];
    z[37]=z[37] - z[43];
    z[37]=z[37]*z[26];
    z[29]=2*z[29] + z[37];
    z[29]=z[9]*z[29];
    z[37]= - z[48] - z[64];
    z[37]=z[10]*z[37];
    z[35]= - z[35] - z[60];
    z[35]=z[35]*z[39];
    z[29]=z[29] + z[35] + z[49] + z[37];
    z[29]=z[14]*z[29];
    z[35]= - z[72] - z[74];
    z[35]=z[10]*z[35];
    z[35]=z[35] + z[46];
    z[35]=z[4]*z[35];
    z[34]=z[6]*z[34];
    z[29]=z[29] + z[34] + z[33] - z[99] - 2*z[127] + z[35];
    z[29]=z[14]*z[29];
    z[33]=15*z[24] + 19*z[40];
    z[33]=z[10]*z[33];
    z[34]= - z[72] - z[40];
    z[34]=z[10]*z[34];
    z[34]= - z[41] + z[34];
    z[34]=z[34]*z[116];
    z[33]=z[34] - z[66] + z[33];
    z[33]=z[4]*z[33];
    z[34]= - z[92] - z[60];
    z[34]=z[10]*z[34];
    z[34]=z[34] - 6*z[117];
    z[26]=z[34]*z[26];
    z[21]= - z[21] - z[36];
    z[21]=z[3]*z[21];
    z[21]=z[26] + z[21] + z[33] + z[75] + z[30];
    z[21]=z[9]*z[21];
    z[26]= - z[3]*z[80];
    z[30]= - z[6]*z[61];
    z[26]=z[30] + z[26] + 3*z[44];
    z[26]=z[6]*z[26];
    z[30]=z[4]*z[55];
    z[30]= - z[84] + z[30];
    z[30]=z[4]*z[30];
    z[33]=z[24] + z[91];
    z[33]=z[33]*z[113];
    z[21]=z[29] + z[26] + z[21] + z[30] + z[33];
    z[21]=z[14]*z[21];
    z[26]=31*z[24];
    z[29]= - z[26] - z[64];
    z[29]=z[29]*z[32];
    z[29]= - z[124] + z[29];
    z[30]=z[112]*z[116];
    z[29]=n<T>(1,2)*z[29] + z[30];
    z[29]=z[4]*z[29];
    z[30]= - z[41] - z[55];
    z[30]=z[10]*z[30];
    z[30]= - z[19] + z[30];
    z[30]=2*z[30] - z[31];
    z[30]=z[4]*z[30];
    z[31]=z[10]*z[87];
    z[31]= - z[41] + z[31];
    z[31]=z[10]*z[31];
    z[31]= - z[71] + z[31];
    z[31]=z[3]*z[31];
    z[30]=z[30] + z[31];
    z[30]=z[6]*z[30];
    z[26]=z[26] + z[38];
    z[26]=z[26]*z[62];
    z[26]=z[28] + z[26];
    z[26]=z[3]*z[26];
    z[26]=z[30] + n<T>(7,2)*z[44] + z[26] - z[24] + z[29];
    z[26]=z[6]*z[26];
    z[28]= - z[82] + z[110] - z[90];
    z[28]=z[4]*z[28];
    z[29]= - z[24] - z[60];
    z[29]=n<T>(1,2)*z[29] - z[58];
    z[29]=z[3]*z[29];
    z[30]= - z[72] - z[82];
    z[30]=z[30]*z[77];
    z[26]=z[26] + z[30] + z[28] + z[29];
    z[26]=z[6]*z[26];
    z[28]=z[24] + z[58];
    z[28]=z[13]*z[9]*z[28];
    z[29]= - z[12]*z[94];
    z[28]=z[29] + z[28];
    z[28]=z[3]*z[28];
    z[29]= - z[27]*z[115];
    z[29]= - z[1] + z[29];
    z[28]= - z[103] - z[97] + 2*z[29] + z[76] + z[28];
    z[28]=z[15]*z[28];
    z[29]= - 51*z[24] - z[125];
    z[29]=z[10]*z[29];
    z[29]= - 29*z[27] + z[29];
    z[29]=z[29]*z[85];
    z[30]=z[19] - z[118];
    z[30]=z[4]*z[30];
    z[30]= - z[25] + z[30];
    z[30]=z[30]*z[77];
    z[29]=z[30] + z[29] - z[107] - z[60];
    z[29]=z[9]*z[29];
    z[25]=z[41] + z[25];
    z[25]=z[3]*z[25];
    z[25]=z[25] - n<T>(23,8)*z[24] + z[123];
    z[25]=z[3]*z[25];
    z[30]=n<T>(71,4)*z[24] + 21*z[40];
    z[30]=z[4]*z[30];
    z[30]=19*z[1] + z[30];
    z[25]=z[29] + n<T>(1,2)*z[30] + z[25];
    z[25]=z[9]*z[25];
    z[29]=n<T>(1,3)*z[24] + z[90];
    z[30]= - n<T>(15,4)*z[27] - z[67];
    z[30]=z[4]*z[30];
    z[29]=11*z[29] + z[30];
    z[19]=z[19]*z[85];
    z[19]=z[27] + z[19];
    z[19]=z[19]*z[130];
    z[19]=n<T>(1,2)*z[29] + z[19];
    z[19]=z[3]*z[19];
    z[24]= - n<T>(9,8)*z[24] + z[82];
    z[24]=z[4]*z[24];
    z[19]=z[19] - n<T>(11,4)*z[1] + z[24];
    z[19]=z[3]*z[19];
    z[24]=n<T>(7,2)*z[1] - z[89];
    z[24]=z[4]*z[24];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[23]
       + n<T>(3,2)*z[24] + z[25] + z[26] + 2*z[28] + z[52];
 
    return r;
}

template double qqb_2lNLC_r316(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r316(const std::array<dd_real,31>&);
#endif
