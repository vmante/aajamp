#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r750(const std::array<T,31>& k) {
  T z[105];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[9];
    z[10]=k[4];
    z[11]=k[5];
    z[12]=k[15];
    z[13]=k[16];
    z[14]=k[2];
    z[15]=k[8];
    z[16]=k[6];
    z[17]=npow(z[8],3);
    z[18]=z[17]*z[9];
    z[19]=12*z[18];
    z[20]=npow(z[8],2);
    z[21]=n<T>(71,8)*z[20] + z[19];
    z[21]=z[9]*z[21];
    z[21]= - n<T>(7,4)*z[8] + z[21];
    z[22]=3*z[9];
    z[21]=z[21]*z[22];
    z[23]=3*z[8];
    z[24]=z[23] + z[10];
    z[25]=npow(z[10],2);
    z[26]=z[25]*z[5];
    z[27]=2*z[24] + z[26];
    z[28]=4*z[3];
    z[27]=z[27]*z[28];
    z[29]=z[5]*z[10];
    z[30]=z[2]*z[10];
    z[31]=z[25]*z[6];
    z[32]=z[10] + n<T>(1,4)*z[31];
    z[32]=z[6]*z[32];
    z[21]=z[27] - n<T>(41,8)*z[29] + n<T>(21,2)*z[32] - 6*z[30] - n<T>(121,8) + 
    z[21];
    z[21]=z[3]*z[21];
    z[27]=npow(z[9],2);
    z[32]=2*z[27];
    z[33]=z[9]*z[8];
    z[34]= - static_cast<T>(1)+ 3*z[33];
    z[34]=z[34]*z[32];
    z[35]=npow(z[15],2);
    z[36]=z[35]*z[4];
    z[37]=npow(z[6],2);
    z[38]=z[37]*z[5];
    z[39]=z[36] - z[38];
    z[40]=z[37]*z[2];
    z[39]= - n<T>(21,2)*z[40] - 13*z[39];
    z[39]=z[11]*z[39];
    z[41]=z[5]*z[6];
    z[42]=z[2]*z[4];
    z[43]=n<T>(5,8)*z[6];
    z[44]=17*z[2] - z[43];
    z[44]=z[6]*z[44];
    z[34]=n<T>(1,4)*z[39] + n<T>(71,4)*z[41] + z[44] + z[34] + n<T>(215,8)*z[42];
    z[34]=z[11]*z[34];
    z[39]=npow(z[8],4);
    z[44]=z[39]*z[9];
    z[45]= - z[17] - z[44];
    z[46]=6*z[9];
    z[45]=z[45]*z[46];
    z[45]= - z[20] + z[45];
    z[45]=z[9]*z[45];
    z[47]=6*z[8];
    z[48]=z[8] + z[10];
    z[49]=z[47]*z[48];
    z[50]= - z[3]*z[49];
    z[24]=z[50] + 3*z[24] + z[45];
    z[45]=2*z[3];
    z[24]=z[24]*z[45];
    z[50]=z[20] + z[18];
    z[50]=z[50]*z[46];
    z[50]=z[8] + z[50];
    z[51]=4*z[9];
    z[50]=z[50]*z[51];
    z[52]=z[20]*z[9];
    z[53]= - z[8] - z[52];
    z[53]=z[53]*z[46];
    z[53]= - static_cast<T>(1)+ z[53];
    z[54]=2*z[9];
    z[53]=z[53]*z[54];
    z[55]=7*z[4];
    z[53]=z[55] + z[53];
    z[53]=z[11]*z[53];
    z[56]=z[4]*z[10];
    z[57]= - static_cast<T>(1)- z[56];
    z[24]=z[53] + z[24] + 7*z[57] + z[50];
    z[24]=z[7]*z[24];
    z[50]= - 11*z[8] - 21*z[52];
    z[50]=z[50]*z[27];
    z[53]=6*z[4];
    z[50]= - z[53] + z[50];
    z[57]=n<T>(1,2)*z[6];
    z[58]=z[25]*z[4];
    z[59]=n<T>(5,2)*z[10] - 13*z[58];
    z[59]=z[59]*z[57];
    z[60]= - n<T>(21,4) + z[56];
    z[59]=13*z[60] + z[59];
    z[59]=z[59]*z[57];
    z[21]=z[24] + z[34] + z[21] + n<T>(29,2)*z[5] + 2*z[50] + z[59];
    z[21]=z[7]*z[21];
    z[24]=2*z[2];
    z[34]= - static_cast<T>(7)+ 3*z[30];
    z[34]=z[34]*z[24];
    z[50]=n<T>(1,4)*z[5];
    z[59]= - n<T>(101,2) + 11*z[29];
    z[59]=z[59]*z[50];
    z[60]=23*z[27];
    z[61]=z[20]*z[60];
    z[62]=z[6]*z[10];
    z[61]= - n<T>(165,4)*z[62] - n<T>(357,4) + z[61];
    z[63]= - 27*z[10] + n<T>(23,4)*z[26];
    z[63]=z[5]*z[63];
    z[61]=n<T>(1,4)*z[61] + z[63];
    z[61]=z[3]*z[61];
    z[63]= - n<T>(79,4)*z[8] - 38*z[52];
    z[63]=z[9]*z[63];
    z[63]=static_cast<T>(15)+ z[63];
    z[63]=z[9]*z[63];
    z[64]= - n<T>(11,2) - z[62];
    z[64]=z[6]*z[64];
    z[34]=z[61] + z[59] + n<T>(13,8)*z[64] + z[63] + z[34];
    z[34]=z[3]*z[34];
    z[59]=z[6]*z[2];
    z[61]=npow(z[2],2);
    z[63]=n<T>(13,4)*z[59] + z[32] - 7*z[61];
    z[63]=z[6]*z[63];
    z[64]=npow(z[5],2);
    z[65]=z[64]*z[37];
    z[66]=z[61]*z[37];
    z[66]=z[66] + z[65];
    z[67]=2*z[11];
    z[66]=z[66]*z[67];
    z[67]=npow(z[4],2);
    z[68]=2*z[67];
    z[69]=z[68] + z[42];
    z[69]=z[2]*z[69];
    z[70]= - 17*z[37] - n<T>(19,4)*z[41];
    z[71]=n<T>(1,2)*z[5];
    z[70]=z[70]*z[71];
    z[63]=z[66] + z[70] + z[63] + 19*z[36] + z[69];
    z[63]=z[11]*z[63];
    z[66]=static_cast<T>(2)+ 9*z[33];
    z[66]=z[66]*z[32];
    z[69]= - n<T>(431,2)*z[4] - 283*z[2];
    z[70]=8*z[56];
    z[72]= - n<T>(17,2) + z[70];
    z[72]=z[6]*z[72];
    z[69]=n<T>(1,8)*z[69] + z[72];
    z[69]=z[6]*z[69];
    z[72]=z[4]*z[15];
    z[73]=n<T>(261,8)*z[4] + 11*z[2];
    z[73]=z[2]*z[73];
    z[74]= - 269*z[6] - 35*z[5];
    z[74]=z[5]*z[74];
    z[21]=z[21] + z[63] + z[34] + n<T>(1,8)*z[74] + z[69] + z[73] + 21*z[72]
    + z[66];
    z[21]=z[7]*z[21];
    z[34]= - z[8] - 5*z[52];
    z[34]=z[34]*z[32];
    z[63]= - static_cast<T>(3)- z[29];
    z[63]=z[63]*z[45];
    z[66]=9*z[6];
    z[69]=4*z[5];
    z[73]=3*z[2];
    z[34]=z[63] + z[69] - z[66] + z[34] + z[73];
    z[34]=z[34]*z[45];
    z[63]=z[54]*z[17];
    z[74]=z[20] + z[63];
    z[74]=z[74]*z[32];
    z[47]=z[3]*z[47];
    z[47]=z[47] - 3*z[62] - static_cast<T>(3)+ z[74];
    z[47]=z[47]*z[45];
    z[74]=z[54]*z[20];
    z[75]= - z[8] - z[74];
    z[75]=z[75]*z[27];
    z[76]= - 13*z[59] + 7*z[42];
    z[76]=z[11]*z[76];
    z[77]=7*z[56];
    z[78]=static_cast<T>(13)- z[77];
    z[78]=z[6]*z[78];
    z[47]=z[76] + z[47] + z[78] - z[55] + 4*z[75];
    z[47]=z[7]*z[47];
    z[55]=n<T>(13,2)*z[38];
    z[75]=z[61]*z[6];
    z[76]=4*z[75];
    z[78]=z[61]*z[4];
    z[79]=z[55] + 9*z[78] + z[76];
    z[79]=z[11]*z[79];
    z[80]=6*z[42];
    z[81]=npow(z[9],3);
    z[82]=z[81]*z[8];
    z[83]=5*z[82] - z[80];
    z[84]=2*z[4];
    z[85]= - z[84] + z[73];
    z[85]=6*z[85] - n<T>(13,4)*z[6];
    z[85]=z[6]*z[85];
    z[34]=z[47] + z[79] + z[34] + n<T>(103,4)*z[41] + 2*z[83] + z[85];
    z[34]=z[7]*z[34];
    z[47]=6*z[65];
    z[79]=2*z[37];
    z[83]=npow(z[2],3);
    z[85]=z[79]*z[83];
    z[86]=z[85]*z[11];
    z[87]=3*z[42];
    z[88]=z[68] - z[87];
    z[88]=z[88]*z[61];
    z[76]= - 3*z[83] - z[76];
    z[76]=z[6]*z[76];
    z[76]=z[86] + z[47] + z[88] + z[76];
    z[76]=z[11]*z[76];
    z[88]=2*z[6];
    z[89]=z[84] - z[2];
    z[90]=z[89]*z[88];
    z[91]= - n<T>(205,16)*z[4] + 12*z[2];
    z[91]=z[2]*z[91];
    z[90]=z[91] + z[90];
    z[90]=z[6]*z[90];
    z[91]= - 4*z[37] + n<T>(29,8)*z[41];
    z[91]=z[5]*z[91];
    z[69]= - 27*z[6] + z[69];
    z[69]=z[5]*z[69];
    z[69]=16*z[82] + z[69];
    z[69]=z[3]*z[69];
    z[92]=2*z[81];
    z[34]=z[34] + z[76] + z[69] + z[91] + z[90] - z[92] - 13*z[78];
    z[34]=z[7]*z[34];
    z[69]=3*z[41];
    z[76]= - z[6] + z[5];
    z[76]=z[3]*z[76];
    z[76]=z[76] + z[82] + z[69];
    z[76]=z[76]*z[28];
    z[82]= - z[20]*z[81];
    z[90]= - static_cast<T>(1)- z[62];
    z[90]=z[90]*z[45];
    z[82]=z[90] + z[82] + z[6];
    z[82]=z[82]*z[45];
    z[90]=z[2] - z[4];
    z[91]=z[6]*z[90];
    z[91]= - z[42] + z[91];
    z[93]=2*z[75];
    z[94]= - z[11]*z[93];
    z[82]=z[94] + 7*z[91] + z[82];
    z[82]=z[7]*z[82];
    z[91]=z[84]*z[83];
    z[94]=z[11]*z[91];
    z[95]=z[42]*z[6];
    z[76]=z[82] + z[94] + z[76] - 12*z[95] + n<T>(13,4)*z[38];
    z[76]=z[7]*z[76];
    z[82]=16*z[4];
    z[94]= - z[82] + 5*z[2];
    z[94]=z[94]*z[61];
    z[96]=z[24]*z[6];
    z[97]=z[4] + z[24];
    z[97]=z[97]*z[96];
    z[94]=z[94] + z[97];
    z[94]=z[6]*z[94];
    z[69]= - z[37] + z[69];
    z[69]=z[5]*z[69];
    z[97]= - z[45]*z[41];
    z[69]=z[97] - z[81] + z[69];
    z[69]=z[69]*z[45];
    z[83]=z[83]*z[4];
    z[47]=z[76] - z[86] + z[69] + z[47] - 5*z[83] + z[94];
    z[47]=z[7]*z[47];
    z[69]= - z[89]*z[93];
    z[69]= - 7*z[83] + z[69];
    z[69]=z[6]*z[69];
    z[65]=z[45]*z[65];
    z[76]=npow(z[3],2);
    z[86]=z[76]*z[41];
    z[93]=z[7]*z[95];
    z[86]=4*z[86] - 7*z[93];
    z[86]=z[7]*z[86];
    z[65]=z[86] + z[69] + z[65];
    z[65]=z[7]*z[65];
    z[69]=z[64]*z[87];
    z[69]= - 14*z[83] + z[69];
    z[69]=z[5]*z[69];
    z[86]= - z[5]*z[89]*z[61];
    z[86]=z[83] + z[86];
    z[86]=z[86]*z[28];
    z[69]=z[69] + z[86];
    z[69]=z[3]*z[69];
    z[76]= - z[5]*z[76]*z[91];
    z[86]= - z[7]*z[37]*z[83];
    z[76]=z[76] + z[86];
    z[76]=z[1]*z[76];
    z[64]=z[64]*z[83];
    z[86]=z[37]*z[91];
    z[65]=2*z[76] + z[65] + z[69] + z[86] - z[64];
    z[65]=z[1]*z[65];
    z[69]=3*z[4];
    z[76]= - static_cast<T>(4)+ z[30];
    z[76]=z[2]*z[76];
    z[86]=2*z[62];
    z[93]=static_cast<T>(1)- z[86];
    z[93]=z[6]*z[93];
    z[76]=z[93] + z[69] + z[76];
    z[76]=z[5]*z[76];
    z[80]= - z[80] - z[37];
    z[76]=2*z[80] + z[76];
    z[76]=z[5]*z[76];
    z[80]=7*z[2];
    z[82]= - z[82] + z[80];
    z[93]=2*z[61];
    z[82]=z[82]*z[93];
    z[76]=z[82] + z[76];
    z[76]=z[5]*z[76];
    z[82]=static_cast<T>(1)+ z[30];
    z[82]=z[5]*z[82];
    z[82]= - z[90] + z[82];
    z[82]=z[28]*z[61]*z[82];
    z[76]=z[82] + 8*z[83] + z[76];
    z[76]=z[3]*z[76];
    z[75]= - z[90]*z[75];
    z[75]=z[91] + z[75];
    z[75]=z[75]*z[88];
    z[82]= - z[69] + z[2];
    z[82]=z[82]*z[61];
    z[53]=z[53] + z[2];
    z[53]=z[53]*z[2];
    z[83]=z[53] - z[79];
    z[83]=z[5]*z[83];
    z[82]=z[82] + z[83];
    z[82]=z[5]*z[82];
    z[83]= - z[68] - 13*z[42];
    z[83]=z[83]*z[61];
    z[82]=z[83] + z[82];
    z[82]=z[5]*z[82];
    z[64]= - z[11]*z[64];
    z[47]=z[65] + z[47] + z[64] + z[76] + z[75] + z[82];
    z[47]=z[1]*z[47];
    z[64]=3*z[12];
    z[65]= - z[24] + z[64] - n<T>(251,8)*z[4];
    z[65]=z[2]*z[65];
    z[75]=4*z[13];
    z[76]=z[75] - z[64];
    z[82]= - 4*z[6] + z[76];
    z[82]=z[6]*z[82];
    z[83]= - static_cast<T>(1)- z[86];
    z[83]=z[6]*z[83];
    z[83]=z[83] + z[89];
    z[89]=3*z[5];
    z[83]=z[83]*z[89];
    z[65]=z[83] + z[65] + z[82];
    z[65]=z[5]*z[65];
    z[82]=npow(z[12],2);
    z[83]=3*z[82];
    z[90]= - 35*z[4] + 13*z[2];
    z[90]=z[2]*z[90];
    z[90]=z[90] + z[83] - z[68];
    z[90]=z[2]*z[90];
    z[91]= - z[6]*z[83];
    z[65]=z[65] + z[90] + z[91];
    z[65]=z[5]*z[65];
    z[90]=z[56] - z[62];
    z[91]=z[90]*z[89];
    z[94]=static_cast<T>(17)- 5*z[30];
    z[94]=z[2]*z[94];
    z[66]=z[91] - z[66] - 12*z[4] + z[94];
    z[66]=z[5]*z[66];
    z[94]=static_cast<T>(9)+ 7*z[30];
    z[94]=z[94]*z[24];
    z[94]= - n<T>(425,16)*z[4] + z[94];
    z[94]=z[2]*z[94];
    z[66]=z[94] + z[66];
    z[66]=z[5]*z[66];
    z[94]=4*z[2];
    z[95]=z[4]*z[8];
    z[97]=static_cast<T>(4)+ z[95];
    z[97]=z[4]*z[97];
    z[97]=z[97] - z[94];
    z[97]=z[2]*z[97];
    z[97]=z[67] + z[97];
    z[97]=z[2]*z[97];
    z[97]= - z[92] + z[97];
    z[98]=z[25]*z[61];
    z[98]= - static_cast<T>(1)+ z[98];
    z[98]=z[2]*z[98];
    z[98]=z[4] + z[98];
    z[98]=z[5]*z[98];
    z[99]= - z[10]*z[61];
    z[99]= - z[4] + z[99];
    z[99]=z[2]*z[99];
    z[98]=z[99] + z[98];
    z[98]=z[98]*z[28];
    z[66]=z[98] + 2*z[97] + z[66];
    z[66]=z[3]*z[66];
    z[97]=static_cast<T>(3)- z[95];
    z[97]=z[4]*z[97];
    z[97]=z[97] - z[2];
    z[97]=z[97]*z[24];
    z[97]=z[83] + z[97];
    z[97]=z[2]*z[97];
    z[98]= - z[84] - z[2];
    z[96]=z[98]*z[96];
    z[96]=z[97] + z[96];
    z[96]=z[6]*z[96];
    z[87]= - z[68] - z[87];
    z[87]=z[87]*z[61];
    z[79]=z[42] - z[79];
    z[79]=z[5]*z[79];
    z[78]= - z[78] + z[79];
    z[78]=z[78]*z[89];
    z[78]=z[87] + z[78];
    z[78]=z[5]*z[78];
    z[78]=z[85] + z[78];
    z[78]=z[11]*z[78];
    z[61]=z[67]*z[61];
    z[34]=z[47] + z[34] + z[78] + z[66] + z[65] + 4*z[61] + z[96];
    z[34]=z[1]*z[34];
    z[47]=2*z[10];
    z[61]=z[47]*z[13];
    z[65]=n<T>(45,2)*z[62] - n<T>(57,4)*z[30] - z[61] + n<T>(23,2)*z[56];
    z[65]=z[5]*z[65];
    z[66]=4*z[30];
    z[78]= - n<T>(55,8) + z[66];
    z[78]=z[78]*z[80];
    z[79]=2*z[13];
    z[85]=49*z[62];
    z[87]= - static_cast<T>(299)- z[85];
    z[87]=z[6]*z[87];
    z[65]=z[65] + n<T>(1,16)*z[87] + z[78] + z[79] - n<T>(467,16)*z[4];
    z[65]=z[5]*z[65];
    z[78]=z[25]*z[2];
    z[87]=z[78] - z[31];
    z[87]= - 23*z[58] - n<T>(45,2)*z[87];
    z[87]=z[87]*z[50];
    z[96]=4*z[78];
    z[97]= - n<T>(61,4)*z[10] + z[96];
    z[97]=z[2]*z[97];
    z[87]=z[87] - n<T>(61,8)*z[62] + n<T>(21,2)*z[56] + z[97];
    z[87]=z[5]*z[87];
    z[97]=n<T>(45,4)*z[10];
    z[98]=z[82]*z[10];
    z[99]=z[12] - n<T>(1,2)*z[98];
    z[99]=z[99]*z[97];
    z[100]=z[10] - z[78];
    z[94]=z[100]*z[94];
    z[94]=z[94] + n<T>(37,16)*z[95] + static_cast<T>(4)+ z[99];
    z[94]=z[2]*z[94];
    z[99]=z[8] - z[14];
    z[60]= - z[99]*z[60];
    z[60]= - n<T>(39,4)*z[4] + z[60];
    z[60]=z[87] + n<T>(1,4)*z[60] + z[94];
    z[60]=z[3]*z[60];
    z[66]= - z[95] - z[66];
    z[66]=z[66]*z[24];
    z[87]=107*z[12] - n<T>(285,2)*z[98];
    z[94]=z[84]*z[8];
    z[100]=n<T>(65,8) - z[94];
    z[100]=z[4]*z[100];
    z[66]=z[66] + n<T>(1,4)*z[87] + z[100];
    z[66]=z[2]*z[66];
    z[87]=7*z[8];
    z[100]= - z[14] + z[87];
    z[100]=z[100]*z[54];
    z[100]= - n<T>(5,8) + z[100];
    z[100]=z[100]*z[27];
    z[60]=z[60] + z[65] + z[66] + z[68] + z[100];
    z[60]=z[3]*z[60];
    z[65]=n<T>(45,2)*z[9];
    z[66]= - z[88] + z[65] + z[76];
    z[66]=z[6]*z[66];
    z[76]= - static_cast<T>(3)- z[86];
    z[76]=z[6]*z[76];
    z[76]=z[4] + z[76];
    z[76]=z[76]*z[89];
    z[88]=z[4]*z[12];
    z[89]=z[64] - n<T>(155,8)*z[4];
    z[89]=z[2]*z[89];
    z[66]=z[76] + z[66] - 23*z[88] + z[89];
    z[66]=z[5]*z[66];
    z[76]=6*z[82];
    z[89]=z[6]*z[9];
    z[100]=n<T>(13,2)*z[89] - z[76] - n<T>(5,2)*z[27];
    z[100]=z[6]*z[100];
    z[67]=z[83] - z[67];
    z[83]= - z[64] - 11*z[4];
    z[83]=z[2]*z[83];
    z[67]=2*z[67] + z[83];
    z[67]=z[2]*z[67];
    z[83]=z[82]*z[4];
    z[66]=z[66] + z[100] + n<T>(23,4)*z[83] + z[67];
    z[66]=z[5]*z[66];
    z[67]= - z[2]*z[64];
    z[67]=z[67] + n<T>(187,8)*z[35] + z[76];
    z[67]=z[2]*z[67];
    z[76]=n<T>(21,4)*z[15];
    z[100]= - z[76] + z[24];
    z[100]=z[2]*z[100];
    z[101]=n<T>(21,8)*z[35];
    z[100]=z[101] + z[100];
    z[100]=z[6]*z[100];
    z[67]=z[67] + z[100];
    z[67]=z[6]*z[67];
    z[100]=z[27]*z[6];
    z[102]=z[92] + n<T>(13,4)*z[100];
    z[102]=z[102]*z[6];
    z[103]= - 23*z[83] + n<T>(45,2)*z[100];
    z[104]=n<T>(1,4)*z[103] - 6*z[38];
    z[104]=z[5]*z[104];
    z[104]=z[102] + z[104];
    z[104]=z[5]*z[104];
    z[93]= - z[101] - z[93];
    z[40]=z[93]*z[40];
    z[40]=z[40] + z[104];
    z[40]=z[11]*z[40];
    z[40]=z[40] + z[67] + z[66];
    z[40]=z[11]*z[40];
    z[66]=n<T>(49,16)*z[6];
    z[67]=7*z[12];
    z[93]= - z[66] + z[67] - n<T>(51,4)*z[9];
    z[93]=z[6]*z[93];
    z[90]=z[5]*z[90];
    z[75]=6*z[90] + n<T>(217,8)*z[6] + n<T>(55,4)*z[2] + z[75] - n<T>(373,8)*z[4];
    z[75]=z[5]*z[75];
    z[90]=33*z[2] - z[67] + n<T>(209,8)*z[4];
    z[90]=z[2]*z[90];
    z[75]=z[75] + z[93] + n<T>(51,4)*z[88] + z[90];
    z[75]=z[5]*z[75];
    z[53]=z[53] - 15*z[82] + z[68];
    z[53]=z[2]*z[53];
    z[53]=z[81] + z[53];
    z[68]= - static_cast<T>(2)+ z[95];
    z[68]=z[68]*z[24];
    z[90]=n<T>(113,8) + z[94];
    z[90]=z[4]*z[90];
    z[67]=z[68] + z[90] + n<T>(47,2)*z[15] - z[67];
    z[67]=z[2]*z[67];
    z[68]=z[35]*z[10];
    z[90]=z[15] - n<T>(1,2)*z[68];
    z[90]=21*z[90] - 19*z[4];
    z[93]=static_cast<T>(4)- n<T>(17,16)*z[95];
    z[93]=z[2]*z[93];
    z[90]=n<T>(1,4)*z[90] + z[93];
    z[90]=z[6]*z[90];
    z[67]=z[90] - 26*z[35] + z[67];
    z[67]=z[6]*z[67];
    z[21]=z[34] + z[21] + z[40] + z[60] + z[75] + 2*z[53] + z[67];
    z[21]=z[1]*z[21];
    z[34]=z[32] + n<T>(21,4)*z[59];
    z[34]=z[6]*z[34];
    z[34]= - z[55] + z[34] + n<T>(139,4)*z[36] - z[92];
    z[34]=z[11]*z[34];
    z[40]=19*z[35];
    z[53]=z[47]*npow(z[16],2);
    z[55]=n<T>(31,4)*z[15] - z[53];
    z[55]=z[4]*z[55];
    z[59]=n<T>(5,4)*z[6] + z[46] - n<T>(1,4)*z[2];
    z[59]=z[6]*z[59];
    z[34]=z[34] - n<T>(201,8)*z[41] + z[59] - n<T>(17,2)*z[42] - z[32] - z[40]
    + z[55];
    z[34]=z[11]*z[34];
    z[42]= - n<T>(165,4)*z[17] - 28*z[44];
    z[42]=z[9]*z[42];
    z[42]= - n<T>(11,4)*z[20] + z[42];
    z[42]=z[9]*z[42];
    z[55]= - 4*z[10] - z[23];
    z[55]=z[8]*z[55];
    z[55]= - z[25] + z[55];
    z[28]=z[55]*z[28];
    z[55]=57*z[10] + 29*z[8];
    z[28]=z[28] + n<T>(21,4)*z[31] + n<T>(3,8)*z[55] + z[42];
    z[28]=z[3]*z[28];
    z[42]=z[47] + z[8];
    z[42]=z[8]*z[42];
    z[42]=z[25] + z[42];
    z[42]=z[42]*z[45];
    z[55]=5*z[8];
    z[42]=z[42] - 7*z[10] - z[55];
    z[42]=z[8]*z[42];
    z[59]=npow(z[8],5);
    z[60]=z[59]*z[54];
    z[60]=3*z[39] + z[60];
    z[60]=z[9]*z[60];
    z[60]=z[17] + z[60];
    z[60]=z[60]*z[54];
    z[42]=z[60] - 2*z[25] + z[42];
    z[42]=z[42]*z[45];
    z[45]=3*z[20] + z[63];
    z[45]=z[9]*z[45];
    z[45]=z[8] + z[45];
    z[60]=12*z[9];
    z[45]=z[45]*z[60];
    z[63]= - z[23] - z[74];
    z[63]=z[9]*z[63];
    z[63]= - static_cast<T>(1)+ z[63];
    z[63]=z[11]*z[63]*z[51];
    z[45]=z[63] + z[77] + z[45];
    z[45]=z[11]*z[45];
    z[63]= - z[39]*z[54];
    z[63]= - 3*z[17] + z[63];
    z[63]=z[9]*z[63];
    z[63]= - z[20] + z[63];
    z[60]=z[63]*z[60];
    z[63]=6*z[10];
    z[42]=z[45] + z[42] + z[60] + z[63] + 13*z[8];
    z[42]=z[7]*z[42];
    z[45]=z[72] + n<T>(1,4)*z[35];
    z[45]=13*z[45];
    z[60]= - static_cast<T>(2)- z[33];
    z[60]=z[60]*z[32];
    z[67]=z[36]*z[11];
    z[60]= - n<T>(13,2)*z[67] + z[45] + z[60];
    z[60]=z[11]*z[60];
    z[75]= - z[87] - 6*z[52];
    z[75]=z[9]*z[75];
    z[75]= - static_cast<T>(2)+ z[75];
    z[75]=z[75]*z[51];
    z[60]=z[60] + n<T>(159,8)*z[4] + z[75];
    z[60]=z[11]*z[60];
    z[75]=n<T>(293,4)*z[20] + 54*z[18];
    z[75]=z[9]*z[75];
    z[75]=n<T>(43,4)*z[8] + z[75];
    z[75]=z[9]*z[75];
    z[87]= - n<T>(319,4) + 13*z[56];
    z[90]= - 8*z[10] - n<T>(13,2)*z[58];
    z[90]=z[6]*z[90];
    z[28]=z[42] + z[60] + z[28] + z[90] + n<T>(1,2)*z[87] + z[75];
    z[28]=z[7]*z[28];
    z[19]=n<T>(139,8)*z[20] + z[19];
    z[19]=z[19]*z[22];
    z[19]=n<T>(55,8)*z[8] + z[19];
    z[19]=z[9]*z[19];
    z[42]= - z[20] - n<T>(1,2)*z[18];
    z[60]=23*z[9];
    z[42]=z[42]*z[60];
    z[26]= - n<T>(23,2)*z[26] + z[42] + 31*z[10] + n<T>(357,16)*z[8];
    z[26]=z[3]*z[26];
    z[19]=z[26] + n<T>(45,8)*z[29] + n<T>(7,16)*z[62] + 21*z[30] - n<T>(109,4) + 
    z[19];
    z[19]=z[3]*z[19];
    z[26]= - n<T>(5,2)*z[62] - static_cast<T>(1)- n<T>(35,8)*z[56];
    z[26]=z[26]*z[57];
    z[29]=n<T>(169,8)*z[15];
    z[42]= - n<T>(271,8)*z[8] - 34*z[52];
    z[42]=z[9]*z[42];
    z[42]= - n<T>(19,4) + z[42];
    z[42]=z[9]*z[42];
    z[19]=z[28] + z[34] + z[19] + n<T>(39,8)*z[5] + z[26] - n<T>(339,8)*z[2] + 
    z[42] - z[29] - 9*z[4];
    z[19]=z[7]*z[19];
    z[26]=n<T>(45,4)*z[31] + z[47] - n<T>(23,2)*z[58];
    z[26]=z[5]*z[26];
    z[26]=z[26] - n<T>(205,16)*z[62] + n<T>(117,4)*z[30] - n<T>(71,16)*z[56] - n<T>(417,8) + z[61];
    z[26]=z[5]*z[26];
    z[28]=z[99]*z[33];
    z[34]=n<T>(1,2)*z[14];
    z[42]=z[34] - z[8];
    z[28]=n<T>(1,2)*z[28] - z[42];
    z[28]=z[28]*z[60];
    z[47]=z[98] - z[12];
    z[61]=z[47]*z[97];
    z[61]=static_cast<T>(8)+ z[61];
    z[61]=z[10]*z[61];
    z[61]= - z[96] + z[61] - n<T>(37,16)*z[8];
    z[61]=z[2]*z[61];
    z[47]=z[10]*z[47];
    z[47]= - n<T>(29,2) - 15*z[47];
    z[75]=z[5]*z[78];
    z[28]=n<T>(45,4)*z[75] + z[61] + n<T>(3,4)*z[47] + z[28];
    z[28]=z[3]*z[28];
    z[47]=4*z[4];
    z[48]=z[47]*z[48];
    z[61]=n<T>(107,16) - z[48];
    z[61]=z[4]*z[61];
    z[75]=4*z[8];
    z[78]=z[14] - z[75];
    z[78]=z[78]*z[33];
    z[87]= - 59*z[14] - 91*z[8];
    z[78]=n<T>(1,8)*z[87] + 4*z[78];
    z[78]=z[9]*z[78];
    z[78]=n<T>(95,8) + z[78];
    z[78]=z[9]*z[78];
    z[87]=n<T>(9,2)*z[12] + 13*z[98];
    z[87]=z[10]*z[87];
    z[87]=z[95] + n<T>(41,4) + 13*z[87];
    z[30]=n<T>(1,4)*z[87] - 8*z[30];
    z[30]=z[2]*z[30];
    z[87]= - n<T>(3,2)*z[12] - 217*z[98];
    z[26]=z[28] + z[26] - n<T>(57,4)*z[6] + z[30] + z[78] + n<T>(1,4)*z[87] + 
    z[61];
    z[26]=z[3]*z[26];
    z[28]=4*z[12];
    z[30]= - z[28] - z[69];
    z[30]=z[30]*z[73];
    z[61]=z[13] + z[64];
    z[61]= - z[66] + 4*z[61] - n<T>(289,8)*z[9];
    z[61]=z[6]*z[61];
    z[64]=z[91] + n<T>(575,8)*z[6] + n<T>(135,4)*z[9] + 23*z[12] - n<T>(415,8)*z[4];
    z[64]=z[5]*z[64];
    z[53]=n<T>(27,4)*z[12] + z[53];
    z[53]=z[4]*z[53];
    z[30]=z[64] + z[61] + z[30] - n<T>(169,8)*z[27] - n<T>(23,4)*z[82] + z[53];
    z[30]=z[5]*z[30];
    z[53]=n<T>(1,4)*z[82];
    z[61]= - z[12]*z[84];
    z[61]=z[53] + z[61];
    z[64]= - static_cast<T>(5)- z[86];
    z[41]=z[64]*z[41];
    z[41]=z[41] + 45*z[89] + 23*z[61] + n<T>(45,4)*z[27];
    z[41]=z[5]*z[41];
    z[61]= - z[82]*z[69];
    z[61]=z[61] + z[92];
    z[64]= - n<T>(5,4)*z[27] + z[89];
    z[64]=z[6]*z[64];
    z[41]=z[41] + 2*z[61] + n<T>(13,2)*z[64];
    z[41]=z[5]*z[41];
    z[61]=13*z[27];
    z[64]=21*z[35] - z[61];
    z[24]= - z[76] - z[24];
    z[24]=z[2]*z[24];
    z[24]=n<T>(1,4)*z[64] + z[24];
    z[24]=z[6]*z[24];
    z[64]=z[35]*z[2];
    z[24]=16*z[64] + z[24];
    z[24]=z[6]*z[24];
    z[66]=4*z[81] + n<T>(13,2)*z[100];
    z[66]=z[6]*z[66];
    z[38]=n<T>(1,2)*z[103] - 2*z[38];
    z[38]=z[5]*z[38];
    z[38]=z[66] + z[38];
    z[38]=z[5]*z[38];
    z[37]=z[64]*z[37];
    z[38]= - n<T>(21,4)*z[37] + z[38];
    z[38]=z[11]*z[38];
    z[24]=z[38] + z[24] + z[41];
    z[24]=z[11]*z[24];
    z[38]= - n<T>(85,2)*z[35] + 9*z[27];
    z[28]= - n<T>(11,8)*z[15] - z[28];
    z[28]=z[28]*z[73];
    z[41]=n<T>(17,8)*z[2];
    z[66]=z[15] - z[68];
    z[66]= - z[41] + n<T>(21,2)*z[66] - 13*z[9];
    z[57]=z[66]*z[57];
    z[28]=z[57] + n<T>(1,2)*z[38] + z[28];
    z[28]=z[6]*z[28];
    z[38]=z[40] + n<T>(35,2)*z[82];
    z[38]=z[38]*z[4];
    z[38]=z[38] - z[92];
    z[24]=z[24] + z[30] + z[28] + n<T>(85,4)*z[64] + z[38];
    z[24]=z[11]*z[24];
    z[28]=19*z[15];
    z[30]=z[28] - n<T>(89,2)*z[68];
    z[57]=n<T>(1,4)*z[6];
    z[66]=z[2]*z[8];
    z[66]=static_cast<T>(29)+ n<T>(17,4)*z[66];
    z[66]=z[66]*z[57];
    z[73]=4*z[95];
    z[76]=n<T>(251,16) + z[73];
    z[76]=z[4]*z[76];
    z[78]= - static_cast<T>(8)- n<T>(9,4)*z[95];
    z[78]=z[2]*z[78];
    z[30]=z[66] + z[78] + n<T>(3,2)*z[9] + n<T>(1,2)*z[30] + z[76];
    z[30]=z[6]*z[30];
    z[66]=6*z[13];
    z[76]=n<T>(89,4)*z[68];
    z[47]=z[47] + z[76] + n<T>(49,4)*z[12] - z[66] + n<T>(59,2)*z[15];
    z[47]=z[4]*z[47];
    z[78]= - static_cast<T>(155)- n<T>(49,4)*z[62];
    z[78]=z[78]*z[57];
    z[81]=47*z[62] + n<T>(181,4) + 21*z[56];
    z[81]=z[5]*z[81];
    z[78]=z[81] + z[78] - n<T>(359,8)*z[2] - n<T>(297,8)*z[9] - n<T>(17,4)*z[4] - 
    z[79] - n<T>(31,8)*z[12];
    z[78]=z[5]*z[78];
    z[79]=z[54]*z[14];
    z[81]=n<T>(41,4) + z[79];
    z[81]=z[81]*z[27];
    z[29]= - 10*z[2] - n<T>(1,2)*z[4] + 31*z[98] + z[29] + 30*z[12];
    z[29]=z[2]*z[29];
    z[19]=z[21] + z[19] + z[24] + z[26] + z[78] + z[30] + z[29] + z[47]
    + z[81];
    z[19]=z[1]*z[19];
    z[21]= - z[8]*z[61];
    z[21]= - n<T>(21,2)*z[68] + z[21];
    z[21]=z[21]*z[57];
    z[24]=z[15]*z[41];
    z[21]=z[21] + z[24] + n<T>(19,4)*z[35] + 8*z[27];
    z[21]=z[6]*z[21];
    z[24]=z[12]*z[14];
    z[26]=static_cast<T>(3)- z[24];
    z[26]=z[12]*z[26];
    z[26]=z[26] - z[69];
    z[29]=z[9]*z[14];
    z[30]=static_cast<T>(3)+ z[29];
    z[30]=z[30]*z[65];
    z[26]=143*z[6] + 23*z[26] + z[30];
    z[26]=z[26]*z[50];
    z[30]=n<T>(5,2)*z[82] + z[88];
    z[47]= - n<T>(87,8) + z[79];
    z[47]=z[47]*z[27];
    z[26]=z[26] - n<T>(227,8)*z[89] + 7*z[30] + z[47];
    z[26]=z[5]*z[26];
    z[30]=z[53] - z[88];
    z[30]=n<T>(45,2)*z[89] + 23*z[30] + n<T>(45,8)*z[27];
    z[30]=z[5]*z[30];
    z[30]=z[30] - n<T>(45,8)*z[100] - n<T>(47,4)*z[83] + z[92];
    z[30]=z[5]*z[30];
    z[47]=z[103]*z[50];
    z[47]=z[102] + z[47];
    z[47]=z[5]*z[47];
    z[37]= - n<T>(21,8)*z[37] + z[47];
    z[37]=z[11]*z[37];
    z[47]=n<T>(21,2)*z[35] - z[61];
    z[47]=z[6]*z[47];
    z[47]= - n<T>(59,2)*z[64] + z[47];
    z[47]=z[47]*z[57];
    z[30]=z[37] + z[47] + z[30];
    z[30]=z[11]*z[30];
    z[21]=z[30] + z[26] + z[21] - n<T>(19,4)*z[64] + z[38];
    z[21]=z[11]*z[21];
    z[26]=n<T>(131,2) - 7*z[33];
    z[26]=z[9]*z[26];
    z[26]= - z[43] - n<T>(73,4)*z[2] + n<T>(1,2)*z[26] - z[28] - z[76];
    z[26]=z[6]*z[26];
    z[28]=z[85] + n<T>(45,2)*z[29] + 19*z[56] + n<T>(227,4) - 23*z[24];
    z[28]=z[5]*z[28];
    z[30]= - static_cast<T>(29)- n<T>(57,2)*z[24];
    z[30]=z[12]*z[30];
    z[37]= - static_cast<T>(57)- n<T>(149,2)*z[29];
    z[37]=z[9]*z[37];
    z[30]= - 75*z[6] + z[37] + z[30] - 55*z[4];
    z[28]=n<T>(1,2)*z[30] + z[28];
    z[28]=z[28]*z[71];
    z[30]=93*z[82];
    z[35]=89*z[35] - z[30];
    z[35]=z[10]*z[35];
    z[35]=z[35] + 5*z[15] - 29*z[12];
    z[35]=z[4]*z[35];
    z[30]= - z[30] + z[35];
    z[35]=z[8] + z[14];
    z[35]=z[35]*z[54];
    z[37]=n<T>(21,4) - z[35];
    z[27]=z[37]*z[27];
    z[37]=z[15] + z[68];
    z[37]=z[37]*z[41];
    z[21]=z[21] + z[28] + z[26] + z[37] + n<T>(1,4)*z[30] + z[27];
    z[21]=z[11]*z[21];
    z[26]= - z[82]*z[34];
    z[26]=z[26] + z[98];
    z[26]=z[10]*z[26];
    z[26]= - n<T>(91,2) + 45*z[26];
    z[26]=z[10]*z[26];
    z[26]= - 115*z[8] + n<T>(45,2)*z[14] + z[26];
    z[27]=z[99]*z[52];
    z[28]=z[8]*z[42];
    z[27]=z[28] - n<T>(1,4)*z[27];
    z[27]=z[27]*z[60];
    z[28]=z[2]*z[82]*npow(z[10],4);
    z[26]= - n<T>(45,8)*z[28] + n<T>(1,4)*z[26] + z[27];
    z[26]=z[3]*z[26];
    z[27]=static_cast<T>(147)- n<T>(149,2)*z[24];
    z[27]=z[12]*z[27];
    z[27]=z[27] + 101*z[98];
    z[27]=z[10]*z[27];
    z[28]=static_cast<T>(41)- 227*z[24];
    z[27]=n<T>(1,2)*z[28] + z[27];
    z[23]= - z[14] + z[23];
    z[23]=z[23]*z[74];
    z[28]=n<T>(59,8)*z[14] + 12*z[8];
    z[28]=z[8]*z[28];
    z[23]=z[28] + z[23];
    z[23]=z[9]*z[23];
    z[28]=n<T>(47,2)*z[14] + 21*z[8];
    z[23]=n<T>(1,2)*z[28] + z[23];
    z[23]=z[9]*z[23];
    z[28]=37*z[12] - 53*z[98];
    z[28]=z[10]*z[28];
    z[28]=static_cast<T>(21)+ n<T>(1,2)*z[28];
    z[28]=z[10]*z[28];
    z[28]=n<T>(1,4)*z[28] + z[75];
    z[28]=z[2]*z[28];
    z[30]=z[14] - z[10];
    z[30]=z[5]*z[30];
    z[23]=z[26] + 2*z[30] + z[28] + z[23] + n<T>(1,4)*z[27] + z[73];
    z[23]=z[3]*z[23];
    z[26]= - static_cast<T>(1)- z[33];
    z[26]=z[26]*z[32];
    z[26]= - n<T>(13,4)*z[67] + z[45] + z[26];
    z[26]=z[11]*z[26];
    z[27]= - z[69] - 3*z[15] + z[68];
    z[28]= - z[55] - z[52];
    z[28]=z[9]*z[28];
    z[28]= - static_cast<T>(4)+ z[28];
    z[28]=z[28]*z[54];
    z[26]=z[26] + n<T>(13,4)*z[27] + z[28];
    z[26]=z[11]*z[26];
    z[27]=z[10]*z[15];
    z[28]= - z[56] - n<T>(59,4) - 13*z[27];
    z[30]=6*z[18];
    z[34]=n<T>(119,8)*z[20] + z[30];
    z[34]=z[9]*z[34];
    z[34]=8*z[8] + z[34];
    z[34]=z[34]*z[22];
    z[26]=z[26] + n<T>(1,2)*z[28] + z[34];
    z[26]=z[11]*z[26];
    z[28]= - 2*z[17] - z[44];
    z[28]=z[9]*z[28];
    z[28]= - z[20] + z[28];
    z[28]=z[28]*z[46];
    z[18]=2*z[20] + z[18];
    z[18]=z[9]*z[18];
    z[18]=z[8] + z[18];
    z[18]=z[18]*z[51];
    z[34]= - 2*z[8] - z[52];
    z[34]=z[9]*z[34];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[11]*z[9]*z[34];
    z[18]=z[18] + z[34];
    z[18]=z[11]*z[18];
    z[18]=z[28] + z[18];
    z[18]=z[11]*z[18];
    z[28]=z[59]*z[9];
    z[34]=2*z[39] + z[28];
    z[34]=z[9]*z[34];
    z[34]=z[17] + z[34];
    z[34]=z[34]*z[51];
    z[37]= - z[9]*npow(z[8],6);
    z[37]= - 2*z[59] + z[37];
    z[37]=z[9]*z[37];
    z[37]= - z[39] + z[37];
    z[37]=z[3]*z[9]*z[37];
    z[18]=z[18] + z[34] + z[37];
    z[18]=z[7]*z[18];
    z[28]=n<T>(149,8)*z[39] + 8*z[28];
    z[28]=z[9]*z[28];
    z[28]=8*z[17] + z[28];
    z[28]=z[9]*z[28];
    z[28]=z[49] + z[28];
    z[28]=z[3]*z[28];
    z[34]= - n<T>(205,4)*z[17] - 22*z[44];
    z[34]=z[9]*z[34];
    z[34]= - 24*z[20] + z[34];
    z[34]=z[9]*z[34];
    z[18]=2*z[18] + z[26] + z[28] + z[34] - n<T>(13,4)*z[58] - z[63] + n<T>(59,8)
   *z[8];
    z[18]=z[7]*z[18];
    z[26]= - static_cast<T>(4)- z[33];
    z[26]=z[26]*z[32];
    z[28]=n<T>(63,4)*z[36] - z[92];
    z[28]=z[11]*z[28];
    z[26]=z[28] + z[26] - z[40] - 8*z[72];
    z[26]=z[11]*z[26];
    z[28]=125*z[15] - 17*z[68];
    z[28]=n<T>(1,2)*z[28] + z[69];
    z[32]= - n<T>(17,8)*z[8] - z[74];
    z[32]=z[9]*z[32];
    z[32]= - n<T>(215,8) + z[32];
    z[32]=z[9]*z[32];
    z[26]=z[26] - 8*z[6] + n<T>(1,4)*z[28] + z[32];
    z[26]=z[11]*z[26];
    z[28]=z[17] + n<T>(1,4)*z[44];
    z[28]=z[9]*z[28];
    z[32]=z[10] + n<T>(3,2)*z[8];
    z[32]=z[8]*z[32];
    z[25]=z[28] + n<T>(1,4)*z[25] + z[32];
    z[25]=z[3]*z[25];
    z[25]=z[25] - z[10];
    z[17]= - n<T>(259,8)*z[17] - 12*z[44];
    z[17]=z[9]*z[17];
    z[17]= - n<T>(85,2)*z[20] + z[17];
    z[17]=z[9]*z[17];
    z[17]=z[17] - n<T>(469,8)*z[8] + 23*z[25];
    z[17]=z[3]*z[17];
    z[25]= - static_cast<T>(75)+ 17*z[27];
    z[25]=n<T>(1,4)*z[25] + z[77];
    z[20]=n<T>(49,4)*z[20] + z[30];
    z[20]=z[20]*z[22];
    z[20]=n<T>(371,8)*z[8] + z[20];
    z[20]=z[9]*z[20];
    z[17]=z[18] + z[26] + z[17] + n<T>(1,2)*z[25] + z[20];
    z[17]=z[7]*z[17];
    z[18]=n<T>(45,8)*z[31] - n<T>(23,4)*z[58] + 2*z[14] - n<T>(1,8)*z[10];
    z[18]=z[5]*z[18];
    z[20]= - static_cast<T>(2)+ n<T>(17,4)*z[24];
    z[18]=z[18] - 10*z[62] - 23*z[29] + 5*z[20] + n<T>(33,2)*z[56];
    z[18]=z[5]*z[18];
    z[20]=z[8]*z[80];
    z[20]=z[20] - z[48] + n<T>(157,8) + 7*z[27];
    z[20]=z[6]*z[20];
    z[22]= - 7*z[15] - 13*z[12];
    z[22]=z[10]*z[22];
    z[22]= - z[70] - static_cast<T>(15)+ z[22];
    z[22]=z[4]*z[22];
    z[25]= - z[35] - n<T>(3,8);
    z[25]=z[8]*z[25];
    z[25]=13*z[14] + z[25];
    z[25]=z[9]*z[25];
    z[25]= - n<T>(21,8) + z[25];
    z[25]=z[9]*z[25];
    z[24]=static_cast<T>(19)+ n<T>(103,8)*z[24];
    z[24]=z[12]*z[24];
    z[26]= - n<T>(17,8)*z[15] - z[98];
    z[26]=z[10]*z[26];
    z[26]= - n<T>(57,4) + z[26];
    z[26]=z[2]*z[26];
    z[17]=z[19] + z[17] + z[21] + z[23] + z[18] + z[20] + z[26] + z[25]
    + z[22] + 29*z[98] + z[66] + z[24];
    z[17]=z[1]*z[17];
    z[18]=z[11]*z[5];
    z[19]= - z[7]*z[8];
    z[18]=z[19] - static_cast<T>(1)+ z[18];

    r += z[17] + 2*z[18];
 
    return r;
}

template double qqb_2lNLC_r750(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r750(const std::array<dd_real,31>&);
#endif
