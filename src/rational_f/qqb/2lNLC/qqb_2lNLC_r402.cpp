#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r402(const std::array<T,31>& k) {
  T z[27];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[14];
    z[7]=k[12];
    z[8]=k[13];
    z[9]=k[4];
    z[10]=k[15];
    z[11]=k[5];
    z[12]=k[9];
    z[13]=z[8]*z[12];
    z[14]=n<T>(1,2)*z[13];
    z[15]=z[7]*z[12];
    z[15]=z[14] - z[15];
    z[16]=n<T>(1,2)*z[10];
    z[17]=z[5]*z[11];
    z[18]= - z[10]*z[17];
    z[18]= - z[5] + z[18];
    z[18]=z[18]*z[16];
    z[19]=z[3]*z[5];
    z[18]=z[18] - n<T>(3,2)*z[19] - z[15];
    z[18]=z[10]*z[18];
    z[20]=2*z[19];
    z[21]=z[7]*z[8];
    z[22]=z[3] - z[5];
    z[23]=z[7] - n<T>(1,2)*z[8] + n<T>(3,2)*z[22];
    z[23]=z[10]*z[23];
    z[21]=z[23] + z[21] - z[20];
    z[21]=z[4]*z[21];
    z[23]=npow(z[7],2);
    z[24]=z[13] - z[23];
    z[24]=z[7]*z[24];
    z[25]= - z[5]*npow(z[3],2);
    z[24]=z[24] + z[25];
    z[18]=z[21] + n<T>(1,2)*z[24] + z[18];
    z[18]=z[6]*z[18];
    z[21]=z[3]*z[9];
    z[24]=z[10]*z[21];
    z[24]= - z[3] + z[24];
    z[24]=z[24]*z[16];
    z[15]=z[24] + z[20] + z[15];
    z[15]=z[10]*z[15];
    z[20]=n<T>(1,2)*z[3];
    z[24]=npow(z[4],2);
    z[25]= - z[24]*z[20];
    z[26]=z[13]*z[7];
    z[15]=z[25] - z[26] + z[15];
    z[15]=z[4]*z[15];
    z[25]= - z[8] - z[7];
    z[23]=z[25]*z[23];
    z[25]=z[4]*z[22];
    z[25]=z[19] + z[25];
    z[25]=z[4]*z[25];
    z[23]=z[23] + z[25];
    z[23]=z[4]*z[23];
    z[25]=z[8]*npow(z[7],3);
    z[23]=z[25] + z[23];
    z[23]=z[6]*z[23];
    z[24]=z[24]*z[19];
    z[24]=z[24] - z[25];
    z[25]= - z[1]*z[6];
    z[25]=z[25] + 1;
    z[24]=z[25]*z[24]*z[4];
    z[23]=z[23] + z[24];
    z[23]=z[1]*z[23];
    z[20]=npow(z[10],3)*z[20];
    z[15]=n<T>(1,2)*z[23] + z[18] + z[20] + z[15];
    z[18]=npow(z[2],2);
    z[15]=z[1]*z[18]*z[15];
    z[20]= - z[12] + z[8];
    z[20]=z[7]*z[20];
    z[20]=z[20] - z[19];
    z[23]= - z[5]*npow(z[11],2);
    z[23]=z[11] + z[23];
    z[23]=z[23]*z[16];
    z[23]=z[23] + static_cast<T>(1)- n<T>(3,2)*z[17];
    z[23]=z[10]*z[23];
    z[23]=z[23] - 2*z[5] + n<T>(1,2)*z[12];
    z[23]=z[10]*z[23];
    z[24]=n<T>(1,2)*z[4];
    z[25]=3*z[10] + z[8] + z[22];
    z[25]=z[25]*z[24];
    z[20]=z[25] + n<T>(1,2)*z[20] + z[23];
    z[20]=z[6]*z[20];
    z[21]=z[21] + 1;
    z[17]=z[17] - z[21];
    z[16]=z[17]*z[16];
    z[16]=z[5] + z[16];
    z[16]=z[10]*z[16];
    z[16]=n<T>(1,2)*z[19] + z[16];
    z[16]=z[10]*z[16];
    z[17]= - z[3]*npow(z[9],2);
    z[17]=z[9] + z[17];
    z[17]=z[10]*z[17];
    z[17]=z[17] - z[21];
    z[17]=z[10]*z[17];
    z[17]=z[17] - z[12] - z[22];
    z[17]=z[10]*z[17];
    z[13]= - z[13] + z[17];
    z[13]=z[13]*z[24];
    z[14]= - z[7]*z[14];
    z[13]=z[20] + z[13] + z[14] + z[16];
    z[13]=z[13]*z[18];
    z[13]=z[13] + z[15];

    r += z[13]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r402(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r402(const std::array<dd_real,31>&);
#endif
