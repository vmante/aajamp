#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r593(const std::array<T,31>& k) {
  T z[82];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[14];
    z[7]=k[12];
    z[8]=k[13];
    z[9]=k[4];
    z[10]=k[15];
    z[11]=k[5];
    z[12]=k[9];
    z[13]=k[3];
    z[14]=k[8];
    z[15]=3*z[8];
    z[16]=6*z[12];
    z[17]=3*z[4];
    z[18]=z[15] + z[16] + n<T>(5,2)*z[14] + z[17];
    z[18]=z[8]*z[18];
    z[19]=n<T>(3,2)*z[5];
    z[20]=z[14]*z[11];
    z[21]=z[20] + 2;
    z[22]=z[14]*z[21];
    z[23]=z[8]*z[11];
    z[24]= - static_cast<T>(1)+ z[23];
    z[24]=z[8]*z[24];
    z[22]=z[24] + z[19] + z[22] - z[4];
    z[22]=z[7]*z[22];
    z[24]=5*z[14];
    z[25]= - 9*z[12] + z[5] + z[24] - z[17];
    z[26]=3*z[10];
    z[25]=n<T>(1,2)*z[25] - z[26];
    z[25]=z[10]*z[25];
    z[27]=npow(z[4],2);
    z[28]=n<T>(1,2)*z[27];
    z[29]=npow(z[12],2);
    z[30]=npow(z[5],2);
    z[31]=npow(z[14],2);
    z[18]=z[22] + z[25] + z[18] - n<T>(3,2)*z[29] + 3*z[30] + 13*z[31] - 
    z[28];
    z[18]=z[7]*z[18];
    z[22]=z[12]*z[11];
    z[25]=3*z[22];
    z[32]=z[25] + 4;
    z[33]=4*z[12];
    z[32]=z[32]*z[33];
    z[34]=npow(z[11],2);
    z[35]=z[34]*z[12];
    z[36]=z[35] + z[11];
    z[37]=z[36]*z[12];
    z[38]=z[4]*z[9];
    z[39]=6*z[37] + static_cast<T>(1)+ 4*z[38];
    z[40]=2*z[10];
    z[39]=z[39]*z[40];
    z[41]=17*z[4];
    z[42]=n<T>(3,2)*z[6];
    z[39]=z[39] + 2*z[8] + z[32] + z[41] - z[42];
    z[39]=z[10]*z[39];
    z[43]=n<T>(1,2)*z[6];
    z[44]=z[43] + z[17];
    z[45]= - z[24] - z[44];
    z[45]=z[6]*z[45];
    z[46]=5*z[6];
    z[47]=7*z[4];
    z[48]= - z[47] + z[46];
    z[49]=n<T>(1,2)*z[5];
    z[48]=z[48]*z[49];
    z[50]= - z[42] + z[33];
    z[50]=z[12]*z[50];
    z[51]=n<T>(1,2)*z[4];
    z[52]=z[51] - z[14];
    z[53]=n<T>(7,2)*z[5];
    z[54]= - z[53] + z[52];
    z[15]=z[54]*z[15];
    z[15]=z[39] + z[15] + z[50] + z[45] + z[48];
    z[15]=z[10]*z[15];
    z[39]=z[6]*z[11];
    z[45]=z[39] - 2;
    z[48]=z[4]*z[13];
    z[50]= - z[48] + z[45];
    z[50]=z[5]*z[50];
    z[54]=4*z[4];
    z[50]=z[50] - z[54] + z[43];
    z[50]=z[5]*z[50];
    z[55]=npow(z[9],2);
    z[56]=z[55]*z[4];
    z[57]= - z[9] - z[56];
    z[57]=z[10]*z[57];
    z[57]=6*z[57] + n<T>(3,2)*z[23] - n<T>(11,2) - 10*z[38];
    z[57]=z[10]*z[57];
    z[57]=z[57] + z[6] - n<T>(11,2)*z[8];
    z[57]=z[10]*z[57];
    z[58]=z[8]*z[5];
    z[59]=z[5]*z[11];
    z[60]=n<T>(9,2) - z[59];
    z[60]=z[60]*z[58];
    z[61]=z[31]*z[11];
    z[62]=z[5]*z[13];
    z[62]=z[62] - 2;
    z[63]=z[5]*z[62];
    z[63]=z[10] + n<T>(3,2)*z[8] + z[61] + z[63];
    z[63]=z[7]*z[63];
    z[64]=z[6]*z[52];
    z[65]= - z[3]*z[4];
    z[50]=z[65] + z[63] + z[57] + z[60] + z[64] + z[50];
    z[50]=z[3]*z[50];
    z[57]=n<T>(1,2)*z[12];
    z[60]=z[24] + z[12];
    z[60]=z[60]*z[57];
    z[63]=z[12] - z[5];
    z[64]=z[8]*z[63];
    z[60]=z[64] - 2*z[30] + z[60];
    z[60]=z[8]*z[60];
    z[64]=z[6]*z[4];
    z[65]=z[27] + 9*z[64];
    z[66]=z[5]*z[6];
    z[65]=n<T>(1,2)*z[65] - z[66];
    z[65]=z[5]*z[65];
    z[67]= - z[24] - z[6];
    z[67]=z[67]*z[57];
    z[68]=12*z[31];
    z[67]=z[68] + z[67];
    z[67]=z[12]*z[67];
    z[69]=z[4]*npow(z[6],2);
    z[15]=z[50] + z[18] + z[15] + z[60] + z[67] - z[69] + z[65];
    z[15]=z[3]*z[15];
    z[18]=n<T>(3,2)*z[4];
    z[50]= - z[14] + z[18];
    z[50]=3*z[50] + z[43];
    z[50]=z[6]*z[50];
    z[60]=2*z[5];
    z[65]=z[17] - z[60];
    z[65]=z[8]*z[65];
    z[67]=z[4]*z[14];
    z[70]=5*z[67];
    z[71]= - n<T>(21,2)*z[6] - z[5];
    z[71]=z[5]*z[71];
    z[72]= - z[51] - 10*z[6];
    z[72]=z[12]*z[72];
    z[50]=z[65] + z[72] + z[71] + z[70] + z[50];
    z[50]=z[8]*z[50];
    z[65]=3*z[6];
    z[71]=z[65] + z[5];
    z[72]= - z[24] + z[4] + z[71];
    z[73]=3*z[12];
    z[74]=z[36]*z[73];
    z[74]=static_cast<T>(1)+ z[74];
    z[74]=z[74]*z[33];
    z[72]=n<T>(1,2)*z[72] + z[74];
    z[72]=z[10]*z[72];
    z[44]=z[6]*z[44];
    z[32]=z[42] + z[32];
    z[32]=z[12]*z[32];
    z[42]=z[5]*z[71];
    z[32]=z[72] + z[32] + z[42] + z[68] + z[44];
    z[32]=z[10]*z[32];
    z[42]=z[34]*z[6];
    z[44]= - z[11] + 2*z[42];
    z[44]=z[44]*z[73];
    z[68]=z[13]*z[19];
    z[44]=z[44] + z[68] - static_cast<T>(3)+ 2*z[39];
    z[44]=z[12]*z[44];
    z[44]=z[44] - 4*z[6] - n<T>(11,2)*z[5];
    z[44]=z[12]*z[44];
    z[68]=z[5]*z[9];
    z[68]=z[68] + z[38];
    z[71]=z[45] + z[68];
    z[71]=z[8]*z[71];
    z[51]= - z[51] + z[6];
    z[51]=z[71] + 3*z[51] + z[53];
    z[51]=z[8]*z[51];
    z[71]=z[67] - z[31];
    z[72]=n<T>(1,2)*z[64];
    z[74]=z[7]*z[6];
    z[44]=z[74] + z[51] + z[44] + z[72] + z[71];
    z[44]=z[7]*z[44];
    z[51]=9*z[6];
    z[74]=z[39]*z[12];
    z[75]=8*z[74] - z[51] + z[53];
    z[75]=z[12]*z[75];
    z[75]=12*z[66] + z[75];
    z[75]=z[12]*z[75];
    z[27]=z[27]*z[6];
    z[32]=z[44] + z[32] + z[50] + z[27] + z[75];
    z[32]=z[7]*z[32];
    z[44]=z[5]*z[14];
    z[21]= - z[21]*z[44];
    z[50]=z[6]*z[14];
    z[21]=z[21] + 11*z[31] - n<T>(21,2)*z[50];
    z[21]=z[5]*z[21];
    z[75]=2*z[12];
    z[76]= - z[48]*z[75];
    z[76]= - z[4] + z[76];
    z[76]=z[12]*z[76];
    z[76]=7*z[76] + 2*z[67] - n<T>(17,2)*z[50];
    z[76]=z[12]*z[76];
    z[77]=z[59]*z[31];
    z[78]=2*z[4];
    z[79]= - z[12]*z[48];
    z[79]=z[78] + z[79];
    z[79]=z[12]*z[79];
    z[80]= - z[14] + z[4];
    z[80]=z[6]*z[80];
    z[79]=z[79] + z[80] - z[77];
    z[79]=z[8]*z[79];
    z[21]=z[79] + z[21] + z[76];
    z[21]=z[8]*z[21];
    z[25]=static_cast<T>(1)- z[25];
    z[76]=8*z[12];
    z[25]=z[76]*z[25];
    z[76]=z[39]*z[5];
    z[25]=z[25] - z[51] - z[76];
    z[25]=z[5]*z[25];
    z[51]=z[50] - z[67];
    z[79]=12*z[51];
    z[29]=3*z[29];
    z[80]= - z[34]*z[29];
    z[80]= - static_cast<T>(1)+ z[80];
    z[80]=z[80]*z[33];
    z[81]=z[24] + 7*z[6];
    z[80]=n<T>(1,2)*z[81] + z[80];
    z[80]=z[8]*z[80];
    z[35]=z[35]*z[5];
    z[81]=z[59] - z[35];
    z[16]=z[81]*z[16];
    z[16]= - 7*z[76] + z[16];
    z[16]=z[16]*z[40];
    z[16]=z[16] + z[80] - z[79] + z[25];
    z[16]=z[10]*z[16];
    z[25]=4*z[31];
    z[53]= - z[14]*z[53];
    z[53]=z[53] + z[72] - z[25] + z[67];
    z[67]= - static_cast<T>(1)- 9*z[22];
    z[33]=z[67]*z[33];
    z[17]= - z[17] + z[33];
    z[17]=z[12]*z[17];
    z[17]=3*z[53] + z[17];
    z[17]=z[8]*z[17];
    z[33]= - z[52]*z[65];
    z[33]= - z[66] - z[70] + z[33];
    z[33]=z[5]*z[33];
    z[52]=z[4] + z[6];
    z[52]=z[52]*z[19];
    z[53]=z[12]*z[5];
    z[52]=z[52] - 16*z[53];
    z[52]=z[12]*z[52];
    z[16]=z[16] + z[17] + z[33] + z[52];
    z[16]=z[10]*z[16];
    z[17]=z[4] + z[43];
    z[17]=z[6]*z[17];
    z[17]=z[17] + z[66];
    z[17]=z[5]*z[17];
    z[33]=z[6] - z[4];
    z[43]= - z[5]*z[33];
    z[43]=z[64] + z[43];
    z[43]=z[3]*z[43];
    z[52]=z[58]*z[7];
    z[53]=static_cast<T>(1)+ z[38];
    z[53]=z[53]*npow(z[10],3);
    z[17]=z[43] - z[52] + 3*z[53] - z[69] + z[17];
    z[17]=z[3]*z[17];
    z[43]=z[30]*z[64];
    z[53]= - z[30] - z[58];
    z[53]=z[8]*z[53];
    z[52]=z[53] - z[52];
    z[52]=z[7]*z[52];
    z[17]=z[17] + z[43] + z[52];
    z[17]=z[3]*z[17];
    z[43]=z[8]*z[6];
    z[28]=z[43] + z[28] + z[64];
    z[28]=z[8]*z[28];
    z[43]=z[6] + z[74];
    z[29]=z[43]*z[29];
    z[43]= - z[8]*z[33];
    z[43]=z[64] + z[43];
    z[43]=z[7]*z[43];
    z[27]=z[43] + z[28] + z[27] + z[29];
    z[27]=z[7]*z[27];
    z[28]=npow(z[8],2)*z[64];
    z[27]=z[28] + z[27];
    z[27]=z[7]*z[27];
    z[28]= - z[8]*npow(z[7],3);
    z[29]= - z[5]*npow(z[3],3);
    z[28]=z[28] + z[29];
    z[28]=z[1]*z[64]*z[28];
    z[17]=z[28] + z[27] + z[17];
    z[17]=z[1]*z[17];
    z[24]=z[24] - z[47];
    z[24]=z[24]*z[49];
    z[27]=z[12]*z[41];
    z[24]=z[27] - z[79] + z[24];
    z[24]=z[12]*z[24];
    z[25]= - z[25] + n<T>(7,2)*z[50];
    z[25]=z[5]*z[25];
    z[24]=3*z[25] + z[24];
    z[24]=z[12]*z[24];
    z[25]=z[64] + z[71];
    z[25]=z[25]*z[30];
    z[15]=z[17] + z[15] + z[32] + z[16] + z[21] + z[25] + z[24];
    z[16]=npow(z[2],2);
    z[15]=z[1]*z[16]*z[15];
    z[17]=z[4]*npow(z[9],3);
    z[17]=z[55] + z[17];
    z[17]=z[17]*z[26];
    z[17]=z[17] + 7*z[9] + 10*z[56];
    z[17]=z[10]*z[17];
    z[17]=z[17] + static_cast<T>(5)+ 12*z[38];
    z[17]=z[10]*z[17];
    z[21]=z[34]*z[14];
    z[24]= - z[21] + 3*z[11];
    z[24]=z[24]*z[14];
    z[25]= - z[24] - z[62];
    z[25]=z[7]*z[25];
    z[20]=z[20] - 1;
    z[20]=z[20]*z[14];
    z[26]=z[5]*z[48];
    z[17]=z[25] + z[17] + z[26] + z[20] + z[54];
    z[17]=z[3]*z[17];
    z[25]=2*z[14];
    z[26]=n<T>(5,2) - z[59];
    z[26]=z[5]*z[26];
    z[26]=z[57] + z[26] + z[25] - z[18];
    z[26]=z[8]*z[26];
    z[27]= - 4*z[56] - z[9] - 6*z[36];
    z[27]=z[27]*z[40];
    z[22]=12*z[22];
    z[27]=z[27] - z[22] - static_cast<T>(8)- 15*z[38];
    z[27]=z[10]*z[27];
    z[28]=12*z[12];
    z[29]=13*z[14];
    z[30]= - z[29] - 29*z[4];
    z[27]=z[27] + z[8] - z[28] + 6*z[5] + n<T>(1,2)*z[30] + z[46];
    z[27]=z[10]*z[27];
    z[21]=z[21] + z[11];
    z[30]=z[14]*z[21];
    z[23]= - z[23] - static_cast<T>(1)+ z[30];
    z[23]=z[7]*z[23];
    z[30]=z[63] - z[8];
    z[30]=z[4] + 5*z[30];
    z[23]=z[23] + n<T>(1,2)*z[30] + z[40];
    z[23]=z[7]*z[23];
    z[18]=z[25] + z[18];
    z[18]=z[18]*z[6];
    z[25]=z[5]*z[45];
    z[25]=z[25] + z[4] - z[65];
    z[25]=z[5]*z[25];
    z[30]=3*z[14];
    z[32]= - z[30] + z[6];
    z[32]=z[12]*z[32];
    z[17]=z[17] + z[23] + z[27] + z[26] + n<T>(3,2)*z[32] + z[25] + 2*z[31]
    + z[18];
    z[17]=z[3]*z[17];
    z[23]=z[8]*z[68];
    z[19]=z[23] - z[57] + z[19] - z[6] - n<T>(13,2)*z[14] + z[78];
    z[19]=z[8]*z[19];
    z[23]= - z[13] - z[11];
    z[23]=z[11]*z[23];
    z[25]=z[6]*npow(z[11],3);
    z[23]=z[23] + z[25];
    z[23]=z[23]*z[73];
    z[25]=z[42] - z[11];
    z[23]=z[23] - z[25];
    z[23]=z[12]*z[23];
    z[23]=z[39] + z[23];
    z[23]=z[12]*z[23];
    z[26]= - z[8]*z[45];
    z[23]=z[26] + z[23] - z[61] + z[6];
    z[23]=z[7]*z[23];
    z[26]=4*z[42] - 3*z[13] - 10*z[11];
    z[26]=z[26]*z[75];
    z[26]=z[26] - static_cast<T>(7)+ z[39];
    z[26]=z[12]*z[26];
    z[27]=11*z[4];
    z[32]= - z[27] + 15*z[6];
    z[26]=z[26] + n<T>(1,2)*z[32] + z[5];
    z[26]=z[12]*z[26];
    z[32]=z[37] + 1;
    z[28]=z[32]*z[28];
    z[30]= - z[30] - z[4];
    z[30]=z[5] + 3*z[30] + z[6];
    z[30]=n<T>(1,2)*z[30] - z[28];
    z[30]=z[10]*z[30];
    z[18]=z[23] + z[30] + z[19] + z[26] - n<T>(1,2)*z[66] - 2*z[71] + z[18];
    z[18]=z[7]*z[18];
    z[19]=z[11] - 7*z[42];
    z[19]=z[19]*z[60];
    z[23]= - static_cast<T>(1)+ z[38];
    z[19]=12*z[35] + z[19] + 5*z[23] + 17*z[39];
    z[19]=z[10]*z[19];
    z[23]= - static_cast<T>(1)- 8*z[39];
    z[25]= - z[5]*z[25];
    z[22]=z[22] + 2*z[23] + z[25];
    z[22]=z[5]*z[22];
    z[19]=z[19] + 6*z[4] + 11*z[6] + z[22];
    z[19]=z[10]*z[19];
    z[22]=8*z[51];
    z[23]= - 31*z[6] + z[29] + z[4];
    z[23]=n<T>(1,2)*z[23] + z[76];
    z[23]=z[5]*z[23];
    z[25]=n<T>(1,2)*z[33] + 4*z[5];
    z[25]=z[25]*z[73];
    z[26]=z[28] - n<T>(7,2)*z[6] + n<T>(9,2)*z[14] + z[4];
    z[26]=z[8]*z[26];
    z[19]=z[19] + z[26] + z[25] + z[22] + z[23];
    z[19]=z[10]*z[19];
    z[23]=npow(z[13],2);
    z[25]=z[4]*z[23];
    z[25]= - z[13] + z[25];
    z[25]=z[12]*z[25];
    z[25]=z[25] + static_cast<T>(1)- z[48];
    z[25]=z[12]*z[25];
    z[24]= - static_cast<T>(2)+ z[24];
    z[24]=z[5]*z[24];
    z[20]=z[25] + z[24] + z[20] + z[4];
    z[20]=z[8]*z[20];
    z[24]= - z[31] - z[50];
    z[21]= - z[21]*z[44];
    z[21]=4*z[14] + z[21];
    z[21]=z[5]*z[21];
    z[23]=z[23]*z[47];
    z[23]=z[23] - 19*z[13] + 18*z[11];
    z[23]=z[12]*z[23];
    z[23]=z[23] - static_cast<T>(9)+ 8*z[48];
    z[23]=z[23]*z[75];
    z[25]=31*z[4] - z[6];
    z[23]=n<T>(1,2)*z[25] + z[23];
    z[23]=z[12]*z[23];
    z[20]=z[20] + z[23] + 2*z[24] + z[21];
    z[20]=z[8]*z[20];
    z[21]= - z[31] - z[51];
    z[21]=2*z[21] - z[77];
    z[21]=z[5]*z[21];
    z[23]=5*z[39] + static_cast<T>(7)- 17*z[48];
    z[23]=z[12]*z[23];
    z[23]=z[23] - z[27] - z[6];
    z[23]=z[12]*z[23];
    z[24]=9*z[14] + z[47];
    z[24]=n<T>(1,2)*z[24] - z[6];
    z[24]=z[5]*z[24];
    z[22]=z[23] + z[22] + z[24];
    z[22]=z[12]*z[22];
    z[17]=z[17] + z[18] + z[19] + z[20] + z[21] + z[22];
    z[16]=z[17]*z[16];
    z[15]=z[16] + z[15];

    r += z[15]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r593(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r593(const std::array<dd_real,31>&);
#endif
