#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r597(const std::array<T,31>& k) {
  T z[43];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[14];
    z[8]=k[11];
    z[9]=k[13];
    z[10]=k[12];
    z[11]=k[5];
    z[12]=k[8];
    z[13]=k[9];
    z[14]=z[5]*npow(z[2],3);
    z[15]=npow(z[2],2);
    z[16]=13*z[15] + z[14];
    z[16]=z[5]*z[16];
    z[14]= - z[15] - z[14];
    z[14]=z[4]*z[14];
    z[17]=10*z[2];
    z[14]=z[14] + z[17] + z[16];
    z[14]=z[4]*z[14];
    z[15]=z[15]*z[5];
    z[16]= - z[17] - z[15];
    z[16]=z[5]*z[16];
    z[17]=z[7]*z[11];
    z[18]=z[17] - 1;
    z[19]=z[7]*npow(z[11],2);
    z[19]=z[19] - z[11];
    z[19]= - z[2] + 9*z[19];
    z[19]=z[8]*z[19];
    z[14]=z[14] + z[16] - 10*z[18] + z[19];
    z[14]=z[6]*z[14];
    z[16]=13*z[2];
    z[19]=z[16] + 2*z[15];
    z[19]=z[5]*z[19];
    z[20]= - z[4]*z[15];
    z[19]=z[20] + static_cast<T>(8)+ z[19];
    z[19]=z[4]*z[19];
    z[20]=9*z[17];
    z[21]= - static_cast<T>(8)+ z[20];
    z[21]=z[8]*z[21];
    z[22]=z[5]*z[2];
    z[23]=z[22] + 1;
    z[24]=z[5]*z[23];
    z[14]=z[14] + z[19] - 2*z[24] - 8*z[7] + z[21];
    z[14]=z[6]*z[14];
    z[19]=n<T>(3,2)*z[12];
    z[21]=2*z[7];
    z[24]= - z[19] + z[21];
    z[24]=z[8]*z[24];
    z[25]=n<T>(1,2)*z[9];
    z[26]=z[12] + z[13];
    z[27]= - 5*z[8] - z[26];
    z[27]=z[27]*z[25];
    z[28]=n<T>(1,2)*z[10];
    z[29]=5*z[7] + z[26];
    z[29]=z[29]*z[28];
    z[30]=n<T>(3,2)*z[10];
    z[31]=3*z[13];
    z[32]= - 13*z[8] + z[31] - z[7];
    z[32]= - z[5] + z[30] + n<T>(1,2)*z[32] + z[9];
    z[32]=z[5]*z[32];
    z[33]=z[8] + 13*z[7];
    z[34]=3*z[9];
    z[35]= - z[34] + 3*z[12] - z[33];
    z[36]=static_cast<T>(10)+ z[22];
    z[36]=z[5]*z[36];
    z[35]=z[36] + n<T>(1,2)*z[35] - z[10];
    z[35]=z[4]*z[35];
    z[36]=z[7]*z[13];
    z[14]=z[14] + z[35] + z[32] + z[29] + z[27] - n<T>(3,2)*z[36] + z[24];
    z[14]=z[6]*z[14];
    z[24]= - z[9]*z[11];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=n<T>(1,2)*z[24] + z[22];
    z[24]=z[4]*z[24];
    z[27]=z[9] + z[7];
    z[29]= - static_cast<T>(8)- z[22];
    z[29]=z[5]*z[29];
    z[24]=z[24] + z[29] + n<T>(3,2)*z[27] + 2*z[10];
    z[24]=z[4]*z[24];
    z[29]=n<T>(3,2)*z[9];
    z[32]=z[8] + z[26];
    z[32]=z[32]*z[29];
    z[35]= - z[7] - z[26];
    z[35]=z[35]*z[30];
    z[22]=z[22]*z[28];
    z[36]=2*z[9];
    z[22]=z[22] - z[10] + n<T>(3,2)*z[8] - z[36];
    z[22]=z[5]*z[22];
    z[16]= - z[16] - z[15];
    z[16]=z[5]*z[16];
    z[15]=z[2] + z[15];
    z[15]=z[4]*z[15];
    z[15]=2*z[15] - static_cast<T>(10)+ z[16];
    z[15]=z[4]*z[15];
    z[16]=static_cast<T>(1)+ z[20];
    z[16]=z[8]*z[16];
    z[15]=z[16] + z[15];
    z[15]=z[6]*z[15];
    z[16]=z[8]*z[7];
    z[15]=z[15] + z[24] + z[22] + z[35] + 14*z[16] + z[32];
    z[15]=z[6]*z[15];
    z[20]=npow(z[7],2);
    z[22]=z[20] + 13*z[16];
    z[24]=z[10] - z[9];
    z[32]=z[8] + z[7];
    z[24]= - 4*z[32] - n<T>(9,2)*z[24];
    z[24]=z[5]*z[24];
    z[35]=3*z[8] + 2*z[12];
    z[37]= - z[9]*z[35];
    z[38]=n<T>(1,2)*z[12];
    z[39]= - z[38] + z[8];
    z[39]=z[10]*z[39];
    z[25]= - z[10] - z[7] - z[25];
    z[25]=z[4]*z[25];
    z[22]=z[25] + z[24] + 3*z[39] + n<T>(1,2)*z[22] + z[37];
    z[22]=z[4]*z[22];
    z[24]=n<T>(1,2)*z[8];
    z[25]=z[33]*z[24];
    z[33]=n<T>(1,2)*z[13] - z[7];
    z[33]=z[33]*z[34];
    z[34]=3*z[7];
    z[37]=z[34] + 2*z[13];
    z[39]=z[10]*z[37];
    z[40]=z[28] - z[8] + z[9];
    z[40]=z[5]*z[40];
    z[25]=z[40] + z[39] + z[25] + z[33];
    z[25]=z[5]*z[25];
    z[33]=z[38] + z[7];
    z[33]=3*z[33] + z[24];
    z[38]=z[9]*z[8];
    z[33]=z[33]*z[38];
    z[39]=n<T>(1,2)*z[7];
    z[31]= - z[31] - z[7];
    z[31]=z[31]*z[39];
    z[31]=z[31] - 3*z[16];
    z[31]=z[10]*z[31];
    z[15]=z[15] + z[22] + z[25] + z[33] + z[31];
    z[15]=z[6]*z[15];
    z[22]=npow(z[9],2);
    z[25]=z[34] + z[8];
    z[25]=z[8]*z[25];
    z[31]= - 6*z[9] + z[10];
    z[31]=z[10]*z[31];
    z[33]=z[10] - z[8];
    z[40]=z[5]*z[33];
    z[25]=z[40] + z[31] + z[22] + z[20] + z[25];
    z[25]=z[5]*z[25];
    z[31]= - z[10]*z[9];
    z[40]= - z[5]*z[27];
    z[31]=z[40] + z[31] - z[16] - z[38];
    z[31]=z[4]*z[31];
    z[35]= - n<T>(5,2)*z[9] + z[35];
    z[35]=z[9]*z[35];
    z[40]=z[24] - z[9];
    z[40]=z[10]*z[40];
    z[35]=z[35] + z[40];
    z[35]=z[10]*z[35];
    z[40]=z[22]*z[8];
    z[25]=z[31] + z[25] - n<T>(3,2)*z[40] + z[35];
    z[25]=z[4]*z[25];
    z[31]=z[22]*z[39];
    z[35]= - z[34] + 5*z[9];
    z[28]=z[35]*z[28];
    z[35]=z[9] + z[37];
    z[35]=z[9]*z[35];
    z[28]=z[35] + z[28];
    z[28]=z[10]*z[28];
    z[35]=z[7] - z[9];
    z[35]=z[10]*z[35];
    z[35]= - z[16] + z[35];
    z[35]=z[5]*z[35];
    z[28]=z[35] + z[31] + z[28];
    z[28]=z[5]*z[28];
    z[31]=npow(z[8],2);
    z[35]= - z[31]*z[39];
    z[37]=npow(z[10],2);
    z[41]= - z[27]*z[37];
    z[35]=z[35] + z[41];
    z[35]=z[5]*z[35];
    z[37]=z[37]*z[7];
    z[41]= - z[9]*z[37];
    z[35]=z[41] + z[35];
    z[35]=z[5]*z[35];
    z[41]=z[5] - z[24];
    z[41]=z[20]*z[41];
    z[42]=z[22]*z[10];
    z[40]=z[42] - z[40] + z[41];
    z[40]=z[4]*z[40];
    z[41]=z[8]*z[42];
    z[31]=z[31]*npow(z[5],2);
    z[31]=z[40] + z[41] + z[31];
    z[31]=z[4]*z[31];
    z[22]= - z[22]*z[37];
    z[23]= - z[23]*npow(z[4],2)*npow(z[6],3);
    z[22]=z[23] + z[31] + z[22] + z[35];
    z[22]=z[1]*z[22];
    z[23]= - z[34] - z[24];
    z[23]=z[8]*z[23];
    z[20]=z[38] - n<T>(1,2)*z[20] + z[23];
    z[20]=z[9]*z[20];
    z[23]=z[8]*z[2];
    z[18]= - z[23] - z[18];
    z[18]=z[18]*z[42];
    z[18]=z[20] + z[18];
    z[18]=z[10]*z[18];
    z[15]=z[22] + z[15] + z[25] + z[18] + z[28];
    z[15]=z[1]*z[15];
    z[18]=z[26] + z[32];
    z[20]= - static_cast<T>(1)- z[23];
    z[20]=z[9]*z[20];
    z[18]=3*z[18] + z[20];
    z[18]=z[9]*z[18];
    z[20]= - z[13]*z[11];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[7]*z[20];
    z[17]=z[9]*z[17];
    z[17]=z[20] + z[17];
    z[17]=z[10]*z[17];
    z[20]=z[13]*z[39];
    z[17]=z[17] + z[18] + z[20] + 2*z[16];
    z[17]=z[10]*z[17];
    z[18]=z[30] - 2*z[32] - z[29];
    z[18]=z[5]*z[18];
    z[20]=n<T>(1,2)*z[16] + z[38];
    z[22]= - z[36] - n<T>(5,2)*z[12] + z[33];
    z[22]=z[10]*z[22];
    z[18]=z[18] + 3*z[20] + z[22];
    z[18]=z[4]*z[18];
    z[20]= - z[34] - z[36];
    z[20]=z[10]*z[20];
    z[22]=n<T>(5,2)*z[13] + z[27];
    z[22]=z[9]*z[22];
    z[16]=z[20] + n<T>(3,2)*z[16] + z[22];
    z[16]=z[5]*z[16];
    z[19]= - z[19] - z[21];
    z[19]=z[8]*z[19];
    z[20]= - z[12]*z[11]*z[38];
    z[19]=z[19] + z[20];
    z[19]=z[9]*z[19];
    z[14]=z[15] + z[14] + z[18] + z[16] + z[19] + z[17];

    r += z[14]*npow(z[3],2)*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r597(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r597(const std::array<dd_real,31>&);
#endif
