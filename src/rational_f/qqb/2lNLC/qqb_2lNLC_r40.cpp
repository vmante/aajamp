#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r40(const std::array<T,31>& k) {
  T z[69];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[11];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[9];
    z[8]=k[15];
    z[9]=k[14];
    z[10]=k[10];
    z[11]=k[8];
    z[12]=k[4];
    z[13]=k[5];
    z[14]=k[3];
    z[15]=n<T>(1,4)*z[11];
    z[16]= - 21*z[6] + 5*z[11];
    z[16]=z[16]*z[15];
    z[17]=n<T>(1,2)*z[4];
    z[18]=11*z[6] + z[4];
    z[18]=z[18]*z[17];
    z[19]=z[7]*z[13];
    z[20]=z[19] + 1;
    z[20]=z[20]*z[7];
    z[21]= - z[17] - z[20] + z[6];
    z[21]=z[5]*z[21];
    z[22]=npow(z[10],2);
    z[23]=n<T>(11,4)*z[7];
    z[24]= - z[23] + z[6];
    z[24]=z[6]*z[24];
    z[16]=z[21] + z[18] + z[16] - z[22] + z[24];
    z[16]=z[5]*z[16];
    z[18]=n<T>(5,4)*z[11];
    z[21]=3*z[6];
    z[24]= - z[18] + n<T>(5,4)*z[7] + z[21];
    z[24]=z[11]*z[24];
    z[25]=z[10]*z[12];
    z[26]=z[25]*z[4];
    z[27]= - z[10] + z[11];
    z[27]=n<T>(1,2)*z[27] - z[26];
    z[27]=z[27]*z[17];
    z[28]=z[3]*z[10];
    z[24]=z[27] + z[24] + z[22] + n<T>(1,4)*z[28];
    z[24]=z[4]*z[24];
    z[27]=n<T>(1,2)*z[6];
    z[29]= - z[7] + 23*z[10];
    z[29]=z[29]*z[27];
    z[30]=n<T>(1,2)*z[11];
    z[31]=5*z[3];
    z[32]= - z[31] + z[11];
    z[32]=z[32]*z[30];
    z[33]=3*z[11];
    z[34]=z[7] - z[10];
    z[34]=n<T>(1,2)*z[34] - z[6];
    z[34]=z[33] + 5*z[34] + n<T>(13,2)*z[3];
    z[34]=z[4]*z[34];
    z[35]= - 11*z[10] - z[3];
    z[35]=z[3]*z[35];
    z[29]=z[34] + z[32] + z[29] + z[35];
    z[32]=n<T>(1,4)*z[5];
    z[34]=z[6] - 3*z[3];
    z[34]=z[32] + z[17] + n<T>(1,2)*z[34] - z[11];
    z[34]=z[8]*z[34];
    z[35]=z[4] - z[11];
    z[23]=z[23] + 3*z[10] + 2*z[35];
    z[23]=z[5]*z[23];
    z[23]=z[34] + n<T>(1,2)*z[29] + z[23];
    z[23]=z[8]*z[23];
    z[29]=3*z[8];
    z[34]=npow(z[13],2);
    z[36]=z[34]*z[4];
    z[37]= - z[36]*z[29];
    z[38]=z[6]*z[13];
    z[39]=7*z[4];
    z[40]= - z[13]*z[39];
    z[37]=z[37] - n<T>(1,2)*z[38] + z[40];
    z[37]=z[8]*z[37];
    z[40]=z[3] + z[6];
    z[37]=z[37] - z[5] - z[39] + z[40];
    z[37]=z[8]*z[37];
    z[39]=z[6]*z[10];
    z[41]=z[39] - z[28];
    z[42]=n<T>(5,2)*z[11];
    z[43]=n<T>(5,2)*z[4];
    z[44]=z[42] - z[43];
    z[19]= - static_cast<T>(2)- z[19];
    z[19]=z[7]*z[19];
    z[19]=z[19] - z[10] - z[44];
    z[19]=z[5]*z[19];
    z[42]=z[42] - z[10] - z[3];
    z[42]=z[4]*z[42];
    z[19]=n<T>(1,2)*z[37] + z[19] + n<T>(3,4)*z[41] + z[42];
    z[19]=z[9]*z[19];
    z[37]=z[6] - z[7];
    z[41]=z[3] + z[37];
    z[41]=z[41]*z[15];
    z[42]=z[6]*z[7];
    z[41]= - 4*z[42] + z[41];
    z[41]=z[11]*z[41];
    z[45]=npow(z[3],2);
    z[46]=z[45]*z[10];
    z[47]=z[10]*npow(z[6],2);
    z[16]=z[19] + z[23] + z[16] + z[24] + z[41] + z[47] - n<T>(3,4)*z[46];
    z[16]=z[9]*z[16];
    z[19]=npow(z[11],2);
    z[23]=n<T>(1,2)*z[19];
    z[24]=z[23]*z[37];
    z[41]=n<T>(1,2)*z[3];
    z[47]= - z[10] + z[41];
    z[47]=z[47]*z[45];
    z[48]=z[11]*z[6];
    z[49]= - z[10]*z[17];
    z[49]= - z[48] + z[49];
    z[49]=z[4]*z[49];
    z[47]=z[49] + z[47] - z[24];
    z[47]=z[4]*z[47];
    z[49]=z[11]*z[3];
    z[50]=z[10] + z[7];
    z[51]=z[50]*z[3];
    z[52]= - n<T>(3,2)*z[51] - z[49];
    z[53]=n<T>(3,2)*z[50] - z[35];
    z[54]=n<T>(1,2)*z[5];
    z[53]=z[53]*z[54];
    z[55]=z[4]*z[3];
    z[52]=z[53] + n<T>(1,2)*z[52] + z[55];
    z[52]=z[8]*z[52];
    z[53]=z[19]*z[41];
    z[56]=z[35]*z[5];
    z[57]= - z[23] + z[56];
    z[57]=z[5]*z[57];
    z[52]=z[52] + z[57] - z[46] + z[53];
    z[52]=z[8]*z[52];
    z[57]= - z[11]*z[42];
    z[57]=z[46] + z[57];
    z[58]= - 7*z[6] + 3*z[7];
    z[59]=z[58]*z[30];
    z[59]= - z[45] + z[59];
    z[59]=z[4]*z[59];
    z[57]=n<T>(3,2)*z[57] + z[59];
    z[59]=n<T>(1,4)*z[4];
    z[58]=z[58]*z[59];
    z[48]=z[58] - n<T>(3,4)*z[42] + z[48];
    z[48]=z[5]*z[48];
    z[58]=n<T>(3,2)*z[11];
    z[60]= - z[3] - z[58];
    z[60]=z[4]*z[60];
    z[56]=z[60] - n<T>(3,2)*z[56];
    z[56]=z[8]*z[56];
    z[48]=z[56] + n<T>(1,2)*z[57] + z[48];
    z[48]=z[9]*z[48];
    z[56]=npow(z[4],2);
    z[22]= - z[56] + z[22] + z[39];
    z[22]=z[5]*z[6]*z[22];
    z[22]=z[48] + z[52] + z[47] + z[22];
    z[22]=z[9]*z[22];
    z[47]= - z[37]*z[56]*z[54];
    z[48]= - z[11]*z[37]*z[17];
    z[48]=z[46] + z[48];
    z[48]=z[4]*z[48];
    z[52]=z[37]*z[4];
    z[52]=z[52] + z[42];
    z[57]= - z[5] - z[11];
    z[52]=z[9]*z[52]*z[57];
    z[47]=n<T>(1,2)*z[52] + z[48] + z[47];
    z[47]=z[9]*z[47];
    z[48]=z[50]*z[45];
    z[52]=z[5]*z[50];
    z[52]= - z[51] + z[52];
    z[52]=z[8]*z[52];
    z[48]=z[48] + z[52];
    z[52]=npow(z[8],2);
    z[52]=n<T>(1,2)*z[52];
    z[48]=z[48]*z[52];
    z[57]= - z[10]*z[55];
    z[24]=z[24] + z[57];
    z[24]=z[24]*z[56];
    z[24]=z[47] + z[24] + z[48];
    z[24]=z[9]*z[24];
    z[47]=z[10]*z[7];
    z[23]=z[56]*z[47]*z[23];
    z[48]=z[3]*z[6];
    z[57]= - z[5]*z[56]*z[48];
    z[60]=npow(z[3],3);
    z[61]=z[60]*z[6];
    z[62]=z[4]*z[61];
    z[57]=z[62] + z[57];
    z[57]=z[5]*z[57];
    z[62]=z[45]*z[42];
    z[63]= - z[8]*z[5]*z[51];
    z[62]=z[62] + z[63];
    z[52]=z[62]*z[52];
    z[23]=z[24] + z[52] + z[23] + z[57];
    z[23]=z[1]*z[23];
    z[24]=n<T>(3,2)*z[8];
    z[52]= - static_cast<T>(1)- z[25];
    z[45]=z[52]*z[45]*z[24];
    z[52]=n<T>(1,4)*z[6];
    z[57]=z[52] - z[10];
    z[62]= - z[3]*z[57];
    z[62]=n<T>(1,2)*z[42] + z[62];
    z[62]=z[3]*z[62];
    z[51]= - n<T>(5,2)*z[51] - z[49];
    z[51]=n<T>(1,2)*z[51] + z[55];
    z[51]=z[5]*z[51];
    z[45]=z[45] + z[62] + z[51];
    z[45]=z[8]*z[45];
    z[49]=z[49] - z[55];
    z[49]=z[5]*z[49];
    z[49]=z[53] + z[49];
    z[49]=z[5]*z[49];
    z[45]=z[49] + z[45];
    z[45]=z[8]*z[45];
    z[49]=z[58] - z[10] - z[52];
    z[49]=z[49]*z[19];
    z[51]=z[11]*z[10];
    z[51]=z[28] + z[51];
    z[51]=z[51]*z[17];
    z[49]=z[49] + z[51];
    z[49]=z[4]*z[49];
    z[51]= - z[19]*z[47];
    z[51]=z[61] + z[51];
    z[49]=n<T>(1,2)*z[51] + z[49];
    z[49]=z[4]*z[49];
    z[51]=n<T>(3,2)*z[6] - z[3];
    z[51]=z[51]*z[54];
    z[53]=z[3]*z[17];
    z[51]=z[51] - z[48] + z[53];
    z[51]=z[56]*z[51];
    z[53]= - z[60]*z[27];
    z[51]=z[53] + z[51];
    z[51]=z[5]*z[51];
    z[22]=n<T>(1,2)*z[23] + z[22] + z[45] + z[49] + z[51];
    z[22]=z[1]*z[22];
    z[23]=z[34]*z[3];
    z[23]=z[23] - z[13];
    z[45]= - z[12] - z[23];
    z[45]=z[45]*z[33];
    z[49]=n<T>(1,2)*z[25];
    z[51]=z[3]*z[13];
    z[45]=z[45] - z[51] + static_cast<T>(1)+ z[49];
    z[45]=z[11]*z[45];
    z[53]=n<T>(1,2)*z[10];
    z[56]=z[53] - z[3];
    z[45]=3*z[56] + z[45];
    z[45]=z[45]*z[30];
    z[60]=n<T>(3,4)*z[6] + z[3];
    z[60]=z[4]*z[60];
    z[45]=z[60] + z[45] - n<T>(3,4)*z[39] + z[48];
    z[45]=z[5]*z[45];
    z[60]=n<T>(1,4)*z[7] - z[10];
    z[60]=z[60]*z[39];
    z[61]=z[3]*z[38];
    z[61]= - z[27] + z[61];
    z[61]=z[61]*z[41];
    z[62]=z[10] + n<T>(1,2)*z[7];
    z[63]= - n<T>(5,2)*z[62] - z[6];
    z[63]=z[6]*z[63];
    z[61]=z[63] + z[61];
    z[61]=z[3]*z[61];
    z[63]=z[53] + z[3];
    z[64]= - z[33] - z[63];
    z[64]=z[11]*z[64];
    z[64]=7*z[39] + z[64];
    z[64]=z[64]*z[30];
    z[65]=z[4]*z[21];
    z[65]= - z[48] + z[65];
    z[65]=z[65]*z[59];
    z[45]=z[45] + z[65] + z[64] + z[60] + z[61];
    z[45]=z[5]*z[45];
    z[60]=3*z[38];
    z[61]= - z[60] + static_cast<T>(1)+ 5*z[25];
    z[61]=z[3]*z[61];
    z[61]=5*z[10] + z[61];
    z[61]=z[3]*z[61];
    z[42]=z[42] + z[61];
    z[35]= - z[3] - z[50] - z[35];
    z[32]=z[35]*z[32];
    z[35]=npow(z[12],2);
    z[61]=z[35]*z[10];
    z[64]=z[12] + z[61];
    z[64]=z[3]*z[64];
    z[64]=z[64] + n<T>(1,2) - z[25];
    z[64]=z[3]*z[64]*z[29];
    z[57]= - z[11]*z[57];
    z[32]=z[64] + z[32] + n<T>(1,4)*z[42] + z[57];
    z[32]=z[8]*z[32];
    z[42]= - z[10] + z[6];
    z[42]=z[3]*z[42];
    z[57]= - z[10] - 9*z[6];
    z[57]=z[11]*z[57];
    z[42]=z[57] - 5*z[47] + z[42];
    z[42]=z[42]*z[59];
    z[57]=5*z[62];
    z[62]= - z[57] - z[41];
    z[62]=z[3]*z[62];
    z[64]=n<T>(5,2)*z[47];
    z[62]=z[64] + z[62];
    z[65]=n<T>(15,4)*z[3] - z[11];
    z[65]=z[11]*z[65];
    z[55]= - n<T>(13,4)*z[55] + n<T>(1,2)*z[62] + z[65];
    z[55]=z[5]*z[55];
    z[62]=z[6] + z[10];
    z[65]= - static_cast<T>(1)- z[38];
    z[65]=z[3]*z[65];
    z[65]= - n<T>(1,2)*z[62] + z[65];
    z[65]=z[3]*z[65];
    z[57]=z[6]*z[57];
    z[57]=z[57] + z[65];
    z[57]=z[3]*z[57];
    z[65]=z[39]*z[7];
    z[57]= - n<T>(3,2)*z[65] + z[57];
    z[66]=z[39] - z[48];
    z[67]=n<T>(1,4)*z[10];
    z[68]= - z[67] + z[6];
    z[68]=z[11]*z[68];
    z[66]=n<T>(5,2)*z[66] + z[68];
    z[66]=z[11]*z[66];
    z[32]=z[32] + z[55] + z[42] + n<T>(1,2)*z[57] + z[66];
    z[32]=z[8]*z[32];
    z[42]=z[33] - z[53] - z[6];
    z[42]=z[11]*z[42];
    z[42]= - z[64] + z[42];
    z[42]=z[11]*z[42];
    z[53]=z[38] - z[25];
    z[55]= - static_cast<T>(3)- z[53];
    z[57]=z[34]*z[6];
    z[64]= - z[12] - z[57];
    z[64]=z[11]*z[64];
    z[55]=n<T>(1,2)*z[55] + z[64];
    z[19]=z[55]*z[19];
    z[19]= - z[48] + z[19];
    z[26]= - z[11]*z[26];
    z[19]=3*z[19] + z[26];
    z[19]=z[4]*z[19];
    z[19]=z[19] + 3*z[46] + z[42];
    z[19]=z[19]*z[17];
    z[26]=npow(z[7],2);
    z[42]= - z[26]*z[39];
    z[46]=z[47]*z[15];
    z[46]=z[65] + z[46];
    z[46]=z[11]*z[46];
    z[16]=z[22] + z[16] + z[32] + z[45] + z[19] + z[42] + z[46];
    z[19]=npow(z[2],2);
    z[16]=z[1]*z[19]*z[16];
    z[22]= - 3*z[13] + z[36];
    z[22]=z[22]*z[24];
    z[32]=2*z[13];
    z[42]=z[4]*z[32];
    z[22]=z[22] + z[42] + z[51] - n<T>(13,4) - z[38];
    z[22]=z[8]*z[22];
    z[42]=n<T>(7,4)*z[7];
    z[45]=2*z[10];
    z[18]=z[22] - n<T>(3,4)*z[5] - z[59] + z[18] - n<T>(3,2)*z[3] + z[6] + z[42]
    - z[45];
    z[18]=z[8]*z[18];
    z[22]= - z[7]*z[34];
    z[22]= - z[32] + z[22];
    z[22]=z[7]*z[22];
    z[46]=z[11]*z[12];
    z[55]=z[4]*z[12];
    z[22]=z[55] - z[46] - static_cast<T>(1)+ z[22];
    z[22]=z[5]*z[22];
    z[55]=npow(z[13],3);
    z[64]= - z[4]*z[55];
    z[64]=z[34] + z[64];
    z[24]=z[64]*z[24];
    z[24]=z[24] + z[32] - n<T>(7,2)*z[36];
    z[24]=z[8]*z[24];
    z[32]= - z[54] - z[43];
    z[32]=z[13]*z[32];
    z[24]=z[24] + static_cast<T>(1)+ z[32];
    z[24]=z[8]*z[24];
    z[32]= - static_cast<T>(1)+ z[46];
    z[32]=z[4]*z[32];
    z[20]=z[24] + z[22] - z[20] + z[32];
    z[20]=z[9]*z[20];
    z[17]= - z[17]*z[46];
    z[22]=z[46] + n<T>(15,4);
    z[22]=z[22]*z[11];
    z[17]=z[17] - z[22] - z[6] - z[56];
    z[17]=z[4]*z[17];
    z[24]= - z[34]*z[26];
    z[32]=z[12]*z[30];
    z[24]=z[32] + n<T>(1,2) + z[24];
    z[24]=z[5]*z[24];
    z[22]=z[24] - z[43] + z[7] + z[22];
    z[22]=z[5]*z[22];
    z[24]= - 7*z[37] + z[31];
    z[24]=z[24]*z[15];
    z[31]=2*z[26];
    z[17]=z[20] + z[18] + z[22] + z[17] + z[24] - n<T>(7,4)*z[28] + z[31] + 
   n<T>(9,4)*z[39];
    z[17]=z[9]*z[17];
    z[18]= - z[12] - n<T>(5,2)*z[61];
    z[18]=z[3]*z[18];
    z[18]=z[18] + z[38] - n<T>(1,4) + 3*z[25];
    z[18]=z[3]*z[18];
    z[20]= - z[10]*npow(z[12],3);
    z[20]= - z[35] + z[20];
    z[20]=z[20]*z[41];
    z[20]=z[20] - n<T>(1,2)*z[12] + z[61];
    z[20]=z[3]*z[20];
    z[20]=z[20] + static_cast<T>(1)- z[49];
    z[20]=z[20]*z[29];
    z[18]=z[20] + z[59] - z[67] + z[18];
    z[18]=z[8]*z[18];
    z[20]=z[30] + z[21] + z[63];
    z[20]=z[4]*z[20];
    z[21]=n<T>(3,2) + z[51];
    z[21]=z[3]*z[21];
    z[21]=n<T>(7,2)*z[50] + z[21] + z[44];
    z[21]=z[21]*z[54];
    z[22]=n<T>(3,2)*z[10] - z[6];
    z[24]=z[3]*z[25];
    z[22]=n<T>(1,2)*z[22] - 2*z[24];
    z[22]=z[3]*z[22];
    z[24]= - n<T>(5,2)*z[62] - z[3];
    z[24]=z[24]*z[30];
    z[28]=n<T>(7,4)*z[47];
    z[29]= - z[42] - 4*z[10];
    z[29]=z[6]*z[29];
    z[18]=z[18] + z[21] + z[20] + z[24] + z[22] - z[28] + z[29];
    z[18]=z[8]*z[18];
    z[20]= - z[34]*z[41];
    z[20]=z[12] + z[20];
    z[20]=z[20]*z[33];
    z[20]=z[20] + z[51] + n<T>(1,4) - z[25];
    z[20]=z[11]*z[20];
    z[20]=z[20] + n<T>(3,4)*z[3] - z[45] + n<T>(15,4)*z[6];
    z[20]=z[11]*z[20];
    z[21]=z[13] - z[12];
    z[21]=z[13]*z[21];
    z[22]= - z[3]*z[55];
    z[21]=z[21] + z[22];
    z[21]=z[21]*z[58];
    z[21]=z[21] + z[23];
    z[21]=z[11]*z[21];
    z[21]= - n<T>(1,2) + z[21];
    z[21]=z[11]*z[21];
    z[22]=z[13]*z[26];
    z[21]=z[21] + z[22] - z[41];
    z[21]=z[5]*z[21];
    z[22]=7*z[7] + 9*z[10];
    z[22]=z[22]*z[52];
    z[23]=z[27] + z[10];
    z[24]= - z[3]*z[23];
    z[25]= - n<T>(7,4)*z[6] + z[3];
    z[25]=z[4]*z[25];
    z[20]=z[21] + z[25] + z[20] + z[24] + z[31] + z[22];
    z[20]=z[5]*z[20];
    z[15]=z[40]*z[15];
    z[15]=z[15] + n<T>(5,2)*z[48] - z[28] + z[39];
    z[15]=z[11]*z[15];
    z[21]=z[34]*z[27];
    z[21]= - z[12] + z[21];
    z[21]=z[21]*z[33];
    z[21]=z[21] - n<T>(3,4) - z[53];
    z[21]=z[11]*z[21];
    z[21]=n<T>(3,2)*z[23] + z[21];
    z[21]=z[11]*z[21];
    z[22]= - z[12] - z[13];
    z[22]=z[13]*z[22];
    z[23]= - z[6]*z[55];
    z[22]=z[22] + z[23];
    z[22]=z[22]*z[58];
    z[22]=z[22] + n<T>(1,2)*z[13] + 2*z[57];
    z[22]=z[11]*z[22];
    z[23]= - static_cast<T>(1)- z[60];
    z[22]=n<T>(1,2)*z[23] + z[22];
    z[22]=z[11]*z[22];
    z[22]=z[27] + z[22];
    z[22]=z[4]*z[22];
    z[23]=z[3]*z[62];
    z[21]=z[22] + z[23] + z[21];
    z[21]=z[4]*z[21];
    z[22]=z[7]*z[14];
    z[22]=static_cast<T>(1)+ 2*z[22];
    z[22]=z[22]*z[47];
    z[22]= - z[26] + z[22];
    z[22]=z[6]*z[22];
    z[23]= - z[10]*z[41];
    z[23]= - 3*z[39] + z[23];
    z[23]=z[3]*z[23];
    z[24]= - z[10]*z[26];
    z[15]=z[17] + z[18] + z[20] + z[21] + z[15] + z[23] + z[24] + z[22];
    z[15]=z[15]*z[19];
    z[15]=z[15] + z[16];

    r += z[15]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r40(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r40(const std::array<dd_real,31>&);
#endif
