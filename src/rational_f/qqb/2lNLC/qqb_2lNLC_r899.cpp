#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r899(const std::array<T,31>& k) {
  T z[44];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[14];
    z[7]=k[12];
    z[8]=k[13];
    z[9]=k[3];
    z[10]=k[9];
    z[11]=k[4];
    z[12]=k[15];
    z[13]=k[5];
    z[14]=k[8];
    z[15]=z[14]*z[13];
    z[16]= - static_cast<T>(3)+ n<T>(11,4)*z[15];
    z[17]=npow(z[14],2);
    z[18]=3*z[17];
    z[19]=z[16]*z[18];
    z[20]=n<T>(1,2)*z[12];
    z[21]=19*z[12];
    z[22]=z[21]*z[13];
    z[23]= - static_cast<T>(23)- z[22];
    z[23]=z[12]*z[23];
    z[23]= - n<T>(185,2)*z[14] + z[23];
    z[23]=z[23]*z[20];
    z[24]=z[8]*z[14];
    z[25]=npow(z[13],2);
    z[26]=z[25]*z[14];
    z[26]=11*z[26];
    z[27]=z[13] + z[26];
    z[27]=z[14]*z[27];
    z[27]= - static_cast<T>(203)+ 3*z[27];
    z[27]=z[27]*z[24];
    z[28]=n<T>(1,2)*z[7];
    z[29]= - n<T>(113,2)*z[8] + 9*z[6];
    z[29]=z[29]*z[28];
    z[30]=z[4]*z[12];
    z[31]=z[12]*z[25];
    z[31]= - z[13] + n<T>(19,2)*z[31];
    z[31]=z[12]*z[31];
    z[31]=n<T>(239,4) + z[31];
    z[31]=z[12]*z[31];
    z[31]=27*z[4] + n<T>(9,2)*z[14] + z[31];
    z[31]=z[6]*z[31];
    z[32]= - 459*z[12] - 137*z[4];
    z[32]=n<T>(95,2)*z[7] + n<T>(137,2)*z[6] + n<T>(1,2)*z[32] - 45*z[8];
    z[32]=z[3]*z[32];
    z[19]=n<T>(1,2)*z[32] + z[29] + z[31] + n<T>(1,4)*z[27] - n<T>(23,4)*z[30] + 
    z[19] + z[23];
    z[23]=n<T>(1,4)*z[5];
    z[19]=z[19]*z[23];
    z[27]=npow(z[10],2);
    z[29]=3*z[27];
    z[31]=z[10]*z[13];
    z[32]=static_cast<T>(1)+ n<T>(3,4)*z[31];
    z[32]=z[32]*z[29];
    z[33]=z[12]*z[13];
    z[33]=static_cast<T>(3)- n<T>(19,2)*z[33];
    z[33]=z[12]*z[33];
    z[33]= - n<T>(3,4)*z[10] + z[33];
    z[33]=z[12]*z[33];
    z[34]=n<T>(3,2)*z[8];
    z[35]= - z[10]*z[34];
    z[32]=z[35] - n<T>(227,4)*z[30] + z[32] + z[33];
    z[32]=z[6]*z[32];
    z[33]=z[10]*z[9];
    z[35]= - static_cast<T>(1)- n<T>(3,4)*z[33];
    z[35]=z[35]*z[29];
    z[21]=z[21]*z[11];
    z[36]= - static_cast<T>(25)- z[21];
    z[36]=z[12]*z[36];
    z[36]=z[36] + n<T>(3,2)*z[10];
    z[36]=z[36]*z[20];
    z[35]=z[35] + z[36];
    z[35]=z[4]*z[35];
    z[36]=11*z[15];
    z[37]=z[36] - 1;
    z[37]=z[37]*z[17];
    z[38]=3*z[10];
    z[39]= - z[9]*z[38];
    z[39]= - static_cast<T>(1)+ z[39];
    z[39]=z[39]*z[27];
    z[39]=z[39] - z[37];
    z[40]=n<T>(1,2)*z[10];
    z[41]=npow(z[9],2)*z[38];
    z[41]=5*z[9] + z[41];
    z[41]=z[41]*z[40];
    z[41]=static_cast<T>(1)+ z[41];
    z[42]=z[4]*z[10];
    z[41]=z[41]*z[42];
    z[39]=n<T>(1,2)*z[39] + z[41];
    z[34]=z[39]*z[34];
    z[39]=npow(z[12],3);
    z[39]=19*z[39];
    z[41]=npow(z[10],3);
    z[43]= - n<T>(9,2)*z[41] + z[39];
    z[32]=z[32] + z[34] + n<T>(1,2)*z[43] + z[35];
    z[34]=z[12]*z[11];
    z[34]=static_cast<T>(11)+ n<T>(19,4)*z[34];
    z[34]=z[12]*z[34];
    z[34]=n<T>(185,8)*z[14] + z[34];
    z[34]=z[12]*z[34];
    z[34]=n<T>(3,8)*z[37] + z[34];
    z[35]=z[12]*npow(z[11],2);
    z[35]=6*z[11] + n<T>(19,8)*z[35];
    z[35]=z[12]*z[35];
    z[35]=n<T>(305,16) + z[35];
    z[35]=z[35]*z[30];
    z[37]=n<T>(1,8)*z[7];
    z[26]= - 25*z[13] + z[26];
    z[26]=z[14]*z[26];
    z[26]= - static_cast<T>(85)+ n<T>(3,2)*z[26];
    z[26]=z[14]*z[26];
    z[26]=z[26] + n<T>(111,2)*z[8];
    z[26]=z[26]*z[37];
    z[43]= - n<T>(15,4)*z[12] - 29*z[4];
    z[43]=z[6]*z[43];
    z[26]=z[26] + n<T>(1,4)*z[43] - n<T>(15,16)*z[24] + n<T>(1,2)*z[34] + z[35];
    z[26]=z[3]*z[26];
    z[31]= - static_cast<T>(1)- n<T>(3,2)*z[31];
    z[31]=z[31]*z[27];
    z[16]= - z[16]*z[17];
    z[16]= - n<T>(1,4)*z[42] + n<T>(1,2)*z[31] + z[16];
    z[17]=z[10]*z[25];
    z[17]=z[13] + z[17];
    z[17]=z[10]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[40];
    z[17]=z[17] - z[14];
    z[17]=3*z[17] + n<T>(1,2)*z[4];
    z[17]=z[6]*z[17];
    z[16]=n<T>(3,8)*z[17] + n<T>(3,4)*z[16] + 14*z[24];
    z[16]=z[7]*z[16];
    z[16]=z[19] + z[26] + n<T>(1,4)*z[32] + z[16];
    z[17]=npow(z[2],2);
    z[16]=z[16]*z[17];
    z[19]=z[6]*z[4];
    z[25]= - 83*z[19] - 61*z[24];
    z[25]=z[12]*z[25];
    z[21]= - n<T>(13,2) - z[21];
    z[21]=z[4]*z[21]*npow(z[12],2);
    z[21]= - z[39] + z[21];
    z[26]= - static_cast<T>(9)+ z[36];
    z[26]=z[26]*z[18];
    z[31]=z[12]*z[14];
    z[26]=z[26] + 239*z[31];
    z[32]=3*z[8];
    z[34]=n<T>(349,2)*z[14] - z[32];
    z[34]=z[8]*z[34];
    z[26]=n<T>(1,2)*z[26] + z[34];
    z[26]=z[26]*z[28];
    z[34]=9*z[7];
    z[35]= - z[34] + n<T>(37,4)*z[4];
    z[36]=npow(z[3],2);
    z[36]=3*z[36];
    z[39]=z[35]*z[36];
    z[21]=z[39] + z[26] + n<T>(1,2)*z[21] + z[25];
    z[21]=z[3]*z[21];
    z[25]=z[13]*z[38];
    z[25]=static_cast<T>(5)+ z[25];
    z[25]=z[25]*z[29];
    z[26]= - z[38] - 239*z[14];
    z[26]=z[12]*z[26];
    z[25]= - 3*z[30] + z[25] + z[26];
    z[26]= - 3*z[4] - z[38] - n<T>(329,2)*z[14];
    z[26]=z[8]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[26];
    z[26]=n<T>(1,2)*z[6];
    z[25]=z[25]*z[26];
    z[29]=npow(z[14],3);
    z[38]=z[10]*z[30];
    z[38]= - 11*z[29] + z[38];
    z[39]=z[8]*z[4];
    z[40]= - z[10]*z[39];
    z[38]=n<T>(1,4)*z[38] + z[40];
    z[28]= - z[6]*z[28];
    z[28]= - z[39] + z[28];
    z[28]=z[7]*z[28];
    z[25]=n<T>(3,2)*z[28] + 3*z[38] + z[25];
    z[25]=z[7]*z[25];
    z[28]= - static_cast<T>(1)- z[33];
    z[27]=z[28]*z[27];
    z[28]=z[12]*z[10];
    z[27]=n<T>(3,2)*z[27] - z[28];
    z[27]=z[27]*z[39];
    z[28]=z[28] + z[30];
    z[28]=z[6]*z[8]*z[28];
    z[33]=z[4]*z[41];
    z[27]=z[28] + n<T>(3,2)*z[33] + z[27];
    z[21]=z[21] + n<T>(3,2)*z[27] + z[25];
    z[15]=static_cast<T>(1)+ n<T>(11,8)*z[15];
    z[15]=z[15]*z[18];
    z[18]=npow(z[8],2);
    z[15]= - n<T>(5,8)*z[18] + z[15] + n<T>(61,2)*z[31];
    z[15]=z[8]*z[15];
    z[22]=n<T>(1,2) + z[22];
    z[22]=z[12]*z[22];
    z[22]=n<T>(239,2)*z[14] + z[22];
    z[20]=z[22]*z[20];
    z[20]=n<T>(329,4)*z[24] + z[20] + 85*z[30];
    z[20]=z[20]*z[26];
    z[22]=329*z[8] + 239*z[12];
    z[22]=z[6]*z[22]*z[37];
    z[15]=z[22] + z[20] + n<T>(33,8)*z[29] + z[15];
    z[20]=z[8]*z[12];
    z[20]= - n<T>(485,4)*z[30] + 61*z[20];
    z[22]= - n<T>(239,2)*z[12] - 221*z[8];
    z[22]=z[22]*z[37];
    z[24]=n<T>(437,16)*z[12] + 28*z[4];
    z[24]=z[6]*z[24];
    z[25]=z[32] + n<T>(7,2)*z[6];
    z[25]=z[3]*z[25];
    z[20]=n<T>(3,4)*z[25] + z[22] + n<T>(1,4)*z[20] + z[24];
    z[20]=z[3]*z[20];
    z[22]=n<T>(29,2)*z[4] + z[34];
    z[22]=z[3]*z[22];
    z[24]= - n<T>(119,4)*z[6] + 27*z[8];
    z[25]= - z[5]*z[24];
    z[22]=z[22] + z[25];
    z[22]=z[22]*z[23];
    z[15]=z[22] + n<T>(1,2)*z[15] + z[20];
    z[15]=z[5]*z[15];
    z[20]=npow(z[8],3);
    z[18]= - z[7]*z[18];
    z[18]= - z[20] + z[18];
    z[22]=z[7]*z[8];
    z[23]= - 9*z[22] + n<T>(37,4)*z[19];
    z[25]=n<T>(37,4)*z[6] - 9*z[8] - z[35];
    z[25]=z[3]*z[25];
    z[25]=z[25] - z[23];
    z[25]=z[3]*z[25];
    z[18]=n<T>(5,4)*z[18] + 3*z[25];
    z[18]=z[3]*z[18];
    z[22]= - 27*z[22] + n<T>(119,4)*z[19];
    z[25]=z[3]*z[22];
    z[24]=27*z[7] - n<T>(119,4)*z[4] - z[24];
    z[24]=z[3]*z[24];
    z[24]=z[24] + z[22];
    z[24]=z[5]*z[24];
    z[24]=z[25] + z[24];
    z[24]=z[5]*z[24];
    z[20]=z[20]*z[7];
    z[20]=n<T>(5,4)*z[20];
    z[18]=z[24] - z[20] + z[18];
    z[18]=z[5]*z[18];
    z[23]=z[23]*z[36];
    z[20]=z[23] - z[20];
    z[22]=z[22]*npow(z[5],2);
    z[22]=z[20] + z[22];
    z[22]=z[5]*z[3]*z[22];
    z[19]=z[19]*z[8];
    z[23]=npow(z[7],3)*z[19];
    z[22]=n<T>(3,4)*z[23] + z[22];
    z[22]=z[1]*z[22];
    z[23]= - z[4] + z[8];
    z[23]=z[6]*z[23];
    z[23]= - z[39] + z[23];
    z[23]=z[7]*z[23];
    z[19]= - z[19] + z[23];
    z[19]=z[19]*npow(z[7],2);
    z[20]= - z[3]*z[20];
    z[18]=z[22] + z[18] + n<T>(3,4)*z[19] + z[20];
    z[18]=z[1]*z[18];
    z[15]=n<T>(1,4)*z[18] + n<T>(1,4)*z[21] + z[15];
    z[15]=z[1]*z[17]*z[15];
    z[15]=z[16] + z[15];

    r += z[15]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r899(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r899(const std::array<dd_real,31>&);
#endif
