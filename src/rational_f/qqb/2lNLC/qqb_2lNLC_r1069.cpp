#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1069(const std::array<T,31>& k) {
  T z[90];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[19];
    z[6]=k[22];
    z[7]=k[13];
    z[8]=k[11];
    z[9]=k[4];
    z[10]=k[10];
    z[11]=k[15];
    z[12]=k[14];
    z[13]=k[8];
    z[14]=k[9];
    z[15]=k[17];
    z[16]=k[2];
    z[17]=k[3];
    z[18]=k[5];
    z[19]=2*z[3];
    z[20]=npow(z[9],2);
    z[21]=z[20]*z[10];
    z[22]= - z[9] - z[21];
    z[22]=z[22]*z[19];
    z[23]=4*z[18];
    z[24]=z[23]*z[14];
    z[25]=n<T>(40,3)*z[9] - z[21];
    z[25]=z[10]*z[25];
    z[22]=z[22] + z[24] + n<T>(31,3) + z[25];
    z[22]=z[3]*z[22];
    z[25]=z[12] - z[10];
    z[26]=z[13]*z[16];
    z[27]=z[26]*z[25];
    z[28]=2*z[12];
    z[29]=npow(z[18],2);
    z[30]=z[29]*z[12];
    z[31]=3*z[30];
    z[32]= - n<T>(28,3)*z[18] + z[31];
    z[32]=z[32]*z[28];
    z[24]= - z[24] + static_cast<T>(1)+ z[32];
    z[24]=z[8]*z[24];
    z[32]=2*z[9];
    z[33]=z[18] + z[32] + z[21];
    z[33]=z[3]*z[33];
    z[34]= - z[8]*z[30];
    z[33]=z[34] + z[33];
    z[34]=n<T>(44,3)*z[11];
    z[33]=z[33]*z[34];
    z[22]=z[33] + z[22] + 4*z[27] + z[24];
    z[22]=z[11]*z[22];
    z[24]=2*z[14];
    z[27]=z[14]*z[18];
    z[33]= - static_cast<T>(2)- z[27];
    z[33]=z[33]*z[24];
    z[35]=z[7]*z[18];
    z[36]=z[10]*z[9];
    z[37]= - n<T>(1,2) - z[36];
    z[37]=3*z[37] + n<T>(1,2)*z[35];
    z[37]=z[3]*z[37];
    z[38]=n<T>(1,2)*z[12];
    z[39]=z[36] + 3;
    z[40]=z[10]*z[39];
    z[33]=z[37] + z[33] + z[7] + z[40] + z[38];
    z[33]=z[3]*z[33];
    z[37]=z[10]*z[13];
    z[40]=static_cast<T>(2)+ z[26];
    z[40]=z[40]*z[37];
    z[41]=z[26] - 1;
    z[42]=z[12]*z[13];
    z[43]= - z[41]*z[42];
    z[40]=z[40] + z[43];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[24];
    z[43]=n<T>(1,2)*z[10];
    z[44]=z[12]*z[18];
    z[45]= - n<T>(13,3) + 10*z[44];
    z[45]=z[12]*z[45];
    z[27]=z[27] - n<T>(1,2)*z[7] - z[43] + z[45];
    z[27]=z[8]*z[27];
    z[45]=2*z[18];
    z[46]=npow(z[13],2);
    z[47]=z[45]*z[46];
    z[39]=z[39]*z[43];
    z[43]=z[45]*z[14];
    z[48]=static_cast<T>(5)+ z[43];
    z[48]=z[14]*z[48];
    z[39]=z[48] - n<T>(7,2)*z[12] - z[47] + z[13] + z[39];
    z[39]=z[4]*z[39];
    z[48]=static_cast<T>(2)+ 3*z[44];
    z[48]=z[12]*z[48];
    z[48]=z[48] + z[13] + z[47];
    z[48]=z[7]*z[48];
    z[49]=3*z[7];
    z[50]= - z[7]*z[43];
    z[50]= - z[49] + z[50];
    z[50]=z[14]*z[50];
    z[22]=z[22] + z[33] + z[39] + z[27] + z[50] + 2*z[40] + z[48];
    z[22]=z[11]*z[22];
    z[27]=npow(z[13],3);
    z[33]=z[23]*z[27];
    z[39]=4*z[15];
    z[40]=z[39] - z[38];
    z[40]=z[12]*z[40];
    z[43]= - static_cast<T>(3)- z[43];
    z[43]=z[14]*z[43];
    z[43]= - z[28] + z[43];
    z[43]=z[43]*z[24];
    z[48]=2*z[46];
    z[50]= - z[39] - n<T>(3,2)*z[10];
    z[50]=z[10]*z[50];
    z[51]=z[13] - z[5];
    z[52]=n<T>(8,3)*z[51];
    z[53]=z[8]*z[52];
    z[40]=z[53] + z[43] + z[40] + z[33] - z[48] + z[50];
    z[40]=z[4]*z[40];
    z[43]=n<T>(2,3)*z[5];
    z[50]= - z[15] - z[43];
    z[50]=4*z[50] + z[38];
    z[50]=z[12]*z[50];
    z[53]=z[28] + z[10];
    z[54]=n<T>(4,3)*z[14];
    z[55]=z[53]*z[54];
    z[56]=z[7]*z[5];
    z[57]=z[15] - z[5];
    z[57]=4*z[57] - z[10];
    z[57]=z[10]*z[57];
    z[52]= - z[4]*z[52];
    z[58]=n<T>(3,2)*z[7];
    z[58]= - z[3]*z[58];
    z[50]=z[58] + z[52] + z[55] - 4*z[56] + z[57] + z[50];
    z[50]=z[3]*z[50];
    z[52]=z[45]*z[27];
    z[55]=z[52] + z[46];
    z[57]=npow(z[12],2);
    z[58]=npow(z[14],3)*z[45];
    z[58]=z[58] + 2*z[57] - z[55];
    z[58]=z[7]*z[58];
    z[59]= - z[10]*z[46];
    z[58]=z[59] + z[58];
    z[59]=z[7] + z[10];
    z[60]= - z[51]*z[59];
    z[43]=z[43] + z[12];
    z[43]=z[12]*z[43];
    z[43]=z[43] + z[60];
    z[60]=npow(z[14],2);
    z[43]=2*z[43] + z[60];
    z[61]=n<T>(1,2)*z[8];
    z[59]= - z[59]*z[61];
    z[43]=2*z[43] + z[59];
    z[43]=z[8]*z[43];
    z[22]=z[22] + z[50] + z[40] + 2*z[58] + z[43];
    z[22]=z[11]*z[22];
    z[40]=z[10]*z[15];
    z[43]= - z[46] + n<T>(1,2)*z[40];
    z[50]=npow(z[15],2);
    z[58]= - n<T>(1,2)*z[50] + z[43];
    z[58]=z[10]*z[58];
    z[59]=z[50]*z[38];
    z[62]=2*z[27];
    z[63]=npow(z[6],2);
    z[64]=n<T>(3,2)*z[63];
    z[65]=z[7]*z[64];
    z[58]=z[65] + z[59] + z[62] + z[58];
    z[58]=z[8]*z[58];
    z[59]=2*z[6];
    z[65]=z[59] + z[5];
    z[65]=n<T>(2,3)*z[65];
    z[66]=z[65]*z[63];
    z[67]=z[66]*z[7];
    z[68]=n<T>(1,2)*z[57];
    z[69]=npow(z[5],2);
    z[70]=z[68]*z[69];
    z[67]=z[67] + z[70];
    z[70]= - z[14]*z[46];
    z[70]=z[62] + z[70];
    z[70]=z[70]*z[24];
    z[58]=z[58] + z[70] - z[67];
    z[58]=z[8]*z[58];
    z[70]=z[6] + z[5];
    z[71]=n<T>(4,3)*z[4];
    z[72]=z[70]*z[71];
    z[65]=z[65]*z[6];
    z[65]=z[72] - z[65] + n<T>(3,2)*z[69];
    z[72]= - z[4]*z[65];
    z[72]= - z[66] + z[72];
    z[72]=z[4]*z[72];
    z[68]= - z[3]*z[5]*z[68];
    z[67]=z[68] + z[72] + z[67];
    z[67]=z[3]*z[67];
    z[68]=npow(z[6],3);
    z[72]=z[56]*z[68];
    z[73]=z[4]*z[6];
    z[73]=z[73] - z[63];
    z[73]=z[73]*z[4];
    z[73]=z[73] + z[68];
    z[74]=z[4]*z[5]*z[73];
    z[74]= - z[72] + z[74];
    z[74]=z[3]*z[74];
    z[72]=z[8]*z[72];
    z[73]= - z[4]*z[73]*z[8]*z[5];
    z[72]=z[74] + z[72] + z[73];
    z[72]=z[1]*z[72];
    z[73]=z[63]*z[57];
    z[74]=z[15] + z[6];
    z[74]=z[12]*z[74];
    z[74]=z[50] + z[74];
    z[74]=z[12]*z[74];
    z[75]= - z[10]*z[50];
    z[74]=z[75] + z[74];
    z[74]=z[7]*z[74];
    z[73]=z[73] + z[74];
    z[74]=n<T>(5,2)*z[7];
    z[73]=z[73]*z[74];
    z[70]=z[70]*z[8];
    z[65]=n<T>(3,2)*z[70] + z[65];
    z[65]=z[4]*z[65];
    z[64]= - z[8]*z[64];
    z[64]=z[65] + z[66] + z[64];
    z[64]=z[8]*z[64];
    z[65]=n<T>(5,2)*z[57];
    z[66]= - z[63]*z[65];
    z[64]=z[66] + z[64];
    z[64]=z[4]*z[64];
    z[66]=z[24]*z[42];
    z[70]= - z[12]*z[46];
    z[66]=z[70] + z[66];
    z[60]=z[66]*z[60];
    z[66]= - z[4]*z[48];
    z[70]=static_cast<T>(1)+ z[36];
    z[70]=z[11]*z[70]*npow(z[3],2);
    z[66]=z[66] + z[70];
    z[66]=z[11]*z[66];
    z[70]=z[4]*z[27];
    z[66]=4*z[70] + z[66];
    z[66]=z[11]*z[66];
    z[58]=n<T>(4,3)*z[72] + z[66] + z[67] + z[64] + z[58] + z[73] + 2*z[60];
    z[58]=z[1]*z[58];
    z[60]=z[62]*z[29];
    z[64]=n<T>(1,2)*z[6];
    z[51]= - z[60] - z[64] - z[51];
    z[51]=z[7]*z[51];
    z[66]=z[13]*z[9];
    z[67]=z[66] + 1;
    z[70]= - z[67]*z[48];
    z[72]=3*z[15];
    z[73]=z[72] - z[5];
    z[75]=static_cast<T>(2)+ z[66];
    z[75]=z[13]*z[75];
    z[75]=n<T>(1,2)*z[73] + z[75];
    z[75]=z[10]*z[75];
    z[76]=n<T>(3,2)*z[12];
    z[77]= - z[15] - z[13];
    z[76]=z[77]*z[76];
    z[51]=z[51] + z[76] + z[70] + z[75];
    z[51]=z[8]*z[51];
    z[70]= - z[46] + z[52];
    z[75]=z[13] - z[6];
    z[76]=n<T>(8,3)*z[75];
    z[74]= - z[76] - z[74];
    z[74]=z[7]*z[74];
    z[77]= - z[18]*z[46];
    z[49]= - z[49] - 3*z[12] + z[13] + z[77];
    z[49]=z[49]*z[24];
    z[49]=z[49] + z[74] + 2*z[70] - z[65];
    z[49]=z[14]*z[49];
    z[70]=z[50] - z[69];
    z[43]=n<T>(1,2)*z[70] - z[43];
    z[74]=3*z[10];
    z[43]=z[43]*z[74];
    z[77]=n<T>(3,2)*z[70];
    z[78]=z[12]*z[5];
    z[79]= - n<T>(5,2)*z[78] - z[77] - 4*z[46];
    z[79]=z[12]*z[79];
    z[69]=n<T>(5,2)*z[69];
    z[80]=z[69] + z[48];
    z[81]=n<T>(8,3)*z[5];
    z[82]= - z[81] + n<T>(7,2)*z[6];
    z[82]=z[82]*z[6];
    z[83]=z[33] - z[82] + z[80];
    z[83]=z[7]*z[83];
    z[43]=z[51] + z[49] + z[83] + z[79] + z[62] + z[43];
    z[43]=z[8]*z[43];
    z[49]=5*z[5];
    z[51]=3*z[13];
    z[62]= - z[51] + z[49] + z[6];
    z[62]=z[62]*z[61];
    z[79]=3*z[46];
    z[76]=z[14]*z[76];
    z[62]=z[62] + z[76] - z[79] + z[69] + z[82];
    z[62]=z[8]*z[62];
    z[76]=z[27]*z[9];
    z[82]=2*z[63];
    z[83]=z[82] - z[76];
    z[84]=4*z[6];
    z[85]=z[84]*z[9];
    z[86]=static_cast<T>(1)- z[85];
    z[86]=z[6]*z[86];
    z[87]= - static_cast<T>(1)+ z[66];
    z[87]=z[13]*z[87];
    z[86]=z[86] + z[87];
    z[86]=z[10]*z[86];
    z[51]=z[51] - z[5] - 7*z[6];
    z[51]=z[51]*z[61];
    z[51]=z[51] - n<T>(3,2)*z[42] + z[52] + 2*z[83] + z[86];
    z[51]=z[4]*z[51];
    z[61]=z[6]*z[9];
    z[61]= - static_cast<T>(11)+ 8*z[61];
    z[61]=z[61]*z[63];
    z[83]=n<T>(3,2)*z[40] + z[79] - n<T>(5,2)*z[50] + z[61];
    z[83]=z[10]*z[83];
    z[86]=z[50] + 3*z[63];
    z[64]=z[15] - z[64];
    z[64]=z[12]*z[64];
    z[64]=z[64] + n<T>(5,2)*z[86] + z[48];
    z[64]=z[12]*z[64];
    z[86]=z[14]*z[29]*z[57];
    z[87]=z[44] + 2;
    z[88]=z[12]*z[87];
    z[86]=z[88] + z[86];
    z[86]=z[86]*z[24];
    z[88]=z[10]*z[75];
    z[86]=z[86] - 4*z[88] - z[57];
    z[86]=z[14]*z[86];
    z[88]= - 4*z[68] - z[27];
    z[51]=z[51] + z[62] + z[86] + z[64] + 2*z[88] + z[83];
    z[51]=z[4]*z[51];
    z[62]=z[27]*z[18];
    z[64]= - z[82] - z[62];
    z[64]=z[64]*z[45];
    z[83]=4*z[13];
    z[64]=z[71] + z[64] - z[83] + n<T>(1,2)*z[5] + z[84];
    z[64]=z[4]*z[64];
    z[71]=5*z[6];
    z[86]=z[71] - z[81];
    z[86]=z[86]*z[6];
    z[27]=2*z[68] - z[27];
    z[27]=z[27]*z[23];
    z[88]=4*z[14];
    z[89]= - n<T>(2,3)*z[6] + z[13];
    z[89]=z[89]*z[88];
    z[27]=z[64] + z[89] + z[27] - z[86] - z[80];
    z[27]=z[4]*z[27];
    z[64]=z[23]*z[63];
    z[80]=z[6] - z[64];
    z[80]=z[7]*z[80];
    z[68]=8*z[68];
    z[89]= - z[18]*z[68];
    z[69]=z[80] + z[89] - z[69] + z[86];
    z[69]=z[7]*z[69];
    z[80]= - npow(z[7],2);
    z[80]=z[80] + z[57];
    z[80]=z[18]*z[80];
    z[47]=z[47] + z[80];
    z[47]=z[14]*z[47];
    z[80]=n<T>(8,3)*z[6] - z[7];
    z[80]=z[7]*z[80];
    z[47]=z[47] - 2*z[55] + z[80];
    z[47]=z[14]*z[47];
    z[55]= - z[77] + z[40];
    z[55]=z[10]*z[55];
    z[49]=z[15] + z[49];
    z[49]=z[12]*z[49];
    z[49]=3*z[70] + z[49];
    z[49]=z[49]*z[38];
    z[70]=z[78] + z[56];
    z[77]= - z[10]*z[5];
    z[70]=z[77] + n<T>(3,2)*z[70];
    z[70]=z[3]*z[70];
    z[27]=z[70] + z[27] + z[47] + z[69] + z[55] + z[49];
    z[27]=z[3]*z[27];
    z[47]= - 11*z[15] + z[6];
    z[47]=z[12]*z[47];
    z[47]=z[47] - 11*z[50] - 15*z[63];
    z[47]=z[47]*z[38];
    z[49]= - z[9]*z[59];
    z[49]=static_cast<T>(5)+ z[49];
    z[49]=z[49]*z[59];
    z[49]=n<T>(7,2)*z[15] + z[49];
    z[49]=z[10]*z[49];
    z[55]= - 7*z[15] - 15*z[6];
    z[38]=z[55]*z[38];
    z[55]=4*z[63];
    z[38]=z[38] + z[55] + z[49];
    z[38]=z[7]*z[38];
    z[49]=n<T>(11,2)*z[50] - z[61];
    z[49]=z[10]*z[49];
    z[38]=z[38] + z[47] + z[68] + z[49];
    z[38]=z[7]*z[38];
    z[37]=z[41]*z[37];
    z[47]= - static_cast<T>(1)- z[26];
    z[42]=z[47]*z[42];
    z[37]=z[37] + z[42];
    z[41]=z[41]*z[28];
    z[42]=z[10]*z[17];
    z[47]=2*z[42];
    z[49]=npow(z[17],2);
    z[50]=z[49]*z[10];
    z[61]=z[7]*z[50];
    z[61]= - z[47] + z[61];
    z[61]=z[7]*z[61];
    z[26]=static_cast<T>(1)- 2*z[26];
    z[26]=z[10]*z[26];
    z[26]=z[61] + z[26] + z[41];
    z[26]=z[26]*z[24];
    z[41]=static_cast<T>(1)+ z[47];
    z[41]=z[7]*z[41];
    z[41]=z[41] + 5*z[12] + 2*z[10];
    z[41]=z[7]*z[41];
    z[26]=z[26] + 2*z[37] + z[41];
    z[26]=z[14]*z[26];
    z[37]= - z[10]*z[84];
    z[41]= - 6*z[10] + n<T>(5,2)*z[12];
    z[41]=z[7]*z[41];
    z[37]=z[41] + z[37] + z[65];
    z[37]=z[7]*z[37];
    z[41]=z[10]*z[48];
    z[26]=z[26] + z[41] + z[37];
    z[26]=z[14]*z[26];
    z[22]=z[58] + z[22] + z[27] + z[51] + z[43] + z[38] + z[26];
    z[26]=npow(z[2],2);
    z[22]=z[1]*z[26]*z[22];
    z[27]=z[18]*z[9];
    z[37]=npow(z[9],3);
    z[38]=z[37]*z[10];
    z[41]= - z[27] - 2*z[20] - z[38];
    z[19]=z[41]*z[19];
    z[41]=z[12]*npow(z[18],3);
    z[43]=z[41] - z[29];
    z[51]=2*z[8];
    z[58]= - z[43]*z[51];
    z[61]=z[18] - z[9];
    z[19]=z[19] + z[58] + z[30] + 2*z[21] - z[61];
    z[19]=z[19]*z[34];
    z[34]= - 52*z[20] + z[38];
    z[34]=z[10]*z[34];
    z[58]=z[20] + z[38];
    z[58]=z[3]*z[58];
    z[34]=z[58] - 28*z[18] - 73*z[9] + z[34];
    z[34]=z[3]*z[34];
    z[58]=4*z[16];
    z[65]= - z[21] + z[58] + n<T>(103,3)*z[9];
    z[65]=z[10]*z[65];
    z[68]= - 6*z[30] - z[58] + n<T>(95,3)*z[18];
    z[68]=z[12]*z[68];
    z[69]= - n<T>(67,3)*z[29] + 3*z[41];
    z[69]=z[69]*z[28];
    z[69]=z[69] - z[9] + n<T>(112,3)*z[18];
    z[69]=z[8]*z[69];
    z[19]=z[19] + z[34] + z[69] + z[68] - static_cast<T>(11)+ z[65];
    z[19]=z[11]*z[19];
    z[34]=z[45]*z[12];
    z[65]=n<T>(5,3) - z[34];
    z[65]=z[65]*z[28];
    z[68]= - 14*z[18] + 5*z[30];
    z[68]=z[12]*z[68];
    z[68]=n<T>(16,3) + z[68];
    z[51]=z[68]*z[51];
    z[68]=z[32] + 3*z[21];
    z[68]=z[3]*z[68];
    z[69]= - static_cast<T>(3)- n<T>(23,3)*z[36];
    z[68]=4*z[69] + z[68];
    z[68]=z[3]*z[68];
    z[19]=z[19] + z[68] + z[51] + n<T>(23,3)*z[10] + z[65];
    z[19]=z[11]*z[19];
    z[51]= - n<T>(11,3) + z[34];
    z[51]=z[51]*z[28];
    z[65]=5*z[13];
    z[68]=z[65] + 11*z[5];
    z[51]=z[24] + n<T>(1,3)*z[68] + z[51];
    z[51]=z[8]*z[51];
    z[69]=4*z[12];
    z[68]=z[69] - 38*z[10] - z[68];
    z[70]=static_cast<T>(1)+ 3*z[36];
    z[70]=z[3]*z[70];
    z[68]=z[70] - z[4] - z[24] + n<T>(1,3)*z[68] - z[7];
    z[68]=z[3]*z[68];
    z[70]=z[12]*z[15];
    z[53]= - z[13] + z[53];
    z[53]=z[7]*z[53];
    z[77]= - n<T>(11,3)*z[25] - z[7];
    z[77]=z[14]*z[77];
    z[78]=z[14] - z[10] + z[13];
    z[78]=z[4]*z[78];
    z[19]=z[19] + z[68] + z[78] + z[51] + z[77] + z[53] - z[40] + z[70];
    z[19]=z[11]*z[19];
    z[40]=n<T>(10,3)*z[13];
    z[51]=z[7] + z[60] - z[40] + z[81] - z[6];
    z[51]=z[7]*z[51];
    z[53]= - z[76] - z[62];
    z[53]=z[53]*z[45];
    z[60]=z[62] - z[46];
    z[68]=z[60]*z[45];
    z[68]=z[68] + z[13];
    z[70]= - z[68]*z[35];
    z[53]=z[53] + z[70];
    z[53]=z[8]*z[53];
    z[66]=static_cast<T>(1)+ 2*z[66];
    z[48]= - z[66]*z[48];
    z[70]=z[72] + n<T>(13,3)*z[5];
    z[76]=z[83] + z[70];
    z[76]=z[12]*z[76];
    z[77]=2*z[7];
    z[78]=z[77] + z[13] + z[28];
    z[78]=z[78]*z[24];
    z[73]=z[73]*z[10];
    z[48]=z[53] + z[78] + z[51] + z[76] - z[33] + z[48] - z[73];
    z[48]=z[8]*z[48];
    z[51]=z[45] + z[30];
    z[51]=z[12]*z[51];
    z[53]=z[29] + z[41];
    z[53]=z[12]*z[53];
    z[53]=z[45] + z[53];
    z[53]=z[14]*z[53];
    z[51]=z[51] + z[53];
    z[51]=z[51]*z[24];
    z[53]=z[65] + z[59];
    z[53]=n<T>(5,3)*z[53];
    z[59]=static_cast<T>(6)- z[44];
    z[59]=z[12]*z[59];
    z[51]=z[51] + z[53] + z[59];
    z[51]=z[14]*z[51];
    z[59]=z[66]*z[46];
    z[59]= - z[82] + z[59];
    z[66]=z[85] - 3;
    z[66]=z[66]*z[6];
    z[76]=z[39] - z[66];
    z[76]=z[10]*z[76];
    z[39]= - z[12] - z[83] - z[39] - z[6];
    z[39]=z[12]*z[39];
    z[67]= - z[67]*z[46];
    z[67]=z[67] + z[62];
    z[67]=z[67]*z[45];
    z[67]=z[67] + z[12];
    z[67]=z[4]*z[67];
    z[78]=2*z[13];
    z[80]=z[78] - z[5] + z[6];
    z[80]=z[8]*z[80];
    z[33]=z[67] + z[80] + z[51] + z[39] + z[33] + 2*z[59] + z[76];
    z[33]=z[4]*z[33];
    z[39]= - z[63] + z[46];
    z[39]=2*z[39] - 5*z[62];
    z[39]=z[39]*z[45];
    z[46]= - z[18]*z[60];
    z[46]= - 2*z[75] + z[46];
    z[46]=z[46]*z[45];
    z[46]= - static_cast<T>(1)+ z[46];
    z[46]=z[4]*z[46];
    z[39]=z[46] + z[39] - n<T>(13,3)*z[13] + z[5] + n<T>(5,3)*z[6] - n<T>(8,3)*z[14]
   ;
    z[39]=z[4]*z[39];
    z[46]= - 8*z[5] - z[71];
    z[46]=n<T>(1,3)*z[46] + z[64];
    z[46]=z[7]*z[46];
    z[51]=z[7] + z[12] - z[13] - n<T>(2,3)*z[10];
    z[51]=z[51]*z[24];
    z[59]= - z[12]*z[70];
    z[62]=z[3]*z[10];
    z[39]=z[62] + z[39] + z[51] + z[46] + z[73] + z[59];
    z[39]=z[3]*z[39];
    z[46]= - z[72] + z[66];
    z[46]=z[10]*z[46];
    z[51]= - static_cast<T>(5)+ z[85];
    z[51]=z[10]*z[51];
    z[51]=z[84] + z[51];
    z[51]=z[7]*z[51];
    z[59]=z[72] + z[6];
    z[59]=z[12]*z[59];
    z[46]=z[51] + z[59] + z[55] + z[46];
    z[46]=z[7]*z[46];
    z[51]= - static_cast<T>(1)+ z[42];
    z[51]=z[7]*z[51];
    z[51]=4*z[51] - z[69] - z[53] - z[74];
    z[51]=z[7]*z[51];
    z[53]=z[10]*npow(z[17],3);
    z[55]=z[49] - z[53];
    z[55]=z[7]*z[55];
    z[55]=z[55] - z[45] - z[17] + 4*z[50];
    z[55]=z[7]*z[55];
    z[59]=3*z[17];
    z[58]= - z[59] + z[58];
    z[58]=z[10]*z[58];
    z[62]=z[30] - 2*z[16] + z[18];
    z[62]=z[12]*z[62];
    z[55]=z[55] + z[58] + z[62];
    z[55]=z[14]*z[55];
    z[58]=z[17] - z[50];
    z[58]=z[58]*z[77];
    z[58]=z[58] + static_cast<T>(3)+ z[47];
    z[58]=z[7]*z[58];
    z[28]= - z[10] + z[28];
    z[28]=z[55] + 2*z[28] + z[58];
    z[28]=z[28]*z[24];
    z[28]=z[28] - z[57] + z[51];
    z[28]=z[14]*z[28];
    z[19]=z[19] + z[39] + z[33] + z[48] + z[46] + z[28];
    z[19]=z[19]*z[26];
    z[19]=z[19] + z[22];
    z[19]=z[1]*z[19];
    z[22]= - z[35] - z[42] + z[87];
    z[22]=z[22]*z[54];
    z[28]=z[60]*z[23];
    z[33]=n<T>(4,3)*z[5];
    z[35]=z[44]*z[33];
    z[39]=n<T>(4,3)*z[18];
    z[42]= - z[56]*z[39];
    z[46]=z[79] - z[52];
    z[46]=z[46]*z[18];
    z[40]= - z[40] + z[46];
    z[40]=z[40]*z[45];
    z[48]=z[17]*z[54];
    z[40]=z[48] + n<T>(11,3) + z[40];
    z[40]=z[4]*z[40];
    z[48]= - z[33] - z[13];
    z[22]=z[40] + z[22] + z[42] + z[35] - z[28] + 2*z[48] + n<T>(19,3)*z[10]
   ;
    z[22]=z[3]*z[22];
    z[35]=z[10]*npow(z[9],4);
    z[40]=z[18]*z[20];
    z[35]=z[40] + 2*z[37] + z[35];
    z[35]=z[3]*z[35];
    z[32]=z[16] - z[32];
    z[32]=z[32]*z[21];
    z[37]=z[18] - z[16];
    z[40]=z[37]*z[30];
    z[29]=z[61]*z[29];
    z[42]= - z[12]*npow(z[18],4);
    z[29]=z[29] + z[42];
    z[29]=z[8]*z[29];
    z[42]= - z[16] - z[9];
    z[42]=z[9]*z[42];
    z[48]=z[9] - z[37];
    z[48]=z[18]*z[48];
    z[29]=z[35] + z[29] + z[40] + z[48] + z[42] + z[32];
    z[29]=z[11]*z[29];
    z[32]=n<T>(4,3)*z[44] + n<T>(7,3);
    z[32]=z[37]*z[32];
    z[35]=6*z[16] - n<T>(47,3)*z[9];
    z[35]=z[35]*z[36];
    z[40]=11*z[9];
    z[42]= - z[40] + z[23];
    z[42]=z[18]*z[42];
    z[41]=n<T>(1,3)*z[42] - 5*z[41];
    z[41]=z[8]*z[41];
    z[20]=47*z[20] + 29*z[38];
    z[20]=n<T>(1,3)*z[20] + 6*z[27];
    z[20]=z[3]*z[20];
    z[20]=n<T>(11,3)*z[29] + z[20] + z[41] + z[35] - 6*z[9] + z[32];
    z[27]=2*z[11];
    z[20]=z[20]*z[27];
    z[21]=z[40] + n<T>(31,3)*z[21];
    z[21]=2*z[21] + 5*z[18];
    z[21]=z[3]*z[21];
    z[29]=5*z[16] - 22*z[9];
    z[29]=z[10]*z[29];
    z[32]=z[12]*z[37];
    z[30]= - n<T>(20,3)*z[30] - n<T>(11,3)*z[9] - z[18];
    z[30]=z[8]*z[30];
    z[20]=z[20] + z[21] + z[30] + n<T>(1,3)*z[32] - static_cast<T>(5)+ z[29];
    z[20]=z[11]*z[20];
    z[21]= - z[88]*z[16]*z[25];
    z[25]= - z[13] + 2*z[5];
    z[25]=z[25]*z[23];
    z[29]= - z[44] - static_cast<T>(7)+ z[25];
    z[29]=z[8]*z[29];
    z[25]= - z[25] + static_cast<T>(23)+ 37*z[36];
    z[25]=z[3]*z[25];
    z[21]=z[25] + z[29] + z[21] - 23*z[10] - z[69];
    z[20]=n<T>(1,3)*z[21] + z[20];
    z[20]=z[20]*z[27];
    z[21]= - z[18]*z[17];
    z[21]=z[21] + 2*z[49] - z[53];
    z[21]=z[7]*z[21];
    z[25]=z[44] - 1;
    z[27]=z[18] + z[16];
    z[25]=z[27]*z[25];
    z[29]= - z[16]*z[17];
    z[29]=z[49] + z[29];
    z[29]=z[10]*z[29];
    z[21]=z[21] + z[29] - z[17] + z[25];
    z[21]=z[21]*z[24];
    z[25]=z[59] - z[16];
    z[25]=z[10]*z[25];
    z[27]=z[12]*z[27];
    z[29]= - z[18] + 4*z[17] - 3*z[50];
    z[29]=z[7]*z[29];
    z[21]=z[21] + z[29] + z[27] - static_cast<T>(1)+ z[25];
    z[21]=z[14]*z[21];
    z[25]= - z[13]*z[39];
    z[25]=z[25] + n<T>(5,3) - z[47];
    z[25]=z[7]*z[25];
    z[27]= - z[10] - z[69];
    z[21]=z[21] + n<T>(1,3)*z[27] + z[25];
    z[21]=z[21]*z[24];
    z[25]=z[33] - z[68];
    z[27]=z[5] - z[65];
    z[27]=n<T>(2,3)*z[27] + z[46];
    z[27]=z[27]*z[45];
    z[27]=n<T>(7,3) + z[27];
    z[27]=z[7]*z[27];
    z[29]= - z[18]*z[33];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[12]*z[29];
    z[25]=z[27] + 2*z[25] + z[29];
    z[25]=z[8]*z[25];
    z[27]=z[43]*z[24];
    z[27]=z[27] - z[45] + z[31];
    z[27]=z[14]*z[27];
    z[23]=z[13]*z[23];
    z[23]= - static_cast<T>(7)+ z[23];
    z[23]=z[27] + n<T>(1,3)*z[23] + z[34];
    z[23]=z[23]*z[24];
    z[23]=z[23] + 2*z[68] + z[12];
    z[23]=z[4]*z[23];
    z[24]=z[28] + z[78] - z[10];
    z[24]=z[7]*z[24];
    z[20]=z[20] + z[22] + z[23] + z[25] + z[24] + z[21];
    z[20]=z[20]*z[26];
    z[19]=z[20] + z[19];

    r += z[19]*npow(z[1],2);
 
    return r;
}

template double qqb_2lNLC_r1069(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1069(const std::array<dd_real,31>&);
#endif
