#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r85(const std::array<T,31>& k) {
  T z[70];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[14];
    z[7]=k[4];
    z[8]=k[10];
    z[9]=k[15];
    z[10]=k[5];
    z[11]=k[8];
    z[12]=k[3];
    z[13]=k[9];
    z[14]=k[2];
    z[15]=z[4] - z[5];
    z[16]=5*z[13];
    z[17]=z[16] + z[11];
    z[17]= - z[17]*z[15];
    z[18]= - z[5] + n<T>(3,2)*z[4];
    z[18]=z[6]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[18]=npow(z[6],2);
    z[19]=z[18]*z[10];
    z[20]=n<T>(1,4)*z[5];
    z[21]= - 2*z[3] - z[20];
    z[21]=z[21]*z[19];
    z[17]=n<T>(1,4)*z[17] + z[21];
    z[17]=z[10]*z[17];
    z[21]=npow(z[7],2);
    z[22]=z[7]*z[8];
    z[23]=3*z[22];
    z[24]=static_cast<T>(4)+ z[23];
    z[24]=z[24]*z[21];
    z[25]=n<T>(1,4)*z[10];
    z[26]=z[10]*z[5];
    z[26]=z[26] + 1;
    z[26]=z[26]*z[10];
    z[27]=5*z[7] + z[26];
    z[27]=z[27]*z[25];
    z[24]=z[24] + z[27];
    z[24]=z[2]*z[24];
    z[27]= - static_cast<T>(7)- n<T>(15,2)*z[22];
    z[27]=z[7]*z[27];
    z[28]=z[5]*z[25];
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=z[10]*z[28];
    z[24]=z[24] + z[27] + z[28];
    z[24]=z[2]*z[24];
    z[27]=n<T>(5,4)*z[4];
    z[28]=n<T>(1,4)*z[6];
    z[29]= - z[28] + z[5] - z[27];
    z[29]=z[10]*z[29];
    z[30]=7*z[22];
    z[29]=z[30] + z[29];
    z[24]=n<T>(1,2)*z[29] + z[24];
    z[24]=z[2]*z[24];
    z[29]=n<T>(1,2)*z[10];
    z[31]=z[21]*z[29];
    z[32]=static_cast<T>(1)+ n<T>(1,2)*z[22];
    z[33]=npow(z[7],3);
    z[34]=z[32]*z[33];
    z[34]=z[34] + z[31];
    z[34]=z[2]*z[34];
    z[35]=z[22] + n<T>(3,2);
    z[36]= - z[35]*z[21];
    z[37]= - z[7]*z[29];
    z[36]=z[36] + z[37];
    z[34]=n<T>(3,2)*z[36] + z[34];
    z[34]=z[2]*z[34];
    z[36]=z[23] + n<T>(11,4);
    z[37]=z[7]*z[36];
    z[34]=3*z[34] + z[37] + z[29];
    z[34]=z[2]*z[34];
    z[37]=z[18]*z[3];
    z[38]=n<T>(1,2)*z[37];
    z[39]= - npow(z[10],3)*z[38];
    z[34]=z[39] + z[34];
    z[34]=z[9]*z[34];
    z[39]= - z[3] - z[8];
    z[39]=z[7]*z[39];
    z[39]=n<T>(3,2)*z[39] - 5;
    z[39]=z[4]*z[39];
    z[39]=n<T>(3,2)*z[3] + z[39];
    z[17]=z[34] + z[24] + n<T>(1,4)*z[39] + z[17];
    z[17]=z[9]*z[17];
    z[24]=n<T>(1,4)*z[4];
    z[34]=n<T>(3,2)*z[8];
    z[39]= - n<T>(1,4)*z[19] - n<T>(5,4)*z[6] + z[24] + z[34] + z[5];
    z[40]=3*z[4];
    z[41]=z[40] + z[6];
    z[42]= - z[5] + z[41];
    z[42]=z[42]*z[25];
    z[43]=n<T>(1,2) + z[22];
    z[43]=z[7]*z[43];
    z[44]= - z[5]*z[29];
    z[44]= - static_cast<T>(1)+ z[44];
    z[44]=z[10]*z[44];
    z[43]=3*z[43] + z[44];
    z[44]=n<T>(1,2)*z[2];
    z[43]=z[43]*z[44];
    z[42]=z[43] + z[42] - n<T>(5,4) - z[23];
    z[42]=z[2]*z[42];
    z[39]=n<T>(1,2)*z[39] + z[42];
    z[39]=z[2]*z[39];
    z[42]=3*z[13];
    z[43]=3*z[3];
    z[45]=3*z[8];
    z[46]=z[45] - z[43] + 17*z[11] - z[42];
    z[24]=z[46]*z[24];
    z[46]=5*z[5];
    z[47]= - n<T>(1,4)*z[11] + z[13];
    z[47]=z[47]*z[46];
    z[24]=z[47] + z[24];
    z[47]=npow(z[11],2);
    z[48]= - z[47]*z[15];
    z[49]=n<T>(1,2)*z[4];
    z[50]=z[46] - z[49];
    z[51]= - 13*z[3] - z[50];
    z[51]=z[51]*z[18];
    z[48]=3*z[48] + z[51];
    z[48]=z[48]*z[25];
    z[51]= - z[5] - n<T>(1,8)*z[4];
    z[51]=z[6]*z[51];
    z[17]=z[17] + z[39] + z[48] + n<T>(1,2)*z[24] + z[51];
    z[17]=z[9]*z[17];
    z[24]=z[6]*z[3];
    z[39]=z[5]*z[3];
    z[24]=z[24] + z[39];
    z[48]=npow(z[4],2);
    z[51]=z[48]*z[5];
    z[52]=z[51]*z[10];
    z[53]=n<T>(3,2)*z[5] - z[4];
    z[53]=z[4]*z[53];
    z[53]= - z[52] + z[53] - n<T>(3,2)*z[24];
    z[53]=z[10]*z[53];
    z[54]=n<T>(3,2)*z[6];
    z[53]=z[53] + z[4] + z[54];
    z[53]=z[2]*z[53];
    z[55]=z[22] + 1;
    z[56]=n<T>(1,2)*z[6];
    z[57]= - z[55]*z[56];
    z[57]= - z[3] + z[57];
    z[57]=z[57]*z[56];
    z[58]=z[51] + z[37];
    z[58]=z[58]*z[25];
    z[59]=z[4]*z[5];
    z[53]=n<T>(1,4)*z[53] + z[58] + z[57] + z[39] - z[59];
    z[53]=z[2]*z[53];
    z[57]=z[5] + z[3];
    z[57]=z[57]*z[4];
    z[58]=5*z[39];
    z[60]= - z[58] - z[57];
    z[60]=z[4]*z[60];
    z[60]=z[60] + z[38];
    z[53]=n<T>(1,4)*z[60] + z[53];
    z[53]=z[2]*z[53];
    z[60]= - z[44] + z[49];
    z[18]=z[18]*z[60];
    z[60]= - z[47]*z[40];
    z[18]=z[60] + z[18];
    z[60]=z[7]*z[35];
    z[60]=z[60] + z[29];
    z[61]=z[22] + 2;
    z[62]= - z[61]*z[21];
    z[63]=z[10]*z[7];
    z[62]=z[62] - z[63];
    z[62]=z[2]*z[62];
    z[60]=n<T>(3,2)*z[60] + z[62];
    z[62]=npow(z[2],2);
    z[60]=z[9]*z[60]*z[62];
    z[64]= - n<T>(5,4) - z[22];
    z[64]=z[7]*z[64];
    z[64]=z[64] - z[25];
    z[64]=z[2]*z[64];
    z[65]=z[22] + n<T>(3,4);
    z[64]=z[64] + z[65];
    z[64]=z[2]*z[64];
    z[41]= - n<T>(1,8)*z[41] + z[64];
    z[41]=z[2]*z[41];
    z[64]=z[43] + z[6];
    z[66]= - z[11] + z[64];
    z[66]=z[4]*z[66];
    z[41]=z[60] + n<T>(1,8)*z[66] + z[41];
    z[41]=z[9]*z[41];
    z[18]=n<T>(1,4)*z[18] + z[41];
    z[18]=z[9]*z[18];
    z[41]= - z[64]*z[28];
    z[60]=z[51] + z[38];
    z[60]=z[60]*z[29];
    z[64]=z[49] - z[3] - n<T>(5,4)*z[5];
    z[64]=z[4]*z[64];
    z[41]=z[60] + z[41] + n<T>(5,4)*z[39] + z[64];
    z[41]=z[2]*z[41];
    z[58]=z[58] - z[59];
    z[58]=z[4]*z[58];
    z[58]=z[58] + z[37];
    z[41]=n<T>(1,2)*z[58] + z[41];
    z[41]=z[2]*z[41];
    z[48]=z[48]*z[39];
    z[41]=n<T>(1,2)*z[48] + z[41];
    z[41]=z[2]*z[41];
    z[57]=n<T>(7,2)*z[39] - z[57];
    z[57]=z[4]*z[57];
    z[38]=z[57] + z[38];
    z[38]=z[2]*z[38];
    z[38]=z[48] + z[38];
    z[38]=z[38]*z[62];
    z[57]=npow(z[2],3);
    z[58]=z[1]*z[57]*z[48];
    z[38]=z[38] + z[58];
    z[58]=n<T>(1,2)*z[1];
    z[38]=z[38]*z[58];
    z[60]=z[7]*z[32];
    z[60]=z[60] + z[29];
    z[57]=z[60]*z[57]*npow(z[9],3);
    z[38]=z[38] + z[41] + z[57];
    z[38]=z[38]*z[58];
    z[41]=npow(z[5],2);
    z[57]=z[41]*z[4];
    z[58]=z[6]*z[57];
    z[48]=z[48] + z[58];
    z[18]=z[38] + z[18] + n<T>(1,4)*z[48] + z[53];
    z[18]=z[1]*z[18];
    z[38]=z[45]*z[3];
    z[39]=z[38] - n<T>(1,2)*z[39];
    z[48]=n<T>(3,4)*z[11] - z[50];
    z[48]=z[4]*z[48];
    z[50]= - n<T>(1,2)*z[3] + 5*z[8];
    z[53]= - static_cast<T>(3)- n<T>(5,2)*z[22];
    z[53]=z[6]*z[53];
    z[50]=n<T>(1,2)*z[50] + z[53];
    z[50]=z[6]*z[50];
    z[41]=3*z[47] - z[41];
    z[41]=n<T>(1,2)*z[41] - z[59];
    z[41]=z[4]*z[41];
    z[41]=z[41] + n<T>(1,4)*z[37];
    z[41]=z[10]*z[41];
    z[39]=z[41] + z[50] + n<T>(1,2)*z[39] + z[48];
    z[41]=z[29]*z[51];
    z[41]=z[41] + z[59] - n<T>(7,8)*z[24];
    z[41]=z[10]*z[41];
    z[48]=z[12]*z[38];
    z[50]= - static_cast<T>(1)+ z[22];
    z[50]=z[6]*z[50];
    z[48]=z[48] + z[50];
    z[50]=z[5] + z[4];
    z[50]=z[50]*z[49];
    z[50]=z[50] - z[52];
    z[51]=npow(z[10],2);
    z[50]=z[50]*z[51];
    z[50]= - n<T>(1,2) + z[50];
    z[50]=z[50]*z[44];
    z[41]=z[50] + n<T>(1,4)*z[48] + z[41];
    z[41]=z[2]*z[41];
    z[39]=n<T>(1,2)*z[39] + z[41];
    z[39]=z[2]*z[39];
    z[41]= - 11*z[8] + z[5];
    z[41]=z[5]*z[41];
    z[41]= - z[38] + z[41];
    z[48]=n<T>(1,2)*z[5];
    z[50]=z[48] - z[13] - n<T>(1,2)*z[8];
    z[40]=z[50]*z[40];
    z[40]=n<T>(1,2)*z[41] + z[40];
    z[41]=z[8]*z[46];
    z[46]=z[4]*z[45];
    z[38]=z[46] + z[38] + z[41];
    z[41]=n<T>(1,4)*z[7];
    z[38]=z[38]*z[41];
    z[46]=2*z[4];
    z[38]=z[38] + z[46] - n<T>(7,4)*z[3] - z[5];
    z[38]=z[6]*z[38];
    z[38]=n<T>(1,2)*z[40] + z[38];
    z[38]=z[6]*z[38];
    z[40]=z[3]*z[11];
    z[50]=z[8]*z[13];
    z[50]=z[40] + n<T>(15,2)*z[50];
    z[50]=z[50]*z[48];
    z[52]=7*z[3];
    z[53]= - z[52] + 9*z[8];
    z[20]=z[53]*z[20];
    z[20]=z[20] + z[59];
    z[20]=z[4]*z[20];
    z[20]=z[50] + z[20];
    z[50]=npow(z[13],2);
    z[53]=z[50]*z[4];
    z[19]=z[53]*z[19];
    z[58]=z[4]*z[13];
    z[60]=z[6]*z[58];
    z[57]=z[57] + 7*z[60];
    z[57]=z[57]*z[56];
    z[57]=z[57] + z[19];
    z[57]=z[57]*z[29];
    z[17]=z[18] + z[17] + z[39] + z[57] + n<T>(1,2)*z[20] + z[38];
    z[17]=z[1]*z[17];
    z[18]=z[10]*z[4];
    z[18]=z[18] + 1;
    z[20]=n<T>(3,2)*z[47];
    z[18]=z[20]*z[18];
    z[38]=n<T>(3,2)*z[11];
    z[39]=z[38] + z[4];
    z[39]=z[4]*z[39];
    z[18]=z[39] + z[18] - z[24];
    z[18]=z[10]*z[18];
    z[24]=z[6] + z[5];
    z[39]=z[4] + z[3];
    z[18]=z[18] - z[45] - z[38] + z[39] - n<T>(5,2)*z[24];
    z[24]= - z[21]*z[34];
    z[47]=z[48] + z[4];
    z[47]=z[10]*z[47];
    z[47]=static_cast<T>(1)+ z[47];
    z[47]=z[51]*z[4]*z[47];
    z[24]=z[24] + z[47];
    z[24]=z[24]*z[44];
    z[47]=z[51]*z[59];
    z[51]=2*z[22];
    z[57]=z[8]*z[12];
    z[24]=z[24] + z[47] - n<T>(3,4)*z[57] + z[51];
    z[24]=z[2]*z[24];
    z[18]=n<T>(1,2)*z[18] + z[24];
    z[18]=z[2]*z[18];
    z[24]=z[35]*z[33];
    z[24]=z[24] + z[31];
    z[47]=npow(z[7],4);
    z[60]= - z[61]*z[47];
    z[61]= - z[10]*z[33];
    z[60]=z[60] + z[61];
    z[60]=z[2]*z[60];
    z[24]=n<T>(9,2)*z[24] + z[60];
    z[24]=z[2]*z[24];
    z[60]= - n<T>(11,2) - 6*z[22];
    z[60]=z[60]*z[21];
    z[24]=z[24] + z[60] - z[63];
    z[24]=z[2]*z[24];
    z[60]=n<T>(1,2)*z[7];
    z[61]=5*z[22];
    z[62]=n<T>(3,2) + z[61];
    z[62]=z[62]*z[60];
    z[64]=z[56] + z[3];
    z[66]=z[64]*z[6];
    z[67]=z[37]*z[10];
    z[68]=z[66] - z[67];
    z[68]=z[10]*z[68];
    z[28]= - z[28] + z[68];
    z[28]=z[10]*z[28];
    z[28]= - n<T>(1,4) + z[28];
    z[28]=z[10]*z[28];
    z[24]=z[24] + z[62] + z[28];
    z[24]=z[9]*z[24];
    z[28]=n<T>(25,2) + 12*z[22];
    z[28]=z[28]*z[21];
    z[23]= - n<T>(17,4) - z[23];
    z[23]=z[23]*z[33];
    z[62]=z[21]*z[10];
    z[23]=z[23] - n<T>(3,2)*z[62];
    z[23]=z[2]*z[23];
    z[23]=z[23] + z[28] + 3*z[63];
    z[23]=z[2]*z[23];
    z[28]=z[10]*z[6];
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=z[28]*z[25];
    z[63]=13*z[22];
    z[68]= - static_cast<T>(5)- z[63];
    z[68]=z[7]*z[68];
    z[23]=z[23] + z[68] + z[28];
    z[23]=z[2]*z[23];
    z[28]=3*z[6];
    z[68]=z[3] + n<T>(3,4)*z[6];
    z[68]=z[68]*z[28];
    z[68]=z[68] - 4*z[67];
    z[68]=z[10]*z[68];
    z[68]=z[56] + z[68];
    z[68]=z[10]*z[68];
    z[69]= - z[43] + 29*z[8];
    z[69]=z[7]*z[69];
    z[23]=z[24] + z[23] + z[68] - static_cast<T>(2)+ n<T>(1,8)*z[69];
    z[23]=z[9]*z[23];
    z[24]=z[42] + n<T>(7,2)*z[11];
    z[24]=z[24]*z[15];
    z[42]=n<T>(15,4)*z[6] + z[43] - z[49];
    z[42]=z[6]*z[42];
    z[24]= - n<T>(13,2)*z[67] + n<T>(1,2)*z[24] + z[42];
    z[24]=z[10]*z[24];
    z[42]=3*z[21];
    z[42]= - z[65]*z[42];
    z[26]= - z[7] - z[26];
    z[25]=z[26]*z[25];
    z[25]=z[42] + z[25];
    z[25]=z[2]*z[25];
    z[26]=static_cast<T>(17)+ 39*z[22];
    z[26]=z[26]*z[41];
    z[41]= - z[5] + z[49];
    z[41]=z[10]*z[41];
    z[41]= - n<T>(3,4) + z[41];
    z[41]=z[10]*z[41];
    z[25]=z[25] + z[26] + z[41];
    z[25]=z[2]*z[25];
    z[15]=n<T>(5,2)*z[15] + z[6];
    z[15]=z[15]*z[29];
    z[15]=z[25] + z[15] + n<T>(3,2) - z[30];
    z[15]=z[2]*z[15];
    z[25]=z[8] - z[3];
    z[25]=z[25]*z[7];
    z[26]=n<T>(9,2)*z[25] + 5;
    z[26]=z[4]*z[26];
    z[30]=n<T>(5,2)*z[8] - z[5];
    z[26]=3*z[30] + z[26];
    z[15]=z[23] + z[15] + z[24] + n<T>(1,4)*z[26] - z[6];
    z[15]=z[9]*z[15];
    z[16]=z[16] - z[52];
    z[16]=n<T>(1,2)*z[16] + 4*z[4];
    z[16]=z[6]*z[16];
    z[16]=z[16] - z[50] - n<T>(5,2)*z[58];
    z[16]=z[6]*z[16];
    z[23]=z[50] + n<T>(7,2)*z[58];
    z[24]=z[23]*z[6];
    z[26]= - z[50]*z[49];
    z[26]=z[26] + z[24];
    z[26]=z[6]*z[26];
    z[26]=z[26] + z[19];
    z[26]=z[10]*z[26];
    z[20]= - z[20] - z[40];
    z[20]=z[20]*z[48];
    z[16]=z[26] + z[20] + z[16];
    z[16]=z[10]*z[16];
    z[20]=n<T>(9,4)*z[25] + 1;
    z[20]=z[59]*z[20];
    z[25]= - z[13]*z[12];
    z[25]= - n<T>(7,2) + z[25];
    z[25]=z[13]*z[25];
    z[25]=n<T>(9,4)*z[8] + z[38] + z[25];
    z[25]=z[5]*z[25];
    z[20]=z[50] + z[25] + z[20];
    z[25]=z[39]*z[60];
    z[25]=z[25] + 1;
    z[26]=z[25]*z[28];
    z[30]=z[48] - n<T>(9,2)*z[13] - z[3];
    z[26]=z[26] + n<T>(1,2)*z[30] - z[46];
    z[26]=z[6]*z[26];
    z[15]=z[17] + z[15] + z[18] + z[16] + n<T>(1,2)*z[20] + z[26];
    z[15]=z[1]*z[15];
    z[16]=z[33]*z[29];
    z[17]=z[47]*z[35];
    z[16]=z[16] + z[17];
    z[17]=z[47]*z[29];
    z[18]=z[32]*npow(z[7],5);
    z[17]=z[18] + z[17];
    z[17]=z[2]*z[17];
    z[17]= - 3*z[16] + z[17];
    z[17]=z[17]*z[44];
    z[18]=z[36]*z[33];
    z[17]=z[17] + z[18] + z[31];
    z[17]=z[2]*z[17];
    z[18]=z[8]*z[14];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=n<T>(3,2)*z[18] - z[61];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(1,2)*z[14] + z[18];
    z[18]=z[18]*z[60];
    z[20]= - z[29]*z[37];
    z[20]=z[66] + z[20];
    z[20]=z[10]*z[20];
    z[26]=z[6]*z[14];
    z[30]= - n<T>(3,2) - z[26];
    z[30]=z[6]*z[30];
    z[30]= - z[3] + z[30];
    z[20]=n<T>(1,2)*z[30] + z[20];
    z[20]=z[10]*z[20];
    z[30]=3*z[26];
    z[32]=z[30] + 1;
    z[20]=n<T>(1,4)*z[32] + z[20];
    z[20]=z[10]*z[20];
    z[35]= - z[14] + z[7];
    z[20]=n<T>(1,4)*z[35] + z[20];
    z[20]=z[10]*z[20];
    z[17]=z[17] + z[18] + z[20];
    z[17]=z[9]*z[17];
    z[16]=z[2]*z[16];
    z[18]=11*z[22];
    z[20]= - n<T>(25,2) - z[18];
    z[20]=z[20]*z[33];
    z[20]=z[20] - 3*z[62];
    z[16]=n<T>(1,2)*z[20] + z[16];
    z[16]=z[2]*z[16];
    z[20]=n<T>(1,2)*z[21];
    z[35]=n<T>(23,2) + 19*z[22];
    z[35]=z[35]*z[20];
    z[16]=z[35] + z[16];
    z[16]=z[2]*z[16];
    z[35]=z[14]*z[45];
    z[35]= - z[63] + n<T>(1,2) + z[35];
    z[35]=z[7]*z[35];
    z[35]= - n<T>(5,2)*z[14] + z[35];
    z[28]=z[64]*z[28];
    z[28]=z[28] - 2*z[67];
    z[28]=z[10]*z[28];
    z[36]= - n<T>(5,4) - z[26];
    z[36]=z[6]*z[36];
    z[28]=z[28] - z[3] + z[36];
    z[28]=z[10]*z[28];
    z[26]=2*z[26] + z[28];
    z[26]=z[10]*z[26];
    z[16]=z[17] + z[16] + n<T>(1,2)*z[35] + z[26];
    z[16]=z[9]*z[16];
    z[17]= - z[32]*z[56];
    z[17]= - z[3] + z[17];
    z[26]=z[43] + n<T>(7,4)*z[6];
    z[26]=z[6]*z[26];
    z[26]=z[26] - n<T>(13,4)*z[67];
    z[26]=z[10]*z[26];
    z[17]=n<T>(1,2)*z[17] + z[26];
    z[17]=z[10]*z[17];
    z[26]=z[55]*z[33];
    z[26]=3*z[26] + z[31];
    z[26]=z[2]*z[26];
    z[28]= - static_cast<T>(17)- 27*z[22];
    z[20]=z[28]*z[20];
    z[20]=z[20] + z[26];
    z[20]=z[2]*z[20];
    z[26]=n<T>(7,2) + 17*z[22];
    z[26]=z[7]*z[26];
    z[20]=z[20] + z[26] + z[29];
    z[20]=z[20]*z[44];
    z[26]=z[30] - static_cast<T>(1)- n<T>(13,2)*z[22];
    z[16]=z[16] + z[20] + n<T>(1,2)*z[26] + z[17];
    z[16]=z[9]*z[16];
    z[17]=z[14] - z[12];
    z[20]=z[17]*z[50];
    z[17]=z[13]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[34];
    z[26]=z[13]*npow(z[12],2);
    z[28]=z[12] + z[26];
    z[28]=z[28]*z[34];
    z[26]= - n<T>(3,2)*z[12] + z[26];
    z[26]=z[13]*z[26];
    z[26]=z[28] - n<T>(3,2) + z[26];
    z[26]=z[5]*z[26];
    z[28]= - z[13]*z[30];
    z[17]=z[28] + z[26] + z[20] + z[17];
    z[20]=z[13]*z[14];
    z[26]=n<T>(5,2) + z[20];
    z[26]=z[13]*z[26];
    z[26]=z[26] - n<T>(7,2)*z[3];
    z[26]=n<T>(1,2)*z[26] + z[46];
    z[26]=z[6]*z[26];
    z[23]= - n<T>(1,2)*z[23] + z[26];
    z[23]=z[6]*z[23];
    z[24]= - z[53] + z[24];
    z[24]=z[6]*z[24];
    z[19]=z[24] + z[19];
    z[19]=z[19]*z[29];
    z[19]=z[23] + z[19];
    z[19]=z[10]*z[19];
    z[23]=n<T>(1,2)*z[20] + z[25];
    z[23]=z[23]*z[54];
    z[20]= - n<T>(5,4) - z[20];
    z[20]=z[13]*z[20];
    z[20]=z[23] - z[27] + z[20] + n<T>(1,4)*z[3];
    z[20]=z[6]*z[20];
    z[23]= - z[12]*z[50]*z[48];
    z[19]=z[19] + z[23] + z[20];
    z[19]=z[10]*z[19];
    z[18]= - z[18] - static_cast<T>(1)+ z[57];
    z[18]=z[18]*z[60];
    z[20]=n<T>(3,2)*z[22] + static_cast<T>(1)- n<T>(1,2)*z[57];
    z[20]=z[2]*z[20]*z[21];
    z[18]=z[18] + z[20];
    z[18]=z[18]*z[44];
    z[18]=z[18] + n<T>(1,4) + z[51];
    z[18]=z[2]*z[18];
    z[15]=z[15] + z[16] + z[18] + n<T>(1,2)*z[17] + z[19];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r85(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r85(const std::array<dd_real,31>&);
#endif
