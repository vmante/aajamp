#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r726(const std::array<T,31>& k) {
  T z[147];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[8];
    z[9]=k[9];
    z[10]=k[15];
    z[11]=k[2];
    z[12]=k[5];
    z[13]=k[16];
    z[14]=k[3];
    z[15]=k[4];
    z[16]=npow(z[1],2);
    z[17]=4*z[16];
    z[18]=z[12]*z[1];
    z[19]=3*z[18];
    z[20]=z[17] + z[19];
    z[20]=z[20]*z[12];
    z[21]=npow(z[1],3);
    z[22]= - z[21] - z[20];
    z[23]=npow(z[12],2);
    z[22]=z[22]*z[23];
    z[24]=2*z[16];
    z[25]=z[24] + z[18];
    z[25]=z[25]*z[12];
    z[26]=z[25] + z[21];
    z[27]=npow(z[12],3);
    z[28]=z[27]*z[5];
    z[29]=z[26]*z[28];
    z[22]=z[22] + z[29];
    z[22]=z[5]*z[22];
    z[29]=z[21]*z[9];
    z[30]=z[29]*z[23];
    z[31]=2*z[1];
    z[32]=z[27]*z[31];
    z[22]=z[30] + z[32] + z[22];
    z[32]=3*z[10];
    z[22]=z[22]*z[32];
    z[33]=z[23]*z[5];
    z[34]=z[33]*z[26];
    z[35]=n<T>(93,2)*z[16];
    z[36]= - z[35] - 32*z[18];
    z[36]=z[12]*z[36];
    z[36]= - n<T>(29,2)*z[21] + z[36];
    z[36]=z[12]*z[36];
    z[36]=z[36] + 14*z[34];
    z[36]=z[5]*z[36];
    z[37]=2*z[30];
    z[38]=z[16]*z[12];
    z[39]=6*z[38];
    z[40]=n<T>(23,2)*z[21] - z[39];
    z[40]=z[12]*z[40];
    z[40]=z[40] + z[37];
    z[40]=z[9]*z[40];
    z[41]=npow(z[1],4);
    z[42]=z[41]*z[6];
    z[42]=z[42] + z[21];
    z[43]=n<T>(5,2)*z[16];
    z[44]=z[43] + 19*z[18];
    z[44]=z[12]*z[44];
    z[22]=z[22] + z[40] + z[36] + z[44] - n<T>(3,8)*z[42];
    z[22]=z[10]*z[22];
    z[36]=z[5]*z[12];
    z[40]=z[36]*z[26];
    z[44]= - 113*z[16] - n<T>(161,2)*z[18];
    z[45]=n<T>(1,2)*z[12];
    z[44]=z[44]*z[45];
    z[46]=19*z[21];
    z[44]=26*z[40] - z[46] + z[44];
    z[44]=z[5]*z[44];
    z[47]=z[29]*z[12];
    z[48]=n<T>(9,2)*z[47] + n<T>(45,4)*z[21] - 14*z[38];
    z[48]=z[9]*z[48];
    z[49]=3*z[16];
    z[50]=z[49] + n<T>(23,2)*z[18];
    z[51]=z[21]*z[6];
    z[22]=z[22] - n<T>(17,8)*z[51] + z[48] + n<T>(3,2)*z[50] + z[44];
    z[22]=z[10]*z[22];
    z[44]=20*z[16];
    z[48]=7*z[18];
    z[50]= - z[44] - z[48];
    z[50]=z[12]*z[50];
    z[46]= - z[46] + z[50];
    z[46]=z[12]*z[46];
    z[46]= - 6*z[41] + z[46];
    z[46]=z[12]*z[46];
    z[50]=z[49] + z[18];
    z[52]=z[50]*z[12];
    z[53]=3*z[21];
    z[52]=z[52] + z[53];
    z[52]=z[52]*z[12];
    z[52]=z[52] + z[41];
    z[54]=z[23]*z[7];
    z[55]=z[52]*z[54];
    z[46]=z[46] - 3*z[55];
    z[46]=z[7]*z[46];
    z[55]=2*z[41];
    z[56]=z[21]*z[12];
    z[57]=z[55] + z[56];
    z[57]=z[57]*z[12];
    z[58]=npow(z[1],5);
    z[57]=z[57] + z[58];
    z[59]=z[9]*z[57];
    z[46]=z[46] - 4*z[52] + z[59];
    z[46]=z[8]*z[46];
    z[59]=2*z[38];
    z[60]= - n<T>(15,2)*z[21] - z[59];
    z[60]=z[12]*z[60];
    z[60]= - n<T>(11,2)*z[41] + z[60];
    z[61]=2*z[9];
    z[62]=z[57]*z[61];
    z[60]=3*z[60] + z[62];
    z[60]=z[9]*z[60];
    z[62]=z[26]*z[54];
    z[63]=n<T>(89,2)*z[16] + 29*z[18];
    z[63]=z[12]*z[63];
    z[63]=n<T>(31,2)*z[21] + z[63];
    z[63]=z[12]*z[63];
    z[63]=z[63] + 14*z[62];
    z[63]=z[7]*z[63];
    z[64]=n<T>(67,2)*z[16] + 18*z[18];
    z[64]=z[12]*z[64];
    z[46]=z[46] + z[63] + z[60] + n<T>(119,8)*z[21] + z[64];
    z[46]=z[8]*z[46];
    z[60]=z[56] + z[41];
    z[63]=z[9]*z[60];
    z[63]= - n<T>(9,2)*z[63] + n<T>(107,4)*z[21] + 16*z[38];
    z[63]=z[9]*z[63];
    z[64]=z[18] + z[16];
    z[65]=z[64]*z[54];
    z[66]= - z[16] - n<T>(7,4)*z[18];
    z[66]=z[12]*z[66];
    z[67]=n<T>(11,4)*z[21];
    z[66]= - 26*z[65] - z[67] + 21*z[66];
    z[66]=z[7]*z[66];
    z[68]= - 45*z[16] - 67*z[18];
    z[46]=z[46] + z[66] + n<T>(1,4)*z[68] + z[63];
    z[46]=z[8]*z[46];
    z[63]=z[41]*z[5];
    z[66]=n<T>(15,8)*z[21] - z[63];
    z[66]=z[5]*z[66];
    z[68]=npow(z[9],2);
    z[69]=z[41]*z[68];
    z[70]=5*z[41];
    z[71]= - z[8]*z[70];
    z[71]=z[21] + z[71];
    z[71]=z[8]*z[71];
    z[72]=n<T>(1,2)*z[9];
    z[73]=z[41]*z[72];
    z[73]= - z[21] + z[73];
    z[73]=z[3]*z[73];
    z[66]=z[73] + n<T>(1,8)*z[71] + z[69] - z[17] + z[66];
    z[66]=z[3]*z[66];
    z[69]=z[21]*z[7];
    z[71]=z[21]*z[8];
    z[73]=z[21]*z[5];
    z[74]=z[71] + z[73] - z[69];
    z[75]=n<T>(1,2)*z[8];
    z[76]=z[41]*z[75];
    z[77]=n<T>(1,2)*z[63];
    z[76]=z[76] - z[21] + z[77];
    z[76]=z[3]*z[76];
    z[78]=n<T>(1,2)*z[7];
    z[79]= - z[41]*z[78];
    z[79]= - z[21] + z[79];
    z[79]=z[6]*z[79];
    z[42]=z[10]*z[42];
    z[42]=n<T>(3,2)*z[42] + z[79] + n<T>(1,2)*z[74] + z[76];
    z[74]=n<T>(1,2)*z[4];
    z[42]=z[42]*z[74];
    z[76]=5*z[18];
    z[79]=n<T>(39,4)*z[16] + z[76];
    z[80]=3*z[12];
    z[79]=z[79]*z[80];
    z[79]=n<T>(53,4)*z[21] + z[79];
    z[79]=z[5]*z[79];
    z[81]=12*z[16];
    z[79]=z[79] - z[81] - n<T>(47,4)*z[18];
    z[79]=z[5]*z[79];
    z[82]= - z[49] + 41*z[18];
    z[83]=n<T>(1,4)*z[16];
    z[84]=z[83] + z[76];
    z[84]=z[84]*z[80];
    z[84]= - z[21] + z[84];
    z[84]=z[7]*z[84];
    z[82]=n<T>(1,4)*z[82] + z[84];
    z[82]=z[7]*z[82];
    z[84]=4*z[9];
    z[85]=z[84]*z[21];
    z[86]=z[41]*z[7];
    z[87]=n<T>(9,8)*z[21] - z[86];
    z[87]=z[7]*z[87];
    z[88]=n<T>(9,2)*z[16];
    z[87]=z[87] - z[88] + z[85];
    z[87]=z[6]*z[87];
    z[89]= - 24*z[16] + 7*z[29];
    z[89]=z[9]*z[89];
    z[90]=6*z[1];
    z[22]=z[42] + z[22] + z[87] + z[66] + z[46] + z[82] + z[89] + z[90]
    + z[79];
    z[22]=z[4]*z[22];
    z[42]=n<T>(9,2)*z[21];
    z[46]= - z[42] + z[59];
    z[46]=z[46]*z[80];
    z[46]=z[46] - z[37];
    z[46]=z[9]*z[46];
    z[66]=z[80]*z[16];
    z[79]=4*z[21];
    z[82]= - z[79] + z[66];
    z[87]=2*z[12];
    z[82]=z[82]*z[87];
    z[82]=z[82] - 3*z[30];
    z[82]=z[10]*z[82];
    z[89]=4*z[18];
    z[91]=35*z[16] - z[89];
    z[91]=z[12]*z[91];
    z[46]=z[82] + z[46] - n<T>(47,2)*z[21] + z[91];
    z[46]=z[10]*z[46];
    z[82]=z[29]*z[45];
    z[91]= - z[82] - z[67] + 10*z[38];
    z[91]=z[9]*z[91];
    z[92]=z[53] - z[38];
    z[93]=n<T>(1,4)*z[7];
    z[92]=z[92]*z[93];
    z[94]=11*z[18];
    z[95]=z[21]*z[3];
    z[46]=z[46] + n<T>(1,8)*z[95] + z[92] + z[91] + n<T>(111,4)*z[16] - z[94];
    z[46]=z[10]*z[46];
    z[91]= - z[21]*z[80];
    z[91]= - 7*z[41] + z[91];
    z[92]=n<T>(1,4)*z[5];
    z[91]=z[91]*z[92];
    z[91]=z[91] - n<T>(31,4)*z[21] - z[59];
    z[91]=z[5]*z[91];
    z[96]= - z[41] + n<T>(3,4)*z[56];
    z[96]=z[7]*z[96];
    z[97]=n<T>(23,4)*z[21];
    z[59]=z[96] - z[97] + z[59];
    z[59]=z[7]*z[59];
    z[96]=z[58]*z[7];
    z[98]=z[96] - z[41];
    z[98]=z[98]*z[6];
    z[86]=z[98] + z[86];
    z[98]=z[58]*z[5];
    z[99]=z[98] - z[41];
    z[100]=z[99]*z[3];
    z[100]=z[100] + z[63];
    z[101]=z[100] - z[86];
    z[102]=n<T>(1,4)*z[4];
    z[101]=z[101]*z[102];
    z[103]=n<T>(5,8)*z[41] - z[98];
    z[103]=z[5]*z[103];
    z[103]= - n<T>(13,4)*z[21] + z[103];
    z[103]=z[3]*z[103];
    z[96]=n<T>(3,8)*z[41] - z[96];
    z[96]=z[7]*z[96];
    z[96]= - z[97] + z[96];
    z[96]=z[6]*z[96];
    z[97]=13*z[16];
    z[59]=z[101] + z[96] + z[103] + z[59] + z[97] + z[91];
    z[59]=z[4]*z[59];
    z[91]=z[38] + z[21];
    z[96]=z[56] - z[41];
    z[101]= - z[7]*z[96];
    z[101]=z[101] - z[91];
    z[101]=z[6]*z[101];
    z[86]= - z[100] - z[86];
    z[86]=z[4]*z[86];
    z[100]=z[63] - z[21];
    z[103]=n<T>(3,2)*z[8];
    z[104]=z[3] + z[103];
    z[100]=z[100]*z[104];
    z[104]=z[10]*z[45]*z[69];
    z[105]=n<T>(1,2)*z[73];
    z[86]=z[86] + z[104] + z[101] + z[49] - z[105] + z[100];
    z[100]=n<T>(1,2)*z[2];
    z[86]=z[86]*z[100];
    z[101]=n<T>(25,2)*z[21];
    z[39]=z[101] + z[39];
    z[39]=z[12]*z[39];
    z[37]=z[39] - z[37];
    z[37]=z[9]*z[37];
    z[39]=8*z[16];
    z[104]=z[39] + z[19];
    z[104]=z[12]*z[104];
    z[53]=z[53] + z[104];
    z[53]=z[12]*z[53];
    z[53]=z[53] - z[30];
    z[53]=z[8]*z[53];
    z[104]= - z[17] + z[18];
    z[104]=z[12]*z[104];
    z[106]=n<T>(1,2)*z[21];
    z[37]=z[53] + z[37] - z[77] + z[106] + z[104];
    z[37]=z[8]*z[37];
    z[53]=23*z[16];
    z[104]=15*z[73] - z[53] - z[18];
    z[107]=z[82] + n<T>(1,4)*z[21] - 12*z[38];
    z[107]=z[9]*z[107];
    z[37]=z[37] + n<T>(1,4)*z[104] + z[107];
    z[37]=z[8]*z[37];
    z[104]= - n<T>(5,4)*z[21] - z[38];
    z[104]=z[12]*z[104];
    z[107]=z[106]*z[54];
    z[104]=z[104] + z[107];
    z[104]=z[7]*z[104];
    z[107]=z[12]*z[91];
    z[108]=n<T>(1,2)*z[18];
    z[109]=z[108] + z[16];
    z[110]=z[109]*z[12];
    z[110]=z[110] + z[106];
    z[111]=z[110]*z[8];
    z[112]=z[23]*z[111];
    z[107]=z[107] + z[112];
    z[107]=z[8]*z[107];
    z[104]=z[107] + n<T>(3,4)*z[38] + z[104];
    z[104]=z[6]*z[104];
    z[107]=z[45]*z[21];
    z[107]=z[107] + z[41];
    z[112]=z[7]*z[107];
    z[113]=4*z[38];
    z[114]=n<T>(19,4)*z[21];
    z[112]=z[112] - z[114] - z[113];
    z[112]=z[7]*z[112];
    z[115]=3*z[8];
    z[116]=z[26]*z[23]*z[115];
    z[117]=18*z[16] + n<T>(11,2)*z[18];
    z[117]=z[12]*z[117];
    z[101]=z[101] + z[117];
    z[101]=z[12]*z[101];
    z[101]=z[101] + z[116];
    z[101]=z[8]*z[101];
    z[116]= - 75*z[16] - 53*z[18];
    z[116]=z[12]*z[116];
    z[117]=n<T>(7,2)*z[21];
    z[116]=z[117] + z[116];
    z[101]=n<T>(1,4)*z[116] + z[101];
    z[101]=z[8]*z[101];
    z[116]=25*z[18];
    z[118]=n<T>(33,2)*z[16] + z[116];
    z[101]=z[104] + z[101] + n<T>(1,2)*z[118] + z[112];
    z[101]=z[6]*z[101];
    z[104]= - z[80]*z[73];
    z[104]= - 11*z[21] + z[104];
    z[92]=z[104]*z[92];
    z[104]= - z[9]*z[45]*z[73];
    z[92]=z[104] + z[39] + z[92];
    z[92]=z[9]*z[92];
    z[82]=z[82] - z[114] + z[38];
    z[82]=z[9]*z[82];
    z[47]=z[7]*z[47];
    z[47]=n<T>(5,4)*z[47] + n<T>(19,4)*z[16] + z[82];
    z[47]=z[7]*z[47];
    z[82]= - z[79] + z[63];
    z[82]=z[5]*z[82];
    z[104]=n<T>(11,2)*z[16];
    z[82]=z[104] + z[82];
    z[82]=z[3]*z[82];
    z[112]=z[16]*z[5];
    z[118]= - 23*z[1] + 13*z[112];
    z[37]=z[86] + z[59] + z[46] + z[101] + z[82] + z[37] + z[47] + n<T>(1,2)
   *z[118] + z[92];
    z[37]=z[2]*z[37];
    z[46]=z[73] - z[71];
    z[46]=z[3]*z[46];
    z[47]=z[51] + z[16];
    z[59]= - z[47]*z[32];
    z[82]=z[69]*z[6];
    z[86]=z[16]*z[7];
    z[92]= - z[8]*z[16];
    z[46]=z[59] - z[82] + z[46] + z[92] + z[112] - z[86];
    z[46]=z[46]*z[102];
    z[59]=3*z[73];
    z[92]=z[83] - z[59];
    z[92]=z[5]*z[92];
    z[101]=z[49] - z[71];
    z[102]=n<T>(1,4)*z[8];
    z[118]= - z[101]*z[102];
    z[92]=z[92] + z[118];
    z[118]=2*z[3];
    z[119]= - z[73]*z[118];
    z[92]=n<T>(5,2)*z[92] + z[119];
    z[92]=z[3]*z[92];
    z[119]=n<T>(29,2)*z[16];
    z[120]= - z[119] - z[48];
    z[120]=z[5]*z[120];
    z[121]=n<T>(5,2)*z[1];
    z[120]=z[121] + z[120];
    z[120]=z[5]*z[120];
    z[122]=n<T>(15,2)*z[16];
    z[123]= - z[122] + z[48];
    z[123]=z[7]*z[123];
    z[123]=z[121] + z[123];
    z[123]=z[7]*z[123];
    z[124]=z[83] - z[18];
    z[125]=npow(z[8],2);
    z[126]=n<T>(1,2)*z[125];
    z[124]=z[124]*z[126];
    z[127]=5*z[69];
    z[128]=z[83] - z[127];
    z[128]=z[7]*z[128];
    z[82]=n<T>(3,2)*z[128] - 2*z[82];
    z[82]=z[6]*z[82];
    z[47]=z[18] + n<T>(3,4)*z[47];
    z[47]=z[10]*z[47];
    z[128]=z[16]*z[6];
    z[47]= - n<T>(17,4)*z[128] + z[47];
    z[47]=z[10]*z[47];
    z[46]=z[46] + n<T>(1,2)*z[47] + z[82] + z[92] + z[124] + z[120] + z[123]
   ;
    z[46]=z[4]*z[46];
    z[47]=z[24] + z[95];
    z[82]=npow(z[5],2);
    z[47]=z[3]*z[82]*z[47];
    z[92]=z[24] + z[51];
    z[120]=npow(z[7],2);
    z[123]=z[120]*z[6];
    z[92]=z[92]*z[123];
    z[124]=z[120]*z[1];
    z[129]=z[82]*z[1];
    z[47]=z[92] + z[47] + z[129] + z[124];
    z[47]=z[4]*z[47];
    z[92]=z[24] - z[73];
    z[92]=z[5]*z[92];
    z[92]= - z[1] + z[92];
    z[130]=npow(z[3],2);
    z[92]=z[7]*z[92]*z[130];
    z[47]=z[92] + z[47];
    z[92]=z[106]*z[3];
    z[44]=z[44] + z[92];
    z[44]=z[3]*z[44];
    z[131]=z[18] - z[16];
    z[132]= - 8*z[131] + z[95];
    z[132]=z[10]*z[132];
    z[44]=z[132] - n<T>(59,2)*z[1] + z[44];
    z[44]=z[10]*z[44];
    z[132]=z[16]*z[3];
    z[133]= - n<T>(71,4)*z[1] + z[132];
    z[133]=z[3]*z[133];
    z[44]=z[133] + z[44];
    z[44]=z[10]*z[44];
    z[133]=z[8]*z[18];
    z[133]= - z[1] + z[133];
    z[125]=z[133]*z[125];
    z[133]= - z[16] + z[105];
    z[133]=z[5]*z[133]*z[130];
    z[44]=z[44] + 3*z[125] + z[133];
    z[44]=z[2]*z[44];
    z[125]=2*z[18];
    z[133]=n<T>(1,2)*z[16];
    z[134]=z[105] - z[133] - z[125];
    z[134]=z[7]*z[134];
    z[135]=z[54]*z[1];
    z[136]= - z[18] + z[135];
    z[136]=z[8]*z[136];
    z[134]=z[136] + z[1] + z[134];
    z[136]=z[8]*z[7];
    z[134]=z[134]*z[136];
    z[124]=z[124] + z[134];
    z[124]=z[124]*z[115];
    z[134]= - z[92] - z[16];
    z[82]=z[82]*z[134];
    z[120]= - z[16]*z[120];
    z[82]=z[120] + z[82];
    z[82]=z[3]*z[82];
    z[92]= - z[92]*z[123];
    z[82]=z[92] - n<T>(1,2)*z[129] + z[82];
    z[82]=z[6]*z[82];
    z[92]= - z[31] - z[132];
    z[92]=z[10]*z[92];
    z[120]=z[130]*z[1];
    z[123]=n<T>(1,2)*z[120];
    z[130]=z[3]*z[1];
    z[134]=z[10]*z[130];
    z[134]=z[123] + z[134];
    z[134]=z[15]*z[134];
    z[137]= - n<T>(17,2)*z[1] - z[132];
    z[137]=z[3]*z[137];
    z[92]=z[134] + z[137] + 2*z[92];
    z[92]=z[2]*z[92];
    z[134]=z[32]*z[130];
    z[92]=z[92] - z[120] - z[134];
    z[137]=npow(z[10],2);
    z[138]=z[137]*z[15];
    z[92]=z[92]*z[138];
    z[139]=3*z[1];
    z[140]=z[139] + z[132];
    z[140]=z[140]*z[32];
    z[141]=n<T>(39,2)*z[1] + z[132];
    z[141]=z[3]*z[141];
    z[140]=z[141] + z[140];
    z[140]=z[140]*z[137];
    z[44]=z[92] + z[44] + z[140] + z[82] + z[124] + 2*z[47];
    z[44]=z[15]*z[44];
    z[47]=z[125] + z[49];
    z[47]=z[47]*z[87];
    z[82]= - z[8]*z[47];
    z[82]=z[82] + z[105] + z[43] + z[89];
    z[82]=z[8]*z[82];
    z[82]=z[82] - z[1] + n<T>(1,4)*z[112];
    z[82]=z[8]*z[82];
    z[87]=16*z[16];
    z[92]=z[87] - z[19];
    z[92]=z[12]*z[92];
    z[92]= - z[79] + z[92];
    z[92]=z[10]*z[92];
    z[92]=z[92] - n<T>(23,2)*z[95] + 53*z[16] - n<T>(43,2)*z[18];
    z[92]=z[10]*z[92];
    z[124]=n<T>(81,4)*z[16] - z[95];
    z[124]=z[3]*z[124];
    z[92]=z[92] - n<T>(119,4)*z[1] + z[124];
    z[92]=z[10]*z[92];
    z[124]=z[49] + z[105];
    z[124]=z[5]*z[124];
    z[59]=z[59] + z[16];
    z[140]=n<T>(1,4)*z[3];
    z[141]=z[59]*z[140];
    z[142]=n<T>(23,2)*z[1];
    z[124]=z[141] - z[142] + z[124];
    z[124]=z[3]*z[124];
    z[141]=z[73] - z[16];
    z[143]=z[141]*z[3];
    z[144]= - z[141]*z[103];
    z[145]=z[10]*z[133];
    z[144]=z[145] + z[144] + z[143];
    z[100]=z[144]*z[100];
    z[82]=z[100] + z[92] + z[82] + z[124];
    z[82]=z[2]*z[82];
    z[47]=z[47] + 3*z[65];
    z[47]=z[8]*z[47];
    z[47]=z[47] - 8*z[135] - n<T>(11,2)*z[73] - z[133] - z[89];
    z[47]=z[47]*z[136];
    z[92]= - 5*z[73] + z[133] + z[48];
    z[92]=z[7]*z[92];
    z[100]=n<T>(9,4)*z[112];
    z[92]=z[92] + z[1] + z[100];
    z[92]=z[7]*z[92];
    z[47]=z[92] + z[47];
    z[47]=z[8]*z[47];
    z[92]=n<T>(5,4)*z[16];
    z[124]=z[92] - z[73];
    z[124]=z[5]*z[124];
    z[127]=n<T>(23,4)*z[16] - z[127];
    z[127]=z[7]*z[127];
    z[136]=5*z[71];
    z[144]=9*z[16];
    z[145]=z[144] + z[136];
    z[145]=z[145]*z[102];
    z[146]= - z[7]*z[106];
    z[146]= - z[73] + z[146];
    z[146]=z[3]*z[146];
    z[124]=z[146] + z[145] + 5*z[124] + z[127];
    z[127]=n<T>(1,2)*z[3];
    z[124]=z[124]*z[127];
    z[145]= - n<T>(5,2)*z[71] - n<T>(3,2)*z[73] - z[69];
    z[145]=z[145]*z[127];
    z[146]=z[24] - n<T>(3,2)*z[69];
    z[146]=z[7]*z[146];
    z[92]= - z[92] + z[111];
    z[92]=z[8]*z[92];
    z[92]=z[145] + z[92] - n<T>(3,4)*z[112] + z[146];
    z[92]=z[6]*z[92];
    z[88]= - z[88] - z[125];
    z[88]=z[5]*z[88];
    z[88]=z[121] + z[88];
    z[88]=z[5]*z[88];
    z[145]=n<T>(9,4)*z[16];
    z[146]=z[145] + z[18];
    z[126]=z[146]*z[126];
    z[146]= - z[1] + z[86];
    z[146]=z[7]*z[146];
    z[88]=z[92] + z[124] + z[126] + z[88] + z[146];
    z[88]=z[6]*z[88];
    z[92]=19*z[16] - 11*z[73];
    z[92]=z[5]*z[92];
    z[59]= - z[59]*z[78];
    z[59]=z[59] + n<T>(21,2)*z[1] + z[92];
    z[59]=z[59]*z[78];
    z[86]= - z[86]*z[103];
    z[92]=z[7]*z[141];
    z[92]=n<T>(31,4)*z[92] + n<T>(7,2)*z[1] - z[112];
    z[92]=z[3]*z[92];
    z[124]= - z[1] + z[112];
    z[124]=z[5]*z[124];
    z[59]=z[92] + z[86] + z[124] + z[59];
    z[59]=z[3]*z[59];
    z[86]=z[31] - n<T>(19,8)*z[132];
    z[92]=z[6]*z[95];
    z[124]= - z[10]*z[144];
    z[86]=z[124] + 11*z[86] + n<T>(3,8)*z[92];
    z[86]=z[10]*z[86];
    z[92]=n<T>(7,2)*z[16];
    z[124]=z[92] - 3*z[95];
    z[124]=z[6]*z[124];
    z[124]=z[124] + 97*z[1] - 7*z[132];
    z[124]=z[3]*z[124];
    z[86]=n<T>(1,4)*z[124] + z[86];
    z[86]=z[10]*z[86];
    z[44]=z[44] + z[82] + z[46] + z[86] + z[88] + z[59] + n<T>(3,2)*z[129]
    + z[47];
    z[44]=z[15]*z[44];
    z[46]=z[5]*z[57];
    z[46]= - 2*z[46] + z[55] + n<T>(3,2)*z[56];
    z[46]=z[7]*z[46];
    z[47]=n<T>(15,4)*z[21] + z[113];
    z[47]=z[12]*z[47];
    z[47]= - n<T>(3,2)*z[41] + z[47];
    z[47]=z[5]*z[47];
    z[46]=z[46] + z[47] + z[117] - z[66];
    z[46]=z[7]*z[46];
    z[47]=z[64]*z[23];
    z[47]=z[47] - z[34];
    z[47]=z[9]*z[47];
    z[55]= - z[91]*z[36];
    z[55]=z[38] + z[55];
    z[47]=2*z[55] + z[47];
    z[47]=z[47]*z[61];
    z[55]= - z[12]*z[111];
    z[55]=z[55] - z[91];
    z[55]=z[8]*z[55];
    z[59]=z[99]*z[7];
    z[82]=z[59] - z[63];
    z[82]=z[82]*z[3];
    z[86]=n<T>(3,2)*z[21] - z[38];
    z[88]=n<T>(1,2)*z[5];
    z[86]=z[86]*z[88];
    z[46]= - n<T>(5,4)*z[82] + z[55] + z[46] + z[47] + z[86] - z[24] + 
    z[108];
    z[46]=z[6]*z[46];
    z[47]=3*z[41];
    z[55]=z[47] + z[98];
    z[55]=z[5]*z[55];
    z[55]=z[59] - z[67] + z[55];
    z[55]=z[55]*z[78];
    z[59]= - n<T>(29,4)*z[21] - z[63];
    z[59]=z[59]*z[88];
    z[55]= - n<T>(3,4)*z[82] - n<T>(53,8)*z[71] + z[55] + z[81] + z[59];
    z[55]=z[3]*z[55];
    z[59]=n<T>(17,2)*z[18];
    z[39]=z[39] + z[59];
    z[39]=z[12]*z[39];
    z[39]= - z[106] + z[39];
    z[39]=z[12]*z[39];
    z[67]= - z[110]*z[33];
    z[39]=z[39] + z[67];
    z[39]=z[5]*z[39];
    z[67]=z[119] - z[48];
    z[67]=z[12]*z[67];
    z[27]= - z[1]*z[27];
    z[27]=z[27] + z[34];
    z[27]=z[27]*z[84];
    z[27]=z[27] + z[67] + z[39];
    z[27]=z[9]*z[27];
    z[34]=n<T>(81,2)*z[16] + 47*z[18];
    z[34]=z[12]*z[34];
    z[34]= - n<T>(55,4)*z[21] + z[34];
    z[34]=n<T>(1,2)*z[34] - 2*z[40];
    z[34]=z[5]*z[34];
    z[27]=z[27] + z[34] + z[144] - n<T>(49,2)*z[18];
    z[27]=z[9]*z[27];
    z[34]=z[35] + 59*z[18];
    z[35]= - n<T>(29,4)*z[16] - z[19];
    z[35]=z[12]*z[35];
    z[35]= - z[114] + z[35];
    z[35]=z[5]*z[35];
    z[34]=n<T>(1,4)*z[34] + z[35];
    z[34]=z[5]*z[34];
    z[35]= - z[96]*z[88];
    z[35]=z[35] + n<T>(17,2)*z[21] - z[66];
    z[35]=z[5]*z[35];
    z[35]=n<T>(21,2)*z[16] + z[35];
    z[39]=z[47] + n<T>(5,2)*z[56];
    z[39]=z[5]*z[39];
    z[39]= - z[42] + z[39];
    z[39]=z[7]*z[39];
    z[35]=n<T>(1,2)*z[35] + z[39];
    z[35]=z[7]*z[35];
    z[39]= - n<T>(19,2)*z[16] - z[48];
    z[39]=z[12]*z[39];
    z[40]= - z[52]*z[115];
    z[39]=z[40] - n<T>(5,2)*z[21] + z[39];
    z[39]=z[8]*z[39];
    z[40]=z[92] + 43*z[18];
    z[39]=n<T>(1,4)*z[40] + z[39];
    z[39]=z[8]*z[39];
    z[40]=10*z[1];
    z[27]=z[46] + z[55] + z[39] + z[35] + z[27] - z[40] + z[34];
    z[27]=z[6]*z[27];
    z[34]=5*z[16];
    z[35]= - z[34] - z[95];
    z[35]=z[3]*z[35];
    z[39]=z[34] + z[51];
    z[39]=z[6]*z[39];
    z[35]=z[35] + z[39];
    z[35]=z[35]*z[74];
    z[39]=z[95] - z[51];
    z[39]=z[4]*z[39];
    z[39]=z[39] - z[132] + z[128];
    z[39]=z[2]*z[39];
    z[42]=z[139] + n<T>(1,2)*z[132];
    z[42]=z[3]*z[42];
    z[46]= - z[6]*z[133];
    z[46]= - z[139] + z[46];
    z[46]=z[6]*z[46];
    z[35]=z[39] + z[35] + z[42] + z[46];
    z[35]=z[2]*z[35];
    z[39]= - z[97] + z[85];
    z[39]=z[9]*z[39];
    z[42]=n<T>(1,2)*z[29];
    z[46]= - z[16] - z[42];
    z[46]=z[7]*z[46];
    z[39]=z[46] + 17*z[1] + z[39];
    z[39]=z[7]*z[9]*z[39];
    z[46]=z[16]*z[9];
    z[47]=z[46] - z[31];
    z[47]=z[47]*z[61];
    z[17]=z[17] - z[29];
    z[17]=z[9]*z[17];
    z[17]= - z[90] + z[17];
    z[17]=z[7]*z[17];
    z[17]= - z[47] + z[17];
    z[17]=z[17]*z[118];
    z[51]=5*z[1];
    z[52]=8*z[46];
    z[55]= - z[51] + z[52];
    z[55]=z[55]*z[68];
    z[17]=z[17] + z[55] + z[39];
    z[17]=z[3]*z[17];
    z[39]=n<T>(15,2)*z[1] - z[52];
    z[39]=z[39]*z[68];
    z[55]=z[68]*z[7];
    z[67]=z[16]*z[55];
    z[39]=z[39] + z[67];
    z[39]=z[7]*z[39];
    z[67]=z[68]*z[1];
    z[74]= - z[78]*z[67];
    z[82]=npow(z[9],3);
    z[86]=z[82]*z[1];
    z[96]=4*z[86];
    z[74]=z[96] + z[74];
    z[74]=z[7]*z[74];
    z[108]= - z[3]*z[31]*z[55];
    z[74]=z[74] + z[108];
    z[74]=z[14]*z[74];
    z[47]=z[7]*z[47];
    z[47]=z[67] + z[47];
    z[47]=z[47]*z[118];
    z[39]=z[74] + z[47] - z[96] + z[39];
    z[39]=z[3]*z[39];
    z[47]=z[1]*z[55];
    z[47]= - 12*z[86] + z[47];
    z[47]=z[7]*z[47];
    z[39]=z[47] + z[39];
    z[39]=z[14]*z[39];
    z[47]=z[64]*z[84];
    z[67]= - z[139] + z[47];
    z[67]=z[67]*z[68];
    z[55]= - z[109]*z[55];
    z[55]=3*z[67] + z[55];
    z[55]=z[7]*z[55];
    z[17]=z[39] + z[17] + 8*z[86] + z[55];
    z[17]=z[14]*z[17];
    z[39]= - n<T>(79,2)*z[16] + 11*z[29];
    z[39]=z[9]*z[39];
    z[39]=n<T>(73,2)*z[1] + z[39];
    z[55]=z[145] + z[29];
    z[55]=z[7]*z[55];
    z[39]=n<T>(1,2)*z[39] + z[55];
    z[39]=z[7]*z[39];
    z[55]=2*z[29];
    z[67]= - z[122] + z[55];
    z[67]=z[9]*z[67];
    z[74]=z[7]*z[133];
    z[67]=z[74] + z[40] + z[67];
    z[67]=z[3]*z[67];
    z[74]=z[144] - z[85];
    z[74]=z[9]*z[74];
    z[74]= - 9*z[1] + z[74];
    z[74]=z[9]*z[74];
    z[39]=z[67] + z[74] + z[39];
    z[39]=z[3]*z[39];
    z[67]=z[23]*z[1];
    z[74]= - z[84]*z[67];
    z[86]=n<T>(9,2)*z[18];
    z[74]=z[74] + z[87] + z[86];
    z[74]=z[9]*z[74];
    z[74]= - 15*z[1] + z[74];
    z[74]=z[9]*z[74];
    z[87]=z[1] + n<T>(9,2)*z[46];
    z[87]=z[87]*z[78];
    z[108]=z[15]*z[120];
    z[74]= - 8*z[108] + z[74] + z[87];
    z[74]=z[7]*z[74];
    z[87]= - z[16] + z[89];
    z[87]=z[87]*z[68];
    z[108]=z[9]*z[23]*z[31];
    z[108]= - z[133] + z[108];
    z[108]=z[6]*z[9]*z[108];
    z[87]=z[87] + z[108];
    z[87]=z[6]*z[87];
    z[108]=z[49] - z[29];
    z[108]=z[108]*z[9];
    z[110]= - z[29]*z[127];
    z[110]=z[108] + z[110];
    z[110]=z[3]*z[110];
    z[42]=z[6]*z[42];
    z[42]= - z[108] + z[42];
    z[42]=z[6]*z[42];
    z[42]=z[110] + z[42];
    z[42]=z[4]*z[42];
    z[47]=n<T>(11,2)*z[1] - z[47];
    z[47]=z[47]*z[68];
    z[17]=z[17] + z[35] + z[42] + z[87] + z[39] + z[47] + z[74];
    z[17]=z[14]*z[17];
    z[35]=z[71] - z[16];
    z[35]=z[35]*z[3];
    z[39]=z[131]*z[5];
    z[39]=z[39] - z[1];
    z[42]=z[73]*z[8];
    z[47]=z[35] - z[42] - z[39];
    z[47]=z[10]*z[47];
    z[71]=z[5]*z[49];
    z[71]=z[71] - z[42];
    z[71]=z[71]*z[75];
    z[74]=z[5]*z[1];
    z[71]= - z[74] + z[71];
    z[87]= - z[101]*z[103];
    z[87]=z[31] + z[87];
    z[87]=z[3]*z[87];
    z[47]=z[47] + 3*z[71] + z[87];
    z[47]=z[47]*z[137];
    z[71]=z[84]*z[73];
    z[87]=z[8]*z[105];
    z[71]=z[87] - n<T>(3,2)*z[112] + z[71];
    z[87]=z[68]*z[8];
    z[71]=z[71]*z[87];
    z[101]=z[106]*z[8];
    z[103]= - z[101] + n<T>(3,2)*z[16] - z[85];
    z[87]=z[103]*z[87];
    z[103]= - z[1] + z[52];
    z[103]=z[103]*z[68];
    z[87]=z[103] + z[87];
    z[87]=z[3]*z[87];
    z[103]= - z[15]*npow(z[10],3);
    z[108]=z[14]*z[82];
    z[103]= - 4*z[108] + z[103];
    z[103]=z[130]*z[103];
    z[39]=z[39]*z[84];
    z[39]=z[74] + z[39];
    z[39]=z[39]*z[68];
    z[39]=z[47] + z[87] + z[39] + z[71] + z[103];
    z[39]=z[11]*z[39];
    z[47]=z[5]*z[67];
    z[47]=8*z[47] - z[104] - 24*z[18];
    z[47]=z[5]*z[47];
    z[71]=z[1]*z[28];
    z[71]= - 4*z[67] + z[71];
    z[71]=z[5]*z[71];
    z[71]=z[19] + z[71];
    z[71]=z[71]*z[32];
    z[35]=z[71] - n<T>(3,2)*z[35] + n<T>(11,2)*z[42] + z[40] + z[47];
    z[35]=z[10]*z[35];
    z[40]=7*z[112] - n<T>(23,2)*z[42];
    z[40]=z[40]*z[102];
    z[42]=z[34] - z[101];
    z[42]=z[42]*z[75];
    z[42]= - 13*z[1] + z[42];
    z[42]=z[42]*z[127];
    z[47]=z[5]*z[48];
    z[47]= - n<T>(27,2)*z[1] + z[47];
    z[47]=z[5]*z[47];
    z[35]=z[35] + z[42] + z[47] + z[40];
    z[35]=z[10]*z[35];
    z[40]= - z[67]*z[88];
    z[40]=z[40] - z[16] + z[86];
    z[40]=z[5]*z[40];
    z[42]=2*z[21];
    z[25]= - z[42] + z[25];
    z[25]=z[5]*z[25];
    z[25]=z[25] - z[64];
    z[25]=z[25]*z[84];
    z[25]=z[25] - 4*z[1] + z[40];
    z[25]=z[9]*z[25];
    z[40]=z[68]*z[63];
    z[47]=z[9]*z[63];
    z[47]= - n<T>(33,8)*z[73] + z[47];
    z[47]=z[8]*z[47];
    z[40]=z[47] + z[100] + 8*z[40];
    z[40]=z[8]*z[40];
    z[47]= - z[5]*z[18];
    z[47]=z[142] + z[47];
    z[47]=z[5]*z[47];
    z[25]=z[40] + z[47] + z[25];
    z[25]=z[9]*z[25];
    z[40]=z[122] + z[85];
    z[40]=z[9]*z[40];
    z[40]= - n<T>(19,2)*z[1] + z[40];
    z[40]=z[9]*z[40];
    z[47]= - z[83] - z[55];
    z[47]=z[9]*z[47];
    z[48]=z[8]*z[29];
    z[47]=z[47] + n<T>(5,8)*z[48];
    z[48]=5*z[8];
    z[47]=z[47]*z[48];
    z[48]=z[24] - z[29];
    z[48]=z[48]*z[61];
    z[48]= - n<T>(3,2)*z[1] + z[48];
    z[48]=z[3]*z[48];
    z[40]=z[48] + z[40] + z[47];
    z[40]=z[3]*z[40];
    z[47]=z[121] - z[52];
    z[47]=z[47]*z[68];
    z[46]= - z[1] + z[46];
    z[46]=z[3]*z[46]*z[84];
    z[46]=z[47] + z[46];
    z[46]=z[3]*z[46];
    z[47]=z[82]*z[31];
    z[48]= - z[68]*z[130];
    z[47]=z[47] + z[48];
    z[47]=z[14]*z[47]*z[118];
    z[46]=z[47] - z[96] + z[46];
    z[46]=z[14]*z[46];
    z[47]=z[123] + z[134];
    z[47]=z[15]*z[47];
    z[48]= - z[10]*z[90];
    z[47]=z[47] - 13*z[130] + z[48];
    z[47]=z[47]*z[138];
    z[25]=z[39] + z[46] + z[47] + z[35] + z[40] + z[129] + z[25];
    z[25]=z[11]*z[25];
    z[19]=z[92] + z[19];
    z[19]=z[12]*z[19];
    z[35]= - n<T>(31,2)*z[16] - z[94];
    z[35]=z[35]*z[33];
    z[19]=5*z[19] + z[35];
    z[19]=z[5]*z[19];
    z[35]=z[23]*z[105];
    z[35]=z[56] + z[35];
    z[35]=z[5]*z[35];
    z[39]= - n<T>(9,4)*z[56] + z[30];
    z[39]=z[39]*z[72];
    z[35]=z[35] + z[39];
    z[35]=z[7]*z[35];
    z[39]=z[54]*z[21];
    z[39]=z[39] - z[41];
    z[40]= - z[39]*z[75];
    z[46]=z[7]*z[56];
    z[40]=z[40] - z[21] + n<T>(3,8)*z[46];
    z[40]=z[8]*z[40];
    z[46]=z[12]*z[107];
    z[46]=n<T>(1,2)*z[58] + z[46];
    z[46]=z[8]*z[46];
    z[46]= - n<T>(11,8)*z[60] + z[46];
    z[46]=z[8]*z[46];
    z[30]=n<T>(1,4)*z[56] - z[30];
    z[30]=z[9]*z[30];
    z[30]=z[46] + z[42] + n<T>(3,2)*z[30];
    z[30]=z[6]*z[30];
    z[46]=z[16] + z[125];
    z[46]=z[46]*z[23];
    z[28]= - z[64]*z[28];
    z[28]=z[46] + z[28];
    z[28]=z[5]*z[28];
    z[28]= - z[67] + z[28];
    z[28]=z[28]*z[32];
    z[19]=z[28] + z[30] + z[40] + 3*z[35] + z[19] - z[119] - z[89];
    z[19]=z[10]*z[19];
    z[28]=z[12]*z[73];
    z[28]=8*z[28] + 12*z[21] - n<T>(13,4)*z[38];
    z[28]=z[5]*z[28];
    z[30]=z[85]*z[23];
    z[32]=z[79] - n<T>(3,2)*z[38];
    z[32]=z[32]*z[80];
    z[32]=z[32] + z[30];
    z[32]=z[9]*z[32];
    z[35]= - 45*z[21] - z[38];
    z[32]=n<T>(1,4)*z[35] + z[32];
    z[32]=z[9]*z[32];
    z[28]=z[32] + z[24] + z[28];
    z[28]=z[7]*z[28];
    z[32]=13*z[21] + z[66];
    z[32]=z[7]*z[32]*z[45];
    z[35]= - z[39]*z[115];
    z[39]=6*z[21];
    z[32]=z[35] - z[39] + z[32];
    z[32]=z[8]*z[32];
    z[35]= - n<T>(21,2)*z[21] - 23*z[38];
    z[35]=z[35]*z[93];
    z[32]=z[32] + z[35] + z[16] + n<T>(53,8)*z[73];
    z[32]=z[8]*z[32];
    z[35]= - 14*z[21] + n<T>(9,2)*z[38];
    z[35]=z[12]*z[35];
    z[30]=z[35] - z[30];
    z[30]=z[9]*z[30];
    z[35]= - n<T>(97,2)*z[21] + 13*z[38];
    z[30]=n<T>(1,4)*z[35] + z[30];
    z[30]=z[9]*z[30];
    z[35]= - 29*z[21] - z[66];
    z[35]=z[35]*z[45];
    z[40]=z[57]*z[115];
    z[35]=z[40] - 13*z[41] + z[35];
    z[35]=z[8]*z[35];
    z[40]=n<T>(49,2)*z[21] + 9*z[38];
    z[35]=n<T>(3,4)*z[40] + z[35];
    z[35]=z[8]*z[35];
    z[40]=3*z[5];
    z[46]=z[91]*z[40];
    z[46]= - z[104] + z[46];
    z[30]= - n<T>(35,8)*z[95] + z[35] + n<T>(1,2)*z[46] + z[30];
    z[30]=z[6]*z[30];
    z[35]= - n<T>(59,2)*z[16] - 15*z[18];
    z[35]=z[35]*z[36];
    z[35]=z[35] + 11*z[16] + n<T>(37,4)*z[18];
    z[35]=z[5]*z[35];
    z[36]= - n<T>(91,2)*z[16] + z[136];
    z[36]=z[36]*z[140];
    z[19]=z[19] + z[30] + z[36] + z[32] + z[28] + n<T>(47,4)*z[1] + z[35];
    z[19]=z[10]*z[19];
    z[28]=z[21] - n<T>(11,8)*z[63];
    z[30]= - z[70] + z[98];
    z[30]=z[30]*z[72];
    z[28]=3*z[28] + z[30];
    z[28]=z[9]*z[28];
    z[20]= - 4*z[20] - 11*z[65];
    z[20]=z[7]*z[20];
    z[30]=z[97] + z[89];
    z[30]=z[12]*z[30];
    z[30]=z[39] + z[30];
    z[30]=z[12]*z[30];
    z[30]=z[30] + 3*z[62];
    z[30]=z[7]*z[30];
    z[32]= - z[9]*z[41];
    z[30]=z[32] + z[30];
    z[30]=z[8]*z[30];
    z[20]=z[30] + z[20] + z[28] + z[16] + n<T>(23,4)*z[73];
    z[20]=z[8]*z[20];
    z[28]=z[79] + z[77];
    z[30]=z[99]*z[84];
    z[28]=3*z[28] + z[30];
    z[28]=z[9]*z[28];
    z[28]=z[28] - z[34] - n<T>(99,8)*z[73];
    z[28]=z[9]*z[28];
    z[30]=z[49] + z[116];
    z[30]=n<T>(1,2)*z[30] + 29*z[73];
    z[30]=n<T>(1,2)*z[30] + 15*z[135];
    z[30]=z[7]*z[30];
    z[20]=z[20] + z[30] - 4*z[112] + z[28];
    z[20]=z[8]*z[20];
    z[28]=n<T>(59,2)*z[21] + z[63];
    z[28]=z[5]*z[28];
    z[30]= - 5*z[21] + z[63];
    z[30]=z[7]*z[30];
    z[28]=z[30] - n<T>(25,4)*z[29] - n<T>(125,4)*z[16] + z[28];
    z[28]=z[28]*z[78];
    z[30]=15*z[16] - n<T>(19,2)*z[29];
    z[30]= - z[101] + n<T>(1,2)*z[30] + 3*z[69];
    z[30]=z[30]*z[75];
    z[32]=z[21] - n<T>(3,2)*z[63];
    z[32]=z[7]*z[32];
    z[32]=z[32] - z[92] - z[29];
    z[32]=z[32]*z[127];
    z[34]=n<T>(33,4)*z[16] - z[85];
    z[34]=z[9]*z[34];
    z[35]=53*z[1] - 27*z[112];
    z[28]=z[32] + z[30] + z[28] + n<T>(1,4)*z[35] + z[34];
    z[28]=z[3]*z[28];
    z[30]=7*z[16];
    z[32]=z[30] + 13*z[18];
    z[32]=z[32]*z[45];
    z[33]= - z[109]*z[33];
    z[32]=z[33] - z[42] + z[32];
    z[32]=z[5]*z[32];
    z[33]= - z[12]*z[64];
    z[26]=z[12]*z[26];
    z[26]= - z[41] + z[26];
    z[26]=z[5]*z[26];
    z[21]=z[26] - z[21] + z[33];
    z[21]=z[21]*z[84];
    z[26]=z[133] - z[125];
    z[21]=z[21] + 3*z[26] + z[32];
    z[21]=z[9]*z[21];
    z[26]= - z[43] - z[18];
    z[26]=z[5]*z[26]*z[45];
    z[26]=z[26] + n<T>(13,2)*z[16] + z[76];
    z[26]=z[26]*z[40];
    z[21]=z[21] - z[142] + z[26];
    z[21]=z[9]*z[21];
    z[26]= - n<T>(5,2)*z[29] + 9*z[73] - z[30] - z[18];
    z[26]=z[26]*z[78];
    z[23]= - z[16]*z[23]*z[84];
    z[18]= - z[81] - z[18];
    z[18]=z[12]*z[18];
    z[18]=z[23] - z[106] + z[18];
    z[18]=z[9]*z[18];
    z[18]=6*z[50] + z[18];
    z[18]=z[9]*z[18];
    z[23]= - 31*z[16] + 25*z[73];
    z[23]=z[5]*z[23];
    z[23]= - z[139] + z[23];
    z[18]=z[26] + n<T>(1,4)*z[23] + z[18];
    z[18]=z[7]*z[18];
    z[23]=z[5]*z[60];
    z[23]=z[38] + z[23];
    z[23]=z[6]*z[23];
    z[26]= - z[15]*z[143];
    z[16]=z[26] + z[23] + z[16] + z[73];
    z[16]=z[5]*z[16];
    z[23]=z[3]*z[73];
    z[24]= - z[4]*z[24];
    z[16]=z[24] + z[23] + z[31] + z[16];
    z[16]=z[13]*z[16];
    z[23]= - z[53] - z[59];
    z[23]=z[5]*z[23];
    z[23]=z[51] + z[23];
    z[23]=z[5]*z[23];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[23]
       + z[25] + z[27] + z[28] + z[37] + z[44];
 
    return r;
}

template double qqb_2lNLC_r726(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r726(const std::array<dd_real,31>&);
#endif
