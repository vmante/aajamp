#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1174(const std::array<T,31>& k) {
  T z[156];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[17];
    z[5]=k[19];
    z[6]=k[12];
    z[7]=k[22];
    z[8]=k[13];
    z[9]=k[14];
    z[10]=k[11];
    z[11]=k[3];
    z[12]=k[6];
    z[13]=k[9];
    z[14]=k[4];
    z[15]=k[15];
    z[16]=k[5];
    z[17]=k[8];
    z[18]=k[2];
    z[19]=k[20];
    z[20]=4*z[3];
    z[21]=z[14]*z[1];
    z[22]= - z[21]*z[20];
    z[23]=5*z[1];
    z[24]=npow(z[1],2);
    z[25]=z[24]*z[3];
    z[22]=z[22] - z[23] + z[25];
    z[22]=z[14]*z[22];
    z[26]=2*z[24];
    z[27]=z[16]*z[1];
    z[28]=z[26] - z[27];
    z[22]=z[22] + z[28];
    z[22]=z[14]*z[22];
    z[29]=4*z[1];
    z[30]=z[29] - z[25];
    z[31]=2*z[3];
    z[32]=z[31]*z[21];
    z[33]=z[32] + z[30];
    z[33]=z[14]*z[33];
    z[34]=z[27] - z[24];
    z[33]=2*z[34] + z[33];
    z[33]=z[14]*z[33];
    z[35]=z[24]*z[16];
    z[33]= - z[35] + z[33];
    z[36]=z[2]*z[14];
    z[33]=z[33]*z[36];
    z[37]=z[27] + z[24];
    z[38]=z[37]*z[16];
    z[39]=z[26] + z[27];
    z[40]= - z[39] + z[21];
    z[41]=npow(z[16],2);
    z[40]=z[10]*z[41]*z[40];
    z[22]=z[40] + z[33] + z[38] + z[22];
    z[33]=npow(z[1],4);
    z[40]=z[33]*z[16];
    z[42]=z[33]*z[14];
    z[43]= - z[40] - 2*z[42];
    z[44]=npow(z[14],3);
    z[45]=n<T>(1,3)*z[2];
    z[43]=z[43]*z[44]*z[45];
    z[46]=z[12]*z[2];
    z[47]=n<T>(1,3)*z[3];
    z[48]= - npow(z[21],5)*z[47]*z[46];
    z[49]=z[33]*z[3];
    z[50]=z[49]*npow(z[14],4);
    z[43]=z[48] + z[50] + z[43];
    z[43]=z[12]*z[43];
    z[48]=npow(z[1],3);
    z[51]=z[48]*z[44];
    z[43]=z[51] + z[43];
    z[51]=npow(z[12],3);
    z[43]=z[43]*z[51];
    z[39]=z[39]*z[16];
    z[39]=z[39] + z[48];
    z[52]=z[39]*z[10];
    z[52]=z[52] - z[37];
    z[53]=n<T>(1,3)*z[9];
    z[54]=z[53]*z[41]*z[52];
    z[22]=z[54] + n<T>(1,3)*z[22] + z[43];
    z[43]=4*z[15];
    z[22]=z[22]*z[43];
    z[54]=z[48]*z[3];
    z[55]=z[44]*z[54];
    z[56]=z[48]*z[16];
    z[57]=z[48]*z[14];
    z[58]= - 2*z[56] - 7*z[57];
    z[59]=npow(z[14],2);
    z[60]=z[59]*z[2];
    z[58]=z[58]*z[60];
    z[50]=z[46]*z[50];
    z[50]= - 5*z[50] + 13*z[55] + z[58];
    z[58]=n<T>(1,3)*z[12];
    z[50]=z[50]*z[58];
    z[61]=3*z[24];
    z[62]=z[59]*z[61];
    z[50]=z[62] + z[50];
    z[62]=npow(z[12],2);
    z[63]=2*z[62];
    z[50]=z[50]*z[63];
    z[64]=z[21]*z[3];
    z[65]= - n<T>(26,3)*z[64] - z[29] + n<T>(5,3)*z[25];
    z[65]=z[14]*z[65];
    z[66]=4*z[27];
    z[67]=5*z[24];
    z[68]= - z[67] + z[66];
    z[69]=5*z[25];
    z[70]=19*z[1] - z[69];
    z[70]=n<T>(1,3)*z[70] + 5*z[64];
    z[70]=z[14]*z[70];
    z[68]=n<T>(1,3)*z[68] + z[70];
    z[68]=z[68]*z[36];
    z[70]=19*z[24];
    z[71]=8*z[27];
    z[72]=z[70] + z[71];
    z[72]=z[16]*z[72];
    z[73]=11*z[48];
    z[72]=z[73] + z[72];
    z[72]=z[10]*z[72];
    z[74]=11*z[24];
    z[66]=z[72] - z[74] - z[66];
    z[66]=z[53]*z[16]*z[66];
    z[72]= - z[67] - n<T>(4,3)*z[27];
    z[72]=z[16]*z[72];
    z[75]=z[27]*z[14];
    z[76]=n<T>(4,3)*z[75];
    z[72]=z[72] + z[76];
    z[72]=z[10]*z[72];
    z[22]=z[22] + z[66] + z[50] + z[72] + z[68] + n<T>(5,3)*z[24] + z[65];
    z[22]=z[22]*z[43];
    z[30]=7*z[64] + z[30];
    z[30]=z[30]*z[36];
    z[50]=7*z[1];
    z[65]=z[50] + 8*z[16];
    z[66]=z[3]*z[1];
    z[68]=n<T>(32,3) - 25*z[66];
    z[68]=z[14]*z[68];
    z[30]=5*z[30] + 2*z[68] + n<T>(1,3)*z[65] + z[69];
    z[65]=z[59]*z[25];
    z[68]=z[24]*z[14];
    z[69]=5*z[68];
    z[72]= - z[35] - z[69];
    z[72]=z[72]*z[36];
    z[55]=z[46]*z[55];
    z[55]= - n<T>(17,9)*z[55] + 4*z[65] + n<T>(1,3)*z[72];
    z[55]=z[55]*z[62];
    z[72]=z[41]*z[24];
    z[77]=z[72]*z[62];
    z[78]= - 50*z[24] - 19*z[27];
    z[79]=z[67] + n<T>(8,3)*z[27];
    z[79]=z[16]*z[79];
    z[79]=n<T>(25,9)*z[48] + z[79];
    z[79]=z[10]*z[79];
    z[78]= - n<T>(8,9)*z[77] + n<T>(1,9)*z[78] + 2*z[79];
    z[79]=2*z[9];
    z[78]=z[78]*z[79];
    z[80]= - z[26] - n<T>(1,3)*z[27];
    z[81]=8*z[1];
    z[82]=z[81] + z[25];
    z[82]=z[14]*z[82];
    z[80]=n<T>(1,3)*z[82] + 8*z[80] + n<T>(11,3)*z[54];
    z[80]=z[10]*z[80];
    z[82]=z[14]*z[3];
    z[83]=z[82]*z[24];
    z[84]=z[73] + z[68];
    z[84]=z[10]*z[84];
    z[85]=11*z[54];
    z[84]=z[84] - z[85] - z[83];
    z[86]=n<T>(1,3)*z[8];
    z[84]=z[84]*z[86];
    z[22]=z[22] + z[84] + z[78] + 8*z[55] + n<T>(2,3)*z[30] + z[80];
    z[22]=z[15]*z[22];
    z[30]=npow(z[16],3);
    z[55]=z[30]*z[33];
    z[78]=z[41]*z[42];
    z[78]= - z[55] + z[78];
    z[78]=z[14]*z[78];
    z[80]=npow(z[27],4);
    z[78]=z[80] + z[78];
    z[78]=z[2]*z[78];
    z[84]=npow(z[27],5);
    z[87]=z[84]*z[46];
    z[87]=z[87] - z[80];
    z[87]=z[6]*z[87];
    z[88]=z[14]*z[6];
    z[55]=z[88]*z[55];
    z[55]=z[78] + z[55] + z[87];
    z[78]=npow(z[12],4);
    z[55]=z[55]*z[78];
    z[87]=z[41]*z[6];
    z[89]=z[16]*z[88];
    z[89]=z[89] - z[87];
    z[90]=2*z[27];
    z[91]=z[90] + z[24];
    z[89]=z[91]*z[89];
    z[92]=z[6]*z[30];
    z[92]=z[41] + z[92];
    z[92]=z[91]*z[92];
    z[93]=2*z[41];
    z[94]= - z[21]*z[93];
    z[92]=z[94] + z[92];
    z[92]=z[2]*z[92];
    z[94]= - z[48] + z[38];
    z[94]=z[16]*z[94];
    z[39]=z[14]*z[39];
    z[39]=z[39] - z[33] + z[94];
    z[39]=z[10]*z[39];
    z[94]=z[78]*z[33];
    z[95]= - z[59]*z[94];
    z[52]=z[95] - z[21] + z[52];
    z[52]=z[8]*z[41]*z[52];
    z[39]=z[52] + z[55] + z[39] + z[92] + z[48] + z[89];
    z[39]=z[17]*z[39];
    z[34]= - z[16]*z[34];
    z[52]= - z[14]*z[37];
    z[55]=2*z[48];
    z[34]=z[52] + z[55] + z[34];
    z[34]=z[10]*z[34];
    z[34]=z[34] + z[39];
    z[39]=z[48]*z[6];
    z[52]=z[30]*z[39];
    z[89]=z[39]*z[14];
    z[92]= - z[89]*z[93];
    z[92]=5*z[52] + z[92];
    z[95]=n<T>(5,3)*z[48];
    z[96]= - z[30]*z[95];
    z[97]=z[41]*z[95];
    z[98]=z[57]*z[16];
    z[97]=z[97] - z[98];
    z[97]=z[14]*z[97];
    z[96]=z[96] + z[97];
    z[96]=z[2]*z[96];
    z[97]=z[6]*z[80]*z[46];
    z[92]= - n<T>(5,3)*z[97] + n<T>(1,3)*z[92] + z[96];
    z[92]=z[92]*z[51];
    z[93]= - z[10]*z[37]*z[93];
    z[96]=z[41]*z[1];
    z[93]=z[96] + z[93];
    z[97]= - z[48]*z[41];
    z[97]=z[97] + z[98];
    z[97]=z[14]*z[97]*z[51];
    z[93]=n<T>(2,3)*z[93] + z[97];
    z[93]=z[8]*z[93];
    z[97]=n<T>(1,3)*z[24];
    z[98]=z[97] + z[90];
    z[99]=z[6]*z[16];
    z[100]=z[98]*z[99];
    z[101]=n<T>(2,3)*z[27];
    z[102]= - z[88]*z[101];
    z[103]= - z[16] - z[87];
    z[98]=z[98]*z[103];
    z[76]=z[76] + z[98];
    z[76]=z[2]*z[76];
    z[34]=z[93] + z[92] + z[76] + z[102] - z[24] + z[100] + n<T>(2,3)*z[34];
    z[34]=z[17]*z[34];
    z[76]= - 14*z[35] + z[69];
    z[76]=z[14]*z[76];
    z[76]=17*z[72] + z[76];
    z[76]=z[76]*z[45];
    z[92]=z[88]*z[35];
    z[93]=z[72]*z[6];
    z[98]=z[46]*z[52];
    z[76]=n<T>(17,3)*z[98] + z[76] - 4*z[93] + z[92];
    z[98]=n<T>(32,3)*z[62];
    z[76]=z[76]*z[98];
    z[100]=4*z[24];
    z[102]=5*z[21] - z[100] - 17*z[27];
    z[69]= - z[69] + 13*z[35];
    z[69]=z[14]*z[69];
    z[69]= - 5*z[72] + z[69];
    z[69]=z[69]*z[62];
    z[103]=z[16]*z[91];
    z[104]=n<T>(1,3)*z[48];
    z[103]=z[104] + z[103];
    z[105]=4*z[10];
    z[103]=z[103]*z[105];
    z[69]=n<T>(2,3)*z[69] + n<T>(1,3)*z[102] + z[103];
    z[69]=z[8]*z[69];
    z[102]=n<T>(1,3)*z[1];
    z[103]= - z[6]*z[27];
    z[103]=z[102] + z[103];
    z[106]=z[24] + 7*z[27];
    z[107]=z[99] + 1;
    z[106]=z[106]*z[107];
    z[106]=n<T>(1,3)*z[106] - z[21];
    z[106]=z[2]*z[106];
    z[103]=n<T>(4,3)*z[103] + z[106];
    z[106]=z[24]*z[6];
    z[81]=z[81] + z[106];
    z[81]=z[14]*z[81];
    z[81]=4*z[81] - 32*z[28] - z[39];
    z[107]=n<T>(1,3)*z[10];
    z[81]=z[81]*z[107];
    z[100]= - z[88]*z[100];
    z[108]= - z[48] + 4*z[68];
    z[108]=z[10]*z[108];
    z[100]=z[108] + z[39] + z[100];
    z[100]=z[100]*z[53];
    z[34]=32*z[34] + n<T>(16,3)*z[69] + z[100] + z[76] + 16*z[103] + z[81];
    z[34]=z[17]*z[34];
    z[69]=npow(z[1],5);
    z[40]=z[40] - z[69];
    z[76]=z[2]*z[40];
    z[81]=z[42] + z[69];
    z[100]=z[10]*z[81];
    z[76]=z[100] - z[33] + z[76];
    z[76]=z[8]*z[76];
    z[100]=z[2]*z[6];
    z[103]=npow(z[1],6);
    z[108]= - z[103]*z[100];
    z[109]=z[103]*z[2];
    z[110]=z[103]*z[10];
    z[109]=z[109] - z[110];
    z[111]=z[8]*z[109];
    z[110]=z[6]*z[110];
    z[108]=z[111] + z[108] + z[110];
    z[108]=z[5]*z[108];
    z[110]= - z[40]*z[100];
    z[111]= - z[33]*z[88];
    z[112]=z[69]*z[6];
    z[111]= - z[112] + z[111];
    z[111]=z[10]*z[111];
    z[113]=z[33]*z[6];
    z[76]=z[108] + z[76] + z[111] + z[113] + z[110];
    z[76]=z[7]*z[76];
    z[108]=npow(z[6],2);
    z[110]=z[108]*z[33];
    z[111]=z[82]*z[39];
    z[40]=z[40]*z[6];
    z[40]=4*z[40] + 7*z[33];
    z[114]=z[40]*z[100];
    z[111]=n<T>(1,9)*z[114] - n<T>(4,9)*z[110] + z[111];
    z[114]=n<T>(4,3)*z[6];
    z[115]=z[114]*z[103];
    z[115]=z[115] - z[69];
    z[116]=z[115]*z[100];
    z[115]=z[115]*z[6];
    z[117]= - z[10]*z[115];
    z[118]=z[10] - z[2];
    z[119]=z[118]*z[69];
    z[120]= - z[8]*z[119];
    z[116]=z[120] + z[116] + z[117];
    z[117]=n<T>(8,3)*z[5];
    z[116]=z[116]*z[117];
    z[114]=z[114]*z[69];
    z[114]=z[114] - z[33];
    z[120]=8*z[6];
    z[114]=z[114]*z[120];
    z[121]=7*z[48];
    z[122]= - z[121] + 32*z[113];
    z[123]=z[33]*z[10];
    z[124]= - z[123] + n<T>(1,3)*z[122];
    z[124]=z[88]*z[124];
    z[124]=z[114] + z[124];
    z[124]=z[124]*z[107];
    z[125]=8*z[33];
    z[126]=z[42]*z[10];
    z[127]=z[126] + z[125] + n<T>(7,3)*z[57];
    z[127]=z[127]*z[107];
    z[128]=z[82]*z[48];
    z[129]=z[33]*z[2];
    z[130]= - z[128] - n<T>(7,9)*z[129];
    z[127]=8*z[130] + z[127];
    z[127]=z[8]*z[127];
    z[76]=n<T>(32,9)*z[76] + z[116] + z[127] + 8*z[111] + z[124];
    z[76]=z[7]*z[76];
    z[111]=z[105]*z[57];
    z[116]=n<T>(59,3)*z[48];
    z[124]= - z[116] + 17*z[68];
    z[124]=n<T>(1,3)*z[124] - z[111];
    z[124]=z[10]*z[124];
    z[127]=17*z[24];
    z[130]=4*z[54];
    z[131]=z[127] - z[130];
    z[131]=z[131]*z[82];
    z[132]=z[35] - z[48];
    z[133]=z[2]*z[132];
    z[134]=8*z[54];
    z[124]=z[124] + n<T>(32,9)*z[133] + n<T>(2,3)*z[131] + n<T>(59,3)*z[24] + z[134]
   ;
    z[124]=z[8]*z[124];
    z[40]= - z[6]*z[40];
    z[40]= - 4*z[132] + z[40];
    z[40]=z[40]*z[100];
    z[114]=z[116] - z[114];
    z[114]=z[6]*z[114];
    z[116]=n<T>(1,3)*z[6];
    z[122]= - z[122]*z[116];
    z[122]= - z[127] + z[122];
    z[122]=z[122]*z[88];
    z[114]=z[114] + z[122];
    z[122]=4*z[48];
    z[127]=z[122] + n<T>(1,3)*z[113];
    z[127]=z[10]*z[127]*z[88];
    z[114]=n<T>(1,3)*z[114] + z[127];
    z[114]=z[10]*z[114];
    z[127]=z[10]*z[6];
    z[100]= - z[100] + z[127];
    z[115]=z[115] + n<T>(1,3)*z[33];
    z[100]=z[115]*z[100];
    z[115]=z[129] - z[123];
    z[86]=z[115]*z[86];
    z[86]=z[86] + z[100];
    z[86]=z[86]*z[117];
    z[100]= - 59*z[24] + n<T>(32,3)*z[110];
    z[100]=z[100]*z[116];
    z[110]= - n<T>(17,3)*z[24] - 4*z[39];
    z[110]=z[6]*z[110];
    z[127]=z[39]*z[3];
    z[110]=z[110] + n<T>(4,3)*z[127];
    z[131]=2*z[82];
    z[110]=z[110]*z[131];
    z[40]=z[76] + z[86] + z[124] + z[114] + n<T>(8,9)*z[40] + z[110] + 
    z[100] - 8*z[127];
    z[40]=z[7]*z[40];
    z[76]=z[108]*z[41];
    z[86]=z[121]*z[76];
    z[100]=npow(z[99],3);
    z[110]=2*z[33];
    z[114]=z[12]*z[100]*z[110];
    z[86]=z[86] + z[114];
    z[86]=z[12]*z[86];
    z[114]=z[35]*z[6];
    z[86]= - 7*z[114] + z[86];
    z[124]=n<T>(1,3)*z[62];
    z[86]=z[86]*z[124];
    z[132]=2*z[87];
    z[133]=z[1]*z[132];
    z[133]=z[27] + z[133];
    z[133]=z[6]*z[133];
    z[133]= - 14*z[1] + z[133];
    z[133]=z[133]*z[116];
    z[135]=n<T>(2,3)*z[54];
    z[136]=z[24] + z[135];
    z[136]=z[136]*z[31];
    z[136]=n<T>(17,3)*z[1] + z[136];
    z[136]=z[136]*z[31];
    z[86]=z[86] + z[136] + static_cast<T>(4)+ z[133];
    z[133]=npow(z[6],3);
    z[136]=z[133]*z[12];
    z[137]=z[33]*z[41]*z[136];
    z[138]=z[56]*z[108];
    z[137]=z[138] + n<T>(2,3)*z[137];
    z[137]=z[12]*z[137];
    z[139]=2*z[25];
    z[140]=z[106] - z[139];
    z[137]=n<T>(11,3)*z[140] + z[137];
    z[137]=z[137]*z[124];
    z[140]=npow(z[3],3);
    z[141]=z[140]*z[11];
    z[142]=z[141]*z[94];
    z[143]=npow(z[3],2);
    z[144]=z[51]*z[143];
    z[145]=z[95]*z[144];
    z[146]=z[140]*z[1];
    z[147]=2*z[146];
    z[145]= - n<T>(2,3)*z[142] + z[147] + z[145];
    z[148]=n<T>(4,3)*z[11];
    z[145]=z[145]*z[148];
    z[149]= - z[1] - n<T>(2,3)*z[25];
    z[149]=z[149]*z[143];
    z[137]=z[145] + 4*z[149] + z[137];
    z[137]=z[11]*z[137];
    z[86]=n<T>(1,3)*z[86] + z[137];
    z[86]=z[11]*z[86];
    z[137]=7*z[24];
    z[77]= - n<T>(8,3)*z[77] - n<T>(4,3)*z[54] - z[137] - n<T>(25,3)*z[27];
    z[145]=z[143]*z[94];
    z[149]=z[12]*z[1];
    z[149]=npow(z[149],5)*z[141];
    z[145]=n<T>(2,3)*z[149] - z[147] - n<T>(7,3)*z[145];
    z[145]=z[11]*z[145];
    z[139]=z[23] + z[139];
    z[139]=z[139]*z[143];
    z[147]=z[51]*z[54];
    z[139]=z[145] + z[139] + n<T>(13,3)*z[147];
    z[139]=z[11]*z[139];
    z[145]=z[62]*z[24];
    z[147]=n<T>(2,9)*z[54];
    z[149]= - z[24] - z[147];
    z[149]=z[149]*z[31];
    z[149]= - n<T>(49,9)*z[1] + z[149];
    z[149]=z[3]*z[149];
    z[139]=n<T>(2,3)*z[139] + z[149] - n<T>(28,9)*z[145];
    z[139]=z[11]*z[139];
    z[149]=z[35]*z[62];
    z[61]=z[61] + z[147];
    z[61]=z[3]*z[61];
    z[61]=z[139] + n<T>(20,9)*z[149] + n<T>(50,9)*z[1] + z[61];
    z[61]=z[11]*z[61];
    z[61]=n<T>(1,3)*z[77] + z[61];
    z[61]=z[8]*z[61];
    z[71]=z[24] + z[71];
    z[71]=z[16]*z[71];
    z[77]=z[91]*z[132];
    z[71]=z[71] + z[77];
    z[71]=z[6]*z[71];
    z[77]=10*z[24];
    z[71]=z[71] + z[77] + 23*z[27];
    z[71]=z[6]*z[71];
    z[91]= - z[67] + 2*z[54];
    z[91]=z[91]*z[31];
    z[132]=2*z[16];
    z[139]= - z[1] + z[132];
    z[71]=z[91] + 4*z[139] + z[71];
    z[30]=z[30]*z[108];
    z[91]=z[121]*z[30];
    z[139]=z[133]*z[80];
    z[147]=2*z[139];
    z[150]=z[12]*z[147];
    z[91]=z[91] + z[150];
    z[91]=z[12]*z[91];
    z[91]=29*z[93] + z[91];
    z[91]=z[91]*z[124];
    z[150]=z[2]*z[24];
    z[71]=z[91] + n<T>(1,3)*z[71] + z[150];
    z[91]=n<T>(5,3)*z[27];
    z[150]= - z[24] - z[91];
    z[150]=z[16]*z[150];
    z[150]= - n<T>(1,9)*z[48] + z[150];
    z[150]=z[16]*z[150];
    z[38]=z[38] + z[104];
    z[87]=z[38]*z[87];
    z[87]=z[150] - n<T>(2,3)*z[87];
    z[87]=z[6]*z[87];
    z[150]=z[24] - n<T>(2,9)*z[27];
    z[150]=z[16]*z[150];
    z[87]=z[87] - n<T>(2,9)*z[48] + z[150];
    z[87]=z[6]*z[87];
    z[80]=z[108]*z[80];
    z[84]=z[84]*z[136];
    z[80]= - 7*z[80] - 2*z[84];
    z[80]=z[12]*z[80];
    z[52]= - 13*z[52] + z[80];
    z[52]=z[12]*z[52];
    z[52]= - 8*z[72] + z[52];
    z[52]=z[52]*z[62];
    z[80]=z[26] + z[91];
    z[52]=n<T>(1,9)*z[52] + n<T>(2,3)*z[80] + z[87];
    z[52]=z[9]*z[52];
    z[52]=2*z[61] + z[86] + n<T>(1,3)*z[71] + z[52];
    z[52]=z[13]*z[52];
    z[61]= - z[24] - z[39];
    z[61]=z[61]*z[88];
    z[71]=z[68] + z[48];
    z[80]=z[8]*z[71];
    z[84]= - z[8]*z[57];
    z[84]=z[89] + z[84];
    z[84]=z[7]*z[84];
    z[61]=z[84] + z[80] - z[39] + z[61];
    z[61]=z[7]*z[61];
    z[80]= - z[24] + z[39];
    z[80]=z[80]*z[88];
    z[84]= - z[9]*z[71];
    z[86]=z[9]*z[57];
    z[86]=z[89] + z[86];
    z[86]=z[4]*z[86];
    z[80]=z[86] + z[84] - z[39] + z[80];
    z[80]=z[4]*z[80];
    z[84]= - z[13]*z[97];
    z[61]=n<T>(1,3)*z[80] + n<T>(2,3)*z[61] + z[84] - z[102] + 3*z[106];
    z[61]=z[19]*z[61];
    z[52]=z[52] + z[61];
    z[61]=4*z[140];
    z[80]=z[59]*z[12];
    z[84]=z[33]*z[80]*z[61];
    z[86]=z[143]*z[57];
    z[84]= - 29*z[86] + z[84];
    z[84]=z[12]*z[84];
    z[84]=55*z[25] + z[84];
    z[84]=z[84]*z[124];
    z[87]=n<T>(47,3)*z[1] + 4*z[25];
    z[87]=z[87]*z[143];
    z[91]= - z[21]*z[61];
    z[84]=z[84] + z[87] + z[91];
    z[87]=z[48]*z[143];
    z[91]=z[12]*z[140]*z[42];
    z[87]= - n<T>(71,3)*z[87] + 8*z[91];
    z[87]=z[87]*z[51];
    z[91]=16*z[146];
    z[87]=8*z[142] - z[91] + z[87];
    z[87]=z[11]*z[87];
    z[84]=2*z[84] + z[87];
    z[84]=z[11]*z[84];
    z[87]=npow(z[82],2);
    z[95]= - z[12]*z[87]*z[95];
    z[95]=9*z[83] + z[95];
    z[95]=z[95]*z[62];
    z[124]=z[143]*z[21];
    z[136]= - 47*z[1] - 19*z[25];
    z[136]=z[3]*z[136];
    z[124]=17*z[124] - static_cast<T>(49)+ z[136];
    z[84]=n<T>(1,3)*z[84] + n<T>(2,9)*z[124] + z[95];
    z[84]=z[11]*z[84];
    z[95]=z[35] + z[68];
    z[95]=z[95]*z[36];
    z[95]=n<T>(23,3)*z[65] + z[95];
    z[124]=n<T>(4,3)*z[62];
    z[95]=z[95]*z[124];
    z[134]=n<T>(19,3)*z[24] + z[134];
    z[134]=z[134]*z[47];
    z[75]=z[35] + z[75];
    z[75]=z[2]*z[75];
    z[75]= - n<T>(8,3)*z[24] + z[75];
    z[136]=n<T>(4,3)*z[2];
    z[75]=z[75]*z[136];
    z[140]=n<T>(1,3)*z[21];
    z[150]= - z[27] - z[140];
    z[150]=z[10]*z[150];
    z[151]=n<T>(7,9)*z[1] + z[16];
    z[66]= - static_cast<T>(85)- 31*z[66];
    z[66]=z[14]*z[66];
    z[66]=4*z[84] + z[95] + 16*z[150] + z[75] + n<T>(4,9)*z[66] + 32*z[151]
    + z[134];
    z[66]=z[8]*z[66];
    z[75]= - z[48] + n<T>(2,3)*z[113];
    z[84]=n<T>(16,3)*z[6];
    z[75]=z[75]*z[84];
    z[75]=z[75] - z[24];
    z[75]=z[75]*z[6];
    z[95]=z[47]*z[33];
    z[95]=z[95] - z[55];
    z[134]=n<T>(8,3)*z[3];
    z[95]=z[95]*z[134];
    z[95]=z[95] - z[24];
    z[95]=z[95]*z[3];
    z[75]=z[75] - z[95];
    z[95]=z[20]*z[33];
    z[134]=z[95] + 3*z[48];
    z[134]=z[134]*z[3];
    z[150]=z[113] - z[48];
    z[150]=z[150]*z[6];
    z[134]=z[134] - z[150];
    z[150]= - z[10]*z[134];
    z[150]=z[150] + z[75];
    z[150]=z[10]*z[150];
    z[75]= - z[2]*z[75];
    z[75]=z[75] + z[150];
    z[75]=z[11]*z[75];
    z[150]= - z[48]*z[118];
    z[151]=z[56] + z[33];
    z[152]=z[118]*z[9];
    z[153]=z[151]*z[152];
    z[154]= - z[8]*z[56]*z[118];
    z[150]=z[154] + 2*z[150] + z[153];
    z[134]= - z[11]*z[134]*z[118];
    z[134]=z[134] + n<T>(4,3)*z[150];
    z[134]=z[5]*z[134];
    z[150]=z[69]*z[3];
    z[153]=z[150] + 5*z[33];
    z[154]=n<T>(2,9)*z[3];
    z[153]=z[153]*z[154];
    z[153]=z[153] - z[48];
    z[153]=z[153]*z[20];
    z[112]= - 32*z[112] + 47*z[33];
    z[154]=n<T>(1,3)*z[108];
    z[112]=z[112]*z[154];
    z[77]=z[112] - z[77];
    z[77]= - z[153] + n<T>(1,3)*z[77];
    z[112]=n<T>(8,3)*z[2];
    z[153]= - z[48]*z[112];
    z[153]=z[153] - z[77];
    z[153]=z[2]*z[153];
    z[77]=z[10]*z[77];
    z[151]=npow(z[2],2)*z[151]*z[9];
    z[154]= - z[56]*z[45];
    z[154]=z[35] + z[154];
    z[154]=z[2]*z[154];
    z[155]= - z[10]*z[35];
    z[154]=z[154] + z[155];
    z[154]=z[8]*z[154];
    z[75]=z[134] + 4*z[154] + z[75] + n<T>(4,3)*z[151] + z[153] + z[77];
    z[75]=z[5]*z[75];
    z[77]=z[47]*z[69];
    z[77]=z[77] + z[110];
    z[110]=z[77]*z[3];
    z[134]=z[143]*z[42];
    z[134]= - z[110] - n<T>(1,3)*z[134];
    z[134]=z[2]*z[134];
    z[151]=2*z[39];
    z[153]= - z[82]*z[151];
    z[134]=z[153] + z[134];
    z[77]=z[77]*z[20];
    z[95]= - z[95] + 29*z[48];
    z[153]=n<T>(1,3)*z[82];
    z[154]= - z[95]*z[153];
    z[77]=z[77] + z[154];
    z[123]=z[82]*z[123];
    z[77]=2*z[77] + 11*z[123];
    z[77]=z[10]*z[77];
    z[77]=8*z[134] + z[77];
    z[123]=z[82]*z[33];
    z[123]=z[123] + z[150];
    z[123]= - z[123]*z[118];
    z[81]=z[81]*z[152];
    z[134]=z[9] - z[3];
    z[109]=z[5]*z[109]*z[134];
    z[81]=z[109] + z[81] + z[123];
    z[81]=z[4]*z[81];
    z[89]=z[89] + n<T>(4,3)*z[129];
    z[109]= - z[125] + n<T>(29,3)*z[57];
    z[109]=2*z[109] - 11*z[126];
    z[107]=z[109]*z[107];
    z[89]=4*z[89] + z[107];
    z[89]=z[9]*z[89];
    z[103]=z[47]*z[103];
    z[69]=z[103] + 2*z[69];
    z[69]=z[69]*z[3];
    z[103]= - z[69]*z[118];
    z[79]=z[119]*z[79];
    z[79]=z[79] + z[103];
    z[79]=z[79]*z[117];
    z[77]=n<T>(8,9)*z[81] + z[79] + n<T>(1,3)*z[77] + z[89];
    z[77]=z[4]*z[77];
    z[79]=z[104] + z[110];
    z[79]=z[3]*z[79];
    z[81]=z[33]*z[143];
    z[81]=z[24] + z[81];
    z[81]=z[81]*z[153];
    z[79]=z[79] + z[81];
    z[79]=z[79]*z[136];
    z[81]=n<T>(23,3)*z[24] + z[151];
    z[81]=z[3]*z[81];
    z[89]=z[137] - n<T>(4,3)*z[39];
    z[89]=z[6]*z[89];
    z[89]=z[89] + n<T>(8,3)*z[127];
    z[89]=z[89]*z[82];
    z[79]=z[79] + n<T>(4,3)*z[81] + z[89];
    z[39]= - n<T>(46,9)*z[24] - z[39];
    z[70]= - z[70] + z[151];
    z[70]=z[70]*z[88];
    z[71]=z[2]*z[71];
    z[39]= - n<T>(4,9)*z[71] + 2*z[39] + n<T>(1,3)*z[70];
    z[70]=n<T>(73,3)*z[48];
    z[71]=z[70] - 22*z[68];
    z[71]=n<T>(1,3)*z[71] + z[111];
    z[71]=z[10]*z[71];
    z[39]=2*z[39] + z[71];
    z[39]=z[9]*z[39];
    z[70]= - z[70] - 8*z[110];
    z[70]=z[3]*z[70];
    z[71]=z[95]*z[47];
    z[71]=z[74] + z[71];
    z[71]=z[71]*z[131];
    z[70]=z[70] + z[71];
    z[71]= - z[122] - n<T>(11,3)*z[49];
    z[71]=z[10]*z[71]*z[82];
    z[70]=n<T>(1,3)*z[70] + z[71];
    z[70]=z[10]*z[70];
    z[69]=z[69] + n<T>(7,3)*z[33];
    z[69]=z[3]*z[69]*z[118];
    z[71]=z[9]*z[115];
    z[69]=n<T>(7,3)*z[71] + z[69];
    z[69]=z[69]*z[117];
    z[39]=z[77] + z[69] + z[39] + 2*z[79] + z[70];
    z[39]=z[4]*z[39];
    z[28]= - z[16]*z[28];
    z[28]=z[55] + z[28];
    z[28]=z[28]*z[84];
    z[28]=z[28] - n<T>(71,3)*z[24] + 10*z[27];
    z[28]=z[6]*z[28];
    z[28]=z[29] + z[28];
    z[28]=z[6]*z[28];
    z[69]=z[24] - z[135];
    z[69]=z[69]*z[31];
    z[69]= - z[102] + z[69];
    z[69]=z[69]*z[31];
    z[70]= - n<T>(1,3)*z[25] - 2*z[1];
    z[70]=z[14]*z[70]*z[143];
    z[28]=8*z[70] + z[69] + static_cast<T>(52)+ z[28];
    z[28]=z[28]*z[45];
    z[45]=n<T>(4,3)*z[48];
    z[69]=z[76]*z[45];
    z[70]=5*z[48];
    z[71]=z[87]*z[70];
    z[69]=z[69] + z[71];
    z[69]=z[2]*z[69];
    z[71]=n<T>(4,3)*z[33];
    z[74]=z[100]*z[46]*z[71];
    z[69]=z[69] + z[74];
    z[69]=z[12]*z[69];
    z[74]=13*z[114] + n<T>(8,3)*z[83];
    z[74]=z[2]*z[74];
    z[69]=z[74] + z[69];
    z[69]=z[69]*z[124];
    z[74]= - z[138] - n<T>(4,3)*z[86];
    z[74]=z[2]*z[74];
    z[77]= - z[41]*z[133];
    z[59]= - z[59]*z[61];
    z[59]=z[77] + z[59];
    z[46]=n<T>(2,3)*z[46];
    z[33]=z[46]*z[33]*z[59];
    z[33]=z[74] + z[33];
    z[33]=z[12]*z[33];
    z[59]=55*z[24];
    z[61]=z[143]*z[59];
    z[74]= - 11*z[106] - 10*z[25];
    z[74]=z[2]*z[74];
    z[61]=z[61] + z[74];
    z[33]=n<T>(1,3)*z[61] + z[33];
    z[33]=z[33]*z[62];
    z[61]=z[141]*z[51]*z[48];
    z[33]= - 8*z[61] + z[91] + z[33];
    z[33]=z[33]*z[148];
    z[61]= - 3*z[1] - n<T>(11,9)*z[25];
    z[61]=z[61]*z[31];
    z[61]= - n<T>(71,9) + z[61];
    z[61]=z[61]*z[20];
    z[28]=z[33] + z[69] + z[61] + z[28];
    z[28]=z[11]*z[28];
    z[33]= - z[80]*z[85];
    z[33]=z[33] + z[35] - z[68];
    z[33]=z[33]*z[63];
    z[61]= - z[41]*z[51]*z[55];
    z[61]=11*z[27] + z[61];
    z[61]=z[9]*z[61];
    z[33]=z[61] + z[33] - z[50] + 11*z[64];
    z[44]= - z[44]*z[78]*z[49];
    z[32]=z[1] + z[32];
    z[32]=z[14]*z[32];
    z[32]= - z[27] + z[32];
    z[50]=z[53]*z[96];
    z[32]=z[50] + n<T>(1,3)*z[32] + z[44];
    z[32]=z[32]*z[43];
    z[32]=n<T>(1,3)*z[33] + z[32];
    z[32]=z[15]*z[32];
    z[33]= - z[83]*z[98];
    z[43]=z[23] - 4*z[149];
    z[43]=z[9]*z[43];
    z[44]=z[3]*z[23];
    z[32]=2*z[32] + n<T>(1,9)*z[43] + z[33] - n<T>(8,9) + z[44];
    z[32]=z[15]*z[32];
    z[33]=z[48]*z[144];
    z[33]=n<T>(1,3)*z[142] - z[146] - n<T>(2,3)*z[33];
    z[43]=2*z[11];
    z[33]=z[33]*z[43];
    z[44]=z[25]*z[62];
    z[50]=n<T>(4,3)*z[1];
    z[25]=z[50] + z[25];
    z[25]=z[25]*z[143];
    z[25]=z[33] + 2*z[25] + n<T>(5,3)*z[44];
    z[25]=z[25]*z[43];
    z[33]= - z[26] - z[54];
    z[33]=z[33]*z[20];
    z[33]= - 17*z[1] + z[33];
    z[33]=z[3]*z[33];
    z[23]=z[23] - z[149];
    z[23]=z[9]*z[23];
    z[23]=z[23] + static_cast<T>(1)+ z[33];
    z[23]=n<T>(1,3)*z[23] + z[25];
    z[23]=z[13]*z[23];
    z[25]= - z[51]*z[56]*z[53];
    z[33]=z[49]*z[80];
    z[33]=z[33] + z[56] - z[57];
    z[33]=z[33]*z[51];
    z[41]= - z[9]*z[41]*z[94];
    z[33]=z[33] + z[41];
    z[33]=z[15]*z[33];
    z[41]=z[12]*z[128];
    z[41]= - z[97] + z[41];
    z[41]=z[41]*z[62];
    z[25]=n<T>(2,3)*z[33] + z[41] + z[25];
    z[25]=z[15]*z[25];
    z[33]= - z[26]*z[62]*z[9];
    z[33]=5*z[44] + z[33];
    z[25]=n<T>(1,9)*z[33] + z[25];
    z[25]=z[15]*z[25];
    z[33]= - z[9]*z[145];
    z[33]=z[44] + z[33];
    z[33]=z[13]*z[33];
    z[25]=z[25] + n<T>(2,9)*z[33];
    z[25]=z[18]*z[25];
    z[23]=4*z[25] + n<T>(4,3)*z[23] - z[20] + z[32];
    z[23]=z[18]*z[23];
    z[25]=53*z[24] - 22*z[27];
    z[25]=z[16]*z[25];
    z[25]=79*z[48] + z[25];
    z[32]=z[24] - z[101];
    z[32]=z[16]*z[32];
    z[32]=z[45] + z[32];
    z[32]=z[16]*z[32];
    z[32]= - z[71] + z[32];
    z[32]=z[32]*z[120];
    z[25]=n<T>(1,3)*z[25] + z[32];
    z[25]=z[25]*z[116];
    z[25]=z[25] - z[97] - 14*z[27];
    z[25]=z[6]*z[25];
    z[32]= - z[70] - z[49];
    z[32]=z[32]*z[31];
    z[32]=z[24] + z[32];
    z[32]=z[3]*z[32];
    z[33]=z[24] - z[54];
    z[20]=z[33]*z[20];
    z[20]=77*z[1] + z[20];
    z[20]=z[3]*z[20];
    z[20]=static_cast<T>(44)+ z[20];
    z[20]=z[14]*z[20];
    z[33]=z[21] + z[24];
    z[41]=z[33]*z[112];
    z[43]= - 10*z[1] - n<T>(97,3)*z[16];
    z[20]=z[41] + n<T>(2,9)*z[20] + n<T>(4,9)*z[32] + n<T>(4,3)*z[43] + z[25];
    z[20]=z[2]*z[20];
    z[25]=z[26]*z[60];
    z[25]=z[25] - 32*z[93] - 13*z[65];
    z[25]=z[2]*z[25];
    z[32]=z[100]*z[55];
    z[41]= - z[2]*z[104]*z[30];
    z[43]= - z[139]*z[46];
    z[32]=z[43] + z[32] + z[41];
    z[32]=z[12]*z[32];
    z[41]=z[108]*z[72];
    z[25]=z[32] + n<T>(7,3)*z[41] + z[25];
    z[25]=z[25]*z[124];
    z[32]= - 61*z[24] - 86*z[27];
    z[32]=z[16]*z[32];
    z[32]= - 16*z[48] + z[32];
    z[41]=z[38]*z[99];
    z[32]=n<T>(1,3)*z[32] - 16*z[41];
    z[32]=z[6]*z[32];
    z[32]=z[32] + n<T>(169,3)*z[24] + 38*z[27];
    z[32]=z[6]*z[32];
    z[41]= - 25*z[24] - 26*z[27];
    z[38]= - z[38]*z[120];
    z[38]=n<T>(1,3)*z[41] + z[38];
    z[38]=z[6]*z[38];
    z[38]=n<T>(82,3)*z[1] + z[38];
    z[38]=z[38]*z[88];
    z[21]=z[37] + z[21];
    z[21]= - z[14]*z[21];
    z[21]=z[21] - z[48] - z[35];
    z[21]=z[2]*z[21];
    z[21]=z[21] + z[33];
    z[21]=z[2]*z[21];
    z[33]=43*z[1];
    z[41]=z[33] + z[132];
    z[21]=4*z[21] + z[38] + n<T>(2,3)*z[41] + z[32];
    z[32]= - z[100]*z[42];
    z[32]= - z[147] + z[32];
    z[38]=2*z[12];
    z[32]=z[32]*z[38];
    z[30]=z[48]*z[30];
    z[38]= - z[57]*z[76];
    z[30]=z[32] - 8*z[30] + z[38];
    z[30]=z[30]*z[58];
    z[32]= - 7*z[93] - 2*z[92];
    z[35]= - z[35]*z[36];
    z[30]=z[30] + n<T>(1,3)*z[32] + z[35];
    z[30]=z[30]*z[124];
    z[32]= - z[140] + z[37];
    z[32]=z[32]*z[105];
    z[21]=z[30] + n<T>(1,3)*z[21] + z[32];
    z[21]=z[9]*z[21];
    z[30]= - z[97] + z[90];
    z[30]=z[16]*z[30];
    z[30]= - z[45] + z[30];
    z[30]=z[30]*z[120];
    z[27]=z[30] - n<T>(79,3)*z[24] + 20*z[27];
    z[27]=z[6]*z[27];
    z[30]= - z[33] + 88*z[16];
    z[27]=n<T>(2,3)*z[30] + z[27];
    z[27]=z[6]*z[27];
    z[30]= - z[50] + z[106];
    z[32]=z[59] - z[130];
    z[32]=z[32]*z[47];
    z[30]=8*z[30] + z[32];
    z[30]=z[30]*z[31];
    z[27]=40*z[82] + z[30] + n<T>(140,3) + z[27];
    z[30]= - z[121] - z[113];
    z[30]=z[30]*z[116];
    z[31]=z[73] + 23*z[49];
    z[31]=z[31]*z[47];
    z[26]=z[31] + z[26] + z[30];
    z[26]=z[10]*z[26];
    z[30]=z[121] + 16*z[113];
    z[30]=z[6]*z[30];
    z[30]=z[67] + n<T>(1,9)*z[30];
    z[30]=z[6]*z[30];
    z[29]=z[29] + z[30];
    z[30]= - 67*z[48] + 16*z[49];
    z[30]=z[3]*z[30];
    z[24]=18*z[24] + n<T>(1,9)*z[30];
    z[24]=z[3]*z[24];
    z[24]=z[26] + 2*z[29] + z[24];
    z[24]=z[10]*z[24];

    r += z[20] + z[21] + z[22] + 2*z[23] + z[24] + z[25] + n<T>(1,3)*z[27]
       + z[28] + z[34] + z[39] + z[40] + 4*z[52] + z[66] + z[75];
 
    return r;
}

template double qqb_2lNLC_r1174(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1174(const std::array<dd_real,31>&);
#endif
