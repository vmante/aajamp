#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1204(const std::array<T,31>& k) {
  T z[158];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[17];
    z[5]=k[19];
    z[6]=k[12];
    z[7]=k[22];
    z[8]=k[13];
    z[9]=k[14];
    z[10]=k[11];
    z[11]=k[5];
    z[12]=k[6];
    z[13]=k[8];
    z[14]=k[15];
    z[15]=k[9];
    z[16]=k[16];
    z[17]=k[2];
    z[18]=k[3];
    z[19]=k[4];
    z[20]=2*z[1];
    z[21]=npow(z[11],2);
    z[22]=z[20]*z[21];
    z[23]=z[8] - z[2];
    z[24]=z[22]*z[23];
    z[25]=z[11]*z[1];
    z[26]=5*z[25];
    z[27]=npow(z[1],2);
    z[28]=7*z[27];
    z[29]=z[26] + z[28];
    z[29]=z[29]*z[11];
    z[30]=npow(z[1],3);
    z[31]=3*z[30];
    z[29]=z[29] + z[31];
    z[32]=z[29]*z[6];
    z[29]= - z[10]*z[29];
    z[24]=z[29] + z[32] + z[24];
    z[24]=z[19]*z[24];
    z[29]=4*z[27];
    z[33]=z[32] + z[29] + z[26];
    z[33]=z[21]*z[33];
    z[34]=z[30]*z[15];
    z[35]=z[34]*z[21];
    z[33]=z[35] + z[33];
    z[33]=z[2]*z[33];
    z[36]=npow(z[1],4);
    z[37]=2*z[36];
    z[38]=z[30]*z[11];
    z[39]=z[37] + z[38];
    z[40]=z[39]*z[11];
    z[41]=npow(z[1],5);
    z[40]=z[40] + z[41];
    z[40]=z[40]*z[15];
    z[42]=3*z[25];
    z[43]=z[27] - z[42];
    z[43]=z[11]*z[43];
    z[44]=7*z[30];
    z[43]=z[44] + z[43];
    z[43]=z[11]*z[43];
    z[45]=4*z[36];
    z[43]= - z[40] + z[45] + z[43];
    z[43]=z[10]*z[43];
    z[46]=2*z[27];
    z[47]=z[42] + z[46];
    z[48]=z[47]*z[21];
    z[49]=5*z[27];
    z[50]=4*z[25];
    z[51]= - z[49] - z[50];
    z[51]=z[11]*z[51];
    z[52]=2*z[30];
    z[51]= - z[52] + z[51];
    z[53]=z[21]*z[10];
    z[51]=z[51]*z[53];
    z[48]=z[48] + z[51];
    z[48]=z[8]*z[48];
    z[32]= - z[11]*z[32];
    z[51]=z[36]*z[15];
    z[24]=z[24] + z[48] + z[43] + z[33] + z[51] + z[32];
    z[32]=2*z[13];
    z[24]=z[24]*z[32];
    z[33]=z[27]*z[11];
    z[43]=z[33] - z[30];
    z[48]=z[43]*z[11];
    z[48]=z[48] - z[35];
    z[48]=z[48]*z[15];
    z[54]=z[50] + z[27];
    z[55]=4*z[11];
    z[56]= - z[54]*z[55];
    z[57]=17*z[25];
    z[58]=13*z[27];
    z[59]=z[57] + z[58];
    z[59]=z[59]*z[11];
    z[59]=z[59] + z[30];
    z[59]=z[59]*z[6];
    z[60]= - z[11]*z[59];
    z[56]=z[60] + z[56] + z[48];
    z[56]=z[2]*z[56];
    z[60]=z[31] - z[33];
    z[60]=z[11]*z[60];
    z[60]=z[40] + z[45] + z[60];
    z[60]=z[15]*z[60];
    z[61]= - z[58] + z[26];
    z[61]=z[11]*z[61];
    z[60]=z[60] - 15*z[30] + z[61];
    z[60]=z[10]*z[60];
    z[61]=6*z[1];
    z[62]= - z[21]*z[61];
    z[47]=z[47]*z[53];
    z[47]=z[62] + 5*z[47];
    z[47]=z[8]*z[47];
    z[62]=2*z[25];
    z[63]= - z[62]*z[23];
    z[64]=3*z[27];
    z[65]=z[50] + z[64];
    z[66]=z[10] - z[6];
    z[67]=z[65]*z[66];
    z[63]=z[67] + z[63];
    z[67]=2*z[19];
    z[63]=z[63]*z[67];
    z[68]=4*z[30];
    z[69]= - z[68] - z[51];
    z[69]=z[15]*z[69];
    z[24]=z[24] + z[63] + z[47] + z[60] + z[56] + z[69] + z[59];
    z[24]=z[24]*z[32];
    z[47]=z[55]*z[27];
    z[56]= - n<T>(5,3)*z[30] - z[47];
    z[56]=z[11]*z[56];
    z[56]=z[56] + n<T>(8,3)*z[35];
    z[56]=z[15]*z[56];
    z[59]=z[21]*z[1];
    z[60]=n<T>(2,3)*z[30] + z[59];
    z[56]=4*z[60] + z[56];
    z[56]=z[15]*z[56];
    z[60]=9*z[25];
    z[63]=z[46] + z[60];
    z[69]=23*z[27];
    z[70]=37*z[25];
    z[71]=z[69] + z[70];
    z[71]=z[11]*z[71];
    z[72]=8*z[30];
    z[71]=z[72] + z[71];
    z[71]=z[6]*z[71];
    z[56]=z[71] + 2*z[63] + z[56];
    z[56]=z[2]*z[56];
    z[63]=17*z[27];
    z[70]= - z[63] - z[70];
    z[70]=z[11]*z[70];
    z[70]= - n<T>(16,3)*z[30] + z[70];
    z[70]=z[10]*z[70];
    z[71]=z[30]*z[3];
    z[73]=z[30]*z[2];
    z[70]=z[71] + z[70] + 6*z[25] - z[73];
    z[70]=z[8]*z[70];
    z[74]=z[30] + n<T>(2,3)*z[51];
    z[74]=z[15]*z[74];
    z[74]=z[27] + z[74];
    z[75]=2*z[15];
    z[74]=z[74]*z[75];
    z[60]= - z[49] - z[60];
    z[60]=z[6]*z[60];
    z[60]=z[74] + z[60];
    z[74]=n<T>(13,3)*z[30];
    z[76]=z[74] + z[47];
    z[76]=z[11]*z[76];
    z[40]= - n<T>(8,3)*z[40] + n<T>(1,3)*z[36] + z[76];
    z[40]=z[15]*z[40];
    z[22]= - z[74] - z[22];
    z[22]=2*z[22] + z[40];
    z[22]=z[15]*z[22];
    z[40]=z[64] - z[25];
    z[22]=6*z[40] + z[22];
    z[22]=z[10]*z[22];
    z[74]=4*z[1];
    z[76]=z[27]*z[2];
    z[77]= - z[3]*z[27];
    z[77]=z[77] + z[74] + z[76];
    z[77]=z[8]*z[77];
    z[78]=3*z[1];
    z[66]= - z[78]*z[66];
    z[79]= - z[2]*z[20];
    z[66]=z[79] + z[66];
    z[79]=z[76]*z[3];
    z[66]=z[77] + 2*z[66] + z[79];
    z[66]=z[19]*z[66];
    z[77]= - z[3]*z[73];
    z[22]=z[24] + z[66] + z[70] + z[77] + z[22] + 2*z[60] + z[56];
    z[22]=z[13]*z[22];
    z[24]=z[19]*z[3];
    z[56]=z[24]*z[2];
    z[60]=z[20]*z[56];
    z[66]=z[2]*z[1];
    z[70]= - z[1] - z[76];
    z[70]=z[3]*z[70];
    z[60]=z[60] + 7*z[66] + 4*z[70];
    z[60]=z[19]*z[60];
    z[70]=11*z[27];
    z[77]= - z[70] + 8*z[25];
    z[77]=z[2]*z[77];
    z[80]=z[46] + z[73];
    z[81]=z[3]*z[80];
    z[60]=z[60] + 2*z[81] - z[61] + z[77];
    z[60]=z[19]*z[60];
    z[77]=10*z[27];
    z[81]= - z[77] + z[42];
    z[81]=z[11]*z[81];
    z[82]=5*z[30];
    z[81]=z[82] + z[81];
    z[81]=z[2]*z[81];
    z[83]= - z[20]*z[53];
    z[60]=z[60] + z[83] + z[29] + z[81];
    z[60]=z[19]*z[60];
    z[81]=3*z[33];
    z[83]=z[68] - z[81];
    z[83]=z[11]*z[83];
    z[83]=z[83] + z[35];
    z[83]=z[2]*z[83];
    z[84]=z[29] - z[25];
    z[84]=z[84]*z[21];
    z[84]=z[84] - z[35];
    z[84]=z[10]*z[84];
    z[85]=z[21]*z[27];
    z[86]=z[25] - z[27];
    z[87]=z[86]*z[11];
    z[88]=z[87] - z[30];
    z[53]=z[88]*z[53];
    z[53]=z[85] + z[53];
    z[53]=z[9]*z[53];
    z[53]=z[60] + z[53] + z[84] - z[33] + z[83];
    z[60]=2*z[14];
    z[53]=z[53]*z[60];
    z[83]=z[64]*z[2];
    z[84]= - z[74] - z[83];
    z[84]=z[3]*z[84];
    z[89]=z[78]*z[56];
    z[90]=8*z[1];
    z[91]=z[2]*z[90];
    z[84]=z[89] + z[91] + z[84];
    z[84]=z[84]*z[67];
    z[89]= - 12*z[27] + 11*z[25];
    z[89]=z[2]*z[89];
    z[91]= - z[10]*z[50];
    z[92]=z[3]*z[46];
    z[84]=z[84] + z[92] + z[91] - z[90] + z[89];
    z[84]=z[19]*z[84];
    z[89]= - z[49] + z[25];
    z[89]=z[11]*z[89];
    z[48]=z[89] + z[48];
    z[48]=z[2]*z[48];
    z[89]= - z[82] - z[33];
    z[89]=z[11]*z[89];
    z[35]=z[89] + z[35];
    z[35]=z[15]*z[35];
    z[89]=z[28] - z[62];
    z[91]=2*z[11];
    z[89]=z[89]*z[91];
    z[35]=z[89] + z[35];
    z[35]=z[10]*z[35];
    z[89]=z[49] - z[62];
    z[89]=z[11]*z[89];
    z[92]=5*z[11];
    z[88]=z[10]*z[88]*z[92];
    z[88]=z[89] + z[88];
    z[88]=z[9]*z[88];
    z[35]=z[53] + z[84] + z[88] + z[35] - 2*z[40] + z[48];
    z[35]=z[35]*z[60];
    z[40]=z[34]*z[11];
    z[48]=z[40] + z[30] + z[47];
    z[48]=z[15]*z[48];
    z[53]=z[33] + z[30];
    z[84]= - z[6]*z[53];
    z[88]=13*z[25];
    z[48]=z[84] + z[48] - z[28] + z[88];
    z[48]=z[2]*z[48];
    z[84]=z[2] - z[6];
    z[53]= - z[53]*z[84];
    z[89]=z[64] + z[62];
    z[93]= - z[49] - z[25];
    z[93]=z[93]*z[92];
    z[93]= - n<T>(46,3)*z[30] + z[93];
    z[93]=z[10]*z[93];
    z[53]=z[93] + 4*z[89] + z[53];
    z[53]=z[9]*z[53];
    z[89]= - z[20] - z[76];
    z[89]=z[3]*z[89];
    z[56]=z[90]*z[56];
    z[93]= - z[10]*z[20];
    z[56]=z[56] + 6*z[89] + z[93] + static_cast<T>(2)+ 15*z[66];
    z[56]=z[56]*z[67];
    z[66]=z[77] - z[25];
    z[77]=z[91]*z[27];
    z[89]= - z[82] - z[77];
    z[40]=2*z[89] + 3*z[40];
    z[40]=z[15]*z[40];
    z[40]=2*z[66] + z[40];
    z[40]=z[10]*z[40];
    z[66]=z[46]*z[15];
    z[89]=z[66] - z[61] + z[11];
    z[93]=z[27] + n<T>(2,3)*z[73];
    z[94]=8*z[3];
    z[93]=z[93]*z[94];
    z[35]=z[35] + z[56] + z[93] + z[53] + z[40] + 2*z[89] + z[48];
    z[35]=z[14]*z[35];
    z[40]= - z[21]*z[73];
    z[48]=z[52]*z[21];
    z[53]=z[10]*z[48];
    z[56]=z[38]*z[2];
    z[89]=z[19]*z[73];
    z[89]= - 5*z[56] - 4*z[89];
    z[89]=z[19]*z[89];
    z[40]=z[89] + z[40] + z[53];
    z[40]=z[19]*z[40];
    z[53]=npow(z[11],3);
    z[89]=z[53]*z[30];
    z[93]=z[89]*z[9];
    z[95]=z[30]*z[10];
    z[96]=z[95]*z[53];
    z[93]= - z[96] - z[93];
    z[97]=z[21]*z[30];
    z[98]=z[19]*z[52];
    z[98]=z[38] + z[98];
    z[98]=z[19]*z[98];
    z[98]= - z[97] + z[98];
    z[98]=z[19]*z[98];
    z[98]=z[89] + z[98];
    z[98]=z[98]*z[60];
    z[40]=z[98] + 3*z[93] + z[40];
    z[40]=z[14]*z[40];
    z[93]=5*z[9];
    z[98]=z[96]*z[93];
    z[40]=z[98] + z[40];
    z[40]=z[40]*z[60];
    z[98]=z[36]*z[6];
    z[99]=z[36]*z[10];
    z[100]=z[36]*z[2];
    z[101]=z[8]*z[36];
    z[101]=z[101] - z[99] + z[98] - z[100];
    z[101]=z[19]*z[53]*z[101];
    z[84]=z[8] + z[84];
    z[102]=npow(z[25],4);
    z[84]=z[102]*z[84];
    z[103]=z[102]*z[10];
    z[84]=z[101] - z[103] + z[84];
    z[84]=z[84]*z[32];
    z[101]=z[2]*z[6];
    z[104]= - z[102]*z[101];
    z[105]=z[8]*z[103];
    z[104]=z[104] + z[105];
    z[84]=5*z[104] + z[84];
    z[84]=z[84]*npow(z[13],2);
    z[104]=z[53]*z[99];
    z[105]= - z[91] - z[19];
    z[105]=z[19]*z[105];
    z[105]= - z[21] + z[105];
    z[105]=z[19]*z[100]*z[105];
    z[104]=z[104] + z[105];
    z[104]=z[19]*z[104];
    z[102]= - z[9]*z[102];
    z[102]=z[104] - z[103] + z[102];
    z[102]=z[102]*z[60];
    z[93]=z[103]*z[93];
    z[93]=z[93] + z[102];
    z[102]=npow(z[14],2);
    z[93]=z[93]*z[102];
    z[103]= - z[8]*z[10];
    z[101]=z[101] + z[103];
    z[101]=npow(z[13],3)*z[101];
    z[103]=z[9]*npow(z[14],3);
    z[104]=z[10]*z[103];
    z[101]=z[101] + z[104];
    z[104]=2*z[12];
    z[101]=z[104]*z[101]*npow(z[25],5);
    z[84]=z[101] + z[84] + z[93];
    z[84]=z[84]*z[104];
    z[93]=npow(z[6],2);
    z[101]=z[93]*z[97];
    z[104]=z[73]*z[6];
    z[105]=z[21]*z[104];
    z[105]= - z[101] - 8*z[105];
    z[105]=z[2]*z[105];
    z[106]=npow(z[2],2);
    z[107]=z[30]*z[6];
    z[108]=z[18]*z[106]*z[55]*z[107];
    z[105]=z[105] + z[108];
    z[105]=z[18]*z[105];
    z[108]= - z[48]*z[23];
    z[109]= - z[21]*z[107];
    z[110]=z[10]*z[97];
    z[108]=z[110] + z[109] + z[108];
    z[67]=z[108]*z[67];
    z[108]=z[107] - z[73];
    z[109]= - z[8]*z[31];
    z[108]=z[109] + 5*z[108];
    z[108]=z[53]*z[108];
    z[67]=z[67] + 3*z[96] + z[108];
    z[67]=z[13]*z[67];
    z[108]=z[104]*z[53];
    z[96]= - z[8]*z[96];
    z[96]=z[108] + z[96];
    z[67]=n<T>(19,3)*z[96] + z[67];
    z[67]=z[67]*z[32];
    z[89]=z[89]*z[93];
    z[96]=z[89] + 4*z[108];
    z[96]=z[2]*z[96];
    z[101]=z[9]*z[101];
    z[108]=npow(z[18],2);
    z[109]=z[108]*z[71];
    z[110]=z[106]*z[109];
    z[101]=z[101] - 4*z[110];
    z[101]=z[19]*z[101];
    z[89]=z[9]*z[89];
    z[40]=z[84] + z[40] + z[67] + z[101] + z[89] + z[96] + z[105];
    z[40]=z[12]*z[40];
    z[67]=z[85]*z[15];
    z[84]=z[85]*z[6];
    z[89]=n<T>(4,3)*z[67] - z[84];
    z[89]=z[6]*z[89];
    z[96]=z[2]*z[85];
    z[96]= - n<T>(10,3)*z[84] + z[96];
    z[101]=2*z[2];
    z[96]=z[96]*z[101];
    z[105]=n<T>(1,3)*z[6];
    z[110]=z[27]*z[15];
    z[105]=z[110]*z[105];
    z[111]=n<T>(1,3)*z[27];
    z[112]= - z[6]*z[111];
    z[112]=z[112] + z[76];
    z[112]=z[2]*z[112];
    z[105]=z[105] + z[112];
    z[105]=z[18]*z[105];
    z[112]=z[33]*z[6];
    z[113]= - z[2]*z[33];
    z[113]=z[112] + z[113];
    z[113]=z[2]*z[113];
    z[114]=z[15]*z[112];
    z[113]= - n<T>(1,3)*z[114] + z[113];
    z[105]=2*z[113] + z[105];
    z[113]=2*z[18];
    z[105]=z[105]*z[113];
    z[114]=z[85]*z[10];
    z[115]=2*z[114];
    z[116]=z[84] + z[115];
    z[116]=z[9]*z[116];
    z[67]= - z[67] + z[115];
    z[117]=z[91] - z[18];
    z[117]=z[18]*z[110]*z[117];
    z[67]=2*z[67] + z[117];
    z[67]=z[8]*z[67];
    z[117]=z[46]*z[2];
    z[108]=z[108]*z[3];
    z[118]=z[108]*z[117];
    z[67]=n<T>(2,3)*z[67] + z[118] + z[116] + z[105] + z[89] + z[96];
    z[89]=z[33]*z[10];
    z[96]=z[89] - z[112];
    z[23]=z[81]*z[23];
    z[23]=z[23] - z[96];
    z[23]=z[19]*z[23];
    z[105]=z[8] + 5*z[2];
    z[105]=z[85]*z[105];
    z[23]=z[23] - z[115] - 4*z[84] + z[105];
    z[23]=z[23]*z[32];
    z[32]= - z[9]*z[85];
    z[32]= - z[114] + z[32];
    z[105]= - z[91]*z[76];
    z[83]= - z[19]*z[83];
    z[83]=z[83] + z[105] + z[89];
    z[83]=z[19]*z[83];
    z[105]=z[27]*z[19];
    z[114]=z[33] + 6*z[105];
    z[114]=z[19]*z[114];
    z[114]=z[85] + z[114];
    z[114]=z[14]*z[114];
    z[32]=z[114] + 2*z[32] + z[83];
    z[32]=z[32]*z[60];
    z[60]=z[46]*z[18];
    z[60]=z[60] - z[33];
    z[83]=z[106]*z[60];
    z[96]= - z[9]*z[96];
    z[106]=4*z[18];
    z[79]= - z[106]*z[79];
    z[89]=z[8]*z[89];
    z[79]=n<T>(7,3)*z[89] + z[79] + 4*z[83] + z[96];
    z[79]=z[19]*z[79];
    z[23]=z[40] + z[32] + z[23] + 2*z[67] + z[79];
    z[32]=npow(z[12],2);
    z[23]=z[23]*z[32];
    z[40]=z[27] - z[50];
    z[40]=z[15]*z[11]*z[40];
    z[40]=z[40] - n<T>(7,3)*z[27] - 10*z[25];
    z[40]=z[15]*z[40];
    z[40]=z[20] + z[40];
    z[67]=z[25] + z[27];
    z[79]=4*z[15];
    z[83]=z[79]*z[21];
    z[89]= - z[67]*z[83];
    z[96]=8*z[27];
    z[114]= - z[96] - z[88];
    z[114]=z[11]*z[114];
    z[89]=z[114] + z[89];
    z[89]=z[15]*z[89];
    z[114]=n<T>(5,2)*z[27];
    z[89]=z[114] + z[89];
    z[89]=z[6]*z[89];
    z[115]=z[27] - n<T>(8,3)*z[34];
    z[115]=2*z[115] - n<T>(7,6)*z[107];
    z[115]=z[10]*z[115];
    z[40]=z[115] + 4*z[40] + z[89];
    z[89]=npow(z[15],2);
    z[40]=z[6]*z[89]*z[40];
    z[115]=npow(z[15],4);
    z[116]= - z[115]*z[42];
    z[118]= - z[15]*z[59];
    z[111]=z[111] + z[118];
    z[118]=npow(z[15],3);
    z[111]=z[6]*z[111]*z[118];
    z[111]=z[116] + z[111];
    z[111]=z[6]*z[111];
    z[116]=z[118]*z[93]*z[95];
    z[119]= - z[115]*z[78];
    z[111]= - n<T>(1,3)*z[116] + z[119] + z[111];
    z[111]=z[111]*z[106];
    z[116]=z[46] - z[42];
    z[116]=z[116]*z[75];
    z[119]=11*z[1];
    z[116]= - z[119] + z[116];
    z[120]=2*z[118];
    z[116]=z[116]*z[120];
    z[121]=z[6]*z[15];
    z[66]=z[1] - z[66];
    z[66]=z[2]*z[66]*z[121];
    z[40]=z[111] + n<T>(2,3)*z[66] + z[116] + z[40];
    z[40]=z[18]*z[40];
    z[66]=43*z[25];
    z[111]= - 28*z[27] - z[66];
    z[111]=z[11]*z[111];
    z[116]= - z[11]*z[65];
    z[116]=z[30] + z[116];
    z[122]=z[55]*z[15];
    z[116]=z[116]*z[122];
    z[111]=z[116] + z[68] + z[111];
    z[111]=z[15]*z[111];
    z[111]=z[111] - n<T>(89,3)*z[27] - 36*z[25];
    z[111]=z[15]*z[111];
    z[111]=12*z[1] + z[111];
    z[111]=z[15]*z[111];
    z[116]= - z[46] - z[25];
    z[116]=z[11]*z[116];
    z[116]= - z[30] + z[116];
    z[116]=z[116]*z[83];
    z[123]=21*z[27];
    z[124]= - z[123] - z[88];
    z[124]=z[11]*z[124];
    z[124]= - z[72] + z[124];
    z[124]=z[11]*z[124];
    z[116]=z[124] + z[116];
    z[116]=z[15]*z[116];
    z[124]= - 18*z[27] - z[57];
    z[124]=z[11]*z[124];
    z[116]=z[116] - z[68] + z[124];
    z[116]=z[15]*z[116];
    z[116]=z[64] + z[116];
    z[116]=z[116]*z[121];
    z[111]=z[111] + z[116];
    z[111]=z[6]*z[111];
    z[116]=z[42] + z[27];
    z[124]= - z[11]*z[116];
    z[124]=z[82] + z[124];
    z[124]=z[124]*z[79];
    z[125]=22*z[25];
    z[124]=z[124] - n<T>(73,3)*z[27] - z[125];
    z[124]=z[15]*z[124];
    z[124]= - 14*z[1] + z[124];
    z[124]=z[124]*z[89];
    z[126]=2*z[34];
    z[127]=z[126] - z[28];
    z[128]=z[127]*z[75];
    z[128]=37*z[1] + z[128];
    z[129]=n<T>(1,2)*z[27];
    z[130]= - z[129] - z[25];
    z[130]=z[6]*z[130];
    z[128]=n<T>(1,3)*z[128] + z[130];
    z[128]=z[6]*z[128];
    z[127]=z[15]*z[127];
    z[130]=5*z[1];
    z[127]=z[130] + z[127];
    z[127]=z[15]*z[127];
    z[131]= - z[6]*z[25];
    z[131]= - z[1] + z[131];
    z[131]=z[2]*z[131];
    z[127]=8*z[131] + z[128] - static_cast<T>(3)+ n<T>(2,3)*z[127];
    z[127]=z[2]*z[127];
    z[128]=z[64] - n<T>(14,3)*z[34];
    z[128]=z[15]*z[128];
    z[128]= - z[1] + z[128];
    z[128]=z[128]*z[75];
    z[114]= - z[114] - z[126];
    z[114]=z[15]*z[114];
    z[131]=z[6]*z[34];
    z[114]=z[114] - n<T>(11,6)*z[131];
    z[114]=z[6]*z[114];
    z[114]=z[128] + z[114];
    z[114]=z[10]*z[114];
    z[40]=z[40] + z[114] + z[127] + z[124] + z[111];
    z[40]=z[18]*z[40];
    z[111]=9*z[30];
    z[114]=z[111] - z[51];
    z[114]=z[15]*z[114];
    z[124]=14*z[27];
    z[114]= - z[124] + z[114];
    z[114]=z[114]*z[79];
    z[127]=6*z[34];
    z[128]=z[129] + z[127];
    z[128]=z[10]*z[128];
    z[114]=z[128] + 25*z[1] + z[114];
    z[114]=z[15]*z[114];
    z[128]=z[130] - 6*z[110];
    z[128]=z[128]*z[118];
    z[129]=z[18]*z[115]*z[20];
    z[128]=z[128] + z[129];
    z[113]=z[128]*z[113];
    z[128]= - z[70] + 4*z[34];
    z[128]=z[15]*z[128];
    z[129]=10*z[1];
    z[128]=z[129] + z[128];
    z[131]=3*z[89];
    z[128]=z[128]*z[131];
    z[113]=z[128] + z[113];
    z[113]=z[18]*z[113];
    z[128]=n<T>(1,2)*z[6];
    z[132]= - z[110]*z[128];
    z[133]=z[15]*z[64];
    z[129]= - z[129] + z[133];
    z[129]=z[2]*z[129];
    z[113]=z[113] + z[129] + z[132] + z[114];
    z[113]=z[18]*z[113];
    z[114]= - z[74] + 5*z[110];
    z[114]=z[114]*z[131];
    z[129]=z[18]*z[1];
    z[131]=z[129]*z[118];
    z[131]=5*z[131];
    z[114]=z[114] - z[131];
    z[114]=z[114]*z[18];
    z[132]=15*z[34];
    z[133]= - n<T>(43,2)*z[27] + z[132];
    z[133]=z[15]*z[133];
    z[133]=z[78] + z[133];
    z[133]=z[15]*z[133];
    z[134]=z[89]*z[95];
    z[133]= - z[114] + z[133] - n<T>(5,2)*z[134];
    z[133]=z[18]*z[133];
    z[134]=5*z[51];
    z[135]=z[44] - z[134];
    z[135]=z[15]*z[135];
    z[136]= - n<T>(9,2)*z[30] + z[134];
    z[136]=z[10]*z[136];
    z[135]=z[136] - z[27] + z[135];
    z[135]=z[15]*z[135];
    z[133]=z[133] - z[20] + z[135];
    z[133]=z[18]*z[133];
    z[135]=z[41]*z[15];
    z[136]=9*z[36];
    z[137]=z[136] - 5*z[135];
    z[137]=z[15]*z[137];
    z[137]= - n<T>(37,3)*z[30] + z[137];
    z[137]=z[10]*z[137];
    z[137]=z[107] + z[137];
    z[138]= - z[52] + n<T>(5,2)*z[51];
    z[138]=z[15]*z[138];
    z[133]=z[133] + n<T>(20,3)*z[27] + z[138] + n<T>(1,2)*z[137];
    z[133]=z[3]*z[133];
    z[137]=z[51] - z[52];
    z[138]=z[15]*z[137];
    z[138]= - z[27] - 13*z[138];
    z[138]=z[15]*z[138];
    z[93]= - z[30]*z[93];
    z[139]=z[30] - 6*z[51];
    z[139]=z[15]*z[139];
    z[139]=z[139] + 6*z[27];
    z[139]=z[10]*z[139];
    z[93]=z[133] + z[113] + z[139] + 4*z[76] + z[93] + z[130] + z[138];
    z[93]=z[3]*z[93];
    z[113]=z[137]*z[79];
    z[65]= - 3*z[65] + z[113];
    z[65]=z[15]*z[65];
    z[65]=n<T>(47,3)*z[1] + z[65];
    z[65]=z[15]*z[65];
    z[113]=z[115]*z[129];
    z[115]=8*z[113];
    z[133]=z[64] + z[25];
    z[138]= - z[133]*z[79];
    z[138]=9*z[1] + z[138];
    z[138]=z[138]*z[118];
    z[138]=z[138] + z[115];
    z[138]=z[18]*z[138];
    z[116]= - z[15]*z[116];
    z[116]=20*z[1] + z[116];
    z[116]=z[116]*z[89];
    z[116]=z[116] + z[138];
    z[116]=z[18]*z[116];
    z[65]=z[116] - static_cast<T>(1)+ z[65];
    z[65]=z[18]*z[65];
    z[116]= - z[1] + 12*z[110];
    z[116]=z[116]*z[118];
    z[113]=z[116] - 4*z[113];
    z[113]=z[18]*z[113];
    z[116]=z[27] - z[127];
    z[116]=z[116]*z[120];
    z[113]=z[116] + z[113];
    z[113]=z[18]*z[113];
    z[116]=4*z[51];
    z[120]= - z[30] + z[116];
    z[120]=z[15]*z[120];
    z[120]=z[27] + z[120];
    z[120]=z[15]*z[120];
    z[120]=z[120] - 7*z[1];
    z[120]=z[15]*z[120];
    z[113]=z[120] + z[113];
    z[113]=z[18]*z[113];
    z[120]=z[64] - z[34];
    z[120]=z[15]*z[120];
    z[113]=z[113] - z[61] + z[120];
    z[113]=z[18]*z[113];
    z[120]=z[96] - 5*z[34];
    z[120]=z[15]*z[120];
    z[120]= - z[90] + 3*z[120];
    z[120]=z[15]*z[120];
    z[114]=z[120] + z[114];
    z[114]=z[18]*z[114];
    z[120]=12*z[30];
    z[127]= - z[120] + z[134];
    z[127]=z[15]*z[127];
    z[127]=z[96] + z[127];
    z[127]=z[15]*z[127];
    z[114]=z[127] + z[114];
    z[114]=z[114]*z[108];
    z[113]=z[114] - z[27] + z[113];
    z[113]=z[3]*z[113];
    z[114]=z[38] - z[36];
    z[127]=z[10]*z[114];
    z[138]=z[27]*z[18];
    z[127]=z[138] - z[30] + z[127];
    z[139]=n<T>(8,3)*z[8];
    z[127]=z[127]*z[139];
    z[140]=8*z[10];
    z[141]= - z[43]*z[140];
    z[80]=z[127] + z[141] - z[80];
    z[80]=z[8]*z[80];
    z[127]=z[29] - 7*z[25];
    z[141]=z[91]*z[30];
    z[142]=z[11]*z[51];
    z[142]= - z[141] + z[142];
    z[142]=z[15]*z[142];
    z[142]=z[33] + z[142];
    z[142]=z[142]*z[75];
    z[127]=n<T>(1,3)*z[127] + z[142];
    z[75]=z[127]*z[75];
    z[127]= - z[21]*z[51];
    z[127]=z[48] + z[127];
    z[127]=z[15]*z[127];
    z[127]= - z[85] + z[127];
    z[127]=z[127]*z[89];
    z[142]=z[52]*z[2];
    z[127]= - z[142] - z[64] + 4*z[127];
    z[127]=z[2]*z[127];
    z[143]= - z[96] + 41*z[25];
    z[143]=z[10]*z[143];
    z[143]=z[143] + 19*z[1] + 10*z[11];
    z[144]=z[27]*z[9];
    z[65]=z[80] + z[113] + z[144] + z[65] + z[127] + z[75] + n<T>(1,3)*
    z[143];
    z[65]=z[8]*z[65];
    z[75]= - 22*z[33] + n<T>(25,3)*z[30];
    z[80]=z[55]*z[73];
    z[80]=z[80] - z[75];
    z[80]=z[2]*z[80];
    z[113]=z[100]*z[11];
    z[113]=z[113] - z[136] + n<T>(38,3)*z[38];
    z[113]=z[113]*z[2];
    z[127]=z[36]*z[11];
    z[136]=z[127] - z[41];
    z[143]=z[136]*z[2];
    z[145]=z[41]*z[10];
    z[146]=z[143] + z[145];
    z[147]=z[146] - z[36];
    z[148]=z[147]*z[139];
    z[148]=z[113] - z[148] + 8*z[99];
    z[149]=n<T>(38,3)*z[30];
    z[150]=z[149] - z[148];
    z[150]=z[8]*z[150];
    z[151]=z[9]*z[43];
    z[152]=n<T>(23,6)*z[27];
    z[80]=z[150] - z[71] + z[151] + 8*z[95] - z[152] + z[80];
    z[80]=z[8]*z[80];
    z[146]= - z[6]*z[146];
    z[147]=z[8]*z[147];
    z[150]=z[99]*z[6];
    z[151]=z[8]*z[99];
    z[151]= - z[150] + z[151];
    z[151]=z[19]*z[151];
    z[146]=z[151] + z[147] + z[98] + z[146];
    z[146]=z[7]*z[146];
    z[147]=z[38] + z[36];
    z[151]=z[147]*z[9];
    z[113]= - z[151] + z[113];
    z[113]=z[6]*z[113];
    z[148]=z[151] + n<T>(35,3)*z[30] - z[148];
    z[148]=z[8]*z[148];
    z[139]=z[99]*z[139];
    z[139]=z[139] + n<T>(17,6)*z[95] + z[71];
    z[139]=z[8]*z[139];
    z[153]= - n<T>(17,6)*z[10] - z[3];
    z[153]=z[107]*z[153];
    z[139]=z[139] + z[153];
    z[139]=z[19]*z[139];
    z[113]=n<T>(8,3)*z[146] + z[139] + z[148] + 8*z[150] - n<T>(35,3)*z[107] + 
    z[113];
    z[113]=z[7]*z[113];
    z[69]=z[69] + 17*z[107];
    z[69]=z[10]*z[69];
    z[139]=z[64] + z[107];
    z[139]=z[3]*z[139];
    z[69]=n<T>(1,6)*z[69] + z[139];
    z[69]=z[6]*z[69];
    z[139]=npow(z[8],2);
    z[139]=n<T>(8,3)*z[139];
    z[146]=z[99]*z[139];
    z[148]=z[27]*z[10];
    z[153]= - z[3]*z[64];
    z[146]=z[146] - n<T>(23,6)*z[148] + z[153];
    z[146]=z[8]*z[146];
    z[69]=z[146] + z[69];
    z[69]=z[19]*z[69];
    z[146]=z[147]*z[6];
    z[146]=z[146] - z[43];
    z[146]=z[9]*z[146];
    z[146]=z[146] + z[152] - z[107];
    z[146]=z[6]*z[146];
    z[75]=z[6]*z[75];
    z[104]= - z[55]*z[104];
    z[75]=z[75] + z[104];
    z[75]=z[2]*z[75];
    z[104]=z[3] - z[140];
    z[104]=z[107]*z[104];
    z[69]=z[113] + z[69] + z[80] + z[75] + z[104] + z[146];
    z[69]=z[7]*z[69];
    z[75]=z[98] + z[82];
    z[80]=z[75]*z[128];
    z[104]=z[149] + z[100];
    z[104]=z[104]*z[2];
    z[80]=n<T>(25,6)*z[95] + z[80] + z[104];
    z[99]=z[99] - z[100];
    z[113]=z[99]*z[9];
    z[146]=n<T>(8,3)*z[113];
    z[149]=z[146] + z[80];
    z[149]=z[9]*z[149];
    z[80]= - z[3]*z[80];
    z[152]=z[30]*z[9];
    z[152]=z[152] - z[71];
    z[152]=z[152]*z[8];
    z[80]= - z[152] + z[149] + z[80];
    z[80]=z[19]*z[80];
    z[149]=z[41]*z[2];
    z[145]=z[149] - z[145];
    z[149]=n<T>(1,3)*z[9];
    z[153]=z[149]*z[145];
    z[153]=z[153] + z[99];
    z[154]= - z[9]*z[153];
    z[155]=z[99]*z[3];
    z[154]=z[154] + z[155];
    z[113]=z[113] - z[155];
    z[113]=z[19]*z[113];
    z[156]=z[3] - z[9];
    z[157]=z[145]*z[156];
    z[113]=z[113] + z[157];
    z[113]=z[4]*z[113];
    z[80]=n<T>(8,3)*z[113] + 8*z[154] + z[80];
    z[80]=z[4]*z[80];
    z[98]=z[31] + n<T>(1,2)*z[98];
    z[98]=z[98]*z[6];
    z[98]= - z[98] + n<T>(28,3)*z[95] - n<T>(13,3)*z[73] + n<T>(38,3)*z[27];
    z[113]=8*z[9];
    z[153]= - z[153]*z[113];
    z[153]=z[153] + z[98];
    z[153]=z[9]*z[153];
    z[98]= - z[3]*z[98];
    z[154]=z[52]*z[6];
    z[154]=z[154] + n<T>(15,2)*z[27];
    z[154]=z[154]*z[6];
    z[142]=z[142] + z[70];
    z[142]=z[142]*z[101];
    z[142]=z[154] + z[142] + n<T>(35,6)*z[148];
    z[104]=z[104] + z[146];
    z[104]=z[9]*z[104];
    z[104]=z[104] - z[142];
    z[104]=z[9]*z[104];
    z[75]=z[6]*z[75];
    z[75]=z[75] + n<T>(25,3)*z[95];
    z[75]=z[3]*z[75];
    z[75]=n<T>(1,2)*z[75] + z[142];
    z[75]=z[3]*z[75];
    z[71]=z[27] - z[71];
    z[71]=z[3]*z[71];
    z[71]= - z[144] + z[71];
    z[71]=z[8]*z[71];
    z[71]=z[71] + z[104] + z[75];
    z[71]=z[19]*z[71];
    z[71]=z[80] + z[71] - z[152] + z[153] + z[98];
    z[71]=z[4]*z[71];
    z[75]=z[10] - z[2];
    z[80]= - z[75]*npow(z[1],6);
    z[98]=z[80]*z[8];
    z[104]=z[145] - n<T>(1,3)*z[98];
    z[104]=z[104]*z[8];
    z[142]=z[6]*z[145];
    z[144]= - z[6]*z[80];
    z[98]=z[98] + z[144];
    z[98]=z[7]*z[98];
    z[98]=n<T>(1,3)*z[98] - z[104] + z[142];
    z[98]=z[7]*z[98];
    z[99]=n<T>(4,3)*z[99];
    z[104]= - z[99] - z[104];
    z[104]=z[8]*z[104];
    z[142]= - z[6]*z[100];
    z[142]=z[142] + z[150];
    z[98]=z[98] + n<T>(4,3)*z[142] + z[104];
    z[98]=z[7]*z[98];
    z[104]=z[149]*z[80];
    z[104]=z[145] - z[104];
    z[104]=z[104]*z[9];
    z[142]=z[3]*z[145];
    z[80]= - z[4]*z[80]*z[156];
    z[80]=n<T>(1,3)*z[80] - z[104] + z[142];
    z[80]=z[4]*z[80];
    z[99]= - z[99] - z[104];
    z[99]=z[9]*z[99];
    z[80]=z[80] + z[99] + n<T>(4,3)*z[155];
    z[80]=z[4]*z[80];
    z[80]=z[98] + z[80];
    z[98]=11*z[33];
    z[99]=z[98] + 19*z[30];
    z[104]= - z[2]*z[147];
    z[104]=z[104] - z[99];
    z[104]=z[2]*z[104];
    z[127]=z[127] + 2*z[41];
    z[127]=z[149]*z[127];
    z[127]=z[127] - z[39];
    z[113]=z[113]*z[75]*z[127];
    z[99]=z[10]*z[99];
    z[99]=z[113] + z[104] + z[99];
    z[99]=z[9]*z[99];
    z[104]= - z[10]*z[136];
    z[104]=z[143] + z[104];
    z[104]=z[8]*z[104];
    z[113]=z[114]*z[75];
    z[104]=n<T>(1,3)*z[104] + z[113];
    z[104]=z[8]*z[104];
    z[72]=z[72] - z[98];
    z[98]=z[56] - z[72];
    z[98]=z[2]*z[98];
    z[72]=z[10]*z[72];
    z[72]=8*z[104] + z[98] + z[72];
    z[72]=z[8]*z[72];
    z[95]= - z[73] + z[95];
    z[75]= - z[75]*z[151];
    z[98]=z[10]*z[38];
    z[56]= - z[56] + z[98];
    z[56]=z[8]*z[56];
    z[56]=z[56] + 2*z[95] + z[75];
    z[56]=z[5]*z[56];
    z[75]= - z[96] + z[73];
    z[75]=z[2]*z[75];
    z[76]=z[148] - z[76];
    z[95]=z[106]*z[6]*z[76];
    z[98]=z[10]*z[96];
    z[75]=z[95] + z[75] + z[98];
    z[76]= - z[18]*z[76]*z[94];
    z[56]=z[56] + z[72] + z[76] + 2*z[75] + z[99] + 8*z[80];
    z[56]=z[5]*z[56];
    z[72]= - z[53]*z[107];
    z[75]= - z[18]*z[38];
    z[48]=z[48] + z[75];
    z[48]=z[18]*z[6]*z[48];
    z[75]=z[19]*z[109];
    z[48]=z[75] + z[72] + z[48];
    z[48]=z[12]*z[48];
    z[72]=z[77] - z[138];
    z[72]=z[18]*z[72];
    z[60]= - z[19]*z[60];
    z[48]=z[48] + z[60] - z[85] + z[72];
    z[48]=z[48]*z[32];
    z[60]=3*z[67] + z[34];
    z[60]=z[6]*z[60];
    z[72]=z[34] + z[27];
    z[75]=z[121]*z[18];
    z[76]=z[72]*z[75];
    z[60]=z[76] + z[1] + z[60];
    z[60]=z[18]*z[60];
    z[76]= - z[30] - 2*z[51];
    z[76]=z[15]*z[76];
    z[72]=z[15]*z[72];
    z[72]=z[1] + z[72];
    z[72]=z[18]*z[72];
    z[72]=z[72] - z[46] + z[76];
    z[72]=z[18]*z[72];
    z[76]=z[41]*z[89];
    z[72]=z[72] + z[30] + z[76];
    z[72]=z[3]*z[72];
    z[76]=z[36]*z[89];
    z[77]=z[29] + z[42];
    z[77]=z[77]*z[11];
    z[80]= - z[52] - z[77];
    z[80]=z[6]*z[80];
    z[85]= - z[3]*z[129];
    z[85]=z[1] + z[85];
    z[85]=z[19]*z[85];
    z[48]=z[48] + z[85] + z[72] + z[60] + z[80] - z[62] + z[76];
    z[48]=z[16]*z[48];
    z[60]= - z[27]*z[108];
    z[72]=z[24]*z[138];
    z[76]= - z[18]*z[112];
    z[60]=z[72] + z[60] + z[84] + z[76];
    z[60]=z[60]*z[32];
    z[67]=z[67]*z[6];
    z[72]=z[137]*z[89];
    z[72]=2*z[67] - z[1] + z[72];
    z[76]= - z[27] + z[34];
    z[76]=z[15]*z[76];
    z[76]= - z[130] + z[76];
    z[76]=z[6]*z[76];
    z[80]=z[126] - z[27];
    z[80]=z[80]*z[15];
    z[80]=z[80] - z[20];
    z[75]=z[80]*z[75];
    z[75]=z[75] + static_cast<T>(1)+ z[76];
    z[75]=z[18]*z[75];
    z[76]=z[82] - z[116];
    z[76]=z[15]*z[76];
    z[80]=z[18]*z[80];
    z[64]=z[80] + z[64] + z[76];
    z[64]=z[15]*z[64];
    z[64]=z[78] + z[64];
    z[64]=z[18]*z[64];
    z[37]= - z[37] + z[135];
    z[37]=z[37]*z[89];
    z[37]= - z[27] + z[37];
    z[37]=2*z[37] + z[64];
    z[37]=z[3]*z[37];
    z[37]=z[48] + z[60] - z[19] + z[37] + 2*z[72] + z[75];
    z[37]=z[16]*z[37];
    z[48]=114*z[27] + z[66];
    z[48]=z[11]*z[48];
    z[48]=95*z[30] + z[48];
    z[48]=z[11]*z[48];
    z[48]=20*z[36] + z[48];
    z[48]=z[11]*z[48];
    z[60]=z[70] + z[50];
    z[60]=z[11]*z[60];
    z[60]=z[111] + z[60];
    z[60]=z[11]*z[60];
    z[60]=z[36] + z[60];
    z[60]=z[11]*z[60];
    z[60]= - z[41] + z[60];
    z[60]=z[60]*z[122];
    z[64]=4*z[41];
    z[48]=z[60] - z[64] + z[48];
    z[48]=z[15]*z[48];
    z[60]=48*z[25];
    z[70]=103*z[27] + z[60];
    z[70]=z[11]*z[70];
    z[70]=71*z[30] + z[70];
    z[70]=z[11]*z[70];
    z[48]=z[48] + 16*z[36] + z[70];
    z[48]=z[15]*z[48];
    z[70]=41*z[27] + 27*z[25];
    z[70]=z[11]*z[70];
    z[48]=z[48] + z[120] + z[70];
    z[48]=z[15]*z[48];
    z[70]=z[29] + z[25];
    z[70]=z[11]*z[70];
    z[70]=6*z[30] + z[70];
    z[70]=z[11]*z[70];
    z[70]=z[45] + z[70];
    z[70]=z[11]*z[70];
    z[70]=z[41] + z[70];
    z[70]=z[70]*z[83];
    z[72]=47*z[27] + z[88];
    z[72]=z[11]*z[72];
    z[72]=63*z[30] + z[72];
    z[72]=z[11]*z[72];
    z[72]=37*z[36] + z[72];
    z[72]=z[11]*z[72];
    z[72]=8*z[41] + z[72];
    z[72]=z[11]*z[72];
    z[70]=z[72] + z[70];
    z[70]=z[15]*z[70];
    z[72]=52*z[27] + z[57];
    z[72]=z[11]*z[72];
    z[72]=57*z[30] + z[72];
    z[72]=z[11]*z[72];
    z[72]=26*z[36] + z[72];
    z[72]=z[11]*z[72];
    z[64]=z[70] + z[64] + z[72];
    z[64]=z[15]*z[64];
    z[70]=z[58] + z[26];
    z[72]=z[70]*z[91];
    z[72]=21*z[30] + z[72];
    z[72]=z[11]*z[72];
    z[75]=5*z[36];
    z[64]=z[64] + z[75] + z[72];
    z[64]=z[15]*z[64];
    z[72]=n<T>(3,2)*z[27];
    z[76]=z[72] + z[25];
    z[76]=z[76]*z[92];
    z[64]=z[64] + z[68] + z[76];
    z[64]=z[6]*z[64];
    z[48]=z[64] + z[48] + z[124] + z[88];
    z[48]=z[6]*z[48];
    z[39]= - z[10]*z[39];
    z[39]=z[39] - z[33] + 2*z[100];
    z[39]=z[9]*z[39];
    z[64]= - n<T>(65,3)*z[30] - z[100];
    z[64]=z[2]*z[64];
    z[76]=z[52] + z[33];
    z[80]=z[76]*z[140];
    z[39]=n<T>(8,3)*z[39] + z[80] - n<T>(22,3)*z[27] + z[64];
    z[39]=z[9]*z[39];
    z[64]=z[28] + z[42];
    z[64]=z[64]*z[53]*z[79];
    z[80]=71*z[27];
    z[84]=z[80] + z[125];
    z[21]=z[84]*z[21];
    z[21]=z[21] + z[64];
    z[21]=z[15]*z[21];
    z[64]=14*z[25];
    z[84]=z[80] + z[64];
    z[84]=z[11]*z[84];
    z[21]=z[84] + z[21];
    z[21]=z[15]*z[21];
    z[21]=z[21] + 32*z[27] - z[25];
    z[21]=z[15]*z[21];
    z[84]= - z[15]*z[136];
    z[75]=z[84] - z[75] + z[141];
    z[75]=z[11]*z[75];
    z[41]=z[41] + z[75];
    z[41]=z[15]*z[41];
    z[44]=z[44] - z[33];
    z[44]=z[11]*z[44];
    z[41]=z[41] - z[45] + z[44];
    z[41]=z[15]*z[41];
    z[41]=z[41] + z[82] - z[81];
    z[41]=z[41]*z[79];
    z[41]=3*z[73] + z[123] + z[41];
    z[41]=z[2]*z[41];
    z[44]= - z[10]*z[70];
    z[21]=z[39] + z[44] + z[41] + z[48] - z[1] + z[21];
    z[21]=z[9]*z[21];
    z[39]= - z[19]*z[30];
    z[39]=z[38] + z[39];
    z[39]=z[19]*z[39];
    z[39]= - z[97] + z[39];
    z[39]=z[14]*z[39];
    z[41]=z[97]*z[9];
    z[39]=z[41] + z[39];
    z[39]=z[39]*z[102];
    z[41]=z[12]*z[36]*z[53]*z[103];
    z[39]=z[39] + z[41];
    z[39]=z[12]*z[39];
    z[41]= - z[102]*z[105];
    z[39]=z[41] + z[39];
    z[32]=z[39]*z[32];
    z[39]=z[20]*z[24];
    z[39]= - z[1] + z[39];
    z[39]=z[19]*z[39];
    z[41]= - z[9]*z[59];
    z[39]=z[39] + z[25] + z[41];
    z[39]=z[14]*z[39];
    z[24]=z[24] + 1;
    z[24]=z[1]*z[24];
    z[41]= - z[9]*z[62];
    z[24]=z[39] + z[41] + z[24];
    z[24]=z[14]*z[24];
    z[39]= - z[9]*z[74];
    z[41]=z[3]*z[78];
    z[24]=z[24] + z[39] + z[41];
    z[24]=z[14]*z[24];
    z[24]=z[24] + z[32];
    z[32]=17*z[1];
    z[39]= - z[32] + 24*z[110];
    z[39]=z[39]*z[118];
    z[39]=z[39] - z[115];
    z[39]=z[18]*z[39];
    z[41]=z[63] - 12*z[34];
    z[41]=z[15]*z[41];
    z[41]= - z[119] + z[41];
    z[41]=z[41]*z[89];
    z[39]=2*z[41] + z[39];
    z[39]=z[18]*z[39];
    z[20]= - z[20] + 15*z[110];
    z[20]=z[20]*z[89];
    z[20]=z[20] - z[131];
    z[20]=z[18]*z[20];
    z[29]=z[29] - z[132];
    z[29]=z[15]*z[29];
    z[29]=z[1] + z[29];
    z[29]=z[15]*z[29];
    z[20]=z[29] + z[20];
    z[20]=z[18]*z[20];
    z[29]= - z[52] + z[134];
    z[29]=z[15]*z[29];
    z[29]= - z[27] + z[29];
    z[29]=z[15]*z[29];
    z[20]=z[29] + z[20];
    z[20]=z[3]*z[20];
    z[29]= - 17*z[30] + 8*z[51];
    z[29]=z[15]*z[29];
    z[29]=22*z[27] + z[29];
    z[29]=z[15]*z[29];
    z[29]= - z[119] + z[29];
    z[29]=z[15]*z[29];
    z[20]=z[20] + z[29] + z[39];
    z[20]=z[3]*z[20];
    z[29]= - z[30] - z[87];
    z[39]=8*z[15];
    z[29]=z[29]*z[39];
    z[29]= - 17*z[86] + z[29];
    z[29]=z[15]*z[29];
    z[29]= - 22*z[1] + z[29];
    z[29]=z[29]*z[89];
    z[41]=z[46] - z[25];
    z[41]=z[41]*z[39];
    z[41]= - z[32] + z[41];
    z[41]=z[41]*z[118];
    z[41]=z[41] - z[115];
    z[41]=z[18]*z[41];
    z[44]=z[15]*z[53]*z[90];
    z[44]=17*z[59] + z[44];
    z[44]=z[15]*z[44];
    z[44]=z[125] + z[44];
    z[44]=z[15]*z[44];
    z[44]=z[119] + z[44];
    z[44]=z[9]*z[15]*z[44];
    z[20]=z[20] + z[44] + z[29] + z[41] + 4*z[24];
    z[20]=z[17]*z[20];
    z[24]=13*z[30] + z[47];
    z[29]= - z[36] - n<T>(2,3)*z[38];
    z[29]=z[29]*z[79];
    z[24]=n<T>(5,3)*z[24] + z[29];
    z[24]=z[15]*z[24];
    z[29]= - 77*z[27] - 16*z[25];
    z[24]=n<T>(1,3)*z[29] + z[24];
    z[24]=z[15]*z[24];
    z[29]= - z[58] - 32*z[25];
    z[29]=2*z[29] + n<T>(19,2)*z[34];
    z[38]=z[72] + z[62];
    z[38]=z[11]*z[38];
    z[38]=n<T>(1,2)*z[30] + z[38];
    z[38]=z[6]*z[38];
    z[29]=n<T>(1,3)*z[29] + z[38];
    z[29]=z[6]*z[29];
    z[38]=z[55]*z[67];
    z[38]=z[38] + z[54];
    z[38]=z[38]*z[101];
    z[24]=z[38] + z[29] + z[24] + 16*z[1] - z[92];
    z[24]=z[2]*z[24];
    z[29]= - z[80] - z[66];
    z[29]=z[11]*z[29];
    z[29]= - 24*z[30] + z[29];
    z[29]=z[11]*z[29];
    z[28]= - z[28] - z[50];
    z[28]=z[11]*z[28];
    z[28]= - z[52] + z[28];
    z[28]=z[11]*z[28];
    z[28]=z[36] + z[28];
    z[28]=z[28]*z[122];
    z[28]=z[28] + z[45] + z[29];
    z[28]=z[15]*z[28];
    z[29]= - 55*z[27] - z[60];
    z[29]=z[11]*z[29];
    z[28]=z[28] - 16*z[30] + z[29];
    z[28]=z[15]*z[28];
    z[28]=z[28] + z[49] - 21*z[25];
    z[28]=z[15]*z[28];
    z[29]= - z[11]*z[133];
    z[29]= - z[31] + z[29];
    z[29]=z[11]*z[29];
    z[29]= - z[36] + z[29];
    z[29]=z[29]*z[83];
    z[31]= - 34*z[27] - z[88];
    z[31]=z[11]*z[31];
    z[31]= - 29*z[30] + z[31];
    z[31]=z[11]*z[31];
    z[31]= - 8*z[36] + z[31];
    z[31]=z[11]*z[31];
    z[29]=z[31] + z[29];
    z[29]=z[15]*z[29];
    z[31]= - 35*z[27] - z[57];
    z[31]=z[11]*z[31];
    z[31]= - 22*z[30] + z[31];
    z[31]=z[11]*z[31];
    z[29]=z[29] - z[45] + z[31];
    z[29]=z[15]*z[29];
    z[26]= - z[96] - z[26];
    z[26]=z[26]*z[91];
    z[26]=z[29] - z[82] + z[26];
    z[26]=z[15]*z[26];
    z[25]=n<T>(2,3)*z[27] - z[25];
    z[25]=4*z[25] + z[26];
    z[25]=z[6]*z[25];
    z[25]=z[25] + z[28] - z[130] + z[11];
    z[25]=z[6]*z[25];
    z[26]=z[147]*z[39];
    z[26]=z[26] - n<T>(29,3)*z[30] - 8*z[33];
    z[26]=z[15]*z[26];
    z[26]=z[26] - z[49] + z[50];
    z[26]=z[15]*z[26];
    z[28]= - n<T>(17,3)*z[107] - z[27] - n<T>(43,3)*z[34];
    z[28]=z[28]*z[128];
    z[29]= - z[32] - z[91];
    z[26]=z[28] + n<T>(2,3)*z[29] + z[26];
    z[26]=z[10]*z[26];
    z[28]=z[76]*npow(z[9],2);
    z[28]= - n<T>(8,3)*z[28] + z[61];
    z[28]=z[10]*z[28];
    z[29]=n<T>(7,2)*z[27] + z[42];
    z[29]=z[6]*z[29];
    z[29]=z[61] + z[29];
    z[29]=z[6]*z[29];
    z[28]=z[29] + z[28];
    z[28]=z[9]*z[28];
    z[29]=z[43]*z[139];
    z[29]=z[29] + n<T>(19,3)*z[1];
    z[29]=z[10]*z[29];
    z[31]=z[27] + z[129];
    z[31]=z[31]*npow(z[3],2);
    z[29]=z[31] - static_cast<T>(2)+ z[29];
    z[29]=z[8]*z[29];
    z[31]= - z[1] - z[117];
    z[31]=z[3]*z[31]*z[101];
    z[28]=z[29] + z[31] + 4*z[2] + z[28];
    z[28]=z[19]*z[28];
    z[29]= - 49*z[27] - z[125];
    z[29]=z[11]*z[29];
    z[31]=z[68] - z[77];
    z[31]=z[11]*z[31];
    z[31]= - z[45] + z[31];
    z[31]=z[31]*z[79];
    z[29]=z[31] + n<T>(131,3)*z[30] + z[29];
    z[29]=z[15]*z[29];
    z[27]=z[29] - 61*z[27] - z[64];
    z[27]=z[15]*z[27];
    z[27]=z[1] + z[27];
    z[27]=z[15]*z[27];

    r += z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[27]
       + z[28] + z[35] + 4*z[37] + z[40] + z[56] + z[65] + z[69] + 
      z[71] + z[93];
 
    return r;
}

template double qqb_2lNLC_r1204(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1204(const std::array<dd_real,31>&);
#endif
