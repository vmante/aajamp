#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r58(const std::array<T,31>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[14];
    z[7]=k[12];
    z[8]=k[13];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[8];
    z[12]=k[15];
    z[13]=5*z[12];
    z[14]=z[13]*z[4];
    z[15]=z[11] + z[13];
    z[15]=z[5]*z[15];
    z[16]= - z[11] + 6*z[5];
    z[16]=z[7]*z[16];
    z[17]= - z[4] - 2*z[7];
    z[17]=z[8]*z[17];
    z[15]=z[17] + z[16] + z[15] - z[14];
    z[15]=z[8]*z[15];
    z[16]=z[3]*z[9];
    z[17]=z[5]*z[9];
    z[18]= - z[17] + z[16];
    z[18]=z[4]*z[18];
    z[18]=z[18] + 2*z[5] - z[3];
    z[18]=z[4]*z[18];
    z[19]=z[5]*z[12];
    z[20]=z[3]*z[5];
    z[21]=2*z[4];
    z[22]=z[21] + z[7];
    z[22]=z[8]*z[22];
    z[18]=z[22] + z[18] + z[19] + z[20];
    z[18]=z[6]*z[18];
    z[22]= - 3*z[12] + 5*z[5];
    z[22]=z[3]*z[22];
    z[22]=3*z[19] + z[22];
    z[13]= - z[7] + z[13] + z[21];
    z[13]=z[7]*z[13];
    z[23]= - z[5] + 2*z[3];
    z[24]=z[4]*z[23];
    z[13]=z[13] + 2*z[22] + z[24];
    z[13]=z[4]*z[13];
    z[13]=z[18] + z[15] + z[13];
    z[13]=z[6]*z[13];
    z[15]= - z[21] - z[23];
    z[15]=z[7]*z[15];
    z[18]=z[11] - 10*z[5];
    z[18]=z[3]*z[18];
    z[21]=5*z[3] + z[4];
    z[21]=z[4]*z[21];
    z[15]=z[15] + z[18] + z[21];
    z[15]=z[7]*z[15];
    z[18]=z[3]*z[10];
    z[21]=z[18] - static_cast<T>(1)+ z[17];
    z[21]=z[7]*z[21];
    z[22]=z[3] - z[5];
    z[21]=2*z[22] + z[21];
    z[21]=z[7]*z[21];
    z[23]= - z[5]*z[11];
    z[21]=z[21] + z[23] - z[20];
    z[21]=z[8]*z[21];
    z[15]=z[15] + z[21];
    z[15]=z[8]*z[15];
    z[21]=npow(z[7],2);
    z[23]= - z[8]*z[7];
    z[21]=z[21] + z[23];
    z[21]=z[8]*z[21];
    z[23]=npow(z[4],2);
    z[24]=z[6]*z[4];
    z[24]= - z[23] + z[24];
    z[24]=z[6]*z[24];
    z[21]=z[21] + z[24];
    z[20]=z[1]*z[20]*z[21];
    z[21]=z[5] - z[12];
    z[23]=z[3]*z[21]*z[23];
    z[24]=z[7]*z[3];
    z[25]=z[11] - z[5];
    z[25]=z[25]*z[24];
    z[14]= - z[3]*z[14];
    z[14]=z[14] + z[25];
    z[14]=z[7]*z[14];
    z[13]=3*z[20] + z[13] + z[15] + z[23] + z[14];
    z[13]=z[1]*z[13];
    z[14]=z[3]*z[12];
    z[15]= - z[4] + z[21];
    z[15]=z[7]*z[15];
    z[20]=z[4] - z[7];
    z[20]=z[8]*z[20];
    z[14]=z[20] + z[14] + z[15];
    z[15]= - static_cast<T>(2)+ z[16];
    z[15]=z[4]*z[15];
    z[16]=z[17] + 2;
    z[17]=z[6]*z[16];
    z[15]=z[17] - 7*z[22] + z[15];
    z[15]=z[4]*z[15];
    z[14]=z[15] + 7*z[14];
    z[14]=z[6]*z[14];
    z[15]=z[4] - z[5];
    z[17]=static_cast<T>(1)+ z[18];
    z[17]=z[7]*z[17];
    z[16]= - z[8]*z[16];
    z[15]=z[16] + z[17] + 8*z[3] + 7*z[15];
    z[15]=z[7]*z[15];
    z[16]= - z[12] - z[3];
    z[16]=z[4]*z[16];
    z[16]=z[19] + z[16];
    z[15]=7*z[16] + z[15];
    z[15]=z[8]*z[15];
    z[16]=z[4]*z[19];
    z[17]=z[12]*z[24];
    z[16]=z[16] + z[17];
    z[13]=z[13] + z[14] + 7*z[16] + z[15];

    r += z[13]*npow(z[2],2)*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r58(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r58(const std::array<dd_real,31>&);
#endif
