#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r513(const std::array<T,31>& k) {
  T z[120];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[23];
    z[9]=k[30];
    z[10]=k[8];
    z[11]=k[9];
    z[12]=k[15];
    z[13]=k[2];
    z[14]=k[3];
    z[15]=k[4];
    z[16]=k[21];
    z[17]=k[20];
    z[18]=k[5];
    z[19]=k[18];
    z[20]=12*z[2];
    z[21]=5*z[8];
    z[22]=z[20] - z[21];
    z[22]=z[14]*z[22];
    z[23]=z[10]*z[15];
    z[24]=11*z[23];
    z[25]=z[11]*z[14];
    z[26]=6*z[25];
    z[27]=z[15]*z[16];
    z[28]=z[5]*z[15];
    z[29]=z[9]*z[14];
    z[22]=n<T>(79,4)*z[28] + z[26] - z[24] - n<T>(13,4)*z[29] + z[22] - static_cast<T>(17)+ 
    z[27];
    z[22]=z[4]*z[22];
    z[30]= - z[29] + z[25];
    z[31]=n<T>(41,2)*z[3];
    z[32]=z[14] - z[13];
    z[30]=z[31]*z[32]*z[30];
    z[33]=n<T>(17,2)*z[29];
    z[34]= - z[32]*z[33];
    z[35]=n<T>(43,4)*z[13];
    z[34]=z[34] - z[35] + 48*z[14];
    z[34]=z[9]*z[34];
    z[36]=12*z[11];
    z[37]= - z[32]*z[36];
    z[37]=z[37] - 34;
    z[37]=z[14]*z[37];
    z[35]=z[35] + z[37];
    z[35]=z[11]*z[35];
    z[37]=z[8] - z[2];
    z[38]=z[37]*z[15];
    z[30]=z[30] + z[35] + n<T>(1,2)*z[38] + z[34];
    z[30]=z[3]*z[30];
    z[34]=z[12]*z[15];
    z[35]= - n<T>(35,2)*z[28] - z[34] + static_cast<T>(42)- z[23];
    z[35]=z[6]*z[35];
    z[39]=17*z[9];
    z[40]= - n<T>(1,2)*z[13] + 3*z[14];
    z[40]=z[40]*z[39];
    z[40]= - n<T>(45,2) + z[40];
    z[41]=n<T>(1,2)*z[9];
    z[40]=z[40]*z[41];
    z[42]= - z[13] + 2*z[14];
    z[43]=npow(z[13],2);
    z[44]=z[43]*z[10];
    z[45]= - 2*z[44] + z[42];
    z[46]=6*z[11];
    z[45]=z[45]*z[46];
    z[45]=n<T>(181,4) + z[45];
    z[45]=z[11]*z[45];
    z[47]=5*z[2];
    z[48]=z[47] + z[16];
    z[49]=z[48]*z[15];
    z[50]=static_cast<T>(6)- z[49];
    z[50]=z[15]*z[50];
    z[50]=z[50] + 6*z[44];
    z[51]=2*z[12];
    z[50]=z[50]*z[51];
    z[52]=n<T>(1,2)*z[2];
    z[53]= - z[16] - z[52];
    z[53]=z[15]*z[53];
    z[50]=z[50] + n<T>(37,2) + 5*z[53];
    z[50]=z[12]*z[50];
    z[21]=z[52] - z[21];
    z[21]=z[15]*z[21];
    z[21]=n<T>(35,2) + z[21];
    z[21]=z[5]*z[21];
    z[53]=npow(z[8],2);
    z[54]=z[53]*z[14];
    z[55]=n<T>(1,2)*z[54];
    z[56]=z[53]*z[15];
    z[57]=n<T>(1,2)*z[56];
    z[58]=4*z[16];
    z[59]=6*z[10];
    z[21]=z[30] + z[35] + z[22] + z[21] + z[50] + z[45] + z[59] + z[40]
    + z[55] - z[57] - 25*z[8] + z[58] + n<T>(89,4)*z[2];
    z[21]=z[3]*z[21];
    z[22]=npow(z[14],2);
    z[30]=z[22]*z[9];
    z[35]=z[14] - n<T>(17,2)*z[30];
    z[35]=z[35]*z[41];
    z[40]= - z[22]*z[46];
    z[45]=n<T>(1,2)*z[14];
    z[40]= - z[45] + z[40];
    z[40]=z[11]*z[40];
    z[50]=z[22]*z[11];
    z[60]= - z[30] + z[50];
    z[61]=n<T>(41,4)*z[3];
    z[60]=z[60]*z[61];
    z[35]=z[60] + z[35] + z[40];
    z[35]=z[3]*z[35];
    z[40]= - static_cast<T>(13)+ z[33];
    z[40]=z[40]*z[41];
    z[60]= - n<T>(5,4) + z[26];
    z[60]=z[11]*z[60];
    z[62]=z[6]*z[15];
    z[63]=n<T>(17,2) - 2*z[62];
    z[63]=z[6]*z[63];
    z[35]=z[35] + z[63] + n<T>(33,4)*z[5] + z[40] + z[60];
    z[35]=z[3]*z[35];
    z[40]=z[15]*z[8];
    z[60]=29*z[40] + z[23];
    z[63]=n<T>(1,2)*z[5];
    z[60]=z[60]*z[63];
    z[64]=14*z[3];
    z[65]=static_cast<T>(1)+ z[28];
    z[65]=z[65]*z[64];
    z[60]=z[60] + z[65];
    z[60]=z[7]*z[60];
    z[65]=181*z[8];
    z[66]=17*z[56];
    z[67]= - z[65] + z[66];
    z[67]=n<T>(1,2)*z[67] - 15*z[10];
    z[67]=n<T>(1,2)*z[67] + 34*z[12];
    z[67]=z[5]*z[67];
    z[68]=z[15]*z[17];
    z[69]=z[4]*z[15];
    z[70]= - 6*z[68] + 7*z[69];
    z[71]=2*z[6];
    z[70]=z[70]*z[71];
    z[71]=7*z[17];
    z[72]= - z[71] + z[2];
    z[70]=z[70] - n<T>(55,4)*z[4] + n<T>(41,2)*z[5] + 2*z[72] - n<T>(13,2)*z[9];
    z[70]=z[6]*z[70];
    z[72]=npow(z[9],2);
    z[73]=n<T>(1,2)*z[72];
    z[74]=2*z[11];
    z[75]= - z[17]*z[74];
    z[76]=13*z[11] - 41*z[9] - z[10];
    z[76]=z[12]*z[76];
    z[77]=z[10] + z[8];
    z[78]=n<T>(27,2)*z[9] - 4*z[2] + n<T>(71,4)*z[77];
    z[78]=z[4]*z[78];
    z[35]=z[60] + z[35] + z[70] + z[78] + z[67] + z[76] + z[73] + z[75];
    z[35]=z[7]*z[35];
    z[60]=n<T>(1,2)*z[25];
    z[67]=14*z[28] + z[60] - static_cast<T>(28)- n<T>(29,2)*z[29];
    z[67]=z[4]*z[67];
    z[70]=z[9] - z[11];
    z[70]=z[61]*z[32]*z[70];
    z[75]= - 14*z[6] - n<T>(1,2)*z[37];
    z[75]=z[28]*z[75];
    z[39]=z[32]*z[39];
    z[39]= - static_cast<T>(19)+ z[39];
    z[39]=z[39]*z[41];
    z[39]=z[39] + z[37];
    z[76]=z[46]*z[32];
    z[78]= - n<T>(37,4) + z[76];
    z[78]=z[11]*z[78];
    z[39]=z[70] + z[67] + n<T>(1,2)*z[39] + z[78] + z[75];
    z[39]=z[3]*z[39];
    z[67]=7*z[8];
    z[70]=n<T>(5,2)*z[2];
    z[57]=z[57] + z[70] - z[67];
    z[57]=z[5]*z[57];
    z[75]=n<T>(1,4)*z[9];
    z[78]=static_cast<T>(235)- 17*z[29];
    z[78]=z[78]*z[75];
    z[55]= - n<T>(55,4)*z[5] - n<T>(27,2)*z[11] + n<T>(21,2)*z[10] + z[78] - z[55]
    - z[16] - z[67];
    z[55]=z[4]*z[55];
    z[67]=z[11]*z[10];
    z[78]=n<T>(17,4)*z[72];
    z[67]=z[67] + z[78];
    z[79]=n<T>(39,4)*z[2];
    z[80]=z[12]*z[49];
    z[80]=z[80] - z[79] + z[10];
    z[80]=z[12]*z[80];
    z[81]= - z[5] + z[12] + z[10];
    z[82]=12*z[28];
    z[83]=z[6]*z[82];
    z[81]=n<T>(11,2)*z[81] + z[83];
    z[81]=z[6]*z[81];
    z[39]=z[39] + z[81] + z[55] + z[57] + z[80] - z[67];
    z[39]=z[3]*z[39];
    z[55]= - z[53] + n<T>(85,2)*z[72];
    z[57]=3*z[12];
    z[80]=n<T>(65,4)*z[9] - 8*z[11];
    z[80]=z[80]*z[57];
    z[81]=z[36]*z[10];
    z[83]=z[10] + z[16];
    z[84]=z[83]*z[4];
    z[85]=npow(z[10],2);
    z[86]= - n<T>(143,4)*z[12] - z[47] + 39*z[8];
    z[86]=z[5]*z[86];
    z[55]= - z[84] + z[86] + z[80] - z[81] + n<T>(1,2)*z[55] - 18*z[85];
    z[55]=z[4]*z[55];
    z[80]=z[11] - z[8];
    z[86]=z[6]*z[5];
    z[87]=z[80]*z[86];
    z[47]=z[47] + z[19];
    z[88]=z[47]*z[85];
    z[89]=24*z[12];
    z[90]=z[85]*z[89];
    z[91]=npow(z[11],2);
    z[92]=6*z[91];
    z[93]=n<T>(17,4)*z[53];
    z[94]=z[92] - z[93];
    z[95]= - z[5]*z[94];
    z[90]= - n<T>(23,2)*z[87] + z[95] - z[88] + z[90];
    z[90]=z[6]*z[90];
    z[95]=z[9] - z[2];
    z[96]=z[95]*z[6];
    z[97]=n<T>(1,2)*z[96];
    z[98]=npow(z[5],2);
    z[99]=14*z[98];
    z[73]=z[97] - z[73] - z[99];
    z[73]=z[6]*z[73];
    z[100]=12*z[91];
    z[78]=z[78] + z[100];
    z[101]=z[12]*z[9];
    z[102]= - n<T>(41,4)*z[101] - z[78];
    z[102]=z[12]*z[102];
    z[103]=z[5]*z[12];
    z[104]=14*z[103];
    z[105]=npow(z[12],2);
    z[106]=n<T>(41,4)*z[105] + z[104];
    z[106]=z[5]*z[106];
    z[107]=12*z[85];
    z[108]=z[93] + z[107];
    z[108]=z[4]*z[108];
    z[109]=z[77]*z[7];
    z[110]=z[109]*z[4];
    z[73]=n<T>(43,2)*z[110] + z[73] + z[108] + z[102] + z[106];
    z[73]=z[7]*z[73];
    z[92]=z[92] + z[93];
    z[102]=z[92]*z[5];
    z[102]= - z[102] + n<T>(41,4)*z[87];
    z[106]=npow(z[6],2);
    z[102]=z[102]*z[106];
    z[108]=6*z[85];
    z[111]=z[108] + z[93];
    z[112]=z[111]*z[4];
    z[112]=z[112] + n<T>(41,4)*z[110];
    z[113]=npow(z[7],2);
    z[112]=z[112]*z[113];
    z[114]=17*z[72];
    z[115]=z[114] + 7*z[101];
    z[115]=z[115]*z[105];
    z[116]=z[5]*npow(z[12],3);
    z[116]= - z[115] + 7*z[116];
    z[117]=n<T>(1,4)*z[4];
    z[118]=z[116]*z[117];
    z[102]= - z[118] + z[112] + z[102];
    z[102]=z[102]*z[18];
    z[112]=z[36]*z[85];
    z[118]=n<T>(79,4)*z[9] - z[36];
    z[118]=z[12]*z[118];
    z[118]=n<T>(51,4)*z[72] + z[118];
    z[118]=z[12]*z[118];
    z[105]=z[105]*z[5];
    z[118]= - n<T>(75,4)*z[105] + z[112] + z[118];
    z[118]=z[4]*z[118];
    z[119]= - z[19]*z[105];
    z[73]=z[102] + z[73] + z[90] + z[119] + z[118];
    z[73]=z[18]*z[73];
    z[90]= - z[70] + 14*z[8];
    z[90]=z[5]*z[90];
    z[67]=z[90] + z[67];
    z[67]=z[4]*z[67];
    z[90]=z[12]*z[10];
    z[90]=z[90] + z[99];
    z[90]=z[6]*z[90];
    z[118]=z[37]*z[5];
    z[37]= - z[11] + 29*z[9] - z[37];
    z[37]=z[4]*z[37];
    z[37]= - z[118] + z[37];
    z[37]=z[3]*z[37];
    z[37]=n<T>(1,2)*z[37] + z[67] + z[90];
    z[37]=z[3]*z[37];
    z[67]= - z[70] + 14*z[9];
    z[67]=z[4]*z[67];
    z[67]= - z[97] - 28*z[98] + z[67];
    z[67]=z[6]*z[67];
    z[90]= - z[99] - n<T>(13,2)*z[86];
    z[90]=z[3]*z[90];
    z[97]= - z[5] + z[6];
    z[97]=z[3]*z[97];
    z[97]= - z[86] + z[97];
    z[97]=z[7]*z[97];
    z[98]=z[10]*z[103];
    z[67]=14*z[97] + z[90] + z[98] + z[67];
    z[67]=z[7]*z[67];
    z[90]=12*z[12];
    z[97]=z[85]*z[90];
    z[98]=n<T>(1,2)*z[4];
    z[96]= - z[98]*z[96];
    z[119]=z[11]*z[4]*z[12];
    z[96]=z[96] + z[97] + z[119];
    z[96]=z[6]*z[96];
    z[97]=z[95]*z[106]*z[98];
    z[86]= - z[7]*z[86]*z[64];
    z[99]= - z[3]*z[6]*z[99];
    z[86]=z[86] + z[97] + z[99];
    z[86]=z[7]*z[86];
    z[97]=npow(z[3],2)*z[98]*z[118];
    z[86]=z[97] + z[86];
    z[86]=z[1]*z[86];
    z[97]=z[4]*z[112];
    z[37]=z[86] + z[67] + z[37] + z[97] + z[96];
    z[37]=z[1]*z[37];
    z[67]=z[14]*z[8];
    z[60]= - z[60] + n<T>(29,2)*z[67];
    z[86]= - static_cast<T>(14)+ z[60];
    z[86]=z[4]*z[86];
    z[82]= - z[17]*z[82];
    z[82]=z[86] + n<T>(1,2)*z[95] + z[82];
    z[82]=z[6]*z[82];
    z[79]= - z[59] - z[19] - z[79];
    z[79]=z[10]*z[79];
    z[65]=z[65] + 17*z[54];
    z[86]= - static_cast<T>(13)+ z[29];
    z[86]=z[9]*z[86];
    z[65]=23*z[12] - 15*z[11] + n<T>(1,2)*z[65] + z[86];
    z[65]=z[65]*z[98];
    z[86]= - z[11] - z[19] - 11*z[10];
    z[86]=z[12]*z[86];
    z[71]=z[71] - 6*z[8];
    z[71]=28*z[5] + 2*z[71] - n<T>(29,4)*z[11];
    z[71]=z[5]*z[71];
    z[65]=z[82] + z[65] + z[71] + z[79] + z[86];
    z[65]=z[6]*z[65];
    z[71]=z[52] - z[19];
    z[79]=z[10]*z[71];
    z[82]=z[13]*z[81];
    z[82]=z[82] - 2*z[17] + z[10];
    z[82]=z[11]*z[82];
    z[86]= - z[19] - z[10];
    z[86]=z[12]*z[86];
    z[79]=z[86] + z[82] + n<T>(1,2)*z[53] + z[79];
    z[79]=z[5]*z[79];
    z[82]=z[12]*z[48];
    z[82]=z[107] + z[82];
    z[82]=z[12]*z[82];
    z[35]=z[37] + z[73] + z[35] + z[39] + z[65] + z[55] + z[79] - z[112]
    + z[82];
    z[35]=z[1]*z[35];
    z[37]=npow(z[14],3);
    z[39]=z[37]*z[9];
    z[55]= - 54*z[22] + n<T>(17,2)*z[39];
    z[55]=z[9]*z[55];
    z[65]=z[37]*z[74];
    z[65]=9*z[22] + z[65];
    z[65]=z[65]*z[46];
    z[73]=z[37]*z[11];
    z[79]=z[39] - z[73];
    z[31]=z[79]*z[31];
    z[31]=z[31] + z[55] + z[65];
    z[31]=z[3]*z[31];
    z[55]=3*z[11];
    z[65]= - 19*z[14] - 8*z[50];
    z[65]=z[65]*z[55];
    z[79]=93*z[14] - 119*z[30];
    z[79]=z[9]*z[79];
    z[79]=static_cast<T>(29)+ z[79];
    z[31]=z[31] - n<T>(11,2)*z[62] + n<T>(7,4)*z[28] + n<T>(1,4)*z[79] + z[65];
    z[31]=z[3]*z[31];
    z[62]= - z[15] - z[14];
    z[62]=z[62]*z[64];
    z[62]=z[62] - n<T>(1,2)*z[23] + static_cast<T>(14)- n<T>(29,2)*z[40];
    z[62]=z[7]*z[62];
    z[64]=n<T>(183,2) + 49*z[29];
    z[64]=z[64]*z[41];
    z[65]= - n<T>(19,2)*z[40] - 13*z[23];
    z[63]=z[65]*z[63];
    z[65]=n<T>(79,4)*z[69] + n<T>(59,4) - 12*z[68];
    z[65]=z[6]*z[65];
    z[79]=3*z[19];
    z[82]=z[79] - z[17];
    z[86]= - z[15]*z[93];
    z[96]=z[14]*z[36];
    z[96]=n<T>(13,2) + z[96];
    z[96]=z[11]*z[96];
    z[31]=z[62] + z[31] + z[65] - n<T>(59,4)*z[4] + z[63] - z[90] + z[96] + 
   n<T>(37,2)*z[10] + z[64] + z[86] + n<T>(137,4)*z[8] + z[82];
    z[31]=z[7]*z[31];
    z[62]=z[90]*z[15];
    z[20]= - z[20] + n<T>(13,4)*z[8];
    z[20]=z[14]*z[20];
    z[20]= - z[62] - z[26] + 5*z[29] - n<T>(107,4) + z[20];
    z[20]=z[4]*z[20];
    z[63]= - n<T>(79,2) - z[29];
    z[63]=z[63]*z[41];
    z[60]= - static_cast<T>(14)- z[60];
    z[60]=z[6]*z[60];
    z[64]= - z[14]*z[93];
    z[65]=12*z[23];
    z[86]=static_cast<T>(13)- z[65];
    z[86]=z[10]*z[86];
    z[96]=z[15]*z[19];
    z[97]= - static_cast<T>(6)+ z[96];
    z[97]=z[12]*z[97];
    z[68]=n<T>(17,2) + 4*z[68];
    z[68]=z[5]*z[68];
    z[20]=z[60] + z[20] + 3*z[68] + z[97] + n<T>(87,2)*z[11] + z[86] + z[63]
    + z[64] - 40*z[8] + z[79] + 32*z[2];
    z[20]=z[6]*z[20];
    z[60]=z[2] - z[17];
    z[63]=z[60] + z[36];
    z[63]=z[63]*z[11];
    z[64]=n<T>(17,2)*z[53];
    z[68]= - z[64] + z[72];
    z[86]= - z[36] + n<T>(19,4)*z[9] - z[16] + z[52];
    z[86]=z[12]*z[86];
    z[83]= - 13*z[9] - n<T>(99,4)*z[8] + z[2] + z[83];
    z[83]=z[4]*z[83];
    z[52]= - n<T>(7,4)*z[5] - z[52] + 5*z[9];
    z[52]=z[6]*z[52];
    z[52]= - n<T>(43,2)*z[109] + z[52] + z[83] + n<T>(7,4)*z[103] + z[86] + 
    z[63] + n<T>(1,2)*z[68] - z[107];
    z[52]=z[7]*z[52];
    z[68]=z[91] - z[85];
    z[83]=6*z[12];
    z[86]= - z[68]*z[83];
    z[86]= - z[88] + z[86];
    z[97]=n<T>(41,4)*z[6];
    z[99]=z[97]*z[80];
    z[92]=z[99] - z[92];
    z[99]=z[80]*z[5];
    z[103]=18*z[99] - z[92];
    z[103]=z[6]*z[103];
    z[112]= - z[93] - 24*z[91];
    z[112]=z[5]*z[112];
    z[86]=z[103] + 2*z[86] + z[112];
    z[86]=z[6]*z[86];
    z[93]=z[84] - z[93] + 24*z[85];
    z[93]=z[4]*z[93];
    z[68]=z[68]*z[90];
    z[103]=z[111] + n<T>(41,4)*z[109];
    z[77]=z[77]*z[4];
    z[109]=n<T>(21,2)*z[77] - z[103];
    z[109]=z[7]*z[109];
    z[68]=z[109] + z[68] + z[93];
    z[68]=z[7]*z[68];
    z[93]= - z[64] - z[100];
    z[93]=z[5]*z[93];
    z[87]=z[93] + n<T>(41,2)*z[87];
    z[87]=z[87]*z[106];
    z[93]= - z[116]*z[98];
    z[106]=z[64] + z[107];
    z[106]=z[4]*z[106];
    z[106]=z[106] + n<T>(41,2)*z[110];
    z[106]=z[106]*z[113];
    z[87]=z[106] + z[93] + z[87];
    z[87]=z[18]*z[87];
    z[93]=n<T>(1,4)*z[115];
    z[106]=n<T>(7,4)*z[12];
    z[109]=2*z[19];
    z[110]= - z[109] + z[106];
    z[110]=z[110]*z[105];
    z[111]=85*z[72] + 89*z[101];
    z[111]=z[12]*z[111];
    z[111]=z[111] - 129*z[105];
    z[111]=z[111]*z[117];
    z[68]=z[87] + z[68] + z[86] + z[111] - z[93] + z[110];
    z[68]=z[18]*z[68];
    z[86]=n<T>(9,2)*z[2];
    z[87]=n<T>(9,2)*z[9];
    z[80]=z[87] - z[86] + 23*z[80];
    z[80]=z[6]*z[80];
    z[110]=12*z[10];
    z[111]= - z[110] + z[19] + z[70];
    z[111]=z[10]*z[111];
    z[112]= - z[11]*z[89];
    z[113]=14*z[5] + n<T>(59,4)*z[8] + 9*z[11];
    z[113]=z[5]*z[113];
    z[80]=n<T>(1,2)*z[80] + z[113] + z[112] + z[111] + z[72] + z[94];
    z[80]=z[6]*z[80];
    z[94]= - 19*z[53] + n<T>(83,2)*z[72];
    z[111]=22*z[12] + n<T>(149,4)*z[9] - z[36];
    z[111]=z[12]*z[111];
    z[112]= - n<T>(187,4)*z[12] - z[2] + 38*z[8];
    z[112]=z[5]*z[112];
    z[81]=z[112] + z[111] + z[81] + n<T>(1,2)*z[94] - z[107];
    z[81]=z[4]*z[81];
    z[94]=z[19] + n<T>(19,2)*z[12];
    z[94]=z[12]*z[94];
    z[63]= - z[104] + z[94] + n<T>(19,2)*z[53] - z[63];
    z[63]=z[5]*z[63];
    z[94]=n<T>(17,2)*z[72];
    z[104]=z[109] - n<T>(19,2)*z[9];
    z[104]=z[12]*z[104];
    z[104]= - z[94] + z[104];
    z[104]=z[12]*z[104];
    z[52]=z[68] + z[52] + z[80] + z[81] + z[63] - 2*z[88] + z[104];
    z[52]=z[18]*z[52];
    z[63]= - z[13] + z[44];
    z[63]=z[63]*z[36];
    z[68]= - z[13]*z[110];
    z[63]=z[63] + n<T>(13,2) + z[68];
    z[63]=z[11]*z[63];
    z[68]=z[10]*z[13];
    z[80]=z[12]*z[44];
    z[68]= - 4*z[80] + n<T>(33,4) - 8*z[68];
    z[57]=z[68]*z[57];
    z[68]= - z[15]*z[71];
    z[68]= - static_cast<T>(6)+ z[68];
    z[68]=z[10]*z[68];
    z[57]=z[57] + z[63] + z[68] - n<T>(15,4)*z[56] + n<T>(93,4)*z[8] + 3*z[16]
    + z[82];
    z[57]=z[5]*z[57];
    z[63]= - static_cast<T>(3)- z[49];
    z[51]=z[63]*z[51];
    z[63]= - z[16] - z[19];
    z[51]=z[51] - z[110] - z[9] + 2*z[63] - z[86];
    z[51]=z[12]*z[51];
    z[63]=z[27] + z[23];
    z[63]=z[63]*z[4];
    z[63]=z[63] + z[16];
    z[68]=static_cast<T>(79)- 15*z[29];
    z[68]=z[68]*z[75];
    z[65]=static_cast<T>(29)+ z[65];
    z[65]=z[10]*z[65];
    z[65]= - n<T>(17,2)*z[5] + n<T>(49,2)*z[12] - 24*z[11] + z[65] + z[68] + n<T>(15,4)*z[54] + n<T>(43,4)*z[8] + 2*z[2] + z[63];
    z[65]=z[4]*z[65];
    z[68]=z[109] - z[70];
    z[68]=z[10]*z[68];
    z[60]=2*z[60] + z[10];
    z[60]=z[60]*z[36];
    z[20]=z[35] + z[52] + z[31] + z[21] + z[20] + z[65] + z[57] + z[51]
    + z[60] - z[94] + z[68];
    z[20]=z[1]*z[20];
    z[21]=z[30]*z[32];
    z[31]= - z[32]*z[50];
    z[31]=z[21] + z[31];
    z[31]=z[31]*z[61];
    z[32]=43*z[13];
    z[35]= - z[32] + 173*z[14];
    z[35]=z[35]*z[14];
    z[21]= - z[35] + 17*z[21];
    z[21]=z[9]*z[21];
    z[38]=z[14]*z[38];
    z[21]= - 11*z[38] + z[21];
    z[38]=z[22]*z[76];
    z[35]=n<T>(1,4)*z[35] + z[38];
    z[35]=z[11]*z[35];
    z[21]=z[31] + n<T>(1,4)*z[21] + z[35];
    z[21]=z[3]*z[21];
    z[31]= - z[14]*z[42];
    z[31]=2*z[43] + z[31];
    z[31]=z[31]*z[46];
    z[35]=12*z[44];
    z[32]= - z[32] - 255*z[14];
    z[31]=z[31] + n<T>(1,4)*z[32] - z[35];
    z[31]=z[11]*z[31];
    z[32]= - static_cast<T>(12)+ z[49];
    z[32]=z[15]*z[32];
    z[32]=18*z[13] + z[32];
    z[32]=z[15]*z[32];
    z[32]= - 12*z[43] + z[32];
    z[32]=z[12]*z[32];
    z[38]=n<T>(49,4)*z[2];
    z[42]=5*z[16] + z[38];
    z[42]=z[15]*z[42];
    z[42]= - static_cast<T>(34)+ z[42];
    z[42]=z[15]*z[42];
    z[32]=z[32] + z[35] + n<T>(163,4)*z[13] + z[42];
    z[32]=z[12]*z[32];
    z[42]=z[5] + z[10];
    z[44]=6*z[4];
    z[51]=npow(z[15],2);
    z[44]=z[44]*z[51]*z[42];
    z[52]=z[8] + z[2];
    z[52]=z[58] + n<T>(27,4)*z[52];
    z[52]=z[15]*z[52];
    z[57]=z[13] - 5*z[14];
    z[33]=z[57]*z[33];
    z[33]=81*z[14] + z[33];
    z[33]=z[33]*z[41];
    z[57]=4*z[13] - z[15];
    z[57]=z[57]*z[110];
    z[60]=6*z[6];
    z[65]=z[60]*z[51];
    z[42]=z[12] + z[42];
    z[42]=z[42]*z[65];
    z[58]= - z[58] + n<T>(45,4)*z[8];
    z[58]=z[14]*z[58];
    z[68]=z[5]*z[13];
    z[21]=z[21] + z[42] + z[44] + 24*z[68] + z[32] + z[31] + z[57] + 
    z[33] + z[58] - static_cast<T>(8)+ z[52];
    z[21]=z[3]*z[21];
    z[31]=npow(z[14],4);
    z[32]=z[31]*z[9];
    z[33]=107*z[37] - n<T>(17,2)*z[32];
    z[33]=z[33]*z[41];
    z[42]= - z[31]*z[46];
    z[37]= - n<T>(107,2)*z[37] + z[42];
    z[37]=z[11]*z[37];
    z[31]=z[11]*z[31];
    z[31]= - z[32] + z[31];
    z[31]=z[31]*z[61];
    z[31]=z[31] + z[33] + z[37];
    z[31]=z[3]*z[31];
    z[32]= - n<T>(433,2)*z[22] + 51*z[39];
    z[32]=z[32]*z[41];
    z[22]=113*z[22] + 18*z[73];
    z[22]=z[11]*z[22];
    z[22]=z[31] + z[65] + z[22] + z[32] - 3*z[15] - n<T>(11,2)*z[14];
    z[22]=z[3]*z[22];
    z[30]=181*z[14] - n<T>(219,2)*z[30];
    z[30]=z[30]*z[41];
    z[31]=z[51]*z[4];
    z[32]=4*z[15] + z[31];
    z[32]=z[32]*z[60];
    z[33]= - z[79] - 20*z[8];
    z[33]=z[15]*z[33];
    z[37]=z[14]*z[19];
    z[39]= - n<T>(233,2)*z[14] - 30*z[50];
    z[39]=z[11]*z[39];
    z[22]=z[22] + z[32] + n<T>(15,4)*z[69] + z[62] + z[39] + n<T>(17,4)*z[23] + 
    z[30] - 6*z[37] + static_cast<T>(52)+ z[33];
    z[22]=z[7]*z[22];
    z[30]=n<T>(59,2)*z[99] - z[92];
    z[30]=z[6]*z[30];
    z[32]= - z[64] - 18*z[91];
    z[32]=z[5]*z[32];
    z[30]=z[30] - z[88] + z[32];
    z[30]=z[6]*z[30];
    z[32]=z[84] - z[64] + z[107];
    z[32]=z[4]*z[32];
    z[33]= - 11*z[77] - z[103];
    z[33]=z[7]*z[33];
    z[32]=z[32] + z[33];
    z[32]=z[7]*z[32];
    z[33]= - z[19] + z[106];
    z[33]=z[33]*z[105];
    z[37]=z[114] + 5*z[101];
    z[37]=z[12]*z[37];
    z[37]=z[37] - 27*z[105];
    z[37]=z[37]*z[98];
    z[30]=z[102] + z[32] + z[30] + z[37] - z[93] + z[33];
    z[30]=z[18]*z[30];
    z[32]= - z[59] + z[109] + z[38];
    z[32]=z[10]*z[32];
    z[33]=n<T>(17,2)*z[54] + 59*z[8] + n<T>(11,2)*z[95];
    z[37]=z[67] - z[25];
    z[37]=z[37]*z[97];
    z[26]= - n<T>(59,2) + z[26];
    z[26]=z[11]*z[26];
    z[26]=z[37] + n<T>(1,2)*z[33] + z[26];
    z[26]=z[6]*z[26];
    z[33]=z[11] - z[10];
    z[33]=z[33]*z[90];
    z[37]= - n<T>(139,4)*z[8] + 59*z[11];
    z[37]=z[5]*z[37];
    z[26]=z[26] + z[37] - z[33] + 30*z[91] + z[64] + z[32];
    z[26]=z[6]*z[26];
    z[32]=z[40] + z[23];
    z[32]=z[32]*z[7];
    z[37]=61*z[8] - z[66];
    z[39]=6*z[23];
    z[40]=n<T>(61,4) - z[39];
    z[40]=z[10]*z[40];
    z[37]= - n<T>(41,4)*z[32] + n<T>(1,4)*z[37] + z[40];
    z[37]=z[7]*z[37];
    z[40]=17*z[53] - 33*z[72];
    z[42]= - n<T>(23,4)*z[10] + n<T>(25,2)*z[9] - z[16] + n<T>(25,4)*z[8];
    z[42]=z[4]*z[42];
    z[33]=z[37] + z[42] + z[33] - z[100] + n<T>(1,2)*z[40] + z[108];
    z[33]=z[7]*z[33];
    z[37]=z[53] + z[72];
    z[37]=n<T>(5,4)*z[37] + 2*z[85];
    z[40]=n<T>(5,4)*z[9] + z[12];
    z[40]=z[12]*z[40];
    z[42]=n<T>(25,2)*z[8] - 11*z[12];
    z[42]=z[5]*z[42];
    z[37]=z[84] + z[42] + 3*z[37] + 5*z[40];
    z[37]=z[4]*z[37];
    z[40]= - n<T>(5,4)*z[53] - 4*z[91];
    z[42]=z[12]*z[13];
    z[44]=static_cast<T>(51)- 7*z[42];
    z[44]=z[12]*z[44];
    z[44]=z[109] + n<T>(1,4)*z[44];
    z[44]=z[12]*z[44];
    z[40]=3*z[40] + z[44];
    z[40]=z[5]*z[40];
    z[44]=z[9]*z[13];
    z[46]= - static_cast<T>(27)+ 17*z[44];
    z[46]=z[46]*z[75];
    z[50]=z[13]*z[101];
    z[46]=n<T>(7,4)*z[50] + z[19] + z[46];
    z[46]=z[12]*z[46];
    z[46]= - z[94] + z[46];
    z[46]=z[12]*z[46];
    z[26]=z[30] + z[33] + z[26] + z[37] + z[40] - z[88] + z[46];
    z[26]=z[18]*z[26];
    z[30]=11*z[2];
    z[33]=z[30] + 123*z[8];
    z[33]=z[14]*z[33];
    z[33]= - 123*z[25] + z[33] - 11*z[29];
    z[33]=z[6]*z[33];
    z[37]=3*z[10];
    z[40]=4*z[23];
    z[46]=n<T>(1,2) - z[40];
    z[46]=z[46]*z[37];
    z[50]= - n<T>(69,4) + 4*z[25];
    z[50]=z[50]*z[55];
    z[33]=n<T>(1,4)*z[33] + 38*z[5] + z[50] + z[46] + z[87] + n<T>(51,4)*z[54]
    + n<T>(79,2)*z[8] - z[19] - z[38];
    z[33]=z[6]*z[33];
    z[46]= - static_cast<T>(19)+ 25*z[29];
    z[46]=z[9]*z[46];
    z[39]= - n<T>(133,4) - z[39];
    z[39]=z[10]*z[39];
    z[50]=n<T>(259,4) + 30*z[25];
    z[50]=z[11]*z[50];
    z[32]=n<T>(83,4)*z[32] + 24*z[6] - n<T>(25,4)*z[4] + z[89] + z[50] + z[39]
    + 2*z[46] + n<T>(51,4)*z[56] - z[17] - n<T>(39,2)*z[8];
    z[32]=z[7]*z[32];
    z[39]=n<T>(3,2) + z[40];
    z[37]=z[39]*z[37];
    z[39]= - n<T>(7,2) - 11*z[34];
    z[39]=z[12]*z[39];
    z[37]=n<T>(25,4)*z[5] + z[39] + z[37] + z[87] - n<T>(9,2)*z[8] + z[63];
    z[37]=z[4]*z[37];
    z[39]=z[47]*z[23];
    z[38]=z[39] + z[79] + z[38];
    z[38]=z[10]*z[38];
    z[39]= - z[96] - n<T>(11,2)*z[44];
    z[39]=z[12]*z[39];
    z[40]=static_cast<T>(18)- n<T>(17,4)*z[44];
    z[40]=z[9]*z[40];
    z[39]=z[39] - z[79] + z[40];
    z[39]=z[12]*z[39];
    z[40]= - z[13]*z[74];
    z[40]=n<T>(53,4) + z[40];
    z[40]=z[40]*z[55];
    z[42]= - n<T>(59,2) - z[42];
    z[42]=z[12]*z[42];
    z[40]=n<T>(1,2)*z[42] + z[40] - n<T>(27,2)*z[8] - z[19] + z[17] + 6*z[16];
    z[40]=z[5]*z[40];
    z[26]=z[26] + z[32] + z[33] + z[37] + z[40] + z[39] + z[38] + z[78];
    z[26]=z[18]*z[26];
    z[30]=z[30] + 149*z[8];
    z[30]=z[30]*z[45];
    z[30]= - n<T>(45,2)*z[29] - static_cast<T>(11)+ z[30];
    z[31]=z[31]*z[83];
    z[24]=z[31] + n<T>(31,2)*z[28] + z[62] - n<T>(53,2)*z[25] + n<T>(1,2)*z[30] + 
    z[24];
    z[24]=z[6]*z[24];
    z[25]= - 2*z[13] + z[15];
    z[25]=z[10]*z[25];
    z[25]=4*z[25] + n<T>(5,2) + z[27];
    z[30]= - z[43]*z[36];
    z[30]=z[30] + n<T>(139,4)*z[13] + z[35];
    z[30]=z[11]*z[30];
    z[31]=z[43]*z[90];
    z[31]=z[31] + n<T>(53,4)*z[13] - z[35];
    z[31]=z[12]*z[31];
    z[25]=z[31] + 3*z[25] + z[30];
    z[25]=z[5]*z[25];
    z[27]=n<T>(15,4)*z[28] - 7*z[34] - 5*z[23] + n<T>(39,2) + 2*z[27];
    z[27]=z[4]*z[27];
    z[28]=z[79] + z[48];
    z[28]=z[15]*z[28];
    z[30]=static_cast<T>(6)+ z[49];
    z[30]=z[30]*z[34];
    z[31]= - z[13]*z[41];
    z[28]=z[30] + z[31] + n<T>(17,4) + z[28];
    z[28]=z[12]*z[28];
    z[30]=z[56] - z[54];
    z[29]= - static_cast<T>(11)+ n<T>(85,2)*z[29];
    z[29]=z[29]*z[41];
    z[31]= - z[79] - n<T>(29,4)*z[2];
    z[23]=z[31]*z[23];
    z[31]=z[13] + 4*z[14];
    z[31]=z[31]*z[74];
    z[31]= - n<T>(21,4) + z[31];
    z[31]=z[31]*z[55];
    z[32]= - 6*z[17] - z[16];
    z[20]=z[20] + z[26] + z[22] + z[21] + z[24] + z[27] + z[25] + z[28]
    + z[31] + z[23] + z[29] + n<T>(113,4)*z[8] + 4*z[32] - n<T>(19,2)*z[2] - 49.
   /4.*z[30];

    r += z[20]*z[1];
 
    return r;
}

template double qqb_2lNLC_r513(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r513(const std::array<dd_real,31>&);
#endif
