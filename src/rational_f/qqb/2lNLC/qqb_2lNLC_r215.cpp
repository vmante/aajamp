#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r215(const std::array<T,31>& k) {
  T z[137];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[8];
    z[7]=k[14];
    z[8]=k[10];
    z[9]=k[15];
    z[10]=k[3];
    z[11]=k[4];
    z[12]=k[5];
    z[13]=k[2];
    z[14]=k[9];
    z[15]=npow(z[1],5);
    z[16]=z[15]*z[3];
    z[17]=npow(z[1],4);
    z[18]=z[16] - z[17];
    z[19]=z[18]*z[2];
    z[20]=npow(z[1],3);
    z[21]=5*z[20];
    z[22]=z[17]*z[3];
    z[23]=6*z[19] - z[21] + n<T>(9,2)*z[22];
    z[23]=z[2]*z[23];
    z[24]=z[20]*z[2];
    z[25]=npow(z[1],2);
    z[26]=z[24] + z[25];
    z[27]=npow(z[2],2);
    z[28]=7*z[27];
    z[26]=z[26]*z[28];
    z[28]=npow(z[2],3);
    z[29]=z[25]*z[11];
    z[30]=z[28]*z[29];
    z[26]=z[26] - z[30];
    z[26]=z[26]*z[11];
    z[31]=z[25]*z[3];
    z[26]=z[26] - z[31];
    z[32]=7*z[25];
    z[33]=z[32] + 4*z[24];
    z[33]=z[2]*z[33];
    z[33]=z[33] - z[26];
    z[33]=z[11]*z[33];
    z[34]=3*z[20];
    z[35]= - z[5]*z[17];
    z[35]=z[35] + z[34] - z[29];
    z[36]=z[20]*z[11];
    z[37]=z[36] - z[17];
    z[38]=z[5]*z[15];
    z[38]=z[38] + z[37];
    z[38]=z[4]*z[38];
    z[35]=n<T>(1,2)*z[35] + z[38];
    z[35]=z[4]*z[35];
    z[38]=z[28]*z[36];
    z[39]=z[20]*z[27];
    z[39]=z[39] - z[38];
    z[39]=z[11]*z[39];
    z[40]=z[17]*z[2];
    z[41]= - z[20] - z[40];
    z[41]=z[2]*z[41];
    z[42]=z[20]*z[3];
    z[39]=z[39] + z[42] + z[41];
    z[39]=z[11]*z[39];
    z[41]=npow(z[1],6);
    z[43]=z[41]*z[3];
    z[44]=z[43] - z[15];
    z[45]=z[2]*z[44];
    z[45]=z[16] + z[45];
    z[45]=z[2]*z[45];
    z[39]=z[45] + z[39];
    z[39]=z[8]*z[39];
    z[45]=3*z[3];
    z[46]=z[45]*z[20];
    z[47]=3*z[29];
    z[48]= - z[20] - z[47];
    z[49]=n<T>(5,4)*z[5];
    z[48]=z[48]*z[49];
    z[50]=n<T>(5,2)*z[25];
    z[23]=2*z[39] + n<T>(5,2)*z[35] + z[48] + z[33] + z[23] + z[50] - z[46];
    z[23]=z[8]*z[23];
    z[33]=3*z[12];
    z[35]=z[20]*z[33];
    z[35]=n<T>(11,2)*z[17] + z[35];
    z[35]=z[5]*z[35];
    z[39]=z[25]*z[12];
    z[35]=z[35] + n<T>(5,2)*z[29] - z[34] - n<T>(1,2)*z[39];
    z[35]=z[4]*z[35];
    z[48]=2*z[20];
    z[51]= - z[48] - n<T>(3,2)*z[39];
    z[52]=3*z[5];
    z[51]=z[51]*z[52];
    z[53]=z[12]*z[1];
    z[54]=3*z[53];
    z[55]=z[11]*z[1];
    z[35]=z[35] + z[51] + n<T>(3,2)*z[55] - n<T>(7,4)*z[25] + z[54];
    z[35]=z[4]*z[35];
    z[51]=z[45]*z[17];
    z[56]=2*z[2];
    z[57]= - z[17]*z[56];
    z[58]=7*z[20];
    z[59]=z[12]*z[42];
    z[57]=z[57] + z[59] - z[58] + z[51];
    z[57]=z[57]*z[56];
    z[59]=n<T>(17,2)*z[25];
    z[60]= - z[59] + z[46];
    z[60]=z[12]*z[60];
    z[61]=4*z[20];
    z[60]=z[60] - z[61] + z[51];
    z[60]=z[3]*z[60];
    z[62]=6*z[25];
    z[57]=z[57] - z[62] + z[60];
    z[57]=z[2]*z[57];
    z[60]= - 25*z[20] - 11*z[40];
    z[60]=z[2]*z[60];
    z[63]=13*z[25];
    z[60]= - z[63] + z[60];
    z[60]=z[2]*z[60];
    z[26]=z[60] + z[26];
    z[26]=z[11]*z[26];
    z[60]= - z[61] - 3*z[40];
    z[60]=z[60]*z[27];
    z[38]=z[60] + z[38];
    z[38]=z[11]*z[38];
    z[60]=z[15]*z[2];
    z[61]=8*z[17] + 3*z[60];
    z[61]=z[2]*z[61];
    z[64]=6*z[20];
    z[61]=z[64] + z[61];
    z[61]=z[2]*z[61];
    z[38]=z[61] + z[38];
    z[38]=z[11]*z[38];
    z[61]= - z[2]*z[41];
    z[61]= - 4*z[15] + z[61];
    z[61]=z[2]*z[61];
    z[61]= - 6*z[17] + z[61];
    z[61]=z[2]*z[61];
    z[38]=z[38] - z[48] + z[61];
    z[38]=z[6]*z[38];
    z[60]=18*z[17] + 5*z[60];
    z[60]=z[2]*z[60];
    z[60]=18*z[20] + z[60];
    z[60]=z[2]*z[60];
    z[26]=z[38] + z[26] + z[62] + z[60];
    z[26]=z[6]*z[26];
    z[38]=npow(z[3],3);
    z[60]=z[38]*z[53];
    z[61]=4*z[31];
    z[65]=3*z[1];
    z[66]= - z[65] + z[61];
    z[67]=npow(z[3],2);
    z[66]=z[66]*z[67];
    z[66]=z[66] + z[60];
    z[66]=z[66]*z[33];
    z[68]=4*z[25];
    z[69]= - z[68] + z[46];
    z[70]=6*z[3];
    z[69]=z[69]*z[70];
    z[71]=7*z[1];
    z[69]=z[71] + z[69];
    z[69]=z[3]*z[69];
    z[66]=z[69] + z[66];
    z[66]=z[12]*z[66];
    z[69]= - z[58] + 4*z[22];
    z[69]=z[3]*z[69];
    z[69]=z[68] + z[69];
    z[69]=z[69]*z[45];
    z[66]=z[69] + z[66];
    z[66]=z[12]*z[66];
    z[69]=2*z[17];
    z[72]= - z[69] + z[16];
    z[72]=z[72]*z[45];
    z[72]=z[21] + z[72];
    z[72]=z[3]*z[72];
    z[66]=z[66] + z[25] + z[72];
    z[66]=z[12]*z[66];
    z[72]=z[31] - z[1];
    z[73]=2*z[67];
    z[72]=z[72]*z[73];
    z[73]=n<T>(1,2)*z[60];
    z[72]=z[72] + z[73];
    z[74]=z[72]*z[33];
    z[75]=2*z[25];
    z[76]=z[75] - z[42];
    z[77]=9*z[3];
    z[78]=z[76]*z[77];
    z[78]= - z[78] + n<T>(17,2)*z[1];
    z[78]=z[78]*z[3];
    z[74]=z[78] + z[74];
    z[74]=z[74]*z[12];
    z[78]=4*z[1];
    z[74]=z[74] - z[78];
    z[79]=z[34] - z[22];
    z[79]=z[79]*z[70];
    z[79]=z[79] - 17*z[25];
    z[79]=z[79]*z[3];
    z[79]=z[79] - z[74];
    z[79]=z[79]*z[12];
    z[79]=z[79] + z[68];
    z[80]=n<T>(1,2)*z[3];
    z[81]=z[80]*z[15];
    z[82]= - z[69] + z[81];
    z[82]=z[82]*z[45];
    z[83]=n<T>(17,2)*z[20];
    z[82]=z[83] + z[82];
    z[82]=z[3]*z[82];
    z[82]=z[82] - z[79];
    z[84]=npow(z[12],2);
    z[82]=z[9]*z[82]*z[84];
    z[66]=z[66] + z[82];
    z[66]=z[9]*z[66];
    z[82]=z[75]*z[3];
    z[85]=z[82] - z[1];
    z[85]=z[85]*z[67];
    z[85]=z[85] + z[73];
    z[85]=z[85]*z[12];
    z[86]=z[42] - z[25];
    z[87]=z[86]*z[45];
    z[88]=n<T>(1,2)*z[1];
    z[87]=z[88] + z[87];
    z[87]=z[3]*z[87];
    z[87]=z[87] + z[85];
    z[87]=z[87]*z[33];
    z[89]=z[22] - z[20];
    z[70]=z[89]*z[70];
    z[70]= - z[32] + z[70];
    z[70]=z[3]*z[70];
    z[90]=2*z[1];
    z[70]=z[87] - z[90] + z[70];
    z[70]=z[12]*z[70];
    z[87]=z[15]*z[67];
    z[91]= - 19*z[20] + 3*z[87];
    z[91]=z[91]*z[80];
    z[92]=12*z[25];
    z[66]=z[66] + z[70] + z[92] + z[91];
    z[66]=z[9]*z[66];
    z[70]= - z[53]*z[80];
    z[91]=z[67]*z[53];
    z[93]= - z[1] - z[31];
    z[93]=z[3]*z[93];
    z[93]=z[93] - z[91];
    z[93]=z[11]*z[93];
    z[94]=2*z[12];
    z[95]=z[94]*z[25];
    z[96]=z[20]*z[12];
    z[97]=z[96]*z[9];
    z[97]=z[97] - z[95];
    z[98]=z[97]*z[9];
    z[99]=n<T>(3,4)*z[25] - z[98];
    z[99]=z[99]*z[52];
    z[100]=5*z[1];
    z[23]=z[23] + z[35] + z[99] + z[66] + z[26] + n<T>(1,4)*z[93] + z[57] + 
    z[70] + z[100] + n<T>(1,2)*z[31];
    z[23]=z[7]*z[23];
    z[26]=z[75]*z[11];
    z[35]=z[26]*z[27];
    z[57]=n<T>(25,4)*z[1];
    z[35]=z[35] - z[57];
    z[66]=z[68] + z[24];
    z[66]=z[66]*z[56];
    z[66]=z[66] - z[35];
    z[66]=z[11]*z[66];
    z[70]= - z[6]*z[37];
    z[70]=z[70] - z[34] + 8*z[29];
    z[70]=z[6]*z[70];
    z[93]=z[11]*z[34];
    z[93]= - z[17] + z[93];
    z[93]=z[9]*z[93];
    z[93]=z[93] + z[34] - 5*z[29];
    z[99]=n<T>(5,4)*z[9];
    z[93]=z[93]*z[99];
    z[101]=z[26]*z[2];
    z[101]=z[101] - z[24];
    z[101]=z[101]*z[11];
    z[101]=z[101] - z[40];
    z[102]=z[20] - z[26];
    z[102]=z[11]*z[102];
    z[102]=z[17] + z[102];
    z[102]=z[6]*z[102];
    z[102]=z[102] - z[101];
    z[102]=z[5]*z[102];
    z[103]= - z[2]*z[34];
    z[66]=z[102] + z[93] + z[70] + z[103] + z[66];
    z[66]=z[5]*z[66];
    z[70]= - z[27]*z[75];
    z[70]=z[70] + z[30];
    z[70]=z[11]*z[70];
    z[93]=z[75] + z[24];
    z[93]=z[2]*z[93];
    z[70]=z[93] + z[70];
    z[93]=2*z[11];
    z[70]=z[70]*z[93];
    z[102]= - z[9]*z[37];
    z[103]=z[4]*z[36];
    z[102]=z[103] + z[102];
    z[49]=z[49]*z[102];
    z[18]=z[18]*z[3];
    z[102]= - 13*z[20] - 5*z[18];
    z[103]=n<T>(1,4)*z[3];
    z[102]=z[102]*z[103];
    z[19]= - z[20] - z[19];
    z[19]=z[19]*z[56];
    z[104]=z[42]*z[11];
    z[105]= - z[22] + z[104];
    z[99]=z[105]*z[99];
    z[19]=z[99] + z[70] + z[102] + z[19] + z[49];
    z[19]=z[8]*z[19];
    z[49]=n<T>(31,2)*z[25] - 5*z[42];
    z[49]=z[49]*z[80];
    z[70]=z[25]*z[2];
    z[99]=z[55]*z[2];
    z[49]= - n<T>(3,2)*z[99] + n<T>(17,2)*z[70] + z[65] + z[49];
    z[49]=z[11]*z[49];
    z[102]=n<T>(3,2)*z[31];
    z[105]= - z[99] + n<T>(11,2)*z[70] + z[90] + z[102];
    z[105]=z[11]*z[105];
    z[105]=z[105] - n<T>(9,2)*z[24] - z[25] - n<T>(21,4)*z[42];
    z[105]=z[11]*z[105];
    z[106]=n<T>(1,2)*z[99] - z[1] - z[70];
    z[106]=z[11]*z[106];
    z[107]=n<T>(1,2)*z[24];
    z[106]=z[106] + z[25] + z[107];
    z[108]=z[9]*npow(z[11],2);
    z[106]=z[106]*z[108];
    z[105]=z[106] + n<T>(5,4)*z[22] + z[105];
    z[105]=z[9]*z[105];
    z[49]=z[105] + z[49] + n<T>(13,2)*z[24] - z[68] - n<T>(33,4)*z[42];
    z[49]=z[9]*z[49];
    z[105]=z[22] + z[20];
    z[76]=z[11]*z[76];
    z[76]=z[76] + z[105];
    z[76]=z[3]*z[76];
    z[106]= - z[20] + 2*z[22];
    z[106]=z[3]*z[106];
    z[109]= - z[67]*z[36];
    z[106]=z[106] + z[109];
    z[106]=z[11]*z[106];
    z[18]=z[106] + z[48] - z[18];
    z[18]=z[6]*z[18];
    z[18]=z[18] - z[62] + z[76];
    z[18]=z[6]*z[18];
    z[76]=5*z[17];
    z[106]= - z[76] + z[16];
    z[106]=z[3]*z[106];
    z[109]=11*z[20];
    z[106]=z[109] + z[106];
    z[106]=z[3]*z[106];
    z[110]=5*z[25];
    z[106]= - z[110] + z[106];
    z[111]=z[2]*z[89];
    z[106]=n<T>(5,4)*z[106] - 4*z[111];
    z[106]=z[2]*z[106];
    z[111]=n<T>(5,2)*z[1];
    z[112]= - z[2]*z[68];
    z[112]= - z[111] + z[112];
    z[112]=z[2]*z[112];
    z[30]=z[112] + 4*z[30];
    z[30]=z[11]*z[30];
    z[112]= - z[20] - n<T>(3,2)*z[22];
    z[112]=z[3]*z[112];
    z[113]=n<T>(1,2)*z[25];
    z[112]=z[113] + z[112];
    z[112]=z[3]*z[112];
    z[112]= - z[65] + z[112];
    z[114]=n<T>(1,2)*z[5];
    z[37]=z[4]*z[37]*z[114];
    z[37]=z[25] + z[37];
    z[37]=z[4]*z[37];
    z[18]=z[19] + 5*z[37] + z[66] + z[49] + z[18] + z[30] + n<T>(5,2)*z[112]
    + z[106];
    z[18]=z[8]*z[18];
    z[19]=z[38]*z[29];
    z[29]=n<T>(13,2)*z[25] + z[46];
    z[29]=z[29]*z[67];
    z[30]=n<T>(1,2)*z[38];
    z[30]= - z[39]*z[30];
    z[29]=3*z[19] + z[29] + z[30];
    z[29]=z[11]*z[29];
    z[30]=z[105]*z[45];
    z[30]=z[32] + z[30];
    z[30]=z[3]*z[30];
    z[37]=n<T>(1,2)*z[42];
    z[49]= - z[25] - z[37];
    z[66]=z[67]*z[12];
    z[49]=z[49]*z[66];
    z[29]=z[29] + z[30] + z[49];
    z[29]=z[11]*z[29];
    z[30]=z[44]*z[3];
    z[30]=z[30] + z[17];
    z[44]=z[30]*z[45];
    z[49]=z[87] + z[20];
    z[106]=z[80]*z[12];
    z[112]= - z[49]*z[106];
    z[44]=z[44] + z[112];
    z[44]=z[2]*z[44];
    z[112]=z[49]*z[45];
    z[114]= - z[105]*z[80];
    z[114]= - z[25] + z[114];
    z[114]=z[12]*z[3]*z[114];
    z[101]=z[6]*z[101];
    z[29]=z[101] + z[29] + z[44] + z[114] + z[25] + z[112];
    z[29]=z[5]*z[29];
    z[44]= - z[100] - z[70];
    z[44]=z[11]*z[44];
    z[70]=n<T>(9,2)*z[53];
    z[101]=2*z[24];
    z[44]=z[44] + z[101] + z[32] + z[70];
    z[44]=z[11]*z[44];
    z[112]= - z[90] + z[31];
    z[114]=2*z[3];
    z[112]=z[112]*z[114];
    z[112]=z[112] + z[91];
    z[112]=z[12]*z[112];
    z[115]= - z[62] + z[42];
    z[115]=z[3]*z[115];
    z[112]=z[112] + z[65] + z[115];
    z[112]=z[12]*z[112];
    z[112]= - 2*z[86] + z[112];
    z[112]=z[112]*z[84];
    z[115]=z[78]*z[84];
    z[116]=z[54] + z[25];
    z[117]= - z[55] + z[116];
    z[117]=z[11]*z[117];
    z[117]=z[115] + z[117];
    z[117]=z[11]*z[117];
    z[112]=2*z[112] + z[117];
    z[112]=z[6]*z[112];
    z[117]=6*z[91];
    z[118]=n<T>(25,2)*z[1];
    z[119]=z[118] - 8*z[31];
    z[119]=z[3]*z[119];
    z[119]=z[119] - z[117];
    z[119]=z[12]*z[119];
    z[120]=z[110] - 2*z[42];
    z[120]=z[3]*z[120];
    z[119]=z[119] - z[111] + z[120];
    z[119]=z[12]*z[119];
    z[120]=z[63] - 7*z[42];
    z[119]=n<T>(1,2)*z[120] + z[119];
    z[119]=z[12]*z[119];
    z[40]=z[112] + z[44] + z[119] - z[40];
    z[40]=z[6]*z[40];
    z[44]= - 9*z[25] - z[101];
    z[44]=z[2]*z[44];
    z[35]=z[44] + z[35];
    z[35]=z[11]*z[35];
    z[44]= - z[110] - z[42];
    z[61]= - n<T>(5,4)*z[1] + z[61];
    z[61]=z[3]*z[61];
    z[61]=z[61] + z[117];
    z[61]=z[12]*z[61];
    z[61]=z[61] - n<T>(43,4)*z[1] + 6*z[31];
    z[61]=z[12]*z[61];
    z[35]=z[40] + z[35] + 6*z[24] + n<T>(3,4)*z[44] + z[61];
    z[35]=z[6]*z[35];
    z[37]= - z[63] + z[37];
    z[40]= - z[34] + z[95];
    z[40]=z[2]*z[40];
    z[44]=n<T>(1,2)*z[20];
    z[61]=z[44] + z[39];
    z[63]=z[96]*z[6];
    z[61]=3*z[61] - z[63];
    z[61]=z[6]*z[61];
    z[37]=z[61] + n<T>(1,2)*z[37] + z[40];
    z[40]=n<T>(1,2)*z[22];
    z[61]=z[34] - z[40];
    z[24]=z[33]*z[24];
    z[24]= - n<T>(3,2)*z[63] - n<T>(9,4)*z[104] + n<T>(1,2)*z[61] + z[24];
    z[24]=z[9]*z[24];
    z[61]= - z[59] + z[42];
    z[61]=z[61]*z[80];
    z[47]= - z[67]*z[47];
    z[47]=z[61] + z[47];
    z[47]=z[11]*z[47];
    z[24]=z[24] + z[47] + 3*z[37];
    z[24]=z[9]*z[24];
    z[37]= - z[75] - n<T>(9,4)*z[42];
    z[37]=z[3]*z[37];
    z[37]=n<T>(1,4)*z[1] + z[37];
    z[37]=z[37]*z[45];
    z[47]=z[25]*z[45];
    z[47]= - z[1] + z[47];
    z[47]=z[47]*z[66];
    z[37]= - 6*z[19] + z[37] + n<T>(1,4)*z[47];
    z[37]=z[11]*z[37];
    z[47]=z[42] + z[25];
    z[47]=z[47]*z[3];
    z[61]= - z[100] + z[47];
    z[61]=z[3]*z[61];
    z[61]=n<T>(3,4)*z[61] - 2*z[91];
    z[61]=z[12]*z[61];
    z[63]=29*z[17] - 27*z[16];
    z[63]=z[63]*z[103];
    z[63]=z[20] + z[63];
    z[63]=z[3]*z[63];
    z[95]=z[67]*z[17];
    z[101]= - z[25] + n<T>(3,2)*z[95];
    z[101]=z[101]*z[106];
    z[63]=z[101] + z[113] + z[63];
    z[63]=z[2]*z[63];
    z[101]=z[20] - n<T>(27,2)*z[22];
    z[101]=z[3]*z[101];
    z[101]=z[50] + z[101];
    z[101]=z[3]*z[101];
    z[101]=z[65] + z[101];
    z[24]=z[29] + z[24] + z[35] + z[37] + z[63] + n<T>(1,2)*z[101] + z[61];
    z[24]=z[5]*z[24];
    z[29]=z[34] + z[39];
    z[29]=z[12]*z[29];
    z[35]=3*z[17];
    z[29]=z[35] + z[29];
    z[29]=z[12]*z[29];
    z[29]=z[15] + z[29];
    z[29]=z[29]*z[94];
    z[37]=z[68] + z[53];
    z[61]=z[37]*z[12];
    z[61]=z[61] + z[64];
    z[63]=z[61]*z[12];
    z[64]=4*z[17];
    z[63]=z[63] + z[64];
    z[101]= - z[12]*z[63];
    z[101]= - z[15] + z[101];
    z[103]=z[84]*z[2];
    z[101]=z[101]*z[103];
    z[29]=z[29] + z[101];
    z[29]=z[2]*z[29];
    z[101]=16*z[25];
    z[104]=z[101] + 7*z[53];
    z[104]=z[12]*z[104];
    z[112]=10*z[20];
    z[104]=z[112] + z[104];
    z[104]=z[104]*z[84];
    z[29]=z[29] - z[15] + z[104];
    z[29]=z[2]*z[29];
    z[104]=z[75] + z[53];
    z[117]=z[104]*z[12];
    z[119]=z[117] + z[20];
    z[120]=z[53] + z[25];
    z[121]= - z[11]*z[120];
    z[121]=2*z[119] + z[121];
    z[121]=z[121]*z[93];
    z[122]=z[32] + z[54];
    z[122]=z[122]*z[12];
    z[123]= - z[21] - z[122];
    z[123]=z[12]*z[123];
    z[123]= - z[17] + z[123];
    z[29]=z[121] + 2*z[123] + z[29];
    z[29]=z[6]*z[29];
    z[121]=3*z[25];
    z[123]=z[121] + z[53];
    z[124]=z[123]*z[12];
    z[125]=z[124] + z[34];
    z[126]=z[12]*z[125];
    z[126]=z[17] + z[126];
    z[126]=z[126]*z[84]*z[56];
    z[127]=z[84]*z[1];
    z[128]= - z[34] + z[127];
    z[128]=z[12]*z[128];
    z[69]= - z[69] + z[128];
    z[69]=z[12]*z[69];
    z[69]=z[69] + z[126];
    z[69]=z[69]*z[56];
    z[126]= - 33*z[25] - 19*z[53];
    z[126]=z[12]*z[126];
    z[126]= - 14*z[20] + z[126];
    z[126]=z[12]*z[126];
    z[69]=z[126] + z[69];
    z[69]=z[2]*z[69];
    z[126]=13*z[53];
    z[128]=18*z[25] + z[126];
    z[128]=z[12]*z[128];
    z[129]=2*z[53];
    z[130]=z[129] + z[25];
    z[93]= - z[130]*z[93];
    z[29]=z[29] + z[93] + z[69] + z[21] + z[128];
    z[29]=z[6]*z[29];
    z[69]=z[120]*z[84];
    z[93]= - z[119]*z[103];
    z[93]= - z[69] + z[93];
    z[93]=z[2]*z[93];
    z[128]=40*z[25] + n<T>(79,2)*z[53];
    z[128]=z[12]*z[128];
    z[93]=6*z[93] + z[21] + z[128];
    z[93]=z[2]*z[93];
    z[128]=5*z[53];
    z[131]=z[128] + z[68];
    z[29]=z[29] + 2*z[55] - 4*z[131] + z[93];
    z[29]=z[6]*z[29];
    z[36]=z[36]*z[38];
    z[93]=3*z[67];
    z[89]=z[89]*z[93];
    z[89]=z[89] - z[36];
    z[93]=n<T>(1,2)*z[11];
    z[89]=z[89]*z[93];
    z[81]=z[17] - z[81];
    z[81]=z[3]*z[81];
    z[81]= - z[20] + z[81];
    z[81]=z[81]*z[45];
    z[81]=z[81] + z[89];
    z[81]=z[11]*z[81];
    z[89]=z[96] + z[17];
    z[89]=z[89]*z[6];
    z[132]= - 3*z[15] + z[43];
    z[132]=z[132]*z[80];
    z[132]=z[35] + z[132];
    z[132]=z[3]*z[132];
    z[133]=n<T>(7,2)*z[20];
    z[81]=n<T>(1,2)*z[89] + z[81] - z[133] + z[132];
    z[81]=z[9]*z[81];
    z[132]=z[86]*z[67];
    z[132]= - z[19] + 7*z[132];
    z[134]= - z[132]*z[93];
    z[135]= - z[112] + n<T>(11,2)*z[22];
    z[135]=z[3]*z[135];
    z[136]=8*z[25];
    z[135]=z[136] + z[135];
    z[135]=z[3]*z[135];
    z[134]=z[135] + z[134];
    z[134]=z[11]*z[134];
    z[135]= - z[25]*z[33];
    z[89]=z[89] - z[133] + z[135];
    z[89]=z[6]*z[89];
    z[133]=5*z[16];
    z[135]=13*z[17] - z[133];
    z[135]=z[135]*z[80];
    z[112]= - z[112] + z[135];
    z[112]=z[3]*z[112];
    z[81]=z[81] + z[89] + z[134] + z[62] + z[112];
    z[81]=z[9]*z[81];
    z[89]=z[3]*npow(z[1],7);
    z[41]= - 2*z[41] + z[89];
    z[41]=z[3]*z[41];
    z[41]=2*z[15] + z[41];
    z[41]=z[3]*z[41];
    z[41]=z[96] - z[17] + z[41];
    z[41]=z[2]*z[41];
    z[89]=z[105]*z[67];
    z[36]=z[89] + z[36];
    z[36]=z[11]*z[36];
    z[49]=z[3]*z[49];
    z[36]=z[49] + z[36];
    z[36]=z[11]*z[36];
    z[30]=z[3]*z[30];
    z[30]=z[36] + z[41] - z[20] + z[30];
    z[30]=z[30]*z[52];
    z[36]=8*z[15] - n<T>(7,2)*z[43];
    z[36]=z[3]*z[36];
    z[36]= - n<T>(17,4)*z[17] + z[36];
    z[36]=z[36]*z[45];
    z[36]= - 5*z[39] + z[20] + z[36];
    z[36]=z[2]*z[36];
    z[41]=3*z[11];
    z[43]= - z[132]*z[41];
    z[49]=n<T>(13,2)*z[20] - 21*z[22];
    z[49]=z[3]*z[49];
    z[49]=21*z[25] + z[49];
    z[49]=z[3]*z[49];
    z[43]=z[49] + z[43];
    z[43]=z[43]*z[93];
    z[49]=9*z[17];
    z[52]=z[49] - 7*z[16];
    z[52]=z[3]*z[52];
    z[52]=z[20] + z[52];
    z[52]=z[52]*z[45];
    z[52]=19*z[25] + z[52];
    z[30]=z[30] + z[43] + n<T>(1,2)*z[52] + z[36];
    z[30]=z[5]*z[30];
    z[36]=z[75] + z[54];
    z[36]=z[12]*z[36];
    z[43]=z[56]*z[69];
    z[36]=z[36] + z[43];
    z[36]=z[36]*z[56];
    z[43]= - z[49] + z[133];
    z[43]=z[43]*z[45];
    z[43]= - n<T>(11,2)*z[20] + z[43];
    z[43]=z[3]*z[43];
    z[43]=n<T>(21,2)*z[25] + z[43];
    z[36]=z[36] + n<T>(1,2)*z[43] - 33*z[53];
    z[36]=z[2]*z[36];
    z[43]=z[42]*z[93];
    z[43]= - z[34] + z[43];
    z[43]=z[5]*z[43];
    z[43]= - n<T>(1,2)*z[86] + z[43];
    z[43]=z[4]*z[43];
    z[49]= - n<T>(85,2)*z[20] + 27*z[22];
    z[49]=z[49]*z[80];
    z[49]=z[92] + z[49];
    z[49]=z[3]*z[49];
    z[52]=8*z[1];
    z[29]=z[43] + z[30] + 3*z[81] + z[29] + z[36] + z[52] + z[49];
    z[29]=z[4]*z[29];
    z[30]= - z[5]*z[44];
    z[30]= - z[25] + z[30];
    z[36]=5*z[8];
    z[30]=z[30]*z[36];
    z[43]=z[25]*z[5];
    z[30]=z[30] - z[65] - n<T>(11,2)*z[43];
    z[30]=z[8]*z[30];
    z[44]= - z[5]*z[65];
    z[30]=z[44] + z[30];
    z[30]=z[14]*z[30];
    z[44]=z[1] + z[43];
    z[44]=z[44]*z[36];
    z[49]=z[5]*z[1];
    z[44]=8*z[49] + z[44];
    z[44]=z[8]*z[44];
    z[30]=z[44] + z[30];
    z[30]=z[14]*z[30];
    z[44]=npow(z[8],2);
    z[69]= - z[5]*z[44]*z[100];
    z[81]=z[88] + z[43];
    z[81]=z[81]*z[36];
    z[81]=n<T>(11,2)*z[49] + z[81];
    z[86]=z[14]*z[8];
    z[81]=z[81]*z[86];
    z[69]=z[69] + z[81];
    z[69]=z[14]*z[69];
    z[81]=n<T>(5,2)*z[49];
    z[86]=z[10]*npow(z[86],2);
    z[89]= - z[86]*z[81];
    z[69]=z[69] + z[89];
    z[69]=z[10]*z[69];
    z[44]=z[44]*z[49];
    z[30]=z[69] - n<T>(15,4)*z[44] + z[30];
    z[30]=z[10]*z[30];
    z[40]=z[40] - z[20];
    z[44]=z[40]*z[3];
    z[69]= - n<T>(9,4)*z[25] - z[44];
    z[69]=z[69]*z[45];
    z[69]=n<T>(7,2)*z[1] + z[69];
    z[89]=z[103]*z[1];
    z[92]= - z[129] - z[89];
    z[92]=z[2]*z[92];
    z[69]=3*z[69] + z[92];
    z[69]=z[4]*z[2]*z[69];
    z[92]=z[35] - z[16];
    z[92]=z[3]*z[92];
    z[92]= - z[21] + z[92];
    z[92]=z[3]*z[92];
    z[92]=z[121] + z[92];
    z[92]=z[2]*z[92];
    z[93]= - z[25] - z[55];
    z[93]=z[5]*z[93];
    z[92]=z[93] + z[92] + z[99];
    z[93]=n<T>(1,2)*z[8];
    z[92]=z[92]*z[93];
    z[44]= - n<T>(3,2)*z[25] - z[44];
    z[44]=z[3]*z[44];
    z[44]=z[88] + z[44];
    z[44]=z[2]*z[44];
    z[44]=z[92] + 3*z[44] + z[81];
    z[44]=z[8]*z[44];
    z[81]=z[25]*z[8];
    z[43]= - z[43] - z[81];
    z[36]=z[43]*z[36];
    z[43]=z[8]*z[21];
    z[43]=z[25] + z[43];
    z[43]=z[43]*z[93];
    z[92]= - z[4]*z[53];
    z[92]= - z[1] + z[92];
    z[43]=3*z[92] + z[43];
    z[43]=z[14]*z[43];
    z[36]=z[43] - 6*z[49] + z[36];
    z[36]=z[14]*z[36];
    z[28]=z[28]*z[55];
    z[43]= - z[2]*z[53];
    z[43]= - z[65] + z[43];
    z[43]=z[43]*z[27];
    z[30]=z[30] + z[36] + n<T>(5,2)*z[44] + z[69] + z[43] + z[28];
    z[30]=z[10]*z[30];
    z[36]=z[121] + z[129];
    z[36]=z[36]*z[94];
    z[36]=z[34] + z[36];
    z[36]=z[2]*z[36];
    z[36]=4*z[130] + z[36];
    z[36]=z[2]*z[36];
    z[36]=z[100] + z[36];
    z[36]=z[2]*z[36];
    z[43]= - z[25]*z[67];
    z[44]= - z[2]*z[130];
    z[44]= - z[1] + z[44];
    z[44]=z[11]*z[44]*z[27];
    z[36]=z[44] + z[43] + z[36];
    z[36]=z[11]*z[36];
    z[43]=z[128] + z[62];
    z[44]= - z[43]*z[94];
    z[49]=z[123]*z[94];
    z[69]= - z[34] - z[49];
    z[69]=z[69]*z[94];
    z[35]= - z[35] + z[69];
    z[35]=z[2]*z[35];
    z[35]=z[35] - z[21] + z[44];
    z[35]=z[2]*z[35];
    z[35]=z[35] - z[136] - z[70];
    z[35]=z[2]*z[35];
    z[44]=z[121] + 4*z[42];
    z[44]=z[3]*z[44];
    z[69]=z[90]*z[3];
    z[70]=z[69] - z[91];
    z[92]=z[70]*z[94];
    z[35]=z[36] + z[35] + z[44] + z[92];
    z[35]=z[11]*z[35];
    z[36]= - z[12]*z[116];
    z[37]= - z[37]*z[103];
    z[36]=z[36] + z[37];
    z[36]=z[2]*z[36];
    z[37]=z[53] + z[89];
    z[37]=z[2]*z[37];
    z[37]=z[1] + z[37];
    z[37]=z[11]*z[37];
    z[36]=z[37] + z[36] - z[116];
    z[36]=z[2]*z[36];
    z[37]=z[1] + z[82];
    z[37]=z[3]*z[37];
    z[37]=z[37] + z[91];
    z[37]=z[12]*z[37];
    z[37]=z[47] + z[37];
    z[36]=2*z[37] + z[36];
    z[36]=z[11]*z[36];
    z[37]= - z[68] - z[46];
    z[37]=z[3]*z[37];
    z[44]= - z[12]*z[70];
    z[37]=z[37] + z[44];
    z[37]=z[12]*z[37];
    z[44]= - z[105]*z[114];
    z[37]=z[44] + z[37];
    z[44]= - z[34] + z[49];
    z[44]=z[12]*z[44];
    z[49]=z[61]*z[103];
    z[44]=z[44] + z[49];
    z[44]=z[2]*z[44];
    z[44]= - z[115] + z[44];
    z[44]=z[2]*z[44];
    z[36]=z[36] + 2*z[37] + z[44];
    z[36]=z[11]*z[36];
    z[37]= - z[65] + z[31];
    z[37]=z[3]*z[37];
    z[37]=z[37] + z[91];
    z[37]=z[12]*z[37];
    z[37]= - z[82] + z[37];
    z[37]=z[12]*z[37];
    z[44]=z[48] + z[22];
    z[44]=z[3]*z[44];
    z[37]=z[44] + z[37];
    z[37]=z[12]*z[37];
    z[16]=z[17] + z[16];
    z[16]=z[3]*z[16];
    z[16]=z[16] + z[37];
    z[37]=z[104]*z[84];
    z[44]=z[76] - z[37];
    z[44]=z[12]*z[44];
    z[49]= - z[63]*z[103];
    z[44]=z[44] + z[49];
    z[44]=z[2]*z[44];
    z[49]=z[34] + z[122];
    z[49]=z[49]*z[94];
    z[44]=z[44] - z[17] + z[49];
    z[44]=z[2]*z[44];
    z[16]=z[36] + 2*z[16] + z[44];
    z[16]=z[6]*z[16];
    z[36]=z[111] - z[82];
    z[36]=z[3]*z[36];
    z[36]=z[36] - 4*z[91];
    z[36]=z[12]*z[36];
    z[36]= - 11*z[31] + z[36];
    z[36]=z[12]*z[36];
    z[37]=z[17] + 3*z[37];
    z[44]=z[125]*z[94];
    z[17]=z[17] + z[44];
    z[17]=z[17]*z[94];
    z[15]=z[15] + z[17];
    z[15]=z[2]*z[15];
    z[15]=2*z[37] + z[15];
    z[15]=z[2]*z[15];
    z[17]= - n<T>(27,2)*z[25] - z[126];
    z[17]=z[12]*z[17];
    z[15]=z[15] + z[34] + z[17];
    z[15]=z[2]*z[15];
    z[17]= - z[83] - z[51];
    z[17]=z[3]*z[17];
    z[15]=z[16] + z[35] + z[15] + z[17] + z[36];
    z[15]=z[6]*z[15];
    z[16]=z[110] + z[54];
    z[16]=z[16]*z[94];
    z[16]=9*z[20] + z[16];
    z[16]=z[2]*z[16];
    z[16]=z[16] + z[101] + 15*z[53];
    z[16]=z[2]*z[16];
    z[16]=z[57] + z[16];
    z[16]=z[2]*z[16];
    z[17]= - z[2]*z[43];
    z[17]= - z[100] + z[17];
    z[17]=z[17]*z[27];
    z[17]=z[17] + z[28];
    z[17]=z[11]*z[17];
    z[16]=z[17] - z[69] + z[16];
    z[16]=z[11]*z[16];
    z[17]= - z[110] - 4*z[53];
    z[17]=z[17]*z[33];
    z[21]= - z[21] - 6*z[117];
    z[21]=z[12]*z[21];
    z[21]= - z[64] + z[21];
    z[21]=z[2]*z[21];
    z[17]=z[21] - z[109] + z[17];
    z[17]=z[2]*z[17];
    z[17]=z[17] + z[62] + n<T>(83,4)*z[53];
    z[17]=z[2]*z[17];
    z[21]=z[3]*z[100];
    z[21]=z[21] + z[91];
    z[21]=z[12]*z[21];
    z[21]=7*z[31] + z[21];
    z[15]=z[15] + z[16] + 2*z[21] + z[17];
    z[15]=z[6]*z[15];
    z[16]=z[75] + z[46];
    z[16]=z[3]*z[16];
    z[16]= - z[65] + z[16];
    z[16]=z[3]*z[16];
    z[16]=z[16] + z[91];
    z[16]=z[16]*z[33];
    z[17]= - z[90] - z[31];
    z[17]=z[17]*z[67];
    z[17]=z[17] + z[73];
    z[17]=z[12]*z[17];
    z[21]=z[67]*z[34];
    z[21]=z[100] + z[21];
    z[21]=z[21]*z[80];
    z[17]=z[21] + z[17];
    z[21]= - z[1]*z[56];
    z[17]= - n<T>(3,2)*z[19] + 3*z[17] + z[21];
    z[17]=z[11]*z[17];
    z[19]= - z[75] - n<T>(1,2)*z[95];
    z[19]=z[19]*z[77];
    z[21]=n<T>(19,2)*z[25] - z[53];
    z[21]=z[2]*z[21];
    z[16]=z[17] + z[21] + z[16] + z[52] + z[19];
    z[16]=z[11]*z[16];
    z[17]= - z[12]*z[72];
    z[19]= - z[1] + z[102];
    z[19]=z[19]*z[45];
    z[17]=z[19] + z[17];
    z[17]=z[17]*z[33];
    z[19]= - z[67]*z[65];
    z[19]=z[19] + z[60];
    z[19]=z[12]*z[19];
    z[21]=z[1]*z[45];
    z[19]=z[21] + z[19];
    z[21]=z[2]*z[1];
    z[19]=n<T>(3,2)*z[19] + z[21];
    z[19]=z[11]*z[19];
    z[35]= - z[75] + n<T>(1,2)*z[53];
    z[35]=z[2]*z[35];
    z[17]=z[19] + z[35] + z[17] + z[78] - 9*z[31];
    z[17]=z[11]*z[17];
    z[19]=z[75] - n<T>(3,2)*z[42];
    z[19]=z[19]*z[77];
    z[19]=z[19] + z[74];
    z[19]=z[12]*z[19];
    z[35]=z[20] - z[39];
    z[35]=z[2]*z[35];
    z[36]=n<T>(9,2)*z[42];
    z[17]=z[17] + z[35] + z[19] - z[68] + z[36];
    z[17]=z[11]*z[17];
    z[19]=z[40]*z[77];
    z[19]=z[107] + z[19] + z[79];
    z[19]=z[12]*z[19];
    z[17]=z[17] + z[19];
    z[17]=z[9]*z[17];
    z[19]=z[75] - z[46];
    z[19]=z[3]*z[19];
    z[19]=z[88] + z[19];
    z[19]=z[3]*z[19];
    z[19]=z[19] - z[85];
    z[19]=z[19]*z[33];
    z[35]=z[51] - z[48];
    z[37]= - z[35]*z[45];
    z[37]=z[68] + z[37];
    z[37]=z[3]*z[37];
    z[19]=z[19] - z[78] + z[37];
    z[19]=z[12]*z[19];
    z[37]=z[58] + z[87];
    z[37]=z[3]*z[37];
    z[39]= - n<T>(9,2)*z[20] + z[39];
    z[39]=z[2]*z[39];
    z[16]=z[17] + z[16] + z[39] + z[19] - z[110] + n<T>(3,2)*z[37];
    z[16]=z[9]*z[16];
    z[17]= - z[75] + z[36];
    z[17]=z[3]*z[17];
    z[17]=z[1] + z[17];
    z[17]=z[3]*z[17];
    z[19]= - z[88] - z[31];
    z[19]=z[19]*z[66];
    z[26]= - z[38]*z[26];
    z[17]=z[26] - z[21] + z[17] + z[19];
    z[17]=z[17]*z[41];
    z[19]= - z[3]*z[35];
    z[19]= - z[32] + z[19];
    z[19]=z[3]*z[19];
    z[21]= - z[1] + z[47];
    z[21]=z[21]*z[106];
    z[19]=z[21] + z[90] + z[19];
    z[21]=25*z[25] - n<T>(3,2)*z[53];
    z[21]=z[2]*z[21];
    z[16]=z[16] + z[17] + 3*z[19] + z[21];
    z[16]=z[9]*z[16];
    z[17]=n<T>(11,2)*z[25] + z[54];
    z[17]=z[12]*z[17];
    z[19]=z[33]*z[4];
    z[21]=z[119]*z[19];
    z[17]=z[17] + z[21];
    z[17]=z[7]*z[17];
    z[19]= - z[120]*z[19];
    z[21]=z[20]*z[8];
    z[17]=z[17] + n<T>(5,2)*z[21] + z[19] - z[50] - z[54];
    z[17]=z[14]*z[17];
    z[19]=z[53] + z[98];
    z[19]=z[5]*z[19];
    z[26]=z[34] - z[97];
    z[26]=z[9]*z[26];
    z[26]= - z[25] + z[26];
    z[26]=z[4]*z[26];
    z[19]=z[26] + z[90] + z[19];
    z[26]= - z[48] - z[124];
    z[26]=z[4]*z[26];
    z[26]=3*z[26] - z[59] - 6*z[53];
    z[26]=z[7]*z[26];
    z[17]=z[17] + 3*z[19] + z[26];
    z[17]=z[14]*z[17];
    z[19]= - z[53] + z[55];
    z[19]=z[9]*z[19];
    z[26]=z[9]*z[127];
    z[26]= - z[129] + z[26];
    z[26]=z[7]*z[26];
    z[19]=z[26] + z[65] + z[19];
    z[19]=z[19]*npow(z[9],2);
    z[26]= - z[88] + z[81];
    z[26]=z[8]*z[26]*npow(z[14],2);
    z[31]= - z[88]*z[86];
    z[26]=z[26] + z[31];
    z[26]=z[10]*z[26];
    z[31]= - z[1]*z[108];
    z[31]= - 4*z[55] + z[31];
    z[31]=z[9]*z[31];
    z[31]= - z[65] + z[31];
    z[31]=z[8]*z[9]*z[31];
    z[21]=z[25] - z[21];
    z[21]=z[8]*z[21];
    z[25]=z[7]*z[53];
    z[21]=z[25] - z[1] + z[21];
    z[21]=z[14]*z[21];
    z[25]=z[8] - z[7];
    z[25]=z[1]*z[25];
    z[21]=z[21] + z[25];
    z[21]=z[14]*z[21];
    z[19]=5*z[26] + n<T>(5,2)*z[21] + z[31] + z[19];
    z[19]=z[13]*z[19];
    z[21]=z[2]*z[120]*z[94];
    z[21]=z[21] - z[42] + z[131];
    z[21]=z[21]*z[56];
    z[20]= - z[20] + n<T>(1,4)*z[22];
    z[20]=z[3]*z[20];
    z[20]= - z[75] + z[20];
    z[20]=z[20]*z[45];
    z[20]=z[21] - z[118] + z[20];
    z[20]=z[2]*z[20];
    z[21]= - z[130]*z[56];
    z[21]= - z[71] + z[21];
    z[21]=z[21]*z[27];
    z[21]=z[21] + 2*z[28];
    z[21]=z[11]*z[21];
    z[22]= - z[113] + z[42];
    z[22]=z[3]*z[22];
    z[22]= - n<T>(9,4)*z[1] + z[22];
    z[22]=z[22]*z[45];

    r += z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22]
       + z[23] + z[24] + z[29] + z[30];
 
    return r;
}

template double qqb_2lNLC_r215(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r215(const std::array<dd_real,31>&);
#endif
