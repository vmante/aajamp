#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r903(const std::array<T,31>& k) {
  T z[28];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[11];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[3];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=k[5];
    z[11]=k[14];
    z[12]=k[8];
    z[13]=k[15];
    z[14]=z[11]*z[9];
    z[15]=z[3]*z[12];
    z[16]=z[15] + z[14];
    z[17]=z[11]*z[10];
    z[18]=z[17] - 1;
    z[19]=z[11]*npow(z[10],2);
    z[19]= - z[10] + z[19];
    z[19]=z[8]*z[19];
    z[19]=z[19] + z[18];
    z[20]=2*z[8];
    z[19]=z[19]*z[20];
    z[19]=z[19] - z[9] + 2*z[11];
    z[19]=z[8]*z[19];
    z[21]=z[11] - z[9];
    z[22]=z[3] + z[12];
    z[23]= - z[22] - z[21];
    z[23]=n<T>(1,2)*z[23] + z[8];
    z[23]=z[6]*z[23];
    z[16]=z[23] + n<T>(1,2)*z[16] + z[19];
    z[16]=z[5]*z[16];
    z[19]=z[13]*z[9];
    z[23]=z[11]*z[13];
    z[24]=z[19] - z[23];
    z[25]=z[9]*z[7];
    z[18]= - z[25] + z[18];
    z[18]=z[8]*z[18];
    z[18]=z[18] + z[21];
    z[18]=z[18]*z[20];
    z[18]=z[18] + z[24];
    z[18]=z[8]*z[18];
    z[22]=z[22]*z[4];
    z[14]=z[22] - z[14];
    z[26]=z[9]*npow(z[7],2);
    z[26]= - z[7] + z[26];
    z[26]=z[8]*z[26];
    z[25]=z[25] + z[26];
    z[25]=z[25]*z[20];
    z[26]=2*z[9];
    z[25]=z[25] + z[26] - z[11];
    z[25]=z[8]*z[25];
    z[14]=n<T>(1,2)*z[14] + z[25];
    z[14]=z[6]*z[14];
    z[22]=z[22] - z[15];
    z[22]=z[22]*z[13];
    z[25]= - z[9]*z[23];
    z[14]=z[16] + z[14] + z[18] + n<T>(1,2)*z[22] + z[25];
    z[16]=npow(z[2],2);
    z[14]=z[14]*z[16];
    z[18]=z[4] - z[12];
    z[25]= - z[26] - z[18];
    z[25]=z[11]*z[25];
    z[27]=z[4]*z[3];
    z[21]=z[8]*z[21];
    z[21]= - n<T>(3,2)*z[21] + z[25] - n<T>(1,2)*z[15] + z[27];
    z[21]=z[6]*z[21];
    z[24]=n<T>(3,2)*z[24];
    z[17]=z[8]*z[17];
    z[17]=z[11] + z[17];
    z[17]=z[17]*z[20];
    z[17]=z[24] + z[17];
    z[17]=z[8]*z[17];
    z[15]= - z[15] + z[27];
    z[15]=z[13]*z[15];
    z[18]= - n<T>(3,2)*z[9] - z[18];
    z[18]=z[18]*z[23];
    z[15]=z[21] + z[17] + z[15] + z[18];
    z[15]=z[5]*z[15];
    z[17]=npow(z[4],3);
    z[18]=z[17] - z[22];
    z[20]=z[4]*z[12];
    z[19]= - z[20] + n<T>(3,2)*z[19];
    z[19]=z[11]*z[19];
    z[21]= - z[7]*npow(z[8],2)*z[26];
    z[21]= - z[24] + z[21];
    z[21]=z[8]*z[21];
    z[18]=z[21] + n<T>(1,2)*z[18] + z[19];
    z[18]=z[6]*z[18];
    z[19]=z[1]*z[5];
    z[19]=z[19] + 1;
    z[17]=z[17]*z[3];
    z[19]=z[19]*z[17]*z[6];
    z[21]=z[3] + z[4];
    z[21]=z[6]*z[21]*npow(z[4],2);
    z[17]= - z[17] + z[21];
    z[17]=z[5]*z[17];
    z[17]=z[17] + z[19];
    z[17]=z[1]*z[17];
    z[19]= - z[23]*z[20];
    z[20]=npow(z[8],3)*z[26];
    z[15]=n<T>(1,2)*z[17] + z[15] + z[18] + z[19] + z[20];
    z[15]=z[1]*z[16]*z[15];
    z[14]=z[14] + z[15];

    r += z[14]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r903(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r903(const std::array<dd_real,31>&);
#endif
