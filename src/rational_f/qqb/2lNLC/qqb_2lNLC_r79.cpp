#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r79(const std::array<T,31>& k) {
  T z[30];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[14];
    z[7]=k[3];
    z[8]=k[9];
    z[9]=k[13];
    z[10]=k[4];
    z[11]=k[15];
    z[12]=k[5];
    z[13]=k[12];
    z[14]=z[13]*z[9];
    z[15]=z[3]*z[5];
    z[16]= - z[15] + n<T>(1,2)*z[14];
    z[17]=z[9]*z[7];
    z[18]=z[17] - 1;
    z[19]= - z[9]*npow(z[7],2);
    z[19]=z[7] + z[19];
    z[19]=z[8]*z[19];
    z[19]=z[19] - z[18];
    z[20]=2*z[8];
    z[19]=z[19]*z[20];
    z[19]=z[19] - 2*z[9] + z[13];
    z[19]=z[8]*z[19];
    z[21]=z[3]*z[10];
    z[22]= - z[3]*npow(z[10],2);
    z[22]=z[10] + z[22];
    z[22]=z[11]*z[22];
    z[22]=z[22] + static_cast<T>(1)- 2*z[21];
    z[22]=z[11]*z[22];
    z[23]=n<T>(1,2)*z[5];
    z[22]=z[22] - n<T>(5,2)*z[3] + z[23] - z[8];
    z[22]=z[11]*z[22];
    z[24]=z[3] - z[5];
    z[25]=z[13] - z[9];
    z[26]= - z[25] + z[24];
    z[26]=n<T>(1,2)*z[26] + 2*z[11];
    z[26]=z[6]*z[26];
    z[19]=z[26] + z[22] + z[19] - z[16];
    z[19]=z[4]*z[19];
    z[22]=z[13]*z[12];
    z[26]=z[22] + 1;
    z[27]=npow(z[12],2);
    z[28]= - z[13]*z[27];
    z[28]= - z[12] + z[28];
    z[28]=z[8]*z[28];
    z[28]=z[28] - z[26];
    z[28]=z[28]*z[20];
    z[28]=z[28] + z[9] - 2*z[13];
    z[28]=z[8]*z[28];
    z[29]=n<T>(1,2)*z[3];
    z[27]= - z[5]*z[27];
    z[27]=z[12] + z[27];
    z[27]=z[27]*npow(z[11],2);
    z[27]=z[27] + z[29] - n<T>(3,2)*z[5] + z[8];
    z[27]=z[11]*z[27];
    z[16]=z[27] + z[28] + z[16];
    z[16]=z[6]*z[16];
    z[17]=z[17] + z[26];
    z[17]=z[8]*z[17];
    z[17]=z[13] + z[17];
    z[17]=z[17]*z[20];
    z[17]= - z[14] + z[17];
    z[17]=z[8]*z[17];
    z[26]=z[5]*z[12];
    z[27]= - z[21] - static_cast<T>(1)+ z[26];
    z[27]=z[11]*z[27];
    z[27]=z[27] + z[5] - 2*z[3];
    z[27]=z[11]*z[27];
    z[27]=2*z[15] + z[27];
    z[27]=z[11]*z[27];
    z[16]=z[19] + z[16] + z[17] + z[27];
    z[17]=npow(z[2],2);
    z[16]=z[16]*z[17];
    z[19]=z[25]*z[8];
    z[19]= - 4*z[15] + n<T>(3,2)*z[19];
    z[21]=z[11]*z[21];
    z[21]=z[29] + z[21];
    z[21]=z[11]*z[21];
    z[21]=z[21] - z[19];
    z[21]=z[11]*z[21];
    z[15]=z[14] - z[15];
    z[24]=z[25] + z[24];
    z[24]=z[11]*z[24];
    z[15]=2*z[15] + n<T>(3,2)*z[24];
    z[15]=z[6]*z[15];
    z[14]=n<T>(3,2)*z[14];
    z[18]=z[18]*npow(z[8],2);
    z[18]= - z[14] + 2*z[18];
    z[18]=z[8]*z[18];
    z[24]=npow(z[5],2);
    z[25]=npow(z[3],2);
    z[27]= - z[24] - n<T>(1,2)*z[25];
    z[27]=z[3]*z[27];
    z[15]=z[15] + z[21] + z[18] + z[27];
    z[15]=z[4]*z[15];
    z[18]= - z[11]*z[26];
    z[18]= - z[23] + z[18];
    z[18]=z[11]*z[18];
    z[18]=z[18] + z[19];
    z[18]=z[11]*z[18];
    z[19]= - z[8]*z[22];
    z[19]= - z[13] + z[19];
    z[19]=z[19]*z[20];
    z[14]=z[14] + z[19];
    z[14]=z[8]*z[14];
    z[19]=z[25]*z[5];
    z[20]=npow(z[5],3);
    z[14]=z[18] - z[19] - n<T>(1,2)*z[20] + z[14];
    z[14]=z[6]*z[14];
    z[18]=z[19] + z[20];
    z[18]=z[18]*z[3];
    z[19]=z[5] + z[3];
    z[19]=z[3]*z[19];
    z[19]= - z[24] + z[19];
    z[19]=z[3]*z[19];
    z[19]= - z[20] + z[19];
    z[19]=z[6]*z[19];
    z[19]=z[18] + z[19];
    z[19]=z[4]*z[19];
    z[20]= - z[1]*z[4];
    z[20]=z[20] - 1;
    z[18]=z[20]*z[6]*z[18];
    z[18]=z[19] + z[18];
    z[18]=z[1]*z[18];
    z[19]=z[3]*npow(z[11],3);
    z[14]=n<T>(1,2)*z[18] + z[15] + z[19] + z[14];
    z[14]=z[1]*z[17]*z[14];
    z[14]=z[16] + z[14];

    r += z[14]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r79(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r79(const std::array<dd_real,31>&);
#endif
