#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r480(const std::array<T,31>& k) {
  T z[58];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[8];
    z[12]=k[15];
    z[13]=k[9];
    z[14]=k[2];
    z[15]=n<T>(1,2)*z[3];
    z[16]=npow(z[1],3);
    z[17]=z[15]*z[16];
    z[18]=z[10]*z[1];
    z[19]=n<T>(1,2)*z[18];
    z[20]=npow(z[1],2);
    z[21]= - z[17] + 2*z[20] - z[19];
    z[21]=z[2]*z[21];
    z[22]=z[20]*z[3];
    z[23]=z[22] - z[1];
    z[24]=z[23]*z[2];
    z[25]=z[3]*z[1];
    z[24]=z[24] + z[25];
    z[26]=z[25]*z[9];
    z[27]= - z[2]*z[26];
    z[27]=n<T>(3,2)*z[24] + z[27];
    z[27]=z[9]*z[27];
    z[28]=n<T>(5,8)*z[1];
    z[29]=z[4]*z[19];
    z[21]=z[27] + z[21] + z[29] + z[28] - z[22];
    z[21]=z[9]*z[21];
    z[27]=z[9]*z[2];
    z[29]=n<T>(1,2)*z[1];
    z[30]= - z[3]*z[29]*z[27];
    z[24]=z[30] + z[24];
    z[30]=n<T>(1,2)*z[9];
    z[24]=z[24]*z[30];
    z[31]=z[22] - z[29];
    z[32]= - z[20] + n<T>(1,4)*z[18];
    z[33]=z[16]*z[3];
    z[34]=n<T>(1,4)*z[33];
    z[35]= - z[34] - z[32];
    z[35]=z[2]*z[35];
    z[24]=z[24] - n<T>(1,2)*z[31] + z[35];
    z[24]=z[9]*z[24];
    z[35]=z[20]*z[10];
    z[36]=z[35] - z[16];
    z[37]=z[36]*z[2];
    z[38]=z[18] + z[20];
    z[39]=npow(z[10],2);
    z[40]=z[39]*z[4];
    z[41]=z[1]*z[40];
    z[41]=z[41] - z[38];
    z[41]=n<T>(1,2)*z[41] + z[37];
    z[24]=n<T>(1,2)*z[41] + z[24];
    z[24]=z[9]*z[24];
    z[41]= - z[2]*z[16];
    z[41]=z[41] + z[38];
    z[42]=n<T>(1,2)*z[10];
    z[41]=z[42]*z[41];
    z[43]=z[19] + z[20];
    z[44]= - z[43]*z[40];
    z[41]=z[44] + z[41];
    z[44]=z[38]*z[39];
    z[45]=z[43]*z[10];
    z[46]=n<T>(1,2)*z[16];
    z[45]=z[45] + z[46];
    z[40]=z[45]*z[40];
    z[40]= - n<T>(1,2)*z[44] + z[40];
    z[47]=n<T>(1,2)*z[5];
    z[40]=z[40]*z[47];
    z[24]=z[40] + n<T>(1,2)*z[41] + z[24];
    z[24]=z[12]*z[24];
    z[40]=5*z[18];
    z[41]= - 19*z[20] - z[40];
    z[41]=z[4]*z[41]*z[42];
    z[48]=5*z[20];
    z[41]=z[48] + z[41];
    z[37]=n<T>(1,2)*z[41] + z[37];
    z[41]=3*z[20];
    z[49]= - z[41] - z[18];
    z[49]=z[49]*z[42];
    z[50]=n<T>(5,2)*z[20] + z[18];
    z[50]=z[10]*z[50];
    z[50]=n<T>(3,2)*z[16] + z[50];
    z[50]=z[4]*z[10]*z[50];
    z[49]=z[49] + z[50];
    z[49]=z[5]*z[49];
    z[21]=n<T>(5,2)*z[24] + n<T>(5,4)*z[49] + n<T>(1,2)*z[37] + z[21];
    z[21]=z[12]*z[21];
    z[24]=z[1] - z[26];
    z[24]=z[9]*z[24];
    z[37]=z[5]*z[1]*z[39];
    z[24]=z[37] - z[18] + z[24];
    z[24]=z[12]*z[24];
    z[37]=n<T>(3,2)*z[5];
    z[49]=z[18]*z[37];
    z[50]=n<T>(1,4)*z[1];
    z[24]=n<T>(5,4)*z[24] + z[49] - z[50] - z[26];
    z[24]=z[12]*z[24];
    z[49]=z[5]*z[50];
    z[24]=z[49] + z[24];
    z[24]=z[14]*z[24];
    z[23]= - n<T>(5,4)*z[26] + z[23];
    z[23]=z[2]*z[23];
    z[26]=z[4]*z[50];
    z[23]=z[25] + z[26] + z[23];
    z[23]=z[9]*z[23];
    z[26]= - 11*z[20] + z[40];
    z[40]=z[48] + z[18];
    z[40]=z[10]*z[40];
    z[40]=7*z[16] + n<T>(3,2)*z[40];
    z[40]=z[4]*z[40];
    z[26]=n<T>(1,2)*z[26] + z[40];
    z[26]=z[5]*z[26];
    z[32]=z[4]*z[32];
    z[34]=z[20] + z[34];
    z[40]=n<T>(1,2)*z[2];
    z[34]=z[34]*z[40];
    z[21]=n<T>(1,2)*z[24] + z[21] + n<T>(1,4)*z[26] + z[23] + z[34] - z[28] + 
    z[32];
    z[21]=z[12]*z[21];
    z[23]=z[38]*z[10];
    z[24]= - z[16] + z[23];
    z[24]=z[10]*z[24];
    z[26]=npow(z[1],4);
    z[24]= - z[26] + z[24];
    z[28]=z[7]*z[45]*z[39];
    z[24]=n<T>(1,2)*z[24] + z[28];
    z[24]=z[4]*z[24];
    z[28]=z[45]*z[6];
    z[32]= - z[43] - z[28];
    z[32]=z[2]*z[39]*z[32];
    z[34]=z[2] - z[7];
    z[39]=z[39]*z[29]*z[34];
    z[43]=z[4] - z[6];
    z[45]=z[45]*z[43];
    z[39]=z[45] + z[39];
    z[39]=z[9]*z[39];
    z[45]=n<T>(1,2)*z[7];
    z[44]= - z[45]*z[44];
    z[28]=z[10]*z[28];
    z[24]=z[39] + z[32] + z[24] + z[44] + z[28];
    z[28]=n<T>(1,2)*z[11];
    z[24]=z[24]*z[28];
    z[32]=n<T>(1,2)*z[20];
    z[39]=z[32] + z[18];
    z[44]=z[39]*z[10];
    z[44]=z[44] - z[46];
    z[44]=z[44]*z[6];
    z[48]=n<T>(5,2)*z[18];
    z[49]= - z[20] - z[48];
    z[50]=z[7]*z[10];
    z[49]=z[49]*z[50];
    z[49]=z[49] - z[44];
    z[48]=z[41] + z[48];
    z[48]=z[10]*z[48];
    z[48]=z[46] + z[48];
    z[51]=n<T>(3,2)*z[20];
    z[52]=z[51] + z[18];
    z[52]=z[10]*z[52];
    z[52]=z[46] + z[52];
    z[50]=z[52]*z[50];
    z[48]=n<T>(1,2)*z[48] + z[50];
    z[48]=z[4]*z[48];
    z[44]=z[44] - z[51] + z[18];
    z[44]=z[40]*z[10]*z[44];
    z[50]=z[18]*z[34];
    z[38]=z[38]*z[43];
    z[38]=z[38] + z[50];
    z[38]=z[38]*z[30];
    z[24]=z[24] + z[38] + z[44] + n<T>(1,2)*z[49] + z[48];
    z[24]=z[24]*z[28];
    z[28]=z[20] + 3*z[18];
    z[38]=z[28]*z[45];
    z[44]=z[20] - z[19];
    z[44]=z[6]*z[44];
    z[44]=z[38] + z[44];
    z[48]= - n<T>(3,4)*z[20] - z[18];
    z[48]=z[10]*z[48];
    z[48]= - n<T>(3,4)*z[16] + z[48];
    z[48]=z[7]*z[48];
    z[28]= - n<T>(1,8)*z[28] + z[48];
    z[28]=z[4]*z[28];
    z[18]= - z[41] + z[18];
    z[23]=z[16] + n<T>(1,4)*z[23];
    z[23]=z[6]*z[23];
    z[18]=n<T>(1,4)*z[18] + z[23];
    z[18]=z[18]*z[40];
    z[23]= - z[34] - z[43];
    z[23]=z[9]*z[1]*z[23];
    z[18]=z[24] + n<T>(1,8)*z[23] + z[18] + n<T>(1,4)*z[44] + z[28];
    z[18]=z[11]*z[18];
    z[23]=npow(z[7],3);
    z[24]=z[23]*z[20];
    z[28]=z[20]*z[7];
    z[34]=z[28]*npow(z[3],2);
    z[34]=z[34] - z[24];
    z[41]= - z[8] - z[9];
    z[34]=z[15]*z[34]*z[41];
    z[41]=z[16]*z[7];
    z[43]=z[41] + z[20];
    z[44]= - z[43]*z[45];
    z[44]=z[1] + z[44];
    z[44]=z[7]*z[44];
    z[43]=z[3]*z[43];
    z[43]=z[28] + z[43];
    z[43]=z[43]*z[15];
    z[43]=z[44] + z[43];
    z[43]=z[3]*z[43];
    z[44]=z[26]*npow(z[6],3);
    z[48]=z[4]*z[33];
    z[44]=z[48] + 3*z[44] + 7*z[22];
    z[44]=z[4]*z[44];
    z[48]=z[6]*z[36];
    z[48]=z[20] + z[48];
    z[49]=npow(z[6],2);
    z[48]=z[48]*z[49];
    z[44]=3*z[48] + z[44];
    z[40]=z[44]*z[40];
    z[32]= - z[23]*z[32];
    z[32]=z[40] + z[32] + z[43] + z[34];
    z[32]=z[8]*z[32];
    z[34]=npow(z[1],5);
    z[40]=z[34]*z[7];
    z[43]=z[6]*z[40];
    z[43]= - z[16] + z[43];
    z[43]=z[43]*z[15];
    z[44]=z[26]*z[7];
    z[48]=z[44]*z[6];
    z[43]=z[43] - z[41] - n<T>(1,2)*z[48];
    z[43]=z[3]*z[43];
    z[50]=z[46]*z[7];
    z[50]=z[50] + z[20];
    z[51]= - z[7]*z[50];
    z[52]=z[40] + z[26];
    z[52]=z[52]*z[7];
    z[53]= - z[16] - z[52];
    z[45]=z[6]*z[53]*z[45];
    z[43]=z[43] + z[45] - z[1] + z[51];
    z[43]=z[3]*z[43];
    z[45]=z[3]*z[50];
    z[50]= - z[46] - z[35];
    z[50]=z[6]*z[50];
    z[50]= - z[20] + z[50];
    z[50]=z[5]*z[50];
    z[51]= - z[8]*z[7]*z[22];
    z[45]=z[51] + z[50] + z[28] + z[45];
    z[45]=z[13]*z[45];
    z[50]=z[44] - z[16];
    z[51]=z[50]*z[7];
    z[51]=z[51] - z[20];
    z[51]=z[51]*z[7];
    z[50]=z[35] - z[50];
    z[53]=3*z[6];
    z[50]=z[50]*z[53];
    z[50]= - z[20] + z[50];
    z[50]=z[6]*z[50];
    z[50]= - z[51] + z[50];
    z[54]=n<T>(1,2)*z[6];
    z[50]=z[50]*z[54];
    z[55]= - z[7]*z[1];
    z[32]=z[45] + z[32] + z[43] + z[55] + z[50];
    z[43]=z[15]*z[34];
    z[43]=z[43] + z[26];
    z[45]= - z[16]*z[42];
    z[45]=z[45] - z[43];
    z[45]=z[4]*z[45];
    z[50]=z[42]*z[26];
    z[50]=z[50] + z[34];
    z[55]=npow(z[1],6);
    z[56]= - z[55]*z[15];
    z[56]=z[56] - z[50];
    z[56]=z[4]*z[56];
    z[43]=z[56] + z[43];
    z[43]=z[2]*z[43];
    z[42]=z[42]*z[20];
    z[56]=z[42] + z[16];
    z[15]=z[15]*z[26];
    z[57]= - z[15] - z[56];
    z[57]=z[4]*z[57];
    z[15]=z[2]*z[15];
    z[15]=z[57] + z[15];
    z[15]=z[9]*z[15];
    z[15]=z[15] + z[43] - z[42] + z[45];
    z[15]=z[15]*z[37];
    z[17]= - z[27]*z[17];
    z[27]= - z[2]*z[46];
    z[15]=z[15] + z[17] + z[20] + z[27];
    z[15]=z[15]*z[47];
    z[17]= - z[10]*z[16];
    z[17]= - z[26] + z[17];
    z[27]=z[7]*z[50];
    z[17]=n<T>(1,2)*z[17] + z[27];
    z[17]=z[7]*z[17];
    z[27]=3*z[16];
    z[37]= - z[27] - z[35];
    z[17]=n<T>(1,2)*z[37] + z[17];
    z[17]=z[7]*z[17];
    z[17]=z[17] + z[39];
    z[17]=z[6]*z[17];
    z[37]=7*z[1] + z[51];
    z[39]=z[4]*z[3]*z[26];
    z[39]=z[33] - n<T>(11,2)*z[39];
    z[39]=z[2]*z[39];
    z[17]=z[39] + n<T>(1,2)*z[37] + z[17];
    z[37]=z[26]*z[6];
    z[37]=z[37] - z[44];
    z[39]=z[55]*z[7];
    z[39]=z[39] - z[34];
    z[39]=z[39]*z[6];
    z[42]= - z[40] - z[39];
    z[42]=z[3]*z[42];
    z[42]=z[42] - z[37];
    z[42]=z[3]*z[42];
    z[43]= - z[16] + z[52];
    z[43]=z[7]*z[43];
    z[23]=z[55]*z[23];
    z[23]= - z[27] + z[23];
    z[23]=z[6]*z[23];
    z[23]=z[42] + z[43] + z[23];
    z[23]= - z[20] + n<T>(1,8)*z[23];
    z[23]=z[3]*z[23];
    z[27]=npow(z[7],2);
    z[42]=z[27]*z[26];
    z[43]=z[20] - z[42];
    z[43]=z[7]*z[43];
    z[37]= - z[3]*z[37];
    z[45]= - z[6]*z[16];
    z[37]=z[45] + z[37];
    z[37]=z[3]*z[37];
    z[45]=z[6]*z[20];
    z[37]=z[37] + z[43] + z[45];
    z[37]=z[3]*z[37];
    z[43]=z[6]*z[1];
    z[37]=z[43] + z[37];
    z[31]= - z[4]*z[31];
    z[43]=z[2]*z[22];
    z[31]=z[43] + n<T>(1,4)*z[37] + z[31];
    z[31]=z[31]*z[30];
    z[19]= - z[19] - z[33];
    z[19]=z[4]*z[19];
    z[15]=z[15] + z[31] + n<T>(1,2)*z[19] + z[23] + n<T>(1,4)*z[17];
    z[15]=z[5]*z[15];
    z[17]= - z[16] - z[35];
    z[17]=z[10]*z[17];
    z[19]=z[10]*z[26];
    z[19]= - z[34] + z[19];
    z[19]=z[7]*z[19];
    z[17]=z[19] + z[26] + z[17];
    z[17]=z[17]*z[54];
    z[17]=z[17] + n<T>(1,2)*z[44] - z[56];
    z[19]=n<T>(3,2)*z[6];
    z[17]=z[17]*z[19];
    z[23]=z[7]*z[36];
    z[17]=z[23] + z[17];
    z[17]=z[6]*z[17];
    z[17]= - n<T>(1,4)*z[28] + z[17];
    z[23]=z[26] + z[39];
    z[23]=z[6]*z[23];
    z[23]=z[44] + z[23];
    z[23]=z[23]*z[53];
    z[31]=z[7]*z[35];
    z[23]=z[23] - 7*z[20] - 3*z[31];
    z[23]=n<T>(1,8)*z[23] + z[33];
    z[23]=z[4]*z[23];
    z[31]= - z[10]*z[41];
    z[31]= - z[16] + z[31];
    z[31]=z[4]*z[31];
    z[31]=z[20] + z[31];
    z[31]=z[2]*z[31];
    z[17]=n<T>(3,8)*z[31] + n<T>(1,2)*z[17] + z[23];
    z[17]=z[2]*z[17];
    z[23]= - z[26] + z[40];
    z[19]=z[23]*z[19];
    z[19]= - z[16] + z[19];
    z[19]=z[6]*z[19];
    z[19]= - z[41] + z[19];
    z[19]=z[6]*z[19];
    z[19]=z[38] + z[19];
    z[23]=z[20] + z[33];
    z[23]=z[4]*z[23];
    z[19]=n<T>(1,8)*z[23] + n<T>(1,4)*z[19] + z[22];
    z[19]=z[4]*z[19];
    z[16]=z[16]*z[27];
    z[20]=z[20] + z[42];
    z[20]=z[6]*z[20];
    z[16]=z[20] + z[1] + z[16];
    z[16]=z[7]*z[16];
    z[20]= - z[41] - z[48];
    z[20]=z[3]*z[20];
    z[20]=z[28] + z[20];
    z[20]=z[3]*z[20];
    z[16]=z[20] + z[16];
    z[16]=z[3]*z[16];
    z[16]= - z[24] + z[16];
    z[20]=z[41] + n<T>(3,2)*z[48];
    z[20]=z[20]*z[49];
    z[22]=z[7]*z[29];
    z[20]=z[22] + z[20];
    z[20]=z[4]*z[20];
    z[16]=n<T>(1,2)*z[16] + z[20];
    z[20]= - z[2]*z[25];
    z[16]=n<T>(1,2)*z[16] + z[20];
    z[16]=z[16]*z[30];

    r += z[15] + z[16] + z[17] + z[18] + z[19] + z[21] + n<T>(1,4)*z[32];
 
    return r;
}

template double qqb_2lNLC_r480(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r480(const std::array<dd_real,31>&);
#endif
