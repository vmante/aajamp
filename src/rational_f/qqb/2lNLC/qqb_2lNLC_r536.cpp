#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r536(const std::array<T,31>& k) {
  T z[74];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[15];
    z[7]=k[14];
    z[8]=k[9];
    z[9]=k[13];
    z[10]=k[10];
    z[11]=k[11];
    z[12]=k[3];
    z[13]=k[4];
    z[14]=k[5];
    z[15]=n<T>(5,4)*z[6];
    z[16]=n<T>(1,4)*z[9];
    z[17]=2*z[4];
    z[18]=z[16] + n<T>(17,4)*z[5] - z[15] + z[17];
    z[18]=z[9]*z[18];
    z[19]=7*z[4];
    z[20]=25*z[6] + 13*z[8];
    z[20]=n<T>(1,2)*z[20] - z[19];
    z[21]=n<T>(1,2)*z[4];
    z[20]=z[20]*z[21];
    z[22]=3*z[4];
    z[23]= - z[10] + z[22];
    z[24]=n<T>(1,2)*z[10];
    z[25]=z[24]*z[11];
    z[26]= - z[13]*z[25];
    z[23]=z[26] + n<T>(1,4)*z[23] + z[5];
    z[23]=z[11]*z[23];
    z[26]=z[8]*z[6];
    z[27]=2*z[3];
    z[28]= - z[6] - z[27];
    z[28]=z[3]*z[28];
    z[29]=2*z[10];
    z[30]=z[6] + z[29];
    z[30]=z[10]*z[30];
    z[31]= - z[5] + n<T>(21,4)*z[6] + z[8];
    z[31]=z[5]*z[31];
    z[18]=z[23] + z[18] + z[31] + z[20] + n<T>(1,4)*z[26] + z[28] + z[30];
    z[18]=z[11]*z[18];
    z[20]=n<T>(1,2)*z[3];
    z[23]=z[20]*z[10];
    z[28]=z[3]*z[14];
    z[30]=z[28]*z[8];
    z[31]= - z[3] - z[30];
    z[31]=z[8]*z[31];
    z[31]= - z[23] + z[31];
    z[32]=n<T>(1,2)*z[8];
    z[33]=z[8]*z[14];
    z[34]=static_cast<T>(5)- z[33];
    z[34]=z[34]*z[32];
    z[34]= - z[10] + z[34];
    z[35]=6*z[4];
    z[34]=n<T>(1,2)*z[34] - z[35];
    z[34]=z[5]*z[34];
    z[36]=7*z[8];
    z[37]= - 7*z[5] + z[10] - z[36];
    z[37]=z[37]*z[16];
    z[38]=2*z[8];
    z[35]= - n<T>(3,4)*z[9] + 6*z[5] + z[35] - n<T>(1,4)*z[3] + z[38];
    z[35]=z[11]*z[35];
    z[39]=z[11]*z[28];
    z[30]=z[30] + z[39];
    z[39]=n<T>(1,2)*z[7];
    z[30]=z[30]*z[39];
    z[30]=z[30] + z[35] + z[37] + n<T>(1,2)*z[31] + z[34];
    z[30]=z[7]*z[30];
    z[31]= - z[15] - z[29];
    z[31]=z[10]*z[31];
    z[34]=13*z[6];
    z[19]= - z[34] + z[19];
    z[19]=z[19]*z[21];
    z[35]= - z[10] - z[4];
    z[37]=z[10]*z[13];
    z[40]= - static_cast<T>(1)- z[37];
    z[40]=z[5]*z[40];
    z[35]=n<T>(1,2)*z[35] + z[40];
    z[40]=n<T>(1,2)*z[5];
    z[35]=z[35]*z[40];
    z[41]=npow(z[8],2);
    z[42]=n<T>(1,2)*z[41];
    z[19]=z[35] + z[19] + z[31] + z[42];
    z[19]=z[5]*z[19];
    z[31]= - z[34] + z[36];
    z[31]=z[8]*z[31];
    z[34]=z[4]*z[8];
    z[35]=z[10]*z[6];
    z[31]= - 13*z[34] - n<T>(21,2)*z[35] + z[31];
    z[36]=3*z[8];
    z[43]=z[5] - 7*z[10] - z[36];
    z[16]=z[43]*z[16];
    z[43]= - n<T>(15,2)*z[4] - n<T>(9,4)*z[10] + z[8];
    z[43]=z[5]*z[43];
    z[16]=z[16] + n<T>(1,2)*z[31] + z[43];
    z[16]=z[9]*z[16];
    z[31]=z[10]*z[3];
    z[43]=n<T>(5,2)*z[6];
    z[44]=z[43] + z[3];
    z[44]=z[44]*z[31];
    z[45]=z[3]*z[6];
    z[46]=n<T>(5,2)*z[45];
    z[47]=3*z[3];
    z[48]= - z[8]*z[47];
    z[48]=z[46] + z[48];
    z[48]=z[48]*z[32];
    z[49]=5*z[3];
    z[49]=z[8]*z[49];
    z[49]=9*z[45] + z[49];
    z[50]=n<T>(1,4)*z[4];
    z[49]=z[49]*z[50];
    z[16]=z[30] + z[18] + z[16] + z[19] + z[49] + z[44] + z[48];
    z[16]=z[7]*z[16];
    z[18]=npow(z[3],2);
    z[19]=npow(z[6],2);
    z[30]=z[31] - z[18] - n<T>(3,4)*z[19];
    z[30]=z[10]*z[30];
    z[44]=n<T>(3,2)*z[19];
    z[48]=z[26] - z[44];
    z[49]=z[48]*z[32];
    z[51]= - z[41] + z[34];
    z[51]=z[4]*z[51];
    z[52]= - z[44] - z[41];
    z[53]=2*z[5];
    z[53]=z[6]*z[53];
    z[52]=n<T>(1,2)*z[52] + z[53];
    z[52]=z[5]*z[52];
    z[53]=n<T>(1,2)*z[9];
    z[54]=npow(z[4],2);
    z[55]= - z[54]*z[53];
    z[56]=z[5] + z[4];
    z[57]=z[56]*z[9];
    z[25]= - 2*z[57] - z[25];
    z[25]=z[11]*z[25];
    z[58]=z[19]*z[3];
    z[59]=n<T>(3,4)*z[58];
    z[25]=z[25] + z[55] + z[52] + z[51] + z[49] + z[59] + z[30];
    z[25]=z[11]*z[25];
    z[30]=z[41]*z[20];
    z[49]=5*z[6];
    z[51]=z[4]*z[49];
    z[42]= - z[42] + z[51];
    z[42]=z[5]*z[42];
    z[30]=z[30] + z[42];
    z[36]=n<T>(5,2)*z[4] + z[10] - z[36];
    z[36]=z[5]*z[36];
    z[42]=z[5] + z[10];
    z[51]=z[9]*z[42];
    z[36]=n<T>(5,4)*z[51] - n<T>(1,2)*z[34] + z[36];
    z[36]=z[9]*z[36];
    z[51]= - z[49] + z[8];
    z[51]=z[51]*z[21];
    z[52]=n<T>(7,2)*z[5];
    z[22]= - z[22] - z[52];
    z[22]=z[9]*z[22];
    z[43]= - z[43] + z[8];
    z[43]=z[5]*z[43];
    z[22]=z[22] + z[51] + z[43];
    z[22]=z[11]*z[22];
    z[43]=z[9]*z[10];
    z[43]= - z[31] + z[43];
    z[51]=z[11]*z[3];
    z[43]=n<T>(1,2)*z[43] + z[51];
    z[43]=z[7]*z[43];
    z[22]=z[43] + z[22] + n<T>(1,2)*z[30] + z[36];
    z[22]=z[7]*z[22];
    z[30]= - z[45]*z[21];
    z[36]=z[41]*z[3];
    z[30]=z[30] - z[59] + z[36];
    z[30]=z[4]*z[30];
    z[43]= - z[4]*z[6];
    z[43]=z[44] + z[43];
    z[43]=z[43]*z[21];
    z[44]= - z[6]*z[17];
    z[51]=z[5]*z[24];
    z[44]=z[44] + z[51];
    z[44]=z[5]*z[44];
    z[43]=z[43] + z[44];
    z[43]=z[5]*z[43];
    z[26]=z[35] + z[26];
    z[44]=z[5]*z[10];
    z[51]=z[9]*z[40];
    z[26]=z[51] + 2*z[26] + z[44];
    z[26]=z[9]*z[26];
    z[48]= - z[8]*z[48];
    z[51]=z[19]*z[10];
    z[55]=n<T>(3,2)*z[51];
    z[48]=z[55] + z[48];
    z[60]=npow(z[10],2);
    z[61]=z[5]*z[60];
    z[26]=z[26] + n<T>(1,2)*z[48] + z[61];
    z[26]=z[9]*z[26];
    z[22]=z[22] + z[25] + z[26] + z[30] + z[43];
    z[22]=z[7]*z[22];
    z[25]=z[5]*z[3];
    z[26]=z[4]*z[10];
    z[26]=z[25] + z[31] + z[26];
    z[30]=n<T>(1,2)*z[11];
    z[26]=z[26]*z[30];
    z[43]=3*z[10];
    z[48]= - z[54]*z[43];
    z[61]=z[3]*npow(z[5],2);
    z[48]=z[48] - 5*z[61];
    z[62]= - z[3] + n<T>(5,4)*z[5];
    z[62]=z[5]*z[62];
    z[62]= - n<T>(1,4)*z[54] + z[62];
    z[62]=z[9]*z[62];
    z[26]=z[26] + n<T>(1,4)*z[48] + z[62];
    z[26]=z[11]*z[26];
    z[48]=z[3] - z[40];
    z[48]=z[5]*z[48];
    z[48]= - z[18] + z[48];
    z[48]=z[5]*z[48];
    z[62]=npow(z[9],2);
    z[63]=z[62]*z[20];
    z[48]=z[48] + z[63];
    z[48]=z[9]*z[48];
    z[63]=z[8]*z[10];
    z[64]=z[63]*z[54];
    z[65]=z[35]*z[8];
    z[55]=z[55] + z[65];
    z[32]=z[55]*z[32];
    z[55]=2*z[45];
    z[66]= - z[5]*z[55];
    z[66]=z[59] + z[66];
    z[66]=z[5]*z[66];
    z[26]=z[26] + z[48] + z[66] + z[32] - z[64];
    z[26]=z[11]*z[26];
    z[32]=npow(z[6],3);
    z[48]=z[4]*z[19];
    z[48]= - z[32] + z[48];
    z[48]=z[4]*z[48];
    z[66]=z[5]*z[4];
    z[67]= - z[19]*z[66];
    z[48]=z[48] + z[67];
    z[48]=z[5]*z[48];
    z[67]=z[58]*z[4];
    z[68]=z[32]*z[3];
    z[67]=z[67] - z[68];
    z[67]=z[67]*z[4];
    z[48]= - z[67] + z[48];
    z[69]=z[19]*z[8];
    z[70]=z[69] - z[32];
    z[70]=z[70]*z[8];
    z[71]=z[32]*z[10];
    z[70]=z[70] - z[71];
    z[69]=z[51] + z[69];
    z[69]=z[69]*z[53];
    z[72]=npow(z[5],3);
    z[73]= - z[10]*z[72];
    z[69]=z[69] + n<T>(1,2)*z[70] + z[73];
    z[69]=z[9]*z[69];
    z[19]=z[5]*z[19];
    z[19]=z[32] + z[19];
    z[19]=z[5]*z[19];
    z[19]=z[19] - z[68] - z[70];
    z[32]= - z[8] + z[9];
    z[32]=z[54]*z[32];
    z[70]= - z[11]*z[31];
    z[32]=n<T>(1,2)*z[32] + z[70];
    z[32]=z[11]*z[32];
    z[19]=n<T>(1,2)*z[19] + z[32];
    z[19]=z[11]*z[19];
    z[19]=z[19] + n<T>(1,2)*z[48] + z[69];
    z[32]=z[41]*z[56];
    z[41]=z[5]*z[8];
    z[41]=z[41] + z[34];
    z[48]=z[41] - z[57];
    z[56]=z[11]*z[48];
    z[32]=z[56] + z[32];
    z[32]=z[11]*z[32];
    z[41]= - z[9]*z[41];
    z[48]=z[31] + z[48];
    z[48]=z[11]*z[48];
    z[41]=z[41] + z[48];
    z[39]=z[41]*z[39];
    z[36]= - z[50]*z[36];
    z[41]= - z[62]*z[44];
    z[32]=z[39] + n<T>(1,4)*z[32] + z[36] + z[41];
    z[32]=z[7]*z[32];
    z[19]=n<T>(1,2)*z[19] + z[32];
    z[19]=z[7]*z[19];
    z[32]=z[35] - z[45];
    z[36]=z[4]*z[32];
    z[36]=z[58] + z[36];
    z[36]=z[36]*z[66];
    z[36]= - z[67] + z[36];
    z[36]=z[5]*z[36];
    z[39]=z[51]*z[8];
    z[39]=z[39] - z[71];
    z[32]=z[8]*z[32];
    z[32]= - z[51] + z[32];
    z[32]=z[9]*z[32];
    z[32]= - z[39] + z[32];
    z[32]=z[9]*z[8]*z[32];
    z[32]=z[36] + z[32];
    z[36]=z[8]*z[39];
    z[39]= - z[5]*z[58];
    z[39]= - z[68] + z[39];
    z[39]=z[5]*z[39];
    z[36]=z[36] + z[39];
    z[39]=npow(z[9],3)*z[25];
    z[36]=n<T>(1,2)*z[36] + z[39];
    z[39]= - z[9]*z[61];
    z[39]=n<T>(1,4)*z[64] + z[39];
    z[39]=z[11]*z[39];
    z[36]=n<T>(1,2)*z[36] + z[39];
    z[36]=z[11]*z[36];
    z[19]=z[19] + n<T>(1,4)*z[32] + z[36];
    z[19]=z[1]*z[19];
    z[32]=z[45] - n<T>(1,2)*z[35];
    z[32]=z[8]*z[32];
    z[32]= - n<T>(3,4)*z[51] + z[32];
    z[32]=z[8]*z[32];
    z[36]=z[72]*z[24];
    z[39]=z[10] - z[3];
    z[41]=z[8]*z[39];
    z[41]= - 2*z[35] - n<T>(1,4)*z[41];
    z[41]=z[8]*z[41];
    z[44]=z[20]*z[5];
    z[48]= - z[9]*z[44];
    z[41]=z[41] + z[48];
    z[41]=z[9]*z[41];
    z[32]=z[41] + z[32] + z[36];
    z[32]=z[9]*z[32];
    z[36]= - n<T>(1,2)*z[45] + z[35];
    z[36]=z[4]*z[36];
    z[36]= - z[59] + z[36];
    z[36]=z[4]*z[36];
    z[41]= - z[39]*z[50];
    z[41]=z[55] + z[41];
    z[41]=z[41]*z[66];
    z[36]=z[36] + z[41];
    z[36]=z[5]*z[36];
    z[19]=z[19] + z[22] + z[26] + z[36] + z[32];
    z[19]=z[1]*z[19];
    z[22]= - n<T>(1,2) + z[37];
    z[22]=z[22]*z[54];
    z[21]= - z[14]*z[21];
    z[21]=static_cast<T>(3)+ z[21];
    z[21]=z[4]*z[21];
    z[21]=z[52] - z[3] + z[21];
    z[21]=z[9]*z[21];
    z[26]= - z[11]*z[4]*z[37];
    z[21]=z[26] + z[21] + z[44] + z[23] + z[22];
    z[21]=z[21]*z[30];
    z[15]= - z[15] - z[27];
    z[15]=z[3]*z[15];
    z[22]= - z[9]*z[28];
    z[22]= - z[20] + z[22];
    z[22]=z[22]*z[53];
    z[23]=9*z[3] + z[5];
    z[23]=z[5]*z[23];
    z[15]=z[22] + n<T>(1,4)*z[23] + z[15] + n<T>(1,2)*z[54];
    z[15]=z[9]*z[15];
    z[22]= - z[46] - z[31];
    z[22]=z[10]*z[22];
    z[23]= - z[35] - z[63];
    z[26]=z[10]*z[17];
    z[23]=n<T>(5,4)*z[23] + z[26];
    z[23]=z[4]*z[23];
    z[25]= - 3*z[45] + z[25];
    z[25]=z[5]*z[25];
    z[15]=z[21] + z[15] + n<T>(7,4)*z[25] + z[23] + z[22] - n<T>(9,4)*z[65];
    z[15]=z[11]*z[15];
    z[21]= - z[3] - z[24];
    z[21]=z[21]*z[40];
    z[22]=z[10] + z[3];
    z[23]=z[4]*z[22];
    z[18]=z[21] + n<T>(5,4)*z[23] + z[18] - z[60];
    z[18]=z[5]*z[18];
    z[21]=5*z[45];
    z[23]=z[21] + 21*z[35];
    z[25]= - z[47] + z[24];
    z[25]=z[8]*z[25];
    z[23]=n<T>(1,2)*z[23] + z[25];
    z[23]=z[8]*z[23];
    z[25]=z[22]*z[34];
    z[21]=z[10]*z[21];
    z[21]=n<T>(5,2)*z[25] + z[21] + z[23];
    z[23]=z[28] - 1;
    z[25]=z[12]*z[24];
    z[25]=z[25] + z[23];
    z[25]=z[8]*z[25];
    z[26]= - z[9]*z[23];
    z[25]=z[26] + z[43] + z[25];
    z[25]=z[8]*z[25];
    z[26]=z[20] + z[10];
    z[27]= - z[5]*z[26];
    z[25]=z[27] + z[25];
    z[25]=z[25]*z[53];
    z[18]=z[25] + n<T>(1,2)*z[21] + z[18];
    z[18]=z[9]*z[18];
    z[21]=21*z[45] + 5*z[35];
    z[20]=z[20] - z[29];
    z[20]=z[4]*z[20];
    z[20]=n<T>(1,4)*z[21] + z[20];
    z[20]=z[4]*z[20];
    z[21]=n<T>(1,2)*z[23] - z[37];
    z[21]=z[4]*z[21];
    z[21]=z[21] - z[47] - z[10];
    z[21]=z[4]*z[21];
    z[23]= - z[37]*z[66];
    z[21]=z[21] + z[23];
    z[21]=z[21]*z[40];
    z[23]=z[10]*z[46];
    z[20]=z[21] + z[23] + z[20];
    z[20]=z[5]*z[20];
    z[15]=z[19] + z[16] + z[15] + z[20] + z[18];
    z[16]=npow(z[2],2);
    z[15]=z[1]*z[16]*z[15];
    z[18]=z[33]*z[9];
    z[19]=n<T>(1,2) + z[33];
    z[19]=z[8]*z[19];
    z[20]=npow(z[14],2);
    z[21]=z[8]*z[20];
    z[21]=n<T>(1,2)*z[14] + z[21];
    z[21]=z[8]*z[21];
    z[21]=n<T>(1,2) + z[21];
    z[21]=z[5]*z[21];
    z[23]=z[9]*z[14];
    z[25]=z[11]*z[23];
    z[19]=z[25] + z[18] + z[19] + z[21];
    z[19]=z[7]*z[19];
    z[21]=11*z[4];
    z[25]= - z[21] - z[39];
    z[23]=n<T>(3,2) - z[23];
    z[23]=z[9]*z[23];
    z[27]=z[4]*z[13];
    z[30]= - z[11]*z[27];
    z[23]=z[30] + z[23] + n<T>(1,2)*z[25] - 3*z[5];
    z[23]=z[11]*z[23];
    z[25]=z[6] - z[3];
    z[25]=z[10]*z[25];
    z[25]= - z[45] + z[25];
    z[30]=z[49] - z[47];
    z[30]=n<T>(1,2)*z[30] - z[38];
    z[30]=z[8]*z[30];
    z[21]=z[21] + z[8] + z[6] + z[10];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[5]*z[27];
    z[21]=n<T>(1,2)*z[21] + z[27];
    z[21]=z[5]*z[21];
    z[27]=9*z[8] + z[42];
    z[18]=n<T>(1,2)*z[27] - z[18];
    z[18]=z[9]*z[18];
    z[18]=z[19] + z[23] + z[18] + z[21] + n<T>(1,2)*z[25] + z[30];
    z[18]=z[7]*z[18];
    z[19]=z[3] + z[6];
    z[21]= - z[19]*z[24];
    z[23]= - z[12]*z[29];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[8]*z[23];
    z[23]= - 3*z[26] + z[23];
    z[23]=z[8]*z[23];
    z[24]=z[4] - z[8] + z[39];
    z[24]=z[24]*z[40];
    z[25]=z[12]*z[43];
    z[25]=static_cast<T>(1)+ z[25];
    z[26]=z[10]*npow(z[12],2);
    z[26]= - z[12] + z[26];
    z[26]=z[8]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[26];
    z[25]=z[8]*z[25];
    z[25]=z[10] + z[25];
    z[25]=z[9]*z[25];
    z[21]=z[25] + z[24] + z[21] + z[23];
    z[21]=z[9]*z[21];
    z[23]=z[10]*z[19];
    z[23]= - z[45] + z[23];
    z[24]= - z[49] + z[43];
    z[24]=n<T>(1,2)*z[24] + z[17];
    z[24]=z[4]*z[24];
    z[19]= - z[5] + z[4] + z[19];
    z[19]=z[19]*z[53];
    z[25]=z[4]*z[14];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[4]*z[25];
    z[26]=z[4]*z[20];
    z[26]= - n<T>(3,2)*z[14] + z[26];
    z[26]=z[4]*z[26];
    z[26]=static_cast<T>(1)+ z[26];
    z[26]=z[9]*z[26];
    z[25]=z[25] + z[26];
    z[25]=z[11]*z[25];
    z[19]=z[25] + z[19] + z[44] + n<T>(1,2)*z[23] + z[24];
    z[19]=z[11]*z[19];
    z[23]= - z[45] - z[31];
    z[17]= - n<T>(3,2)*z[22] - z[17];
    z[17]=z[4]*z[17];
    z[20]= - z[3]*z[20];
    z[20]=z[14] + z[20];
    z[20]=z[4]*z[20];
    z[20]=z[20] - static_cast<T>(1)+ n<T>(3,2)*z[28];
    z[20]=z[4]*z[20];
    z[20]= - z[3] + z[20];
    z[20]=z[5]*z[20];
    z[17]=z[20] + n<T>(1,2)*z[23] + z[17];
    z[17]=z[5]*z[17];
    z[20]= - n<T>(5,2)*z[35] + z[63];
    z[20]=z[8]*z[20];
    z[22]=z[4]*z[46];
    z[17]=z[18] + z[19] + z[21] + z[17] + z[20] + z[22];
    z[16]=z[17]*z[16];
    z[15]=z[16] + z[15];

    r += z[15]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r536(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r536(const std::array<dd_real,31>&);
#endif
