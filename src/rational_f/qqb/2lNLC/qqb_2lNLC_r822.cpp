#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r822(const std::array<T,31>& k) {
  T z[61];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[12];
    z[4]=k[15];
    z[5]=k[4];
    z[6]=k[7];
    z[7]=k[10];
    z[8]=k[5];
    z[9]=k[11];
    z[10]=k[2];
    z[11]=k[14];
    z[12]=k[3];
    z[13]=k[13];
    z[14]=5*z[8];
    z[15]=z[2]*z[3];
    z[16]= - z[15]*z[14];
    z[16]=7*z[3] + z[16];
    z[16]=z[8]*z[16];
    z[17]=z[5]*z[7];
    z[18]=z[7]*z[12];
    z[19]= - static_cast<T>(2)+ z[18];
    z[19]=5*z[19] - 12*z[17];
    z[19]=z[5]*z[19];
    z[20]=z[7]*npow(z[12],2);
    z[19]= - z[8] + z[19] + 2*z[12] - z[20];
    z[19]=z[6]*z[19];
    z[21]=2*z[18];
    z[22]=z[3]*z[12];
    z[23]= - static_cast<T>(1)- z[22];
    z[24]= - z[9]*z[12];
    z[16]=z[19] + z[16] + z[24] + 10*z[17] + 3*z[23] - z[21];
    z[16]=z[6]*z[16];
    z[19]=z[17] + 4;
    z[23]=z[7]*z[10];
    z[24]=3*z[23];
    z[25]= - z[24] + z[19];
    z[26]=3*z[5];
    z[25]=z[25]*z[26];
    z[27]=3*z[10];
    z[28]=npow(z[10],2);
    z[29]=z[28]*z[7];
    z[30]=4*z[29];
    z[25]=z[25] - z[27] + z[30];
    z[25]=z[5]*z[25];
    z[31]=z[28]*z[11];
    z[32]=z[11]*z[10];
    z[33]=z[8]*z[32];
    z[27]= - 3*z[33] + z[27] + 4*z[31];
    z[27]=z[8]*z[27];
    z[33]=static_cast<T>(1)+ 5*z[17];
    z[34]=npow(z[5],3);
    z[35]=z[33]*z[34];
    z[36]=z[17] + 2;
    z[37]= - z[36]*npow(z[5],4);
    z[38]= - z[8]*z[34];
    z[37]=z[37] + z[38];
    z[37]=z[6]*z[37];
    z[38]=npow(z[5],2);
    z[39]=z[38]*z[8];
    z[35]=3*z[37] + z[35] - 4*z[39];
    z[35]=z[6]*z[35];
    z[25]=z[35] + z[27] - 4*z[28] + z[25];
    z[25]=z[4]*z[25];
    z[27]= - static_cast<T>(1)+ 4*z[17];
    z[27]=z[27]*z[38];
    z[28]= - static_cast<T>(20)- 13*z[17];
    z[28]=z[28]*z[34];
    z[28]=z[28] - 7*z[39];
    z[28]=z[6]*z[28];
    z[35]=z[8]*z[5];
    z[27]=z[28] + 4*z[27] - 11*z[35];
    z[27]=z[6]*z[27];
    z[28]=11*z[17];
    z[37]=z[28] + static_cast<T>(23)- 18*z[23];
    z[37]=z[5]*z[37];
    z[40]=z[9]*z[11];
    z[41]=z[40]*z[8];
    z[42]= - z[41] + z[11] + z[9];
    z[42]=z[8]*z[42];
    z[42]=7*z[42] - static_cast<T>(7)- 4*z[32];
    z[42]=z[8]*z[42];
    z[25]=z[25] + z[27] + z[42] + z[37] + z[10] + z[30];
    z[25]=z[4]*z[25];
    z[27]= - static_cast<T>(12)- z[28];
    z[27]=z[27]*z[38];
    z[27]=2*z[27] - 5*z[35];
    z[27]=z[6]*z[27];
    z[28]= - static_cast<T>(8)+ 21*z[17];
    z[28]=z[5]*z[28];
    z[27]=z[27] + z[28] - 8*z[8];
    z[27]=z[6]*z[27];
    z[28]=2*z[32];
    z[30]=static_cast<T>(3)- z[28];
    z[37]= - 11*z[41] + 5*z[11] + 12*z[9];
    z[37]=z[8]*z[37];
    z[25]=z[25] + z[27] + z[37] + 6*z[17] + 2*z[30] - 5*z[23];
    z[25]=z[4]*z[25];
    z[27]=3*z[13];
    z[30]=2*z[2];
    z[37]= - z[27] + z[30];
    z[37]=z[37]*z[30];
    z[42]=z[9]*z[2];
    z[43]=z[2]*z[5];
    z[44]= - static_cast<T>(1)- z[43];
    z[44]=z[44]*z[42];
    z[37]=z[37] + z[44];
    z[37]=z[9]*z[37];
    z[44]=npow(z[9],2);
    z[45]=z[8]*z[13];
    z[46]=z[44]*z[45];
    z[47]=npow(z[2],2);
    z[48]=z[46]*z[47];
    z[49]=z[47]*z[13];
    z[50]= - z[9]*z[47];
    z[50]=5*z[49] + z[50];
    z[50]=z[9]*z[50];
    z[50]=z[50] - z[48];
    z[50]=z[8]*z[50];
    z[51]=z[47]*z[3];
    z[52]=4*z[51];
    z[37]=z[50] - z[52] + z[37];
    z[37]=z[8]*z[37];
    z[50]= - static_cast<T>(8)- z[43];
    z[50]=z[2]*z[50];
    z[50]=z[50] - 7*z[11] + z[27];
    z[50]=z[9]*z[50];
    z[53]=z[43]*z[3];
    z[54]=8*z[3] + z[53];
    z[54]=z[2]*z[54];
    z[37]=z[37] + z[54] + z[50];
    z[37]=z[8]*z[37];
    z[50]=2*z[9];
    z[54]=z[5]*z[11];
    z[54]=static_cast<T>(3)+ z[54];
    z[54]=z[54]*z[50];
    z[55]=2*z[11];
    z[56]=3*z[3];
    z[16]=z[25] + z[16] + z[37] + z[54] + z[55] - z[56];
    z[25]= - static_cast<T>(37)- 26*z[17];
    z[25]=z[5]*z[25];
    z[37]= - static_cast<T>(11)+ 3*z[45];
    z[37]=z[8]*z[37];
    z[25]=z[25] + z[37];
    z[25]=z[6]*z[25];
    z[25]=z[25] + 6*z[45] - static_cast<T>(1)+ 8*z[17];
    z[25]=z[6]*z[25];
    z[29]=z[31] - z[29];
    z[31]=4*z[2];
    z[37]=z[29]*z[31];
    z[54]= - z[36]*z[38];
    z[54]=z[54] - z[35];
    z[57]=9*z[6];
    z[54]=z[54]*z[57];
    z[58]=z[5]*z[33];
    z[54]=z[54] + z[58] - 4*z[8];
    z[54]=z[6]*z[54];
    z[37]=z[37] + z[54];
    z[54]=2*z[4];
    z[37]=z[37]*z[54];
    z[24]=z[24] + static_cast<T>(2)+ z[32];
    z[24]=z[24]*z[31];
    z[58]=z[3] - z[13];
    z[47]=z[47]*z[8];
    z[58]=z[58]*z[47];
    z[27]=z[27] + z[3];
    z[27]=z[2]*z[27];
    z[27]=z[27] - 2*z[58];
    z[59]=2*z[8];
    z[27]=z[27]*z[59];
    z[24]=z[37] + z[25] + z[27] - z[3] + z[24];
    z[24]=z[4]*z[24];
    z[25]= - z[13] + 2*z[3];
    z[25]=z[25]*z[47];
    z[27]=11*z[13];
    z[37]= - 5*z[7] - 6*z[3] + z[11] - z[27];
    z[37]=z[2]*z[37];
    z[25]=z[37] + 8*z[25];
    z[37]=2*z[13];
    z[45]= - z[6]*z[45];
    z[45]= - z[37] + z[45];
    z[45]=z[6]*z[45];
    z[24]=z[24] + 2*z[25] + 11*z[45];
    z[24]=z[4]*z[24];
    z[25]=4*z[7];
    z[45]=8*z[2] + z[27] + z[25];
    z[45]=z[2]*z[45];
    z[47]= - z[43] - static_cast<T>(4)+ z[17];
    z[47]=z[2]*z[47];
    z[47]=z[7] + z[47];
    z[47]=z[9]*z[47];
    z[45]=z[45] + z[47];
    z[45]=z[9]*z[45];
    z[47]= - z[37] + z[2];
    z[47]=z[47]*z[42];
    z[47]=8*z[49] + z[47];
    z[47]=z[9]*z[47];
    z[47]=z[47] - z[48];
    z[47]=z[8]*z[47];
    z[45]=z[47] - 8*z[51] + z[45];
    z[47]= - z[55] + z[27];
    z[21]= - z[21] - static_cast<T>(6)- z[22];
    z[21]=z[9]*z[21];
    z[21]=z[21] + 2*z[47] + z[56];
    z[21]=z[9]*z[21];
    z[27]=z[27] - z[50];
    z[27]=z[9]*z[27];
    z[27]=z[27] - 2*z[46];
    z[27]=z[8]*z[27];
    z[47]= - z[12] - z[20];
    z[47]=z[47]*z[50];
    z[60]= - static_cast<T>(11)+ 10*z[18];
    z[47]=z[47] - z[60];
    z[47]=z[9]*z[47];
    z[27]=z[47] + z[27];
    z[27]=z[6]*z[27];
    z[46]=4*z[46];
    z[21]=z[27] + z[21] - z[46];
    z[21]=z[6]*z[21];
    z[27]=z[3] - z[30];
    z[27]=z[2]*z[27];
    z[47]= - z[8]*z[52];
    z[52]=z[5]*z[36];
    z[52]=z[52] + z[8];
    z[52]=z[4]*z[52]*npow(z[6],2);
    z[27]=3*z[52] + z[27] + z[47];
    z[27]=z[4]*z[27];
    z[27]=10*z[51] + z[27];
    z[27]=z[4]*z[27];
    z[47]= - z[7] + z[2];
    z[44]=z[2]*z[47]*z[44];
    z[47]=2*z[1];
    z[52]= - npow(z[4],2)*z[51]*z[47];
    z[27]=z[52] + z[44] + z[27];
    z[27]=z[27]*z[47];
    z[21]=z[27] + z[24] + 2*z[45] + z[21];
    z[21]=z[1]*z[21];
    z[24]= - z[33]*z[38];
    z[24]=z[24] + 4*z[35];
    z[27]=z[36]*z[34];
    z[27]=z[27] + z[39];
    z[27]=z[27]*z[57];
    z[24]=2*z[24] + z[27];
    z[24]=z[6]*z[24];
    z[19]= - z[19]*z[26];
    z[19]=z[24] - 4*z[29] + z[19];
    z[19]=z[19]*z[54];
    z[24]=3*z[11] - 7*z[41];
    z[24]=z[8]*z[24];
    z[24]=z[24] - 6*z[23] - static_cast<T>(5)- z[28];
    z[26]=static_cast<T>(77)+ 52*z[17];
    z[26]=z[26]*z[38];
    z[26]=z[26] + 28*z[35];
    z[26]=z[6]*z[26];
    z[27]=static_cast<T>(9)- 20*z[17];
    z[27]=z[5]*z[27];
    z[27]=z[27] + 11*z[8];
    z[26]=2*z[27] + z[26];
    z[26]=z[6]*z[26];
    z[19]=z[19] + 2*z[24] + z[26];
    z[19]=z[4]*z[19];
    z[24]=z[13] - 5*z[3];
    z[24]=z[2]*z[24];
    z[24]=6*z[58] + z[24] - 9*z[40];
    z[24]=z[8]*z[24];
    z[23]= - z[23] - static_cast<T>(1)+ z[32];
    z[23]=z[23]*z[31];
    z[26]=z[56] + z[11];
    z[23]=z[24] + z[23] + 6*z[7] + z[26];
    z[24]=static_cast<T>(17)+ 22*z[17];
    z[24]=z[5]*z[24];
    z[14]=z[24] + z[14];
    z[14]=z[6]*z[14];
    z[14]=z[14] + static_cast<T>(11)- 14*z[17];
    z[24]=5*z[13];
    z[27]= - z[24] + z[56];
    z[27]=z[8]*z[27];
    z[14]=z[27] + 2*z[14];
    z[14]=z[6]*z[14];
    z[14]=z[19] + 2*z[23] + z[14];
    z[14]=z[4]*z[14];
    z[19]= - z[12]*z[50];
    z[18]=z[19] - 4*z[18] + static_cast<T>(11)+ 7*z[22];
    z[18]=z[9]*z[18];
    z[19]= - z[55] + z[24];
    z[19]=3*z[19] - 4*z[9];
    z[19]=z[9]*z[19];
    z[15]= - z[46] - 14*z[15] + z[19];
    z[15]=z[8]*z[15];
    z[19]= - z[12] + 2*z[20];
    z[19]=z[19]*z[50];
    z[19]=z[19] + z[60];
    z[19]=z[6]*z[19];
    z[15]=z[19] + z[15] + z[18] + 4*z[11] - z[56];
    z[15]=z[6]*z[15];
    z[17]= - static_cast<T>(2)+ z[17];
    z[17]=2*z[17] - z[43];
    z[17]=z[17]*z[30];
    z[18]=static_cast<T>(1)+ 2*z[43];
    z[18]=z[9]*z[18];
    z[17]=z[18] + z[17] - z[25] - 8*z[11] + z[3];
    z[17]=z[9]*z[17];
    z[18]= - z[37] - z[2];
    z[18]=z[18]*z[42];
    z[18]=13*z[49] + z[18];
    z[18]=z[9]*z[18];
    z[18]=z[18] - 2*z[48];
    z[18]=z[8]*z[18];
    z[19]= - static_cast<T>(2)- z[43];
    z[19]=z[19]*z[42];
    z[20]=z[13] + 6*z[2];
    z[20]=z[2]*z[20];
    z[19]=z[20] + z[19];
    z[19]=z[9]*z[19];
    z[19]= - 6*z[51] + z[19];
    z[18]=2*z[19] + z[18];
    z[18]=z[18]*z[59];
    z[19]=z[7] + z[26];
    z[19]=2*z[19] + z[53];
    z[19]=z[19]*z[30];
    z[14]=z[21] + z[14] + z[15] + z[18] + z[19] + z[17];
    z[14]=z[1]*z[14];
    z[14]=2*z[16] + z[14];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r822(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r822(const std::array<dd_real,31>&);
#endif
