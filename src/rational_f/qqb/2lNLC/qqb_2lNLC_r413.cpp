#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r413(const std::array<T,31>& k) {
  T z[99];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[3];
    z[7]=k[5];
    z[8]=k[10];
    z[9]=k[14];
    z[10]=k[4];
    z[11]=k[8];
    z[12]=k[2];
    z[13]=k[9];
    z[14]=k[15];
    z[15]=npow(z[9],2);
    z[16]=n<T>(29,3)*z[15];
    z[17]=npow(z[13],2);
    z[18]=n<T>(65,2)*z[17] + z[16];
    z[19]=n<T>(1,2)*z[9];
    z[18]=z[18]*z[19];
    z[20]=npow(z[11],2);
    z[21]=npow(z[2],2);
    z[22]= - n<T>(887,4)*z[20] + 11*z[21];
    z[23]=n<T>(1,12)*z[2];
    z[22]=z[22]*z[23];
    z[24]=n<T>(9,2)*z[5] + 5*z[9] - n<T>(7,3)*z[2];
    z[24]=z[5]*z[24];
    z[24]=3*z[21] + z[24];
    z[25]=n<T>(1,2)*z[5];
    z[24]=z[24]*z[25];
    z[26]=npow(z[13],3);
    z[27]=n<T>(47,4)*z[26];
    z[28]=z[27]*z[9];
    z[29]=npow(z[11],3);
    z[30]=20*z[29];
    z[31]=z[30]*z[2];
    z[28]=z[28] - z[31];
    z[28]=n<T>(1,3)*z[28];
    z[32]=npow(z[5],3);
    z[33]=z[32]*z[9];
    z[34]=z[28] - 3*z[33];
    z[34]=z[7]*z[34];
    z[35]=n<T>(20,3)*z[29];
    z[18]=z[34] + z[24] + z[22] + z[35] + z[18];
    z[18]=z[7]*z[18];
    z[22]=z[2]*z[6];
    z[24]=z[22] - 1;
    z[34]= - z[24]*z[21];
    z[36]=npow(z[2],3);
    z[37]=z[36]*z[7];
    z[38]=npow(z[14],2);
    z[34]= - z[37] - n<T>(3,2)*z[38] + z[34];
    z[39]=n<T>(1,2)*z[7];
    z[34]=z[34]*z[39];
    z[40]=8*z[2];
    z[41]=n<T>(7,3)*z[9];
    z[42]=z[40] + z[41];
    z[43]=z[38]*z[10];
    z[44]=3*z[43];
    z[45]=z[14] - z[44];
    z[34]=z[34] + n<T>(3,4)*z[45] + z[42];
    z[34]=z[7]*z[34];
    z[45]=z[43] - z[14];
    z[46]=n<T>(3,4)*z[5];
    z[47]=z[46] - n<T>(3,4)*z[45];
    z[48]=npow(z[10],2);
    z[49]=z[48]*z[8];
    z[47]=z[49]*z[47];
    z[50]=n<T>(3,2)*z[10];
    z[51]=z[14] - n<T>(3,2)*z[43];
    z[51]=z[51]*z[50];
    z[52]=8*z[22];
    z[53]=z[9]*z[10];
    z[34]=z[34] + z[52] + n<T>(14,3)*z[53] - static_cast<T>(8)+ z[51] + z[47];
    z[34]=z[4]*z[34];
    z[47]=z[5]*z[2];
    z[51]=z[21]*z[5];
    z[54]= - z[7]*z[51];
    z[47]=z[54] + z[47] - z[38] - z[21];
    z[47]=z[7]*z[47];
    z[54]=z[8]*z[10];
    z[55]=z[45]*z[54];
    z[47]=z[55] - z[47];
    z[55]=n<T>(1,2)*z[14];
    z[56]=z[55] - z[43];
    z[57]=z[3]*z[10];
    z[58]=n<T>(19,3)*z[57] + n<T>(21,2)*z[54];
    z[58]=z[58]*z[25];
    z[34]=z[34] + z[58] + 16*z[2] + n<T>(14,3)*z[9] + 3*z[56] - n<T>(19,6)*z[3]
    - n<T>(3,2)*z[47];
    z[34]=z[4]*z[34];
    z[47]=3*z[38];
    z[58]=n<T>(887,12)*z[20] - z[47];
    z[59]=z[53] - 1;
    z[60]=29*z[9];
    z[61]=z[60]*z[59];
    z[62]= - n<T>(95,4)*z[13] + z[61];
    z[63]=n<T>(1,3)*z[9];
    z[62]=z[62]*z[63];
    z[64]=13*z[9];
    z[45]= - n<T>(1,2)*z[45] - z[64];
    z[65]=z[53]*z[8];
    z[45]=n<T>(1,2)*z[45] - 2*z[65];
    z[45]=z[8]*z[45];
    z[66]=n<T>(1,3)*z[2];
    z[67]=static_cast<T>(29)+ 11*z[22];
    z[67]=z[67]*z[66];
    z[68]=z[3]*z[6];
    z[69]=n<T>(2831,12) - 97*z[68];
    z[69]=z[3]*z[69];
    z[67]=z[67] - n<T>(395,6)*z[11] + z[69];
    z[69]=n<T>(1,4)*z[2];
    z[67]=z[67]*z[69];
    z[70]=n<T>(17,12)*z[2] + n<T>(13,6)*z[8] - n<T>(31,3)*z[3] - 17*z[9];
    z[71]=n<T>(3,2)*z[5];
    z[72]= - z[10]*z[71];
    z[72]=n<T>(5,3) + z[72];
    z[72]=z[5]*z[72];
    z[70]=n<T>(1,2)*z[70] + z[72];
    z[70]=z[5]*z[70];
    z[72]=z[35]*z[10];
    z[18]=z[34] + z[18] + z[70] + z[67] + 3*z[45] + z[62] + n<T>(1,4)*z[58]
    - z[72];
    z[18]=z[4]*z[18];
    z[34]=z[37]*z[25];
    z[45]=n<T>(1,2)*z[36] + z[34] - z[51];
    z[45]=z[45]*z[7];
    z[58]=n<T>(1,2)*z[21];
    z[62]=z[24]*z[58];
    z[67]=n<T>(15,2)*z[2];
    z[70]=z[41] - z[67];
    z[70]=z[5]*z[70];
    z[62]=z[45] + z[62] + z[70];
    z[62]=z[7]*z[62];
    z[70]=z[68] + 1;
    z[40]= - z[70]*z[40];
    z[73]=n<T>(7,3)*z[54] + n<T>(17,3) - 8*z[57];
    z[73]=z[5]*z[73];
    z[74]=8*z[3];
    z[40]=z[62] + z[73] + z[40] + n<T>(7,3)*z[65] + z[74] - z[41];
    z[40]=z[4]*z[40];
    z[62]=z[37]*z[5];
    z[73]= - z[62] - z[36] + 5*z[51];
    z[73]=z[73]*z[39];
    z[75]=n<T>(1,2)*z[22];
    z[76]=static_cast<T>(2)- z[75];
    z[76]=z[2]*z[76];
    z[76]= - z[74] + z[76];
    z[76]=z[2]*z[76];
    z[77]=z[41]*z[8];
    z[78]=n<T>(7,3)*z[8];
    z[79]=z[78] - 10*z[2];
    z[79]=z[5]*z[79];
    z[40]=z[40] + z[73] + z[79] + z[77] + z[76];
    z[40]=z[4]*z[40];
    z[73]=z[57] + 1;
    z[76]=npow(z[3],2);
    z[79]=z[73]*z[76];
    z[80]=6*z[8];
    z[81]= - static_cast<T>(1)- z[54];
    z[81]=z[81]*z[80];
    z[81]=n<T>(47,6)*z[9] + z[81];
    z[81]=z[8]*z[81];
    z[82]= - static_cast<T>(1)+ n<T>(5,2)*z[57];
    z[82]=n<T>(5,3)*z[82] - z[54];
    z[82]=z[5]*z[82];
    z[82]=z[82] + z[9] + z[8];
    z[82]=z[5]*z[82];
    z[83]=n<T>(19,24)*z[3] - 4*z[2];
    z[83]=z[2]*z[83];
    z[81]=z[82] + z[83] + n<T>(103,8)*z[79] + z[81];
    z[81]=z[5]*z[81];
    z[82]=z[65] + z[9];
    z[82]=z[82]*z[80];
    z[83]=z[59]*z[15];
    z[82]=z[82] - n<T>(29,6)*z[83];
    z[82]=z[82]*z[8];
    z[83]=npow(z[9],3);
    z[84]=n<T>(29,3)*z[83];
    z[85]=npow(z[3],3);
    z[86]=z[84] + n<T>(103,4)*z[85];
    z[82]=z[82] + n<T>(1,2)*z[86];
    z[86]= - static_cast<T>(1)+ z[68];
    z[86]=z[86]*z[76];
    z[70]=z[70]*z[2];
    z[87]= - 53*z[3] - 5*z[70];
    z[87]=z[87]*z[66];
    z[86]=n<T>(103,2)*z[86] + z[87];
    z[86]=z[86]*z[69];
    z[87]=29*z[83] - n<T>(5,2)*z[36];
    z[88]=npow(z[5],2);
    z[89]= - n<T>(19,2)*z[9] + n<T>(25,3)*z[2];
    z[89]=z[89]*z[88];
    z[87]=n<T>(1,3)*z[87] + z[89];
    z[87]=z[7]*z[87]*z[25];
    z[40]=z[40] + z[87] + z[81] + z[86] - z[82];
    z[40]=z[4]*z[40];
    z[81]=15*z[3] - n<T>(7,6)*z[2];
    z[81]=z[81]*z[58];
    z[86]=n<T>(25,6)*z[2];
    z[87]=n<T>(25,3)*z[3] - 7*z[9];
    z[87]= - z[86] + n<T>(1,2)*z[87] + z[8];
    z[87]=z[87]*z[88];
    z[89]=npow(z[8],3);
    z[81]=z[87] + z[81] + 6*z[89] + n<T>(103,8)*z[85] + z[84];
    z[81]=z[5]*z[81];
    z[41]= - 4*z[3] + z[41];
    z[41]=z[67] + 2*z[41] - z[78];
    z[41]=z[5]*z[41];
    z[67]=n<T>(1,2)*z[2];
    z[24]= - z[24]*z[67];
    z[24]=z[74] + z[24];
    z[24]=z[2]*z[24];
    z[24]= - z[45] + z[41] - z[77] + z[24];
    z[24]=z[4]*z[24];
    z[41]= - n<T>(5,2)*z[3] + z[70];
    z[41]=z[41]*z[21];
    z[45]=5*z[3];
    z[74]=z[45] - 7*z[2];
    z[74]=z[2]*z[74]*z[25];
    z[24]=z[24] + z[62] + z[41] + z[74];
    z[24]=z[4]*z[24];
    z[41]=n<T>(103,2)*z[85];
    z[62]=z[21]*z[3];
    z[74]=z[41] + n<T>(7,3)*z[62];
    z[74]=z[74]*z[69];
    z[78]=npow(z[8],2);
    z[84]=z[78]*z[9];
    z[84]= - 6*z[84] + n<T>(29,6)*z[83];
    z[84]=z[84]*z[8];
    z[24]=z[24] + z[81] - z[74] - z[84];
    z[24]=z[4]*z[24];
    z[70]= - z[3] + z[70];
    z[58]=z[70]*z[58];
    z[70]= - n<T>(15,2)*z[3] - z[2];
    z[70]=z[2]*z[70];
    z[70]=z[77] + z[70];
    z[70]=z[5]*z[70];
    z[34]=z[34] + z[58] + z[70];
    z[34]=z[4]*z[34];
    z[58]=3*z[3];
    z[70]=z[58] - z[2];
    z[70]=z[70]*z[51];
    z[77]=z[36]*z[3];
    z[70]= - z[77] + z[70];
    z[34]=n<T>(3,2)*z[70] + z[34];
    z[34]=z[4]*z[34];
    z[70]=z[8]*z[9];
    z[81]=z[2]*z[3];
    z[87]= - z[70] + n<T>(25,6)*z[81];
    z[87]=z[87]*z[88];
    z[84]=z[87] + z[84];
    z[87]=z[41] + n<T>(25,3)*z[62];
    z[87]=z[87]*z[69];
    z[87]=z[87] + z[84];
    z[87]=z[5]*z[87];
    z[34]=z[87] + z[34];
    z[34]=z[4]*z[34];
    z[87]=z[15]*z[3];
    z[89]=22*z[87];
    z[90]=n<T>(343,8)*z[85];
    z[91]=z[89] + z[90];
    z[91]=z[91]*z[9];
    z[92]=n<T>(25,2)*z[78];
    z[93]=z[9]*z[3];
    z[94]=z[92]*z[93];
    z[94]=z[91] - z[94];
    z[95]=z[93]*z[21];
    z[95]=n<T>(29,4)*z[95];
    z[96]= - z[94] - z[95];
    z[96]=z[66]*z[8]*z[96];
    z[97]=n<T>(1,2)*z[3];
    z[36]= - z[36]*z[97];
    z[98]=z[3] - z[67];
    z[51]=z[98]*z[51];
    z[36]=z[36] + z[51];
    z[36]=z[4]*z[36];
    z[51]=z[5]*z[77];
    z[36]=2*z[51] + z[36];
    z[36]=z[36]*npow(z[4],2);
    z[51]=z[1]*npow(z[4],3)*z[25]*z[77];
    z[36]=z[36] + z[51];
    z[36]=z[1]*z[36];
    z[34]=z[36] + z[96] + z[34];
    z[34]=z[1]*z[34];
    z[36]=z[9] + z[3];
    z[51]= - z[36]*z[92];
    z[51]=z[51] + z[90] + 22*z[83];
    z[51]=z[8]*z[51];
    z[77]=n<T>(343,4)*z[85];
    z[87]=z[77] + 44*z[87];
    z[87]=z[87]*z[9];
    z[92]=n<T>(1,2)*z[8];
    z[36]=z[36]*z[92];
    z[36]= - z[93] + z[36];
    z[36]=z[36]*z[21];
    z[36]=n<T>(29,2)*z[36] - z[87] + z[51];
    z[36]=z[2]*z[36];
    z[51]= - z[8]*z[94];
    z[36]=z[51] + z[36];
    z[51]=z[74] + z[84];
    z[51]=z[5]*z[51];
    z[24]=z[34] + z[24] + n<T>(1,3)*z[36] + z[51];
    z[24]=z[1]*z[24];
    z[34]=z[59] + z[68];
    z[34]=z[34]*z[8];
    z[36]=z[3] - z[34];
    z[36]=z[8]*z[36];
    z[51]=z[6]*z[90];
    z[74]=22*z[15];
    z[84]=z[10]*z[74];
    z[84]= - n<T>(629,8)*z[3] + z[84];
    z[84]=z[9]*z[84];
    z[36]=n<T>(25,2)*z[36] + z[51] + z[84];
    z[36]=z[8]*z[36];
    z[51]= - n<T>(271,2)*z[93] - 29*z[70];
    z[34]=n<T>(29,2)*z[34] + 17*z[3] + z[60];
    z[34]=z[2]*z[34];
    z[34]=n<T>(1,2)*z[51] + z[34];
    z[34]=z[34]*z[67];
    z[34]=z[34] + z[36] + n<T>(17,4)*z[85] + 44*z[83];
    z[34]=z[2]*z[34];
    z[36]= - z[57]*z[74];
    z[36]= - n<T>(343,8)*z[79] + z[36];
    z[36]=z[9]*z[36];
    z[51]=z[53]*z[97];
    z[51]= - z[3] + z[51];
    z[51]=z[51]*z[78];
    z[36]=25*z[51] + z[77] + z[36];
    z[36]=z[8]*z[36];
    z[51]=z[30]*z[3];
    z[34]=z[34] + z[36] - z[51] - z[87];
    z[36]=n<T>(103,8)*z[76] + 4*z[81];
    z[36]=z[2]*z[36];
    z[60]=n<T>(5,3)*z[3] - z[9];
    z[60]=n<T>(5,2)*z[60] + z[65];
    z[60]=z[5]*z[60];
    z[60]= - z[70] + z[60];
    z[60]=z[5]*z[60];
    z[36]=z[60] + z[36] + z[82];
    z[36]=z[5]*z[36];
    z[60]= - z[91] - z[95];
    z[60]=z[60]*z[66];
    z[41]= - z[41] + n<T>(5,3)*z[62];
    z[41]=z[41]*z[67];
    z[65]=z[88]*z[81];
    z[41]=z[41] - n<T>(25,3)*z[65];
    z[41]=z[41]*z[25];
    z[41]=z[60] + z[41];
    z[41]=z[7]*z[41];
    z[24]=z[24] + z[40] + z[41] + n<T>(1,3)*z[34] + z[36];
    z[24]=z[1]*z[24];
    z[34]=z[13]*z[12];
    z[36]=static_cast<T>(35)- n<T>(47,4)*z[34];
    z[36]=z[36]*z[17];
    z[40]=z[26]*z[6];
    z[41]=n<T>(47,4)*z[40];
    z[59]= - z[3]*z[59];
    z[59]= - n<T>(11,2)*z[8] + z[59];
    z[59]=z[8]*z[59];
    z[36]=z[59] - n<T>(55,2)*z[93] - 73*z[76] + z[36] + z[41];
    z[36]=z[8]*z[36];
    z[59]=z[30]*z[10];
    z[60]=n<T>(17,2)*z[76] - n<T>(247,16)*z[20] + z[59];
    z[60]=z[3]*z[60];
    z[36]=z[36] + z[27] + z[60];
    z[60]=n<T>(1231,16)*z[38];
    z[65]=npow(z[14],3);
    z[70]=z[65]*z[10];
    z[74]= - z[60] - 25*z[70];
    z[77]=n<T>(1,3)*z[10];
    z[74]=z[74]*z[77];
    z[78]=n<T>(25,6)*z[8];
    z[79]=z[10] + z[6];
    z[82]=z[79]*z[8];
    z[83]= - static_cast<T>(1)+ z[82];
    z[83]=z[83]*z[78];
    z[84]= - n<T>(293,16) - n<T>(73,3)*z[68];
    z[84]=z[3]*z[84];
    z[85]=n<T>(19,12) - z[53];
    z[85]=z[9]*z[85];
    z[74]=z[83] + n<T>(11,2)*z[85] + z[84] + n<T>(119,8)*z[14] + z[74];
    z[74]=z[8]*z[74];
    z[83]=50*z[70];
    z[60]= - z[60] - z[83];
    z[84]=11*z[9];
    z[85]= - n<T>(115,12)*z[3] - z[84];
    z[85]=z[85]*z[19];
    z[82]= - static_cast<T>(1)- z[82];
    z[82]=z[2]*z[82];
    z[82]=z[82] + z[8];
    z[82]= - z[3] + n<T>(271,12)*z[9] + n<T>(29,6)*z[82];
    z[67]=z[82]*z[67];
    z[60]=z[67] + z[74] + n<T>(1,3)*z[60] + z[85];
    z[60]=z[2]*z[60];
    z[67]=z[93]*z[65];
    z[74]=z[38]*z[9];
    z[82]=z[9]*z[14];
    z[85]=z[5]*z[82];
    z[85]=n<T>(1,2)*z[74] + z[85];
    z[87]=3*z[5];
    z[85]=z[85]*z[87];
    z[90]=z[3]*z[35];
    z[85]=z[90] + z[85];
    z[85]=z[5]*z[85];
    z[90]=n<T>(3,4)*z[74];
    z[32]=z[7]*z[32]*z[90];
    z[32]=z[32] + n<T>(25,3)*z[67] + z[85];
    z[32]=z[7]*z[32];
    z[85]=n<T>(265,2)*z[38] - 49*z[76];
    z[85]=z[3]*z[85];
    z[85]=n<T>(7,8)*z[85] - z[89];
    z[85]=z[9]*z[85];
    z[89]=25*z[65];
    z[91]=z[2]*z[93];
    z[91]= - z[89] - n<T>(271,8)*z[91];
    z[91]=z[2]*z[91];
    z[51]=z[91] - z[51] + z[85];
    z[76]= - n<T>(1361,6)*z[20] - 103*z[76];
    z[76]=z[76]*z[97];
    z[85]=z[47]*z[9];
    z[62]= - n<T>(283,6)*z[62] + z[76] + z[85];
    z[76]=z[5]*z[3];
    z[91]=9*z[82] - n<T>(25,3)*z[76];
    z[91]=z[5]*z[91];
    z[62]=n<T>(1,2)*z[62] + z[91];
    z[62]=z[62]*z[25];
    z[32]=z[32] + n<T>(1,3)*z[51] + z[62];
    z[32]=z[7]*z[32];
    z[41]= - 35*z[17] - z[41];
    z[41]=z[6]*z[41];
    z[51]= - static_cast<T>(65)+ 29*z[53];
    z[51]=z[51]*z[19];
    z[41]=z[51] + n<T>(259,4)*z[13] + z[41];
    z[51]=z[10] - z[6];
    z[51]=z[51]*z[8];
    z[62]= - static_cast<T>(2)- z[51];
    z[62]=z[62]*z[80];
    z[41]=n<T>(1,3)*z[41] + z[62];
    z[41]=z[8]*z[41];
    z[62]=static_cast<T>(1)+ n<T>(1,2)*z[57];
    z[62]=z[3]*z[62];
    z[62]= - n<T>(153,4)*z[11] + 103*z[62];
    z[62]=z[62]*z[97];
    z[62]=z[62] + 3*z[82];
    z[51]=n<T>(25,6)*z[73] - z[51];
    z[51]=z[5]*z[51];
    z[51]=z[51] + n<T>(5,2)*z[9] + z[8];
    z[51]=z[5]*z[51];
    z[41]=z[51] - n<T>(1231,48)*z[81] + n<T>(1,2)*z[62] + z[41];
    z[41]=z[5]*z[41];
    z[51]=n<T>(1,4)*z[3];
    z[62]=z[73]*z[3];
    z[73]=n<T>(113,4)*z[14] - n<T>(343,3)*z[62];
    z[73]=z[73]*z[51];
    z[91]=z[15]*z[57];
    z[73]=z[73] - n<T>(44,3)*z[91];
    z[73]=z[9]*z[73];
    z[18]=z[24] + z[18] + z[32] + z[41] + z[60] + z[73] + n<T>(1,3)*z[36];
    z[18]=z[1]*z[18];
    z[24]=n<T>(47,2)*z[40];
    z[32]=83*z[17] + z[24];
    z[32]=z[6]*z[32];
    z[32]=35*z[13] + z[32];
    z[32]=z[6]*z[32];
    z[32]=n<T>(7,2)*z[53] - n<T>(37,4) + z[32];
    z[36]=z[10]*z[6];
    z[41]=npow(z[6],2);
    z[36]=z[36] + z[41];
    z[36]=z[36]*z[8];
    z[60]= - z[36] + z[79];
    z[60]=z[60]*z[80];
    z[32]=n<T>(1,3)*z[32] + z[60];
    z[32]=z[8]*z[32];
    z[60]= - 41*z[17] - n<T>(47,3)*z[40];
    z[73]=n<T>(1,2)*z[6];
    z[60]=z[60]*z[73];
    z[79]=n<T>(1,8)*z[3];
    z[80]=static_cast<T>(79)+ 103*z[57];
    z[80]=z[80]*z[79];
    z[36]= - z[36] - n<T>(5,3)*z[6] + z[10];
    z[36]=z[5]*z[36];
    z[91]=z[8]*z[6];
    z[36]=z[36] - n<T>(9,2) + z[91];
    z[36]=z[5]*z[36];
    z[94]=n<T>(3,2)*z[14];
    z[95]=n<T>(181,8)*z[11] + 32*z[13];
    z[32]=z[36] + n<T>(285,16)*z[2] + z[32] - z[84] + z[80] + z[94] + n<T>(1,3)*
    z[95] + z[60];
    z[32]=z[5]*z[32];
    z[36]=n<T>(201,2)*z[11] - 103*z[62];
    z[36]=z[36]*z[79];
    z[60]= - n<T>(9,2) - n<T>(25,3)*z[57];
    z[60]=z[60]*z[88];
    z[36]=n<T>(1,2)*z[60] - n<T>(421,48)*z[81] - n<T>(15,4)*z[82] + z[36] + n<T>(1361,48)
   *z[20] + z[47];
    z[36]=z[5]*z[36];
    z[60]=12*z[38];
    z[80]=n<T>(50,3)*z[70];
    z[81]=n<T>(11,12)*z[21] - n<T>(905,48)*z[93] + z[80] - n<T>(31,16)*z[20] + z[60]
   ;
    z[81]=z[2]*z[81];
    z[84]= - z[5]*z[14];
    z[84]=z[84] + z[38] - n<T>(3,2)*z[82];
    z[84]=z[84]*z[71];
    z[95]=z[20]*z[3];
    z[96]= - z[30] - n<T>(2243,16)*z[95];
    z[84]=z[84] + n<T>(1,3)*z[96] - z[85];
    z[84]=z[5]*z[84];
    z[25]=z[25]*z[82];
    z[25]= - z[74] + z[25];
    z[25]=z[25]*z[71];
    z[74]=n<T>(40,3)*z[29];
    z[74]=z[3]*z[74];
    z[25]=z[74] + z[25];
    z[25]=z[5]*z[25];
    z[25]=n<T>(50,3)*z[67] + z[25];
    z[25]=z[7]*z[25];
    z[65]=5*z[65];
    z[67]=2*z[29] - z[65];
    z[67]=z[3]*z[67];
    z[26]=n<T>(47,2)*z[26];
    z[74]=z[26] - z[89];
    z[85]=z[38]*z[3];
    z[74]=n<T>(1,3)*z[74] + n<T>(759,16)*z[85];
    z[74]=z[9]*z[74];
    z[96]=z[29]*z[2];
    z[25]=z[25] + z[84] - n<T>(40,3)*z[96] + n<T>(10,3)*z[67] + z[74];
    z[25]=z[7]*z[25];
    z[67]=z[89] - z[27];
    z[74]= - 809*z[20] - 2053*z[38];
    z[84]=z[29]*z[10];
    z[74]=n<T>(1,16)*z[74] + 40*z[84];
    z[74]=z[3]*z[74];
    z[74]=z[74] + z[67];
    z[62]= - n<T>(731,2)*z[14] - 343*z[62];
    z[62]=z[62]*z[97];
    z[62]=z[62] + 301*z[17] - n<T>(1891,4)*z[38];
    z[89]= - n<T>(5,2) - n<T>(22,3)*z[57];
    z[15]=z[89]*z[15];
    z[15]=n<T>(1,12)*z[62] + z[15];
    z[15]=z[9]*z[15];
    z[15]=z[25] + z[36] + z[81] + n<T>(1,3)*z[74] + z[15];
    z[15]=z[7]*z[15];
    z[25]= - z[27] + 40*z[29];
    z[16]=n<T>(103,2)*z[17] + z[16];
    z[16]=z[16]*z[19];
    z[36]=n<T>(153,4)*z[20];
    z[62]=z[36] + n<T>(11,3)*z[21];
    z[62]=z[62]*z[69];
    z[74]=n<T>(3,4)*z[88];
    z[81]=3*z[9] + z[5];
    z[81]=z[81]*z[74];
    z[26]=z[9]*z[26];
    z[26]=z[26] - 40*z[96];
    z[26]=n<T>(1,3)*z[26] - n<T>(3,4)*z[33];
    z[26]=z[7]*z[26];
    z[16]=z[26] + z[81] + z[62] + n<T>(1,3)*z[25] + z[16];
    z[16]=z[7]*z[16];
    z[25]= - z[5]*z[10];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[87];
    z[25]=z[25] - z[64] - z[86];
    z[25]=z[5]*z[25];
    z[25]=z[25] - z[36] - 65*z[17];
    z[26]= - n<T>(65,2)*z[13] + z[61];
    z[26]=z[9]*z[26];
    z[33]=z[22] + 1;
    z[36]=z[2]*z[33];
    z[36]=n<T>(1201,4)*z[11] - 11*z[36];
    z[23]=z[36]*z[23];
    z[16]=z[16] + z[23] + n<T>(1,6)*z[26] - n<T>(40,3)*z[84] + n<T>(1,4)*z[25];
    z[16]=z[7]*z[16];
    z[23]=static_cast<T>(1)+ z[75];
    z[23]=z[23]*z[21];
    z[23]=z[23] - n<T>(1,2)*z[37];
    z[23]=z[7]*z[23];
    z[23]=z[23] + n<T>(3,2)*z[56] + z[42];
    z[23]=z[7]*z[23];
    z[21]=z[33]*z[21];
    z[21]=z[21] - z[37];
    z[21]=z[21]*z[39];
    z[25]=n<T>(3,4)*z[14];
    z[21]=z[21] + z[25] + z[42];
    z[21]=z[7]*z[21];
    z[26]=3*z[10];
    z[33]=z[14]*z[26];
    z[33]= - n<T>(133,3) + z[33];
    z[21]=z[21] - z[52] + n<T>(1,4)*z[33] + n<T>(7,3)*z[53];
    z[21]=z[7]*z[21];
    z[33]=z[8]*npow(z[10],3);
    z[36]=z[33] + z[48];
    z[37]=z[5] + z[14];
    z[36]= - z[36]*z[37];
    z[21]=n<T>(3,4)*z[36] + z[21];
    z[21]=z[4]*z[21];
    z[36]= - z[55] - z[43];
    z[36]=z[36]*z[26];
    z[42]= - z[94] - z[43];
    z[56]=n<T>(3,2)*z[49];
    z[42]=z[42]*z[56];
    z[56]= - z[10] - z[56];
    z[56]=z[56]*z[71];
    z[21]=z[21] + z[23] + z[56] - z[52] + z[42] + n<T>(35,4) + z[36];
    z[21]=z[4]*z[21];
    z[23]=n<T>(397,24)*z[20];
    z[36]=z[23] - z[47];
    z[36]=z[10]*z[36];
    z[42]= - z[25] - z[43];
    z[26]=z[42]*z[26];
    z[26]=n<T>(15,2)*z[53] - n<T>(2,3) + z[26];
    z[26]=z[8]*z[26];
    z[42]=n<T>(59,12)*z[54] + n<T>(53,12) - 2*z[57];
    z[42]=z[5]*z[42];
    z[52]= - static_cast<T>(2059)+ 1135*z[68];
    z[52]=z[2]*z[52];
    z[16]=z[21] + z[16] + z[42] + n<T>(1,48)*z[52] + z[26] + n<T>(13,2)*z[9] + 
   n<T>(439,48)*z[3] + z[36] - z[25] - n<T>(301,24)*z[11] - 2*z[13];
    z[16]=z[4]*z[16];
    z[21]= - static_cast<T>(83)+ n<T>(47,2)*z[34];
    z[21]=z[21]*z[17];
    z[21]=z[21] - z[24];
    z[21]=z[6]*z[21];
    z[25]= - n<T>(1843,16)*z[38] - z[83];
    z[25]=z[25]*z[77];
    z[26]= - n<T>(139,4) - 19*z[57];
    z[19]=z[26]*z[19];
    z[26]= - static_cast<T>(9)+ n<T>(11,3)*z[91];
    z[26]=z[26]*z[92];
    z[36]= - static_cast<T>(1)+ n<T>(53,6)*z[34];
    z[36]=z[13]*z[36];
    z[19]=z[26] + z[19] + n<T>(155,6)*z[3] + z[25] - n<T>(329,16)*z[14] + z[36]
    + n<T>(1,3)*z[21];
    z[19]=z[8]*z[19];
    z[21]=n<T>(401,16)*z[38] + 10*z[70];
    z[21]=z[10]*z[21];
    z[21]=n<T>(907,16)*z[14] + n<T>(5,3)*z[21];
    z[21]=z[10]*z[21];
    z[25]=z[6]*z[54];
    z[25]= - z[10] + z[25];
    z[25]=z[25]*z[78];
    z[21]=z[25] + n<T>(341,24)*z[53] + n<T>(1279,48)*z[68] + n<T>(363,16) + z[21];
    z[21]=z[8]*z[21];
    z[25]=n<T>(2581,16)*z[38] + 100*z[70];
    z[25]=z[25]*z[77];
    z[26]= - 29*z[54] - 11;
    z[26]=z[6]*z[26];
    z[26]=29*z[10] + z[26];
    z[26]=z[26]*z[69];
    z[26]=z[26] + static_cast<T>(7)+ n<T>(29,4)*z[91];
    z[26]=z[26]*z[66];
    z[36]= - n<T>(95,2)*z[11] + n<T>(1127,3)*z[14];
    z[21]=z[26] + z[21] + n<T>(215,12)*z[9] - z[58] + n<T>(1,8)*z[36] + z[25];
    z[21]=z[2]*z[21];
    z[25]= - static_cast<T>(39)+ n<T>(47,6)*z[34];
    z[25]=z[25]*z[17];
    z[25]=z[25] + n<T>(497,8)*z[38];
    z[26]=n<T>(41,2)*z[11] - 149*z[14];
    z[36]=z[10]*z[20];
    z[26]=5*z[26] - 397*z[36];
    z[26]=n<T>(1,6)*z[26] + 11*z[3];
    z[26]=z[26]*z[51];
    z[36]=n<T>(25,3)*z[70];
    z[42]=n<T>(13,3)*z[9] - n<T>(13,6)*z[3] - z[13] + n<T>(53,24)*z[14];
    z[42]=z[9]*z[42];
    z[15]=z[18] + z[16] + z[15] + z[32] + z[21] + z[19] + z[42] + z[26]
    + n<T>(1,2)*z[25] - z[36];
    z[15]=z[1]*z[15];
    z[16]= - 281*z[20] - 11*z[38];
    z[18]=4*z[29];
    z[19]=z[18] + z[65];
    z[19]=z[10]*z[19];
    z[16]=n<T>(1,8)*z[16] + 5*z[19];
    z[16]=z[3]*z[16];
    z[19]=static_cast<T>(67)+ 47*z[34];
    z[19]=z[19]*z[17];
    z[21]=25*z[38];
    z[25]=z[14]*z[12];
    z[26]=n<T>(1,8) + z[25];
    z[26]=z[26]*z[21];
    z[29]=z[3]*z[14];
    z[26]= - n<T>(719,16)*z[29] + n<T>(1,4)*z[19] + z[26];
    z[26]=z[9]*z[26];
    z[29]=n<T>(673,8)*z[20] + z[59];
    z[29]=z[2]*z[29];
    z[16]=z[29] + z[26] + z[16] + z[67];
    z[26]=n<T>(281,6)*z[20] - z[47];
    z[29]=z[3]*z[11];
    z[32]=z[14]*z[46];
    z[26]=z[32] - n<T>(3,4)*z[82] + n<T>(1403,48)*z[29] + n<T>(1,4)*z[26] - z[72];
    z[26]=z[5]*z[26];
    z[29]= - z[82]*z[46];
    z[29]=z[29] + z[90] - z[35] - n<T>(147,8)*z[95];
    z[29]=z[5]*z[29];
    z[32]=z[18] - z[65];
    z[32]=z[32]*z[45];
    z[35]=n<T>(211,8)*z[85] - z[67];
    z[35]=z[9]*z[35];
    z[31]= - z[31] + z[32] + z[35];
    z[32]=z[93]*z[65];
    z[18]=z[76]*z[18];
    z[18]=z[32] + z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(5,3)*z[18] + n<T>(1,3)*z[31] + z[29];
    z[18]=z[7]*z[18];
    z[16]=z[18] + n<T>(1,3)*z[16] + z[26];
    z[16]=z[7]*z[16];
    z[18]= - z[36] - z[23] - z[60];
    z[18]=z[10]*z[18];
    z[26]= - n<T>(77,2)*z[11] + n<T>(31,3)*z[14];
    z[18]=n<T>(7,8)*z[26] + z[18];
    z[18]=z[2]*z[18];
    z[24]= - 53*z[17] - z[24];
    z[24]=z[24]*z[73];
    z[24]=z[24] + n<T>(29,16)*z[11] + 22*z[13];
    z[26]=n<T>(397,6)*z[20] + z[47];
    z[29]=n<T>(1,4)*z[10];
    z[26]=z[26]*z[29];
    z[24]= - n<T>(41,6)*z[3] + z[26] + n<T>(1,3)*z[24] + z[94];
    z[24]=z[5]*z[24];
    z[26]=47*z[40];
    z[19]=z[19] + z[26];
    z[31]= - n<T>(3,8) - n<T>(1,3)*z[25];
    z[21]=z[31]*z[21];
    z[31]= - n<T>(397,3)*z[20] + 33*z[38];
    z[31]=z[10]*z[31];
    z[32]= - 29*z[11] + 307*z[14];
    z[31]=n<T>(1,6)*z[32] + z[31];
    z[31]=z[31]*z[79];
    z[32]= - static_cast<T>(109)+ 53*z[34];
    z[32]=z[32]*z[13];
    z[35]=n<T>(521,8) + 14*z[25];
    z[35]=z[14]*z[35];
    z[35]= - 13*z[3] + n<T>(1,2)*z[32] + z[35];
    z[35]=z[35]*z[63];
    z[16]=z[16] + z[24] + z[18] + z[35] + z[31] - z[36] - n<T>(1,12)*z[19]
    + z[21];
    z[16]=z[7]*z[16];
    z[18]=z[74] + n<T>(19,2)*z[17];
    z[18]=z[9]*z[18];
    z[21]=z[30] - z[27];
    z[24]=z[7]*z[28];
    z[27]=z[2]*z[20];
    z[18]=z[24] + n<T>(673,24)*z[27] + n<T>(1,3)*z[21] + z[18];
    z[18]=z[7]*z[18];
    z[20]= - n<T>(47,6)*z[40] - n<T>(673,12)*z[20] - 19*z[17];
    z[21]=z[9] - z[5];
    z[21]=z[21]*z[46];
    z[24]=z[9]*z[13];
    z[27]=z[2]*z[11];
    z[18]=z[18] + z[21] - n<T>(829,16)*z[27] - 9*z[24] + n<T>(1,2)*z[20] - z[72]
   ;
    z[18]=z[7]*z[18];
    z[20]=z[6]*z[17];
    z[21]=z[10]*z[23];
    z[18]=z[18] - z[71] + n<T>(331,12)*z[2] - n<T>(22,3)*z[9] + z[21] - n<T>(2,3)*
    z[20] + n<T>(539,16)*z[11] + n<T>(5,3)*z[13];
    z[18]=z[7]*z[18];
    z[20]= - z[33]*z[55];
    z[21]= - z[48] - n<T>(1,2)*z[33];
    z[21]=z[5]*z[21];
    z[23]= - z[39]*z[10]*z[37];
    z[24]= - z[14]*z[48];
    z[20]=z[23] + z[21] + z[24] + z[20];
    z[20]=z[4]*z[20];
    z[21]= - z[44] - n<T>(397,12)*z[11] - 15*z[14];
    z[21]=z[21]*z[29];
    z[23]= - z[14] - n<T>(1,4)*z[43];
    z[23]=z[23]*z[49];
    z[24]= - z[50] - z[49];
    z[24]=z[24]*z[87];
    z[27]= - z[6]*z[13];
    z[27]= - n<T>(281,8) + z[27];
    z[18]=n<T>(3,2)*z[20] + z[18] + z[24] - n<T>(119,16)*z[22] + 3*z[23] - n<T>(19,6)
   *z[53] + n<T>(1,3)*z[27] + z[21];
    z[18]=z[4]*z[18];
    z[20]=static_cast<T>(16)- n<T>(47,12)*z[34];
    z[20]=z[20]*z[17];
    z[21]=n<T>(47,12)*z[40];
    z[20]=z[20] + z[21];
    z[20]=z[6]*z[20];
    z[22]=static_cast<T>(37)- n<T>(53,2)*z[34];
    z[22]=z[13]*z[22];
    z[20]=n<T>(1,3)*z[22] + z[20];
    z[20]=z[6]*z[20];
    z[22]=n<T>(213,8) - n<T>(25,3)*z[25];
    z[22]=z[22]*z[38];
    z[22]=z[22] + z[80];
    z[22]=z[10]*z[22];
    z[23]= - n<T>(17,2) - 12*z[25];
    z[23]=z[14]*z[23];
    z[22]=z[23] + z[22];
    z[22]=z[10]*z[22];
    z[23]= - n<T>(137,8) + 22*z[34];
    z[20]=z[22] + n<T>(217,24)*z[25] + n<T>(1,3)*z[23] + z[20];
    z[20]=z[8]*z[20];
    z[22]=149*z[17] + z[26];
    z[22]=z[6]*z[22];
    z[22]=5*z[13] + n<T>(1,6)*z[22];
    z[22]=z[6]*z[22];
    z[17]= - 16*z[17] - z[21];
    z[17]=z[6]*z[17];
    z[17]= - n<T>(191,12)*z[13] + z[17];
    z[17]=z[17]*z[41];
    z[17]=z[17] + n<T>(23,12)*z[10];
    z[17]=z[8]*z[17];
    z[21]=n<T>(397,24)*z[11];
    z[23]= - z[21] + 3*z[14];
    z[23]=z[10]*z[23];
    z[17]=z[17] - 3*z[57] + n<T>(1,2)*z[23] - n<T>(829,48) + z[22];
    z[17]=z[5]*z[17];
    z[19]= - z[19]*z[73];
    z[19]= - z[32] + z[19];
    z[22]=n<T>(17,8) + z[25];
    z[22]=z[22]*z[38];
    z[22]=z[22] + z[70];
    z[23]=25*z[10];
    z[22]=z[22]*z[23];
    z[24]= - static_cast<T>(37)+ 11*z[25];
    z[24]=z[14]*z[24];
    z[19]=z[22] + n<T>(1,2)*z[19] + z[24];
    z[22]=n<T>(13,6)*z[57] - n<T>(305,24)*z[25] - n<T>(59,8) - n<T>(22,3)*z[34];
    z[22]=z[9]*z[22];
    z[24]= - n<T>(9,8)*z[38] - n<T>(2,3)*z[70];
    z[23]=z[24]*z[23];
    z[21]=z[21] + 11*z[14];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[21]=z[10]*z[21];
    z[23]= - n<T>(129,8)*z[38] - z[36];
    z[23]=z[10]*z[23];
    z[23]= - n<T>(269,48)*z[14] + z[23];
    z[23]=z[10]*z[23];
    z[23]= - n<T>(401,48) + z[23];
    z[23]=z[10]*z[23];
    z[23]=n<T>(33,16)*z[6] + z[23];
    z[23]=z[8]*z[23];
    z[21]=z[23] + n<T>(103,12) + z[21];
    z[21]=z[2]*z[21];
    z[23]=n<T>(397,3)*z[11] + 33*z[14];
    z[23]=z[10]*z[23];
    z[23]=static_cast<T>(19)+ n<T>(1,8)*z[23];
    z[23]=z[23]*z[97];
    z[15]=z[15] + z[18] + z[16] + z[17] + z[21] + z[20] + z[22] + n<T>(1,3)*
    z[19] + z[23];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r413(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r413(const std::array<dd_real,31>&);
#endif
