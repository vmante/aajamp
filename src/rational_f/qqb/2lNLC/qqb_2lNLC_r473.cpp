#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r473(const std::array<T,31>& k) {
  T z[40];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[5];
    z[10]=k[4];
    z[11]=k[8];
    z[12]=k[15];
    z[13]=k[2];
    z[14]=z[2]*z[9];
    z[15]=z[3]*z[10];
    z[16]= - z[15] + z[14];
    z[17]=2*z[5];
    z[16]=z[16]*z[17];
    z[18]=3*z[3];
    z[19]=2*z[2];
    z[20]=z[6]*z[10];
    z[21]= - static_cast<T>(3)+ z[20];
    z[21]=z[6]*z[21];
    z[16]=z[16] + z[19] - z[18] + z[21];
    z[16]=z[5]*z[16];
    z[21]=z[15] + 1;
    z[22]=npow(z[10],2);
    z[23]= - z[3]*z[22];
    z[23]=z[9] + z[23];
    z[23]=z[5]*z[23];
    z[23]=z[23] + 3*z[21] - z[20];
    z[23]=z[5]*z[23];
    z[24]=2*z[6];
    z[25]=z[7]*z[10];
    z[26]=static_cast<T>(3)- z[25];
    z[26]=z[26]*z[24];
    z[27]=z[24]*z[8];
    z[28]=z[3]*z[8];
    z[29]= - z[28] + z[27];
    z[29]=z[2]*z[29];
    z[23]=z[23] + z[29] + z[26] - z[7] - z[18];
    z[26]=z[10] + z[9];
    z[29]=z[17]*z[10];
    z[30]=z[26]*z[29];
    z[31]=2*z[10];
    z[32]= - z[9] - z[31];
    z[30]=3*z[32] + z[30];
    z[30]=z[5]*z[30];
    z[32]=z[10] - z[9];
    z[33]=z[7]*z[32];
    z[30]=z[30] + static_cast<T>(4)+ z[33];
    z[30]=z[4]*z[30];
    z[23]=2*z[23] + z[30];
    z[23]=z[4]*z[23];
    z[30]= - static_cast<T>(3)- z[25];
    z[30]=z[6]*z[30];
    z[30]= - 7*z[7] + z[30];
    z[30]=z[6]*z[30];
    z[33]=3*z[7];
    z[34]= - 6*z[6] - z[33] + 13*z[3];
    z[34]=z[2]*z[34];
    z[16]=z[23] + z[16] + z[30] + z[34];
    z[16]=z[4]*z[16];
    z[23]=z[7]*z[9];
    z[27]= - z[27] + static_cast<T>(2)- z[23];
    z[27]=z[2]*z[27];
    z[26]=z[26]*z[17];
    z[30]= - static_cast<T>(2)- z[14];
    z[26]=3*z[30] + z[26];
    z[26]=z[5]*z[26];
    z[30]= - static_cast<T>(1)+ z[25];
    z[30]=z[6]*z[30];
    z[26]=z[26] + z[27] + z[7] + z[30];
    z[26]=z[4]*z[26];
    z[27]= - z[33] + z[24];
    z[27]=z[6]*z[27];
    z[30]=z[6]*z[8];
    z[34]=static_cast<T>(11)+ 3*z[30];
    z[34]=z[6]*z[34];
    z[34]=z[34] - z[33] - 4*z[3];
    z[34]=z[2]*z[34];
    z[35]=z[19] + z[3];
    z[21]=z[21]*z[5];
    z[36]=3*z[35] - z[21];
    z[36]=z[36]*z[17];
    z[26]=z[26] + z[36] + z[27] + z[34];
    z[26]=z[4]*z[26];
    z[27]=z[17]*z[2];
    z[34]=npow(z[6],2);
    z[36]=z[2]*z[3];
    z[36]=z[27] - z[34] - 9*z[36];
    z[36]=z[5]*z[36];
    z[37]=z[2]*z[6];
    z[38]= - 2*z[7] - z[6];
    z[38]=z[38]*z[37];
    z[26]=z[26] + 3*z[38] + z[36];
    z[26]=z[4]*z[26];
    z[36]=3*z[2];
    z[38]=static_cast<T>(1)+ z[14];
    z[38]=z[5]*z[38];
    z[38]= - z[36] + z[38];
    z[38]=z[38]*z[17];
    z[39]=z[6]*z[7];
    z[30]= - static_cast<T>(2)- z[30];
    z[30]=z[6]*z[30];
    z[30]=z[7] + z[30];
    z[30]=z[2]*z[30];
    z[30]=z[38] + z[39] + z[30];
    z[30]=z[4]*z[30];
    z[38]= - 4*z[7] + z[6];
    z[38]=z[38]*z[37];
    z[39]=z[18]*z[2];
    z[35]= - z[5]*z[35];
    z[35]=z[39] + z[35];
    z[17]=z[35]*z[17];
    z[17]=z[30] + z[38] + z[17];
    z[17]=z[4]*z[17];
    z[19]=z[19]*npow(z[5],2);
    z[30]=z[19]*z[3];
    z[17]=z[30] + z[17];
    z[17]=z[4]*z[17];
    z[35]=z[7]*z[37];
    z[19]=z[35] + z[19];
    z[19]=z[4]*z[19];
    z[19]= - z[30] + z[19];
    z[19]=z[1]*z[19]*npow(z[4],2);
    z[17]=z[17] + z[19];
    z[17]=z[1]*z[17];
    z[19]= - z[6]*z[25];
    z[19]= - z[7] + z[19];
    z[30]=npow(z[3],2);
    z[19]=z[6]*z[30]*z[19];
    z[21]= - z[3]*z[34]*z[21];
    z[19]=z[19] + z[21];
    z[17]=z[17] + 2*z[19] + z[26];
    z[17]=z[1]*z[17];
    z[19]=static_cast<T>(1)+ z[25];
    z[19]=z[19]*z[24];
    z[21]=static_cast<T>(2)- 3*z[25];
    z[21]=z[3]*z[21];
    z[19]=z[19] - z[33] + z[21];
    z[19]=z[3]*z[19];
    z[21]=npow(z[11],2);
    z[19]= - 4*z[21] + z[19];
    z[19]=z[6]*z[19];
    z[26]= - static_cast<T>(2)- z[15];
    z[26]=z[26]*z[27];
    z[27]= - z[6]*z[15];
    z[27]= - 5*z[3] + z[27];
    z[27]=z[6]*z[27];
    z[26]=z[26] + z[27] - z[39];
    z[26]=z[5]*z[26];
    z[27]= - z[30]*z[33];
    z[30]=2*z[9];
    z[34]=z[30]*z[21];
    z[35]=3*z[11];
    z[37]=z[3] + z[35] + z[34];
    z[24]=z[37]*z[24];
    z[37]=z[12]*z[18];
    z[24]=z[37] + z[24];
    z[24]=z[2]*z[24];
    z[16]=z[17] + z[16] + z[26] + z[24] + z[27] + z[19];
    z[16]=z[1]*z[16];
    z[17]=4*z[5];
    z[19]= - z[32]*z[17];
    z[24]=z[12]*z[30];
    z[24]=static_cast<T>(3)+ z[24];
    z[14]=z[19] + z[14] + 5*z[20] + 2*z[24] - 3*z[15];
    z[14]=z[5]*z[14];
    z[19]=z[21]*z[9];
    z[20]=z[19] - z[11];
    z[21]= - z[20]*z[30];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[7]*z[21];
    z[24]= - static_cast<T>(1)- 2*z[25];
    z[24]=z[6]*z[24];
    z[21]=z[24] + 6*z[3] - z[34] + z[21];
    z[24]=z[30] - z[10];
    z[24]=z[24]*z[29];
    z[26]= - z[9] + z[31];
    z[24]=3*z[26] + z[24];
    z[24]=z[5]*z[24];
    z[26]=z[5]*z[22]*z[30];
    z[27]=3*z[10];
    z[29]= - z[9]*z[27];
    z[26]=z[29] + z[26];
    z[26]=z[5]*z[26];
    z[29]= - z[9]*z[25];
    z[26]=z[29] + z[26];
    z[26]=z[4]*z[26];
    z[29]= - z[9] - z[27];
    z[29]=z[7]*z[29];
    z[24]=z[26] + z[24] - static_cast<T>(2)+ z[29];
    z[24]=z[4]*z[24];
    z[23]=z[28] - static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[36];
    z[14]=z[24] + z[14] + 2*z[21] + z[23];
    z[14]=z[4]*z[14];
    z[21]= - z[10]*z[12];
    z[21]= - z[28] - static_cast<T>(1)+ z[21];
    z[21]=z[3]*z[21];
    z[23]=2*z[11];
    z[19]=z[21] + 4*z[19] - z[12] + z[23];
    z[20]=4*z[20];
    z[21]=z[20]*z[9];
    z[24]=z[6]*z[21];
    z[19]=z[24] - z[33] + 2*z[19];
    z[19]=z[2]*z[19];
    z[24]=z[2] - 5*z[6];
    z[24]=z[15]*z[24];
    z[18]= - z[12] - z[18];
    z[17]= - z[17] + 3*z[18] + z[24];
    z[17]=z[5]*z[17];
    z[18]= - z[7]*z[11];
    z[24]=z[3]*z[12];
    z[18]=z[18] + z[24];
    z[24]=static_cast<T>(4)- z[25];
    z[24]=z[3]*z[24];
    z[20]=z[6] + z[24] - z[20] - 5*z[7];
    z[20]=z[6]*z[20];
    z[14]=z[16] + z[14] + z[17] + z[19] + 2*z[18] + z[20];
    z[14]=z[1]*z[14];
    z[16]=z[31] + z[30];
    z[17]=z[34] - z[11];
    z[16]=z[17]*z[16];
    z[16]=static_cast<T>(3)+ z[16];
    z[16]=z[7]*z[16];
    z[18]=npow(z[8],2);
    z[19]=z[10]*z[8];
    z[18]=z[18] + z[19];
    z[18]=z[3]*z[7]*z[18];
    z[19]= - z[13] + z[31];
    z[19]=z[12]*z[19];
    z[20]= - z[8]*z[33];
    z[18]=z[18] + z[20] + z[19];
    z[18]=z[3]*z[18];
    z[17]= - z[12] - z[17];
    z[17]=z[17]*z[31];
    z[15]= - z[15] + 1;
    z[15]=z[8]*z[15];
    z[19]= - z[12]*z[22];
    z[15]=z[19] + z[15];
    z[15]=z[3]*z[15];
    z[19]= - z[12] - z[23];
    z[19]=z[9]*z[19];
    z[15]=z[15] + z[17] - static_cast<T>(2)+ z[19];
    z[15]=z[2]*z[15];
    z[17]=z[35] - z[34];
    z[17]=z[17]*z[30];
    z[17]= - static_cast<T>(5)+ z[17];
    z[17]=z[9]*z[17];
    z[17]=z[17] - z[27];
    z[17]=z[7]*z[17];
    z[19]=z[5]*z[12]*npow(z[9],2);
    z[17]=z[19] + z[17] + static_cast<T>(1)- z[21];
    z[17]=z[4]*z[17];
    z[19]= - z[9] + z[13];
    z[19]=z[12]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[5]*z[19];
    z[14]=z[14] + z[17] + z[19] + z[15] + z[18] + z[12] + z[16];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r473(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r473(const std::array<dd_real,31>&);
#endif
