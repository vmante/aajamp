#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r45(const std::array<T,31>& k) {
  T z[81];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[13];
    z[6]=k[12];
    z[7]=k[14];
    z[8]=k[15];
    z[9]=k[10];
    z[10]=k[11];
    z[11]=k[3];
    z[12]=k[30];
    z[13]=k[4];
    z[14]=k[21];
    z[15]=k[5];
    z[16]=k[18];
    z[17]=k[23];
    z[18]=k[26];
    z[19]=k[29];
    z[20]=z[3] - z[18];
    z[21]=z[20]*z[2];
    z[22]=z[4] - z[17];
    z[23]=z[22]*z[7];
    z[24]=z[21] + z[23];
    z[24]=z[6]*z[24];
    z[25]=z[4] + z[19];
    z[26]=z[25]*npow(z[7],2);
    z[27]=z[16] - z[2];
    z[27]=z[27]*npow(z[3],2);
    z[24]=z[24] + z[26] - z[27];
    z[28]=n<T>(1,2)*z[6];
    z[24]=z[24]*z[28];
    z[29]=npow(z[8],2);
    z[30]=z[29]*z[16];
    z[31]=n<T>(1,2)*z[7];
    z[32]= - z[31]*z[30];
    z[33]=z[29]*z[12];
    z[34]=z[29]*z[7];
    z[35]= - n<T>(1,2)*z[33] + z[34];
    z[35]=z[10]*z[35];
    z[36]=z[3] + z[17];
    z[37]=z[36]*z[5];
    z[38]=z[37]*z[10];
    z[39]=z[3] + z[14];
    z[40]=z[39]*npow(z[10],2);
    z[41]=z[40] + z[38];
    z[42]=n<T>(1,2)*z[5];
    z[41]=z[41]*z[42];
    z[24]=z[24] + z[41] + z[32] + z[35];
    z[24]=z[15]*z[24];
    z[32]=z[17] - z[19];
    z[35]=n<T>(1,2)*z[4];
    z[41]=z[35] - z[32];
    z[41]=z[7]*z[41];
    z[43]=z[22] + z[20];
    z[44]= - z[43]*z[28];
    z[45]=z[3]*z[16];
    z[46]=n<T>(1,2)*z[18];
    z[47]= - z[46] - z[3];
    z[47]=z[2]*z[47];
    z[41]=z[44] + z[41] + z[45] + z[47];
    z[41]=z[6]*z[41];
    z[44]=z[39]*z[10];
    z[47]=n<T>(1,2)*z[8];
    z[48]= - z[12] - z[47];
    z[48]=z[8]*z[48];
    z[49]=n<T>(3,2)*z[8];
    z[50]=z[7]*z[49];
    z[48]=n<T>(1,2)*z[44] + z[48] + z[50];
    z[48]=z[10]*z[48];
    z[50]=z[12] + z[16];
    z[29]=z[50]*z[29];
    z[29]=z[29] - z[27];
    z[25]=z[25]*z[31];
    z[47]=z[16] - z[47];
    z[47]=z[8]*z[47];
    z[25]=z[47] + z[25];
    z[25]=z[7]*z[25];
    z[47]=n<T>(1,2)*z[14];
    z[50]= - z[47] - z[17];
    z[50]=z[10]*z[50];
    z[50]=z[50] - n<T>(1,2)*z[37];
    z[50]=z[5]*z[50];
    z[24]=z[24] + z[41] + z[50] + z[48] + n<T>(1,2)*z[29] + z[25];
    z[24]=z[15]*z[24];
    z[25]=3*z[14];
    z[29]=n<T>(1,2)*z[17];
    z[41]=n<T>(1,2)*z[16];
    z[48]=3*z[18];
    z[50]= - z[19] + z[48];
    z[50]= - z[49] - z[41] - z[29] + n<T>(1,2)*z[50] + z[25];
    z[50]=z[7]*z[50];
    z[51]=z[20]*z[13];
    z[52]=z[4]*z[11];
    z[53]=z[11]*z[17];
    z[53]=z[51] + z[53] - z[52];
    z[28]=z[53]*z[28];
    z[53]=3*z[7];
    z[28]=z[28] - z[53] + 2*z[2] - z[41] + n<T>(1,2)*z[19] + 3*z[12] - z[22]
   ;
    z[28]=z[6]*z[28];
    z[41]=z[2]*z[3];
    z[54]=z[41] - z[45];
    z[55]=3*z[16];
    z[56]=z[8]*z[13];
    z[57]= - z[16]*z[56];
    z[57]=z[57] - z[12] - z[55];
    z[57]=z[8]*z[57];
    z[58]=z[13]*z[27];
    z[57]=z[57] - 3*z[54] + z[58];
    z[58]=z[17] - z[12];
    z[56]=static_cast<T>(1)+ z[56];
    z[56]=z[8]*z[56];
    z[56]=z[56] + z[14] + z[58];
    z[59]=n<T>(1,2)*z[13];
    z[60]=z[44]*z[59];
    z[56]=z[60] + n<T>(1,2)*z[56] + z[7];
    z[56]=z[10]*z[56];
    z[60]= - z[37]*z[59];
    z[61]=n<T>(3,2)*z[19];
    z[62]=2*z[12];
    z[60]=z[60] - z[10] + z[17] - z[61] - z[62];
    z[60]=z[5]*z[60];
    z[24]=z[24] + z[28] + z[60] + z[56] + n<T>(1,2)*z[57] + z[50];
    z[24]=z[15]*z[24];
    z[28]=2*z[14];
    z[50]=n<T>(3,2)*z[17];
    z[56]=z[28] - z[50];
    z[57]= - z[18] + z[12];
    z[57]=n<T>(1,2)*z[57] - z[56];
    z[57]=z[11]*z[57];
    z[60]=n<T>(3,2)*z[2];
    z[56]= - z[60] + z[56];
    z[56]=z[13]*z[56];
    z[63]=npow(z[11],2);
    z[64]=z[63]*z[12];
    z[65]=z[63]*z[4];
    z[64]=z[64] - z[65];
    z[66]=n<T>(1,2)*z[9];
    z[67]= - z[64]*z[66];
    z[68]=z[52] - 1;
    z[69]=z[2]*z[11];
    z[56]=z[67] + z[56] + n<T>(1,2)*z[69] + z[57] - z[68];
    z[56]=z[9]*z[56];
    z[57]=z[4] - z[12];
    z[67]=npow(z[11],3);
    z[70]= - z[66]*z[67]*z[57];
    z[71]=z[18] - n<T>(5,2)*z[12];
    z[71]=z[71]*z[63];
    z[72]=n<T>(3,2)*z[13];
    z[70]=z[70] + z[72] + z[71] + 2*z[65];
    z[70]=z[9]*z[70];
    z[63]=z[63]*z[18];
    z[63]=z[63] + z[65];
    z[65]= - z[18] - z[4];
    z[65]=z[9]*z[67]*z[65];
    z[65]=z[65] + z[63];
    z[65]=z[65]*z[42];
    z[46]= - z[55] + z[46] + 4*z[12];
    z[46]=z[11]*z[46];
    z[32]= - z[16] + z[32];
    z[32]= - z[10] + n<T>(3,2)*z[32] + z[3];
    z[32]=z[13]*z[32];
    z[32]=z[65] + z[70] + z[46] + z[32] - z[68];
    z[32]=z[5]*z[32];
    z[46]=z[13]*z[2];
    z[65]=z[46]*z[19];
    z[67]=z[2] - z[14];
    z[68]=z[65] - z[19] - z[67];
    z[70]=npow(z[13],2);
    z[68]=z[68]*z[70];
    z[70]=z[70]*z[9];
    z[71]=z[47] - z[2];
    z[71]=z[13]*z[71];
    z[71]=n<T>(1,2) + z[71];
    z[71]=z[71]*z[70];
    z[68]=n<T>(1,2)*z[68] + z[71];
    z[68]=z[8]*z[68];
    z[71]=3*z[2];
    z[73]= - z[71] + z[55] - z[19] + z[14];
    z[73]=n<T>(1,2)*z[73] - z[65];
    z[73]=z[13]*z[73];
    z[74]=z[9]*z[13];
    z[71]=5*z[14] - z[71];
    z[71]=z[13]*z[71];
    z[71]= - static_cast<T>(1)+ z[71];
    z[71]=z[71]*z[74];
    z[68]=z[68] + n<T>(1,2)*z[71] + n<T>(3,2) + z[73];
    z[68]=z[8]*z[68];
    z[58]=z[11]*z[58];
    z[58]= - z[69] + 3*z[58] - z[52];
    z[51]=n<T>(1,2)*z[58] - z[51];
    z[51]=z[6]*z[51];
    z[58]=3*z[45];
    z[71]=z[19] + z[3];
    z[71]=z[2]*z[71];
    z[71]= - z[58] + z[71];
    z[59]=z[71]*z[59];
    z[71]= - z[18] + z[14];
    z[71]=z[71]*z[72];
    z[71]= - static_cast<T>(2)+ z[71];
    z[71]=z[7]*z[71];
    z[73]=n<T>(3,2)*z[18];
    z[39]= - z[7] + z[39];
    z[39]=z[13]*z[39];
    z[39]=static_cast<T>(2)+ z[39];
    z[39]=z[10]*z[39];
    z[24]=z[24] + z[51] + z[32] + z[39] + z[71] + z[68] + z[56] + z[59]
    - z[35] - z[73] - z[28];
    z[32]=n<T>(169,27)*z[2];
    z[35]= - z[12] - z[14];
    z[35]=z[32] + n<T>(3,2)*z[35] + z[3];
    z[35]=z[9]*z[35];
    z[39]= - n<T>(595,2)*z[2] - 142*z[9];
    z[39]=n<T>(1,27)*z[39] + z[49];
    z[39]=z[7]*z[39];
    z[51]=z[8]*z[4];
    z[56]=z[3]*z[4];
    z[35]= - n<T>(3,2)*z[44] + z[39] + z[51] + z[56] + z[35];
    z[35]=z[10]*z[35];
    z[39]=n<T>(1,27)*z[5];
    z[51]=z[10] - z[2];
    z[51]= - n<T>(757,2)*z[7] + 169*z[9] - 142*z[51];
    z[51]=z[51]*z[39];
    z[45]=n<T>(3,2)*z[45];
    z[59]=n<T>(3,2)*z[16];
    z[68]= - z[59] + z[4];
    z[68]=z[8]*z[68];
    z[22]= - n<T>(3,2)*z[22] + n<T>(169,27)*z[9];
    z[22]=z[7]*z[22];
    z[71]=z[32] + n<T>(5,2)*z[8];
    z[71]=z[10]*z[71];
    z[22]=z[51] + z[71] + z[22] - z[45] + z[68];
    z[22]=z[6]*z[22];
    z[51]=z[66]*z[8];
    z[68]=z[9]*z[2];
    z[71]=z[51] + n<T>(1,2)*z[56] - n<T>(311,27)*z[68];
    z[71]=z[7]*z[71];
    z[51]= - z[4]*z[51];
    z[51]=z[51] + z[71];
    z[51]=z[10]*z[51];
    z[71]=z[8]*z[9];
    z[75]=z[71] - z[41];
    z[75]=z[4]*z[75];
    z[76]=z[9]*z[56];
    z[75]=z[76] + z[75];
    z[66]=z[3] - z[66];
    z[66]=z[7]*z[8]*z[66];
    z[76]= - z[10]*z[3]*z[31];
    z[66]=z[76] + n<T>(1,2)*z[75] + z[66];
    z[66]=z[5]*z[66];
    z[75]=z[9]*z[3];
    z[75]= - z[41] + z[75];
    z[76]=z[7]*z[9];
    z[77]= - n<T>(311,27)*z[2] - z[31];
    z[77]=z[10]*z[77];
    z[75]=z[77] + n<T>(1,2)*z[75] - n<T>(338,27)*z[76];
    z[75]=z[5]*z[75];
    z[77]=z[4] + z[9];
    z[77]=z[8]*z[77];
    z[78]=z[10]*z[4];
    z[77]=z[78] + z[77];
    z[77]=z[31]*z[77];
    z[71]=z[3]*z[71];
    z[71]=z[75] + z[71] + z[77];
    z[71]=z[6]*z[71];
    z[75]=z[2]*z[4];
    z[77]= - z[75] - z[68];
    z[77]=z[8]*z[77]*z[31];
    z[51]=z[71] + z[66] + z[77] + z[51];
    z[51]=z[1]*z[51];
    z[66]= - z[59] - z[4];
    z[66]=z[3]*z[66];
    z[71]= - z[16]*z[49];
    z[66]=z[71] + n<T>(142,27)*z[68] + z[66] + n<T>(5,2)*z[41];
    z[66]=z[7]*z[66];
    z[68]=z[11]*z[12];
    z[71]=z[68] - z[52];
    z[71]=z[71]*z[9];
    z[77]= - z[4] + z[71];
    z[78]=n<T>(3,2)*z[9];
    z[77]=z[77]*z[78];
    z[79]= - z[32] + z[50] + z[3];
    z[79]=z[10]*z[79];
    z[80]= - z[4] + z[2];
    z[80]=z[8]*z[80];
    z[76]=z[79] - n<T>(169,27)*z[76] + z[77] + z[80];
    z[76]=z[5]*z[76];
    z[77]= - z[74] - 1;
    z[77]=z[49]*z[67]*z[77];
    z[75]= - z[75] + z[77];
    z[75]=z[8]*z[75];
    z[77]= - z[16] + z[10];
    z[77]=z[34]*z[77];
    z[79]=z[6]*z[23];
    z[79]=z[79] - z[27];
    z[79]=z[6]*z[79];
    z[36]=z[10]*z[36]*npow(z[5],2);
    z[36]=z[79] + z[36] + z[77];
    z[36]=z[15]*z[36];
    z[57]=z[57]*z[78];
    z[56]=z[56] + z[57];
    z[56]=z[9]*z[56];
    z[41]= - z[4]*z[41];
    z[22]=z[51] + n<T>(3,2)*z[36] + z[22] + z[76] + z[35] + z[66] + z[75] + 
    z[41] + z[56];
    z[22]=z[1]*z[22];
    z[35]=n<T>(3,2)*z[12];
    z[29]=z[59] - z[29] + z[35] + z[61] - z[18];
    z[36]=n<T>(257,54)*z[69];
    z[41]= - z[11]*z[50];
    z[50]= - z[8]*z[72];
    z[41]=z[50] - z[36] + n<T>(169,27) + z[41];
    z[41]=z[10]*z[41];
    z[50]=z[9]*z[18];
    z[20]=z[6]*z[20];
    z[20]=z[20] + z[50];
    z[20]=z[72]*z[20];
    z[50]= - z[19] + z[16];
    z[50]=z[13]*z[50];
    z[50]=z[50] + z[74];
    z[49]=z[50]*z[49];
    z[50]=n<T>(419,2)*z[74];
    z[51]=z[10]*z[13];
    z[51]= - n<T>(419,2)*z[51] - static_cast<T>(257)- z[50];
    z[39]=z[51]*z[39];
    z[51]=2*z[3];
    z[50]= - static_cast<T>(439)- z[50];
    z[50]=z[7]*z[50];
    z[20]=z[39] + z[41] + n<T>(1,27)*z[50] + z[49] + z[32] + 3*z[29] + z[51]
    + z[20];
    z[20]=z[6]*z[20];
    z[29]=3*z[9];
    z[32]= - z[64]*z[29];
    z[39]= - z[48] + 5*z[12];
    z[39]=z[11]*z[39];
    z[32]=z[32] - 6*z[52] - n<T>(527,27) + n<T>(3,2)*z[39];
    z[32]=z[9]*z[32];
    z[39]= - z[17]*z[72];
    z[39]=n<T>(419,54)*z[74] - n<T>(250,27) + z[39];
    z[39]=z[7]*z[39];
    z[41]=z[5]*z[63]*z[78];
    z[49]=n<T>(1,2)*z[10];
    z[50]=z[55] - z[17] - z[18] - z[12];
    z[32]=z[41] + z[49] + z[39] + z[32] + n<T>(3,2)*z[50] - z[51];
    z[32]=z[5]*z[32];
    z[39]=3*z[19] - z[17];
    z[39]=n<T>(1,2)*z[39] + z[4];
    z[39]=z[39]*z[53];
    z[41]=z[6]*z[43];
    z[43]=z[73] - 4*z[3];
    z[43]=z[2]*z[43];
    z[50]= - z[2] - z[7];
    z[50]=z[5]*z[50];
    z[39]= - n<T>(3,2)*z[41] + n<T>(419,54)*z[50] + z[39] + z[45] + z[43];
    z[39]=z[6]*z[39];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[21]=z[6]*z[21];
    z[21]=z[21] + n<T>(1,2)*z[26] - z[27];
    z[21]=z[6]*z[21];
    z[23]= - z[33] + 3*z[34];
    z[23]=z[23]*z[49];
    z[26]=n<T>(1,2)*z[40] + z[38];
    z[26]=z[5]*z[26];
    z[33]= - z[16]*z[34];
    z[21]=z[21] + z[26] + z[33] + z[23];
    z[21]=z[15]*z[21];
    z[23]=z[8]*z[16];
    z[26]=z[2]*z[19];
    z[23]= - z[26] + z[23];
    z[23]=z[23]*z[31];
    z[21]=z[21] + z[23] + z[30] - z[27];
    z[23]=3*z[8];
    z[26]= - z[35] - z[8];
    z[26]=z[26]*z[23];
    z[27]=n<T>(257,27)*z[2];
    z[30]= - z[27] + 13*z[8];
    z[30]=z[30]*z[31];
    z[26]=z[26] + z[30];
    z[26]=z[10]*z[26];
    z[30]= - z[7] + z[12] + z[67];
    z[30]=z[8]*z[30];
    z[33]=z[2]*z[18];
    z[30]=z[37] - z[33] - z[30];
    z[33]=z[14] - z[17];
    z[27]=z[27] + 3*z[33] + 5*z[3];
    z[27]=z[10]*z[27];
    z[27]=z[27] - 3*z[30];
    z[27]=z[27]*z[42];
    z[21]=z[39] + z[27] + z[26] + 3*z[21];
    z[21]=z[15]*z[21];
    z[26]=z[14]*z[72];
    z[26]=z[26] + z[36] + n<T>(338,27) + n<T>(3,2)*z[68];
    z[26]=z[9]*z[26];
    z[27]= - z[17] - z[62] + z[47];
    z[30]=static_cast<T>(7)- n<T>(419,27)*z[74];
    z[30]=z[30]*z[31];
    z[31]=z[72]*z[44];
    z[26]=z[31] + z[30] - z[8] + z[26] - n<T>(419,27)*z[2] + 3*z[27] - z[3];
    z[26]=z[10]*z[26];
    z[27]= - n<T>(1,2)*z[65] + z[67];
    z[27]=z[13]*z[27];
    z[30]= - z[14] + z[60];
    z[30]=z[30]*z[70];
    z[27]=z[27] + z[30];
    z[23]=z[27]*z[23];
    z[27]= - z[14] - z[16];
    z[25]= - z[25] + z[2];
    z[25]=z[13]*z[25];
    z[25]=static_cast<T>(1)+ n<T>(5,2)*z[25];
    z[25]=z[9]*z[25];
    z[23]=z[23] + z[25] + n<T>(9,2)*z[65] + 3*z[27] + 5*z[2];
    z[23]=z[8]*z[23];
    z[25]=z[48] - z[12];
    z[25]=z[71] + z[4] + n<T>(1,2)*z[25] + z[28];
    z[25]=z[25]*z[29];
    z[27]=z[18] + z[14];
    z[27]=z[59] + n<T>(3,2)*z[27] + z[17];
    z[28]= - z[54]*z[72];
    z[29]= - static_cast<T>(676)+ n<T>(419,2)*z[46];
    z[29]=z[9]*z[29];
    z[27]= - z[8] + n<T>(1,27)*z[29] + z[28] + 3*z[27] + n<T>(338,27)*z[2];
    z[27]=z[7]*z[27];
    z[28]= - n<T>(9,2)*z[19] - z[51];
    z[28]=z[2]*z[28];
    z[20]=z[22] + z[21] + z[20] + z[32] + z[26] + z[27] + z[23] + z[25]
    + z[58] + z[28];
    z[20]=z[1]*z[20];
    z[20]=3*z[24] + z[20];

    r += z[20]*z[1];
 
    return r;
}

template double qqb_2lNLC_r45(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r45(const std::array<dd_real,31>&);
#endif
