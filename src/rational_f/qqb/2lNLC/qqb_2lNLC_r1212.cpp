#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1212(const std::array<T,31>& k) {
  T z[70];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[7];
    z[9]=k[15];
    z[10]=k[2];
    z[11]=k[5];
    z[12]=k[12];
    z[13]=k[11];
    z[14]=k[22];
    z[15]=k[19];
    z[16]=k[8];
    z[17]=npow(z[1],2);
    z[18]=z[17]*z[6];
    z[19]=z[18] + z[1];
    z[20]=z[11]*z[1];
    z[21]=z[20]*z[6];
    z[22]=z[2]*z[1];
    z[23]=z[22]*z[6];
    z[24]= - 2*z[23] + z[21] + z[19];
    z[25]=npow(z[2],2);
    z[24]=z[24]*z[25];
    z[26]=3*z[17];
    z[27]=npow(z[1],3);
    z[28]=z[27]*z[6];
    z[29]=z[26] + z[28];
    z[19]=z[23] - z[19];
    z[30]=3*z[2];
    z[19]=z[19]*z[30];
    z[19]=z[19] + z[29];
    z[19]=z[2]*z[19];
    z[19]= - z[27] + z[19];
    z[19]=z[5]*z[2]*z[19];
    z[31]=npow(z[1],4);
    z[32]=z[31]*z[6];
    z[33]=npow(z[11],2);
    z[34]=z[33]*z[32];
    z[35]=z[11]*z[6];
    z[36]= - z[31]*z[35];
    z[37]=z[2]*z[32];
    z[36]=z[36] + z[37];
    z[36]=z[36]*z[30];
    z[34]=z[34] + z[36];
    z[34]=z[34]*z[25];
    z[36]=z[3]*z[5];
    z[37]=z[36]*z[6];
    z[38]= - npow(z[22],5)*z[37];
    z[39]=npow(z[22],4);
    z[40]=z[5]*z[39];
    z[34]=z[38] + z[34] + z[40];
    z[34]=z[3]*z[34];
    z[38]=2*z[27];
    z[40]=npow(z[2],3);
    z[41]= - z[40]*z[38];
    z[34]=z[41] + z[34];
    z[41]=npow(z[3],3);
    z[34]=z[34]*z[41];
    z[19]=z[34] + z[19] - z[27] + 3*z[24];
    z[24]=2*z[4];
    z[19]=z[19]*z[24];
    z[34]=z[40]*z[5];
    z[42]=z[27]*z[34];
    z[37]=z[39]*z[37];
    z[37]=z[42] - z[37];
    z[39]=3*z[27];
    z[42]=z[39]*z[33];
    z[43]=z[6]*z[42];
    z[44]= - 11*z[11] + 13*z[2];
    z[44]=z[2]*z[28]*z[44];
    z[43]=z[43] + z[44];
    z[43]=z[2]*z[43];
    z[37]=z[43] + 5*z[37];
    z[37]=z[3]*z[37];
    z[43]=7*z[17];
    z[44]= - z[25]*z[43];
    z[37]=z[44] + z[37];
    z[44]=npow(z[3],2);
    z[37]=z[37]*z[44];
    z[45]= - 18*z[23] + 7*z[21] + 4*z[1] + 5*z[18];
    z[45]=z[2]*z[45];
    z[46]=z[23] - z[1];
    z[46]= - 7*z[18] + 11*z[46];
    z[46]=z[2]*z[46];
    z[46]=z[46] + z[43] + z[28];
    z[46]=z[2]*z[46];
    z[46]= - z[27] + z[46];
    z[46]=z[5]*z[46];
    z[19]=z[19] + z[37] + z[46] + z[26] + z[45];
    z[19]=z[19]*z[24];
    z[37]=5*z[2];
    z[45]= - 3*z[11] + z[37];
    z[46]=2*z[2];
    z[45]=z[46]*z[45];
    z[45]=z[33] + z[45];
    z[45]=z[18]*z[45];
    z[47]=4*z[17];
    z[48]=z[25]*z[5];
    z[49]=z[48]*z[47];
    z[40]=z[36]*z[40];
    z[50]=z[28]*z[40];
    z[45]= - 5*z[50] + z[49] + z[45];
    z[49]=2*z[44];
    z[45]=z[45]*z[49];
    z[50]=2*z[1];
    z[23]=3*z[23] - z[50] - z[18];
    z[23]=z[23]*z[37];
    z[37]=2*z[17];
    z[23]=z[37] + z[23];
    z[23]=z[5]*z[23];
    z[51]=z[6]*z[1];
    z[52]= - static_cast<T>(4)- 7*z[51];
    z[52]=z[52]*z[46];
    z[19]=z[19] + z[45] + z[23] + z[52] - z[18] + z[21];
    z[19]=z[4]*z[19];
    z[21]=z[2]*z[11];
    z[23]= - z[27]*z[21];
    z[23]= - z[42] + z[23];
    z[23]=z[2]*z[23]*z[41];
    z[42]=z[3]*z[1];
    z[45]=npow(z[42],4);
    z[52]=z[45]*z[33];
    z[53]= - z[25]*z[24]*z[52];
    z[54]= - z[20]*z[30];
    z[23]=z[53] + z[54] + z[23];
    z[23]=z[23]*z[24];
    z[24]= - z[11]*z[46];
    z[24]= - z[33] + z[24];
    z[24]=z[49]*z[17]*z[24];
    z[23]=z[23] + z[24] - z[22] - z[37] - z[20];
    z[23]=z[4]*z[23];
    z[21]=2*z[33] - z[21];
    z[21]=z[2]*z[21];
    z[24]= - npow(z[11],3);
    z[21]=z[24] + z[21];
    z[24]=npow(z[8],2);
    z[37]=z[24]*z[27];
    z[21]=z[3]*z[37]*z[21];
    z[49]=z[17]*z[8];
    z[53]=z[49]*z[33];
    z[54]=z[11]*z[8];
    z[55]= - z[2]*z[17]*z[54];
    z[21]=z[21] + z[53] + z[55];
    z[21]=z[21]*z[44];
    z[55]=3*z[24];
    z[56]=z[55]*z[20];
    z[57]=z[1] - n<T>(7,2)*z[49];
    z[57]=z[8]*z[57];
    z[57]= - z[56] - static_cast<T>(2)+ z[57];
    z[57]=z[11]*z[57];
    z[58]=z[8]*z[1];
    z[56]= - 2*z[58] + z[56];
    z[56]=z[2]*z[56];
    z[59]=3*z[49];
    z[21]=z[23] + z[21] + z[56] + z[57] - z[50] + z[59];
    z[21]=z[12]*z[21];
    z[23]=npow(z[6],2);
    z[56]=z[23]*z[17];
    z[57]=z[24]*z[17];
    z[60]=z[56] - z[57];
    z[61]=z[2]*z[60];
    z[62]=z[57]*z[11];
    z[61]=2*z[62] + z[61];
    z[61]=z[2]*z[61];
    z[63]=z[23]*z[27];
    z[40]= - z[40]*z[63];
    z[57]= - z[33]*z[57];
    z[64]= - z[49] - 3*z[18];
    z[64]=z[64]*z[48];
    z[40]=z[40] + z[64] + z[57] + z[61];
    z[40]=z[40]*z[44];
    z[46]=z[60]*z[46];
    z[37]=z[37] - z[63];
    z[25]=z[37]*z[25]*z[36];
    z[36]=2*z[5];
    z[37]=z[2]*z[49]*z[36];
    z[25]=z[25] + z[37] + z[62] + z[46];
    z[25]=z[25]*z[44];
    z[37]= - z[24] + z[23];
    z[37]=z[37]*z[22];
    z[46]=z[1] + n<T>(1,2)*z[49];
    z[46]=z[8]*z[46];
    z[37]=z[37] - n<T>(1,2)*z[56] + static_cast<T>(4)+ z[46];
    z[37]=z[5]*z[37];
    z[46]=z[7]*z[5];
    z[56]=z[46]*z[44];
    z[57]=z[49]*z[56];
    z[60]= - static_cast<T>(4)+ z[58];
    z[60]=z[8]*z[60];
    z[61]= - z[1]*z[23];
    z[25]= - 4*z[57] + z[25] + z[37] + z[60] + z[61];
    z[25]=z[7]*z[25];
    z[37]=9*z[5];
    z[37]=z[27]*z[37];
    z[57]=z[27]*z[8];
    z[37]= - 11*z[57] + z[37];
    z[37]=z[37]*z[41];
    z[60]=z[46]*z[8];
    z[61]=z[45]*z[60];
    z[62]=z[58]*z[5];
    z[37]= - 5*z[61] + 7*z[62] + z[37];
    z[37]=z[7]*z[37];
    z[61]=3*z[1];
    z[63]= - z[61] - z[49];
    z[63]=z[63]*z[36];
    z[64]=z[54]*z[3];
    z[65]=z[27]*z[64];
    z[65]=9*z[17] - 7*z[65];
    z[65]=z[65]*z[44];
    z[37]=z[37] + z[65] + 15*z[58] + z[63];
    z[37]=z[7]*z[37];
    z[63]=z[41]*z[57];
    z[65]= - z[33]*z[63];
    z[66]=z[20]*z[8];
    z[67]=2*z[49];
    z[68]= - z[1] - z[67];
    z[37]=z[37] + z[65] + 2*z[68] + 9*z[66];
    z[37]=z[7]*z[37];
    z[65]=z[58]*z[36];
    z[68]=z[31]*z[36];
    z[69]=z[31]*z[8];
    z[68]= - 3*z[69] + z[68];
    z[68]=z[68]*npow(z[3],4);
    z[42]= - npow(z[42],5)*z[60];
    z[42]=z[42] + z[65] + z[68];
    z[42]=z[7]*z[42];
    z[60]= - z[50] - z[49];
    z[60]=z[5]*z[60];
    z[31]= - z[31]*z[64];
    z[31]=z[27] + z[31];
    z[31]=z[31]*z[41];
    z[31]=z[42] + 3*z[31] + 6*z[58] + z[60];
    z[31]=z[7]*z[31];
    z[42]=2*z[66];
    z[60]=z[42] - z[1] - z[49];
    z[64]= - z[8]*z[52];
    z[31]=z[31] + 3*z[60] + z[64];
    z[31]=z[7]*z[31];
    z[42]= - z[59] + z[42];
    z[42]=z[11]*z[42];
    z[31]=z[42] + z[31];
    z[31]=z[7]*z[31];
    z[31]= - z[53] + z[31];
    z[42]=2*z[9];
    z[31]=z[31]*z[42];
    z[53]= - z[67] + z[66];
    z[53]=z[11]*z[53];
    z[31]=z[31] + z[53] + z[37];
    z[31]=z[31]*z[42];
    z[37]= - z[44]*z[54]*z[47];
    z[53]=z[5]*z[17];
    z[53]= - z[49] + z[53];
    z[53]=z[53]*z[44];
    z[46]=z[46]*z[63];
    z[46]= - 10*z[46] + 9*z[62] + 14*z[53];
    z[46]=z[7]*z[46];
    z[53]=static_cast<T>(4)+ 7*z[58];
    z[60]= - 6*z[1] - z[49];
    z[60]=z[5]*z[60];
    z[37]=z[46] + z[37] + 2*z[53] + z[60];
    z[37]=z[7]*z[37];
    z[31]=z[31] + z[37] - z[49] + 5*z[66];
    z[31]=z[9]*z[31];
    z[37]=z[69] + z[39];
    z[46]=z[37]*z[6];
    z[53]=z[46]*z[8];
    z[60]=n<T>(9,2)*z[17];
    z[62]=z[60] + 2*z[57];
    z[63]= - z[8]*z[62];
    z[63]=z[63] + n<T>(1,2)*z[53];
    z[63]=z[63]*z[35];
    z[37]= - z[12]*z[37];
    z[37]=z[37] + z[46];
    z[64]=z[54] - 1;
    z[37]=z[14]*z[64]*z[37];
    z[64]=z[39]*z[8];
    z[46]=z[64] - z[46];
    z[65]=n<T>(1,2)*z[6];
    z[46]=z[46]*z[65];
    z[54]=z[62]*z[54];
    z[54]= - n<T>(3,2)*z[57] + z[54];
    z[54]=z[12]*z[54];
    z[37]=n<T>(1,2)*z[37] + z[54] + z[46] + z[63];
    z[37]=z[14]*z[37];
    z[46]=z[7]*z[11];
    z[54]=z[46] + z[33];
    z[41]=z[41]*z[27];
    z[54]=z[41]*z[54];
    z[62]=5*z[20];
    z[54]= - z[62] + z[54];
    z[54]=z[7]*z[54];
    z[63]= - z[33]*z[50];
    z[52]=z[7]*z[52];
    z[52]=z[63] + z[52];
    z[52]=z[7]*z[52];
    z[33]=z[17]*z[33];
    z[33]=z[33] + z[52];
    z[33]=z[33]*z[42];
    z[47]=z[47] - z[20];
    z[47]=z[11]*z[47];
    z[33]=z[33] + z[47] + z[54];
    z[33]=z[33]*z[42];
    z[42]= - z[7]*z[1];
    z[33]=z[33] + z[42] + z[26] - z[62];
    z[33]=z[9]*z[33];
    z[42]=z[41]*z[46];
    z[20]=z[42] + z[20];
    z[20]=z[23]*z[20];
    z[42]= - z[11]*z[44]*z[18];
    z[20]=z[42] + 2*z[51] + z[20];
    z[20]=z[7]*z[20];
    z[42]= - z[1] + n<T>(1,2)*z[18];
    z[42]=z[42]*z[35];
    z[20]=z[33] + z[20] + 3*z[42] - z[1] + z[18];
    z[20]=z[13]*z[20];
    z[32]=z[32] - z[39];
    z[32]=z[13]*z[32];
    z[33]=z[69]*z[6];
    z[32]=z[32] - z[33] + z[64];
    z[33]=z[35] + 1;
    z[32]=z[15]*z[33]*z[32];
    z[24]= - z[24]*z[39];
    z[24]=z[32] + z[24] + z[53];
    z[26]= - z[26] - z[57];
    z[32]=z[38] + n<T>(1,2)*z[69];
    z[32]=z[6]*z[32];
    z[26]=n<T>(3,2)*z[26] + z[32];
    z[26]=z[35]*z[8]*z[26];
    z[32]=z[60] - 2*z[28];
    z[32]=z[32]*z[35];
    z[32]= - n<T>(3,2)*z[28] + z[32];
    z[32]=z[13]*z[32];
    z[24]=z[32] + z[26] + n<T>(1,2)*z[24];
    z[24]=z[15]*z[24];
    z[26]= - z[17] + z[22];
    z[26]=z[26]*z[30];
    z[26]=z[27] + z[26];
    z[26]=z[5]*z[26];
    z[27]= - z[45]*z[34];
    z[26]=z[26] + z[27];
    z[26]=z[4]*z[26];
    z[22]=2*z[22];
    z[27]= - z[17] + z[22];
    z[27]=z[5]*z[27];
    z[30]= - z[41]*z[48];
    z[26]=z[26] + z[27] + z[30];
    z[26]=z[26]*npow(z[4],2);
    z[17]= - z[17]*z[56];
    z[27]= - npow(z[7],2)*z[41]*z[36];
    z[30]= - z[9]*z[5]*npow(z[7],3)*z[45];
    z[27]=z[27] + z[30];
    z[27]=z[9]*z[27];
    z[17]=z[17] + z[27];
    z[17]=z[9]*z[17];
    z[17]=z[26] + z[17];
    z[17]=z[10]*z[17];
    z[18]=z[50] - n<T>(3,2)*z[18];
    z[18]=z[6]*z[18];
    z[22]=z[23]*z[22];
    z[18]=z[22] + z[18] - static_cast<T>(2)+ z[58];
    z[18]=z[2]*z[18];
    z[22]=z[6]*z[29];
    z[22]= - z[59] + z[22];
    z[18]=n<T>(1,2)*z[22] + z[18];
    z[18]=z[5]*z[18];
    z[22]=z[43] + z[57];
    z[22]=z[8]*z[22];
    z[23]=z[57]*z[6];
    z[22]=z[23] - z[22];
    z[22]=z[61] - n<T>(1,2)*z[22];
    z[22]=z[6]*z[22];
    z[26]=z[57] - z[28];
    z[26]=z[5]*z[26];
    z[23]=z[23] + z[26];
    z[23]=z[16]*z[23];
    z[26]=z[1] - z[49];
    z[26]=z[8]*z[26];
    z[27]= - z[1]*z[55];
    z[27]=z[27] - 2*z[6];
    z[27]=z[11]*z[27];
    z[28]=static_cast<T>(1)+ 3*z[58];
    z[28]=z[8]*z[28];
    z[28]=z[28] + 5*z[6];
    z[28]=z[2]*z[28];

    r += static_cast<T>(2)+ 4*z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + n<T>(1,2)*
      z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[31] + z[37] + 
      z[40];
 
    return r;
}

template double qqb_2lNLC_r1212(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1212(const std::array<dd_real,31>&);
#endif
