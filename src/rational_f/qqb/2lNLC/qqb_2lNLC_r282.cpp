#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r282(const std::array<T,31>& k) {
  T z[140];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[9];
    z[9]=k[15];
    z[10]=k[3];
    z[11]=k[4];
    z[12]=k[5];
    z[13]=k[23];
    z[14]=k[8];
    z[15]=k[18];
    z[16]=k[2];
    z[17]=k[25];
    z[18]=k[17];
    z[19]=k[24];
    z[20]=z[18] + n<T>(1,4)*z[3];
    z[20]=z[11]*z[20];
    z[20]=n<T>(1,2) + z[20];
    z[20]=z[7]*z[20];
    z[21]=2*z[3];
    z[22]=z[18] + z[21];
    z[22]=z[11]*z[22];
    z[22]=n<T>(3,2) + z[22];
    z[22]=z[4]*z[22];
    z[23]=z[11]*z[3];
    z[24]=n<T>(5,2) + z[23];
    z[24]=z[6]*z[24];
    z[20]=z[24] + z[20] + z[22];
    z[20]=z[5]*z[20];
    z[22]=z[14]*z[15];
    z[24]=3*z[22];
    z[25]=npow(z[15],2);
    z[26]=z[24] - z[25];
    z[27]=z[11]*z[14];
    z[26]=z[26]*z[27];
    z[28]=npow(z[8],2);
    z[29]=z[25] + 4*z[28];
    z[30]=3*z[18];
    z[31]= - static_cast<T>(2)+ z[23];
    z[31]=z[7]*z[31];
    z[31]=z[31] - z[30] + n<T>(17,4)*z[3];
    z[31]=z[7]*z[31];
    z[32]=3*z[3];
    z[33]= - z[17] - z[32];
    z[33]=z[11]*z[33];
    z[33]= - static_cast<T>(2)+ z[33];
    z[33]=z[4]*z[33];
    z[33]=z[33] - z[30] + n<T>(35,9)*z[3];
    z[33]=z[4]*z[33];
    z[34]=6*z[13];
    z[35]=z[34] + n<T>(7,3)*z[8];
    z[36]=z[23] + 1;
    z[37]=z[6]*z[36];
    z[35]=z[37] - n<T>(23,6)*z[7] + 2*z[35] + n<T>(23,2)*z[3];
    z[35]=z[6]*z[35];
    z[37]=npow(z[13],2);
    z[38]=8*z[37];
    z[39]=4*z[3];
    z[40]= - z[13]*z[39];
    z[41]=z[8]*z[16];
    z[42]=8*z[41];
    z[43]=static_cast<T>(5)- z[42];
    z[43]=z[8]*z[43];
    z[43]= - 15*z[15] + z[43];
    z[43]=z[14]*z[43];
    z[20]=z[20] + z[35] + z[33] + z[31] - z[26] + z[43] + z[40] - z[38]
    + z[29];
    z[20]=z[5]*z[20];
    z[31]=n<T>(1,2)*z[7];
    z[33]=9*z[9];
    z[35]=z[11]*z[33];
    z[35]=n<T>(1,2) + z[35];
    z[35]=z[9]*z[35];
    z[35]= - z[31] + z[35];
    z[40]=n<T>(1,2)*z[9];
    z[35]=z[35]*z[40];
    z[43]=n<T>(1,2)*z[2];
    z[44]=3*z[6];
    z[45]=z[44] - z[9];
    z[45]=z[45]*z[43];
    z[46]=z[5]*z[4];
    z[47]=2*z[4];
    z[48]= - n<T>(5,2)*z[7] - z[47];
    z[48]=z[4]*z[48];
    z[49]=11*z[14];
    z[50]=z[49] + n<T>(5,12)*z[7];
    z[50]=z[6]*z[50];
    z[35]=z[45] + z[35] + n<T>(128,9)*z[46] + z[48] + z[50];
    z[35]=z[2]*z[35];
    z[45]=npow(z[7],2);
    z[48]= - z[7]*z[47];
    z[48]= - 5*z[45] + z[48];
    z[48]=z[4]*z[48];
    z[50]=npow(z[14],2);
    z[51]=17*z[50] + 4*z[45];
    z[51]=z[6]*z[51];
    z[52]=npow(z[4],2);
    z[53]=2*z[52];
    z[54]=z[53] + n<T>(7,2)*z[46];
    z[54]=z[5]*z[54];
    z[55]=npow(z[9],2);
    z[56]=n<T>(3,2)*z[9];
    z[57]=z[18] + z[56];
    z[57]=z[57]*z[55];
    z[35]=z[35] + z[57] + z[54] + z[48] + z[51];
    z[35]=z[2]*z[35];
    z[48]=z[5] - z[7];
    z[48]=z[52]*z[48];
    z[51]=8*z[50];
    z[54]= - z[51] - z[45];
    z[54]=z[6]*z[54];
    z[57]= - z[14] + z[31];
    z[57]=z[6]*z[57];
    z[58]= - z[9]*z[31];
    z[57]=z[57] + z[58];
    z[57]=z[2]*z[57];
    z[58]=z[55]*z[7];
    z[48]=z[57] - n<T>(3,4)*z[58] + z[54] + z[48];
    z[48]=z[2]*z[48];
    z[54]=npow(z[14],3);
    z[57]=2*z[6];
    z[59]=z[54]*z[57];
    z[48]= - z[59] + z[48];
    z[48]=z[2]*z[48];
    z[60]=z[4]*z[7];
    z[61]=z[60]*z[50];
    z[62]=z[45]*z[50];
    z[61]=z[61] + z[62];
    z[61]=z[61]*z[4];
    z[63]=z[25] + 2*z[22];
    z[64]=z[50]*z[6];
    z[65]=z[64]*z[63];
    z[61]=z[61] - z[65];
    z[65]=z[4]*npow(z[5],2);
    z[66]=npow(z[9],3)*z[65]*z[12];
    z[67]=z[61] + n<T>(3,2)*z[66];
    z[68]=z[5]*z[6];
    z[69]=z[68]*z[28];
    z[70]=z[28] - z[37];
    z[71]=z[70]*npow(z[6],2);
    z[72]= - 12*z[71] + z[69];
    z[72]=z[5]*z[72];
    z[73]=z[52] + z[25];
    z[74]=n<T>(1,4)*z[7];
    z[75]= - z[74] - 5*z[4];
    z[75]=z[5]*z[75];
    z[75]=z[75] + z[73];
    z[75]=z[5]*z[75];
    z[76]=z[4] - z[15];
    z[77]=z[9]*z[5];
    z[78]=z[76]*z[77];
    z[75]=z[75] + 5*z[78];
    z[75]=z[75]*z[55];
    z[48]=z[48] + z[75] + z[72] - z[67];
    z[48]=z[12]*z[48];
    z[72]=6*z[4];
    z[75]=z[74] - z[72];
    z[75]=z[5]*z[75];
    z[75]=z[75] + z[25] + z[53];
    z[75]=z[5]*z[75];
    z[78]=z[6] - z[7];
    z[79]=n<T>(5,4)*z[8];
    z[80]= - n<T>(1,4)*z[14] + z[79] - z[18];
    z[80]= - z[80]*z[78];
    z[81]=7*z[15];
    z[82]=10*z[4] - z[81] + z[31];
    z[82]=z[5]*z[82];
    z[83]= - z[4]*z[18];
    z[80]=z[82] + z[83] + z[80];
    z[80]=z[9]*z[80];
    z[82]= - z[28] + z[50];
    z[82]=z[7]*z[82];
    z[83]=3*z[50];
    z[84]=z[28] + z[83];
    z[84]=z[6]*z[84];
    z[75]=z[80] + z[75] + z[82] + z[84];
    z[75]=z[9]*z[75];
    z[80]=2*z[37];
    z[82]= - z[80] + z[83];
    z[84]=z[13] - z[19];
    z[84]=5*z[84];
    z[85]=6*z[14];
    z[86]=z[84] + z[85];
    z[86]=z[7]*z[86];
    z[82]=2*z[82] + z[86];
    z[82]=z[7]*z[82];
    z[86]=2*z[14];
    z[87]=z[86]*z[7];
    z[88]=z[50] - z[87];
    z[88]=z[4]*z[88];
    z[89]= - z[8]*z[51];
    z[82]=z[88] + z[89] + z[82];
    z[82]=z[4]*z[82];
    z[88]=4*z[70];
    z[88]=z[88]*z[6];
    z[89]=z[14]*z[25];
    z[90]=z[19]*z[45];
    z[89]=z[88] + z[89] + z[90];
    z[89]=z[6]*z[89];
    z[90]=z[8] - z[13];
    z[91]=z[90]*z[6];
    z[92]= - 14*z[91] - n<T>(37,3)*z[28] - 3*z[45];
    z[92]=z[6]*z[92];
    z[93]=3*z[8];
    z[94]=z[93]*z[68];
    z[92]=z[92] + z[94];
    z[92]=z[5]*z[92];
    z[35]=z[48] + z[35] + z[75] + z[92] + z[82] + z[89];
    z[35]=z[12]*z[35];
    z[48]=3*z[7];
    z[75]=z[21] - z[48];
    z[75]=z[7]*z[75];
    z[82]=6*z[91];
    z[89]=4*z[37];
    z[75]= - z[82] - z[89] + z[75];
    z[75]=z[6]*z[75];
    z[92]=z[52]*z[21];
    z[94]=z[24] - z[29];
    z[94]=z[14]*z[94];
    z[95]=z[21]*z[46];
    z[96]=z[45]*z[3];
    z[75]=z[95] + z[75] - z[92] + z[94] - z[96];
    z[75]=z[5]*z[75];
    z[94]=313*z[3];
    z[95]=z[3]*z[10];
    z[97]= - static_cast<T>(295)- 313*z[95];
    z[97]=z[4]*z[97];
    z[98]= - static_cast<T>(617)- 313*z[23];
    z[98]=z[5]*z[98];
    z[97]=z[98] + z[94] + z[97];
    z[98]=z[4]*z[10];
    z[99]= - n<T>(1,2) - z[98];
    z[99]=z[6]*z[99];
    z[97]=z[99] + n<T>(1,18)*z[97];
    z[97]=z[2]*z[97];
    z[99]=n<T>(79,9)*z[3] + z[86];
    z[100]=3*z[23];
    z[101]= - static_cast<T>(5)- z[100];
    z[101]=z[5]*z[101];
    z[99]=n<T>(1,4)*z[101] + 2*z[99] + n<T>(181,18)*z[4];
    z[99]=z[5]*z[99];
    z[101]=n<T>(1,2)*z[23];
    z[102]=z[101] + 1;
    z[103]=3*z[9];
    z[104]=z[103]*z[11];
    z[105]= - z[102]*z[104];
    z[106]=n<T>(1,4)*z[23];
    z[105]= - z[106] + z[105];
    z[105]=z[105]*z[55];
    z[107]=z[31]*z[6];
    z[108]= - z[4] + n<T>(11,3)*z[3] - z[31];
    z[108]=z[4]*z[108];
    z[97]=z[97] + z[105] + z[99] + z[108] + z[107];
    z[97]=z[2]*z[97];
    z[99]=n<T>(1,2)*z[5];
    z[105]=z[4] + z[8];
    z[108]= - n<T>(1,2)*z[3] + z[105];
    z[108]=z[108]*z[99];
    z[109]=z[7] - z[3];
    z[110]= - z[109]*z[40];
    z[111]=z[31]*z[8];
    z[112]= - z[3] + n<T>(1,2)*z[4];
    z[112]=z[6]*z[112];
    z[108]=z[110] + z[108] + z[111] + z[112];
    z[108]=z[9]*z[108];
    z[110]=2*z[5];
    z[112]= - z[3] + z[47];
    z[112]=z[112]*z[110];
    z[113]= - n<T>(41,18)*z[3] - z[47];
    z[113]=z[4]*z[113];
    z[112]=z[112] - z[83] + z[113];
    z[112]=z[5]*z[112];
    z[113]=z[60]*z[6];
    z[97]=z[97] + z[108] + z[112] + z[92] + n<T>(1,4)*z[113];
    z[97]=z[2]*z[97];
    z[108]=z[21]*z[13];
    z[112]=z[52] + z[83] - z[25] - z[108];
    z[112]=z[6]*z[112];
    z[114]=2*z[13];
    z[115]=n<T>(9,4)*z[3] + z[114] - n<T>(1,2)*z[8];
    z[115]=z[6]*z[115];
    z[115]= - z[108] + z[115];
    z[115]=z[5]*z[115];
    z[116]=z[7]*z[8];
    z[117]=z[3] + z[8];
    z[118]=n<T>(3,2)*z[4] + n<T>(1,2)*z[14] - z[117];
    z[118]=z[6]*z[118];
    z[118]=z[116] + z[118];
    z[118]=z[118]*z[40];
    z[112]=z[118] + z[112] + z[115];
    z[112]=z[9]*z[112];
    z[115]=z[53] + n<T>(1,2)*z[46];
    z[115]=z[5]*z[115];
    z[118]= - z[31] - z[103];
    z[118]=z[118]*z[55];
    z[119]= - z[6]*z[7];
    z[60]=z[60] + z[119];
    z[60]=n<T>(1,2)*z[60] + n<T>(152,9)*z[46];
    z[60]=z[2]*z[60];
    z[60]=z[60] + z[115] + n<T>(1,2)*z[118];
    z[115]=npow(z[2],2);
    z[60]=z[60]*z[115];
    z[118]=4*z[5];
    z[119]= - z[71]*z[118];
    z[60]=z[119] + z[60];
    z[60]=z[12]*z[60];
    z[119]=z[117]*z[5];
    z[109]= - z[9]*z[109];
    z[109]=z[109] - z[116] - z[119];
    z[109]=z[9]*z[109];
    z[116]= - n<T>(313,9)*z[3] - z[7];
    z[116]=z[4]*z[116];
    z[120]=z[7] + z[4];
    z[120]=z[6]*z[120];
    z[94]= - z[94] + 617*z[4];
    z[94]=z[5]*z[94];
    z[94]=n<T>(1,9)*z[94] + z[116] + z[120];
    z[43]=z[94]*z[43];
    z[94]= - n<T>(19,6)*z[3] + z[4];
    z[94]=z[4]*z[94];
    z[116]=z[3]*z[99];
    z[94]=z[94] + z[116];
    z[94]=z[5]*z[94];
    z[107]= - z[4]*z[107];
    z[43]=z[43] + n<T>(1,4)*z[109] + z[107] + z[94];
    z[43]=z[2]*z[43];
    z[94]=z[46]*z[3];
    z[92]= - z[92] + n<T>(3,2)*z[94];
    z[92]=z[5]*z[92];
    z[107]=z[7]*z[93];
    z[107]=z[107] + z[119];
    z[109]=n<T>(1,4)*z[55];
    z[107]=z[107]*z[109];
    z[43]=z[43] + z[92] + z[107];
    z[43]=z[2]*z[43];
    z[92]=z[55]*z[111];
    z[94]= - z[113] + n<T>(313,9)*z[94];
    z[94]=z[2]*z[94];
    z[65]= - z[3]*z[65];
    z[65]=z[94] + z[65] + z[92];
    z[65]=z[1]*z[65]*z[115];
    z[92]= - z[117]*z[109];
    z[92]= - z[96] + z[92];
    z[68]=z[68]*z[92];
    z[43]=n<T>(1,2)*z[65] + z[43] + z[68];
    z[43]=z[1]*z[43];
    z[65]=z[50]*z[4];
    z[68]=4*z[8];
    z[92]= - z[68] + z[4];
    z[92]=z[92]*z[65];
    z[94]=z[6]*z[96];
    z[43]=z[43] + z[60] + z[97] + z[112] + z[75] + z[92] + z[94];
    z[43]=z[1]*z[43];
    z[60]=n<T>(15,4)*z[3];
    z[75]=z[25]*z[11];
    z[92]=z[4]*z[11];
    z[94]=n<T>(3,4) - z[92];
    z[94]=z[4]*z[94];
    z[94]=z[94] + z[75] - n<T>(13,4)*z[14] + z[60] + n<T>(25,4)*z[8] - z[18] + 
    z[15];
    z[94]=z[6]*z[94];
    z[96]=n<T>(1,4)*z[92];
    z[97]= - z[96] - static_cast<T>(1)- z[106];
    z[97]=z[6]*z[97];
    z[106]=z[5]*z[16];
    z[107]=4*z[14];
    z[109]= - z[107]*z[106];
    z[97]=z[109] + n<T>(3,4)*z[4] + z[97];
    z[97]=z[9]*z[97];
    z[109]= - n<T>(3,4)*z[14] - z[18] + z[8];
    z[109]=z[109]*z[48];
    z[30]= - z[30] + z[105];
    z[30]=z[4]*z[30];
    z[105]=5*z[14];
    z[111]=2*z[15];
    z[112]=n<T>(13,4)*z[6] - n<T>(25,18)*z[4] + 4*z[7] - z[111] - z[105];
    z[112]=z[5]*z[112];
    z[30]=z[97] + z[112] + z[94] + z[30] + z[109] - z[25] + 4*z[50];
    z[30]=z[9]*z[30];
    z[94]=z[89] + z[25];
    z[97]= - z[15] - z[49];
    z[97]=z[14]*z[97];
    z[109]= - z[19] + n<T>(11,4)*z[3];
    z[109]=z[7]*z[109];
    z[112]=z[74] - z[4];
    z[112]=z[4]*z[112];
    z[82]=z[82] + z[112] + z[109] + z[97] + z[108] + z[94];
    z[82]=z[6]*z[82];
    z[97]=n<T>(313,2)*z[95];
    z[109]=n<T>(313,2)*z[23] + static_cast<T>(152)+ z[97];
    z[112]=z[6]*z[10];
    z[113]= - z[11]*z[40];
    z[109]=z[113] + n<T>(1,9)*z[109] + z[112];
    z[109]=z[2]*z[109];
    z[113]=2*z[10];
    z[115]=npow(z[10],2);
    z[116]=z[115]*z[3];
    z[117]=z[113] + z[116];
    z[117]=z[117]*z[4];
    z[119]= - n<T>(301,9) + z[95];
    z[119]=n<T>(1,2)*z[119] - z[117];
    z[119]=z[4]*z[119];
    z[120]=npow(z[11],2);
    z[121]=z[120]*z[103];
    z[122]=z[102]*z[121];
    z[36]=z[11]*z[36];
    z[36]=n<T>(5,4)*z[36] + z[122];
    z[36]=z[36]*z[103];
    z[122]=static_cast<T>(3)+ n<T>(7,2)*z[23];
    z[36]=n<T>(1,2)*z[122] + z[36];
    z[36]=z[9]*z[36];
    z[123]=z[115]*z[4];
    z[124]=4*z[10] + z[123];
    z[124]=z[4]*z[124];
    z[124]=static_cast<T>(4)+ z[124];
    z[124]=z[6]*z[124];
    z[125]=n<T>(31,6)*z[3] - z[107];
    z[125]=z[11]*z[125];
    z[125]= - n<T>(569,36) + z[125];
    z[125]=z[5]*z[125];
    z[36]=z[109] + z[36] + z[125] + z[124] + z[119] - n<T>(325,18)*z[3] - 
    z[107];
    z[36]=z[2]*z[36];
    z[109]=n<T>(3,2)*z[23];
    z[119]= - static_cast<T>(5)+ z[109];
    z[99]=z[119]*z[99];
    z[119]=z[50]*z[11];
    z[124]=3*z[119];
    z[125]=2*z[8];
    z[60]=z[99] - n<T>(601,18)*z[4] + z[124] + 13*z[14] + z[125] - z[60];
    z[60]=z[5]*z[60];
    z[99]=z[109] + 1;
    z[126]= - z[99]*z[104];
    z[127]=static_cast<T>(7)- z[109];
    z[126]=n<T>(1,2)*z[127] + z[126];
    z[126]=z[9]*z[126];
    z[127]=5*z[5];
    z[126]=z[126] - z[127] - n<T>(19,4)*z[6] - n<T>(3,4)*z[7] + n<T>(973,36)*z[3] - 
    z[18] - z[93];
    z[126]=z[9]*z[126];
    z[128]=2*z[7];
    z[129]=3*z[4];
    z[130]= - z[129] - n<T>(133,18)*z[3] - z[128];
    z[130]=z[4]*z[130];
    z[131]= - z[3] - z[105];
    z[132]= - n<T>(5,2) + z[98];
    z[132]=z[4]*z[132];
    z[131]=z[132] + 2*z[131] - n<T>(59,12)*z[7];
    z[131]=z[6]*z[131];
    z[36]=z[36] + z[126] + z[60] + z[131] + z[83] + z[130];
    z[36]=z[2]*z[36];
    z[60]=z[49] + z[68] - z[3];
    z[60]=z[14]*z[60];
    z[126]=3*z[14];
    z[130]=z[126] + z[119];
    z[131]=z[17] + z[39] - z[130];
    z[131]=z[4]*z[131];
    z[84]=z[84] + n<T>(53,4)*z[14];
    z[84]=z[7]*z[84];
    z[60]=z[131] + z[84] - z[89] + z[60];
    z[60]=z[4]*z[60];
    z[84]= - z[68] + 3*z[15];
    z[131]= - z[14]*z[84];
    z[29]=z[131] + z[29];
    z[29]=z[14]*z[29];
    z[79]= - z[79] + z[7];
    z[79]=z[7]*z[3]*z[79];
    z[20]=z[43] + z[35] + z[36] + z[30] + z[20] + z[82] + z[60] + z[29]
    + z[79];
    z[20]=z[1]*z[20];
    z[29]=2*z[50];
    z[30]=z[8] - z[14];
    z[30]=z[30]*z[29];
    z[35]= - z[7]*z[52];
    z[30]=z[30] + z[35];
    z[35]=43*z[50] + z[45];
    z[35]=z[6]*z[35];
    z[36]=z[52]*z[110];
    z[43]=21*z[14] + z[7];
    z[43]=z[6]*z[43];
    z[43]= - z[51] + z[43];
    z[60]= - z[86] + n<T>(9,2)*z[6];
    z[60]=z[2]*z[60];
    z[43]=2*z[43] + z[60];
    z[43]=z[2]*z[43];
    z[30]=z[43] + z[58] + z[36] + 2*z[30] + z[35];
    z[30]=z[2]*z[30];
    z[35]=z[6]*z[14];
    z[36]=z[35]*z[2];
    z[36]=z[36] + 8*z[64];
    z[36]=z[36]*z[2];
    z[36]=z[36] + z[59];
    z[36]=z[36]*z[2];
    z[43]= - 6*z[71] + z[69];
    z[43]=z[5]*z[43];
    z[43]=z[36] - z[43] + z[61];
    z[58]= - z[73] + 5*z[46];
    z[59]= - z[58]*z[110];
    z[60]=n<T>(3,2)*z[5];
    z[61]=z[60] - 10*z[15] + 13*z[4];
    z[61]=z[61]*z[77];
    z[59]=z[59] + z[61];
    z[59]=z[59]*z[55];
    z[43]= - 3*z[66] + z[59] - 2*z[43];
    z[43]=z[12]*z[43];
    z[53]= - 13*z[46] - z[25] + z[53];
    z[53]=z[5]*z[53];
    z[59]=5*z[15];
    z[61]=n<T>(21,4)*z[5] + n<T>(43,2)*z[4] - z[59] - z[7];
    z[61]=z[5]*z[61];
    z[66]=z[9]*z[76];
    z[61]= - 10*z[66] - 2*z[73] + z[61];
    z[61]=z[9]*z[61];
    z[53]=z[53] + z[61];
    z[53]=z[9]*z[53];
    z[61]= - z[50]*z[125];
    z[66]=7*z[50];
    z[76]=z[66] - z[89];
    z[79]=5*z[13];
    z[82]=z[79] + 7*z[14];
    z[131]=z[7]*z[82];
    z[131]=z[131] + z[76];
    z[131]=z[7]*z[131];
    z[61]=z[61] + z[131];
    z[87]= - z[50] - z[87];
    z[87]=z[4]*z[87];
    z[61]=2*z[61] + z[87];
    z[61]=z[4]*z[61];
    z[87]=5*z[22];
    z[131]= - z[25] - z[87];
    z[131]=z[14]*z[131];
    z[70]=z[6]*z[70];
    z[70]=z[131] + 8*z[70];
    z[70]=z[6]*z[70];
    z[131]=3*z[37];
    z[132]=z[131] - n<T>(20,3)*z[28];
    z[91]= - 10*z[91] + 4*z[132] - z[45];
    z[91]=z[6]*z[91];
    z[132]=z[93]*z[6];
    z[132]=z[132] + z[28];
    z[133]=z[132]*z[110];
    z[91]=z[91] + z[133];
    z[91]=z[5]*z[91];
    z[133]=z[63]*z[29];
    z[30]=z[43] + z[30] + z[53] + z[91] + z[70] + z[61] + z[133] + z[62]
   ;
    z[30]=z[12]*z[30];
    z[43]=n<T>(9,4)*z[6];
    z[53]=z[125] + z[18];
    z[61]= - n<T>(5,4) - z[104];
    z[61]=z[9]*z[61];
    z[61]=z[61] - z[5] - z[43] + z[74] + z[53];
    z[61]=z[9]*z[61];
    z[70]=2*z[11];
    z[91]=z[9]*z[120];
    z[91]= - z[70] - n<T>(9,2)*z[91];
    z[91]=z[9]*z[91];
    z[91]= - n<T>(1,2) + z[91];
    z[91]=z[9]*z[91];
    z[133]=15*z[14];
    z[134]=z[133] + z[7];
    z[135]=n<T>(3,2) - z[112];
    z[135]=z[2]*z[135];
    z[91]=3*z[135] + z[91] + 2*z[134] - n<T>(119,6)*z[6];
    z[91]=z[2]*z[91];
    z[134]= - n<T>(125,2)*z[14] + n<T>(4,3)*z[7];
    z[134]=z[6]*z[134];
    z[61]=z[91] + z[61] - n<T>(83,6)*z[46] + z[134] - 4*z[52] + 13*z[50] + 2
   *z[45];
    z[61]=z[2]*z[61];
    z[91]=z[37]*z[10];
    z[134]=z[8]*z[10];
    z[135]=static_cast<T>(2)+ z[134];
    z[135]=z[8]*z[135];
    z[135]=z[135] - z[114] - z[91];
    z[135]=z[6]*z[135];
    z[136]=3*z[25];
    z[137]=z[136] + z[89];
    z[138]= - z[7]*z[19];
    z[135]=4*z[135] + z[138] - 23*z[50] + n<T>(34,3)*z[28] - z[137];
    z[135]=z[6]*z[135];
    z[138]=z[7] + z[8];
    z[139]=16*z[13];
    z[138]=z[6] + z[139] - n<T>(29,6)*z[138];
    z[138]=z[6]*z[138];
    z[129]=5*z[6] + z[68] - z[129];
    z[129]=z[5]*z[129];
    z[45]=z[129] + z[138] - z[45] - n<T>(59,3)*z[28] - z[137];
    z[45]=z[5]*z[45];
    z[43]=z[127] + z[43] + n<T>(157,6)*z[4] + z[81] + z[74];
    z[43]=z[5]*z[43];
    z[74]= - z[18] + z[93] + z[14];
    z[74]=z[74]*z[78];
    z[78]= - z[47] - z[53];
    z[78]=z[4]*z[78];
    z[93]=4*z[15];
    z[127]= - z[56] - 7*z[5] - z[93] - n<T>(29,4)*z[4];
    z[127]=z[9]*z[127];
    z[129]=2*z[25];
    z[43]=z[127] + z[43] + z[78] + z[129] + z[74];
    z[43]=z[9]*z[43];
    z[74]= - z[86] - z[119];
    z[74]=z[74]*z[47];
    z[78]=z[68] + 23*z[14];
    z[78]=z[14]*z[78];
    z[127]=3*z[13];
    z[137]= - z[19] - z[127];
    z[137]= - z[7] + 5*z[137] + n<T>(21,2)*z[14];
    z[137]=z[7]*z[137];
    z[74]=z[74] + z[137] + z[89] + z[78];
    z[74]=z[4]*z[74];
    z[78]= - z[79] - z[85];
    z[78]=z[7]*z[78];
    z[76]=z[78] + z[28] - z[76];
    z[76]=z[7]*z[76];
    z[24]= - z[25] - z[24];
    z[24]=z[24]*z[86];
    z[24]=z[30] + z[61] + z[43] + z[45] + z[135] + z[74] + z[24] + z[76]
   ;
    z[24]=z[12]*z[24];
    z[30]=z[87] + z[108] + z[89] - z[25];
    z[30]=z[11]*z[30];
    z[43]=npow(z[16],2);
    z[45]=z[43]*z[68];
    z[61]=5*z[16] - z[45];
    z[61]=z[8]*z[61];
    z[61]= - static_cast<T>(4)+ z[61];
    z[61]=z[14]*z[61];
    z[68]=z[7]*z[11];
    z[74]= - n<T>(13,4)*z[3] - z[18] - z[79];
    z[74]=z[11]*z[74];
    z[74]=z[68] - n<T>(67,12) + z[74];
    z[74]=z[7]*z[74];
    z[76]=n<T>(71,9)*z[3] - z[17] + z[18];
    z[76]=z[11]*z[76];
    z[76]= - 2*z[92] + n<T>(3,2) + z[76];
    z[76]=z[4]*z[76];
    z[78]=z[6]*z[11];
    z[85]=z[78] - n<T>(5,2) - n<T>(17,3)*z[23];
    z[85]=z[6]*z[85];
    z[86]=z[92] + z[78] + 2;
    z[108]=z[86]*z[110];
    z[135]=8*z[13];
    z[42]= - n<T>(5,2) + z[42];
    z[42]=z[8]*z[42];
    z[30]=z[108] + z[85] + z[76] + z[74] + z[30] + z[61] - n<T>(85,9)*z[3]
    + z[42] + 16*z[15] + z[135] - z[17] + 4*z[18];
    z[30]=z[5]*z[30];
    z[42]=2*z[91];
    z[61]= - z[13] + z[42];
    z[61]=z[61]*z[113];
    z[61]= - n<T>(3,4)*z[68] - 3*z[134] + n<T>(1,2) + z[61];
    z[61]=z[4]*z[61];
    z[74]=z[115]*z[13];
    z[76]=z[115]*z[8];
    z[74]=z[74] - z[76];
    z[85]=z[74]*z[47];
    z[108]= - z[10]*z[34];
    z[85]=z[85] + 6*z[134] - static_cast<T>(1)+ z[108];
    z[85]=z[6]*z[85];
    z[108]=7*z[13];
    z[113]=z[42] - z[108] + z[15];
    z[137]=z[3] + z[14];
    z[137]=z[14]*z[137];
    z[137]= - z[25] + z[137];
    z[137]=z[11]*z[137];
    z[101]=static_cast<T>(5)+ z[101];
    z[31]=z[101]*z[31];
    z[101]=n<T>(39,2)*z[14];
    z[135]= - z[10]*z[135];
    z[135]= - n<T>(27,2) + z[135];
    z[135]=z[3]*z[135];
    z[31]=z[85] + z[61] + z[31] + z[137] + z[101] + z[135] + 2*z[113] - 
   n<T>(29,3)*z[8];
    z[31]=z[6]*z[31];
    z[61]=z[3]*z[43]*z[107];
    z[85]= - z[14]*z[43];
    z[85]=z[16] + z[85];
    z[85]=z[85]*z[118];
    z[109]=z[109] - 1;
    z[113]= - z[109]*z[104];
    z[61]=z[113] + z[85] - z[96] + n<T>(23,4)*z[23] + n<T>(35,4) + z[61];
    z[61]=z[9]*z[61];
    z[85]= - n<T>(5,2)*z[92] + z[122];
    z[85]=z[6]*z[85];
    z[113]=z[3]*z[16];
    z[122]=z[14]*z[113];
    z[135]=n<T>(13,4) - z[92];
    z[135]=z[4]*z[135];
    z[105]= - z[16]*z[105];
    z[105]= - n<T>(233,9) + z[105];
    z[105]=z[5]*z[105];
    z[48]=z[61] + z[105] + n<T>(1,2)*z[85] + z[135] - z[48] + z[75] + z[122]
    + z[17] + n<T>(551,18)*z[3];
    z[48]=z[9]*z[48];
    z[61]=npow(z[11],3);
    z[33]= - z[102]*z[61]*z[33];
    z[85]= - static_cast<T>(9)- n<T>(31,4)*z[23];
    z[85]=z[85]*z[120];
    z[33]=z[85] + z[33];
    z[33]=z[9]*z[33];
    z[85]= - n<T>(3,2) - 2*z[23];
    z[85]=z[11]*z[85];
    z[33]=3*z[85] + z[33];
    z[33]=z[9]*z[33];
    z[85]=n<T>(1,9)*z[11];
    z[97]= - static_cast<T>(152)+ z[97];
    z[97]=z[97]*z[85];
    z[105]=z[120]*z[40];
    z[122]=3*z[10];
    z[97]=z[105] - z[122] + z[97];
    z[97]=z[2]*z[97];
    z[105]=11*z[10];
    z[123]= - z[105] + 4*z[123];
    z[123]=z[6]*z[123];
    z[135]= - static_cast<T>(1)- n<T>(32,9)*z[95];
    z[137]= - n<T>(37,6)*z[3] + z[107];
    z[137]=z[11]*z[137];
    z[33]=z[97] + z[33] + z[123] + z[117] + 7*z[135] + z[137];
    z[33]=z[2]*z[33];
    z[97]=n<T>(23,9) - 5*z[95];
    z[97]=z[3]*z[97];
    z[97]=z[107] + z[8] + z[97];
    z[117]=n<T>(61,2) - 62*z[95];
    z[117]=n<T>(1,9)*z[117] - 4*z[98];
    z[117]=z[4]*z[117];
    z[123]=4*z[95] + n<T>(56,3) + z[134];
    z[123]=2*z[123] + n<T>(7,2)*z[98];
    z[123]=z[6]*z[123];
    z[100]=static_cast<T>(2)+ z[100];
    z[100]=z[100]*z[121];
    z[135]=9*z[23];
    z[137]= - n<T>(5,2) + z[135];
    z[137]=z[11]*z[137];
    z[100]=z[137] + z[100];
    z[100]=z[9]*z[100];
    z[137]= - static_cast<T>(325)- n<T>(1309,4)*z[23];
    z[100]=n<T>(1,9)*z[137] + z[100];
    z[100]=z[9]*z[100];
    z[137]=n<T>(19,12)*z[3] - z[14];
    z[137]=z[11]*z[137];
    z[137]= - n<T>(9,4) + 5*z[137];
    z[137]=z[5]*z[137];
    z[33]=z[33] + z[100] + z[137] + z[123] + z[117] + n<T>(22,3)*z[7] + 2*
    z[97] - z[124];
    z[33]=z[2]*z[33];
    z[97]=z[10]*z[15];
    z[100]=z[97] + 2;
    z[117]= - z[23] + z[95] - z[100];
    z[117]=z[7]*z[117];
    z[123]=z[125]*z[115];
    z[124]= - n<T>(3,2)*z[10] - z[123];
    z[124]=z[8]*z[124];
    z[124]= - static_cast<T>(2)+ z[124];
    z[32]=z[124]*z[32];
    z[124]=6*z[19];
    z[137]=z[124] + z[18];
    z[32]=z[117] - 12*z[14] + z[32] - n<T>(7,2)*z[8] + 8*z[15] + 2*z[137] - 
    z[79];
    z[32]=z[7]*z[32];
    z[89]=z[89]*z[10];
    z[53]=z[89] - 4*z[13] - z[17] - z[53];
    z[117]=z[3] - z[14];
    z[117]=z[117]*z[27];
    z[137]= - n<T>(151,9) + z[134];
    z[137]=z[3]*z[137];
    z[53]= - z[47] - n<T>(17,4)*z[7] + z[117] - n<T>(41,2)*z[14] + 2*z[53] + 
    z[137];
    z[53]=z[4]*z[53];
    z[117]= - z[10] + n<T>(14,3)*z[16];
    z[137]= - z[117]*z[125];
    z[137]= - n<T>(49,6) + z[137];
    z[137]=z[8]*z[137];
    z[138]= - 3*z[19] + z[13];
    z[95]=z[13]*z[95];
    z[95]=10*z[95] + 2*z[138] + z[137];
    z[95]=z[3]*z[95];
    z[137]=3*z[94];
    z[138]= - z[16] + z[45];
    z[138]=z[8]*z[138];
    z[138]=static_cast<T>(2)+ z[138];
    z[138]=z[3]*z[138];
    z[84]=2*z[84] + z[138];
    z[84]=z[14]*z[84];
    z[20]=z[20] + z[24] + z[33] + z[48] + z[30] + z[31] + z[53] + z[32]
    + z[26] + z[84] + z[95] + z[137] + n<T>(28,3)*z[28];
    z[20]=z[1]*z[20];
    z[24]= - 4*z[71] + z[69];
    z[24]=z[5]*z[24];
    z[26]= - z[5]*z[58];
    z[30]=z[60] - z[59] + 8*z[4];
    z[30]=z[30]*z[77];
    z[26]=z[26] + z[30];
    z[26]=z[26]*z[55];
    z[24]= - z[36] + z[26] + z[24] - z[67];
    z[24]=z[12]*z[24];
    z[26]= - z[129] - z[87];
    z[26]=z[14]*z[26];
    z[26]=z[26] + z[88];
    z[26]=z[6]*z[26];
    z[30]= - z[90]*z[57];
    z[30]=z[30] + z[38] - n<T>(43,3)*z[28];
    z[30]=z[6]*z[30];
    z[31]=z[5]*z[132];
    z[30]=z[30] + z[31];
    z[30]=z[5]*z[30];
    z[31]= - static_cast<T>(1)- z[106];
    z[31]=z[31]*z[60];
    z[31]=z[31] + z[59] - n<T>(13,2)*z[4];
    z[31]=z[9]*z[31];
    z[32]=n<T>(7,2)*z[5] + z[111] + 11*z[4];
    z[32]=z[5]*z[32];
    z[31]=z[31] + z[32] - z[73];
    z[31]=z[9]*z[31];
    z[32]= - z[25] - 3*z[46];
    z[32]=z[32]*z[110];
    z[31]=z[32] + z[31];
    z[31]=z[9]*z[31];
    z[32]= - z[14] + z[44];
    z[32]=z[2]*z[32];
    z[32]=z[32] - z[51] + 31*z[35];
    z[32]=z[2]*z[32];
    z[33]= - z[54] + 13*z[64];
    z[32]=2*z[33] + z[32];
    z[32]=z[2]*z[32];
    z[33]=z[63]*z[50];
    z[29]=z[29] - z[37];
    z[29]=4*z[29];
    z[35]=z[79] + 8*z[14];
    z[36]=z[7]*z[35];
    z[36]=z[29] + z[36];
    z[36]=z[7]*z[36];
    z[36]=z[36] - z[65];
    z[36]=z[4]*z[36];
    z[24]=z[24] + z[32] + z[31] + z[30] + z[26] + z[36] + z[33] + z[62];
    z[24]=z[12]*z[24];
    z[26]=z[15] - z[126];
    z[26]=z[26]*z[107];
    z[30]=z[42] + z[13];
    z[31]=z[10]*z[125];
    z[31]=static_cast<T>(1)+ z[31];
    z[31]=z[8]*z[31];
    z[31]=z[31] - z[30];
    z[31]=z[31]*z[57];
    z[26]=z[31] + z[26] + n<T>(40,3)*z[28] - z[38] + z[25];
    z[26]=z[6]*z[26];
    z[31]=z[11]*z[15];
    z[32]= - z[31] + z[92];
    z[32]=5*z[32] + 6*z[106];
    z[32]=z[9]*z[32];
    z[33]= - static_cast<T>(1)+ z[92];
    z[33]=z[4]*z[33];
    z[36]= - static_cast<T>(5)- 2*z[106];
    z[36]=z[5]*z[36];
    z[32]=z[32] + z[36] + z[33] - z[81] + z[75];
    z[32]=z[9]*z[32];
    z[33]=3*z[5] + 9*z[15] + n<T>(23,18)*z[4];
    z[33]=z[5]*z[33];
    z[32]=z[32] + z[33] + z[136] + z[52];
    z[32]=z[9]*z[32];
    z[33]=z[6] - z[4];
    z[36]=z[41] + 2;
    z[36]=z[8]*z[36];
    z[33]=z[36] + n<T>(5,2)*z[33];
    z[33]=z[5]*z[33];
    z[36]=z[7] + z[114] - n<T>(35,4)*z[8];
    z[36]=z[6]*z[36];
    z[33]=z[33] + z[36] - n<T>(31,3)*z[28] + z[94];
    z[33]=z[5]*z[33];
    z[36]= - z[63]*z[130];
    z[37]=z[119] - z[82];
    z[37]=z[7]*z[37];
    z[29]= - z[29] + z[37];
    z[29]=z[7]*z[29];
    z[37]=z[80] + z[83];
    z[38]= - z[128] - 20*z[13] - n<T>(19,4)*z[14];
    z[38]=z[7]*z[38];
    z[42]= - z[14] - z[119];
    z[42]=z[4]*z[42];
    z[37]=z[42] + 4*z[37] + z[38];
    z[37]=z[4]*z[37];
    z[38]=4*z[119];
    z[42]=z[133] + z[38];
    z[44]= - 3*z[112] + static_cast<T>(3)+ z[27];
    z[44]=z[2]*z[44];
    z[42]=z[44] + 2*z[42] - n<T>(125,3)*z[6];
    z[42]=z[2]*z[42];
    z[44]=z[11]*z[54];
    z[44]=z[66] + z[44];
    z[46]= - n<T>(239,4)*z[14] - z[7];
    z[46]=z[6]*z[46];
    z[42]=z[42] + 2*z[44] + z[46];
    z[42]=z[2]*z[42];
    z[24]=z[24] + z[42] + z[32] + z[33] + z[26] + z[37] + z[29] + z[36];
    z[24]=z[12]*z[24];
    z[26]=z[80] - 5*z[50];
    z[26]=z[26]*z[70];
    z[29]=n<T>(11,4) + z[134];
    z[29]=z[8]*z[29];
    z[32]= - z[11]*z[35];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[7]*z[32];
    z[26]=z[32] + z[26] + z[107] + z[139] + z[29];
    z[26]=z[7]*z[26];
    z[29]=z[101] + z[119];
    z[32]=z[10] + z[123];
    z[32]=z[8]*z[32];
    z[30]= - z[10]*z[30];
    z[30]=z[30] + z[32];
    z[30]=z[30]*z[57];
    z[32]=static_cast<T>(3)+ n<T>(34,3)*z[134];
    z[32]=z[8]*z[32];
    z[30]=z[30] + z[32] - 8*z[91] - z[114] - z[15] + z[29];
    z[30]=z[6]*z[30];
    z[32]=z[61]*z[56];
    z[32]=z[120] + z[32];
    z[32]=z[9]*z[32];
    z[32]=n<T>(1,2)*z[11] + z[32];
    z[32]=z[9]*z[32];
    z[33]=z[115]*z[6];
    z[35]= - z[10] - z[11];
    z[35]=3*z[35] + z[33];
    z[35]=z[2]*z[35];
    z[32]=z[35] + z[32] + n<T>(115,6)*z[112] - n<T>(98,3) - 19*z[27];
    z[32]=z[2]*z[32];
    z[35]=n<T>(5,2)*z[11] + z[121];
    z[35]=z[9]*z[35];
    z[35]=n<T>(413,18) + z[35];
    z[35]=z[35]*z[40];
    z[32]=z[32] + z[35] + 55*z[6] - z[72] - z[7] - z[49] + z[38];
    z[32]=z[2]*z[32];
    z[35]=z[127] - z[91];
    z[36]= - n<T>(49,4) - z[68];
    z[36]=z[7]*z[36];
    z[29]= - z[47] + z[36] + 4*z[35] - z[29];
    z[29]=z[4]*z[29];
    z[35]= - 3*z[16] + z[11];
    z[35]=z[35]*z[56];
    z[31]=z[35] + n<T>(21,4)*z[106] - n<T>(9,4)*z[92] + static_cast<T>(3)+ 11*z[31];
    z[31]=z[9]*z[31];
    z[35]= - n<T>(55,36) - z[106];
    z[35]=z[5]*z[35];
    z[31]=z[31] + z[35] - n<T>(37,4)*z[4] - z[111] - 3*z[75];
    z[31]=z[9]*z[31];
    z[35]=n<T>(1,3)*z[8];
    z[36]= - n<T>(29,4) - 34*z[41];
    z[36]=z[36]*z[35];
    z[37]=z[41] + z[86];
    z[37]=z[5]*z[37];
    z[38]=6*z[15];
    z[36]=z[37] - 18*z[6] - n<T>(259,18)*z[4] + z[7] + z[36] + z[13] - z[38]
   ;
    z[36]=z[5]*z[36];
    z[28]= - z[131] + n<T>(7,3)*z[28];
    z[27]=z[63]*z[27];
    z[24]=z[24] + z[32] + z[31] + z[36] + z[30] + z[29] + z[26] + 3*
    z[27] + 4*z[28] + 7*z[22];
    z[24]=z[12]*z[24];
    z[26]=static_cast<T>(5)+ 4*z[23];
    z[26]=z[26]*z[61];
    z[27]=z[102]*npow(z[11],4)*z[103];
    z[26]=z[26] + z[27];
    z[26]=z[9]*z[26];
    z[27]=z[99]*z[120];
    z[26]=3*z[27] + z[26];
    z[26]=z[9]*z[26];
    z[27]=z[21]*z[115];
    z[28]=n<T>(5,3)*z[10] - z[27];
    z[28]=z[3]*z[28];
    z[28]=z[23] + n<T>(19,3) + z[28];
    z[28]=z[28]*z[70];
    z[29]=z[122] - z[116];
    z[29]=z[11]*z[29];
    z[29]=z[115] + z[29];
    z[29]=z[2]*z[29];
    z[26]=z[29] + z[26] - z[33] + z[28] + n<T>(19,6)*z[10] + 7*z[116];
    z[26]=z[2]*z[26];
    z[28]= - z[99]*z[61]*z[103];
    z[29]= - n<T>(7,4) - z[135];
    z[29]=z[29]*z[120];
    z[28]=z[29] + z[28];
    z[28]=z[9]*z[28];
    z[29]=n<T>(1159,4) + 110*z[23];
    z[29]=z[29]*z[85];
    z[28]=z[29] + z[28];
    z[28]=z[9]*z[28];
    z[29]= - n<T>(241,18)*z[10] - z[27];
    z[29]=z[3]*z[29];
    z[30]=5*z[10] - z[27];
    z[30]=z[30]*z[21];
    z[30]=n<T>(367,18) + z[30];
    z[30]=z[3]*z[30];
    z[30]=z[30] - 14*z[14];
    z[30]=z[11]*z[30];
    z[31]= - n<T>(35,3)*z[10] - 4*z[116];
    z[31]=z[6]*z[31];
    z[26]=z[26] + z[28] + z[31] + 5*z[98] + z[30] + n<T>(343,9) + z[29];
    z[26]=z[2]*z[26];
    z[28]= - z[10]*z[21];
    z[28]=z[28] + static_cast<T>(3)+ z[97];
    z[28]=z[11]*z[28];
    z[29]=z[10]*z[100];
    z[28]=z[28] + z[29] - z[27];
    z[28]=z[7]*z[28];
    z[29]=z[8]*npow(z[10],3);
    z[29]=n<T>(47,4)*z[115] + 6*z[29];
    z[29]=z[8]*z[29];
    z[29]=z[105] + z[29];
    z[29]=z[3]*z[29];
    z[30]=z[108] - z[111];
    z[30]=20*z[14] + 3*z[30] + 11*z[3];
    z[30]=z[11]*z[30];
    z[31]= - n<T>(33,2)*z[10] - 7*z[76];
    z[31]=z[8]*z[31];
    z[28]=z[28] + z[30] + z[29] + z[31] - n<T>(1,4) - 6*z[97];
    z[28]=z[7]*z[28];
    z[29]=z[118] - z[39];
    z[29]=z[43]*z[29];
    z[23]= - n<T>(5,4)*z[23] - static_cast<T>(16)+ n<T>(41,4)*z[113];
    z[23]=z[11]*z[23];
    z[30]= - n<T>(1,2)*z[113] + z[109];
    z[30]=z[11]*z[30];
    z[30]=z[16] + z[30];
    z[30]=z[30]*z[104];
    z[23]=z[30] + z[23] - n<T>(1,4)*z[16] + z[29];
    z[23]=z[9]*z[23];
    z[29]= - n<T>(233,2) + 175*z[113];
    z[30]= - z[38] - n<T>(943,36)*z[3];
    z[30]=z[11]*z[30];
    z[23]=z[23] - n<T>(35,36)*z[106] + z[96] + n<T>(1,18)*z[29] + z[30];
    z[23]=z[9]*z[23];
    z[29]=z[115]*z[127];
    z[29]=z[29] - z[76];
    z[29]=z[29]*z[21];
    z[30]=z[74]*z[57];
    z[31]=z[13] + 12*z[91];
    z[31]=z[31]*z[10];
    z[32]= - n<T>(13,3)*z[10] + 8*z[76];
    z[32]=z[8]*z[32];
    z[29]=z[30] + z[29] + z[32] - n<T>(13,3) - z[31];
    z[29]=z[6]*z[29];
    z[27]=z[27]*z[13];
    z[30]=z[10]*z[117];
    z[30]= - 2*z[43] + z[30];
    z[30]=z[30]*z[125];
    z[32]=137*z[16] - 257*z[10];
    z[30]=n<T>(1,12)*z[32] + z[30];
    z[30]=z[8]*z[30];
    z[30]=z[27] + z[30] - n<T>(304,9) - z[31];
    z[30]=z[3]*z[30];
    z[31]= - z[127] + z[89];
    z[32]= - z[10]*z[79];
    z[27]=z[32] + z[27];
    z[21]=z[27]*z[21];
    z[21]=3*z[31] + z[21];
    z[21]=z[3]*z[21];
    z[21]= - 6*z[22] - z[137] + z[21];
    z[21]=z[11]*z[21];
    z[22]= - n<T>(101,12)*z[16] + z[45];
    z[22]=z[8]*z[22];
    z[22]= - n<T>(38,3)*z[78] + n<T>(134,9)*z[92] + n<T>(17,9) + z[22];
    z[22]=z[5]*z[22];
    z[27]=31*z[16] + 25*z[10];
    z[27]=z[8]*z[27];
    z[27]=n<T>(5,4) + z[27];
    z[27]=z[27]*z[35];
    z[31]= - z[10]*z[114];
    z[31]= - n<T>(21,4)*z[68] - n<T>(25,18) + z[31];
    z[31]=z[4]*z[31];
    z[25]=z[10]*z[25];
    z[20]=z[20] + z[24] + z[26] + z[23] + z[22] + z[29] + z[31] + z[28]
    + z[21] + z[30] + z[27] - 6*z[25] + z[93] - z[34] + z[17] - z[124];

    r += z[20]*z[1];
 
    return r;
}

template double qqb_2lNLC_r282(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r282(const std::array<dd_real,31>&);
#endif
