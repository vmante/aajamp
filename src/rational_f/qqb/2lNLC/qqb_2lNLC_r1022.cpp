#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1022(const std::array<T,31>& k) {
  T z[90];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[17];
    z[5]=k[19];
    z[6]=k[14];
    z[7]=k[11];
    z[8]=k[5];
    z[9]=k[6];
    z[10]=k[8];
    z[11]=k[12];
    z[12]=k[9];
    z[13]=k[22];
    z[14]=k[13];
    z[15]=k[15];
    z[16]=k[2];
    z[17]=k[3];
    z[18]=k[4];
    z[19]=k[29];
    z[20]=k[26];
    z[21]=z[11]*z[17];
    z[22]=z[6]*z[16];
    z[23]=z[11] - z[6];
    z[23]=z[8]*z[23];
    z[23]=n<T>(5,3)*z[23] + n<T>(5,3)*z[21] - z[22];
    z[24]=2*z[8];
    z[25]=npow(z[9],3);
    z[23]=z[24]*z[25]*z[23];
    z[26]=9*z[19];
    z[27]=5*z[15];
    z[28]=z[26] - z[27];
    z[29]=z[11]*z[28];
    z[30]=npow(z[16],2);
    z[31]=5*z[30];
    z[32]= - z[25]*z[31];
    z[32]=z[32] - 16*z[11];
    z[33]=n<T>(2,3)*z[6];
    z[32]=z[32]*z[33];
    z[34]=z[6] - z[19];
    z[35]=z[2]*z[34];
    z[23]=z[23] + 3*z[35] + z[29] + z[32];
    z[23]=z[8]*z[23];
    z[29]=2*z[17];
    z[32]=z[16] + z[17];
    z[32]=z[32]*z[29];
    z[32]=z[31] + z[32];
    z[32]=z[17]*z[32]*z[25];
    z[32]= - static_cast<T>(1)+ n<T>(1,3)*z[32];
    z[35]=2*z[3];
    z[32]=z[32]*z[35];
    z[36]=2*z[7];
    z[37]=z[3]*z[17];
    z[38]=static_cast<T>(2)- n<T>(7,3)*z[37];
    z[38]=z[38]*z[36];
    z[39]=npow(z[17],3);
    z[40]=z[39]*z[25];
    z[41]=z[7]*z[17];
    z[42]=2*z[40] - z[41];
    z[43]=n<T>(2,3)*z[11];
    z[42]=z[42]*z[43];
    z[44]=5*z[6];
    z[45]=2*z[10];
    z[46]=z[16]*z[45];
    z[46]= - static_cast<T>(1)+ z[46];
    z[46]=z[46]*z[44];
    z[47]=z[11]*z[19];
    z[48]=z[6]*z[11];
    z[49]=z[47] - z[48];
    z[50]=npow(z[8],2);
    z[51]=z[50]*z[12];
    z[52]=3*z[51];
    z[53]=z[49]*z[52];
    z[54]=5*z[10];
    z[23]=z[53] + z[23] + z[46] + z[42] + z[38] - z[54] + z[32];
    z[32]=2*z[12];
    z[23]=z[23]*z[32];
    z[38]= - static_cast<T>(2)- z[40];
    z[42]=n<T>(4,3)*z[7];
    z[38]=z[38]*z[42];
    z[28]=z[38] + z[28];
    z[38]=2*z[11];
    z[28]=z[28]*z[38];
    z[46]=z[25]*z[50]*z[48];
    z[50]=npow(z[6],2);
    z[50]=n<T>(8,3)*z[50];
    z[53]=npow(z[15],2);
    z[55]=5*z[53];
    z[56]= - z[55] + z[50];
    z[56]=z[2]*z[56];
    z[57]=npow(z[10],2);
    z[58]=z[57]*z[7];
    z[46]= - n<T>(23,3)*z[46] + n<T>(22,3)*z[58] + z[56];
    z[46]=z[46]*z[24];
    z[56]=static_cast<T>(29)- 8*z[40];
    z[59]=n<T>(1,3)*z[3];
    z[56]=z[56]*z[59];
    z[56]= - 6*z[10] + z[56];
    z[56]=z[7]*z[56];
    z[60]= - 10*z[10] - 23*z[11];
    z[60]=z[6]*z[60];
    z[26]= - z[26] + n<T>(17,3)*z[15];
    z[26]=2*z[26] + n<T>(37,3)*z[6];
    z[26]=z[2]*z[26];
    z[23]=z[23] + z[46] + z[26] + z[60] + z[28] - n<T>(22,3)*z[57] + z[56];
    z[23]=z[12]*z[23];
    z[26]=z[5]*z[4];
    z[28]=npow(z[4],2);
    z[46]= - 3*z[26] + n<T>(8,3)*z[28];
    z[56]=z[46]*z[3];
    z[60]=z[5]*z[14];
    z[61]=n<T>(10,3)*z[60];
    z[62]=z[11]*z[5];
    z[62]=n<T>(10,3)*z[62] - z[61];
    z[62]=z[13]*z[62];
    z[63]=z[4] + 2*z[5];
    z[63]=n<T>(8,3)*z[63];
    z[64]=z[6]*z[63];
    z[64]=z[64] + z[46];
    z[64]=z[6]*z[64];
    z[65]=z[8]*z[9];
    z[65]=npow(z[65],4);
    z[66]=z[57]*z[11];
    z[67]=z[66]*z[65];
    z[62]=n<T>(4,3)*z[67] + z[64] - z[56] + z[62];
    z[62]=z[2]*z[62];
    z[64]=z[7]*z[5];
    z[67]=z[64]*z[28];
    z[68]=z[6]*z[7];
    z[69]=z[68]*z[26];
    z[69]=z[67] + z[69];
    z[69]=z[6]*z[69];
    z[67]= - z[3]*z[67];
    z[28]=z[28]*z[5];
    z[26]= - z[6]*z[26];
    z[26]= - z[28] + z[26];
    z[26]=z[6]*z[26];
    z[28]=z[3]*z[28];
    z[26]=z[28] + z[26];
    z[26]=z[2]*z[26];
    z[26]=z[26] + z[67] + z[69];
    z[26]=z[1]*z[26];
    z[28]= - z[7]*z[46];
    z[46]= - z[68]*z[63];
    z[28]=z[28] + z[46];
    z[28]=z[6]*z[28];
    z[46]=z[48]*z[65];
    z[63]=z[7]*z[35];
    z[65]=z[6]*z[54];
    z[46]= - n<T>(10,3)*z[46] + z[63] + z[65];
    z[46]=z[12]*z[46];
    z[46]=n<T>(11,3)*z[58] + z[46];
    z[46]=z[12]*z[46];
    z[63]=z[13]*z[61];
    z[56]=z[63] + z[56];
    z[56]=z[7]*z[56];
    z[63]=n<T>(2,3)*z[13];
    z[63]= - z[64]*z[63];
    z[65]=z[57]*z[15];
    z[63]= - z[65] + z[63];
    z[67]=5*z[11];
    z[63]=z[63]*z[67];
    z[26]=n<T>(8,3)*z[26] + z[46] + z[28] + z[56] + z[63] + z[62];
    z[26]=z[1]*z[26];
    z[28]=z[40]*z[5];
    z[40]=11*z[4];
    z[46]=17*z[5];
    z[56]=4*z[28] + z[40] - z[46];
    z[56]=z[3]*z[56];
    z[62]=z[14]*z[13];
    z[63]=z[62] + z[60];
    z[56]=10*z[63] + z[56];
    z[63]=npow(z[18],3);
    z[69]=z[63]*z[25];
    z[69]= - static_cast<T>(23)+ 8*z[69];
    z[69]=z[69]*z[59];
    z[70]=3*z[19];
    z[71]= - z[35] - z[70];
    z[71]=z[18]*z[71];
    z[71]= - static_cast<T>(10)+ z[71];
    z[72]=2*z[15];
    z[71]=z[71]*z[72];
    z[69]=z[71] + 18*z[19] + z[69];
    z[69]=z[15]*z[69];
    z[71]=z[5] + 2*z[28];
    z[73]=5*z[13];
    z[74]= - z[73] + z[71];
    z[74]=z[11]*z[74];
    z[40]= - z[40] - z[46];
    z[40]=n<T>(2,3)*z[40] + z[54];
    z[40]=z[6]*z[40];
    z[40]=z[40] + n<T>(4,3)*z[74] + n<T>(2,3)*z[56] + z[69];
    z[40]=z[2]*z[40];
    z[46]=z[3]*z[4];
    z[46]=5*z[57] - n<T>(4,3)*z[46];
    z[56]= - z[70] + z[54];
    z[56]=2*z[56] + z[3];
    z[56]=z[15]*z[56];
    z[69]= - n<T>(4,3)*z[71] - z[27];
    z[69]=z[7]*z[69];
    z[46]=z[69] + 2*z[46] + z[56];
    z[46]=z[11]*z[46];
    z[56]=z[15]*z[16];
    z[69]=static_cast<T>(1)+ z[56];
    z[69]=z[15]*z[10]*z[69];
    z[62]= - n<T>(2,3)*z[62] + z[69];
    z[69]=3*z[4];
    z[71]=n<T>(17,3)*z[5];
    z[74]= - z[69] + z[71];
    z[74]=z[7]*z[74];
    z[75]=2*z[4];
    z[73]=z[73] + z[75];
    z[43]=z[73]*z[43];
    z[73]=n<T>(16,3)*z[68];
    z[43]=z[73] + z[43] + 5*z[62] + z[74];
    z[62]=2*z[6];
    z[43]=z[43]*z[62];
    z[74]=z[11]*z[10];
    z[76]=z[57] - 2*z[74];
    z[76]=z[2]*z[76];
    z[77]= - z[15]*z[68];
    z[76]=z[76] - z[66] + z[77];
    z[76]=z[8]*z[25]*z[76];
    z[77]= - z[58] + z[66];
    z[25]=z[25]*z[18]*z[77];
    z[25]=z[76] + z[25];
    z[25]=z[8]*z[25];
    z[76]=z[2]*z[5];
    z[76]=z[76] - z[64];
    z[50]=z[50]*z[76];
    z[76]= - z[27]*z[66];
    z[25]=n<T>(4,3)*z[25] + z[76] + z[50];
    z[25]=z[25]*z[24];
    z[28]= - n<T>(4,3)*z[28] + z[69] + z[71];
    z[28]=2*z[28] + z[54];
    z[28]=z[3]*z[28];
    z[50]= - 10*z[60] - 7*z[57];
    z[28]=n<T>(2,3)*z[50] + z[28];
    z[28]=z[7]*z[28];
    z[23]=2*z[26] + z[23] + z[25] + z[40] + z[43] + z[46] - 10*z[65] + 
    z[28];
    z[23]=z[1]*z[23];
    z[25]=npow(z[9],2);
    z[26]=2*z[25];
    z[28]=z[16] + n<T>(5,3)*z[17];
    z[28]=z[28]*z[26];
    z[40]=z[70]*z[11];
    z[43]=n<T>(2,3)*z[25] + z[40];
    z[43]=5*z[43] - 27*z[48];
    z[43]=z[8]*z[43];
    z[46]=n<T>(8,3)*z[7];
    z[28]=z[43] - 12*z[6] + n<T>(20,3)*z[11] + z[46] + z[70] + z[28];
    z[28]=z[8]*z[28];
    z[43]=n<T>(5,3)*z[30];
    z[50]=z[17]*z[16];
    z[50]=z[43] + z[50];
    z[50]=z[50]*z[26];
    z[60]=z[54]*z[30];
    z[65]=3*z[20];
    z[69]=z[65]*z[17];
    z[71]=n<T>(5,3) + z[69];
    z[71]=z[17]*z[71];
    z[71]= - z[60] - n<T>(5,3)*z[16] + z[71];
    z[71]=z[3]*z[71];
    z[76]=4*z[7];
    z[77]=npow(z[17],2);
    z[78]=z[77]*z[3];
    z[79]= - n<T>(4,3)*z[17] + z[78];
    z[79]=z[79]*z[76];
    z[80]=z[30]*z[10];
    z[81]=z[80] - 2*z[16];
    z[81]=z[81]*z[44];
    z[49]=z[49]*z[8];
    z[34]=z[49] - z[34];
    z[49]=z[34]*z[51];
    z[28]=6*z[49] + z[28] + z[81] + n<T>(2,3)*z[21] + z[79] + z[71] - n<T>(1,3)
    + z[50];
    z[28]=z[28]*z[32];
    z[49]= - z[65] + n<T>(10,3)*z[14];
    z[33]= - z[33] - n<T>(14,3)*z[15] - n<T>(2,3)*z[10] + z[70] - z[49];
    z[33]=z[2]*z[33];
    z[50]=z[10] - n<T>(25,3)*z[15];
    z[50]=z[7]*z[50];
    z[51]=z[17]*z[25];
    z[51]=12*z[19] + z[51];
    z[51]=z[11]*z[51];
    z[33]=z[33] + z[50] + z[51];
    z[50]= - z[55] + n<T>(11,3)*z[57];
    z[51]= - z[2] + z[7];
    z[50]=z[50]*z[51];
    z[51]=3*z[6];
    z[71]= - z[51] + n<T>(23,3)*z[11];
    z[71]=z[25]*z[71];
    z[50]=z[71] + z[50];
    z[50]=z[50]*z[24];
    z[71]=z[16]*z[25];
    z[71]=n<T>(16,3)*z[6] - n<T>(14,3)*z[71] - 71*z[11];
    z[71]=z[6]*z[71];
    z[33]=z[50] + z[71] + 2*z[33];
    z[33]=z[8]*z[33];
    z[50]=n<T>(4,3)*z[30];
    z[71]=7*z[16];
    z[79]= - z[71] - n<T>(8,3)*z[17];
    z[79]=z[17]*z[79];
    z[79]= - z[50] + z[79];
    z[79]=z[79]*z[25];
    z[81]=9*z[20];
    z[82]= - z[17]*z[81];
    z[79]=z[79] - n<T>(5,3) + z[82];
    z[79]=z[3]*z[79];
    z[49]=4*z[15] + z[79] + n<T>(26,3)*z[10] + z[49];
    z[79]=z[77]*z[25];
    z[79]=8*z[79];
    z[41]= - 3*z[41] + n<T>(61,3) + z[79];
    z[41]=z[11]*z[41];
    z[82]=4*z[25];
    z[83]=z[30]*z[82];
    z[83]= - static_cast<T>(37)+ z[83];
    z[84]=z[54]*z[16];
    z[83]=n<T>(1,3)*z[83] - z[84];
    z[62]=z[83]*z[62];
    z[37]=n<T>(16,3) - 11*z[37];
    z[37]=z[7]*z[37];
    z[28]=z[28] + z[33] + z[62] + z[41] + 2*z[49] + z[37];
    z[28]=z[12]*z[28];
    z[33]=n<T>(22,3)*z[11];
    z[37]= - z[25]*z[33];
    z[41]=n<T>(4,3)*z[25];
    z[41]=z[41]*z[15];
    z[49]=z[7]*z[55];
    z[37]=z[37] + z[41] + z[49];
    z[37]=z[6]*z[37];
    z[49]=z[65]*z[10];
    z[62]= - z[82] - z[49];
    z[62]=z[10]*z[62];
    z[82]= - z[25] - n<T>(2,3)*z[57];
    z[82]=z[11]*z[82];
    z[41]=z[82] + z[62] + z[41];
    z[41]=z[2]*z[41];
    z[62]=z[26]*z[74];
    z[82]=z[7]*z[10]*z[25];
    z[62]=z[82] + z[62];
    z[37]=z[41] + n<T>(4,3)*z[62] + z[37];
    z[37]=z[37]*z[24];
    z[41]=z[25]*z[18];
    z[62]= - z[41] - n<T>(14,3)*z[10];
    z[62]=z[10]*z[62];
    z[82]= - z[15]*z[41];
    z[62]=z[82] + z[61] + z[62];
    z[62]=z[7]*z[62];
    z[57]=z[65]*z[57];
    z[62]=n<T>(17,3)*z[66] + z[57] + z[62];
    z[56]= - z[26]*z[56];
    z[82]=4*z[5] + 47*z[15];
    z[82]=z[7]*z[82];
    z[56]=z[56] + z[82];
    z[82]=z[11]*z[41];
    z[56]=z[73] + n<T>(1,3)*z[56] - 8*z[82];
    z[56]=z[6]*z[56];
    z[41]= - z[65] - z[41];
    z[41]=z[10]*z[41];
    z[73]=3*z[18];
    z[82]=z[25]*z[73];
    z[82]=z[82] + 20*z[15];
    z[82]=z[15]*z[82];
    z[41]=z[82] - z[61] + z[41];
    z[61]=z[17]*z[26];
    z[61]=z[61] + n<T>(7,3)*z[10];
    z[61]=z[61]*z[67];
    z[82]=z[6]*z[5];
    z[41]= - n<T>(4,3)*z[82] + 2*z[41] + z[61];
    z[41]=z[2]*z[41];
    z[37]=z[37] + z[41] + 2*z[62] + z[56];
    z[37]=z[8]*z[37];
    z[41]=npow(z[18],2);
    z[56]=z[41]*z[25];
    z[61]=z[18]*z[19];
    z[62]=3*z[56] - n<T>(8,3) - 15*z[61];
    z[82]=z[41]*z[3];
    z[83]=6*z[61];
    z[85]=z[83] + 25;
    z[86]=z[18]*z[85];
    z[86]=z[86] + 4*z[82];
    z[86]=z[86]*z[72];
    z[87]=z[3]*z[18];
    z[62]=z[86] + 2*z[62] + 19*z[87];
    z[62]=z[15]*z[62];
    z[77]=4*z[77];
    z[86]= - 7*z[17] + 4*z[18];
    z[86]=z[18]*z[86];
    z[86]=z[77] + z[86];
    z[86]=z[86]*z[26];
    z[87]=z[17]*z[5];
    z[86]=z[86] + static_cast<T>(17)- 26*z[87];
    z[59]=z[86]*z[59];
    z[86]=static_cast<T>(13)- 10*z[87];
    z[79]=n<T>(1,3)*z[86] - z[79];
    z[79]=z[11]*z[79];
    z[86]=z[70] - n<T>(2,3)*z[5];
    z[59]=z[79] + z[62] + z[59] + 8*z[86] - z[54];
    z[59]=z[2]*z[59];
    z[26]=z[26]*z[41];
    z[62]=z[10]*z[18];
    z[79]=n<T>(26,3)*z[62] - n<T>(35,3) - z[26];
    z[79]=z[10]*z[79];
    z[86]=z[18]*z[4];
    z[88]= - n<T>(8,3)*z[86] + z[62];
    z[88]=z[3]*z[88];
    z[89]=static_cast<T>(1)+ n<T>(5,3)*z[87];
    z[36]=z[89]*z[36];
    z[36]=z[36] + z[88] + 6*z[19] + z[79];
    z[36]=z[11]*z[36];
    z[79]=z[25]*z[50];
    z[80]= - z[16] + z[80];
    z[80]=z[80]*z[27];
    z[79]=z[80] + z[84] + static_cast<T>(1)+ z[79];
    z[79]=z[15]*z[79];
    z[80]=z[65] - z[75];
    z[79]=z[79] + 3*z[80] + z[54];
    z[26]=z[26] + static_cast<T>(5)- n<T>(26,3)*z[86];
    z[26]=z[7]*z[26];
    z[80]= - static_cast<T>(11)+ z[86];
    z[80]=n<T>(4,3)*z[80] - z[56];
    z[38]=z[80]*z[38];
    z[26]=z[38] + 2*z[79] + z[26];
    z[26]=z[6]*z[26];
    z[38]=z[18] - z[16];
    z[79]= - z[38]*z[73];
    z[50]= - z[50] + z[79];
    z[25]=z[50]*z[25];
    z[25]= - static_cast<T>(4)+ z[25];
    z[25]=z[3]*z[25];
    z[50]= - 9*z[18] - z[60];
    z[50]=z[3]*z[50];
    z[60]= - static_cast<T>(5)- 2*z[61];
    z[50]=3*z[60] + z[50];
    z[50]=z[15]*z[50];
    z[25]=z[50] + z[70] + z[25];
    z[25]=z[15]*z[25];
    z[50]=z[65] + z[75];
    z[60]=z[65]*z[18];
    z[75]= - static_cast<T>(5)- z[60];
    z[75]=z[10]*z[75];
    z[50]=3*z[50] + z[75];
    z[50]=z[3]*z[50];
    z[75]=static_cast<T>(2)- z[62];
    z[75]=z[10]*z[20]*z[75];
    z[25]=z[25] + 3*z[75] + z[50];
    z[50]= - n<T>(4,3)*z[62] + static_cast<T>(2)+ z[56];
    z[50]=z[10]*z[50];
    z[50]=n<T>(8,3)*z[5] + z[50];
    z[56]=z[86] + z[87];
    z[56]= - static_cast<T>(46)+ 13*z[56];
    z[54]= - z[18]*z[54];
    z[54]=n<T>(2,3)*z[56] + z[54];
    z[54]=z[3]*z[54];
    z[50]= - z[27] + 2*z[50] + z[54];
    z[50]=z[7]*z[50];
    z[23]=z[23] + z[28] + z[37] + z[59] + z[26] + z[36] + 2*z[25] + 
    z[50];
    z[23]=z[1]*z[23];
    z[25]= - z[11] - z[3];
    z[25]=z[42]*z[39]*z[25];
    z[26]=6*z[47] - n<T>(59,3)*z[48];
    z[26]=z[8]*z[26];
    z[26]=z[26] - n<T>(46,3)*z[6] + z[70] + z[33];
    z[26]=z[8]*z[26];
    z[28]=z[16]*z[19];
    z[33]=2*z[21] + static_cast<T>(2)- z[28];
    z[26]=z[26] + 3*z[33] - n<T>(23,3)*z[22];
    z[26]=z[8]*z[26];
    z[33]=z[8]*z[34];
    z[28]=z[33] + z[28] - z[22];
    z[28]=z[28]*z[52];
    z[33]= - static_cast<T>(1)- z[69];
    z[33]=z[17]*z[33];
    z[33]=n<T>(1,3)*z[16] + z[33];
    z[33]=z[17]*z[33];
    z[33]=z[43] + z[33];
    z[33]=z[3]*z[33];
    z[34]=static_cast<T>(10)+ z[69];
    z[34]=z[17]*z[34];
    z[36]= - z[6]*z[31];
    z[25]=z[28] + z[26] + z[36] + z[33] + n<T>(17,3)*z[16] + z[34] + z[25];
    z[25]=z[12]*z[25];
    z[26]=4*z[2];
    z[28]=z[26] - z[76];
    z[33]=z[15] - z[10];
    z[28]=z[33]*z[28];
    z[28]= - 29*z[48] + z[40] + z[28];
    z[28]=z[8]*z[28];
    z[28]=z[28] + 8*z[2] - n<T>(14,3)*z[6] + n<T>(43,3)*z[11] - z[70] + z[46];
    z[28]=z[8]*z[28];
    z[33]=n<T>(1,3) + z[69];
    z[33]=z[17]*z[33];
    z[33]=3*z[16] + z[33];
    z[33]=z[33]*z[35];
    z[34]=z[78] - n<T>(7,3)*z[17];
    z[34]=z[34]*z[76];
    z[36]=n<T>(13,3)*z[17];
    z[37]= - z[7]*z[77];
    z[37]=z[36] + z[37];
    z[37]=z[11]*z[37];
    z[26]= - z[17]*z[26];
    z[22]=z[25] + z[28] + z[26] - n<T>(16,3)*z[22] + z[37] + z[34] + z[33]
    - n<T>(29,3) + z[69];
    z[22]=z[22]*z[32];
    z[25]= - z[16]*z[70];
    z[25]=z[25] + z[85];
    z[25]=z[18]*z[25];
    z[26]= - 4*z[16] + z[73];
    z[26]=z[26]*z[73];
    z[26]=z[31] + z[26];
    z[26]=z[3]*z[26];
    z[25]=z[26] - 5*z[16] + z[25];
    z[25]=z[15]*z[25];
    z[26]= - z[18] - z[16];
    z[26]=z[70]*z[26];
    z[28]= - z[71] + 13*z[18];
    z[28]=z[3]*z[28];
    z[25]=z[25] + z[28] + n<T>(16,3) + z[26];
    z[25]=z[15]*z[25];
    z[26]=static_cast<T>(1)- n<T>(10,3)*z[87];
    z[28]=z[77]*z[3]*z[5];
    z[26]=z[28] + 4*z[26] + z[62];
    z[26]=z[7]*z[26];
    z[28]= - static_cast<T>(1)- z[60];
    z[28]=z[28]*z[45];
    z[31]= - z[64]*z[77];
    z[32]=z[10]*z[73];
    z[31]=z[31] + n<T>(22,3) + z[32];
    z[31]=z[11]*z[31];
    z[32]= - z[65] - 2*z[19];
    z[33]=static_cast<T>(2)- z[69];
    z[33]=z[3]*z[33];
    z[25]=z[31] + z[26] + z[25] + z[33] + 3*z[32] + z[28];
    z[26]= - static_cast<T>(5)- z[61];
    z[26]=z[26]*z[41];
    z[28]= - z[63]*z[35];
    z[26]=3*z[26] + z[28];
    z[26]=z[15]*z[26];
    z[28]= - static_cast<T>(5)+ z[83];
    z[28]=z[18]*z[28];
    z[26]=z[26] + z[28] - 7*z[82];
    z[26]=z[26]*z[72];
    z[28]=8*z[87];
    z[31]= - static_cast<T>(5)+ z[28];
    z[21]=z[31]*z[21];
    z[31]= - static_cast<T>(7)+ 80*z[87];
    z[28]=static_cast<T>(3)- z[28];
    z[28]=z[17]*z[28];
    z[28]=z[28] - n<T>(22,3)*z[18];
    z[28]=z[3]*z[28];
    z[21]=z[21] + z[26] + z[28] + n<T>(1,3)*z[31] - z[83];
    z[21]=z[2]*z[21];
    z[26]=n<T>(5,3)*z[74] + z[49] - z[55];
    z[26]=z[2]*z[26];
    z[28]=z[55]*z[68];
    z[31]= - z[57] + n<T>(2,3)*z[66];
    z[32]=z[2]*z[31];
    z[28]=z[28] + z[32];
    z[28]=z[8]*z[28];
    z[32]=z[7]*z[72];
    z[32]= - z[53] + z[32];
    z[32]=z[32]*z[44];
    z[26]=z[28] + z[26] + z[32] - n<T>(7,3)*z[58] - z[31];
    z[24]=z[26]*z[24];
    z[26]= - z[18]*z[57];
    z[28]=static_cast<T>(1)- 8*z[62];
    z[28]=z[10]*z[28];
    z[28]=32*z[5] + z[28];
    z[28]=n<T>(1,3)*z[28] - z[72];
    z[28]=z[7]*z[28];
    z[31]=static_cast<T>(10)+ 17*z[62];
    z[31]=z[31]*z[74];
    z[26]=n<T>(1,3)*z[31] + z[28] + z[26] + z[55];
    z[28]=z[16]*z[27];
    z[28]= - n<T>(16,3) + z[28];
    z[28]=z[15]*z[28];
    z[28]=5*z[7] + z[81] + z[28];
    z[28]=2*z[28] - n<T>(85,3)*z[11];
    z[28]=z[6]*z[28];
    z[31]=z[15]*z[18];
    z[31]=static_cast<T>(2)- 15*z[31];
    z[31]=z[15]*z[31];
    z[31]=z[31] - n<T>(32,3)*z[5] + z[10];
    z[31]=2*z[31] - z[67];
    z[31]=z[2]*z[31];
    z[24]=z[24] + z[31] + 2*z[26] + z[28];
    z[24]=z[8]*z[24];
    z[26]= - z[30]*z[27];
    z[26]=n<T>(8,3)*z[16] + z[26];
    z[26]=z[15]*z[26];
    z[27]= - z[18]*z[81];
    z[26]=z[26] - static_cast<T>(10)+ z[27];
    z[27]= - n<T>(5,3)*z[11] - n<T>(7,3)*z[7];
    z[27]=z[18]*z[27];
    z[26]=2*z[26] + z[27];
    z[26]=z[6]*z[26];
    z[21]=z[23] + z[22] + z[24] + z[21] + 2*z[25] + z[26];
    z[21]=z[1]*z[21];
    z[22]=z[29] - z[38];
    z[22]=z[3]*z[22];
    z[23]= - z[15] - z[7];
    z[23]=4*z[23] + 19*z[11];
    z[23]=7*z[2] + n<T>(1,3)*z[23] - z[51];
    z[23]=z[8]*z[23];
    z[24]=z[16] - 8*z[18];
    z[24]=z[15]*z[24];
    z[25]= - z[36] + 5*z[18];
    z[25]=z[2]*z[25];
    z[26]=z[71] + 13*z[17];
    z[26]=n<T>(1,3)*z[26] + 3*z[8];
    z[26]=z[12]*z[26];
    z[22]=z[26] + z[23] + z[25] + n<T>(1,3)*z[24] - n<T>(17,3) + 3*z[22];

    r += z[21] + 2*z[22];
 
    return r;
}

template double qqb_2lNLC_r1022(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1022(const std::array<dd_real,31>&);
#endif
