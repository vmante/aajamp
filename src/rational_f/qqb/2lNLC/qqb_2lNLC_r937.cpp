#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r937(const std::array<T,31>& k) {
  T z[77];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[3];
    z[11]=k[8];
    z[12]=k[2];
    z[13]=k[9];
    z[14]=k[15];
    z[15]=z[9]*z[2];
    z[16]=z[3]*z[8];
    z[17]= - 2*z[16] - static_cast<T>(3)- z[15];
    z[18]=n<T>(2,5)*z[4];
    z[17]=z[17]*z[18];
    z[17]=z[17] + n<T>(7,5)*z[2] + z[3];
    z[17]=z[4]*z[17];
    z[19]=z[3]*z[2];
    z[17]= - z[19] + z[17];
    z[17]=z[4]*z[17];
    z[20]=z[9]*z[7];
    z[21]=z[20] - 1;
    z[22]=z[21] + z[16];
    z[23]=z[22]*z[6];
    z[24]= - n<T>(2,5)*z[23] - n<T>(7,5)*z[7] + z[3];
    z[24]=z[6]*z[24];
    z[25]=z[3]*z[7];
    z[24]= - z[25] + z[24];
    z[24]=z[6]*z[24];
    z[17]=z[17] + z[24];
    z[24]=2*z[8];
    z[26]=z[24] + z[9];
    z[27]=npow(z[8],2);
    z[28]=z[27]*z[3];
    z[29]=z[26] + z[28];
    z[30]=npow(z[4],2);
    z[31]=39*z[30];
    z[32]=z[29]*z[31];
    z[33]=n<T>(1,2)*z[3];
    z[34]=z[2] + z[33];
    z[32]=n<T>(389,3)*z[34] + z[32];
    z[32]=z[4]*z[32];
    z[34]=npow(z[6],2);
    z[35]=z[34]*z[28];
    z[36]=z[33] - z[7];
    z[35]= - n<T>(1649,3)*z[36] - 39*z[35];
    z[35]=z[6]*z[35];
    z[37]=389*z[2];
    z[38]=1649*z[7] - z[37];
    z[38]=z[3]*z[38];
    z[32]=z[35] + n<T>(1,6)*z[38] + z[32];
    z[35]=n<T>(1,5)*z[5];
    z[32]=z[32]*z[35];
    z[17]=39*z[17] + z[32];
    z[17]=z[5]*z[17];
    z[32]=npow(z[7],3);
    z[38]=n<T>(1414,5)*z[32];
    z[39]=npow(z[2],3);
    z[40]= - z[38] - n<T>(121,4)*z[39];
    z[41]=npow(z[3],2);
    z[42]=1113*z[7];
    z[43]=z[42] - 6953*z[2];
    z[43]=z[43]*z[41];
    z[40]=n<T>(1,3)*z[40] + n<T>(1,40)*z[43];
    z[40]=z[3]*z[40];
    z[43]=n<T>(121,3)*z[39];
    z[44]=npow(z[3],3);
    z[44]=z[43] + n<T>(6953,20)*z[44];
    z[45]=n<T>(1,4)*z[4];
    z[46]= - 1243*z[2] - n<T>(5279,10)*z[3];
    z[46]=z[46]*z[45];
    z[46]= - 117*z[19] + z[46];
    z[46]=z[4]*z[46];
    z[44]=n<T>(1,2)*z[44] + z[46];
    z[44]=z[4]*z[44];
    z[46]=npow(z[7],2);
    z[47]=4*z[46] - n<T>(1113,8)*z[41];
    z[47]=z[3]*z[47];
    z[47]= - n<T>(2828,3)*z[32] + z[47];
    z[36]=z[6]*z[36];
    z[36]=117*z[25] - n<T>(1951,30)*z[36];
    z[36]=z[6]*z[36];
    z[36]=n<T>(1,5)*z[47] + z[36];
    z[36]=z[6]*z[36];
    z[17]=z[17] + z[36] + z[40] + z[44];
    z[17]=z[5]*z[17];
    z[36]=n<T>(8717,120)*z[30];
    z[40]=z[2] - z[7];
    z[44]=z[40]*z[36];
    z[47]=10*z[32];
    z[44]=z[44] - z[47] - n<T>(1493,60)*z[39];
    z[44]=z[4]*z[44];
    z[48]=3229*z[4];
    z[49]= - z[40]*z[48];
    z[50]=z[2]*z[7];
    z[51]=3229*z[50];
    z[49]=z[49] - z[51] - n<T>(1951,3)*z[25];
    z[49]=z[49]*z[34];
    z[52]=z[41]*z[7];
    z[52]= - n<T>(202,3)*z[32] + n<T>(159,8)*z[52];
    z[53]=n<T>(7,5)*z[3];
    z[52]=z[52]*z[53];
    z[53]=npow(z[2],2);
    z[54]=z[53]*z[7];
    z[55]= - z[47] + n<T>(1493,60)*z[54];
    z[56]=z[55]*z[2];
    z[44]=n<T>(1,20)*z[49] + z[44] - z[56] - z[52];
    z[44]=z[6]*z[44];
    z[49]=2*z[4];
    z[57]= - 3*z[2] - 2*z[3];
    z[57]=z[57]*z[49];
    z[57]=9*z[19] + z[57];
    z[57]=z[57]*z[30];
    z[58]=2*z[7];
    z[59]=z[58] - z[3];
    z[60]=z[59]*z[6];
    z[61]= - 3*z[25] - z[60];
    z[61]=z[61]*z[34];
    z[57]=z[57] + 3*z[61];
    z[61]=z[15] + z[16];
    z[62]=static_cast<T>(2)+ z[61];
    z[62]=z[4]*z[62];
    z[63]=z[3] + 2*z[2];
    z[62]=z[62] - z[63];
    z[64]=39*z[4];
    z[62]=z[62]*z[64];
    z[62]=n<T>(623,6)*z[19] + z[62];
    z[62]=z[4]*z[62];
    z[59]=z[23] + z[59];
    z[65]=39*z[6];
    z[59]=z[59]*z[65];
    z[59]=n<T>(1883,6)*z[25] + z[59];
    z[59]=z[6]*z[59];
    z[59]=z[62] + z[59];
    z[59]=z[5]*z[59];
    z[57]=39*z[57] + z[59];
    z[57]=z[57]*z[35];
    z[59]=z[41]*z[2];
    z[59]=z[43] + n<T>(6953,10)*z[59];
    z[62]=z[3]*z[59];
    z[66]=z[30]*z[19];
    z[62]=z[62] - n<T>(5279,10)*z[66];
    z[62]=z[62]*z[45];
    z[66]=z[34]*z[25];
    z[52]=z[52] + n<T>(671,12)*z[66];
    z[52]=z[6]*z[52];
    z[52]=z[57] + z[62] + z[52];
    z[52]=z[5]*z[52];
    z[36]=n<T>(3229,20)*z[34] - z[36];
    z[36]=z[50]*z[36];
    z[36]=z[56] + z[36];
    z[36]=z[6]*z[4]*z[36];
    z[57]=z[4]*z[63];
    z[57]= - 2*z[19] + z[57];
    z[57]=z[57]*z[30];
    z[60]=2*z[25] + z[60];
    z[34]=z[60]*z[34];
    z[34]=z[57] + z[34];
    z[34]=z[5]*z[34];
    z[57]=z[25]*npow(z[6],3);
    z[60]=z[19]*npow(z[4],3);
    z[57]=z[57] + z[60];
    z[34]= - 4*z[57] + z[34];
    z[34]=z[34]*npow(z[5],2);
    z[57]=z[1]*z[57]*npow(z[5],3);
    z[34]=z[34] + z[57];
    z[34]=z[1]*z[34];
    z[34]=n<T>(39,5)*z[34] + z[36] + z[52];
    z[34]=z[1]*z[34];
    z[36]=n<T>(1,4)*z[3];
    z[52]=z[59]*z[36];
    z[57]= - n<T>(8717,15)*z[50] + 1243*z[19];
    z[30]=z[57]*z[30];
    z[30]=n<T>(1,8)*z[30] + z[56] - z[52];
    z[30]=z[4]*z[30];
    z[17]=z[34] + z[17] + z[30] + z[44];
    z[17]=z[1]*z[17];
    z[30]=n<T>(8717,2)*z[15] - n<T>(8717,2);
    z[30]=z[7]*z[30];
    z[30]=14617*z[2] + z[30];
    z[34]=z[2]*z[10];
    z[44]=static_cast<T>(6527)+ n<T>(6839,2)*z[34];
    z[44]=z[3]*z[44];
    z[30]=n<T>(1,3)*z[30] + z[44];
    z[30]=z[30]*z[45];
    z[19]=z[30] - n<T>(8717,24)*z[50] + 312*z[19];
    z[30]=n<T>(1,5)*z[4];
    z[19]=z[19]*z[30];
    z[44]=z[55]*z[15];
    z[55]=n<T>(1,2)*z[2];
    z[56]= - static_cast<T>(1)- n<T>(1,2)*z[34];
    z[56]=z[3]*z[56];
    z[56]=z[55] + z[56];
    z[56]=z[3]*z[56];
    z[43]= - z[10]*z[43];
    z[43]=z[43] + n<T>(6953,5)*z[56];
    z[36]=z[43]*z[36];
    z[43]=npow(z[11],3);
    z[56]=n<T>(1449,10)*z[43];
    z[19]=z[19] + z[36] - z[44] - n<T>(1049,30)*z[39] + z[56] - z[47];
    z[19]=z[4]*z[19];
    z[36]=z[2]*z[48];
    z[36]=z[36] + z[51] - n<T>(2729,3)*z[25];
    z[48]=z[34] - 1;
    z[51]=z[7]*z[8];
    z[57]=z[48] + z[51];
    z[57]=z[57]*z[4];
    z[59]=n<T>(3229,2)*z[2];
    z[60]=n<T>(3229,2)*z[15] - n<T>(5351,3);
    z[60]=z[7]*z[60];
    z[60]=z[59] + z[60];
    z[62]=z[16]*z[7];
    z[60]=n<T>(3229,10)*z[57] + n<T>(1,5)*z[60] + n<T>(203,6)*z[62];
    z[60]=z[6]*z[60];
    z[36]=n<T>(1,10)*z[36] + z[60];
    z[36]=z[6]*z[36];
    z[40]= - z[57] + z[40];
    z[40]=z[4]*z[40];
    z[57]= - z[8]*z[47];
    z[60]=z[34] + 1;
    z[60]=z[60]*z[2];
    z[63]= - n<T>(5617,2)*z[7] + 1493*z[60];
    z[63]=z[2]*z[63];
    z[40]=n<T>(8717,120)*z[40] + z[57] + n<T>(1,60)*z[63];
    z[40]=z[4]*z[40];
    z[57]=1564*z[32] + n<T>(1493,4)*z[39];
    z[63]= - static_cast<T>(701)+ 707*z[51];
    z[63]=z[63]*z[46];
    z[66]=z[7] + z[62];
    z[66]=z[3]*z[66];
    z[66]= - n<T>(2,3)*z[63] + n<T>(1113,8)*z[66];
    z[67]=n<T>(1,5)*z[3];
    z[66]=z[66]*z[67];
    z[36]=n<T>(1,2)*z[36] + z[40] + z[66] + n<T>(1,15)*z[57] + z[44];
    z[36]=z[6]*z[36];
    z[40]=2*z[27];
    z[44]=z[9]*z[8];
    z[57]=z[40] + z[44];
    z[66]=npow(z[8],3);
    z[68]=z[66]*z[3];
    z[69]=z[68] + z[57];
    z[69]=z[4]*z[69];
    z[29]=z[69] + z[29];
    z[29]=z[29]*z[64];
    z[61]=static_cast<T>(1)+ n<T>(1,2)*z[61];
    z[29]=n<T>(389,3)*z[61] + z[29];
    z[29]=z[4]*z[29];
    z[61]=z[27] + z[68];
    z[61]=z[6]*z[61];
    z[61]=z[28] + z[61];
    z[61]=z[61]*z[65];
    z[22]=n<T>(1649,6)*z[22] + z[61];
    z[22]=z[6]*z[22];
    z[61]=z[2]*z[8];
    z[68]= - 1649*z[51] - 389*z[61];
    z[68]=z[68]*z[33];
    z[37]=z[68] + n<T>(1649,2)*z[7] - z[37];
    z[22]=z[22] + n<T>(1,3)*z[37] + z[29];
    z[22]=z[5]*z[22];
    z[29]=z[9] + 3*z[8];
    z[37]= - 2*z[28] - z[29];
    z[37]=z[37]*z[49];
    z[49]=z[16] + 1;
    z[37]=z[37] + z[49];
    z[31]=z[37]*z[31];
    z[37]=3*z[16];
    z[68]=z[6]*z[28];
    z[68]= - z[37] + z[68];
    z[68]=z[68]*z[65];
    z[68]= - n<T>(1649,6)*z[3] + z[68];
    z[68]=z[6]*z[68];
    z[22]=z[22] + z[68] + n<T>(1649,6)*z[25] + z[31];
    z[22]=z[22]*z[35];
    z[31]=n<T>(121,4)*z[53];
    z[68]=static_cast<T>(1)- z[61];
    z[68]=z[68]*z[31];
    z[63]=n<T>(2,5)*z[63] + z[68];
    z[68]= - 1113*z[51] - 6953*z[61];
    z[68]=z[3]*z[68];
    z[68]= - z[42] + z[68];
    z[68]=z[3]*z[68];
    z[63]=n<T>(1,3)*z[63] + n<T>(1,40)*z[68];
    z[63]=z[3]*z[63];
    z[39]=n<T>(121,2)*z[39];
    z[68]=z[9]*z[39];
    z[68]= - n<T>(799,5)*z[53] + z[68];
    z[41]=z[8]*z[41];
    z[41]=n<T>(10981,3)*z[2] + n<T>(6953,4)*z[41];
    z[41]=z[41]*z[67];
    z[41]=n<T>(1,3)*z[68] + z[41];
    z[68]= - n<T>(5279,10)*z[16] - static_cast<T>(1243)- n<T>(6839,10)*z[15];
    z[68]=z[68]*z[45];
    z[69]= - 312*z[2] - n<T>(9959,8)*z[3];
    z[68]=n<T>(1,5)*z[69] + z[68];
    z[68]=z[4]*z[68];
    z[41]=n<T>(1,2)*z[41] + z[68];
    z[41]=z[4]*z[41];
    z[32]=z[9]*z[32];
    z[32]=n<T>(13019,4)*z[46] - 1414*z[32];
    z[49]=z[3]*z[49];
    z[49]=283*z[7] + 1113*z[49];
    z[49]=z[3]*z[49];
    z[32]=n<T>(1,3)*z[32] + n<T>(1,8)*z[49];
    z[49]=312*z[7] - n<T>(2729,12)*z[3];
    z[23]=n<T>(1,5)*z[49] + n<T>(203,12)*z[23];
    z[23]=z[6]*z[23];
    z[23]=n<T>(1,5)*z[32] + z[23];
    z[23]=z[6]*z[23];
    z[32]= - z[38] - z[39];
    z[22]=z[22] + z[23] + z[41] + n<T>(1,3)*z[32] + z[63];
    z[22]=z[5]*z[22];
    z[17]=z[17] + z[22] + z[36] + z[52] + z[19];
    z[17]=z[1]*z[17];
    z[19]= - z[56]*z[21];
    z[21]=npow(z[11],2);
    z[22]= - n<T>(837,10)*z[21] + 10*z[46];
    z[22]=z[7]*z[22];
    z[19]= - n<T>(379,5)*z[54] + z[22] + z[19];
    z[19]=z[9]*z[19];
    z[22]= - static_cast<T>(27043)- 32659*z[34];
    z[22]=z[22]*z[33];
    z[22]= - 8717*z[7] + z[22];
    z[23]=z[20] - z[51];
    z[23]=static_cast<T>(14617)+ n<T>(8717,4)*z[23];
    z[23]=z[4]*z[23];
    z[22]=n<T>(1,2)*z[22] + z[23];
    z[22]=z[4]*z[22];
    z[23]=n<T>(1449,2)*z[43];
    z[32]=z[23]*z[8];
    z[36]= - n<T>(7064,3)*z[21] - z[32];
    z[38]= - n<T>(4989,20)*z[7] - n<T>(295,3)*z[2];
    z[38]=z[38]*z[55];
    z[39]=z[46]*z[8];
    z[41]= - n<T>(16477,40)*z[11] - 10*z[39];
    z[41]=z[7]*z[41];
    z[49]= - 35113*z[2] - n<T>(7013,2)*z[3];
    z[49]=z[3]*z[49];
    z[19]=n<T>(1,30)*z[22] + n<T>(1,60)*z[49] + z[19] + z[38] + n<T>(1,5)*z[36] + 
    z[41];
    z[19]=z[4]*z[19];
    z[22]=n<T>(2717,3)*z[21];
    z[36]= - n<T>(3437,3)*z[46] + z[22] + z[32];
    z[22]= - n<T>(1493,12)*z[53] - z[22] + n<T>(259,4)*z[46];
    z[22]=z[2]*z[22];
    z[38]=z[15] - 1;
    z[41]=z[23]*z[38];
    z[22]=z[22] + z[41];
    z[41]=n<T>(1,5)*z[9];
    z[22]=z[22]*z[41];
    z[49]= - z[59] + n<T>(1969,3)*z[62];
    z[48]= - z[15] - z[48];
    z[48]=z[6]*z[48];
    z[52]=z[51] - 1;
    z[52]=z[4]*z[52];
    z[48]=n<T>(3229,20)*z[48] + n<T>(1,5)*z[49] + n<T>(2483,6)*z[52];
    z[48]=z[6]*z[48];
    z[49]=n<T>(37991,15)*z[11] - 333*z[7];
    z[49]=n<T>(1,2)*z[49] - n<T>(1493,15)*z[60];
    z[49]=z[2]*z[49];
    z[52]= - n<T>(48749,2)*z[7] - 2821*z[2];
    z[54]=z[4]*z[34];
    z[52]=n<T>(1,3)*z[52] + n<T>(2557,4)*z[54];
    z[52]=z[4]*z[52];
    z[22]=z[48] + n<T>(1,10)*z[52] + n<T>(1691,4)*z[25] + z[22] + n<T>(1,5)*z[36] + 
   n<T>(1,4)*z[49];
    z[22]=z[6]*z[22];
    z[25]=npow(z[13],2);
    z[36]= - 3649*z[25] + n<T>(12971,10)*z[46];
    z[48]=npow(z[13],3);
    z[49]=z[48]*z[9];
    z[52]=n<T>(1949,5)*z[49];
    z[36]=n<T>(1,3)*z[36] - z[52];
    z[54]=n<T>(1,2)*z[9];
    z[36]=z[36]*z[54];
    z[55]= - static_cast<T>(963)- n<T>(5801,20)*z[16];
    z[55]=z[55]*z[33];
    z[56]=117*z[16];
    z[54]=z[54] + z[8];
    z[59]=z[6]*z[54];
    z[59]=n<T>(547,6)*z[59] + n<T>(547,6) + z[56];
    z[62]=n<T>(1,5)*z[6];
    z[59]=z[59]*z[62];
    z[63]= - 14068*z[13] + n<T>(15181,4)*z[7];
    z[36]=z[59] + z[55] + n<T>(1,15)*z[63] + z[36];
    z[36]=z[6]*z[36];
    z[55]=z[9]*z[27];
    z[55]=2*z[66] + z[55];
    z[55]=z[4]*z[55];
    z[55]=2*z[57] + z[55];
    z[55]=z[55]*z[64];
    z[55]=n<T>(623,3)*z[54] + z[55];
    z[55]=z[55]*z[30];
    z[29]=z[29]*z[9];
    z[57]=z[40] + z[29];
    z[59]=4*z[8];
    z[63]=z[59] + z[9];
    z[63]=z[63]*z[41];
    z[63]=z[27] + z[63];
    z[63]=z[9]*z[63];
    z[63]=n<T>(2,5)*z[66] + z[63];
    z[63]=z[6]*z[63];
    z[57]=n<T>(2,5)*z[57] + z[63];
    z[57]=z[57]*z[65];
    z[54]=n<T>(1883,15)*z[54] + z[57];
    z[54]=z[6]*z[54];
    z[54]=z[55] + z[54];
    z[54]=z[5]*z[54];
    z[55]=463*z[51] + 487*z[61];
    z[55]=z[3]*z[55];
    z[55]=487*z[2] + z[55];
    z[57]=3*z[27];
    z[63]= - z[57] - z[44];
    z[18]=z[63]*z[18];
    z[18]=z[18] + n<T>(3,5)*z[28] - z[8] - n<T>(2,5)*z[9];
    z[18]=z[4]*z[18];
    z[37]=static_cast<T>(1)+ z[37];
    z[18]=n<T>(1,5)*z[37] + z[18];
    z[18]=z[4]*z[18];
    z[18]=n<T>(1,60)*z[55] + 3*z[18];
    z[28]=z[8] + z[28];
    z[28]=z[6]*z[28];
    z[28]=117*z[28] - n<T>(1649,3) + z[56];
    z[28]=z[28]*z[62];
    z[18]=z[54] + 13*z[18] + z[28];
    z[18]=z[5]*z[18];
    z[28]=npow(z[14],2);
    z[37]=59*z[28];
    z[54]=npow(z[14],3);
    z[55]=n<T>(694,3)*z[54];
    z[56]=z[9]*z[55];
    z[56]=z[56] + z[37] - n<T>(799,6)*z[53];
    z[56]=z[9]*z[56];
    z[62]=static_cast<T>(10507)+ n<T>(7013,2)*z[16];
    z[62]=z[3]*z[62];
    z[56]=n<T>(1,12)*z[62] + z[56] - n<T>(21801,8)*z[14] + n<T>(2128,3)*z[2];
    z[62]= - 1243*z[8] - n<T>(6839,10)*z[9];
    z[45]=z[62]*z[45];
    z[62]= - n<T>(7463,4) - 351*z[16];
    z[45]=n<T>(1,5)*z[62] + z[45];
    z[45]=z[4]*z[45];
    z[45]=n<T>(1,5)*z[56] + z[45];
    z[45]=z[4]*z[45];
    z[46]=n<T>(763,2)*z[46] + 47*z[53];
    z[56]= - n<T>(28013,24) - 4*z[51];
    z[56]=z[7]*z[56];
    z[56]=z[56] + n<T>(21151,24)*z[2];
    z[56]=z[3]*z[56];
    z[46]=n<T>(17,6)*z[46] + z[56];
    z[18]=z[18] + z[36] + n<T>(1,5)*z[46] + z[45];
    z[18]=z[5]*z[18];
    z[36]=z[10] - z[12];
    z[45]=z[13]*z[36];
    z[45]= - n<T>(2393,3) - n<T>(1949,2)*z[45];
    z[45]=z[45]*z[25];
    z[46]=npow(z[10],2);
    z[56]=z[46]*z[13];
    z[62]=n<T>(1949,2)*z[56];
    z[63]=n<T>(2393,3)*z[10] + z[62];
    z[63]=z[13]*z[63];
    z[63]=n<T>(3587,2) + z[63];
    z[63]=z[13]*z[63];
    z[68]=z[8] - z[10];
    z[69]=z[7]*z[68];
    z[69]=static_cast<T>(701)- 707*z[69];
    z[69]=z[7]*z[69];
    z[63]=z[63] + n<T>(2,3)*z[69];
    z[63]=z[7]*z[63];
    z[45]=z[45] + z[63];
    z[63]=1661*z[28];
    z[69]=z[54]*z[8];
    z[70]=347*z[69];
    z[71]=z[63] - z[70];
    z[71]=z[71]*z[24];
    z[71]=n<T>(6323,8)*z[14] + z[71];
    z[72]=n<T>(121,4)*z[2];
    z[73]=z[8] + z[10];
    z[74]=z[73]*z[2];
    z[75]= - static_cast<T>(1)+ z[74];
    z[75]=z[75]*z[72];
    z[71]=n<T>(1,5)*z[71] + z[75];
    z[75]=n<T>(1,3)*z[2];
    z[71]=z[71]*z[75];
    z[76]=n<T>(1113,2)*z[7];
    z[68]=z[68]*z[76];
    z[68]=n<T>(6953,2)*z[74] + static_cast<T>(4033)+ z[68];
    z[68]=z[3]*z[68];
    z[42]=z[68] + z[42] - n<T>(6953,2)*z[2];
    z[42]=z[3]*z[42];
    z[42]=n<T>(1,20)*z[42] + n<T>(1,5)*z[45] + z[71];
    z[42]=z[3]*z[42];
    z[45]=z[63] - 694*z[69];
    z[31]=n<T>(2,5)*z[45] + z[31];
    z[31]=z[31]*z[75];
    z[45]=z[54]*z[15];
    z[17]=z[17] + z[18] + z[22] + z[19] + z[42] - n<T>(694,15)*z[45] + z[31]
    - n<T>(1949,10)*z[48] - z[47];
    z[17]=z[1]*z[17];
    z[18]=1449*z[43];
    z[19]= - z[20]*z[18];
    z[22]=z[21]*z[7];
    z[19]=z[19] + n<T>(11971,6)*z[22] - z[23] - n<T>(1388,3)*z[54];
    z[19]=z[19]*z[41];
    z[31]= - n<T>(131,5)*z[11] + 5*z[39];
    z[31]=z[31]*z[58];
    z[39]=z[43]*z[8];
    z[42]=n<T>(1449,5)*z[39];
    z[19]=z[19] - n<T>(5177,40)*z[50] + z[31] - z[42] - n<T>(1331,10)*z[21] + 4*
    z[28];
    z[19]=z[9]*z[19];
    z[31]=n<T>(1,10)*z[3];
    z[45]= - static_cast<T>(413)+ n<T>(7787,12)*z[34];
    z[45]=z[45]*z[31];
    z[47]=z[4]*z[9];
    z[50]=n<T>(8717,6)*z[47] - n<T>(8717,6);
    z[50]=z[51]*z[50];
    z[50]=n<T>(8717,6)*z[20] + static_cast<T>(3023)+ z[50];
    z[50]=z[4]*z[50];
    z[63]=z[21]*z[8];
    z[68]=n<T>(727,3)*z[63];
    z[71]=n<T>(2471,3)*z[11] - n<T>(7533,10)*z[14];
    z[19]=n<T>(1,20)*z[50] + z[45] + z[19] - n<T>(9763,120)*z[2] - n<T>(76051,120)*
    z[7] + n<T>(1,2)*z[71] + z[68];
    z[19]=z[4]*z[19];
    z[40]= - z[40]*z[47];
    z[40]=z[40] + z[27] - 4*z[44];
    z[40]=z[4]*z[40];
    z[45]=z[8] - z[9];
    z[40]=2*z[45] + z[40];
    z[40]=z[40]*z[64];
    z[40]=n<T>(2567,6) + z[40];
    z[26]=z[26]*z[9];
    z[26]=z[26] + z[27];
    z[45]=z[6]*z[9];
    z[50]= - z[26]*z[45];
    z[71]=npow(z[9],2);
    z[27]=z[50] + z[27] - z[71];
    z[27]=z[6]*z[27];
    z[27]=n<T>(39,5)*z[27] + n<T>(78,5)*z[8] - n<T>(283,6)*z[9];
    z[27]=z[6]*z[27];
    z[26]=z[9]*z[26];
    z[29]=z[57] + z[29];
    z[29]=z[9]*z[29];
    z[29]=z[66] + z[29];
    z[29]=z[29]*z[45];
    z[26]=3*z[26] + z[29];
    z[26]=z[26]*z[65];
    z[29]=z[9] + z[8];
    z[50]=z[9]*z[29];
    z[26]=n<T>(2351,6)*z[50] + z[26];
    z[26]=z[6]*z[26];
    z[47]=z[66]*z[47];
    z[50]=z[9]*z[57];
    z[47]=z[50] + z[47];
    z[47]=z[47]*z[64];
    z[47]=n<T>(1091,6)*z[44] + z[47];
    z[47]=z[4]*z[47];
    z[26]=z[26] + n<T>(1253,3)*z[9] + z[47];
    z[26]=z[26]*z[35];
    z[26]=z[26] + n<T>(1,5)*z[40] + z[27];
    z[26]=z[5]*z[26];
    z[27]=1949*z[48];
    z[35]= - z[55] - z[27];
    z[35]=z[9]*z[35];
    z[35]=z[35] - z[37] - n<T>(7901,2)*z[25];
    z[35]=z[35]*z[41];
    z[37]=z[54]*z[9];
    z[40]=183*z[28] + n<T>(1388,15)*z[37];
    z[40]=z[9]*z[40];
    z[47]=n<T>(191,5)*z[14] + n<T>(61,12)*z[2];
    z[40]=n<T>(1,2)*z[47] + z[40];
    z[40]=z[9]*z[40];
    z[44]=z[4]*z[44];
    z[44]=z[44] + z[9];
    z[44]= - 78*z[8] - n<T>(6839,8)*z[44];
    z[30]=z[44]*z[30];
    z[30]=z[30] - n<T>(7439,120)*z[16] + n<T>(2726,5) + z[40];
    z[30]=z[4]*z[30];
    z[40]= - n<T>(6263,6)*z[25] - z[52];
    z[40]=z[9]*z[40];
    z[44]= - 56333*z[13] - n<T>(19591,2)*z[7];
    z[40]=n<T>(1,60)*z[44] + z[40];
    z[40]=z[9]*z[40];
    z[29]=z[29]*z[45];
    z[29]=z[9] + z[29];
    z[29]=z[6]*z[29];
    z[16]=n<T>(547,60)*z[29] - n<T>(571,2)*z[16] - n<T>(53143,60) + z[40];
    z[16]=z[6]*z[16];
    z[29]= - 25453*z[61] - static_cast<T>(5489)- 10757*z[51];
    z[29]=z[3]*z[29];
    z[40]=4063*z[14] - n<T>(33893,4)*z[13];
    z[40]=n<T>(337,10)*z[2] + n<T>(1,5)*z[40] - n<T>(199,8)*z[7];
    z[16]=z[26] + z[16] + z[30] + n<T>(1,120)*z[29] + n<T>(1,3)*z[40] + z[35];
    z[16]=z[5]*z[16];
    z[26]=npow(z[10],3);
    z[29]=1949*z[13];
    z[30]=z[26]*z[29];
    z[35]= - n<T>(7163,3)*z[46] - z[30];
    z[35]=z[13]*z[35];
    z[35]= - n<T>(8777,4)*z[10] + z[35];
    z[35]=z[13]*z[35];
    z[40]=z[8]*z[10];
    z[40]=z[40] + z[46];
    z[44]=z[7]*z[40];
    z[44]= - n<T>(707,3)*z[44] + n<T>(713,3)*z[10] + z[24];
    z[44]=z[44]*z[58];
    z[35]=z[44] + n<T>(11619,8) + z[35];
    z[35]=z[7]*z[35];
    z[44]=z[10]*z[36]*z[29];
    z[47]=2729*z[12];
    z[50]= - z[47] + 7163*z[10];
    z[44]=n<T>(1,3)*z[50] + z[44];
    z[44]=z[13]*z[44];
    z[44]=n<T>(6367,4) + z[44];
    z[44]=z[13]*z[44];
    z[50]=1388*z[69];
    z[52]=4739*z[28] - z[50];
    z[52]=z[8]*z[52];
    z[35]=z[35] + n<T>(1,3)*z[52] + n<T>(581,12)*z[14] + z[44];
    z[44]= - 1244*z[28] + z[70];
    z[44]=z[44]*z[59];
    z[44]= - 1751*z[14] + z[44];
    z[44]=z[8]*z[44];
    z[44]=n<T>(24641,8) + z[44];
    z[52]=z[34]*z[8];
    z[54]= - z[10] + z[52];
    z[54]=z[54]*z[72];
    z[44]=n<T>(1,5)*z[44] + z[54];
    z[44]=z[44]*z[75];
    z[40]=z[40]*z[76];
    z[40]=n<T>(6953,2)*z[52] - 4033*z[10] + z[40];
    z[33]=z[40]*z[33];
    z[40]=z[7]*z[73];
    z[33]=z[33] - n<T>(6953,4)*z[61] - n<T>(8647,3) - n<T>(1113,4)*z[40];
    z[31]=z[33]*z[31];
    z[31]=z[31] + n<T>(1,5)*z[35] + z[44];
    z[31]=z[3]*z[31];
    z[33]=z[18]*z[38];
    z[35]=n<T>(1493,4)*z[53];
    z[38]= - 7093*z[21] - z[35];
    z[38]=z[38]*z[75];
    z[40]=n<T>(1949,2)*z[48];
    z[33]=z[38] + z[40] + z[33];
    z[33]=z[33]*z[41];
    z[38]=n<T>(173,5)*z[21] + n<T>(89,2)*z[25];
    z[44]=n<T>(1493,6)*z[60] + n<T>(2507,3)*z[11] + n<T>(2073,4)*z[7];
    z[44]=z[2]*z[44];
    z[33]=z[33] + n<T>(1,10)*z[44] + n<T>(41,3)*z[38] + z[42];
    z[33]=z[9]*z[33];
    z[38]=z[15] - z[34];
    z[44]=n<T>(20521,3) - 3229*z[38];
    z[38]=n<T>(457,3) - n<T>(3229,20)*z[38];
    z[38]=z[38]*z[45];
    z[38]=n<T>(1,20)*z[44] + z[38];
    z[38]=z[6]*z[38];
    z[44]=static_cast<T>(459)+ n<T>(3229,6)*z[51];
    z[44]=z[44]*z[67];
    z[45]= - n<T>(1403,15) + 717*z[51];
    z[45]=n<T>(1,2)*z[45] - n<T>(247,15)*z[34];
    z[45]=z[4]*z[45];
    z[48]=n<T>(623,20)*z[2] + n<T>(6992,5)*z[7] - 727*z[63] - 872*z[11] + n<T>(12589,10)*z[13];
    z[33]=z[38] + z[45] + z[44] + n<T>(1,3)*z[48] + z[33];
    z[33]=z[6]*z[33];
    z[35]= - z[35] + z[50] - 6352*z[21] - 1417*z[28];
    z[35]=z[35]*z[75];
    z[38]=z[40] + z[55];
    z[40]=z[43]*z[9];
    z[43]=n<T>(1,2)*z[7];
    z[44]=z[43] + z[2];
    z[44]=z[44]*z[40];
    z[35]=1449*z[44] + z[35] + n<T>(837,2)*z[22] + z[38];
    z[35]=z[35]*z[41];
    z[44]=z[46]*z[29];
    z[44]=n<T>(5122,3)*z[10] + z[44];
    z[44]=z[13]*z[44];
    z[44]=n<T>(9529,12) + z[44];
    z[44]=z[13]*z[44];
    z[45]=782*z[10] + 707*z[8];
    z[45]=z[45]*z[58];
    z[45]= - static_cast<T>(1873)+ z[45];
    z[45]=z[7]*z[45];
    z[44]=n<T>(1,3)*z[45] - 429*z[11] + z[44];
    z[44]=z[7]*z[44];
    z[45]=z[13]*z[12];
    z[48]=n<T>(1648,3) - n<T>(1949,10)*z[45];
    z[48]=z[48]*z[25];
    z[52]= - 2131*z[28] + n<T>(2776,3)*z[69];
    z[52]=z[8]*z[52];
    z[52]=n<T>(1421,6)*z[7] + z[52] + n<T>(1643,3)*z[11] - n<T>(4623,4)*z[14];
    z[53]=n<T>(1493,5)*z[10] - 121*z[8];
    z[53]=z[2]*z[53];
    z[53]=n<T>(71,5) + n<T>(1,12)*z[53];
    z[53]=z[2]*z[53];
    z[52]=n<T>(1,5)*z[52] + z[53];
    z[52]=z[2]*z[52];
    z[53]=n<T>(694,15)*z[69];
    z[16]=z[17] + z[16] + z[33] + z[19] + z[31] + z[35] + z[52] + n<T>(1,5)*
    z[44] - z[53] - n<T>(517,15)*z[28] + z[48];
    z[16]=z[1]*z[16];
    z[17]=n<T>(4376,3)*z[21];
    z[19]= - z[17] - z[32];
    z[19]=z[2]*z[19];
    z[19]=z[38] + z[19];
    z[31]= - n<T>(2027,3)*z[21] + z[42];
    z[31]=z[31]*z[43];
    z[32]=z[7] + z[2];
    z[32]=z[32]*z[40];
    z[19]=n<T>(1449,10)*z[32] + z[31] + n<T>(1,5)*z[19];
    z[19]=z[9]*z[19];
    z[31]=n<T>(1,5)*z[13];
    z[32]=n<T>(2729,3)*z[10] + z[62];
    z[32]=z[13]*z[32];
    z[32]=n<T>(8721,8) + z[32];
    z[32]=z[32]*z[31];
    z[33]=n<T>(1049,2)*z[11];
    z[32]= - z[68] + z[33] + z[32];
    z[32]=z[7]*z[32];
    z[35]= - n<T>(694,5)*z[69] + 727*z[21] + n<T>(1417,5)*z[28];
    z[35]=z[8]*z[35];
    z[40]= - 239*z[11] + n<T>(23209,10)*z[14];
    z[35]=n<T>(1,2)*z[40] + z[35];
    z[35]=z[35]*z[75];
    z[40]=n<T>(2,3)*z[28];
    z[42]=z[14]*z[12];
    z[43]=static_cast<T>(118)- n<T>(347,5)*z[42];
    z[43]=z[43]*z[40];
    z[44]=z[10] + z[12];
    z[48]=z[44]*z[29];
    z[48]=n<T>(7223,3) + z[48];
    z[48]=z[48]*z[25];
    z[19]=z[19] + z[35] + z[32] - z[53] + z[43] + n<T>(1,10)*z[48];
    z[19]=z[9]*z[19];
    z[32]=z[36]*z[56];
    z[35]=n<T>(2729,15)*z[12] - 318*z[10];
    z[35]=z[10]*z[35];
    z[32]=z[35] - n<T>(1949,10)*z[32];
    z[32]=z[13]*z[32];
    z[35]=8721*z[12] - n<T>(40243,3)*z[10];
    z[32]=n<T>(1,40)*z[35] + z[32];
    z[32]=z[13]*z[32];
    z[35]= - static_cast<T>(3071)- 694*z[42];
    z[35]=z[35]*z[28];
    z[35]=z[35] + z[50];
    z[35]=z[8]*z[35];
    z[36]= - static_cast<T>(19334)+ 1417*z[42];
    z[36]=z[14]*z[36];
    z[35]=z[36] + z[35];
    z[35]=z[8]*z[35];
    z[36]=z[13]*npow(z[10],4);
    z[26]=318*z[26] + n<T>(1949,10)*z[36];
    z[26]=z[13]*z[26];
    z[26]=n<T>(48407,120)*z[46] + z[26];
    z[26]=z[13]*z[26];
    z[26]=n<T>(601,40)*z[8] + n<T>(5323,60)*z[10] + z[26];
    z[26]=z[7]*z[26];
    z[36]=827*z[28] - z[70];
    z[24]=z[36]*z[24];
    z[24]=n<T>(54601,4)*z[14] + z[24];
    z[36]=n<T>(1,5)*z[8];
    z[24]=z[24]*z[36];
    z[24]=n<T>(9203,8) + z[24];
    z[24]=z[8]*z[24];
    z[24]= - n<T>(15097,40)*z[10] + z[24];
    z[24]=z[24]*z[75];
    z[43]= - static_cast<T>(845)+ n<T>(23209,20)*z[42];
    z[24]=z[24] + z[26] + n<T>(1,15)*z[35] + n<T>(1,3)*z[43] + z[32];
    z[24]=z[3]*z[24];
    z[18]= - z[18] + z[27];
    z[15]=z[15]*z[23];
    z[17]= - z[2]*z[17];
    z[15]=z[15] + n<T>(1,2)*z[18] + z[17];
    z[15]=z[15]*z[41];
    z[17]=z[13]*z[10];
    z[18]=n<T>(1307,3) + n<T>(1949,10)*z[17];
    z[18]=z[18]*z[25];
    z[26]=z[2]*z[11];
    z[15]=z[15] + n<T>(29789,120)*z[26] + n<T>(1449,10)*z[39] + n<T>(4376,15)*z[21]
    + z[18];
    z[15]=z[9]*z[15];
    z[18]=static_cast<T>(1867)+ n<T>(3806,3)*z[17];
    z[18]=z[18]*z[31];
    z[15]=z[15] + n<T>(2753,30)*z[2] - z[68] + n<T>(239,6)*z[11] + z[18];
    z[15]=z[9]*z[15];
    z[18]=z[8]*z[11];
    z[26]=n<T>(1283,2) + 1903*z[17];
    z[26]= - n<T>(169,10)*z[34] + n<T>(1,5)*z[26] + n<T>(727,2)*z[18];
    z[15]=n<T>(1,3)*z[26] + z[15];
    z[15]=z[6]*z[15];
    z[26]= - static_cast<T>(937)+ 347*z[42];
    z[26]=z[26]*z[40];
    z[27]= - z[12]*z[29];
    z[27]= - n<T>(7223,3) + z[27];
    z[27]=z[27]*z[25];
    z[29]= - z[9]*z[38];
    z[26]=z[29] + z[26] + n<T>(1,2)*z[27];
    z[26]=z[26]*z[41];
    z[27]= - n<T>(3037,60) + 187*z[42];
    z[27]=z[14]*z[27];
    z[29]= - n<T>(2261,8) - n<T>(2729,15)*z[45];
    z[29]=z[13]*z[29];
    z[26]=z[26] + z[27] + z[29];
    z[26]=z[9]*z[26];
    z[27]= - n<T>(81833,10) - 2953*z[42];
    z[27]=n<T>(1,3)*z[27] - n<T>(8721,10)*z[45];
    z[29]=428*z[28] + n<T>(347,3)*z[37];
    z[29]=z[9]*z[29];
    z[29]=n<T>(13783,12)*z[14] + 2*z[29];
    z[29]=z[29]*z[41];
    z[29]=n<T>(8779,24) + z[29];
    z[29]=z[9]*z[29];
    z[29]= - n<T>(14873,120)*z[8] + z[29];
    z[29]=z[4]*z[29];
    z[25]= - n<T>(1307,3)*z[25] - n<T>(1949,10)*z[49];
    z[25]=z[9]*z[25];
    z[25]= - n<T>(23657,40)*z[13] + z[25];
    z[25]=z[9]*z[25];
    z[25]= - n<T>(7939,15) + z[25];
    z[25]=z[9]*z[25];
    z[25]= - 203*z[8] + z[25];
    z[25]=z[6]*z[25];
    z[25]=z[25] + z[29] + n<T>(1,4)*z[27] + z[26];
    z[25]=z[5]*z[25];
    z[20]= - z[20] - 1;
    z[20]=z[23]*z[20];
    z[20]=n<T>(7241,3)*z[22] - z[55] + z[20];
    z[20]=z[20]*z[41];
    z[22]=n<T>(2027,2)*z[21] - n<T>(1874,5)*z[28];
    z[23]= - z[23] + z[55];
    z[23]=z[23]*z[36];
    z[26]=z[7]*z[11];
    z[20]=z[20] - n<T>(138049,120)*z[26] + n<T>(1,3)*z[22] + z[23];
    z[20]=z[9]*z[20];
    z[21]=n<T>(727,3)*z[21] - n<T>(79,5)*z[28];
    z[21]=z[8]*z[21];
    z[20]=z[20] + n<T>(7783,15)*z[7] + z[21] - z[33] - n<T>(2458,5)*z[14];
    z[20]=z[9]*z[20];
    z[21]= - n<T>(727,3)*z[11] - n<T>(79,5)*z[14];
    z[21]=z[8]*z[21];
    z[21]=n<T>(5881,15)*z[51] - n<T>(4807,20) + z[21];
    z[20]=n<T>(1,2)*z[21] + z[20];
    z[20]=z[4]*z[20];
    z[17]=z[44]*z[17];
    z[21]=z[47] + n<T>(7223,2)*z[10];
    z[17]=n<T>(1,3)*z[21] + n<T>(1949,2)*z[17];
    z[17]=z[17]*z[31];
    z[17]=n<T>(2261,8) + z[17];
    z[17]=z[13]*z[17];
    z[21]= - static_cast<T>(81)+ n<T>(347,3)*z[42];
    z[21]=z[21]*z[28];
    z[21]=z[21] + n<T>(347,3)*z[69];
    z[21]=z[8]*z[21];
    z[22]= - n<T>(7499,3)*z[46] - z[30];
    z[22]=z[13]*z[22];
    z[22]= - n<T>(33203,12)*z[10] + z[22];
    z[22]=z[13]*z[22];
    z[22]= - n<T>(2141,12) + z[22];
    z[18]=n<T>(1,5)*z[22] + n<T>(727,6)*z[18];
    z[18]=z[7]*z[18];
    z[22]=3071*z[28] - z[50];
    z[22]=z[22]*z[36];
    z[22]=z[22] + n<T>(19334,5)*z[14] - n<T>(727,2)*z[11];
    z[22]=z[8]*z[22];
    z[22]=n<T>(19927,20) + z[22];
    z[22]=z[22]*z[75];
    z[23]= - n<T>(561,4) - n<T>(2111,3)*z[42];
    z[23]=z[14]*z[23];
    z[15]=z[16] + z[25] + z[15] + z[20] + z[24] + z[19] + z[22] + z[18]
    + n<T>(2,5)*z[21] + n<T>(1,5)*z[23] + z[17];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r937(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r937(const std::array<dd_real,31>&);
#endif
