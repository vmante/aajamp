#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r104(const std::array<T,31>& k) {
  T z[51];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[14];
    z[5]=k[5];
    z[6]=k[8];
    z[7]=k[10];
    z[8]=k[12];
    z[9]=k[13];
    z[10]=k[3];
    z[11]=k[4];
    z[12]=k[15];
    z[13]=k[9];
    z[14]=z[8]*z[10];
    z[15]=z[7]*z[10];
    z[16]=z[14] - z[15];
    z[17]=z[9] + z[4];
    z[18]=n<T>(1,2)*z[5];
    z[19]= - z[17]*z[18];
    z[20]=n<T>(1,2)*z[4];
    z[21]=z[20] + z[9];
    z[21]=z[21]*z[5];
    z[22]=z[21] + n<T>(1,2);
    z[22]=z[22]*z[5];
    z[22]=z[22] + n<T>(1,2)*z[10];
    z[23]=n<T>(1,2)*z[8];
    z[24]=z[7] + z[23];
    z[24]=z[24]*npow(z[10],2);
    z[25]=z[24] + z[22];
    z[25]=z[3]*z[25];
    z[19]=n<T>(1,2)*z[25] + z[19] - z[16];
    z[19]=z[3]*z[19];
    z[25]=npow(z[12],2);
    z[26]=z[25]*z[11];
    z[27]=z[26]*z[7];
    z[28]=n<T>(1,2)*z[27];
    z[29]=z[7]*z[4];
    z[30]=z[28] - n<T>(1,4)*z[29] + z[25];
    z[30]=z[11]*z[30];
    z[31]=npow(z[6],2);
    z[32]=z[31]*z[5];
    z[33]=z[32] + z[9];
    z[33]=z[23]*z[33];
    z[33]=z[25] + z[33];
    z[33]=z[33]*z[18];
    z[19]=z[19] + z[30] + z[33];
    z[19]=z[2]*z[19];
    z[21]=z[21] + n<T>(1,4)*z[14] + n<T>(3,2) + z[15];
    z[21]=z[3]*z[21];
    z[21]=z[21] - n<T>(3,4)*z[8] + z[7] - z[9];
    z[21]=z[3]*z[21];
    z[30]=n<T>(3,4)*z[29];
    z[33]=z[32]*z[8];
    z[34]=z[8]*z[9];
    z[19]=z[19] + z[21] - n<T>(3,4)*z[33] - z[30] - z[34];
    z[19]=z[2]*z[19];
    z[21]=z[20]*z[25];
    z[35]=z[31]*z[9];
    z[21]=z[21] + z[35];
    z[36]=z[21]*z[18];
    z[37]=z[12]*z[20];
    z[38]=n<T>(1,2)*z[6];
    z[39]=z[9] - z[38];
    z[39]=z[6]*z[39];
    z[36]=z[36] + z[37] + z[39];
    z[36]=z[5]*z[36];
    z[37]=z[34] + z[29];
    z[39]=z[11]*z[37];
    z[40]=3*z[7];
    z[39]=z[39] + z[40] - z[8];
    z[41]=z[6]*z[11];
    z[42]=z[41] + 3;
    z[43]=z[6]*z[42];
    z[39]=n<T>(1,4)*z[39] + z[43];
    z[36]=n<T>(1,2)*z[39] + z[36];
    z[36]=z[3]*z[36];
    z[39]= - z[18]*z[35];
    z[43]=z[38] + z[9];
    z[43]=z[43]*z[6];
    z[36]=z[36] + z[39] - n<T>(3,8)*z[37] - z[43];
    z[36]=z[3]*z[36];
    z[39]=npow(z[2],2);
    z[44]=z[1]*z[39]*z[20];
    z[44]=z[44] - z[31];
    z[44]=z[44]*npow(z[3],2);
    z[30]=z[34]*z[30];
    z[45]=z[5]*z[4];
    z[45]= - n<T>(1,2) + z[45];
    z[45]=z[3]*z[45];
    z[45]= - z[4] + z[45];
    z[39]=z[3]*z[45]*z[39];
    z[30]=z[39] + z[30] + z[44];
    z[30]=z[1]*z[30];
    z[39]=z[9]*z[7];
    z[44]=2*z[13];
    z[45]=z[44] + n<T>(3,4)*z[4];
    z[45]=z[45]*z[39];
    z[46]=7*z[4] + n<T>(11,2)*z[7];
    z[46]=z[9]*z[46];
    z[46]=n<T>(3,2)*z[29] + z[46];
    z[47]=n<T>(1,4)*z[8];
    z[46]=z[46]*z[47];
    z[48]=z[31]*z[23];
    z[19]=n<T>(1,2)*z[30] + z[19] + z[36] + z[48] + z[45] + z[46];
    z[19]=z[1]*z[19];
    z[30]=z[7]*z[20];
    z[36]=n<T>(1,2)*z[12];
    z[45]=z[7] - z[36];
    z[45]=z[12]*z[45];
    z[30]=z[27] + z[30] + z[45];
    z[30]=z[11]*z[30];
    z[34]= - 3*z[25] - z[34];
    z[45]=n<T>(7,2)*z[8] - z[6];
    z[45]=z[6]*z[45];
    z[34]= - n<T>(7,2)*z[33] + n<T>(1,2)*z[34] + z[45];
    z[34]=z[5]*z[34];
    z[45]=n<T>(3,2)*z[6];
    z[46]= - 3*z[4] + z[7];
    z[30]=z[34] - z[45] + z[30] + n<T>(9,2)*z[8] - z[9] + n<T>(1,2)*z[46] - 
    z[12];
    z[34]=3*z[9];
    z[46]= - z[4] - z[34];
    z[46]=z[46]*z[18];
    z[22]=z[3]*z[22];
    z[22]=z[22] + z[46] - n<T>(3,4)*z[14] + n<T>(1,4) - z[15];
    z[22]=z[3]*z[22];
    z[46]=z[23]*z[32];
    z[48]= - z[8] + z[38];
    z[48]=z[6]*z[48];
    z[46]=z[48] + z[46];
    z[46]=z[5]*z[46];
    z[48]=z[47] + z[12];
    z[46]=z[46] - z[38] - z[26] - z[48];
    z[46]=z[5]*z[46];
    z[49]=2*z[12];
    z[50]= - z[7] - z[12];
    z[50]=z[50]*z[49];
    z[27]=z[50] - z[27];
    z[27]=z[11]*z[27];
    z[27]=z[27] + n<T>(1,4)*z[7] - 3*z[12];
    z[27]=z[11]*z[27];
    z[50]= - z[3]*z[24];
    z[16]=z[50] + z[46] + z[27] + z[16];
    z[16]=z[2]*z[16];
    z[16]=z[16] + n<T>(1,2)*z[30] + z[22];
    z[16]=z[2]*z[16];
    z[22]=n<T>(1,4)*z[11];
    z[27]=z[37]*z[22];
    z[30]=z[41] + 1;
    z[37]=z[30]*z[6];
    z[40]= - 5*z[4] + z[40];
    z[27]=z[37] + z[27] - n<T>(5,4)*z[9] + n<T>(1,4)*z[40] + z[48];
    z[34]= - z[34] - n<T>(5,2)*z[6];
    z[34]=z[34]*z[38];
    z[40]=z[25]*z[4];
    z[46]= - z[40] - 7*z[35];
    z[48]=n<T>(1,4)*z[5];
    z[46]=z[46]*z[48];
    z[50]= - n<T>(1,4)*z[4] + z[12];
    z[50]=z[12]*z[50];
    z[34]=z[46] + z[50] + z[34];
    z[34]=z[5]*z[34];
    z[21]=z[21]*z[5];
    z[46]=z[4] - z[12];
    z[46]=z[46]*z[36];
    z[43]=z[21] + z[46] + z[43];
    z[43]=z[5]*z[43];
    z[46]=static_cast<T>(2)+ z[41];
    z[46]=z[6]*z[46];
    z[36]=z[43] - z[36] + z[46];
    z[36]=z[5]*z[36];
    z[43]=z[17]*z[22];
    z[43]=static_cast<T>(1)+ z[43];
    z[36]=n<T>(1,2)*z[43] + z[36];
    z[36]=z[3]*z[36];
    z[27]=z[36] + n<T>(1,2)*z[27] + z[34];
    z[27]=z[3]*z[27];
    z[34]=z[4]*z[13];
    z[36]=z[9]*z[20];
    z[34]=z[34] + z[36];
    z[34]=z[8]*z[34];
    z[36]=z[9] + n<T>(5,2)*z[8];
    z[36]=z[36]*z[31];
    z[34]=z[34] + n<T>(1,2)*z[36];
    z[34]=z[5]*z[34];
    z[36]= - z[29] + z[39];
    z[36]=z[36]*z[23];
    z[29]= - z[9]*z[29];
    z[29]=z[29] + z[36];
    z[29]=z[29]*z[22];
    z[30]= - z[8]*z[30];
    z[30]=n<T>(3,2)*z[9] + z[30];
    z[30]=z[30]*z[38];
    z[36]= - z[10]*z[44];
    z[36]= - n<T>(17,8) + z[36];
    z[36]=z[7]*z[36];
    z[36]=z[36] + z[13] + n<T>(5,4)*z[4];
    z[36]=z[9]*z[36];
    z[43]=n<T>(1,4)*z[9] - n<T>(3,2)*z[7] - z[44] + n<T>(1,8)*z[4];
    z[43]=z[8]*z[43];
    z[16]=z[19] + z[16] + z[27] + z[34] + z[30] + z[29] + z[36] + z[43];
    z[16]=z[1]*z[16];
    z[19]= - z[31]*z[22];
    z[22]=5*z[8] - z[45];
    z[22]=z[22]*z[38];
    z[22]=z[22] - z[33];
    z[22]=z[5]*z[22];
    z[27]=n<T>(3,2)*z[12];
    z[19]=z[22] + z[19] + n<T>(3,4)*z[26] + z[27] - z[8];
    z[19]=z[5]*z[19];
    z[22]=z[32]*z[47];
    z[29]= - z[8] + n<T>(1,4)*z[6];
    z[29]=z[6]*z[29];
    z[22]=z[29] + z[22];
    z[22]=z[5]*z[22];
    z[29]=n<T>(1,4)*z[41];
    z[30]= - static_cast<T>(1)- z[29];
    z[30]=z[6]*z[30];
    z[22]=z[22] + n<T>(3,2)*z[8] + z[30];
    z[22]=z[5]*z[22];
    z[30]=z[12] + n<T>(1,2)*z[26];
    z[30]=z[11]*z[30];
    z[14]=z[22] + n<T>(1,2)*z[41] + z[30] + n<T>(3,2) - z[14];
    z[14]=z[5]*z[14];
    z[22]=2*z[7];
    z[30]=z[22] + z[12];
    z[30]=z[12]*z[30];
    z[30]=z[30] + z[28];
    z[30]=z[11]*z[30];
    z[33]=z[12] + n<T>(1,2)*z[7];
    z[30]=3*z[33] + z[30];
    z[30]=z[11]*z[30];
    z[30]=z[30] + n<T>(7,4) - z[15];
    z[30]=z[11]*z[30];
    z[24]= - n<T>(5,2)*z[10] + z[24];
    z[14]=z[14] + n<T>(1,2)*z[24] + z[30];
    z[14]=z[2]*z[14];
    z[22]= - z[22] + n<T>(1,4)*z[12];
    z[22]=z[12]*z[22];
    z[22]=z[22] - z[28];
    z[22]=z[11]*z[22];
    z[24]= - 5*z[7] + z[27];
    z[22]=n<T>(1,2)*z[24] + z[22];
    z[22]=z[11]*z[22];
    z[23]=z[10]*z[23];
    z[14]=z[14] + z[19] + z[29] + z[22] + z[23] + n<T>(7,4) + z[15];
    z[14]=z[2]*z[14];
    z[19]= - z[40] - 5*z[35];
    z[19]=z[5]*z[19];
    z[22]=z[4] + z[12];
    z[22]=z[12]*z[22];
    z[23]=z[9] - 3*z[6];
    z[23]=z[6]*z[23];
    z[19]=z[19] + z[22] + z[23];
    z[19]=z[19]*z[48];
    z[22]=z[42]*z[38];
    z[19]=z[19] + z[22] - z[49] - z[26];
    z[19]=z[5]*z[19];
    z[21]=z[21] + z[31] - n<T>(1,2)*z[25];
    z[21]=z[5]*z[21];
    z[22]=z[26] + z[12];
    z[21]=z[21] + n<T>(1,2)*z[22] + z[37];
    z[21]=z[5]*z[21];
    z[21]=n<T>(1,2) + z[21];
    z[18]=z[3]*z[21]*z[18];
    z[17]=z[11]*z[17];
    z[17]= - n<T>(1,2) + z[17];
    z[17]=z[18] + n<T>(1,2)*z[17] + z[19];
    z[17]=z[3]*z[17];
    z[18]=n<T>(1,2)*z[9] - z[8];
    z[18]=z[18]*z[41];
    z[18]= - 3*z[8] + z[18];
    z[18]=z[18]*z[38];
    z[19]=z[9] + z[8];
    z[19]=z[19]*z[32];
    z[21]= - z[8]*z[4];
    z[18]=n<T>(3,4)*z[19] + z[21] + z[18];
    z[18]=z[5]*z[18];
    z[15]= - z[29] - n<T>(5,4) + 2*z[15];
    z[15]=z[9]*z[15];
    z[19]= - z[8]*z[20];
    z[19]=z[39] + z[19];
    z[19]=z[11]*z[19];
    z[14]=z[16] + z[14] + z[17] + z[18] + z[19] + n<T>(5,4)*z[8] + z[15];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r104(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r104(const std::array<dd_real,31>&);
#endif
