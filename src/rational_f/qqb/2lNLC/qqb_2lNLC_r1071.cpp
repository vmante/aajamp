#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1071(const std::array<T,31>& k) {
  T z[101];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[8];
    z[6]=k[12];
    z[7]=k[11];
    z[8]=k[13];
    z[9]=k[9];
    z[10]=k[14];
    z[11]=k[15];
    z[12]=k[22];
    z[13]=k[19];
    z[14]=k[10];
    z[15]=k[17];
    z[16]=k[2];
    z[17]=k[3];
    z[18]=k[4];
    z[19]=4*z[11];
    z[20]=z[6] - z[8];
    z[21]= - z[20]*z[19];
    z[22]=npow(z[6],2);
    z[23]=z[10]*z[6];
    z[24]= - 2*z[22] - z[23];
    z[25]=z[2]*z[10];
    z[24]=z[24]*z[25];
    z[21]=z[24] - 18*z[23] + z[22] + z[21];
    z[21]=z[2]*z[21];
    z[24]=npow(z[17],2);
    z[26]=z[8]*z[14];
    z[27]=z[24]*z[26];
    z[28]=z[17] - z[16];
    z[29]= - z[14]*z[28];
    z[30]= - npow(z[2],2)*z[23];
    z[27]=z[30] + z[27] - static_cast<T>(1)+ z[29];
    z[29]=12*z[9];
    z[27]=z[27]*z[29];
    z[30]=4*z[14];
    z[31]=z[5]*z[16];
    z[32]=z[31]*z[30];
    z[33]=4*z[10];
    z[34]=static_cast<T>(1)- z[31];
    z[34]=z[34]*z[33];
    z[35]=z[17]*z[26];
    z[21]=z[27] + z[21] + z[34] + z[32] - 13*z[14] + 12*z[35];
    z[21]=z[9]*z[21];
    z[27]=2*z[5];
    z[32]=static_cast<T>(3)+ z[31];
    z[32]=z[32]*z[27];
    z[34]=5*z[6];
    z[35]=n<T>(1,2)*z[8];
    z[32]= - z[4] + z[32] + z[35] - z[34];
    z[32]=z[10]*z[32];
    z[36]=z[4] - z[7];
    z[37]=z[20] - z[36];
    z[38]=npow(z[11],2);
    z[37]=z[37]*z[38];
    z[39]=npow(z[5],2);
    z[40]=z[39]*z[4];
    z[41]=z[7]*z[39];
    z[37]=z[37] + z[41] - z[40];
    z[41]=3*z[6];
    z[42]= - z[41] - n<T>(1,2)*z[4];
    z[42]=z[10]*z[42];
    z[42]= - z[22] + z[42];
    z[42]=z[10]*z[42];
    z[37]=2*z[37] + z[42];
    z[37]=z[2]*z[37];
    z[42]=3*z[7];
    z[43]=z[14] - z[42];
    z[44]=z[7]*z[17];
    z[44]=static_cast<T>(1)- n<T>(1,2)*z[44];
    z[44]=z[6]*z[44];
    z[43]=n<T>(1,2)*z[43] + z[44];
    z[43]=z[6]*z[43];
    z[44]=z[27]*z[16];
    z[45]=z[44]*z[14];
    z[45]=z[45] + z[42];
    z[46]=2*z[14];
    z[47]= - z[46] - z[45];
    z[47]=z[5]*z[47];
    z[48]=2*z[26];
    z[49]=z[7]*z[8];
    z[50]=n<T>(1,2)*z[14];
    z[51]= - z[50] + z[5];
    z[51]=z[4]*z[51];
    z[52]=z[4] + z[7] - 6*z[6];
    z[52]=z[11]*z[52];
    z[21]=z[21] + z[37] + z[32] + z[52] + z[51] + z[47] + z[43] + z[48]
    - n<T>(1,2)*z[49];
    z[21]=z[9]*z[21];
    z[32]=4*z[12];
    z[37]=z[32] + z[35];
    z[43]=z[8]*z[37];
    z[47]=4*z[5];
    z[51]= - z[8] - z[5];
    z[47]=z[51]*z[47];
    z[51]= - z[6] + z[4];
    z[51]=z[51]*z[19];
    z[52]=2*z[4];
    z[53]= - z[41] - z[52];
    z[53]=z[10]*z[53];
    z[54]= - z[32] + z[6];
    z[54]=z[6]*z[54];
    z[43]=z[53] + z[51] + z[47] + z[43] + z[54];
    z[43]=z[10]*z[43];
    z[47]=npow(z[11],3);
    z[51]=z[36]*z[47];
    z[53]=npow(z[5],3);
    z[54]=z[53]*z[7];
    z[55]=z[4]*z[53];
    z[51]=z[51] - z[54] + z[55];
    z[51]=z[2]*z[51];
    z[37]= - z[37]*z[49];
    z[55]=n<T>(3,2)*z[6];
    z[56]=z[55] + z[32];
    z[56]=z[7]*z[56];
    z[57]=npow(z[14],2);
    z[58]=n<T>(1,2)*z[57];
    z[56]= - z[58] + z[56];
    z[56]=z[6]*z[56];
    z[59]=2*z[7];
    z[60]=z[59]*z[8];
    z[61]=z[14] + z[42];
    z[61]=z[5]*z[61];
    z[61]=z[60] + z[61];
    z[61]=z[61]*z[27];
    z[62]=2*z[39];
    z[58]=z[58] - z[62];
    z[58]=z[4]*z[58];
    z[63]=2*z[6];
    z[64]=z[63] - z[8];
    z[65]= - z[52] - z[7] + z[64];
    z[66]=2*z[38];
    z[65]=z[65]*z[66];
    z[21]=z[21] + 4*z[51] + z[43] + z[65] + z[58] + z[61] + z[37] + 
    z[56];
    z[21]=z[9]*z[21];
    z[37]=z[7]*z[13];
    z[43]=z[6]*z[15];
    z[51]=z[5] - z[13];
    z[56]= - z[15] - z[51];
    z[56]=z[4]*z[56];
    z[43]=z[56] + z[39] - z[37] + z[43];
    z[56]= - static_cast<T>(3)+ z[31];
    z[56]=z[5]*z[56];
    z[44]= - z[11]*z[44];
    z[44]=z[44] - z[59] + z[56];
    z[44]=z[11]*z[44];
    z[43]=2*z[43] + z[44];
    z[44]=2*z[11];
    z[43]=z[43]*z[44];
    z[56]=3*z[8];
    z[58]=z[12] + z[15];
    z[61]= - z[58]*z[56];
    z[65]=npow(z[12],2);
    z[67]=5*z[65];
    z[68]=npow(z[15],2);
    z[61]=z[61] - 3*z[68] - z[67];
    z[61]=z[61]*z[35];
    z[69]=npow(z[13],2);
    z[70]= - n<T>(5,2)*z[68] + 3*z[69];
    z[71]=3*z[13];
    z[72]=z[5]*z[18];
    z[73]=z[72] + 2;
    z[74]= - z[5]*z[73];
    z[74]=z[71] + z[74];
    z[74]=z[4]*z[74];
    z[74]=z[74] + 3*z[39] - z[70];
    z[74]=z[4]*z[74];
    z[75]=z[7]*z[15];
    z[70]= - n<T>(5,2)*z[75] + z[70];
    z[70]=z[7]*z[70];
    z[75]=z[68] + z[65];
    z[76]=z[6]*z[12];
    z[77]=n<T>(5,2)*z[75] - z[76];
    z[77]=z[6]*z[77];
    z[78]=npow(z[8],2);
    z[79]=z[8]*z[18];
    z[80]=static_cast<T>(3)- z[79];
    z[80]=z[5]*z[8]*z[80];
    z[80]=z[78] + z[80];
    z[80]=z[5]*z[80];
    z[81]=n<T>(3,2)*z[8];
    z[82]=n<T>(3,2)*z[4];
    z[83]=z[82] - z[81] + z[6];
    z[84]=z[10]*z[15];
    z[83]=z[83]*z[84];
    z[43]=z[83] + z[43] + z[74] + z[80] + z[77] + z[61] + z[70];
    z[43]=z[10]*z[43];
    z[61]=z[84] + z[68];
    z[61]=n<T>(1,2)*z[61];
    z[61]=z[78]*z[61];
    z[70]=2*z[69];
    z[74]=n<T>(3,2)*z[68] - z[70];
    z[74]=z[7]*z[74];
    z[77]=npow(z[13],3);
    z[80]=4*z[77];
    z[74]= - z[80] + z[74];
    z[74]=z[7]*z[74];
    z[83]= - z[70] + z[39];
    z[83]=z[4]*z[83];
    z[83]=z[80] + z[83];
    z[83]=z[4]*z[83];
    z[84]=npow(z[7],2);
    z[85]=z[84]*z[66];
    z[61]=z[85] + z[83] + z[74] + z[61];
    z[61]=z[10]*z[61];
    z[74]=z[65]*z[57];
    z[83]= - z[68]*z[26];
    z[74]=z[74] + z[83];
    z[74]=z[8]*z[74];
    z[83]=z[57] + z[78];
    z[83]=z[83]*z[69];
    z[85]=z[15] + z[13];
    z[85]=z[14]*z[85];
    z[85]= - z[68] + z[85];
    z[85]=z[7]*z[14]*z[85];
    z[85]=z[85] + z[83];
    z[85]=z[85]*z[42];
    z[76]= - z[65] + z[76];
    z[76]=z[6]*z[57]*z[76];
    z[74]=z[76] + z[74] + z[85];
    z[76]=z[53]*z[63];
    z[85]=z[12] + z[13];
    z[85]=z[8]*z[85];
    z[85]=z[65] + z[85];
    z[85]=z[8]*z[85];
    z[86]= - z[6]*z[65];
    z[85]=z[85] + z[86];
    z[82]=z[85]*z[82];
    z[76]=z[82] - n<T>(3,2)*z[83] + z[76];
    z[76]=z[4]*z[76];
    z[82]=z[39]*z[59];
    z[83]= - z[22] + z[62];
    z[83]=z[10]*z[83];
    z[85]= - z[22]*z[25];
    z[33]= - z[5]*z[33];
    z[33]=z[33] + z[85];
    z[33]=z[9]*z[33];
    z[33]=z[33] + z[82] + z[83];
    z[33]=z[9]*z[33];
    z[33]= - 4*z[54] + z[33];
    z[33]=z[9]*z[33];
    z[54]=z[54]*z[78];
    z[82]=z[47]*z[84];
    z[83]=z[82]*z[10];
    z[85]=z[53]*z[6];
    z[86]=z[85]*npow(z[4],2);
    z[54]= - z[83] + z[54] + z[86];
    z[83]=2*z[2];
    z[86]=z[54]*z[83];
    z[87]= - z[53]*z[60];
    z[88]=z[39]*z[63];
    z[89]= - z[6]*z[84];
    z[88]=z[89] + z[88];
    z[88]=z[11]*z[88];
    z[88]= - 4*z[85] + z[88];
    z[88]=z[11]*z[88];
    z[33]=z[33] - z[86] + z[61] + z[88] + z[76] + n<T>(1,2)*z[74] + z[87];
    z[33]=z[1]*z[33];
    z[61]=4*z[6];
    z[74]= - z[27] - z[61] - z[30] - z[56];
    z[74]=z[5]*z[74];
    z[76]=n<T>(3,2)*z[78];
    z[74]= - z[76] + z[74];
    z[74]=z[5]*z[74];
    z[87]=5*z[12];
    z[88]=7*z[13];
    z[89]= - z[87] - z[88];
    z[89]=z[89]*z[35];
    z[90]=2*z[13];
    z[91]=z[13]*z[17];
    z[92]=static_cast<T>(2)- z[91];
    z[92]=z[92]*z[90];
    z[92]=n<T>(5,2)*z[12] + z[92];
    z[92]=z[6]*z[92];
    z[93]=z[8] + z[14];
    z[94]=n<T>(3,2)*z[93] + z[27];
    z[94]=z[5]*z[94];
    z[89]=z[94] + z[92] + z[70] + z[89];
    z[89]=z[4]*z[89];
    z[68]=5*z[68];
    z[92]=7*z[69];
    z[94]=z[68] + z[92];
    z[95]=n<T>(1,2)*z[13];
    z[96]=z[15] + z[95];
    z[96]=z[14]*z[96];
    z[96]= - n<T>(1,2)*z[94] + z[96];
    z[96]=z[14]*z[96];
    z[67]=z[67] + z[92];
    z[87]=z[87] + z[13];
    z[87]=z[8]*z[87];
    z[87]=z[87] + z[67];
    z[35]=z[87]*z[35];
    z[87]= - static_cast<T>(3)+ 4*z[91];
    z[87]=z[87]*z[69];
    z[87]=z[87] - n<T>(5,2)*z[65];
    z[92]=z[6]*z[87];
    z[35]=z[89] + z[74] + z[92] + z[35] - z[80] + z[96];
    z[35]=z[4]*z[35];
    z[74]=4*z[8];
    z[89]=3*z[14];
    z[92]= - z[59] - z[89] - z[74];
    z[92]=z[7]*z[92];
    z[96]=npow(z[18],2);
    z[97]= - z[96]*z[57];
    z[97]=static_cast<T>(3)+ z[97];
    z[97]=z[4]*z[97];
    z[45]=z[97] + z[45];
    z[45]=z[45]*z[44];
    z[97]=z[14]*z[18];
    z[98]=static_cast<T>(3)+ z[97];
    z[98]=z[14]*z[98];
    z[99]=z[7]*z[18];
    z[100]=static_cast<T>(3)+ z[99];
    z[100]=z[7]*z[100];
    z[98]=z[98] + z[100];
    z[98]=z[6]*z[98];
    z[31]= - z[14]*z[31];
    z[31]=z[31] - z[64];
    z[31]=z[31]*z[27];
    z[64]= - z[4]*z[46];
    z[31]=z[45] + z[64] + z[31] + z[98] + z[48] + z[92];
    z[31]=z[11]*z[31];
    z[45]= - z[7]*z[93];
    z[45]=z[57] + z[45];
    z[45]=z[7]*z[45];
    z[64]= - z[8]*z[57];
    z[45]=z[64] + z[45];
    z[64]= - z[90] + z[7];
    z[64]=z[64]*z[59];
    z[92]=4*z[15];
    z[98]= - z[92] - z[14];
    z[98]=z[14]*z[98];
    z[64]=z[98] + z[64];
    z[64]=z[6]*z[64];
    z[93]=z[41] - z[93];
    z[93]=z[93]*z[62];
    z[51]= - z[61]*z[51];
    z[98]=z[92] - z[14];
    z[98]=z[14]*z[98];
    z[51]=z[98] + z[51];
    z[51]=z[4]*z[51];
    z[31]=z[31] + z[51] + z[93] + n<T>(3,2)*z[45] + z[64];
    z[31]=z[11]*z[31];
    z[45]=2*z[77];
    z[51]= - z[7]*z[69];
    z[51]= - z[45] + z[51];
    z[51]=z[7]*z[51];
    z[64]=z[69]*z[4];
    z[45]=z[45] - z[64];
    z[45]=z[4]*z[45];
    z[77]= - z[11]*z[7];
    z[77]=z[84] + z[77];
    z[77]=z[77]*z[66];
    z[45]=z[77] + z[51] + z[45];
    z[45]=z[10]*z[45];
    z[51]=z[59]*z[78];
    z[77]=z[78] - 6*z[49];
    z[77]=z[5]*z[77];
    z[77]=z[51] + z[77];
    z[77]=z[77]*z[39];
    z[93]=z[20]*z[39];
    z[98]= - z[84]*z[44];
    z[93]=z[93] + z[98];
    z[93]=z[11]*z[93];
    z[98]=z[20]*z[53];
    z[93]= - 2*z[98] + z[93];
    z[93]=z[11]*z[93];
    z[98]=z[5] - z[6];
    z[100]= - z[98]*z[40];
    z[100]=z[85] + z[100];
    z[100]=z[100]*z[52];
    z[45]= - z[86] + z[45] + z[93] + z[77] + z[100];
    z[45]=z[45]*z[83];
    z[77]=5*z[15];
    z[86]= - z[77] - z[13];
    z[86]=z[14]*z[86];
    z[86]=z[86] + z[94];
    z[86]=z[86]*z[50];
    z[93]= - z[12] - z[95];
    z[93]=z[8]*z[93];
    z[67]= - n<T>(1,2)*z[67] + z[93];
    z[67]=z[8]*z[67];
    z[77]=z[77] + z[88];
    z[77]=z[77]*z[50];
    z[77]=z[70] + z[77];
    z[77]=z[7]*z[77];
    z[67]=z[77] + z[67] + z[80] + z[86];
    z[67]=z[7]*z[67];
    z[77]=z[89]*z[58];
    z[65]=z[77] - z[68] - 3*z[65];
    z[50]=z[65]*z[50];
    z[65]=2*z[91];
    z[68]=z[65] + 1;
    z[80]= - z[68]*z[37];
    z[80]=z[80] - z[87];
    z[80]=z[7]*z[80];
    z[86]=z[14] - z[7];
    z[55]=z[55]*z[12]*z[86];
    z[50]=z[55] + z[50] + z[80];
    z[50]=z[6]*z[50];
    z[55]=z[63] - z[59];
    z[80]=z[8] + z[55];
    z[80]=z[5]*z[80];
    z[60]=z[80] + z[26] + z[60];
    z[60]=z[60]*z[27];
    z[76]=z[14]*z[76];
    z[60]=z[76] + z[60];
    z[60]=z[5]*z[60];
    z[76]= - z[14]*z[12];
    z[75]=z[76] + z[75];
    z[75]=z[14]*z[75];
    z[76]=z[15]*z[26];
    z[75]=z[75] + z[76];
    z[75]=z[75]*z[81];
    z[21]=z[33] + z[21] + z[45] + z[43] + z[31] + z[35] + z[60] + z[50]
    + z[75] + z[67];
    z[21]=z[1]*z[21];
    z[31]=z[17]*z[28]*z[46];
    z[33]=npow(z[17],3);
    z[35]= - z[14]*z[33];
    z[35]=z[24] + z[35];
    z[43]=2*z[8];
    z[35]=z[35]*z[43];
    z[45]= - z[63]*z[25];
    z[50]=2*z[10];
    z[45]=z[45] + z[6] - z[50];
    z[45]=z[2]*z[45];
    z[45]=static_cast<T>(1)+ z[45];
    z[45]=z[2]*z[45];
    z[31]=z[45] + z[35] - z[16] + z[31];
    z[29]=z[31]*z[29];
    z[31]= - z[22] - z[23];
    z[31]=z[31]*z[25];
    z[35]= - 41*z[6] - z[10];
    z[35]=z[10]*z[35];
    z[22]=z[31] + z[22] + z[35];
    z[22]=z[2]*z[22];
    z[31]=z[6]*z[17];
    z[35]=static_cast<T>(22)+ z[31];
    z[35]=z[6]*z[35];
    z[22]=z[22] - 27*z[10] - z[74] + z[35];
    z[22]=z[2]*z[22];
    z[35]=z[10]*z[16];
    z[45]= - 16*z[16] + 37*z[17];
    z[45]=z[14]*z[45];
    z[60]=z[24]*z[14];
    z[67]=29*z[17] - 36*z[60];
    z[67]=z[8]*z[67];
    z[74]=z[17]*z[63];
    z[22]=z[29] + z[22] + 4*z[35] + z[74] + z[67] + static_cast<T>(16)+ z[45];
    z[22]=z[9]*z[22];
    z[29]= - 11*z[6] - z[10];
    z[29]=z[29]*z[50];
    z[45]=z[2]*z[41]*npow(z[10],2);
    z[29]=z[29] - z[45];
    z[29]=z[2]*z[29];
    z[67]=z[10] - z[6];
    z[30]=z[30]*z[17];
    z[30]=z[30] - 1;
    z[30]=z[30]*z[8];
    z[22]=z[22] + z[29] + 16*z[14] - 5*z[30] - 10*z[67];
    z[22]=z[9]*z[22];
    z[29]= - z[10] + z[34] + z[36];
    z[29]=z[10]*z[29];
    z[74]=z[12] - 8*z[14];
    z[74]=z[8]*z[74];
    z[75]= - z[12] + z[7];
    z[75]=z[6]*z[75];
    z[20]=z[7] + z[20];
    z[20]=z[5]*z[20];
    z[76]=z[5] + z[6];
    z[80]= - z[4]*z[76];
    z[36]= - z[11]*z[36];
    z[20]=z[22] - z[45] + z[29] + z[36] + z[80] + z[20] + z[74] + z[75];
    z[20]=z[9]*z[20];
    z[22]=z[46] + z[56];
    z[29]=z[18]*z[27];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[27];
    z[22]=z[29] + 2*z[22] + z[34];
    z[22]=z[5]*z[22];
    z[29]= - static_cast<T>(1)- z[72];
    z[29]=z[29]*z[27];
    z[34]= - static_cast<T>(5)+ z[65];
    z[34]=z[6]*z[34];
    z[29]=z[29] + z[34] + z[90] - z[14];
    z[29]=z[4]*z[29];
    z[34]=3*z[12];
    z[36]=z[68]*z[13];
    z[45]=z[34] - z[36];
    z[45]=z[6]*z[45];
    z[65]=z[92] + z[13];
    z[65]=z[14]*z[65];
    z[34]= - z[34] - z[13];
    z[34]=z[8]*z[34];
    z[22]=z[29] + z[22] + z[45] + z[34] - z[70] + z[65];
    z[22]=z[4]*z[22];
    z[29]=z[14]*npow(z[18],3);
    z[29]=z[29] + 2*z[96];
    z[34]=z[14]*z[29];
    z[34]=z[18] + z[34];
    z[34]=z[4]*z[34];
    z[45]=z[96]*z[14];
    z[65]= - z[45] - 2*z[16] + 3*z[18];
    z[65]=z[14]*z[65];
    z[68]= - z[18]*z[42];
    z[34]=z[34] + z[68] - static_cast<T>(1)+ z[65];
    z[34]=z[11]*z[34];
    z[65]=2*z[18];
    z[45]=z[65] + z[45];
    z[45]=z[14]*z[45];
    z[45]=static_cast<T>(1)+ z[45];
    z[45]=z[4]*z[45];
    z[57]= - z[18]*z[57];
    z[34]=z[34] + z[45] + z[57] + z[7];
    z[34]=z[34]*z[44];
    z[45]=z[89] + z[43];
    z[45]=z[45]*z[59];
    z[57]=z[15] - z[14];
    z[57]=z[14]*z[57];
    z[68]=z[5]*z[7];
    z[72]= - static_cast<T>(3)+ z[97];
    z[72]=z[14]*z[72];
    z[72]=z[72] - z[5];
    z[72]=z[4]*z[72];
    z[34]=z[34] + z[72] + z[68] + z[45] + z[57] - 4*z[26];
    z[34]=z[11]*z[34];
    z[45]=z[78] - 7*z[49];
    z[45]=z[5]*z[45];
    z[45]=z[51] + z[45];
    z[45]=z[45]*z[39];
    z[51]= - z[2]*z[54];
    z[54]=z[63] - z[5];
    z[54]=z[54]*z[40];
    z[54]= - z[85] + z[54];
    z[54]=z[4]*z[54];
    z[57]= - z[7]*z[19];
    z[57]=z[84] + z[57];
    z[68]=z[38]*z[10];
    z[57]=z[57]*z[68];
    z[45]=z[51] + z[57] - z[82] + z[45] + z[54];
    z[45]=z[45]*z[83];
    z[51]=static_cast<T>(2)+ z[79];
    z[51]=z[8]*z[51];
    z[51]=z[61] + z[51] - 4*z[7];
    z[51]=z[5]*z[51];
    z[51]=z[51] - z[78] + 8*z[49];
    z[51]=z[51]*z[27];
    z[54]= - z[78]*z[42];
    z[51]=z[54] + z[51];
    z[51]=z[5]*z[51];
    z[54]= - z[76]*z[62];
    z[57]=z[73]*z[40];
    z[54]=z[54] + z[57];
    z[52]=z[54]*z[52];
    z[37]=z[69] + z[37];
    z[37]=z[7]*z[37];
    z[54]= - 6*z[7] + z[11];
    z[54]=z[11]*z[54];
    z[54]= - z[84] + z[54];
    z[54]=z[11]*z[54];
    z[37]=z[54] + z[37] - z[64];
    z[37]=z[37]*z[50];
    z[50]=z[47]*z[59];
    z[54]=static_cast<T>(4)+ z[99];
    z[54]=z[54]*z[50];
    z[37]=z[45] + z[37] + z[54] + z[51] + z[52];
    z[37]=z[2]*z[37];
    z[45]= - z[41] + z[56];
    z[45]=z[58]*z[45];
    z[51]= - z[92] + z[71];
    z[51]=z[4]*z[51];
    z[52]=z[11]*z[16];
    z[52]=static_cast<T>(2)+ z[52];
    z[19]=z[52]*z[19];
    z[19]=z[19] - z[15] + z[7];
    z[19]=z[11]*z[19];
    z[52]=z[13] - z[15];
    z[52]= - 3*z[52] + z[7];
    z[52]=z[7]*z[52];
    z[19]= - z[23] + z[19] + z[51] + z[52] + z[45];
    z[19]=z[10]*z[19];
    z[45]= - 3*z[15] - z[13];
    z[45]=z[14]*z[45];
    z[51]=z[8] + z[32] + z[13];
    z[51]=z[8]*z[51];
    z[45]=z[49] + z[51] + z[70] + z[45];
    z[45]=z[7]*z[45];
    z[32]= - z[32] + z[36];
    z[32]=z[7]*z[32];
    z[32]=z[77] + z[32];
    z[32]=z[6]*z[32];
    z[36]= - z[27]*z[79];
    z[51]= - static_cast<T>(1)- z[79];
    z[51]=z[8]*z[51];
    z[36]=z[36] + z[51] - z[55];
    z[36]=z[5]*z[36];
    z[36]=z[36] - z[48] - 5*z[49];
    z[36]=z[36]*z[27];
    z[48]=z[58]*z[26];
    z[19]=z[21] + z[20] + z[37] + z[19] + z[34] + z[22] + z[36] + z[32]
    - 3*z[48] + z[45];
    z[20]=npow(z[3],2);
    z[19]=z[1]*z[20]*z[19];
    z[21]= - z[53]*z[49];
    z[22]= - z[4]*z[85];
    z[32]= - z[7]*z[47]*z[10];
    z[21]=z[32] + z[21] + z[22];
    z[21]=z[21]*z[83];
    z[22]= - z[6] + z[7] - z[8];
    z[32]=z[22]*z[27];
    z[34]=3*z[49] - z[32];
    z[34]=z[34]*z[39];
    z[36]=z[41] - z[27];
    z[36]=z[36]*z[40];
    z[37]= - z[42] + z[44];
    z[37]=z[37]*z[68];
    z[21]=z[21] + z[37] + z[50] + z[34] + z[36];
    z[21]=z[2]*z[21];
    z[22]=z[5]*z[22];
    z[34]=z[4]*z[98];
    z[22]=z[34] - z[49] + z[22];
    z[22]=z[5]*z[22];
    z[34]= - z[4]*z[18];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[34]*z[44];
    z[34]=z[34] + z[59] - z[4];
    z[34]=z[34]*z[38];
    z[36]= - z[16]*z[44];
    z[36]=static_cast<T>(1)+ z[36];
    z[36]=z[11]*z[36];
    z[36]= - z[59] + z[36];
    z[36]=z[10]*z[11]*z[36];
    z[21]=z[21] + z[36] + 2*z[22] + z[34];
    z[21]=z[21]*z[83];
    z[22]=z[35] - 1;
    z[31]=z[22] - z[31];
    z[23]=z[23]*z[2];
    z[34]= - z[23] - z[67];
    z[34]=z[2]*z[34];
    z[34]=z[34] - z[31];
    z[34]=z[2]*z[34];
    z[35]=z[17] + z[16];
    z[36]=z[8]*z[24];
    z[34]=z[34] + z[36] + z[35];
    z[34]=z[2]*z[34];
    z[28]= - z[28]*z[60];
    z[36]=z[14]*npow(z[17],4);
    z[36]= - 2*z[33] + z[36];
    z[36]=z[8]*z[36];
    z[37]=z[17]*z[35];
    z[28]=z[34] + z[36] + z[37] + z[28];
    z[28]=z[9]*z[28];
    z[25]= - z[25] + 1;
    z[25]=z[63]*z[25];
    z[25]= - z[10] + z[25];
    z[25]=z[2]*z[25];
    z[34]=z[8]*z[17];
    z[25]=z[25] + z[34] - z[31];
    z[25]=z[2]*z[25];
    z[31]=z[33]*z[46];
    z[24]= - 3*z[24] + z[31];
    z[24]=z[8]*z[24];
    z[31]=z[16] - 2*z[17];
    z[31]=z[14]*z[17]*z[31];
    z[24]=z[28] + z[25] + z[24] + z[31] + z[35];
    z[24]=z[9]*z[24];
    z[25]= - 4*z[17] + 5*z[60];
    z[25]=z[25]*z[43];
    z[28]=z[16] - 7*z[17];
    z[28]=z[14]*z[28];
    z[31]=z[17]*z[41];
    z[33]= - 10*z[23] - z[10] + z[8] + 9*z[6];
    z[33]=z[2]*z[33];
    z[22]=6*z[24] + z[33] + z[31] + z[25] + z[28] - z[22];
    z[22]=z[9]*z[22];
    z[22]=z[22] - 4*z[23] + z[41] - z[14] + z[30];
    z[22]=z[9]*z[22];
    z[23]= - z[16] + z[65];
    z[23]=z[23]*z[97];
    z[24]= - z[4]*z[29];
    z[23]=z[24] + z[23] + z[16] + z[18];
    z[23]=z[23]*z[44];
    z[24]= - z[96]*z[89];
    z[25]=4*z[18];
    z[24]= - z[25] + z[24];
    z[24]=z[4]*z[24];
    z[25]= - z[16] + z[25];
    z[25]=z[14]*z[25];
    z[23]=z[23] + z[24] + static_cast<T>(1)+ z[25];
    z[23]=z[11]*z[23];
    z[24]= - z[18]*z[46];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[4]*z[24];
    z[23]=z[23] + z[24] + z[14] + z[7];
    z[23]=z[23]*z[44];
    z[24]= - z[27] - z[14] + z[6];
    z[24]=z[4]*z[24];
    z[25]= - z[16]*z[66];
    z[25]=z[25] - z[7] - z[6];
    z[25]=z[10]*z[25];
    z[21]=2*z[22] + z[21] + z[25] + z[23] + z[24] - z[32] + z[26] + 
    z[49];
    z[20]=z[21]*z[20];
    z[19]=z[20] + z[19];

    r += z[19]*npow(z[1],2);
 
    return r;
}

template double qqb_2lNLC_r1071(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1071(const std::array<dd_real,31>&);
#endif
