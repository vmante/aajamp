#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r390(const std::array<T,31>& k) {
  T z[31];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[11];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[10];
    z[8]=k[14];
    z[9]=k[5];
    z[10]=k[8];
    z[11]=k[15];
    z[12]=k[9];
    z[13]=z[3]*z[9];
    z[14]=z[4]*z[9];
    z[15]= - z[13] - z[14];
    z[15]=z[10]*z[15];
    z[15]= - z[3] + z[15];
    z[15]=z[10]*z[15];
    z[16]=z[4]*z[11];
    z[17]=z[3]*z[11];
    z[15]=z[15] + z[17] - z[16];
    z[15]=z[10]*z[15];
    z[16]=z[8]*z[11];
    z[18]=z[7] + z[12];
    z[19]=z[16]*z[18];
    z[20]=z[7]*z[12];
    z[21]=z[20]*z[11];
    z[19]=z[19] - z[21];
    z[22]=npow(z[9],2);
    z[23]= - z[4]*z[22];
    z[23]=z[9] + z[23];
    z[23]=z[10]*z[23];
    z[23]=z[23] + static_cast<T>(1)+ z[14];
    z[23]=z[10]*z[23];
    z[24]=z[4] - z[3];
    z[23]=z[23] - z[24];
    z[23]=z[10]*z[23];
    z[23]= - z[20] + z[23];
    z[23]=z[6]*z[23];
    z[25]=z[4]*z[3];
    z[26]=z[25]*z[11];
    z[15]=z[23] + z[15] - z[26] + z[19];
    z[23]=z[18]*z[8];
    z[27]= - z[23] + z[25];
    z[22]= - z[3]*z[22];
    z[22]=z[9] + z[22];
    z[22]=z[10]*z[22];
    z[22]= - z[13] + z[22];
    z[28]=n<T>(1,2)*z[10];
    z[22]=z[22]*z[28];
    z[22]= - z[3] + z[22];
    z[22]=z[10]*z[22];
    z[29]= - z[18] - z[24];
    z[29]=n<T>(1,2)*z[29] + z[10];
    z[29]=z[6]*z[29];
    z[22]=z[29] + n<T>(1,2)*z[27] + z[22];
    z[22]=z[5]*z[22];
    z[15]=n<T>(1,2)*z[15] + z[22];
    z[22]=npow(z[2],2);
    z[15]=z[15]*z[22];
    z[27]=z[16] - z[17];
    z[27]=3*z[27];
    z[29]=npow(z[10],2);
    z[13]=static_cast<T>(1)- z[13];
    z[13]=z[13]*z[29];
    z[13]= - z[27] + z[13];
    z[13]=z[10]*z[13];
    z[30]=npow(z[8],2);
    z[18]=z[11]*z[18];
    z[18]=z[18] - z[30];
    z[18]=z[8]*z[18];
    z[27]=z[27] - z[25];
    z[27]=z[4]*z[27];
    z[13]=z[13] + z[27] - z[21] + z[18];
    z[18]=n<T>(3,2)*z[8];
    z[21]=z[18] - 2*z[3];
    z[21]=z[4]*z[21];
    z[27]= - z[8] + z[3];
    z[27]=z[10]*z[27];
    z[20]=n<T>(3,2)*z[27] + z[21] - n<T>(1,2)*z[20] + z[23];
    z[20]=z[6]*z[20];
    z[13]=n<T>(1,2)*z[13] + z[20];
    z[13]=z[5]*z[13];
    z[14]= - z[10]*z[14];
    z[14]= - z[4] + z[14];
    z[14]=z[14]*z[28];
    z[18]=z[18] + 2*z[11];
    z[18]=z[4]*z[18];
    z[14]=z[14] - 2*z[17] + z[18];
    z[14]=z[10]*z[14];
    z[17]=npow(z[6],2);
    z[18]=z[4]*z[17];
    z[14]= - n<T>(1,2)*z[18] + z[14] + 2*z[26] - z[19];
    z[14]=z[6]*z[14];
    z[18]=z[7] + z[8];
    z[18]=z[18]*z[30];
    z[19]= - z[6]*z[24];
    z[19]= - z[25] + z[19];
    z[19]=z[6]*z[19];
    z[18]=z[18] + z[19];
    z[18]=z[6]*z[18];
    z[19]=z[7]*npow(z[8],3);
    z[18]= - z[19] + z[18];
    z[18]=z[5]*z[18];
    z[17]=z[17]*z[25];
    z[17]=z[17] - z[19];
    z[19]= - z[1]*z[5];
    z[19]=z[19] - 1;
    z[17]=z[19]*z[17]*z[6];
    z[17]=z[18] + z[17];
    z[17]=z[1]*z[17];
    z[16]=3*z[16] - z[29];
    z[16]=z[28]*z[4]*z[16];
    z[13]=n<T>(1,2)*z[17] + z[13] + z[16] + z[14];
    z[13]=z[1]*z[22]*z[13];
    z[13]=z[15] + z[13];

    r += z[13]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r390(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r390(const std::array<dd_real,31>&);
#endif
