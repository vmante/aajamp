#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r665(const std::array<T,31>& k) {
  T z[65];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[8];
    z[9]=k[9];
    z[10]=k[2];
    z[11]=k[5];
    z[12]=k[3];
    z[13]=k[4];
    z[14]=k[15];
    z[15]=npow(z[14],2);
    z[16]=npow(z[8],2);
    z[17]=z[15] + z[16];
    z[18]=z[17]*z[4];
    z[17]=z[17]*z[2];
    z[18]=z[18] - z[17];
    z[18]=z[18]*z[11];
    z[19]=2*z[7];
    z[20]=2*z[14];
    z[21]=z[20] - z[2];
    z[21]=z[21]*z[19];
    z[22]=2*z[5];
    z[23]=4*z[6] + z[2];
    z[23]=z[23]*z[22];
    z[24]=z[14]*z[6];
    z[25]=4*z[24];
    z[26]=z[8] - z[14];
    z[27]=z[26]*z[2];
    z[26]=z[26]*z[4];
    z[21]= - 6*z[18] + z[23] + 3*z[26] + z[21] - z[25] + z[27];
    z[23]=2*z[11];
    z[21]=z[21]*z[23];
    z[28]=z[12] - z[10];
    z[29]=z[28]*z[12];
    z[30]=npow(z[10],2);
    z[29]=z[29] + z[30];
    z[31]=2*z[3];
    z[32]=z[29]*z[31];
    z[33]=npow(z[12],3);
    z[34]= - z[33]*z[31];
    z[35]=npow(z[12],2);
    z[36]=3*z[35];
    z[34]=z[36] + z[34];
    z[34]=z[7]*z[34];
    z[37]=z[11]*z[6];
    z[37]=z[37] + 1;
    z[37]=z[22]*z[37];
    z[37]= - z[7] + z[37];
    z[37]=z[11]*z[37];
    z[38]=z[10]*z[22];
    z[37]=z[37] - static_cast<T>(1)+ z[38];
    z[37]=z[11]*z[37];
    z[38]=z[12] + z[10];
    z[39]=z[30]*z[5];
    z[32]=z[37] - z[39] + z[34] + z[32] - z[38];
    z[34]=8*z[9];
    z[32]=z[32]*z[34];
    z[37]=z[35]*z[3];
    z[40]=11*z[12] - 16*z[37];
    z[40]=z[40]*z[19];
    z[41]=6*z[7];
    z[42]=z[11]*z[5];
    z[43]=z[42]*z[6];
    z[44]=8*z[43] + z[41] - z[5];
    z[44]=z[44]*z[23];
    z[45]=z[12]*z[6];
    z[46]=z[5]*z[10];
    z[47]= - 32*z[10] + 33*z[12];
    z[47]=z[3]*z[47];
    z[32]=z[32] + z[44] + 28*z[46] + z[40] + z[47] + static_cast<T>(6)- z[45];
    z[32]=z[9]*z[32];
    z[40]=2*z[12];
    z[44]=npow(z[6],2);
    z[47]=z[40]*z[44];
    z[48]=3*z[6];
    z[49]=z[48] + z[47];
    z[50]=z[3]*z[12];
    z[51]=z[8]*z[10];
    z[52]=2*z[51];
    z[53]=static_cast<T>(9)- z[52];
    z[53]=2*z[53] - z[50];
    z[53]=z[53]*z[31];
    z[45]=z[45] - z[50];
    z[54]=static_cast<T>(2)+ z[45];
    z[54]=z[4]*z[54];
    z[55]= - static_cast<T>(15)+ 4*z[51];
    z[55]=z[55]*z[22];
    z[56]=z[7]*z[50];
    z[21]=z[32] + z[21] + z[55] + z[54] - 32*z[56] + z[53] - 8*z[8] + 
    z[49];
    z[21]=z[9]*z[21];
    z[32]= - 6*z[14] + z[2];
    z[32]=z[32]*z[19];
    z[53]=2*z[6] - z[2];
    z[53]=z[53]*z[22];
    z[18]=4*z[18] + z[53] - 7*z[26] + z[32] - z[25] - z[27];
    z[18]=z[11]*z[18];
    z[25]=z[7]*z[14];
    z[26]=z[5]*z[6];
    z[26]=z[26] - z[24] + z[25];
    z[26]=z[11]*z[26];
    z[26]=z[5] + z[26];
    z[26]=z[11]*z[26];
    z[27]=z[30]*z[8];
    z[32]= - z[27] - z[28];
    z[32]=z[3]*z[32];
    z[53]=z[7]*z[37];
    z[27]= - 2*z[10] + z[27];
    z[27]=z[5]*z[27];
    z[26]=z[26] + z[27] + z[53] - static_cast<T>(1)+ z[32];
    z[26]=z[26]*z[34];
    z[27]= - static_cast<T>(1)+ 6*z[50];
    z[27]=z[27]*z[19];
    z[32]=static_cast<T>(8)+ z[45];
    z[32]=z[4]*z[32];
    z[53]=28*z[5];
    z[54]=static_cast<T>(1)- z[51];
    z[54]=z[54]*z[53];
    z[51]= - static_cast<T>(13)+ 12*z[51];
    z[51]=z[3]*z[51];
    z[18]=z[26] + z[18] + z[54] + z[32] + z[27] + 24*z[8] + z[51];
    z[18]=z[9]*z[18];
    z[26]=17*z[14];
    z[27]=2*z[50];
    z[32]= - static_cast<T>(9)+ z[27];
    z[32]=z[3]*z[32];
    z[32]=z[32] - 11*z[8] - z[26] - 9*z[6] - z[47];
    z[32]=z[4]*z[32];
    z[51]= - 3*z[8] + z[3];
    z[51]=z[3]*z[51];
    z[24]= - z[24] + z[51];
    z[51]=5*z[14];
    z[54]=3*z[3];
    z[55]=z[2] + z[51] + z[54];
    z[55]=z[55]*z[19];
    z[56]=z[6] + 19*z[8];
    z[56]=z[56]*z[22];
    z[57]=2*z[15];
    z[58]= - z[57] - z[16];
    z[58]=z[4]*z[58];
    z[59]= - z[2]*z[16];
    z[58]=z[59] + z[58];
    z[59]=8*z[11];
    z[58]=z[58]*z[59];
    z[60]=z[20] + z[8];
    z[60]=z[2]*z[60];
    z[18]=z[18] + z[58] + z[56] + z[32] + z[55] + 2*z[24] + z[60];
    z[18]=z[9]*z[18];
    z[24]=z[16]*z[4];
    z[32]=z[24]*z[59];
    z[52]= - static_cast<T>(1)+ z[52];
    z[52]=z[5]*z[52];
    z[52]= - z[8] + z[52];
    z[34]=z[52]*z[34];
    z[52]=4*z[16];
    z[55]=7*z[8];
    z[56]= - z[55] + z[3];
    z[56]=z[4]*z[56];
    z[53]= - z[8]*z[53];
    z[32]=z[34] + z[32] + z[53] - z[52] + z[56];
    z[32]=z[9]*z[32];
    z[34]=npow(z[3],2);
    z[53]=2*z[16] - z[34];
    z[56]=2*z[4];
    z[53]=z[53]*z[56];
    z[32]=z[53] + z[32];
    z[32]=z[9]*z[32];
    z[53]=z[4]*z[7];
    z[58]=z[53]*z[2];
    z[59]=z[44]*z[58];
    z[60]=z[4]*z[2];
    z[61]=z[5]*z[34]*z[60];
    z[62]=z[9]*z[8]*z[22];
    z[24]=z[24] + z[62];
    z[24]=z[24]*npow(z[9],2);
    z[24]=2*z[24] + z[59] + z[61];
    z[24]=z[1]*z[24];
    z[59]=z[34] + z[44];
    z[61]=2*z[2];
    z[62]= - z[59]*z[61];
    z[63]= - z[2]*z[6];
    z[63]=2*z[44] + z[63];
    z[63]=z[7]*z[63];
    z[62]=z[62] + z[63];
    z[62]=z[4]*z[62];
    z[63]= - z[2]*z[3];
    z[64]=z[4]*z[61];
    z[63]=z[64] + 2*z[34] + z[63];
    z[63]=z[4]*z[63];
    z[34]= - z[34]*z[61];
    z[34]=z[34] + z[63];
    z[34]=z[5]*z[34];
    z[63]= - z[19]*z[44]*z[2];
    z[24]=2*z[24] + z[32] + z[34] + z[63] + z[62];
    z[24]=z[1]*z[24];
    z[27]=static_cast<T>(3)- z[27];
    z[27]=z[3]*z[27];
    z[27]=z[27] + z[49];
    z[27]=z[2]*z[27];
    z[32]=z[61] + 6*z[6] - z[8];
    z[32]=z[7]*z[32];
    z[27]= - 4*z[60] + z[27] + z[32];
    z[27]=z[4]*z[27];
    z[32]=z[8]*z[13];
    z[34]=z[7]*z[32];
    z[34]= - z[5] + z[34] - z[51] - 6*z[8];
    z[34]=z[19]*z[34];
    z[26]=z[56] + 4*z[2] + z[26] + 6*z[3];
    z[26]=z[4]*z[26];
    z[26]=z[26] + z[34];
    z[26]=z[5]*z[26];
    z[34]=z[52]*z[7];
    z[49]=z[34] - z[58];
    z[49]=z[4]*z[49];
    z[51]=8*z[15];
    z[56]=z[51] + z[60];
    z[56]=z[4]*z[56];
    z[58]= - z[5]*z[25];
    z[56]=z[56] + z[58];
    z[56]=z[5]*z[56];
    z[49]=z[49] + z[56];
    z[49]=z[49]*z[23];
    z[56]=z[57] + z[59];
    z[56]=z[56]*z[61];
    z[18]=z[24] + z[18] + z[49] + z[26] + z[56] + z[27];
    z[18]=z[1]*z[18];
    z[24]=z[52] + z[44] + z[51];
    z[24]=z[2]*z[24];
    z[25]= - 4*z[15] - z[25];
    z[26]=z[4] + 11*z[14] + z[61];
    z[26]=z[4]*z[26];
    z[27]=z[5]*z[14];
    z[25]=z[27] + 2*z[25] + z[26];
    z[25]=z[5]*z[25];
    z[26]= - z[55] - z[61];
    z[26]=z[7]*z[26];
    z[26]=z[26] - z[53];
    z[26]=z[4]*z[26];
    z[15]=z[15]*z[4];
    z[27]=z[42]*z[15];
    z[24]=12*z[27] + z[25] + z[26] + z[24] - z[34];
    z[24]=z[24]*z[23];
    z[25]=4*z[14];
    z[26]=z[14]*z[13];
    z[26]= - static_cast<T>(3)+ z[26];
    z[26]=z[26]*z[25];
    z[27]= - static_cast<T>(1)+ z[50];
    z[27]=z[27]*z[31];
    z[26]=z[27] - 10*z[8] + z[26] + z[6] - z[47];
    z[26]=z[2]*z[26];
    z[27]=5*z[3];
    z[31]= - static_cast<T>(1)- z[32];
    z[31]=z[7]*z[31];
    z[31]=z[31] + z[2] + z[55] - z[27];
    z[31]=z[31]*z[19];
    z[34]= - z[6] - z[14];
    z[34]=z[14]*z[34];
    z[42]=z[3]*z[8];
    z[34]=z[34] + z[42];
    z[42]=static_cast<T>(6)- z[45];
    z[42]=z[2]*z[42];
    z[41]=z[42] + z[41];
    z[41]=z[4]*z[41];
    z[32]=static_cast<T>(3)+ z[32];
    z[32]=z[32]*z[19];
    z[42]=z[4] - 2*z[8] + z[6] - z[14];
    z[32]=z[32] + z[2] + 3*z[42];
    z[32]=z[32]*z[22];
    z[18]=z[18] + z[21] + z[24] + z[32] + z[41] + z[31] + 4*z[34] + 
    z[26];
    z[18]=z[1]*z[18];
    z[21]=z[5] - z[6];
    z[24]=z[43] + z[21];
    z[24]=z[11]*z[24];
    z[26]=z[7]*z[12];
    z[24]=z[24] + z[46] - static_cast<T>(1)- z[26];
    z[24]=z[11]*z[24];
    z[31]=z[7]*z[36];
    z[24]=z[24] + z[39] + z[31] - z[38];
    z[24]=z[11]*z[24];
    z[29]= - z[29]*z[50];
    z[31]=z[3]*npow(z[12],4);
    z[31]= - 3*z[33] + z[31];
    z[31]=z[7]*z[31];
    z[32]= - z[10] + z[40];
    z[32]=z[12]*z[32];
    z[24]=z[24] + z[31] + z[29] - z[30] + z[32];
    z[29]=2*z[9];
    z[24]=z[24]*z[29];
    z[21]=3*z[43] - z[19] + z[21];
    z[21]=z[11]*z[21];
    z[21]=z[21] - z[46] - static_cast<T>(1)+ 9*z[26];
    z[21]=z[11]*z[21];
    z[26]=z[28]*z[50];
    z[27]=z[33]*z[27];
    z[27]= - 12*z[35] + z[27];
    z[27]=z[7]*z[27];
    z[21]=z[24] + z[21] + z[27] - 5*z[26] + z[10] + 7*z[12];
    z[21]=z[21]*z[29];
    z[19]=7*z[43] + 5*z[5] - z[6] + z[19];
    z[19]=z[11]*z[19];
    z[24]=4*z[10] - 13*z[12];
    z[24]=z[3]*z[24];
    z[26]= - 15*z[12] + 13*z[37];
    z[26]=z[7]*z[26];
    z[19]=z[21] + z[19] - 4*z[46] + z[26] - static_cast<T>(1)+ z[24];
    z[19]=z[9]*z[19];
    z[21]=z[7]*z[16];
    z[17]= - z[17] + z[21];
    z[21]=z[4]*z[14];
    z[21]= - z[57] + z[21];
    z[21]=z[5]*z[21];
    z[16]= - z[16]*z[53];
    z[15]=z[5]*z[15];
    z[15]=z[16] + z[15];
    z[15]=z[15]*z[23];
    z[16]=z[8]*z[53];
    z[15]=z[15] + z[21] + 2*z[17] + z[16];
    z[15]=z[15]*z[23];
    z[16]= - z[13]*z[25];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[14]*z[16];
    z[16]=z[16] + z[8];
    z[16]=z[2]*z[16];
    z[17]= - z[7]*z[8];
    z[16]=z[17] + z[57] + z[16];
    z[17]=z[10]*z[20];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[20];
    z[17]=z[4] + z[48] + z[17];
    z[17]=z[5]*z[17];
    z[15]=z[15] + z[17] + 2*z[16] - z[53];
    z[15]=z[11]*z[15];
    z[16]=2*z[13];
    z[17]=z[16] - z[10];
    z[17]=z[17]*z[20];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[14]*z[17];
    z[20]= - npow(z[13],2)*z[20];
    z[20]= - z[13] + z[20];
    z[20]=z[14]*z[20];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[2]*z[20];
    z[17]=z[20] + z[17] - z[54];
    z[16]=z[16] + 9*z[12];
    z[16]=z[3]*z[16];
    z[16]= - static_cast<T>(4)+ z[16];
    z[16]=z[7]*z[16];
    z[15]=z[19] + z[15] + z[22] + 2*z[17] + z[16];
    z[15]=2*z[15] + z[18];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r665(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r665(const std::array<dd_real,31>&);
#endif
