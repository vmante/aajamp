#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r914(const std::array<T,31>& k) {
  T z[53];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[11];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[10];
    z[8]=k[14];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[3];
    z[12]=k[9];
    z[13]=k[15];
    z[14]=k[8];
    z[15]=z[14]*z[10];
    z[16]=n<T>(5,8)*z[15];
    z[17]=z[16] - 4;
    z[18]=npow(z[14],2);
    z[17]=z[17]*z[18];
    z[19]=9*z[14];
    z[20]=z[13]*z[10];
    z[21]= - static_cast<T>(7)+ n<T>(17,2)*z[20];
    z[21]=z[13]*z[21];
    z[21]=z[19] + z[21];
    z[22]=n<T>(1,4)*z[13];
    z[21]=z[21]*z[22];
    z[23]=z[6]*z[14];
    z[24]=npow(z[10],2);
    z[25]=z[24]*z[14];
    z[25]=5*z[25];
    z[26]=47*z[10] - z[25];
    z[26]=z[14]*z[26];
    z[26]=static_cast<T>(9)+ n<T>(1,2)*z[26];
    z[26]=z[26]*z[23];
    z[27]=n<T>(1,4)*z[3];
    z[28]= - n<T>(31,2)*z[13] + 15*z[6];
    z[28]=z[28]*z[27];
    z[29]=n<T>(1,4)*z[7];
    z[30]= - n<T>(7,2)*z[13] - 15*z[3];
    z[30]=z[30]*z[29];
    z[21]=z[30] + z[28] + n<T>(1,4)*z[26] - z[17] + z[21];
    z[21]=z[4]*z[21];
    z[26]=npow(z[12],2);
    z[28]=n<T>(1,2)*z[26];
    z[30]=z[12]*z[10];
    z[31]= - n<T>(17,2) + z[30];
    z[31]=z[31]*z[28];
    z[32]=n<T>(1,2)*z[4];
    z[33]=3*z[3];
    z[34]= - z[6] + z[33];
    z[34]=z[34]*z[32];
    z[35]=17*z[12] - n<T>(3,2)*z[14];
    z[35]=z[6]*z[35];
    z[25]=17*z[10] - z[25];
    z[25]=z[14]*z[25];
    z[25]= - static_cast<T>(29)+ z[25];
    z[25]=z[14]*z[25];
    z[25]=z[25] - 7*z[6];
    z[25]=z[3]*z[25];
    z[36]=z[7]*z[12];
    z[17]=z[34] - n<T>(17,8)*z[36] + n<T>(1,8)*z[25] + n<T>(1,4)*z[35] + z[31] + 
    z[17];
    z[17]=z[5]*z[17];
    z[20]=static_cast<T>(5)+ n<T>(17,8)*z[20];
    z[20]=z[13]*z[20];
    z[20]=z[33] - n<T>(9,4)*z[12] + z[20];
    z[20]=z[13]*z[20];
    z[25]= - static_cast<T>(5)- z[30];
    z[25]=z[25]*z[28];
    z[28]=n<T>(1,2)*z[7];
    z[31]=z[27] - 11*z[13] + n<T>(23,4)*z[6];
    z[31]=z[31]*z[28];
    z[34]=17*z[13];
    z[35]= - z[24]*z[34];
    z[35]= - 9*z[10] + z[35];
    z[36]=n<T>(1,2)*z[13];
    z[35]=z[35]*z[36];
    z[35]=static_cast<T>(25)+ z[35];
    z[22]=z[35]*z[22];
    z[35]=2*z[14];
    z[22]=n<T>(27,8)*z[7] + n<T>(19,8)*z[3] - z[35] + z[22];
    z[22]=z[4]*z[22];
    z[37]= - z[12]*z[24];
    z[37]=n<T>(9,2)*z[10] + z[37];
    z[37]=z[12]*z[37];
    z[37]=n<T>(133,4) + z[37];
    z[37]=z[12]*z[37];
    z[35]= - 2*z[4] + n<T>(41,8)*z[7] - n<T>(17,4)*z[6] + n<T>(1,2)*z[37] + z[35];
    z[35]=z[5]*z[35];
    z[37]=z[6]*z[12];
    z[20]=z[35] + z[22] + z[31] + n<T>(21,8)*z[37] + z[25] + z[20];
    z[20]=z[8]*z[20];
    z[22]=z[12]*z[11];
    z[25]=static_cast<T>(5)+ z[22];
    z[25]=z[25]*z[26];
    z[31]=z[34]*z[9];
    z[35]= - static_cast<T>(23)+ z[31];
    z[35]=z[35]*z[36];
    z[35]=9*z[12] + z[35];
    z[35]=z[35]*z[36];
    z[38]= - z[12]*npow(z[11],2);
    z[38]= - n<T>(29,2)*z[11] + z[38];
    z[38]=z[12]*z[38];
    z[38]= - n<T>(101,4) + z[38];
    z[38]=z[38]*z[37];
    z[39]=npow(z[9],2);
    z[40]= - z[39]*z[34];
    z[40]=37*z[9] + z[40];
    z[40]=z[13]*z[40];
    z[40]=static_cast<T>(39)+ z[40];
    z[40]=z[13]*z[40]*z[27];
    z[25]=z[40] + z[38] + z[25] + z[35];
    z[25]=z[25]*z[28];
    z[35]=static_cast<T>(2)+ n<T>(1,2)*z[22];
    z[35]=z[35]*z[26];
    z[15]=5*z[15];
    z[38]=z[15] - 27;
    z[40]=z[38]*z[18];
    z[35]=z[35] + n<T>(1,8)*z[40];
    z[35]=z[6]*z[35];
    z[40]=n<T>(1,2)*z[18];
    z[38]= - z[38]*z[40];
    z[41]=n<T>(17,2)*z[13];
    z[41]= - z[9]*z[41];
    z[41]=static_cast<T>(3)+ z[41];
    z[41]=z[13]*z[41];
    z[19]= - z[19] + z[41];
    z[19]=z[13]*z[19];
    z[19]= - n<T>(11,2)*z[23] + z[38] + z[19];
    z[19]=z[19]*z[27];
    z[38]=npow(z[12],3);
    z[41]=npow(z[13],3);
    z[42]=z[38] - n<T>(17,4)*z[41];
    z[17]=z[20] + z[17] + z[21] + z[25] + z[19] + n<T>(1,2)*z[42] + z[35];
    z[19]=npow(z[2],2);
    z[17]=z[17]*z[19];
    z[16]= - static_cast<T>(7)- z[16];
    z[16]=z[16]*z[18];
    z[18]=z[13]*z[14];
    z[20]=z[3]*z[6];
    z[21]=3*z[6];
    z[25]= - z[21] + n<T>(17,4)*z[14];
    z[25]=z[6]*z[25];
    z[16]= - 4*z[20] + n<T>(1,2)*z[25] + z[16] + n<T>(17,4)*z[18];
    z[16]=z[3]*z[16];
    z[25]=z[6]*z[10];
    z[35]=npow(z[3],2);
    z[42]=z[35]*z[25];
    z[43]=2*z[35];
    z[44]=z[24]*z[43];
    z[44]= - n<T>(3,4) + z[44];
    z[44]=z[5]*z[3]*z[44];
    z[45]=z[4]*z[6];
    z[46]=z[7]*z[6];
    z[42]=z[44] - z[45] + 4*z[42] + n<T>(33,4)*z[46];
    z[42]=z[5]*z[42];
    z[44]=z[13]*z[12];
    z[37]=n<T>(27,2)*z[44] + 67*z[37];
    z[37]=z[37]*z[29];
    z[47]= - z[34] + 13*z[6];
    z[47]=z[47]*z[27];
    z[33]= - z[4]*z[33];
    z[33]=z[47] + z[33];
    z[33]=z[4]*z[33];
    z[47]=npow(z[14],3);
    z[16]=z[42] + z[33] + z[37] + n<T>(5,8)*z[47] + z[16];
    z[16]=z[5]*z[16];
    z[30]= - n<T>(157,4) - z[30];
    z[30]=z[30]*z[26];
    z[33]=17*z[14];
    z[37]= - z[33] - n<T>(27,2)*z[12];
    z[37]=z[37]*z[36];
    z[30]=z[30] + z[37];
    z[37]= - n<T>(27,4)*z[13] - 17*z[6];
    z[37]=z[37]*z[28];
    z[42]=z[34] - z[21];
    z[48]=n<T>(1,4)*z[4];
    z[42]=z[42]*z[48];
    z[49]=n<T>(1,2)*z[5];
    z[50]=11*z[7];
    z[51]=z[50] + n<T>(5,4)*z[5];
    z[51]=z[51]*z[49];
    z[52]= - n<T>(11,4)*z[6] + 4*z[12] + n<T>(3,4)*z[14];
    z[52]=z[6]*z[52];
    z[30]=z[51] + z[42] + z[37] + n<T>(1,2)*z[30] + z[52];
    z[30]=z[5]*z[30];
    z[34]= - z[10]*z[34];
    z[34]= - static_cast<T>(123)+ z[34];
    z[34]=z[34]*z[36];
    z[33]=z[33] + z[34];
    z[33]=z[13]*z[33];
    z[23]=z[33] - 3*z[23];
    z[33]=n<T>(25,4)*z[13] + 13*z[3];
    z[33]=z[33]*z[28];
    z[34]=n<T>(9,8)*z[13] - z[3];
    z[34]=z[3]*z[34];
    z[36]= - z[50] + 4*z[4];
    z[36]=z[4]*z[36];
    z[23]=z[36] + z[33] + n<T>(1,4)*z[23] + z[34];
    z[23]=z[4]*z[23];
    z[33]=z[39]*z[32];
    z[33]=z[9] + z[33];
    z[33]=z[4]*z[33];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[4]*z[33];
    z[34]=z[9] + n<T>(1,2)*z[10];
    z[34]=z[10]*z[34];
    z[34]=n<T>(1,2)*z[39] + z[34];
    z[34]=z[5]*z[34];
    z[34]=z[34] + z[9] + z[10];
    z[34]=z[5]*z[34];
    z[34]=static_cast<T>(1)+ z[34];
    z[34]=z[5]*z[34];
    z[33]=z[33] + z[34];
    z[34]=n<T>(11,2)*z[8];
    z[33]=z[33]*z[34];
    z[36]=z[7]*z[9];
    z[37]= - z[4]*z[9];
    z[37]=z[37] - static_cast<T>(1)+ z[36];
    z[37]=z[4]*z[37];
    z[37]=z[7] + z[37];
    z[37]=z[4]*z[37];
    z[42]=z[36] + 1;
    z[51]=z[5]*z[42];
    z[51]=z[7] + z[51];
    z[51]=z[5]*z[51];
    z[37]=z[37] + z[51];
    z[33]=z[33] + 2*z[46] + n<T>(11,2)*z[37];
    z[33]=z[8]*z[33];
    z[37]=z[6]*z[13];
    z[51]=91*z[37];
    z[52]=z[3]*z[13];
    z[52]=z[51] - 53*z[52];
    z[52]=z[7]*z[52];
    z[51]=z[12]*z[51];
    z[51]=z[51] + z[52];
    z[23]=z[33] + z[30] + n<T>(1,8)*z[51] + z[23];
    z[23]=z[8]*z[23];
    z[22]=n<T>(191,4) + z[22];
    z[22]=z[22]*z[26];
    z[26]=npow(z[6],2);
    z[22]= - n<T>(5,2)*z[26] + z[22] - n<T>(91,4)*z[44];
    z[22]=z[6]*z[22];
    z[30]=3*z[35];
    z[31]=static_cast<T>(83)+ z[31];
    z[31]=z[31]*npow(z[13],2);
    z[31]=z[31] + z[30];
    z[27]=z[31]*z[27];
    z[31]=z[3] + z[6];
    z[33]=npow(z[7],2);
    z[44]=z[31]*z[33];
    z[22]=n<T>(11,4)*z[44] + z[27] - z[38] + z[22];
    z[22]=z[7]*z[22];
    z[27]=49*z[37];
    z[37]=z[14]*z[27];
    z[37]=n<T>(17,2)*z[41] + z[37];
    z[37]=z[3]*z[37];
    z[22]=n<T>(1,2)*z[37] + z[22];
    z[15]=static_cast<T>(83)- z[15];
    z[15]=z[15]*z[40];
    z[15]=z[15] - 49*z[18];
    z[15]=z[6]*z[15];
    z[18]= - z[27] + 33*z[20];
    z[18]=z[3]*z[18];
    z[15]=z[18] - n<T>(5,2)*z[47] + z[15];
    z[18]=z[7]*z[3];
    z[27]=49*z[18] + n<T>(19,2)*z[45];
    z[27]=z[27]*z[48];
    z[37]=z[13]*z[18];
    z[15]=z[27] + n<T>(1,4)*z[15] - 23*z[37];
    z[15]=z[4]*z[15];
    z[15]=z[23] + z[16] + n<T>(1,2)*z[22] + z[15];
    z[15]=z[15]*z[19];
    z[16]=z[39]*z[7];
    z[16]=z[16] + z[9];
    z[22]=z[4]*z[16];
    z[22]=z[22] + z[42];
    z[22]=z[4]*z[22];
    z[23]=z[7] + z[3];
    z[22]=z[22] + z[23];
    z[22]=z[4]*z[22];
    z[16]=z[5]*z[16];
    z[16]=z[36] + z[16];
    z[16]=z[5]*z[16];
    z[27]=z[7] - z[6];
    z[16]=z[16] + z[27];
    z[16]=z[5]*z[16];
    z[37]= - z[7]*z[31];
    z[16]=z[16] + z[37] + z[22];
    z[16]=z[8]*z[16];
    z[22]= - static_cast<T>(1)- n<T>(3,2)*z[36];
    z[22]=z[4]*z[22];
    z[22]=z[28] + z[22];
    z[22]=z[4]*z[22];
    z[22]=n<T>(1,2)*z[18] + z[22];
    z[22]=z[4]*z[22];
    z[28]=z[36]*z[49];
    z[28]= - z[7] + z[28];
    z[28]=z[5]*z[28];
    z[28]= - n<T>(1,2)*z[46] + z[28];
    z[28]=z[5]*z[28];
    z[16]=n<T>(1,2)*z[16] + z[22] + z[28];
    z[16]=z[16]*z[34];
    z[22]=n<T>(11,2)*z[33];
    z[28]= - z[31]*z[22];
    z[31]=npow(z[6],3);
    z[31]=5*z[31];
    z[34]=npow(z[3],3);
    z[28]=z[28] + z[31] - n<T>(3,2)*z[34];
    z[28]=z[28]*z[29];
    z[37]= - z[3] + z[7];
    z[37]=z[37]*z[50];
    z[30]= - z[30] + z[37];
    z[30]=z[7]*z[30];
    z[37]=3*z[34];
    z[30]=z[37] + z[30];
    z[38]=4*z[3] + n<T>(19,2)*z[7];
    z[38]=z[4]*z[38];
    z[38]= - n<T>(25,2)*z[18] + z[38];
    z[38]=z[4]*z[38];
    z[30]=n<T>(1,8)*z[30] + z[38];
    z[30]=z[4]*z[30];
    z[38]=z[6] + z[7];
    z[38]=z[7]*z[38];
    z[26]=5*z[26] + n<T>(11,2)*z[38];
    z[26]=z[7]*z[26];
    z[38]=z[27]*z[5];
    z[39]= - 35*z[46] + 9*z[38];
    z[39]=z[5]*z[39];
    z[26]=n<T>(3,2)*z[39] + z[31] + z[26];
    z[26]=z[5]*z[26];
    z[16]=z[16] + n<T>(1,4)*z[26] + z[28] + z[30];
    z[16]=z[8]*z[16];
    z[24]= - z[6]*z[24];
    z[24]= - z[10] + z[24];
    z[24]=z[3]*z[24];
    z[24]=z[25] + z[24];
    z[24]=z[3]*z[24];
    z[24]=n<T>(3,4)*z[6] + 2*z[24];
    z[24]=z[3]*z[24];
    z[26]=z[3] - z[6];
    z[28]=n<T>(3,4)*z[4];
    z[30]=z[26]*z[28];
    z[24]=z[30] + z[24] + n<T>(27,8)*z[46];
    z[24]=z[5]*z[24];
    z[28]=z[28]*z[20];
    z[25]=z[25] + 1;
    z[25]=z[25]*z[3];
    z[21]= - z[21] + z[25];
    z[21]=z[21]*z[43];
    z[21]=z[24] + z[21] + z[28];
    z[21]=z[5]*z[21];
    z[24]=5*z[6] - z[3];
    z[24]=z[24]*z[43];
    z[26]=z[26]*z[4];
    z[30]=z[20] - z[26];
    z[30]=z[4]*z[30];
    z[24]=z[24] + n<T>(19,8)*z[30];
    z[24]=z[4]*z[24];
    z[22]=z[22]*z[6];
    z[22]=z[22] - z[31];
    z[30]=z[22]*z[29];
    z[31]=z[34]*z[6];
    z[34]=2*z[31];
    z[21]=z[21] + z[24] - z[34] + z[30];
    z[21]=z[5]*z[21];
    z[24]=z[33]*z[3];
    z[24]=z[37] + 11*z[24];
    z[30]=z[7]*z[24];
    z[20]=n<T>(19,8)*z[20];
    z[33]=z[20] - 4*z[18];
    z[37]=npow(z[4],2);
    z[33]=z[33]*z[37];
    z[30]=z[33] + z[34] - n<T>(1,8)*z[30];
    z[30]=z[4]*z[30];
    z[16]=z[16] + z[30] + z[21];
    z[16]=z[16]*z[19];
    z[21]=z[24]*z[29];
    z[24]=z[37]*z[18];
    z[21]=z[21] + 19*z[24];
    z[21]=z[4]*z[21];
    z[22]= - z[7]*z[22];
    z[24]=npow(z[5],2);
    z[29]=z[24]*z[46];
    z[22]=z[22] - n<T>(71,2)*z[29];
    z[22]=z[22]*z[49];
    z[21]=z[21] + z[22];
    z[22]=z[42]*z[37];
    z[22]=z[18] + z[22];
    z[22]=z[4]*z[22];
    z[29]= - z[24]*z[36];
    z[29]= - z[46] + z[29];
    z[29]=z[5]*z[29];
    z[22]=z[22] + z[29];
    z[22]=z[8]*z[22];
    z[29]= - z[3] - n<T>(3,2)*z[7];
    z[29]=z[29]*z[32];
    z[29]=z[18] + z[29];
    z[29]=z[29]*z[37];
    z[27]= - z[27]*z[49];
    z[27]=z[46] + z[27];
    z[27]=z[27]*z[24];
    z[22]=n<T>(1,4)*z[22] + z[29] + z[27];
    z[22]=z[8]*z[22];
    z[21]=n<T>(1,2)*z[21] + 11*z[22];
    z[21]=z[8]*z[21];
    z[22]=z[4]*z[23];
    z[22]= - z[18] + z[22];
    z[22]=z[22]*z[37];
    z[23]= - z[46] + z[38];
    z[23]=z[23]*z[24];
    z[22]=z[22] + z[23];
    z[22]=z[8]*z[22];
    z[23]=npow(z[5],3);
    z[27]=z[23]*z[46];
    z[18]=z[18]*npow(z[4],3);
    z[18]=z[27] - z[18];
    z[22]=3*z[18] + z[22];
    z[22]=z[22]*npow(z[8],2);
    z[23]=z[4]*z[23]*z[34];
    z[18]=z[18]*npow(z[8],3);
    z[18]=z[23] - n<T>(11,4)*z[18];
    z[18]=z[1]*z[18];
    z[23]= - z[35]*z[26];
    z[23]= - z[31] + z[23];
    z[23]=z[5]*z[23];
    z[26]=z[4]*z[31];
    z[23]=3*z[26] + z[23];
    z[23]=z[23]*z[24];
    z[18]=z[18] + 2*z[23] + n<T>(11,4)*z[22];
    z[18]=z[1]*z[18];
    z[22]= - z[6] + z[25];
    z[22]=z[22]*z[43];
    z[22]=z[22] - z[28];
    z[22]=z[5]*z[22];
    z[23]=2*z[6] - z[3];
    z[23]=z[23]*z[35]*z[4];
    z[23]= - z[31] + z[23];
    z[22]=4*z[23] + z[22];
    z[22]=z[5]*z[22];
    z[20]=z[37]*z[20];
    z[20]=6*z[31] + z[20];
    z[20]=z[4]*z[20];
    z[20]=z[20] + z[22];
    z[20]=z[5]*z[20];
    z[18]=z[18] + z[20] + z[21];
    z[18]=z[1]*z[19]*z[18];
    z[16]=z[16] + z[18];
    z[16]=z[1]*z[16];
    z[15]=z[15] + z[16];
    z[15]=z[1]*z[15];
    z[15]=z[17] + z[15];

    r += z[15]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r914(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r914(const std::array<dd_real,31>&);
#endif
