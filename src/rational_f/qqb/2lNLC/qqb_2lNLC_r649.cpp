#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r649(const std::array<T,31>& k) {
  T z[74];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[8];
    z[11]=k[9];
    z[12]=k[2];
    z[13]=k[3];
    z[14]=k[15];
    z[15]=n<T>(1,2)*z[4];
    z[16]=3*z[4];
    z[17]= - n<T>(17,2)*z[2] + z[16];
    z[17]=z[17]*z[15];
    z[18]=n<T>(1,2)*z[2];
    z[19]=z[15] + z[6] + z[18];
    z[19]=z[11]*z[19];
    z[17]=z[17] + z[19];
    z[19]=npow(z[11],3);
    z[20]=n<T>(1,2)*z[6];
    z[21]=z[9]*z[19]*z[20];
    z[22]=3*z[6];
    z[23]=z[22] + z[18];
    z[24]=npow(z[11],2);
    z[25]=n<T>(1,2)*z[24];
    z[23]=z[23]*z[25];
    z[26]=npow(z[4],2);
    z[27]=z[26]*z[2];
    z[23]=z[21] + z[27] + z[23];
    z[23]=z[9]*z[23];
    z[17]=n<T>(3,2)*z[17] + z[23];
    z[17]=z[9]*z[17];
    z[23]=3*z[2];
    z[28]=z[3]*z[8];
    z[29]=3*z[28];
    z[30]=n<T>(19,2) + z[29];
    z[30]=z[30]*z[23];
    z[31]=9*z[6];
    z[32]= - z[11] + z[31] - n<T>(7,2)*z[4];
    z[32]=z[9]*z[32];
    z[32]=z[32] + static_cast<T>(5)+ n<T>(9,2)*z[28];
    z[32]=z[7]*z[32];
    z[30]=z[32] - n<T>(43,2)*z[4] - n<T>(5,2)*z[6] + z[30];
    z[32]=z[9]*z[6];
    z[33]=z[32]*z[19];
    z[34]=n<T>(1,4)*z[33];
    z[35]=n<T>(1,4)*z[2];
    z[36]=z[6] + z[35];
    z[36]=z[36]*z[24];
    z[36]=z[34] - n<T>(1,2)*z[27] + z[36];
    z[36]=z[9]*z[36];
    z[37]=2*z[4];
    z[38]=z[2] - z[37];
    z[38]=z[4]*z[38];
    z[39]=n<T>(3,2)*z[6];
    z[40]=z[39] + z[2];
    z[41]=z[11]*z[40];
    z[36]=z[36] + z[38] + z[41];
    z[36]=z[9]*z[36];
    z[38]=z[4]*z[8];
    z[41]=3*z[38];
    z[42]=n<T>(5,2) - z[41];
    z[42]=z[4]*z[42];
    z[36]=z[36] + z[6] + n<T>(3,4)*z[42];
    z[36]=z[9]*z[36];
    z[42]=z[6]*z[8];
    z[43]=z[42] + 9*z[38];
    z[36]=n<T>(1,4)*z[43] + z[36];
    z[36]=z[5]*z[36];
    z[17]=z[36] + z[17] + n<T>(1,4)*z[30];
    z[17]=z[5]*z[17];
    z[30]=z[4]*z[2];
    z[36]=npow(z[2],2);
    z[43]= - n<T>(3,2)*z[36] - z[30];
    z[43]=z[4]*z[43];
    z[40]= - z[4] + z[40];
    z[40]=z[40]*z[25];
    z[40]=z[43] + z[40];
    z[40]=z[9]*z[40];
    z[43]=z[6] + z[3];
    z[44]=z[24]*z[9];
    z[45]=z[44] - 3*z[43] + z[4];
    z[46]=5*z[11];
    z[45]= - z[46] + 3*z[45];
    z[47]=z[28] + 3;
    z[31]=z[31] - z[4];
    z[31]=n<T>(3,4)*z[31] + z[11];
    z[31]=z[9]*z[31];
    z[31]=n<T>(3,2)*z[47] + z[31];
    z[31]=z[7]*z[31];
    z[31]=n<T>(1,2)*z[45] + z[31];
    z[45]=n<T>(1,2)*z[7];
    z[31]=z[31]*z[45];
    z[48]= - z[6]*z[3];
    z[47]=z[2]*z[47];
    z[47]= - n<T>(7,2)*z[3] + z[47];
    z[47]=z[2]*z[47];
    z[47]=z[48] + z[47];
    z[48]=n<T>(1,2)*z[3];
    z[49]=n<T>(5,4)*z[2] + z[14] - z[48];
    z[50]=n<T>(3,4)*z[28];
    z[51]= - static_cast<T>(1)- z[50];
    z[51]=z[4]*z[51];
    z[49]=n<T>(3,2)*z[49] + z[51];
    z[49]=z[4]*z[49];
    z[51]=z[39] + z[4];
    z[51]=z[11]*z[51];
    z[17]=z[17] + z[31] + z[40] + z[51] + n<T>(3,4)*z[47] + z[49];
    z[17]=z[5]*z[17];
    z[31]= - n<T>(3,2)*z[32] - static_cast<T>(1)- n<T>(1,2)*z[28];
    z[31]=z[7]*z[31];
    z[40]=n<T>(9,4)*z[6];
    z[47]=n<T>(1,4)*z[11];
    z[31]=z[31] + z[47] - n<T>(3,8)*z[4] + z[3] + z[40];
    z[31]=z[7]*z[31];
    z[49]= - n<T>(3,4)*z[38] + static_cast<T>(1)+ n<T>(3,8)*z[28];
    z[49]=z[4]*z[49];
    z[51]= - static_cast<T>(1)- n<T>(1,4)*z[28];
    z[51]=z[2]*z[51];
    z[49]=z[51] + z[49];
    z[51]= - z[4] + n<T>(3,2)*z[2];
    z[52]=z[51]*z[16];
    z[53]=z[27]*z[9];
    z[52]=z[52] - 2*z[53];
    z[52]=z[9]*z[52];
    z[54]=z[28]*z[7];
    z[49]= - n<T>(3,8)*z[54] + 3*z[49] + z[52];
    z[49]=z[5]*z[49];
    z[52]= - z[47] + z[16] - 4*z[3] - n<T>(75,8)*z[2];
    z[52]=z[4]*z[52];
    z[55]=3*z[53];
    z[56]=3*z[3];
    z[57]=z[2]*z[56];
    z[31]=z[49] + z[31] + z[55] + z[57] + z[52];
    z[31]=z[5]*z[31];
    z[49]=z[32] + 1;
    z[49]=z[49]*z[7];
    z[52]= - n<T>(9,8)*z[49] + z[48] + z[22];
    z[52]=z[7]*z[52];
    z[57]=z[22]*z[3];
    z[58]=z[57] + z[25];
    z[52]=n<T>(1,2)*z[58] + z[52];
    z[52]=z[7]*z[52];
    z[58]=n<T>(19,2)*z[3] - 9*z[2];
    z[58]=z[58]*z[35];
    z[59]=z[48] + 2*z[2];
    z[59]=z[59]*z[4];
    z[58]=z[58] - z[59];
    z[58]=z[4]*z[58];
    z[60]=n<T>(1,4)*z[4];
    z[61]= - z[24]*z[60];
    z[62]=n<T>(3,4)*z[36];
    z[63]=z[3]*z[62];
    z[31]=z[31] + z[52] + z[61] + z[63] + z[58];
    z[31]=z[5]*z[31];
    z[52]=5*z[2];
    z[50]= - static_cast<T>(2)- z[50];
    z[50]=z[4]*z[50];
    z[50]=z[50] + n<T>(11,8)*z[3] + z[52];
    z[50]=z[4]*z[50];
    z[58]=z[2]*z[3];
    z[54]= - z[3] + z[54];
    z[54]=z[7]*z[54];
    z[50]=n<T>(1,8)*z[54] - z[55] - n<T>(5,4)*z[58] + z[50];
    z[50]=z[5]*z[50];
    z[54]=n<T>(5,4)*z[3] + z[23];
    z[54]=z[4]*z[54];
    z[54]= - 4*z[58] + z[54];
    z[54]=z[4]*z[54];
    z[49]=n<T>(3,4)*z[49] - n<T>(1,4)*z[3] - z[22];
    z[55]=npow(z[7],2);
    z[49]=z[49]*z[55];
    z[49]=z[50] + z[54] + n<T>(1,2)*z[49];
    z[49]=z[5]*z[49];
    z[50]= - z[62] - z[30];
    z[50]=z[4]*z[3]*z[50];
    z[54]=npow(z[7],3);
    z[61]=z[54]*z[6];
    z[49]=z[49] + z[50] - n<T>(9,8)*z[61];
    z[49]=z[5]*z[49];
    z[50]=z[2]*z[6];
    z[26]=z[50]*z[26];
    z[62]= - z[6] - z[2];
    z[62]=z[4]*z[62];
    z[62]=n<T>(7,2)*z[50] + z[62];
    z[62]=z[7]*z[62]*z[15];
    z[62]=z[26] + z[62];
    z[55]=z[62]*z[55];
    z[58]=n<T>(7,4)*z[58] - z[59];
    z[58]=z[5]*z[4]*z[58];
    z[59]=z[27]*z[3];
    z[58]=z[58] + z[59] + n<T>(3,8)*z[61];
    z[58]=z[58]*npow(z[5],2);
    z[26]= - z[54]*z[26];
    z[54]= - npow(z[5],3)*z[59];
    z[26]=z[26] + z[54];
    z[26]=z[1]*z[26];
    z[26]=n<T>(1,2)*z[26] + z[55] + z[58];
    z[26]=z[1]*z[26];
    z[54]=z[20]*z[8];
    z[54]=z[54] - 1;
    z[55]=z[54]*z[4];
    z[58]=n<T>(7,2)*z[6] + z[52];
    z[58]=n<T>(1,2)*z[58] + z[55];
    z[58]=z[4]*z[58];
    z[58]=z[53] - n<T>(5,2)*z[50] + z[58];
    z[58]=z[58]*z[45];
    z[59]=n<T>(1,4)*z[6];
    z[61]=z[59] + z[2];
    z[61]=z[4]*z[61];
    z[61]= - 4*z[50] + z[61];
    z[61]=z[4]*z[61];
    z[58]=z[61] + z[58];
    z[58]=z[7]*z[58];
    z[61]=z[59]*z[36];
    z[62]= - z[4]*z[50];
    z[62]=z[61] + z[62];
    z[62]=z[4]*z[62];
    z[58]=z[62] + z[58];
    z[58]=z[7]*z[58];
    z[26]=z[26] + z[58] + z[49];
    z[26]=z[1]*z[26];
    z[49]= - z[4]*z[51];
    z[49]= - z[53] + n<T>(3,2)*z[50] + z[49];
    z[49]=z[9]*z[49];
    z[51]=static_cast<T>(1)- z[42];
    z[51]=n<T>(3,2)*z[51] + z[38];
    z[51]=z[4]*z[51];
    z[51]=z[39] + z[51];
    z[49]=n<T>(1,2)*z[51] + z[49];
    z[49]=z[49]*z[45];
    z[51]= - z[23] + z[60];
    z[51]=z[4]*z[51];
    z[49]=z[49] - z[53] + 3*z[50] + z[51];
    z[49]=z[7]*z[49];
    z[51]=n<T>(15,2)*z[6] + z[2];
    z[51]=z[51]*z[35];
    z[53]= - z[20] - z[2];
    z[53]=z[4]*z[53];
    z[51]=z[51] + z[53];
    z[51]=z[4]*z[51];
    z[49]=z[49] - z[61] + z[51];
    z[49]=z[7]*z[49];
    z[51]=z[56] - z[6];
    z[53]=n<T>(1,2)*z[36];
    z[58]=z[51]*z[53];
    z[43]=z[43]*z[30];
    z[43]=z[58] + z[43];
    z[43]=z[43]*z[15];
    z[58]= - z[10] + z[48];
    z[58]=z[4]*z[58]*z[24];
    z[26]=z[26] + z[31] + z[49] + z[43] + z[58];
    z[26]=z[1]*z[26];
    z[31]=z[18]*z[24];
    z[30]= - z[53] + z[30];
    z[30]=z[4]*z[30];
    z[30]= - z[31] + z[61] + z[30];
    z[30]=z[9]*z[30];
    z[43]=n<T>(15,2)*z[2] - z[4];
    z[43]=z[4]*z[43];
    z[43]= - 9*z[50] + z[43];
    z[49]= - z[18] - z[11];
    z[49]=z[11]*z[49];
    z[43]=n<T>(1,2)*z[43] + z[49];
    z[49]=z[24]*z[35];
    z[49]=z[27] + z[49];
    z[49]=z[9]*z[49];
    z[43]=n<T>(1,2)*z[43] + z[49];
    z[43]=z[9]*z[43];
    z[49]=n<T>(1,2)*z[11];
    z[53]=z[2] + z[11];
    z[53]=z[53]*z[49];
    z[27]=z[27] - z[31];
    z[27]=z[9]*z[27];
    z[31]=z[18] - z[4];
    z[31]=z[4]*z[31];
    z[27]=z[27] + z[31] + z[53];
    z[27]=z[9]*z[27];
    z[31]=n<T>(1,2) - z[38];
    z[31]=z[4]*z[31];
    z[53]=z[11]*z[13];
    z[53]= - static_cast<T>(1)+ z[53];
    z[53]=z[11]*z[53];
    z[31]=z[31] + z[53];
    z[27]=n<T>(1,2)*z[31] + z[27];
    z[27]=z[9]*z[27];
    z[31]=npow(z[13],2);
    z[53]=npow(z[13],4);
    z[58]=z[53]*z[3];
    z[61]=z[11]*z[58];
    z[61]=z[31] + z[61];
    z[61]=z[11]*z[61];
    z[61]= - z[13] + z[61];
    z[61]=z[11]*z[61];
    z[62]=static_cast<T>(5)- z[38];
    z[61]=n<T>(1,2)*z[62] + z[61];
    z[27]=n<T>(1,2)*z[61] + z[27];
    z[27]=z[27]*z[45];
    z[61]=n<T>(1,2)*z[13];
    z[62]=npow(z[13],3);
    z[63]=z[62]*z[3];
    z[64]= - z[11]*z[63];
    z[64]= - z[61] + z[64];
    z[65]=3*z[11];
    z[64]=z[64]*z[65];
    z[64]=static_cast<T>(1)+ z[64];
    z[64]=z[11]*z[64];
    z[66]= - z[3] - z[22];
    z[66]=n<T>(3,2)*z[66] + z[4];
    z[64]=n<T>(1,2)*z[66] + z[64];
    z[27]=z[27] + n<T>(1,2)*z[64] + z[43];
    z[27]=z[7]*z[27];
    z[43]= - z[57] - n<T>(11,2)*z[50];
    z[57]=z[6] + n<T>(9,2)*z[2];
    z[55]=n<T>(1,2)*z[57] + z[55];
    z[55]=z[4]*z[55];
    z[43]=n<T>(1,2)*z[43] + z[55];
    z[55]=z[3]*z[13];
    z[57]=static_cast<T>(1)- z[55];
    z[64]=z[31]*z[3];
    z[66]=z[11]*z[64];
    z[57]=n<T>(1,4)*z[57] + z[66];
    z[57]=z[57]*z[65];
    z[66]=n<T>(3,2)*z[3] - z[2];
    z[57]=n<T>(1,2)*z[66] + z[57];
    z[57]=z[11]*z[57];
    z[27]=z[27] + z[30] + n<T>(1,2)*z[43] + z[57];
    z[27]=z[7]*z[27];
    z[30]= - z[51]*z[35];
    z[43]= - z[3]*z[14];
    z[51]= - z[6]*z[10];
    z[30]=z[30] + z[43] + z[51];
    z[30]=z[2]*z[30];
    z[43]=z[48]*z[13];
    z[51]=z[20]*z[13];
    z[43]=z[43] - z[51];
    z[57]=z[43] + 1;
    z[36]=z[57]*z[36];
    z[57]=z[2]*z[57];
    z[66]=5*z[3] - z[6];
    z[57]=n<T>(1,4)*z[66] + z[57];
    z[57]=z[4]*z[57];
    z[36]=z[36] + z[57];
    z[36]=z[4]*z[36];
    z[57]=z[6]*z[13];
    z[66]=z[57] - z[55];
    z[67]=static_cast<T>(5)+ z[66];
    z[67]=z[4]*z[67];
    z[68]=3*z[12];
    z[69]= - z[68] + 7*z[13];
    z[70]= - z[3]*z[69];
    z[70]= - static_cast<T>(3)+ z[70];
    z[70]=z[70]*z[49];
    z[67]=z[70] + z[48] + z[67];
    z[67]=z[67]*z[49];
    z[70]=z[14] - z[10];
    z[71]=z[20] - z[3] - z[70];
    z[71]=z[4]*z[71];
    z[72]=2*z[14];
    z[73]= - z[10] - z[72];
    z[73]=z[2]*z[73];
    z[67]=z[67] + z[73] + z[71];
    z[67]=z[11]*z[67];
    z[71]=z[2] - z[4];
    z[70]=z[70]*z[71];
    z[44]= - z[70]*z[44];
    z[17]=z[26] + z[17] + z[27] + z[44] + z[67] + z[30] + z[36];
    z[17]=z[1]*z[17];
    z[26]=3*z[14];
    z[27]= - z[15] + z[26] + z[35];
    z[27]=z[4]*z[27];
    z[30]= - n<T>(13,4)*z[6] - 2*z[11];
    z[30]=z[30]*z[24];
    z[30]=z[30] - z[33];
    z[30]=z[9]*z[30];
    z[36]= - 17*z[6] - z[52];
    z[36]= - n<T>(7,2)*z[11] + n<T>(1,4)*z[36] + z[37];
    z[36]=z[11]*z[36];
    z[27]=z[30] + z[27] + z[36];
    z[27]=z[9]*z[27];
    z[30]=z[11] + z[4];
    z[36]=npow(z[9],2);
    z[44]=z[30]*z[36]*z[45];
    z[52]=z[9]*z[25];
    z[37]=z[52] - z[49] - n<T>(27,8)*z[6] + z[37];
    z[37]=z[9]*z[37];
    z[52]= - static_cast<T>(17)- 9*z[28];
    z[37]=z[44] + n<T>(1,8)*z[52] + z[37];
    z[37]=z[7]*z[37];
    z[52]=2*z[6];
    z[67]=z[52] + z[49];
    z[67]=z[67]*z[24];
    z[67]=z[67] + z[21];
    z[67]=z[9]*z[67];
    z[71]=z[18] + z[4];
    z[73]= - z[71]*z[15];
    z[35]=n<T>(7,4)*z[11] + z[22] - z[35];
    z[35]=z[11]*z[35];
    z[35]=z[67] + z[73] + z[35];
    z[35]=z[9]*z[35];
    z[67]=static_cast<T>(1)- z[41];
    z[60]=z[67]*z[60];
    z[35]=z[35] + n<T>(5,2)*z[11] + z[52] + z[60];
    z[35]=z[9]*z[35];
    z[60]=n<T>(9,4)*z[38] + n<T>(13,4) + z[42];
    z[35]=n<T>(1,2)*z[60] + z[35];
    z[35]=z[5]*z[35];
    z[60]=z[52] + z[47];
    z[60]=z[60]*z[24];
    z[60]=z[60] + z[21];
    z[60]=z[9]*z[60];
    z[30]=z[22] - z[2] + z[30];
    z[30]=z[11]*z[30];
    z[67]=z[15] - z[2];
    z[67]=z[67]*z[4];
    z[30]=z[60] + z[67] + z[30];
    z[30]=z[9]*z[30];
    z[60]=13*z[6];
    z[73]=z[60] - 11*z[4];
    z[73]=n<T>(1,2)*z[73] + 7*z[11];
    z[30]=z[35] + n<T>(1,4)*z[73] + z[30];
    z[30]=z[9]*z[30];
    z[35]=n<T>(3,2) + z[42];
    z[35]=5*z[35] - z[41];
    z[30]= - z[44] + n<T>(1,4)*z[35] + z[30];
    z[30]=z[5]*z[30];
    z[35]= - static_cast<T>(1)+ n<T>(3,2)*z[28];
    z[35]=z[35]*z[20];
    z[41]= - n<T>(1,2)*z[38] + static_cast<T>(1)- z[28];
    z[41]=z[4]*z[41];
    z[44]= - n<T>(1,2)*z[14] + z[3];
    z[28]= - static_cast<T>(13)- 7*z[28];
    z[28]=z[2]*z[28];
    z[27]=z[30] + z[37] + z[27] - z[11] + n<T>(3,2)*z[41] + n<T>(3,8)*z[28] + 3*
    z[44] + z[35];
    z[27]=z[5]*z[27];
    z[28]=npow(z[13],5);
    z[30]=z[28]*z[3];
    z[35]=z[53] - z[30];
    z[35]=z[11]*z[35];
    z[35]=z[35] + n<T>(1,2)*z[62] - z[58];
    z[35]=z[35]*z[24];
    z[37]=z[4]*z[71];
    z[41]=z[2]*z[49];
    z[37]=z[37] + z[41];
    z[37]=z[9]*z[37];
    z[41]= - static_cast<T>(1)+ z[38];
    z[41]=z[4]*z[41];
    z[41]=z[41] - z[11];
    z[37]=n<T>(1,2)*z[41] + z[37];
    z[37]=z[9]*z[37];
    z[41]=n<T>(1,4)*z[38];
    z[44]= - z[13]*z[49];
    z[37]=z[37] + z[44] - static_cast<T>(1)- z[41];
    z[37]=z[9]*z[37];
    z[35]=z[37] + n<T>(3,4)*z[13] + z[35];
    z[35]=z[35]*z[45];
    z[37]= - n<T>(3,4)*z[62] + z[58];
    z[37]=z[37]*z[65];
    z[44]=n<T>(1,2)*z[31];
    z[37]=z[37] - z[44] + 2*z[63];
    z[37]=z[37]*z[24];
    z[45]= - z[15] + z[65];
    z[71]= - z[11]*z[2];
    z[67]=z[67] + z[71];
    z[67]=z[9]*z[67];
    z[45]=n<T>(1,4)*z[45] + z[67];
    z[45]=z[9]*z[45];
    z[67]= - 3*z[8] - z[13];
    z[67]=z[3]*z[67];
    z[67]= - static_cast<T>(7)+ z[67];
    z[35]=z[35] + z[45] + n<T>(1,8)*z[67] + z[37];
    z[35]=z[7]*z[35];
    z[37]=3*z[13];
    z[45]= - z[37] - 13*z[64];
    z[67]=n<T>(7,4)*z[31] - 6*z[63];
    z[67]=z[11]*z[67];
    z[45]=n<T>(1,4)*z[45] + z[67];
    z[45]=z[11]*z[45];
    z[67]=z[13]*z[56];
    z[67]= - static_cast<T>(1)+ z[67];
    z[45]=n<T>(1,4)*z[67] + z[45];
    z[45]=z[11]*z[45];
    z[67]=n<T>(5,2)*z[2];
    z[71]= - z[67] + z[4];
    z[71]=z[4]*z[71];
    z[67]=z[67] - z[11];
    z[67]=z[11]*z[67];
    z[67]=z[67] + n<T>(17,4)*z[50] + z[71];
    z[71]=n<T>(1,2)*z[9];
    z[67]=z[67]*z[71];
    z[41]=z[41] + z[54];
    z[41]=z[4]*z[41];
    z[29]=static_cast<T>(5)+ z[29];
    z[29]=z[6]*z[29];
    z[29]=z[2] + 7*z[3] + z[29];
    z[29]=z[35] + z[67] + z[45] + n<T>(1,4)*z[29] + z[41];
    z[29]=z[7]*z[29];
    z[35]= - z[12] + n<T>(3,2)*z[13];
    z[35]=z[35]*z[56];
    z[41]=z[69]*z[55];
    z[41]=z[41] - n<T>(5,2)*z[12] + z[37];
    z[41]=z[11]*z[41];
    z[35]=z[41] - z[57] + static_cast<T>(1)+ z[35];
    z[35]=z[35]*z[49];
    z[41]= - static_cast<T>(3)- z[66];
    z[41]=z[4]*z[41];
    z[23]=z[35] + z[41] + z[23] - z[48] - z[6];
    z[23]=z[11]*z[23];
    z[35]= - n<T>(3,2)*z[8] - z[13];
    z[35]=z[3]*z[35];
    z[35]=z[57] - n<T>(3,2) + z[35];
    z[18]=z[35]*z[18];
    z[35]=z[72]*z[8];
    z[41]=n<T>(3,4) + z[35];
    z[41]=z[3]*z[41];
    z[18]=z[18] + z[20] + 4*z[14] + z[41];
    z[18]=z[2]*z[18];
    z[20]= - z[22] + z[46];
    z[20]=z[20]*z[47];
    z[20]=2*z[70] + z[20];
    z[20]=z[11]*z[20];
    z[41]= - z[10]*z[52];
    z[41]=z[41] - n<T>(3,4)*z[50];
    z[41]=z[2]*z[41];
    z[20]=z[41] + z[20];
    z[20]=z[9]*z[20];
    z[41]= - z[72] - z[39];
    z[41]=z[3]*z[41];
    z[43]=static_cast<T>(1)- z[43];
    z[43]=z[2]*z[43];
    z[43]=z[4] + z[43] + z[59] + n<T>(7,4)*z[3] + n<T>(3,2)*z[10] - z[14];
    z[43]=z[4]*z[43];
    z[17]=z[17] + z[27] + z[29] + z[20] + z[23] + z[43] + z[18] + z[41];
    z[17]=z[1]*z[17];
    z[18]=5*z[6] + z[11];
    z[18]=z[18]*z[25];
    z[20]= - z[10]*z[50];
    z[18]=z[21] + z[20] + z[18];
    z[18]=z[9]*z[18];
    z[20]=3*z[10];
    z[21]= - z[20] - z[14];
    z[15]=z[21]*z[15];
    z[21]=z[11]*z[12];
    z[23]=z[21] + n<T>(3,2);
    z[25]=z[23]*z[65];
    z[25]=7*z[6] + z[25];
    z[25]=z[25]*z[49];
    z[27]=z[10]*z[39];
    z[29]= - z[14] + z[6];
    z[29]=z[2]*z[29];
    z[15]=z[18] + z[25] + z[15] + z[27] + z[29];
    z[15]=z[9]*z[15];
    z[18]= - z[8] - z[13];
    z[18]=z[18]*z[55];
    z[25]= - z[11]*z[62];
    z[25]= - z[31] + z[25];
    z[27]=3*z[24];
    z[25]=z[25]*z[27];
    z[25]= - z[38] + z[25];
    z[25]=z[9]*z[25];
    z[18]=z[25] + z[8] + z[18];
    z[25]=n<T>(3,2)*z[53] - z[30];
    z[25]=z[11]*z[25];
    z[25]=n<T>(3,2)*z[25] + n<T>(11,4)*z[62] - 2*z[58];
    z[25]=z[11]*z[25];
    z[27]=z[31] - z[63];
    z[25]=n<T>(1,2)*z[27] + z[25];
    z[25]=z[11]*z[25];
    z[27]=z[28]*z[48];
    z[29]=npow(z[13],6)*z[48];
    z[28]= - z[28] + z[29];
    z[28]=z[28]*z[49];
    z[27]=z[28] - z[53] + z[27];
    z[27]=z[11]*z[27];
    z[28]=z[53]*z[48];
    z[28]= - z[62] + z[28];
    z[27]=n<T>(1,2)*z[28] + z[27];
    z[27]=z[11]*z[27];
    z[28]=z[53]*z[49];
    z[28]=z[62] + z[28];
    z[28]=z[11]*z[28];
    z[28]=z[44] + z[28];
    z[28]=z[9]*z[28]*z[49];
    z[27]=z[27] + z[28];
    z[27]=z[7]*z[27];
    z[18]=z[27] + z[25] + n<T>(1,4)*z[18];
    z[18]=z[7]*z[18];
    z[25]=z[53]*z[56];
    z[25]= - n<T>(7,4)*z[62] + z[25];
    z[25]=z[11]*z[25];
    z[25]=z[25] - n<T>(7,2)*z[31] + 4*z[63];
    z[25]=z[11]*z[25];
    z[25]=z[25] - z[13] + z[64];
    z[25]=z[11]*z[25];
    z[27]=z[8] + z[37];
    z[27]=z[27]*z[48];
    z[27]=z[38] + static_cast<T>(1)+ z[27];
    z[19]=z[31]*z[19];
    z[28]= - z[9]*z[4]*z[10];
    z[19]= - n<T>(3,2)*z[19] + z[28];
    z[19]=z[19]*z[71];
    z[18]=z[18] + z[19] + n<T>(1,2)*z[27] + z[25];
    z[18]=z[7]*z[18];
    z[19]=5*z[12];
    z[25]=z[19] - z[37];
    z[25]=z[13]*z[25];
    z[27]= - z[69]*z[64];
    z[25]=z[25] + z[27];
    z[25]=z[25]*z[49];
    z[27]=z[68] - 5*z[13];
    z[27]=z[27]*z[55];
    z[19]=z[25] + z[27] + z[19] + z[61];
    z[19]=z[11]*z[19];
    z[25]=z[12] - z[13];
    z[25]=z[25]*z[56];
    z[25]=static_cast<T>(7)+ z[25];
    z[19]=n<T>(1,2)*z[25] + z[19];
    z[19]=z[19]*z[49];
    z[25]=z[49]*z[12];
    z[27]=z[25] - n<T>(3,2)*z[38] + static_cast<T>(1)+ n<T>(5,2)*z[42];
    z[25]=static_cast<T>(1)- z[25];
    z[25]=z[11]*z[25];
    z[25]=z[22] + z[25];
    z[25]=z[11]*z[25];
    z[28]=z[24]*z[32];
    z[25]=z[25] + z[28];
    z[25]=z[25]*z[71];
    z[25]=z[25] + z[40] + z[11];
    z[25]=z[9]*z[25];
    z[25]=n<T>(1,2)*z[27] + z[25];
    z[25]=z[9]*z[25];
    z[27]=z[6] + z[47];
    z[27]=z[27]*z[24];
    z[27]=z[27] + z[34];
    z[27]=z[9]*z[27];
    z[28]=static_cast<T>(3)+ z[21];
    z[28]=z[28]*z[49];
    z[22]=z[22] + z[28];
    z[22]=z[22]*z[49];
    z[22]=z[22] + z[27];
    z[22]=z[9]*z[22];
    z[23]=z[23]*z[49];
    z[22]=z[22] + z[6] + z[23];
    z[22]=z[9]*z[22];
    z[23]=z[21] + static_cast<T>(1)+ z[42];
    z[22]=n<T>(1,4)*z[23] + z[22];
    z[22]=z[5]*z[22]*z[36];
    z[22]=z[25] + z[22];
    z[22]=z[5]*z[22];
    z[23]= - 4*z[6] - n<T>(3,4)*z[11];
    z[23]=z[23]*z[24];
    z[23]=z[23] - n<T>(3,4)*z[33];
    z[23]=z[9]*z[23];
    z[24]=3*z[21];
    z[25]= - static_cast<T>(7)- z[24];
    z[25]=z[11]*z[25];
    z[25]= - z[60] + z[25];
    z[25]=z[11]*z[25];
    z[16]=z[14]*z[16];
    z[16]=z[16] + z[25];
    z[16]=n<T>(1,2)*z[16] + z[23];
    z[16]=z[9]*z[16];
    z[23]= - z[26] - n<T>(13,2)*z[6];
    z[24]= - n<T>(7,2) - z[24];
    z[24]=z[11]*z[24];
    z[16]=z[16] + n<T>(1,2)*z[23] + z[24];
    z[16]=z[9]*z[16];
    z[23]=z[14]*z[12];
    z[23]= - z[38] - n<T>(1,2) + z[23];
    z[21]=3*z[23] - n<T>(5,2)*z[21];
    z[16]=z[22] + n<T>(1,2)*z[21] + z[16];
    z[16]=z[5]*z[16];
    z[21]= - z[8]*z[14];
    z[21]= - n<T>(1,2) + z[21];
    z[21]=z[8]*z[21];
    z[21]=z[21] + z[61];
    z[21]=z[3]*z[21];
    z[21]= - z[51] + z[21] - static_cast<T>(1)- z[35];
    z[21]=z[2]*z[21];
    z[22]= - z[14]*z[68];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=n<T>(1,2)*z[22] + z[35];
    z[22]=z[3]*z[22];
    z[23]=z[8]*z[10];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[39];
    z[20]= - z[20] + z[14];
    z[20]=z[8]*z[20];
    z[20]=static_cast<T>(1)+ n<T>(1,2)*z[20];
    z[20]=z[4]*z[20];
    z[15]=z[17] + z[16] + z[18] + z[15] + z[19] + z[20] + z[21] + z[23]
    + n<T>(3,2)*z[14] + z[22];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r649(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r649(const std::array<dd_real,31>&);
#endif
