#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r389(const std::array<T,31>& k) {
  T z[19];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=k[5];
    z[8]=k[12];
    z[9]=k[14];
    z[10]=k[15];
    z[11]=z[6]*z[2];
    z[12]=z[8]*z[7];
    z[13]= - z[8]*npow(z[7],2);
    z[13]= - z[7] + z[13];
    z[13]=z[9]*z[13];
    z[14]= - z[6]*npow(z[2],2);
    z[14]=z[2] + z[14];
    z[14]=z[5]*z[14];
    z[13]=z[14] + z[13] + z[12] + static_cast<T>(1)+ z[11];
    z[14]=n<T>(1,2)*z[4];
    z[13]=z[13]*z[14];
    z[15]=n<T>(1,2)*z[6];
    z[12]= - static_cast<T>(1)- n<T>(3,2)*z[12];
    z[12]=z[9]*z[12];
    z[16]= - z[2]*z[15];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[5]*z[16];
    z[12]=z[13] + z[16] + z[12] + z[15] + z[8];
    z[12]=z[4]*z[12];
    z[13]= - z[8]*z[15];
    z[16]=z[10] - 2*z[8];
    z[16]=z[9]*z[16];
    z[17]= - z[10] - z[15] + n<T>(1,2)*z[8];
    z[18]=z[5]*z[17];
    z[12]=z[12] + z[18] + z[13] + z[16];
    z[12]=z[4]*z[12];
    z[13]=z[9]*z[8];
    z[16]= - z[7]*z[13];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[5]*z[11];
    z[11]=z[16] + z[11];
    z[11]=z[4]*z[11];
    z[16]= - z[5]*z[6];
    z[11]=z[11] - z[13] + z[16];
    z[11]=z[11]*z[14];
    z[14]=z[6]*z[10];
    z[16]=z[10] + z[6];
    z[16]=z[8]*z[16];
    z[14]= - z[14] + z[16];
    z[14]=z[9]*z[14];
    z[16]=z[6] + n<T>(1,2)*z[10];
    z[16]=z[16]*z[8];
    z[18]=z[15]*z[10];
    z[16]=z[16] - z[18];
    z[18]= - z[5]*z[16];
    z[11]=z[11] + n<T>(1,2)*z[14] + z[18];
    z[11]=z[4]*z[11];
    z[14]=z[5]*z[9];
    z[16]=z[16]*z[14];
    z[11]=z[16] + z[11];
    z[11]=z[1]*z[11];
    z[13]=z[15]*z[13];
    z[14]= - z[17]*z[14];
    z[11]=z[11] + z[12] + z[13] + z[14];

    r += z[11]*npow(z[3],2)*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r389(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r389(const std::array<dd_real,31>&);
#endif
