#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r347(const std::array<T,31>& k) {
  T z[141];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[8];
    z[4]=k[14];
    z[5]=k[11];
    z[6]=k[12];
    z[7]=k[15];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[10];
    z[11]=k[13];
    z[12]=k[9];
    z[13]=k[21];
    z[14]=k[2];
    z[15]=k[3];
    z[16]=npow(z[9],2);
    z[17]=z[16]*z[1];
    z[18]=npow(z[1],3);
    z[19]=3*z[18];
    z[20]= - z[19] + z[17];
    z[20]=z[9]*z[20];
    z[21]=npow(z[1],2);
    z[22]=z[21]*z[9];
    z[23]=z[19] + z[22];
    z[23]=z[9]*z[23];
    z[24]=npow(z[1],4);
    z[25]=3*z[24];
    z[23]=z[25] + z[23];
    z[23]=z[9]*z[23];
    z[26]=npow(z[1],5);
    z[23]=z[26] + z[23];
    z[23]=z[3]*z[23];
    z[27]=2*z[24];
    z[20]=z[23] - z[27] + z[20];
    z[20]=z[3]*z[9]*z[20];
    z[23]=z[9]*z[1];
    z[28]=z[23] + z[21];
    z[29]=z[28]*z[16];
    z[20]= - 3*z[29] + z[20];
    z[20]=z[3]*z[20];
    z[30]=2*z[23];
    z[31]=z[30] + z[21];
    z[32]=2*z[9];
    z[33]=z[31]*z[32];
    z[20]=z[33] + z[20];
    z[33]=3*z[21];
    z[34]=z[33] + z[23];
    z[35]=z[34]*z[9];
    z[36]=z[35] + z[19];
    z[37]=z[36]*z[9];
    z[37]=z[37] + z[24];
    z[38]=4*z[37];
    z[39]=4*z[21];
    z[40]=z[39] + z[23];
    z[41]=z[40]*z[9];
    z[42]=6*z[18];
    z[43]=z[41] + z[42];
    z[44]=z[43]*z[9];
    z[45]=4*z[24];
    z[46]=z[44] + z[45];
    z[47]=z[46]*z[9];
    z[47]=z[47] + z[26];
    z[48]= - z[3]*z[47];
    z[48]=z[38] + z[48];
    z[48]=z[3]*z[16]*z[48];
    z[49]=2*z[21];
    z[50]=z[49] + z[23];
    z[51]=z[50]*z[9];
    z[52]=z[51] + z[18];
    z[53]=z[52]*z[16];
    z[48]= - 6*z[53] + z[48];
    z[48]=z[3]*z[48];
    z[48]=4*z[29] + z[48];
    z[48]=z[2]*z[48];
    z[20]=2*z[20] + z[48];
    z[20]=z[2]*z[20];
    z[48]=5*z[21];
    z[54]=z[48] + z[30];
    z[55]= - z[9]*z[54];
    z[55]= - z[19] + z[55];
    z[55]=z[9]*z[55];
    z[54]=z[54]*z[32];
    z[56]=7*z[18];
    z[54]=z[56] + z[54];
    z[54]=z[54]*z[16];
    z[54]= - z[26] + z[54];
    z[54]=z[3]*z[54];
    z[54]=4*z[55] + z[54];
    z[54]=z[3]*z[54];
    z[55]=6*z[21];
    z[57]=5*z[23];
    z[58]=z[55] + z[57];
    z[59]=3*z[9];
    z[60]=z[58]*z[59];
    z[61]=4*z[18];
    z[54]=z[54] + z[61] + z[60];
    z[54]=z[3]*z[54];
    z[60]=z[19]*z[5];
    z[62]=11*z[21];
    z[20]=z[20] + z[54] - z[60] - z[62] - 8*z[23];
    z[20]=z[2]*z[20];
    z[54]=11*z[18];
    z[63]=z[21]*z[8];
    z[64]=7*z[63];
    z[65]=z[54] - z[64];
    z[65]=z[65]*z[8];
    z[66]=z[56] - z[63];
    z[67]=npow(z[8],2);
    z[68]=z[66]*z[67];
    z[69]= - z[5]*z[68];
    z[69]= - z[65] + z[69];
    z[69]=z[5]*z[69];
    z[70]=z[49]*z[8];
    z[71]=z[70] + z[18];
    z[69]=6*z[71] + z[69];
    z[69]=z[5]*z[69];
    z[72]=z[5]*z[25];
    z[73]=z[18]*z[9];
    z[74]=z[2]*z[73];
    z[72]=z[74] - z[18] + z[72];
    z[72]=z[2]*z[72];
    z[74]=2*z[18];
    z[75]=z[74]*z[67];
    z[76]=z[18]*z[5];
    z[77]=npow(z[8],3);
    z[78]=z[77]*z[76];
    z[78]=z[75] + z[78];
    z[78]=z[5]*z[78];
    z[79]=z[18]*z[8];
    z[78]=z[79] + z[78];
    z[80]=2*z[5];
    z[78]=z[78]*z[80];
    z[81]= - z[2]*z[9];
    z[81]=z[81] + 1;
    z[81]=z[24]*z[81];
    z[82]= - z[9]*z[19];
    z[81]=z[82] + z[81];
    z[81]=z[2]*z[81];
    z[78]=z[78] + z[81];
    z[78]=z[11]*z[78];
    z[69]=z[78] + z[72] + 10*z[21] + z[69];
    z[69]=z[11]*z[69];
    z[72]=z[52]*z[8];
    z[78]=8*z[21];
    z[81]=3*z[23];
    z[82]=z[78] + z[81];
    z[82]=z[82]*z[9];
    z[83]= - z[56] - z[82];
    z[83]=z[9]*z[83];
    z[83]=z[72] - z[27] + z[83];
    z[83]=z[3]*z[83];
    z[84]=z[19] + z[41];
    z[85]=4*z[23];
    z[86]=z[85] + z[48];
    z[87]=z[8]*z[86];
    z[83]=z[83] + 2*z[84] + z[87];
    z[83]=z[3]*z[83];
    z[84]=z[8]*z[1];
    z[87]=2*z[84];
    z[83]=z[83] + z[87] - z[86];
    z[83]=z[3]*z[83];
    z[88]= - z[64] + 25*z[18];
    z[88]=z[88]*z[8];
    z[88]=z[88] - 18*z[24];
    z[66]=z[66]*z[8];
    z[66]= - z[66] + 11*z[24];
    z[66]=z[66]*z[8];
    z[66]=z[66] - 5*z[26];
    z[89]=z[5]*z[66];
    z[89]=z[89] - z[88];
    z[89]=z[5]*z[89];
    z[90]= - 16*z[63] + 23*z[18];
    z[89]=z[89] - z[90];
    z[89]=z[5]*z[89];
    z[91]=z[27] - z[79];
    z[91]=z[91]*z[8];
    z[91]=z[91] - z[26];
    z[91]=4*z[91];
    z[92]=z[25] - z[79];
    z[92]=z[92]*z[8];
    z[92]=z[92] - 3*z[26];
    z[92]=z[92]*z[8];
    z[93]=npow(z[1],6);
    z[92]=z[92] + z[93];
    z[93]=z[5]*z[92];
    z[93]=z[91] + z[93];
    z[93]=z[5]*z[93];
    z[94]=z[79] - z[24];
    z[93]= - 7*z[94] + z[93];
    z[93]=z[5]*z[93];
    z[95]=z[73] + z[24];
    z[96]=2*z[3];
    z[97]=z[95]*z[96];
    z[93]=z[97] - z[74] + z[93];
    z[93]=z[7]*z[93];
    z[97]=z[32]*z[21];
    z[98]=z[97] + z[19];
    z[99]=z[98]*z[96];
    z[100]=14*z[21];
    z[89]=z[93] - z[99] + z[100] + z[89];
    z[89]=z[7]*z[89];
    z[93]=z[7]*z[79];
    z[93]= - z[74] + z[93];
    z[101]=z[67]*z[7];
    z[102]=z[18]*z[101];
    z[103]=z[19]*z[8];
    z[102]= - z[103] + z[102];
    z[102]=z[10]*z[102];
    z[93]=z[102] + 3*z[93];
    z[93]=z[7]*z[93];
    z[102]=3*z[63];
    z[104]=z[102] + z[18];
    z[105]=z[79]*z[3];
    z[106]=z[105] - z[104];
    z[106]=z[3]*z[106];
    z[107]= - z[11]*z[79];
    z[107]=z[18] + z[107];
    z[108]=2*z[11];
    z[107]=z[107]*z[108];
    z[93]=z[107] + z[106] + z[93];
    z[93]=z[10]*z[93];
    z[106]=z[11]*z[95];
    z[106]=z[106] + z[18] + z[22];
    z[106]=z[11]*z[106];
    z[107]=z[63] - z[18];
    z[109]=z[10]*z[79];
    z[109]=z[109] - z[107];
    z[109]=z[10]*z[109];
    z[106]=z[106] + z[109];
    z[109]=z[28]*z[8];
    z[110]=z[28]*z[9];
    z[111]=z[109] + z[110];
    z[111]=z[111]*z[4];
    z[106]= - 2*z[111] + z[87] - z[31] + 3*z[106];
    z[106]=z[4]*z[106];
    z[112]=z[5]*z[45];
    z[56]= - z[56] + z[112];
    z[112]=npow(z[5],2);
    z[56]=z[56]*z[112];
    z[20]=z[106] + z[93] + z[69] + z[20] + z[89] + z[56] + z[83];
    z[20]=z[6]*z[20];
    z[56]=z[110] - z[18];
    z[56]=z[56]*z[9];
    z[56]=z[56] + z[72] - z[24];
    z[69]=z[56]*z[5];
    z[83]=z[97] + z[18];
    z[89]=z[8]*z[50];
    z[89]=z[69] + z[89] + z[83];
    z[89]=z[5]*z[89];
    z[93]=z[23] + z[87] - z[39];
    z[93]=z[93]*z[8];
    z[106]=z[22] - z[18];
    z[93]=z[93] - 2*z[106];
    z[93]=z[93]*z[8];
    z[93]=z[93] + z[73];
    z[93]=z[93]*z[7];
    z[113]=9*z[21];
    z[114]=7*z[84];
    z[115]=z[114] - z[113] + z[23];
    z[115]=z[8]*z[115];
    z[115]=z[93] + z[115] - z[106];
    z[115]=z[7]*z[115];
    z[116]=z[49] - z[84];
    z[116]=z[116]*z[8];
    z[116]=z[116] - z[18];
    z[117]=z[116]*z[101];
    z[118]=6*z[84];
    z[119]=7*z[21];
    z[120]= - z[119] + z[118];
    z[120]=z[8]*z[120];
    z[120]=z[18] + z[120];
    z[120]=z[8]*z[120];
    z[120]=z[120] - z[117];
    z[120]=z[7]*z[120];
    z[121]=z[94]*z[5];
    z[122]=5*z[84];
    z[123]= - z[49] + z[122];
    z[123]=z[8]*z[123];
    z[124]=5*z[18];
    z[120]=z[120] + z[121] + z[124] + z[123];
    z[120]=z[10]*z[120];
    z[123]=z[16]*z[5];
    z[125]=z[52]*z[123];
    z[126]= - z[9]*z[31];
    z[126]=z[18] + z[126];
    z[126]=z[9]*z[126];
    z[126]=z[126] + z[125];
    z[126]=z[5]*z[126];
    z[127]=z[73]*z[7];
    z[126]= - z[127] + z[17] + z[126];
    z[126]=z[11]*z[126];
    z[89]=z[120] + z[126] + z[115] + z[89] + z[23] + z[118];
    z[89]=z[13]*z[89];
    z[115]=z[81] - z[21];
    z[120]=z[115]*z[9];
    z[69]= - z[120] + z[69] + z[61] + z[109];
    z[69]=z[69]*z[5];
    z[120]=z[84] - z[23];
    z[126]= - z[69] - z[120];
    z[126]=z[5]*z[126];
    z[128]= - z[62] + 12*z[84];
    z[128]=z[8]*z[128];
    z[128]= - z[18] + z[128];
    z[128]=z[8]*z[128];
    z[128]=z[128] - 3*z[117];
    z[128]=z[7]*z[128];
    z[129]=z[48] + 11*z[84];
    z[129]=z[8]*z[129];
    z[128]=z[129] + z[128];
    z[128]=z[7]*z[128];
    z[129]= - z[33] + z[84];
    z[104]= - z[5]*z[104];
    z[104]=z[128] + 2*z[129] + z[104];
    z[104]=z[10]*z[104];
    z[128]=z[84] - z[21];
    z[129]=z[8]*z[128];
    z[93]=3*z[93] - z[18] + 12*z[129];
    z[93]=z[7]*z[93];
    z[93]=z[93] - z[128];
    z[93]=z[7]*z[93];
    z[129]=z[81] + z[49];
    z[129]=z[129]*z[9];
    z[129]=z[129] - z[18];
    z[129]=z[129]*z[32];
    z[129]=z[129] - z[125];
    z[130]=z[5]*z[129];
    z[131]=z[33] - z[57];
    z[131]=z[9]*z[131];
    z[130]=z[130] - z[19] + z[131];
    z[130]=z[5]*z[130];
    z[106]=z[127] + 3*z[106];
    z[131]= - z[7]*z[106];
    z[130]=z[130] + z[131];
    z[130]=z[11]*z[130];
    z[131]= - z[87] - z[28];
    z[111]=2*z[131] + z[111];
    z[111]=z[4]*z[111];
    z[131]=2*z[1];
    z[89]=z[89] + z[111] + z[104] + z[130] + z[93] + z[131] + z[126];
    z[89]=z[13]*z[89];
    z[93]=z[28] - z[84];
    z[93]=z[93]*z[8];
    z[41]=z[93] + z[41];
    z[41]=z[41]*z[8];
    z[104]=z[81] + z[119];
    z[104]=z[104]*z[9];
    z[100]= - z[100] - z[57];
    z[100]=z[9]*z[100];
    z[100]= - 13*z[18] + z[100];
    z[100]=z[9]*z[100];
    z[100]= - z[45] + z[100];
    z[100]=z[5]*z[100];
    z[100]=z[100] + z[61] + z[104];
    z[100]=z[9]*z[100];
    z[100]=z[41] + z[100];
    z[100]=z[3]*z[100];
    z[111]=29*z[21] + 20*z[23];
    z[111]=z[9]*z[111];
    z[126]=9*z[18];
    z[111]=z[126] + z[111];
    z[111]=z[9]*z[111];
    z[111]=z[111] - 4*z[125];
    z[111]=z[5]*z[111];
    z[130]=7*z[23];
    z[132]= - z[48] - z[130];
    z[132]=z[9]*z[132];
    z[133]=2*z[28] - z[84];
    z[133]=z[8]*z[133];
    z[100]=z[100] + z[111] + z[132] + 3*z[133];
    z[100]=z[3]*z[100];
    z[111]=z[129]*z[80];
    z[129]= - 17*z[21] - 23*z[23];
    z[129]=z[9]*z[129];
    z[111]=z[111] - 12*z[18] + z[129];
    z[111]=z[5]*z[111];
    z[100]=z[100] + z[111] + 5*z[50] - z[114];
    z[100]=z[3]*z[100];
    z[111]= - z[39]*z[123];
    z[111]=z[111] + z[19] - z[22];
    z[111]=z[5]*z[111];
    z[102]=z[74] - z[102];
    z[129]=z[74] - z[63];
    z[132]=z[8]*z[129];
    z[132]= - z[24] + z[132];
    z[132]=z[3]*z[132];
    z[102]=3*z[102] + z[132];
    z[102]=z[3]*z[102];
    z[132]= - z[16]*z[112];
    z[132]=static_cast<T>(1)+ z[132];
    z[132]=z[18]*z[132];
    z[107]=z[107]*z[8];
    z[133]=z[3]*z[107];
    z[132]=z[133] + z[132];
    z[133]=2*z[2];
    z[132]=z[132]*z[133];
    z[134]=2*z[7];
    z[106]=z[106]*z[134];
    z[102]=z[132] + z[106] + z[102] + z[78] + z[111];
    z[102]=z[2]*z[102];
    z[106]=z[16]*z[84];
    z[111]=z[29] + z[106];
    z[111]=z[8]*z[111];
    z[132]= - z[37]*z[123];
    z[111]=z[132] + z[53] + z[111];
    z[111]=z[111]*z[96];
    z[29]=6*z[29];
    z[132]=z[85] + z[21];
    z[132]=z[132]*z[8];
    z[135]= - 4*z[17] - z[132];
    z[135]=z[8]*z[135];
    z[111]=z[111] + 8*z[125] - z[29] + z[135];
    z[111]=z[3]*z[111];
    z[125]=3*z[1];
    z[135]=z[125]*z[16];
    z[136]=z[23] + z[84];
    z[136]=z[8]*z[136];
    z[137]=z[28]*z[123];
    z[136]= - 6*z[137] + z[135] + z[136];
    z[111]=2*z[136] + z[111];
    z[111]=z[3]*z[111];
    z[136]=z[33]*z[67];
    z[137]=z[21]*z[5];
    z[138]=z[77]*z[137];
    z[138]=z[136] + z[138];
    z[138]=z[5]*z[138];
    z[139]=z[8]*z[34];
    z[135]=z[138] + z[135] + z[139];
    z[135]=z[135]*z[80];
    z[138]=z[70] - z[18];
    z[138]=z[138]*z[8];
    z[138]=z[138] - z[24];
    z[139]=z[3]*z[138];
    z[139]=z[18] + z[139];
    z[139]=z[2]*z[139];
    z[111]=z[139] + z[111] + z[119] + z[135];
    z[111]=z[11]*z[111];
    z[64]=z[64] - z[74];
    z[135]= - z[80]*z[107];
    z[135]=z[135] - z[64];
    z[135]=z[5]*z[135];
    z[139]=z[18]*z[3];
    z[140]= - z[32]*z[139];
    z[121]=z[140] + z[18] + z[121];
    z[121]=z[7]*z[121];
    z[99]=z[121] + z[99] - z[62] + z[135];
    z[99]=z[7]*z[99];
    z[121]= - z[5]*z[67]*z[49];
    z[135]=4*z[63];
    z[121]=z[121] - z[17] - z[135];
    z[121]=z[121]*z[80];
    z[121]=z[121] + 5*z[28] + z[84];
    z[121]=z[5]*z[121];
    z[140]=6*z[1];
    z[99]=z[111] + z[102] + z[99] + z[100] - z[140] + z[121];
    z[99]=z[11]*z[99];
    z[100]=z[48] - z[87];
    z[100]=z[8]*z[100];
    z[102]=z[5]*z[107];
    z[111]=z[128]*z[101];
    z[100]=6*z[111] + z[100] + z[102];
    z[100]=z[7]*z[100];
    z[102]= - z[129]*z[80];
    z[100]=z[100] + 8*z[139] + z[102] - z[119] - z[118];
    z[100]=z[7]*z[100];
    z[94]=z[94]*z[3];
    z[102]= - z[94] + z[64];
    z[102]=z[3]*z[102];
    z[107]= - z[133]*z[107];
    z[64]=z[107] + z[64];
    z[64]=z[2]*z[64];
    z[107]= - z[2] - z[3];
    z[107]=z[138]*z[107];
    z[107]= - z[74] + z[107];
    z[107]=z[11]*z[107];
    z[64]=z[107] + z[64] + 2*z[128] + z[102];
    z[64]=z[11]*z[64];
    z[102]= - z[1]*z[67];
    z[102]=z[18] + z[102];
    z[102]=z[102]*z[67];
    z[107]=z[77]*z[7];
    z[111]=z[116]*z[107];
    z[102]=z[102] + z[111];
    z[102]=z[7]*z[102];
    z[102]= - z[79] + z[102];
    z[102]=z[7]*z[102];
    z[77]=z[77]*z[2];
    z[111]=z[21]*z[77];
    z[111]= - z[136] + z[111];
    z[111]=z[2]*z[111];
    z[102]=z[111] + z[135] + z[102];
    z[102]=z[102]*z[133];
    z[111]=z[21] + 4*z[84];
    z[111]=z[111]*z[67];
    z[116]=z[128]*z[107];
    z[111]=z[111] + 4*z[116];
    z[111]=z[7]*z[111];
    z[111]= - z[70] + z[111];
    z[111]=z[7]*z[111];
    z[116]=z[24]*z[5];
    z[61]=z[61] - z[116];
    z[61]=z[5]*z[61];
    z[118]= - z[11]*z[70];
    z[61]=z[118] + z[102] + z[111] - z[119] + z[61];
    z[61]=z[10]*z[61];
    z[102]=16*z[84];
    z[111]=z[62] - z[102];
    z[111]=z[8]*z[111];
    z[111]=z[124] + z[111];
    z[111]=z[8]*z[111];
    z[111]=z[111] + 11*z[117];
    z[111]=z[7]*z[111];
    z[117]=z[21] - z[122];
    z[117]=z[8]*z[117];
    z[111]=z[117] + z[111];
    z[111]=z[7]*z[111];
    z[117]=z[84] + z[21];
    z[118]=z[2]*z[21]*z[67];
    z[70]= - z[70] + z[118];
    z[70]=z[2]*z[70];
    z[60]=4*z[70] + z[111] + z[60] + z[117];
    z[60]=z[2]*z[60];
    z[70]=z[76] - z[33];
    z[70]=z[70]*z[5];
    z[111]=z[139] - z[119];
    z[111]=z[111]*z[96];
    z[60]=z[61] + z[64] + z[60] + z[100] - z[70] + z[111];
    z[60]=z[10]*z[60];
    z[27]=z[27] + z[73];
    z[27]=z[9]*z[27];
    z[27]=z[26] + z[27];
    z[27]=z[5]*z[27];
    z[27]= - 2*z[95] + z[27];
    z[27]=z[27]*z[80];
    z[61]=z[3]*z[91];
    z[61]=z[61] - z[88];
    z[61]=z[3]*z[61];
    z[64]= - z[3]*z[92];
    z[64]=z[64] - z[66];
    z[64]=z[3]*z[64];
    z[64]= - z[45] + z[64];
    z[64]=z[2]*z[64];
    z[27]=z[64] + z[61] - z[124] + z[27];
    z[27]=z[2]*z[27];
    z[61]=z[74] + z[22];
    z[61]=z[9]*z[61];
    z[61]=z[24] + z[61];
    z[64]=4*z[5];
    z[61]=z[61]*z[64];
    z[61]=z[61] - 14*z[18] - 11*z[22];
    z[61]=z[5]*z[61];
    z[66]=7*z[94] + z[90];
    z[66]=z[3]*z[66];
    z[27]=z[27] + z[66] + z[119] + z[61];
    z[27]=z[2]*z[27];
    z[61]= - z[18]*z[77];
    z[61]=z[75] + z[61];
    z[61]=z[2]*z[61];
    z[61]= - z[79] + z[61];
    z[61]=z[61]*z[133];
    z[66]=z[8]*z[116];
    z[66]= - z[103] + z[66];
    z[66]=z[5]*z[66];
    z[75]=z[79]*z[108];
    z[61]=z[75] + z[66] + z[61];
    z[61]=z[10]*z[61];
    z[66]= - z[2]*z[68];
    z[65]=z[65] + z[66];
    z[65]=z[2]*z[65];
    z[66]=2*z[71] - z[116];
    z[65]=3*z[66] + z[65];
    z[65]=z[2]*z[65];
    z[66]=z[112]*z[79];
    z[68]=13*z[21];
    z[71]=z[11]*z[74];
    z[61]=z[61] + z[71] + z[65] - z[68] + z[66];
    z[61]=z[10]*z[61];
    z[65]=z[47]*z[5];
    z[38]= - z[38] + z[65];
    z[38]=z[5]*z[38];
    z[66]=z[119] + z[30];
    z[66]=z[9]*z[66];
    z[38]=z[38] + z[124] + z[66];
    z[38]=z[80]*z[9]*z[38];
    z[66]=z[37]*z[16];
    z[47]=z[47]*z[123];
    z[47]= - 6*z[66] + z[47];
    z[47]=z[5]*z[47];
    z[47]=11*z[53] + z[47];
    z[47]=z[5]*z[47];
    z[29]= - z[29] + z[47];
    z[29]=z[7]*z[29];
    z[47]= - z[48] + z[85];
    z[47]=z[9]*z[47];
    z[29]=z[29] + z[47] + z[38];
    z[29]=z[7]*z[29];
    z[37]= - 2*z[37] + z[65];
    z[37]=z[5]*z[37];
    z[38]=15*z[23];
    z[47]= - z[78] - z[38];
    z[47]=z[9]*z[47];
    z[37]=z[37] + z[18] + z[47];
    z[37]=z[5]*z[37];
    z[47]= - z[3]*z[74];
    z[29]=z[29] + z[47] + 3*z[115] + z[37];
    z[29]=z[7]*z[29];
    z[37]=z[33]*z[9];
    z[47]=z[127] + z[74] - z[37];
    z[47]=z[7]*z[47];
    z[53]=z[74] - z[105];
    z[53]=z[3]*z[53];
    z[65]=z[67]*z[139];
    z[65]= - z[103] + z[65];
    z[65]=z[3]*z[65];
    z[65]=z[19] + z[65];
    z[65]=z[11]*z[65];
    z[47]=z[65] + 3*z[53] + z[47];
    z[47]=z[11]*z[47];
    z[53]=z[81] + z[21];
    z[52]=z[5]*z[52];
    z[65]=3*z[84];
    z[52]=z[52] - 2*z[53] + z[65];
    z[52]=z[52]*z[80];
    z[27]=z[61] + z[47] + z[27] + z[29] - z[111] - z[1] + z[52];
    z[27]=z[4]*z[27];
    z[29]=z[127] - z[98];
    z[29]=z[7]*z[29];
    z[47]=z[4]*z[83];
    z[29]=z[47] + z[29] - z[28];
    z[29]=z[6]*z[29];
    z[47]=z[18]*z[11];
    z[52]= - z[55] + z[47];
    z[52]=z[11]*z[52];
    z[61]=4*z[1];
    z[66]= - z[10]*z[33];
    z[52]=z[66] - z[61] + z[52];
    z[52]=z[10]*z[52];
    z[66]=z[125]*z[11];
    z[71]=z[21]*z[11];
    z[75]=z[1] + z[71];
    z[75]=z[10]*z[75];
    z[75]=z[66] + z[75];
    z[66]=z[66]*z[10];
    z[77]=npow(z[11],2);
    z[79]= - z[1]*z[77];
    z[79]=z[79] - z[66];
    z[79]=z[15]*z[79];
    z[75]=z[79] + 3*z[75];
    z[75]=z[10]*z[75];
    z[79]=z[77]*z[131];
    z[75]=z[79] + z[75];
    z[75]=z[15]*z[75];
    z[83]= - z[11]*z[23];
    z[83]= - 7*z[1] + z[83];
    z[83]=z[11]*z[83];
    z[52]=z[75] + z[83] + z[52];
    z[52]=z[15]*z[52];
    z[75]= - z[113] - z[47];
    z[75]=z[11]*z[75];
    z[47]= - z[49] - z[47];
    z[47]=z[10]*z[47];
    z[47]=z[47] - z[61] + z[75];
    z[47]=z[10]*z[47];
    z[71]=z[125] + z[71];
    z[71]=z[71]*z[108];
    z[75]= - z[10]*z[11];
    z[75]= - z[77] + z[75];
    z[77]=z[15]*z[1];
    z[75]=z[75]*z[77];
    z[83]=z[11]*z[49];
    z[83]=z[1] + z[83];
    z[83]=z[10]*z[83];
    z[71]=z[75] + z[71] + z[83];
    z[71]=z[10]*z[71];
    z[71]=z[79] + z[71];
    z[71]=z[15]*z[71];
    z[75]= - z[11]*z[50];
    z[75]= - z[140] + z[75];
    z[75]=z[11]*z[75];
    z[47]=z[71] + z[75] + z[47];
    z[47]=z[15]*z[47];
    z[71]=z[18]*z[10];
    z[75]=z[11]*z[19];
    z[75]=z[71] + z[48] + z[75];
    z[75]=z[10]*z[75];
    z[79]=z[11]*z[40];
    z[83]= - z[6]*z[23];
    z[47]=z[47] + z[83] + z[79] + z[75];
    z[47]=z[15]*z[47];
    z[22]= - z[4]*z[22];
    z[22]=z[47] + z[22] + z[21] - z[71];
    z[22]=z[12]*z[22];
    z[47]=z[97] - z[127];
    z[47]=z[7]*z[47];
    z[47]=z[47] + z[40];
    z[47]=z[11]*z[47];
    z[75]=z[49] + z[139];
    z[75]=z[10]*z[75];
    z[79]= - z[3]*z[24];
    z[79]= - z[18] + z[79];
    z[79]=z[3]*z[79];
    z[79]=z[21] + z[79];
    z[79]=z[4]*z[79];
    z[22]=z[22] + z[52] + z[29] + z[79] + z[47] + z[75];
    z[29]=2*z[12];
    z[22]=z[22]*z[29];
    z[47]= - z[58]*z[32];
    z[52]=4*z[31];
    z[75]=z[52] - z[84];
    z[75]=z[8]*z[75];
    z[47]=z[75] - z[124] + z[47];
    z[47]=z[8]*z[47];
    z[34]=z[34]*z[32];
    z[75]= - z[19] + z[34];
    z[75]=z[9]*z[75];
    z[53]= - z[9]*z[53];
    z[79]=z[23]*z[8];
    z[53]=z[53] + z[79];
    z[53]=z[8]*z[53];
    z[53]=z[75] + z[53];
    z[53]=z[8]*z[53];
    z[75]=z[50]*z[16];
    z[83]=5*z[24] - z[75];
    z[83]=z[9]*z[83];
    z[53]=z[83] + z[53];
    z[53]=z[3]*z[53];
    z[75]=z[24] + 3*z[75];
    z[47]=z[53] + 2*z[75] + z[47];
    z[47]=z[3]*z[47];
    z[38]= - z[122] + 16*z[21] + z[38];
    z[38]=z[8]*z[38];
    z[53]= - z[86]*z[59];
    z[38]=z[47] + z[38] - z[54] + z[53];
    z[38]=z[3]*z[38];
    z[47]=z[30] + z[33];
    z[53]=z[47]*z[32];
    z[54]= - z[8]*z[31];
    z[53]=z[54] + z[19] + z[53];
    z[53]=z[8]*z[53];
    z[34]=z[34] + z[19];
    z[54]= - z[34]*z[32];
    z[25]=z[53] - z[25] + z[54];
    z[25]=z[8]*z[25];
    z[40]=z[40]*z[16];
    z[40]=z[40] - z[106];
    z[40]=z[40]*z[8];
    z[43]=z[43]*z[16];
    z[40]=z[40] - z[43];
    z[40]=z[40]*z[8];
    z[43]=z[46]*z[16];
    z[40]=z[40] + z[43];
    z[43]= - z[3]*z[40];
    z[36]=z[36]*z[32];
    z[36]=z[36] + z[24];
    z[46]=z[36]*z[32];
    z[25]=z[43] + z[25] + z[26] + z[46];
    z[25]=z[3]*z[25];
    z[43]=z[48] + z[81];
    z[43]=z[43]*z[32];
    z[46]=z[84] - z[58];
    z[46]=z[8]*z[46];
    z[43]=z[46] + z[126] + z[43];
    z[43]=z[8]*z[43];
    z[46]= - z[124] - 6*z[51];
    z[46]=z[9]*z[46];
    z[25]=z[25] + z[43] - z[45] + z[46];
    z[25]=z[3]*z[25];
    z[28]=z[28]*z[32];
    z[31]=z[84] - z[31];
    z[31]=z[8]*z[31];
    z[28]=z[28] + z[31];
    z[25]=2*z[28] + z[25];
    z[25]=z[2]*z[25];
    z[28]=z[32]*z[18];
    z[31]= - z[24] - z[28];
    z[31]=z[5]*z[31];
    z[31]=z[74] + z[31];
    z[31]=z[31]*z[80];
    z[25]=z[25] + z[38] + z[31] + z[52] - z[114];
    z[25]=z[2]*z[25];
    z[31]=z[65] - z[119] - 6*z[23];
    z[31]=z[8]*z[31];
    z[38]=8*z[18];
    z[43]=z[38] + z[82];
    z[43]=z[9]*z[43];
    z[41]= - z[41] - z[24] + z[43];
    z[41]=z[3]*z[41];
    z[43]= - z[48] - z[23];
    z[43]=z[9]*z[43];
    z[43]=z[74] + z[43];
    z[31]=z[41] + 2*z[43] + z[31];
    z[31]=z[3]*z[31];
    z[31]=z[31] + z[114] - z[68] - z[23];
    z[31]=z[3]*z[31];
    z[30]= - z[102] + 15*z[21] - z[30];
    z[30]=z[8]*z[30];
    z[41]= - z[102] + 32*z[21] - z[130];
    z[41]=z[8]*z[41];
    z[43]=z[9]*z[119];
    z[38]= - z[38] + z[43];
    z[38]=2*z[38] + z[41];
    z[38]=z[8]*z[38];
    z[38]= - 7*z[73] + z[38];
    z[38]=z[7]*z[38];
    z[30]=z[38] + z[30] + z[98];
    z[30]=z[7]*z[30];
    z[38]=z[84] + z[50];
    z[30]=4*z[38] + z[30];
    z[30]=z[7]*z[30];
    z[38]= - z[9]*z[39];
    z[38]= - z[19] + z[38];
    z[38]=z[5]*z[38];
    z[38]=z[33] + z[38];
    z[38]=z[38]*z[80];
    z[25]=z[25] + z[30] + z[31] + 13*z[1] + z[38];
    z[25]=z[2]*z[25];
    z[30]= - z[56]*z[64];
    z[31]= - z[124] - z[104];
    z[31]=z[9]*z[31];
    z[31]= - z[72] - z[24] + z[31];
    z[31]=z[3]*z[31];
    z[30]=z[31] + z[30] + 7*z[110] - 4*z[109];
    z[30]=z[3]*z[5]*z[30];
    z[31]= - z[69] - z[117];
    z[31]=z[31]*z[80];
    z[30]=z[31] + z[30];
    z[30]=z[3]*z[30];
    z[16]=z[131]*z[16];
    z[31]=z[18] + z[16];
    z[31]=2*z[31] - z[132];
    z[31]=z[8]*z[31];
    z[38]= - z[49] + z[23];
    z[38]=z[9]*z[38];
    z[38]= - z[63] + z[19] + z[38];
    z[38]=z[8]*z[38];
    z[28]= - z[24] + z[28];
    z[28]=3*z[28] + z[38];
    z[28]=z[8]*z[28];
    z[38]=6*z[24];
    z[39]= - z[38] - z[44];
    z[39]=z[9]*z[39];
    z[26]=z[28] + z[26] + z[39];
    z[26]=z[5]*z[26];
    z[28]=z[42] + z[35];
    z[28]=z[28]*z[32];
    z[24]=z[26] + z[31] - z[24] + z[28];
    z[24]=z[5]*z[24];
    z[26]=z[122] - z[62] - 16*z[23];
    z[26]=z[8]*z[26];
    z[28]=z[55] + z[130];
    z[28]=z[9]*z[28];
    z[24]=z[24] + z[26] + z[42] + z[28];
    z[24]=z[5]*z[24];
    z[26]= - z[9]*z[34];
    z[28]=z[9]*z[47];
    z[28]=z[28] - z[79];
    z[28]=z[8]*z[28];
    z[26]=z[26] + z[28];
    z[26]=z[8]*z[26];
    z[28]=z[9]*z[36];
    z[26]=z[28] + z[26];
    z[28]= - z[5]*z[40];
    z[26]=3*z[26] + z[28];
    z[26]=z[5]*z[26];
    z[28]=20*z[21] + 11*z[23];
    z[28]=z[9]*z[28];
    z[31]= - z[33] - z[57];
    z[31]=2*z[31] + z[65];
    z[31]=z[8]*z[31];
    z[19]=z[31] + z[19] + z[28];
    z[19]=z[8]*z[19];
    z[18]=10*z[18];
    z[28]= - z[18] - 11*z[51];
    z[28]=z[9]*z[28];
    z[19]=z[26] + z[28] + z[19];
    z[19]=z[5]*z[19];
    z[26]=z[110] - z[93];
    z[19]=6*z[26] + z[19];
    z[19]=z[7]*z[19];
    z[19]=z[19] + z[24] + z[102] - z[21] - 10*z[23];
    z[19]=z[7]*z[19];
    z[24]= - z[135] + z[126] - z[97];
    z[24]=z[8]*z[24];
    z[24]=z[24] - z[38] + z[73];
    z[24]=z[5]*z[24];
    z[26]= - z[78] - z[23];
    z[26]=z[8]*z[26];
    z[18]=z[24] + z[26] + z[18] - z[37];
    z[18]=z[5]*z[18];
    z[24]= - z[33] + z[23];
    z[18]=z[18] + 4*z[24] + z[84];
    z[18]=z[5]*z[18];
    z[18]=z[19] + z[1] + z[18];
    z[18]=z[7]*z[18];
    z[19]=z[113] - 4*z[76];
    z[19]=z[5]*z[19];
    z[19]= - 5*z[1] + z[19];
    z[19]=z[2]*z[19];
    z[24]= - z[11]*z[84];
    z[24]=z[131] + z[24];
    z[26]=2*z[10];
    z[24]=z[24]*z[26];
    z[26]= - z[49] - z[84];
    z[26]=z[13]*z[26];
    z[26]=z[131] + z[26];
    z[26]=z[13]*z[26];
    z[28]=z[11]*z[1];
    z[19]=8*z[26] + z[24] + z[19] + 10*z[28];
    z[19]=z[10]*z[19];
    z[24]= - z[21] - z[76];
    z[24]=z[24]*z[64];
    z[26]=z[2]*z[120];
    z[24]=z[26] + z[1] + z[24];
    z[24]=z[2]*z[24];
    z[26]= - z[5]*z[74];
    z[26]=z[26] + z[49] - z[81];
    z[17]= - z[2]*z[17];
    z[17]=2*z[26] + z[17];
    z[17]=z[2]*z[17];
    z[26]=z[137] + z[1];
    z[17]=3*z[26] + z[17];
    z[17]=z[6]*z[17];
    z[26]= - z[26]*z[64];
    z[17]=z[17] + z[26] + z[24];
    z[17]=z[2]*z[17];
    z[24]=z[76] - z[49];
    z[24]=z[24]*z[5];
    z[24]=z[24] + z[1];
    z[24]=z[24]*npow(z[2],2);
    z[26]=npow(z[13],2)*z[61];
    z[26]=z[26] - z[24] - z[66];
    z[26]=z[10]*z[26];
    z[24]=z[6]*z[24];
    z[24]=z[24] + z[26];
    z[24]=z[15]*z[24];
    z[17]=2*z[24] + z[17] + z[19];
    z[17]=z[15]*z[17];
    z[19]= - z[21] - z[71];
    z[19]=z[10]*z[19];
    z[24]=npow(z[10],2)*z[77];
    z[26]=z[10]*z[49];
    z[26]=z[1] + z[26];
    z[26]=z[10]*z[26];
    z[26]=z[26] - z[24];
    z[26]=z[15]*z[26];
    z[23]= - z[4]*z[23];
    z[19]=z[26] + z[23] + z[1] + z[19];
    z[19]=z[12]*z[19];
    z[23]=z[139] - z[49];
    z[26]=z[23]*z[3];
    z[21]=z[10]*z[21];
    z[21]=z[26] + z[21];
    z[21]=z[10]*z[21];
    z[26]= - z[4]*z[26];
    z[19]=z[19] - z[24] + z[21] + z[26];
    z[19]=z[19]*z[29];
    z[21]=z[23]*z[96];
    z[23]= - z[1]*z[101];
    z[23]= - z[87] + z[23];
    z[23]=z[23]*z[134];
    z[23]=z[23] - z[1] + z[21];
    z[23]=z[7]*z[23];
    z[24]= - z[67] - z[107];
    z[26]=npow(z[7],2);
    z[28]=2*z[26];
    z[24]=z[10]*z[28]*z[1]*z[24];
    z[23]=z[23] + z[24];
    z[23]=z[10]*z[23];
    z[16]=z[26]*z[16];
    z[16]=z[16] - z[1] - z[21];
    z[16]=z[4]*z[7]*z[16];
    z[21]=z[7]*z[120];
    z[21]=z[1] + z[21];
    z[21]=z[21]*z[28];
    z[16]=z[19] + z[16] + z[21] + z[23];
    z[16]=z[14]*z[16];
    z[19]= - z[140] + z[70];
    z[19]=z[5]*z[19];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + z[22] + z[25] + z[27]
       + z[30] + z[60] + 2*z[89] + z[99];
 
    return r;
}

template double qqb_2lNLC_r347(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r347(const std::array<dd_real,31>&);
#endif
