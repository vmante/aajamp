#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r523(const std::array<T,31>& k) {
  T z[80];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[14];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[3];
    z[10]=k[10];
    z[11]=k[9];
    z[12]=k[4];
    z[13]=k[15];
    z[14]=k[17];
    z[15]=npow(z[1],2);
    z[16]=2*z[15];
    z[17]=z[7]*z[1];
    z[18]=z[16] + z[17];
    z[19]=z[18]*z[7];
    z[20]=npow(z[1],3);
    z[21]=z[19] + z[20];
    z[22]=npow(z[7],2);
    z[23]=z[22]*z[3];
    z[24]=z[21]*z[23];
    z[25]=z[15]*z[7];
    z[26]=z[25] + z[20];
    z[27]=z[26]*z[7];
    z[28]=z[27] - z[24];
    z[28]=z[3]*z[28];
    z[29]=z[17] + z[15];
    z[30]=z[29]*z[22];
    z[24]=z[24] - z[30];
    z[31]= - z[6]*z[24];
    z[28]=z[31] - z[25] + z[28];
    z[28]=z[6]*z[28];
    z[31]=z[22]*z[2];
    z[32]=z[21]*z[31];
    z[32]=z[27] + z[32];
    z[32]=z[2]*z[32];
    z[33]=z[21]*z[5];
    z[34]= - z[7] + z[31];
    z[34]=z[34]*z[33];
    z[32]=z[34] + z[32] - z[26];
    z[32]=z[5]*z[32];
    z[34]= - npow(z[3],2);
    z[35]=npow(z[5],2);
    z[34]=z[35] + z[34];
    z[21]=z[21]*z[34];
    z[34]=npow(z[2],2);
    z[35]=z[34]*z[1];
    z[36]= - z[22]*z[35];
    z[37]=z[22]*z[1];
    z[38]=npow(z[6],2);
    z[39]=z[38]*z[37];
    z[21]=z[39] + z[36] + z[21];
    z[21]=z[12]*z[21];
    z[36]= - z[7]*z[29];
    z[36]=z[20] + z[36];
    z[36]=z[7]*z[36];
    z[39]=npow(z[1],4);
    z[36]=z[39] + z[36];
    z[36]=z[3]*z[36];
    z[36]=z[36] + z[26];
    z[36]=z[3]*z[36];
    z[31]=z[18]*z[31];
    z[31]=z[25] + z[31];
    z[31]=z[2]*z[31];
    z[21]=z[21] + z[32] + z[28] + z[31] + z[36];
    z[21]=z[8]*z[21];
    z[28]=2*z[20];
    z[31]=z[28] - z[37];
    z[32]=2*z[7];
    z[36]=z[26]*z[32];
    z[40]=z[36]*z[3];
    z[41]= - z[40] - z[31];
    z[41]=z[3]*z[41];
    z[42]=3*z[1];
    z[43]= - z[22]*z[42];
    z[44]=z[29]*z[23];
    z[43]=z[43] + 4*z[44];
    z[43]=z[6]*z[43];
    z[44]=3*z[17];
    z[45]=z[44] + z[15];
    z[41]=z[43] + z[41] + z[45];
    z[41]=z[6]*z[41];
    z[43]=4*z[2];
    z[46]= - z[30]*z[43];
    z[31]=z[46] - z[31];
    z[31]=z[2]*z[31];
    z[46]=z[2]*z[27];
    z[46]=z[46] - z[26];
    z[47]=2*z[5];
    z[46]=z[46]*z[47];
    z[48]=3*z[29];
    z[31]=z[46] + z[48] + z[31];
    z[31]=z[5]*z[31];
    z[46]=2*z[17];
    z[49]=z[46] + z[15];
    z[50]= - z[2]*z[49]*z[32];
    z[45]=z[50] - z[45];
    z[45]=z[2]*z[45];
    z[50]=4*z[15];
    z[51]= - z[50] - z[17];
    z[51]=z[7]*z[51];
    z[52]=3*z[20];
    z[51]= - z[52] + z[51];
    z[51]=z[3]*z[51];
    z[51]= - z[48] + z[51];
    z[51]=z[3]*z[51];
    z[53]=z[20]*z[5];
    z[54]=z[20]*z[6];
    z[55]=z[54] - z[53];
    z[55]=z[13]*z[55];
    z[56]=z[34] - z[38];
    z[56]=z[12]*z[17]*z[56];
    z[21]=z[21] + 2*z[56] + 3*z[55] + z[31] + z[41] + z[45] + z[51];
    z[21]=z[8]*z[21];
    z[24]=z[13]*z[24];
    z[31]=z[49]*z[7];
    z[24]=z[24] + z[31] + z[40];
    z[24]=z[3]*z[24];
    z[40]=z[6]*z[25];
    z[24]=z[40] - z[44] + z[24];
    z[24]=z[13]*z[24];
    z[33]=z[22]*z[33];
    z[33]=z[30] + z[33];
    z[33]=z[11]*z[33];
    z[40]=z[5]*z[36];
    z[31]=z[33] - z[31] + z[40];
    z[31]=z[5]*z[31];
    z[33]= - z[2]*z[25];
    z[31]= - z[44] + z[33] + z[31];
    z[31]=z[11]*z[31];
    z[33]=z[20]*z[7];
    z[40]=z[33] + 2*z[39];
    z[40]=z[40]*z[7];
    z[41]=npow(z[1],5);
    z[40]=z[40] + z[41];
    z[45]=z[2]*z[40];
    z[45]=z[36] + z[45];
    z[45]=z[3]*z[45];
    z[51]=z[33] + z[39];
    z[55]=2*z[2];
    z[56]= - z[51]*z[55];
    z[45]=z[45] - z[25] + z[56];
    z[45]=z[2]*z[45];
    z[45]= - z[48] + z[45];
    z[45]=z[3]*z[45];
    z[40]=z[6]*z[40];
    z[36]= - z[36] + z[40];
    z[36]=z[5]*z[36];
    z[40]=2*z[6];
    z[48]=z[51]*z[40];
    z[36]=z[36] - z[25] + z[48];
    z[36]=z[6]*z[36];
    z[29]=2*z[29] + z[36];
    z[29]=z[5]*z[29];
    z[36]=z[54] + z[16];
    z[36]=z[36]*z[6];
    z[48]=13*z[15];
    z[51]=z[20]*z[2];
    z[56]= - z[48] + z[51];
    z[56]=z[2]*z[56];
    z[57]=z[3]*z[1];
    z[58]= - z[5]*z[1];
    z[58]= - z[57] + z[58];
    z[58]=z[12]*z[58];
    z[24]=z[31] + z[58] + z[24] + z[29] + z[36] + z[45] - 16*z[1] + 
    z[56];
    z[24]=z[4]*z[24];
    z[29]=z[15]*z[5];
    z[31]= - z[32]*z[29];
    z[45]=z[20]*z[3];
    z[31]=z[31] + z[45] + z[15] - 22*z[17];
    z[31]=z[5]*z[31];
    z[56]=z[15]*z[6];
    z[58]=z[56] - z[29];
    z[58]=z[22]*z[58];
    z[59]=z[58]*z[13];
    z[60]=z[16] - z[17];
    z[60]=z[60]*z[7];
    z[61]=z[6]*z[60];
    z[62]=z[5]*z[19];
    z[61]=z[59] + z[61] + z[62];
    z[61]=z[13]*z[61];
    z[62]=z[6]*z[37];
    z[59]=z[59] - z[15] - z[62];
    z[30]= - z[5]*z[30];
    z[30]= - 25*z[37] + z[30];
    z[30]=z[5]*z[30];
    z[30]=z[30] - 24*z[59];
    z[30]=z[11]*z[30];
    z[59]=z[32]*z[15];
    z[59]=z[59] - z[20];
    z[59]=z[59]*z[2];
    z[62]= - 72*z[17] - z[59];
    z[62]=z[6]*z[62];
    z[63]= - z[51] + z[45];
    z[63]=z[8]*z[63];
    z[64]=z[16]*z[3];
    z[65]=z[15]*z[2];
    z[30]=z[30] + 3*z[63] + 24*z[61] + z[31] + z[62] - z[64] - 45*z[1]
    + z[65];
    z[30]=z[11]*z[30];
    z[31]=z[45]*z[55];
    z[61]= - z[20]*z[55];
    z[61]=z[15] + z[61];
    z[61]=z[2]*z[61];
    z[61]=z[61] - z[31];
    z[61]=z[3]*z[61];
    z[62]=z[15]*z[3];
    z[63]=z[62] - z[42];
    z[66]=3*z[15];
    z[67]=z[66] + 2*z[54];
    z[67]=z[6]*z[67];
    z[68]=z[11]*z[54];
    z[67]=z[68] + z[67] - z[63];
    z[67]=z[11]*z[67];
    z[68]=z[56] + z[1];
    z[68]=z[68]*z[40];
    z[69]= - z[42] + z[65];
    z[69]=z[69]*z[55];
    z[70]=2*z[1];
    z[71]=z[70]*z[34];
    z[72]=z[12]*z[71];
    z[61]=z[67] + z[72] + z[68] + z[69] + z[61];
    z[61]=z[10]*z[61];
    z[67]=z[66] - 4*z[51];
    z[67]=z[2]*z[67];
    z[31]=z[67] - z[31];
    z[31]=z[3]*z[31];
    z[67]=z[17] - z[15];
    z[69]= - z[2]*z[67];
    z[69]= - z[1] + z[69];
    z[43]=z[69]*z[43];
    z[69]=z[2]*z[45];
    z[69]= - z[65] + z[69];
    z[47]=z[69]*z[47];
    z[31]=z[47] + z[43] + z[31];
    z[31]=z[5]*z[31];
    z[43]=z[16]*z[6];
    z[43]=z[43] + z[1];
    z[47]=z[6]*z[43];
    z[69]=z[38]*z[1];
    z[72]= - z[9]*z[69];
    z[47]=z[72] + z[47];
    z[47]=z[10]*z[47];
    z[70]=z[38]*z[70];
    z[47]=z[70] + z[47];
    z[47]=z[9]*z[47];
    z[70]= - z[6]*z[18];
    z[72]=23*z[1];
    z[70]=z[72] + z[70];
    z[70]=z[6]*z[70];
    z[47]=z[47] + z[70];
    z[47]=z[47]*npow(z[11],2);
    z[36]= - z[11]*z[36];
    z[36]= - z[68] + z[36];
    z[36]=z[11]*z[36];
    z[68]=z[45] - z[16];
    z[70]=z[3]*z[68]*z[34];
    z[70]=z[70] + z[35];
    z[36]=z[36] - z[70];
    z[36]=z[10]*z[36];
    z[70]=z[5]*z[70];
    z[36]=z[36] + z[70] + z[47];
    z[36]=z[9]*z[36];
    z[47]= - z[16] - z[51];
    z[47]=z[3]*z[2]*z[47];
    z[70]=z[1]*z[55];
    z[47]=z[70] + z[47];
    z[47]=z[3]*z[47];
    z[47]= - z[69] - z[35] + z[47];
    z[70]=23*z[15];
    z[73]=48*z[17];
    z[74]= - z[70] - z[73];
    z[74]=z[6]*z[74];
    z[75]= - z[5]*z[37];
    z[75]= - z[17] + z[75];
    z[75]=z[5]*z[75];
    z[76]=24*z[1];
    z[74]=z[75] - z[76] + z[74];
    z[74]=z[11]*z[74];
    z[75]=z[76] + z[56];
    z[75]=z[6]*z[75];
    z[77]=z[1] - z[62];
    z[77]=z[5]*z[77];
    z[75]=z[75] + z[77];
    z[74]=2*z[75] + z[74];
    z[74]=z[11]*z[74];
    z[31]=z[36] + z[61] + z[74] + 2*z[47] + z[31];
    z[31]=z[9]*z[31];
    z[36]=z[25] - z[20];
    z[33]=z[33] - z[39];
    z[47]=z[33]*z[55];
    z[61]=z[47] + 3*z[36];
    z[61]=z[61]*z[2];
    z[74]=2*z[3];
    z[75]= - z[74] + z[55];
    z[41]=z[41]*z[2];
    z[41]=z[41] + z[39];
    z[75]=z[41]*z[75];
    z[75]= - z[20] + z[75];
    z[75]=z[3]*z[75];
    z[33]= - z[2]*z[33];
    z[77]= - z[3]*z[41];
    z[33]=z[77] + z[20] + z[33];
    z[33]=z[33]*z[40];
    z[33]=z[33] + z[61] + z[75];
    z[33]=z[6]*z[33];
    z[75]=z[32]*z[20];
    z[75]=z[75] + z[39];
    z[77]=z[51]*z[22];
    z[78]=z[77] - z[75];
    z[78]=z[6]*z[78];
    z[27]=z[27] + z[39];
    z[79]= - z[27]*z[55];
    z[41]=z[41]*z[74];
    z[25]=4*z[25];
    z[41]=z[78] + z[41] + z[79] + z[20] + z[25];
    z[41]=z[6]*z[41];
    z[26]=z[26]*z[55];
    z[78]=z[55]*z[39];
    z[79]= - z[52] - z[78];
    z[79]=z[3]*z[79];
    z[26]=z[41] + z[79] + z[26] + z[15] - z[46];
    z[26]=z[5]*z[26];
    z[41]= - z[15] + z[44];
    z[41]=z[7]*z[41];
    z[41]=z[20] + z[41];
    z[41]=z[41]*z[55];
    z[79]=7*z[15];
    z[41]=z[41] + z[79] - z[17];
    z[41]=z[2]*z[41];
    z[78]=z[20] - z[78];
    z[78]=z[2]*z[78];
    z[39]=z[2]*z[39];
    z[20]=z[20] + z[39];
    z[20]=z[20]*z[74];
    z[20]=z[20] - z[15] + z[78];
    z[20]=z[3]*z[20];
    z[20]=z[26] + z[33] + z[20] - 9*z[1] + z[41];
    z[20]=z[5]*z[20];
    z[26]=z[32]*z[65];
    z[26]=z[26] - 25*z[15] + 24*z[17];
    z[26]=z[2]*z[26];
    z[32]= - z[15] + 7*z[17];
    z[33]= - z[3]*z[60];
    z[32]=3*z[32] + z[33];
    z[32]=z[3]*z[32];
    z[33]=z[15] + 25*z[17];
    z[33]=z[7]*z[33];
    z[18]= - z[18]*z[23];
    z[18]=z[33] + z[18];
    z[18]=z[3]*z[18];
    z[33]= - z[7]*z[51];
    z[33]=24*z[60] + z[33];
    z[33]=z[2]*z[33];
    z[18]=z[33] + z[18];
    z[18]=z[13]*z[18];
    z[33]= - z[15] - z[59];
    z[33]=z[6]*z[33];
    z[39]= - z[5]*z[68];
    z[18]=z[18] + z[39] + z[33] + z[32] - 21*z[1] + z[26];
    z[18]=z[13]*z[18];
    z[26]=z[66] - z[17];
    z[26]=z[2]*z[26];
    z[26]=z[1] + z[26];
    z[26]=z[2]*z[26];
    z[32]=z[3]*z[29];
    z[26]=z[32] + z[26] + z[57];
    z[32]=z[36]*z[55];
    z[32]=z[32] + z[70] - z[73];
    z[32]=z[2]*z[32];
    z[33]=z[3]*z[37];
    z[33]= - z[17] + z[33];
    z[33]=z[3]*z[33];
    z[32]=z[33] + z[76] + z[32];
    z[32]=z[13]*z[32];
    z[26]=2*z[26] + z[32];
    z[26]=z[13]*z[26];
    z[32]= - z[5]*z[45];
    z[32]= - z[64] + z[32];
    z[33]=z[38]*z[5];
    z[32]=z[32]*z[33];
    z[36]=z[50] - z[17];
    z[36]=z[2]*z[36];
    z[36]= - z[72] + z[36];
    z[37]=z[13]*z[2];
    z[36]=z[36]*z[37];
    z[36]= - 6*z[35] + z[36];
    z[36]=z[13]*z[36];
    z[38]=z[12]*npow(z[13],2);
    z[39]= - z[38]*z[71];
    z[41]= - z[3]*z[69];
    z[32]=z[39] + z[36] + z[41] + z[32];
    z[32]=z[12]*z[32];
    z[36]= - z[6]*z[67];
    z[36]= - z[63] + z[36];
    z[36]=z[40]*z[3]*z[36];
    z[39]=z[79] - 2*z[45];
    z[39]=z[3]*z[39];
    z[41]=z[45]*z[40];
    z[39]=z[41] - 4*z[1] + z[39];
    z[39]=z[6]*z[39];
    z[41]= - z[15] + z[45];
    z[41]=2*z[41] + z[54];
    z[40]=z[5]*z[41]*z[40];
    z[39]=z[39] + z[40];
    z[39]=z[5]*z[39];
    z[26]=z[32] + z[26] + z[39] - 5*z[35] + z[36];
    z[26]=z[12]*z[26];
    z[32]=5*z[1];
    z[36]=4*z[65];
    z[39]=z[32] + z[36];
    z[39]=z[2]*z[39];
    z[40]= - z[15] - z[51];
    z[40]=z[40]*z[37];
    z[39]=z[39] + z[40];
    z[39]=z[13]*z[39];
    z[40]=z[55]*z[15];
    z[41]=z[1] + z[40];
    z[37]=z[41]*z[37];
    z[37]= - 4*z[35] + z[37];
    z[37]=z[13]*z[37];
    z[35]= - z[35]*z[38];
    z[35]=z[37] + z[35];
    z[35]=z[12]*z[35];
    z[34]= - z[34]*z[42];
    z[37]= - z[16] - z[53];
    z[33]=z[37]*z[33];
    z[33]=z[35] + z[39] + z[34] + z[33];
    z[33]=z[12]*z[33];
    z[34]=z[62] + z[29];
    z[35]=z[56] + z[65];
    z[37]=z[35] + 11*z[34];
    z[37]=z[4]*z[37];
    z[38]=z[66] - 4*z[54];
    z[38]=z[5]*z[38];
    z[38]=z[38] - z[43];
    z[38]=z[6]*z[38];
    z[39]= - z[1] - z[65];
    z[29]=3*z[39] - z[29];
    z[29]=z[13]*z[29];
    z[39]=z[2]*z[42];
    z[29]=z[37] + z[33] + z[29] + z[39] + z[38];
    z[29]=z[12]*z[29];
    z[33]= - z[45] - z[48] - z[51];
    z[33]=z[3]*z[33];
    z[37]= - z[54] - z[66] + z[45];
    z[37]=z[11]*z[37];
    z[38]=z[15] - 3*z[54];
    z[38]=z[6]*z[38];
    z[39]=z[5]*z[54];
    z[29]=z[37] + z[39] + z[38] + z[33] + 13*z[1] + z[65] + z[29];
    z[29]=z[10]*z[29];
    z[22]=z[22]*z[65];
    z[23]= - z[15]*z[23];
    z[22]=z[22] + z[23] - z[58];
    z[22]=z[13]*z[22];
    z[23]=z[5] + z[3];
    z[19]= - z[19]*z[23];
    z[33]=z[6] + z[2];
    z[37]= - z[60]*z[33];
    z[19]=z[22] + z[37] + z[19];
    z[19]=z[13]*z[19];
    z[22]=z[35] + z[34];
    z[22]=z[4]*z[22];
    z[23]=z[23] - z[33];
    z[23]=z[1]*z[23];
    z[22]=z[22] + z[23];
    z[22]=z[12]*z[22];
    z[23]=z[66] + z[46];
    z[33]=z[5] - z[3];
    z[23]=z[23]*z[33];
    z[33]=z[66] - z[46];
    z[34]= - z[6] + z[2];
    z[33]=z[33]*z[34];
    z[19]=2*z[19] + 8*z[1] + z[22] + z[33] + z[23];
    z[19]=z[14]*z[19];
    z[22]= - 2*z[27] - z[77];
    z[22]=z[2]*z[22];
    z[22]= - z[28] + z[22];
    z[22]=z[3]*z[22];
    z[16]=z[44] - z[16];
    z[22]=z[22] - z[61] - z[16];
    z[22]=z[3]*z[22];
    z[16]= - z[7]*z[16];
    z[16]=z[47] - z[28] + z[16];
    z[16]=z[3]*z[16];
    z[16]=z[16] + z[49];
    z[16]=z[6]*z[16];
    z[16]=z[16] + z[22] + 33*z[1] + z[40];
    z[16]=z[6]*z[16];
    z[22]= - z[2]*z[75];
    z[22]=z[22] - z[52] - z[25];
    z[22]=z[2]*z[22];
    z[22]=z[22] - z[49];
    z[22]=z[3]*z[22];
    z[22]=z[22] - z[1] + z[36];
    z[22]=z[3]*z[22];
    z[15]= - z[15] + 6*z[17];
    z[15]=z[2]*z[15];
    z[15]=z[32] + z[15];
    z[15]=z[2]*z[15];

    r += z[15] + z[16] + z[18] + 12*z[19] + z[20] + z[21] + z[22] + 
      z[24] + z[26] + z[29] + z[30] + z[31];
 
    return r;
}

template double qqb_2lNLC_r523(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r523(const std::array<dd_real,31>&);
#endif
