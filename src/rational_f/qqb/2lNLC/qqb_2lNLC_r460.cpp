#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r460(const std::array<T,31>& k) {
  T z[45];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[3];
    z[7]=k[10];
    z[8]=k[4];
    z[9]=k[14];
    z[10]=k[5];
    z[11]=k[15];
    z[12]=k[9];
    z[13]=k[2];
    z[14]=npow(z[8],2);
    z[15]=2*z[5];
    z[16]=z[14]*z[15];
    z[17]=npow(z[10],2);
    z[18]=2*z[11];
    z[19]=z[17]*z[18];
    z[20]=z[10] - z[6];
    z[21]=z[2]*z[10];
    z[22]=z[20]*z[21];
    z[23]=z[8]*z[11];
    z[24]=z[10]*z[18];
    z[24]=z[24] + z[23];
    z[25]=2*z[8];
    z[24]=z[24]*z[25];
    z[19]=z[16] + z[24] + z[22] - z[10] + z[19];
    z[19]=z[4]*z[19];
    z[22]=z[14]*z[7];
    z[24]=z[22]*z[18];
    z[26]=3*z[2];
    z[20]= - z[20]*z[26];
    z[27]=4*z[8] + z[22];
    z[27]=z[27]*z[15];
    z[28]=z[11]*z[10];
    z[29]= - static_cast<T>(1)+ 3*z[28];
    z[19]=z[19] + z[27] + z[24] + 8*z[23] + 2*z[29] + z[20];
    z[19]=z[4]*z[19];
    z[20]=z[10]*z[9];
    z[27]= - z[12]*z[20];
    z[29]=2*z[23];
    z[30]=z[7]*z[29];
    z[27]=z[30] + 6*z[2] + z[27] + 3*z[11];
    z[30]=4*z[2];
    z[31]=z[10]*z[30];
    z[32]=z[7]*z[8];
    z[31]=6*z[32] - static_cast<T>(3)+ z[31];
    z[31]=z[5]*z[31];
    z[19]=z[19] + 2*z[27] + z[31];
    z[19]=z[4]*z[19];
    z[14]=z[14]*z[5];
    z[14]=z[14] - z[25];
    z[27]=z[3]*z[14];
    z[31]=z[5]*z[8];
    z[27]=z[27] - z[23] + z[31];
    z[27]=z[15]*z[27];
    z[32]=z[2]*z[6];
    z[33]=z[9] - z[18];
    z[33]=z[8]*z[33];
    z[33]=z[33] - static_cast<T>(1)+ 7*z[32];
    z[33]=z[7]*z[33];
    z[34]=11*z[8] - z[16];
    z[34]=z[5]*z[34];
    z[35]=z[32] - 1;
    z[34]=9*z[35] + z[34];
    z[34]=z[4]*z[34];
    z[27]=z[34] + z[33] + z[27];
    z[27]=z[3]*z[27];
    z[33]=3*z[8];
    z[34]=npow(z[11],2);
    z[33]=z[33]*z[34];
    z[36]=z[9] + z[11];
    z[36]=z[8]*z[36];
    z[36]=z[36] - z[35];
    z[36]=z[7]*z[36];
    z[37]=z[2] + z[9] - z[11];
    z[36]=z[36] + 4*z[37] - z[33];
    z[36]=z[7]*z[36];
    z[37]=2*z[2];
    z[38]= - static_cast<T>(1)- z[31];
    z[38]=z[5]*z[38];
    z[38]= - z[37] + z[38];
    z[39]=4*z[32];
    z[40]= - 7*z[31] + static_cast<T>(7)- z[39];
    z[40]=z[4]*z[40];
    z[38]=2*z[38] + z[40];
    z[38]=z[4]*z[38];
    z[33]=z[33] + 4*z[11];
    z[40]=z[15] - z[33];
    z[40]=z[5]*z[40];
    z[27]=z[27] + z[38] + z[36] + z[40];
    z[27]=z[3]*z[27];
    z[36]= - static_cast<T>(2)+ z[31];
    z[36]=z[36]*z[15];
    z[16]= - 7*z[8] + z[16];
    z[16]=z[5]*z[16];
    z[16]=z[16] + static_cast<T>(5)- 3*z[32];
    z[16]=z[4]*z[16];
    z[38]=z[32] + 1;
    z[38]=z[38]*z[7];
    z[16]=z[16] - 3*z[38] + z[36];
    z[16]=z[3]*z[16];
    z[36]=z[8]*z[9];
    z[36]=z[36] - static_cast<T>(1)+ 2*z[32];
    z[36]=z[7]*z[36];
    z[40]=5*z[2];
    z[36]= - z[40] + z[36];
    z[36]=z[7]*z[36];
    z[41]=z[5] - z[2];
    z[41]=z[41]*z[4];
    z[42]=z[30]*z[5];
    z[16]=z[16] + 9*z[41] + z[36] + z[42];
    z[16]=z[3]*z[16];
    z[35]= - z[31] - z[35];
    z[35]=z[4]*z[35];
    z[36]=4*z[5];
    z[26]=z[35] + z[26] - z[36];
    z[26]=z[4]*z[26];
    z[35]= - z[30] + z[5];
    z[35]=z[35]*z[15];
    z[26]=z[35] + z[26];
    z[26]=z[4]*z[26];
    z[35]=z[7]*z[11];
    z[35]=z[35] - z[34];
    z[43]= - z[7]*z[35];
    z[44]=z[2]*z[15];
    z[34]=z[34] + z[44];
    z[34]=z[5]*z[34];
    z[16]=z[16] + z[26] + z[43] + z[34];
    z[16]=z[3]*z[16];
    z[26]=npow(z[5],2);
    z[30]=z[26]*z[30];
    z[34]= - z[42] - z[41];
    z[34]=z[4]*z[34];
    z[34]=z[30] + z[34];
    z[34]=z[4]*z[34];
    z[41]= - z[37] + z[5];
    z[41]=z[41]*z[15];
    z[42]=z[8]*z[15];
    z[42]= - static_cast<T>(7)+ z[42];
    z[42]=z[5]*z[42];
    z[40]=z[40] + z[42];
    z[40]=z[4]*z[40];
    z[38]=z[2] - z[38];
    z[38]=z[7]*z[38];
    z[38]=z[40] + z[38] + z[41];
    z[38]=z[3]*z[38];
    z[40]=z[4]*z[5];
    z[41]=8*z[2];
    z[42]=z[41] - z[5];
    z[42]=z[42]*z[40];
    z[43]= - z[2]*z[26];
    z[42]=z[43] + z[42];
    z[38]=2*z[42] + z[38];
    z[38]=z[3]*z[38];
    z[34]=z[34] + z[38];
    z[34]=z[3]*z[34];
    z[26]=z[26]*z[37];
    z[38]= - 7*z[2] + z[15];
    z[38]=z[38]*z[40];
    z[38]=z[26] + z[38];
    z[38]=z[3]*z[38];
    z[30]= - z[4]*z[30];
    z[30]=z[30] + z[38];
    z[30]=z[3]*z[30];
    z[38]=z[5]*z[2];
    z[40]=z[38]*npow(z[4],3);
    z[30]= - z[40] + z[30];
    z[30]=z[3]*z[30];
    z[42]=z[1]*z[4]*npow(z[3],3)*z[26];
    z[30]=z[30] + z[42];
    z[30]=z[1]*z[30];
    z[30]=z[30] + z[40] + z[34];
    z[30]=z[1]*z[30];
    z[21]=z[21] - 1;
    z[21]=z[21]*z[5];
    z[34]= - z[2] - z[21];
    z[34]=z[4]*z[34];
    z[34]=3*z[38] + z[34];
    z[34]=z[4]*z[34];
    z[26]= - z[26] + z[34];
    z[26]=z[4]*z[26];
    z[34]=z[5]*z[7];
    z[35]=z[35]*z[34];
    z[16]=z[30] + z[16] + z[35] + z[26];
    z[16]=z[1]*z[16];
    z[21]= - z[37] + z[21];
    z[21]=z[21]*z[15];
    z[26]=z[6] + z[10];
    z[26]=z[2]*z[26];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[4]*z[26];
    z[26]= - z[37] + z[26];
    z[26]=z[4]*z[26];
    z[21]=z[21] + z[26];
    z[21]=z[4]*z[21];
    z[26]= - static_cast<T>(1)- z[23];
    z[26]=z[7]*z[26];
    z[26]=z[26] + z[33];
    z[26]=z[26]*z[34];
    z[16]=z[16] + z[27] + z[26] + z[21];
    z[16]=z[1]*z[16];
    z[21]=5*z[23];
    z[26]=z[21] + static_cast<T>(3)- z[39];
    z[26]=z[7]*z[26];
    z[27]= - 4*z[31] + static_cast<T>(4)+ z[21];
    z[27]=z[5]*z[27];
    z[30]= - 13*z[31] + static_cast<T>(11)- 12*z[32];
    z[30]=z[4]*z[30];
    z[14]= - z[5]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[3]*z[14];
    z[14]=2*z[14] + z[30] + z[27] - z[41] + z[26];
    z[14]=z[3]*z[14];
    z[26]=z[6]*z[12];
    z[21]= - z[21] - static_cast<T>(1)- 2*z[26];
    z[21]=z[7]*z[21];
    z[26]=z[12] + z[37];
    z[21]= - z[36] + 2*z[26] + z[21];
    z[21]=z[5]*z[21];
    z[26]=2*z[9];
    z[27]=z[11] + z[12] - z[26];
    z[30]= - z[9]*z[25];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[7]*z[30];
    z[27]=2*z[27] + z[30];
    z[27]=z[7]*z[27];
    z[30]= - z[12]*z[26];
    z[14]=z[16] + z[14] + z[19] + z[21] + z[30] + z[27];
    z[14]=z[1]*z[14];
    z[16]=z[28] + z[23];
    z[16]=z[8]*z[16];
    z[19]=z[10] + z[8];
    z[19]=z[19]*z[31];
    z[17]= - z[4]*z[17];
    z[16]=z[17] + z[19] - z[10] + z[16];
    z[16]=z[4]*z[16];
    z[17]=5*z[12];
    z[19]=z[20]*z[17];
    z[21]= - 4*z[9] - z[19];
    z[21]=z[10]*z[21];
    z[22]=z[22] + 5*z[8] + 3*z[10];
    z[15]=z[22]*z[15];
    z[22]= - z[9] + 6*z[11];
    z[22]=z[8]*z[22];
    z[15]=2*z[16] + z[15] + z[24] + z[21] + z[22];
    z[15]=z[4]*z[15];
    z[16]=z[18] + z[17] - z[26];
    z[16]=z[13]*z[16];
    z[21]=2*z[7];
    z[22]= - z[13] + z[8];
    z[21]=z[22]*z[21];
    z[22]=z[17]*z[6];
    z[16]=z[21] + z[29] - z[22] + z[16];
    z[16]=z[7]*z[16];
    z[21]=npow(z[6],2)*z[17];
    z[21]=z[21] + z[25];
    z[21]=z[7]*z[21];
    z[23]=z[17]*z[10];
    z[22]=static_cast<T>(3)- z[22];
    z[21]=z[21] + 2*z[22] + z[23];
    z[21]=z[5]*z[21];
    z[22]= - z[9]*z[13];
    z[20]= - z[20] + static_cast<T>(1)+ z[22];
    z[18]=z[20]*z[18];
    z[20]= - z[13]*z[17];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[9]*z[20];
    z[14]=z[14] + z[15] + z[21] + z[16] + z[18] - z[19] + z[17] + z[20];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r460(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r460(const std::array<dd_real,31>&);
#endif
