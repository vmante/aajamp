#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1151(const std::array<T,31>& k) {
  T z[146];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=k[5];
    z[8]=k[12];
    z[9]=k[14];
    z[10]=k[11];
    z[11]=k[4];
    z[12]=k[7];
    z[13]=k[15];
    z[14]=k[8];
    z[15]=k[2];
    z[16]=k[19];
    z[17]=k[17];
    z[18]=k[22];
    z[19]=k[26];
    z[20]=npow(z[2],3);
    z[21]=npow(z[3],3);
    z[22]=z[20]*z[21];
    z[23]= - n<T>(19,6)*z[22] - n<T>(7,6);
    z[23]=z[12]*z[23];
    z[24]=n<T>(14,3)*z[17];
    z[25]=z[11]*z[9];
    z[26]=z[25]*z[24];
    z[27]=2*z[10];
    z[28]=z[11]*z[18];
    z[29]=static_cast<T>(1)- z[28];
    z[29]=z[29]*z[27];
    z[30]=7*z[18];
    z[24]= - 9*z[18] - z[24];
    z[24]=z[11]*z[24];
    z[24]=n<T>(11,3) + z[24];
    z[24]=z[5]*z[24];
    z[23]=z[24] + z[29] + z[26] + z[30] + n<T>(4,3)*z[9] + z[23];
    z[23]=z[8]*z[23];
    z[24]=npow(z[17],2);
    z[26]=2*z[24];
    z[29]=npow(z[18],2);
    z[31]=9*z[29];
    z[32]=z[31] - z[26];
    z[32]=z[11]*z[32];
    z[33]=2*z[11];
    z[34]= - n<T>(7,3)*z[18] - z[17];
    z[34]=z[34]*z[33];
    z[34]=n<T>(5,3) + z[34];
    z[34]=z[5]*z[34];
    z[35]=2*z[14];
    z[36]= - 193*z[18] - 49*z[17];
    z[32]=z[34] + 7*z[13] + z[32] + n<T>(1,6)*z[36] + z[35];
    z[32]=z[5]*z[32];
    z[34]=4*z[24];
    z[36]=z[34] + n<T>(13,3)*z[29];
    z[37]=npow(z[14],3);
    z[38]=2*z[37];
    z[39]=n<T>(1,3)*z[24];
    z[40]=z[9]*z[39];
    z[40]=z[40] + z[38];
    z[40]=z[40]*z[33];
    z[41]=z[29]*z[33];
    z[41]=n<T>(11,2)*z[18] + z[41];
    z[41]=z[10]*z[41];
    z[42]=z[10] - z[12];
    z[43]= - z[16]*z[42];
    z[44]=2*z[43];
    z[45]=z[22]*z[44];
    z[46]=npow(z[14],2);
    z[47]= - 11*z[18] + n<T>(19,2)*z[17];
    z[47]=z[9]*z[47];
    z[48]=4*z[14];
    z[49]= - z[18] - z[48];
    z[49]=z[12]*z[49];
    z[50]=3*z[14];
    z[51]= - n<T>(34,3)*z[17] + z[50];
    z[51]=2*z[51] + z[10];
    z[51]=z[13]*z[51];
    z[23]=z[23] + z[45] + z[32] + z[51] + z[41] + z[49] + z[40] + 5*
    z[46] + z[47] - z[36];
    z[23]=z[8]*z[23];
    z[32]=z[8]*z[9];
    z[40]=z[21]*z[2];
    z[41]=npow(z[13],2);
    z[45]=95*z[32] - 64*z[41] + 19*z[40];
    z[47]=n<T>(1,3)*z[8];
    z[45]=z[45]*z[47];
    z[49]=z[46]*z[10];
    z[51]=z[46]*z[12];
    z[52]=z[49] - z[51];
    z[53]=z[9]*z[15];
    z[54]=z[21]*z[53];
    z[55]=13*z[12];
    z[56]=13*z[10];
    z[57]=z[56] + 64*z[6] - z[55];
    z[57]=z[57]*z[41];
    z[58]=z[21]*z[9];
    z[59]=z[21]*z[8];
    z[60]= - n<T>(33,2)*z[58] + n<T>(68,3)*z[59];
    z[60]=z[7]*z[60];
    z[61]=z[6]*z[40];
    z[45]=z[60] + z[45] + n<T>(49,3)*z[61] + n<T>(1,3)*z[57] - 9*z[54] + z[52];
    z[45]=z[7]*z[45];
    z[54]=2*z[12];
    z[57]=n<T>(1,3)*z[6];
    z[60]= - z[57] + z[19] - n<T>(4,3)*z[14];
    z[60]=z[60]*z[54];
    z[61]=z[10]*z[14];
    z[60]=z[60] + 5*z[61];
    z[62]=npow(z[15],2);
    z[63]=z[62]*z[9];
    z[64]=z[21]*z[63];
    z[65]=n<T>(2,3)*z[13];
    z[66]=4*z[12];
    z[67]=35*z[6] - z[66];
    z[67]=2*z[67] + 11*z[10];
    z[67]=z[67]*z[65];
    z[68]=npow(z[2],2);
    z[69]=z[68]*z[21];
    z[70]=z[6]*z[69];
    z[71]= - 25*z[9] + 136*z[13];
    z[71]= - n<T>(55,6)*z[8] + n<T>(1,3)*z[71] - 13*z[69];
    z[71]=z[8]*z[71];
    z[45]=z[45] + z[71] - n<T>(179,3)*z[70] + z[67] + 2*z[60] - n<T>(31,3)*z[64]
   ;
    z[45]=z[7]*z[45];
    z[60]=n<T>(5,3)*z[12];
    z[64]=n<T>(2,3)*z[6];
    z[67]= - z[19] - z[9];
    z[70]=4*z[53];
    z[71]=static_cast<T>(1)+ z[70];
    z[71]=z[14]*z[71];
    z[67]= - 9*z[10] + z[60] + z[64] + 2*z[67] + z[71];
    z[71]=z[21]*z[15];
    z[72]=2*z[6];
    z[73]= - z[13]*z[72];
    z[73]=z[71] + z[73];
    z[74]=13*z[40];
    z[73]=14*z[73] + z[74];
    z[75]= - z[9] + n<T>(7,3)*z[13];
    z[75]=4*z[75] - z[47];
    z[75]=z[8]*z[75];
    z[76]=npow(z[8],2);
    z[77]=z[76]*z[9];
    z[78]=13*z[21] + 4*z[77];
    z[79]=n<T>(1,3)*z[7];
    z[78]=z[78]*z[79];
    z[73]=z[78] + n<T>(1,3)*z[73] + z[75];
    z[73]=z[7]*z[73];
    z[75]=z[62]*z[21];
    z[78]=z[27] + 7*z[75];
    z[74]=14*z[71] + z[74];
    z[74]=z[2]*z[74];
    z[74]=2*z[78] + z[74];
    z[73]=n<T>(1,3)*z[74] + z[73];
    z[73]=z[7]*z[73];
    z[74]=n<T>(13,3)*z[10];
    z[78]=n<T>(11,3)*z[6];
    z[80]=z[19] - z[78];
    z[80]= - n<T>(10,3)*z[5] + 2*z[80] + z[74];
    z[80]=z[5]*z[80];
    z[81]=z[10] - z[6];
    z[82]=npow(z[5],2);
    z[83]=n<T>(4,3)*z[82];
    z[84]= - z[81]*z[83];
    z[84]= - 11*z[21] + z[84];
    z[84]=z[2]*z[84];
    z[80]=z[84] + n<T>(14,3)*z[71] + z[80];
    z[80]=z[2]*z[80];
    z[75]=n<T>(13,3)*z[5] - z[27] + n<T>(7,3)*z[75];
    z[75]=2*z[75] + z[80];
    z[75]=z[2]*z[75];
    z[80]=z[10]*z[47]*z[68];
    z[84]=z[5]*z[15];
    z[85]=static_cast<T>(1)- n<T>(5,3)*z[84];
    z[73]=z[73] - z[80] + 2*z[85] + z[75];
    z[75]=2*z[4];
    z[73]=z[73]*z[75];
    z[85]=n<T>(1,3)*z[5];
    z[86]= - 17*z[6] - z[27];
    z[86]=z[86]*z[85];
    z[71]= - 27*z[71] + z[86];
    z[71]=z[5]*z[71];
    z[86]=z[21]*z[5];
    z[87]=z[6]*z[21];
    z[87]=139*z[87] + 61*z[86];
    z[87]=z[2]*z[87];
    z[71]=z[71] + n<T>(1,2)*z[87];
    z[71]=z[2]*z[71];
    z[87]=n<T>(31,3)*z[62];
    z[88]=z[21]*z[87];
    z[89]=12*z[19];
    z[88]=n<T>(20,3)*z[5] + z[88] + n<T>(19,3)*z[10] - z[89] + n<T>(17,3)*z[6];
    z[88]=z[5]*z[88];
    z[71]=z[88] + z[71];
    z[71]=z[2]*z[71];
    z[88]=n<T>(5,3)*z[10];
    z[90]= - z[88] - n<T>(3,2)*z[69];
    z[90]=z[2]*z[90];
    z[80]=z[90] - z[80];
    z[80]=z[8]*z[80];
    z[90]=2*z[84];
    z[91]=z[48]*z[15];
    z[92]= - z[90] + static_cast<T>(15)- z[91];
    z[92]=z[5]*z[92];
    z[45]=z[73] + z[45] + z[80] + z[71] + 2*z[67] + z[92];
    z[45]=z[4]*z[45];
    z[67]=n<T>(1,3)*z[12];
    z[71]=n<T>(1,3)*z[10];
    z[73]=z[71] - 6*z[6] - z[67];
    z[80]=npow(z[13],3);
    z[73]=z[73]*z[80];
    z[92]=z[37]*z[12];
    z[93]=z[37]*z[10];
    z[73]=z[73] + z[92] - z[93];
    z[94]=z[8]*z[2];
    z[95]=z[21]*z[94];
    z[95]=24*z[80] - n<T>(13,2)*z[95];
    z[95]=z[8]*z[95];
    z[58]= - n<T>(139,3)*z[58] - 13*z[59];
    z[96]=z[7]*z[8];
    z[58]=z[58]*z[96];
    z[58]=n<T>(1,2)*z[58] + 4*z[73] + z[95];
    z[58]=z[7]*z[58];
    z[73]=3*z[46];
    z[95]=npow(z[6],2);
    z[97]=n<T>(8,3)*z[95];
    z[98]=z[73] - z[97];
    z[98]=z[12]*z[98];
    z[99]=z[88] - 56*z[6] - n<T>(13,3)*z[12];
    z[99]=z[99]*z[41];
    z[100]=n<T>(13,2)*z[69];
    z[101]=28*z[9];
    z[102]=z[101] - z[100];
    z[102]=z[8]*z[102];
    z[102]= - n<T>(128,3)*z[41] + z[102];
    z[102]=z[8]*z[102];
    z[58]=z[58] + z[102] + z[99] + z[98] + 9*z[49];
    z[58]=z[7]*z[58];
    z[98]=8*z[19];
    z[99]=z[72] + z[48] - z[98] + z[9];
    z[99]=z[12]*z[99];
    z[102]=n<T>(1,2)*z[12];
    z[89]= - n<T>(25,6)*z[5] + z[56] + z[102] + z[89] - n<T>(133,6)*z[6];
    z[89]=z[5]*z[89];
    z[103]=n<T>(14,3)*z[6];
    z[104]=z[103] - 3*z[10];
    z[104]=z[104]*z[82];
    z[105]=z[67] - z[27];
    z[106]= - n<T>(57,2)*z[6] + z[105];
    z[106]=z[106]*z[21];
    z[106]=z[106] + z[86];
    z[106]=z[5]*z[106]*z[68];
    z[104]=z[104] + z[106];
    z[104]=z[2]*z[104];
    z[22]=z[105]*z[22];
    z[69]= - z[27] + n<T>(19,6)*z[69];
    z[69]=z[2]*z[69];
    z[69]= - n<T>(19,2) + z[69];
    z[69]=z[8]*z[69];
    z[105]= - n<T>(73,2)*z[9] - 10*z[12];
    z[22]=z[69] + z[22] + n<T>(67,6)*z[5] + n<T>(110,3)*z[13] + n<T>(1,3)*z[105] - 
   n<T>(15,2)*z[10];
    z[22]=z[8]*z[22];
    z[69]=4*z[9];
    z[105]=7*z[14];
    z[106]= - z[69] - z[105];
    z[106]=z[14]*z[106];
    z[95]=z[106] + n<T>(4,3)*z[95];
    z[106]=5*z[6];
    z[107]= - z[106] - z[67];
    z[107]=z[13]*z[107];
    z[22]=z[45] + z[58] + z[22] + z[104] + z[89] + 10*z[107] - 12*z[61]
    + 2*z[95] + z[99];
    z[22]=z[4]*z[22];
    z[45]=n<T>(14,3)*z[62];
    z[58]=npow(z[3],2);
    z[89]=npow(z[58],2);
    z[95]=z[89]*z[45];
    z[99]= - z[57] + z[27];
    z[99]=z[5]*z[99];
    z[95]=z[95] + z[99];
    z[95]=z[5]*z[95];
    z[99]=z[89]*z[84];
    z[104]=z[89]*z[5];
    z[107]=z[89]*z[6];
    z[108]=41*z[107] + 13*z[104];
    z[109]=n<T>(1,3)*z[2];
    z[108]=z[108]*z[109];
    z[99]= - 6*z[99] + z[108];
    z[99]=z[2]*z[99];
    z[95]=z[95] + z[99];
    z[95]=z[2]*z[95];
    z[99]= - 14*z[10] + 5*z[5];
    z[99]=z[99]*z[85];
    z[95]=z[99] + z[95];
    z[95]=z[2]*z[95];
    z[99]= - z[89]*z[63];
    z[108]=z[68]*z[107];
    z[99]=z[99] + z[108];
    z[108]=z[68]*z[89];
    z[108]= - 2*z[108] + z[32];
    z[108]=z[8]*z[108];
    z[99]=14*z[99] + z[108];
    z[108]=z[89]*z[53];
    z[110]=z[89]*z[8];
    z[111]=z[2]*z[110];
    z[108]= - n<T>(7,3)*z[108] + 2*z[111];
    z[111]=z[89]*z[9];
    z[112]= - n<T>(13,3)*z[111] + 4*z[110];
    z[112]=z[7]*z[112];
    z[108]=2*z[108] + z[112];
    z[108]=z[7]*z[108];
    z[99]=n<T>(1,3)*z[99] + z[108];
    z[99]=z[7]*z[99];
    z[108]= - 7*z[107] - z[110];
    z[108]=z[20]*z[108];
    z[99]=2*z[108] + z[99];
    z[99]=z[7]*z[99];
    z[108]=npow(z[2],4);
    z[112]=z[108]*z[89];
    z[113]= - z[47]*z[112];
    z[114]=z[27] - z[5];
    z[95]=z[99] + z[113] + n<T>(4,3)*z[114] + z[95];
    z[95]=z[95]*z[75];
    z[99]=z[76]*z[89];
    z[99]=n<T>(19,2)*z[99];
    z[113]= - z[2]*z[99];
    z[111]= - 68*z[111] - n<T>(19,2)*z[110];
    z[111]=z[111]*z[96];
    z[111]=z[113] + z[111];
    z[111]=z[7]*z[111];
    z[99]= - z[68]*z[99];
    z[99]=z[99] + z[111];
    z[79]=z[99]*z[79];
    z[99]=z[46]*z[27];
    z[111]=z[89]*z[20];
    z[111]=55*z[9] - 19*z[111];
    z[111]=z[111]*z[76];
    z[79]=z[79] + z[99] + n<T>(1,6)*z[111];
    z[79]=z[7]*z[79];
    z[99]=n<T>(11,2)*z[6];
    z[111]=5*z[10];
    z[113]=z[99] + z[111];
    z[113]=z[113]*z[82];
    z[104]= - n<T>(76,3)*z[107] - n<T>(1,2)*z[104];
    z[104]=z[5]*z[104]*z[20];
    z[104]=n<T>(1,3)*z[113] + z[104];
    z[104]=z[2]*z[104];
    z[107]=n<T>(1,2)*z[13];
    z[107]=z[42]*z[107];
    z[113]=n<T>(1,6)*z[5];
    z[56]= - z[56] - 11*z[5];
    z[56]=z[56]*z[113];
    z[114]=z[108]*z[110];
    z[114]=n<T>(2,3)*z[114] - n<T>(1,2)*z[10];
    z[114]=z[8]*z[114];
    z[115]=z[12]*z[14];
    z[116]=z[69] - z[14];
    z[116]=z[14]*z[116];
    z[56]=z[95] + z[79] + z[114] + z[104] + z[56] + z[107] + n<T>(17,6)*
    z[61] + z[116] + n<T>(1,2)*z[115];
    z[56]=z[4]*z[56];
    z[79]=n<T>(2,3)*z[112];
    z[81]=z[81]*z[79]*z[82];
    z[95]=z[13]*z[10];
    z[104]= - z[14] + z[13];
    z[104]=z[5]*z[104];
    z[104]=z[104] + z[115] - z[95];
    z[107]= - z[10]*z[79];
    z[107]=n<T>(19,2)*z[9] + z[107];
    z[107]=z[8]*z[107];
    z[104]=n<T>(1,2)*z[104] + z[107];
    z[104]=z[8]*z[104];
    z[107]=z[38] + 5*z[49];
    z[112]= - z[13]*z[12];
    z[112]=z[61] + z[112];
    z[114]=3*z[6];
    z[116]= - z[114] + n<T>(7,3)*z[10];
    z[116]=z[5]*z[116];
    z[112]=n<T>(1,2)*z[112] + z[116];
    z[112]=z[5]*z[112];
    z[116]=z[89]*npow(z[7],3)*z[77];
    z[117]=8*z[93];
    z[116]= - z[117] + n<T>(13,2)*z[116];
    z[116]=z[7]*z[116];
    z[56]=z[56] + z[116] + z[104] + z[81] + 2*z[107] + z[112];
    z[56]=z[4]*z[56];
    z[69]= - z[69] - z[47];
    z[81]=npow(z[3],5);
    z[69]=z[96]*z[81]*z[69];
    z[96]=n<T>(1,3)*z[81];
    z[76]=z[96]*z[76];
    z[104]= - z[2]*z[76];
    z[69]=z[104] + z[69];
    z[69]=z[7]*z[69];
    z[104]= - z[68]*z[76];
    z[69]=z[104] + z[69];
    z[69]=z[7]*z[69];
    z[104]= - z[20]*z[76];
    z[69]=z[104] + z[69];
    z[69]=z[7]*z[69];
    z[76]= - z[108]*z[76];
    z[69]=z[76] + z[69];
    z[69]=z[7]*z[69];
    z[76]=z[111] - z[5];
    z[76]=z[76]*z[85];
    z[104]=4*z[6];
    z[107]= - z[81]*z[104];
    z[96]=z[5]*z[96];
    z[96]=z[107] + z[96];
    z[96]=z[5]*z[96]*z[108];
    z[83]= - z[10]*z[83];
    z[83]=z[83] + z[96];
    z[83]=z[2]*z[83];
    z[69]=z[69] + z[76] + z[83];
    z[69]=z[69]*z[75];
    z[76]=z[5]*z[10];
    z[76]= - n<T>(1,2)*z[61] - n<T>(2,3)*z[76];
    z[76]=z[5]*z[76];
    z[83]=z[2]*z[3];
    z[96]=z[6]*z[82]*npow(z[83],5);
    z[107]=z[81]*npow(z[7],5);
    z[112]=z[77]*z[107];
    z[95]=z[8]*z[95];
    z[49]=z[69] + n<T>(19,6)*z[112] + n<T>(1,2)*z[95] - n<T>(5,6)*z[96] + z[49] + 
    z[76];
    z[49]=z[4]*z[49];
    z[49]= - 4*z[93] + z[49];
    z[49]=z[4]*z[49];
    z[69]=z[10]*z[9];
    z[76]=z[80]*z[69];
    z[95]=z[93]*z[6];
    z[96]=z[92]*z[8];
    z[76]=z[96] - z[95] + n<T>(7,3)*z[76];
    z[76]=z[76]*z[107];
    z[81]=z[5]*z[12]*npow(z[11],5)*z[81]*z[80];
    z[76]= - n<T>(17,3)*z[81] + z[76];
    z[81]= - z[6]*npow(z[83],6);
    z[81]=z[10] + z[81];
    z[81]=z[82]*z[81];
    z[83]=z[7]*z[3];
    z[83]=z[77]*npow(z[83],6);
    z[81]=z[83] + z[81];
    z[81]=z[1]*z[81]*npow(z[4],3);
    z[49]=n<T>(2,3)*z[81] + 4*z[76] + z[49];
    z[49]=z[1]*z[49];
    z[76]=z[79]*z[43];
    z[79]=n<T>(1,6)*z[17];
    z[81]= - z[30] + z[79];
    z[81]=z[9]*z[81];
    z[83]=z[24]*z[25];
    z[107]= - z[18] - z[17];
    z[107]=z[5]*z[107];
    z[107]=z[107] + z[24];
    z[112]=7*z[11];
    z[107]=z[112]*z[107];
    z[107]= - z[17] + z[107];
    z[107]=z[107]*z[113];
    z[81]=z[76] + z[107] + z[81] - n<T>(7,6)*z[83];
    z[81]=z[8]*z[81];
    z[83]=7*z[9];
    z[107]=z[29]*z[83];
    z[116]=4*z[13];
    z[118]= - z[46]*z[116];
    z[119]=z[29]*z[112];
    z[119]= - 25*z[18] + z[119];
    z[119]=z[119]*z[82];
    z[81]=z[81] + n<T>(1,6)*z[119] + z[107] + z[118];
    z[81]=z[8]*z[81];
    z[107]=z[80]*z[89];
    z[118]=z[10]*z[11];
    z[119]=z[53] + z[118];
    z[119]=z[119]*z[107];
    z[120]=z[11]*z[6];
    z[121]=z[12]*z[11];
    z[122]=z[120] - z[121];
    z[123]=z[37]*z[122];
    z[124]= - z[11]*z[93];
    z[123]=z[124] + z[123];
    z[123]=z[123]*z[89];
    z[124]=z[37]*z[11];
    z[125]=z[110]*z[124];
    z[119]=z[125] + z[123] + n<T>(7,3)*z[119];
    z[123]=4*z[37];
    z[125]=z[46]*z[6];
    z[126]= - z[123] + 11*z[125];
    z[126]=z[126]*z[10];
    z[127]=z[37]*z[6];
    z[128]=z[127] + z[92];
    z[128]=4*z[128] + z[126];
    z[128]=z[128]*z[89];
    z[89]=z[41]*z[89];
    z[129]=z[10] + z[9];
    z[130]=z[13]*z[129];
    z[130]=29*z[69] - n<T>(28,3)*z[130];
    z[130]=z[130]*z[89];
    z[131]=3*z[51];
    z[132]= - z[123] - z[131];
    z[110]=z[132]*z[110];
    z[110]=z[110] + z[128] + z[130];
    z[110]=z[7]*z[110];
    z[110]=4*z[119] + z[110];
    z[110]=z[7]*z[110];
    z[119]=npow(z[11],2);
    z[128]=z[119]*z[12];
    z[130]= - 17*z[63] - 7*z[128];
    z[130]=z[130]*z[107];
    z[110]=n<T>(4,3)*z[130] + z[110];
    z[110]=z[7]*z[110];
    z[130]=z[12]*npow(z[11],3);
    z[132]=z[130]*z[107];
    z[110]= - n<T>(124,3)*z[132] + z[110];
    z[110]=z[7]*z[110];
    z[132]= - z[15] + z[11];
    z[132]=z[11]*z[132];
    z[132]=n<T>(1,3)*z[62] + z[132];
    z[133]=68*z[13];
    z[132]=z[132]*z[119]*z[133];
    z[134]=z[12]*npow(z[11],4);
    z[132]= - n<T>(167,3)*z[134] + z[132];
    z[89]=z[132]*z[89];
    z[132]=7*z[120];
    z[135]= - z[29]*z[132];
    z[136]=z[6]*z[18];
    z[135]=25*z[136] + z[135];
    z[113]=z[135]*z[113];
    z[89]=z[89] + z[113];
    z[89]=z[5]*z[89];
    z[113]=z[29]*z[6];
    z[135]= - z[83]*z[113];
    z[107]=z[107]*z[134];
    z[76]= - z[82]*z[76];
    z[49]=z[49] + z[56] + z[110] + z[81] + z[76] + z[89] - n<T>(164,3)*
    z[107] + z[135] + z[117];
    z[49]=z[1]*z[49];
    z[56]=n<T>(1,3)*z[11];
    z[76]= - 365*z[15] + 433*z[11];
    z[76]=z[76]*z[56];
    z[76]=33*z[62] + z[76];
    z[81]=z[21]*z[11];
    z[76]=z[76]*z[81];
    z[89]=z[13]*z[128];
    z[76]=n<T>(68,3)*z[89] - 3*z[121] + z[76];
    z[76]=z[13]*z[76];
    z[89]=z[130]*z[21];
    z[107]=3*z[12] - n<T>(82,3)*z[89];
    z[76]=2*z[107] + z[76];
    z[76]=z[13]*z[76];
    z[107]=n<T>(14,3)*z[18] + z[17];
    z[107]=z[107]*z[120];
    z[110]=z[17]*z[33];
    z[110]= - n<T>(13,3) + z[110];
    z[110]=z[10]*z[110];
    z[103]=z[110] - z[103] + z[107];
    z[103]=z[5]*z[103];
    z[107]=n<T>(193,2)*z[18] + 40*z[17];
    z[107]=z[107]*z[57];
    z[31]= - z[31] - n<T>(7,3)*z[24];
    z[31]=z[31]*z[120];
    z[110]=n<T>(2,3)*z[24];
    z[117]= - z[11]*z[110];
    z[117]= - 3*z[17] + z[117];
    z[54]=z[117]*z[54];
    z[117]=6*z[11];
    z[135]= - z[24]*z[117];
    z[135]=n<T>(23,6)*z[17] + z[135];
    z[135]=z[10]*z[135];
    z[137]=z[48]*z[19];
    z[31]=z[103] + z[76] + z[135] + z[54] + z[31] - z[137] + z[107];
    z[31]=z[5]*z[31];
    z[54]=z[6]*z[14];
    z[76]=7*z[46] - 12*z[54];
    z[76]=z[10]*z[76];
    z[76]=z[76] - 7*z[125] - z[131];
    z[76]=z[76]*z[21];
    z[103]=28*z[13];
    z[107]= - 59*z[129] + z[103];
    z[107]=z[13]*z[107];
    z[107]=104*z[69] + z[107];
    z[129]=n<T>(1,3)*z[13];
    z[107]=z[129]*z[21]*z[107];
    z[131]= - z[12]*z[48];
    z[73]=z[73] + z[131];
    z[73]=z[73]*z[21];
    z[131]=5*z[12];
    z[135]=29*z[9] + z[131];
    z[59]=z[135]*z[59];
    z[59]=z[73] + n<T>(1,6)*z[59];
    z[59]=z[8]*z[59];
    z[59]=z[59] + z[76] + z[107];
    z[59]=z[7]*z[59];
    z[73]=z[6]*z[17];
    z[76]=z[12]*z[17];
    z[107]=3*z[73] + n<T>(4,3)*z[76];
    z[135]=8*z[17];
    z[138]= - z[135] + z[83];
    z[138]=z[138]*z[71];
    z[139]=z[11] + z[15];
    z[140]=n<T>(7,3)*z[21];
    z[140]= - z[139]*z[140];
    z[107]=z[140] + 2*z[107] + z[138];
    z[107]=z[107]*z[116];
    z[138]=n<T>(28,3)*z[118] + n<T>(23,3)*z[53] + z[121];
    z[138]=z[138]*z[21];
    z[140]=z[6] + z[42];
    z[140]=z[24]*z[140];
    z[107]=z[107] + n<T>(8,3)*z[140] + z[138];
    z[107]=z[107]*z[41];
    z[138]= - z[46]*z[122];
    z[140]=z[46]*z[11];
    z[141]=z[27]*z[140];
    z[138]=z[141] + z[138];
    z[141]=2*z[21];
    z[138]=z[138]*z[141];
    z[141]= - z[21]*z[140];
    z[142]=z[13]*z[17];
    z[143]= - z[39] - 3*z[142];
    z[143]=z[143]*z[41];
    z[92]=2*z[143] + z[92] + z[141];
    z[141]=5*z[25];
    z[143]=z[21]*z[141];
    z[40]=z[12]*z[40];
    z[40]=z[143] - 29*z[40];
    z[40]=z[8]*z[40];
    z[40]=4*z[92] + n<T>(1,6)*z[40];
    z[40]=z[8]*z[40];
    z[40]=z[59] + z[40] + z[138] + z[107];
    z[40]=z[7]*z[40];
    z[59]=7*z[73] - n<T>(13,3)*z[76];
    z[92]=89*z[9];
    z[107]= - 64*z[17] + z[92];
    z[107]=z[107]*z[71];
    z[138]= - 37*z[63] - 89*z[128];
    z[138]=z[138]*z[21];
    z[112]=z[139]*z[112];
    z[139]=17*z[62];
    z[112]=z[139] + z[112];
    z[112]=z[112]*z[21];
    z[112]=31*z[12] + z[112];
    z[143]=n<T>(4,3)*z[13];
    z[112]=z[112]*z[143];
    z[59]=z[112] + n<T>(1,3)*z[138] + 8*z[59] + z[107];
    z[59]=z[13]*z[59];
    z[107]=z[10] + z[6] + z[12];
    z[107]=z[24]*z[107];
    z[59]=n<T>(16,3)*z[107] + z[59];
    z[59]=z[13]*z[59];
    z[107]= - n<T>(8,3)*z[29] - z[46];
    z[107]=z[12]*z[107];
    z[112]=z[110] - z[46];
    z[138]=n<T>(16,3)*z[142] + z[112];
    z[138]=z[13]*z[138];
    z[100]=z[12]*z[100];
    z[144]=z[9]*z[18];
    z[100]= - 15*z[144] + z[100];
    z[100]=z[8]*z[100];
    z[145]=z[29]*z[9];
    z[100]=z[100] + 8*z[138] + z[107] + 15*z[145] - z[123];
    z[100]=z[8]*z[100];
    z[107]=z[29] + z[136];
    z[107]=z[107]*z[64];
    z[123]=z[46]*z[19];
    z[107]=z[123] + z[107];
    z[107]=z[107]*z[66];
    z[138]=z[113]*z[9];
    z[40]=z[40] + z[100] + z[59] + 20*z[93] - 15*z[138] + z[107];
    z[40]=z[7]*z[40];
    z[59]=z[24]*z[9];
    z[38]= - z[113] + n<T>(7,3)*z[59] - z[38];
    z[38]=z[38]*z[33];
    z[79]= - 6*z[16] + z[79];
    z[79]=z[9]*z[79];
    z[34]=z[38] - n<T>(11,2)*z[136] - 15*z[46] + z[34] + z[79];
    z[34]=z[10]*z[34];
    z[38]=z[12]*z[135];
    z[38]=z[38] - z[46] + 11*z[73];
    z[79]=7*z[15];
    z[100]= - z[79] + 65*z[11];
    z[100]=z[11]*z[100];
    z[100]= - z[139] + z[100];
    z[81]=z[100]*z[81];
    z[81]=41*z[121] + z[81];
    z[81]=z[81]*z[116];
    z[81]=z[81] - 73*z[12] - 259*z[89];
    z[81]=z[81]*z[129];
    z[89]= - 32*z[17] + n<T>(121,2)*z[9];
    z[89]=z[89]*z[71];
    z[38]=z[81] + 4*z[38] + z[89];
    z[38]=z[13]*z[38];
    z[81]= - z[122]*z[21]*z[82];
    z[21]=z[21]*z[44];
    z[44]= - z[114] - z[12];
    z[44]=z[44]*z[86];
    z[21]=z[21] + n<T>(1,2)*z[44];
    z[44]=z[2]*z[5];
    z[21]=z[21]*z[44];
    z[21]=z[81] + z[21];
    z[21]=z[21]*z[68];
    z[81]=n<T>(8,3)*z[136];
    z[36]= - z[81] + 11*z[144] + z[36];
    z[36]=z[6]*z[36];
    z[86]=3*z[16];
    z[89]=z[86] - z[17];
    z[89]=z[9]*z[89];
    z[100]=4*z[19];
    z[107]=z[100] + z[9];
    z[107]=z[14]*z[107];
    z[26]=z[107] - z[26] + z[89];
    z[26]=2*z[26] + z[136];
    z[26]=z[12]*z[26];
    z[89]= - z[46]*z[100];
    z[59]=z[120]*z[59];
    z[21]=z[49] + z[22] + z[40] + z[23] + z[21] + z[31] + z[38] + z[34]
    + z[26] + z[59] + z[89] + z[36];
    z[21]=z[1]*z[21];
    z[22]=z[17]*z[83];
    z[22]= - z[24] + z[22];
    z[22]=n<T>(1,3)*z[22] - z[46];
    z[23]=4*z[11];
    z[22]=z[22]*z[23];
    z[26]=static_cast<T>(29)- 32*z[28];
    z[26]=z[26]*z[71];
    z[28]=2*z[17];
    z[31]= - z[30] - z[28];
    z[31]=z[31]*z[33];
    z[34]= - z[13]*z[117];
    z[31]=z[34] + n<T>(61,6) + z[31];
    z[31]=z[5]*z[31];
    z[34]=z[58]*z[2];
    z[36]= - z[12]*z[34];
    z[36]=5*z[43] + z[36];
    z[36]=z[2]*z[36];
    z[38]=n<T>(5,6)*z[12];
    z[40]=z[43]*z[2];
    z[49]= - z[38] + z[40];
    z[49]=z[2]*z[49];
    z[49]=z[49] - n<T>(9,2) - z[141];
    z[49]=z[8]*z[49];
    z[59]=4*z[17];
    z[89]= - 5*z[18] + z[59];
    z[107]= - static_cast<T>(12)- z[118];
    z[107]=z[13]*z[107];
    z[22]=z[49] + n<T>(2,3)*z[36] + z[31] + z[107] + z[26] + z[38] + z[22]
    - z[50] + n<T>(22,3)*z[89] - n<T>(83,2)*z[9];
    z[22]=z[8]*z[22];
    z[26]= - z[29] - z[39];
    z[31]=3*z[9];
    z[36]=z[31]*z[18];
    z[26]=2*z[26] - z[36];
    z[38]=z[18] - n<T>(31,2)*z[14];
    z[38]=z[38]*z[67];
    z[49]= - n<T>(37,6)*z[9] + z[35];
    z[49]=z[11]*z[49]*z[58];
    z[67]= - z[66]*z[34];
    z[89]=12*z[124];
    z[107]= - n<T>(80,3)*z[17] + z[50];
    z[107]=z[13]*z[107];
    z[113]=z[18] + z[9];
    z[113]=8*z[113] - n<T>(7,2)*z[12];
    z[113]=z[8]*z[113];
    z[26]=z[113] + z[67] + 2*z[107] + z[49] + z[38] + z[89] + 4*z[26] + 
   13*z[46];
    z[26]=z[8]*z[26];
    z[38]=2*z[145] - 3*z[37];
    z[49]=n<T>(8,3)*z[12];
    z[67]=12*z[9];
    z[107]=z[49] - z[67] - z[14];
    z[107]=z[107]*z[58];
    z[113]=10*z[142] + z[112];
    z[113]=z[113]*z[116];
    z[117]= - 8*z[144] - n<T>(17,3)*z[58];
    z[117]=z[8]*z[117];
    z[38]=z[117] + z[113] + z[107] + 4*z[38] - 5*z[51];
    z[38]=z[8]*z[38];
    z[107]= - 5*z[73] + n<T>(11,3)*z[76];
    z[113]= - 88*z[17] + 265*z[9];
    z[113]=z[113]*z[71];
    z[117]= - n<T>(1,3)*z[9] - z[12];
    z[117]=z[117]*z[103];
    z[107]=z[117] + n<T>(31,3)*z[58] + 8*z[107] + z[113];
    z[107]=z[13]*z[107];
    z[113]=z[110]*z[42];
    z[112]= - z[6]*z[112];
    z[112]=z[112] + z[113];
    z[113]= - 19*z[9] - 15*z[10];
    z[113]=z[113]*z[58];
    z[107]=z[107] + 4*z[112] + z[113];
    z[107]=z[13]*z[107];
    z[112]= - z[12]*z[35];
    z[112]=5*z[54] + z[112];
    z[113]=z[6] + z[31] - z[14];
    z[113]=z[113]*z[111];
    z[112]=2*z[112] + z[113];
    z[112]=z[112]*z[58];
    z[113]=z[19]*z[51];
    z[113]= - z[138] + z[113];
    z[93]=2*z[113] + 3*z[93];
    z[80]=z[10]*z[83]*z[80];
    z[80]=z[80] + 3*z[96];
    z[95]= - z[95] + z[80];
    z[96]=4*z[7];
    z[95]=z[95]*z[96];
    z[38]=z[95] + z[38] + z[107] + 4*z[93] + z[112];
    z[38]=z[7]*z[38];
    z[93]=n<T>(5,3)*z[15] - z[11];
    z[93]=z[93]*z[58];
    z[95]=n<T>(7,3) - 31*z[121];
    z[95]=z[95]*z[116];
    z[92]= - z[92] + 145*z[12];
    z[92]=z[95] + z[93] + n<T>(1,3)*z[92] - 20*z[10];
    z[92]=z[13]*z[92];
    z[93]=z[53] + n<T>(7,3)*z[118];
    z[93]=z[93]*z[58];
    z[95]= - z[35] - n<T>(200,3)*z[17] - z[9];
    z[95]=z[6]*z[95];
    z[107]= - n<T>(136,3)*z[17] + n<T>(133,2)*z[9];
    z[107]=z[10]*z[107];
    z[92]=z[92] + 2*z[93] + z[107] + z[95] - n<T>(176,3)*z[76];
    z[92]=z[13]*z[92];
    z[93]=n<T>(4,3)*z[24];
    z[95]=z[31]*z[16];
    z[107]=z[93] - z[95];
    z[107]= - z[89] + n<T>(47,6)*z[54] + 2*z[107] - 23*z[46];
    z[107]=z[10]*z[107];
    z[112]=2*z[19];
    z[113]= - z[46]*z[112];
    z[29]=z[29] - z[39];
    z[29]=2*z[29] + z[36];
    z[29]=z[6]*z[29];
    z[29]=z[113] + z[29];
    z[36]=z[19]*z[35];
    z[36]=z[36] + z[93] + z[95];
    z[36]=2*z[36] - n<T>(1,3)*z[136];
    z[36]=z[12]*z[36];
    z[93]= - z[14]*z[122];
    z[95]=n<T>(3,2)*z[6] - n<T>(31,6)*z[9] - z[35];
    z[95]=z[95]*z[118];
    z[93]=4*z[93] + z[95];
    z[93]=z[93]*z[58];
    z[26]=z[38] + z[26] + z[92] + z[93] + z[107] + 4*z[29] + z[36];
    z[26]=z[7]*z[26];
    z[29]=z[115] - z[61];
    z[36]= - z[111] - 31*z[6] + z[131];
    z[36]=z[36]*z[143];
    z[38]=z[77]*z[7];
    z[77]= - z[101] + 31*z[13];
    z[77]=n<T>(4,3)*z[77] - n<T>(45,2)*z[8];
    z[77]=z[8]*z[77];
    z[36]=39*z[38] + z[77] + z[36] - n<T>(20,3)*z[29] + n<T>(33,2)*z[58];
    z[36]=z[7]*z[36];
    z[77]=z[58]*z[15];
    z[92]=9*z[77];
    z[93]= - 80*z[10] + 40*z[12] - n<T>(271,2)*z[9] - 140*z[6];
    z[95]= - static_cast<T>(37)- n<T>(55,6)*z[94];
    z[95]=z[8]*z[95];
    z[36]=z[36] + z[95] + n<T>(33,2)*z[34] + n<T>(1,3)*z[93] + z[92];
    z[36]=z[7]*z[36];
    z[78]=5*z[19] + z[78];
    z[78]= - n<T>(25,3)*z[5] + 4*z[78] - z[88];
    z[78]=z[5]*z[78];
    z[82]=z[82]*z[2];
    z[93]=z[106] - z[10];
    z[93]=z[93]*z[82];
    z[78]=z[93] - n<T>(319,6)*z[58] + z[78];
    z[78]=z[2]*z[78];
    z[93]= - n<T>(107,3) + 4*z[84];
    z[93]=z[5]*z[93];
    z[78]=z[78] + z[93] + z[92] + n<T>(52,3)*z[10] - n<T>(20,3)*z[12] - z[100]
    - n<T>(131,6)*z[6];
    z[78]=z[2]*z[78];
    z[92]=n<T>(5,3)*z[5];
    z[93]= - z[71] - z[19] + n<T>(8,3)*z[6];
    z[93]=2*z[93] + z[92];
    z[95]=2*z[5];
    z[93]=z[93]*z[95];
    z[101]= - z[72] + z[71];
    z[101]=z[101]*z[82];
    z[93]=z[93] + z[101];
    z[93]=z[2]*z[93];
    z[93]=z[93] - 9*z[5] + z[100] - n<T>(55,3)*z[6];
    z[93]=z[2]*z[93];
    z[101]=static_cast<T>(20)+ z[84];
    z[93]=n<T>(2,3)*z[101] + z[93];
    z[93]=z[2]*z[93];
    z[101]=z[2]*z[10];
    z[107]=static_cast<T>(1)- 4*z[101];
    z[107]=z[107]*z[68];
    z[108]=z[108]*z[8];
    z[111]= - z[10]*z[108];
    z[107]=z[107] + z[111];
    z[107]=z[107]*z[47];
    z[67]=z[67] + z[8];
    z[67]=z[67]*z[8];
    z[111]= - z[67] + 2*z[38];
    z[111]=z[7]*z[111];
    z[113]= - static_cast<T>(16)- z[94];
    z[113]=z[8]*z[113];
    z[113]=z[113] - 41*z[9] + 28*z[6];
    z[111]=n<T>(1,3)*z[113] + z[111];
    z[111]=z[7]*z[111];
    z[111]=n<T>(14,3) + z[111];
    z[111]=z[7]*z[111];
    z[113]=z[62]*z[5];
    z[117]= - 2*z[15] + z[113];
    z[93]=z[111] + z[107] + n<T>(14,3)*z[117] + z[93];
    z[93]=z[93]*z[75];
    z[107]=z[63]*z[48];
    z[111]=z[58]*z[87];
    z[117]=z[48]*z[62];
    z[124]=n<T>(67,3)*z[15] - z[117];
    z[124]=z[5]*z[124];
    z[129]=z[68]*z[8];
    z[131]=n<T>(1,3) - z[101];
    z[131]=z[131]*z[129];
    z[71]= - z[2]*z[71];
    z[71]= - n<T>(37,2) + z[71];
    z[71]=z[2]*z[71];
    z[71]=z[71] + z[131];
    z[71]=z[8]*z[71];
    z[36]=z[93] + z[36] + z[71] + z[78] + z[124] + z[111] + z[107] - 11
    - 8*z[53];
    z[36]=z[4]*z[36];
    z[32]=33*z[32] + n<T>(97,6)*z[58] - 40*z[41];
    z[32]=z[8]*z[32];
    z[71]=7*z[6];
    z[78]= - n<T>(32,3)*z[9] + z[71];
    z[78]=z[78]*z[58];
    z[42]=40*z[6] - 7*z[42];
    z[42]=z[42]*z[41];
    z[32]=z[32] + z[42] + z[78] - z[52];
    z[32]=z[7]*z[32];
    z[42]=z[64] - z[19] + n<T>(5,3)*z[14];
    z[42]=z[12]*z[42];
    z[42]=z[42] - n<T>(5,3)*z[61];
    z[52]=z[58]*z[53];
    z[61]= - z[10] + 53*z[6] + 19*z[12];
    z[61]=z[61]*z[65];
    z[64]=z[6]*z[34];
    z[42]= - 19*z[64] + z[61] + 2*z[42] + z[52];
    z[52]= - n<T>(25,2)*z[9] + n<T>(32,3)*z[13];
    z[52]= - n<T>(37,2)*z[8] + 5*z[52] - n<T>(73,6)*z[34];
    z[52]=z[8]*z[52];
    z[32]=z[32] + 2*z[42] + z[52];
    z[32]=z[7]*z[32];
    z[42]=n<T>(7,6)*z[5] - n<T>(62,3)*z[77] - n<T>(91,6)*z[10] + z[66] - 16*z[19] + 
   n<T>(71,2)*z[6];
    z[42]=z[5]*z[42];
    z[52]=z[72] + z[10];
    z[52]=z[5]*z[52];
    z[52]=n<T>(83,3)*z[58] + z[52];
    z[52]=z[5]*z[52];
    z[61]=z[6]*z[58];
    z[52]=n<T>(361,6)*z[61] + z[52];
    z[52]=z[2]*z[52];
    z[42]=z[52] - z[97] + z[42];
    z[42]=z[2]*z[42];
    z[34]=n<T>(2,3)*z[34] - 7*z[5] - z[49] - n<T>(17,2)*z[10];
    z[34]=z[2]*z[34];
    z[49]=n<T>(10,3) - z[101];
    z[49]=z[49]*z[94];
    z[34]=z[49] + n<T>(61,3) + z[34];
    z[34]=z[8]*z[34];
    z[49]=static_cast<T>(5)- z[70];
    z[49]=z[49]*z[35];
    z[52]=z[84] - n<T>(65,3) + z[91];
    z[52]=z[5]*z[52];
    z[32]=z[36] + z[32] + z[34] + z[42] + z[52] + n<T>(158,3)*z[13] + 16*
    z[10] - z[66] + n<T>(35,2)*z[6] + z[49] + z[100] - n<T>(37,3)*z[9];
    z[32]=z[4]*z[32];
    z[34]=z[119] - z[130];
    z[34]=z[34]*z[133];
    z[34]=z[34] - n<T>(149,3)*z[128] - z[117] + 33*z[11];
    z[34]=z[13]*z[34];
    z[36]= - 64*z[15] + n<T>(325,3)*z[11];
    z[36]=z[11]*z[36];
    z[36]=z[87] + z[36];
    z[36]=z[36]*z[58];
    z[34]=z[34] + z[36] - n<T>(67,6)*z[121] - n<T>(1,6) - z[91];
    z[34]=z[13]*z[34];
    z[36]=z[128]*z[58];
    z[30]=z[30] + n<T>(20,3)*z[17];
    z[30]= - z[5] + 2*z[30];
    z[30]=z[120]*z[30];
    z[42]= - z[35] + z[112] - n<T>(19,3)*z[17];
    z[49]=z[11]*z[17];
    z[52]=n<T>(7,2) + n<T>(22,3)*z[49];
    z[52]=z[12]*z[52];
    z[49]= - n<T>(127,6) + 12*z[49];
    z[49]=z[10]*z[49];
    z[30]=z[34] - n<T>(65,3)*z[36] + z[49] + z[52] + 2*z[42] - n<T>(97,3)*z[6]
    + z[30];
    z[30]=z[5]*z[30];
    z[34]=11*z[15];
    z[42]= - z[34] + n<T>(323,3)*z[11];
    z[42]=z[11]*z[42];
    z[42]= - z[87] + z[42];
    z[42]=z[42]*z[58];
    z[49]=n<T>(65,3)*z[11] - 41*z[128];
    z[49]=z[49]*z[116];
    z[42]=z[49] + z[42] - n<T>(113,3)*z[121] - n<T>(97,3) + z[107];
    z[42]=z[13]*z[42];
    z[49]=static_cast<T>(1)+ 2*z[53];
    z[48]=z[49]*z[48];
    z[49]=16*z[17];
    z[36]=z[42] - 40*z[36] - 10*z[10] - n<T>(45,2)*z[12] + z[71] + z[48] - 
    z[49] - n<T>(107,6)*z[9];
    z[36]=z[13]*z[36];
    z[42]=z[9]*z[17];
    z[48]=5*z[42];
    z[52]= - 16*z[24] - z[48];
    z[31]= - z[112] + z[31];
    z[31]=z[31]*z[35];
    z[39]=z[39] - z[42];
    z[61]=z[14]*z[9];
    z[39]=2*z[39] - z[61];
    z[39]=z[39]*z[120];
    z[31]=z[39] + n<T>(1,3)*z[52] + z[31];
    z[24]= - z[24] - z[48];
    z[24]=z[81] + n<T>(1,3)*z[24] + z[46];
    z[23]=z[24]*z[23];
    z[24]=n<T>(97,6)*z[9];
    z[39]=z[16] - z[59];
    z[23]=z[23] + z[99] + 15*z[14] + 6*z[39] + z[24];
    z[23]=z[10]*z[23];
    z[39]= - z[61] + z[110] + z[42];
    z[39]=z[39]*z[33];
    z[42]= - z[86] + n<T>(52,3)*z[17];
    z[39]=z[39] + 2*z[42] - 9*z[9];
    z[39]=z[12]*z[39];
    z[42]=z[122]*z[58];
    z[42]=10*z[43] - 11*z[42];
    z[42]=z[42]*z[85];
    z[48]= - 89*z[6] + z[55];
    z[48]=z[48]*z[58];
    z[52]= - z[5]*z[43];
    z[48]=n<T>(1,6)*z[48] + z[52];
    z[44]=z[48]*z[44];
    z[42]=z[42] + z[44];
    z[42]=z[2]*z[42];
    z[44]=n<T>(11,3)*z[18] - z[59];
    z[44]=2*z[44] - z[9];
    z[44]=5*z[44] + z[35];
    z[44]=z[6]*z[44];
    z[21]=z[21] + z[32] + z[26] + z[22] + z[42] + z[30] + z[36] + z[23]
    + z[39] + z[44] + 2*z[31];
    z[21]=z[1]*z[21];
    z[22]= - z[27]*z[127];
    z[22]=z[22] + z[80];
    z[22]=z[22]*z[96];
    z[23]=z[19] + z[50];
    z[23]=z[23]*z[51];
    z[23]=z[127] + z[23];
    z[26]= - n<T>(2,3)*z[9] - z[10];
    z[26]=z[26]*z[103];
    z[26]=n<T>(263,3)*z[69] + z[26];
    z[26]=z[26]*z[41];
    z[30]= - 12*z[37] - 7*z[51];
    z[30]=z[8]*z[30];
    z[22]=z[22] + z[30] + z[26] + 4*z[23] + z[126];
    z[22]=z[7]*z[22];
    z[23]=n<T>(34,3)*z[9] - n<T>(3,2)*z[12];
    z[23]=z[8]*z[23];
    z[23]=z[23] - 16*z[142] - n<T>(17,3)*z[115] + 11*z[46] + z[89];
    z[23]=z[8]*z[23];
    z[26]= - z[123] - z[125];
    z[30]= - z[112] - z[14];
    z[30]=z[30]*z[115];
    z[26]=2*z[26] + z[30];
    z[30]= - z[89] - z[46] + n<T>(25,3)*z[54];
    z[30]=z[10]*z[30];
    z[31]=static_cast<T>(2)+ z[53];
    z[31]=n<T>(1,3)*z[31] + z[121];
    z[31]=z[31]*z[103];
    z[32]=40*z[10];
    z[36]= - 148*z[9] - 59*z[12];
    z[31]=z[31] + n<T>(1,3)*z[36] - z[32];
    z[31]=z[13]*z[31];
    z[36]=z[73] + z[76];
    z[37]= - z[49] + 81*z[9];
    z[37]=z[10]*z[37];
    z[31]=z[31] + 16*z[36] + z[37];
    z[31]=z[13]*z[31];
    z[22]=z[22] + z[23] + z[31] + 2*z[26] + z[30];
    z[22]=z[7]*z[22];
    z[23]=z[2]*z[12];
    z[23]= - n<T>(4,3)*z[23] - static_cast<T>(3)- n<T>(1,3)*z[25];
    z[23]=z[8]*z[23];
    z[25]=4*z[140];
    z[26]= - z[18] + z[28];
    z[26]=2*z[26] - 11*z[9];
    z[23]=z[23] - z[25] + 2*z[26] - z[50];
    z[23]= - z[102] + 2*z[23];
    z[23]=z[8]*z[23];
    z[26]=61*z[121] + static_cast<T>(92)+ 131*z[53];
    z[30]= - z[15] - z[33];
    z[30]=n<T>(7,3)*z[30] + 31*z[128];
    z[30]=z[30]*z[116];
    z[26]=z[30] + n<T>(1,3)*z[26] + 10*z[118];
    z[26]=z[13]*z[26];
    z[30]= - z[74] - z[83] - n<T>(2,3)*z[12];
    z[26]=2*z[30] + z[26];
    z[26]=z[13]*z[26];
    z[28]=z[18] + z[28];
    z[28]=8*z[28] + z[105];
    z[28]=z[6]*z[28];
    z[30]= - z[49] - z[105];
    z[30]=z[12]*z[30];
    z[25]=z[25] - n<T>(16,3)*z[6] + 5*z[14] - z[135] + n<T>(47,3)*z[9];
    z[25]=z[25]*z[27];
    z[22]=z[22] + z[23] + z[26] + z[25] + z[30] - z[137] + z[28];
    z[22]=z[7]*z[22];
    z[23]= - static_cast<T>(29)- 40*z[94];
    z[23]=z[23]*z[47];
    z[25]= - n<T>(155,3)*z[9] - n<T>(33,2)*z[8];
    z[25]=z[8]*z[25];
    z[25]=z[25] + n<T>(59,3)*z[38];
    z[25]=z[7]*z[25];
    z[26]= - 21*z[9] + n<T>(58,3)*z[6];
    z[23]=z[25] + 2*z[26] + z[23];
    z[23]=z[7]*z[23];
    z[25]=z[19] - n<T>(68,3)*z[6];
    z[25]=z[2]*z[25];
    z[26]= - 33*z[2] - n<T>(13,6)*z[129];
    z[26]=z[8]*z[26];
    z[23]=z[23] + z[26] + 4*z[25] + n<T>(51,2) - 38*z[53];
    z[23]=z[7]*z[23];
    z[25]= - 16*z[2] - z[129];
    z[25]=z[25]*z[47];
    z[26]= - z[67] + n<T>(4,3)*z[38];
    z[26]=z[7]*z[26];
    z[28]= - z[2]*z[47];
    z[28]=static_cast<T>(4)+ z[28];
    z[28]=z[8]*z[28];
    z[28]= - n<T>(20,3)*z[9] + z[28];
    z[26]=2*z[28] + z[26];
    z[26]=z[7]*z[26];
    z[28]=z[2]*z[6];
    z[25]=z[26] + z[25] + n<T>(28,3)*z[28] + static_cast<T>(9)- 14*z[53];
    z[25]=z[7]*z[25];
    z[26]=z[19] - 8*z[6];
    z[26]=z[2]*z[26];
    z[26]=n<T>(7,3) + z[26];
    z[26]=z[2]*z[26];
    z[26]=n<T>(14,3)*z[15] + z[26];
    z[25]=2*z[26] + z[25];
    z[25]=z[7]*z[25];
    z[26]=z[19] + z[57];
    z[26]=2*z[26] - z[92];
    z[26]=z[5]*z[26];
    z[28]=z[6]*z[82];
    z[26]=z[26] + n<T>(4,3)*z[28];
    z[26]=z[2]*z[26];
    z[28]= - n<T>(4,3)*z[5] - z[112] + n<T>(7,3)*z[6];
    z[26]=2*z[28] + z[26];
    z[26]=z[2]*z[26];
    z[28]= - static_cast<T>(1)+ z[90];
    z[26]=n<T>(13,3)*z[28] + z[26];
    z[26]=z[2]*z[26];
    z[28]=z[15] - 2*z[113];
    z[26]=n<T>(14,3)*z[28] + z[26];
    z[26]=z[2]*z[26];
    z[28]=4*z[20] + z[108];
    z[28]=z[28]*z[47];
    z[25]=z[25] + z[28] - z[45] + z[26];
    z[25]=z[25]*z[75];
    z[26]=4*z[5] - z[27] - z[98] + z[106];
    z[26]=z[5]*z[26];
    z[27]= - z[6] + z[27];
    z[27]=z[27]*z[82];
    z[26]=z[26] + n<T>(1,3)*z[27];
    z[26]=z[2]*z[26];
    z[27]=z[112] + n<T>(71,3)*z[6];
    z[28]= - n<T>(59,6) - z[90];
    z[28]=z[5]*z[28];
    z[26]=z[26] + 2*z[27] + z[28];
    z[26]=z[2]*z[26];
    z[27]= - static_cast<T>(155)+ 26*z[84];
    z[26]=n<T>(1,3)*z[27] + z[26];
    z[26]=z[2]*z[26];
    z[27]=2*z[68];
    z[28]=n<T>(5,3) - z[101];
    z[27]=z[28]*z[27];
    z[28]=static_cast<T>(1)- n<T>(2,3)*z[101];
    z[20]=z[8]*z[28]*z[20];
    z[20]=z[27] + z[20];
    z[20]=z[8]*z[20];
    z[27]=4*z[63];
    z[20]=z[25] + z[23] + z[20] + z[26] - n<T>(19,3)*z[113] + 29*z[15] - 
    z[27];
    z[20]=z[4]*z[20];
    z[23]=z[95] + n<T>(8,3)*z[10] - n<T>(23,6)*z[12] + z[100] - n<T>(61,2)*z[6];
    z[23]=z[5]*z[23];
    z[25]= - z[114] - z[10];
    z[25]=z[25]*z[82];
    z[23]=z[23] + z[25];
    z[23]=z[2]*z[23];
    z[25]=static_cast<T>(18)- z[84];
    z[25]=z[5]*z[25];
    z[23]=z[23] + z[25] - n<T>(28,3)*z[10] + 20*z[12] + z[98] + n<T>(329,3)*z[6]
   ;
    z[23]=z[2]*z[23];
    z[25]=n<T>(17,2)*z[12] - 8*z[10];
    z[25]=z[2]*z[25];
    z[25]= - n<T>(161,2) + z[25];
    z[25]=z[25]*z[109];
    z[26]= - n<T>(5,2) - z[101];
    z[26]=z[26]*z[129];
    z[25]=z[25] + z[26];
    z[25]=z[8]*z[25];
    z[26]=z[88] - z[104] - z[60];
    z[26]=z[13]*z[26];
    z[26]=n<T>(5,3)*z[29] + z[26];
    z[28]= - n<T>(29,2)*z[8] - n<T>(149,2)*z[9] + 16*z[13];
    z[28]=z[8]*z[28];
    z[26]=21*z[38] + 4*z[26] + z[28];
    z[26]=z[7]*z[26];
    z[28]=z[12] + z[9];
    z[28]=z[32] - 259*z[6] - 80*z[28];
    z[29]=static_cast<T>(23)- 9*z[94];
    z[29]=z[8]*z[29];
    z[26]=z[26] + n<T>(1,3)*z[28] + z[29];
    z[26]=z[7]*z[26];
    z[28]= - 55*z[84] - static_cast<T>(112)- z[53];
    z[20]=z[20] + z[26] + z[25] + n<T>(1,3)*z[28] + z[23];
    z[20]=z[4]*z[20];
    z[23]=z[15] - z[33];
    z[23]=z[23]*z[119];
    z[23]=z[23] + z[134];
    z[23]=z[23]*z[133];
    z[25]=65*z[15] - n<T>(532,3)*z[11];
    z[25]=z[11]*z[25];
    z[23]=z[23] + n<T>(325,3)*z[130] + 4*z[62] + z[25];
    z[23]=z[13]*z[23];
    z[25]=181*z[128] + 41*z[15] - 188*z[11];
    z[23]=n<T>(1,3)*z[25] + z[23];
    z[23]=z[13]*z[23];
    z[23]=z[23] - n<T>(16,3)*z[121] + static_cast<T>(29)- z[132];
    z[23]=z[5]*z[23];
    z[24]=z[35] + z[135] - z[24];
    z[24]=z[11]*z[24];
    z[25]=z[66] + z[40];
    z[25]=z[2]*z[25];
    z[26]=n<T>(5,2)*z[12] + z[40];
    z[26]=z[26]*z[129];
    z[24]=z[26] + n<T>(8,3)*z[25] + n<T>(14,3) + z[24];
    z[24]=z[8]*z[24];
    z[25]=z[79] - 130*z[11];
    z[25]=z[25]*z[56];
    z[25]=z[25] + 41*z[130];
    z[25]=z[25]*z[116];
    z[25]=z[25] + n<T>(445,3)*z[128] - n<T>(226,3)*z[11] - n<T>(103,3)*z[15] - z[27]
   ;
    z[25]=z[13]*z[25];
    z[26]= - static_cast<T>(23)+ 20*z[53];
    z[26]=2*z[26] + 103*z[121];
    z[25]=n<T>(1,3)*z[26] + z[25];
    z[25]=z[13]*z[25];
    z[26]=n<T>(8,3)*z[43];
    z[27]= - z[5]*z[6];
    z[27]= - z[26] + z[27];
    z[27]=z[5]*z[27];
    z[28]=z[43]*z[82];
    z[27]=z[27] + z[28];
    z[27]=z[2]*z[27];
    z[28]= - 41*z[6] + z[66];
    z[29]=static_cast<T>(2)- z[120];
    z[29]=z[5]*z[29];
    z[28]=n<T>(2,3)*z[28] + z[29];
    z[28]=z[5]*z[28];
    z[26]=z[27] + z[26] + z[28];
    z[26]=z[2]*z[26];
    z[27]= - z[19] + 12*z[17];
    z[27]=2*z[27] - z[83];
    z[28]= - z[120]*z[135];
    z[29]= - n<T>(26,3)*z[6] - z[35] + z[135] - n<T>(19,6)*z[9];
    z[29]=z[11]*z[29];
    z[29]= - n<T>(10,3) + z[29];
    z[29]=z[10]*z[29];
    z[30]= - z[11]*z[135];
    z[30]= - static_cast<T>(7)+ z[30];
    z[30]=z[12]*z[30];
    z[20]=z[21] + z[20] + z[22] + z[24] + z[26] + z[23] + z[25] + z[29]
    + z[30] + z[28] + 2*z[27] + n<T>(91,3)*z[6];
    z[20]=z[1]*z[20];
    z[21]= - static_cast<T>(7)+ 10*z[120];
    z[22]= - z[34] + 83*z[11];
    z[22]=z[13]*z[22];
    z[21]=z[22] + 2*z[21] - 11*z[121];
    z[22]=157*z[6] - 43*z[12];
    z[22]=n<T>(1,2)*z[22] + 31*z[5];
    z[22]=z[22]*z[109];
    z[23]=n<T>(16,3)*z[7] - z[15] - n<T>(47,3)*z[2];
    z[23]=z[23]*z[75];
    z[24]= - n<T>(31,3)*z[15] + 32*z[11];
    z[24]=z[5]*z[24];
    z[25]= - 17*z[9] - 23*z[6];
    z[25]=n<T>(7,2)*z[8] + n<T>(26,3)*z[13] + n<T>(2,3)*z[25] + n<T>(9,2)*z[12];
    z[25]=z[7]*z[25];

    r += z[20] + n<T>(1,3)*z[21] + z[22] + z[23] + z[24] + z[25];
 
    return r;
}

template double qqb_2lNLC_r1151(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1151(const std::array<dd_real,31>&);
#endif
