#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r644(const std::array<T,31>& k) {
  T z[145];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[14];
    z[9]=k[10];
    z[10]=k[2];
    z[11]=k[5];
    z[12]=k[3];
    z[13]=k[4];
    z[14]=k[15];
    z[15]=npow(z[1],2);
    z[16]=n<T>(21,2)*z[15];
    z[17]=npow(z[1],3);
    z[18]=z[17]*z[2];
    z[19]= - z[16] + z[18];
    z[19]=z[2]*z[19];
    z[20]=2*z[15];
    z[21]=z[18] + z[20];
    z[22]=z[9]*z[21];
    z[19]=z[22] - 13*z[1] + z[19];
    z[19]=z[9]*z[19];
    z[22]=n<T>(13,2)*z[1];
    z[23]=z[20]*z[2];
    z[24]=z[22] - z[23];
    z[24]=z[2]*z[24];
    z[25]=z[9]*z[1];
    z[26]=z[25]*z[2];
    z[27]=npow(z[2],2);
    z[28]=z[27]*z[1];
    z[26]=z[26] + z[28];
    z[29]=z[13]*z[26];
    z[30]=z[15]*z[2];
    z[31]= - z[1] - z[30];
    z[31]=z[9]*z[31];
    z[24]=z[29] + z[24] + 2*z[31];
    z[24]=z[9]*z[24];
    z[24]=2*z[28] + z[24];
    z[24]=z[13]*z[24];
    z[29]=4*z[15];
    z[31]=z[11]*z[1];
    z[32]= - z[29] + z[31];
    z[32]=z[2]*z[32];
    z[32]=n<T>(47,2)*z[1] + z[32];
    z[32]=z[2]*z[32];
    z[19]=z[24] + z[32] + z[19];
    z[19]=z[13]*z[19];
    z[24]=z[15]*z[11];
    z[32]=z[24] - z[17];
    z[32]=z[32]*z[2];
    z[33]=26*z[31];
    z[34]= - 2*z[32] - n<T>(69,2)*z[15] + z[33];
    z[34]=z[2]*z[34];
    z[35]=n<T>(5,2)*z[15];
    z[36]= - z[35] + 4*z[18];
    z[36]=z[9]*z[36];
    z[37]=3*z[31];
    z[38]=npow(z[11],2);
    z[39]=z[38]*z[1];
    z[40]=z[3]*z[39];
    z[40]= - z[37] + z[40];
    z[40]=z[3]*z[40];
    z[41]=n<T>(51,2)*z[1];
    z[19]=z[19] + z[40] + z[36] - z[41] + z[34];
    z[19]=z[13]*z[19];
    z[34]=z[17]*z[7];
    z[36]=z[34]*z[38];
    z[40]=z[17] + 3*z[24];
    z[40]=z[11]*z[40];
    z[40]=z[40] - z[36];
    z[42]=4*z[7];
    z[40]=z[40]*z[42];
    z[43]=23*z[15];
    z[44]=11*z[31];
    z[45]=z[43] - z[44];
    z[46]=n<T>(1,2)*z[11];
    z[45]=z[45]*z[46];
    z[47]=z[20] + z[31];
    z[48]=z[38]*z[3];
    z[49]= - z[47]*z[48];
    z[40]=z[49] + z[45] + z[40];
    z[40]=z[3]*z[40];
    z[45]=4*z[31];
    z[49]= - n<T>(33,2)*z[15] - z[45];
    z[49]=z[11]*z[49];
    z[49]= - n<T>(25,2)*z[17] + z[49];
    z[49]=z[11]*z[49];
    z[50]=z[47]*z[11];
    z[51]=z[50] + z[17];
    z[52]=z[51]*z[48];
    z[49]=z[49] + z[52];
    z[49]=z[3]*z[49];
    z[52]=z[31] + z[15];
    z[53]=z[52]*z[38];
    z[54]=z[15] + n<T>(1,2)*z[31];
    z[54]=z[11]*z[54];
    z[55]=n<T>(1,2)*z[17];
    z[54]=z[55] + z[54];
    z[48]=z[54]*z[48];
    z[48]= - n<T>(1,2)*z[53] + z[48];
    z[48]=z[8]*z[48];
    z[54]=25*z[15] + z[37];
    z[46]=z[54]*z[46];
    z[46]=3*z[48] + z[46] + z[49];
    z[46]=z[8]*z[46];
    z[48]=19*z[15];
    z[49]= - z[48] + z[45];
    z[54]=2*z[11];
    z[49]=z[49]*z[54];
    z[56]=11*z[17];
    z[57]=z[18]*z[11];
    z[49]=z[57] + z[56] + z[49];
    z[49]=z[2]*z[49];
    z[58]=3*z[11];
    z[59]= - z[32]*z[58];
    z[60]=z[38]*z[7];
    z[61]=z[18]*z[60];
    z[59]=z[59] + z[61];
    z[59]=z[59]*z[42];
    z[61]=9*z[15];
    z[19]=z[19] + z[46] + z[40] + z[59] + z[61] + z[49];
    z[19]=z[14]*z[19];
    z[40]= - n<T>(31,2)*z[15] - z[37];
    z[40]=z[11]*z[40];
    z[46]=z[24] + z[17];
    z[49]=z[46]*z[54];
    z[59]=z[3]*z[49];
    z[40]=z[59] - 24*z[17] + z[40];
    z[40]=z[3]*z[40];
    z[59]=z[3]*z[51];
    z[62]=7*z[15];
    z[59]=6*z[59] - z[62] - n<T>(9,2)*z[31];
    z[59]=z[8]*z[11]*z[59];
    z[63]=2*z[31];
    z[64]=z[17]*z[6];
    z[40]=z[59] + z[40] + n<T>(45,2)*z[64] - z[35] - z[63];
    z[40]=z[8]*z[40];
    z[59]=z[54]*z[15];
    z[65]=5*z[17];
    z[66]=z[57] + z[65] - z[59];
    z[66]=z[2]*z[66];
    z[67]=4*z[24];
    z[68]=z[17]*z[11];
    z[69]=6*z[68];
    z[70]=z[6]*z[69];
    z[70]=z[70] + z[17] - z[67];
    z[71]=2*z[6];
    z[70]=z[70]*z[71];
    z[72]=npow(z[7],2);
    z[73]= - z[38]*z[72];
    z[73]= - static_cast<T>(1)+ z[73];
    z[73]=z[73]*z[34];
    z[74]=z[65] + z[59];
    z[75]=z[8]*z[68];
    z[75]=z[75] + z[74];
    z[75]=z[8]*z[75];
    z[66]=z[75] + z[70] + 8*z[73] + z[29] + z[66];
    z[66]=z[5]*z[66];
    z[70]=z[17]*z[9];
    z[73]= - z[20] + z[70];
    z[73]=z[9]*z[73];
    z[75]=z[17]*z[3];
    z[76]= - z[20] - z[75];
    z[76]=z[3]*z[76];
    z[73]=z[73] + z[76];
    z[73]=z[4]*z[73];
    z[76]=z[13]*z[9];
    z[76]=4*z[76] + 6;
    z[76]=z[28]*z[76];
    z[77]=n<T>(25,2)*z[1] - 4*z[30];
    z[77]=z[2]*z[77];
    z[78]=z[9]*z[23];
    z[77]=z[77] + z[78];
    z[77]=z[9]*z[77];
    z[76]=z[77] + z[76];
    z[76]=z[13]*z[76];
    z[77]=5*z[15];
    z[78]= - z[77] + z[63];
    z[78]=z[2]*z[78];
    z[78]=39*z[1] + z[78];
    z[78]=z[2]*z[78];
    z[79]=z[15] + 2*z[18];
    z[80]= - z[9]*z[79];
    z[80]=z[80] - 24*z[1] - 13*z[30];
    z[80]=z[9]*z[80];
    z[81]= - z[15]*npow(z[3],2);
    z[73]=z[76] + z[73] + z[81] + z[78] + z[80];
    z[73]=z[13]*z[73];
    z[76]=n<T>(31,2)*z[17];
    z[78]=z[7]*z[68];
    z[78]= - n<T>(23,2)*z[78] + z[76] + 24*z[24];
    z[78]=z[7]*z[78];
    z[80]= - z[20] + z[31];
    z[80]=z[11]*z[80];
    z[80]=z[17] + z[80];
    z[80]=z[3]*z[80];
    z[81]=17*z[31];
    z[78]=z[80] + z[78] - z[20] - z[81];
    z[78]=z[3]*z[78];
    z[80]=z[17]*z[54];
    z[36]=z[80] + z[36];
    z[36]=z[7]*z[36];
    z[80]=2*z[17];
    z[36]=z[80] + z[36];
    z[82]=2*z[7];
    z[36]=z[36]*z[82];
    z[83]=3*z[15];
    z[36]= - z[83] + z[36];
    z[84]=npow(z[1],4);
    z[85]=z[68] + z[84];
    z[86]=6*z[6];
    z[86]= - z[85]*z[86];
    z[56]=z[86] + z[56] + z[67];
    z[56]=z[6]*z[56];
    z[36]=z[56] + 2*z[36] + 3*z[70];
    z[56]=4*z[17];
    z[67]=z[3]*z[84];
    z[67]= - z[56] + z[67];
    z[67]=z[3]*z[67];
    z[36]=2*z[36] + z[67];
    z[36]=z[4]*z[36];
    z[67]= - z[54]*z[30];
    z[67]=z[67] - 37*z[15] + 21*z[31];
    z[67]=z[2]*z[67];
    z[86]=n<T>(7,2)*z[7];
    z[87]=z[57]*z[86];
    z[88]=z[24]*z[2];
    z[87]= - 16*z[88] + z[87];
    z[87]=z[7]*z[87];
    z[89]=n<T>(1,2)*z[18];
    z[90]=10*z[15];
    z[91]=z[90] - z[89];
    z[91]=z[9]*z[91];
    z[92]=11*z[1];
    z[19]=z[19] + z[73] + z[66] + z[36] + z[40] + z[78] + z[91] + z[87]
    - z[92] + z[67];
    z[19]=z[14]*z[19];
    z[36]=z[18]*z[38];
    z[40]=z[36] + z[84] + z[49];
    z[40]=z[2]*z[40];
    z[66]=z[51]*z[6];
    z[67]=z[66]*z[38];
    z[73]=z[49] + z[67];
    z[73]=z[6]*z[73];
    z[40]=z[73] + z[17] + z[40];
    z[40]=z[3]*z[40];
    z[73]=z[84]*z[11];
    z[78]=npow(z[1],5);
    z[73]=z[73] - z[78];
    z[87]=z[73]*z[2];
    z[91]= - n<T>(1,2)*z[85] + z[87];
    z[91]=z[2]*z[91];
    z[93]=3*z[17];
    z[94]= - z[93] - 11*z[24];
    z[91]=n<T>(1,2)*z[94] + z[91];
    z[91]=z[2]*z[91];
    z[94]= - 69*z[15] - 47*z[31];
    z[94]=z[11]*z[94];
    z[94]= - 22*z[17] + z[94];
    z[94]=z[11]*z[94];
    z[67]=z[94] + 14*z[67];
    z[67]=z[6]*z[67];
    z[94]=48*z[15] + n<T>(119,2)*z[31];
    z[94]=z[11]*z[94];
    z[95]=n<T>(5,2)*z[17];
    z[67]=z[67] + z[95] + z[94];
    z[67]=z[6]*z[67];
    z[40]=z[40] + z[67] + z[91] - z[20] - n<T>(59,2)*z[31];
    z[40]=z[3]*z[40];
    z[67]= - 13*z[17] + z[24];
    z[91]=npow(z[1],6);
    z[94]=npow(z[9],2);
    z[96]=z[91]*z[94];
    z[96]= - n<T>(9,2)*z[84] + z[96];
    z[96]=z[9]*z[96];
    z[97]=3*z[84];
    z[98]=z[97] + z[68];
    z[99]=z[78]*z[9];
    z[100]= - n<T>(1,2)*z[98] - z[99];
    z[100]=z[8]*z[100];
    z[67]=z[100] + n<T>(1,2)*z[67] + z[96];
    z[67]=z[8]*z[67];
    z[87]= - n<T>(3,2)*z[68] - z[87];
    z[87]=z[2]*z[87];
    z[87]=z[87] + z[95] - z[59];
    z[87]=z[2]*z[87];
    z[96]= - z[91]*z[27];
    z[96]=n<T>(21,4)*z[84] + z[96];
    z[96]=z[2]*z[96];
    z[100]=z[78]*z[2];
    z[101]=z[100] + z[84];
    z[102]=z[3]*z[101];
    z[96]=z[102] + n<T>(33,4)*z[17] + z[96];
    z[96]=z[3]*z[96];
    z[102]=2*z[84];
    z[103]=z[102] - z[99];
    z[103]=z[9]*z[103];
    z[103]=n<T>(5,4)*z[17] + z[103];
    z[103]=z[9]*z[103];
    z[104]=n<T>(7,2)*z[15];
    z[67]=z[67] + z[96] + z[103] + z[104] + z[87];
    z[67]=z[4]*z[67];
    z[87]=8*z[15];
    z[96]=z[60]*z[87];
    z[103]=n<T>(1,2)*z[57];
    z[96]=z[96] - z[103] + z[55] + 8*z[39];
    z[96]=z[7]*z[96];
    z[96]=z[96] + n<T>(3,2)*z[32] - z[62] - n<T>(23,2)*z[31];
    z[96]=z[7]*z[96];
    z[105]=n<T>(17,4)*z[70] - n<T>(19,2)*z[34] + z[77] - z[18];
    z[105]=z[9]*z[105];
    z[99]=z[99] - z[84];
    z[99]=z[99]*z[9];
    z[106]=z[17] + z[99];
    z[106]=z[9]*z[106];
    z[107]=z[84]*z[9];
    z[108]=z[55] - z[107];
    z[108]=z[8]*z[108];
    z[109]=n<T>(3,2)*z[15];
    z[106]=z[108] - n<T>(3,2)*z[64] + z[109] + z[106];
    z[106]=z[8]*z[106];
    z[108]=n<T>(1,2)*z[70];
    z[110]=z[108] + z[15];
    z[111]=z[84]*z[2];
    z[112]=z[111]*z[11];
    z[112]=z[112] + z[68];
    z[113]=n<T>(1,2)*z[2];
    z[114]= - z[3]*z[112]*z[113];
    z[112]= - z[84] + z[112];
    z[112]=z[2]*z[112];
    z[112]= - z[17] + z[112];
    z[112]=z[4]*z[112];
    z[112]=z[112] + z[114] - z[110];
    z[112]=z[5]*z[112];
    z[114]=z[77] + 16*z[31];
    z[114]=z[11]*z[114];
    z[53]=z[6]*z[53];
    z[53]=z[114] - 7*z[53];
    z[53]=z[53]*z[71];
    z[114]=z[35] - 9*z[31];
    z[53]=3*z[114] + z[53];
    z[53]=z[6]*z[53];
    z[114]=3*z[18];
    z[115]=z[77] - z[114];
    z[115]=z[2]*z[115];
    z[115]= - z[1] + z[115];
    z[40]=z[112] + z[67] + z[106] + z[40] + z[53] + z[105] + n<T>(1,2)*
    z[115] + z[96];
    z[40]=z[5]*z[40];
    z[53]=z[7]*z[20];
    z[53]=n<T>(19,2)*z[1] + z[53];
    z[53]=z[53]*z[72];
    z[67]=z[83] - z[34];
    z[96]=3*z[7];
    z[67]=z[67]*z[96];
    z[105]=8*z[1];
    z[67]= - z[105] + z[67];
    z[106]=z[96]*z[9];
    z[67]=z[67]*z[106];
    z[53]=z[53] + z[67];
    z[53]=z[9]*z[53];
    z[67]=npow(z[7],3);
    z[112]=z[67]*z[1];
    z[53]=19*z[112] + z[53];
    z[53]=z[5]*z[53];
    z[115]=z[94]*z[112];
    z[115]=3*z[115];
    z[116]=z[115]*z[12];
    z[117]=n<T>(3,2)*z[1];
    z[118]=z[15]*z[7];
    z[119]=z[117] - z[118];
    z[120]=z[72]*z[9];
    z[121]=9*z[120];
    z[119]=z[119]*z[121];
    z[121]= - z[112] - z[119];
    z[121]=z[9]*z[121];
    z[121]= - z[116] + z[121];
    z[121]=z[5]*z[121];
    z[115]=z[115] + z[121];
    z[115]=z[12]*z[115];
    z[119]=4*z[112] + z[119];
    z[119]=z[9]*z[119];
    z[53]=z[115] + z[119] + z[53];
    z[53]=z[12]*z[53];
    z[115]=7*z[1];
    z[119]=z[115] - 11*z[118];
    z[119]=z[119]*z[72];
    z[121]=z[96]*z[17];
    z[122]=n<T>(17,2)*z[15];
    z[123]=z[122] - z[121];
    z[124]= - z[7]*z[123];
    z[115]=z[115] + z[124];
    z[115]=z[115]*z[106];
    z[115]=z[115] + z[28] + z[119];
    z[115]=z[9]*z[115];
    z[119]=z[27]*z[20];
    z[124]=n<T>(3,2)*z[70];
    z[125]= - z[72]*z[124];
    z[125]= - z[119] + z[125];
    z[125]=z[9]*z[125];
    z[126]=z[3]*z[27]*z[70];
    z[125]=z[125] + z[126];
    z[125]=z[3]*z[125];
    z[126]=z[27]*z[75];
    z[126]= - z[119] + z[126];
    z[126]=z[3]*z[126];
    z[127]= - z[67]*z[37];
    z[126]=z[126] + z[28] + z[127];
    z[126]=z[4]*z[126];
    z[16]= - z[16] - z[34];
    z[16]=z[7]*z[16];
    z[16]=z[41] + z[16];
    z[16]=z[7]*z[16];
    z[41]=n<T>(9,2)*z[17];
    z[127]=z[84]*z[7];
    z[128]= - z[41] + z[127];
    z[128]=z[7]*z[128];
    z[87]=z[87] + z[128];
    z[87]=z[87]*z[96];
    z[128]=z[15]*z[9];
    z[87]=z[128] - 20*z[1] + z[87];
    z[87]=z[9]*z[87];
    z[16]=z[16] + z[87];
    z[16]=z[9]*z[16];
    z[87]= - z[48] - 23*z[31];
    z[87]=z[7]*z[87];
    z[87]=29*z[1] + z[87];
    z[87]=z[87]*z[72];
    z[16]=z[87] + z[16];
    z[16]=z[5]*z[16];
    z[16]=z[53] + z[16] + z[126] + z[125] - 18*z[112] + z[115];
    z[16]=z[12]*z[16];
    z[53]=z[15] - z[31];
    z[32]=8*z[53] - z[32];
    z[32]=z[2]*z[32];
    z[53]=n<T>(1,2)*z[1];
    z[32]= - z[53] + z[32];
    z[32]=z[2]*z[32];
    z[87]=19*z[31];
    z[115]=z[60]*z[1];
    z[125]= - 22*z[115] - n<T>(13,2)*z[15] - z[87];
    z[125]=z[7]*z[125];
    z[126]=n<T>(3,2)*z[30];
    z[129]=4*z[1];
    z[125]=z[125] - z[129] - z[126];
    z[125]=z[7]*z[125];
    z[130]=8*z[17];
    z[131]= - z[130] - z[111];
    z[131]=z[2]*z[131];
    z[131]= - n<T>(39,4)*z[15] + z[131];
    z[131]=z[2]*z[131];
    z[132]= - z[15] + z[34];
    z[86]=z[132]*z[86];
    z[132]=z[18]*z[3];
    z[86]=z[132] + z[131] + z[86];
    z[86]=z[3]*z[86];
    z[131]= - z[30] + n<T>(3,2)*z[118];
    z[131]=z[9]*z[131];
    z[133]=n<T>(1,2)*z[4];
    z[133]= - z[132]*z[133];
    z[32]=z[133] + z[86] + z[131] + z[32] + z[125];
    z[32]=z[4]*z[32];
    z[86]=n<T>(7,2)*z[17] - z[127];
    z[86]=z[86]*z[96];
    z[86]= - 14*z[15] + z[86];
    z[86]=z[7]*z[86];
    z[125]=27*z[1] + n<T>(1,2)*z[30];
    z[86]= - z[128] + n<T>(1,2)*z[125] + z[86];
    z[86]=z[9]*z[86];
    z[22]= - z[22] - z[30];
    z[22]=z[2]*z[22];
    z[125]=10*z[34];
    z[131]= - n<T>(47,2)*z[15] + z[125];
    z[131]=z[7]*z[131];
    z[131]= - n<T>(17,2)*z[1] + z[131];
    z[131]=z[7]*z[131];
    z[22]=z[86] + z[22] + z[131];
    z[22]=z[9]*z[22];
    z[86]= - n<T>(3,4)*z[15] + z[18];
    z[86]=z[2]*z[86];
    z[131]=n<T>(9,2)*z[15];
    z[133]= - z[131] + 7*z[34];
    z[133]=z[7]*z[133];
    z[134]=z[84]*z[96];
    z[135]=n<T>(17,2)*z[17];
    z[134]= - z[135] + z[134];
    z[134]=z[7]*z[134];
    z[134]=n<T>(3,4)*z[18] + z[134];
    z[134]=z[9]*z[134];
    z[86]=3*z[132] + z[134] + z[86] + z[133];
    z[86]=z[9]*z[86];
    z[86]= - z[119] + z[86];
    z[86]=z[3]*z[86];
    z[119]= - z[122] + z[34];
    z[119]=z[7]*z[119];
    z[122]= - n<T>(9,4)*z[15] - z[70];
    z[122]=z[9]*z[122];
    z[119]=z[122] + n<T>(31,2)*z[1] + z[119];
    z[119]=z[9]*z[119];
    z[122]=n<T>(1,2)*z[15];
    z[132]=z[122] + z[31];
    z[133]= - 33*z[132] + 8*z[115];
    z[133]=z[7]*z[133];
    z[133]=10*z[1] + z[133];
    z[133]=z[7]*z[133];
    z[119]=z[133] + z[119];
    z[119]=z[5]*z[119];
    z[26]= - z[9]*z[26];
    z[105]= - z[105] + z[128];
    z[105]=z[5]*z[105]*z[94];
    z[26]=z[26] + z[105];
    z[26]=z[13]*z[26];
    z[105]=6*z[1];
    z[133]= - z[105] - z[30];
    z[133]=z[133]*z[27];
    z[134]=7*z[31];
    z[136]=z[29] - z[134];
    z[136]=z[7]*z[136];
    z[92]= - z[92] + z[136];
    z[92]=z[92]*z[72];
    z[16]=z[16] + z[26] + z[119] + z[32] + z[86] + z[22] + z[133] + 2*
    z[92];
    z[16]=z[12]*z[16];
    z[22]=z[37] + z[77];
    z[22]=z[22]*z[11];
    z[26]= - z[80] - z[22];
    z[26]=z[11]*z[26];
    z[32]=z[38]*z[2];
    z[38]=z[51]*z[32];
    z[26]=10*z[26] + 13*z[38];
    z[26]=z[2]*z[26];
    z[38]=z[62] + z[63];
    z[38]=z[11]*z[38];
    z[38]=z[130] + z[38];
    z[38]=z[11]*z[38];
    z[38]=z[97] + z[38];
    z[38]=z[38]*z[54];
    z[86]=z[83] + z[31];
    z[92]=z[86]*z[11];
    z[92]=z[92] + z[93];
    z[119]= - z[11]*z[92];
    z[119]= - z[84] + z[119];
    z[133]=3*z[32];
    z[119]=z[119]*z[133];
    z[38]=z[38] + z[119];
    z[38]=z[2]*z[38];
    z[119]= - z[77] - z[31];
    z[119]=z[11]*z[119];
    z[136]=7*z[17];
    z[119]= - z[136] + z[119];
    z[119]=z[11]*z[119];
    z[38]=z[38] - z[97] + z[119];
    z[38]=z[6]*z[38];
    z[119]=36*z[15] + z[81];
    z[119]=z[11]*z[119];
    z[137]=19*z[17];
    z[26]=z[38] + z[26] + z[137] + z[119];
    z[26]=z[6]*z[26];
    z[38]=n<T>(11,2)*z[31];
    z[119]=z[20] + z[38];
    z[119]=z[11]*z[119];
    z[138]=n<T>(3,2)*z[17];
    z[119]= - z[138] + z[119];
    z[139]=z[52]*z[32];
    z[119]=9*z[119] - 22*z[139];
    z[119]=z[2]*z[119];
    z[26]=z[26] - z[124] + z[119] - n<T>(49,2)*z[15] - 29*z[31];
    z[26]=z[6]*z[26];
    z[119]=z[51]*z[60];
    z[139]=4*z[11];
    z[51]= - z[51]*z[139];
    z[51]=z[51] - z[119];
    z[51]=z[7]*z[51];
    z[140]=6*z[31];
    z[141]= - n<T>(23,2)*z[15] - z[140];
    z[141]=z[11]*z[141];
    z[41]=z[107] + z[51] - z[41] + z[141];
    z[41]=z[8]*z[41];
    z[51]=13*z[31];
    z[141]=z[43] + z[51];
    z[141]=z[11]*z[141];
    z[142]=10*z[17];
    z[141]=z[142] + z[141];
    z[141]=z[11]*z[141];
    z[119]=z[141] + 7*z[119];
    z[119]=z[119]*z[82];
    z[141]=38*z[15] + n<T>(65,2)*z[31];
    z[141]=z[11]*z[141];
    z[119]=z[119] + z[135] + z[141];
    z[119]=z[7]*z[119];
    z[135]=z[131] + z[45];
    z[141]=6*z[17];
    z[99]=z[141] - z[99];
    z[99]=z[9]*z[99];
    z[41]=z[41] + z[99] + 5*z[135] + z[119];
    z[41]=z[8]*z[41];
    z[99]=z[100] - z[84];
    z[99]=z[99]*z[2];
    z[119]= - n<T>(61,4)*z[17] + z[99];
    z[119]=z[2]*z[119];
    z[135]=z[80] + z[111];
    z[143]= - z[3]*z[135];
    z[119]=z[143] - n<T>(3,2)*z[34] - n<T>(23,4)*z[15] + z[119];
    z[119]=z[3]*z[119];
    z[87]= - z[122] + z[87];
    z[87]=z[11]*z[87];
    z[46]=z[11]*z[46];
    z[46]= - z[84] + z[46];
    z[46]=z[2]*z[46];
    z[46]=z[46] + z[17] + z[87];
    z[46]=z[2]*z[46];
    z[46]=z[46] + n<T>(105,4)*z[15] - z[81];
    z[46]=z[2]*z[46];
    z[87]=12*z[15];
    z[143]= - z[87] - z[44];
    z[58]=z[143]*z[58];
    z[143]= - 11*z[15] - z[134];
    z[143]=z[143]*z[60];
    z[58]=z[58] + 2*z[143];
    z[58]=z[7]*z[58];
    z[58]=z[58] - n<T>(35,2)*z[15] - 33*z[31];
    z[58]=z[7]*z[58];
    z[143]=z[107] + z[17];
    z[144]=n<T>(1,2)*z[143];
    z[144]=z[8]*z[144];
    z[89]=z[144] + z[15] + z[89];
    z[89]=z[4]*z[89];
    z[21]= - z[124] - z[21];
    z[21]=z[9]*z[21];
    z[21]=z[89] + z[41] + z[119] + z[26] + z[21] + z[58] - 3*z[1] + 
    z[46];
    z[21]=z[4]*z[21];
    z[26]=z[130] - z[107];
    z[26]=z[9]*z[26];
    z[26]=z[83] + z[26];
    z[26]=z[9]*z[26];
    z[41]= - z[15] + z[64];
    z[41]=z[6]*z[41];
    z[46]= - z[8]*z[70];
    z[58]=z[5]*z[108];
    z[26]=z[58] + z[46] + z[26] + n<T>(7,2)*z[41];
    z[26]=z[8]*z[26];
    z[41]=14*z[39];
    z[46]= - z[6]*z[41];
    z[46]=z[46] - z[122] + z[33];
    z[46]=z[6]*z[46];
    z[58]=n<T>(3,2)*z[128];
    z[89]=14*z[1];
    z[46]=z[46] + z[58] - z[89] - z[126];
    z[46]=z[6]*z[46];
    z[119]= - z[61] + z[70];
    z[119]=z[9]*z[119];
    z[119]=z[119] + z[53] + z[23];
    z[119]=z[9]*z[119];
    z[124]=z[143]*z[4];
    z[126]=n<T>(9,4)*z[17] + z[107];
    z[126]=z[9]*z[126];
    z[143]=n<T>(7,4)*z[15];
    z[126]=z[124] + z[143] + z[126];
    z[126]=z[9]*z[126];
    z[75]=z[143] + z[75];
    z[75]=z[3]*z[75];
    z[75]=z[75] + z[126];
    z[75]=z[4]*z[75];
    z[126]=z[3]*z[15];
    z[126]= - n<T>(15,2)*z[1] + z[126];
    z[126]=z[3]*z[126];
    z[26]=z[75] + z[126] + z[119] + z[46] + z[26];
    z[26]=z[5]*z[26];
    z[46]= - z[15] - 17*z[18];
    z[46]=z[46]*z[113];
    z[75]=z[65] + 3*z[111];
    z[119]=z[6]*z[2];
    z[75]=z[75]*z[119];
    z[46]=z[46] + z[75];
    z[46]=z[6]*z[46];
    z[35]= - z[35] + 2*z[70];
    z[35]=z[9]*z[35];
    z[75]=z[70] + z[15];
    z[126]= - z[3]*z[75];
    z[35]=z[126] - n<T>(7,2)*z[1] + z[35];
    z[35]=z[3]*z[35];
    z[126]=z[15] + z[18];
    z[126]=z[126]*z[113];
    z[114]=z[9]*z[114];
    z[114]=z[126] + z[114];
    z[114]=z[9]*z[114];
    z[126]=z[47] + z[70];
    z[143]=3*z[3];
    z[144]= - z[126]*z[143];
    z[144]=z[53] + z[144];
    z[144]=z[8]*z[144];
    z[35]=z[144] + z[35] + z[114] + z[46];
    z[35]=z[8]*z[35];
    z[46]=z[122] + z[63];
    z[46]=z[2]*z[46];
    z[46]=z[1] + z[46];
    z[46]=z[2]*z[46];
    z[114]= - z[2]*z[39];
    z[114]= - z[31] + z[114];
    z[114]=z[114]*z[119];
    z[46]=z[46] + z[114];
    z[46]=z[6]*z[46];
    z[46]= - z[28] + z[46];
    z[46]=z[6]*z[46];
    z[114]= - z[8]*z[55];
    z[114]=z[15] + z[114];
    z[114]=z[8]*z[114];
    z[114]=z[114] - z[53];
    z[114]=z[94]*z[114];
    z[122]=npow(z[6],2);
    z[144]=z[6]*z[31];
    z[144]= - z[1] + z[144];
    z[144]=z[144]*z[122];
    z[114]=z[144] + z[114];
    z[114]=z[5]*z[114];
    z[144]=z[9]*z[28];
    z[46]=z[114] + z[144] + z[46];
    z[27]= - z[27]*z[122]*z[138];
    z[114]=z[94]*z[18];
    z[110]=z[9]*z[110];
    z[53]=z[53] + z[110];
    z[53]=z[53]*z[143];
    z[53]=z[114] + z[53];
    z[53]=z[8]*z[53];
    z[23]= - z[94]*z[23];
    z[23]=z[53] + z[23] + z[27];
    z[23]=z[8]*z[23];
    z[27]= - z[20] - z[70];
    z[27]=z[9]*z[27];
    z[27]= - z[1] + z[27];
    z[27]=z[4]*z[27]*npow(z[8],2);
    z[23]=z[27] + z[23] + 3*z[46];
    z[23]=z[13]*z[23];
    z[27]=2*z[66];
    z[46]=z[27] - z[108] + z[132];
    z[46]=z[6]*z[46];
    z[53]=z[66] - z[75];
    z[53]=z[3]*z[53];
    z[46]=z[53] - n<T>(1,2)*z[128] + z[46];
    z[46]=z[3]*z[46];
    z[53]= - z[87] - z[51];
    z[53]=z[11]*z[53];
    z[53]= - z[93] + z[53];
    z[53]=z[2]*z[53];
    z[33]=z[53] - z[77] - z[33];
    z[33]=z[2]*z[33];
    z[53]=z[86]*z[133];
    z[41]=z[41] + z[53];
    z[41]=z[41]*z[119];
    z[33]=z[33] + z[41];
    z[33]=z[6]*z[33];
    z[41]=z[90] + z[81];
    z[41]=z[2]*z[41];
    z[41]=z[89] + z[41];
    z[41]=z[2]*z[41];
    z[33]=z[46] + z[41] + z[33];
    z[33]=z[6]*z[33];
    z[27]= - z[27] - z[108] - z[109] - z[31];
    z[27]=z[6]*z[27];
    z[27]=z[58] + z[27];
    z[27]=z[6]*z[27];
    z[41]=z[55] + z[107];
    z[41]=z[9]*z[41];
    z[41]=n<T>(1,2)*z[124] + z[131] + z[41];
    z[41]=z[9]*z[41];
    z[46]=z[8]*z[126];
    z[41]= - 4*z[46] + z[105] + z[41];
    z[41]=z[8]*z[41];
    z[27]=z[27] + z[41];
    z[27]=z[4]*z[27];
    z[41]=5*z[1];
    z[46]=z[41] - z[30];
    z[46]=z[2]*z[46];
    z[53]=n<T>(5,2)*z[1];
    z[55]=z[53] - z[30];
    z[55]=z[9]*z[55];
    z[46]=n<T>(3,2)*z[46] + z[55];
    z[46]=z[9]*z[46];
    z[23]=z[23] + z[26] + z[27] + z[35] - 4*z[28] + z[46] + z[33];
    z[23]=z[13]*z[23];
    z[26]= - z[83] - z[34];
    z[26]=z[26]*z[120];
    z[27]=z[70]*z[6];
    z[28]=z[72]*z[27];
    z[26]=z[26] + z[28];
    z[26]=z[6]*z[26];
    z[28]= - z[17] + z[127];
    z[28]=z[6]*z[28];
    z[28]=3*z[28] + z[29] - 5*z[34];
    z[33]=z[72]*z[6];
    z[28]=z[28]*z[33];
    z[35]=z[7]*z[47];
    z[35]= - z[1] + z[35];
    z[35]=z[35]*z[72];
    z[28]=z[35] + z[28];
    z[28]=z[8]*z[28];
    z[35]=z[20]*z[9];
    z[46]=z[35] - z[27];
    z[55]=z[67]*z[6];
    z[46]=z[46]*z[55];
    z[58]= - z[20] + z[64];
    z[55]=z[58]*z[55];
    z[55]=z[112] + z[55];
    z[55]=z[8]*z[55];
    z[58]=z[112]*z[9];
    z[46]=z[55] - z[58] + z[46];
    z[46]=z[10]*z[46];
    z[55]=z[1] + z[118];
    z[55]=z[55]*z[120];
    z[58]= - z[12]*z[58];
    z[26]=z[46] + z[58] + z[28] + z[26] - z[112] + 2*z[55];
    z[26]=z[10]*z[26];
    z[28]=z[80] - z[127];
    z[28]=z[28]*z[96];
    z[28]= - z[15] + z[28];
    z[28]=z[28]*z[96];
    z[33]= - z[97]*z[33];
    z[46]= - z[7]*z[70];
    z[28]=z[33] + z[28] + z[46];
    z[28]=z[6]*z[28];
    z[33]= - z[20] + z[121];
    z[33]=z[33]*z[72];
    z[46]=z[62] - z[34];
    z[46]=z[9]*z[7]*z[46];
    z[28]=z[28] + 3*z[33] + z[46];
    z[33]=4*z[6];
    z[28]=z[28]*z[33];
    z[46]=z[87] + z[134];
    z[46]=z[11]*z[46];
    z[46]=z[141] + z[46];
    z[46]=z[46]*z[82];
    z[46]=z[46] - z[29] - n<T>(3,2)*z[31];
    z[46]=z[7]*z[46];
    z[55]=z[137] - 12*z[127];
    z[55]=z[7]*z[55];
    z[58]=z[7]*z[78];
    z[58]= - z[102] + z[58];
    z[66]=9*z[7];
    z[58]=z[58]*z[66];
    z[58]=z[142] + z[58];
    z[58]=z[6]*z[58];
    z[55]=z[58] - z[90] + z[55];
    z[33]=z[33]*z[55];
    z[55]= - z[63] - z[115];
    z[55]=z[8]*z[55];
    z[33]=z[33] + z[55] + n<T>(39,2)*z[1] + z[46];
    z[33]=z[8]*z[7]*z[33];
    z[39]=z[8]*z[39];
    z[39]=n<T>(3,2)*z[39] + 12*z[64] - z[87] - z[38];
    z[39]=z[8]*z[39];
    z[46]=z[9]*z[83];
    z[46]= - 3*z[27] + z[1] + z[46];
    z[55]=z[13]*z[1]*z[94];
    z[55]=n<T>(11,2)*z[25] + z[55];
    z[55]=z[13]*z[55];
    z[39]=z[55] + 4*z[46] + z[39];
    z[39]=z[14]*z[39];
    z[46]= - z[20] - n<T>(5,2)*z[64];
    z[46]=z[6]*z[46];
    z[55]=z[8]*z[37];
    z[41]=z[55] - z[41] + z[46];
    z[41]=z[8]*z[41];
    z[27]=z[35] + n<T>(5,2)*z[27];
    z[27]=z[6]*z[27];
    z[25]=z[39] + z[41] + n<T>(15,2)*z[25] + z[27];
    z[25]=z[14]*z[25];
    z[27]=z[7]*z[29];
    z[27]=z[117] + z[27];
    z[27]=z[27]*z[72];
    z[35]=z[77] - z[121];
    z[35]=z[7]*z[35];
    z[35]= - 2*z[1] + z[35];
    z[35]=z[35]*z[106];
    z[27]=5*z[27] + z[35];
    z[27]=z[9]*z[27];
    z[35]=z[15]*z[96];
    z[35]= - z[53] + z[35];
    z[35]=z[35]*z[120];
    z[35]= - 10*z[112] + 3*z[35];
    z[35]=z[9]*z[35];
    z[35]=z[35] - z[116];
    z[35]=z[12]*z[35];
    z[27]=z[35] - 14*z[112] + z[27];
    z[27]=z[12]*z[27];
    z[35]= - z[104] - z[125];
    z[35]=z[7]*z[35];
    z[35]= - n<T>(59,2)*z[1] + z[35];
    z[35]=z[7]*z[35];
    z[39]= - z[95] + z[127];
    z[39]=z[7]*z[39];
    z[39]=z[20] + z[39];
    z[39]=z[39]*z[96];
    z[39]= - z[1] + z[39];
    z[39]=z[9]*z[39];
    z[35]=z[35] + z[39];
    z[35]=z[9]*z[35];
    z[39]= - z[77] - z[134];
    z[39]=z[39]*z[82];
    z[39]=z[53] + z[39];
    z[39]=z[39]*z[72];
    z[25]=12*z[26] + z[27] + z[25] + z[33] + z[28] + z[39] + z[35];
    z[25]=z[10]*z[25];
    z[26]=z[101]*z[9];
    z[27]=z[62] + z[37];
    z[27]=z[11]*z[27];
    z[28]=5*z[84];
    z[33]=3*z[68];
    z[35]=z[28] + z[33];
    z[35]=z[35]*z[113];
    z[27]=z[26] + z[35] + z[136] + n<T>(3,2)*z[27];
    z[27]=z[3]*z[27];
    z[35]= - z[47]*z[60];
    z[22]=z[35] - z[22] + z[57];
    z[22]=z[7]*z[22];
    z[35]=13*z[15];
    z[37]=n<T>(3,2)*z[18];
    z[39]=z[111]*z[9];
    z[22]=z[27] - z[39] + z[22] - z[37] - z[35] - z[140];
    z[22]=z[8]*z[22];
    z[27]=z[97] + 5*z[68];
    z[27]=z[27]*z[113];
    z[27]=z[27] - z[56] + n<T>(11,2)*z[24];
    z[27]=z[2]*z[27];
    z[41]= - z[26] - n<T>(19,2)*z[17] - z[99];
    z[41]=z[9]*z[41];
    z[46]=z[102] + z[68];
    z[46]=z[46]*z[11];
    z[46]=z[46] + z[78];
    z[47]=z[2]*z[46];
    z[47]=z[47] - z[84] + z[49];
    z[47]=z[2]*z[47];
    z[26]= - z[26] - z[17] + z[47];
    z[26]=z[3]*z[26];
    z[26]=z[26] + z[41] + z[27] - z[29] + z[38];
    z[26]=z[3]*z[26];
    z[27]=z[48] + 12*z[31];
    z[27]=z[11]*z[27];
    z[38]=z[141] + 7*z[50];
    z[38]=z[7]*z[38]*z[54];
    z[27]=z[38] + z[103] + z[130] + z[27];
    z[27]=z[7]*z[27];
    z[38]=41*z[31];
    z[41]=33*z[15] + z[38];
    z[47]=z[56] + n<T>(1,2)*z[24];
    z[47]=z[2]*z[47];
    z[27]=z[27] + n<T>(1,2)*z[41] + z[47];
    z[27]=z[7]*z[27];
    z[28]= - z[28] - n<T>(3,2)*z[100];
    z[28]=z[2]*z[28];
    z[41]=3*z[78];
    z[47]=z[7]*z[91];
    z[47]= - z[41] + z[47];
    z[47]=z[47]*z[96];
    z[47]=10*z[84] + z[47];
    z[47]=z[47]*z[42];
    z[28]=z[47] - n<T>(51,2)*z[17] + z[28];
    z[28]=z[6]*z[28];
    z[47]=23*z[17] + n<T>(17,2)*z[111];
    z[47]=z[2]*z[47];
    z[49]=z[96]*z[78];
    z[53]=7*z[84];
    z[54]=z[53] - z[49];
    z[54]=z[54]*z[42];
    z[55]=29*z[17];
    z[54]= - z[55] + z[54];
    z[54]=z[7]*z[54];
    z[28]=z[28] + z[54] + 16*z[15] + z[47];
    z[28]=z[6]*z[28];
    z[47]=z[95] + z[111];
    z[47]=z[2]*z[47];
    z[39]=z[39] + z[131] + z[47];
    z[39]=z[9]*z[39];
    z[47]=z[2]*z[79];
    z[22]=z[22] + z[26] + z[28] + z[39] + z[27] - z[129] - 5*z[47];
    z[22]=z[8]*z[22];
    z[26]= - z[137] - 12*z[24];
    z[26]=z[11]*z[26];
    z[27]=z[46]*z[66];
    z[26]=z[27] - z[53] + z[26];
    z[26]=z[26]*z[42];
    z[27]=z[11]*z[74];
    z[27]=4*z[84] + z[27];
    z[27]=z[11]*z[27];
    z[28]= - z[11]*z[98];
    z[28]= - z[41] + z[28];
    z[28]=z[11]*z[28];
    z[28]= - z[91] + z[28];
    z[28]=z[7]*z[28];
    z[27]=z[28] + z[78] + z[27];
    z[27]=z[7]*z[27];
    z[28]=z[52]*z[11];
    z[39]=z[28] - z[17];
    z[39]=z[39]*z[11];
    z[39]=z[39] - z[84];
    z[27]=6*z[27] + z[39];
    z[27]=z[27]*z[71];
    z[41]= - z[61] - 5*z[31];
    z[41]=z[41]*z[139];
    z[26]=z[27] + n<T>(1,2)*z[107] + z[26] - z[76] + z[41];
    z[26]=z[6]*z[26];
    z[27]=z[83] + z[45];
    z[41]=z[7]*z[85];
    z[41]=z[24] - z[41];
    z[41]=z[55] + 40*z[41];
    z[41]=z[7]*z[41];
    z[26]=z[26] - n<T>(9,2)*z[70] + 7*z[27] + z[41];
    z[26]=z[6]*z[26];
    z[27]=17*z[84] - z[49];
    z[27]=z[7]*z[27];
    z[27]=n<T>(1,2)*z[27] - n<T>(39,4)*z[17] + z[111];
    z[27]=z[9]*z[27];
    z[41]= - n<T>(27,4)*z[17] + z[111];
    z[41]=z[2]*z[41];
    z[45]=27*z[17] - 7*z[127];
    z[45]=z[7]*z[45];
    z[27]=z[27] + z[45] - n<T>(31,4)*z[15] + z[41];
    z[27]=z[9]*z[27];
    z[41]=z[29] + z[31];
    z[41]=z[11]*z[41];
    z[39]=z[6]*z[39];
    z[39]=z[39] + z[107] + z[56] + z[41];
    z[39]=z[6]*z[39];
    z[41]=z[93] - z[111];
    z[41]=z[41]*z[2];
    z[45]=z[9]*z[135];
    z[39]=z[39] + z[45] + z[15] + z[41];
    z[39]=z[3]*z[39];
    z[41]= - z[41] - z[83] - z[63];
    z[41]=z[2]*z[41];
    z[34]= - z[15] + n<T>(13,2)*z[34];
    z[34]=z[7]*z[34];
    z[26]=z[39] + z[26] + z[27] + z[34] - z[129] + z[41];
    z[26]=z[3]*z[26];
    z[27]=z[7]*z[73];
    z[27]=z[27] - z[69] - z[36];
    z[27]=z[27]*z[96];
    z[34]= - z[80] + z[24];
    z[36]=z[17] + 9*z[24];
    z[36]=z[2]*z[11]*z[36];
    z[27]=z[27] + 6*z[34] + z[36];
    z[27]=z[27]*z[42];
    z[34]= - z[48] + z[51];
    z[34]=z[11]*z[34];
    z[34]=9*z[17] + z[34];
    z[34]=z[11]*z[34];
    z[36]= - z[92]*z[133];
    z[34]=z[34] + z[36];
    z[34]=z[2]*z[34];
    z[36]=z[11]*z[97];
    z[39]=npow(z[11],3)*z[18];
    z[36]=z[39] + z[78] + z[36];
    z[36]=z[7]*z[36];
    z[39]=z[17] - z[59];
    z[32]=z[39]*z[32];
    z[32]=z[36] - z[33] + z[32];
    z[32]=z[7]*z[32];
    z[32]=z[34] + 12*z[32];
    z[32]=z[6]*z[32];
    z[20]=z[20] - z[38];
    z[20]=z[11]*z[20];
    z[33]=z[141] + 13*z[50];
    z[33]=z[11]*z[33];
    z[33]=n<T>(3,2)*z[84] + z[33];
    z[33]=z[2]*z[33];
    z[20]=z[33] + z[80] + z[20];
    z[20]=z[2]*z[20];
    z[20]=z[32] + z[108] + z[27] + z[87] + z[20];
    z[20]=z[6]*z[20];
    z[27]= - 17*z[15] - 22*z[31];
    z[27]=z[11]*z[27];
    z[27]= - z[136] + z[27];
    z[27]=z[2]*z[27];
    z[27]=z[27] - z[35] + 40*z[31];
    z[27]=z[2]*z[27];
    z[31]= - z[93] - 28*z[24];
    z[31]=z[2]*z[31];
    z[24]=z[56] + z[24];
    z[24]=3*z[24] + z[57];
    z[24]=z[24]*z[42];
    z[24]=z[31] + z[24];
    z[24]=z[7]*z[24];
    z[31]= - z[9]*z[123];
    z[20]=z[20] + z[31] + z[27] + z[24];
    z[20]=z[6]*z[20];
    z[24]=z[65] - 7*z[28];
    z[24]=z[24]*z[82];
    z[24]=z[24] + z[37] - z[43] - z[44];
    z[24]=z[7]*z[24];
    z[27]= - 17*z[1] - z[30];
    z[24]=n<T>(3,2)*z[27] + z[24];
    z[24]=z[7]*z[24];
    z[27]= - z[136] + n<T>(3,2)*z[127];
    z[27]=z[7]*z[27];
    z[27]=z[70] + z[27] + z[29] - z[18];
    z[27]=z[9]*z[27];
    z[18]=n<T>(21,4)*z[15] - z[18];
    z[18]=z[2]*z[18];
    z[17]=n<T>(11,2)*z[17] - z[127];
    z[17]=z[17]*z[96];
    z[17]= - n<T>(55,2)*z[15] + z[17];
    z[17]=z[7]*z[17];
    z[17]=z[27] + z[17] + z[117] + z[18];
    z[17]=z[9]*z[17];
    z[15]=z[88] + n<T>(11,2)*z[15] + z[81];
    z[15]=z[2]*z[15];
    z[15]=z[89] + z[15];
    z[15]=z[2]*z[15];

    r += z[15] + z[16] + z[17] + z[19] + z[20] + z[21] + z[22] + z[23]
       + z[24] + z[25] + z[26] + z[40];
 
    return r;
}

template double qqb_2lNLC_r644(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r644(const std::array<dd_real,31>&);
#endif
