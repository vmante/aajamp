#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1205(const std::array<T,31>& k) {
  T z[101];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[12];
    z[4]=k[19];
    z[5]=k[22];
    z[6]=k[13];
    z[7]=k[11];
    z[8]=k[8];
    z[9]=k[9];
    z[10]=k[14];
    z[11]=k[10];
    z[12]=k[2];
    z[13]=k[3];
    z[14]=k[4];
    z[15]=k[5];
    z[16]=k[15];
    z[17]=k[17];
    z[18]=npow(z[1],3);
    z[19]=z[18]*z[3];
    z[20]=z[18]*z[6];
    z[21]=z[19] - z[20];
    z[22]=z[21]*z[5];
    z[23]=npow(z[1],2);
    z[24]=5*z[23];
    z[25]=z[24] + z[19];
    z[25]=z[3]*z[25];
    z[26]= - z[24] + 13*z[20];
    z[26]=z[6]*z[26];
    z[25]= - 14*z[22] + z[25] + z[26];
    z[26]=2*z[5];
    z[25]=z[25]*z[26];
    z[27]=2*z[23];
    z[19]= - z[27] - z[19];
    z[19]=z[3]*z[19];
    z[28]=z[20] - z[27];
    z[29]= - z[6]*z[28];
    z[21]=z[21]*z[26];
    z[19]=z[21] + z[19] + z[29];
    z[19]=z[5]*z[19];
    z[21]=npow(z[6],2);
    z[29]=z[21]*z[27];
    z[19]=z[29] + z[19];
    z[29]=8*z[5];
    z[19]=z[19]*z[29];
    z[30]=npow(z[3],2);
    z[31]=npow(z[7],2);
    z[31]=z[30] + z[31];
    z[32]=npow(z[8],2);
    z[33]=z[32]*z[18];
    z[31]=z[33]*z[31];
    z[19]=z[19] + z[31];
    z[19]=z[14]*z[19];
    z[31]=z[18]*z[8];
    z[34]= - z[23] + 4*z[31];
    z[34]=z[34]*z[8];
    z[35]=2*z[8];
    z[36]=npow(z[1],4);
    z[37]=z[35]*z[36];
    z[38]=3*z[18];
    z[39]=z[38] - z[37];
    z[40]=z[7]*z[8];
    z[39]=z[39]*z[40];
    z[39]=z[34] + z[39];
    z[39]=z[7]*z[39];
    z[41]= - z[3]*z[31];
    z[34]=z[34] + z[41];
    z[34]=z[3]*z[34];
    z[21]=z[23]*z[21];
    z[19]=z[19] + z[25] - 16*z[21] + z[34] + z[39];
    z[19]=z[14]*z[19];
    z[21]=z[36]*z[7];
    z[21]=z[21] - z[18];
    z[25]=npow(z[1],5);
    z[34]=z[7]*z[25];
    z[34]= - z[36] + z[34];
    z[34]=z[9]*z[34];
    z[34]= - 3*z[21] + z[34];
    z[39]=2*z[9];
    z[34]=z[34]*z[39];
    z[41]=6*z[23];
    z[42]=z[18]*z[7];
    z[34]=z[34] + 6*z[42] - z[41] - z[31];
    z[43]=4*z[9];
    z[34]=z[34]*z[43];
    z[44]=5*z[18];
    z[45]=z[44] - z[37];
    z[45]=z[8]*z[45];
    z[45]= - z[41] + z[45];
    z[46]=z[25]*z[8];
    z[47]= - 3*z[36] + z[46];
    z[47]=z[8]*z[47];
    z[48]=2*z[18];
    z[47]=z[48] + z[47];
    z[47]=z[7]*z[47];
    z[45]=2*z[45] + z[47];
    z[45]=z[7]*z[45];
    z[47]=3*z[23];
    z[49]= - z[9]*z[18];
    z[49]= - 9*z[20] - z[47] + z[49];
    z[50]=2*z[6];
    z[49]=z[49]*z[50];
    z[51]= - z[23] + 3*z[31];
    z[51]=z[8]*z[51];
    z[52]=2*z[1];
    z[51]=z[52] + z[51];
    z[53]= - z[47] - 5*z[31];
    z[53]=z[3]*z[53];
    z[19]=z[19] + 10*z[22] + z[49] + z[34] + z[45] + 2*z[51] + z[53];
    z[19]=z[11]*z[19];
    z[22]=z[15]*z[1];
    z[34]=2*z[22];
    z[45]=z[34] + z[23];
    z[49]=4*z[22];
    z[51]= - z[47] - z[49];
    z[51]=z[15]*z[51];
    z[51]=z[18] + z[51];
    z[53]=3*z[3];
    z[51]=z[51]*z[53];
    z[51]=z[51] - 5*z[45];
    z[51]=z[15]*z[51];
    z[54]=npow(z[15],2);
    z[55]=z[54]*z[1];
    z[56]=z[55] - z[18];
    z[57]=z[54]*z[3];
    z[57]=z[57] + z[15];
    z[56]= - z[56]*z[57];
    z[56]=z[46] - z[36] + z[56];
    z[56]=z[56]*z[39];
    z[58]=2*z[36];
    z[46]= - z[58] - z[46];
    z[46]=z[8]*z[46];
    z[46]=z[56] + z[46] + z[38] + z[51];
    z[46]=z[9]*z[46];
    z[51]=z[36]*z[8];
    z[56]=z[18] + z[51];
    z[56]=z[8]*z[56];
    z[59]= - 18*z[23] - 19*z[22];
    z[59]=z[15]*z[59];
    z[59]=z[18] + z[59];
    z[59]=z[3]*z[59];
    z[60]=11*z[22];
    z[46]=z[46] + z[59] + z[56] - z[47] - z[60];
    z[46]=z[46]*z[39];
    z[56]=z[23]*z[15];
    z[59]=z[56] + z[18];
    z[61]=z[18]*z[15];
    z[62]=z[61] + z[36];
    z[63]= - z[6]*z[62];
    z[63]=z[63] + z[59];
    z[63]=z[6]*z[63];
    z[64]= - z[3]*z[59];
    z[65]=z[6] - z[3];
    z[62]= - z[5]*z[62]*z[65];
    z[62]=z[62] + z[64] + z[63];
    z[62]=z[62]*z[26];
    z[63]= - z[31] - 17*z[23] - 18*z[22];
    z[63]=z[3]*z[63];
    z[64]=z[31] - z[23];
    z[66]= - z[64]*z[35];
    z[67]=z[22] + z[23];
    z[68]=2*z[67];
    z[69]= - z[7]*z[68];
    z[28]=z[28]*z[50];
    z[70]=z[3]*z[8];
    z[70]=z[70] + z[40];
    z[70]=z[14]*z[23]*z[70];
    z[28]=z[70] + z[62] + z[28] + z[46] + z[69] + z[66] + z[63];
    z[28]=z[10]*z[28];
    z[46]=n<T>(2,3)*z[5];
    z[62]=npow(z[1],6);
    z[46]=z[46]*z[62];
    z[63]=z[6]*z[7];
    z[66]=z[7]*z[3];
    z[69]= - z[66] + z[63];
    z[69]=z[69]*z[46];
    z[70]=n<T>(2,3)*z[3];
    z[62]=z[70]*z[62];
    z[62]=z[62] + z[25];
    z[71]=z[62]*z[66];
    z[72]= - z[25]*z[63];
    z[69]=z[69] + z[71] + z[72];
    z[69]=z[5]*z[69];
    z[62]=z[62]*z[3];
    z[71]=n<T>(5,3)*z[36];
    z[72]=z[62] + z[71];
    z[73]= - z[72]*z[66];
    z[74]=z[63]*z[71];
    z[69]=z[69] + z[73] + z[74];
    z[73]=4*z[5];
    z[69]=z[69]*z[73];
    z[46]= - z[65]*z[46];
    z[74]=z[6]*z[25];
    z[46]=z[46] - z[62] + z[74];
    z[46]=z[5]*z[46];
    z[62]=z[3]*z[72];
    z[71]= - z[6]*z[71];
    z[46]=z[46] + z[62] + z[71];
    z[26]=z[46]*z[26];
    z[46]=2*z[3];
    z[62]=z[46]*z[25];
    z[62]=z[62] + z[36];
    z[71]=n<T>(2,3)*z[30];
    z[62]=z[62]*z[71];
    z[62]=z[62] - z[47];
    z[71]=4*z[6];
    z[72]=z[56]*z[71];
    z[26]=z[26] + z[72] - z[62];
    z[26]=z[2]*z[26];
    z[72]=z[2]*z[3];
    z[72]= - z[66] + z[72];
    z[74]=z[70]*z[36];
    z[74]=z[74] + z[18];
    z[74]=z[74]*z[3];
    z[74]=z[74] + z[23];
    z[72]=z[13]*z[74]*z[72];
    z[74]=z[20]*z[15];
    z[75]= - z[18] - z[74];
    z[75]=z[2]*z[75];
    z[76]=z[7]*z[74];
    z[75]=z[75] + z[42] + z[76];
    z[75]=z[4]*z[75];
    z[62]=2*z[62] + z[42];
    z[62]=z[7]*z[62];
    z[76]=z[15]*z[42];
    z[76]= - 8*z[56] + z[76];
    z[63]=z[76]*z[63];
    z[26]=z[75] + 4*z[72] + 2*z[26] + z[69] + z[62] + z[63];
    z[26]=z[4]*z[26];
    z[62]=z[64]*z[8];
    z[63]=z[27] - z[31];
    z[63]=z[63]*z[39];
    z[63]=z[62] + z[63];
    z[63]=z[11]*z[63];
    z[69]=z[64] + z[22];
    z[72]=z[69]*z[39];
    z[72]= - z[62] + z[72];
    z[72]=z[10]*z[72];
    z[63]=z[72] + z[63];
    z[72]=npow(z[9],2);
    z[63]=z[72]*z[63];
    z[75]=z[14]*z[1];
    z[76]=z[75] - z[64];
    z[77]=z[11]*z[76];
    z[69]=z[10]*z[69];
    z[69]=z[69] - z[1] + z[77];
    z[77]=2*z[16];
    z[69]=z[69]*z[77];
    z[78]=z[52] + z[62];
    z[78]=z[11]*z[78];
    z[62]= - z[10]*z[62];
    z[62]=z[69] + z[78] + z[62];
    z[62]=z[62]*npow(z[16],2);
    z[69]=npow(z[9],3);
    z[78]=z[69]*z[52];
    z[79]=z[13]*z[11];
    z[80]=z[79]*z[78];
    z[62]= - z[80] + z[62] - z[78] + z[63];
    z[62]=z[12]*z[62];
    z[63]=z[27] - z[22];
    z[81]=z[63]*z[15];
    z[81]=z[81] - z[48];
    z[82]=z[37] + z[81];
    z[82]=z[82]*z[39];
    z[37]= - z[18] - z[37];
    z[37]=z[8]*z[37];
    z[37]=z[82] + z[37] + z[47] - z[49];
    z[37]=z[9]*z[37];
    z[37]=z[37] + z[1] + z[33];
    z[37]=z[10]*z[37];
    z[82]=4*z[18];
    z[83]=z[9]*z[82];
    z[83]=z[83] - z[24] - z[31];
    z[83]=z[9]*z[83];
    z[83]=z[83] - z[1] + z[33];
    z[83]=z[11]*z[83];
    z[37]=z[37] + z[83];
    z[37]=z[9]*z[37];
    z[83]=z[11]*npow(z[14],2);
    z[84]= - z[52]*z[83];
    z[85]= - z[10]*z[55];
    z[84]=z[85] + z[84] + z[22] - z[75];
    z[84]=z[84]*z[77];
    z[85]=z[52]*z[14];
    z[86]= - z[85] + z[64];
    z[87]=3*z[11];
    z[86]=z[86]*z[87];
    z[64]=z[10]*z[64];
    z[64]=z[84] + 3*z[64] - z[52] + z[86];
    z[64]=z[16]*z[64];
    z[84]= - z[52] - z[33];
    z[84]=z[11]*z[84];
    z[33]= - z[10]*z[33];
    z[33]=z[64] + z[84] + z[33];
    z[33]=z[16]*z[33];
    z[64]=3*z[1];
    z[84]=4*z[23];
    z[86]= - z[9]*z[84];
    z[86]=z[64] + z[86];
    z[86]=z[11]*z[86]*z[72];
    z[88]=z[69]*z[1];
    z[80]=z[80] + z[88] + z[86];
    z[86]=2*z[13];
    z[80]=z[80]*z[86];
    z[89]= - z[47] + z[22];
    z[89]=z[9]*z[89];
    z[89]=z[52] + z[89];
    z[72]=2*z[72];
    z[89]=z[89]*z[72];
    z[33]=z[62] + z[80] + z[33] + z[89] + z[37];
    z[33]=z[12]*z[33];
    z[37]=z[11] - z[10];
    z[37]=z[17]*z[23]*z[37];
    z[33]=z[33] + z[37];
    z[37]=n<T>(1,3)*z[36];
    z[62]=z[66]*z[37];
    z[80]= - z[7]*z[37];
    z[80]=z[48] + z[80];
    z[80]=z[6]*z[80];
    z[89]= - z[3]*z[48];
    z[62]=z[80] + z[89] + z[62];
    z[62]=z[62]*z[73];
    z[30]=z[30]*z[82];
    z[73]=4*z[36];
    z[80]=z[73]*z[3];
    z[89]=7*z[18];
    z[80]=z[80] + z[89];
    z[90]=z[80]*z[66];
    z[91]=n<T>(7,3)*z[42] + 4*z[20];
    z[91]=z[6]*z[91];
    z[30]=z[62] + z[91] + z[30] - n<T>(1,3)*z[90];
    z[30]=z[5]*z[30];
    z[62]=4*z[1];
    z[90]= - z[7]*z[47];
    z[91]=z[6]*z[84];
    z[90]=z[91] - z[62] + z[90];
    z[90]=z[6]*z[90];
    z[91]=n<T>(1,3)*z[3];
    z[80]=z[80]*z[91];
    z[92]=z[47] + z[80];
    z[92]=z[92]*z[66];
    z[93]= - z[3]*z[23];
    z[93]=z[1] + z[93];
    z[93]=z[3]*z[93];
    z[30]=z[30] + z[90] + 4*z[93] + z[92];
    z[30]=z[5]*z[30];
    z[90]=z[34] + z[47];
    z[92]=z[90]*z[15];
    z[93]=z[92] + z[18];
    z[93]=z[93]*z[35];
    z[45]=z[93] - z[45];
    z[45]=z[8]*z[45];
    z[45]=z[52] + z[45];
    z[45]=z[8]*z[45];
    z[94]=2*z[15];
    z[95]= - z[67]*z[94];
    z[96]=z[27] + z[22];
    z[97]=z[96]*z[15];
    z[98]=z[97] + z[18];
    z[99]=z[98]*z[8];
    z[100]=z[15]*z[99];
    z[95]=z[95] + z[100];
    z[100]=z[32]*z[3];
    z[95]=z[95]*z[100];
    z[45]=z[45] + z[95];
    z[45]=z[3]*z[45];
    z[90]= - z[93] + z[90];
    z[90]=z[8]*z[90];
    z[90]= - z[52] + z[90];
    z[90]=z[8]*z[90];
    z[93]= - z[23]*z[94];
    z[95]=z[38] - z[55];
    z[95]=z[15]*z[95];
    z[95]=z[58] + z[95];
    z[95]=z[8]*z[95];
    z[93]=z[95] - z[38] + z[93];
    z[93]=z[8]*z[93];
    z[93]=z[23] + z[93];
    z[93]=z[93]*z[40];
    z[90]=z[90] + z[93];
    z[90]=z[7]*z[90];
    z[93]=z[55]*z[8];
    z[93]=z[93] - z[22];
    z[93]=z[93]*z[35];
    z[93]=z[93] + z[1];
    z[93]=z[93]*z[8];
    z[95]=z[7]*z[1];
    z[95]=z[93] + z[95];
    z[95]=z[95]*z[50];
    z[30]=z[30] + z[95] + z[45] + z[90];
    z[45]=z[67]*npow(z[8],3);
    z[45]=4*z[45];
    z[90]=z[98]*z[35];
    z[90]=z[90] - z[23];
    z[95]= - z[90]*z[100];
    z[95]=z[45] + z[95];
    z[95]=z[3]*z[95];
    z[90]= - z[7]*z[90]*z[32];
    z[45]= - z[45] + z[90];
    z[45]=z[7]*z[45];
    z[45]=z[95] + z[45];
    z[45]=z[14]*z[45];
    z[30]=2*z[30] + z[45];
    z[30]=z[14]*z[30];
    z[45]= - z[7]*z[98];
    z[45]=z[67] + z[45];
    z[45]=z[10]*z[45];
    z[90]=z[7]*z[96];
    z[45]=z[45] + z[90];
    z[45]=z[54]*z[45];
    z[90]= - z[7]*z[55];
    z[85]=z[85] + z[90] - z[63];
    z[85]=z[14]*z[85];
    z[90]= - z[15]*z[67];
    z[95]= - z[23] + z[75];
    z[95]=z[95]*z[83];
    z[45]=z[95] + z[85] + z[90] + z[45];
    z[45]=z[45]*z[77];
    z[85]= - 7*z[23] - z[49];
    z[85]=z[15]*z[85];
    z[85]= - z[38] + z[85];
    z[90]=z[7]*z[15];
    z[85]=z[85]*z[90];
    z[85]=z[92] + z[85];
    z[85]=z[10]*z[85];
    z[92]=z[24] + z[34];
    z[90]=z[92]*z[90];
    z[92]= - z[7]*z[22];
    z[64]=z[64] + z[92];
    z[64]=z[14]*z[64];
    z[62]=z[83]*z[62];
    z[45]=z[45] + z[85] + z[62] + z[90] + 2*z[64];
    z[45]=z[16]*z[45];
    z[62]=3*z[22];
    z[64]=z[62] + z[84];
    z[83]=z[64]*z[15];
    z[85]= - z[18] - z[83];
    z[85]=z[7]*z[85];
    z[85]=z[85] + z[22] + z[31];
    z[85]=z[10]*z[85];
    z[75]= - z[75] + z[67];
    z[75]=z[7]*z[75];
    z[90]= - z[23]*z[35];
    z[76]=z[76]*z[87];
    z[45]=z[45] + z[85] + z[76] + z[1] + z[90] + z[75];
    z[45]=z[45]*z[77];
    z[75]=z[9]*z[67];
    z[75]= - z[1] + z[75];
    z[72]=z[75]*z[72];
    z[75]=5*z[9];
    z[76]=z[75]*z[6]*z[1];
    z[72]=z[72] - z[76];
    z[72]=z[6]*z[72];
    z[72]=z[78] + z[72];
    z[69]=z[69]*z[84];
    z[77]=8*z[23];
    z[78]=z[9]*z[77];
    z[78]= - 17*z[1] + z[78];
    z[78]=z[9]*z[78];
    z[85]=10*z[23];
    z[87]= - z[9]*z[85];
    z[87]=z[1] + z[87];
    z[87]=z[6]*z[87];
    z[78]=z[78] + z[87];
    z[78]=z[6]*z[78];
    z[78]=z[69] + z[78];
    z[78]=z[11]*z[78];
    z[79]=z[79]*z[71];
    z[79]=z[79] - 8*z[6];
    z[79]=z[88]*z[79];
    z[69]= - z[69] + z[76];
    z[69]=z[6]*z[69];
    z[69]= - 4*z[88] + z[69];
    z[69]=z[11]*z[69];
    z[69]=z[69] + z[79];
    z[69]=z[13]*z[69];
    z[69]=z[69] + 2*z[72] + z[78];
    z[69]=z[13]*z[69];
    z[72]= - z[3]*z[81];
    z[58]= - z[66]*z[58];
    z[58]=z[58] + z[72] - z[63];
    z[58]=z[9]*z[58];
    z[63]=5*z[22];
    z[72]= - z[84] + z[63];
    z[72]=z[3]*z[72];
    z[76]=z[66]*z[82];
    z[58]=z[58] + z[76] + 6*z[1] + z[72];
    z[58]=z[58]*z[39];
    z[72]= - z[66]*z[84];
    z[76]=z[3]*z[1];
    z[58]=z[58] + 5*z[76] + z[72];
    z[58]=z[9]*z[58];
    z[21]= - z[9]*z[21];
    z[72]=z[7]*z[48];
    z[21]=z[21] - z[47] + z[72];
    z[21]=z[21]*z[43];
    z[72]= - z[7]*z[84];
    z[21]=z[21] + 9*z[1] + z[72];
    z[21]=z[9]*z[21];
    z[72]= - z[9]*z[48];
    z[72]=9*z[23] + z[72];
    z[72]=z[9]*z[72];
    z[72]= - z[52] + z[72];
    z[76]=z[18]*z[75];
    z[76]=z[77] + z[76];
    z[76]=z[6]*z[76];
    z[72]=2*z[72] + z[76];
    z[72]=z[6]*z[72];
    z[21]=z[21] + z[72];
    z[21]=z[11]*z[21];
    z[72]=z[22] - z[23];
    z[43]=z[72]*z[43];
    z[76]=11*z[1];
    z[43]=z[76] + z[43];
    z[43]=z[9]*z[43];
    z[75]=z[96]*z[75];
    z[75]= - z[52] + z[75];
    z[75]=z[6]*z[75];
    z[43]=z[43] + z[75];
    z[43]=z[6]*z[43];
    z[75]= - z[18] + z[56];
    z[75]=z[75]*z[46];
    z[75]= - z[23] + z[75];
    z[75]=z[75]*z[91];
    z[75]=z[1] + z[75];
    z[46]=z[2]*z[75]*z[46];
    z[21]=z[69] + z[46] + z[21] + z[58] + z[43];
    z[21]=z[21]*z[86];
    z[34]=z[34] - z[23];
    z[43]=z[82] + z[51];
    z[43]=z[8]*z[43];
    z[46]=z[49] - z[23];
    z[49]=z[15]*z[46]*z[53];
    z[53]=z[72]*z[57];
    z[51]= - z[51] + z[53];
    z[51]=z[51]*z[39];
    z[43]=z[51] + z[49] + 5*z[34] + z[43];
    z[43]=z[9]*z[43];
    z[31]= - z[23] - z[31];
    z[31]=z[31]*z[35];
    z[49]=z[3]*z[22];
    z[31]=z[43] + 15*z[49] + z[76] + z[31];
    z[31]=z[31]*z[39];
    z[43]=z[35]*z[54];
    z[49]= - z[47] - z[22];
    z[49]=z[15]*z[49];
    z[49]= - z[48] + z[49];
    z[49]=z[49]*z[43];
    z[51]=9*z[22];
    z[53]=13*z[23] + z[51];
    z[53]=z[15]*z[53];
    z[53]=z[48] + z[53];
    z[53]=z[15]*z[53];
    z[49]=z[53] + z[49];
    z[49]=z[8]*z[49];
    z[41]= - z[41] - z[60];
    z[41]=z[15]*z[41];
    z[41]=z[49] - n<T>(8,3)*z[18] + z[41];
    z[41]=z[8]*z[41];
    z[41]=8*z[22] + z[41];
    z[49]=z[68] - z[99];
    z[49]=z[49]*z[35];
    z[53]=npow(z[15],3)*z[49];
    z[57]=z[62] + z[27];
    z[58]= - z[15]*z[57];
    z[58]= - z[18] + z[58];
    z[58]=z[15]*z[58];
    z[58]=z[58] + z[53];
    z[58]=z[8]*z[58];
    z[58]=z[55] + z[58];
    z[58]=z[7]*z[58];
    z[41]=2*z[41] + z[58];
    z[41]=z[7]*z[41];
    z[58]=7*z[22];
    z[60]= - z[27] - z[58];
    z[60]=z[15]*z[60];
    z[68]=z[54]*z[8];
    z[64]=z[64]*z[68];
    z[60]=z[60] + z[64];
    z[60]=z[8]*z[60];
    z[60]=2*z[72] + z[60];
    z[60]=z[8]*z[60];
    z[60]=8*z[1] + z[60];
    z[39]=z[46]*z[39];
    z[46]= - n<T>(22,3)*z[23] + z[22];
    z[46]=z[46]*z[50];
    z[39]=z[46] + z[39] + 2*z[60] + z[41];
    z[39]=z[6]*z[39];
    z[41]=z[94]*z[18];
    z[41]=z[41] - z[37];
    z[46]=z[3]*z[41];
    z[50]=n<T>(1,3)*z[25];
    z[60]=z[66]*z[50];
    z[64]= - z[7]*z[50];
    z[64]=z[64] - z[41];
    z[64]=z[6]*z[64];
    z[46]=z[64] + z[46] + z[60];
    z[46]=z[46]*z[29];
    z[60]=16*z[56];
    z[64]=z[60] + n<T>(17,3)*z[18];
    z[37]=z[61] - z[37];
    z[61]=8*z[3];
    z[69]= - z[37]*z[61];
    z[69]=z[69] - z[64];
    z[69]=z[3]*z[69];
    z[72]=z[7]*z[73];
    z[64]= - 8*z[74] + z[72] + z[64];
    z[64]=z[6]*z[64];
    z[70]=z[70]*z[25];
    z[70]=z[70] + z[36];
    z[66]=4*z[66];
    z[72]= - z[70]*z[66];
    z[46]=z[46] + z[64] + z[69] + z[72];
    z[46]=z[5]*z[46];
    z[64]=z[60] + z[89];
    z[69]=n<T>(8,3)*z[3];
    z[72]= - z[36]*z[69];
    z[72]=z[72] + z[64];
    z[72]=z[3]*z[72];
    z[72]=z[84] + z[72];
    z[72]=z[3]*z[72];
    z[70]=z[3]*z[70];
    z[70]=n<T>(1,3)*z[18] + z[70];
    z[66]=z[70]*z[66];
    z[42]=z[42] + z[20];
    z[42]= - z[23] - n<T>(1,3)*z[42];
    z[42]=z[42]*z[71];
    z[42]=z[46] + z[42] + z[72] + z[66];
    z[42]=z[5]*z[42];
    z[46]=z[60] + n<T>(35,3)*z[18];
    z[46]=z[46]*z[15];
    z[60]=n<T>(4,3)*z[36];
    z[46]=z[46] + z[60];
    z[37]=z[15]*z[37];
    z[37]=z[50] + z[37];
    z[37]=z[37]*z[61];
    z[37]=z[37] + z[46];
    z[37]=z[3]*z[37];
    z[20]=z[54]*z[20];
    z[20]=8*z[20] - z[46];
    z[20]=z[6]*z[20];
    z[41]=z[41]*z[15];
    z[41]=z[41] + z[50];
    z[29]=z[29]*z[41]*z[65];
    z[20]=z[29] + z[37] + z[20];
    z[20]=z[5]*z[20];
    z[29]= - z[15]*z[64];
    z[37]=z[15]*z[36];
    z[37]= - z[25] + z[37];
    z[37]=z[37]*z[69];
    z[29]=z[37] - z[60] + z[29];
    z[29]=z[3]*z[29];
    z[37]=11*z[56] + 20*z[18];
    z[29]= - n<T>(2,3)*z[37] + z[29];
    z[29]=z[3]*z[29];
    z[37]= - 7*z[74] + z[37];
    z[37]=z[6]*z[37];
    z[20]=z[20] + z[29] + n<T>(2,3)*z[37];
    z[20]=z[5]*z[20];
    z[29]=z[51] + 14*z[23];
    z[29]=z[29]*z[15];
    z[29]=z[29] + z[89];
    z[37]=z[15]*z[29];
    z[37]=z[37] - z[53];
    z[37]=z[8]*z[37];
    z[22]=n<T>(4,3)*z[23] + z[22];
    z[22]=z[15]*z[22];
    z[41]= - z[15]*z[59];
    z[41]=z[36] + z[41];
    z[41]=z[41]*z[69];
    z[22]=z[41] + z[37] - n<T>(4,3)*z[18] + z[22];
    z[22]=z[3]*z[22];
    z[24]=z[24] + z[62];
    z[24]=z[15]*z[24];
    z[24]=z[48] + z[24];
    z[24]=z[24]*z[43];
    z[37]=z[58] + z[47];
    z[37]=z[37]*z[15];
    z[41]=z[48] - z[37];
    z[41]=z[15]*z[41];
    z[24]=z[41] + z[24];
    z[24]=z[8]*z[24];
    z[41]=z[85] + z[51];
    z[41]=z[15]*z[41];
    z[24]=z[24] + n<T>(20,3)*z[18] + z[41];
    z[24]=z[8]*z[24];
    z[41]=2*z[57];
    z[24]= - z[41] + z[24];
    z[22]=2*z[24] + z[22];
    z[22]=z[3]*z[22];
    z[24]= - z[27] - z[63];
    z[24]=z[15]*z[24];
    z[43]=z[77] + z[63];
    z[46]=z[43]*z[68];
    z[24]=z[24] + z[46];
    z[24]=z[8]*z[24];
    z[24]=z[41] + z[24];
    z[24]=z[8]*z[24];
    z[24]= - z[52] + z[24];
    z[41]=z[14]*z[93];
    z[46]=z[6]*z[23];
    z[20]= - 4*z[41] + z[20] - n<T>(10,3)*z[46] + 2*z[24] + z[22];
    z[20]=z[2]*z[20];
    z[22]= - z[15]*z[43];
    z[22]= - z[38] + z[22];
    z[22]=z[8]*z[22];
    z[22]=z[22] + z[23] + z[63];
    z[22]=z[15]*z[22];
    z[22]= - z[48] + z[22];
    z[22]=z[8]*z[22];
    z[22]=z[22] - z[34];
    z[22]=z[8]*z[22];
    z[22]=7*z[1] + z[22];
    z[24]=z[54]*z[49];
    z[24]=z[24] - z[29];
    z[24]=z[8]*z[24];
    z[29]=z[59]*z[69];
    z[24]=z[29] - z[23] + z[24];
    z[24]=z[3]*z[24];
    z[22]=2*z[22] + z[24];
    z[22]=z[3]*z[22];
    z[24]=z[38] - z[83];
    z[24]=z[15]*z[24];
    z[24]=z[73] + z[24];
    z[24]=z[8]*z[24];
    z[24]=z[24] - z[89] + z[37];
    z[24]=z[8]*z[24];
    z[23]=z[23] - z[62];
    z[23]=2*z[23] + z[24];
    z[23]=z[8]*z[23];
    z[24]= - z[27] - z[80];
    z[24]=z[3]*z[24];
    z[23]=z[24] - z[52] + z[23];
    z[18]=z[18] + z[55];
    z[18]=z[18]*z[94];
    z[24]= - z[67]*z[54];
    z[24]= - z[36] + z[24];
    z[24]=z[15]*z[24];
    z[24]= - z[25] + z[24];
    z[24]=z[24]*z[35];
    z[18]=z[24] + 5*z[36] + z[18];
    z[18]=z[8]*z[18];
    z[18]=z[18] - z[44] - z[97];
    z[18]=z[18]*z[40];
    z[18]=2*z[23] + z[18];
    z[18]=z[7]*z[18];
    z[23]=z[32]*z[84];

    r += z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[26] + z[28]
       + z[30] + z[31] + 2*z[33] + z[39] + z[42] + z[45];
 
    return r;
}

template double qqb_2lNLC_r1205(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1205(const std::array<dd_real,31>&);
#endif
