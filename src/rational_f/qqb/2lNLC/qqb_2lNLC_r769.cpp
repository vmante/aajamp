#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r769(const std::array<T,31>& k) {
  T z[103];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[2];
    z[12]=k[9];
    z[13]=k[15];
    z[14]=k[8];
    z[15]=k[6];
    z[16]=npow(z[1],2);
    z[17]=n<T>(19,4)*z[16];
    z[18]=npow(z[1],3);
    z[19]=z[18]*z[5];
    z[20]= - z[17] + 12*z[19];
    z[20]=z[5]*z[20];
    z[21]=z[18]*z[7];
    z[17]= - z[17] + 12*z[21];
    z[17]=z[7]*z[17];
    z[22]=n<T>(5,4)*z[6];
    z[23]= - z[19]*z[22];
    z[17]=z[23] + z[20] + z[17];
    z[17]=z[6]*z[17];
    z[20]=z[16] - z[19];
    z[23]=10*z[7];
    z[20]=z[20]*z[23];
    z[24]= - z[21]*z[22];
    z[25]=n<T>(11,2)*z[1];
    z[26]=z[4]*z[19];
    z[20]=n<T>(5,2)*z[26] + z[24] - z[25] + z[20];
    z[20]=z[3]*z[20];
    z[24]=2*z[5];
    z[26]= - z[18]*z[24];
    z[27]=n<T>(11,2)*z[16];
    z[26]= - z[27] + z[26];
    z[26]=z[5]*z[26];
    z[28]=3*z[19];
    z[29]=13*z[16] + z[28];
    z[30]=n<T>(1,2)*z[7];
    z[29]=z[29]*z[30];
    z[31]=7*z[1];
    z[26]=z[29] - z[31] + z[26];
    z[26]=z[7]*z[26];
    z[29]=z[16]*z[5];
    z[32]=z[1] - z[29];
    z[32]=z[32]*z[24];
    z[33]=npow(z[1],4);
    z[34]= - z[33]*z[24];
    z[35]=5*z[18];
    z[34]=z[35] + z[34];
    z[34]=z[5]*z[34];
    z[36]=n<T>(15,4)*z[16];
    z[34]= - z[36] + z[34];
    z[34]=z[5]*z[34];
    z[37]=z[16]*z[4];
    z[38]=4*z[37];
    z[34]=z[34] - z[38];
    z[34]=z[4]*z[34];
    z[17]=z[20] + z[34] + z[17] + z[32] + z[26];
    z[17]=z[3]*z[17];
    z[20]=z[10]*z[1];
    z[26]=7*z[20];
    z[32]=z[26] + 12*z[16];
    z[34]=2*z[18];
    z[39]=z[16]*z[10];
    z[40]= - z[34] - z[39];
    z[40]=z[40]*z[24];
    z[40]=z[40] + z[32];
    z[40]=z[5]*z[40];
    z[41]=5*z[1];
    z[40]=z[41] + z[40];
    z[40]=z[5]*z[40];
    z[42]=z[39] - z[18];
    z[43]=2*z[7];
    z[44]=z[42]*z[43];
    z[45]=5*z[16];
    z[44]=z[44] + z[45] - z[26];
    z[44]=z[7]*z[44];
    z[46]=4*z[1];
    z[44]=z[46] + z[44];
    z[44]=z[7]*z[44];
    z[47]=z[43]*z[33];
    z[35]=z[35] - z[47];
    z[35]=z[7]*z[35];
    z[35]= - z[36] + z[35];
    z[35]=z[7]*z[35];
    z[36]=z[6]*z[21];
    z[35]=n<T>(5,2)*z[36] - z[46] + z[35];
    z[35]=z[6]*z[35];
    z[36]=z[5] - z[7];
    z[36]=npow(z[15],2)*z[39]*z[36];
    z[48]=z[4]*z[1];
    z[35]=z[36] - 8*z[48] + z[35] + z[40] + z[44];
    z[35]=z[4]*z[35];
    z[36]=npow(z[5],2);
    z[40]=npow(z[7],2);
    z[44]=z[36] + z[40];
    z[44]=z[16]*z[44];
    z[49]=z[18]*z[6];
    z[50]=z[40]*z[49];
    z[44]=2*z[44] + z[50];
    z[50]=3*z[6];
    z[44]=z[44]*z[50];
    z[50]=z[18]*z[4];
    z[51]= - n<T>(5,2)*z[50] + 3*z[49];
    z[51]=z[36]*z[51];
    z[52]= - z[16] + n<T>(1,2)*z[19];
    z[52]=z[5]*z[52];
    z[53]=n<T>(1,2)*z[1];
    z[52]=z[53] + z[52];
    z[54]=5*z[7];
    z[52]=z[52]*z[54];
    z[51]=z[52] + z[51];
    z[51]=z[3]*z[51];
    z[52]= - z[4]*z[36]*z[45];
    z[44]=z[51] + z[44] + z[52];
    z[44]=z[3]*z[44];
    z[51]=z[40]*z[1];
    z[52]= - z[1]*z[36];
    z[52]=z[52] - z[51];
    z[55]= - z[16] - n<T>(1,2)*z[49];
    z[55]=z[6]*z[40]*z[55];
    z[52]=n<T>(1,2)*z[52] + z[55];
    z[52]=z[4]*z[52];
    z[55]=3*z[1];
    z[56]=z[6]*z[36]*z[55];
    z[57]=2*z[16];
    z[58]=z[57] - z[19];
    z[59]=npow(z[3],2);
    z[58]=z[2]*z[5]*z[58]*z[59];
    z[44]=3*z[58] + z[44] + z[56] + 5*z[52];
    z[44]=z[9]*z[44];
    z[52]=5*z[20];
    z[56]=11*z[16];
    z[58]=z[52] + z[56];
    z[60]=z[58]*z[24];
    z[60]= - z[55] + z[60];
    z[60]=z[5]*z[60];
    z[61]= - z[40]*z[57];
    z[62]=n<T>(1,2)*z[21];
    z[63]=z[16] - z[62];
    z[63]=z[7]*z[63];
    z[29]= - n<T>(5,4)*z[29] + z[63];
    z[29]=z[6]*z[29];
    z[29]=z[29] + z[60] + z[61];
    z[29]=z[6]*z[29];
    z[60]=z[33]*z[5];
    z[61]= - z[34] + z[60];
    z[61]=z[61]*z[24];
    z[63]=n<T>(1,2)*z[16];
    z[61]=z[63] + z[61];
    z[61]=z[5]*z[61];
    z[64]=6*z[1];
    z[65]=z[16]*z[7];
    z[66]=z[3]*z[19];
    z[61]= - 6*z[66] - z[65] + z[64] + z[61];
    z[61]=z[3]*z[61];
    z[66]=z[2]*z[3];
    z[67]= - z[33]*z[36]*z[66];
    z[61]=z[61] + z[67];
    z[61]=z[2]*z[61];
    z[67]=z[36]*z[53];
    z[68]=2*z[51];
    z[17]=z[44] + z[61] + z[17] + z[29] + z[67] + z[68] + z[35];
    z[17]=z[9]*z[17];
    z[29]=z[20] + z[16];
    z[35]=7*z[29];
    z[44]=z[57] + z[20];
    z[61]=z[44]*z[10];
    z[61]=z[61] + z[18];
    z[67]=3*z[61];
    z[69]=z[67]*z[6];
    z[70]= - z[35] - z[69];
    z[70]=z[6]*z[70];
    z[71]=z[7]*z[1];
    z[72]=npow(z[10],2);
    z[73]=z[71]*z[72];
    z[74]= - z[20] + z[73];
    z[74]=z[7]*z[74];
    z[35]=z[4]*z[35];
    z[75]=n<T>(5,2)*z[20];
    z[76]=z[2]*z[75];
    z[35]=z[76] + z[35] + n<T>(5,2)*z[74] + z[70];
    z[35]=z[9]*z[35];
    z[70]=z[29]*z[10];
    z[74]=z[29]*z[72];
    z[76]=z[54]*z[74];
    z[76]= - 9*z[70] + z[76];
    z[76]=z[76]*z[30];
    z[77]=9*z[16];
    z[78]=11*z[20];
    z[79]=z[77] - z[78];
    z[80]=n<T>(1,2)*z[10];
    z[79]=z[79]*z[80];
    z[81]=z[10]*z[69];
    z[82]=10*z[18];
    z[81]=z[81] - z[82] - z[79];
    z[81]=z[6]*z[81];
    z[83]=z[16] + n<T>(1,2)*z[20];
    z[83]=z[83]*z[10];
    z[83]=z[83] + n<T>(1,2)*z[18];
    z[84]= - z[83]*z[72]*z[54];
    z[85]=n<T>(13,2)*z[16];
    z[86]=2*z[20];
    z[87]=z[85] + z[86];
    z[87]=z[10]*z[87];
    z[87]=n<T>(9,2)*z[18] + z[87];
    z[87]=z[10]*z[87];
    z[84]=z[87] + z[84];
    z[84]=z[7]*z[84];
    z[70]=n<T>(9,2)*z[70];
    z[84]=z[70] + z[84];
    z[84]=z[4]*z[84];
    z[87]=3*z[16];
    z[88]= - z[87] - 17*z[20];
    z[88]=z[88]*z[80];
    z[89]=7*z[18];
    z[88]=z[89] + z[88];
    z[88]=z[10]*z[88];
    z[69]= - z[72]*z[69];
    z[69]=z[88] + z[69];
    z[69]=z[6]*z[69];
    z[69]=z[79] + z[69];
    z[69]=z[2]*z[69];
    z[35]=z[35] + z[69] + z[84] + z[76] + z[81];
    z[35]=z[14]*z[35];
    z[69]=n<T>(11,2)*z[20];
    z[73]= - n<T>(15,2)*z[73] - z[77] + z[69];
    z[73]=z[7]*z[73];
    z[76]=z[39] + z[18];
    z[79]=6*z[6];
    z[81]=z[76]*z[79];
    z[81]=z[81] + 10*z[16] - z[78];
    z[81]=z[6]*z[81];
    z[84]=z[63] + z[86];
    z[84]=z[10]*z[84];
    z[84]=z[18] + z[84];
    z[23]=z[74]*z[23];
    z[23]=3*z[84] + z[23];
    z[23]=z[7]*z[23];
    z[84]=3*z[20];
    z[23]=z[23] + z[45] + z[84];
    z[23]=z[4]*z[23];
    z[88]= - z[87] + z[75];
    z[88]=z[10]*z[88];
    z[90]=z[76]*z[10];
    z[79]= - z[90]*z[79];
    z[79]=z[79] + 13*z[18] + z[88];
    z[79]=z[6]*z[79];
    z[77]=z[79] + z[77] + z[75];
    z[77]=z[2]*z[77];
    z[79]= - z[4] + z[6];
    z[79]=z[55]*z[79];
    z[88]= - z[7]*z[20];
    z[88]=z[53] + z[88];
    z[54]=z[88]*z[54];
    z[88]=n<T>(5,2)*z[1];
    z[91]= - z[2]*z[88];
    z[54]=z[91] + z[54] + z[79];
    z[54]=z[9]*z[54];
    z[23]=z[35] + z[54] + z[77] + z[23] + z[73] + z[81];
    z[23]=z[14]*z[23];
    z[35]=z[18]*z[10];
    z[54]= - z[35] + 3*z[33];
    z[73]=z[33]*z[10];
    z[77]=npow(z[1],5);
    z[79]= - 2*z[77] - z[73];
    z[79]=z[79]*z[24];
    z[79]= - n<T>(1,2)*z[54] + z[79];
    z[79]=z[5]*z[79];
    z[81]= - z[18] - n<T>(3,2)*z[39];
    z[79]=3*z[81] + z[79];
    z[79]=z[5]*z[79];
    z[81]=z[73] - z[77];
    z[81]=z[81]*z[43];
    z[91]=n<T>(1,2)*z[35];
    z[92]=2*z[33];
    z[93]=z[81] - z[92] - z[91];
    z[93]=z[7]*z[93];
    z[94]=9*z[39];
    z[89]=z[89] + z[94];
    z[89]=n<T>(1,2)*z[89] + z[93];
    z[89]=z[7]*z[89];
    z[93]=z[35] + z[33];
    z[93]=z[93]*z[5];
    z[95]= - z[10]*z[21];
    z[95]=z[95] - z[34] + z[93];
    z[96]=2*z[4];
    z[95]=z[95]*z[96];
    z[97]=npow(z[1],6);
    z[98]= - z[7]*z[97];
    z[98]= - z[77] + z[98];
    z[98]=z[98]*z[43];
    z[99]=n<T>(41,4)*z[33];
    z[98]= - z[99] + z[98];
    z[98]=z[7]*z[98];
    z[100]=n<T>(11,2)*z[18];
    z[98]=z[100] + z[98];
    z[98]=z[6]*z[98];
    z[79]=z[95] + z[98] + z[89] - z[56] + z[79];
    z[79]=z[4]*z[79];
    z[89]=z[73] + z[77];
    z[95]=z[89]*z[24];
    z[54]=z[95] - z[54];
    z[54]=z[5]*z[54];
    z[95]=z[92] + z[35];
    z[98]= - z[43]*z[73];
    z[98]=z[98] - z[95];
    z[98]=z[7]*z[98];
    z[89]=z[5]*z[89];
    z[101]= - z[7]*z[73];
    z[89]=z[101] - z[92] + z[89];
    z[89]=z[89]*z[96];
    z[96]= - z[6]*z[92];
    z[54]=z[89] + z[96] + z[98] - z[34] + z[54];
    z[54]=z[4]*z[54];
    z[89]= - z[18] - 3*z[60];
    z[89]=z[5]*z[89];
    z[73]=z[40]*z[73];
    z[73]=z[34] + z[73];
    z[73]=z[6]*z[73];
    z[54]=z[54] + z[73] + z[57] + z[89];
    z[54]=z[2]*z[54];
    z[73]= - z[81] + z[33] - 2*z[35];
    z[73]=z[7]*z[73];
    z[81]= - 15*z[18] + 23*z[39];
    z[73]=n<T>(1,2)*z[81] + z[73];
    z[73]=z[7]*z[73];
    z[81]= - z[72]*z[21];
    z[81]=2*z[90] + z[81];
    z[89]=3*z[7];
    z[81]=z[81]*z[89];
    z[81]= - n<T>(5,4)*z[39] + z[81];
    z[81]=z[6]*z[81];
    z[96]=6*z[16];
    z[73]=z[81] + z[73] - z[96] - z[26];
    z[73]=z[6]*z[73];
    z[81]= - z[5]*z[97];
    z[81]= - z[77] + z[81];
    z[81]=z[81]*z[24];
    z[81]= - z[99] + z[81];
    z[81]=z[5]*z[81];
    z[97]=n<T>(27,2)*z[18];
    z[92]= - z[4]*z[92];
    z[81]=z[92] + z[97] + z[81];
    z[81]=z[4]*z[81];
    z[92]=z[77]*z[5];
    z[92]=z[92] + z[33];
    z[92]=z[92]*z[24];
    z[97]= - z[97] + z[92];
    z[97]=z[5]*z[97];
    z[98]=n<T>(37,4)*z[16];
    z[81]=z[81] - z[21] - z[98] + z[97];
    z[81]=z[3]*z[81];
    z[60]=n<T>(3,2)*z[18] + 4*z[60];
    z[60]=z[5]*z[60];
    z[60]= - n<T>(57,2)*z[16] + z[60];
    z[60]=z[5]*z[60];
    z[97]= - n<T>(19,2)*z[16] + z[21];
    z[89]=z[97]*z[89];
    z[54]=z[54] + z[81] + z[79] + z[73] + z[89] - z[55] + z[60];
    z[54]=z[2]*z[54];
    z[37]=z[37] - z[1];
    z[60]=4*z[4];
    z[37]=z[37]*z[60];
    z[60]= - 31*z[1] - 7*z[65];
    z[60]=z[60]*z[30];
    z[73]= - z[41] + n<T>(1,2)*z[65];
    z[73]=z[3]*z[73];
    z[79]=z[71]*z[3];
    z[81]= - z[51] + 5*z[79];
    z[81]=z[9]*z[81];
    z[60]=2*z[81] + n<T>(5,2)*z[73] + z[60] - z[37];
    z[60]=z[3]*z[60];
    z[73]=z[33]*z[4];
    z[81]=z[18] - z[73];
    z[81]=z[3]*z[81];
    z[73]=z[6]*z[73];
    z[73]=z[81] - z[49] + z[73];
    z[73]=z[2]*z[73];
    z[81]= - z[56] - n<T>(5,2)*z[49];
    z[81]=z[6]*z[81];
    z[49]=z[4]*z[49];
    z[49]=2*z[73] + n<T>(1,2)*z[81] + 6*z[49];
    z[49]=z[4]*z[49];
    z[73]=z[27] - 6*z[50];
    z[73]=z[4]*z[73];
    z[81]= - z[16] + z[50];
    z[89]=n<T>(5,4)*z[3];
    z[81]=z[81]*z[89];
    z[73]=z[81] - z[1] + z[73];
    z[73]=z[3]*z[73];
    z[22]=z[16]*z[22];
    z[22]=z[1] + z[22];
    z[22]=z[6]*z[22];
    z[22]=z[73] + z[22] + z[49];
    z[22]=z[2]*z[22];
    z[49]=n<T>(5,2)*z[16];
    z[73]=3*z[21];
    z[81]=z[18]*z[3];
    z[97]=n<T>(5,2)*z[81];
    z[99]= - z[97] + z[49] - z[73];
    z[99]=z[3]*z[99];
    z[78]=z[45] + z[78];
    z[30]=z[78]*z[30];
    z[78]= - z[6]*z[72]*z[53];
    z[78]= - z[20] + z[78];
    z[101]=5*z[6];
    z[78]=z[78]*z[101];
    z[30]=z[99] + z[78] + z[55] + z[30];
    z[30]=z[12]*z[30];
    z[78]= - z[85] - 6*z[21];
    z[78]=z[7]*z[78];
    z[85]=z[16]*z[3];
    z[78]=10*z[85] - z[55] + z[78];
    z[78]=z[3]*z[78];
    z[99]=6*z[65];
    z[102]=z[88] - z[99];
    z[102]=z[7]*z[102];
    z[30]=z[30] + z[102] + z[78];
    z[30]=z[12]*z[30];
    z[73]=n<T>(17,2)*z[16] + z[73];
    z[73]=z[7]*z[73];
    z[62]=z[16] + z[62];
    z[78]=5*z[3];
    z[62]=z[62]*z[78];
    z[62]=z[73] + z[62];
    z[62]=z[3]*z[62];
    z[44]=3*z[44];
    z[73]=z[7]*z[44];
    z[73]= - z[88] + z[73];
    z[73]=z[7]*z[73];
    z[62]=z[73] + z[62];
    z[62]=z[12]*z[62];
    z[31]= - z[31] + z[99];
    z[31]=z[7]*z[31];
    z[73]= - z[1] - z[65];
    z[73]=z[3]*z[73];
    z[31]=z[31] + 10*z[73];
    z[31]=z[3]*z[31];
    z[31]=z[31] + z[62];
    z[31]=z[12]*z[31];
    z[62]= - z[25] - z[99];
    z[62]=z[7]*z[62];
    z[65]= - z[53] - z[65];
    z[65]=z[65]*z[78];
    z[62]=z[62] + z[65];
    z[62]=z[3]*z[62];
    z[51]= - 6*z[51] + z[62];
    z[51]=z[12]*z[51];
    z[62]=z[59]*z[71];
    z[51]=10*z[62] + z[51];
    z[51]=z[12]*z[51];
    z[40]=z[40]*z[55];
    z[40]=z[40] + n<T>(5,2)*z[79];
    z[62]=z[8]*npow(z[12],2);
    z[40]=z[3]*z[40]*z[62];
    z[40]=z[51] + z[40];
    z[40]=z[8]*z[40];
    z[51]= - z[68] + 15*z[79];
    z[51]=z[3]*z[51];
    z[31]=z[40] + z[51] + z[31];
    z[31]=z[8]*z[31];
    z[40]=z[16]*z[43];
    z[40]=z[88] + z[40];
    z[40]=z[7]*z[40];
    z[40]= - static_cast<T>(1)+ z[40];
    z[40]=z[7]*z[40];
    z[37]=z[6]*z[37];
    z[22]=z[31] + z[30] + z[22] + z[40] + z[37] + z[60];
    z[22]=z[8]*z[22];
    z[30]= - z[9]*z[2]*z[55];
    z[30]=z[30] + z[64];
    z[30]=z[59]*z[30];
    z[31]=6*z[85];
    z[37]= - n<T>(7,2)*z[1] + z[31];
    z[37]=z[37]*z[66];
    z[30]=z[37] + z[30];
    z[30]=z[9]*z[30];
    z[37]=z[46] - 3*z[85];
    z[37]=z[3]*z[37];
    z[40]=z[27] - z[81];
    z[46]=3*z[3];
    z[40]=z[40]*z[46];
    z[40]= - z[53] + z[40];
    z[40]=z[2]*z[40];
    z[30]=z[30] + 2*z[37] + z[40];
    z[30]=z[9]*z[30];
    z[37]=n<T>(21,2)*z[16] - 13*z[81];
    z[37]=z[2]*z[37];
    z[40]= - z[4]*z[26];
    z[30]=z[30] + z[37] - n<T>(35,2)*z[85] + n<T>(19,2)*z[1] + z[40];
    z[30]=z[9]*z[30];
    z[37]=9*z[20];
    z[40]= - z[45] + z[37];
    z[40]=z[10]*z[40];
    z[51]=z[72]*z[5];
    z[53]=5*z[51];
    z[29]=z[29]*z[53];
    z[29]=z[40] + z[29];
    z[40]=n<T>(1,2)*z[5];
    z[29]=z[29]*z[40];
    z[60]=z[63] - z[86];
    z[60]=z[10]*z[60];
    z[63]=n<T>(5,2)*z[18];
    z[60]=z[63] + z[60];
    z[60]=z[10]*z[60];
    z[53]= - z[83]*z[53];
    z[53]=z[60] + z[53];
    z[53]=z[5]*z[53];
    z[53]=z[70] + z[53];
    z[53]=z[4]*z[53];
    z[60]= - z[2]*z[82];
    z[26]=z[30] + z[60] + z[53] - z[26] + z[29];
    z[26]=z[13]*z[26];
    z[29]=z[42]*z[6];
    z[30]= - n<T>(9,2)*z[16] - z[20];
    z[53]=z[57] + n<T>(3,2)*z[20];
    z[60]=5*z[5];
    z[53]=z[10]*z[53]*z[60];
    z[30]= - z[29] + 3*z[30] + z[53];
    z[30]=z[5]*z[30];
    z[42]=z[5]*z[42];
    z[29]=19*z[81] + z[29] + z[96] + z[42];
    z[29]=z[2]*z[29];
    z[42]=z[5]*z[10];
    z[53]=z[61]*z[42];
    z[61]=n<T>(7,2)*z[16];
    z[64]=z[61] + z[86];
    z[64]=z[10]*z[64];
    z[63]=z[63] + z[64];
    z[63]=3*z[63] - 10*z[53];
    z[63]=z[5]*z[63];
    z[63]=z[63] + z[57] - z[84];
    z[63]=z[4]*z[63];
    z[64]=z[3]*z[57];
    z[64]=z[55] + z[64];
    z[64]=z[3]*z[64];
    z[65]=z[3]*z[34];
    z[65]= - z[87] + z[65];
    z[65]=z[65]*z[66];
    z[48]=z[65] - z[48] + z[64];
    z[31]=z[88] - z[31];
    z[31]=z[9]*z[31]*z[66];
    z[31]=3*z[48] + z[31];
    z[31]=z[9]*z[31];
    z[26]=z[26] + z[31] + z[29] + 13*z[85] + z[63] + z[41] + z[30];
    z[26]=z[13]*z[26];
    z[29]= - 19*z[16] - 13*z[20];
    z[29]=z[29]*z[80];
    z[30]=3*z[18];
    z[29]= - z[30] + z[29];
    z[29]=z[10]*z[29];
    z[31]=z[51]*z[67];
    z[29]=z[29] + z[31];
    z[29]=z[5]*z[29];
    z[31]=z[83]*z[51];
    z[31]= - n<T>(1,2)*z[74] + z[31];
    z[31]=z[31]*z[101];
    z[48]=z[87] + n<T>(7,2)*z[20];
    z[48]=z[10]*z[48];
    z[29]=z[31] + z[48] + z[29];
    z[29]=z[6]*z[29];
    z[31]=n<T>(29,2)*z[16];
    z[37]= - z[31] - z[37];
    z[37]=z[10]*z[37];
    z[48]=z[51]*z[44];
    z[37]=z[37] + z[48];
    z[37]=z[5]*z[37];
    z[48]=6*z[20];
    z[27]= - z[97] + z[29] + z[37] + z[27] + z[48];
    z[27]=z[12]*z[27];
    z[29]= - n<T>(3,2)*z[16] - z[20];
    z[37]=z[42]*z[44];
    z[29]=5*z[29] + z[37];
    z[29]=z[5]*z[29];
    z[37]=2*z[1];
    z[29]=z[37] + z[29];
    z[44]= - 49*z[16] - 53*z[20];
    z[44]=z[44]*z[80];
    z[30]=12*z[53] - z[30] + z[44];
    z[30]=z[5]*z[30];
    z[42]=z[76]*z[42];
    z[42]= - z[39] + z[42];
    z[42]=z[42]*z[101];
    z[30]=z[42] + 20*z[20] + z[30];
    z[30]=z[6]*z[30];
    z[42]= - z[57] - z[69];
    z[42]=z[7]*z[42];
    z[44]=z[43]*z[18];
    z[49]=z[49] + z[44];
    z[46]=z[49]*z[46];
    z[27]=z[27] + z[46] + z[30] + 3*z[29] + z[42];
    z[27]=z[12]*z[27];
    z[29]=z[59]*z[41];
    z[30]=5*z[85];
    z[42]=z[25] - z[30];
    z[42]=z[12]*z[3]*z[42];
    z[29]=z[29] + z[42];
    z[29]=z[12]*z[29];
    z[42]=z[59]*z[88]*z[62];
    z[29]=z[29] + z[42];
    z[29]=z[8]*z[29];
    z[42]=z[55]*z[51];
    z[42]= - n<T>(23,2)*z[20] + z[42];
    z[42]=z[5]*z[42];
    z[46]= - z[56] + 5*z[81];
    z[46]=z[3]*z[46];
    z[49]=n<T>(17,2)*z[1];
    z[42]=n<T>(1,2)*z[46] + z[49] + z[42];
    z[42]=z[12]*z[42];
    z[46]=z[5]*z[48];
    z[46]= - 17*z[1] + z[46];
    z[46]=z[5]*z[46];
    z[30]=11*z[1] - z[30];
    z[30]=z[3]*z[30];
    z[30]=z[42] + z[46] + z[30];
    z[30]=z[12]*z[30];
    z[41]= - z[51]*z[41];
    z[41]=z[20] + z[41];
    z[41]=z[41]*z[40];
    z[42]= - z[9]*z[59];
    z[42]= - n<T>(3,2)*z[3] + z[42];
    z[42]=z[9]*z[1]*z[42];
    z[41]=3*z[42] + z[37] + z[41];
    z[41]=z[13]*z[41];
    z[42]= - z[20]*z[60];
    z[42]=9*z[1] + z[42];
    z[42]=z[5]*z[42];
    z[46]= - z[3]*z[49];
    z[41]=z[41] + z[42] + z[46];
    z[41]=z[13]*z[41];
    z[36]= - z[36]*z[37];
    z[37]= - z[1]*z[24];
    z[42]=z[3]*z[55];
    z[37]=z[37] + z[42];
    z[37]=z[3]*z[37];
    z[29]=z[41] + z[29] + z[30] + z[36] + z[37];
    z[29]=z[11]*z[29];
    z[30]= - 3*z[93] + z[18] - z[94];
    z[30]=z[30]*z[40];
    z[36]=z[47] - z[100] + 7*z[93];
    z[36]=z[7]*z[36];
    z[30]=z[36] + z[30] - n<T>(23,2)*z[16] - z[86];
    z[30]=z[7]*z[30];
    z[36]= - 5*z[33] + z[35];
    z[37]=z[33] + z[91];
    z[37]=z[10]*z[37];
    z[37]=n<T>(1,2)*z[77] + z[37];
    z[37]=z[37]*z[60];
    z[36]=n<T>(1,2)*z[36] + z[37];
    z[36]=z[7]*z[36];
    z[37]= - z[60]*z[90];
    z[36]=z[36] + z[37] - n<T>(17,2)*z[18] - z[39];
    z[36]=z[7]*z[36];
    z[37]=z[5]*z[76];
    z[37]= - n<T>(5,2)*z[37] + z[45] + z[20];
    z[36]=n<T>(1,2)*z[37] + z[36];
    z[36]=z[6]*z[36];
    z[37]=n<T>(67,2)*z[16] + 16*z[20];
    z[37]=z[10]*z[37];
    z[37]=n<T>(35,2)*z[18] + z[37];
    z[37]=z[5]*z[37];
    z[31]=z[37] - z[31] - 14*z[20];
    z[31]=z[5]*z[31];
    z[30]=z[36] + z[30] + n<T>(3,2)*z[1] + z[31];
    z[30]=z[6]*z[30];
    z[31]=n<T>(51,4)*z[18];
    z[36]= - z[31] - z[92];
    z[36]=z[5]*z[36];
    z[37]= - z[4]*z[34];
    z[36]=z[37] + n<T>(43,4)*z[16] + z[36];
    z[36]=z[4]*z[36];
    z[37]=z[56] + z[19];
    z[37]=n<T>(1,2)*z[37] + 7*z[21];
    z[37]=z[7]*z[37];
    z[40]=z[19] + z[21];
    z[40]= - z[57] + n<T>(3,4)*z[40];
    z[40]=z[40]*z[101];
    z[21]= - z[21] + z[50];
    z[21]=z[21]*z[89];
    z[41]= - z[5]*z[56];
    z[41]= - 25*z[1] + z[41];
    z[21]=z[21] + z[36] + z[40] + n<T>(1,2)*z[41] + z[37];
    z[21]=z[3]*z[21];
    z[19]= - z[44] + n<T>(9,2)*z[19] + z[16] - z[75];
    z[19]=z[7]*z[19];
    z[28]=17*z[16] - z[28];
    z[28]=z[5]*z[28];
    z[28]= - 29*z[1] + z[28];
    z[19]=n<T>(1,2)*z[28] + z[19];
    z[19]=z[7]*z[19];
    z[28]=n<T>(3,2)*z[10];
    z[36]= - z[58]*z[28];
    z[37]= - z[95]*z[24];
    z[18]=z[37] - 11*z[18] + z[36];
    z[18]=z[5]*z[18];
    z[20]=4*z[20];
    z[18]=z[18] - n<T>(15,2)*z[16] - z[20];
    z[18]=z[5]*z[18];
    z[16]=z[16] - z[52];
    z[16]=z[16]*z[28];
    z[28]= - z[33] + z[35];
    z[28]=z[28]*z[43];
    z[16]=z[28] - z[34] + z[16];
    z[16]=z[7]*z[16];
    z[16]=z[16] - z[61] + z[20];
    z[16]=z[7]*z[16];
    z[20]= - z[7]*z[77];
    z[20]= - z[33] + z[20];
    z[20]=z[20]*z[43];
    z[20]= - z[31] + z[20];
    z[20]=z[7]*z[20];
    z[20]=z[98] + z[20];
    z[20]=z[6]*z[20];
    z[16]= - z[38] + z[20] + z[16] - 16*z[1] + z[18];
    z[16]=z[4]*z[16];
    z[18]= - z[5]*z[39];
    z[18]=z[18] + z[32];
    z[18]=z[18]*z[24];
    z[18]=z[18] - z[25] + z[10];
    z[18]=z[5]*z[18];

    r +=  - static_cast<T>(1)+ z[16] + z[17] + z[18] + z[19] + z[21] + z[22] + z[23]
       + z[26] + z[27] + z[29] + z[30] + z[54];
 
    return r;
}

template double qqb_2lNLC_r769(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r769(const std::array<dd_real,31>&);
#endif
