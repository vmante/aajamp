#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1116(const std::array<T,31>& k) {
  T z[77];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[8];
    z[4]=k[14];
    z[5]=k[9];
    z[6]=k[2];
    z[7]=k[4];
    z[8]=k[5];
    z[9]=k[12];
    z[10]=k[11];
    z[11]=k[13];
    z[12]=k[10];
    z[13]=k[15];
    z[14]=k[3];
    z[15]=k[19];
    z[16]=k[22];
    z[17]=npow(z[1],2);
    z[18]=n<T>(1,4)*z[17];
    z[19]=npow(z[1],3);
    z[20]=z[19]*z[3];
    z[21]=z[18] - z[20];
    z[21]=z[21]*z[3];
    z[22]=n<T>(3,2)*z[19];
    z[23]=npow(z[1],4);
    z[24]=z[23]*z[3];
    z[25]= - z[22] + z[24];
    z[26]=n<T>(1,2)*z[3];
    z[25]=z[2]*z[25]*z[26];
    z[25]= - z[21] + z[25];
    z[27]=3*z[2];
    z[25]=z[25]*z[27];
    z[28]=n<T>(1,4)*z[11];
    z[29]=z[20]*z[28];
    z[21]= - z[21] + z[29];
    z[21]=z[11]*z[21];
    z[29]=npow(z[2],2);
    z[30]=npow(z[3],2);
    z[31]=3*z[30];
    z[31]= - z[29]*z[31];
    z[32]=npow(z[11],2);
    z[33]= - z[30]*z[32];
    z[31]=z[31] + z[33];
    z[31]=z[7]*z[19]*z[31];
    z[21]=n<T>(1,4)*z[31] + z[25] + z[21];
    z[21]=z[7]*z[21];
    z[25]=npow(z[1],5);
    z[31]=z[25]*z[3];
    z[33]=z[8]*z[19];
    z[33]=z[33] - z[23] + z[31];
    z[33]=z[5]*z[33];
    z[25]= - z[25]*z[26];
    z[25]= - 2*z[23] + z[25];
    z[25]=z[3]*z[25];
    z[34]=n<T>(5,2)*z[19];
    z[35]=z[17]*z[8];
    z[25]=z[33] - z[35] + z[34] + z[25];
    z[25]=z[5]*z[25];
    z[33]=2*z[17];
    z[36]=z[8]*z[1];
    z[37]= - z[33] + n<T>(1,2)*z[36];
    z[38]=z[24] + z[19];
    z[39]=n<T>(3,2)*z[3];
    z[39]=z[38]*z[39];
    z[25]=z[25] + z[39] + z[37];
    z[25]=z[5]*z[25];
    z[34]=z[34] - z[24];
    z[34]=z[34]*z[3];
    z[31]=3*z[23] - z[31];
    z[31]=z[31]*z[26];
    z[31]= - z[19] + z[31];
    z[31]=z[2]*z[31];
    z[31]=n<T>(1,2)*z[31] - z[17] + z[34];
    z[31]=z[31]*z[27];
    z[39]=3*z[17];
    z[40]=z[39] - 7*z[20];
    z[40]=z[3]*z[40];
    z[41]=z[39] - 5*z[20];
    z[41]=z[41]*z[28];
    z[37]=n<T>(5,2)*z[20] + z[37];
    z[42]=3*z[13];
    z[37]=z[37]*z[42];
    z[21]=z[37] + z[25] + z[21] + z[41] + z[40] + z[31];
    z[21]=z[4]*z[21];
    z[25]=z[19]*z[11];
    z[31]=z[33] + z[25];
    z[31]=z[5]*z[31];
    z[37]=z[17]*z[11];
    z[40]=2*z[1];
    z[31]=z[31] - z[40] - n<T>(5,2)*z[37];
    z[31]=z[5]*z[31];
    z[41]=n<T>(3,2)*z[1];
    z[43]=z[11]*z[41];
    z[31]=z[43] + z[31];
    z[43]=z[12]*z[5];
    z[31]=z[31]*z[43];
    z[44]=npow(z[5],3);
    z[45]= - z[44]*z[40];
    z[44]=z[44]*z[1];
    z[46]=z[44]*z[12];
    z[47]=z[14]*z[46];
    z[45]=z[47] + z[45];
    z[45]=z[11]*z[45];
    z[47]=z[11]*z[40];
    z[48]= - z[11]*z[33];
    z[48]= - z[1] + z[48];
    z[48]=z[5]*z[48];
    z[47]=z[47] + z[48];
    z[48]=npow(z[5],2);
    z[49]=z[48]*z[12];
    z[47]=z[47]*z[49];
    z[45]=z[47] + z[45];
    z[45]=z[14]*z[45];
    z[47]=z[33] + z[36];
    z[47]=z[11]*z[47];
    z[47]=z[1] + z[47];
    z[47]=z[5]*z[47];
    z[50]=3*z[1];
    z[51]= - z[11]*z[50];
    z[47]=z[51] + z[47];
    z[47]=z[47]*z[48];
    z[46]=z[46]*z[6];
    z[31]=z[45] + z[46] + z[47] + z[31];
    z[31]=z[14]*z[31];
    z[45]=5*z[17];
    z[47]=z[45] + z[25];
    z[51]=z[19]*z[5];
    z[47]=n<T>(1,2)*z[47] - z[51];
    z[47]=z[5]*z[47];
    z[47]=z[47] - z[41] - z[37];
    z[47]=z[5]*z[47];
    z[52]=n<T>(1,2)*z[1];
    z[53]=z[11]*z[52];
    z[47]=z[53] + z[47];
    z[47]=z[12]*z[47];
    z[53]= - z[29]*z[50];
    z[54]= - z[1]*z[32];
    z[53]=z[53] + z[54];
    z[54]=n<T>(3,2)*z[17];
    z[55]=z[54] + z[36];
    z[55]=z[11]*z[55];
    z[55]=z[1] + z[55];
    z[55]=z[5]*z[55];
    z[56]= - z[11]*z[1];
    z[55]=z[56] + z[55];
    z[55]=z[5]*z[55];
    z[56]= - z[5]*z[17];
    z[56]=z[1] + z[56];
    z[56]=z[56]*z[49];
    z[46]=2*z[56] - z[46];
    z[46]=z[6]*z[46];
    z[56]=z[36] - z[17];
    z[57]=z[9]*z[56]*z[29];
    z[31]=z[31] - n<T>(3,4)*z[57] + z[46] + z[47] + n<T>(1,4)*z[53] + z[55];
    z[31]=z[14]*z[31];
    z[46]=z[17]*z[3];
    z[47]=z[46] - z[1];
    z[53]=z[47]*z[30];
    z[55]=z[36]*npow(z[3],3);
    z[57]=z[55] + 3*z[53];
    z[58]=n<T>(1,2)*z[8];
    z[57]=z[57]*z[58];
    z[58]=z[41] - 2*z[46];
    z[58]=z[3]*z[58];
    z[58]=z[58] + z[57];
    z[58]=z[8]*z[58];
    z[59]=z[26]*z[19];
    z[59]=z[59] - z[17];
    z[60]=z[59]*z[3];
    z[58]=z[58] - z[52] - z[60];
    z[58]=z[58]*z[27];
    z[61]=z[46] - z[40];
    z[61]=z[61]*z[30]*z[8];
    z[62]=z[40] - n<T>(7,2)*z[46];
    z[62]=z[3]*z[62];
    z[58]=z[58] + z[62] + z[61];
    z[58]=z[2]*z[58];
    z[62]=n<T>(3,2)*z[46];
    z[63]= - z[40] + z[62];
    z[63]=z[3]*z[63];
    z[64]= - z[53] - z[55];
    z[64]=z[8]*z[64];
    z[65]=z[3]*z[1];
    z[64]=z[65] + z[64];
    z[64]=z[8]*z[64];
    z[47]=z[64] + z[47];
    z[47]=z[11]*z[47];
    z[47]=n<T>(1,2)*z[47] + z[63] - z[61];
    z[47]=z[11]*z[47];
    z[61]=z[30]*z[1];
    z[63]=n<T>(1,2)*z[55];
    z[64]=z[61] - z[63];
    z[64]=z[64]*z[8];
    z[65]=z[26]*z[17];
    z[66]=z[65] - z[1];
    z[67]=z[66]*z[26];
    z[64]=z[64] + z[67];
    z[67]=z[64]*z[27];
    z[61]=z[61] - z[55];
    z[61]=2*z[61];
    z[67]=z[61] + z[67];
    z[67]=z[2]*z[67];
    z[64]=z[11]*z[64];
    z[61]= - z[61] + z[64];
    z[61]=z[11]*z[61];
    z[61]=z[67] + z[61];
    z[61]=z[7]*z[61];
    z[47]=z[61] + z[58] + z[47];
    z[47]=z[7]*z[47];
    z[58]=z[19]*z[2];
    z[61]= - z[45] - z[58];
    z[64]=z[7]*z[2];
    z[67]=z[64]*z[40];
    z[68]=n<T>(1,2)*z[17];
    z[69]=z[2]*z[68];
    z[69]=z[1] + z[69];
    z[69]=5*z[69] - z[67];
    z[69]=z[7]*z[69];
    z[61]=n<T>(1,2)*z[61] + z[69];
    z[61]=z[12]*z[61];
    z[69]=n<T>(7,2)*z[17];
    z[70]=z[69] - z[36];
    z[70]=z[2]*z[70];
    z[71]=z[7]*z[1];
    z[72]= - z[27]*z[71];
    z[61]=z[61] + z[72] + 4*z[1] + z[70];
    z[61]=z[7]*z[61];
    z[70]=4*z[17] - z[36];
    z[70]=z[2]*z[70];
    z[67]= - z[67] + z[50] + z[70];
    z[67]=z[7]*z[67];
    z[70]=z[35] - z[19];
    z[72]=z[2]*z[70];
    z[67]=z[67] - z[39] + 2*z[72];
    z[67]=z[7]*z[67];
    z[72]=z[64]*z[1];
    z[73]=z[2]*z[33];
    z[73]= - z[72] + z[50] + z[73];
    z[73]=z[7]*z[73];
    z[73]=z[73] - z[39] - z[58];
    z[74]=npow(z[7],2);
    z[73]=z[12]*z[73]*z[74];
    z[75]= - z[8]*z[58];
    z[67]=z[73] + z[75] + z[67];
    z[67]=z[13]*z[67];
    z[35]= - n<T>(1,2)*z[19] + z[35];
    z[35]=z[2]*z[35];
    z[35]=z[67] + z[68] + z[35] + z[61];
    z[35]=z[13]*z[35];
    z[61]= - z[64]*z[41];
    z[64]=n<T>(5,2)*z[1];
    z[67]=z[17]*z[2];
    z[61]=z[61] + z[64] + z[67];
    z[61]=z[7]*z[61];
    z[61]=z[61] + z[59];
    z[61]=z[12]*z[61];
    z[35]=z[35] + z[61] - z[72] + z[67] + z[52] - z[46];
    z[35]=z[35]*z[42];
    z[61]=z[20] - z[17];
    z[72]=z[61]*z[26];
    z[73]=z[61] + z[36];
    z[75]=z[5]*z[73];
    z[75]= - z[72] + z[75];
    z[75]=z[75]*z[48];
    z[73]=z[13]*z[73];
    z[73]= - z[72] + z[73];
    z[76]=npow(z[13],2);
    z[76]=3*z[76];
    z[73]=z[73]*z[76];
    z[73]=z[75] + z[73];
    z[73]=z[4]*z[73];
    z[75]=z[33] - z[20];
    z[75]=z[5]*z[75];
    z[75]=z[72] + z[75];
    z[49]=z[75]*z[49];
    z[72]=z[1] + z[72];
    z[72]=z[12]*z[72];
    z[75]=z[71] - z[61];
    z[75]=z[12]*z[75];
    z[75]= - z[1] + z[75];
    z[75]=z[13]*z[75];
    z[72]=z[72] + z[75];
    z[72]=z[72]*z[76];
    z[44]=z[73] + z[72] - z[44] + z[49];
    z[44]=z[6]*z[44];
    z[49]= - z[22] - z[24];
    z[49]=z[3]*z[49];
    z[70]=z[24] + z[70];
    z[70]=z[5]*z[70];
    z[49]=2*z[70] - z[36] + n<T>(5,2)*z[17] + z[49];
    z[49]=z[5]*z[49];
    z[70]=n<T>(3,2)*z[20];
    z[72]=z[70] - z[17];
    z[72]=z[72]*z[3];
    z[49]=z[72] + z[49];
    z[49]=z[5]*z[49];
    z[36]=n<T>(5,2)*z[61] + z[36];
    z[36]=z[13]*z[36];
    z[36]= - z[72] + z[36];
    z[36]=z[36]*z[42];
    z[36]=z[49] + z[36];
    z[36]=z[4]*z[36];
    z[49]= - z[45] + z[20];
    z[49]=n<T>(1,2)*z[49] + z[51];
    z[49]=z[5]*z[49];
    z[49]= - z[60] + z[49];
    z[43]=z[49]*z[43];
    z[49]= - z[13]*z[74]*z[50];
    z[49]=z[49] + n<T>(1,2)*z[61] - 4*z[71];
    z[49]=z[12]*z[49];
    z[49]= - z[1] + z[49];
    z[49]=z[13]*z[49];
    z[50]= - z[1] + z[60];
    z[50]=z[12]*z[50];
    z[49]=z[50] + z[49];
    z[42]=z[49]*z[42];
    z[49]= - z[5]*z[33];
    z[49]=z[1] + z[49];
    z[48]=z[49]*z[48];
    z[36]=z[44] + z[36] + z[42] + z[48] + z[43];
    z[36]=z[6]*z[36];
    z[42]=z[66]*z[27];
    z[43]=z[11]*z[65];
    z[42]=z[42] + z[43];
    z[42]=z[7]*z[42];
    z[39]= - z[39] + z[20];
    z[39]=z[3]*z[39];
    z[28]= - z[61]*z[28];
    z[43]=z[17] - n<T>(1,2)*z[51];
    z[43]=z[5]*z[43];
    z[28]=z[43] + n<T>(1,2)*z[42] + z[28] + z[41] + z[39];
    z[28]=z[12]*z[28];
    z[39]=z[62] - z[1];
    z[39]=z[39]*z[30];
    z[39]=z[39] + z[63];
    z[39]=z[39]*z[8];
    z[42]= - n<T>(11,2)*z[17] + 4*z[20];
    z[42]=z[3]*z[42];
    z[42]=z[52] + z[42];
    z[42]=z[3]*z[42];
    z[42]=z[42] + 3*z[39];
    z[42]=z[8]*z[42];
    z[43]=3*z[3];
    z[43]=z[59]*z[43];
    z[44]=z[43] + n<T>(7,4)*z[1];
    z[44]=z[44]*z[3];
    z[44]=z[44] + z[57];
    z[44]=z[44]*z[8];
    z[48]=z[44] - z[1];
    z[49]=z[54] - z[20];
    z[49]=z[49]*z[3];
    z[50]= - z[49] - z[48];
    z[50]=z[8]*z[50];
    z[23]=z[23]*z[26];
    z[51]=z[23] - z[19];
    z[54]=z[51]*z[26];
    z[50]=z[54] + z[50];
    z[27]=z[50]*z[27];
    z[27]=z[27] + z[42] + z[1] + z[43];
    z[27]=z[2]*z[27];
    z[42]= - n<T>(3,4)*z[1] - z[60];
    z[42]=z[3]*z[42];
    z[43]= - z[53] - z[63];
    z[43]=z[8]*z[43];
    z[42]=z[42] + z[43];
    z[42]=z[8]*z[42];
    z[43]=n<T>(1,4)*z[1];
    z[42]=z[43] + z[42];
    z[42]=z[11]*z[42];
    z[49]=z[52] + z[49];
    z[49]=z[3]*z[49];
    z[42]=z[42] + z[49] - z[39];
    z[42]=z[8]*z[42];
    z[42]=z[66] + z[42];
    z[42]=z[11]*z[42];
    z[23]=2*z[19] + z[23];
    z[23]=z[3]*z[23];
    z[38]= - z[5]*z[38];
    z[23]=z[38] + z[17] + z[23];
    z[23]=z[5]*z[23];
    z[38]= - z[17] - z[20];
    z[38]=z[3]*z[38];
    z[23]=z[23] - n<T>(1,2)*z[37] - z[52] + z[38];
    z[23]=z[5]*z[23];
    z[20]= - z[45] + 3*z[20];
    z[20]=z[20]*z[3];
    z[20]=z[20] + z[40];
    z[20]=z[20]*z[3];
    z[38]= - z[64] + 3*z[46];
    z[38]=z[38]*z[30];
    z[38]=z[38] + z[55];
    z[38]=z[38]*z[8];
    z[20]=z[20] + z[38];
    z[20]=z[20]*z[8];
    z[34]=z[34] - z[33];
    z[34]=z[34]*z[3];
    z[20]=z[20] - z[34];
    z[34]= - z[43] + z[20];
    z[34]=z[8]*z[34];
    z[24]= - z[24] + 3*z[19];
    z[24]=z[24]*z[3];
    z[24]=z[24] - z[69];
    z[38]= - z[3]*z[24];
    z[38]= - z[41] + z[38];
    z[38]=n<T>(1,2)*z[38] + z[44];
    z[40]=npow(z[8],2);
    z[38]=z[11]*z[38]*z[40];
    z[18]=z[38] - z[18] + z[34];
    z[18]=z[11]*z[18];
    z[34]=z[70] - z[33];
    z[34]=z[34]*z[3];
    z[34]=z[34] + z[52];
    z[34]=z[34]*z[3];
    z[34]=z[34] + z[39];
    z[34]=z[34]*z[8];
    z[38]=z[51]*z[3];
    z[38]=z[38] + z[68];
    z[38]=z[38]*z[3];
    z[34]=z[34] + z[38];
    z[32]=z[32]*z[7];
    z[38]=z[56]*z[32];
    z[18]= - n<T>(1,4)*z[38] + z[18] + z[34];
    z[18]=z[10]*z[18];
    z[20]= - z[52] + z[20];
    z[20]=z[8]*z[20];
    z[24]=z[24]*z[26];
    z[24]=z[24] - z[48];
    z[24]=z[2]*z[24]*z[40];
    z[20]=z[20] + z[24];
    z[20]=z[2]*z[20];
    z[20]=z[20] - z[34];
    z[20]=z[9]*z[20];
    z[24]=n<T>(1,2)*z[16];
    z[26]= - z[11] + z[9];
    z[34]=z[19]*z[10];
    z[26]=z[24]*z[34]*z[7]*z[26];
    z[19]= - z[19]*z[32];
    z[19]=z[25] + z[19];
    z[19]=z[10]*z[19];
    z[25]= - z[17] - n<T>(1,2)*z[34];
    z[25]=z[9]*z[25];
    z[19]=z[26] + z[25] + z[37] + n<T>(1,2)*z[19];
    z[19]=z[19]*z[24];
    z[22]= - z[29]*z[22];
    z[24]=z[58] - z[34];
    z[24]=z[15]*z[24];
    z[22]=n<T>(3,2)*z[24] + z[22];
    z[22]=z[14]*z[9]*z[22];
    z[17]= - z[10]*z[17];
    z[17]=z[67] + z[17] + z[22];
    z[17]=z[15]*z[17];
    z[22]=z[30]*z[33];

    r += n<T>(1,2)*z[17] + z[18] + z[19] + 3*z[20] + z[21] + z[22] + z[23]
       + z[27] + z[28] + z[31] + z[35] + z[36] + z[42] + z[47];
 
    return r;
}

template double qqb_2lNLC_r1116(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1116(const std::array<dd_real,31>&);
#endif
