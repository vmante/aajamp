#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r906(const std::array<T,31>& k) {
  T z[51];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[11];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[10];
    z[8]=k[14];
    z[9]=k[4];
    z[10]=k[3];
    z[11]=k[8];
    z[12]=k[15];
    z[13]=k[9];
    z[14]=npow(z[2],2);
    z[15]=z[14]*z[6];
    z[16]=npow(z[1],6);
    z[17]=z[15]*z[16];
    z[18]=npow(z[1],5);
    z[19]=z[18]*z[14];
    z[20]=z[17] + z[19];
    z[20]=z[20]*z[6];
    z[21]=z[14]*npow(z[1],4);
    z[20]=z[20] + 3*z[21];
    z[20]=z[20]*z[6];
    z[22]=z[17] - z[19];
    z[23]=z[22]*z[5];
    z[18]=z[15]*z[18];
    z[23]=z[23] - z[18];
    z[24]=n<T>(1,2)*z[5];
    z[23]=z[23]*z[24];
    z[25]=npow(z[1],3);
    z[26]=z[25]*z[14];
    z[20]=z[20] + z[23] - z[26];
    z[20]=z[20]*z[5];
    z[23]=npow(z[6],2);
    z[27]=z[19]*z[23];
    z[27]=z[27] + n<T>(1,2)*z[26];
    z[27]=z[27]*z[6];
    z[20]=z[20] + z[27];
    z[28]=3*z[14];
    z[29]=npow(z[1],7);
    z[30]=z[28]*z[29];
    z[31]=z[15]*npow(z[1],8);
    z[30]=z[30] - z[31];
    z[30]=z[30]*z[6];
    z[28]=z[28]*z[16];
    z[30]=z[30] - z[28];
    z[30]=z[30]*z[6];
    z[30]=z[30] + z[19];
    z[30]=z[30]*z[5];
    z[31]=2*z[14];
    z[31]=z[31]*z[16];
    z[29]=z[15]*z[29];
    z[31]=z[31] - z[29];
    z[31]=z[31]*z[6];
    z[31]=z[31] - z[19];
    z[32]=z[31]*z[6];
    z[30]=z[30] + z[32];
    z[32]= - z[7]*z[30];
    z[14]=z[16]*z[14];
    z[14]= - z[29] + 4*z[14];
    z[14]=z[14]*z[6];
    z[14]=z[14] - 5*z[19];
    z[14]=z[14]*z[6];
    z[16]=2*z[21];
    z[14]=z[14] + z[16];
    z[14]=z[14]*z[5];
    z[32]= - z[14] + z[32];
    z[32]=z[7]*z[32];
    z[32]=z[32] + z[20];
    z[32]=z[7]*z[32];
    z[33]=z[18] - z[21];
    z[34]=npow(z[5],2);
    z[34]=n<T>(1,2)*z[34];
    z[35]=z[33]*z[34];
    z[27]=z[35] + z[27];
    z[27]=z[27]*z[5];
    z[35]=z[5]*z[6];
    z[36]=z[16]*z[35];
    z[37]=z[4]*z[36];
    z[32]=z[32] + z[27] + z[37];
    z[32]=z[8]*z[32];
    z[30]=z[4]*z[30];
    z[14]= - z[14] + z[30];
    z[14]=z[4]*z[14];
    z[14]=z[14] - z[20];
    z[14]=z[4]*z[14];
    z[14]=z[27] + z[14];
    z[14]=z[3]*z[14];
    z[20]=3*z[19];
    z[27]=z[20] - z[17];
    z[30]=z[23]*z[5];
    z[27]=z[27]*z[30];
    z[37]=z[18] - z[16];
    z[37]=z[37]*z[23];
    z[27]= - z[37] + z[27];
    z[27]=z[4]*z[27];
    z[38]=z[18] + z[16];
    z[39]=z[38]*z[30];
    z[40]=npow(z[6],3);
    z[41]=z[16]*z[40];
    z[27]=z[27] - z[41] - z[39];
    z[27]=z[27]*npow(z[4],2);
    z[22]=z[22]*z[30];
    z[30]=z[19]*z[40];
    z[22]= - z[30] + z[22];
    z[22]=z[7]*z[22];
    z[22]= - z[39] + z[22];
    z[39]=npow(z[7],2);
    z[22]=z[22]*z[39];
    z[30]=z[30]*z[5];
    z[40]=z[21]*z[40];
    z[42]= - z[40] - z[30];
    z[42]=z[42]*npow(z[4],3);
    z[43]=npow(z[7],3);
    z[30]= - z[43]*z[30];
    z[30]=z[42] + z[30];
    z[30]=z[9]*z[30];
    z[22]=z[30] + z[27] + z[22];
    z[22]=z[9]*z[22];
    z[27]=2*z[8];
    z[30]= - z[5] + z[4];
    z[30]=z[27]*z[21]*z[30];
    z[42]=z[21]*z[6];
    z[44]= - 3*z[26] + 5*z[42];
    z[45]=n<T>(1,2)*z[44];
    z[46]=z[16]*z[5];
    z[47]=z[45] - z[46];
    z[48]=z[47]*z[3];
    z[45]=z[4]*z[45];
    z[30]= - z[48] + z[45] + z[30];
    z[30]=z[12]*z[30];
    z[45]=z[26]*z[35];
    z[15]=z[15]*z[25];
    z[25]= - z[4]*z[15];
    z[25]=z[45] + z[25];
    z[49]= - z[35]*z[21];
    z[50]=z[4]*z[42];
    z[49]=z[49] + z[50];
    z[27]=z[49]*z[27];
    z[24]=z[44]*z[24];
    z[44]=z[3]*z[24];
    z[25]=z[30] + z[44] + n<T>(3,2)*z[25] + z[27];
    z[25]=z[11]*z[25];
    z[15]= - n<T>(3,2)*z[15] - z[36];
    z[15]=z[7]*z[15];
    z[24]=z[8]*z[24];
    z[27]=z[7] - z[8];
    z[27]=z[12]*z[47]*z[27];
    z[15]=z[27] + z[24] - n<T>(3,2)*z[45] + z[15];
    z[15]=z[13]*z[15];
    z[17]=2*z[19] - z[17];
    z[17]=z[6]*z[17];
    z[17]= - z[21] + z[17];
    z[17]=z[6]*z[17];
    z[19]=z[28] - z[29];
    z[19]=z[6]*z[19];
    z[19]= - z[20] + z[19];
    z[19]=z[19]*z[35];
    z[17]=z[17] + z[19];
    z[17]=z[4]*z[17];
    z[19]=4*z[21] - z[18];
    z[19]=z[19]*z[35];
    z[17]=z[17] - z[37] + z[19];
    z[17]=z[4]*z[17];
    z[19]=z[38]*z[6];
    z[19]=z[19] + z[26];
    z[19]=z[19]*z[6];
    z[18]=z[34]*z[18];
    z[18]=z[19] + z[18];
    z[18]=z[18]*z[5];
    z[18]=z[18] + z[41];
    z[17]=z[17] - z[18];
    z[17]=z[4]*z[17];
    z[16]= - z[23]*z[16];
    z[19]=z[33]*z[35];
    z[16]=z[16] + z[19];
    z[19]=z[31]*z[35];
    z[19]= - z[42] + z[19];
    z[19]=z[7]*z[19];
    z[16]=2*z[16] + z[19];
    z[16]=z[7]*z[16];
    z[16]=z[16] - z[18];
    z[16]=z[7]*z[16];
    z[18]= - z[7]*z[47];
    z[19]=z[4]*z[46];
    z[18]=z[19] + z[18];
    z[18]=z[8]*z[18];
    z[19]=z[4]*z[48];
    z[18]=z[18] + z[19];
    z[18]=z[12]*z[18];
    z[19]=z[7]*z[23]*z[21];
    z[19]=z[40] + z[19];
    z[19]=z[19]*z[39];
    z[20]= - z[10]*z[43]*z[40];
    z[19]=2*z[19] + z[20];
    z[19]=z[10]*z[19];

    r += z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[22] + z[25]
       + z[32];
 
    return r;
}

template double qqb_2lNLC_r906(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r906(const std::array<dd_real,31>&);
#endif
