#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1237(const std::array<T,31>& k) {
  T z[95];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[7];
    z[9]=k[15];
    z[10]=k[5];
    z[11]=k[12];
    z[12]=k[14];
    z[13]=k[11];
    z[14]=k[2];
    z[15]=k[19];
    z[16]=k[8];
    z[17]=k[22];
    z[18]=k[17];
    z[19]=npow(z[3],5);
    z[20]=3*z[19];
    z[21]=z[12]*z[13];
    z[22]= - z[21]*z[20];
    z[23]=z[19]*z[9];
    z[24]=z[12] + z[13];
    z[25]=z[24]*z[23];
    z[22]=z[22] + z[25];
    z[25]=npow(z[9],3);
    z[26]=19*z[25];
    z[22]=z[22]*z[26];
    z[27]=z[12]*z[11];
    z[28]=z[27]*z[20];
    z[29]=z[12] - z[11];
    z[30]=z[4]*z[29]*z[19];
    z[28]=z[28] + z[30];
    z[30]=npow(z[4],3);
    z[31]=45*z[30];
    z[28]=z[28]*z[31];
    z[22]=z[22] + z[28];
    z[22]=z[10]*z[22];
    z[28]=z[7]*z[13];
    z[32]=z[12]*z[14];
    z[28]=z[28] + z[32];
    z[33]=npow(z[9],4);
    z[33]=19*z[33];
    z[34]=z[33]*z[19];
    z[35]= - z[28]*z[34];
    z[36]=npow(z[4],4);
    z[36]=45*z[36];
    z[37]=z[36]*z[19];
    z[38]=z[2]*z[11];
    z[39]=z[32] - z[38];
    z[39]=z[39]*z[37];
    z[22]=z[22] + z[35] + z[39];
    z[22]=z[10]*z[22];
    z[35]=npow(z[14],2);
    z[39]=z[35]*z[12];
    z[40]=npow(z[7],2);
    z[41]=z[13]*z[40];
    z[41]=z[39] + z[41];
    z[41]=z[41]*z[34];
    z[42]=npow(z[2],2);
    z[43]= - z[11]*z[42];
    z[43]=z[39] + z[43];
    z[43]=z[43]*z[37];
    z[22]=z[22] + z[41] + z[43];
    z[22]=z[10]*z[22];
    z[34]=z[34]*z[8];
    z[41]=npow(z[7],3);
    z[43]= - z[41]*z[34];
    z[44]=npow(z[2],3);
    z[45]=z[44]*z[6];
    z[46]= - z[37]*z[45];
    z[22]=z[22] + z[43] + z[46];
    z[22]=z[10]*z[22];
    z[43]=npow(z[7],4);
    z[34]= - z[43]*z[34];
    z[46]=npow(z[2],4);
    z[47]=z[46]*z[6];
    z[37]=z[37]*z[47];
    z[34]=z[34] + z[37];
    z[22]=3*z[34] + z[22];
    z[22]=z[10]*z[22];
    z[34]=z[33]*z[21];
    z[37]=z[36]*z[27];
    z[34]=z[34] - z[37];
    z[37]= - npow(z[10],6)*z[34];
    z[48]=z[5]*z[8];
    z[33]= - npow(z[7],6)*z[48]*z[33];
    z[49]=z[5]*z[6];
    z[36]=npow(z[2],6)*z[36]*z[49];
    z[33]=z[37] + z[33] + z[36];
    z[33]=z[1]*z[33]*npow(z[3],6);
    z[36]=z[5]*z[14];
    z[37]=z[5] - z[8];
    z[50]=z[7]*z[37];
    z[50]= - z[36] + z[50];
    z[51]=3*z[7];
    z[50]=z[50]*z[51];
    z[52]=z[35]*z[5];
    z[50]=z[52] + z[50];
    z[23]=z[50]*z[41]*z[23];
    z[20]= - npow(z[7],5)*z[48]*z[20];
    z[20]=z[20] + z[23];
    z[20]=z[20]*z[26];
    z[23]=z[2] - z[14];
    z[50]= - z[2]*z[23];
    z[50]= - z[35] + z[50];
    z[50]=z[5]*z[50]*z[44];
    z[53]=3*z[6];
    z[54]=z[53]*npow(z[2],5);
    z[50]= - z[54] + z[50];
    z[50]=z[4]*z[50];
    z[54]=z[5]*z[54];
    z[50]=z[54] + z[50];
    z[19]=z[31]*z[19]*z[50];
    z[19]=z[33] + z[22] + z[20] + z[19];
    z[19]=z[1]*z[19];
    z[20]=npow(z[3],4);
    z[22]=4*z[20];
    z[33]= - z[21]*z[22];
    z[50]=z[24]*z[20];
    z[54]=z[20]*z[9];
    z[50]=2*z[50] - z[54];
    z[50]=z[9]*z[50];
    z[33]=z[33] + z[50];
    z[50]=npow(z[9],2);
    z[55]=19*z[50];
    z[33]=z[33]*z[55];
    z[56]=z[27]*z[22];
    z[57]=2*z[12];
    z[58]=3*z[11];
    z[59]= - z[58] + z[57];
    z[59]=z[59]*z[20];
    z[60]=z[20]*z[4];
    z[59]=z[59] - z[60];
    z[59]=z[4]*z[59];
    z[56]=z[56] + z[59];
    z[59]=npow(z[4],2);
    z[61]=45*z[59];
    z[56]=z[56]*z[61];
    z[33]=z[33] + z[56];
    z[33]=z[10]*z[33];
    z[28]= - z[28]*z[20];
    z[56]=z[7] + z[14];
    z[62]=z[56]*z[54];
    z[28]=z[28] + z[62];
    z[28]=z[28]*z[26];
    z[62]= - z[2]*z[58];
    z[62]=z[32] + z[62];
    z[62]=z[62]*z[20];
    z[63]=z[2] + z[14];
    z[64]= - z[63]*z[60];
    z[62]=z[62] + z[64];
    z[62]=z[62]*z[31];
    z[28]=z[33] + z[28] + z[62];
    z[28]=z[10]*z[28];
    z[33]=z[56]*z[7];
    z[33]=z[33] + z[35];
    z[33]=z[33]*z[54];
    z[62]=z[13] - z[8];
    z[62]=z[62]*z[40];
    z[62]=z[39] + z[62];
    z[62]=z[62]*z[20];
    z[62]=z[62] - z[33];
    z[62]=z[62]*z[26];
    z[63]=z[63]*z[2];
    z[63]=z[63] + z[35];
    z[63]=z[63]*z[60];
    z[64]=2*z[6];
    z[65]= - z[11] - z[64];
    z[65]=z[65]*z[42];
    z[65]=z[39] + z[65];
    z[65]=z[65]*z[20];
    z[65]=z[65] - z[63];
    z[65]=z[65]*z[31];
    z[28]=z[28] + z[62] + z[65];
    z[28]=z[10]*z[28];
    z[62]=z[20]*z[41]*z[8];
    z[33]=z[7]*z[33];
    z[33]= - 5*z[62] + z[33];
    z[26]=z[33]*z[26];
    z[33]=z[20]*z[45];
    z[62]= - z[2]*z[63];
    z[33]=7*z[33] + z[62];
    z[31]=z[33]*z[31];
    z[26]=z[28] + z[26] + z[31];
    z[26]=z[10]*z[26];
    z[28]=z[6]*z[17];
    z[31]=npow(z[17],2);
    z[33]=z[28] + z[31];
    z[62]=z[33]*z[53];
    z[63]=z[8]*z[15];
    z[65]=npow(z[15],2);
    z[66]=z[63] - z[65];
    z[67]=n<T>(1,3)*z[8];
    z[68]=z[66]*z[67];
    z[69]=z[58]*z[31];
    z[70]=z[65]*z[13];
    z[62]=z[68] + z[62] - z[69] + n<T>(1,3)*z[70];
    z[62]=z[62]*z[12];
    z[43]= - z[43]*z[48]*z[22];
    z[68]=2*z[5];
    z[69]=z[35]*z[68];
    z[70]=7*z[8];
    z[71]= - z[70] + 8*z[5];
    z[71]=z[7]*z[71];
    z[71]= - 7*z[36] + z[71];
    z[71]=z[7]*z[71];
    z[69]=z[69] + z[71];
    z[69]=z[69]*z[20];
    z[71]= - z[14] + 5*z[7];
    z[72]=z[7]*z[71];
    z[72]= - z[35] + z[72];
    z[54]=z[72]*z[54];
    z[54]=z[69] + z[54];
    z[54]=z[9]*z[40]*z[54];
    z[43]=z[43] + z[54];
    z[43]=z[43]*z[55];
    z[22]=z[46]*z[49]*z[22];
    z[46]=2*z[2];
    z[54]= - z[14] + z[46];
    z[54]=z[2]*z[54];
    z[54]= - z[35] + z[54];
    z[54]=z[54]*z[42]*z[60];
    z[55]=3*z[2];
    z[23]= - z[23]*z[55];
    z[23]= - z[35] + z[23];
    z[23]=z[5]*z[23]*z[42];
    z[23]= - 8*z[47] + z[23];
    z[20]=z[23]*z[20];
    z[20]=z[20] + z[54];
    z[20]=z[4]*z[20];
    z[20]=z[22] + z[20];
    z[20]=z[20]*z[61];
    z[22]=npow(z[16],3);
    z[23]=z[22]*z[13];
    z[19]=z[19] + z[26] + z[20] + z[43] + n<T>(26,3)*z[23] + z[62];
    z[19]=z[1]*z[19];
    z[20]= - z[6] + 5*z[18] + z[17];
    z[20]=z[20]*z[53];
    z[26]= - z[15] - z[8];
    z[26]=z[26]*z[67];
    z[43]=z[58]*z[17];
    z[47]=4*z[18] + n<T>(1,3)*z[15];
    z[47]=z[13]*z[47];
    z[20]=z[26] + z[20] - z[43] + z[47];
    z[20]=z[12]*z[20];
    z[26]=npow(z[16],2);
    z[47]=2*z[17];
    z[54]= - z[47] + n<T>(5,3)*z[15];
    z[54]=2*z[54] - z[11];
    z[54]=z[11]*z[54];
    z[54]=z[54] - n<T>(4,3)*z[65] - 15*z[26];
    z[54]=z[13]*z[54];
    z[60]=5*z[11];
    z[47]=2*z[16] - z[47] - z[15];
    z[47]=z[47]*z[60];
    z[47]=2*z[65] + z[47];
    z[61]=n<T>(5,3)*z[17] - z[15];
    z[61]=z[61]*z[64];
    z[47]= - n<T>(2,3)*z[63] + n<T>(1,3)*z[47] + z[61];
    z[61]=2*z[8];
    z[47]=z[47]*z[61];
    z[69]=7*z[26];
    z[72]=4*z[31] - z[69];
    z[73]=n<T>(1,3)*z[11];
    z[72]=z[72]*z[73];
    z[74]= - z[16] + z[17] + z[15];
    z[74]=z[13]*z[74];
    z[74]=z[74] - n<T>(1,3)*z[33];
    z[75]=4*z[6];
    z[74]=z[74]*z[75];
    z[76]=z[13]*z[15];
    z[77]=z[76] + z[65];
    z[77]=z[77]*z[13];
    z[78]=z[65]*z[8];
    z[77]=z[77] - z[78];
    z[78]= - z[2]*z[11]*z[77];
    z[20]=n<T>(2,3)*z[78] + z[20] + z[47] + z[74] + z[72] + z[54];
    z[47]= - 2*z[18] - 7*z[15];
    z[47]= - z[68] + 4*z[47] + 7*z[13];
    z[47]=z[13]*z[47];
    z[54]= - z[2]*z[77];
    z[72]=z[11]*z[17];
    z[74]= - 6*z[18] + n<T>(19,3)*z[17];
    z[74]=z[6]*z[74];
    z[47]=7*z[54] + 28*z[63] + 5*z[74] - n<T>(95,3)*z[72] + z[47];
    z[47]=z[5]*z[47];
    z[54]=z[72] - z[31];
    z[54]=z[54]*z[11];
    z[74]=npow(z[18],2);
    z[77]=6*z[74] - n<T>(19,3)*z[31];
    z[77]=z[6]*z[77];
    z[78]=z[13]*z[18];
    z[79]=z[68]*z[78];
    z[80]=z[74]*z[13];
    z[77]=z[79] + z[77] - n<T>(19,3)*z[54] - 2*z[80];
    z[77]=z[5]*z[77];
    z[79]=z[6]*z[31];
    z[54]=z[79] - n<T>(26,3)*z[22] + z[54];
    z[54]=z[13]*z[54];
    z[79]= - z[12]*z[18];
    z[74]=z[79] - z[74];
    z[74]=z[53]*z[74];
    z[74]=z[80] + z[74];
    z[74]=z[12]*z[74];
    z[79]=z[22]*z[11];
    z[54]=z[74] + n<T>(26,3)*z[79] + z[54];
    z[54]=2*z[54] + z[77];
    z[54]=z[7]*z[54];
    z[20]=z[54] + 2*z[20] + z[47];
    z[33]= - n<T>(5,3)*z[33] - z[66];
    z[33]=z[6]*z[33];
    z[31]=5*z[31] + z[69];
    z[31]=z[31]*z[73];
    z[31]=z[31] + z[33];
    z[31]=z[8]*z[31];
    z[33]=z[79] - z[23];
    z[47]=n<T>(1,3)*z[26];
    z[54]= - z[65] + z[47];
    z[54]=z[6]*z[13]*z[54];
    z[31]=z[62] + z[31] - n<T>(26,3)*z[33] + z[54];
    z[54]=npow(z[3],3);
    z[62]=2*z[54];
    z[65]=z[24]*z[62];
    z[66]= - z[9]*z[21];
    z[66]= - z[54] + z[66];
    z[66]=z[9]*z[66];
    z[65]=z[65] + z[66];
    z[65]=z[9]*z[65];
    z[66]=3*z[54];
    z[69]= - z[21]*z[66];
    z[65]=z[69] + z[65];
    z[69]=19*z[9];
    z[65]=z[65]*z[69];
    z[73]=2*z[11];
    z[74]= - z[73] + z[12];
    z[74]=z[74]*z[62];
    z[77]=z[4]*z[27];
    z[62]= - z[62] + z[77];
    z[62]=z[4]*z[62];
    z[62]=z[74] + z[62];
    z[62]=z[4]*z[62];
    z[66]=z[27]*z[66];
    z[62]=z[66] + z[62];
    z[66]=45*z[4];
    z[62]=z[62]*z[66];
    z[62]=z[65] + z[62];
    z[65]=2*z[10];
    z[62]=z[62]*z[65];
    z[74]= - z[13] - z[8];
    z[74]=z[7]*z[74];
    z[74]= - z[32] + z[74];
    z[74]=z[74]*z[54];
    z[77]=38*z[9];
    z[80]=z[8]*z[77];
    z[80]= - n<T>(259,3)*z[21] + z[80];
    z[80]=z[9]*z[80];
    z[74]=19*z[74] + z[80];
    z[74]=z[74]*z[50];
    z[80]= - z[60] - z[53];
    z[80]=z[2]*z[80];
    z[80]=z[32] + z[80];
    z[80]=z[80]*z[54];
    z[81]=z[46] + z[14];
    z[82]=90*z[54];
    z[83]= - z[81]*z[82];
    z[83]=n<T>(713,3)*z[27] + z[83];
    z[83]=z[4]*z[83];
    z[80]=45*z[80] + z[83];
    z[80]=z[80]*z[59];
    z[83]=z[23]*z[6];
    z[84]=z[79]*z[8];
    z[83]=z[83] - z[84];
    z[83]=n<T>(52,3)*z[83];
    z[62]=z[62] + z[80] - z[83] + z[74];
    z[62]=z[62]*z[65];
    z[74]=z[13] - z[70];
    z[74]=z[74]*z[40];
    z[74]=z[39] + z[74];
    z[80]=38*z[54];
    z[74]=z[74]*z[80];
    z[80]=z[80]*z[7];
    z[56]=z[56]*z[80];
    z[84]=z[7]*z[8];
    z[85]=z[9]*z[84];
    z[56]=114*z[85] + n<T>(31,3)*z[8] + z[56];
    z[85]=2*z[9];
    z[56]=z[56]*z[85];
    z[56]=z[56] - 161*z[21] + z[74];
    z[56]=z[56]*z[50];
    z[74]= - z[11] + 13*z[6];
    z[74]=z[74]*z[42];
    z[39]=z[39] + z[74];
    z[39]=z[39]*z[82];
    z[74]= - z[2]*z[81];
    z[74]= - z[35] + z[74];
    z[74]=z[74]*z[54];
    z[74]=z[74] + z[4];
    z[81]=180*z[4];
    z[74]=z[74]*z[81];
    z[86]=443*z[27];
    z[39]=z[74] + z[86] + z[39];
    z[39]=z[39]*z[59];
    z[31]=z[62] + z[39] + 4*z[31] + z[56];
    z[31]=z[10]*z[31];
    z[39]=z[84]*z[5];
    z[56]=z[61] + z[39];
    z[56]=z[7]*z[56];
    z[62]=2*z[7];
    z[71]=z[71]*z[62];
    z[71]= - z[35] + z[71];
    z[71]=z[71]*z[80];
    z[40]=z[40]*z[77];
    z[74]=z[39] + 3*z[8];
    z[87]=z[74]*z[40];
    z[56]=z[87] + n<T>(31,3)*z[56] + z[71];
    z[56]=z[56]*z[85];
    z[71]=3*z[5];
    z[87]=z[35]*z[71];
    z[88]= - z[70] + 9*z[5];
    z[88]=z[88]*z[62];
    z[88]= - 13*z[36] + z[88];
    z[88]=z[7]*z[88];
    z[87]=z[87] + z[88];
    z[80]=z[87]*z[80];
    z[87]=z[8] + z[39];
    z[56]=z[56] + n<T>(31,3)*z[87] + z[80];
    z[56]=z[9]*z[56];
    z[41]=z[41]*z[54]*z[48];
    z[41]=z[56] - n<T>(241,3)*z[21] - 228*z[41];
    z[41]=z[9]*z[41];
    z[44]=z[44]*z[54]*z[49];
    z[48]=5*z[2];
    z[54]= - z[14] + z[48];
    z[54]=z[2]*z[54];
    z[54]= - z[35] + z[54];
    z[54]=z[2]*z[54]*z[82];
    z[56]=z[2]*z[6];
    z[80]=z[56] - 1;
    z[87]=z[80]*z[2];
    z[88]=z[87] + z[14];
    z[89]=z[88]*z[2];
    z[90]=z[89] - z[35];
    z[91]= - z[5]*z[90];
    z[92]=2*z[14];
    z[91]=z[91] - z[92] + z[2];
    z[93]=90*z[4];
    z[91]=z[91]*z[93];
    z[88]= - z[5]*z[88];
    z[88]=static_cast<T>(1)+ z[88];
    z[54]=z[91] + n<T>(173,3)*z[88] + z[54];
    z[88]=2*z[4];
    z[54]=z[54]*z[88];
    z[91]=5*z[14] - 8*z[2];
    z[91]=z[2]*z[91];
    z[91]= - z[35] + z[91];
    z[94]=z[5]*z[2];
    z[91]=z[91]*z[94];
    z[45]= - 18*z[45] + z[91];
    z[45]=z[45]*z[82];
    z[80]=z[5]*z[80];
    z[45]=z[54] - 7*z[80] + z[45];
    z[45]=z[4]*z[45];
    z[54]=z[86] - 152*z[49];
    z[44]=z[45] + n<T>(1,3)*z[54] + 540*z[44];
    z[44]=z[4]*z[44];
    z[19]=4*z[19] + z[31] + z[44] + 2*z[20] + z[41];
    z[19]=z[1]*z[19];
    z[20]= - z[17]*z[53];
    z[20]=z[20] + n<T>(20,3)*z[26] + z[43];
    z[20]=z[13]*z[20];
    z[31]=z[18]*z[53];
    z[31]= - z[78] + z[31];
    z[41]=3*z[12];
    z[31]=z[31]*z[41];
    z[43]=z[26]*z[11];
    z[20]=z[31] - n<T>(20,3)*z[43] + z[20];
    z[31]= - 18*z[18] + 19*z[17];
    z[31]=z[6]*z[31];
    z[31]=z[31] - 19*z[72] + 6*z[78];
    z[31]=z[5]*z[31];
    z[20]=2*z[20] + z[31];
    z[20]=z[7]*z[20];
    z[31]=z[76] - z[63];
    z[44]=z[2]*z[31];
    z[45]=28*z[8];
    z[44]=z[68] + 21*z[44] - z[45] + 11*z[6] + 24*z[18] + 19*z[11];
    z[44]=z[5]*z[44];
    z[54]=4*z[2];
    z[63]=z[54]*z[31];
    z[76]=13*z[17];
    z[63]=z[63] - n<T>(13,3)*z[11] - z[76] - n<T>(20,3)*z[16];
    z[63]=z[11]*z[63];
    z[78]=n<T>(2,3)*z[16];
    z[76]=n<T>(8,3)*z[6] - 8*z[13] + z[76] - z[78];
    z[76]=z[6]*z[76];
    z[78]=z[15] + z[78];
    z[75]=n<T>(8,3)*z[8] - z[75] + 17*z[78] + n<T>(20,3)*z[11];
    z[75]=z[8]*z[75];
    z[78]=z[57] - 6*z[6] - 13*z[13] - 8*z[18] + 15*z[11];
    z[78]=z[78]*z[41];
    z[80]=n<T>(17,3)*z[13] - 4*z[11] - 17*z[15] + n<T>(40,3)*z[16];
    z[80]=z[13]*z[80];
    z[20]=z[20] + z[44] + z[78] + z[75] + z[76] + z[80] + z[63];
    z[44]=5*z[17];
    z[63]=3*z[15];
    z[75]=z[44] - z[63];
    z[75]=z[6]*z[75];
    z[44]= - z[44] + 9*z[16];
    z[44]=z[11]*z[44];
    z[44]=z[75] - n<T>(13,3)*z[26] + z[44];
    z[44]=z[8]*z[44];
    z[28]=z[72] - z[28];
    z[28]= - z[31] + 9*z[28];
    z[28]=z[12]*z[28];
    z[31]=z[63] - n<T>(25,3)*z[16];
    z[31]=z[13]*z[31];
    z[31]= - z[47] + z[31];
    z[31]=z[6]*z[31];
    z[47]=z[7]*z[33];
    z[63]=11*z[43];
    z[72]=z[26]*z[13];
    z[28]=n<T>(52,3)*z[47] + z[28] + z[44] + z[31] + z[63] - n<T>(19,3)*z[72];
    z[31]=z[34]*z[10];
    z[34]=3*z[13];
    z[44]=z[34] + z[12];
    z[44]=z[44]*z[69];
    z[44]= - n<T>(430,3)*z[21] + z[44];
    z[44]=z[44]*z[25];
    z[47]= - z[11] + z[41];
    z[47]=z[47]*z[66];
    z[47]=n<T>(1118,3)*z[27] + z[47];
    z[47]=z[47]*z[30];
    z[44]= - 3*z[31] + z[47] - z[83] + z[44];
    z[44]=z[10]*z[44];
    z[47]= - 2*z[79] - z[23];
    z[66]=26*z[22] + 53*z[72];
    z[66]=z[6]*z[66];
    z[47]=26*z[47] + z[66];
    z[63]=n<T>(52,3)*z[22] - z[63];
    z[63]=z[8]*z[63];
    z[44]=z[44] + n<T>(1,3)*z[47] + z[63];
    z[47]=3*z[84];
    z[63]= - static_cast<T>(1)- z[47];
    z[63]=z[63]*z[77];
    z[66]=19*z[8];
    z[69]=n<T>(316,3)*z[13] - z[66];
    z[63]=z[63] + 2*z[69] + n<T>(259,3)*z[12];
    z[63]=z[63]*z[85];
    z[69]=npow(z[3],2);
    z[75]=76*z[69];
    z[63]=z[63] - 593*z[21] - z[75];
    z[63]=z[9]*z[63];
    z[24]=z[24]*z[75];
    z[24]=z[24] + z[63];
    z[24]=z[9]*z[24];
    z[63]= - 713*z[11] + 1696*z[12];
    z[63]=n<T>(1,3)*z[63] - z[93];
    z[63]=z[63]*z[88];
    z[63]=z[63] + n<T>(4577,3)*z[27] - 360*z[69];
    z[63]=z[4]*z[63];
    z[75]=z[57] - z[60] - z[6];
    z[76]=90*z[69];
    z[75]=z[75]*z[76];
    z[63]=z[75] + z[63];
    z[63]=z[4]*z[63];
    z[75]=z[69]*z[12];
    z[78]=225*z[11] - 76*z[13];
    z[78]=z[78]*z[75];
    z[24]=z[63] + z[24] + z[78] + 4*z[44];
    z[24]=z[10]*z[24];
    z[44]=38*z[69];
    z[63]=z[14] + z[62];
    z[63]=z[63]*z[44];
    z[78]=z[77]*z[7];
    z[80]=9*z[84];
    z[82]=static_cast<T>(1)- z[80];
    z[82]=z[82]*z[78];
    z[86]= - static_cast<T>(145)- 632*z[84];
    z[82]=n<T>(1,3)*z[86] + z[82];
    z[82]=z[82]*z[85];
    z[63]=z[82] + z[63] + 161*z[12] + 273*z[13] - n<T>(76,3)*z[8];
    z[63]=z[9]*z[63];
    z[82]=z[69]*z[84];
    z[82]= - 17*z[21] - 4*z[82];
    z[63]=19*z[82] + z[63];
    z[63]=z[9]*z[63];
    z[54]= - z[14] - z[54];
    z[54]=z[54]*z[76];
    z[82]=z[14]*z[81];
    z[82]= - n<T>(713,3) + z[82];
    z[82]=z[82]*z[88];
    z[54]=z[82] + z[54] - 443*z[11] + 755*z[12];
    z[54]=z[4]*z[54];
    z[82]= - z[11] + z[53];
    z[82]=z[2]*z[82]*z[69];
    z[54]=z[54] + 641*z[27] + 180*z[82];
    z[54]=z[4]*z[54];
    z[82]=45*z[11] + 19*z[13];
    z[75]=z[7]*z[82]*z[75];
    z[24]=z[24] + z[54] + z[63] + 4*z[28] + z[75];
    z[24]=z[10]*z[24];
    z[28]=z[5] - z[74];
    z[28]=z[28]*z[51];
    z[28]=static_cast<T>(5)+ z[28];
    z[28]=z[28]*z[40];
    z[54]= - 202*z[39] - 461*z[8] + 145*z[5];
    z[54]=z[54]*z[62];
    z[54]=static_cast<T>(145)+ z[54];
    z[62]=n<T>(1,3)*z[7];
    z[54]=z[54]*z[62];
    z[28]=z[54] + z[28];
    z[28]=z[28]*z[85];
    z[54]= - 3*z[14] + 16*z[7];
    z[54]=z[7]*z[54];
    z[54]= - z[35] + z[54];
    z[54]=z[54]*z[44];
    z[63]= - n<T>(215,3)*z[39] - 97*z[8] + n<T>(107,3)*z[5];
    z[63]=z[7]*z[63];
    z[28]=z[28] + z[54] - n<T>(79,3) + z[63];
    z[28]=z[9]*z[28];
    z[54]=11*z[5];
    z[63]= - z[70] + z[54];
    z[63]=z[7]*z[63];
    z[63]= - 6*z[36] + z[63];
    z[63]=z[7]*z[63];
    z[52]=z[52] + z[63];
    z[44]=z[52]*z[44];
    z[52]=n<T>(19,3)*z[8];
    z[63]=35*z[12] + 44*z[13] - z[52];
    z[28]=z[28] + z[44] - n<T>(61,3)*z[39] + 2*z[63] + n<T>(7,3)*z[5];
    z[28]=z[9]*z[28];
    z[44]=z[42]*z[6];
    z[63]=z[14] - z[44];
    z[55]=z[63]*z[55];
    z[63]=z[90]*z[94];
    z[55]=3*z[63] - z[35] + z[55];
    z[55]=z[55]*z[93];
    z[63]= - static_cast<T>(45)- n<T>(443,3)*z[56];
    z[46]=z[63]*z[46];
    z[63]= - 45*z[35] + n<T>(578,3)*z[89];
    z[63]=z[63]*z[68];
    z[46]=z[55] + z[63] + n<T>(983,3)*z[14] + z[46];
    z[46]=z[46]*z[88];
    z[55]=12*z[14] + 29*z[87];
    z[54]=z[55]*z[54];
    z[55]= - z[14] + 9*z[2];
    z[55]=z[2]*z[55];
    z[55]= - z[35] + z[55];
    z[55]=z[55]*z[76];
    z[46]=z[46] + z[55] + z[54] - n<T>(713,3) - 139*z[56];
    z[46]=z[4]*z[46];
    z[54]=z[92] - z[48];
    z[54]=z[54]*z[94];
    z[44]= - 11*z[44] + z[54];
    z[44]=z[44]*z[76];
    z[54]= - static_cast<T>(66)+ 149*z[56];
    z[54]=z[5]*z[54];
    z[44]=z[46] + z[44] + z[54] + 156*z[12] - 90*z[11] - 59*z[6];
    z[44]=z[4]*z[44];
    z[46]=z[42]*z[49];
    z[49]=45*z[6] + z[66];
    z[49]=z[49]*z[94];
    z[49]=z[49] - 76*z[39];
    z[49]=z[7]*z[49];
    z[46]=225*z[46] + z[49];
    z[46]=z[46]*z[69];
    z[19]=z[19] + z[24] + z[44] + z[28] + 2*z[20] + z[46];
    z[19]=z[1]*z[19];
    z[20]=z[34] + z[57];
    z[20]=z[20]*z[77];
    z[20]= - n<T>(943,3)*z[21] + z[20];
    z[20]=z[20]*z[25];
    z[24]= - z[73] + z[41];
    z[24]=z[24]*z[93];
    z[24]=n<T>(2333,3)*z[27] + z[24];
    z[24]=z[24]*z[30];
    z[20]= - 6*z[31] + z[24] - z[83] + z[20];
    z[20]=z[10]*z[20];
    z[24]=z[22] + 2*z[72];
    z[24]=z[6]*z[24];
    z[23]=z[24] - z[79] - z[23];
    z[24]=20*z[43];
    z[25]=13*z[22] - z[24];
    z[25]=z[8]*z[25];
    z[23]=13*z[23] + z[25];
    z[25]= - z[13]*z[51];
    z[25]=z[25] - static_cast<T>(2)- z[32];
    z[25]=z[25]*z[77];
    z[28]=601*z[13] + 487*z[12];
    z[25]=n<T>(1,3)*z[28] + z[25];
    z[25]=z[9]*z[25];
    z[25]= - 368*z[21] + z[25];
    z[25]=z[25]*z[50];
    z[28]= - z[38] - static_cast<T>(2)+ 3*z[32];
    z[28]=z[28]*z[93];
    z[28]=n<T>(1523,3)*z[29] + z[28];
    z[28]=z[4]*z[28];
    z[28]=n<T>(2704,3)*z[27] + z[28];
    z[28]=z[28]*z[59];
    z[20]=z[20] + z[28] + n<T>(4,3)*z[23] + z[25];
    z[20]=z[10]*z[20];
    z[23]=z[13]*z[16];
    z[25]= - 13*z[26] - 25*z[23];
    z[25]=z[6]*z[25];
    z[24]=z[25] + z[24] + 13*z[72];
    z[25]=z[8] - z[6];
    z[22]= - z[22]*z[25];
    z[22]=z[33] + z[22];
    z[22]=z[7]*z[22];
    z[28]=z[16]*z[58];
    z[28]= - n<T>(10,3)*z[26] + z[28];
    z[28]=z[28]*z[61];
    z[22]=n<T>(13,3)*z[22] + n<T>(1,3)*z[24] + z[28];
    z[24]=static_cast<T>(2)+ z[47];
    z[24]=z[7]*z[24];
    z[24]=z[14] + z[24];
    z[24]=z[24]*z[77];
    z[28]= - static_cast<T>(1)- z[32];
    z[29]= - n<T>(487,3)*z[13] + 76*z[8];
    z[29]=z[7]*z[29];
    z[24]=z[24] + n<T>(259,3)*z[28] + z[29];
    z[24]=z[9]*z[24];
    z[24]=z[24] + n<T>(389,3)*z[12] + n<T>(446,3)*z[13] + z[66];
    z[24]=z[9]*z[24];
    z[21]= - 238*z[21] + z[24];
    z[21]=z[9]*z[21];
    z[24]= - 713*z[38] - static_cast<T>(983)+ 1253*z[32];
    z[28]= - z[92] - z[2];
    z[28]=z[28]*z[93];
    z[24]=n<T>(1,3)*z[24] + z[28];
    z[24]=z[4]*z[24];
    z[28]= - 812*z[11] + 523*z[12];
    z[24]=n<T>(2,3)*z[28] + z[24];
    z[24]=z[4]*z[24];
    z[24]=n<T>(1622,3)*z[27] + z[24];
    z[24]=z[4]*z[24];
    z[20]=z[20] + z[24] + 4*z[22] + z[21];
    z[20]=z[20]*z[65];
    z[21]=z[11]*z[16];
    z[22]= - 13*z[21] - 7*z[23];
    z[24]= - 8*z[11] + 13*z[16];
    z[24]=z[24]*z[67];
    z[27]=n<T>(7,3)*z[16] + z[34];
    z[27]=z[6]*z[27];
    z[22]=z[24] + n<T>(1,3)*z[22] + z[27];
    z[24]=z[26]*z[25];
    z[24]= - z[43] + z[72] + z[24];
    z[24]=z[7]*z[24];
    z[25]= - static_cast<T>(2)+ z[80];
    z[25]=z[7]*z[25];
    z[25]= - z[14] + z[25];
    z[25]=z[25]*z[78];
    z[26]=static_cast<T>(31)+ 1171*z[84];
    z[26]=z[7]*z[26];
    z[26]=145*z[14] + z[26];
    z[25]=n<T>(1,3)*z[26] + z[25];
    z[25]=z[9]*z[25];
    z[26]= - n<T>(2,3) - z[32];
    z[27]= - 75*z[13] + n<T>(437,3)*z[8];
    z[27]=z[7]*z[27];
    z[25]=z[25] + 65*z[26] + z[27];
    z[25]=z[9]*z[25];
    z[25]=z[25] + 50*z[12] + 64*z[13] + z[52];
    z[25]=z[25]*z[85];
    z[26]= - z[42]*z[53];
    z[26]= - z[14] + z[26];
    z[26]=z[2]*z[26];
    z[26]=z[35] + z[26];
    z[26]=z[26]*z[93];
    z[27]= - static_cast<T>(443)- 1253*z[56];
    z[27]=z[2]*z[27];
    z[27]= - 983*z[14] + z[27];
    z[26]=n<T>(1,3)*z[27] + z[26];
    z[26]=z[4]*z[26];
    z[27]= - n<T>(481,3)*z[11] - 201*z[6];
    z[27]=z[2]*z[27];
    z[26]=z[26] + z[27] - n<T>(506,3) + 201*z[32];
    z[26]=z[4]*z[26];
    z[26]=z[26] + 102*z[12] - n<T>(713,3)*z[11] - 33*z[6];
    z[26]=z[26]*z[88];
    z[27]=105*z[11] - 46*z[13];
    z[27]=z[27]*z[41];
    z[20]=z[20] + z[26] + z[25] + n<T>(80,3)*z[24] + 8*z[22] + z[27];
    z[20]=z[10]*z[20];
    z[21]=z[21] - z[23];
    z[22]=n<T>(5,3)*z[16];
    z[23]=z[22] + z[13];
    z[23]=z[6]*z[23];
    z[22]= - z[8]*z[22];
    z[21]=z[22] + n<T>(5,3)*z[21] + z[23];
    z[22]=z[60] + z[34];
    z[22]=z[12]*z[22];
    z[23]=12*z[6] - 23*z[8];
    z[23]=z[23]*z[68];
    z[21]=z[23] + 8*z[21] + 9*z[22];
    z[21]=z[7]*z[21];
    z[22]= - z[68] + z[74];
    z[22]=z[22]*z[51];
    z[22]=z[22] - static_cast<T>(10)+ 3*z[36];
    z[22]=z[7]*z[22];
    z[22]=z[14] + z[22];
    z[22]=z[22]*z[40];
    z[23]=829*z[8] - 601*z[5];
    z[23]=2*z[23] + 715*z[39];
    z[23]=z[7]*z[23];
    z[23]=z[23] - static_cast<T>(1285)+ 487*z[36];
    z[23]=z[7]*z[23];
    z[23]= - 31*z[14] + z[23];
    z[23]=z[23]*z[62];
    z[22]=z[23] + z[22];
    z[22]=z[9]*z[22];
    z[23]=137*z[39] + 232*z[8] - 175*z[5];
    z[23]=z[7]*z[23];
    z[23]=n<T>(4,3)*z[23] - static_cast<T>(157)+ n<T>(209,3)*z[36];
    z[23]=z[7]*z[23];
    z[22]=z[22] + n<T>(50,3)*z[14] + z[23];
    z[22]=z[9]*z[22];
    z[23]=n<T>(106,3)*z[39] - 9*z[13] - n<T>(98,3)*z[37];
    z[23]=z[7]*z[23];
    z[22]=z[22] + z[23] + n<T>(19,3)*z[36] - n<T>(20,3) - 23*z[32];
    z[22]=z[22]*z[85];
    z[23]= - z[90]*z[42]*z[71];
    z[24]= - static_cast<T>(1)+ 2*z[56];
    z[24]=z[24]*z[42];
    z[24]=2*z[35] + 3*z[24];
    z[24]=z[2]*z[24];
    z[23]=z[24] + z[23];
    z[23]=z[23]*z[93];
    z[24]=180*z[35] - n<T>(1793,3)*z[89];
    z[24]=z[24]*z[94];
    z[25]= - static_cast<T>(1253)+ 3046*z[56];
    z[25]=z[2]*z[25];
    z[25]= - 713*z[14] + z[25];
    z[25]=z[2]*z[25];
    z[23]=z[23] + z[24] + 90*z[35] + n<T>(1,3)*z[25];
    z[23]=z[4]*z[23];
    z[24]= - 97*z[14] - 172*z[87];
    z[24]=z[2]*z[24];
    z[24]=15*z[35] + z[24];
    z[24]=z[24]*z[71];
    z[25]= - n<T>(253,3) + 336*z[56];
    z[25]=z[2]*z[25];
    z[25]= - 78*z[14] + z[25];
    z[23]=z[23] + 2*z[25] + z[24];
    z[23]=z[4]*z[23];
    z[24]=static_cast<T>(11)- 14*z[56];
    z[24]=z[24]*z[48];
    z[24]= - 11*z[14] + z[24];
    z[24]=z[24]*z[71];
    z[25]= - static_cast<T>(4)+ 11*z[32];
    z[26]= - n<T>(173,3)*z[11] + 198*z[6];
    z[26]=z[2]*z[26];
    z[23]=z[23] + z[24] + 3*z[25] + z[26];
    z[23]=z[23]*z[88];
    z[24]= - 143*z[8] + 142*z[6] - 238*z[11] + 11*z[13];
    z[25]=z[8]*z[38];
    z[26]= - 39*z[6] + z[45];
    z[26]=z[2]*z[26];
    z[26]=static_cast<T>(34)+ z[26];
    z[26]=z[5]*z[26];
    z[19]=z[19] + z[20] + z[23] + z[22] + z[21] + z[26] + n<T>(16,3)*z[25]
    + n<T>(1,3)*z[24] + 32*z[12];
    z[19]=z[1]*z[19];
    z[20]= - 19*z[14] - 77*z[2];
    z[20]=z[20]*z[68];
    z[21]=38*z[5] - 15*z[6] - z[66];
    z[21]=z[21]*z[51];
    z[22]= - z[14] + 6*z[7];
    z[22]=z[22]*z[77];
    z[23]= - z[58] + z[64];
    z[23]= - z[81] + 45*z[23] + 64*z[12];
    z[23]=z[10]*z[23];
    z[24]=z[4]*z[2];

    r += static_cast<T>(64)+ z[19] + z[20] + z[21] + z[22] + z[23] + 360*z[24] - 315*
      z[56];
 
    return r;
}

template double qqb_2lNLC_r1237(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1237(const std::array<dd_real,31>&);
#endif
