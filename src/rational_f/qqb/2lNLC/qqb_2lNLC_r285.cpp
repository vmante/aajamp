#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r285(const std::array<T,31>& k) {
  T z[124];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[15];
    z[11]=k[5];
    z[12]=k[9];
    z[13]=k[23];
    z[14]=k[18];
    z[15]=k[8];
    z[16]=k[24];
    z[17]=k[2];
    z[18]=npow(z[12],2);
    z[19]=z[18]*z[8];
    z[20]=2*z[12];
    z[21]=z[19] - z[20] + z[14];
    z[22]=2*z[8];
    z[21]=z[21]*z[22];
    z[23]=z[22]*z[18];
    z[24]=3*z[12];
    z[25]=z[24] - z[23];
    z[25]=z[8]*z[25];
    z[25]=n<T>(52,9) + z[25];
    z[26]=z[3]*z[8];
    z[25]=z[25]*z[26];
    z[27]=z[4]*z[9];
    z[28]=5*z[3];
    z[29]= - z[13] + z[28];
    z[29]=z[9]*z[29];
    z[21]= - z[27] + z[29] + z[25] + n<T>(11,3) + z[21];
    z[25]=npow(z[8],2);
    z[29]=z[25]*z[3];
    z[30]=z[22]*z[14];
    z[31]=n<T>(349,18) - z[30];
    z[31]=z[8]*z[31];
    z[32]=static_cast<T>(313)- 295*z[26];
    z[32]=z[9]*z[32];
    z[31]=n<T>(1,18)*z[32] + z[31] - n<T>(277,18)*z[29];
    z[31]=z[7]*z[31];
    z[32]=npow(z[9],2);
    z[33]=z[32]*z[4];
    z[34]=z[9]*z[3];
    z[35]=static_cast<T>(1)+ z[34];
    z[35]=z[9]*z[35];
    z[35]=6*z[35] - z[33];
    z[35]=z[6]*z[35];
    z[21]=z[31] + 2*z[21] + z[35];
    z[21]=z[7]*z[21];
    z[31]=npow(z[14],2);
    z[35]=3*z[31];
    z[36]=z[35] - n<T>(1,3)*z[18];
    z[36]=z[36]*z[22];
    z[37]=17*z[12] + z[23];
    z[37]=z[8]*z[37];
    z[37]=n<T>(277,6) + z[37];
    z[37]=z[3]*z[37];
    z[38]=npow(z[13],2);
    z[39]=2*z[38];
    z[40]= - z[14] + z[3];
    z[40]=z[10]*z[40];
    z[40]=z[39] + z[40];
    z[41]=2*z[9];
    z[40]=z[40]*z[41];
    z[42]=3*z[4];
    z[43]=z[42]*z[9];
    z[44]=z[9]*z[10];
    z[45]= - z[43] - n<T>(67,6) + z[44];
    z[45]=z[4]*z[45];
    z[46]=static_cast<T>(2)- n<T>(5,2)*z[34];
    z[46]=5*z[46] + n<T>(23,2)*z[27];
    z[46]=z[6]*z[46];
    z[47]=22*z[15];
    z[48]=7*z[14];
    z[49]=2*z[10];
    z[50]=n<T>(7,3)*z[12];
    z[51]= - z[50] + 9*z[13];
    z[52]=2*z[13];
    z[53]= - n<T>(175,18)*z[3] + z[52] + z[14];
    z[53]=z[9]*z[53];
    z[53]= - n<T>(631,18) + z[53];
    z[53]=z[5]*z[53];
    z[21]=z[21] + z[46] + n<T>(22,3)*z[2] + z[53] + z[45] + z[40] + z[49] + 
   n<T>(1,3)*z[37] + z[47] + z[36] + 2*z[51] - z[48];
    z[21]=z[7]*z[21];
    z[36]=n<T>(241,18)*z[3];
    z[37]=3*z[15];
    z[40]=3*z[34];
    z[45]=static_cast<T>(5)+ z[40];
    z[45]=z[5]*z[45];
    z[45]=n<T>(1,2)*z[45] - n<T>(187,18)*z[4] + z[37] - z[36];
    z[45]=z[5]*z[45];
    z[46]=npow(z[10],2);
    z[51]=z[34]*z[46];
    z[53]=2*z[51];
    z[54]=static_cast<T>(295)+ 313*z[26];
    z[55]=z[4]*z[54];
    z[55]= - 313*z[3] + z[55];
    z[56]=static_cast<T>(304)+ n<T>(313,2)*z[34];
    z[56]=z[5]*z[56];
    z[55]=n<T>(1,2)*z[55] + z[56];
    z[56]=n<T>(1,9)*z[2];
    z[55]=z[55]*z[56];
    z[57]=z[4]*z[3];
    z[58]= - z[3] + z[49];
    z[58]=z[10]*z[58];
    z[45]=z[55] + z[45] - z[57] + z[58] + z[53];
    z[45]=z[2]*z[45];
    z[55]=npow(z[3],2);
    z[58]=4*z[55];
    z[59]=npow(z[15],2);
    z[60]=z[58] - z[59];
    z[61]=z[10]*z[3];
    z[62]=n<T>(152,9)*z[3] + z[4];
    z[62]=z[4]*z[62];
    z[63]= - z[5]*z[42];
    z[62]=z[63] + z[62] + z[61] + z[60];
    z[62]=z[5]*z[62];
    z[58]=z[58] - z[57];
    z[63]=z[58]*z[4];
    z[45]=z[45] + z[63] + z[62];
    z[45]=z[2]*z[45];
    z[62]=3*z[3];
    z[64]= - z[52] - z[62];
    z[64]=z[10]*z[64];
    z[65]=2*z[5];
    z[66]=z[65]*z[3];
    z[67]=4*z[38];
    z[64]= - z[66] + z[67] + z[64];
    z[64]=z[5]*z[64];
    z[68]=z[10]*z[14];
    z[69]=2*z[31];
    z[70]=z[68] - z[69];
    z[71]=z[3]*z[13];
    z[72]=z[71] + z[70];
    z[72]=z[10]*z[72];
    z[73]=z[46]*z[4];
    z[72]=z[72] - z[73];
    z[74]=2*z[4];
    z[75]=z[2]*z[4];
    z[76]=z[8]*z[75];
    z[76]= - z[74] + z[76];
    z[76]=z[2]*z[76];
    z[61]=z[61] + z[76];
    z[61]=z[2]*z[61];
    z[76]=z[6]*z[5];
    z[77]=z[13] - z[12];
    z[78]=6*z[77];
    z[79]= - z[78] - z[3];
    z[79]=z[79]*z[76];
    z[61]=z[79] + z[61] + 2*z[72] + z[64];
    z[61]=z[6]*z[61];
    z[64]= - static_cast<T>(1)+ z[34];
    z[64]=z[64]*z[65];
    z[64]= - z[36] + z[64];
    z[64]=z[5]*z[64];
    z[72]=z[5] - z[6];
    z[79]= - static_cast<T>(277)+ 295*z[34];
    z[72]=z[7]*z[79]*z[72];
    z[36]=z[2] + n<T>(215,9)*z[5] + z[36] - z[4];
    z[36]=z[6]*z[36];
    z[36]=n<T>(1,18)*z[72] + z[36] + z[64] - z[75];
    z[36]=z[7]*z[36];
    z[64]=2*z[2];
    z[72]=z[42] - z[64];
    z[72]=z[2]*z[72];
    z[79]=n<T>(61,18)*z[3] + z[5];
    z[79]=z[5]*z[79];
    z[80]= - z[3] + z[5];
    z[80]=z[6]*z[80];
    z[72]=z[80] + z[79] + z[72];
    z[72]=z[6]*z[72];
    z[79]= - z[74] - z[14] + z[62];
    z[79]=z[10]*z[79];
    z[66]=z[66] + z[79];
    z[66]=z[5]*z[66];
    z[79]=npow(z[2],2);
    z[80]=z[79]*z[74];
    z[36]=z[36] + z[72] + z[66] + z[80];
    z[36]=z[7]*z[36];
    z[66]=npow(z[10],3);
    z[72]=z[66]*z[74];
    z[80]=z[49]*z[14];
    z[80]=z[80] - z[31];
    z[81]=z[80]*z[46];
    z[72]=z[72] - z[81];
    z[81]= - z[72]*z[65];
    z[82]=z[5]*z[4];
    z[83]=z[82]*z[2];
    z[84]=npow(z[5],2);
    z[85]=z[84]*z[4];
    z[86]= - z[85] - n<T>(295,18)*z[83];
    z[86]=z[86]*z[79];
    z[87]=z[18] - z[38];
    z[88]=4*z[87];
    z[89]=z[5]*npow(z[6],2)*z[88];
    z[90]=2*z[6];
    z[91]= - z[84]*z[90];
    z[92]=z[7]*z[76];
    z[91]=z[91] - n<T>(259,18)*z[92];
    z[92]=npow(z[7],2);
    z[91]=z[91]*z[92];
    z[81]=z[91] + z[89] + z[81] + z[86];
    z[81]=z[11]*z[81];
    z[86]=z[82]*z[3];
    z[63]= - z[63] + z[86];
    z[63]=z[5]*z[63];
    z[86]=n<T>(313,2)*z[3] - 304*z[4];
    z[86]=z[5]*z[86];
    z[86]=n<T>(313,2)*z[57] + z[86];
    z[56]=z[86]*z[56];
    z[86]=z[84]*z[3];
    z[56]= - z[86] + z[56];
    z[56]=z[2]*z[56];
    z[56]=z[63] + z[56];
    z[56]=z[2]*z[56];
    z[63]=z[62] - z[65];
    z[63]=z[5]*z[63];
    z[63]=z[63] - z[75];
    z[63]=z[6]*z[63];
    z[89]=n<T>(295,2)*z[3];
    z[91]= - z[5]*z[89];
    z[89]=z[89] - 286*z[5];
    z[89]=z[6]*z[89];
    z[89]=z[91] + z[89];
    z[89]=z[7]*z[89];
    z[63]=z[63] + n<T>(1,9)*z[89];
    z[63]=z[7]*z[63];
    z[89]=z[4]*z[79];
    z[86]=z[86] + z[89];
    z[89]=z[76]*z[3];
    z[86]=2*z[86] + z[89];
    z[86]=z[6]*z[86];
    z[63]=z[86] + z[63];
    z[63]=z[7]*z[63];
    z[86]=z[84]*z[57];
    z[83]=z[3]*z[83];
    z[83]=z[86] - n<T>(313,18)*z[83];
    z[83]=z[83]*z[79];
    z[86]=npow(z[7],3);
    z[89]=z[86]*z[89];
    z[83]=z[83] - n<T>(295,18)*z[89];
    z[83]=z[1]*z[83];
    z[56]=z[83] + z[56] + z[63];
    z[56]=z[1]*z[56];
    z[63]=5*z[10];
    z[83]= - z[62] - z[63];
    z[83]=z[10]*z[83];
    z[58]=z[83] - z[58];
    z[58]=z[4]*z[58];
    z[83]=z[15]*z[14];
    z[89]=z[69] - z[83];
    z[89]=z[89]*z[15];
    z[71]= - z[31] + z[71];
    z[71]=2*z[71] + 5*z[68];
    z[71]=z[10]*z[71];
    z[58]=z[58] - z[89] + z[71];
    z[58]=z[5]*z[58];
    z[36]=z[56] + z[81] + z[36] + z[61] + z[58] + z[45];
    z[36]=z[1]*z[36];
    z[45]=z[46]*z[9];
    z[56]=3*z[45];
    z[58]=z[67]*z[8];
    z[61]=z[12] + z[13];
    z[61]=z[42] + z[56] - z[10] + 3*z[61] - z[58];
    z[61]=z[4]*z[61];
    z[71]=15*z[13];
    z[81]=7*z[3];
    z[91]=n<T>(9,2)*z[10];
    z[93]=static_cast<T>(2)+ z[34];
    z[93]=z[5]*z[93];
    z[93]=3*z[93] - z[91] - z[81] - n<T>(23,3)*z[12] - z[71];
    z[93]=z[5]*z[93];
    z[94]=z[2]*z[8];
    z[95]=4*z[4];
    z[96]= - z[8]*z[95];
    z[96]= - static_cast<T>(1)+ z[96];
    z[94]=2*z[96] - z[94];
    z[94]=z[2]*z[94];
    z[96]=6*z[15];
    z[97]=z[96] + z[3];
    z[98]=z[42]*z[8];
    z[99]=n<T>(17,2) - z[98];
    z[99]=z[4]*z[99];
    z[91]=z[94] + z[99] + 2*z[97] + z[91];
    z[91]=z[2]*z[91];
    z[94]= - z[77]*z[22];
    z[94]= - static_cast<T>(3)+ z[94];
    z[94]=z[4]*z[94];
    z[97]=z[2]*z[98];
    z[78]=z[97] - z[5] + z[78] + z[94];
    z[78]=z[6]*z[78];
    z[94]=z[52]*z[3];
    z[94]=z[94] - z[83];
    z[97]= - z[39] + z[31];
    z[98]=3*z[10];
    z[99]= - z[9]*z[70]*z[98];
    z[100]=2*z[14];
    z[101]=z[12] + z[100];
    z[101]=2*z[101] + z[3];
    z[101]=z[10]*z[101];
    z[61]=z[78] + z[91] + z[93] + z[61] + z[99] + z[101] + 2*z[97] - 
    z[94];
    z[61]=z[6]*z[61];
    z[78]=z[67] + z[35];
    z[91]=z[9]*z[89];
    z[40]= - static_cast<T>(7)+ z[40];
    z[40]=z[40]*z[82];
    z[93]=7*z[83];
    z[97]=3*z[68];
    z[99]=4*z[13];
    z[101]=z[3]*z[99];
    z[102]= - z[12] + z[28];
    z[102]=z[4] + 2*z[102] + n<T>(31,6)*z[10];
    z[102]=z[4]*z[102];
    z[40]=z[40] + z[102] + z[91] - z[97] + z[101] + 2*z[78] - z[93];
    z[40]=z[5]*z[40];
    z[78]=6*z[51];
    z[91]= - z[28] - 8*z[10];
    z[91]=z[10]*z[91];
    z[91]=z[91] - z[78];
    z[91]=z[9]*z[91];
    z[28]= - z[37] - z[28];
    z[28]=z[9]*z[28];
    z[28]=n<T>(115,18) + z[28];
    z[28]=z[5]*z[28];
    z[54]= - 313*z[34] - z[54];
    z[54]=z[2]*z[54];
    z[101]=n<T>(283,18) - z[26];
    z[101]=z[4]*z[101];
    z[28]=n<T>(1,18)*z[54] + z[28] + z[101] + z[91] - z[98] - z[37] + n<T>(277,18)*z[3];
    z[28]=z[2]*z[28];
    z[54]=n<T>(13,3)*z[51];
    z[91]=n<T>(7,3)*z[10];
    z[101]=z[91] + z[20];
    z[102]= - n<T>(11,6)*z[3] + z[101];
    z[102]=z[10]*z[102];
    z[103]= - static_cast<T>(3)+ z[26];
    z[103]=z[3]*z[103];
    z[104]= - z[4]*z[26];
    z[103]=4*z[103] + z[104];
    z[103]=z[4]*z[103];
    z[104]=z[59]*z[9];
    z[105]= - static_cast<T>(137)- n<T>(373,2)*z[34];
    z[105]=z[5]*z[105];
    z[105]=n<T>(1,9)*z[105] + n<T>(205,18)*z[4] + z[104] + n<T>(7,2)*z[10] - n<T>(107,18)
   *z[3] - z[12] + 7*z[15];
    z[105]=z[5]*z[105];
    z[28]=z[28] + z[105] + z[103] + z[54] + z[102] - z[60];
    z[28]=z[2]*z[28];
    z[60]=z[82]*z[46];
    z[102]= - 2*z[72] - z[60];
    z[102]=z[102]*z[65];
    z[103]=z[2]*z[15];
    z[105]=11*z[59];
    z[103]=z[103] + z[105];
    z[103]=z[103]*z[2];
    z[106]=npow(z[15],3);
    z[103]=z[103] - z[106];
    z[107]=z[103]*z[2];
    z[108]=z[69] + z[83];
    z[108]=z[108]*z[59];
    z[107]=z[107] + z[108];
    z[76]=z[87]*z[76];
    z[76]=8*z[76] + z[107];
    z[76]=z[6]*z[76];
    z[109]=2*z[59];
    z[110]=z[109] - z[38];
    z[111]=z[110]*z[95];
    z[112]=2*z[15];
    z[113]=z[112] + z[13];
    z[114]=z[113]*z[74];
    z[115]=z[2] - z[5];
    z[115]=z[115]*z[6];
    z[114]=z[114] - z[115];
    z[114]=z[7]*z[114];
    z[116]=z[79]*z[6];
    z[114]=z[114] + z[111] + z[116];
    z[114]=z[114]*z[92];
    z[86]=z[11]*z[86]*z[59];
    z[117]=z[86]*z[4];
    z[76]= - z[117] + z[114] + z[102] + z[76];
    z[76]=z[11]*z[76];
    z[102]=n<T>(23,3)*z[18];
    z[114]=z[102] - z[39];
    z[118]=z[5]*z[12];
    z[114]=2*z[114] + z[118];
    z[114]=z[5]*z[114];
    z[118]=9*z[59];
    z[119]=10*z[15];
    z[120]= - z[119] - z[2];
    z[120]=z[2]*z[120];
    z[120]= - z[118] + z[120];
    z[120]=z[2]*z[120];
    z[121]=z[77]*z[5];
    z[122]= - z[88] - 7*z[121];
    z[122]=z[6]*z[122];
    z[109]= - z[14]*z[109];
    z[109]=z[122] + z[120] + z[109] + z[114];
    z[109]=z[6]*z[109];
    z[114]=n<T>(25,3)*z[16];
    z[120]= - z[37] + z[114] - z[99];
    z[120]=z[4]*z[120];
    z[122]= - 25*z[16] + n<T>(161,3)*z[5];
    z[122]=n<T>(1,3)*z[122] - 8*z[2];
    z[122]=z[6]*z[122];
    z[123]=z[7]*z[5];
    z[120]=z[123] + z[122] + z[120] + n<T>(37,6)*z[75];
    z[120]=z[7]*z[120];
    z[122]=n<T>(10,3)*z[79] + z[118];
    z[122]=z[4]*z[122];
    z[79]=z[84] - n<T>(1,6)*z[79];
    z[79]=z[6]*z[79];
    z[123]= - z[10]*z[84];
    z[79]=z[120] + z[79] + z[123] + z[122];
    z[79]=z[7]*z[79];
    z[120]=z[31] + z[97];
    z[120]=z[120]*z[49];
    z[122]=z[82]*z[10];
    z[120]= - 8*z[122] + z[120] - n<T>(31,3)*z[73];
    z[120]=z[5]*z[120];
    z[82]= - 2*z[46] - n<T>(223,18)*z[82];
    z[82]=z[2]*z[82];
    z[82]= - 5*z[85] + z[82];
    z[82]=z[2]*z[82];
    z[76]=z[76] + z[79] + z[109] + z[82] + 6*z[72] + z[120];
    z[76]=z[11]*z[76];
    z[79]=z[14] + z[20] - 7*z[13];
    z[82]= - z[9]*z[67];
    z[85]= - static_cast<T>(2)- n<T>(31,18)*z[34];
    z[85]=z[5]*z[85];
    z[79]=z[85] + z[95] + z[82] + z[10] + 2*z[79] + n<T>(91,6)*z[3];
    z[79]=z[5]*z[79];
    z[62]=z[13] - z[62];
    z[62]=z[62]*z[41];
    z[62]=n<T>(179,9) + z[62];
    z[62]=z[5]*z[62];
    z[82]= - z[27] - n<T>(191,9) - z[34];
    z[82]=z[6]*z[82];
    z[85]=z[26] - z[34];
    z[109]=z[5]*z[9];
    z[85]= - z[109] - static_cast<T>(1)+ n<T>(295,18)*z[85];
    z[85]=z[7]*z[85];
    z[62]=z[85] + z[82] + z[62] + n<T>(223,18)*z[3] + z[4];
    z[62]=z[7]*z[62];
    z[82]=6*z[13];
    z[85]= - z[119] + z[114] - z[82];
    z[85]=z[4]*z[85];
    z[119]= - n<T>(9,2) - z[27];
    z[119]=z[119]*z[42];
    z[43]=z[34] + z[43];
    z[43]=z[6]*z[43];
    z[120]=z[114] - z[15];
    z[43]=z[43] + n<T>(2,3)*z[2] + n<T>(197,9)*z[5] + z[119] + n<T>(13,2)*z[3] + 
    z[120];
    z[43]=z[6]*z[43];
    z[119]= - z[20] - z[3];
    z[119]=z[10]*z[119];
    z[43]=z[62] + z[43] - n<T>(13,3)*z[75] + z[79] + z[119] + z[85];
    z[43]=z[7]*z[43];
    z[62]= - z[69] - z[68];
    z[62]=z[62]*z[49];
    z[55]=z[38] + z[55];
    z[75]= - z[12] + z[98];
    z[75]=z[10]*z[75];
    z[55]=2*z[55] + z[75];
    z[55]=2*z[55] - z[57];
    z[55]=z[4]*z[55];
    z[28]=z[36] + z[76] + z[43] + z[61] + z[28] + z[40] + z[55] + z[89]
    + z[62];
    z[28]=z[1]*z[28];
    z[36]=z[83] + z[31];
    z[36]= - z[36]*z[37];
    z[40]=z[102] - z[67];
    z[43]=z[12]*z[65];
    z[43]=z[43] + z[40];
    z[43]=z[5]*z[43];
    z[36]=z[36] + z[43];
    z[43]=5*z[2];
    z[55]= - 49*z[15] - z[43];
    z[55]=z[2]*z[55];
    z[55]= - 16*z[59] + z[55];
    z[55]=z[2]*z[55];
    z[57]= - z[88] - z[121];
    z[57]=z[6]*z[57];
    z[36]=z[57] + 2*z[36] + z[55];
    z[36]=z[6]*z[36];
    z[55]=8*z[13];
    z[57]= - z[55] - 9*z[15];
    z[57]=z[57]*z[42];
    z[61]= - z[4] - z[113];
    z[61]=z[7]*z[61];
    z[57]=2*z[61] + z[115] - 4*z[110] + z[57];
    z[57]=z[7]*z[57];
    z[61]=3*z[59];
    z[62]=z[39] + z[61];
    z[62]=z[62]*z[42];
    z[62]=z[62] - z[116];
    z[57]=2*z[62] + z[57];
    z[57]=z[7]*z[57];
    z[60]= - z[60] - z[72];
    z[60]=z[5]*z[60];
    z[62]=z[6]*z[87]*z[65];
    z[62]=z[62] + z[107];
    z[62]=z[6]*z[62];
    z[60]=z[60] + z[62];
    z[62]=8*z[4];
    z[75]=z[110]*z[62];
    z[76]=z[52] + 5*z[15];
    z[79]=z[76]*z[74];
    z[79]=z[59] + z[79];
    z[79]=z[7]*z[79];
    z[75]=z[75] + z[79];
    z[75]=z[75]*z[92];
    z[79]= - z[74]*z[86];
    z[60]=z[79] + 2*z[60] + z[75];
    z[60]=z[11]*z[60];
    z[72]=z[108] + z[72];
    z[75]=4*z[31];
    z[68]=z[75] + z[68];
    z[68]=z[10]*z[68];
    z[79]=z[95]*z[10];
    z[79]=z[79] - z[46];
    z[85]= - z[79]*z[65];
    z[68]=z[85] + z[68] - n<T>(4,3)*z[73];
    z[68]=z[5]*z[68];
    z[73]=z[103]*z[64];
    z[36]=z[60] + z[57] + z[36] + z[73] + 2*z[72] + z[68];
    z[36]=z[11]*z[36];
    z[57]=34*z[15];
    z[60]=n<T>(5,2)*z[10];
    z[68]=3*z[2];
    z[72]=z[68]*z[8];
    z[73]=n<T>(107,6) + z[72];
    z[73]=z[2]*z[73];
    z[73]=z[73] + z[57] + z[60];
    z[73]=z[2]*z[73];
    z[85]=z[88]*z[8];
    z[86]=z[68] - z[85] + z[77];
    z[86]=z[6]*z[86];
    z[40]= - 5*z[31] - z[40];
    z[87]=z[15] + z[12];
    z[88]= - z[87]*z[49];
    z[102]=10*z[5] - n<T>(17,3)*z[12] - z[60];
    z[102]=z[5]*z[102];
    z[40]=z[86] + z[73] + z[102] + z[88] + 2*z[40] + z[93];
    z[40]=z[6]*z[40];
    z[73]=z[10]*z[87];
    z[73]= - 6*z[59] + z[73];
    z[82]= - 40*z[15] + z[114] + z[82];
    z[82]=z[4]*z[82];
    z[86]= - z[64] - n<T>(17,6)*z[4] + z[20] - z[10];
    z[86]=z[2]*z[86];
    z[87]=z[10] + z[74];
    z[87]=z[5]*z[87];
    z[88]=n<T>(55,6)*z[2] - n<T>(9,2)*z[5] + z[120];
    z[88]=z[6]*z[88];
    z[93]=z[52] + z[15];
    z[93]= - z[7] - z[5] + 2*z[93] - n<T>(7,6)*z[4];
    z[93]=z[7]*z[93];
    z[73]=z[93] + z[88] + z[86] + z[87] + 2*z[73] + z[82];
    z[73]=z[7]*z[73];
    z[43]= - z[43] + 4*z[45] - 35*z[15] + z[98];
    z[43]=z[2]*z[43];
    z[82]= - z[10]*z[101];
    z[86]= - z[20] + 15*z[15];
    z[86]=z[15]*z[86];
    z[60]=z[60] + 11*z[4];
    z[60]=z[5]*z[60];
    z[43]=z[43] + z[60] + z[86] + z[82];
    z[43]=z[2]*z[43];
    z[60]=z[15]*z[12];
    z[60]=z[39] + z[60];
    z[66]=z[66]*z[9];
    z[82]=z[20] + n<T>(5,3)*z[10];
    z[82]=z[10]*z[82];
    z[60]= - 12*z[66] + 2*z[60] + z[82];
    z[60]=z[4]*z[60];
    z[82]=n<T>(11,3)*z[18];
    z[86]=z[82] - z[31];
    z[87]= - z[39] + z[86];
    z[63]=z[63] + 4*z[12];
    z[62]= - z[62] + z[63];
    z[62]=z[5]*z[62];
    z[88]=n<T>(1,3)*z[10];
    z[93]=n<T>(62,3)*z[4] - 8*z[14] + z[88];
    z[93]=z[10]*z[93];
    z[62]=z[62] + 2*z[87] + z[93];
    z[62]=z[5]*z[62];
    z[87]=8*z[31];
    z[93]=z[87] + 3*z[83];
    z[93]=z[93]*z[15];
    z[97]= - z[87] + z[97];
    z[97]=z[10]*z[97];
    z[101]=z[80]*z[45];
    z[36]=z[36] + z[73] + z[40] + z[43] + z[62] + z[60] + 6*z[101] - 
    z[93] + z[97];
    z[36]=z[11]*z[36];
    z[40]=z[70]*z[44];
    z[43]= - z[100] - z[3];
    z[43]=z[10]*z[43];
    z[40]=z[40] - 10*z[31] + z[43];
    z[40]=z[9]*z[40];
    z[43]=z[8]*z[12];
    z[60]= - 4*z[26] - n<T>(44,3) - z[43];
    z[62]=z[74]*z[25];
    z[70]=5*z[8] - z[62];
    z[70]=z[70]*z[68];
    z[73]=z[4]*z[8];
    z[60]=z[70] + 2*z[60] + n<T>(1,2)*z[73];
    z[60]=z[2]*z[60];
    z[70]=z[50] + z[52];
    z[32]= - z[46]*z[32];
    z[97]= - z[58] + 5*z[12] + z[13];
    z[97]=z[8]*z[97];
    z[32]=z[32] - n<T>(19,2) + z[97];
    z[32]=z[4]*z[32];
    z[34]=2*z[34];
    z[97]=4*z[109] + static_cast<T>(1)+ z[34];
    z[97]=z[5]*z[97];
    z[62]= - z[62] + 8*z[8];
    z[62]=z[77]*z[62];
    z[62]= - z[72] + static_cast<T>(1)+ z[62];
    z[62]=z[6]*z[62];
    z[101]=z[49] + z[14];
    z[102]=z[8]*z[55];
    z[102]=n<T>(27,2) + z[102];
    z[102]=z[3]*z[102];
    z[32]=z[62] + z[60] + z[97] + z[32] + z[40] + z[102] + 2*z[70] - 
    z[101];
    z[32]=z[6]*z[32];
    z[40]=13*z[3] + 10*z[10];
    z[40]=z[10]*z[40];
    z[40]=z[40] + z[78];
    z[40]=z[9]*z[40];
    z[60]=z[15] + n<T>(5,2)*z[3];
    z[40]=z[40] + 3*z[60] + 13*z[10];
    z[40]=z[9]*z[40];
    z[60]= - z[22] + z[29];
    z[60]=z[4]*z[60];
    z[62]=3*z[8];
    z[70]=static_cast<T>(1)- z[26];
    z[70]=z[9]*z[70];
    z[70]=z[62] + n<T>(313,18)*z[70];
    z[70]=z[2]*z[70];
    z[78]=static_cast<T>(17)+ n<T>(457,9)*z[26];
    z[40]=z[70] + z[60] + n<T>(1,2)*z[78] + z[40];
    z[40]=z[2]*z[40];
    z[60]= - n<T>(8,3)*z[3] - 9*z[10];
    z[60]=z[10]*z[60];
    z[51]= - n<T>(26,3)*z[51] - z[59] + z[60];
    z[51]=z[9]*z[51];
    z[60]= - z[12] - z[96];
    z[70]= - n<T>(125,6) + 6*z[26];
    z[70]=z[3]*z[70];
    z[78]=z[3]*z[22];
    z[78]=n<T>(17,2) + z[78];
    z[78]=z[4]*z[78];
    z[97]= - z[15] - n<T>(85,18)*z[3];
    z[97]=z[9]*z[97];
    z[97]=n<T>(161,6) + z[97];
    z[97]=z[5]*z[97];
    z[40]=z[40] + z[97] + z[78] + z[51] - n<T>(2,3)*z[10] + 4*z[60] + z[70];
    z[40]=z[2]*z[40];
    z[51]=6*z[12];
    z[45]=2*z[45];
    z[60]=12*z[38];
    z[70]= - z[8]*z[60];
    z[43]= - static_cast<T>(14)- z[43];
    z[43]=z[3]*z[43];
    z[43]=z[74] + z[45] - 16*z[10] + z[43] + z[70] + z[51] + 13*z[13];
    z[43]=z[4]*z[43];
    z[70]=5*z[13];
    z[78]= - z[26]*z[70];
    z[97]=z[82]*z[8];
    z[78]=z[78] + z[97] + z[114] + z[99];
    z[78]=z[3]*z[78];
    z[102]=6*z[38];
    z[78]=z[78] + z[83] - z[102] - z[86];
    z[86]= - z[91] + n<T>(185,9)*z[3] - z[50] + z[100];
    z[94]= - z[9]*z[94];
    z[103]=z[74]*z[9];
    z[110]= - n<T>(65,9) + z[103];
    z[110]=z[110]*z[65];
    z[34]=static_cast<T>(7)+ z[34];
    z[34]=z[4]*z[34];
    z[34]=z[110] + z[34] + 2*z[86] + z[94];
    z[34]=z[5]*z[34];
    z[86]=n<T>(13,3)*z[3];
    z[94]= - 10*z[14] + z[86];
    z[94]=z[10]*z[94];
    z[94]=12*z[31] + z[94];
    z[94]=z[10]*z[94];
    z[89]= - z[89] + z[94];
    z[89]=z[9]*z[89];
    z[94]=3*z[14];
    z[110]=2*z[3];
    z[113]=z[94] + z[110];
    z[91]=2*z[113] - z[91];
    z[91]=z[10]*z[91];
    z[21]=z[28] + z[36] + z[21] + z[32] + z[40] + z[34] + z[43] + z[89]
    + 2*z[78] + z[91];
    z[21]=z[1]*z[21];
    z[28]= - z[42] + z[104] - z[76];
    z[28]=z[7]*z[28];
    z[32]= - z[70] - z[96];
    z[32]=z[32]*z[95];
    z[28]=z[28] + z[32] + z[67] - z[118];
    z[28]=z[7]*z[28];
    z[32]=z[67] + z[61];
    z[32]=z[32]*z[42];
    z[28]=z[32] + z[28];
    z[28]=z[7]*z[28];
    z[32]=z[6]*z[107];
    z[34]=z[37] + z[13];
    z[36]=z[34]*z[74];
    z[36]=z[59] + z[36];
    z[36]=z[7]*z[36];
    z[36]=z[111] + z[36];
    z[36]=z[36]*z[92];
    z[32]= - z[117] + z[32] + z[36];
    z[32]=z[11]*z[32];
    z[36]= - z[35] - 2*z[83];
    z[36]=z[36]*z[112];
    z[37]= - 13*z[15] - z[2];
    z[37]=z[37]*z[68];
    z[37]= - 7*z[59] + z[37];
    z[37]=z[2]*z[37];
    z[40]=z[12]*z[84];
    z[36]=z[37] + z[36] + z[40];
    z[36]=z[6]*z[36];
    z[37]=z[46]*z[74];
    z[37]=z[37] - z[122];
    z[37]=z[5]*z[37];
    z[28]=z[32] + z[28] + z[36] + z[37] + z[107];
    z[28]=z[11]*z[28];
    z[20]= - z[20] + z[5];
    z[20]=z[20]*z[65];
    z[32]=n<T>(301,6) + z[72];
    z[32]=z[2]*z[32];
    z[32]=n<T>(65,2)*z[15] + z[32];
    z[32]=z[2]*z[32];
    z[20]=z[32] + z[20] + z[87] + 5*z[83];
    z[20]=z[6]*z[20];
    z[32]=z[67] - z[105];
    z[32]=z[9]*z[32];
    z[34]= - z[34]*z[41];
    z[34]= - z[103] - static_cast<T>(1)+ z[34];
    z[34]=z[7]*z[34];
    z[32]=z[34] + z[95] + z[32] + 16*z[13] + 19*z[15];
    z[32]=z[7]*z[32];
    z[34]=35*z[13] - n<T>(69,2)*z[15];
    z[34]=z[4]*z[34];
    z[32]=z[32] + z[34] - z[60] - z[59];
    z[32]=z[7]*z[32];
    z[34]= - z[9]*z[105];
    z[36]= - z[9]*z[15];
    z[36]= - static_cast<T>(3)+ z[36];
    z[36]=z[2]*z[36];
    z[34]=z[36] - 38*z[15] + z[34];
    z[34]=z[2]*z[34];
    z[36]=z[9]*z[106];
    z[34]=z[34] + 4*z[59] + z[36];
    z[34]=z[2]*z[34];
    z[36]= - z[9]*z[108];
    z[37]= - z[102] - z[46];
    z[37]=z[37]*z[74];
    z[40]= - z[4] + z[12] + z[10];
    z[40]=z[5]*z[40];
    z[40]=2*z[79] + z[40];
    z[40]=z[5]*z[40];
    z[20]=z[28] + z[32] + z[20] + z[34] + z[40] + z[37] - z[93] + z[36];
    z[20]=z[11]*z[20];
    z[28]= - z[82] - z[39];
    z[28]=z[28]*z[22];
    z[32]=3*z[77] - z[85];
    z[32]=z[6]*z[8]*z[32];
    z[34]=n<T>(5,3)*z[12];
    z[36]=6*z[14];
    z[37]=z[9]*z[69];
    z[39]= - static_cast<T>(4)+ z[109];
    z[39]=z[5]*z[39];
    z[40]= - z[2]*z[25];
    z[40]= - n<T>(139,6)*z[8] + z[40];
    z[40]=z[2]*z[40];
    z[40]= - n<T>(57,2) + z[40];
    z[40]=z[2]*z[40];
    z[28]=z[32] + z[40] + z[39] + z[37] - z[15] + z[28] - z[36] - z[34]
    + z[52];
    z[28]=z[6]*z[28];
    z[32]=z[87] + z[83];
    z[32]=z[15]*z[32];
    z[37]=z[31]*z[49];
    z[39]= - z[80]*z[45];
    z[32]=z[39] + z[32] + z[37];
    z[32]=z[9]*z[32];
    z[23]=z[23] + z[12];
    z[37]=z[8]*z[23];
    z[37]=static_cast<T>(1)+ z[37];
    z[39]= - z[33] + z[62] + 5*z[9];
    z[39]=z[7]*z[39];
    z[40]=18*z[13];
    z[41]=z[40] + 31*z[15];
    z[41]=z[9]*z[41];
    z[37]=z[39] - n<T>(13,6)*z[27] + 2*z[37] + z[41];
    z[37]=z[7]*z[37];
    z[39]=z[97] + z[50] - 12*z[13];
    z[41]= - 16*z[38] + 23*z[59];
    z[41]=z[9]*z[41];
    z[37]=z[37] + z[90] + n<T>(85,3)*z[4] + z[41] + 2*z[39] + 21*z[15];
    z[37]=z[7]*z[37];
    z[39]= - n<T>(1,3)*z[46] + 4*z[66];
    z[39]=z[9]*z[39];
    z[39]=z[39] - n<T>(19,6)*z[10] + z[15] - z[71] + z[58];
    z[39]=z[4]*z[39];
    z[41]= - z[45] + 25*z[15] - z[98];
    z[41]=z[9]*z[41];
    z[42]=z[8] + z[9];
    z[42]=z[42]*z[68];
    z[41]=z[42] + n<T>(247,6) + z[41];
    z[41]=z[2]*z[41];
    z[42]= - 24*z[59] + n<T>(7,3)*z[46];
    z[42]=z[9]*z[42];
    z[41]=z[41] + 6*z[4] + z[42] - z[47] - n<T>(17,6)*z[10];
    z[41]=z[2]*z[41];
    z[42]=3*z[38];
    z[43]=z[42] + z[75];
    z[45]=z[10]*z[17];
    z[46]=z[12]*z[17];
    z[45]=z[45] - z[46];
    z[47]=z[27] + static_cast<T>(2)- z[45];
    z[47]=z[5]*z[47];
    z[47]=z[47] + 7*z[4] - z[63];
    z[47]=z[5]*z[47];
    z[50]=z[10]*z[101];
    z[20]=z[20] + z[37] + z[28] + z[41] + z[47] + z[39] + z[32] + z[50]
    + 4*z[43] + z[83];
    z[20]=z[11]*z[20];
    z[28]= - static_cast<T>(3)+ z[30];
    z[28]=z[28]*z[25];
    z[30]=z[8]*z[14];
    z[32]= - static_cast<T>(2)+ z[30];
    z[32]=z[32]*z[22];
    z[32]=z[32] - z[29];
    z[32]=z[9]*z[32];
    z[37]= - z[3]*npow(z[8],3);
    z[28]=z[32] + z[28] + z[37];
    z[28]=z[7]*z[28];
    z[32]= - z[14] - z[23];
    z[37]=4*z[8];
    z[32]=z[32]*z[37];
    z[32]= - static_cast<T>(5)+ z[32];
    z[32]=z[8]*z[32];
    z[23]=z[23]*z[22];
    z[23]=n<T>(35,6) + z[23];
    z[23]=z[23]*z[29];
    z[30]=n<T>(23,6)*z[26] - n<T>(59,6) + z[30];
    z[30]=z[9]*z[30];
    z[23]=z[28] + z[33] + z[30] + z[32] + z[23];
    z[23]=z[7]*z[23];
    z[28]= - z[31]*z[62];
    z[28]= - 20*z[15] + z[28] - z[40] + z[48];
    z[30]=z[94] - z[3];
    z[30]=z[30]*z[44];
    z[28]=z[6] + z[2] + z[30] + 2*z[28] + z[3];
    z[28]=z[9]*z[28];
    z[30]= - z[34] + z[100];
    z[32]= - n<T>(5,3)*z[18] - z[35];
    z[32]=z[32]*z[22];
    z[30]=5*z[30] + z[32];
    z[30]=z[8]*z[30];
    z[30]= - n<T>(29,3) + z[30];
    z[19]=z[51] - n<T>(1,3)*z[19];
    z[19]=z[19]*z[22];
    z[19]=n<T>(67,6) + z[19];
    z[19]=z[19]*z[26];
    z[26]= - z[9]*z[49];
    z[26]=n<T>(31,3) + z[26];
    z[26]=z[26]*z[27];
    z[19]=z[23] + z[26] + 2*z[30] + z[19] + z[28];
    z[19]=z[7]*z[19];
    z[23]= - z[77]*z[25]*z[90];
    z[18]= - 2*z[18] + z[42];
    z[18]=z[18]*z[37];
    z[18]=z[18] + n<T>(13,3)*z[12] + z[99];
    z[18]=z[8]*z[18];
    z[26]=2*z[29];
    z[28]=z[12] - 3*z[13];
    z[28]=z[28]*z[26];
    z[30]=z[25]*z[64];
    z[32]=4*z[29];
    z[30]=z[30] + z[32] - n<T>(1,3)*z[8];
    z[30]=z[2]*z[30];
    z[33]=z[65] - z[14];
    z[33]=z[9]*z[33];
    z[18]=z[23] + z[30] + z[28] - static_cast<T>(7)+ z[18] + z[33];
    z[18]=z[6]*z[18];
    z[23]=7*z[8];
    z[26]= - z[23] + z[26];
    z[26]=z[26]*z[110];
    z[26]=n<T>(1,6) + z[26];
    z[26]=z[3]*z[26];
    z[28]=n<T>(9,2)*z[3] + n<T>(20,3)*z[10];
    z[28]=z[10]*z[28];
    z[28]=z[28] + z[54];
    z[28]=z[9]*z[28];
    z[26]=z[28] - z[88] + z[57] + z[26];
    z[26]=z[9]*z[26];
    z[28]= - z[81] - 4*z[10];
    z[28]=z[10]*z[28];
    z[28]=z[28] - z[53];
    z[28]=z[9]*z[28];
    z[30]= - z[3] - z[49];
    z[28]=5*z[30] + z[28];
    z[28]=z[9]*z[28];
    z[30]= - n<T>(5,6)*z[8] + z[32];
    z[30]=z[3]*z[30];
    z[28]=z[28] - n<T>(127,6) + z[30];
    z[28]=z[9]*z[28];
    z[30]= - z[62] + z[29];
    z[30]=z[9]*z[30];
    z[25]= - z[25] + z[30];
    z[25]=z[2]*z[25];
    z[25]=z[25] + z[28] - n<T>(31,6)*z[8] - 8*z[29];
    z[25]=z[2]*z[25];
    z[28]= - z[37] + z[29];
    z[28]=z[3]*z[28];
    z[28]=n<T>(16,3) + z[28];
    z[25]=z[25] - z[73] + 2*z[28] + z[26];
    z[25]=z[2]*z[25];
    z[26]=z[52]*z[29];
    z[28]= - z[82] + z[102];
    z[28]=z[8]*z[28];
    z[28]=n<T>(1,3)*z[12] + z[28];
    z[28]=z[28]*z[22];
    z[28]= - z[26] + z[28] - static_cast<T>(6)+ z[46];
    z[28]=z[3]*z[28];
    z[29]=z[83] + z[67] - z[35];
    z[23]=z[13]*z[23];
    z[23]=z[23] - z[26];
    z[23]=z[3]*z[23];
    z[26]= - z[8]*z[102];
    z[23]=z[23] + z[13] + z[26];
    z[23]=z[3]*z[23];
    z[23]=3*z[29] + z[23];
    z[26]=z[48] - z[86];
    z[26]=z[10]*z[26];
    z[26]= - 6*z[31] + z[26];
    z[26]=z[26]*z[44];
    z[29]= - n<T>(25,3)*z[3] - z[49];
    z[29]=z[10]*z[29];
    z[23]=z[26] + 2*z[23] + z[29];
    z[23]=z[9]*z[23];
    z[26]= - z[38] - z[31];
    z[29]=12*z[8];
    z[26]=z[26]*z[29];
    z[22]=z[13]*z[22];
    z[29]=n<T>(59,6)*z[10] - z[56];
    z[29]=z[9]*z[29];
    z[22]=z[29] - n<T>(3,2) + z[22];
    z[22]=z[4]*z[22];
    z[27]=z[27] + n<T>(25,3) + z[45];
    z[27]=z[27]*z[65];
    z[29]= - z[3]*z[17];
    z[29]=static_cast<T>(2)+ z[29];
    z[29]=z[10]*z[29];
    z[18]=z[21] + z[20] + z[19] + z[18] + z[25] + z[27] + z[22] + z[23]
    + z[29] + z[28] + z[26] + z[36] + z[55] + n<T>(50,3)*z[16] + z[24];

    r += z[18]*z[1];
 
    return r;
}

template double qqb_2lNLC_r285(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r285(const std::array<dd_real,31>&);
#endif
