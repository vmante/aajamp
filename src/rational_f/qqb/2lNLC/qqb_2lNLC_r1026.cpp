#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1026(const std::array<T,31>& k) {
  T z[160];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[7];
    z[9]=k[15];
    z[10]=k[5];
    z[11]=k[8];
    z[12]=k[12];
    z[13]=k[11];
    z[14]=k[14];
    z[15]=k[22];
    z[16]=k[2];
    z[17]=k[17];
    z[18]=k[19];
    z[19]=npow(z[10],2);
    z[20]=npow(z[1],2);
    z[21]=z[19]*z[20];
    z[22]=z[20]*z[7];
    z[23]=z[20]*z[10];
    z[24]=n<T>(7,3)*z[23] - z[22];
    z[24]=z[7]*z[24];
    z[24]=n<T>(67,3)*z[21] + z[24];
    z[24]=z[14]*z[24];
    z[25]=z[21]*z[4];
    z[26]=z[20]*z[11];
    z[27]=z[26]*z[19];
    z[28]=z[22]*z[11];
    z[29]=z[23]*z[11];
    z[30]=10*z[29] - z[28];
    z[30]=z[7]*z[30];
    z[31]=z[8]*z[21];
    z[24]=z[24] - n<T>(53,3)*z[31] + z[30] - 43*z[25] - n<T>(107,3)*z[27];
    z[30]=npow(z[10],3);
    z[31]=npow(z[1],3);
    z[32]=z[30]*z[31];
    z[33]=npow(z[11],2);
    z[34]=z[33]*z[32];
    z[35]=7*z[32];
    z[36]=npow(z[4],2);
    z[37]= - z[36]*z[35];
    z[37]=z[37] + 6*z[34];
    z[38]=z[19]*z[31];
    z[39]=z[33]*z[38];
    z[40]=z[33]*z[7];
    z[41]=n<T>(2,3)*z[40];
    z[42]=z[31]*z[10];
    z[43]=z[42]*z[41];
    z[43]= - 23*z[39] + z[43];
    z[43]=z[7]*z[43];
    z[37]=7*z[37] + z[43];
    z[43]=z[10]*z[1];
    z[44]=npow(z[43],4);
    z[45]=npow(z[4],3);
    z[46]=z[44]*z[45];
    z[47]=npow(z[11],3);
    z[48]=z[47]*z[44];
    z[49]= - 5*z[46] - n<T>(13,3)*z[48];
    z[50]=npow(z[1],4);
    z[51]=z[50]*z[7];
    z[52]=z[51]*z[47];
    z[53]=z[52]*z[19];
    z[54]=z[47]*z[30];
    z[55]=z[50]*z[54];
    z[55]=26*z[55] - 5*z[53];
    z[56]=n<T>(1,3)*z[7];
    z[55]=z[55]*z[56];
    z[57]=z[44]*z[33];
    z[58]=z[8]*z[57];
    z[49]= - 21*z[58] + 2*z[49] + z[55];
    z[55]=5*z[14];
    z[55]=z[45]*z[55];
    z[58]=z[8]*z[47];
    z[55]=n<T>(13,3)*z[58] + z[55];
    z[58]=4*z[3];
    z[59]=npow(z[43],5);
    z[55]=z[58]*z[59]*z[55];
    z[60]=z[36]*z[14];
    z[61]=z[44]*z[60];
    z[49]=z[55] + 2*z[49] + 49*z[61];
    z[55]=2*z[3];
    z[49]=z[49]*z[55];
    z[61]=z[7]*z[42];
    z[61]=2*z[38] + z[61];
    z[61]=z[7]*z[61];
    z[61]=z[32] + z[61];
    z[62]=n<T>(1,3)*z[14];
    z[61]=z[61]*z[62];
    z[63]=z[31]*z[4];
    z[64]=z[30]*z[63];
    z[61]=48*z[64] + z[61];
    z[64]=2*z[14];
    z[61]=z[61]*z[64];
    z[65]=n<T>(244,3)*z[11] - z[8];
    z[65]=z[8]*z[32]*z[65];
    z[37]=z[49] + z[61] + 2*z[37] + z[65];
    z[37]=z[3]*z[37];
    z[24]=2*z[24] + z[37];
    z[37]=npow(z[3],2);
    z[24]=z[24]*z[37];
    z[49]=13*z[20];
    z[61]=z[49] + 14*z[43];
    z[61]=z[61]*z[19];
    z[65]=z[43] + z[20];
    z[66]=3*z[4];
    z[67]=z[66]*z[65];
    z[68]=z[30]*z[67];
    z[61]=z[61] + z[68];
    z[61]=z[4]*z[61];
    z[68]=15*z[43];
    z[69]=z[7]*z[1];
    z[70]=n<T>(14,3)*z[69] + n<T>(41,3)*z[20] + z[68];
    z[70]=z[7]*z[70];
    z[71]=n<T>(139,2)*z[20] + 64*z[43];
    z[72]=n<T>(1,3)*z[10];
    z[71]=z[71]*z[72];
    z[73]=n<T>(5,2)*z[31];
    z[61]=z[70] + z[61] + z[73] + z[71];
    z[61]=z[14]*z[61];
    z[70]=91*z[20] + 186*z[43];
    z[70]=z[70]*z[19];
    z[71]=2*z[43];
    z[74]=z[71] + z[20];
    z[75]=40*z[30];
    z[76]=z[4]*z[74]*z[75];
    z[70]=z[70] + z[76];
    z[70]=z[4]*z[70];
    z[76]=61*z[20] + 148*z[43];
    z[76]=z[10]*z[76];
    z[70]=z[76] + z[70];
    z[70]=z[4]*z[70];
    z[76]=3*z[20];
    z[61]=z[61] - n<T>(5,3)*z[69] + z[70] + z[76] + n<T>(110,3)*z[43];
    z[61]=z[14]*z[61];
    z[70]= - 93*z[20] - 172*z[43];
    z[70]=z[10]*z[70];
    z[77]=6*z[31];
    z[70]= - z[77] + z[70];
    z[70]=z[10]*z[70];
    z[78]=z[65]*z[10];
    z[79]=z[78] + n<T>(1,3)*z[31];
    z[80]=104*z[79];
    z[81]=z[19]*z[11];
    z[82]=z[81]*z[80];
    z[70]=z[82] - z[50] + z[70];
    z[70]=z[11]*z[70];
    z[82]=34*z[20] + 111*z[43];
    z[82]=z[10]*z[82];
    z[70]=z[70] + n<T>(13,3)*z[31] + z[82];
    z[70]=z[11]*z[70];
    z[82]=6*z[43];
    z[83]= - z[49] - z[82];
    z[83]=z[10]*z[83];
    z[83]= - 8*z[31] + z[83];
    z[83]=z[10]*z[83];
    z[84]=z[76] + z[43];
    z[84]=z[84]*z[10];
    z[85]=3*z[31];
    z[86]=z[85] + z[84];
    z[86]=z[10]*z[86];
    z[86]=z[50] + z[86];
    z[87]=z[11]*z[10];
    z[86]=z[86]*z[87];
    z[83]=z[86] - z[50] + z[83];
    z[83]=z[11]*z[83];
    z[86]=7*z[43];
    z[88]=n<T>(13,2)*z[20] + z[86];
    z[88]=z[10]*z[88];
    z[83]=z[88] + z[83];
    z[83]=z[8]*z[83];
    z[68]= - z[20] - z[68];
    z[68]=z[83] + 2*z[68] + z[70];
    z[68]=z[8]*z[68];
    z[70]=10*z[20];
    z[83]=z[70] + 19*z[43];
    z[88]=3*z[10];
    z[83]=z[83]*z[88];
    z[83]=z[31] + z[83];
    z[80]= - z[87]*z[80];
    z[80]=3*z[83] + z[80];
    z[80]=z[11]*z[80];
    z[83]= - 59*z[20] - 281*z[43];
    z[80]=n<T>(1,3)*z[83] + z[80];
    z[80]=z[11]*z[80];
    z[79]=z[11]*z[79];
    z[79]=52*z[79] - 20*z[20] - 43*z[43];
    z[79]=z[11]*z[79];
    z[79]=n<T>(28,3)*z[1] + z[79];
    z[79]=z[11]*z[79];
    z[83]=5*z[11];
    z[89]=z[83]*z[74];
    z[89]=z[89] - z[1];
    z[41]= - z[89]*z[41];
    z[41]=z[41] - static_cast<T>(1)+ z[79];
    z[79]=2*z[7];
    z[41]=z[41]*z[79];
    z[90]=z[4]*z[1];
    z[75]= - z[90]*z[75];
    z[91]=z[19]*z[1];
    z[75]= - 91*z[91] + z[75];
    z[75]=z[4]*z[75];
    z[75]= - 60*z[43] + z[75];
    z[75]=z[4]*z[75];
    z[92]=z[20]*z[13];
    z[93]=5*z[1];
    z[94]= - z[93] - 19*z[10];
    z[95]= - z[5]*z[69];
    z[24]=z[95] + z[24] - 6*z[92] + z[61] + z[68] + z[41] + z[80] + n<T>(1,3)
   *z[94] + z[75];
    z[24]=z[12]*z[24];
    z[41]=2*z[50];
    z[61]=z[41] + z[42];
    z[61]=z[61]*z[10];
    z[68]=npow(z[1],5);
    z[61]=z[61] + z[68];
    z[75]=z[61]*z[4];
    z[80]= - z[75] - z[50] + z[21];
    z[94]=2*z[4];
    z[80]=z[80]*z[94];
    z[95]=2*z[6];
    z[96]=z[31]*z[7];
    z[97]=z[95]*z[96];
    z[97]=z[97] + z[22];
    z[98]=2*z[23];
    z[80]=z[80] - z[31] + z[98] + z[97];
    z[80]=z[13]*z[80];
    z[99]=z[63]*z[19];
    z[100]= - z[21] + z[99];
    z[100]=z[4]*z[100];
    z[100]= - z[23] + z[100];
    z[100]=2*z[100] - z[22];
    z[100]=z[8]*z[100];
    z[101]=4*z[36];
    z[102]=z[50]*z[101];
    z[97]=z[31] + z[97];
    z[97]=z[5]*z[97];
    z[80]=z[97] + z[80] + z[100] + z[76] + z[102];
    z[97]=z[50]*z[10];
    z[100]=z[68] + z[97];
    z[102]=z[100]*z[14];
    z[102]=n<T>(7,6)*z[102];
    z[103]=10*z[42];
    z[104]=z[102] - n<T>(67,6)*z[50] - z[103];
    z[104]=z[14]*z[104];
    z[105]=n<T>(1,2)*z[8];
    z[106]=z[105]*z[97];
    z[107]= - z[106] + n<T>(1,2)*z[50];
    z[103]= - z[103] + z[107];
    z[103]=z[8]*z[103];
    z[108]=5*z[96];
    z[109]=z[108]*z[5];
    z[110]=z[96]*z[13];
    z[103]= - z[109] - 5*z[110] + z[104] + 20*z[31] + z[103];
    z[103]=z[12]*z[103];
    z[104]=5*z[42];
    z[102]= - z[102] + n<T>(37,6)*z[50] + z[104];
    z[102]=z[6]*z[102];
    z[68]=z[68]*z[4];
    z[111]=z[50] - z[68];
    z[111]=z[111]*z[94];
    z[111]=z[23] + z[111];
    z[102]=5*z[111] + z[102];
    z[102]=z[14]*z[102];
    z[111]=z[104] - z[107];
    z[111]=z[8]*z[111];
    z[112]=10*z[31];
    z[111]= - z[112] + z[111];
    z[111]=z[6]*z[111];
    z[80]=z[103] + z[102] + z[111] + 5*z[80];
    z[80]=z[15]*z[80];
    z[102]=3*z[42];
    z[103]=z[102] + z[50];
    z[103]=z[103]*z[8];
    z[98]= - n<T>(1,6)*z[31] + z[98];
    z[98]=5*z[98] + z[103];
    z[98]=z[8]*z[98];
    z[111]=14*z[42] + 11*z[50];
    z[113]= - z[111]*z[62];
    z[113]=z[113] + n<T>(29,2)*z[31] + 16*z[23];
    z[113]=z[14]*z[113];
    z[114]=6*z[22];
    z[115]=5*z[31];
    z[116]=z[115] + z[114];
    z[116]=z[13]*z[116];
    z[117]=12*z[20];
    z[114]=z[5]*z[114];
    z[98]=z[114] + z[116] + z[113] - z[117] + z[98];
    z[98]=z[12]*z[98];
    z[100]=z[6]*z[100];
    z[100]= - n<T>(7,2)*z[100] + z[111];
    z[100]=z[100]*z[62];
    z[111]=2*z[42];
    z[113]=n<T>(19,6)*z[50] + z[111];
    z[113]=z[6]*z[113];
    z[114]=9*z[23];
    z[100]=z[100] + z[113] - n<T>(25,2)*z[31] - z[114];
    z[100]=z[6]*z[100];
    z[113]=2*z[20];
    z[116]=z[113] + z[43];
    z[68]=17*z[50] - 14*z[68];
    z[68]=z[4]*z[68];
    z[68]= - z[85] + z[68];
    z[68]=z[68]*z[94];
    z[68]=z[68] + z[100] + z[116];
    z[68]=z[14]*z[68];
    z[85]=5*z[23];
    z[100]= - z[85] + 9*z[31];
    z[100]=z[100]*z[10];
    z[100]=z[100] + 4*z[75] + 14*z[50];
    z[118]=z[4]*z[100];
    z[70]=z[70] - z[43];
    z[70]=z[70]*z[10];
    z[118]=z[118] + z[115] - z[70];
    z[118]=z[118]*z[94];
    z[119]=z[76] - z[71];
    z[120]=z[96]*z[6];
    z[120]= - 11*z[22] + 4*z[120];
    z[77]= - z[77] + z[120];
    z[77]=z[6]*z[77];
    z[77]=z[77] + z[69] + z[118] - z[119];
    z[77]=z[13]*z[77];
    z[85]=z[85] - z[31];
    z[85]=z[85]*z[10];
    z[85]=z[85] - 4*z[99];
    z[118]=z[4]*z[85];
    z[70]=z[118] + z[31] + z[70];
    z[70]=z[4]*z[70];
    z[118]=4*z[20];
    z[70]=z[70] + z[118] - z[43];
    z[70]=2*z[70] - z[69];
    z[70]=z[8]*z[70];
    z[121]=3*z[23];
    z[103]= - z[103] + n<T>(17,6)*z[31] - z[121];
    z[103]=z[8]*z[103];
    z[107]=z[111] - z[107];
    z[107]=z[8]*z[107];
    z[122]=4*z[31];
    z[107]= - z[122] + z[107];
    z[107]=z[6]*z[107];
    z[103]=z[107] + 6*z[20] + z[103];
    z[103]=z[6]*z[103];
    z[107]= - z[113] + z[63];
    z[120]= - z[122] + z[120];
    z[120]=z[6]*z[120];
    z[107]=z[120] + 2*z[107] + z[69];
    z[107]=z[5]*z[107];
    z[120]=z[50]*z[4];
    z[123]= - 22*z[31] + 5*z[120];
    z[101]=z[123]*z[101];
    z[123]=3*z[1];
    z[68]=z[80] + z[98] + z[107] + z[77] + z[68] + z[103] + z[70] + 
    z[123] + z[101];
    z[68]=z[15]*z[68];
    z[70]= - z[31] - 3*z[78];
    z[70]=z[14]*z[70];
    z[77]=3*z[43];
    z[70]=z[70] - z[69] + z[113] + z[77];
    z[70]=z[13]*z[70];
    z[78]=z[14]*z[74];
    z[70]=z[70] + z[78];
    z[70]=z[19]*z[70];
    z[78]=z[50]*z[19];
    z[80]=z[97] + z[51];
    z[98]=3*z[7];
    z[80]=z[80]*z[98];
    z[78]=z[78] + z[80];
    z[80]=npow(z[7],2);
    z[78]=z[8]*z[78]*z[80];
    z[51]= - z[30]*z[51];
    z[51]=z[44] + z[51];
    z[51]=z[13]*z[51];
    z[101]=z[3]*z[14];
    z[103]=z[101]*z[13];
    z[107]= - z[59]*z[103];
    z[124]=z[44]*z[14];
    z[51]=z[107] + z[51] + z[78] + z[124];
    z[51]=z[3]*z[51];
    z[78]= - z[42] - z[108];
    z[78]=z[7]*z[78];
    z[78]=z[38] + z[78];
    z[78]=z[7]*z[78];
    z[51]=z[51] - z[32] + z[78];
    z[78]=npow(z[3],3);
    z[51]=z[51]*z[78];
    z[107]=4*z[69];
    z[108]= - z[20] + z[107];
    z[108]=z[108]*z[80];
    z[124]=2*z[69];
    z[125]=z[124] - z[20];
    z[126]=npow(z[7],3);
    z[127]= - z[8]*z[125]*z[126];
    z[128]=3*z[50];
    z[129]=npow(z[7],4);
    z[130]= - z[129]*z[128];
    z[131]=z[3]*z[8]*npow(z[69],5);
    z[130]=z[130] + z[131];
    z[131]=npow(z[3],4);
    z[130]=z[130]*z[131];
    z[108]=z[130] + z[108] + z[127];
    z[108]=z[5]*z[108];
    z[119]=z[10]*z[119];
    z[125]= - z[71] - z[125];
    z[98]=z[125]*z[98];
    z[98]=z[119] + z[98];
    z[98]=z[7]*z[98];
    z[98]=z[21] + z[98];
    z[98]=z[8]*z[98];
    z[74]=z[74]*z[10];
    z[119]=z[43] - z[20];
    z[125]=2*z[119] + 7*z[69];
    z[125]=z[7]*z[125];
    z[51]=z[108] + z[51] + z[98] - z[74] + z[125] + z[70];
    z[70]=4*z[9];
    z[51]=z[51]*z[70];
    z[98]=3*z[38];
    z[108]=17*z[42] + 25*z[96];
    z[108]=z[7]*z[108];
    z[108]=z[98] + z[108];
    z[125]=z[8]*z[7];
    z[108]=z[108]*z[125];
    z[127]=z[96]*z[19];
    z[130]=z[35] - 6*z[127];
    z[130]=z[13]*z[130];
    z[35]=z[14]*z[35];
    z[44]=z[44]*z[103];
    z[35]= - 11*z[44] + z[130] + z[108] + z[35];
    z[35]=z[3]*z[35];
    z[44]= - z[23] - 31*z[22];
    z[44]=z[7]*z[44];
    z[35]=z[35] - 3*z[21] + z[44];
    z[35]=z[35]*z[37];
    z[44]=8*z[20];
    z[108]= - 33*z[69] + z[44] - 21*z[43];
    z[108]=z[7]*z[108];
    z[130]=z[118] - z[77];
    z[130]=z[10]*z[130];
    z[108]=z[130] + z[108];
    z[108]=z[8]*z[108];
    z[130]=29*z[43];
    z[132]= - 25*z[20] - z[130];
    z[132]=z[10]*z[132];
    z[133]=7*z[31];
    z[132]= - z[133] + z[132];
    z[132]=z[14]*z[132];
    z[134]=9*z[20];
    z[135]=20*z[43];
    z[132]=z[132] + z[134] + z[135];
    z[132]=z[10]*z[132];
    z[136]= - z[7]*z[71];
    z[132]=z[136] + z[132];
    z[132]=z[13]*z[132];
    z[136]=z[31]*z[126];
    z[137]=z[50]*z[3];
    z[129]=z[8]*z[129]*z[137];
    z[129]= - 27*z[136] + 11*z[129];
    z[129]=z[129]*z[78];
    z[136]=z[118] - 15*z[69];
    z[136]=z[8]*z[136];
    z[136]=20*z[1] + z[136];
    z[136]=z[80]*z[136];
    z[129]=z[129] + z[136];
    z[129]=z[5]*z[129];
    z[77]= - z[77] + 8*z[69];
    z[136]=z[14]*z[74];
    z[35]=z[51] + z[129] + z[35] + z[132] + 7*z[136] + 2*z[77] + z[108];
    z[51]=2*z[9];
    z[35]=z[35]*z[51];
    z[77]=z[23] + 3*z[22];
    z[77]=z[77]*z[125];
    z[108]=5*z[21];
    z[129]=z[14]*z[108];
    z[132]=z[23]*z[7];
    z[108]=z[108] - 2*z[132];
    z[108]=z[13]*z[108];
    z[103]=z[32]*z[103];
    z[77]= - 12*z[103] + z[108] + 6*z[77] + z[129];
    z[103]=2*z[37];
    z[77]=z[77]*z[103];
    z[108]=z[80]*z[20];
    z[129]=z[31]*z[8];
    z[136]=z[3]*z[126]*z[129];
    z[136]= - 23*z[108] + 12*z[136];
    z[136]=z[136]*z[103];
    z[138]=17*z[20];
    z[139]=z[138] - 71*z[69];
    z[125]=z[139]*z[125];
    z[139]=z[31]*z[11];
    z[125]=z[136] + n<T>(1,3)*z[125] + n<T>(76,3)*z[69] - n<T>(20,3)*z[20] + z[139];
    z[125]=z[5]*z[125];
    z[136]=13*z[43];
    z[140]=z[113] + z[136];
    z[141]=z[23] + z[31];
    z[142]= - z[4]*z[141];
    z[130]= - z[138] - z[130];
    z[130]=z[10]*z[130];
    z[130]= - z[122] + n<T>(5,3)*z[130];
    z[130]=z[14]*z[130];
    z[130]=z[130] + 2*z[140] + z[142];
    z[130]=z[13]*z[130];
    z[140]=z[118] - z[86];
    z[142]=z[23]*z[4];
    z[140]= - n<T>(106,3)*z[69] + n<T>(5,3)*z[140] + z[142];
    z[140]=z[8]*z[140];
    z[143]=z[118] + n<T>(31,3)*z[43];
    z[143]=z[14]*z[143];
    z[35]=z[35] + z[125] + z[77] + z[130] + z[143] + z[140] - 30*z[7] + 
   n<T>(5,3)*z[1] - 4*z[10];
    z[35]=z[9]*z[35];
    z[77]=z[104]*z[45];
    z[104]=z[111]*z[60];
    z[125]=z[45]*z[50];
    z[130]=z[125]*z[19];
    z[140]=z[101]*z[130];
    z[104]=5*z[140] - z[77] + z[104];
    z[104]=z[3]*z[104];
    z[140]= - z[36]*z[113];
    z[104]=z[140] + z[104];
    z[104]=z[104]*z[37];
    z[140]=z[96] - z[42];
    z[137]=z[137]*z[14];
    z[143]=z[19]*z[137];
    z[143]=z[143] + z[140];
    z[143]=z[143]*z[78];
    z[144]= - z[50]*z[80]*z[131]*z[5];
    z[143]=z[143] + z[144];
    z[143]=z[143]*z[70];
    z[102]=z[101]*z[102];
    z[102]=z[20] + z[102];
    z[102]=z[102]*z[37];
    z[109]= - z[78]*z[109];
    z[102]=z[143] + z[102] + z[109];
    z[102]=z[9]*z[102];
    z[109]=z[20]*z[5];
    z[143]= - z[37]*z[109];
    z[102]=z[143] + z[102];
    z[102]=z[9]*z[102];
    z[119]=z[119]*z[45];
    z[143]=z[36]*z[26];
    z[119]= - 14*z[119] + z[143];
    z[119]=z[14]*z[119];
    z[143]=5*z[20];
    z[144]=z[143] + 7*z[63];
    z[144]=z[144]*z[94];
    z[145]=z[63] + z[20];
    z[146]=5*z[15];
    z[147]=z[146]*z[145];
    z[144]=z[147] + z[144] + z[1];
    z[147]=z[5] - z[14];
    z[144]=z[15]*z[144]*z[147]*z[4];
    z[148]=z[131]*z[125];
    z[149]=z[2]*z[5];
    z[150]=z[149]*z[148];
    z[151]=z[78]*z[31];
    z[152]=z[45]*z[151];
    z[152]=z[150] + z[152];
    z[153]=z[78]*z[36];
    z[154]= - z[122]*z[153];
    z[155]=z[45]*z[1];
    z[154]=27*z[155] + z[154];
    z[154]=z[5]*z[154];
    z[152]=z[154] - 10*z[152];
    z[154]=2*z[2];
    z[152]=z[152]*z[154];
    z[156]=z[20]*z[4];
    z[157]=z[1] - 6*z[156];
    z[157]=8*z[157] - z[26];
    z[157]=z[5]*z[36]*z[157];
    z[102]=z[152] + z[102] + z[144] + z[157] + 4*z[104] + 34*z[155] + 
    z[119];
    z[102]=z[16]*z[102];
    z[104]=z[31] - 4*z[120];
    z[119]=7*z[4];
    z[104]=z[104]*z[119];
    z[104]=z[44] + z[104];
    z[104]=z[4]*z[104];
    z[104]=z[1] + z[104];
    z[104]=z[14]*z[104];
    z[44]= - z[44] + z[63];
    z[44]=z[4]*z[44];
    z[44]= - z[1] + z[44];
    z[44]=z[5]*z[44];
    z[41]= - z[36]*z[41];
    z[41]=z[20] + z[41];
    z[41]=z[14]*z[41];
    z[41]=z[41] - z[109];
    z[41]=z[41]*z[146];
    z[41]=z[41] + z[104] + z[44];
    z[41]=z[15]*z[41];
    z[41]=z[41] + z[102];
    z[44]=z[7]*z[140];
    z[102]= - z[30]*z[137];
    z[44]=z[102] + z[38] + z[44];
    z[44]=z[44]*z[78];
    z[102]=z[126]*z[131]*z[128];
    z[104]=2*z[1];
    z[109]= - z[80]*z[104];
    z[102]=z[109] + z[102];
    z[102]=z[5]*z[102];
    z[109]= - z[14]*z[91];
    z[44]=z[102] + z[44] + z[109] + z[43] - z[69];
    z[44]=z[44]*z[70];
    z[70]= - z[101]*z[98];
    z[98]=5*z[22];
    z[70]=z[70] - z[23] + z[98];
    z[70]=z[70]*z[37];
    z[102]=z[80]*z[151];
    z[109]=5*z[69];
    z[102]= - z[109] + 21*z[102];
    z[102]=z[5]*z[102];
    z[86]= - z[14]*z[86];
    z[44]=z[44] + z[102] + z[70] + z[123] + z[86];
    z[44]=z[44]*z[51];
    z[51]=n<T>(1,3)*z[1];
    z[70]= - z[51] + z[26];
    z[70]=z[14]*z[70];
    z[86]=z[37]*z[22];
    z[102]=24*z[86] - n<T>(17,3)*z[1] - z[26];
    z[102]=z[5]*z[102];
    z[44]=z[44] + z[102] + static_cast<T>(2)+ z[70];
    z[44]=z[9]*z[44];
    z[70]=20*z[45];
    z[102]= - z[70]*z[151];
    z[126]=z[115]*z[153];
    z[126]= - 4*z[155] + z[126];
    z[126]=z[5]*z[126];
    z[102]=14*z[150] + z[102] + 9*z[126];
    z[102]=z[102]*z[154];
    z[126]= - z[36]*z[134];
    z[128]= - z[3]*z[42]*z[70];
    z[126]=z[126] + z[128];
    z[126]=z[126]*z[103];
    z[128]= - 53*z[1] + 20*z[156];
    z[128]=z[128]*z[36];
    z[134]=z[37]*z[156];
    z[128]=3*z[128] + 34*z[134];
    z[128]=z[5]*z[128];
    z[102]=z[102] + z[126] + z[128];
    z[102]=z[2]*z[102];
    z[126]=7*z[20];
    z[128]= - z[126] + 10*z[43];
    z[128]=z[10]*z[128];
    z[128]=z[133] + z[128];
    z[134]=8*z[4];
    z[128]=z[128]*z[134];
    z[128]=z[128] - 52*z[20] + 97*z[43];
    z[128]=z[4]*z[128];
    z[137]=2*z[63];
    z[140]=z[137] - z[20];
    z[140]=z[140]*z[11];
    z[128]=z[140] + z[123] + z[128];
    z[128]=z[4]*z[128];
    z[144]=z[19]*z[90];
    z[144]=8*z[43] + 3*z[144];
    z[144]=z[4]*z[144];
    z[144]=4*z[1] + z[144];
    z[144]=z[14]*z[144];
    z[128]=z[144] + z[128];
    z[128]=z[14]*z[128];
    z[144]= - z[70] + 9*z[60];
    z[144]=z[38]*z[144];
    z[146]=20*z[30];
    z[146]=z[146]*z[125];
    z[101]=z[101]*z[146];
    z[101]=z[101] + z[144];
    z[101]=z[3]*z[101];
    z[144]= - z[36]*z[114];
    z[150]=z[14]*z[142];
    z[101]=z[101] + z[144] + z[150];
    z[101]=z[101]*z[103];
    z[144]=16*z[63];
    z[150]=65*z[20] - z[144];
    z[150]=z[4]*z[150];
    z[140]=z[140] - 15*z[1] + z[150];
    z[140]=z[4]*z[140];
    z[140]=static_cast<T>(6)+ z[140];
    z[140]=z[5]*z[140];
    z[150]=5*z[43];
    z[117]=z[117] - z[150];
    z[117]=z[4]*z[117];
    z[117]= - 41*z[1] + 4*z[117];
    z[117]=z[4]*z[117];
    z[117]= - static_cast<T>(1)+ z[117];
    z[117]=z[117]*z[94];
    z[41]=z[102] + z[44] + z[140] + z[101] + z[117] + z[128] + 2*z[41];
    z[41]=z[16]*z[41];
    z[44]=z[50] + n<T>(8,3)*z[42];
    z[101]= - z[106] - z[44];
    z[101]=z[6]*z[101];
    z[102]= - z[23] + n<T>(1,2)*z[31];
    z[102]=n<T>(13,3)*z[102];
    z[106]=z[42] - z[50];
    z[117]=z[106]*z[105];
    z[101]=z[101] - z[102] + z[117];
    z[101]=z[101]*z[6]*z[8];
    z[44]=z[6]*z[44];
    z[44]=z[102] + z[44];
    z[44]=z[6]*z[44];
    z[102]=z[141]*z[14];
    z[117]=n<T>(5,3)*z[20];
    z[128]=z[117] + z[71];
    z[44]= - n<T>(1,2)*z[102] + z[44] - z[128];
    z[44]=z[13]*z[44];
    z[140]=z[20]*z[8];
    z[92]=z[92] - z[140];
    z[151]=npow(z[8],2);
    z[152]= - z[2]*z[151]*z[113];
    z[152]=z[152] + 3*z[92];
    z[152]=z[12]*z[152];
    z[153]=z[31]*z[13];
    z[153]=z[153] - z[129];
    z[153]=z[153]*z[5];
    z[157]= - 2*z[153] + z[92];
    z[157]=z[5]*z[157];
    z[158]=z[13]*z[1];
    z[159]= - z[1] - z[140];
    z[159]=z[8]*z[159];
    z[157]=n<T>(1,3)*z[157] + z[159] + z[158];
    z[152]=2*z[157] + z[152];
    z[152]=z[2]*z[152];
    z[97]=z[97]*z[6];
    z[106]=z[106] - z[97];
    z[106]=z[106]*z[6];
    z[157]= - z[8]*z[106];
    z[106]=z[31] + z[106];
    z[106]=z[13]*z[106];
    z[106]=z[106] - z[129] + z[157];
    z[129]= - z[2]*z[12];
    z[129]=z[129] - 1;
    z[92]=z[154]*z[92]*z[129];
    z[92]=n<T>(1,2)*z[106] + z[92];
    z[92]=z[18]*z[92];
    z[106]=z[31]*z[105];
    z[106]=z[106] + z[128];
    z[106]=z[8]*z[106];
    z[102]=z[105]*z[102];
    z[44]=z[92] + z[152] + z[153] + z[44] + z[102] + z[106] + z[101];
    z[44]=z[18]*z[44];
    z[92]= - z[111]*z[40];
    z[39]=43*z[39] + z[92];
    z[39]=z[39]*z[56];
    z[56]=22*z[34];
    z[92]=z[6]*z[11]*z[32];
    z[101]=z[80]*npow(z[14],2)*z[42];
    z[39]= - n<T>(1,3)*z[101] - 37*z[92] + z[56] + z[39];
    z[39]=z[13]*z[39];
    z[30]=z[52]*z[30];
    z[47]= - z[3]*z[13]*z[47]*z[59];
    z[47]=z[47] + z[48] + z[30];
    z[52]=7*z[6];
    z[47]=z[52]*z[47];
    z[59]=7*z[50];
    z[54]= - z[54]*z[59];
    z[53]=z[54] + n<T>(5,3)*z[53];
    z[53]=z[7]*z[53];
    z[54]=18*z[6];
    z[57]=z[57]*z[54];
    z[53]=z[57] - 7*z[48] + z[53];
    z[53]=z[13]*z[53];
    z[30]=z[48] - z[30];
    z[30]=z[8]*z[30];
    z[46]=z[14]*z[46];
    z[30]=z[53] + 10*z[46] + n<T>(26,3)*z[30] + z[47];
    z[30]=z[30]*z[55];
    z[46]=29*z[60] - z[70];
    z[32]=z[32]*z[46];
    z[33]=z[127]*z[33];
    z[34]= - 42*z[34] + n<T>(107,3)*z[33];
    z[34]=z[8]*z[34];
    z[33]= - z[56] - 27*z[33];
    z[33]=z[6]*z[33];
    z[30]=z[30] + z[39] + z[33] + z[34] + z[32];
    z[30]=z[30]*z[55];
    z[32]= - 6*z[29] + z[28];
    z[32]=z[7]*z[32];
    z[32]= - 15*z[27] + z[32];
    z[33]=n<T>(13,3)*z[23] + 2*z[22];
    z[33]=z[7]*z[33];
    z[33]= - 10*z[21] + z[33];
    z[33]=z[14]*z[33];
    z[34]=30*z[21] + 11*z[132];
    z[34]=z[6]*z[34];
    z[32]=z[33] + 2*z[32] + z[34];
    z[32]=z[13]*z[32];
    z[33]=z[132]*z[11];
    z[34]=n<T>(122,3)*z[27] - 27*z[33];
    z[39]= - z[21] + z[132];
    z[39]=z[8]*z[39];
    z[34]=2*z[34] + z[39];
    z[34]=z[8]*z[34];
    z[27]= - z[25] + 2*z[27];
    z[27]=5*z[27] + 19*z[33];
    z[27]=z[27]*z[95];
    z[33]=z[21]*z[62];
    z[25]=19*z[25] + z[33];
    z[25]=z[25]*z[64];
    z[21]=z[36]*z[21];
    z[21]=z[30] + z[32] + z[25] + z[27] - 58*z[21] + z[34];
    z[21]=z[21]*z[37];
    z[25]= - n<T>(25,3)*z[20] + z[139];
    z[27]=3*z[26];
    z[30]= - z[51] - z[27];
    z[30]=z[7]*z[30];
    z[32]=z[22]*z[105];
    z[25]=z[32] + n<T>(1,2)*z[25] + z[30];
    z[25]=z[8]*z[25];
    z[30]=z[118] - z[63];
    z[32]= - n<T>(20,3)*z[31] - z[22];
    z[32]=z[6]*z[32];
    z[27]=z[27] + z[1];
    z[27]=z[27]*z[7];
    z[33]=n<T>(5,2)*z[139];
    z[30]=z[32] + z[27] + 7*z[30] - z[33];
    z[30]=z[6]*z[30];
    z[32]=z[80]*z[37]*z[140];
    z[34]= - z[145]*z[119];
    z[39]=z[113] + z[63];
    z[39]=z[11]*z[39];
    z[46]=2*z[31];
    z[47]=z[13]*z[46];
    z[47]= - z[20] + z[47];
    z[48]=n<T>(4,3)*z[5];
    z[47]=z[47]*z[48];
    z[51]= - z[13]*z[113];
    z[25]=z[47] + 10*z[32] + z[51] + z[30] + z[25] - 16*z[7] + z[39] + 
   n<T>(11,3)*z[1] + z[34];
    z[25]=z[5]*z[25];
    z[30]= - 12*z[156] + 23*z[1];
    z[32]=5*z[36];
    z[30]=z[30]*z[32];
    z[32]=z[36]*z[6];
    z[34]=z[32]*z[76];
    z[39]= - z[30] + z[34];
    z[39]=z[6]*z[39];
    z[47]=z[31]*z[36];
    z[51]=33*z[6];
    z[53]=z[63]*z[51];
    z[47]= - 34*z[47] + z[53];
    z[47]=z[47]*z[78];
    z[53]=72*z[155];
    z[39]=2*z[47] + z[53] + z[39];
    z[39]=z[5]*z[39];
    z[47]=z[3]*z[1];
    z[47]=z[45]*npow(z[47],5)*z[52]*z[149];
    z[55]=z[50]*z[6];
    z[56]=z[36]*z[55];
    z[56]= - 7*z[125] + 17*z[56];
    z[56]=z[56]*z[131];
    z[54]= - z[155]*z[54];
    z[54]=z[54] + z[56];
    z[54]=z[5]*z[54];
    z[56]=z[6]*z[148];
    z[47]=z[47] - 21*z[56] + z[54];
    z[47]=z[2]*z[47];
    z[54]=28*z[31];
    z[56]=z[3]*z[97];
    z[56]=42*z[56] + z[54];
    z[45]=z[45]*z[56];
    z[56]=z[31]*z[32];
    z[45]= - 87*z[56] + z[45];
    z[45]=z[45]*z[78];
    z[56]=z[6]*z[53];
    z[45]=z[56] + z[45];
    z[39]=4*z[47] + 2*z[45] + z[39];
    z[39]=z[2]*z[39];
    z[45]=z[20]*z[51];
    z[47]=z[3]*z[151]*z[96];
    z[51]=z[8]*z[76];
    z[45]=z[47] + z[45] - 80*z[156] + z[51];
    z[45]=z[45]*z[37];
    z[47]=37*z[20] - z[144];
    z[47]=z[4]*z[47];
    z[47]= - 58*z[1] + z[47];
    z[47]=z[4]*z[47];
    z[51]= - z[113] - 3*z[63];
    z[51]=z[4]*z[51];
    z[51]= - z[104] + z[51];
    z[51]=z[6]*z[51];
    z[47]=z[47] + z[51];
    z[47]=z[6]*z[47];
    z[30]=z[45] + z[30] + z[47];
    z[30]=z[5]*z[30];
    z[45]=z[130]*z[3];
    z[47]= - z[36]*z[111];
    z[51]= - z[151]*z[42];
    z[47]= - 12*z[45] + z[47] + z[51];
    z[47]=z[3]*z[47];
    z[51]= - z[156] + z[140];
    z[47]=4*z[51] + z[47];
    z[47]=z[47]*z[37];
    z[51]=z[151]*z[104];
    z[47]=z[51] + z[47];
    z[47]=z[12]*z[47];
    z[51]= - z[143] - z[82];
    z[56]=12*z[4];
    z[51]=z[51]*z[56];
    z[51]=175*z[1] + z[51];
    z[51]=z[51]*z[36];
    z[34]=z[51] - z[34];
    z[34]=z[6]*z[34];
    z[51]=z[42]*z[32];
    z[51]= - z[77] + 18*z[51];
    z[45]= - z[52]*z[45];
    z[45]=2*z[51] + z[45];
    z[45]=z[45]*z[58];
    z[51]=116*z[36] - z[151];
    z[51]=z[20]*z[51];
    z[56]=z[6]*z[156];
    z[45]=z[45] - 128*z[56] + z[51];
    z[37]=z[45]*z[37];
    z[30]=z[39] + z[47] + z[30] + z[37] - z[53] + z[34];
    z[30]=z[2]*z[30];
    z[34]=z[118] + 5*z[63];
    z[34]=z[4]*z[34];
    z[37]=z[137] - z[69];
    z[37]=z[6]*z[37];
    z[34]=z[37] - z[93] + z[34];
    z[34]=z[34]*z[95];
    z[37]= - z[143] + z[137];
    z[37]=z[37]*z[134];
    z[37]=73*z[1] + z[37];
    z[37]=z[4]*z[37];
    z[39]=z[52]*z[86];
    z[45]= - z[140]*z[48];
    z[47]= - z[8]*z[69];
    z[47]=n<T>(8,3)*z[1] + z[47];
    z[47]=z[8]*z[47];
    z[34]=z[45] + z[39] + z[34] + 2*z[47] - static_cast<T>(24)+ z[37];
    z[34]=z[5]*z[34];
    z[32]= - z[70] - 19*z[32];
    z[32]=z[3]*z[38]*z[32];
    z[37]=z[36]*z[23];
    z[39]=z[23] - z[22];
    z[39]=z[39]*z[151];
    z[45]=z[6]*z[142];
    z[32]=z[32] + 36*z[45] - 29*z[37] + z[39];
    z[32]=z[32]*z[103];
    z[36]= - 30*z[36] + z[151];
    z[36]=z[38]*z[36];
    z[37]= - z[3]*z[146];
    z[36]=z[37] + z[36];
    z[36]=z[3]*z[36];
    z[37]=z[8]*z[23];
    z[36]=z[36] - 12*z[142] - n<T>(1,3)*z[37];
    z[36]=z[36]*z[103];
    z[37]=12*z[43];
    z[38]= - z[4]*z[37];
    z[38]= - z[1] + z[38];
    z[38]=z[4]*z[38];
    z[39]=4*z[43];
    z[45]= - z[8]*z[39];
    z[45]= - z[1] + z[45];
    z[45]=z[8]*z[45];
    z[36]=z[36] + z[158] + z[45] - static_cast<T>(2)+ z[38];
    z[36]=z[12]*z[36];
    z[38]= - z[20] - z[39];
    z[38]=z[4]*z[38];
    z[38]=18*z[1] + 5*z[38];
    z[38]=z[38]*z[66];
    z[39]= - z[1] + z[156];
    z[39]=z[39]*z[95];
    z[38]=z[39] - static_cast<T>(44)+ z[38];
    z[38]=z[6]*z[38];
    z[39]=static_cast<T>(29)- 36*z[90];
    z[39]=z[4]*z[39];
    z[39]=z[39] + n<T>(2,3)*z[8];
    z[30]=z[30] + z[36] + z[34] + z[32] + 2*z[39] + z[38];
    z[30]=z[2]*z[30];
    z[32]=z[117] - z[43];
    z[34]= - z[31] - n<T>(3,2)*z[23];
    z[34]=z[4]*z[34];
    z[36]=z[8]*z[46];
    z[27]=z[36] - z[27] + 6*z[139] + z[34] - z[32];
    z[27]=z[8]*z[27];
    z[34]=5*z[87];
    z[36]=z[141]*z[34];
    z[36]=z[36] + z[31] - 8*z[23];
    z[36]=z[11]*z[36];
    z[38]=z[113] - n<T>(1,2)*z[63];
    z[29]= - z[20] + z[29];
    z[29]=z[7]*z[29]*z[83];
    z[39]=z[4]*z[42];
    z[39]=n<T>(3,2)*z[39] - z[141];
    z[39]=z[8]*z[39];
    z[29]=z[39] + z[29] + 3*z[38] + z[36];
    z[29]=z[6]*z[29];
    z[36]= - 11*z[20] - n<T>(91,3)*z[43];
    z[36]=z[10]*z[36];
    z[38]=26*z[43];
    z[39]=z[143] + z[38];
    z[42]=n<T>(4,3)*z[81];
    z[39]=z[39]*z[42];
    z[36]=z[39] + z[115] + z[36];
    z[36]=z[11]*z[36];
    z[36]=z[36] + z[128];
    z[36]=z[11]*z[36];
    z[39]=z[81]*z[1];
    z[39]=z[118] - n<T>(20,3)*z[39] + n<T>(47,3)*z[43];
    z[39]=z[39]*z[11];
    z[39]=z[39] - n<T>(31,3)*z[1];
    z[39]=z[39]*z[11];
    z[45]= - static_cast<T>(17)- z[39];
    z[45]=z[7]*z[45];
    z[47]=n<T>(5,2)*z[20] - z[71];
    z[47]=z[47]*z[66];
    z[27]=z[29] + z[27] + z[45] + z[36] + z[47] + n<T>(41,3)*z[1] + 20*z[10]
   ;
    z[27]=z[6]*z[27];
    z[29]=z[20] - z[33];
    z[29]=z[7]*z[29];
    z[29]=n<T>(2,3)*z[31] + z[29];
    z[29]=z[6]*z[29];
    z[33]= - z[1] + n<T>(5,2)*z[26];
    z[33]=z[7]*z[33];
    z[29]=z[29] + z[33] + n<T>(7,2)*z[139] - n<T>(23,3)*z[63] - z[32];
    z[29]=z[6]*z[29];
    z[32]=n<T>(23,2)*z[20] + 11*z[43];
    z[32]=z[10]*z[32];
    z[33]=z[19]*z[67];
    z[32]=z[32] + z[33];
    z[32]=z[4]*z[32];
    z[33]= - z[46] + n<T>(7,6)*z[55];
    z[33]=z[6]*z[33];
    z[32]=z[33] + z[107] + z[32] + n<T>(35,2)*z[20] + z[37];
    z[32]=z[14]*z[32];
    z[33]=42*z[20] + 53*z[43];
    z[33]=z[10]*z[33];
    z[36]= - z[133] + 10*z[74];
    z[36]=z[10]*z[36];
    z[36]=z[59] + z[36];
    z[36]=z[36]*z[94];
    z[33]=z[36] - 26*z[31] + z[33];
    z[33]=z[33]*z[94];
    z[33]=z[33] + n<T>(101,2)*z[20] + 28*z[43];
    z[33]=z[4]*z[33];
    z[28]=z[28] - z[139];
    z[28]= - n<T>(22,3)*z[20] - n<T>(3,2)*z[142] - n<T>(1,2)*z[28];
    z[28]=z[8]*z[28];
    z[26]=z[26] - z[1] - 5*z[10];
    z[26]=z[32] + z[29] + z[28] + z[33] - 2*z[26];
    z[26]=z[14]*z[26];
    z[28]=43*z[20] + 109*z[43];
    z[28]=z[10]*z[28];
    z[28]=z[31] + z[28];
    z[28]=z[10]*z[28];
    z[29]=31*z[20];
    z[32]= - z[29] - 47*z[43];
    z[32]=z[10]*z[32];
    z[32]= - z[115] + z[32];
    z[32]=z[32]*z[42];
    z[28]=z[32] - 5*z[50] + z[28];
    z[28]=z[11]*z[28];
    z[32]= - 58*z[20] - 221*z[43];
    z[32]=z[32]*z[72];
    z[28]=z[28] + z[112] + z[32];
    z[28]=z[11]*z[28];
    z[32]=12*z[31];
    z[33]=z[32] + 13*z[23];
    z[33]=z[10]*z[33];
    z[23]= - z[46] - z[23];
    z[23]=z[10]*z[23];
    z[23]= - z[50] + z[23];
    z[23]=z[23]*z[34];
    z[23]=z[23] - z[50] + z[33];
    z[23]=z[11]*z[23];
    z[33]= - z[116]*z[79];
    z[23]=z[33] - z[114] + z[23];
    z[23]=z[6]*z[23];
    z[33]=n<T>(53,3)*z[43];
    z[23]=z[23] + z[109] + z[28] + n<T>(7,6)*z[63] - z[118] + z[33];
    z[23]=z[6]*z[23];
    z[28]=z[122] - z[121];
    z[28]=z[10]*z[28];
    z[34]=z[61]*z[66];
    z[28]=z[34] + z[59] + z[28];
    z[28]=z[4]*z[28];
    z[34]=21*z[20] - n<T>(26,3)*z[43];
    z[34]=z[10]*z[34];
    z[34]=40*z[31] + z[34];
    z[34]=z[10]*z[34];
    z[34]= - 7*z[75] + n<T>(52,3)*z[50] + z[34];
    z[36]=4*z[11];
    z[34]=z[34]*z[36];
    z[33]= - 126*z[20] + z[33];
    z[33]=z[10]*z[33];
    z[28]=z[34] + 6*z[28] - n<T>(299,3)*z[31] + z[33];
    z[28]=z[11]*z[28];
    z[33]= - z[100]*z[94];
    z[34]=15*z[20] - z[71];
    z[34]=z[10]*z[34];
    z[33]=z[33] - 18*z[31] + z[34];
    z[33]=z[4]*z[33];
    z[34]=z[138] - n<T>(4,3)*z[43];
    z[28]=z[28] + 2*z[34] + z[33];
    z[28]=z[11]*z[28];
    z[33]=7*z[65] - z[124];
    z[33]=z[7]*z[33];
    z[33]= - n<T>(7,2)*z[141] + z[33];
    z[33]=z[14]*z[33];
    z[34]= - z[49] - z[135];
    z[33]=z[33] + 23*z[69] + 2*z[34] + n<T>(7,2)*z[63];
    z[33]=z[33]*z[62];
    z[29]= - z[29] - z[38];
    z[29]=z[10]*z[29];
    z[29]= - z[32] + z[29];
    z[32]=2*z[11];
    z[29]=z[29]*z[32];
    z[29]=z[29] + n<T>(68,3)*z[20] + 37*z[43];
    z[29]=z[11]*z[29];
    z[29]= - n<T>(23,3)*z[1] + z[29];
    z[29]=z[29]*z[32];
    z[34]=z[89]*z[40];
    z[29]=n<T>(4,3)*z[34] - static_cast<T>(1)+ z[29];
    z[29]=z[7]*z[29];
    z[34]=z[4]*z[113];
    z[23]=z[33] + z[23] + z[29] + z[34] + z[28];
    z[23]=z[13]*z[23];
    z[28]= - z[46] + z[114];
    z[28]=z[10]*z[28];
    z[28]=z[28] - 9*z[99];
    z[28]=z[28]*z[94];
    z[29]=z[117] + z[136];
    z[19]=z[29]*z[19];
    z[19]=2*z[19] + 7*z[99];
    z[19]=z[19]*z[36];
    z[29]= - 47*z[20] - 475*z[43];
    z[29]=z[29]*z[72];
    z[19]=z[19] + z[28] - z[115] + z[29];
    z[19]=z[11]*z[19];
    z[28]= - z[85]*z[94];
    z[29]= - z[138] + z[71];
    z[29]=z[10]*z[29];
    z[28]=z[28] - z[31] + z[29];
    z[28]=z[4]*z[28];
    z[29]= - z[118] + 265*z[43];
    z[19]=z[19] + n<T>(1,3)*z[29] + z[28];
    z[19]=z[11]*z[19];
    z[28]= - z[76] - z[71];
    z[28]=z[28]*z[88];
    z[29]=z[46] + z[84];
    z[29]=z[29]*z[87];
    z[28]=z[29] - z[73] + z[28];
    z[28]=z[11]*z[28];
    z[29]= - z[65]*z[87];
    z[29]=z[29] + n<T>(3,2)*z[20] + z[71];
    z[29]=z[11]*z[29];
    z[29]=z[1] + z[29];
    z[29]=z[7]*z[29];
    z[28]=z[29] + z[28] + z[20] + z[150];
    z[28]=z[8]*z[28];
    z[29]=n<T>(73,3) + z[39];
    z[29]=z[7]*z[29];
    z[33]= - 67*z[1] - 109*z[10];
    z[19]=z[28] + z[29] + n<T>(1,3)*z[33] + z[19];
    z[19]=z[8]*z[19];
    z[28]=8*z[96] + n<T>(13,2)*z[50];
    z[29]=n<T>(1,3)*z[6];
    z[28]=z[28]*z[29];
    z[28]=z[28] - z[98] - n<T>(44,3)*z[31];
    z[28]=z[28]*z[6];
    z[29]=z[22]*z[8];
    z[29]= - 97*z[20] + 29*z[29];
    z[33]= - z[31] + n<T>(4,3)*z[22];
    z[33]=z[33]*z[13];
    z[28]=z[33] + z[28] - n<T>(1,6)*z[29];
    z[29]=z[55]*z[7];
    z[29]= - n<T>(7,3)*z[29] + 11*z[96];
    z[33]=n<T>(1,2)*z[6];
    z[29]=z[29]*z[33];
    z[22]=z[29] + 4*z[22];
    z[29]= - z[14]*z[22];
    z[29]=z[29] - z[28];
    z[29]=z[14]*z[29];
    z[33]=n<T>(4,3)*z[110];
    z[34]= - z[5]*z[33];
    z[28]=z[34] + z[28];
    z[28]=z[5]*z[28];
    z[34]=z[12]*z[108];
    z[22]=4*z[34] + z[22] + z[33];
    z[22]=z[17]*z[147]*z[22];
    z[33]= - z[14]*z[80]*z[118];
    z[33]=z[98] + z[33];
    z[33]=z[14]*z[33];
    z[34]= - z[5]*z[98];
    z[33]=z[33] + z[34];
    z[33]=z[12]*z[33];
    z[34]= - z[16]*z[1]*z[147];
    z[22]=z[22] + 4*z[34] + z[33] + z[28] + z[29];
    z[22]=z[17]*z[22];
    z[28]=z[46] - 5*z[91];
    z[28]=z[28]*z[134];
    z[20]=z[28] - 23*z[20] - 51*z[43];
    z[20]=z[4]*z[20];
    z[20]=z[20] + z[123] - 38*z[10];
    z[20]=z[4]*z[20];
    z[28]= - z[54] - 9*z[120];
    z[28]=z[4]*z[28];
    z[29]= - n<T>(5,3)*z[31] + 7*z[120];
    z[29]=z[29]*z[32];
    z[28]=z[29] + z[113] + z[28];
    z[28]=z[11]*z[28];
    z[29]=z[122] + z[120];
    z[29]=z[29]*z[94];
    z[29]=z[126] + z[29];
    z[29]=z[4]*z[29];
    z[29]=z[1] + z[29];
    z[28]=2*z[29] + z[28];
    z[28]=z[28]*z[32];

    r += n<T>(2,3) + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25]
       + z[26] + z[27] + z[28] + z[30] + z[35] + z[41] + z[44] + z[68];
 
    return r;
}

template double qqb_2lNLC_r1026(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1026(const std::array<dd_real,31>&);
#endif
