#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1179(const std::array<T,31>& k) {
  T z[63];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[19];
    z[6]=k[11];
    z[7]=k[4];
    z[8]=k[10];
    z[9]=k[17];
    z[10]=k[22];
    z[11]=k[13];
    z[12]=k[14];
    z[13]=k[6];
    z[14]=k[9];
    z[15]=k[5];
    z[16]=k[8];
    z[17]=k[2];
    z[18]=npow(z[1],3);
    z[19]=z[18]*z[11];
    z[20]=z[18]*z[4];
    z[21]=z[19] - z[20];
    z[22]=z[7]*z[21];
    z[23]=npow(z[1],4);
    z[24]=z[23]*z[11];
    z[25]=z[23]*z[4];
    z[26]=z[24] - z[25];
    z[27]=n<T>(5,3)*z[26];
    z[28]=z[8]*z[7];
    z[29]=z[28]*z[27];
    z[22]=z[22] + z[29];
    z[22]=z[8]*z[22];
    z[29]=n<T>(1,3)*z[7];
    z[29]= - z[21]*z[29];
    z[30]=z[6]*z[7];
    z[31]= - z[26]*z[30];
    z[29]=z[29] + z[31];
    z[29]=z[6]*z[29];
    z[31]= - z[15]*z[21];
    z[26]=z[31] - z[26];
    z[31]=n<T>(1,3)*z[12];
    z[26]=z[26]*z[31];
    z[22]=z[26] + z[29] + n<T>(2,3)*z[21] + z[22];
    z[22]=z[10]*z[22];
    z[26]=4*z[18];
    z[29]=z[26] + n<T>(5,3)*z[25];
    z[29]=z[4]*z[29];
    z[32]=4*z[19];
    z[29]= - z[32] + z[29];
    z[29]=z[7]*z[29];
    z[27]= - z[27] + z[29];
    z[27]=z[8]*z[27];
    z[29]=npow(z[1],2);
    z[33]=z[20] + z[29];
    z[33]=z[33]*z[4];
    z[34]=z[29]*z[11];
    z[33]=z[33] - z[34];
    z[33]=z[33]*z[7];
    z[27]=z[33] + z[27];
    z[27]=z[8]*z[27];
    z[33]= - 5*z[21] - z[33];
    z[35]= - n<T>(8,3)*z[18] - z[25];
    z[35]=z[4]*z[35];
    z[35]=n<T>(8,3)*z[19] + z[35];
    z[35]=z[35]*z[30];
    z[33]=n<T>(1,3)*z[33] + z[35];
    z[33]=z[6]*z[33];
    z[24]=z[18] - z[24];
    z[24]=z[11]*z[24];
    z[35]=z[19] - z[29];
    z[35]=z[35]*z[11];
    z[36]=z[29]*z[4];
    z[35]=z[35] + z[36];
    z[37]= - z[15]*z[35];
    z[24]=z[37] + z[24] - z[20];
    z[24]=z[24]*z[31];
    z[22]=z[22] + z[24] + z[33] + n<T>(2,3)*z[35] + z[27];
    z[22]=z[10]*z[22];
    z[24]=z[15]*z[6];
    z[27]= - z[24] + 1;
    z[31]=z[11]*z[1];
    z[27]=z[31]*z[27];
    z[33]=z[34] + z[1];
    z[37]= - z[6]*z[33];
    z[27]=z[37] + z[27];
    z[27]=z[15]*z[27];
    z[37]=z[36]*z[15];
    z[38]=2*z[29];
    z[37]=z[37] + z[38];
    z[39]=z[37] + z[20];
    z[40]=z[7]*z[1];
    z[41]= - z[40] + z[39];
    z[41]=z[3]*z[41];
    z[42]=z[29]*z[6];
    z[43]=z[31]*z[7];
    z[27]=z[41] + z[27] - z[42] - z[36] + z[43];
    z[27]=z[15]*z[27];
    z[27]= - z[20] + z[27];
    z[27]=z[16]*z[27];
    z[41]=2*z[4];
    z[44]=z[41]*z[29];
    z[45]=z[1] - z[44];
    z[45]=z[15]*z[45];
    z[45]=n<T>(1,3)*z[45] - z[29] + z[40];
    z[45]=z[3]*z[45];
    z[46]=z[34] + z[44];
    z[45]=z[45] + n<T>(2,3)*z[46] - z[43];
    z[46]=7*z[24] - 5;
    z[46]=z[31]*z[46];
    z[47]=4*z[1];
    z[48]=z[47] + z[34];
    z[48]=z[6]*z[48];
    z[46]=z[48] + z[46];
    z[46]=z[15]*z[46];
    z[48]=z[36]*z[7];
    z[49]=z[20] + n<T>(1,3)*z[48];
    z[50]=z[29]*z[7];
    z[51]=z[18] + n<T>(1,3)*z[50];
    z[51]=z[6]*z[51];
    z[51]=z[51] - z[49];
    z[51]=z[12]*z[51];
    z[49]=z[6]*z[49];
    z[27]=n<T>(4,3)*z[27] + z[51] + n<T>(2,3)*z[46] + z[49] + 2*z[45];
    z[27]=z[16]*z[27];
    z[45]=z[30]*z[29];
    z[46]=z[7]*z[11];
    z[49]=z[46]*z[29];
    z[51]=z[34] + z[42];
    z[51]=z[15]*z[51];
    z[51]=z[51] + z[49] - z[45];
    z[52]=z[12]*z[15];
    z[51]=z[51]*z[52];
    z[53]= - z[6]*z[49];
    z[54]=z[34]*z[24];
    z[53]=z[53] + z[54];
    z[53]=z[15]*z[53];
    z[54]=npow(z[28],2);
    z[55]=z[54]*z[19];
    z[56]=2*z[18];
    z[57]=z[46]*z[56];
    z[58]=z[2]*z[19];
    z[57]=z[57] + z[58];
    z[57]=z[2]*z[57]*npow(z[8],2);
    z[55]=z[55] + z[57];
    z[55]=z[2]*z[55];
    z[57]=z[12]*z[7];
    z[58]=z[46] + z[57];
    z[24]=z[58]*npow(z[24],2);
    z[54]= - z[3]*z[2]*z[54];
    z[24]=z[54] + z[24];
    z[24]=z[18]*z[24];
    z[24]=z[55] + z[24];
    z[24]=z[13]*z[24];
    z[54]= - npow(z[46],2)*z[38];
    z[55]=npow(z[7],2);
    z[58]= - z[8]*z[55]*z[34];
    z[24]=z[24] + z[51] + z[53] + z[54] + z[58];
    z[51]=npow(z[11],2);
    z[53]= - z[51]*z[38];
    z[54]=2*z[34];
    z[58]=z[29]*z[8];
    z[59]= - z[54] - z[58];
    z[59]=z[8]*z[59];
    z[53]=z[53] + z[59];
    z[59]=n<T>(1,3)*z[2];
    z[53]=z[53]*z[59];
    z[50]=z[51]*z[50];
    z[60]= - z[46]*z[58];
    z[50]=z[53] - n<T>(4,3)*z[50] + z[60];
    z[50]=z[2]*z[50];
    z[53]= - z[34] + z[58];
    z[53]=z[55]*z[53];
    z[55]= - z[15]*z[49];
    z[60]= - z[2]*z[29]*z[28];
    z[53]=z[60] + z[55] + z[53];
    z[55]=n<T>(1,3)*z[3];
    z[53]=z[53]*z[55];
    z[24]=z[53] + z[50] + n<T>(1,3)*z[24];
    z[24]=z[24]*npow(z[13],2);
    z[50]=z[41]*z[18];
    z[32]= - z[50] + n<T>(11,3)*z[29] + z[32];
    z[32]=z[8]*z[32];
    z[53]= - n<T>(35,3)*z[29] - z[20];
    z[53]=z[4]*z[53];
    z[60]= - z[1] + 4*z[34];
    z[32]=z[32] + n<T>(1,3)*z[43] + n<T>(2,3)*z[60] + z[53];
    z[32]=z[8]*z[32];
    z[53]=static_cast<T>(7)+ 4*z[31];
    z[46]=z[53]*z[46];
    z[53]=z[11]*z[60];
    z[46]=z[53] - z[46];
    z[53]=z[29]*npow(z[4],2);
    z[22]=z[22] + z[32] + n<T>(2,3)*z[53] - static_cast<T>(2)- n<T>(1,3)*z[46] + z[27] + z[24];
    z[24]=z[51]*z[40];
    z[27]=z[6]*z[40];
    z[27]=z[1] + 4*z[27];
    z[27]=z[6]*z[27];
    z[27]=z[27] + z[24] - static_cast<T>(2)- z[31];
    z[27]=z[15]*z[27];
    z[32]=z[18]*z[51];
    z[33]= - z[11]*z[33];
    z[33]=z[33] + z[24];
    z[33]=z[7]*z[33];
    z[46]=5*z[29];
    z[53]= - z[46] + z[40];
    z[53]=z[6]*z[53];
    z[61]=2*z[1];
    z[62]=5*z[36];
    z[27]=z[27] + z[53] + z[33] + z[62] + z[61] + z[32];
    z[27]=z[12]*z[27];
    z[21]= - z[10]*z[21];
    z[21]= - z[35] + z[21];
    z[21]=z[10]*z[21];
    z[32]= - z[15]*z[1];
    z[32]=z[32] + z[38];
    z[32]=z[51]*z[32];
    z[21]=z[21] - z[24] + z[32];
    z[21]=z[15]*z[21];
    z[24]=7*z[29];
    z[32]= - z[24] - z[40];
    z[32]=z[8]*z[32];
    z[33]=z[46] - 2*z[40];
    z[33]=z[8]*z[33];
    z[33]=z[61] + z[33];
    z[35]=z[2]*z[8];
    z[33]=z[33]*z[35];
    z[40]=2*z[31];
    z[46]=static_cast<T>(1)+ z[40];
    z[46]=z[7]*z[46];
    z[21]=z[33] + z[32] + z[46] - z[62] - 5*z[1] - z[34] + z[21];
    z[21]=z[3]*z[21];
    z[21]=z[27] + z[21];
    z[27]=z[38] + z[19];
    z[27]=z[8]*z[27];
    z[32]=z[35] - 1;
    z[32]=z[31]*z[32];
    z[33]= - z[1] - z[54];
    z[33]=z[8]*z[33];
    z[32]=z[33] + z[32];
    z[32]=z[2]*z[32];
    z[27]=z[32] + z[34] + z[27];
    z[27]=z[2]*z[27];
    z[32]= - z[39]*z[52];
    z[33]=z[18]*z[8];
    z[27]=z[27] + z[32] - z[33] + z[37];
    z[27]=z[14]*z[27];
    z[32]= - z[1] - z[44];
    z[32]=z[15]*z[32];
    z[32]=z[32] - z[29] - z[50];
    z[32]=z[12]*z[32];
    z[32]=z[32] + 4*z[36] + z[1] - z[54];
    z[34]=5*z[34];
    z[37]=z[1] + z[34];
    z[37]=2*z[37] + z[36];
    z[37]=z[8]*z[37];
    z[39]= - z[35]*z[40];
    z[37]=z[37] + z[39];
    z[37]=z[37]*z[59];
    z[33]=z[20] + z[33];
    z[39]= - z[36] - z[58];
    z[39]=z[2]*z[39];
    z[33]=5*z[33] + z[39];
    z[33]=z[33]*z[55];
    z[19]= - z[29] - n<T>(4,3)*z[19];
    z[19]=2*z[19] - n<T>(5,3)*z[20];
    z[19]=z[8]*z[19];
    z[19]=n<T>(4,3)*z[27] + z[33] + z[37] + z[19] + n<T>(2,3)*z[32];
    z[27]=z[35] - z[52] + 1;
    z[27]=z[1]*z[27];
    z[27]= - z[58] + z[27];
    z[27]=z[14]*z[27];
    z[32]= - z[8] + z[12];
    z[32]=z[1]*z[32];
    z[27]=2*z[27] + z[32];
    z[27]=z[17]*z[27];
    z[19]=n<T>(2,9)*z[27] + n<T>(1,3)*z[19];
    z[19]=z[14]*z[19];
    z[27]=z[18] + n<T>(1,3)*z[25];
    z[32]=z[27]*z[41]*z[28];
    z[26]=z[26] + z[25];
    z[26]=z[26]*z[41];
    z[24]=z[26] + z[24];
    z[24]=z[48] + n<T>(1,3)*z[24];
    z[26]=z[32] + z[24];
    z[26]=z[8]*z[26];
    z[24]= - n<T>(4,3)*z[45] - z[24];
    z[24]=z[12]*z[24];
    z[32]= - z[28] + z[57];
    z[27]=z[9]*z[27]*z[4]*z[32];
    z[28]=z[28]*z[42];
    z[24]=2*z[27] + z[24] + z[26] + n<T>(4,3)*z[28];
    z[24]=z[9]*z[24];
    z[26]=z[38]*z[8];
    z[26]=z[26] - z[36];
    z[23]=z[41]*z[23];
    z[27]=z[23] + 5*z[18];
    z[27]=z[27]*z[4];
    z[28]=z[6]*z[27];
    z[28]=2*z[26] + z[28];
    z[28]=z[28]*z[59];
    z[18]=z[23] + 7*z[18];
    z[23]=n<T>(1,3)*z[4];
    z[18]=z[18]*z[23];
    z[18]=z[18] + z[38];
    z[28]= - z[18] + z[28];
    z[28]=z[6]*z[28];
    z[26]=z[2]*z[26];
    z[18]= - n<T>(2,3)*z[26] + z[18];
    z[18]=z[3]*z[18];
    z[26]=z[6] - z[3];
    z[26]=z[5]*z[2]*z[26]*z[27];
    z[18]=n<T>(1,3)*z[26] + z[18] + z[28];
    z[18]=z[5]*z[18];
    z[18]=z[18] + z[24];
    z[24]=z[47] + z[34];
    z[20]=n<T>(19,3)*z[29] + z[20];
    z[20]=z[20]*z[41];
    z[20]= - n<T>(16,3)*z[58] - n<T>(2,3)*z[43] + n<T>(1,3)*z[24] + z[20];
    z[24]=z[56] + z[25];
    z[23]=z[24]*z[23];
    z[23]= - n<T>(5,9)*z[49] + z[29] + z[23];
    z[23]=z[6]*z[23];
    z[20]=n<T>(1,3)*z[20] + z[23];
    z[20]=z[6]*z[20];
    z[23]= - static_cast<T>(1)+ n<T>(2,9)*z[30];
    z[23]=z[6]*z[31]*z[23];
    z[24]= - static_cast<T>(2)+ z[31];
    z[24]=z[11]*z[24];
    z[23]=n<T>(1,9)*z[24] + z[23];
    z[23]=z[15]*z[23];
    z[24]= - z[8]*z[60];
    z[24]=z[24] + static_cast<T>(1)- z[40];
    z[24]=z[8]*z[24];
    z[25]=static_cast<T>(2)+ n<T>(5,3)*z[31];
    z[25]=z[11]*z[25];
    z[24]=z[25] + n<T>(2,3)*z[24];
    z[24]=z[24]*z[59];
    z[18]=z[24] + z[23] + z[20] + z[19] + n<T>(1,9)*z[21] + n<T>(2,3)*z[18] + n<T>(1,3)*z[22];

    r += 4*z[18];
 
    return r;
}

template double qqb_2lNLC_r1179(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1179(const std::array<dd_real,31>&);
#endif
