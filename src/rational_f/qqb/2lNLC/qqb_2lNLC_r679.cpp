#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r679(const std::array<T,31>& k) {
  T z[146];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[8];
    z[9]=k[3];
    z[10]=k[4];
    z[11]=k[5];
    z[12]=k[9];
    z[13]=k[2];
    z[14]=k[15];
    z[15]=k[23];
    z[16]=npow(z[1],3);
    z[17]=z[16]*z[4];
    z[18]=npow(z[1],2);
    z[19]=2*z[18];
    z[20]=z[19] - z[17];
    z[21]=2*z[4];
    z[22]=npow(z[11],2);
    z[20]=z[21]*z[22]*z[20];
    z[23]=z[18]*z[11];
    z[24]=z[23] + z[16];
    z[25]= - z[24]*z[22];
    z[26]=2*z[16];
    z[27]=z[26]*z[4];
    z[28]=npow(z[11],3);
    z[29]= - z[28]*z[27];
    z[25]=z[25] + z[29];
    z[25]=z[25]*z[7]*z[4];
    z[29]=z[6]*z[22]*z[26];
    z[30]=z[16]*z[11];
    z[29]=z[30] + z[29];
    z[29]=z[6]*z[29];
    z[20]=z[25] + z[20] + z[29];
    z[20]=z[7]*z[20];
    z[25]=z[23] - z[16];
    z[29]=2*z[11];
    z[31]=z[29]*z[16];
    z[32]= - z[4]*z[31];
    z[32]= - 3*z[25] + z[32];
    z[32]=z[4]*z[32];
    z[33]=z[26] - z[23];
    z[34]=z[29]*z[18];
    z[35]=z[34] + z[16];
    z[36]=z[35]*z[11];
    z[37]=npow(z[1],4);
    z[36]=z[36] - z[37];
    z[38]=npow(z[1],5);
    z[39]=z[38]*z[4];
    z[40]= - z[39] - z[36];
    z[41]=2*z[6];
    z[40]=z[40]*z[41];
    z[42]=z[37]*z[4];
    z[40]=z[40] - 3*z[33] + 4*z[42];
    z[40]=z[6]*z[40];
    z[20]=z[20] + z[40] - z[18] + z[32];
    z[20]=z[7]*z[20];
    z[32]=z[22]*z[16];
    z[40]=z[16]*z[5];
    z[43]= - z[28]*z[40];
    z[43]= - z[32] + z[43];
    z[43]=z[5]*z[43];
    z[43]= - z[31] + z[43];
    z[43]=z[5]*z[43];
    z[44]=z[16]*z[7];
    z[45]=z[28]*z[44];
    z[32]= - z[32] + z[45];
    z[32]=z[7]*z[32];
    z[32]=z[31] + z[32];
    z[32]=z[7]*z[32];
    z[45]= - z[8]*z[31];
    z[32]=z[45] + z[43] + z[32];
    z[32]=z[12]*z[32];
    z[43]=4*z[23];
    z[45]=3*z[16];
    z[46]= - z[45] + z[43];
    z[46]=z[11]*z[46];
    z[47]=4*z[16];
    z[48]=z[47] - z[23];
    z[49]=z[22]*z[5];
    z[50]= - z[48]*z[49];
    z[46]=z[46] + z[50];
    z[46]=z[5]*z[46];
    z[50]=5*z[23];
    z[46]=z[50] + z[46];
    z[46]=z[5]*z[46];
    z[51]=z[16] + z[43];
    z[51]=z[11]*z[51];
    z[52]=z[24]*z[7];
    z[53]= - z[22]*z[52];
    z[51]=z[51] + z[53];
    z[51]=z[7]*z[51];
    z[50]=z[51] + z[26] - z[50];
    z[50]=z[7]*z[50];
    z[51]=z[34] - z[16];
    z[53]=2*z[8];
    z[54]=z[51]*z[53];
    z[32]=z[32] + z[54] + z[46] + z[50];
    z[32]=z[12]*z[32];
    z[46]=z[11]*z[1];
    z[50]=z[46] + z[18];
    z[54]=z[50]*z[22];
    z[55]=z[50]*z[6];
    z[56]=npow(z[11],4);
    z[57]=z[56]*z[55];
    z[58]=z[56]*z[1];
    z[57]=z[58] + z[57];
    z[57]=z[6]*z[57];
    z[57]= - z[54] + z[57];
    z[57]=z[6]*z[57];
    z[59]=z[22]*z[1];
    z[57]= - z[59] + z[57];
    z[60]=2*z[24];
    z[61]=z[60]*z[28];
    z[62]=z[19] + z[46];
    z[63]=z[62]*z[11];
    z[64]=z[63] + z[16];
    z[65]=z[64]*z[6];
    z[66]= - z[56]*z[65];
    z[66]=z[61] + z[66];
    z[66]=z[6]*z[66];
    z[67]=3*z[46];
    z[68]=4*z[18];
    z[69]=z[67] + z[68];
    z[70]=z[69]*z[28];
    z[66]=z[70] + z[66];
    z[66]=z[6]*z[66];
    z[71]=2*z[1];
    z[72]=z[28]*z[71];
    z[66]=z[72] + z[66];
    z[66]=z[8]*z[66];
    z[57]=2*z[57] + z[66];
    z[57]=z[8]*z[57];
    z[66]= - z[68] + z[46];
    z[66]=z[11]*z[66];
    z[73]=z[35]*z[29];
    z[74]= - z[6]*z[58];
    z[73]=z[73] + z[74];
    z[73]=z[6]*z[73];
    z[66]=z[66] + z[73];
    z[66]=z[6]*z[66];
    z[73]=z[68] + z[46];
    z[74]=2*z[73];
    z[75]=z[10]*z[71];
    z[57]=z[75] + z[57] - z[74] + z[66];
    z[57]=z[8]*z[57];
    z[66]=17*z[16] + z[43];
    z[66]=z[11]*z[66];
    z[48]= - z[11]*z[48];
    z[75]=18*z[37];
    z[48]= - z[75] + z[48];
    z[48]=z[11]*z[48];
    z[48]= - 20*z[38] + z[48];
    z[48]=z[5]*z[48];
    z[48]=z[48] + 22*z[37] + z[66];
    z[48]=z[5]*z[48];
    z[48]= - z[16] + z[48];
    z[48]=z[5]*z[48];
    z[66]=3*z[37];
    z[76]= - z[66] - z[30];
    z[76]=z[11]*z[76];
    z[77]=4*z[37];
    z[78]=z[77] + z[30];
    z[78]=z[11]*z[78];
    z[78]=6*z[38] + z[78];
    z[78]=z[11]*z[78];
    z[79]=npow(z[1],6);
    z[78]=4*z[79] + z[78];
    z[78]=z[5]*z[78];
    z[80]=3*z[38];
    z[76]=z[78] - z[80] + z[76];
    z[76]=z[5]*z[76];
    z[78]=2*z[37];
    z[76]=z[76] + z[78] + z[30];
    z[76]=z[5]*z[76];
    z[76]= - z[26] + z[76];
    z[81]=z[79]*z[5];
    z[82]= - z[81] + 2*z[38];
    z[83]=z[4]*z[5];
    z[84]=z[82]*z[83];
    z[76]=2*z[76] + z[84];
    z[76]=z[4]*z[76];
    z[48]=z[76] + z[18] + z[48];
    z[48]=z[4]*z[48];
    z[76]=z[66] + z[39];
    z[84]=n<T>(1,2)*z[4];
    z[76]=z[76]*z[84];
    z[60]= - z[60] + z[76];
    z[60]=z[6]*z[60];
    z[76]=2*z[46];
    z[85]=5*z[18];
    z[86]=z[76] + z[85];
    z[87]=z[84]*z[38];
    z[88]=z[77] - z[87];
    z[88]=z[4]*z[88];
    z[89]=7*z[16];
    z[88]= - z[89] + z[88];
    z[88]=z[4]*z[88];
    z[60]=z[60] + z[88] + z[86];
    z[60]=z[6]*z[60];
    z[88]=npow(z[1],7);
    z[90]=2*z[5];
    z[91]=z[88]*z[90];
    z[91]= - z[79] + z[91];
    z[91]=z[5]*z[91];
    z[91]= - z[80] + z[91];
    z[91]=z[5]*z[91];
    z[88]=z[88]*z[5];
    z[88]= - z[88] + n<T>(7,2)*z[79];
    z[88]=z[88]*z[5];
    z[92]=n<T>(5,2)*z[38];
    z[88]=z[88] - z[92];
    z[93]=z[4]*z[88];
    z[91]=z[93] + z[78] + z[91];
    z[91]=z[4]*z[91];
    z[93]=9*z[38] - 7*z[81];
    z[93]=z[5]*z[93];
    z[94]=n<T>(1,2)*z[37];
    z[93]=z[94] + z[93];
    z[93]=z[5]*z[93];
    z[91]=z[91] + z[47] + z[93];
    z[91]=z[4]*z[91];
    z[93]=7*z[37];
    z[95]=z[38]*z[5];
    z[96]= - z[93] + 5*z[95];
    z[96]=z[5]*z[96];
    z[96]= - z[26] + z[96];
    z[96]=z[5]*z[96];
    z[97]=z[37]*z[5];
    z[98]=3*z[97];
    z[99]=z[98] - z[89];
    z[99]=z[99]*z[5];
    z[100]=z[68] + z[99];
    z[100]=z[5]*z[100];
    z[101]=4*z[1];
    z[100]= - z[101] + z[100];
    z[100]=z[10]*z[100];
    z[102]=6*z[18];
    z[91]=z[100] + z[91] + z[102] + z[96];
    z[91]=z[3]*z[91];
    z[96]=z[30] + z[37];
    z[100]= - z[96]*z[83];
    z[103]=z[30]*z[7];
    z[104]= - z[4]*z[103];
    z[100]=z[104] + z[40] + z[100];
    z[100]=z[2]*z[100];
    z[104]=5*z[16];
    z[105]= - z[104] + z[98];
    z[106]=4*z[5];
    z[105]=z[105]*z[106];
    z[107]=7*z[18];
    z[105]=z[107] + z[105];
    z[105]=z[5]*z[105];
    z[20]=2*z[100] + z[91] + z[32] + z[20] + z[60] + z[48] + z[101] + 
    z[105] + z[57];
    z[20]=z[2]*z[20];
    z[32]= - z[22]*z[44];
    z[32]=z[31] + z[32];
    z[32]=z[7]*z[32];
    z[48]=z[34] + z[45];
    z[57]=z[48]*z[11];
    z[60]= - z[37] - z[57];
    z[60]=z[5]*z[60];
    z[32]=z[32] + z[60] - z[45] + z[34];
    z[32]=z[7]*z[32];
    z[60]= - z[18]*z[28];
    z[91]= - z[5]*z[62]*z[56];
    z[60]=z[60] + z[91];
    z[60]=z[5]*z[60];
    z[91]=8*z[18];
    z[100]=z[91] + z[67];
    z[105]=z[100]*z[22];
    z[60]=z[105] + z[60];
    z[60]=z[5]*z[60];
    z[105]=z[64]*z[5];
    z[108]= - z[56]*z[105];
    z[108]= - z[61] + z[108];
    z[108]=z[5]*z[108];
    z[108]=z[70] + z[108];
    z[108]=z[5]*z[108];
    z[108]= - z[72] + z[108];
    z[108]=z[6]*z[108];
    z[86]= - z[11]*z[86];
    z[60]=z[108] + z[60] + z[45] + z[86];
    z[60]=z[12]*z[60];
    z[86]=z[37] + z[31];
    z[86]=z[5]*z[86];
    z[96]=z[96]*z[53];
    z[86]=z[96] - z[26] + z[86];
    z[86]=z[4]*z[86];
    z[96]=z[62]*z[22];
    z[108]= - z[107] - z[67];
    z[108]=z[5]*z[108]*z[28];
    z[108]= - 2*z[96] + z[108];
    z[108]=z[5]*z[108];
    z[109]=11*z[18];
    z[110]=z[109] + 8*z[46];
    z[110]=z[11]*z[110];
    z[108]=z[110] + z[108];
    z[108]=z[5]*z[108];
    z[110]= - z[64]*z[28]*z[90];
    z[111]=z[73]*z[11];
    z[112]=z[111] + z[45];
    z[112]=z[112]*z[22];
    z[110]= - z[112] + z[110];
    z[110]=z[5]*z[110];
    z[110]=5*z[54] + z[110];
    z[110]=z[5]*z[110];
    z[113]=z[71]*z[22];
    z[110]= - z[113] + z[110];
    z[110]=z[110]*z[41];
    z[114]=z[85] + z[67];
    z[32]=z[60] + z[32] + z[110] + z[108] + z[86] - z[114];
    z[32]=z[12]*z[32];
    z[60]=z[59] - z[16];
    z[86]=z[5]*z[11];
    z[60]=z[60]*z[86];
    z[108]=z[46] - z[18];
    z[108]=z[108]*z[11];
    z[60]=z[60] - z[108];
    z[110]=z[64]*z[22];
    z[115]=z[110]*z[5];
    z[116]=z[115] - z[54];
    z[116]=z[116]*z[6];
    z[60]=z[116] + 2*z[60];
    z[117]=z[60]*z[41];
    z[118]= - z[11]*z[64];
    z[115]=z[118] - z[115];
    z[118]=6*z[5];
    z[115]=z[115]*z[118];
    z[119]=z[19] + 9*z[46];
    z[119]=z[11]*z[119];
    z[115]=z[115] - n<T>(11,2)*z[16] + z[119];
    z[115]=z[5]*z[115];
    z[119]=z[107] - z[46];
    z[115]=z[117] + 2*z[119] + z[115];
    z[115]=z[6]*z[115];
    z[117]=3*z[23];
    z[119]=z[117] + z[26];
    z[120]=z[29]*z[5];
    z[24]= - z[24]*z[120];
    z[121]=z[11]*z[52];
    z[24]=z[121] + z[24] - z[119];
    z[24]=z[7]*z[24];
    z[36]=z[5]*z[36];
    z[36]=z[36] + z[104] + z[34];
    z[36]=z[5]*z[36];
    z[121]=5*z[46];
    z[122]=z[121] - z[102];
    z[24]=z[24] + z[36] + z[122];
    z[24]=z[7]*z[24];
    z[36]=4*z[46];
    z[123]= - z[107] - z[36];
    z[123]=z[11]*z[123];
    z[124]=10*z[18];
    z[125]= - z[124] - z[67];
    z[125]=z[125]*z[49];
    z[123]=z[123] + z[125];
    z[123]=z[5]*z[123];
    z[125]=z[19] - z[46];
    z[123]=z[123] - z[125];
    z[123]=z[5]*z[123];
    z[43]= - z[45] - z[43];
    z[43]=z[11]*z[43];
    z[43]=z[37] + z[43];
    z[43]=z[5]*z[43];
    z[43]= - 4*z[35] + z[43];
    z[43]=z[5]*z[43];
    z[43]= - z[68] + z[43];
    z[43]=z[4]*z[43];
    z[126]= - z[4]*z[51];
    z[127]= - z[8]*z[97];
    z[126]=z[127] - z[40] + z[126];
    z[126]=z[126]*z[53];
    z[127]=6*z[1];
    z[24]=z[32] + z[126] + z[24] + z[115] + z[43] + z[127] + z[123];
    z[24]=z[12]*z[24];
    z[32]= - z[4]*z[66];
    z[32]=z[47] + z[32];
    z[43]=npow(z[4],2);
    z[32]=z[32]*z[43];
    z[115]=npow(z[6],2);
    z[123]=z[115]*z[47];
    z[32]=z[32] + z[123];
    z[32]=z[8]*z[32];
    z[123]=z[17] - z[18];
    z[126]=z[123]*z[43];
    z[128]=z[16]*z[6];
    z[129]= - z[107] - z[128];
    z[129]=z[129]*z[115];
    z[32]=z[32] + 7*z[126] + z[129];
    z[32]=z[8]*z[32];
    z[126]=npow(z[4],3);
    z[129]=npow(z[6],3);
    z[126]=z[126] + z[129];
    z[129]=2*z[40];
    z[130]=z[129] + z[18];
    z[130]= - z[5]*z[130]*z[126];
    z[131]=z[18]*z[126];
    z[132]=z[16]*z[8];
    z[126]=z[126]*z[132];
    z[126]=z[126] - z[131];
    z[126]=z[8]*z[126];
    z[126]=z[126] + z[130];
    z[126]=z[10]*z[126];
    z[130]=4*z[40];
    z[133]=z[130] + z[107];
    z[133]=z[133]*z[5];
    z[134]=z[89] - z[97];
    z[134]=z[134]*z[83];
    z[134]= - z[133] + z[134];
    z[134]=z[134]*z[43];
    z[135]= - z[16] + z[97];
    z[136]=z[6]*z[5];
    z[135]=z[135]*z[136];
    z[133]= - z[133] + z[135];
    z[133]=z[133]*z[115];
    z[135]= - z[19] - z[128];
    z[137]=npow(z[7],2);
    z[135]=z[135]*z[137]*z[41];
    z[32]=z[126] + z[32] + z[135] + z[134] + z[133];
    z[32]=z[10]*z[32];
    z[126]=z[78] - z[95];
    z[126]=z[126]*z[5];
    z[133]=z[45] - z[126];
    z[133]=z[5]*z[133];
    z[133]= - 6*z[128] - z[102] + z[133];
    z[133]=z[7]*z[133];
    z[134]=3*z[18];
    z[99]= - z[134] - z[99];
    z[99]=z[5]*z[99];
    z[135]=n<T>(3,2)*z[37];
    z[138]=z[6]*z[135];
    z[138]=z[47] + z[138];
    z[138]=z[6]*z[138];
    z[138]= - z[18] + z[138];
    z[138]=z[6]*z[138];
    z[99]=z[133] + z[138] - z[1] + z[99];
    z[99]=z[7]*z[99];
    z[133]=25*z[16] - 11*z[42];
    z[133]=z[4]*z[133];
    z[138]=16*z[18];
    z[133]= - z[138] + z[133];
    z[133]=z[4]*z[133];
    z[139]= - z[6]*z[45];
    z[138]= - z[138] + z[139];
    z[138]=z[6]*z[138];
    z[139]=8*z[37];
    z[140]= - z[139] + 3*z[39];
    z[140]=z[4]*z[140];
    z[140]=z[89] + z[140];
    z[140]=z[4]*z[140];
    z[141]=z[6]*z[89];
    z[140]=z[140] + z[141];
    z[140]=z[8]*z[140];
    z[133]=z[140] + z[133] + z[138];
    z[133]=z[8]*z[133];
    z[138]=n<T>(15,2)*z[16] - z[97];
    z[138]=z[5]*z[138];
    z[140]=n<T>(7,2)*z[37] - z[95];
    z[140]=z[140]*z[83];
    z[138]=z[138] + z[140];
    z[138]=z[4]*z[138];
    z[140]= - z[134] - z[40];
    z[106]=z[140]*z[106];
    z[106]=z[106] + z[138];
    z[106]=z[4]*z[106];
    z[138]=n<T>(1,2)*z[16];
    z[140]=z[138] + z[97];
    z[140]=z[5]*z[140];
    z[135]=z[135] - z[95];
    z[135]=z[135]*z[136];
    z[135]=z[140] + z[135];
    z[135]=z[6]*z[135];
    z[130]= - 13*z[18] - z[130];
    z[130]=z[5]*z[130];
    z[130]=z[130] + z[135];
    z[130]=z[6]*z[130];
    z[32]=z[32] + z[133] + z[99] + z[106] + z[130];
    z[32]=z[10]*z[32];
    z[99]=5*z[38] - z[81];
    z[99]=z[5]*z[99];
    z[88]=z[6]*z[88];
    z[88]=z[88] - n<T>(5,2)*z[37] + z[99];
    z[88]=z[6]*z[88];
    z[99]= - n<T>(9,2)*z[37] - z[95];
    z[99]=z[5]*z[99];
    z[106]=9*z[16];
    z[88]=z[88] + z[106] + z[99];
    z[88]=z[6]*z[88];
    z[99]=npow(z[5],2);
    z[130]= - z[38]*z[99];
    z[133]=z[37] - z[95];
    z[133]=z[133]*z[41];
    z[130]=z[133] - z[45] + z[130];
    z[130]=z[7]*z[130];
    z[133]=z[93] - z[95];
    z[133]=z[5]*z[133];
    z[133]= - 13*z[16] + z[133];
    z[133]=z[5]*z[133];
    z[88]=z[130] + z[88] + z[68] + z[133];
    z[88]=z[7]*z[88];
    z[130]=z[79]*z[99];
    z[130]=z[94] + 2*z[130];
    z[130]=z[5]*z[130];
    z[133]=n<T>(7,2)*z[38] - z[81];
    z[133]=z[5]*z[133];
    z[133]= - z[139] + z[133];
    z[133]=z[4]*z[133];
    z[130]=z[133] + n<T>(29,2)*z[16] + z[130];
    z[130]=z[4]*z[130];
    z[95]= - z[37] - 4*z[95];
    z[95]=z[5]*z[95];
    z[95]=z[26] + z[95];
    z[95]=z[5]*z[95];
    z[95]=z[130] - n<T>(9,2)*z[18] + z[95];
    z[95]=z[4]*z[95];
    z[130]=z[104] + z[97];
    z[130]=z[5]*z[130];
    z[81]= - z[92] + z[81];
    z[81]=z[81]*z[136];
    z[81]= - n<T>(5,2)*z[97] + z[81];
    z[81]=z[6]*z[81];
    z[81]=z[81] - n<T>(3,2)*z[18] + z[130];
    z[81]=z[6]*z[81];
    z[75]= - z[75] + 5*z[39];
    z[75]=z[4]*z[75];
    z[75]=23*z[16] + z[75];
    z[75]=z[4]*z[75];
    z[92]=4*z[38];
    z[130]= - z[4]*z[79];
    z[130]=z[92] + z[130];
    z[130]=z[4]*z[130];
    z[130]= - z[93] + z[130];
    z[130]=z[4]*z[130];
    z[130]=8*z[16] + z[130];
    z[130]=z[8]*z[130];
    z[75]=z[130] - 20*z[18] + z[75];
    z[75]=z[8]*z[75];
    z[130]=z[8]*z[26];
    z[130]=z[130] + z[19] - n<T>(9,2)*z[44];
    z[130]=z[12]*z[130];
    z[133]= - z[5]*z[107];
    z[133]= - z[1] + z[133];
    z[32]=z[130] + z[32] + z[75] + z[88] + z[81] + 2*z[133] + z[95];
    z[32]=z[3]*z[32];
    z[75]=z[76] - z[18];
    z[81]=z[75]*z[11];
    z[88]=z[76] + z[18];
    z[95]=z[88]*z[11];
    z[95]=z[95] - z[16];
    z[130]= - z[95]*z[86];
    z[116]= - z[116] + z[81] + z[130];
    z[116]=z[6]*z[116];
    z[130]=z[110]*z[4];
    z[130]=z[130] - z[54];
    z[130]=z[130]*z[7];
    z[133]=z[4]*z[11];
    z[95]= - z[95]*z[133];
    z[81]=z[130] + z[81] + z[95];
    z[81]=z[7]*z[81];
    z[86]=z[86] - z[133];
    z[86]=z[125]*z[86];
    z[95]=z[40] - z[18];
    z[133]=z[59]*z[7];
    z[135]= - z[133] + z[67] + z[95];
    z[135]=z[7]*z[135];
    z[139]=z[18]*z[5];
    z[140]=3*z[1];
    z[135]=z[135] - z[140] - z[139];
    z[135]=z[10]*z[135];
    z[141]=z[59]*z[6];
    z[142]=z[141] + z[67];
    z[123]=z[142] + z[123];
    z[123]=z[6]*z[123];
    z[143]=z[18]*z[4];
    z[123]=z[123] + z[140] + z[143];
    z[123]=z[9]*z[123];
    z[81]=z[123] + z[135] + z[81] + z[116] + z[86];
    z[81]=z[15]*z[81];
    z[86]=z[134] + z[46];
    z[116]=z[86]*z[11];
    z[116]=z[116] + z[26];
    z[116]=z[116]*z[29];
    z[123]=z[116]*z[4];
    z[123]= - z[111] + z[123] + z[130];
    z[130]=z[7]*z[123];
    z[125]= - z[133] + z[129] - z[125];
    z[125]=z[7]*z[125];
    z[125]=z[139] + z[125];
    z[125]=z[10]*z[125];
    z[135]=z[11]*z[50];
    z[135]=z[45] - 5*z[135];
    z[135]=z[4]*z[135];
    z[36]=z[125] + z[130] + z[135] - 5*z[40] + z[85] + z[36];
    z[36]=z[7]*z[36];
    z[60]= - z[6]*z[60];
    z[125]=z[134] - z[46];
    z[125]=z[11]*z[125];
    z[125]= - z[26] + z[125];
    z[125]=z[5]*z[125];
    z[130]=z[104]*z[4];
    z[60]=z[60] + z[130] - z[102] + z[125];
    z[60]=z[6]*z[60];
    z[27]=z[27] - z[19] + z[142];
    z[27]=z[6]*z[27];
    z[27]=z[27] + z[140] - z[143];
    z[27]=z[6]*z[27];
    z[125]=z[3]*z[10];
    z[135]= - z[1]*z[125];
    z[135]=z[140] + z[135];
    z[135]=z[3]*z[135];
    z[27]=z[27] + z[135];
    z[27]=z[9]*z[27];
    z[135]=z[4]*z[75];
    z[144]=z[10]*z[140];
    z[144]= - z[91] + z[144];
    z[144]=z[3]*z[144];
    z[145]=5*z[1];
    z[27]=z[81] + z[27] + z[144] + z[60] + z[135] - z[145] + z[139] + 
    z[36];
    z[27]=z[15]*z[27];
    z[36]= - z[11]*z[114];
    z[60]=z[104] - z[108];
    z[60]=z[11]*z[60];
    z[60]=z[66] + z[60];
    z[60]=z[4]*z[60];
    z[36]=z[60] - z[26] + z[36];
    z[36]=z[4]*z[36];
    z[60]=6*z[46];
    z[81]=z[60] + z[107];
    z[36]=z[36] - z[81];
    z[36]=z[4]*z[36];
    z[100]=z[100]*z[11];
    z[108]=z[11]*z[65];
    z[108]=z[108] - z[104] - z[100];
    z[108]=z[6]*z[108];
    z[81]=z[108] + z[81];
    z[81]=z[6]*z[81];
    z[36]=z[36] + z[81];
    z[36]=z[8]*z[36];
    z[81]= - 6*z[16] - z[111];
    z[81]=z[4]*z[81];
    z[75]=z[81] - z[75];
    z[75]=z[4]*z[75];
    z[81]=z[19]*z[5];
    z[108]=z[81] - z[1];
    z[75]=z[75] + z[108];
    z[75]=z[4]*z[75];
    z[114]= - z[29]*z[55];
    z[73]=z[114] + z[73];
    z[73]=z[6]*z[73];
    z[73]=z[73] - z[108];
    z[73]=z[6]*z[73];
    z[36]=z[36] + z[75] + z[73];
    z[36]=z[8]*z[36];
    z[73]= - z[18]*z[99];
    z[75]=z[19]*z[4];
    z[75]=z[139] + z[75];
    z[75]=z[4]*z[75];
    z[73]=z[73] + z[75];
    z[73]=z[73]*z[21];
    z[75]=z[99]*z[19];
    z[108]= - z[6]*z[18];
    z[108]=z[81] + z[108];
    z[108]=z[6]*z[108];
    z[108]=z[75] + z[108];
    z[108]=z[6]*z[108];
    z[36]=z[36] + z[73] + z[108];
    z[36]=z[8]*z[36];
    z[64]=z[64]*z[4];
    z[73]= - z[64] - z[50];
    z[73]=z[73]*z[43];
    z[108]= - z[65] + z[50];
    z[108]=z[108]*z[115];
    z[73]=z[73] + z[108];
    z[73]=z[8]*z[73];
    z[73]=z[73] + z[131];
    z[73]=z[73]*npow(z[8],2);
    z[108]= - z[99]*z[131];
    z[73]=2*z[108] + z[73];
    z[73]=z[10]*z[73];
    z[108]=z[102]*z[99];
    z[114]=z[68] - z[40];
    z[115]=z[114]*z[83];
    z[115]= - z[108] + z[115];
    z[43]=z[115]*z[43];
    z[115]=6*z[23];
    z[104]= - z[104] - z[115];
    z[104]=z[5]*z[104];
    z[104]= - z[18] + z[104];
    z[104]=z[104]*z[136];
    z[104]= - z[108] + z[104];
    z[104]=z[6]*z[104];
    z[75]= - z[4]*z[75];
    z[75]=z[75] + z[104];
    z[75]=z[6]*z[75];
    z[36]=z[73] + z[36] + z[43] + z[75];
    z[36]=z[10]*z[36];
    z[43]= - z[119]*z[29];
    z[43]=z[37] + z[43];
    z[43]=z[5]*z[43];
    z[33]=z[43] - z[33];
    z[33]=z[33]*z[136];
    z[43]=z[84]*z[40];
    z[73]= - z[5]*z[35];
    z[73]= - z[18] + z[73];
    z[73]=z[73]*z[118];
    z[33]=z[33] + z[73] - z[43];
    z[33]=z[6]*z[33];
    z[73]= - z[85] - z[46];
    z[73]=2*z[73] - z[105];
    z[73]=z[5]*z[73];
    z[73]= - z[1] + z[73];
    z[73]=z[5]*z[73];
    z[75]=z[18] + z[40];
    z[75]=z[75]*z[90];
    z[43]=z[75] - z[43];
    z[43]=z[4]*z[43];
    z[33]=z[33] + z[73] + z[43];
    z[33]=z[6]*z[33];
    z[43]=z[106] - z[113];
    z[43]=z[11]*z[43];
    z[73]=z[28]*z[1];
    z[75]= - z[77] - z[73];
    z[75]=z[11]*z[75];
    z[75]= - z[80] + z[75];
    z[75]=z[4]*z[75];
    z[43]=z[75] + z[93] + z[43];
    z[43]=z[4]*z[43];
    z[75]=z[115] + z[89];
    z[43]=z[43] + z[75];
    z[43]=z[4]*z[43];
    z[77]= - z[6]*z[110];
    z[77]=z[116] + z[77];
    z[77]=z[6]*z[77];
    z[75]=z[77] - z[75];
    z[75]=z[6]*z[75];
    z[43]=z[43] + z[75];
    z[43]=z[8]*z[43];
    z[75]= - z[45] - z[63];
    z[77]=z[66] + z[31];
    z[77]=z[4]*z[77];
    z[75]=4*z[75] + 3*z[77];
    z[75]=z[4]*z[75];
    z[77]=z[67] + z[19];
    z[75]=z[75] - 2*z[77] - 3*z[40];
    z[75]=z[4]*z[75];
    z[80]=z[6]*z[54];
    z[80]= - z[63] + z[80];
    z[80]=z[80]*z[41];
    z[69]=z[80] + 2*z[69] + z[40];
    z[69]=z[6]*z[69];
    z[43]=z[43] + z[75] + z[69];
    z[43]=z[8]*z[43];
    z[69]= - 11*z[16] - z[34];
    z[69]=z[4]*z[69];
    z[69]=z[69] - z[129] + 9*z[18] - z[46];
    z[69]=z[4]*z[69];
    z[75]=z[85] + z[40];
    z[75]=z[75]*z[90];
    z[69]=z[69] - z[1] + z[75];
    z[69]=z[4]*z[69];
    z[75]=z[134] - z[40];
    z[75]=z[75]*z[90];
    z[80]= - z[19] - z[40];
    z[80]=2*z[80] - z[141];
    z[80]=z[6]*z[80];
    z[75]=z[80] + z[1] + z[75];
    z[75]=z[6]*z[75];
    z[80]=z[46] - z[133];
    z[80]=z[7]*z[80];
    z[80]= - z[1] + z[80];
    z[104]=2*z[7];
    z[80]=z[80]*z[104];
    z[43]=z[43] + z[80] + z[69] + z[75];
    z[43]=z[8]*z[43];
    z[69]=z[114]*z[90];
    z[75]=z[26] - z[97];
    z[75]=z[75]*z[83];
    z[69]=z[69] + z[75];
    z[69]=z[4]*z[69];
    z[75]= - z[99]*z[91];
    z[69]=z[75] + z[69];
    z[69]=z[4]*z[69];
    z[75]=z[84]*z[37];
    z[80]= - z[26] + z[75];
    z[80]=z[4]*z[80];
    z[106]=z[39] - z[66];
    z[106]=z[106]*z[84];
    z[108]=z[6]*z[106];
    z[80]=z[108] - z[19] + z[80];
    z[80]=z[6]*z[80];
    z[80]=z[80] + z[71] - z[143];
    z[80]=z[6]*z[80];
    z[108]= - z[18] + z[128];
    z[108]=z[7]*z[108]*z[41];
    z[110]= - z[4]*z[1];
    z[80]=z[108] + z[110] + z[80];
    z[80]=z[7]*z[80];
    z[33]=z[36] + z[43] + z[80] + z[69] + z[33];
    z[33]=z[10]*z[33];
    z[36]=z[30]*z[12];
    z[43]= - z[34] + z[36];
    z[43]=z[12]*z[43];
    z[43]= - z[85] + z[43];
    z[69]= - z[10]*z[102];
    z[69]= - z[138] + z[69];
    z[69]=z[3]*z[69];
    z[43]=2*z[43] + z[69];
    z[43]=z[2]*z[43];
    z[69]=z[40]*z[8];
    z[80]=z[69] - z[81];
    z[108]=z[132] - z[19];
    z[110]= - z[3]*z[108];
    z[110]=z[80] + z[110];
    z[110]=z[13]*z[8]*z[110];
    z[114]=n<T>(5,2)*z[16] - z[34];
    z[114]=z[114]*z[83];
    z[36]=z[48] - z[36];
    z[48]=2*z[12];
    z[36]=z[48]*z[4]*z[36];
    z[115]=z[3]*z[124];
    z[36]=2*z[110] + z[43] + z[115] + z[36] - 6*z[69] + z[81] + z[114];
    z[36]=z[14]*z[36];
    z[43]= - z[11]*z[37];
    z[43]= - z[38] + z[43];
    z[43]=z[5]*z[43];
    z[43]=z[30] + z[43];
    z[43]=z[5]*z[43];
    z[31]= - z[6]*z[31];
    z[31]=z[43] + z[31];
    z[31]=z[6]*z[31];
    z[22]=z[22]*z[18];
    z[43]= - z[28]*z[143];
    z[43]=z[22] + z[43];
    z[21]=z[43]*z[21];
    z[21]=z[23] + z[21];
    z[21]=z[7]*z[21];
    z[43]=z[47] - z[98];
    z[43]=z[5]*z[43];
    z[57]= - z[83]*z[57];
    z[21]=z[21] + z[31] + z[43] + z[57];
    z[21]=z[7]*z[21];
    z[31]=z[11]*z[51];
    z[31]= - z[66] + z[31];
    z[31]=z[5]*z[31];
    z[31]=z[31] + z[47] + 9*z[23];
    z[31]=z[5]*z[31];
    z[34]= - z[4]*z[34];
    z[31]=z[34] + z[31] - z[18] - z[67];
    z[31]=z[4]*z[31];
    z[34]=z[5]*z[82];
    z[34]=z[34] + z[87];
    z[34]=z[6]*z[34];
    z[34]=z[34] + 2*z[35] + z[75];
    z[34]=z[6]*z[34];
    z[43]=z[11]*z[40];
    z[43]=z[43] + z[89] - z[23];
    z[43]=z[5]*z[43];
    z[34]=z[34] - z[130] - z[68] + z[43];
    z[34]=z[6]*z[34];
    z[43]=z[5]*z[95];
    z[21]=z[21] + z[34] + z[31] + z[145] + 8*z[43];
    z[21]=z[7]*z[21];
    z[31]= - z[66] - z[96];
    z[31]=z[11]*z[31];
    z[34]= - z[50]*z[28];
    z[34]=z[38] + z[34];
    z[34]=z[11]*z[34];
    z[34]=z[79] + z[34];
    z[34]=z[4]*z[34];
    z[31]=z[34] - z[92] + z[31];
    z[31]=z[4]*z[31];
    z[34]=z[113] - z[45];
    z[34]=z[34]*z[11];
    z[31]=z[34] + z[31];
    z[31]=z[4]*z[31];
    z[38]=z[28]*z[65];
    z[38]= - z[112] + z[38];
    z[38]=z[6]*z[38];
    z[34]= - z[34] + z[38];
    z[34]=z[6]*z[34];
    z[38]= - z[56]*z[64];
    z[38]= - z[61] + z[38];
    z[38]=z[4]*z[38];
    z[38]=z[70] + z[38];
    z[38]=z[4]*z[38];
    z[38]= - z[72] + z[38];
    z[38]=z[7]*z[38];
    z[31]=z[38] + z[31] + z[34];
    z[31]=z[8]*z[31];
    z[34]=z[45] - z[59];
    z[34]=z[34]*z[29];
    z[38]= - z[78] + z[73];
    z[38]=z[11]*z[38];
    z[38]= - z[92] + z[38];
    z[38]=z[4]*z[38];
    z[34]=z[38] + 11*z[37] + z[34];
    z[34]=z[4]*z[34];
    z[38]= - z[16] - z[59];
    z[34]=z[34] + 2*z[38] + z[97];
    z[34]=z[4]*z[34];
    z[28]= - z[28]*z[55];
    z[22]=z[22] + z[28];
    z[22]=z[6]*z[22];
    z[22]=z[59] + z[22];
    z[22]=z[22]*z[41];
    z[28]=z[50]*z[56]*z[4];
    z[28]= - z[58] + z[28];
    z[28]=z[4]*z[28];
    z[28]= - z[54] + z[28];
    z[28]=z[4]*z[28];
    z[28]=z[59] + z[28];
    z[28]=z[28]*z[104];
    z[22]=z[31] + z[28] + z[34] + z[22];
    z[22]=z[8]*z[22];
    z[28]=z[93] + z[30];
    z[28]=z[4]*z[28];
    z[28]=z[28] - 14*z[16] - z[117];
    z[28]=z[4]*z[28];
    z[28]=z[28] - 9*z[40] + z[91] - z[121];
    z[28]=z[4]*z[28];
    z[30]=z[123]*z[104];
    z[31]=z[91] - z[67];
    z[31]=z[11]*z[31];
    z[34]= - z[4]*z[58];
    z[34]=z[72] + z[34];
    z[34]=z[4]*z[34];
    z[31]=z[34] - z[26] + z[31];
    z[31]=z[4]*z[31];
    z[30]=z[30] + z[74] + z[31];
    z[30]=z[7]*z[30];
    z[31]=z[6]*z[73];
    z[31]=z[31] - z[26] - z[111];
    z[31]=z[6]*z[31];
    z[31]=z[31] + z[122];
    z[31]=z[6]*z[31];
    z[22]=z[22] + z[30] + z[31] + 16*z[139] + z[28];
    z[22]=z[8]*z[22];
    z[28]=z[18]*z[7];
    z[30]=z[101] + z[28];
    z[30]=z[30]*z[137];
    z[31]= - 12*z[18] - z[44];
    z[31]=z[7]*z[31];
    z[31]= - 12*z[1] + z[31];
    z[34]=z[12]*z[7];
    z[31]=z[31]*z[34];
    z[30]=2*z[30] + z[31];
    z[30]=z[12]*z[30];
    z[31]=npow(z[7],3);
    z[38]=z[31]*z[1];
    z[30]= - z[38] + z[30];
    z[43]=z[3]*z[12];
    z[30]=z[30]*z[43];
    z[51]=npow(z[12],3);
    z[54]=z[38]*z[51];
    z[55]=z[140] + z[28];
    z[56]=z[137]*z[12];
    z[55]=z[55]*z[56];
    z[55]= - z[38] + z[55];
    z[57]=npow(z[12],2);
    z[55]=z[3]*z[55]*z[57];
    z[55]=z[54] + z[55];
    z[54]= - z[9]*z[3]*z[54];
    z[54]=2*z[55] + z[54];
    z[54]=z[9]*z[54];
    z[55]= - z[7]*z[62];
    z[55]= - 9*z[1] + z[55];
    z[55]=z[55]*z[56];
    z[38]=4*z[38] + z[55];
    z[38]=z[38]*z[57];
    z[30]=z[54] + z[38] + z[30];
    z[30]=z[9]*z[30];
    z[38]= - z[7]*z[88];
    z[38]= - 11*z[1] + z[38];
    z[38]=z[38]*z[137];
    z[54]=z[7]*z[86];
    z[55]=7*z[1];
    z[54]=z[55] + 3*z[54];
    z[34]=z[54]*z[34];
    z[34]=z[38] + z[34];
    z[34]=z[12]*z[34];
    z[38]=z[31]*z[71];
    z[34]=z[38] + z[34];
    z[34]=z[12]*z[34];
    z[38]=z[68]*z[7];
    z[38]=z[38] + z[145];
    z[54]= - z[38]*z[104];
    z[56]=z[68] + z[44];
    z[56]=z[7]*z[56];
    z[55]=z[55] + 6*z[56];
    z[55]=z[12]*z[55];
    z[54]=z[54] + z[55];
    z[54]=z[12]*z[54];
    z[55]=z[137]*z[101];
    z[54]=z[55] + z[54];
    z[54]=z[54]*z[43];
    z[30]=z[30] + z[34] + z[54];
    z[30]=z[9]*z[30];
    z[34]= - z[44] + z[77];
    z[34]=z[7]*z[34];
    z[34]=z[71] + z[34];
    z[34]=z[7]*z[34];
    z[54]= - z[107] + z[67];
    z[54]=z[7]*z[54];
    z[54]=z[140] + z[54];
    z[54]=z[12]*z[54];
    z[34]=z[34] + z[54];
    z[34]=z[12]*z[34];
    z[54]= - z[7]*z[46];
    z[54]= - z[127] + z[54];
    z[54]=z[54]*z[137];
    z[34]=z[54] + z[34];
    z[34]=z[12]*z[34];
    z[54]= - z[107] - 6*z[44];
    z[54]=z[12]*z[54];
    z[55]=z[7]*z[85];
    z[54]=z[54] + z[71] + z[55];
    z[48]=z[54]*z[48];
    z[38]= - z[7]*z[38];
    z[38]=z[38] + z[48];
    z[38]=z[38]*z[43];
    z[30]=z[30] + z[34] + z[38];
    z[30]=z[9]*z[30];
    z[34]=z[52] + 2*z[62];
    z[34]=z[34]*z[137];
    z[38]=z[45] - z[103];
    z[38]=z[7]*z[38];
    z[38]=6*z[50] + z[38];
    z[38]=z[7]*z[38];
    z[48]= - z[12]*z[102];
    z[38]=z[48] + z[145] + z[38];
    z[38]=z[12]*z[38];
    z[48]= - z[6]*z[142];
    z[48]= - z[140] + z[48];
    z[41]=z[48]*z[41];
    z[34]=z[38] + z[41] + z[34];
    z[34]=z[12]*z[34];
    z[38]=n<T>(3,2)*z[42];
    z[41]=z[38] - z[47];
    z[41]=z[41]*z[4];
    z[42]= - z[18] - z[41];
    z[42]=z[4]*z[42];
    z[39]=z[39] - z[37];
    z[39]= - z[39]*z[84];
    z[39]= - z[26] + z[39];
    z[39]=z[4]*z[39];
    z[39]=z[19] + z[39];
    z[39]=z[6]*z[39];
    z[39]=z[39] - z[1] + z[42];
    z[39]=z[6]*z[39];
    z[42]=z[125] - 1;
    z[42]=z[71]*z[42];
    z[19]= - z[19] - z[41];
    z[19]=z[4]*z[19];
    z[19]=z[19] + z[42];
    z[19]=z[3]*z[19];
    z[19]=z[39] + z[19];
    z[19]=z[2]*z[19];
    z[39]=z[85] + z[44];
    z[39]=z[39]*z[104];
    z[41]=z[12]*z[89];
    z[41]= - z[68] + z[41];
    z[41]=z[12]*z[41];
    z[39]=z[39] + z[41];
    z[39]=z[12]*z[39];
    z[28]= - z[1] + z[28];
    z[28]=z[28]*z[104];
    z[28]=z[28] + z[39];
    z[28]=z[3]*z[28];
    z[31]= - z[18]*z[31];
    z[19]=z[30] + z[19] + z[28] + z[31] + z[34];
    z[19]=z[9]*z[19];
    z[28]=z[11]*z[25];
    z[30]= - z[35]*z[49];
    z[28]=z[30] + z[94] + z[28];
    z[28]=z[5]*z[28];
    z[25]= - z[106] + z[28] + z[25];
    z[25]=z[6]*z[25];
    z[28]=z[38] - z[26] + n<T>(1,2)*z[97];
    z[28]=z[4]*z[28];
    z[30]= - z[16] - z[117];
    z[30]=z[30]*z[120];
    z[30]= - z[23] + z[30];
    z[30]=z[5]*z[30];
    z[25]=z[25] + z[28] + z[18] + z[30];
    z[25]=z[6]*z[25];
    z[28]= - z[29]*z[105];
    z[26]=z[28] - z[26] - z[100];
    z[26]=z[26]*z[90];
    z[26]=z[134] + z[26];
    z[26]=z[5]*z[26];
    z[17]=n<T>(9,2)*z[17] - n<T>(7,2)*z[18] - 6*z[40];
    z[17]=z[4]*z[17];
    z[17]=z[25] + z[26] + z[17];
    z[17]=z[6]*z[17];
    z[25]= - z[5]*z[58];
    z[25]=z[73] + z[25];
    z[25]=z[5]*z[25];
    z[25]=6*z[59] + z[25];
    z[25]=z[5]*z[25];
    z[25]=z[25] + z[85] - z[60];
    z[25]=z[12]*z[25];
    z[26]= - z[99]*z[73];
    z[26]=z[67] + z[26];
    z[26]=z[5]*z[26];
    z[26]= - z[71] + z[26];
    z[25]=2*z[26] + z[25];
    z[25]=z[12]*z[25];
    z[26]= - z[5]*z[59];
    z[26]= - z[46] + z[26];
    z[26]=z[26]*z[99];
    z[28]= - z[80]*z[53];
    z[25]=z[25] + z[26] + z[28];
    z[25]=z[12]*z[25];
    z[26]=z[8]*z[108];
    z[26]=z[1] + z[26];
    z[28]= - z[57]*z[45];
    z[26]=2*z[26] + z[28];
    z[26]=z[26]*z[43];
    z[28]= - z[9]*z[140];
    z[28]=z[28] + z[102];
    z[28]=z[3]*z[28];
    z[28]= - z[145] + z[28];
    z[28]=z[9]*z[51]*z[28];
    z[25]=z[28] + z[25] + z[26];
    z[25]=z[13]*z[25];
    z[26]= - z[5]*z[63];
    z[26]=z[26] - z[109] - z[76];
    z[26]=z[26]*z[99];
    z[28]=z[45] + z[23];
    z[23]=z[47] + z[23];
    z[23]=z[11]*z[23];
    z[23]=6*z[37] + z[23];
    z[23]=z[11]*z[23];
    z[23]=z[92] + z[23];
    z[23]=z[23]*z[90];
    z[23]= - z[37] + z[23];
    z[23]=z[5]*z[23];
    z[23]=2*z[28] + z[23];
    z[23]=z[5]*z[23];
    z[29]= - z[16] + z[126];
    z[29]=z[4]*z[29];
    z[23]=z[29] + z[18] + z[23];
    z[23]=z[4]*z[23];
    z[28]= - z[11]*z[28];
    z[28]= - z[66] + z[28];
    z[28]=z[28]*z[90];
    z[16]= - z[16] + z[28];
    z[16]=z[5]*z[16];
    z[16]=z[18] + z[16];
    z[16]=z[16]*z[90];
    z[16]=z[23] + z[1] + z[16];
    z[16]=z[4]*z[16];

    r += z[16] + z[17] + z[19] + z[20] + z[21] + z[22] + z[24] + z[25]
       + z[26] + 2*z[27] + z[32] + z[33] + z[36];
 
    return r;
}

template double qqb_2lNLC_r679(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r679(const std::array<dd_real,31>&);
#endif
