#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r339(const std::array<T,31>& k) {
  T z[82];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[8];
    z[7]=k[3];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[12];
    z[11]=k[13];
    z[12]=k[9];
    z[13]=k[2];
    z[14]=k[15];
    z[15]=npow(z[8],2);
    z[16]=n<T>(1,2)*z[15];
    z[17]=z[9]*z[10];
    z[18]=z[17] + 1;
    z[19]=z[18]*z[9];
    z[20]=z[19] - z[8];
    z[21]=z[20]*z[9];
    z[22]=z[16] + z[21];
    z[23]=3*z[9];
    z[22]=z[22]*z[23];
    z[24]=z[21] + z[15];
    z[25]=npow(z[9],2);
    z[26]=z[25]*z[6];
    z[27]= - z[24]*z[26];
    z[28]=npow(z[8],3);
    z[29]=n<T>(1,4)*z[28];
    z[22]=z[27] - z[29] + z[22];
    z[22]=z[6]*z[22];
    z[27]=n<T>(1,2)*z[8];
    z[30]=z[27]*z[5];
    z[31]= - static_cast<T>(3)- z[30];
    z[31]=z[31]*z[16];
    z[32]=n<T>(5,2)*z[8];
    z[33]=z[32] - 3*z[19];
    z[33]=z[9]*z[33];
    z[22]=z[22] + z[31] + z[33];
    z[22]=z[6]*z[22];
    z[31]=n<T>(1,2)*z[3];
    z[33]=z[3]*z[7];
    z[34]=static_cast<T>(5)- z[33];
    z[34]=z[34]*z[31];
    z[35]=z[8]*z[3];
    z[36]=n<T>(1,2)*z[5];
    z[37]=z[36] + z[3];
    z[37]=z[37]*z[35];
    z[34]=z[34] + z[37];
    z[34]=z[8]*z[34];
    z[37]= - static_cast<T>(1)- 3*z[33];
    z[34]=n<T>(1,4)*z[37] + z[34];
    z[34]=z[34]*z[27];
    z[22]=z[22] + z[34] + z[19];
    z[22]=z[2]*z[22];
    z[34]= - n<T>(15,4) + z[33];
    z[34]=z[34]*z[31];
    z[37]=n<T>(7,2)*z[5];
    z[38]=z[37] + z[11];
    z[39]= - n<T>(1,2)*z[38] - z[3];
    z[39]=z[39]*z[35];
    z[34]=z[34] + z[39];
    z[34]=z[8]*z[34];
    z[38]=z[8]*z[38];
    z[38]=static_cast<T>(5)+ z[38];
    z[38]=z[8]*z[38];
    z[39]=3*z[17];
    z[40]= - n<T>(5,2) + z[39];
    z[40]=z[9]*z[40];
    z[38]=z[38] + z[40];
    z[40]=2*z[17];
    z[41]=static_cast<T>(1)+ z[40];
    z[41]=z[41]*z[25];
    z[42]=n<T>(1,4)*z[15];
    z[41]= - z[42] + z[41];
    z[41]=z[6]*z[41];
    z[43]= - static_cast<T>(1)- 4*z[17];
    z[43]=z[9]*z[43];
    z[41]=z[41] - z[8] + z[43];
    z[41]=z[9]*z[41];
    z[43]=n<T>(3,4)*z[15];
    z[41]=z[43] + z[41];
    z[41]=z[6]*z[41];
    z[38]=n<T>(1,2)*z[38] + z[41];
    z[38]=z[6]*z[38];
    z[41]=n<T>(1,2)*z[7];
    z[44]= - z[10]*z[41];
    z[22]=z[22] + z[38] + n<T>(7,8)*z[17] + z[34] + n<T>(3,8)*z[33] + static_cast<T>(1)+ z[44];
    z[22]=z[2]*z[22];
    z[34]=z[5]*z[4];
    z[38]=z[11]*z[4];
    z[44]=npow(z[14],2);
    z[45]=n<T>(27,2)*z[38] - z[44] + n<T>(15,2)*z[34];
    z[45]=z[9]*z[45];
    z[45]=z[45] - z[14];
    z[46]=n<T>(1,2)*z[44];
    z[47]=npow(z[11],2);
    z[48]= - z[46] - z[47];
    z[49]=z[31]*z[8];
    z[48]=z[48]*z[49];
    z[50]=n<T>(1,2)*z[11];
    z[51]= - z[50] - z[14] + 7*z[5];
    z[51]=n<T>(1,4)*z[51] + z[3];
    z[51]=z[3]*z[51];
    z[46]=z[48] - z[46] + z[51];
    z[46]=z[8]*z[46];
    z[48]=static_cast<T>(1)- z[17];
    z[48]=z[48]*z[25];
    z[48]= - z[16] + z[48];
    z[51]=n<T>(1,2)*z[6];
    z[48]=z[48]*z[51];
    z[52]=n<T>(1,4)*z[8];
    z[53]= - z[11]*z[52];
    z[53]= - static_cast<T>(2)+ z[53];
    z[53]=z[8]*z[53];
    z[54]= - n<T>(3,2) - z[17];
    z[54]=z[9]*z[54];
    z[48]=z[48] + z[53] + z[54];
    z[48]=z[6]*z[48];
    z[53]=z[47]*z[27];
    z[54]= - 2*z[5] - z[11];
    z[53]=2*z[54] + z[53];
    z[53]=z[8]*z[53];
    z[54]=n<T>(7,2)*z[17];
    z[48]=z[48] + z[54] - n<T>(9,4) + z[53];
    z[48]=z[6]*z[48];
    z[53]=n<T>(1,4)*z[10];
    z[55]=z[4]*z[7];
    z[56]= - n<T>(23,2) + 5*z[55];
    z[56]=z[56]*z[53];
    z[57]=n<T>(9,2) - z[55];
    z[57]=n<T>(1,2)*z[57] - z[33];
    z[57]=z[57]*z[31];
    z[22]=z[22] + z[48] + z[46] + z[57] - n<T>(15,4)*z[11] - n<T>(31,8)*z[5] + 
    z[56] + z[4] + n<T>(1,4)*z[45];
    z[22]=z[2]*z[22];
    z[45]=z[8]*z[5];
    z[46]=static_cast<T>(3)+ z[45];
    z[42]=z[46]*z[42];
    z[19]= - z[27] + z[19];
    z[19]=z[19]*z[23];
    z[20]=z[20]*z[26];
    z[19]= - n<T>(3,2)*z[20] + z[42] + z[19];
    z[19]=z[6]*z[19];
    z[20]=static_cast<T>(9)+ 7*z[45];
    z[20]=z[20]*z[27];
    z[42]= - n<T>(5,2) - z[39];
    z[42]=z[9]*z[42];
    z[20]=z[20] + z[42];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[6]*z[19];
    z[20]=z[33] - n<T>(7,2);
    z[42]= - z[20]*z[31];
    z[46]=npow(z[3],2);
    z[48]=z[46]*z[45];
    z[37]=z[37] - z[3];
    z[37]=z[37]*z[3];
    z[37]=z[37] + z[48];
    z[56]= - z[8]*z[37];
    z[42]=z[42] + z[56];
    z[42]=z[8]*z[42];
    z[56]= - static_cast<T>(1)- z[33];
    z[42]=n<T>(3,4)*z[56] + z[42];
    z[19]=n<T>(1,2)*z[42] + z[19];
    z[19]=z[2]*z[19];
    z[42]=n<T>(3,2)*z[5];
    z[56]= - 3*z[3] - z[42] - z[11];
    z[56]=z[56]*z[31];
    z[48]=z[56] + z[48];
    z[48]=z[48]*z[27];
    z[56]=z[45] + 1;
    z[57]=n<T>(3,4)*z[8];
    z[56]=z[56]*z[57];
    z[58]=z[17] + n<T>(1,2);
    z[59]=z[58]*z[9];
    z[60]= - z[52] + z[59];
    z[23]=z[6]*z[60]*z[23];
    z[54]= - static_cast<T>(1)- z[54];
    z[54]=z[9]*z[54];
    z[23]=z[23] - z[56] + z[54];
    z[23]=z[6]*z[23];
    z[54]=5*z[5];
    z[60]= - z[54] - z[50];
    z[60]=z[8]*z[60];
    z[23]=z[23] - n<T>(7,4) + z[60];
    z[23]=z[6]*z[23];
    z[60]=n<T>(3,2)*z[38];
    z[61]=z[60] + z[34];
    z[61]=z[61]*z[9];
    z[62]=n<T>(3,2)*z[4];
    z[63]=z[10]*z[55];
    z[63]= - n<T>(39,4)*z[5] + z[62] + z[63];
    z[64]=n<T>(1,4)*z[3];
    z[20]=z[20]*z[64];
    z[65]=3*z[11];
    z[19]=z[19] + z[23] + n<T>(3,2)*z[61] + z[48] + z[20] + n<T>(1,2)*z[63] - 
    z[65];
    z[19]=z[2]*z[19];
    z[20]= - z[10]*z[4];
    z[23]=11*z[4] + z[5];
    z[23]=z[5]*z[23];
    z[20]=z[20] + z[23];
    z[23]=z[62] + z[11];
    z[23]=z[23]*z[65];
    z[48]=n<T>(3,2)*z[11];
    z[63]=static_cast<T>(3)+ z[55];
    z[63]=z[3]*z[63];
    z[63]=z[63] + z[48] - 3*z[4] + z[5];
    z[63]=z[63]*z[31];
    z[66]=npow(z[5],2);
    z[67]=z[66] + n<T>(5,2)*z[47];
    z[68]= - z[3]*z[5];
    z[67]=n<T>(1,2)*z[67] + z[68];
    z[67]=z[67]*z[35];
    z[68]=z[4]*z[47]*z[9];
    z[20]= - n<T>(15,4)*z[68] + z[67] + z[63] + n<T>(1,2)*z[20] + z[23];
    z[23]=n<T>(5,2)*z[10];
    z[63]= - z[26]*z[23];
    z[67]=3*z[5];
    z[69]=z[67] + z[11];
    z[69]=z[8]*z[69];
    z[63]=z[63] - z[17] + n<T>(5,2) + z[69];
    z[63]=z[63]*z[51];
    z[52]= - z[47]*z[52];
    z[69]=n<T>(3,2)*z[10];
    z[52]=z[63] + z[52] + z[48] + z[69] + z[54];
    z[52]=z[6]*z[52];
    z[19]=z[19] + n<T>(1,2)*z[20] + z[52];
    z[19]=z[2]*z[19];
    z[20]= - z[18]*z[26];
    z[20]=z[20] - z[56] + z[59];
    z[20]=z[6]*z[20];
    z[20]=z[20] - static_cast<T>(1)- n<T>(11,4)*z[45];
    z[20]=z[6]*z[20];
    z[52]=static_cast<T>(7)+ 3*z[55];
    z[52]=n<T>(1,2)*z[52] - z[33];
    z[52]=z[52]*z[64];
    z[37]= - z[37]*z[27];
    z[55]=n<T>(1,8)*z[4] - z[5];
    z[20]=z[20] + n<T>(3,4)*z[61] + z[37] + 3*z[55] + z[52];
    z[20]=z[2]*z[20];
    z[37]=z[11] + z[4];
    z[52]=z[37]*z[50];
    z[52]=3*z[34] + z[52];
    z[55]= - 9*z[5] - z[3];
    z[55]=z[55]*z[31];
    z[52]= - n<T>(9,4)*z[68] + 3*z[52] + z[55];
    z[45]=3*z[45];
    z[55]=n<T>(1,2) + z[45];
    z[40]=n<T>(5,4) + z[40];
    z[40]=z[6]*z[9]*z[40];
    z[40]=z[40] + n<T>(1,2)*z[55] - z[17];
    z[40]=z[6]*z[40];
    z[40]=n<T>(13,4)*z[5] + z[40];
    z[40]=z[6]*z[40];
    z[20]=z[20] + n<T>(1,2)*z[52] + z[40];
    z[20]=z[2]*z[20];
    z[40]=z[38]*z[69];
    z[52]=13*z[34] - z[47];
    z[55]=z[5] + z[4];
    z[55]=z[55]*z[3];
    z[52]=n<T>(1,4)*z[52] - z[55];
    z[52]=z[3]*z[52];
    z[40]=z[40] + z[52];
    z[52]= - z[67] - z[50];
    z[56]= - n<T>(1,4) - z[17];
    z[56]=z[6]*z[56];
    z[52]=n<T>(1,2)*z[52] + z[56];
    z[52]=z[6]*z[52];
    z[56]=n<T>(1,4)*z[47];
    z[52]= - z[56] + z[52];
    z[52]=z[6]*z[52];
    z[20]=z[20] + n<T>(1,2)*z[40] + z[52];
    z[20]=z[2]*z[20];
    z[40]=n<T>(1,2)*z[4];
    z[52]=z[40] + z[5];
    z[59]=z[4]*z[41];
    z[59]=static_cast<T>(1)+ z[59];
    z[59]=z[3]*z[59];
    z[59]= - n<T>(7,2)*z[52] + z[59];
    z[59]=z[59]*z[31];
    z[30]= - z[46]*z[30];
    z[61]= - z[10]*z[26];
    z[45]=z[61] + static_cast<T>(1)+ z[45];
    z[45]=z[6]*z[45];
    z[45]=z[54] + z[45];
    z[54]=n<T>(1,4)*z[6];
    z[45]=z[45]*z[54];
    z[30]=z[45] - n<T>(3,8)*z[68] + z[30] + 2*z[34] + z[59];
    z[30]=z[2]*z[30];
    z[45]= - z[52]*z[31];
    z[45]=4*z[34] + z[45];
    z[45]=z[3]*z[45];
    z[52]=z[6]*z[17];
    z[42]= - z[42] + z[52];
    z[52]=npow(z[6],2);
    z[42]=z[42]*z[52];
    z[30]=z[30] + z[45] + n<T>(1,2)*z[42];
    z[30]=z[2]*z[30];
    z[42]=z[46]*z[34];
    z[45]= - npow(z[6],3)*z[53];
    z[30]=z[30] + z[42] + z[45];
    z[30]=z[2]*z[30];
    z[45]=n<T>(7,2)*z[34] - z[55];
    z[45]=z[3]*z[45];
    z[46]= - z[52]*z[36];
    z[45]=z[45] + z[46];
    z[45]=z[2]*z[45];
    z[45]=z[42] + n<T>(1,2)*z[45];
    z[45]=z[45]*npow(z[2],2);
    z[42]=z[1]*npow(z[2],3)*z[42];
    z[42]=z[45] + n<T>(1,2)*z[42];
    z[42]=z[1]*z[42];
    z[45]=z[66]*z[10];
    z[46]=n<T>(3,4)*z[11];
    z[52]=z[3]*z[46]*z[45];
    z[30]=z[42] + z[52] + z[30];
    z[30]=z[1]*z[30];
    z[42]=z[5]*z[12];
    z[52]= - z[4] + z[50];
    z[52]=z[3]*z[52];
    z[55]=z[6]*z[40];
    z[42]=z[55] + z[42] + z[52];
    z[42]=z[6]*z[42];
    z[52]=z[47]*z[31];
    z[42]=z[52] + z[42];
    z[42]=z[6]*z[42];
    z[52]=z[65]*z[45];
    z[55]=n<T>(1,2)*z[10];
    z[59]=z[55] + z[5];
    z[59]=z[11]*z[5]*z[59];
    z[59]= - z[45] + z[59];
    z[34]=z[34]*z[3];
    z[59]=n<T>(3,2)*z[59] + z[34];
    z[59]=z[3]*z[59];
    z[42]=z[42] + z[52] + z[59];
    z[20]=z[30] + n<T>(1,2)*z[42] + z[20];
    z[20]=z[1]*z[20];
    z[30]=z[12]*z[13];
    z[42]=z[14]*z[13];
    z[52]= - static_cast<T>(7)- z[42];
    z[52]=n<T>(1,2)*z[52] + z[30];
    z[36]=z[52]*z[36];
    z[52]=z[42] + 1;
    z[59]=n<T>(1,2)*z[52] - z[30];
    z[59]=z[59]*z[31];
    z[61]=z[10] - z[4];
    z[63]= - z[50] - z[61];
    z[63]=z[63]*z[49];
    z[67]=z[38]*z[9];
    z[68]= - z[67] - z[61];
    z[68]=z[9]*z[68];
    z[70]=z[61]*z[8];
    z[68]=z[70] + z[68];
    z[68]=z[68]*z[54];
    z[71]=n<T>(3,4)*z[67];
    z[36]=z[68] - z[71] + z[63] + z[59] + z[36] - 2*z[4] + z[10];
    z[36]=z[6]*z[36];
    z[59]=static_cast<T>(1)+ n<T>(1,2)*z[42];
    z[63]=z[59]*z[14];
    z[68]=n<T>(1,2)*z[12];
    z[55]= - z[46] - z[55] + z[4] + z[63] - z[68];
    z[55]=z[3]*z[55];
    z[72]=z[42] + n<T>(1,2);
    z[72]=z[72]*z[14];
    z[73]= - z[72] + z[12];
    z[73]=z[5]*z[73];
    z[60]=z[73] - z[60];
    z[35]=z[35]*z[56];
    z[35]=z[36] + z[35] + n<T>(1,2)*z[60] + z[55];
    z[35]=z[6]*z[35];
    z[36]=npow(z[7],2);
    z[55]=npow(z[12],2);
    z[56]= - z[55]*z[36];
    z[56]= - static_cast<T>(1)+ z[56];
    z[56]=z[56]*z[65];
    z[60]=z[55]*z[7];
    z[73]=5*z[60];
    z[74]=3*z[12];
    z[56]=z[56] - z[69] + z[74] + z[73];
    z[56]=z[56]*z[50];
    z[75]=z[4] - z[69];
    z[75]=z[5]*z[75];
    z[76]= - z[4]*z[31];
    z[56]=z[76] + z[56] - z[55] + z[75];
    z[56]=z[56]*z[31];
    z[75]= - z[5]*z[50];
    z[75]= - z[66] + z[75];
    z[75]=z[75]*z[48];
    z[34]=z[34] + z[45] + z[75];
    z[34]=z[34]*z[49];
    z[66]=z[66]*z[17];
    z[75]=z[5] + z[10];
    z[76]=z[5]*z[75];
    z[66]=z[66] + z[76];
    z[66]=z[46]*z[66];
    z[19]=z[20] + z[19] + z[35] + z[34] + z[56] - z[45] + z[66];
    z[19]=z[1]*z[19];
    z[20]=2*z[10];
    z[34]= - n<T>(1,4)*z[67] + z[46] - n<T>(5,4)*z[4] + z[20];
    z[34]=z[9]*z[34];
    z[35]=z[67] + z[10];
    z[56]=z[50] - z[40] - z[35];
    z[56]=z[9]*z[56];
    z[56]=z[70] + z[56];
    z[56]=z[9]*z[56];
    z[66]=z[15]*z[50];
    z[56]=z[66] + z[56];
    z[51]=z[56]*z[51];
    z[56]=n<T>(5,4)*z[11] - z[61];
    z[56]=z[8]*z[56];
    z[34]=z[51] + z[56] + z[34];
    z[34]=z[6]*z[34];
    z[51]=3*z[59] - z[30];
    z[51]=z[5]*z[51];
    z[56]=3*z[42];
    z[59]= - n<T>(5,2) - z[56];
    z[59]=n<T>(1,2)*z[59] + z[30];
    z[59]=z[3]*z[59];
    z[66]= - z[47]*z[49];
    z[70]=n<T>(7,4)*z[11] + z[10] + z[4];
    z[70]=z[3]*z[70];
    z[66]=z[70] + z[66];
    z[66]=z[8]*z[66];
    z[70]=3*z[10];
    z[76]=n<T>(13,2)*z[4] - z[70];
    z[34]=z[34] + z[71] + z[66] + z[59] + n<T>(1,2)*z[76] + z[51];
    z[34]=z[6]*z[34];
    z[51]=z[60] + z[12];
    z[59]=z[51]*z[7];
    z[66]=n<T>(3,4) - z[59];
    z[66]=z[66]*z[65];
    z[62]=z[66] + z[62] - z[68] + z[60] - n<T>(9,4)*z[75];
    z[62]=z[62]*z[50];
    z[66]=z[65]*z[7];
    z[71]= - static_cast<T>(5)+ z[66];
    z[75]=z[59] + n<T>(3,4);
    z[71]=z[50]*z[75]*z[71];
    z[51]=z[71] + n<T>(3,8)*z[10] - z[40] - z[63] + z[51];
    z[51]=z[3]*z[51];
    z[63]=z[4]*z[14];
    z[71]=z[10]*z[68];
    z[75]= - z[5]*z[53];
    z[71]=z[75] - 2*z[63] + z[71];
    z[71]=z[5]*z[71];
    z[75]=z[5]*z[10];
    z[38]=z[75] - 3*z[38];
    z[38]=z[11]*z[38];
    z[75]=z[55]*z[10];
    z[76]=z[44]*z[4];
    z[75]=z[75] - z[76];
    z[77]=n<T>(1,2)*z[9];
    z[78]=z[77]*z[5];
    z[79]=z[75]*z[78];
    z[38]=z[79] + n<T>(3,8)*z[38] + n<T>(3,4)*z[76] + z[71];
    z[38]=z[9]*z[38];
    z[71]=n<T>(9,2)*z[63];
    z[79]=z[4]*z[23];
    z[79]=z[79] + z[55] + z[71];
    z[80]= - z[69] + z[11];
    z[80]=z[80]*z[48];
    z[81]=z[4] + z[69];
    z[81]=z[5]*z[81];
    z[80]=z[81] + z[80];
    z[80]=z[3]*z[80];
    z[45]=z[45] + z[80];
    z[45]=z[45]*z[27];
    z[72]=n<T>(5,4)*z[10] + n<T>(1,2)*z[72] - z[12];
    z[72]=z[5]*z[72];
    z[19]=z[19] + z[22] + z[34] + z[38] + z[45] + z[51] + z[62] + n<T>(1,2)*
    z[79] + z[72];
    z[19]=z[1]*z[19];
    z[22]=z[44] + z[55];
    z[34]=n<T>(1,2)*z[60];
    z[38]= - z[12] + z[34];
    z[38]=z[10]*z[38];
    z[38]=z[38] + n<T>(17,4)*z[63] - z[22];
    z[45]=z[78] - n<T>(1,2);
    z[45]=z[75]*z[45];
    z[22]= - z[71] + z[22];
    z[20]=z[12]*z[20];
    z[20]=n<T>(1,2)*z[22] + z[20];
    z[20]=z[5]*z[20];
    z[20]=z[20] + z[45];
    z[20]=z[9]*z[20];
    z[22]=n<T>(7,2) - z[42];
    z[22]=z[14]*z[22];
    z[22]=z[22] + z[74];
    z[22]=z[70] + n<T>(1,2)*z[22] - z[4];
    z[22]=z[5]*z[22];
    z[34]=z[34] + z[12];
    z[45]=z[34]*z[7];
    z[51]=z[45] + n<T>(1,2);
    z[55]= - z[11]*z[51];
    z[34]=z[55] + n<T>(3,4)*z[4] - z[34];
    z[34]=z[34]*z[48];
    z[47]= - z[47]*z[40];
    z[47]= - z[76] + z[47];
    z[47]=z[47]*z[57];
    z[20]=z[20] + z[47] + z[34] + n<T>(1,2)*z[38] + z[22];
    z[20]=z[9]*z[20];
    z[22]=static_cast<T>(1)- z[42];
    z[22]=n<T>(5,2)*z[22] + z[30];
    z[22]=z[5]*z[22];
    z[34]= - n<T>(5,2) + z[42];
    z[34]=z[14]*z[34];
    z[22]=z[22] + z[10] - z[40] - z[60] + z[34] - z[74];
    z[34]=z[49] + 1;
    z[34]=z[44]*z[34];
    z[37]=z[37]*z[46];
    z[38]= - z[7]*z[50];
    z[38]= - static_cast<T>(1)+ z[38];
    z[38]=z[38]*z[48];
    z[40]= - z[14]*z[52];
    z[38]=z[40] + z[38];
    z[38]=z[3]*z[38];
    z[23]=z[4] + z[23];
    z[23]=z[5]*z[23];
    z[23]=z[38] + z[37] + z[23] - n<T>(3,4)*z[63] + z[34];
    z[23]=z[23]*z[27];
    z[37]=z[7]*z[69];
    z[37]=z[33] - static_cast<T>(1)+ z[37];
    z[38]=z[14]*z[64];
    z[34]=z[38] + z[34];
    z[34]=z[8]*z[34];
    z[38]= - z[14] + z[31];
    z[34]=n<T>(1,2)*z[38] + z[34];
    z[34]=z[8]*z[34];
    z[38]= - 3*z[14] - 7*z[10];
    z[40]=z[8]*z[44];
    z[38]=n<T>(1,2)*z[38] + z[40];
    z[38]=z[38]*z[77];
    z[34]=z[38] + n<T>(1,2)*z[37] + z[34];
    z[24]=z[24]*z[9];
    z[37]=n<T>(1,2)*z[28];
    z[38]= - z[37] + z[24];
    z[38]=z[9]*z[38];
    z[24]=z[28] - z[24];
    z[24]=z[24]*z[26];
    z[24]=z[38] + n<T>(1,4)*z[24];
    z[24]=z[6]*z[24];
    z[21]= - n<T>(5,2)*z[15] - 3*z[21];
    z[21]=z[9]*z[21];
    z[21]=z[37] + z[21];
    z[21]=n<T>(1,2)*z[21] + z[24];
    z[21]=z[6]*z[21];
    z[24]=z[41] + z[8];
    z[24]=z[24]*z[27];
    z[26]= - z[7]*z[53];
    z[18]=z[26] + z[18];
    z[18]=z[9]*z[18];
    z[18]=z[18] - n<T>(1,4)*z[7] - z[8];
    z[18]=z[9]*z[18];
    z[18]=z[21] + z[24] + z[18];
    z[18]=z[2]*z[18];
    z[21]= - static_cast<T>(1)- z[39];
    z[21]=z[9]*z[21];
    z[21]= - z[8] + z[21];
    z[21]=z[21]*z[77];
    z[21]=z[15] + z[21];
    z[21]=z[9]*z[21];
    z[24]=z[58]*z[25];
    z[16]= - z[16] + z[24];
    z[16]=z[9]*z[16];
    z[16]=z[37] + z[16];
    z[16]=z[6]*z[16]*z[77];
    z[16]=z[16] - z[29] + z[21];
    z[16]=z[6]*z[16];
    z[21]=z[25]*z[70];
    z[21]=z[32] + z[21];
    z[21]=z[9]*z[21];
    z[21]= - n<T>(3,2)*z[15] + z[21];
    z[16]=n<T>(1,2)*z[21] + z[16];
    z[16]=z[6]*z[16];
    z[21]=z[10]*z[7];
    z[21]= - 5*z[17] + static_cast<T>(3)+ z[21];
    z[21]=z[9]*z[21];
    z[24]= - static_cast<T>(5)- z[33];
    z[24]=z[8]*z[24];
    z[21]=z[21] - z[7] + z[24];
    z[16]=z[18] + n<T>(1,8)*z[21] + z[16];
    z[16]=z[2]*z[16];
    z[18]= - static_cast<T>(1)- n<T>(1,2)*z[17];
    z[18]=z[18]*z[25];
    z[21]= - z[8] + z[9];
    z[21]=z[9]*z[21];
    z[21]= - z[15] + z[21];
    z[21]=z[9]*z[21];
    z[21]=z[28] + z[21];
    z[21]=z[21]*z[54];
    z[18]=z[21] + z[43] + z[18];
    z[18]=z[6]*z[18];
    z[17]=static_cast<T>(1)+ n<T>(5,4)*z[17];
    z[17]=z[9]*z[17];
    z[17]=z[18] + z[27] + z[17];
    z[17]=z[6]*z[17];
    z[16]=z[16] + n<T>(1,2)*z[34] + z[17];
    z[16]=z[2]*z[16];
    z[17]= - n<T>(3,4) - z[45];
    z[17]=z[17]*z[36]*z[65];
    z[18]=7*z[12] + z[73];
    z[18]=z[7]*z[18];
    z[18]=n<T>(13,2) + z[18];
    z[18]=z[18]*z[41];
    z[17]=z[18] + z[17];
    z[17]=z[11]*z[17];
    z[18]= - static_cast<T>(1)+ z[56];
    z[17]=z[17] - z[59] + n<T>(1,2)*z[18] - z[30];
    z[17]=z[17]*z[31];
    z[18]= - z[61]*z[27];
    z[21]=z[11] - z[4];
    z[24]=n<T>(1,2)*z[67] + z[10] - n<T>(1,4)*z[21];
    z[24]=z[9]*z[24];
    z[18]=z[18] + z[24];
    z[18]=z[9]*z[18];
    z[24]= - z[15]*z[46];
    z[21]= - z[35] + z[21];
    z[21]=z[9]*z[21];
    z[25]=z[11] + z[61];
    z[25]=z[8]*z[25];
    z[21]=z[25] + z[21];
    z[21]=z[9]*z[21];
    z[15]=z[11]*z[15];
    z[15]=z[15] + z[21];
    z[15]=z[9]*z[15];
    z[21]= - z[11]*z[28];
    z[15]=z[21] + z[15];
    z[15]=z[15]*z[54];
    z[15]=z[15] + z[24] + z[18];
    z[15]=z[6]*z[15];
    z[18]= - z[67] + z[4] - 5*z[10];
    z[18]=z[18]*z[77];
    z[21]=n<T>(1,2)*z[61] - z[11];
    z[21]=z[8]*z[21];
    z[18]=z[21] + z[18];
    z[15]=n<T>(1,2)*z[18] + z[15];
    z[15]=z[6]*z[15];
    z[18]= - z[68] - z[60];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(25,4) + z[18];
    z[21]=z[51]*z[66];
    z[18]=n<T>(1,2)*z[18] + z[21];
    z[18]=z[11]*z[18];
    z[15]=z[19] + z[16] + z[15] + z[20] + z[23] + z[17] + n<T>(1,2)*z[22] + 
    z[18];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r339(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r339(const std::array<dd_real,31>&);
#endif
