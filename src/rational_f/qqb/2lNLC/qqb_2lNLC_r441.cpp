#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r441(const std::array<T,31>& k) {
  T z[57];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[5];
    z[10]=k[4];
    z[11]=k[15];
    z[12]=k[9];
    z[13]=z[10] + z[9];
    z[14]=npow(z[10],2);
    z[15]= - z[6]*z[14];
    z[15]=z[15] - z[13];
    z[16]=2*z[7];
    z[15]=z[15]*z[16];
    z[17]=z[10]*z[11];
    z[18]=z[17] + 3;
    z[19]=z[6]*z[10];
    z[15]=z[15] + 2*z[18] + 7*z[19];
    z[15]=z[7]*z[15];
    z[20]=z[10]*z[9];
    z[21]= - z[16]*z[20];
    z[22]=z[10] - z[9];
    z[21]= - 3*z[22] + z[21];
    z[21]=z[7]*z[21];
    z[23]=2*z[10];
    z[24]=z[23] + z[9];
    z[25]=3*z[5];
    z[26]=z[24]*z[25];
    z[21]=z[21] + z[26];
    z[21]=z[4]*z[21];
    z[26]=z[17] + 2;
    z[27]=z[2]*z[8];
    z[28]=2*z[26] - 5*z[27];
    z[28]=z[3]*z[28];
    z[29]=5*z[6];
    z[30]=z[3]*z[10];
    z[31]=static_cast<T>(4)- z[30];
    z[31]=z[5]*z[31];
    z[15]=z[21] + z[31] + z[15] - z[29] + z[28];
    z[15]=z[4]*z[15];
    z[21]=z[2]*z[9];
    z[28]=z[21] + 2;
    z[31]=2*z[2];
    z[32]=z[28]*z[31];
    z[33]=npow(z[6],2);
    z[34]=z[33]*z[23];
    z[35]=z[19] + 1;
    z[36]= - z[21] - z[35];
    z[36]=z[36]*z[16];
    z[37]=npow(z[11],2);
    z[38]=z[37]*z[10];
    z[38]=z[38] + z[11];
    z[38]=3*z[38];
    z[34]=z[36] + z[34] + z[38] + z[32];
    z[34]=z[7]*z[34];
    z[36]=4*z[11];
    z[39]=static_cast<T>(3)+ z[21];
    z[39]=z[2]*z[39];
    z[39]=z[36] + z[39];
    z[40]=z[5]*z[3];
    z[41]= - z[23]*z[40];
    z[42]=static_cast<T>(7)- z[30];
    z[42]=z[3]*z[42];
    z[39]=z[41] + 2*z[39] + z[42];
    z[39]=z[5]*z[39];
    z[41]=7*z[2];
    z[42]=2*z[6];
    z[43]=z[41] - z[42];
    z[43]=z[6]*z[43];
    z[18]= - z[3]*z[18];
    z[18]=z[18] + z[38] + z[2];
    z[18]=z[3]*z[18];
    z[15]=z[15] + z[39] + z[34] + z[43] + z[18];
    z[15]=z[4]*z[15];
    z[18]=3*z[2];
    z[34]=z[18] + z[42];
    z[39]= - z[35]*z[16];
    z[34]=3*z[34] + z[39];
    z[34]=z[7]*z[34];
    z[39]=z[16]*z[9];
    z[43]= - static_cast<T>(1)+ z[21];
    z[43]=3*z[43] - z[39];
    z[43]=z[7]*z[43];
    z[28]=z[30] + z[28];
    z[28]=z[28]*z[25];
    z[28]=z[28] - 3*z[3] + z[43];
    z[28]=z[4]*z[28];
    z[43]=z[18]*z[8];
    z[44]= - static_cast<T>(1)- z[43];
    z[44]=z[3]*z[44];
    z[41]=z[41] + z[44];
    z[41]=z[3]*z[41];
    z[44]=z[6]*z[2];
    z[45]= - z[31] + z[3];
    z[45]=z[5]*z[45];
    z[28]=z[28] + z[45] + z[34] - 4*z[44] + z[41];
    z[28]=z[4]*z[28];
    z[34]=npow(z[2],2);
    z[41]=z[3]*z[11];
    z[41]=z[41] - z[37] - 2*z[34];
    z[41]=z[3]*z[41];
    z[45]=npow(z[3],2);
    z[46]= - z[5]*z[45]*z[23];
    z[47]=2*z[3];
    z[48]=z[2] + z[47];
    z[48]=z[3]*z[48];
    z[46]=z[46] + 4*z[34] + z[48];
    z[46]=z[5]*z[46];
    z[48]= - z[37] - 9*z[44];
    z[48]=z[7]*z[48];
    z[28]=z[28] + z[46] + z[41] + z[48];
    z[28]=z[4]*z[28];
    z[41]=npow(z[7],2);
    z[44]=z[41]*z[44];
    z[46]=z[34]*z[40];
    z[44]=z[44] + z[46];
    z[46]=z[31]*z[9];
    z[48]= - z[7]*z[46];
    z[48]= - z[18] + z[48];
    z[48]=z[7]*z[48];
    z[49]=z[31] + z[3];
    z[50]=z[49]*z[25];
    z[51]=static_cast<T>(1)+ z[27];
    z[51]=z[3]*z[51];
    z[51]= - 4*z[2] + z[51];
    z[51]=z[3]*z[51];
    z[48]=z[50] + z[51] + z[48];
    z[48]=z[4]*z[48];
    z[18]=z[6]*z[18];
    z[50]=z[6] + z[2];
    z[51]= - z[7]*z[50];
    z[18]=z[18] + z[51];
    z[18]=z[7]*z[18];
    z[51]= - z[2]*z[40];
    z[18]=z[18] + z[51];
    z[18]=2*z[18] + z[48];
    z[18]=z[4]*z[18];
    z[18]=2*z[44] + z[18];
    z[18]=z[4]*z[18];
    z[44]=npow(z[6],3);
    z[48]=2*z[44];
    z[51]= - z[3]*z[33];
    z[52]=z[3]*z[6];
    z[53]= - z[7]*z[52];
    z[51]=z[53] + z[48] + z[51];
    z[51]=z[7]*z[51];
    z[53]=z[44]*z[3];
    z[51]= - z[53] + z[51];
    z[51]=z[5]*z[51];
    z[53]= - z[7]*z[53];
    z[51]=z[53] + z[51];
    z[40]=z[44]*z[16]*z[40];
    z[41]= - z[41]*z[31]*z[6];
    z[53]=z[4]*z[3]*z[2]*z[25];
    z[41]=z[41] + z[53];
    z[41]=z[41]*npow(z[4],2);
    z[40]=z[40] + z[41];
    z[40]=z[1]*z[40];
    z[18]=z[40] + 2*z[51] + z[18];
    z[18]=z[1]*z[18];
    z[40]=2*z[33];
    z[41]=z[6]*z[9];
    z[51]= - static_cast<T>(1)+ z[41];
    z[51]=z[51]*z[40];
    z[53]= - z[42] - z[3];
    z[53]=z[53]*z[16];
    z[54]=z[6] - z[47];
    z[54]=z[3]*z[54];
    z[51]=z[53] + z[51] + z[54];
    z[51]=z[7]*z[51];
    z[53]=z[2]*z[10];
    z[54]=z[53] - z[19];
    z[54]=z[3]*z[54];
    z[54]=z[54] + z[2] - z[6];
    z[54]=z[3]*z[54];
    z[55]=z[7]*z[10];
    z[45]=z[45]*z[55];
    z[45]=z[54] + z[45];
    z[54]=2*z[5];
    z[45]=z[45]*z[54];
    z[56]= - z[30] + z[35];
    z[56]=z[33]*z[56];
    z[56]= - z[34] + z[56];
    z[56]=z[3]*z[56];
    z[44]= - z[44] + z[56];
    z[44]=z[45] + 2*z[44] + z[51];
    z[44]=z[5]*z[44];
    z[45]=z[19] + 2;
    z[40]=z[45]*z[40];
    z[35]= - z[35]*z[42];
    z[35]= - z[11] + z[35];
    z[35]=z[3]*z[35];
    z[35]=z[35] + z[37] + z[40];
    z[35]=z[3]*z[35];
    z[37]=z[16]*z[52];
    z[35]=z[37] - z[48] + z[35];
    z[35]=z[7]*z[35];
    z[18]=z[18] + z[28] + z[35] + z[44];
    z[18]=z[1]*z[18];
    z[28]=3*z[6];
    z[35]= - z[45]*z[28];
    z[35]= - z[31] + z[35];
    z[37]=z[31]*z[10];
    z[40]= - z[37] + 3*z[19];
    z[40]=z[3]*z[40];
    z[35]=2*z[35] + z[40];
    z[35]=z[3]*z[35];
    z[40]= - static_cast<T>(1)- z[30];
    z[40]=z[3]*z[40];
    z[41]= - static_cast<T>(1)- z[41];
    z[41]=z[7]*z[41];
    z[28]=z[41] + z[28] + z[40];
    z[28]=z[28]*z[16];
    z[40]=z[37] - z[19];
    z[40]=z[3]*z[40];
    z[30]=z[16]*z[30];
    z[30]=z[30] + z[40] + z[31] - z[6];
    z[30]=z[30]*z[54];
    z[24]=z[24]*z[33];
    z[24]= - 4*z[12] + z[24];
    z[24]=z[6]*z[24];
    z[24]= - z[34] + z[24];
    z[24]=z[30] + z[28] + 2*z[24] + z[35];
    z[24]=z[5]*z[24];
    z[26]= - z[19] + z[26];
    z[26]=z[3]*z[26];
    z[28]=8*z[12];
    z[30]= - static_cast<T>(3)- z[19];
    z[30]=z[6]*z[30];
    z[26]=z[26] + z[30] + z[28] - z[38];
    z[26]=z[3]*z[26];
    z[30]= - z[6]*z[32];
    z[32]=z[6]*z[21];
    z[32]=z[32] + z[3];
    z[32]=z[32]*z[16];
    z[26]=z[32] + z[30] + z[26];
    z[26]=z[7]*z[26];
    z[30]=z[36] - z[2];
    z[32]= - z[2]*z[30];
    z[34]=z[6]*z[50];
    z[32]=z[52] + z[32] + z[34];
    z[32]=z[3]*z[32];
    z[33]= - z[2]*z[33];
    z[32]=z[33] + z[32];
    z[15]=z[18] + z[15] + z[24] + 2*z[32] + z[26];
    z[15]=z[1]*z[15];
    z[18]=npow(z[9],2);
    z[18]=z[18] + z[20];
    z[18]=z[18]*z[6];
    z[24]= - z[10] + z[18];
    z[24]=z[6]*z[24];
    z[26]=2*z[9];
    z[32]= - z[12]*z[26];
    z[32]= - static_cast<T>(3)+ z[32];
    z[24]=2*z[32] + z[24];
    z[24]=z[6]*z[24];
    z[32]=2*z[19];
    z[33]= - z[32] - static_cast<T>(1)- z[37];
    z[33]=z[3]*z[33];
    z[34]=z[42]*z[9];
    z[34]=z[34] - 1;
    z[23]=z[23]*z[3];
    z[35]= - z[23] + z[34];
    z[35]=z[7]*z[35];
    z[36]=z[2]*z[13];
    z[36]=z[55] + static_cast<T>(4)+ z[36];
    z[36]=z[5]*z[36];
    z[24]=z[36] + z[35] + z[33] - z[31] + z[24];
    z[24]=z[24]*z[54];
    z[14]= - z[14]*z[39];
    z[14]=3*z[20] + z[14];
    z[14]=z[7]*z[14];
    z[33]=z[20]*z[25];
    z[14]=z[14] + z[33];
    z[14]=z[4]*z[14];
    z[33]=z[20]*z[7];
    z[35]=z[33] - z[10];
    z[36]=3*z[9];
    z[35]=z[36] - 4*z[35];
    z[35]=z[7]*z[35];
    z[22]= - z[22]*z[25];
    z[14]=z[14] + z[22] - static_cast<T>(5)+ z[35];
    z[14]=z[4]*z[14];
    z[22]=z[7]*z[9];
    z[17]=5*z[17];
    z[22]= - 4*z[22] - z[32] + static_cast<T>(8)- z[17];
    z[22]=z[7]*z[22];
    z[25]=z[9]*z[11];
    z[23]= - z[23] + 4*z[21] + static_cast<T>(3)+ 8*z[25];
    z[23]=z[5]*z[23];
    z[25]=6*z[27] + static_cast<T>(1)- z[17];
    z[25]=z[3]*z[25];
    z[27]=static_cast<T>(8)- z[43];
    z[27]=z[6]*z[27];
    z[14]=z[14] + z[23] + z[22] + z[25] - 9*z[2] + z[27];
    z[14]=z[4]*z[14];
    z[17]= - 11*z[19] - static_cast<T>(6)+ z[17];
    z[17]=z[3]*z[17];
    z[17]=z[16] + z[17] + z[28] - 13*z[6];
    z[17]=z[7]*z[17];
    z[19]= - z[30]*z[31];
    z[22]=z[8]*z[31];
    z[22]=z[22] - z[34];
    z[22]=z[6]*z[22];
    z[23]=static_cast<T>(5)+ z[46];
    z[23]=z[2]*z[23];
    z[22]=z[23] + z[22];
    z[22]=z[6]*z[22];
    z[23]=z[29] + z[49];
    z[23]=z[23]*z[47];
    z[14]=z[15] + z[14] + z[24] + z[17] + z[23] + z[19] + z[22];
    z[14]=z[1]*z[14];
    z[15]=z[18] + z[10] + 4*z[9];
    z[15]=z[6]*z[15];
    z[13]=z[13]*z[7];
    z[17]=z[5]*z[26];
    z[15]=z[17] - z[13] + z[15] + static_cast<T>(4)- z[53];
    z[15]=z[5]*z[15];
    z[17]= - z[5]*z[20];
    z[17]= - z[33] + z[17];
    z[17]=z[4]*z[17];
    z[18]= - z[36] + z[10];
    z[18]=z[5]*z[18];
    z[13]=z[17] + z[18] - static_cast<T>(2)- z[13];
    z[13]=z[4]*z[13];
    z[17]=z[9] - z[8];
    z[18]=z[17]*z[21];
    z[18]= - z[26] + z[18];
    z[18]=z[6]*z[18];
    z[17]= - z[2]*z[17];
    z[17]=z[18] + static_cast<T>(2)+ z[17];
    z[17]=z[6]*z[17];
    z[13]=z[13] + z[15] - z[16] + z[47] + z[31] + z[17];
    z[13]=2*z[13] + z[14];

    r += z[13]*z[1];
 
    return r;
}

template double qqb_2lNLC_r441(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r441(const std::array<dd_real,31>&);
#endif
