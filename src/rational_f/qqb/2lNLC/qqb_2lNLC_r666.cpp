#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r666(const std::array<T,31>& k) {
  T z[132];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[8];
    z[9]=k[9];
    z[10]=k[2];
    z[11]=k[3];
    z[12]=k[4];
    z[13]=k[15];
    z[14]=k[5];
    z[15]=npow(z[1],2);
    z[16]=21*z[15];
    z[17]=z[14]*z[1];
    z[18]=13*z[17];
    z[19]= - z[16] - z[18];
    z[20]=n<T>(1,2)*z[14];
    z[19]=z[19]*z[20];
    z[21]=npow(z[1],3);
    z[22]=4*z[21];
    z[19]= - z[22] + z[19];
    z[19]=z[14]*z[19];
    z[23]=2*z[15];
    z[24]=z[23] + z[17];
    z[25]=z[24]*z[14];
    z[25]=z[25] + z[21];
    z[26]=npow(z[14],2);
    z[27]=z[26]*z[2];
    z[28]=z[25]*z[27];
    z[19]=z[19] + z[28];
    z[19]=z[2]*z[19];
    z[28]=45*z[15] + z[18];
    z[28]=z[28]*z[20];
    z[29]=z[25]*z[6];
    z[30]= - z[14] + z[27];
    z[30]=z[30]*z[29];
    z[31]=16*z[21];
    z[19]=z[30] + z[19] + z[31] + z[28];
    z[19]=z[6]*z[19];
    z[28]=5*z[21];
    z[30]=z[15]*z[14];
    z[32]=3*z[30];
    z[33]=z[28] + z[32];
    z[33]=z[14]*z[33];
    z[34]=npow(z[1],4);
    z[35]=2*z[34];
    z[36]=z[21]*z[14];
    z[37]=z[35] + z[36];
    z[37]=z[37]*z[14];
    z[38]=npow(z[1],5);
    z[37]=z[37] + z[38];
    z[39]= - z[9]*z[37];
    z[33]=z[39] + z[35] + z[33];
    z[39]=4*z[9];
    z[33]=z[33]*z[39];
    z[40]=17*z[17];
    z[41]= - 49*z[15] - z[40];
    z[41]=z[41]*z[20];
    z[42]=z[17] + z[15];
    z[43]=z[14]*z[42];
    z[43]= - z[21] + z[43];
    z[43]=z[14]*z[43];
    z[43]= - z[34] + z[43];
    z[43]=z[4]*z[43];
    z[33]=z[43] + z[33] - z[31] + z[41];
    z[33]=z[4]*z[33];
    z[41]=z[30] + z[21];
    z[43]=z[41]*z[14];
    z[44]=z[26]*z[4];
    z[45]=z[25]*z[44];
    z[46]= - n<T>(21,2)*z[43] + z[45];
    z[46]=z[4]*z[46];
    z[47]=z[42]*z[26];
    z[48]=n<T>(1,2)*z[17];
    z[49]=z[15] + z[48];
    z[49]=z[14]*z[49];
    z[50]=n<T>(1,2)*z[21];
    z[49]=z[50] + z[49];
    z[49]=z[49]*z[44];
    z[49]= - n<T>(1,2)*z[47] + z[49];
    z[51]=3*z[7];
    z[49]=z[49]*z[51];
    z[52]=3*z[15];
    z[53]= - z[52] + z[17];
    z[53]=z[53]*z[20];
    z[54]=z[36]*z[13];
    z[46]=z[49] + 12*z[54] + z[53] + z[46];
    z[46]=z[7]*z[46];
    z[49]=7*z[15];
    z[53]=3*z[17];
    z[55]=z[49] + z[53];
    z[55]=z[55]*z[20];
    z[56]=z[24]*z[27];
    z[55]=z[55] + z[56];
    z[55]=z[2]*z[55];
    z[32]=z[32] + z[21];
    z[56]=z[2]*z[14];
    z[57]= - z[32]*z[56];
    z[58]=z[21]*z[2];
    z[59]=z[58]*z[26];
    z[60]=z[34] + z[59];
    z[60]=z[9]*z[60];
    z[57]=z[57] + z[60];
    z[57]=z[57]*z[39];
    z[60]=z[36] + z[34];
    z[61]=z[60]*z[6];
    z[62]=z[61]*z[13];
    z[19]=z[46] - 12*z[62] + z[33] + z[57] + z[55] + z[19];
    z[19]=z[8]*z[19];
    z[33]=2*z[14];
    z[46]=z[41]*z[33];
    z[55]=z[46]*z[4];
    z[57]=n<T>(11,2)*z[15];
    z[63]=z[57] - z[53];
    z[63]=z[14]*z[63];
    z[63]=z[55] - n<T>(27,2)*z[21] + z[63];
    z[63]=z[4]*z[63];
    z[64]=z[33]*z[15];
    z[65]= - n<T>(5,2)*z[54] + n<T>(21,2)*z[21] - z[64];
    z[65]=z[13]*z[65];
    z[66]=z[42]*z[44];
    z[67]=z[26]*z[1];
    z[66]=n<T>(3,2)*z[67] - 2*z[66];
    z[66]=z[66]*z[51];
    z[68]=n<T>(3,2)*z[17];
    z[63]=z[66] + z[65] + z[63] - z[23] - z[68];
    z[63]=z[7]*z[63];
    z[65]=z[34]*z[2];
    z[66]=z[22] + z[65];
    z[66]=z[2]*z[66];
    z[69]= - z[9]*z[38];
    z[69]=z[35] + z[69];
    z[69]=z[9]*z[69];
    z[70]=2*z[21];
    z[69]= - z[70] + z[69];
    z[69]=z[9]*z[69];
    z[71]=14*z[13];
    z[72]=z[21]*z[71];
    z[73]=z[21]*z[7];
    z[74]=4*z[15];
    z[66]=6*z[73] + z[72] + 8*z[69] - z[74] + z[66];
    z[66]=z[5]*z[66];
    z[69]=25*z[15] + n<T>(49,2)*z[17];
    z[69]=z[14]*z[69];
    z[72]=z[42]*z[27];
    z[75]=n<T>(3,2)*z[21];
    z[69]= - 4*z[72] + z[75] + z[69];
    z[69]=z[2]*z[69];
    z[72]=z[56] - 1;
    z[41]=z[6]*z[41]*z[72];
    z[72]=11*z[15];
    z[18]=2*z[41] + z[69] - z[72] - z[18];
    z[18]=z[6]*z[18];
    z[41]=z[9]*z[60];
    z[41]=n<T>(23,2)*z[41] - n<T>(17,2)*z[21] - 24*z[30];
    z[41]=z[9]*z[41];
    z[69]=z[74] + z[17];
    z[69]=z[14]*z[69];
    z[69]=z[22] + z[69];
    z[69]=z[4]*z[69];
    z[41]=z[69] + z[41] + z[72] + 15*z[17];
    z[41]=z[4]*z[41];
    z[69]=z[48] + z[23];
    z[76]=2*z[17];
    z[77]=z[76] + z[15];
    z[78]= - z[77]*z[33];
    z[78]= - z[21] + z[78];
    z[78]=z[2]*z[78];
    z[78]=z[78] - z[69];
    z[78]=z[2]*z[78];
    z[79]=z[34]*z[9];
    z[80]=z[58]*z[14];
    z[81]=8*z[79] - 24*z[21] - n<T>(7,2)*z[80];
    z[81]=z[9]*z[81];
    z[82]=n<T>(7,2)*z[21];
    z[83]=16*z[30];
    z[84]= - z[82] + z[83];
    z[84]=z[2]*z[84];
    z[81]=z[81] + 8*z[15] + z[84];
    z[81]=z[9]*z[81];
    z[84]=n<T>(23,2)*z[21];
    z[85]=z[84] + z[64];
    z[85]=z[6]*z[85];
    z[62]=z[85] + n<T>(5,2)*z[62];
    z[62]=z[13]*z[62];
    z[85]=3*z[6];
    z[86]=z[85]*z[21];
    z[87]=z[21]*z[9];
    z[88]=4*z[87] - z[23] + z[86];
    z[89]=z[4]*z[34];
    z[22]= - z[22] + z[89];
    z[22]=z[4]*z[22];
    z[89]=z[13]*z[70];
    z[22]=z[89] + 2*z[88] + z[22];
    z[22]=z[3]*z[22];
    z[18]=z[19] + z[22] + z[66] + z[63] + z[62] + z[41] + z[81] + z[78]
    + z[18];
    z[18]=z[8]*z[18];
    z[19]=npow(z[1],6);
    z[22]=npow(z[2],2);
    z[41]= - z[19]*z[22];
    z[41]=n<T>(21,4)*z[34] + z[41];
    z[41]=z[2]*z[41];
    z[62]=z[38]*z[2];
    z[63]=z[62] + z[34];
    z[66]=z[4]*z[63];
    z[78]=n<T>(33,4)*z[21];
    z[41]=z[66] + z[78] + z[41];
    z[41]=z[4]*z[41];
    z[66]=npow(z[6],2);
    z[19]=z[19]*z[66];
    z[19]= - n<T>(9,2)*z[34] + z[19];
    z[19]=z[6]*z[19];
    z[81]=z[38]*z[6];
    z[88]= - z[34] - z[81];
    z[88]=z[7]*z[88];
    z[19]=z[88] - n<T>(15,2)*z[21] + z[19];
    z[19]=z[7]*z[19];
    z[62]=z[62] - z[34];
    z[62]=z[62]*z[2];
    z[88]=z[21] + z[62];
    z[88]=z[2]*z[88];
    z[89]=z[35] - z[81];
    z[89]=z[6]*z[89];
    z[89]=n<T>(5,4)*z[21] + z[89];
    z[89]=z[6]*z[89];
    z[19]=z[19] + z[41] + z[89] + z[23] + z[88];
    z[19]=z[5]*z[19];
    z[41]=z[38]*z[22];
    z[41]= - z[78] + z[41];
    z[41]=z[2]*z[41];
    z[78]= - 17*z[21] + 7*z[79];
    z[78]=z[9]*z[78];
    z[88]= - z[70] - z[65];
    z[88]=z[4]*z[88];
    z[41]=z[88] + n<T>(1,2)*z[78] + z[57] + z[41];
    z[41]=z[4]*z[41];
    z[78]=z[34] - z[81];
    z[78]=z[6]*z[78];
    z[81]=6*z[21];
    z[78]=z[81] + z[78];
    z[78]=z[6]*z[78];
    z[88]=z[34]*z[6];
    z[89]= - z[70] + z[88];
    z[89]=z[7]*z[89];
    z[90]=n<T>(3,2)*z[15];
    z[78]=z[89] + n<T>(1,2)*z[87] + z[90] + z[78];
    z[78]=z[7]*z[78];
    z[89]=5*z[15];
    z[91]=z[89] + z[58];
    z[92]= - z[86] - z[91];
    z[93]=n<T>(1,2)*z[6];
    z[92]=z[92]*z[93];
    z[86]= - z[86] - 41*z[15] - 27*z[58];
    z[86]=z[13]*z[86];
    z[94]=z[88] + z[21];
    z[95]=n<T>(1,2)*z[7];
    z[96]=z[94]*z[95];
    z[97]=z[4]*z[50];
    z[96]=z[96] + z[15] + z[97];
    z[96]=z[3]*z[96];
    z[97]=z[21] - z[65];
    z[97]=z[2]*z[97];
    z[97]=z[72] + z[97];
    z[97]=z[2]*z[97];
    z[98]=18*z[15] - n<T>(25,2)*z[87];
    z[98]=z[9]*z[98];
    z[19]=z[96] + z[19] + z[78] + n<T>(1,2)*z[86] + z[41] + z[98] + z[97] + 
    z[92];
    z[19]=z[3]*z[19];
    z[41]=npow(z[9],2);
    z[78]= - z[66]*z[41]*z[75];
    z[86]=z[23]*z[22];
    z[92]= - z[6]*z[86];
    z[96]=z[21]*z[6];
    z[97]=z[4]*z[22]*z[96];
    z[78]=z[97] + z[92] + z[78];
    z[78]=z[4]*z[78];
    z[92]=z[21]*z[4];
    z[97]=z[22]*z[92];
    z[97]= - z[86] + z[97];
    z[97]=z[4]*z[97];
    z[98]=22*z[15];
    z[99]= - z[98] + 7*z[87];
    z[100]=2*z[9];
    z[99]=z[99]*z[100];
    z[101]= - z[23] - z[87];
    z[101]=z[7]*z[101];
    z[99]=z[101] + n<T>(77,2)*z[1] + z[99];
    z[99]=z[7]*z[9]*z[99];
    z[101]=z[15]*z[9];
    z[102]=14*z[101];
    z[103]= - 15*z[1] + z[102];
    z[103]=z[103]*z[41];
    z[104]=z[22]*z[1];
    z[97]=z[99] + z[97] + z[104] + 2*z[103];
    z[97]=z[3]*z[97];
    z[99]=z[66]*z[90];
    z[103]= - z[6]*z[67];
    z[103]= - z[53] + z[103];
    z[103]=z[103]*z[85];
    z[103]=32*z[1] + z[103];
    z[103]=z[9]*z[103];
    z[99]=z[99] + z[103];
    z[99]=z[99]*z[41];
    z[103]=26*z[15] + 37*z[17];
    z[103]=z[103]*z[100];
    z[103]= - n<T>(179,2)*z[1] + z[103];
    z[103]=z[103]*z[41];
    z[105]=z[41]*z[7];
    z[106]= - z[24]*z[105];
    z[103]=z[103] + z[106];
    z[103]=z[7]*z[103];
    z[102]=17*z[1] - z[102];
    z[102]=z[102]*z[41];
    z[106]=z[15]*z[105];
    z[102]=z[102] + z[106];
    z[102]=z[7]*z[102];
    z[106]=npow(z[9],3);
    z[107]=z[106]*z[1];
    z[102]= - 7*z[107] + z[102];
    z[102]=z[3]*z[102];
    z[105]=z[105]*z[1];
    z[108]= - 26*z[107] + z[105];
    z[108]=z[7]*z[108];
    z[102]=z[108] + z[102];
    z[105]=14*z[107] - z[105];
    z[108]=z[11]*z[3];
    z[105]=z[7]*z[105]*z[108];
    z[102]=2*z[102] + z[105];
    z[102]=z[11]*z[102];
    z[105]=z[6]*z[104];
    z[78]=z[102] + z[97] + z[103] + z[78] + z[105] + z[99];
    z[78]=z[11]*z[78];
    z[81]=z[81] + z[65];
    z[81]=z[2]*z[81];
    z[81]=n<T>(23,4)*z[15] + z[81];
    z[81]=z[2]*z[81];
    z[97]=z[15] - z[87];
    z[97]=z[9]*z[97];
    z[99]=z[58]*z[4];
    z[81]=z[99] + z[81] + n<T>(7,2)*z[97];
    z[81]=z[4]*z[81];
    z[97]=13*z[15];
    z[102]= - z[97] + 5*z[87];
    z[100]=z[102]*z[100];
    z[102]=z[90] + 2*z[87];
    z[102]=z[7]*z[102];
    z[100]=z[102] + 23*z[1] + z[100];
    z[100]=z[7]*z[100];
    z[91]= - z[2]*z[91];
    z[102]=n<T>(17,2)*z[1];
    z[91]=z[102] + z[91];
    z[91]=z[2]*z[91];
    z[103]=z[15]*z[2];
    z[105]= - z[103]*z[93];
    z[109]=n<T>(85,2)*z[15] - 14*z[87];
    z[109]=z[9]*z[109];
    z[109]= - n<T>(51,2)*z[1] + z[109];
    z[109]=z[9]*z[109];
    z[110]=z[3]*z[99];
    z[81]=n<T>(1,2)*z[110] + z[100] + z[81] + z[109] + z[91] + z[105];
    z[81]=z[3]*z[81];
    z[91]=z[85]*z[14];
    z[69]= - z[69]*z[91];
    z[69]=z[69] + z[15] + 10*z[17];
    z[69]=z[6]*z[69];
    z[100]=z[85]*z[26];
    z[105]= - z[42]*z[100];
    z[105]=13*z[67] + z[105];
    z[105]=z[6]*z[105];
    z[16]=z[105] - z[16] - 20*z[17];
    z[16]=z[9]*z[16];
    z[16]=z[16] + 48*z[1] + z[69];
    z[16]=z[9]*z[16];
    z[69]=4*z[1];
    z[105]= - z[6]*z[74];
    z[105]=z[105] + z[69] - n<T>(3,2)*z[103];
    z[105]=z[6]*z[105];
    z[16]=z[105] + z[16];
    z[16]=z[9]*z[16];
    z[105]= - n<T>(13,4)*z[15] - 3*z[58];
    z[105]=z[2]*z[105];
    z[109]=z[6]*z[58];
    z[99]= - z[99] + z[105] - n<T>(3,4)*z[109];
    z[99]=z[6]*z[99];
    z[105]=9*z[15];
    z[109]=z[105] + 11*z[96];
    z[109]=z[109]*z[93];
    z[110]=7*z[9];
    z[111]= - z[96]*z[110];
    z[109]=z[109] + z[111];
    z[109]=z[9]*z[109];
    z[86]=z[109] + z[86] + z[99];
    z[86]=z[4]*z[86];
    z[99]=z[17] - z[15];
    z[109]=3*z[2];
    z[111]= - z[99]*z[109];
    z[112]=n<T>(1,4)*z[15];
    z[113]= - z[112] - z[17];
    z[113]=z[6]*z[113];
    z[102]=z[113] + z[102] + z[111];
    z[102]=z[6]*z[2]*z[102];
    z[111]=z[9]*z[67];
    z[111]= - 16*z[111] + 15*z[15] + n<T>(53,2)*z[17];
    z[111]=z[9]*z[111];
    z[111]= - n<T>(49,2)*z[1] + z[111];
    z[113]=3*z[9];
    z[111]=z[111]*z[113];
    z[114]=n<T>(1,2)*z[1];
    z[115]=z[114] + z[101];
    z[115]=z[7]*z[115];
    z[111]=z[111] + z[115];
    z[111]=z[7]*z[111];
    z[115]=6*z[1];
    z[116]= - z[115] - z[103];
    z[117]=z[12]*z[3];
    z[116]=z[116]*z[22]*z[117];
    z[16]=z[78] + z[116] + z[81] + z[111] + z[86] + z[16] - 5*z[104] + 
    z[102];
    z[16]=z[11]*z[16];
    z[78]=16*z[15];
    z[81]=7*z[17];
    z[86]= - z[78] - z[81];
    z[86]=z[14]*z[86];
    z[86]=z[31] + z[86];
    z[86]=z[9]*z[86];
    z[78]=z[86] - z[78] + z[53];
    z[78]=z[9]*z[78];
    z[86]=n<T>(3,2)*z[1];
    z[78]= - z[86] + z[78];
    z[78]=z[9]*z[78];
    z[102]=17*z[67];
    z[111]=z[13]*z[102];
    z[111]=z[111] - 12*z[15] + 25*z[17];
    z[111]=z[13]*z[111];
    z[111]=n<T>(19,2)*z[1] + z[111];
    z[111]=z[13]*z[111];
    z[78]=z[78] + z[111];
    z[78]=z[5]*z[78];
    z[111]= - z[15] + 11*z[87];
    z[111]=z[9]*z[111];
    z[111]=z[86] + z[111];
    z[111]=z[9]*z[111];
    z[116]=z[13]*z[74];
    z[116]= - z[114] + z[116];
    z[116]=z[13]*z[116];
    z[111]=z[111] + 3*z[116];
    z[111]=z[3]*z[111];
    z[79]=z[21] - z[79];
    z[79]=z[79]*z[41];
    z[116]=3*z[21];
    z[118]=z[116]*z[13];
    z[118]=z[118] - z[23];
    z[119]=z[13]*z[118];
    z[79]=4*z[79] + z[119];
    z[79]=z[5]*z[79];
    z[119]=z[3]*z[13];
    z[118]= - z[118]*z[119];
    z[79]=z[79] + z[118];
    z[79]=z[8]*z[79];
    z[118]=11*z[108] - 5;
    z[118]=z[107]*z[118];
    z[101]=z[1] - 22*z[101];
    z[101]=z[3]*z[101]*z[41];
    z[101]=z[101] + z[118];
    z[101]=z[11]*z[101];
    z[99]= - z[5]*z[99];
    z[118]= - z[5] + z[3];
    z[118]=z[8]*z[21]*z[118];
    z[120]= - z[3]*z[23];
    z[99]=z[118] + z[120] + z[99];
    z[99]=z[106]*z[99];
    z[106]=z[108] + 1;
    z[106]=z[107]*z[106];
    z[99]=z[106] + z[99];
    z[99]=z[10]*z[99];
    z[106]=z[52] + z[17];
    z[107]=z[106]*z[110];
    z[107]= - 3*z[1] + z[107];
    z[107]=z[107]*z[41];
    z[108]=20*z[1];
    z[40]= - z[13]*z[40];
    z[40]= - z[108] + z[40];
    z[110]=npow(z[13],2);
    z[40]=z[40]*z[110];
    z[118]= - 5*z[117] + 5;
    z[120]=npow(z[13],3);
    z[118]=z[120]*z[118];
    z[121]=z[110]*z[3];
    z[118]=3*z[121] + z[118];
    z[118]=z[12]*z[1]*z[118];
    z[40]=8*z[99] + z[101] + z[118] + 4*z[79] + z[111] + z[78] + z[107]
    + z[40];
    z[40]=z[10]*z[40];
    z[78]=23*z[21];
    z[79]= - z[78] - 19*z[30];
    z[99]= - z[34] - n<T>(9,2)*z[36];
    z[99]=z[2]*z[99];
    z[79]=n<T>(1,2)*z[79] + z[99];
    z[79]=z[2]*z[79];
    z[63]=z[63]*z[6];
    z[62]= - z[63] - n<T>(19,2)*z[21] - z[62];
    z[62]=z[6]*z[62];
    z[99]=z[59] - z[34] + z[46];
    z[99]=z[2]*z[99];
    z[63]=z[63] + z[21];
    z[99]=z[99] - z[63];
    z[99]=z[4]*z[99];
    z[101]=11*z[17];
    z[107]= - 19*z[15] - z[101];
    z[62]=z[99] + z[62] + n<T>(1,2)*z[107] + z[79];
    z[62]=z[4]*z[62];
    z[79]=z[33]*z[21];
    z[99]= - z[79] - n<T>(3,2)*z[59];
    z[99]=z[2]*z[99];
    z[107]=npow(z[14],3);
    z[41]=z[107]*z[41]*z[116];
    z[41]=z[36] + z[41];
    z[41]=z[41]*z[39];
    z[41]=z[99] + z[41];
    z[41]=z[13]*z[41];
    z[99]=z[20]*z[15];
    z[111]=12*z[21];
    z[118]=n<T>(11,2)*z[80];
    z[122]=z[118] + z[111] + z[99];
    z[122]=z[2]*z[122];
    z[123]=z[21] - 6*z[30];
    z[123]=z[14]*z[123];
    z[124]=z[70] - z[30];
    z[125]=z[26]*z[9];
    z[126]=z[124]*z[125];
    z[123]=z[123] + 6*z[126];
    z[123]=z[123]*z[39];
    z[126]=7*z[21];
    z[83]=z[123] + z[126] - z[83];
    z[83]=z[9]*z[83];
    z[41]=z[41] + z[83] - z[23] + z[122];
    z[41]=z[13]*z[41];
    z[83]= - z[15] + z[53];
    z[83]=z[14]*z[83];
    z[122]=n<T>(3,2)*z[36];
    z[123]=z[34] - z[122];
    z[123]=z[2]*z[123];
    z[63]=z[123] + n<T>(3,2)*z[83] + z[63];
    z[63]=z[4]*z[63];
    z[83]=3*z[34];
    z[123]=z[83] + z[36];
    z[123]=n<T>(1,2)*z[123] + z[59];
    z[123]=z[6]*z[123];
    z[127]= - z[34] + z[79];
    z[127]=z[2]*z[127];
    z[123]=z[123] - z[50] + z[127];
    z[123]=z[6]*z[123];
    z[48]=z[52] + z[48];
    z[52]=z[21] - z[80];
    z[52]=z[9]*z[52];
    z[52]=z[63] + z[52] + z[123] - z[48];
    z[52]=z[7]*z[52];
    z[63]= - z[116] - z[64];
    z[63]=z[14]*z[63];
    z[63]=z[34] + z[63];
    z[63]=z[2]*z[63];
    z[63]= - z[88] + z[63] + n<T>(13,2)*z[21] - z[30];
    z[63]=z[6]*z[63];
    z[123]=z[20]*z[21];
    z[127]=z[34] - z[123];
    z[127]=z[2]*z[127];
    z[127]= - n<T>(9,2)*z[30] + z[127];
    z[127]=z[2]*z[127];
    z[63]=z[63] + z[49] + z[127];
    z[63]=z[6]*z[63];
    z[127]=z[58]*z[20];
    z[128]=6*z[17];
    z[129]=z[128] + z[15];
    z[130]=4*z[14];
    z[131]= - z[129]*z[130];
    z[74]=z[74] - z[17];
    z[125]=z[74]*z[125];
    z[125]= - 12*z[125] - z[127] + z[50] + z[131];
    z[125]=z[9]*z[125];
    z[131]=z[126] - z[30];
    z[131]=z[2]*z[131];
    z[105]=z[125] + n<T>(1,2)*z[131] + z[105] + 35*z[17];
    z[105]=z[9]*z[105];
    z[41]=z[52] + z[41] + z[62] + z[105] - z[108] + z[63];
    z[41]=z[7]*z[41];
    z[52]=9*z[17];
    z[62]= - z[72] - z[52];
    z[62]=z[62]*z[20];
    z[48]=z[14]*z[48];
    z[48]=n<T>(9,2)*z[21] + z[48];
    z[48]=z[14]*z[48];
    z[48]=z[35] + z[48];
    z[48]=z[6]*z[48];
    z[48]=z[48] - z[21] + z[62];
    z[48]=z[48]*z[91];
    z[62]=z[14]*z[106];
    z[62]=z[116] + z[62];
    z[62]=z[14]*z[62];
    z[62]=z[34] + z[62];
    z[62]=z[62]*z[100];
    z[63]= - z[25]*z[26];
    z[62]=z[63] + z[62];
    z[62]=z[6]*z[62];
    z[63]=4*z[17];
    z[91]= - z[72] - z[63];
    z[91]=z[14]*z[91];
    z[91]= - 8*z[21] + z[91];
    z[91]=z[14]*z[91];
    z[62]=z[62] + 8*z[34] + z[91];
    z[62]=z[9]*z[62];
    z[91]=8*z[17];
    z[105]= - z[57] - z[91];
    z[105]=z[14]*z[105];
    z[31]=z[62] + z[48] + z[127] - z[31] + z[105];
    z[31]=z[9]*z[31];
    z[48]= - n<T>(37,2)*z[15] - z[52];
    z[48]=z[14]*z[48];
    z[52]= - z[15]*z[26];
    z[52]=z[34] + z[52];
    z[52]=z[52]*z[85];
    z[48]=z[52] - z[84] + z[48];
    z[48]=z[6]*z[48];
    z[52]=n<T>(3,2)*z[30];
    z[62]= - z[21] - z[52];
    z[62]=z[2]*z[62];
    z[31]=z[31] + z[48] - n<T>(5,2)*z[17] + z[62];
    z[31]=z[9]*z[31];
    z[48]=66*z[15] + 43*z[17];
    z[48]=z[14]*z[48];
    z[48]=z[78] + z[48];
    z[48]=z[14]*z[48];
    z[48]=z[48] + z[45];
    z[48]=z[4]*z[48];
    z[45]= - z[47] + z[45];
    z[45]=z[45]*z[71];
    z[47]= - z[90] - z[76];
    z[47]=z[14]*z[47];
    z[45]=z[45] + 15*z[47] + z[48];
    z[45]=z[13]*z[45];
    z[47]=67*z[15] + n<T>(119,2)*z[17];
    z[47]=z[14]*z[47];
    z[47]=z[55] + z[111] + z[47];
    z[47]=z[4]*z[47];
    z[48]=z[99] + z[21];
    z[55]=z[6] - z[2];
    z[48]=z[48]*z[55];
    z[55]= - 27*z[15] - 53*z[17];
    z[45]=z[45] + z[47] + n<T>(1,2)*z[55] + z[48];
    z[45]=z[13]*z[45];
    z[47]=z[34]*z[14];
    z[48]=z[47] + 2*z[38];
    z[55]= - z[2]*z[48];
    z[55]=z[55] - z[35] - z[122];
    z[55]=z[2]*z[55];
    z[52]=z[55] + z[70] + z[52];
    z[52]=z[2]*z[52];
    z[37]=z[2]*z[37];
    z[37]=z[37] + z[34] + z[46];
    z[37]=z[2]*z[37];
    z[37]=z[21] + z[37];
    z[37]=z[4]*z[37];
    z[46]=55*z[15] + 59*z[17];
    z[37]=z[37] + n<T>(1,2)*z[46] + z[52];
    z[37]=z[4]*z[37];
    z[32]=z[14]*z[32];
    z[46]=z[6]*z[48];
    z[32]=z[46] - z[35] + z[32];
    z[32]=z[6]*z[32];
    z[46]= - z[50] + z[30];
    z[32]=3*z[46] + z[32];
    z[32]=z[6]*z[32];
    z[46]= - z[34] - z[123];
    z[46]=z[14]*z[46];
    z[46]= - n<T>(1,2)*z[38] + z[46];
    z[46]=z[46]*z[85];
    z[46]=z[46] - z[83] - z[79];
    z[46]=z[6]*z[46];
    z[46]= - z[50] + z[46];
    z[46]=z[7]*z[46];
    z[48]=z[54] - z[30];
    z[48]= - z[28] - n<T>(7,2)*z[48];
    z[48]=z[13]*z[48];
    z[52]=n<T>(5,2)*z[15];
    z[32]=z[46] + z[48] + z[52] + z[32];
    z[32]=z[7]*z[32];
    z[46]=z[17] + n<T>(9,4)*z[15];
    z[48]=z[14]*z[46];
    z[34]= - z[34] + z[43];
    z[34]=z[6]*z[34];
    z[34]=z[34] + n<T>(13,4)*z[21] + z[48];
    z[34]=z[6]*z[34];
    z[43]=n<T>(1,2)*z[15];
    z[48]=z[43] + z[17];
    z[34]=3*z[48] + z[34];
    z[34]=z[6]*z[34];
    z[38]=z[38] + z[47];
    z[38]=z[2]*z[38];
    z[38]=z[38] + z[60];
    z[38]=z[4]*z[38];
    z[47]=z[65] + z[21];
    z[38]=n<T>(1,2)*z[38] - n<T>(3,2)*z[47];
    z[38]=z[2]*z[38];
    z[48]= - z[21] - z[61];
    z[48]=z[48]*z[95];
    z[38]=z[48] - z[15] + z[38];
    z[38]=z[5]*z[38];
    z[48]= - z[50] + 2*z[65];
    z[48]=z[2]*z[48];
    z[48]=z[15] + z[48];
    z[48]=z[2]*z[48];
    z[31]=z[38] + z[32] + z[45] + z[37] + z[31] + z[34] - 8*z[1] + z[48]
   ;
    z[31]=z[5]*z[31];
    z[29]=z[29] - z[42];
    z[29]=z[6]*z[29];
    z[32]=z[4]*z[25];
    z[32]=z[32] + z[42];
    z[32]=z[4]*z[32];
    z[34]= - z[2]*z[67];
    z[34]= - n<T>(7,2)*z[17] + z[34];
    z[34]=z[2]*z[34];
    z[37]= - z[67]*z[51];
    z[37]=z[81] + z[37];
    z[37]=z[37]*z[95];
    z[29]=z[37] + z[32] + z[34] + z[29];
    z[29]=z[8]*z[29];
    z[32]=z[23] - z[58];
    z[32]=z[2]*z[32];
    z[34]= - z[23] - z[73];
    z[34]=z[7]*z[34];
    z[32]=z[32] + z[34];
    z[32]=z[5]*z[32];
    z[34]= - z[23] + z[96];
    z[34]=z[6]*z[34];
    z[37]= - z[23] - z[92];
    z[37]=z[4]*z[37];
    z[34]=z[34] + z[37];
    z[34]=z[3]*z[34];
    z[37]= - npow(z[4],2);
    z[37]=z[37] + z[66];
    z[37]=z[15]*z[37];
    z[38]=z[2]*z[77];
    z[42]=n<T>(7,2)*z[1];
    z[38]=z[42] + z[38];
    z[38]=z[2]*z[38];
    z[45]=z[15] + z[53];
    z[45]=z[7]*z[45];
    z[45]= - z[42] + z[45];
    z[45]=z[7]*z[45];
    z[29]=z[29] + z[34] + z[32] + z[45] + z[38] + z[37];
    z[29]=z[8]*z[29];
    z[32]=z[75] + z[65];
    z[32]=z[2]*z[32];
    z[34]= - z[5]*z[47];
    z[32]=z[34] + z[23] + z[32];
    z[32]=z[2]*z[32];
    z[34]=n<T>(9,4)*z[21] + z[88];
    z[34]=z[6]*z[34];
    z[37]=n<T>(7,4)*z[15];
    z[34]=z[37] + z[34];
    z[34]=z[6]*z[34];
    z[37]=z[37] + z[92];
    z[37]=z[4]*z[37];
    z[38]= - z[15] + z[73];
    z[38]=z[38]*z[95];
    z[32]=z[38] + z[37] + z[34] + z[32];
    z[32]=z[5]*z[32];
    z[34]=n<T>(1,2)*z[96];
    z[37]= - z[34] - n<T>(31,2)*z[15] - 6*z[58];
    z[37]=z[13]*z[37];
    z[38]=13*z[103];
    z[45]=2*z[1];
    z[47]= - z[45] + z[38];
    z[48]=z[6]*z[90];
    z[37]=z[37] + 2*z[47] + z[48];
    z[37]=z[13]*z[37];
    z[47]=z[50] + z[88];
    z[47]=z[6]*z[47];
    z[48]=n<T>(9,2)*z[15];
    z[47]=z[48] + z[47];
    z[47]=z[6]*z[47];
    z[51]= - z[52] - 4*z[96];
    z[51]=z[7]*z[51];
    z[47]=z[51] + n<T>(13,2)*z[1] + z[47];
    z[47]=z[7]*z[47];
    z[51]= - z[43] - z[58];
    z[51]=z[2]*z[51];
    z[51]= - n<T>(25,2)*z[1] + z[51];
    z[51]=z[2]*z[51];
    z[54]=z[95] + z[5];
    z[54]=z[3]*z[94]*z[6]*z[54];
    z[32]=z[54] + z[32] + z[47] + z[51] + z[37];
    z[32]=z[3]*z[32];
    z[37]=11*z[1];
    z[38]= - z[37] - z[38];
    z[38]=z[2]*z[38];
    z[47]=z[98] + 9*z[58];
    z[47]=z[2]*z[47];
    z[51]=13*z[1];
    z[47]=z[51] + z[47];
    z[47]=z[13]*z[47];
    z[38]=2*z[38] + z[47];
    z[38]=z[13]*z[38];
    z[38]=22*z[104] + z[38];
    z[38]=z[38]*z[119];
    z[47]=3*z[117] + 6;
    z[47]=z[120]*z[104]*z[47];
    z[37]= - z[37] - 9*z[103];
    z[54]=z[13]*z[2];
    z[37]=z[37]*z[54];
    z[37]=13*z[104] + z[37];
    z[37]=z[37]*z[121];
    z[37]=z[37] + z[47];
    z[37]=z[12]*z[37];
    z[47]= - 6*z[15] + z[17];
    z[47]=z[47]*z[109];
    z[47]= - z[51] + z[47];
    z[47]=z[47]*z[54];
    z[47]=20*z[104] + z[47];
    z[47]=z[47]*z[110];
    z[37]=z[37] + z[47] + z[38];
    z[37]=z[12]*z[37];
    z[38]=z[97] - z[81];
    z[47]=z[2]*z[124];
    z[38]=2*z[38] + 9*z[47];
    z[38]=z[2]*z[38];
    z[38]=z[45] + z[38];
    z[38]=z[13]*z[38];
    z[47]= - z[57] + z[17];
    z[47]=z[2]*z[47];
    z[47]= - z[1] + z[47];
    z[51]=7*z[2];
    z[47]=z[47]*z[51];
    z[38]=z[47] + z[38];
    z[38]=z[13]*z[38];
    z[38]=24*z[104] + z[38];
    z[38]=z[13]*z[38];
    z[47]=z[15] + z[34];
    z[47]=z[6]*z[47];
    z[47]=z[114] + z[47];
    z[54]=3*z[4];
    z[55]=npow(z[7],2);
    z[47]=z[47]*z[55]*z[54];
    z[60]=28*z[15] + 13*z[58];
    z[60]=z[2]*z[60];
    z[61]=3*z[65];
    z[62]=11*z[21];
    z[71]= - z[62] - z[61];
    z[71]=z[2]*z[71];
    z[71]= - z[97] + z[71];
    z[71]=z[13]*z[71];
    z[60]=z[71] + z[115] + z[60];
    z[60]=z[13]*z[60];
    z[71]= - z[2]*z[23];
    z[71]= - z[86] + z[71];
    z[71]=z[2]*z[71];
    z[60]=11*z[71] + z[60];
    z[60]=z[13]*z[60];
    z[71]= - z[23] - z[96];
    z[55]=z[6]*z[71]*z[55];
    z[55]=z[55] + 12*z[104] + z[60];
    z[55]=z[3]*z[55];
    z[37]=z[37] + z[55] + z[38] + z[47];
    z[37]=z[12]*z[37];
    z[38]=z[67]*z[4];
    z[34]=z[38] - z[34] - z[43] - z[53];
    z[34]=z[4]*z[34];
    z[38]=z[38] - z[15] + z[101];
    z[43]=z[128] - z[49];
    z[47]= - z[43]*z[130];
    z[35]= - z[35] + 3*z[36];
    z[35]=z[35]*z[109];
    z[53]=13*z[21];
    z[35]=z[35] - z[53] + z[47];
    z[35]=z[2]*z[35];
    z[35]=z[35] + 2*z[38];
    z[35]=z[13]*z[35];
    z[38]=n<T>(37,2)*z[21] - 14*z[30];
    z[38]=z[2]*z[38];
    z[38]= - 3*z[43] + z[38];
    z[38]=z[2]*z[38];
    z[43]=10*z[1];
    z[34]=z[35] + z[34] + z[43] + z[38];
    z[34]=z[13]*z[34];
    z[35]=z[96] + z[15];
    z[38]=z[35]*z[4];
    z[47]= - z[15]*z[93];
    z[47]=z[47] - z[38];
    z[47]=z[4]*z[47];
    z[55]=5*z[17];
    z[60]= - 17*z[15] + z[55];
    z[60]=z[2]*z[60];
    z[45]=z[45] + z[60];
    z[45]=z[2]*z[45];
    z[34]=z[34] + z[45] + z[47];
    z[34]=z[13]*z[34];
    z[45]= - z[52] + 2*z[96];
    z[45]=z[6]*z[45];
    z[38]= - z[38] - z[42] + z[45];
    z[38]=z[4]*z[38];
    z[42]= - z[66]*z[50];
    z[35]=z[17] - z[35];
    z[35]=z[35]*z[54];
    z[35]=z[42] + z[35];
    z[35]=z[7]*z[35];
    z[42]=z[15]*z[66];
    z[35]=z[35] + z[42] + z[38];
    z[35]=z[7]*z[35];
    z[38]=z[70] + z[30];
    z[38]=z[6]*z[38];
    z[38]=z[38] + z[46];
    z[38]=z[38]*z[66];
    z[42]=z[4]*z[15];
    z[42]= - n<T>(15,2)*z[1] + z[42];
    z[42]=z[4]*z[42];
    z[38]=z[38] + z[42];
    z[38]=z[5]*z[38];
    z[42]=z[43] + z[103];
    z[22]=z[42]*z[22];
    z[22]=z[37] + z[29] + z[32] + z[38] + z[35] + z[22] + z[34];
    z[22]=z[12]*z[22];
    z[29]=z[116] - z[65];
    z[29]=z[29]*z[2];
    z[32]= - z[29] + z[77];
    z[32]=z[2]*z[32];
    z[34]= - n<T>(27,4)*z[21] + z[65];
    z[34]=z[2]*z[34];
    z[35]= - z[82] + z[65];
    z[35]=z[6]*z[35];
    z[34]=z[35] - z[48] + z[34];
    z[34]=z[6]*z[34];
    z[35]=n<T>(13,2)*z[87] - z[15] + n<T>(31,2)*z[96];
    z[35]=z[9]*z[35];
    z[37]=z[6]*z[65];
    z[29]=z[37] + z[15] + z[29];
    z[29]=z[4]*z[29];
    z[29]=z[29] + z[35] + z[34] - z[69] + z[32];
    z[29]=z[4]*z[29];
    z[28]= - z[28] - 12*z[30];
    z[28]=z[14]*z[28];
    z[32]=z[26]*z[87];
    z[28]=z[28] + 9*z[32];
    z[28]=z[28]*z[39];
    z[32]= - z[24]*z[44];
    z[28]=z[32] + z[28] + n<T>(1,2)*z[88] + z[50] - z[102];
    z[28]=z[4]*z[28];
    z[32]=z[30] - z[21];
    z[27]=z[32]*z[27];
    z[34]=z[107]*z[9];
    z[35]= - z[58]*z[34];
    z[27]=2*z[27] + z[35];
    z[35]=12*z[9];
    z[27]=z[27]*z[35];
    z[35]=z[64] + z[21];
    z[37]= - z[35]*z[26];
    z[38]=z[107]*z[87];
    z[37]=z[37] + z[38];
    z[38]=6*z[9];
    z[37]=z[37]*z[38];
    z[24]= - z[24]*z[26];
    z[24]=z[24] + z[37];
    z[37]=2*z[4];
    z[24]=z[24]*z[37];
    z[37]= - z[72] + z[81];
    z[37]=z[37]*z[33];
    z[38]=z[14]*z[74];
    z[38]= - z[126] + 6*z[38];
    z[38]=z[38]*z[33];
    z[42]= - z[14]*z[61];
    z[38]=z[38] + z[42];
    z[38]=z[2]*z[38];
    z[24]=z[24] + z[27] + z[37] + z[38];
    z[24]=z[13]*z[24];
    z[27]=23*z[15];
    z[37]=z[27] - 12*z[17];
    z[33]=z[37]*z[33];
    z[33]=z[118] - n<T>(35,2)*z[21] + z[33];
    z[33]=z[2]*z[33];
    z[34]= - z[34] - 2*z[26];
    z[34]=z[96]*z[34];
    z[34]= - z[59] + z[34];
    z[34]=z[34]*z[113];
    z[37]= - z[126] + 9*z[30];
    z[38]=z[37]*z[56];
    z[42]=z[6]*z[36];
    z[34]=z[34] + z[38] - 4*z[42];
    z[34]=z[34]*z[39];
    z[38]= - z[57] + z[91];
    z[24]=z[24] + z[28] + z[34] - 6*z[96] + 5*z[38] + z[33];
    z[24]=z[13]*z[24];
    z[28]= - z[21] + z[64];
    z[28]=z[9]*z[28]*z[100];
    z[33]=z[6]*z[14];
    z[34]=z[37]*z[33];
    z[28]=z[28] - z[80] + z[34];
    z[28]=z[28]*z[39];
    z[34]= - z[21] + 4*z[30];
    z[34]=z[34]*z[51];
    z[37]= - z[53] + 28*z[30];
    z[37]=z[6]*z[37];
    z[28]=z[28] + z[34] + z[37];
    z[28]=z[9]*z[28];
    z[23]= - z[23] + z[17];
    z[23]=z[14]*z[23];
    z[23]=z[23] + z[94];
    z[23]=z[4]*z[23];
    z[34]=z[9]*z[36];
    z[34]=z[30] - z[34];
    z[34]= - z[62] - 40*z[34];
    z[34]=z[9]*z[34];
    z[23]=z[23] + z[34] - 5*z[129] - n<T>(9,2)*z[96];
    z[23]=z[4]*z[23];
    z[34]=z[2]*z[30];
    z[27]= - 5*z[34] + z[27] - n<T>(25,2)*z[17];
    z[27]=z[2]*z[27];
    z[34]= - z[2]*z[35];
    z[34]=z[49] + z[34];
    z[34]=z[6]*z[34];
    z[23]=z[24] + z[23] + z[28] + z[34] + n<T>(45,2)*z[1] + z[27];
    z[23]=z[13]*z[23];
    z[24]= - z[89] - z[17];
    z[20]=z[24]*z[20];
    z[20]= - z[70] + z[20];
    z[20]=z[20]*z[85];
    z[20]=z[20] + 31*z[15] + n<T>(27,2)*z[17];
    z[20]=z[20]*z[33];
    z[24]= - z[25]*z[100];
    z[25]=z[97] - z[101];
    z[25]=z[25]*z[26];
    z[24]=z[25] + z[24];
    z[24]=z[6]*z[24];
    z[25]= - z[89] + z[63];
    z[25]=z[14]*z[25];
    z[24]=z[24] - z[62] + z[25];
    z[24]=z[9]*z[24];
    z[25]=n<T>(3,2)*z[58];
    z[20]=z[24] + z[20] + z[25] + z[90] - z[63];
    z[20]=z[9]*z[20];
    z[24]=z[32]*z[85];
    z[24]=z[24] - z[76] + z[25];
    z[24]=z[6]*z[24];
    z[25]=29*z[1] - 3*z[103];
    z[20]=z[20] + n<T>(1,2)*z[25] + z[24];
    z[20]=z[9]*z[20];
    z[24]=z[46]*z[56];
    z[25]= - z[6]*z[30];
    z[24]=z[25] + z[24] - z[89] - z[68];
    z[24]=z[6]*z[24];
    z[25]=z[90] + z[55];
    z[25]=z[14]*z[25];
    z[21]= - z[21] + z[25];
    z[21]=z[2]*z[21];
    z[17]=z[21] - z[112] - 22*z[17];
    z[17]=z[2]*z[17];
    z[17]=z[24] + n<T>(11,2)*z[1] + z[17];
    z[17]=z[6]*z[17];
    z[15]= - z[58] + n<T>(13,2)*z[15] + z[81];
    z[15]=z[2]*z[15];
    z[15]= - z[43] + z[15];
    z[15]=z[2]*z[15];

    r += z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[22] + z[23]
       + z[29] + z[31] + z[40] + z[41];
 
    return r;
}

template double qqb_2lNLC_r666(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r666(const std::array<dd_real,31>&);
#endif
