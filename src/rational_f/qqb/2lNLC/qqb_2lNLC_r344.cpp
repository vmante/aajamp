#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r344(const std::array<T,31>& k) {
  T z[87];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[3];
    z[7]=k[12];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[13];
    z[11]=k[9];
    z[12]=k[8];
    z[13]=k[15];
    z[14]=n<T>(1,2)*z[7];
    z[15]=z[7]*z[6];
    z[16]=static_cast<T>(59)+ 7*z[15];
    z[16]=z[16]*z[14];
    z[17]=z[9]*z[10];
    z[18]=z[17] + 1;
    z[19]=3*z[15];
    z[20]= - z[19] + z[18];
    z[21]=z[3]*z[6];
    z[20]=4*z[20] - 7*z[21];
    z[20]=z[4]*z[20];
    z[22]=npow(z[10],2);
    z[23]=z[22]*z[9];
    z[23]=n<T>(7,2)*z[23];
    z[24]=n<T>(1,2) - z[21];
    z[24]=z[3]*z[24];
    z[25]=z[5]*z[9];
    z[26]= - static_cast<T>(3)- z[25];
    z[26]=z[5]*z[26];
    z[16]=z[20] + z[26] + z[24] + z[23] - 14*z[10] + z[16];
    z[16]=z[4]*z[16];
    z[20]=7*z[13];
    z[24]=5*z[3];
    z[26]=npow(z[13],2);
    z[27]=z[26]*z[8];
    z[28]=2*z[27];
    z[29]=z[10]*z[8];
    z[30]= - static_cast<T>(3)- z[29];
    z[30]=z[10]*z[30];
    z[30]= - z[24] + n<T>(3,2)*z[30] - z[20] + z[28];
    z[30]=z[3]*z[30];
    z[31]= - 7*z[25] - 12*z[17] - static_cast<T>(5)+ 4*z[15];
    z[31]=z[4]*z[31];
    z[32]=7*z[8];
    z[33]=3*z[6];
    z[34]=z[32] - z[33];
    z[34]=n<T>(1,2)*z[34];
    z[35]=z[6]*z[8];
    z[36]=npow(z[8],2);
    z[37]= - 2*z[36] + z[35];
    z[37]=z[3]*z[37];
    z[37]= - z[34] + z[37];
    z[37]=z[3]*z[37];
    z[37]=n<T>(3,2) + z[37];
    z[37]=z[2]*z[37];
    z[38]=n<T>(5,2) + z[29];
    z[39]=5*z[8] - z[6];
    z[39]=z[3]*z[39];
    z[38]=3*z[38] + z[39];
    z[38]=z[3]*z[38];
    z[39]=z[3]*z[8];
    z[40]=static_cast<T>(25)+ 11*z[39];
    z[40]=z[5]*z[40];
    z[31]=z[37] + z[31] + n<T>(1,2)*z[40] + 13*z[10] + z[38];
    z[31]=z[2]*z[31];
    z[37]=npow(z[12],2);
    z[38]=8*z[37];
    z[40]=z[38] - 5*z[22];
    z[40]=z[7]*z[40];
    z[41]=npow(z[11],2);
    z[42]=z[22]*z[41];
    z[43]=2*z[9];
    z[44]= - z[43]*z[42];
    z[45]=2*z[41];
    z[46]= - z[10]*z[45];
    z[40]=z[44] + z[46] + z[40];
    z[40]=z[9]*z[40];
    z[44]=2*z[7];
    z[46]=4*z[10];
    z[47]= - z[7] - z[46] + z[12] - z[11];
    z[47]=z[47]*z[44];
    z[48]=z[37]*z[8];
    z[49]=z[48] + z[12];
    z[50]=3*z[39];
    z[51]= - static_cast<T>(2)- z[50];
    z[51]=z[5]*z[51];
    z[51]=z[51] - z[3] + z[49];
    z[51]=z[5]*z[51];
    z[52]=2*z[26];
    z[16]=z[31] + z[16] + z[51] + z[30] + z[40] + z[47] - 4*z[22] - 
    z[45] + z[52] + z[37];
    z[16]=z[2]*z[16];
    z[30]=z[7]*z[8];
    z[31]=3*z[8] - z[9];
    z[31]=z[5]*z[31];
    z[31]=z[31] + n<T>(11,2)*z[39] - static_cast<T>(5)+ 3*z[30];
    z[31]=z[5]*z[31];
    z[40]= - z[9]*z[8];
    z[40]=z[36] + z[40];
    z[40]=z[5]*z[40];
    z[47]=n<T>(3,2)*z[9];
    z[40]=z[40] - z[8] + z[47];
    z[40]=z[5]*z[40];
    z[40]= - static_cast<T>(3)+ z[40];
    z[40]=z[4]*z[40];
    z[51]=2*z[12];
    z[53]=z[43]*z[12];
    z[54]=z[10]*z[53];
    z[31]=z[40] + z[31] - n<T>(25,2)*z[3] + z[54] + z[51] - 13*z[7];
    z[31]=z[4]*z[31];
    z[40]=z[6]*z[41];
    z[40]=z[40] + z[46];
    z[54]=npow(z[6],2);
    z[55]=z[54]*z[45];
    z[55]=5*z[29] - static_cast<T>(1)+ z[55];
    z[55]=z[7]*z[55];
    z[40]=2*z[40] + z[55];
    z[40]=z[7]*z[40];
    z[55]=static_cast<T>(3)- z[30];
    z[55]=z[55]*z[14];
    z[56]=3*z[13];
    z[55]=z[5] - z[56] + z[55];
    z[57]= - z[26]*z[43];
    z[58]=z[50] - 1;
    z[59]= - z[3]*z[58];
    z[55]=z[59] + z[57] + 3*z[55];
    z[55]=z[5]*z[55];
    z[57]= - z[17]*z[38];
    z[59]=2*z[3];
    z[49]=z[59] + z[49];
    z[49]=z[3]*z[49];
    z[60]=z[10]*z[12];
    z[31]=z[31] + z[55] + z[49] + z[57] + z[40] - 2*z[60] + 7*z[37] + 
    z[45];
    z[31]=z[4]*z[31];
    z[40]=npow(z[7],2);
    z[49]=npow(z[11],3);
    z[55]=z[40]*z[49];
    z[57]=z[55]*z[9];
    z[61]= - z[40]*z[45];
    z[61]=z[61] - z[57];
    z[61]=z[9]*z[61];
    z[62]=4*z[11];
    z[63]=z[62]*z[7];
    z[64]=7*z[22];
    z[65]=z[64] - z[63];
    z[65]=z[7]*z[65];
    z[66]=z[7] - z[10];
    z[67]=z[66]*z[3];
    z[68]=z[7]*z[10];
    z[69]= - z[68] - z[67];
    z[69]=z[3]*z[69];
    z[70]= - z[10]*z[44];
    z[67]=z[70] + z[67];
    z[67]=z[5]*z[67];
    z[61]=z[67] + z[69] + z[65] + 4*z[61];
    z[61]=z[5]*z[61];
    z[65]=z[59]*z[36];
    z[32]=z[32] + z[65];
    z[32]=z[3]*z[32];
    z[32]=static_cast<T>(8)+ z[32];
    z[32]=z[5]*z[32];
    z[67]= - z[21] - static_cast<T>(1)- 3*z[17];
    z[67]=n<T>(1,2)*z[67] - z[25];
    z[69]=3*z[4];
    z[67]=z[67]*z[69];
    z[70]= - 2*z[8] + z[6];
    z[70]=z[3]*z[70];
    z[70]= - n<T>(7,2) + z[70];
    z[70]=z[3]*z[70];
    z[32]=z[67] + z[70] + z[32];
    z[32]=z[2]*z[32];
    z[67]=npow(z[3],2);
    z[70]= - z[22] + z[67];
    z[71]=z[5]*z[3];
    z[72]=2*z[39];
    z[73]=static_cast<T>(3)- z[72];
    z[73]=z[73]*z[71];
    z[23]= - 15*z[5] - 4*z[3] - z[46] + z[23];
    z[23]=z[4]*z[23];
    z[23]=z[32] + z[23] + 3*z[70] + z[73];
    z[23]=z[2]*z[23];
    z[32]=n<T>(1,2)*z[25];
    z[70]= - static_cast<T>(1)+ z[19];
    z[70]=z[32] + n<T>(1,2)*z[70] + z[21];
    z[69]=z[70]*z[69];
    z[70]=8*z[3];
    z[73]=n<T>(7,2)*z[15];
    z[74]= - static_cast<T>(12)- z[73];
    z[74]=z[7]*z[74];
    z[69]=z[69] - 4*z[5] + z[74] - z[70];
    z[69]=z[4]*z[69];
    z[74]=2*z[67];
    z[75]=11*z[3] + z[5];
    z[75]=z[5]*z[75];
    z[69]=z[69] + z[75] + n<T>(7,2)*z[40] + z[74];
    z[69]=z[4]*z[69];
    z[75]= - z[74] + 2*z[71];
    z[76]= - z[37] - z[75];
    z[76]=z[5]*z[76];
    z[77]=z[3]*z[22];
    z[23]=z[23] + z[69] + n<T>(3,2)*z[77] + z[76];
    z[23]=z[2]*z[23];
    z[69]=npow(z[5],2);
    z[76]= - z[69]*z[59];
    z[77]=z[4]*z[5];
    z[78]=7*z[3];
    z[79]= - z[78] + z[5];
    z[79]=z[79]*z[77];
    z[76]=z[76] + z[79];
    z[76]=z[76]*npow(z[4],2);
    z[79]= - z[74]*z[77];
    z[80]=z[74] - 7*z[71];
    z[80]=z[4]*z[80];
    z[81]=z[5]*z[74];
    z[80]=z[81] + z[80];
    z[80]=z[2]*z[80];
    z[79]=z[79] + z[80];
    z[79]=z[2]*z[79];
    z[80]=npow(z[4],3);
    z[69]=z[69]*z[80];
    z[81]=z[3]*z[69];
    z[82]= - z[67]*npow(z[2],2)*z[77];
    z[81]=z[81] + z[82];
    z[81]=z[1]*z[81];
    z[76]=2*z[81] + z[76] + z[79];
    z[76]=z[2]*z[76];
    z[59]=z[59]*z[69];
    z[59]=z[59] + z[76];
    z[59]=z[1]*z[59];
    z[69]=static_cast<T>(1)+ z[15];
    z[69]=z[7]*z[69];
    z[76]= - static_cast<T>(1)- z[25];
    z[76]=z[5]*z[76];
    z[24]=z[76] + n<T>(3,2)*z[69] + z[24];
    z[24]=z[4]*z[24];
    z[69]=n<T>(3,2)*z[40];
    z[76]=10*z[3] + z[5];
    z[76]=z[5]*z[76];
    z[24]=z[24] - z[69] + z[76];
    z[24]=z[4]*z[24];
    z[76]=z[71] - z[67];
    z[79]=2*z[5];
    z[81]=z[76]*z[79];
    z[24]=z[81] + z[24];
    z[24]=z[4]*z[24];
    z[81]=static_cast<T>(7)+ z[72];
    z[81]=z[81]*z[71];
    z[82]=z[22]*z[47];
    z[83]=n<T>(7,2) - z[21];
    z[83]=z[3]*z[83];
    z[82]= - 8*z[5] + z[82] + z[83];
    z[82]=z[4]*z[82];
    z[74]=z[82] - z[74] + z[81];
    z[74]=z[2]*z[74];
    z[71]= - z[67] - 10*z[71];
    z[71]=z[4]*z[71];
    z[71]=z[71] + z[74];
    z[71]=z[2]*z[71];
    z[24]=z[24] + z[71];
    z[24]=z[2]*z[24];
    z[71]= - z[68]*z[76];
    z[74]=static_cast<T>(1)+ z[72];
    z[74]=z[5]*z[74];
    z[74]= - z[78] + z[74];
    z[74]=z[74]*z[80];
    z[71]=z[71] + z[74];
    z[71]=z[5]*z[71];
    z[24]=z[59] + z[24] + z[71];
    z[24]=z[1]*z[24];
    z[59]=static_cast<T>(1)+ z[39];
    z[59]=z[59]*z[79];
    z[71]=3*z[3];
    z[59]=z[71] + z[59];
    z[59]=z[5]*z[59];
    z[65]=z[65] + z[8] - z[9];
    z[65]=z[5]*z[65];
    z[65]=z[65] - static_cast<T>(1)- 7*z[39];
    z[65]=z[5]*z[65];
    z[65]=z[70] + z[65];
    z[65]=z[4]*z[65];
    z[59]=z[65] - 3*z[40] + z[59];
    z[59]=z[4]*z[59];
    z[65]=z[69] + z[75];
    z[65]=z[5]*z[65];
    z[69]= - z[3]*z[37];
    z[59]=z[59] + z[69] + z[65];
    z[59]=z[4]*z[59];
    z[65]= - z[67]*z[68];
    z[23]=z[24] + z[23] + z[59] + z[65] + z[61];
    z[23]=z[1]*z[23];
    z[24]=29*z[41];
    z[59]=z[7]*z[11];
    z[61]= - 18*z[59] - z[24] + z[64];
    z[61]=z[7]*z[61];
    z[65]=4*z[57];
    z[67]=z[41]*z[7];
    z[69]=4*z[49];
    z[70]= - z[69] - 9*z[67];
    z[70]=z[7]*z[70];
    z[70]=z[70] - z[65];
    z[74]=3*z[9];
    z[70]=z[70]*z[74];
    z[61]=z[61] + z[70];
    z[61]=z[9]*z[61];
    z[58]=z[10]*z[58];
    z[70]= - z[7]*z[17];
    z[58]=z[70] + z[44] + z[58];
    z[58]=z[5]*z[58];
    z[70]=z[50] + static_cast<T>(5)+ n<T>(1,2)*z[30];
    z[70]=z[7]*z[70];
    z[75]=n<T>(1,2)*z[29];
    z[76]= - static_cast<T>(5)+ z[75];
    z[76]=z[10]*z[76];
    z[70]=z[76] + z[70];
    z[70]=z[3]*z[70];
    z[76]=6*z[13];
    z[78]=z[76] - z[12];
    z[78]=z[78]*z[12];
    z[79]= - 4*z[7] - 18*z[11] + z[10];
    z[79]=z[7]*z[79];
    z[58]=z[58] + z[70] + z[61] + z[79] + z[78] + z[64];
    z[58]=z[5]*z[58];
    z[61]=z[54]*z[11];
    z[64]= - 2*z[6] - 3*z[61];
    z[64]=z[64]*z[62];
    z[64]= - static_cast<T>(1)+ z[64];
    z[64]=z[11]*z[64];
    z[70]=npow(z[6],3);
    z[79]=z[70]*z[62];
    z[80]=3*z[54];
    z[81]=z[80] + z[79];
    z[81]=z[81]*z[41];
    z[81]=static_cast<T>(2)+ z[81];
    z[81]=z[10]*z[81];
    z[64]=z[64] + z[81];
    z[64]=z[10]*z[64];
    z[81]=z[11]*z[6];
    z[82]=static_cast<T>(5)+ 8*z[81];
    z[82]=z[82]*z[41];
    z[68]=z[39]*z[68];
    z[83]=z[7]*z[29];
    z[83]= - 11*z[10] - 7*z[83];
    z[83]=z[7]*z[83];
    z[64]=z[68] + z[83] + z[64] - z[78] + z[82];
    z[64]=z[3]*z[64];
    z[55]=z[55]*z[43];
    z[40]=z[41]*z[40];
    z[40]=z[55] + z[42] + 4*z[40];
    z[40]=z[9]*z[40];
    z[68]=2*z[11];
    z[78]=z[68]*z[7];
    z[22]=z[78] - 4*z[37] - z[22];
    z[22]=z[7]*z[22];
    z[42]=z[6]*z[42];
    z[22]=z[40] + z[42] + z[22];
    z[16]=z[23] + z[16] + z[31] + z[58] + 2*z[22] + z[64];
    z[16]=z[1]*z[16];
    z[22]=4*z[12];
    z[23]=n<T>(1,2)*z[10];
    z[31]=z[22] + z[23];
    z[40]=3*z[10];
    z[31]=z[31]*z[40];
    z[42]=24*z[37];
    z[58]= - z[17]*z[42];
    z[31]=z[58] + z[31] + z[26] - z[38];
    z[31]=z[9]*z[31];
    z[58]=2*z[10];
    z[64]=z[26]*z[9];
    z[83]= - z[64] + z[58] - 9*z[13] - z[68];
    z[83]=z[9]*z[83];
    z[84]=z[43]*z[5];
    z[85]= - static_cast<T>(15)- 13*z[30];
    z[83]= - z[84] + z[50] + n<T>(1,2)*z[85] + z[83];
    z[83]=z[5]*z[83];
    z[85]=z[53] + 1;
    z[85]=z[10]*z[85];
    z[85]=z[51] + z[85];
    z[85]=z[9]*z[85];
    z[85]=static_cast<T>(1)+ z[85];
    z[84]= - z[84] - 2;
    z[84]=z[8]*z[84];
    z[84]=z[47] + z[84];
    z[84]=z[5]*z[84];
    z[86]= - z[36]*z[25];
    z[47]=z[8]*z[47];
    z[47]=z[47] + z[86];
    z[47]=z[47]*z[77];
    z[47]=z[47] + 2*z[85] + z[84];
    z[47]=z[4]*z[47];
    z[77]= - z[6]*z[68];
    z[77]= - z[29] + static_cast<T>(13)+ z[77];
    z[77]=z[7]*z[77];
    z[84]=z[8]*z[12];
    z[85]=n<T>(11,2) + 3*z[84];
    z[86]=z[3]*z[85];
    z[31]=z[47] + z[83] + z[86] + z[31] + z[77] - z[40] - 15*z[48] - 
    z[56] - 14*z[12];
    z[31]=z[4]*z[31];
    z[47]=z[9]*z[7];
    z[48]=n<T>(5,2) + z[53];
    z[48]=z[48]*z[47];
    z[56]= - z[6]*z[72];
    z[34]=z[34] + z[56];
    z[34]=z[3]*z[34];
    z[56]=z[21] - 1;
    z[56]=z[36]*z[56];
    z[35]=n<T>(3,2)*z[35] + z[56];
    z[35]=z[3]*z[35];
    z[35]=z[35] - n<T>(3,2)*z[8];
    z[35]=z[2]*z[35];
    z[56]=static_cast<T>(1)- z[15];
    z[34]=z[35] + z[34] + 4*z[56] + z[48];
    z[34]=z[2]*z[34];
    z[35]=z[9]*z[42];
    z[35]=z[35] - 12*z[12] + z[66];
    z[35]=z[7]*z[35];
    z[48]= - z[10]*z[11];
    z[48]=12*z[37] + z[48];
    z[35]=2*z[48] + z[35];
    z[35]=z[9]*z[35];
    z[20]=z[20] - z[27];
    z[20]=z[8]*z[20];
    z[20]=2*z[21] + n<T>(13,2)*z[29] - n<T>(1,2) + z[20];
    z[20]=z[3]*z[20];
    z[48]=z[21] - z[25];
    z[17]= - n<T>(7,2)*z[17] + static_cast<T>(1)+ z[73];
    z[17]=3*z[17] + n<T>(7,2)*z[48];
    z[17]=z[4]*z[17];
    z[48]= - z[26] - z[37];
    z[48]=z[8]*z[48];
    z[56]= - static_cast<T>(6)+ z[81];
    z[56]=2*z[56] - n<T>(3,2)*z[15];
    z[56]=z[7]*z[56];
    z[50]= - z[50] + z[85];
    z[50]=z[5]*z[50];
    z[17]=z[34] + z[17] + z[50] + z[20] + z[35] + z[56] + 12*z[10] + 11*
    z[13] + z[48];
    z[17]=z[2]*z[17];
    z[20]=z[70]*z[11];
    z[34]=15*z[54] + 8*z[20];
    z[34]=z[11]*z[34];
    z[34]=5*z[6] + z[34];
    z[34]=z[11]*z[34];
    z[35]=npow(z[6],4);
    z[48]=z[35]*z[62];
    z[50]= - 7*z[70] - z[48];
    z[50]=z[11]*z[50];
    z[50]= - z[80] + z[50];
    z[50]=z[50]*z[68];
    z[50]=z[50] - z[8] - n<T>(3,2)*z[6];
    z[50]=z[10]*z[50];
    z[21]= - z[21] + z[50] - static_cast<T>(13)+ 3*z[34];
    z[21]=z[10]*z[21];
    z[34]=27*z[6];
    z[50]= - z[34] - 16*z[61];
    z[50]=z[11]*z[50];
    z[50]= - static_cast<T>(10)+ z[50];
    z[50]=z[11]*z[50];
    z[56]=5*z[12];
    z[66]= - n<T>(7,2) - 4*z[29];
    z[66]=z[7]*z[66];
    z[21]=z[66] + z[50] + z[28] - z[76] + z[56] + z[21];
    z[21]=z[3]*z[21];
    z[28]= - z[69] - 5*z[67];
    z[28]=z[7]*z[28];
    z[28]=z[28] - z[55];
    z[28]=z[28]*z[74];
    z[50]= - 11*z[41] - z[63];
    z[50]=z[7]*z[50];
    z[28]=z[28] - z[69] + 3*z[50];
    z[28]=z[28]*z[43];
    z[50]=z[10]*z[68];
    z[46]= - n<T>(11,2)*z[7] - 49*z[11] - z[46];
    z[46]=z[7]*z[46];
    z[28]=z[28] + z[46] + z[50] + z[52] - 17*z[41];
    z[28]=z[9]*z[28];
    z[46]= - static_cast<T>(27)+ z[30];
    z[46]=z[46]*z[14];
    z[50]=z[29] - z[30];
    z[50]=z[50]*z[71];
    z[25]=z[7]*z[25];
    z[25]=z[25] + z[50] + z[28] + z[46] - n<T>(15,2)*z[10] - z[68] + 8*z[13]
    - z[56];
    z[25]=z[5]*z[25];
    z[28]= - z[6] - z[61];
    z[28]=z[11]*z[28];
    z[28]= - static_cast<T>(2)+ z[28];
    z[28]=z[28]*z[44];
    z[46]=z[38]*z[8];
    z[50]=3*z[12] + z[46];
    z[55]=static_cast<T>(23)+ 6*z[81];
    z[55]=z[11]*z[55];
    z[28]=z[28] + z[40] + 2*z[50] + z[55];
    z[28]=z[7]*z[28];
    z[40]=z[62]*z[6];
    z[50]=static_cast<T>(7)+ z[40];
    z[50]=z[50]*z[78];
    z[24]=z[50] - z[42] + z[24];
    z[24]=z[7]*z[24];
    z[42]=static_cast<T>(19)+ z[40];
    z[42]=z[42]*z[67];
    z[50]=12*z[49];
    z[42]=z[50] + z[42];
    z[42]=z[7]*z[42];
    z[42]=z[42] + 8*z[57];
    z[42]=z[9]*z[42];
    z[38]=z[10]*z[38];
    z[24]=z[42] + z[38] + z[24];
    z[24]=z[9]*z[24];
    z[38]=7*z[6];
    z[42]= - z[38] - 6*z[61];
    z[42]=z[11]*z[42];
    z[42]= - static_cast<T>(3)+ z[42];
    z[42]=z[11]*z[42];
    z[22]=z[22] + z[42];
    z[42]=z[70]*z[68];
    z[42]=z[80] + z[42];
    z[42]=z[11]*z[42];
    z[42]=z[6] + z[42];
    z[42]=z[42]*z[62];
    z[42]= - n<T>(7,2) + z[42];
    z[42]=z[10]*z[42];
    z[22]=2*z[22] + z[42];
    z[22]=z[10]*z[22];
    z[16]=z[16] + z[17] + z[31] + z[25] + z[21] + z[24] + z[28] + z[22]
    - z[52] - z[82];
    z[16]=z[1]*z[16];
    z[17]=static_cast<T>(37)+ 12*z[81];
    z[17]=z[17]*z[41];
    z[21]=z[62]*z[54];
    z[21]=z[21] + 11*z[6];
    z[21]=z[21]*z[11];
    z[22]=static_cast<T>(10)+ z[21];
    z[22]=z[22]*z[59];
    z[24]=16*z[37];
    z[17]=z[22] - z[24] + z[17];
    z[17]=z[7]*z[17];
    z[22]=z[40] + 11;
    z[25]=z[22]*z[67];
    z[25]=z[50] + z[25];
    z[25]=z[7]*z[25];
    z[25]=z[25] + z[65];
    z[25]=z[9]*z[25];
    z[28]=z[37]*z[58];
    z[28]=z[49] + z[28];
    z[17]=z[25] + 8*z[28] + z[17];
    z[17]=z[9]*z[17];
    z[25]= - z[56] + z[46];
    z[28]=z[79] + 11*z[54];
    z[28]=z[28]*z[11];
    z[31]=10*z[6] + z[28];
    z[31]=z[11]*z[31];
    z[31]=static_cast<T>(3)+ z[31];
    z[31]=z[10]*z[31];
    z[25]=2*z[25] + z[31];
    z[25]=z[10]*z[25];
    z[22]=z[22]*z[45];
    z[31]=z[46] + 11*z[12];
    z[31]=2*z[31];
    z[37]=37*z[6] + 12*z[61];
    z[37]=z[11]*z[37];
    z[37]=static_cast<T>(32)+ z[37];
    z[37]=z[11]*z[37];
    z[37]=z[7] + z[31] + z[37];
    z[37]=z[7]*z[37];
    z[17]=z[17] + z[37] + z[25] + z[26] + z[22];
    z[17]=z[9]*z[17];
    z[22]= - z[24]*z[18];
    z[22]=24*z[60] - z[26] + z[22];
    z[22]=z[9]*z[22];
    z[25]=z[26] - z[24];
    z[25]=z[8]*z[25];
    z[37]= - static_cast<T>(25)+ z[29];
    z[23]=z[37]*z[23];
    z[22]=z[22] + z[23] + z[25] - n<T>(3,2)*z[13] + 10*z[12];
    z[22]=z[9]*z[22];
    z[23]=n<T>(1,2)*z[13];
    z[25]=8*z[12];
    z[37]=z[23] + z[25];
    z[37]=z[8]*z[37];
    z[18]=z[12]*z[18];
    z[18]= - z[10] + z[18];
    z[18]=z[18]*z[43];
    z[18]= - z[75] + z[18];
    z[18]=z[9]*z[18];
    z[32]= - z[8]*z[32];
    z[18]=z[18] + z[32];
    z[18]=z[4]*z[18];
    z[32]=n<T>(5,2)*z[13] + z[64];
    z[32]=z[9]*z[32];
    z[32]=n<T>(1,2) + z[32];
    z[32]=z[9]*z[32];
    z[32]= - z[8] + z[32];
    z[32]=z[5]*z[32];
    z[18]=z[18] + z[32] + z[22] - n<T>(3,2)*z[29] + n<T>(7,2) + z[37];
    z[18]=z[4]*z[18];
    z[22]=z[47] + 1;
    z[22]=z[24]*z[22];
    z[24]= - 24*z[12] - z[14];
    z[24]=z[7]*z[24];
    z[22]=z[24] + z[22];
    z[22]=z[9]*z[22];
    z[15]=static_cast<T>(13)+ n<T>(1,2)*z[15];
    z[15]=z[7]*z[15];
    z[15]=z[22] - z[31] + z[15];
    z[15]=z[9]*z[15];
    z[22]=z[53] - n<T>(7,2);
    z[22]=z[7]*z[22];
    z[22]=z[51] + z[22];
    z[22]=z[9]*z[22];
    z[19]= - static_cast<T>(7)+ z[19];
    z[19]=n<T>(1,2)*z[19] + z[22];
    z[19]=z[9]*z[19];
    z[22]=n<T>(1,2)*z[6];
    z[24]=z[39]*z[22];
    z[29]= - z[8] + z[33];
    z[19]=z[24] + n<T>(1,2)*z[29] + z[19];
    z[19]=z[2]*z[19];
    z[24]=z[25] - z[27];
    z[24]=z[8]*z[24];
    z[23]= - z[23] - z[27];
    z[23]=z[8]*z[23];
    z[23]= - n<T>(1,2) + z[23];
    z[23]=z[8]*z[23];
    z[23]=z[23] - z[6];
    z[23]=z[3]*z[23];
    z[15]=z[19] + z[23] + z[15] - z[73] + static_cast<T>(5)+ z[24];
    z[15]=z[2]*z[15];
    z[19]=z[8] + z[38];
    z[19]=z[19]*z[22];
    z[22]=npow(z[6],5)*z[62];
    z[22]=11*z[35] + z[22];
    z[22]=z[11]*z[22];
    z[22]=10*z[70] + z[22];
    z[22]=z[11]*z[22];
    z[19]=z[19] + z[22];
    z[19]=z[10]*z[19];
    z[22]=z[11]*z[35];
    z[22]= - 37*z[70] - 12*z[22];
    z[22]=z[11]*z[22];
    z[22]= - 32*z[54] + z[22];
    z[22]=z[11]*z[22];
    z[19]=z[19] + z[22] + 4*z[8] - n<T>(7,2)*z[6];
    z[19]=z[10]*z[19];
    z[22]=z[26]*z[36];
    z[23]=6*z[6];
    z[24]=z[23] + z[28];
    z[24]=z[24]*z[68];
    z[19]=z[19] + z[24] - static_cast<T>(7)+ z[22];
    z[19]=z[3]*z[19];
    z[22]= - z[50] - 11*z[67];
    z[22]=z[7]*z[22];
    z[22]=z[22] - z[65];
    z[22]=z[9]*z[22];
    z[24]= - 37*z[41] - 10*z[59];
    z[24]=z[7]*z[24];
    z[22]=z[22] - 8*z[49] + z[24];
    z[22]=z[9]*z[22];
    z[24]= - 32*z[11] - n<T>(7,2)*z[7];
    z[24]=z[7]*z[24];
    z[22]=z[22] + z[24] - z[26] - 22*z[41];
    z[22]=z[9]*z[22];
    z[24]= - static_cast<T>(7)- z[30];
    z[14]=z[24]*z[14];
    z[24]= - z[13] - 6*z[11];
    z[14]=z[22] + 2*z[24] + z[14];
    z[14]=z[9]*z[14];
    z[22]=z[8]*z[44];
    z[22]=static_cast<T>(3)+ z[22];
    z[14]=2*z[22] + z[14];
    z[14]=z[5]*z[14];
    z[20]=37*z[54] + 12*z[20];
    z[20]=z[11]*z[20];
    z[20]=z[34] + z[20];
    z[20]=z[11]*z[20];
    z[22]= - 11*z[70] - z[48];
    z[22]=z[11]*z[22];
    z[22]= - 10*z[54] + z[22];
    z[22]=z[22]*z[68];
    z[22]=z[22] - z[23] - n<T>(1,2)*z[8];
    z[22]=z[10]*z[22];
    z[23]=8*z[84];
    z[20]=z[22] + z[20] + n<T>(9,2) - z[23];
    z[20]=z[10]*z[20];
    z[21]=static_cast<T>(6)+ z[21];
    z[21]=z[21]*z[68];
    z[22]=5*z[81] - n<T>(17,2) - z[23];
    z[22]=z[7]*z[22];
    z[14]=z[16] + z[15] + z[18] + z[14] + z[19] + z[17] + z[22] + z[20]
    + z[21] + z[13] - z[27];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r344(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r344(const std::array<dd_real,31>&);
#endif
