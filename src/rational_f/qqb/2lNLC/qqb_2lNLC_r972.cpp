#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r972(const std::array<T,31>& k) {
  T z[81];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[8];
    z[12]=k[2];
    z[13]=k[9];
    z[14]=k[15];
    z[15]=z[4] + z[7];
    z[16]=n<T>(1,2)*z[2];
    z[17]=z[15]*z[16];
    z[18]=z[7] + z[5];
    z[19]=z[18]*z[3];
    z[20]=z[7]*z[5];
    z[21]=91*z[20];
    z[22]=n<T>(1,2)*z[4];
    z[23]= - z[7]*z[22];
    z[17]=z[17] - n<T>(91,2)*z[19] + z[21] + z[23];
    z[23]=npow(z[6],2);
    z[17]=z[17]*z[23];
    z[24]=npow(z[3],2);
    z[25]=n<T>(241,2)*z[24];
    z[26]=z[18]*z[25];
    z[27]=npow(z[5],3);
    z[28]=3*z[27];
    z[29]=npow(z[7],3);
    z[26]=z[26] + z[28] + n<T>(151,2)*z[29];
    z[30]=n<T>(1,2)*z[3];
    z[26]=z[26]*z[30];
    z[31]=npow(z[2],2);
    z[32]=81*z[31];
    z[33]=z[15]*z[32];
    z[34]=npow(z[4],3);
    z[35]=39*z[29];
    z[33]=z[33] + z[35] - 155*z[34];
    z[36]=n<T>(1,4)*z[2];
    z[33]=z[33]*z[36];
    z[37]=npow(z[7],2);
    z[38]=z[37]*z[5];
    z[39]=z[28] + n<T>(151,2)*z[38];
    z[39]=z[39]*z[7];
    z[40]=npow(z[4],2);
    z[41]=z[40]*z[7];
    z[41]= - z[35] + 155*z[41];
    z[42]=n<T>(1,4)*z[4];
    z[43]=z[41]*z[42];
    z[17]=z[17] + z[33] + z[26] - z[39] + z[43];
    z[17]=z[6]*z[17];
    z[26]=z[40]*z[5];
    z[33]=117*z[27];
    z[26]=z[33] + n<T>(875,2)*z[26];
    z[26]=z[26]*z[4];
    z[43]=n<T>(833,2)*z[24];
    z[44]=z[4]*z[5];
    z[45]= - z[44]*z[43];
    z[45]=z[26] + z[45];
    z[45]=z[3]*z[45];
    z[46]=n<T>(79,2)*z[3];
    z[47]=z[31]*z[44]*z[46];
    z[45]=z[45] + z[47];
    z[45]=z[2]*z[45];
    z[25]= - z[20]*z[25];
    z[25]= - z[39] + z[25];
    z[25]=z[3]*z[25];
    z[41]=z[4]*z[41];
    z[47]=z[4]*z[7];
    z[48]= - z[47]*z[32];
    z[41]=z[41] + z[48];
    z[41]=z[41]*z[16];
    z[48]=z[3]*z[21];
    z[49]= - z[2]*z[47];
    z[48]=z[48] + z[49];
    z[23]=z[48]*z[23];
    z[23]=z[23] + z[25] + z[41];
    z[23]=z[6]*z[23];
    z[23]=z[45] + z[23];
    z[23]=z[1]*z[23];
    z[25]= - 241*z[20] - 833*z[44];
    z[24]=z[25]*z[24];
    z[24]=n<T>(1,2)*z[24] - z[39] + z[26];
    z[24]=z[24]*z[30];
    z[25]=n<T>(31,2)*z[7];
    z[26]=175*z[5];
    z[41]=z[25] + z[26];
    z[45]=n<T>(5,2)*z[40];
    z[41]=z[41]*z[45];
    z[48]=z[28] - n<T>(1,4)*z[29];
    z[48]=39*z[48] + z[41];
    z[48]=z[4]*z[48];
    z[49]=z[4] + z[5];
    z[43]=z[49]*z[43];
    z[34]=z[43] - z[33] - n<T>(875,2)*z[34];
    z[34]=z[34]*z[30];
    z[43]= - z[49]*z[46];
    z[49]=79*z[5];
    z[50]=z[49] - n<T>(81,2)*z[7];
    z[50]=z[4]*z[50];
    z[43]=z[50] + z[43];
    z[43]=z[43]*z[31];
    z[34]=n<T>(1,2)*z[43] + z[48] + z[34];
    z[34]=z[2]*z[34];
    z[17]=n<T>(1,2)*z[23] + z[17] + z[24] + z[34];
    z[17]=z[1]*z[17];
    z[23]=npow(z[5],2);
    z[24]=3*z[23];
    z[34]=z[5]*z[9];
    z[43]=z[34] - 1;
    z[48]=z[43]*z[24];
    z[50]=z[7]*z[34];
    z[50]= - z[5] + z[50];
    z[50]=z[7]*z[50];
    z[48]=z[48] + n<T>(151,2)*z[50];
    z[48]=z[7]*z[48];
    z[50]= - static_cast<T>(1)+ n<T>(1,2)*z[34];
    z[50]=z[50]*z[4];
    z[51]=n<T>(1,2)*z[5];
    z[52]=z[51] + z[50];
    z[52]=z[4]*z[52];
    z[53]=z[33]*z[9];
    z[52]=z[53] + 875*z[52];
    z[52]=z[4]*z[52];
    z[54]=n<T>(241,2)*z[7];
    z[55]=z[34]*z[54];
    z[50]=z[55] - 833*z[50];
    z[50]=z[3]*z[50];
    z[50]=z[50] + n<T>(241,2)*z[20];
    z[50]=z[3]*z[50];
    z[48]=z[50] + z[48] + z[52];
    z[48]=z[48]*z[30];
    z[50]=z[4]*z[8];
    z[52]=z[43] + z[50];
    z[55]=z[3]*z[52];
    z[55]= - z[4] + z[55];
    z[55]=z[3]*z[55];
    z[40]=z[8]*z[40];
    z[40]=17*z[5] - 25*z[40];
    z[40]=z[4]*z[40];
    z[40]=n<T>(833,2)*z[55] - z[53] + n<T>(35,2)*z[40];
    z[40]=z[40]*z[30];
    z[53]=z[3]*z[49];
    z[44]=265*z[44] + z[53];
    z[52]= - z[52]*z[46];
    z[52]=z[52] - z[49] + z[4];
    z[52]=z[2]*z[52];
    z[44]=n<T>(1,2)*z[44] + z[52];
    z[44]=z[44]*z[16];
    z[25]=z[25] - 103*z[4];
    z[25]=z[25]*z[45];
    z[25]=z[44] + z[40] - z[33] + z[25];
    z[25]=z[2]*z[25];
    z[33]=31*z[7];
    z[40]=z[33] - z[26];
    z[44]=z[40]*z[45];
    z[45]=z[28] + n<T>(1,2)*z[29];
    z[45]=39*z[45] - z[44];
    z[45]=z[4]*z[45];
    z[49]=z[49] + 81*z[7];
    z[49]=z[49]*z[31]*z[22];
    z[45]=z[45] + z[49];
    z[45]=z[10]*z[45]*z[16];
    z[49]=npow(z[11],3);
    z[52]=n<T>(7,2)*z[49];
    z[27]= - n<T>(13,4)*z[29] - z[52] + 39*z[27];
    z[27]=3*z[27] + z[41];
    z[27]=z[4]*z[27];
    z[17]=z[17] + z[45] + z[25] + z[48] - n<T>(1,2)*z[39] + z[27];
    z[25]=z[7]*z[9];
    z[27]=z[25] - 1;
    z[41]= - z[27]*z[22];
    z[45]=z[25] + z[34];
    z[48]=n<T>(91,2)*z[3];
    z[48]=z[45]*z[48];
    z[53]=z[50] + 1;
    z[55]= - z[53]*z[16];
    z[56]= - z[2]*z[7];
    z[21]=z[21] + z[56];
    z[56]=n<T>(1,2)*z[10];
    z[21]=z[21]*z[56];
    z[21]=z[21] + z[55] + z[48] + z[41] - n<T>(91,2)*z[5] - 45*z[7];
    z[21]=z[6]*z[21];
    z[15]= - z[2]*z[15];
    z[15]=91*z[19] + z[15];
    z[15]=n<T>(1,2)*z[15] + z[21];
    z[19]=n<T>(1,4)*z[6];
    z[15]=z[15]*z[19];
    z[21]=z[43]*z[23];
    z[41]=n<T>(151,16)*z[7];
    z[48]= - z[27]*z[41];
    z[48]= - 8*z[5] + z[48];
    z[48]=z[7]*z[48];
    z[45]= - z[3]*z[45];
    z[18]=z[45] - z[18];
    z[18]=z[3]*z[18];
    z[18]=n<T>(241,16)*z[18] - n<T>(3,8)*z[21] + z[48];
    z[18]=z[3]*z[18];
    z[21]=n<T>(271,2)*z[5] + 95*z[7];
    z[21]=z[21]*z[37];
    z[45]=155*z[4];
    z[27]=z[27]*z[4];
    z[48]=z[7] + z[27];
    z[48]=z[48]*z[45];
    z[35]= - z[9]*z[35];
    z[35]=z[35] + z[48];
    z[35]=z[35]*z[22];
    z[21]=z[35] + z[28] + z[21];
    z[28]=27*z[31];
    z[35]= - z[7]*z[28];
    z[29]= - 13*z[29] + z[35];
    z[35]=n<T>(3,2)*z[2];
    z[29]=z[29]*z[35];
    z[29]= - z[39] + z[29];
    z[39]=n<T>(1,8)*z[10];
    z[29]=z[29]*z[39];
    z[48]= - static_cast<T>(1)+ z[50];
    z[45]=z[48]*z[45];
    z[45]= - 583*z[7] + z[45];
    z[45]=z[4]*z[45];
    z[48]= - z[2]*z[53];
    z[48]= - z[4] + z[48];
    z[48]=z[2]*z[48];
    z[45]=z[45] + 81*z[48];
    z[45]=z[2]*z[45];
    z[15]=z[15] + z[29] + n<T>(1,16)*z[45] + n<T>(1,8)*z[21] + z[18];
    z[15]=z[6]*z[15];
    z[15]=z[15] + n<T>(1,4)*z[17];
    z[15]=z[1]*z[15];
    z[17]=npow(z[14],2);
    z[18]=26*z[17];
    z[21]=npow(z[14],3);
    z[29]=z[21]*z[9];
    z[45]=n<T>(381,8)*z[29];
    z[48]=z[18] - z[45];
    z[53]=z[9]*z[48];
    z[55]=n<T>(447,8) + 41*z[34];
    z[55]=z[55]*z[51];
    z[57]=n<T>(1,16)*z[4];
    z[58]=static_cast<T>(2157)+ 1561*z[50];
    z[58]=z[58]*z[57];
    z[59]=n<T>(833,16)*z[3];
    z[60]=z[9] + z[8];
    z[61]=z[60]*z[3];
    z[62]=static_cast<T>(1)- z[61];
    z[62]=z[62]*z[59];
    z[53]=z[62] + z[58] + z[55] - n<T>(2433,16)*z[14] + z[53];
    z[53]=z[3]*z[53];
    z[55]=n<T>(1,8)*z[4];
    z[58]= - n<T>(21,2)*z[5] + 565*z[7];
    z[58]=z[58]*z[55];
    z[61]=static_cast<T>(1)+ z[61];
    z[61]=z[2]*z[61];
    z[61]=z[61] - z[3];
    z[62]= - n<T>(53,4)*z[5] + 11*z[4];
    z[61]=5*z[62] + n<T>(79,4)*z[61];
    z[61]=z[61]*z[36];
    z[62]=n<T>(381,4)*z[29];
    z[18]=z[61] + z[53] + z[58] + n<T>(41,2)*z[23] + z[18] - z[62];
    z[18]=z[2]*z[18];
    z[53]=z[49]*z[9];
    z[58]=21*z[53];
    z[61]=npow(z[11],2);
    z[63]= - n<T>(887,2)*z[61] - z[58];
    z[64]=z[3]*z[34];
    z[64]= - n<T>(699,2)*z[64] - 997*z[5] + n<T>(963,2)*z[7];
    z[64]=z[64]*z[30];
    z[65]=z[43]*z[5];
    z[66]= - n<T>(3067,4)*z[13] - 3*z[65];
    z[66]=z[5]*z[66];
    z[67]=n<T>(41,4)*z[5] - 17*z[7];
    z[67]=z[7]*z[67];
    z[47]=z[64] - n<T>(877,4)*z[47] + 5*z[67] + n<T>(1,2)*z[63] + z[66];
    z[32]=z[32] + 887*z[61] + 69*z[37];
    z[32]=z[32]*z[16];
    z[63]=npow(z[13],2);
    z[64]= - n<T>(1397,2)*z[63] - z[24];
    z[64]=z[5]*z[64];
    z[66]=21*z[49];
    z[32]=z[32] + n<T>(271,2)*z[38] + z[66] + z[64];
    z[38]=npow(z[13],3);
    z[64]=31*z[38];
    z[67]=z[64]*z[5];
    z[68]=n<T>(21,8)*z[49];
    z[69]=z[68]*z[2];
    z[67]=z[67] + z[69];
    z[67]=z[67]*z[10];
    z[32]=n<T>(1,8)*z[32] - z[67];
    z[32]=z[10]*z[32];
    z[70]=z[2] + 91*z[5];
    z[56]=z[70]*z[56];
    z[71]=z[16]*z[8];
    z[72]=z[56] + z[71] - n<T>(1,2) + 91*z[34];
    z[19]=z[72]*z[19];
    z[72]=z[3]*z[25];
    z[19]=z[19] + z[36] + n<T>(151,8)*z[72] + n<T>(91,4)*z[5] + 15*z[27];
    z[19]=z[6]*z[19];
    z[27]=z[2]*z[8];
    z[72]=z[27] + 1;
    z[72]=z[72]*z[2];
    z[73]=7*z[11] - n<T>(153,4)*z[7];
    z[74]=static_cast<T>(26)- n<T>(107,16)*z[50];
    z[74]=z[4]*z[74];
    z[73]=n<T>(81,16)*z[72] + n<T>(1,4)*z[73] + z[74];
    z[73]=z[2]*z[73];
    z[19]=z[19] + z[32] + n<T>(1,4)*z[47] + z[73];
    z[19]=z[6]*z[19];
    z[32]=npow(z[8],2);
    z[47]=31*z[13];
    z[73]=z[32]*z[47];
    z[74]=n<T>(147,16)*z[8] + z[73];
    z[74]=z[13]*z[74];
    z[74]=n<T>(1365,16) + z[74];
    z[74]=z[13]*z[74];
    z[75]= - n<T>(373,4) + 27*z[34];
    z[75]=z[75]*z[51];
    z[76]=z[9] - z[8];
    z[77]= - z[7]*z[76];
    z[77]=static_cast<T>(1)+ z[77];
    z[77]=z[77]*z[41];
    z[74]=z[77] + z[74] + z[75];
    z[74]=z[7]*z[74];
    z[75]=z[8] - z[12];
    z[77]= - z[75]*z[47];
    z[77]= - n<T>(147,16) + z[77];
    z[77]=z[77]*z[63];
    z[78]=2451*z[5] + 1561*z[4];
    z[57]=z[78]*z[57];
    z[76]= - z[76]*z[54];
    z[76]= - static_cast<T>(537)+ z[76];
    z[76]=z[3]*z[76];
    z[43]=z[4]*z[43];
    z[43]=z[76] - 241*z[7] - n<T>(1243,2)*z[43];
    z[43]=z[3]*z[43];
    z[43]=n<T>(1,8)*z[43] + z[57] + z[77] + z[74];
    z[43]=z[3]*z[43];
    z[26]=z[26] + z[33];
    z[33]=n<T>(31,4)*z[25] - static_cast<T>(103)+ n<T>(175,2)*z[34];
    z[33]=z[4]*z[33];
    z[26]=n<T>(1,2)*z[26] + z[33];
    z[26]=z[4]*z[26];
    z[33]=n<T>(971,2)*z[61] + z[58];
    z[57]=n<T>(1,4)*z[7];
    z[74]=z[37]*z[9];
    z[76]=1445*z[11] - 39*z[74];
    z[76]=z[76]*z[57];
    z[77]=117*z[23];
    z[78]=z[77]*z[9];
    z[79]=1303*z[14] + z[78];
    z[79]=z[5]*z[79];
    z[26]=5*z[26] + z[76] + n<T>(1,2)*z[33] + z[79];
    z[26]=z[26]*z[42];
    z[33]=589*z[17];
    z[76]=z[33] + z[77];
    z[76]=z[5]*z[76];
    z[77]=1219*z[61] + 39*z[37];
    z[77]=z[7]*z[77];
    z[44]= - z[44] + n<T>(1,2)*z[77] - z[66] + z[76];
    z[44]=z[4]*z[44];
    z[66]=127*z[21];
    z[76]=z[66]*z[5];
    z[77]=7*z[49];
    z[79]=z[77]*z[7];
    z[76]=z[76] + z[79];
    z[76]=z[76]*z[10]*z[4];
    z[79]=53*z[5] + 97*z[7];
    z[79]=z[2]*z[4]*z[79];
    z[79]= - 381*z[21] + n<T>(5,2)*z[79];
    z[79]=z[2]*z[79];
    z[44]=3*z[76] + z[44] + z[79];
    z[39]=z[44]*z[39];
    z[44]=271*z[5] - 39*z[7];
    z[37]=z[44]*z[37];
    z[15]=z[15] + z[19] + z[39] + z[18] + z[43] + z[26] - z[64] + n<T>(1,16)
   *z[37];
    z[15]=z[1]*z[15];
    z[18]=npow(z[8],3);
    z[19]=62*z[13];
    z[26]=z[18]*z[19];
    z[37]= - n<T>(1089,16)*z[32] - z[26];
    z[37]=z[13]*z[37];
    z[37]= - n<T>(1083,16)*z[8] + z[37];
    z[37]=z[13]*z[37];
    z[39]=z[9]*z[8];
    z[43]=z[39] + z[32];
    z[44]= - z[7]*z[43];
    z[44]=z[8] + z[44];
    z[41]=z[44]*z[41];
    z[37]=z[41] - n<T>(35,2)*z[34] - n<T>(85,16) + z[37];
    z[37]=z[7]*z[37];
    z[41]=z[8]*z[75]*z[19];
    z[44]= - 4*z[12] + n<T>(99,16)*z[8];
    z[41]=11*z[44] + z[41];
    z[41]=z[13]*z[41];
    z[41]=static_cast<T>(85)+ z[41];
    z[41]=z[13]*z[41];
    z[44]=127*z[29];
    z[79]= - 23*z[17] - z[44];
    z[79]=z[9]*z[79];
    z[80]= - static_cast<T>(425)- 131*z[34];
    z[55]=z[80]*z[55];
    z[43]= - z[43]*z[54];
    z[43]=537*z[8] + z[43];
    z[43]=z[43]*z[30];
    z[54]=z[7]*z[60];
    z[43]=z[43] - static_cast<T>(217)+ n<T>(241,4)*z[54];
    z[54]=n<T>(1,4)*z[3];
    z[43]=z[43]*z[54];
    z[37]=z[43] + z[55] + z[37] - n<T>(573,16)*z[5] + n<T>(3,4)*z[79] + n<T>(143,4)*
    z[14] + z[41];
    z[37]=z[3]*z[37];
    z[41]=175*z[34] - 31*z[25];
    z[41]=z[4]*z[41];
    z[40]=z[41] - z[40];
    z[40]=z[4]*z[40];
    z[41]=n<T>(1591,2)*z[61] - 1297*z[17];
    z[43]=4033*z[14] + z[78];
    z[43]=z[43]*z[51];
    z[55]= - 83*z[11] + 13*z[74];
    z[55]=z[7]*z[55];
    z[40]=n<T>(5,4)*z[40] + n<T>(3,4)*z[55] + z[43] + n<T>(1,2)*z[41] + z[58];
    z[40]=z[40]*z[42];
    z[41]=401*z[5] + 615*z[7];
    z[41]=z[41]*z[42];
    z[43]=173*z[17];
    z[55]=381*z[29];
    z[31]=n<T>(81,4)*z[31] + z[41] + z[55] + n<T>(597,4)*z[61] + z[43];
    z[31]=z[31]*z[36];
    z[41]=z[17]*z[5];
    z[60]=z[61]*z[7];
    z[52]=n<T>(599,4)*z[60] + n<T>(571,2)*z[41] + z[52] - z[66];
    z[52]=z[4]*z[52];
    z[52]=z[76] + z[52];
    z[38]=62*z[38];
    z[21]=n<T>(381,8)*z[21];
    z[74]= - z[21] - z[38];
    z[74]=z[5]*z[74];
    z[78]=z[68]*z[7];
    z[49]=n<T>(21,4)*z[49];
    z[79]=z[49]*z[2];
    z[52]= - z[79] + z[74] - z[78] + n<T>(3,4)*z[52];
    z[52]=z[10]*z[52];
    z[21]=z[21] + z[64];
    z[33]= - z[33] - n<T>(2101,2)*z[63];
    z[23]=n<T>(1,2)*z[33] + 57*z[23];
    z[23]=z[5]*z[23];
    z[23]=z[52] + z[31] + z[40] - n<T>(1219,16)*z[60] + n<T>(1,4)*z[23] + z[21];
    z[23]=z[10]*z[23];
    z[31]= - 969*z[61] + 1397*z[63];
    z[33]= - n<T>(27,2)*z[72] - n<T>(499,2)*z[11] - 9*z[7];
    z[33]=z[33]*z[35];
    z[35]= - n<T>(1511,2)*z[13] - z[65];
    z[35]=z[5]*z[35];
    z[20]=z[33] - n<T>(213,4)*z[20] + n<T>(3,2)*z[35] + n<T>(1,4)*z[31] - z[58];
    z[24]= - n<T>(2855,2)*z[63] - z[24];
    z[24]=z[5]*z[24];
    z[28]=323*z[61] + z[28];
    z[28]=z[2]*z[28];
    z[31]= - z[5]*z[38];
    z[31]=z[31] - z[79];
    z[31]=z[10]*z[31];
    z[24]=z[31] + n<T>(3,16)*z[28] + n<T>(1,8)*z[24] + z[49] + z[64];
    z[24]=z[10]*z[24];
    z[20]=n<T>(1,4)*z[20] + z[24];
    z[20]=z[10]*z[20];
    z[24]= - z[8]*z[36];
    z[28]=z[10]*z[70];
    z[24]=n<T>(1,4)*z[28] + z[24] - static_cast<T>(23)+ n<T>(91,4)*z[34];
    z[24]=z[6]*z[10]*z[24];
    z[28]=z[56] + static_cast<T>(45)- z[71];
    z[24]=n<T>(1,2)*z[28] + z[24];
    z[24]=z[6]*z[24];
    z[28]= - static_cast<T>(53)+ n<T>(89,2)*z[25];
    z[28]=z[28]*z[42];
    z[31]=n<T>(49,2)*z[25] + n<T>(463,2) - 413*z[34];
    z[31]=z[31]*z[54];
    z[33]=static_cast<T>(417)- 173*z[50];
    z[33]=z[33]*z[36];
    z[35]=z[61]*z[9];
    z[24]=z[24] + z[33] + z[31] + z[28] + n<T>(69,4)*z[7] - n<T>(1419,4)*z[5] - 
   n<T>(145,4)*z[35] + 61*z[11] + n<T>(1555,8)*z[13];
    z[20]=z[20] + n<T>(1,2)*z[24];
    z[20]=z[6]*z[20];
    z[24]=141*z[17] + z[44];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(235,2)*z[14] + 3*z[24];
    z[24]=z[9]*z[24];
    z[28]=z[39]*z[3];
    z[28]=z[28] - z[9];
    z[31]= - z[28]*z[59];
    z[24]=z[31] - n<T>(683,8)*z[50] - n<T>(235,16)*z[34] - static_cast<T>(95)+ n<T>(1,4)*z[24];
    z[24]=z[3]*z[24];
    z[29]=n<T>(381,2)*z[29];
    z[31]=149*z[17] + z[29];
    z[31]=z[9]*z[31];
    z[28]= - 81*z[8] + 79*z[28];
    z[16]=z[28]*z[16];
    z[28]= - z[8]*z[46];
    z[16]=z[16] - static_cast<T>(109)+ z[28];
    z[28]=n<T>(1,8)*z[2];
    z[16]=z[16]*z[28];
    z[33]=n<T>(21,4)*z[11] - 13*z[14];
    z[16]=z[16] + z[24] + n<T>(255,4)*z[4] - n<T>(1039,16)*z[7] - n<T>(9,4)*z[5] + 
   n<T>(19,4)*z[33] + z[31];
    z[16]=z[2]*z[16];
    z[24]=z[47]*z[12];
    z[31]=n<T>(1139,16) - z[24];
    z[31]=z[31]*z[63];
    z[33]= - n<T>(639,4)*z[14] - 127*z[13];
    z[33]=n<T>(5,2)*z[33] + 11*z[5];
    z[33]=z[33]*z[51];
    z[19]=z[32]*z[19];
    z[19]=n<T>(851,16)*z[8] + z[19];
    z[19]=z[13]*z[19];
    z[19]=n<T>(1213,16) + z[19];
    z[19]=z[13]*z[19];
    z[38]=95*z[8] + n<T>(151,2)*z[9];
    z[38]=z[7]*z[38];
    z[38]= - static_cast<T>(75)+ z[38];
    z[38]=z[7]*z[38];
    z[19]=n<T>(1,8)*z[38] - n<T>(175,8)*z[5] - n<T>(293,8)*z[11] + z[19];
    z[19]=z[7]*z[19];
    z[38]=145*z[35];
    z[39]=z[38] - n<T>(343,2)*z[11] - 797*z[14];
    z[40]= - static_cast<T>(49)+ n<T>(155,8)*z[25];
    z[40]=z[4]*z[40];
    z[39]=z[40] - n<T>(169,4)*z[7] + n<T>(1,4)*z[39] + 277*z[5];
    z[39]=z[39]*z[22];
    z[15]=z[15] + z[20] + z[23] + z[16] + z[37] + z[39] + z[19] + z[33]
    + z[31] + z[48];
    z[15]=z[1]*z[15];
    z[16]=z[77] + z[66];
    z[16]=z[9]*z[16];
    z[19]=155*z[61];
    z[20]=z[5]*z[14];
    z[23]=z[7]*z[11];
    z[16]= - n<T>(595,2)*z[23] + n<T>(4407,4)*z[20] + n<T>(3,2)*z[16] + z[19] - n<T>(743,2)*z[17];
    z[16]=z[16]*z[42];
    z[20]=z[77] - z[66];
    z[20]=n<T>(289,4)*z[60] + n<T>(3,4)*z[20] + 281*z[41];
    z[20]=z[20]*z[22];
    z[22]= - z[5]*z[21];
    z[20]=n<T>(3,8)*z[76] - z[69] + z[20] + z[22] - z[78];
    z[20]=z[10]*z[20];
    z[22]=z[14]*z[12];
    z[23]=381*z[22];
    z[31]= - static_cast<T>(743)+ z[23];
    z[31]=z[31]*z[17];
    z[24]= - n<T>(481,8) - z[24];
    z[24]=z[24]*z[63];
    z[24]=n<T>(1,8)*z[31] + z[24];
    z[24]=z[5]*z[24];
    z[19]= - z[19] - n<T>(21,2)*z[53];
    z[19]=z[19]*z[57];
    z[31]=41*z[61] + z[58];
    z[28]=z[31]*z[28];
    z[16]=z[20] + z[28] + z[16] + z[19] + z[24] + z[21];
    z[16]=z[10]*z[16];
    z[19]=44*z[8] + z[73];
    z[19]=z[13]*z[19];
    z[19]=static_cast<T>(57)+ z[19];
    z[19]=z[13]*z[19];
    z[20]=n<T>(955,16)*z[11];
    z[19]= - n<T>(145,8)*z[35] + z[20] + z[19];
    z[19]=z[7]*z[19];
    z[21]= - z[29] + n<T>(145,2)*z[61] - z[43];
    z[21]=z[9]*z[21];
    z[24]= - 375*z[11] - 2137*z[14];
    z[21]=n<T>(1,4)*z[24] + z[21];
    z[21]=z[21]*z[36];
    z[24]=static_cast<T>(181)- n<T>(381,2)*z[22];
    z[24]=z[24]*z[17];
    z[28]=z[8] + z[12];
    z[28]=z[28]*z[47];
    z[28]=z[28] + n<T>(481,8);
    z[31]=z[28]*z[63];
    z[33]= - n<T>(2867,16) + 52*z[22];
    z[33]=z[14]*z[33];
    z[35]=44*z[12];
    z[36]= - z[13]*z[35];
    z[36]= - n<T>(347,4) + z[36];
    z[36]=z[13]*z[36];
    z[33]=z[33] + z[36];
    z[33]=z[5]*z[33];
    z[36]=n<T>(145,4)*z[61] + 177*z[17];
    z[37]=n<T>(1,2)*z[9];
    z[36]=z[36]*z[37];
    z[20]=n<T>(783,16)*z[7] + n<T>(613,8)*z[5] + z[36] - z[20] - 49*z[14];
    z[20]=z[4]*z[20];
    z[16]=z[16] + z[21] + z[20] + z[19] + z[33] - z[45] + n<T>(1,4)*z[24] + 
    z[31];
    z[16]=z[10]*z[16];
    z[19]=z[5]*z[63];
    z[20]=n<T>(41,8)*z[61];
    z[21]=z[2]*z[20];
    z[19]= - z[67] + z[21] - n<T>(729,8)*z[19] + z[68] + z[64];
    z[19]=z[10]*z[19];
    z[21]=z[8]*z[47];
    z[21]=n<T>(729,8) + z[21];
    z[21]=z[21]*z[63];
    z[24]=z[5]*z[13];
    z[31]=z[2]*z[11];
    z[19]=z[19] - n<T>(95,2)*z[31] - n<T>(2469,16)*z[24] - n<T>(21,8)*z[53] - z[20]
    + z[21];
    z[19]=z[10]*z[19];
    z[20]=z[13]*z[8];
    z[21]=n<T>(1557,2) + 377*z[20];
    z[21]=z[13]*z[21];
    z[21]= - 703*z[5] - z[38] + n<T>(375,2)*z[11] + z[21];
    z[21]=n<T>(1,2)*z[21] + 151*z[2];
    z[19]=n<T>(1,4)*z[21] + z[19];
    z[19]=z[10]*z[19];
    z[21]=z[9]*z[11];
    z[20]=n<T>(145,2)*z[21] + static_cast<T>(165)+ n<T>(377,2)*z[20];
    z[20]= - n<T>(43,2)*z[27] + n<T>(1,4)*z[20] - 51*z[34];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[6]*z[19];
    z[20]= - z[75]*z[73];
    z[24]=z[35] - n<T>(471,8)*z[8];
    z[24]=z[8]*z[24];
    z[20]=z[24] + z[20];
    z[20]=z[13]*z[20];
    z[24]=z[12] - n<T>(5,4)*z[8];
    z[20]=57*z[24] + z[20];
    z[20]=z[13]*z[20];
    z[24]=static_cast<T>(175)- n<T>(381,8)*z[22];
    z[24]=z[24]*z[17];
    z[24]=z[24] + z[62];
    z[24]=z[9]*z[24];
    z[27]=n<T>(2807,2) - 173*z[22];
    z[27]=z[14]*z[27];
    z[24]=n<T>(1,4)*z[27] + z[24];
    z[24]=z[9]*z[24];
    z[27]=npow(z[8],4)*z[47];
    z[18]=n<T>(471,8)*z[18] + z[27];
    z[18]=z[13]*z[18];
    z[18]=n<T>(1259,16)*z[32] + z[18];
    z[18]=z[13]*z[18];
    z[18]=n<T>(63,2)*z[9] + n<T>(561,8)*z[8] + z[18];
    z[18]=z[7]*z[18];
    z[27]= - static_cast<T>(31)- n<T>(2137,4)*z[22];
    z[18]=z[18] + z[24] + n<T>(1,4)*z[27] + z[20];
    z[18]=z[3]*z[18];
    z[20]= - n<T>(145,4)*z[11] + 177*z[14];
    z[20]=z[9]*z[20];
    z[20]=n<T>(307,2) + z[20];
    z[20]=n<T>(233,16)*z[25] + n<T>(1,4)*z[20] - 30*z[34];
    z[20]=z[4]*z[20];
    z[24]= - 175*z[17] - z[62];
    z[24]=z[9]*z[24];
    z[25]= - 2807*z[14] - n<T>(145,2)*z[11];
    z[24]=n<T>(1,8)*z[25] + z[24];
    z[24]=z[9]*z[24];
    z[25]= - 527*z[17] - z[29];
    z[25]=z[9]*z[25];
    z[25]= - n<T>(4185,4)*z[14] + z[25];
    z[25]=z[25]*z[37];
    z[25]= - static_cast<T>(139)+ z[25];
    z[25]=z[9]*z[25];
    z[25]=n<T>(221,4)*z[8] + z[25];
    z[25]=z[25]*z[30];
    z[24]=z[25] - static_cast<T>(39)+ z[24];
    z[24]=z[2]*z[24];
    z[23]=static_cast<T>(19)+ z[23];
    z[17]=z[23]*z[17];
    z[17]=z[17] + z[55];
    z[17]=z[9]*z[17];
    z[23]=n<T>(2143,2) - 35*z[22];
    z[23]=z[14]*z[23];
    z[17]=z[23] + z[17];
    z[23]=z[8]*z[28];
    z[23]=z[35] + z[23];
    z[23]=z[13]*z[23];
    z[23]=n<T>(347,4) + z[23];
    z[23]=z[13]*z[23];
    z[22]=static_cast<T>(1343)+ 2207*z[22];
    z[25]=z[13]*z[12];
    z[22]=n<T>(1,16)*z[22] - 57*z[25];
    z[22]=z[5]*z[22];
    z[25]= - n<T>(823,8)*z[32] - z[26];
    z[25]=z[13]*z[25];
    z[25]= - n<T>(513,4)*z[8] + z[25];
    z[25]=z[13]*z[25];
    z[21]=n<T>(145,16)*z[21] - n<T>(295,4) + z[25];
    z[21]=z[7]*z[21];
    z[15]=z[15] + z[19] + z[16] + z[24] + z[18] + z[20] + z[21] + z[22]
    + z[23] + n<T>(1,8)*z[17];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r972(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r972(const std::array<dd_real,31>&);
#endif
