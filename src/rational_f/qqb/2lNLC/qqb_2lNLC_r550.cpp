#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r550(const std::array<T,31>& k) {
  T z[125];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[8];
    z[9]=k[9];
    z[10]=k[15];
    z[11]=k[3];
    z[12]=k[4];
    z[13]=k[5];
    z[14]=k[18];
    z[15]=k[30];
    z[16]=k[19];
    z[17]=k[2];
    z[18]=k[23];
    z[19]=k[20];
    z[20]=k[24];
    z[21]=n<T>(19,4)*z[2];
    z[22]=2*z[15];
    z[23]=2*z[10];
    z[24]=z[23] + z[22] - z[21];
    z[24]=z[10]*z[24];
    z[25]=n<T>(3,4)*z[5];
    z[26]=2*z[7];
    z[27]= - z[26] + 8*z[2] - z[25];
    z[27]=z[7]*z[27];
    z[28]=n<T>(5,2)*z[2];
    z[29]=z[2]*z[11];
    z[30]=n<T>(1,2)*z[29];
    z[31]= - z[4]*z[30];
    z[31]=z[31] - 5*z[7] + z[28] + z[10];
    z[31]=z[4]*z[31];
    z[32]=z[5]*z[10];
    z[24]=z[31] + z[27] + z[24] + n<T>(11,4)*z[32];
    z[24]=z[4]*z[24];
    z[27]=6*z[29];
    z[31]=z[27] - n<T>(11,2);
    z[33]=z[7]*z[12];
    z[34]=n<T>(11,2)*z[33] + z[31];
    z[34]=z[4]*z[34];
    z[35]=6*z[2];
    z[36]=z[3]*z[12];
    z[37]=n<T>(1,2)*z[36];
    z[38]=static_cast<T>(1)- z[37];
    z[38]=z[5]*z[38];
    z[39]= - static_cast<T>(5)- z[37];
    z[39]=z[7]*z[39];
    z[34]=z[34] + z[39] + z[35] + z[38];
    z[34]=z[6]*z[34];
    z[38]=n<T>(1,4)*z[8];
    z[39]=z[8]*z[12];
    z[40]=z[3]*z[39];
    z[40]=z[2] + z[40];
    z[40]=z[40]*z[38];
    z[41]=z[33] - static_cast<T>(1)+ z[29];
    z[41]=z[4]*z[41];
    z[42]=n<T>(5,4)*z[2];
    z[41]=n<T>(5,4)*z[41] - z[42] + 8*z[7];
    z[41]=z[4]*z[41];
    z[43]=2*z[2];
    z[44]= - z[43] + z[7];
    z[44]=z[7]*z[44];
    z[34]=z[34] + z[41] + z[40] + z[44];
    z[34]=z[6]*z[34];
    z[40]=5*z[3];
    z[41]= - z[2] + z[40];
    z[41]=z[41]*z[38];
    z[44]=n<T>(3,4)*z[2];
    z[45]=z[44] + z[10];
    z[45]=z[10]*z[45];
    z[41]=z[41] + z[45] - n<T>(3,4)*z[32];
    z[41]=z[8]*z[41];
    z[45]=z[2] - z[5];
    z[45]=z[45]*z[26];
    z[46]=npow(z[3],2);
    z[47]=z[3]*z[25];
    z[45]=z[45] - z[46] + z[47];
    z[45]=z[7]*z[45];
    z[47]=npow(z[10],2);
    z[48]=2*z[14];
    z[49]= - z[16] - z[48];
    z[49]=z[49]*z[47];
    z[50]=z[46]*z[5];
    z[24]=z[34] + z[24] + z[45] + z[41] + z[49] - z[50];
    z[24]=z[6]*z[24];
    z[34]=z[8]*z[3];
    z[41]=z[3]*z[2];
    z[45]=z[2] + z[3];
    z[45]=z[5]*z[45];
    z[45]=z[34] - z[41] + z[45];
    z[49]=n<T>(1,2)*z[4];
    z[45]=z[45]*z[49];
    z[51]=n<T>(1,2)*z[5];
    z[52]= - z[51]*z[41];
    z[53]=z[5]*z[9];
    z[54]= - z[3]*z[9];
    z[54]=z[54] + z[53];
    z[54]=n<T>(1,4)*z[54] - z[34];
    z[54]=z[8]*z[54];
    z[55]=z[7]*z[5];
    z[56]= - z[38]*z[55];
    z[45]=z[45] + z[56] + z[52] + z[54];
    z[45]=z[4]*z[45];
    z[52]= - z[9]*z[40];
    z[52]=z[55] - z[52] - z[53];
    z[52]= - n<T>(1,4)*z[52];
    z[54]=npow(z[8],2);
    z[52]=z[54]*z[52];
    z[46]=z[46]*z[35];
    z[56]=z[47]*z[2];
    z[57]=n<T>(1,4)*z[56] + z[46];
    z[57]=z[5]*z[57];
    z[45]=z[45] + z[57] + z[52];
    z[45]=z[4]*z[45];
    z[52]=z[10]*z[2];
    z[57]=z[3]*z[10];
    z[34]=z[34] - z[52] + z[57];
    z[34]=z[34]*z[38];
    z[57]=z[7] - z[2];
    z[57]=z[57]*z[4];
    z[58]=z[7]*z[2];
    z[59]= - z[57] + n<T>(1,4)*z[52] + 9*z[58];
    z[59]=z[4]*z[59];
    z[60]=z[51]*z[3];
    z[61]= - n<T>(3,2)*z[5] - z[35] + n<T>(1,2)*z[3];
    z[61]=z[7]*z[61];
    z[57]=6*z[57] + z[60] + z[61];
    z[57]=z[6]*z[57];
    z[55]= - z[60] - z[55];
    z[55]=z[7]*z[55];
    z[34]=z[57] + z[59] + z[34] + z[55];
    z[34]=z[6]*z[34];
    z[55]=z[47]*z[5];
    z[56]=z[56] - z[55];
    z[57]=npow(z[7],2);
    z[59]= - z[57]*z[43];
    z[61]=3*z[2];
    z[62]= - z[61] - n<T>(1,4)*z[5];
    z[62]=z[7]*z[62];
    z[53]=n<T>(1,4)*z[53] + z[62];
    z[53]=z[4]*z[53];
    z[53]=z[53] + n<T>(1,4)*z[56] + z[59];
    z[53]=z[4]*z[53];
    z[59]= - z[10]*z[40];
    z[59]=z[52] + z[59];
    z[59]=z[8]*z[59];
    z[56]=z[59] - z[56];
    z[56]=z[56]*z[38];
    z[59]=z[7]*z[50];
    z[34]=z[34] + z[53] + z[56] + z[59];
    z[34]=z[6]*z[34];
    z[53]=n<T>(1,4)*z[10];
    z[56]=z[54]*z[3];
    z[59]= - z[56]*z[53];
    z[62]=npow(z[4],2);
    z[58]= - z[62]*z[58];
    z[63]=z[4]*z[35];
    z[60]= - z[60] + z[63];
    z[63]=z[6]*z[7];
    z[60]=z[60]*z[63];
    z[58]=z[60] + z[59] + z[58];
    z[59]=npow(z[6],2);
    z[58]=z[58]*z[59];
    z[60]=z[9]*z[56];
    z[64]=z[4]*z[5]*z[41];
    z[60]=n<T>(1,2)*z[60] + z[64];
    z[60]=z[60]*z[62];
    z[58]=n<T>(1,2)*z[60] + z[58];
    z[58]=z[1]*z[58];
    z[60]= - z[2]*z[55];
    z[62]=z[8]*z[5]*z[52];
    z[60]=z[60] + z[62];
    z[60]=z[60]*z[38];
    z[34]=z[58] + z[34] + z[60] + z[45];
    z[34]=z[1]*z[34];
    z[45]=2*z[5];
    z[58]=z[45]*z[2];
    z[60]=npow(z[2],2);
    z[62]=2*z[60];
    z[21]= - z[21] + 6*z[3];
    z[21]=z[3]*z[21];
    z[21]=z[58] + z[21] + z[62] - n<T>(19,4)*z[52];
    z[21]=z[5]*z[21];
    z[64]=n<T>(1,4)*z[3];
    z[65]= - static_cast<T>(1)+ n<T>(5,4)*z[36];
    z[65]=z[8]*z[65];
    z[65]=z[64] + z[65];
    z[65]=z[8]*z[65];
    z[66]= - z[43] + z[38];
    z[66]=z[7]*z[66];
    z[67]=z[29] + 3;
    z[68]= - z[3]*z[67];
    z[69]=static_cast<T>(1)+ z[36];
    z[69]=z[5]*z[69];
    z[68]=z[68] + z[69];
    z[69]=n<T>(1,2) - z[36];
    z[69]=z[8]*z[69];
    z[68]=n<T>(1,2)*z[68] + z[69];
    z[68]=z[4]*z[68];
    z[65]=z[68] + z[66] + n<T>(1,2)*z[41] + z[65];
    z[65]=z[4]*z[65];
    z[66]=3*z[5];
    z[68]= - z[66] + z[8];
    z[68]=z[68]*z[38];
    z[69]= - z[2]*z[26];
    z[68]=z[68] + z[69];
    z[68]=z[7]*z[68];
    z[21]=z[65] + z[68] + n<T>(15,4)*z[56] - z[46] + z[21];
    z[21]=z[4]*z[21];
    z[46]=z[60] + n<T>(3,4)*z[52];
    z[46]=z[5]*z[46];
    z[52]=n<T>(3,2)*z[2];
    z[56]=z[52] + z[9];
    z[65]=z[10] + z[48] - z[56];
    z[65]=z[5]*z[65];
    z[68]=z[10] - z[9];
    z[68]=z[68]*z[3];
    z[65]= - z[68] + z[65];
    z[65]=z[8]*z[65];
    z[46]=z[46] + z[65];
    z[46]=z[8]*z[46];
    z[65]= - z[57]*z[45];
    z[69]=z[35] - z[5];
    z[63]=z[69]*z[63];
    z[63]=z[65] + z[63];
    z[59]=z[13]*z[63]*z[59];
    z[63]= - z[35]*z[50];
    z[65]=z[54]*z[51];
    z[50]=z[50] + z[65];
    z[50]=z[7]*z[50];
    z[21]=z[34] + z[59] + z[24] + z[21] + z[50] + z[63] + z[46];
    z[21]=z[1]*z[21];
    z[24]= - z[11]*z[16];
    z[24]= - n<T>(3,4)*z[29] + n<T>(1,2) + z[24];
    z[24]=z[3]*z[24];
    z[34]=static_cast<T>(1)+ z[37];
    z[34]=z[34]*z[51];
    z[37]=n<T>(1,2)*z[8];
    z[46]=npow(z[12],2);
    z[50]=z[46]*z[3];
    z[59]=3*z[12] - z[50];
    z[59]=z[8]*z[59];
    z[59]= - static_cast<T>(1)+ z[59];
    z[59]=z[59]*z[37];
    z[63]=z[12] + z[50];
    z[63]=z[63]*z[51];
    z[65]= - z[12] + n<T>(1,2)*z[50];
    z[65]=z[8]*z[65];
    z[63]=z[63] + z[65];
    z[63]=z[4]*z[63];
    z[65]=z[52] + z[16];
    z[69]=4*z[7];
    z[24]=z[63] - z[69] + z[59] + z[34] + z[24] + z[10] + z[65];
    z[24]=z[4]*z[24];
    z[34]=3*z[16];
    z[59]=4*z[15];
    z[63]=n<T>(11,2) - z[29];
    z[63]=z[2]*z[63];
    z[70]= - static_cast<T>(11)- z[27];
    z[70]=z[3]*z[70];
    z[63]=z[70] + z[63] - z[34] + z[59];
    z[63]=z[3]*z[63];
    z[53]=z[2] - z[53];
    z[70]=5*z[36];
    z[71]= - static_cast<T>(6)+ z[70];
    z[71]=z[3]*z[71];
    z[53]=z[45] + 9*z[53] + z[71];
    z[53]=z[5]*z[53];
    z[71]=npow(z[15],2);
    z[72]=2*z[71];
    z[73]=z[15] + z[10];
    z[73]=z[10]*z[73];
    z[73]= - z[72] + z[73];
    z[40]= - z[40] + 11*z[5];
    z[74]=n<T>(3,2) - z[70];
    z[74]=z[8]*z[74];
    z[40]=n<T>(1,2)*z[40] + z[74];
    z[40]=z[40]*z[37];
    z[28]=z[28] - z[26];
    z[74]=z[20] + 2*z[16];
    z[75]= - n<T>(13,2)*z[8] - z[18] + z[28] + z[74];
    z[75]=z[7]*z[75];
    z[24]=z[24] + z[75] + z[40] + z[53] + 2*z[73] + z[63];
    z[24]=z[4]*z[24];
    z[40]=z[26]*z[12];
    z[53]=z[10]*z[12];
    z[63]= - static_cast<T>(5)+ z[30];
    z[63]=z[40] + n<T>(1,2)*z[63] - z[53];
    z[63]=z[4]*z[63];
    z[73]=2*z[12];
    z[75]=z[73]*z[47];
    z[75]=z[75] + 4*z[2];
    z[76]=npow(z[18],2);
    z[77]=z[76]*z[11];
    z[78]=2*z[18];
    z[79]= - z[15] - z[78];
    z[80]=n<T>(105,4) - z[40];
    z[80]=z[7]*z[80];
    z[63]=z[63] + z[80] + z[51] + 4*z[79] + z[77] - z[75];
    z[63]=z[4]*z[63];
    z[79]=z[12]*z[19];
    z[80]=z[79] + n<T>(9,4)*z[36];
    z[81]= - z[73] + z[50];
    z[81]=z[7]*z[81];
    z[81]=z[81] - n<T>(9,2) - z[80];
    z[81]=z[7]*z[81];
    z[82]=z[11]*z[18];
    z[83]=z[46]*z[7];
    z[84]=n<T>(19,4)*z[12] + z[83];
    z[84]=z[7]*z[84];
    z[84]=z[84] + z[30] - n<T>(23,4) - z[82];
    z[84]=z[4]*z[84];
    z[85]=z[50] - z[12];
    z[86]=z[85]*z[51];
    z[87]=z[37]*z[50];
    z[31]=z[87] + z[86] - z[31];
    z[31]=z[6]*z[31];
    z[86]= - n<T>(7,4)*z[36] + n<T>(19,4) + z[79];
    z[86]=z[5]*z[86];
    z[85]=z[85]*z[37];
    z[85]=z[85] + n<T>(7,4) + z[36];
    z[85]=z[8]*z[85];
    z[87]=n<T>(23,4)*z[2];
    z[31]=z[31] + z[84] + z[81] + z[85] - z[87] + z[86];
    z[31]=z[6]*z[31];
    z[81]=static_cast<T>(1)+ 2*z[36];
    z[81]=z[81]*z[26];
    z[84]=n<T>(9,4) + z[36];
    z[84]=z[3]*z[84];
    z[81]=z[81] - z[66] + z[84] - z[19] + z[56];
    z[81]=z[7]*z[81];
    z[84]=n<T>(1,2) + z[70];
    z[84]=z[84]*z[37];
    z[25]=z[84] - z[25] + z[64] + n<T>(9,2)*z[2] - z[10];
    z[25]=z[8]*z[25];
    z[64]=6*z[14];
    z[84]=z[14]*z[53];
    z[84]=z[84] + z[64] - z[15];
    z[84]=z[84]*z[23];
    z[85]=static_cast<T>(4)- z[70];
    z[85]=z[3]*z[85];
    z[85]=z[85] + z[23] - z[19] + n<T>(3,4)*z[9];
    z[85]=z[5]*z[85];
    z[25]=z[31] + z[63] + z[81] + z[25] + z[84] + z[85];
    z[25]=z[6]*z[25];
    z[31]=4*z[71];
    z[63]=npow(z[9],2);
    z[81]=z[31] - z[63];
    z[84]=z[63]*z[11];
    z[85]=z[84] + z[59];
    z[86]=3*z[9];
    z[88]=z[86] - z[85];
    z[88]=z[3]*z[88];
    z[88]=z[88] + z[81];
    z[88]=z[11]*z[88];
    z[88]=n<T>(17,4)*z[9] + z[88];
    z[88]=z[3]*z[88];
    z[89]=z[12]*z[18];
    z[90]=z[89] + z[39];
    z[91]= - static_cast<T>(1)+ z[90];
    z[91]=z[5]*z[91];
    z[92]=2*z[9];
    z[93]= - z[92] + z[84];
    z[93]=z[11]*z[93];
    z[93]=static_cast<T>(3)+ z[93];
    z[93]=z[3]*z[93];
    z[91]=z[93] + z[91];
    z[91]=z[7]*z[91];
    z[93]= - z[16] - z[2];
    z[93]=z[93]*z[23];
    z[94]=4*z[18];
    z[95]=z[76]*z[12];
    z[96]=z[3] + z[94] + z[95];
    z[96]=z[5]*z[96];
    z[97]= - z[37] + z[10] - 6*z[5];
    z[97]=z[8]*z[97];
    z[88]=z[91] + z[97] + z[96] + z[88] + z[63] + z[93];
    z[88]=z[7]*z[88];
    z[91]= - n<T>(7,2)*z[2] + z[8];
    z[91]=z[91]*z[37];
    z[93]=z[52] + z[5];
    z[93]=3*z[93] + z[26];
    z[93]=z[7]*z[93];
    z[96]=6*z[18];
    z[97]= - z[96] + z[9];
    z[97]=z[5]*z[97];
    z[98]= - z[6]*z[35];
    z[91]=z[98] + z[93] + z[97] + z[91];
    z[91]=z[6]*z[91];
    z[93]=24*z[10];
    z[44]= - z[93] - z[48] - z[44];
    z[44]=z[8]*z[44];
    z[44]=z[47] + z[44];
    z[44]=z[8]*z[44];
    z[97]=z[5] + z[2];
    z[97]=z[97]*z[57];
    z[98]=z[76] + z[63];
    z[99]=z[98]*z[5];
    z[100]= - z[16]*z[47];
    z[44]=z[91] - 2*z[97] + z[44] + z[100] + z[99];
    z[44]=z[6]*z[44];
    z[91]=z[8] + z[18];
    z[100]=3*z[7];
    z[101]=z[91]*z[100];
    z[101]=z[101] - z[76] + 3*z[54];
    z[57]=z[101]*z[57];
    z[102]=npow(z[10],3);
    z[103]= - z[102]*z[45];
    z[104]=z[49]*z[54]*z[7];
    z[103]=z[104] + z[103] - z[57];
    z[103]=z[4]*z[103];
    z[104]=z[76] - z[63];
    z[105]= - z[5]*z[104];
    z[106]=z[54]*z[2];
    z[107]=5*z[18];
    z[108]= - z[107] + 6*z[9];
    z[109]=z[108]*z[5];
    z[110]= - z[6]*z[109];
    z[97]=z[110] - z[97] + z[105] - n<T>(1,2)*z[106];
    z[97]=z[6]*z[97];
    z[105]=z[2] - z[14];
    z[110]=z[105]*npow(z[8],3);
    z[111]=2*z[110];
    z[97]=z[111] + z[97];
    z[97]=z[6]*z[97];
    z[112]=z[102]*z[14];
    z[113]=z[112]*z[45];
    z[114]=npow(z[6],3)*z[63]*z[13];
    z[115]=z[114]*z[5];
    z[113]=z[113] - z[115];
    z[97]=z[97] + z[103] + z[113];
    z[97]=z[13]*z[97];
    z[103]=4*z[8];
    z[94]= - z[103] + z[61] + z[20] - z[94];
    z[94]=z[7]*z[94];
    z[94]=z[94] - n<T>(17,4)*z[54] + z[76] + z[62];
    z[94]=z[7]*z[94];
    z[115]=z[5]*z[2];
    z[115]=z[115] + z[60] - z[47];
    z[115]=z[115]*z[45];
    z[65]=z[38] + z[65];
    z[65]=z[7]*z[65];
    z[65]=z[65] + n<T>(9,4)*z[54];
    z[65]=z[4]*z[65];
    z[65]=z[65] + z[115] + z[94];
    z[65]=z[4]*z[65];
    z[94]=z[14]*z[45];
    z[94]= - z[60] + z[94];
    z[94]=z[47]*z[94];
    z[115]=z[10]*z[16];
    z[116]= - z[60] + z[115];
    z[116]=z[10]*z[116];
    z[117]= - z[8]*z[47];
    z[116]=z[116] + z[117];
    z[116]=z[7]*z[116];
    z[44]=z[97] + z[44] + z[65] + z[116] + z[94];
    z[44]=z[13]*z[44];
    z[65]= - z[60]*z[46];
    z[94]=z[16]*z[17];
    z[65]= - z[94] + z[65];
    z[65]=z[10]*z[65];
    z[42]=z[65] + z[34] + z[42];
    z[42]=z[10]*z[42];
    z[65]=z[9]*z[17];
    z[97]= - static_cast<T>(3)- z[65];
    z[97]=z[9]*z[97];
    z[97]=z[35] + z[97] + z[85];
    z[97]=z[3]*z[97];
    z[42]=z[97] + z[42] - z[81];
    z[42]=z[3]*z[42];
    z[97]=z[10]*z[17];
    z[116]=static_cast<T>(1)+ z[97];
    z[115]=z[116]*z[115];
    z[41]= - z[58] + z[41] - 3*z[60] + z[115];
    z[41]=z[5]*z[41];
    z[58]=7*z[9];
    z[115]=z[60]*z[12];
    z[116]= - static_cast<T>(31)- 24*z[97];
    z[116]=z[10]*z[116];
    z[116]=z[116] - z[115] - 12*z[14] + z[58];
    z[116]=z[5]*z[116];
    z[117]=z[45]*z[12];
    z[118]=z[117]*z[105];
    z[52]=z[118] - z[48] + z[52];
    z[52]=z[8]*z[52];
    z[52]=z[52] + z[116] - z[60] + z[68];
    z[52]=z[8]*z[52];
    z[68]=2*z[47];
    z[116]= - z[14] - z[115];
    z[116]=z[116]*z[68];
    z[21]=z[21] + z[44] + z[25] + z[24] + z[88] + z[52] + z[41] + z[116]
    + z[42];
    z[21]=z[1]*z[21];
    z[24]=z[46]*z[5];
    z[25]=z[46]*z[8];
    z[24]=z[24] + z[25];
    z[41]=z[24]*z[6];
    z[42]=n<T>(1,2)*z[12];
    z[44]=z[42] + z[25];
    z[44]=z[44]*z[37];
    z[44]=n<T>(1,2)*z[41] + z[40] + z[44] + z[117] + n<T>(17,4)*z[29] - n<T>(7,2) + 
    z[82];
    z[44]=z[6]*z[44];
    z[52]=z[46]*z[26];
    z[52]= - n<T>(59,4)*z[12] + z[52];
    z[52]=z[7]*z[52];
    z[88]=z[73]*z[10];
    z[116]=z[5]*z[12];
    z[117]=2*z[11];
    z[119]= - z[15]*z[117];
    z[52]=z[52] - n<T>(11,2)*z[116] + z[88] + n<T>(35,4)*z[29] - n<T>(19,4) + z[119]
   ;
    z[52]=z[4]*z[52];
    z[119]=z[36] + n<T>(5,2)*z[116] + n<T>(3,2)*z[39];
    z[120]= - n<T>(1,4) + z[119];
    z[120]=z[8]*z[120];
    z[50]=z[26]*z[50];
    z[50]=z[50] - static_cast<T>(14)- z[80];
    z[50]=z[7]*z[50];
    z[80]= - z[12]*z[14];
    z[80]= - static_cast<T>(6)+ z[80];
    z[80]=z[80]*z[23];
    z[79]= - n<T>(5,2)*z[36] + n<T>(13,2) - z[79];
    z[79]=z[5]*z[79];
    z[121]=8*z[9];
    z[122]=5*z[14];
    z[123]= - 7*z[15] + 3*z[19] - z[122];
    z[124]=static_cast<T>(6)- z[53];
    z[124]=z[3]*z[124];
    z[44]=z[44] + z[52] + z[50] + z[120] + z[79] + z[124] + z[80] - n<T>(15,4)*z[2] - z[77] - z[121] + 2*z[123] + 17*z[18];
    z[44]=z[6]*z[44];
    z[25]=z[42] - z[25];
    z[25]=z[25]*z[37];
    z[37]=z[24]*z[49];
    z[25]=z[37] + n<T>(3,4)*z[33] + z[25] - z[53] + z[116];
    z[25]=z[4]*z[25];
    z[37]= - n<T>(27,4) - z[119];
    z[37]=z[8]*z[37];
    z[42]=z[31] + z[76];
    z[49]=z[11]*z[42];
    z[50]=z[16] - z[59];
    z[50]=z[11]*z[50];
    z[50]=n<T>(31,4)*z[29] - n<T>(15,4) + z[50];
    z[50]=z[3]*z[50];
    z[52]= - static_cast<T>(49)+ 9*z[36];
    z[52]=z[52]*z[51];
    z[77]=n<T>(81,4) - z[40];
    z[77]=z[7]*z[77];
    z[25]=z[25] + z[77] + z[37] + z[52] + z[50] + z[49] - z[96] - z[34]
    + 22*z[15] - z[75];
    z[25]=z[4]*z[25];
    z[34]= - static_cast<T>(23)+ 9*z[39];
    z[34]=z[34]*z[38];
    z[37]= - z[22] + z[18];
    z[37]=2*z[37] - n<T>(9,4)*z[2];
    z[27]=n<T>(13,2) + z[27];
    z[27]=z[6]*z[27];
    z[27]=z[27] + z[69] + z[34] + 3*z[37] + 10*z[5];
    z[27]=z[6]*z[27];
    z[34]=z[86] + z[16];
    z[37]=8*z[15];
    z[49]= - z[37] + z[34];
    z[49]=z[10]*z[49];
    z[50]=3*z[10] - z[96] - z[9];
    z[50]=z[50]*z[45];
    z[52]=n<T>(55,2)*z[8] + 26*z[10] + z[64] - z[87];
    z[52]=z[8]*z[52];
    z[56]=z[66] + z[56];
    z[56]=z[7]*z[56];
    z[75]=12*z[71];
    z[27]=z[27] + z[56] + z[52] + z[50] + z[49] - 4*z[63] + z[75] - 
    z[76];
    z[27]=z[6]*z[27];
    z[49]= - z[51] - 11*z[2] + z[59] + z[108];
    z[49]=z[6]*z[49];
    z[50]=4*z[9];
    z[51]=z[18] - z[50];
    z[51]=z[51]*z[66];
    z[52]=23*z[2] - 9*z[8];
    z[38]=z[52]*z[38];
    z[52]= - z[61] + z[5];
    z[52]=z[52]*z[26];
    z[38]=z[49] + z[52] + z[38] + z[51] + z[104];
    z[38]=z[6]*z[38];
    z[49]= - z[76] + 3*z[63];
    z[49]=z[5]*z[49];
    z[51]=4*z[14];
    z[52]= - z[93] + z[51] - n<T>(37,4)*z[2];
    z[52]=z[52]*z[54];
    z[38]=z[38] + z[49] + z[52];
    z[38]=z[6]*z[38];
    z[49]=z[23]*z[15];
    z[49]=z[49] - z[71];
    z[52]=z[49]*z[47];
    z[56]= - z[102]*z[66];
    z[52]=z[52] + z[56];
    z[52]=2*z[52] - z[57];
    z[56]=z[54]*z[4];
    z[77]=z[7]*z[56];
    z[52]=2*z[52] - n<T>(7,4)*z[77];
    z[52]=z[4]*z[52];
    z[77]= - z[76]*z[45];
    z[79]= - z[108]*z[45];
    z[79]=z[63] + z[79];
    z[79]=z[6]*z[79];
    z[77]=z[79] + z[77] + n<T>(9,4)*z[106];
    z[77]=z[6]*z[77];
    z[77]=4*z[110] + z[77];
    z[77]=z[6]*z[77];
    z[79]=z[102]*z[5];
    z[80]=z[51]*z[79];
    z[86]= - z[45]*z[114];
    z[52]=z[86] + z[77] + z[80] + z[52];
    z[52]=z[13]*z[52];
    z[77]= - z[10]*z[15];
    z[77]=12*z[54] + z[72] + z[77];
    z[77]=z[10]*z[77];
    z[77]=z[55] + z[77];
    z[80]=z[7]*z[101];
    z[77]=2*z[77] + z[80];
    z[77]=z[7]*z[77];
    z[80]=z[107] + z[8];
    z[80]=z[7]*z[80];
    z[80]=z[80] - z[76] - n<T>(127,4)*z[54];
    z[80]=z[7]*z[80];
    z[86]=4*z[10];
    z[87]=29*z[15] + z[86];
    z[87]=z[10]*z[87];
    z[87]= - z[75] + z[87];
    z[87]=z[10]*z[87];
    z[101]=z[7]*z[8];
    z[102]=z[54] - z[101];
    z[102]=z[4]*z[102];
    z[80]=n<T>(1,4)*z[102] + z[80] + z[87] - 49*z[55];
    z[80]=z[4]*z[80];
    z[87]= - z[14]*z[55];
    z[87]=z[110] - z[112] + z[87];
    z[38]=z[52] + z[38] + z[80] + 4*z[87] + z[77];
    z[38]=z[13]*z[38];
    z[52]=7*z[2];
    z[34]=z[52] - 11*z[15] - z[34];
    z[34]=z[10]*z[34];
    z[69]=z[91]*z[69];
    z[77]=2*z[63];
    z[80]= - z[23] + n<T>(9,2)*z[8];
    z[80]=z[8]*z[80];
    z[34]=z[69] + z[80] + 15*z[32] + z[34] + z[77] - z[42];
    z[34]=z[7]*z[34];
    z[42]=8*z[71];
    z[69]= - z[42] - z[76];
    z[28]=n<T>(27,4)*z[8] + 10*z[18] + z[20] + z[16] - z[28];
    z[28]=z[7]*z[28];
    z[80]= - static_cast<T>(3)- n<T>(7,4)*z[39];
    z[80]=z[8]*z[80];
    z[80]=z[80] + z[26];
    z[80]=z[4]*z[80];
    z[87]=35*z[15] - z[86];
    z[87]=z[10]*z[87];
    z[91]=z[45] + n<T>(21,2)*z[2] - 55*z[10];
    z[91]=z[5]*z[91];
    z[28]=z[80] + z[28] - n<T>(57,2)*z[54] + z[91] + 2*z[69] + z[87];
    z[28]=z[4]*z[28];
    z[69]=2*z[105];
    z[80]= - z[69] + z[115];
    z[80]=z[80]*z[23];
    z[80]=z[60] + z[80];
    z[80]=z[10]*z[80];
    z[87]=3*z[14];
    z[91]= - z[10]*z[87];
    z[91]=z[91] + z[98];
    z[45]=z[91]*z[45];
    z[91]=n<T>(15,2)*z[2];
    z[102]=z[51] - z[91];
    z[102]=z[102]*z[54];
    z[27]=z[38] + z[27] + z[28] + z[34] + z[102] + z[80] + z[45];
    z[27]=z[13]*z[27];
    z[28]=3*z[115];
    z[34]=5*z[2];
    z[38]= - z[34] + z[28];
    z[38]=z[12]*z[38];
    z[45]=z[73]*z[60];
    z[60]=z[45] + z[2];
    z[46]=z[60]*z[46];
    z[46]=z[17] + z[46];
    z[46]=z[10]*z[46];
    z[38]=z[46] + z[38] - static_cast<T>(3)- z[94];
    z[38]=z[10]*z[38];
    z[46]=static_cast<T>(2)+ z[65];
    z[46]=z[9]*z[46];
    z[46]=z[46] - z[85];
    z[46]=z[11]*z[46];
    z[73]=3*z[29];
    z[46]=z[46] + z[73];
    z[80]=2*z[3];
    z[46]=z[46]*z[80];
    z[102]= - static_cast<T>(3)+ z[65];
    z[102]=z[9]*z[102];
    z[106]=z[11]*z[42];
    z[112]= - static_cast<T>(3)+ z[29];
    z[112]=z[2]*z[112];
    z[38]=z[46] + z[38] + z[112] + z[106] + z[102] - z[78] + z[37] + 
    z[74];
    z[38]=z[3]*z[38];
    z[46]=z[84] - z[9];
    z[74]=z[46]*z[117];
    z[46]= - z[11]*z[46];
    z[46]= - static_cast<T>(1)+ z[46];
    z[46]=z[11]*z[46];
    z[46]=z[46] + z[12];
    z[46]=z[46]*z[80];
    z[46]=z[46] - static_cast<T>(1)+ z[74] - z[90];
    z[46]=z[7]*z[46];
    z[74]= - z[11]*z[31];
    z[74]=z[74] - 15*z[15] + z[9];
    z[74]=z[74]*z[117];
    z[22]=z[22] - z[9];
    z[22]=2*z[22] + z[84];
    z[22]=z[22]*z[117];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[3]*z[11]*z[22];
    z[22]=z[22] + n<T>(3,4) + z[74];
    z[22]=z[3]*z[22];
    z[74]=20*z[71];
    z[80]=z[74] - z[63];
    z[80]=z[11]*z[80];
    z[70]=static_cast<T>(13)- z[70];
    z[70]=z[5]*z[70];
    z[22]=z[46] + z[70] + z[22] + z[10] - z[95] - z[61] + z[80] - z[121]
    - 12*z[18] - 3*z[20] + z[37] + n<T>(7,2)*z[8];
    z[22]=z[7]*z[22];
    z[37]= - z[39]*z[69];
    z[37]=z[37] - z[118] + z[93] + z[115] + z[51] + z[91];
    z[37]=z[8]*z[37];
    z[46]=z[122] - z[78];
    z[69]=23*z[97];
    z[70]=z[69] + static_cast<T>(29)+ z[94];
    z[70]=z[10]*z[70];
    z[35]=z[36]*z[35];
    z[35]=z[35] + z[70] - z[95] + z[61] + 2*z[46] - 5*z[9];
    z[35]=z[5]*z[35];
    z[36]=4*z[115];
    z[46]=z[36] + z[48] + z[2];
    z[46]=z[46]*z[53];
    z[46]=z[46] + z[28] + z[51] - 13*z[2];
    z[46]=z[10]*z[46];
    z[51]=z[31] + z[63];
    z[21]=z[21] + z[27] + z[44] + z[25] + z[22] + z[37] + z[35] + z[38]
    + z[46] - z[51];
    z[21]=z[1]*z[21];
    z[22]=z[49]*z[68];
    z[22]=z[22] - 5*z[79];
    z[25]= - z[26]*z[56];
    z[22]=z[25] + 2*z[22] - z[57];
    z[22]=z[4]*z[22];
    z[25]=z[54]*z[61];
    z[27]=z[63] - z[109];
    z[27]=z[6]*z[27];
    z[25]=z[27] - z[99] + z[25];
    z[25]=z[6]*z[25];
    z[25]=z[111] + z[25];
    z[25]=z[6]*z[25];
    z[22]=z[25] + z[22] + z[113];
    z[22]=z[13]*z[22];
    z[25]=2*z[76];
    z[27]=9*z[18];
    z[35]=z[27] + 5*z[8];
    z[35]=z[7]*z[35];
    z[35]=z[35] - z[25] - 25*z[54];
    z[35]=z[7]*z[35];
    z[37]=23*z[15] + z[23];
    z[37]=z[10]*z[37];
    z[37]= - z[42] + z[37];
    z[37]=z[10]*z[37];
    z[38]=2*z[54];
    z[44]= - z[38] + n<T>(7,2)*z[101];
    z[44]=z[4]*z[44];
    z[35]=z[44] + z[35] + z[37] - 45*z[55];
    z[35]=z[4]*z[35];
    z[37]= - z[34] + z[85] + z[108];
    z[37]=z[6]*z[37];
    z[44]=13*z[9];
    z[27]=z[27] - z[44];
    z[27]=z[5]*z[27];
    z[46]= - 3*z[8] + n<T>(1,2)*z[2];
    z[46]=z[8]*z[46];
    z[27]=z[37] + z[46] + z[27] + z[98];
    z[27]=z[6]*z[27];
    z[25]= - z[25] + z[63];
    z[25]=z[5]*z[25];
    z[37]=z[64] - z[34];
    z[37]=z[37]*z[54];
    z[25]=z[27] + z[25] + z[37];
    z[25]=z[6]*z[25];
    z[27]= - z[14] - z[59];
    z[27]=z[10]*z[27];
    z[27]=z[72] + z[27];
    z[27]=z[27]*z[47];
    z[37]= - z[87] + z[86];
    z[37]=z[37]*z[55];
    z[27]=z[110] + z[27] + z[37];
    z[22]=z[22] + z[25] + z[35] + 2*z[27] + z[57];
    z[22]=z[13]*z[22];
    z[25]=z[31] - z[76];
    z[27]=10*z[15];
    z[35]=static_cast<T>(8)- z[53];
    z[35]=z[10]*z[35];
    z[35]=z[27] + z[35];
    z[35]=z[35]*z[23];
    z[37]=z[100] - 7*z[18] + 24*z[8];
    z[37]=z[7]*z[37];
    z[46]=n<T>(3,2) - 2*z[39];
    z[46]=z[8]*z[46];
    z[46]=z[46] - n<T>(3,2)*z[7];
    z[46]=z[4]*z[46];
    z[32]=z[46] + z[37] - 24*z[54] - 60*z[32] + z[35] - z[25];
    z[32]=z[4]*z[32];
    z[35]=z[50] - z[59] - z[107];
    z[35]=z[11]*z[35];
    z[35]=z[35] + 5*z[29];
    z[35]=z[6]*z[35];
    z[37]=z[76] + z[77];
    z[37]=z[11]*z[37];
    z[46]=z[59] + 3*z[18];
    z[47]=3*z[39];
    z[49]= - n<T>(1,2) + z[47];
    z[49]=z[8]*z[49];
    z[35]=z[35] + z[49] + n<T>(9,2)*z[5] - z[43] + z[37] - 3*z[46] + 14*z[9]
   ;
    z[35]=z[6]*z[35];
    z[37]= - 3*z[15] + z[2];
    z[37]=z[10]*z[37];
    z[37]=z[37] + z[75] + z[104];
    z[49]=z[18] - z[92];
    z[49]=3*z[49] + z[86];
    z[49]=z[5]*z[49];
    z[50]=z[103] - z[14] + z[61];
    z[50]=z[8]*z[50];
    z[35]=z[35] + 6*z[50] + 2*z[37] + z[49];
    z[35]=z[6]*z[35];
    z[37]=z[90]*z[100];
    z[49]=8*z[18];
    z[47]= - static_cast<T>(5)+ z[47];
    z[47]=z[8]*z[47];
    z[37]=z[37] + z[47] - z[49] - z[95];
    z[37]=z[7]*z[37];
    z[42]= - z[42] + z[76];
    z[37]=z[37] + 2*z[42] + z[63];
    z[37]=z[7]*z[37];
    z[42]= - static_cast<T>(4)- z[39];
    z[38]=z[38]*z[105]*z[42];
    z[42]=z[88] + 8;
    z[42]=z[14]*z[42];
    z[42]= - 5*z[15] + z[42];
    z[42]=z[10]*z[42];
    z[31]= - z[31] + z[42];
    z[31]=z[10]*z[31];
    z[42]=z[64] + 35*z[10];
    z[42]=z[10]*z[42];
    z[42]=z[42] - z[104];
    z[42]=z[5]*z[42];
    z[22]=z[22] + z[35] + z[32] + z[37] + z[38] + z[31] + z[42];
    z[22]=z[13]*z[22];
    z[31]=9*z[9];
    z[32]=z[15] - z[49];
    z[32]=2*z[32] + z[31];
    z[32]=z[11]*z[32];
    z[32]=z[39] - n<T>(3,2)*z[116] + n<T>(7,2)*z[29] - static_cast<T>(4)+ z[32];
    z[32]=z[6]*z[32];
    z[25]= - 3*z[25] - z[63];
    z[25]=z[11]*z[25];
    z[35]= - z[18] + z[14] - 25*z[15];
    z[37]= - static_cast<T>(11)- z[39];
    z[37]=z[8]*z[37];
    z[25]=z[32] + z[26] + z[37] + n<T>(11,2)*z[5] + z[91] + z[25] + 2*z[35]
    + z[44];
    z[25]=z[6]*z[25];
    z[26]=npow(z[11],2);
    z[32]=z[26]*z[63];
    z[35]= - 8*z[39] - 10*z[89] - static_cast<T>(2)+ z[32];
    z[35]=z[7]*z[35];
    z[37]=z[11]*z[71];
    z[38]=static_cast<T>(11)- z[39];
    z[38]=z[8]*z[38];
    z[35]=z[35] + z[38] + 3*z[95] + 32*z[37] - z[58] + 41*z[15] + z[96];
    z[35]=z[7]*z[35];
    z[37]= - z[87] + z[43];
    z[37]=2*z[37] - z[115];
    z[37]=z[12]*z[37];
    z[37]= - static_cast<T>(26)+ z[37];
    z[37]=z[10]*z[37];
    z[38]= - 7*z[14] + z[59];
    z[37]=z[37] - z[115] + 2*z[38] + z[52];
    z[37]=z[10]*z[37];
    z[38]=static_cast<T>(4)+ 3*z[53];
    z[23]=z[38]*z[23];
    z[33]=z[39] - z[33];
    z[33]=z[4]*z[33];
    z[38]=static_cast<T>(9)+ z[39];
    z[38]=z[8]*z[38];
    z[42]= - n<T>(45,2) + z[40];
    z[42]=z[7]*z[42];
    z[23]=z[33] + z[42] + z[38] - n<T>(63,2)*z[5] + z[23] + 6*z[15] + z[18];
    z[23]=z[4]*z[23];
    z[33]=z[65] + 1;
    z[33]=z[33]*z[9];
    z[38]=z[33] - z[18];
    z[42]=static_cast<T>(10)- z[69];
    z[42]=z[10]*z[42];
    z[42]=z[42] - z[48] + z[38];
    z[42]=z[5]*z[42];
    z[43]=z[52] - z[64];
    z[43]=z[43]*z[39];
    z[44]=z[43] - 14*z[14] + z[34];
    z[44]=z[8]*z[44];
    z[22]=z[22] + z[25] + z[23] + z[35] + z[44] + z[42] - z[63] + z[37];
    z[22]=z[13]*z[22];
    z[23]=z[51]*z[11];
    z[25]=static_cast<T>(2)- z[65];
    z[25]=z[9]*z[25];
    z[25]= - z[23] - 2*z[46] + z[25];
    z[25]=z[11]*z[25];
    z[28]=z[52] - z[28];
    z[28]=z[12]*z[28];
    z[28]= - static_cast<T>(5)+ z[28];
    z[28]=z[12]*z[28];
    z[35]= - z[2] - z[115];
    z[35]=z[12]*z[35];
    z[35]=static_cast<T>(1)+ z[35];
    z[35]=z[12]*z[35];
    z[35]=z[17] + z[35];
    z[35]=z[35]*z[53];
    z[28]=z[35] + z[17] + z[28];
    z[28]=z[10]*z[28];
    z[33]= - z[33] + z[85];
    z[33]=z[33]*z[26];
    z[29]= - z[82] + 2*z[29];
    z[29]=z[12]*z[29];
    z[29]=z[33] + z[29];
    z[29]=z[3]*z[29];
    z[33]=z[2]*z[67];
    z[33]=z[33] - z[45];
    z[33]=z[12]*z[33];
    z[25]=z[29] + z[28] + z[33] - z[73] - static_cast<T>(8)+ z[25];
    z[25]=z[3]*z[25];
    z[27]=z[27] - z[9];
    z[23]=3*z[27] + z[23];
    z[23]=z[11]*z[23];
    z[23]=static_cast<T>(24)+ z[23];
    z[23]=z[11]*z[23];
    z[27]=z[9] - z[85];
    z[27]=z[11]*z[27];
    z[27]= - static_cast<T>(3)+ z[27];
    z[26]=z[27]*z[26];
    z[27]= - z[12]*z[11];
    z[26]=z[26] + z[27];
    z[26]=z[3]*z[26];
    z[23]=z[26] + z[23] + 5*z[12];
    z[23]=z[3]*z[23];
    z[26]= - z[74] - z[63];
    z[26]=z[11]*z[26];
    z[26]=z[26] - 65*z[15] + z[31];
    z[26]=z[11]*z[26];
    z[27]=static_cast<T>(1)- z[32];
    z[27]=z[27]*z[117];
    z[28]=z[3]*z[63]*npow(z[11],4);
    z[27]=z[27] + z[28];
    z[27]=z[7]*z[27];
    z[23]=z[27] + z[39] + z[23] + 9*z[89] - n<T>(67,2) + z[26];
    z[23]=z[7]*z[23];
    z[24]=z[4]*z[24];
    z[26]=4*z[39];
    z[27]= - static_cast<T>(2)- z[53];
    z[28]= - n<T>(25,2)*z[12] + z[83];
    z[28]=z[7]*z[28];
    z[24]=z[24] + z[28] + z[26] + 2*z[27] + n<T>(27,2)*z[116];
    z[24]=z[4]*z[24];
    z[27]=z[15] - z[18];
    z[27]=6*z[27] + z[9];
    z[27]=z[11]*z[27];
    z[27]= - z[30] + n<T>(15,2) + z[27];
    z[26]= - z[41] + z[40] - z[26] + 3*z[27] - n<T>(23,2)*z[116];
    z[26]=z[6]*z[26];
    z[27]=z[48] + z[34];
    z[27]=3*z[27] - z[36];
    z[27]=z[12]*z[27];
    z[28]= - z[12]*z[60];
    z[28]= - static_cast<T>(26)+ z[28];
    z[28]=z[12]*z[28];
    z[28]=23*z[17] + z[28];
    z[28]=z[10]*z[28];
    z[27]=z[28] - static_cast<T>(11)+ z[27];
    z[27]=z[10]*z[27];
    z[28]=3*z[76];
    z[29]=z[28] + z[81];
    z[29]=z[11]*z[29];
    z[28]= - z[28] - z[62];
    z[28]=z[12]*z[28];
    z[21]=z[21] + z[22] + z[26] + z[24] + z[23] - z[43] + z[66] + z[25]
    + z[27] + z[28] + 12*z[2] + z[29] + z[15] + z[20] + 24*z[14] - 
    z[38];

    r += z[21]*z[1];
 
    return r;
}

template double qqb_2lNLC_r550(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r550(const std::array<dd_real,31>&);
#endif
