#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r989(const std::array<T,31>& k) {
  T z[58];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[3];
    z[11]=k[15];
    z[12]=k[9];
    z[13]=k[2];
    z[14]=npow(z[9],2);
    z[15]=2*z[14];
    z[16]=z[15]*z[2];
    z[17]=z[9] + z[8];
    z[18]=3*z[17] + z[16];
    z[19]=3*z[8];
    z[20]=2*z[9];
    z[21]= - z[19] - z[20];
    z[21]=z[21]*z[20];
    z[22]=npow(z[9],3);
    z[23]= - z[2]*z[22];
    z[21]=z[21] + z[23];
    z[21]=z[4]*z[21];
    z[18]=2*z[18] + z[21];
    z[18]=z[4]*z[18];
    z[21]=z[3]*z[8];
    z[23]=z[21] + 4;
    z[24]=z[2]*z[9];
    z[25]=z[23] + 6*z[24];
    z[18]=z[18] - z[25];
    z[18]=z[4]*z[18];
    z[26]=z[15]*z[7];
    z[27]= - z[19] - z[26];
    z[19]= - z[19] - z[9];
    z[19]=z[19]*z[20];
    z[28]= - z[7]*z[22];
    z[19]=z[19] + z[28];
    z[19]=z[6]*z[19];
    z[19]=2*z[27] + z[19];
    z[19]=z[6]*z[19];
    z[27]=z[21] - 3;
    z[28]=z[7]*z[9];
    z[29]=z[27] + 6*z[28];
    z[19]=z[19] - z[29];
    z[19]=z[6]*z[19];
    z[27]=z[7]*z[27];
    z[23]=z[2]*z[23];
    z[18]=z[19] + z[18] + z[27] + z[23];
    z[18]=z[5]*z[18];
    z[19]=z[14]*z[2];
    z[23]=3*z[9];
    z[19]=z[19] + z[23];
    z[27]=2*z[4];
    z[30]=z[19]*z[27];
    z[31]=z[24] + 1;
    z[30]= - 15*z[31] + z[30];
    z[30]=z[4]*z[30];
    z[32]=3*z[2];
    z[33]=z[32] + z[3];
    z[30]=8*z[33] + z[30];
    z[30]=z[4]*z[30];
    z[34]= - static_cast<T>(3)+ 5*z[28];
    z[35]=z[6]*z[26];
    z[34]=3*z[34] + z[35];
    z[34]=z[6]*z[34];
    z[34]=z[34] - 7*z[3] + 24*z[7];
    z[34]=z[6]*z[34];
    z[35]=z[2]*z[3];
    z[36]=z[7]*z[3];
    z[18]=z[18] + z[34] + z[30] + 7*z[36] - 8*z[35];
    z[30]=2*z[5];
    z[18]=z[18]*z[30];
    z[34]=18*z[3];
    z[37]= - z[2]*z[31];
    z[37]= - z[34] + z[37];
    z[37]=z[2]*z[37];
    z[38]=z[21] + 2;
    z[39]= - z[24] + z[38];
    z[39]=z[4]*z[39];
    z[39]=z[39] + z[3] + 16*z[2];
    z[39]=z[4]*z[39];
    z[37]=z[37] + z[39];
    z[37]=z[4]*z[37];
    z[39]=static_cast<T>(1)- z[28];
    z[39]=z[7]*z[39];
    z[34]= - z[34] + z[39];
    z[34]=z[7]*z[34];
    z[39]=z[28] + 1;
    z[40]=z[21] - z[39];
    z[40]=z[6]*z[40];
    z[40]=z[40] + z[3] - 16*z[7];
    z[40]=z[6]*z[40];
    z[34]=z[34] + z[40];
    z[34]=z[6]*z[34];
    z[40]=npow(z[7],2);
    z[41]=z[21] - 1;
    z[42]=z[7]*z[41];
    z[42]= - z[3] + z[42];
    z[42]=z[42]*z[40];
    z[43]=npow(z[2],2);
    z[44]=z[2]*z[38];
    z[44]= - z[3] + z[44];
    z[44]=z[44]*z[43];
    z[18]=z[18] + z[34] + z[37] + z[42] + z[44];
    z[18]=z[5]*z[18];
    z[34]=2*z[8];
    z[37]=z[34] + z[23];
    z[16]= - z[16] - z[37];
    z[16]=z[4]*z[16];
    z[16]=z[16] + z[25];
    z[16]=z[16]*z[27];
    z[42]=4*z[2];
    z[44]=z[42] + z[3];
    z[45]=3*z[44];
    z[16]= - z[45] + z[16];
    z[16]=z[4]*z[16];
    z[46]=z[34] - z[9];
    z[26]= - z[26] - z[46];
    z[26]=z[6]*z[26];
    z[26]=z[26] - z[29];
    z[47]=2*z[6];
    z[26]=z[26]*z[47];
    z[48]= - z[3] + 4*z[7];
    z[49]=3*z[48];
    z[26]= - z[49] + z[26];
    z[26]=z[6]*z[26];
    z[50]= - z[36] + z[35];
    z[16]=z[26] + 3*z[50] + z[16];
    z[16]=z[5]*z[16];
    z[26]=z[4]*z[31];
    z[26]= - 7*z[33] + 6*z[26];
    z[26]=z[4]*z[26];
    z[26]=15*z[35] + z[26];
    z[26]=z[4]*z[26];
    z[31]=3*z[28];
    z[50]=z[31] - 1;
    z[47]=z[50]*z[47];
    z[47]=z[47] - 5*z[3] + 21*z[7];
    z[47]=z[6]*z[47];
    z[47]=15*z[36] + z[47];
    z[47]=z[6]*z[47];
    z[16]=z[16] + z[26] + z[47];
    z[16]=z[16]*z[30];
    z[26]=2*z[2];
    z[47]=z[3] - z[26];
    z[47]=z[4]*z[47];
    z[47]=10*z[35] + z[47];
    z[47]=z[4]*z[47];
    z[51]=npow(z[2],3);
    z[47]= - 2*z[51] + z[47];
    z[47]=z[4]*z[47];
    z[52]=2*z[7];
    z[53]=z[52] + z[3];
    z[54]= - z[6]*z[53];
    z[54]= - 10*z[36] + z[54];
    z[54]=z[6]*z[54];
    z[55]=npow(z[7],3);
    z[54]= - 2*z[55] + z[54];
    z[54]=z[6]*z[54];
    z[56]=z[55]*z[3];
    z[57]=z[51]*z[3];
    z[16]=z[16] + z[54] + z[47] - z[56] + z[57];
    z[16]=z[5]*z[16];
    z[25]= - z[4]*z[25];
    z[25]=z[45] + z[25];
    z[25]=z[4]*z[25];
    z[25]= - 6*z[35] + z[25];
    z[25]=z[4]*z[25];
    z[29]= - z[6]*z[29];
    z[29]= - z[49] + z[29];
    z[29]=z[6]*z[29];
    z[29]= - 6*z[36] + z[29];
    z[29]=z[6]*z[29];
    z[25]=z[25] + z[29];
    z[25]=z[5]*z[25];
    z[29]=z[33]*z[27];
    z[29]= - 9*z[35] + z[29];
    z[33]=npow(z[4],2);
    z[29]=z[29]*z[33];
    z[45]= - z[3] + 6*z[7];
    z[45]=z[6]*z[45];
    z[45]=9*z[36] + z[45];
    z[47]=npow(z[6],2);
    z[45]=z[45]*z[47];
    z[25]=z[25] + z[29] + z[45];
    z[25]=z[25]*z[30];
    z[29]= - z[33]*z[35];
    z[29]= - z[57] + z[29];
    z[29]=z[4]*z[29];
    z[45]= - z[47]*z[36];
    z[45]= - z[56] + z[45];
    z[45]=z[6]*z[45];
    z[25]=z[25] + z[29] + z[45];
    z[25]=z[5]*z[25];
    z[29]=z[47] + z[33];
    z[45]=z[2]*z[7];
    z[29]=z[45]*z[29];
    z[49]=z[43]*z[7];
    z[49]=z[49] + z[55];
    z[49]=z[49]*z[2];
    z[29]= - z[49] + z[29];
    z[29]=z[6]*z[4]*z[29];
    z[44]= - z[4]*z[44];
    z[44]=4*z[35] + z[44];
    z[44]=z[44]*z[33];
    z[48]= - z[6]*z[48];
    z[48]= - 4*z[36] + z[48];
    z[48]=z[48]*z[47];
    z[44]=z[44] + z[48];
    z[44]=z[5]*z[44];
    z[48]=z[36]*npow(z[6],3);
    z[54]=z[35]*npow(z[4],3);
    z[48]=z[48] + z[54];
    z[44]=2*z[48] + z[44];
    z[44]=z[44]*npow(z[5],2);
    z[48]= - z[1]*z[48]*npow(z[5],3);
    z[44]=z[44] + z[48];
    z[44]=z[1]*z[44];
    z[25]=2*z[44] + z[29] + z[25];
    z[25]=z[1]*z[25];
    z[29]=z[2] - z[7];
    z[44]= - z[29]*z[33];
    z[44]=z[44] - z[55] + z[51];
    z[44]=z[4]*z[44];
    z[36]=z[45] + z[36];
    z[48]= - z[4]*z[29];
    z[48]=z[48] - z[36];
    z[47]=z[48]*z[47];
    z[44]=z[47] + z[44] + z[56] + z[49];
    z[44]=z[6]*z[44];
    z[33]=z[33] - z[43];
    z[47]=z[7] - z[3];
    z[33]=z[47]*z[33];
    z[33]= - z[55] + z[33];
    z[47]=z[4]*z[2];
    z[33]=z[33]*z[47];
    z[16]=z[25] + z[16] + z[33] + z[44];
    z[16]=z[1]*z[16];
    z[25]=z[2]*z[10];
    z[33]= - static_cast<T>(1)- z[25];
    z[33]=z[33]*z[43];
    z[44]=z[7]*z[8];
    z[44]=z[44] - 1;
    z[48]=z[44] + z[25];
    z[49]=z[48]*z[4];
    z[29]=z[49] - z[29];
    z[29]=z[4]*z[29];
    z[51]= - z[8]*z[55];
    z[29]=z[29] + z[51] + z[33];
    z[29]=z[4]*z[29];
    z[33]=z[21] - 2;
    z[33]=z[33]*z[7];
    z[51]=z[2]*z[39];
    z[49]=z[49] + z[33] + z[51];
    z[49]=z[6]*z[49];
    z[36]=z[49] + z[47] + z[36];
    z[36]=z[6]*z[36];
    z[47]=z[3] - z[33];
    z[47]=z[47]*z[40];
    z[49]= - z[39]*z[43];
    z[51]=z[55]*z[9];
    z[49]= - z[51] + z[49];
    z[49]=z[2]*z[49];
    z[29]=z[36] + z[29] + z[47] + z[49];
    z[29]=z[6]*z[29];
    z[36]=z[28] + 2;
    z[47]=z[3]*z[10];
    z[49]=z[36] + z[47];
    z[43]=z[49]*z[43];
    z[43]=z[51] + z[43];
    z[43]=z[2]*z[43];
    z[49]= - z[2]*z[49];
    z[51]=2*z[3];
    z[49]=z[49] - z[51] + z[7];
    z[49]=z[4]*z[49];
    z[45]=z[45] + z[49];
    z[45]=z[4]*z[45];
    z[43]=z[45] - z[55] + z[43];
    z[43]=z[4]*z[43];
    z[16]=z[16] + z[18] + z[29] - z[57] + z[43];
    z[16]=z[1]*z[16];
    z[18]=z[7]*z[21];
    z[29]=z[4]*z[44];
    z[43]=z[9] + z[10];
    z[43]=z[43]*z[2];
    z[44]=static_cast<T>(1)- z[43];
    z[44]=z[6]*z[44];
    z[18]=z[44] + z[29] + z[18] - z[26];
    z[18]=z[6]*z[18];
    z[26]=3*z[7];
    z[25]= - z[4]*z[25];
    z[25]=z[25] - z[26] + z[42];
    z[25]=z[4]*z[25];
    z[29]=3*z[3];
    z[42]=z[29] - z[52];
    z[42]=z[7]*z[42];
    z[44]= - static_cast<T>(3)+ z[28];
    z[44]=z[7]*z[44];
    z[43]=static_cast<T>(1)+ z[43];
    z[43]=z[2]*z[43];
    z[43]=z[44] + z[43];
    z[43]=z[2]*z[43];
    z[18]=z[18] + z[25] + z[42] + z[43];
    z[18]=z[6]*z[18];
    z[25]=4*z[8];
    z[42]= - z[25] - z[23];
    z[42]=z[6]*z[42]*z[14];
    z[43]=z[17]*z[9];
    z[42]= - 6*z[43] + z[42];
    z[42]=z[6]*z[42];
    z[42]=z[42] - z[37];
    z[42]=z[6]*z[42];
    z[43]=z[14]*z[4];
    z[25]= - z[25] - z[9];
    z[25]=z[25]*z[43];
    z[44]=z[9]*z[8];
    z[25]=6*z[44] + z[25];
    z[25]=z[4]*z[25];
    z[25]=z[25] - z[46];
    z[25]=z[4]*z[25];
    z[25]=z[25] + z[42];
    z[25]=z[5]*z[25];
    z[42]=z[21] + 3;
    z[45]= - z[2]*z[42];
    z[33]= - z[33] + z[45];
    z[45]=z[4]*z[15];
    z[19]= - 3*z[19] + z[45];
    z[19]=z[4]*z[19];
    z[19]=z[19] + static_cast<T>(10)+ 9*z[24];
    z[19]=z[4]*z[19];
    z[45]=z[7]*z[14];
    z[45]= - z[9] + z[45];
    z[15]=z[6]*z[15];
    z[15]=3*z[45] + z[15];
    z[15]=z[6]*z[15];
    z[15]=z[15] - static_cast<T>(8)+ 9*z[28];
    z[15]=z[6]*z[15];
    z[15]=z[25] + z[15] + 3*z[33] + z[19];
    z[15]=z[15]*z[30];
    z[19]=3*z[24];
    z[25]=static_cast<T>(1)+ z[19];
    z[33]=z[34] + z[9];
    z[45]=z[4]*z[33];
    z[25]=2*z[25] + z[45];
    z[25]=z[4]*z[25];
    z[45]= - static_cast<T>(17)- z[24];
    z[45]=z[2]*z[45];
    z[25]=z[25] + z[45] + 3*z[11] - z[3];
    z[25]=z[4]*z[25];
    z[33]=z[6]*z[33];
    z[33]= - 2*z[50] + z[33];
    z[33]=z[6]*z[33];
    z[45]= - static_cast<T>(17)+ z[28];
    z[45]=z[7]*z[45];
    z[33]=z[33] + z[45] - 3*z[12] + z[3];
    z[33]=z[6]*z[33];
    z[45]= - z[29] + z[7];
    z[45]=z[7]*z[45];
    z[49]=9*z[3] + z[2];
    z[49]=z[2]*z[49];
    z[15]=z[15] + z[33] + z[25] + z[45] + z[49];
    z[15]=z[5]*z[15];
    z[25]=z[2]*z[36];
    z[25]=z[25] + z[51] + z[26];
    z[25]=z[2]*z[25];
    z[33]=z[10]*z[35];
    z[35]=z[9] - z[8];
    z[36]=z[35]*z[7];
    z[45]= - static_cast<T>(4)- z[36];
    z[45]=z[4]*z[45];
    z[33]=z[45] + z[33] + z[53];
    z[33]=z[4]*z[33];
    z[35]=z[35]*z[55];
    z[25]=z[33] + z[35] + z[25];
    z[25]=z[4]*z[25];
    z[33]=z[12]*z[29];
    z[35]=z[10] - z[8];
    z[35]=z[3]*z[35];
    z[35]= - static_cast<T>(1)+ z[35];
    z[35]=z[7]*z[35];
    z[35]=z[3] + z[35];
    z[35]=z[7]*z[35];
    z[33]=z[33] + z[35];
    z[33]=z[7]*z[33];
    z[29]= - z[11]*z[29];
    z[35]= - z[10] - z[8];
    z[35]=z[3]*z[35];
    z[35]= - static_cast<T>(1)+ z[35];
    z[35]=z[2]*z[35];
    z[35]=z[3] + z[35];
    z[35]=z[2]*z[35];
    z[29]=z[29] + z[35];
    z[29]=z[2]*z[29];
    z[15]=z[16] + z[15] + z[18] + z[25] + z[33] + z[29];
    z[15]=z[1]*z[15];
    z[16]=z[20]*z[11];
    z[18]=z[4]*z[8];
    z[25]=static_cast<T>(1)+ z[18];
    z[25]=z[4]*z[9]*z[25];
    z[19]=z[25] - z[19] + z[16] - z[21];
    z[19]=z[4]*z[19];
    z[21]=z[21] + 1;
    z[25]= - z[12]*z[20];
    z[29]=z[17]*z[6];
    z[33]=static_cast<T>(1)+ z[29];
    z[33]=z[6]*z[9]*z[33];
    z[25]=z[33] - z[31] + z[25] - z[21];
    z[25]=z[6]*z[25];
    z[31]=z[46]*z[14];
    z[33]= - z[22]*z[18];
    z[31]=z[31] + z[33];
    z[31]=z[4]*z[31];
    z[33]= - z[8] + z[20];
    z[33]=z[9]*z[33];
    z[31]=z[33] + z[31];
    z[31]=z[4]*z[31];
    z[33]= - z[37]*z[14];
    z[29]= - z[22]*z[29];
    z[29]=z[33] + z[29];
    z[29]=z[6]*z[29];
    z[23]= - z[8] - z[23];
    z[23]=z[9]*z[23];
    z[23]=z[23] + z[29];
    z[23]=z[6]*z[23];
    z[23]=z[23] - z[20] + z[31];
    z[23]=z[5]*z[23];
    z[29]=z[20] - z[43];
    z[29]=z[4]*z[29];
    z[22]=z[22]*z[6];
    z[14]=z[14] + z[22];
    z[14]=z[6]*z[14];
    z[14]= - z[9] + z[14];
    z[14]=z[6]*z[14];
    z[14]=z[23] + z[14] - static_cast<T>(2)+ z[29];
    z[14]=z[14]*z[30];
    z[22]=z[41]*z[26];
    z[23]=z[38]*z[32];
    z[26]= - z[12] - 4*z[3];
    z[14]=z[14] + z[25] + z[19] + z[23] + 2*z[26] + z[22];
    z[14]=z[5]*z[14];
    z[19]=z[40]*z[44];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[7]*z[19];
    z[22]= - 2*z[28] - static_cast<T>(3)+ z[47];
    z[22]=z[2]*z[22];
    z[18]= - z[28]*z[18];
    z[18]=z[18] - static_cast<T>(2)- z[36];
    z[18]=z[4]*z[18];
    z[18]=z[18] + z[22] + z[51] + z[19];
    z[18]=z[4]*z[18];
    z[19]=z[9] - z[10];
    z[22]=z[19]*z[24];
    z[22]= - z[9] + z[22];
    z[22]=z[2]*z[22];
    z[22]=2*z[39] + z[22];
    z[22]=z[2]*z[22];
    z[23]= - z[48]*z[27];
    z[24]= - z[6]*z[24];
    z[24]=z[24] - z[2];
    z[19]=z[19]*z[24];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[6]*z[19];
    z[24]= - z[7]*z[42];
    z[19]=z[19] + z[23] + z[24] + z[22];
    z[19]=z[6]*z[19];
    z[22]=z[12] - z[11];
    z[23]=z[22]*z[51];
    z[24]=npow(z[10],2);
    z[25]= - z[8]*z[10];
    z[24]= - z[24] + z[25];
    z[24]=z[3]*z[24];
    z[25]=2*z[10];
    z[24]=z[24] + z[25] + z[8];
    z[24]=z[7]*z[24];
    z[24]=z[47] + z[24];
    z[24]=z[7]*z[24];
    z[26]=z[10]*z[12];
    z[27]= - static_cast<T>(3)- z[26];
    z[27]=z[3]*z[27];
    z[27]=z[12] + z[27];
    z[24]=2*z[27] + z[24];
    z[24]=z[7]*z[24];
    z[21]= - z[10]*z[21];
    z[17]=z[17] + z[21];
    z[17]=z[2]*z[17];
    z[17]=z[47] + z[17];
    z[17]=z[2]*z[17];
    z[21]=z[34]*z[11];
    z[27]=z[21] + 1;
    z[28]=z[3]*z[27];
    z[17]=z[17] + 2*z[11] + z[28];
    z[17]=z[2]*z[17];
    z[14]=z[15] + z[14] + z[19] + z[18] + z[17] + z[23] + z[24];
    z[14]=z[1]*z[14];
    z[15]=z[22]*z[13];
    z[17]=z[21] - z[26] + z[15];
    z[17]=z[3]*z[17];
    z[18]=z[12] + z[11];
    z[17]=z[17] + z[18];
    z[19]=z[9]*z[12];
    z[21]=z[19] - static_cast<T>(1)- 2*z[26];
    z[22]=static_cast<T>(1)+ z[26];
    z[22]=z[22]*z[25];
    z[22]=z[22] + z[8];
    z[22]=z[3]*z[22];
    z[21]=2*z[21] + z[22];
    z[21]=z[7]*z[21];
    z[18]= - z[9]*z[18];
    z[15]=z[18] - z[15];
    z[18]= - static_cast<T>(1)- z[19];
    z[18]=z[18]*z[20];
    z[18]= - z[8] + z[18];
    z[18]=z[6]*z[18];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[9]*z[16];
    z[16]= - z[8] + z[16];
    z[16]=z[4]*z[16];
    z[15]=z[18] + 2*z[15] + z[16];
    z[15]=z[5]*z[15];
    z[16]= - z[9]*z[11];
    z[16]=z[16] - z[27];
    z[18]= - z[8]*z[27];
    z[18]=z[10] + z[18];
    z[18]=z[3]*z[18];
    z[16]=2*z[16] + z[18];
    z[16]=z[2]*z[16];
    z[14]=z[14] + z[15] + z[4] + z[16] + 2*z[17] + z[21];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r989(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r989(const std::array<dd_real,31>&);
#endif
