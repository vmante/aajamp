#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r982(const std::array<T,31>& k) {
  T z[59];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[8];
    z[12]=k[15];
    z[13]=k[2];
    z[14]=npow(z[10],2);
    z[15]=z[14]*z[7];
    z[16]=z[10] + z[8];
    z[17]=z[15] + z[16];
    z[18]=z[10]*z[16];
    z[19]=npow(z[10],3);
    z[20]=z[7]*z[19];
    z[18]=z[18] + z[20];
    z[18]=z[6]*z[18];
    z[18]=z[18] - z[17];
    z[18]=z[6]*z[18];
    z[20]=z[4]*z[8];
    z[21]=z[20] + 1;
    z[22]=z[7]*z[10];
    z[23]=z[21] + z[22];
    z[18]=z[18] + z[23];
    z[18]=z[6]*z[18];
    z[24]=3*z[9];
    z[25]=2*z[8];
    z[26]=z[24] + z[25];
    z[27]=z[9] + z[8];
    z[28]=z[9]*z[27];
    z[29]=npow(z[9],3);
    z[30]= - z[5]*z[29];
    z[28]=z[28] + z[30];
    z[28]=z[3]*z[28];
    z[30]=npow(z[9],2);
    z[31]=z[30]*z[5];
    z[28]=z[28] - 4*z[31] + z[26];
    z[28]=z[3]*z[28];
    z[32]=z[20] - 3;
    z[33]=z[5]*z[9];
    z[34]=6*z[33];
    z[28]=z[28] - z[34] - z[32];
    z[28]=z[3]*z[28];
    z[35]=z[22]*z[4];
    z[36]=2*z[4];
    z[35]=z[35] + z[36];
    z[37]=z[4]*z[10];
    z[38]= - static_cast<T>(4)+ z[37];
    z[38]=z[5]*z[38];
    z[18]=z[28] + z[18] + z[38] - z[35];
    z[18]=z[2]*z[18];
    z[28]=2*z[7];
    z[38]= - z[14]*z[28];
    z[38]= - z[10] + z[38];
    z[38]=z[6]*z[38];
    z[38]=z[38] + static_cast<T>(4)+ 5*z[22];
    z[38]=z[6]*z[38];
    z[39]=7*z[4];
    z[38]=z[38] - z[39] - 8*z[7];
    z[38]=z[6]*z[38];
    z[40]= - z[8] + z[31];
    z[40]=z[3]*z[40];
    z[40]=z[40] + static_cast<T>(4)- z[33];
    z[40]=z[3]*z[40];
    z[40]=z[40] - 8*z[4] - 9*z[5];
    z[40]=z[3]*z[40];
    z[41]=z[5]*z[4];
    z[42]=z[7]*z[4];
    z[18]=z[18] + z[40] + z[38] + 8*z[42] + 11*z[41];
    z[38]=2*z[2];
    z[18]=z[18]*z[38];
    z[40]=18*z[4];
    z[43]=npow(z[7],2);
    z[44]=z[10]*z[43];
    z[44]=z[40] + z[44];
    z[44]=z[7]*z[44];
    z[45]=z[22] - z[21];
    z[45]=z[6]*z[45];
    z[45]=z[45] - z[4] - 9*z[7];
    z[45]=z[6]*z[45];
    z[44]=z[44] + z[45];
    z[44]=z[6]*z[44];
    z[45]=npow(z[5],2);
    z[46]= - z[9]*z[45];
    z[40]=z[40] + z[46];
    z[40]=z[5]*z[40];
    z[32]=z[33] + z[32];
    z[32]=z[3]*z[32];
    z[32]= - z[4] + z[32];
    z[32]=z[3]*z[32];
    z[32]=z[40] + z[32];
    z[32]=z[3]*z[32];
    z[40]=npow(z[7],3);
    z[46]= - z[40]*z[37];
    z[47]=z[37] - 2;
    z[48]=npow(z[5],3);
    z[49]=z[47]*z[48];
    z[18]=z[18] + z[32] + z[44] + z[46] + z[49];
    z[18]=z[2]*z[18];
    z[17]= - z[6]*z[17];
    z[17]=2*z[23] + z[17];
    z[17]=z[6]*z[17];
    z[32]=z[7] + z[4];
    z[44]=3*z[32];
    z[17]= - z[44] + z[17];
    z[17]=z[6]*z[17];
    z[46]= - z[31] + z[27];
    z[46]=z[3]*z[46];
    z[46]=z[46] - 4*z[33] + static_cast<T>(3)- 2*z[20];
    z[46]=z[3]*z[46];
    z[49]=2*z[5];
    z[50]= - z[4] - z[49];
    z[46]=3*z[50] + z[46];
    z[46]=z[3]*z[46];
    z[50]=4*z[41];
    z[17]=z[46] + z[17] + 3*z[42] + z[50];
    z[17]=z[2]*z[17];
    z[46]=static_cast<T>(1)+ 2*z[22];
    z[46]=z[6]*z[46];
    z[46]=z[46] - 5*z[4] - 7*z[7];
    z[46]=z[6]*z[46];
    z[46]=15*z[42] + z[46];
    z[46]=z[6]*z[46];
    z[39]=z[3] - z[39] - 5*z[5];
    z[39]=z[3]*z[39];
    z[39]=15*z[41] + z[39];
    z[39]=z[3]*z[39];
    z[17]=z[17] + z[46] + z[39];
    z[17]=z[17]*z[38];
    z[39]=z[48]*z[36];
    z[46]=z[4] - z[7];
    z[46]=z[6]*z[46];
    z[46]=10*z[42] + z[46];
    z[46]=z[6]*z[46];
    z[46]= - z[40] + z[46];
    z[46]=z[6]*z[46];
    z[51]=z[5] + z[4];
    z[51]=z[51]*z[3];
    z[52]=10*z[41] + z[51];
    z[52]=z[3]*z[52];
    z[52]= - z[48] + z[52];
    z[52]=z[3]*z[52];
    z[53]=z[40]*z[4];
    z[17]=z[17] + z[52] + z[46] + z[53] + z[39];
    z[17]=z[2]*z[17];
    z[23]=z[6]*z[23];
    z[23]= - z[44] + z[23];
    z[23]=z[6]*z[23];
    z[23]=6*z[42] + z[23];
    z[23]=z[6]*z[23];
    z[39]=z[33] - 1;
    z[44]= - z[20] - z[39];
    z[44]=z[3]*z[44];
    z[46]=3*z[4];
    z[44]=z[44] - z[46] - 4*z[5];
    z[44]=z[3]*z[44];
    z[44]=6*z[41] + z[44];
    z[44]=z[3]*z[44];
    z[23]=z[23] + z[44];
    z[23]=z[2]*z[23];
    z[44]=9*z[42];
    z[52]= - z[4] - z[28];
    z[52]=z[6]*z[52];
    z[52]=z[44] + z[52];
    z[54]=npow(z[6],2);
    z[52]=z[52]*z[54];
    z[55]= - z[36] - z[5];
    z[55]=z[3]*z[55];
    z[55]=9*z[41] + z[55];
    z[56]=npow(z[3],2);
    z[55]=z[55]*z[56];
    z[23]=z[23] + z[52] + z[55];
    z[23]=z[23]*z[38];
    z[52]=z[54]*z[42];
    z[52]=z[53] + z[52];
    z[52]=z[6]*z[52];
    z[55]=z[56]*z[41];
    z[57]=z[4]*z[48];
    z[55]=z[57] + z[55];
    z[55]=z[3]*z[55];
    z[23]=z[23] + z[52] + z[55];
    z[23]=z[2]*z[23];
    z[52]= - z[56] - z[54];
    z[55]=z[5]*z[7];
    z[52]=z[55]*z[52];
    z[57]=z[45]*z[7];
    z[57]=z[57] + z[40];
    z[58]=z[5]*z[57];
    z[52]=z[58] + z[52];
    z[52]=z[3]*z[6]*z[52];
    z[58]= - z[6]*z[32];
    z[58]=4*z[42] + z[58];
    z[58]=z[58]*z[54];
    z[50]=z[50] - z[51];
    z[50]=z[50]*z[56];
    z[50]=z[58] + z[50];
    z[50]=z[2]*z[50];
    z[51]=npow(z[6],3);
    z[58]=z[51]*z[42];
    z[41]=z[41]*npow(z[3],3);
    z[41]=z[58] + z[41];
    z[50]=2*z[41] + z[50];
    z[50]=z[50]*npow(z[2],2);
    z[41]=z[1]*z[41]*npow(z[2],3);
    z[41]=z[50] + z[41];
    z[41]=z[1]*z[41];
    z[23]=2*z[41] + z[52] + z[23];
    z[23]=z[1]*z[23];
    z[41]=z[5] + z[7];
    z[50]=z[41]*z[54];
    z[48]=z[50] - z[40] - z[48];
    z[48]=z[6]*z[48];
    z[50]=z[32]*z[45];
    z[50]=z[40] + z[50];
    z[50]=z[5]*z[50];
    z[52]=z[41]*z[6];
    z[58]= - z[5]*z[32];
    z[58]=z[58] + z[52];
    z[56]=z[58]*z[56];
    z[48]=z[56] + z[50] + z[48];
    z[48]=z[3]*z[48];
    z[50]=z[57]*z[49];
    z[56]= - z[5]*z[28];
    z[56]= - z[42] + z[56];
    z[56]=z[56]*z[54];
    z[50]=z[56] + z[53] + z[50];
    z[50]=z[6]*z[50];
    z[17]=z[23] + z[17] + z[50] + z[48];
    z[17]=z[1]*z[17];
    z[23]=z[7]*z[9];
    z[48]=z[23] - 1;
    z[48]=z[48]*z[43];
    z[39]=z[39]*z[45];
    z[50]=z[23] + z[33];
    z[50]=z[50]*z[6];
    z[56]= - z[50] - z[41];
    z[56]=z[6]*z[56];
    z[39]=z[56] + z[48] + z[39];
    z[39]=z[6]*z[39];
    z[56]=z[4]*z[9];
    z[57]=z[56] - z[23];
    z[57]=z[57]*z[5];
    z[58]=z[7] + z[57];
    z[58]=z[5]*z[58];
    z[48]= - z[48] + z[58];
    z[48]=z[5]*z[48];
    z[57]= - z[50] + z[36] - z[57];
    z[57]=z[3]*z[57];
    z[52]=z[57] + z[55] - z[52];
    z[52]=z[3]*z[52];
    z[39]=z[52] + z[48] + z[39];
    z[39]=z[3]*z[39];
    z[48]=z[56] - 2;
    z[52]=z[48]*z[40];
    z[55]=z[43] + z[45];
    z[57]=z[22] - 1;
    z[58]=z[57]*z[5];
    z[55]=z[55]*z[58];
    z[48]= - z[7]*z[48];
    z[48]= - z[58] + z[4] + z[48];
    z[48]=z[48]*z[54];
    z[48]=z[48] + z[52] + z[55];
    z[48]=z[6]*z[48];
    z[36]=z[36] + z[7];
    z[36]=z[36]*z[45];
    z[36]=z[40] + z[36];
    z[36]=z[5]*z[36];
    z[17]=z[17] + z[18] + z[39] + z[48] + z[53] + z[36];
    z[17]=z[1]*z[17];
    z[18]= - z[6] - z[4];
    z[18]=z[33]*z[18];
    z[36]=z[28] - z[4];
    z[39]=z[9] - z[8];
    z[40]=z[39]*z[7];
    z[48]= - z[3]*z[40];
    z[18]=z[48] + z[18] - z[36];
    z[18]=z[3]*z[18];
    z[40]= - static_cast<T>(1)+ z[40];
    z[40]=z[40]*z[43];
    z[48]=z[7]*z[33];
    z[36]= - 2*z[36] + z[48];
    z[36]=z[5]*z[36];
    z[48]= - z[6]*z[23];
    z[41]= - 4*z[41] + z[48];
    z[41]=z[6]*z[41];
    z[18]=z[18] + z[41] + z[40] + z[36];
    z[18]=z[3]*z[18];
    z[36]= - z[16]*z[14]*z[51];
    z[26]=z[9]*z[26];
    z[40]=z[3]*z[27]*z[30];
    z[26]=z[26] + z[40];
    z[26]=z[3]*z[26];
    z[26]=z[26] + z[8] + z[24];
    z[26]=z[3]*z[26];
    z[26]=z[26] + static_cast<T>(1)+ z[36];
    z[26]=z[2]*z[26];
    z[36]= - z[25] - z[9];
    z[36]=z[3]*z[36];
    z[36]=z[36] + 1;
    z[36]=z[9]*z[36];
    z[31]= - 3*z[31] - z[25] + z[36];
    z[31]=z[3]*z[31];
    z[31]=z[31] + static_cast<T>(5)- 9*z[33];
    z[31]=z[3]*z[31];
    z[36]= - static_cast<T>(3)+ z[37];
    z[36]=z[5]*z[36];
    z[35]=z[36] - z[35];
    z[15]= - z[10] - z[15];
    z[36]=z[6]*z[14];
    z[15]=3*z[15] + z[36];
    z[15]=z[6]*z[15];
    z[36]=static_cast<T>(1)+ z[22];
    z[15]=3*z[36] + z[15];
    z[15]=z[6]*z[15];
    z[15]=z[26] + z[31] + 3*z[35] + z[15];
    z[15]=z[15]*z[38];
    z[26]=3*z[22];
    z[31]=static_cast<T>(1)+ z[26];
    z[16]=z[6]*z[16];
    z[16]=2*z[31] + z[16];
    z[16]=z[6]*z[16];
    z[31]= - static_cast<T>(8)- z[22];
    z[31]=z[7]*z[31];
    z[16]=z[16] + z[31] - 3*z[11] - z[4];
    z[16]=z[6]*z[16];
    z[24]=z[8] - z[24];
    z[24]=z[3]*z[24];
    z[24]=z[24] - static_cast<T>(7)+ z[34];
    z[24]=z[3]*z[24];
    z[31]=static_cast<T>(4)+ z[33];
    z[31]=z[5]*z[31];
    z[24]=z[24] + z[31] - 3*z[12] - z[4];
    z[24]=z[3]*z[24];
    z[31]=12*z[4] + z[5];
    z[31]=z[5]*z[31];
    z[15]=z[15] + z[24] + z[16] + z[44] + z[31];
    z[15]=z[2]*z[15];
    z[16]= - z[9]*z[42];
    z[24]=2*z[9];
    z[31]=z[24] + z[10];
    z[34]=z[31]*z[5];
    z[35]= - static_cast<T>(1)- z[34];
    z[35]=z[6]*z[35];
    z[16]=z[35] - z[49] + z[4] + z[16];
    z[16]=z[6]*z[16];
    z[28]=z[32]*z[28];
    z[32]= - static_cast<T>(5)- z[22];
    z[32]=z[7]*z[32];
    z[34]= - static_cast<T>(2)+ z[34];
    z[34]=z[5]*z[34];
    z[32]=z[32] + z[34];
    z[32]=z[5]*z[32];
    z[16]=z[16] + z[28] + z[32];
    z[16]=z[6]*z[16];
    z[28]=z[4]*z[31]*z[45];
    z[31]=z[12]*z[46];
    z[28]=z[28] + z[31] - z[43];
    z[28]=z[5]*z[28];
    z[31]= - z[10] + z[9];
    z[31]=z[4]*z[31];
    z[31]=static_cast<T>(1)+ z[31];
    z[31]=z[31]*z[43];
    z[32]=z[11]*z[46];
    z[31]=z[32] + z[31];
    z[31]=z[7]*z[31];
    z[15]=z[17] + z[15] + z[18] + z[16] + z[31] + z[28];
    z[15]=z[1]*z[15];
    z[16]=z[6]*z[10];
    z[16]=z[16] + 1;
    z[17]=z[10] - z[8];
    z[16]=z[6]*z[17]*z[16];
    z[18]=2*z[10];
    z[28]=z[18]*z[11];
    z[16]=z[16] + z[26] - z[28] + z[21];
    z[16]=z[6]*z[16];
    z[21]=z[24]*z[12];
    z[26]=z[3]*z[8];
    z[31]=5*z[26] - 9;
    z[31]=z[9]*z[31];
    z[31]=4*z[8] + z[31];
    z[31]=z[3]*z[31];
    z[20]=z[31] - 3*z[33] - z[20] - static_cast<T>(7)+ z[21];
    z[20]=z[3]*z[20];
    z[19]=z[19]*z[6];
    z[31]= - z[14] - z[19];
    z[17]=z[54]*z[17]*z[31];
    z[31]=z[25] - z[9];
    z[31]=z[31]*z[30];
    z[29]=z[29]*z[26];
    z[29]=z[31] + z[29];
    z[29]=z[3]*z[29];
    z[24]=z[8] - z[24];
    z[24]=z[9]*z[24];
    z[24]=z[24] + z[29];
    z[24]=z[3]*z[24];
    z[17]=z[24] - z[9] + z[17];
    z[17]=z[2]*z[17];
    z[19]=2*z[14] + z[19];
    z[19]=z[19]*z[54];
    z[24]=z[30]*z[26];
    z[26]=z[9]*z[39];
    z[24]=4*z[26] - 3*z[24];
    z[24]=z[3]*z[24];
    z[24]=z[24] - z[8] + 6*z[9];
    z[24]=z[3]*z[24];
    z[17]=z[17] + z[24] + static_cast<T>(2)+ z[19];
    z[17]=z[17]*z[38];
    z[19]= - z[22]*z[46];
    z[22]=z[4] + z[12];
    z[22]=2*z[22];
    z[24]=z[5]*z[47];
    z[16]=z[17] + z[20] + z[16] + 3*z[24] + z[22] + z[19];
    z[16]=z[2]*z[16];
    z[17]=z[9]*z[8];
    z[19]=npow(z[8],2);
    z[17]=z[17] + z[19];
    z[19]=z[7]*z[17];
    z[19]= - z[8] + z[19];
    z[19]=z[19]*z[43];
    z[20]=2*z[23] + static_cast<T>(4)+ z[56];
    z[20]=z[5]*z[20];
    z[17]= - z[3]*z[17];
    z[17]=z[17] + z[27];
    z[17]=z[7]*z[17];
    z[17]=static_cast<T>(4)+ z[17];
    z[17]=z[3]*z[17];
    z[17]=z[17] + 2*z[50] + z[20] - z[22] + z[19];
    z[17]=z[3]*z[17];
    z[19]=z[9]*z[10];
    z[14]=z[19] + z[14];
    z[19]=z[5]*z[14];
    z[19]= - z[10] + z[19];
    z[19]=z[5]*z[19];
    z[19]= - 2*z[57] + z[19];
    z[19]=z[5]*z[19];
    z[14]= - z[6]*z[14];
    z[14]=z[14] - z[10];
    z[14]=z[5]*z[14];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[6]*z[14];
    z[20]=static_cast<T>(3)+ z[56];
    z[20]=z[7]*z[20];
    z[14]=z[14] + z[19] - z[4] + z[20];
    z[14]=z[6]*z[14];
    z[19]= - z[37] - 1;
    z[19]=z[9]*z[19];
    z[19]= - z[25] + z[19];
    z[19]=z[19]*z[43];
    z[20]=z[28] - 1;
    z[23]=z[4]*z[20];
    z[19]=z[23] + z[19];
    z[19]=z[7]*z[19];
    z[23]=z[9]*z[37];
    z[23]=z[18] + z[23];
    z[23]=z[5]*z[23];
    z[23]= - static_cast<T>(2)+ z[23];
    z[23]=z[5]*z[23];
    z[24]=z[12]*z[37];
    z[24]=z[24] - z[7];
    z[23]=2*z[24] + z[23];
    z[23]=z[5]*z[23];
    z[14]=z[15] + z[16] + z[17] + z[14] + z[19] + z[23];
    z[14]=z[1]*z[14];
    z[15]=z[21] + 1;
    z[16]= - z[10]*z[12];
    z[16]=z[16] - z[15];
    z[17]= - z[10]*z[11];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[18];
    z[17]= - z[8] + z[17];
    z[17]=z[6]*z[17];
    z[19]= - z[9]*z[15];
    z[19]=z[8] + z[19];
    z[19]=z[3]*z[19];
    z[16]=z[19] + 2*z[16] + z[17];
    z[16]=z[2]*z[16];
    z[17]=z[13] - z[10];
    z[17]=z[12]*z[17];
    z[18]=z[12]*z[18];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[10]*z[18];
    z[18]=z[18] - z[9];
    z[18]=z[4]*z[18];
    z[17]=2*z[17] + z[18];
    z[17]=z[5]*z[17];
    z[18]= - z[12]*z[13];
    z[15]=z[18] + z[15];
    z[15]=z[3]*z[15];
    z[18]=z[10]*z[20];
    z[18]=z[18] - z[9];
    z[18]=z[4]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[7]*z[18];
    z[14]=z[14] + z[16] + 2*z[15] + z[17] + z[22] + z[18];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r982(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r982(const std::array<dd_real,31>&);
#endif
