#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r936(const std::array<T,31>& k) {
  T z[81];
  T r = 0;

    z[1]=k[1];
    z[2]=k[10];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[14];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[7];
    z[9]=k[11];
    z[10]=k[3];
    z[11]=k[8];
    z[12]=k[2];
    z[13]=k[9];
    z[14]=k[15];
    z[15]=z[9]*z[6];
    z[16]=z[15] + 1;
    z[17]=npow(z[9],2);
    z[18]=z[16]*z[17];
    z[19]=z[8]*z[6];
    z[20]=z[19] - 1;
    z[21]=31*z[8];
    z[22]= - z[20]*z[21];
    z[22]=n<T>(159,2)*z[9] + z[22];
    z[22]=z[8]*z[22];
    z[23]=z[4]*z[6];
    z[24]=z[23] - 1;
    z[25]=npow(z[4],2);
    z[26]=3*z[25];
    z[27]=z[24]*z[26];
    z[28]=z[19] - z[15];
    z[29]=249*z[23];
    z[30]= - 145*z[28] - z[29];
    z[30]=z[2]*z[30];
    z[31]=249*z[4];
    z[30]= - z[31] + z[30];
    z[30]=z[2]*z[30];
    z[22]=z[30] + z[27] - n<T>(123,2)*z[18] + z[22];
    z[27]=n<T>(1,4)*z[2];
    z[22]=z[22]*z[27];
    z[28]=71*z[28] + 87*z[23];
    z[30]=n<T>(1,2)*z[2];
    z[28]=z[28]*z[30];
    z[32]=z[8] - z[9];
    z[33]=71*z[32];
    z[34]=z[8]*z[9];
    z[35]=z[7]*z[34];
    z[28]= - n<T>(71,2)*z[35] + z[28] + z[33] - n<T>(87,2)*z[4];
    z[28]=z[5]*z[28];
    z[35]=z[2]*z[4];
    z[28]= - n<T>(87,2)*z[35] + z[28];
    z[28]=z[5]*z[28];
    z[36]=npow(z[8],2);
    z[37]=n<T>(31,2)*z[8];
    z[38]=47*z[9] - z[37];
    z[38]=z[38]*z[36];
    z[39]=npow(z[9],3);
    z[40]=n<T>(123,2)*z[39];
    z[41]=31*z[36];
    z[42]=z[41]*z[9];
    z[43]=z[40] - z[42];
    z[44]=z[7]*z[8];
    z[45]=z[43]*z[44];
    z[46]=npow(z[4],3);
    z[22]=n<T>(1,2)*z[28] - n<T>(1,4)*z[45] + z[22] - n<T>(3,4)*z[46] - n<T>(123,4)*
    z[39] + z[38];
    z[22]=z[5]*z[22];
    z[28]=z[9]*z[10];
    z[38]=z[28] - 1;
    z[45]= - z[38]*z[41];
    z[47]=z[10]*z[40];
    z[45]=z[47] + z[45];
    z[45]=z[8]*z[45];
    z[45]=123*z[39] + z[45];
    z[47]=n<T>(1,2)*z[8];
    z[48]= - z[38]*z[47];
    z[48]= - z[9] + z[48];
    z[48]=z[2]*z[48];
    z[48]=n<T>(1,2)*z[34] + z[48];
    z[48]=z[2]*z[48];
    z[45]=n<T>(1,2)*z[45] + 145*z[48];
    z[45]=z[45]*z[30];
    z[48]=n<T>(555,2)*z[39];
    z[49]=z[48] - z[42];
    z[49]=z[49]*z[8];
    z[50]=z[26]*z[34];
    z[50]=z[49] + z[50];
    z[51]=z[7]*z[4];
    z[52]=z[50]*z[51];
    z[42]=n<T>(339,2)*z[39] - z[42];
    z[42]=z[42]*z[47];
    z[53]= - z[8]*z[17];
    z[39]= - z[39] + z[53];
    z[53]= - z[9]*z[25];
    z[39]=n<T>(185,2)*z[39] + z[53];
    z[39]=z[4]*z[39];
    z[53]=npow(z[11],3);
    z[54]=53*z[53];
    z[55]=z[54]*z[9];
    z[22]=z[22] + n<T>(1,4)*z[52] + z[45] + n<T>(3,4)*z[39] + z[55] + z[42];
    z[39]=n<T>(555,2)*z[17];
    z[38]= - z[38]*z[39];
    z[42]=z[28] + 1;
    z[42]=z[42]*z[8];
    z[45]=z[9] + z[42];
    z[45]=z[45]*z[21];
    z[38]=z[38] + z[45];
    z[38]=z[8]*z[38];
    z[45]=npow(z[8],3);
    z[45]=31*z[45];
    z[52]= - z[8]*z[26];
    z[52]=z[45] + z[52];
    z[52]=z[52]*z[51];
    z[38]=z[52] + z[48] + z[38];
    z[18]= - n<T>(111,4)*z[18] + 19*z[34];
    z[52]=n<T>(1,2)*z[15];
    z[56]=static_cast<T>(1)- z[52];
    z[56]=z[56]*z[26];
    z[18]=5*z[18] + z[56];
    z[18]=z[4]*z[18];
    z[24]= - z[24]*z[25];
    z[56]=z[35]*z[6];
    z[57]=z[4] + z[56];
    z[57]=z[2]*z[57];
    z[24]=z[24] + 83*z[57];
    z[24]=z[2]*z[24];
    z[18]=n<T>(3,2)*z[24] + z[18] + n<T>(1,2)*z[38];
    z[24]=z[2]*z[6];
    z[38]=z[51] + z[24] - 1;
    z[57]=z[38]*z[5];
    z[58]=z[2] - z[57];
    z[58]=z[5]*z[58];
    z[59]=6*z[25];
    z[60]=static_cast<T>(1)+ z[24];
    z[60]=z[2]*z[60];
    z[60]=n<T>(181,2)*z[4] + 249*z[60];
    z[60]=z[2]*z[60];
    z[61]=z[7]*z[46];
    z[58]=n<T>(87,16)*z[58] - n<T>(3,16)*z[61] + z[59] + n<T>(1,16)*z[60];
    z[58]=z[5]*z[58];
    z[60]=z[8]*z[51];
    z[42]=z[60] - z[9] + z[42];
    z[60]= - static_cast<T>(31)+ n<T>(351,2)*z[15];
    z[60]=z[4]*z[60];
    z[42]= - n<T>(289,2)*z[56] + z[60] + n<T>(351,2)*z[42];
    z[60]=npow(z[6],2);
    z[61]=npow(z[6],3);
    z[62]=z[2]*z[61];
    z[62]=z[60] + z[62];
    z[62]=z[5]*z[62];
    z[63]=z[60]*z[2];
    z[62]=z[63] + z[62];
    z[62]=z[5]*z[62];
    z[62]= - n<T>(289,32)*z[38] + z[62];
    z[62]=z[5]*z[62];
    z[42]=n<T>(1,16)*z[42] + z[62];
    z[42]=z[3]*z[42];
    z[62]=z[4]*z[8];
    z[62]=z[34] + z[62];
    z[62]=351*z[62] - 545*z[35];
    z[64]=z[63]*z[5];
    z[65]= - 3*z[24] + z[64];
    z[65]=z[5]*z[65];
    z[65]=z[65] + 8*z[4] - n<T>(545,32)*z[2];
    z[65]=z[5]*z[65];
    z[42]=z[42] + n<T>(1,32)*z[62] + z[65];
    z[42]=z[3]*z[42];
    z[18]=z[42] + n<T>(1,8)*z[18] + z[58];
    z[18]=z[3]*z[18];
    z[42]=z[50]*z[4];
    z[50]=npow(z[2],2);
    z[58]=z[50]*z[34];
    z[43]=z[43]*z[8];
    z[58]= - z[43] + 145*z[58];
    z[62]= - z[2]*z[58];
    z[62]= - z[42] + z[62];
    z[65]= - 145*z[32] + z[31];
    z[65]=z[65]*z[50];
    z[40]=z[65] - 3*z[46] - z[40] - z[45];
    z[40]=z[40]*z[30];
    z[33]=z[33] - 87*z[4];
    z[33]=z[33]*z[30];
    z[33]=z[33] - 71*z[34];
    z[65]=npow(z[5],2);
    z[33]=z[33]*z[65];
    z[33]=z[33] - z[43] + z[40];
    z[33]=z[5]*z[33];
    z[33]=n<T>(1,2)*z[62] + z[33];
    z[40]=z[32]*z[26];
    z[40]=z[40] - z[48] - z[45];
    z[40]=z[4]*z[40];
    z[43]=z[50]*z[4];
    z[43]= - z[46] + 83*z[43];
    z[45]=3*z[2];
    z[43]=z[43]*z[45];
    z[40]= - z[43] + z[49] + z[40];
    z[45]=npow(z[2],3);
    z[45]= - z[46] - n<T>(83,2)*z[45];
    z[46]=z[30] - z[4];
    z[48]=z[5]*z[46];
    z[48]= - 5*z[35] + n<T>(87,8)*z[48];
    z[48]=z[5]*z[48];
    z[45]=n<T>(3,8)*z[45] + z[48];
    z[45]=z[5]*z[45];
    z[32]= - z[4]*z[32];
    z[32]= - z[34] + z[32];
    z[32]=39*z[32] + 25*z[35];
    z[38]= - 2*z[38] - z[64];
    z[38]=z[5]*z[38];
    z[38]=n<T>(225,16)*z[46] + z[38];
    z[38]=z[5]*z[38];
    z[32]=n<T>(9,32)*z[32] + z[38];
    z[32]=z[3]*z[32];
    z[38]= - 7*z[4] + 5*z[2];
    z[38]=z[5]*z[38];
    z[38]=15*z[35] + z[38];
    z[38]=z[5]*z[38];
    z[32]=z[38] + z[32];
    z[32]=z[3]*z[32];
    z[32]=z[32] + n<T>(1,16)*z[40] + z[45];
    z[32]=z[3]*z[32];
    z[38]=71*z[65];
    z[40]= - z[35]*z[38];
    z[40]=z[43] + z[40];
    z[40]=z[5]*z[40];
    z[40]= - z[42] + z[40];
    z[42]= - z[2] + 2*z[4];
    z[43]=z[42]*z[5];
    z[45]= - 9*z[35] + z[43];
    z[45]=z[45]*z[65];
    z[42]= - 3*z[42] + z[57];
    z[42]=z[5]*z[42];
    z[42]= - n<T>(129,32)*z[35] + z[42];
    z[42]=z[5]*z[42];
    z[46]=z[4]*z[34];
    z[42]=n<T>(351,32)*z[46] + z[42];
    z[42]=z[3]*z[42];
    z[42]=z[45] + z[42];
    z[42]=z[3]*z[42];
    z[40]=n<T>(1,16)*z[40] + z[42];
    z[40]=z[3]*z[40];
    z[38]= - z[34]*z[38];
    z[38]=z[58] + z[38];
    z[38]=z[5]*z[2]*z[38];
    z[42]= - 4*z[35] + z[43];
    z[42]=z[3]*z[42]*z[65];
    z[43]=z[35]*npow(z[5],3);
    z[42]=2*z[43] + z[42];
    z[42]=z[42]*npow(z[3],2);
    z[43]=z[1]*npow(z[3],3)*z[43];
    z[42]=z[42] + z[43];
    z[42]=z[1]*z[42];
    z[38]=z[42] + n<T>(1,16)*z[38] + z[40];
    z[38]=z[1]*z[38];
    z[32]=z[38] + n<T>(1,8)*z[33] + z[32];
    z[32]=z[1]*z[32];
    z[18]=z[32] + n<T>(1,4)*z[22] + z[18];
    z[18]=z[1]*z[18];
    z[22]=npow(z[14],3);
    z[32]=73*z[22];
    z[33]=z[32]*z[6];
    z[38]=npow(z[14],2);
    z[40]=n<T>(335,8)*z[38] + z[33];
    z[40]=z[6]*z[40];
    z[40]=n<T>(1303,8)*z[14] + z[40];
    z[42]= - static_cast<T>(23)- n<T>(393,32)*z[28];
    z[42]=z[9]*z[42];
    z[43]=z[6] + z[10];
    z[45]=z[43]*z[8];
    z[46]= - static_cast<T>(1)+ z[45];
    z[46]=z[8]*z[46];
    z[40]=n<T>(31,16)*z[46] + n<T>(1,4)*z[40] + z[42];
    z[40]=z[8]*z[40];
    z[42]=npow(z[10],2);
    z[46]=8*z[13];
    z[48]=z[42]*z[46];
    z[49]=n<T>(69,32)*z[10] - z[48];
    z[49]=z[13]*z[49];
    z[49]= - n<T>(2181,32) + z[49];
    z[49]=z[13]*z[49];
    z[50]=n<T>(3,16)*z[4];
    z[57]=z[6] - z[10];
    z[58]=z[57]*z[4];
    z[62]=static_cast<T>(1)- z[58];
    z[62]=z[62]*z[50];
    z[49]=z[49] + z[62];
    z[49]=z[4]*z[49];
    z[45]=n<T>(249,2)*z[58] + static_cast<T>(197)+ n<T>(145,2)*z[45];
    z[45]=z[2]*z[45];
    z[58]= - 337*z[9] - 145*z[8];
    z[31]=z[45] + n<T>(1,2)*z[58] + z[31];
    z[31]=z[2]*z[31];
    z[45]=z[10] - z[12];
    z[58]=z[45]*z[46];
    z[58]= - n<T>(69,32) + z[58];
    z[62]=npow(z[13],2);
    z[58]=z[58]*z[62];
    z[31]=n<T>(1,8)*z[31] + z[49] + z[40] + z[58] - n<T>(393,32)*z[17];
    z[31]=z[2]*z[31];
    z[40]= - 501*z[38] - 41*z[17];
    z[40]=z[9]*z[40];
    z[49]=z[7]*z[9];
    z[58]= - z[32]*z[49];
    z[64]=z[36]*z[9];
    z[40]=z[58] + n<T>(3,8)*z[40] + 47*z[64];
    z[58]=n<T>(1,4)*z[7];
    z[40]=z[40]*z[58];
    z[66]= - 171*z[19] - 151*z[23];
    z[66]=z[2]*z[66];
    z[66]= - 171*z[8] + z[66];
    z[67]= - z[15] - n<T>(1,2)*z[49];
    z[67]=z[5]*z[67];
    z[66]=n<T>(1,2)*z[66] + 71*z[67];
    z[67]=n<T>(1,8)*z[5];
    z[66]=z[66]*z[67];
    z[68]=n<T>(1,4)*z[9];
    z[69]=z[16]*z[9];
    z[70]=123*z[69];
    z[71]= - n<T>(2927,2)*z[14] - z[70];
    z[71]=z[71]*z[68];
    z[72]= - n<T>(3,4)*z[9] - 47*z[8];
    z[72]=z[8]*z[72];
    z[71]=z[71] + z[72];
    z[72]=z[2]*z[15];
    z[72]=n<T>(337,16)*z[72] + n<T>(947,32)*z[4] - 41*z[9] - n<T>(827,32)*z[8];
    z[72]=z[2]*z[72];
    z[40]=z[66] + z[40] + z[72] + n<T>(1,4)*z[71] + z[59];
    z[40]=z[5]*z[40];
    z[66]=z[8]*z[10];
    z[71]=z[66] + 1;
    z[21]=z[71]*z[21];
    z[71]= - static_cast<T>(2951)+ 1009*z[28];
    z[71]=z[9]*z[71];
    z[71]=583*z[11] + z[71];
    z[71]=n<T>(1,2)*z[71] - z[21];
    z[72]=n<T>(1,4)*z[8];
    z[71]=z[71]*z[72];
    z[73]=z[54]*z[6];
    z[74]=npow(z[11],2);
    z[71]=z[71] + n<T>(447,4)*z[74] + z[73];
    z[75]= - 447*z[74] - z[41];
    z[75]=z[75]*z[72];
    z[75]= - z[54] + z[75];
    z[76]=n<T>(53,2)*z[53];
    z[77]=z[44]*z[76];
    z[78]=z[8]*z[25];
    z[75]=z[77] + n<T>(1,2)*z[75] - 9*z[78];
    z[75]=z[7]*z[75];
    z[77]= - 367*z[9] + n<T>(425,2)*z[8];
    z[77]=n<T>(1,8)*z[77] - 3*z[4];
    z[77]=z[4]*z[77];
    z[35]=z[75] - n<T>(963,16)*z[35] + n<T>(1,2)*z[71] + z[77];
    z[71]=z[44] - 1;
    z[75]= - z[66] - z[71];
    z[77]=4*z[6] + z[7];
    z[77]=z[7]*z[77];
    z[77]=5*z[60] + z[77];
    z[77]=z[7]*z[77];
    z[77]=2*z[61] + z[77];
    z[65]=z[77]*z[65];
    z[77]=n<T>(1,2)*z[7];
    z[78]=z[77] + z[6];
    z[65]= - n<T>(321,16)*z[78] + z[65];
    z[65]=z[5]*z[65];
    z[65]=n<T>(351,32)*z[75] + z[65];
    z[65]=z[3]*z[65];
    z[75]=z[4]*z[15];
    z[75]=z[75] - z[9];
    z[56]= - n<T>(113,2)*z[56] - 117*z[8] + n<T>(191,2)*z[75];
    z[75]=z[7] + 3*z[6];
    z[75]=z[75]*z[7];
    z[79]=2*z[60] + z[75];
    z[79]=z[5]*z[79];
    z[63]=z[6] + z[63];
    z[63]=3*z[63] + 2*z[79];
    z[63]=z[5]*z[63];
    z[79]= - n<T>(107,16) + z[24];
    z[63]=3*z[79] + z[63];
    z[63]=z[5]*z[63];
    z[56]=z[65] + n<T>(3,16)*z[56] + z[63];
    z[56]=z[3]*z[56];
    z[63]=npow(z[13],3);
    z[65]=8*z[63];
    z[79]=z[65]*z[7];
    z[59]=z[79] + n<T>(757,32)*z[62] + z[59];
    z[59]=z[7]*z[59];
    z[80]=n<T>(29,8) + z[24];
    z[78]=z[5]*z[78];
    z[78]=3*z[80] - n<T>(71,8)*z[78];
    z[78]=z[5]*z[78];
    z[80]=static_cast<T>(1431)+ 509*z[24];
    z[80]=z[2]*z[80];
    z[80]=z[80] + n<T>(3485,2)*z[13] + 247*z[4];
    z[59]=z[78] + n<T>(1,16)*z[80] + z[59];
    z[59]=z[5]*z[59];
    z[35]=z[56] + n<T>(1,2)*z[35] + z[59];
    z[35]=z[3]*z[35];
    z[56]=z[8]*z[32];
    z[56]=z[55] + z[56];
    z[59]=241*z[74];
    z[39]= - z[59] + z[39];
    z[25]=n<T>(3,16)*z[25] + n<T>(1,16)*z[39];
    z[25]=z[9]*z[25];
    z[25]= - 5*z[64] + z[25];
    z[25]=z[4]*z[25];
    z[39]=n<T>(53,4)*z[53];
    z[53]= - z[9]*z[51]*z[39];
    z[25]=z[53] + n<T>(1,4)*z[56] + z[25];
    z[25]=z[7]*z[25];
    z[17]=n<T>(339,2)*z[17] - n<T>(871,4)*z[74] - z[73];
    z[17]=z[17]*z[68];
    z[53]= - 67*z[9] + n<T>(31,4)*z[8];
    z[53]=z[53]*z[47];
    z[53]=z[53] + n<T>(335,16)*z[38] + z[33];
    z[53]=z[53]*z[47];
    z[56]=3*z[9];
    z[52]=z[52] + 1;
    z[64]=z[9]*z[52];
    z[64]= - n<T>(489,2)*z[11] - 185*z[64];
    z[64]=z[64]*z[56];
    z[16]= - z[16]*z[26];
    z[16]=z[16] + z[64] - n<T>(395,2)*z[34];
    z[64]=n<T>(1,16)*z[4];
    z[16]=z[16]*z[64];
    z[16]=z[18] + z[35] + z[40] + z[25] + z[31] + z[16] + z[53] + z[65]
    + z[17];
    z[16]=z[1]*z[16];
    z[17]= - n<T>(1573,16)*z[38] - z[33];
    z[17]=z[6]*z[17];
    z[17]= - n<T>(1145,16)*z[14] + z[17];
    z[17]=z[6]*z[17];
    z[18]=z[20]*z[66];
    z[17]=n<T>(31,8)*z[18] + n<T>(467,16)*z[28] + n<T>(453,16) + z[17];
    z[17]=z[17]*z[47];
    z[18]=npow(z[10],3);
    z[20]=16*z[13];
    z[25]=z[18]*z[20];
    z[31]=n<T>(457,32)*z[42] + z[25];
    z[31]=z[13]*z[31];
    z[31]=n<T>(1679,32)*z[10] + z[31];
    z[31]=z[13]*z[31];
    z[35]=z[6]*z[10];
    z[35]=z[35] + z[42];
    z[35]=z[35]*z[4];
    z[40]=z[10] - z[35];
    z[40]=z[40]*z[50];
    z[31]=z[40] + n<T>(65,2) + z[31];
    z[31]=z[4]*z[31];
    z[40]= - static_cast<T>(197)+ n<T>(145,2)*z[19];
    z[40]=z[10]*z[40];
    z[35]=n<T>(249,2)*z[35] + z[40];
    z[30]=z[35]*z[30];
    z[35]=z[4]*z[43];
    z[30]=z[30] - n<T>(249,4)*z[35] + static_cast<T>(113)- n<T>(145,4)*z[19];
    z[27]=z[30]*z[27];
    z[30]= - z[10]*z[45]*z[20];
    z[35]=73*z[12];
    z[40]=z[35] - n<T>(457,8)*z[10];
    z[30]=n<T>(1,4)*z[40] + z[30];
    z[30]=z[13]*z[30];
    z[30]= - n<T>(1133,16) + z[30];
    z[30]=z[13]*z[30];
    z[40]=n<T>(919,16)*z[38] + z[33];
    z[43]=n<T>(1,2)*z[6];
    z[40]=z[40]*z[43];
    z[17]=z[27] + z[31] + z[17] + 5*z[9] + z[40] + n<T>(539,32)*z[14] + 
    z[30];
    z[17]=z[2]*z[17];
    z[27]=123*z[11] + n<T>(185,2)*z[69];
    z[27]=z[27]*z[56];
    z[26]=z[15]*z[26];
    z[26]=z[26] - n<T>(59,2)*z[34] + z[59] + z[27];
    z[26]=z[26]*z[64];
    z[27]=z[55]*z[51];
    z[30]=z[54]*z[8];
    z[27]=z[30] - z[27];
    z[30]= - z[76] + z[32];
    z[30]=z[9]*z[30];
    z[31]=z[74]*z[9];
    z[40]=z[54] + n<T>(245,4)*z[31];
    z[40]=z[4]*z[40];
    z[30]=n<T>(1,2)*z[40] + z[30] + z[27];
    z[30]=z[30]*z[77];
    z[40]=73*z[38];
    z[50]= - n<T>(865,4)*z[74] - z[40];
    z[36]= - n<T>(31,8)*z[36] + n<T>(1,2)*z[50] - z[33];
    z[36]=z[36]*z[47];
    z[50]=n<T>(73,4)*z[22];
    z[51]=z[50] + z[65];
    z[53]= - 199*z[74] + n<T>(719,2)*z[38];
    z[53]=n<T>(3,8)*z[53] - z[73];
    z[55]=n<T>(1,2)*z[9];
    z[53]=z[53]*z[55];
    z[26]=z[30] + z[26] + z[36] + z[53] - z[51];
    z[26]=z[7]*z[26];
    z[21]=407*z[11] + z[21];
    z[21]=n<T>(169,16)*z[4] + n<T>(1,8)*z[21];
    z[21]=z[8]*z[21];
    z[30]=1221*z[74];
    z[36]=z[30] - n<T>(757,2)*z[62];
    z[21]=n<T>(1,8)*z[36] + z[73] + z[21];
    z[36]=z[76]*z[71];
    z[30]= - z[30] - z[41];
    z[30]=z[8]*z[30];
    z[30]=n<T>(1,16)*z[30] - z[65] + z[36];
    z[30]=z[7]*z[30];
    z[21]=n<T>(1,2)*z[21] + z[30];
    z[21]=z[7]*z[21];
    z[30]=2*z[6];
    z[36]=z[30] + z[7];
    z[36]=z[36]*z[7];
    z[36]=z[36] + z[60];
    z[41]=z[5]*z[7];
    z[53]=z[36]*z[41];
    z[56]=npow(z[7],2);
    z[53]=3*z[53] + z[60] - z[56];
    z[53]=z[5]*z[53];
    z[56]=n<T>(321,32)*z[7];
    z[53]= - z[56] + z[53];
    z[53]=z[5]*z[53];
    z[36]= - z[7]*z[36];
    z[59]=3*z[60] + z[75];
    z[59]=z[7]*z[59];
    z[59]=z[61] + z[59];
    z[59]=z[59]*z[41];
    z[36]=z[36] + z[59];
    z[36]=z[5]*z[36];
    z[59]=z[7] + z[6];
    z[56]= - z[59]*z[56];
    z[36]=z[56] + z[36];
    z[36]=z[5]*z[36];
    z[44]=z[66] - z[44];
    z[56]=static_cast<T>(7)+ n<T>(117,32)*z[44];
    z[56]=z[7]*z[56];
    z[36]=3*z[56] + z[36];
    z[36]=z[3]*z[36];
    z[44]=static_cast<T>(11)+ 9*z[44];
    z[36]=z[36] + n<T>(39,32)*z[44] + z[53];
    z[36]=z[3]*z[36];
    z[44]=z[74]*z[6];
    z[53]=209*z[44];
    z[56]= - 455*z[11] - n<T>(1373,2)*z[13];
    z[28]=static_cast<T>(143)- 75*z[28];
    z[28]=z[8]*z[28];
    z[28]=n<T>(7,4)*z[28] - n<T>(637,4)*z[9] + n<T>(1,2)*z[56] - z[53];
    z[56]=16*z[63];
    z[60]=z[7]*z[56];
    z[60]=n<T>(1911,32)*z[62] + z[60];
    z[60]=z[7]*z[60];
    z[60]=z[60] + n<T>(5337,32)*z[13] - 4*z[4];
    z[60]=z[7]*z[60];
    z[59]=z[59]*z[41];
    z[30]= - n<T>(39,16)*z[59] + z[30] + n<T>(103,16)*z[7];
    z[30]=z[5]*z[30];
    z[24]=static_cast<T>(1209)+ 271*z[24];
    z[24]=z[30] + n<T>(1,16)*z[24] + z[60];
    z[24]=z[5]*z[24];
    z[30]=static_cast<T>(8)+ n<T>(505,32)*z[15];
    z[30]=z[4]*z[30];
    z[29]= - static_cast<T>(397)- z[29];
    z[29]=z[2]*z[29];
    z[21]=z[36] + z[24] + z[21] + n<T>(1,32)*z[29] + n<T>(1,8)*z[28] + z[30];
    z[21]=z[3]*z[21];
    z[24]=z[22]*z[49];
    z[28]=z[38]*z[9];
    z[24]= - n<T>(73,2)*z[24] - n<T>(2741,32)*z[28] + z[50] + z[56];
    z[24]=z[7]*z[24];
    z[29]=167*z[38] + 149*z[62];
    z[30]= - 3235*z[14] - z[70];
    z[30]=z[9]*z[30];
    z[29]=9*z[29] + z[30];
    z[29]=n<T>(1,16)*z[29] + 41*z[34];
    z[24]=n<T>(1,2)*z[29] + z[24];
    z[24]=z[7]*z[24];
    z[19]=n<T>(41,4)*z[23] + n<T>(115,4)*z[19] + static_cast<T>(225)+ n<T>(257,2)*z[15];
    z[19]=z[2]*z[19];
    z[23]=n<T>(1289,2)*z[14] + 1641*z[13];
    z[19]=z[19] + n<T>(395,4)*z[4] - n<T>(167,2)*z[8] + n<T>(1,2)*z[23] - 375*z[9];
    z[23]=z[52]*z[41];
    z[23]=static_cast<T>(5)- 71*z[23];
    z[23]=z[23]*z[67];
    z[19]=z[23] + n<T>(1,8)*z[19] + z[24];
    z[19]=z[5]*z[19];
    z[23]= - n<T>(2157,32)*z[38] - z[33];
    z[23]=z[6]*z[23];
    z[24]= - z[57]*z[37];
    z[24]=static_cast<T>(103)+ z[24];
    z[24]=z[8]*z[24];
    z[29]=z[11] - n<T>(437,8)*z[14];
    z[23]=n<T>(1,8)*z[24] - 31*z[9] + n<T>(1,2)*z[29] + z[23];
    z[23]=z[8]*z[23];
    z[20]= - z[42]*z[20];
    z[20]= - n<T>(515,32)*z[10] + z[20];
    z[20]=z[13]*z[20];
    z[20]= - n<T>(2335,32) + z[20];
    z[20]=z[13]*z[20];
    z[24]= - static_cast<T>(193)- n<T>(185,2)*z[15];
    z[24]=z[9]*z[24];
    z[29]=z[10] + z[43];
    z[29]=z[4]*z[29];
    z[29]= - static_cast<T>(3)+ z[29];
    z[29]=z[4]*z[29];
    z[20]=n<T>(3,8)*z[29] + n<T>(159,8)*z[8] + n<T>(3,16)*z[24] - n<T>(5,16)*z[11] + 
    z[20];
    z[20]=z[4]*z[20];
    z[24]=z[46]*z[12];
    z[29]= - n<T>(443,32) + z[24];
    z[29]=z[29]*z[62];
    z[30]= - n<T>(23,8)*z[9] + n<T>(209,4)*z[44] + 83*z[11] + n<T>(651,8)*z[14];
    z[30]=z[30]*z[55];
    z[22]=z[22]*z[6];
    z[22]=n<T>(73,4)*z[22];
    z[16]=z[16] + z[21] + z[19] + z[26] + z[17] + z[20] + z[23] + z[30]
    + z[22] - n<T>(919,32)*z[38] + z[29];
    z[16]=z[1]*z[16];
    z[17]= - z[54] + z[32];
    z[17]=z[9]*z[17];
    z[19]=z[54] + n<T>(243,2)*z[31];
    z[19]=z[4]*z[19];
    z[17]=z[19] + z[17] + z[27];
    z[17]=z[17]*z[58];
    z[19]=n<T>(327,2)*z[38];
    z[20]=137*z[74] + z[19];
    z[21]= - z[54] - z[32];
    z[21]=z[6]*z[21];
    z[20]=n<T>(1,2)*z[20] + z[21];
    z[20]=z[20]*z[68];
    z[21]= - n<T>(387,2)*z[74] - z[73];
    z[21]=z[21]*z[72];
    z[23]= - n<T>(137,2)*z[74] + z[73];
    z[26]=z[9]*z[11];
    z[23]=n<T>(1,4)*z[23] - 45*z[26];
    z[23]=z[4]*z[23];
    z[17]=z[17] + z[23] + z[21] + z[20] - z[51];
    z[17]=z[7]*z[17];
    z[20]=z[33] + n<T>(209,2)*z[74] + z[40];
    z[20]=z[6]*z[20];
    z[21]=1055*z[11];
    z[23]=z[21] + n<T>(909,2)*z[14];
    z[20]=n<T>(1,4)*z[23] + z[20];
    z[20]=z[20]*z[72];
    z[23]=n<T>(1,4)*z[38];
    z[26]=z[35]*z[14];
    z[27]= - n<T>(35,4) + z[26];
    z[27]=z[27]*z[23];
    z[29]=z[10] + z[12];
    z[30]= - z[29]*z[46];
    z[30]= - n<T>(449,16) + z[30];
    z[30]=z[30]*z[62];
    z[19]=209*z[74] - z[19];
    z[19]=z[6]*z[19];
    z[31]=73*z[11] + n<T>(123,2)*z[14];
    z[19]=n<T>(3,2)*z[31] + z[19];
    z[19]=z[9]*z[19];
    z[31]= - n<T>(73,4)*z[10] - z[48];
    z[31]=z[13]*z[31];
    z[31]= - n<T>(1215,32) + z[31];
    z[31]=z[13]*z[31];
    z[31]=n<T>(53,32)*z[9] - n<T>(209,8)*z[44] - n<T>(219,16)*z[11] + z[31];
    z[31]=z[4]*z[31];
    z[17]=z[17] + z[31] + z[20] + n<T>(1,8)*z[19] + z[22] + z[27] + z[30];
    z[17]=z[7]*z[17];
    z[19]=z[45]*z[48];
    z[20]= - z[35] + n<T>(263,4)*z[10];
    z[20]=z[10]*z[20];
    z[19]=n<T>(1,4)*z[20] + z[19];
    z[19]=z[13]*z[19];
    z[20]= - 1215*z[12] + 1367*z[10];
    z[19]=n<T>(1,32)*z[20] + z[19];
    z[19]=z[13]*z[19];
    z[20]= - n<T>(911,4) + z[26];
    z[20]=z[20]*z[38];
    z[20]=n<T>(1,2)*z[20] - z[33];
    z[20]=z[6]*z[20];
    z[22]= - n<T>(1215,4) + z[26];
    z[22]=z[14]*z[22];
    z[20]=n<T>(1,2)*z[22] + z[20];
    z[20]=z[20]*z[43];
    z[22]=n<T>(619,4)*z[38] + z[33];
    z[22]=z[6]*z[22];
    z[22]=231*z[14] + z[22];
    z[22]=z[6]*z[22];
    z[22]=n<T>(663,8) + z[22];
    z[22]=z[6]*z[22];
    z[22]= - n<T>(357,8)*z[10] + z[22];
    z[22]=z[22]*z[72];
    z[27]= - npow(z[10],4)*z[46];
    z[18]= - n<T>(263,16)*z[18] + z[27];
    z[18]=z[13]*z[18];
    z[18]= - n<T>(669,16)*z[42] + z[18];
    z[18]=z[13]*z[18];
    z[18]= - n<T>(857,32)*z[6] - n<T>(895,16)*z[10] + z[18];
    z[18]=z[4]*z[18];
    z[27]=z[14]*z[12];
    z[27]=static_cast<T>(125)+ n<T>(909,8)*z[27];
    z[18]=z[18] + z[22] + z[20] + n<T>(1,4)*z[27] + z[19];
    z[18]=z[2]*z[18];
    z[19]=z[39]*z[71];
    z[20]=n<T>(387,8)*z[74];
    z[22]= - z[8]*z[20];
    z[19]=z[22] - z[65] + z[19];
    z[19]=z[7]*z[19];
    z[22]=z[46]*z[10];
    z[27]= - n<T>(577,16) - z[22];
    z[27]=z[27]*z[62];
    z[30]=z[6]*z[39];
    z[31]=z[8]*z[11];
    z[19]=z[19] + n<T>(215,2)*z[31] + z[30] + z[20] + z[27];
    z[19]=z[7]*z[19];
    z[20]=z[13]*z[10];
    z[27]= - n<T>(1301,2) - 285*z[20];
    z[27]=z[13]*z[27];
    z[21]= - z[21] + z[27];
    z[21]= - 457*z[8] + n<T>(1,2)*z[21] - z[53];
    z[19]=n<T>(1,8)*z[21] + z[19];
    z[19]=z[7]*z[19];
    z[21]=209*z[11];
    z[27]=z[6]*z[21];
    z[20]=n<T>(491,2)*z[66] + z[27] + static_cast<T>(169)- n<T>(285,2)*z[20];
    z[27]=n<T>(577,16)*z[62] + z[79];
    z[27]=z[7]*z[27];
    z[27]=n<T>(629,8)*z[13] + z[27];
    z[27]=z[7]*z[27];
    z[27]=n<T>(153,4) + z[27];
    z[27]=z[7]*z[27];
    z[27]=n<T>(179,16)*z[6] + z[27];
    z[27]=z[5]*z[27];
    z[19]=z[27] + n<T>(1,16)*z[20] + z[19];
    z[19]=z[3]*z[19];
    z[20]= - z[50]*z[49];
    z[20]=z[20] - n<T>(619,16)*z[28] + z[51];
    z[20]=z[7]*z[20];
    z[27]=n<T>(327,4) - z[26];
    z[23]=z[27]*z[23];
    z[24]=n<T>(449,16) + z[24];
    z[24]=z[24]*z[62];
    z[27]=z[9]*z[14];
    z[20]=z[20] - n<T>(483,8)*z[27] + z[23] + z[24];
    z[20]=z[7]*z[20];
    z[23]=n<T>(1021,8) - z[26];
    z[23]=z[14]*z[23];
    z[24]=z[13]*z[35];
    z[24]=n<T>(1647,8) + z[24];
    z[24]=z[13]*z[24];
    z[23]= - n<T>(363,4)*z[9] + z[23] + z[24];
    z[20]=n<T>(1,4)*z[23] + z[20];
    z[20]=z[7]*z[20];
    z[23]=n<T>(1215,4)*z[13] - n<T>(909,4)*z[14];
    z[23]=z[12]*z[23];
    z[23]=n<T>(189,2)*z[15] - static_cast<T>(163)+ z[23];
    z[20]=n<T>(1,8)*z[23] + z[20];
    z[20]=z[5]*z[20];
    z[23]=n<T>(555,16)*z[42] + z[25];
    z[23]=z[13]*z[23];
    z[23]=n<T>(1291,16)*z[10] + z[23];
    z[23]=z[13]*z[23];
    z[24]=z[6]*z[11];
    z[15]=n<T>(63,32)*z[15] + n<T>(209,16)*z[24] + n<T>(2205,32) + z[23];
    z[15]=z[4]*z[15];
    z[22]= - z[29]*z[22];
    z[23]= - z[35] - n<T>(449,4)*z[10];
    z[22]=n<T>(1,4)*z[23] + z[22];
    z[22]=z[13]*z[22];
    z[22]= - n<T>(1647,32) + z[22];
    z[22]=z[13]*z[22];
    z[23]= - n<T>(257,4) - z[26];
    z[23]=z[23]*z[38];
    z[23]=z[23] - z[33];
    z[23]=z[6]*z[23];
    z[24]= - z[21] - n<T>(327,2)*z[14];
    z[24]=z[6]*z[24];
    z[24]= - static_cast<T>(53)+ n<T>(1,8)*z[24];
    z[24]=z[24]*z[55];
    z[25]=n<T>(911,8)*z[38] + z[33];
    z[25]=z[6]*z[25];
    z[21]= - z[21] + 1215*z[14];
    z[21]=n<T>(1,8)*z[21] + z[25];
    z[21]=z[6]*z[21];
    z[21]= - n<T>(33,4) + z[21];
    z[21]=z[21]*z[47];
    z[15]=z[16] + z[19] + z[20] + z[17] + z[18] + z[15] + z[21] + z[24]
    + n<T>(1,4)*z[23] - n<T>(951,32)*z[14] + z[22];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r936(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r936(const std::array<dd_real,31>&);
#endif
