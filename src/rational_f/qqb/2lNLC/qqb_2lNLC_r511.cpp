#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r511(const std::array<T,31>& k) {
  T z[124];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[12];
    z[9]=k[13];
    z[10]=k[15];
    z[11]=k[3];
    z[12]=k[4];
    z[13]=k[5];
    z[14]=k[19];
    z[15]=k[28];
    z[16]=k[16];
    z[17]=k[22];
    z[18]=k[25];
    z[19]=k[30];
    z[20]=k[20];
    z[21]=k[17];
    z[22]=k[29];
    z[23]=npow(z[10],2);
    z[24]=z[23]*z[13];
    z[25]=2*z[10];
    z[26]=z[24] - z[25];
    z[27]=4*z[14];
    z[28]=z[15] + z[27] - z[26];
    z[28]=z[13]*z[28];
    z[29]=2*z[12];
    z[30]=z[12]*z[21];
    z[31]= - static_cast<T>(1)+ z[30];
    z[31]=z[31]*z[29];
    z[32]=npow(z[12],2);
    z[33]=z[32]*z[3];
    z[34]=2*z[33];
    z[31]=z[34] - z[13] + z[31];
    z[31]=z[5]*z[31];
    z[35]=z[3]*z[12];
    z[36]=4*z[35];
    z[37]=6*z[30];
    z[28]=z[31] - z[36] - z[37] - n<T>(3,2) + z[28];
    z[28]=z[5]*z[28];
    z[31]=z[5]*z[13];
    z[38]=n<T>(9,4)*z[31];
    z[39]=z[11]*z[18];
    z[40]=z[8]*z[11];
    z[41]=z[3]*z[11];
    z[42]=n<T>(45,4)*z[40] - z[38] + n<T>(29,4)*z[41] + static_cast<T>(9)+ z[39];
    z[42]=z[2]*z[42];
    z[43]=29*z[13];
    z[44]=z[43] - z[29];
    z[44]=z[44]*z[6];
    z[45]=7*z[35];
    z[46]=z[5]*z[12];
    z[47]=5*z[46] + n<T>(151,2) + z[45];
    z[47]=n<T>(1,2)*z[47] - z[44];
    z[47]=z[6]*z[47];
    z[48]=npow(z[13],2);
    z[49]=n<T>(5,2)*z[48];
    z[50]=n<T>(1,2)*z[13];
    z[51]= - z[50] + z[12];
    z[51]=z[12]*z[51];
    z[51]= - z[49] + z[51];
    z[52]=n<T>(1,2)*z[6];
    z[51]=z[51]*z[52];
    z[53]=n<T>(3,4)*z[12];
    z[51]=z[51] + z[13] + z[53];
    z[51]=z[6]*z[51];
    z[54]=z[32]*z[5];
    z[55]= - z[6]*z[32];
    z[55]= - z[54] + z[55];
    z[56]=n<T>(1,2)*z[4];
    z[55]=z[55]*z[56];
    z[51]=z[55] - z[46] + z[51];
    z[51]=z[4]*z[51];
    z[24]=z[24]*z[19];
    z[55]=3*z[10];
    z[57]=z[19]*z[55];
    z[57]=z[57] + z[24];
    z[57]=z[13]*z[57];
    z[58]=4*z[19];
    z[59]=4*z[21];
    z[60]=z[59] + z[10];
    z[61]=8*z[17];
    z[62]=z[27] - z[19];
    z[62]=z[11]*z[62];
    z[62]=n<T>(35,4) + z[62];
    z[62]=z[3]*z[62];
    z[63]=z[27]*z[11];
    z[64]=n<T>(25,4) + z[63];
    z[64]=z[8]*z[64];
    z[28]=z[51] + z[47] + z[42] + z[64] + z[28] + z[62] + z[57] + z[58]
    + z[61] - 2*z[18] + z[16] + z[60];
    z[28]=z[4]*z[28];
    z[42]=npow(z[13],3);
    z[47]=z[42]*z[6];
    z[51]=z[48] - z[47];
    z[57]=n<T>(1,4)*z[6];
    z[51]=z[51]*z[57];
    z[62]=2*z[13];
    z[51]=z[51] - z[62] - z[53];
    z[51]=z[4]*z[51];
    z[53]=z[8]*z[12];
    z[64]=z[2]*z[13];
    z[65]=z[53] - z[64];
    z[66]=85*z[13];
    z[67]=z[48]*z[6];
    z[68]=z[66] - 49*z[67];
    z[68]=z[68]*z[52];
    z[69]=z[27]*z[13];
    z[70]=4*z[17];
    z[71]=z[70]*z[12];
    z[51]=z[51] + z[68] - z[71] - n<T>(53,4) + z[69] + n<T>(29,4)*z[65];
    z[51]=z[4]*z[51];
    z[65]=npow(z[11],2);
    z[68]=n<T>(1,2)*z[12];
    z[72]=z[68] + z[11];
    z[73]=z[12]*z[72];
    z[73]=n<T>(1,2)*z[65] + z[73];
    z[73]=z[3]*z[73];
    z[74]= - z[32]*z[52];
    z[75]=n<T>(1,2)*z[11];
    z[73]=z[74] + z[73] - z[75] - z[12];
    z[73]=z[9]*z[73];
    z[74]=3*z[3];
    z[76]= - z[11] - z[12];
    z[76]=z[76]*z[74];
    z[77]=n<T>(9,2)*z[13];
    z[78]= - z[77] + z[12];
    z[78]=z[12]*z[78];
    z[78]= - n<T>(13,2)*z[48] + z[78];
    z[78]=z[6]*z[78];
    z[79]=n<T>(5,2)*z[13];
    z[78]=z[78] + z[79] + 7*z[12];
    z[78]=z[78]*z[52];
    z[47]= - 5*z[48] + n<T>(17,4)*z[47];
    z[47]=z[6]*z[47];
    z[47]=z[47] + z[62] + n<T>(3,2)*z[12];
    z[47]=z[4]*z[47];
    z[47]=z[73] + z[47] + z[78] + n<T>(5,2) + z[76];
    z[47]=z[9]*z[47];
    z[73]=z[32]*z[21];
    z[73]=z[73] + z[33];
    z[76]=2*z[5];
    z[73]=z[73]*z[76];
    z[78]= - z[70] + z[10];
    z[78]=z[13]*z[78];
    z[78]=z[73] - 6*z[35] - z[37] - n<T>(21,4) + z[78];
    z[78]=z[5]*z[78];
    z[80]=z[2]*z[12];
    z[44]=z[44] - n<T>(5,2)*z[80] + n<T>(7,2)*z[46] - n<T>(41,4) + z[35];
    z[44]=z[6]*z[44];
    z[80]=z[7] - z[19];
    z[81]=z[80]*z[11];
    z[82]=static_cast<T>(1)- 2*z[81];
    z[82]=z[11]*z[82];
    z[82]=z[82] + z[29];
    z[82]=z[3]*z[82];
    z[83]=3*z[7];
    z[84]= - 5*z[19] + z[83];
    z[84]=z[11]*z[84];
    z[82]=z[82] + z[71] - n<T>(73,4) + z[84];
    z[82]=z[3]*z[82];
    z[84]=z[12]*z[20];
    z[85]= - n<T>(25,4)*z[31] - n<T>(13,4)*z[35] + static_cast<T>(9)+ z[84];
    z[85]=z[8]*z[85];
    z[86]=z[70]*z[13];
    z[87]=z[8]*z[13];
    z[88]=n<T>(45,4)*z[87] + n<T>(5,2)*z[35] + n<T>(27,2) + z[86];
    z[88]=z[2]*z[88];
    z[89]=2*z[7];
    z[90]= - z[13]*z[19];
    z[90]=z[90] + 1;
    z[90]=z[10]*z[90];
    z[91]=z[21] + z[14];
    z[44]=z[47] + z[51] + z[44] + z[88] + z[85] + z[78] + z[82] + z[89]
    + 4*z[91] + z[19] + z[90];
    z[44]=z[9]*z[44];
    z[47]= - z[17]*z[29];
    z[47]=z[64] + static_cast<T>(1)+ z[47];
    z[51]= - 5*z[13] + 21*z[67];
    z[51]=z[51]*z[57];
    z[47]=z[51] - n<T>(5,2)*z[53] + 2*z[47];
    z[47]=z[4]*z[47];
    z[51]=z[5]*z[3];
    z[78]=n<T>(1,2)*z[51];
    z[82]= - z[32]*z[78];
    z[85]=n<T>(1,2)*z[5];
    z[88]=z[85]*z[32];
    z[90]=z[6]*z[88];
    z[72]= - z[3]*z[72];
    z[72]=z[90] + z[72] + z[82];
    z[72]=z[9]*z[72];
    z[82]=n<T>(1,4)*z[35];
    z[90]=z[82] - n<T>(7,4) + z[86];
    z[90]=z[5]*z[90];
    z[36]=z[36] + z[38];
    z[36]=z[8]*z[36];
    z[38]= - n<T>(9,4)*z[87] - n<T>(1,2) - z[86];
    z[38]=z[2]*z[38];
    z[86]= - z[88] - z[62] + n<T>(1,4)*z[12];
    z[86]=z[6]*z[86];
    z[86]= - static_cast<T>(2)+ z[86];
    z[86]=z[6]*z[86];
    z[71]=n<T>(9,4) + z[71];
    z[71]=z[3]*z[71];
    z[36]=z[72] + z[47] + z[86] + z[38] + z[36] + z[71] + z[90];
    z[36]=z[9]*z[36];
    z[38]=z[13]*z[14];
    z[38]=static_cast<T>(1)+ z[38];
    z[38]=2*z[38] - z[53];
    z[47]= - z[79] - 3*z[67];
    z[47]=z[47]*z[52];
    z[38]=z[47] + 2*z[38] + n<T>(5,2)*z[64];
    z[38]=z[4]*z[38];
    z[47]=n<T>(1,4)*z[2];
    z[71]=static_cast<T>(5)+ 7*z[64];
    z[71]=z[71]*z[47];
    z[72]=z[6]*z[13];
    z[86]= - static_cast<T>(51)- 49*z[72];
    z[86]=z[86]*z[52];
    z[90]=3*z[17] + 2*z[14];
    z[53]=static_cast<T>(1)- z[53];
    z[53]=z[8]*z[53];
    z[38]=z[38] + z[86] + z[71] + 4*z[90] + n<T>(7,4)*z[53];
    z[38]=z[4]*z[38];
    z[53]=z[35]*z[76];
    z[71]=2*z[35];
    z[86]=z[71] + n<T>(15,2);
    z[86]=z[86]*z[3];
    z[53]=z[53] + z[61] - z[86];
    z[53]=z[5]*z[53];
    z[90]=n<T>(9,2) - z[87];
    z[90]=z[8]*z[90];
    z[91]=n<T>(1,2) + 2*z[87];
    z[91]=z[2]*z[91];
    z[90]=z[91] + n<T>(3,2)*z[90] + z[61] + n<T>(3,4)*z[3];
    z[90]=z[2]*z[90];
    z[91]=8*z[10];
    z[92]=n<T>(1,2)*z[3];
    z[93]= - n<T>(3,4) + z[46];
    z[93]=z[6]*z[93];
    z[93]=3*z[93] + n<T>(7,4)*z[2] - n<T>(9,4)*z[5] + z[91] - z[92];
    z[93]=z[6]*z[93];
    z[94]=z[80]*z[41];
    z[94]=z[94] + 12*z[17] + z[7];
    z[94]=z[3]*z[94];
    z[84]=n<T>(3,4)*z[35] + n<T>(3,2) + z[84];
    z[84]=z[8]*z[84];
    z[95]=n<T>(33,4) + z[35];
    z[95]=z[3]*z[95];
    z[84]=z[84] - n<T>(23,4)*z[5] + z[20] + z[95];
    z[84]=z[8]*z[84];
    z[95]=z[10]*z[14];
    z[96]=8*z[95];
    z[36]=z[36] + z[38] + z[93] + z[90] + z[84] + z[53] - z[96] + z[94];
    z[36]=z[9]*z[36];
    z[38]=5*z[2];
    z[53]=static_cast<T>(1)+ n<T>(1,4)*z[64];
    z[53]=z[53]*z[38];
    z[84]=2*z[8];
    z[72]=static_cast<T>(2)+ n<T>(7,4)*z[72];
    z[72]=z[6]*z[72];
    z[53]=z[72] + z[84] + z[53];
    z[53]=z[4]*z[53];
    z[72]= - z[87]*z[38];
    z[90]=3*z[8];
    z[72]= - z[90] + z[72];
    z[72]=z[72]*z[47];
    z[93]=npow(z[6],2);
    z[94]=n<T>(1,4)*z[93];
    z[97]=static_cast<T>(1)- z[46];
    z[97]=z[97]*z[94];
    z[98]=z[2] - z[5];
    z[98]=z[87]*z[98];
    z[99]=z[35] - 1;
    z[100]=z[5]*z[99];
    z[98]=z[3] + z[100] + z[98];
    z[98]=z[9]*z[98];
    z[100]=z[70] + n<T>(11,4)*z[8];
    z[100]=z[5]*z[100];
    z[53]=n<T>(1,2)*z[98] + z[53] + z[97] + z[72] + z[100];
    z[53]=z[9]*z[53];
    z[72]=z[6]*z[5];
    z[97]=z[8]*z[3];
    z[98]=z[3]*z[10];
    z[100]=z[2]*z[8];
    z[100]=z[72] + z[100] - z[98] - z[97];
    z[101]=n<T>(3,4)*z[6];
    z[100]=z[100]*z[101];
    z[102]=5*z[8];
    z[103]=n<T>(1,4)*z[8];
    z[104]= - z[12]*z[103];
    z[104]=static_cast<T>(1)+ z[104];
    z[104]=z[104]*z[102];
    z[105]=2*z[2];
    z[104]= - z[57] + z[104] + z[105];
    z[104]=z[4]*z[104];
    z[106]=n<T>(1,2)*z[2];
    z[107]= - n<T>(3,2) - z[64];
    z[107]=z[107]*z[106];
    z[108]=n<T>(1,4)*z[10];
    z[109]=z[108] + 8*z[8];
    z[107]=3*z[109] + z[107];
    z[107]=z[2]*z[107];
    z[109]=n<T>(1,2)*z[8];
    z[110]=z[12]*z[109];
    z[110]= - static_cast<T>(2)+ z[110];
    z[110]=z[8]*z[110];
    z[110]=n<T>(3,4)*z[5] + z[110];
    z[110]=z[8]*z[110];
    z[111]=z[5] + z[10];
    z[112]=n<T>(3,4)*z[111] + 2*z[6];
    z[112]=z[6]*z[112];
    z[104]=z[104] + z[112] + z[110] + z[107];
    z[104]=z[4]*z[104];
    z[107]=npow(z[8],2);
    z[110]=z[107]*z[35];
    z[112]=npow(z[3],2);
    z[113]= - n<T>(9,2)*z[3] - z[5];
    z[113]=z[5]*z[113];
    z[113]= - n<T>(1,2)*z[110] - z[112] + z[113];
    z[113]=z[8]*z[113];
    z[38]=z[8]*z[38];
    z[38]=z[38] - n<T>(3,4)*z[98] + 2*z[107];
    z[38]=z[2]*z[38];
    z[114]=z[112] - z[51];
    z[115]=z[114]*z[5];
    z[38]=z[53] + z[104] + z[100] + z[38] + z[115] + z[113];
    z[38]=z[9]*z[38];
    z[53]=z[2] + z[108] + z[90];
    z[53]=z[2]*z[53];
    z[100]=z[10] - z[6];
    z[100]=z[100]*z[57];
    z[53]=z[53] + z[100];
    z[53]=z[4]*z[53];
    z[100]=z[109]*z[51];
    z[104]=z[5]*z[10];
    z[72]= - z[104] - z[72];
    z[72]=z[72]*z[57];
    z[108]=z[92] - z[5];
    z[113]=z[8]*z[108];
    z[113]= - z[78] + z[113];
    z[113]=z[9]*z[113];
    z[116]=npow(z[2],2);
    z[117]=z[116]*z[8];
    z[53]=z[113] + z[53] + z[72] + z[100] - z[117];
    z[53]=z[9]*z[53];
    z[72]=z[3]*z[7];
    z[113]=z[8] + z[7];
    z[118]=z[2]*z[113];
    z[118]=z[118] - z[97] - z[72] - 5*z[104];
    z[118]=z[6]*z[118];
    z[113]= - z[113]*z[116];
    z[119]=z[23]*z[3];
    z[113]=z[118] + z[119] + z[113];
    z[113]=z[113]*z[57];
    z[118]=n<T>(1,4)*z[23];
    z[120]=z[90]*z[2];
    z[121]= - z[120] - z[118] - 3*z[107];
    z[121]=z[2]*z[121];
    z[122]=n<T>(1,4)*z[5] + z[8];
    z[122]=z[8]*z[122];
    z[123]=z[5]*z[57];
    z[120]=z[123] + z[122] + z[120];
    z[120]=z[4]*z[120];
    z[111]=z[6]*z[111];
    z[111]= - z[23] + z[111];
    z[111]=z[111]*z[57];
    z[111]=z[120] + z[121] + z[111];
    z[111]=z[4]*z[111];
    z[120]=z[5] - z[3];
    z[121]= - z[8]*z[120];
    z[121]= - z[51] + z[121];
    z[121]=z[121]*z[109];
    z[121]=z[115] + z[121];
    z[121]=z[8]*z[121];
    z[47]=z[119]*z[47];
    z[47]=z[53] + z[111] + z[113] + z[121] + z[47];
    z[47]=z[9]*z[47];
    z[53]=z[6]*z[3];
    z[111]= - z[2]*z[120];
    z[111]= - z[53] - z[51] + z[111];
    z[56]=z[111]*z[56];
    z[111]=z[5]*z[7];
    z[113]= - z[103]*z[111];
    z[121]=z[78] - z[107];
    z[121]=z[2]*z[121];
    z[72]=z[72] - z[111];
    z[72]=n<T>(1,4)*z[72] + z[53];
    z[72]=z[6]*z[72];
    z[56]=z[56] + z[72] + z[113] + z[121];
    z[56]=z[4]*z[56];
    z[72]=z[118] + z[114];
    z[72]=z[5]*z[72];
    z[113]=z[8]*z[10];
    z[114]=z[113] + z[23];
    z[118]=z[114]*z[103];
    z[108]=z[2]*z[108];
    z[108]= - z[78] + z[108];
    z[108]=z[2]*z[108];
    z[72]=z[108] + z[118] - n<T>(1,4)*z[119] + z[72];
    z[72]=z[2]*z[72];
    z[108]=z[23]*z[5];
    z[118]= - z[103]*z[108];
    z[121]=5*z[7];
    z[122]=z[121] + z[10];
    z[123]=z[3]*z[122];
    z[123]=z[123] - z[111];
    z[123]=z[6]*z[123];
    z[119]= - z[119] + z[123];
    z[57]=z[119]*z[57];
    z[56]=z[56] + z[57] + z[118] + z[72];
    z[56]=z[4]*z[56];
    z[57]= - z[8]*z[114];
    z[57]= - z[108] + z[57];
    z[72]= - z[7]*z[85];
    z[114]= - z[2]*z[5];
    z[72]=z[72] + z[114];
    z[72]=z[2]*z[72];
    z[57]=n<T>(1,2)*z[57] + z[72];
    z[57]=z[2]*z[57];
    z[72]=z[8]*z[98];
    z[72]=z[108] + z[72];
    z[72]=z[72]*z[109];
    z[57]=z[72] + z[57];
    z[72]= - 5*z[98] + z[97];
    z[72]=z[72]*z[103];
    z[97]=z[5]*z[122];
    z[97]=z[97] + z[113];
    z[108]= - z[5] + z[103];
    z[108]=z[2]*z[108];
    z[97]=n<T>(1,4)*z[97] + z[108];
    z[97]=z[2]*z[97];
    z[72]=z[72] + z[97];
    z[72]=z[6]*z[72];
    z[57]=n<T>(1,2)*z[57] + z[72];
    z[57]=z[6]*z[57];
    z[72]= - z[107]*z[98];
    z[97]=z[116]*z[111];
    z[72]=z[72] + z[97];
    z[72]=z[72]*z[93];
    z[97]=npow(z[2],3);
    z[108]= - z[97]*z[51];
    z[93]= - z[7]*z[93]*z[92];
    z[111]= - z[4]*z[2]*z[51];
    z[93]=z[93] + z[111];
    z[93]=z[4]*z[93];
    z[93]=z[108] + z[93];
    z[93]=z[4]*z[93];
    z[72]=n<T>(1,2)*z[72] + z[93];
    z[93]=z[104]*z[94];
    z[94]= - z[9]*z[100];
    z[100]=z[4]*z[117];
    z[93]=z[94] + z[93] + z[100];
    z[93]=z[9]*z[93];
    z[94]=npow(z[8],3);
    z[78]= - z[94]*z[78];
    z[100]=z[2]*z[107]*npow(z[4],2);
    z[78]=z[93] + z[78] + z[100];
    z[78]=z[9]*z[78];
    z[72]=n<T>(1,2)*z[72] + z[78];
    z[72]=z[1]*z[72];
    z[78]=z[94] + z[97];
    z[78]=z[51]*z[78];
    z[47]=z[72] + z[47] + z[56] + n<T>(1,2)*z[78] + z[57];
    z[47]=z[1]*z[47];
    z[56]=z[41] + z[40];
    z[56]=z[2]*z[56];
    z[57]=z[35] + 1;
    z[72]= - z[5]*z[57];
    z[56]=z[56] + z[74] + z[72];
    z[72]= - n<T>(1,2) + z[35];
    z[72]=z[6]*z[72];
    z[56]=n<T>(1,2)*z[56] + z[72];
    z[56]=z[4]*z[56];
    z[72]= - z[11]*z[109];
    z[72]= - static_cast<T>(1)+ z[72];
    z[72]=z[8]*z[72];
    z[72]=n<T>(5,2)*z[72] - z[92] + 4*z[5];
    z[72]=z[2]*z[72];
    z[78]=z[5]*z[27];
    z[93]=static_cast<T>(2)- n<T>(5,4)*z[35];
    z[93]=z[6]*z[93];
    z[93]= - n<T>(5,4)*z[3] + z[93];
    z[93]=z[6]*z[93];
    z[56]=z[56] + z[93] + z[72] + z[78] + n<T>(5,4)*z[107];
    z[56]=z[4]*z[56];
    z[64]= - z[85]*z[64];
    z[72]=n<T>(5,4)*z[5];
    z[64]=z[64] + z[72] + z[84];
    z[64]=z[2]*z[64];
    z[78]= - z[55] + 25*z[8];
    z[78]=z[78]*z[103];
    z[93]=n<T>(3,4)*z[10] - z[3];
    z[93]=z[3]*z[93];
    z[94]= - n<T>(1,2)*z[10] - z[74];
    z[94]=n<T>(3,2)*z[94] - z[5];
    z[94]=z[5]*z[94];
    z[64]=z[64] + z[78] + z[93] + z[94];
    z[64]=z[2]*z[64];
    z[78]=z[15] - z[3];
    z[78]=z[5]*z[78];
    z[78]=z[112] + z[78];
    z[78]=z[5]*z[78];
    z[93]=n<T>(3,2)*z[104] - z[107];
    z[93]=z[93]*z[109];
    z[53]=z[98] - 5*z[53];
    z[53]=z[53]*z[101];
    z[53]=z[56] + z[53] + z[64] + z[78] + z[93];
    z[53]=z[4]*z[53];
    z[56]=z[10] + z[7];
    z[64]=z[120]*z[56];
    z[78]=z[8]*z[35];
    z[78]=z[74] + z[78];
    z[78]=z[78]*z[103];
    z[93]= - n<T>(7,4)*z[87] + static_cast<T>(1)+ n<T>(5,4)*z[46];
    z[93]=z[2]*z[93];
    z[93]=z[93] - n<T>(15,4)*z[5] + z[84];
    z[93]=z[2]*z[93];
    z[78]=z[93] + 2*z[64] + z[78];
    z[78]=z[6]*z[78];
    z[93]=n<T>(1,2) + z[46];
    z[93]=z[2]*z[93];
    z[93]=z[93] + z[72] - z[84];
    z[93]=z[2]*z[93];
    z[94]=z[5]*z[55];
    z[97]=z[55] + z[8];
    z[97]=z[8]*z[97];
    z[94]=z[94] + z[97];
    z[93]=n<T>(1,4)*z[94] + z[93];
    z[93]=z[2]*z[93];
    z[94]=z[8]*z[104];
    z[78]=z[78] - n<T>(3,4)*z[94] + z[93];
    z[78]=z[6]*z[78];
    z[93]= - z[16] + z[3];
    z[93]=z[5]*z[93];
    z[93]= - z[112] + z[93];
    z[93]=z[5]*z[93];
    z[94]= - z[85]*z[110];
    z[93]=z[93] + z[94];
    z[93]=z[8]*z[93];
    z[94]=static_cast<T>(3)+ z[35];
    z[94]=z[5]*z[94];
    z[94]= - z[3] + z[94];
    z[94]=z[94]*z[116];
    z[94]= - z[115] + n<T>(1,2)*z[94];
    z[94]=z[2]*z[94];
    z[38]=z[47] + z[38] + z[53] + z[78] + z[93] + z[94];
    z[38]=z[1]*z[38];
    z[39]= - n<T>(9,4)*z[40] + 4*z[31] - n<T>(13,4)*z[41] - n<T>(3,2) - z[39];
    z[39]=z[2]*z[39];
    z[40]=z[33] + z[12];
    z[47]= - z[40]*z[85];
    z[53]=z[92]*z[32];
    z[78]=z[12] - z[53];
    z[78]=z[6]*z[78];
    z[47]=z[47] + z[78];
    z[47]=z[4]*z[47];
    z[78]= - z[8] - z[3];
    z[63]=z[63] - n<T>(3,2);
    z[63]=z[63]*z[78];
    z[69]= - z[82] - n<T>(1,2) + z[69];
    z[69]=z[5]*z[69];
    z[78]=z[50] + z[12];
    z[82]= - 5*z[78] + z[33];
    z[52]=z[82]*z[52];
    z[52]=z[52] - n<T>(5,2) + z[35];
    z[52]=z[6]*z[52];
    z[39]=z[47] + z[52] + z[39] + z[69] - z[18] + z[63];
    z[39]=z[4]*z[39];
    z[47]=z[13]*z[15];
    z[47]= - z[71] - static_cast<T>(2)+ z[47];
    z[47]=z[5]*z[47];
    z[52]=z[71] - n<T>(7,2);
    z[52]=z[52]*z[3];
    z[63]=8*z[14];
    z[47]=z[47] + z[52] - z[63] + z[10];
    z[47]=z[5]*z[47];
    z[69]= - z[11]*z[90];
    z[31]=n<T>(7,2)*z[31] + z[69];
    z[31]=z[31]*z[106];
    z[69]= - n<T>(1,2) - z[41];
    z[69]=z[3]*z[69];
    z[82]=z[11]*z[84];
    z[82]=static_cast<T>(11)+ z[82];
    z[82]=z[8]*z[82];
    z[31]=z[31] + z[82] + z[69] - n<T>(55,4)*z[5];
    z[31]=z[2]*z[31];
    z[69]=12*z[14];
    z[74]= - z[74] - z[69] + z[19];
    z[74]=z[3]*z[74];
    z[82]= - n<T>(7,4)*z[5] + 8*z[7];
    z[92]=3*z[35];
    z[93]= - n<T>(133,4) + z[92];
    z[93]=z[6]*z[93];
    z[93]=z[93] + n<T>(7,4)*z[3] + z[82];
    z[93]=z[6]*z[93];
    z[61]=z[61]*z[7];
    z[69]=n<T>(9,4)*z[8] + z[69] - n<T>(5,2)*z[5];
    z[69]=z[8]*z[69];
    z[31]=z[39] + z[93] + z[31] + z[69] + z[47] - z[61] + z[74];
    z[31]=z[4]*z[31];
    z[39]=z[32]*z[51];
    z[39]= - z[35] + z[39];
    z[39]=z[39]*z[106];
    z[47]= - static_cast<T>(5)- n<T>(1,2)*z[35];
    z[47]=z[5]*z[47];
    z[47]=z[3] + z[47];
    z[39]=z[39] + n<T>(1,2)*z[47] - z[84];
    z[39]=z[2]*z[39];
    z[47]=z[57]*z[76];
    z[47]= - z[52] + z[47];
    z[47]=z[5]*z[47];
    z[51]=z[10] + z[3];
    z[51]=z[3]*z[51];
    z[39]=z[39] - n<T>(21,4)*z[107] + z[47] + z[61] + z[51];
    z[39]=z[2]*z[39];
    z[47]=z[8]*z[33];
    z[47]= - n<T>(5,2) + z[47];
    z[47]=z[47]*z[109];
    z[47]=z[47] + z[72] - z[91] - n<T>(9,4)*z[3];
    z[47]=z[8]*z[47];
    z[51]= - z[12] - z[88];
    z[51]=z[2]*z[51];
    z[51]=z[51] + n<T>(5,4)*z[87] - n<T>(3,4) - z[46];
    z[51]=z[2]*z[51];
    z[52]= - static_cast<T>(51)+ n<T>(5,2)*z[87];
    z[52]=z[52]*z[109];
    z[51]=z[51] + z[52] - z[82];
    z[51]=z[2]*z[51];
    z[52]=17*z[13];
    z[57]= - z[52] - 5*z[12];
    z[61]=z[8]*z[48];
    z[57]= - n<T>(21,2)*z[61] + n<T>(1,2)*z[57] - z[54];
    z[57]=z[57]*z[106];
    z[61]=z[48]*z[90];
    z[61]=z[61] - 49*z[13];
    z[61]=z[61]*z[109];
    z[69]=3*z[46];
    z[57]=z[57] + z[61] + n<T>(17,4) + z[69];
    z[57]=z[2]*z[57];
    z[61]=z[12] - z[13];
    z[72]=3*z[61] + z[33];
    z[72]=z[72]*z[109];
    z[72]=z[72] + n<T>(125,4) + z[92];
    z[72]=z[8]*z[72];
    z[57]=z[57] - 6*z[120] + z[72];
    z[57]=z[6]*z[57];
    z[47]=z[57] + z[51] + 8*z[64] + z[47];
    z[47]=z[6]*z[47];
    z[51]= - z[13]*z[16];
    z[51]=z[51] - z[71];
    z[51]=z[5]*z[51];
    z[51]=z[51] - z[7] + z[86];
    z[51]=z[5]*z[51];
    z[57]=z[99]*z[85];
    z[40]=z[5]*z[40];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[8]*z[40];
    z[40]=z[57] + z[40];
    z[40]=z[40]*z[109];
    z[40]=z[40] + z[96] + z[51];
    z[40]=z[8]*z[40];
    z[51]=z[7]*z[17];
    z[51]=z[51] + z[95];
    z[51]=8*z[51];
    z[57]=z[80]*z[3];
    z[64]=z[51] - z[57];
    z[64]=z[3]*z[64];
    z[72]=z[15] + z[16];
    z[74]= - z[5]*z[72];
    z[51]= - z[51] + z[74];
    z[51]=z[5]*z[51];
    z[31]=z[38] + z[36] + z[31] + z[47] + z[39] + z[40] + z[64] + z[51];
    z[31]=z[1]*z[31];
    z[36]= - static_cast<T>(1)- z[30];
    z[36]=z[36]*z[29];
    z[38]=z[7] + z[22];
    z[39]=z[38]*z[13];
    z[40]= - static_cast<T>(1)- z[39];
    z[40]=z[13]*z[40];
    z[36]= - z[34] + z[40] + z[36];
    z[36]=z[5]*z[36];
    z[40]=3*z[22];
    z[47]= - z[121] - z[16] - z[40];
    z[47]=z[13]*z[47];
    z[36]=z[36] - z[37] + static_cast<T>(8)+ z[47];
    z[36]=z[5]*z[36];
    z[47]=n<T>(1,2)*z[48];
    z[51]=z[13] + z[68];
    z[51]=z[12]*z[51];
    z[51]=z[47] + z[51];
    z[51]=z[5]*z[51];
    z[50]= - z[50] + z[51];
    z[50]=z[8]*z[50];
    z[51]=z[12] + z[13];
    z[64]=z[5]*z[51];
    z[50]=z[50] - static_cast<T>(1)+ z[64];
    z[50]=z[8]*z[50];
    z[64]=3*z[19];
    z[68]= - z[20] - z[22];
    z[74]=z[12]*z[10];
    z[82]=z[22]*z[74];
    z[74]= - static_cast<T>(1)- z[74];
    z[74]=z[3]*z[74];
    z[36]=z[50] + z[36] + z[74] + z[82] - z[64] - z[63] + 3*z[68] - 
    z[60];
    z[36]=z[8]*z[36];
    z[50]=z[13]*z[22];
    z[37]= - z[73] - z[71] - z[37] + n<T>(43,4) + z[50];
    z[37]=z[5]*z[37];
    z[50]= - z[2]*z[53];
    z[53]=n<T>(3,2)*z[11] - 4*z[13];
    z[53]=z[8]*z[53];
    z[35]=z[50] + z[53] - static_cast<T>(2)+ z[35];
    z[35]=z[2]*z[35];
    z[50]=z[23]*z[12];
    z[53]=z[50]*z[22];
    z[63]= - z[22]*z[55];
    z[63]=z[63] + z[53];
    z[63]=z[12]*z[63];
    z[68]=z[50] + 4*z[10];
    z[68]=z[68]*z[12];
    z[71]=z[11] + z[29];
    z[71]=z[3]*z[71];
    z[71]=z[71] + n<T>(9,2) - z[68];
    z[71]=z[3]*z[71];
    z[74]= - 3*z[11] + 7*z[13];
    z[74]=z[8]*z[74];
    z[74]= - static_cast<T>(93)+ z[74];
    z[74]=z[74]*z[103];
    z[35]=z[35] + z[74] + z[37] + z[71] + z[63] - 7*z[10] - z[7] - z[70]
    - z[59] - z[18] + z[40];
    z[35]=z[2]*z[35];
    z[37]=z[43] + z[29];
    z[43]=z[42]*z[109];
    z[43]= - 49*z[48] + z[43];
    z[43]=z[43]*z[109];
    z[63]=z[42]*z[8];
    z[71]=z[63] + z[48];
    z[74]=z[77] + z[12];
    z[74]=z[12]*z[74];
    z[71]=z[74] - n<T>(17,2)*z[71];
    z[71]=z[71]*z[106];
    z[43]=z[71] + z[43] - z[37];
    z[43]=z[2]*z[43];
    z[71]=z[12]*z[78];
    z[71]= - z[47] + z[71];
    z[71]=z[71]*z[109];
    z[37]=z[71] + z[37];
    z[37]=z[8]*z[37];
    z[37]=z[37] + z[43];
    z[37]=z[6]*z[37];
    z[43]=z[13] + 9*z[12];
    z[71]=z[8]*z[32];
    z[43]=n<T>(1,2)*z[43] + z[71];
    z[43]=z[8]*z[43];
    z[43]=z[43] - z[69] - n<T>(143,2) + z[45];
    z[43]=z[43]*z[109];
    z[45]=z[48]*z[102];
    z[69]=z[32]*z[106];
    z[71]=9*z[13] - z[12];
    z[45]=z[69] + n<T>(1,2)*z[71] + z[45];
    z[45]=z[2]*z[45];
    z[69]= - z[48]*z[109];
    z[66]=z[66] + z[69];
    z[66]=z[8]*z[66];
    z[46]=z[66] + n<T>(33,2) + 7*z[46];
    z[45]=n<T>(1,2)*z[46] + z[45];
    z[45]=z[2]*z[45];
    z[37]=z[37] + z[45] - 10*z[120] + z[43];
    z[37]=z[6]*z[37];
    z[27]= - z[27] - z[70] - z[56] + z[72];
    z[27]=z[27]*z[76];
    z[41]=2*z[41] - 1;
    z[41]=z[80]*z[41];
    z[43]=z[17] + z[14];
    z[41]=z[25] + 8*z[43] + z[41];
    z[41]=z[3]*z[41];
    z[43]=z[7]*z[20];
    z[45]= - z[18] - z[15];
    z[45]=z[10]*z[45];
    z[27]=z[31] + z[44] + z[28] + z[37] + z[35] + z[36] + z[27] + z[41]
    + z[43] + z[45];
    z[27]=z[1]*z[27];
    z[28]=z[48]*z[12];
    z[31]=z[28] + z[42];
    z[31]=z[31]*z[6];
    z[35]=z[61]*z[12];
    z[36]= - z[31] + z[47] - z[35];
    z[36]=z[6]*z[36];
    z[37]=z[12]*z[13];
    z[41]=z[48] + z[37];
    z[43]=npow(z[13],4);
    z[44]=z[43]*z[6];
    z[45]= - n<T>(3,2)*z[42] + z[44];
    z[45]=z[6]*z[45];
    z[41]=n<T>(1,2)*z[41] + z[45];
    z[41]=z[4]*z[41];
    z[45]=2*z[11] + z[12];
    z[45]=z[12]*z[45];
    z[45]=z[65] + z[45];
    z[45]=z[3]*z[45];
    z[46]= - z[11] + z[13];
    z[36]=z[41] + z[36] + z[45] + n<T>(1,2)*z[46] - z[29];
    z[36]=z[9]*z[36];
    z[41]= - n<T>(5,2)*z[42] + z[44];
    z[41]=z[6]*z[41];
    z[44]=n<T>(3,2)*z[48];
    z[37]=z[41] + z[44] + z[37];
    z[37]=z[4]*z[37];
    z[37]=z[37] + 18*z[67] - n<T>(33,2)*z[13] - 4*z[12];
    z[37]=z[4]*z[37];
    z[41]=z[40] + z[58];
    z[41]=z[13]*z[41];
    z[40]=z[40] + z[59];
    z[40]=z[12]*z[40];
    z[45]=static_cast<T>(18)- 5*z[81];
    z[45]=z[11]*z[45];
    z[46]=npow(z[11],3)*z[57];
    z[45]=z[46] + z[45] + 13*z[12];
    z[45]=z[3]*z[45];
    z[46]=z[32]*z[105];
    z[46]= - z[52] + z[46];
    z[46]=z[6]*z[46];
    z[47]= - 8*z[19] + z[121];
    z[47]=z[11]*z[47];
    z[34]= - z[2]*z[34];
    z[34]=z[36] + z[37] + z[46] + z[34] - z[73] + z[45] + z[40] + z[41]
    - static_cast<T>(12)+ z[47];
    z[34]=z[9]*z[34];
    z[36]=z[22]*z[25];
    z[36]=z[36] - z[53];
    z[36]=z[12]*z[36];
    z[36]=z[36] + 5*z[10] - z[22] + z[59];
    z[36]=z[12]*z[36];
    z[37]=z[55] + z[50];
    z[37]=z[12]*z[37];
    z[37]=static_cast<T>(7)+ z[37];
    z[37]=z[12]*z[37];
    z[37]= - 10*z[11] + z[37];
    z[37]=z[3]*z[37];
    z[40]=z[11] - z[79];
    z[40]=z[40]*z[87];
    z[40]=z[40] + z[11] - 15*z[13];
    z[40]=z[8]*z[40];
    z[41]=z[87] + 1;
    z[45]=z[75] - z[13];
    z[41]=z[45]*z[41];
    z[33]=z[33] + z[41];
    z[33]=z[2]*z[33];
    z[33]=z[33] + z[40] + z[73] + z[37] + static_cast<T>(8)+ z[36];
    z[33]=z[2]*z[33];
    z[36]=static_cast<T>(9)- z[39];
    z[36]=z[36]*z[62];
    z[37]=z[38]*z[5];
    z[38]= - z[42]*z[37];
    z[29]=z[29]*z[21];
    z[39]=static_cast<T>(13)- z[29];
    z[39]=z[12]*z[39];
    z[36]=z[38] + z[36] + z[39];
    z[36]=z[5]*z[36];
    z[38]= - z[62] - z[12];
    z[38]=z[12]*z[38];
    z[38]= - z[48] + z[38];
    z[38]=z[5]*z[38];
    z[38]=3*z[13] + z[38];
    z[38]=z[8]*z[38];
    z[39]=z[64] - z[7];
    z[39]=z[11]*z[39];
    z[40]=z[83] - z[22] - 6*z[19];
    z[40]=z[13]*z[40];
    z[30]=z[38] + z[36] - 4*z[30] + z[40] + static_cast<T>(10)+ z[39];
    z[30]=z[8]*z[30];
    z[32]=z[76]*z[32];
    z[32]=z[32] - z[52];
    z[35]= - z[49] - z[35];
    z[35]=z[8]*z[35];
    z[35]=z[35] + z[32];
    z[35]=z[8]*z[35];
    z[36]=z[51]*z[12];
    z[38]=n<T>(3,2)*z[63] + z[44] - z[36];
    z[38]=z[2]*z[38];
    z[39]=18*z[48] + n<T>(5,2)*z[63];
    z[39]=z[8]*z[39];
    z[38]=z[38] + z[52] + z[39];
    z[38]=z[2]*z[38];
    z[39]= - z[43]*z[107];
    z[28]=z[28] - z[42];
    z[40]= - z[8]*z[43];
    z[40]=z[40] + z[28];
    z[40]=z[2]*z[40];
    z[39]=z[39] + z[40];
    z[39]=z[2]*z[39];
    z[28]= - z[28]*z[107];
    z[28]=z[28] + z[39];
    z[28]=z[6]*z[28];
    z[28]=z[28] + z[35] + z[38];
    z[28]=z[6]*z[28];
    z[31]=z[31] - z[44] - z[36];
    z[31]=z[6]*z[31];
    z[31]= - z[54] + z[31];
    z[31]=z[4]*z[31];
    z[35]=z[19]*z[25];
    z[35]=z[35] + z[24];
    z[35]=z[13]*z[35];
    z[35]=z[35] + z[19] - z[55];
    z[35]=z[13]*z[35];
    z[23]= - z[23]*z[48];
    z[23]= - static_cast<T>(9)+ z[23];
    z[23]=z[13]*z[23];
    z[29]=static_cast<T>(5)+ z[29];
    z[29]=z[12]*z[29];
    z[23]=z[23] + z[29];
    z[23]=z[5]*z[23];
    z[29]= - z[6]*z[32];
    z[32]= - z[12]*z[60];
    z[23]=z[31] + z[29] + z[23] + z[32] - static_cast<T>(12)+ z[35];
    z[23]=z[4]*z[23];
    z[29]= - z[65]*z[57];
    z[31]= - z[19] + z[83];
    z[31]=z[11]*z[31];
    z[29]=z[29] - z[68] - static_cast<T>(27)+ z[31];
    z[29]=z[3]*z[29];
    z[26]=z[22] - z[7] + z[26];
    z[26]=z[13]*z[26];
    z[31]= - z[48]*z[37];
    z[26]=z[31] + static_cast<T>(24)+ z[26];
    z[26]=z[5]*z[26];
    z[31]=z[10]*z[19];
    z[24]=z[31] - z[24];
    z[24]=z[13]*z[24];
    z[31]=z[10]*z[22];
    z[31]=z[31] + z[53];
    z[31]=z[12]*z[31];
    z[23]=z[27] + z[34] + z[23] + z[28] + z[33] + z[30] + z[26] + z[29]
    + z[31] + z[24] + z[25] + z[89] + z[20] - z[18] - z[72];

    r += z[23]*z[1];
 
    return r;
}

template double qqb_2lNLC_r511(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r511(const std::array<dd_real,31>&);
#endif
