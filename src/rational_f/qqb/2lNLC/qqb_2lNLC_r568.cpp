#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r568(const std::array<T,31>& k) {
  T z[147];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[8];
    z[4]=k[9];
    z[5]=k[14];
    z[6]=k[13];
    z[7]=k[15];
    z[8]=k[10];
    z[9]=k[11];
    z[10]=k[12];
    z[11]=k[4];
    z[12]=k[5];
    z[13]=k[18];
    z[14]=k[30];
    z[15]=k[2];
    z[16]=k[3];
    z[17]=k[23];
    z[18]=k[17];
    z[19]=k[20];
    z[20]=k[24];
    z[21]=npow(z[13],2);
    z[22]=4*z[21];
    z[23]=z[22]*z[11];
    z[24]=7*z[13];
    z[25]=z[24] - z[23];
    z[25]=z[11]*z[25];
    z[26]=npow(z[11],2);
    z[27]=z[26]*z[13];
    z[28]=z[26]*z[9];
    z[29]=z[27] - z[28];
    z[30]=2*z[7];
    z[31]=z[29]*z[30];
    z[32]=z[9]*z[11];
    z[33]=z[8]*z[11];
    z[25]=z[31] - 15*z[32] - 7*z[33] - static_cast<T>(38)+ z[25];
    z[25]=z[7]*z[25];
    z[31]=z[5]*z[11];
    z[34]=z[19] - z[18];
    z[34]=z[11]*z[34];
    z[34]=z[31] + n<T>(5,2)*z[33] + n<T>(61,2) + z[34];
    z[34]=z[5]*z[34];
    z[35]=16*z[21];
    z[36]=npow(z[3],2);
    z[37]= - z[35] - z[36];
    z[37]=z[11]*z[37];
    z[38]=z[4]*z[16];
    z[39]=z[38] - 1;
    z[40]=z[16]*z[17];
    z[41]=z[39] - z[40];
    z[42]=2*z[31] - z[41];
    z[42]=z[10]*z[42];
    z[43]=2*z[18];
    z[44]=23*z[13];
    z[45]=2*z[2];
    z[46]=3*z[8];
    z[47]=6*z[3];
    z[48]=npow(z[17],2);
    z[49]=z[48]*z[16];
    z[50]=z[2]*z[16];
    z[51]=5*z[50];
    z[52]= - static_cast<T>(13)+ z[51];
    z[52]=z[9]*z[52];
    z[25]=z[42] + z[34] + z[25] + z[52] + z[46] - z[4] + z[37] - z[45]
    + z[49] + z[47] + z[44] - 11*z[17] - 3*z[19] - z[43];
    z[25]=z[10]*z[25];
    z[34]=2*z[10];
    z[37]= - z[8] + z[5];
    z[37]=z[37]*z[34];
    z[42]=z[9]*z[2];
    z[52]=n<T>(3,4)*z[42];
    z[53]=npow(z[2],2);
    z[54]=3*z[5];
    z[55]= - z[54] + 8*z[8] - n<T>(3,4)*z[9];
    z[55]=z[5]*z[55];
    z[37]=z[37] + z[55] - z[53] + z[52];
    z[37]=z[10]*z[37];
    z[55]=z[8]*z[4];
    z[56]=npow(z[4],2);
    z[57]= - z[56] + z[55];
    z[58]= - static_cast<T>(1)+ z[33];
    z[58]=z[5]*z[58];
    z[58]= - z[8] + z[58];
    z[58]=z[5]*z[58];
    z[57]=n<T>(5,2)*z[58] + n<T>(1,2)*z[57] + z[42];
    z[58]=6*z[33];
    z[59]= - z[32] - static_cast<T>(5)+ z[58];
    z[59]=z[10]*z[59];
    z[60]=n<T>(1,2)*z[9];
    z[58]=n<T>(11,2) - z[58];
    z[58]=z[5]*z[58];
    z[58]=z[59] - z[60] + z[58];
    z[58]=z[6]*z[58];
    z[59]=n<T>(1,2)*z[2];
    z[61]=z[59] + n<T>(23,2)*z[5];
    z[62]=2*z[8];
    z[63]= - z[10] + z[9] + z[62] - z[61];
    z[63]=z[10]*z[63];
    z[57]=z[58] + n<T>(1,2)*z[57] + z[63];
    z[57]=z[6]*z[57];
    z[58]=2*z[9];
    z[63]=2*z[13];
    z[64]= - z[58] + n<T>(19,4)*z[8] - z[63] + n<T>(3,4)*z[4];
    z[64]=z[7]*z[64];
    z[65]=5*z[8];
    z[66]=z[5]*z[33];
    z[66]= - z[65] + z[66];
    z[67]=n<T>(1,2)*z[5];
    z[66]=z[66]*z[67];
    z[64]=z[64] + z[66];
    z[64]=z[5]*z[64];
    z[66]=z[56]*z[8];
    z[68]=z[53]*z[9];
    z[69]=z[55]*z[7];
    z[70]=z[56]*z[2];
    z[37]=z[57] + z[37] + z[64] - n<T>(3,4)*z[69] + z[68] - z[70] + n<T>(1,4)*
    z[66];
    z[37]=z[6]*z[37];
    z[57]=z[4]*z[2];
    z[64]=z[2]*z[3];
    z[71]= - z[64] - z[57];
    z[72]=n<T>(1,2)*z[4];
    z[71]=z[71]*z[72];
    z[73]=z[8]*z[2];
    z[74]=z[3]*z[72];
    z[74]=z[74] - z[73];
    z[74]=z[9]*z[74];
    z[75]=3*z[2];
    z[76]=z[75] + z[8];
    z[76]=z[9]*z[76];
    z[76]= - z[73] + z[76];
    z[76]=z[5]*z[76];
    z[71]=z[76] + z[71] + z[74];
    z[71]=z[71]*z[67];
    z[74]=6*z[8];
    z[76]=z[74]*z[53];
    z[77]=z[5]*z[4];
    z[78]=z[56] + z[77];
    z[78]=z[10]*z[78];
    z[79]=z[56]*z[3];
    z[78]=n<T>(1,4)*z[78] + n<T>(1,4)*z[79] + z[76];
    z[78]=z[9]*z[78];
    z[80]=z[64]*z[56];
    z[81]=z[8] + z[4];
    z[82]=npow(z[7],2);
    z[81]=z[81]*z[82];
    z[83]=n<T>(1,4)*z[9];
    z[84]=z[83]*z[81];
    z[71]=z[71] + z[84] - n<T>(5,4)*z[80] + z[78];
    z[71]=z[5]*z[71];
    z[61]= - z[60] - z[74] + z[61];
    z[61]=z[10]*z[61];
    z[78]=n<T>(1,2)*z[42];
    z[84]=z[5]*z[8];
    z[85]= - z[78] + 6*z[84];
    z[61]=z[61] + z[85];
    z[61]=z[6]*z[61];
    z[86]=z[57] - z[55];
    z[86]=z[7]*z[86];
    z[86]=z[70] + z[86];
    z[87]=n<T>(1,4)*z[7];
    z[88]=z[8]*z[87];
    z[88]=z[88] + z[84];
    z[88]=z[5]*z[88];
    z[89]=z[10] - 9*z[8] - n<T>(1,4)*z[5];
    z[89]=z[5]*z[89];
    z[78]=z[78] + z[89];
    z[78]=z[10]*z[78];
    z[61]=z[61] + z[78] + n<T>(1,4)*z[86] + z[88];
    z[61]=z[6]*z[61];
    z[66]=z[66] - z[69];
    z[69]=5*z[70] - z[66];
    z[69]=z[7]*z[69];
    z[78]=z[5]*z[9];
    z[86]= - z[3]*z[78];
    z[81]= - z[81] + z[86];
    z[81]=z[5]*z[81];
    z[69]=z[69] + z[81];
    z[81]=z[84]*z[34];
    z[46]= - z[46] - z[83];
    z[86]=npow(z[5],2);
    z[46]=z[46]*z[86];
    z[46]=z[81] + z[68] + z[46];
    z[46]=z[10]*z[46];
    z[46]=z[61] + n<T>(1,4)*z[69] + z[46];
    z[46]=z[6]*z[46];
    z[61]=n<T>(1,2)*z[56];
    z[69]=z[64]*z[61];
    z[84]=z[42]*z[84];
    z[69]=z[69] + z[84];
    z[69]=z[69]*z[86];
    z[84]=z[6]*z[85];
    z[85]=z[8]*z[86];
    z[84]=z[84] + z[85];
    z[84]=z[10]*z[84];
    z[85]= - z[70]*z[87];
    z[84]=z[85] + z[84];
    z[85]=npow(z[6],2);
    z[84]=z[84]*z[85];
    z[69]=n<T>(1,2)*z[69] + z[84];
    z[69]=z[1]*z[69];
    z[66]=z[87]*z[9]*z[66];
    z[46]=z[69] + z[46] + z[66] + z[71];
    z[46]=z[1]*z[46];
    z[66]=z[33]*z[2];
    z[69]=3*z[9];
    z[71]=z[69] - z[75] - z[66];
    z[71]=z[5]*z[71];
    z[84]=n<T>(1,2)*z[57];
    z[71]=z[71] - n<T>(5,2)*z[42] + z[84] + z[73];
    z[71]=z[71]*z[67];
    z[87]=3*z[4];
    z[88]= - z[87] - 19*z[8];
    z[83]=z[88]*z[83];
    z[88]=z[9] - z[13];
    z[89]=z[88]*z[7];
    z[83]= - 10*z[89] - z[22] + z[83];
    z[83]=z[7]*z[83];
    z[73]=11*z[53] - n<T>(19,4)*z[73];
    z[73]=z[9]*z[73];
    z[71]=z[71] + z[83] + z[73] + z[70] - z[76];
    z[71]=z[5]*z[71];
    z[73]=z[4]*z[69];
    z[73]= - z[56] + z[73];
    z[76]=n<T>(1,4)*z[4];
    z[83]= - z[76] + z[62];
    z[83]=z[5]*z[83];
    z[73]=n<T>(1,4)*z[73] + z[83];
    z[73]=z[5]*z[73];
    z[83]= - z[53] - z[61];
    z[83]=z[9]*z[83];
    z[90]=2*z[21];
    z[91]= - z[90] - z[89];
    z[91]=z[91]*z[30];
    z[73]= - z[81] + z[73] + z[83] + z[91];
    z[73]=z[10]*z[73];
    z[81]= - z[88]*z[30];
    z[81]= - z[21] + z[81];
    z[81]=z[81]*z[82];
    z[54]=z[42]*z[54];
    z[83]=n<T>(1,2)*z[70];
    z[54]=z[83] + z[54];
    z[54]=z[54]*z[67];
    z[54]=4*z[81] + z[54];
    z[54]=z[5]*z[54];
    z[81]=z[5]*z[34];
    z[81]= - n<T>(5,4)*z[86] + z[81];
    z[81]=z[10]*z[81];
    z[91]= - z[2] + 11*z[5];
    z[91]=z[10]*z[91];
    z[91]=z[42] + z[91];
    z[92]=n<T>(1,2)*z[6];
    z[91]=z[91]*z[92];
    z[81]=z[91] + n<T>(1,4)*z[70] + z[81];
    z[81]=z[81]*z[85];
    z[54]=z[54] + z[81];
    z[54]=z[12]*z[54];
    z[81]=6*z[53];
    z[91]=2*z[14];
    z[93]=z[91] - z[4];
    z[93]=z[8]*z[93];
    z[93]=z[93] - z[81] + z[61];
    z[93]=z[8]*z[93];
    z[79]= - z[79] + z[93];
    z[79]=z[9]*z[79];
    z[55]=z[56] + n<T>(3,4)*z[55];
    z[55]=z[9]*z[55];
    z[55]= - z[70] + z[55];
    z[55]=z[7]*z[55];
    z[37]=z[46] + z[54] + z[37] + z[73] + z[71] + z[55] + z[80] + z[79];
    z[37]=z[1]*z[37];
    z[46]= - n<T>(5,4)*z[77] - z[48] - n<T>(7,4)*z[56];
    z[46]=z[5]*z[46];
    z[54]=3*z[17];
    z[55]=2*z[4];
    z[71]=z[54] - z[55];
    z[73]=2*z[5];
    z[73]=z[71]*z[73];
    z[79]=2*z[64];
    z[73]=z[73] - z[36] + z[79];
    z[73]=z[10]*z[73];
    z[80]=z[63]*z[3];
    z[80]=z[80] + z[21];
    z[93]=4*z[3];
    z[94]=z[80]*z[93];
    z[95]=7*z[36] - z[64];
    z[95]=z[2]*z[95];
    z[96]=z[4] - z[18];
    z[97]=z[96]*z[82];
    z[46]=z[73] + z[46] + z[97] - z[94] + z[95];
    z[46]=z[10]*z[46];
    z[73]=4*z[17];
    z[95]=5*z[3];
    z[97]= - n<T>(11,4)*z[2] + z[95] - z[20] + z[73];
    z[97]=z[9]*z[97];
    z[98]=n<T>(9,4)*z[2];
    z[99]= - z[34] - n<T>(19,4)*z[5] - z[20] + z[98];
    z[99]=z[10]*z[99];
    z[100]=z[9] + z[4];
    z[92]=z[100]*z[92];
    z[92]=z[92] + z[99] - z[61] + z[97];
    z[92]=z[6]*z[92];
    z[97]=z[5] + z[2];
    z[99]= - z[97]*z[34];
    z[99]=z[99] + z[53] + 2*z[86];
    z[99]=z[10]*z[99];
    z[101]=4*z[36];
    z[102]=z[101] - z[48];
    z[103]=5*z[53] + z[102];
    z[103]=z[9]*z[103];
    z[96]= - z[91] - z[96];
    z[96]=z[7]*z[96];
    z[104]=24*z[56];
    z[96]= - z[104] + z[96];
    z[96]=z[7]*z[96];
    z[105]=2*z[82];
    z[106]=z[5]*z[7];
    z[106]=z[105] + z[106];
    z[106]=z[5]*z[106];
    z[107]=n<T>(3,2)*z[70];
    z[92]=z[92] + z[99] + z[106] + z[96] - z[107] + z[103];
    z[92]=z[6]*z[92];
    z[96]=z[102]*z[9];
    z[99]=3*z[3];
    z[103]=5*z[17];
    z[106]=z[99] + z[103];
    z[108]= - z[59] + z[106];
    z[108]=z[9]*z[108];
    z[84]= - z[84] + z[108];
    z[84]=z[6]*z[84];
    z[97]=z[97]*npow(z[10],2);
    z[83]=z[84] + z[97] + z[83] + z[96];
    z[83]=z[83]*z[85];
    z[84]=6*z[21];
    z[108]=12*z[13];
    z[109]=z[108] - 13*z[9];
    z[109]=z[7]*z[109];
    z[109]= - z[84] + z[109];
    z[109]=z[109]*z[105];
    z[110]=z[57] + z[42];
    z[110]=z[5]*z[110];
    z[70]= - z[70] + z[110];
    z[70]=z[70]*z[67];
    z[70]=z[109] + z[70];
    z[70]=z[5]*z[70];
    z[109]=z[4] - z[17];
    z[110]=3*z[10];
    z[111]=z[109]*z[110];
    z[111]= - z[111] - z[48] + 3*z[56];
    z[112]= - z[5]*z[111];
    z[113]=z[36]*z[2];
    z[112]=z[113] + z[112];
    z[113]=z[112]*z[10];
    z[114]=z[53]*z[36];
    z[113]=z[113] + z[114];
    z[115]= - z[86]*z[61];
    z[115]=z[115] + z[113];
    z[115]=z[10]*z[115];
    z[116]=npow(z[7],3);
    z[117]=z[14]*z[116]*z[58];
    z[118]=npow(z[6],3)*z[36]*z[12];
    z[119]=z[118]*z[9];
    z[70]=z[119] + z[83] + z[115] + z[117] + z[70];
    z[70]=z[12]*z[70];
    z[83]=24*z[4];
    z[115]=8*z[14];
    z[117]= - z[83] + z[18] + z[115];
    z[117]=z[9]*z[117];
    z[120]=12*z[21];
    z[121]=z[2]*z[18];
    z[89]=24*z[89] + z[117] + z[120] - z[121];
    z[89]=z[89]*z[82];
    z[52]=n<T>(3,2)*z[78] - z[57] - z[52];
    z[52]=z[5]*z[52];
    z[78]=z[88]*z[82];
    z[52]=z[52] - 13*z[78] - z[107] + 5*z[68];
    z[52]=z[5]*z[52];
    z[46]=z[70] + z[92] + z[46] + z[89] + z[52];
    z[46]=z[12]*z[46];
    z[52]=z[33]*z[58];
    z[68]=2*z[33];
    z[70]=n<T>(1,2) + z[68];
    z[70]=z[8]*z[70];
    z[52]=z[52] + z[70] - n<T>(5,4)*z[2] + z[55];
    z[52]=z[9]*z[52];
    z[70]=z[11]*z[2];
    z[78]= - z[18]*z[70];
    z[59]=z[9] - n<T>(3,4)*z[66] + z[59] + z[78];
    z[59]=z[5]*z[59];
    z[78]=z[3]*z[13];
    z[88]=z[90] - z[78];
    z[89]=3*z[18];
    z[92]= - 11*z[2] - z[89] + z[93];
    z[92]=z[2]*z[92];
    z[107]= - z[11]*z[81];
    z[107]= - z[66] + n<T>(11,2)*z[2] + z[107];
    z[107]=z[8]*z[107];
    z[117]= - 13*z[13] + n<T>(23,4)*z[9];
    z[117]=z[7]*z[117];
    z[52]=z[59] + z[117] + z[52] + z[107] - n<T>(7,4)*z[57] + 4*z[88] + 
    z[92];
    z[52]=z[5]*z[52];
    z[59]=z[11]*z[13];
    z[59]=z[59] - z[32];
    z[59]=z[59]*z[7];
    z[88]=15*z[9];
    z[92]=z[21]*z[11];
    z[107]=static_cast<T>(3)+ z[33];
    z[107]=z[8]*z[107];
    z[107]= - 4*z[59] + z[88] + z[107] - z[4] + 8*z[92] + z[99] + z[89]
    - z[24];
    z[107]=z[7]*z[107];
    z[117]= - z[11]*z[18];
    z[117]= - n<T>(3,2)*z[33] + static_cast<T>(2)+ z[117];
    z[117]=z[5]*z[117];
    z[122]=2*z[17];
    z[123]= - n<T>(3,2) - z[68];
    z[123]=z[8]*z[123];
    z[89]=z[117] + z[123] + n<T>(15,4)*z[4] - z[122] + z[19] - z[89];
    z[89]=z[5]*z[89];
    z[41]=z[9]*z[41];
    z[117]= - z[11]*z[19];
    z[117]=z[117] + 3*z[33];
    z[117]=z[5]*z[117];
    z[123]=2*z[3];
    z[124]=z[36]*z[11];
    z[41]=z[117] + z[41] - z[123] + z[124];
    z[41]=z[10]*z[41];
    z[73]=6*z[4] - z[2] + z[73] - z[49];
    z[73]=z[9]*z[73];
    z[117]=4*z[78];
    z[125]= - n<T>(5,4)*z[3] + z[2];
    z[125]=z[2]*z[125];
    z[41]=z[41] + z[89] + z[107] + z[73] + z[61] - z[117] + z[125];
    z[41]=z[10]*z[41];
    z[61]=2*z[11];
    z[73]=z[26]*z[8];
    z[89]= - z[28] + z[61] - z[73];
    z[89]=z[10]*z[89];
    z[107]=static_cast<T>(7)- 13*z[33];
    z[89]=z[89] + n<T>(1,2)*z[107] + z[32];
    z[89]=z[10]*z[89];
    z[107]= - z[16] + z[11];
    z[74]=z[107]*z[74];
    z[39]= - z[32] + n<T>(1,2)*z[39] + z[74];
    z[39]=z[6]*z[39];
    z[74]= - static_cast<T>(1)- z[38];
    z[74]=z[4]*z[74];
    z[107]=npow(z[16],2);
    z[125]=z[107]*z[4];
    z[126]=n<T>(7,2)*z[16] - z[125];
    z[126]=z[4]*z[126];
    z[126]= - n<T>(21,2) + z[126];
    z[126]=z[8]*z[126];
    z[74]=z[74] + z[126];
    z[126]=n<T>(1,2)*z[33];
    z[127]=z[11]*z[17];
    z[128]=z[126] - n<T>(23,4) + z[127];
    z[128]=z[5]*z[128];
    z[39]=z[39] + z[89] + z[128] + n<T>(1,2)*z[74] + z[69];
    z[39]=z[6]*z[39];
    z[74]=static_cast<T>(5)- z[126];
    z[74]=z[74]*z[67];
    z[89]=z[48]*z[11];
    z[126]=4*z[8];
    z[128]= - z[122] + z[13];
    z[74]=z[74] - 3*z[7] + n<T>(13,4)*z[9] + z[126] - n<T>(7,4)*z[4] + 4*z[128]
    - z[89];
    z[74]=z[5]*z[74];
    z[128]=2*z[32];
    z[129]=z[128] + 1;
    z[68]=z[68] + z[129];
    z[68]=z[10]*z[68];
    z[130]=z[8] + z[2];
    z[131]=z[3] + z[20];
    z[132]=6*z[9];
    z[68]=z[68] + n<T>(19,2)*z[5] - z[132] + z[131] + n<T>(7,2)*z[130];
    z[68]=z[10]*z[68];
    z[130]=z[107]*z[14];
    z[133]= - z[130] + z[125];
    z[133]=z[8]*z[133];
    z[134]=z[16]*z[14];
    z[135]= - z[16] - z[125];
    z[135]=z[4]*z[135];
    z[133]=z[133] + z[134] + z[135];
    z[133]=z[133]*z[62];
    z[135]= - static_cast<T>(25)+ 9*z[38];
    z[135]=z[135]*z[76];
    z[133]=z[135] + z[133];
    z[133]=z[8]*z[133];
    z[135]= - z[62] + z[55] + z[18] - z[115];
    z[135]=z[7]*z[135];
    z[136]=n<T>(1,4)*z[2] + z[55];
    z[136]=z[4]*z[136];
    z[137]= - n<T>(45,4)*z[2] + n<T>(9,4)*z[3] - z[20] - z[17];
    z[137]=z[9]*z[137];
    z[39]=z[39] + z[68] + z[74] + z[135] + z[137] + z[136] + z[133];
    z[39]=z[6]*z[39];
    z[68]= - z[2]*z[73];
    z[68]=z[70] + z[68];
    z[68]=z[8]*z[68];
    z[74]=6*z[13];
    z[68]=14*z[9] + z[68] - z[74] + z[2];
    z[68]=z[7]*z[68];
    z[133]=z[18]*z[75];
    z[66]=n<T>(17,4)*z[2] + z[66];
    z[66]=z[8]*z[66];
    z[135]=12*z[14];
    z[136]= - 31*z[4] + z[18] + z[135];
    z[136]=z[9]*z[136];
    z[57]=z[68] + z[136] + z[66] + z[57] - z[22] + z[133];
    z[57]=z[7]*z[57];
    z[66]=z[16] - z[15];
    z[68]=z[66]*z[55];
    z[68]=static_cast<T>(1)+ z[68];
    z[68]=z[4]*z[68];
    z[133]=z[14]*z[15];
    z[136]=z[133] - z[134];
    z[137]= - z[4]*z[66];
    z[137]=z[137] - z[136];
    z[137]=z[137]*z[62];
    z[68]=z[137] - z[91] + z[68];
    z[68]=z[8]*z[68];
    z[68]=z[68] + z[81] - n<T>(1,4)*z[56];
    z[68]=z[8]*z[68];
    z[81]=2*z[134];
    z[137]=z[38] - static_cast<T>(3)- z[81];
    z[137]=z[8]*z[137];
    z[135]= - z[58] + z[137] - 12*z[4] + z[135] + z[2];
    z[135]=z[8]*z[135];
    z[137]=7*z[3];
    z[138]=z[4]*z[137];
    z[135]=z[138] + z[135];
    z[135]=z[9]*z[135];
    z[138]= - z[4]*z[64];
    z[37]=z[37] + z[46] + z[39] + z[41] + z[52] + z[57] + z[135] + 
    z[138] + z[68];
    z[37]=z[1]*z[37];
    z[39]=z[11]*z[16];
    z[41]=11*z[107] + 6*z[39];
    z[41]=z[8]*z[41];
    z[46]=n<T>(1,2)*z[125];
    z[52]=4*z[13];
    z[57]=z[52]*z[16];
    z[68]= - n<T>(15,2) - z[57];
    z[68]=z[16]*z[68];
    z[41]=z[41] - z[46] + z[68] - 6*z[11];
    z[41]=z[6]*z[41];
    z[68]=npow(z[16],3);
    z[135]=z[68]*z[4];
    z[138]=23*z[107] - 9*z[135];
    z[138]=z[4]*z[138];
    z[139]= - 5*z[16] + z[11];
    z[138]=17*z[139] + z[138];
    z[138]=z[8]*z[138];
    z[139]=z[11] + z[73];
    z[139]=3*z[139] - z[28];
    z[139]=z[139]*z[34];
    z[140]=z[16]*z[13];
    z[46]= - z[16] + z[46];
    z[46]=z[4]*z[46];
    z[41]=z[41] + z[139] + z[128] + n<T>(1,4)*z[138] + n<T>(11,2)*z[46] - z[127]
    + n<T>(1,4) + 12*z[140];
    z[41]=z[6]*z[41];
    z[46]=z[135] + 4*z[107];
    z[46]=z[4]*z[46];
    z[127]=z[68]*z[14];
    z[138]=z[127] - z[135];
    z[138]=z[8]*z[138];
    z[46]=z[138] - 4*z[130] + z[46];
    z[46]=z[46]*z[126];
    z[130]=n<T>(5,2) + z[81];
    z[46]=z[46] + 7*z[130] - n<T>(125,4)*z[38];
    z[46]=z[8]*z[46];
    z[130]=z[128] + 13;
    z[138]=z[52]*z[11];
    z[139]=4*z[33] - z[138] + z[130];
    z[139]=z[7]*z[139];
    z[141]=z[54] + z[20] + z[14];
    z[141]=3*z[141] + z[63];
    z[142]=static_cast<T>(61)- 13*z[38];
    z[76]=z[142]*z[76];
    z[142]=z[63]*z[11];
    z[143]= - n<T>(35,4)*z[33] + n<T>(19,4) + z[142];
    z[143]=z[5]*z[143];
    z[144]=static_cast<T>(1)+ z[33];
    z[144]=n<T>(1,2)*z[144] + z[32];
    z[145]=4*z[10];
    z[146]= - z[11]*z[145];
    z[144]=3*z[144] + z[146];
    z[144]=z[10]*z[144];
    z[120]=z[16]*z[120];
    z[41]=z[41] + z[144] + z[143] + z[139] - 11*z[9] + z[46] + z[76] + 
    z[89] - 6*z[2] + z[120] + 2*z[141] + z[99];
    z[41]=z[6]*z[41];
    z[46]=8*z[21];
    z[76]=z[46] - z[48];
    z[89]=z[24] - z[3];
    z[89]=z[89]*z[123];
    z[120]=12*z[17];
    z[139]=4*z[5];
    z[141]=z[139] + z[120] + n<T>(163,4)*z[4];
    z[141]=z[5]*z[141];
    z[71]=z[5] + z[124] - z[3] - z[71];
    z[71]=z[71]*z[34];
    z[143]= - 16*z[3] + z[2];
    z[143]=z[2]*z[143];
    z[99]=z[99] + z[18];
    z[144]= - 46*z[4] + z[99];
    z[144]=z[7]*z[144];
    z[71]=z[71] + z[141] + z[144] + n<T>(3,2)*z[56] + z[143] + z[89] - z[76]
   ;
    z[71]=z[10]*z[71];
    z[89]=z[145] - n<T>(59,4)*z[5] - z[98] + z[131];
    z[89]=z[10]*z[89];
    z[98]= - z[17] - z[3];
    z[131]=n<T>(3,2) + z[32];
    z[131]=z[6]*z[131];
    z[60]=z[131] + 4*z[98] - z[60];
    z[60]=z[6]*z[60];
    z[98]=z[48] - z[36];
    z[99]=22*z[4] + z[2] + z[91] - z[99];
    z[99]=z[7]*z[99];
    z[131]= - z[30] - n<T>(5,2)*z[4] + z[69];
    z[131]=z[5]*z[131];
    z[83]= - z[2] + z[83];
    z[83]=z[4]*z[83];
    z[141]=n<T>(5,2)*z[2] - z[93] - z[20] - 10*z[17];
    z[141]=z[9]*z[141];
    z[60]=z[60] + z[89] + z[131] + z[99] + z[141] + z[83] + z[98];
    z[60]=z[6]*z[60];
    z[83]= - z[80]*z[123];
    z[89]=z[101] - z[64];
    z[89]=z[2]*z[89];
    z[99]=6*z[7];
    z[101]= - z[56]*z[99];
    z[83]=z[101] + z[83] + z[89];
    z[89]=2*z[36];
    z[101]=z[17] + z[4];
    z[101]=z[5]*z[101];
    z[79]=z[101] + z[79] - z[89] + z[111];
    z[79]=z[10]*z[79];
    z[101]=n<T>(11,2)*z[77] + z[48] + n<T>(55,4)*z[56];
    z[101]=z[5]*z[101];
    z[79]=z[79] + 4*z[83] + z[101];
    z[79]=z[10]*z[79];
    z[83]=z[112]*z[34];
    z[86]=z[86]*z[56];
    z[83]=z[83] + 2*z[114] + n<T>(7,4)*z[86];
    z[83]=z[10]*z[83];
    z[101]=z[9]*z[14];
    z[112]=z[116]*z[101];
    z[74]=z[74] - 7*z[9];
    z[74]=z[7]*z[74];
    z[74]= - 3*z[21] + z[74];
    z[116]=z[82]*z[5];
    z[74]=z[74]*z[116];
    z[74]=z[112] + z[74];
    z[131]=z[102]*z[58];
    z[123]=z[123] + z[103];
    z[141]=z[123]*z[58];
    z[141]= - z[36] + z[141];
    z[141]=z[6]*z[141];
    z[131]=z[131] + z[141];
    z[131]=z[131]*z[85];
    z[118]=z[58]*z[118];
    z[74]=z[118] + z[131] + 4*z[74] + z[83];
    z[74]=z[12]*z[74];
    z[83]= - z[106] + n<T>(3,2)*z[9];
    z[83]=z[6]*z[83];
    z[106]=9*z[9];
    z[118]= - z[17]*z[106];
    z[83]=z[83] + z[118] - z[102];
    z[83]=z[6]*z[83];
    z[102]=z[48] + 9*z[36];
    z[102]=z[9]*z[102];
    z[118]=z[7]*z[104];
    z[83]=z[83] - 2*z[97] + z[102] + z[118];
    z[83]=z[6]*z[83];
    z[97]=12*z[9] - z[14] - z[108];
    z[97]=z[7]*z[97];
    z[97]=z[97] + z[84] + 7*z[101];
    z[82]=z[97]*z[82];
    z[82]=z[114] + z[82];
    z[97]=z[7] - z[63] + z[9];
    z[97]=z[7]*z[97];
    z[97]=z[84] + z[97];
    z[97]=z[97]*z[30];
    z[100]=z[5]*z[100];
    z[100]=n<T>(5,2)*z[56] + z[100];
    z[67]=z[100]*z[67];
    z[67]=z[97] + z[67];
    z[67]=z[5]*z[67];
    z[67]=z[74] + z[83] + z[79] + 2*z[82] + z[67];
    z[67]=z[12]*z[67];
    z[74]=z[22] - z[48];
    z[79]=19*z[13];
    z[82]= - z[99] - z[79] + 23*z[9];
    z[82]=z[7]*z[82];
    z[83]= - z[2] + n<T>(77,4)*z[4];
    z[83]=z[4]*z[83];
    z[97]= - z[7] + 5*z[4] + z[58];
    z[97]=z[5]*z[97];
    z[42]=z[97] + z[82] + n<T>(9,2)*z[42] + 2*z[74] + z[83];
    z[42]=z[5]*z[42];
    z[82]=z[2] + 14*z[13];
    z[83]=3*z[14];
    z[59]=12*z[59] + z[9] - 6*z[92] - z[83] + z[82];
    z[59]=z[59]*z[30];
    z[97]=z[18] + 18*z[14];
    z[97]=z[9]*z[97];
    z[59]=z[59] + z[97] - 24*z[21] - z[121];
    z[59]=z[7]*z[59];
    z[80]=z[3]*z[80];
    z[97]=17*z[36] - 4*z[64];
    z[97]=z[2]*z[97];
    z[99]=z[48] + z[36];
    z[99]=z[99]*z[58];
    z[42]=z[67] + z[60] + z[71] + z[42] + z[59] + z[99] - 8*z[80] + 
    z[97];
    z[42]=z[12]*z[42];
    z[59]=z[66]*z[38];
    z[60]=z[16]*z[136];
    z[60]=z[60] + z[59];
    z[60]=z[60]*z[126];
    z[67]=4*z[15];
    z[59]= - 4*z[59] + z[67] - 13*z[16];
    z[59]=z[4]*z[59];
    z[59]=z[60] + z[59] + 14*z[134] + static_cast<T>(1)- 4*z[133];
    z[59]=z[8]*z[59];
    z[60]=3*z[11];
    z[71]=z[53]*z[60];
    z[71]=z[71] - 4*z[2] - z[83] + z[17];
    z[80]=static_cast<T>(25)- n<T>(7,4)*z[38];
    z[80]=z[4]*z[80];
    z[59]=z[59] + 2*z[71] + z[80];
    z[59]=z[8]*z[59];
    z[71]=z[49] - z[124];
    z[80]=6*z[50] + static_cast<T>(3)- z[81];
    z[80]=z[8]*z[80];
    z[81]= - z[3] + z[91] + z[17];
    z[45]=z[80] - z[45] + 6*z[81] - z[71];
    z[45]=z[9]*z[45];
    z[80]=z[18] - z[93];
    z[80]=z[2]*z[80];
    z[80]=z[80] + z[117] + z[48] + z[22];
    z[80]=z[11]*z[80];
    z[81]=n<T>(21,2)*z[33] + z[130];
    z[81]=z[9]*z[81];
    z[83]=z[63] + z[17];
    z[97]= - z[18] + z[83];
    z[99]=static_cast<T>(9)+ n<T>(31,4)*z[70];
    z[99]=z[8]*z[99];
    z[80]=z[5] + z[81] + z[99] + n<T>(31,2)*z[4] + z[80] + 4*z[97] - n<T>(15,4)*
    z[2];
    z[80]=z[5]*z[80];
    z[81]=z[70] - 1;
    z[97]=z[81]*z[26]*z[62];
    z[99]=static_cast<T>(1)- 2*z[70];
    z[99]=z[11]*z[99];
    z[97]=z[99] + z[97];
    z[97]=z[8]*z[97];
    z[99]=9*z[13];
    z[100]= - z[99] - z[2];
    z[100]=z[11]*z[100];
    z[100]= - static_cast<T>(17)+ z[100];
    z[97]=z[128] + 2*z[100] + z[97];
    z[97]=z[7]*z[97];
    z[100]=z[33] - static_cast<T>(2)- 13*z[70];
    z[100]=z[8]*z[100];
    z[97]=z[97] + 8*z[9] + z[100] + 20*z[92] - 9*z[2] + z[13] - 4*z[14];
    z[97]=z[7]*z[97];
    z[100]= - z[22] + 7*z[78];
    z[43]=z[2] + z[43] - 17*z[3];
    z[43]=z[2]*z[43];
    z[72]= - z[72] - z[19] + z[2];
    z[72]=z[4]*z[72];
    z[25]=z[37] + z[42] + z[41] + z[25] + z[80] + z[97] + z[45] + z[59]
    + z[72] + 2*z[100] + z[43];
    z[25]=z[1]*z[25];
    z[37]=z[22] - z[36];
    z[37]=z[11]*z[37];
    z[41]=z[40] - z[38];
    z[42]= - static_cast<T>(1)+ 4*z[41];
    z[42]=z[42]*z[34];
    z[43]=3*z[49];
    z[45]=static_cast<T>(18)- z[50];
    z[45]=z[2]*z[45];
    z[49]= - static_cast<T>(12)+ 7*z[38];
    z[49]=z[4]*z[49];
    z[59]=static_cast<T>(56)+ n<T>(3,2)*z[31];
    z[59]=z[5]*z[59];
    z[37]=z[42] + z[59] + z[49] + z[37] + z[45] - z[43] + z[47] + z[122]
    - z[79];
    z[37]=z[10]*z[37];
    z[42]=3*z[36];
    z[45]=z[42] - z[64];
    z[45]=z[45]*z[75];
    z[45]=z[45] - z[94];
    z[49]=2*z[48];
    z[59]=n<T>(9,2)*z[77] + z[49] + 13*z[56];
    z[59]=z[5]*z[59];
    z[72]=z[109]*z[5];
    z[72]=5*z[72] - z[36] + z[111];
    z[72]=z[10]*z[72];
    z[59]=z[72] + z[59] + z[45];
    z[59]=z[10]*z[59];
    z[72]=5*z[9];
    z[77]=z[52] - z[72];
    z[77]=z[7]*z[77];
    z[77]= - z[90] + z[77];
    z[77]=z[77]*z[116];
    z[77]=z[112] + z[77];
    z[79]=2*z[86] + z[113];
    z[79]=z[10]*z[79];
    z[80]=z[103] + z[3];
    z[86]=z[9]*z[80];
    z[86]= - z[36] + z[86];
    z[86]=z[6]*z[86];
    z[86]=z[96] + z[86];
    z[85]=z[86]*z[85];
    z[77]=z[119] + z[85] + 2*z[77] + z[79];
    z[77]=z[12]*z[77];
    z[79]=z[69] - z[124] - z[123];
    z[79]=z[6]*z[79];
    z[85]= - 13*z[17] - z[95];
    z[85]=z[9]*z[85];
    z[42]=z[79] + z[85] + z[48] - z[42];
    z[42]=z[6]*z[42];
    z[79]=z[49] + 5*z[36];
    z[79]=z[9]*z[79];
    z[42]=z[79] + z[42];
    z[42]=z[6]*z[42];
    z[79]=z[14]*z[69];
    z[85]=4*z[9] - z[14] - z[52];
    z[85]=z[7]*z[85];
    z[79]=z[85] + z[90] + z[79];
    z[79]=z[79]*z[105];
    z[24]=z[30] - z[24] + z[72];
    z[24]=z[7]*z[24];
    z[24]=z[46] + z[24];
    z[24]=z[7]*z[24];
    z[46]=2*z[56];
    z[46]=z[5]*z[46];
    z[24]=z[24] + z[46];
    z[24]=z[5]*z[24];
    z[24]=z[77] + z[42] + z[59] + z[24] + z[114] + z[79];
    z[24]=z[12]*z[24];
    z[41]=z[41]*z[110];
    z[42]= - static_cast<T>(5)+ 3*z[38];
    z[42]=z[4]*z[42];
    z[41]=z[41] + z[42] + z[103] - z[71];
    z[41]=z[10]*z[41];
    z[42]=z[99] - z[3];
    z[42]=z[3]*z[42];
    z[42]=z[42] - z[48] + z[84];
    z[46]=z[47] - z[2];
    z[59]= - z[46]*z[75];
    z[71]=z[139] - z[17] + 48*z[4];
    z[71]=z[5]*z[71];
    z[41]=z[41] + z[71] + 9*z[56] + 2*z[42] + z[59];
    z[41]=z[10]*z[41];
    z[42]=z[11]*z[98];
    z[59]= - z[11]*z[80];
    z[59]=z[59] + z[129];
    z[59]=z[6]*z[59];
    z[42]=z[59] - z[106] + z[42] + z[120] + z[95];
    z[42]=z[6]*z[42];
    z[59]=z[54] - z[93];
    z[59]=z[59]*z[69];
    z[42]=z[42] + z[59] - z[104] - z[49] - z[36];
    z[42]=z[6]*z[42];
    z[49]=z[7]*z[15];
    z[59]= - static_cast<T>(2)- z[49];
    z[59]=z[59]*z[30];
    z[52]=z[59] - z[52] + z[88];
    z[52]=z[7]*z[52];
    z[59]=z[15]*z[55];
    z[59]=n<T>(7,2) + z[59];
    z[59]=z[4]*z[59];
    z[59]=z[59] + z[9];
    z[59]=z[5]*z[59];
    z[52]=z[59] + z[52] + 16*z[56] - z[74];
    z[52]=z[5]*z[52];
    z[59]= - 4*z[32] + z[133] + z[138];
    z[59]=z[59]*z[30];
    z[69]= - z[91] + 5*z[13];
    z[59]=z[59] - 21*z[9] + 3*z[69] - z[23];
    z[59]=z[7]*z[59];
    z[69]= - z[90] + z[101];
    z[59]=6*z[69] + z[59];
    z[59]=z[7]*z[59];
    z[69]= - z[53]*z[124];
    z[71]= - z[48] + z[89];
    z[71]=z[9]*z[71];
    z[24]=z[24] + z[42] + z[41] + z[52] + z[59] + z[71] + z[69] + z[45];
    z[24]=z[12]*z[24];
    z[41]=3*z[16];
    z[42]=z[28] - z[41] - 5*z[11];
    z[42]=z[6]*z[42];
    z[45]=6*z[17];
    z[52]=z[45] + z[3];
    z[52]=z[11]*z[52];
    z[52]= - n<T>(5,2) + z[52];
    z[59]=n<T>(1,2)*z[16] + 3*z[125];
    z[59]=z[4]*z[59];
    z[42]=z[42] - n<T>(11,2)*z[32] + 3*z[52] + z[59];
    z[42]=z[6]*z[42];
    z[52]= - z[115] - z[54];
    z[48]= - 3*z[48] + z[89];
    z[48]=z[11]*z[48];
    z[54]=n<T>(5,2) + 16*z[38];
    z[54]=z[54]*z[87];
    z[34]=z[42] + z[34] + n<T>(45,2)*z[9] + z[54] + z[48] + 2*z[52] - z[3];
    z[34]=z[6]*z[34];
    z[42]= - static_cast<T>(3)+ 8*z[49];
    z[42]=z[42]*z[30];
    z[48]=z[4]*z[15];
    z[52]=z[48] + 1;
    z[54]=n<T>(3,2)*z[5];
    z[54]=z[52]*z[54];
    z[48]=static_cast<T>(29)+ 20*z[48];
    z[48]=z[4]*z[48];
    z[42]=z[54] + z[42] + n<T>(23,2)*z[9] + z[48] + z[83];
    z[42]=z[5]*z[42];
    z[36]= - 10*z[36] + 3*z[64];
    z[36]=z[2]*z[36];
    z[36]=z[94] + z[36];
    z[36]=z[11]*z[36];
    z[47]=z[124] - z[47] + z[91] - z[17];
    z[47]=z[9]*z[47];
    z[29]=z[7]*z[29];
    z[29]=z[32] - z[29];
    z[23]=z[23] - z[44] - z[2];
    z[23]=z[11]*z[23];
    z[23]=static_cast<T>(12)+ z[23] + 8*z[29];
    z[23]=z[7]*z[23];
    z[23]=z[23] - z[132] - 11*z[13] + 16*z[92];
    z[23]=z[7]*z[23];
    z[29]=z[35] + 11*z[78];
    z[35]= - z[137] + z[2];
    z[35]=z[35]*z[75];
    z[23]=z[24] + z[34] + z[37] + z[42] + z[23] + z[47] - 18*z[56] + 
    z[36] + 2*z[29] + z[35];
    z[23]=z[12]*z[23];
    z[24]=static_cast<T>(3)+ z[57];
    z[24]=z[24]*z[107];
    z[29]=static_cast<T>(1)+ z[140];
    z[29]=z[29]*z[39];
    z[34]= - z[11]*z[107];
    z[34]= - z[68] + z[34];
    z[34]=z[34]*z[65];
    z[24]=z[34] + z[24] + 4*z[29];
    z[24]=z[6]*z[24];
    z[29]=z[107] + n<T>(1,2)*z[39];
    z[34]=npow(z[16],4);
    z[35]=z[34]*z[87];
    z[35]= - n<T>(1,2)*z[68] + z[35];
    z[35]=z[4]*z[35];
    z[29]=13*z[29] + z[35];
    z[29]=z[8]*z[29];
    z[35]=2*z[16];
    z[36]=static_cast<T>(5)- 6*z[140];
    z[36]=z[36]*z[35];
    z[37]= - z[107] - 6*z[135];
    z[37]=z[4]*z[37];
    z[39]= - z[16]*z[63];
    z[39]=n<T>(25,2) + z[39];
    z[39]=z[11]*z[39];
    z[24]=z[24] + 3*z[28] + z[29] + z[37] + z[36] + z[39];
    z[24]=z[6]*z[24];
    z[29]=z[34]*z[4];
    z[36]= - z[29] - 7*z[68];
    z[36]=z[4]*z[36];
    z[34]= - z[14]*z[34];
    z[29]=z[34] + z[29];
    z[29]=z[8]*z[29];
    z[29]=z[29] + 7*z[127] + z[36];
    z[29]=z[29]*z[62];
    z[34]= - n<T>(1,2) - 12*z[134];
    z[34]=z[34]*z[41];
    z[36]=n<T>(13,2)*z[107] - z[135];
    z[36]=z[36]*z[87];
    z[29]=z[29] + z[36] + z[34] - z[61];
    z[29]=z[8]*z[29];
    z[26]= - z[26]*z[58];
    z[26]=z[26] + 3*z[27] - z[73];
    z[26]=z[26]*z[30];
    z[21]= - z[21]*z[41];
    z[21]=z[21] + 5*z[14] - z[13];
    z[27]=8*z[16];
    z[21]=z[21]*z[27];
    z[22]= - z[16]*z[22];
    z[22]= - 7*z[17] + z[22];
    z[22]=z[22]*z[60];
    z[30]= - 10*z[16] - 19*z[125];
    z[30]=z[4]*z[30];
    z[21]=z[24] + z[26] - n<T>(21,2)*z[32] + z[29] + z[30] + z[22] - n<T>(27,2)
    + z[21];
    z[21]=z[6]*z[21];
    z[22]=z[66]*z[125];
    z[24]= - z[15] + z[41];
    z[24]=z[24]*z[35];
    z[24]=z[24] + z[22];
    z[24]=z[24]*z[55];
    z[26]= - z[136]*z[107];
    z[22]=z[26] - z[22];
    z[22]=z[22]*z[62];
    z[26]=z[133] - 3*z[134];
    z[26]=z[16]*z[26];
    z[29]=z[40] - z[50];
    z[29]=z[11]*z[29];
    z[22]=z[22] + z[24] + 4*z[26] + z[29];
    z[22]=z[8]*z[22];
    z[24]=z[16]*z[53];
    z[24]=z[45] + z[24];
    z[24]=z[11]*z[24];
    z[26]=2*z[125] - n<T>(25,2)*z[15] - z[27];
    z[26]=z[4]*z[26];
    z[27]= - n<T>(7,2) + 6*z[134];
    z[22]=z[22] + z[26] + z[24] + 3*z[27] - z[51];
    z[22]=z[8]*z[22];
    z[24]=z[11]*z[81];
    z[24]= - z[67] + z[24];
    z[24]=z[11]*z[24];
    z[26]=z[70] - 2;
    z[26]=z[26]*z[11];
    z[26]=z[26] + z[15];
    z[27]= - z[26]*z[73];
    z[24]=z[24] + z[27];
    z[24]=z[8]*z[24];
    z[27]=z[11]*z[82];
    z[27]=static_cast<T>(34)+ z[27];
    z[27]=z[11]*z[27];
    z[24]= - 6*z[28] + z[24] - 18*z[15] + z[27];
    z[24]=z[7]*z[24];
    z[26]= - z[26]*z[33];
    z[27]= - static_cast<T>(19)+ 10*z[70];
    z[27]=z[11]*z[27];
    z[26]=z[26] + z[15] + z[27];
    z[26]=z[8]*z[26];
    z[27]=static_cast<T>(12)- z[133];
    z[28]= - 12*z[92] + 24*z[13] + 7*z[2];
    z[28]=z[11]*z[28];
    z[24]=z[24] - 9*z[32] + z[26] + 2*z[27] + z[28];
    z[24]=z[7]*z[24];
    z[26]= - static_cast<T>(3)+ 2*z[40];
    z[26]=n<T>(39,2)*z[31] - 5*z[38] - z[142] + 3*z[26] - 4*z[50];
    z[26]=z[10]*z[26];
    z[27]=z[2]*z[46];
    z[27]=z[27] - 6*z[78] - z[76];
    z[27]=z[27]*z[60];
    z[28]= - 5*z[49] + 11*z[52] - n<T>(13,2)*z[32];
    z[28]=z[5]*z[28];
    z[29]= - 11*z[15] + 14*z[16];
    z[29]=z[29]*z[55];
    z[29]= - n<T>(17,2) + z[29];
    z[29]=z[4]*z[29];
    z[30]=static_cast<T>(19)- z[50];
    z[30]=z[2]*z[30];
    z[21]=z[25] + z[23] + z[21] + z[26] + z[28] + z[24] - z[9] + z[22]
    + z[29] + z[27] + z[30] - z[43] - 16*z[13] - z[19] - z[17];

    r += z[21]*z[1];
 
    return r;
}

template double qqb_2lNLC_r568(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r568(const std::array<dd_real,31>&);
#endif
