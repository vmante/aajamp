#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r476(const std::array<T,31>& k) {
  T z[63];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[2];
    z[12]=k[9];
    z[13]=k[15];
    z[14]=k[8];
    z[15]=z[3]*z[8];
    z[16]=z[6]*z[8];
    z[17]=z[15] - z[16];
    z[18]=z[2]*z[17];
    z[19]=2*z[3];
    z[18]=2*z[18] + z[7] + z[19] + z[5];
    z[20]=z[7]*z[9];
    z[21]=z[5]*z[9];
    z[22]= - static_cast<T>(2)+ z[21];
    z[22]=2*z[22] + z[20];
    z[22]=z[4]*z[22];
    z[18]=2*z[18] + z[22];
    z[18]=z[4]*z[18];
    z[22]=z[5]*z[3];
    z[23]=z[3]*z[9];
    z[24]= - n<T>(5,2) + z[23];
    z[24]=z[24]*z[22];
    z[25]=n<T>(3,2)*z[14];
    z[26]=npow(z[7],2);
    z[27]=z[26]*z[9];
    z[28]=z[25] - z[27];
    z[28]=z[7]*z[28];
    z[29]=3*z[2];
    z[30]=n<T>(1,2)*z[5];
    z[31]=3*z[3] + z[30];
    z[31]=z[29] + n<T>(27,2)*z[6] + 3*z[31] + 4*z[7];
    z[31]=z[2]*z[31];
    z[32]=npow(z[14],2);
    z[33]=n<T>(5,2)*z[32];
    z[34]=z[6]*z[7];
    z[35]=2*z[34];
    z[36]=npow(z[3],2);
    z[18]=z[18] + z[31] - z[35] + z[28] + z[24] - z[33] - z[36];
    z[18]=z[4]*z[18];
    z[24]=z[22] + z[34];
    z[28]=z[6] - z[7];
    z[31]=2*z[5];
    z[36]= - z[3] + z[31] - z[28];
    z[36]=z[2]*z[36];
    z[36]=z[36] + z[24];
    z[37]=npow(z[4],2);
    z[36]=z[36]*z[37];
    z[38]=n<T>(1,2)*z[3];
    z[39]=n<T>(1,2)*z[6];
    z[40]=z[38] + z[39];
    z[41]=n<T>(1,2)*z[7];
    z[42]= - z[41] - z[5] + z[40];
    z[43]=npow(z[2],2);
    z[42]=z[42]*z[43];
    z[44]=npow(z[7],3);
    z[42]= - z[44] + z[42];
    z[42]=z[2]*z[42];
    z[45]=z[44]*z[6];
    z[36]=z[36] - z[45] + z[42];
    z[36]=z[4]*z[36];
    z[42]=n<T>(1,2)*z[43];
    z[46]=z[42]*z[24];
    z[46]=z[46] + z[45];
    z[47]=z[24]*z[37];
    z[47]= - z[46] + z[47];
    z[48]=z[4]*z[2];
    z[47]=z[47]*z[48];
    z[49]=npow(z[5],3);
    z[50]=z[49]*z[7];
    z[51]=z[50]*z[39];
    z[52]=z[3]*z[51];
    z[47]=z[52] + z[47];
    z[47]=z[1]*z[47];
    z[52]=z[38]*z[50];
    z[49]= - z[49]*z[38];
    z[49]=z[49] + z[50];
    z[49]=z[6]*z[49];
    z[46]=z[2]*z[46];
    z[36]=z[47] + z[36] + z[46] + z[52] + z[49];
    z[36]=z[1]*z[36];
    z[17]= - static_cast<T>(2)- z[17];
    z[17]=z[2]*z[17];
    z[46]=static_cast<T>(2)+ z[23];
    z[46]=z[5]*z[46];
    z[47]= - static_cast<T>(1)+ z[20];
    z[47]=z[6]*z[47];
    z[17]=z[17] + z[47] + z[7] - z[19] + z[46];
    z[17]=z[4]*z[17];
    z[28]= - z[2]*z[28];
    z[17]=z[17] + z[28] + z[24];
    z[17]=z[4]*z[17];
    z[24]= - 3*z[22] + z[34];
    z[28]= - z[39] + z[38];
    z[28]=z[8]*z[28];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=z[2]*z[28];
    z[34]=3*z[5];
    z[46]=z[34] + z[6];
    z[28]= - n<T>(1,2)*z[46] + z[28];
    z[28]=z[2]*z[28];
    z[24]=n<T>(1,2)*z[24] + z[28];
    z[24]=z[2]*z[24];
    z[28]= - z[9]*z[45];
    z[17]=z[17] + z[24] - z[44] + z[28];
    z[17]=z[4]*z[17];
    z[24]= - z[7]*z[43]*z[39];
    z[24]= - z[45] + z[24];
    z[24]=z[2]*z[24];
    z[28]= - z[37] + z[42];
    z[42]=z[7] - z[5];
    z[28]=z[42]*z[28];
    z[28]=z[44] + z[28];
    z[28]=z[28]*z[48];
    z[24]=z[28] + z[51] + z[24];
    z[24]=z[10]*z[24];
    z[28]=z[23] - 1;
    z[28]=z[28]*z[5];
    z[28]=z[28] - z[3];
    z[44]=npow(z[5],2);
    z[44]=n<T>(1,2)*z[44];
    z[28]=z[28]*z[44];
    z[45]=n<T>(11,2)*z[22] + z[26];
    z[45]=z[7]*z[45];
    z[45]=z[28] + z[45];
    z[45]=z[6]*z[45];
    z[28]= - z[7]*z[28];
    z[47]=static_cast<T>(1)+ n<T>(1,2)*z[23];
    z[47]=z[5]*z[47];
    z[40]=z[47] - z[40];
    z[40]=z[2]*z[40];
    z[40]= - n<T>(1,2)*z[22] + z[40];
    z[40]=z[40]*z[43];
    z[17]=z[36] + z[24] + z[17] + z[40] + z[28] + z[45];
    z[17]=z[1]*z[17];
    z[24]=npow(z[13],2);
    z[28]=n<T>(1,2)*z[24];
    z[36]=npow(z[13],3);
    z[40]=2*z[36];
    z[45]=z[40]*z[9];
    z[47]=z[28] - z[45];
    z[47]=z[9]*z[47];
    z[47]= - z[13] + z[47];
    z[47]=z[3]*z[47];
    z[48]=z[16] - 1;
    z[49]= - z[8] - z[9];
    z[49]=z[3]*z[49];
    z[49]=z[49] + z[48];
    z[49]=z[2]*z[49];
    z[46]=z[49] + z[3] + z[46];
    z[49]=n<T>(1,2)*z[2];
    z[46]=z[46]*z[49];
    z[50]=z[36]*z[9];
    z[51]=4*z[50];
    z[28]=z[46] - z[35] + z[47] + z[28] - z[51];
    z[28]=z[2]*z[28];
    z[35]=z[4]*z[5];
    z[46]=z[35]*z[40];
    z[47]=npow(z[12],3);
    z[52]=z[47]*z[5];
    z[53]=z[52]*z[39];
    z[46]=z[46] - z[53];
    z[46]=z[46]*z[10];
    z[53]=z[43] - 5*z[32] + z[26];
    z[53]=z[39]*z[53];
    z[53]= - z[40] + z[53];
    z[53]=z[2]*z[53];
    z[37]= - z[37] + n<T>(3,2)*z[43];
    z[37]=z[42]*z[37];
    z[54]= - n<T>(7,2)*z[32] + z[26];
    z[54]=z[7]*z[54];
    z[55]=z[24]*z[5];
    z[37]=n<T>(3,2)*z[55] + z[54] + z[37];
    z[37]=z[4]*z[37];
    z[54]=npow(z[12],2);
    z[56]=3*z[54];
    z[57]=z[56] + z[44];
    z[57]=z[6]*z[5]*z[57];
    z[37]=z[46] + z[37] + z[57] + z[53];
    z[37]=z[10]*z[37];
    z[53]=z[21] - 1;
    z[57]=z[5]*z[53];
    z[57]=z[57] + n<T>(7,2)*z[12] - z[19];
    z[57]=z[5]*z[57];
    z[58]=z[38] - z[5];
    z[58]= - 3*z[58] - z[41];
    z[58]=z[7]*z[58];
    z[33]=z[58] + z[33] + z[57];
    z[33]=z[6]*z[33];
    z[57]=z[8] - z[11];
    z[58]=z[57]*z[12];
    z[58]=static_cast<T>(9)- z[58];
    z[58]=z[3]*z[58]*z[54];
    z[58]= - z[47] + z[58];
    z[59]=npow(z[8],2);
    z[60]=z[59]*z[12];
    z[61]= - 9*z[8] + z[60];
    z[61]=z[12]*z[61];
    z[61]=static_cast<T>(5)+ z[61];
    z[61]=z[3]*z[12]*z[61];
    z[22]=z[22]*z[9];
    z[62]=z[3] + z[22];
    z[62]=z[5]*z[62];
    z[61]=z[61] + z[62];
    z[26]=n<T>(1,2)*z[61] - z[26];
    z[26]=z[7]*z[26];
    z[17]=z[17] + z[37] + z[18] + z[28] + z[33] + n<T>(1,2)*z[58] + z[26];
    z[17]=z[1]*z[17];
    z[15]=n<T>(7,2)*z[16] + n<T>(15,2) - 4*z[15];
    z[15]=z[2]*z[15];
    z[16]=n<T>(13,2)*z[14];
    z[18]=3*z[13];
    z[26]=z[32]*z[9];
    z[28]=2*z[9];
    z[33]= - z[7]*z[28];
    z[33]=n<T>(15,2) + z[33];
    z[33]=z[6]*z[33];
    z[37]=static_cast<T>(4)+ z[20];
    z[37]=z[4]*z[37];
    z[15]=z[37] + z[15] + z[33] + z[7] - n<T>(3,2)*z[22] + z[3] - z[26] + 
    z[16] + z[18];
    z[15]=z[4]*z[15];
    z[22]=19*z[14] + 3*z[7];
    z[22]=z[22]*z[39];
    z[33]= - z[2]*z[48];
    z[33]= - z[6] + z[33];
    z[33]=z[33]*z[49];
    z[37]=4*z[24];
    z[22]=z[33] + z[22] + z[51] - n<T>(3,2)*z[32] + z[37];
    z[22]=z[2]*z[22];
    z[33]=3*z[24];
    z[48]= - z[5]*z[13];
    z[48]=z[48] - z[32] - z[33];
    z[16]=z[16] + z[27];
    z[16]=z[7]*z[16];
    z[27]= - z[34] + z[7];
    z[27]=z[27]*z[49];
    z[20]=z[21] - z[20];
    z[20]=z[4]*z[20];
    z[20]=z[20] - z[42];
    z[20]=z[4]*z[20];
    z[16]=z[20] + z[27] + n<T>(3,2)*z[48] + z[16];
    z[16]=z[4]*z[16];
    z[20]=z[54] + z[44];
    z[20]=z[5]*z[20];
    z[27]=z[32] + z[43];
    z[27]=z[49]*z[27];
    z[42]=n<T>(1,2)*z[47];
    z[20]=z[27] + z[42] + z[20];
    z[20]=z[6]*z[20];
    z[27]=z[32]*z[7];
    z[36]=4*z[36];
    z[43]= - n<T>(5,2)*z[27] - z[36] + n<T>(9,2)*z[55];
    z[43]=z[4]*z[43];
    z[47]= - z[40] - z[47];
    z[47]=z[5]*z[47];
    z[35]=z[36]*z[35];
    z[36]= - z[6]*z[52];
    z[35]=z[36] + z[35];
    z[35]=z[10]*z[35];
    z[20]=z[35] + z[43] + z[47] + z[20];
    z[20]=z[10]*z[20];
    z[35]=z[44] - n<T>(3,2)*z[24] + 2*z[54];
    z[35]=z[5]*z[35];
    z[36]=z[53]*z[30];
    z[36]=n<T>(3,2)*z[7] - 4*z[12] + z[36];
    z[36]=z[5]*z[36];
    z[36]= - n<T>(1,2)*z[32] - z[56] + z[36];
    z[36]=z[6]*z[36];
    z[43]=z[42] + z[40];
    z[16]=z[20] + z[16] + z[22] + z[36] + n<T>(7,2)*z[27] + z[35] + z[43];
    z[16]=z[10]*z[16];
    z[20]=n<T>(7,2)*z[8];
    z[22]= - z[20] + z[60];
    z[22]=z[12]*z[22];
    z[22]= - n<T>(3,2) + z[22];
    z[22]=z[12]*z[22];
    z[35]=npow(z[8],3);
    z[36]=z[35]*z[12];
    z[44]=n<T>(7,2)*z[59] - z[36];
    z[44]=z[12]*z[44];
    z[44]=n<T>(15,2)*z[8] + z[44];
    z[44]=z[12]*z[44];
    z[44]=static_cast<T>(4)+ z[44];
    z[44]=z[3]*z[44];
    z[47]=3*z[23];
    z[48]= - static_cast<T>(1)- z[47];
    z[48]=z[48]*z[30];
    z[52]=z[7]*z[8];
    z[52]=n<T>(1,2) + z[52];
    z[52]=z[7]*z[52];
    z[22]=z[52] + z[48] + z[44] - 4*z[14] + z[22];
    z[22]=z[7]*z[22];
    z[44]=n<T>(21,2)*z[24] + 8*z[50];
    z[44]=z[9]*z[44];
    z[48]=n<T>(13,2)*z[24] + z[51];
    z[48]=z[9]*z[48];
    z[48]= - n<T>(5,2)*z[13] + z[48];
    z[48]=z[9]*z[48];
    z[48]= - static_cast<T>(7)+ z[48];
    z[48]=z[3]*z[48];
    z[50]= - z[23] - 1;
    z[50]=z[8]*z[50];
    z[50]=z[9] + z[50];
    z[49]=z[50]*z[49];
    z[38]=z[8]*z[38];
    z[38]=z[49] - static_cast<T>(2)+ z[38];
    z[38]=z[2]*z[38];
    z[38]=z[38] - n<T>(23,2)*z[6] - z[7] + z[48] + n<T>(9,2)*z[14] + z[44];
    z[38]=z[2]*z[38];
    z[44]=z[12]*z[8];
    z[48]=z[57]*z[44];
    z[20]=z[48] - z[11] - z[20];
    z[20]=z[12]*z[20];
    z[20]= - static_cast<T>(6)+ z[20];
    z[20]=z[12]*z[20];
    z[48]= - n<T>(7,2)*z[24] - z[51];
    z[48]=z[9]*z[48];
    z[49]=n<T>(1,2)*z[13];
    z[20]=z[3] + z[48] - z[49] + z[20];
    z[20]=z[3]*z[20];
    z[19]= - z[30] + z[49] + z[19];
    z[19]=z[5]*z[19];
    z[47]= - static_cast<T>(1)+ z[47];
    z[47]=z[47]*z[41];
    z[23]= - static_cast<T>(1)+ 2*z[23];
    z[23]=z[5]*z[23];
    z[23]=z[47] + z[23] - n<T>(5,2)*z[3] + z[26] - 7*z[14] - n<T>(3,2)*z[12];
    z[23]=z[6]*z[23];
    z[47]=z[12]*z[11];
    z[48]= - static_cast<T>(7)- z[47];
    z[48]=z[48]*z[54];
    z[48]= - z[24] + z[48];
    z[15]=z[17] + z[16] + z[15] + z[38] + z[23] + z[22] + z[19] + z[20]
    + n<T>(1,2)*z[48] - z[45];
    z[15]=z[1]*z[15];
    z[16]= - z[54]*z[31];
    z[17]=z[32]*z[29];
    z[16]=z[17] + z[42] + z[16];
    z[16]=z[6]*z[16];
    z[17]=z[24]*z[34];
    z[17]=z[27] - z[40] + z[17];
    z[17]=z[4]*z[17];
    z[19]= - z[5]*z[43];
    z[16]=z[46] + z[17] + z[19] + z[16];
    z[16]=z[10]*z[16];
    z[17]=z[45] + z[32];
    z[19]=z[13]*z[31];
    z[20]=z[14]*z[41];
    z[19]=z[20] + z[19] - z[24] + z[17];
    z[19]=z[4]*z[19];
    z[20]=z[13]*z[11];
    z[22]=2*z[20];
    z[23]= - static_cast<T>(1)+ z[22];
    z[23]=z[23]*z[24];
    z[31]=n<T>(1,2)*z[54];
    z[34]= - static_cast<T>(3)- z[47];
    z[34]=z[34]*z[31];
    z[23]=z[23] + z[34];
    z[23]=z[5]*z[23];
    z[34]=n<T>(1,2)*z[12];
    z[38]=z[34]*z[8];
    z[40]=z[38] + 2;
    z[41]=z[40]*z[54];
    z[42]=z[5]*z[12];
    z[41]= - 8*z[42] - 3*z[32] + z[41];
    z[41]=z[6]*z[41];
    z[25]= - z[6]*z[25];
    z[25]=z[32] + z[25];
    z[25]=z[25]*z[29];
    z[16]=z[16] + z[19] + z[25] + z[41] - z[27] + z[23] + z[43];
    z[16]=z[10]*z[16];
    z[19]=z[34]*z[59];
    z[19]=z[19] + z[8];
    z[19]=z[19]*z[12];
    z[23]=n<T>(3,2) + z[19];
    z[23]=z[12]*z[23];
    z[25]=n<T>(1,2)*z[14];
    z[23]=z[26] + z[25] + z[23];
    z[23]=z[7]*z[23];
    z[27]= - z[32] + z[33];
    z[27]=z[9]*z[27];
    z[29]= - z[14] + z[18];
    z[27]=z[30] + n<T>(1,2)*z[29] + z[27];
    z[27]=z[4]*z[27];
    z[29]= - static_cast<T>(1)- z[22];
    z[29]=z[29]*z[24];
    z[30]=z[8] + z[11];
    z[32]=z[12]*z[30];
    z[32]=static_cast<T>(3)+ z[32];
    z[31]=z[32]*z[31];
    z[32]= - n<T>(13,2) - z[47];
    z[32]=z[12]*z[32];
    z[32]= - z[49] + z[32];
    z[32]=z[5]*z[32];
    z[33]=n<T>(13,2) + z[44];
    z[33]=z[12]*z[33];
    z[26]= - 7*z[5] + z[26] + n<T>(5,2)*z[14] + z[33];
    z[26]=z[6]*z[26];
    z[17]= - z[37] - z[17];
    z[17]=z[9]*z[17];
    z[33]= - z[14] - z[13];
    z[17]=2*z[6] + n<T>(5,2)*z[33] + z[17];
    z[17]=z[2]*z[17];
    z[16]=z[16] + z[27] + z[17] + z[26] + z[23] + z[32] - z[45] + z[29]
    + z[31];
    z[16]=z[10]*z[16];
    z[17]= - z[57]*z[19];
    z[19]=3*z[11] + 5*z[8];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[12]*z[17];
    z[19]=static_cast<T>(11)- z[22];
    z[19]=z[19]*z[24];
    z[19]=z[19] + z[51];
    z[19]=z[9]*z[19];
    z[23]=static_cast<T>(11)- 4*z[20];
    z[23]=z[13]*z[23];
    z[19]=z[23] + z[19];
    z[19]=z[9]*z[19];
    z[20]=n<T>(1,2)*z[20];
    z[23]=static_cast<T>(1)- z[20];
    z[17]=z[19] + 5*z[23] + z[17];
    z[17]=z[3]*z[17];
    z[19]= - 2*z[59] - z[36];
    z[19]=z[12]*z[19];
    z[19]=z[8] + z[19];
    z[19]=z[12]*z[19];
    z[23]=npow(z[8],4)*z[34];
    z[23]=z[35] + z[23];
    z[23]=z[12]*z[23];
    z[23]= - n<T>(5,2)*z[59] + z[23];
    z[23]=z[12]*z[23];
    z[23]= - n<T>(1,2)*z[9] - 3*z[8] + z[23];
    z[23]=z[3]*z[23];
    z[26]=z[25]*z[9];
    z[19]=z[23] - z[26] + n<T>(1,2) + z[19];
    z[19]=z[7]*z[19];
    z[23]= - z[5]*z[28];
    z[23]=z[23] - z[26] + z[40];
    z[23]=z[6]*z[23];
    z[26]= - 11*z[24] - z[51];
    z[26]=z[9]*z[26];
    z[25]=z[26] + z[25] - 11*z[13];
    z[25]=z[9]*z[25];
    z[26]= - 7*z[24] - z[45];
    z[26]=z[9]*z[26];
    z[26]= - 10*z[13] + z[26];
    z[26]=z[9]*z[26];
    z[26]= - static_cast<T>(5)+ z[26];
    z[26]=z[9]*z[26];
    z[26]=2*z[8] + z[26];
    z[26]=z[3]*z[26];
    z[27]= - z[8]*z[39];
    z[25]=z[27] + z[26] - static_cast<T>(3)+ z[25];
    z[25]=z[2]*z[25];
    z[26]=z[30]*z[38];
    z[26]=z[26] + z[11] + n<T>(3,2)*z[8];
    z[26]=z[12]*z[26];
    z[26]=n<T>(13,2) + z[26];
    z[26]=z[12]*z[26];
    z[27]=static_cast<T>(3)+ z[22];
    z[24]=z[27]*z[24];
    z[24]=z[24] + z[45];
    z[24]=z[9]*z[24];
    z[20]= - n<T>(3,2)*z[47] - static_cast<T>(3)+ z[20];
    z[20]=z[5]*z[20];
    z[18]=z[14] + z[18];
    z[18]=z[9]*z[18];
    z[18]= - z[21] + static_cast<T>(5)+ z[18];
    z[18]=z[4]*z[18];
    z[21]=n<T>(3,2) + z[22];
    z[21]=z[13]*z[21];
    z[15]=z[15] + z[16] + n<T>(1,2)*z[18] + z[25] + z[23] + z[19] + z[20] + 
    z[17] + z[24] + z[21] + z[26];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r476(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r476(const std::array<dd_real,31>&);
#endif
