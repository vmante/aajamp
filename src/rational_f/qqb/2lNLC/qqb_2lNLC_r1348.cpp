#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1348(const std::array<T,31>& k) {
  T z[76];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[10];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[12];
    z[10]=k[11];
    z[11]=k[13];
    z[12]=k[14];
    z[13]=k[19];
    z[14]=k[2];
    z[15]=k[3];
    z[16]=npow(z[3],3);
    z[17]=5*z[16];
    z[18]=npow(z[14],2);
    z[19]=z[17]*z[18];
    z[20]=z[16]*z[2];
    z[21]=z[14]*z[16];
    z[21]=z[21] + z[20];
    z[21]=z[2]*z[21];
    z[22]=z[7] - z[2] - z[14];
    z[22]=z[17]*z[22];
    z[23]=z[10]*z[12];
    z[22]=z[23] + z[22];
    z[22]=z[7]*z[22];
    z[21]=z[22] + 5*z[21] + z[4] + z[19];
    z[21]=z[7]*z[21];
    z[22]=z[4]*z[5];
    z[24]= - z[14]*z[17];
    z[24]=25*z[20] + z[22] + z[24];
    z[24]=z[2]*z[24];
    z[25]=2*z[4];
    z[19]=z[24] + z[25] - z[19];
    z[19]=z[2]*z[19];
    z[24]=z[12] - z[5];
    z[24]=z[24]*z[18];
    z[26]=z[24]*z[8];
    z[27]=z[13]*z[5];
    z[28]=z[12]*z[13];
    z[29]=z[27] - z[28];
    z[30]=z[29]*z[18];
    z[19]=z[21] + z[26] - 2*z[30] + z[19];
    z[21]=n<T>(2,3)*z[6];
    z[19]=z[19]*z[21];
    z[30]=npow(z[13],2);
    z[31]=z[30]*z[5];
    z[32]=z[30]*z[12];
    z[33]=z[31] - z[32];
    z[33]=z[33]*z[14];
    z[27]=z[27] + 2*z[28];
    z[27]=2*z[27] - z[33];
    z[27]=z[14]*z[27];
    z[27]=2*z[13] + z[27];
    z[34]=z[14]*z[5];
    z[35]=z[16]*z[34];
    z[36]=53*z[5] - 43*z[4];
    z[20]=z[36]*z[20];
    z[20]= - 43*z[35] + z[20];
    z[20]=z[2]*z[20];
    z[35]=11*z[16];
    z[36]=z[18]*z[5];
    z[37]=z[36]*z[35];
    z[20]=z[37] + z[20];
    z[20]=z[2]*z[20];
    z[20]=z[20] + 2*z[27] + z[9];
    z[27]=n<T>(1,3)*z[2];
    z[37]=10*z[10];
    z[38]=z[37] - z[4];
    z[38]=z[38]*z[27];
    z[39]=z[10] + z[12];
    z[40]=z[39]*z[7];
    z[41]=z[14]*z[12];
    z[38]= - n<T>(19,3)*z[40] + z[38] + 3*z[41];
    z[38]=z[16]*z[38];
    z[42]=z[30]*z[11];
    z[43]=z[30]*z[9];
    z[44]=z[42] - z[43];
    z[45]=z[9] - z[11];
    z[46]=npow(z[8],2);
    z[47]=z[45]*z[46];
    z[38]=z[47] + n<T>(2,3)*z[44] + z[38];
    z[38]=z[7]*z[38];
    z[48]=z[9]*z[13];
    z[49]=z[48] + z[23];
    z[50]=z[18]*z[12];
    z[51]=npow(z[2],2);
    z[52]=z[51]*z[4];
    z[53]= - n<T>(23,3)*z[52] - 3*z[50];
    z[53]=z[16]*z[53];
    z[54]=2*z[8];
    z[55]= - z[9]*z[54];
    z[38]=z[38] + z[55] + n<T>(4,3)*z[49] + z[53];
    z[38]=z[7]*z[38];
    z[26]=n<T>(1,3)*z[26];
    z[53]=n<T>(1,3)*z[5];
    z[55]=z[53] + z[12];
    z[55]=z[14]*z[55];
    z[55]= - z[26] - static_cast<T>(2)+ z[55];
    z[55]=z[8]*z[55];
    z[19]=z[19] + z[38] + n<T>(1,3)*z[20] + z[55];
    z[19]=z[6]*z[19];
    z[20]=z[31] + z[32];
    z[38]=z[14]*z[20];
    z[38]=z[38] - z[30] + z[28];
    z[38]=2*z[38] + z[49];
    z[49]=npow(z[2],3);
    z[55]=z[16]*z[49]*z[22];
    z[38]=2*z[38] - 17*z[55];
    z[55]=npow(z[8],3);
    z[56]=2*z[55];
    z[57]= - z[45]*z[56];
    z[58]=z[23]*z[7];
    z[35]=z[58]*z[35];
    z[35]=z[57] + z[35];
    z[35]=z[7]*z[35];
    z[57]= - n<T>(1,3)*z[11] + 3*z[9];
    z[57]=z[57]*z[46];
    z[35]=z[57] + z[35];
    z[35]=z[7]*z[35];
    z[57]=n<T>(1,3)*z[41];
    z[59]=static_cast<T>(2)- z[57];
    z[59]=z[59]*z[54];
    z[60]=5*z[9];
    z[61]=z[12] - z[60];
    z[59]=n<T>(1,3)*z[61] + z[59];
    z[59]=z[8]*z[59];
    z[19]=z[19] + z[35] + n<T>(1,3)*z[38] + z[59];
    z[19]=z[6]*z[19];
    z[35]=npow(z[3],4);
    z[38]=z[35]*z[36];
    z[59]=z[4] - z[5];
    z[61]= - z[2]*z[59];
    z[61]= - z[34] + z[61];
    z[62]=z[35]*z[2];
    z[61]=z[61]*z[62];
    z[38]=n<T>(1,3)*z[38] + z[61];
    z[38]=z[38]*z[51];
    z[51]=z[2]*z[10];
    z[40]= - z[40] + z[41] + z[51];
    z[51]=z[35]*z[7];
    z[40]=z[40]*z[51];
    z[50]= - z[50] - z[52];
    z[50]=z[35]*z[50];
    z[40]=z[40] + z[50];
    z[50]=n<T>(1,3)*z[7];
    z[40]=z[40]*z[50];
    z[49]= - z[4]*z[35]*z[49];
    z[40]=z[49] + z[40];
    z[40]=z[7]*z[40];
    z[38]=z[38] + z[40];
    z[38]=z[6]*z[38];
    z[40]=npow(z[7],3);
    z[49]=z[35]*z[40]*z[23];
    z[52]=z[46]*z[9];
    z[52]= - z[52] + n<T>(2,3)*z[43];
    z[49]= - 2*z[52] + n<T>(29,3)*z[49];
    z[49]=z[7]*z[49];
    z[61]=2*z[9];
    z[63]=z[13]*z[61];
    z[63]= - z[30] + z[63];
    z[35]=z[35]*npow(z[2],4)*z[22];
    z[64]= - z[61] + z[8];
    z[64]=z[8]*z[64];
    z[35]=10*z[38] + z[49] + z[64] + n<T>(2,3)*z[63] - 7*z[35];
    z[35]=z[6]*z[35];
    z[38]=n<T>(5,3)*z[9];
    z[49]=z[38] - z[8];
    z[49]=z[49]*z[46];
    z[63]=z[55]*z[7];
    z[64]= - z[61]*z[63];
    z[65]=n<T>(1,3)*z[43];
    z[49]=z[64] - z[65] + z[49];
    z[35]=2*z[49] + z[35];
    z[35]=z[6]*z[35];
    z[49]=z[4] - z[11];
    z[64]=z[10] - z[9];
    z[66]= - z[64] - z[49];
    z[67]=n<T>(10,3)*z[55];
    z[62]=z[66]*z[67]*z[62];
    z[66]=z[4]*z[9];
    z[68]=7*z[66];
    z[69]=z[10]*z[11];
    z[45]=z[4] - z[10] - z[45];
    z[45]=z[8]*z[45];
    z[45]=n<T>(10,3)*z[45] + n<T>(29,3)*z[69] - z[68];
    z[45]=z[46]*z[45]*z[51];
    z[45]=z[62] + z[45];
    z[40]=z[45]*z[40];
    z[45]= - z[69] + z[66];
    z[51]=z[7]*z[3];
    z[51]=npow(z[51],5);
    z[45]=z[45]*z[51]*z[67];
    z[62]=z[2]*z[3];
    z[62]= - z[22]*npow(z[62],5);
    z[51]=z[23]*z[51];
    z[51]=z[62] + z[51];
    z[51]=z[6]*z[51];
    z[51]=n<T>(10,3)*z[51] - z[52];
    z[51]=z[6]*z[51];
    z[52]= - z[55]*z[61];
    z[51]=z[52] + z[51];
    z[51]=z[6]*z[51];
    z[45]=z[45] + z[51];
    z[45]=z[1]*z[45];
    z[51]=z[30]*z[23];
    z[52]=n<T>(4,3)*z[10];
    z[55]=z[9] + z[52];
    z[55]=z[55]*z[56];
    z[35]=z[45] + z[35] + z[40] - n<T>(1,3)*z[51] + z[55];
    z[35]=z[1]*z[35];
    z[40]=2*z[11];
    z[45]=z[40] - z[25];
    z[51]=z[64] - z[45];
    z[17]=z[2]*z[51]*z[17];
    z[51]=4*z[69];
    z[55]= - z[51] + z[66];
    z[55]=z[8]*z[55];
    z[17]=z[17] + z[55];
    z[17]=z[17]*z[46];
    z[55]=7*z[9];
    z[56]=n<T>(19,3)*z[10];
    z[62]=7*z[4];
    z[67]= - z[62] + z[56] - n<T>(19,3)*z[11] + z[55];
    z[67]=z[8]*z[67];
    z[67]=z[67] - 11*z[69] + n<T>(17,3)*z[66];
    z[16]=z[7]*z[8]*z[16]*z[67];
    z[16]=n<T>(2,3)*z[17] + z[16];
    z[16]=z[7]*z[16];
    z[17]=z[42] - z[32];
    z[42]=n<T>(1,3)*z[10];
    z[17]=z[17]*z[42];
    z[67]=3*z[10];
    z[38]=z[38] + z[67];
    z[38]=z[38]*z[54];
    z[38]=z[69] + z[38];
    z[38]=z[38]*z[46];
    z[16]=z[16] + z[17] + z[38];
    z[16]=z[7]*z[16];
    z[17]=z[8]*z[2];
    z[38]=z[64]*z[17];
    z[70]=5*z[10];
    z[55]= - z[12] - z[55];
    z[55]= - n<T>(8,3)*z[38] + n<T>(1,3)*z[55] - z[70];
    z[55]=z[55]*z[46];
    z[71]=2*z[30] + z[28];
    z[71]=z[71]*z[42];
    z[72]=z[13]*z[15];
    z[72]=z[72] - 1;
    z[73]=z[72]*z[48];
    z[74]= - z[15]*z[31];
    z[74]=z[74] + z[73];
    z[74]=z[4]*z[74];
    z[16]=z[35] + z[19] + z[16] + z[55] + n<T>(1,3)*z[74] + z[71] - z[65] - 
    z[31] + n<T>(2,3)*z[32];
    z[16]=z[1]*z[16];
    z[19]= - z[30]*z[40];
    z[30]=z[13]*z[11];
    z[28]= - z[30] + z[28];
    z[28]=z[10]*z[28];
    z[19]=z[28] + 2*z[43] + z[19] - z[32];
    z[28]=npow(z[3],2);
    z[31]=z[28]*z[15];
    z[32]=z[66]*z[31];
    z[19]=2*z[19] + z[32];
    z[32]=n<T>(1,3)*z[9];
    z[35]= - z[42] + z[32] - z[49];
    z[42]=z[28]*z[2];
    z[35]=z[35]*z[42];
    z[43]= - z[40] + z[60];
    z[38]= - n<T>(26,3)*z[38] - n<T>(4,3)*z[4] + n<T>(1,3)*z[43] - z[70];
    z[38]=z[8]*z[38];
    z[43]=n<T>(1,3)*z[66];
    z[35]=z[38] + 5*z[35] - z[51] - z[43];
    z[35]=z[35]*z[54];
    z[38]=z[25] + z[10] + z[11] - 4*z[9];
    z[38]=z[38]*z[54];
    z[38]=z[38] + 31*z[69] - 8*z[66];
    z[38]=z[8]*z[38];
    z[51]=14*z[10];
    z[55]=17*z[4] - z[51] + 13*z[11] - 16*z[9];
    z[55]=z[55]*z[28];
    z[38]=z[55] + z[38];
    z[38]=z[38]*z[54];
    z[55]=z[11] + z[12];
    z[55]=z[10]*z[55];
    z[55]=28*z[55] - 13*z[66];
    z[55]=z[55]*z[28];
    z[65]= - 13*z[69] + z[68];
    z[65]=z[65]*z[63];
    z[38]=4*z[65] + z[55] + z[38];
    z[38]=z[38]*z[50];
    z[55]=z[11] - z[12];
    z[55]=z[55]*z[42]*z[67];
    z[19]=z[38] + z[35] + n<T>(1,3)*z[19] + z[55];
    z[19]=z[7]*z[19];
    z[35]=z[22]*z[2];
    z[38]= - 7*z[35] + 4*z[5] - 19*z[4];
    z[38]=z[2]*z[38];
    z[38]=static_cast<T>(5)+ z[38];
    z[38]=z[38]*z[27];
    z[55]=2*z[10];
    z[65]=7*z[58] - 5*z[4] - z[12] - z[55];
    z[65]=z[7]*z[65];
    z[68]=z[2]*z[4];
    z[65]=z[65] + static_cast<T>(1)- 17*z[68];
    z[65]=z[65]*z[50];
    z[24]=z[65] - z[24] + z[38];
    z[38]=2*z[6];
    z[24]=z[24]*z[38];
    z[65]=2*z[14];
    z[71]= - z[29]*z[65];
    z[71]=z[71] - 5*z[5] - 11*z[12];
    z[71]=z[14]*z[71];
    z[74]= - z[18]*z[28];
    z[71]=z[74] - static_cast<T>(1)+ z[71];
    z[53]=z[53] - z[25];
    z[74]= - 8*z[22] + 55*z[28];
    z[74]=z[74]*z[27];
    z[75]=z[28]*z[14];
    z[53]=z[74] + 2*z[53] - n<T>(11,3)*z[75];
    z[53]=z[2]*z[53];
    z[74]= - z[4] - z[39];
    z[74]=z[42] + 4*z[74] + z[75];
    z[75]=n<T>(34,3)*z[23] + 3*z[28];
    z[75]=z[7]*z[75];
    z[74]=n<T>(1,3)*z[74] + z[75];
    z[74]=z[7]*z[74];
    z[24]=z[24] + z[74] - z[26] + n<T>(1,3)*z[71] + z[53];
    z[24]=z[6]*z[24];
    z[26]=z[39]*z[28];
    z[26]= - 7*z[26] - z[44];
    z[26]=2*z[26] - z[47];
    z[26]=z[7]*z[26];
    z[30]=z[30] - z[48];
    z[25]=z[70] - z[25];
    z[25]=z[25]*z[42];
    z[39]= - z[11]*z[54];
    z[25]=z[26] + z[39] + z[25] + 2*z[30] + 25*z[23];
    z[25]=z[25]*z[50];
    z[26]= - z[33] - z[29];
    z[26]=z[26]*z[65];
    z[29]=z[28]*z[36];
    z[30]=7*z[12];
    z[26]=z[29] + z[4] + z[9] - z[30] + z[26];
    z[28]=z[28]*z[34];
    z[28]=z[22] - 14*z[28];
    z[29]=10*z[5] - n<T>(19,3)*z[4];
    z[29]=z[29]*z[42];
    z[28]=n<T>(1,3)*z[28] + z[29];
    z[28]=z[2]*z[28];
    z[29]= - static_cast<T>(1)- z[57];
    z[29]=z[29]*z[54];
    z[24]=z[24] + z[25] + z[29] + n<T>(1,3)*z[26] + z[28];
    z[24]=z[24]*z[38];
    z[25]=2*z[5];
    z[26]=z[5] + z[11];
    z[26]=z[15]*z[26];
    z[26]= - static_cast<T>(5)+ z[26];
    z[26]=z[13]*z[26];
    z[26]=z[25] + z[26];
    z[26]=z[13]*z[26];
    z[20]=z[20]*z[65];
    z[28]=z[5]*z[15];
    z[28]= - static_cast<T>(2)+ z[28];
    z[28]=z[13]*z[28];
    z[29]= - z[9]*z[72];
    z[28]=z[28] + z[29];
    z[28]=z[4]*z[28];
    z[20]=z[28] + 5*z[23] - z[73] + z[26] + z[20];
    z[26]=10*z[2];
    z[28]=z[64]*z[26];
    z[28]=static_cast<T>(1)+ z[28];
    z[29]=n<T>(1,3)*z[8];
    z[28]=z[28]*z[29];
    z[29]= - z[12] + z[61];
    z[28]=z[28] + n<T>(1,3)*z[29] + z[55];
    z[28]=z[28]*z[54];
    z[29]=n<T>(1,3)*z[31] - 4*z[42];
    z[29]=z[29]*z[35];
    z[16]=2*z[16] + z[24] + z[19] + z[28] + n<T>(2,3)*z[20] + z[29];
    z[16]=z[1]*z[16];
    z[19]=11*z[66];
    z[20]= - 43*z[69] + z[19];
    z[24]= - z[51] + 14*z[9] - z[49];
    z[17]=n<T>(2,3)*z[17];
    z[24]=z[24]*z[17];
    z[24]=z[24] - n<T>(28,3)*z[4] + z[56] - 8*z[11] + 11*z[9];
    z[24]=z[8]*z[24];
    z[20]=n<T>(1,3)*z[20] + z[24];
    z[20]=z[8]*z[20];
    z[24]=3*z[11];
    z[28]=n<T>(11,3)*z[9];
    z[29]=n<T>(11,3)*z[4] - z[67] + z[24] - z[28];
    z[29]=z[29]*z[54];
    z[29]=z[29] + 19*z[69] - n<T>(29,3)*z[66];
    z[29]=z[29]*z[46];
    z[19]= - 14*z[69] + z[19];
    z[19]=z[19]*z[63];
    z[19]=z[29] + n<T>(2,3)*z[19];
    z[19]=z[7]*z[19];
    z[19]=z[20] + z[19];
    z[19]=z[7]*z[19];
    z[20]=z[60] - z[70];
    z[29]=z[49] - z[20];
    z[29]=z[29]*z[17];
    z[29]=z[29] + n<T>(5,3)*z[4] - z[52] + z[40] - n<T>(7,3)*z[9];
    z[29]=z[8]*z[29];
    z[24]=z[24] + n<T>(19,3)*z[12];
    z[24]=z[24]*z[55];
    z[19]=2*z[19] + 4*z[29] + z[24] + z[43];
    z[19]=z[7]*z[19];
    z[24]= - 14*z[12] - 19*z[10];
    z[24]=59*z[58] + 2*z[24] + z[4];
    z[24]=z[24]*z[50];
    z[29]=static_cast<T>(16)+ 17*z[41];
    z[24]=z[24] + n<T>(1,3)*z[29] + 9*z[68];
    z[24]=z[7]*z[24];
    z[29]=z[10] + 10*z[4];
    z[29]=z[29]*z[27];
    z[31]=z[58] - z[10];
    z[31]= - 2*z[12] + n<T>(11,3)*z[31];
    z[31]=z[7]*z[31];
    z[29]=z[31] + z[29] + static_cast<T>(2)+ z[57];
    z[29]=z[7]*z[29];
    z[31]= - n<T>(1,3) + z[41];
    z[31]=z[14]*z[31];
    z[33]= - static_cast<T>(2)+ n<T>(31,3)*z[68];
    z[33]=z[2]*z[33];
    z[29]=z[29] + z[31] + z[33];
    z[29]=z[7]*z[29];
    z[31]=11*z[35] - 19*z[5] + 32*z[4];
    z[31]=z[31]*z[27];
    z[33]=n<T>(1,3)*z[34];
    z[36]= - static_cast<T>(2)+ z[33];
    z[31]=5*z[36] + z[31];
    z[31]=z[2]*z[31];
    z[36]=n<T>(1,3) + z[34];
    z[36]=z[14]*z[36];
    z[31]=z[36] + z[31];
    z[31]=z[2]*z[31];
    z[18]=z[29] - z[18] + z[31];
    z[18]=z[18]*z[38];
    z[29]=29*z[35] - 34*z[5] + 55*z[4];
    z[27]=z[29]*z[27];
    z[27]=z[27] - static_cast<T>(4)+ z[33];
    z[27]=z[2]*z[27];
    z[25]=z[25] + z[12];
    z[25]=z[14]*z[25];
    z[25]= - static_cast<T>(5)+ n<T>(2,3)*z[25];
    z[25]=z[14]*z[25];
    z[18]=z[18] + z[24] + z[25] + z[27];
    z[18]=z[6]*z[18];
    z[24]= - z[30] - z[37];
    z[24]=56*z[58] + 2*z[24] + z[4];
    z[24]=z[24]*z[50];
    z[25]= - z[5] + n<T>(10,3)*z[12];
    z[25]=z[14]*z[25];
    z[27]=n<T>(11,3)*z[35] - z[5] + n<T>(10,3)*z[4];
    z[27]=z[2]*z[27];
    z[18]=z[18] + z[24] + z[27] + n<T>(2,3) + z[25];
    z[18]=z[18]*z[38];
    z[20]=z[20] + z[45];
    z[17]=z[20]*z[17];
    z[20]= - 3*z[23] - n<T>(2,3)*z[22];
    z[20]=z[2]*z[20];
    z[22]=n<T>(2,3)*z[11];
    z[16]=z[16] + z[18] + z[19] + z[17] + z[20] + z[4] - n<T>(2,3)*z[10] + 
    z[32] - z[22] + z[5];
    z[16]=z[1]*z[16];
    z[17]= - 5*z[11] + 7*z[5];
    z[17]=2*z[17] + z[62];
    z[17]=z[2]*z[17];
    z[18]= - z[11] + z[59];
    z[18]=z[15]*z[18];
    z[17]=z[17] - 2*z[34] + static_cast<T>(1)+ z[18];
    z[18]= - n<T>(13,3)*z[4] + z[28] + z[22] - 3*z[12];
    z[18]=z[7]*z[18];
    z[19]=5*z[7] - z[14] + z[26];
    z[19]=z[19]*z[21];
    z[16]=z[16] + z[19] + n<T>(1,3)*z[17] + z[18];

    r += 2*z[16];
 
    return r;
}

template double qqb_2lNLC_r1348(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1348(const std::array<dd_real,31>&);
#endif
