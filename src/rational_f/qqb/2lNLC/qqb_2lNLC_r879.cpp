#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r879(const std::array<T,31>& k) {
  T z[85];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[10];
    z[7]=k[14];
    z[8]=k[15];
    z[9]=k[3];
    z[10]=k[4];
    z[11]=k[5];
    z[12]=k[8];
    z[13]=k[9];
    z[14]=k[2];
    z[15]=z[4]*z[7];
    z[16]=npow(z[5],2);
    z[17]=z[16]*z[15];
    z[18]=z[4] - z[7];
    z[19]=z[18]*z[5];
    z[20]= - n<T>(7,2)*z[15] - z[19];
    z[21]=n<T>(1,2)*z[6];
    z[22]=z[21]*z[5];
    z[20]=z[20]*z[22];
    z[17]=z[17] + z[20];
    z[20]=npow(z[6],2);
    z[17]=z[17]*z[20];
    z[23]=z[4]*z[2];
    z[24]= - z[16]*z[23];
    z[25]=z[4] + z[2];
    z[26]=z[25]*z[5];
    z[27]= - n<T>(7,2)*z[23] + z[26];
    z[28]=n<T>(1,2)*z[3];
    z[27]=z[5]*z[27]*z[28];
    z[24]=z[24] + z[27];
    z[27]=npow(z[3],2);
    z[24]=z[24]*z[27];
    z[29]=npow(z[6],3);
    z[30]=z[29]*z[15];
    z[31]=npow(z[3],3);
    z[32]=z[31]*z[23];
    z[30]=z[30] + z[32];
    z[30]=z[1]*z[16]*z[30];
    z[31]=n<T>(1,4)*z[31];
    z[32]=z[4]*npow(z[8],2)*z[31];
    z[17]=n<T>(1,2)*z[30] + z[32] + z[17] + z[24];
    z[17]=z[1]*z[17];
    z[24]=z[10]*z[4];
    z[30]=z[16]*z[24];
    z[32]=n<T>(1,2)*z[2];
    z[33]=z[32]*z[11];
    z[33]=z[33] - 1;
    z[33]=z[33]*z[5];
    z[34]=7*z[4];
    z[35]= - n<T>(11,2)*z[2] - z[34];
    z[35]=n<T>(1,2)*z[35] - z[33];
    z[35]=z[5]*z[35];
    z[36]=n<T>(1,4)*z[6];
    z[37]=z[2]*z[9];
    z[38]=z[37] + 1;
    z[39]=z[38]*z[6];
    z[40]=z[2] - z[39];
    z[40]=z[40]*z[36];
    z[35]=z[30] + z[40] + n<T>(5,2)*z[23] + z[35];
    z[35]=z[35]*z[28];
    z[40]=z[29]*z[7];
    z[41]=z[40]*z[10];
    z[42]=z[41] - z[29];
    z[43]=n<T>(1,2)*z[5];
    z[25]= - z[25]*z[43];
    z[25]=4*z[23] + z[25];
    z[25]=z[5]*z[25];
    z[25]=z[35] + z[25] - n<T>(1,8)*z[42];
    z[25]=z[3]*z[25];
    z[35]=npow(z[4],2);
    z[42]=n<T>(1,4)*z[35];
    z[44]=z[2]*z[42];
    z[45]=z[5]*z[23];
    z[44]=z[44] + z[45];
    z[44]=z[5]*z[44];
    z[25]=z[25] + z[44] - n<T>(1,8)*z[40];
    z[25]=z[3]*z[25];
    z[18]= - z[18]*z[43];
    z[18]= - 4*z[15] + z[18];
    z[18]=z[5]*z[18];
    z[40]=5*z[4];
    z[44]= - n<T>(9,2)*z[7] + z[40];
    z[44]=z[5]*z[44];
    z[44]=5*z[15] + z[44];
    z[44]=z[44]*z[36];
    z[18]=z[18] + z[44];
    z[18]=z[6]*z[18];
    z[44]=z[42]*z[7];
    z[45]=z[5]*z[15];
    z[45]=z[44] + z[45];
    z[45]=z[5]*z[45];
    z[18]=z[45] + z[18];
    z[18]=z[6]*z[18];
    z[45]=n<T>(1,2)*z[10];
    z[46]=z[29]*z[45];
    z[47]=n<T>(1,2)*z[7];
    z[48]=z[47] - z[4];
    z[49]= - z[48]*z[16]*z[46];
    z[50]=z[11]*z[7];
    z[51]=z[50] + n<T>(1,2);
    z[52]= - n<T>(3,2)*z[24] + z[51];
    z[52]=z[52]*z[28];
    z[52]= - z[4] + z[52];
    z[52]=z[52]*z[27];
    z[53]=npow(z[11],2);
    z[54]=z[8]*z[7]*z[53]*z[31];
    z[52]=z[52] + z[54];
    z[52]=z[8]*z[52];
    z[40]=z[7] - z[40];
    z[31]=z[40]*z[31];
    z[31]=z[31] + z[52];
    z[31]=z[8]*z[31];
    z[17]=z[17] + z[31] + z[25] + z[18] + z[49];
    z[17]=z[1]*z[17];
    z[18]=static_cast<T>(3)- z[37];
    z[18]=z[4]*z[18];
    z[18]=z[32] + z[18];
    z[25]=z[5]*z[11];
    z[31]=3*z[2];
    z[40]=z[31]*z[11];
    z[49]= - static_cast<T>(11)+ z[40];
    z[49]=n<T>(1,2)*z[49] - z[25];
    z[49]=z[5]*z[49];
    z[52]=n<T>(3,2)*z[6];
    z[38]= - z[38]*z[52];
    z[18]=z[38] + 3*z[18] + z[49];
    z[38]= - z[5] + n<T>(7,2)*z[4];
    z[49]=z[38]*z[5];
    z[49]=z[49] - z[30];
    z[49]=z[49]*z[10];
    z[18]=n<T>(1,2)*z[18] - z[49];
    z[18]=z[3]*z[18];
    z[54]=n<T>(13,2)*z[2];
    z[55]=9*z[4];
    z[56]=z[54] + z[55];
    z[56]=z[5]*z[56];
    z[23]= - 9*z[23] + z[56];
    z[39]= - n<T>(5,4)*z[2] + z[39];
    z[39]=z[6]*z[39];
    z[56]=z[20]*z[7];
    z[57]= - z[10]*z[56];
    z[18]=z[18] + z[57] + n<T>(1,2)*z[23] + z[39];
    z[18]=z[3]*z[18];
    z[23]= - z[35]*z[32];
    z[39]=n<T>(1,2)*z[4];
    z[57]= - n<T>(17,2)*z[2] + z[4];
    z[57]=z[57]*z[39];
    z[26]=z[57] + z[26];
    z[26]=z[5]*z[26];
    z[57]=5*z[7];
    z[58]=3*z[6];
    z[59]= - z[57] - z[58];
    z[59]=z[59]*z[36];
    z[60]=z[31]*z[7];
    z[59]= - z[60] + z[59];
    z[59]=z[6]*z[59];
    z[18]=z[18] + n<T>(3,4)*z[41] + z[59] + z[23] + z[26];
    z[18]=z[18]*z[28];
    z[23]=9*z[15];
    z[26]= - n<T>(13,2)*z[7] + z[55];
    z[26]=z[5]*z[26];
    z[26]= - z[22] + z[23] + z[26];
    z[26]=z[26]*z[21];
    z[41]=z[35]*z[47];
    z[59]= - n<T>(17,2)*z[7] - z[4];
    z[59]=z[59]*z[39];
    z[19]=z[59] - z[19];
    z[19]=z[5]*z[19];
    z[19]=z[26] - z[41] + z[19];
    z[19]=z[19]*z[21];
    z[26]=n<T>(3,2)*z[5];
    z[59]= - z[21] + z[34] - z[26];
    z[61]=2*z[50];
    z[62]= - n<T>(3,2) + z[61];
    z[62]=z[11]*z[62];
    z[63]= - static_cast<T>(1)+ z[24];
    z[63]=z[10]*z[63];
    z[62]=z[62] + n<T>(3,4)*z[63];
    z[62]=z[3]*z[62];
    z[62]=z[62] + 2*z[24] - n<T>(1,4) - z[61];
    z[62]=z[3]*z[62];
    z[59]=n<T>(1,4)*z[59] + z[62];
    z[59]=z[3]*z[59];
    z[62]=n<T>(3,2)*z[11];
    z[63]=z[50] - n<T>(1,2);
    z[64]= - z[63]*z[62];
    z[65]=z[50] - 1;
    z[66]=z[65]*z[53];
    z[67]=z[3]*z[66];
    z[64]=z[64] + z[67];
    z[27]=z[8]*z[64]*z[27];
    z[64]=z[6]*z[5];
    z[67]=z[4]*z[12];
    z[27]=z[27] + z[59] + z[67] + n<T>(1,8)*z[64];
    z[27]=z[8]*z[27];
    z[59]=n<T>(11,4)*z[24] - n<T>(3,2) + z[50];
    z[59]=z[3]*z[59];
    z[55]= - z[7] + z[55];
    z[55]=n<T>(1,2)*z[55] + z[59];
    z[55]=z[3]*z[55];
    z[59]=n<T>(1,8)*z[20];
    z[55]=z[59] + z[55];
    z[55]=z[3]*z[55];
    z[59]= - z[5]*z[59];
    z[27]=z[27] + z[59] + z[55];
    z[27]=z[8]*z[27];
    z[55]=3*z[7];
    z[59]= - z[2] + z[55];
    z[42]=z[59]*z[42];
    z[59]=z[32] - z[7];
    z[67]=z[5]*z[4];
    z[68]= - z[59]*z[67];
    z[42]=z[42] + z[68];
    z[42]=z[5]*z[42];
    z[68]=3*z[48] - z[5];
    z[68]=z[5]*z[68];
    z[68]= - 3*z[15] + z[68];
    z[68]=n<T>(1,2)*z[68] - z[30];
    z[68]=z[68]*z[46];
    z[17]=z[17] + z[27] + z[18] + z[68] + z[42] + z[19];
    z[17]=z[1]*z[17];
    z[18]=z[46]*z[2];
    z[19]=z[20]*z[31];
    z[19]=z[19] + z[18];
    z[19]=z[19]*z[45];
    z[27]=z[6]*z[31];
    z[19]=z[27] + z[19];
    z[19]=z[10]*z[19];
    z[19]=n<T>(5,2)*z[2] + z[19];
    z[19]=z[10]*z[19];
    z[27]=z[45]*z[11];
    z[42]= - z[66] - z[27];
    z[68]=npow(z[11],3);
    z[69]=z[65]*z[68];
    z[70]=z[53]*z[10];
    z[71]=z[69] + z[70];
    z[71]=z[3]*z[71];
    z[42]=3*z[42] + z[71];
    z[42]=z[3]*z[42];
    z[71]=3*z[11];
    z[72]= - n<T>(5,4) + z[50];
    z[72]=z[72]*z[71];
    z[42]=n<T>(3,2)*z[42] + z[72] + z[45];
    z[42]=z[3]*z[42];
    z[72]=n<T>(3,4)*z[2];
    z[73]=z[11]*z[72];
    z[19]=z[42] + z[73] + z[19];
    z[19]=z[8]*z[19];
    z[42]=z[13] - z[12];
    z[73]= - z[47] - z[32] + z[42];
    z[73]=z[11]*z[73];
    z[73]=static_cast<T>(1)+ z[73];
    z[73]=z[5]*z[73];
    z[74]=z[34] - z[5];
    z[75]= - z[31] - z[74];
    z[75]=z[75]*z[36];
    z[76]=z[24]*z[29];
    z[77]= - z[4]*z[20];
    z[77]=z[77] - n<T>(1,4)*z[76];
    z[77]=z[10]*z[77];
    z[75]=z[75] + z[77];
    z[75]=z[10]*z[75];
    z[77]=3*z[53];
    z[78]=z[63]*z[77];
    z[79]=static_cast<T>(3)- z[24];
    z[79]=z[79]*z[45];
    z[79]=z[71] + z[79];
    z[79]=z[79]*z[45];
    z[78]=z[78] + z[79];
    z[78]=z[3]*z[78];
    z[79]=z[61] - 1;
    z[71]= - z[79]*z[71];
    z[80]=n<T>(1,2) - z[24];
    z[80]=z[10]*z[80];
    z[71]=z[78] + z[71] + z[80];
    z[71]=z[3]*z[71];
    z[74]= - n<T>(1,2)*z[74] - z[6];
    z[74]=z[10]*z[74];
    z[74]=z[74] - static_cast<T>(1)+ n<T>(19,2)*z[50];
    z[71]=n<T>(1,2)*z[74] + z[71];
    z[71]=z[3]*z[71];
    z[74]=z[11]*z[4];
    z[78]=z[74]*z[42];
    z[80]=z[72] + 4*z[4];
    z[19]=z[19] + z[71] + z[75] + z[73] - z[78] - z[80];
    z[19]=z[8]*z[19];
    z[71]= - z[21] - 25*z[4] + z[26];
    z[71]=z[71]*z[45];
    z[73]=static_cast<T>(9)- 7*z[24];
    z[73]=z[73]*z[45];
    z[75]=3*z[50];
    z[81]=n<T>(1,2) + z[75];
    z[81]=z[11]*z[81];
    z[73]=z[81] + z[73];
    z[73]=z[3]*z[73];
    z[71]=z[73] + z[71] + static_cast<T>(5)- z[75];
    z[71]=z[3]*z[71];
    z[73]=n<T>(1,4)*z[10];
    z[75]= - z[20]*z[73];
    z[81]=z[6] - z[5];
    z[57]=z[71] + z[75] + z[57] - n<T>(23,2)*z[4] - n<T>(9,4)*z[81];
    z[57]=z[57]*z[28];
    z[71]=3*z[4] + n<T>(5,2)*z[5];
    z[71]=z[71]*z[20];
    z[71]=z[71] + z[76];
    z[71]=z[71]*z[73];
    z[75]=2*z[13];
    z[81]= - z[32] + z[12] + z[75];
    z[81]=z[5]*z[81];
    z[82]=n<T>(3,4)*z[6];
    z[83]=z[2] + z[26];
    z[83]=z[83]*z[82];
    z[84]=z[4]*z[42];
    z[19]=z[19] + z[57] + z[71] + z[83] + z[84] + z[81];
    z[19]=z[8]*z[19];
    z[57]=z[32]*z[9];
    z[71]=z[57] - 1;
    z[81]=z[71]*z[4];
    z[72]= - z[72] + z[81];
    z[72]=z[4]*z[72];
    z[83]=z[7] - z[2];
    z[84]= - static_cast<T>(1)- n<T>(3,2)*z[37];
    z[84]=z[6]*z[84];
    z[84]= - n<T>(9,4)*z[83] + z[84];
    z[84]=z[6]*z[84];
    z[67]=z[35] + z[67];
    z[67]=z[5]*z[67];
    z[56]=z[67] + n<T>(9,4)*z[56];
    z[56]=z[10]*z[56];
    z[33]= - z[33] - z[12] + z[4];
    z[33]=z[5]*z[33];
    z[33]=z[56] + z[84] + z[33] - z[60] + z[72];
    z[56]=z[9]*z[31];
    z[56]=static_cast<T>(1)+ z[56];
    z[56]=z[56]*z[52];
    z[67]= - n<T>(37,2) + 9*z[37];
    z[67]=z[4]*z[67];
    z[72]=static_cast<T>(1)- z[25];
    z[72]=z[5]*z[72];
    z[56]=z[56] + z[67] + z[72];
    z[67]=n<T>(5,4)*z[4] + z[5];
    z[67]=z[5]*z[67];
    z[52]= - z[7]*z[52];
    z[52]=z[30] + z[67] + z[52];
    z[52]=z[10]*z[52];
    z[52]=n<T>(1,2)*z[56] + z[52];
    z[56]= - n<T>(11,2) - z[25];
    z[56]=z[56]*z[43];
    z[49]=z[56] - z[49];
    z[49]=z[49]*z[45];
    z[56]=n<T>(3,8)*z[25];
    z[49]=z[49] + static_cast<T>(1)+ z[56];
    z[49]=z[3]*z[49];
    z[49]=n<T>(1,2)*z[52] + z[49];
    z[49]=z[3]*z[49];
    z[33]=n<T>(1,2)*z[33] + z[49];
    z[33]=z[3]*z[33];
    z[49]=z[5]*z[9];
    z[52]=n<T>(3,2) - z[49];
    z[52]=z[52]*z[64];
    z[67]= - n<T>(13,2)*z[4] + z[5];
    z[67]=z[5]*z[67];
    z[23]=z[52] - z[23] + z[67];
    z[23]=z[23]*z[21];
    z[52]=z[48]*z[5];
    z[67]=z[35] - z[52];
    z[67]=z[5]*z[67];
    z[23]=z[23] + z[41] + z[67];
    z[23]=z[6]*z[23];
    z[41]=z[39] + z[5];
    z[67]=z[41]*z[64];
    z[16]=z[4]*z[16];
    z[16]=z[16] + z[67];
    z[16]=z[16]*z[20];
    z[29]=z[29]*z[30];
    z[16]=z[16] + z[29];
    z[16]=z[10]*z[16];
    z[16]=z[23] + z[16];
    z[16]=z[16]*z[45];
    z[23]=z[7] + z[2];
    z[29]=z[23]*z[11];
    z[30]=z[35]*z[29];
    z[35]=z[29]*z[39];
    z[48]=z[35] + z[48];
    z[48]=z[5]*z[48];
    z[67]= - n<T>(1,2)*z[83] - z[4];
    z[67]=z[4]*z[67];
    z[30]=z[48] + z[67] + n<T>(1,2)*z[30];
    z[30]=z[5]*z[30];
    z[48]=z[7]*z[2];
    z[48]=z[48] - n<T>(1,2)*z[15];
    z[67]=static_cast<T>(3)- z[49];
    z[67]=z[67]*z[36];
    z[67]= - z[5] + z[67];
    z[67]=z[6]*z[67];
    z[48]=z[67] + n<T>(3,2)*z[48] + z[52];
    z[48]=z[48]*z[21];
    z[52]=z[59]*z[39];
    z[59]=z[7]*z[13];
    z[52]= - z[59] + z[52];
    z[52]=z[4]*z[52];
    z[16]=z[17] + z[19] + z[33] + z[16] + z[48] + z[52] + z[30];
    z[16]=z[1]*z[16];
    z[17]= - z[50]*z[31];
    z[19]= - z[2] - z[47];
    z[30]=static_cast<T>(9)- n<T>(19,2)*z[37];
    z[30]=z[4]*z[30];
    z[17]=z[17] + 5*z[19] + z[30];
    z[19]=z[34] + 5*z[5];
    z[19]=z[19]*z[45];
    z[19]=z[19] - n<T>(5,2) - z[25];
    z[19]=z[5]*z[19];
    z[19]= - n<T>(5,4)*z[7] + z[19];
    z[19]=z[10]*z[19];
    z[30]= - static_cast<T>(5)+ 3*z[25];
    z[19]=n<T>(1,4)*z[30] + z[19];
    z[30]=z[41]*z[45];
    z[30]=z[30] - static_cast<T>(1)- n<T>(1,4)*z[25];
    z[30]=z[10]*z[5]*z[30];
    z[30]=z[56] + z[30];
    z[30]=z[3]*z[10]*z[30];
    z[19]=n<T>(1,2)*z[19] + z[30];
    z[19]=z[3]*z[19];
    z[30]= - z[12] - z[32];
    z[30]=z[11]*z[30];
    z[30]= - n<T>(1,2)*z[25] - n<T>(1,2) + z[30];
    z[30]=z[5]*z[30];
    z[32]=static_cast<T>(1)- n<T>(9,4)*z[37];
    z[32]=z[32]*z[21];
    z[33]=2*z[4];
    z[41]=z[33] + z[26];
    z[41]=z[5]*z[41];
    z[48]=z[6]*z[7];
    z[41]=z[41] + n<T>(9,8)*z[48];
    z[41]=z[10]*z[41];
    z[17]=z[19] + z[41] + z[32] + n<T>(1,4)*z[17] + z[30];
    z[17]=z[3]*z[17];
    z[19]=z[27] - z[53];
    z[19]=z[19]*z[10];
    z[30]= - z[69] + z[19];
    z[32]=z[70] - z[68];
    z[32]=z[32]*z[10];
    z[41]=npow(z[11],4);
    z[48]=z[41]*z[65];
    z[52]=z[48] - z[32];
    z[52]=z[3]*z[52];
    z[30]=n<T>(9,2)*z[30] + z[52];
    z[30]=z[3]*z[30];
    z[52]= - z[10] + n<T>(15,2)*z[11];
    z[56]=z[10]*z[52];
    z[30]=z[30] + 6*z[66] + z[56];
    z[30]=z[3]*z[30];
    z[56]= - z[31] + z[21];
    z[56]=z[56]*z[20];
    z[56]=z[56] - z[18];
    z[56]=z[10]*z[56];
    z[66]=2*z[2];
    z[67]= - z[66] + z[82];
    z[58]=z[67]*z[58];
    z[56]=z[58] + z[56];
    z[56]=z[10]*z[56];
    z[58]=5*z[2];
    z[56]=z[56] - z[58] + n<T>(7,4)*z[6];
    z[56]=z[10]*z[56];
    z[67]=z[40] + n<T>(7,2);
    z[67]=n<T>(1,2)*z[67];
    z[56]= - z[67] + z[56];
    z[56]=z[10]*z[56];
    z[70]=z[11]*z[65];
    z[30]=z[30] - n<T>(7,4)*z[70] + z[56];
    z[30]=z[8]*z[30];
    z[56]= - z[66] + z[36];
    z[56]=z[56]*z[20];
    z[56]=z[56] - z[18];
    z[56]=z[10]*z[56];
    z[54]= - z[54] + z[6];
    z[54]=z[54]*z[21];
    z[54]=z[54] + z[56];
    z[54]=z[10]*z[54];
    z[56]= - 15*z[2] - z[6];
    z[54]=n<T>(1,4)*z[56] + z[54];
    z[54]=z[10]*z[54];
    z[56]=z[79]*z[68];
    z[70]=npow(z[10],2);
    z[72]=n<T>(1,2)*z[70];
    z[79]= - z[11] - z[45];
    z[79]=z[79]*z[72];
    z[56]=z[56] + z[79];
    z[56]=z[3]*z[56];
    z[61]=z[61] - n<T>(1,2);
    z[79]= - z[61]*z[77];
    z[56]=z[56] + z[79] - n<T>(1,4)*z[70];
    z[56]=z[3]*z[56];
    z[70]=7*z[50];
    z[79]= - static_cast<T>(1)+ z[70];
    z[79]=z[11]*z[79];
    z[82]=z[10]*z[6];
    z[82]= - static_cast<T>(1)+ z[82];
    z[82]=z[10]*z[82];
    z[79]=n<T>(5,2)*z[79] + z[82];
    z[56]=n<T>(1,2)*z[79] + z[56];
    z[56]=z[3]*z[56];
    z[79]=n<T>(1,2)*z[11];
    z[82]= - z[31] - n<T>(11,2)*z[7];
    z[82]=z[82]*z[79];
    z[30]=z[30] + z[56] + z[54] + static_cast<T>(1)+ z[82];
    z[30]=z[8]*z[30];
    z[54]= - z[51]*z[62];
    z[56]=z[38]*z[45];
    z[56]= - static_cast<T>(2)+ z[56];
    z[56]=z[10]*z[56];
    z[24]= - static_cast<T>(1)+ n<T>(1,4)*z[24];
    z[24]=z[10]*z[24];
    z[24]= - z[79] + z[24];
    z[24]=z[10]*z[24];
    z[62]=z[68]*z[7];
    z[24]=z[62] + z[24];
    z[24]=z[3]*z[24];
    z[24]=z[24] + z[54] + z[56];
    z[24]=z[3]*z[24];
    z[54]=z[33] - z[5];
    z[54]=2*z[54] + z[21];
    z[54]=z[10]*z[54];
    z[56]= - static_cast<T>(5)+ n<T>(11,2)*z[50];
    z[24]=z[24] + n<T>(1,2)*z[56] + z[54];
    z[24]=z[3]*z[24];
    z[38]=z[21] + z[38];
    z[38]=z[38]*z[20];
    z[46]=z[4]*z[46];
    z[38]=z[38] + z[46];
    z[38]=z[38]*z[45];
    z[46]=z[6] - 2*z[5] + z[80];
    z[46]=z[6]*z[46];
    z[38]=z[46] + z[38];
    z[38]=z[10]*z[38];
    z[42]= - 2*z[42] + z[23];
    z[42]=z[11]*z[42];
    z[42]= - static_cast<T>(6)+ z[42];
    z[42]=z[5]*z[42];
    z[46]=z[31] - n<T>(5,2)*z[7];
    z[24]=z[30] + z[24] + z[38] + n<T>(5,4)*z[6] + z[42] + 2*z[78] + n<T>(1,2)*
    z[46] + 6*z[4];
    z[24]=z[8]*z[24];
    z[30]=n<T>(1,2)*z[12];
    z[38]=n<T>(1,4)*z[2];
    z[35]=z[43] - z[35] + z[4] + n<T>(3,4)*z[7] - z[38] - z[30] - z[75];
    z[35]=z[5]*z[35];
    z[42]= - n<T>(1,4) - z[49];
    z[42]=z[5]*z[42];
    z[38]=z[42] - z[39] - z[38] - z[55];
    z[42]=npow(z[9],2);
    z[46]=z[42]*z[43];
    z[46]= - z[9] + z[46];
    z[46]=z[5]*z[46];
    z[54]= - z[5]*npow(z[9],3);
    z[54]=n<T>(5,2)*z[42] + z[54];
    z[54]=z[5]*z[54];
    z[54]= - n<T>(3,2)*z[9] + z[54];
    z[54]=z[54]*z[36];
    z[55]=static_cast<T>(1)+ z[57];
    z[46]=z[54] + n<T>(3,4)*z[55] + z[46];
    z[46]=z[6]*z[46];
    z[38]=n<T>(1,2)*z[38] + z[46];
    z[38]=z[6]*z[38];
    z[33]=z[43] - z[47] + z[33];
    z[33]=z[5]*z[33];
    z[46]=z[60] - n<T>(19,2)*z[15];
    z[54]=z[5]*z[42];
    z[54]= - n<T>(5,2)*z[9] + z[54];
    z[43]=z[54]*z[43];
    z[43]=static_cast<T>(1)+ z[43];
    z[43]=z[6]*z[43];
    z[54]=n<T>(13,4) - z[49];
    z[54]=z[5]*z[54];
    z[43]=z[54] + z[43];
    z[43]=z[43]*z[21];
    z[33]=z[43] + n<T>(1,4)*z[46] + z[33];
    z[33]=z[6]*z[33];
    z[34]=z[5]*z[34];
    z[34]=z[34] + z[64];
    z[34]=z[34]*z[20];
    z[43]=z[5]*z[76];
    z[34]=z[34] + z[43];
    z[34]=z[34]*z[73];
    z[33]=z[34] + z[44] + z[33];
    z[33]=z[10]*z[33];
    z[34]=19*z[2] - 29*z[7];
    z[34]=n<T>(1,4)*z[34] - z[81];
    z[34]=z[34]*z[39];
    z[39]=2*z[59];
    z[23]=z[23]*z[4];
    z[43]= - z[39] - n<T>(1,4)*z[23];
    z[43]=z[43]*z[74];
    z[16]=z[16] + z[24] + z[17] + z[33] + z[38] + z[35] + z[43] - z[39]
    + z[34];
    z[16]=z[1]*z[16];
    z[17]=z[63]*z[41];
    z[24]=z[53]*z[72];
    z[17]=z[17] + z[24];
    z[17]=z[17]*z[28];
    z[24]= - z[61]*z[68];
    z[33]= - z[10]*z[11];
    z[33]=z[53] + z[33];
    z[33]=z[10]*z[33];
    z[17]=z[17] + z[24] + z[33];
    z[17]=z[3]*z[17];
    z[24]= - static_cast<T>(1)+ 4*z[50];
    z[24]=z[24]*z[53];
    z[33]= - n<T>(7,2)*z[11] + z[10];
    z[33]=z[10]*z[33];
    z[17]=z[17] + z[24] + z[33];
    z[17]=z[3]*z[17];
    z[19]= - z[68] - z[19];
    z[19]=z[10]*z[19];
    z[19]= - z[48] + z[19];
    z[24]=z[41] + z[32];
    z[24]=z[10]*z[24];
    z[32]=z[65]*npow(z[11],5);
    z[24]=z[32] + z[24];
    z[24]=z[24]*z[28];
    z[19]=3*z[19] + z[24];
    z[19]=z[19]*z[28];
    z[24]= - z[52]*z[45];
    z[24]=z[77] + z[24];
    z[24]=z[10]*z[24];
    z[19]=z[19] + 3*z[69] + z[24];
    z[19]=z[3]*z[19];
    z[24]=z[31] - z[6];
    z[24]=z[24]*z[20];
    z[24]=z[24] + z[18];
    z[24]=z[24]*z[45];
    z[32]=z[6]*z[14];
    z[33]= - static_cast<T>(9)+ z[32];
    z[33]=z[33]*z[36];
    z[31]=z[31] + z[33];
    z[31]=z[6]*z[31];
    z[24]=z[31] + z[24];
    z[24]=z[10]*z[24];
    z[31]=3*z[14];
    z[33]=z[31]*z[6];
    z[33]=z[33] - 7;
    z[34]=z[33]*z[21];
    z[34]=z[58] + z[34];
    z[24]=n<T>(1,2)*z[34] + z[24];
    z[24]=z[10]*z[24];
    z[33]=z[40] - z[33];
    z[24]=n<T>(1,4)*z[33] + z[24];
    z[24]=z[10]*z[24];
    z[33]=z[31] - 7*z[11];
    z[24]=n<T>(1,4)*z[33] + z[24];
    z[24]=z[10]*z[24];
    z[33]=z[31]*z[7];
    z[34]= - z[70] + static_cast<T>(7)+ z[33];
    z[34]=z[11]*z[34];
    z[31]= - z[31] + z[34];
    z[31]=z[11]*z[31];
    z[19]=z[19] + n<T>(1,4)*z[31] + z[24];
    z[19]=z[8]*z[19];
    z[24]=z[66] - z[6];
    z[24]=z[24]*z[20];
    z[24]=z[24] + z[18];
    z[24]=z[10]*z[24];
    z[31]= - n<T>(11,2) + z[32];
    z[31]=z[31]*z[21];
    z[34]=4*z[2];
    z[31]=z[34] + z[31];
    z[31]=z[6]*z[31];
    z[24]=z[31] + z[24];
    z[24]=z[10]*z[24];
    z[31]= - static_cast<T>(7)+ n<T>(3,2)*z[32];
    z[31]=z[31]*z[21];
    z[24]=z[24] + z[34] + z[31];
    z[24]=z[10]*z[24];
    z[24]=z[67] + z[24];
    z[24]=z[10]*z[24];
    z[31]=z[33] - n<T>(7,2)*z[50];
    z[31]=z[11]*z[31];
    z[31]= - n<T>(3,2)*z[14] + z[31];
    z[17]=z[19] + z[17] + n<T>(1,2)*z[31] + z[24];
    z[17]=z[8]*z[17];
    z[19]=z[2] - z[6];
    z[19]=z[19]*z[20];
    z[18]=z[19] + z[18];
    z[18]=z[18]*z[45];
    z[19]=z[14]*z[21];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[19]*z[21];
    z[19]=z[2] + z[19];
    z[19]=z[6]*z[19];
    z[18]=z[19] + z[18];
    z[18]=z[10]*z[18];
    z[18]=z[18] + n<T>(3,2)*z[2] - z[6];
    z[18]=z[10]*z[18];
    z[19]=z[3]*z[41]*z[47];
    z[19]=z[19] - z[62] - z[27];
    z[19]=z[19]*z[28];
    z[20]= - n<T>(3,4) + z[50];
    z[20]=z[11]*z[20];
    z[19]=z[19] + z[20] + z[45];
    z[19]=z[3]*z[19];
    z[20]=z[7]*z[14];
    z[20]=z[29] - static_cast<T>(1)+ z[20];
    z[17]=z[17] + z[19] + n<T>(3,4)*z[20] + z[18];
    z[17]=z[8]*z[17];
    z[18]=z[9]*z[13];
    z[18]=n<T>(3,2) + z[18];
    z[18]=z[18]*z[49];
    z[19]= - z[42]*z[22];
    z[20]= - z[9] + z[14];
    z[20]=z[13]*z[20];
    z[18]=z[19] + z[18] + z[71] + z[20];
    z[18]=z[18]*z[21];
    z[19]=z[37] - z[49];
    z[19]=z[19]*z[21];
    z[19]=z[19] - z[2] + z[26];
    z[19]=z[6]*z[19];
    z[20]=z[2]*z[12];
    z[21]= - z[5]*z[12];
    z[15]=z[19] + z[21] + z[20] - z[15];
    z[15]=z[15]*z[45];
    z[19]=z[20] - 3*z[59];
    z[20]=z[20] - z[59];
    z[20]=z[20]*z[74];
    z[19]=z[20] + n<T>(1,2)*z[19] - z[23];
    z[19]=z[11]*z[19];
    z[20]= - z[50] - z[25];
    z[20]=z[3]*z[20];
    z[20]=z[20] + z[7] + 3*z[5];
    z[20]=z[45]*z[20];
    z[21]= - z[11]*z[12];
    z[21]= - n<T>(1,2) + z[21];
    z[21]=z[21]*z[25];
    z[20]=z[21] - z[51] + z[20];
    z[20]=z[20]*z[28];
    z[21]= - z[13]*z[14];
    z[21]=n<T>(3,2) + z[21];
    z[21]=z[7]*z[21];
    z[22]=z[4]*z[37];
    z[21]=z[22] + z[21] + 3*z[13] - z[2];
    z[22]= - z[9]*z[75];
    z[23]= - z[30] + z[13];
    z[23]=z[11]*z[23];
    z[22]=z[23] - n<T>(1,4) + z[22];
    z[22]=z[5]*z[22];
    z[15]=z[16] + z[17] + z[20] + z[15] + z[18] + z[22] + n<T>(1,2)*z[21] + 
    z[19];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r879(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r879(const std::array<dd_real,31>&);
#endif
