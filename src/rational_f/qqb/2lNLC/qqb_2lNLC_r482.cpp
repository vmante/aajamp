#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r482(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[3];
    z[9]=z[7]*z[3]*z[6];
    z[9]=z[8] + z[9];
    z[9]=z[5]*z[9];
    z[9]=static_cast<T>(1)+ z[9];
    z[9]=z[5]*z[9];
    z[9]=z[9] - z[2];
    z[10]=z[2]*z[7];
    z[11]= - static_cast<T>(1)- n<T>(1,2)*z[10];
    z[11]=z[4]*z[11];
    z[12]=z[3]*z[4];
    z[13]= - z[6]*z[12];
    z[9]=z[13] + z[11] + n<T>(1,2)*z[9];
    z[9]=z[5]*z[9];
    z[11]=z[4]*z[2];
    z[13]= - z[2] - z[4];
    z[13]=z[3]*z[13];
    z[14]=z[11]*z[3];
    z[15]= - z[1]*z[14];
    z[13]=z[15] + z[11] + z[13];
    z[13]=z[1]*z[13]*npow(z[5],3);
    z[10]=z[10] - 1;
    z[15]=z[4]*z[6];
    z[15]= - z[15] + z[10];
    z[15]=z[3]*z[15];
    z[10]= - z[4]*z[10];
    z[10]=z[10] + z[15];
    z[15]=npow(z[5],2);
    z[10]=z[10]*z[15];
    z[10]= - 3*z[14] + z[10];
    z[10]=z[5]*z[10];
    z[10]=z[10] + z[13];
    z[10]=z[1]*z[10];
    z[13]= - z[6] + z[7];
    z[13]=z[3]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[13]*z[15];
    z[13]=z[11] + z[13];
    z[12]= - z[12] + n<T>(1,2)*z[13];
    z[12]=z[5]*z[12];
    z[10]=n<T>(1,2)*z[10] + z[14] + z[12];
    z[10]=z[1]*z[10];
    z[12]= - z[2]*z[8];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[4]*z[12];
    z[12]= - z[2] + z[12];
    z[12]=z[3]*z[12];
    z[9]=z[10] + z[9] + z[11] + z[12];

    r += z[9]*npow(z[1],2);
 
    return r;
}

template double qqb_2lNLC_r482(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r482(const std::array<dd_real,31>&);
#endif
