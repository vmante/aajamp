#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r615(const std::array<T,31>& k) {
  T z[161];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[14];
    z[9]=k[10];
    z[10]=k[2];
    z[11]=k[5];
    z[12]=k[3];
    z[13]=k[4];
    z[14]=k[15];
    z[15]=npow(z[1],2);
    z[16]=2*z[15];
    z[17]=z[11]*z[1];
    z[18]=z[16] + z[17];
    z[19]=z[18]*z[11];
    z[20]=npow(z[1],3);
    z[21]=z[19] + z[20];
    z[22]=npow(z[11],2);
    z[23]=z[21]*z[22];
    z[24]=z[23]*z[7];
    z[25]=4*z[11];
    z[26]=z[21]*z[25];
    z[26]=z[26] + z[24];
    z[26]=z[7]*z[26];
    z[27]=n<T>(9,2)*z[15];
    z[28]=2*z[17];
    z[29]=z[27] + z[28];
    z[29]=z[11]*z[29];
    z[30]=npow(z[1],4);
    z[31]=5*z[30];
    z[32]=z[20]*z[11];
    z[33]= - z[31] - 3*z[32];
    z[34]=n<T>(1,2)*z[5];
    z[33]=z[33]*z[34];
    z[35]=npow(z[1],5);
    z[36]=z[35]*z[5];
    z[36]=z[36] - z[30];
    z[37]= - z[9]*z[36];
    z[38]=n<T>(17,2)*z[20];
    z[26]=z[26] + z[37] + z[33] + z[38] + 3*z[29];
    z[26]=z[8]*z[26];
    z[29]=npow(z[1],6);
    z[33]=z[29]*z[5];
    z[33]=z[33] - z[35];
    z[37]=z[9]*z[33];
    z[37]=z[30] + z[37];
    z[37]=z[9]*z[37];
    z[39]=4*z[20];
    z[40]=z[30]*z[5];
    z[37]=z[37] + z[39] - n<T>(9,2)*z[40];
    z[37]=z[9]*z[37];
    z[41]=4*z[17];
    z[42]=7*z[15];
    z[43]=z[42] + z[41];
    z[43]=z[11]*z[43];
    z[44]=3*z[20];
    z[43]=z[44] + z[43];
    z[43]=z[11]*z[43];
    z[24]=z[43] + 7*z[24];
    z[43]=2*z[7];
    z[24]=z[24]*z[43];
    z[45]=n<T>(1,2)*z[17];
    z[46]=4*z[15];
    z[47]= - z[46] + z[45];
    z[47]=z[11]*z[47];
    z[48]=n<T>(3,2)*z[20];
    z[24]=z[24] - z[48] + z[47];
    z[24]=z[7]*z[24];
    z[47]=7*z[20];
    z[49]=z[15]*z[11];
    z[50]= - z[47] - z[49];
    z[50]=z[50]*z[34];
    z[51]=n<T>(19,2)*z[15];
    z[24]=z[26] + z[24] + z[37] + z[50] - z[51] - z[41];
    z[24]=z[8]*z[24];
    z[26]=2*z[6];
    z[37]=z[21]*z[26];
    z[50]=n<T>(3,2)*z[15];
    z[37]=z[37] + z[50] + z[17];
    z[52]=npow(z[6],2);
    z[37]=z[37]*z[52];
    z[53]=3*z[15];
    z[54]=z[20]*z[6];
    z[55]=z[53] - z[54];
    z[55]=z[55]*z[6];
    z[56]=z[15]*z[5];
    z[57]=z[55] + n<T>(9,2)*z[56];
    z[58]=z[30]*z[9];
    z[59]=z[58]*z[5];
    z[60]=z[20]*z[5];
    z[61]=n<T>(7,4)*z[60] + z[59];
    z[61]=z[9]*z[61];
    z[57]=n<T>(1,2)*z[57] + z[61];
    z[57]=z[9]*z[57];
    z[61]=n<T>(1,2)*z[20];
    z[62]=z[61] + z[58];
    z[63]=z[9]*z[62];
    z[64]=n<T>(11,2)*z[15];
    z[63]= - z[64] + z[63];
    z[63]=z[9]*z[63];
    z[65]=z[20]*z[9];
    z[66]=z[18] + z[65];
    z[67]=z[8]*z[66];
    z[68]=4*z[1];
    z[63]=4*z[67] - z[68] + z[63];
    z[63]=z[8]*z[63];
    z[67]=z[3]*z[60];
    z[67]=n<T>(5,4)*z[56] + z[67];
    z[67]=z[3]*z[67];
    z[69]=z[16] + z[65];
    z[69]=z[9]*z[69];
    z[69]=z[1] + z[69];
    z[69]=z[13]*z[69]*npow(z[8],2);
    z[57]=z[69] + z[63] + z[67] - z[37] + z[57];
    z[57]=z[13]*z[57];
    z[63]=z[53] + z[17];
    z[67]=z[63]*z[11];
    z[67]=z[67] + z[44];
    z[69]=z[67]*z[11];
    z[69]=z[69] + z[30];
    z[70]=3*z[6];
    z[71]=z[70]*z[22];
    z[72]=z[69]*z[71];
    z[72]= - 13*z[23] + z[72];
    z[72]=z[6]*z[72];
    z[73]=z[17] + z[15];
    z[74]=z[73]*z[22];
    z[72]=22*z[74] + z[72];
    z[72]=z[6]*z[72];
    z[75]=z[30]*z[11];
    z[75]=z[75] - z[35];
    z[76]=z[75]*z[5];
    z[76]=z[76] + z[30];
    z[77]=z[49] + z[20];
    z[78]=z[11]*z[77];
    z[78]=z[78] - z[76];
    z[78]=z[2]*z[78];
    z[79]=n<T>(1,2)*z[15];
    z[80]=17*z[17];
    z[81]= - z[79] - z[80];
    z[81]=z[11]*z[81];
    z[82]=z[40]*z[11];
    z[83]= - n<T>(3,2)*z[32] + z[82];
    z[83]=z[5]*z[83];
    z[72]=z[78] + z[83] + z[72] + z[20] + z[81];
    z[72]=z[2]*z[72];
    z[78]=23*z[15];
    z[81]=z[78] + 15*z[17];
    z[81]=z[11]*z[81];
    z[81]=8*z[20] + z[81];
    z[81]=z[11]*z[81];
    z[83]=11*z[15];
    z[84]= - z[83] - z[41];
    z[84]=z[11]*z[84];
    z[84]= - 10*z[20] + z[84];
    z[84]=z[11]*z[84];
    z[85]=3*z[30];
    z[84]= - z[85] + z[84];
    z[86]=z[6]*z[11];
    z[84]=z[84]*z[86];
    z[81]=z[81] + z[84];
    z[81]=z[81]*z[26];
    z[84]=22*z[15];
    z[87]= - z[84] - n<T>(53,2)*z[17];
    z[87]=z[11]*z[87];
    z[81]=z[81] - n<T>(27,2)*z[20] + z[87];
    z[81]=z[6]*z[81];
    z[87]= - z[30] + z[32];
    z[87]=z[5]*z[87];
    z[87]=z[87] - z[61] - z[49];
    z[87]=z[5]*z[87];
    z[88]=z[80] - z[65];
    z[72]=z[72] + z[87] + z[81] + n<T>(31,4)*z[15] + z[88];
    z[72]=z[2]*z[72];
    z[81]=2*z[40];
    z[87]=z[35]*z[9];
    z[89]=z[87]*z[5];
    z[90]= - z[89] - z[48] + z[81];
    z[90]=z[9]*z[90];
    z[90]=z[90] + n<T>(11,4)*z[60] - z[16] - n<T>(3,2)*z[54];
    z[90]=z[9]*z[90];
    z[91]=z[20] + 3*z[40];
    z[33]= - z[2]*z[33];
    z[33]= - z[30] + z[33];
    z[33]=z[2]*z[33];
    z[33]=n<T>(9,4)*z[91] + z[33];
    z[33]=z[2]*z[33];
    z[91]=19*z[15] + 27*z[60];
    z[36]=z[2]*z[36];
    z[36]=z[40] + z[36];
    z[36]=z[3]*z[36];
    z[92]= - z[7]*z[48];
    z[33]=z[36] + z[92] + n<T>(1,4)*z[91] + z[33];
    z[33]=z[3]*z[33];
    z[36]=z[60] + z[59];
    z[36]=z[9]*z[36];
    z[91]=z[58] + z[20];
    z[92]=z[91]*z[8];
    z[93]=n<T>(1,2)*z[9];
    z[94]=z[93]*z[92];
    z[36]=z[36] + z[94];
    z[36]=z[13]*z[36];
    z[94]=z[2]*z[61];
    z[36]=z[36] + n<T>(1,2)*z[92] + z[15] + z[94];
    z[36]=z[4]*z[36];
    z[92]= - 32*z[15] - z[80];
    z[92]=z[11]*z[92];
    z[94]=5*z[17];
    z[95]=13*z[15];
    z[96]=z[94] + z[95];
    z[97]=z[11]*z[96];
    z[98]=11*z[20];
    z[97]=z[98] + z[97];
    z[97]=z[11]*z[97];
    z[97]=z[85] + z[97];
    z[97]=z[6]*z[97];
    z[92]=z[97] - 15*z[20] + z[92];
    z[92]=z[6]*z[92];
    z[92]=z[92] + z[50] + z[94];
    z[92]=z[6]*z[92];
    z[97]=14*z[7];
    z[97]= - z[74]*z[97];
    z[97]= - 9*z[19] + z[97];
    z[97]=z[7]*z[97];
    z[99]=n<T>(7,2)*z[15];
    z[100]=3*z[17];
    z[97]=z[97] - z[99] - z[100];
    z[97]=z[7]*z[97];
    z[101]=z[99] - z[60];
    z[101]=z[5]*z[101];
    z[24]=z[36] + z[57] + z[24] + z[33] + z[97] + z[72] + z[90] + z[101]
    - z[1] + z[92];
    z[24]=z[4]*z[24];
    z[33]=n<T>(9,2)*z[1];
    z[36]=z[16]*z[9];
    z[57]= - z[33] + z[36];
    z[57]=z[9]*z[57];
    z[72]=2*z[2];
    z[90]=z[15]*z[9];
    z[92]=z[1] - z[90];
    z[92]=z[92]*z[72];
    z[97]=z[9]*z[1];
    z[101]=z[97]*z[2];
    z[102]=npow(z[9],2);
    z[103]=z[102]*z[1];
    z[104]= - z[103] + z[101];
    z[104]=z[13]*z[104];
    z[57]=z[104] + z[57] + z[92];
    z[57]=z[2]*z[57];
    z[92]=2*z[1];
    z[104]=z[102]*z[92];
    z[57]=z[104] + z[57];
    z[57]=z[13]*z[57];
    z[104]=z[65] - z[46] + z[17];
    z[104]=z[2]*z[104];
    z[105]= - z[64] - z[65];
    z[105]=z[9]*z[105];
    z[104]=z[104] - n<T>(7,2)*z[1] + z[105];
    z[104]=z[2]*z[104];
    z[105]=z[1] - z[36];
    z[105]=z[9]*z[105];
    z[57]=z[57] + z[105] + z[104];
    z[57]=z[13]*z[57];
    z[104]=z[49] - z[20];
    z[105]= - z[104]*z[72];
    z[105]=z[105] - n<T>(39,2)*z[15] + 10*z[65];
    z[105]=z[2]*z[105];
    z[106]=5*z[1];
    z[107]= - z[106] + 49*z[90];
    z[108]=z[22]*z[1];
    z[109]= - z[3]*z[108];
    z[109]=z[94] + z[109];
    z[109]=z[3]*z[109];
    z[57]=z[57] + z[109] + n<T>(1,2)*z[107] + z[105];
    z[57]=z[13]*z[57];
    z[105]=n<T>(45,2)*z[15] + 8*z[17];
    z[105]=z[11]*z[105];
    z[105]=n<T>(29,2)*z[20] + z[105];
    z[105]=z[11]*z[105];
    z[107]= - z[3]*z[23];
    z[105]=z[105] + z[107];
    z[105]=z[3]*z[105];
    z[45]= - z[15] - z[45];
    z[45]=z[11]*z[45];
    z[45]= - z[61] + z[45];
    z[45]=z[45]*z[22]*z[3];
    z[45]=n<T>(1,2)*z[74] + z[45];
    z[45]=z[8]*z[45];
    z[107]=29*z[15];
    z[109]=11*z[17];
    z[110]= - z[107] - z[109];
    z[111]=n<T>(1,2)*z[11];
    z[110]=z[110]*z[111];
    z[45]=3*z[45] + z[110] + z[105];
    z[45]=z[8]*z[45];
    z[105]=23*z[20];
    z[110]=12*z[49];
    z[112]=z[32]*z[2];
    z[113]=z[112] + z[105] - z[110];
    z[113]=z[2]*z[113];
    z[114]=13*z[17];
    z[115]= - 15*z[15] - z[114];
    z[115]=z[115]*z[111];
    z[116]=z[18]*z[22];
    z[117]=z[3]*z[116];
    z[118]=z[32]*z[7];
    z[115]=z[117] + z[115] - 12*z[118];
    z[115]=z[3]*z[115];
    z[117]=z[7]*z[112];
    z[45]=z[57] + z[45] + z[115] + 12*z[117] + z[113] + z[95] + z[41];
    z[45]=z[14]*z[45];
    z[57]=2*z[11];
    z[113]=z[57]*z[15];
    z[115]=5*z[20];
    z[117]= - z[115] + z[113];
    z[117]=z[5]*z[117];
    z[119]=z[60]*z[11];
    z[120]= - z[113] - z[119];
    z[120]=z[2]*z[120];
    z[117]=z[120] + n<T>(5,2)*z[65] + z[117] - z[95] + z[100];
    z[117]=z[2]*z[117];
    z[120]=9*z[17];
    z[121]=n<T>(59,2)*z[15] + z[120];
    z[121]=z[11]*z[121];
    z[77]=z[77]*z[57];
    z[122]= - z[3]*z[77];
    z[123]=29*z[20];
    z[121]=z[122] + z[123] + z[121];
    z[121]=z[3]*z[121];
    z[122]=z[42] + n<T>(9,2)*z[17];
    z[122]=z[11]*z[122];
    z[124]=6*z[11];
    z[125]= - z[3]*z[21]*z[124];
    z[122]=z[125] + z[122] - z[119];
    z[122]=z[8]*z[122];
    z[125]= - z[42] - 45*z[54];
    z[126]=z[113] + z[115];
    z[127]= - z[5]*z[126];
    z[121]=z[122] + z[121] + n<T>(1,2)*z[125] + z[127];
    z[121]=z[8]*z[121];
    z[122]=n<T>(5,2)*z[118] - n<T>(45,2)*z[20] + z[113];
    z[122]=z[7]*z[122];
    z[125]=z[16] - z[17];
    z[125]=z[11]*z[125];
    z[125]= - z[20] + z[125];
    z[125]=z[3]*z[125];
    z[122]=z[125] + z[122] - z[18];
    z[122]=z[3]*z[122];
    z[125]=2*z[65];
    z[127]= - z[95] + z[125];
    z[127]=z[9]*z[127];
    z[128]= - z[42] + z[28];
    z[128]=z[2]*z[128];
    z[127]=z[128] - z[1] + z[127];
    z[127]=z[2]*z[127];
    z[128]=n<T>(13,2)*z[1];
    z[129]= - z[128] - z[36];
    z[129]=z[9]*z[129];
    z[130]=3*z[1];
    z[131]=z[130] - z[36];
    z[72]=z[131]*z[72];
    z[72]=z[129] + z[72];
    z[72]=z[2]*z[72];
    z[129]=npow(z[2],2);
    z[131]=z[129]*z[97];
    z[132]=z[13]*z[131];
    z[72]=z[72] + 4*z[132];
    z[72]=z[13]*z[72];
    z[132]=z[92] + 3*z[90];
    z[132]=z[9]*z[132];
    z[133]=z[15]*npow(z[3],2);
    z[72]=z[72] + z[133] + z[132] + z[127];
    z[72]=z[13]*z[72];
    z[127]=z[32] + z[30];
    z[132]=6*z[6];
    z[133]=z[127]*z[132];
    z[134]=4*z[49];
    z[133]=z[133] - z[98] - z[134];
    z[133]=z[6]*z[133];
    z[118]=6*z[118] + z[47] - z[134];
    z[118]=z[7]*z[118];
    z[135]=3*z[65];
    z[118]=z[118] + z[133] + z[135];
    z[133]= - z[16] + z[65];
    z[133]=z[9]*z[133];
    z[136]=z[20]*z[3];
    z[137]=z[16] + z[136];
    z[137]=z[3]*z[137];
    z[133]=z[133] + z[137];
    z[133]=z[13]*z[133];
    z[137]= - z[3]*z[30];
    z[137]=z[39] + z[137];
    z[137]=z[3]*z[137];
    z[118]=z[133] + 2*z[118] + z[137];
    z[118]=z[4]*z[118];
    z[133]= - z[54]*z[124];
    z[133]=z[133] - z[20] + z[134];
    z[133]=z[5]*z[133]*z[26];
    z[137]=z[20] + z[134];
    z[137]=z[5]*z[137];
    z[138]=z[49]*z[2];
    z[137]=z[137] - z[138];
    z[139]= - 12*z[119] - n<T>(5,2)*z[112];
    z[139]=z[7]*z[139];
    z[137]=2*z[137] + z[139];
    z[137]=z[7]*z[137];
    z[139]=14*z[90];
    z[140]=7*z[1];
    z[45]=z[45] + z[118] + z[72] + z[121] + z[122] + z[137] + z[117] + 
    z[139] - z[140] + z[133];
    z[45]=z[14]*z[45];
    z[72]=z[42] - z[60];
    z[117]=npow(z[5],2);
    z[118]= - z[72]*z[117];
    z[121]=4*z[60];
    z[122]= - z[53] + z[121];
    z[122]=z[9]*z[5]*z[122];
    z[118]=z[118] + z[122];
    z[118]=z[9]*z[118];
    z[122]=z[102]*z[16];
    z[133]=z[2]*z[52]*z[48];
    z[122]=z[122] + z[133];
    z[122]=z[2]*z[122];
    z[133]=n<T>(3,2)*z[60];
    z[137]= - z[2]*z[20];
    z[137]=z[133] + z[137];
    z[137]=z[102]*z[137];
    z[141]= - z[15] - n<T>(1,2)*z[65];
    z[141]=z[9]*z[141];
    z[142]=n<T>(1,2)*z[1];
    z[141]= - z[142] + z[141];
    z[143]=3*z[3];
    z[141]=z[141]*z[143];
    z[137]=z[141] + z[137];
    z[137]=z[8]*z[137];
    z[141]=4*z[54];
    z[72]= - z[141] + z[72];
    z[72]=z[117]*z[6]*z[72];
    z[72]=z[137] + z[122] + z[72] + z[118];
    z[72]=z[8]*z[72];
    z[118]= - z[6]*z[17];
    z[118]=z[1] + z[118];
    z[118]=z[118]*z[52];
    z[122]=z[28] + z[15];
    z[137]=z[108]*z[6];
    z[144]=z[137] - z[122];
    z[144]=z[6]*z[144];
    z[144]=z[1] + z[144];
    z[145]=z[5]*z[6];
    z[144]=z[144]*z[145];
    z[118]=z[118] + z[144];
    z[118]=z[5]*z[118];
    z[144]=z[1]*npow(z[6],3);
    z[118]=z[144] + z[118];
    z[118]=z[5]*z[118];
    z[146]=npow(z[5],3);
    z[147]=z[146]*z[15];
    z[148]= - z[146]*z[125];
    z[148]=z[147] + z[148];
    z[148]=z[9]*z[148];
    z[149]=z[54] - z[15];
    z[150]=z[6]*z[149]*z[146];
    z[148]=z[150] + z[148];
    z[148]=z[8]*z[148];
    z[150]=z[146]*z[102];
    z[151]=z[150]*z[16];
    z[144]= - z[2]*z[144];
    z[118]=z[148] + z[144] + z[118] + z[151];
    z[118]=z[13]*z[118];
    z[144]=z[16] - z[100];
    z[144]=z[144]*z[86];
    z[144]=z[144] + z[15] + z[94];
    z[144]=z[6]*z[144];
    z[144]= - z[92] + z[144];
    z[144]=z[6]*z[144];
    z[148]=z[6]*z[74];
    z[148]= - z[108] + z[148];
    z[148]=z[6]*z[148];
    z[148]= - z[17] + z[148];
    z[148]=z[6]*z[148];
    z[148]=z[1] + z[148];
    z[148]=z[5]*z[148];
    z[144]=z[144] + z[148];
    z[144]=z[5]*z[144];
    z[148]=z[16]*z[6];
    z[152]=z[148] - z[1];
    z[153]= - z[152]*z[52];
    z[144]=z[153] + z[144];
    z[144]=z[5]*z[144];
    z[148]= - z[148] - 7*z[56];
    z[117]=z[148]*z[117];
    z[148]= - z[5]*z[16];
    z[148]=z[142] + z[148];
    z[153]=3*z[9];
    z[148]=z[5]*z[148]*z[153];
    z[117]=z[117] + z[148];
    z[117]=z[9]*z[117];
    z[137]=z[137] - z[79] - z[28];
    z[137]=z[6]*z[137];
    z[137]=z[1] + z[137];
    z[137]=z[6]*z[137];
    z[137]=z[137] + z[97];
    z[137]=z[2]*z[137];
    z[148]=z[152] - z[56];
    z[148]=z[52]*z[148];
    z[137]=3*z[137] + z[148];
    z[137]=z[2]*z[137];
    z[72]=z[118] + z[72] + z[137] + z[144] + z[117];
    z[72]=z[13]*z[72];
    z[117]=z[79] - 5*z[54];
    z[117]=z[6]*z[117];
    z[118]=z[50] - z[65];
    z[118]=z[118]*z[153];
    z[137]=z[70]*z[30];
    z[144]=z[38] - z[137];
    z[144]=z[6]*z[144];
    z[144]=z[144] - n<T>(3,2)*z[65];
    z[144]=z[2]*z[144];
    z[117]=z[144] + z[117] + z[118];
    z[117]=z[2]*z[117];
    z[66]=z[66]*z[143];
    z[118]=z[60]*z[9];
    z[143]= - z[2]*z[125];
    z[66]=z[66] + z[143] - z[142] + z[118];
    z[66]=z[8]*z[66];
    z[143]=z[15] - z[133];
    z[143]=z[5]*z[143];
    z[144]=7*z[60];
    z[148]=z[15] - z[144];
    z[148]=2*z[148] - z[59];
    z[148]=z[9]*z[148];
    z[143]=z[143] + z[148];
    z[143]=z[9]*z[143];
    z[99]= - z[99] - z[125];
    z[99]=z[9]*z[99];
    z[125]=z[15] + z[65];
    z[125]=z[3]*z[125];
    z[99]=z[125] - n<T>(3,2)*z[1] + z[99];
    z[99]=z[3]*z[99];
    z[125]= - z[95] + 7*z[54];
    z[125]=n<T>(1,2)*z[125] + z[60];
    z[148]=3*z[5];
    z[125]=z[148]*z[6]*z[125];
    z[66]=z[66] + z[99] + z[117] + z[125] + z[143];
    z[66]=z[8]*z[66];
    z[99]=7*z[17];
    z[117]=z[99] + z[53];
    z[117]=z[117]*z[57];
    z[117]=z[117] - z[20];
    z[117]=z[117]*z[6];
    z[109]=z[53] - z[109];
    z[109]=2*z[109] + z[117];
    z[109]=z[6]*z[109];
    z[125]=6*z[1];
    z[109]=z[125] + z[109];
    z[109]=z[6]*z[109];
    z[143]=z[90] - z[33] - z[56];
    z[143]=z[9]*z[143];
    z[63]= - z[63]*z[71];
    z[152]=12*z[15];
    z[154]=z[152] + z[114];
    z[154]=z[11]*z[154];
    z[63]=z[63] + z[44] + z[154];
    z[63]=z[6]*z[63];
    z[63]=z[63] - 10*z[15] - z[80];
    z[63]=z[6]*z[63];
    z[63]=n<T>(1,2)*z[90] + 10*z[1] + z[63];
    z[63]=z[2]*z[63];
    z[154]=z[79] + z[54];
    z[154]=z[154]*z[145];
    z[63]=z[63] + z[143] + z[109] + z[154];
    z[63]=z[2]*z[63];
    z[109]=z[6]*z[21];
    z[109]=z[15] + z[109];
    z[109]=z[6]*z[109];
    z[143]=z[9]*z[54];
    z[109]=z[143] + z[109] + z[56];
    z[109]=z[3]*z[109];
    z[143]=z[5]*z[108];
    z[143]= - z[28] + z[143];
    z[143]=z[5]*z[143];
    z[154]=n<T>(11,2)*z[1];
    z[143]= - z[154] + z[143];
    z[143]=z[5]*z[143];
    z[155]=z[15] + z[54];
    z[155]=z[6]*z[155]*z[93];
    z[37]=z[109] + z[155] + z[37] + z[143];
    z[37]=z[3]*z[37];
    z[109]=z[22]*z[92];
    z[143]=z[20] - z[108];
    z[143]=z[143]*z[86];
    z[109]=z[109] + z[143];
    z[109]=z[6]*z[109];
    z[109]=z[109] - z[18];
    z[109]=z[109]*z[26];
    z[143]=z[23]*z[6];
    z[155]= - 2*z[74] + z[143];
    z[155]=z[6]*z[155];
    z[155]=z[108] + z[155];
    z[155]=z[6]*z[155];
    z[156]=5*z[15];
    z[155]=z[156] + z[155];
    z[155]=z[5]*z[155];
    z[109]=z[109] + z[155];
    z[109]=z[5]*z[109];
    z[155]=22*z[17];
    z[117]= - z[117] - z[51] + z[155];
    z[117]=z[6]*z[117];
    z[117]= - z[125] + z[117];
    z[117]=z[6]*z[117];
    z[109]=z[117] + z[109];
    z[109]=z[5]*z[109];
    z[117]=n<T>(13,2)*z[15] - z[54];
    z[117]=z[6]*z[117];
    z[157]=9*z[15];
    z[158]=z[157] + z[54];
    z[158]=2*z[158] + 3*z[60];
    z[158]=z[5]*z[158];
    z[117]=z[158] - z[154] + z[117];
    z[117]=z[5]*z[117];
    z[158]=17*z[15];
    z[121]=z[158] + z[121];
    z[121]=z[5]*z[121];
    z[33]=z[118] - z[33] + z[121];
    z[33]=z[9]*z[33];
    z[33]=z[117] + z[33];
    z[33]=z[9]*z[33];
    z[33]=z[72] + z[66] + z[37] + z[63] + z[109] + z[33];
    z[33]=z[13]*z[33];
    z[37]=z[158] + 9*z[60];
    z[37]=z[9]*z[37];
    z[37]=n<T>(3,2)*z[37] + z[140] + n<T>(61,2)*z[56];
    z[37]=z[9]*z[37];
    z[63]= - z[44] - z[40];
    z[63]=z[63]*z[153];
    z[63]=z[63] - z[95] - 11*z[60];
    z[63]=z[9]*z[63];
    z[66]= - z[5]*z[96];
    z[63]=z[63] - z[92] + z[66];
    z[63]=z[7]*z[63];
    z[66]=z[5]*z[1];
    z[37]=z[63] + 7*z[66] + z[37];
    z[37]=z[7]*z[37];
    z[63]= - z[140] - 8*z[56];
    z[63]=z[63]*z[153];
    z[63]= - n<T>(41,2)*z[66] + z[63];
    z[63]=z[9]*z[63];
    z[37]=z[63] + z[37];
    z[37]=z[7]*z[37];
    z[63]=z[60] + z[15];
    z[63]=z[63]*z[9];
    z[72]=z[68] + 11*z[56];
    z[72]=2*z[72] + 9*z[63];
    z[72]=z[9]*z[72];
    z[72]=13*z[66] + z[72];
    z[72]=z[7]*z[72];
    z[96]= - z[142] - z[56];
    z[96]=z[9]*z[96];
    z[96]= - n<T>(47,2)*z[66] + 27*z[96];
    z[96]=z[9]*z[96];
    z[72]=z[96] + z[72];
    z[72]=z[7]*z[72];
    z[96]=z[103]*z[5];
    z[72]=24*z[96] + z[72];
    z[72]=z[7]*z[72];
    z[109]= - z[5]*z[53];
    z[109]= - z[1] + z[109];
    z[109]=z[109]*z[153];
    z[66]= - 11*z[66] + z[109];
    z[109]=z[7]*z[9];
    z[66]=z[66]*z[109];
    z[66]=n<T>(27,2)*z[96] + z[66];
    z[96]=npow(z[7],2);
    z[66]=z[66]*z[96];
    z[117]=npow(z[7],3);
    z[118]=z[117]*z[12];
    z[121]=z[118]*z[103];
    z[140]=z[148]*z[121];
    z[66]=z[66] + z[140];
    z[66]=z[12]*z[66];
    z[66]=z[66] + z[151] + z[72];
    z[66]=z[12]*z[66];
    z[72]= - z[146]*z[156];
    z[140]= - z[53] - 2*z[60];
    z[140]=z[5]*z[140];
    z[140]=11*z[1] + z[140];
    z[151]=2*z[5];
    z[140]=z[140]*z[151];
    z[159]=z[90]*z[5];
    z[140]=z[140] + z[159];
    z[140]=z[9]*z[140];
    z[72]=z[72] + z[140];
    z[72]=z[9]*z[72];
    z[140]=z[102]*z[96]*z[48];
    z[160]=z[3]*z[65];
    z[160]=z[160] - z[36];
    z[160]=z[129]*z[160];
    z[140]=z[140] + z[160];
    z[140]=z[3]*z[140];
    z[160]=z[16] - z[136];
    z[160]=z[3]*z[160];
    z[160]=z[160] - z[1];
    z[129]=z[129]*z[160];
    z[160]=z[117]*z[100];
    z[129]=z[160] + z[129];
    z[129]=z[4]*z[129];
    z[150]=z[13]*z[150]*z[15];
    z[150]=6*z[150];
    z[37]=z[66] + z[129] + z[150] + z[140] + z[37] + z[72] + z[131];
    z[37]=z[12]*z[37];
    z[66]=z[108]*z[7];
    z[72]= - 14*z[66] - z[79] - z[17];
    z[72]=z[7]*z[72];
    z[79]=z[2]*z[50];
    z[72]=z[72] + z[79] - z[92] - n<T>(3,2)*z[90];
    z[72]=z[7]*z[72];
    z[79]= - z[15] + z[17];
    z[129]= - z[2]*z[104];
    z[79]=6*z[79] + z[129];
    z[79]=z[2]*z[79];
    z[36]=z[79] - z[128] + z[36];
    z[36]=z[2]*z[36];
    z[79]=6*z[20];
    z[129]= - z[2]*z[30];
    z[129]=z[79] + z[129];
    z[129]=z[2]*z[129];
    z[129]= - z[136] + n<T>(11,4)*z[15] + z[129];
    z[129]=z[2]*z[129];
    z[131]=z[7]*z[20];
    z[131]= - z[15] + z[131];
    z[131]=z[7]*z[131];
    z[129]=n<T>(7,2)*z[131] + z[129];
    z[129]=z[3]*z[129];
    z[131]=z[3]*z[2];
    z[136]= - z[4]*z[61]*z[131];
    z[36]=z[136] + z[129] + z[36] + z[72];
    z[36]=z[4]*z[36];
    z[72]= - n<T>(21,2)*z[65] + z[50] - z[144];
    z[72]=z[9]*z[72];
    z[129]= - z[64] + z[17];
    z[129]=z[129]*z[148];
    z[136]=3*z[58];
    z[140]=2*z[20];
    z[144]=z[136] + z[140];
    z[144]=z[144]*z[9];
    z[160]= - 14*z[17] + z[144];
    z[160]=z[7]*z[160];
    z[72]=z[160] + z[72] + 16*z[1] + z[129];
    z[72]=z[7]*z[72];
    z[56]= - 37*z[1] + 49*z[56];
    z[56]=n<T>(1,2)*z[56] + z[139];
    z[56]=z[9]*z[56];
    z[129]= - z[5]*z[92];
    z[56]=z[72] + z[129] + z[56];
    z[56]=z[7]*z[56];
    z[72]=z[15] + 17*z[65];
    z[72]=z[72]*z[93];
    z[129]= - z[115] - z[136];
    z[129]=z[129]*z[109];
    z[72]=z[72] + z[129];
    z[72]=z[7]*z[72];
    z[129]=z[2]*z[140];
    z[129]=z[129] + z[46] + z[135];
    z[129]=z[129]*z[131];
    z[131]= - z[95] - z[135];
    z[131]=z[9]*z[131];
    z[135]=z[2]*z[65];
    z[131]=n<T>(1,4)*z[131] + z[135];
    z[131]=z[2]*z[131];
    z[72]=z[129] + z[131] + z[72];
    z[72]=z[3]*z[72];
    z[129]= - z[16] - z[60];
    z[129]=z[129]*z[148];
    z[129]=z[106] + z[129];
    z[129]=z[129]*z[151];
    z[129]=z[129] + z[159];
    z[129]=z[9]*z[129];
    z[129]= - 11*z[147] + z[129];
    z[129]=z[9]*z[129];
    z[101]= - z[103] - z[101];
    z[101]=z[2]*z[101];
    z[101]=z[150] + z[129] + z[101];
    z[101]=z[13]*z[101];
    z[129]=z[39] + z[40];
    z[129]=z[129]*z[151];
    z[129]=n<T>(25,4)*z[15] + z[129];
    z[129]=z[5]*z[129];
    z[63]= - z[63] - n<T>(27,2)*z[1] + z[129];
    z[63]=z[9]*z[63];
    z[129]=z[95] + 5*z[60];
    z[129]=z[5]*z[129];
    z[129]= - n<T>(31,2)*z[1] + z[129];
    z[129]=z[5]*z[129];
    z[63]=z[129] + z[63];
    z[63]=z[9]*z[63];
    z[129]= - z[2]*z[15];
    z[129]=z[129] + z[68] - z[90];
    z[129]=z[2]*z[129];
    z[130]=z[130] + n<T>(7,2)*z[90];
    z[130]=z[130]*z[93];
    z[129]=z[130] + z[129];
    z[129]=z[2]*z[129];
    z[130]=z[146]*z[53];
    z[36]=z[37] + z[36] + z[101] + z[72] + z[56] + z[129] + z[130] + 
    z[63];
    z[36]=z[12]*z[36];
    z[37]=z[20]*z[57];
    z[56]=z[22]*z[60];
    z[37]=z[56] + z[30] + z[37];
    z[37]=z[2]*z[37];
    z[56]=z[58] - z[44];
    z[63]=z[30] + z[77];
    z[63]=z[5]*z[63];
    z[37]=z[37] + z[63] + z[134] - z[56];
    z[37]=z[2]*z[37];
    z[63]=z[46] + z[17];
    z[63]=z[63]*z[11];
    z[72]=z[73]*z[11];
    z[73]= - z[20] + z[72];
    z[73]=z[11]*z[73];
    z[73]= - z[30] + z[73];
    z[73]=z[6]*z[73];
    z[73]=z[73] + z[140] + z[63];
    z[73]=z[6]*z[73];
    z[101]=z[77] + z[143];
    z[101]=z[6]*z[101];
    z[101]=z[20] + z[101];
    z[101]=z[5]*z[101];
    z[129]=z[30]*z[6];
    z[130]=z[140] - z[129];
    z[130]=z[9]*z[130];
    z[37]=z[37] + z[130] + z[101] + z[73] + z[156] + z[28];
    z[37]=z[3]*z[37];
    z[73]=z[108] - z[44];
    z[73]=z[73]*z[11];
    z[101]=2*z[30];
    z[73]=z[73] - z[101];
    z[130]= - z[11]*z[73];
    z[131]= - z[44] - z[49];
    z[131]=z[11]*z[131];
    z[131]= - z[85] + z[131];
    z[131]=z[11]*z[131];
    z[131]= - z[35] + z[131];
    z[131]=z[131]*z[86];
    z[130]=z[130] + z[131];
    z[130]=z[6]*z[130];
    z[74]=3*z[74];
    z[130]=z[74] + z[130];
    z[130]=z[130]*z[26];
    z[69]=z[69]*z[22];
    z[63]= - z[79] - z[63];
    z[63]=z[11]*z[63];
    z[131]=4*z[30];
    z[63]= - z[131] + z[63];
    z[63]=z[11]*z[63];
    z[63]= - z[35] + z[63];
    z[63]=z[6]*z[63]*z[22];
    z[63]=4*z[69] + z[63];
    z[63]=z[6]*z[63];
    z[63]= - 6*z[23] + z[63];
    z[63]=z[6]*z[63];
    z[63]=z[74] + z[63];
    z[63]=z[5]*z[63];
    z[134]=z[94] + z[16];
    z[134]=z[134]*z[11];
    z[63]=z[63] - z[134] + z[130];
    z[63]=z[5]*z[63];
    z[95]= - z[95] - z[120];
    z[95]=z[11]*z[95];
    z[95]= - z[39] + z[95];
    z[95]=z[11]*z[95];
    z[80]=36*z[15] + z[80];
    z[80]=z[11]*z[80];
    z[80]=20*z[20] + z[80];
    z[80]=z[80]*z[22];
    z[80]= - z[35] + z[80];
    z[80]=z[6]*z[80];
    z[80]=5*z[95] + z[80];
    z[80]=z[6]*z[80];
    z[95]=42*z[15] + n<T>(101,2)*z[17];
    z[95]=z[11]*z[95];
    z[80]=z[80] + n<T>(11,2)*z[20] + z[95];
    z[80]=z[6]*z[80];
    z[63]=z[63] + z[80] - z[15] - n<T>(39,2)*z[17];
    z[63]=z[5]*z[63];
    z[80]= - z[82] - z[127];
    z[80]=z[80]*z[34];
    z[76]=z[2]*z[76];
    z[76]=z[76] - z[58] + z[140] + z[80];
    z[76]=z[2]*z[76];
    z[80]= - z[119] + z[98] - 3*z[49];
    z[80]=z[80]*z[34];
    z[82]=n<T>(19,4)*z[20] - z[58];
    z[82]=z[9]*z[82];
    z[76]=z[76] + z[82] - z[46] + z[80];
    z[76]=z[2]*z[76];
    z[80]=19*z[20];
    z[82]= - z[80] - z[110];
    z[82]=z[11]*z[82];
    z[95]=z[11]*z[126];
    z[95]=z[131] + z[95];
    z[95]=z[11]*z[95];
    z[95]=z[35] + z[95];
    z[95]=z[95]*z[70];
    z[98]=7*z[30];
    z[82]=z[95] - z[98] + z[82];
    z[95]=4*z[6];
    z[82]=z[82]*z[95];
    z[82]=z[82] + z[123] + 40*z[49];
    z[82]=z[6]*z[82];
    z[101]=z[101] + z[32];
    z[101]=z[101]*z[11];
    z[101]=z[101] + z[35];
    z[123]=z[85] + z[32];
    z[126]= - z[11]*z[123];
    z[126]= - 3*z[35] + z[126];
    z[126]=z[11]*z[126];
    z[126]= - z[29] + z[126];
    z[126]=z[6]*z[126];
    z[126]=3*z[101] + z[126];
    z[126]=z[126]*z[70];
    z[126]= - 10*z[127] + z[126];
    z[126]=z[126]*z[95];
    z[31]=z[31] + n<T>(3,2)*z[87];
    z[31]=z[9]*z[31];
    z[31]=z[31] + n<T>(51,2)*z[20] + z[126];
    z[31]=z[7]*z[31];
    z[126]= - z[105] - n<T>(17,2)*z[58];
    z[126]=z[9]*z[126];
    z[127]=16*z[15];
    z[31]=z[31] + z[126] - z[127] + z[82];
    z[31]=z[7]*z[31];
    z[82]= - z[156] - z[100];
    z[82]=z[82]*z[25];
    z[21]=z[21]*z[86];
    z[21]=4*z[21] - z[38] + z[82];
    z[21]=z[6]*z[21];
    z[38]=z[53] + z[28];
    z[21]=5*z[38] + z[21];
    z[21]=z[6]*z[21];
    z[38]=9*z[20];
    z[53]=z[38] - z[129];
    z[53]=z[6]*z[53];
    z[53]=n<T>(39,2)*z[65] + n<T>(23,2)*z[15] + z[53];
    z[53]=z[53]*z[93];
    z[21]=z[37] + z[31] + z[76] + z[53] + z[63] + z[68] + z[21];
    z[21]=z[3]*z[21];
    z[31]= - z[158] - z[120];
    z[31]=z[31]*z[111];
    z[37]=n<T>(1,2)*z[123] + z[87];
    z[37]=z[2]*z[37];
    z[31]=z[37] + z[31] + z[56];
    z[31]=z[3]*z[31];
    z[37]=z[100] + z[42];
    z[37]=z[37]*z[11];
    z[53]=z[7]*z[116];
    z[53]=z[53] + z[37] + z[112];
    z[53]=z[7]*z[53];
    z[56]= - z[2]*z[62];
    z[31]=z[31] + z[53] + z[56] - z[59] - n<T>(5,2)*z[60] + z[83] + 6*z[17];
    z[31]=z[8]*z[31];
    z[53]=z[6]*z[35];
    z[53]=z[98] - 9*z[53];
    z[53]=z[53]*z[26];
    z[37]=z[53] + z[39] - z[37];
    z[53]=z[79] + 7*z[19];
    z[53]=z[11]*z[53];
    z[29]=z[6]*z[29];
    z[29]= - z[35] + z[29];
    z[29]=z[29]*z[132];
    z[29]=z[53] + z[29];
    z[29]=z[29]*z[43];
    z[29]=z[29] + 2*z[37] + n<T>(1,2)*z[112];
    z[29]=z[7]*z[29];
    z[37]= - 33*z[15] + z[114];
    z[53]= - z[115] + 6*z[129];
    z[53]=z[6]*z[53];
    z[56]=z[39] + n<T>(1,2)*z[49];
    z[59]=z[2]*z[56];
    z[29]=z[29] + z[59] + n<T>(1,2)*z[37] + 7*z[53];
    z[29]=z[7]*z[29];
    z[37]=z[35]*z[102];
    z[53]= - z[85] - 5*z[32];
    z[53]=n<T>(1,2)*z[53] + z[87];
    z[53]=z[2]*z[53];
    z[37]=z[53] + 3*z[56] + z[37];
    z[37]=z[2]*z[37];
    z[53]= - z[2]*z[101];
    z[53]=z[53] + z[87] + z[30] - z[77];
    z[53]=z[2]*z[53];
    z[53]=z[53] + z[91];
    z[53]=z[3]*z[53];
    z[56]=n<T>(19,2)*z[20] + z[58];
    z[56]=z[9]*z[56];
    z[59]=n<T>(3,2)*z[17];
    z[37]=z[53] + z[37] + z[56] + 14*z[15] + z[59];
    z[37]=z[3]*z[37];
    z[53]=z[70]*z[35];
    z[56]= - 17*z[30] + z[53];
    z[56]=z[6]*z[56];
    z[38]= - z[58] + z[38] + n<T>(1,2)*z[56];
    z[38]=z[2]*z[38];
    z[56]= - z[105] + 5*z[129];
    z[56]=z[6]*z[56];
    z[58]=n<T>(13,2)*z[20] - z[58];
    z[58]=z[9]*z[58];
    z[38]=z[38] + z[58] + z[16] + z[56];
    z[38]=z[2]*z[38];
    z[40]= - z[40] + z[89];
    z[40]=z[9]*z[40];
    z[40]=z[40] - n<T>(29,2)*z[15] - 13*z[60];
    z[40]=z[9]*z[40];
    z[56]=z[83] - n<T>(13,2)*z[54];
    z[56]=z[56]*z[70];
    z[58]=3*z[54];
    z[62]=z[15] - z[58];
    z[34]=z[62]*z[34];
    z[29]=z[31] + z[37] + z[29] + z[38] + z[40] + z[34] - z[106] + z[56]
   ;
    z[29]=z[8]*z[29];
    z[31]=z[11]*z[85];
    z[31]=z[35] + z[31];
    z[31]=z[6]*z[31];
    z[31]= - 6*z[32] + z[31];
    z[31]=z[6]*z[31];
    z[31]=z[31] + z[39] + z[49];
    z[31]=z[6]*z[31];
    z[22]= - z[20]*z[22];
    z[34]=npow(z[11],3)*z[54];
    z[22]=z[22] + z[34];
    z[22]=z[22]*z[70];
    z[22]=z[32] + z[22];
    z[22]=z[22]*z[95];
    z[22]= - n<T>(1,2)*z[119] - z[48] + z[22];
    z[22]=z[2]*z[22];
    z[32]=z[75]*z[52];
    z[32]=6*z[32] + z[20] - 7*z[72];
    z[32]=2*z[32] + z[136];
    z[32]=z[7]*z[32];
    z[34]=z[61] + z[110];
    z[34]=z[5]*z[34];
    z[35]= - 17*z[20] - z[136];
    z[35]=z[35]*z[93];
    z[22]=z[32] + z[22] + z[35] + z[34] + 12*z[31] - z[46] + z[94];
    z[22]=z[7]*z[22];
    z[31]=7*z[65] - n<T>(31,2)*z[60] + z[64] + 9*z[54];
    z[31]=z[9]*z[31];
    z[32]=z[20] - z[113];
    z[32]=z[32]*z[71];
    z[34]=z[20] + 9*z[49];
    z[34]=z[11]*z[34];
    z[32]=z[34] + z[32];
    z[32]=z[32]*z[95];
    z[32]=z[32] - z[44] - 28*z[49];
    z[32]=z[6]*z[32];
    z[34]=z[5]*z[104];
    z[32]=n<T>(3,2)*z[34] + z[51] + z[32];
    z[32]=z[2]*z[32];
    z[34]= - z[140] + z[49];
    z[35]= - z[11]*z[58];
    z[34]=2*z[34] + z[35];
    z[34]=z[34]*z[52];
    z[35]= - z[78] - z[59];
    z[35]=z[5]*z[35];
    z[22]=z[22] + z[32] + z[31] + z[35] - n<T>(19,2)*z[1] + 12*z[34];
    z[22]=z[7]*z[22];
    z[31]=n<T>(15,2)*z[65] - n<T>(53,2)*z[15] - z[141];
    z[31]=z[9]*z[31];
    z[32]=z[20] - z[129];
    z[32]=z[6]*z[32];
    z[32]=18*z[32] - z[156] - z[99];
    z[32]=2*z[32] - z[144];
    z[32]=z[7]*z[32];
    z[34]=z[79] - z[129];
    z[34]=z[6]*z[34];
    z[34]= - z[16] + z[34];
    z[34]=z[34]*z[95];
    z[34]=z[128] + z[34];
    z[31]=z[32] + 3*z[34] + z[31];
    z[31]=z[7]*z[31];
    z[32]=z[152]*z[52];
    z[34]=z[127] - z[58];
    z[34]=z[34]*z[26];
    z[34]= - 6*z[90] - n<T>(23,2)*z[1] + z[34];
    z[34]=z[9]*z[34];
    z[31]=z[31] - z[32] + z[34];
    z[31]=z[7]*z[31];
    z[34]=z[152] + z[99];
    z[34]=z[11]*z[34];
    z[35]= - z[131] + z[53];
    z[35]=z[35]*z[132];
    z[34]=z[35] + z[79] + z[34];
    z[34]=z[34]*z[43];
    z[35]=z[80] - 18*z[129];
    z[35]=z[35]*z[95];
    z[34]=z[34] + z[35] - z[46] - n<T>(41,2)*z[17];
    z[34]=z[7]*z[34];
    z[35]= - z[84] + 21*z[54];
    z[26]=z[35]*z[26];
    z[28]=z[28] + z[66];
    z[28]=z[8]*z[28];
    z[26]=z[28] + z[34] - n<T>(5,2)*z[1] + z[26];
    z[26]=z[7]*z[26];
    z[28]=z[9]*z[92];
    z[26]=z[28] + z[26];
    z[26]=z[8]*z[26];
    z[28]=z[8]*z[108];
    z[28]= - n<T>(3,2)*z[28] - 12*z[54] + z[152] + n<T>(11,2)*z[17];
    z[28]=z[8]*z[28];
    z[34]=z[149]*z[153];
    z[34]= - z[1] + z[34];
    z[35]= - z[13]*z[103];
    z[35]=n<T>(5,2)*z[97] + z[35];
    z[35]=z[13]*z[35];
    z[28]=z[35] + 4*z[34] + z[28];
    z[28]=z[14]*z[28];
    z[34]=z[16] + n<T>(5,2)*z[54];
    z[34]=z[34]*z[6];
    z[35]= - z[8]*z[100];
    z[35]=z[35] + z[106] + z[34];
    z[35]=z[8]*z[35];
    z[34]=z[142] - z[34];
    z[34]=z[9]*z[34];
    z[28]=z[28] + z[34] + z[35];
    z[28]=z[14]*z[28];
    z[34]= - z[115] + z[137];
    z[34]=z[6]*z[34];
    z[18]=z[34] + z[18];
    z[18]=z[7]*z[18];
    z[34]=z[46] - z[58];
    z[34]=z[6]*z[34];
    z[18]=z[18] - z[1] + z[34];
    z[18]=z[8]*z[18];
    z[34]=z[92] - z[55];
    z[34]=z[9]*z[34];
    z[16]=z[54] - z[16];
    z[35]= - z[9]*z[16];
    z[35]= - z[1] + z[35];
    z[35]=z[7]*z[35];
    z[18]=z[18] + z[34] + z[35];
    z[18]=z[96]*z[18];
    z[16]=z[16]*z[6];
    z[16]=z[16] + z[1];
    z[34]= - z[9] + z[8];
    z[16]=z[10]*z[117]*z[16]*z[34];
    z[34]= - z[97]*z[118];
    z[16]=z[16] + z[34] + z[18];
    z[16]=z[10]*z[16];
    z[18]= - z[92] - 9*z[90];
    z[18]=z[18]*z[109];
    z[18]=n<T>(15,2)*z[103] + z[18];
    z[18]=z[18]*z[96];
    z[18]=z[18] + 3*z[121];
    z[18]=z[12]*z[18];
    z[34]=z[46] + 9*z[65];
    z[34]=z[9]*z[34];
    z[34]= - 14*z[1] + z[34];
    z[34]=z[7]*z[34];
    z[35]=n<T>(61,2)*z[1] - 15*z[90];
    z[35]=z[9]*z[35];
    z[34]=z[35] + z[34];
    z[34]=z[7]*z[34];
    z[34]=6*z[103] + z[34];
    z[34]=z[7]*z[34];
    z[18]=z[34] + z[18];
    z[18]=z[12]*z[18];
    z[16]=12*z[16] + z[18] + z[28] + z[26] + 3*z[103] + z[31];
    z[16]=z[10]*z[16];
    z[18]=z[67]*z[71];
    z[19]= - z[79] - 13*z[19];
    z[19]=z[11]*z[19];
    z[18]=z[18] - n<T>(3,2)*z[30] + z[19];
    z[18]=z[6]*z[18];
    z[19]=z[158] + z[155];
    z[19]=z[11]*z[19];
    z[18]=z[18] + z[47] + z[19];
    z[18]=z[6]*z[18];
    z[18]=z[138] - z[133] + z[18] - z[50] - z[88];
    z[18]=z[2]*z[18];
    z[19]=34*z[15] - z[99];
    z[19]=z[11]*z[19];
    z[26]= - 37*z[15] + z[99];
    z[26]=z[11]*z[26];
    z[26]= - 12*z[20] + z[26];
    z[26]=z[26]*z[86];
    z[19]=z[26] - z[115] + z[19];
    z[19]=z[6]*z[19];
    z[19]=z[19] + z[157] + 20*z[17];
    z[19]=z[6]*z[19];
    z[26]=z[65] - n<T>(37,4)*z[15] - z[60];
    z[26]=z[9]*z[26];
    z[28]=z[64] + z[58];
    z[28]=z[5]*z[28];
    z[18]=z[18] + z[26] + z[28] - z[125] + z[19];
    z[18]=z[2]*z[18];
    z[19]=z[11]*z[122];
    z[19]= - z[20] + z[19];
    z[19]=z[19]*z[57];
    z[26]= - z[73]*z[86];
    z[19]=z[19] + z[26];
    z[19]=z[6]*z[19];
    z[19]= - z[134] + z[19];
    z[19]=z[6]*z[19];
    z[26]=z[6]*z[69];
    z[23]= - 3*z[23] + z[26];
    z[23]=z[6]*z[23];
    z[23]=z[74] + z[23];
    z[23]=z[23]*z[145];
    z[19]=z[23] - z[42] + z[19];
    z[19]=z[5]*z[19];
    z[23]=z[41] + z[156];
    z[26]=z[23]*z[124];
    z[23]= - z[23]*z[25];
    z[23]= - z[44] + z[23];
    z[23]=z[11]*z[23];
    z[23]=z[30] + z[23];
    z[23]=z[6]*z[23];
    z[23]=z[23] + z[20] + z[26];
    z[23]=z[6]*z[23];
    z[17]=z[23] + z[27] - 23*z[17];
    z[17]=z[6]*z[17];
    z[17]=z[19] + z[154] + z[17];
    z[17]=z[5]*z[17];
    z[19]= - n<T>(1,4)*z[20] - z[81];
    z[19]=z[5]*z[19];
    z[19]=z[65] - z[157] + z[19];
    z[19]=z[9]*z[19];
    z[20]= - z[107] + z[54];
    z[20]=z[6]*z[20];
    z[20]=z[106] + z[20];
    z[15]=8*z[15] - z[58];
    z[15]=2*z[15] - n<T>(15,2)*z[60];
    z[15]=z[5]*z[15];
    z[15]=z[19] + n<T>(1,2)*z[20] + z[15];
    z[15]=z[9]*z[15];

    r += z[15] + z[16] + z[17] + z[18] + z[21] + z[22] + z[24] + z[29]
       + z[32] + z[33] + z[36] + z[45];
 
    return r;
}

template double qqb_2lNLC_r615(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r615(const std::array<dd_real,31>&);
#endif
