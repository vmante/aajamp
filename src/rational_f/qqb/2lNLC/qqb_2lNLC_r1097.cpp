#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1097(const std::array<T,31>& k) {
  T z[101];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[7];
    z[9]=k[15];
    z[10]=k[5];
    z[11]=k[12];
    z[12]=k[14];
    z[13]=k[11];
    z[14]=k[2];
    z[15]=k[17];
    z[16]=k[22];
    z[17]=k[19];
    z[18]=npow(z[2],3);
    z[19]=npow(z[6],2);
    z[20]=npow(z[3],3);
    z[21]= - z[18]*z[19]*z[20];
    z[22]=z[2]*z[8];
    z[23]=n<T>(1,2)*z[22];
    z[24]=npow(z[17],2);
    z[25]=z[24]*z[23];
    z[24]= - z[2]*z[24];
    z[26]=z[2]*z[17];
    z[27]=static_cast<T>(3)- z[26];
    z[27]=z[13]*z[27];
    z[24]=z[27] - z[15] + z[24];
    z[27]=n<T>(1,2)*z[13];
    z[24]=z[24]*z[27];
    z[28]=2*z[15];
    z[29]=n<T>(1,2)*z[6];
    z[30]=z[28] + z[29];
    z[31]= - z[6]*z[30];
    z[32]=3*z[8];
    z[33]=z[32]*z[15];
    z[34]=z[11]*z[15];
    z[35]= - z[5]*z[13];
    z[21]=z[35] + z[21] + z[31] + z[24] + z[25] + n<T>(11,2)*z[34] - z[33];
    z[21]=z[5]*z[21];
    z[24]=11*z[8] - 17*z[5];
    z[25]=2*z[7];
    z[24]=z[25]*z[20]*z[24];
    z[31]=z[20]*z[14];
    z[35]=n<T>(1,2)*z[11];
    z[36]=z[35] + z[8];
    z[37]=z[5]*z[36];
    z[37]=36*z[31] + z[37];
    z[37]=z[5]*z[37];
    z[24]=z[37] + z[24];
    z[24]=z[7]*z[24];
    z[37]=npow(z[14],2);
    z[38]=z[37]*z[20];
    z[39]=2*z[11];
    z[40]=z[39] + z[8];
    z[41]= - 12*z[38] + z[40];
    z[41]=z[5]*z[41];
    z[24]=z[41] + z[24];
    z[24]=z[7]*z[24];
    z[41]=8*z[14];
    z[42]=z[20]*z[41];
    z[43]=npow(z[5],2);
    z[44]= - z[8]*z[43];
    z[44]= - 28*z[20] + z[44];
    z[44]=z[7]*z[44];
    z[45]=z[5]*z[8];
    z[42]=z[44] + z[42] - z[45];
    z[42]=z[7]*z[42];
    z[44]=8*z[38];
    z[42]=z[42] + z[32] + z[44];
    z[46]=z[9]*z[7];
    z[42]=z[42]*z[46];
    z[47]=2*z[8];
    z[24]=z[42] + z[24] - z[29] + z[13] + n<T>(7,2)*z[11] - z[47];
    z[24]=z[9]*z[24];
    z[42]=n<T>(3,2)*z[11];
    z[48]= - z[42] - z[8];
    z[48]=z[48]*z[43];
    z[49]=npow(z[7],2);
    z[50]=z[49]*z[20];
    z[51]=z[45]*z[50];
    z[48]=z[48] + 10*z[51];
    z[48]=z[7]*z[48];
    z[51]=z[27] - z[6];
    z[52]= - n<T>(11,2)*z[11] + z[32] + z[51];
    z[52]=z[5]*z[52];
    z[53]=7*z[11];
    z[54]=5*z[8];
    z[55]=z[53] - z[54];
    z[56]= - z[6] - z[13] - z[55];
    z[56]=z[4]*z[56];
    z[57]=z[29]*z[13];
    z[58]=z[12]*z[13];
    z[24]=z[24] + z[56] + z[48] - 8*z[58] + z[57] + z[52];
    z[24]=z[9]*z[24];
    z[48]=z[6]*z[2];
    z[52]=3*z[48];
    z[56]= - z[2]*z[39];
    z[56]=z[56] - z[52];
    z[59]=2*z[20];
    z[56]=z[56]*z[59];
    z[60]=n<T>(1,2)*z[8];
    z[61]=4*z[11];
    z[62]= - z[61] - z[60];
    z[63]=npow(z[12],2);
    z[62]=z[62]*z[63];
    z[64]=z[2] + z[14];
    z[65]= - z[64]*z[59];
    z[66]=z[12]*z[11];
    z[65]=z[65] + z[66];
    z[67]=2*z[4];
    z[65]=z[65]*z[67];
    z[56]=z[65] + z[56] + z[62];
    z[62]=npow(z[4],2);
    z[56]=z[56]*z[62];
    z[65]=z[12]*z[14];
    z[59]= - z[65]*z[59];
    z[68]=z[20]*z[7];
    z[69]=3*z[13];
    z[70]=z[8] - z[69];
    z[70]=z[70]*z[68];
    z[59]=z[59] + z[70];
    z[70]=z[6] - z[11];
    z[71]=z[13] - z[8];
    z[72]=z[71] - z[70];
    z[72]=z[72]*z[62];
    z[31]=z[68] + z[31];
    z[68]= - z[58] + z[31];
    z[73]=z[71]*z[4];
    z[68]=4*z[68] - z[73];
    z[74]=2*z[9];
    z[68]=z[68]*z[74];
    z[59]=z[68] + 2*z[59] + z[72];
    z[59]=z[9]*z[59];
    z[68]=npow(z[4],3);
    z[72]=z[70]*z[68];
    z[59]=2*z[72] + z[59];
    z[59]=z[9]*z[59];
    z[72]=5*z[11];
    z[75]= - z[20]*z[72];
    z[76]=z[20]*z[12];
    z[75]=z[75] + 3*z[76];
    z[77]=4*z[20];
    z[78]=z[63]*z[11];
    z[79]= - z[77] - z[78];
    z[79]=z[4]*z[79];
    z[75]=2*z[75] + z[79];
    z[75]=z[4]*z[75];
    z[79]=z[20]*z[66];
    z[75]=10*z[79] + z[75];
    z[75]=z[4]*z[75];
    z[79]=z[13]*z[20];
    z[79]=z[79] + z[76];
    z[80]= - z[20]*z[74];
    z[79]=3*z[79] + z[80];
    z[79]=z[9]*z[79];
    z[80]=z[20]*z[58];
    z[79]= - 5*z[80] + z[79];
    z[80]=4*z[9];
    z[79]=z[79]*z[80];
    z[75]=z[75] + z[79];
    z[75]=z[10]*z[75];
    z[79]= - z[6]*z[20];
    z[76]=z[79] - z[76];
    z[79]=npow(z[13],2);
    z[76]=z[7]*z[79]*z[76];
    z[56]=z[75] + z[59] + z[76] + z[56];
    z[56]=z[10]*z[56];
    z[59]=npow(z[2],2);
    z[75]=z[59]*z[6];
    z[76]=z[20]*z[75];
    z[81]=4*z[38] + z[72] - z[8];
    z[81]=z[12]*z[81];
    z[82]=2*z[37];
    z[64]= - z[2]*z[64];
    z[64]= - z[82] + z[64];
    z[83]=z[77]*z[4];
    z[64]=z[64]*z[83];
    z[64]=z[64] + 30*z[76] + z[81];
    z[64]=z[4]*z[64];
    z[76]= - 6*z[11] - n<T>(5,2)*z[8];
    z[63]=z[76]*z[63];
    z[63]=z[63] + z[64];
    z[63]=z[4]*z[63];
    z[38]= - 4*z[13] + z[38];
    z[64]=2*z[12];
    z[38]=z[38]*z[64];
    z[76]=7*z[8];
    z[81]=z[76] + z[13];
    z[81]=z[81]*z[50];
    z[38]=z[38] + z[81];
    z[31]=z[7]*z[31];
    z[31]= - 8*z[31] - z[44] + z[54] + z[13];
    z[31]=z[9]*z[31];
    z[44]=z[13] + z[8];
    z[54]=z[11] - z[44];
    z[54]=3*z[54] - z[6];
    z[54]=z[4]*z[54];
    z[31]=z[31] + 2*z[38] + z[54];
    z[31]=z[9]*z[31];
    z[38]= - z[6] - z[72] + z[44];
    z[38]=z[38]*z[62];
    z[31]=z[38] + z[31];
    z[31]=z[9]*z[31];
    z[38]=z[6]*z[16];
    z[54]=npow(z[16],2);
    z[81]=z[38] + z[54];
    z[81]=z[81]*z[6];
    z[84]=z[54]*z[11];
    z[81]=z[81] - z[84];
    z[81]=n<T>(1,2)*z[81];
    z[85]=z[12]*z[81];
    z[50]=z[13]*z[19]*z[50];
    z[31]=z[56] + z[31] + z[63] + z[85] + z[50];
    z[31]=z[10]*z[31];
    z[50]=z[2] - z[14];
    z[56]=z[50]*z[2];
    z[63]=z[56] + z[37];
    z[85]=2*z[5];
    z[86]=npow(z[3],4);
    z[87]= - z[63]*z[59]*z[86]*z[85];
    z[88]=z[6]*npow(z[2],4);
    z[89]=5*z[86];
    z[90]=z[88]*z[89];
    z[87]= - z[90] + z[87];
    z[87]=z[4]*z[87];
    z[90]=z[5]*z[90];
    z[87]=z[90] + z[87];
    z[87]=z[87]*z[62];
    z[90]=npow(z[9],3);
    z[91]=z[90]*z[58];
    z[92]=z[68]*z[66];
    z[92]=z[92] - 2*z[91];
    z[92]=npow(z[10],5)*z[92];
    z[93]=npow(z[7],5)*z[90]*z[45];
    z[92]=z[92] + z[93];
    z[92]=z[92]*npow(z[3],5);
    z[93]=z[3]*z[2];
    z[93]=z[6]*z[68]*npow(z[93],5)*z[85];
    z[92]=z[93] + z[92];
    z[92]=z[1]*z[92];
    z[87]=z[87] + z[92];
    z[92]=z[37]*z[86];
    z[93]=z[92]*z[64];
    z[94]= - z[86]*z[75];
    z[94]=z[94] + z[93];
    z[94]=z[94]*z[68];
    z[44]=z[44]*z[86]*z[49];
    z[44]=z[93] + z[44];
    z[44]=z[44]*z[90];
    z[44]=z[94] + z[44];
    z[93]=z[66]*z[89];
    z[94]=z[86]*z[11];
    z[95]=z[86]*z[12];
    z[96]= - z[94] + z[95];
    z[67]=z[96]*z[67];
    z[67]=z[93] + z[67];
    z[67]=z[67]*z[62];
    z[93]= - z[58]*z[89];
    z[96]=z[86]*z[13];
    z[95]=z[96] + z[95];
    z[95]=z[95]*z[74];
    z[93]=z[93] + z[95];
    z[95]=npow(z[9],2);
    z[97]=2*z[95];
    z[93]=z[93]*z[97];
    z[67]=z[67] + z[93];
    z[67]=z[10]*z[67];
    z[93]=z[65]*z[86];
    z[94]= - z[2]*z[94];
    z[94]=z[94] + z[93];
    z[94]=z[94]*z[68];
    z[96]= - z[7]*z[96];
    z[93]= - z[93] + z[96];
    z[93]=z[93]*z[90];
    z[93]=z[94] + 2*z[93];
    z[67]=2*z[93] + z[67];
    z[67]=z[10]*z[67];
    z[44]=2*z[44] + z[67];
    z[44]=z[10]*z[44];
    z[67]=4*z[86];
    z[18]=z[18]*z[6];
    z[93]=z[68]*z[18]*z[67];
    z[86]=z[32]*z[86];
    z[90]=npow(z[7],3)*z[90]*z[86];
    z[90]=z[93] + z[90];
    z[44]=2*z[90] + z[44];
    z[90]=2*z[10];
    z[44]=z[44]*z[90];
    z[93]=z[79] - z[19];
    z[94]=npow(z[15],2);
    z[93]=z[94]*z[93];
    z[96]=z[79]*z[5];
    z[98]= - z[15]*z[96];
    z[98]=z[98] + z[93];
    z[98]=z[5]*z[98];
    z[99]=z[19]*z[15];
    z[100]=z[12]*z[99];
    z[93]=z[100] - z[93];
    z[93]=z[12]*z[93];
    z[93]=z[98] + z[93];
    z[93]=z[7]*z[93];
    z[96]=z[99] + z[96];
    z[96]=z[5]*z[96];
    z[98]=z[16] - z[15];
    z[98]=z[6]*z[98];
    z[54]=z[54] + z[98];
    z[54]=z[6]*z[54];
    z[54]= - z[84] + z[54];
    z[54]=z[12]*z[54];
    z[54]=z[93] + z[96] + z[54];
    z[84]=z[5]*z[14];
    z[93]=z[84]*z[89];
    z[67]= - z[5]*z[67];
    z[67]=z[86] + z[67];
    z[67]=z[7]*z[67];
    z[67]=z[93] + z[67];
    z[67]=z[7]*z[67];
    z[86]= - z[85]*z[92];
    z[67]=z[86] + z[67];
    z[49]=z[67]*z[49]*z[74];
    z[45]=npow(z[7],4)*z[89]*z[45];
    z[45]=z[45] + z[49];
    z[45]=z[45]*z[97];
    z[44]=z[44] + z[45] + n<T>(1,2)*z[54] + 4*z[87];
    z[44]=z[1]*z[44];
    z[45]=3*z[16] - 11*z[15];
    z[35]=z[45]*z[35];
    z[30]= - n<T>(3,2)*z[16] + z[30];
    z[30]=z[6]*z[30];
    z[45]= - z[29] - z[61] - n<T>(7,2)*z[8];
    z[45]=z[12]*z[45];
    z[49]=z[27]*z[15];
    z[30]=z[45] + z[30] + z[49] + z[35] + z[33];
    z[30]=z[12]*z[30];
    z[33]=z[28]*z[6];
    z[33]=z[33] + n<T>(3,2)*z[94];
    z[33]=z[33]*z[6];
    z[35]=z[28]*z[13];
    z[35]=z[35] + n<T>(1,2)*z[94];
    z[35]=z[35]*z[13];
    z[45]=z[11] + z[8];
    z[45]=z[94]*z[45];
    z[33]=z[35] - z[33] + n<T>(5,2)*z[45];
    z[35]=z[8]*z[15];
    z[45]=z[49] + n<T>(3,2)*z[34] + z[35];
    z[45]=z[5]*z[45];
    z[49]=npow(z[48],2);
    z[54]= - z[20]*z[49];
    z[45]=z[45] + z[54] - z[33];
    z[45]=z[5]*z[45];
    z[54]=z[6]*z[15];
    z[67]=z[54] - z[35];
    z[67]=z[34] - n<T>(3,2)*z[67];
    z[67]=z[12]*z[67];
    z[33]=z[67] + z[33];
    z[33]=z[12]*z[33];
    z[33]=z[45] + z[33];
    z[33]=z[7]*z[33];
    z[45]= - z[37] - 5*z[56];
    z[45]=z[5]*z[45]*z[77];
    z[67]=3*z[2];
    z[77]= - z[14] + z[67];
    z[77]=z[2]*z[77];
    z[77]= - z[82] + z[77];
    z[77]=z[77]*z[83];
    z[45]=z[77] + z[45];
    z[45]=z[2]*z[45];
    z[20]=z[18]*z[20];
    z[45]=z[27] - 44*z[20] + z[45];
    z[45]=z[4]*z[45];
    z[20]=z[5]*z[20];
    z[55]= - z[29] + z[55];
    z[55]=z[12]*z[55];
    z[20]=z[45] + z[55] + z[57] + 20*z[20];
    z[20]=z[4]*z[20];
    z[20]=z[44] + z[31] + z[24] + z[20] + z[33] + z[30] - z[81] + z[21];
    z[20]=z[1]*z[20];
    z[21]=npow(z[3],2);
    z[24]= - z[61] - z[6];
    z[24]=z[24]*z[21];
    z[30]=z[21] - 3*z[66];
    z[30]=z[30]*z[64];
    z[24]=z[24] + z[30];
    z[30]=6*z[21];
    z[31]=27*z[11] - n<T>(13,2)*z[12];
    z[31]=z[12]*z[31];
    z[33]=2*z[6];
    z[44]= - z[33] + 15*z[12];
    z[44]=z[4]*z[44];
    z[31]=z[44] - z[30] + z[31];
    z[31]=z[4]*z[31];
    z[24]=2*z[24] + z[31];
    z[24]=z[4]*z[24];
    z[31]= - z[21] - 13*z[58];
    z[44]=z[69] + z[12];
    z[44]=z[9]*z[44];
    z[31]=8*z[44] + 4*z[31] - z[73];
    z[31]=z[9]*z[31];
    z[44]=z[21]*z[12];
    z[45]=z[13]*z[21];
    z[45]=z[45] + z[44];
    z[55]=z[70]*z[62];
    z[31]=z[31] + 8*z[45] + z[55];
    z[31]=z[9]*z[31];
    z[45]=z[10]*z[78]*z[68];
    z[55]=z[4]*z[12];
    z[61]=z[72] - z[12];
    z[61]=z[61]*z[55];
    z[68]=4*z[78];
    z[61]= - z[68] + z[61];
    z[61]=z[61]*z[62];
    z[61]= - z[45] + z[61] - 12*z[91];
    z[61]=z[61]*z[90];
    z[73]=z[6]*z[13];
    z[77]= - z[21]*z[73];
    z[72]=z[72] - 9*z[13];
    z[72]=z[72]*z[44];
    z[24]=z[61] + z[31] + z[24] + z[77] + z[72];
    z[24]=z[10]*z[24];
    z[31]=8*z[12];
    z[60]=11*z[11] + z[60];
    z[60]= - z[31] + 3*z[60] - z[29];
    z[60]=z[12]*z[60];
    z[61]=z[2]*z[11];
    z[61]= - z[61] + 8*z[48];
    z[72]=2*z[21];
    z[61]=z[61]*z[72];
    z[30]= - z[2]*z[30];
    z[30]= - 7*z[4] + 28*z[12] + z[6] + z[30];
    z[30]=z[4]*z[30];
    z[30]=z[30] + z[60] + z[57] + z[61];
    z[30]=z[4]*z[30];
    z[33]=z[33] - z[39] + z[71];
    z[33]=z[4]*z[33];
    z[39]=z[71]*z[7];
    z[60]= - z[72]*z[39];
    z[33]=z[33] - 17*z[58] + z[60];
    z[60]= - z[7] - z[14];
    z[61]=4*z[21];
    z[60]=z[61]*z[60];
    z[39]= - static_cast<T>(4)- z[39];
    z[39]=z[39]*z[74];
    z[71]=z[8] + 11*z[13];
    z[39]=z[39] + 16*z[12] + 3*z[71] + z[60];
    z[39]=z[9]*z[39];
    z[33]=2*z[33] + z[39];
    z[33]=z[9]*z[33];
    z[27]=z[19]*z[27];
    z[39]=z[11]*z[16];
    z[38]=z[39] - z[38];
    z[38]=n<T>(3,2)*z[38] - 8*z[66];
    z[38]=z[12]*z[38];
    z[39]=z[11] + z[69];
    z[39]=z[7]*z[39]*z[44];
    z[24]=z[24] + z[33] + z[30] + z[39] + z[27] + z[38];
    z[24]=z[10]*z[24];
    z[27]=z[75]*z[21];
    z[23]= - z[17]*z[23];
    z[23]=z[28] + z[23];
    z[30]=n<T>(1,2)*z[5];
    z[26]= - static_cast<T>(1)+ n<T>(3,2)*z[26];
    z[26]=z[13]*z[26];
    z[33]=static_cast<T>(3)- n<T>(1,2)*z[48];
    z[33]=z[6]*z[33];
    z[23]=z[30] + 12*z[27] + z[33] + 3*z[23] + z[26];
    z[23]=z[5]*z[23];
    z[26]=z[34] + z[35];
    z[33]=z[13]*z[15];
    z[34]=3*z[15];
    z[35]= - z[34] - z[6];
    z[35]=z[6]*z[35];
    z[33]=z[35] + 3*z[26] + z[33];
    z[35]= - z[22] + z[52];
    z[35]=z[35]*z[21];
    z[38]=z[7]*z[8];
    z[39]=z[38]*z[61];
    z[33]=z[39] + n<T>(3,2)*z[33] + z[35];
    z[33]=z[5]*z[33];
    z[29]= - z[79]*z[29];
    z[35]=z[72]*z[2];
    z[19]=z[19]*z[35];
    z[26]=z[26] - z[54];
    z[34]= - z[34] - 7*z[13];
    z[34]=z[13]*z[34];
    z[26]=z[34] - 9*z[26];
    z[26]=n<T>(1,2)*z[26] - 2*z[66];
    z[26]=z[12]*z[26];
    z[19]=z[26] + z[29] + z[19] + z[33];
    z[19]=z[7]*z[19];
    z[26]=z[21]*z[14];
    z[29]= - z[5] + 20*z[26] - z[42] - z[76] - z[51];
    z[29]=z[5]*z[29];
    z[33]=z[76] - 13*z[5];
    z[33]=z[25]*z[21]*z[33];
    z[29]=z[33] - z[57] + z[29];
    z[29]=z[7]*z[29];
    z[30]=z[8] + z[30];
    z[30]=z[5]*z[30];
    z[30]= - 42*z[21] + z[30];
    z[30]=z[7]*z[30];
    z[32]=z[32] - z[5];
    z[26]=z[30] + 12*z[26] - z[13] + z[32];
    z[26]=z[7]*z[26];
    z[30]=z[37]*z[72];
    z[30]= - static_cast<T>(3)+ z[30];
    z[33]=z[38]*z[43];
    z[32]=z[5]*z[32];
    z[32]=z[32] + z[33];
    z[25]=z[32]*z[25];
    z[32]=6*z[8];
    z[25]=z[25] + z[32] - 7*z[5];
    z[25]=z[7]*z[25];
    z[25]= - static_cast<T>(10)+ z[25];
    z[25]=z[25]*z[46];
    z[25]=z[25] + 2*z[30] + z[26];
    z[25]=z[9]*z[25];
    z[26]= - z[37]*z[61];
    z[26]= - static_cast<T>(3)+ z[26];
    z[26]=z[5]*z[26];
    z[25]=z[25] + z[29] + z[31] + z[26] + n<T>(5,2)*z[6] - 9*z[36] + 14*
    z[13];
    z[25]=z[9]*z[25];
    z[26]= - z[37] + 6*z[59];
    z[26]=z[26]*z[61];
    z[29]=z[75] - z[50];
    z[30]=8*z[5];
    z[29]=z[29]*z[30];
    z[31]=z[18] - z[63];
    z[31]=z[31]*z[30];
    z[31]=z[31] - z[75] + 15*z[14] - 7*z[2];
    z[31]=z[4]*z[31];
    z[26]=z[31] + z[29] + z[26] - n<T>(15,2) - z[48];
    z[26]=z[4]*z[26];
    z[29]=2*z[14];
    z[31]=5*z[2];
    z[34]=z[29] - z[31];
    z[34]=z[34]*z[35];
    z[34]=z[34] - static_cast<T>(1)+ z[48];
    z[34]=z[34]*z[85];
    z[26]=z[26] + 19*z[12] + z[34] + z[13] - 34*z[27];
    z[26]=z[4]*z[26];
    z[27]=z[8]*z[17];
    z[34]= - z[17] + z[13];
    z[34]=z[13]*z[34];
    z[27]=z[27] + z[34];
    z[27]=3*z[27] - 5*z[73];
    z[21]=z[21]*z[49];
    z[28]=z[47] - z[28] + n<T>(13,2)*z[11];
    z[28]= - z[64] + 3*z[28] - n<T>(13,2)*z[13];
    z[28]=z[12]*z[28];
    z[19]=z[20] + z[24] + z[25] + z[26] + z[19] + z[28] + z[23] + n<T>(1,2)*
    z[27] + z[21];
    z[19]=z[1]*z[19];
    z[20]= - z[12] + 12*z[11];
    z[21]=z[20]*z[55];
    z[21]= - z[68] + z[21];
    z[21]=z[21]*z[62];
    z[21]= - z[45] + z[21] - 24*z[91];
    z[21]=z[10]*z[21];
    z[23]=static_cast<T>(12)- z[65];
    z[23]=z[12]*z[23];
    z[23]= - z[53] + z[23];
    z[23]=z[4]*z[23];
    z[24]=3*z[12];
    z[25]=32*z[11] - z[24];
    z[25]=z[12]*z[25];
    z[23]=z[25] + z[23];
    z[23]=z[4]*z[23];
    z[23]= - 6*z[78] + z[23];
    z[23]=z[4]*z[23];
    z[25]=z[69] + z[64];
    z[25]=z[9]*z[25];
    z[25]= - 7*z[58] + z[25];
    z[25]=z[25]*z[95];
    z[21]=z[21] + z[23] + 8*z[25];
    z[21]=z[10]*z[21];
    z[20]=z[20]*z[24];
    z[23]=2*z[48];
    z[24]=21*z[65] - static_cast<T>(7)- z[23];
    z[24]=z[4]*z[24];
    z[25]=static_cast<T>(9)- z[65];
    z[25]=z[12]*z[25];
    z[25]=z[25] - 8*z[11] - z[6];
    z[24]=2*z[25] + z[24];
    z[24]=z[4]*z[24];
    z[20]=z[20] + z[24];
    z[20]=z[4]*z[20];
    z[24]= - static_cast<T>(2)- z[65];
    z[25]= - z[8] - z[69];
    z[25]=z[7]*z[25];
    z[24]=4*z[24] + 3*z[25];
    z[24]=z[9]*z[24];
    z[24]=z[24] + 14*z[12] - z[8] + 17*z[13];
    z[24]=z[9]*z[24];
    z[24]= - 23*z[58] + z[24];
    z[24]=z[24]*z[74];
    z[20]=z[21] + z[24] - z[68] + z[20];
    z[20]=z[10]*z[20];
    z[21]= - z[79] - z[73];
    z[21]=z[6]*z[21];
    z[24]= - 3*z[79] - z[66];
    z[24]=z[12]*z[24];
    z[21]=z[21] + z[24];
    z[21]=z[7]*z[21];
    z[24]= - z[41] - z[2];
    z[24]=2*z[24] - 3*z[75];
    z[24]=z[4]*z[24];
    z[23]=z[24] + 26*z[65] - static_cast<T>(9)- z[23];
    z[23]=z[4]*z[23];
    z[24]= - 3*z[11] - z[6];
    z[25]=static_cast<T>(14)- z[65];
    z[25]=z[12]*z[25];
    z[23]=z[23] + 4*z[24] + z[25];
    z[23]=z[4]*z[23];
    z[24]= - static_cast<T>(3)- 4*z[65];
    z[25]= - z[76] - 8*z[13];
    z[25]=z[7]*z[25];
    z[24]=2*z[24] + z[25];
    z[25]= - 19*z[8] + z[13];
    z[25]=z[7]*z[25];
    z[25]=static_cast<T>(16)+ z[25];
    z[25]=z[7]*z[25];
    z[25]=z[41] + z[25];
    z[25]=z[9]*z[25];
    z[24]=2*z[24] + z[25];
    z[24]=z[9]*z[24];
    z[25]=15*z[13];
    z[24]=z[24] + 10*z[12] - z[8] + z[25];
    z[24]=z[9]*z[24];
    z[25]= - z[12] + 19*z[11] - z[25];
    z[25]=z[12]*z[25];
    z[20]=z[20] + z[24] + z[23] + z[21] - 2*z[73] + z[25];
    z[20]=z[10]*z[20];
    z[21]=z[2]*z[63];
    z[21]=z[21] - z[88];
    z[21]=z[5]*z[21];
    z[23]= - 11*z[14] - z[31];
    z[23]=z[2]*z[23];
    z[21]=16*z[21] + 21*z[18] + 8*z[37] + z[23];
    z[21]=z[4]*z[21];
    z[18]=z[56] - z[18];
    z[18]=z[37] + 7*z[18];
    z[18]=z[18]*z[85];
    z[18]=z[18] + 15*z[75] - z[2] - 12*z[14];
    z[18]=2*z[18] + z[21];
    z[18]=z[4]*z[18];
    z[21]=z[75] - z[2];
    z[21]= - 5*z[14] - 9*z[21];
    z[21]=z[21]*z[85];
    z[18]=z[18] + 11*z[65] + z[21] - static_cast<T>(5)+ 18*z[48];
    z[18]=z[4]*z[18];
    z[21]= - z[32] + z[5];
    z[21]=z[21]*z[85];
    z[21]=z[21] - z[33];
    z[21]=z[7]*z[21];
    z[23]=static_cast<T>(28)- z[84];
    z[23]=z[5]*z[23];
    z[21]=z[21] - 23*z[8] + z[23];
    z[21]=z[7]*z[21];
    z[23]=static_cast<T>(2)- z[84];
    z[21]=14*z[23] + z[21];
    z[21]=z[7]*z[21];
    z[23]= - 9*z[8] + z[85];
    z[23]=z[5]*z[23];
    z[23]=z[23] - z[33];
    z[23]=z[7]*z[23];
    z[24]=static_cast<T>(23)- z[84];
    z[24]=z[5]*z[24];
    z[23]=z[23] - 21*z[8] + z[24];
    z[23]=z[7]*z[23];
    z[24]=static_cast<T>(19)- 7*z[84];
    z[23]=2*z[24] + z[23];
    z[23]=z[7]*z[23];
    z[23]= - z[41] + z[23];
    z[23]=z[23]*z[46];
    z[21]=z[23] + z[41] + z[21];
    z[21]=z[9]*z[21];
    z[23]= - z[65] + static_cast<T>(1)- 3*z[84];
    z[24]= - z[85]*z[38];
    z[25]=2*z[13];
    z[26]= - z[8] - z[25] + 4*z[5];
    z[24]=3*z[26] + z[24];
    z[24]=z[7]*z[24];
    z[21]=z[21] + 2*z[23] + z[24];
    z[21]=z[9]*z[21];
    z[23]=4*z[12];
    z[24]=z[11] + z[13];
    z[24]=z[24]*z[23];
    z[25]=z[25] + z[6];
    z[25]=z[6]*z[25];
    z[26]=static_cast<T>(3)- z[48];
    z[26]=z[6]*z[26];
    z[26]=z[8] + z[26];
    z[26]=z[5]*z[26];
    z[24]=z[24] + z[25] + z[26];
    z[24]=z[7]*z[24];
    z[22]= - z[48] + static_cast<T>(4)+ z[22];
    z[22]=z[5]*z[22];
    z[18]=z[19] + z[20] + z[21] + z[18] + z[24] + z[23] + z[22] - 2*
    z[40] + 5*z[6];
    z[18]=z[1]*z[18];
    z[19]= - z[80] - 4*z[4] + 3*z[70] + z[23];
    z[19]=z[10]*z[19];
    z[20]=z[29] - z[67];
    z[20]=z[5]*z[20];
    z[20]=z[20] + static_cast<T>(1)- 7*z[48];
    z[21]=z[8] - z[6];
    z[21]=3*z[21] - z[30];
    z[21]=z[7]*z[21];
    z[22]=z[14] - 4*z[7];
    z[22]=z[22]*z[80];
    z[23]=z[4]*z[2];

    r += z[18] + z[19] + 2*z[20] + z[21] + z[22] + 12*z[23];
 
    return r;
}

template double qqb_2lNLC_r1097(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1097(const std::array<dd_real,31>&);
#endif
