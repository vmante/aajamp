#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r589(const std::array<T,31>& k) {
  T z[79];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[14];
    z[7]=k[12];
    z[8]=k[13];
    z[9]=k[8];
    z[10]=k[15];
    z[11]=k[5];
    z[12]=k[2];
    z[13]=k[3];
    z[14]=k[9];
    z[15]=k[4];
    z[16]=z[4]*z[15];
    z[17]=z[16] + 2;
    z[18]=npow(z[11],2);
    z[19]=z[18]*z[14];
    z[20]= - 3*z[11] - z[19];
    z[20]=z[14]*z[20];
    z[20]=z[20] - z[17];
    z[20]=z[6]*z[20];
    z[21]=n<T>(5,2)*z[4];
    z[22]=2*z[10];
    z[23]=5*z[5];
    z[24]=n<T>(3,2)*z[8];
    z[25]=3*z[9];
    z[26]=2*z[14];
    z[27]=z[11]*z[26];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[14]*z[27];
    z[20]=z[20] + z[22] - z[25] - z[24] + z[21] + z[27] + z[23];
    z[20]=z[6]*z[20];
    z[27]=z[11] - z[15];
    z[27]=z[11]*z[27];
    z[28]=npow(z[11],3);
    z[29]= - z[3]*z[28];
    z[27]=z[27] + z[29];
    z[27]=z[27]*z[25];
    z[29]=z[18]*z[3];
    z[30]=z[29] - z[11];
    z[27]=z[27] - z[30];
    z[27]=z[9]*z[27];
    z[31]=z[3]*z[11];
    z[27]= - z[31] + z[27];
    z[27]=z[9]*z[27];
    z[32]=z[8]*z[11];
    z[19]= - z[11] + z[19];
    z[19]=z[14]*z[19];
    z[19]=z[32] - static_cast<T>(1)+ z[19];
    z[19]=z[6]*z[19];
    z[33]=npow(z[14],2);
    z[34]= - z[11]*z[33];
    z[35]=z[31] - 2;
    z[36]=z[8]*z[35];
    z[19]=z[19] + z[27] + z[36] + z[34] + z[3];
    z[19]=z[7]*z[19];
    z[27]=z[5]*z[15];
    z[34]=z[27] + z[16];
    z[36]= - z[8]*z[34];
    z[37]=3*z[5];
    z[21]=z[36] + z[21] + n<T>(13,2)*z[14] + z[37];
    z[21]=z[8]*z[21];
    z[36]=n<T>(3,2)*z[3];
    z[38]=z[26] - z[36];
    z[38]=z[4]*z[38];
    z[39]= - 14*z[11] + 3*z[15];
    z[29]= - z[29] + z[39];
    z[29]=z[9]*z[29];
    z[29]=z[29] + static_cast<T>(8)- 3*z[31];
    z[29]=z[9]*z[29];
    z[40]=n<T>(1,2)*z[8];
    z[29]=z[29] + z[40] + n<T>(13,2)*z[3] + z[4];
    z[29]=z[9]*z[29];
    z[41]=12*z[9];
    z[42]=z[18]*z[9];
    z[43]=z[42] - z[11];
    z[44]=z[9]*z[43];
    z[44]=static_cast<T>(1)+ z[44];
    z[44]=z[44]*z[41];
    z[45]=3*z[14];
    z[46]=z[45] - z[5];
    z[46]=z[4] + 3*z[46] + z[3];
    z[44]=n<T>(1,2)*z[46] + z[44];
    z[44]=z[10]*z[44];
    z[46]=3*z[33];
    z[47]=z[3]*z[5];
    z[48]=n<T>(3,2)*z[47];
    z[49]=z[5]*z[14];
    z[19]=z[19] + z[20] + z[44] + z[29] + z[21] + z[38] - z[48] - z[46]
    - 2*z[49];
    z[19]=z[7]*z[19];
    z[20]=npow(z[12],2);
    z[21]=z[20]*z[41];
    z[29]=z[18]*z[5];
    z[38]=3*z[12];
    z[44]= - z[38] + z[11];
    z[44]= - z[21] + 4*z[44] - z[29];
    z[44]=z[10]*z[44];
    z[50]=z[41]*z[12];
    z[51]=z[5]*z[11];
    z[44]=z[44] - z[50] - static_cast<T>(1)- 5*z[51];
    z[44]=z[10]*z[44];
    z[52]=13*z[14];
    z[53]= - z[52] + z[23];
    z[54]=6*z[4];
    z[55]=5*z[3];
    z[44]=z[44] - z[41] + z[8] - z[54] + n<T>(1,2)*z[53] - z[55];
    z[44]=z[10]*z[44];
    z[28]=z[5]*z[28];
    z[18]= - z[18] + z[28];
    z[28]=3*z[10];
    z[18]=z[18]*z[28];
    z[18]=z[18] - 7*z[11] + 10*z[29];
    z[18]=z[10]*z[18];
    z[18]=z[18] - static_cast<T>(5)+ 12*z[51];
    z[18]=z[10]*z[18];
    z[53]=4*z[5];
    z[56]=z[14]*z[11];
    z[57]= - static_cast<T>(2)- z[56];
    z[57]=z[14]*z[57];
    z[58]= - z[5]*z[16];
    z[18]=z[18] + z[58] + z[57] + z[53];
    z[18]=z[6]*z[18];
    z[57]=n<T>(3,2)*z[5];
    z[58]= - n<T>(5,2) - z[16];
    z[58]=z[4]*z[58];
    z[58]=z[58] + z[26] + z[57];
    z[58]=z[8]*z[58];
    z[26]= - z[26] + z[57];
    z[26]=z[3]*z[26];
    z[59]=z[16]*z[3];
    z[60]=3*z[3];
    z[61]= - z[59] + z[5] - z[60];
    z[61]=z[4]*z[61];
    z[62]=9*z[14];
    z[63]=z[8] - z[60] - z[62] + 11*z[5];
    z[64]=n<T>(1,2)*z[9];
    z[63]=z[63]*z[64];
    z[18]=z[18] + z[44] + z[63] + z[58] + z[61] + z[46] + z[26];
    z[18]=z[6]*z[18];
    z[26]=2*z[8];
    z[44]= - z[11] + 7*z[29];
    z[44]=z[44]*z[26];
    z[46]=2*z[31];
    z[39]= - z[5]*z[39];
    z[39]=z[44] + z[39] + z[46];
    z[39]=z[9]*z[39];
    z[44]=10*z[51];
    z[58]= - z[11] + z[29];
    z[58]=z[8]*z[58];
    z[58]=z[58] + static_cast<T>(3)- z[44];
    z[58]=z[8]*z[58];
    z[39]=z[39] + z[58] - 8*z[5] - z[60];
    z[39]=z[9]*z[39];
    z[58]=z[3]*z[14];
    z[61]=z[58] - z[49];
    z[63]=8*z[61];
    z[65]=7*z[5];
    z[66]=z[62] - z[65];
    z[67]=n<T>(1,2)*z[4];
    z[66]=z[66]*z[67];
    z[68]=z[65] - z[3];
    z[69]=static_cast<T>(1)- 3*z[51];
    z[69]=z[8]*z[69];
    z[68]=z[69] + n<T>(3,2)*z[68] + z[4];
    z[68]=z[8]*z[68];
    z[39]=z[39] + z[68] + z[63] + z[66];
    z[39]=z[9]*z[39];
    z[66]=z[3]*z[15];
    z[68]= - static_cast<T>(9)+ 8*z[66];
    z[69]=npow(z[15],2);
    z[70]= - z[3]*z[69];
    z[70]=z[15] + z[70];
    z[70]=z[4]*z[70];
    z[50]=z[50] + 2*z[68] + z[70];
    z[50]=z[4]*z[50];
    z[68]=7*z[3];
    z[69]=z[69]*z[68];
    z[69]=z[69] + 18*z[12] - 19*z[15];
    z[21]=z[21] + 2*z[69];
    z[21]=z[4]*z[21];
    z[21]=17*z[66] - static_cast<T>(13)- 2*z[51] + z[21];
    z[21]=z[10]*z[21];
    z[21]=z[21] + z[5] + 11*z[3] + z[50];
    z[21]=z[10]*z[21];
    z[42]=z[42]*z[8];
    z[50]=z[32] - z[42];
    z[69]=4*z[9];
    z[50]=z[50]*z[69];
    z[70]=z[8] - z[4];
    z[71]=z[3] - z[5];
    z[50]=z[50] + n<T>(1,2)*z[71] - 4*z[70];
    z[50]=z[50]*z[25];
    z[52]=31*z[3] + z[52] - z[5];
    z[72]= - static_cast<T>(1)+ z[66];
    z[72]=z[4]*z[72];
    z[52]=n<T>(1,2)*z[52] + z[72];
    z[52]=z[4]*z[52];
    z[72]= - n<T>(7,2)*z[3] - n<T>(9,2)*z[14] + z[5];
    z[72]=z[8]*z[72];
    z[21]=z[21] + z[50] + z[72] + z[63] + z[52];
    z[21]=z[10]*z[21];
    z[50]=z[49] + z[33];
    z[52]=z[4]*z[14];
    z[63]=z[14]*z[13];
    z[72]= - static_cast<T>(2)- z[63];
    z[72]=z[72]*z[52];
    z[72]= - 2*z[50] + z[72];
    z[72]=z[4]*z[72];
    z[73]=2*z[5];
    z[74]= - static_cast<T>(1)+ z[63];
    z[74]=z[14]*z[74];
    z[75]=z[14]*npow(z[13],2);
    z[76]=z[13] - z[75];
    z[76]=z[14]*z[76];
    z[76]= - static_cast<T>(1)+ z[76];
    z[76]=z[4]*z[76];
    z[74]=z[76] + z[74] + z[73];
    z[74]=z[8]*z[74];
    z[76]=z[33] - z[58];
    z[75]=3*z[13] + z[75];
    z[75]=z[14]*z[75];
    z[75]=static_cast<T>(3)+ z[75];
    z[75]=z[4]*z[75];
    z[75]=z[3] + z[75];
    z[75]=z[4]*z[75];
    z[74]=z[74] + 2*z[76] + z[75];
    z[74]=z[8]*z[74];
    z[18]=z[19] + z[18] + z[21] + z[39] + z[72] + z[74];
    z[19]=npow(z[2],2);
    z[18]=z[18]*z[19];
    z[21]= - static_cast<T>(3)- z[56];
    z[21]=z[14]*z[21];
    z[17]= - z[4]*z[17];
    z[17]= - z[10] - z[24] + z[21] + z[17];
    z[17]=z[6]*z[17];
    z[21]=n<T>(1,2)*z[5];
    z[39]=5*z[14];
    z[72]= - z[39] - z[21];
    z[72]=z[5]*z[72];
    z[74]=z[45] + z[5];
    z[74]=n<T>(5,2)*z[74] + z[26];
    z[74]=z[8]*z[74];
    z[75]=11*z[8] - z[25];
    z[75]=z[75]*z[64];
    z[76]= - z[4] + z[39] + z[37] - 9*z[9];
    z[76]=n<T>(1,2)*z[76] - z[28];
    z[76]=z[10]*z[76];
    z[77]=npow(z[4],2);
    z[78]=3*z[77];
    z[17]=z[17] + z[76] + z[75] + z[74] + z[78] + 14*z[33] + z[72];
    z[17]=z[6]*z[17];
    z[30]= - z[15] - z[30];
    z[30]=z[30]*z[25];
    z[31]=static_cast<T>(1)- z[31];
    z[30]=z[30] + 4*z[31] + n<T>(3,2)*z[16];
    z[30]=z[9]*z[30];
    z[30]=z[30] - z[55] - n<T>(11,2)*z[4];
    z[30]=z[9]*z[30];
    z[31]= - z[35] - z[34];
    z[31]=z[8]*z[31];
    z[31]=z[31] + n<T>(9,2)*z[4] - z[21] + 4*z[3];
    z[31]=z[8]*z[31];
    z[34]=static_cast<T>(1)+ z[56];
    z[34]=z[14]*z[34];
    z[35]=npow(z[8],2);
    z[56]= - z[11]*z[35];
    z[34]=z[56] - n<T>(3,2)*z[4] + z[34] + z[5];
    z[34]=z[6]*z[34];
    z[56]=n<T>(1,2)*z[47];
    z[72]=z[7]*z[3];
    z[30]=z[72] + z[34] + z[30] + z[31] + z[49] - z[56];
    z[30]=z[7]*z[30];
    z[31]=z[4] + z[60] + z[39] + z[5];
    z[34]= - z[43]*z[25];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[34]*z[69];
    z[31]=n<T>(1,2)*z[31] + z[34];
    z[31]=z[10]*z[31];
    z[34]=n<T>(1,2)*z[3];
    z[43]=z[14] - z[5];
    z[43]=3*z[43] - z[34];
    z[43]=z[3]*z[43];
    z[60]=z[9]*z[11];
    z[72]= - static_cast<T>(7)+ 9*z[60];
    z[72]=z[72]*z[69];
    z[72]=z[36] + z[72];
    z[72]=z[9]*z[72];
    z[74]=12*z[33];
    z[45]= - z[45] - z[4];
    z[45]=z[4]*z[45];
    z[31]=z[31] + z[72] + z[45] - z[74] + z[43];
    z[31]=z[10]*z[31];
    z[43]= - z[14] - z[57];
    z[43]=3*z[43] - z[34];
    z[43]=z[3]*z[43];
    z[45]= - z[36] + z[4];
    z[45]=z[4]*z[45];
    z[57]= - 3*z[4] + z[73] - z[3];
    z[57]=z[8]*z[57];
    z[43]=z[57] + z[43] + z[45];
    z[43]=z[8]*z[43];
    z[45]=z[3] - z[54];
    z[45]=z[45]*z[26];
    z[54]= - 2*z[3] + z[67];
    z[46]= - static_cast<T>(17)+ z[46];
    z[46]=z[9]*z[46];
    z[46]=7*z[54] + z[46];
    z[46]=z[9]*z[46];
    z[45]=z[45] + z[46];
    z[45]=z[9]*z[45];
    z[46]=npow(z[5],2);
    z[54]=z[46]*z[3];
    z[17]=z[30] + z[17] + z[31] + z[45] + z[54] + z[43];
    z[17]=z[7]*z[17];
    z[30]=12*z[61];
    z[31]= - z[68] + z[59];
    z[31]=z[4]*z[31];
    z[43]= - z[39] + z[68];
    z[40]=z[43]*z[40];
    z[42]=static_cast<T>(1)+ z[42];
    z[42]=z[42]*z[25];
    z[42]=z[42] + z[70];
    z[42]=z[42]*z[69];
    z[20]=z[20]*z[9];
    z[38]= - z[38] - z[20];
    z[38]=z[41]*z[4]*z[38];
    z[38]=z[38] - 17*z[3] - 14*z[59];
    z[38]=z[10]*z[38];
    z[31]=z[38] + z[42] + z[40] - z[30] + z[31];
    z[31]=z[10]*z[31];
    z[38]=z[4]*z[3];
    z[40]=2*z[38];
    z[41]= - z[40] + 5*z[49] - z[48];
    z[41]=z[4]*z[41];
    z[42]= - 7*z[14] - z[3];
    z[42]=z[42]*z[67];
    z[42]=z[42] + 4*z[33] - z[56];
    z[42]=z[8]*z[42];
    z[43]= - z[67] - z[5] + z[34];
    z[43]=z[8]*z[43];
    z[45]= - z[5]*z[67];
    z[43]=z[45] + z[43];
    z[45]= - z[25]*z[32];
    z[45]=z[45] + z[70];
    z[45]=z[45]*z[69];
    z[43]=3*z[43] + z[45];
    z[43]=z[9]*z[43];
    z[31]=z[31] + z[43] + z[41] + 3*z[42];
    z[31]=z[10]*z[31];
    z[41]=z[53] - z[34];
    z[25]=z[12]*z[25];
    z[25]=static_cast<T>(4)+ z[25];
    z[25]=z[25]*z[69];
    z[20]=z[12] + z[20];
    z[20]=z[9]*z[20];
    z[20]=z[51] + 6*z[20];
    z[20]=z[20]*z[22];
    z[20]=z[20] + z[25] - 3*z[41] + z[26];
    z[20]=z[10]*z[20];
    z[22]= - z[34] + z[39] - z[37];
    z[22]=z[3]*z[22];
    z[25]= - z[65] + z[55];
    z[25]=z[25]*z[67];
    z[26]=z[5] + 7*z[4];
    z[24]=z[26]*z[24];
    z[26]=z[36] + z[69];
    z[26]=z[9]*z[26];
    z[20]=z[20] + z[26] + z[24] + z[25] - 3*z[49] + z[22];
    z[20]=z[10]*z[20];
    z[22]= - z[27] + z[66];
    z[22]=z[4]*z[22];
    z[22]=z[22] + z[41];
    z[22]=z[4]*z[22];
    z[24]=z[29]*z[28];
    z[24]=z[24] + z[44] + n<T>(3,2)*z[32];
    z[24]=z[10]*z[24];
    z[24]=z[24] + 7*z[8] + 12*z[5] + z[3];
    z[24]=z[10]*z[24];
    z[25]=z[8]*z[4];
    z[16]=n<T>(9,2) + z[16];
    z[16]=z[16]*z[25];
    z[21]= - z[14] - z[21];
    z[21]=z[3]*z[21];
    z[26]=z[6]*z[5];
    z[16]=z[26] + z[24] + z[16] + z[21] + z[22];
    z[16]=z[6]*z[16];
    z[21]=6*z[33] - z[49];
    z[22]=z[62] + z[23];
    z[22]=n<T>(1,2)*z[22] + z[8];
    z[22]=z[8]*z[22];
    z[23]=z[8] - z[39] + z[3];
    z[23]=z[23]*z[64];
    z[21]=z[23] + 2*z[21] + z[22];
    z[21]=z[9]*z[21];
    z[22]= - z[46] - 9*z[47];
    z[22]=n<T>(1,2)*z[22] + z[40];
    z[22]=z[4]*z[22];
    z[23]= - z[78] + z[25];
    z[23]=z[8]*z[23];
    z[24]=z[5]*npow(z[3],2);
    z[16]=z[16] + z[20] + z[21] + z[23] + z[24] + z[22];
    z[16]=z[6]*z[16];
    z[20]= - z[6]*z[70];
    z[20]=z[20] - n<T>(1,2)*z[46] - z[47];
    z[20]=z[8]*z[20];
    z[21]= - z[8]*z[71];
    z[21]= - z[47] + z[21];
    z[21]=z[7]*z[21];
    z[20]=z[21] + z[54] + z[20];
    z[20]=z[7]*z[20];
    z[21]= - z[35]*z[47];
    z[22]=npow(z[9],3);
    z[23]=static_cast<T>(1)- 2*z[60];
    z[23]=z[10]*z[23]*npow(z[9],2);
    z[23]=2*z[22] + z[23];
    z[23]=z[10]*z[23];
    z[26]= - z[77] + z[25];
    z[26]=z[8]*z[26];
    z[25]= - z[6]*z[25];
    z[25]=z[26] + z[25];
    z[25]=z[6]*z[25];
    z[20]=z[20] + z[25] + z[21] + 12*z[23];
    z[20]=z[7]*z[20];
    z[21]=z[5] + z[34];
    z[21]=z[21]*z[38];
    z[23]= - z[4]*z[71];
    z[23]=z[47] + z[23];
    z[23]=z[6]*z[23];
    z[21]=z[23] - z[24] + z[21];
    z[21]=z[6]*z[21];
    z[23]=z[77]*z[47];
    z[21]=z[23] + z[21];
    z[21]=z[6]*z[21];
    z[22]=z[22]*npow(z[10],2);
    z[22]=12*z[22];
    z[23]=z[8]*npow(z[7],2)*z[47];
    z[23]= - z[22] + z[23];
    z[23]=z[7]*z[23];
    z[24]=z[4]*npow(z[6],3)*z[47];
    z[23]=z[24] + z[23];
    z[23]=z[1]*z[23];
    z[20]=z[23] + z[20] - z[22] + z[21];
    z[20]=z[1]*z[20];
    z[21]=z[39] + z[65];
    z[21]=z[21]*z[67];
    z[22]=z[51]*z[8];
    z[23]= - z[53] + z[22];
    z[23]=z[8]*z[23];
    z[22]=17*z[5] + 14*z[22];
    z[22]=z[9]*z[22];
    z[21]=z[22] + z[23] - z[30] + z[21];
    z[21]=z[9]*z[21];
    z[22]=2*z[58] - n<T>(21,2)*z[52];
    z[22]=z[8]*z[22];
    z[23]= - z[4]*z[74];
    z[21]=z[21] + z[23] + z[22];
    z[21]=z[9]*z[21];
    z[22]= - z[4]*z[13];
    z[22]=static_cast<T>(11)+ z[22];
    z[22]=z[4]*z[33]*z[22];
    z[23]= - static_cast<T>(2)+ z[63];
    z[23]=z[23]*z[52];
    z[24]= - z[14] - z[5];
    z[24]=z[3]*z[24];
    z[23]=z[24] + z[23];
    z[23]=z[8]*z[23];
    z[22]=z[22] + z[23];
    z[22]=z[8]*z[22];
    z[23]= - z[47] + z[50];
    z[23]=z[23]*z[77];
    z[16]=z[20] + z[17] + z[16] + z[31] + z[21] + z[23] + z[22];
    z[16]=z[1]*z[19]*z[16];
    z[16]=z[18] + z[16];

    r += z[16]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r589(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r589(const std::array<dd_real,31>&);
#endif
