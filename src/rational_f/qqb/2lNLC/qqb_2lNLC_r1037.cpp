#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1037(const std::array<T,31>& k) {
  T z[153];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=k[11];
    z[8]=k[5];
    z[9]=k[7];
    z[10]=k[8];
    z[11]=k[12];
    z[12]=k[14];
    z[13]=k[15];
    z[14]=k[17];
    z[15]=k[19];
    z[16]=k[16];
    z[17]=k[4];
    z[18]=k[26];
    z[19]=k[22];
    z[20]=k[2];
    z[21]=k[29];
    z[22]=npow(z[19],2);
    z[23]=z[6]*z[19];
    z[24]=z[22] + z[23];
    z[25]=8*z[6];
    z[26]=z[24]*z[25];
    z[27]=z[15]*z[14];
    z[28]=npow(z[14],2);
    z[27]=25*z[28] + 49*z[27];
    z[29]=n<T>(1,3)*z[7];
    z[30]=z[27]*z[29];
    z[30]=z[26] + z[30];
    z[31]=n<T>(1,3)*z[9];
    z[32]= - z[31] + z[29];
    z[33]=26*z[15] + 25*z[14];
    z[32]=z[33]*z[32];
    z[33]=2*z[11];
    z[34]=z[19]*z[33];
    z[32]=z[34] - 2*z[23] + z[32];
    z[34]=2*z[12];
    z[32]=z[32]*z[34];
    z[35]=z[18]*z[15];
    z[36]=z[7]*z[15];
    z[37]= - z[35] + 2*z[36];
    z[38]=z[13]*z[6];
    z[37]=6*z[37] + z[38];
    z[37]=z[13]*z[37];
    z[39]=z[10]*z[13];
    z[40]=6*z[18];
    z[41]= - 12*z[7] + z[40] + z[6];
    z[41]=z[41]*z[39];
    z[42]=3*z[13];
    z[43]=z[42]*z[10];
    z[22]=8*z[22];
    z[44]= - z[22] + z[43];
    z[44]=z[44]*z[33];
    z[45]=z[42]*z[15];
    z[46]= - n<T>(1,3)*z[27] - z[45];
    z[46]=2*z[46] - z[39];
    z[46]=z[9]*z[46];
    z[30]=z[32] + z[46] + z[44] + z[41] + 2*z[30] + z[37];
    z[30]=z[12]*z[30];
    z[32]=n<T>(4,3)*z[7];
    z[37]=npow(z[2],4);
    z[41]=npow(z[3],4);
    z[44]=z[41]*z[11];
    z[46]= - z[37]*z[44]*z[32];
    z[47]=z[41]*z[7];
    z[48]=z[41]*z[6];
    z[49]=5*z[48] - z[47];
    z[50]=n<T>(2,3)*z[37];
    z[49]=z[49]*z[50];
    z[51]=n<T>(7,3)*z[7];
    z[52]=2*z[16];
    z[49]=z[49] - z[52] - z[51];
    z[53]=npow(z[2],3);
    z[54]=z[53]*z[41];
    z[55]=29*z[6];
    z[56]=z[55] + 10*z[7];
    z[56]=n<T>(1,3)*z[56] - z[54];
    z[56]=z[2]*z[56];
    z[56]= - n<T>(29,3) + z[56];
    z[56]=z[5]*z[56];
    z[49]=2*z[49] + z[56];
    z[49]=z[5]*z[49];
    z[56]= - 7*z[48] + z[44];
    z[57]=n<T>(1,3)*z[37];
    z[56]=z[56]*z[57];
    z[32]=z[32] + z[16];
    z[54]= - n<T>(1,3)*z[54] - z[32];
    z[58]=2*z[2];
    z[54]=z[54]*z[58];
    z[59]=2*z[7];
    z[60]= - n<T>(1,3)*z[6] + z[59];
    z[60]=z[2]*z[60];
    z[60]=n<T>(5,3) + z[60];
    z[60]=z[5]*z[2]*z[60];
    z[54]=z[60] - n<T>(5,3) + z[54];
    z[54]=z[5]*z[54];
    z[32]=z[54] + z[56] + z[32];
    z[54]=4*z[4];
    z[32]=z[32]*z[54];
    z[56]=2*z[10];
    z[60]=z[56]*z[7];
    z[61]=z[9]*z[10];
    z[32]=z[32] + z[49] + z[46] + z[60] + z[61];
    z[32]=z[4]*z[32];
    z[46]=z[47] - z[48];
    z[49]=z[46]*z[50];
    z[61]=2*z[6];
    z[49]=z[49] - z[61] + z[51];
    z[62]=2*z[5];
    z[49]=z[49]*z[62];
    z[63]=z[10]*z[7];
    z[64]= - z[11]*z[10];
    z[49]=z[49] + z[63] + z[64];
    z[49]=z[5]*z[49];
    z[64]=4*z[7];
    z[65]=npow(z[10],2);
    z[66]= - z[65]*z[64];
    z[67]=z[9]*z[11];
    z[68]=z[10]*z[67];
    z[32]=z[32] + z[49] + z[66] + z[68];
    z[32]=z[4]*z[32];
    z[49]=npow(z[10],3);
    z[66]=z[49]*z[41];
    z[68]=z[65]*z[44];
    z[68]= - n<T>(10,3)*z[66] + 9*z[68];
    z[68]=z[9]*z[68];
    z[69]=z[44]*z[49];
    z[68]=n<T>(10,3)*z[69] + z[68];
    z[70]=npow(z[13],2);
    z[47]= - z[70]*z[47];
    z[71]=z[70]*z[12];
    z[72]= - z[41]*z[71];
    z[47]=z[47] + z[72];
    z[47]=z[12]*z[47];
    z[72]=n<T>(8,3)*z[4];
    z[73]=npow(z[12],2);
    z[74]=z[73]*z[44]*z[72];
    z[47]=z[74] + 8*z[68] + z[47];
    z[47]=z[8]*z[47];
    z[68]= - z[17]*z[69];
    z[74]=z[9]*z[17];
    z[75]=z[74]*z[66];
    z[68]=z[68] + z[75];
    z[47]=n<T>(80,3)*z[68] + z[47];
    z[47]=z[8]*z[47];
    z[46]= - z[46]*z[49];
    z[66]= - z[9]*z[66];
    z[46]=z[46] + z[66] + z[69];
    z[66]=npow(z[17],2);
    z[46]=z[66]*z[46];
    z[68]= - z[48] + z[44];
    z[69]=npow(z[4],3);
    z[75]=npow(z[2],2);
    z[68]=z[68]*z[75]*z[69];
    z[46]=5*z[46] + z[68];
    z[46]=n<T>(8,3)*z[46] + z[47];
    z[46]=z[8]*z[46];
    z[44]=z[48] - n<T>(1,3)*z[44];
    z[44]=z[44]*z[53]*z[69];
    z[44]=8*z[44] + z[46];
    z[44]=z[8]*z[44];
    z[46]=z[2]*z[3];
    z[47]=npow(z[46],5);
    z[48]=5*z[6];
    z[68]= - z[47]*z[48];
    z[68]= - z[64] + z[68];
    z[76]=n<T>(1,3)*z[5];
    z[68]=z[68]*z[76];
    z[77]=npow(z[3],5);
    z[78]=z[77]*z[37];
    z[78]= - z[64] + z[78];
    z[78]=z[2]*z[78];
    z[78]= - static_cast<T>(1)+ z[78];
    z[76]=z[78]*z[76];
    z[47]=z[6]*z[47];
    z[47]=z[76] + n<T>(4,3)*z[47] + z[16] + z[7];
    z[47]=z[47]*z[54];
    z[47]=z[47] - z[63] + z[68];
    z[54]=npow(z[4],2);
    z[47]=z[54]*z[47];
    z[68]=z[9]*z[15];
    z[76]=z[68] - z[36];
    z[78]= - z[28]*z[76];
    z[47]=z[47] + n<T>(50,3)*z[78];
    z[47]=z[5]*z[47];
    z[78]=n<T>(50,3)*z[76];
    z[78]=z[28]*z[78];
    z[79]=z[12]*z[14]*z[76];
    z[80]=z[70]*z[6];
    z[81]= - z[10]*z[80];
    z[78]=n<T>(50,3)*z[79] + z[81] + z[78];
    z[78]=z[12]*z[78];
    z[79]=z[49]*z[67];
    z[81]=z[7]*z[70]*z[73];
    z[79]= - n<T>(80,3)*z[79] + z[81];
    z[77]=npow(z[8],5)*z[77]*z[79];
    z[46]= - z[6]*npow(z[46],6);
    z[46]=z[7] + z[46];
    z[46]=z[1]*z[46]*npow(z[5],2)*z[69];
    z[46]=n<T>(4,3)*z[46] + z[77] + z[78] + z[47];
    z[46]=z[1]*z[46];
    z[47]= - z[63] + z[36];
    z[47]=z[42]*z[47];
    z[69]=4*z[19];
    z[69]= - z[36]*z[69];
    z[47]=z[69] + z[47];
    z[69]=4*z[11];
    z[47]=z[47]*z[69];
    z[43]= - z[43] + z[45];
    z[43]=z[18]*z[43];
    z[77]=z[15]*z[23];
    z[43]= - 8*z[77] + z[43];
    z[77]=4*z[15];
    z[78]=z[19]*z[77];
    z[45]=z[78] - z[45];
    z[39]=4*z[45] + 5*z[39];
    z[39]=z[11]*z[39];
    z[39]=2*z[43] + z[39];
    z[39]=z[9]*z[39];
    z[41]= - z[57]*z[41]*z[76];
    z[43]= - z[11]*z[19];
    z[41]=z[41] + z[23] + z[43];
    z[41]=z[41]*z[62];
    z[43]=z[9] - z[7];
    z[27]=z[27]*z[43];
    z[27]=n<T>(1,3)*z[27] + z[41];
    z[27]=z[27]*z[62];
    z[41]=16*z[36];
    z[45]=z[23]*z[41];
    z[57]=z[65]*z[7];
    z[78]=z[80] - n<T>(40,3)*z[57];
    z[78]=z[10]*z[78];
    z[27]=z[46] + z[44] + z[32] + z[27] + z[30] + z[39] + z[47] + z[45]
    + z[78];
    z[27]=z[1]*z[27];
    z[30]=6*z[17];
    z[32]=z[59] + z[6];
    z[30]=z[30]*z[32]*z[28];
    z[32]=z[32]*z[14];
    z[39]=2*z[17];
    z[44]=z[32]*z[39];
    z[45]=n<T>(14,3)*z[7];
    z[44]=z[44] + z[48] - z[45];
    z[46]=z[17]*z[14];
    z[47]=static_cast<T>(11)- z[46];
    z[47]=z[47]*z[31];
    z[44]=2*z[44] + z[47];
    z[44]=z[12]*z[44];
    z[47]=3*z[18];
    z[78]=z[13]*z[20];
    z[79]=static_cast<T>(1)+ z[78];
    z[79]=z[13]*z[79];
    z[79]= - z[47] + z[79];
    z[79]=z[79]*z[56];
    z[81]=13*z[19];
    z[82]= - z[42] + z[81] - 7*z[14];
    z[83]=z[28]*z[17];
    z[84]=7*z[83];
    z[82]=6*z[10] + 2*z[82] + z[84];
    z[82]=z[11]*z[82];
    z[85]= - z[15]*z[40];
    z[86]=15*z[14];
    z[87]= - 26*z[19] - z[86] - 16*z[6];
    z[87]=z[6]*z[87];
    z[88]=9*z[14];
    z[89]= - z[88] - n<T>(13,3)*z[15];
    z[89]=z[7]*z[89];
    z[90]=25*z[7];
    z[91]=13*z[6] - z[90];
    z[91]=z[13]*z[91];
    z[83]=z[83] - 137*z[14] - 31*z[15];
    z[83]=5*z[13] - n<T>(1,3)*z[83];
    z[83]=z[9]*z[83];
    z[44]=z[44] + z[83] + z[82] + z[79] + z[30] + z[91] + z[89] + z[85]
    + z[87];
    z[44]=z[12]*z[44];
    z[79]=z[10]*z[17];
    z[82]=static_cast<T>(1)- 3*z[79];
    z[82]=z[82]*z[56];
    z[83]=2*z[13];
    z[82]=z[82] - z[84] - z[83] - z[81] + 14*z[14];
    z[82]=z[11]*z[82];
    z[84]=npow(z[3],3);
    z[85]=z[84]*z[6];
    z[87]= - z[66]*z[85];
    z[89]=z[66]*z[84];
    z[91]=z[9]*z[89];
    z[87]=z[87] + z[91];
    z[91]=z[17]*z[85];
    z[92]=z[84]*z[17];
    z[93]=z[9]*z[92];
    z[91]= - n<T>(7,3)*z[91] + z[93];
    z[93]=n<T>(13,3)*z[85];
    z[94]= - z[9]*z[84];
    z[94]= - z[93] + z[94];
    z[94]=z[2]*z[94];
    z[91]=2*z[91] + z[94];
    z[91]=z[2]*z[91];
    z[87]=n<T>(4,3)*z[87] + z[91];
    z[87]=z[2]*z[87];
    z[32]=z[17]*z[32];
    z[32]= - n<T>(13,3)*z[7] + z[32];
    z[91]= - static_cast<T>(2)+ 7*z[46];
    z[91]=z[11]*z[91];
    z[32]=z[87] + 2*z[32] + z[91];
    z[32]=z[5]*z[32];
    z[87]=2*z[18];
    z[91]=npow(z[18],2);
    z[94]=z[91]*z[17];
    z[95]=z[87] + 3*z[94];
    z[96]=z[47]*z[79];
    z[95]=2*z[95] + z[96];
    z[95]=z[95]*z[56];
    z[81]=z[81] + z[86];
    z[81]=z[6]*z[81];
    z[86]=16*z[15];
    z[88]=z[88] + z[86];
    z[88]=z[7]*z[88];
    z[96]=n<T>(1,3)*z[28] - 12*z[70];
    z[96]=z[17]*z[96];
    z[86]=z[96] + 10*z[13] - n<T>(137,3)*z[14] - z[86];
    z[86]=z[9]*z[86];
    z[96]=2*z[53];
    z[96]=z[76]*z[96];
    z[97]=z[84]*z[96];
    z[98]=36*z[91];
    z[30]=z[32] + z[97] + z[86] + z[82] + z[95] - z[30] + z[88] - z[98]
    + z[81];
    z[30]=z[5]*z[30];
    z[32]=z[52] - z[29];
    z[32]=z[32]*z[69];
    z[81]=z[84]*z[11];
    z[82]= - 35*z[85] + 13*z[81];
    z[82]=z[82]*z[75];
    z[32]=z[32] + n<T>(1,3)*z[82];
    z[32]=z[2]*z[32];
    z[82]=6*z[91];
    z[86]=z[2]*z[84];
    z[86]= - z[82] + z[86];
    z[86]=z[2]*z[86];
    z[88]=44*z[6] + 13*z[7];
    z[86]=n<T>(1,3)*z[88] + z[86];
    z[86]=z[86]*z[58];
    z[88]=22*z[6];
    z[95]= - z[88] - z[7];
    z[95]=z[95]*z[58];
    z[95]=static_cast<T>(47)+ z[95];
    z[95]=z[2]*z[95];
    z[97]=5*z[20];
    z[95]= - z[97] + n<T>(1,3)*z[95];
    z[95]=z[95]*z[62];
    z[99]= - z[20]*z[56];
    z[86]=z[95] + z[86] + static_cast<T>(9)+ z[99];
    z[86]=z[5]*z[86];
    z[95]=5*z[18];
    z[99]=z[64] - n<T>(38,3)*z[6] + z[52] + z[95];
    z[99]=z[2]*z[99];
    z[99]=n<T>(43,3) + z[99];
    z[99]=z[2]*z[99];
    z[100]=z[7] - z[6];
    z[101]= - z[100]*z[58];
    z[101]= - static_cast<T>(5)+ z[101];
    z[102]=z[75]*z[5];
    z[101]=z[101]*z[102];
    z[103]=3*z[20];
    z[99]=n<T>(4,3)*z[101] - z[103] + z[99];
    z[99]=z[5]*z[99];
    z[101]=n<T>(2,3)*z[2];
    z[104]=z[84]*z[101];
    z[105]=z[29] - z[16];
    z[106]= - z[11]*z[105];
    z[104]=z[106] + z[104];
    z[104]=z[2]*z[104];
    z[104]= - z[59] + z[104];
    z[104]=z[104]*z[58];
    z[99]=z[99] + n<T>(5,3) + z[104];
    z[104]=2*z[4];
    z[99]=z[99]*z[104];
    z[106]=n<T>(77,3)*z[7];
    z[107]=z[61] - z[18];
    z[32]=z[99] + z[86] + z[32] + z[31] + z[56] - 2*z[107] - z[106];
    z[32]=z[4]*z[32];
    z[86]=z[16] - z[40];
    z[86]=z[56] + 19*z[7] + 2*z[86] - n<T>(17,3)*z[6];
    z[99]=n<T>(7,3)*z[9];
    z[108]=z[99] + z[59];
    z[109]= - z[84]*z[108];
    z[93]=z[93] + z[109];
    z[93]=z[93]*z[75];
    z[93]=z[98] + z[93];
    z[93]=z[2]*z[93];
    z[98]=z[84]*z[75];
    z[98]=z[98] - n<T>(34,3)*z[6] - 3*z[7];
    z[98]=z[2]*z[98];
    z[98]=n<T>(19,3) + z[98];
    z[98]=z[98]*z[62];
    z[86]=z[98] + z[93] + 2*z[86] + z[99];
    z[86]=z[5]*z[86];
    z[93]= - z[53]*z[81]*z[108];
    z[98]=npow(z[6],2);
    z[98]=4*z[98];
    z[108]=3*z[91];
    z[109]=z[98] - z[108];
    z[110]=14*z[6];
    z[90]= - z[110] + z[90];
    z[90]=z[13]*z[90];
    z[111]=6*z[21];
    z[112]= - n<T>(14,3)*z[13] + z[111] - z[107];
    z[112]= - n<T>(11,3)*z[11] + 4*z[112] - z[10];
    z[112]=z[9]*z[112];
    z[113]=12*z[11];
    z[114]= - n<T>(16,3)*z[9] - z[10] + z[113];
    z[114]=z[114]*z[34];
    z[111]= - z[111] + z[7];
    z[111]=4*z[111] + 19*z[13];
    z[111]=z[11]*z[111];
    z[32]=z[32] + z[86] + z[93] + z[114] + z[112] + z[111] + 5*z[63] - 4
   *z[109] + z[90];
    z[32]=z[4]*z[32];
    z[86]=4*z[21];
    z[90]=z[86] + z[107];
    z[90]= - 3*z[10] + 2*z[90] - n<T>(23,3)*z[13];
    z[90]=z[9]*z[90];
    z[93]=9*z[11];
    z[107]=z[93] - z[99];
    z[107]=z[107]*z[34];
    z[111]=5*z[85];
    z[112]=z[111] - 3*z[81];
    z[112]=z[112]*z[75];
    z[114]=z[13] - z[21];
    z[114]=z[114]*z[11];
    z[115]=10*z[6] - n<T>(17,3)*z[7];
    z[115]=z[13]*z[115];
    z[116]=z[7]*z[72];
    z[90]=z[116] + 2*z[112] + z[107] + z[90] + 24*z[114] + z[115] + 7*
    z[63];
    z[90]=z[4]*z[90];
    z[107]=3*z[70];
    z[109]=z[65] + z[107] + z[109];
    z[109]=z[9]*z[109];
    z[112]=3*z[6];
    z[115]= - z[11] - z[112] + 22*z[7];
    z[115]=z[70]*z[115];
    z[116]= - z[33] - n<T>(11,3)*z[9];
    z[116]=z[116]*z[73];
    z[57]=z[90] + 2*z[116] + 4*z[109] - 8*z[57] + z[115];
    z[57]=z[4]*z[57];
    z[90]=z[9] - z[100] - z[11];
    z[90]=z[70]*z[90];
    z[85]= - z[85] + z[81];
    z[85]=z[2]*z[85];
    z[100]= - z[38] + z[114];
    z[109]=3*z[12];
    z[114]=z[109]*z[11];
    z[100]=4*z[100] + z[114];
    z[100]=z[4]*z[100];
    z[115]=z[73]*z[11];
    z[85]=z[100] + n<T>(5,3)*z[85] - z[115] + z[90];
    z[85]=z[4]*z[85];
    z[90]=npow(z[13],3);
    z[100]=z[90]*z[7];
    z[116]= - z[9]*z[90];
    z[116]=z[100] + z[116];
    z[85]=4*z[116] + z[85];
    z[85]=z[85]*z[104];
    z[116]=z[84]*z[7];
    z[117]=z[81] - z[116];
    z[118]=z[84]*z[83];
    z[118]=z[118] - z[117];
    z[118]=z[13]*z[118];
    z[119]=z[70]*z[59];
    z[119]=z[84] + z[119];
    z[119]=z[119]*z[83];
    z[117]=z[119] + z[117];
    z[117]=z[12]*z[117];
    z[119]=z[12]*z[84];
    z[119]= - z[81] + z[119];
    z[72]=z[119]*z[72];
    z[72]=z[72] + z[117] + z[118];
    z[72]=z[12]*z[72];
    z[117]=72*z[65];
    z[118]=z[84]*z[117];
    z[119]=z[13] - n<T>(232,3)*z[10];
    z[119]=z[119]*z[81];
    z[118]=z[118] + z[119];
    z[118]=z[9]*z[118];
    z[117]= - z[81]*z[117];
    z[72]=z[117] + z[118] + z[72];
    z[72]=z[8]*z[72];
    z[117]=z[10]*z[40];
    z[117]=z[117] + z[108] - n<T>(50,3)*z[92];
    z[118]=2*z[65];
    z[117]=z[117]*z[118];
    z[119]=z[70]*z[17];
    z[84]=z[84]*z[119];
    z[49]=z[49]*z[11];
    z[84]= - n<T>(56,3)*z[49] + z[84] + z[117];
    z[117]=2*z[9];
    z[84]=z[84]*z[117];
    z[120]=7*z[7];
    z[121]=z[6] + z[120];
    z[121]=z[121]*z[70];
    z[92]= - z[29]*z[92];
    z[122]= - z[17]*z[81];
    z[92]=z[122] + z[121] + z[92];
    z[92]=z[12]*z[92];
    z[121]=z[64]*z[90];
    z[92]= - z[121] + z[92];
    z[92]=z[92]*z[34];
    z[111]=z[111] - z[116];
    z[111]=n<T>(136,3)*z[81] + n<T>(16,3)*z[111];
    z[111]=z[65]*z[17]*z[111];
    z[116]= - z[119]*z[116];
    z[72]=z[72] + z[85] + z[92] + z[84] + z[116] + z[111];
    z[72]=z[8]*z[72];
    z[84]=z[89]*z[70];
    z[85]=4*z[6];
    z[24]= - z[24]*z[85];
    z[92]=5*z[90];
    z[24]=z[24] - z[92];
    z[111]=z[95] + 12*z[89];
    z[111]=z[10]*z[111];
    z[111]=z[82] + z[111];
    z[111]=z[10]*z[111];
    z[116]=z[22] - n<T>(11,3)*z[65];
    z[116]=z[11]*z[116];
    z[24]=z[116] + z[111] + 2*z[24] + z[84];
    z[24]=z[24]*z[117];
    z[23]= - z[23] + n<T>(8,3)*z[36];
    z[111]=9*z[7];
    z[116]=z[85] + z[111];
    z[116]=z[13]*z[116];
    z[122]=n<T>(2,3)*z[7];
    z[123]=z[89]*z[122];
    z[124]=z[19] - n<T>(1,3)*z[89];
    z[124]=z[124]*z[33];
    z[23]= - n<T>(16,3)*z[68] + z[124] + z[123] + 2*z[23] + z[116];
    z[23]=z[12]*z[23];
    z[68]=11*z[7];
    z[116]=z[6] - z[68];
    z[116]=z[116]*z[70];
    z[22]= - z[11]*z[22];
    z[22]=z[23] + z[22] + z[26] + z[116];
    z[22]=z[22]*z[34];
    z[23]= - z[112] + z[29];
    z[23]=z[23]*z[89];
    z[26]=n<T>(10,3)*z[7];
    z[116]=z[26] + z[47];
    z[123]= - z[116]*z[56];
    z[23]=z[123] - z[108] + 2*z[23];
    z[123]=4*z[10];
    z[23]=z[23]*z[123];
    z[89]=n<T>(28,3)*z[10] + z[13] - n<T>(2,3)*z[89];
    z[89]=z[89]*z[123];
    z[89]=z[70] + z[89];
    z[89]=z[11]*z[89];
    z[23]=z[89] - z[80] + z[23];
    z[23]=z[10]*z[23];
    z[22]=z[72] + z[57] + z[22] + z[24] + z[23];
    z[22]=z[8]*z[22];
    z[23]=3*z[19];
    z[24]=z[23] + 2*z[15];
    z[24]=z[24]*z[85];
    z[57]=z[47]*z[15];
    z[72]= - 12*z[21] + z[13];
    z[72]=z[13]*z[72];
    z[24]=z[72] + z[57] + z[24];
    z[72]=2*z[21];
    z[80]=z[72] - z[42];
    z[89]=z[80]*z[70];
    z[84]=4*z[89] + z[84];
    z[84]=z[17]*z[84];
    z[124]=z[56]*z[18];
    z[125]=z[56] - 24*z[19] + 7*z[13];
    z[125]=z[11]*z[125];
    z[24]=z[125] + z[124] + 2*z[24] + z[84];
    z[24]=z[9]*z[24];
    z[84]=z[116]*z[123];
    z[116]=z[17]*z[84];
    z[125]=11*z[18];
    z[126]=z[82]*z[17];
    z[116]=z[116] + z[126] - z[125] + n<T>(62,3)*z[7];
    z[116]=z[116]*z[56];
    z[127]=24*z[91];
    z[116]=z[116] - z[127] - z[38];
    z[116]=z[10]*z[116];
    z[128]=n<T>(2,3)*z[10];
    z[129]=static_cast<T>(11)- 56*z[79];
    z[129]=z[129]*z[128];
    z[129]=z[13] + z[129];
    z[129]=z[10]*z[129];
    z[130]=8*z[21];
    z[131]=z[130] - z[13];
    z[132]=z[13]*z[131];
    z[129]=z[132] + z[129];
    z[129]=z[11]*z[129];
    z[81]=z[81]*z[96];
    z[41]= - z[6]*z[41];
    z[22]=z[27] + z[22] + z[32] + z[30] + z[81] + z[44] + z[24] + z[129]
    + z[41] + z[116];
    z[22]=z[1]*z[22];
    z[24]=z[45] + z[112];
    z[27]= - z[13]*z[24];
    z[30]= - z[10] + n<T>(7,3)*z[13];
    z[32]=z[30]*z[117];
    z[41]=19*z[11];
    z[44]=z[41] - z[34];
    z[44]=z[12]*z[44];
    z[81]=3*z[11];
    z[96]= - z[72] + z[6];
    z[96]=4*z[96] - z[81] + 6*z[12];
    z[96]=z[4]*z[96];
    z[116]= - 20*z[21] + z[42];
    z[116]=z[11]*z[116];
    z[27]=z[96] + z[44] + z[32] + z[116] + z[27] + z[60];
    z[27]=z[4]*z[27];
    z[32]= - z[65]*z[59];
    z[44]= - z[107] + z[118];
    z[44]=z[9]*z[44];
    z[60]=npow(z[3],2);
    z[96]=n<T>(2,3)*z[60];
    z[116]= - z[96] - z[114];
    z[116]=z[116]*z[34];
    z[129]=z[60]*z[6];
    z[132]=n<T>(1,3)*z[129];
    z[133]=n<T>(1,3)*z[60];
    z[134]=z[133]*z[11];
    z[135]=z[7]*z[107];
    z[27]=z[27] + z[116] + z[44] + z[134] + z[32] - z[132] + z[135];
    z[27]=z[27]*z[104];
    z[32]=z[73]*z[8];
    z[44]=z[90]*z[59]*z[32];
    z[73]=z[69]*z[21];
    z[73]=z[73] - z[114];
    z[73]=z[73]*z[4];
    z[73]=z[73] + z[115];
    z[54]=z[73]*z[54];
    z[73]=z[124] + z[91];
    z[114]=z[73]*z[65];
    z[116]=9*z[114] - n<T>(94,3)*z[49];
    z[116]=z[9]*z[116];
    z[124]=6*z[7];
    z[135]=z[124] - z[13];
    z[135]=z[135]*z[71];
    z[135]= - 6*z[100] + z[135];
    z[135]=z[12]*z[135];
    z[44]=z[44] - z[54] + z[116] + z[135];
    z[44]=z[8]*z[44];
    z[40]= - z[40] - 5*z[10];
    z[40]=z[40]*z[56];
    z[40]=z[40] - z[108] - n<T>(58,3)*z[60];
    z[40]=z[40]*z[123];
    z[116]= - z[6] + 16*z[13];
    z[116]=z[13]*z[116];
    z[116]=z[60] + z[116];
    z[116]=z[13]*z[116];
    z[135]=101*z[60] + 224*z[65];
    z[136]=n<T>(1,3)*z[11];
    z[135]=z[135]*z[136];
    z[40]=z[135] + z[116] + z[40];
    z[40]=z[9]*z[40];
    z[116]=9*z[18];
    z[135]=n<T>(5,3)*z[7];
    z[137]= - z[116] - z[135];
    z[137]=z[137]*z[56];
    z[138]=9*z[91];
    z[38]=z[137] - z[138] - z[38];
    z[38]=z[10]*z[38];
    z[38]=n<T>(10,3)*z[129] + z[38];
    z[38]=z[38]*z[123];
    z[137]=z[52]*z[60];
    z[139]=z[13] + n<T>(94,3)*z[10];
    z[139]=z[10]*z[139];
    z[139]=16*z[60] + z[139];
    z[139]=z[10]*z[139];
    z[139]=z[137] + z[139];
    z[139]=z[139]*z[69];
    z[140]=2*z[60];
    z[141]=8*z[13];
    z[142]=z[141] + z[6] - 68*z[7];
    z[142]=z[13]*z[142];
    z[142]= - z[140] + z[142];
    z[142]=z[13]*z[142];
    z[143]=4*z[13];
    z[144]=z[59] - z[13];
    z[144]=z[144]*z[143];
    z[144]= - z[133] + z[144];
    z[144]=z[12]*z[144];
    z[145]=z[60]*z[11];
    z[142]=4*z[144] + z[142] + n<T>(8,3)*z[145];
    z[142]=z[12]*z[142];
    z[144]= - z[60]*z[70];
    z[27]=4*z[44] + z[27] + z[142] + z[40] + z[139] + z[144] + z[38];
    z[27]=z[8]*z[27];
    z[38]= - z[19]*z[112];
    z[35]=z[35] + z[38];
    z[35]=3*z[35] - n<T>(40,3)*z[36];
    z[38]=z[60]*z[17];
    z[40]= - z[38]*z[26];
    z[23]=z[23] + z[83];
    z[23]=3*z[23] + n<T>(11,3)*z[38];
    z[23]=z[23]*z[33];
    z[44]=8*z[12];
    z[139]=z[122] - z[13];
    z[44]=z[139]*z[44];
    z[139]=35*z[13] - z[112] - n<T>(250,3)*z[7];
    z[139]=z[13]*z[139];
    z[142]=11*z[13];
    z[144]=n<T>(62,3)*z[15] - z[142];
    z[144]=z[9]*z[144];
    z[23]=z[44] + z[144] + z[23] + z[40] + 2*z[35] + z[139];
    z[23]=z[12]*z[23];
    z[35]=14*z[13];
    z[40]=z[56] + z[35] - z[110] - z[86] + z[125];
    z[40]=z[9]*z[40];
    z[44]=n<T>(20,3)*z[9];
    z[139]=z[4] + n<T>(29,3)*z[12] + z[44] - z[93] - z[86] - z[48] - n<T>(40,3)*
    z[7];
    z[139]=z[4]*z[139];
    z[26]=z[13]*z[26];
    z[131]= - z[11]*z[131];
    z[26]=z[26] + z[131];
    z[131]=z[12] - z[9];
    z[144]=26*z[11];
    z[131]=z[144] - n<T>(17,3)*z[131];
    z[131]=z[12]*z[131];
    z[134]=z[129] - z[134];
    z[134]=z[134]*z[58];
    z[26]=z[139] + z[134] + z[131] + 2*z[26] + z[40];
    z[26]=z[26]*z[104];
    z[40]=z[116] + n<T>(20,3)*z[7];
    z[40]=z[17]*z[40]*z[56];
    z[131]=n<T>(38,3)*z[7];
    z[40]=z[40] + 9*z[94] + z[47] + z[131];
    z[40]=z[10]*z[40];
    z[134]= - z[60]*z[29];
    z[134]= - 11*z[129] + z[134];
    z[134]=z[17]*z[134];
    z[139]=n<T>(1,3)*z[10];
    z[146]= - static_cast<T>(59)- 94*z[79];
    z[146]=z[146]*z[139];
    z[146]=z[146] - z[13] - n<T>(13,3)*z[38];
    z[146]=z[11]*z[146];
    z[40]=z[146] + z[40] - z[82] + z[134];
    z[40]=z[123]*z[40];
    z[134]=3*z[60];
    z[146]=z[134] + 40*z[70];
    z[146]=z[17]*z[146];
    z[146]=z[146] - z[61] - z[142];
    z[146]=z[13]*z[146];
    z[147]=z[19] - z[15];
    z[147]=z[147]*z[25];
    z[57]= - z[57] + z[147];
    z[147]=7*z[18];
    z[148]=n<T>(16,3)*z[10] - z[147] + n<T>(47,3)*z[38];
    z[148]=z[148]*z[123];
    z[149]=n<T>(116,3)*z[10] - 16*z[19] - z[142];
    z[149]=z[11]*z[149];
    z[57]=z[149] + z[148] + 2*z[57] + z[146];
    z[57]=z[9]*z[57];
    z[146]=z[36]*z[85];
    z[146]=z[146] - z[90];
    z[148]=z[60]*z[67];
    z[149]=z[16]*z[145];
    z[148]= - 12*z[149] + n<T>(17,3)*z[148];
    z[148]=z[2]*z[148];
    z[149]=z[13]*z[7]*z[38];
    z[23]=z[27] + z[26] + z[148] + z[23] + z[57] + 4*z[146] + z[149] + 
    z[40];
    z[23]=z[8]*z[23];
    z[26]=n<T>(7,3)*z[14];
    z[27]=33*z[18];
    z[40]=9*z[6];
    z[57]= - z[40] + z[26] + z[27];
    z[146]=z[28]*z[85];
    z[140]= - z[28] - z[140];
    z[140]=z[7]*z[140];
    z[140]=z[146] + z[140];
    z[140]=z[17]*z[140];
    z[24]=z[14]*z[24];
    z[24]=z[24] + z[28];
    z[138]=z[138] - z[24];
    z[138]=2*z[138] + z[140];
    z[138]=z[138]*z[39];
    z[140]=z[7] - z[18];
    z[140]=z[140]*z[66]*z[12];
    z[146]=12*z[18];
    z[148]=z[68] + z[14] - z[146];
    z[149]= - z[7]*z[46];
    z[148]=z[11] + 2*z[148] + z[149];
    z[148]=z[39]*z[148];
    z[148]= - 12*z[140] + n<T>(61,3) + z[148];
    z[148]=z[12]*z[148];
    z[149]=static_cast<T>(4)- z[78];
    z[149]=z[149]*z[83];
    z[150]=z[78] - 2;
    z[146]=z[17]*z[146];
    z[146]=z[146] - z[150];
    z[146]=z[146]*z[56];
    z[151]= - z[14] + 4*z[38];
    z[151]=z[17]*z[151];
    z[151]= - 18*z[79] + static_cast<T>(8)+ z[151];
    z[151]=z[11]*z[151];
    z[152]= - static_cast<T>(2)+ n<T>(1,3)*z[46];
    z[152]=z[9]*z[152];
    z[57]=z[148] + 5*z[152] + z[151] + z[146] + z[138] + z[149] + 2*
    z[57] - n<T>(245,3)*z[7];
    z[57]=z[12]*z[57];
    z[24]= - 6*z[70] + z[82] + z[24];
    z[138]= - z[28] - z[133];
    z[138]=z[138]*z[85];
    z[28]=z[7]*z[28];
    z[28]=z[138] + z[28];
    z[28]=z[17]*z[28];
    z[24]=2*z[24] + z[28];
    z[24]=z[17]*z[24];
    z[28]=z[147] + z[126];
    z[138]= - z[17]*z[28];
    z[146]=z[66]*z[10];
    z[147]= - z[47]*z[146];
    z[138]=z[147] + z[138] + z[150];
    z[138]=z[10]*z[138];
    z[147]=z[110] - z[95];
    z[148]=n<T>(61,3)*z[7];
    z[24]=z[138] + z[24] + z[83] - z[148] - z[52] - z[26] - z[147];
    z[26]=z[108] + n<T>(2,3)*z[36];
    z[132]=z[137] + z[132];
    z[132]=z[17]*z[132];
    z[77]= - z[77] - 5*z[38];
    z[77]=z[77]*z[31];
    z[77]=z[77] + 2*z[26] + z[132];
    z[132]=4*z[16];
    z[137]= - z[9] + z[132];
    z[137]=z[60]*z[137];
    z[137]=n<T>(19,3)*z[129] + z[137];
    z[137]=z[2]*z[137];
    z[77]=2*z[77] + z[137];
    z[77]=z[2]*z[77];
    z[31]=z[48] + z[31];
    z[96]=z[96] - z[76];
    z[96]=z[96]*z[58];
    z[31]=7*z[31] + z[96];
    z[31]=z[2]*z[31];
    z[96]=z[85]*z[46];
    z[96]=z[96] - 4*z[14] - z[6];
    z[96]=z[96]*z[39];
    z[31]=z[31] - n<T>(35,3) + z[96];
    z[31]=z[5]*z[31];
    z[96]=3*z[146];
    z[137]=z[39] + z[96];
    z[137]=z[137]*z[56];
    z[46]=z[137] - static_cast<T>(1)+ z[46];
    z[46]=z[11]*z[46];
    z[133]=z[133] + z[107];
    z[133]=z[17]*z[133];
    z[133]=8*z[133] - n<T>(5,3)*z[14] + 12*z[13];
    z[133]=z[17]*z[133];
    z[133]=static_cast<T>(8)+ z[133];
    z[133]=z[9]*z[133];
    z[24]=z[31] + z[77] + z[133] + 2*z[24] + z[46];
    z[24]=z[5]*z[24];
    z[31]=12*z[91];
    z[46]=z[31] + z[60];
    z[77]=z[132] + z[135];
    z[77]=z[11]*z[77];
    z[77]= - 2*z[46] + z[77];
    z[77]=z[2]*z[77];
    z[44]=z[77] - z[44] - 18*z[11] + n<T>(52,3)*z[7] + 8*z[18] + z[6];
    z[44]=z[2]*z[44];
    z[77]= - z[112] + z[16] - z[95];
    z[91]=z[2]*z[127];
    z[77]=z[91] + 4*z[77] + z[29];
    z[77]=z[2]*z[77];
    z[77]= - static_cast<T>(7)+ z[77];
    z[77]=z[2]*z[77];
    z[91]=10*z[20];
    z[110]=z[110] - z[7];
    z[110]=z[2]*z[110];
    z[110]= - n<T>(52,3) + z[110];
    z[110]=z[2]*z[110];
    z[110]=z[91] + z[110];
    z[127]=z[58]*z[5];
    z[110]=z[110]*z[127];
    z[77]=z[110] + 20*z[20] + z[77];
    z[77]=z[5]*z[77];
    z[110]=z[2]*z[147];
    z[110]= - static_cast<T>(10)+ z[110];
    z[110]=z[2]*z[110];
    z[110]=z[103] + z[110];
    z[110]=z[2]*z[110];
    z[133]= - z[61] + z[29];
    z[133]=z[2]*z[133];
    z[133]=n<T>(10,3) + z[133];
    z[133]=z[5]*z[133]*z[53];
    z[110]=z[110] + z[133];
    z[110]=z[110]*z[62];
    z[133]= - n<T>(2,3)*z[11] + z[95] - n<T>(43,3)*z[6];
    z[133]=z[2]*z[133];
    z[133]=static_cast<T>(4)+ z[133];
    z[133]=z[133]*z[58];
    z[110]=z[110] - z[20] + z[133];
    z[110]=z[110]*z[104];
    z[44]=z[110] + z[77] - static_cast<T>(16)+ z[44];
    z[44]=z[4]*z[44];
    z[77]=z[108] + z[98];
    z[98]=6*z[16] + z[29];
    z[98]=z[11]*z[98];
    z[77]= - n<T>(2,3)*z[67] + 2*z[77] + z[98];
    z[98]= - z[129] + z[145];
    z[98]=z[2]*z[98];
    z[77]=2*z[77] + n<T>(19,3)*z[98];
    z[77]=z[2]*z[77];
    z[98]=25*z[18];
    z[52]= - z[52] + z[98];
    z[46]=z[2]*z[46];
    z[46]= - 4*z[46] + z[9] - 7*z[11] - n<T>(65,3)*z[7] + 2*z[52] - 51*z[6];
    z[46]=z[2]*z[46];
    z[52]=9*z[20];
    z[110]=31*z[6] + z[7];
    z[110]=z[2]*z[110];
    z[110]= - n<T>(70,3) + z[110];
    z[110]=z[2]*z[110];
    z[110]=z[52] + z[110];
    z[110]=z[110]*z[62];
    z[133]=z[10]*z[20];
    z[137]= - n<T>(74,3) + z[133];
    z[46]=z[110] + 2*z[137] + z[46];
    z[46]=z[5]*z[46];
    z[110]=z[132] - z[125];
    z[125]=n<T>(41,3) - z[133];
    z[125]=z[125]*z[34];
    z[44]=z[44] + z[46] + z[77] + z[125] + n<T>(13,3)*z[9] - z[144] - z[56]
    - n<T>(56,3)*z[13] + 47*z[7] + 2*z[110] + 21*z[6];
    z[44]=z[4]*z[44];
    z[46]=z[66]*z[60];
    z[77]= - n<T>(10,3) + z[46];
    z[110]=19*z[17] + 56*z[146];
    z[110]=z[110]*z[139];
    z[77]=2*z[77] + z[110];
    z[77]=z[10]*z[77];
    z[110]=8*z[16];
    z[125]=5*z[19];
    z[77]=z[77] + z[83] + z[59] - z[86] + z[110] - z[125];
    z[77]=z[77]*z[33];
    z[133]= - z[7]*z[60];
    z[129]=n<T>(11,3)*z[129] + z[133];
    z[129]=z[17]*z[129];
    z[129]=z[31] + z[129];
    z[129]=z[129]*z[39];
    z[133]=z[135] + z[47];
    z[133]=z[133]*z[123];
    z[137]= - z[66]*z[133];
    z[138]= - z[126] + 17*z[18] - 26*z[7];
    z[138]=z[17]*z[138];
    z[137]=z[138] + z[137];
    z[137]=z[10]*z[137];
    z[98]=z[137] + z[129] - z[13] - z[148] + z[98] - n<T>(8,3)*z[6];
    z[98]=z[98]*z[56];
    z[125]=z[125] + z[25];
    z[125]=z[6]*z[125];
    z[36]= - n<T>(19,3)*z[36] - z[31] + z[125];
    z[38]= - z[70]*z[38];
    z[38]=8*z[89] + z[38];
    z[38]=z[17]*z[38];
    z[125]=10*z[21] + z[42];
    z[125]=z[125]*z[143];
    z[129]= - z[80]*z[141];
    z[129]=z[134] + z[129];
    z[129]=z[17]*z[13]*z[129];
    z[125]=z[125] + z[129];
    z[125]=z[17]*z[125];
    z[46]=static_cast<T>(4)- 11*z[46];
    z[46]=z[10]*z[46];
    z[129]=16*z[21];
    z[134]= - z[129] + n<T>(19,3)*z[15];
    z[46]= - 16*z[11] + n<T>(4,3)*z[46] + z[125] + 2*z[134] + n<T>(209,3)*z[13];
    z[46]=z[9]*z[46];
    z[125]= - z[11]*z[76];
    z[134]=z[16]*z[69];
    z[134]=z[134] - n<T>(31,3)*z[67];
    z[60]=z[2]*z[60]*z[134];
    z[60]= - n<T>(38,3)*z[125] + z[60];
    z[60]=z[2]*z[60];
    z[134]= - z[130] + 17*z[13];
    z[134]=z[13]*z[134];
    z[22]=z[22] + z[23] + z[44] + z[24] + z[60] + z[57] + z[46] + z[77]
    + z[98] + z[38] + 2*z[36] + z[134];
    z[22]=z[1]*z[22];
    z[23]= - z[73]*z[118];
    z[23]=z[100] + z[23];
    z[24]= - z[18] - z[128];
    z[24]=z[10]*z[24];
    z[24]= - z[31] + 25*z[24];
    z[24]=z[10]*z[24];
    z[36]=z[11]*z[65];
    z[24]=z[24] + 46*z[36];
    z[24]=z[9]*z[24];
    z[23]=z[24] + 3*z[23] + n<T>(86,3)*z[49];
    z[24]= - z[120] + z[83];
    z[24]=z[24]*z[107];
    z[36]=z[83]*z[20];
    z[38]= - static_cast<T>(3)+ z[36];
    z[38]=z[13]*z[38];
    z[38]=z[68] + z[38];
    z[38]=z[12]*z[13]*z[38];
    z[24]=z[24] + z[38];
    z[24]=z[12]*z[24];
    z[38]=z[109] - z[86];
    z[44]=z[11] + z[38];
    z[44]=z[4]*z[44];
    z[46]= - z[11]*z[130];
    z[57]=10*z[11] - z[12];
    z[57]=z[12]*z[57];
    z[44]=z[44] + z[46] + z[57];
    z[44]=z[4]*z[44];
    z[44]= - n<T>(8,3)*z[115] + z[44];
    z[44]=z[4]*z[44];
    z[23]=z[44] + 2*z[23] + z[24];
    z[24]=3*z[114] - n<T>(43,3)*z[49];
    z[24]=z[9]*z[24];
    z[44]=z[68] - z[143];
    z[44]=z[44]*z[71];
    z[44]= - 16*z[100] + z[44];
    z[44]=z[12]*z[44];
    z[32]=z[121]*z[32];
    z[24]=z[32] - 2*z[54] + 8*z[24] + z[44];
    z[24]=z[8]*z[24];
    z[23]=2*z[23] + z[24];
    z[23]=z[8]*z[23];
    z[24]=4*z[17];
    z[32]= - z[90]*z[24];
    z[44]=n<T>(37,3) + 5*z[79];
    z[44]=z[44]*z[56];
    z[44]=13*z[18] + z[44];
    z[44]=z[44]*z[56];
    z[46]=z[13] - n<T>(104,3)*z[10];
    z[46]=z[11]*z[46];
    z[32]=z[46] + z[44] + z[32] + z[31] + z[70];
    z[32]=z[32]*z[117];
    z[44]=z[13]*z[51];
    z[46]= - z[11]*z[72];
    z[30]= - z[9]*z[30];
    z[30]=z[30] + z[46] + z[44] - z[63];
    z[38]=z[20]*z[38];
    z[44]=z[85] - z[81];
    z[44]=z[2]*z[44];
    z[38]=z[44] + static_cast<T>(1)+ z[38];
    z[38]=z[4]*z[38];
    z[44]= - z[72] + z[112];
    z[46]=z[12]*z[20];
    z[49]=static_cast<T>(7)- z[46];
    z[49]=z[12]*z[49];
    z[38]=z[38] + z[49] + 2*z[44] - z[81];
    z[38]=z[4]*z[38];
    z[41]=z[41] - z[109];
    z[41]=z[12]*z[41];
    z[30]=z[38] + 2*z[30] + z[41];
    z[30]=z[30]*z[104];
    z[38]=z[17]*z[133];
    z[38]=z[38] + z[126] + z[51] + 19*z[18] + z[6];
    z[38]=z[10]*z[38];
    z[38]=z[82] + z[38];
    z[38]=z[38]*z[123];
    z[41]= - static_cast<T>(47)- n<T>(86,3)*z[79];
    z[41]=z[41]*z[65]*z[69];
    z[44]=static_cast<T>(6)- 5*z[78];
    z[44]=z[44]*z[83];
    z[44]= - z[11] - z[106] + z[44];
    z[44]=z[13]*z[44];
    z[49]= - static_cast<T>(7)+ z[78];
    z[49]=z[49]*z[83];
    z[51]=17*z[11];
    z[49]= - z[51] + 15*z[7] + z[49];
    z[49]=z[12]*z[49];
    z[44]=2*z[44] + z[49];
    z[44]=z[12]*z[44];
    z[49]=21*z[7] - z[141];
    z[49]=z[49]*z[70];
    z[23]=z[23] + z[30] + z[44] + z[32] + z[41] + z[49] + z[38];
    z[23]=z[8]*z[23];
    z[30]=6*z[6];
    z[32]= - n<T>(68,3)*z[7] + z[30] - z[28];
    z[32]=z[17]*z[32];
    z[38]= - z[66]*z[84];
    z[32]=z[32] + z[38];
    z[32]=z[10]*z[32];
    z[28]=z[32] - z[111] + z[61] + z[28];
    z[28]=z[10]*z[28];
    z[32]=z[90]*z[39];
    z[38]=47*z[17] + n<T>(76,3)*z[146];
    z[38]=z[10]*z[38];
    z[38]=n<T>(77,3) + z[38];
    z[38]=z[10]*z[38];
    z[38]=z[110] + z[38];
    z[38]=z[11]*z[38];
    z[41]=z[6]*z[59];
    z[44]= - static_cast<T>(7)+ 8*z[78];
    z[44]=z[13]*z[44];
    z[44]=17*z[7] + z[44];
    z[44]=z[13]*z[44];
    z[28]=z[38] + z[28] + z[32] + z[41] + z[44];
    z[32]= - z[113] - z[116] + n<T>(50,3)*z[7];
    z[32]=z[17]*z[32];
    z[36]= - static_cast<T>(1)+ z[36];
    z[32]= - 6*z[140] + 3*z[36] + z[32];
    z[32]=z[12]*z[32];
    z[36]=z[95] - z[124];
    z[38]=n<T>(1,3) - z[78];
    z[35]=z[38]*z[35];
    z[38]=18*z[94];
    z[32]=z[32] + z[51] + z[38] + 3*z[36] + z[35];
    z[32]=z[32]*z[34];
    z[35]=z[82]*z[2];
    z[36]= - z[35] - z[69] + z[95] - 27*z[6];
    z[36]=z[2]*z[36];
    z[33]=z[33] + z[95] - z[88];
    z[33]=z[2]*z[33];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[2]*z[33];
    z[33]=z[20] + z[33];
    z[33]=z[4]*z[33];
    z[41]=z[72]*z[20];
    z[44]=static_cast<T>(1)+ z[41];
    z[49]=z[20]*z[109];
    z[33]=z[33] + z[36] + 2*z[44] + z[49];
    z[33]=z[4]*z[33];
    z[36]=z[135] + z[21] - z[112];
    z[44]=n<T>(40,3)*z[9];
    z[33]=z[33] - z[35] + 10*z[12] - z[44] + 4*z[36] - z[81];
    z[33]=z[33]*z[104];
    z[36]= - z[17]*z[92];
    z[36]= - z[70] + z[36];
    z[36]=z[17]*z[36];
    z[36]= - n<T>(35,3)*z[13] + z[36];
    z[49]= - static_cast<T>(23)- n<T>(70,3)*z[79];
    z[49]=z[10]*z[49];
    z[36]=2*z[36] + z[49];
    z[36]=2*z[36] - n<T>(53,3)*z[11];
    z[36]=z[9]*z[36];
    z[23]=z[23] + z[33] + z[32] + 2*z[28] + z[36];
    z[23]=z[8]*z[23];
    z[27]= - z[38] + z[59] + z[14] - z[27];
    z[27]=z[17]*z[27];
    z[28]=z[116] + z[45];
    z[28]=z[17]*z[28];
    z[28]=static_cast<T>(7)+ z[28];
    z[28]=z[17]*z[28];
    z[32]=z[11]*z[66];
    z[28]= - n<T>(20,3)*z[32] + z[91] + z[28];
    z[28]=z[12]*z[28];
    z[32]= - z[47] + z[7];
    z[32]=z[32]*z[96];
    z[33]=2*z[20];
    z[36]=z[14]*z[33];
    z[36]= - static_cast<T>(5)+ z[36];
    z[38]=3*z[17] + z[146];
    z[38]=z[11]*z[38];
    z[27]=z[28] + 6*z[38] + z[32] + z[27] + 3*z[36] - n<T>(41,3)*z[78];
    z[27]=z[27]*z[34];
    z[28]=z[42]*z[20];
    z[32]=static_cast<T>(7)- z[28];
    z[32]=z[32]*z[83];
    z[25]=12*z[119] + z[32] + z[25] - z[14] + z[47];
    z[25]=z[17]*z[25];
    z[32]=z[108] - z[76];
    z[32]=z[32]*z[58];
    z[34]=17*z[6];
    z[32]=z[32] + z[44] - z[126] - 10*z[18] + z[34];
    z[32]=z[2]*z[32];
    z[36]=z[17]*z[6];
    z[38]=static_cast<T>(31)- 20*z[36];
    z[38]=2*z[38] - 7*z[74];
    z[42]=z[2]*z[76];
    z[42]= - n<T>(61,3)*z[6] + z[42];
    z[42]=z[2]*z[42];
    z[38]=n<T>(1,3)*z[38] + z[42];
    z[38]=z[2]*z[38];
    z[36]=static_cast<T>(2)- z[36];
    z[36]=z[17]*z[36];
    z[36]= - z[20] + z[36];
    z[36]=4*z[36] + z[38];
    z[36]=z[5]*z[36];
    z[38]= - z[142] - 6*z[119];
    z[38]=z[17]*z[38];
    z[38]= - n<T>(4,3) + z[38];
    z[38]=z[38]*z[74];
    z[42]= - z[14]*z[103];
    z[42]=static_cast<T>(11)+ z[42];
    z[25]=z[36] + z[32] + z[38] + z[25] + 2*z[42] - z[28];
    z[25]=z[25]*z[62];
    z[32]=z[11]*z[7];
    z[36]=z[31] - z[32];
    z[36]=z[2]*z[36];
    z[38]=15*z[18];
    z[36]=z[36] + z[136] - z[38] + n<T>(109,3)*z[6];
    z[36]=z[2]*z[36];
    z[36]= - static_cast<T>(13)+ z[36];
    z[36]=z[2]*z[36];
    z[30]=z[95] - z[30];
    z[30]= - z[35] + 2*z[30] - z[7];
    z[30]=z[2]*z[30];
    z[30]= - static_cast<T>(2)+ z[30];
    z[30]=z[2]*z[30];
    z[30]= - z[52] + z[30];
    z[30]=z[2]*z[30];
    z[35]= - z[48] + z[7];
    z[35]=z[35]*z[101];
    z[35]=static_cast<T>(7)+ z[35];
    z[35]=z[2]*z[35];
    z[35]= - z[97] + z[35];
    z[35]=z[35]*z[102];
    z[30]=z[30] + z[35];
    z[30]=z[5]*z[30];
    z[35]= - z[122] + z[95] - 18*z[6];
    z[35]=z[2]*z[35];
    z[35]=n<T>(31,3) + z[35];
    z[35]=z[2]*z[35];
    z[35]= - z[103] + z[35];
    z[35]=z[35]*z[75];
    z[42]=z[2]*z[85];
    z[42]= - static_cast<T>(5)+ z[42];
    z[42]=z[5]*z[42]*z[50];
    z[35]=z[35] + z[42];
    z[35]=z[5]*z[35];
    z[42]= - z[18] + n<T>(10,3)*z[6];
    z[29]= - z[2]*z[11]*z[29];
    z[29]=5*z[42] + z[29];
    z[29]=z[29]*z[58];
    z[29]= - static_cast<T>(11)+ z[29];
    z[29]=z[2]*z[29];
    z[29]=z[20] + z[29];
    z[29]=z[2]*z[29];
    z[29]=z[29] + z[35];
    z[29]=z[4]*z[29];
    z[29]=z[29] + z[30] - z[33] + z[36];
    z[29]=z[29]*z[104];
    z[30]=4*z[18];
    z[33]=z[16] - z[30];
    z[31]=z[2]*z[31];
    z[31]=z[31] - n<T>(5,3)*z[9] + z[64] + 4*z[33] + z[55];
    z[31]=z[2]*z[31];
    z[31]= - n<T>(28,3) + z[31];
    z[31]=z[2]*z[31];
    z[33]= - z[34] - z[7];
    z[33]=z[2]*z[33];
    z[33]=static_cast<T>(16)+ z[33];
    z[33]=z[2]*z[33];
    z[33]= - z[52] + z[33];
    z[33]=z[33]*z[127];
    z[31]=z[33] - n<T>(76,3)*z[20] + z[31];
    z[31]=z[5]*z[31];
    z[30]=10*z[9] - n<T>(23,3)*z[11] - z[45] - z[30] + z[34];
    z[33]= - z[105]*z[69];
    z[33]=z[33] + 5*z[67];
    z[33]=z[2]*z[33];
    z[30]=2*z[30] + z[33];
    z[30]=z[2]*z[30];
    z[33]= - static_cast<T>(9)+ 8*z[46];
    z[29]=z[29] + z[31] + 2*z[33] + z[30];
    z[29]=z[4]*z[29];
    z[28]=z[41] - z[28];
    z[28]=z[13]*z[28];
    z[28]=z[72] + z[28];
    z[28]=z[13]*z[28];
    z[30]= - z[39]*z[89];
    z[28]=z[30] - z[82] + z[28];
    z[28]=z[28]*z[24];
    z[30]= - 12*z[94] + z[131] - 23*z[18] - n<T>(14,3)*z[6];
    z[30]=z[17]*z[30];
    z[31]= - z[38] + n<T>(22,3)*z[7];
    z[31]=z[31]*z[146];
    z[30]=z[30] + z[31];
    z[30]=z[30]*z[56];
    z[31]= - z[11]*z[16];
    z[26]=z[31] - z[26];
    z[31]= - z[125]*z[101];
    z[33]=n<T>(4,3)*z[15] + z[93];
    z[33]=z[9]*z[33];
    z[26]=z[31] + 2*z[26] + z[33];
    z[26]=z[26]*z[58];
    z[31]= - z[87] + z[16] + z[72];
    z[31]=2*z[31] + n<T>(5,3)*z[6];
    z[33]=z[20]*z[86];
    z[33]=7*z[78] + n<T>(43,3) + z[33];
    z[33]=z[33]*z[83];
    z[34]=17*z[17] + 23*z[146];
    z[34]=z[34]*z[128];
    z[34]= - static_cast<T>(17)+ z[34];
    z[34]=z[11]*z[34];
    z[35]=z[80]*z[119];
    z[36]= - z[129] - 15*z[13];
    z[36]=z[13]*z[36];
    z[35]=z[36] + 4*z[35];
    z[35]=z[17]*z[35];
    z[36]=z[21] - n<T>(26,3)*z[13];
    z[35]=8*z[36] + z[35];
    z[35]=z[17]*z[35];
    z[35]=n<T>(40,3)*z[79] - n<T>(166,3) + z[35];
    z[35]=z[9]*z[35];
    z[22]=z[22] + z[23] + z[29] + z[25] + z[26] + z[27] + z[35] + z[34]
    + z[30] + z[28] + z[33] + 4*z[31] + z[135];
    z[22]=z[1]*z[22];
    z[23]=z[53]*z[3];
    z[25]= - z[125]*z[23];
    z[26]=z[76]*z[23];
    z[26]= - static_cast<T>(2)+ z[26];
    z[26]=z[5]*z[26];
    z[25]=z[25] + z[26];
    z[26]= - 3*z[16] + n<T>(2,3)*z[6];
    z[25]=4*z[26] - z[99] + n<T>(4,3)*z[25];
    z[25]=z[2]*z[25];
    z[26]= - z[32] + z[67];
    z[23]=z[26]*z[23];
    z[23]=static_cast<T>(5)+ z[23];
    z[23]=z[2]*z[23];
    z[26]=z[5]*z[37]*z[3]*z[43];
    z[23]=z[23] + z[26];
    z[23]=z[4]*z[23];
    z[26]= - z[13] - z[16] + n<T>(16,3)*z[6];
    z[24]=z[26]*z[24];
    z[26]=z[132] - z[40];
    z[26]=n<T>(109,3)*z[9] + 2*z[26] - n<T>(73,3)*z[11];
    z[26]=z[8]*z[26];

    r +=  - n<T>(5,3) + z[22] + n<T>(4,3)*z[23] + z[24] + z[25] + z[26] - n<T>(58,3)
      *z[74];
 
    return r;
}

template double qqb_2lNLC_r1037(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1037(const std::array<dd_real,31>&);
#endif
