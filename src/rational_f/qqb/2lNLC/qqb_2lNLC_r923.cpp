#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r923(const std::array<T,31>& k) {
  T z[39];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[14];
    z[7]=k[12];
    z[8]=k[13];
    z[9]=k[4];
    z[10]=k[15];
    z[11]=k[5];
    z[12]=k[8];
    z[13]=k[9];
    z[14]=z[12]*z[11];
    z[15]=z[14] - 1;
    z[16]=npow(z[12],2);
    z[15]=z[15]*z[16];
    z[17]=3*z[10];
    z[18]=z[10]*z[11];
    z[19]=static_cast<T>(1)+ z[18];
    z[19]=z[19]*z[17];
    z[19]=17*z[12] + z[19];
    z[19]=z[10]*z[19];
    z[19]=z[15] + z[19];
    z[20]=n<T>(3,2)*z[7];
    z[21]=25*z[4];
    z[22]=npow(z[18],2);
    z[22]= - n<T>(109,2) - 9*z[22];
    z[22]=z[10]*z[22];
    z[22]= - z[21] + z[20] + n<T>(3,2)*z[12] + z[22];
    z[23]=n<T>(1,2)*z[6];
    z[22]=z[22]*z[23];
    z[24]=3*z[12];
    z[25]=z[12]*npow(z[11],2);
    z[25]=n<T>(1,2)*z[25];
    z[26]= - 2*z[11] + z[25];
    z[26]=z[26]*z[24];
    z[26]=n<T>(131,4) + z[26];
    z[27]=z[8]*z[12];
    z[26]=z[26]*z[27];
    z[28]=z[4]*z[10];
    z[29]=z[7]*z[8];
    z[30]=z[6] - z[4];
    z[31]= - n<T>(49,4)*z[7] + 55*z[10] + n<T>(53,4)*z[8] - n<T>(59,4)*z[30];
    z[31]=z[3]*z[31];
    z[19]=z[31] + z[22] + n<T>(9,4)*z[28] + n<T>(31,2)*z[29] + n<T>(3,2)*z[19] + 
    z[26];
    z[19]=z[5]*z[19];
    z[22]=z[10]*z[9];
    z[26]= - static_cast<T>(1)- n<T>(1,2)*z[22];
    z[26]=z[26]*z[17];
    z[26]= - n<T>(17,2)*z[12] + z[26];
    z[26]=z[10]*z[26];
    z[31]=npow(z[12],3);
    z[32]=z[31]*z[11];
    z[26]= - n<T>(1,4)*z[27] + n<T>(1,2)*z[32] + z[26];
    z[25]=z[11] + z[25];
    z[24]=z[25]*z[24];
    z[24]=n<T>(97,4) + z[24];
    z[24]=z[12]*z[24];
    z[24]=z[24] - n<T>(29,2)*z[8];
    z[24]=z[7]*z[24];
    z[25]=z[10]*npow(z[9],2);
    z[25]= - z[9] - n<T>(1,2)*z[25];
    z[25]=z[10]*z[25];
    z[25]= - n<T>(127,4) + 9*z[25];
    z[25]=z[25]*z[28];
    z[33]=n<T>(9,2)*z[10];
    z[21]=z[33] + z[21];
    z[21]=z[21]*z[23];
    z[21]=z[21] + z[25] + 3*z[26] + z[24];
    z[21]=z[3]*z[21];
    z[23]=static_cast<T>(1)+ z[22];
    z[23]=z[23]*z[33];
    z[24]=2*z[13];
    z[23]= - z[24] + z[23];
    z[23]=z[10]*z[23];
    z[25]= - z[8]*z[24];
    z[23]= - z[29] + z[23] + z[25];
    z[23]=z[4]*z[23];
    z[25]=npow(z[10],2);
    z[25]=n<T>(9,2)*z[25];
    z[26]=z[11]*z[25];
    z[26]=z[24] + z[26];
    z[26]=z[10]*z[26];
    z[24]=z[8] - z[24] - n<T>(3,4)*z[12];
    z[24]=z[7]*z[24];
    z[33]=z[7] - z[8];
    z[34]=27*z[10] - z[33];
    z[34]=z[4]*z[34];
    z[24]=z[34] + z[26] + z[24];
    z[24]=z[6]*z[24];
    z[26]=npow(z[10],3);
    z[32]= - z[8]*z[32];
    z[32]= - 3*z[26] + z[32];
    z[34]= - z[13] - 15*z[12];
    z[34]=z[8]*z[34];
    z[15]= - n<T>(3,2)*z[15] + 2*z[34];
    z[15]=z[7]*z[15];
    z[15]=z[19] + z[21] + z[24] + z[23] + n<T>(3,2)*z[32] + z[15];
    z[19]=npow(z[2],2);
    z[15]=z[15]*z[19];
    z[21]=z[33] + z[30];
    z[21]=z[21]*z[3];
    z[23]=z[6]*z[4];
    z[24]=z[23] - z[29];
    z[30]= - z[21] + z[24];
    z[30]=z[3]*z[30];
    z[32]=npow(z[4],2);
    z[34]=z[4] + z[6];
    z[34]=z[6]*z[34];
    z[34]= - z[32] + z[34];
    z[34]=z[6]*z[34];
    z[35]=npow(z[4],3);
    z[30]=n<T>(27,2)*z[30] - z[35] + z[34];
    z[30]=z[3]*z[30];
    z[21]= - z[21] - z[24];
    z[21]=z[5]*z[21];
    z[34]=z[24]*z[3];
    z[21]= - z[34] + z[21];
    z[21]=z[5]*z[21];
    z[34]=npow(z[6],2);
    z[36]=z[34]*z[4];
    z[36]=z[36] + z[35];
    z[36]=z[36]*z[6];
    z[21]=n<T>(27,2)*z[21] + z[36] + z[30];
    z[21]=z[5]*z[21];
    z[30]=z[33]*z[4];
    z[33]= - z[29] - z[30];
    z[33]=z[33]*z[32];
    z[30]=z[29] - z[30];
    z[30]=z[6]*z[30];
    z[37]=z[4]*z[29];
    z[30]=z[37] + z[30];
    z[30]=z[6]*z[30];
    z[30]=z[33] + z[30];
    z[30]=z[6]*z[30];
    z[33]=npow(z[3],2);
    z[33]=n<T>(27,2)*z[33];
    z[37]=z[33]*z[24];
    z[37]=z[37] - z[36];
    z[38]=z[3]*z[37];
    z[24]=n<T>(27,2)*z[24];
    z[24]= - npow(z[5],2)*z[24];
    z[24]= - z[37] + z[24];
    z[24]=z[5]*z[3]*z[24];
    z[36]=z[29]*z[36];
    z[24]=z[36] + z[24];
    z[24]=z[1]*z[24];
    z[29]= - z[35]*z[29];
    z[21]=z[24] + z[21] + z[38] + z[29] + z[30];
    z[21]=z[1]*z[21];
    z[24]=z[8]*z[10];
    z[29]=z[12]*z[24];
    z[26]=9*z[26] + n<T>(125,2)*z[29];
    z[14]=3*z[14];
    z[29]=n<T>(17,2) + z[14];
    z[29]=z[29]*z[16];
    z[30]=z[10]*z[12];
    z[29]=z[29] - n<T>(129,2)*z[30];
    z[35]= - n<T>(87,2)*z[12] - z[8];
    z[35]=z[8]*z[35];
    z[29]=n<T>(1,2)*z[29] + z[35];
    z[29]=z[7]*z[29];
    z[22]=n<T>(1,2) + z[22];
    z[22]=z[22]*z[25];
    z[22]=z[22] + z[32];
    z[22]=z[4]*z[22];
    z[25]=z[7] - z[4];
    z[25]=z[25]*z[33];
    z[23]=z[10]*z[23];
    z[22]=z[25] + n<T>(75,2)*z[23] + z[22] + n<T>(1,2)*z[26] + z[29];
    z[22]=z[3]*z[22];
    z[23]=n<T>(129,4)*z[10] + 58*z[8];
    z[23]=z[7]*z[23];
    z[25]= - n<T>(39,4)*z[10] - 10*z[4];
    z[25]=z[6]*z[25];
    z[26]= - z[8] - z[6];
    z[26]=z[3]*z[26];
    z[23]=n<T>(9,2)*z[26] + 5*z[25] + n<T>(195,4)*z[28] - n<T>(125,4)*z[24] + z[23];
    z[23]=z[3]*z[23];
    z[18]= - n<T>(1,2) - z[18];
    z[18]=z[18]*z[17];
    z[18]= - n<T>(43,2)*z[12] + z[18];
    z[18]=z[18]*z[17];
    z[25]= - n<T>(129,2)*z[10] - 85*z[8];
    z[25]=z[7]*z[25];
    z[18]= - 75*z[28] + z[25] + z[18] - 85*z[27];
    z[18]=n<T>(1,2)*z[18] + z[34];
    z[18]=z[6]*z[18];
    z[14]= - n<T>(17,2) + z[14];
    z[14]=z[14]*z[16];
    z[14]=z[14] - n<T>(125,2)*z[30];
    z[14]=z[8]*z[14];
    z[16]= - 11*z[7] - 9*z[4];
    z[16]=z[3]*z[16];
    z[25]=z[8] - z[6];
    z[25]=z[5]*z[25];
    z[16]=z[16] + 27*z[25];
    z[16]=z[5]*z[16];
    z[14]=z[16] + 3*z[31] + z[14];
    z[14]=z[23] + z[18] + n<T>(1,2)*z[14];
    z[14]=z[5]*z[14];
    z[16]=z[13] + n<T>(43,4)*z[12];
    z[16]=z[16]*z[17];
    z[18]=3*z[13] + n<T>(85,2)*z[12];
    z[18]=z[8]*z[18];
    z[16]= - z[34] + z[16] + z[18];
    z[16]=z[7]*z[16];
    z[18]=z[17]*z[8];
    z[17]=z[17] + 4*z[8];
    z[17]=z[7]*z[17];
    z[17]= - z[18] + z[17];
    z[17]=z[4]*z[17];
    z[18]= - z[13]*z[18];
    z[16]=z[17] + z[18] + z[16];
    z[16]=z[6]*z[16];
    z[17]= - z[31]*z[20];
    z[18]= - z[10] - z[8];
    z[18]=z[7]*z[18];
    z[18]=z[24] + z[18];
    z[18]=z[13]*z[18];
    z[20]= - z[8]*z[32];
    z[18]=3*z[18] + z[20];
    z[18]=z[4]*z[18];
    z[14]=z[21] + z[14] + z[22] + z[16] + z[17] + z[18];
    z[14]=z[1]*z[19]*z[14];
    z[14]=z[15] + z[14];

    r += z[14]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r923(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r923(const std::array<dd_real,31>&);
#endif
