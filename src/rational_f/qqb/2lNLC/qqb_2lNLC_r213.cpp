#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r213(const std::array<T,31>& k) {
  T z[65];
  T r = 0;

    z[1]=k[1];
    z[2]=k[12];
    z[3]=k[13];
    z[4]=k[14];
    z[5]=k[5];
    z[6]=k[4];
    z[7]=k[7];
    z[8]=k[10];
    z[9]=k[11];
    z[10]=k[3];
    z[11]=k[9];
    z[12]=k[15];
    z[13]=k[8];
    z[14]=n<T>(1,2)*z[13];
    z[15]=z[14] - z[12];
    z[16]=z[6]*z[8];
    z[17]=z[16]*z[12];
    z[18]=n<T>(1,2)*z[17];
    z[19]=z[18] - z[15];
    z[19]=z[6]*z[19];
    z[20]=n<T>(1,2)*z[5];
    z[21]=z[13]*z[7];
    z[22]=z[21]*z[5];
    z[23]=z[22] - z[13];
    z[24]=z[12] - z[23];
    z[24]=z[24]*z[20];
    z[19]=z[19] + z[24];
    z[19]=z[2]*z[19];
    z[24]=n<T>(1,2)*z[9];
    z[25]=z[7]*z[10];
    z[26]=n<T>(3,2) - z[25];
    z[26]=z[26]*z[24];
    z[27]=z[8]*z[12];
    z[28]=n<T>(1,2)*z[4];
    z[29]= - z[8]*z[28];
    z[29]=z[27] + z[29];
    z[29]=z[6]*z[29];
    z[30]=n<T>(3,2)*z[13];
    z[19]=z[19] + z[22] + z[29] - z[28] + z[26] - z[30] + z[12];
    z[19]=z[2]*z[19];
    z[26]=z[9]*z[7];
    z[29]=n<T>(1,2)*z[21];
    z[31]= - z[29] + z[26];
    z[32]=z[6]*npow(z[8],2);
    z[32]=z[32] - z[11] + z[8];
    z[32]=z[4]*z[32];
    z[31]=3*z[31] + z[27] + z[32];
    z[19]=n<T>(1,2)*z[31] + z[19];
    z[31]=n<T>(1,2)*z[2];
    z[19]=z[19]*z[31];
    z[32]=z[5]*z[7];
    z[33]=z[32] - 1;
    z[33]=z[33]*z[5];
    z[34]=static_cast<T>(1)+ z[16];
    z[34]=z[6]*z[34];
    z[34]=z[34] + z[33];
    z[34]=z[34]*z[31];
    z[35]=3*z[8];
    z[36]= - n<T>(7,2)*z[9] + z[35];
    z[36]=z[6]*z[36];
    z[36]=static_cast<T>(3)+ z[36];
    z[32]=z[34] + n<T>(1,2)*z[36] - z[32];
    z[32]=z[2]*z[32];
    z[34]=3*z[9];
    z[36]=z[7] + z[34];
    z[36]=n<T>(1,2)*z[36] + z[8];
    z[32]=n<T>(3,2)*z[36] + z[32];
    z[32]=z[2]*z[32];
    z[36]=n<T>(3,4)*z[13];
    z[37]=z[7] + z[36];
    z[37]=z[9]*z[37];
    z[38]=n<T>(1,2)*z[8];
    z[39]= - z[11]*z[38];
    z[40]=n<T>(7,4)*z[8];
    z[41]= - z[12] + z[40];
    z[41]=z[4]*z[41];
    z[42]=npow(z[4],2)*z[16];
    z[32]=z[32] + n<T>(1,2)*z[42] + z[41] + z[37] + z[39];
    z[37]=npow(z[6],2);
    z[39]=n<T>(1,2) - z[16];
    z[39]=z[39]*z[37];
    z[33]=z[33] + z[6];
    z[41]= - z[33]*z[20];
    z[39]=z[39] + z[41];
    z[39]=z[39]*z[31];
    z[41]=z[7] + n<T>(5,2)*z[4];
    z[41]=z[5]*z[41];
    z[41]= - n<T>(5,2) + z[41];
    z[41]=z[41]*z[20];
    z[42]=n<T>(5,8)*z[9] - 2*z[8];
    z[42]=z[6]*z[42];
    z[42]=n<T>(3,4) + z[42];
    z[42]=z[6]*z[42];
    z[39]=z[39] + z[42] + z[41];
    z[39]=z[2]*z[39];
    z[41]= - z[24] - z[35];
    z[41]=z[6]*z[41];
    z[42]=z[5]*z[4];
    z[39]=z[39] + n<T>(1,4)*z[42] + n<T>(1,4) + z[41];
    z[39]=z[2]*z[39];
    z[41]=n<T>(1,2)*z[6];
    z[43]= - z[9]*z[41];
    z[43]=static_cast<T>(1)+ z[43];
    z[43]=z[43]*z[37];
    z[44]=z[42] - 1;
    z[45]=z[44]*z[5];
    z[46]=z[45] + z[6];
    z[47]=z[46]*z[5];
    z[43]=z[43] - z[47];
    z[43]=z[43]*z[31];
    z[48]=n<T>(1,4)*z[9];
    z[49]=z[6]*z[48];
    z[49]=static_cast<T>(1)+ z[49];
    z[49]=z[6]*z[49];
    z[43]=z[43] + z[49] + z[45];
    z[43]=z[2]*z[43];
    z[49]=z[4]*z[12];
    z[50]=z[9]*z[13];
    z[51]=n<T>(1,4)*z[50] - z[49];
    z[52]=npow(z[5],2);
    z[51]=z[51]*z[52];
    z[43]=z[51] + z[43];
    z[51]=n<T>(1,2)*z[3];
    z[43]=z[43]*z[51];
    z[53]=n<T>(1,4)*z[4];
    z[54]=z[16]*z[53];
    z[55]=z[10]*z[11];
    z[56]= - static_cast<T>(3)+ z[55];
    z[56]=z[8]*z[56];
    z[54]=z[54] + z[56] - z[4];
    z[56]=n<T>(1,4)*z[7] + z[13];
    z[56]=z[56]*z[24];
    z[56]=z[56] - z[49];
    z[56]=z[5]*z[56];
    z[39]=z[43] + z[39] + n<T>(1,2)*z[54] + z[56];
    z[39]=z[3]*z[39];
    z[32]=n<T>(1,2)*z[32] + z[39];
    z[32]=z[3]*z[32];
    z[39]=2*z[12];
    z[43]=n<T>(1,2)*z[7];
    z[54]=z[43] - z[39];
    z[54]=z[9]*z[54];
    z[56]=z[43] + z[9];
    z[40]= - z[56]*z[40];
    z[40]=z[54] + z[40];
    z[40]=z[4]*z[40];
    z[54]=z[2]*z[42];
    z[54]=z[53] + z[54];
    z[54]=z[2]*z[54];
    z[57]=static_cast<T>(1)- n<T>(3,2)*z[42];
    z[57]=z[5]*z[57];
    z[57]= - z[41] + z[57];
    z[57]=z[2]*z[57];
    z[57]=n<T>(1,2)*z[44] + z[57];
    z[57]=z[2]*z[57];
    z[57]=z[28] + z[57];
    z[57]=z[57]*z[51];
    z[54]=z[54] + z[57];
    z[57]=npow(z[3],2);
    z[54]=z[54]*z[57];
    z[58]=npow(z[2],2);
    z[59]=static_cast<T>(1)- 3*z[42];
    z[59]=z[3]*z[59]*z[58];
    z[58]=z[58]*z[4];
    z[59]=z[58] + z[59];
    z[57]=z[59]*z[57];
    z[58]= - z[1]*npow(z[3],3)*z[58];
    z[57]=z[57] + z[58];
    z[57]=z[1]*z[57];
    z[58]=z[4]*z[8]*z[26];
    z[54]=n<T>(1,4)*z[57] - n<T>(3,8)*z[58] + z[54];
    z[54]=z[1]*z[54];
    z[57]=z[12]*z[7];
    z[26]=z[57] + n<T>(3,4)*z[26];
    z[26]=z[8]*z[26];
    z[19]=z[54] + z[32] + z[19] + z[26] + z[40];
    z[19]=z[1]*z[19];
    z[26]=z[50] - z[49];
    z[26]=z[26]*z[20];
    z[32]=z[48] - z[12];
    z[40]=n<T>(1,4)*z[13];
    z[26]=z[26] - z[40] - z[32];
    z[26]=z[5]*z[26];
    z[26]=static_cast<T>(1)+ z[26];
    z[26]=z[5]*z[26];
    z[54]=z[37]*z[24];
    z[54]= - z[10] + z[54];
    z[57]=z[6]*z[9];
    z[58]=z[57] + 1;
    z[59]= - z[58]*z[37];
    z[47]=z[59] + z[47];
    z[47]=z[47]*z[31];
    z[26]=z[47] + n<T>(1,2)*z[54] + z[26];
    z[26]=z[26]*z[51];
    z[47]=3*z[50] + z[49];
    z[47]=z[47]*z[20];
    z[54]=z[12] - z[13];
    z[59]=n<T>(3,4)*z[9];
    z[47]=z[47] - z[59] + z[54];
    z[47]=z[47]*z[20];
    z[60]= - n<T>(7,8) - z[16];
    z[60]=z[60]*z[37];
    z[46]=z[46]*z[20];
    z[46]=z[60] + z[46];
    z[46]=z[2]*z[46];
    z[59]=z[59] - 4*z[8];
    z[59]=z[6]*z[59];
    z[59]= - n<T>(5,4) + z[59];
    z[59]=z[6]*z[59];
    z[46]=z[46] + z[59] - n<T>(1,4)*z[45];
    z[46]=z[2]*z[46];
    z[59]=z[8]*z[10];
    z[60]=n<T>(15,8) - z[55];
    z[60]=z[60]*z[59];
    z[61]= - z[9] - n<T>(25,4)*z[8];
    z[61]=z[61]*z[41];
    z[26]=z[26] + z[46] + z[47] + z[61] + z[60] - n<T>(5,8) + z[55];
    z[26]=z[3]*z[26];
    z[46]=z[55] - n<T>(3,2);
    z[35]=z[46]*z[35];
    z[47]=n<T>(3,4)*z[7];
    z[35]=z[35] + n<T>(9,2)*z[9] - z[47] - z[54];
    z[60]= - z[52]*z[53];
    z[61]= - n<T>(5,2) - z[16];
    z[61]=z[61]*z[37];
    z[33]=z[5]*z[33];
    z[33]=z[61] + z[33];
    z[33]=z[33]*z[31];
    z[61]= - n<T>(5,4) - z[16];
    z[61]=z[6]*z[61];
    z[33]=z[33] + z[61] + z[60];
    z[33]=z[2]*z[33];
    z[57]= - static_cast<T>(1)- 3*z[57];
    z[47]= - z[47] - z[4];
    z[47]=z[5]*z[47];
    z[47]=n<T>(1,2)*z[57] + z[47];
    z[33]=n<T>(1,2)*z[47] + z[33];
    z[33]=z[2]*z[33];
    z[47]=z[7]*z[24];
    z[47]=z[47] + 5*z[49];
    z[57]=n<T>(1,4)*z[5];
    z[47]=z[47]*z[57];
    z[60]=n<T>(3,8)*z[16] + 1;
    z[60]=z[4]*z[60];
    z[26]=z[26] + z[33] + z[47] + n<T>(1,2)*z[35] + z[60];
    z[26]=z[3]*z[26];
    z[33]=n<T>(1,2)*z[11];
    z[35]=z[42]*z[33];
    z[47]=n<T>(3,2)*z[12];
    z[35]=z[35] - z[28] - z[47] - z[43] - z[13];
    z[35]=z[35]*z[20];
    z[43]=n<T>(5,2)*z[12];
    z[60]= - z[43] - z[17];
    z[61]=n<T>(1,2)*z[37];
    z[60]=z[60]*z[61];
    z[62]= - n<T>(1,2)*z[12] - z[23];
    z[62]=z[62]*z[20];
    z[63]=z[14] + z[12];
    z[64]= - z[6]*z[63];
    z[62]=z[62] - n<T>(1,4) + z[64];
    z[62]=z[5]*z[62];
    z[60]=z[60] + z[62];
    z[60]=z[2]*z[60];
    z[62]= - z[4] + z[13] - 7*z[12];
    z[62]=n<T>(1,4)*z[62] - z[17];
    z[62]=z[6]*z[62];
    z[64]= - n<T>(1,2) + z[25];
    z[35]=z[60] + z[35] + n<T>(1,4)*z[64] + z[62];
    z[35]=z[2]*z[35];
    z[30]= - z[47] - z[7] + z[30];
    z[60]=n<T>(1,4)*z[25];
    z[62]=static_cast<T>(1)+ z[60];
    z[62]=z[9]*z[62];
    z[18]=z[35] - z[18] + z[28] - n<T>(3,4)*z[8] + n<T>(1,2)*z[30] + z[62];
    z[18]=z[2]*z[18];
    z[30]=n<T>(1,8)*z[7] - z[39];
    z[30]=z[30]*z[42];
    z[30]=z[30] - z[36] + z[39];
    z[30]=z[9]*z[30];
    z[35]=z[53] - z[38] + z[24] + n<T>(5,4)*z[12] + n<T>(1,4)*z[11] - z[7];
    z[35]=z[4]*z[35];
    z[36]= - z[43] - z[11] - n<T>(3,2)*z[7];
    z[39]=static_cast<T>(2)+ z[60];
    z[39]=z[9]*z[39];
    z[36]=n<T>(1,4)*z[8] + n<T>(1,2)*z[36] + z[39];
    z[36]=z[8]*z[36];
    z[39]= - z[8]*z[56]*z[53];
    z[27]= - z[7]*z[27];
    z[27]=z[27] + z[39];
    z[27]=z[6]*z[27];
    z[18]=z[19] + z[26] + z[18] + z[27] + z[35] + z[36] - z[29] + z[30];
    z[18]=z[1]*z[18];
    z[19]=npow(z[10],2);
    z[26]= - static_cast<T>(1)+ z[55];
    z[26]=z[26]*z[19]*z[38];
    z[27]=z[50] + z[49];
    z[27]=z[5]*z[27];
    z[24]=z[27] - z[24] - z[63];
    z[24]=z[24]*z[57];
    z[27]=z[40] + z[12];
    z[27]=z[27]*z[41];
    z[30]=n<T>(1,2)*z[55];
    z[24]=z[24] + z[27] - static_cast<T>(1)+ z[30];
    z[24]=z[5]*z[24];
    z[27]= - n<T>(7,4) - z[16];
    z[27]=z[27]*z[37];
    z[35]= - n<T>(7,2)*z[6] - z[45];
    z[35]=z[35]*z[20];
    z[27]=z[27] + z[35];
    z[27]=z[27]*z[31];
    z[35]=z[9] + z[13];
    z[36]= - z[35]*z[41];
    z[39]=z[5]*z[50];
    z[35]=z[39] - z[35];
    z[35]=z[35]*z[20];
    z[35]=z[35] - static_cast<T>(1)+ z[36];
    z[35]=z[35]*z[20];
    z[36]= - z[37]*z[48];
    z[35]=z[35] + z[10] + z[36];
    z[35]=z[5]*z[35];
    z[36]= - z[58]*z[61];
    z[19]= - z[19] + z[36];
    z[19]=n<T>(1,2)*z[19] + z[35];
    z[19]=z[19]*z[51];
    z[35]=z[48] - z[8];
    z[35]=z[35]*z[41];
    z[36]=z[10]*z[38];
    z[35]=z[35] - static_cast<T>(1)+ z[36];
    z[35]=z[6]*z[35];
    z[36]= - z[10]*z[46];
    z[19]=z[19] + z[27] + z[24] + z[35] + z[36] + z[26];
    z[19]=z[3]*z[19];
    z[24]=n<T>(3,2)*z[50] + z[49];
    z[24]=z[5]*z[24];
    z[15]=z[24] + z[34] + 3*z[11] + z[15];
    z[15]=z[15]*z[57];
    z[24]= - n<T>(3,2) - z[16];
    z[24]=z[24]*z[37];
    z[20]= - z[44]*z[20];
    z[20]= - z[6] + z[20];
    z[20]=z[5]*z[20];
    z[20]=z[24] + z[20];
    z[20]=z[2]*z[20];
    z[16]= - n<T>(3,2) - 5*z[16];
    z[16]=z[6]*z[16];
    z[24]=n<T>(17,2) - z[42];
    z[24]=z[5]*z[24];
    z[16]=z[16] + z[24];
    z[16]=n<T>(1,2)*z[16] + z[20];
    z[16]=z[16]*z[31];
    z[20]=static_cast<T>(1)- 3*z[55];
    z[20]=z[20]*z[59];
    z[24]= - n<T>(9,4)*z[8] - n<T>(1,8)*z[13] - z[32];
    z[24]=z[6]*z[24];
    z[15]=z[19] + z[16] + z[15] + z[24] - static_cast<T>(1)+ n<T>(1,4)*z[20];
    z[15]=z[3]*z[15];
    z[16]= - z[54]*z[41];
    z[19]=z[11]*z[28];
    z[19]= - z[21] + z[19];
    z[19]=z[5]*z[19];
    z[20]= - z[11] + z[13];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[5]*z[19];
    z[16]=z[19] + static_cast<T>(1)+ z[16];
    z[16]=z[5]*z[16];
    z[19]= - z[6]*z[13];
    z[20]= - z[5]*z[23];
    z[19]=z[20] + static_cast<T>(1)+ z[19];
    z[19]=z[19]*z[52]*z[31];
    z[20]= - z[47] - z[17];
    z[20]=z[20]*z[37];
    z[16]=z[19] + z[20] + z[16];
    z[16]=z[2]*z[16];
    z[19]= - z[13] - 3*z[12];
    z[17]= - n<T>(5,2)*z[17] + n<T>(1,2)*z[19] + z[4];
    z[17]=z[6]*z[17];
    z[14]= - n<T>(3,4)*z[22] + z[4] - z[14] - n<T>(3,2)*z[11] + z[7];
    z[14]=z[5]*z[14];
    z[14]=z[16] + z[14] + z[17] - n<T>(3,2) - z[25];
    z[14]=z[14]*z[31];
    z[16]= - z[25] + static_cast<T>(1)+ z[30];
    z[16]=z[8]*z[16];
    z[17]=z[33] + z[12];
    z[16]=z[16] + z[7] + z[17];
    z[19]=z[29] + z[50];
    z[20]=z[7] - z[12];
    z[20]=z[8]*z[20];
    z[19]=n<T>(1,2)*z[19] + z[20];
    z[20]= - z[4]*z[9];
    z[19]=n<T>(1,2)*z[19] + z[20];
    z[19]=z[6]*z[19];
    z[20]= - z[29] + z[50];
    z[17]= - n<T>(1,2)*z[17] + z[9];
    z[17]=z[4]*z[17];
    z[17]=n<T>(1,4)*z[20] + z[17];
    z[17]=z[5]*z[17];
    z[14]=z[18] + z[15] + z[14] + z[17] + z[19] + n<T>(1,2)*z[16] - z[4];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r213(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r213(const std::array<dd_real,31>&);
#endif
