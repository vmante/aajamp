#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1067(const std::array<T,31>& k) {
  T z[94];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[7];
    z[9]=k[15];
    z[10]=k[17];
    z[11]=k[14];
    z[12]=k[19];
    z[13]=k[12];
    z[14]=k[8];
    z[15]=k[11];
    z[16]=k[22];
    z[17]=k[2];
    z[18]=k[5];
    z[19]=2*z[14];
    z[20]=z[14]*z[17];
    z[21]=static_cast<T>(1)- z[20];
    z[21]=z[21]*z[19];
    z[22]=z[6]*z[2];
    z[23]=3*z[22];
    z[24]=z[23] - 2;
    z[25]=n<T>(1,2)*z[15];
    z[26]= - z[2]*z[25];
    z[26]=z[26] + z[24];
    z[26]=z[5]*z[26];
    z[27]=n<T>(3,2)*z[15];
    z[28]=n<T>(1,2)*z[13];
    z[29]= - z[2]*npow(z[6],2);
    z[21]=z[26] + z[27] + z[29] + z[21] - z[28];
    z[21]=z[5]*z[21];
    z[26]=2*z[5];
    z[29]=npow(z[2],2);
    z[30]=z[29]*z[6];
    z[31]= - z[2] + z[30];
    z[31]=z[31]*z[26];
    z[32]=4*z[2] + z[30];
    z[32]=z[6]*z[32];
    z[31]=z[31] + z[32] - static_cast<T>(7)+ 4*z[20];
    z[31]=z[5]*z[31];
    z[32]=3*z[11];
    z[33]= - z[13]*z[32];
    z[34]=z[6] - z[13];
    z[35]=z[9]*z[34];
    z[33]=z[35] + z[33];
    z[33]=z[18]*z[33];
    z[35]=z[20]*z[11];
    z[36]=z[35] - z[11];
    z[33]= - z[36] + z[33];
    z[37]=z[2] - z[17];
    z[38]=z[30] - z[37];
    z[38]=z[5]*z[38];
    z[39]=npow(z[18],2);
    z[40]=z[39]*z[11];
    z[41]= - z[13]*z[40];
    z[38]=z[38] - static_cast<T>(1)+ z[41];
    z[41]=12*z[4];
    z[38]=z[38]*z[41];
    z[31]=z[38] + 4*z[33] + z[31];
    z[31]=z[4]*z[31];
    z[33]=z[19]*z[18];
    z[38]=z[33] + 1;
    z[42]= - z[14]*z[38];
    z[43]=n<T>(1,2)*z[6];
    z[44]= - z[18]*z[43];
    z[44]=static_cast<T>(1)+ z[44];
    z[44]=z[6]*z[44];
    z[42]=z[44] + z[42] + z[28];
    z[42]=z[8]*z[42];
    z[44]=z[15] - z[8];
    z[45]=z[44] - z[34];
    z[46]=2*z[9];
    z[47]=z[46]*z[18];
    z[45]=z[45]*z[47];
    z[48]=3*z[15];
    z[49]=2*z[6];
    z[50]= - z[13] - z[49];
    z[45]=z[45] + z[48] + 2*z[50] - z[8];
    z[45]=z[9]*z[45];
    z[50]=z[13]*z[11];
    z[35]=z[11] + z[35];
    z[35]=z[14]*z[35];
    z[35]=z[35] - z[50];
    z[51]= - z[11] + z[6];
    z[51]=z[51]*z[43];
    z[52]=n<T>(1,2)*z[11];
    z[33]=z[33] - 1;
    z[53]=z[14]*z[33];
    z[53]=z[52] + z[53];
    z[53]=z[15]*z[53];
    z[21]=z[31] + z[21] + z[45] + z[53] + z[42] + 2*z[35] + z[51];
    z[21]=z[4]*z[21];
    z[31]=z[14] - z[16];
    z[35]=z[28] + 4*z[31];
    z[35]=z[35]*z[13];
    z[42]=npow(z[14],2);
    z[45]=2*z[42];
    z[51]=4*z[16];
    z[53]=z[51] + z[6];
    z[53]=z[6]*z[53];
    z[54]=z[6] - z[15];
    z[54]=z[9]*z[54];
    z[55]= - z[15]*z[26];
    z[53]=z[55] + 4*z[54] + z[53] - z[45] + z[35];
    z[53]=z[5]*z[53];
    z[54]=z[38]*z[45];
    z[55]=n<T>(3,2)*z[6];
    z[56]= - z[51] + z[55];
    z[56]=z[6]*z[56];
    z[35]=z[56] + z[54] - z[35];
    z[35]=z[8]*z[35];
    z[56]=npow(z[11],2);
    z[57]= - z[56]*z[43];
    z[33]=z[33]*z[45];
    z[58]=n<T>(1,2)*z[56] - z[33];
    z[58]=z[15]*z[58];
    z[47]= - z[44]*z[47];
    z[47]=z[47] + z[6] - z[48];
    z[47]=z[47]*npow(z[9],2);
    z[21]=z[21] + z[53] + 2*z[47] + z[58] + z[57] + z[35];
    z[21]=z[4]*z[21];
    z[35]=npow(z[8],2);
    z[47]=2*z[7];
    z[48]=z[35]*z[47];
    z[53]=npow(z[7],2);
    z[57]=z[53]*z[8];
    z[58]=z[57] - z[7];
    z[58]=z[58]*z[8];
    z[59]=z[20] + z[58];
    z[59]=z[59]*z[46];
    z[20]= - static_cast<T>(2)- z[20];
    z[20]=z[14]*z[20];
    z[20]=z[59] + z[20] + z[48];
    z[20]=z[9]*z[20];
    z[59]= - z[10]*z[49];
    z[60]=2*z[12];
    z[61]=z[8]*z[60];
    z[62]=z[14] - z[12];
    z[63]=z[10] + z[62];
    z[63]=z[15]*z[63];
    z[20]=z[20] + 2*z[63] + z[61] + z[42] + z[59];
    z[20]=z[20]*z[46];
    z[59]=npow(z[16],2);
    z[61]=5*z[59];
    z[63]=npow(z[10],2);
    z[64]=z[61] + 3*z[63];
    z[65]=z[10] + z[16];
    z[66]=z[14]*z[7];
    z[67]=static_cast<T>(1)- z[66];
    z[67]=z[14]*z[67];
    z[67]= - n<T>(3,2)*z[65] + z[67];
    z[67]=z[13]*z[67];
    z[68]=3*z[42];
    z[64]=z[67] + n<T>(1,2)*z[64] - z[68];
    z[64]=z[13]*z[64];
    z[67]=z[12]*z[2];
    z[69]= - static_cast<T>(1)+ 4*z[67];
    z[70]=npow(z[12],2);
    z[69]=z[69]*z[70];
    z[69]=z[69] + n<T>(5,2)*z[63];
    z[67]= - static_cast<T>(1)- z[67];
    z[67]=z[67]*z[60];
    z[67]= - n<T>(5,2)*z[10] + z[67];
    z[67]=z[8]*z[67];
    z[67]=z[67] + z[69];
    z[67]=z[8]*z[67];
    z[71]=z[60]*z[2];
    z[72]=z[71] - 3;
    z[72]=z[72]*z[12];
    z[73]= - static_cast<T>(2)- z[66];
    z[73]=z[14]*z[73];
    z[73]= - z[72] + z[73];
    z[73]=z[15]*z[73];
    z[69]=z[73] - z[68] - z[69];
    z[69]=z[15]*z[69];
    z[73]=z[59] + z[63];
    z[74]=z[6]*z[16];
    z[75]= - n<T>(5,2)*z[73] - z[74];
    z[75]=z[6]*z[75];
    z[76]=n<T>(3,2)*z[13];
    z[27]=z[27] - z[76] + z[6];
    z[77]=z[5]*z[10];
    z[27]=z[27]*z[77];
    z[20]=z[27] + z[20] + z[69] + z[67] + z[64] + z[75];
    z[20]=z[5]*z[20];
    z[27]=2*z[18];
    z[64]=z[70]*z[27];
    z[67]=z[42]*z[39];
    z[69]= - static_cast<T>(1)+ z[67];
    z[69]=z[69]*z[19];
    z[69]=z[69] - z[64] + n<T>(5,2)*z[16] + z[60];
    z[69]=z[6]*z[69];
    z[66]=static_cast<T>(1)+ z[66];
    z[66]=z[66]*z[19];
    z[75]=n<T>(3,2)*z[11];
    z[66]=z[75] + z[66];
    z[66]=z[14]*z[66];
    z[78]= - 3*z[14] + 5*z[16];
    z[79]=7*z[12];
    z[80]= - z[79] - z[78];
    z[80]=z[80]*z[28];
    z[81]=2*z[70];
    z[66]=z[69] + z[80] - z[81] + z[66];
    z[66]=z[15]*z[66];
    z[69]=7*z[70];
    z[61]=z[61] + z[69];
    z[61]=n<T>(1,2)*z[61];
    z[78]=z[12] + z[78];
    z[28]=z[78]*z[28];
    z[28]=z[28] - z[61] + z[68];
    z[28]=z[13]*z[28];
    z[68]=npow(z[12],3);
    z[68]=4*z[68];
    z[78]=z[68]*z[18];
    z[78]= - n<T>(5,2)*z[59] + z[78] - z[70];
    z[80]= - z[54] - z[78];
    z[80]=z[6]*z[80];
    z[82]=5*z[63];
    z[69]=z[82] + z[69];
    z[83]=n<T>(1,2)*z[12];
    z[84]=z[10] + z[83];
    z[84]=z[11]*z[84];
    z[84]=n<T>(1,2)*z[69] + z[84];
    z[84]=z[11]*z[84];
    z[85]=2*z[11];
    z[86]=z[85] - z[14];
    z[86]=z[86]*z[45];
    z[28]=z[66] + z[80] + z[28] + z[86] - z[68] + z[84];
    z[28]=z[15]*z[28];
    z[66]=4*z[13];
    z[80]= - z[6]*z[18];
    z[80]=static_cast<T>(1)+ z[80];
    z[80]=z[8]*z[80];
    z[80]=z[80] - z[49] + z[32] + z[66];
    z[80]=z[8]*z[80];
    z[36]=z[14]*z[36];
    z[84]=z[18]*z[42];
    z[84]= - z[11] + z[84];
    z[84]=z[13]*z[84];
    z[36]=z[36] + z[84];
    z[84]=z[14]*z[18];
    z[86]= - static_cast<T>(1)- z[84];
    z[86]=z[86]*z[19];
    z[87]=z[11]*z[18];
    z[88]= - static_cast<T>(2)- z[87];
    z[88]=z[11]*z[88];
    z[86]=z[88] + z[86];
    z[86]=z[6]*z[86];
    z[88]=z[85]*z[15];
    z[89]=z[27]*z[11];
    z[90]=static_cast<T>(3)- z[89];
    z[90]=z[90]*z[88];
    z[91]=z[27] - z[40];
    z[91]=z[15]*z[91];
    z[92]= - z[17]*z[19];
    z[91]=z[91] + z[92];
    z[91]=z[11]*z[91];
    z[92]=z[47]*z[8];
    z[93]=static_cast<T>(3)+ z[92];
    z[93]=z[8]*z[93];
    z[91]=z[93] + z[91];
    z[91]=z[91]*z[46];
    z[36]=z[91] + z[90] + z[80] + 2*z[36] + z[86];
    z[36]=z[9]*z[36];
    z[80]=n<T>(3,2)*z[56];
    z[86]=4*z[6];
    z[90]=z[12]*z[86];
    z[91]= - z[11] - z[13];
    z[91]=n<T>(3,2)*z[91] + z[6];
    z[91]=z[8]*z[91];
    z[90]=z[91] + z[80] + z[90];
    z[90]=z[8]*z[90];
    z[91]=4*z[10];
    z[93]= - z[91] - z[32];
    z[93]=z[11]*z[93];
    z[62]=z[62]*z[86];
    z[62]=z[93] + z[62];
    z[62]=z[15]*z[62];
    z[33]= - z[80] - z[33];
    z[33]=z[13]*z[33];
    z[38]=z[38]*z[42];
    z[80]=2*z[10] - z[11];
    z[80]=z[11]*z[80];
    z[38]=z[80] + z[38];
    z[38]=z[38]*z[49];
    z[33]=z[36] + z[62] + z[90] + z[33] + z[38];
    z[33]=z[9]*z[33];
    z[36]=z[59]*z[56];
    z[38]= - z[63]*z[50];
    z[36]=z[36] + z[38];
    z[36]=z[13]*z[36];
    z[38]=npow(z[13],2);
    z[62]=z[56] + z[38];
    z[62]=z[70]*z[62];
    z[70]= - z[10] - z[12];
    z[70]=z[11]*z[70];
    z[70]= - z[63] + z[70];
    z[70]=z[8]*z[11]*z[70];
    z[70]=z[70] + z[62];
    z[70]=z[8]*z[70];
    z[80]= - z[59] - z[74];
    z[56]=z[6]*z[56]*z[80];
    z[36]=3*z[70] + z[36] + z[56];
    z[56]=z[11] + z[15];
    z[56]=z[42]*z[56];
    z[70]=npow(z[5],2);
    z[25]=z[70]*z[25];
    z[80]=static_cast<T>(1)- z[22];
    z[70]=z[80]*z[70];
    z[80]=4*z[11];
    z[86]= - z[14]*z[80];
    z[70]=z[86] + z[70];
    z[70]=z[4]*z[70];
    z[25]=z[70] + 2*z[56] + z[25];
    z[25]=z[4]*z[25];
    z[56]=npow(z[14],3);
    z[70]=z[15]*z[56];
    z[25]= - 4*z[70] + z[25];
    z[25]=z[4]*z[25];
    z[70]= - z[16] - z[12];
    z[70]=z[13]*z[70];
    z[70]=z[59] + z[70];
    z[70]=z[70]*z[76];
    z[55]= - z[59]*z[55];
    z[76]=2*z[56];
    z[55]=z[55] - z[76] + z[70];
    z[55]=z[15]*z[55];
    z[55]= - n<T>(3,2)*z[62] + z[55];
    z[55]=z[15]*z[55];
    z[62]=z[38] + 3*z[35];
    z[62]=z[63]*z[62];
    z[38]=z[38]*z[77];
    z[38]=z[62] - z[38];
    z[48]= - npow(z[9],3)*z[48];
    z[62]=z[42]*npow(z[15],2);
    z[38]=z[48] + z[62] + n<T>(1,2)*z[38];
    z[38]=z[5]*z[38];
    z[42]=z[13]*z[42];
    z[35]= - z[9]*z[35];
    z[35]=z[42] + z[35];
    z[35]=z[9]*z[35];
    z[42]= - z[13]*z[76];
    z[35]=z[42] + z[35];
    z[35]=z[35]*z[46];
    z[25]=z[25] + z[38] + z[35] + n<T>(1,2)*z[36] + z[55];
    z[25]=z[1]*z[25];
    z[35]=z[39]*z[45];
    z[35]=static_cast<T>(1)+ z[35];
    z[35]=z[14]*z[35];
    z[35]=z[35] - z[16] - z[83];
    z[35]=z[13]*z[35];
    z[35]=z[35] + z[61] + z[54];
    z[35]=z[13]*z[35];
    z[36]= - n<T>(3,2)*z[74] + z[78];
    z[36]=z[6]*z[36];
    z[38]=5*z[10];
    z[42]=z[38] + z[79];
    z[42]=z[42]*z[52];
    z[48]=z[64] + z[12];
    z[54]= - z[6]*z[48];
    z[42]=z[54] - z[81] + z[42];
    z[42]=z[8]*z[42];
    z[38]= - z[38] - z[12];
    z[38]=z[11]*z[38];
    z[38]=z[38] - z[69];
    z[38]=z[38]*z[52];
    z[35]=z[42] + z[36] + z[35] + z[68] + z[38];
    z[35]=z[8]*z[35];
    z[36]= - z[11]*z[16];
    z[36]=z[36] - z[73];
    z[36]=z[36]*z[75];
    z[38]= - z[11] + z[14];
    z[38]=z[38]*z[45];
    z[42]=z[18] - z[7];
    z[52]= - z[42]*z[45];
    z[52]=z[75] + z[52];
    z[52]=z[14]*z[52];
    z[54]=z[10]*z[75];
    z[52]=z[54] + z[52];
    z[52]=z[13]*z[52];
    z[36]=z[52] + z[36] + z[38];
    z[36]=z[13]*z[36];
    z[38]=z[65]*z[32];
    z[52]=z[38] + 3*z[59] + z[82];
    z[52]=z[11]*z[52];
    z[54]=z[32]*z[74];
    z[52]=z[52] + z[54];
    z[43]=z[52]*z[43];
    z[20]=z[25] + z[21] + z[20] + z[33] + z[28] + z[35] + z[36] + z[43];
    z[20]=z[1]*z[20];
    z[21]=z[39]*z[14];
    z[25]=z[27] - 5*z[21];
    z[25]=z[25]*z[19];
    z[25]= - static_cast<T>(3)+ z[25];
    z[25]=z[14]*z[25];
    z[28]=3*z[16];
    z[25]=z[25] - z[28] + z[48];
    z[25]=z[6]*z[25];
    z[33]=z[7] + z[18];
    z[33]=z[33]*z[56]*z[27];
    z[35]=npow(z[18],3);
    z[36]=z[35]*z[14];
    z[36]=z[36] - z[39];
    z[43]=z[14]*z[36];
    z[43]= - z[18] + z[43];
    z[43]=z[43]*z[19];
    z[52]=z[12]*z[27];
    z[43]=z[43] - static_cast<T>(1)+ z[52];
    z[43]=z[6]*z[43];
    z[33]=z[43] - z[11] + z[33];
    z[33]=z[15]*z[33];
    z[43]=z[42]*z[19];
    z[43]=z[43] - 1;
    z[43]=z[43]*z[14];
    z[43]=z[43] + z[85];
    z[43]=z[43]*z[19];
    z[52]= - z[11] - z[91] - z[12];
    z[52]=z[11]*z[52];
    z[28]= - 6*z[14] + z[28] + z[12];
    z[28]=z[13]*z[28];
    z[25]=z[33] + z[25] + z[28] - z[43] - z[81] + z[52];
    z[25]=z[15]*z[25];
    z[28]=static_cast<T>(2)+ z[67];
    z[28]=z[28]*z[19];
    z[33]=z[36]*z[19];
    z[33]=z[18] + z[33];
    z[33]=z[13]*z[14]*z[33];
    z[28]=z[33] + z[28] - z[51] - z[12];
    z[28]=z[13]*z[28];
    z[33]=z[51] - z[48];
    z[33]=z[6]*z[33];
    z[36]=3*z[10];
    z[48]=z[36] + z[12];
    z[48]=z[11]*z[48];
    z[51]= - z[60] + z[13];
    z[51]=z[8]*z[51];
    z[28]=z[51] + z[33] + z[28] + z[81] + z[48];
    z[28]=z[8]*z[28];
    z[33]=z[35]*z[11];
    z[33]=z[33] - z[39];
    z[33]=z[33]*z[15];
    z[48]=2*z[17];
    z[51]= - z[33] + z[40] + z[48] - 5*z[18];
    z[51]=z[11]*z[51];
    z[51]= - z[58] + static_cast<T>(4)+ z[51];
    z[51]=z[9]*z[51];
    z[52]=z[18] - z[40];
    z[52]=z[52]*z[88];
    z[54]= - static_cast<T>(3)+ z[87];
    z[54]=z[11]*z[54];
    z[55]= - z[8]*z[7];
    z[55]= - static_cast<T>(1)+ z[55];
    z[55]=z[8]*z[55];
    z[51]=z[51] + z[52] + z[54] + z[55];
    z[51]=z[51]*z[46];
    z[52]=static_cast<T>(2)- z[87];
    z[32]=z[52]*z[32];
    z[32]=z[32] - z[14];
    z[32]=z[15]*z[32];
    z[52]=z[10] + z[11];
    z[52]=z[11]*z[52];
    z[54]=2*z[8] - z[66] - 6*z[11] + z[14];
    z[54]=z[8]*z[54];
    z[32]=z[51] + z[32] + z[54] + z[52] + 4*z[50];
    z[32]=z[9]*z[32];
    z[50]=static_cast<T>(3)+ z[71];
    z[50]=z[8]*z[50];
    z[36]=z[50] - z[36] - z[72];
    z[36]=z[8]*z[36];
    z[50]=z[65]*z[34];
    z[51]=z[91] + z[72];
    z[51]=z[15]*z[51];
    z[52]=z[8]*npow(z[7],3);
    z[54]=4*z[53] - z[52];
    z[54]=z[8]*z[54];
    z[48]=z[54] - z[48] - z[7];
    z[48]=z[9]*z[48];
    z[54]=static_cast<T>(1)- z[58];
    z[48]=2*z[54] + z[48];
    z[48]=z[48]*z[46];
    z[48]=z[48] - z[10] - 5*z[8];
    z[48]=z[9]*z[48];
    z[54]= - z[5]*z[6];
    z[36]=z[54] + z[48] + z[51] + 3*z[50] + z[36];
    z[36]=z[5]*z[36];
    z[48]=z[30] - z[40];
    z[50]=z[85]*z[35];
    z[51]=z[39] - z[50];
    z[51]=z[13]*z[51];
    z[54]=npow(z[2],3);
    z[55]=z[54]*z[6];
    z[56]=z[2]*z[37];
    z[56]=z[56] - z[55];
    z[26]=z[56]*z[26];
    z[56]=z[18] - z[17];
    z[26]=z[26] + z[51] + z[56] + 2*z[48];
    z[26]=z[26]*z[41];
    z[41]= - 28*z[29] - z[55];
    z[41]=z[6]*z[41];
    z[48]=z[29] - z[55];
    z[48]=z[5]*z[48];
    z[41]=z[48] + z[41] - 16*z[17] + 31*z[2];
    z[41]=z[5]*z[41];
    z[48]=4*z[17] - 23*z[18];
    z[48]=z[11]*z[48];
    z[51]= - 36*z[40] + z[2] + 16*z[18];
    z[51]=z[13]*z[51];
    z[55]=z[30] + 23*z[2] - 4*z[18];
    z[55]=z[6]*z[55];
    z[26]=z[26] + z[41] + z[55] + z[51] + static_cast<T>(11)+ z[48];
    z[26]=z[4]*z[26];
    z[41]=2*z[2];
    z[48]=z[41] - 3*z[30];
    z[48]=z[5]*z[48];
    z[24]= - 4*z[24] + z[48];
    z[24]=z[5]*z[24];
    z[48]=static_cast<T>(3)- 20*z[87];
    z[48]=z[13]*z[48];
    z[24]=z[26] + z[24] + 7*z[6] - z[80] + z[48];
    z[24]=z[4]*z[24];
    z[26]= - z[9] + z[6] - z[14];
    z[26]=z[44]*z[26];
    z[31]=z[31]*z[34];
    z[23]=static_cast<T>(1)- z[23];
    z[23]=z[5]*z[23];
    z[23]=z[23] - 10*z[6] - z[44];
    z[23]=z[5]*z[23];
    z[23]=z[24] + z[23] + z[31] + z[26];
    z[23]=z[4]*z[23];
    z[24]= - z[42]*z[84];
    z[24]=z[18] + z[24];
    z[24]=z[13]*z[24]*z[45];
    z[24]=z[24] + z[38] + z[43];
    z[24]=z[13]*z[24];
    z[26]= - z[6]*z[38];
    z[20]=z[20] + z[23] + z[36] + z[32] + z[25] + z[28] + z[24] + z[26];
    z[23]=npow(z[3],2);
    z[20]=z[1]*z[23]*z[20];
    z[24]=z[18] + z[17];
    z[25]= - z[24]*z[40];
    z[26]=z[2] + z[18];
    z[26]=z[26]*z[39];
    z[28]= - z[11]*npow(z[18],4);
    z[26]=z[26] + z[28];
    z[26]=z[13]*z[26];
    z[28]=z[18]*z[29];
    z[28]= - 2*z[54] + z[28];
    z[28]=z[6]*z[28];
    z[31]= - z[37]*z[29];
    z[32]=z[6]*npow(z[2],4);
    z[31]=z[31] + z[32];
    z[31]=z[5]*z[31];
    z[32]=z[17] + z[2];
    z[32]=z[2]*z[32];
    z[34]=z[2] + z[24];
    z[34]=z[18]*z[34];
    z[25]=z[31] + z[28] + z[26] + z[25] + z[32] + z[34];
    z[25]=z[4]*z[25];
    z[26]= - z[87] + 1;
    z[26]=z[24]*z[26];
    z[28]=z[2] + z[27];
    z[28]=z[18]*z[28];
    z[28]=z[28] - z[50];
    z[28]=z[13]*z[28];
    z[31]=z[18]*z[2];
    z[29]= - 3*z[29] + z[31];
    z[29]=z[6]*z[29];
    z[31]=z[54]*z[49];
    z[32]=z[17] - z[41];
    z[32]=z[2]*z[32];
    z[31]=z[32] + z[31];
    z[31]=z[5]*z[31];
    z[25]=z[25] + z[31] + z[29] + z[28] + z[2] + z[26];
    z[25]=z[4]*z[25];
    z[26]=z[2] + 3*z[18];
    z[26]=3*z[26] - 10*z[40];
    z[26]=z[13]*z[26];
    z[24]= - z[11]*z[24];
    z[28]= - 8*z[2] + z[18];
    z[28]=z[6]*z[28];
    z[29]=10*z[30] + z[17] - 7*z[2];
    z[29]=z[5]*z[29];
    z[24]=6*z[25] + z[29] + z[28] + z[26] + static_cast<T>(1)+ z[24];
    z[24]=z[4]*z[24];
    z[25]=static_cast<T>(3)- 4*z[87];
    z[25]=z[13]*z[25];
    z[22]= - static_cast<T>(1)+ 4*z[22];
    z[22]=z[5]*z[22];
    z[22]=z[24] + z[22] + z[25] - z[6];
    z[22]=z[4]*z[22];
    z[24]=z[56]*z[87];
    z[25]= - z[18]*z[7];
    z[25]= - 2*z[53] + z[25];
    z[25]=z[8]*z[25];
    z[24]= - z[33] + z[25] + z[24] + z[17] - z[42];
    z[24]=z[24]*z[46];
    z[25]=z[27] - 3*z[40];
    z[25]=z[15]*z[25];
    z[26]=z[11]*z[56];
    z[28]=4*z[7];
    z[29]= - z[28] - z[18];
    z[29]=z[8]*z[29];
    z[24]=z[24] + z[25] + z[29] + static_cast<T>(1)+ z[26];
    z[24]=z[9]*z[24];
    z[25]=static_cast<T>(1)- z[89];
    z[25]=z[15]*z[25];
    z[24]=z[24] - z[8] + z[25];
    z[24]=z[24]*z[46];
    z[21]=z[21] - z[18];
    z[21]=z[21]*z[19];
    z[21]=z[21] + 1;
    z[21]=z[21]*z[19];
    z[25]=z[35]*z[19];
    z[25]= - z[25] + 3*z[39];
    z[25]=z[25]*z[14];
    z[25]=z[25] - z[27];
    z[19]=z[25]*z[19];
    z[19]=z[19] + 1;
    z[25]=z[13]*z[19];
    z[25]= - z[21] + z[25];
    z[25]=z[8]*z[25];
    z[19]=z[6]*z[19];
    z[19]=z[19] - z[11] - z[21];
    z[19]=z[15]*z[19];
    z[26]= - z[17] + z[47];
    z[26]=z[7]*z[26];
    z[26]=z[26] - z[52];
    z[26]=z[26]*z[46];
    z[26]=z[26] - 3*z[57] - z[17] + z[28];
    z[26]=z[9]*z[26];
    z[26]=z[26] + static_cast<T>(1)- z[92];
    z[26]=z[26]*z[46];
    z[26]=z[26] + z[6] - z[8];
    z[26]=z[5]*z[26];
    z[27]= - z[11] + z[21];
    z[27]=z[13]*z[27];
    z[21]=z[6]*z[21];
    z[19]=2*z[22] + z[26] + z[24] + z[19] + z[25] + z[27] + z[21];
    z[19]=z[19]*z[23];
    z[19]=z[19] + z[20];

    r += z[19]*npow(z[1],2);
 
    return r;
}

template double qqb_2lNLC_r1067(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1067(const std::array<dd_real,31>&);
#endif
