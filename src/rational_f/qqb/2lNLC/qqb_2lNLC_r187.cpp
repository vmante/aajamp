#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r187(const std::array<T,31>& k) {
  T z[106];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[3];
    z[7]=k[12];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[13];
    z[11]=k[9];
    z[12]=k[2];
    z[13]=k[8];
    z[14]=k[15];
    z[15]=npow(z[11],2);
    z[16]=n<T>(3,2)*z[15];
    z[17]=z[6] - z[12];
    z[18]= - z[11]*z[17];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[18]*z[16];
    z[19]=npow(z[2],2);
    z[20]=4*z[6];
    z[21]= - z[20] + n<T>(3,2)*z[8];
    z[22]= - z[2]*z[21];
    z[22]=n<T>(11,2) + z[22];
    z[22]=z[22]*z[19];
    z[23]=npow(z[11],3);
    z[24]=n<T>(3,2)*z[23];
    z[25]=npow(z[6],2);
    z[26]=z[10]*z[25]*z[24];
    z[27]=n<T>(3,2)*z[2];
    z[28]=npow(z[8],2);
    z[29]=z[2]*z[28];
    z[29]= - z[8] + z[29];
    z[29]=z[2]*z[29];
    z[29]=n<T>(3,2) + z[29];
    z[29]=z[29]*z[27];
    z[30]=4*z[10];
    z[29]=z[29] + z[30];
    z[29]=z[5]*z[29];
    z[31]=z[5] - z[2];
    z[32]=z[11]*z[6];
    z[33]=3*z[32];
    z[34]= - n<T>(77,2) + z[33];
    z[34]=z[11]*z[34];
    z[31]=z[34] - n<T>(9,2)*z[31];
    z[34]=n<T>(1,2)*z[4];
    z[31]=z[31]*z[34];
    z[18]=z[31] + z[29] + z[26] + z[18] + z[22];
    z[18]=z[3]*z[18];
    z[22]=npow(z[13],2);
    z[26]=n<T>(1,2)*z[22];
    z[29]= - z[26] + 7*z[15];
    z[31]=z[2]*z[6];
    z[35]=z[31] + 1;
    z[36]=2*z[19];
    z[37]= - z[35]*z[36];
    z[29]=n<T>(1,2)*z[29] + z[37];
    z[37]=n<T>(1,2)*z[5];
    z[38]=z[5]*z[8];
    z[39]=3*z[38];
    z[40]=n<T>(49,2) + z[39];
    z[40]=z[40]*z[37];
    z[41]=z[5]*z[28];
    z[41]= - n<T>(7,2)*z[8] + z[41];
    z[41]=z[41]*z[37];
    z[41]=z[41] + static_cast<T>(2)+ n<T>(3,4)*z[31];
    z[42]=3*z[4];
    z[41]=z[41]*z[42];
    z[40]=z[41] - 16*z[2] + z[40];
    z[40]=z[4]*z[40];
    z[41]=n<T>(1,4)*z[5];
    z[43]=115*z[2] + 9*z[5];
    z[43]=z[43]*z[41];
    z[29]=z[40] + 3*z[29] + z[43];
    z[29]=z[4]*z[29];
    z[40]=z[2]*z[8];
    z[43]=static_cast<T>(7)+ 3*z[40];
    z[43]=z[43]*z[19];
    z[44]=npow(z[10],2);
    z[43]=7*z[43] - n<T>(9,2)*z[44];
    z[45]=z[10]*z[8];
    z[46]= - static_cast<T>(4)+ n<T>(9,8)*z[45];
    z[46]=z[10]*z[46];
    z[46]= - n<T>(9,4)*z[2] + z[46];
    z[46]=z[5]*z[46];
    z[43]=n<T>(1,4)*z[43] + z[46];
    z[43]=z[5]*z[43];
    z[46]=npow(z[2],3);
    z[47]=n<T>(11,4)*z[44];
    z[47]=z[2]*z[47];
    z[18]=z[18] + z[29] + z[43] + z[47] - z[24] - z[46];
    z[18]=z[3]*z[18];
    z[29]=29*z[11];
    z[43]=25*z[10];
    z[47]= - z[29] - z[43];
    z[47]=z[5]*z[47];
    z[48]=7*z[2] + 3*z[10];
    z[48]=3*z[48] + 11*z[5];
    z[49]= - static_cast<T>(11)- 15*z[31];
    z[49]=z[4]*z[49];
    z[48]=n<T>(1,2)*z[48] + z[49];
    z[48]=z[4]*z[48];
    z[39]=z[45] - z[39];
    z[39]=z[3]*z[39];
    z[49]=3*z[5];
    z[39]= - z[49] + z[39];
    z[39]=z[3]*z[39];
    z[50]=z[10]*z[2];
    z[39]=n<T>(3,4)*z[39] + z[48] - n<T>(9,2)*z[50] + z[47];
    z[39]=z[7]*z[39];
    z[47]=n<T>(1,4)*z[10];
    z[48]= - z[2] - 9*z[10];
    z[48]=z[48]*z[47];
    z[51]=z[31]*z[4];
    z[52]= - 12*z[2] + z[10];
    z[52]=2*z[52] + n<T>(33,4)*z[51];
    z[52]=z[4]*z[52];
    z[48]=z[52] + z[36] + z[48];
    z[48]=z[4]*z[48];
    z[52]=z[36] + n<T>(9,4)*z[50];
    z[52]=z[52]*z[10];
    z[53]=z[5]*z[10];
    z[53]=n<T>(74,3)*z[44] - n<T>(67,8)*z[53];
    z[53]=z[5]*z[53];
    z[54]=4*z[5];
    z[55]= - z[10] + z[5];
    z[55]=z[55]*z[54];
    z[56]= - n<T>(29,8)*z[10] - z[54];
    z[56]=z[3]*z[56];
    z[55]=z[55] + z[56];
    z[55]=z[3]*z[55];
    z[39]=n<T>(1,2)*z[39] + z[55] + z[48] + z[52] + z[53];
    z[39]=z[7]*z[39];
    z[48]=z[5]*z[2];
    z[53]= - 35*z[19] + n<T>(9,2)*z[48];
    z[53]=z[53]*z[37];
    z[55]= - n<T>(7,2) + z[38];
    z[55]=z[5]*z[55];
    z[55]=n<T>(5,2)*z[2] + z[55];
    z[42]=z[55]*z[42];
    z[42]=35*z[48] + z[42];
    z[42]=z[42]*z[34];
    z[42]=z[42] + z[46] + z[53];
    z[42]=z[4]*z[42];
    z[53]=4*z[19];
    z[35]= - z[35]*z[53];
    z[55]=n<T>(3,4)*z[15];
    z[35]= - n<T>(9,4)*z[48] - z[55] + z[35];
    z[35]=z[4]*z[35];
    z[56]= - static_cast<T>(1)+ z[38];
    z[56]=z[46]*z[56];
    z[35]=n<T>(3,2)*z[56] + z[35];
    z[35]=z[3]*z[35];
    z[56]=z[46]*z[5];
    z[35]=z[35] + n<T>(21,4)*z[56] + z[42];
    z[35]=z[3]*z[35];
    z[42]=2*z[50];
    z[57]=n<T>(11,4)*z[2];
    z[57]=z[4]*z[57];
    z[57]=z[42] + z[57];
    z[57]=z[4]*z[57];
    z[51]= - z[2] + z[51];
    z[51]=z[4]*z[51];
    z[51]=9*z[50] + 11*z[51];
    z[51]=z[7]*z[51];
    z[51]=n<T>(1,4)*z[51] - z[52] + z[57];
    z[51]=z[4]*z[51];
    z[52]=npow(z[5],2);
    z[57]=z[52]*z[44];
    z[57]=n<T>(3,8)*z[57];
    z[58]=z[3]*z[5];
    z[58]= - z[52] + z[58];
    z[58]=z[3]*z[10]*z[58];
    z[51]=4*z[58] + z[57] + z[51];
    z[51]=z[7]*z[51];
    z[58]=z[4]*z[5];
    z[59]= - n<T>(7,2)*z[2] + z[5];
    z[59]=z[59]*z[58];
    z[60]=z[52]*z[2];
    z[59]= - z[60] + z[59];
    z[59]=z[4]*z[59];
    z[59]= - n<T>(7,2)*z[56] + z[59];
    z[59]=z[4]*z[59];
    z[61]= - z[5]*z[19];
    z[61]=z[46] + z[61];
    z[61]=z[4]*z[61];
    z[61]=z[56] + z[61];
    z[61]=z[3]*z[61];
    z[62]=z[60]*npow(z[4],3);
    z[63]= - z[3]*z[4]*z[56];
    z[63]=z[62] + z[63];
    z[63]=z[1]*z[63];
    z[59]=z[63] + z[59] + z[61];
    z[59]=3*z[59];
    z[59]=z[3]*z[59];
    z[59]= - 5*z[62] + z[59];
    z[59]=z[1]*z[59];
    z[61]=n<T>(11,4)*z[46];
    z[62]=z[44]*z[61];
    z[63]=4*z[60];
    z[64]=npow(z[4],2);
    z[65]= - z[64]*z[63];
    z[62]=z[62] + z[65];
    z[62]=z[9]*z[62];
    z[65]=n<T>(5,2)*z[5];
    z[66]=9*z[2] - z[65];
    z[58]=z[66]*z[58];
    z[58]=z[63] + z[58];
    z[58]=z[4]*z[58];
    z[58]=z[62] - 6*z[56] + z[58];
    z[58]=z[4]*z[58];
    z[35]=n<T>(1,2)*z[59] + z[51] + z[35] + z[58];
    z[35]=z[1]*z[35];
    z[51]= - 11*z[19] - 3*z[50];
    z[51]=z[51]*z[47];
    z[58]= - 17*z[19] - n<T>(7,2)*z[48];
    z[59]=n<T>(3,4)*z[5];
    z[58]=z[58]*z[59];
    z[46]=z[58] - 2*z[46] + z[51];
    z[51]=2*z[2];
    z[58]=z[51] + z[37];
    z[58]=z[58]*z[49];
    z[62]=6*z[2];
    z[63]=static_cast<T>(9)- n<T>(5,2)*z[38];
    z[63]=z[5]*z[63];
    z[63]= - z[62] + z[63];
    z[63]=z[4]*z[63];
    z[42]=z[63] + z[42] + z[58];
    z[42]=z[4]*z[42];
    z[42]=3*z[46] + z[42];
    z[42]=z[4]*z[42];
    z[46]=npow(z[13],3);
    z[58]=z[10]*z[19];
    z[58]=n<T>(5,2)*z[58] - z[61] + 16*z[46];
    z[58]=z[10]*z[58];
    z[56]=z[58] - n<T>(3,4)*z[56];
    z[58]=3*z[2];
    z[61]=2*z[5];
    z[63]=z[58] - z[61];
    z[63]=z[63]*z[64]*z[61];
    z[56]=3*z[56] + z[63];
    z[56]=z[4]*z[56];
    z[63]=z[15]*z[5];
    z[64]=z[7]*z[63];
    z[57]=z[57] - 29*z[64];
    z[57]=z[7]*z[57];
    z[64]=24*z[46];
    z[66]=z[4]*z[64]*z[44];
    z[67]=n<T>(29,2)*z[23];
    z[68]=z[67]*z[5];
    z[69]= - npow(z[7],2)*z[68];
    z[69]=z[66] + z[69];
    z[69]=z[9]*z[69];
    z[56]=z[69] + z[56] + z[57];
    z[56]=z[9]*z[56];
    z[57]=z[19]*z[44];
    z[69]= - n<T>(1,4)*z[22] + z[36];
    z[69]=z[2]*z[69];
    z[70]=z[5]*z[44];
    z[69]=z[69] + n<T>(1,8)*z[70];
    z[69]=z[69]*z[49];
    z[18]=z[35] + z[56] + z[39] + z[18] + z[42] - n<T>(11,2)*z[57] + z[69];
    z[18]=z[1]*z[18];
    z[35]=n<T>(1,2)*z[11];
    z[39]=z[25]*z[11];
    z[42]=77*z[6] - 3*z[39];
    z[42]=z[42]*z[35];
    z[42]= - n<T>(189,4)*z[38] - n<T>(81,4)*z[31] + static_cast<T>(67)+ z[42];
    z[42]=z[42]*z[34];
    z[56]=n<T>(9,2)*z[17];
    z[57]=z[32]*z[56];
    z[69]=7*z[6];
    z[57]=z[57] + n<T>(17,2)*z[12] - z[69];
    z[57]=z[11]*z[57];
    z[57]=static_cast<T>(20)+ z[57];
    z[57]=z[11]*z[57];
    z[21]= - z[21]*z[40];
    z[21]=z[21] - z[20] + 7*z[8];
    z[21]=z[2]*z[21];
    z[21]= - n<T>(31,4) + z[21];
    z[21]=z[2]*z[21];
    z[70]=npow(z[6],3);
    z[71]=z[70]*z[11];
    z[72]=11*z[25] - 9*z[71];
    z[72]=z[72]*z[15];
    z[72]=n<T>(3,4) + z[72];
    z[73]=n<T>(1,2)*z[10];
    z[72]=z[72]*z[73];
    z[74]=z[10]*z[38];
    z[21]=z[42] - 18*z[74] + z[72] + z[57] + z[21];
    z[21]=z[3]*z[21];
    z[42]=n<T>(11,2)*z[2];
    z[57]= - n<T>(3,2) + z[40];
    z[57]=z[57]*z[42];
    z[72]=n<T>(29,2)*z[71];
    z[74]=26*z[25] + z[72];
    z[74]=z[11]*z[74];
    z[74]= - n<T>(25,2)*z[6] + z[74];
    z[74]=z[11]*z[74];
    z[74]= - n<T>(11,4)*z[40] + n<T>(147,8) + z[74];
    z[74]=z[10]*z[74];
    z[75]=12*z[13];
    z[76]=n<T>(205,6)*z[6] - 39*z[39];
    z[76]=z[11]*z[76];
    z[76]= - n<T>(307,12) + z[76];
    z[76]=z[11]*z[76];
    z[57]=z[74] + z[57] - z[75] + z[76];
    z[57]=z[10]*z[57];
    z[74]=n<T>(1,4)*z[13];
    z[76]=z[13]*z[8];
    z[77]=z[76] + 1;
    z[78]=z[77]*z[74];
    z[79]= - static_cast<T>(1)- n<T>(1,2)*z[32];
    z[79]=z[11]*z[79];
    z[78]=z[78] + 7*z[79];
    z[79]=static_cast<T>(5)+ 6*z[31];
    z[79]=z[79]*z[51];
    z[80]= - static_cast<T>(139)- 89*z[31];
    z[80]=n<T>(1,2)*z[80] + 25*z[38];
    z[34]=z[80]*z[34];
    z[34]=z[34] + n<T>(311,8)*z[5] + 3*z[78] + z[79];
    z[34]=z[4]*z[34];
    z[78]=7*z[45];
    z[79]= - static_cast<T>(267)+ z[78];
    z[79]=z[79]*z[47];
    z[80]= - 63*z[40] + 31*z[45];
    z[59]=z[80]*z[59];
    z[80]= - n<T>(311,4) + 25*z[40];
    z[80]=z[2]*z[80];
    z[59]=z[59] + z[80] + z[79];
    z[59]=z[59]*z[37];
    z[79]= - 25*z[13] + 3*z[14];
    z[80]=n<T>(3,2)*z[13];
    z[81]= - z[79]*z[80];
    z[82]=npow(z[14],2);
    z[83]=z[82]*z[8];
    z[84]=n<T>(115,2)*z[14] + 3*z[83];
    z[85]=6*z[6] - z[8];
    z[85]=z[2]*z[85];
    z[85]=static_cast<T>(7)+ z[85];
    z[85]=z[2]*z[85];
    z[84]=n<T>(1,2)*z[84] + z[85];
    z[84]=z[2]*z[84];
    z[85]=29*z[32];
    z[86]= - n<T>(233,3) + z[85];
    z[86]=z[11]*z[86];
    z[86]= - n<T>(109,2)*z[13] + z[86];
    z[86]=z[11]*z[86];
    z[21]=z[21] + z[34] + z[59] + z[57] + z[84] + z[81] + z[86];
    z[21]=z[3]*z[21];
    z[34]= - static_cast<T>(99)- 73*z[31];
    z[34]=n<T>(11,2)*z[38] + n<T>(1,4)*z[34] + 2*z[45];
    z[34]=z[4]*z[34];
    z[57]=n<T>(135,2) + 37*z[32];
    z[57]=z[57]*z[35];
    z[59]=5*z[2];
    z[81]=n<T>(1,2)*z[31];
    z[84]=static_cast<T>(10)- z[81];
    z[84]=z[84]*z[59];
    z[86]=n<T>(1967,12) + 29*z[45];
    z[86]=z[86]*z[73];
    z[34]=z[34] + n<T>(33,4)*z[5] + z[86] + z[57] + z[84];
    z[34]=z[4]*z[34];
    z[57]= - static_cast<T>(227)+ 147*z[76];
    z[84]=z[13]*z[57];
    z[27]=z[84] - z[27];
    z[84]= - n<T>(703,4) + 25*z[45];
    z[84]=z[10]*z[84];
    z[27]=n<T>(1,2)*z[27] + z[84];
    z[84]=n<T>(89,8) - 6*z[38];
    z[49]=z[84]*z[49];
    z[78]= - static_cast<T>(3)+ z[78];
    z[78]=5*z[78] + 93*z[38];
    z[78]=z[3]*z[78];
    z[27]=n<T>(1,8)*z[78] + n<T>(1,2)*z[27] + z[49];
    z[27]=z[3]*z[27];
    z[49]=45*z[6] + 29*z[39];
    z[49]=z[11]*z[49];
    z[49]= - 11*z[38] - n<T>(679,6)*z[45] + n<T>(115,2)*z[31] + n<T>(907,6) + z[49];
    z[49]=z[4]*z[49];
    z[78]=3*z[8];
    z[84]=z[28]*z[13];
    z[86]=z[78] - z[84];
    z[87]=z[13]*z[86];
    z[87]=n<T>(7,8) - 4*z[87];
    z[87]=n<T>(7,8)*z[38] + 3*z[87] - n<T>(601,24)*z[45];
    z[87]=z[3]*z[87];
    z[88]=24*z[13];
    z[89]=z[8] - z[84];
    z[89]=z[89]*z[88];
    z[89]= - n<T>(29,2) + z[89];
    z[89]=z[13]*z[89];
    z[90]=n<T>(29,2)*z[11];
    z[49]=z[87] + n<T>(1,4)*z[49] - n<T>(123,8)*z[5] + 27*z[10] - n<T>(29,8)*z[2] + 
    z[89] + z[90];
    z[49]=z[7]*z[49];
    z[87]=16*z[22];
    z[89]=3*z[76];
    z[91]= - n<T>(23,3) - z[89];
    z[91]=z[91]*z[87];
    z[92]=n<T>(29,4)*z[11];
    z[93]= - z[51] + n<T>(52,3)*z[13] - z[92];
    z[93]=z[2]*z[93];
    z[94]= - n<T>(2267,8)*z[2] - 160*z[10];
    z[94]=z[10]*z[94];
    z[95]= - n<T>(57,2)*z[5] + n<T>(343,8)*z[10] - z[75] - n<T>(2237,12)*z[11];
    z[95]=z[5]*z[95];
    z[27]=z[49] + z[27] + z[34] + z[95] + n<T>(1,3)*z[94] + z[91] + z[93];
    z[27]=z[7]*z[27];
    z[34]=z[67] - z[64];
    z[49]=24*z[22];
    z[67]=z[49]*z[2];
    z[91]= - 113*z[63] + z[67] + z[34];
    z[91]=z[7]*z[91];
    z[93]=11*z[63];
    z[94]= - 87*z[23] - z[93];
    z[94]=z[94]*z[37];
    z[91]=z[94] + z[91];
    z[91]=z[7]*z[91];
    z[52]=z[52]*z[23];
    z[94]=n<T>(1,2)*z[52];
    z[95]=z[46]*z[2];
    z[96]=8*z[95] - z[68];
    z[96]=z[7]*z[96];
    z[96]=z[94] + z[96];
    z[96]=z[7]*z[96];
    z[96]=z[66] + z[96];
    z[97]=3*z[9];
    z[96]=z[96]*z[97];
    z[98]=z[15]*z[2];
    z[99]= - z[64] - n<T>(29,4)*z[98];
    z[99]=z[99]*z[44];
    z[55]=z[60]*z[55];
    z[60]=z[22]*z[10];
    z[100]=2*z[60];
    z[101]=3*z[46];
    z[102]=z[101] - z[100];
    z[103]=z[4]*z[10];
    z[102]=z[102]*z[103];
    z[55]=z[96] + z[91] + 48*z[102] + z[99] + z[55];
    z[55]=z[9]*z[55];
    z[91]=3*z[13];
    z[59]=z[91] - z[59];
    z[59]=z[59]*z[73];
    z[58]=z[58] - z[5];
    z[54]=z[58]*z[54];
    z[58]=static_cast<T>(3)- 2*z[38];
    z[58]=z[4]*z[58]*z[61];
    z[54]=z[58] + z[59] + z[54];
    z[54]=z[4]*z[54];
    z[58]=n<T>(115,8)*z[50] - n<T>(64,3)*z[22] - n<T>(73,4)*z[19];
    z[58]=z[10]*z[58];
    z[59]=3*z[82];
    z[96]= - n<T>(81,4)*z[48] - z[59] - n<T>(89,2)*z[19];
    z[96]=z[96]*z[37];
    z[99]=48*z[46];
    z[54]=z[54] + z[96] + z[99] + z[58];
    z[54]=z[4]*z[54];
    z[58]=z[2]*z[13];
    z[96]=z[58] - z[50];
    z[102]= - static_cast<T>(1)+ z[76];
    z[102]=z[102]*z[49];
    z[43]= - 191*z[11] - z[43];
    z[43]=z[43]*z[37];
    z[104]=29*z[15];
    z[43]=z[43] + z[102] + z[104] + n<T>(29,2)*z[96];
    z[43]=z[7]*z[43];
    z[96]=z[2]*z[22];
    z[96]=z[101] + n<T>(55,3)*z[96];
    z[36]= - z[36] + n<T>(679,24)*z[50];
    z[36]=z[10]*z[36];
    z[102]= - 1399*z[15] + n<T>(601,4)*z[44];
    z[105]= - 51*z[11] - n<T>(35,4)*z[10];
    z[105]=z[5]*z[105];
    z[102]=n<T>(1,3)*z[102] + z[105];
    z[102]=z[102]*z[37];
    z[36]=z[43] + z[102] + 8*z[96] + z[36];
    z[36]=z[7]*z[36];
    z[43]= - 12*z[46] + z[98];
    z[96]=z[2]*z[11];
    z[102]=z[104] - 45*z[96];
    z[47]=z[102]*z[47];
    z[43]=4*z[43] + z[47];
    z[43]=z[10]*z[43];
    z[47]=z[11]*z[48];
    z[47]= - 12*z[98] - n<T>(77,4)*z[47];
    z[47]=z[5]*z[47];
    z[48]=z[64]*z[2];
    z[36]=z[55] + z[36] + z[54] + z[47] - z[48] + z[43];
    z[36]=z[9]*z[36];
    z[43]= - z[28]*z[65];
    z[43]=9*z[8] + z[43];
    z[43]=z[5]*z[43];
    z[43]= - static_cast<T>(12)+ z[43];
    z[43]=z[4]*z[43];
    z[47]=z[91] - 29*z[2];
    z[54]=static_cast<T>(15)- z[38];
    z[54]=z[5]*z[54];
    z[43]=z[43] + z[54] + n<T>(1,2)*z[47] + 2*z[10];
    z[43]=z[4]*z[43];
    z[47]=n<T>(1463,12) + 48*z[76];
    z[47]=z[47]*z[22];
    z[54]= - 45*z[13] + 19*z[14];
    z[54]=2*z[54];
    z[29]= - z[54] + z[29];
    z[29]=z[11]*z[29];
    z[55]=3*z[11];
    z[98]= - 27*z[5] - n<T>(9,2)*z[10] - n<T>(63,2)*z[2] + 109*z[14] + z[55];
    z[41]=z[98]*z[41];
    z[98]= - n<T>(9,4)*z[10] + 8*z[13] - n<T>(139,8)*z[2];
    z[98]=z[10]*z[98];
    z[29]=z[43] + z[41] + z[98] + z[53] + z[47] + z[29];
    z[29]=z[4]*z[29];
    z[41]=z[79]*z[91];
    z[43]=z[77]*z[80];
    z[43]=n<T>(139,2)*z[2] + z[43] - 43*z[11];
    z[43]=z[2]*z[43];
    z[47]=z[11]*z[13];
    z[41]=z[43] + z[41] + 109*z[47];
    z[43]=z[57]*z[74];
    z[47]=z[86]*z[75];
    z[47]=n<T>(269,12) + z[47];
    z[47]=z[10]*z[47];
    z[43]=z[43] + z[47];
    z[43]=z[10]*z[43];
    z[47]= - 67*z[2] - 5*z[10];
    z[47]=z[47]*z[37];
    z[41]=z[47] + n<T>(1,2)*z[41] + z[43];
    z[41]=z[5]*z[41];
    z[26]=z[82] + z[26];
    z[43]=z[54] - n<T>(13,2)*z[11];
    z[43]=z[11]*z[43];
    z[26]=6*z[19] + n<T>(3,2)*z[26] + z[43];
    z[26]=z[2]*z[26];
    z[43]=static_cast<T>(45)+ z[85];
    z[43]=z[43]*z[35];
    z[43]=z[43] - 19*z[2];
    z[43]=z[10]*z[43];
    z[47]=45*z[11] + n<T>(91,2)*z[2];
    z[47]=z[2]*z[47];
    z[43]=z[43] - n<T>(45,2)*z[15] + z[47];
    z[43]=z[43]*z[73];
    z[18]=z[18] + z[36] + z[27] + z[21] + z[29] + z[41] + z[26] + z[43];
    z[18]=z[1]*z[18];
    z[21]=z[28]*z[22];
    z[26]=static_cast<T>(19)- 48*z[21];
    z[26]=z[13]*z[26];
    z[27]=static_cast<T>(81)+ z[85];
    z[27]=z[11]*z[27];
    z[26]= - 26*z[5] + z[42] + z[26] + z[27];
    z[26]=z[7]*z[26];
    z[27]=z[89] + n<T>(32,3);
    z[27]=z[27]*z[87];
    z[29]= - 809*z[11] - n<T>(71,2)*z[10];
    z[29]=n<T>(1,3)*z[29] - 56*z[5];
    z[29]=z[5]*z[29];
    z[26]=z[26] + z[29] - n<T>(113,6)*z[50] - 214*z[58] - z[27] + n<T>(1399,6)*
    z[15];
    z[26]=z[7]*z[26];
    z[29]=z[44]*z[46];
    z[36]=z[101] - 4*z[60];
    z[41]=16*z[103];
    z[36]=z[36]*z[41];
    z[36]=z[36] - 16*z[29] + z[52];
    z[42]= - 139*z[63] - z[99] + 29*z[23];
    z[42]=z[7]*z[42];
    z[43]= - 90*z[23] - z[93];
    z[43]=z[5]*z[43];
    z[44]=48*z[95];
    z[42]=z[42] + z[44] + z[43];
    z[42]=z[7]*z[42];
    z[43]=16*z[95] - z[68];
    z[43]=z[7]*z[43];
    z[43]=z[52] + z[43];
    z[43]=z[7]*z[43];
    z[43]=z[66] + z[43];
    z[43]=z[43]*z[97];
    z[36]=z[43] + 3*z[36] + z[42];
    z[36]=z[9]*z[36];
    z[42]=z[10]*z[13];
    z[43]=n<T>(269,2)*z[42];
    z[47]= - n<T>(568,3)*z[22] + z[43];
    z[47]=z[10]*z[47];
    z[53]=z[82]*z[65];
    z[54]=z[91]*z[103];
    z[47]=z[54] + z[53] + 72*z[46] + z[47];
    z[47]=z[4]*z[47];
    z[53]=n<T>(29,2)*z[32];
    z[54]=static_cast<T>(84)+ z[53];
    z[54]=z[54]*z[15];
    z[57]=z[5]*z[11];
    z[65]=z[8]*z[99];
    z[54]= - 135*z[57] - 19*z[58] + z[65] + z[54];
    z[54]=z[7]*z[54];
    z[65]=n<T>(512,3)*z[22] + n<T>(3,2)*z[58];
    z[65]=z[2]*z[65];
    z[73]= - n<T>(2527,6)*z[15] - 51*z[57];
    z[73]=z[5]*z[73];
    z[54]=z[54] + z[73] + n<T>(87,2)*z[23] + z[65];
    z[54]=z[7]*z[54];
    z[65]=z[76] - 3;
    z[60]=z[65]*z[60];
    z[65]= - z[101] - z[60];
    z[65]=z[10]*z[65];
    z[73]= - 67*z[23] - n<T>(53,2)*z[63];
    z[73]=z[73]*z[37];
    z[36]=z[36] + z[54] + z[47] + 24*z[65] + z[73];
    z[36]=z[9]*z[36];
    z[47]=z[10]*z[92];
    z[47]= - 57*z[57] + z[47] - n<T>(79,4)*z[96] + n<T>(3,2)*z[82] - n<T>(563,3)*
    z[15];
    z[47]=z[5]*z[47];
    z[54]= - z[28]*z[61];
    z[54]=z[78] + z[54];
    z[54]=z[4]*z[54];
    z[54]=z[54] + static_cast<T>(9)- 4*z[38];
    z[54]=z[61]*z[54];
    z[54]=z[91] - z[30] + z[54];
    z[54]=z[4]*z[54];
    z[65]=z[89] + n<T>(5,3);
    z[65]=z[65]*z[87];
    z[73]=n<T>(17,2)*z[10] - n<T>(122,3)*z[13] - 23*z[2];
    z[73]=z[10]*z[73];
    z[74]=13*z[11];
    z[78]=45*z[14] - z[74];
    z[78]=n<T>(23,4)*z[10] + n<T>(1,2)*z[78] - 8*z[2] - n<T>(29,2)*z[5];
    z[78]=z[5]*z[78];
    z[54]=z[54] + z[78] + z[73] - n<T>(5,2)*z[82] + z[65];
    z[54]=z[4]*z[54];
    z[50]= - 4*z[50] - z[65] - n<T>(209,4)*z[96];
    z[50]=z[10]*z[50];
    z[27]=z[2]*z[27];
    z[24]=z[36] + z[26] + z[54] + z[47] + z[50] + z[24] + z[27];
    z[24]=z[9]*z[24];
    z[26]=npow(z[6],4);
    z[27]=z[26]*z[11];
    z[36]=29*z[27];
    z[47]= - 81*z[70] - z[36];
    z[47]=z[11]*z[47];
    z[47]= - 27*z[25] + z[47];
    z[47]=z[11]*z[47];
    z[50]=4*z[8];
    z[47]=z[47] - z[69] - z[50];
    z[47]=z[10]*z[47];
    z[54]=n<T>(1055,6)*z[25] + 78*z[71];
    z[54]=z[11]*z[54];
    z[54]= - z[6] + z[54];
    z[54]=z[11]*z[54];
    z[65]=12*z[76];
    z[47]=z[47] + 13*z[40] + z[54] - n<T>(469,12) + z[65];
    z[47]=z[10]*z[47];
    z[54]= - z[39]*z[56];
    z[56]=17*z[12];
    z[73]= - z[56] + n<T>(47,4)*z[6];
    z[73]=z[6]*z[73];
    z[54]=z[73] + z[54];
    z[54]=z[11]*z[54];
    z[73]=10*z[12];
    z[54]=z[54] - z[73] + z[69];
    z[54]=z[11]*z[54];
    z[69]=z[28]*z[31];
    z[78]=2*z[8];
    z[79]= - z[6]*z[78];
    z[69]=z[79] + z[69];
    z[69]=z[2]*z[69];
    z[69]=n<T>(29,2)*z[6] + 4*z[69];
    z[69]=z[2]*z[69];
    z[79]= - 11*z[70] + n<T>(9,2)*z[27];
    z[79]=z[11]*z[79];
    z[79]= - n<T>(51,2)*z[25] + z[79];
    z[79]=z[11]*z[79];
    z[86]=z[6] + z[78];
    z[79]=9*z[86] + z[79];
    z[79]=z[10]*z[79];
    z[54]=z[79] + z[69] - static_cast<T>(20)+ z[54];
    z[54]=z[3]*z[54];
    z[69]=z[89] - n<T>(85,2);
    z[79]=3*z[69] - 79*z[32];
    z[79]=n<T>(67,4)*z[38] + n<T>(1,4)*z[79] + 8*z[31];
    z[79]=z[4]*z[79];
    z[86]=17*z[14] + z[83];
    z[87]= - n<T>(353,3)*z[6] - 107*z[39];
    z[87]=z[11]*z[87];
    z[87]=n<T>(67,3) + z[87];
    z[87]=z[11]*z[87];
    z[86]=z[87] + 3*z[86] + 5*z[13];
    z[87]=3*z[6] - z[78];
    z[87]=z[87]*z[40];
    z[87]=z[87] - 9*z[6] + z[78];
    z[51]=z[87]*z[51];
    z[87]= - 7*z[14] + z[83];
    z[87]=z[8]*z[87];
    z[51]=z[51] - static_cast<T>(47)+ n<T>(5,2)*z[87];
    z[51]=z[2]*z[51];
    z[87]=n<T>(19,4)*z[45] + static_cast<T>(20)- n<T>(67,4)*z[40];
    z[87]=z[5]*z[87];
    z[47]=z[54] + z[79] + z[87] + z[47] + n<T>(1,2)*z[86] + z[51];
    z[47]=z[3]*z[47];
    z[51]=z[78] - z[84];
    z[54]= - z[51]*z[75];
    z[75]=n<T>(29,2)*z[39];
    z[79]= - 37*z[6] - z[75];
    z[79]=z[79]*z[35];
    z[86]=z[4]*z[20];
    z[87]= - z[3]*z[50];
    z[54]=z[87] + z[86] + z[38] - n<T>(17,2)*z[31] + z[79] - n<T>(1321,12) + 
    z[54];
    z[54]=z[7]*z[54];
    z[79]= - n<T>(119,6) - 209*z[32];
    z[79]= - 13*z[38] - n<T>(113,6)*z[45] + n<T>(1,4)*z[79] + 23*z[31];
    z[79]=z[4]*z[79];
    z[86]=23*z[76];
    z[33]=3*z[31] + z[33] + n<T>(553,6) + z[86];
    z[33]= - n<T>(19,2)*z[38] + n<T>(1,2)*z[33] - n<T>(71,3)*z[45];
    z[33]=z[3]*z[33];
    z[87]=5*z[31];
    z[89]=z[87] + n<T>(297,2) + 13*z[32];
    z[89]=z[2]*z[89];
    z[33]=z[89] + z[33];
    z[89]=8*z[84];
    z[92]=n<T>(81,4)*z[8] + z[89];
    z[92]=z[92]*z[91];
    z[92]=n<T>(731,4) + z[92];
    z[92]=z[13]*z[92];
    z[65]= - 23*z[38] + n<T>(55,12) + z[65];
    z[65]=z[5]*z[65];
    z[96]=n<T>(706,3) + 9*z[32];
    z[96]=z[11]*z[96];
    z[33]=z[54] + z[79] + z[65] + n<T>(40,3)*z[10] + z[92] + z[96] + n<T>(1,2)*
    z[33];
    z[33]=z[7]*z[33];
    z[54]=static_cast<T>(1)- z[40];
    z[54]=z[54]*z[62];
    z[62]=8*z[14];
    z[65]= - n<T>(100,3) - n<T>(99,4)*z[76];
    z[65]=z[13]*z[65];
    z[54]=z[54] + n<T>(97,2)*z[11] + z[65] - z[62] + n<T>(5,2)*z[83];
    z[54]=z[2]*z[54];
    z[65]= - n<T>(63,4)*z[8] - z[89];
    z[65]=z[65]*z[91];
    z[65]= - static_cast<T>(155)+ z[65];
    z[65]=z[13]*z[65];
    z[79]=z[45] + z[38];
    z[89]= - static_cast<T>(8)+ z[79];
    z[92]=2*z[4];
    z[89]=z[89]*z[92];
    z[96]=n<T>(142,3) + n<T>(29,2)*z[45];
    z[96]=z[10]*z[96];
    z[97]=5*z[38];
    z[98]= - n<T>(57,2) - z[97];
    z[98]=z[5]*z[98];
    z[65]=z[89] + z[98] + z[96] + 31*z[2] - n<T>(149,2)*z[11] - 12*z[14] + 
    z[65];
    z[65]=z[4]*z[65];
    z[89]=n<T>(295,4)*z[25] + 29*z[71];
    z[89]=z[11]*z[89];
    z[89]=n<T>(17,2)*z[6] + z[89];
    z[89]=z[11]*z[89];
    z[21]=z[89] - n<T>(1375,12) + 12*z[21];
    z[21]=z[10]*z[21];
    z[89]=n<T>(67,3) + 45*z[76];
    z[89]=z[13]*z[89];
    z[96]=n<T>(151,3)*z[6] - 81*z[39];
    z[96]=z[11]*z[96];
    z[96]= - static_cast<T>(59)+ z[96];
    z[96]=z[11]*z[96];
    z[89]= - n<T>(667,12)*z[2] + n<T>(1,2)*z[89] + z[96];
    z[21]=n<T>(1,2)*z[89] + z[21];
    z[21]=z[10]*z[21];
    z[89]=3*z[12];
    z[96]=55*z[6];
    z[98]=z[89] + z[96];
    z[99]= - z[11]*z[98];
    z[99]=n<T>(857,6) + z[99];
    z[99]=z[99]*z[15];
    z[59]= - z[59] + z[99];
    z[86]=n<T>(365,6) + z[86];
    z[86]=n<T>(1,4)*z[86] + 4*z[45];
    z[86]=z[10]*z[86];
    z[69]=z[2]*z[69];
    z[69]= - 40*z[5] + z[86] + n<T>(3,4)*z[69] + z[11] - 24*z[14] - n<T>(5,2)*
    z[13];
    z[69]=z[5]*z[69];
    z[18]=z[18] + z[24] + z[33] + z[47] + z[65] + z[69] + z[21] + n<T>(1,2)*
    z[59] + z[54];
    z[18]=z[1]*z[18];
    z[21]=z[46] - z[100];
    z[21]=z[21]*z[41];
    z[21]=z[21] - 8*z[29] + z[94];
    z[24]= - 55*z[63] - z[67] + z[34];
    z[24]=z[7]*z[24];
    z[29]= - 93*z[23] - z[93];
    z[29]=z[29]*z[37];
    z[24]=z[24] + z[44] + z[29];
    z[24]=z[7]*z[24];
    z[29]=z[48] - z[68];
    z[29]=z[7]*z[29];
    z[29]=n<T>(3,2)*z[52] + z[29];
    z[29]=z[7]*z[29];
    z[29]=z[66] + z[29];
    z[29]=z[9]*z[29];
    z[21]=z[29] + 3*z[21] + z[24];
    z[21]=z[9]*z[21];
    z[24]= - 168*z[22] + z[43];
    z[24]=z[10]*z[24];
    z[29]=z[103]*z[80];
    z[33]=4*z[82];
    z[34]=z[5]*z[33];
    z[24]=z[29] + z[34] + z[64] + z[24];
    z[24]=z[4]*z[24];
    z[29]=z[77]*z[49];
    z[34]=static_cast<T>(55)+ z[53];
    z[34]=z[34]*z[15];
    z[29]= - 54*z[57] - n<T>(19,2)*z[58] + z[29] + z[34];
    z[29]=z[7]*z[29];
    z[34]= - 8*z[22] + n<T>(1,2)*z[58];
    z[34]=z[2]*z[34];
    z[34]=z[34] + 15*z[23] - 8*z[46];
    z[41]= - 188*z[15] - n<T>(51,2)*z[57];
    z[41]=z[5]*z[41];
    z[29]=z[29] + 3*z[34] + z[41];
    z[29]=z[7]*z[29];
    z[34]= - z[46] - z[60];
    z[34]=z[10]*z[34];
    z[34]=z[95] + z[34];
    z[41]=z[11]*z[12];
    z[43]= - static_cast<T>(7)+ n<T>(3,2)*z[41];
    z[43]=z[43]*z[63];
    z[43]= - 32*z[23] + z[43];
    z[43]=z[5]*z[43];
    z[21]=z[21] + z[29] + z[24] + 24*z[34] + z[43];
    z[21]=z[9]*z[21];
    z[24]= - z[82] - 18*z[22];
    z[29]=n<T>(346,3)*z[13] - n<T>(125,2)*z[10];
    z[29]=z[10]*z[29];
    z[34]=z[5]*z[62];
    z[43]=z[13] - z[10];
    z[43]=z[4]*z[43];
    z[24]=n<T>(3,2)*z[43] + z[34] + 4*z[24] + z[29];
    z[24]=z[4]*z[24];
    z[29]=z[84] + z[8];
    z[29]=z[29]*z[88];
    z[34]=n<T>(19,2) - z[29];
    z[34]=z[13]*z[34];
    z[43]=z[96] + z[75];
    z[43]=z[11]*z[43];
    z[43]=static_cast<T>(54)+ z[43];
    z[43]=z[11]*z[43];
    z[34]= - n<T>(33,2)*z[5] + n<T>(13,2)*z[2] + z[34] + z[43];
    z[34]=z[7]*z[34];
    z[43]=static_cast<T>(129)+ z[85];
    z[43]=z[43]*z[16];
    z[44]= - n<T>(653,3)*z[11] - 47*z[5];
    z[44]=z[44]*z[37];
    z[46]= - n<T>(302,3)*z[13] - z[2];
    z[46]=z[2]*z[46];
    z[34]=z[34] + z[44] + z[43] + z[46];
    z[34]=z[7]*z[34];
    z[43]= - static_cast<T>(77)- 3*z[41];
    z[16]=z[43]*z[16];
    z[43]=17*z[41];
    z[44]= - static_cast<T>(37)- z[43];
    z[37]=z[11]*z[44]*z[37];
    z[16]=z[37] - z[33] + z[16];
    z[16]=z[5]*z[16];
    z[19]=z[19]*z[91];
    z[19]=61*z[23] + z[19];
    z[23]=z[51]*z[88];
    z[23]= - n<T>(125,2) + z[23];
    z[23]=z[23]*z[42];
    z[22]=72*z[22] + z[23];
    z[22]=z[10]*z[22];
    z[16]=z[21] + z[34] + z[24] + z[16] + n<T>(1,2)*z[19] + z[22];
    z[16]=z[9]*z[16];
    z[19]=z[29] + n<T>(53,3);
    z[19]=z[19]*z[13];
    z[21]= - z[79]*z[92];
    z[22]= - z[14] + z[83];
    z[23]= - n<T>(7,3) - z[45];
    z[23]=z[10]*z[23];
    z[24]= - static_cast<T>(12)- z[97];
    z[24]=z[5]*z[24];
    z[21]=z[21] + z[24] + n<T>(23,2)*z[23] + 4*z[22] - z[19];
    z[21]=z[4]*z[21];
    z[22]=z[29] + n<T>(269,3);
    z[22]=z[22]*z[13];
    z[23]= - z[38] + static_cast<T>(2)+ z[31];
    z[23]=z[7]*z[23];
    z[24]=199*z[6] + 42*z[39];
    z[24]=z[11]*z[24];
    z[24]=n<T>(403,3) + z[24];
    z[24]=z[11]*z[24];
    z[29]=n<T>(337,3) - z[81];
    z[29]=z[2]*z[29];
    z[34]=n<T>(137,6) - z[97];
    z[34]=z[5]*z[34];
    z[23]=3*z[23] + z[34] + z[29] + z[22] + z[24];
    z[23]=z[7]*z[23];
    z[24]=55*z[25] + z[72];
    z[24]=z[11]*z[24];
    z[24]=54*z[6] + z[24];
    z[24]=z[11]*z[24];
    z[29]= - z[8] + 2*z[84];
    z[29]=z[13]*z[29];
    z[29]=static_cast<T>(1)+ 6*z[29];
    z[24]=4*z[29] + z[24];
    z[24]=z[10]*z[24];
    z[19]=z[19] + z[24];
    z[19]=z[10]*z[19];
    z[24]= - n<T>(47,6) + z[43];
    z[24]=z[11]*z[24];
    z[29]= - static_cast<T>(1)- z[41];
    z[29]=z[5]*z[29];
    z[24]=10*z[29] + z[30] - 4*z[14] + z[24];
    z[24]=z[5]*z[24];
    z[29]=z[89] + 29*z[6];
    z[29]=z[11]*z[29];
    z[29]=n<T>(245,2) + z[29];
    z[15]=z[29]*z[15];
    z[22]= - z[22] - z[2];
    z[22]=z[2]*z[22];
    z[15]=z[16] + z[23] + z[21] + z[24] + z[19] + z[22] + z[33] + z[15];
    z[15]=z[9]*z[15];
    z[16]= - z[4]*z[50];
    z[19]=z[3]*z[20];
    z[20]= - z[6] - z[8];
    z[20]=z[7]*z[20];
    z[16]=8*z[20] + z[19] + z[16] + 32*z[38] - 41*z[31] + 28*z[32] - 203.
   /3. + 2*z[76];
    z[16]=z[7]*z[16];
    z[19]=z[56] - 11*z[6];
    z[19]=z[19]*z[25];
    z[17]=z[17]*z[71];
    z[17]=z[19] + 3*z[17];
    z[17]=z[17]*z[35];
    z[19]=z[73] - 27*z[6];
    z[19]=z[6]*z[19];
    z[17]=z[19] + z[17];
    z[17]=z[11]*z[17];
    z[19]=npow(z[6],5);
    z[20]= - z[19]*z[55];
    z[20]=11*z[26] + z[20];
    z[20]=z[11]*z[20];
    z[20]=51*z[70] + z[20];
    z[20]=z[20]*z[35];
    z[21]=z[8]*z[6];
    z[20]=z[20] + n<T>(47,2)*z[25] + 5*z[21];
    z[20]=z[10]*z[20];
    z[22]=z[8]*z[87];
    z[17]=z[20] + z[22] - 20*z[6] + z[17];
    z[17]=z[3]*z[17];
    z[20]=z[83] + z[14];
    z[22]= - z[20]*z[50];
    z[22]=static_cast<T>(5)+ z[22];
    z[22]=z[8]*z[22];
    z[23]=z[31]*z[78];
    z[22]=z[23] - 16*z[6] + z[22];
    z[22]=z[2]*z[22];
    z[21]=n<T>(11,2)*z[25] + z[21];
    z[19]=z[19]*z[90];
    z[19]=55*z[26] + z[19];
    z[19]=z[11]*z[19];
    z[19]=54*z[70] + z[19];
    z[19]=z[11]*z[19];
    z[19]=3*z[21] + z[19];
    z[19]=z[10]*z[19];
    z[21]= - 70*z[70] - 13*z[27];
    z[21]=z[21]*z[55];
    z[21]= - n<T>(556,3)*z[25] + z[21];
    z[21]=z[11]*z[21];
    z[19]=z[19] + z[21] - n<T>(145,6)*z[6] + 27*z[8];
    z[19]=z[10]*z[19];
    z[20]=z[20]*z[78];
    z[20]= - static_cast<T>(37)+ z[20];
    z[21]=n<T>(21,2)*z[25] + 2*z[71];
    z[21]=z[21]*z[74];
    z[21]=n<T>(269,6)*z[6] + z[21];
    z[21]=z[11]*z[21];
    z[17]=z[17] + z[19] + z[22] + 2*z[20] + z[21];
    z[17]=z[3]*z[17];
    z[19]=409*z[25] + 81*z[71];
    z[19]=z[11]*z[19];
    z[19]=n<T>(791,3)*z[6] + z[19];
    z[19]=z[19]*z[35];
    z[20]= - 110*z[70] - z[36];
    z[20]=z[11]*z[20];
    z[20]= - 108*z[25] + z[20];
    z[20]=z[11]*z[20];
    z[20]=z[20] - 24*z[84] - n<T>(51,2)*z[6] - 11*z[8];
    z[20]=z[10]*z[20];
    z[21]=12*z[84] + 13*z[8];
    z[21]=z[21]*z[13];
    z[22]=static_cast<T>(2)- z[21];
    z[19]=z[20] - 4*z[40] + 2*z[22] + z[19];
    z[19]=z[10]*z[19];
    z[20]= - z[28]*z[33];
    z[22]= - n<T>(1,2)*z[6] - z[78];
    z[22]=z[2]*z[22];
    z[20]=z[22] + 2*z[21] + n<T>(139,3) + z[20];
    z[20]=z[2]*z[20];
    z[21]=z[97] + n<T>(8,3)*z[45] + static_cast<T>(15)- z[76];
    z[21]=z[21]*z[92];
    z[22]=z[98]*z[32];
    z[22]=z[22] - z[56] + 259*z[6];
    z[22]=z[22]*z[35];
    z[22]=n<T>(79,3) + z[22];
    z[22]=z[11]*z[22];
    z[23]=static_cast<T>(42)+ 5*z[41];
    z[23]=z[23]*z[61];
    z[15]=z[18] + z[15] + z[16] + z[17] + z[21] + z[23] + z[19] + z[20]
    - 4*z[83] + z[22];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r187(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r187(const std::array<dd_real,31>&);
#endif
