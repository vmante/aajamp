#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1265(const std::array<T,31>& k) {
  T z[88];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[10];
    z[6]=k[13];
    z[7]=k[5];
    z[8]=k[7];
    z[9]=k[8];
    z[10]=k[12];
    z[11]=k[11];
    z[12]=k[14];
    z[13]=k[2];
    z[14]=k[4];
    z[15]=k[22];
    z[16]=z[14]*z[11];
    z[17]=z[10]*z[14];
    z[18]= - z[16] + z[17];
    z[18]=z[9]*z[18];
    z[19]=z[11] + z[8];
    z[20]=n<T>(1,2)*z[6];
    z[21]=n<T>(3,2)*z[10];
    z[18]=n<T>(3,2)*z[18] + z[21] + z[20] - z[19];
    z[18]=z[9]*z[18];
    z[22]=z[10]*z[8];
    z[23]=3*z[22];
    z[24]=z[6]*z[11];
    z[25]=z[23] + n<T>(7,2)*z[24];
    z[26]=n<T>(1,2)*z[25];
    z[18]=z[26] + z[18];
    z[18]=z[9]*z[18];
    z[27]=3*z[8];
    z[28]=z[27] + z[11];
    z[29]=n<T>(3,2)*z[6];
    z[30]=n<T>(5,2)*z[10];
    z[31]=z[30] + z[29] - z[28];
    z[32]=npow(z[3],2);
    z[33]=n<T>(1,2)*z[32];
    z[31]=z[31]*z[33];
    z[18]=z[31] + z[18];
    z[31]=3*z[9];
    z[18]=z[18]*z[31];
    z[34]=npow(z[4],4);
    z[35]=z[10]*z[12];
    z[36]=z[34]*z[35];
    z[37]=z[22] + z[24];
    z[38]=npow(z[9],4);
    z[38]=3*z[38];
    z[39]=z[38]*z[37];
    z[36]=z[36] - z[39];
    z[39]=z[36]*z[7];
    z[40]=z[6] - z[11];
    z[41]=3*z[10];
    z[42]= - z[41] + z[27] - z[40];
    z[42]=z[9]*z[42];
    z[42]=z[42] - 5*z[24] - z[23];
    z[43]=npow(z[9],3);
    z[43]=3*z[43];
    z[42]=z[42]*z[43];
    z[44]= - 3*z[12] + z[10];
    z[44]=z[4]*z[44];
    z[44]= - 7*z[35] + z[44];
    z[45]=npow(z[4],3);
    z[44]=z[44]*z[45];
    z[42]= - 3*z[39] + z[42] + z[44];
    z[42]=z[7]*z[42];
    z[44]=n<T>(1,2)*z[4];
    z[46]=z[44] - n<T>(5,2)*z[12] + z[10];
    z[46]=z[4]*z[46];
    z[46]=z[46] - n<T>(13,4)*z[35] + z[32];
    z[46]=z[4]*z[46];
    z[47]=z[20] - z[12];
    z[48]=z[30] + z[47];
    z[48]=z[48]*z[33];
    z[46]=z[48] + z[46];
    z[46]=z[4]*z[46];
    z[48]=3*z[24];
    z[49]= - z[12] + z[27];
    z[49]=z[10]*z[49];
    z[49]=z[48] + n<T>(5,4)*z[49];
    z[49]=z[49]*z[33];
    z[18]=n<T>(1,2)*z[42] + z[46] + z[49] + z[18];
    z[18]=z[7]*z[18];
    z[42]=z[8]*z[15];
    z[46]=z[42]*z[6];
    z[49]= - z[15]*z[22];
    z[49]=z[46] + z[49];
    z[50]=z[48] - z[35];
    z[50]=z[14]*z[50];
    z[23]= - z[2]*z[23];
    z[23]=z[23] + z[50];
    z[23]=z[23]*z[32];
    z[23]=9*z[49] + z[23];
    z[49]=npow(z[14],2);
    z[50]=z[49]*z[11];
    z[51]=z[49]*z[10];
    z[50]=z[50] - z[51];
    z[52]=z[50]*z[31];
    z[53]=5*z[16];
    z[54]=z[41]*z[14];
    z[52]=z[52] + z[53] - z[54];
    z[55]=n<T>(1,2)*z[9];
    z[52]=z[52]*z[55];
    z[52]=z[52] - z[21] - n<T>(1,4)*z[6] + z[8] + n<T>(5,4)*z[11];
    z[52]=z[9]*z[52];
    z[56]=z[6]*z[14];
    z[57]=n<T>(1,2)*z[56];
    z[58]=n<T>(1,2)*z[10];
    z[59]= - z[58] + z[8];
    z[59]=z[14]*z[59];
    z[59]= - z[57] + z[59];
    z[59]=z[59]*z[32];
    z[60]=n<T>(1,2)*z[24];
    z[52]=z[52] - z[60] + z[59];
    z[52]=z[52]*z[31];
    z[59]=7*z[12];
    z[61]= - z[59] + z[41];
    z[62]=n<T>(1,4)*z[13] + z[2];
    z[62]=z[62]*z[32];
    z[63]= - z[4]*z[13];
    z[63]=static_cast<T>(1)+ z[63];
    z[63]=z[4]*z[63];
    z[61]=z[63] + n<T>(1,4)*z[61] + z[62];
    z[61]=z[4]*z[61];
    z[62]=z[32]*z[2];
    z[63]=3*z[6];
    z[64]= - z[63] + z[10];
    z[64]=z[64]*z[62];
    z[65]=3*z[35];
    z[64]= - z[65] + z[64];
    z[61]=n<T>(1,2)*z[64] + z[61];
    z[61]=z[4]*z[61];
    z[18]=z[18] + z[61] + n<T>(1,8)*z[23] + z[52];
    z[18]=z[7]*z[18];
    z[23]=z[27] - z[11];
    z[23]=z[23]*z[14];
    z[23]=z[23] - z[54] + z[56];
    z[52]=npow(z[3],4);
    z[61]=z[43]*z[52];
    z[64]=z[23]*z[61];
    z[66]=z[13]*z[12];
    z[67]=z[2]*z[41];
    z[67]= - z[66] + z[67];
    z[67]=z[67]*z[52];
    z[68]=z[52]*z[4];
    z[69]=z[2] + z[13];
    z[70]=z[69]*z[68];
    z[67]=z[67] + z[70];
    z[67]=z[67]*z[45];
    z[64]=z[64] + z[67];
    z[67]=2*z[52];
    z[70]=z[37]*z[67];
    z[71]= - n<T>(3,2)*z[8] + z[21] + z[40];
    z[71]=z[71]*z[9];
    z[72]=z[52]*z[71];
    z[70]=z[70] + z[72];
    z[72]=npow(z[9],2);
    z[73]=3*z[72];
    z[70]=z[70]*z[73];
    z[74]= - z[35]*z[67];
    z[21]=z[44] - z[12] + z[21];
    z[21]=z[21]*z[68];
    z[21]=z[74] + z[21];
    z[74]=npow(z[4],2);
    z[21]=z[21]*z[74];
    z[21]=z[70] + z[21];
    z[21]=z[7]*z[21];
    z[21]=n<T>(1,2)*z[64] + z[21];
    z[21]=z[7]*z[21];
    z[64]=z[58] + z[6];
    z[70]=n<T>(1,2)*z[11];
    z[75]=z[70] + z[8];
    z[76]= - z[75] + z[64];
    z[61]=z[61]*z[49]*z[76];
    z[76]=npow(z[2],2);
    z[64]=z[64]*z[76];
    z[69]=z[69]*z[2];
    z[77]=npow(z[13],2);
    z[69]=z[69] + z[77];
    z[78]=z[69]*z[44];
    z[79]=n<T>(1,2)*z[77];
    z[80]= - z[12]*z[79];
    z[64]=z[78] + z[80] + z[64];
    z[64]=z[45]*z[52]*z[64];
    z[21]=z[21] + z[61] + z[64];
    z[21]=z[7]*z[21];
    z[61]=npow(z[2],3);
    z[64]=z[61]*z[6];
    z[78]=z[52]*z[64];
    z[69]=z[2]*z[69]*z[68];
    z[69]= - 7*z[78] + z[69];
    z[69]=z[69]*z[45];
    z[78]=z[38]*z[11];
    z[21]=z[21] + z[78] + n<T>(1,2)*z[69];
    z[21]=z[7]*z[21];
    z[69]=npow(z[3],5);
    z[80]=3*z[69];
    z[81]= - z[37]*z[80];
    z[82]= - z[10] + z[8] - z[40];
    z[82]=z[9]*z[82]*z[69];
    z[81]=z[81] + z[82];
    z[81]=z[81]*z[43];
    z[82]= - z[35]*z[80];
    z[83]=z[10] - z[12];
    z[84]=z[69]*z[4];
    z[85]=z[83]*z[84];
    z[82]=z[82] + z[85];
    z[82]=z[82]*z[45];
    z[81]=z[81] + z[82];
    z[81]=z[7]*z[81];
    z[82]= - z[8] + z[11];
    z[82]=z[14]*z[82];
    z[82]=z[17] + z[82] - z[56];
    z[38]=z[38]*z[69];
    z[82]=z[82]*z[38];
    z[69]=z[34]*z[69];
    z[85]=z[2]*z[10];
    z[86]= - z[66] + z[85];
    z[86]=z[86]*z[69];
    z[81]=z[81] + z[82] + z[86];
    z[81]=z[7]*z[81];
    z[82]=z[19] - z[6];
    z[82]=z[49]*z[82];
    z[82]= - z[51] + z[82];
    z[38]=z[82]*z[38];
    z[82]=z[77]*z[12];
    z[86]=z[10]*z[76];
    z[86]= - z[82] + z[86];
    z[86]=z[86]*z[69];
    z[38]=z[81] + z[38] + z[86];
    z[38]=z[7]*z[38];
    z[64]=z[69]*z[64];
    z[38]=z[64] + z[38];
    z[38]=z[7]*z[38];
    z[64]=npow(z[2],4);
    z[69]= - z[64]*z[63]*z[69];
    z[38]=z[69] + z[38];
    z[38]=z[7]*z[38];
    z[69]=z[5]*z[13];
    z[81]=z[5] + z[63];
    z[81]=z[2]*z[81];
    z[81]= - z[69] + z[81];
    z[81]=z[2]*z[81];
    z[86]=z[77]*z[5];
    z[81]=z[86] + z[81];
    z[81]=z[81]*z[61]*z[84];
    z[84]=z[6]*z[5];
    z[80]= - npow(z[2],5)*z[84]*z[80];
    z[80]=z[80] + z[81];
    z[80]=z[80]*z[45];
    z[36]= - npow(z[7],6)*z[36];
    z[34]= - npow(z[2],6)*z[34]*z[84];
    z[34]=z[34] + z[36];
    z[34]=z[1]*z[34]*npow(z[3],6);
    z[34]=z[34] + z[38] + z[78] + z[80];
    z[34]=z[1]*z[34];
    z[36]= - z[64]*z[84]*z[67];
    z[38]=4*z[6];
    z[64]=n<T>(3,2)*z[5];
    z[67]=z[64] + z[38];
    z[67]=z[2]*z[67];
    z[67]= - n<T>(3,2)*z[69] + z[67];
    z[67]=z[2]*z[67];
    z[78]=n<T>(1,2)*z[86];
    z[67]=z[78] + z[67];
    z[52]=z[67]*z[52];
    z[67]=n<T>(1,2)*z[13];
    z[80]=z[67] - z[2];
    z[80]=z[2]*z[80];
    z[80]=z[79] + z[80];
    z[68]=z[80]*z[68];
    z[52]=z[52] + z[68];
    z[52]=z[4]*z[76]*z[52];
    z[36]=z[36] + z[52];
    z[36]=z[36]*z[74];
    z[52]=z[16] + n<T>(1,2);
    z[68]= - z[9]*z[52];
    z[68]= - z[11] + z[68];
    z[68]=z[68]*z[43];
    z[21]=n<T>(1,2)*z[34] + z[21] + z[68] + z[36];
    z[21]=z[1]*z[21];
    z[34]=2*z[8];
    z[36]=2*z[10];
    z[68]= - z[36] + z[34] - z[40];
    z[80]=npow(z[3],3);
    z[68]=z[68]*z[80];
    z[81]=z[37]*z[72];
    z[68]=z[68] + n<T>(1,2)*z[81];
    z[68]=z[9]*z[68];
    z[81]=n<T>(3,2)*z[80];
    z[37]= - z[37]*z[81];
    z[37]=z[37] + z[68];
    z[37]=z[37]*z[31];
    z[36]= - z[12] + z[36];
    z[36]=z[36]*z[80];
    z[68]= - z[35]*z[44];
    z[68]=z[80] + z[68];
    z[68]=z[4]*z[68];
    z[36]=z[36] + z[68];
    z[36]=z[4]*z[36];
    z[68]= - z[35]*z[81];
    z[36]=z[68] + z[36];
    z[36]=z[4]*z[36];
    z[36]=z[37] + z[36];
    z[36]=z[7]*z[36];
    z[37]= - 7*z[8] + z[11];
    z[37]=z[14]*z[37];
    z[37]=5*z[17] + z[37] + z[56];
    z[68]=n<T>(1,2)*z[80];
    z[37]=z[37]*z[68];
    z[81]=z[11] - z[10];
    z[81]=z[9]*z[81];
    z[24]= - z[24] + z[81];
    z[24]=z[9]*z[24];
    z[24]=z[37] + z[24];
    z[24]=z[24]*z[72];
    z[37]=5*z[10];
    z[81]=z[63] + z[37];
    z[81]=z[2]*z[81];
    z[81]= - z[66] + z[81];
    z[81]=z[81]*z[80];
    z[67]=z[67] + z[2];
    z[87]=z[67]*z[80];
    z[87]= - z[35] + z[87];
    z[87]=z[4]*z[87];
    z[81]=n<T>(1,4)*z[81] + z[87];
    z[81]=z[81]*z[74];
    z[24]=z[36] + n<T>(3,2)*z[24] + z[81];
    z[24]=z[7]*z[24];
    z[28]=z[28] - z[63];
    z[28]=z[49]*z[28];
    z[28]= - z[51] + z[28];
    z[28]=z[28]*z[68];
    z[36]= - z[53] + z[17];
    z[36]=z[9]*z[36];
    z[51]=3*z[11];
    z[36]= - z[51] + z[36];
    z[36]=z[9]*z[36];
    z[28]=z[36] + z[28] + z[60] + z[22];
    z[28]=z[28]*z[72];
    z[36]=npow(z[15],2);
    z[53]= - z[8]*z[36];
    z[46]=z[53] - z[46];
    z[46]=z[6]*z[46];
    z[53]=z[36]*z[22];
    z[46]=z[46] + z[53];
    z[28]=n<T>(1,4)*z[46] + z[28];
    z[46]= - 13*z[6] + z[10];
    z[46]=z[46]*z[76];
    z[46]= - z[82] + z[46];
    z[46]=z[46]*z[80];
    z[46]= - z[65] + z[46];
    z[53]=z[2]*z[67];
    z[53]=z[79] + z[53];
    z[53]=z[53]*z[80];
    z[53]=z[53] - z[44];
    z[53]=z[4]*z[53];
    z[46]=n<T>(1,4)*z[46] + z[53];
    z[46]=z[46]*z[74];
    z[24]=z[24] + n<T>(3,2)*z[28] + z[46];
    z[24]=z[7]*z[24];
    z[28]=n<T>(1,4)*z[36];
    z[36]=z[28] + z[42];
    z[28]=z[5]*z[28]*z[14];
    z[28]= - z[28] + 3*z[36];
    z[36]=z[5]*z[15];
    z[42]=z[6]*z[15];
    z[42]=n<T>(3,4)*z[42] + n<T>(5,4)*z[36] + z[28];
    z[42]=z[6]*z[42];
    z[46]= - n<T>(1,4)*z[17] - n<T>(5,4);
    z[46]=z[36]*z[46];
    z[28]=z[46] - z[28];
    z[28]=z[10]*z[28];
    z[28]=z[42] + z[28];
    z[42]=z[13] - 5*z[2];
    z[42]=z[2]*z[42];
    z[42]=z[77] + z[42];
    z[46]=n<T>(1,2)*z[2];
    z[42]=z[42]*z[80]*z[46];
    z[53]=z[69] - 1;
    z[60]=z[84]*z[2];
    z[65]=z[60] - z[5];
    z[67]=z[2]*z[65];
    z[67]=z[67] + z[53];
    z[67]=z[67]*z[46];
    z[67]=z[67] + z[13] - z[78];
    z[67]=z[4]*z[67];
    z[42]=z[42] + z[67];
    z[42]=z[4]*z[42];
    z[67]=2*z[5] + n<T>(9,2)*z[6];
    z[67]=z[2]*z[67];
    z[67]= - n<T>(5,4)*z[69] + z[67];
    z[67]=z[2]*z[67];
    z[68]=n<T>(1,4)*z[86];
    z[67]=z[68] + z[67];
    z[67]=z[2]*z[67]*z[80];
    z[42]=z[42] + n<T>(1,2)*z[65] + z[67];
    z[42]=z[4]*z[42];
    z[61]=z[61]*z[80];
    z[61]= - 3*z[61] - 1;
    z[61]=z[84]*z[61];
    z[61]= - n<T>(1,2)*z[35] + z[61];
    z[42]=n<T>(1,2)*z[61] + z[42];
    z[42]=z[4]*z[42];
    z[50]=z[50]*z[9];
    z[61]=n<T>(1,2)*z[50];
    z[16]=z[61] + static_cast<T>(1)+ n<T>(3,2)*z[16];
    z[16]=z[9]*z[16];
    z[67]=z[58] - z[11];
    z[16]=z[16] - z[67];
    z[16]=z[9]*z[16];
    z[16]=n<T>(1,2)*z[22] + z[16];
    z[16]=z[16]*z[31];
    z[16]=z[21] + z[24] + z[42] + n<T>(1,2)*z[28] + z[16];
    z[16]=z[1]*z[16];
    z[21]=5*z[5] + 11*z[6];
    z[21]=z[21]*z[46];
    z[21]= - z[69] + z[21];
    z[21]=z[21]*z[62];
    z[22]=n<T>(1,2)*z[5];
    z[21]=z[21] + z[58] - z[20] - z[12] + z[22];
    z[24]= - n<T>(3,2)*z[60] + z[64] + z[6];
    z[24]=z[24]*z[2];
    z[28]=3*z[69];
    z[42]=static_cast<T>(1)- z[28];
    z[42]=n<T>(1,2)*z[42] + z[24];
    z[42]=z[2]*z[42];
    z[62]=z[6] - z[65];
    z[62]=z[2]*z[62];
    z[62]= - z[69] + z[62];
    z[62]=z[2]*z[62];
    z[65]=z[86] - z[13];
    z[62]=z[62] + z[65];
    z[72]=3*z[2];
    z[62]=z[62]*z[72];
    z[62]=z[77] + z[62];
    z[62]=z[62]*z[44];
    z[80]= - 3*z[13] + z[86];
    z[42]=z[62] + n<T>(1,2)*z[80] + z[42];
    z[42]=z[4]*z[42];
    z[62]=z[13] - 9*z[2];
    z[62]=z[2]*z[62];
    z[62]=z[77] + z[62];
    z[32]=z[62]*z[32];
    z[24]=z[42] + n<T>(1,4)*z[32] - n<T>(1,2)*z[53] + z[24];
    z[24]=z[4]*z[24];
    z[21]=n<T>(1,2)*z[21] + z[24];
    z[21]=z[4]*z[21];
    z[24]=z[36]*z[14];
    z[24]=z[24] + z[15];
    z[32]= - z[58] + z[64] - z[12] - n<T>(3,2)*z[24];
    z[32]=z[10]*z[32];
    z[24]= - z[6] - z[5] - z[27] + z[24];
    z[24]=z[24]*z[29];
    z[29]= - z[14]*z[84];
    z[29]=z[29] - 5*z[60];
    z[29]=z[2]*z[29]*z[33];
    z[24]=z[29] + z[24] + z[32];
    z[29]= - z[8] + z[6];
    z[29]=z[33]*z[49]*z[29];
    z[29]=z[29] + n<T>(1,2)*z[8] + z[67];
    z[32]=z[14]*z[58];
    z[32]= - z[61] + z[32] - z[52];
    z[32]=z[9]*z[32];
    z[29]=n<T>(1,2)*z[29] + z[32];
    z[29]=z[29]*z[31];
    z[16]=z[16] + z[18] + z[21] + n<T>(1,4)*z[24] + z[29];
    z[16]=z[1]*z[16];
    z[18]=z[34] + z[70];
    z[18]=z[14]*z[18];
    z[18]=n<T>(3,2)*z[50] - z[54] + z[18] + z[57];
    z[18]=z[9]*z[18];
    z[21]=11*z[8] + 5*z[11];
    z[18]=z[18] - z[41] + n<T>(1,4)*z[21] - z[6];
    z[18]=z[9]*z[18];
    z[18]= - z[26] + z[18];
    z[18]=z[18]*z[31];
    z[21]= - z[71] - z[25];
    z[21]=z[21]*z[43];
    z[24]= - n<T>(3,2)*z[12] + z[10];
    z[24]=z[4]*z[24];
    z[24]= - 4*z[35] + z[24];
    z[24]=z[24]*z[45];
    z[21]= - n<T>(3,2)*z[39] + z[21] + z[24];
    z[21]=z[7]*z[21];
    z[23]= - z[23]*z[55];
    z[20]=z[10] + z[20] - z[75];
    z[20]=3*z[20] + z[23];
    z[20]=z[9]*z[20];
    z[20]=z[20] + z[25];
    z[20]=z[20]*z[73];
    z[23]=z[2]*z[58];
    z[23]=z[23] + static_cast<T>(1)- n<T>(3,2)*z[66];
    z[23]=z[4]*z[23];
    z[23]=n<T>(5,2)*z[83] + z[23];
    z[23]=z[4]*z[23];
    z[23]= - n<T>(9,2)*z[35] + z[23];
    z[23]=z[23]*z[74];
    z[20]=z[21] + z[20] + z[23];
    z[20]=z[7]*z[20];
    z[21]=z[13] + z[46];
    z[21]=z[4]*z[21];
    z[21]=z[21] + z[85] + n<T>(3,2) - 2*z[66];
    z[21]=z[4]*z[21];
    z[23]= - n<T>(7,2)*z[12] + z[37];
    z[21]=n<T>(1,2)*z[23] + z[21];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(11,4)*z[35] + z[21];
    z[21]=z[4]*z[21];
    z[18]=z[20] + z[18] + z[21];
    z[18]=z[7]*z[18];
    z[20]= - 5*z[8] - z[51];
    z[20]=z[14]*z[20];
    z[20]=7*z[17] + z[20] + z[56];
    z[20]=n<T>(1,4)*z[20] - z[50];
    z[20]=z[9]*z[20];
    z[21]=z[30] - n<T>(5,2)*z[8] + z[40];
    z[20]=n<T>(1,2)*z[21] + z[20];
    z[20]=z[20]*z[31];
    z[21]=5*z[6];
    z[23]=z[21] + z[10];
    z[23]=z[2]*z[23];
    z[23]=z[23] + static_cast<T>(3)- 5*z[66];
    z[24]=z[76]*z[63];
    z[24]=z[13] + z[24];
    z[24]=z[2]*z[24];
    z[24]= - z[77] + z[24];
    z[24]=z[24]*z[44];
    z[25]=z[2]*z[6];
    z[25]=n<T>(1,2) + 2*z[25];
    z[25]=z[2]*z[25];
    z[24]=z[24] + n<T>(3,2)*z[13] + z[25];
    z[24]=z[4]*z[24];
    z[23]=n<T>(1,4)*z[23] + z[24];
    z[23]=z[4]*z[23];
    z[23]=z[23] + n<T>(1,2)*z[47] + z[10];
    z[23]=z[4]*z[23];
    z[24]= - z[59] + 9*z[8];
    z[24]=z[24]*z[58];
    z[24]=z[48] + z[24];
    z[18]=z[18] + z[23] + n<T>(1,4)*z[24] + z[20];
    z[18]=z[7]*z[18];
    z[19]=z[14]*z[19];
    z[17]=z[50] - z[17] + z[19] - z[56];
    z[17]=z[9]*z[17];
    z[19]= - z[66] + z[69];
    z[20]=3*z[60];
    z[23]=z[20] - n<T>(5,2)*z[5] - z[63];
    z[23]=z[2]*z[23];
    z[19]=n<T>(1,2)*z[19] + z[23];
    z[20]= - z[20] + 3*z[5];
    z[23]= - z[21] - z[20];
    z[23]=z[2]*z[23];
    z[23]=z[23] + static_cast<T>(2)+ z[28];
    z[23]=z[2]*z[23];
    z[24]=n<T>(1,2)*z[60] - z[22] - z[6];
    z[24]=z[2]*z[24];
    z[25]=static_cast<T>(1)+ z[69];
    z[24]=n<T>(1,2)*z[25] + z[24];
    z[24]=z[2]*z[24];
    z[24]= - z[78] + z[24];
    z[24]=z[24]*z[72];
    z[24]= - z[77] + z[24];
    z[24]=z[4]*z[24];
    z[23]=z[24] + z[23] - z[65];
    z[23]=z[2]*z[23];
    z[23]= - z[79] + z[23];
    z[23]=z[4]*z[23];
    z[20]= - z[38] - z[20];
    z[20]=z[2]*z[20];
    z[24]=static_cast<T>(3)+ 7*z[69];
    z[20]=n<T>(1,4)*z[24] + z[20];
    z[20]=z[2]*z[20];
    z[20]=z[23] + z[20] + z[13] - z[68];
    z[20]=z[4]*z[20];
    z[19]=n<T>(1,2)*z[19] + z[20];
    z[19]=z[4]*z[19];
    z[20]=3*z[75] - z[22];
    z[22]= - z[14]*z[12];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[10]*z[22];
    z[16]=z[16] + z[18] + z[19] + n<T>(3,4)*z[17] + n<T>(3,8)*z[60] + n<T>(1,8)*
    z[22] + n<T>(1,4)*z[20] - z[6];
    z[16]=z[1]*z[16];
    z[17]= - z[8] + z[5];
    z[17]=n<T>(3,2)*z[17] + z[21];
    z[17]=z[2]*z[17];
    z[18]= - z[14]*z[27];
    z[17]=z[17] + n<T>(7,2)*z[56] - n<T>(1,2) + z[18];
    z[18]= - z[12] + 15*z[8];
    z[18]=z[44] - n<T>(3,4)*z[10] + n<T>(1,8)*z[18] - z[6];
    z[18]=z[7]*z[18];
    z[19]= - z[4]*z[2];

    r += z[16] + n<T>(1,4)*z[17] + z[18] + z[19];
 
    return r;
}

template double qqb_2lNLC_r1265(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1265(const std::array<dd_real,31>&);
#endif
