#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r706(const std::array<T,31>& k) {
  T z[126];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=k[8];
    z[11]=k[9];
    z[12]=k[2];
    z[13]=k[3];
    z[14]=k[15];
    z[15]=npow(z[1],4);
    z[16]=3*z[15];
    z[17]=npow(z[1],3);
    z[18]=z[17]*z[9];
    z[19]= - z[16] - z[18];
    z[19]=z[9]*z[19];
    z[20]=npow(z[1],5);
    z[21]=3*z[20];
    z[19]= - z[21] + z[19];
    z[22]=4*z[15] + z[18];
    z[22]=z[9]*z[22];
    z[22]=6*z[20] + z[22];
    z[22]=z[9]*z[22];
    z[23]=npow(z[1],6);
    z[22]=4*z[23] + z[22];
    z[22]=z[5]*z[22];
    z[19]=2*z[19] + z[22];
    z[19]=z[5]*z[19];
    z[22]=2*z[15];
    z[24]=z[22] + z[18];
    z[19]=2*z[24] + z[19];
    z[19]=z[5]*z[19];
    z[24]=z[18] - z[15];
    z[25]=z[24]*z[9];
    z[25]=z[25] + z[20];
    z[26]= - z[9]*z[25];
    z[26]=z[23] + z[26];
    z[26]=z[7]*z[26];
    z[25]= - 2*z[25] + z[26];
    z[25]=z[7]*z[25];
    z[25]= - 2*z[24] + z[25];
    z[25]=z[7]*z[25];
    z[26]=2*z[23];
    z[27]=npow(z[1],7);
    z[28]=z[7]*z[27];
    z[28]= - z[26] + z[28];
    z[28]=z[7]*z[28];
    z[29]=2*z[20];
    z[28]=z[29] + z[28];
    z[28]=z[7]*z[28];
    z[28]= - z[15] + z[28];
    z[28]=z[6]*z[28];
    z[30]=2*z[17];
    z[19]=z[28] + z[25] - z[30] + z[19];
    z[25]=3*z[4];
    z[19]=z[19]*z[25];
    z[28]=npow(z[1],2);
    z[31]=z[28]*z[9];
    z[32]=6*z[31];
    z[33]=n<T>(149,4)*z[17] + z[32];
    z[33]=z[9]*z[33];
    z[34]=n<T>(1,2)*z[31];
    z[35]=z[34] - z[30];
    z[36]=z[9]*z[35];
    z[36]= - 9*z[15] + z[36];
    z[36]=z[9]*z[36];
    z[36]= - 10*z[20] + z[36];
    z[37]=3*z[5];
    z[36]=z[36]*z[37];
    z[33]=z[36] + n<T>(221,4)*z[15] + z[33];
    z[33]=z[5]*z[33];
    z[36]=7*z[31];
    z[38]=z[36] + n<T>(57,2)*z[17];
    z[38]=n<T>(1,2)*z[38];
    z[33]= - z[38] + z[33];
    z[33]=z[5]*z[33];
    z[32]= - n<T>(49,4)*z[17] + z[32];
    z[32]=z[9]*z[32];
    z[39]=z[31] + z[17];
    z[40]=z[39]*z[9];
    z[41]=z[16] - z[40];
    z[41]=z[9]*z[41];
    z[42]=5*z[20];
    z[41]= - z[42] + z[41];
    z[41]=z[7]*z[41];
    z[43]=18*z[15];
    z[32]=n<T>(3,2)*z[41] + z[43] + z[32];
    z[32]=z[7]*z[32];
    z[41]=n<T>(23,2)*z[17];
    z[44]= - z[41] + 11*z[31];
    z[32]=n<T>(1,2)*z[44] + z[32];
    z[32]=z[7]*z[32];
    z[44]=8*z[20];
    z[45]=n<T>(7,2)*z[23];
    z[45]= - z[7]*z[45];
    z[45]=z[44] + z[45];
    z[46]=3*z[7];
    z[45]=z[45]*z[46];
    z[45]= - n<T>(49,4)*z[15] + z[45];
    z[45]=z[7]*z[45];
    z[45]= - z[30] + z[45];
    z[45]=z[6]*z[45];
    z[47]=6*z[28];
    z[19]=z[19] + z[45] + z[32] - z[47] + z[33];
    z[19]=z[4]*z[19];
    z[32]=npow(z[9],2);
    z[33]=z[32]*z[17];
    z[45]=z[17]*z[5];
    z[48]=npow(z[9],3);
    z[49]= - z[48]*z[45];
    z[49]= - z[33] + z[49];
    z[50]=n<T>(1,2)*z[5];
    z[49]=z[49]*z[50];
    z[49]= - z[18] + z[49];
    z[49]=z[5]*z[49];
    z[51]=z[17]*z[7];
    z[52]=z[48]*z[51];
    z[33]= - z[33] + z[52];
    z[52]=n<T>(1,2)*z[7];
    z[33]=z[33]*z[52];
    z[33]=z[18] + z[33];
    z[33]=z[7]*z[33];
    z[33]=z[49] + z[33];
    z[33]=z[11]*z[33];
    z[49]=2*z[9];
    z[53]=z[49]*z[28];
    z[54]=n<T>(3,2)*z[17];
    z[55]= - z[54] + z[53];
    z[55]=z[9]*z[55];
    z[56]=z[32]*z[5];
    z[35]=z[35]*z[56];
    z[35]=z[55] + z[35];
    z[35]=z[5]*z[35];
    z[55]=n<T>(5,2)*z[31];
    z[35]=z[55] + z[35];
    z[35]=z[5]*z[35];
    z[57]=n<T>(1,2)*z[17];
    z[58]=z[57] + z[53];
    z[58]=z[9]*z[58];
    z[59]=z[39]*z[52];
    z[60]= - z[32]*z[59];
    z[58]=z[58] + z[60];
    z[58]=z[7]*z[58];
    z[55]=z[58] + z[17] - z[55];
    z[55]=z[7]*z[55];
    z[33]=z[33] + z[55] - z[47] + z[35];
    z[35]=3*z[11];
    z[33]=z[33]*z[35];
    z[55]=z[9]*z[1];
    z[58]=5*z[55];
    z[60]= - z[47] - z[58];
    z[60]=z[9]*z[60];
    z[60]= - z[17] + z[60];
    z[60]=z[9]*z[60];
    z[61]=z[55] + z[28];
    z[62]=z[61]*z[6];
    z[63]=npow(z[9],4);
    z[64]=z[63]*z[62];
    z[65]=z[63]*z[1];
    z[64]=z[65] + z[64];
    z[64]=z[6]*z[64];
    z[60]=z[60] + z[64];
    z[60]=z[6]*z[60];
    z[64]=z[61]*z[9];
    z[66]=5*z[64];
    z[60]= - z[66] + z[60];
    z[67]=z[39]*z[48];
    z[68]=2*z[28];
    z[69]=z[68] + z[55];
    z[70]=z[69]*z[9];
    z[71]=z[70] + z[17];
    z[72]=z[71]*z[6];
    z[73]= - z[63]*z[72];
    z[73]=2*z[67] + z[73];
    z[73]=z[6]*z[73];
    z[74]=8*z[28];
    z[75]=z[74] + z[58];
    z[75]=z[9]*z[75];
    z[75]=z[30] + z[75];
    z[75]=z[75]*z[32];
    z[73]=z[75] + z[73];
    z[73]=z[6]*z[73];
    z[75]=z[61]*z[49];
    z[75]=z[17] + z[75];
    z[75]=z[75]*z[49];
    z[73]=z[75] + z[73];
    z[73]=z[10]*z[73];
    z[60]=2*z[60] + z[73];
    z[60]=z[10]*z[60];
    z[73]=18*z[28] + 17*z[55];
    z[73]=z[9]*z[73];
    z[75]=2*z[1];
    z[76]= - z[48]*z[75];
    z[65]= - z[6]*z[65];
    z[65]=z[76] + z[65];
    z[65]=z[6]*z[65];
    z[76]=6*z[17];
    z[65]=z[65] + z[76] + z[73];
    z[65]=z[6]*z[65];
    z[73]=z[53] + z[17];
    z[77]=z[18]*z[11];
    z[78]=z[77] - z[73];
    z[79]=6*z[11];
    z[78]=z[78]*z[79];
    z[80]=6*z[55];
    z[81]=z[28] + z[80];
    z[60]=z[60] - z[78] + 2*z[81] + z[65];
    z[60]=z[10]*z[60];
    z[65]=n<T>(3,2)*z[15] + z[18];
    z[81]=5*z[5];
    z[65]=z[65]*z[81];
    z[82]=n<T>(1,2)*z[15];
    z[83]=z[82] - z[18];
    z[83]=z[7]*z[83];
    z[84]=z[20]*z[7];
    z[85]= - z[15] + z[84];
    z[85]=z[6]*z[85];
    z[65]=n<T>(1,2)*z[85] + z[83] - z[76] + z[65];
    z[65]=z[4]*z[65];
    z[24]=z[7]*z[24];
    z[24]=z[24] + z[17] + 5*z[31];
    z[24]=z[6]*z[24];
    z[76]=3*z[45];
    z[83]=z[28] - z[76];
    z[24]=5*z[83] + z[24];
    z[24]=n<T>(1,2)*z[24] + z[65];
    z[24]=z[2]*z[24];
    z[65]= - z[22] + n<T>(3,2)*z[18];
    z[83]= - z[9]*z[16];
    z[42]=z[42] + z[83];
    z[42]=z[42]*z[52];
    z[42]=3*z[65] + z[42];
    z[42]=z[42]*z[46];
    z[65]=37*z[17] - 55*z[31];
    z[42]=n<T>(1,4)*z[65] + z[42];
    z[42]=z[7]*z[42];
    z[42]=z[42] - z[28] - 14*z[55];
    z[42]=z[6]*z[42];
    z[65]=4*z[55];
    z[83]=z[65] + z[28];
    z[85]= - z[10]*z[70];
    z[85]=z[85] + z[83];
    z[85]=z[10]*z[85];
    z[86]=3*z[1];
    z[85]= - z[86] + z[85];
    z[85]=z[10]*z[85];
    z[87]=npow(z[10],2);
    z[88]=z[10]*z[55];
    z[88]= - z[1] + z[88];
    z[88]=z[8]*z[88]*z[87];
    z[85]=z[85] + z[88];
    z[85]=z[8]*z[85];
    z[88]=z[52]*z[28];
    z[89]=z[15]*z[5];
    z[90]= - n<T>(167,4)*z[17] + 18*z[89];
    z[90]=z[5]*z[90];
    z[90]=n<T>(113,4)*z[28] + z[90];
    z[90]=z[5]*z[90];
    z[19]=z[24] + 2*z[85] + z[60] + z[33] + z[19] + z[42] + z[90] + 
    z[88];
    z[19]=z[2]*z[19];
    z[24]=2*z[45];
    z[33]= - n<T>(23,2)*z[28] - z[24];
    z[33]=z[5]*z[33];
    z[42]=npow(z[5],2);
    z[60]=z[42]*z[15];
    z[85]= - z[60] + z[51];
    z[90]=2*z[6];
    z[85]=z[85]*z[90];
    z[33]=z[85] + z[33] + z[88];
    z[33]=z[6]*z[33];
    z[85]=z[17]*z[10];
    z[91]=z[17]*z[6];
    z[92]=7*z[28];
    z[93]=4*z[85] - z[92] - z[91];
    z[93]=z[10]*z[93];
    z[94]= - z[92] - z[24];
    z[94]=z[5]*z[94];
    z[95]= - z[6]*z[45];
    z[93]=z[93] + z[94] + z[95];
    z[94]=npow(z[6],2);
    z[93]=z[94]*z[93];
    z[24]= - z[28] - z[24];
    z[24]=z[5]*z[24];
    z[95]= - z[28] + z[85];
    z[95]=z[10]*z[95];
    z[24]=z[24] + z[95];
    z[95]=npow(z[6],3);
    z[24]=z[8]*z[95]*z[24];
    z[24]=z[24] + z[93];
    z[24]=z[8]*z[24];
    z[93]= - z[15]*z[37];
    z[93]=17*z[17] + z[93];
    z[93]=z[5]*z[93];
    z[93]= - 13*z[28] + z[93];
    z[93]=z[93]*z[50];
    z[93]=z[1] + z[93];
    z[96]=z[20]*z[5];
    z[97]=n<T>(1,4)*z[96];
    z[98]=z[97] - z[15];
    z[98]=z[98]*z[5];
    z[99]=z[54] + z[98];
    z[99]=z[5]*z[99];
    z[100]=n<T>(3,4)*z[28];
    z[99]= - z[100] + z[99];
    z[99]=z[7]*z[99];
    z[93]=n<T>(1,2)*z[93] + z[99];
    z[93]=z[7]*z[93];
    z[60]=z[28] - n<T>(3,4)*z[60];
    z[60]=z[5]*z[60];
    z[99]=z[20]*z[42];
    z[99]=z[17] + z[99];
    z[99]=z[4]*z[99]*z[50];
    z[60]=z[60] + z[99];
    z[60]=z[4]*z[60];
    z[99]=3*z[17];
    z[101]= - z[6]*z[99];
    z[101]= - 16*z[28] + z[101];
    z[101]=z[6]*z[101];
    z[102]=z[28]*z[7];
    z[103]=z[10]*z[91];
    z[101]=7*z[103] - z[102] + z[101];
    z[101]=z[10]*z[101];
    z[24]=z[24] + z[101] + 5*z[60] + z[93] + z[33];
    z[24]=z[8]*z[24];
    z[33]= - z[15] + z[96];
    z[33]=z[4]*z[33];
    z[60]=z[45] - z[28];
    z[60]= - z[8]*z[60];
    z[33]=z[60] + z[33] + z[17] - z[89];
    z[33]=z[2]*z[33];
    z[60]=z[23]*z[5];
    z[44]=z[44] - n<T>(7,2)*z[60];
    z[44]=z[44]*z[37];
    z[44]= - n<T>(53,4)*z[15] + z[44];
    z[44]=z[5]*z[44];
    z[27]=z[5]*z[27];
    z[26]= - z[26] + z[27];
    z[26]=z[5]*z[26];
    z[26]=z[29] + z[26];
    z[26]=z[5]*z[26];
    z[26]= - z[15] + z[26];
    z[26]=z[26]*z[25];
    z[26]=z[44] + z[26];
    z[26]=z[4]*z[26];
    z[27]=6*z[15];
    z[44]= - z[27] + n<T>(5,2)*z[96];
    z[44]=z[5]*z[44];
    z[44]=n<T>(17,4)*z[17] + z[44];
    z[44]=z[44]*z[37];
    z[93]= - z[99] + z[89];
    z[101]=9*z[5];
    z[93]=z[93]*z[101];
    z[103]=n<T>(59,2)*z[28];
    z[93]=z[103] + z[93];
    z[93]=z[93]*z[50];
    z[93]=z[86] + z[93];
    z[93]=z[8]*z[93];
    z[104]=n<T>(5,2)*z[28];
    z[26]=n<T>(5,2)*z[33] + z[93] + z[26] - z[104] + z[44];
    z[26]=z[2]*z[26];
    z[33]=z[22] - z[96];
    z[44]= - z[5]*z[33];
    z[21]=z[21] - z[60];
    z[21]=z[5]*z[21];
    z[21]= - z[22] + z[21];
    z[21]=z[7]*z[21];
    z[21]=z[44] + z[21];
    z[21]=z[21]*z[90];
    z[44]=3*z[28];
    z[60]=z[44] + n<T>(1,2)*z[45];
    z[29]= - z[42]*z[29];
    z[29]= - z[57] + z[29];
    z[29]=z[7]*z[29];
    z[21]=z[21] + 3*z[60] + z[29];
    z[21]=z[6]*z[21];
    z[29]=z[17]*z[11];
    z[60]=z[10]*z[99];
    z[60]=z[60] - z[29] - 9*z[28] + z[51];
    z[60]=z[10]*z[60];
    z[93]=z[22] - z[97];
    z[93]=z[5]*z[93];
    z[93]= - n<T>(19,2)*z[17] + z[93];
    z[93]=z[5]*z[93];
    z[97]=n<T>(3,4)*z[17];
    z[98]= - z[97] - z[98];
    z[98]=z[7]*z[98];
    z[93]=z[98] + z[104] + z[93];
    z[93]=z[7]*z[93];
    z[98]=16*z[15] - n<T>(29,4)*z[96];
    z[98]=z[5]*z[98];
    z[105]=n<T>(5,2)*z[17];
    z[98]=z[105] + z[98];
    z[98]=z[5]*z[98];
    z[106]=z[23]*z[37];
    z[106]= - n<T>(11,2)*z[20] + z[106];
    z[106]=z[5]*z[106];
    z[106]=z[16] + z[106];
    z[106]=z[5]*z[106];
    z[106]= - n<T>(11,2)*z[17] + z[106];
    z[106]=z[4]*z[106];
    z[98]=z[106] - n<T>(17,2)*z[28] + z[98];
    z[98]=z[4]*z[98];
    z[106]=z[52]*z[17];
    z[107]=z[106] - z[28];
    z[108]= - z[4]*z[15];
    z[108]= - z[57] + z[108];
    z[108]=z[11]*z[108];
    z[109]=z[4]*z[30];
    z[108]=z[108] + z[109] - z[107];
    z[108]=z[108]*z[35];
    z[109]=7*z[1];
    z[110]=z[28]*z[5];
    z[21]=z[26] + z[24] + z[60] + z[108] + z[98] + z[21] + z[93] - 
    z[109] - 12*z[110];
    z[21]=z[3]*z[21];
    z[24]= - z[54] - z[31];
    z[24]=z[9]*z[24];
    z[24]= - z[82] + z[24];
    z[24]=z[5]*z[24];
    z[26]= - z[32]*z[106];
    z[26]=z[18] + z[26];
    z[26]=z[7]*z[26];
    z[24]=z[26] + z[24] - z[54] + z[31];
    z[24]=z[7]*z[24];
    z[26]=n<T>(1,2)*z[28];
    z[60]= - z[48]*z[26];
    z[93]=z[28] + n<T>(1,2)*z[55];
    z[63]=z[63]*z[5];
    z[98]= - z[93]*z[63];
    z[60]=z[60] + z[98];
    z[60]=z[5]*z[60];
    z[98]=n<T>(3,2)*z[55];
    z[108]=4*z[28];
    z[111]=z[108] + z[98];
    z[111]=z[111]*z[32];
    z[60]=z[111] + z[60];
    z[60]=z[5]*z[60];
    z[111]=z[93]*z[9];
    z[111]=z[111] + z[57];
    z[112]= - z[111]*z[63];
    z[67]= - z[67] + z[112];
    z[67]=z[5]*z[67];
    z[112]=z[68] + z[98];
    z[112]=z[112]*z[48];
    z[67]=z[112] + z[67];
    z[67]=z[5]*z[67];
    z[112]=z[48]*z[1];
    z[67]= - z[112] + z[67];
    z[67]=z[6]*z[67];
    z[113]= - z[104] - z[55];
    z[113]=z[9]*z[113];
    z[60]=z[67] + z[60] + z[54] + z[113];
    z[60]=z[11]*z[60];
    z[67]=3*z[55];
    z[113]= - z[92] - z[67];
    z[113]=z[113]*z[48]*z[50];
    z[114]= - z[69]*z[32];
    z[113]=z[114] + z[113];
    z[113]=z[5]*z[113];
    z[113]=7*z[64] + z[113];
    z[113]=z[5]*z[113];
    z[114]=z[108] + z[55];
    z[115]=z[114]*z[9];
    z[115]=z[115] + z[99];
    z[115]=z[115]*z[32];
    z[116]=z[71]*z[5];
    z[117]=2*z[48];
    z[117]= - z[116]*z[117];
    z[117]= - z[115] + z[117];
    z[117]=z[5]*z[117];
    z[118]=8*z[55];
    z[119]=n<T>(13,2)*z[28] + z[118];
    z[119]=z[9]*z[119];
    z[119]= - z[54] + z[119];
    z[119]=z[9]*z[119];
    z[117]=z[119] + z[117];
    z[117]=z[5]*z[117];
    z[119]=n<T>(3,2)*z[28] - z[58];
    z[119]=z[9]*z[119];
    z[117]=z[119] + z[117];
    z[117]=z[6]*z[117];
    z[119]=z[82] + z[18];
    z[119]=z[5]*z[119];
    z[120]=5*z[17];
    z[119]= - z[120] + z[119];
    z[119]=z[4]*z[119];
    z[24]=z[60] + z[119] + z[117] + z[24] + z[113] - z[28] - n<T>(9,2)*z[55]
   ;
    z[24]=z[24]*z[35];
    z[60]=z[17] + n<T>(3,2)*z[31];
    z[113]=z[39]*z[5];
    z[117]=z[113]*z[9];
    z[40]=z[52]*z[40];
    z[40]=z[40] - z[117] - z[60];
    z[40]=z[7]*z[40];
    z[119]=z[57] + z[31];
    z[119]=z[9]*z[119];
    z[119]= - z[82] + z[119];
    z[119]=z[5]*z[119];
    z[105]=z[119] + z[105] + z[31];
    z[105]=z[5]*z[105];
    z[26]=z[26] + z[55];
    z[40]=z[40] + z[105] + z[26];
    z[40]=z[40]*z[46];
    z[105]=2*z[55];
    z[119]= - n<T>(7,2)*z[28] - z[105];
    z[119]=z[9]*z[119];
    z[121]=5*z[28];
    z[122]= - z[121] - z[98];
    z[122]=z[122]*z[56];
    z[119]=z[119] + z[122];
    z[119]=z[119]*z[37];
    z[122]=z[121] + 9*z[55];
    z[119]=2*z[122] + z[119];
    z[119]=z[5]*z[119];
    z[122]= - z[9] - z[56];
    z[101]=z[101]*z[71]*z[122];
    z[122]=z[103] + 36*z[55];
    z[122]=z[9]*z[122];
    z[101]=z[101] - 11*z[17] + z[122];
    z[101]=z[5]*z[101];
    z[122]=7*z[55];
    z[123]=z[68] - z[122];
    z[101]=3*z[123] + z[101];
    z[101]=z[6]*z[101];
    z[53]= - z[54] - z[53];
    z[53]=z[9]*z[53];
    z[53]=z[82] + z[53];
    z[53]=z[5]*z[53];
    z[53]= - 2*z[73] + z[53];
    z[53]=z[5]*z[53];
    z[53]= - z[91] + z[47] + z[53];
    z[25]=z[53]*z[25];
    z[24]=z[24] + z[25] + z[101] + z[40] - n<T>(15,2)*z[1] + z[119];
    z[24]=z[11]*z[24];
    z[25]=z[32]*z[7];
    z[40]=z[49] + z[25];
    z[53]=2*z[7];
    z[40]=z[53]*z[71]*z[40];
    z[54]=z[48]*z[72];
    z[54]= - z[115] + z[54];
    z[54]=z[6]*z[54];
    z[64]=z[17] - 4*z[64];
    z[64]=z[9]*z[64];
    z[54]=z[64] + z[54];
    z[54]=z[6]*z[54];
    z[64]=z[74] + z[67];
    z[74]= - z[9]*z[64];
    z[82]=7*z[17];
    z[74]= - z[82] + z[74];
    z[74]=z[9]*z[74];
    z[74]= - z[22] + z[74];
    z[74]=z[9]*z[74];
    z[91]=z[44] + z[55];
    z[101]=z[91]*z[9];
    z[115]= - z[99] - z[101];
    z[115]=z[9]*z[115];
    z[115]= - z[15] + z[115];
    z[115]=z[115]*z[25];
    z[74]=z[74] + z[115];
    z[74]=z[74]*z[53];
    z[82]= - z[82] - 4*z[70];
    z[82]=z[9]*z[82];
    z[74]=z[74] - z[16] + z[82];
    z[74]=z[4]*z[74];
    z[40]=z[74] + z[40] + z[54];
    z[40]=z[10]*z[40];
    z[54]=z[25]*z[61];
    z[74]=z[67] + z[68];
    z[82]=z[74]*z[9];
    z[115]= - z[82] - z[54];
    z[115]=z[115]*z[53];
    z[119]=z[28]*z[32];
    z[48]= - z[48]*z[62];
    z[48]=z[119] + z[48];
    z[48]=z[6]*z[48];
    z[48]=z[48] + z[17] + z[66];
    z[48]=z[48]*z[90];
    z[66]=z[121] + z[67];
    z[119]=z[9]*z[66];
    z[119]=z[30] + z[119];
    z[119]=z[9]*z[119];
    z[123]=z[71]*z[25];
    z[119]=z[119] + z[123];
    z[119]=z[119]*z[53];
    z[82]=z[119] + z[30] + z[82];
    z[82]=z[4]*z[82];
    z[89]= - z[11]*z[89];
    z[40]=z[40] + z[89] + 2*z[82] + z[48] - z[45] + z[115];
    z[40]=z[10]*z[40];
    z[48]= - z[61]*z[32]*z[53];
    z[82]= - z[108] - z[122];
    z[82]=z[9]*z[82];
    z[48]=z[82] + z[48];
    z[48]=z[7]*z[48];
    z[48]=z[48] - z[76] - z[28] + z[58];
    z[48]=z[4]*z[48];
    z[82]= - 4*z[45] - z[44] - z[105];
    z[82]=z[7]*z[82];
    z[89]=z[44] + z[45];
    z[58]= - z[58] - z[89];
    z[115]=z[32]*z[1];
    z[119]=z[6]*z[112];
    z[119]=z[115] + z[119];
    z[119]=z[6]*z[119];
    z[58]=3*z[58] + z[119];
    z[58]=z[6]*z[58];
    z[119]=z[18] + z[15];
    z[79]=z[119]*z[79];
    z[73]=z[79] - 6*z[73];
    z[73]=z[4]*z[73];
    z[73]=z[45] + z[73];
    z[73]=z[11]*z[73];
    z[79]=z[5]*z[47];
    z[40]=z[40] + z[73] + z[48] + z[58] + z[79] + z[82];
    z[40]=z[10]*z[40];
    z[48]= - z[17] - z[34];
    z[48]=z[48]*z[37];
    z[48]=z[48] + z[61];
    z[48]=z[48]*z[50];
    z[48]=z[1] + z[48];
    z[48]=z[48]*z[81];
    z[58]=z[45] + z[28];
    z[58]=z[58]*z[5];
    z[73]=n<T>(3,4)*z[15];
    z[79]=npow(z[7],2);
    z[73]= - z[79]*z[73];
    z[73]=z[28] + z[73];
    z[73]=z[7]*z[73];
    z[73]=z[58] + z[73];
    z[73]=z[6]*z[73];
    z[82]=z[31] - z[17];
    z[122]=z[82]*z[9];
    z[123]=z[122] + z[15];
    z[124]=z[123]*z[79];
    z[125]=z[20]*z[79];
    z[125]=z[17] + z[125];
    z[125]=z[6]*z[125];
    z[124]=z[125] + z[28] + z[124];
    z[124]=z[7]*z[124];
    z[125]=z[99] + z[31];
    z[125]=z[9]*z[125];
    z[125]=z[16] + z[125];
    z[125]=z[125]*z[42];
    z[125]=z[28] + z[125];
    z[125]=z[125]*z[81];
    z[124]=z[125] + z[124];
    z[124]=z[4]*z[124];
    z[59]= - z[55] - z[59];
    z[59]=z[59]*z[52];
    z[59]=z[1] + z[59];
    z[59]=z[7]*z[59];
    z[48]=n<T>(1,2)*z[124] + z[73] + z[48] + z[59];
    z[48]=z[4]*z[48];
    z[54]=z[70] + z[54];
    z[54]=z[54]*z[53];
    z[59]=z[30] + z[101];
    z[59]=z[59]*z[49];
    z[73]= - z[32]*z[72];
    z[59]=z[59] + z[73];
    z[59]=z[6]*z[59];
    z[73]=z[55] - z[28];
    z[73]=z[73]*z[49];
    z[73]=z[73] - z[120];
    z[59]=z[59] + z[73];
    z[59]=z[6]*z[59];
    z[73]= - z[4]*z[73];
    z[54]=z[73] + z[54] + z[59];
    z[54]=z[10]*z[54];
    z[59]=z[32]*z[62];
    z[59]= - z[70] + z[59];
    z[59]=z[59]*z[90];
    z[59]=z[59] + z[121] - z[65];
    z[59]=z[6]*z[59];
    z[70]= - z[118] - z[89];
    z[70]=z[7]*z[70];
    z[65]=z[65] - z[89];
    z[65]=z[4]*z[65];
    z[54]=z[54] + z[65] + z[70] + z[59];
    z[54]=z[10]*z[54];
    z[59]=z[86] + z[110];
    z[65]= - 2*z[74] + z[45];
    z[65]=z[7]*z[65];
    z[59]=2*z[59] + z[65];
    z[59]=z[7]*z[59];
    z[65]=z[92] - z[45];
    z[65]=z[5]*z[65];
    z[70]= - z[68] - z[45];
    z[73]= - z[6]*z[115];
    z[70]=2*z[70] + z[73];
    z[70]=z[6]*z[70];
    z[65]=z[70] - z[86] + z[65];
    z[65]=z[6]*z[65];
    z[58]=z[86] + z[58];
    z[58]=z[4]*z[58];
    z[54]=z[54] + z[58] + z[59] + z[65];
    z[54]=z[10]*z[54];
    z[58]=z[72] - z[64];
    z[58]=z[9]*z[58];
    z[58]= - z[120] + z[58];
    z[58]=z[6]*z[58];
    z[58]=z[58] + z[66];
    z[58]=z[6]*z[58];
    z[25]=z[1]*z[25];
    z[25]= - z[55] + z[25];
    z[25]=z[25]*z[53];
    z[59]= - z[4]*z[66];
    z[25]=z[59] + z[25] + z[58];
    z[25]=z[10]*z[25];
    z[49]= - z[49]*z[62];
    z[49]=z[49] + z[114];
    z[49]=z[6]*z[49];
    z[58]=z[110] + z[1];
    z[49]=z[49] - z[58];
    z[49]=z[6]*z[49];
    z[59]=z[45] - z[83];
    z[59]=z[7]*z[59];
    z[59]=z[75] + z[59];
    z[59]=z[7]*z[59];
    z[58]=z[4]*z[58];
    z[25]=z[25] + z[58] + z[59] + z[49];
    z[25]=z[10]*z[25];
    z[49]=z[42]*z[28];
    z[58]=z[28]*z[6];
    z[59]=z[5]*z[68];
    z[59]=z[59] - z[58];
    z[59]=z[6]*z[59];
    z[59]=z[49] + z[59];
    z[59]=z[6]*z[59];
    z[62]=z[79]*z[75];
    z[64]= - z[4]*z[49];
    z[25]=z[25] + z[64] + z[62] + z[59];
    z[25]=z[10]*z[25];
    z[59]= - z[42]*z[95]*z[68];
    z[62]= - z[72] + z[61];
    z[62]=z[6]*z[62];
    z[62]= - z[1] + z[62];
    z[62]=z[6]*z[62];
    z[64]=z[4]*z[1];
    z[62]=z[62] + z[64];
    z[62]=z[10]*z[62];
    z[64]=z[28]*z[95];
    z[62]=z[64] + z[62];
    z[62]=z[62]*z[87];
    z[59]=z[59] + z[62];
    z[59]=z[8]*z[59];
    z[62]= - z[42]*z[108];
    z[64]= - z[28] - 6*z[113];
    z[65]=z[6]*z[5];
    z[64]=z[64]*z[65];
    z[62]=z[62] + z[64];
    z[62]=z[62]*z[94];
    z[64]=z[4]*z[6];
    z[49]= - z[64]*z[49];
    z[25]=z[59] + z[25] + z[62] + z[49];
    z[25]=z[8]*z[25];
    z[49]= - 19*z[28] - 11*z[55];
    z[59]= - z[111]*z[37];
    z[49]=n<T>(1,2)*z[49] + z[59];
    z[49]=z[5]*z[49];
    z[49]= - z[1] + z[49];
    z[49]=z[5]*z[49];
    z[59]= - 6*z[117] - z[30] + z[31];
    z[59]=z[6]*z[59];
    z[62]= - z[99] - 4*z[31];
    z[62]=z[5]*z[62];
    z[62]= - z[44] + z[62];
    z[59]=2*z[62] + z[59];
    z[59]=z[59]*z[65];
    z[49]=z[49] + z[59];
    z[49]=z[6]*z[49];
    z[59]=z[79]*z[1];
    z[25]=z[25] + z[54] + z[48] + n<T>(9,2)*z[59] + z[49];
    z[25]=z[8]*z[25];
    z[34]=9*z[17] - z[34];
    z[48]= - n<T>(1,4)*z[17] - z[31];
    z[48]=z[9]*z[48]*z[37];
    z[36]=z[57] + z[36];
    z[36]=z[9]*z[36];
    z[36]= - n<T>(13,2)*z[15] + z[36];
    z[36]=z[36]*z[52];
    z[34]=z[36] + n<T>(1,2)*z[34] + z[48];
    z[34]=z[7]*z[34];
    z[36]=z[97] + z[31];
    z[36]=z[9]*z[36];
    z[36]= - n<T>(1,4)*z[15] + z[36];
    z[36]=z[5]*z[36];
    z[36]=z[57] + z[36];
    z[36]=z[36]*z[37];
    z[34]=z[34] + z[68] + z[36];
    z[34]=z[7]*z[34];
    z[36]=3*z[31];
    z[41]= - z[41] - z[36];
    z[41]=z[9]*z[41];
    z[48]=4*z[17] + z[31];
    z[48]=z[9]*z[48];
    z[27]=z[27] + z[48];
    z[27]=z[9]*z[27];
    z[27]=4*z[20] + z[27];
    z[27]=z[27]*z[37];
    z[27]=z[27] - 14*z[15] + z[41];
    z[27]=z[5]*z[27];
    z[30]=z[30] + z[31];
    z[27]=3*z[30] + z[27];
    z[27]=z[5]*z[27];
    z[41]=n<T>(7,2)*z[17];
    z[36]=z[41] - z[36];
    z[36]=z[9]*z[36];
    z[48]= - z[9]*z[123];
    z[48]=z[20] + z[48];
    z[48]=z[48]*z[46];
    z[36]=z[48] - n<T>(7,2)*z[15] + z[36];
    z[36]=z[7]*z[36];
    z[36]= - 3*z[82] + z[36];
    z[36]=z[7]*z[36];
    z[48]=z[23]*z[46];
    z[48]= - n<T>(7,2)*z[20] + z[48];
    z[48]=z[7]*z[48];
    z[48]=z[16] + z[48];
    z[48]=z[7]*z[48];
    z[48]= - z[57] + z[48];
    z[48]=z[6]*z[48];
    z[27]=z[48] + z[36] - z[47] + z[27];
    z[27]=z[4]*z[27];
    z[36]=93*z[17] + 29*z[31];
    z[38]= - z[9]*z[38];
    z[38]= - z[43] + z[38];
    z[38]=z[5]*z[38];
    z[36]=n<T>(1,4)*z[36] + z[38];
    z[36]=z[5]*z[36];
    z[36]= - z[121] + z[36];
    z[36]=z[5]*z[36];
    z[38]=8*z[15] - n<T>(25,4)*z[84];
    z[38]=z[7]*z[38];
    z[38]=z[57] + z[38];
    z[38]=z[7]*z[38];
    z[38]=z[38] - n<T>(11,2)*z[28] - z[76];
    z[38]=z[6]*z[38];
    z[27]=z[27] + z[38] + z[34] - 8*z[1] + z[36];
    z[27]=z[4]*z[27];
    z[34]= - z[44] + z[51];
    z[36]=z[17]*z[4];
    z[29]=n<T>(7,2)*z[29] + n<T>(3,2)*z[34] + z[36];
    z[29]=z[11]*z[29];
    z[34]= - z[7]*z[44];
    z[34]=z[1] + z[34];
    z[38]= - z[4]*z[68];
    z[29]=z[29] + n<T>(1,2)*z[34] + z[38];
    z[29]=z[29]*z[35];
    z[34]=z[86] - z[88];
    z[34]=z[34]*z[52];
    z[38]=n<T>(1,2)*z[1];
    z[43]=z[38]*z[79];
    z[48]= - z[8]*z[43];
    z[49]=z[36] - z[28];
    z[54]= - z[2] - z[4];
    z[49]=z[49]*z[54];
    z[49]= - z[1] + z[49];
    z[54]=3*z[2];
    z[49]=z[49]*z[54];
    z[29]=z[49] + z[48] + z[34] + z[29];
    z[29]=z[3]*z[29];
    z[34]=npow(z[7],3);
    z[48]=z[34]*z[75];
    z[49]= - z[7]*z[93];
    z[49]=z[49] - n<T>(9,2)*z[1];
    z[62]=z[79]*z[11];
    z[49]=z[49]*z[62];
    z[48]=z[48] + z[49];
    z[49]=npow(z[11],2);
    z[48]=z[48]*z[49];
    z[65]=4*z[1] + z[102];
    z[65]=z[65]*z[79];
    z[47]= - z[47] - z[106];
    z[47]=z[7]*z[47];
    z[47]= - 6*z[1] + z[47];
    z[47]=z[11]*z[7]*z[47];
    z[47]=z[65] + z[47];
    z[47]=z[11]*z[47];
    z[65]=z[34]*z[1];
    z[47]= - n<T>(1,2)*z[65] + z[47];
    z[47]=z[3]*z[11]*z[47];
    z[66]=z[13]*z[3];
    z[70]= - n<T>(1,2)*z[66] + 1;
    z[72]=npow(z[11],3);
    z[70]=z[72]*z[65]*z[70];
    z[73]=z[86] + z[102];
    z[62]=z[73]*z[62];
    z[62]= - z[65] + z[62];
    z[49]=z[49]*z[3];
    z[62]=z[62]*z[49];
    z[62]=z[62] + z[70];
    z[62]=z[13]*z[62];
    z[47]=z[62] + z[48] + z[47];
    z[47]=z[13]*z[47];
    z[26]= - z[7]*z[26];
    z[26]= - n<T>(11,2)*z[1] + z[26];
    z[26]=z[26]*z[79];
    z[48]=z[91]*z[46];
    z[48]=z[109] + z[48];
    z[48]=z[11]*z[48]*z[52];
    z[26]=z[26] + z[48];
    z[26]=z[11]*z[26];
    z[48]= - z[75] - z[102];
    z[48]=z[7]*z[48];
    z[62]=z[108] + z[51];
    z[46]=z[62]*z[46];
    z[46]=n<T>(7,2)*z[1] + z[46];
    z[46]=z[11]*z[46];
    z[46]=4*z[48] + z[46];
    z[46]=z[11]*z[46];
    z[46]=z[59] + z[46];
    z[46]=z[3]*z[46];
    z[26]=z[46] + z[65] + z[26];
    z[26]=z[11]*z[26];
    z[26]=z[47] + z[26];
    z[46]=3*z[13];
    z[26]=z[26]*z[46];
    z[47]=z[98] - z[107];
    z[47]=z[7]*z[47];
    z[47]=z[109] + z[47];
    z[47]=z[7]*z[47];
    z[48]= - z[92] + z[67];
    z[48]=z[7]*z[48];
    z[48]=z[86] + z[48];
    z[48]=z[11]*z[48];
    z[47]=z[47] + n<T>(1,2)*z[48];
    z[47]=z[11]*z[47];
    z[48]= - z[55]*z[52];
    z[48]= - z[1] + z[48];
    z[48]=z[48]*z[79];
    z[47]=z[48] + z[47];
    z[47]=z[47]*z[35];
    z[48]= - z[92] - 6*z[51];
    z[48]=z[11]*z[48];
    z[51]=5*z[1];
    z[48]=z[48] + z[51] + n<T>(13,2)*z[102];
    z[48]=z[11]*z[48];
    z[52]= - z[7]*z[1];
    z[48]=z[52] + z[48];
    z[48]=z[48]*z[35];
    z[43]= - z[43] + z[48];
    z[43]=z[3]*z[43];
    z[26]=z[26] + z[47] + z[43];
    z[26]=z[13]*z[26];
    z[36]=z[36]*z[6];
    z[36]=z[36] - z[58];
    z[43]= - z[7]*z[18];
    z[43]=z[99] + z[43];
    z[43]=z[7]*z[43];
    z[43]=z[44] + z[43];
    z[43]=z[7]*z[43];
    z[43]= - z[1] + z[43];
    z[44]= - z[11]*z[44];
    z[43]=z[44] + n<T>(1,2)*z[43] - z[36];
    z[43]=z[11]*z[43];
    z[34]=z[39]*z[34];
    z[39]=z[68]*z[64];
    z[34]=z[43] + n<T>(1,2)*z[34] + z[39];
    z[34]=z[34]*z[35];
    z[39]=z[99]*z[64];
    z[43]= - z[6]*z[121];
    z[39]=z[43] + z[39];
    z[39]=z[4]*z[39];
    z[36]=z[36]*z[54];
    z[43]=z[6]*z[51];
    z[36]=z[36] + z[43] + z[39];
    z[36]=z[2]*z[36];
    z[39]=z[75] - n<T>(5,4)*z[102];
    z[39]=z[39]*z[79];
    z[26]=z[26] + z[29] + z[36] + z[39] + z[34];
    z[26]=z[13]*z[26];
    z[15]=z[15]*z[9];
    z[15]=z[15] + z[20];
    z[29]=z[5]*z[15];
    z[18]=z[29] - z[22] - n<T>(9,4)*z[18];
    z[18]=z[18]*z[81];
    z[20]= - z[9]*z[20];
    z[20]= - z[23] + z[20];
    z[20]=z[5]*z[20];
    z[15]=3*z[15] + z[20];
    z[15]=z[15]*z[81];
    z[15]=z[16] + z[15];
    z[15]=z[7]*z[15];
    z[15]=n<T>(1,4)*z[15] - n<T>(9,4)*z[17] + z[18];
    z[15]=z[7]*z[15];
    z[18]= - z[5]*z[119];
    z[18]=z[18] + z[60];
    z[18]=z[18]*z[81];
    z[18]=z[28] + z[18];
    z[15]=n<T>(3,2)*z[18] + z[15];
    z[15]=z[7]*z[15];
    z[18]=z[33]*z[53];
    z[18]=z[18] - 4*z[117] - z[30];
    z[18]=z[5]*z[18];
    z[20]=z[32]*z[113];
    z[20]=z[122] - 2*z[20];
    z[20]=z[5]*z[20];
    z[20]=z[31] + z[20];
    z[20]=z[6]*z[20];
    z[18]=z[20] + z[28] + z[18];
    z[18]=z[6]*z[18];
    z[20]= - z[103] - 23*z[55];
    z[20]=z[9]*z[20];
    z[20]= - n<T>(13,2)*z[17] + z[20];
    z[23]=z[9]*z[116];
    z[20]=n<T>(1,2)*z[20] - 6*z[23];
    z[20]=z[5]*z[20];
    z[20]=z[20] + 14*z[28] + n<T>(31,2)*z[55];
    z[20]=z[5]*z[20];
    z[15]=z[18] + z[15] - z[51] + z[20];
    z[15]=z[6]*z[15];
    z[18]= - z[71]*z[81];
    z[20]=z[8]*z[1];
    z[18]= - z[78] + z[20] + z[18] - z[69];
    z[18]=z[4]*z[18];
    z[23]= - 2*z[82] + z[77];
    z[23]=z[11]*z[23];
    z[29]=z[8]*z[75];
    z[23]=z[29] + z[23] - z[108] + z[55];
    z[23]=z[2]*z[23];
    z[29]=z[28] - z[20];
    z[20]= - z[68] + z[20];
    z[20]=z[8]*z[20];
    z[20]=z[17] + z[20];
    z[20]=z[2]*z[20];
    z[20]=2*z[29] + z[20];
    z[20]=z[3]*z[20];
    z[20]=z[23] + z[20];
    z[23]=z[5]*z[61];
    z[23]= - z[1] + z[23];
    z[18]=5*z[23] + 6*z[20] + z[18];
    z[18]=z[14]*z[18];
    z[20]=z[32]*z[86];
    z[23]= - z[1]*z[63];
    z[23]=z[112] + z[23];
    z[23]=z[23]*z[50];
    z[20]=z[20] + z[23];
    z[20]=z[5]*z[20];
    z[20]=z[20] + z[104] - z[67];
    z[20]=z[11]*z[20];
    z[23]= - z[42]*z[112];
    z[23]=z[80] + z[23];
    z[23]=z[5]*z[23];
    z[20]=z[20] - z[51] + z[23];
    z[20]=z[11]*z[20];
    z[23]= - z[1]*z[56];
    z[23]= - z[55] + z[23];
    z[23]=z[5]*z[23];
    z[23]=z[86] + z[23];
    z[23]=z[23]*z[50];
    z[20]=z[23] + z[20];
    z[20]=z[20]*z[35];
    z[23]= - z[11]*z[57];
    z[23]=z[28] + z[23];
    z[23]=z[23]*z[35];
    z[23]= - z[38] + z[23];
    z[23]=z[23]*z[35];
    z[29]= - z[108] + z[85];
    z[30]=z[10]*z[11];
    z[29]=z[29]*z[30];
    z[23]=z[23] + z[29];
    z[23]=z[3]*z[23];
    z[29]= - n<T>(3,2)*z[66] - n<T>(5,2);
    z[29]=z[72]*z[1]*z[29];
    z[32]=z[11]*z[28];
    z[32]= - z[1] + z[32];
    z[32]=z[32]*z[49];
    z[29]=3*z[32] + z[29];
    z[29]=z[29]*z[46];
    z[32]=z[5]*z[108];
    z[33]= - z[10]*z[45];
    z[32]=z[32] + z[33];
    z[30]=z[32]*z[30];
    z[32]= - z[5] + z[3];
    z[32]=z[14]*z[1]*z[32];
    z[20]=5*z[32] + z[29] + z[23] + z[20] + z[30];
    z[20]=z[12]*z[20];
    z[16]=z[16] - z[96];
    z[16]=z[5]*z[16];
    z[16]=n<T>(5,4)*z[16] - n<T>(11,4)*z[17] + z[31];
    z[16]=z[7]*z[16];
    z[17]=z[5]*z[22];
    z[17]= - z[41] + z[17];
    z[17]=z[5]*z[17];
    z[16]=z[16] + z[17] - z[100] - z[105];
    z[16]=z[7]*z[16];
    z[17]= - z[104] + z[45];
    z[17]=z[17]*z[50];
    z[16]=z[16] + z[51] + z[17];
    z[16]=z[7]*z[16];
    z[17]= - n<T>(9,2)*z[28] - z[55];
    z[17]=z[9]*z[17]*z[50];
    z[17]=z[17] - n<T>(11,4)*z[28] - z[55];
    z[17]=z[5]*z[17];
    z[17]=z[38] + z[17];
    z[17]=z[17]*z[37];

    r += z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[24]
       + z[25] + z[26] + z[27] + z[40];
 
    return r;
}

template double qqb_2lNLC_r706(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r706(const std::array<dd_real,31>&);
#endif
