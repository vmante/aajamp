#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r1125(const std::array<T,31>& k) {
  T z[125];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[17];
    z[5]=k[19];
    z[6]=k[12];
    z[7]=k[22];
    z[8]=k[13];
    z[9]=k[14];
    z[10]=k[11];
    z[11]=k[3];
    z[12]=k[6];
    z[13]=k[9];
    z[14]=k[5];
    z[15]=k[8];
    z[16]=k[15];
    z[17]=k[2];
    z[18]=k[4];
    z[19]=k[16];
    z[20]=z[18]*z[10];
    z[21]=npow(z[1],4);
    z[22]=z[20]*z[21];
    z[23]=npow(z[1],5);
    z[24]=z[23]*z[10];
    z[25]=z[22] + z[24];
    z[26]=z[21]*z[18];
    z[27]=z[23] + z[26];
    z[27]=z[2]*z[27];
    z[27]=z[27] - z[25];
    z[27]=z[3]*z[27];
    z[28]=z[10]*z[9];
    z[29]=npow(z[1],6);
    z[30]= - z[29]*z[28];
    z[31]=z[29]*z[2];
    z[32]=z[29]*z[10];
    z[33]=z[31] - z[32];
    z[34]= - z[3]*z[33];
    z[31]=z[9]*z[31];
    z[30]=z[34] + z[30] + z[31];
    z[30]=z[5]*z[30];
    z[31]=z[9]*z[25];
    z[34]=z[21]*z[9];
    z[35]=z[34]*z[18];
    z[36]= - z[9]*z[23];
    z[36]=z[36] - z[35];
    z[36]=z[2]*z[36];
    z[27]=z[30] + z[27] + z[36] + z[31];
    z[27]=z[4]*z[27];
    z[30]=n<T>(4,3)*z[9];
    z[31]=z[30]*z[29];
    z[31]=z[31] + z[23];
    z[36]= - z[31]*z[28];
    z[31]=z[31]*z[9];
    z[37]=z[2]*z[31];
    z[38]=z[23]*z[2];
    z[38]=z[38] - z[24];
    z[39]= - z[3]*z[38];
    z[36]=z[39] + z[36] + z[37];
    z[37]=n<T>(1,3)*z[5];
    z[36]=z[36]*z[37];
    z[39]=z[30]*z[23];
    z[39]=z[39] + z[21];
    z[40]=z[39]*z[28];
    z[41]=npow(z[9],2);
    z[42]=z[41]*z[22];
    z[40]=z[40] + n<T>(4,3)*z[42];
    z[39]=z[39]*z[9];
    z[42]=npow(z[1],3);
    z[43]=11*z[42];
    z[44]=z[43] + 4*z[34];
    z[45]=n<T>(1,3)*z[18];
    z[46]= - z[9]*z[44]*z[45];
    z[35]= - z[2]*z[35];
    z[35]=z[35] - z[39] + z[46];
    z[46]=n<T>(1,3)*z[2];
    z[35]=z[35]*z[46];
    z[47]=z[26]*z[2];
    z[48]=z[42]*z[18];
    z[49]=z[47] + z[21] + n<T>(11,3)*z[48];
    z[49]=z[49]*z[46];
    z[50]=z[21]*z[10];
    z[51]=n<T>(1,3)*z[50];
    z[52]= - z[8]*z[48];
    z[49]=z[49] - z[51] + z[52];
    z[49]=z[3]*z[49];
    z[52]=z[48]*z[9];
    z[53]=z[8]*z[52];
    z[27]=n<T>(4,9)*z[27] + z[36] + z[49] + z[35] + n<T>(1,3)*z[40] + z[53];
    z[27]=z[4]*z[27];
    z[35]=n<T>(4,3)*z[42];
    z[36]=z[35] - z[39];
    z[36]=z[9]*z[36];
    z[40]=n<T>(1,3)*z[9];
    z[44]= - z[44]*z[40];
    z[49]=npow(z[1],2);
    z[53]=2*z[49];
    z[44]=z[53] + z[44];
    z[54]=z[18]*z[9];
    z[44]=z[44]*z[54];
    z[47]= - z[41]*z[47];
    z[36]=z[47] + z[36] + z[44];
    z[36]=z[36]*z[46];
    z[44]=4*z[49];
    z[47]= - z[20]*z[44];
    z[55]=7*z[49];
    z[56]=z[42]*z[10];
    z[47]=z[47] - z[55] - 4*z[56];
    z[57]=n<T>(1,3)*z[8];
    z[58]=z[48]*z[57];
    z[59]=z[49]*z[18];
    z[58]=z[58] + z[42] + n<T>(2,3)*z[59];
    z[58]=z[8]*z[58];
    z[60]=n<T>(2,3)*z[42];
    z[61]= - z[60] - z[59];
    z[62]=n<T>(2,3)*z[2];
    z[61]=z[61]*z[62];
    z[47]=z[61] + n<T>(1,9)*z[47] + z[58];
    z[47]=z[3]*z[47];
    z[58]=z[21]*z[41];
    z[58]=z[49] + z[58];
    z[30]=z[58]*z[20]*z[30];
    z[35]=z[35] + z[39];
    z[35]=z[35]*z[28];
    z[39]=z[49]*z[9];
    z[30]=z[30] + n<T>(7,3)*z[39] + z[35];
    z[35]=n<T>(2,3)*z[49];
    z[58]=z[42]*z[9];
    z[61]= - z[35] + z[58];
    z[61]=z[61]*z[54];
    z[63]= - z[57]*z[52];
    z[61]=z[63] - z[58] + z[61];
    z[61]=z[8]*z[61];
    z[63]=z[2]*z[9];
    z[63]= - z[28] + z[63];
    z[64]=n<T>(1,3)*z[21];
    z[31]=z[64] + z[31];
    z[31]=z[31]*z[63];
    z[63]=z[21]*z[2];
    z[63]=z[63] - z[50];
    z[65]=n<T>(1,3)*z[3];
    z[66]= - z[63]*z[65];
    z[31]=z[66] + z[31];
    z[31]=z[31]*z[37];
    z[27]=z[27] + z[31] + z[47] + z[36] + n<T>(1,3)*z[30] + z[61];
    z[27]=z[4]*z[27];
    z[25]=z[25] - z[21];
    z[30]=z[21]*z[14];
    z[31]=z[30] - z[23];
    z[36]= - z[2]*z[31];
    z[36]=z[36] - z[25];
    z[36]=z[6]*z[36];
    z[47]=z[2]*z[8];
    z[61]=z[29]*z[47];
    z[33]= - z[6]*z[33];
    z[66]= - z[8]*z[32];
    z[33]=z[33] + z[66] + z[61];
    z[33]=z[5]*z[33];
    z[61]=z[8]*z[25];
    z[66]=z[31]*z[47];
    z[33]=z[33] + z[36] + z[61] + z[66];
    z[33]=z[7]*z[33];
    z[36]=n<T>(4,3)*z[8];
    z[25]=z[25]*z[36];
    z[61]=z[42]*z[14];
    z[66]=z[61] + z[21];
    z[67]=z[66]*z[9];
    z[68]=n<T>(1,3)*z[42];
    z[25]=z[25] - 4*z[67] + z[68] + z[50];
    z[69]=z[8]*z[25];
    z[70]=2*z[21];
    z[71]= - z[70] + n<T>(11,3)*z[61];
    z[31]=z[31]*z[36];
    z[72]=z[71] + z[31];
    z[72]=z[72]*z[8];
    z[73]=z[30]*z[47];
    z[73]=z[72] + z[73];
    z[73]=z[2]*z[73];
    z[69]=z[69] + z[73];
    z[73]=z[30]*z[2];
    z[71]= - z[73] - z[71];
    z[71]=z[71]*z[46];
    z[71]=z[71] - z[51] - n<T>(1,9)*z[42] + z[67];
    z[71]=z[6]*z[71];
    z[32]=z[32]*z[36];
    z[24]=z[32] + z[24];
    z[24]=z[24]*z[8];
    z[29]=z[36]*z[29];
    z[29]=z[29] + z[23];
    z[32]=z[29]*z[47];
    z[36]= - z[6]*z[38];
    z[32]=z[36] - z[24] + z[32];
    z[32]=z[32]*z[37];
    z[32]=n<T>(4,9)*z[33] + z[32] + n<T>(1,3)*z[69] + z[71];
    z[32]=z[7]*z[32];
    z[25]=z[25]*z[57];
    z[33]=z[49]*z[14];
    z[36]=n<T>(2,3)*z[67] + z[68] + z[33];
    z[36]=z[9]*z[36];
    z[38]=z[20]*z[49];
    z[69]=z[38] + z[56];
    z[71]=z[69] + z[49];
    z[25]=z[25] + z[36] + n<T>(4,9)*z[71];
    z[25]=z[8]*z[25];
    z[36]= - z[67] - z[42] - 2*z[33];
    z[36]=z[9]*z[36];
    z[60]=z[60] - z[33];
    z[71]=2*z[2];
    z[74]= - z[60]*z[71];
    z[75]=n<T>(4,3)*z[49];
    z[36]=z[74] - z[75] + z[36] - n<T>(4,3)*z[69];
    z[69]=n<T>(1,3)*z[6];
    z[36]=z[36]*z[69];
    z[24]= - z[51] - z[24];
    z[24]=z[8]*z[24];
    z[29]=z[8]*z[29];
    z[29]=z[64] + z[29];
    z[29]=z[29]*z[47];
    z[47]= - z[63]*z[69];
    z[24]=z[47] + z[24] + z[29];
    z[24]=z[24]*z[37];
    z[29]=2*z[60] + z[72];
    z[29]=z[8]*z[29];
    z[47]=npow(z[8],2);
    z[51]=z[47]*z[73];
    z[29]=z[29] + z[51];
    z[29]=z[29]*z[46];
    z[24]=z[32] + z[24] + z[36] + z[25] + z[29];
    z[24]=z[7]*z[24];
    z[24]=z[24] + z[27];
    z[25]=npow(z[14],2);
    z[22]=z[22]*z[25];
    z[27]=npow(z[14],3);
    z[29]=z[50]*z[27];
    z[32]= - z[29] - z[22];
    z[32]=z[18]*z[32];
    z[36]=z[27]*z[26];
    z[50]=z[14]*z[1];
    z[51]=npow(z[50],4);
    z[36]=z[51] + z[36];
    z[36]=z[8]*z[36];
    z[60]=z[8]*z[10];
    z[63]=z[12]*npow(z[50],5);
    z[64]= - z[63]*z[60];
    z[72]=z[51]*z[10];
    z[32]=z[64] + z[36] - z[72] + z[32];
    z[36]=npow(z[12],4);
    z[32]=z[32]*z[36];
    z[64]=z[50] - z[49];
    z[73]= - z[14]*z[64];
    z[73]=z[42] + z[73];
    z[73]=z[10]*z[73];
    z[74]=2*z[50];
    z[76]=z[74] + z[49];
    z[77]= - z[76]*z[20];
    z[73]=z[73] + z[77];
    z[73]=z[18]*z[73];
    z[77]=z[25]*z[10];
    z[78]=z[76]*z[77];
    z[32]=z[32] + z[73] + z[42] - z[78];
    z[73]=z[53] + z[50];
    z[79]=z[73]*z[14];
    z[79]=z[79] + z[42];
    z[80]=z[18]*z[76];
    z[80]=z[80] + z[79];
    z[80]=z[18]*z[80];
    z[81]=z[25]*z[2];
    z[82]=z[81] - z[14];
    z[79]=z[79]*z[82];
    z[36]=z[36]*z[21];
    z[82]=npow(z[18],2);
    z[83]=z[36]*z[82];
    z[84]=z[25]*z[83];
    z[79]=z[84] + z[80] + z[79];
    z[79]=z[79]*z[69];
    z[80]=z[18]*z[1];
    z[84]=n<T>(1,3)*z[80];
    z[85]=z[84] + n<T>(1,3)*z[76];
    z[85]=z[25]*z[85];
    z[86]=z[50] + z[49];
    z[87]= - z[14]*z[86];
    z[87]= - z[68] + z[87];
    z[87]=z[87]*z[77];
    z[85]=z[87] + z[85];
    z[85]=z[8]*z[85];
    z[73]=z[73] - z[80];
    z[73]=z[46]*z[25]*z[73];
    z[32]=z[79] + z[73] + z[85] + n<T>(1,3)*z[32];
    z[32]=z[15]*z[32];
    z[73]=z[61]*z[20];
    z[79]=z[56]*z[25];
    z[85]=z[79] + z[73];
    z[85]=z[85]*z[45];
    z[87]=z[27]*z[42];
    z[88]=z[25]*z[48];
    z[88]= - z[87] - n<T>(4,3)*z[88];
    z[88]=z[8]*z[88];
    z[89]=z[56]*z[27];
    z[90]=z[51]*z[12];
    z[91]=n<T>(5,3)*z[90];
    z[60]=z[60]*z[91];
    z[60]=z[60] + z[88] + z[89] + z[85];
    z[85]=npow(z[12],3);
    z[60]=z[60]*z[85];
    z[88]=4*z[14];
    z[81]= - 4*z[81] + z[88];
    z[81]=z[86]*z[81];
    z[74]=z[74] - z[49];
    z[86]= - z[80] + z[74];
    z[86]=z[18]*z[86];
    z[81]=z[86] + z[81];
    z[86]=z[25]*z[42];
    z[92]= - z[61]*z[45];
    z[92]=z[86] + z[92];
    z[92]=z[18]*z[92]*z[85];
    z[81]=n<T>(1,3)*z[81] + z[92];
    z[81]=z[6]*z[81];
    z[92]=z[84] - z[49] - n<T>(2,3)*z[50];
    z[92]=z[92]*z[20];
    z[93]=z[50]*z[18];
    z[94]=z[25]*z[1];
    z[78]= - n<T>(2,3)*z[93] - z[94] + n<T>(5,3)*z[78];
    z[78]=z[8]*z[78];
    z[95]=z[76]*z[14];
    z[93]= - z[95] + z[93];
    z[62]=z[93]*z[62];
    z[93]=z[77]*z[1];
    z[32]=2*z[32] + z[81] + z[60] + z[62] + z[78] + z[92] - z[49] + 
    z[93];
    z[32]=z[15]*z[32];
    z[60]=n<T>(1,3)*z[49];
    z[62]=z[60]*z[25];
    z[78]=z[89]*z[12];
    z[81]=z[33]*z[18];
    z[92]= - n<T>(17,9)*z[78] + z[62] + z[81];
    z[92]=z[8]*z[92];
    z[96]= - z[77]*z[44];
    z[97]=z[33]*z[10];
    z[98]= - z[97] - z[38];
    z[98]=z[18]*z[98];
    z[96]=z[96] + z[98];
    z[92]=n<T>(2,9)*z[96] + z[92];
    z[96]=npow(z[12],2);
    z[92]=z[92]*z[96];
    z[32]=z[92] + z[32];
    z[92]=z[25]*z[49];
    z[98]=z[53]*z[18];
    z[99]= - 5*z[33] + z[98];
    z[99]=z[18]*z[99];
    z[99]=5*z[92] + z[99];
    z[99]=z[99]*z[96];
    z[100]=z[60] + z[50];
    z[101]=z[68] + z[95];
    z[102]=n<T>(4,3)*z[2];
    z[101]=z[101]*z[102];
    z[99]=n<T>(2,9)*z[99] + z[101] - z[84] - z[100];
    z[99]=z[6]*z[99];
    z[101]=5*z[80] - z[44] - 11*z[50];
    z[101]=z[10]*z[101];
    z[103]=4*z[1];
    z[101]=z[103] + z[101];
    z[104]=z[14]*z[100];
    z[104]= - n<T>(4,9)*z[42] - 5*z[104];
    z[104]=z[10]*z[104];
    z[104]=z[104] + n<T>(2,3)*z[80];
    z[104]=z[8]*z[104];
    z[99]=z[99] + n<T>(1,9)*z[101] + z[104];
    z[84]= - z[84] + z[100];
    z[100]=2*z[42];
    z[101]=z[100] + z[59];
    z[104]=z[101]*z[57];
    z[84]=8*z[84] + z[104];
    z[84]=z[2]*z[84];
    z[104]= - z[8] + z[2];
    z[101]=z[65]*z[101]*z[104];
    z[32]=z[101] + z[84] + 4*z[99] + 8*z[32];
    z[32]=z[15]*z[32];
    z[84]=z[79] - z[73];
    z[84]=z[84]*z[45];
    z[99]=z[58]*z[27];
    z[91]=z[28]*z[91];
    z[84]=z[91] + z[84] - z[99] - z[89];
    z[84]=z[12]*z[84];
    z[89]=n<T>(1,3)*z[33];
    z[91]=z[89] + z[98];
    z[91]=z[18]*z[91];
    z[62]=z[84] + z[62] + z[91];
    z[62]=z[62]*z[96];
    z[22]=z[29] - z[22];
    z[22]=z[18]*z[22];
    z[29]=z[28]*z[63];
    z[84]= - z[9]*z[51];
    z[22]=z[29] + z[22] + z[84] - z[72];
    z[22]=z[12]*z[22];
    z[29]=z[61] + 2*z[48];
    z[29]=z[18]*z[29];
    z[29]= - z[86] + z[29];
    z[29]=z[18]*z[29];
    z[22]=z[22] + z[87] + z[29];
    z[22]=z[22]*z[85];
    z[29]= - z[76]*z[25];
    z[72]=z[76]*z[9];
    z[84]=z[27]*z[72];
    z[29]=z[29] + z[84];
    z[29]=z[10]*z[29];
    z[84]=z[9]*z[1];
    z[91]= - z[27]*z[84];
    z[22]=z[22] + z[29] + z[94] + z[91];
    z[29]=n<T>(1,3)*z[50];
    z[91]= - n<T>(5,3)*z[80] + n<T>(2,3)*z[93] + z[49] - z[29];
    z[91]=z[18]*z[91];
    z[93]=2*z[80];
    z[101]=z[93] - z[44] + z[50];
    z[101]=z[18]*z[101];
    z[104]=z[33] - z[42];
    z[101]= - 2*z[104] + z[101];
    z[101]=z[18]*z[101];
    z[101]=z[61] + z[101];
    z[101]=z[101]*z[46];
    z[22]=z[101] + z[91] + n<T>(1,3)*z[22];
    z[91]=2*z[16];
    z[22]=z[22]*z[91];
    z[101]=z[25]*z[72];
    z[101]= - z[95] + n<T>(5,3)*z[101];
    z[101]=z[10]*z[101];
    z[105]=z[60] - z[50];
    z[106]=z[80] - z[49];
    z[29]=z[29] + z[106];
    z[29]=z[18]*z[29];
    z[29]= - z[89] + z[29];
    z[29]=z[29]*z[71];
    z[71]=z[9]*z[94];
    z[107]=z[10]*z[50];
    z[107]= - 3*z[1] + n<T>(4,3)*z[107];
    z[107]=z[18]*z[107];
    z[22]=z[22] + z[62] + z[29] + z[107] + z[101] - n<T>(5,3)*z[71] - z[105]
   ;
    z[22]=z[16]*z[22];
    z[29]=3*z[49] + n<T>(19,3)*z[50];
    z[29]=z[9]*z[14]*z[29];
    z[62]=n<T>(7,3)*z[50];
    z[29]=z[29] - z[49] - z[62];
    z[29]=z[10]*z[29];
    z[71]=z[44] + 17*z[50];
    z[101]= - z[9]*z[71];
    z[107]=7*z[1];
    z[101]=z[101] - z[107] + 10*z[14];
    z[108]=z[10]*z[1];
    z[109]=n<T>(20,9) + z[108];
    z[109]=z[18]*z[109];
    z[29]=z[109] + n<T>(1,9)*z[101] + z[29];
    z[101]=z[100] - z[33];
    z[109]=z[9]*z[101];
    z[64]=32*z[80] + 8*z[64] + z[109];
    z[64]=z[64]*z[46];
    z[109]=z[77]*z[49];
    z[110]=z[39]*z[25];
    z[111]= - z[110] - z[109];
    z[112]=z[97] - z[38];
    z[112]=z[18]*z[112];
    z[111]=4*z[111] + z[112];
    z[78]=z[9]*z[78];
    z[78]=2*z[111] + 17*z[78];
    z[78]=z[78]*z[96];
    z[111]= - z[9] + z[2];
    z[101]=z[69]*z[101]*z[111];
    z[22]=8*z[22] + z[101] + n<T>(8,9)*z[78] + 4*z[29] + z[64];
    z[22]=z[16]*z[22];
    z[29]=npow(z[8],3);
    z[64]=z[103]*z[29];
    z[78]=z[47]*z[36];
    z[78]=z[64] + 7*z[78];
    z[78]=z[3]*z[78];
    z[101]=z[29]*z[36];
    z[111]=z[11]*z[3];
    z[112]=z[111]*z[29];
    z[113]=z[12]*z[1];
    z[113]=npow(z[113],5)*z[112];
    z[78]= - 2*z[113] + 4*z[101] + z[78];
    z[78]=z[11]*z[78];
    z[43]= - z[47]*z[43];
    z[101]=z[29]*z[12];
    z[113]=z[30]*z[101];
    z[43]=z[43] - 2*z[113];
    z[43]=z[43]*z[85];
    z[113]=z[53]*z[8];
    z[114]= - 13*z[1] - z[113];
    z[114]=z[114]*z[47];
    z[115]=z[85]*z[42];
    z[116]=z[8]*z[115];
    z[114]=z[114] - 13*z[116];
    z[114]=z[3]*z[114];
    z[43]=z[78] + z[114] - z[64] + z[43];
    z[64]=n<T>(1,3)*z[11];
    z[43]=z[43]*z[64];
    z[78]=n<T>(1,3)*z[12];
    z[114]=z[47]*z[61]*z[78];
    z[116]=z[49]*z[8];
    z[114]=z[116] + z[114];
    z[114]=z[114]*z[96];
    z[117]=z[47]*z[1];
    z[114]=n<T>(5,3)*z[117] + 2*z[114];
    z[118]=z[96]*z[49];
    z[119]=n<T>(8,3)*z[118];
    z[120]=n<T>(19,3)*z[1];
    z[113]=z[120] + z[113];
    z[113]=z[8]*z[113];
    z[113]=z[113] + z[119];
    z[113]=z[3]*z[113];
    z[43]=z[43] + 2*z[114] + z[113];
    z[43]=z[11]*z[43];
    z[113]=z[33]*z[6];
    z[114]= - z[96]*z[113];
    z[121]=n<T>(8,3)*z[8];
    z[122]= - z[1]*z[121];
    z[123]=z[44]*z[8];
    z[124]= - 5*z[1] - z[123];
    z[124]=z[3]*z[124];
    z[43]=z[43] + n<T>(2,3)*z[124] + z[114] - static_cast<T>(1)+ z[122];
    z[43]=z[11]*z[43];
    z[114]= - z[25]*z[123];
    z[114]=z[110] + z[114];
    z[122]=z[87]*z[41];
    z[123]=npow(z[9],3);
    z[124]=n<T>(2,3)*z[123];
    z[90]=z[90]*z[124];
    z[90]= - z[122] + z[90];
    z[90]=z[12]*z[90];
    z[90]=n<T>(1,3)*z[114] + z[90];
    z[90]=z[90]*z[96];
    z[114]= - z[1] + n<T>(2,3)*z[72];
    z[114]=z[9]*z[27]*z[114];
    z[95]=n<T>(2,3)*z[95] + z[114];
    z[95]=z[9]*z[95];
    z[114]= - z[41]*z[51];
    z[63]=z[63]*z[124];
    z[63]=z[114] + z[63];
    z[63]=z[12]*z[63];
    z[63]=n<T>(5,3)*z[99] + z[63];
    z[63]=z[63]*z[85];
    z[63]=z[95] + z[63];
    z[63]=z[6]*z[63];
    z[95]=2*z[9];
    z[99]=z[1]*z[27]*z[95];
    z[94]= - z[94] + z[99];
    z[94]=z[9]*z[94];
    z[74]=z[94] + z[74];
    z[74]=z[9]*z[74];
    z[94]=n<T>(2,3)*z[8];
    z[76]= - z[76]*z[94];
    z[99]=z[49]*z[10];
    z[75]=z[3]*z[75];
    z[43]=z[43] + z[75] + z[63] + z[90] + z[76] - z[99] + z[1] + n<T>(2,3)*
    z[74];
    z[43]=z[13]*z[43];
    z[63]=4*z[79];
    z[74]=z[63] + z[73];
    z[74]=z[41]*z[74];
    z[63]=z[63] - z[73];
    z[63]=z[63]*z[47];
    z[63]=z[63] + z[74];
    z[63]=z[18]*z[63];
    z[73]=z[123]*z[21];
    z[74]= - z[21]*z[29];
    z[74]= - z[73] + z[74];
    z[75]=8*z[12];
    z[74]=z[75]*z[82]*z[77]*z[74];
    z[76]=z[123]*z[87];
    z[63]=z[74] + 16*z[76] + z[63];
    z[63]=z[12]*z[63];
    z[74]=25*z[97] - 16*z[38];
    z[74]=z[74]*z[45];
    z[76]=4*z[8];
    z[79]=z[81]*z[76];
    z[74]=z[79] + 16*z[109] + z[74];
    z[74]=z[74]*z[57];
    z[79]=z[33]*z[28];
    z[81]= - n<T>(8,3)*z[39] + z[99];
    z[81]=z[81]*z[20];
    z[79]= - n<T>(25,3)*z[79] + 2*z[81];
    z[79]=z[79]*z[45];
    z[81]= - z[41]*z[92];
    z[87]=n<T>(16,3)*z[39];
    z[77]=z[77]*z[87];
    z[63]=n<T>(1,9)*z[63] + z[74] + z[79] + z[81] + z[77];
    z[63]=z[63]*z[96];
    z[74]=z[30] + 2*z[23];
    z[77]=n<T>(4,9)*z[9];
    z[74]=z[74]*z[77];
    z[74]=z[74] + z[70] + n<T>(5,3)*z[61];
    z[74]=z[74]*z[95];
    z[77]=3*z[33];
    z[74]=z[74] - z[77] - n<T>(16,3)*z[42];
    z[74]=z[74]*z[9];
    z[35]=z[74] + z[35];
    z[23]=z[30] + z[23];
    z[23]=z[23]*z[9];
    z[74]=z[70] + z[61];
    z[23]=z[23] - z[74];
    z[23]=z[23]*z[9];
    z[23]=z[23] + z[100];
    z[79]=z[61] - z[21];
    z[30]=z[30]*z[8];
    z[81]=z[30] - z[79];
    z[81]=z[81]*z[8];
    z[81]=z[81] - z[23];
    z[81]=z[81]*z[2];
    z[21]=z[31] - z[21] + 5*z[61];
    z[21]=z[21]*z[94];
    z[21]=z[21] - z[77] + n<T>(7,3)*z[42];
    z[31]=z[8]*z[21];
    z[31]=z[81] + z[31] - z[35];
    z[31]=z[2]*z[31];
    z[23]= - z[10]*z[23];
    z[77]=z[79]*z[10];
    z[30]=z[10]*z[30];
    z[30]= - z[77] + z[30];
    z[30]=z[8]*z[30];
    z[23]= - z[81] + z[23] + z[30];
    z[23]=z[5]*z[23];
    z[30]=z[10]*z[35];
    z[21]= - z[8]*z[10]*z[21];
    z[35]=z[42]*z[2];
    z[35]=z[35] - z[56];
    z[79]=z[35]*z[69];
    z[21]=z[23] + z[79] + z[31] + z[30] + z[21];
    z[21]=z[5]*z[21];
    z[23]=z[48] - z[61];
    z[30]= - z[18]*z[23];
    z[31]=z[27]*z[12];
    z[79]=z[34]*z[31];
    z[30]=z[79] - z[86] + z[30];
    z[79]=n<T>(2,3)*z[16];
    z[30]=z[30]*z[85]*z[79];
    z[81]=z[25]*z[58]*z[78];
    z[81]=z[81] + z[89] - z[59];
    z[81]=z[81]*z[96];
    z[89]=z[9]*z[50];
    z[89]= - z[1] + z[89];
    z[30]=z[30] + n<T>(2,3)*z[89] + z[81];
    z[30]=z[30]*z[91];
    z[81]=z[33]*z[9];
    z[89]=z[96]*z[81];
    z[89]=4*z[89] - static_cast<T>(10)+ z[84];
    z[30]=n<T>(1,9)*z[89] + z[30];
    z[30]=z[16]*z[30];
    z[89]= - z[12]*z[25]*z[34];
    z[89]=z[89] - z[23];
    z[89]=z[89]*z[85]*z[91];
    z[90]= - z[12]*z[9]*z[61];
    z[90]= - z[49] + z[90];
    z[90]=z[90]*z[96];
    z[89]=z[90] + z[89];
    z[89]=z[16]*z[89];
    z[39]=z[39]*z[96];
    z[89]= - n<T>(2,3)*z[39] + z[89];
    z[90]=z[85]*z[48];
    z[79]=z[83]*z[79];
    z[79]=z[90] + z[79];
    z[79]=z[16]*z[79];
    z[79]=n<T>(5,9)*z[118] + z[79];
    z[79]=z[3]*z[79];
    z[79]=n<T>(1,3)*z[89] + z[79];
    z[79]=z[16]*z[79];
    z[83]=z[3]*z[118];
    z[39]= - z[39] + z[83];
    z[39]=z[13]*z[39];
    z[39]=n<T>(2,9)*z[39] + z[79];
    z[39]=z[17]*z[39];
    z[79]=z[82]*z[115];
    z[83]=npow(z[18],3);
    z[89]=z[83]*z[36];
    z[90]=z[1]*z[82];
    z[89]=z[90] - n<T>(2,3)*z[89];
    z[89]=z[89]*z[91];
    z[79]=z[89] + z[93] - n<T>(7,3)*z[79];
    z[79]=z[79]*z[91];
    z[89]= - z[96]*z[98];
    z[89]=z[1] + z[89];
    z[79]=n<T>(5,3)*z[89] + z[79];
    z[79]=z[16]*z[79];
    z[79]= - n<T>(4,3) + z[79];
    z[79]=z[3]*z[79];
    z[89]= - z[41]*z[86];
    z[31]=z[73]*z[31];
    z[31]=z[89] + z[31];
    z[31]=z[31]*z[78];
    z[78]= - z[60]*z[111];
    z[31]=z[78] + z[81] + z[31];
    z[31]=z[96]*z[31];
    z[78]=z[1]*z[65];
    z[31]=z[78] - static_cast<T>(1)- n<T>(2,3)*z[84] + z[31];
    z[31]=z[13]*z[31];
    z[30]=2*z[39] + n<T>(2,3)*z[31] + z[30] + z[79];
    z[30]=z[17]*z[30];
    z[31]=z[104]*z[20];
    z[31]=z[31] - z[42] + z[77];
    z[31]=z[31]*z[121];
    z[39]= - z[100] + 7*z[33];
    z[39]=z[10]*z[39];
    z[77]=4*z[50];
    z[78]= - z[20]*z[77];
    z[31]=z[31] + z[78] + z[39] - 5*z[49] + 8*z[58];
    z[31]=z[31]*z[57];
    z[39]=z[55] - z[58];
    z[39]=z[39]*z[40];
    z[57]= - z[49] + 7*z[50];
    z[57]=z[10]*z[57];
    z[39]=n<T>(1,9)*z[57] + z[39] + z[1] + n<T>(2,3)*z[14];
    z[57]= - static_cast<T>(23)+ 11*z[108];
    z[57]=z[18]*z[57];
    z[31]=z[31] + 4*z[39] + n<T>(1,9)*z[57];
    z[31]=z[8]*z[31];
    z[39]=z[61]*z[18];
    z[57]= - 14*z[86] - z[39];
    z[57]=z[18]*z[41]*z[57];
    z[57]= - 25*z[122] + z[57];
    z[61]=z[25]*z[45];
    z[27]=z[27] + z[61];
    z[27]=z[18]*z[73]*z[27];
    z[51]=z[123]*z[51];
    z[27]=z[51] + z[27];
    z[27]=z[27]*z[75];
    z[27]=n<T>(1,3)*z[57] + z[27];
    z[27]=z[12]*z[27];
    z[51]=z[87] + z[99];
    z[51]=z[18]*z[51];
    z[51]=n<T>(37,3)*z[81] + z[51];
    z[51]=z[18]*z[51];
    z[27]=z[27] + n<T>(41,3)*z[110] + z[51];
    z[27]=z[27]*z[96];
    z[51]=z[49] - n<T>(5,3)*z[50];
    z[51]=z[14]*z[51];
    z[51]=z[42] + z[51];
    z[57]=8*z[72];
    z[25]=z[25]*z[57];
    z[25]=5*z[51] + z[25];
    z[25]=z[9]*z[25];
    z[51]=z[49] + z[77];
    z[25]=n<T>(8,3)*z[51] + z[25];
    z[25]=z[9]*z[25];
    z[51]=z[72]*z[88];
    z[51]= - z[62] + z[51];
    z[51]=z[9]*z[51];
    z[51]=z[51] + n<T>(8,3)*z[1];
    z[51]=z[51]*z[95];
    z[57]=z[57] - z[1];
    z[41]=z[57]*z[41];
    z[45]=z[45]*z[41];
    z[45]=z[45] + z[108] + static_cast<T>(1)+ z[51];
    z[45]=z[18]*z[45];
    z[51]= - z[49] - 16*z[50];
    z[51]=z[2]*z[51];
    z[25]=z[27] + z[51] + z[45] + z[99] - 16*z[14] + z[25];
    z[25]=z[25]*z[69];
    z[27]=z[106]*z[82]*z[102];
    z[45]= - z[53] + z[80];
    z[45]=z[18]*z[45];
    z[45]=z[42] + z[45];
    z[45]=z[45]*z[46];
    z[45]=z[45] + z[49] - n<T>(4,3)*z[80];
    z[45]=z[82]*z[45];
    z[46]=npow(z[18],4)*z[36];
    z[45]=n<T>(1,3)*z[46] + z[45];
    z[45]=z[45]*z[91];
    z[46]=z[49] - n<T>(11,3)*z[80];
    z[46]=z[18]*z[46];
    z[51]=z[83]*z[115];
    z[27]=z[45] + n<T>(4,3)*z[51] + z[46] + z[27];
    z[27]=z[27]*z[91];
    z[45]= - z[49] + z[93];
    z[45]=z[18]*z[45];
    z[45]=z[68] + z[45];
    z[45]=z[45]*z[102];
    z[46]=z[82]*z[119];
    z[27]=z[27] + z[46] + z[45] + z[49] - n<T>(14,3)*z[80];
    z[27]=z[16]*z[27];
    z[45]=z[2]*z[80];
    z[27]=z[45] + z[27];
    z[45]=z[10]*z[53];
    z[45]= - z[120] + z[45];
    z[46]=npow(z[10],2);
    z[51]=z[46]*z[80];
    z[57]=static_cast<T>(8)+ z[108];
    z[57]=2*z[57] + z[51];
    z[57]=z[18]*z[57];
    z[45]=2*z[45] + z[57];
    z[57]=z[42] - n<T>(5,3)*z[59];
    z[57]=z[57]*z[47];
    z[27]=n<T>(1,3)*z[45] + z[57] + 4*z[27];
    z[27]=z[3]*z[27];
    z[45]=16*z[33] - 13*z[59];
    z[45]=z[45]*z[47];
    z[39]=z[101]*z[39];
    z[39]=n<T>(1,3)*z[45] - 4*z[39];
    z[39]=z[39]*z[96];
    z[45]=z[8]*z[60];
    z[45]=z[1] + z[45];
    z[45]=z[45]*z[76];
    z[45]=n<T>(23,3) + z[45];
    z[45]=z[8]*z[45];
    z[39]=z[45] + z[39];
    z[45]=z[46]*z[49];
    z[46]=z[103]*z[2];
    z[45]=z[51] + z[45] - z[46];
    z[46]=z[96]*z[38];
    z[46]=z[46] + static_cast<T>(1)- z[45];
    z[46]=z[6]*z[46];
    z[51]=z[8]*z[59];
    z[38]= - z[38] - n<T>(17,3)*z[51];
    z[38]=z[38]*z[96];
    z[51]=n<T>(26,3)*z[1] - 5*z[116];
    z[51]=z[8]*z[51];
    z[38]=z[38] + z[51] + static_cast<T>(2)+ z[45];
    z[38]=z[3]*z[38];
    z[38]=z[38] + 2*z[39] + z[46];
    z[39]=z[42]*z[47];
    z[45]=8*z[101];
    z[26]= - z[26]*z[45];
    z[26]=41*z[39] + z[26];
    z[26]=z[26]*z[85];
    z[39]=z[29]*z[1];
    z[26]=16*z[39] + z[26];
    z[26]=z[3]*z[26];
    z[36]=z[36]*z[112];
    z[29]=z[29]*z[115];
    z[26]= - 16*z[36] + 40*z[29] + z[26];
    z[26]=z[26]*z[64];
    z[29]=z[49]*z[47];
    z[23]=z[23]*z[45];
    z[23]= - n<T>(77,3)*z[29] + z[23];
    z[23]=z[23]*z[96];
    z[29]=z[12]*z[47]*z[48];
    z[29]= - 46*z[116] + 13*z[29];
    z[29]=z[29]*z[96];
    z[29]= - 37*z[117] + z[29];
    z[29]=z[29]*z[65];
    z[23]=z[26] + z[29] - 8*z[39] + z[23];
    z[23]=z[23]*z[64];
    z[26]=z[49] + n<T>(1,3)*z[56];
    z[26]=z[26]*z[10];
    z[29]=z[37]*z[35];
    z[35]=z[49]*z[2];
    z[26]=z[29] - z[26] + z[35];
    z[29]=z[3] - z[6];
    z[26]=z[5]*z[29]*z[26];
    z[23]=z[23] + n<T>(1,3)*z[38] + z[26];
    z[23]=z[11]*z[23];
    z[26]=z[9]*z[70];
    z[26]=5*z[42] + z[26];
    z[26]=z[26]*z[40];
    z[26]=z[49] + z[26];
    z[26]=z[26]*z[95];
    z[26]= - z[1] + z[26];
    z[29]= - z[8]*z[100];
    z[29]=11*z[49] + z[29];
    z[29]=z[8]*z[29];
    z[26]=2*z[26] + z[29];
    z[29]= - z[100] + 5*z[34];
    z[29]=z[29]*z[40];
    z[34]=z[8]*z[68];
    z[29]=z[34] + z[53] + z[29];
    z[29]=z[2]*z[29];
    z[26]=n<T>(2,3)*z[26] + z[29];
    z[26]=z[2]*z[26];
    z[29]= - z[49] + z[58];
    z[29]=z[29]*z[54];
    z[34]=z[42] + z[59];
    z[34]=z[3]*z[34];
    z[35]= - z[3]*z[48];
    z[35]=z[52] + z[35];
    z[35]=z[4]*z[35];
    z[29]=z[35] + z[34] - z[58] + z[29];
    z[29]=z[4]*z[29];
    z[34]=z[6]*z[66];
    z[34]=z[67] + z[34];
    z[34]=z[7]*z[34];
    z[35]= - z[33] - z[67];
    z[35]=z[9]*z[35];
    z[34]=z[34] + z[35] - z[113];
    z[34]=z[7]*z[34];
    z[35]=z[49] + z[58];
    z[35]=z[9]*z[35];
    z[36]=z[13]*z[53];
    z[29]=z[36] + z[29] + z[34] + 2*z[1] + z[35];
    z[29]=z[19]*z[29];
    z[34]=z[55] + z[77];
    z[35]= - z[9]*z[105]*z[88];
    z[34]=n<T>(1,3)*z[34] + z[35];
    z[34]=z[9]*z[34];
    z[35]=z[1] - n<T>(1,3)*z[14];
    z[34]=2*z[35] + z[34];
    z[34]=z[34]*z[95];
    z[34]=static_cast<T>(7)+ z[34];
    z[35]=z[9]*z[74];
    z[35]= - n<T>(8,9)*z[35] - 3*z[42] - n<T>(7,3)*z[33];
    z[35]=z[9]*z[35];
    z[35]=n<T>(4,9)*z[71] + z[35];
    z[28]=z[35]*z[28];
    z[33]= - z[100] - z[33];
    z[33]=z[33]*z[95];
    z[33]=z[33] + z[44] + 5*z[50];
    z[33]=z[33]*z[95];
    z[33]= - z[107] + z[33];
    z[33]=z[9]*z[33];
    z[33]=static_cast<T>(1)+ n<T>(1,9)*z[33];
    z[33]=z[10]*z[33];
    z[20]=z[20]*z[41];
    z[20]=2*z[33] - n<T>(1,9)*z[20];
    z[20]=z[18]*z[20];
    z[20]=n<T>(2,3)*z[29] + 4*z[30] + n<T>(4,3)*z[43] + z[23] + z[32] + z[21] + 
    z[27] + z[22] + z[25] + z[63] + z[26] + z[31] + z[20] + n<T>(1,3)*z[34]
    + z[28] + 2*z[24];

    r += 2*z[20];
 
    return r;
}

template double qqb_2lNLC_r1125(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r1125(const std::array<dd_real,31>&);
#endif
