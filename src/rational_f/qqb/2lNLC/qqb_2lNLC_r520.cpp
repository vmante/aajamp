#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r520(const std::array<T,31>& k) {
  T z[88];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[5];
    z[7]=k[8];
    z[8]=k[9];
    z[9]=k[22];
    z[10]=k[2];
    z[11]=k[3];
    z[12]=k[12];
    z[13]=k[13];
    z[14]=k[4];
    z[15]=k[15];
    z[16]=npow(z[1],3);
    z[17]=z[16]*z[2];
    z[18]=npow(z[1],2);
    z[19]=z[17] + z[18];
    z[20]=z[2]*z[19];
    z[21]=2*z[18];
    z[22]=z[21] + z[17];
    z[22]=z[3]*z[22];
    z[20]=z[22] - z[1] + z[20];
    z[20]=z[3]*z[20];
    z[22]=npow(z[2],2);
    z[23]= - z[18]*z[22];
    z[24]=z[18]*z[2];
    z[25]= - z[1] - z[24];
    z[25]=z[3]*z[25];
    z[23]=z[23] + z[25];
    z[23]=z[3]*z[23];
    z[25]=z[22]*z[1];
    z[23]=z[25] + z[23];
    z[26]=z[3]*z[1];
    z[27]=z[2]*z[26];
    z[27]=z[25] + z[27];
    z[27]=z[14]*z[3]*z[27];
    z[23]=2*z[23] + z[27];
    z[23]=z[14]*z[23];
    z[27]=4*z[18];
    z[28]=z[6]*z[1];
    z[29]= - z[27] + z[28];
    z[29]=z[2]*z[29];
    z[29]= - z[1] + z[29];
    z[29]=z[2]*z[29];
    z[20]=z[23] + z[29] + z[20];
    z[20]=z[14]*z[20];
    z[23]=2*z[28];
    z[29]=npow(z[6],2);
    z[30]=z[29]*z[4];
    z[31]= - z[1]*z[30];
    z[31]=z[23] + z[31];
    z[31]=z[4]*z[31];
    z[32]=z[18]*z[6];
    z[33]=z[16] - z[32];
    z[34]=2*z[2];
    z[33]=z[33]*z[34];
    z[35]=3*z[18];
    z[33]=z[35] + z[33];
    z[33]=z[2]*z[33];
    z[36]= - z[3]*z[19];
    z[20]=z[20] + z[36] + z[31] + z[33];
    z[20]=z[14]*z[20];
    z[31]=z[21] + z[28];
    z[33]=z[31]*z[6];
    z[36]=z[33] + z[16];
    z[37]=z[36]*z[30];
    z[38]=7*z[18];
    z[39]=z[38] + 4*z[28];
    z[39]=z[6]*z[39];
    z[40]=3*z[16];
    z[39]=z[40] + z[39];
    z[39]=z[6]*z[39];
    z[39]=z[39] - z[37];
    z[39]=z[4]*z[39];
    z[41]=z[28] + z[18];
    z[42]=z[41]*z[29];
    z[42]=z[42] - z[37];
    z[42]=z[5]*z[42];
    z[43]=z[35] + z[23];
    z[43]=z[43]*z[6];
    z[39]=z[42] - z[43] + z[39];
    z[39]=z[5]*z[39];
    z[42]=5*z[18];
    z[44]=3*z[28];
    z[45]=z[42] + z[44];
    z[46]=z[45]*z[6];
    z[30]=z[31]*z[30];
    z[30]= - z[46] + z[30];
    z[30]=z[4]*z[30];
    z[47]=2*z[16];
    z[48]=z[6]*z[17];
    z[48]= - z[47] + z[48];
    z[48]=z[2]*z[48];
    z[20]=z[20] + z[48] + z[39] + z[30] + z[31];
    z[20]=z[15]*z[20];
    z[30]=z[32] + z[16];
    z[39]=2*z[6];
    z[48]=z[4]*z[30]*z[39];
    z[48]=z[48] - z[16];
    z[49]=6*z[18];
    z[50]=z[49] + z[28];
    z[50]=z[6]*z[50];
    z[50]=z[50] - z[48];
    z[50]=z[4]*z[50];
    z[51]=z[44] + z[27];
    z[51]=z[51]*z[6];
    z[52]=z[36]*z[4];
    z[53]=z[6]*z[52];
    z[53]=z[51] - 4*z[53];
    z[53]=z[5]*z[53];
    z[50]=z[53] + z[50] - z[21] + z[44];
    z[50]=z[5]*z[50];
    z[53]=2*z[3];
    z[19]= - z[19]*z[53];
    z[54]=7*z[1];
    z[19]=z[19] + z[54] + 12*z[24];
    z[19]=z[3]*z[19];
    z[55]=11*z[1];
    z[56]=z[21]*z[3];
    z[57]=z[56] - z[55] - 4*z[24];
    z[57]=z[3]*z[2]*z[57];
    z[58]=z[22]*z[14];
    z[59]=z[26]*z[58];
    z[57]=4*z[59] + 6*z[25] + z[57];
    z[57]=z[14]*z[57];
    z[59]= - z[35] + z[28];
    z[59]=z[59]*z[34];
    z[59]= - 9*z[1] + z[59];
    z[59]=z[2]*z[59];
    z[19]=z[57] + z[59] + z[19];
    z[19]=z[14]*z[19];
    z[57]=z[16]*z[4];
    z[59]=z[57] - z[17];
    z[59]=z[8]*z[59];
    z[60]=z[16]*z[3];
    z[61]=z[16]*z[5];
    z[62]=z[61] - z[60];
    z[62]=z[7]*z[62];
    z[59]=z[59] + z[62];
    z[62]=z[21] - z[28];
    z[63]=z[4]*z[6]*z[62];
    z[63]=z[63] - z[21] + 5*z[28];
    z[63]=z[4]*z[63];
    z[24]= - z[39]*z[24];
    z[24]=z[38] + z[24];
    z[24]=z[2]*z[24];
    z[17]= - z[35] - 4*z[17];
    z[17]=z[3]*z[17];
    z[17]=z[20] + z[19] + z[17] + z[24] + z[50] - 6*z[1] + z[63] + 3*
    z[59];
    z[17]=z[15]*z[17];
    z[19]=z[23] - z[18];
    z[20]=z[6]*z[19];
    z[20]=z[20] + z[48];
    z[20]=z[4]*z[20];
    z[24]=z[51] + z[16];
    z[24]=z[24]*z[6];
    z[37]= - z[24] + z[37];
    z[37]=z[4]*z[37];
    z[50]=z[23] + z[18];
    z[51]=z[50]*z[6];
    z[59]=z[14]*z[28];
    z[37]=z[59] + z[51] + z[37];
    z[37]=z[7]*z[37];
    z[59]=z[18]*z[5];
    z[63]=z[1] - z[59];
    z[63]=z[14]*z[63];
    z[20]=z[37] + z[63] + z[20] + z[18] - z[28];
    z[20]=z[7]*z[20];
    z[37]= - 3*z[32] + z[48];
    z[37]=z[4]*z[37];
    z[48]=z[16]*npow(z[4],2);
    z[63]=z[29]*z[2];
    z[64]=z[63]*z[48];
    z[37]=z[37] + z[64];
    z[37]=z[2]*z[37];
    z[64]=2*z[1];
    z[65]= - z[64] - z[59];
    z[66]=z[61] - z[18];
    z[67]=z[3]*z[66];
    z[65]=3*z[65] - 4*z[67];
    z[65]=z[3]*z[65];
    z[67]=z[61] - z[21];
    z[68]=z[67]*z[5];
    z[69]=z[1] + z[68];
    z[70]=npow(z[3],2);
    z[69]=z[14]*z[69]*z[70];
    z[71]=z[4]*z[1];
    z[65]=z[69] - z[71] + z[65];
    z[65]=z[14]*z[65];
    z[69]=z[30]*z[5];
    z[72]=z[32]*z[2];
    z[69]= - z[21] + z[69] + z[72];
    z[73]=z[18]*z[4];
    z[74]=z[18]*z[3];
    z[75]=z[73] + z[74];
    z[75]=z[75]*z[14];
    z[76]=11*z[75] - z[69];
    z[76]=z[12]*z[76];
    z[77]=5*z[1];
    z[78]=z[4]*z[28];
    z[78]= - z[77] + z[78];
    z[79]=z[42] - z[61];
    z[79]=z[3]*z[79];
    z[72]= - z[18] + z[72];
    z[72]=z[8]*z[72];
    z[20]=z[76] + z[20] + z[65] + z[72] + z[79] + z[37] + 3*z[78] + 11*
    z[59];
    z[20]=z[13]*z[20];
    z[37]=z[47] + z[32];
    z[37]=z[37]*z[6];
    z[65]=npow(z[1],4);
    z[37]=z[37] + z[65];
    z[37]=z[37]*z[4];
    z[72]=z[63]*z[18];
    z[37]=z[37] - z[72];
    z[72]= - z[5]*z[65];
    z[72]=z[72] + z[47] - z[37];
    z[72]=z[8]*z[72];
    z[76]=z[29]*z[1];
    z[78]=z[76] - z[16];
    z[78]=z[78]*z[4];
    z[79]=z[63]*z[1];
    z[78]=z[78] - z[79];
    z[72]=z[72] + z[61] + z[78];
    z[80]=2*z[8];
    z[72]=z[72]*z[80];
    z[81]= - z[2]*z[1];
    z[81]=z[26] + z[71] + z[81];
    z[81]=z[14]*z[81];
    z[69]=z[75] - z[69];
    z[69]=z[13]*z[69];
    z[75]=z[4]*z[19];
    z[82]=z[23]*z[2];
    z[83]=z[28]*z[5];
    z[84]=3*z[1];
    z[69]=z[69] + z[81] + z[72] + z[74] - z[82] + z[83] + z[84] + z[75];
    z[69]=z[9]*z[69];
    z[72]=22*z[28];
    z[75]=47*z[18] + z[72];
    z[75]=z[6]*z[75];
    z[81]=z[41]*z[6];
    z[85]= - z[16] + z[81];
    z[85]=z[6]*z[85];
    z[85]= - z[65] + z[85];
    z[85]=z[4]*z[85];
    z[75]=z[85] + 25*z[16] + z[75];
    z[75]=z[4]*z[75];
    z[72]=z[35] - z[72];
    z[72]=z[6]*z[72];
    z[85]= - z[31]*z[63];
    z[72]=z[72] + z[85];
    z[72]=z[2]*z[72];
    z[37]=z[37] - z[16];
    z[85]=24*z[8];
    z[86]= - z[37]*z[85];
    z[52]=z[52] - z[41];
    z[52]=z[4]*z[52];
    z[87]= - z[28] + z[79];
    z[87]=z[2]*z[87];
    z[52]=z[52] + z[87];
    z[52]=z[14]*z[52];
    z[52]=z[52] + z[86] + z[75] + z[72];
    z[52]=z[7]*z[52];
    z[72]=z[27] + z[28];
    z[72]=z[6]*z[72];
    z[72]=z[40] + z[72];
    z[72]=z[4]*z[72];
    z[72]=z[72] - 28*z[18] - 23*z[28];
    z[72]=z[4]*z[72];
    z[50]=z[2]*z[50]*z[39];
    z[50]=z[50] + 21*z[28] - z[67];
    z[50]=z[2]*z[50];
    z[37]=z[8]*z[37];
    z[37]=z[37] - z[21] - z[78];
    z[37]=z[37]*z[85];
    z[67]=z[21]*z[5];
    z[75]= - z[82] - z[1] + z[67];
    z[75]=z[2]*z[75];
    z[78]=2*z[4];
    z[82]=z[78]*z[74];
    z[85]=z[4]*z[64];
    z[75]=z[82] + z[85] + z[75];
    z[75]=z[14]*z[75];
    z[82]=z[57] - z[21];
    z[85]= - z[3]*z[82];
    z[37]=z[52] + z[75] + z[37] + z[85] + z[50] + z[72] - z[67];
    z[37]=z[7]*z[37];
    z[46]=z[47] + z[46];
    z[46]=z[6]*z[46];
    z[50]= - z[36]*z[63];
    z[46]=z[46] + z[50];
    z[46]=z[2]*z[46];
    z[50]=z[14]*z[41];
    z[43]=z[50] + z[46] - z[16] - z[43];
    z[43]=z[7]*z[43];
    z[46]=7*z[28];
    z[50]= - z[42] - z[46];
    z[50]=z[6]*z[50];
    z[41]=z[41]*z[63];
    z[41]=z[50] + 4*z[41];
    z[41]=z[2]*z[41];
    z[50]= - z[64] - z[74];
    z[50]=z[14]*z[50];
    z[41]=z[43] + z[50] + z[41] + z[18] + z[44];
    z[41]=z[7]*z[41];
    z[43]=z[36]*z[5];
    z[46]= - 4*z[43] + 9*z[18] + z[46];
    z[46]=z[6]*z[46];
    z[46]=z[47] + z[46];
    z[46]=z[5]*z[46];
    z[43]= - z[29]*z[43];
    z[24]=z[24] + z[43];
    z[24]=z[5]*z[24];
    z[24]= - z[51] + z[24];
    z[24]=z[8]*z[24];
    z[24]=z[24] + z[46] - z[21] - z[44];
    z[24]=z[8]*z[24];
    z[43]=2*z[5];
    z[46]= - z[31]*z[43];
    z[46]= - z[1] + z[46];
    z[46]=z[46]*z[43];
    z[47]=4*z[61];
    z[50]=z[35] - z[47];
    z[50]=z[3]*z[5]*z[50];
    z[21]= - z[21] - z[60];
    z[21]=z[3]*z[21];
    z[21]= - z[1] + z[21];
    z[51]=npow(z[5],2);
    z[21]=z[14]*z[51]*z[21];
    z[21]=z[21] + z[46] + z[50];
    z[21]=z[14]*z[21];
    z[36]= - z[36]*z[43];
    z[36]=z[36] + z[35] + z[28];
    z[46]=3*z[5];
    z[36]=z[36]*z[46];
    z[50]= - 6*z[79] - 3*z[62] + z[57];
    z[50]=z[2]*z[50];
    z[52]=z[3]*z[61];
    z[21]=z[41] + z[21] + z[24] + z[52] + z[50] + z[36] + 16*z[1] - 11*
    z[73];
    z[21]=z[12]*z[21];
    z[24]=z[82]*z[4];
    z[24]=z[24] + z[1];
    z[36]= - z[12] + z[3];
    z[24]=z[22]*z[24]*z[36];
    z[36]=z[18]*z[8];
    z[41]= - z[64] + z[36];
    z[41]=z[80]*z[70]*z[41];
    z[50]=z[18] + z[60];
    z[50]=z[8]*z[50];
    z[50]=z[50] - z[77] - 4*z[74];
    z[52]=z[8]*z[3];
    z[50]=z[50]*z[52];
    z[63]=z[70]*z[1];
    z[50]=6*z[63] + z[50];
    z[50]=z[13]*z[50];
    z[67]=npow(z[8],2);
    z[72]=z[67]*z[63];
    z[75]=z[11]*z[13];
    z[75]=z[75] - 1;
    z[75]=z[72]*z[75];
    z[79]= - z[1] - z[56];
    z[52]=z[79]*z[52];
    z[79]=4*z[63];
    z[52]=z[79] + z[52];
    z[52]=z[13]*z[8]*z[52];
    z[52]=z[52] + z[75];
    z[52]=z[11]*z[52];
    z[24]=z[52] + z[50] + z[41] + z[24];
    z[24]=z[11]*z[24];
    z[41]=z[57] - z[18];
    z[50]=z[41]*z[34];
    z[52]= - z[38] + 4*z[57];
    z[52]=z[4]*z[52];
    z[52]=z[50] + 8*z[1] + z[52];
    z[52]=z[2]*z[52];
    z[50]= - z[77] - z[50];
    z[50]=z[3]*z[50];
    z[58]= - z[64]*z[58];
    z[50]=z[58] + z[52] + z[50];
    z[50]=z[3]*z[50];
    z[41]=z[28] + z[41];
    z[41]=z[2]*z[41];
    z[52]=z[4]*z[35];
    z[41]=4*z[41] + z[64] + z[52];
    z[41]=z[2]*z[41];
    z[52]= - z[8]*z[28];
    z[52]=z[52] - z[64] + z[73];
    z[52]=z[8]*z[52];
    z[41]=z[41] + z[52];
    z[41]=z[12]*z[41];
    z[52]=z[73] + z[56];
    z[52]=z[52]*z[53];
    z[56]=z[18] - z[60];
    z[56]=z[3]*z[56];
    z[56]= - z[1] + z[56];
    z[56]=z[8]*z[56];
    z[52]=z[52] + z[56];
    z[52]=z[8]*z[52];
    z[56]= - z[1] + z[73];
    z[56]=z[56]*z[78];
    z[48]=z[1] + z[48];
    z[48]=z[2]*z[48];
    z[48]=z[56] + z[48];
    z[48]=z[48]*z[34];
    z[56]= - z[3]*z[84];
    z[58]=z[1] + z[74];
    z[58]=z[8]*z[58];
    z[56]=z[56] + z[58];
    z[58]=z[14]*z[79];
    z[56]=3*z[56] + z[58];
    z[56]=z[13]*z[56];
    z[24]=z[24] + z[56] + z[41] + z[52] + z[48] + z[50];
    z[24]=z[11]*z[24];
    z[33]= - z[5]*z[33];
    z[33]=z[33] - z[38] + z[23];
    z[33]=z[33]*z[46];
    z[41]=8*z[16];
    z[46]=z[41] + z[81];
    z[29]= - z[5]*z[31]*z[29];
    z[29]=3*z[46] + z[29];
    z[29]=z[5]*z[29];
    z[29]= - z[60] - z[23] + z[29];
    z[29]=z[8]*z[29];
    z[46]=z[78]*z[18];
    z[48]=z[18]*z[39];
    z[48]=z[16] + z[48];
    z[48]=z[5]*z[48];
    z[48]=z[18] + z[48];
    z[48]=z[2]*z[48];
    z[50]=z[35] - z[57];
    z[50]=z[3]*z[50];
    z[29]=z[29] + z[50] + z[48] + z[33] - z[84] - z[46];
    z[29]=z[8]*z[29];
    z[33]=z[5]*z[1];
    z[26]=z[33] - z[26];
    z[33]=z[67]*z[61];
    z[33]= - 2*z[33] + z[26];
    z[33]=z[9]*z[33];
    z[48]= - z[59] + z[74];
    z[48]=z[8]*z[48];
    z[48]=z[48] - z[26];
    z[48]=z[9]*z[8]*z[48];
    z[26]=z[26]*z[67];
    z[26]=z[26] + z[48];
    z[26]=z[10]*z[26];
    z[26]=z[33] + z[26];
    z[28]=24*z[18] + z[28];
    z[33]=z[76]*z[5];
    z[28]=2*z[28] - z[33];
    z[28]=z[5]*z[28];
    z[48]=z[16]*z[70];
    z[28]=z[48] - z[1] + z[28];
    z[28]=z[8]*z[28];
    z[48]= - z[55] - z[83];
    z[48]=z[5]*z[48];
    z[50]=12*z[1] - z[74];
    z[50]=z[3]*z[50];
    z[48]=z[48] + z[50];
    z[28]=2*z[48] + z[28];
    z[28]=z[8]*z[28];
    z[48]=z[1] - z[83];
    z[48]=z[48]*z[43];
    z[33]=z[23] - z[33];
    z[33]=z[5]*z[33];
    z[50]=npow(z[14],2)*z[63];
    z[33]=z[50] - z[1] + z[33];
    z[33]=z[15]*z[33];
    z[33]=z[48] + z[33];
    z[33]=z[15]*z[33];
    z[36]= - z[70]*z[36];
    z[36]=z[63] + z[36];
    z[36]=z[36]*z[80];
    z[48]=z[11]*z[72];
    z[36]=z[36] + z[48];
    z[36]=z[11]*z[36];
    z[26]=z[36] + z[33] - z[63] + z[28] + 24*z[26];
    z[26]=z[10]*z[26];
    z[28]=npow(z[1],5);
    z[33]=z[28]*z[4];
    z[33]=z[33] - z[65];
    z[36]=z[33]*z[5];
    z[48]=z[65]*z[4];
    z[36]= - z[16] + z[48] - z[36];
    z[36]=z[36]*z[34];
    z[48]= - z[43] - z[78];
    z[33]=z[33]*z[48];
    z[33]=z[16] + z[33];
    z[33]=z[5]*z[33];
    z[48]=z[78]*z[65];
    z[50]=z[48] + z[16];
    z[50]=z[50]*z[4];
    z[33]= - z[36] + z[50] + z[33];
    z[33]=z[2]*z[33];
    z[48]=z[48]*z[5];
    z[52]=5*z[57];
    z[35]= - z[36] + z[48] + z[35] - z[52];
    z[35]=z[3]*z[35];
    z[36]= - z[48] + z[49] - z[50];
    z[36]=z[5]*z[36];
    z[48]= - z[27] + z[52];
    z[48]=z[4]*z[48];
    z[33]=z[35] + z[33] + z[48] + z[36];
    z[33]=z[3]*z[33];
    z[35]=z[78]*z[16];
    z[36]=z[38] - z[35];
    z[36]=z[4]*z[36];
    z[48]=z[78]*z[61];
    z[36]=z[48] - 4*z[1] + z[36];
    z[36]=z[5]*z[36];
    z[38]= - z[38] + z[47];
    z[38]=z[5]*z[38];
    z[34]= - z[66]*z[34];
    z[34]=z[34] - z[84] + z[38];
    z[34]=z[2]*z[34];
    z[38]=z[78] + z[2];
    z[38]=z[53]*z[61]*z[38];
    z[34]=z[38] + z[36] + z[34];
    z[34]=z[3]*z[34];
    z[36]= - z[51]*z[57];
    z[38]=z[2]*z[68];
    z[36]=z[36] + z[38];
    z[36]=z[3]*z[36];
    z[38]= - z[51]*z[46];
    z[22]=z[22]*z[84];
    z[22]=z[36] + z[38] + z[22];
    z[22]=z[3]*z[22];
    z[36]= - z[51]*z[71];
    z[22]=z[36] + z[22];
    z[22]=z[14]*z[22];
    z[36]=z[84] - z[73];
    z[36]=z[4]*z[36];
    z[31]=z[4]*z[31];
    z[31]= - z[1] + z[31];
    z[31]=z[5]*z[31];
    z[31]=z[36] + z[31];
    z[31]=z[31]*z[43];
    z[22]=z[22] + z[34] + z[31] + 5*z[25];
    z[22]=z[14]*z[22];
    z[25]= - z[35] + z[45];
    z[25]=z[4]*z[25];
    z[18]= - 8*z[18] - z[44];
    z[18]=z[6]*z[18];
    z[18]= - 7*z[16] + z[18];
    z[18]=z[4]*z[18];
    z[18]=z[18] + z[62];
    z[18]=z[5]*z[18];
    z[18]=z[18] - z[84] + z[25];
    z[18]=z[5]*z[18];
    z[25]=z[39]*z[16];
    z[31]=3*z[65] + z[25];
    z[34]=z[16]*z[6];
    z[34]=z[34] + 2*z[65];
    z[35]= - z[6]*z[34];
    z[28]= - z[28] + z[35];
    z[28]=z[4]*z[28];
    z[28]=2*z[31] + z[28];
    z[28]=z[4]*z[28];
    z[31]=5*z[16];
    z[28]= - z[31] + z[28];
    z[28]=z[5]*z[28];
    z[25]=z[65] + z[25];
    z[25]=z[4]*z[25];
    z[16]= - 4*z[16] + z[25];
    z[16]=z[4]*z[16];
    z[16]=z[28] - 3*z[19] + z[16];
    z[16]=z[2]*z[16];
    z[19]= - z[6]*z[30];
    z[19]= - z[65] + z[19];
    z[19]=z[19]*z[78];
    z[19]=z[19] + z[41] + 7*z[32];
    z[19]=z[4]*z[19];
    z[25]= - z[34]*z[78];
    z[25]=z[31] + z[25];
    z[25]=z[5]*z[25];
    z[19]=z[25] - z[27] + z[19];
    z[19]=z[5]*z[19];
    z[25]=z[40] + 4*z[32];
    z[25]=z[4]*z[25];
    z[25]= - z[27] + z[25];
    z[25]=z[4]*z[25];
    z[16]=z[16] + z[19] + z[1] + z[25];
    z[16]=z[2]*z[16];
    z[19]=z[42] + z[23];
    z[19]=z[4]*z[19];
    z[19]= - z[54] + z[19];
    z[19]=z[4]*z[19];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[24]
       + z[26] + z[29] + z[33] + z[37] + 12*z[69];
 
    return r;
}

template double qqb_2lNLC_r520(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r520(const std::array<dd_real,31>&);
#endif
