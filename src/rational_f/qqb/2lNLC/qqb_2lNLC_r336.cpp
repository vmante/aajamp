#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r336(const std::array<T,31>& k) {
  T z[131];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[10];
    z[7]=k[14];
    z[8]=k[9];
    z[9]=k[2];
    z[10]=k[3];
    z[11]=k[4];
    z[12]=k[5];
    z[13]=k[8];
    z[14]=k[28];
    z[15]=k[25];
    z[16]=k[15];
    z[17]=npow(z[1],2);
    z[18]=2*z[17];
    z[19]=z[12]*z[1];
    z[20]=z[18] + z[19];
    z[21]=z[20]*z[12];
    z[22]=npow(z[1],3);
    z[23]=z[21] + z[22];
    z[24]=z[23]*z[4];
    z[25]=z[19] + z[17];
    z[26]=z[24] - z[25];
    z[26]=z[4]*z[26];
    z[27]=npow(z[12],2);
    z[28]=z[27]*z[1];
    z[29]=z[5]*z[28];
    z[29]= - z[19] + z[29];
    z[29]=z[5]*z[29];
    z[30]=z[3]*z[25];
    z[26]=z[30] + z[26] + z[29];
    z[26]=z[11]*z[26];
    z[29]=z[17]*z[12];
    z[30]=z[29] + z[22];
    z[31]=2*z[30];
    z[32]= - z[12]*z[24];
    z[32]=z[31] + z[32];
    z[32]=z[4]*z[32];
    z[33]=z[27]*z[5];
    z[34]=z[33]*z[25];
    z[35]=z[18]*z[12];
    z[36]=z[35] + z[34];
    z[36]=z[5]*z[36];
    z[31]= - z[3]*z[31];
    z[26]=z[26] + z[31] + z[32] + z[36];
    z[26]=z[11]*z[26];
    z[24]= - 2*z[25] - z[24];
    z[31]=npow(z[12],3);
    z[24]=z[4]*z[31]*z[24];
    z[32]=z[28] - z[22];
    z[36]=z[32]*z[12];
    z[37]=z[11]*z[19];
    z[37]= - z[35] + z[37];
    z[37]=z[11]*z[37];
    z[24]=z[37] - z[36] + z[24];
    z[24]=z[2]*z[24];
    z[37]=z[27]*z[4];
    z[38]=z[23]*z[37];
    z[38]=z[36] + z[38];
    z[38]=z[4]*z[38];
    z[39]=3*z[17];
    z[40]=z[39] + z[19];
    z[40]=z[40]*z[12];
    z[41]=2*z[22];
    z[42]=z[40] + z[41];
    z[42]=z[42]*z[12];
    z[43]=z[33]*z[23];
    z[44]=z[42] + z[43];
    z[44]=z[5]*z[44];
    z[45]=2*z[12];
    z[46]= - z[45] - z[33];
    z[47]=3*z[22];
    z[48]=z[40] + z[47];
    z[48]=z[48]*z[12];
    z[49]=npow(z[1],4);
    z[48]=z[48] + z[49];
    z[46]=z[5]*z[48]*z[46];
    z[42]= - z[42] + z[46];
    z[42]=z[3]*z[42];
    z[24]=z[24] + z[26] + z[42] + z[38] + z[44];
    z[24]=z[13]*z[24];
    z[26]=z[22]*z[7];
    z[38]=n<T>(1,2)*z[26];
    z[42]=n<T>(1,2)*z[17];
    z[44]=2*z[19];
    z[46]=z[38] - z[42] - z[44];
    z[46]=z[5]*z[46];
    z[46]=z[1] + z[46];
    z[46]=z[5]*z[46];
    z[50]=npow(z[4],2);
    z[51]=z[50]*z[42];
    z[52]=n<T>(1,2)*z[22];
    z[53]=z[52]*z[6];
    z[54]= - z[50]*z[53];
    z[46]=z[54] - z[51] + z[46];
    z[46]=z[11]*z[46];
    z[54]=3*z[19];
    z[55]=z[54] + z[17];
    z[56]=n<T>(1,2)*z[55];
    z[57]=z[4]*z[32];
    z[57]=z[56] + z[57];
    z[57]=z[4]*z[57];
    z[58]= - z[17] + n<T>(27,4)*z[19];
    z[59]=2*z[5];
    z[60]= - z[28]*z[59];
    z[60]=z[60] - z[26] + z[58];
    z[60]=z[5]*z[60];
    z[61]=3*z[3];
    z[62]=z[23]*z[61];
    z[56]= - z[56] + z[62];
    z[56]=z[3]*z[56];
    z[62]=z[22]*z[4];
    z[63]= - z[6]*z[62];
    z[46]=z[46] + z[63] + z[56] + z[57] + z[60];
    z[46]=z[11]*z[46];
    z[56]= - n<T>(35,4)*z[17] - z[19];
    z[56]=z[12]*z[56];
    z[56]= - n<T>(23,4)*z[22] + z[56];
    z[56]=z[12]*z[56];
    z[57]=z[32]*z[37];
    z[56]=z[56] + z[57];
    z[56]=z[4]*z[56];
    z[57]=n<T>(1,2)*z[19];
    z[60]=z[57] + z[17];
    z[63]=n<T>(1,2)*z[1];
    z[64]=z[63]*z[11];
    z[65]= - z[64] + z[60];
    z[65]=z[27]*z[65];
    z[66]=z[60]*z[12];
    z[66]=z[66] + z[52];
    z[67]=z[66]*z[37];
    z[65]=z[67] + z[65];
    z[65]=z[2]*z[65];
    z[67]=z[17] + n<T>(1,4)*z[19];
    z[67]=z[12]*z[67];
    z[68]=z[11]*z[1];
    z[58]= - z[68] - z[58];
    z[58]=z[11]*z[58];
    z[56]=n<T>(9,2)*z[65] + z[58] + z[67] + z[56];
    z[56]=z[2]*z[56];
    z[58]=35*z[17];
    z[65]=27*z[19];
    z[67]= - z[58] - z[65];
    z[67]=z[12]*z[67];
    z[67]= - z[41] + n<T>(1,4)*z[67];
    z[67]=z[12]*z[67];
    z[67]=z[67] + 4*z[43];
    z[67]=z[5]*z[67];
    z[69]=z[12]*z[25];
    z[69]= - z[22] + z[69];
    z[69]=z[12]*z[69];
    z[43]=z[43] - z[49] + z[69];
    z[43]=z[43]*z[61];
    z[69]=37*z[17];
    z[70]= - z[69] - 31*z[19];
    z[71]=n<T>(1,2)*z[12];
    z[70]=z[70]*z[71];
    z[72]=5*z[22];
    z[70]= - z[72] + z[70];
    z[43]=z[43] + n<T>(1,2)*z[70] + z[67];
    z[43]=z[3]*z[43];
    z[67]=17*z[17];
    z[70]=z[67] - z[19];
    z[70]=z[70]*z[71];
    z[70]=z[72] + z[70];
    z[73]= - z[4]*z[36];
    z[70]=n<T>(1,2)*z[70] + z[73];
    z[70]=z[4]*z[70];
    z[73]=4*z[17];
    z[74]=z[73] + n<T>(31,4)*z[19];
    z[74]=z[12]*z[74];
    z[74]=z[74] - 3*z[34];
    z[74]=z[5]*z[74];
    z[75]=z[22]*z[6];
    z[76]=z[75] - z[26];
    z[24]=z[24] + z[56] + z[46] + z[43] + z[74] + z[70] - z[76];
    z[24]=z[13]*z[24];
    z[43]=3*z[62];
    z[46]=7*z[17];
    z[56]= - z[46] + z[43];
    z[56]=z[4]*z[56];
    z[70]=9*z[1];
    z[74]=z[17]*z[5];
    z[56]= - z[74] - z[70] + z[56];
    z[77]=z[53] - z[17] + z[62];
    z[77]=z[6]*z[77];
    z[56]=n<T>(1,2)*z[56] + z[77];
    z[56]=z[6]*z[56];
    z[77]=z[17]*z[7];
    z[78]=3*z[77];
    z[79]=2*z[1];
    z[80]=z[77] - z[1];
    z[81]=z[80]*z[5];
    z[82]= - z[11]*z[81];
    z[82]=z[82] - z[79] + z[78];
    z[83]=npow(z[7],2);
    z[84]=3*z[83];
    z[82]=z[84]*z[82];
    z[85]=z[5]*z[1];
    z[86]=z[7]*z[80];
    z[86]=n<T>(15,2)*z[86] + z[85];
    z[86]=z[5]*z[86];
    z[87]=z[17]*z[4];
    z[88]=z[4]*z[53];
    z[88]= - z[87] + z[88];
    z[88]=z[6]*z[88];
    z[89]=n<T>(3,2)*z[1];
    z[90]= - z[4]*z[89];
    z[88]=z[90] + z[88];
    z[88]=z[6]*z[88];
    z[82]=z[88] + z[86] + z[82];
    z[82]=z[11]*z[82];
    z[86]=3*z[1];
    z[88]=z[87] - z[86] - n<T>(1,2)*z[77];
    z[88]=z[4]*z[88];
    z[90]= - z[63] - z[77];
    z[91]=n<T>(3,2)*z[26];
    z[92]=z[19] - z[91];
    z[92]=z[5]*z[92];
    z[90]=n<T>(15,2)*z[90] + z[92];
    z[90]=z[5]*z[90];
    z[92]= - z[17] - 3*z[26];
    z[92]=z[7]*z[92];
    z[92]=z[89] + z[92];
    z[93]=3*z[7];
    z[92]=z[92]*z[93];
    z[56]=z[82] + z[56] + z[90] + z[92] + z[88];
    z[56]=z[11]*z[56];
    z[82]=n<T>(9,2)*z[17];
    z[88]= - z[82] + 29*z[19];
    z[88]=z[88]*z[71];
    z[90]=z[62]*z[12];
    z[88]= - z[90] - 4*z[22] + z[88];
    z[88]=z[4]*z[88];
    z[37]=z[37]*z[25];
    z[92]= - z[42] - z[19];
    z[92]=z[12]*z[92];
    z[94]=z[11]*z[57];
    z[92]=z[94] + z[92] - z[37];
    z[92]=z[2]*z[92];
    z[94]=5*z[19];
    z[68]=9*z[92] + n<T>(27,4)*z[68] + z[88] - n<T>(67,4)*z[17] - z[94];
    z[68]=z[2]*z[68];
    z[88]=z[62] + z[38] - z[18] - n<T>(11,2)*z[19];
    z[88]=z[4]*z[88];
    z[33]=z[86]*z[33];
    z[33]=z[33] + n<T>(5,2)*z[26] + n<T>(73,4)*z[17] - z[54];
    z[33]=z[5]*z[33];
    z[92]= - z[73] + n<T>(19,4)*z[19];
    z[92]=z[12]*z[92];
    z[95]=10*z[22];
    z[34]= - 6*z[34] - z[95] + z[92];
    z[34]=z[5]*z[34];
    z[92]=z[73] + z[19];
    z[92]=z[12]*z[92];
    z[96]=z[30]*z[12];
    z[97]=z[96]*z[59];
    z[92]=z[97] + z[47] + z[92];
    z[92]=z[92]*z[61];
    z[65]=z[17] + z[65];
    z[34]=z[92] + n<T>(1,2)*z[65] + z[34];
    z[34]=z[3]*z[34];
    z[65]=n<T>(3,2)*z[17];
    z[92]= - z[65] + z[62];
    z[97]=z[52]*z[5];
    z[53]=z[53] + 3*z[92] + z[97];
    z[53]=z[6]*z[53];
    z[92]=z[22]*z[3];
    z[98]=z[2]*z[22];
    z[76]=z[98] - z[92] + z[76];
    z[76]=z[8]*z[76];
    z[98]=z[49]*z[7];
    z[99]=z[52] + z[98];
    z[99]=z[7]*z[99];
    z[65]=z[65] + z[99];
    z[65]=z[65]*z[93];
    z[24]=z[24] + 4*z[76] + z[68] + z[56] + z[53] + z[34] + z[33] + 
    z[65] + z[88];
    z[24]=z[13]*z[24];
    z[32]= - z[32]*z[27];
    z[33]=z[7]*z[23]*z[31];
    z[32]=z[32] + z[33];
    z[32]=z[7]*z[32];
    z[33]= - z[17]*z[27];
    z[32]=z[33] + z[32];
    z[33]= - z[23]*z[27];
    z[34]=z[27]*z[7];
    z[48]=z[48]*z[34];
    z[33]=z[33] + z[48];
    z[33]=z[4]*z[33];
    z[32]=3*z[32] + z[33];
    z[32]=z[4]*z[32];
    z[33]=z[20]*z[31]*z[93];
    z[48]= - z[46] - 4*z[19];
    z[48]=z[48]*z[27];
    z[33]=z[48] + z[33];
    z[33]=z[7]*z[33];
    z[48]=3*z[6];
    z[53]=z[48]*z[49];
    z[32]= - z[53] + z[32] + z[40] + z[33];
    z[32]=z[8]*z[32];
    z[33]=z[18] - z[54];
    z[33]=z[12]*z[33];
    z[33]=z[72] + z[33];
    z[33]=z[12]*z[33];
    z[56]=z[23]*z[34];
    z[33]=z[33] + 6*z[56];
    z[33]=z[7]*z[33];
    z[56]=5*z[17];
    z[65]=z[56] + z[54];
    z[65]=z[65]*z[12];
    z[33]= - z[65] + z[33];
    z[65]= - z[41] - z[65];
    z[65]=z[12]*z[65];
    z[68]=z[54] + 8*z[17];
    z[76]=z[12]*z[68];
    z[88]=7*z[22];
    z[76]=z[88] + z[76];
    z[76]=z[12]*z[76];
    z[99]=2*z[49];
    z[76]=z[99] + z[76];
    z[100]=z[7]*z[12];
    z[76]=z[76]*z[100];
    z[65]=z[65] + z[76];
    z[65]=z[4]*z[65];
    z[33]=2*z[33] + z[65];
    z[33]=z[4]*z[33];
    z[65]=n<T>(13,2)*z[17];
    z[76]=z[65] + z[54];
    z[76]=z[76]*z[27]*z[93];
    z[101]= - z[56] - z[44];
    z[45]=z[101]*z[45];
    z[45]=z[45] + z[76];
    z[45]=z[7]*z[45];
    z[76]=z[49]*z[3];
    z[101]=z[47] - 2*z[76];
    z[102]=npow(z[1],5);
    z[103]= - z[3]*z[102];
    z[103]=z[49] + z[103];
    z[103]=z[6]*z[103];
    z[101]=2*z[101] + n<T>(3,2)*z[103];
    z[101]=z[6]*z[101];
    z[103]=npow(z[5],2);
    z[104]=z[27]*z[103]*z[52];
    z[91]=z[27]*z[91];
    z[105]=z[22]*z[12];
    z[91]=z[105] + z[91];
    z[91]=z[7]*z[91];
    z[91]=z[104] + z[41] + z[91];
    z[91]=z[2]*z[91];
    z[104]=z[22]*z[5];
    z[106]=z[104]*z[12];
    z[107]=z[22] - z[106];
    z[108]=n<T>(1,2)*z[5];
    z[107]=z[107]*z[108];
    z[32]=z[32] + z[91] + z[101] - 5*z[92] + z[107] + z[33] + z[45] + 
    z[82] - z[94];
    z[32]=z[8]*z[32];
    z[33]=6*z[17];
    z[45]= - z[33] - n<T>(31,2)*z[19];
    z[45]=z[12]*z[45];
    z[23]=z[23]*z[100];
    z[82]=n<T>(7,2)*z[22];
    z[23]=18*z[23] - z[82] + z[45];
    z[23]=z[7]*z[23];
    z[40]=z[72] + 2*z[40];
    z[40]=z[12]*z[40];
    z[40]=z[49] + z[40];
    z[40]=z[7]*z[40];
    z[21]=z[40] - z[22] - 2*z[21];
    z[21]=z[4]*z[21];
    z[21]=z[21] + z[23] - 16*z[17] - n<T>(5,2)*z[19];
    z[21]=z[4]*z[21];
    z[23]= - z[41] + n<T>(5,2)*z[76];
    z[23]=z[23]*z[48];
    z[40]=11*z[17];
    z[23]=z[23] + 12*z[92] + z[40] + n<T>(19,2)*z[104];
    z[23]=z[6]*z[23];
    z[45]=z[12]*z[26];
    z[45]=n<T>(15,2)*z[45] + z[22] - n<T>(7,2)*z[29];
    z[45]=z[7]*z[45];
    z[91]=z[71]*z[104];
    z[91]=z[91] - z[22] + n<T>(5,2)*z[29];
    z[91]=z[5]*z[91];
    z[101]=z[52]*z[4];
    z[45]=z[91] + z[101] - z[46] + z[45];
    z[45]=z[2]*z[45];
    z[91]=21*z[19];
    z[107]= - 55*z[17] - z[91];
    z[109]=3*z[12];
    z[68]=z[7]*z[68]*z[109];
    z[68]=n<T>(1,2)*z[107] + z[68];
    z[68]=z[7]*z[68];
    z[107]=z[29]*z[7];
    z[110]=9*z[17];
    z[111]= - z[104] + z[110] - z[107];
    z[111]=z[111]*z[108];
    z[112]=n<T>(3,2)*z[22];
    z[35]=z[112] + z[35];
    z[35]=z[7]*z[35];
    z[35]= - n<T>(3,2)*z[62] + z[40] + z[35];
    z[35]=z[3]*z[35];
    z[21]=z[32] + z[45] + z[23] + z[35] + z[111] + z[21] + z[89] + z[68]
   ;
    z[21]=z[8]*z[21];
    z[23]=z[49]*z[4];
    z[32]=z[23] + z[22];
    z[35]= - z[22]*z[109];
    z[35]=z[49] + z[35];
    z[35]=z[7]*z[35];
    z[35]=z[35] + z[32];
    z[45]=3*z[49];
    z[68]= - z[45] + z[105];
    z[109]=z[22]*z[71];
    z[109]=z[49] + z[109];
    z[109]=z[12]*z[109];
    z[109]=n<T>(1,2)*z[102] + z[109];
    z[109]=z[109]*z[93];
    z[68]=n<T>(1,2)*z[68] + z[109];
    z[109]=z[27]*z[104];
    z[68]=n<T>(1,2)*z[68] + z[109];
    z[68]=z[68]*z[61];
    z[35]=z[68] + n<T>(1,2)*z[35] - z[106];
    z[35]=z[3]*z[35];
    z[68]=z[96] - z[49];
    z[106]=z[49]*z[12];
    z[109]=z[106] - z[102];
    z[111]=z[5]*z[109];
    z[111]=z[111] - z[68];
    z[111]=z[4]*z[111];
    z[113]=npow(z[1],6);
    z[114]=z[113]*z[4];
    z[109]=z[114] - z[109];
    z[109]=z[109]*z[5];
    z[114]=z[102]*z[4];
    z[115]=z[114] + z[49];
    z[109]=z[109] - z[115];
    z[116]=z[3]*z[109];
    z[111]=z[116] - z[29] + z[111];
    z[116]=n<T>(1,2)*z[2];
    z[111]=z[111]*z[116];
    z[117]=z[5]*z[71]*z[23];
    z[90]=z[117] - z[22] + n<T>(9,4)*z[90];
    z[90]=z[5]*z[90];
    z[117]=z[98] - z[22];
    z[118]=z[102]*z[7];
    z[119]=z[118] - z[49];
    z[120]=z[3]*z[119];
    z[120]=z[120] - z[117];
    z[121]=2*z[6];
    z[120]=z[120]*z[121];
    z[122]=7*z[19];
    z[123]= - z[17] + z[122];
    z[124]=n<T>(1,4)*z[17];
    z[125]= - z[124] + z[44];
    z[125]=z[12]*z[125];
    z[125]= - z[52] + 7*z[125];
    z[125]=z[4]*z[125];
    z[126]=15*z[26];
    z[127]=13*z[17];
    z[128]=z[127] - z[126];
    z[128]=z[6]*z[128];
    z[128]=37*z[1] + z[128];
    z[129]=z[6]*z[1];
    z[130]=z[129]*z[11];
    z[128]=n<T>(1,4)*z[128] + 9*z[130];
    z[128]=z[11]*z[128];
    z[35]=z[111] + z[128] + z[120] + z[35] + z[90] + z[125] + 2*z[123]
    - n<T>(9,2)*z[26];
    z[35]=z[2]*z[35];
    z[90]=n<T>(1,2)*z[3];
    z[109]=z[109]*z[90];
    z[111]=9*z[29];
    z[88]=z[88] + z[111];
    z[88]=z[88]*z[71];
    z[45]= - z[45] + z[88];
    z[45]=z[7]*z[45];
    z[88]=n<T>(1,2)*z[49];
    z[96]=z[88] + 6*z[96];
    z[96]=z[5]*z[96];
    z[120]=n<T>(1,2)*z[23];
    z[45]=z[109] + z[96] - z[120] + z[45] + z[95] + n<T>(3,2)*z[29];
    z[45]=z[3]*z[45];
    z[95]=z[105] + z[49];
    z[96]=z[95]*z[7];
    z[109]=7*z[96] - z[72] - z[29];
    z[109]=z[7]*z[109];
    z[109]= - 69*z[17] + z[109];
    z[123]=13*z[22];
    z[111]= - z[123] - z[111];
    z[125]= - 17*z[49] - 5*z[114];
    z[125]=z[4]*z[125];
    z[111]=n<T>(1,2)*z[111] + z[125];
    z[125]= - n<T>(5,2)*z[105] - z[115];
    z[125]=z[5]*z[125];
    z[111]=n<T>(1,2)*z[111] + z[125];
    z[111]=z[5]*z[111];
    z[123]= - z[123] + n<T>(5,2)*z[23];
    z[123]=z[4]*z[123];
    z[45]=z[45] + z[111] + n<T>(1,4)*z[109] + z[123];
    z[45]=z[3]*z[45];
    z[109]=25*z[22] + 13*z[98];
    z[111]=2*z[3];
    z[111]= - z[119]*z[111];
    z[109]=n<T>(1,4)*z[109] + z[111];
    z[109]=z[3]*z[109];
    z[101]=z[109] - z[97] - z[101] + z[17] - n<T>(9,4)*z[26];
    z[101]=z[6]*z[101];
    z[109]=n<T>(45,2)*z[22] + z[29];
    z[111]=n<T>(5,2)*z[49] - z[105];
    z[111]=z[4]*z[111];
    z[109]=n<T>(1,2)*z[109] + z[111];
    z[109]=z[4]*z[109];
    z[111]=2*z[23];
    z[119]=z[12]*z[111];
    z[88]=z[119] + z[88] - z[105];
    z[88]=z[4]*z[88];
    z[88]= - z[52] + z[88];
    z[88]=z[5]*z[88];
    z[88]=z[88] - z[18] + z[109];
    z[88]=z[5]*z[88];
    z[109]=z[4]*z[30];
    z[119]=13*z[19];
    z[69]= - 5*z[109] + z[69] - z[119];
    z[109]=n<T>(1,2)*z[4];
    z[69]=z[69]*z[109];
    z[33]= - z[33] + n<T>(31,4)*z[26];
    z[33]=z[7]*z[33];
    z[65]=z[65] + 7*z[26];
    z[65]=z[7]*z[65];
    z[65]=z[74] - 35*z[1] + z[65];
    z[123]=n<T>(1,2)*z[6];
    z[65]=z[11]*z[65]*z[123];
    z[33]=z[35] + z[65] + z[101] + z[45] + z[88] + z[69] + n<T>(69,4)*z[1]
    + z[33];
    z[33]=z[2]*z[33];
    z[35]=z[29] - z[22];
    z[45]=z[4]*z[35];
    z[65]=z[23]*z[3];
    z[45]=z[65] + z[17] + z[45];
    z[45]=z[45]*z[116];
    z[69]=z[110] - n<T>(19,2)*z[19];
    z[69]=z[4]*z[69];
    z[88]= - z[43] + n<T>(1,4)*z[92];
    z[61]=z[88]*z[61];
    z[88]=z[92] - z[17];
    z[101]=z[6]*z[88];
    z[45]=z[45] - n<T>(11,2)*z[130] + 5*z[101] + z[61] - n<T>(41,4)*z[1] + z[69]
   ;
    z[45]=z[2]*z[45];
    z[61]=z[65] + z[39] + n<T>(19,2)*z[62];
    z[61]=z[61]*z[90];
    z[65]= - z[110] - n<T>(5,2)*z[62];
    z[65]=z[4]*z[65];
    z[61]=z[65] + z[61];
    z[61]=z[3]*z[61];
    z[65]= - z[42] + z[92];
    z[65]=z[3]*z[65];
    z[69]= - z[56] + 7*z[92];
    z[69]=z[6]*z[69];
    z[65]=n<T>(1,4)*z[69] - z[87] + n<T>(15,2)*z[65];
    z[65]=z[6]*z[65];
    z[69]=z[86] + 5*z[87];
    z[69]=z[69]*z[109];
    z[101]=npow(z[6],2);
    z[64]=z[101]*z[64];
    z[45]=z[45] + z[64] + z[65] + z[69] + z[61];
    z[45]=z[2]*z[45];
    z[61]=z[47] + z[76];
    z[61]=z[61]*z[48];
    z[61]=z[61] + 4*z[92] + z[67] + 8*z[104];
    z[61]=z[6]*z[61];
    z[64]=z[86]*z[27];
    z[37]= - z[64] - z[37];
    z[37]=z[4]*z[37];
    z[53]= - z[41] - z[53];
    z[53]=z[6]*z[53];
    z[37]=z[53] + z[37] + z[56] - z[44];
    z[37]=z[8]*z[37];
    z[53]= - z[18] - 9*z[19];
    z[65]= - z[18] - z[54];
    z[65]=z[4]*z[12]*z[65];
    z[53]=2*z[53] + z[65];
    z[53]=z[4]*z[53];
    z[65]=12*z[17] - z[97];
    z[65]=z[5]*z[65];
    z[69]= - z[3]*z[62];
    z[37]=z[37] + z[61] + z[69] + z[65] - 14*z[1] + z[53];
    z[37]=z[8]*z[37];
    z[53]=5*z[1];
    z[61]= - z[53] + z[87];
    z[65]= - 32*z[17] + z[104];
    z[65]=z[5]*z[65];
    z[69]= - z[18] - n<T>(5,2)*z[92];
    z[69]=z[69]*z[48];
    z[76]=z[17]*z[3];
    z[61]=z[69] - n<T>(7,2)*z[76] + n<T>(3,2)*z[61] + z[65];
    z[61]=z[6]*z[61];
    z[65]= - z[2]*z[18];
    z[65]=z[65] - z[1] + z[87];
    z[65]=z[4]*z[65];
    z[69]=z[44] - z[17];
    z[97]= - z[5]*z[69];
    z[97]= - n<T>(39,2)*z[1] + z[97];
    z[97]=z[5]*z[97];
    z[110]=z[56] - z[62];
    z[110]=z[3]*z[110]*z[109];
    z[37]=z[37] + z[61] + z[110] + z[97] + z[65];
    z[37]=z[8]*z[37];
    z[61]=z[5]*z[49];
    z[61]=z[47] + z[61];
    z[61]=z[61]*z[48];
    z[61]=z[61] + z[127] + 12*z[104];
    z[61]=z[6]*z[61];
    z[65]= - z[4]*z[28];
    z[65]= - 6*z[19] + z[65];
    z[65]=z[4]*z[65];
    z[53]=z[61] + 9*z[74] - z[53] + z[65];
    z[53]=z[8]*z[53];
    z[52]=z[52]*z[3];
    z[61]= - z[52] - n<T>(15,2)*z[17] - 4*z[104];
    z[61]=z[61]*z[48];
    z[65]= - 50*z[17] - z[104];
    z[65]=z[5]*z[65];
    z[61]=z[61] - 23*z[1] + z[65];
    z[61]=z[6]*z[61];
    z[65]= - z[54] - n<T>(7,2)*z[17];
    z[65]=z[5]*z[65];
    z[65]= - 30*z[1] + z[65];
    z[65]=z[5]*z[65];
    z[50]= - z[50]*z[52];
    z[50]=z[53] + z[61] + z[50] + z[51] + z[65];
    z[50]=z[8]*z[50];
    z[51]=4*z[1] + 5*z[74];
    z[51]=z[51]*z[48];
    z[51]=n<T>(71,2)*z[85] + z[51];
    z[51]=z[6]*z[51];
    z[53]=z[103]*z[1];
    z[50]=z[50] + 4*z[53] + z[51];
    z[50]=z[8]*z[50];
    z[51]=z[103]*z[86];
    z[61]=z[18]*z[5];
    z[65]=21*z[1] + z[61];
    z[65]=z[5]*z[65];
    z[61]=z[1] + z[61];
    z[61]=z[6]*z[61];
    z[61]=z[65] + 6*z[61];
    z[61]=z[6]*z[61];
    z[51]=z[51] + z[61];
    z[61]= - 24*z[17] - z[104];
    z[61]=z[5]*z[61];
    z[65]=z[104] + z[17];
    z[97]=z[6]*z[65];
    z[61]= - 9*z[97] - 8*z[1] + z[61];
    z[61]=z[6]*z[61];
    z[20]= - z[5]*z[20];
    z[20]= - z[70] + z[20];
    z[20]=z[5]*z[20];
    z[20]=z[20] + z[61];
    z[20]=z[8]*z[20];
    z[20]=2*z[51] + z[20];
    z[20]=z[8]*z[20];
    z[51]=z[79]*z[103];
    z[61]=z[129]*z[5];
    z[97]= - z[51] - 15*z[61];
    z[97]=z[6]*z[97];
    z[20]=z[97] + z[20];
    z[20]=z[8]*z[20];
    z[97]=6*z[1];
    z[103]=z[97] + z[74];
    z[59]=z[103]*z[59];
    z[103]=z[5]*z[39];
    z[103]=z[1] + z[103];
    z[103]=z[103]*z[48];
    z[59]=z[59] + z[103];
    z[59]=z[6]*z[59];
    z[51]=z[51] + z[59];
    z[51]=z[8]*z[51];
    z[59]= - z[53] - 4*z[61];
    z[48]=z[59]*z[48];
    z[48]=z[48] + z[51];
    z[51]=npow(z[8],2);
    z[48]=z[48]*z[51];
    z[59]= - z[6]*z[5]*z[86];
    z[53]= - z[53] + z[59];
    z[59]=z[10]*npow(z[8],3);
    z[53]=z[6]*z[53]*z[59];
    z[48]=z[48] + z[53];
    z[48]=z[10]*z[48];
    z[20]=z[20] + z[48];
    z[20]=z[10]*z[20];
    z[48]=z[4]*z[52];
    z[48]= - z[87] + z[48];
    z[48]=z[3]*z[48];
    z[53]=z[4]*z[63];
    z[48]=z[53] + z[48];
    z[53]= - z[18] + z[92];
    z[53]=z[3]*z[53];
    z[53]=z[1] + z[53];
    z[53]=z[6]*z[53];
    z[48]=n<T>(3,2)*z[48] + z[53];
    z[48]=z[48]*npow(z[2],2);
    z[53]=z[74]*z[6];
    z[61]= - z[85] + z[53];
    z[103]=n<T>(1,2)*z[101];
    z[61]=z[61]*z[103];
    z[20]=z[20] + z[50] + z[61] + 3*z[48];
    z[20]=z[10]*z[20];
    z[48]= - z[65]*z[123];
    z[48]=z[48] - z[97] - n<T>(1,4)*z[74];
    z[48]=z[6]*z[48];
    z[48]=15*z[85] + z[48];
    z[48]=z[6]*z[48];
    z[50]=29*z[85] + z[53];
    z[50]=z[11]*z[50]*z[103];
    z[20]=z[20] + z[37] + z[45] + z[48] + z[50];
    z[20]=z[10]*z[20];
    z[37]=z[112] + z[23];
    z[37]=z[37]*z[109];
    z[45]=n<T>(29,2)*z[17];
    z[37]=z[37] + z[45] - z[126];
    z[37]=z[5]*z[37];
    z[48]= - z[98] + z[32];
    z[48]=z[5]*z[48];
    z[50]=z[7]*z[23];
    z[48]=z[48] + z[17] + z[50];
    z[48]=z[48]*z[123];
    z[50]=z[26]*z[3];
    z[53]= - 27*z[1] + z[77];
    z[61]=2*z[26];
    z[65]=z[4]*z[61];
    z[37]=z[48] + n<T>(43,4)*z[50] + z[37] + n<T>(1,2)*z[53] + z[65];
    z[37]=z[6]*z[37];
    z[48]=z[111] + z[22];
    z[53]=z[48]*z[5];
    z[65]=z[53] + n<T>(21,8)*z[17] + 8*z[62];
    z[65]=z[4]*z[65];
    z[85]= - z[7]*z[117];
    z[45]=z[45] + z[85];
    z[85]=n<T>(1,2)*z[7];
    z[45]=z[45]*z[85];
    z[45]= - 12*z[1] + z[45] + z[65];
    z[45]=z[5]*z[45];
    z[65]=z[7]*z[18];
    z[65]= - z[1] + z[65];
    z[65]=z[65]*z[93];
    z[104]=n<T>(1,2)*z[98];
    z[110]=z[47] + z[104];
    z[110]=z[7]*z[110];
    z[110]= - n<T>(67,8)*z[17] + z[110];
    z[111]=z[4]*z[7];
    z[110]=z[110]*z[111];
    z[112]=n<T>(113,8)*z[17] + 6*z[26];
    z[112]=z[7]*z[112];
    z[112]=z[112] + n<T>(7,8)*z[50];
    z[112]=z[3]*z[112];
    z[37]=z[37] + z[112] + z[45] + z[65] + z[110];
    z[37]=z[6]*z[37];
    z[45]=n<T>(3,4)*z[22] + z[23];
    z[45]=z[4]*z[45];
    z[65]= - z[29] + z[32];
    z[65]=z[3]*z[65];
    z[45]=z[65] + z[45] + n<T>(3,4)*z[17] - z[19];
    z[45]=z[5]*z[45];
    z[65]=n<T>(9,4)*z[17];
    z[110]= - z[65] - z[19];
    z[110]=z[7]*z[110];
    z[45]=z[110] + z[45];
    z[45]=z[45]*z[90];
    z[90]=z[40] - z[19];
    z[110]=z[66]*z[93];
    z[90]=n<T>(1,2)*z[90] + z[110];
    z[90]=z[7]*z[90];
    z[90]=z[97] + z[90];
    z[90]=z[7]*z[90];
    z[48]=z[48]*z[4];
    z[48]=z[48] - z[17] + z[19];
    z[48]=z[5]*z[48];
    z[65]=z[65] - 5*z[62];
    z[65]=z[65]*z[109];
    z[48]=z[48] + 10*z[1] + z[65];
    z[48]=z[5]*z[48];
    z[65]= - z[77]*z[109];
    z[45]=z[45] + z[48] + z[90] + z[65];
    z[45]=z[3]*z[45];
    z[48]=z[26] - z[18];
    z[48]=z[48]*z[7];
    z[48]=z[48] + z[1];
    z[48]=z[48]*z[5];
    z[65]=z[79] - n<T>(3,2)*z[77];
    z[65]=z[65]*z[93];
    z[90]=z[83]*z[92];
    z[97]=z[26]*z[109];
    z[97]= - 3*z[80] + z[97];
    z[97]=z[6]*z[97];
    z[110]= - z[4]*z[77];
    z[65]=z[97] - n<T>(15,2)*z[90] + 12*z[48] + z[65] + z[110];
    z[65]=z[6]*z[65];
    z[38]= - z[39] - z[38];
    z[38]=z[3]*z[38];
    z[38]=z[38] - 2*z[80];
    z[38]=z[83]*z[38];
    z[80]= - z[63]*z[111];
    z[90]=z[7]*z[81];
    z[38]= - n<T>(5,2)*z[90] + z[80] + z[38];
    z[38]=3*z[38] + z[65];
    z[38]=z[6]*z[38];
    z[48]= - z[7]*z[48];
    z[65]=npow(z[7],3);
    z[52]=z[65]*z[52];
    z[48]=z[48] + z[52];
    z[48]=z[6]*z[48];
    z[52]=z[83]*z[81];
    z[76]=z[65]*z[76];
    z[48]=z[48] + z[52] + z[76];
    z[48]=z[6]*z[48];
    z[52]=z[65]*z[63];
    z[76]=z[3]*z[52];
    z[48]=z[76] + z[48];
    z[76]=3*z[11];
    z[48]=z[48]*z[76];
    z[80]= - z[7]*z[60];
    z[80]= - z[63] + z[80];
    z[80]=z[3]*z[80]*z[83];
    z[80]=z[52] + z[80];
    z[38]=z[48] + 3*z[80] + z[38];
    z[38]=z[11]*z[38];
    z[48]=z[127] + z[122];
    z[80]=z[71]*z[17];
    z[81]=z[22] + z[80];
    z[81]=z[7]*z[81];
    z[48]=n<T>(1,2)*z[48] + z[81];
    z[48]=z[7]*z[48];
    z[81]=11*z[1];
    z[48]= - z[81] + z[48];
    z[48]=z[48]*z[111];
    z[90]= - z[7]*z[25];
    z[90]= - z[1] + z[90];
    z[90]=z[90]*z[83];
    z[37]=z[38] + z[37] + z[45] + n<T>(3,2)*z[90] + z[48];
    z[37]=z[11]*z[37];
    z[38]=z[66]*z[34];
    z[27]= - z[60]*z[27];
    z[27]=z[27] + z[38];
    z[27]=z[3]*z[27];
    z[45]=n<T>(3,2)*z[19];
    z[40]=z[40] + z[45];
    z[40]=z[12]*z[40];
    z[48]= - z[55]*z[71];
    z[48]=z[22] + z[48];
    z[48]=z[48]*z[100];
    z[27]=9*z[27] + z[40] + z[48];
    z[27]=z[3]*z[27];
    z[40]= - z[100] + 1;
    z[40]=z[55]*z[40];
    z[27]=z[27] + z[40];
    z[40]=z[17]*z[6];
    z[48]=z[1] - z[40];
    z[48]=2*z[48] + z[130];
    z[48]=z[11]*z[48];
    z[48]=z[48] + z[75] - z[73] + z[19];
    z[48]=z[11]*z[48];
    z[35]= - 2*z[35] + z[48];
    z[35]=z[11]*z[35];
    z[35]=z[105] + z[35];
    z[35]=z[2]*z[35];
    z[48]= - z[17] + z[91];
    z[48]=n<T>(1,2)*z[48] - z[75];
    z[55]=z[6]*z[56];
    z[55]= - n<T>(9,2)*z[130] + n<T>(3,4)*z[1] + z[55];
    z[55]=z[11]*z[55];
    z[48]=n<T>(1,2)*z[48] + z[55];
    z[48]=z[11]*z[48];
    z[55]= - z[22] - n<T>(21,2)*z[29];
    z[35]=3*z[35] + n<T>(1,2)*z[55] + z[48];
    z[35]=z[2]*z[35];
    z[48]=z[3]*z[28];
    z[48]= - z[44] + n<T>(3,4)*z[48];
    z[48]=z[3]*z[48];
    z[48]= - z[63] + z[48];
    z[55]= - z[6]*z[42];
    z[48]=n<T>(3,2)*z[130] + 3*z[48] + z[55];
    z[48]=z[11]*z[48];
    z[27]=z[35] + n<T>(1,2)*z[27] + z[48];
    z[27]=z[16]*z[27];
    z[35]=z[36] - z[38];
    z[35]=z[35]*z[93];
    z[36]=15*z[17] + z[19];
    z[36]=z[12]*z[36];
    z[36]=29*z[22] + z[36];
    z[35]=n<T>(1,4)*z[36] + z[35];
    z[35]=z[7]*z[35];
    z[36]=z[57] - z[17];
    z[36]=z[36]*z[12];
    z[30]=z[30]*z[100];
    z[30]=z[36] + z[30];
    z[30]=z[3]*z[30];
    z[38]=n<T>(5,2)*z[17];
    z[30]=n<T>(9,2)*z[30] + z[35] + z[38] - z[119];
    z[30]=z[3]*z[30];
    z[35]=2*z[130];
    z[48]= - z[6]*z[18];
    z[48]=z[35] + z[86] + z[48];
    z[48]=z[11]*z[48];
    z[48]=z[48] - z[39] + z[19];
    z[48]=z[11]*z[48];
    z[48]= - z[29] + z[48];
    z[48]=z[2]*z[48];
    z[55]= - z[17] + z[54];
    z[55]=7*z[55] + n<T>(3,2)*z[75];
    z[56]= - z[6]*z[39];
    z[56]= - n<T>(31,2)*z[130] + n<T>(13,4)*z[1] + z[56];
    z[56]=z[11]*z[56];
    z[48]=6*z[48] + n<T>(1,2)*z[55] + z[56];
    z[48]=z[2]*z[48];
    z[25]=z[25]*z[34];
    z[25]= - z[36] + n<T>(1,2)*z[25];
    z[25]=z[25]*z[93];
    z[34]= - 27*z[17] - z[94];
    z[25]=n<T>(1,2)*z[34] + z[25];
    z[25]=z[7]*z[25];
    z[34]=z[87] - z[74];
    z[25]=z[27] + z[48] + z[35] + n<T>(17,2)*z[40] + z[30] + z[1] + z[25] - 
   4*z[34];
    z[25]=z[16]*z[25];
    z[27]=z[7]*z[1];
    z[30]=z[31]*z[27];
    z[30]= - z[64] + z[30];
    z[30]=z[7]*z[30];
    z[31]=z[49]*z[101];
    z[30]=z[31] + z[30] + z[69];
    z[30]=z[8]*z[30];
    z[28]=z[28]*z[7];
    z[31]= - z[44] + z[28];
    z[31]=z[7]*z[31];
    z[34]= - z[22]*z[101];
    z[31]=z[34] + z[1] + z[31];
    z[30]=2*z[31] + z[30];
    z[30]=z[8]*z[30];
    z[31]=z[7]*z[19];
    z[31]= - z[79] + z[31];
    z[31]=z[7]*z[31];
    z[30]=z[30] + z[31] + z[129];
    z[30]=z[8]*z[30];
    z[31]=z[101]*z[73];
    z[34]= - z[101]*z[47];
    z[34]=z[1] + z[34];
    z[34]=z[8]*z[34];
    z[31]=z[31] + z[34];
    z[31]=z[31]*z[51];
    z[34]=z[8]*z[39];
    z[34]= - z[79] + z[34];
    z[34]=z[51]*z[101]*z[34];
    z[35]=z[101]*z[1];
    z[36]= - z[59]*z[35];
    z[34]=z[34] + z[36];
    z[34]=z[10]*z[34];
    z[31]=z[31] + z[34];
    z[31]=z[10]*z[31];
    z[34]=npow(z[6],3);
    z[34]=z[34] - z[65];
    z[36]= - z[13]*z[17];
    z[36]=z[36] + z[1];
    z[34]=z[9]*z[34]*z[36];
    z[30]=z[34] + z[30] + z[31];
    z[31]=z[42] + z[61];
    z[31]=z[31]*z[84];
    z[34]= - z[39] + z[75];
    z[34]=z[34]*z[103];
    z[36]=z[1] - z[78];
    z[36]=z[36]*z[83];
    z[35]=z[36] - z[35];
    z[35]=z[35]*z[76];
    z[31]=z[35] + z[31] + z[34];
    z[31]=z[13]*z[31];
    z[34]= - z[83]*z[89];
    z[35]= - z[27] - z[129];
    z[35]=z[35]*z[121];
    z[34]=z[34] + z[35];
    z[34]=z[6]*z[34];
    z[34]=z[52] + z[34];
    z[34]=z[34]*z[76];
    z[35]= - z[18] - z[57];
    z[35]=z[7]*z[35];
    z[35]=z[89] + z[35];
    z[35]=z[35]*z[84];
    z[36]=z[70] - z[40];
    z[36]=z[6]*z[36];
    z[27]= - 15*z[27] + z[36];
    z[27]=z[27]*z[123];
    z[28]=z[54] - z[28];
    z[28]=z[7]*z[28];
    z[28]= - z[86] + z[28];
    z[28]=z[7]*z[28];
    z[28]=z[28] + z[129];
    z[28]=z[16]*z[28];
    z[27]=n<T>(3,2)*z[28] + z[31] + z[34] + z[35] + z[27] + 3*z[30];
    z[27]=z[9]*z[27];
    z[28]= - z[10]*z[3]*z[88];
    z[28]=z[28] - z[17];
    z[28]=z[2]*z[28];
    z[30]=z[77] + z[50];
    z[30]=z[11]*z[3]*z[30];
    z[31]=z[18]*z[16];
    z[34]= - npow(z[3],2)*z[41];
    z[28]= - z[31] + z[30] + z[34] - z[79] + z[77] + z[28];
    z[28]=z[15]*z[28];
    z[30]=z[29] + z[96];
    z[30]=z[3]*z[30];
    z[34]=z[17] - z[26];
    z[34]=z[11]*z[6]*z[34];
    z[30]=z[34] + z[30];
    z[30]=z[7]*z[30];
    z[34]= - z[22]*z[83];
    z[34]= - z[1] + z[34];
    z[30]= - z[31] - z[40] + 2*z[34] + z[30];
    z[30]=z[14]*z[30];
    z[28]=z[30] + z[28];
    z[30]=z[120] + z[22];
    z[31]= - z[80] + z[30];
    z[31]=z[5]*z[31];
    z[34]= - z[105] + z[115];
    z[34]=z[34]*z[108];
    z[30]=z[34] - z[30];
    z[30]=z[3]*z[30];
    z[34]= - z[47] - z[29];
    z[34]=z[7]*z[34];
    z[35]=n<T>(5,4)*z[22] - z[23];
    z[35]=z[35]*z[109];
    z[30]=z[30] + z[31] + z[35] + n<T>(3,2)*z[60] + z[34];
    z[30]=z[3]*z[30];
    z[31]= - z[38] + z[54];
    z[31]=z[12]*z[31];
    z[31]=z[31] - z[32];
    z[31]=z[5]*z[31];
    z[32]= - n<T>(95,4)*z[17] - z[19];
    z[34]= - n<T>(179,4)*z[22] - 9*z[23];
    z[34]=z[34]*z[109];
    z[31]=z[31] + z[34] + n<T>(1,2)*z[32] - z[107];
    z[31]=z[5]*z[31];
    z[32]= - z[38] + z[19];
    z[32]=z[12]*z[32];
    z[32]= - z[82] + z[32];
    z[34]= - z[66]*z[100];
    z[32]=n<T>(1,2)*z[32] + z[34];
    z[32]=z[32]*z[93];
    z[32]=z[32] - n<T>(115,8)*z[17] - z[44];
    z[32]=z[7]*z[32];
    z[34]=n<T>(25,4)*z[17] + z[26];
    z[34]=n<T>(1,2)*z[34] + z[43];
    z[34]=z[4]*z[34];
    z[30]=z[30] + z[31] + z[34] + n<T>(1,4)*z[1] + z[32];
    z[30]=z[3]*z[30];
    z[31]=z[7]*z[113];
    z[31]= - z[102] + z[31];
    z[31]=z[4]*z[31];
    z[31]=z[118] + z[31];
    z[31]=z[5]*z[31];
    z[32]= - z[7]*z[114];
    z[31]=z[31] + z[22] + z[32];
    z[31]=z[31]*z[123];
    z[32]= - z[22] + z[104];
    z[32]=z[4]*z[32];
    z[34]=z[82] - z[98];
    z[23]=n<T>(1,2)*z[34] + z[23];
    z[23]=z[5]*z[23];
    z[23]=z[31] - 9*z[92] + z[23] + n<T>(19,4)*z[17] + z[32];
    z[23]=z[6]*z[23];
    z[31]=z[118] + z[49];
    z[31]=z[31]*z[7];
    z[32]=n<T>(49,4)*z[22] - z[31];
    z[32]=z[7]*z[32];
    z[32]=z[62] - z[46] + z[32];
    z[32]=z[4]*z[32];
    z[32]=z[32] - z[81] - n<T>(45,2)*z[77];
    z[31]=n<T>(9,2)*z[22] + z[31];
    z[31]=z[7]*z[31];
    z[35]=z[113]*z[83];
    z[35]= - n<T>(83,4)*z[49] + z[35];
    z[35]=z[7]*z[35];
    z[35]=n<T>(203,4)*z[22] + z[35];
    z[35]=z[4]*z[35];
    z[31]=z[35] - n<T>(203,4)*z[17] + z[31];
    z[31]=n<T>(1,2)*z[31] + z[53];
    z[31]=z[5]*z[31];
    z[26]=z[58] - 33*z[26];
    z[35]=n<T>(29,8)*z[22] - 2*z[98];
    z[35]=z[3]*z[35];
    z[26]=n<T>(1,8)*z[26] + z[35];
    z[26]=z[3]*z[26];
    z[23]=z[23] + z[26] + n<T>(1,2)*z[32] + z[31];
    z[23]=z[6]*z[23];
    z[26]=z[49]*z[71];
    z[26]=z[102] + z[26];
    z[26]=z[7]*z[26];
    z[26]= - n<T>(11,4)*z[95] + z[26];
    z[26]=z[7]*z[26];
    z[29]=z[72] + n<T>(13,2)*z[29];
    z[26]=n<T>(1,2)*z[29] + z[26];
    z[26]=z[7]*z[26];
    z[29]=z[41] - z[96];
    z[29]=z[4]*z[29];
    z[26]=z[29] - z[124] + z[26];
    z[26]=z[4]*z[26];
    z[29]= - z[49] - n<T>(3,2)*z[105];
    z[31]=z[106] + z[102];
    z[32]=z[31]*z[85];
    z[29]=3*z[29] + z[32];
    z[29]=z[7]*z[29];
    z[31]=z[7]*z[31];
    z[31]= - z[99] + z[31];
    z[32]=2*z[4];
    z[31]=z[31]*z[32];
    z[29]=z[31] + n<T>(11,2)*z[22] + z[29];
    z[29]=z[4]*z[29];
    z[31]= - z[72] + z[104];
    z[31]=z[7]*z[31];
    z[29]=z[29] + 10*z[17] + z[31];
    z[29]=z[5]*z[29];
    z[31]= - z[34]*z[85];
    z[31]=20*z[17] + z[31];
    z[31]=z[7]*z[31];
    z[26]=z[29] + z[26] - n<T>(19,4)*z[1] + z[31];
    z[26]=z[5]*z[26];
    z[18]=z[18] + z[45];
    z[18]=z[12]*z[18];
    z[18]= - z[47] + z[18];
    z[18]=z[7]*z[18];
    z[18]=z[18] + z[67] + z[54];
    z[18]=z[7]*z[18];
    z[18]=z[89] + z[18];
    z[18]=z[7]*z[18];
    z[29]=n<T>(21,2)*z[17] + z[94];
    z[29]=z[12]*z[29];
    z[31]=z[7]*z[68];
    z[22]=z[31] + n<T>(53,2)*z[22] + 5*z[29];
    z[22]=z[22]*z[85];
    z[17]=z[22] - n<T>(121,8)*z[17] - 25*z[19];
    z[17]=z[7]*z[17];
    z[17]=10*z[87] + n<T>(17,4)*z[1] + z[17];
    z[17]=z[4]*z[17];

    r += z[17] + z[18] + z[20] + z[21] + z[23] + z[24] + z[25] + z[26]
       + z[27] + 2*z[28] + z[30] + z[33] + z[37];
 
    return r;
}

template double qqb_2lNLC_r336(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r336(const std::array<dd_real,31>&);
#endif
