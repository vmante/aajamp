#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r980(const std::array<T,31>& k) {
  T z[67];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[8];
    z[12]=k[15];
    z[13]=k[2];
    z[14]=npow(z[10],2);
    z[15]=z[14]*z[7];
    z[16]=z[10] + z[8];
    z[17]=z[15] + z[16];
    z[18]= - z[10]*z[16];
    z[19]=npow(z[10],3);
    z[20]= - z[7]*z[19];
    z[18]=z[18] + z[20];
    z[18]=z[6]*z[18];
    z[18]=z[18] + z[17];
    z[18]=z[6]*z[18];
    z[20]=z[4]*z[8];
    z[21]=z[20] + 1;
    z[22]=z[7]*z[10];
    z[23]=z[21] + z[22];
    z[18]=z[18] - z[23];
    z[18]=z[6]*z[18];
    z[24]=3*z[9];
    z[25]=2*z[8];
    z[26]=z[24] + z[25];
    z[27]=z[9] + z[8];
    z[28]= - z[9]*z[27];
    z[29]=npow(z[9],3);
    z[30]=z[5]*z[29];
    z[28]=z[28] + z[30];
    z[28]=z[3]*z[28];
    z[30]=npow(z[9],2);
    z[31]=z[30]*z[5];
    z[28]=z[28] + 4*z[31] - z[26];
    z[28]=z[3]*z[28];
    z[32]=z[5]*z[9];
    z[28]=z[28] + 6*z[32] - static_cast<T>(3)+ z[20];
    z[28]=z[3]*z[28];
    z[33]=z[7]*z[4];
    z[34]=z[33]*z[10];
    z[35]=2*z[4];
    z[36]=z[34] + z[35];
    z[37]=z[4]*z[10];
    z[38]=static_cast<T>(4)- z[37];
    z[38]=z[5]*z[38];
    z[18]=z[28] + z[18] + z[38] + z[36];
    z[18]=z[2]*z[18];
    z[28]=5*z[22];
    z[38]=z[10] + 2*z[15];
    z[38]=z[6]*z[38];
    z[38]=z[38] - static_cast<T>(4)- z[28];
    z[38]=z[6]*z[38];
    z[39]=7*z[4];
    z[38]=z[38] + z[39] + 8*z[7];
    z[38]=z[6]*z[38];
    z[40]=z[8] - z[31];
    z[40]=z[3]*z[40];
    z[40]=z[40] - static_cast<T>(4)+ z[32];
    z[40]=z[3]*z[40];
    z[41]=9*z[5];
    z[40]=z[40] + 8*z[4] + z[41];
    z[40]=z[3]*z[40];
    z[42]=z[5]*z[4];
    z[18]=z[18] + z[40] + z[38] - 11*z[42] - 8*z[33];
    z[38]=2*z[2];
    z[18]=z[18]*z[38];
    z[40]=18*z[4];
    z[43]=npow(z[7],2);
    z[44]=z[10]*z[43];
    z[44]= - z[40] + z[44];
    z[44]=z[7]*z[44];
    z[45]=3*z[22];
    z[46]= - z[45] - z[21];
    z[46]=z[6]*z[46];
    z[47]=7*z[7];
    z[46]=z[46] - z[4] + z[47];
    z[46]=z[6]*z[46];
    z[44]=z[44] + z[46];
    z[44]=z[6]*z[44];
    z[46]=npow(z[5],2);
    z[48]=z[46]*z[9];
    z[40]= - z[40] - z[48];
    z[40]=z[5]*z[40];
    z[21]=z[32] + z[21];
    z[21]=z[3]*z[21];
    z[21]= - z[4] + z[21];
    z[21]=z[3]*z[21];
    z[21]=z[40] + z[21];
    z[21]=z[3]*z[21];
    z[40]=z[37] - 2;
    z[49]=npow(z[5],3);
    z[50]=z[40]*z[49];
    z[51]=npow(z[7],3);
    z[52]= - z[51]*z[37];
    z[18]=z[18] + z[21] + z[44] + z[50] + z[52];
    z[18]=z[2]*z[18];
    z[17]=z[6]*z[17];
    z[17]= - 2*z[23] + z[17];
    z[17]=z[6]*z[17];
    z[21]=z[7] + z[4];
    z[44]=3*z[21];
    z[17]=z[44] + z[17];
    z[17]=z[6]*z[17];
    z[50]=z[31] - z[27];
    z[50]=z[3]*z[50];
    z[50]=z[50] + 4*z[32] - static_cast<T>(3)+ 2*z[20];
    z[50]=z[3]*z[50];
    z[52]=z[4] + 2*z[5];
    z[50]=3*z[52] + z[50];
    z[50]=z[3]*z[50];
    z[53]=3*z[33];
    z[54]=4*z[42];
    z[17]=z[50] + z[17] - z[54] - z[53];
    z[17]=z[2]*z[17];
    z[50]=5*z[4];
    z[55]= - static_cast<T>(1)- 2*z[22];
    z[55]=z[6]*z[55];
    z[47]=z[55] + z[50] + z[47];
    z[47]=z[6]*z[47];
    z[47]= - 15*z[33] + z[47];
    z[47]=z[6]*z[47];
    z[55]=5*z[5];
    z[56]= - z[3] + z[39] + z[55];
    z[56]=z[3]*z[56];
    z[56]= - 15*z[42] + z[56];
    z[56]=z[3]*z[56];
    z[17]=z[17] + z[47] + z[56];
    z[17]=z[17]*z[38];
    z[47]=z[35]*z[49];
    z[56]=3*z[7];
    z[57]=z[4] + z[56];
    z[57]=z[6]*z[57];
    z[57]= - 10*z[33] + z[57];
    z[57]=z[6]*z[57];
    z[57]= - z[51] + z[57];
    z[57]=z[6]*z[57];
    z[58]=z[5] + z[4];
    z[59]=z[58]*z[3];
    z[60]= - 10*z[42] + z[59];
    z[60]=z[3]*z[60];
    z[60]= - z[49] + z[60];
    z[60]=z[3]*z[60];
    z[61]=z[51]*z[4];
    z[17]=z[17] + z[60] + z[57] + z[47] + z[61];
    z[17]=z[2]*z[17];
    z[23]= - z[6]*z[23];
    z[23]=z[44] + z[23];
    z[23]=z[6]*z[23];
    z[23]= - 6*z[33] + z[23];
    z[23]=z[6]*z[23];
    z[44]=z[32] - 1;
    z[57]=z[20] + z[44];
    z[57]=z[3]*z[57];
    z[60]=3*z[4];
    z[57]=z[57] + z[60] + 4*z[5];
    z[57]=z[3]*z[57];
    z[57]= - 6*z[42] + z[57];
    z[57]=z[3]*z[57];
    z[23]=z[23] + z[57];
    z[23]=z[2]*z[23];
    z[57]=z[4] + 2*z[7];
    z[57]=z[6]*z[57];
    z[57]= - 9*z[33] + z[57];
    z[62]=npow(z[6],2);
    z[57]=z[57]*z[62];
    z[63]=z[35] + z[5];
    z[63]=z[3]*z[63];
    z[63]= - 9*z[42] + z[63];
    z[64]=npow(z[3],2);
    z[63]=z[63]*z[64];
    z[23]=z[23] + z[57] + z[63];
    z[23]=z[23]*z[38];
    z[53]= - z[62]*z[53];
    z[53]=z[61] + z[53];
    z[53]=z[6]*z[53];
    z[57]=z[64]*z[42];
    z[61]=z[49]*z[4];
    z[57]=z[61] - 3*z[57];
    z[57]=z[3]*z[57];
    z[23]=z[23] + z[53] + z[57];
    z[23]=z[2]*z[23];
    z[53]= - z[64] - z[62];
    z[57]=z[41]*z[7];
    z[53]=z[57]*z[53];
    z[63]=z[43]*z[5];
    z[63]=z[63] + z[49];
    z[63]=z[63]*z[7];
    z[53]=z[63] + z[53];
    z[53]=z[3]*z[6]*z[53];
    z[21]=z[6]*z[21];
    z[21]= - 4*z[33] + z[21];
    z[21]=z[21]*z[62];
    z[54]= - z[54] + z[59];
    z[54]=z[54]*z[64];
    z[21]=z[21] + z[54];
    z[21]=z[2]*z[21];
    z[54]=npow(z[6],3);
    z[59]=z[54]*z[33];
    z[65]=z[42]*npow(z[3],3);
    z[59]=z[65] + z[59];
    z[21]= - 2*z[59] + z[21];
    z[21]=z[21]*npow(z[2],2);
    z[59]= - z[1]*z[59]*npow(z[2],3);
    z[21]=z[21] + z[59];
    z[21]=z[1]*z[21];
    z[21]=2*z[21] + z[53] + z[23];
    z[21]=z[1]*z[21];
    z[23]=z[7] + z[5];
    z[53]=z[23]*z[62];
    z[51]=9*z[53] - z[49] - z[51];
    z[51]=z[6]*z[51];
    z[53]=9*z[6];
    z[59]=z[23]*z[53];
    z[57]=z[59] - z[42] - z[57];
    z[57]=z[57]*z[64];
    z[51]=z[57] + z[51] + z[61] + z[63];
    z[51]=z[3]*z[51];
    z[52]=z[52]*z[43];
    z[57]=18*z[5];
    z[59]= - z[4] - z[57];
    z[59]=z[59]*z[62];
    z[52]=z[59] + 2*z[49] + z[52];
    z[52]=z[6]*z[7]*z[52];
    z[17]=z[21] + z[17] + z[52] + z[51];
    z[17]=z[1]*z[17];
    z[21]=z[7]*z[9];
    z[51]=z[21] + z[32];
    z[52]= - z[51]*z[53];
    z[59]= - z[4]*z[32];
    z[61]=9*z[32];
    z[63]=z[7]*z[61];
    z[52]=z[52] + z[63] + z[35] + z[59];
    z[52]=z[3]*z[52];
    z[59]=z[7]*z[5];
    z[63]= - z[6]*z[23];
    z[59]=z[59] + z[63];
    z[52]=9*z[59] + z[52];
    z[52]=z[3]*z[52];
    z[59]=z[44]*z[46];
    z[51]=z[51]*z[6];
    z[63]= - z[51] - z[23];
    z[63]=z[63]*z[53];
    z[64]= - static_cast<T>(1)+ z[21];
    z[64]=z[7]*z[64];
    z[64]= - 16*z[5] + z[64];
    z[64]=z[7]*z[64];
    z[63]=z[63] + z[59] + z[64];
    z[63]=z[6]*z[63];
    z[64]=z[4]*z[9];
    z[65]=z[49]*z[64];
    z[66]= - z[7]*z[32];
    z[66]=z[5] + z[66];
    z[66]=z[7]*z[66];
    z[59]= - z[59] + z[66];
    z[59]=z[7]*z[59];
    z[52]=z[52] + z[63] + z[65] + z[59];
    z[52]=z[3]*z[52];
    z[59]=z[5]*z[10];
    z[59]=z[59] - 2;
    z[63]=z[64] + z[59];
    z[63]=z[7]*z[63];
    z[63]= - z[5] + z[63];
    z[63]=z[7]*z[63];
    z[65]=z[10]*z[49];
    z[63]=z[65] + z[63];
    z[63]=z[7]*z[63];
    z[65]=z[41]*z[10];
    z[66]= - z[65] + static_cast<T>(10)- z[64];
    z[66]=z[7]*z[66];
    z[66]=z[66] + z[4] + z[41];
    z[66]=z[66]*z[62];
    z[63]=z[66] - z[49] + z[63];
    z[63]=z[6]*z[63];
    z[58]=z[58]*z[43];
    z[49]=z[49] + z[58];
    z[49]=z[7]*z[49];
    z[17]=z[17] + z[18] + z[52] + z[63] + z[47] + z[49];
    z[17]=z[1]*z[17];
    z[18]= - z[32]*z[53];
    z[47]=z[9] - z[8];
    z[49]= - z[47]*z[56];
    z[49]= - static_cast<T>(4)+ z[49];
    z[52]=3*z[3];
    z[49]=z[49]*z[52];
    z[44]= - z[4]*z[44];
    z[18]=z[49] + z[18] + 5*z[44] - 18*z[7];
    z[18]=z[3]*z[18];
    z[44]=npow(z[4],2);
    z[42]= - 2*z[44] + z[42];
    z[49]= - static_cast<T>(20)+ z[32];
    z[49]=z[5]*z[49];
    z[58]=z[7]*z[47];
    z[58]= - static_cast<T>(1)+ z[58];
    z[58]=z[7]*z[58];
    z[49]=z[49] + z[58];
    z[49]=z[7]*z[49];
    z[21]= - z[21]*z[53];
    z[21]= - 20*z[23] + z[21];
    z[21]=z[6]*z[21];
    z[18]=z[18] + z[21] + 2*z[42] + z[49];
    z[18]=z[3]*z[18];
    z[21]=z[16]*z[14]*z[54];
    z[23]= - z[9]*z[26];
    z[26]= - z[3]*z[27]*z[30];
    z[23]=z[23] + z[26];
    z[23]=z[3]*z[23];
    z[23]=z[23] - z[8] - z[24];
    z[23]=z[3]*z[23];
    z[21]=z[23] - static_cast<T>(1)+ z[21];
    z[21]=z[2]*z[21];
    z[23]=z[25] + z[9];
    z[23]=z[3]*z[23];
    z[23]=z[23] - 1;
    z[23]=z[9]*z[23];
    z[23]=3*z[31] + z[25] + z[23];
    z[23]=z[3]*z[23];
    z[23]=z[23] - static_cast<T>(5)+ z[61];
    z[23]=z[3]*z[23];
    z[24]=static_cast<T>(3)- z[37];
    z[24]=z[5]*z[24];
    z[24]=z[24] + z[36];
    z[15]=z[10] + z[15];
    z[26]= - z[6]*z[14];
    z[15]=3*z[15] + z[26];
    z[15]=z[6]*z[15];
    z[26]= - static_cast<T>(1)- z[22];
    z[15]=3*z[26] + z[15];
    z[15]=z[6]*z[15];
    z[15]=z[21] + z[23] + 3*z[24] + z[15];
    z[15]=z[15]*z[38];
    z[21]=static_cast<T>(1)- z[45];
    z[16]=z[6]*z[16];
    z[16]=2*z[21] + z[16];
    z[16]=z[6]*z[16];
    z[21]=4*z[20];
    z[23]= - static_cast<T>(5)+ z[21];
    z[23]=z[4]*z[23];
    z[22]=static_cast<T>(12)- z[22];
    z[22]=z[7]*z[22];
    z[16]=z[16] + z[22] - 3*z[11] + z[23];
    z[16]=z[6]*z[16];
    z[22]=static_cast<T>(3)- 2*z[32];
    z[23]= - 3*z[8] + z[9];
    z[23]=z[3]*z[23];
    z[22]=3*z[22] + z[23];
    z[22]=z[3]*z[22];
    z[21]= - static_cast<T>(1)- z[21];
    z[21]=z[4]*z[21];
    z[21]=z[22] + z[48] - 3*z[12] + z[21];
    z[21]=z[3]*z[21];
    z[22]= - 16*z[4] + z[5];
    z[22]=z[5]*z[22];
    z[15]=z[15] + z[21] + z[16] + z[22] - 11*z[33];
    z[15]=z[2]*z[15];
    z[16]=2*z[9];
    z[21]=z[16] + z[10];
    z[22]= - z[21]*z[41];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[6]*z[22];
    z[23]=z[9]*z[33];
    z[22]=z[22] - 5*z[23] + z[50] - z[57];
    z[22]=z[6]*z[22];
    z[23]=z[5]*z[21];
    z[23]= - static_cast<T>(2)+ z[23];
    z[23]=z[23]*z[46];
    z[24]= - z[7]*z[59];
    z[24]=z[24] + z[35] - 37*z[5];
    z[24]=z[7]*z[24];
    z[22]=z[22] + z[23] + z[24];
    z[22]=z[6]*z[22];
    z[21]=z[4]*z[21]*z[46];
    z[23]=z[12]*z[60];
    z[21]=z[23] + z[21];
    z[21]=z[5]*z[21];
    z[23]=z[11]*z[60];
    z[24]= - z[10] + z[9];
    z[24]=z[4]*z[24];
    z[24]=static_cast<T>(1)+ z[24];
    z[24]=z[7]*z[24];
    z[24]= - z[5] + z[24];
    z[24]=z[7]*z[24];
    z[23]=z[23] + z[24];
    z[23]=z[7]*z[23];
    z[15]=z[17] + z[15] + z[18] + z[22] + z[21] + z[23];
    z[15]=z[1]*z[15];
    z[17]=z[6]*z[10];
    z[17]=z[17] + 1;
    z[18]=z[10] - z[8];
    z[17]=z[6]*z[18]*z[17];
    z[20]=3*z[20];
    z[21]=2*z[10];
    z[22]=z[21]*z[11];
    z[17]=z[17] - z[28] - z[20] + static_cast<T>(5)- z[22];
    z[17]=z[6]*z[17];
    z[23]=4*z[8];
    z[24]=z[3]*z[8];
    z[26]= - 7*z[24] + 11;
    z[26]=z[9]*z[26];
    z[26]= - z[23] + z[26];
    z[26]=z[3]*z[26];
    z[28]=z[16]*z[12];
    z[20]=z[26] + 5*z[32] + z[20] + static_cast<T>(9)+ z[28];
    z[20]=z[3]*z[20];
    z[19]=z[19]*z[6];
    z[26]=z[14] + z[19];
    z[18]=z[62]*z[18]*z[26];
    z[26]= - z[25] + z[9];
    z[26]=z[26]*z[30];
    z[29]= - z[29]*z[24];
    z[26]=z[26] + z[29];
    z[26]=z[3]*z[26];
    z[16]= - z[8] + z[16];
    z[16]=z[9]*z[16];
    z[16]=z[16] + z[26];
    z[16]=z[3]*z[16];
    z[16]=z[16] + z[9] + z[18];
    z[16]=z[2]*z[16];
    z[18]= - 2*z[14] - z[19];
    z[18]=z[18]*z[62];
    z[19]=z[30]*z[24];
    z[24]=z[9]*z[47];
    z[19]= - 4*z[24] + 3*z[19];
    z[19]=z[3]*z[19];
    z[19]=z[19] + z[8] - 6*z[9];
    z[19]=z[3]*z[19];
    z[16]=z[16] + z[19] - static_cast<T>(2)+ z[18];
    z[16]=z[16]*z[38];
    z[18]= - z[40]*z[55];
    z[19]=z[12] - z[60];
    z[16]=z[16] + z[20] + z[17] + 5*z[34] + 2*z[19] + z[18];
    z[16]=z[2]*z[16];
    z[17]=z[9]*z[10];
    z[14]=z[17] + z[14];
    z[17]=z[5]*z[14];
    z[17]= - z[10] + z[17];
    z[17]=z[5]*z[17];
    z[17]=static_cast<T>(2)+ z[17];
    z[17]=z[5]*z[17];
    z[18]= - z[10]*z[57];
    z[18]=z[18] - static_cast<T>(1)- 3*z[64];
    z[18]=z[7]*z[18];
    z[14]= - z[14]*z[41];
    z[14]=8*z[10] + z[14];
    z[14]=z[6]*z[14];
    z[14]=z[14] + static_cast<T>(5)- z[65];
    z[14]=z[6]*z[14];
    z[14]=z[14] + z[18] + 11*z[4] + z[17];
    z[14]=z[6]*z[14];
    z[17]=z[51] - z[12] + z[39];
    z[18]=z[9]*z[8];
    z[19]=npow(z[8],2);
    z[18]=z[18] + z[19];
    z[19]=z[7]*z[18];
    z[19]= - z[8] + z[19];
    z[19]=z[7]*z[19];
    z[20]= - static_cast<T>(8)+ z[61];
    z[19]=2*z[20] + z[19];
    z[19]=z[7]*z[19];
    z[18]= - z[18]*z[56];
    z[18]=z[23] + z[18];
    z[18]=z[18]*z[52];
    z[20]=z[7]*z[27];
    z[18]=z[18] - static_cast<T>(8)+ 9*z[20];
    z[18]=z[3]*z[18];
    z[20]= - static_cast<T>(8)- z[64];
    z[20]=z[5]*z[20];
    z[17]=z[18] + z[19] + 3*z[20] + 2*z[17];
    z[17]=z[3]*z[17];
    z[18]=2*z[12];
    z[18]=z[37]*z[18];
    z[19]=z[9]*z[37];
    z[19]=z[21] + z[19];
    z[19]=z[5]*z[19];
    z[19]= - static_cast<T>(2)+ z[19];
    z[19]=z[5]*z[19];
    z[18]=z[18] + z[19];
    z[18]=z[5]*z[18];
    z[19]= - z[37] - 1;
    z[19]=z[9]*z[19];
    z[19]= - z[25] + z[19];
    z[19]=z[19]*z[43];
    z[20]=z[22] - 1;
    z[22]=z[4]*z[20];
    z[19]=z[19] + z[22] - z[57];
    z[19]=z[7]*z[19];
    z[14]=z[15] + z[16] + z[17] + z[14] + z[19] - 8*z[44] + z[18];
    z[14]=z[1]*z[14];
    z[15]=z[28] + 1;
    z[16]= - z[10]*z[12];
    z[16]=z[16] - z[15];
    z[17]= - z[10]*z[11];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[21];
    z[17]= - z[8] + z[17];
    z[17]=z[6]*z[17];
    z[18]= - z[9]*z[15];
    z[18]=z[8] + z[18];
    z[18]=z[3]*z[18];
    z[16]=z[18] + 2*z[16] + z[17];
    z[16]=z[2]*z[16];
    z[17]= - z[12]*z[13];
    z[15]=z[17] + z[15];
    z[15]=z[3]*z[15];
    z[15]=z[15] + z[12] + z[4];
    z[17]=z[13] - z[10];
    z[17]=z[12]*z[17];
    z[18]=z[12]*z[21];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[10]*z[18];
    z[18]=z[18] - z[9];
    z[18]=z[4]*z[18];
    z[17]=2*z[17] + z[18];
    z[17]=z[5]*z[17];
    z[18]=z[10]*z[20];
    z[18]=z[18] - z[9];
    z[18]=z[4]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[7]*z[18];
    z[14]=z[14] + z[16] + z[18] + z[17] + 2*z[15];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r980(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r980(const std::array<dd_real,31>&);
#endif
