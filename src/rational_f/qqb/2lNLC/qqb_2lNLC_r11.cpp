#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r11(const std::array<T,31>& k) {
  T z[105];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[10];
    z[7]=k[14];
    z[8]=k[23];
    z[9]=k[8];
    z[10]=k[9];
    z[11]=k[15];
    z[12]=k[18];
    z[13]=k[2];
    z[14]=k[3];
    z[15]=k[30];
    z[16]=k[4];
    z[17]=k[5];
    z[18]=k[24];
    z[19]=k[29];
    z[20]=z[16]*z[12];
    z[21]=z[2]*z[16];
    z[22]=z[20] - z[21];
    z[22]=z[22]*z[9];
    z[23]=n<T>(58,9)*z[22];
    z[24]=n<T>(1,2)*z[2];
    z[25]=static_cast<T>(799)- 367*z[21];
    z[25]=z[25]*z[24];
    z[26]=npow(z[12],2);
    z[27]=139*z[26];
    z[28]=z[27]*z[16];
    z[25]=z[25] - 512*z[12] + z[28];
    z[29]=12*z[10];
    z[30]=z[29]*z[13];
    z[30]=z[30] - n<T>(25,2);
    z[30]=z[30]*z[10];
    z[25]= - z[23] + n<T>(1,9)*z[25] - z[30];
    z[25]=z[9]*z[25];
    z[31]=2*z[8];
    z[32]=n<T>(1,3)*z[6] - z[8];
    z[32]=z[32]*z[31];
    z[33]=n<T>(1,3)*z[2];
    z[34]=n<T>(367,3)*z[2];
    z[35]=z[6] - z[34];
    z[35]=z[35]*z[33];
    z[36]=n<T>(11,2)*z[2];
    z[37]= - z[36] + z[29];
    z[37]=z[10]*z[37];
    z[38]=n<T>(139,9)*z[26];
    z[39]=z[11]*z[13];
    z[40]= - 18*z[39] - n<T>(45,2);
    z[40]=z[9]*z[40];
    z[40]= - n<T>(28,9)*z[12] + z[40];
    z[40]=z[11]*z[40];
    z[41]=11*z[8];
    z[42]=n<T>(46,9)*z[11] - n<T>(146,9)*z[2] - n<T>(19,3)*z[6] + z[41];
    z[42]=z[3]*z[42];
    z[25]=z[42] + z[40] + z[25] + z[37] + z[35] + z[38] + z[32];
    z[25]=z[7]*z[25];
    z[32]=z[11]*z[12];
    z[35]=z[32]*z[16];
    z[37]=2*z[9];
    z[40]=545*z[12] + z[28];
    z[40]=n<T>(58,9)*z[35] + n<T>(1,9)*z[40] - z[37];
    z[40]=z[11]*z[40];
    z[42]=n<T>(367,18)*z[3];
    z[43]=z[11]*z[16];
    z[44]=z[42]*z[43];
    z[45]=n<T>(1,9)*z[11];
    z[46]= - static_cast<T>(419)- 58*z[43];
    z[46]=z[46]*z[45];
    z[47]=z[8]*z[14];
    z[48]=31*z[47];
    z[49]= - static_cast<T>(110)- z[48];
    z[49]=z[8]*z[49];
    z[49]=z[49] - 119*z[2];
    z[46]= - z[44] + z[46] + n<T>(1,9)*z[49] - n<T>(13,2)*z[10];
    z[46]=z[3]*z[46];
    z[49]=n<T>(1,3)*z[3];
    z[50]=z[3]*z[16];
    z[51]=static_cast<T>(74)- n<T>(367,2)*z[50];
    z[51]=z[51]*z[49];
    z[52]=n<T>(49,2)*z[6];
    z[53]=n<T>(128,3)*z[18];
    z[51]= - n<T>(49,6)*z[7] + z[51] + n<T>(41,2)*z[2] - 11*z[12] + z[53] + 
    z[52];
    z[54]=n<T>(1,3)*z[5];
    z[51]=z[51]*z[54];
    z[55]=10*z[2];
    z[56]= - 13*z[12] + z[55];
    z[56]=z[56]*z[37];
    z[57]=n<T>(139,3)*z[26];
    z[56]=z[57] + z[56];
    z[58]=z[2]*z[14];
    z[59]= - n<T>(25,2)*z[58] + 8*z[47];
    z[60]=z[10]*z[14];
    z[59]=n<T>(1,2)*z[60] + n<T>(1,9)*z[59];
    z[61]= - z[3]*z[59];
    z[61]=n<T>(25,18)*z[2] + z[61];
    z[61]=z[4]*z[61];
    z[62]=n<T>(11,3)*z[6];
    z[63]=n<T>(13,9)*z[8] + z[62] - 5*z[12];
    z[63]=2*z[63] + n<T>(11,2)*z[10];
    z[63]=z[7]*z[63];
    z[40]=z[61] + z[51] + z[63] + z[46] + n<T>(1,3)*z[56] + z[40];
    z[40]=z[4]*z[40];
    z[46]=npow(z[8],2);
    z[51]=n<T>(31,9)*z[46];
    z[56]=npow(z[10],2);
    z[61]= - z[51] + 6*z[56];
    z[63]=z[10] - z[8];
    z[64]=z[63]*z[4];
    z[65]= - z[61] + n<T>(23,9)*z[64];
    z[66]= - z[4]*z[7]*z[65];
    z[67]=z[2] - z[12];
    z[67]=z[67]*z[9];
    z[68]= - z[27] + 220*z[67];
    z[69]=npow(z[9],2);
    z[68]=z[68]*z[69];
    z[66]=z[66] + n<T>(1,9)*z[68];
    z[66]=z[66]*z[4];
    z[70]= - z[27] + 220*z[32];
    z[71]=npow(z[11],2);
    z[70]=z[70]*z[71];
    z[72]=z[3]*npow(z[11],3);
    z[72]= - z[70] + 220*z[72];
    z[73]=n<T>(1,9)*z[7];
    z[72]=z[72]*z[73];
    z[73]=13*z[69];
    z[74]=z[73] + z[51];
    z[75]=z[9] + z[8];
    z[76]=z[75]*z[5];
    z[77]=z[74] + n<T>(148,9)*z[76];
    z[78]=npow(z[5],2);
    z[79]=z[78]*z[3]*z[77];
    z[66]=z[66] - z[72] + z[79];
    z[66]=z[66]*z[17];
    z[64]=n<T>(2,9)*z[64] - z[51];
    z[64]=z[7]*z[64];
    z[72]=z[9]*z[2];
    z[72]=z[38] + 9*z[72];
    z[72]=z[9]*z[72];
    z[79]= - n<T>(367,6)*z[7] - z[53] + n<T>(37,2)*z[2];
    z[78]=z[79]*z[78];
    z[79]=z[11]*z[69];
    z[64]=n<T>(1,3)*z[78] + z[72] - 18*z[79] + z[64];
    z[64]=z[4]*z[64];
    z[72]=128*z[18];
    z[78]=128*z[2] + z[72] - 229*z[75];
    z[78]=z[5]*z[78];
    z[79]=31*z[46];
    z[80]=npow(z[2],2);
    z[81]=n<T>(367,2)*z[80];
    z[78]=z[78] - z[79] + z[81];
    z[73]= - z[73] + n<T>(1,9)*z[78];
    z[73]=z[5]*z[73];
    z[78]=10*z[10];
    z[82]= - z[69]*z[78];
    z[73]=z[73] + z[82];
    z[73]=z[3]*z[73];
    z[80]=n<T>(245,9)*z[71] + 6*z[46] + n<T>(367,18)*z[80];
    z[80]=z[3]*z[80];
    z[82]=z[27] - 245*z[32];
    z[82]=z[82]*z[45];
    z[80]=z[82] + z[80];
    z[80]=z[7]*z[80];
    z[64]= - z[66] + z[64] + z[80] + z[73];
    z[64]=z[17]*z[64];
    z[73]=n<T>(1,9)*z[8];
    z[80]=z[8]*z[16];
    z[82]=31*z[80];
    z[83]=static_cast<T>(452)- z[82];
    z[83]=z[83]*z[73];
    z[84]=z[9]*z[16];
    z[85]=n<T>(1,2)*z[84];
    z[86]=179*z[80];
    z[87]= - n<T>(367,2) - z[86];
    z[87]=n<T>(1,9)*z[87] + z[85];
    z[87]=z[5]*z[87];
    z[88]=2*z[6];
    z[83]=z[87] - n<T>(11,2)*z[9] + z[83] - z[88] + n<T>(11,3)*z[12];
    z[83]=z[7]*z[83];
    z[87]=6*z[10];
    z[89]=z[6]*z[14];
    z[90]=z[89]*z[87];
    z[91]=2*z[89];
    z[92]=static_cast<T>(9)- z[91];
    z[92]=z[6]*z[92];
    z[90]=z[90] + z[92] + z[36];
    z[90]=z[10]*z[90];
    z[92]=n<T>(64,3)*z[18] - 13*z[8];
    z[92]=2*z[92] + n<T>(143,3)*z[2];
    z[92]= - z[42] + n<T>(1,3)*z[92] - z[37];
    z[92]=z[3]*z[92];
    z[93]=npow(z[6],2);
    z[94]=2*z[15];
    z[95]=z[93]*z[94];
    z[96]=z[14]*z[95];
    z[97]= - 3*z[2] + z[37];
    z[97]=z[11]*z[97];
    z[83]=z[92] + z[97] + z[96] + z[90] + z[83];
    z[83]=z[5]*z[83];
    z[90]=n<T>(58,9)*z[11];
    z[92]=n<T>(1,2)*z[10];
    z[96]=z[92] + z[90];
    z[96]=z[11]*z[96];
    z[97]=z[2] + z[11];
    z[97]=z[97]*z[42];
    z[98]=z[7]*z[92];
    z[96]=z[98] + z[96] + z[97];
    z[96]=z[3]*z[96];
    z[97]= - z[27] - 58*z[32];
    z[97]=z[97]*z[45];
    z[98]=n<T>(1,2)*z[3];
    z[52]= - z[52] + 10*z[12];
    z[52]= - n<T>(367,18)*z[5] + n<T>(1,3)*z[52] - z[98];
    z[52]=z[7]*z[52];
    z[99]=52*z[2] - n<T>(367,2)*z[3];
    z[99]=z[3]*z[99];
    z[52]=n<T>(1,9)*z[99] + z[52];
    z[52]=z[5]*z[52];
    z[99]= - z[3]*z[2];
    z[100]= - z[2] + z[3];
    z[100]=z[5]*z[100];
    z[99]=z[99] + z[100];
    z[99]=z[4]*z[99];
    z[52]=n<T>(25,18)*z[99] + z[52] + z[97] + z[96];
    z[52]=z[4]*z[52];
    z[81]=z[81] - z[27];
    z[81]= - n<T>(58,9)*z[67] + n<T>(1,9)*z[81];
    z[24]= - z[24] - z[29];
    z[24]=z[10]*z[24];
    z[24]=z[24] + z[81];
    z[24]=z[9]*z[24];
    z[62]=z[62] + 4*z[8];
    z[62]=z[8]*z[62];
    z[34]= - 17*z[6] + z[34];
    z[34]=z[2]*z[34];
    z[96]=z[9]*z[92];
    z[34]=z[96] + z[62] + n<T>(1,6)*z[34];
    z[34]=z[3]*z[34];
    z[62]=z[46]*z[6];
    z[24]=z[34] - z[62] + z[24];
    z[24]=z[7]*z[24];
    z[34]=z[10]*z[2];
    z[96]=z[11]*z[34];
    z[97]=npow(z[3],2);
    z[99]=z[2]*z[97];
    z[96]=z[96] - n<T>(367,9)*z[99];
    z[99]= - z[11]*z[37];
    z[98]= - z[9]*z[98];
    z[98]=z[99] + z[98];
    z[98]=z[7]*z[98];
    z[96]=n<T>(1,2)*z[96] + z[98];
    z[96]=z[5]*z[96];
    z[98]=z[4]*z[3];
    z[97]= - 367*z[97] + 25*z[98];
    z[97]=z[4]*z[5]*z[2]*z[97];
    z[98]=z[62]*z[3];
    z[99]=z[7]*z[98];
    z[97]=z[99] + n<T>(1,18)*z[97];
    z[97]=z[1]*z[97];
    z[24]=z[97] + z[52] + z[96] - z[98] + z[24];
    z[24]=z[1]*z[24];
    z[42]=z[42] + z[10] + z[90];
    z[42]=z[11]*z[42];
    z[52]=z[92] + n<T>(5,2)*z[2] - z[18] - z[94];
    z[52]=z[6]*z[52];
    z[90]= - static_cast<T>(3)- z[89];
    z[90]=z[8]*z[90];
    z[90]= - n<T>(10,3)*z[6] + z[90];
    z[90]=z[8]*z[90];
    z[92]= - z[9]*z[10];
    z[42]=z[92] + z[90] + z[52] + z[42];
    z[42]=z[3]*z[42];
    z[52]=3*z[10];
    z[90]= - z[6]*z[52];
    z[90]=z[93] + z[90];
    z[92]=2*z[10];
    z[90]=z[90]*z[92];
    z[93]=z[2] + z[29];
    z[93]=z[10]*z[93];
    z[81]=z[93] - z[81];
    z[81]=z[9]*z[81];
    z[93]=z[34] + z[38];
    z[96]= - n<T>(58,9)*z[32] - z[93];
    z[96]=z[11]*z[96];
    z[24]=z[24] + z[64] + z[40] + z[83] + z[25] + z[42] + z[96] + z[81]
    + z[90] - z[95] + z[62];
    z[24]=z[1]*z[24];
    z[25]=73*z[47] - n<T>(29,2)*z[58];
    z[25]=n<T>(187,18)*z[43] + n<T>(1,9)*z[25] - n<T>(13,2)*z[60];
    z[25]=z[3]*z[25];
    z[40]=5*z[19];
    z[42]= - z[40] + z[94];
    z[62]=z[6]*z[18];
    z[62]=z[62] - z[38];
    z[62]=z[16]*z[62];
    z[64]=8*z[12];
    z[81]= - n<T>(55,2)*z[6] + z[64];
    z[81]=z[16]*z[81];
    z[81]=n<T>(187,6)*z[50] + n<T>(107,3) + z[81];
    z[81]=z[81]*z[54];
    z[59]=z[4]*z[59];
    z[83]=n<T>(9,2)*z[9];
    z[90]=static_cast<T>(17)+ n<T>(31,9)*z[47];
    z[90]=z[8]*z[90];
    z[95]= - n<T>(116,9)*z[12] + z[40] - n<T>(11,2)*z[6];
    z[95]=z[16]*z[95];
    z[95]= - static_cast<T>(18)+ z[95];
    z[95]=z[11]*z[95];
    z[96]=z[16]*z[6];
    z[97]=static_cast<T>(23)- n<T>(8,3)*z[96];
    z[97]=z[7]*z[97];
    z[25]=z[59] + z[81] + z[97] + z[25] + z[95] - z[83] - n<T>(148,9)*z[10]
    + n<T>(125,18)*z[2] + z[90] + z[62] + 3*z[42] - 17*z[12];
    z[25]=z[4]*z[25];
    z[42]=npow(z[14],2);
    z[59]=z[42]*z[6];
    z[62]= - n<T>(49,9)*z[14] + z[59];
    z[62]=z[62]*z[88];
    z[81]=z[59] - z[14];
    z[90]= - z[81]*z[87];
    z[62]=z[90] + n<T>(95,9) + z[62];
    z[62]=z[62]*z[92];
    z[90]=4*z[15];
    z[95]= - z[59]*z[90];
    z[97]=z[14]*z[15];
    z[95]=z[95] - n<T>(307,6) + 10*z[97];
    z[95]=z[6]*z[95];
    z[98]= - static_cast<T>(443)+ z[82];
    z[98]=z[98]*z[73];
    z[99]=n<T>(16,3)*z[6] - z[12];
    z[99]=z[16]*z[99];
    z[99]=n<T>(92,3)*z[80] + n<T>(391,3) + 8*z[99];
    z[99]=n<T>(1,3)*z[99] + 3*z[84];
    z[99]=z[7]*z[99];
    z[100]=z[12]*z[14];
    z[101]=z[100] - z[89];
    z[86]=z[86] - n<T>(367,2) - 106*z[101];
    z[85]=n<T>(1,9)*z[86] - z[85];
    z[85]=z[5]*z[85];
    z[86]=6*z[11];
    z[102]= - static_cast<T>(199)- 367*z[50];
    z[102]=z[3]*z[102];
    z[62]=z[85] + z[99] + n<T>(1,18)*z[102] + z[86] - n<T>(337,9)*z[9] + z[62]
    - n<T>(175,3)*z[2] + z[98] + n<T>(332,9)*z[12] - z[94] + z[95];
    z[62]=z[5]*z[62];
    z[85]= - z[27] + n<T>(31,3)*z[46];
    z[95]=4*z[9];
    z[98]=122*z[12] - n<T>(685,2)*z[2];
    z[98]=n<T>(1,9)*z[98] + z[95];
    z[98]=z[9]*z[98];
    z[53]=n<T>(683,6)*z[7] + z[53] - n<T>(35,2)*z[2];
    z[53]=z[53]*z[54];
    z[31]= - z[92] + z[31] + n<T>(25,2)*z[2];
    z[31]=z[4]*z[31];
    z[54]= - 7*z[10] + 11*z[9];
    z[54]=z[11]*z[54];
    z[99]=2*z[12];
    z[102]= - 3*z[19] - z[99];
    z[102]=n<T>(56,9)*z[10] + 5*z[102] - n<T>(52,3)*z[8];
    z[102]=z[7]*z[102];
    z[31]=n<T>(1,9)*z[31] + z[53] + z[102] + n<T>(3,2)*z[54] + n<T>(1,3)*z[85] + 
    z[98];
    z[31]=z[4]*z[31];
    z[53]=5*z[10];
    z[54]= - z[69]*z[53];
    z[53]= - n<T>(220,9)*z[11] - z[15] + z[53];
    z[53]=z[53]*z[71];
    z[53]=z[54] + z[53];
    z[53]=z[3]*z[53];
    z[54]=z[38] + 5*z[34];
    z[67]= - n<T>(220,9)*z[67] + z[54];
    z[67]=z[67]*z[69];
    z[54]=n<T>(220,9)*z[32] - z[54];
    z[54]=z[54]*z[71];
    z[53]=z[66] - z[53] - z[67] - z[54];
    z[54]=z[10] + z[19];
    z[67]=5*z[7];
    z[54]=z[54]*z[67];
    z[67]=12*z[56];
    z[85]= - z[54] + z[51] - z[67];
    z[85]=z[7]*z[85];
    z[98]=2*z[56] + 3*z[69];
    z[86]=z[98]*z[86];
    z[98]=463*z[12];
    z[102]= - z[98] + 751*z[2];
    z[102]=z[9]*z[102];
    z[102]= - z[27] + z[102];
    z[103]=n<T>(1,9)*z[9];
    z[102]=z[102]*z[103];
    z[63]=z[63]*z[7];
    z[104]= - n<T>(25,3)*z[63] - z[65];
    z[104]=z[4]*z[104];
    z[85]=z[104] + z[85] + z[102] - z[86];
    z[85]=z[4]*z[85];
    z[102]=z[51] - 40*z[69];
    z[102]=z[3]*z[102];
    z[75]=z[75]*z[3];
    z[104]= - 11*z[75] + z[77];
    z[104]=z[5]*z[104];
    z[86]=z[104] + z[86] + z[102];
    z[86]=z[5]*z[86];
    z[102]= - z[27] - 305*z[32];
    z[102]=z[102]*z[45];
    z[104]=4*z[46] + n<T>(359,9)*z[71];
    z[104]=z[3]*z[104];
    z[102]=z[102] + z[104];
    z[102]=z[7]*z[102];
    z[53]=z[85] + z[86] + z[102] - 2*z[53];
    z[53]=z[17]*z[53];
    z[85]= - z[27] - n<T>(71,3)*z[46];
    z[86]=z[2]*z[40];
    z[41]=n<T>(263,18)*z[11] + z[41] - n<T>(23,9)*z[2];
    z[41]=z[3]*z[41];
    z[41]=z[41] + n<T>(41,3)*z[32] - z[67] + n<T>(1,3)*z[85] + z[86];
    z[41]=z[7]*z[41];
    z[67]=z[7] + z[10];
    z[36]= - z[83] + z[94] + z[36] + n<T>(5,2)*z[67];
    z[36]=z[11]*z[36];
    z[67]=122*z[2] + z[72] + 329*z[8];
    z[67]=n<T>(1,9)*z[67] + 21*z[9];
    z[67]=z[3]*z[67];
    z[36]=n<T>(229,9)*z[76] + z[67] + z[74] + z[36];
    z[36]=z[5]*z[36];
    z[67]= - 521*z[12] + 404*z[2];
    z[67]=z[67]*z[103];
    z[67]= - 2*z[93] + z[67];
    z[67]=z[9]*z[67];
    z[72]=z[92] - z[9];
    z[72]=z[72]*z[95];
    z[74]=2*z[11];
    z[76]= - n<T>(124,9)*z[11] - 3*z[15] + z[78];
    z[76]=z[76]*z[74];
    z[72]=z[76] + n<T>(35,9)*z[46] + z[72];
    z[72]=z[3]*z[72];
    z[38]=z[38] - z[34];
    z[76]= - n<T>(31,9)*z[12] - 11*z[2];
    z[76]=z[11]*z[76];
    z[38]=2*z[38] + z[76];
    z[38]=z[11]*z[38];
    z[31]=z[53] + z[31] + z[36] + z[41] + z[72] + z[67] + z[38];
    z[31]=z[17]*z[31];
    z[36]=n<T>(11,3) + z[97];
    z[36]=z[6]*z[36];
    z[36]= - z[90] + z[36];
    z[38]= - n<T>(40,3)*z[47] + n<T>(10,3) - z[89];
    z[41]=n<T>(1,3)*z[8];
    z[38]=z[38]*z[41];
    z[53]=z[18]*z[96];
    z[67]= - n<T>(91,9) + n<T>(5,2)*z[89];
    z[67]=z[2]*z[67];
    z[72]=static_cast<T>(6)- n<T>(1,2)*z[89];
    z[72]=z[10]*z[72];
    z[76]=z[16]*z[71];
    z[36]= - z[44] - n<T>(58,9)*z[76] - n<T>(7,2)*z[9] + z[72] + z[67] + z[38]
    + 2*z[36] + z[53];
    z[36]=z[3]*z[36];
    z[38]=n<T>(31,3)*z[80] - n<T>(337,3) - z[96];
    z[38]=z[8]*z[38];
    z[44]= - static_cast<T>(55)+ 131*z[96];
    z[33]=z[44]*z[33];
    z[44]=z[26]*z[16];
    z[53]=n<T>(139,3)*z[44];
    z[33]=z[33] + z[38] + 157*z[12] - z[53];
    z[38]=z[13]*z[92];
    z[38]=z[38] - n<T>(74,9)*z[21] - static_cast<T>(8)+ n<T>(83,9)*z[20];
    z[38]=z[9]*z[38];
    z[37]= - z[13]*z[37];
    z[37]=6*z[39] + n<T>(15,2) + z[37];
    z[67]=3*z[11];
    z[37]=z[37]*z[67];
    z[72]= - static_cast<T>(35)- 8*z[96];
    z[49]=z[72]*z[49];
    z[30]=z[49] + z[37] + z[38] + n<T>(1,3)*z[33] + z[30];
    z[30]=z[7]*z[30];
    z[33]=n<T>(1,9)*z[2];
    z[37]=static_cast<T>(103)+ n<T>(367,2)*z[21];
    z[37]=z[37]*z[33];
    z[38]=n<T>(139,9)*z[44];
    z[23]=z[23] - z[78] + z[37] + 38*z[12] - z[38];
    z[23]=z[9]*z[23];
    z[37]= - static_cast<T>(1)+ z[91];
    z[37]=z[37]*z[52];
    z[44]=6*z[2];
    z[49]= - static_cast<T>(3)- z[91];
    z[49]=z[6]*z[49];
    z[37]=z[37] + z[49] - z[44];
    z[37]=z[37]*z[92];
    z[49]=z[40] + 8*z[6];
    z[49]=z[49]*z[16];
    z[72]=z[49] + 12;
    z[76]=z[72]*z[21];
    z[76]=n<T>(58,9)*z[20] + z[76];
    z[76]=z[11]*z[76];
    z[78]=130*z[12] + z[28];
    z[83]=15*z[19];
    z[85]= - z[83] - n<T>(128,9)*z[6];
    z[85]=z[16]*z[85];
    z[85]= - n<T>(236,9) + z[85];
    z[85]=z[2]*z[85];
    z[76]=z[76] + 18*z[9] + n<T>(1,9)*z[78] + z[85];
    z[76]=z[11]*z[76];
    z[78]=z[89]*z[94];
    z[78]=z[78] + n<T>(137,9)*z[18] - z[15];
    z[78]=z[78]*z[88];
    z[85]=static_cast<T>(1)+ z[89];
    z[85]=z[8]*z[85];
    z[85]=n<T>(203,9)*z[6] + z[85];
    z[85]=z[8]*z[85];
    z[86]=z[83] - n<T>(106,9)*z[6];
    z[86]=z[2]*z[86];
    z[23]=z[24] + z[31] + z[25] + z[62] + z[30] + z[36] + z[76] + z[23]
    + z[37] + z[86] + z[85] + z[78] + z[57];
    z[23]=z[1]*z[23];
    z[24]=npow(z[14],3);
    z[25]= - z[24]*z[88];
    z[25]=n<T>(115,9)*z[42] + z[25];
    z[25]=z[6]*z[25];
    z[24]=z[24]*z[6];
    z[30]= - 2*z[42] + z[24];
    z[30]=z[30]*z[87];
    z[25]=z[30] - n<T>(89,9)*z[14] + z[25];
    z[25]=z[10]*z[25];
    z[30]=z[16]*z[101];
    z[31]=z[12]*z[42];
    z[30]=z[30] - z[59] + z[31];
    z[30]=z[5]*z[30];
    z[31]=n<T>(16,3) - 5*z[97];
    z[31]=z[14]*z[31];
    z[24]=z[15]*z[24];
    z[24]=z[31] + z[24];
    z[24]=z[24]*z[88];
    z[31]=z[40] + z[99];
    z[31]=z[16]*z[31];
    z[24]=n<T>(106,9)*z[30] - 2*z[50] - 8*z[43] + 33*z[84] + z[25] + n<T>(116,3)
   *z[80] + 3*z[31] + 11*z[100] + z[24] - n<T>(385,18) + 16*z[97];
    z[24]=z[5]*z[24];
    z[25]= - 2*z[61] - z[54];
    z[25]=z[7]*z[25];
    z[30]= - z[98] + 670*z[2];
    z[30]=z[9]*z[30];
    z[26]= - 278*z[26] + z[30];
    z[26]=z[26]*z[103];
    z[30]= - n<T>(77,9)*z[63] - z[65];
    z[30]=z[4]*z[30];
    z[25]=z[30] + z[26] + z[25];
    z[25]=z[4]*z[25];
    z[26]= - z[68] + z[70];
    z[30]=2*z[3];
    z[31]= - z[15] - n<T>(110,9)*z[11];
    z[31]=z[31]*z[71]*z[30];
    z[32]= - z[57] - 10*z[32];
    z[32]=z[11]*z[32];
    z[36]=z[46] + n<T>(38,3)*z[71];
    z[36]=z[3]*z[36];
    z[32]=n<T>(2,3)*z[32] + z[36];
    z[32]=z[7]*z[32];
    z[36]=n<T>(62,9)*z[46] - 27*z[69];
    z[36]=z[3]*z[36];
    z[37]=n<T>(130,9)*z[75] + z[77];
    z[37]=z[5]*z[37];
    z[36]=z[36] + z[37];
    z[36]=z[5]*z[36];
    z[25]= - z[66] + z[25] + z[36] + z[32] + n<T>(1,9)*z[26] + z[31];
    z[25]=z[17]*z[25];
    z[26]= - static_cast<T>(77)- z[48];
    z[26]=z[26]*z[73];
    z[31]=z[47] - z[60];
    z[32]=n<T>(23,9)*z[4];
    z[32]=z[31]*z[32];
    z[36]=n<T>(77,9) + 6*z[60];
    z[36]=z[10]*z[36];
    z[26]=z[32] + z[26] + z[36];
    z[26]=z[4]*z[26];
    z[32]=z[27] - 62*z[46];
    z[36]=269*z[12] - 602*z[2];
    z[36]=n<T>(1,9)*z[36] + z[95];
    z[36]=z[9]*z[36];
    z[37]=3*z[9];
    z[42]=z[37] - z[10];
    z[61]= - z[42]*z[74];
    z[62]= - z[19] - n<T>(1,3)*z[12];
    z[62]= - n<T>(64,9)*z[10] + 10*z[62] + n<T>(193,9)*z[8];
    z[62]=z[7]*z[62];
    z[63]=z[2] + z[7];
    z[63]=z[5]*z[63];
    z[26]=z[26] + 2*z[63] + z[62] + z[61] + z[36] + n<T>(1,9)*z[32] + 24*
    z[56];
    z[26]=z[4]*z[26];
    z[27]=z[27] + z[79];
    z[32]=z[10] + z[2];
    z[32]= - z[32]*z[87];
    z[36]=n<T>(499,9)*z[12] + z[67];
    z[36]=z[11]*z[36];
    z[56]= - n<T>(28,3)*z[11] + n<T>(11,3)*z[8] - z[44];
    z[56]=z[3]*z[56];
    z[27]= - z[54] + z[56] + z[36] + n<T>(1,9)*z[27] + z[32];
    z[27]=z[7]*z[27];
    z[32]=z[15] - n<T>(80,9)*z[12];
    z[32]= - n<T>(220,9)*z[35] + z[55] + 2*z[32] + z[38];
    z[32]=z[11]*z[32];
    z[34]= - z[57] + 12*z[34];
    z[32]=z[32] - z[34];
    z[32]=z[11]*z[32];
    z[35]=z[2] - z[92];
    z[35]=z[35]*z[52];
    z[36]= - z[2] + z[42];
    z[36]=z[11]*z[36];
    z[35]=z[36] - 2*z[69] - z[51] + z[35];
    z[36]= - static_cast<T>(161)+ z[82];
    z[36]=z[36]*z[73];
    z[38]=z[84] + z[80];
    z[42]=n<T>(148,9)*z[5];
    z[42]=z[38]*z[42];
    z[51]= - n<T>(161,9) + 13*z[84];
    z[51]=z[9]*z[51];
    z[36]=z[42] + z[36] + z[51];
    z[36]=z[5]*z[36];
    z[42]=43*z[9] + n<T>(125,9)*z[8] + z[44];
    z[42]=z[3]*z[42];
    z[51]= - z[7]*z[74];
    z[35]=z[36] + z[51] + 2*z[35] + z[42];
    z[35]=z[5]*z[35];
    z[22]= - 220*z[22] + 580*z[2] - 544*z[12] - z[28];
    z[22]=z[22]*z[103];
    z[22]=z[22] + z[34];
    z[22]=z[9]*z[22];
    z[28]= - z[52] - z[9];
    z[28]=z[9]*z[28];
    z[28]= - n<T>(10,9)*z[46] + z[28];
    z[34]=static_cast<T>(17)+ 110*z[43];
    z[34]=z[34]*z[45];
    z[36]= - z[15] + z[52];
    z[34]=2*z[36] + z[34];
    z[34]=z[11]*z[34];
    z[28]=2*z[28] + z[34];
    z[28]=z[28]*z[30];
    z[22]=z[25] + z[26] + z[35] + z[27] + z[28] + z[22] + z[32];
    z[22]=z[17]*z[22];
    z[25]= - n<T>(136,3) - z[82];
    z[25]=z[25]*z[41];
    z[26]=z[5]*z[38];
    z[27]=n<T>(19,9) + 18*z[60];
    z[27]=z[10]*z[27];
    z[28]=n<T>(61,9) - 22*z[84];
    z[28]=z[9]*z[28];
    z[25]= - n<T>(382,9)*z[26] - n<T>(323,18)*z[3] - 12*z[11] + z[28] + z[27] + 
    z[25] + z[83] - 8*z[15];
    z[25]=z[5]*z[25];
    z[26]= - static_cast<T>(97)- z[48];
    z[26]=z[26]*z[41];
    z[27]=static_cast<T>(5)+ 3*z[60];
    z[27]=z[27]*z[92];
    z[28]=z[4]*z[31];
    z[30]=z[18] + 4*z[5];
    z[26]=n<T>(25,3)*z[28] + n<T>(43,6)*z[7] + 8*z[11] - z[9] + z[27] + n<T>(577,18)
   *z[2] + z[26] + n<T>(1,9)*z[12] + 12*z[15] - z[40] + z[30];
    z[26]=z[4]*z[26];
    z[27]=11*z[21] - static_cast<T>(5)+ n<T>(92,3)*z[20];
    z[27]=z[11]*z[27];
    z[27]=z[27] - z[33] - z[53] - z[94] - n<T>(113,3)*z[12];
    z[27]=z[11]*z[27];
    z[28]=z[29] - z[44] - z[15] - n<T>(58,9)*z[8];
    z[29]= - static_cast<T>(7)+ z[43];
    z[29]=z[11]*z[29];
    z[28]=n<T>(1,3)*z[29] + 2*z[28] + z[37];
    z[28]=z[3]*z[28];
    z[29]=z[10]*z[13];
    z[31]=static_cast<T>(5)- 4*z[29];
    z[31]=z[31]*z[52];
    z[32]= - static_cast<T>(13)- 9*z[39];
    z[32]=z[32]*z[74];
    z[31]= - n<T>(65,6)*z[3] + z[32] + z[31] + n<T>(40,3)*z[8] + z[40] - n<T>(82,3)*
    z[12];
    z[31]=z[7]*z[31];
    z[32]= - z[44] + z[10];
    z[32]=z[32]*z[87];
    z[33]=521*z[20] - 323*z[21];
    z[33]=z[33]*z[103];
    z[33]=z[33] - n<T>(439,9)*z[2] + 44*z[12] + z[53];
    z[33]=z[9]*z[33];
    z[22]=z[22] + z[26] + z[25] + z[31] + z[28] + z[27] + z[32] + z[33];
    z[22]=z[17]*z[22];
    z[25]=z[97] + z[58];
    z[26]=2*z[84];
    z[27]= - n<T>(14,3)*z[7] + z[30];
    z[27]=z[16]*z[27];
    z[25]=4*z[43] - z[26] - z[60] + 13*z[47] + z[27] - 6*z[25];
    z[25]=z[4]*z[25];
    z[27]=12*z[58];
    z[28]=z[16]*z[18];
    z[26]= - 2*z[43] - z[26] - 12*z[60] + z[27] + n<T>(11,3) + z[28];
    z[26]=z[3]*z[26];
    z[28]=z[64] + z[40] - z[6];
    z[28]=z[16]*z[28];
    z[30]=2*z[19] - n<T>(5,9)*z[6];
    z[30]=z[16]*z[30];
    z[30]=n<T>(136,9) + 5*z[30];
    z[30]=z[30]*z[21];
    z[31]= - static_cast<T>(41)+ z[49];
    z[31]=z[16]*z[31];
    z[32]= - z[2]*z[72]*npow(z[16],2);
    z[31]=z[32] + 18*z[13] + z[31];
    z[31]=z[11]*z[31];
    z[28]=z[31] + z[30] + static_cast<T>(1)+ z[28];
    z[28]=z[11]*z[28];
    z[30]= - static_cast<T>(9)+ n<T>(25,9)*z[89];
    z[30]=z[30]*z[96];
    z[31]= - 34*z[14] + 31*z[16];
    z[31]=z[31]*z[41];
    z[32]= - n<T>(59,9) - z[91];
    z[30]=z[31] + 2*z[32] + z[30];
    z[30]=z[8]*z[30];
    z[31]=2*z[13] - z[81];
    z[31]=z[31]*z[52];
    z[32]=3*z[14] + z[59];
    z[32]=z[6]*z[32];
    z[27]=z[31] + z[27] - static_cast<T>(7)+ z[32];
    z[27]=z[27]*z[92];
    z[29]= - n<T>(7,3)*z[50] + 3*z[39] + n<T>(104,9) - z[29];
    z[29]=z[7]*z[29];
    z[31]= - z[94]*z[81];
    z[31]= - n<T>(5,3) + z[31];
    z[31]=z[6]*z[31];
    z[32]=static_cast<T>(128)- 25*z[89];
    z[32]=z[6]*z[32];
    z[32]= - z[40] + n<T>(1,9)*z[32];
    z[32]=z[16]*z[32];
    z[33]=n<T>(155,2) - n<T>(17,3)*z[89];
    z[32]=n<T>(1,3)*z[33] + z[32];
    z[32]=z[2]*z[32];
    z[20]= - static_cast<T>(4)- n<T>(41,3)*z[20];
    z[20]=2*z[20] - n<T>(5,3)*z[21];
    z[20]=z[9]*z[20];
    z[21]=n<T>(323,3) - 139*z[100];
    z[21]=z[12]*z[21];
    z[20]=z[23] + z[22] + z[25] + z[24] + 2*z[29] + z[26] + z[28] + 
    z[20] + z[27] + z[32] + z[30] - z[53] + n<T>(2,3)*z[21] + n<T>(274,9)*z[18]
    + z[31];

    r += z[20]*z[1];
 
    return r;
}

template double qqb_2lNLC_r11(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r11(const std::array<dd_real,31>&);
#endif
