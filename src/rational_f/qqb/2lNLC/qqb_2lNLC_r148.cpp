#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r148(const std::array<T,31>& k) {
  T z[53];
  T r = 0;

    z[1]=k[1];
    z[2]=k[10];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[14];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[7];
    z[9]=k[11];
    z[10]=k[3];
    z[11]=k[9];
    z[12]=k[15];
    z[13]=k[8];
    z[14]=2*z[7];
    z[15]= - z[12] - z[13];
    z[15]=z[15]*z[14];
    z[16]=z[5]*z[7];
    z[17]=n<T>(1,3)*z[16];
    z[18]=4*z[12];
    z[19]= - z[6]*z[18];
    z[15]=z[19] + z[15] - z[17];
    z[15]=z[6]*z[15];
    z[19]=z[7]*z[8];
    z[20]=z[19] - 1;
    z[21]=npow(z[7],2);
    z[22]=z[20]*z[21];
    z[23]=npow(z[6],3);
    z[24]=z[23]*z[2];
    z[25]=2*z[6];
    z[26]=z[7] - z[25];
    z[26]=z[6]*z[26];
    z[26]= - z[24] + z[22] + z[26];
    z[27]=2*z[4];
    z[26]=z[26]*z[27];
    z[28]=2*z[2];
    z[23]=z[23]*z[28];
    z[29]=z[12]*z[23];
    z[30]=z[19]*z[13];
    z[31]=z[30] - z[13];
    z[32]= - z[31]*z[14];
    z[32]= - n<T>(5,3) + z[32];
    z[32]=z[7]*z[32];
    z[33]=n<T>(1,3)*z[5];
    z[34]= - z[21]*z[33];
    z[15]=z[26] - z[29] + z[15] + z[32] + z[34];
    z[15]=z[3]*z[15];
    z[26]=npow(z[6],2);
    z[32]=z[26]*z[28];
    z[34]= - 3*z[26] - 4*z[24];
    z[34]=z[4]*z[34];
    z[35]=3*z[6];
    z[34]=z[34] - z[32] + z[14] - z[35];
    z[34]=z[4]*z[34];
    z[36]=z[32]*z[12];
    z[37]=2*z[5];
    z[38]=2*z[12];
    z[39]=z[37] - z[38] + z[13];
    z[39]=z[6]*z[39];
    z[40]=z[7]*z[13];
    z[15]=z[15] + z[34] - z[36] + z[39] + n<T>(5,3)*z[16] - n<T>(5,3) - 2*z[40];
    z[15]=z[3]*z[15];
    z[34]=z[17] + n<T>(4,3) - z[19];
    z[32]=z[32] + z[6];
    z[39]=z[4]*z[32];
    z[41]=z[6]*z[9];
    z[42]=z[2]*z[6];
    z[34]= - 8*z[39] + n<T>(20,3)*z[42] + 2*z[34] - z[41];
    z[34]=z[4]*z[34];
    z[39]=z[28]*z[6];
    z[43]=n<T>(2,3)*z[5];
    z[44]=z[12] - z[43];
    z[44]=z[44]*z[39];
    z[45]=3*z[13];
    z[46]=2*z[30];
    z[47]=z[7]*z[11];
    z[48]= - static_cast<T>(1)- 7*z[47];
    z[48]=z[5]*z[48];
    z[15]=z[15] + z[34] + z[44] + n<T>(4,3)*z[48] + z[45] + z[46];
    z[15]=z[3]*z[15];
    z[34]= - z[7] + z[6];
    z[34]=z[6]*z[34];
    z[22]= - z[23] - z[22] + z[34];
    z[22]=z[4]*z[22];
    z[20]=z[7]*z[20];
    z[20]=z[22] + z[20] + z[32];
    z[20]=z[4]*z[20];
    z[22]=z[38] - z[31];
    z[22]=z[7]*z[22];
    z[32]=z[18] - z[43];
    z[34]= - z[13] + z[32];
    z[34]=z[6]*z[34];
    z[17]=z[20] + z[36] + z[34] + z[22] - z[17];
    z[17]=z[3]*z[17];
    z[20]=z[26]*z[2];
    z[22]= - static_cast<T>(3)+ 2*z[19];
    z[26]=z[7]*z[22];
    z[26]= - 8*z[20] + z[26] + 4*z[6];
    z[26]=z[4]*z[26];
    z[22]=z[26] + n<T>(23,3)*z[42] - z[22];
    z[22]=z[4]*z[22];
    z[26]=z[38] + z[5];
    z[26]=z[26]*z[39];
    z[17]=z[17] + z[22] + z[26] + z[46] - z[45] + z[32];
    z[17]=z[3]*z[17];
    z[22]=static_cast<T>(14)- 5*z[16];
    z[22]=n<T>(1,3)*z[22] - 12*z[42];
    z[22]=z[4]*z[22];
    z[26]=3*z[9];
    z[22]=z[22] + n<T>(26,3)*z[2] + z[26] - z[33];
    z[22]=z[4]*z[22];
    z[32]=z[5]*z[11];
    z[33]=n<T>(5,3)*z[5];
    z[34]=z[12] + z[33];
    z[34]=z[2]*z[34];
    z[32]= - n<T>(7,3)*z[32] + z[34];
    z[17]=z[17] + 2*z[32] + z[22];
    z[17]=z[3]*z[17];
    z[16]=z[42] + z[16] - 1;
    z[16]=z[16]*z[4];
    z[22]=z[2]*z[5];
    z[32]=z[22]*z[6];
    z[32]=z[32] - z[5];
    z[34]= - z[16] - z[32];
    z[36]=n<T>(1,3)*z[3];
    z[34]=z[34]*z[36];
    z[39]=z[37] - n<T>(7,3)*z[2];
    z[39]=z[4]*z[39];
    z[34]=z[34] - n<T>(7,3)*z[22] + z[39];
    z[34]=z[3]*z[34];
    z[16]= - z[16] - 11*z[5] + 7*z[2];
    z[16]=z[4]*z[16];
    z[16]= - 8*z[22] + z[16];
    z[39]=n<T>(1,3)*z[4];
    z[16]=z[16]*z[39];
    z[16]=z[16] + z[34];
    z[16]=z[3]*z[16];
    z[34]=z[37] - z[2];
    z[42]= - z[34]*z[39];
    z[44]=z[28]*z[5];
    z[42]= - z[44] + z[42];
    z[48]=npow(z[4],2);
    z[42]=z[42]*z[48];
    z[34]= - z[4]*z[34];
    z[34]=z[22] + z[34];
    z[34]=z[34]*z[36];
    z[44]=z[4]*z[44];
    z[34]=z[44] + z[34];
    z[34]=z[3]*z[34];
    z[34]=z[42] + z[34];
    z[34]=z[3]*z[34];
    z[42]=npow(z[4],3);
    z[44]= - z[4]*npow(z[3],2);
    z[44]= - z[42] + z[44];
    z[36]=z[1]*z[36]*z[44];
    z[36]=z[36] - n<T>(1,3)*z[42];
    z[36]=z[22]*z[36];
    z[34]=z[34] + z[36];
    z[34]=z[1]*z[34];
    z[36]=z[8]*z[22]*z[26];
    z[32]=z[4]*z[32];
    z[32]= - 7*z[22] + z[32];
    z[32]=z[32]*z[48];
    z[16]=z[34] + z[16] + z[36] + n<T>(1,3)*z[32];
    z[16]=z[1]*z[16];
    z[32]=z[6] - z[10];
    z[34]= - z[32]*z[39];
    z[36]=z[6]*z[5];
    z[42]=z[11]*z[10];
    z[34]=z[34] + 3*z[36] - n<T>(11,3) + 2*z[42];
    z[34]=z[2]*z[34];
    z[19]=z[9]*z[19];
    z[19]=z[19] - z[33] + z[34];
    z[19]=z[4]*z[19];
    z[33]=4*z[9];
    z[34]=z[8]*z[33];
    z[44]=8*z[11] - z[5];
    z[44]=z[2]*z[44];
    z[19]=z[19] + z[34] + n<T>(1,3)*z[44];
    z[19]=z[4]*z[19];
    z[34]=z[33]*z[5];
    z[44]= - z[38] + z[8];
    z[44]=z[44]*z[34];
    z[48]=3*z[8];
    z[33]= - z[48] - z[33];
    z[22]=z[33]*z[22];
    z[16]=z[16] + z[17] + z[19] + z[44] + z[22];
    z[16]=z[1]*z[16];
    z[17]=npow(z[10],2);
    z[19]= - z[6]*z[10];
    z[19]= - z[17] + z[19];
    z[19]=z[2]*z[19];
    z[19]=z[19] + z[10] + z[6];
    z[19]=z[19]*z[39];
    z[22]=z[42] - 1;
    z[33]=z[7]*z[9];
    z[39]=z[17]*z[11];
    z[44]=n<T>(4,3)*z[10] - z[39];
    z[44]=4*z[44] - 15*z[6];
    z[44]=z[2]*z[44];
    z[19]=z[19] + z[44] + 4*z[22] + z[33];
    z[19]=z[4]*z[19];
    z[44]=n<T>(11,3)*z[11];
    z[49]=2*z[8];
    z[50]=z[44] - z[49];
    z[51]=z[8] - 2*z[13];
    z[51]=z[51]*z[33];
    z[52]= - static_cast<T>(3)- n<T>(2,3)*z[42];
    z[52]=2*z[52] + n<T>(7,3)*z[36];
    z[52]=z[2]*z[52];
    z[19]=z[19] + z[52] + n<T>(14,3)*z[5] + z[51] + 2*z[50] + 9*z[9];
    z[19]=z[4]*z[19];
    z[50]=3*z[12];
    z[44]=z[9] - z[49] - z[44] + z[50];
    z[49]=8*z[12];
    z[51]= - z[49] + z[8];
    z[33]=z[51]*z[33];
    z[33]=2*z[44] + z[33];
    z[33]=z[5]*z[33];
    z[44]=2*z[9];
    z[50]=z[43] + z[44] + n<T>(8,3)*z[11] - z[50];
    z[48]= - z[48] + z[44];
    z[36]=z[48]*z[36];
    z[36]=2*z[50] + z[36];
    z[36]=z[2]*z[36];
    z[45]=z[49] - z[45];
    z[45]=z[9]*z[45];
    z[15]=z[16] + z[15] + z[19] + z[36] + z[45] + z[33];
    z[15]=z[1]*z[15];
    z[16]=z[13] - z[46];
    z[16]=z[16]*z[21];
    z[19]= - z[6]*z[38];
    z[19]=z[40] + z[19];
    z[19]=z[6]*z[19];
    z[33]=z[25] + z[7];
    z[33]=z[33]*z[6];
    z[23]= - z[33] - z[23];
    z[23]=z[4]*z[23];
    z[31]= - z[31]*npow(z[7],3);
    z[21]= - z[6]*z[13]*z[21];
    z[21]=z[31] + z[21];
    z[21]=z[3]*z[21];
    z[16]=z[21] + z[23] - z[29] + z[16] + z[19];
    z[16]=z[3]*z[16];
    z[19]= - z[33] - z[24];
    z[19]=z[19]*z[27];
    z[19]=z[19] - 4*z[20] + 5*z[7] + z[35];
    z[19]=z[4]*z[19];
    z[21]=n<T>(2,3) - z[47];
    z[14]=z[5]*z[21]*z[14];
    z[18]= - z[18]*z[20];
    z[20]= - z[13] - z[30];
    z[20]=z[7]*z[20];
    z[21]= - z[13] + z[43];
    z[21]=z[6]*z[21];
    z[14]=z[16] + z[19] + z[18] + z[21] + z[20] + z[14];
    z[14]=z[3]*z[14];
    z[16]=z[11]*npow(z[10],3);
    z[18]= - z[6]*z[32];
    z[16]=z[18] - z[17] + z[16];
    z[16]=z[2]*z[16];
    z[17]=z[10] - z[39];
    z[18]=z[7]*z[22];
    z[16]=z[16] - z[25] + 2*z[17] + z[18];
    z[16]=z[16]*z[27];
    z[17]=z[40]*z[9];
    z[18]=z[17] + 2*z[11] + z[26];
    z[18]=z[7]*z[18];
    z[19]=n<T>(1,3)*z[10] - z[39];
    z[19]=2*z[19] - n<T>(11,3)*z[6];
    z[19]=z[19]*z[28];
    z[20]= - n<T>(8,3) + z[42];
    z[16]=z[16] + z[19] - z[41] + 2*z[20] + z[18];
    z[16]=z[4]*z[16];
    z[18]=z[12] + z[11];
    z[19]=z[44] - z[18];
    z[19]=z[7]*z[19];
    z[19]= - n<T>(10,3) + z[19];
    z[19]=z[19]*z[37];
    z[20]=z[9]*z[13];
    z[20]=z[20] - z[34];
    z[20]=z[6]*z[20];
    z[21]= - z[6]*z[12];
    z[21]=z[21] + n<T>(7,3) + z[42];
    z[21]=z[21]*z[28];
    z[14]=z[15] + z[14] + z[16] + z[21] + z[20] + z[19] + 2*z[18] + 
    z[17];

    r += z[14]*z[1];
 
    return r;
}

template double qqb_2lNLC_r148(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r148(const std::array<dd_real,31>&);
#endif
