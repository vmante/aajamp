#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r877(const std::array<T,31>& k) {
  T z[102];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[8];
    z[9]=k[3];
    z[10]=k[4];
    z[11]=k[5];
    z[12]=k[9];
    z[13]=k[2];
    z[14]=k[15];
    z[15]=7*z[5];
    z[16]=2*z[3];
    z[17]=z[15] - z[16];
    z[18]=z[17]*z[3];
    z[19]=npow(z[3],2);
    z[20]=z[19]*z[5];
    z[21]=2*z[10];
    z[22]=z[20]*z[21];
    z[18]=z[18] + z[22];
    z[18]=z[18]*z[10];
    z[23]=z[16]*z[9];
    z[24]=z[23] + 5;
    z[25]= - z[3]*z[24];
    z[25]=z[25] + z[18];
    z[25]=z[10]*z[25];
    z[26]=z[11]*z[6];
    z[27]=z[26] + 3;
    z[28]=z[3]*z[9];
    z[29]=3*z[28];
    z[30]=z[6]*z[9];
    z[25]=z[25] - z[29] - z[30] - z[27];
    z[25]=z[2]*z[25];
    z[31]=2*z[4];
    z[32]=z[31] + z[6];
    z[33]=9*z[5];
    z[34]=z[33] + z[32];
    z[35]=z[10]*z[5];
    z[36]=z[19]*z[35];
    z[37]=z[3]*z[5];
    z[38]=z[37] - z[36];
    z[38]=z[38]*z[21];
    z[39]=z[11]*z[4];
    z[40]= - z[39]*z[33];
    z[41]=4*z[6];
    z[42]=3*z[4];
    z[43]=z[41] - z[42];
    z[43]=z[11]*z[43];
    z[44]=z[16]*z[10];
    z[43]= - z[44] + z[43];
    z[43]=z[7]*z[43];
    z[45]= - static_cast<T>(7)+ z[23];
    z[45]=z[3]*z[45];
    z[25]=z[25] + z[43] + z[40] + z[38] + 3*z[34] + z[45];
    z[25]=z[2]*z[25];
    z[34]=2*z[39];
    z[38]=npow(z[5],2);
    z[40]=z[34]*z[38];
    z[43]=17*z[4];
    z[45]=4*z[5];
    z[46]= - z[43] + z[45];
    z[46]=z[5]*z[46];
    z[47]=z[16]*z[4];
    z[48]=z[28] - 1;
    z[49]=z[48]*z[47];
    z[37]=z[37] + z[38];
    z[50]=z[37]*z[44];
    z[51]=3*z[6];
    z[52]=4*z[4];
    z[53]= - z[51] + z[52];
    z[53]=z[11]*z[53];
    z[54]=z[10]*z[3];
    z[53]= - z[54] + z[53];
    z[53]=z[7]*z[53];
    z[55]=z[4] - z[6];
    z[56]=3*z[55] + z[3];
    z[53]=3*z[56] + z[53];
    z[53]=z[7]*z[53];
    z[56]=z[4]*z[6];
    z[57]=9*z[56];
    z[25]=z[25] + z[53] - z[40] + z[50] + z[49] + z[57] + z[46];
    z[25]=z[2]*z[25];
    z[46]=npow(z[10],2);
    z[49]= - static_cast<T>(3)- z[35];
    z[49]=z[49]*z[46];
    z[50]=z[26] + 1;
    z[53]=2*z[11];
    z[58]= - z[50]*z[53];
    z[58]=z[10] + z[58];
    z[59]=6*z[11];
    z[58]=z[58]*z[59];
    z[49]=z[49] + z[58];
    z[49]=z[2]*z[49];
    z[58]=4*z[35];
    z[60]=static_cast<T>(5)+ z[58];
    z[60]=z[10]*z[60];
    z[61]=npow(z[11],2);
    z[62]=6*z[61];
    z[63]=z[6]*z[62];
    z[49]=z[49] + z[60] + z[63];
    z[49]=z[2]*z[49];
    z[60]=7*z[35];
    z[63]=3*z[10];
    z[64]= - z[7]*z[63];
    z[49]=z[49] + z[64] + 27*z[26] - static_cast<T>(1)- z[60];
    z[49]=z[2]*z[49];
    z[64]=z[34] - 1;
    z[65]=3*z[11];
    z[66]=z[64]*z[65];
    z[67]= - z[21] + z[66];
    z[67]=z[11]*z[67];
    z[68]=z[66] - z[10];
    z[69]=z[61]*z[7];
    z[70]=z[68]*z[69];
    z[67]=z[67] + z[70];
    z[67]=z[7]*z[67];
    z[66]=z[67] - z[10] - z[66];
    z[66]=z[7]*z[66];
    z[67]=2*z[26];
    z[70]=z[67] + 1;
    z[71]= - z[70]*z[65];
    z[72]=z[61]*z[2];
    z[73]=z[50]*z[11];
    z[74]=z[73] - z[10];
    z[75]=z[74]*z[72];
    z[76]=2*z[61];
    z[77]= - z[6]*z[76];
    z[77]=z[10] + z[77];
    z[77]=z[11]*z[77];
    z[75]=z[77] + 2*z[75];
    z[75]=z[2]*z[75];
    z[71]=3*z[75] + z[10] + z[71];
    z[71]=z[2]*z[71];
    z[75]= - z[55]*z[59];
    z[66]=z[71] + z[75] + z[66];
    z[66]=z[8]*z[66];
    z[71]=4*z[39];
    z[75]=z[71] - 1;
    z[77]=z[75]*z[61];
    z[78]=npow(z[10],3);
    z[79]=z[78]*z[5];
    z[77]=z[79] + 3*z[77];
    z[77]=z[77]*z[7];
    z[79]=2*z[5];
    z[80]=z[46]*z[79];
    z[81]=3*z[39];
    z[82]=z[81] - 1;
    z[83]= - z[11]*z[82];
    z[80]=z[80] + z[83];
    z[80]=2*z[80] - z[77];
    z[80]=z[7]*z[80];
    z[17]= - z[10]*z[17];
    z[17]=z[80] + 27*z[39] - static_cast<T>(1)+ z[17];
    z[17]=z[7]*z[17];
    z[80]=z[4] + z[6];
    z[83]=z[44]*z[80];
    z[84]=z[12] - z[14];
    z[85]= - z[13]*z[84];
    z[86]=static_cast<T>(4)+ z[85];
    z[86]=z[86]*z[45];
    z[87]=4*z[3];
    z[88]= - z[85]*z[87];
    z[17]=z[66] + z[49] + z[17] + z[83] + z[88] + z[86] - 21*z[6] + 19*
    z[4];
    z[17]=z[8]*z[17];
    z[49]=z[84]*z[79];
    z[66]=2*z[14];
    z[84]= - z[66] - z[12];
    z[32]=2*z[84] + z[32];
    z[32]=z[3]*z[32];
    z[32]=z[49] + z[32];
    z[49]= - z[5] - z[3];
    z[49]=z[49]*z[63];
    z[84]=z[5]*z[46];
    z[86]=z[4]*z[62];
    z[84]=z[84] + z[86];
    z[84]=z[7]*z[84];
    z[49]=z[49] + z[84];
    z[49]=z[7]*z[49];
    z[49]=z[49] + z[4] + 9*z[3];
    z[49]=z[7]*z[49];
    z[84]=static_cast<T>(5)+ 6*z[26];
    z[84]=z[84]*z[11];
    z[86]= - static_cast<T>(9)- z[60];
    z[86]=z[10]*z[86];
    z[86]=z[86] + z[84];
    z[86]=z[2]*z[86];
    z[88]=z[7]*z[21];
    z[86]=z[86] + z[88] + static_cast<T>(11)+ 25*z[35];
    z[86]=z[2]*z[86];
    z[88]=z[7]*z[10];
    z[88]= - static_cast<T>(9)+ z[88];
    z[88]=z[7]*z[88];
    z[86]=z[86] + z[88] + z[6] - 23*z[5];
    z[86]=z[2]*z[86];
    z[17]=z[17] + z[86] + 2*z[32] + z[49];
    z[17]=z[8]*z[17];
    z[32]=z[5] - z[6];
    z[49]=z[32]*z[19]*z[21];
    z[86]=z[5]*z[6];
    z[88]=8*z[86];
    z[89]=z[32]*z[3];
    z[49]=z[49] - 5*z[89] + z[56] - z[88];
    z[49]=z[7]*z[49];
    z[88]=z[88] + z[89];
    z[88]=z[88]*z[16];
    z[49]=z[88] + z[49];
    z[49]=z[7]*z[49];
    z[88]= - z[16]*z[86];
    z[90]= - z[6]*z[38];
    z[88]=z[90] + z[88];
    z[88]=z[88]*z[16];
    z[49]=z[88] + z[49];
    z[49]=z[7]*z[49];
    z[88]=z[4]*z[9];
    z[88]= - static_cast<T>(1)+ z[88];
    z[88]=z[88]*z[16];
    z[90]=5*z[4];
    z[88]=z[88] + z[90] + z[15];
    z[88]=z[3]*z[88];
    z[91]=z[22] - z[56];
    z[92]=8*z[5];
    z[92]=z[92]*z[4];
    z[93]=z[55]*z[7];
    z[88]=z[93] + z[88] - z[92] + z[91];
    z[88]=z[2]*z[88];
    z[94]=z[31] + z[5];
    z[94]=z[3]*z[94];
    z[92]= - z[92] + z[94];
    z[92]=z[3]*z[92];
    z[94]=z[7]*z[6]*z[31];
    z[92]=z[92] + z[94];
    z[88]=2*z[92] + z[88];
    z[88]=z[2]*z[88];
    z[92]= - z[5]*z[16];
    z[92]= - z[38] + z[92];
    z[47]=z[92]*z[47];
    z[92]=4*z[56];
    z[94]= - z[92] + z[93];
    z[95]=npow(z[7],2);
    z[94]=z[94]*z[95];
    z[47]=z[88] + z[47] + z[94];
    z[47]=z[2]*z[47];
    z[88]=z[20]*z[41];
    z[94]=z[32]*z[16];
    z[96]= - z[6]*z[15];
    z[96]=z[96] - z[94];
    z[96]=z[7]*z[3]*z[96];
    z[88]=z[88] + z[96];
    z[88]=z[88]*z[95];
    z[96]= - z[20]*z[52];
    z[97]=z[5] + z[4];
    z[97]=z[97]*z[16];
    z[98]=z[15]*z[4];
    z[99]= - z[98] + z[97];
    z[99]=z[3]*z[99];
    z[100]=z[7]*z[56];
    z[99]=z[99] + z[100];
    z[99]=z[2]*z[99];
    z[96]=z[96] + z[99];
    z[96]=z[2]*z[96];
    z[99]=npow(z[7],3);
    z[100]=z[99]*z[56];
    z[96]=z[100] + z[96];
    z[96]=z[2]*z[96];
    z[99]= - z[6]*z[99];
    z[100]=npow(z[2],3);
    z[101]= - z[4]*z[100];
    z[99]=z[99] + z[101];
    z[20]=z[1]*z[20]*z[99];
    z[99]=z[100]*z[5];
    z[100]=npow(z[8],2)*z[99];
    z[20]=2*z[20] + z[100] + z[88] + z[96];
    z[20]=z[1]*z[20];
    z[88]=z[69]*z[4];
    z[34]=z[34] + z[88];
    z[34]=z[7]*z[34];
    z[34]=z[4] + z[34];
    z[34]=z[7]*z[34];
    z[96]=z[6]*z[72];
    z[96]= - z[67] + z[96];
    z[96]=z[2]*z[96];
    z[96]=z[6] + z[96];
    z[96]=z[2]*z[96];
    z[34]=z[34] + z[96];
    z[34]=z[8]*z[34];
    z[96]= - static_cast<T>(1)- 3*z[35];
    z[96]=z[2]*z[96];
    z[96]=z[45] + z[96];
    z[96]=z[96]*npow(z[2],2);
    z[34]=z[96] + z[34];
    z[34]=z[8]*z[34];
    z[34]= - 5*z[99] + z[34];
    z[34]=z[8]*z[34];
    z[20]=z[20] + z[34] + z[49] + z[47];
    z[20]=z[1]*z[20];
    z[34]=z[9]*z[42];
    z[24]=z[34] - z[24];
    z[24]=z[3]*z[24];
    z[34]= - z[81] + 12;
    z[34]=z[5]*z[34];
    z[47]=z[93]*z[11];
    z[49]=static_cast<T>(2)+ z[30];
    z[49]=z[4]*z[49];
    z[18]= - z[47] + z[18] + z[24] + z[6] + z[49] + z[34];
    z[18]=z[2]*z[18];
    z[24]= - z[56] - z[98];
    z[34]=3*z[5];
    z[49]=z[52] + z[34];
    z[49]=3*z[49] - z[16];
    z[49]=z[3]*z[49];
    z[18]=z[18] + 4*z[93] + 3*z[24] + z[49];
    z[18]=z[2]*z[18];
    z[24]= - z[38]*z[52];
    z[49]= - 9*z[4] + z[79];
    z[49]=z[5]*z[49];
    z[49]=z[49] + z[97];
    z[49]=z[3]*z[49];
    z[47]= - z[47] - 4*z[55] - z[3];
    z[47]=z[7]*z[47];
    z[47]= - 6*z[56] + z[47];
    z[47]=z[7]*z[47];
    z[18]=z[18] + z[47] + z[24] + z[49];
    z[18]=z[2]*z[18];
    z[24]=static_cast<T>(1)+ z[35];
    z[24]=z[24]*z[63];
    z[47]= - z[70]*z[53];
    z[24]=z[24] + z[47];
    z[24]=z[2]*z[24];
    z[47]=z[67] - static_cast<T>(1)- z[58];
    z[24]=2*z[47] + z[24];
    z[24]=z[2]*z[24];
    z[24]=z[24] + z[15] + z[7];
    z[24]=z[2]*z[24];
    z[47]=z[82]*z[53];
    z[49]=z[75]*z[69];
    z[49]=z[47] + z[49];
    z[49]=z[7]*z[49];
    z[49]= - static_cast<T>(1)+ z[49];
    z[49]=z[7]*z[49];
    z[52]=4*z[72];
    z[58]=z[50]*z[52];
    z[58]= - z[84] + z[58];
    z[58]=z[2]*z[58];
    z[58]=static_cast<T>(1)+ z[58];
    z[58]=z[2]*z[58];
    z[75]=2*z[55];
    z[49]=z[58] - z[75] + z[49];
    z[49]=z[8]*z[49];
    z[58]= - z[12]*z[79];
    z[82]= - z[3]*z[4];
    z[58]=z[58] + z[82];
    z[82]= - z[39] - z[88];
    z[82]=z[7]*z[82];
    z[82]= - z[3] + 4*z[82];
    z[82]=z[7]*z[82];
    z[24]=z[49] + z[24] + 2*z[58] + z[82];
    z[24]=z[8]*z[24];
    z[49]=18*z[5];
    z[58]=static_cast<T>(4)+ 11*z[35];
    z[58]=z[2]*z[58];
    z[58]= - z[49] + z[58];
    z[58]=z[2]*z[58];
    z[58]=z[95] + z[58];
    z[58]=z[2]*z[58];
    z[82]= - z[3]*z[95];
    z[24]=z[24] + z[82] + z[58];
    z[24]=z[8]*z[24];
    z[32]=3*z[32] - z[16];
    z[32]=z[3]*z[32];
    z[32]=z[32] - z[91];
    z[32]=z[10]*z[32];
    z[58]= - z[34]*z[50];
    z[32]=z[32] + 2*z[6] + z[4] + z[58];
    z[32]=z[7]*z[32];
    z[32]=z[32] + 9*z[89] - z[92] + 21*z[86];
    z[32]=z[7]*z[32];
    z[41]= - z[38]*z[41];
    z[58]= - 9*z[6] - z[79];
    z[58]=z[5]*z[58];
    z[58]=z[58] - z[94];
    z[58]=z[3]*z[58];
    z[32]=z[32] + z[41] + z[58];
    z[32]=z[7]*z[32];
    z[41]= - z[16]*z[55]*z[37];
    z[18]=z[20] + z[24] + z[18] + z[41] + z[32];
    z[18]=z[1]*z[18];
    z[20]= - z[34] - z[16];
    z[20]=z[3]*z[20];
    z[20]=z[20] - z[36];
    z[20]=z[20]*z[21];
    z[24]=z[48]*z[87];
    z[32]= - z[5] + z[16];
    z[32]=z[3]*z[32];
    z[22]=z[32] + z[22];
    z[22]=z[10]*z[22];
    z[22]=z[22] + z[4] + z[24];
    z[22]=z[10]*z[22];
    z[24]=z[39] - 1;
    z[22]=z[22] - z[24];
    z[22]=z[7]*z[22];
    z[32]=z[33]*z[50];
    z[33]= - z[51] - z[31];
    z[20]=z[22] + z[20] + 2*z[33] + z[32];
    z[20]=z[7]*z[20];
    z[22]=17*z[6];
    z[32]= - z[22] - z[79];
    z[32]=z[5]*z[32];
    z[33]=2*z[12] - z[6];
    z[33]=z[33]*z[16];
    z[34]=z[38] + z[89];
    z[34]=z[34]*z[44];
    z[36]= - z[38]*z[67];
    z[20]=z[20] + z[36] + z[34] + z[33] - z[57] + z[32];
    z[20]=z[7]*z[20];
    z[32]=z[6] - z[31];
    z[32]=z[32]*z[38];
    z[19]=z[19]*z[31];
    z[33]=z[80]*z[37];
    z[34]= - z[33]*z[54];
    z[19]=z[34] + z[32] + z[19];
    z[17]=z[18] + z[17] + z[25] + 2*z[19] + z[20];
    z[17]=z[1]*z[17];
    z[18]=z[15] + z[87];
    z[18]=z[18]*z[54];
    z[19]=z[23] - 1;
    z[20]=z[19]*z[16];
    z[20]=z[20] + z[18];
    z[20]=z[10]*z[20];
    z[25]=z[16] + z[5];
    z[25]=z[25]*z[54];
    z[32]= - static_cast<T>(1)- z[28];
    z[32]=z[32]*z[16];
    z[32]=z[32] - z[25];
    z[32]=z[10]*z[32];
    z[32]=z[32] + static_cast<T>(5)- z[29];
    z[32]=z[10]*z[32];
    z[34]=z[30] - 5;
    z[36]= - 5*z[26] + z[34];
    z[36]=z[11]*z[36];
    z[32]=z[36] + z[9] + z[32];
    z[32]=z[2]*z[32];
    z[36]=z[46]*z[7];
    z[37]=z[16]*z[36];
    z[20]=z[32] + z[37] - 7*z[26] + z[20] - static_cast<T>(14)+ z[29];
    z[20]=z[2]*z[20];
    z[29]= - z[55]*z[65];
    z[32]= - static_cast<T>(1)- z[44];
    z[37]=z[3]*z[36];
    z[29]=z[37] + 2*z[32] + z[29];
    z[32]=2*z[7];
    z[29]=z[29]*z[32];
    z[31]= - z[9]*z[31];
    z[19]=z[31] - z[19];
    z[19]=z[19]*z[16];
    z[31]=z[79] + z[3];
    z[37]= - z[10]*z[31]*z[87];
    z[38]=10*z[5];
    z[41]= - z[39]*z[38];
    z[48]= - static_cast<T>(3)- 7*z[30];
    z[48]=z[4]*z[48];
    z[19]=z[20] + z[29] + z[41] + z[37] + z[19] + z[49] + z[22] + z[48];
    z[19]=z[2]*z[19];
    z[20]=npow(z[11],3);
    z[22]=z[4]*z[20];
    z[22]= - z[46] + z[22];
    z[22]=z[22]*z[53];
    z[29]=z[71] - 3;
    z[29]=z[29]*z[11];
    z[37]=z[29] - z[21];
    z[37]=z[37]*z[11];
    z[37]=z[37] - z[46];
    z[41]=z[37]*z[69];
    z[22]=z[22] + z[41];
    z[22]=z[7]*z[22];
    z[41]=z[63] - z[29];
    z[41]=z[41]*z[53];
    z[22]=z[22] + z[46] + z[41];
    z[22]=z[7]*z[22];
    z[41]=static_cast<T>(1)- z[26];
    z[41]=z[11]*z[41];
    z[41]= - z[63] + z[41];
    z[41]=z[41]*z[53];
    z[41]=z[46] + z[41];
    z[41]=z[11]*z[41];
    z[48]=z[74]*z[11];
    z[48]=z[48] + z[46];
    z[49]=z[48]*z[52];
    z[41]=z[41] + z[49];
    z[41]=z[2]*z[41];
    z[49]=4*z[26];
    z[52]=z[49] + 3;
    z[52]=z[52]*z[11];
    z[54]= - z[63] - z[52];
    z[54]=z[54]*z[53];
    z[41]=z[41] - z[46] + z[54];
    z[41]=z[2]*z[41];
    z[54]= - z[55]*z[62];
    z[22]=z[41] + z[54] + z[22];
    z[22]=z[8]*z[22];
    z[41]= - z[68]*z[76];
    z[41]=z[78] + z[41];
    z[41]=z[7]*z[41];
    z[41]=z[41] - z[46] - z[76];
    z[41]=z[7]*z[41];
    z[54]=4*z[10];
    z[57]= - static_cast<T>(25)+ 51*z[39];
    z[57]=z[11]*z[57];
    z[41]=z[41] - z[54] + z[57];
    z[41]=z[7]*z[41];
    z[57]=z[10] - z[11];
    z[57]=z[57]*z[65];
    z[57]= - z[46] + z[57];
    z[58]= - z[74]*z[53];
    z[58]= - z[46] + z[58];
    z[58]=z[58]*z[59];
    z[58]=z[78] + z[58];
    z[58]=z[2]*z[58];
    z[57]=4*z[57] + z[58];
    z[57]=z[2]*z[57];
    z[58]=2*z[36];
    z[62]=static_cast<T>(25)+ 51*z[26];
    z[62]=z[11]*z[62];
    z[57]=z[57] + z[58] + 8*z[10] + z[62];
    z[57]=z[2]*z[57];
    z[62]=z[80]*z[21];
    z[63]=z[55]*z[11];
    z[22]=z[22] + z[57] + z[41] + z[62] + 39*z[63];
    z[22]=z[8]*z[22];
    z[41]= - static_cast<T>(3)- z[85];
    z[41]=z[41]*z[45];
    z[57]=static_cast<T>(2)+ z[85];
    z[57]=z[57]*z[87];
    z[41]= - z[83] + z[57] + z[41] + 13*z[6] - 12*z[4];
    z[15]= - z[15] + z[87];
    z[15]=z[10]*z[15];
    z[15]=static_cast<T>(4)+ z[15];
    z[15]=z[10]*z[15];
    z[15]=z[77] + z[15] - z[47];
    z[15]=z[7]*z[15];
    z[47]=z[5] - z[3];
    z[47]=z[10]*z[47];
    z[15]=z[15] + 16*z[47] - 43*z[39];
    z[15]=z[7]*z[15];
    z[47]=static_cast<T>(6)+ z[35];
    z[47]=z[47]*z[46];
    z[57]=5*z[10];
    z[62]= - z[57] + 6*z[73];
    z[62]=z[62]*z[53];
    z[47]=z[47] + z[62];
    z[47]=z[2]*z[47];
    z[62]=static_cast<T>(5)+ z[67];
    z[62]=z[62]*z[65];
    z[60]= - static_cast<T>(16)- z[60];
    z[60]=z[10]*z[60];
    z[47]=z[47] - z[58] + z[60] + z[62];
    z[47]=z[2]*z[47];
    z[36]=z[57] - z[36];
    z[32]=z[36]*z[32];
    z[35]=static_cast<T>(1)+ 8*z[35];
    z[32]=z[47] + z[32] + 2*z[35] - 43*z[26];
    z[32]=z[2]*z[32];
    z[15]=z[22] + z[32] + 2*z[41] + z[15];
    z[15]=z[8]*z[15];
    z[22]= - static_cast<T>(2)+ z[28];
    z[22]=z[22]*z[87];
    z[18]=z[22] + z[18];
    z[18]=z[10]*z[18];
    z[22]= - z[9]*z[87];
    z[22]=static_cast<T>(5)+ z[22];
    z[22]=z[3]*z[22];
    z[22]=z[22] - z[25];
    z[22]=z[10]*z[22];
    z[25]= - z[3]*npow(z[9],2);
    z[25]=2*z[9] + z[25];
    z[25]=z[3]*z[25];
    z[25]= - static_cast<T>(1)+ z[25];
    z[22]=2*z[25] + z[22];
    z[22]=z[10]*z[22];
    z[25]=z[10]*z[4];
    z[25]=z[25] + z[81];
    z[28]= - z[11]*z[25];
    z[22]=z[28] - z[9] + z[22];
    z[22]=z[7]*z[22];
    z[28]=6*z[39];
    z[18]=z[22] + z[28] - static_cast<T>(2)+ z[18];
    z[18]=z[7]*z[18];
    z[22]= - z[38]*z[50];
    z[32]=z[9]*z[12];
    z[35]= - static_cast<T>(1)- 2*z[32];
    z[35]=z[35]*z[87];
    z[31]=z[6] - z[31];
    z[31]=z[31]*z[87];
    z[31]= - 7*z[56] + z[31];
    z[31]=z[10]*z[31];
    z[18]=z[18] + z[31] + z[35] + z[90] + 8*z[12] - z[51] + z[22];
    z[18]=z[7]*z[18];
    z[22]=z[66] + z[55];
    z[22]=z[22]*z[79];
    z[31]=z[12] + z[14];
    z[35]=4*z[31] - z[42];
    z[35]=z[35]*z[16];
    z[33]= - z[33]*z[21];
    z[15]=z[17] + z[15] + z[19] + z[18] - z[40] + z[33] + z[35] + z[56]
    + z[22];
    z[15]=z[1]*z[15];
    z[17]=3*z[26];
    z[18]= - z[17] + z[34];
    z[18]=z[18]*z[53];
    z[19]= - z[49] - static_cast<T>(4)+ z[30];
    z[19]=z[11]*z[19];
    z[19]=z[19] + z[9] + z[54];
    z[19]=z[11]*z[19];
    z[22]= - z[9] - z[21];
    z[22]=z[10]*z[22];
    z[19]=z[22] + z[19];
    z[19]=z[2]*z[19];
    z[22]=7*z[10];
    z[18]=z[19] + z[18] + 3*z[9] + z[22];
    z[18]=z[2]*z[18];
    z[19]= - z[44] - 4;
    z[19]=z[14]*z[19];
    z[19]= - z[3] + z[19];
    z[19]=z[19]*z[21];
    z[33]= - z[14] + 6*z[6];
    z[33]=z[11]*z[33];
    z[18]=z[18] + 4*z[33] + z[19] + z[23] + static_cast<T>(3)- 5*z[30];
    z[18]=z[2]*z[18];
    z[19]=3*z[46];
    z[30]=z[21] + z[11];
    z[30]=z[11]*z[30];
    z[30]=z[19] + z[30];
    z[30]=z[11]*z[30];
    z[30]=z[78] + z[30];
    z[30]=z[11]*z[30];
    z[24]=z[11]*z[24];
    z[24]= - z[10] + z[24];
    z[24]=z[11]*z[24];
    z[24]= - z[46] + z[24];
    z[24]=z[11]*z[24];
    z[24]= - z[78] + z[24];
    z[24]=z[24]*z[69];
    z[24]=z[30] + z[24];
    z[24]=z[7]*z[24];
    z[30]=static_cast<T>(2)- z[81];
    z[30]=z[30]*z[61];
    z[33]=6*z[46];
    z[30]= - z[33] + z[30];
    z[30]=z[11]*z[30];
    z[24]=z[30] + z[24];
    z[24]=z[7]*z[24];
    z[30]= - z[21] + z[11];
    z[30]=z[11]*z[30];
    z[19]=z[19] + z[30];
    z[19]=z[11]*z[19];
    z[19]= - z[78] + z[19];
    z[19]=z[11]*z[19];
    z[30]=z[11]*z[48];
    z[30]= - z[78] + z[30];
    z[30]=z[30]*z[72];
    z[19]=z[19] + z[30];
    z[19]=z[2]*z[19];
    z[17]= - static_cast<T>(2)- z[17];
    z[17]=z[17]*z[61];
    z[17]=z[33] + z[17];
    z[17]=z[11]*z[17];
    z[17]=z[17] + z[19];
    z[17]=z[2]*z[17];
    z[19]= - z[20]*z[75];
    z[17]=z[17] + z[19] + z[24];
    z[17]=z[8]*z[17];
    z[19]= - static_cast<T>(2)+ z[39];
    z[19]=z[11]*z[19];
    z[19]= - z[21] + z[19];
    z[19]=z[19]*z[53];
    z[20]= - z[11]*z[37];
    z[20]=2*z[78] + z[20];
    z[20]=z[7]*z[20];
    z[24]=5*z[46];
    z[19]=z[20] - z[24] + z[19];
    z[19]=z[11]*z[19];
    z[19]= - z[78] + z[19];
    z[19]=z[7]*z[19];
    z[20]= - z[10] + z[29];
    z[20]=z[20]*z[59];
    z[29]=7*z[46];
    z[19]=z[19] + z[29] + z[20];
    z[19]=z[7]*z[19];
    z[20]= - z[11]*z[27];
    z[20]=z[57] + z[20];
    z[20]=z[11]*z[20];
    z[27]= - z[48]*z[53];
    z[27]=z[78] + z[27];
    z[27]=z[2]*z[27];
    z[20]=z[27] - 4*z[46] + z[20];
    z[20]=z[53]*z[20];
    z[20]=z[78] + z[20];
    z[20]=z[2]*z[20];
    z[27]=z[10] + z[52];
    z[27]=z[27]*z[59];
    z[20]=z[20] - z[29] + z[27];
    z[20]=z[2]*z[20];
    z[27]=z[55]*z[61];
    z[17]=z[17] + z[20] + 18*z[27] + z[19];
    z[17]=z[8]*z[17];
    z[19]=static_cast<T>(5)- z[28];
    z[19]=z[11]*z[19];
    z[19]=z[21] + z[19];
    z[19]=z[11]*z[19];
    z[20]=z[11]*z[68];
    z[20]=z[46] + z[20];
    z[20]=z[11]*z[20];
    z[20]= - z[78] + z[20];
    z[20]=z[7]*z[20];
    z[19]=z[20] + 2*z[46] + z[19];
    z[19]=z[7]*z[19];
    z[20]=19*z[11];
    z[27]= - z[64]*z[20];
    z[19]=z[19] + z[22] + z[27];
    z[19]=z[7]*z[19];
    z[26]=static_cast<T>(2)+ z[26];
    z[26]=z[26]*z[53];
    z[26]= - z[57] + z[26];
    z[26]=z[26]*z[65];
    z[27]=z[74]*z[59];
    z[27]=z[24] + z[27];
    z[27]=z[11]*z[27];
    z[27]= - z[78] + z[27];
    z[27]=z[2]*z[27];
    z[24]=z[27] + z[24] + z[26];
    z[24]=z[2]*z[24];
    z[20]= - z[70]*z[20];
    z[20]=z[24] - z[22] + z[20];
    z[20]=z[2]*z[20];
    z[17]=z[17] + z[20] - 22*z[63] + z[19];
    z[17]=z[8]*z[17];
    z[19]=z[4]*z[21];
    z[19]=z[19] + 5*z[39];
    z[19]=z[11]*z[19];
    z[20]= - z[25]*z[61];
    z[20]= - z[46] + z[20];
    z[20]=z[7]*z[20];
    z[19]=z[19] + z[20];
    z[19]=z[7]*z[19];
    z[20]=static_cast<T>(1)+ z[32];
    z[20]=z[20]*z[23];
    z[20]=z[20] - static_cast<T>(3)- 4*z[32];
    z[16]=z[90] + z[16];
    z[16]=z[10]*z[16];
    z[22]=4*z[12] + z[43];
    z[22]=z[11]*z[22];
    z[16]=z[19] + z[22] + 2*z[20] + z[16];
    z[16]=z[7]*z[16];
    z[19]=z[85]*z[45];
    z[20]= - z[32] - z[85];
    z[20]=z[20]*z[87];
    z[22]= - z[5]*z[80];
    z[23]=z[14]*z[87];
    z[22]=z[22] + z[23];
    z[21]=z[22]*z[21];
    z[22]= - z[6]*z[12];
    z[23]=z[4]*z[14];
    z[22]=z[22] + z[23];
    z[22]=z[22]*z[53];
    z[23]= - z[6] - z[31];
    z[22]=z[22] + 2*z[23] + z[4];
    z[22]=z[53]*z[5]*z[22];
    z[23]=2*z[31] - 5*z[6];
    z[15]=z[15] + z[17] + z[18] + z[16] + z[22] + z[21] + z[20] + z[19]
    + 2*z[23] + 7*z[4];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r877(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r877(const std::array<dd_real,31>&);
#endif
