#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r683(const std::array<T,31>& k) {
  T z[146];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[8];
    z[9]=k[15];
    z[10]=k[5];
    z[11]=k[9];
    z[12]=k[2];
    z[13]=k[3];
    z[14]=k[4];
    z[15]=npow(z[1],3);
    z[16]=z[15]*z[10];
    z[17]=npow(z[1],4);
    z[18]=z[16] + z[17];
    z[19]=n<T>(1,2)*z[10];
    z[20]=z[19]*z[15];
    z[21]=z[17] + z[20];
    z[21]=z[10]*z[21];
    z[22]=npow(z[1],5);
    z[21]=n<T>(1,2)*z[22] + z[21];
    z[21]=z[5]*z[21];
    z[21]= - n<T>(1,2)*z[18] + z[21];
    z[23]=n<T>(5,2)*z[6];
    z[21]=z[21]*z[23];
    z[24]=3*z[17];
    z[25]=z[24] + n<T>(7,2)*z[16];
    z[25]=z[5]*z[25];
    z[21]=z[21] - n<T>(11,4)*z[15] + z[25];
    z[21]=z[6]*z[21];
    z[25]=z[10]*z[1];
    z[26]=npow(z[1],2);
    z[27]=z[25] + z[26];
    z[28]=npow(z[10],2);
    z[29]=z[27]*z[28];
    z[30]=n<T>(1,2)*z[25];
    z[31]=z[30] + z[26];
    z[31]=z[31]*z[10];
    z[32]=n<T>(1,2)*z[15];
    z[31]=z[31] + z[32];
    z[33]=z[28]*z[8];
    z[34]=z[31]*z[33];
    z[34]=z[29] - n<T>(1,2)*z[34];
    z[35]=5*z[8];
    z[34]=z[34]*z[35];
    z[36]=15*z[25];
    z[37]=z[36] - z[26];
    z[38]=n<T>(1,4)*z[10];
    z[37]=z[37]*z[38];
    z[34]= - z[37] + z[34];
    z[34]=z[4]*z[34];
    z[39]=n<T>(1,4)*z[4] - z[11];
    z[39]=z[16]*z[39];
    z[40]=z[15]*z[6];
    z[41]=z[19]*z[40];
    z[39]=z[41] + z[39];
    z[39]=z[2]*z[39];
    z[41]=z[27]*z[33];
    z[42]=z[28]*z[1];
    z[41]= - 3*z[42] + z[41];
    z[41]=z[8]*z[41];
    z[41]=z[25] + z[41];
    z[43]=z[15]*z[11];
    z[44]=2*z[26];
    z[45]=z[43] - z[44];
    z[46]=z[22]*z[5];
    z[47]=z[46] - z[17];
    z[48]= - z[6]*z[47];
    z[49]=z[17]*z[5];
    z[48]= - z[49] + z[48];
    z[50]=n<T>(1,2)*z[3];
    z[48]=z[48]*z[50];
    z[51]=z[15]*z[5];
    z[21]=z[39] + z[48] + z[34] + z[21] + n<T>(9,4)*z[51] + z[45] + n<T>(5,4)*
    z[41];
    z[21]=z[7]*z[21];
    z[34]=7*z[15];
    z[39]=z[26]*z[10];
    z[41]= - z[11]*z[16];
    z[41]=z[41] + z[34] - z[39];
    z[41]=z[11]*z[41];
    z[48]=z[22]*z[6];
    z[52]=z[48] + z[17];
    z[52]=z[52]*z[4];
    z[53]=z[15]*z[9];
    z[54]=3*z[10];
    z[55]=z[54]*z[53];
    z[56]=z[17]*z[6];
    z[55]= - z[52] + z[56] + z[55];
    z[55]=z[2]*z[55];
    z[41]=z[41] + z[55];
    z[55]=npow(z[1],6);
    z[57]=npow(z[6],2);
    z[58]=z[55]*z[57];
    z[58]= - n<T>(13,4)*z[17] + z[58];
    z[58]=z[6]*z[58];
    z[59]=5*z[15];
    z[52]=z[52] + z[58] - z[59] - n<T>(1,4)*z[39];
    z[52]=z[4]*z[52];
    z[58]=z[17]*z[10];
    z[60]= - z[22] + z[58];
    z[60]=z[6]*z[60];
    z[60]=z[60] + z[17] - z[16];
    z[60]=z[6]*z[60];
    z[61]=5*z[39];
    z[62]=17*z[15] - z[61];
    z[60]=n<T>(1,4)*z[62] + z[60];
    z[60]=z[6]*z[60];
    z[62]=z[16]*z[9];
    z[63]= - 19*z[62] + 29*z[15] + 13*z[39];
    z[63]=z[9]*z[63];
    z[41]=z[52] + n<T>(1,4)*z[63] - n<T>(21,2)*z[26] + z[60] + n<T>(1,2)*z[41];
    z[41]=z[2]*z[41];
    z[52]=z[51]*z[10];
    z[60]= - 9*z[52] - 16*z[15] + n<T>(9,4)*z[39];
    z[60]=z[5]*z[60];
    z[63]=2*z[8];
    z[64]=z[63]*z[28];
    z[65]=11*z[15];
    z[66]=12*z[39];
    z[67]=z[65] + z[66];
    z[67]=z[67]*z[64];
    z[68]= - z[34] - 15*z[39];
    z[68]=z[10]*z[68];
    z[67]=z[68] + z[67];
    z[67]=z[8]*z[67];
    z[68]=n<T>(53,2)*z[15] + 29*z[39];
    z[67]=n<T>(1,2)*z[68] + z[67];
    z[67]=z[8]*z[67];
    z[68]= - z[15]*z[28];
    z[69]=npow(z[10],3);
    z[70]=z[15]*z[8];
    z[71]=z[69]*z[70];
    z[68]=z[68] - 4*z[71];
    z[71]=3*z[8];
    z[68]=z[68]*z[71];
    z[68]= - n<T>(17,4)*z[16] + z[68];
    z[68]=z[8]*z[68];
    z[72]=3*z[51];
    z[73]= - z[28]*z[72];
    z[73]= - n<T>(13,2)*z[16] + z[73];
    z[74]=n<T>(1,2)*z[5];
    z[73]=z[73]*z[74];
    z[68]=z[73] + z[68];
    z[68]=z[9]*z[68];
    z[60]=z[68] + z[60] + z[67];
    z[60]=z[9]*z[60];
    z[67]=13*z[25];
    z[68]=n<T>(67,4)*z[26] + z[67];
    z[68]=z[10]*z[68];
    z[68]=n<T>(15,4)*z[15] + z[68];
    z[68]=z[10]*z[68];
    z[73]=z[44] + z[25];
    z[75]=z[73]*z[10];
    z[76]=z[75] + z[15];
    z[77]=z[76]*z[33];
    z[68]=z[68] - 8*z[77];
    z[68]=z[68]*z[71];
    z[77]=n<T>(31,2)*z[26];
    z[36]= - z[77] - z[36];
    z[78]=5*z[10];
    z[36]=z[36]*z[78];
    z[36]= - n<T>(31,2)*z[15] + z[36];
    z[36]=n<T>(1,2)*z[36] + z[68];
    z[36]=z[8]*z[36];
    z[68]=z[56] + z[15];
    z[79]=z[68]*z[4];
    z[80]=11*z[26];
    z[81]=z[80] + 39*z[25];
    z[82]=z[22]*z[57];
    z[83]=4*z[15];
    z[82]= - z[83] + z[82];
    z[82]=z[6]*z[82];
    z[36]=z[79] + z[36] + n<T>(1,2)*z[81] + z[82];
    z[36]=z[4]*z[36];
    z[81]=3*z[15];
    z[82]= - z[81] - n<T>(23,2)*z[39];
    z[84]=n<T>(5,2)*z[17] + z[16];
    z[84]=z[5]*z[84];
    z[82]=n<T>(1,2)*z[82] + z[84];
    z[82]=z[5]*z[82];
    z[84]=z[39] + z[15];
    z[85]=z[5]*z[10];
    z[86]=z[84]*z[85];
    z[87]= - 5*z[86] - z[32] + z[61];
    z[87]=n<T>(1,2)*z[87] - z[56];
    z[87]=z[6]*z[87];
    z[88]=n<T>(21,4)*z[26];
    z[82]=z[87] - z[88] + z[82];
    z[82]=z[6]*z[82];
    z[87]=35*z[26];
    z[89]=5*z[51];
    z[90]=z[89] - z[87] + 79*z[25];
    z[91]=6*z[25];
    z[92]=z[26] + z[91];
    z[92]=z[92]*z[64];
    z[93]= - 17*z[26] - 81*z[25];
    z[93]=z[93]*z[38];
    z[92]=z[93] + z[92];
    z[92]=z[8]*z[92];
    z[90]=n<T>(1,4)*z[90] + z[92];
    z[90]=z[8]*z[90];
    z[92]=z[53]*z[28];
    z[93]=z[54]*z[26];
    z[94]=8*z[15];
    z[95]= - z[94] + z[93];
    z[95]=z[10]*z[95];
    z[95]=z[95] - z[92];
    z[96]=3*z[9];
    z[95]=z[95]*z[96];
    z[97]=z[26]*z[28];
    z[97]=z[97] - z[92];
    z[98]=4*z[11];
    z[97]=z[97]*z[98];
    z[99]=12*z[26];
    z[100]=z[99] - z[25];
    z[101]=2*z[10];
    z[100]=z[100]*z[101];
    z[95]=z[97] + z[95] + z[32] + z[100];
    z[95]=z[11]*z[95];
    z[62]=n<T>(11,4)*z[62] + z[65] + n<T>(19,2)*z[39];
    z[62]=z[9]*z[62];
    z[65]=9*z[26];
    z[97]= - z[65] + z[30];
    z[62]=z[95] + n<T>(3,2)*z[97] + z[62];
    z[62]=z[11]*z[62];
    z[95]=n<T>(1,2)*z[49];
    z[97]= - z[59] + z[95];
    z[100]=3*z[5];
    z[97]=z[97]*z[100];
    z[102]=z[17] + 3*z[46];
    z[102]=z[5]*z[102];
    z[102]=21*z[15] + z[102];
    z[103]=n<T>(1,2)*z[6];
    z[102]=z[102]*z[103];
    z[97]=n<T>(35,4)*z[43] + z[102] + n<T>(53,4)*z[26] + z[97];
    z[97]=z[3]*z[97];
    z[102]=z[26] - n<T>(13,2)*z[51];
    z[102]=z[5]*z[102];
    z[21]=z[21] + z[41] + z[97] + z[36] + z[62] + z[60] + z[90] + z[82]
    - n<T>(51,4)*z[1] + z[102];
    z[21]=z[7]*z[21];
    z[36]= - z[4] + z[6];
    z[41]=npow(z[5],2);
    z[36]=z[41]*z[44]*z[36];
    z[60]=n<T>(3,2)*z[15];
    z[62]=npow(z[8],2);
    z[82]=z[57]*z[62]*z[60];
    z[90]=z[15]*z[4];
    z[97]= - z[90] + z[40];
    z[97]=z[41]*z[97];
    z[102]=npow(z[9],2);
    z[104]= - z[102]*z[44];
    z[97]=z[104] + z[97];
    z[97]=z[3]*z[97];
    z[104]=z[26]*z[9];
    z[105]= - 167*z[1] + 70*z[104];
    z[105]=z[105]*z[102];
    z[36]=z[97] + z[105] + z[82] + z[36];
    z[36]=z[3]*z[36];
    z[82]=z[76]*z[6];
    z[97]= - z[82] + z[27];
    z[105]=z[8]*z[6];
    z[97]=z[97]*z[105];
    z[106]=z[57]*z[26];
    z[107]=n<T>(1,2)*z[106];
    z[97]=z[107] + z[97];
    z[97]=z[97]*z[62];
    z[108]=z[44] - z[51];
    z[108]=z[108]*z[5];
    z[109]= - z[44] - z[53];
    z[109]=z[9]*z[109];
    z[109]=z[108] + z[109];
    z[109]=z[3]*z[109];
    z[110]= - 93*z[26] + 20*z[53];
    z[110]=z[9]*z[110];
    z[110]=85*z[1] + z[110];
    z[110]=z[9]*z[110];
    z[109]=z[110] + z[109];
    z[109]=z[3]*z[109];
    z[110]=14*z[26];
    z[111]=5*z[25];
    z[112]= - z[110] + z[111];
    z[112]=z[9]*z[112];
    z[112]=114*z[1] + 7*z[112];
    z[112]=z[112]*z[102];
    z[109]=z[112] + z[109];
    z[109]=z[2]*z[109];
    z[108]=z[1] - z[108];
    z[112]=npow(z[3],2);
    z[108]=z[108]*z[112];
    z[113]=n<T>(1,2)*z[40];
    z[114]=z[113] + z[26];
    z[114]=z[114]*z[6];
    z[115]=n<T>(1,2)*z[1];
    z[116]= - z[115] - z[114];
    z[116]=z[7]*z[4]*z[116];
    z[108]=z[108] + n<T>(5,2)*z[116];
    z[108]=z[7]*z[108];
    z[116]=z[26]*z[3];
    z[104]=z[116] + 31*z[1] - 20*z[104];
    z[117]=2*z[3];
    z[104]=z[117]*z[102]*z[104];
    z[118]=z[3]*z[1];
    z[119]=z[118]*z[102];
    z[120]=npow(z[9],3);
    z[121]=z[120]*z[1];
    z[122]=20*z[121] - z[119];
    z[123]=z[14]*z[3];
    z[122]=z[122]*z[123];
    z[104]=z[122] + 49*z[121] + z[104];
    z[104]=z[2]*z[104];
    z[122]= - 35*z[121] + z[119];
    z[122]=z[122]*z[117];
    z[104]=z[122] + z[104];
    z[104]=z[14]*z[104];
    z[122]=z[27]*npow(z[8],3);
    z[124]=z[41]*z[1];
    z[122]= - z[124] - 3*z[122];
    z[122]=z[4]*z[122];
    z[124]=z[6]*z[124];
    z[36]=z[104] + z[108] + z[109] + z[36] + z[122] - 98*z[121] + z[124]
    + 3*z[97];
    z[36]=z[14]*z[36];
    z[97]=n<T>(15,2)*z[15] - z[49];
    z[97]=z[5]*z[97];
    z[97]= - n<T>(35,8)*z[26] + z[97];
    z[97]=z[5]*z[97];
    z[104]=5*z[26];
    z[108]=z[104] - n<T>(11,2)*z[70];
    z[108]=z[8]*z[108];
    z[109]= - z[51] - 3*z[70];
    z[109]=z[4]*z[109];
    z[97]=n<T>(1,2)*z[109] + z[97] + z[108];
    z[97]=z[4]*z[97];
    z[108]=4*z[26];
    z[109]= - z[108] - n<T>(7,2)*z[40];
    z[109]=z[6]*z[109];
    z[122]=z[40]*z[35];
    z[109]=z[109] + z[122];
    z[109]=z[8]*z[109];
    z[122]=z[4] + z[6];
    z[122]=z[51]*z[122];
    z[124]=3*z[26];
    z[125]=z[124] + z[40];
    z[125]=z[9]*z[125];
    z[126]=2*z[1];
    z[122]=z[125] - z[126] + z[122];
    z[122]=z[3]*z[122];
    z[125]=2*z[5];
    z[127]=z[26]*z[5];
    z[128]=z[1] - z[127];
    z[128]=z[128]*z[125];
    z[72]=n<T>(1,4)*z[26] + z[72];
    z[72]=z[72]*z[100];
    z[129]=z[6]*z[51];
    z[72]=z[72] - n<T>(7,4)*z[129];
    z[72]=z[72]*z[103];
    z[129]=213*z[26] + z[40];
    z[130]=n<T>(1,2)*z[9];
    z[129]=z[129]*z[130];
    z[131]=z[26]*z[6];
    z[132]= - n<T>(93,2)*z[1] - z[131];
    z[129]=3*z[132] + z[129];
    z[129]=z[9]*z[129];
    z[72]=z[122] + z[97] + z[129] + z[109] + z[128] + z[72];
    z[72]=z[3]*z[72];
    z[97]= - z[126]*z[28]*z[9];
    z[97]=z[97] + z[113] + n<T>(1,2)*z[26] + z[25];
    z[97]=z[9]*z[97];
    z[109]=z[6]*z[108];
    z[97]=z[109] + z[97];
    z[97]=z[9]*z[97];
    z[109]=2*z[25];
    z[113]=n<T>(9,2)*z[26] + z[109];
    z[122]=2*z[15];
    z[128]= - z[122] - z[39];
    z[128]=z[5]*z[128];
    z[113]=3*z[113] + z[128];
    z[113]=z[5]*z[113];
    z[128]=n<T>(3,2)*z[1];
    z[113]=z[128] + z[113];
    z[113]=z[5]*z[113];
    z[129]=11*z[25];
    z[132]=z[129] + 19*z[26];
    z[132]=z[132]*z[10];
    z[94]=z[132] + z[94];
    z[132]= - z[94]*z[63];
    z[132]=z[132] + z[124] + n<T>(23,2)*z[25];
    z[132]=z[8]*z[132];
    z[133]=4*z[1];
    z[132]= - z[133] + z[132];
    z[132]=z[8]*z[132];
    z[134]=z[26] + z[40];
    z[135]=n<T>(5,2)*z[9];
    z[134]=z[134]*z[135];
    z[136]=n<T>(3,2)*z[26];
    z[137]= - z[8]*z[136];
    z[134]=z[134] - z[127] + z[137];
    z[134]=z[4]*z[134];
    z[137]= - z[127]*z[103];
    z[97]=z[134] + z[97] + z[132] + z[113] + z[137];
    z[97]=z[4]*z[97];
    z[113]=2*z[53] - z[26] - z[51];
    z[113]=z[3]*z[113];
    z[132]= - z[60] + z[49];
    z[132]=z[5]*z[132];
    z[134]=n<T>(7,2)*z[26];
    z[132]= - z[134] + z[132];
    z[132]=z[5]*z[132];
    z[137]= - 78*z[26] + 31*z[53];
    z[137]=z[9]*z[137];
    z[113]=z[113] + z[137] + 46*z[1] + z[132];
    z[113]=z[3]*z[113];
    z[33]=z[33]*z[126];
    z[89]=z[89] - z[104];
    z[132]=23*z[25];
    z[137]=z[132] - z[89];
    z[137]=n<T>(1,4)*z[137] - z[33];
    z[137]=z[8]*z[137];
    z[138]=5*z[1];
    z[139]=3*z[127];
    z[140]= - z[138] - z[139];
    z[137]=n<T>(3,4)*z[140] + z[137];
    z[137]=z[8]*z[137];
    z[140]=3*z[25];
    z[87]= - z[87] + z[140];
    z[87]=z[87]*z[101];
    z[87]=49*z[15] + z[87];
    z[87]=z[9]*z[87];
    z[87]=z[87] - n<T>(679,4)*z[26] + 52*z[25];
    z[87]=z[9]*z[87];
    z[87]=n<T>(189,2)*z[1] + z[87];
    z[87]=z[9]*z[87];
    z[141]=z[51] - z[26];
    z[142]=z[141]*z[35];
    z[143]=z[9]*z[124];
    z[142]=z[142] + z[143];
    z[95]= - z[15] - z[95];
    z[95]=z[5]*z[95];
    z[95]=z[136] + z[95];
    z[95]=z[3]*z[95];
    z[95]=n<T>(1,2)*z[142] + z[95];
    z[95]=z[2]*z[95];
    z[87]=z[95] + z[113] + z[137] + z[87];
    z[87]=z[2]*z[87];
    z[95]=10*z[26];
    z[113]= - z[95] + n<T>(9,2)*z[51];
    z[113]=z[5]*z[113];
    z[136]= - z[26] + 4*z[40];
    z[136]=z[6]*z[136];
    z[137]=z[3]*z[141];
    z[113]= - 3*z[137] + z[136] + z[128] + z[113];
    z[113]=z[3]*z[113];
    z[136]=z[8]*z[42];
    z[136]=n<T>(5,2)*z[136] - z[111] - z[141];
    z[136]=z[8]*z[136];
    z[137]=n<T>(5,2)*z[15];
    z[57]=z[57]*z[137];
    z[57]=z[57] + z[136];
    z[136]=2*z[40] + z[44] - n<T>(5,2)*z[25];
    z[136]=z[4]*z[136];
    z[142]=n<T>(3,2)*z[141] - z[40];
    z[50]=z[142]*z[50];
    z[50]=z[50] + n<T>(1,2)*z[57] + z[136];
    z[50]=z[7]*z[50];
    z[57]= - z[132] - z[89];
    z[33]=n<T>(1,4)*z[57] + z[33];
    z[33]=z[8]*z[33];
    z[57]=15*z[1] + 7*z[127];
    z[33]=n<T>(1,4)*z[57] + z[33];
    z[33]=z[8]*z[33];
    z[57]=n<T>(7,4)*z[15] + z[56];
    z[57]=z[6]*z[57];
    z[57]=n<T>(13,4)*z[26] + z[57];
    z[57]=z[6]*z[57];
    z[89]=z[56] - z[15];
    z[136]= - z[89]*z[103];
    z[136]=z[26] + z[136];
    z[136]=z[4]*z[136];
    z[142]=n<T>(11,2)*z[1];
    z[57]=z[136] + z[142] + z[57];
    z[57]=z[4]*z[57];
    z[33]=z[50] + z[113] + z[57] - n<T>(5,2)*z[106] + z[33];
    z[33]=z[7]*z[33];
    z[50]=z[42] - z[15];
    z[57]=3*z[6];
    z[106]=z[50]*z[57];
    z[113]=13*z[26];
    z[132]= - z[113] - z[132];
    z[132]=n<T>(1,2)*z[132] - z[106];
    z[132]=z[6]*z[132];
    z[136]=z[54]*z[82];
    z[94]=2*z[94] + z[136];
    z[94]=z[94]*z[105];
    z[94]=z[132] + z[94];
    z[94]=z[8]*z[94];
    z[132]= - z[6]*z[44];
    z[132]=z[132] + z[133] + n<T>(3,2)*z[127];
    z[132]=z[6]*z[132];
    z[94]=z[132] + z[94];
    z[94]=z[8]*z[94];
    z[132]=n<T>(5,2)*z[26];
    z[136]=z[132] + z[25];
    z[136]=z[136]*z[100];
    z[143]=n<T>(7,8)*z[26];
    z[144]=z[143] + z[25];
    z[145]= - z[6]*z[144];
    z[136]=z[145] - n<T>(5,2)*z[1] + z[136];
    z[136]=z[6]*z[5]*z[136];
    z[145]=98*z[26] - z[67];
    z[145]=z[9]*z[145];
    z[145]= - 140*z[1] + z[145];
    z[145]=z[145]*z[102];
    z[33]=z[36] + z[33] + z[87] + z[72] + z[97] + z[145] + z[136] + 
    z[94];
    z[33]=z[14]*z[33];
    z[36]=z[64]*z[15];
    z[64]= - z[20] + z[36];
    z[64]=z[8]*z[64];
    z[72]=2*z[92];
    z[20]=z[20] + z[72];
    z[20]=z[9]*z[20];
    z[87]=z[19]*z[51];
    z[20]=z[20] + z[87] + z[64];
    z[20]=z[11]*z[20];
    z[64]=z[26]*z[19];
    z[52]=z[52] + z[83] + z[64];
    z[52]=z[5]*z[52];
    z[64]=6*z[39];
    z[87]= - z[59] - z[64];
    z[87]=z[10]*z[87];
    z[36]=z[87] - z[36];
    z[36]=z[8]*z[36];
    z[36]=z[36] - z[122] + 7*z[39];
    z[36]=z[8]*z[36];
    z[87]=2*z[39];
    z[94]=z[34] - z[87];
    z[94]=z[10]*z[94];
    z[94]=z[94] + z[72];
    z[94]=z[9]*z[94];
    z[60]=z[94] + z[60] - z[61];
    z[60]=z[60]*z[96];
    z[20]=z[20] + z[60] + z[36] - 8*z[26] + z[52];
    z[20]=z[11]*z[20];
    z[36]= - z[80] + z[67];
    z[36]=z[10]*z[36];
    z[36]=5*z[49] - z[59] + z[36];
    z[37]= - z[122] - z[37];
    z[37]=z[10]*z[37];
    z[52]= - z[28]*z[106];
    z[37]=z[37] + z[52];
    z[37]=z[6]*z[37];
    z[52]=2*z[27] + z[82];
    z[52]=z[57]*z[69]*z[52];
    z[60]=z[108] + z[140];
    z[60]=z[60]*z[28];
    z[52]=z[60] + z[52];
    z[52]=z[8]*z[52];
    z[36]=z[52] + n<T>(1,4)*z[36] + z[37];
    z[36]=z[8]*z[36];
    z[37]=7*z[26];
    z[52]= - z[37] - n<T>(29,4)*z[25];
    z[52]=z[10]*z[52];
    z[60]=z[54]*z[40];
    z[52]=z[60] + z[137] + z[52];
    z[52]=z[6]*z[52];
    z[36]=z[36] + z[52] - n<T>(49,4)*z[51] + z[110] - n<T>(67,4)*z[25];
    z[36]=z[8]*z[36];
    z[52]=z[49] - z[15];
    z[60]=z[47]*z[4];
    z[61]=z[52] - z[60];
    z[61]=z[61]*z[3];
    z[82]= - z[5]*z[55];
    z[82]=z[22] + z[82];
    z[82]=z[5]*z[82];
    z[82]=n<T>(27,8)*z[17] + z[82];
    z[82]=z[5]*z[82];
    z[60]= - z[60] - n<T>(55,8)*z[15] + z[82];
    z[60]=z[4]*z[60];
    z[82]=z[47]*z[41];
    z[94]=n<T>(15,2)*z[26];
    z[53]=z[61] + z[60] + 9*z[53] - z[94] + z[82];
    z[53]=z[3]*z[53];
    z[59]=z[59] - 3*z[49];
    z[59]=z[5]*z[59];
    z[60]= - z[15] + z[93];
    z[60]=z[6]*z[60];
    z[35]= - z[52]*z[35];
    z[35]=z[35] + z[60] - z[124] + z[59];
    z[52]=z[22] + z[58];
    z[52]=z[5]*z[52];
    z[52]=z[52] - n<T>(1,2)*z[17] - z[16];
    z[52]=z[5]*z[52];
    z[59]=n<T>(1,2)*z[56];
    z[52]=z[59] + z[15] + z[52];
    z[52]=z[4]*z[52];
    z[35]= - n<T>(3,2)*z[61] + n<T>(1,2)*z[35] + z[52];
    z[35]=z[2]*z[35];
    z[52]=z[39] - z[15];
    z[60]=z[6] + z[5];
    z[60]=z[52]*z[60];
    z[61]=7*z[25];
    z[82]= - n<T>(71,2)*z[26] + z[61];
    z[60]=5*z[82] + z[60];
    z[82]=35*z[15] - z[66];
    z[82]=z[9]*z[82];
    z[96]=4*z[25];
    z[82]=z[82] - 73*z[26] + z[96];
    z[82]=z[10]*z[82];
    z[82]=51*z[15] + z[82];
    z[82]=z[9]*z[82];
    z[60]=n<T>(1,2)*z[60] + z[82];
    z[60]=z[9]*z[60];
    z[82]=7*z[17] + 5*z[16];
    z[58]= - 2*z[22] - z[58];
    z[58]=z[5]*z[58];
    z[58]=n<T>(1,2)*z[82] + z[58];
    z[58]=z[5]*z[58];
    z[82]=n<T>(23,2)*z[15] - z[93];
    z[58]=n<T>(1,2)*z[82] + z[58];
    z[58]=z[5]*z[58];
    z[82]=2*z[17];
    z[48]=z[82] - z[48];
    z[48]=z[6]*z[48];
    z[48]=n<T>(69,8)*z[15] + z[48];
    z[48]=z[6]*z[48];
    z[97]= - z[49] - z[56];
    z[97]=z[4]*z[97];
    z[48]=z[97] + z[48] - z[88] + z[58];
    z[48]=z[4]*z[48];
    z[58]= - n<T>(31,8)*z[26] - z[25];
    z[58]=z[10]*z[58];
    z[88]= - z[10]*z[84];
    z[88]=z[17] + z[88];
    z[88]=z[6]*z[88];
    z[58]=z[88] - z[81] + z[58];
    z[58]=z[6]*z[58];
    z[88]=14*z[25];
    z[58]=z[58] - n<T>(9,8)*z[26] + z[88];
    z[58]=z[6]*z[58];
    z[97]=2*z[49];
    z[106]= - z[32] + z[97];
    z[106]=z[5]*z[106];
    z[106]= - z[26] + z[106];
    z[106]=z[5]*z[106];
    z[20]=z[35] + z[53] + z[48] + z[20] + z[60] + z[36] + z[58] + 43*
    z[1] + z[106];
    z[20]=z[2]*z[20];
    z[35]=z[28]*z[5];
    z[36]=z[35]*z[76];
    z[48]=z[80] + z[61];
    z[48]=z[10]*z[48];
    z[48]=z[83] + z[48];
    z[48]=z[48]*z[78];
    z[48]=z[48] - 13*z[36];
    z[48]=z[5]*z[48];
    z[53]=z[100]*z[69];
    z[58]= - z[76]*z[53];
    z[60]=z[37] + z[111];
    z[60]=z[10]*z[60];
    z[60]=z[122] + z[60];
    z[60]=z[60]*z[28];
    z[58]=2*z[60] + z[58];
    z[58]=z[5]*z[58];
    z[60]=z[108] - z[61];
    z[60]=z[60]*z[28];
    z[58]=z[60] + z[58];
    z[58]=z[9]*z[58];
    z[60]=z[44] - z[129];
    z[60]=z[60]*z[101];
    z[32]=z[58] - z[59] + z[48] - z[32] + z[60];
    z[32]=z[9]*z[32];
    z[48]=z[85]*z[76];
    z[58]=n<T>(149,2)*z[26] + 48*z[25];
    z[58]=z[10]*z[58];
    z[58]= - 22*z[48] + n<T>(81,4)*z[15] + z[58];
    z[58]=z[5]*z[58];
    z[60]=25*z[26] - 63*z[25];
    z[32]=z[32] + 11*z[40] + n<T>(1,2)*z[60] + z[58];
    z[32]=z[9]*z[32];
    z[58]=z[82] + z[16];
    z[60]=z[58]*z[10];
    z[60]=z[60] + z[22];
    z[78]=z[60]*z[63];
    z[100]=z[81] + z[87];
    z[100]=z[10]*z[100];
    z[100]=z[17] + z[100];
    z[100]=3*z[100] + z[78];
    z[100]=z[8]*z[100];
    z[100]=z[100] - z[34] - 11*z[39];
    z[100]=z[8]*z[100];
    z[64]= - 25*z[15] + z[64];
    z[64]=z[10]*z[64];
    z[64]=z[64] - 6*z[92];
    z[64]=z[9]*z[64];
    z[64]=z[64] - 26*z[15] + 19*z[39];
    z[64]=z[9]*z[64];
    z[78]=n<T>(9,2)*z[18] - z[78];
    z[78]=z[8]*z[78];
    z[72]= - n<T>(9,2)*z[16] - z[72];
    z[72]=z[9]*z[72];
    z[34]=z[72] - z[34] + z[78];
    z[34]=z[11]*z[34];
    z[72]=z[99] + z[40];
    z[34]=z[34] + z[64] + 2*z[72] + z[100];
    z[34]=z[11]*z[34];
    z[59]=z[122] + z[59];
    z[59]=z[6]*z[59];
    z[64]= - z[68]*z[135];
    z[59]=z[64] + n<T>(3,2)*z[70] + z[59] - z[141];
    z[59]=z[4]*z[59];
    z[64]= - z[94] - z[96];
    z[64]=z[64]*z[54];
    z[58]= - z[5]*z[58];
    z[58]=z[58] - n<T>(19,2)*z[15] + z[64];
    z[58]=z[5]*z[58];
    z[64]=n<T>(73,2)*z[26] + 27*z[25];
    z[58]=n<T>(1,2)*z[64] + z[58];
    z[58]=z[5]*z[58];
    z[64]= - z[65] - z[51];
    z[68]= - z[6]*z[89];
    z[64]=n<T>(1,2)*z[64] + z[68];
    z[64]=z[6]*z[64];
    z[68]= - 47*z[26] + 49*z[25];
    z[68]=z[68]*z[19];
    z[68]= - 31*z[15] + z[68];
    z[72]= - z[37] - z[91];
    z[72]=z[10]*z[72];
    z[78]=13*z[15];
    z[72]=z[78] + 4*z[72];
    z[72]=z[10]*z[72];
    z[89]=17*z[17];
    z[72]=z[89] + z[72];
    z[72]=z[8]*z[72];
    z[68]=n<T>(1,2)*z[68] + z[72];
    z[68]=z[8]*z[68];
    z[72]=31*z[26];
    z[68]=z[68] - z[72] - n<T>(39,2)*z[25];
    z[68]=z[8]*z[68];
    z[32]=z[59] + z[34] + z[32] + z[68] + z[64] - 10*z[1] + z[58];
    z[32]=z[4]*z[32];
    z[34]= - z[72] + z[61];
    z[58]=z[42]*z[98];
    z[34]=n<T>(3,4)*z[34] + z[58];
    z[34]=z[11]*z[34];
    z[58]=n<T>(5,4)*z[1];
    z[34]= - z[58] + z[34];
    z[34]=z[11]*z[34];
    z[59]=n<T>(35,2)*z[26] - 9*z[43];
    z[59]=z[11]*z[59];
    z[59]= - z[116] + z[115] + z[59];
    z[59]=z[3]*z[59];
    z[64]=z[26]*z[11];
    z[58]= - n<T>(1,4)*z[116] - z[58] - z[64];
    z[58]=z[7]*z[58];
    z[68]=z[14]*z[112]*z[133];
    z[34]=z[68] + z[58] + z[34] + z[59];
    z[34]=z[7]*z[34];
    z[58]=npow(z[11],2);
    z[59]=z[118]*z[58];
    z[68]=npow(z[11],3);
    z[72]=z[133]*z[68];
    z[59]=z[59] - z[72];
    z[72]=z[12]*z[59];
    z[91]=z[64] - z[126];
    z[91]=z[91]*z[117]*z[11];
    z[92]=8*z[64];
    z[100]= - z[128] - z[92];
    z[100]=z[100]*z[58];
    z[72]=z[72] + z[100] + z[91];
    z[72]=z[3]*z[72];
    z[100]=z[13]*z[7];
    z[100]=z[100] - 1;
    z[59]=z[59]*z[3]*z[100];
    z[92]=z[115] + z[92];
    z[92]=z[92]*z[58];
    z[91]=z[92] - z[91];
    z[91]=z[3]*z[91];
    z[92]=z[68]*z[1];
    z[91]=12*z[92] + z[91];
    z[91]=z[7]*z[91];
    z[59]=z[91] + z[59];
    z[59]=z[13]*z[59];
    z[91]=n<T>(17,2)*z[26];
    z[100]=4*z[43];
    z[101]=z[91] - z[100];
    z[101]=z[11]*z[101];
    z[106]=n<T>(7,2)*z[1];
    z[101]= - z[106] + z[101];
    z[101]=z[11]*z[101];
    z[110]= - z[108] + z[43];
    z[110]=z[11]*z[110];
    z[110]=6*z[1] + z[110];
    z[110]=z[3]*z[110];
    z[101]=z[101] + z[110];
    z[101]=z[3]*z[101];
    z[110]=z[11]*z[27];
    z[110]= - n<T>(19,4)*z[1] - 12*z[110];
    z[110]=z[110]*z[58];
    z[101]=z[110] + z[101];
    z[101]=z[7]*z[101];
    z[59]=z[59] + z[101] - 8*z[92] + z[72];
    z[59]=z[13]*z[59];
    z[72]=z[6]*z[42];
    z[72]= - n<T>(5,4)*z[72] + z[26] - z[140];
    z[72]=z[6]*z[72];
    z[92]=z[27]*z[98];
    z[72]=z[92] + z[128] + z[72];
    z[72]=z[11]*z[72];
    z[92]= - z[11]*z[40];
    z[92]=z[114] + z[92];
    z[92]=z[4]*z[92];
    z[72]=z[92] - z[107] + z[72];
    z[72]=z[11]*z[72];
    z[92]=z[132] - z[43];
    z[92]=z[11]*z[92];
    z[101]=n<T>(3,2)*z[43];
    z[107]=z[4]*z[101];
    z[92]=z[107] - z[138] + z[92];
    z[92]=z[3]*z[92];
    z[94]= - z[94] + z[100];
    z[94]=z[94]*z[58];
    z[100]= - z[104] + z[43];
    z[100]=z[4]*z[11]*z[100];
    z[92]=z[92] + z[94] + z[100];
    z[92]=z[3]*z[92];
    z[52]=z[6]*z[52];
    z[52]=z[52] + z[144];
    z[52]=z[6]*z[52];
    z[56]=n<T>(9,8)*z[15] + z[56];
    z[56]=z[6]*z[56];
    z[56]= - z[79] + z[143] + z[56];
    z[56]=z[4]*z[56];
    z[52]=z[56] - z[106] + z[52];
    z[52]=z[6]*z[52];
    z[56]=z[90] - z[26];
    z[56]=z[56]*z[3];
    z[79]=z[4]*z[40];
    z[79]= - z[56] - z[131] + z[79];
    z[79]=z[2]*z[79];
    z[90]=z[4]*z[26];
    z[90]= - 29*z[1] + n<T>(17,4)*z[90];
    z[56]=n<T>(1,2)*z[90] + z[56];
    z[56]=z[3]*z[56];
    z[52]=z[79] + z[56] + z[52];
    z[52]=z[2]*z[52];
    z[56]=z[12]*z[3];
    z[56]=z[56] + 1;
    z[56]=z[68]*z[126]*z[56];
    z[68]=z[11]*z[108];
    z[68]=z[1] + z[68];
    z[68]=z[68]*z[58];
    z[64]=z[1] - z[64];
    z[64]=z[3]*z[11]*z[64];
    z[64]=z[68] + z[64];
    z[64]=z[3]*z[64];
    z[56]=z[64] + z[56];
    z[56]=z[12]*z[56];
    z[34]=z[59] + 2*z[56] + z[52] + z[92] + z[34] + z[72];
    z[34]=z[13]*z[34];
    z[52]=z[40]*z[9];
    z[56]=z[52]*z[28];
    z[59]=z[10]*z[27];
    z[64]= - z[10]*z[76];
    z[64]=z[17] + z[64];
    z[64]=z[5]*z[64];
    z[68]=z[1]*z[69];
    z[68]=z[68] - z[36];
    z[68]=z[6]*z[68];
    z[69]= - z[8]*z[47];
    z[59]=z[56] + z[69] + z[68] + z[64] + z[15] + z[59];
    z[59]=z[59]*z[98];
    z[64]= - 59*z[26] - 33*z[25];
    z[64]=z[64]*z[19];
    z[64]= - z[78] + z[64];
    z[64]=z[64]*z[19];
    z[36]=z[64] + z[36];
    z[36]=z[5]*z[36];
    z[31]=z[31]*z[35];
    z[29]= - n<T>(1,2)*z[29] + z[31];
    z[29]=z[29]*z[23];
    z[31]= - 27*z[26] + n<T>(37,2)*z[25];
    z[19]=z[31]*z[19];
    z[19]=z[29] + z[19] + z[36];
    z[19]=z[6]*z[19];
    z[29]= - z[95] - n<T>(11,2)*z[25];
    z[29]=z[10]*z[29];
    z[31]=z[73]*z[35];
    z[29]=z[31] - z[83] + z[29];
    z[29]=z[5]*z[29];
    z[31]= - z[83] + z[49];
    z[36]=z[17] + z[46];
    z[36]=z[8]*z[36];
    z[31]=3*z[31] + z[36];
    z[31]=z[8]*z[31];
    z[36]=9*z[39];
    z[46]=20*z[15] - z[36];
    z[46]=z[6]*z[10]*z[46];
    z[46]=z[46] + 3*z[56];
    z[46]=z[9]*z[46];
    z[56]=z[104] + z[140];
    z[19]=z[59] + z[46] + z[31] + z[19] + n<T>(3,2)*z[56] + z[29];
    z[19]=z[11]*z[19];
    z[29]= - z[104] - z[61];
    z[29]=z[10]*z[29];
    z[29]=z[15] + 13*z[29];
    z[29]=n<T>(1,4)*z[29] + 4*z[48];
    z[29]=z[5]*z[29];
    z[31]= - z[134] + 8*z[25];
    z[46]= - z[39] + z[86];
    z[23]=z[46]*z[23];
    z[23]=z[23] + 3*z[31] + z[29];
    z[23]=z[6]*z[23];
    z[29]=z[37] + z[140];
    z[29]=z[29]*z[85];
    z[29]=z[29] - z[91] - 16*z[25];
    z[29]=z[5]*z[29];
    z[31]=z[122] - n<T>(7,4)*z[49];
    z[37]= - z[17]*z[63];
    z[31]=3*z[31] + z[37];
    z[31]=z[8]*z[31];
    z[31]=z[31] + z[44] + n<T>(43,4)*z[51];
    z[31]=z[8]*z[31];
    z[37]=n<T>(71,2)*z[15] - 31*z[39];
    z[37]=z[6]*z[37];
    z[46]=z[10]*z[52];
    z[37]=z[37] + n<T>(13,2)*z[46];
    z[37]=z[37]*z[130];
    z[19]=z[19] + z[37] + z[31] + z[23] + 17*z[1] + z[29];
    z[19]=z[11]*z[19];
    z[23]=z[17]*z[8];
    z[29]=n<T>(3,2)*z[23] - z[15] - z[49];
    z[29]=z[4]*z[29];
    z[31]= - z[5]*z[47];
    z[31]=n<T>(11,8)*z[15] + z[31];
    z[31]=z[5]*z[31];
    z[37]=18*z[15];
    z[46]= - z[37] + n<T>(11,2)*z[23];
    z[46]=z[8]*z[46];
    z[47]=z[17]*z[11];
    z[48]= - z[83] - z[47];
    z[48]=z[11]*z[48];
    z[29]=z[29] + z[48] + z[46] + z[77] + z[31];
    z[29]=z[4]*z[29];
    z[31]=n<T>(13,2)*z[40];
    z[46]=14*z[70] + 15*z[26] + z[31];
    z[46]=z[9]*z[46];
    z[47]= - n<T>(3,2)*z[47] + z[137] - z[49];
    z[47]=z[4]*z[47];
    z[47]=z[47] + z[108] + z[101];
    z[47]=z[3]*z[47];
    z[48]= - n<T>(1,4)*z[15] - z[49];
    z[48]=z[48]*z[74];
    z[48]= - z[124] + z[48];
    z[48]=z[48]*z[57];
    z[52]=z[63]*z[15];
    z[52]=z[52] - z[124];
    z[31]= - z[31] - z[52];
    z[31]=z[8]*z[31];
    z[56]= - z[104] - n<T>(7,2)*z[70];
    z[56]=n<T>(1,2)*z[56] + 3*z[43];
    z[56]=z[11]*z[56];
    z[59]= - 25*z[1] + 11*z[127];
    z[29]=z[47] + z[29] + 3*z[56] + z[46] + z[31] + n<T>(5,2)*z[59] + z[48];
    z[29]=z[3]*z[29];
    z[31]=z[70] - z[26];
    z[31]=z[31]*z[9];
    z[46]= - z[65] - 5*z[70];
    z[46]=z[8]*z[46];
    z[46]=54*z[31] - 40*z[1] + z[46];
    z[46]=z[46]*z[102];
    z[47]=z[124] - z[70];
    z[47]=z[8]*z[47];
    z[48]=z[70] - z[44];
    z[56]=z[48]*z[98];
    z[47]=z[56] - z[126] + z[47];
    z[47]=z[47]*z[58];
    z[46]=z[46] + z[47];
    z[46]=z[3]*z[46];
    z[47]=z[51]*z[8];
    z[44]=z[5]*z[44];
    z[44]=z[44] - z[47];
    z[44]=z[8]*z[44];
    z[48]=z[8]*z[48];
    z[48]=z[1] + z[48];
    z[48]=z[3]*z[48];
    z[56]= - z[5]*z[1];
    z[44]=z[48] + z[56] + z[44];
    z[44]=z[12]*z[44];
    z[48]=z[14]*z[118];
    z[44]=12*z[44] - 54*z[48];
    z[44]=z[120]*z[44];
    z[48]=21*z[127] - 19*z[47];
    z[48]=z[8]*z[48];
    z[56]=z[25] - z[26];
    z[56]=z[56]*z[5];
    z[56]=z[56] + z[47] - z[1];
    z[59]=z[9]*z[56];
    z[64]=z[126]*z[5];
    z[48]= - 18*z[59] - z[64] + z[48];
    z[48]=z[48]*z[102];
    z[59]= - z[139] + z[47];
    z[59]=z[8]*z[59];
    z[56]= - z[56]*z[98];
    z[56]=z[56] + z[64] + z[59];
    z[56]=z[56]*z[58];
    z[44]=z[46] + z[48] + z[56] + z[44];
    z[44]=z[12]*z[44];
    z[46]=z[42]*z[5];
    z[48]= - 7*z[46] + 51*z[26] + z[67];
    z[48]=z[5]*z[48];
    z[23]= - z[122] + z[23];
    z[56]=4*z[8];
    z[23]=z[23]*z[56];
    z[23]= - 17*z[51] + z[23];
    z[23]=z[23]*z[71];
    z[58]= - z[1]*z[53];
    z[42]=13*z[42] + z[58];
    z[42]=z[5]*z[42];
    z[58]=10*z[25];
    z[42]= - z[58] + z[42];
    z[42]=z[9]*z[42];
    z[23]=z[42] + z[23] + 13*z[1] + z[48];
    z[23]=z[9]*z[23];
    z[42]= - z[5]*z[111];
    z[42]=z[142] + z[42];
    z[42]=z[5]*z[42];
    z[48]=z[99] - n<T>(25,4)*z[51];
    z[48]=z[8]*z[48];
    z[48]=n<T>(25,2)*z[127] + z[48];
    z[48]=z[8]*z[48];
    z[23]=z[23] + z[42] + z[48];
    z[23]=z[9]*z[23];
    z[42]=z[122] - z[75];
    z[42]=z[5]*z[42];
    z[48]= - z[63]*z[49];
    z[42]=z[48] + z[42] + z[27];
    z[42]=z[42]*z[98];
    z[48]= - z[26] - z[109];
    z[46]=2*z[48] + z[46];
    z[46]=z[5]*z[46];
    z[48]=z[62]*z[97];
    z[42]=z[42] + z[48] + 3*z[1] + z[46];
    z[42]=z[11]*z[42];
    z[46]=z[5]*z[25];
    z[46]= - 7*z[1] + z[46];
    z[46]=z[46]*z[125];
    z[47]=9*z[127] - n<T>(7,2)*z[47];
    z[48]=n<T>(3,2)*z[8];
    z[47]=z[47]*z[48];
    z[42]=z[42] + z[46] + z[47];
    z[42]=z[11]*z[42];
    z[46]= - 21*z[26] + n<T>(17,2)*z[70];
    z[46]=z[8]*z[46];
    z[46]=109*z[1] + z[46];
    z[31]=n<T>(1,2)*z[46] - 5*z[31];
    z[31]=z[9]*z[31];
    z[46]= - 23*z[26] + n<T>(13,2)*z[70];
    z[46]=z[8]*z[46];
    z[43]= - 2*z[43] + z[52];
    z[43]=z[11]*z[43];
    z[43]=2*z[43] + 14*z[1] + n<T>(1,2)*z[46];
    z[43]=z[11]*z[43];
    z[45]=z[11]*z[45];
    z[45]=z[1] + z[45];
    z[45]=z[3]*z[45];
    z[31]=z[45] + z[43] - z[64] + z[31];
    z[31]=z[3]*z[31];
    z[43]=92*z[121] - z[119];
    z[43]=z[43]*z[123];
    z[43]=z[43] + 43*z[121] + 145*z[119];
    z[43]=z[14]*z[43];
    z[41]= - z[41]*z[126];
    z[23]=z[44] + z[43] + z[31] + z[42] + z[41] + z[23];
    z[23]=z[12]*z[23];
    z[31]=z[24] + z[16];
    z[31]=z[10]*z[31];
    z[31]=3*z[22] + z[31];
    z[31]=z[10]*z[31];
    z[31]=z[55] + z[31];
    z[31]=z[6]*z[31];
    z[41]=z[10]*z[82];
    z[31]=z[31] + z[22] + z[41];
    z[31]=z[31]*z[56];
    z[41]=z[6]*z[60];
    z[24]=z[31] - 7*z[41] - z[24] - 8*z[16];
    z[24]=z[24]*z[71];
    z[18]=z[6]*z[18];
    z[18]=z[24] - z[37] + n<T>(41,4)*z[18];
    z[18]=z[8]*z[18];
    z[24]= - n<T>(91,4)*z[26] - 18*z[25];
    z[24]=z[10]*z[24];
    z[31]=n<T>(29,2)*z[26] + z[58];
    z[31]=z[31]*z[35];
    z[24]=z[24] + z[31];
    z[24]=z[5]*z[24];
    z[27]=z[27]*z[53];
    z[31]= - z[108] - z[61];
    z[31]=z[31]*z[28];
    z[27]=z[31] + z[27];
    z[27]=z[5]*z[27];
    z[31]=z[113] + z[96];
    z[31]=z[10]*z[31];
    z[27]=z[31] + z[27];
    z[27]=z[9]*z[27];
    z[18]=z[27] + z[18] + n<T>(3,2)*z[40] + z[24] + n<T>(149,4)*z[26] + z[61];
    z[18]=z[9]*z[18];
    z[24]= - 47*z[15] - z[66];
    z[24]=z[10]*z[24];
    z[24]= - 58*z[17] + z[24];
    z[24]=z[10]*z[24];
    z[22]= - 23*z[22] + z[24];
    z[22]=z[6]*z[22];
    z[16]=z[22] - z[89] - 18*z[16];
    z[16]=z[16]*z[63];
    z[22]=14*z[15] + z[93];
    z[24]=37*z[15] + z[36];
    z[24]=z[10]*z[24];
    z[24]=28*z[17] + z[24];
    z[24]=z[24]*z[57];
    z[16]=z[16] + 4*z[22] + z[24];
    z[16]=z[8]*z[16];
    z[22]= - n<T>(255,2)*z[15] - 41*z[39];
    z[22]=z[22]*z[103];
    z[16]=z[16] + z[22] + 6*z[26] - n<T>(151,4)*z[51];
    z[16]=z[8]*z[16];
    z[22]=n<T>(53,2)*z[26] + 12*z[25];
    z[22]=z[22]*z[85];
    z[24]= - z[15] - z[87];
    z[24]=z[6]*z[24];
    z[22]=z[24] - 20*z[25] + z[22];
    z[22]=z[5]*z[22];
    z[16]=z[18] + z[16] - 28*z[1] + z[22];
    z[16]=z[9]*z[16];
    z[18]=z[26] + z[140];
    z[22]= - z[10]*z[144];
    z[22]= - n<T>(7,8)*z[15] + z[22];
    z[22]=z[5]*z[22];
    z[24]=z[6]*z[84];
    z[18]=z[24] + n<T>(1,4)*z[18] + z[22];
    z[18]=z[6]*z[18];
    z[22]=n<T>(19,2)*z[26] + z[111];
    z[22]=z[10]*z[22];
    z[22]=z[81] + z[22];
    z[22]=z[5]*z[22];
    z[22]=z[22] - n<T>(25,4)*z[26] - z[88];
    z[22]=z[5]*z[22];
    z[18]=z[18] - n<T>(1,4)*z[1] + z[22];
    z[18]=z[6]*z[18];
    z[22]= - 101*z[26] + 67*z[25];
    z[22]=z[22]*z[38];
    z[24]=z[6]*z[50]*z[54];
    z[22]=z[24] - 36*z[15] + z[22];
    z[22]=z[6]*z[22];
    z[24]= - z[76]*z[57]*z[28];
    z[25]=58*z[26] + 9*z[25];
    z[25]=z[10]*z[25];
    z[15]=83*z[15] + z[25];
    z[15]=z[10]*z[15];
    z[15]=z[24] + 34*z[17] + z[15];
    z[15]=z[15]*z[105];
    z[15]=z[15] + 6*z[141] + z[22];
    z[15]=z[8]*z[15];
    z[17]=z[124] + z[30];
    z[17]= - z[40] + 3*z[17] - n<T>(1,2)*z[51];
    z[17]=z[17]*z[57];
    z[15]=z[15] + z[127] + z[17];
    z[15]=z[8]*z[15];
    z[17]=z[80] + z[96];
    z[22]= - z[5]*z[39];
    z[17]=2*z[17] + z[22];
    z[17]=z[5]*z[17];
    z[17]=n<T>(13,2)*z[1] + z[17];
    z[17]=z[5]*z[17];

    r += z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[23]
       + z[29] + z[32] + z[33] + z[34];
 
    return r;
}

template double qqb_2lNLC_r683(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r683(const std::array<dd_real,31>&);
#endif
