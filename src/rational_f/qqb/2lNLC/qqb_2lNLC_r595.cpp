#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r595(const std::array<T,31>& k) {
  T z[45];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[8];
    z[5]=k[11];
    z[6]=k[13];
    z[7]=k[7];
    z[8]=k[12];
    z[9]=k[10];
    z[10]=k[14];
    z[11]=k[4];
    z[12]=k[15];
    z[13]=k[9];
    z[14]=2*z[8];
    z[15]=3*z[12];
    z[16]=z[15] - z[14];
    z[16]=2*z[16] + z[10];
    z[16]=z[10]*z[16];
    z[17]=z[12] + z[10];
    z[18]=z[4]*z[2];
    z[19]= - static_cast<T>(6)+ 7*z[18];
    z[19]=z[4]*z[19];
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[17]=z[4]*z[17];
    z[19]=npow(z[2],2);
    z[20]=z[19]*z[4];
    z[21]=z[20] - z[2];
    z[22]=npow(z[4],2);
    z[23]=z[21]*z[22];
    z[23]=z[23] + z[12] - z[8];
    z[23]=z[5]*z[23];
    z[24]=npow(z[12],2);
    z[24]=2*z[24];
    z[25]=4*z[12];
    z[26]= - z[25] + z[7];
    z[26]=z[7]*z[26];
    z[27]=3*z[7];
    z[28]=z[27] + z[8];
    z[28]=z[8]*z[28];
    z[16]=z[23] + z[17] + z[16] + z[28] + z[24] + z[26];
    z[16]=z[5]*z[16];
    z[17]=n<T>(1,2)*z[10];
    z[23]= - z[11]*z[17];
    z[23]=z[23] + z[18];
    z[23]=z[4]*z[23];
    z[23]= - z[17] + z[23];
    z[23]=z[4]*z[23];
    z[20]=z[2] - 2*z[20];
    z[20]=z[20]*z[22];
    z[26]=z[10] - z[7];
    z[20]=z[20] + z[26];
    z[20]=z[5]*z[20];
    z[28]=z[8]*z[7];
    z[29]=z[9]*z[7];
    z[30]=z[8] - z[9];
    z[30]=z[10]*z[30];
    z[20]=z[20] + z[23] + z[30] - z[28] + z[29];
    z[20]=z[6]*z[20];
    z[23]=2*z[12];
    z[29]=z[12] + z[13];
    z[30]= - z[29]*z[23];
    z[31]=n<T>(1,2)*z[7];
    z[32]=9*z[12] - z[7];
    z[32]=z[32]*z[31];
    z[33]=3*z[28];
    z[34]=n<T>(1,2)*z[8];
    z[35]=z[12] + z[34];
    z[35]=z[9]*z[35];
    z[30]=z[35] - z[33] + z[30] + z[32];
    z[30]=z[9]*z[30];
    z[32]=n<T>(3,2)*z[8];
    z[35]=z[23] - z[32];
    z[35]=z[13]*z[35];
    z[36]=3*z[8];
    z[37]=z[9] - z[25] + z[36];
    z[37]=z[9]*z[37];
    z[38]= - z[36] + 5*z[9];
    z[38]=z[38]*z[17];
    z[35]=z[38] + z[37] + z[35];
    z[35]=z[10]*z[35];
    z[37]=z[7]*z[12];
    z[38]=3*z[9];
    z[39]=z[13]*z[38];
    z[40]=z[13] - z[8];
    z[40]=z[10]*z[40];
    z[39]=z[40] + z[39] - z[37] - 7*z[28];
    z[40]=n<T>(1,2)*z[9];
    z[41]=z[40] - z[4];
    z[42]= - z[10] - z[41];
    z[42]=z[4]*z[42];
    z[39]=n<T>(1,2)*z[39] + z[42];
    z[39]=z[4]*z[39];
    z[16]=z[20] + z[16] + z[39] + z[30] + z[35];
    z[16]=z[6]*z[16];
    z[20]= - 13*z[12] + z[8];
    z[20]=z[20]*z[34];
    z[25]= - n<T>(5,2)*z[9] + z[25] + z[27];
    z[25]=z[9]*z[25];
    z[30]=3*z[37];
    z[35]=z[31] - z[9];
    z[35]=z[10]*z[35];
    z[20]=z[35] + z[25] + z[30] + z[20];
    z[20]=z[10]*z[20];
    z[25]=z[41]*z[22];
    z[35]= - z[11]*z[25];
    z[39]=z[12] - z[7];
    z[39]=z[9]*z[39];
    z[41]=z[9] + z[8];
    z[42]= - z[10]*z[41];
    z[35]=z[35] + z[42] - z[28] + z[39];
    z[35]=z[5]*z[35];
    z[39]= - z[12] - n<T>(3,2)*z[7];
    z[39]=z[9]*z[39];
    z[24]=z[39] + z[24] - z[30];
    z[24]=z[9]*z[24];
    z[30]= - z[40] - 5*z[10];
    z[30]=z[12]*z[30];
    z[17]=9*z[4] + z[9] + z[17];
    z[17]=z[4]*z[17];
    z[17]=z[17] + z[30];
    z[17]=z[4]*z[17];
    z[30]=z[28]*z[12];
    z[17]=z[35] + z[17] + z[20] + n<T>(13,2)*z[30] + z[24];
    z[17]=z[5]*z[17];
    z[20]=npow(z[9],2);
    z[24]= - z[20]*z[26];
    z[35]=npow(z[8],2);
    z[39]=z[35]*z[31];
    z[24]=z[25] + z[39] + z[24];
    z[24]=z[5]*z[24];
    z[20]= - z[10]*z[7]*z[20];
    z[20]=z[20] + z[24];
    z[20]=z[5]*z[20];
    z[24]=npow(z[7],2);
    z[25]=z[24]*z[34];
    z[39]=npow(z[10],2);
    z[42]=z[41]*z[39];
    z[43]=npow(z[4],3);
    z[44]= - z[2]*z[43];
    z[24]= - z[24] + z[44];
    z[24]=z[5]*z[24];
    z[24]=z[24] + z[25] + z[42];
    z[24]=z[6]*z[24];
    z[25]= - z[5]*z[35];
    z[25]= - z[43] + z[25];
    z[25]=z[5]*z[25];
    z[35]=z[9]*z[8]*z[39];
    z[24]=z[24] + z[35] + z[25];
    z[24]=z[6]*z[24];
    z[20]=z[20] + z[24];
    z[20]=z[1]*z[20];
    z[24]= - z[15] - z[31];
    z[24]=z[7]*z[24];
    z[25]=z[15] - z[34];
    z[25]=z[8]*z[25];
    z[31]=z[7] + z[8];
    z[31]=z[9]*z[31];
    z[24]=z[31] + z[24] + z[25];
    z[24]=z[9]*z[24];
    z[25]=z[7] - z[8];
    z[25]=z[9]*z[11]*z[25];
    z[25]= - z[14] + z[25];
    z[25]=z[9]*z[25];
    z[31]= - z[8]*z[13];
    z[25]=z[31] + z[25];
    z[25]=z[10]*z[25];
    z[24]=z[24] + z[25];
    z[24]=z[10]*z[24];
    z[25]=n<T>(3,2)*z[9];
    z[31]=n<T>(3,2)*z[10];
    z[34]=z[25] - z[31];
    z[35]= - z[29]*z[34];
    z[39]=z[7]*z[2];
    z[39]= - static_cast<T>(1)+ z[39];
    z[39]=z[8]*z[39];
    z[39]= - z[7] + 9*z[39];
    z[39]=z[4]*z[39];
    z[35]=z[39] + 5*z[28] + z[35];
    z[35]=z[4]*z[35];
    z[39]=z[10]*z[8]*z[12];
    z[30]= - z[30] + z[39];
    z[30]=n<T>(7,2)*z[30] + z[35];
    z[30]=z[4]*z[30];
    z[35]= - z[28]*z[15];
    z[39]= - z[9]*z[37];
    z[35]=z[35] + z[39];
    z[35]=z[9]*z[35];
    z[16]=z[20] + z[16] + z[17] + z[30] + z[35] + z[24];
    z[16]=z[1]*z[16];
    z[17]=z[4]*npow(z[2],3);
    z[20]= - 2*z[19] + z[17];
    z[20]=z[4]*z[20];
    z[20]=z[2] + z[20];
    z[20]=z[5]*z[20];
    z[21]=z[4]*z[21];
    z[20]=z[20] + static_cast<T>(9)+ 7*z[21];
    z[20]=z[4]*z[20];
    z[20]= - z[31] + z[14] - n<T>(3,2)*z[12] + 2*z[7] + z[20];
    z[20]=z[5]*z[20];
    z[21]=z[2] + z[11];
    z[18]=z[21]*z[18];
    z[21]= - z[11] + z[18];
    z[21]=z[4]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[4]*z[21];
    z[17]=z[19] - z[17];
    z[22]=z[22]*z[5];
    z[17]=z[17]*z[22];
    z[17]=z[21] + z[17];
    z[17]=z[6]*z[17];
    z[21]=z[7] - z[13];
    z[24]= - z[21]*z[32];
    z[21]=z[15] - 5*z[21];
    z[21]=n<T>(1,2)*z[21] - z[41];
    z[21]=z[9]*z[21];
    z[30]=2*z[9];
    z[31]=z[36] + z[30];
    z[31]=z[10]*z[31];
    z[32]=9*z[2];
    z[35]=z[32] + z[11];
    z[36]=z[35]*z[4];
    z[39]=static_cast<T>(1)- z[36];
    z[39]=z[4]*z[39];
    z[34]=z[39] - 4*z[7] + z[8] - z[34];
    z[34]=z[4]*z[34];
    z[17]=z[17] + z[20] + z[34] + z[31] + z[24] + z[21];
    z[17]=z[6]*z[17];
    z[20]=z[37] - z[33];
    z[21]= - n<T>(1,2)*z[12] - z[7];
    z[21]=z[21]*z[38];
    z[24]=z[30] + n<T>(13,2)*z[8] - n<T>(5,2)*z[12] - z[26];
    z[24]=z[10]*z[24];
    z[26]= - static_cast<T>(7)+ z[36];
    z[26]=z[4]*z[26];
    z[25]=z[26] + n<T>(11,2)*z[10] - z[12] - z[25];
    z[25]=z[4]*z[25];
    z[18]= - z[2] + z[18];
    z[18]=z[18]*z[22];
    z[18]=z[18] + z[25] + z[24] + n<T>(1,2)*z[20] + z[21];
    z[18]=z[5]*z[18];
    z[20]= - z[7]*z[11];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[9]*z[20];
    z[14]=z[20] - z[27] - z[14];
    z[14]=z[9]*z[14];
    z[20]= - z[2]*z[13];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[8]*z[20];
    z[21]= - z[8]*z[11];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[9]*z[21];
    z[20]=z[21] - z[13] + z[20];
    z[20]=z[10]*z[20];
    z[15]=z[15]*z[13];
    z[21]=n<T>(3,2)*z[13] + z[23];
    z[21]=z[8]*z[21];
    z[14]=z[20] + z[14] + z[21] - z[15] - 2*z[37];
    z[14]=z[10]*z[14];
    z[20]=z[29]*z[40];
    z[19]=z[7]*z[19];
    z[19]=9*z[19] - z[35];
    z[19]=z[8]*z[19];
    z[21]=z[7]*z[35];
    z[19]=z[21] + z[19];
    z[19]=z[4]*z[19];
    z[21]= - z[7]*z[32];
    z[21]=static_cast<T>(7)+ z[21];
    z[21]=z[8]*z[21];
    z[19]=z[19] - z[7] + z[21];
    z[19]=z[4]*z[19];
    z[21]=2*z[28];
    z[22]= - n<T>(1,2)*z[29] - 4*z[8];
    z[22]=z[10]*z[22];
    z[19]=z[19] + z[22] + z[20] + z[37] + z[21];
    z[19]=z[4]*z[19];
    z[20]=z[12]*z[11];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[7]*z[20];
    z[20]= - z[12] + z[20];
    z[20]=z[9]*z[20];
    z[15]=z[20] + z[21] + z[15] - n<T>(1,2)*z[37];
    z[15]=z[9]*z[15];
    z[20]= - z[28]*z[23];
    z[14]=z[16] + z[17] + z[18] + z[19] + z[14] + z[20] + z[15];

    r += z[14]*npow(z[3],2)*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r595(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r595(const std::array<dd_real,31>&);
#endif
