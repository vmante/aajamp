#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r478(const std::array<T,31>& k) {
  T z[85];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[8];
    z[12]=k[2];
    z[13]=k[9];
    z[14]=k[15];
    z[15]=npow(z[1],3);
    z[16]=z[15]*z[3];
    z[17]=z[10]*z[1];
    z[18]=npow(z[1],2);
    z[19]=n<T>(197,2)*z[16] + n<T>(167,2)*z[18] - 15*z[17];
    z[20]=n<T>(1,4)*z[7];
    z[19]=z[19]*z[20];
    z[21]=npow(z[10],2);
    z[22]=z[21]*z[1];
    z[23]= - z[6]*z[22];
    z[23]=z[23] - z[17] + z[16];
    z[23]=z[13]*z[23];
    z[24]=z[18]*z[3];
    z[25]=n<T>(123,8)*z[24];
    z[19]=n<T>(11,4)*z[23] + z[19] - z[1] + z[25];
    z[23]=3*z[13];
    z[19]=z[19]*z[23];
    z[26]=z[3]*z[1];
    z[27]= - 7*z[1] - n<T>(971,2)*z[24];
    z[27]=z[7]*z[27];
    z[27]= - n<T>(247,2)*z[26] + z[27];
    z[19]=n<T>(1,4)*z[27] + z[19];
    z[19]=z[13]*z[19];
    z[27]=n<T>(1,2)*z[17];
    z[28]=z[27] + z[18];
    z[29]= - n<T>(1,2)*z[16] - z[28];
    z[29]=z[7]*z[29];
    z[30]=n<T>(1,2)*z[1];
    z[29]=z[29] - z[30] - z[24];
    z[31]=n<T>(11,2)*z[13];
    z[29]=z[29]*z[31];
    z[25]=13*z[1] - z[25];
    z[25]=z[7]*z[25];
    z[25]=z[29] + n<T>(37,4)*z[26] + z[25];
    z[25]=z[25]*z[23];
    z[29]=z[26]*z[7];
    z[25]=n<T>(181,8)*z[29] + z[25];
    z[25]=z[13]*z[25];
    z[32]=z[1] + z[24];
    z[32]=z[7]*z[32];
    z[32]=n<T>(1,2)*z[26] + z[32];
    z[33]=11*z[13];
    z[32]=z[32]*z[33];
    z[32]= - n<T>(37,2)*z[29] + z[32];
    z[34]=npow(z[13],2);
    z[32]=z[32]*z[34];
    z[35]=z[8]*npow(z[13],3);
    z[35]=n<T>(11,2)*z[35];
    z[29]= - z[29]*z[35];
    z[29]=z[32] + z[29];
    z[32]=n<T>(3,2)*z[8];
    z[29]=z[29]*z[32];
    z[36]=z[24]*npow(z[7],3);
    z[25]=z[29] + n<T>(39,8)*z[36] + z[25];
    z[25]=z[8]*z[25];
    z[29]=npow(z[1],4);
    z[37]=z[29]*z[4];
    z[38]=43*z[37];
    z[39]= - 171*z[15] + z[38];
    z[39]=z[4]*z[39];
    z[39]=n<T>(147,2)*z[18] + z[39];
    z[39]=z[4]*z[39];
    z[40]=z[37] - z[15];
    z[41]=z[40]*z[2];
    z[42]=z[41] + z[18];
    z[43]=27*z[2];
    z[44]= - z[42]*z[43];
    z[39]=z[44] - n<T>(137,2)*z[1] + z[39];
    z[44]=n<T>(1,4)*z[2];
    z[39]=z[39]*z[44];
    z[45]=npow(z[3],2);
    z[42]=z[42]*z[45];
    z[39]=z[39] + 3*z[42];
    z[39]=z[3]*z[39];
    z[42]=175*z[15] - 107*z[37];
    z[42]=z[4]*z[42];
    z[46]=n<T>(21,2)*z[18];
    z[42]=z[46] + z[42];
    z[42]=z[4]*z[42];
    z[47]=z[18]*z[10];
    z[40]=z[40] + z[47];
    z[48]=npow(z[2],2);
    z[49]=z[40]*z[48];
    z[42]=59*z[49] + n<T>(185,2)*z[1] + z[42];
    z[42]=z[6]*z[42]*z[44];
    z[49]= - 35*z[18] - 13*z[16];
    z[49]=z[7]*z[49];
    z[49]= - 13*z[24] + z[49];
    z[49]=z[7]*z[49];
    z[49]=95*z[26] + n<T>(3,4)*z[49];
    z[49]=z[7]*z[49];
    z[50]=z[18]*z[2];
    z[45]= - z[45]*z[50];
    z[51]=z[18]*npow(z[2],3);
    z[45]=n<T>(9,4)*z[51] + z[45];
    z[45]=z[3]*z[45];
    z[36]=z[45] + n<T>(13,8)*z[36];
    z[36]=z[9]*z[36];
    z[19]=z[25] + z[19] + 3*z[36] + n<T>(1,2)*z[49] + z[42] + n<T>(59,4)*z[51]
    + z[39];
    z[19]=z[8]*z[19];
    z[25]= - 3*z[15] - z[47];
    z[36]=n<T>(1,2)*z[10];
    z[39]=z[36]*z[29];
    z[42]=npow(z[1],5);
    z[39]=z[39] + z[42];
    z[45]=npow(z[4],2);
    z[49]=43*z[45];
    z[52]= - z[39]*z[49];
    z[25]=n<T>(119,2)*z[25] + z[52];
    z[25]=z[4]*z[25];
    z[52]=z[15]*z[10];
    z[53]=z[52] + z[29];
    z[54]=z[4]*z[53];
    z[54]= - z[15] + z[54];
    z[55]=z[39]*z[4];
    z[55]=z[55] - z[29];
    z[56]=z[55]*z[43];
    z[54]=n<T>(59,2)*z[54] + z[56];
    z[54]=z[2]*z[54];
    z[25]=z[54] + 163*z[18] + z[25];
    z[54]=n<T>(1,2)*z[2];
    z[25]=z[25]*z[54];
    z[56]=z[36]*z[15];
    z[56]=z[56] + z[29];
    z[56]=z[56]*z[4];
    z[57]=z[36]*z[18];
    z[58]=z[57] + z[15];
    z[59]= - z[56] - z[58];
    z[59]=z[4]*z[59];
    z[59]=n<T>(43,2)*z[59] - z[18] - n<T>(3,8)*z[17];
    z[59]=z[4]*z[59];
    z[55]=z[2]*z[55];
    z[55]=z[56] + z[55];
    z[55]=n<T>(113,4)*z[47] + 57*z[55];
    z[55]=z[5]*z[55];
    z[56]=z[15]*z[2];
    z[55]=z[55] - 41*z[18] + n<T>(63,4)*z[56];
    z[60]=n<T>(1,2)*z[5];
    z[55]=z[55]*z[60];
    z[25]=z[55] + z[25] + 9*z[1] + z[59];
    z[25]=z[5]*z[25];
    z[55]=z[17] + z[18];
    z[59]=z[55]*z[21];
    z[61]=n<T>(1,2)*z[59];
    z[62]=z[28]*z[10];
    z[63]=n<T>(1,2)*z[15];
    z[62]=z[62] + z[63];
    z[64]=z[21]*z[5];
    z[65]=z[62]*z[64];
    z[65]= - z[61] + z[65];
    z[65]=z[6]*z[65];
    z[66]=z[55]*z[10];
    z[66]=z[66] - z[15];
    z[64]=z[28]*z[64];
    z[64]=z[65] - n<T>(1,2)*z[66] + z[64];
    z[31]=z[64]*z[31];
    z[64]=3*z[17];
    z[65]= - n<T>(29,2)*z[18] + z[64];
    z[65]=z[10]*z[65];
    z[65]= - n<T>(35,2)*z[15] + z[65];
    z[65]=z[5]*z[65];
    z[65]=z[65] + n<T>(35,2)*z[18] - z[64];
    z[65]=z[6]*z[65];
    z[67]= - n<T>(145,8)*z[18] + z[17];
    z[67]=z[5]*z[67];
    z[65]=n<T>(5,4)*z[65] + z[67];
    z[65]=z[10]*z[65];
    z[31]=z[31] - n<T>(197,8)*z[16] + n<T>(153,8)*z[18] - z[17] + z[65];
    z[31]=z[31]*z[23];
    z[65]=n<T>(1,2)*z[18] - z[17];
    z[67]=5*z[18];
    z[68]=z[67] + 19*z[17];
    z[68]=z[10]*z[68];
    z[68]= - 291*z[15] + n<T>(47,2)*z[68];
    z[68]=z[68]*z[60];
    z[65]=165*z[65] + z[68];
    z[65]=z[6]*z[65];
    z[68]= - 339*z[16] + 119*z[18] - 233*z[17];
    z[68]=z[68]*z[20];
    z[69]= - 25*z[18] + n<T>(803,2)*z[17];
    z[69]=z[5]*z[69];
    z[69]=355*z[24] - n<T>(803,2)*z[1] + z[69];
    z[65]=z[68] + n<T>(1,2)*z[69] + z[65];
    z[31]=n<T>(1,2)*z[65] + z[31];
    z[31]=z[13]*z[31];
    z[65]=z[17]*z[5];
    z[68]=z[26]*z[9];
    z[69]=57*z[68] - 23*z[1] - 11*z[65];
    z[70]=z[22]*z[5];
    z[70]=z[70] - z[17];
    z[71]= - z[1] + z[68];
    z[71]=z[9]*z[71];
    z[71]=z[71] - z[70];
    z[72]=17*z[14];
    z[71]=z[71]*z[72];
    z[69]=n<T>(1,2)*z[69] + z[71];
    z[69]=z[14]*z[69];
    z[71]=z[5]*z[1];
    z[73]= - 291*z[71] + 383*z[26];
    z[69]=n<T>(1,8)*z[73] + z[69];
    z[69]=z[14]*z[69];
    z[70]= - z[16] + z[18] + z[70];
    z[70]=z[70]*z[33];
    z[65]=z[24] - z[1] + z[65];
    z[65]=15*z[65] + z[70];
    z[23]=z[65]*z[23];
    z[65]=z[71] - z[26];
    z[23]=n<T>(233,2)*z[65] + z[23];
    z[23]=z[13]*z[23];
    z[65]= - z[30] + z[24];
    z[33]=z[65]*z[33];
    z[33]= - n<T>(15,2)*z[26] + z[33];
    z[33]=z[33]*z[34];
    z[26]= - z[26]*z[35];
    z[26]=z[33] + z[26];
    z[26]=z[26]*z[32];
    z[23]=z[26] + z[69] + n<T>(1,4)*z[23];
    z[23]=z[12]*z[23];
    z[26]=27*z[15] - 59*z[47];
    z[26]=n<T>(1,2)*z[26] - z[38];
    z[26]=z[26]*z[54];
    z[32]=19*z[18];
    z[33]=z[15]*z[4];
    z[26]=z[26] + z[32] - n<T>(81,2)*z[33];
    z[26]=z[2]*z[26];
    z[34]=z[45]*z[29];
    z[35]= - z[46] + 25*z[34];
    z[35]=z[4]*z[35];
    z[35]=n<T>(21,2)*z[1] + z[35];
    z[26]=n<T>(3,2)*z[35] + z[26];
    z[26]=z[2]*z[26];
    z[35]= - n<T>(23,2)*z[18] + 75*z[33];
    z[35]=z[4]*z[35];
    z[35]= - n<T>(161,4)*z[1] + z[35];
    z[35]=z[4]*z[35];
    z[19]=z[31] + z[25] + z[35] + z[26] + z[19] + z[23];
    z[23]=75*z[15] - n<T>(143,2)*z[47];
    z[25]=npow(z[1],6);
    z[26]=z[45]*z[25];
    z[31]=7*z[29] - n<T>(107,8)*z[26];
    z[31]=z[4]*z[31];
    z[35]=z[29]*z[10];
    z[35]=z[35] - z[42];
    z[45]=z[25]*z[4];
    z[46]=z[35] + z[45];
    z[65]=z[46]*z[48];
    z[23]=n<T>(59,8)*z[65] + n<T>(1,8)*z[23] + z[31];
    z[23]=z[2]*z[23];
    z[31]=npow(z[5],2);
    z[65]= - z[39]*z[31];
    z[65]=z[65] - n<T>(691,2)*z[15] - 163*z[47];
    z[69]=n<T>(1,8)*z[5];
    z[65]=z[65]*z[69];
    z[70]=z[25]*z[5];
    z[70]=z[70] - z[42];
    z[71]=z[3]*z[70];
    z[71]=z[29] - z[71];
    z[39]=z[39]*z[5];
    z[71]=z[39] - n<T>(1,2)*z[71];
    z[71]=z[6]*z[71];
    z[73]=z[29]*z[3];
    z[71]=n<T>(1,2)*z[73] + z[71];
    z[71]=z[6]*z[71];
    z[74]=z[42]*z[4];
    z[75]=z[74] + z[29];
    z[76]=107*z[4];
    z[77]=z[75]*z[76];
    z[78]=n<T>(279,2)*z[15] - z[77];
    z[79]=n<T>(1,2)*z[4];
    z[78]=z[78]*z[79];
    z[80]=9*z[18];
    z[78]=z[80] + z[78];
    z[81]=n<T>(1,4)*z[31];
    z[25]= - z[25]*z[81];
    z[25]= - 225*z[29] + z[25];
    z[25]=z[5]*z[25];
    z[25]=n<T>(317,4)*z[15] + z[25];
    z[81]=n<T>(1,4)*z[3];
    z[25]=z[25]*z[81];
    z[23]=n<T>(23,4)*z[71] + z[25] + z[65] + n<T>(1,4)*z[78] + z[23];
    z[23]=z[6]*z[23];
    z[25]=z[2]*z[46];
    z[25]=z[74] + z[25];
    z[46]=n<T>(13,2)*z[3];
    z[65]=z[70]*z[46];
    z[25]=z[65] + 13*z[39] - n<T>(35,2)*z[29] + 11*z[25];
    z[25]=z[6]*z[25];
    z[35]=z[35]*z[4];
    z[39]=z[35]*z[2];
    z[52]=z[52] - z[29];
    z[65]= - z[4]*z[52];
    z[65]= - z[39] + z[15] + z[65];
    z[70]=z[42]*z[5];
    z[71]=z[70]*z[46];
    z[78]=z[29]*z[5];
    z[25]=z[25] + z[71] + 11*z[65] + n<T>(13,2)*z[78];
    z[25]=z[7]*z[25];
    z[53]=z[5]*z[53];
    z[53]=z[73] + z[53];
    z[65]=z[10]*z[56];
    z[53]=23*z[15] - n<T>(7,2)*z[65] - n<T>(39,2)*z[53];
    z[53]=z[6]*z[53];
    z[65]=z[78] - z[15];
    z[71]=z[3]*z[65];
    z[78]=z[5]*z[15];
    z[71]=z[78] - z[71];
    z[71]= - 59*z[18] - 39*z[71];
    z[25]=3*z[25] + n<T>(1,2)*z[71] + z[53];
    z[25]=z[7]*z[25];
    z[52]=z[52]*z[79];
    z[52]=z[52] - z[15] + z[57];
    z[52]=z[52]*z[76];
    z[53]= - 103*z[18] + 111*z[17];
    z[52]=n<T>(1,4)*z[53] + z[52];
    z[52]=z[4]*z[52];
    z[52]=n<T>(45,4)*z[1] + z[52];
    z[35]= - z[29] + z[35];
    z[35]=z[4]*z[35];
    z[35]=n<T>(107,8)*z[35] - n<T>(267,8)*z[15] + 4*z[47];
    z[35]=z[4]*z[35];
    z[53]=z[10]*z[33];
    z[39]= - 103*z[53] - 59*z[39];
    z[53]=n<T>(1,8)*z[2];
    z[39]=z[39]*z[53];
    z[35]=z[39] + n<T>(185,16)*z[18] + z[35];
    z[35]=z[2]*z[35];
    z[39]=z[31]*z[29];
    z[57]= - z[67] - n<T>(1,4)*z[39];
    z[71]=n<T>(1,4)*z[5];
    z[57]=z[57]*z[71];
    z[78]=z[70] + z[29];
    z[82]= - z[78]*z[60];
    z[82]= - 129*z[15] + z[82];
    z[82]=z[5]*z[82];
    z[82]= - n<T>(625,2)*z[18] + z[82];
    z[83]=n<T>(1,8)*z[3];
    z[82]=z[82]*z[83];
    z[23]=n<T>(1,8)*z[25] + z[23] + z[82] + z[57] + n<T>(1,4)*z[52] + z[35];
    z[23]=z[7]*z[23];
    z[25]=z[54]*z[68];
    z[35]=z[2]*z[1];
    z[52]= - z[1] - z[50];
    z[52]=z[3]*z[52];
    z[25]=z[25] + z[35] + z[52];
    z[25]=z[9]*z[25];
    z[52]=2*z[18];
    z[27]= - z[52] + z[27];
    z[27]=z[2]*z[27];
    z[57]=z[18] + n<T>(1,2)*z[56];
    z[57]=z[3]*z[57];
    z[25]=z[25] + z[57] - z[30] + z[27];
    z[25]=z[9]*z[25];
    z[27]= - z[4]*z[22];
    z[27]=z[27] + z[55];
    z[30]=z[47] - z[15];
    z[57]= - z[2]*z[30];
    z[25]=z[25] + n<T>(1,2)*z[27] + z[57];
    z[25]=z[9]*z[25];
    z[27]=z[21]*z[4];
    z[57]= - z[62]*z[27];
    z[57]=z[61] + z[57];
    z[57]=z[5]*z[57];
    z[61]=z[56] - z[55];
    z[61]=z[36]*z[61];
    z[28]=z[28]*z[27];
    z[25]=z[25] + z[57] + z[28] + z[61];
    z[25]=z[25]*z[72];
    z[28]= - 47*z[18] + n<T>(57,4)*z[17];
    z[28]=z[2]*z[28];
    z[57]=14*z[18] - n<T>(1,4)*z[56];
    z[57]=z[3]*z[57];
    z[61]= - 189*z[1] - 131*z[50];
    z[61]=z[3]*z[61];
    z[61]=189*z[35] + z[61];
    z[62]=z[68]*z[2];
    z[61]=n<T>(1,4)*z[61] + 33*z[62];
    z[61]=z[9]*z[61];
    z[68]=z[4]*z[17];
    z[28]=z[61] + z[57] + z[28] - 4*z[1] - n<T>(75,4)*z[68];
    z[28]=z[9]*z[28];
    z[57]=11*z[18];
    z[61]=13*z[17];
    z[68]=z[57] + z[61];
    z[68]=z[10]*z[68];
    z[72]=65*z[18];
    z[82]= - z[72] - 43*z[17];
    z[82]=z[82]*z[36];
    z[82]= - 11*z[15] + z[82];
    z[84]=z[4]*z[10];
    z[82]=z[82]*z[84];
    z[68]=z[68] + z[82];
    z[68]=z[5]*z[68];
    z[67]= - z[67] - 9*z[17];
    z[61]=n<T>(119,4)*z[18] + z[61];
    z[61]=z[61]*z[84];
    z[82]= - z[15] - 57*z[47];
    z[44]=z[82]*z[44];
    z[25]=z[25] + z[28] + z[68] + z[44] + n<T>(1,2)*z[67] + z[61];
    z[25]=z[14]*z[25];
    z[28]=345*z[18] + 263*z[17];
    z[44]= - 157*z[18] - n<T>(457,4)*z[17];
    z[44]=z[10]*z[44];
    z[44]= - n<T>(563,4)*z[15] + z[44];
    z[44]=z[4]*z[44];
    z[28]=n<T>(1,4)*z[28] + z[44];
    z[28]=z[28]*z[71];
    z[44]= - z[4]*z[1];
    z[44]=z[44] + 9*z[35];
    z[61]= - 675*z[1] + 103*z[50];
    z[61]=z[3]*z[61];
    z[44]=n<T>(1117,2)*z[62] + 75*z[44] + z[61];
    z[44]=z[9]*z[44];
    z[57]= - z[57] - n<T>(67,8)*z[17];
    z[57]=z[4]*z[57];
    z[61]=z[80] + n<T>(383,2)*z[17];
    z[53]=z[61]*z[53];
    z[61]= - 7*z[18] + 111*z[56];
    z[61]=z[61]*z[83];
    z[25]=z[25] + n<T>(1,8)*z[44] + z[61] + z[28] + z[53] - n<T>(191,16)*z[1] + 
    z[57];
    z[25]=z[14]*z[25];
    z[28]=z[52] + z[17];
    z[44]=z[28]*z[10];
    z[44]=z[44] + z[15];
    z[27]= - z[44]*z[27];
    z[27]=z[59] + z[27];
    z[27]=z[7]*z[27];
    z[52]=z[7] - z[2];
    z[22]=z[22]*z[52];
    z[53]=z[6] - z[4];
    z[57]=z[44]*z[53];
    z[22]=z[57] + z[22];
    z[22]=z[9]*z[22];
    z[57]= - z[10]*z[66];
    z[57]=z[29] + z[57];
    z[57]=z[4]*z[57];
    z[21]=z[21]*z[2];
    z[28]=z[28]*z[21];
    z[21]= - z[10] + z[21];
    z[21]=z[6]*z[44]*z[21];
    z[21]=z[22] + z[27] + z[21] + z[57] + z[28];
    z[21]=z[11]*z[21];
    z[22]=24*z[17] + n<T>(101,4)*z[18];
    z[27]= - z[10]*z[22];
    z[28]=n<T>(5,4)*z[15];
    z[27]= - z[28] + z[27];
    z[27]=z[2]*z[27];
    z[22]=z[27] + z[22];
    z[22]=z[10]*z[22];
    z[22]=z[28] + z[22];
    z[22]=z[6]*z[22];
    z[27]= - z[18] - n<T>(3,2)*z[17];
    z[27]=z[10]*z[27];
    z[28]=z[32] + 17*z[17];
    z[28]=z[28]*z[36];
    z[28]=z[15] + z[28];
    z[28]=z[28]*z[84];
    z[27]=z[27] + z[28];
    z[27]=z[7]*z[27];
    z[28]= - z[17]*z[52];
    z[32]= - z[55]*z[53];
    z[28]=z[32] + z[28];
    z[32]=n<T>(37,4)*z[9];
    z[28]=z[28]*z[32];
    z[36]= - n<T>(55,2)*z[18] + z[64];
    z[36]=z[10]*z[36];
    z[36]= - n<T>(61,2)*z[15] + z[36];
    z[36]=z[36]*z[79];
    z[44]= - n<T>(7,2)*z[18] - 8*z[17];
    z[52]=3*z[2];
    z[44]=z[10]*z[44]*z[52];
    z[21]=7*z[21] + z[28] + z[27] + z[22] + z[36] + z[44];
    z[21]=z[11]*z[21];
    z[22]=37*z[17];
    z[27]= - 31*z[18] + z[22];
    z[27]=z[27]*z[79];
    z[28]= - 39*z[18] + n<T>(185,2)*z[17];
    z[28]=z[2]*z[28];
    z[27]=z[27] + z[28];
    z[28]=z[18] + n<T>(15,2)*z[17];
    z[28]=z[10]*z[28];
    z[28]=103*z[15] + n<T>(53,2)*z[28];
    z[28]=z[28]*z[54];
    z[28]=z[28] + 17*z[18] - n<T>(185,4)*z[17];
    z[28]=z[6]*z[28];
    z[36]= - 251*z[18] - n<T>(375,2)*z[17];
    z[36]=z[10]*z[36];
    z[36]= - 313*z[15] + z[36];
    z[36]=z[4]*z[36];
    z[22]=z[36] + 97*z[18] - z[22];
    z[22]=z[22]*z[20];
    z[36]=z[7] + z[53];
    z[36]=z[1]*z[36];
    z[35]= - z[35] + z[36];
    z[32]=z[35]*z[32];
    z[22]=z[32] + z[22] + n<T>(1,2)*z[27] + z[28];
    z[21]=n<T>(1,2)*z[22] + z[21];
    z[21]=z[11]*z[21];
    z[22]=z[30]*z[4];
    z[27]= - z[65]*z[46];
    z[28]=11*z[37] + n<T>(13,2)*z[73];
    z[28]=z[6]*z[28];
    z[27]=z[28] + z[27] - n<T>(13,2)*z[18] - 11*z[22];
    z[27]=z[27]*npow(z[7],2);
    z[28]=107*z[34];
    z[30]=n<T>(261,2)*z[18] - z[28];
    z[30]=z[4]*z[30];
    z[32]=z[6]*z[73];
    z[32]=z[16] + z[32];
    z[32]= - 9*z[33] + 23*z[32];
    z[32]=z[6]*z[32];
    z[24]=z[32] + z[30] - n<T>(225,2)*z[24];
    z[24]=z[6]*z[24];
    z[22]= - z[18] + z[22];
    z[22]=z[22]*z[76];
    z[22]=n<T>(111,2)*z[1] + z[22];
    z[22]=z[22]*z[79];
    z[30]=z[65]*z[71];
    z[30]=z[72] + z[30];
    z[30]=z[5]*z[30];
    z[30]=n<T>(35,2)*z[1] + z[30];
    z[30]=z[3]*z[30];
    z[22]=n<T>(3,2)*z[27] + n<T>(1,2)*z[24] + z[22] + z[30];
    z[20]=z[22]*z[20];
    z[22]=n<T>(31,2)*z[18];
    z[24]=z[22] - 43*z[34];
    z[24]=z[4]*z[24];
    z[27]=z[29]*z[48];
    z[27]=n<T>(257,2)*z[18] - 27*z[27];
    z[27]=z[2]*z[27];
    z[30]=z[29]*z[2];
    z[30]=z[30] - z[37];
    z[30]=z[30]*z[5];
    z[32]=n<T>(21,2)*z[56] - 19*z[30];
    z[32]=z[5]*z[32];
    z[24]=3*z[32] + z[24] + z[27];
    z[24]=z[5]*z[24];
    z[27]=z[15]*z[48];
    z[27]=n<T>(143,2)*z[1] + 9*z[27];
    z[27]=z[27]*z[52];
    z[24]=z[27] + z[24];
    z[27]= - z[56] + z[30];
    z[27]=z[3]*z[27];
    z[27]=z[50] + z[27];
    z[30]=10*z[33];
    z[32]=z[5]*z[30];
    z[27]=z[32] + n<T>(3,2)*z[27];
    z[27]=z[3]*z[27];
    z[24]=n<T>(1,8)*z[24] + z[27];
    z[24]=z[3]*z[24];
    z[27]=z[5]*z[58];
    z[32]=z[60]*z[73];
    z[27]=z[27] + z[32];
    z[27]=z[27]*npow(z[6],2);
    z[32]=z[58]*z[31];
    z[34]=n<T>(303,2)*z[1] - z[32];
    z[34]=z[34]*z[69];
    z[35]= - 289*z[18] - z[39];
    z[16]=n<T>(1,16)*z[35] + 4*z[16];
    z[16]=z[3]*z[5]*z[16];
    z[16]=n<T>(23,4)*z[27] + z[34] + z[16];
    z[16]=z[6]*z[16];
    z[27]= - z[58]*z[49];
    z[27]=57*z[32] - n<T>(5,4)*z[1] + z[27];
    z[27]=z[5]*z[4]*z[27];
    z[27]= - n<T>(27,2)*z[51] + z[27];
    z[16]=z[20] + z[16] + n<T>(1,4)*z[27] + z[24];
    z[16]=z[9]*z[16];
    z[20]= - 45*z[29] - n<T>(43,2)*z[26];
    z[20]=z[4]*z[20];
    z[24]=z[45] - z[42];
    z[24]=z[24]*z[2];
    z[26]=z[29] + z[24];
    z[26]=z[2]*z[26];
    z[20]=n<T>(27,8)*z[26] + 20*z[15] + n<T>(1,4)*z[20];
    z[20]=z[2]*z[20];
    z[26]= - z[4]*z[75];
    z[26]= - n<T>(3,2)*z[15] + z[26];
    z[26]=z[4]*z[26];
    z[26]= - n<T>(775,2)*z[18] + 43*z[26];
    z[24]=z[24] + z[74];
    z[27]=z[24]*z[31];
    z[20]=n<T>(57,8)*z[27] + n<T>(1,8)*z[26] + z[20];
    z[20]=z[5]*z[20];
    z[26]=z[74] - z[29];
    z[27]=z[26]*z[54];
    z[24]= - z[24]*z[60];
    z[24]=z[24] + z[27] - z[63] + z[37];
    z[24]=z[3]*z[24];
    z[22]=3*z[24] - n<T>(3,2)*z[41] + z[22] - z[30];
    z[22]=z[3]*z[22];
    z[24]=z[42]*z[49];
    z[24]= - n<T>(963,2)*z[15] + z[24];
    z[24]=z[4]*z[24];
    z[27]= - z[2]*z[26];
    z[27]= - z[15] + z[27];
    z[27]=z[27]*z[43];
    z[24]=z[27] + n<T>(631,2)*z[18] + z[24];
    z[24]=z[24]*z[54];
    z[27]= - n<T>(171,2)*z[15] + z[38];
    z[27]=z[4]*z[27];
    z[27]= - n<T>(219,2)*z[18] + z[27];
    z[27]=z[4]*z[27];
    z[24]=z[24] - n<T>(207,2)*z[1] + z[27];
    z[20]=z[22] + n<T>(1,4)*z[24] + z[20];
    z[20]=z[3]*z[20];
    z[22]=z[47] + z[15];
    z[22]=z[22]*z[10];
    z[24]= - z[22] - z[26];
    z[24]=z[2]*z[24];
    z[24]=z[24] + z[40];
    z[24]=z[2]*z[24];
    z[26]= - n<T>(1153,2)*z[15] + z[77];
    z[26]=z[4]*z[26];
    z[24]=59*z[24] + z[26] + n<T>(41,2)*z[18] - 259*z[17];
    z[24]=z[2]*z[24];
    z[26]= - n<T>(465,2)*z[18] + z[28];
    z[26]=z[4]*z[26];
    z[24]=z[24] - 91*z[1] + z[26];
    z[22]=z[22] - z[29];
    z[26]= - z[22]*z[60];
    z[26]=z[26] + z[58];
    z[26]=z[26]*z[71];
    z[17]=z[26] + n<T>(41,8)*z[18] + 117*z[17];
    z[17]=z[5]*z[17];
    z[26]=z[5]*z[78];
    z[15]=417*z[15] + z[26];
    z[15]=z[15]*z[60];
    z[15]=197*z[18] + z[15];
    z[15]=z[15]*z[81];
    z[15]=z[15] + n<T>(1,4)*z[24] + z[17];
    z[17]=z[5]*z[22];
    z[22]= - z[3]*z[70];
    z[17]=z[22] - z[47] + z[17];
    z[17]=z[6]*z[17];
    z[22]=n<T>(23,8)*z[73] + n<T>(23,4)*z[58];
    z[22]=z[5]*z[22];
    z[17]=n<T>(23,8)*z[17] - 4*z[18] + n<T>(9,8)*z[33] + z[22];
    z[17]=z[6]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[6]*z[15];

    r += z[15] + z[16] + n<T>(1,2)*z[19] + z[20] + z[21] + z[23] + z[25];
 
    return r;
}

template double qqb_2lNLC_r478(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r478(const std::array<dd_real,31>&);
#endif
