#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r350(const std::array<T,31>& k) {
  T z[69];
  T r = 0;

    z[1]=k[1];
    z[2]=k[11];
    z[3]=k[12];
    z[4]=k[15];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[14];
    z[8]=k[7];
    z[9]=k[10];
    z[10]=k[8];
    z[11]=k[13];
    z[12]=k[2];
    z[13]=k[9];
    z[14]=k[3];
    z[15]=z[6]*z[7];
    z[16]=2*z[15];
    z[17]=z[16] - 1;
    z[18]=6*z[17];
    z[19]= - z[6]*z[18];
    z[20]=z[5]*z[3];
    z[21]=2*z[20];
    z[22]=static_cast<T>(1)- z[21];
    z[22]=z[5]*z[22];
    z[19]=z[19] + z[22];
    z[22]=npow(z[6],2);
    z[18]=z[22]*z[18];
    z[23]=static_cast<T>(3)- z[20];
    z[23]=z[5]*z[23];
    z[23]=z[23] + 6*z[6];
    z[23]=z[5]*z[23];
    z[18]=z[18] + z[23];
    z[18]=z[2]*z[18];
    z[18]=2*z[19] + z[18];
    z[18]=z[2]*z[18];
    z[19]=z[16] + 1;
    z[23]=7*z[3];
    z[24]=z[23] - z[11];
    z[25]= - z[9] - z[24];
    z[25]=z[5]*z[25];
    z[18]=z[18] + 6*z[19] + z[25];
    z[18]=z[2]*z[18];
    z[25]=z[15] - 1;
    z[26]=z[25]*z[22];
    z[27]=z[5]*z[6];
    z[28]= - 2*z[26] - z[27];
    z[29]=npow(z[6],3);
    z[30]=z[25]*z[29];
    z[31]=z[22]*z[5];
    z[32]=z[30] + z[31];
    z[33]=2*z[2];
    z[32]=z[32]*z[33];
    z[28]=3*z[28] + z[32];
    z[28]=z[2]*z[28];
    z[32]=3*z[5];
    z[34]= - static_cast<T>(10)+ 11*z[15];
    z[34]=z[6]*z[34];
    z[28]=3*z[28] + z[34] + z[32];
    z[28]=z[2]*z[28];
    z[34]=npow(z[9],2);
    z[35]=2*z[5];
    z[36]=z[35]*z[34]*z[8];
    z[37]=5*z[8];
    z[38]= - z[9]*z[37];
    z[38]=z[38] - z[36];
    z[38]=z[5]*z[38];
    z[39]=4*z[8];
    z[38]= - z[39] + z[38];
    z[38]=z[5]*z[38];
    z[40]=z[6]*z[8];
    z[28]=z[28] - z[40] + z[38];
    z[28]=z[4]*z[28];
    z[38]=z[13] + z[10];
    z[41]=z[38]*z[11];
    z[38]=z[38]*z[3];
    z[41]=z[41] - z[38];
    z[42]=z[7]*z[11];
    z[43]= - 2*z[41] + z[42];
    z[43]=z[6]*z[43];
    z[44]=z[34]*z[5];
    z[45]=2*z[8];
    z[46]=z[45] + z[3];
    z[46]=z[46]*z[44];
    z[47]=3*z[3];
    z[48]= - z[8] + z[47];
    z[48]=z[9]*z[48];
    z[46]=z[48] + z[46];
    z[46]=z[5]*z[46];
    z[48]=2*z[3];
    z[49]=z[48] - z[11];
    z[50]=3*z[8];
    z[18]=z[28] + z[18] + z[46] + z[43] - z[50] - z[49];
    z[18]=z[4]*z[18];
    z[28]=6*z[15];
    z[43]=static_cast<T>(1)+ z[28];
    z[43]=z[6]*z[43];
    z[46]=static_cast<T>(9)- 7*z[20];
    z[46]=z[5]*z[46];
    z[43]=z[43] + z[46];
    z[43]=z[2]*z[43];
    z[46]=3*z[15];
    z[51]=static_cast<T>(5)- z[46];
    z[52]=2*z[11];
    z[53]= - 25*z[3] + z[52];
    z[53]=z[5]*z[53];
    z[43]=z[43] + 2*z[51] + z[53];
    z[43]=z[2]*z[43];
    z[51]=3*z[9];
    z[53]=n<T>(1,3)*z[7];
    z[43]=z[43] - z[51] + z[53] - 23*z[3] + 3*z[11];
    z[43]=z[2]*z[43];
    z[54]=z[11]*z[10];
    z[38]= - z[38] + z[54];
    z[38]=3*z[38] + z[42];
    z[54]= - z[45] - z[47];
    z[54]=z[54]*z[44];
    z[55]= - z[11] + n<T>(13,3)*z[8] - 6*z[3];
    z[55]=z[9]*z[55];
    z[18]=z[18] + z[43] + z[54] + 2*z[38] + z[55];
    z[18]=z[4]*z[18];
    z[38]=2*z[6];
    z[43]=4*z[15];
    z[54]= - static_cast<T>(3)+ z[43];
    z[54]=z[54]*z[38];
    z[55]= - static_cast<T>(1)+ z[20];
    z[55]=z[55]*z[32];
    z[54]=z[54] + z[55];
    z[54]=z[2]*z[54];
    z[55]=z[20] - z[15];
    z[54]=z[54] - static_cast<T>(1)+ 8*z[55];
    z[54]=z[2]*z[54];
    z[24]=z[54] + z[24];
    z[24]=z[2]*z[24];
    z[54]=3*z[6];
    z[55]= - z[17]*z[54];
    z[56]=4*z[2];
    z[57]=z[26]*z[56];
    z[55]=z[55] + z[57];
    z[57]=npow(z[2],2);
    z[55]=z[4]*z[55]*z[57];
    z[58]=z[10]*z[48];
    z[24]=z[55] + z[58] + z[24];
    z[24]=z[4]*z[24];
    z[16]= - static_cast<T>(3)+ z[16];
    z[16]=2*z[16] + 11*z[20];
    z[16]=z[2]*z[16];
    z[55]=9*z[3];
    z[58]=z[55] - z[7];
    z[16]=2*z[58] + z[16];
    z[16]=z[16]*z[57];
    z[16]=z[16] + z[24];
    z[16]=z[4]*z[16];
    z[24]= - 3*z[20] + z[19];
    z[24]=z[2]*z[24];
    z[58]=4*z[3];
    z[24]= - z[58] + z[24];
    z[24]=z[24]*z[57];
    z[59]=z[22]*z[7];
    z[60]=npow(z[2],3);
    z[61]=z[4]*z[60]*z[59];
    z[24]=z[24] + z[61];
    z[24]=z[4]*z[24];
    z[61]= - 5*z[3] + z[7];
    z[61]=z[61]*z[60];
    z[24]=z[61] + z[24];
    z[24]=z[4]*z[24];
    z[60]=z[1]*z[3]*z[60]*npow(z[4],2);
    z[24]=z[24] + z[60];
    z[24]=z[1]*z[24];
    z[60]=z[9]*z[7];
    z[61]=z[11]*z[3];
    z[62]=z[61]*z[60];
    z[57]=z[57]*z[48];
    z[63]=z[8]*z[60];
    z[57]= - n<T>(19,3)*z[63] + z[57];
    z[33]=z[57]*z[33];
    z[16]=z[24] + z[16] - n<T>(29,3)*z[62] + z[33];
    z[16]=z[1]*z[16];
    z[24]=npow(z[11],2);
    z[21]=z[21]*z[24];
    z[33]=5*z[61];
    z[57]= - z[33] + z[21];
    z[57]=z[5]*z[57];
    z[57]= - z[58] + z[57];
    z[21]= - 7*z[61] + z[21];
    z[58]=npow(z[5],2);
    z[21]=z[21]*z[58];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[2]*z[21];
    z[21]=2*z[57] + z[21];
    z[21]=z[2]*z[21];
    z[57]=z[7]*z[8];
    z[57]=n<T>(19,3)*z[57];
    z[62]= - 3*z[10] - z[3];
    z[62]=z[11]*z[62];
    z[62]=z[62] - z[57];
    z[34]=z[34]*z[53];
    z[24]=z[3]*z[24];
    z[24]=z[24] + z[34];
    z[24]=z[5]*z[24];
    z[34]=4*z[9];
    z[63]= - 11*z[7] - z[34];
    z[63]=z[9]*z[63];
    z[21]=z[21] + 4*z[24] + 2*z[62] + n<T>(1,3)*z[63];
    z[21]=z[2]*z[21];
    z[24]=z[14] - z[12];
    z[62]=npow(z[14],2);
    z[63]= - z[11]*z[62];
    z[63]=z[24] + z[63];
    z[64]=2*z[9];
    z[63]=z[64]*z[63]*npow(z[13],2);
    z[65]=z[13]*z[14];
    z[66]=2*z[65];
    z[67]= - n<T>(5,3) + z[66];
    z[67]=z[13]*z[67];
    z[67]=z[67] + 8*z[3];
    z[67]=z[11]*z[67];
    z[55]= - n<T>(7,3)*z[11] + n<T>(25,3)*z[8] + z[55];
    z[55]=z[7]*z[55];
    z[55]=z[63] + z[67] + z[55];
    z[55]=z[9]*z[55];
    z[63]=2*z[7];
    z[67]=z[11] + z[8] + n<T>(2,3)*z[3];
    z[67]=z[67]*z[63]*z[44];
    z[68]=z[3]*z[13];
    z[61]=23*z[68] - 20*z[61];
    z[61]=z[61]*z[53];
    z[16]=z[16] + z[18] + z[21] + z[67] + z[61] + z[55];
    z[16]=z[1]*z[16];
    z[18]= - z[19]*z[54];
    z[19]=z[23] - z[52];
    z[19]=z[5]*z[19];
    z[19]= - static_cast<T>(8)+ z[19];
    z[19]=z[5]*z[19];
    z[20]= - static_cast<T>(4)+ z[20];
    z[20]=z[5]*z[20];
    z[20]= - z[38] + z[20];
    z[20]=z[5]*z[20];
    z[21]=4*z[29];
    z[55]=z[7]*z[21];
    z[20]=z[55] + z[20];
    z[20]=z[2]*z[20];
    z[18]=z[20] + z[18] + z[19];
    z[18]=z[2]*z[18];
    z[19]= - static_cast<T>(8)- n<T>(7,3)*z[15];
    z[20]=8*z[49] + z[9];
    z[20]=z[5]*z[20];
    z[18]=z[18] + 2*z[19] + z[20];
    z[18]=z[2]*z[18];
    z[19]= - z[27] + 2*z[22];
    z[19]=z[19]*z[5];
    z[20]= - 2*z[30] - z[19];
    z[31]=z[31] - z[29];
    z[31]=z[31]*z[5];
    z[49]=npow(z[6],4);
    z[55]=z[49]*z[25];
    z[61]=z[55] - z[31];
    z[56]=z[61]*z[56];
    z[20]=9*z[20] + z[56];
    z[20]=z[2]*z[20];
    z[32]= - z[32] + 10*z[6];
    z[32]=z[32]*z[5];
    z[26]=11*z[26] + z[32];
    z[20]=2*z[26] + z[20];
    z[20]=z[2]*z[20];
    z[26]=z[37] - z[64];
    z[56]=z[9]*z[26];
    z[56]=z[56] + z[36];
    z[56]=z[5]*z[56];
    z[39]=z[56] + z[39] - z[51];
    z[39]=z[5]*z[39];
    z[39]=z[39] - static_cast<T>(3)+ z[40];
    z[39]=z[5]*z[39];
    z[51]= - z[25]*z[54];
    z[39]=z[51] + z[39];
    z[20]=2*z[39] + z[20];
    z[20]=z[4]*z[20];
    z[21]=z[17]*z[21];
    z[39]= - z[38] - z[5];
    z[39]=z[39]*z[58];
    z[21]=z[21] + z[39];
    z[21]=z[2]*z[21];
    z[39]=z[43] - 1;
    z[43]=6*z[22];
    z[43]= - z[39]*z[43];
    z[21]=z[21] + z[43] - z[58];
    z[21]=z[2]*z[21];
    z[43]=static_cast<T>(2)+ 9*z[15];
    z[43]=z[43]*z[38];
    z[51]=z[5]*z[9];
    z[56]= - static_cast<T>(11)+ z[51];
    z[56]=z[5]*z[56];
    z[21]=z[21] + z[43] + z[56];
    z[21]=z[2]*z[21];
    z[43]=z[50] + z[9];
    z[43]=z[43]*z[51];
    z[43]=z[43] + z[50] - z[9];
    z[43]=z[5]*z[43];
    z[20]=z[20] + z[21] + z[43] + static_cast<T>(1)- 7*z[15];
    z[20]=z[4]*z[20];
    z[21]=4*z[41] - 3*z[42];
    z[21]=z[6]*z[21];
    z[26]=z[11] + z[26];
    z[26]=z[26]*z[51];
    z[18]=z[20] + z[18] + z[26] + z[21] + 7*z[9] - n<T>(14,3)*z[7] + 14*z[3]
    - 11*z[11];
    z[18]=z[4]*z[18];
    z[20]= - z[24]*z[66];
    z[21]= - z[12] + 3*z[14];
    z[20]=z[20] - z[21];
    z[26]=2*z[13];
    z[20]=z[20]*z[26];
    z[41]=npow(z[14],3);
    z[42]=z[41]*z[26];
    z[43]=3*z[62];
    z[42]=z[43] + z[42];
    z[42]=z[13]*z[42]*z[52];
    z[20]=z[42] - n<T>(1,3) + z[20];
    z[20]=z[9]*z[20];
    z[42]= - n<T>(7,3) + z[66];
    z[42]=z[42]*z[26];
    z[50]=z[62]*z[13];
    z[56]= - n<T>(5,3)*z[14] - 4*z[50];
    z[56]=z[13]*z[56];
    z[56]=n<T>(2,3) + z[56];
    z[56]=z[56]*z[52];
    z[20]=z[20] - n<T>(88,3)*z[7] + z[56] + z[42] - n<T>(5,3)*z[3];
    z[20]=z[9]*z[20];
    z[42]=z[5]*z[11];
    z[23]=z[23] + 6*z[11];
    z[23]=z[23]*z[42];
    z[23]= - 8*z[11] + z[23];
    z[23]=z[5]*z[23];
    z[56]=z[3] + z[52];
    z[42]=z[56]*z[42];
    z[56]=4*z[11];
    z[42]= - z[56] + z[42];
    z[42]=z[2]*z[42]*z[58];
    z[23]=z[42] - static_cast<T>(1)+ z[23];
    z[23]=z[2]*z[23];
    z[42]= - z[10]*z[56];
    z[42]=z[42] - z[57];
    z[42]=z[6]*z[42];
    z[47]=z[47] + z[52];
    z[47]=z[47]*z[56];
    z[47]=z[47] - n<T>(31,3)*z[60];
    z[47]=z[5]*z[47];
    z[48]=z[48] + 3*z[7];
    z[23]=z[23] + z[47] + 2*z[48] + z[42];
    z[23]=z[2]*z[23];
    z[42]=19*z[11];
    z[47]=19*z[8];
    z[48]=z[42] + z[47] - 31*z[3];
    z[48]=z[48]*z[53];
    z[56]=z[52]*z[9];
    z[57]= - z[3]*z[52];
    z[48]= - z[56] + z[57] + z[48];
    z[48]=z[48]*z[51];
    z[57]=n<T>(1,3) + z[65];
    z[57]=z[13]*z[57];
    z[57]=z[11] + z[57] + z[3];
    z[52]=z[57]*z[52];
    z[52]= - z[68] + z[52];
    z[57]=z[47] + 13*z[13];
    z[42]= - z[42] + 2*z[57] - 43*z[3];
    z[42]=z[42]*z[53];
    z[33]=13*z[68] - z[33];
    z[33]=z[33]*z[15];
    z[16]=z[16] + z[18] + z[23] + z[48] + n<T>(2,3)*z[33] + z[20] + 2*z[52]
    + z[42];
    z[16]=z[1]*z[16];
    z[18]=11*z[12];
    z[20]=2*z[14];
    z[23]= - z[18] + z[20];
    z[26]= - z[62]*z[26];
    z[23]=n<T>(1,3)*z[23] + z[26];
    z[23]=z[13]*z[23];
    z[24]=z[24]*z[50];
    z[21]=z[14]*z[21];
    z[21]=z[21] + z[24];
    z[21]=z[13]*z[21];
    z[24]=3*z[41];
    z[26]= - z[13]*npow(z[14],4);
    z[26]= - z[24] + z[26];
    z[26]=z[13]*z[26];
    z[26]= - z[43] + z[26];
    z[26]=z[11]*z[26];
    z[20]=z[26] + z[20] + z[21];
    z[20]=z[9]*z[20];
    z[21]=z[13]*z[24];
    z[21]=n<T>(10,3)*z[62] + z[21];
    z[21]=z[13]*z[21];
    z[21]= - n<T>(4,3)*z[14] + z[21];
    z[21]=z[11]*z[21];
    z[20]=z[20] + z[21] + n<T>(16,3) + z[23];
    z[20]=z[9]*z[20];
    z[21]=n<T>(11,3)*z[15] - static_cast<T>(1)- z[65];
    z[21]=z[68]*z[21];
    z[23]= - n<T>(8,3) + z[65];
    z[23]=z[11]*z[13]*z[23];
    z[24]=n<T>(11,3)*z[13];
    z[26]=z[24] + z[3];
    z[26]=z[7]*z[26];
    z[21]=z[26] + z[23] + z[21];
    z[21]=z[6]*z[21];
    z[18]=z[13]*z[18];
    z[18]= - static_cast<T>(16)+ z[18];
    z[18]=z[18]*z[53];
    z[23]=n<T>(7,3)*z[14] - 3*z[50];
    z[23]=z[13]*z[23];
    z[23]=n<T>(13,3) + z[23];
    z[23]=z[11]*z[23];
    z[18]=z[21] + z[20] + z[18] - z[24] + z[23];
    z[20]= - z[39]*z[29];
    z[21]=z[22] - z[27];
    z[21]=z[21]*z[35];
    z[20]=z[20] + z[21];
    z[17]=z[17]*z[49];
    z[21]=z[22]*z[58];
    z[17]=z[17] + z[21];
    z[17]=z[2]*z[17];
    z[17]=2*z[20] + z[17];
    z[17]=z[2]*z[17];
    z[20]=static_cast<T>(5)+ z[28];
    z[20]=z[20]*z[22];
    z[21]= - 16*z[6] + 5*z[5];
    z[21]=z[5]*z[21];
    z[17]=z[17] + z[20] + z[21];
    z[17]=z[2]*z[17];
    z[19]= - 2*z[29] + z[19];
    z[19]=z[5]*z[19];
    z[19]= - 2*z[55] + z[19];
    z[20]=z[49] + z[31];
    z[20]=z[5]*z[20];
    z[21]=z[25]*npow(z[6],5);
    z[20]=z[21] + z[20];
    z[20]=z[2]*z[20];
    z[19]=3*z[19] + z[20];
    z[19]=z[2]*z[19];
    z[20]=11*z[22] - z[32];
    z[20]=z[5]*z[20];
    z[19]=z[19] + 11*z[30] + z[20];
    z[19]=z[2]*z[19];
    z[20]= - z[37] + z[34];
    z[20]=z[9]*z[20];
    z[20]=z[20] - z[36];
    z[20]=z[5]*z[20];
    z[21]=z[9]*z[12];
    z[22]=z[21] - 3;
    z[23]=z[22]*z[9];
    z[24]= - z[45] - z[23];
    z[20]=2*z[24] + z[20];
    z[20]=z[5]*z[20];
    z[20]=z[20] - 2*z[22] - z[40];
    z[20]=z[5]*z[20];
    z[22]=z[12] - z[54];
    z[20]=2*z[22] + z[20];
    z[20]=z[5]*z[20];
    z[22]=z[7]*z[12];
    z[24]= - z[46] + static_cast<T>(3)+ z[22];
    z[24]=z[6]*z[24];
    z[24]= - z[12] + z[24];
    z[24]=z[24]*z[38];
    z[19]=z[19] + z[24] + z[20];
    z[19]=z[4]*z[19];
    z[15]=z[15] - static_cast<T>(4)- z[22];
    z[15]=z[6]*z[15];
    z[20]= - z[8] + z[64];
    z[20]=z[9]*z[20];
    z[24]= - z[8]*z[44];
    z[20]=z[20] + z[24];
    z[20]=z[5]*z[20];
    z[20]= - z[23] + z[20];
    z[20]=z[5]*z[20];
    z[20]=z[20] + static_cast<T>(7)- 3*z[21];
    z[20]=z[5]*z[20];
    z[15]=z[20] + 2*z[12] + z[15];
    z[15]=z[19] + 2*z[15] + z[17];
    z[15]=z[4]*z[15];
    z[17]=5*z[21] - static_cast<T>(2)- 11*z[22];
    z[19]=z[8]*z[51];
    z[19]=11*z[19] + z[47] - 16*z[9];
    z[19]=z[19]*z[35];
    z[20]=z[47] + 28*z[7];
    z[20]=z[6]*z[20];
    z[17]=z[19] + 2*z[17] + z[20];
    z[19]= - z[29]*z[63];
    z[20]=z[2]*z[7]*z[49];
    z[19]=z[20] + z[19] - z[27];
    z[19]=z[2]*z[19];
    z[19]=z[19] - n<T>(34,3)*z[59] + z[5];
    z[19]=z[2]*z[19];
    z[15]=z[15] + n<T>(1,3)*z[17] + z[19];
    z[15]=z[4]*z[15];
    z[17]=z[7]*z[3];
    z[19]= - z[14]*z[56];
    z[19]= - n<T>(13,3)*z[11] + z[19];
    z[19]=z[9]*z[19];
    z[17]=z[17] + z[19];
    z[17]=z[5]*z[17];
    z[19]=z[5]*z[7];
    z[19]= - z[46] + z[19];
    z[19]=z[2]*z[19];
    z[15]=z[16] + z[15] + z[19] + 2*z[18] + z[17];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r350(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r350(const std::array<dd_real,31>&);
#endif
