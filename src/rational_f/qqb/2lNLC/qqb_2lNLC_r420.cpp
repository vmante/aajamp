#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r420(const std::array<T,31>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=k[5];
    z[9]=n<T>(1,2)*z[4];
    z[10]=z[6] + z[8];
    z[10]=z[2]*z[10];
    z[10]= - static_cast<T>(1)+ z[10];
    z[10]=z[10]*z[9];
    z[10]=z[2] + z[10];
    z[10]=z[4]*z[10];
    z[11]=z[5]*z[3];
    z[12]=z[2]*z[5];
    z[10]=z[10] - n<T>(1,2)*z[11] + z[12];
    z[10]=z[4]*z[10];
    z[13]=z[5] + z[3];
    z[13]=z[13]*z[2];
    z[14]=z[12]*z[3];
    z[15]= - z[1]*z[14];
    z[11]=z[15] - z[11] + z[13];
    z[11]=z[1]*z[11]*npow(z[4],3);
    z[15]=z[5]*z[8];
    z[16]= - z[3]*z[6];
    z[15]= - z[15] - static_cast<T>(1)+ z[16];
    z[15]=z[2]*z[15];
    z[16]=z[3]*z[7];
    z[17]=static_cast<T>(1)- z[16];
    z[17]=z[5]*z[17];
    z[15]=z[15] + z[3] + z[17];
    z[15]=z[4]*z[15];
    z[13]= - z[13] + z[15];
    z[13]=z[4]*z[13];
    z[13]= - 3*z[14] + z[13];
    z[13]=z[4]*z[13];
    z[11]=z[13] + z[11];
    z[11]=z[1]*z[11];
    z[10]=n<T>(1,2)*z[11] - z[14] + z[10];
    z[10]=z[1]*z[10];
    z[11]=n<T>(1,2)*z[5];
    z[13]= - static_cast<T>(1)- z[16];
    z[11]=z[13]*z[11];
    z[13]=z[8] - z[6];
    z[13]=z[13]*z[2];
    z[15]= - static_cast<T>(1)+ z[13];
    z[15]=z[4]*z[8]*z[15];
    z[13]=z[15] + static_cast<T>(1)+ z[13];
    z[9]=z[13]*z[9];
    z[12]=z[12]*z[8];
    z[9]=z[9] + z[11] - z[12];
    z[9]=z[4]*z[9];
    z[11]=z[8]*z[14];
    z[9]=z[10] + z[11] + z[9];

    r += z[9]*npow(z[1],2);
 
    return r;
}

template double qqb_2lNLC_r420(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r420(const std::array<dd_real,31>&);
#endif
