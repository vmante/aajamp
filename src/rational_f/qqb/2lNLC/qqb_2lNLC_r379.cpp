#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r379(const std::array<T,31>& k) {
  T z[37];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[10];
    z[5]=k[11];
    z[6]=k[14];
    z[7]=k[5];
    z[8]=k[8];
    z[9]=k[12];
    z[10]=k[13];
    z[11]=k[15];
    z[12]=k[9];
    z[13]=z[11]*z[12];
    z[14]=z[4]*z[11];
    z[15]=z[13] + z[14];
    z[16]=n<T>(1,4)*z[10];
    z[17]= - z[15]*z[16];
    z[18]=z[4] + z[12];
    z[19]=z[10]*z[18];
    z[15]=n<T>(1,2)*z[15] + z[19];
    z[15]=z[9]*z[15];
    z[19]=n<T>(1,2)*z[10];
    z[20]=z[19] + z[11];
    z[21]=n<T>(3,8)*z[9];
    z[22]=z[8]*z[20]*z[21];
    z[23]=z[10]*z[4];
    z[24]=z[3]*z[4];
    z[23]= - n<T>(5,2)*z[24] - z[23];
    z[23]=z[6]*z[23];
    z[25]=z[14]*z[3];
    z[15]=n<T>(1,8)*z[23] + z[22] + n<T>(1,4)*z[15] + z[25] + z[17];
    z[15]=z[6]*z[15];
    z[17]=z[21] + n<T>(3,8)*z[8];
    z[20]= - z[20]*z[17];
    z[22]=z[4] + z[11];
    z[23]=z[22]*z[3];
    z[26]=npow(z[6],2);
    z[20]= - n<T>(3,16)*z[26] - n<T>(1,2)*z[14] - z[23] + z[20];
    z[20]=z[6]*z[20];
    z[27]=z[10]*z[3];
    z[28]=z[3]*z[11];
    z[29]=z[27] + z[28];
    z[29]=z[29]*z[9];
    z[30]= - z[11] + n<T>(1,2)*z[3];
    z[27]=z[30]*z[27];
    z[27]=z[29] + z[27];
    z[30]=n<T>(1,2)*z[8];
    z[31]=z[10]*z[7];
    z[32]=static_cast<T>(1)+ z[31];
    z[32]=z[32]*z[30];
    z[32]= - z[10] + z[32];
    z[32]=z[8]*z[32];
    z[33]= - z[10]*z[11];
    z[32]=z[33] + z[32];
    z[33]=n<T>(3,4)*z[8];
    z[32]=z[32]*z[33];
    z[27]=z[32] + z[25] + n<T>(3,4)*z[27];
    z[20]=n<T>(1,2)*z[27] + z[20];
    z[20]=z[5]*z[20];
    z[27]=z[3]*z[7];
    z[32]= - static_cast<T>(1)+ z[27];
    z[32]=z[8]*z[32];
    z[32]=z[3] + z[32];
    z[32]=z[30]*z[9]*z[32];
    z[28]=z[10]*z[28];
    z[28]=z[32] + z[28] - z[29];
    z[29]=n<T>(3,2)*z[8];
    z[28]=z[28]*z[29];
    z[32]=z[14]*z[12];
    z[34]=z[4]*z[12];
    z[35]= - z[10]*z[34];
    z[35]= - z[32] + z[35];
    z[35]=z[9]*z[35];
    z[36]=z[10]*z[32];
    z[28]=z[28] + z[36] + n<T>(1,2)*z[35];
    z[35]= - z[1]*z[5];
    z[35]=z[35] + 1;
    z[35]=z[35]*z[24]*npow(z[6],3);
    z[36]= - z[4] - z[3];
    z[36]=z[6]*z[36];
    z[24]= - z[24] + z[36];
    z[24]=z[5]*z[24]*z[26];
    z[24]=z[24] + z[35];
    z[24]=z[1]*z[24];
    z[15]=n<T>(3,16)*z[24] + z[20] + n<T>(1,4)*z[28] + z[15];
    z[20]=npow(z[2],2);
    z[15]=z[1]*z[20]*z[15];
    z[24]=npow(z[7],2);
    z[26]=z[10]*z[24];
    z[26]=z[7] + z[26];
    z[26]=z[26]*z[30];
    z[26]=z[26] - static_cast<T>(1)- n<T>(3,2)*z[31];
    z[26]=z[8]*z[26];
    z[26]=n<T>(3,2)*z[10] + z[26];
    z[26]=z[26]*z[33];
    z[17]= - z[22] + z[17];
    z[17]=z[6]*z[17];
    z[21]=z[10]*z[21];
    z[17]=z[17] + z[26] + z[23] + z[21];
    z[17]=z[5]*z[17];
    z[21]= - z[29] - z[18];
    z[21]=z[9]*z[21];
    z[13]=z[13] + 5*z[14] + z[21];
    z[13]=z[6]*z[13];
    z[13]=z[32] - z[13];
    z[14]=z[3]*z[24];
    z[14]= - z[7] + z[14];
    z[14]=z[9]*z[14];
    z[14]=z[14] + z[27] - z[31];
    z[14]=z[14]*z[30];
    z[21]= - z[3] + z[10];
    z[22]=static_cast<T>(1)- n<T>(1,2)*z[27];
    z[22]=z[9]*z[22];
    z[14]=z[14] + n<T>(1,2)*z[21] + z[22];
    z[14]=z[8]*z[14];
    z[19]= - z[9]*z[19];
    z[14]=z[19] + z[14];
    z[14]=z[14]*z[33];
    z[18]= - z[9]*z[18];
    z[18]=z[18] - z[34];
    z[16]=z[16]*z[18];
    z[13]=z[17] + z[14] - z[25] + z[16] - n<T>(1,4)*z[13];
    z[13]=z[13]*z[20];
    z[13]=n<T>(1,2)*z[13] + z[15];

    r += z[13]*npow(z[1],3);
 
    return r;
}

template double qqb_2lNLC_r379(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r379(const std::array<dd_real,31>&);
#endif
