#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r319(const std::array<T,31>& k) {
  T z[88];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[8];
    z[7]=k[9];
    z[8]=k[14];
    z[9]=k[2];
    z[10]=k[5];
    z[11]=k[3];
    z[12]=k[4];
    z[13]=k[10];
    z[14]=k[15];
    z[15]=2*z[14];
    z[16]=npow(z[12],2);
    z[17]= - z[16]*z[15];
    z[18]=npow(z[12],3);
    z[19]=z[18]*z[13];
    z[20]=z[19] + 2*z[16];
    z[21]= - z[5]*z[20];
    z[22]=z[19]*z[14];
    z[17]=z[21] + z[17] - z[22];
    z[21]=z[5] - z[6];
    z[23]=2*z[10];
    z[21]=z[23]*z[21];
    z[21]=z[21] + 1;
    z[21]=z[2]*z[21];
    z[24]=2*z[5];
    z[25]=2*z[6];
    z[26]=z[24] - z[25];
    z[21]= - z[26] + z[21];
    z[21]=z[10]*z[21];
    z[26]=z[26] - z[15];
    z[27]=z[12]*z[26];
    z[28]=z[2]*z[11];
    z[21]=z[21] - z[28] - static_cast<T>(1)+ z[27];
    z[21]=z[10]*z[21];
    z[17]=2*z[17] + z[21];
    z[17]=z[4]*z[17];
    z[21]=6*z[13];
    z[27]=z[21]*z[16];
    z[29]= - 3*z[16] - 4*z[19];
    z[29]=z[5]*z[29];
    z[29]=z[29] - 7*z[12] - z[27];
    z[29]=z[5]*z[29];
    z[30]=z[27]*z[14];
    z[31]=3*z[2];
    z[26]= - z[31] + z[26];
    z[26]=z[10]*z[26];
    z[32]=z[12]*z[14];
    z[33]=z[6]*z[12];
    z[34]=z[11]*z[31];
    z[17]=z[17] + z[26] + z[33] + z[29] - z[30] - 8*z[32] + z[34];
    z[17]=z[4]*z[17];
    z[26]=2*z[13];
    z[29]=z[26]*z[16];
    z[34]= - z[12] - z[29];
    z[34]=z[5]*z[34];
    z[35]=z[3]*z[12];
    z[36]=static_cast<T>(7)+ 4*z[35];
    z[37]=z[13]*z[12];
    z[34]=8*z[34] - 2*z[36] - z[37];
    z[34]=z[5]*z[34];
    z[38]=z[10]*z[2];
    z[39]=npow(z[6],3);
    z[40]=z[38]*z[39];
    z[41]=npow(z[6],2);
    z[42]= - z[31] - z[6];
    z[42]=z[42]*z[41];
    z[42]=z[42] + z[40];
    z[42]=z[10]*z[42];
    z[43]=static_cast<T>(3)+ z[33];
    z[43]=z[43]*z[25];
    z[44]=5*z[2];
    z[43]=z[44] + z[43];
    z[43]=z[43]*z[25];
    z[45]=7*z[2];
    z[46]=4*z[8] + z[45];
    z[46]=z[5]*z[46];
    z[42]=4*z[42] + z[46] + z[43];
    z[42]=z[10]*z[42];
    z[43]=3*z[3];
    z[46]=static_cast<T>(2)- z[28];
    z[46]=z[46]*z[43];
    z[32]= - z[21]*z[32];
    z[47]= - z[37] + z[33];
    z[48]=3*z[6];
    z[47]=z[47]*z[48];
    z[49]= - z[14] + z[8];
    z[17]=z[17] + z[42] + z[47] + z[34] + z[32] + z[46] + 4*z[49] - z[2]
   ;
    z[17]=z[4]*z[17];
    z[32]=npow(z[11],4);
    z[34]=z[32]*z[13];
    z[42]=npow(z[11],3);
    z[46]=z[42] - z[34];
    z[46]=z[5]*z[46];
    z[47]=z[42]*z[13];
    z[49]=npow(z[11],2);
    z[46]=6*z[46] - 11*z[49] + 20*z[47];
    z[46]=z[5]*z[46];
    z[50]= - 7*z[11] + 4*z[9];
    z[51]=z[50]*z[11];
    z[52]=npow(z[9],2);
    z[53]=3*z[52];
    z[51]=z[51] - z[53];
    z[54]=z[51]*z[26];
    z[55]=npow(z[9],3);
    z[56]=z[55]*z[13];
    z[57]=z[55]*z[8];
    z[56]=z[56] - z[57];
    z[58]= - z[56]*z[25];
    z[59]=z[8]*z[9];
    z[60]=z[10]*z[8];
    z[61]= - z[60] - static_cast<T>(1)- 6*z[59];
    z[61]=z[10]*z[61];
    z[62]=z[4]*npow(z[10],2);
    z[63]=2*z[62];
    z[64]=z[23]*z[8];
    z[65]=static_cast<T>(1)- z[64];
    z[65]=z[65]*z[63];
    z[46]=z[65] + z[61] + z[58] + z[46] + z[54] + 7*z[9] - 3*z[11];
    z[54]=2*z[7];
    z[46]=z[46]*z[54];
    z[58]=3*z[9];
    z[61]=2*z[11];
    z[65]=z[58] + z[61];
    z[65]=z[13]*z[65];
    z[66]=3*z[8];
    z[67]=z[66]*z[9];
    z[68]=z[67] - 1;
    z[65]=z[65] - z[68];
    z[69]=3*z[5];
    z[70]=z[69]*z[49];
    z[71]=z[49]*z[13];
    z[72]= - z[70] - 4*z[71] + 9*z[11];
    z[72]=z[5]*z[72];
    z[73]=z[52]*z[13];
    z[74]=z[52]*z[8];
    z[75]=z[74] - z[9];
    z[76]= - 2*z[75] + z[73];
    z[76]=z[76]*z[25];
    z[77]=z[8] - z[6];
    z[77]=z[77]*z[23];
    z[78]=static_cast<T>(3)- 5*z[60];
    z[78]=z[78]*z[23];
    z[78]=z[11] + z[78];
    z[78]=z[4]*z[78];
    z[46]=z[46] + z[78] + z[77] + z[76] + 2*z[65] + z[72];
    z[46]=z[7]*z[46];
    z[65]=z[39]*z[23];
    z[72]=z[3] - z[2];
    z[76]=z[72]*z[65];
    z[77]=8*z[3];
    z[45]= - z[45] + z[77];
    z[45]=z[45]*z[41];
    z[45]=z[45] - z[76];
    z[45]=z[45]*z[23];
    z[78]= - z[3]*z[8];
    z[79]=2*z[2];
    z[80]=z[79] - z[8];
    z[81]= - z[5]*z[80];
    z[78]=z[78] + z[81];
    z[43]=z[79] - z[43];
    z[43]=2*z[43] - z[6];
    z[81]=4*z[6];
    z[43]=z[43]*z[81];
    z[43]=z[45] + 3*z[78] + z[43];
    z[43]=z[10]*z[43];
    z[45]=z[26]*z[49];
    z[78]=5*z[11];
    z[82]=z[78] - z[45];
    z[82]=z[5]*z[82];
    z[83]= - z[11]*z[26];
    z[82]=z[82] + static_cast<T>(1)+ z[83];
    z[82]=z[82]*z[24];
    z[83]=3*z[13];
    z[84]=z[6] - z[83];
    z[84]=z[9]*z[84];
    z[85]=z[67] + 4;
    z[84]=z[85] + z[84];
    z[84]=z[84]*z[25];
    z[86]=z[3]*z[11];
    z[87]= - static_cast<T>(1)+ z[86];
    z[87]=3*z[87] + z[64];
    z[87]=z[4]*z[87];
    z[43]=z[46] + z[87] + z[43] + z[84] + z[82] + z[21] - 2*z[80] + 11*
    z[3];
    z[43]=z[7]*z[43];
    z[46]=z[73] + z[75];
    z[56]=z[6]*z[56];
    z[46]=3*z[46] + z[56];
    z[46]=z[6]*z[46];
    z[56]=z[69]*z[47];
    z[56]= - 10*z[71] + z[56];
    z[56]=z[5]*z[56];
    z[71]=2*z[8];
    z[73]= - z[62]*z[71];
    z[50]= - z[13]*z[50];
    z[80]= - z[10]*z[66];
    z[46]=z[73] + z[80] + z[46] + z[56] + z[50] + z[85];
    z[46]=z[46]*z[54];
    z[50]=npow(z[5],2);
    z[56]= - z[50]*z[31];
    z[73]=2*z[3];
    z[80]=z[2] - z[73];
    z[80]=z[80]*z[41];
    z[56]=z[76] + z[56] + 4*z[80];
    z[56]=z[10]*z[56];
    z[76]=6*z[3];
    z[80]=6*z[6] - z[2] + z[76];
    z[80]=z[80]*z[25];
    z[82]=z[2] + z[69];
    z[82]=z[5]*z[82];
    z[56]=z[56] + z[82] + z[80];
    z[56]=z[10]*z[56];
    z[21]= - z[21] - z[66] - z[73];
    z[74]= - z[58] + z[74];
    z[74]=z[74]*z[25];
    z[80]=z[9]*z[26];
    z[74]=z[74] + z[80] - static_cast<T>(10)- z[59];
    z[74]=z[74]*z[25];
    z[45]=z[11] - z[45];
    z[45]=z[45]*z[69];
    z[80]=z[13]*z[11];
    z[82]= - static_cast<T>(2)+ 9*z[80];
    z[45]=2*z[82] + z[45];
    z[45]=z[5]*z[45];
    z[82]= - z[86] - 6*z[60];
    z[82]=z[4]*z[82];
    z[21]=z[46] + z[82] + z[56] + z[74] + 2*z[21] + z[45];
    z[21]=z[7]*z[21];
    z[45]=4*z[2];
    z[46]=z[48] - z[45] + 19*z[3];
    z[56]=2*z[41];
    z[46]=z[46]*z[56];
    z[74]=z[2] - 7*z[3];
    z[74]=z[74]*z[65];
    z[82]=z[50]*z[2];
    z[46]=z[74] + 9*z[82] + z[46];
    z[46]=z[10]*z[46];
    z[74]= - static_cast<T>(9)+ 8*z[80];
    z[74]=z[5]*z[74];
    z[74]=z[74] - z[31] - 11*z[13];
    z[74]=z[5]*z[74];
    z[80]=z[13]*z[9];
    z[80]=z[80] - z[85];
    z[80]=z[6]*z[80];
    z[84]=z[79] + z[13];
    z[85]= - 10*z[3] + z[8] + z[84];
    z[80]=3*z[85] + z[80];
    z[80]=z[6]*z[80];
    z[85]= - z[71] + z[3];
    z[85]=z[4]*z[85];
    z[21]=z[21] + z[85] + z[46] + z[74] + z[80];
    z[21]=z[7]*z[21];
    z[46]= - z[50]*z[38];
    z[74]=z[2] + z[5];
    z[74]=z[5]*z[74];
    z[80]= - z[6]*z[2];
    z[46]=z[46] + z[74] + z[80];
    z[46]=z[10]*z[46];
    z[74]=z[5]*z[12];
    z[80]= - static_cast<T>(1)- z[74];
    z[80]=z[5]*z[80];
    z[46]=z[46] + z[6] + z[2] + z[80];
    z[46]=z[10]*z[46];
    z[18]= - z[18]*z[26];
    z[18]=z[16] + z[18];
    z[18]=z[5]*z[18];
    z[18]=z[12] + z[18];
    z[18]=z[5]*z[18];
    z[80]=z[28] - 1;
    z[18]=z[46] - z[33] + z[18] + z[80];
    z[18]=z[4]*z[18];
    z[46]= - z[79] - z[69];
    z[46]=z[5]*z[46];
    z[82]=z[23]*z[82];
    z[85]=z[2]*z[25];
    z[46]=z[82] + z[46] + z[85];
    z[46]=z[10]*z[46];
    z[29]=z[12] - z[29];
    z[82]=4*z[5];
    z[29]=z[29]*z[82];
    z[29]=z[29] + static_cast<T>(3)- 5*z[35];
    z[29]=z[5]*z[29];
    z[28]=static_cast<T>(5)- 4*z[28];
    z[28]=z[3]*z[28];
    z[18]=z[18] + z[46] - z[48] + z[29] - z[79] + z[28];
    z[18]=z[4]*z[18];
    z[28]= - z[8] + 9*z[2];
    z[28]=z[28]*z[50];
    z[29]= - z[45] - z[6];
    z[29]=z[29]*z[56];
    z[46]=z[2]*z[65];
    z[28]=z[46] + z[28] + z[29];
    z[28]=z[10]*z[28];
    z[29]=z[8] - z[45];
    z[45]= - static_cast<T>(5)- 18*z[37];
    z[45]=z[5]*z[45];
    z[29]=z[45] - z[83] + 3*z[29] + z[3];
    z[29]=z[5]*z[29];
    z[45]=z[12]*z[25];
    z[37]=z[45] + static_cast<T>(8)+ z[37];
    z[37]=z[6]*z[37];
    z[37]=3*z[84] + z[37];
    z[37]=z[6]*z[37];
    z[45]=z[3]*z[2];
    z[18]=z[18] + z[28] + z[37] - z[45] + z[29];
    z[18]=z[4]*z[18];
    z[28]= - z[75]*z[48];
    z[28]=static_cast<T>(2)+ z[28];
    z[28]=z[6]*z[28];
    z[29]=z[41]*z[10];
    z[28]= - z[29] + z[71] + z[28];
    z[28]=z[7]*z[28];
    z[37]=static_cast<T>(1)+ 2*z[59];
    z[46]=z[6]*z[9];
    z[37]=2*z[37] + z[46];
    z[37]=z[6]*z[37];
    z[37]=z[37] + z[8] + z[76];
    z[37]=z[6]*z[37];
    z[46]= - z[77] - z[48];
    z[46]=z[46]*z[41];
    z[39]=z[39]*z[3];
    z[48]=z[39]*z[10];
    z[56]=3*z[48];
    z[46]=z[46] + z[56];
    z[46]=z[10]*z[46];
    z[28]=z[28] + z[37] + z[46];
    z[28]=z[28]*z[54];
    z[37]=z[81] - z[66] + 22*z[3];
    z[37]=z[37]*z[41];
    z[28]=z[28] + z[37] - 16*z[48];
    z[28]=z[7]*z[28];
    z[37]= - z[45]*z[82];
    z[46]= - z[5]*z[72];
    z[46]=z[45] + z[46];
    z[46]=z[4]*z[46];
    z[37]=z[37] + z[46];
    z[37]=z[37]*npow(z[4],2);
    z[46]=z[8] - z[73];
    z[46]=2*z[46] - z[6];
    z[46]=z[46]*z[41];
    z[65]= - z[6]*z[68];
    z[65]= - z[8] + z[65];
    z[65]=z[7]*z[6]*z[65];
    z[46]=z[65] + z[46] + z[56];
    z[46]=z[7]*z[46];
    z[46]= - 3*z[39] + z[46];
    z[46]=z[46]*z[54];
    z[56]= - z[7]*z[8]*z[41];
    z[56]=z[39] + z[56];
    z[56]=z[56]*npow(z[7],2);
    z[65]= - z[5]*npow(z[4],3)*z[45];
    z[56]=z[65] + 2*z[56];
    z[56]=z[1]*z[56];
    z[37]=z[56] + z[37] + z[46];
    z[37]=z[1]*z[37];
    z[46]=z[38] - 1;
    z[56]= - z[35] - z[46];
    z[56]=z[5]*z[56];
    z[65]= - z[3]*z[80];
    z[56]= - z[2] + z[65] + z[56];
    z[56]=z[4]*z[56];
    z[65]=z[31]*z[3];
    z[68]=z[31] - 4*z[3];
    z[68]=z[5]*z[68];
    z[56]=z[56] + z[65] + z[68];
    z[56]=z[4]*z[56];
    z[68]= - z[5]*z[8];
    z[68]=6*z[45] + z[68];
    z[68]=z[5]*z[68];
    z[56]=z[68] + z[56];
    z[56]=z[4]*z[56];
    z[28]=z[37] + z[28] + 6*z[39] + z[56];
    z[28]=z[1]*z[28];
    z[37]= - z[8] - 12*z[13];
    z[37]=z[5]*z[37];
    z[37]=9*z[45] + z[37];
    z[37]=z[5]*z[37];
    z[35]=z[35]*z[25];
    z[39]= - z[35] - 12*z[3] + z[13] + z[8];
    z[39]=z[39]*z[41];
    z[18]=z[28] + z[21] + z[18] + 14*z[48] + z[37] + z[39];
    z[18]=z[1]*z[18];
    z[21]=z[6]*z[3];
    z[28]=z[5]*z[3];
    z[37]=z[28] + 5*z[21];
    z[37]=z[37]*z[41]*z[23];
    z[39]=z[66] + z[44];
    z[39]=z[39]*z[28];
    z[44]= - 9*z[3] - z[35];
    z[44]=z[6]*z[44];
    z[44]=4*z[28] + z[44];
    z[44]=z[44]*z[25];
    z[37]=z[37] + z[39] + z[44];
    z[37]=z[10]*z[37];
    z[39]= - z[35] - z[71] - z[83];
    z[39]=z[6]*z[39];
    z[44]=3*z[12];
    z[45]=z[61] - z[44];
    z[45]=z[13]*z[45];
    z[45]= - static_cast<T>(1)+ z[45];
    z[45]=z[45]*z[82];
    z[48]=z[8] + z[3];
    z[45]=z[45] + 4*z[48] - 5*z[13];
    z[45]=z[5]*z[45];
    z[48]= - z[14]*z[26];
    z[17]=z[18] + z[43] + z[17] + z[37] + z[39] + z[45] - z[65] + z[48];
    z[17]=z[1]*z[17];
    z[18]= - z[52]*z[66];
    z[37]=z[42]*z[69];
    z[37]= - z[49] + z[37];
    z[37]=z[5]*z[37];
    z[39]= - z[64] + static_cast<T>(2)+ z[59];
    z[39]=z[10]*z[39];
    z[18]=z[39] + z[37] + z[61] - z[9] + z[18];
    z[18]=z[10]*z[18];
    z[37]=z[13]*npow(z[11],5);
    z[32]= - 2*z[32] + z[37];
    z[32]=z[32]*z[69];
    z[32]=z[32] + 11*z[42] - 10*z[34];
    z[32]=z[5]*z[32];
    z[37]= - z[11]*z[51];
    z[37]=z[55] + z[37];
    z[37]=z[13]*z[37];
    z[39]=z[60] - 1;
    z[43]= - z[10]*z[39];
    z[43]=z[11] + z[43];
    z[43]=z[43]*z[63];
    z[45]= - z[9] - z[11];
    z[45]=z[11]*z[45];
    z[18]=z[43] + z[18] + z[32] + z[37] + z[45] + z[53] - z[57];
    z[18]=z[7]*z[18];
    z[32]= - 2*z[42] + z[34];
    z[32]=z[32]*z[69];
    z[32]=z[32] + 9*z[49] - 7*z[47];
    z[32]=z[5]*z[32];
    z[34]=4*z[11];
    z[37]= - 5*z[9] + z[34];
    z[37]=z[11]*z[37];
    z[37]= - z[52] + z[37];
    z[37]=z[13]*z[37];
    z[42]= - z[61] + z[70];
    z[42]=z[5]*z[42];
    z[42]=z[59] + z[42];
    z[42]=z[10]*z[42];
    z[39]= - z[39]*z[63];
    z[18]=z[18] + z[39] + z[42] + z[32] + z[37] - z[9] - z[34];
    z[18]=z[18]*z[54];
    z[32]=z[49]*z[83];
    z[34]= - 2*z[49] + z[47];
    z[34]=z[34]*z[24];
    z[32]=z[34] - z[61] + z[32];
    z[32]=z[5]*z[32];
    z[34]=z[50]*z[61];
    z[34]=z[66] + z[34];
    z[34]=z[10]*z[34];
    z[37]= - static_cast<T>(2)+ z[60];
    z[37]=z[4]*z[37]*z[23];
    z[39]=z[58] - z[11];
    z[39]=z[13]*z[39];
    z[18]=z[18] + z[37] + z[34] + z[32] + z[39] - static_cast<T>(5)- z[67];
    z[18]=z[7]*z[18];
    z[32]= - z[79] - z[6];
    z[32]=z[32]*z[41];
    z[32]=z[32] + z[40];
    z[32]=z[32]*z[23];
    z[34]=static_cast<T>(2)+ z[33];
    z[34]=z[34]*z[25];
    z[34]=z[31] + z[34];
    z[34]=z[6]*z[34];
    z[32]=z[34] + z[32];
    z[32]=z[10]*z[32];
    z[34]= - static_cast<T>(2)+ z[33];
    z[34]=z[34]*z[25];
    z[37]= - z[8] - z[2];
    z[39]= - z[12]*z[24];
    z[39]=static_cast<T>(3)+ z[39];
    z[39]=z[5]*z[39];
    z[32]=z[32] + z[34] + 2*z[37] + z[39];
    z[32]=z[10]*z[32];
    z[19]= - z[16] - z[19];
    z[19]=z[5]*z[19];
    z[16]= - z[14]*z[16];
    z[16]=z[19] + z[16] - z[22];
    z[19]= - z[25]*z[38];
    z[19]=z[6] + z[19];
    z[19]=z[10]*z[19];
    z[19]=z[19] - z[74] + z[33];
    z[19]=z[10]*z[19];
    z[22]= - z[10]*z[6]*z[46];
    z[22]= - z[33] + z[22];
    z[22]=z[22]*z[62];
    z[16]=z[22] + 2*z[16] + z[19];
    z[16]=z[4]*z[16];
    z[19]= - z[25] - z[15] - z[66];
    z[19]=z[12]*z[19];
    z[20]= - z[20]*z[24];
    z[20]=z[20] - z[12] - z[27];
    z[20]=z[5]*z[20];
    z[16]=z[16] + z[32] + z[20] - z[30] + z[19];
    z[16]=z[4]*z[16];
    z[19]= - z[3]*z[33];
    z[19]=z[19] - 3*z[72] - z[5];
    z[19]=z[6]*z[19];
    z[20]=z[3]*z[24];
    z[19]=z[20] + z[19];
    z[19]=z[6]*z[19];
    z[20]=z[28] + z[21];
    z[20]=z[20]*z[29];
    z[19]=z[19] + z[20];
    z[19]=z[10]*z[19];
    z[20]= - z[33] + 4;
    z[20]=z[3]*z[20];
    z[20]= - z[31] + z[20];
    z[20]=z[6]*z[20];
    z[21]= - z[8]*z[14];
    z[22]= - z[73] - z[5];
    z[22]=z[5]*z[22];
    z[19]=z[19] + z[20] + z[21] + z[22];
    z[19]=z[19]*z[23];
    z[20]=z[78] - z[44];
    z[20]=z[20]*z[26];
    z[21]=z[12] - z[11];
    z[22]= - z[12]*z[21];
    z[22]= - z[49] + z[22];
    z[22]=z[13]*z[22];
    z[21]= - 2*z[21] + z[22];
    z[21]=z[21]*z[24];
    z[20]=z[21] + z[20] - z[36];
    z[20]=z[5]*z[20];
    z[21]= - z[9]*z[14];
    z[22]= - z[12]*z[15];
    z[21]=z[22] - static_cast<T>(3)+ z[21];
    z[21]=z[21]*z[26];
    z[22]=z[9]*z[15];
    z[22]= - static_cast<T>(3)+ z[22];
    z[22]=z[8]*z[22];
    z[15]=z[17] + z[18] + z[16] + z[19] + z[35] + z[20] + z[21] + z[79]
    + z[15] + z[22];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r319(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r319(const std::array<dd_real,31>&);
#endif
