#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r669(const std::array<T,31>& k) {
  T z[88];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[15];
    z[12]=k[9];
    z[13]=npow(z[1],2);
    z[14]=z[13]*z[7];
    z[15]=2*z[1];
    z[16]=z[15] - z[14];
    z[16]=z[7]*z[16];
    z[17]= - z[4]*z[13];
    z[17]=z[15] + z[17];
    z[17]=z[3]*z[17];
    z[16]=z[16] + z[17];
    z[17]=z[2]*z[1];
    z[18]=4*z[1];
    z[19]= - z[6]*z[18];
    z[20]=3*z[13];
    z[21]=z[20]*z[2];
    z[22]= - z[4]*z[21];
    z[16]=z[22] + z[19] - 7*z[17] + 4*z[16];
    z[16]=z[3]*z[16];
    z[19]=npow(z[2],3);
    z[22]=npow(z[1],3);
    z[23]=z[22]*z[19];
    z[24]=npow(z[2],2);
    z[25]=npow(z[1],4);
    z[26]=z[24]*z[25];
    z[27]=z[13] + z[26];
    z[28]=2*z[6];
    z[29]=z[28]*z[2];
    z[27]=z[27]*z[29];
    z[23]=z[23] + z[27];
    z[23]=z[6]*z[23];
    z[27]=3*z[25];
    z[30]= - z[19]*z[27];
    z[31]=npow(z[1],5);
    z[32]=z[24]*z[31];
    z[32]=z[32] + z[22];
    z[29]= - z[32]*z[29];
    z[29]=z[30] + z[29];
    z[29]=z[6]*z[29];
    z[30]=z[22]*z[2];
    z[33]=z[4]*z[30]*z[28];
    z[29]=z[29] + z[33];
    z[29]=z[4]*z[29];
    z[33]=z[13]*z[2];
    z[34]=z[18] - z[33];
    z[34]=z[34]*z[24];
    z[35]=2*z[22];
    z[36]=z[35]*z[6];
    z[37]= - z[19]*z[36];
    z[37]= - z[34] + z[37];
    z[37]=z[6]*z[37];
    z[38]=2*z[13];
    z[39]=z[38]*z[10];
    z[19]=npow(z[6],2)*z[39]*z[19];
    z[37]=z[37] + z[19];
    z[37]=z[10]*z[37];
    z[40]=z[8]*z[3];
    z[41]=npow(z[7],2);
    z[42]= - z[41]*z[18]*z[40];
    z[43]=z[18] + z[33];
    z[43]=z[43]*z[24];
    z[44]=npow(z[7],3);
    z[45]=z[44]*z[13];
    z[46]=z[41] - z[24];
    z[46]=z[46]*z[1];
    z[46]=4*z[46];
    z[47]=z[9]*z[3];
    z[48]= - z[47]*z[46];
    z[16]=z[42] + z[48] + z[37] + z[16] + z[29] + z[23] - z[43] + z[45];
    z[16]=z[8]*z[16];
    z[23]= - z[10]*z[4];
    z[23]=z[23] + 1;
    z[29]=z[13]*z[5];
    z[23]=z[29]*z[23];
    z[37]=3*z[5];
    z[42]= - z[22]*z[37];
    z[42]= - z[13] + z[42];
    z[42]=z[2]*z[42];
    z[48]=z[22]*z[5];
    z[49]=z[48] - z[30];
    z[50]=3*z[6];
    z[49]=z[49]*z[50];
    z[51]=z[38] - z[48];
    z[51]=z[4]*z[51];
    z[23]=z[51] + z[49] + z[42] + z[23];
    z[23]=z[10]*z[23];
    z[42]=z[47]*z[17];
    z[47]= - z[2]*z[38];
    z[47]= - z[1] + z[47];
    z[47]=z[3]*z[47];
    z[47]=z[42] + 2*z[17] + z[47];
    z[47]=z[9]*z[47];
    z[49]=z[30] + z[13];
    z[51]=z[3]*z[49];
    z[52]= - z[4]*z[1];
    z[52]=z[17] + z[52];
    z[52]=z[10]*z[52];
    z[21]=z[47] + z[52] - z[21] + z[51];
    z[21]=z[9]*z[21];
    z[21]=z[23] + z[21];
    z[23]=z[37]*z[25];
    z[47]=z[35] - z[23];
    z[47]=z[2]*z[47];
    z[51]=z[25]*z[2];
    z[52]=z[25]*z[5];
    z[53]=z[52] - z[51];
    z[50]=z[53]*z[50];
    z[21]=z[50] - z[38] + z[47] + 2*z[21];
    z[21]=z[11]*z[21];
    z[47]= - z[15] - 5*z[33];
    z[50]=2*z[3];
    z[47]=z[47]*z[50];
    z[53]=3*z[1];
    z[54]= - z[4]*z[53];
    z[17]=7*z[42] + z[47] + 10*z[17] + z[54];
    z[17]=z[9]*z[17];
    z[42]=4*z[13];
    z[47]=z[42] - z[48];
    z[47]=z[47]*z[5];
    z[54]=z[42] + z[30];
    z[54]=z[2]*z[54];
    z[54]= - z[47] + z[54];
    z[54]=z[6]*z[54];
    z[55]=z[2]*z[48];
    z[55]=z[55] + z[1] + z[47];
    z[55]=z[2]*z[55];
    z[54]=z[55] + z[54];
    z[55]=2*z[5];
    z[56]=z[55]*z[13];
    z[57]= - z[53] - z[56];
    z[57]=z[4]*z[57];
    z[54]=3*z[54] + z[57];
    z[54]=z[10]*z[54];
    z[57]=8*z[22];
    z[23]=z[57] - z[23];
    z[23]=z[23]*z[5];
    z[58]=8*z[13];
    z[59]= - z[58] + z[23];
    z[59]=z[2]*z[59];
    z[60]= - z[23] + 8*z[30];
    z[60]=z[6]*z[60];
    z[61]=z[58] - 5*z[48];
    z[61]=z[4]*z[61];
    z[62]=z[38] + 3*z[30];
    z[62]=z[3]*z[62];
    z[17]=z[21] + z[17] + z[54] + z[62] + z[61] + z[59] + z[60];
    z[17]=z[11]*z[17];
    z[21]=z[31]*z[5];
    z[54]=z[21] - z[25];
    z[59]=npow(z[5],2);
    z[60]=z[54]*z[59];
    z[61]=3*z[60];
    z[62]=npow(z[1],6);
    z[63]=z[37]*z[62];
    z[63]= - z[63] + 5*z[31];
    z[63]=z[63]*z[5];
    z[64]=2*z[25];
    z[63]=z[63] - z[64];
    z[65]=z[2]*z[5];
    z[66]= - z[63]*z[65];
    z[67]=z[31]*z[2];
    z[68]=z[64] + z[67];
    z[69]= - z[7]*z[68];
    z[69]=z[51] + z[69];
    z[69]=z[7]*z[69];
    z[70]=z[62]*z[2];
    z[71]= - 2*z[31] - z[70];
    z[71]=z[7]*z[71];
    z[71]=z[67] + z[71];
    z[71]=z[7]*z[71];
    z[71]= - z[22] + z[71];
    z[71]=z[6]*z[71];
    z[66]=z[71] + z[69] + z[66] - z[13] + z[61];
    z[69]=2*z[4];
    z[66]=z[66]*z[69];
    z[71]=z[51] + z[35];
    z[72]=4*z[71];
    z[73]=z[72]*z[2];
    z[74]= - z[7]*z[25];
    z[72]=z[72] + z[74];
    z[72]=z[7]*z[72];
    z[23]=z[72] - z[73] - z[38] + z[23];
    z[23]=z[7]*z[23];
    z[72]= - z[27] - z[67];
    z[72]=z[2]*z[72];
    z[72]=z[22] + z[72];
    z[74]= - z[7]*z[31];
    z[68]=6*z[68] + z[74];
    z[68]=z[7]*z[68];
    z[68]=6*z[72] + z[68];
    z[68]=z[7]*z[68];
    z[70]= - z[31] - z[70];
    z[70]=z[7]*z[2]*z[70];
    z[32]=z[70] + z[32];
    z[32]=z[32]*z[28];
    z[70]=9*z[25] + z[67];
    z[70]=z[2]*z[70];
    z[57]= - z[57] + z[70];
    z[57]=z[2]*z[57];
    z[32]=z[32] + z[57] + z[68];
    z[32]=z[6]*z[32];
    z[57]=z[55]*z[31];
    z[68]= - z[57] + 7*z[25];
    z[68]=z[68]*z[5];
    z[68]=z[68] - 12*z[22];
    z[70]=z[62]*z[5];
    z[72]= - z[70] + 3*z[31];
    z[72]=z[72]*z[5];
    z[72]=z[72] - z[27];
    z[74]= - z[2]*z[72];
    z[74]=z[74] - z[68];
    z[74]=z[2]*z[74];
    z[75]= - 14*z[21] + 29*z[25];
    z[75]=z[75]*z[5];
    z[75]=z[75] - 10*z[22];
    z[75]=z[75]*z[5];
    z[75]=z[75] + z[20];
    z[74]=z[74] + z[75];
    z[74]=z[2]*z[74];
    z[76]=z[52] - z[22];
    z[77]=z[76]*z[59];
    z[23]=z[66] + z[32] + z[23] + z[74] - z[53] - 8*z[77];
    z[23]=z[4]*z[23];
    z[32]= - 7*z[70] + 16*z[31];
    z[32]=z[32]*z[5];
    z[32]=z[32] + z[64];
    z[32]=z[32]*z[5];
    z[64]=3*z[22];
    z[32]=z[32] - z[64];
    z[66]=z[7]*z[54];
    z[66]= - 2*z[66] + z[32];
    z[66]=z[7]*z[66];
    z[70]=z[70] - z[31];
    z[70]=z[70]*z[59];
    z[74]=z[5]*npow(z[1],7);
    z[62]= - z[74] + 2*z[62];
    z[62]=z[62]*z[5];
    z[62]=z[62] - z[31];
    z[74]=z[7]*z[5];
    z[78]=z[62]*z[74];
    z[78]=z[70] + z[78];
    z[78]=z[78]*z[28];
    z[79]= - z[27] + z[57];
    z[79]=z[5]*z[79];
    z[79]=4*z[22] + z[79];
    z[79]=z[79]*z[55];
    z[66]=z[78] + z[66] - 9*z[13] + z[79];
    z[66]=z[6]*z[66];
    z[78]=2*z[2];
    z[54]=z[54]*z[78];
    z[32]=z[54] + z[32];
    z[32]=z[2]*z[32];
    z[54]= - z[62]*z[65];
    z[54]=z[70] + z[54];
    z[54]=z[54]*z[69];
    z[60]=z[13] - z[60];
    z[32]=z[54] + 4*z[60] + z[32];
    z[32]=z[4]*z[32];
    z[54]= - 5*z[25] + z[57];
    z[54]=z[54]*z[37];
    z[57]=7*z[22];
    z[54]=z[57] + z[54];
    z[54]=z[5]*z[54];
    z[60]= - z[76]*z[78];
    z[62]=7*z[13];
    z[54]=z[60] - z[62] + z[54];
    z[54]=z[2]*z[54];
    z[60]=17*z[25] - 6*z[21];
    z[60]=z[5]*z[60];
    z[70]=5*z[22];
    z[60]= - z[70] + z[60];
    z[60]=z[5]*z[60];
    z[79]=4*z[25];
    z[80]=z[79] - z[21];
    z[80]=z[80]*z[5];
    z[81]= - z[64] + z[80];
    z[81]=z[7]*z[81];
    z[60]=z[81] + 15*z[13] + z[60];
    z[60]=z[7]*z[60];
    z[81]=z[4] - z[7];
    z[81]=z[76]*z[81];
    z[81]=z[38] + z[81];
    z[81]=z[81]*z[50];
    z[82]= - 13*z[1] + 14*z[29];
    z[32]=z[81] + z[32] + z[66] + z[60] + 2*z[82] + z[54];
    z[32]=z[3]*z[32];
    z[54]= - z[52] + 6*z[22];
    z[54]=z[54]*z[55];
    z[60]=z[27] - z[21];
    z[66]=z[60]*z[5];
    z[81]= - z[7]*z[66];
    z[81]=z[54] + z[81];
    z[81]=z[7]*z[81];
    z[82]=13*z[22];
    z[83]= - z[82] + 7*z[52];
    z[83]=z[83]*z[5];
    z[83]=z[83] + 13*z[13];
    z[83]=z[83]*z[5];
    z[84]= - 6*z[13] - z[30];
    z[78]=z[84]*z[78];
    z[78]=z[81] - z[83] + z[78];
    z[78]=z[7]*z[78];
    z[81]=z[37]*z[31];
    z[79]=z[81] - z[79];
    z[79]=z[79]*z[5];
    z[79]=z[79] + z[22];
    z[81]= - z[5]*z[79];
    z[84]= - z[22] - z[51];
    z[84]=z[2]*z[84];
    z[81]=z[81] + z[84];
    z[81]=z[7]*z[81];
    z[84]=3*z[77];
    z[85]= - z[13] + z[30];
    z[85]=z[2]*z[85];
    z[81]=z[81] + z[84] + z[85];
    z[81]=z[81]*z[28];
    z[85]=z[48] - z[13];
    z[59]=z[85]*z[59];
    z[86]=4*z[59];
    z[87]=z[20] + z[30];
    z[87]=z[2]*z[87];
    z[87]= - z[18] + z[87];
    z[87]=z[2]*z[87];
    z[78]=z[81] + z[78] + z[86] + z[87];
    z[78]=z[6]*z[78];
    z[79]=z[79]*z[65];
    z[71]=z[7]*z[71];
    z[71]= - z[30] + z[71];
    z[71]=z[7]*z[71];
    z[71]=z[71] + z[84] + z[79];
    z[71]=z[71]*z[69];
    z[60]= - z[60]*z[65];
    z[54]= - z[54] + z[60];
    z[54]=z[2]*z[54];
    z[54]= - z[83] + z[54];
    z[54]=z[2]*z[54];
    z[37]=z[7] - z[37];
    z[37]=z[22]*z[37];
    z[30]= - 2*z[30] + z[38] + z[37];
    z[30]=z[7]*z[30];
    z[33]=4*z[33];
    z[37]=z[47] + z[33];
    z[30]=3*z[37] + z[30];
    z[30]=z[7]*z[30];
    z[30]=z[71] + z[30] - z[86] + z[54];
    z[30]=z[4]*z[30];
    z[37]=z[2]*z[77];
    z[47]= - z[49]*z[41];
    z[37]=z[47] + z[59] + z[37];
    z[37]=z[4]*z[37];
    z[37]=z[45] + z[37];
    z[37]=z[37]*z[69];
    z[24]=z[22]*z[24];
    z[24]= - z[77] + z[24];
    z[24]=z[7]*z[24];
    z[24]=z[59] + z[24];
    z[24]=z[24]*z[28];
    z[24]=z[34] + z[24];
    z[24]=z[6]*z[24];
    z[19]= - z[19] + z[24] + z[37];
    z[19]=z[10]*z[19];
    z[24]= - z[44]*z[38];
    z[19]=z[19] + z[30] + z[78] + z[43] + z[24];
    z[19]=z[10]*z[19];
    z[24]=z[20] + z[48];
    z[24]=z[24]*z[5];
    z[21]= - z[21] + 6*z[25];
    z[21]=z[21]*z[5];
    z[21]=z[21] - z[82];
    z[21]=z[21]*z[5];
    z[21]=z[21] + z[58];
    z[30]=z[2]*z[21];
    z[30]=z[30] + 9*z[1] - 4*z[24];
    z[30]=z[2]*z[30];
    z[24]=z[1] - z[24];
    z[21]= - z[7]*z[21];
    z[21]=4*z[24] + z[21];
    z[21]=z[7]*z[21];
    z[24]=z[65] + z[74];
    z[24]=z[76]*z[24];
    z[34]=z[64] - z[52];
    z[34]=z[34]*z[5];
    z[37]=z[34] - z[38];
    z[38]=z[4] + z[6];
    z[37]=z[37]*z[38];
    z[24]=z[24] + z[37];
    z[24]=z[24]*z[50];
    z[37]=z[20] - z[48];
    z[37]=z[37]*z[5];
    z[15]=z[37] - z[15];
    z[37]=z[7]*z[20];
    z[37]=2*z[15] + z[37];
    z[37]=z[6]*z[37];
    z[15]=z[15]*z[69];
    z[15]=z[24] + z[15] + z[37] + z[30] + z[21];
    z[15]=z[3]*z[15];
    z[21]=z[25]*z[41];
    z[24]=z[31]*z[41];
    z[24]=z[22] + z[24];
    z[24]=z[6]*z[24];
    z[21]=z[24] + z[13] + z[21];
    z[21]=z[69]*z[7]*z[21];
    z[24]= - z[22]*z[44];
    z[27]= - z[44]*z[27];
    z[30]= - z[7]*z[36];
    z[27]=z[27] + z[30];
    z[27]=z[6]*z[27];
    z[21]=z[21] + z[24] + z[27];
    z[21]=z[4]*z[21];
    z[18]= - z[18] - z[14];
    z[18]=z[18]*z[41];
    z[24]= - z[4]*z[44]*z[35];
    z[18]=z[18] + z[24];
    z[18]=z[4]*z[18];
    z[24]=z[44]*npow(z[4],2)*z[39];
    z[18]=z[18] + z[24];
    z[18]=z[10]*z[18];
    z[15]=z[18] + z[15] + z[46] + z[21];
    z[15]=z[9]*z[15];
    z[18]= - z[7]*z[72];
    z[18]=z[18] - 4*z[51] + z[68];
    z[18]=z[7]*z[18];
    z[18]=z[18] + z[73] + z[75];
    z[18]=z[7]*z[18];
    z[21]=z[5]*z[63];
    z[24]=z[25] + z[67];
    z[24]=z[2]*z[24];
    z[21]=z[21] + z[24];
    z[21]=z[7]*z[21];
    z[21]=z[21] + z[61] - z[26];
    z[21]=z[21]*z[28];
    z[24]= - z[70] + 4*z[52];
    z[24]=z[5]*z[24];
    z[24]=z[20] + z[24];
    z[24]=z[24]*z[55];
    z[26]= - z[57] - z[51];
    z[26]=z[2]*z[26];
    z[26]= - z[42] + z[26];
    z[26]=z[2]*z[26];
    z[18]=z[21] + z[18] + z[24] + z[26];
    z[18]=z[6]*z[18];
    z[21]=z[7]*z[1];
    z[24]= - z[6]*z[1];
    z[24]=z[21] + z[24];
    z[24]=z[10]*z[24];
    z[26]= - z[40] + 2;
    z[26]=z[21]*z[26];
    z[27]= - z[1] - z[14];
    z[27]=z[3]*z[27];
    z[27]=z[27] - z[26];
    z[27]=z[8]*z[27];
    z[28]=z[3]*z[13];
    z[14]=z[27] + z[24] + z[14] + z[28];
    z[14]=z[8]*z[14];
    z[24]=z[10]*z[6];
    z[24]=z[24] + 1;
    z[24]=z[29]*z[24];
    z[27]=z[85]*z[6];
    z[24]=z[27] + z[24];
    z[24]=z[10]*z[24];
    z[13]=z[14] - z[13] + z[24];
    z[13]=z[12]*z[13];
    z[14]=z[1] + z[56];
    z[14]=z[6]*z[14];
    z[14]= - z[21] + z[14];
    z[14]=z[10]*z[14];
    z[21]= - z[6]*z[53];
    z[21]=z[21] + z[26];
    z[21]=z[8]*z[21];
    z[13]=2*z[13] + z[21] + 5*z[27] + z[14];
    z[13]=z[12]*z[13];
    z[14]=z[35] + z[80];
    z[14]=z[5]*z[14];
    z[21]= - z[35] + z[66];
    z[21]=z[2]*z[21];
    z[14]=z[21] - z[20] + z[14];
    z[14]=z[2]*z[14];
    z[20]= - 19*z[22] + 5*z[52];
    z[20]=z[5]*z[20];
    z[20]=z[58] + z[20];
    z[20]=z[5]*z[20];
    z[14]=z[14] + 6*z[1] + z[20];
    z[14]=z[2]*z[14];
    z[20]= - z[25]*z[55];
    z[20]=15*z[22] + z[20];
    z[20]=z[5]*z[20];
    z[21]=z[22] - z[66];
    z[21]=z[7]*z[21];
    z[20]=z[21] - z[62] + z[20];
    z[20]=z[7]*z[20];
    z[21]= - z[42] + z[34];
    z[21]=z[5]*z[21];
    z[20]=z[20] + 5*z[21] - z[33];
    z[20]=z[7]*z[20];
    z[21]= - z[1] - z[29];
    z[21]=z[5]*z[21];

    r += z[13] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20]
       + 8*z[21] + z[23] + z[32];
 
    return r;
}

template double qqb_2lNLC_r669(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r669(const std::array<dd_real,31>&);
#endif
