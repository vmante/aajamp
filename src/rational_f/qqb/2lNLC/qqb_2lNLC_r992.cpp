#include "qqb_2lNLC_rf_decl.hpp"

template<class T>
T qqb_2lNLC_r992(const std::array<T,31>& k) {
  T z[66];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=k[11];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[8];
    z[12]=k[9];
    z[13]=k[15];
    z[14]=k[2];
    z[15]=npow(z[12],2);
    z[16]=91*z[15];
    z[17]=npow(z[5],2);
    z[18]=n<T>(5,2)*z[17];
    z[19]=z[16] + z[18];
    z[19]=z[5]*z[19];
    z[20]=npow(z[7],2);
    z[21]=z[20]*z[5];
    z[22]=npow(z[11],3);
    z[23]=157*z[22];
    z[19]=n<T>(145,2)*z[21] - z[23] + z[19];
    z[19]=z[10]*z[19];
    z[24]=157*z[9];
    z[25]=z[24]*z[22];
    z[26]=z[5]*z[9];
    z[27]=z[26]*z[3];
    z[28]=223*z[5] - 317*z[7];
    z[28]=n<T>(1,2)*z[28] - 5*z[27];
    z[28]=z[3]*z[28];
    z[29]=z[4]*z[7];
    z[30]=npow(z[11],2);
    z[31]=z[26] - 1;
    z[32]=z[31]*z[5];
    z[33]=n<T>(913,2)*z[12] + 5*z[32];
    z[33]=z[5]*z[33];
    z[34]= - n<T>(1063,2)*z[5] - 5*z[7];
    z[34]=z[7]*z[34];
    z[19]= - 259*z[29] + z[28] + z[19] + z[34] + z[33] + n<T>(483,2)*z[30]
    + z[25];
    z[28]=23*z[10];
    z[33]=z[28]*z[5];
    z[34]=23*z[26];
    z[35]=static_cast<T>(3)- z[34];
    z[35]=2*z[35] - z[33];
    z[35]=z[6]*z[35];
    z[36]=z[7]*z[9];
    z[37]=z[36] - 1;
    z[38]=z[37]*z[4];
    z[39]=z[3]*z[36];
    z[35]=z[35] + n<T>(179,8)*z[38] - 46*z[5] - n<T>(113,8)*z[39];
    z[35]=z[6]*z[35];
    z[19]=n<T>(1,8)*z[19] + z[35];
    z[19]=z[6]*z[19];
    z[35]=npow(z[7],3);
    z[39]=npow(z[5],3);
    z[40]= - z[39] - z[35];
    z[41]=npow(z[3],2);
    z[42]=14*z[41];
    z[43]=z[7] + z[5];
    z[44]=z[43]*z[42];
    z[40]=n<T>(5,16)*z[40] + z[44];
    z[40]=z[3]*z[40];
    z[44]=npow(z[4],2);
    z[45]=z[44]*z[7];
    z[46]=n<T>(101,4)*z[35];
    z[45]= - z[46] + 75*z[45];
    z[47]=n<T>(1,4)*z[4];
    z[45]=z[45]*z[47];
    z[21]=z[21] + z[39];
    z[21]=z[21]*z[7];
    z[48]=z[43]*z[3];
    z[49]=z[7]*z[5];
    z[50]= - 2*z[49] + z[48];
    z[51]=6*z[29];
    z[50]=23*z[50] + z[51];
    z[52]=npow(z[6],2);
    z[50]=z[50]*z[52];
    z[40]=z[50] - z[45] + n<T>(5,8)*z[21] + z[40];
    z[40]=z[6]*z[40];
    z[50]=z[39] + n<T>(1,2)*z[35];
    z[53]=n<T>(25,2)*z[7];
    z[54]=13*z[5];
    z[55]=z[53] + z[54];
    z[56]=n<T>(13,2)*z[3] - z[55];
    z[56]=z[56]*z[44];
    z[57]=npow(z[3],3);
    z[50]=n<T>(3,2)*z[56] + n<T>(101,8)*z[50] + 3*z[57];
    z[50]=z[4]*z[50];
    z[56]=npow(z[4],3);
    z[56]= - z[46] + 75*z[56];
    z[57]=6*z[52];
    z[58]=z[4] + z[7];
    z[57]= - z[58]*z[57];
    z[56]=n<T>(1,4)*z[56] + z[57];
    z[56]=z[6]*z[56];
    z[57]=n<T>(1,2)*z[3];
    z[59]=n<T>(1,2)*z[7];
    z[60]= - z[57] + z[5] + z[59];
    z[60]=z[4]*z[60];
    z[61]=n<T>(1,2)*z[6];
    z[62]= - z[58]*z[61];
    z[63]=z[3]*z[5];
    z[60]=z[62] - n<T>(1,2)*z[63] + z[60];
    z[62]=npow(z[2],2);
    z[60]=z[60]*z[62];
    z[64]=n<T>(101,16)*z[39];
    z[65]=z[41]*z[5];
    z[65]= - z[64] + 3*z[65];
    z[65]=z[65]*z[3];
    z[50]=n<T>(33,4)*z[60] + z[56] + z[65] + z[50];
    z[50]=z[2]*z[50];
    z[44]=z[44]*z[63];
    z[44]=z[65] + n<T>(39,4)*z[44];
    z[44]=z[44]*z[4];
    z[51]=z[52]*z[51];
    z[45]= - z[45] + z[51];
    z[45]=z[6]*z[45];
    z[51]=z[4]*z[63];
    z[29]=z[6]*z[29];
    z[29]=z[51] + z[29];
    z[29]=z[29]*z[62];
    z[29]=n<T>(33,8)*z[29] - z[44] + z[45];
    z[29]=z[2]*z[29];
    z[42]=z[42]*z[49];
    z[45]=n<T>(5,16)*z[21];
    z[42]=z[42] - z[45];
    z[42]=z[42]*z[3];
    z[51]=z[7]*z[52]*z[63];
    z[51]= - z[42] - 23*z[51];
    z[51]=z[6]*z[51];
    z[29]=z[51] + z[29];
    z[29]=z[1]*z[29];
    z[29]=z[29] + z[50] + z[40] - z[42] - z[44];
    z[29]=z[1]*z[29];
    z[21]=z[10]*z[21];
    z[21]=z[39] - z[21];
    z[40]=n<T>(145,2)*z[5] - 53*z[7];
    z[40]=z[40]*z[20];
    z[21]=z[40] - n<T>(5,2)*z[21];
    z[40]=z[31]*z[17];
    z[37]=z[37]*z[59];
    z[37]= - 63*z[5] + z[37];
    z[37]=z[7]*z[37];
    z[37]=n<T>(1,2)*z[40] + z[37];
    z[42]=z[36] + z[26];
    z[42]=z[42]*z[3];
    z[43]= - z[42] - z[43];
    z[44]=14*z[3];
    z[43]=z[43]*z[44];
    z[37]=n<T>(5,8)*z[37] + z[43];
    z[37]=z[3]*z[37];
    z[42]=z[42] - z[5];
    z[28]= - z[49]*z[28];
    z[28]=6*z[38] + z[28] + 17*z[7] - 23*z[42];
    z[28]=z[6]*z[28];
    z[28]= - 23*z[48] + z[28];
    z[28]=z[6]*z[28];
    z[42]=75*z[4];
    z[38]= - z[7] - z[38];
    z[38]=z[38]*z[42];
    z[43]=z[9]*z[46];
    z[38]=z[43] + z[38];
    z[38]=z[38]*z[47];
    z[21]=z[28] + z[38] + n<T>(1,8)*z[21] + z[37];
    z[21]=z[6]*z[21];
    z[28]=z[39] - z[35];
    z[28]=z[10]*z[28];
    z[37]=z[3]*z[8];
    z[38]=z[37] - 1;
    z[43]=3*z[3];
    z[47]=z[38]*z[43];
    z[47]=n<T>(163,16)*z[5] + z[47];
    z[47]=z[3]*z[47];
    z[48]=25*z[7];
    z[50]=z[48] - z[54];
    z[51]=n<T>(1,2)*z[10];
    z[52]=z[50]*z[51];
    z[52]=n<T>(13,2)*z[37] + static_cast<T>(19)+ z[52];
    z[52]=z[4]*z[52];
    z[52]= - z[53] + z[52];
    z[53]=n<T>(3,2)*z[4];
    z[52]=z[52]*z[53];
    z[28]=z[52] + n<T>(101,16)*z[28] + z[47];
    z[28]=z[4]*z[28];
    z[47]= - z[31]*z[57];
    z[52]=z[5] - z[7];
    z[52]=z[52]*z[51];
    z[52]= - n<T>(1,2)*z[37] - static_cast<T>(1)+ z[52];
    z[52]=z[4]*z[52];
    z[56]=z[10]*z[7];
    z[60]=z[4]*z[8];
    z[56]=z[56] + z[60] + 1;
    z[61]=z[56]*z[61];
    z[47]=z[61] + z[52] - z[5] + z[47];
    z[47]=z[2]*z[47];
    z[52]=49*z[5];
    z[61]=11*z[6] + z[52];
    z[61]=z[4]*z[61];
    z[61]=11*z[63] + z[61];
    z[47]=n<T>(1,2)*z[61] + 11*z[47];
    z[61]=n<T>(3,4)*z[2];
    z[47]=z[47]*z[61];
    z[62]=static_cast<T>(1)- z[60];
    z[42]=z[62]*z[42];
    z[42]=n<T>(163,4)*z[7] + z[42];
    z[42]=z[4]*z[42];
    z[46]=z[10]*z[46];
    z[42]=z[46] + z[42];
    z[46]=z[6]*z[56];
    z[46]=z[46] + z[58];
    z[56]=6*z[6];
    z[46]=z[46]*z[56];
    z[42]=n<T>(1,4)*z[42] + z[46];
    z[42]=z[6]*z[42];
    z[41]=3*z[41];
    z[46]=z[31]*z[41];
    z[58]=z[64]*z[9];
    z[46]= - z[58] + z[46];
    z[46]=z[3]*z[46];
    z[28]=z[47] + z[42] + z[28] - n<T>(101,8)*z[39] + z[46];
    z[28]=z[2]*z[28];
    z[35]=n<T>(101,2)*z[35] + z[23] + 101*z[39];
    z[39]=static_cast<T>(2)- z[26];
    z[39]=z[39]*z[41];
    z[39]=z[58] + z[39];
    z[39]=z[3]*z[39];
    z[41]=static_cast<T>(1)- n<T>(1,2)*z[26];
    z[41]=z[3]*z[41];
    z[41]=13*z[41] - z[55];
    z[41]=z[4]*z[41];
    z[41]= - n<T>(13,2)*z[63] + z[41];
    z[41]=z[41]*z[53];
    z[35]=z[41] + n<T>(1,8)*z[35] + z[39];
    z[35]=z[4]*z[35];
    z[39]=z[26]*z[7];
    z[39]=z[5] - z[39];
    z[39]=z[7]*z[39];
    z[39]= - z[40] + z[39];
    z[40]=n<T>(5,16)*z[7];
    z[39]=z[39]*z[40];
    z[27]=z[7]*z[27];
    z[27]=z[49] + z[27];
    z[27]=z[27]*z[44];
    z[27]=z[39] + z[27];
    z[27]=z[3]*z[27];
    z[21]=z[29] + z[28] + z[21] + z[35] + z[45] + z[27];
    z[21]=z[1]*z[21];
    z[27]=z[22]*z[7];
    z[28]=npow(z[13],3);
    z[29]=z[28]*z[5];
    z[27]=z[27] + z[29];
    z[29]=157*z[10];
    z[29]=z[27]*z[29];
    z[35]=101*z[17];
    z[39]=npow(z[13],2);
    z[40]= - 889*z[39] + z[35];
    z[41]=n<T>(1,2)*z[5];
    z[40]=z[40]*z[41];
    z[42]= - 227*z[30] - 101*z[20];
    z[42]=z[42]*z[59];
    z[40]= - z[29] + z[42] + z[23] + z[40];
    z[40]=z[10]*z[40];
    z[35]=z[35]*z[9];
    z[42]= - n<T>(1233,2)*z[13] + z[35];
    z[42]=z[5]*z[42];
    z[44]=z[20]*z[9];
    z[45]= - 359*z[11] + n<T>(101,2)*z[44];
    z[45]=z[7]*z[45];
    z[31]=z[3]*z[31];
    z[31]= - 97*z[5] + 71*z[31];
    z[31]=z[3]*z[31];
    z[31]=z[31] + z[40] + z[45] + z[42] - n<T>(1111,2)*z[30] - z[25];
    z[40]=n<T>(1,4)*z[10];
    z[42]=z[50]*z[40];
    z[42]=z[42] - n<T>(25,4)*z[36] + static_cast<T>(19)- n<T>(13,2)*z[26];
    z[42]=z[4]*z[42];
    z[45]= - 19*z[3] - z[54] - z[48];
    z[42]=n<T>(1,2)*z[45] + z[42];
    z[45]=3*z[4];
    z[42]=z[42]*z[45];
    z[31]=n<T>(1,8)*z[31] + z[42];
    z[31]=z[4]*z[31];
    z[24]=z[24]*z[28];
    z[42]=n<T>(157,2)*z[28];
    z[46]=z[10]*z[42];
    z[46]=z[46] + n<T>(63,4)*z[17] + n<T>(411,4)*z[39] + z[24];
    z[47]= - 161*z[30] - 45*z[20];
    z[48]=z[23]*z[10];
    z[47]=n<T>(3,2)*z[47] + z[48];
    z[47]=z[47]*z[51];
    z[47]=z[47] + n<T>(143,4)*z[11] + 89*z[7];
    z[55]= - n<T>(701,16) + 14*z[60];
    z[55]=z[55]*z[45];
    z[58]=z[10] + z[8];
    z[58]=z[58]*z[6];
    z[62]= - static_cast<T>(2)- z[58];
    z[62]=z[62]*z[56];
    z[47]=z[62] + n<T>(1,4)*z[47] + z[55];
    z[47]=z[6]*z[47];
    z[55]=n<T>(411,2)*z[39] + z[24];
    z[55]=z[9]*z[55];
    z[55]=n<T>(35,2)*z[13] + z[55];
    z[62]=z[9] + z[8];
    z[63]=z[62]*z[3];
    z[64]=static_cast<T>(1)- z[63];
    z[64]=z[64]*z[43];
    z[65]=static_cast<T>(11)+ n<T>(63,16)*z[26];
    z[65]=z[5]*z[65];
    z[55]=z[64] + n<T>(1,8)*z[55] + z[65];
    z[55]=z[3]*z[55];
    z[58]= - z[58] + static_cast<T>(1)+ z[63];
    z[58]=z[2]*z[58];
    z[58]=z[58] - z[6];
    z[63]=z[52] - 37*z[7];
    z[63]=z[63]*z[51];
    z[63]= - static_cast<T>(43)+ z[63];
    z[63]=z[4]*z[63];
    z[52]= - z[52] - 11*z[3];
    z[52]=n<T>(1,2)*z[52] + z[63] + n<T>(11,2)*z[58];
    z[52]=z[52]*z[61];
    z[58]= - n<T>(369,2)*z[3] - 95*z[5] - 99*z[7];
    z[61]=z[4]*z[37];
    z[58]=n<T>(1,4)*z[58] - 19*z[61];
    z[58]=z[58]*z[53];
    z[46]=z[52] + z[47] + z[58] + n<T>(1,4)*z[46] + z[55];
    z[46]=z[2]*z[46];
    z[47]= - static_cast<T>(341)- 73*z[26];
    z[47]=z[47]*z[41];
    z[52]=n<T>(5,2)*z[7];
    z[55]=z[9] - z[8];
    z[58]=z[7]*z[55];
    z[58]= - static_cast<T>(1)+ z[58];
    z[58]=z[58]*z[52];
    z[61]=z[15]*z[8];
    z[63]= - n<T>(5,2)*z[12] - 11*z[61];
    z[47]=z[58] + 5*z[63] + z[47];
    z[47]=z[7]*z[47];
    z[47]=55*z[15] + z[47];
    z[58]=14*z[7];
    z[55]= - z[55]*z[58];
    z[55]= - static_cast<T>(17)+ z[55];
    z[55]=z[3]*z[55];
    z[55]= - 28*z[7] + z[55];
    z[55]=z[3]*z[55];
    z[47]=n<T>(1,8)*z[47] + z[55];
    z[47]=z[3]*z[47];
    z[55]=145*z[5] + 101*z[7];
    z[20]=z[55]*z[20];
    z[19]=z[21] + z[46] + z[19] + z[31] + n<T>(1,16)*z[20] + z[47];
    z[19]=z[1]*z[19];
    z[20]=z[28]*z[9];
    z[21]=z[10]*z[22];
    z[21]=z[20] - z[21];
    z[31]=29*z[39];
    z[21]= - n<T>(1053,16)*z[30] - z[31] - n<T>(157,4)*z[21];
    z[21]=z[10]*z[21];
    z[46]= - n<T>(1301,4)*z[39] - z[24];
    z[46]=z[9]*z[46];
    z[46]= - n<T>(291,4)*z[13] + z[46];
    z[46]=z[9]*z[46];
    z[46]=n<T>(227,4)*z[26] + n<T>(751,4) + z[46];
    z[38]=z[38]*z[9];
    z[43]= - z[38]*z[43];
    z[43]=n<T>(1,4)*z[46] + z[43];
    z[43]=z[3]*z[43];
    z[46]= - n<T>(1517,4)*z[30] + z[48];
    z[46]=z[10]*z[46];
    z[47]=75*z[11] - 119*z[7];
    z[46]=n<T>(1,4)*z[47] + z[46];
    z[46]=z[10]*z[46];
    z[46]= - n<T>(357,4)*z[60] + n<T>(1103,4) + z[46];
    z[47]=z[6]*z[10];
    z[55]=z[47] + 1;
    z[60]=z[10] - z[8];
    z[55]=z[55]*z[60];
    z[56]= - z[55]*z[56];
    z[46]=n<T>(1,4)*z[46] + z[56];
    z[46]=z[6]*z[46];
    z[38]= - z[55] + z[38];
    z[38]=z[2]*z[38];
    z[38]=z[38] + z[47] - z[37];
    z[38]=static_cast<T>(8)+ n<T>(11,8)*z[38];
    z[38]=z[2]*z[38];
    z[55]= - n<T>(1765,8)*z[39] - z[24];
    z[55]=z[9]*z[55];
    z[56]= - n<T>(21,2)*z[11] - 59*z[13];
    z[55]=n<T>(385,8)*z[7] + 51*z[5] + n<T>(3,4)*z[56] + z[55];
    z[56]=n<T>(17,4)*z[5] - 2*z[7];
    z[56]=z[10]*z[56];
    z[37]=n<T>(83,16)*z[37] - n<T>(101,8) + z[56];
    z[37]=z[37]*z[45];
    z[21]=3*z[38] + z[46] + z[37] + z[43] + n<T>(1,2)*z[55] + z[21];
    z[21]=z[2]*z[21];
    z[37]= - n<T>(1,2)*z[22] + z[28];
    z[38]=z[39]*z[5];
    z[43]=z[30]*z[7];
    z[37]= - z[29] + n<T>(507,4)*z[43] + 157*z[37] - n<T>(1479,4)*z[38];
    z[37]=z[10]*z[37];
    z[35]= - 1107*z[13] + z[35];
    z[35]=z[5]*z[35];
    z[35]=z[35] - 691*z[30] + 1315*z[39];
    z[45]=n<T>(1,4)*z[7];
    z[44]=355*z[11] - 101*z[44];
    z[44]=z[44]*z[45];
    z[35]=z[37] + z[44] - z[25] + n<T>(1,4)*z[35];
    z[35]=z[35]*z[40];
    z[37]= - 13*z[26] + 25*z[36];
    z[37]=z[4]*z[37];
    z[37]=z[37] + z[50];
    z[37]=z[51]*z[37];
    z[37]= - static_cast<T>(9)- n<T>(25,2)*z[36] + z[37];
    z[37]=z[37]*z[53];
    z[44]=n<T>(1,8)*z[3];
    z[46]= - static_cast<T>(167)+ 259*z[26];
    z[46]=z[46]*z[44];
    z[50]=z[30]*z[9];
    z[53]=n<T>(285,8)*z[50];
    z[35]=z[37] + z[46] + z[35] - 66*z[7] - n<T>(157,4)*z[5] + z[53] + n<T>(705,16)*z[11] + 29*z[13];
    z[35]=z[4]*z[35];
    z[16]= - z[16] + n<T>(1517,2)*z[30];
    z[32]=939*z[12] + n<T>(5,2)*z[32];
    z[32]=z[32]*z[41];
    z[18]=257*z[15] + z[18];
    z[18]=z[18]*z[41];
    z[18]= - z[23] + z[18];
    z[18]=z[10]*z[18];
    z[16]=z[18] - n<T>(829,4)*z[49] + z[32] + n<T>(1,2)*z[16] + z[25];
    z[16]=z[10]*z[16];
    z[18]=n<T>(195,2)*z[36] - static_cast<T>(175)+ n<T>(303,2)*z[26];
    z[18]=z[18]*z[57];
    z[23]=n<T>(285,2)*z[50];
    z[32]= - n<T>(691,2) + 313*z[36];
    z[32]=z[4]*z[32];
    z[16]=n<T>(1,2)*z[32] + z[18] + z[16] + n<T>(821,4)*z[7] + n<T>(1055,4)*z[5] - 
    z[23] - n<T>(389,2)*z[12] - 105*z[11];
    z[18]= - z[33] + static_cast<T>(29)- z[34];
    z[18]=z[18]*z[47];
    z[18]=z[18] + n<T>(193,4) - z[33];
    z[18]=z[6]*z[18];
    z[16]=n<T>(1,4)*z[16] + z[18];
    z[16]=z[6]*z[16];
    z[17]=53*z[17] + 221*z[15] + n<T>(889,2)*z[39];
    z[17]=z[5]*z[17];
    z[17]=z[29] + n<T>(227,2)*z[43] - 157*z[28] + z[17];
    z[17]=z[17]*z[40];
    z[18]= - 11*z[15] - n<T>(115,2)*z[39];
    z[32]= - 111*z[5] + 457*z[12] + n<T>(787,2)*z[13];
    z[32]=z[5]*z[32];
    z[18]=z[32] + 5*z[18] + z[24];
    z[32]= - 53*z[8] - n<T>(5,2)*z[9];
    z[32]=z[32]*z[59];
    z[32]= - static_cast<T>(29)+ z[32];
    z[32]=z[32]*z[59];
    z[32]=z[32] - n<T>(463,8)*z[5] + n<T>(23,2)*z[11] - 49*z[12] - n<T>(185,4)*z[61]
   ;
    z[32]=z[7]*z[32];
    z[17]=z[17] + n<T>(1,4)*z[18] + z[32];
    z[18]=z[9]*z[8];
    z[32]=npow(z[8],2);
    z[18]=z[18] + z[32];
    z[32]=z[7]*z[18];
    z[32]= - z[8] + z[32];
    z[32]=z[32]*z[52];
    z[33]= - 29*z[12] + 149*z[61];
    z[33]=z[8]*z[33];
    z[32]=z[32] + n<T>(721,2)*z[26] - static_cast<T>(209)+ z[33];
    z[32]=z[32]*z[59];
    z[33]=n<T>(875,4)*z[39] + z[24];
    z[33]=z[9]*z[33];
    z[34]=z[12]*z[14];
    z[37]=65*z[34];
    z[41]= - n<T>(251,2) + z[37];
    z[41]=z[12]*z[41];
    z[32]=z[32] - n<T>(127,4)*z[5] + z[33] - n<T>(57,4)*z[13] + z[41] - n<T>(149,2)*
    z[61];
    z[33]=z[62]*z[58];
    z[18]= - z[18]*z[58];
    z[18]=17*z[8] + z[18];
    z[18]=z[3]*z[18];
    z[18]=z[18] + n<T>(101,4) + z[33];
    z[18]=z[3]*z[18];
    z[18]=n<T>(1,4)*z[32] + z[18];
    z[18]=z[3]*z[18];
    z[16]=z[19] + z[21] + z[16] + z[35] + n<T>(1,2)*z[17] + z[18];
    z[16]=z[1]*z[16];
    z[17]=z[13]*z[14];
    z[18]=static_cast<T>(69)- n<T>(157,2)*z[17];
    z[18]=z[18]*z[39];
    z[15]=83*z[15];
    z[18]=z[15] + z[18];
    z[18]=z[5]*z[18];
    z[19]=n<T>(157,2)*z[22];
    z[21]=z[19]*z[9];
    z[32]= - 105*z[30] + z[21];
    z[32]=z[7]*z[32];
    z[33]=n<T>(157,2)*z[10];
    z[27]=z[27]*z[33];
    z[18]=z[27] + z[32] - z[42] + z[18];
    z[18]=z[10]*z[18];
    z[27]=n<T>(1,2)*z[39];
    z[32]=157*z[17];
    z[33]=static_cast<T>(19)+ z[32];
    z[33]=z[33]*z[27];
    z[35]=z[37] + n<T>(213,2);
    z[35]=z[35]*z[12];
    z[41]=n<T>(107,4) - 41*z[17];
    z[41]=z[13]*z[41];
    z[41]=z[35] + z[41];
    z[41]=z[5]*z[41];
    z[23]= - z[23] - n<T>(25,4)*z[11] - n<T>(137,2)*z[12] - 65*z[61];
    z[23]=z[7]*z[23];
    z[18]=z[18] + z[23] + z[41] + n<T>(157,2)*z[20] - z[15] + z[33];
    z[18]=z[18]*z[40];
    z[23]= - z[22] + z[28];
    z[23]= - z[29] + 367*z[43] + 157*z[23] - 295*z[38];
    z[23]=z[23]*z[51];
    z[29]=35*z[30] + 23*z[39];
    z[22]= - z[22] - z[28];
    z[22]=z[9]*z[22];
    z[28]=z[5]*z[13];
    z[33]=z[7]*z[11];
    z[22]=z[23] - 247*z[33] - 121*z[28] + 3*z[29] + n<T>(157,2)*z[22];
    z[22]=z[22]*z[40];
    z[23]=95*z[30] - 71*z[39];
    z[23]=z[9]*z[23];
    z[28]=25*z[11] - 97*z[13];
    z[23]=n<T>(1,2)*z[28] + 3*z[23];
    z[23]=n<T>(1,2)*z[23] - z[54];
    z[22]=z[22] + n<T>(1,4)*z[23] + 10*z[7];
    z[22]=z[10]*z[22];
    z[23]= - 95*z[11] - 71*z[13];
    z[23]=z[9]*z[23];
    z[23]=15*z[36] - 3*z[26] - static_cast<T>(167)+ n<T>(3,2)*z[23];
    z[22]=n<T>(1,8)*z[23] + z[22];
    z[22]=z[4]*z[22];
    z[23]=z[5]*z[15];
    z[19]= - z[19] + z[23];
    z[19]=z[10]*z[19];
    z[23]=z[5]*z[12];
    z[15]=z[19] + n<T>(361,2)*z[23] + z[21] - z[15] + n<T>(517,2)*z[30];
    z[15]=z[15]*z[40];
    z[15]=z[15] + n<T>(103,4)*z[5] - z[53] - n<T>(1165,16)*z[11] - 28*z[12] - n<T>(9,2)*z[61];
    z[15]=z[10]*z[15];
    z[19]=z[9]*z[11];
    z[21]= - z[8]*z[12];
    z[21]=n<T>(13,2) + z[21];
    z[21]=3*z[21] + n<T>(95,4)*z[19];
    z[21]=3*z[21] + n<T>(103,4)*z[26];
    z[15]=n<T>(1,4)*z[21] + z[15];
    z[15]=z[6]*z[15];
    z[20]=n<T>(157,8)*z[20];
    z[21]=z[20] + n<T>(285,8)*z[30] + z[31];
    z[21]=z[9]*z[21];
    z[23]= - z[48] + 517*z[30];
    z[25]= - z[25] - z[23];
    z[25]=z[10]*z[25];
    z[26]=233*z[11] + 61*z[13];
    z[21]=n<T>(1,8)*z[25] + n<T>(5,16)*z[26] + z[21];
    z[21]=z[10]*z[21];
    z[25]=n<T>(677,2)*z[39] + z[24];
    z[25]=z[9]*z[25];
    z[26]= - n<T>(285,2)*z[11] + 673*z[13];
    z[25]=n<T>(1,2)*z[26] + z[25];
    z[25]=z[9]*z[25];
    z[25]= - n<T>(71,2) + z[25];
    z[26]=445*z[39] + z[24];
    z[26]=z[9]*z[26];
    z[26]=627*z[13] + z[26];
    z[26]=z[9]*z[26];
    z[26]=n<T>(517,2) + z[26];
    z[26]=z[9]*z[26];
    z[26]= - n<T>(149,2)*z[8] + z[26];
    z[26]=z[26]*z[44];
    z[23]= - z[10]*z[23];
    z[23]=879*z[11] + z[23];
    z[23]=z[10]*z[23];
    z[23]= - static_cast<T>(405)+ z[23];
    z[23]=z[10]*z[23];
    z[23]=n<T>(185,2)*z[8] + z[23];
    z[23]=z[6]*z[23];
    z[21]=n<T>(1,8)*z[23] + z[26] + n<T>(1,4)*z[25] + z[21];
    z[21]=z[2]*z[21];
    z[23]=47*z[61];
    z[25]=n<T>(191,2) - z[37];
    z[25]=z[12]*z[25];
    z[25]=z[25] + z[23];
    z[25]=z[8]*z[25];
    z[25]=n<T>(305,4)*z[17] + z[25] + static_cast<T>(11)- n<T>(137,2)*z[34];
    z[26]= - static_cast<T>(677)+ z[32];
    z[26]=z[26]*z[27];
    z[24]=z[26] - z[24];
    z[24]=z[9]*z[24];
    z[26]= - n<T>(673,8) + 29*z[17];
    z[26]=z[13]*z[26];
    z[24]=z[26] + n<T>(1,4)*z[24];
    z[24]=z[9]*z[24];
    z[23]= - n<T>(173,2)*z[12] - z[23];
    z[23]=z[8]*z[23];
    z[23]= - static_cast<T>(99)+ z[23];
    z[23]=z[8]*z[23];
    z[23]=z[23] - n<T>(171,4)*z[9];
    z[23]=z[23]*z[45];
    z[23]=z[23] + n<T>(1,4)*z[25] + z[24];
    z[23]=z[3]*z[23];
    z[24]= - n<T>(29,2) - 15*z[17];
    z[24]=z[13]*z[24];
    z[24]=n<T>(5,2)*z[24] - z[35] - 83*z[61];
    z[25]= - static_cast<T>(22)- n<T>(157,8)*z[17];
    z[25]=z[25]*z[39];
    z[20]=z[25] - z[20];
    z[20]=z[9]*z[20];
    z[17]= - n<T>(155,2)*z[17] - n<T>(235,2) + 137*z[34];
    z[17]=z[5]*z[17];
    z[25]=41*z[12] + 28*z[61];
    z[25]=z[8]*z[25];
    z[19]=n<T>(285,16)*z[19] + n<T>(527,16) + z[25];
    z[19]=z[7]*z[19];
    z[15]=z[16] + z[21] + z[15] + z[22] + z[23] + z[18] + z[19] + n<T>(1,8)*
    z[17] + n<T>(1,4)*z[24] + z[20];

    r += z[15]*z[1];
 
    return r;
}

template double qqb_2lNLC_r992(const std::array<double,31>&);

#ifdef HAVE_QD
template dd_real qqb_2lNLC_r992(const std::array<dd_real,31>&);
#endif
