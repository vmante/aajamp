#include "qqb_1l_rf_decl.hpp"

template<class T>
T qqb_1l_r36(const std::array<T,31>& k) {
  T z[22];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[8];
    z[7]=k[2];
    z[8]=k[11];
    z[9]=k[3];
    z[10]=k[4];
    z[11]=npow(z[10],2);
    z[12]=2*z[8];
    z[12]=z[11]*z[12];
    z[13]=z[2]*z[8];
    z[14]=z[13] - 1;
    z[15]=z[8]*z[10];
    z[16]=z[14] - z[15];
    z[17]=2*z[2];
    z[18]=z[16]*z[17];
    z[17]=z[14]*z[17];
    z[17]=z[1] + z[17];
    z[19]=z[4]*z[2];
    z[17]=z[17]*z[19];
    z[20]=2*z[10];
    z[12]=z[17] + z[18] + z[12] + z[20] + z[1];
    z[12]=z[3]*z[12];
    z[11]=z[8]*z[11];
    z[11]=z[11] + z[10] + z[1];
    z[17]=2*z[6];
    z[11]=z[17]*z[11];
    z[15]= - z[17]*z[15];
    z[18]=z[6]*z[13];
    z[15]=z[15] + z[18];
    z[15]=z[2]*z[15];
    z[16]= - z[2]*z[16];
    z[18]=2*z[1];
    z[16]=z[16] - z[10] - z[18];
    z[16]=z[4]*z[16];
    z[19]= - z[3]*z[19];
    z[19]=z[19] + z[4];
    z[21]=npow(z[1],2);
    z[19]=z[21]*z[19];
    z[19]=z[1] + z[19];
    z[19]=z[5]*z[19];
    z[11]=z[19] + z[12] + z[16] + z[15] - static_cast<T>(1)+ z[11];
    z[11]=z[5]*z[11];
    z[12]= - z[1]*z[17];
    z[15]=z[8]*z[9];
    z[16]= - z[6] + z[8];
    z[16]=z[2]*z[16];
    z[12]=z[16] + z[15] + static_cast<T>(1)+ z[12];
    z[12]=z[2]*z[12];
    z[15]= - z[6]*z[21];
    z[15]=z[15] - z[9] + z[18];
    z[12]=2*z[15] + z[12];
    z[12]=z[4]*z[12];
    z[15]=z[9] - z[20];
    z[15]=z[8]*z[15];
    z[12]=z[12] + z[13] - static_cast<T>(2)+ z[15];
    z[12]=z[3]*z[12];
    z[13]=z[7] - z[20];
    z[13]=z[6]*z[13];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[8]*z[13];
    z[13]= - z[6] + z[13];
    z[14]= - z[4]*z[14];

    r += z[11] + z[12] + 2*z[13] + z[14];
 
    return r;
}

template double qqb_1l_r36(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1l_r36(const std::array<dd_real,31>&);
#endif
