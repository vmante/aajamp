#include "qqb_1l_rf_decl.hpp"

template<class T>
T qqb_1l_r4(const std::array<T,31>& k) {
  T z[50];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[5];
    z[8]=k[12];
    z[9]=k[6];
    z[10]=k[14];
    z[11]=k[4];
    z[12]=4*z[7];
    z[13]=npow(z[3],2);
    z[14]=npow(z[13],2);
    z[15]=z[12]*z[14];
    z[16]=npow(z[3],3);
    z[17]=7*z[16];
    z[15]=z[15] + z[17];
    z[18]=2*z[2];
    z[19]=z[18]*z[14];
    z[20]=6*z[5];
    z[20]=z[20]*z[14];
    z[21]= - z[19] + z[20] + z[15];
    z[21]=z[21]*z[18];
    z[22]=2*z[7];
    z[23]=z[22]*z[14];
    z[24]=2*z[5];
    z[25]=z[24]*z[14];
    z[26]=z[25] + z[16];
    z[27]=4*z[2];
    z[28]= - z[14]*z[27];
    z[28]=z[28] + z[23] + z[26];
    z[19]=z[19] - z[26];
    z[19]=z[19]*z[18];
    z[26]=z[26]*z[24];
    z[26]=z[26] + z[13];
    z[29]= - z[6]*z[5];
    z[29]=z[29] + 1;
    z[26]=z[26]*z[29];
    z[19]=z[19] + z[26];
    z[19]=z[4]*z[19];
    z[19]=2*z[28] + z[19];
    z[19]=z[1]*z[19];
    z[26]=3*z[5];
    z[28]=z[26]*z[14];
    z[29]=z[28] + 4*z[16];
    z[30]=z[5]*z[29];
    z[31]= - z[16] - z[28];
    z[31]=z[2]*z[31];
    z[30]=z[31] + z[13] + z[30];
    z[27]=z[30]*z[27];
    z[30]=4*z[5];
    z[29]=z[29]*z[30];
    z[31]=9*z[13];
    z[29]=z[29] + z[31];
    z[29]=z[29]*z[5];
    z[32]=2*z[3];
    z[27]=z[27] - z[32] - z[29];
    z[27]=z[4]*z[27];
    z[33]=z[14]*z[5];
    z[34]=z[33] + z[16];
    z[35]=z[5]*z[34];
    z[36]=3*z[3];
    z[29]=z[36] + z[29];
    z[29]=z[4]*z[29];
    z[37]=5*z[13];
    z[29]=z[29] - z[37] - 12*z[35];
    z[29]=z[5]*z[29];
    z[29]= - z[3] + z[29];
    z[29]=z[6]*z[29];
    z[35]= - z[16]*z[30];
    z[38]=5*z[16];
    z[39]=z[38] + z[23];
    z[40]= - z[39]*z[22];
    z[19]=z[19] + z[29] + z[27] + z[21] + z[40] - z[37] + z[35];
    z[19]=z[1]*z[19];
    z[14]=z[14]*z[7];
    z[21]=z[14] + z[16];
    z[27]=z[25] + z[21];
    z[29]=z[34] + z[14];
    z[29]=z[29]*z[22];
    z[34]=z[34]*z[24];
    z[34]=z[34] + z[13];
    z[29]=z[29] + z[34];
    z[35]= - z[9]*z[29];
    z[27]=2*z[27] + z[35];
    z[27]=z[2]*z[27];
    z[15]= - z[25] - z[15];
    z[15]=z[7]*z[15];
    z[29]= - z[7]*z[29];
    z[35]= - z[5]*z[34];
    z[29]=z[35] + z[29];
    z[29]=z[9]*z[29];
    z[35]=4*z[13];
    z[40]= - z[5]*z[38];
    z[15]=z[27] + z[29] + z[15] - z[35] + z[40];
    z[15]=z[15]*z[18];
    z[27]=11*z[16] + z[20];
    z[27]=z[5]*z[27];
    z[27]=8*z[13] + z[27];
    z[27]=z[5]*z[27];
    z[27]=z[36] + z[27];
    z[27]=z[27]*z[24];
    z[29]=3*z[16];
    z[40]=z[25] + z[29];
    z[40]=z[40]*z[26];
    z[41]= - z[37] - z[40];
    z[41]=z[5]*z[41];
    z[41]= - z[3] + z[41];
    z[41]=z[7]*z[41];
    z[27]=z[41] + static_cast<T>(1)+ z[27];
    z[17]=z[17] + z[28];
    z[17]=z[17]*z[24];
    z[17]=13*z[13] + z[17];
    z[17]=z[5]*z[17];
    z[41]=6*z[3];
    z[17]=z[41] + z[17];
    z[17]=z[5]*z[17];
    z[42]=2*z[16];
    z[43]=z[42] + z[33];
    z[44]= - z[43]*z[24];
    z[45]=3*z[13];
    z[44]= - z[45] + z[44];
    z[44]=z[5]*z[44];
    z[44]= - z[3] + z[44];
    z[44]=z[7]*z[44];
    z[17]=z[44] + static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[22];
    z[44]= - 8*z[16] - z[28];
    z[44]=z[5]*z[44];
    z[44]= - z[31] + z[44];
    z[44]=z[44]*z[24];
    z[44]= - 11*z[3] + z[44];
    z[44]=z[44]*z[24];
    z[44]= - static_cast<T>(7)+ z[44];
    z[44]=z[5]*z[44];
    z[17]=z[44] + z[17];
    z[17]=z[9]*z[17];
    z[44]=z[20] + 13*z[16];
    z[44]=z[44]*z[5];
    z[46]=z[44] + 12*z[13];
    z[46]=z[46]*z[5];
    z[41]= - z[41] - z[46];
    z[41]=z[41]*z[24];
    z[41]= - static_cast<T>(3)+ z[41];
    z[41]=z[5]*z[41];
    z[47]=z[29] + z[33];
    z[48]=z[47]*z[5];
    z[48]=z[48] + z[35];
    z[49]=z[5]*z[48];
    z[49]=z[36] + z[49];
    z[49]=z[49]*z[30];
    z[49]=static_cast<T>(5)+ z[49];
    z[49]=z[9]*z[49]*npow(z[5],2);
    z[41]=z[41] + z[49];
    z[41]=z[4]*z[41];
    z[17]=z[41] + 2*z[27] + z[17];
    z[17]=z[6]*z[17];
    z[27]=z[42] + z[28];
    z[27]=z[27]*z[24];
    z[28]=z[9]*z[5];
    z[34]= - z[34]*z[28];
    z[27]=z[34] + z[13] + z[27];
    z[27]=z[2]*z[27];
    z[34]= - 7*z[13] - z[44];
    z[34]=z[5]*z[34];
    z[41]=z[47]*z[24];
    z[41]=z[37] + z[41];
    z[41]=z[5]*z[41];
    z[41]=z[32] + z[41];
    z[41]=z[41]*z[28];
    z[27]=z[27] + z[41] - z[3] + z[34];
    z[27]=z[27]*z[18];
    z[34]=5*z[3];
    z[41]=z[34] + z[46];
    z[41]=z[41]*z[24];
    z[44]= - z[48]*z[24];
    z[44]= - z[34] + z[44];
    z[44]=z[44]*z[24];
    z[44]= - static_cast<T>(3)+ z[44];
    z[44]=z[44]*z[28];
    z[27]=z[27] + z[44] + static_cast<T>(1)+ z[41];
    z[27]=z[4]*z[27];
    z[20]= - z[16] + z[20];
    z[20]=z[20]*z[22];
    z[41]=z[5]*z[16];
    z[20]=z[20] - z[13] + 24*z[41];
    z[20]=z[7]*z[20];
    z[41]=z[13]*z[5];
    z[20]=17*z[41] + z[20];
    z[20]=z[7]*z[20];
    z[44]=z[9]*npow(z[7],2);
    z[46]=z[9]*z[7];
    z[49]=z[2]*z[46];
    z[49]=z[49] + z[44];
    z[21]=z[21]*z[22];
    z[21]=z[21] + z[13];
    z[21]=z[21]*z[49];
    z[23]=z[29] + z[23];
    z[23]=z[7]*z[23];
    z[23]=z[37] + 3*z[23];
    z[23]=z[7]*z[23];
    z[21]=z[3] + z[23] + z[21];
    z[18]=z[21]*z[18];
    z[21]=z[42] + z[14];
    z[23]=z[7]*z[21];
    z[42]=2*z[13];
    z[23]=z[42] + z[23];
    z[23]=z[7]*z[23];
    z[23]=z[3] + z[23];
    z[23]=z[23]*z[12];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[46];
    z[30]=z[3]*z[30];
    z[18]=z[18] + z[23] + z[30] + z[20];
    z[18]=z[10]*z[18];
    z[20]=z[47] + z[14];
    z[20]=z[20]*z[7];
    z[20]=z[20] + z[48];
    z[23]= - z[20]*z[22];
    z[16]=z[24]*z[16];
    z[30]= - z[37] - z[16];
    z[30]=z[5]*z[30];
    z[23]=z[23] - z[34] + z[30];
    z[23]=z[23]*z[22];
    z[30]= - z[32] - z[41];
    z[30]=z[30]*z[24];
    z[23]=z[23] - static_cast<T>(3)+ z[30];
    z[23]=z[23]*z[46];
    z[20]=z[20]*z[12];
    z[30]=z[45] + z[16];
    z[30]=z[5]*z[30];
    z[20]=z[20] + 12*z[3] + 5*z[30];
    z[20]=z[7]*z[20];
    z[30]=z[3] + z[41];
    z[30]=z[5]*z[30];
    z[20]=z[20] + static_cast<T>(5)+ 9*z[30];
    z[20]=z[7]*z[20];
    z[14]=z[29] + z[14];
    z[14]=z[7]*z[14];
    z[14]=z[35] + z[14];
    z[14]=z[7]*z[14];
    z[14]=z[36] + z[14];
    z[14]=z[14]*z[12];
    z[14]=static_cast<T>(5)+ z[14];
    z[14]=z[14]*z[44];
    z[29]=z[3]*z[26];
    z[29]=static_cast<T>(2)+ z[29];
    z[29]=z[5]*z[29];
    z[14]=z[14] + z[29] + z[20];
    z[14]=z[10]*z[14];
    z[13]=z[13]*z[26];
    z[13]=z[32] + z[13];
    z[13]=z[7]*z[13];
    z[20]=z[1] + z[5];
    z[20]=z[3]*z[20];
    z[13]=z[14] + z[23] + z[13] + z[20];
    z[13]=z[8]*z[13];
    z[14]= - z[25] + z[39];
    z[14]=z[14]*z[22];
    z[16]=z[45] - z[16];
    z[14]=3*z[16] + z[14];
    z[14]=z[7]*z[14];
    z[16]=z[43]*z[5];
    z[16]=z[16] + z[42];
    z[20]= - z[33] - z[21];
    z[20]=z[7]*z[20];
    z[20]=z[20] - z[16];
    z[20]=z[7]*z[20];
    z[16]= - z[5]*z[16];
    z[16]=z[20] - z[3] + z[16];
    z[12]=z[16]*z[12];
    z[16]=z[38] + z[25];
    z[16]=z[16]*z[24];
    z[16]=z[31] + z[16];
    z[16]=z[5]*z[16];
    z[16]=4*z[3] + z[16];
    z[16]=z[16]*z[24];
    z[12]=z[12] + static_cast<T>(1)+ z[16];
    z[12]=z[9]*z[12];
    z[16]= - z[35] - z[40];
    z[16]=z[5]*z[16];
    z[16]=z[32] + z[16];
    z[20]=z[4]*z[28];
    z[20]= - z[9] + z[20];
    z[20]=z[6]*z[20];
    z[21]=static_cast<T>(1)+ z[46];
    z[21]=z[8]*z[10]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[11]*z[20];

    r += z[12] + z[13] + z[14] + z[15] + 2*z[16] + z[17] + z[18] + 
      z[19] + z[20] + z[27];
 
    return r;
}

template double qqb_1l_r4(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1l_r4(const std::array<dd_real,31>&);
#endif
