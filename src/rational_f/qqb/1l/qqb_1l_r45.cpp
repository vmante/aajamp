#include "qqb_1l_rf_decl.hpp"

template<class T>
T qqb_1l_r45(const std::array<T,31>& k) {
  T z[81];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[18];
    z[7]=k[3];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=k[13];
    z[11]=k[4];
    z[12]=k[2];
    z[13]=k[11];
    z[14]=k[25];
    z[15]=k[15];
    z[16]=k[29];
    z[17]=k[14];
    z[18]=npow(z[2],2);
    z[19]=z[18]*z[11];
    z[20]=npow(z[2],3);
    z[21]=z[19] - z[20];
    z[21]=z[21]*z[3];
    z[22]=2*z[18];
    z[23]=z[11]*z[2];
    z[24]= - z[22] + z[23];
    z[24]=2*z[24] + z[21];
    z[24]=z[3]*z[24];
    z[25]=z[7]*z[2];
    z[26]=z[25] - z[18];
    z[19]=z[19] + z[20];
    z[27]=z[3]*z[19];
    z[27]=z[27] + 2*z[26] + z[23];
    z[27]=z[17]*z[27];
    z[28]=z[18]*z[17];
    z[29]=npow(z[7],2);
    z[30]=z[28]*z[29];
    z[31]=z[30]*z[13];
    z[32]=3*z[25];
    z[33]=z[31] + z[32];
    z[34]=3*z[18];
    z[35]=z[34] - z[25];
    z[36]=z[17]*z[7];
    z[35]=z[35]*z[36];
    z[35]=z[35] - z[18] - z[33];
    z[35]=z[13]*z[35];
    z[37]=2*z[20];
    z[38]=z[3]*npow(z[2],4);
    z[39]= - z[37] - z[38];
    z[39]=z[3]*z[39];
    z[40]= - z[13]*z[19];
    z[39]=z[40] + z[23] + z[39];
    z[39]=z[5]*z[39];
    z[40]=2*z[7];
    z[41]=z[40] + z[11];
    z[24]=z[39] + z[35] + z[27] + 3*z[41] + z[24];
    z[24]=z[6]*z[24];
    z[27]=2*z[3];
    z[35]=z[27]*z[18];
    z[39]= - z[2] - z[35];
    z[39]=z[3]*z[39];
    z[42]= - z[20]*z[27];
    z[42]= - z[18] + z[42];
    z[42]=z[3]*z[42];
    z[43]=2*z[2];
    z[42]=z[43] + z[42];
    z[42]=z[5]*z[42];
    z[39]=z[42] - static_cast<T>(3)+ z[39];
    z[39]=z[6]*z[39];
    z[42]=5*z[2];
    z[44]=z[42] - z[35];
    z[44]=z[3]*z[44];
    z[44]=static_cast<T>(2)+ z[44];
    z[44]=z[3]*z[44];
    z[45]=z[20]*z[3];
    z[34]=z[45] - z[34];
    z[45]= - z[34]*z[27];
    z[45]=z[42] + z[45];
    z[45]=z[3]*z[45];
    z[45]= - static_cast<T>(6)+ z[45];
    z[45]=z[5]*z[45];
    z[46]=4*z[2];
    z[47]=z[18]*z[3];
    z[48]=z[46] - z[47];
    z[48]=z[48]*z[3];
    z[48]=z[48] - 3;
    z[49]=z[2] - z[47];
    z[49]=z[6]*z[49];
    z[49]=z[48] + z[49];
    z[50]=z[5]*z[3];
    z[51]=z[50]*z[1];
    z[49]=z[49]*z[51];
    z[52]=z[9]*z[7];
    z[53]=static_cast<T>(5)+ z[52];
    z[53]=z[9]*z[53];
    z[39]=z[49] + z[39] + z[45] + z[44] + z[53];
    z[39]=z[1]*z[39];
    z[44]=z[19]*z[27];
    z[44]=z[44] - z[22] + 3*z[23];
    z[44]=z[3]*z[44];
    z[45]=7*z[2];
    z[49]=4*z[7];
    z[53]=2*z[11];
    z[44]=z[44] + z[53] - z[45] + z[49];
    z[44]=z[17]*z[44];
    z[54]=z[22] - z[25];
    z[55]=z[17]*z[54]*z[40];
    z[33]=z[55] - z[22] - z[33];
    z[33]=z[13]*z[33];
    z[56]=z[45] - z[40];
    z[56]=z[7]*z[56];
    z[56]= - 6*z[18] + z[56];
    z[56]=z[17]*z[56];
    z[57]=z[7] - z[2];
    z[58]=3*z[11];
    z[33]=z[33] + z[56] - 5*z[57] - z[58];
    z[33]=z[13]*z[33];
    z[56]=z[23] + z[18];
    z[59]=z[21] - z[56];
    z[59]=z[3]*z[59];
    z[60]=9*z[2];
    z[59]=z[59] + z[60] - z[53];
    z[59]=z[3]*z[59];
    z[61]=2*z[13];
    z[19]= - z[19]*z[61];
    z[19]=z[19] + z[18] + 5*z[23];
    z[19]=z[13]*z[19];
    z[37]=z[37] - z[38];
    z[37]=z[37]*z[3];
    z[38]=5*z[18];
    z[62]=z[38] + z[37];
    z[62]=z[3]*z[62];
    z[63]=z[2] - z[11];
    z[19]=z[19] + 3*z[63] + z[62];
    z[19]=z[5]*z[19];
    z[62]=z[11]*z[7];
    z[63]=z[62] + z[29];
    z[63]=z[63]*z[9];
    z[64]=z[58] - z[63];
    z[64]=z[9]*z[64];
    z[19]=z[39] + z[24] + z[19] + z[33] + z[64] + z[44] + static_cast<T>(3)+ z[59];
    z[19]=z[6]*z[19];
    z[24]=z[40]*z[18];
    z[24]=z[24] - z[20];
    z[24]=z[24]*z[17];
    z[33]=z[54]*z[7];
    z[20]=z[33] - z[20];
    z[33]= - z[3]*z[20];
    z[33]= - z[24] + z[33] - z[22] - z[25];
    z[33]=z[17]*z[33];
    z[39]=3*z[7];
    z[44]= - z[42] + z[39];
    z[44]=z[7]*z[44];
    z[59]=z[2] - z[49];
    z[59]=z[11]*z[59];
    z[22]=z[59] + z[22] + z[44];
    z[22]=z[3]*z[22];
    z[44]=npow(z[11],2);
    z[59]=z[44]*z[40];
    z[64]=z[44]*z[3];
    z[65]=z[64]*z[29];
    z[59]=z[59] + z[65];
    z[59]=z[59]*z[9];
    z[65]=3*z[29];
    z[66]=z[65] - z[62];
    z[66]=z[3]*z[66];
    z[67]=z[53] - z[7];
    z[66]= - z[67] + z[66];
    z[66]=z[11]*z[66];
    z[66]=z[59] + z[66];
    z[66]=z[9]*z[66];
    z[22]=z[66] + z[33] + z[11] + z[22];
    z[22]=z[16]*z[22];
    z[20]= - z[20]*z[27];
    z[20]= - z[24] + 3*z[26] + z[20];
    z[20]=z[17]*z[20];
    z[33]=3*z[2];
    z[49]=z[49] - z[33];
    z[66]=z[7]*z[49];
    z[66]= - z[18] + z[66];
    z[66]=z[3]*z[66];
    z[68]=z[43] + z[7];
    z[20]=z[20] + 2*z[68] + z[66];
    z[20]=z[17]*z[20];
    z[66]= - z[58] - z[42] + z[40];
    z[66]=z[3]*z[66];
    z[69]=2*z[29];
    z[70]=z[69] + z[62];
    z[71]=z[53]*z[3];
    z[70]=z[70]*z[71];
    z[59]=z[70] + z[59];
    z[70]=z[62] + z[59];
    z[70]=z[9]*z[70];
    z[72]=6*z[29] + 5*z[62];
    z[72]=z[3]*z[72];
    z[70]=z[70] - z[7] + z[72];
    z[70]=z[9]*z[70];
    z[72]=8*z[7];
    z[23]=z[23]*z[13];
    z[73]=z[23] + 5*z[11] - z[42] + z[72];
    z[73]=z[13]*z[73];
    z[20]=z[22] + z[73] + z[70] + z[20] - static_cast<T>(1)+ z[66];
    z[20]=z[16]*z[20];
    z[22]=npow(z[11],3);
    z[66]=z[22]*z[27];
    z[70]=6*z[11];
    z[73]=z[70] + z[2];
    z[74]= - z[11]*z[73];
    z[66]=z[74] + z[66];
    z[66]=z[3]*z[66];
    z[74]=z[53] + z[2];
    z[75]= - z[74]*z[64];
    z[75]=z[44] + z[75];
    z[75]=z[17]*z[75];
    z[76]=z[2] - z[40];
    z[66]=2*z[75] + z[66] + 3*z[76] - z[53];
    z[66]=z[17]*z[66];
    z[75]=z[3]*npow(z[11],4);
    z[76]= - 5*z[22] + z[75];
    z[76]=z[3]*z[76];
    z[77]=z[11] + z[7];
    z[78]=z[77]*z[11];
    z[79]=z[78] + z[29];
    z[80]=z[11]*z[79];
    z[75]=z[80] - z[75];
    z[75]=z[17]*z[75];
    z[69]=z[75] + z[76] + z[69] + 5*z[78];
    z[69]=z[17]*z[69];
    z[75]=z[40] - z[11];
    z[75]=z[75]*z[11];
    z[64]=z[64]*z[7];
    z[75]=z[75] - z[64];
    z[75]=z[75]*z[9];
    z[76]=z[58] - z[7];
    z[78]=z[11]*z[76];
    z[22]= - z[3]*z[22];
    z[22]=z[78] + z[22];
    z[22]=z[3]*z[22];
    z[78]=z[39] - z[11];
    z[22]=z[75] + z[69] + z[22] + z[78];
    z[69]=2*z[9];
    z[22]=z[22]*z[69];
    z[42]= - z[70] + z[42] + z[39];
    z[42]=z[3]*z[42];
    z[70]= - z[25] + z[62];
    z[70]=z[3]*z[70];
    z[70]=z[75] + z[70] - z[77];
    z[70]=z[70]*z[61];
    z[22]=z[70] + z[22] + z[66] - static_cast<T>(2)+ z[42];
    z[22]=z[13]*z[22];
    z[42]=z[32] + z[18];
    z[42]= - z[7]*z[42];
    z[42]=z[42] - z[30];
    z[42]=z[17]*z[42];
    z[66]=z[41]*z[11];
    z[66]=z[66] + z[29];
    z[70]= - z[9]*z[66];
    z[70]=z[70] - 5*z[7] - z[74];
    z[70]=z[11]*z[70];
    z[46]= - z[46] - z[39];
    z[46]=z[7]*z[46];
    z[42]=z[42] + z[46] + z[70];
    z[42]=z[16]*z[42];
    z[46]=z[26]*z[40];
    z[30]=z[46] + z[30];
    z[30]=z[30]*z[17];
    z[46]=z[45] - z[39];
    z[46]=z[7]*z[46];
    z[46]=z[46] - z[30];
    z[46]=z[17]*z[46];
    z[70]=z[53]*z[9];
    z[66]= - z[66]*z[70];
    z[75]=5*z[29];
    z[80]= - 7*z[7] - z[53];
    z[80]=z[11]*z[80];
    z[66]=z[66] - z[75] + z[80];
    z[66]=z[9]*z[66];
    z[42]=z[42] + z[66] + z[46] + z[53] + z[68];
    z[42]=z[16]*z[42];
    z[46]=z[75] - 3*z[62];
    z[46]=z[17]*z[46];
    z[66]=z[29]*z[17];
    z[68]=z[11]*z[66];
    z[68]= - z[44] + z[68];
    z[68]=z[68]*z[69];
    z[75]=4*z[11];
    z[46]=z[68] - z[75] + z[46];
    z[46]=z[9]*z[46];
    z[32]=z[38] + z[32];
    z[32]=z[3]*z[32];
    z[38]= - z[7]*z[47];
    z[38]=z[18] + z[38];
    z[38]=z[38]*z[61];
    z[32]=z[38] - z[45] + z[32];
    z[32]=z[13]*z[32];
    z[38]= - z[3]*z[60];
    z[32]=z[42] + z[32] + z[46] - 9*z[36] - static_cast<T>(1)+ z[38];
    z[32]=z[5]*z[32];
    z[36]=z[39] + z[53];
    z[38]= - z[11]*z[36];
    z[38]=z[38] - z[59];
    z[38]=z[9]*z[38];
    z[42]= - z[29] + z[44];
    z[42]=z[3]*z[42];
    z[36]=z[38] + 3*z[42] + z[36];
    z[36]=z[9]*z[36];
    z[31]= - z[31] - 5*z[25] + z[55];
    z[31]=z[13]*z[31];
    z[38]= - z[18] + z[29];
    z[38]=z[17]*z[38];
    z[31]=z[31] + 3*z[38] - 2*z[57] - 7*z[11];
    z[31]=z[13]*z[31];
    z[38]=z[75] + z[2] + z[40];
    z[38]=z[3]*z[38];
    z[39]=z[43] - z[39];
    z[39]=z[17]*z[39];
    z[42]=z[44]*z[9];
    z[42]=z[42] - z[11];
    z[42]=z[42]*z[9];
    z[45]=z[42] + 1;
    z[45]=z[45]*z[9];
    z[46]=z[17] - z[45];
    z[46]=z[12]*z[46];
    z[31]=z[46] + z[31] + z[36] + z[39] - static_cast<T>(2)+ z[38];
    z[31]=z[15]*z[31];
    z[36]=z[63] - z[53] - z[7];
    z[36]=z[9]*z[36];
    z[38]= - z[2] + z[41];
    z[38]=2*z[38] + z[23];
    z[38]=z[13]*z[38];
    z[39]= - static_cast<T>(4)- z[52];
    z[39]=z[9]*z[39];
    z[41]= - z[5]*z[70];
    z[39]=z[39] + z[41];
    z[39]=z[1]*z[39];
    z[41]=z[43] - z[58];
    z[41]=z[13]*z[11]*z[41];
    z[41]= - z[53] + z[41];
    z[41]=z[4]*z[41];
    z[23]= - z[11] + z[23];
    z[23]=z[5]*z[23];
    z[23]=z[41] + z[39] + 2*z[23] + z[38] - static_cast<T>(2)+ z[36];
    z[23]=z[10]*z[23];
    z[24]= - z[24] + z[26];
    z[24]=z[17]*z[24];
    z[25]= - 2*z[25] - z[30];
    z[25]=z[5]*z[25];
    z[24]=z[25] + z[24] + z[49];
    z[24]=z[17]*z[24];
    z[25]=z[33] - z[28];
    z[25]=z[25]*z[17];
    z[26]= - static_cast<T>(3)+ z[25];
    z[26]=z[17]*z[26];
    z[26]=z[26] + z[9];
    z[26]=z[12]*z[26];
    z[30]=z[1]*z[40];
    z[29]=z[29] + z[30];
    z[29]=z[10]*z[9]*z[29];
    z[30]= - z[1]*z[69];
    z[24]=z[29] + z[26] + z[30] - z[52] + static_cast<T>(3)+ z[24];
    z[24]=z[8]*z[24];
    z[26]=z[17] + z[3];
    z[26]=z[62]*z[26];
    z[29]= - z[66] - z[78];
    z[29]=z[11]*z[29];
    z[29]=z[64] + z[29];
    z[29]=z[9]*z[29];
    z[26]=z[29] + z[76] + z[26];
    z[26]=z[9]*z[26];
    z[29]= - z[9]*z[76];
    z[29]= - static_cast<T>(4)+ z[29];
    z[29]=z[9]*z[29];
    z[30]=npow(z[9],2);
    z[36]=z[12]*z[30];
    z[29]=z[29] + 2*z[36];
    z[29]=z[12]*z[29];
    z[36]=z[11] + z[2];
    z[38]=z[3]*z[36];
    z[39]= - z[17]*z[57];
    z[41]=z[10]*z[77];
    z[26]= - 2*z[41] + z[29] + z[26] + z[39] + static_cast<T>(4)+ z[38];
    z[26]=z[14]*z[26];
    z[29]=z[33] - z[40];
    z[38]= - z[11]*z[29];
    z[38]=z[38] - z[54];
    z[38]=z[38]*z[27];
    z[38]=z[38] - z[11] + 11*z[2] - z[72];
    z[38]=z[3]*z[38];
    z[29]=z[7]*z[29];
    z[29]= - z[44] - z[18] + z[29];
    z[29]=z[17]*z[29]*z[27];
    z[29]=z[29] + static_cast<T>(2)+ z[38];
    z[29]=z[17]*z[29];
    z[25]= - static_cast<T>(2)+ z[25];
    z[25]=z[17]*z[25];
    z[28]=z[2] - z[28];
    z[28]=z[17]*z[28];
    z[28]=z[28] + z[42];
    z[28]=z[16]*z[28];
    z[25]=z[28] + z[25] + z[45];
    z[25]=z[16]*z[25];
    z[28]=z[67]*z[9];
    z[38]=static_cast<T>(3)+ z[28];
    z[38]=z[9]*z[38];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=z[13]*z[28];
    z[28]=z[38] + z[28];
    z[28]=z[13]*z[28];
    z[28]= - z[30] + z[28];
    z[38]= - z[13]*z[9];
    z[30]= - z[30] + z[38];
    z[30]=z[12]*z[30]*z[61];
    z[25]=z[30] + 2*z[28] + z[25];
    z[25]=z[12]*z[25];
    z[28]= - z[33] + z[35];
    z[28]=z[3]*z[28];
    z[28]= - static_cast<T>(3)+ z[28];
    z[28]=z[3]*z[28];
    z[30]=z[3]*z[34];
    z[30]=z[43] + z[30];
    z[30]=z[5]*z[30]*z[27];
    z[33]= - z[48]*z[51];
    z[28]=z[33] + z[28] + z[30];
    z[28]=z[1]*z[28];
    z[21]= - z[21] - z[56];
    z[21]=z[3]*z[21];
    z[21]=z[21] - z[36];
    z[21]=z[3]*z[21];
    z[18]=z[18] - z[37];
    z[18]=z[18]*z[50];
    z[30]= - 3*z[12] + z[73];
    z[30]=z[13]*z[30];
    z[18]=z[28] + z[18] + static_cast<T>(3)+ z[21] + z[30];
    z[18]=z[4]*z[18];
    z[21]=z[77]*z[44]*z[27];
    z[28]= - 10*z[11] - 9*z[7];
    z[28]=z[11]*z[28];
    z[21]=z[21] - z[65] + z[28];
    z[21]=z[3]*z[21];
    z[28]= - z[17]*z[79]*z[71];
    z[21]=z[28] - z[40] + z[21];
    z[21]=z[17]*z[21];
    z[21]=z[21] - z[70];
    z[21]=z[9]*z[21];
    z[28]=z[3]*z[74];
    z[28]= - static_cast<T>(5)+ z[28];
    z[27]=z[28]*z[27];

    r += z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25]
       + 2*z[26] + z[27] + z[29] + z[31] + z[32];
 
    return r;
}

template double qqb_1l_r45(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1l_r45(const std::array<dd_real,31>&);
#endif
