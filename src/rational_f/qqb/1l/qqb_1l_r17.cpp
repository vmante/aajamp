#include "qqb_1l_rf_decl.hpp"

template<class T>
T qqb_1l_r17(const std::array<T,31>& k) {
  T z[37];
  T r = 0;

    z[1]=k[1];
    z[2]=k[9];
    z[3]=k[10];
    z[4]=k[13];
    z[5]=k[12];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=k[2];
    z[9]=k[15];
    z[10]=k[5];
    z[11]=k[14];
    z[12]=k[11];
    z[13]=k[7];
    z[14]=2*z[11];
    z[15]=z[3]*z[11];
    z[16]=z[7]*z[15];
    z[16]=z[16] + z[14] - z[3];
    z[16]=z[7]*z[16];
    z[16]= - static_cast<T>(1)+ z[16];
    z[17]=2*z[7];
    z[16]=z[16]*z[17];
    z[18]=z[10]*z[11];
    z[19]=z[7]*z[11];
    z[19]=z[18] - static_cast<T>(1)+ 3*z[19];
    z[19]=z[10]*z[19];
    z[16]=z[16] + z[19];
    z[16]=z[4]*z[16];
    z[19]=z[6]*z[11];
    z[20]=z[19] + 1;
    z[21]=z[3]*z[6];
    z[22]= - z[20]*z[21];
    z[23]=z[7]*z[3];
    z[24]=z[23]*z[14];
    z[25]=z[20]*z[3];
    z[26]= - z[24] - 4*z[11] + z[25];
    z[26]=z[7]*z[26];
    z[27]=3*z[18];
    z[16]=z[16] - z[27] + z[26] + z[19] + z[22];
    z[16]=z[5]*z[16];
    z[22]=3*z[9];
    z[26]=z[19] + 2;
    z[28]=z[3]*z[26];
    z[28]=z[24] + z[22] - z[14] + z[28];
    z[28]=z[7]*z[28];
    z[29]=z[9]*z[6];
    z[30]=z[20]*z[29];
    z[30]= - z[19] + z[30];
    z[22]=z[30]*z[22];
    z[30]=npow(z[6],2);
    z[31]=z[30]*z[9];
    z[32]=z[11]*z[31];
    z[32]= - z[19] + z[32];
    z[32]=z[9]*z[32];
    z[32]=z[14] + z[32];
    z[32]=z[10]*z[9]*z[32];
    z[22]=z[32] + z[14] + z[22];
    z[22]=z[10]*z[22];
    z[15]=z[30]*z[15];
    z[26]=z[26]*z[29];
    z[32]=static_cast<T>(2)- z[19];
    z[22]=z[22] + z[28] + 3*z[26] + 2*z[32] + z[15];
    z[22]=z[12]*z[22];
    z[26]=2*z[6];
    z[28]=z[26] + z[31];
    z[28]=z[28]*z[9];
    z[32]= - static_cast<T>(1)- z[28];
    z[32]=z[9]*z[32];
    z[31]= - z[3]*z[31];
    z[31]= - 2*z[21] + z[31];
    z[31]=z[9]*z[31];
    z[31]= - z[3] + z[31];
    z[31]=z[9]*z[31];
    z[33]= - z[3]*z[14];
    z[31]=z[33] + z[31];
    z[31]=z[7]*z[31];
    z[33]=z[3] - z[24];
    z[33]=z[7]*z[33];
    z[34]=z[21] - 1;
    z[33]=z[33] + z[34];
    z[33]=z[12]*z[33];
    z[35]=z[19]*z[3];
    z[31]=z[33] + z[31] + 2*z[32] + z[11] - z[35];
    z[31]=z[7]*z[31];
    z[30]=z[30]*z[3];
    z[26]=z[30] - z[26];
    z[30]= - z[9]*z[26];
    z[28]= - static_cast<T>(2)- z[28];
    z[28]=z[9]*z[28];
    z[28]= - z[11] + z[28];
    z[28]=z[10]*z[28];
    z[15]=z[28] + z[30] + 2*z[19] - z[15] + z[31];
    z[15]=z[13]*z[15];
    z[28]=z[14] - z[9];
    z[30]=npow(z[9],2);
    z[31]=z[28]*z[30];
    z[32]=npow(z[9],3);
    z[33]=z[32]*z[18];
    z[31]=z[31] + z[33];
    z[31]=z[10]*z[31];
    z[23]=z[23] - 1;
    z[32]= - z[7]*z[32]*z[23];
    z[28]= - z[3] + z[28];
    z[28]=z[9]*z[28];
    z[33]=2*z[3];
    z[33]= - z[12]*z[33];
    z[36]= - z[11] + z[3];
    z[36]=z[2]*z[36];
    z[28]=2*z[36] + z[33] + z[31] + z[28] + z[32];
    z[28]=z[8]*z[28];
    z[31]=static_cast<T>(3)+ z[19];
    z[31]=z[31]*z[21];
    z[24]=z[24] - z[35] + 3*z[11];
    z[24]=z[7]*z[24];
    z[32]=3*z[19];
    z[24]=z[27] + z[24] + z[31] - static_cast<T>(4)- z[32];
    z[24]=z[4]*z[24];
    z[27]=z[29] - 2;
    z[31]= - z[32] + z[27];
    z[31]=z[9]*z[31];
    z[31]=z[14] + z[31];
    z[31]=z[9]*z[31];
    z[32]= - z[9]*z[19];
    z[14]=z[14] + z[32];
    z[14]=z[10]*z[14]*z[30];
    z[14]=z[31] + z[14];
    z[14]=z[10]*z[14];
    z[19]=z[21] + z[19];
    z[26]=z[10] + z[26];
    z[26]=z[4]*z[26];
    z[26]=z[26] - z[19];
    z[20]= - z[6]*z[20];
    z[18]= - static_cast<T>(1)- z[18];
    z[18]=z[10]*z[18];
    z[18]=z[20] + z[18];
    z[18]=z[5]*z[18];
    z[18]=2*z[26] + z[18];
    z[18]=z[2]*z[18];
    z[20]= - z[5] + z[2];
    z[20]=z[1]*z[20];
    z[20]=z[20] - 3;
    z[20]=z[20]*z[4]*z[3];
    z[23]=z[4]*z[23];
    z[23]= - z[3] + z[23];
    z[23]=z[5]*z[23];
    z[26]= - z[4]*z[34];
    z[26]=z[3] + z[26];
    z[26]=2*z[26] - z[5];
    z[26]=z[2]*z[26];
    z[20]=z[26] + z[23] + z[20];
    z[20]=z[1]*z[20];
    z[19]=z[29] - z[19];
    z[19]=z[9]*z[19];
    z[23]=z[11] - z[25];
    z[19]=2*z[23] + z[19];
    z[21]= - z[9]*z[21];
    z[21]= - z[3] + z[21];
    z[17]=z[21]*z[17];
    z[17]= - z[27] + z[17];
    z[17]=z[7]*z[30]*z[17];

    r += z[14] + z[15] + z[16] + z[17] + z[18] + 2*z[19] + z[20] + 
      z[22] + z[24] + z[28];
 
    return r;
}

template double qqb_1l_r17(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1l_r17(const std::array<dd_real,31>&);
#endif
