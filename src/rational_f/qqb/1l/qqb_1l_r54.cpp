#include "qqb_1l_rf_decl.hpp"

template<class T>
T qqb_1l_r54(const std::array<T,31>& k) {
  T z[79];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[9];
    z[4]=k[10];
    z[5]=k[30];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=k[4];
    z[9]=k[12];
    z[10]=k[20];
    z[11]=k[7];
    z[12]=k[8];
    z[13]=k[5];
    z[14]=k[23];
    z[15]=k[11];
    z[16]=k[14];
    z[17]=k[15];
    z[18]=npow(z[13],2);
    z[19]=z[18]*z[16];
    z[20]=npow(z[6],2);
    z[21]=z[19]*z[20];
    z[22]=z[21]*z[9];
    z[23]=z[13]*z[6];
    z[24]=3*z[23];
    z[25]=z[22] - z[24];
    z[26]= - z[20] + z[24];
    z[27]=z[16]*z[13];
    z[26]=z[26]*z[27];
    z[26]=z[26] + z[25];
    z[26]=z[9]*z[26];
    z[28]=z[18]*z[7];
    z[29]=z[28] + z[13];
    z[30]=z[8]*z[7];
    z[31]= - z[29]*z[30];
    z[32]=2*z[13];
    z[31]=z[31] + z[32] + 3*z[28];
    z[31]=z[8]*z[31];
    z[33]=3*z[13];
    z[34]= - z[6] - z[33];
    z[34]=z[13]*z[34];
    z[35]=z[18]*z[9];
    z[36]= - z[6]*z[35];
    z[31]=z[36] + z[34] + z[31];
    z[31]=z[15]*z[31];
    z[34]=2*z[6];
    z[36]= - z[34] + z[33];
    z[27]=z[36]*z[27];
    z[36]=z[7]*z[13];
    z[37]=static_cast<T>(1)- z[36];
    z[38]=z[13] - z[28];
    z[38]=z[16]*z[38];
    z[37]=3*z[37] + z[38];
    z[37]=z[8]*z[37];
    z[38]=3*z[6];
    z[26]=z[31] + z[26] + z[37] - z[38] + z[27];
    z[26]=z[14]*z[26];
    z[27]=z[13] + z[6];
    z[31]=5*z[13];
    z[37]= - z[27]*z[31];
    z[39]= - z[34]*z[35];
    z[37]=z[37] + z[39];
    z[37]=z[9]*z[37];
    z[39]=2*z[7];
    z[40]=z[39]*z[18];
    z[31]=z[31] + z[40];
    z[41]= - z[6] - z[31];
    z[42]=z[28] + 4*z[13];
    z[42]=z[42]*z[7];
    z[42]=z[42] + 3;
    z[42]=z[42]*z[30];
    z[43]=13*z[13] + 4*z[28];
    z[43]=z[7]*z[43];
    z[43]= - z[42] + static_cast<T>(6)+ z[43];
    z[43]=z[8]*z[43];
    z[37]=z[37] + 3*z[41] + z[43];
    z[37]=z[15]*z[37];
    z[41]=9*z[13];
    z[43]=5*z[28];
    z[44]=4*z[6];
    z[45]=z[43] + z[44] - z[41];
    z[45]=z[16]*z[45];
    z[46]=2*z[23];
    z[47]=z[46] - z[20];
    z[47]=z[47]*z[32]*z[16];
    z[25]=z[47] + z[25];
    z[25]=z[9]*z[25];
    z[48]= - 6*z[13] + 7*z[6];
    z[49]= - z[13]*z[48];
    z[49]=2*z[20] + z[49];
    z[49]=z[16]*z[49];
    z[25]=z[25] + z[34] + z[49];
    z[25]=z[9]*z[25];
    z[49]=z[33] - z[40];
    z[49]=z[7]*z[49];
    z[49]= - static_cast<T>(2)+ z[49];
    z[49]=z[16]*z[49];
    z[50]= - z[7]*z[33];
    z[50]= - static_cast<T>(5)+ z[50];
    z[50]=z[7]*z[50];
    z[49]=z[50] + z[49];
    z[49]=z[8]*z[49];
    z[50]=3*z[1];
    z[51]= - z[7] + z[9];
    z[51]=z[51]*z[50];
    z[25]=z[26] + z[37] + z[51] + z[25] + z[49] + static_cast<T>(15)+ z[45];
    z[25]=z[14]*z[25];
    z[22]= - z[22] + 5*z[23] - z[47];
    z[22]=z[9]*z[22];
    z[26]=z[20] - z[18];
    z[26]=z[16]*z[26];
    z[37]=5*z[6];
    z[22]=z[22] + 3*z[26] + z[37] + z[32];
    z[22]=z[9]*z[22];
    z[26]=z[4]*z[6];
    z[45]=z[26] + 3;
    z[47]=npow(z[4],2);
    z[49]=z[45]*z[47];
    z[51]=z[20]*z[4];
    z[52]=z[51] + z[34];
    z[53]= - z[4]*z[52];
    z[53]=static_cast<T>(3)+ z[53];
    z[54]=z[7]*z[4];
    z[53]=z[53]*z[54];
    z[49]=z[49] + z[53];
    z[49]=z[1]*z[49];
    z[53]=z[51] + z[6];
    z[55]=2*z[4];
    z[56]=z[53]*z[55];
    z[57]= - static_cast<T>(3)- z[56];
    z[57]=z[4]*z[57];
    z[58]=npow(z[6],3);
    z[59]=z[58]*z[47];
    z[59]= - z[38] + z[59];
    z[59]=z[4]*z[59];
    z[59]=static_cast<T>(2)+ z[59];
    z[59]=z[59]*z[39];
    z[49]=z[49] - 7*z[9] + z[57] + z[59];
    z[49]=z[1]*z[49];
    z[57]=z[58]*z[4];
    z[59]=z[57] - z[20];
    z[59]=z[59]*z[4];
    z[60]= - z[6] + z[59];
    z[60]=z[4]*z[60];
    z[61]=z[4]*npow(z[6],4);
    z[62]= - z[61] + 2*z[58];
    z[63]=z[62]*z[47];
    z[64]=z[34] - z[13];
    z[63]=z[63] - z[64];
    z[63]=z[7]*z[63];
    z[65]=z[44] - z[33];
    z[65]=z[16]*z[65];
    z[66]=2*z[26];
    z[67]=static_cast<T>(1)+ z[66];
    z[67]=z[67]*z[47];
    z[68]=npow(z[4],3);
    z[69]= - z[1]*z[68];
    z[67]=z[67] + z[69];
    z[67]=z[1]*z[67];
    z[53]= - z[4]*z[53];
    z[53]= - static_cast<T>(1)+ z[53];
    z[53]=z[4]*z[53];
    z[53]=z[67] + z[53] + z[16];
    z[53]=z[2]*z[53];
    z[22]=z[53] + z[49] + z[22] + z[65] + z[63] + static_cast<T>(3)+ z[60];
    z[22]=z[3]*z[22];
    z[49]=z[5]*z[13];
    z[53]=z[9]*z[13];
    z[49]=z[49] + z[53];
    z[60]=z[13] - z[6];
    z[63]= - z[1] - z[60];
    z[49]=z[49]*z[63];
    z[63]=z[14]*z[6];
    z[63]=z[63] + z[26];
    z[63]=z[8]*z[63];
    z[49]=z[63] + z[49];
    z[49]=z[11]*z[49];
    z[63]= - z[8]*z[26];
    z[65]= - z[9]*z[23];
    z[63]=z[65] - z[6] + z[63];
    z[63]=z[15]*z[63];
    z[65]=z[26] + 1;
    z[63]=z[65] - z[63];
    z[67]=z[5]*z[38];
    z[69]= - z[8]*z[4];
    z[70]=z[44] - z[13];
    z[70]=z[9]*z[70];
    z[71]=4*z[4];
    z[72]=z[71] + 5*z[5] - 3*z[9];
    z[72]=z[1]*z[72];
    z[73]= - z[6] - z[8];
    z[73]=3*z[73] + 8*z[1];
    z[73]=z[14]*z[73];
    z[49]=z[49] + z[73] + z[72] + z[70] + z[69] + z[67] - 2*z[63];
    z[49]=z[11]*z[49];
    z[63]=z[55]*z[20];
    z[67]= - z[37] + z[63];
    z[67]=z[4]*z[67];
    z[69]= - z[58]*z[55];
    z[69]=7*z[20] + z[69];
    z[69]=z[4]*z[69];
    z[69]=z[13] - 6*z[6] + z[69];
    z[69]=z[7]*z[69];
    z[67]=z[69] - static_cast<T>(2)+ z[67];
    z[67]=z[5]*z[67];
    z[56]= - static_cast<T>(7)+ z[56];
    z[56]=z[4]*z[56];
    z[69]=2*z[47];
    z[70]= - z[58]*z[69];
    z[72]=11*z[6];
    z[70]=z[72] + z[70];
    z[70]=z[4]*z[70];
    z[70]= - static_cast<T>(3)+ z[70];
    z[70]=z[7]*z[70];
    z[56]=z[67] + z[56] + z[70];
    z[56]=z[5]*z[56];
    z[52]=z[7]*z[52];
    z[45]= - z[45] + z[52];
    z[45]=z[47]*z[45];
    z[52]=z[51] - z[6];
    z[67]=z[52]*z[54];
    z[70]=z[47]*z[6];
    z[67]= - z[70] + z[67];
    z[67]=z[5]*z[67];
    z[45]=z[67] + z[45];
    z[45]=z[5]*z[45];
    z[67]=z[9]*z[4];
    z[67]=z[67] - z[47] - z[54];
    z[67]=z[30]*z[67];
    z[67]=4*z[54] + z[67];
    z[73]=2*z[9];
    z[67]=z[67]*z[73];
    z[74]=npow(z[7],2);
    z[75]=z[74]*z[55];
    z[45]=z[67] - z[75] + z[45];
    z[45]=z[1]*z[45];
    z[67]=3*z[4];
    z[76]=z[26] - 1;
    z[77]=z[7]*z[76];
    z[77]= - z[67] + z[77];
    z[77]=z[7]*z[77];
    z[77]=z[47] + z[77];
    z[75]=z[8]*z[75];
    z[71]= - z[71] + z[7];
    z[71]=z[7]*z[71];
    z[74]=z[74]*z[4];
    z[78]=z[8]*z[74];
    z[71]=z[71] + z[78];
    z[71]=z[8]*z[71];
    z[78]=static_cast<T>(2)- z[30];
    z[78]=z[9]*z[78];
    z[71]=z[71] + z[78];
    z[71]=z[71]*z[73];
    z[45]=z[45] + z[71] + z[75] + 2*z[77] + z[56];
    z[45]=z[1]*z[45];
    z[56]= - 6*z[58] + z[61];
    z[56]=z[4]*z[56];
    z[58]= - z[72] + z[33];
    z[58]=z[13]*z[58];
    z[56]=z[58] + 13*z[20] + z[56];
    z[56]=z[7]*z[56];
    z[58]=z[64]*z[13];
    z[58]=z[58] - z[20];
    z[61]= - z[58]*z[36];
    z[71]=z[19]*z[6];
    z[72]=z[34] + z[13];
    z[75]=z[13]*z[72];
    z[61]=z[71] + z[75] + z[61];
    z[61]=z[16]*z[61];
    z[75]=5*z[20];
    z[57]=z[75] - z[57];
    z[57]=z[4]*z[57];
    z[37]=z[61] + z[56] - z[33] - z[37] + z[57];
    z[37]=z[5]*z[37];
    z[56]= - z[38] - z[13];
    z[56]=z[13]*z[56];
    z[56]=4*z[20] + z[56];
    z[56]=z[7]*z[56];
    z[57]=z[32]*z[7];
    z[61]= - z[58]*z[57];
    z[61]=z[61] + z[71];
    z[61]=z[16]*z[61];
    z[56]=z[61] - 2*z[72] + z[56];
    z[56]=z[16]*z[56];
    z[44]=z[44] - z[59];
    z[44]=z[4]*z[44];
    z[59]= - z[4]*z[62];
    z[59]= - z[75] + z[59];
    z[59]=z[4]*z[59];
    z[41]= - z[41] + 18*z[6] + z[59];
    z[41]=z[7]*z[41];
    z[37]=z[37] + z[56] + z[44] + z[41];
    z[37]=z[5]*z[37];
    z[41]=3*z[7];
    z[44]= - static_cast<T>(6)+ z[36];
    z[44]=z[44]*z[41];
    z[56]=z[26] - 2;
    z[59]=z[36] + z[56];
    z[59]=z[16]*z[59];
    z[61]=8*z[4];
    z[44]=z[59] + z[61] + z[44];
    z[44]=z[16]*z[44];
    z[59]=z[54]*z[16];
    z[59]=z[59] + z[74];
    z[59]=z[59]*z[8];
    z[62]= - z[67] + z[7];
    z[41]=z[62]*z[41];
    z[62]= - z[4] + z[39];
    z[62]=z[16]*z[62];
    z[41]=z[59] + z[41] + z[62];
    z[41]=z[16]*z[41];
    z[41]= - z[74] + z[41];
    z[41]=z[8]*z[41];
    z[62]=7*z[4] - z[39];
    z[62]=z[7]*z[62];
    z[41]=z[41] + z[62] + z[44];
    z[44]=2*z[8];
    z[41]=z[41]*z[44];
    z[62]=z[64] - z[51];
    z[62]=z[62]*z[16];
    z[64]=7*z[26];
    z[67]=z[62] + static_cast<T>(15)- z[64];
    z[40]= - 23*z[13] + z[40];
    z[40]=z[7]*z[40];
    z[40]=z[40] + 2*z[67];
    z[40]=z[16]*z[40];
    z[67]=static_cast<T>(17)- z[57];
    z[67]=z[7]*z[67];
    z[40]=z[41] + z[40] - 12*z[4] + z[67];
    z[40]=z[8]*z[40];
    z[41]=z[23] - z[20];
    z[67]=z[16]*z[41];
    z[72]=3*z[51];
    z[67]=z[67] - 10*z[6] + z[72];
    z[43]= - z[43] + 17*z[13] + 2*z[67];
    z[43]=z[16]*z[43];
    z[67]=z[30]*z[13];
    z[75]= - z[13] + z[67];
    z[75]=z[75]*z[73];
    z[40]=z[75] + z[40] + z[43] + z[57] - static_cast<T>(13)+ 6*z[26];
    z[40]=z[9]*z[40];
    z[41]=z[41]*z[32];
    z[41]=z[41] - z[21];
    z[41]=z[41]*z[16];
    z[20]=3*z[20];
    z[43]=z[41] - z[20] + 7*z[23];
    z[43]=z[16]*z[43];
    z[20]= - z[20] - z[23];
    z[20]=z[13]*z[20];
    z[20]=z[20] - z[21];
    z[20]=z[16]*z[20];
    z[20]= - z[24] + z[20];
    z[20]=z[5]*z[20];
    z[20]=z[20] + z[43] - z[6] + z[32];
    z[20]=z[5]*z[20];
    z[21]= - z[67] + z[29];
    z[23]= - z[8]*z[28];
    z[18]=z[18] + z[23];
    z[18]=z[18]*z[73];
    z[18]=3*z[21] + z[18];
    z[18]=z[9]*z[18];
    z[21]=z[51] - z[34];
    z[23]=z[16]*z[21];
    z[23]=z[23] - static_cast<T>(4)+ z[26];
    z[24]=z[38] - z[63];
    z[24]=z[16]*z[4]*z[24];
    z[24]= - z[55] + z[24];
    z[24]=z[8]*z[24];
    z[18]=z[18] + z[24] + 3*z[23] + z[20];
    z[18]=z[15]*z[18];
    z[20]=static_cast<T>(1)- z[66];
    z[20]=z[20]*z[47];
    z[23]=z[76]*z[55];
    z[24]= - z[5]*z[23];
    z[28]=z[5]*z[47];
    z[28]=z[68] + z[28];
    z[28]=z[1]*z[28];
    z[20]=z[28] + z[20] + z[24];
    z[20]=z[1]*z[20];
    z[24]=z[19] - z[33];
    z[24]=z[24]*z[16];
    z[28]= - static_cast<T>(2)- z[24];
    z[28]=z[16]*z[28];
    z[21]=z[4]*z[21];
    z[19]=z[13] - z[19];
    z[19]=z[16]*z[19];
    z[19]=z[19] + static_cast<T>(3)+ z[21];
    z[19]=z[5]*z[19];
    z[21]=z[52]*z[4];
    z[33]=static_cast<T>(3)+ z[21];
    z[33]=z[4]*z[33];
    z[19]=z[20] + z[19] + z[33] + z[28];
    z[19]=z[5]*z[19];
    z[20]=static_cast<T>(3)- z[66];
    z[20]=z[20]*z[55];
    z[27]=z[5]*z[27];
    z[27]=z[27] - static_cast<T>(2)+ 3*z[26];
    z[27]=z[5]*z[27];
    z[20]=z[20] + z[27];
    z[20]=z[15]*z[20];
    z[27]=z[5]*z[56];
    z[23]=z[23] + z[27];
    z[23]=z[5]*z[23];
    z[27]=z[5]*z[4];
    z[27]=z[27] + z[69];
    z[27]=z[27]*z[5];
    z[28]=z[2]*z[27];
    z[23]=z[28] - z[69] + z[23];
    z[23]=z[15]*z[23];
    z[23]= - z[27] + z[23];
    z[23]=z[2]*z[23];
    z[19]=z[23] + z[20] + z[19];
    z[19]=z[2]*z[19];
    z[20]=z[7]*z[31];
    z[20]=static_cast<T>(3)+ z[20];
    z[20]=2*z[20] - z[42];
    z[20]=z[8]*z[20];
    z[23]=z[29] + z[2];
    z[20]=z[20] - 3*z[23];
    z[20]=z[15]*z[20];
    z[23]= - static_cast<T>(2)- 5*z[36];
    z[23]=z[23]*z[30];
    z[27]=static_cast<T>(1)+ z[53];
    z[28]=z[9]*z[50];
    z[27]=4*z[27] + z[28];
    z[27]=z[1]*z[27];
    z[27]=z[27] + z[35] + z[13] - z[8];
    z[27]=z[11]*z[27];
    z[28]=z[1]*z[7];
    z[20]=z[27] - 7*z[28] + z[23] + static_cast<T>(3)+ z[57] + z[20];
    z[20]=z[12]*z[20];
    z[23]= - z[46] + z[41];
    z[23]=z[15]*z[23];
    z[27]= - z[60]*z[32];
    z[27]=z[27] + z[71];
    z[27]=z[16]*z[27];
    z[23]=z[23] + z[27] - z[48];
    z[23]=z[16]*z[23];
    z[24]= - static_cast<T>(3)- z[24];
    z[24]=z[16]*z[24];
    z[24]=z[4] + z[24];
    z[24]=z[2]*z[24];
    z[27]=z[8]*z[66];
    z[27]=z[72] + z[27];
    z[27]=z[11]*z[27];
    z[28]=z[8]*z[55];
    z[23]=z[27] + z[24] + z[28] + 4*z[76] + z[23];
    z[23]=z[17]*z[23];
    z[24]=z[47] - z[54];
    z[24]=z[8]*z[24];
    z[27]=z[47]*z[30];
    z[27]= - z[54] + z[27];
    z[27]=z[1]*z[27];
    z[24]=2*z[11] + z[27] + z[4] + z[24];
    z[24]=z[1]*z[24];
    z[21]=z[16]*z[21];
    z[21]=z[70] + z[21];
    z[21]=z[8]*z[21];
    z[21]=z[21] - z[62] + z[24] + z[65];
    z[21]=z[10]*z[21];
    z[24]=z[58]*z[39]*z[16];
    z[27]=z[7]*z[52];
    z[24]=z[24] + 2*z[76] - 5*z[27];
    z[24]=z[16]*z[24];
    z[27]=static_cast<T>(3)- z[26];
    z[27]=z[7]*z[27];
    z[28]= - z[16]*z[56];
    z[27]=z[28] - z[61] + z[27];
    z[27]=z[7]*z[27];
    z[27]=z[59] + z[27];
    z[27]=z[16]*z[27];
    z[27]= - z[74] + z[27];
    z[27]=z[27]*z[44];
    z[28]= - z[39]*z[62];
    z[26]= - static_cast<T>(27)+ 13*z[26];
    z[26]=z[7]*z[26];
    z[26]=z[26] + z[28];
    z[26]=z[16]*z[26];
    z[26]=z[27] + 11*z[54] + z[26];
    z[26]=z[8]*z[26];
    z[27]=static_cast<T>(3)+ z[66];
    z[27]=z[4]*z[27];
    z[28]=static_cast<T>(4)- z[64];
    z[28]=z[7]*z[28];

    r += z[18] + z[19] + z[20] + 2*z[21] + z[22] + z[23] + z[24] + 
      z[25] + z[26] + z[27] + z[28] + z[37] + z[40] + z[45] + z[49];
 
    return r;
}

template double qqb_1l_r54(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1l_r54(const std::array<dd_real,31>&);
#endif
