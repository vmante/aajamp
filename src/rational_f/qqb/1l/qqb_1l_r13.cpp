#include "qqb_1l_rf_decl.hpp"

template<class T>
T qqb_1l_r13(const std::array<T,31>& k) {
  T z[31];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[10];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[9];
    z[8]=k[2];
    z[9]=k[14];
    z[10]=k[5];
    z[11]=11*z[9];
    z[12]=z[5]*z[6];
    z[13]=z[11]*z[12];
    z[11]=z[13] + z[11] - 7*z[6];
    z[11]=z[5]*z[11];
    z[14]=z[6]*z[1];
    z[15]=z[9]*z[2];
    z[11]=z[11] + 4*z[14] - static_cast<T>(4)- 7*z[15];
    z[11]=z[5]*z[11];
    z[16]=z[1]*z[7];
    z[17]=2*z[16];
    z[18]=z[2]*z[7];
    z[19]= - z[18] + z[17] - 3;
    z[20]= - z[2]*z[19];
    z[21]=npow(z[1],2);
    z[22]=z[21]*z[6];
    z[23]=2*z[22];
    z[24]=z[9]*npow(z[2],2);
    z[25]= - static_cast<T>(1)+ z[16];
    z[25]=z[1]*z[25];
    z[11]=z[11] - z[23] + z[24] + 2*z[25] + z[20];
    z[11]=z[4]*z[11];
    z[20]=z[12] + 1;
    z[25]=5*z[9];
    z[26]=z[25]*z[20];
    z[27]=4*z[6];
    z[26]= - z[27] + z[26];
    z[26]=z[5]*z[26];
    z[28]=static_cast<T>(4)+ 5*z[15];
    z[26]=z[26] + 3*z[14] - z[28];
    z[26]=z[5]*z[26];
    z[29]=2*z[2];
    z[23]=z[26] - z[23] + 2*z[24] + 3*z[1] + z[29];
    z[23]=z[5]*z[23];
    z[26]=z[2]*z[1];
    z[23]=z[23] + z[26] - 2*z[21];
    z[23]=z[4]*z[23];
    z[30]=z[15] + 1;
    z[20]=z[9]*z[20];
    z[20]= - z[6] + z[20];
    z[20]=z[5]*z[20];
    z[14]=z[20] + z[14] - z[30];
    z[14]=z[5]*z[14];
    z[20]=z[24] + z[2] + z[1];
    z[14]=z[14] - z[22] + z[20];
    z[14]=z[5]*z[14];
    z[14]=z[14] - z[21] - z[26];
    z[14]=z[5]*z[14];
    z[21]=z[26] - z[21];
    z[22]= - z[2]*z[21];
    z[14]=z[22] + z[14];
    z[14]=z[4]*z[14];
    z[22]=z[6]*z[30];
    z[24]= - z[9]*z[12];
    z[22]=z[22] + z[24];
    z[22]=z[5]*z[22];
    z[24]=z[20]*z[6];
    z[22]= - z[24] + z[22];
    z[22]=z[22]*npow(z[5],2);
    z[14]=z[14] + z[22] + z[21];
    z[14]=z[3]*z[14];
    z[12]= - z[12]*z[25];
    z[21]=z[6]*z[28];
    z[12]=z[21] + z[12];
    z[12]=z[5]*z[12];
    z[12]= - 3*z[24] + z[12];
    z[12]=z[5]*z[12];
    z[12]=z[14] + z[12] + z[23];
    z[12]=z[3]*z[12];
    z[14]= - z[20]*z[27];
    z[20]=static_cast<T>(7)+ 10*z[15];
    z[20]=z[6]*z[20];
    z[20]=z[20] - z[13];
    z[20]=z[5]*z[20];
    z[21]=z[7]*z[8];
    z[11]=z[12] + z[11] + z[20] + z[14] - z[15] + z[21] + z[19];
    z[11]=z[3]*z[11];
    z[12]=z[10]*z[25];
    z[12]= - static_cast<T>(4)+ z[12];
    z[12]=z[6]*z[12];
    z[12]=z[13] + 9*z[9] + z[12];
    z[12]=z[5]*z[12];
    z[14]=2*z[18];
    z[15]=z[7]*z[10];
    z[19]=z[10] - z[2];
    z[19]=z[9]*z[19];
    z[20]=npow(z[10],2);
    z[21]=z[9]*z[20];
    z[21]= - z[10] + z[21];
    z[21]=z[6]*z[21];
    z[12]=z[12] + z[21] + 3*z[19] - z[14] + z[17] - static_cast<T>(4)+ z[15];
    z[12]=z[4]*z[12];
    z[16]= - z[18] + static_cast<T>(2)- z[16];
    z[15]= - z[18] + static_cast<T>(4)+ z[15];
    z[15]=z[15]*z[29];
    z[17]= - z[7]*z[20];
    z[15]=z[15] - 5*z[10] + z[17];
    z[15]=z[9]*z[15];
    z[15]=2*z[16] + z[15];
    z[15]=z[6]*z[15];
    z[16]= - z[8] + z[10];
    z[16]=z[7]*z[16];
    z[14]= - z[14] + static_cast<T>(2)+ z[16];
    z[14]=z[9]*z[14];

    r +=  - z[7] + z[11] + z[12] - z[13] + z[14] + z[15];
 
    return r;
}

template double qqb_1l_r13(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1l_r13(const std::array<dd_real,31>&);
#endif
