#include "qqb_1l_rf_decl.hpp"

template<class T>
T qqb_1l_r18(const std::array<T,31>& k) {
  T z[56];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[12];
    z[6]=k[18];
    z[7]=k[13];
    z[8]=k[24];
    z[9]=k[3];
    z[10]=k[9];
    z[11]=k[10];
    z[12]=k[4];
    z[13]=k[2];
    z[14]=k[11];
    z[15]=k[14];
    z[16]=k[15];
    z[17]=k[23];
    z[18]=npow(z[2],2);
    z[19]=z[18]*z[12];
    z[20]=npow(z[2],3);
    z[21]=z[19] + z[20];
    z[21]=z[21]*z[14];
    z[22]=z[12]*z[2];
    z[21]=z[21] - z[22];
    z[23]=z[3]*npow(z[2],4);
    z[24]= - 2*z[20] - z[23];
    z[24]=z[3]*z[24];
    z[24]=z[24] - z[21];
    z[24]=z[5]*z[24];
    z[25]=z[20]*z[3];
    z[26]= - 4*z[18] - z[25];
    z[26]=z[3]*z[26];
    z[27]=z[18]*z[3];
    z[28]=2*z[2];
    z[29]=z[27] + z[28];
    z[30]=z[3]*z[29];
    z[30]=static_cast<T>(3)+ z[30];
    z[30]=z[12]*z[30];
    z[31]=2*z[18];
    z[32]=z[31] - z[25];
    z[33]=z[2] + z[27];
    z[33]=z[12]*z[33];
    z[33]=z[33] - z[32];
    z[33]=z[15]*z[33];
    z[34]=z[18]*z[14];
    z[24]=z[33] + z[24] - z[34] + z[26] + z[30];
    z[24]=z[6]*z[24];
    z[26]=z[18] + z[25];
    z[30]=2*z[3];
    z[26]=z[26]*z[30];
    z[21]=z[18] - z[21];
    z[21]=z[14]*z[21];
    z[21]=z[26] + z[21];
    z[21]=z[5]*z[21];
    z[26]= - z[3]*z[32];
    z[32]=z[27] - z[2];
    z[33]=z[3]*z[32];
    z[33]= - static_cast<T>(1)+ z[33];
    z[33]=z[12]*z[33];
    z[26]=z[33] + z[28] + z[26];
    z[26]=z[15]*z[26];
    z[33]=z[12]*z[3];
    z[35]=z[3]*z[2];
    z[36]=3*z[35];
    z[37]= - static_cast<T>(2)- z[36];
    z[37]=z[37]*z[33];
    z[38]=3*z[12];
    z[39]= - z[34] + z[2] + z[38];
    z[39]=z[14]*z[39];
    z[40]=6*z[2] + z[27];
    z[40]=z[3]*z[40];
    z[21]=z[24] + z[26] + z[21] + z[39] + z[37] - static_cast<T>(3)+ z[40];
    z[21]=z[6]*z[21];
    z[24]=npow(z[12],2);
    z[26]=z[4]*z[24]*z[34];
    z[37]=z[19]*z[15];
    z[26]=z[26] + z[37];
    z[39]=z[12]*z[34];
    z[39]=z[39] - z[26];
    z[39]=z[7]*z[39];
    z[40]=npow(z[3],2);
    z[41]=z[20]*z[40];
    z[29]= - z[29]*z[33];
    z[42]=z[14]*z[12];
    z[43]= - z[28]*z[42];
    z[23]=z[23] - z[20];
    z[44]=z[5]*z[3];
    z[45]=z[23]*z[44];
    z[29]=z[45] + z[43] + z[29] - z[2] + z[41] + z[39];
    z[29]=z[4]*z[29];
    z[39]=3*z[18];
    z[41]=z[39] + z[22];
    z[41]=z[41]*z[15];
    z[37]=z[22] - z[37];
    z[37]=z[7]*z[37];
    z[37]=z[37] + z[41] - z[28] - z[34];
    z[37]=z[7]*z[37];
    z[43]=z[25] - z[18];
    z[43]=z[43]*z[30]*z[5];
    z[45]= - z[40]*z[31];
    z[46]=2*z[35];
    z[47]=static_cast<T>(3)+ z[46];
    z[47]=z[47]*z[33];
    z[48]=z[28]*z[14];
    z[29]=z[29] + z[37] - z[43] + z[48] + z[45] + z[47];
    z[29]=z[4]*z[29];
    z[37]=z[15]*z[34];
    z[37]=z[33] + z[37];
    z[45]=npow(z[14],2);
    z[47]=z[45]*z[9];
    z[49]=z[18]*z[15];
    z[50]=z[49]*z[47];
    z[51]=z[14]*z[2];
    z[52]=3*z[51];
    z[37]=z[50] + z[52] - z[35] - 2*z[37];
    z[37]=z[9]*z[37];
    z[37]=z[37] + z[49] - z[2] + z[12];
    z[37]=z[16]*z[37];
    z[47]=z[28]*z[47];
    z[47]=z[47] - 4*z[51];
    z[47]=z[15]*z[47];
    z[48]=z[48] + 1;
    z[49]=z[14]*z[48];
    z[47]=z[3] + z[49] + z[47];
    z[47]=z[9]*z[47];
    z[49]=z[35] + 1;
    z[50]=z[12] + z[2];
    z[53]=z[14]*z[50];
    z[54]=z[15]*z[28];
    z[37]=z[37] + z[47] + z[54] + z[53] - z[33] - z[49];
    z[37]=z[16]*z[37];
    z[19]=z[19] + z[23];
    z[19]=z[14]*z[19];
    z[19]=z[19] - z[22] + z[31] - 3*z[25];
    z[19]=z[14]*z[19];
    z[23]=z[28] + z[12];
    z[31]=z[27] - z[23];
    z[19]=2*z[31] + z[19];
    z[19]=z[5]*z[19];
    z[31]=z[35]*z[12];
    z[31]=z[31] - z[27];
    z[47]=z[12]*z[31];
    z[47]=z[25] + z[47];
    z[47]=z[14]*z[47];
    z[31]=3*z[31] + z[47];
    z[31]=z[14]*z[31];
    z[47]=z[5]*z[12];
    z[50]=z[50]*z[47];
    z[53]=z[23]*z[12];
    z[53]=z[53] + z[18];
    z[54]= - z[53]*z[47];
    z[54]= - z[24] + z[54];
    z[54]=z[15]*z[54];
    z[50]=z[50] + z[54];
    z[50]=z[7]*z[50];
    z[53]=z[5]*z[53];
    z[53]=z[12] + z[53];
    z[53]=z[15]*z[53];
    z[19]=z[50] + 3*z[53] + z[19] + z[31] - static_cast<T>(1)+ z[46];
    z[19]=z[7]*z[19];
    z[31]=z[30]*z[18];
    z[50]=z[31] + z[2];
    z[50]=z[50]*z[3];
    z[20]= - z[20]*z[30];
    z[20]= - z[18] + z[20];
    z[20]=z[3]*z[20];
    z[20]=z[28] + z[20];
    z[20]=z[5]*z[20];
    z[20]=z[20] - static_cast<T>(3)- z[50];
    z[20]=z[6]*z[20];
    z[30]= - z[2] + z[31];
    z[30]=z[30]*z[44];
    z[36]= - static_cast<T>(1)+ z[36];
    z[36]=z[3]*z[36];
    z[20]=z[20] + z[36] + z[30];
    z[20]=z[6]*z[20];
    z[30]=z[50] + z[43];
    z[30]=z[4]*z[30];
    z[32]=z[32]*z[44];
    z[36]=z[49]*z[3];
    z[43]=z[7] - z[36] - z[32];
    z[30]=2*z[43] + z[30];
    z[30]=z[4]*z[30];
    z[43]=npow(z[4],2);
    z[50]= - npow(z[6],2);
    z[50]=z[43] + z[50];
    z[32]=z[32]*z[50];
    z[50]=z[35] - 1;
    z[44]=z[50]*z[44];
    z[53]= - z[7]*z[5]*z[50];
    z[53]= - z[44] + z[53];
    z[53]=z[7]*z[53];
    z[32]=z[53] + z[32];
    z[32]=z[1]*z[32];
    z[47]=static_cast<T>(1)- z[47];
    z[47]=z[7]*z[47];
    z[47]=3*z[3] + z[47];
    z[47]=z[7]*z[47];
    z[20]=z[32] + z[30] + z[20] + z[47];
    z[20]=z[1]*z[20];
    z[18]=z[18]*z[5];
    z[30]=z[18] + z[2];
    z[30]=z[30]*z[15];
    z[32]=z[1] + z[2];
    z[32]=z[7]*z[32];
    z[47]= - z[5]*z[28];
    z[53]=z[5]*z[30];
    z[53]=z[53] - 2*z[7];
    z[53]=z[9]*z[53];
    z[54]=z[9] - z[1];
    z[55]=z[7]*z[54];
    z[55]= - static_cast<T>(1)+ z[55];
    z[55]=z[9]*z[55];
    z[55]=z[1] + z[55];
    z[55]=z[11]*z[55];
    z[30]=z[55] + z[53] + z[30] - static_cast<T>(1)+ z[47] + z[32];
    z[30]=z[10]*z[30];
    z[32]=3*z[2];
    z[47]=z[32] - z[27];
    z[47]=z[3]*z[47];
    z[40]=z[40]*z[12];
    z[46]=static_cast<T>(3)- z[46];
    z[46]=z[3]*z[46];
    z[46]=z[46] - z[40];
    z[46]=z[12]*z[46];
    z[53]=static_cast<T>(2)- z[35];
    z[53]=z[3]*z[53];
    z[40]=z[53] - z[40];
    z[40]=z[12]*z[40];
    z[40]=z[40] + z[50];
    z[40]=z[40]*z[42];
    z[23]=z[5]*z[23];
    z[23]= - 2*z[23] + z[40] + z[46] - static_cast<T>(2)+ z[47];
    z[23]=z[15]*z[23];
    z[40]=z[7] - z[6];
    z[40]=z[40]*z[54];
    z[46]=z[7]*z[12];
    z[47]= - z[6]*z[12];
    z[40]=z[47] + z[46] + z[40];
    z[40]=z[11]*z[40];
    z[46]=z[46] + z[33];
    z[47]= - z[15]*z[46];
    z[40]=z[40] + z[47];
    z[40]=z[9]*z[40];
    z[47]= - z[9]*z[3];
    z[47]= - static_cast<T>(1)+ z[47];
    z[24]=z[16]*z[24]*z[47];
    z[47]=z[9]*z[33];
    z[24]=z[47] + z[24];
    z[24]=z[16]*z[24];
    z[47]=z[6] - z[4];
    z[47]=z[1]*z[47];
    z[24]=z[24] + 2*z[47] + static_cast<T>(1)+ z[40] - z[46];
    z[24]=z[11]*z[24];
    z[40]= - z[39] + z[22];
    z[40]=z[40]*z[42];
    z[22]=3*z[22] + z[40] + z[26];
    z[22]=z[4]*z[22];
    z[26]= - z[12]*z[28];
    z[26]=z[39] + z[26];
    z[26]=z[14]*z[26];
    z[22]=z[22] - z[41] - z[38] + z[26];
    z[22]=z[4]*z[22];
    z[18]=z[2] - z[18];
    z[18]=z[9]*z[18];
    z[18]=z[18] - z[39];
    z[18]=z[5]*z[18];
    z[18]=z[28] + z[18];
    z[18]=z[15]*z[18];
    z[26]=z[32] + z[34];
    z[26]=z[5]*z[26];
    z[18]=z[22] + z[26] + static_cast<T>(3)+ z[51] + z[18];
    z[18]=z[17]*z[18];
    z[22]=z[1]*z[44];
    z[26]= - z[5]*z[35];
    z[22]=z[22] - 2*z[4] - z[36] + z[26];
    z[22]=z[1]*z[22];
    z[26]=z[49]*z[33];
    z[32]=z[35] + 2;
    z[35]= - z[26] + z[32];
    z[35]=z[35]*z[42];
    z[22]=z[22] + z[35] - static_cast<T>(1)- z[26];
    z[22]=z[8]*z[22];
    z[26]=z[32] - z[33];
    z[32]= - z[12]*z[26];
    z[27]=z[27] + z[32];
    z[27]=z[14]*z[27];
    z[26]= - 2*z[26] + z[27];
    z[26]=z[14]*z[26];
    z[27]= - z[2] - z[34];
    z[27]=z[6]*z[27];
    z[27]=z[48] + z[27];
    z[27]=z[9]*z[14]*z[27];
    z[27]=z[27] - static_cast<T>(2)- 5*z[51];
    z[27]=z[15]*z[27];
    z[28]=z[28] + 3*z[34];
    z[28]=z[15]*z[28];
    z[32]=static_cast<T>(2)- z[51];
    z[28]=3*z[32] + z[28];
    z[28]=z[6]*z[28];
    z[32]=static_cast<T>(4)+ z[52];
    z[32]=z[14]*z[32];
    z[27]=z[28] + z[32] + z[27];
    z[27]=z[6]*z[27];
    z[28]= - z[3] + z[5];
    z[28]=z[15]*z[28];
    z[27]=z[28] + z[27];
    z[27]=z[9]*z[27];
    z[28]=z[43]*z[51];
    z[32]= - z[8]*z[14];
    z[28]=z[32] + z[45] + z[28];
    z[28]=z[13]*z[28];
    z[25]=z[14]*z[25];
    z[25]= - z[31] + z[25];
    z[25]=z[14]*z[25];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[5]*z[25];

    r += z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25]
       + z[26] + z[27] + z[28] + z[29] + z[30] + z[37];
 
    return r;
}

template double qqb_1l_r18(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1l_r18(const std::array<dd_real,31>&);
#endif
