#include "qqb_1l_rf_decl.hpp"

template<class T>
T qqb_1l_r50(const std::array<T,31>& k) {
  T z[43];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[20];
    z[7]=k[5];
    z[8]=k[7];
    z[9]=k[2];
    z[10]=k[11];
    z[11]=k[3];
    z[12]=k[16];
    z[13]=k[14];
    z[14]=z[5]*z[2];
    z[15]=z[14]*z[3];
    z[16]=z[3]*z[2];
    z[17]= - z[10]*z[16];
    z[17]=z[15] + z[17];
    z[17]=z[13]*z[17];
    z[18]=z[14] + 1;
    z[19]=z[18]*z[3];
    z[20]=z[5]*z[19];
    z[21]=z[10]*z[3];
    z[22]=z[16] + 1;
    z[22]=z[22]*z[3];
    z[23]= - z[8]*z[22];
    z[17]=z[17] + z[21] - z[20] + z[23];
    z[17]=z[11]*z[17];
    z[23]=2*z[2];
    z[24]=npow(z[2],2);
    z[25]=z[24]*z[3];
    z[26]=z[25] + z[23] + z[7];
    z[26]=z[8]*z[26];
    z[27]= - z[24]*z[21];
    z[28]=2*z[14];
    z[26]=z[27] - z[28] + z[26];
    z[26]=z[13]*z[26];
    z[27]=z[25] - z[2];
    z[29]=z[27]*z[21];
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=z[5]*z[28];
    z[27]= - z[3]*z[27];
    z[27]=static_cast<T>(2)+ z[27];
    z[27]=z[8]*z[27];
    z[17]=z[17] + z[26] + z[29] + z[27] + z[28] + z[15];
    z[17]=z[13]*z[17];
    z[26]=npow(z[3],2);
    z[27]= - z[26]*z[23];
    z[28]=z[8]*z[7];
    z[29]=npow(z[7],2);
    z[30]=z[29]*z[8];
    z[31]= - z[4]*z[30];
    z[31]= - z[28] + z[31];
    z[31]=z[4]*z[31];
    z[25]=z[2] + z[25];
    z[25]=z[8]*z[3]*z[25];
    z[25]=z[31] + z[27] + z[25];
    z[25]=z[10]*z[25];
    z[27]=npow(z[4],2)*z[28];
    z[17]=z[25] - z[26] + z[27] + z[17];
    z[17]=z[11]*z[17];
    z[25]=z[3]*npow(z[2],4);
    z[27]=npow(z[2],3);
    z[25]=z[25] + 2*z[27];
    z[31]= - z[3]*z[25];
    z[32]= - z[7]*z[2];
    z[31]=z[31] - 2*z[24] + z[32];
    z[31]=z[8]*z[31];
    z[32]=z[27]*z[26];
    z[31]=z[31] + z[2] + z[32];
    z[31]=z[10]*z[31];
    z[33]=z[27]*z[3];
    z[33]=z[33] + z[24];
    z[34]=z[7]*z[24];
    z[25]=z[34] + z[25];
    z[25]=z[8]*z[25];
    z[25]=z[25] - z[33];
    z[25]=z[10]*z[25];
    z[34]= - z[2] - z[7];
    z[34]=z[7]*z[34];
    z[33]=z[34] + z[33];
    z[33]=z[8]*z[33];
    z[34]=z[7]*z[14];
    z[25]=z[25] + z[34] + z[33];
    z[25]=z[13]*z[25];
    z[33]=z[14] - 2;
    z[34]=z[7]*z[5];
    z[35]= - z[33]*z[34];
    z[36]= - z[7] - z[32];
    z[36]=z[8]*z[36];
    z[25]=z[25] + z[31] + z[36] - z[14] + z[35];
    z[25]=z[13]*z[25];
    z[31]= - static_cast<T>(2)- z[34];
    z[31]=z[31]*z[28];
    z[31]=z[31] + z[34] - z[33];
    z[31]=z[4]*z[31];
    z[35]=npow(z[5],2);
    z[36]=z[35]*z[2];
    z[37]=z[35]*z[7];
    z[36]=z[36] - z[37];
    z[38]=z[37] - z[5];
    z[39]=z[38]*z[28];
    z[15]=z[15] - z[5];
    z[40]=z[8]*z[34];
    z[40]=z[40] + z[15];
    z[40]=z[4]*z[40];
    z[41]= - z[3]*z[15];
    z[40]=z[41] + z[40];
    z[40]=z[1]*z[40];
    z[41]= - static_cast<T>(3)+ z[14];
    z[41]=z[3]*z[41];
    z[31]=z[40] + z[31] + z[39] + z[41] + z[36];
    z[31]=z[4]*z[31];
    z[31]=z[35] + z[26] + z[31];
    z[31]=z[1]*z[31];
    z[39]=2*z[5];
    z[40]=z[24]*z[39];
    z[40]= - z[2] + z[40];
    z[40]=z[5]*z[40];
    z[41]=z[14] - 1;
    z[42]=z[41]*z[34];
    z[40]=z[40] + z[42];
    z[40]=z[7]*z[40];
    z[27]=z[27]*z[35];
    z[27]=z[27] + z[40];
    z[27]=z[4]*z[27];
    z[35]=3*z[14];
    z[40]= - static_cast<T>(1)- z[35];
    z[40]=z[5]*z[40];
    z[20]=z[11]*z[20];
    z[19]=z[20] + z[40] - z[19];
    z[19]=z[11]*z[19];
    z[20]=z[5]*z[24];
    z[20]=z[23] + z[20];
    z[20]=z[5]*z[20];
    z[23]= - static_cast<T>(1)+ z[35];
    z[23]=z[23]*z[34];
    z[19]=z[19] + z[27] + z[23] - static_cast<T>(2)+ z[20];
    z[19]=z[12]*z[19];
    z[20]=z[41] - z[34];
    z[23]=z[7]*z[20];
    z[24]=z[34] + 1;
    z[27]=z[24]*z[30];
    z[23]=z[23] + z[27];
    z[23]=z[4]*z[23];
    z[27]= - z[7]*z[36];
    z[34]=z[38]*z[7];
    z[35]=static_cast<T>(2)- z[34];
    z[28]=z[35]*z[28];
    z[35]= - npow(z[14],2);
    z[23]=z[23] + z[28] + z[35] + z[27];
    z[23]=z[4]*z[23];
    z[27]=z[39] - z[37];
    z[27]=z[27]*z[7];
    z[28]=z[27] - z[41];
    z[28]=z[7]*z[28];
    z[27]= - static_cast<T>(3)- z[27];
    z[27]=z[27]*z[30];
    z[20]= - z[20]*z[29];
    z[24]= - npow(z[7],3)*z[24]*z[8];
    z[20]=z[20] + z[24];
    z[20]=z[4]*z[20];
    z[20]=z[20] + z[28] + z[27];
    z[20]=z[4]*z[20];
    z[18]=z[18]*z[5];
    z[18]= - z[18] + z[37];
    z[18]=z[7]*z[18];
    z[18]=z[18] + z[33];
    z[18]=z[7]*z[18];
    z[18]=z[32] - 5*z[2] + z[18];
    z[18]=z[8]*z[18];
    z[24]=z[16] + 2;
    z[18]=z[20] + 3*z[24] + z[18];
    z[18]=z[10]*z[18];
    z[15]=z[1]*z[15];
    z[15]=z[15] + z[16] - z[41];
    z[15]=z[3]*z[15];
    z[15]=2*z[8] + z[15];
    z[15]=z[1]*z[15];
    z[20]=z[11]*z[13];
    z[20]=z[20] + 1;
    z[20]=z[22]*z[20];
    z[22]= - z[13]*z[24];
    z[20]=z[22] + z[20];
    z[20]=z[11]*z[20];
    z[22]=z[13]*z[7];
    z[15]=z[15] + z[20] + static_cast<T>(1)+ z[22];
    z[15]=z[6]*z[15];
    z[20]=z[9] + z[11];
    z[20]=z[10]*z[20];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[26]*z[20];
    z[16]= - static_cast<T>(3)- z[16];
    z[16]=z[16]*z[21];
    z[21]=z[3] - 2*z[10];
    z[21]=z[12]*z[21];
    z[16]=z[21] + z[16] + z[20];
    z[16]=z[9]*z[16];
    z[14]= - static_cast<T>(2)- z[14];
    z[14]=z[5]*z[14];
    z[20]= - static_cast<T>(2)- z[34];
    z[20]=z[8]*z[20];

    r += 2*z[3] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20]
       + z[23] + z[25] + z[31];
 
    return r;
}

template double qqb_1l_r50(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1l_r50(const std::array<dd_real,31>&);
#endif
