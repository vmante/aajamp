#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r135(const std::array<T,31>& k) {
  T z[18];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[15];
    z[5]=k[3];
    z[6]=k[12];
    z[7]=k[5];
    z[8]=2*z[2];
    z[9]=npow(z[4],2);
    z[10]=z[8]*z[9];
    z[11]=z[10] + 3*z[4];
    z[11]=z[11]*z[2];
    z[11]=z[11] + 1;
    z[10]=z[10] + z[4];
    z[12]=z[7]*z[10];
    z[12]=z[11] + z[12];
    z[12]=z[2]*z[12];
    z[11]= - z[1]*z[11];
    z[11]=z[11] + z[12];
    z[11]=z[3]*z[11];
    z[12]=z[6]*npow(z[2],2);
    z[13]= - z[2] + z[12];
    z[14]=z[9]*z[2];
    z[15]=z[14] + z[4];
    z[13]=z[6]*z[15]*z[13];
    z[12]= - z[8] + z[12];
    z[16]=z[9]*z[7]*z[6];
    z[12]=z[12]*z[16];
    z[12]=z[12] + 2*z[13] + z[4] - 4*z[14];
    z[12]=z[7]*z[12];
    z[13]=z[6]*z[2];
    z[10]=z[10]*z[13];
    z[10]=z[10] - z[15];
    z[10]=z[6]*z[10];
    z[17]= - static_cast<T>(1)+ z[13];
    z[16]=z[17]*z[16];
    z[10]=z[10] + z[16];
    z[10]=z[7]*z[10];
    z[13]=npow(z[13],2);
    z[16]=z[15]*z[13];
    z[10]=z[16] + z[10];
    z[8]=z[8] + z[7];
    z[8]=z[7]*z[8]*npow(z[6],2);
    z[8]=z[13] + z[8];
    z[8]=z[5]*z[9]*z[8];
    z[8]=2*z[10] + z[8];
    z[8]=z[5]*z[8];
    z[9]=2*z[4] + z[14];
    z[9]=z[2]*z[9];
    z[9]=static_cast<T>(1)+ z[9];
    z[9]=z[9]*z[13];
    z[10]=z[2]*z[15];

    r += static_cast<T>(1)+ z[8] + z[9] - 3*z[10] + z[11] + z[12];
 
    return r;
}

template double qqb_1lha_r135(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r135(const std::array<dd_real,31>&);
#endif
