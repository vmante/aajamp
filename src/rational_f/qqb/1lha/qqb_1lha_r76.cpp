#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r76(const std::array<T,31>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[2];
    z[6]=k[10];
    z[7]=k[25];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=k[4];
    z[11]=k[29];
    z[12]=z[5]*z[9];
    z[13]=z[12] + 3;
    z[14]=z[11]*z[4];
    z[15]= - z[13]*z[14];
    z[16]=z[4]*z[1];
    z[17]=z[14]*z[8];
    z[18]=z[17] + z[4];
    z[19]=4*z[11] - z[18];
    z[19]=z[8]*z[19];
    z[19]=z[19] + static_cast<T>(3)- z[16];
    z[19]=z[3]*z[19];
    z[18]=z[19] - z[7] + 2*z[11] - 3*z[18];
    z[18]=z[3]*z[18];
    z[17]=2*z[17] - z[11] + z[7] + z[4];
    z[17]=z[3]*z[17];
    z[17]=z[17] + 3*z[14];
    z[17]=z[3]*z[17];
    z[14]= - z[2]*npow(z[3],2)*z[14];
    z[14]=z[17] + z[14];
    z[14]=z[2]*z[14];
    z[14]=z[14] + z[15] + z[18];
    z[14]=z[2]*z[14];
    z[15]=2*z[10];
    z[17]=z[15]*z[11];
    z[18]=z[8]*z[11];
    z[15]=z[7]*z[15];
    z[15]= - 3*z[18] + z[17] - static_cast<T>(3)+ z[15];
    z[15]=z[3]*z[8]*z[15];
    z[16]= - static_cast<T>(2)+ z[16];
    z[15]=z[15] + 2*z[16] - 5*z[18];
    z[15]=z[3]*z[15];
    z[16]= - z[10]*z[9];
    z[16]=z[16] - static_cast<T>(1)+ z[12];
    z[16]=z[11]*z[16];
    z[13]=z[4]*z[13];
    z[13]=z[14] + z[15] + z[16] + z[13] + 2*z[7];
    z[13]=z[2]*z[13];
    z[14]=2*z[6];
    z[14]=z[14]*z[5];
    z[14]=z[14] - 3;
    z[15]=z[10]*z[6];
    z[16]= - 4*z[15] + z[14];
    z[16]=z[7]*z[16];
    z[18]=z[8]*z[7];
    z[19]=z[18] - 1;
    z[19]=z[6]*z[19];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[4]*z[12];
    z[12]=z[12] + z[16] - z[9] + z[19];
    z[12]=z[8]*z[12];
    z[16]= - static_cast<T>(2)- z[15];
    z[16]=z[16]*z[17];
    z[17]=2*z[15];
    z[18]= - z[18]*z[17];
    z[17]=static_cast<T>(3)+ z[17];
    z[17]=z[17]*z[7]*z[10];
    z[16]=z[18] + z[17] + z[16];
    z[16]=z[8]*z[16];
    z[17]=z[15] + 3;
    z[18]=z[11]*z[10];
    z[17]= - z[17]*z[18];
    z[19]=npow(z[10],2)*z[7]*z[6];
    z[17]=z[19] + z[17];
    z[17]=z[3]*z[17]*npow(z[8],2);
    z[16]=z[16] + z[17];
    z[16]=z[3]*z[16];
    z[14]=z[15] - z[14];
    z[14]=z[10]*z[14];
    z[17]=z[6]*npow(z[5],2);
    z[14]=z[14] - 3*z[5] + z[17];
    z[14]=z[7]*z[14];
    z[15]= - static_cast<T>(1)- z[15];
    z[15]=z[15]*z[18];
    z[17]= - z[6]*z[5];
    z[18]= - z[1] - z[5];
    z[18]=z[4]*z[18];

    r += static_cast<T>(2)+ z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[18];
 
    return r;
}

template double qqb_1lha_r76(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r76(const std::array<dd_real,31>&);
#endif
