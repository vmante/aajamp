#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r215(const std::array<T,31>& k) {
  T z[4];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];

    r += z[2]*z[1];
 
    return r;
}

template double qqb_1lha_r215(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r215(const std::array<dd_real,31>&);
#endif
