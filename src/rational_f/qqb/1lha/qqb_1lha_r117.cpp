#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r117(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[14];
    z[5]=k[5];
    z[6]=k[16];
    z[7]=z[5]*z[6];
    z[8]=2*z[7];
    z[9]=z[8] - 1;
    z[9]=z[9]*z[4];
    z[10]=z[2]*z[4];
    z[11]= - z[6]*z[10];
    z[11]= - z[9] + z[11];
    z[11]=z[2]*z[11];
    z[12]=static_cast<T>(2)- z[7];
    z[12]=z[5]*z[12];
    z[12]= - z[1] + z[12];
    z[12]=z[4]*z[12];
    z[8]=z[11] + z[12] - static_cast<T>(1)- z[8];
    z[8]=z[2]*z[8];
    z[11]=npow(z[5],2);
    z[12]= - z[6]*z[11];
    z[13]= - 2*z[1] + z[5];
    z[13]=z[4]*z[5]*z[13];
    z[8]=z[8] + z[12] + z[13];
    z[8]=z[2]*z[8];
    z[12]=z[6]*z[1];
    z[13]= - z[4]*z[1];
    z[13]=z[12] + z[13];
    z[13]=z[11]*z[13];
    z[8]=z[8] + z[13];
    z[8]=z[3]*z[8];
    z[12]=z[12] + z[7];
    z[12]=z[5]*z[12];
    z[13]=2*z[6];
    z[13]= - z[13]*z[10];
    z[9]=z[13] + z[6] - z[9];
    z[9]=z[2]*z[9];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[2]*z[9];
    z[11]= - z[4]*z[11];
    z[8]=z[8] + z[9] + z[12] + z[11];
    z[8]=z[3]*z[8];
    z[9]=static_cast<T>(1)- z[10];
    z[9]=z[2]*z[6]*z[9];

    r += z[7] + z[8] + z[9];
 
    return r;
}

template double qqb_1lha_r117(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r117(const std::array<dd_real,31>&);
#endif
