#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r174(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[14];
    z[5]=k[5];
    z[6]=k[15];
    z[7]=2*z[4];
    z[8]=z[5] + z[2];
    z[9]= - z[5]*z[8]*z[7];
    z[9]=z[9] + 3*z[2];
    z[9]=z[4]*z[9];
    z[10]=z[2]*z[6];
    z[11]=z[8]*z[4];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[4]*z[11];
    z[11]= - z[6] + z[11];
    z[11]=z[1]*z[11];
    z[9]=z[11] - 3*z[10] + z[9];
    z[9]=z[1]*z[9];
    z[11]=3*z[6];
    z[12]=npow(z[2],2);
    z[11]= - z[12]*z[11];
    z[13]=npow(z[5],2);
    z[14]=z[13]*z[4];
    z[8]=z[8]*z[14];
    z[15]= - 4*z[2] - z[5];
    z[15]=z[5]*z[15];
    z[8]=z[15] + z[8];
    z[8]=z[4]*z[8];
    z[8]=z[9] + z[11] + z[8];
    z[8]=z[1]*z[8];
    z[9]= - static_cast<T>(1)- z[10];
    z[9]=z[9]*z[12];
    z[10]=z[14] - z[5];
    z[11]=z[2]*z[10];
    z[8]=z[8] + z[9] + z[11];
    z[8]=z[3]*z[8];
    z[9]=z[4]*npow(z[5],3);
    z[9]= - 2*z[13] + z[9];
    z[9]=z[4]*z[9];
    z[7]= - z[10]*z[7];
    z[10]=z[1]*z[5]*npow(z[4],2);
    z[7]=z[7] + z[10];
    z[7]=z[1]*z[7];
    z[7]=z[8] + z[7] + z[5] + z[9];

    r += z[7]*z[3];
 
    return r;
}

template double qqb_1lha_r174(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r174(const std::array<dd_real,31>&);
#endif
