#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r41(const std::array<T,31>& k) {
  T z[30];
  T r = 0;

    z[1]=k[2];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[9];
    z[5]=k[4];
    z[6]=k[15];
    z[7]=k[5];
    z[8]=k[11];
    z[9]=k[8];
    z[10]=z[2]*z[4];
    z[11]=z[10] - 1;
    z[12]=z[11]*z[1];
    z[13]=2*z[2];
    z[14]= - z[13] + z[12];
    z[14]=z[1]*z[14];
    z[15]=z[1]*z[6];
    z[16]=3*z[15];
    z[17]=z[5]*z[6];
    z[18]= - z[16] + 2*z[17];
    z[18]=z[5]*z[18];
    z[19]=z[15] + 1;
    z[19]=z[19]*z[1];
    z[19]=z[19] + z[2];
    z[18]=z[18] + z[19];
    z[18]=z[5]*z[18];
    z[14]=z[14] + z[18];
    z[18]=z[8]*z[9];
    z[20]=z[6]*z[8];
    z[18]=z[18] + z[20];
    z[21]=z[18]*z[5];
    z[22]=z[18]*z[7];
    z[23]= - z[22] + z[21] + z[9] + 3*z[6];
    z[24]=2*z[7];
    z[23]=z[24]*z[23];
    z[25]=z[17] - z[15];
    z[23]=z[23] + static_cast<T>(1)+ 6*z[25];
    z[23]=z[5]*z[23];
    z[23]=z[1] + z[23];
    z[23]=z[7]*z[23];
    z[14]=2*z[14] + z[23];
    z[14]=z[7]*z[14];
    z[23]=3*z[2];
    z[25]= - static_cast<T>(3)+ z[15];
    z[25]=z[1]*z[25];
    z[25]= - z[23] + z[25];
    z[25]=z[1]*z[25];
    z[26]=z[17] + 1;
    z[16]=z[16] - z[26];
    z[16]=z[5]*z[16];
    z[27]=static_cast<T>(1)- z[15];
    z[27]=z[1]*z[27];
    z[16]=z[16] + z[2] + 3*z[27];
    z[16]=z[5]*z[16];
    z[27]=npow(z[2],2);
    z[16]=z[16] - z[27] + z[25];
    z[16]=z[5]*z[16];
    z[25]=3*z[27];
    z[23]=z[23] + z[1];
    z[23]=z[1]*z[23];
    z[23]=z[25] + z[23];
    z[23]=z[1]*z[23];
    z[16]=z[23] + z[16];
    z[16]=z[5]*z[16];
    z[23]=z[15] - z[26];
    z[23]=z[5]*z[23];
    z[23]=z[1] + z[23];
    z[23]=z[5]*z[23];
    z[28]= - 4*z[6] + z[22];
    z[28]=z[7]*z[28]*npow(z[5],2);
    z[23]=6*z[23] + z[28];
    z[23]=z[7]*z[23];
    z[28]=2*z[15];
    z[26]=z[28] - z[26];
    z[26]=z[5]*z[26];
    z[15]=static_cast<T>(2)- z[15];
    z[15]=z[1]*z[15];
    z[15]=z[26] + z[2] + z[15];
    z[15]=z[5]*z[15];
    z[13]=z[13] + z[1];
    z[13]=z[13]*z[1];
    z[15]= - z[13] + z[15];
    z[15]=z[5]*z[15];
    z[26]=npow(z[1],2);
    z[29]=z[2]*z[26];
    z[15]=z[29] + z[15];
    z[15]=4*z[15] + z[23];
    z[15]=z[7]*z[15];
    z[12]=z[2]*z[12];
    z[12]= - z[25] + z[12];
    z[12]=z[12]*z[26];
    z[12]=z[15] + z[12] + z[16];
    z[12]=z[3]*z[12];
    z[15]= - z[28] + z[17];
    z[15]=z[5]*z[15];
    z[15]=z[15] + z[19];
    z[15]=z[5]*z[15];
    z[13]=z[15] - z[27] - 2*z[13];
    z[13]=z[5]*z[13];
    z[15]=static_cast<T>(5)- z[10];
    z[15]=z[2]*z[15];
    z[16]= - static_cast<T>(1)+ 2*z[10];
    z[17]= - z[1]*z[16];
    z[15]=z[15] + z[17];
    z[15]=z[1]*z[15];
    z[15]=2*z[27] + z[15];
    z[15]=z[1]*z[15];
    z[12]=z[12] + z[14] + z[15] + z[13];
    z[12]=z[3]*z[12];
    z[13]=z[6] + z[9];
    z[14]= - 2*z[21] - z[13];
    z[14]=2*z[14] + z[22];
    z[14]=z[7]*z[14];
    z[15]=z[21] + z[13];
    z[15]=z[5]*z[15];
    z[13]=z[13] + z[4];
    z[17]=z[1]*z[13];
    z[14]=z[14] + z[17] + z[15];
    z[14]=z[7]*z[14];
    z[15]=z[1]*z[4];
    z[17]= - z[15] - z[11];
    z[17]=z[1]*z[17];
    z[17]=z[2] + z[17];
    z[14]=2*z[17] + z[14];
    z[14]=z[7]*z[14];
    z[17]=2*z[11] + z[15];
    z[17]=z[1]*z[17];
    z[19]= - static_cast<T>(3)+ z[10];
    z[19]=z[2]*z[19];
    z[17]=z[19] + z[17];
    z[17]=z[1]*z[17];
    z[19]=z[2] + z[1];
    z[19]=z[5]*z[19];
    z[12]=z[12] + z[14] + z[19] - z[27] + z[17];
    z[12]=z[3]*z[12];
    z[14]=z[10] + z[15];
    z[17]=z[24] - 2*z[5];
    z[17]=z[18]*z[17];
    z[13]= - z[8] + z[17] - z[13];
    z[13]=z[7]*z[13];
    z[13]=2*z[14] + z[13];
    z[13]=z[7]*z[13];
    z[14]= - z[15] - z[16];
    z[14]=z[1]*z[14];
    z[11]=z[11]*z[2];
    z[11]=z[12] + z[13] - z[11] + z[14];
    z[11]=z[3]*z[11];
    z[12]=z[15]*z[8];
    z[10]=z[10]*z[8];
    z[13]= - z[10] - z[12];
    z[14]=z[9] + z[4];
    z[14]=z[8]*z[14];
    z[14]=z[14] + z[20];
    z[14]=z[7]*z[14];
    z[13]=2*z[13] + z[14];
    z[13]=z[7]*z[13];
    z[12]=z[12] - z[8] + 2*z[10];
    z[12]=z[1]*z[12];
    z[10]= - z[8] + z[10];
    z[10]=z[2]*z[10];

    r += z[10] + z[11] + z[12] + z[13];
 
    return r;
}

template double qqb_1lha_r41(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r41(const std::array<dd_real,31>&);
#endif
