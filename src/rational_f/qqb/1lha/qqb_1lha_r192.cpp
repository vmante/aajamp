#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r192(const std::array<T,31>& k) {
  T z[13];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[14];
    z[7]=z[5]*z[6];
    z[8]=z[6]*z[4];
    z[9]=2*z[8];
    z[10]=static_cast<T>(3)- z[9];
    z[10]=z[10]*z[7];
    z[11]=z[8] - 1;
    z[12]=z[3]*z[6]*z[11];
    z[10]=z[12] + z[10] - static_cast<T>(1)+ 3*z[8];
    z[10]=z[3]*z[10];
    z[8]= - static_cast<T>(3)+ z[8];
    z[8]=z[8]*z[7];
    z[9]=static_cast<T>(1)- z[9];
    z[8]=2*z[9] + z[8];
    z[8]=z[5]*z[8];
    z[8]=z[10] + z[8] + z[1] + z[4];
    z[8]=z[3]*z[8];
    z[7]=z[7] + z[11];
    z[7]=z[5]*z[7];
    z[7]= - z[1] + z[7];
    z[7]=z[5]*z[7];
    z[9]=npow(z[1],2);
    z[10]=z[4]*z[1];
    z[7]=z[8] + z[7] - z[9] + z[10];
    z[7]=z[2]*z[7];
    z[8]=z[5]*npow(z[6],2);
    z[9]= - z[8] + 2*z[6];
    z[9]=z[5]*z[9];
    z[9]= - static_cast<T>(1)+ z[9];
    z[10]= - z[6] + z[8];
    z[10]=z[3]*z[10];
    z[9]=2*z[9] + z[10];
    z[9]=z[3]*z[9];
    z[8]=z[8] - 3*z[6];
    z[8]=z[5]*z[8];
    z[8]=static_cast<T>(3)+ z[8];
    z[8]=z[5]*z[8];
    z[7]=z[7] + z[9] + z[8] + 3*z[1] - 2*z[4];
    z[7]=z[2]*z[7];

    r +=  - static_cast<T>(2)+ z[7];
 
    return r;
}

template double qqb_1lha_r192(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r192(const std::array<dd_real,31>&);
#endif
