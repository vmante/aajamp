#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r132(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[15];
    z[5]=k[3];
    z[6]=k[5];
    z[7]=k[13];
    z[8]=z[3]*z[1];
    z[9]=z[2]*z[3];
    z[10]= - z[9] + z[8] + 1;
    z[11]=z[7]*z[5];
    z[12]=z[11] + 1;
    z[13]=2*z[7];
    z[14]=z[12]*z[13];
    z[14]= - z[3] + z[14];
    z[14]=z[6]*z[14];
    z[14]=z[14] + 3*z[10];
    z[14]=z[2]*z[14];
    z[8]=z[8] - z[9];
    z[9]= - z[3] + z[7];
    z[15]=z[6]*npow(z[7],2);
    z[9]=2*z[9] + z[15];
    z[9]=z[6]*z[9];
    z[8]=z[9] + static_cast<T>(3)+ 2*z[8];
    z[8]=z[2]*z[8];
    z[9]=z[6]*z[7]*z[12];
    z[9]=z[9] + static_cast<T>(2)+ z[11];
    z[9]=z[6]*z[9];
    z[8]=2*z[9] + z[8];
    z[8]=z[2]*z[8];
    z[9]=z[7]*npow(z[5],2);
    z[12]=2*z[5] + z[9];
    z[12]=z[7]*z[12]*npow(z[6],2);
    z[8]=z[12] + z[8];
    z[8]=z[4]*z[8];
    z[9]=z[5] + z[9];
    z[9]=z[9]*z[13];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[6]*z[9];
    z[8]=z[8] + z[9] + z[14];
    z[8]=z[4]*z[8];
    z[9]=npow(z[11],2);

    r += z[8] + z[9] + z[10];
 
    return r;
}

template double qqb_1lha_r132(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r132(const std::array<dd_real,31>&);
#endif
