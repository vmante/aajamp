#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r136(const std::array<T,31>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[10];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[16];
    z[7]=k[4];
    z[8]=z[2]*z[6];
    z[8]=z[8] - 1;
    z[9]=npow(z[3],2);
    z[8]=z[8]*z[9];
    z[10]=z[9]*z[4];
    z[11]=z[10] - 2*z[3];
    z[12]= - z[6]*z[11];
    z[12]=z[12] - z[8];
    z[12]=z[2]*z[12];
    z[13]=z[4]*z[3];
    z[13]=z[13] - 1;
    z[13]=z[13]*z[6];
    z[12]=z[12] + z[13] + z[11];
    z[12]=z[2]*z[12];
    z[10]=z[10] - z[3];
    z[14]=npow(z[4],2);
    z[15]=z[10]*z[14];
    z[16]=2*z[4];
    z[10]=z[10]*z[16];
    z[11]= - z[4]*z[11];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[7]*z[6]*z[11];
    z[10]=z[10] + z[11];
    z[10]=z[7]*z[10];
    z[10]=z[15] + z[10];
    z[10]=z[5]*z[10];
    z[11]= - z[9]*z[14];
    z[8]=z[16]*z[8];
    z[8]= - z[13] + z[8];
    z[8]=z[7]*z[8];
    z[13]= - z[5]*z[14];
    z[13]=z[13] - z[2] + z[4];
    z[9]=z[9]*z[13];
    z[9]=z[3] + z[9];
    z[9]=z[1]*z[9];

    r += static_cast<T>(1)+ z[8] + z[9] + z[10] + z[11] + z[12];
 
    return r;
}

template double qqb_1lha_r136(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r136(const std::array<dd_real,31>&);
#endif
