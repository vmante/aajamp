#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r156(const std::array<T,31>& k) {
  T z[12];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[4];
    z[5]=k[14];
    z[6]=k[12];
    z[7]=k[5];
    z[8]=z[6]*z[7];
    z[9]=z[7] - 2*z[3];
    z[9]=z[2]*z[9];
    z[10]= - z[6] + z[2];
    z[10]=z[4]*z[10];
    z[8]=z[10] - z[8] + z[9];
    z[8]=z[4]*z[8];
    z[9]=z[3]*z[6];
    z[10]=z[2]*z[3];
    z[10]= - z[9] + z[10];
    z[10]=z[4]*z[10];
    z[11]=z[7]*z[9];
    z[10]= - 2*z[11] + z[10];
    z[10]=z[4]*z[10];
    z[9]= - static_cast<T>(1)- z[9];
    z[9]=z[9]*npow(z[7],2);
    z[9]=z[10] + z[9];
    z[9]=z[5]*z[9];
    z[8]=z[8] + z[9];
    z[8]=z[5]*z[8];
    z[9]= - z[4] - z[1];
    z[9]=z[2]*z[9];

    r += static_cast<T>(4)+ z[8] + z[9];
 
    return r;
}

template double qqb_1lha_r156(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r156(const std::array<dd_real,31>&);
#endif
