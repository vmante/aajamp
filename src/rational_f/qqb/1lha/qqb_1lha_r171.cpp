#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r171(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[10];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[15];
    z[7]=z[5]*z[6];
    z[8]=3*z[7];
    z[9]=z[1]*z[3];
    z[10]=npow(z[9],2);
    z[11]= - z[2]*z[6];
    z[11]=z[11] + z[10] - static_cast<T>(1)- z[8];
    z[11]=z[2]*z[11];
    z[12]=z[1]*npow(z[3],2);
    z[13]=z[3]*z[5];
    z[13]= - static_cast<T>(2)+ z[13];
    z[13]=z[3]*z[13];
    z[13]=z[13] - z[12];
    z[13]=z[1]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[1]*z[13];
    z[8]= - static_cast<T>(2)- z[8];
    z[8]=z[5]*z[8];
    z[8]=z[11] + z[8] + z[13];
    z[8]=z[2]*z[8];
    z[7]= - static_cast<T>(1)- z[7];
    z[7]=z[7]*npow(z[5],2);
    z[9]= - static_cast<T>(1)- z[9];
    z[9]=z[1]*z[5]*z[9];
    z[7]=z[8] + z[7] + z[9];
    z[7]=z[4]*z[7];
    z[8]=z[3] + z[12];
    z[8]=z[8]*npow(z[1],2);
    z[9]= - z[2]*z[10];
    z[7]=z[7] + z[8] + z[9];

    r += z[7]*z[4];
 
    return r;
}

template double qqb_1lha_r171(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r171(const std::array<dd_real,31>&);
#endif
