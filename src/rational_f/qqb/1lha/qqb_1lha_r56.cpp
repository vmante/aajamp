#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r56(const std::array<T,31>& k) {
  T z[27];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[26];
    z[8]=k[4];
    z[9]=k[6];
    z[10]=k[5];
    z[11]=z[8]*z[1];
    z[12]=4*z[11];
    z[13]=npow(z[1],2);
    z[14]=3*z[13];
    z[15]=z[14] + z[12];
    z[16]=4*z[1];
    z[17]=z[16]*z[6];
    z[17]=z[17] + 5;
    z[18]=2*z[5];
    z[19]=z[18]*z[6];
    z[20]= - z[19] + z[17];
    z[20]=z[5]*z[20];
    z[21]=z[1]*z[6];
    z[21]= - static_cast<T>(7)- z[21];
    z[21]=z[1]*z[21];
    z[21]=z[21] - z[8];
    z[20]=2*z[21] + z[20];
    z[20]=z[5]*z[20];
    z[15]=3*z[15] + z[20];
    z[15]=z[5]*z[15];
    z[20]=z[5] - z[8];
    z[21]= - z[20] + 2*z[1];
    z[21]=z[5]*z[21];
    z[12]=z[21] - z[13] - z[12];
    z[12]=z[5]*z[12];
    z[21]=2*z[11];
    z[22]=z[14] + z[21];
    z[22]=z[8]*z[22];
    z[12]=z[22] + z[12];
    z[12]=z[5]*z[12];
    z[22]=z[8]*z[3];
    z[23]=z[13]*z[22];
    z[14]= - z[14] + z[23];
    z[23]=npow(z[8],2);
    z[14]=z[14]*z[23];
    z[12]=z[14] + z[12];
    z[12]=z[7]*z[12];
    z[14]=z[1]*z[3];
    z[24]= - static_cast<T>(2)+ 5*z[14];
    z[24]=z[24]*z[11];
    z[13]= - 12*z[13] + z[24];
    z[13]=z[8]*z[13];
    z[12]=z[12] + z[13] + z[15];
    z[12]=z[7]*z[12];
    z[13]= - static_cast<T>(1)+ 2*z[14];
    z[13]=z[13]*z[21];
    z[15]=z[8]*z[6];
    z[21]=z[15] + z[19];
    z[24]=2*z[6];
    z[24]=z[24]*z[1];
    z[24]=z[24] + 3;
    z[25]= - z[24] + z[21];
    z[25]=z[25]*z[18];
    z[26]=z[2] + z[16];
    z[25]=z[25] + 3*z[26] - 2*z[8];
    z[25]=z[5]*z[25];
    z[26]=z[3]*z[2];
    z[26]= - static_cast<T>(2)+ z[26];
    z[26]=z[1]*z[26];
    z[26]= - 2*z[2] + z[26];
    z[26]=z[1]*z[26];
    z[12]=z[12] + z[25] + 3*z[26] + z[13];
    z[12]=z[4]*z[12];
    z[13]=z[5]*z[6];
    z[13]= - 8*z[13] - 4*z[15] + z[17];
    z[13]=z[5]*z[13];
    z[11]=z[3]*z[11];
    z[11]= - 5*z[1] + 3*z[11];
    z[11]=z[11]*z[23];
    z[15]= - z[1] + z[20];
    z[15]=z[15]*z[18];
    z[16]=z[16] + z[8];
    z[16]=z[8]*z[16];
    z[15]=z[16] + z[15];
    z[15]=z[5]*z[15];
    z[11]=z[11] + z[15];
    z[11]=z[7]*z[11];
    z[15]=z[19] - z[24];
    z[15]=z[15]*z[18];
    z[15]=z[15] + 10*z[1] + 3*z[8];
    z[15]=z[5]*z[15];
    z[16]= - static_cast<T>(1)+ 10*z[14];
    z[16]=z[8]*z[16];
    z[16]= - 13*z[1] + z[16];
    z[16]=z[8]*z[16];
    z[11]=z[11] + z[16] + z[15];
    z[11]=z[7]*z[11];
    z[15]= - static_cast<T>(2)- 3*z[14];
    z[15]=z[1]*z[15];
    z[14]= - static_cast<T>(1)+ 7*z[14];
    z[14]=z[8]*z[14];
    z[11]=z[12] + z[11] + z[13] + z[15] + z[14];
    z[11]=z[4]*z[11];
    z[12]=z[9]*z[10];
    z[13]=static_cast<T>(3)+ z[12];
    z[13]=z[13]*z[22];
    z[12]=z[13] - static_cast<T>(1)+ z[12];
    z[12]=z[12]*z[23];
    z[13]= - z[8]*z[9];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[8]*z[13];
    z[13]=z[13] - z[5];
    z[13]=z[5]*z[13];
    z[12]=z[12] + z[13];
    z[12]=z[7]*z[12];
    z[13]=z[9] + 5*z[3];
    z[13]=z[8]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[8]*z[13];
    z[14]=static_cast<T>(1)- z[19];
    z[14]=z[5]*z[14];
    z[12]=z[12] + z[13] + z[14];
    z[12]=z[7]*z[12];
    z[13]= - static_cast<T>(2)+ z[21];

    r += z[11] + z[12] + 2*z[13];
 
    return r;
}

template double qqb_1lha_r56(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r56(const std::array<dd_real,31>&);
#endif
