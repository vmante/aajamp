#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r94(const std::array<T,31>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[9];
    z[4]=k[5];
    z[5]=k[7];
    z[6]=k[3];
    z[7]=z[4] - z[6];
    z[8]=npow(z[5],2);
    z[9]=z[7]*z[8];
    z[10]=3*z[5];
    z[11]=z[10] - z[9];
    z[11]=z[4]*z[11];
    z[12]=2*z[6];
    z[13]=z[5]*z[12];
    z[11]=z[11] - static_cast<T>(3)+ z[13];
    z[11]=z[4]*z[11];
    z[8]=z[8]*z[4];
    z[13]=2*z[5];
    z[14]= - z[13] + z[8];
    z[14]=z[4]*z[14];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[1]*z[14];
    z[15]=3*z[6];
    z[11]=z[14] + z[11] + z[2] - z[15];
    z[11]=z[1]*z[11];
    z[14]=z[12]*z[4];
    z[15]= - 2*z[2] + z[15];
    z[15]=z[6]*z[15];
    z[11]=z[11] + z[15] + z[14];
    z[11]=z[1]*z[11];
    z[15]=z[5]*z[4]*z[6];
    z[15]= - z[12] + z[15];
    z[15]=z[4]*z[15];
    z[16]=z[6]*z[2];
    z[15]=z[16] + z[15];
    z[8]= - z[10] + z[8];
    z[8]=z[4]*z[8];
    z[8]=static_cast<T>(3)+ z[8];
    z[8]=z[4]*z[8];
    z[8]= - z[2] + z[8];
    z[8]=z[1]*z[8];
    z[8]=3*z[15] + z[8];
    z[8]=z[1]*z[8];
    z[10]=z[4] - z[2];
    z[15]=npow(z[6],2);
    z[16]=z[15]*z[10];
    z[8]=3*z[16] + z[8];
    z[8]=z[1]*z[8];
    z[16]=z[2]*npow(z[6],3);
    z[8]=z[16] + z[8];
    z[8]=z[3]*z[8];
    z[16]=z[2] + z[7];
    z[15]=z[15]*z[16];
    z[8]=z[8] + z[11] + z[15];
    z[8]=z[3]*z[8];
    z[9]=z[13] - z[9];
    z[9]=z[4]*z[9];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[1]*z[9];
    z[9]=2*z[9] + 4*z[6] + z[10];
    z[9]=z[1]*z[9];
    z[10]=z[2] - z[12];
    z[10]=z[6]*z[10];
    z[8]=2*z[8] + z[9] + z[10] + z[14];
    z[8]=z[3]*z[8];
    z[7]=z[8] + z[1] + z[7];

    r += z[7]*z[3];
 
    return r;
}

template double qqb_1lha_r94(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r94(const std::array<dd_real,31>&);
#endif
