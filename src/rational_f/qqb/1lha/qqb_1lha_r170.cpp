#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r170(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[5];
    z[5]=k[14];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=3*z[2];
    z[9]= - z[6] + 2*z[7];
    z[10]=z[5]*z[7]*z[6];
    z[9]=z[10] + 2*z[9] - z[8];
    z[9]=z[5]*z[9];
    z[10]=3*z[7];
    z[11]=z[10] - 2*z[2];
    z[11]=z[5]*z[11];
    z[11]= - static_cast<T>(12)+ z[11];
    z[11]=z[5]*z[11];
    z[12]=4*z[7] - 5*z[2];
    z[12]=z[5]*z[12];
    z[12]= - static_cast<T>(4)+ z[12];
    z[12]=z[3]*z[12];
    z[11]=z[11] + z[12];
    z[11]=z[3]*z[11];
    z[12]=npow(z[5],2);
    z[13]=3*z[12];
    z[14]=z[2] - z[7];
    z[14]=z[14]*z[5];
    z[15]=static_cast<T>(5)+ z[14];
    z[15]=z[15]*z[3]*z[5];
    z[15]=z[13] + z[15];
    z[15]=z[3]*z[15];
    z[12]= - z[4]*z[12]*npow(z[3],2);
    z[12]=z[15] + z[12];
    z[12]=z[4]*z[12];
    z[11]=z[12] - z[13] + z[11];
    z[11]=z[4]*z[11];
    z[12]=static_cast<T>(3)+ 4*z[14];
    z[13]=z[1] - z[7];
    z[13]=3*z[13] + 4*z[2];
    z[13]=z[3]*z[13];
    z[12]=2*z[12] + z[13];
    z[12]=z[3]*z[12];
    z[13]=z[2] + z[6] - z[10];
    z[13]=z[5]*z[13];
    z[13]=static_cast<T>(9)+ z[13];
    z[13]=z[5]*z[13];
    z[11]=z[11] + z[13] + z[12];
    z[11]=z[4]*z[11];
    z[12]= - z[3]*z[1];
    z[12]=z[12] - 1;
    z[8]=z[8]*z[12];
    z[8]= - 2*z[1] + z[10] + z[8];
    z[8]=z[3]*z[8];

    r +=  - static_cast<T>(2)+ z[8] + z[9] + z[11];
 
    return r;
}

template double qqb_1lha_r170(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r170(const std::array<dd_real,31>&);
#endif
