#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r184(const std::array<T,31>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[20];
    z[5]=k[3];
    z[6]=k[14];
    z[7]=k[5];
    z[8]=z[3]*z[5];
    z[9]=z[7]*z[4];
    z[9]=z[9] + z[8];
    z[10]=npow(z[6],2);
    z[9]=z[10]*z[9];
    z[11]=3*z[5];
    z[11]=z[11]*z[10];
    z[12]=2*z[6];
    z[13]= - z[12] - z[11];
    z[13]=z[4]*z[13];
    z[9]=z[13] + z[9];
    z[9]=z[7]*z[9];
    z[13]= - z[4] + z[3];
    z[13]=z[2]*z[13];
    z[13]=z[13] - 2*z[8];
    z[10]=z[10]*z[5];
    z[14]=z[10] + z[6];
    z[13]=z[14]*z[13];
    z[11]=4*z[6] + z[11];
    z[11]=z[5]*z[11];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[4]*z[11];
    z[9]=z[9] + z[11] + z[13];
    z[9]=z[7]*z[9];
    z[11]=z[4]*z[5];
    z[8]= - z[11] + z[8];
    z[12]=z[10] + z[12];
    z[13]=z[12]*z[5];
    z[13]=z[13] + 1;
    z[8]=z[13]*z[8];
    z[13]=2*z[5];
    z[12]=z[12]*z[13];
    z[12]=z[12] + 1;
    z[10]=3*z[6] + z[10];
    z[10]=z[5]*z[10];
    z[10]=static_cast<T>(2)+ z[10];
    z[10]=z[10]*z[11];
    z[11]=z[1]*z[4];
    z[10]=z[11] + z[10] - z[12];
    z[10]=z[3]*z[10];
    z[11]=z[4]*z[12];
    z[10]=z[11] + z[10];
    z[10]=z[2]*z[10];

    r += z[8] + z[9] + z[10];
 
    return r;
}

template double qqb_1lha_r184(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r184(const std::array<dd_real,31>&);
#endif
