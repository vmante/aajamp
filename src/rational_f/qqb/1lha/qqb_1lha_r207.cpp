#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r207(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[10];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[2];
    z[7]=k[11];
    z[8]=k[5];
    z[9]= - z[2] + 2*z[8];
    z[9]=z[9]*z[2];
    z[10]=npow(z[8],2);
    z[9]=z[9] - z[10];
    z[10]=3*z[5];
    z[11]=z[2] - z[8];
    z[12]=4*z[11] + z[10];
    z[12]=z[5]*z[12];
    z[12]=z[12] - z[9];
    z[12]=z[7]*z[12];
    z[13]= - 2*z[11] - z[5];
    z[13]=z[5]*z[13];
    z[9]=z[13] + z[9];
    z[9]=z[7]*z[9];
    z[9]=z[9] - z[5] - z[1] - z[11];
    z[9]=z[5]*z[9];
    z[13]= - z[2]*z[3]*npow(z[1],2);
    z[9]=z[13] + z[9];
    z[9]=z[4]*z[9];
    z[13]=z[1]*z[3];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[1]*z[13];
    z[9]=z[9] + z[12] + 2*z[5] + z[13] + z[11];
    z[9]=z[4]*z[9];
    z[10]= - z[10] + z[6] - z[11];
    z[10]=z[7]*z[10];

    r +=  - static_cast<T>(1)+ z[9] + z[10];
 
    return r;
}

template double qqb_1lha_r207(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r207(const std::array<dd_real,31>&);
#endif
