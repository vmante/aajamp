#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r90(const std::array<T,31>& k) {
  T z[30];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[18];
    z[6]=k[5];
    z[7]=k[3];
    z[8]=k[15];
    z[9]=k[29];
    z[10]=z[5]*z[1];
    z[11]=z[1]*z[4];
    z[12]= - static_cast<T>(1)- z[11];
    z[12]=z[12]*z[10];
    z[13]=z[4]*z[9];
    z[14]=z[5]*z[8];
    z[15]=z[13] + z[14];
    z[15]=z[15]*z[6];
    z[16]=2*z[8];
    z[17]=z[5] - z[16] + z[4] - z[15] - z[9];
    z[17]=z[6]*z[17];
    z[18]=z[2]*z[9];
    z[19]=z[9]*z[7];
    z[20]= - z[18] - static_cast<T>(3)- 2*z[19];
    z[20]=z[8]*z[20];
    z[20]= - z[9] + z[20];
    z[20]=z[2]*z[20];
    z[21]=npow(z[7],2);
    z[22]=z[21]*z[9];
    z[23]= - 5*z[7] - z[22];
    z[23]=z[8]*z[23];
    z[12]=z[20] + z[17] + z[12] + z[23] - z[11] - static_cast<T>(3)+ z[19];
    z[12]=z[2]*z[12];
    z[13]=z[13] + 2*z[14];
    z[13]=z[13]*z[6];
    z[17]=z[4] + z[8] + 2*z[5];
    z[20]= - z[13] + 2*z[9] + z[17];
    z[20]=z[6]*z[20];
    z[23]=z[8]*z[7];
    z[20]=z[20] - 2*z[10] - 4*z[23] - z[11] - static_cast<T>(5)- 3*z[19];
    z[20]=z[6]*z[20];
    z[24]=z[11] - 1;
    z[25]=npow(z[1],2);
    z[26]=z[25]*z[5];
    z[27]=z[24]*z[26];
    z[16]= - z[21]*z[16];
    z[28]=z[25]*z[4];
    z[12]=z[12] + z[20] - z[27] + z[16] + 2*z[22] - 3*z[28];
    z[12]=z[2]*z[12];
    z[16]=z[5] + z[8];
    z[14]=z[14]*z[6];
    z[20]= - z[14] + z[9] + z[16];
    z[20]=z[6]*z[20];
    z[29]= - static_cast<T>(1)- z[19];
    z[20]=z[20] - z[10] + 2*z[29] - z[23];
    z[20]=z[6]*z[20];
    z[21]= - z[8]*z[21];
    z[20]=z[20] - z[26] + z[21] + z[22] - 2*z[28];
    z[20]=z[6]*z[20];
    z[21]= - z[24]*z[25];
    z[12]=z[12] + z[21] + z[20];
    z[12]=z[3]*z[12];
    z[20]=z[11]*z[5];
    z[15]=z[15] + z[20];
    z[18]= - z[18] - static_cast<T>(2)- z[19];
    z[18]=z[8]*z[18];
    z[18]= - z[9] + z[18] - z[15];
    z[18]=z[2]*z[18];
    z[19]=z[23] + 1;
    z[20]=2*z[11];
    z[21]=z[19] + z[20];
    z[13]= - 2*z[13] + z[9] + z[17];
    z[13]=z[6]*z[13];
    z[17]=z[4]*z[26];
    z[13]=z[18] + z[13] - 2*z[21] - 3*z[17];
    z[13]=z[2]*z[13];
    z[16]=2*z[16] - 3*z[14];
    z[16]=z[6]*z[16];
    z[16]=z[16] - z[10] - z[21];
    z[16]=z[6]*z[16];
    z[17]=static_cast<T>(1)- 4*z[11];
    z[17]=z[1]*z[17];
    z[12]=z[12] + z[13] + z[16] + z[17] - z[27];
    z[12]=z[3]*z[12];
    z[13]=z[4]*z[7];
    z[13]=z[13] - z[20];
    z[10]=z[13]*z[10];
    z[13]=z[5]*z[19];
    z[13]=z[13] - 2*z[14];
    z[13]=z[6]*z[13];
    z[14]= - z[2]*z[15];

    r += z[10] - 3*z[11] + z[12] + z[13] + z[14];
 
    return r;
}

template double qqb_1lha_r90(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r90(const std::array<dd_real,31>&);
#endif
