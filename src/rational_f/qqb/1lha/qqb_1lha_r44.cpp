#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r44(const std::array<T,31>& k) {
  T z[31];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[2];
    z[6]=k[10];
    z[7]=k[25];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=k[4];
    z[11]=k[29];
    z[12]=2*z[6];
    z[13]=z[12]*z[5];
    z[14]=z[13] - 3;
    z[15]=2*z[7];
    z[16]=z[14]*z[15];
    z[17]=z[10]*z[6];
    z[18]=z[17]*z[7];
    z[19]=z[5]*z[9];
    z[20]=static_cast<T>(2)- z[19];
    z[20]=z[4]*z[20];
    z[21]=z[12]*z[7];
    z[22]=z[8]*z[21];
    z[16]=z[22] + z[20] - 8*z[18] + z[16] + z[9] - z[12];
    z[16]=z[8]*z[16];
    z[20]=npow(z[11],2);
    z[22]=z[20]*z[4];
    z[23]= - static_cast<T>(3)- z[19];
    z[23]=z[23]*z[22];
    z[24]=z[11]*z[8];
    z[25]=z[8]*z[4];
    z[26]=static_cast<T>(4)- z[25];
    z[26]=z[26]*z[24];
    z[27]=2*z[8];
    z[28]=z[27]*z[4];
    z[26]=z[28] + z[26];
    z[26]=z[11]*z[26];
    z[29]=z[15] + z[4];
    z[26]=z[26] + z[29];
    z[26]=z[3]*z[26];
    z[30]=static_cast<T>(2)- 3*z[25];
    z[30]=z[11]*z[30];
    z[30]=z[30] - 2*z[4];
    z[30]=z[11]*z[30];
    z[26]=z[30] + z[26];
    z[26]=z[3]*z[26];
    z[30]= - static_cast<T>(1)+ z[28];
    z[30]=z[3]*z[30];
    z[30]=3*z[4] + z[30];
    z[20]=z[3]*z[20]*z[30];
    z[22]= - z[2]*npow(z[3],2)*z[22];
    z[20]=z[20] + z[22];
    z[20]=z[2]*z[20];
    z[20]=z[20] + z[23] + z[26];
    z[20]=z[2]*z[20];
    z[22]=static_cast<T>(2)- z[25];
    z[22]=z[22]*z[27];
    z[23]=3*z[8];
    z[26]=2*z[10];
    z[30]=z[26] - z[23];
    z[30]=z[30]*z[24];
    z[22]=z[22] + z[30];
    z[22]=z[11]*z[22];
    z[30]=z[4]*z[1];
    z[22]=z[22] - z[28] + static_cast<T>(5)- 2*z[30];
    z[22]=z[3]*z[22];
    z[25]= - 5*z[24] - static_cast<T>(2)- z[25];
    z[25]=z[11]*z[25];
    z[22]=z[22] + z[25] - z[29];
    z[22]=z[3]*z[22];
    z[25]= - z[10]*z[9];
    z[25]=z[25] - static_cast<T>(1)+ z[19];
    z[25]=z[11]*z[25];
    z[28]=static_cast<T>(4)+ 3*z[19];
    z[28]=z[4]*z[28];
    z[25]=z[25] - z[9] + z[28];
    z[25]=z[11]*z[25];
    z[20]=z[20] + z[25] + z[22];
    z[20]=z[2]*z[20];
    z[19]=2*z[19];
    z[22]=z[11]*z[10];
    z[25]=z[9]*z[26];
    z[25]= - z[22] + z[25] + static_cast<T>(1)- z[19];
    z[25]=z[11]*z[25];
    z[28]=z[10] - z[23];
    z[28]=z[28]*z[27];
    z[29]=npow(z[8],2);
    z[22]=z[29]*z[22];
    z[22]=z[28] - 3*z[22];
    z[22]=z[11]*z[22];
    z[28]=z[10]*z[15];
    z[28]= - static_cast<T>(3)+ z[28];
    z[27]=z[28]*z[27];
    z[22]=z[27] + z[22];
    z[22]=z[3]*z[22];
    z[27]=z[8]*z[10];
    z[28]=z[11]*z[27];
    z[23]= - z[23] - 4*z[28];
    z[23]=z[11]*z[23];
    z[28]= - static_cast<T>(1)+ z[30];
    z[22]=z[22] + 4*z[28] + z[23];
    z[22]=z[3]*z[22];
    z[19]= - static_cast<T>(1)- z[19];
    z[19]=z[4]*z[19];
    z[19]=z[20] + z[22] + z[25] + z[19] + z[9] + 4*z[7];
    z[19]=z[2]*z[19];
    z[20]= - z[10]*z[12];
    z[20]= - static_cast<T>(3)+ z[20];
    z[20]=z[20]*z[27];
    z[22]=npow(z[10],2);
    z[23]= - z[22]*z[24];
    z[20]=z[20] + z[23];
    z[20]=z[11]*z[20];
    z[23]= - z[27]*z[21];
    z[24]=z[10]*z[21];
    z[24]=3*z[7] + z[24];
    z[24]=z[10]*z[24];
    z[23]=z[24] + z[23];
    z[23]=z[8]*z[23];
    z[20]=z[23] + z[20];
    z[17]= - static_cast<T>(3)- z[17];
    z[17]=z[17]*z[29]*z[26];
    z[23]=npow(z[27],2);
    z[24]= - z[11]*z[23];
    z[17]=z[17] + z[24];
    z[17]=z[11]*z[17];
    z[21]=z[23]*z[21];
    z[17]=z[21] + z[17];
    z[17]=z[3]*z[17];
    z[17]=2*z[20] + z[17];
    z[17]=z[3]*z[17];
    z[20]=z[6]*npow(z[5],2);
    z[20]= - 3*z[5] + z[20];
    z[15]=z[20]*z[15];
    z[14]= - z[7]*z[14];
    z[14]=z[14] + z[18];
    z[14]=z[14]*z[26];
    z[12]= - z[12] - z[11];
    z[12]=z[11]*z[22]*z[12];
    z[18]=z[5] - 2*z[1];
    z[18]=z[4]*z[18];

    r += static_cast<T>(1)+ z[12] - z[13] + z[14] + z[15] + z[16] + z[17] + z[18] + 
      z[19];
 
    return r;
}

template double qqb_1lha_r44(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r44(const std::array<dd_real,31>&);
#endif
