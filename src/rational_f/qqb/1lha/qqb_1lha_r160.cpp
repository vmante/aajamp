#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r160(const std::array<T,31>& k) {
  T z[10];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[4];
    z[5]=k[14];
    z[6]=k[5];
    z[7]=z[3]*z[4];
    z[8]=z[6] + z[4];
    z[8]=z[4]*z[8];
    z[8]=z[8] - z[7];
    z[9]=2*z[3];
    z[8]=z[8]*z[9];
    z[9]=z[5]*npow(z[7],2);
    z[8]=z[8] + z[9];
    z[8]=z[5]*z[8];
    z[9]=npow(z[1],2);
    z[8]=z[8] + z[9] - 2*z[7];
    z[8]=z[2]*z[8];
    z[7]=z[6]*npow(z[5],2)*z[7];
    z[7]= - z[1] + z[7];
    z[7]=2*z[7] + z[8];
    z[7]=z[2]*z[7];
    z[8]=z[5]*npow(z[6],2);
    z[8]= - 2*z[6] + z[8];
    z[8]=z[5]*z[8];

    r += z[7] + z[8];
 
    return r;
}

template double qqb_1lha_r160(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r160(const std::array<dd_real,31>&);
#endif
