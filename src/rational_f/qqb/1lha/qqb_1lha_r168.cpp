#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r168(const std::array<T,31>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[9];
    z[4]=k[5];
    z[5]=k[11];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=3*z[6];
    z[9]= - z[8] + 2*z[4];
    z[9]=z[5]*z[9];
    z[9]=static_cast<T>(12)+ z[9];
    z[9]=z[5]*z[9];
    z[10]= - 4*z[6] + 5*z[4];
    z[10]=z[5]*z[10];
    z[10]=static_cast<T>(4)+ z[10];
    z[10]=z[3]*z[10];
    z[9]=z[9] + z[10];
    z[9]=z[3]*z[9];
    z[10]=npow(z[5],2);
    z[11]=3*z[10];
    z[12]=z[4] - z[6];
    z[13]=z[12]*z[5];
    z[14]= - static_cast<T>(5)- z[13];
    z[14]=z[14]*z[3]*z[5];
    z[14]= - z[11] + z[14];
    z[14]=z[3]*z[14];
    z[10]=z[2]*z[10]*npow(z[3],2);
    z[10]=z[14] + z[10];
    z[10]=z[2]*z[10];
    z[9]=z[10] + z[11] + z[9];
    z[9]=z[2]*z[9];
    z[10]= - static_cast<T>(3)- 4*z[13];
    z[11]=z[6] - z[1];
    z[11]=3*z[11] - 4*z[4];
    z[11]=z[3]*z[11];
    z[10]=2*z[10] + z[11];
    z[10]=z[3]*z[10];
    z[8]= - z[4] - z[7] + z[8];
    z[8]=z[5]*z[8];
    z[8]= - static_cast<T>(9)+ z[8];
    z[8]=z[5]*z[8];
    z[8]=z[9] + z[8] + z[10];
    z[8]=z[2]*z[8];
    z[9]=z[7] - 2*z[6];
    z[10]= - z[5]*z[6]*z[7];
    z[9]=z[10] + 2*z[9] + 3*z[4];
    z[9]=z[5]*z[9];
    z[10]=z[3]*z[4];
    z[10]=z[10] + 1;
    z[10]=z[1]*z[10];
    z[10]=z[12] + z[10];
    z[10]=z[3]*z[10];

    r += static_cast<T>(2)+ z[8] + z[9] + 3*z[10];
 
    return r;
}

template double qqb_1lha_r168(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r168(const std::array<dd_real,31>&);
#endif
