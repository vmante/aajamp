#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r45(const std::array<T,31>& k) {
  T z[23];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[9];
    z[4]=k[22];
    z[5]=k[5];
    z[6]=k[7];
    z[7]=k[3];
    z[8]=k[6];
    z[9]=k[4];
    z[10]=k[17];
    z[11]=z[4]*z[9];
    z[12]=z[9]*z[10];
    z[13]=z[11] + z[12];
    z[14]=z[10]*z[7];
    z[15]=npow(z[9],2);
    z[16]=z[15]*z[4];
    z[17]=z[9] - 3*z[16];
    z[17]=z[8]*z[17];
    z[17]=z[17] - static_cast<T>(2)- 3*z[14] + 2*z[13];
    z[17]=z[6]*z[17];
    z[18]=z[12] + 4*z[11];
    z[18]=z[8]*z[18];
    z[19]=z[4] + z[10];
    z[20]=z[4]*z[1];
    z[21]=static_cast<T>(2)+ 3*z[20];
    z[21]=z[3]*z[21];
    z[17]=z[17] + z[18] - 5*z[19] + z[21];
    z[17]=z[6]*z[17];
    z[14]=z[14] + 1;
    z[18]= - 3*z[14] - z[20];
    z[18]=z[3]*z[18];
    z[11]= - z[12] - 3*z[11];
    z[11]=z[8]*z[11];
    z[11]=z[11] + 4*z[19] + z[18];
    z[11]=z[6]*z[11];
    z[18]=z[3]*z[20];
    z[18]=z[18] - z[19];
    z[21]=2*z[3];
    z[18]=z[18]*z[21];
    z[22]=z[19]*z[8];
    z[18]=z[18] + z[22];
    z[11]=2*z[18] + z[11];
    z[11]=z[6]*z[11];
    z[14]= - z[20] - z[14];
    z[14]=z[3]*z[14];
    z[18]=2*z[19];
    z[14]=z[18] + z[14];
    z[14]=z[3]*z[14];
    z[14]=z[14] - z[22];
    z[14]=z[5]*z[14]*npow(z[6],2);
    z[11]=z[11] + z[14];
    z[11]=z[5]*z[11];
    z[14]=static_cast<T>(1)- z[20];
    z[14]=z[14]*npow(z[3],2);
    z[18]=z[8] - z[18] + 3*z[3];
    z[18]=z[8]*z[18];
    z[11]=z[11] + z[17] + 6*z[14] + z[18];
    z[11]=z[5]*z[11];
    z[14]=z[3]*z[7];
    z[17]=2*z[2];
    z[18]=z[17]*z[14];
    z[18]=z[18] - z[7] - z[2];
    z[18]=z[18]*z[21];
    z[19]= - 3*z[9] + z[17];
    z[19]=z[4]*z[19];
    z[14]=z[2]*z[14];
    z[14]= - z[7] + z[14];
    z[14]=z[8]*z[14];
    z[14]=2*z[14] + z[18] + static_cast<T>(3)+ z[19];
    z[14]=z[8]*z[14];
    z[18]= - z[4]*npow(z[9],3);
    z[18]=z[15] + z[18];
    z[18]=z[8]*z[18];
    z[12]= - z[7]*z[12];
    z[12]=z[12] + z[18];
    z[12]=z[6]*z[12];
    z[16]= - z[9] + 2*z[16];
    z[16]=z[8]*z[16];
    z[12]=z[12] + z[16] + static_cast<T>(1)- z[13];
    z[12]=z[6]*z[12];
    z[13]=z[2]*z[20];
    z[13]= - z[17] + z[13];
    z[13]=z[3]*z[13];
    z[16]=4*z[2];
    z[18]= - 3*z[1] + z[16];
    z[18]=z[4]*z[18];
    z[13]=4*z[13] + static_cast<T>(5)+ z[18];
    z[13]=z[3]*z[13];
    z[11]=z[11] + z[12] + z[14] + z[13] - 2*z[4];
    z[11]=z[5]*z[11];
    z[12]=2*z[9] - z[2];
    z[12]=z[2]*z[12];
    z[12]= - z[15] + z[12];
    z[12]=z[4]*z[12];
    z[13]=5*z[7] + z[2];
    z[13]=z[2]*z[13];
    z[14]=npow(z[2],2);
    z[15]=z[14]*z[3];
    z[18]=z[7]*z[15];
    z[13]=z[13] - 3*z[18];
    z[13]=z[3]*z[13];
    z[15]= - z[17] + z[15];
    z[15]=z[3]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[8]*z[15]*npow(z[7],2);
    z[12]=z[15] + z[13] + z[12] - z[17] - 2*z[7] + z[9];
    z[12]=z[8]*z[12];
    z[13]= - z[20] + 3;
    z[13]=z[3]*z[14]*z[13];
    z[14]=z[1] - z[17];
    z[14]=z[4]*z[2]*z[14];
    z[13]=z[13] - z[16] + z[14];
    z[13]=z[3]*z[13];
    z[14]= - z[9] + 3*z[2];
    z[14]=z[4]*z[14];

    r += static_cast<T>(1)+ z[11] + z[12] + z[13] + z[14];
 
    return r;
}

template double qqb_1lha_r45(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r45(const std::array<dd_real,31>&);
#endif
