#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r123(const std::array<T,31>& k) {
  T z[35];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[3];
    z[6]=k[10];
    z[7]=k[2];
    z[8]=k[25];
    z[9]=k[4];
    z[10]=k[29];
    z[11]=2*z[2];
    z[12]=npow(z[3],2);
    z[13]=z[11]*z[12];
    z[14]=z[13] - z[3];
    z[14]=z[14]*z[11];
    z[15]=z[12]*z[5];
    z[16]=z[15] + z[3];
    z[17]=z[13] + z[16];
    z[17]=z[5]*z[17];
    z[17]= - z[14] + z[17];
    z[17]=z[5]*z[17];
    z[18]=z[12]*z[2];
    z[19]=z[18] - z[3];
    z[20]=npow(z[2],2);
    z[21]=z[19]*z[20];
    z[17]=z[21] + z[17];
    z[17]=z[10]*z[17];
    z[21]=2*z[3];
    z[22]=z[18] - z[21];
    z[22]=z[22]*z[2];
    z[23]= - z[13] + 3*z[15];
    z[24]=2*z[5];
    z[23]=z[23]*z[24];
    z[17]=z[17] - z[22] + z[23];
    z[17]=z[10]*z[17];
    z[23]=z[20]*z[12];
    z[25]=z[16]*z[24];
    z[25]= - z[23] + z[25];
    z[25]=z[10]*z[25];
    z[26]=z[24]*z[12];
    z[27]=z[18] + z[3];
    z[25]=z[25] + z[26] - z[27];
    z[25]=z[10]*z[25];
    z[28]=z[16]*z[9];
    z[29]=npow(z[10],2)*z[28];
    z[25]=z[25] + z[29];
    z[25]=z[9]*z[25];
    z[29]=3*z[2];
    z[29]=z[29]*z[12];
    z[30]=4*z[3];
    z[17]=z[25] + z[17] + z[15] - z[30] - z[29];
    z[17]=z[9]*z[17];
    z[25]=3*z[3];
    z[31]=z[25] + z[15];
    z[31]=z[5]*z[31];
    z[32]=npow(z[5],2);
    z[16]=z[32]*z[16]*z[10];
    z[24]=z[24] + z[9];
    z[24]=z[10]*z[24]*z[28];
    z[33]=z[5]*z[3];
    z[33]=z[33] + 1;
    z[28]=3*z[33] - z[28];
    z[28]=z[9]*z[28];
    z[28]= - 3*z[5] + z[28];
    z[28]=z[8]*z[28];
    z[16]=z[28] + z[24] + z[16] + static_cast<T>(2)+ z[31];
    z[16]=z[9]*z[16];
    z[16]= - z[5] + z[16];
    z[24]=2*z[6];
    z[16]=z[16]*z[24];
    z[28]=z[15]*z[11];
    z[31]=z[1]*z[19];
    z[31]=2*z[31] + z[28] + static_cast<T>(1)- z[23];
    z[31]=z[4]*z[31];
    z[34]=4*z[5];
    z[12]=z[34]*z[12];
    z[24]=z[33]*z[24];
    z[24]=z[24] + z[12] - z[3] - z[29] + z[31];
    z[24]=z[1]*z[24];
    z[26]= - z[26] - z[25] - z[18];
    z[26]=z[9]*z[26];
    z[13]=z[13] + z[3];
    z[13]= - z[2]*z[13];
    z[31]=z[18] - z[30];
    z[33]= - z[5]*z[31];
    z[13]=z[26] + z[33] + static_cast<T>(6)+ z[13];
    z[13]=z[9]*z[13];
    z[26]= - z[2]*z[19];
    z[26]=static_cast<T>(2)+ z[26];
    z[26]=z[2]*z[26];
    z[33]=z[2]*z[3];
    z[34]= - static_cast<T>(2)+ z[33];
    z[34]=z[5]*z[34];
    z[13]=z[13] + z[26] + z[34];
    z[13]=z[8]*z[13];
    z[26]=z[7] + z[5] - 3*z[9];
    z[26]=z[8]*z[26];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[6]*z[26];
    z[26]= - 3*z[8] + z[26];
    z[26]=z[7]*z[26];
    z[13]=z[13] + z[26];
    z[19]=z[12] - z[19];
    z[19]=z[5]*z[19];
    z[14]= - z[14] + z[19];
    z[14]=z[5]*z[14];
    z[19]=z[29] - z[21];
    z[21]= - z[2]*z[19];
    z[21]=z[21] + z[28];
    z[21]=z[21]*z[32];
    z[22]=z[22] + 1;
    z[26]=z[22]*z[20];
    z[21]=z[26] + z[21];
    z[21]=z[10]*z[21];
    z[26]=z[2]*z[27];
    z[26]= - static_cast<T>(2)+ z[26];
    z[26]=z[2]*z[26];
    z[14]=z[21] + z[26] + z[14];
    z[14]=z[10]*z[14];
    z[21]= - 3*z[23] + z[28];
    z[21]=z[5]*z[21];
    z[26]= - static_cast<T>(1)+ 2*z[33];
    z[11]=z[26]*z[11];
    z[11]=z[11] + z[21];
    z[11]=z[5]*z[11];
    z[21]= - z[30] + z[29];
    z[21]=z[2]*z[21];
    z[19]= - z[19] + z[15];
    z[19]=z[5]*z[19];
    z[19]=z[19] + static_cast<T>(1)+ z[21];
    z[19]=z[5]*z[20]*z[19];
    z[21]= - z[22]*npow(z[2],3);
    z[19]=z[21] + z[19];
    z[19]=z[10]*z[19];
    z[21]=z[2]*z[31];
    z[21]=static_cast<T>(3)+ z[21];
    z[20]=z[21]*z[20];
    z[11]=z[19] + z[20] + z[11];
    z[11]=z[10]*z[11];
    z[15]=z[2]*z[15];
    z[15]= - z[23] + z[15];
    z[15]=z[5]*z[15];
    z[19]= - static_cast<T>(1)+ z[33];
    z[19]=z[2]*z[19];
    z[15]=z[19] + z[15];
    z[11]=2*z[15] + z[11];
    z[11]=z[4]*z[11];
    z[12]=z[25] + z[12] - z[18];
    z[12]=z[5]*z[12];
    z[15]=z[3] - z[29];
    z[15]=z[2]*z[15];

    r += static_cast<T>(4)+ z[11] + z[12] + 2*z[13] + z[14] + z[15] + z[16] + z[17] + 
      z[24];
 
    return r;
}

template double qqb_1lha_r123(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r123(const std::array<dd_real,31>&);
#endif
