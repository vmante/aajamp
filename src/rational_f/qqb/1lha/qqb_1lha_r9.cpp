#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r9(const std::array<T,31>& k) {
  T z[30];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[15];
    z[5]=k[19];
    z[6]=k[5];
    z[7]=k[3];
    z[8]=k[13];
    z[9]=npow(z[8],2);
    z[10]=z[9]*z[6];
    z[11]=npow(z[5],2);
    z[12]=z[10]*z[11];
    z[13]=z[5]*z[8];
    z[14]=z[9] + 2*z[13];
    z[15]=2*z[5];
    z[14]=z[14]*z[15];
    z[14]=z[14] + z[12];
    z[14]=z[6]*z[14];
    z[16]=npow(z[3],2);
    z[17]=2*z[8];
    z[18]=z[17] + z[5];
    z[18]=z[5]*z[18];
    z[14]=z[14] - z[16] + 3*z[18];
    z[14]=z[6]*z[14];
    z[18]=z[5]*z[1];
    z[19]=z[18] + 1;
    z[19]=z[19]*z[5];
    z[20]=z[3]*z[1];
    z[21]=static_cast<T>(5)+ 3*z[20];
    z[21]=z[3]*z[21];
    z[21]=z[21] + z[19];
    z[14]=2*z[21] + z[14];
    z[14]=z[6]*z[14];
    z[18]=z[18] - z[20];
    z[21]=z[10]*z[5];
    z[22]=4*z[13] + z[21];
    z[22]=z[6]*z[22];
    z[23]=z[3] + z[5];
    z[22]=3*z[23] + z[22];
    z[22]=z[6]*z[22];
    z[22]=2*z[18] + z[22];
    z[22]=z[6]*z[22];
    z[23]=npow(z[1],2);
    z[24]=z[23]*z[3];
    z[25]=z[23]*z[5];
    z[26]=z[24] - z[25];
    z[22]=z[22] - z[26];
    z[27]=2*z[4];
    z[22]=z[22]*z[27];
    z[28]=z[16]*z[23];
    z[23]=z[23]*z[11];
    z[14]=z[22] + z[14] - 5*z[28] + z[23];
    z[14]=z[4]*z[14];
    z[22]=3*z[8];
    z[11]=z[11]*z[22];
    z[11]=z[11] + z[12];
    z[11]=z[6]*z[11];
    z[23]=2*z[16];
    z[11]= - z[23] + z[11];
    z[11]=z[6]*z[11];
    z[29]=static_cast<T>(3)+ 8*z[20];
    z[29]=z[3]*z[29];
    z[11]=z[14] + z[11] + z[29] - z[19];
    z[11]=z[4]*z[11];
    z[14]=z[16]*z[6];
    z[29]=static_cast<T>(2)+ z[20];
    z[29]=z[3]*z[29];
    z[29]=2*z[29] - z[14];
    z[29]=z[6]*z[29];
    z[28]= - z[28] + z[29];
    z[28]=z[4]*z[28];
    z[29]=static_cast<T>(2)+ 5*z[20];
    z[29]=z[3]*z[29];
    z[14]=z[28] + z[29] - 3*z[14];
    z[14]=z[4]*z[14];
    z[14]= - z[23] + z[14];
    z[14]=z[14]*z[27];
    z[20]=2*z[20];
    z[28]=z[20] + 1;
    z[28]=z[28]*z[3];
    z[29]= - z[6]*z[23];
    z[29]=z[28] + z[29];
    z[29]=z[29]*z[27];
    z[29]= - 5*z[16] + z[29];
    z[29]=z[29]*npow(z[4],2);
    z[23]= - z[2]*npow(z[4],3)*z[23];
    z[23]=z[29] + z[23];
    z[23]=z[2]*z[23];
    z[14]=z[14] + z[23];
    z[14]=z[2]*z[14];
    z[11]=z[14] - z[16] + z[11];
    z[11]=z[2]*z[11];
    z[14]=4*z[9];
    z[16]= - z[21] + z[14] - z[13];
    z[16]=z[6]*z[16];
    z[16]=z[16] + z[3] + 5*z[8];
    z[16]=z[6]*z[16];
    z[16]=z[16] + z[18];
    z[16]=z[6]*z[16];
    z[18]=z[17] + z[10];
    z[21]=npow(z[6],3);
    z[18]=z[18]*z[21]*z[27];
    z[23]=2*z[26];
    z[16]=z[18] - z[23] + z[16];
    z[16]=z[16]*z[27];
    z[18]= - z[9] - z[13];
    z[18]=z[18]*z[15];
    z[18]=z[18] - z[12];
    z[18]=z[6]*z[18];
    z[15]= - z[22] - z[15];
    z[15]=z[5]*z[15];
    z[15]=2*z[18] + z[14] + z[15];
    z[15]=z[6]*z[15];
    z[18]=static_cast<T>(3)+ z[20];
    z[18]=z[3]*z[18];
    z[15]=z[15] - z[5] + z[18] + z[17];
    z[15]=z[6]*z[15];
    z[18]= - z[1] - 4*z[24];
    z[18]=z[3]*z[18];
    z[20]=z[1] + 2*z[25];
    z[20]=z[5]*z[20];
    z[15]=z[16] + z[15] + z[18] + z[20];
    z[15]=z[4]*z[15];
    z[16]=z[7]*z[17];
    z[16]= - static_cast<T>(5)+ z[16];
    z[16]=z[16]*z[13];
    z[14]= - z[14] + z[16];
    z[14]=z[5]*z[14];
    z[14]=z[14] - 4*z[12];
    z[14]=z[6]*z[14];
    z[16]=z[8]*z[7];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[5]*z[16];
    z[16]= - z[17] + z[16];
    z[16]=z[5]*z[16];
    z[14]=z[16] + z[14];
    z[14]=z[6]*z[14];
    z[11]=z[11] + z[15] + z[14] + z[28] - z[19];
    z[11]=z[2]*z[11];
    z[14]=z[22]*z[7];
    z[15]=z[14] - 2;
    z[16]=z[15]*z[17];
    z[18]=npow(z[7],2);
    z[19]=z[18]*z[22];
    z[19]= - z[19] + 4*z[7];
    z[19]=z[19]*z[8];
    z[19]=z[19] - 1;
    z[20]= - z[5]*z[19];
    z[20]= - z[16] + z[20];
    z[20]=z[5]*z[20];
    z[13]= - z[15]*z[13];
    z[13]=3*z[9] + z[13];
    z[13]=z[5]*z[13];
    z[12]=z[13] + z[12];
    z[12]=z[6]*z[12];
    z[12]=z[20] + z[12];
    z[12]=z[6]*z[12];
    z[13]= - z[8]*npow(z[7],3);
    z[13]=2*z[18] + z[13];
    z[13]=z[8]*z[13];
    z[13]= - z[7] + z[13];
    z[13]=z[5]*z[13];
    z[13]=z[13] - z[19];
    z[13]=z[5]*z[13];
    z[12]=z[13] + z[12];
    z[12]=z[6]*z[12];
    z[13]= - static_cast<T>(1)+ z[14];
    z[13]=z[13]*z[17];
    z[13]=z[13] - z[10];
    z[13]=z[13]*z[21];
    z[9]=z[7]*z[9]*npow(z[6],4)*z[27];
    z[9]=z[13] + z[9];
    z[9]=z[4]*z[9];
    z[10]=z[16] - 3*z[10];
    z[10]=z[10]*npow(z[6],2);
    z[9]=z[9] - z[23] + z[10];
    z[9]=z[4]*z[9];
    z[10]= - z[1] - z[24];
    z[10]=z[3]*z[10];
    z[13]=z[1] + z[25];
    z[13]=z[5]*z[13];

    r += z[9] + z[10] + z[11] + z[12] + z[13];
 
    return r;
}

template double qqb_1lha_r9(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r9(const std::array<dd_real,31>&);
#endif
