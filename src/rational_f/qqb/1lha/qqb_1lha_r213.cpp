#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r213(const std::array<T,31>& k) {
  T z[5];
  T r = 0;

    z[1]=k[3];
    z[2]=k[15];
    z[3]=k[4];
    z[4]=z[3] + z[1];
    z[4]=z[2]*z[4];

    r += static_cast<T>(1)+ z[4];
 
    return r;
}

template double qqb_1lha_r213(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r213(const std::array<dd_real,31>&);
#endif
