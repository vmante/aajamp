#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r200(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[9];
    z[3]=k[2];
    z[4]=k[11];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[14];
    z[8]=k[5];
    z[9]=z[7]*npow(z[8],2);
    z[9]=z[9] - z[8];
    z[10]= - z[5] + 2*z[8];
    z[10]=z[7]*z[10];
    z[11]= - static_cast<T>(1)+ z[10];
    z[11]=z[5]*z[11];
    z[11]=z[11] + z[3] - z[6] - z[9];
    z[11]=z[5]*z[11];
    z[12]=z[3]*z[2];
    z[13]=static_cast<T>(2)- z[12];
    z[13]=z[3]*z[13];
    z[13]= - z[6] + z[13];
    z[13]=z[3]*z[13];
    z[11]=z[13] + z[11];
    z[11]=z[4]*z[11];
    z[13]= - static_cast<T>(5)+ 3*z[12];
    z[13]=z[3]*z[13];
    z[10]= - z[5]*z[10];
    z[9]=z[11] + z[10] + z[13] + 2*z[6] + z[9];
    z[9]=z[4]*z[9];
    z[10]=z[2]*z[1];

    r += static_cast<T>(2)+ z[9] + z[10] - 2*z[12];
 
    return r;
}

template double qqb_1lha_r200(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r200(const std::array<dd_real,31>&);
#endif
