#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r199(const std::array<T,31>& k) {
  T z[9];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[15];
    z[5]=k[5];
    z[6]=z[4]*z[5];
    z[7]=z[3]*z[1];
    z[8]= - z[2] - z[5] + z[1];
    z[8]=z[4]*z[8];
    z[8]= - static_cast<T>(1)+ z[8];
    z[8]=z[3]*z[8];
    z[8]=z[4] + z[8];
    z[8]=z[2]*z[8];

    r += z[6] + z[7] + z[8];
 
    return r;
}

template double qqb_1lha_r199(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r199(const std::array<dd_real,31>&);
#endif
