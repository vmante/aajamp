#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r131(const std::array<T,31>& k) {
  T z[12];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[7];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=z[5]*z[6];
    z[9]=static_cast<T>(1)+ 2*z[8];
    z[10]=z[7]*z[5];
    z[11]=z[9]*z[10];
    z[9]= - z[5]*z[9];
    z[9]= - z[3] + z[9];
    z[9]=z[1]*z[9];
    z[9]=z[11] + z[9] - static_cast<T>(1)- 3*z[8];
    z[9]=z[7]*z[9];
    z[11]=z[5]*npow(z[6],2);
    z[11]=z[11] + z[6];
    z[8]= - static_cast<T>(1)- z[8];
    z[8]=z[1]*z[8];
    z[8]=z[8] + z[11];
    z[10]= - z[11]*z[10];
    z[8]=2*z[8] + z[10];
    z[8]=z[7]*z[8];
    z[10]=z[6] + z[2];
    z[11]= - z[2]*z[3];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[1]*z[11];
    z[11]=2*z[10] + z[11];
    z[11]=z[1]*z[11];
    z[10]= - z[6]*z[10];
    z[8]=z[8] + z[10] + z[11];
    z[8]=z[4]*z[8];
    z[10]=z[1]*z[3];
    z[10]= - static_cast<T>(2)+ z[10];
    z[10]=z[1]*z[10];
    z[8]=z[8] + z[9] + z[6] + z[10];
    z[8]=z[4]*z[8];
    z[9]= - z[7] + 2*z[1];
    z[10]=npow(z[5],2);
    z[9]=z[10]*z[9];
    z[9]=z[5] + z[9];
    z[9]=z[7]*z[9];
    z[10]= - z[1]*z[10];
    z[10]= - z[5] + z[10];
    z[10]=z[1]*z[10];

    r += z[8] + z[9] + z[10];
 
    return r;
}

template double qqb_1lha_r131(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r131(const std::array<dd_real,31>&);
#endif
