#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r212(const std::array<T,31>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[9];
    z[3]=k[2];
    z[4]=k[11];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[3];
    z[8]=k[13];
    z[9]=npow(z[7],2);
    z[10]=2*z[7] + z[5];
    z[10]=z[5]*z[10];
    z[9]=z[9] + z[10];
    z[9]=z[8]*z[9];
    z[10]=z[6]*z[8];
    z[11]=z[10] + 1;
    z[12]=z[5] + z[7];
    z[12]=z[12]*z[8];
    z[13]= - 2*z[12] + z[11];
    z[13]=z[6]*z[13];
    z[9]=z[13] + z[9] - 2*z[5] - z[7] + z[3];
    z[9]=z[6]*z[9];
    z[13]=z[3]*z[2];
    z[14]=static_cast<T>(3)- z[13];
    z[14]=z[14]*npow(z[3],2);
    z[15]= - 3*z[3] + z[5];
    z[15]=z[5]*z[15];
    z[9]=z[9] + z[14] + z[15];
    z[9]=z[4]*z[9];
    z[11]=z[12] - z[11];
    z[11]=z[6]*z[11];
    z[12]= - static_cast<T>(2)+ z[13];
    z[12]=z[3]*z[12];
    z[12]=z[12] + z[5];
    z[9]=z[9] + 3*z[12] + 2*z[11];
    z[9]=z[4]*z[9];
    z[11]=z[2]*z[1];

    r += static_cast<T>(2)+ z[9] + z[10] + z[11] - 2*z[13];
 
    return r;
}

template double qqb_1lha_r212(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r212(const std::array<dd_real,31>&);
#endif
