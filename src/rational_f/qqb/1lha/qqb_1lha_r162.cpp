#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r162(const std::array<T,31>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[10];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[2];
    z[7]=k[11];
    z[8]=k[5];
    z[9]=k[15];
    z[10]=npow(z[4],2);
    z[11]=z[10]*z[5];
    z[12]=2*z[4];
    z[13]=z[11] - z[12];
    z[14]=2*z[5];
    z[15]=z[13]*z[14];
    z[15]=z[15] + 1;
    z[16]=2*z[8];
    z[17]=z[2] - z[16];
    z[18]=z[11] - z[4];
    z[17]=z[18]*z[17];
    z[17]=z[17] + z[15];
    z[17]=z[2]*z[17];
    z[19]=3*z[4];
    z[20]= - z[19] + z[11];
    z[20]=z[5]*z[20];
    z[20]=static_cast<T>(3)+ z[20];
    z[20]=z[5]*z[20];
    z[21]=z[8]*z[18];
    z[15]=z[21] - z[15];
    z[15]=z[8]*z[15];
    z[15]= - z[6] + z[17] + z[20] + z[15];
    z[15]=z[7]*z[15];
    z[17]=z[10]*z[8];
    z[20]= - 4*z[17] - z[12] - z[11];
    z[21]=z[8]*z[5];
    z[20]=z[20]*z[21];
    z[14]= - z[16] - z[14];
    z[14]=z[10]*z[14];
    z[22]=z[10]*z[2];
    z[14]= - z[22] + z[4] + z[14];
    z[14]=z[2]*z[8]*z[14];
    z[14]=z[20] + z[14];
    z[14]=z[2]*z[14];
    z[20]=3*z[17];
    z[23]= - z[12] - z[20];
    z[21]=z[23]*z[21];
    z[23]=npow(z[8],2);
    z[24]= - z[23]*z[22];
    z[21]=z[21] + z[24];
    z[21]=z[2]*z[21];
    z[24]=npow(z[5],2);
    z[25]=z[24]*z[8];
    z[20]= - 4*z[4] - z[20];
    z[20]=z[20]*z[25];
    z[20]=z[21] - z[24] + z[20];
    z[20]=z[2]*z[20];
    z[12]= - z[12] - z[17];
    z[12]=z[8]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[12]*npow(z[5],3);
    z[12]=z[20] + z[12];
    z[12]=z[9]*z[12];
    z[20]=z[16]*z[10];
    z[19]= - z[19] - z[20];
    z[19]=z[19]*z[25];
    z[12]=z[12] + z[14] - z[24] + z[19];
    z[12]=z[9]*z[12];
    z[14]= - 2*z[18] - z[17];
    z[14]=z[8]*z[14];
    z[16]= - z[16]*z[22];
    z[14]=z[14] + z[16];
    z[14]=z[2]*z[14];
    z[11]= - z[23]*z[11];
    z[11]=z[12] + z[11] + z[14];
    z[11]=z[9]*z[11];
    z[12]=z[8] - z[5];
    z[13]= - z[13]*z[12];
    z[10]= - z[10]*z[12];
    z[10]= - z[4] + z[10];
    z[10]=z[2]*z[10];
    z[12]=z[1]*z[18];
    z[14]= - z[4] + z[22];
    z[14]=z[3]*z[14]*npow(z[1],2);

    r += static_cast<T>(1)+ z[10] + z[11] + z[12] + z[13] + z[14] + z[15];
 
    return r;
}

template double qqb_1lha_r162(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r162(const std::array<dd_real,31>&);
#endif
