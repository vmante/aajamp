#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r188(const std::array<T,31>& k) {
  T z[17];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[25];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[14];
    z[7]=k[5];
    z[8]=2*z[6];
    z[9]=z[4]*z[3];
    z[10]= - z[9]*z[8];
    z[10]= - z[3] + z[10];
    z[10]=z[6]*z[10];
    z[11]=z[2]*z[6];
    z[12]=z[7]*z[3]*npow(z[6],2);
    z[10]=z[12] + z[10] - z[11];
    z[10]=z[7]*z[10];
    z[12]=z[6]*npow(z[4],2);
    z[13]=z[3]*z[12];
    z[14]= - z[9] + z[13];
    z[14]=z[6]*z[14];
    z[15]=z[6]*z[4];
    z[16]= - static_cast<T>(1)- z[15];
    z[11]=z[16]*z[11];
    z[16]=z[6]*z[9];
    z[16]=z[3] + z[16];
    z[16]=z[6]*z[16];
    z[11]=z[16] + z[11];
    z[11]=z[5]*z[11];
    z[15]=static_cast<T>(1)+ 3*z[15];
    z[15]=z[2]*z[15];
    z[10]=z[10] + z[11] + z[14] + z[15];
    z[10]=z[7]*z[10];
    z[11]= - 2*z[9] - z[13];
    z[8]=z[11]*z[8];
    z[11]=z[9] + 2;
    z[14]=z[11]*z[12];
    z[15]=static_cast<T>(4)+ 3*z[9];
    z[15]=z[4]*z[15];
    z[14]=z[15] + z[14];
    z[14]=z[6]*z[14];
    z[14]=z[9] + z[14];
    z[14]=z[2]*z[14];
    z[8]=z[14] - z[3] + z[8];
    z[8]=z[5]*z[8];
    z[14]= - static_cast<T>(3)- z[9];
    z[12]=z[14]*z[12];
    z[11]= - z[4]*z[11];
    z[11]=z[11] + z[12];
    z[11]=z[2]*z[11];
    z[12]= - z[1] + z[5];
    z[12]=z[12]*z[3];
    z[12]=z[12] + static_cast<T>(1)+ z[9];
    z[12]=z[2]*z[12];
    z[12]=z[3] + z[12];
    z[12]=z[1]*z[12];

    r +=  - static_cast<T>(1)+ z[8] + z[9] + z[10] + z[11] + z[12] + 3*z[13];
 
    return r;
}

template double qqb_1lha_r188(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r188(const std::array<dd_real,31>&);
#endif
