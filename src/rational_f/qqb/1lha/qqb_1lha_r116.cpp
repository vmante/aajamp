#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r116(const std::array<T,31>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[9];
    z[6]=k[3];
    z[7]=npow(z[6],2);
    z[8]=npow(z[1],2);
    z[9]=2*z[6];
    z[10]=z[9] - z[2];
    z[10]=z[2]*z[10];
    z[10]=z[10] - z[7] + z[8];
    z[10]=z[2]*z[10];
    z[9]=z[9] - z[1];
    z[9]=z[1]*z[9];
    z[11]=z[1] - z[6];
    z[12]= - 2*z[11] - z[2];
    z[12]=z[2]*z[12];
    z[7]=z[12] - z[7] + z[9];
    z[7]=z[4]*z[7]*npow(z[2],2);
    z[7]=z[10] + z[7];
    z[7]=z[4]*z[7];
    z[9]=z[2]*z[5]*z[8];
    z[7]=z[9] + z[7];
    z[7]=z[3]*z[7];
    z[9]=z[2] - z[6];
    z[10]=2*z[1];
    z[12]=z[10] + z[9];
    z[12]=z[2]*z[12];
    z[11]=z[11]*z[1];
    z[12]=z[11] + z[12];
    z[12]=z[4]*z[12];
    z[9]=2*z[12] + z[9];
    z[9]=z[2]*z[9];
    z[9]= - z[11] + z[9];
    z[9]=z[4]*z[9];
    z[11]= - z[5]*z[11];
    z[7]=z[7] + z[11] + z[9];
    z[7]=z[3]*z[7];
    z[9]= - z[10] - z[2];
    z[9]=z[2]*z[9];
    z[8]= - z[8] + z[9];
    z[8]=z[8]*npow(z[4],2);

    r += z[7] + z[8];
 
    return r;
}

template double qqb_1lha_r116(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r116(const std::array<dd_real,31>&);
#endif
