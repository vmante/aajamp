#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r173(const std::array<T,31>& k) {
  T z[13];
  T r = 0;

    z[1]=k[2];
    z[2]=k[9];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[5];
    z[6]=k[4];
    z[7]=k[13];
    z[8]=z[1]*z[2];
    z[9]= - 2*z[5] + z[4];
    z[9]=z[2]*z[9];
    z[9]=2*z[8] - static_cast<T>(2)+ z[9];
    z[9]=z[1]*z[9];
    z[10]=z[5] - z[4];
    z[10]=z[2]*z[10];
    z[10]= - z[8] + static_cast<T>(2)+ z[10];
    z[10]=z[1]*z[10];
    z[10]=z[10] - z[6] + 2*z[4];
    z[10]=z[1]*z[10];
    z[11]=z[5]*z[7];
    z[11]=z[11] + 1;
    z[12]= - z[5]*z[11];
    z[12]=z[12] - z[4];
    z[12]=z[6]*z[12];
    z[10]=z[10] + z[12];
    z[10]=z[3]*z[10];
    z[11]= - z[4]*z[11];
    z[9]=z[10] + z[11] + z[9];
    z[9]=z[3]*z[9];
    z[10]=z[2]*z[5];

    r +=  - z[8] + z[9] + z[10];
 
    return r;
}

template double qqb_1lha_r173(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r173(const std::array<dd_real,31>&);
#endif
