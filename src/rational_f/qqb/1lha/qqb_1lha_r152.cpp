#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r152(const std::array<T,31>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[12];
    z[6]=k[3];
    z[7]=npow(z[6],2);
    z[8]=2*z[6];
    z[9]= - z[8] + z[1];
    z[9]=z[1]*z[9];
    z[9]=z[7] + z[9];
    z[9]=z[5]*z[9];
    z[10]=z[1] - z[6];
    z[11]=z[10]*z[5];
    z[12]=z[2]*z[5];
    z[13]=z[12] + static_cast<T>(1)+ 2*z[11];
    z[13]=z[2]*z[13];
    z[14]=z[4]*npow(z[1],2);
    z[8]=z[13] + z[9] - z[8] + z[14];
    z[8]=z[2]*z[8];
    z[7]=z[7] + z[8];
    z[7]=z[3]*z[7];
    z[8]= - z[4]*z[10];
    z[8]=z[8] - z[11];
    z[8]=z[1]*z[8];
    z[9]=z[6] - 2*z[1];
    z[9]=z[5]*z[9];
    z[9]=z[9] - z[12];
    z[9]=z[2]*z[9];
    z[7]=z[7] + z[9] + z[8];

    r += z[7]*z[3];
 
    return r;
}

template double qqb_1lha_r152(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r152(const std::array<dd_real,31>&);
#endif
