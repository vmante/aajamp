#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r68(const std::array<T,31>& k) {
  T z[24];
  T r = 0;

    z[1]=k[2];
    z[2]=k[9];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[5];
    z[6]=k[17];
    z[7]=z[4]*z[1];
    z[7]=8*z[7];
    z[8]=npow(z[1],2);
    z[9]=z[4]*z[6];
    z[10]= - static_cast<T>(2)- 3*z[9];
    z[10]=z[4]*z[10];
    z[10]= - 3*z[5] + 17*z[1] + z[10];
    z[10]=z[5]*z[10];
    z[10]=z[10] - 25*z[8] - z[7];
    z[10]=z[5]*z[10];
    z[11]=z[8]*z[4];
    z[12]=npow(z[1],3);
    z[13]=2*z[12] + z[11];
    z[14]=2*z[1];
    z[15]=z[14] - z[4];
    z[15]=z[5]*z[15];
    z[15]=z[15] - 6*z[8];
    z[15]=z[5]*z[15];
    z[13]=3*z[13] + z[15];
    z[13]=z[5]*z[13];
    z[15]=z[12]*z[4];
    z[16]=npow(z[1],4);
    z[17]=z[15] + z[16];
    z[13]= - 2*z[17] + z[13];
    z[18]=2*z[2];
    z[13]=z[13]*z[18];
    z[10]=z[13] + z[10] + 11*z[12] + 10*z[11];
    z[10]=z[2]*z[10];
    z[13]=z[4]*npow(z[6],2);
    z[19]=z[13] + 2*z[6];
    z[20]=2*z[4];
    z[21]= - z[19]*z[20];
    z[21]= - static_cast<T>(5)+ z[21];
    z[21]=z[5]*z[21];
    z[22]=static_cast<T>(1)- z[9];
    z[22]=z[4]*z[22];
    z[22]=7*z[1] + z[22];
    z[21]=2*z[22] + z[21];
    z[21]=z[5]*z[21];
    z[7]=z[10] + z[21] - 9*z[8] - z[7];
    z[7]=z[2]*z[7];
    z[10]=3*z[1];
    z[21]= - z[6] - z[13];
    z[21]=z[4]*z[21];
    z[21]= - static_cast<T>(2)+ z[21];
    z[21]=z[4]*z[21];
    z[21]=z[10] + z[21];
    z[21]=z[5]*z[21];
    z[22]=z[9] + 1;
    z[22]=z[22]*z[4];
    z[23]=z[22] - z[1];
    z[23]=z[23]*z[4];
    z[21]=z[21] - 13*z[8] + z[23];
    z[21]=z[5]*z[21];
    z[21]=z[21] + 17*z[12] + 16*z[11];
    z[21]=z[5]*z[21];
    z[23]= - z[8] - z[23];
    z[23]=z[5]*z[23];
    z[23]=3*z[12] + z[23];
    z[23]=z[5]*z[23];
    z[17]= - 3*z[17] + z[23];
    z[17]=z[5]*z[17];
    z[15]=2*z[16] + z[15];
    z[15]=z[4]*z[15];
    z[23]=npow(z[1],5);
    z[15]=z[17] + z[23] + z[15];
    z[15]=z[15]*z[18];
    z[11]= - 13*z[12] - 6*z[11];
    z[11]=z[4]*z[11];
    z[11]=z[15] + z[21] - 7*z[16] + z[11];
    z[11]=z[2]*z[11];
    z[15]=3*z[6] + z[13];
    z[15]=z[15]*z[20];
    z[15]=static_cast<T>(5)+ z[15];
    z[15]=z[4]*z[15];
    z[16]=z[19]*z[4];
    z[17]=z[5]*z[16];
    z[19]=6*z[1];
    z[15]=z[17] + z[19] + z[15];
    z[15]=z[5]*z[15];
    z[20]= - 8*z[1] + z[22];
    z[20]=z[4]*z[20];
    z[20]= - 7*z[8] + z[20];
    z[15]=2*z[20] + z[15];
    z[15]=z[5]*z[15];
    z[20]=z[4]*z[14];
    z[20]=5*z[8] + z[20];
    z[20]=z[4]*z[20];
    z[11]=z[11] + z[15] + 8*z[12] + 3*z[20];
    z[11]=z[2]*z[11];
    z[13]= - 4*z[6] - z[13];
    z[13]=z[4]*z[13];
    z[13]=static_cast<T>(3)+ z[13];
    z[13]=z[4]*z[13];
    z[13]= - z[17] + z[10] + z[13];
    z[13]=z[5]*z[13];
    z[15]= - static_cast<T>(3)- z[9];
    z[15]=z[4]*z[15];
    z[15]= - z[19] + z[15];
    z[15]=z[4]*z[15];
    z[17]=3*z[8];
    z[11]=z[11] + z[13] - z[17] + z[15];
    z[11]=z[3]*z[11];
    z[9]=static_cast<T>(2)+ z[9];
    z[9]=z[4]*z[9];
    z[13]= - static_cast<T>(2)+ z[16];
    z[13]=z[5]*z[13];
    z[7]=z[11] + z[7] + z[13] + z[14] + z[9];
    z[7]=z[3]*z[7];
    z[9]=z[14] - z[5];
    z[9]=z[5]*z[9];
    z[8]= - z[8] + z[9];
    z[9]=z[10] - z[5];
    z[9]=z[5]*z[9];
    z[9]= - z[17] + z[9];
    z[9]=z[5]*z[9];
    z[9]=z[12] + z[9];
    z[9]=z[2]*z[9];
    z[8]=2*z[8] + z[9];
    z[8]=z[2]*z[8];
    z[8]=z[8] + z[1] - z[5];
    z[8]=z[8]*z[18];

    r += z[7] + z[8];
 
    return r;
}

template double qqb_1lha_r68(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r68(const std::array<dd_real,31>&);
#endif
