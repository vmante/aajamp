#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r197(const std::array<T,31>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[2];
    z[5]=k[11];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[27];
    z[9]=npow(z[2],2);
    z[10]=2*z[2] + z[6];
    z[10]=z[6]*z[10];
    z[11]=z[7]*z[8];
    z[12]=static_cast<T>(3)- z[11];
    z[12]=z[7]*z[12];
    z[12]=z[12] - 3*z[2] - 4*z[6];
    z[12]=z[7]*z[12];
    z[9]=z[12] + z[9] + z[10];
    z[9]=z[5]*z[9];
    z[10]=2*z[6];
    z[12]=z[6]*z[11];
    z[12]= - z[10] + z[12];
    z[12]=z[7]*z[12];
    z[13]=z[6] + z[2];
    z[14]=z[6]*z[13];
    z[12]=z[14] + z[12];
    z[12]=z[5]*z[12];
    z[14]=z[11] - 1;
    z[14]=z[2]*z[14];
    z[12]=z[12] + z[6] + z[14];
    z[12]=z[7]*z[12];
    z[14]=z[2]*z[1];
    z[12]=z[14] + z[12];
    z[12]=z[3]*z[12];
    z[11]= - static_cast<T>(1)- z[11];
    z[11]=z[7]*z[11];
    z[9]=z[12] + z[9] + z[11] + z[13];
    z[9]=z[3]*z[9];
    z[10]=2*z[7] - z[10] + z[4] - z[2];
    z[10]=z[5]*z[10];

    r +=  - static_cast<T>(1)+ z[9] + z[10];
 
    return r;
}

template double qqb_1lha_r197(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r197(const std::array<dd_real,31>&);
#endif
