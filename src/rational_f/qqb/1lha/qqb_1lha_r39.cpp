#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r39(const std::array<T,31>& k) {
  T z[28];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[10];
    z[5]=k[5];
    z[6]=k[4];
    z[7]=k[19];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=npow(z[2],4);
    z[11]=z[10]*z[3];
    z[12]=npow(z[2],3);
    z[13]=z[11] + z[12];
    z[14]=z[3]*z[13];
    z[15]=z[11] + 3*z[12];
    z[15]=z[15]*z[3];
    z[16]=npow(z[2],2);
    z[17]=2*z[16];
    z[18]=z[17] + z[15];
    z[19]=2*z[3];
    z[20]=z[19]*z[6];
    z[18]=z[18]*z[20];
    z[14]= - 3*z[14] + z[18];
    z[14]=z[4]*z[14];
    z[18]=z[6]*z[2];
    z[21]= - z[17] + z[18];
    z[21]=z[6]*z[21];
    z[21]=z[12] + z[21];
    z[21]=z[4]*z[21];
    z[18]=z[21] - z[16] + z[18];
    z[18]=z[8]*z[18]*npow(z[7],2);
    z[21]=z[3]*z[12];
    z[21]=7*z[16] + 10*z[21];
    z[21]=z[3]*z[21];
    z[22]=z[19]*z[16];
    z[22]=z[22] + z[2];
    z[23]= - z[22]*z[20];
    z[14]=z[18] + z[14] + z[21] + z[23];
    z[14]=z[4]*z[14];
    z[18]=2*z[12];
    z[11]=z[18] + z[11];
    z[21]= - z[3]*z[11];
    z[23]=3*z[16];
    z[15]=z[15] + z[23];
    z[24]=z[3]*z[15];
    z[24]=z[2] + z[24];
    z[24]=z[6]*z[24];
    z[21]=z[24] - z[16] + z[21];
    z[24]=2*z[6];
    z[21]=z[4]*z[21]*z[24];
    z[25]=z[12]*z[19];
    z[25]=z[23] + z[25];
    z[25]=z[3]*z[25];
    z[25]=z[2] + z[25];
    z[26]=3*z[6];
    z[25]=z[25]*z[26];
    z[21]=z[25] + z[21];
    z[21]=z[4]*z[21];
    z[15]=z[6]*z[15];
    z[11]= - 2*z[11] + z[15];
    z[11]=z[6]*z[11];
    z[10]=z[10] + z[11];
    z[10]=z[4]*z[10];
    z[11]=4*z[3];
    z[12]=z[11]*z[12];
    z[15]=5*z[16] + z[12];
    z[15]=z[6]*z[15];
    z[10]=z[10] - z[18] + z[15];
    z[10]=z[4]*z[10];
    z[10]=z[17] + z[10];
    z[10]=z[7]*z[10];
    z[10]=z[10] + z[21] + z[22];
    z[10]=z[7]*z[10];
    z[15]= - z[22]*z[19];
    z[17]=z[3]*z[2];
    z[18]=static_cast<T>(1)+ z[17];
    z[11]=z[6]*z[18]*z[11];
    z[11]=z[11] + static_cast<T>(1)+ z[15];
    z[11]=z[6]*z[11];
    z[15]=4*z[17];
    z[18]= - z[20] - static_cast<T>(1)+ z[15];
    z[18]=z[6]*z[18];
    z[21]=z[16]*z[3];
    z[25]=z[2] - z[21];
    z[18]=2*z[25] + z[18];
    z[18]=z[6]*z[18];
    z[16]= - z[16] + z[18];
    z[16]=z[9]*z[16];
    z[11]=z[16] + z[11] - z[22];
    z[11]=z[9]*z[11];
    z[16]=npow(z[3],2);
    z[18]=z[24]*z[16];
    z[25]= - static_cast<T>(5)- z[15];
    z[25]=z[25]*z[18];
    z[19]=z[19]*z[2];
    z[26]= - static_cast<T>(3)- z[19];
    z[26]=z[3]*z[26];
    z[25]=z[26] + z[25];
    z[25]=z[6]*z[25];
    z[26]=static_cast<T>(1)- z[19];
    z[26]=z[3]*z[26];
    z[18]=z[26] + z[18];
    z[18]=z[6]*z[18];
    z[18]= - z[17] + z[18];
    z[18]=z[9]*z[18]*z[24];
    z[18]=z[25] + z[18];
    z[18]=z[9]*z[18];
    z[25]=npow(z[3],3);
    z[26]=z[25]*z[6];
    z[27]=z[16] + 3*z[26];
    z[27]=z[27]*z[24];
    z[24]= - z[25]*z[24];
    z[24]= - z[16] + z[24];
    z[24]=z[9]*z[24]*npow(z[6],2);
    z[24]=z[27] + z[24];
    z[24]=z[9]*z[24];
    z[24]=z[24] - z[16] - 6*z[26];
    z[24]=z[5]*z[24];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[6]*z[19]*z[16];
    z[17]=static_cast<T>(1)+ 6*z[17];
    z[17]=z[3]*z[17];
    z[17]=z[24] + z[18] + z[17] + 8*z[19];
    z[17]=z[5]*z[17];
    z[13]=z[4]*z[13];
    z[12]=z[13] - z[23] - z[12];
    z[12]=z[4]*z[12];
    z[12]=3*z[22] + z[12];
    z[12]=z[16]*z[12];
    z[13]= - static_cast<T>(1)- z[15];
    z[13]=z[13]*z[16];
    z[15]=z[5]*z[25];
    z[13]=z[13] + z[15];
    z[13]=z[5]*z[13];
    z[12]=z[13] + z[12];
    z[12]=z[1]*z[12];
    z[13]=5*z[2];
    z[15]= - z[13] - 12*z[21];
    z[15]=z[3]*z[15];
    z[13]= - z[13] - 6*z[21];
    z[13]=z[3]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[13]*z[20];

    r += z[10] + z[11] + 2*z[12] + z[13] + z[14] + z[15] + z[17];
 
    return r;
}

template double qqb_1lha_r39(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r39(const std::array<dd_real,31>&);
#endif
