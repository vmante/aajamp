#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r185(const std::array<T,31>& k) {
  T z[12];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[3];
    z[6]=k[27];
    z[7]=z[5]*z[4];
    z[7]=z[7] - 1;
    z[8]=z[1]*z[4];
    z[9]=z[8] - z[7];
    z[9]=z[1]*z[9];
    z[10]=z[2]*z[6];
    z[7]= - z[7]*z[10];
    z[11]= - z[6] + z[4];
    z[11]=z[5]*z[11];
    z[7]=z[7] - static_cast<T>(1)+ z[11];
    z[7]=z[2]*z[7];
    z[7]=z[7] + z[5] + z[9];
    z[7]=z[2]*z[7];
    z[9]= - z[1]*z[5];
    z[7]=z[9] + z[7];
    z[7]=z[3]*z[7];
    z[8]=z[8] + 1;
    z[8]= - z[1]*z[8];
    z[9]=static_cast<T>(1)- z[10];
    z[9]=z[2]*z[9];
    z[7]=z[7] + z[8] + z[9];

    r += z[7]*z[3];
 
    return r;
}

template double qqb_1lha_r185(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r185(const std::array<dd_real,31>&);
#endif
