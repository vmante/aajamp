#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r34(const std::array<T,31>& k) {
  T z[24];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[6];
    z[4]=k[15];
    z[5]=k[19];
    z[6]=k[5];
    z[7]=k[3];
    z[8]=k[13];
    z[9]=k[17];
    z[10]=z[5] + z[9];
    z[11]=z[2]*z[5];
    z[12]= - static_cast<T>(1)- z[11];
    z[12]=z[4]*z[12];
    z[12]=z[12] + z[10];
    z[12]=z[4]*z[12];
    z[13]=z[7]*z[9];
    z[14]=3*z[13];
    z[15]= - z[11] - static_cast<T>(4)- z[14];
    z[15]=z[4]*z[15];
    z[15]=4*z[10] + z[15];
    z[15]=z[8]*z[15];
    z[16]=z[5]*z[7];
    z[17]=z[2]*z[9];
    z[18]=z[17] - static_cast<T>(4)+ 3*z[16];
    z[18]=z[8]*z[18];
    z[19]=2*z[10];
    z[18]= - z[19] + z[18];
    z[18]=z[3]*z[18];
    z[12]=z[18] + 4*z[12] + z[15];
    z[12]=z[8]*z[12];
    z[13]= - z[11] - static_cast<T>(2)- z[13];
    z[13]=z[4]*z[13];
    z[13]=z[19] + z[13];
    z[13]=z[4]*z[13];
    z[15]= - z[3]*z[10];
    z[13]=z[13] + z[15];
    z[13]=z[6]*z[13]*npow(z[8],2);
    z[12]=z[13] + z[12];
    z[12]=z[6]*z[12];
    z[13]=2*z[2];
    z[15]= - z[9]*z[13];
    z[14]=z[15] - z[14] - 2*z[16];
    z[14]=z[8]*z[14];
    z[15]= - static_cast<T>(5)- 3*z[11];
    z[15]=z[4]*z[15];
    z[10]=z[14] + 5*z[10] + z[15];
    z[10]=z[8]*z[10];
    z[14]=npow(z[7],2);
    z[15]=z[5]*z[14];
    z[15]=z[13] + 5*z[7] - 3*z[15];
    z[15]=z[8]*z[15];
    z[15]=z[15] + z[17] - static_cast<T>(5)+ 4*z[16];
    z[15]=z[8]*z[15];
    z[18]=z[4]*z[2];
    z[19]=static_cast<T>(1)- 3*z[18];
    z[19]=z[4]*z[19];
    z[20]=z[3]*npow(z[18],2);
    z[15]=z[20] + z[15] + z[19] - 2*z[9] - z[5];
    z[15]=z[3]*z[15];
    z[19]=z[11]*z[4];
    z[20]=2*z[5];
    z[21]=z[20] - 3*z[19];
    z[21]=z[4]*z[21];
    z[10]=z[12] + z[15] + z[21] + z[10];
    z[10]=z[6]*z[10];
    z[12]=z[1] - z[13];
    z[12]=z[12]*z[18];
    z[15]=npow(z[2],2);
    z[21]=z[15]*z[4];
    z[22]=z[2] + z[21];
    z[23]=z[2] - z[1];
    z[22]=z[3]*z[23]*z[22];
    z[12]=z[12] + z[22];
    z[12]= - 3*z[2] + 2*z[12];
    z[12]=z[4]*z[12];
    z[22]=z[5]*npow(z[7],3);
    z[23]= - z[2]*z[7];
    z[22]=z[23] - 2*z[14] + z[22];
    z[22]=z[8]*z[22];
    z[14]= - z[14]*z[20];
    z[14]=z[22] + z[2] + 3*z[7] + z[14];
    z[14]=z[8]*z[14];
    z[12]=z[14] - static_cast<T>(1)+ z[16] + z[12];
    z[12]=z[3]*z[12];
    z[14]=2*z[1];
    z[22]= - npow(z[4],2)*z[11]*z[14];
    z[23]=z[8]*z[7];
    z[23]=z[23] - 1;
    z[17]=z[17]*z[23];
    z[16]= - z[16] + z[17];
    z[16]=z[8]*z[16];
    z[10]=z[10] + z[12] + z[16] + z[5] + z[22];
    z[10]=z[6]*z[10];
    z[12]=npow(z[1],2);
    z[15]=z[12] - z[15];
    z[15]=z[15]*z[18];
    z[16]= - z[1] - z[2];
    z[16]=z[2]*z[16];
    z[15]=z[15] + 2*z[12] + z[16];
    z[15]=z[4]*z[15];
    z[13]= - z[13] - z[21];
    z[13]=z[4]*z[13];
    z[13]=z[13] - 1;
    z[14]=z[14] - z[2];
    z[14]=z[14]*z[2];
    z[14]=z[14] - z[12];
    z[13]=z[3]*z[14]*z[13];
    z[14]= - z[5]*z[12];
    z[13]=z[13] + z[15] - z[1] + z[14];
    z[13]=z[3]*z[13];
    z[14]= - z[19] - z[20];
    z[12]=z[12]*z[14];
    z[11]=z[11]*z[1];
    z[11]=z[11] + z[12];
    z[11]=z[4]*z[11];
    z[12]=z[5]*z[1];

    r += z[10] + z[11] + z[12] + z[13];
 
    return r;
}

template double qqb_1lha_r34(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r34(const std::array<dd_real,31>&);
#endif
