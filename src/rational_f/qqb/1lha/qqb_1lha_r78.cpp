#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r78(const std::array<T,31>& k) {
  T z[31];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[5];
    z[5]=k[18];
    z[6]=k[14];
    z[7]=k[11];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=npow(z[9],2);
    z[11]=z[10]*z[3];
    z[12]=3*z[9];
    z[13]= - z[8] - z[12];
    z[13]=2*z[13] + z[11];
    z[13]=z[5]*z[13];
    z[14]=z[8]*z[7];
    z[15]=z[14] + 2;
    z[15]=2*z[15];
    z[16]= - 10*z[9] + z[11];
    z[16]=z[3]*z[16];
    z[13]=z[13] + z[15] + z[16];
    z[13]=z[5]*z[13];
    z[16]=npow(z[8],2);
    z[17]= - z[8] + z[12];
    z[17]=z[9]*z[17];
    z[17]= - z[16] + z[17];
    z[17]=z[5]*z[17];
    z[18]=2*z[14];
    z[19]=z[18] - 3;
    z[19]=z[19]*z[8];
    z[17]=z[17] + 3*z[11] + z[19] - 9*z[9];
    z[17]=z[5]*z[17];
    z[20]=2*z[2];
    z[21]=z[20] - 5*z[9];
    z[21]=z[3]*z[21];
    z[17]=z[17] + z[21] + static_cast<T>(6)+ z[14];
    z[17]=z[6]*z[17];
    z[21]=4*z[9];
    z[22]=3*z[2] - z[21];
    z[22]=z[3]*z[22];
    z[22]=static_cast<T>(13)+ z[22];
    z[22]=z[3]*z[22];
    z[13]=z[17] + z[13] + z[7] + z[22];
    z[13]=z[6]*z[13];
    z[17]= - z[11] + z[8] + z[21];
    z[17]=z[5]*z[17];
    z[11]=7*z[9] - z[11];
    z[11]=z[3]*z[11];
    z[11]=z[17] + z[11] - static_cast<T>(3)- z[14];
    z[11]=z[5]*z[11];
    z[17]=2*z[9];
    z[22]=z[17] - z[2];
    z[23]=z[3]*z[22];
    z[23]= - static_cast<T>(6)+ z[23];
    z[23]=z[3]*z[23];
    z[11]=z[23] + z[11];
    z[11]=z[6]*z[11];
    z[23]=z[17]*z[3];
    z[23]=z[23] - 1;
    z[24]=2*z[5];
    z[25]=z[23]*z[24];
    z[26]=z[3]*z[21];
    z[26]= - static_cast<T>(9)+ z[26];
    z[26]=z[3]*z[26];
    z[25]=z[26] + z[25];
    z[25]=z[5]*z[25];
    z[26]=npow(z[3],2);
    z[11]=z[11] - 6*z[26] + z[25];
    z[11]=z[6]*z[11];
    z[25]=z[5]*z[3];
    z[27]=z[25] + z[26];
    z[28]=z[27]*z[5];
    z[29]=2*z[3];
    z[30]=z[3]*z[9];
    z[30]=static_cast<T>(2)- z[30];
    z[30]=z[30]*z[29];
    z[23]= - z[5]*z[23];
    z[23]=z[30] + z[23];
    z[23]=z[5]*z[23];
    z[23]=2*z[26] + z[23];
    z[23]=z[6]*z[23];
    z[23]=3*z[28] + z[23];
    z[23]=z[6]*z[23];
    z[28]= - z[4]*npow(z[6],2)*z[28];
    z[23]=z[23] + z[28];
    z[23]=z[4]*z[23];
    z[27]= - z[27]*z[24];
    z[11]=z[23] + z[27] + z[11];
    z[11]=z[4]*z[11];
    z[23]=z[17] - z[1];
    z[25]= - z[23]*z[25];
    z[23]= - z[3]*z[23];
    z[23]=static_cast<T>(4)+ z[23];
    z[23]=z[3]*z[23];
    z[23]=z[23] + z[25];
    z[23]=z[5]*z[23];
    z[11]=z[11] + z[13] + 4*z[26] + z[23];
    z[11]=z[4]*z[11];
    z[13]= - z[10]*z[29];
    z[23]=z[8] - z[9];
    z[23]=z[9]*z[23];
    z[23]=z[16] + z[23];
    z[23]=z[23]*z[24];
    z[13]=z[23] + z[13] - z[19] + 8*z[9];
    z[13]=z[5]*z[13];
    z[19]=static_cast<T>(4)- z[14];
    z[19]=z[19]*z[16];
    z[23]= - z[8]*z[12];
    z[23]= - 2*z[16] + z[23];
    z[24]=z[5]*z[9];
    z[23]=z[23]*z[24];
    z[25]=5*z[8] - z[12];
    z[25]=z[9]*z[25];
    z[19]=z[23] + z[19] + z[25];
    z[19]=z[5]*z[19];
    z[23]=2*z[8];
    z[25]= - static_cast<T>(1)- z[14];
    z[25]=z[25]*z[23];
    z[19]=z[19] + z[21] - z[2] + z[25];
    z[19]=z[6]*z[19];
    z[20]= - z[20] + z[12];
    z[20]=z[3]*z[20];
    z[15]= - z[15] + z[20];
    z[13]=z[19] + 2*z[15] + z[13];
    z[13]=z[6]*z[13];
    z[12]= - z[1] + z[12];
    z[12]=z[3]*z[12];
    z[15]=z[8] + z[17];
    z[15]=z[5]*z[15];
    z[12]=z[15] - z[14] + z[12];
    z[12]=z[5]*z[12];
    z[15]=z[9] - z[2];
    z[17]= - z[1] + z[15];
    z[17]=z[17]*z[29];
    z[17]= - static_cast<T>(5)+ z[17];
    z[17]=z[3]*z[17];
    z[11]=z[11] + z[13] + z[12] - z[7] + z[17];
    z[11]=z[4]*z[11];
    z[12]=npow(z[8],3);
    z[10]=z[8]*z[10];
    z[10]= - z[12] + z[10];
    z[10]=z[5]*z[10];
    z[13]= - z[23] + z[9];
    z[13]=z[9]*z[13];
    z[10]=z[10] - z[16] + z[13];
    z[10]=z[5]*z[10];
    z[13]=z[9]*z[8];
    z[17]= - z[16] + z[13];
    z[17]=z[9]*z[17];
    z[19]=z[9]*z[16];
    z[19]=z[12] + z[19];
    z[19]=z[19]*z[24];
    z[17]=z[19] - z[12] + z[17];
    z[17]=z[5]*z[17];
    z[12]=z[7]*z[12];
    z[12]=z[17] + z[12] - z[13];
    z[12]=z[6]*z[12];
    z[14]=static_cast<T>(1)+ 3*z[14];
    z[14]=z[8]*z[14];
    z[10]=z[12] + z[10] + z[14] - z[22];
    z[10]=z[6]*z[10];
    z[12]= - z[16] - z[13];
    z[12]=z[5]*z[12];
    z[12]= - z[9] + z[12];
    z[12]=z[5]*z[12];
    z[13]=z[7]*z[15];
    z[14]=z[3]*z[1];
    z[14]=z[14] + 1;
    z[14]=z[2]*z[14];
    z[14]=z[14] - z[9] + z[1];
    z[14]=z[3]*z[14];

    r += static_cast<T>(2)+ z[10] + z[11] + z[12] + z[13] + z[14] + z[18];
 
    return r;
}

template double qqb_1lha_r78(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r78(const std::array<dd_real,31>&);
#endif
