#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r204(const std::array<T,31>& k) {
  T z[8];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[3];
    z[5]=k[10];
    z[6]= - z[5]*z[4];
    z[6]=static_cast<T>(1)+ z[6];
    z[6]=z[2]*z[6];
    z[6]= - z[5] + z[6];
    z[6]=z[2]*z[6];
    z[7]= - z[1]*z[3]*npow(z[2],2);
    z[6]=z[6] + z[7];

    r += z[6]*npow(z[1],2);
 
    return r;
}

template double qqb_1lha_r204(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r204(const std::array<dd_real,31>&);
#endif
