#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r146(const std::array<T,31>& k) {
  T z[19];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[12];
    z[4]=k[3];
    z[5]=k[10];
    z[6]=k[5];
    z[7]=k[9];
    z[8]=k[8];
    z[9]=k[4];
    z[10]=k[15];
    z[11]=z[4]*z[10];
    z[12]=z[9]*z[10];
    z[13]=z[11] + static_cast<T>(1)+ 2*z[12];
    z[13]=z[4]*z[13];
    z[14]=static_cast<T>(1)+ z[12];
    z[14]=z[9]*z[14];
    z[13]=z[14] + z[13];
    z[13]=z[3]*z[13];
    z[14]=z[6]*z[10];
    z[11]=z[14] - z[11];
    z[11]=z[4]*z[11];
    z[14]= - z[6]*z[8];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[9]*z[14];
    z[11]=z[14] + z[11];
    z[11]=z[2]*z[11];
    z[14]=z[4]*z[7];
    z[11]=z[11] + z[13] + z[12] - z[14];
    z[12]=z[7] - z[8];
    z[13]=2*z[6];
    z[13]= - z[12]*z[13];
    z[14]=2*z[8];
    z[15]= - z[9]*z[14];
    z[16]=2*z[7];
    z[17]= - z[4]*z[16];
    z[13]=z[17] + z[15] - static_cast<T>(5)+ z[13];
    z[13]=z[2]*z[13];
    z[15]= - z[6]*z[16];
    z[17]= - z[4]*z[5];
    z[18]=z[6] - z[4];
    z[18]=z[3]*z[18];
    z[15]=z[18] + z[15] + z[17];
    z[15]=z[2]*z[15];
    z[15]=z[15] + 2*z[3] + z[16] - 3*z[5] + z[14];
    z[15]=z[2]*z[15];
    z[14]= - z[5]*z[14];
    z[16]=z[1]*z[3]*npow(z[2],2);
    z[14]=z[16] + z[14] + z[15];
    z[14]=z[1]*z[14];
    z[12]=z[3] + z[12];
    z[12]=z[14] + 2*z[12] + z[13];
    z[12]=z[1]*z[12];

    r += 2*z[11] + z[12];
 
    return r;
}

template double qqb_1lha_r146(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r146(const std::array<dd_real,31>&);
#endif
