#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r19(const std::array<T,31>& k) {
  T z[34];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[18];
    z[5]=k[24];
    z[6]=k[5];
    z[7]=k[7];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=k[15];
    z[11]=npow(z[3],2);
    z[12]=z[11]*z[6];
    z[13]=z[12] - z[3];
    z[14]=z[4]*z[6];
    z[15]=z[13]*z[14];
    z[16]=z[12] + z[3];
    z[17]=z[11]*z[2];
    z[15]=z[17] + 2*z[16] + z[15];
    z[15]=z[2]*z[15];
    z[18]=npow(z[6],3);
    z[19]=z[18]*z[4];
    z[20]=z[11]*z[19];
    z[21]=z[6]*z[3];
    z[20]=z[21] + z[20];
    z[15]=3*z[20] + z[15];
    z[15]=z[2]*z[15];
    z[20]=z[18]*z[11];
    z[21]=3*z[19];
    z[22]=z[16]*z[21];
    z[15]=z[15] - 2*z[20] + z[22];
    z[15]=z[2]*z[15];
    z[22]=2*z[3];
    z[23]=z[12] + z[22];
    z[24]=z[23]*z[6];
    z[24]=z[24] + 1;
    z[25]=z[24]*z[19];
    z[18]= - z[16]*z[18];
    z[15]=z[15] + z[18] + z[25];
    z[15]=z[10]*z[15];
    z[18]=npow(z[6],2);
    z[26]=z[24]*z[18];
    z[25]=2*z[26] - z[25];
    z[25]=z[4]*z[25];
    z[27]=z[18]*z[11];
    z[28]=3*z[11];
    z[19]= - z[19]*z[28];
    z[19]=2*z[27] + z[19];
    z[19]=z[4]*z[19];
    z[28]=npow(z[4],2);
    z[29]= - z[6]*z[13]*z[28];
    z[29]=z[11] + z[29];
    z[29]=z[2]*z[29];
    z[19]=z[29] + z[19] + z[16];
    z[19]=z[2]*z[19];
    z[21]=4*z[18] - z[21];
    z[21]=z[4]*z[21];
    z[21]= - z[6] + z[21];
    z[21]=z[16]*z[21];
    z[19]=z[19] + z[21];
    z[19]=z[2]*z[19];
    z[21]= - z[23]*z[18];
    z[15]=z[15] + z[19] + z[21] + z[25];
    z[15]=z[10]*z[15];
    z[19]=2*z[4];
    z[18]= - z[16]*z[18]*z[19];
    z[21]= - z[1]*z[4]*z[27];
    z[18]=z[18] + z[21];
    z[18]=z[1]*z[18];
    z[21]=z[24]*z[6];
    z[23]=z[4]*z[26];
    z[24]=z[5]*npow(z[1],2)*z[27];
    z[18]=z[24] + z[18] + z[21] - z[23];
    z[18]=z[7]*z[18];
    z[24]=2*z[6];
    z[24]=z[24]*z[11];
    z[25]=z[24] + z[3];
    z[26]=z[25]*z[6];
    z[29]= - static_cast<T>(1)+ z[26];
    z[29]=z[29]*z[14];
    z[26]=z[29] - static_cast<T>(1)- z[26];
    z[26]=z[4]*z[26];
    z[29]=3*z[12];
    z[30]= - z[22] + z[29];
    z[30]=z[6]*z[30];
    z[30]= - static_cast<T>(2)+ z[30];
    z[30]=z[4]*z[30];
    z[30]=z[30] + z[3] - z[24];
    z[30]=z[4]*z[30];
    z[31]=z[12] - z[22];
    z[32]=z[28]*z[2];
    z[33]=z[31]*z[32];
    z[30]=z[30] + z[33];
    z[30]=z[2]*z[30];
    z[26]=z[26] + z[30];
    z[26]=z[2]*z[26];
    z[30]=z[6]*z[31];
    z[30]= - static_cast<T>(2)+ z[30];
    z[30]=z[30]*z[19];
    z[30]=z[30] - z[25];
    z[30]=z[4]*z[30];
    z[31]=z[17]*z[28];
    z[12]= - 5*z[3] - z[12];
    z[12]=z[12]*z[28];
    z[12]=z[12] - z[31];
    z[12]=z[2]*z[12];
    z[12]=z[30] + z[12];
    z[12]=z[2]*z[12];
    z[22]= - z[4]*z[22];
    z[11]= - z[11] + z[22];
    z[11]=z[4]*z[11];
    z[11]=z[11] - z[31];
    z[11]=z[2]*z[11];
    z[22]= - z[4] + z[13];
    z[22]=z[4]*z[22];
    z[11]=z[22] + z[11];
    z[11]=z[1]*z[11];
    z[22]= - static_cast<T>(2)+ z[27];
    z[22]=z[22]*z[19];
    z[11]=z[11] + z[22] + z[12];
    z[11]=z[1]*z[11];
    z[12]=z[2]*z[25];
    z[13]=z[17] - z[13];
    z[13]=z[1]*z[13];
    z[12]=z[13] + static_cast<T>(1)+ z[12];
    z[12]=z[1]*z[12];
    z[13]=z[2]*z[6];
    z[16]=z[16]*z[13];
    z[12]=z[12] + z[21] + z[16];
    z[12]=z[5]*z[12];
    z[16]=z[29] + z[17];
    z[16]=z[2]*z[16];
    z[16]=3*z[27] + z[16];
    z[16]=z[2]*z[16];
    z[16]=z[20] + z[16];
    z[16]=z[16]*npow(z[10],2);
    z[17]=z[28]*z[8];
    z[20]=3*z[28];
    z[20]=z[1]*z[20];
    z[16]= - 3*z[17] + z[20] + z[16] + 5*z[4] + 2*z[32];
    z[16]=z[8]*z[16];
    z[13]=z[13]*z[28];
    z[17]= - z[19] + z[17];
    z[17]=z[8]*z[17];
    z[17]=z[17] + static_cast<T>(1)- z[13];
    z[17]=z[8]*z[17];
    z[19]=z[14] + z[13];
    z[19]=z[2]*z[19];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[13]*npow(z[2],2);
    z[14]=static_cast<T>(1)- z[14];
    z[14]=z[10]*z[14]*npow(z[2],3);
    z[13]=z[13] + z[14];
    z[13]=z[10]*z[13];
    z[13]=z[17] + z[19] + z[13];
    z[13]=z[9]*z[13];
    z[14]=3*z[3] + z[24];
    z[14]=z[6]*z[14];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[6]*z[14];
    z[14]=z[14] - z[23];
    z[14]=z[4]*z[14];

    r +=  - static_cast<T>(2)+ z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[18]
       + z[26];
 
    return r;
}

template double qqb_1lha_r19(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r19(const std::array<dd_real,31>&);
#endif
