#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r178(const std::array<T,31>& k) {
  T z[8];
  T r = 0;

    z[1]=k[1];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[3];
    z[6]= - z[5] + z[2];
    z[6]=z[6]*npow(z[3],2);
    z[7]= - z[5] + 2*z[2];
    z[7]=z[3]*z[7];
    z[7]= - static_cast<T>(2)+ z[7];
    z[7]=z[4]*z[7];
    z[6]=z[6] + z[7];
    z[6]=z[4]*z[6];
    z[7]= - z[3]*z[2];
    z[7]=static_cast<T>(1)+ z[7];
    z[7]=z[1]*z[3]*z[7]*npow(z[4],2);
    z[6]=z[6] + z[7];
    z[6]=z[1]*z[6];
    z[7]=z[4]*z[5];
    z[7]=static_cast<T>(1)+ 2*z[7];
    z[7]=z[4]*z[7];
    z[6]=z[7] + z[6];

    r += z[6]*z[1];
 
    return r;
}

template double qqb_1lha_r178(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r178(const std::array<dd_real,31>&);
#endif
