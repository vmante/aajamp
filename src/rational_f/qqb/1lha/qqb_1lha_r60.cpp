#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r60(const std::array<T,31>& k) {
  T z[25];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[15];
    z[4]=k[19];
    z[5]=k[3];
    z[6]=k[5];
    z[7]=k[13];
    z[8]=npow(z[2],3);
    z[9]=z[8]*z[3];
    z[10]=npow(z[2],2);
    z[11]=z[9] + z[10];
    z[12]=2*z[3];
    z[13]= - z[11]*z[12];
    z[14]=3*z[3];
    z[15]= - z[11]*z[14];
    z[15]=z[2] + z[15];
    z[16]=z[6]*z[3];
    z[15]=z[15]*z[16];
    z[13]=z[15] + z[2] + z[13];
    z[15]=2*z[6];
    z[13]=z[13]*z[15];
    z[17]=npow(z[3],2);
    z[18]=z[17]*z[8];
    z[19]=2*z[2];
    z[20]=z[19] - z[18];
    z[21]=z[12]*z[8];
    z[22]= - z[10] - z[21];
    z[22]=z[3]*z[22];
    z[22]=z[19] + z[22];
    z[22]=z[22]*z[16];
    z[20]=2*z[20] + z[22];
    z[20]=z[6]*z[20];
    z[22]=2*z[10];
    z[20]=z[22] + z[20];
    z[23]=z[7]*z[6];
    z[20]=z[20]*z[23];
    z[13]=z[13] + z[20];
    z[13]=z[7]*z[13];
    z[20]= - z[8]*z[14];
    z[20]=z[10] + z[20];
    z[20]=z[20]*z[16];
    z[20]=z[20] + z[10] - z[21];
    z[20]=z[6]*z[20];
    z[21]=z[10] - z[9];
    z[21]=z[21]*z[16];
    z[21]=z[21] + z[22] - z[9];
    z[21]=z[7]*z[21]*npow(z[6],2);
    z[20]=z[20] + z[21];
    z[20]=z[7]*z[20];
    z[18]= - z[15]*z[18];
    z[18]=z[18] + z[20];
    z[18]=z[4]*z[18];
    z[9]=z[22] + z[9];
    z[9]=z[9]*z[3];
    z[9]=z[9] + z[2];
    z[20]= - z[9]*z[12];
    z[11]=z[11]*z[3];
    z[21]= - z[4]*z[11];
    z[20]=z[20] + z[21];
    z[20]=z[1]*z[20];
    z[21]=4*z[3];
    z[8]= - z[8]*z[21];
    z[8]= - 5*z[10] + z[8];
    z[22]=z[17]*z[6];
    z[8]=z[8]*z[22];
    z[8]=z[20] + z[18] + z[13] - z[11] + z[8];
    z[8]=z[4]*z[8];
    z[11]=z[21]*z[10];
    z[13]=3*z[2];
    z[11]=z[11] + z[13];
    z[18]= - z[11]*z[12];
    z[11]= - z[11]*z[14];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[11]*z[16];
    z[11]=z[11] + static_cast<T>(1)+ z[18];
    z[11]=z[6]*z[11];
    z[16]= - z[10]*z[12];
    z[18]=5*z[2];
    z[20]=z[10]*z[3];
    z[24]= - z[18] - 6*z[20];
    z[24]=z[24]*z[22];
    z[20]= - 7*z[2] - 8*z[20];
    z[20]=z[3]*z[20];
    z[20]=z[20] + z[24];
    z[20]=z[6]*z[20];
    z[16]=z[16] + z[20];
    z[16]=z[16]*z[23];
    z[11]=z[11] + z[16];
    z[11]=z[7]*z[11];
    z[16]= - z[17]*z[19];
    z[20]=z[6]*npow(z[3],3);
    z[13]= - z[13]*z[20];
    z[13]=z[16] + z[13];
    z[13]=z[13]*z[15];
    z[16]= - z[3]*z[18];
    z[16]= - static_cast<T>(3)+ z[16];
    z[16]=z[16]*z[12];
    z[12]= - z[2]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[12]*z[22];
    z[12]=z[16] + 3*z[12];
    z[12]=z[6]*z[12];
    z[16]= - z[2]*z[21];
    z[12]=z[12] - static_cast<T>(3)+ z[16];
    z[12]=z[12]*z[23];
    z[12]=z[13] + z[12];
    z[12]=z[7]*z[12];
    z[13]= - z[4]*z[10];
    z[13]= - z[19] + z[13];
    z[13]=z[4]*z[13]*z[6];
    z[16]= - 2*z[17] - z[20];
    z[16]=z[6]*z[16];
    z[16]= - z[3] + z[16];
    z[15]=z[5]*z[16]*z[15];
    z[13]=z[15] + z[13];
    z[13]=z[13]*npow(z[7],2);
    z[12]=z[12] + z[13];
    z[12]=z[5]*z[12];
    z[9]= - z[3]*z[9];
    z[10]= - z[10]*z[14];
    z[10]= - z[19] + z[10];
    z[10]=z[10]*z[22];
    z[9]=z[9] + z[10];

    r += z[8] + 2*z[9] + z[11] + z[12];
 
    return r;
}

template double qqb_1lha_r60(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r60(const std::array<dd_real,31>&);
#endif
