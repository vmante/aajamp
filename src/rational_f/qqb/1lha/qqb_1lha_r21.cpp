#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r21(const std::array<T,31>& k) {
  T z[29];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[15];
    z[4]=k[19];
    z[5]=k[3];
    z[6]=k[12];
    z[7]=k[5];
    z[8]=3*z[7];
    z[9]=npow(z[4],2);
    z[10]=z[8]*z[9];
    z[11]= - 20*z[4] - z[10];
    z[11]=z[7]*z[11];
    z[12]=7*z[4];
    z[13]=z[9]*z[7];
    z[14]= - z[12] - z[13];
    z[14]=z[7]*z[14];
    z[14]= - static_cast<T>(35)+ z[14];
    z[14]=z[7]*z[14];
    z[14]= - 11*z[5] + z[14];
    z[14]=z[6]*z[14];
    z[11]=z[14] - static_cast<T>(25)+ z[11];
    z[11]=z[6]*z[11];
    z[14]=npow(z[5],2);
    z[15]=z[7]*z[4];
    z[16]= - static_cast<T>(9)- z[15];
    z[16]=z[7]*z[16];
    z[16]= - 9*z[5] + z[16];
    z[16]=z[7]*z[16];
    z[16]= - z[14] + z[16];
    z[16]=z[6]*z[16];
    z[17]= - static_cast<T>(4)- z[15];
    z[17]=z[7]*z[17];
    z[17]= - z[5] + z[17];
    z[16]=3*z[17] + z[16];
    z[16]=z[6]*z[16];
    z[17]=2*z[15];
    z[18]=z[4]*z[1];
    z[16]=z[16] - z[17] - static_cast<T>(3)- z[18];
    z[19]=2*z[3];
    z[16]=z[16]*z[19];
    z[20]= - static_cast<T>(9)- z[18];
    z[20]=z[4]*z[20];
    z[11]=z[16] + z[11] + z[20] - 2*z[13];
    z[11]=z[3]*z[11];
    z[16]=4*z[4];
    z[20]= - z[16] - z[13];
    z[21]=2*z[4];
    z[22]= - z[21] + z[13];
    z[22]=z[7]*z[22];
    z[22]= - static_cast<T>(12)+ z[22];
    z[22]=z[6]*z[22];
    z[20]=2*z[20] + z[22];
    z[20]=z[6]*z[20];
    z[22]=2*z[9];
    z[11]=z[11] - z[22] + z[20];
    z[11]=z[3]*z[11];
    z[20]= - 5*z[4] - z[10];
    z[20]=z[6]*z[20];
    z[23]=6*z[3];
    z[24]=z[15] + 1;
    z[25]= - z[6]*z[24];
    z[25]= - z[4] + z[25];
    z[25]=z[25]*z[23];
    z[26]=3*z[9];
    z[20]=z[25] - z[26] + z[20];
    z[20]=z[3]*z[6]*z[20];
    z[25]=npow(z[6],2);
    z[9]=z[25]*z[9];
    z[20]= - z[9] + z[20];
    z[20]=z[3]*z[20];
    z[27]=z[25]*z[3];
    z[28]= - z[21]*z[27];
    z[9]= - z[9] + z[28];
    z[9]=z[2]*z[9]*npow(z[3],2);
    z[9]=z[20] + z[9];
    z[9]=z[2]*z[9];
    z[12]= - z[12] - z[10];
    z[10]= - 11*z[4] - z[10];
    z[10]=z[7]*z[10];
    z[10]= - static_cast<T>(15)+ z[10];
    z[10]=z[6]*z[10];
    z[10]=2*z[12] + z[10];
    z[10]=z[6]*z[10];
    z[12]= - static_cast<T>(3)- z[15];
    z[12]=z[7]*z[12];
    z[12]= - z[5] + z[12];
    z[12]=z[6]*z[12];
    z[12]= - 2*z[24] + z[12];
    z[12]=z[6]*z[12];
    z[12]= - z[21] + 3*z[12];
    z[12]=z[12]*z[19];
    z[10]=z[12] - z[22] + z[10];
    z[10]=z[3]*z[10];
    z[12]=z[13] + 3*z[4];
    z[19]= - z[6]*z[12];
    z[19]= - z[26] + z[19];
    z[19]=z[6]*z[19];
    z[10]=z[19] + z[10];
    z[10]=z[3]*z[10];
    z[9]=z[10] + z[9];
    z[9]=z[2]*z[9];
    z[10]=z[25]*z[13];
    z[9]=z[9] + z[10] + z[11];
    z[9]=z[2]*z[9];
    z[10]=4*z[5];
    z[11]= - static_cast<T>(17)- 3*z[15];
    z[11]=z[7]*z[11];
    z[11]= - z[10] + z[11];
    z[19]=2*z[14];
    z[20]= - static_cast<T>(25)- z[15];
    z[20]=z[7]*z[20];
    z[20]= - 25*z[5] + z[20];
    z[20]=z[7]*z[20];
    z[20]= - z[19] + z[20];
    z[20]=z[6]*z[20];
    z[11]=2*z[11] + z[20];
    z[11]=z[6]*z[11];
    z[20]=3*z[5];
    z[22]= - z[20] - z[7];
    z[22]=z[7]*z[22];
    z[22]= - z[14] + z[22];
    z[24]=z[6]*z[7];
    z[22]=z[22]*z[24];
    z[25]= - z[5] - z[7];
    z[25]=z[7]*z[25];
    z[22]=2*z[25] + z[22];
    z[22]=z[6]*z[22];
    z[22]= - z[7] + z[22];
    z[22]=z[22]*z[23];
    z[25]= - z[1]*z[21];
    z[25]= - static_cast<T>(5)+ z[25];
    z[11]=z[22] + z[11] + 2*z[25] - 5*z[15];
    z[11]=z[3]*z[11];
    z[16]= - z[16] + z[13];
    z[16]=z[7]*z[16];
    z[12]=z[7]*z[12];
    z[12]= - static_cast<T>(20)+ z[12];
    z[12]=z[7]*z[12];
    z[12]= - 5*z[5] + z[12];
    z[12]=z[6]*z[12];
    z[12]=z[12] - static_cast<T>(15)+ z[16];
    z[12]=z[6]*z[12];
    z[16]= - static_cast<T>(5)- z[18];
    z[16]=z[4]*z[16];
    z[11]=z[11] + z[16] + z[12];
    z[11]=z[3]*z[11];
    z[12]=z[21] + z[13];
    z[12]=z[7]*z[12];
    z[12]= - static_cast<T>(3)+ z[12];
    z[12]=z[6]*z[12];
    z[12]=z[13] + z[12];
    z[12]=z[6]*z[12];
    z[9]=z[9] + z[12] + z[11];
    z[9]=z[2]*z[9];
    z[11]= - 17*z[5] - 5*z[7];
    z[11]=z[7]*z[11];
    z[11]= - 4*z[14] + z[11];
    z[11]=z[11]*z[24];
    z[12]= - z[7]*z[5];
    z[12]= - z[14] + z[12];
    z[13]=npow(z[7],2);
    z[14]=z[13]*z[6];
    z[12]=z[12]*z[14];
    z[16]=z[13]*z[5];
    z[12]= - z[16] + z[12];
    z[12]=z[12]*z[23];
    z[16]= - 8*z[5] - 9*z[7];
    z[16]=z[7]*z[16];
    z[11]=z[12] + z[16] + z[11];
    z[11]=z[6]*z[11];
    z[11]= - 4*z[7] + z[11];
    z[11]=z[3]*z[11];
    z[12]= - static_cast<T>(4)+ z[15];
    z[12]=z[7]*z[12];
    z[10]= - z[10] + z[12];
    z[10]=z[10]*z[24];
    z[12]= - static_cast<T>(5)+ z[15];
    z[12]=z[7]*z[12];
    z[10]=z[10] - z[5] + z[12];
    z[10]=z[6]*z[10];
    z[10]=z[10] - static_cast<T>(2)- z[18];
    z[10]=2*z[10] + z[11];
    z[10]=z[3]*z[10];
    z[11]= - static_cast<T>(3)+ z[17];
    z[11]=z[11]*z[24];
    z[12]= - static_cast<T>(1)+ z[15];
    z[11]=2*z[12] + z[11];
    z[11]=z[6]*z[11];
    z[9]=z[9] + z[11] + z[10];
    z[9]=z[2]*z[9];
    z[8]= - z[5]*z[8];
    z[8]= - z[19] + z[8];
    z[8]=z[8]*npow(z[24],2);
    z[10]= - npow(z[7],3)*z[19]*z[27];
    z[8]=z[8] + z[10];
    z[8]=z[3]*z[8];
    z[10]= - z[14]*z[20];
    z[10]=z[13] + z[10];
    z[10]=z[6]*z[10];
    z[8]=z[10] + z[8];
    z[8]=z[3]*z[8];

    r += z[8] + z[9] + z[24];
 
    return r;
}

template double qqb_1lha_r21(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r21(const std::array<dd_real,31>&);
#endif
