#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r163(const std::array<T,31>& k) {
  T z[14];
  T r = 0;

    z[1]=k[2];
    z[2]=k[9];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[5];
    z[6]=k[4];
    z[7]=k[14];
    z[8]=z[2]*z[1];
    z[9]= - z[5]*z[2];
    z[9]=z[9] + z[8];
    z[10]=npow(z[3],2);
    z[11]=z[10]*z[1];
    z[12]=z[11] - 2*z[3];
    z[12]=z[12]*z[1];
    z[13]=z[12] + 1;
    z[9]=z[13]*z[9];
    z[13]= - z[4]*z[3];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[5]*z[13];
    z[10]= - z[4]*z[10];
    z[10]=z[3] + z[10];
    z[10]=z[6]*z[4]*z[10];
    z[10]=z[10] + z[13];
    z[10]=z[7]*z[10];
    z[13]= - z[3] + z[11];
    z[8]=z[13]*z[8];
    z[8]=z[8] + z[3] - 2*z[11];
    z[8]=z[4]*z[8];

    r +=  - static_cast<T>(2)+ z[8] + z[9] + z[10] - z[12];
 
    return r;
}

template double qqb_1lha_r163(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r163(const std::array<dd_real,31>&);
#endif
