#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r205(const std::array<T,31>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[2];
    z[4]=k[11];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[14];
    z[8]=k[5];
    z[9]= - z[6] + z[5];
    z[9]=z[4]*z[9];
    z[9]=static_cast<T>(2)+ z[9];
    z[10]= - 3*z[2] - z[4];
    z[10]=z[8]*z[10];
    z[9]=2*z[9] + z[10];
    z[9]=z[8]*z[9];
    z[10]=z[4]*z[6];
    z[11]=z[8]*z[2];
    z[11]=z[11] - static_cast<T>(2)+ z[10];
    z[11]=z[8]*z[11];
    z[12]=static_cast<T>(1)- 2*z[10];
    z[12]=z[5]*z[12];
    z[11]=z[11] + z[12];
    z[11]=z[8]*z[11];
    z[10]=npow(z[5],2)*z[10];
    z[10]=z[10] + z[11];
    z[10]=z[7]*z[10];
    z[11]= - z[5] + 2*z[6];
    z[11]=z[11]*z[4];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[5]*z[11];
    z[9]=z[10] + z[9] + z[11];
    z[9]=z[7]*z[9];
    z[10]= - z[2]*z[1];
    z[11]=z[3] - z[5];
    z[11]=z[4]*z[11];
    z[12]=2*z[2] + z[4];
    z[12]=z[8]*z[12];

    r +=  - static_cast<T>(2)+ z[9] + z[10] + z[11] + z[12];
 
    return r;
}

template double qqb_1lha_r205(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r205(const std::array<dd_real,31>&);
#endif
