#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r86(const std::array<T,31>& k) {
  T z[17];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[15];
    z[4]=k[19];
    z[5]=k[3];
    z[6]=k[5];
    z[7]=k[13];
    z[8]=k[17];
    z[9]=z[2]*z[4];
    z[10]=z[9] + 2;
    z[11]=z[10]*z[2];
    z[11]=z[11] + z[5];
    z[11]=z[11]*z[3];
    z[12]=z[5]*z[8];
    z[13]=2*z[12];
    z[14]=3*z[11] - z[13] - z[9];
    z[14]=z[3]*z[14];
    z[13]=static_cast<T>(5)+ z[13];
    z[13]=z[5]*z[13];
    z[15]=static_cast<T>(4)+ z[9];
    z[15]=z[2]*z[15];
    z[13]=z[13] + z[15];
    z[13]=z[3]*z[13];
    z[15]=2*z[9];
    z[13]=z[13] - z[12] - z[15];
    z[13]=z[7]*z[13];
    z[13]=z[14] + z[13];
    z[13]=z[7]*z[13];
    z[14]=z[12] + z[9];
    z[16]=static_cast<T>(3)+ z[12];
    z[16]=z[5]*z[16];
    z[9]=static_cast<T>(3)+ z[9];
    z[9]=z[2]*z[9];
    z[9]=z[16] + z[9];
    z[9]=z[3]*z[9];
    z[9]=z[9] - z[14];
    z[9]=z[6]*z[3]*z[9]*npow(z[7],2);
    z[9]=z[13] + z[9];
    z[9]=z[6]*z[9];
    z[10]=z[12] + z[10];
    z[10]=z[7]*z[5]*z[10];
    z[10]=z[10] + 2*z[11] - z[14];
    z[10]=z[7]*z[10];
    z[11]=static_cast<T>(3)+ z[15];
    z[11]=z[2]*z[11]*npow(z[3],2);
    z[9]=z[9] + z[11] + z[10];
    z[9]=z[6]*z[9];
    z[10]=z[3]*npow(z[2],2);
    z[10]=z[2] + z[10];
    z[11]=z[4]*z[1];
    z[11]=z[11] + 1;
    z[10]=z[3]*z[11]*z[10];

    r += z[9] + z[10];
 
    return r;
}

template double qqb_1lha_r86(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r86(const std::array<dd_real,31>&);
#endif
