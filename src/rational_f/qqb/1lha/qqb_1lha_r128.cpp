#include "qqb_1lha_rf_decl.hpp"

template<class T>
T qqb_1lha_r128(const std::array<T,31>& k) {
  T z[19];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[10];
    z[4]=k[7];
    z[5]=k[28];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=k[5];
    z[9]=2*z[6];
    z[10]=z[4]*z[5];
    z[11]=z[10]*z[9];
    z[12]=3*z[5];
    z[13]=z[12] + 2*z[4];
    z[11]=z[11] + z[13];
    z[11]=z[6]*z[11];
    z[14]=z[3]*z[9];
    z[11]=z[11] + z[14];
    z[11]=z[3]*z[11];
    z[14]=z[3]*npow(z[6],2);
    z[14]= - z[9] + z[14];
    z[14]=z[3]*z[14];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[7]*z[10]*z[14];
    z[15]=z[10]*z[6];
    z[11]=z[14] + z[11] - 4*z[15] - z[13];
    z[11]=z[7]*z[11];
    z[13]=2*z[7];
    z[14]=z[13]*z[5];
    z[14]=z[14] - 1;
    z[16]=npow(z[3],2);
    z[14]=z[16]*z[6]*z[14];
    z[17]= - z[5] + z[3];
    z[17]=z[3]*z[17];
    z[16]=z[16]*z[2];
    z[18]=z[5]*z[16];
    z[17]=z[17] + z[18];
    z[17]=z[2]*z[17];
    z[14]=z[17] + z[14] + 2*z[5];
    z[14]=z[2]*z[14];
    z[12]=z[12] + z[4];
    z[15]=z[15] + z[12];
    z[15]=z[6]*z[15];
    z[9]=z[8] + z[13] - z[9];
    z[9]=z[10]*z[9];
    z[9]=z[9] - z[12];
    z[9]=z[8]*z[9];
    z[10]=z[4] + z[16];
    z[10]=z[1]*z[10];

    r += static_cast<T>(2)+ z[9] + z[10] + z[11] + z[14] + z[15];
 
    return r;
}

template double qqb_1lha_r128(const std::array<double,31>&);
#ifdef HAVE_QD

template dd_real qqb_1lha_r128(const std::array<dd_real,31>&);
#endif
