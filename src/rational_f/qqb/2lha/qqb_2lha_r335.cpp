#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r335(const std::array<T,30>& k) {
  T z[19];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[11];
    z[5]=k[3];
    z[6]=k[10];
    z[7]=k[13];
    z[8]=k[6];
    z[9]=z[5] - n<T>(1,4);
    z[10]=n<T>(1,3)*z[4];
    z[11]=z[9]*z[10];
    z[12]=n<T>(1,3)*z[5];
    z[13]=n<T>(5,4)*z[8];
    z[14]= - z[13] + z[12] + n<T>(5,4);
    z[14]=z[14]*z[7];
    z[9]=n<T>(1,3)*z[9] + z[14];
    z[9]=z[7]*z[9];
    z[9]=z[11] + z[9];
    z[9]=z[6]*z[9];
    z[11]=n<T>(1,6)*z[3];
    z[12]= - z[11] + n<T>(1,2) + z[12];
    z[12]=z[4]*z[12];
    z[9]=z[12] + z[9];
    z[12]=z[14] + static_cast<T>(1)+ n<T>(1,6)*z[5];
    z[12]=z[7]*z[12];
    z[9]=z[12] + n<T>(1,2)*z[9];
    z[9]=z[6]*z[9];
    z[12]=n<T>(1,2)*z[3];
    z[15]=z[1] - 1;
    z[15]=z[15]*z[3];
    z[16]=n<T>(1,4) + n<T>(1,3)*z[15];
    z[16]=z[16]*z[12];
    z[17]=static_cast<T>(5)- 7*z[1];
    z[18]=static_cast<T>(1)- n<T>(1,2)*z[1];
    z[18]=z[1]*z[18];
    z[18]= - n<T>(1,2) + z[18];
    z[18]=z[3]*z[18];
    z[17]=n<T>(1,8)*z[17] + z[18];
    z[17]=z[3]*z[17];
    z[17]=n<T>(1,2) + z[17];
    z[17]=z[17]*z[10];
    z[14]=n<T>(7,12) + z[14];
    z[18]=n<T>(1,2)*z[7];
    z[14]=z[14]*z[18];
    z[9]=z[9] + z[14] + z[16] + z[17];
    z[13]=z[13] - n<T>(1,3);
    z[14]= - z[7]*z[13];
    z[12]=z[14] + n<T>(1,3) + z[12];
    z[12]=z[7]*z[12];
    z[14]=npow(z[3],2);
    z[16]=n<T>(5,24)*z[3] + n<T>(1,3) + n<T>(1,8)*z[8];
    z[16]=z[4]*z[16];
    z[12]=z[12] + n<T>(1,6)*z[14] + z[16];
    z[13]= - z[13]*z[18];
    z[16]=n<T>(1,4) - z[3];
    z[13]=n<T>(1,3)*z[16] + z[13];
    z[13]=z[7]*z[13];
    z[13]=n<T>(1,12)*z[4] + z[13];
    z[13]=z[6]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[6]*z[12];
    z[10]= - z[14]*z[10];
    z[13]=z[8] + n<T>(7,3)*z[3];
    z[13]=z[4]*z[13];
    z[16]= - 5*z[8] - n<T>(11,3)*z[3];
    z[16]=z[6]*z[16]*npow(z[7],2);
    z[13]=z[13] + z[16];
    z[13]=n<T>(1,3)*z[14] + n<T>(1,4)*z[13];
    z[13]=z[6]*z[13];
    z[10]=z[10] + z[13];
    z[10]=z[2]*z[10];
    z[13]=n<T>(7,4) + z[15];
    z[11]=z[4]*z[13]*z[11];
    z[10]=n<T>(1,4)*z[10] + z[11] + z[12];
    z[10]=z[2]*z[10];
    z[9]=n<T>(1,2)*z[9] + z[10];

    r += z[9]*z[2];
 
    return r;
}

template double qqb_2lha_r335(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r335(const std::array<dd_real,30>&);
#endif
