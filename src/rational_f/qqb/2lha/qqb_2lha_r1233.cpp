#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1233(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[8];
    z[8]=k[11];
    z[9]=k[10];
    z[10]=n<T>(1,4)*z[1];
    z[11]= - n<T>(25,3) - z[1];
    z[11]=z[11]*z[10];
    z[12]=n<T>(1,2)*z[7];
    z[11]=z[11] + n<T>(1,3) - z[12];
    z[13]=n<T>(1,3)*z[2];
    z[14]=n<T>(1,4)*z[7];
    z[15]=z[14] + z[1];
    z[15]=z[15]*z[13];
    z[11]=n<T>(1,2)*z[11] + z[15];
    z[11]=z[2]*z[11];
    z[15]=n<T>(13,3) + z[1];
    z[15]=z[15]*z[10];
    z[11]=z[11] + z[15] - n<T>(1,3) + z[14];
    z[11]=z[2]*z[11];
    z[14]= - n<T>(5,2) - z[7];
    z[15]=z[13]*z[7];
    z[16]=z[15] - z[7];
    z[17]= - n<T>(5,6) + z[16];
    z[17]=z[2]*z[17];
    z[17]=z[17] + n<T>(5,3) + z[7];
    z[17]=z[2]*z[17];
    z[14]=n<T>(1,3)*z[14] + z[17];
    z[17]=n<T>(1,2)*z[8];
    z[18]=npow(z[2],2);
    z[14]=z[14]*z[18]*z[17];
    z[19]=n<T>(1,4) + z[7];
    z[16]=n<T>(1,12) - z[16];
    z[16]=z[2]*z[16];
    z[16]=z[16] - n<T>(1,6) - z[7];
    z[16]=z[2]*z[16];
    z[16]=n<T>(1,3)*z[19] + z[16];
    z[16]=z[2]*z[16];
    z[14]=z[16] + z[14];
    z[14]=z[14]*z[17];
    z[16]= - static_cast<T>(3)- z[1];
    z[10]=z[16]*z[10];
    z[12]=static_cast<T>(1)- z[12];
    z[10]=n<T>(1,3)*z[12] + z[10];
    z[12]=static_cast<T>(1)- z[13];
    z[12]=z[2]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[16]=npow(z[1],2);
    z[12]=z[2]*z[16]*z[12];
    z[12]=n<T>(1,3)*z[16] + z[12];
    z[16]=n<T>(1,2)*z[3];
    z[12]=z[12]*z[16];
    z[10]=z[12] + z[14] + n<T>(1,2)*z[10] + z[11];
    z[10]=z[3]*z[10];
    z[11]=19*z[7];
    z[12]=n<T>(1,2)*z[1];
    z[14]= - z[12] + static_cast<T>(35)+ z[11];
    z[19]=static_cast<T>(13)- z[11];
    z[19]=n<T>(1,6)*z[19] + z[1];
    z[19]=n<T>(1,2)*z[19] - z[13];
    z[19]=z[2]*z[19];
    z[14]=n<T>(1,6)*z[14] + z[19];
    z[14]=z[2]*z[14];
    z[19]=5*z[1];
    z[11]= - z[19] - static_cast<T>(79)- z[11];
    z[11]=n<T>(1,12)*z[11] + z[14];
    z[14]=n<T>(35,12)*z[7];
    z[20]=z[2]*z[7];
    z[21]= - 17*z[20] + static_cast<T>(79)+ 43*z[7];
    z[21]=z[2]*z[21];
    z[21]=n<T>(1,12)*z[21] - static_cast<T>(9)- z[14];
    z[21]=z[2]*z[21];
    z[22]=n<T>(29,3) + 3*z[7];
    z[21]=n<T>(1,4)*z[22] + z[21];
    z[21]=z[21]*z[17];
    z[22]=3*z[20] - n<T>(41,3) - n<T>(27,4)*z[7];
    z[22]=z[2]*z[22];
    z[22]=z[22] + n<T>(47,3) + n<T>(9,2)*z[7];
    z[21]=z[21] + n<T>(1,2)*z[22];
    z[21]=z[2]*z[21];
    z[21]= - static_cast<T>(1)- n<T>(3,8)*z[7] + z[21];
    z[21]=z[8]*z[21];
    z[10]=z[10] + n<T>(1,2)*z[11] + z[21];
    z[10]=z[10]*z[16];
    z[11]= - n<T>(47,6) - z[19];
    z[11]=z[11]*z[12];
    z[21]=n<T>(1,4)*z[9];
    z[19]= - n<T>(5,2)*z[4] + n<T>(47,12) + z[19];
    z[19]=z[4]*z[19];
    z[11]=z[19] - z[21] - static_cast<T>(1)+ z[11];
    z[19]=n<T>(1,4)*z[4];
    z[11]=z[11]*z[19];
    z[22]= - static_cast<T>(1)- n<T>(11,16)*z[1];
    z[22]=z[1]*z[22];
    z[23]= - n<T>(11,16)*z[4] + static_cast<T>(1)+ n<T>(11,8)*z[1];
    z[23]=z[4]*z[23];
    z[22]=z[23] - n<T>(5,16) + z[22];
    z[23]=npow(z[4],2);
    z[22]=z[22]*z[23];
    z[24]= - static_cast<T>(1)- z[12];
    z[24]=z[1]*z[24];
    z[25]=n<T>(1,2)*z[4];
    z[26]=z[25] - 1;
    z[27]=z[1] - z[26];
    z[27]=z[4]*z[27];
    z[24]=z[27] - n<T>(1,2) + z[24];
    z[27]=n<T>(1,2)*z[5];
    z[24]=z[24]*npow(z[4],3)*z[27];
    z[22]=z[22] + z[24];
    z[22]=z[5]*z[22];
    z[11]=z[11] + z[22];
    z[11]=z[5]*z[11];
    z[22]= - static_cast<T>(3)- n<T>(7,3)*z[7];
    z[22]=z[22]*z[21];
    z[14]=z[14] + 3;
    z[22]=z[22] - z[14];
    z[24]=z[4]*z[9];
    z[22]=z[22]*z[24];
    z[28]= - static_cast<T>(47)- 23*z[7];
    z[28]=z[9]*z[28];
    z[22]=z[22] + n<T>(1,12)*z[28] - z[14];
    z[22]=z[22]*z[19];
    z[15]=static_cast<T>(1)- z[15];
    z[15]=z[2]*z[15];
    z[14]=n<T>(19,4)*z[15] + z[14];
    z[15]=n<T>(1,4)*z[2];
    z[14]=z[14]*z[15];
    z[28]= - n<T>(19,8) - z[7];
    z[14]=z[14] + n<T>(1,3)*z[28] + z[22];
    z[14]=z[8]*z[14];
    z[22]= - n<T>(13,24)*z[9] - n<T>(9,8) + n<T>(1,3)*z[7];
    z[22]=z[22]*z[24];
    z[28]= - n<T>(7,2)*z[9] - static_cast<T>(15)- n<T>(19,6)*z[7];
    z[20]= - static_cast<T>(13)+ n<T>(19,2)*z[20];
    z[20]=z[2]*z[20];
    z[14]=z[14] + n<T>(1,24)*z[20] + n<T>(1,8)*z[28] + z[22];
    z[14]=z[8]*z[14];
    z[20]=n<T>(1,3)*z[9];
    z[22]=3*z[1];
    z[28]= - n<T>(23,3) - z[22];
    z[28]=z[1]*z[28];
    z[28]= - z[20] - n<T>(13,3) + z[28];
    z[29]= - n<T>(3,2)*z[4] + n<T>(23,6) + z[22];
    z[29]=z[4]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[29];
    z[29]= - n<T>(5,3) - z[2];
    z[29]=z[2]*z[29];
    z[10]=z[10] + z[14] + n<T>(1,16)*z[29] + n<T>(1,8)*z[28] + z[11];
    z[11]=z[2]*z[9];
    z[14]= - static_cast<T>(1)- n<T>(17,48)*z[9];
    z[14]=z[14]*z[11];
    z[14]=n<T>(3,8)*z[9] + z[14];
    z[14]=z[2]*z[14];
    z[28]=5*z[8];
    z[29]=z[5] - 1;
    z[30]= - z[2] + z[29];
    z[30]=z[30]*z[28];
    z[30]= - n<T>(11,2)*z[29] + z[30];
    z[30]=z[8]*z[30];
    z[31]= - z[9]*z[13];
    z[31]=z[21] + z[31];
    z[32]=z[18]*z[3];
    z[31]=z[31]*z[32];
    z[33]= - z[9]*z[29];
    z[14]=z[31] + n<T>(1,24)*z[30] + n<T>(1,16)*z[33] + z[14];
    z[14]=z[3]*z[14];
    z[30]=3*z[5];
    z[31]=z[29]*z[30];
    z[33]=static_cast<T>(7)- z[30];
    z[33]=z[33]*z[30];
    z[33]= - n<T>(41,3) + z[33];
    z[33]=z[33]*z[17];
    z[31]=z[31] + z[33];
    z[31]=z[8]*z[31];
    z[33]= - static_cast<T>(5)- n<T>(13,3)*z[9];
    z[11]=z[33]*z[11];
    z[33]=n<T>(1,2)*z[9];
    z[11]=z[31] + z[33] + z[11];
    z[11]=n<T>(1,4)*z[11] + z[14];
    z[11]=z[3]*z[11];
    z[14]=npow(z[8],2);
    z[31]=n<T>(5,2) - z[5];
    z[31]=z[5]*z[31];
    z[31]= - n<T>(3,2) + z[31];
    z[31]=z[31]*z[14]*z[27];
    z[34]=npow(z[9],2);
    z[35]=z[34]*z[18];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[5]*z[27];
    z[27]=n<T>(1,2) + z[27];
    z[14]=z[27]*z[14];
    z[14]= - n<T>(5,2)*z[35] + z[14];
    z[27]=z[3]*z[34]*npow(z[2],3);
    z[14]=n<T>(1,2)*z[14] - n<T>(1,3)*z[27];
    z[14]=z[14]*z[16];
    z[27]= - z[2]*z[34];
    z[14]=z[14] + z[27] + z[31];
    z[14]=z[3]*z[14];
    z[14]= - n<T>(13,24)*z[34] + z[14];
    z[27]=n<T>(1,2)*z[6];
    z[14]=z[14]*z[27];
    z[31]=n<T>(5,8) - z[5];
    z[31]=z[5]*z[31];
    z[34]= - n<T>(3,2) + z[5];
    z[34]=z[8]*z[34];
    z[31]=n<T>(3,4)*z[34] + n<T>(1,4) + z[31];
    z[31]=z[5]*z[31];
    z[21]= - z[21] + z[31];
    z[21]=z[8]*z[21];
    z[31]= - static_cast<T>(11)- n<T>(53,2)*z[9];
    z[31]=z[9]*z[31];
    z[11]=z[14] + z[11] + n<T>(1,24)*z[31] + z[21];
    z[11]=z[6]*z[11];
    z[14]=n<T>(5,2)*z[9];
    z[21]=n<T>(3,2)*z[9];
    z[31]= - n<T>(29,3) - z[21];
    z[31]=z[5]*z[31];
    z[31]=z[31] + static_cast<T>(1)+ z[14];
    z[34]= - n<T>(35,2) - n<T>(17,3)*z[9];
    z[33]=z[34]*z[33];
    z[33]= - static_cast<T>(3)+ z[33];
    z[33]=z[2]*z[33];
    z[31]=n<T>(1,2)*z[31] + z[33];
    z[28]=z[18]*z[28];
    z[28]= - n<T>(29,2)*z[2] + z[28];
    z[33]=n<T>(1,3)*z[8];
    z[28]=z[28]*z[33];
    z[34]= - static_cast<T>(11)- n<T>(35,3)*z[9];
    z[34]=z[2]*z[34];
    z[34]=n<T>(1,2)*z[34] + static_cast<T>(3)+ n<T>(19,6)*z[9];
    z[34]=z[2]*z[34];
    z[28]=z[28] + n<T>(11,6) + z[34];
    z[34]=z[2] - 1;
    z[32]= - z[34]*z[32];
    z[28]=n<T>(1,2)*z[28] + z[32];
    z[28]=z[28]*z[16];
    z[29]= - 13*z[29] + n<T>(155,8)*z[2];
    z[29]=z[8]*z[29];
    z[29]=z[29] - n<T>(41,2) + 17*z[5];
    z[29]=z[29]*z[33];
    z[28]=z[28] + n<T>(1,2)*z[31] + z[29];
    z[16]=z[28]*z[16];
    z[28]= - static_cast<T>(1)- z[21];
    z[28]=z[28]*z[24];
    z[29]=n<T>(7,2) - z[4];
    z[29]=z[29]*z[30];
    z[30]= - n<T>(35,6) - 3*z[24];
    z[29]=n<T>(1,2)*z[30] + z[29];
    z[29]=z[8]*z[29];
    z[28]=z[29] - z[14] + z[28];
    z[29]= - n<T>(3,2) + z[4];
    z[30]= - n<T>(15,8) + z[4];
    z[30]=z[5]*z[30];
    z[29]=n<T>(1,4)*z[29] + z[30];
    z[29]=z[5]*z[29];
    z[28]=z[29] + n<T>(1,4)*z[28];
    z[28]=z[8]*z[28];
    z[29]= - static_cast<T>(41)- 35*z[9];
    z[20]=z[29]*z[20];
    z[20]= - n<T>(3,2) + z[20];
    z[26]= - z[4]*z[26];
    z[26]=static_cast<T>(5)+ z[26];
    z[19]=static_cast<T>(1)- z[19];
    z[19]=z[4]*z[19];
    z[19]= - n<T>(3,2) + z[19];
    z[19]=z[5]*z[4]*z[19];
    z[19]=n<T>(1,8)*z[26] + z[19];
    z[19]=z[5]*z[19];
    z[19]=n<T>(11,12) + z[19];
    z[19]=z[5]*z[19];
    z[11]=z[11] + z[16] + z[28] + n<T>(1,8)*z[20] + z[19];
    z[11]=z[11]*z[27];
    z[16]= - n<T>(1,2) - z[1];
    z[16]=n<T>(13,3)*z[16] - z[21];
    z[19]=z[4] - z[1];
    z[20]=n<T>(67,12) - z[19];
    z[20]=z[4]*z[20];
    z[16]=n<T>(1,2)*z[16] + z[20];
    z[20]=static_cast<T>(3)- z[19];
    z[20]=z[20]*z[25];
    z[20]=z[20] - z[1] - 1;
    z[20]=z[5]*z[20]*z[23];
    z[19]=n<T>(5,2) - z[19];
    z[19]=z[4]*z[19];
    z[19]=z[19] - n<T>(3,2) - z[1];
    z[19]=z[4]*z[19];
    z[19]=n<T>(3,4)*z[19] + z[20];
    z[19]=z[5]*z[19];
    z[16]=n<T>(1,4)*z[16] + z[19];
    z[16]=z[5]*z[16];
    z[14]= - static_cast<T>(3)- z[14];
    z[14]=z[14]*z[24];
    z[19]= - n<T>(23,3) - 13*z[9];
    z[14]=n<T>(1,4)*z[19] + z[14];
    z[19]= - static_cast<T>(3)- z[9];
    z[19]=z[19]*z[24];
    z[19]=z[19] - static_cast<T>(3)- 7*z[9];
    z[19]=z[19]*z[25];
    z[19]= - n<T>(13,4)*z[2] - static_cast<T>(3)+ z[19];
    z[19]=z[19]*z[17];
    z[20]= - static_cast<T>(1)+ z[4];
    z[20]=z[5]*z[20];
    z[14]=z[19] + n<T>(1,2)*z[14] + z[20];
    z[14]=z[14]*z[17];
    z[17]= - n<T>(29,8) - n<T>(5,3)*z[9];
    z[17]=z[9]*z[17];
    z[17]=n<T>(25,24) + z[17];
    z[14]=z[14] + z[15] + n<T>(1,2)*z[17] + z[16];
    z[15]= - static_cast<T>(5)- z[22];
    z[15]=n<T>(1,4)*z[15] + z[13];
    z[15]=z[2]*z[15];
    z[16]=n<T>(5,6) + z[22];
    z[15]=n<T>(1,4)*z[16] + z[15];
    z[15]=z[2]*z[15];
    z[16]=z[8]*z[2];
    z[17]=z[34]*z[16];
    z[13]= - z[1]*z[13];
    z[12]=z[12] + z[13];
    z[12]=z[12]*z[18];
    z[12]= - n<T>(1,6)*z[1] + z[12];
    z[12]=z[3]*z[12];
    z[12]=z[12] + n<T>(7,8)*z[17] + n<T>(17,24) + z[15];
    z[12]=z[3]*z[12];
    z[13]= - static_cast<T>(59)- 13*z[1];
    z[13]=n<T>(1,3)*z[13] + z[21];
    z[15]=n<T>(3,2)*z[2] - 3*z[9] - n<T>(71,12) - z[1];
    z[15]=z[2]*z[15];
    z[13]=n<T>(1,2)*z[13] + z[15];
    z[15]=static_cast<T>(5)- 17*z[2];
    z[15]=z[15]*z[16];
    z[16]= - static_cast<T>(1)+ 8*z[2];
    z[15]=n<T>(1,3)*z[16] + n<T>(3,32)*z[15];
    z[15]=z[8]*z[15];
    z[12]=n<T>(1,4)*z[12] + n<T>(1,8)*z[13] + z[15];
    z[12]=z[3]*z[12];
    z[11]=z[11] + n<T>(1,2)*z[14] + z[12];
    z[11]=z[6]*z[11];

    r += n<T>(1,2)*z[10] + z[11];
 
    return r;
}

template double qqb_2lha_r1233(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1233(const std::array<dd_real,30>&);
#endif
