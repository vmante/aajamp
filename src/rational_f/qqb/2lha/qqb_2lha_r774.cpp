#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r774(const std::array<T,30>& k) {
  T z[70];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[29];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[28];
    z[9]=k[6];
    z[10]=k[12];
    z[11]=k[8];
    z[12]=k[9];
    z[13]=k[10];
    z[14]=k[5];
    z[15]=k[21];
    z[16]=k[11];
    z[17]=k[15];
    z[18]=n<T>(1,2)*z[7];
    z[19]=z[18] + 1;
    z[19]=z[19]*z[7];
    z[19]=z[19] + n<T>(1,2);
    z[19]=z[19]*z[12];
    z[20]=z[7] + 1;
    z[19]=z[19] - z[20];
    z[19]=z[19]*z[12];
    z[19]=z[19] + n<T>(1,2);
    z[21]=npow(z[3],2);
    z[22]=z[9]*z[19]*z[21];
    z[23]=z[7]*z[4];
    z[24]=z[23] + z[4];
    z[25]=z[24]*z[12];
    z[22]= - z[22] - z[23] + n<T>(1,4)*z[25];
    z[20]=z[20]*z[12];
    z[26]=z[20] - 1;
    z[27]=n<T>(1,2)*z[12];
    z[28]=z[26]*z[27];
    z[19]=z[3]*z[19];
    z[19]=z[28] + z[19];
    z[19]=z[3]*z[19];
    z[29]=z[7] + z[27];
    z[29]=z[29]*z[27];
    z[19]=z[29] + z[19];
    z[19]=z[3]*z[19];
    z[29]=z[6] + 1;
    z[30]=z[18]*z[12]*z[29];
    z[31]=n<T>(1,2)*z[4];
    z[32]=z[31] + z[23];
    z[33]=z[7]*z[32];
    z[34]=n<T>(1,8)*z[4];
    z[30]= - z[34] + z[33] + z[30];
    z[30]=z[6]*z[30];
    z[33]=n<T>(1,2)*z[6];
    z[29]= - z[1]*z[33]*z[23]*z[29];
    z[35]=z[5]*z[25];
    z[19]=n<T>(1,8)*z[35] + z[29] + z[30] + z[19] + n<T>(1,2)*z[22];
    z[19]=z[9]*z[19];
    z[22]=z[3] + 1;
    z[29]=z[22]*z[12];
    z[30]=n<T>(1,2)*z[3];
    z[35]=z[29]*z[30];
    z[36]=z[12] + n<T>(1,2)*z[24];
    z[36]=z[36]*z[12];
    z[37]=npow(z[12],2);
    z[38]=z[37]*z[7];
    z[39]=z[8]*z[38];
    z[35]=z[39] + z[35] + z[31] - z[36];
    z[35]=z[8]*z[35];
    z[39]=z[30]*z[23];
    z[40]= - z[39] - z[32];
    z[40]=z[3]*z[40];
    z[41]=7*z[23];
    z[42]= - z[41] - z[25];
    z[35]=z[35] + n<T>(1,2)*z[42] + z[40];
    z[40]=z[23] - z[4];
    z[42]= - z[3]*z[23];
    z[42]=z[42] - z[40];
    z[43]=n<T>(1,8)*z[3];
    z[42]=z[42]*z[43];
    z[44]=z[8]*z[40];
    z[42]=n<T>(1,8)*z[44] - z[23] + z[42];
    z[42]=z[1]*z[42];
    z[44]=z[37]*z[14];
    z[45]=3*z[37];
    z[44]=z[44] + z[45];
    z[46]= - z[8]*z[12];
    z[46]=z[46] + z[44];
    z[46]=z[14]*z[46];
    z[47]= - z[5] + n<T>(1,8);
    z[25]=z[25]*z[47];
    z[25]= - z[24] + z[25];
    z[25]=z[5]*z[25];
    z[25]=z[25] + n<T>(3,8)*z[46] + n<T>(1,4)*z[35] + z[42];
    z[25]=z[10]*z[25];
    z[35]=static_cast<T>(3)- z[21];
    z[42]=z[12] - 1;
    z[46]=z[42]*z[12];
    z[35]=z[30]*z[46]*z[35];
    z[47]=npow(z[3],3);
    z[42]=z[2]*z[27]*z[47]*z[42];
    z[47]=z[47]*z[46];
    z[47]=z[47] - z[42];
    z[47]=z[2]*z[47];
    z[35]=z[35] + z[47];
    z[35]=z[11]*z[35];
    z[47]=z[21]*z[46];
    z[42]=z[47] + z[42];
    z[42]=z[2]*z[42];
    z[47]=n<T>(1,4)*z[3];
    z[48]=z[46]*z[47];
    z[42]=z[48] + z[42];
    z[42]=z[13]*z[42];
    z[48]=z[10] - z[11];
    z[46]=z[17]*z[46]*z[48];
    z[19]=z[46] + z[19] + z[25] + z[35] + z[42];
    z[25]=z[31] - z[23];
    z[25]=z[25]*z[7];
    z[35]=n<T>(5,8)*z[4];
    z[42]=z[3]*z[4];
    z[46]= - n<T>(1,8)*z[42] - z[35] - z[25];
    z[48]=z[23] + n<T>(5,4)*z[4];
    z[48]=z[48]*z[7];
    z[49]=n<T>(1,4)*z[4];
    z[48]=z[48] + z[49];
    z[50]=npow(z[7],2);
    z[51]= - z[48]*z[50]*z[33];
    z[52]=z[35] - z[23];
    z[52]=z[7]*z[52];
    z[53]=n<T>(5,16)*z[4];
    z[52]=z[53] + z[52];
    z[52]=z[7]*z[52];
    z[51]=z[52] + z[51];
    z[51]=z[6]*z[51];
    z[52]=n<T>(3,4)*z[4];
    z[54]=z[52] - z[23];
    z[55]=z[54]*z[7];
    z[56]=z[49] + z[55];
    z[56]=z[7]*z[56];
    z[57]=z[49] + z[23];
    z[58]=npow(z[7],3);
    z[59]=z[6]*z[57]*z[58];
    z[56]=z[56] + z[59];
    z[56]=z[8]*z[56];
    z[46]=z[56] + n<T>(1,2)*z[46] + z[51];
    z[46]=z[8]*z[46];
    z[51]=z[31]*z[3];
    z[51]=z[51] + z[4];
    z[47]= - z[51]*z[47];
    z[47]=z[47] - z[35] - z[23];
    z[56]=n<T>(9,4)*z[4];
    z[59]=z[56] + z[23];
    z[59]=z[59]*z[7];
    z[60]=z[31] + z[59];
    z[60]=z[6]*z[60]*z[18];
    z[25]=z[60] - z[53] - z[25];
    z[25]=z[6]*z[25];
    z[25]=z[46] + n<T>(1,2)*z[47] + z[25];
    z[25]=z[8]*z[25];
    z[46]=3*z[23];
    z[47]=z[46] + z[4];
    z[60]=n<T>(1,2)*z[47];
    z[61]=n<T>(1,4)*z[23];
    z[62]=z[4] + z[61];
    z[62]=z[3]*z[62];
    z[62]=z[60] + z[62];
    z[62]=z[3]*z[62];
    z[63]=z[31]*z[2];
    z[21]= - z[21]*z[63];
    z[21]=z[62] + z[21];
    z[21]=z[2]*z[21];
    z[62]=n<T>(1,2)*z[23];
    z[64]=z[62] + z[4];
    z[65]= - z[64]*z[30];
    z[65]=z[65] - z[47];
    z[65]=z[3]*z[65];
    z[57]=z[57]*z[8];
    z[66]= - z[7]*z[57];
    z[51]=n<T>(1,8)*z[51] + z[66];
    z[51]=z[8]*z[51];
    z[66]=z[4] + z[42];
    z[66]=z[3]*z[66];
    z[51]=n<T>(1,16)*z[66] + z[51];
    z[51]=z[8]*z[51];
    z[21]=z[21] + z[51] + z[31] + z[65];
    z[21]=z[2]*z[21];
    z[51]=z[32]*z[6];
    z[65]=n<T>(3,2)*z[4];
    z[66]= - z[65] - z[51];
    z[66]=z[6]*z[66];
    z[67]= - z[2] + 1;
    z[67]=z[42]*z[67];
    z[67]=z[4] + z[67];
    z[67]=z[1]*z[67];
    z[66]=z[66] + z[67];
    z[67]=z[23] + n<T>(9,16)*z[4];
    z[68]=n<T>(1,16)*z[42] + z[67];
    z[68]=z[3]*z[68];
    z[21]=z[21] + z[25] + n<T>(1,2)*z[54] + z[68] + n<T>(1,4)*z[66];
    z[21]=z[1]*z[21];
    z[25]=3*z[12];
    z[66]=z[25] - z[7] - static_cast<T>(5)+ z[49];
    z[66]=z[12]*z[66];
    z[66]=n<T>(1,2) + z[66];
    z[68]=static_cast<T>(7)+ 3*z[7];
    z[20]=n<T>(1,2)*z[68] - 3*z[20];
    z[20]=z[12]*z[20];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[3]*z[20];
    z[20]=n<T>(1,2)*z[66] + z[20];
    z[20]=z[3]*z[20];
    z[66]=z[24]*z[18];
    z[66]=z[66] - static_cast<T>(3)+ z[49];
    z[66]=z[7]*z[66];
    z[68]=n<T>(3,8)*z[4];
    z[66]=z[66] - static_cast<T>(1)+ z[68];
    z[66]=z[12]*z[66];
    z[47]=z[47]*z[7];
    z[20]=z[20] + z[66] - z[49] - z[47];
    z[20]=z[20]*z[30];
    z[66]= - 21*z[4] - 11*z[23];
    z[66]=z[7]*z[66];
    z[66]= - n<T>(21,2)*z[4] + z[66];
    z[66]=z[66]*z[18];
    z[66]= - z[4] + z[66];
    z[66]=z[66]*z[27];
    z[69]=n<T>(19,8)*z[4] + z[46];
    z[69]=z[7]*z[69];
    z[20]=z[21] + z[20] + z[66] + n<T>(11,16)*z[4] + z[69];
    z[21]= - z[50]*z[27];
    z[21]=z[7] + z[21];
    z[21]=z[21]*z[51];
    z[18]=z[40]*z[18];
    z[40]=z[50]*z[12];
    z[51]= - z[49]*z[40];
    z[18]=z[21] + z[51] + z[4] + z[18];
    z[18]=z[6]*z[18];
    z[21]=z[32]*z[33];
    z[21]=z[21] - z[54];
    z[51]=z[1]*z[31];
    z[51]=z[51] - z[21];
    z[51]=z[6]*z[51];
    z[51]= - z[4] + z[51];
    z[51]=z[1]*z[51];
    z[46]=7*z[4] + z[46];
    z[46]=z[7]*z[46];
    z[46]=n<T>(25,4)*z[4] + z[46];
    z[46]=z[7]*z[46];
    z[46]=n<T>(7,4)*z[4] + z[46];
    z[46]=z[12]*z[46];
    z[18]=z[51] - z[63] + z[18] + z[46] - z[49] + z[23];
    z[46]=z[68] + z[23];
    z[46]=z[46]*z[7];
    z[51]=z[59] + z[65];
    z[51]=z[51]*z[7];
    z[51]=z[51] + z[49];
    z[59]=z[51]*z[33];
    z[66]=n<T>(1,16)*z[4];
    z[59]=z[59] + z[66] + z[46];
    z[59]=z[6]*z[59];
    z[59]=z[66] + z[59];
    z[59]=z[14]*z[59];
    z[68]= - z[1]*z[4];
    z[69]=z[5]*z[31];
    z[21]=z[69] + z[68] + z[21];
    z[21]=z[6]*z[21];
    z[21]=z[31] + z[21];
    z[68]=n<T>(1,2)*z[5];
    z[21]=z[21]*z[68];
    z[18]=z[21] + n<T>(1,2)*z[18] + z[59];
    z[18]=z[18]*z[68];
    z[21]=z[5] + 1;
    z[21]=z[23]*z[21];
    z[59]= - z[23] + z[12];
    z[59]=z[6]*z[59];
    z[68]=z[12]*z[26]*z[9];
    z[21]=z[68] + z[59] - z[45] + z[21];
    z[28]= - z[15]*z[28];
    z[21]=z[28] + n<T>(1,2)*z[21];
    z[21]=z[9]*z[21];
    z[28]= - z[39] - z[64];
    z[28]=z[6]*z[28];
    z[28]=z[28] + z[39] + z[24];
    z[28]=z[3]*z[28];
    z[59]=z[39] - z[4] + z[62];
    z[59]=z[3]*z[59];
    z[59]= - z[4] + z[59];
    z[59]=z[2]*z[59];
    z[62]=z[2] + 1;
    z[62]=z[4]*z[62];
    z[22]=z[22]*z[3];
    z[68]=z[22] - z[4] + z[12];
    z[68]=z[6]*z[68];
    z[62]=z[68] + z[62];
    z[68]=n<T>(1,2)*z[14];
    z[62]=z[62]*z[68];
    z[69]=z[6] + z[2];
    z[68]=z[68]*z[69];
    z[68]= - static_cast<T>(1)+ z[68];
    z[68]=z[5]*z[4]*z[68];
    z[21]=z[68] + z[62] + z[59] + z[24] + z[21] + z[28];
    z[21]=z[15]*z[21];
    z[28]=static_cast<T>(1)- z[31];
    z[49]=static_cast<T>(1)- z[49];
    z[49]=z[7]*z[49];
    z[28]= - z[27] + n<T>(1,2)*z[28] + z[49];
    z[28]=z[12]*z[28];
    z[37]=z[3]*z[50]*z[37];
    z[49]=z[12]*z[7];
    z[49]=z[61] + z[49];
    z[49]=z[12]*z[49];
    z[49]=z[49] - z[37];
    z[49]=z[3]*z[49];
    z[28]=z[49] - static_cast<T>(1)+ z[28];
    z[28]=z[28]*z[43];
    z[32]=z[32]*z[40];
    z[37]= - z[38] + z[37];
    z[37]=z[37]*z[43];
    z[32]=z[37] + n<T>(1,4)*z[32] - z[51];
    z[32]=z[32]*z[33];
    z[37]=z[4] + n<T>(5,2)*z[23];
    z[37]=z[12]*z[37];
    z[37]=n<T>(1,8)*z[37] + z[53] - z[23];
    z[37]=z[7]*z[37];
    z[28]=z[32] + z[28] + z[66] + z[37];
    z[28]=z[6]*z[28];
    z[32]=z[7]*z[64];
    z[37]= - n<T>(5,2) + z[25];
    z[37]=z[12]*z[37];
    z[37]=n<T>(1,2) + z[37];
    z[37]=z[3]*z[37];
    z[32]=z[37] + z[12] + z[34] + z[32];
    z[32]=z[3]*z[32];
    z[37]= - z[4]*z[50];
    z[37]= - z[31] + z[37];
    z[37]=z[37]*z[27];
    z[40]=n<T>(13,2)*z[4] + z[41];
    z[40]=z[7]*z[40];
    z[32]=z[32] + z[37] + n<T>(11,8)*z[4] + z[40];
    z[32]=z[32]*z[30];
    z[37]=z[63] - z[39] - z[4] - n<T>(11,4)*z[23];
    z[37]=z[3]*z[37];
    z[37]= - z[60] + z[37];
    z[37]=z[2]*z[37];
    z[24]=z[7]*z[24];
    z[24]=z[52] + 5*z[24];
    z[24]=z[24]*z[27];
    z[24]=z[37] + z[32] + z[24] + z[35] + z[47];
    z[27]=z[57]*z[50];
    z[32]= - z[27] - z[66] + z[46];
    z[32]=z[8]*z[32];
    z[35]= - 3*z[4] - z[42];
    z[32]=n<T>(1,16)*z[35] + z[32];
    z[32]=z[8]*z[32];
    z[24]=z[32] + n<T>(1,2)*z[24];
    z[24]=z[2]*z[24];
    z[32]= - n<T>(7,16)*z[4] + z[46];
    z[32]=z[7]*z[32];
    z[35]=z[7]*z[51];
    z[37]=n<T>(1,8)*z[38];
    z[35]=z[35] + z[37];
    z[35]=z[35]*z[33];
    z[29]=z[3]*z[29];
    z[29]=z[35] - n<T>(1,32)*z[29] + n<T>(1,16)*z[36] - n<T>(7,32)*z[4] + z[32];
    z[29]=z[6]*z[29];
    z[32]= - z[54]*z[50];
    z[35]=z[56] + z[55];
    z[35]=z[7]*z[35];
    z[31]=z[31] + z[35];
    z[31]=z[7]*z[31];
    z[31]=z[31] - z[37];
    z[31]=z[6]*z[31];
    z[31]=z[31] - z[34] + z[32];
    z[27]=n<T>(1,2)*z[31] - z[27];
    z[27]=z[8]*z[27];
    z[31]=z[34] - z[23];
    z[32]=z[7]*z[31];
    z[27]=z[27] + z[29] + z[34] + z[32];
    z[27]=z[8]*z[27];
    z[29]=n<T>(3,16)*z[4];
    z[32]= - z[4] - n<T>(3,2)*z[23];
    z[32]=z[7]*z[32];
    z[32]= - z[29] + z[32];
    z[32]=z[7]*z[32];
    z[32]=z[66] + z[32];
    z[32]=z[7]*z[32];
    z[30]=static_cast<T>(1)+ z[30];
    z[35]=n<T>(5,4) + z[7];
    z[35]=z[7]*z[35];
    z[35]=n<T>(1,4) + z[35];
    z[35]=z[6]*z[35];
    z[30]=n<T>(1,4)*z[30] + z[35];
    z[30]=z[30]*z[33];
    z[35]=z[48]*z[58];
    z[36]=z[7] + n<T>(1,4);
    z[37]=z[36]*z[6];
    z[38]= - z[7]*z[37];
    z[35]=z[35] + z[38];
    z[35]=z[8]*z[35];
    z[30]=z[35] + z[32] + z[30];
    z[30]=z[8]*z[30];
    z[31]=z[31]*z[50];
    z[22]=z[25] + z[22];
    z[22]=n<T>(1,8)*z[22] + z[37];
    z[22]=z[6]*z[22];
    z[22]=z[31] + z[22];
    z[22]=n<T>(1,2)*z[22] + z[30];
    z[22]=z[8]*z[22];
    z[25]=z[65] + z[41];
    z[30]=z[12]*z[36];
    z[30]=z[30] - z[51];
    z[30]=z[6]*z[30];
    z[25]=n<T>(1,4)*z[25] + z[30];
    z[25]=z[25]*z[33];
    z[29]=z[29] + z[23];
    z[29]=z[7]*z[29];
    z[22]=z[22] + z[25] - z[66] + z[29];
    z[23]=z[53] + z[23];
    z[23]=z[7]*z[23];
    z[23]= - n<T>(1,32)*z[4] + z[23];
    z[23]=z[7]*z[23];
    z[25]=z[58]*z[57];
    z[23]=z[23] - n<T>(1,2)*z[25];
    z[23]=z[8]*z[23];
    z[25]= - z[7]*z[67];
    z[25]=z[34] + z[25];
    z[23]=n<T>(1,2)*z[25] + z[23];
    z[23]=z[8]*z[23];
    z[23]=n<T>(3,32)*z[4] + z[23];
    z[23]=z[2]*z[23];
    z[22]=n<T>(1,2)*z[22] + z[23];
    z[22]=z[14]*z[22];
    z[23]= - z[14]*z[44];
    z[25]=z[26]*z[15];
    z[25]=z[25] + z[14];
    z[25]=z[12]*z[25];
    z[25]=z[45] + z[25];
    z[25]=z[15]*z[25];
    z[23]=z[23] + z[25];
    z[23]=z[16]*z[23];

    r += z[18] + n<T>(1,4)*z[19] + n<T>(1,2)*z[20] + n<T>(1,16)*z[21] + z[22] + n<T>(1,32)*z[23] + z[24] + z[27] + z[28];
 
    return r;
}

template double qqb_2lha_r774(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r774(const std::array<dd_real,30>&);
#endif
