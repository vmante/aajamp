#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r451(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[4];
    z[4]=k[13];
    z[5]=k[10];
    z[6]= - n<T>(3,2) - z[5];
    z[6]=z[5]*z[6];
    z[7]=npow(z[5],2);
    z[8]=static_cast<T>(1)+ 3*z[7];
    z[8]=z[2]*z[8];
    z[6]=n<T>(1,4)*z[8] - n<T>(1,2) + z[6];
    z[6]=z[2]*z[6];
    z[8]=n<T>(1,2)*z[2];
    z[9]=static_cast<T>(1)+ n<T>(1,2)*z[5];
    z[10]=z[9]*z[5];
    z[11]= - n<T>(1,2) - z[10];
    z[11]=3*z[11] + z[8];
    z[12]=npow(z[2],2);
    z[11]=z[4]*z[11]*z[12];
    z[6]=z[6] + z[11];
    z[6]=z[4]*z[6];
    z[11]= - static_cast<T>(1)+ 3*z[5];
    z[11]=z[11]*z[8];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[5]*z[11];
    z[6]=n<T>(1,2)*z[11] + z[6];
    z[6]=z[4]*z[6];
    z[11]=3*z[2];
    z[11]=z[7]*z[11];
    z[13]=z[5] + 1;
    z[14]=z[4]*z[13]*z[2];
    z[14]= - 3*z[14] + static_cast<T>(1)- z[5];
    z[14]=z[5]*z[14];
    z[11]=z[11] + z[14];
    z[11]=z[4]*z[11];
    z[11]=n<T>(3,2)*z[7] + z[11];
    z[11]=z[4]*z[11];
    z[14]=n<T>(3,2) - z[4];
    z[14]=npow(z[4],2)*z[14];
    z[14]=n<T>(1,2) + z[14];
    z[7]=z[3]*z[7]*z[14];
    z[7]=z[7] + z[10] + z[11];
    z[10]=n<T>(1,2)*z[3];
    z[7]=z[7]*z[10];
    z[6]=z[7] + n<T>(1,4)*z[13] + z[6];
    z[6]=z[6]*z[10];
    z[7]= - n<T>(5,2) - z[1];
    z[11]= - static_cast<T>(1)- n<T>(1,4)*z[5];
    z[11]=z[5]*z[11];
    z[7]=z[8] + n<T>(1,2)*z[7] + z[11];
    z[7]=z[7]*z[12];
    z[11]= - static_cast<T>(3)- z[5];
    z[11]=z[5]*z[11];
    z[11]=z[2] + z[11] - static_cast<T>(3)- z[1];
    z[11]=z[4]*z[11]*npow(z[2],3);
    z[7]=z[7] + n<T>(1,4)*z[11];
    z[7]=z[4]*z[7];
    z[9]=z[8] - n<T>(1,2)*z[1] - z[9];
    z[8]=z[9]*z[8];
    z[7]=z[8] + z[7];
    z[7]=z[4]*z[7];
    z[6]=z[7] + z[6];

    r += z[10]*z[6];
 
    return r;
}

template double qqb_2lha_r451(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r451(const std::array<dd_real,30>&);
#endif
