#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2377(const std::array<T,30>& k) {
  T z[28];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[4];
    z[4]=k[7];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[8];
    z[8]=k[9];
    z[9]=n<T>(1,2)*z[1];
    z[10]=z[2] - 1;
    z[11]=z[9]*z[10];
    z[12]=z[10]*z[8];
    z[13]=3*z[10] - z[12];
    z[14]=n<T>(1,2)*z[8];
    z[13]=z[13]*z[14];
    z[13]=z[13] + z[11] - z[10];
    z[13]=z[4]*z[13];
    z[15]=n<T>(1,2) + z[2];
    z[15]=z[15]*z[9];
    z[16]= - static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[17]=z[8]*z[16];
    z[17]=n<T>(5,2) + z[17];
    z[17]=z[17]*z[14];
    z[13]=n<T>(3,2)*z[13] + z[17] - z[2] + z[15];
    z[13]=z[4]*z[13];
    z[15]=z[14]*z[6];
    z[17]=n<T>(1,2)*z[6];
    z[18]=z[15] - static_cast<T>(1)- z[17];
    z[18]=z[18]*z[14];
    z[19]=n<T>(1,4)*z[6];
    z[20]= - z[19] + n<T>(3,4);
    z[20]=z[1]*z[20];
    z[18]=z[18] - z[2] + z[20];
    z[13]=n<T>(1,2)*z[18] + z[13];
    z[13]=z[4]*z[13];
    z[18]=z[1] - 1;
    z[20]=z[18]*z[17];
    z[21]=3*z[1];
    z[20]= - z[21] + z[20];
    z[20]=z[6]*z[20];
    z[22]=npow(z[6],2);
    z[23]= - z[15] - static_cast<T>(1)+ n<T>(1,2)*z[22];
    z[23]=z[8]*z[23];
    z[20]=z[23] + n<T>(1,2) + z[20];
    z[13]=n<T>(1,4)*z[20] + z[13];
    z[13]=z[4]*z[13];
    z[20]= - static_cast<T>(1)+ z[8];
    z[20]=z[22]*z[20];
    z[23]=npow(z[8],2);
    z[24]=z[23] - 1;
    z[25]=z[17] - 1;
    z[24]=z[25]*z[24];
    z[25]= - static_cast<T>(1)+ z[14];
    z[25]=z[8]*z[25];
    z[25]=n<T>(1,2) + z[25];
    z[25]=z[4]*z[25];
    z[24]=3*z[25] + z[24];
    z[24]=z[4]*z[24];
    z[23]=z[23]*z[6];
    z[25]= - z[23] + static_cast<T>(1)- z[6];
    z[24]=n<T>(1,2)*z[25] + z[24];
    z[24]=z[4]*z[24];
    z[20]=n<T>(1,2)*z[20] + z[24];
    z[20]=z[4]*z[20];
    z[24]=z[22]*z[14];
    z[25]=z[8]*z[6];
    z[26]= - z[6] + z[25];
    z[26]=z[4]*z[8]*z[26];
    z[23]= - z[23] + z[26];
    z[23]=z[23]*npow(z[4],2);
    z[23]=z[24] + z[23];
    z[23]=z[4]*z[23];
    z[23]= - z[24] + z[23];
    z[24]=n<T>(1,2)*z[3];
    z[23]=z[23]*z[24];
    z[26]=n<T>(1,2)*z[5];
    z[27]= - static_cast<T>(1)+ z[26];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(1,2) + z[27];
    z[27]=z[27]*z[25];
    z[20]=z[23] + z[20] + n<T>(3,2)*z[22] + z[27];
    z[20]=z[20]*z[24];
    z[22]=z[1] - z[5];
    z[22]=static_cast<T>(1)- n<T>(9,4)*z[22];
    z[22]=z[22]*z[17];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[6]*z[22];
    z[23]=3*z[5];
    z[24]=static_cast<T>(1)+ z[23];
    z[24]=z[5]*z[24];
    z[24]= - static_cast<T>(5)+ z[24];
    z[17]=z[24]*z[17];
    z[24]=z[5] - 1;
    z[17]=z[17] + z[24];
    z[17]=z[17]*z[25];
    z[13]=z[20] + z[13] + z[22] + n<T>(1,4)*z[17];
    z[13]=z[3]*z[13];
    z[17]=z[9] - 1;
    z[17]=z[1]*z[17];
    z[17]=n<T>(3,2) + z[17];
    z[16]=z[16]*z[2];
    z[16]=z[16] + n<T>(1,2);
    z[17]=z[16]*z[17];
    z[16]=z[16]*z[14];
    z[20]=static_cast<T>(2)- z[2];
    z[20]=z[2]*z[20];
    z[16]=z[16] - static_cast<T>(1)+ z[20];
    z[16]=z[8]*z[16];
    z[16]=z[16] + z[17];
    z[16]=z[4]*z[16];
    z[17]=z[10]*z[2];
    z[11]= - z[17] + z[11];
    z[11]=z[1]*z[11];
    z[20]= - n<T>(1,2) + z[2];
    z[20]=z[2]*z[20];
    z[11]=z[11] - n<T>(1,2) + z[20];
    z[20]=n<T>(1,4)*z[8];
    z[22]=z[20] - 1;
    z[22]=z[22]*z[12];
    z[11]=z[16] + n<T>(1,2)*z[11] + z[22];
    z[11]=z[4]*z[11];
    z[16]= - z[2] + z[18];
    z[16]=z[1]*z[16];
    z[16]=z[16] + n<T>(5,2) + z[17];
    z[17]=n<T>(1,8)*z[8] - static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[17]=z[8]*z[17];
    z[11]=z[11] + n<T>(1,4)*z[16] + z[17];
    z[11]=z[4]*z[11];
    z[16]= - static_cast<T>(1)+ n<T>(3,4)*z[1];
    z[16]=z[1]*z[16];
    z[16]=n<T>(3,2) + z[16];
    z[16]=z[6]*z[16];
    z[15]=z[15] + n<T>(1,4) - 2*z[6];
    z[15]=z[8]*z[15];
    z[11]=z[11] + z[15] + z[16] + n<T>(1,4) - z[1];
    z[11]=z[4]*z[11];
    z[15]=n<T>(3,2) + z[5];
    z[15]=z[5]*z[15];
    z[16]= - static_cast<T>(1)- 7*z[5];
    z[16]=n<T>(1,2)*z[16] + z[1];
    z[16]=z[1]*z[16];
    z[15]=z[16] + n<T>(1,2) + z[15];
    z[15]=z[6]*z[15];
    z[16]= - 11*z[1] + static_cast<T>(5)+ z[23];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[6]*z[15];
    z[16]=5*z[5];
    z[17]=static_cast<T>(1)+ z[16];
    z[22]= - static_cast<T>(1)+ n<T>(3,2)*z[5];
    z[22]=z[5]*z[22];
    z[22]= - n<T>(1,2) + z[22];
    z[22]=z[6]*z[22];
    z[17]=n<T>(1,2)*z[17] + z[22];
    z[17]=z[6]*z[17];
    z[17]= - z[25] + n<T>(1,2) + z[17];
    z[17]=z[8]*z[17];
    z[15]=z[17] + n<T>(1,2) + z[15];
    z[11]=z[13] + n<T>(1,2)*z[15] + z[11];
    z[11]=z[3]*z[11];
    z[13]=z[7] + 3;
    z[15]=z[2]*z[7];
    z[17]= - z[15] + z[13];
    z[17]=z[8]*z[17];
    z[22]= - static_cast<T>(1)+ 3*z[7];
    z[22]=z[2]*z[22];
    z[13]=z[17] - 3*z[13] + z[22];
    z[13]=z[13]*z[14];
    z[14]=static_cast<T>(1)+ z[7];
    z[14]=z[2]*z[14];
    z[14]=z[21] + z[14] - static_cast<T>(5)- z[7];
    z[9]=z[14]*z[9];
    z[14]= - z[1]*z[18];
    z[14]=z[14] - 1;
    z[10]=z[10]*z[14];
    z[10]=z[12] + z[10];
    z[12]=n<T>(1,2)*z[4];
    z[10]=z[10]*z[12];
    z[9]=z[10] + z[13] + z[9] + n<T>(7,2) - z[15] + z[7];
    z[9]=z[9]*z[12];
    z[10]=npow(z[5],2);
    z[12]=static_cast<T>(3)+ z[10];
    z[13]= - static_cast<T>(3)- z[16];
    z[13]=n<T>(1,4)*z[13] + z[1];
    z[13]=z[1]*z[13];
    z[12]=n<T>(1,4)*z[12] + z[13];
    z[12]=z[6]*z[5]*z[12];
    z[10]=static_cast<T>(7)+ z[10];
    z[13]=n<T>(5,2)*z[1] - n<T>(11,4) - z[23];
    z[13]=z[1]*z[13];
    z[10]=z[12] + n<T>(1,2)*z[10] + z[13];
    z[10]=z[6]*z[10];
    z[12]= - n<T>(7,2)*z[1] - z[15] + z[26] + static_cast<T>(1)+ n<T>(3,2)*z[7];
    z[10]=n<T>(1,2)*z[12] + z[10];
    z[12]=z[24]*z[6];
    z[12]=n<T>(3,2)*z[12];
    z[13]=z[12] + 7;
    z[13]=z[5]*z[13];
    z[13]= - n<T>(17,2) + z[13];
    z[13]=z[13]*z[19];
    z[12]= - z[12] - static_cast<T>(1)+ n<T>(1,2)*z[7];
    z[12]=z[12]*z[20];
    z[12]=z[12] + z[13] + static_cast<T>(1)- n<T>(1,4)*z[7];
    z[12]=z[8]*z[12];

    r += z[9] + n<T>(1,2)*z[10] + z[11] + z[12];
 
    return r;
}

template double qqb_2lha_r2377(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2377(const std::array<dd_real,30>&);
#endif
