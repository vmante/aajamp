#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1788(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[10];
    z[6]=z[4] - 1;
    z[7]=2773*z[5];
    z[7]= - z[6]*z[7];
    z[7]=n<T>(13433,2) + z[7];
    z[8]=n<T>(1,36)*z[5];
    z[7]=z[7]*z[8];
    z[9]=z[2] - z[1];
    z[10]=3*z[4];
    z[7]=z[7] + z[10] + n<T>(2413,24) + 9*z[9];
    z[7]=z[2]*z[7];
    z[11]=n<T>(1,18)*z[5];
    z[12]=z[5]*z[4];
    z[13]=2773*z[12];
    z[14]=2557*z[4] + z[13];
    z[14]=z[14]*z[11];
    z[15]=static_cast<T>(2665)+ n<T>(2773,2)*z[5];
    z[11]=z[15]*z[11];
    z[15]=z[9] + z[4];
    z[11]=z[11] + n<T>(2341,36) + 6*z[15];
    z[11]=z[2]*z[11];
    z[11]=z[11] - 12*z[4] + z[14];
    z[11]=z[2]*z[11];
    z[14]=n<T>(2773,36)*z[5];
    z[15]= - static_cast<T>(6)+ z[14];
    z[15]=z[5]*z[15]*npow(z[4],2);
    z[11]=z[15] + z[11];
    z[11]=z[3]*z[11];
    z[6]= - z[6]*z[13];
    z[6]=n<T>(2341,2)*z[4] + z[6];
    z[6]=z[6]*z[8];
    z[6]=z[11] + z[7] - z[10] + z[6];
    z[6]=z[3]*z[6];
    z[7]=n<T>(1,2) - z[12];
    z[7]=z[7]*z[14];
    z[6]=z[6] + z[7] + n<T>(2557,72) + 3*z[9];

    r += z[6]*z[3];
 
    return r;
}

template double qqb_2lha_r1788(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1788(const std::array<dd_real,30>&);
#endif
