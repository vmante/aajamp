#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1713(const std::array<T,30>& k) {
  T z[21];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=5*z[4];
    z[8]=npow(z[5],2);
    z[9]=z[7]*z[8];
    z[10]=5*z[5];
    z[11]= - static_cast<T>(3)- z[10];
    z[11]=2*z[11] - z[9];
    z[12]=2*z[4];
    z[11]=z[11]*z[12];
    z[13]=z[5] + 5;
    z[14]=6*z[4];
    z[15]=z[14] - z[13];
    z[16]=2*z[2];
    z[15]=z[15]*z[16];
    z[11]=z[15] - static_cast<T>(3)+ z[11];
    z[11]=z[3]*z[11];
    z[15]=z[12]*z[5];
    z[17]= - static_cast<T>(3)- z[15];
    z[17]=z[17]*z[14];
    z[13]= - z[13]*z[16];
    z[14]= - static_cast<T>(1)+ z[14];
    z[13]=3*z[14] + z[13];
    z[13]=z[2]*z[13];
    z[11]=z[11] + z[17] + z[13];
    z[11]=z[3]*z[11];
    z[13]=z[12] + 1;
    z[14]=4*z[4];
    z[17]= - z[13]*z[14];
    z[18]= - 6*z[2] + static_cast<T>(1)+ 12*z[4];
    z[18]=z[2]*z[18];
    z[17]=z[17] + z[18];
    z[17]=z[2]*z[17];
    z[18]= - static_cast<T>(4)- z[7];
    z[18]=z[18]*z[12];
    z[19]= - static_cast<T>(1)+ z[12];
    z[19]=z[2]*z[19];
    z[18]=4*z[19] + static_cast<T>(3)+ z[18];
    z[18]=z[2]*z[18];
    z[19]=static_cast<T>(1)+ z[5];
    z[19]=z[4]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[4]*z[19];
    z[18]=10*z[19] + z[18];
    z[18]=z[3]*z[18];
    z[19]=npow(z[4],2);
    z[17]=z[18] + 8*z[19] + z[17];
    z[17]=z[3]*z[17];
    z[18]=3*z[2];
    z[13]= - z[13] + z[18];
    z[13]=z[2]*z[4]*z[13];
    z[20]=2*z[19];
    z[13]=z[20] + z[13];
    z[13]=z[2]*z[13];
    z[7]= - static_cast<T>(2)- z[7];
    z[7]=z[4]*z[7];
    z[12]=z[2]*z[12];
    z[7]=z[7] + z[12];
    z[7]=z[2]*z[7];
    z[7]=10*z[19] + z[7];
    z[7]=z[2]*z[7];
    z[7]= - 5*z[19] + z[7];
    z[7]=z[3]*z[7];
    z[7]=z[13] + z[7];
    z[7]=z[3]*z[7];
    z[12]= - z[2]*z[19];
    z[12]=z[20] + z[12];
    z[12]=z[2]*z[12];
    z[12]= - z[19] + z[12];
    z[12]=z[1]*z[2]*z[12]*npow(z[3],2);
    z[7]=z[7] + z[12];
    z[7]=z[1]*z[7];
    z[12]=z[4] - 1;
    z[13]= - z[18] + z[12];
    z[13]=z[2]*z[13];
    z[13]= - z[19] + z[13];
    z[13]=z[2]*z[13];
    z[7]=z[7] + z[13] + z[17];
    z[7]=z[1]*z[7];
    z[13]=z[5] - 1;
    z[17]=z[6] + 6;
    z[17]= - z[2]*z[17]*z[13];
    z[17]=z[17] - static_cast<T>(11)- z[5];
    z[17]=z[2]*z[17];
    z[12]=3*z[12] + z[17];
    z[12]=z[2]*z[12];
    z[7]=z[7] + z[11] - 3*z[19] + z[12];
    z[7]=z[1]*z[7];
    z[11]=z[13]*z[15];
    z[11]=z[10] + z[11];
    z[11]=z[11]*z[14];
    z[12]= - static_cast<T>(2)- z[5];
    z[12]=z[12]*z[16];
    z[9]=z[13]*z[9];
    z[14]= - static_cast<T>(2)+ 15*z[5];
    z[14]=z[5]*z[14];
    z[9]=z[14] + z[9];
    z[9]=z[4]*z[9];
    z[9]=6*z[5] + z[9];
    z[9]=z[3]*z[9];
    z[9]=z[9] + z[12] + static_cast<T>(5)+ z[11];
    z[9]=z[3]*z[9];
    z[11]=z[5] - 2;
    z[12]= - z[5]*z[6]*z[11];
    z[12]=z[12] - z[6];
    z[14]=z[12]*z[16];
    z[17]=3*z[6];
    z[18]= - static_cast<T>(10)- z[17] + 2*z[5];
    z[18]=z[5]*z[18];
    z[14]=z[14] + z[18] + static_cast<T>(8)+ z[17];
    z[14]=z[2]*z[14];
    z[14]=z[14] - static_cast<T>(10)+ z[5];
    z[14]=z[2]*z[14];
    z[17]=3*z[4];
    z[18]=z[4]*z[13];
    z[18]=static_cast<T>(2)+ z[18];
    z[17]=z[18]*z[17];
    z[7]=z[7] + z[9] + z[14] - static_cast<T>(2)+ z[17];
    z[7]=z[1]*z[7];
    z[9]=z[11]*z[5];
    z[9]=z[9] + 1;
    z[14]= - z[9]*z[15];
    z[15]=z[13]*z[5];
    z[14]= - 7*z[15] + z[14];
    z[14]=z[4]*z[14];
    z[9]=z[9]*z[4];
    z[15]= - 4*z[13] - z[9];
    z[15]=z[4]*z[8]*z[15];
    z[8]= - 3*z[8] + z[15];
    z[8]=z[3]*z[8];
    z[8]=z[8] - z[10] + z[14];
    z[8]=z[3]*z[8];
    z[9]= - 3*z[13] - z[9];
    z[9]=z[4]*z[9];
    z[10]= - z[6] + z[11];
    z[10]=z[5]*z[10];
    z[12]=z[2]*z[12];
    z[10]=z[12] + z[10] + static_cast<T>(1)+ z[6];
    z[10]=z[10]*z[16];
    z[7]=z[7] + z[8] + z[10] + 2*z[11] + z[9];

    r += 2*z[7];
 
    return r;
}

template double qqb_2lha_r1713(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1713(const std::array<dd_real,30>&);
#endif
