#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1596(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[11];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[5];
    z[7]=k[13];
    z[8]=k[6];
    z[9]=k[12];
    z[10]=z[3] - 1;
    z[10]=n<T>(1,2)*z[10];
    z[11]= - n<T>(1,2) + z[9];
    z[11]=z[2]*z[11];
    z[11]=z[10] + z[11];
    z[12]=n<T>(1,2)*z[3];
    z[13]=z[12]*z[1]*z[2];
    z[13]=z[13] + z[3];
    z[14]=z[12] + 1;
    z[15]=z[2]*z[14];
    z[15]=n<T>(1,4)*z[15] - z[13];
    z[15]=z[1]*z[15];
    z[16]=n<T>(1,2)*z[7];
    z[17]=n<T>(1,4)*z[8];
    z[18]= - static_cast<T>(1)+ z[17];
    z[18]=z[3]*z[18];
    z[18]=z[18] - z[16];
    z[19]=n<T>(1,2)*z[5];
    z[18]=z[18]*z[19];
    z[20]=z[5]*z[8]*z[12];
    z[20]=z[20] + z[13];
    z[21]=n<T>(1,2)*z[4];
    z[20]=z[20]*z[21];
    z[22]=z[9]*z[16];
    z[11]=z[20] + z[18] + z[22] + n<T>(1,2)*z[11] + z[15];
    z[11]=z[4]*z[11];
    z[15]=z[5]*z[7];
    z[18]=z[15] + 1;
    z[20]=3*z[7];
    z[22]=z[1]*z[3];
    z[22]=z[20] + z[22] - z[12] - z[18];
    z[19]=z[22]*z[19];
    z[22]=n<T>(1,2)*z[2];
    z[14]= - z[14]*z[22];
    z[13]=z[14] + z[13];
    z[13]=z[1]*z[13];
    z[14]= - n<T>(1,2) - z[3];
    z[14]=z[2]*z[14];
    z[10]=z[13] - z[10] + z[14];
    z[10]=z[1]*z[10];
    z[13]=z[2]*z[3];
    z[10]=z[19] - z[7] + z[10] + n<T>(3,4)*z[13] + static_cast<T>(1)- z[12];
    z[10]=n<T>(1,2)*z[10] + z[11];
    z[11]=static_cast<T>(1)+ n<T>(1,2)*z[9];
    z[11]=z[11]*z[20];
    z[12]= - 3*z[3] - static_cast<T>(3)+ 7*z[9];
    z[12]=z[2]*z[12];
    z[12]= - static_cast<T>(11)+ z[12];
    z[11]=n<T>(1,2)*z[12] + z[11];
    z[12]= - n<T>(17,4)*z[7] + static_cast<T>(3)- z[17];
    z[12]=n<T>(1,2)*z[12] + z[15];
    z[12]=z[5]*z[12];
    z[13]= - n<T>(1,2)*z[8] + z[7];
    z[13]=z[5]*z[13];
    z[13]= - z[16] + z[13];
    z[13]=z[13]*z[21];
    z[11]=z[13] + n<T>(1,4)*z[11] + z[12];
    z[11]=z[4]*z[11];
    z[12]=7*z[7];
    z[13]=3*z[1];
    z[14]=z[13] + static_cast<T>(3)- z[2];
    z[14]=3*z[14] - z[12];
    z[13]=9*z[7] - static_cast<T>(5)- z[13];
    z[16]= - n<T>(15,4)*z[7] + z[18];
    z[16]=z[5]*z[16];
    z[13]=n<T>(1,2)*z[13] + z[16];
    z[13]=z[5]*z[13];
    z[13]=n<T>(1,4)*z[14] + z[13];
    z[11]=n<T>(1,2)*z[13] + z[11];
    z[13]= - z[8] + 13*z[7];
    z[13]=n<T>(1,2)*z[13] - 3*z[15];
    z[13]=z[5]*z[13];
    z[14]= - z[5] + 1;
    z[16]=z[7] + z[8];
    z[14]=z[4]*z[16]*z[14];
    z[12]=z[8] - z[12];
    z[12]=z[14] + n<T>(1,2)*z[12] + z[13];
    z[12]=z[4]*z[12];
    z[13]=n<T>(1,4)*z[5];
    z[14]= - n<T>(3,2)*z[15] - n<T>(3,2) + 5*z[7];
    z[14]=z[14]*z[13];
    z[15]= - static_cast<T>(1)+ n<T>(11,8)*z[7];
    z[14]=z[14] - z[15];
    z[14]=z[5]*z[14];
    z[16]=z[9]*z[22];
    z[16]= - static_cast<T>(3)+ z[16];
    z[16]=n<T>(1,2)*z[16] + z[7];
    z[12]=n<T>(1,8)*z[12] + n<T>(1,2)*z[16] + z[14];
    z[12]=z[4]*z[12];
    z[14]=n<T>(9,2)*z[7] - z[18];
    z[14]=z[5]*z[14];
    z[14]=z[14] - n<T>(15,2)*z[7] + n<T>(7,2) + z[1];
    z[13]=z[14]*z[13];
    z[13]=z[13] - n<T>(3,4)*z[1] + z[15];
    z[13]=z[5]*z[13];
    z[14]=static_cast<T>(3)+ z[22];
    z[14]=z[1]*z[14];
    z[14]= - n<T>(3,2)*z[7] + n<T>(3,2) + z[14];
    z[13]=n<T>(1,4)*z[14] + z[13];
    z[14]=z[9] - 1;
    z[15]= - z[2]*z[14];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[4]*z[15];
    z[14]=z[15] + z[1] - z[14];
    z[14]=z[6]*z[14];
    z[12]=n<T>(1,16)*z[14] + n<T>(1,2)*z[13] + z[12];
    z[12]=z[6]*z[12];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[6]*z[11];

    r += n<T>(1,2)*z[10] + z[11];
 
    return r;
}

template double qqb_2lha_r1596(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1596(const std::array<dd_real,30>&);
#endif
