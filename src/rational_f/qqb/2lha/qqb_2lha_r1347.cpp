#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1347(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[2];
    z[7]=k[11];
    z[8]=k[5];
    z[9]=n<T>(1,2)*z[5];
    z[10]=z[9] - 1;
    z[11]=3*z[8];
    z[12]=z[10]*z[11];
    z[13]=9*z[8];
    z[14]=3*z[1];
    z[15]=z[13] + static_cast<T>(11)+ z[14];
    z[16]=n<T>(3,2)*z[8];
    z[17]= - z[16] - n<T>(5,2) - z[1];
    z[17]=z[3]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[3]*z[15];
    z[17]=n<T>(1,2)*z[1];
    z[9]=z[15] + z[12] - z[17] - static_cast<T>(3)+ z[9];
    z[15]=static_cast<T>(3)- z[3];
    z[18]=z[11] - z[7];
    z[19]=n<T>(1,2)*z[3];
    z[15]=z[19]*z[18]*z[15];
    z[20]= - z[7]*z[10];
    z[15]=z[15] + z[12] + z[20];
    z[20]=n<T>(1,4)*z[4];
    z[15]=z[15]*z[20];
    z[21]= - n<T>(1,2) - z[8];
    z[22]=static_cast<T>(1)+ z[16];
    z[22]=z[3]*z[22];
    z[21]=n<T>(9,2)*z[21] + z[22];
    z[21]=z[3]*z[21];
    z[22]=static_cast<T>(5)- z[5];
    z[15]=z[15] + z[21] + n<T>(1,4)*z[22] - z[12];
    z[15]=z[4]*z[15];
    z[9]=n<T>(3,2)*z[9] + z[15];
    z[9]=z[9]*z[20];
    z[15]=static_cast<T>(19)- 3*z[5];
    z[15]=n<T>(1,2)*z[15] + z[14];
    z[15]=n<T>(1,2)*z[15] - z[12];
    z[21]=n<T>(9,2)*z[8];
    z[22]= - static_cast<T>(5)- n<T>(3,4)*z[1];
    z[22]=z[1]*z[22];
    z[22]= - z[21] - n<T>(35,4) + z[22];
    z[23]=z[1] + n<T>(7,2);
    z[24]=n<T>(1,4)*z[1];
    z[25]=z[23]*z[24];
    z[26]=n<T>(3,8)*z[8];
    z[25]=z[25] + z[26] + 1;
    z[27]=z[3]*z[25];
    z[22]=n<T>(1,4)*z[22] + z[27];
    z[22]=z[3]*z[22];
    z[9]=z[9] + n<T>(1,4)*z[15] + z[22];
    z[9]=z[4]*z[9];
    z[15]= - n<T>(9,2) - z[1];
    z[15]=z[1]*z[15];
    z[15]= - n<T>(15,2) + z[15];
    z[15]=z[1]*z[15];
    z[15]= - z[16] - n<T>(11,2) + z[15];
    z[15]=z[3]*z[15];
    z[15]=3*z[25] + n<T>(1,4)*z[15];
    z[15]=z[3]*z[15];
    z[14]=z[14] - z[5];
    z[12]=z[12] - n<T>(13,2) - z[14];
    z[12]=n<T>(1,4)*z[12] + z[15];
    z[9]=n<T>(1,4)*z[12] + z[9];
    z[9]=z[2]*z[9];
    z[12]=z[5]*z[7];
    z[15]=z[8]*z[5];
    z[15]=z[12] - z[15];
    z[22]=n<T>(21,2)*z[8];
    z[25]=z[22] + static_cast<T>(1)- n<T>(5,2)*z[7];
    z[27]=n<T>(9,4)*z[8];
    z[28]= - z[27] - static_cast<T>(1)+ n<T>(1,4)*z[7];
    z[28]=z[3]*z[28];
    z[25]=n<T>(1,2)*z[25] + z[28];
    z[25]=z[3]*z[25];
    z[18]= - z[18]*z[20];
    z[28]=z[7] + 1;
    z[15]=z[18] + z[25] + z[28] - n<T>(3,4)*z[15];
    z[15]=z[4]*z[15];
    z[18]= - z[21] - 1;
    z[18]=z[10]*z[18];
    z[21]=3*z[3];
    z[25]=z[27] + static_cast<T>(3)+ z[1];
    z[25]=z[25]*z[21];
    z[25]=z[25] - n<T>(63,4)*z[8] - n<T>(23,2) - z[1];
    z[25]=z[3]*z[25];
    z[15]=z[15] + z[25] - z[17] + z[18];
    z[15]=z[15]*z[20];
    z[17]= - static_cast<T>(1)+ n<T>(3,8)*z[5];
    z[17]=z[17]*z[11];
    z[18]= - n<T>(3,2)*z[1] - n<T>(29,4) + z[5];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[18]=z[24] + 1;
    z[25]= - z[1]*z[18];
    z[25]= - n<T>(9,16)*z[8] - n<T>(21,16) + z[25];
    z[21]=z[25]*z[21];
    z[24]=static_cast<T>(5)+ z[24];
    z[24]=z[1]*z[24];
    z[24]=n<T>(63,8)*z[8] + n<T>(49,4) + z[24];
    z[21]=n<T>(1,2)*z[24] + z[21];
    z[21]=z[3]*z[21];
    z[15]=z[15] + n<T>(1,2)*z[17] + z[21];
    z[15]=z[4]*z[15];
    z[17]= - n<T>(39,2) - 5*z[1];
    z[17]=z[1]*z[17];
    z[17]= - z[22] - static_cast<T>(25)+ z[17];
    z[21]=static_cast<T>(5)+ z[1];
    z[21]=z[1]*z[21];
    z[21]=n<T>(37,4) + z[21];
    z[21]=z[1]*z[21];
    z[21]=z[27] + n<T>(15,2) + z[21];
    z[21]=z[3]*z[21];
    z[17]=n<T>(1,2)*z[17] + z[21];
    z[17]=z[17]*z[19];
    z[21]=z[5] - 3;
    z[22]= - z[21]*z[26];
    z[24]=static_cast<T>(9)- z[5];
    z[17]=z[17] + z[22] + n<T>(1,4)*z[24] + z[1];
    z[15]=n<T>(1,2)*z[17] + z[15];
    z[9]=n<T>(1,2)*z[15] + z[9];
    z[9]=z[2]*z[9];
    z[15]=z[5]*z[16];
    z[15]=z[15] + 3*z[28] - n<T>(7,2)*z[12];
    z[17]=n<T>(21,4)*z[8];
    z[22]= - static_cast<T>(9)+ n<T>(1,2)*z[7];
    z[22]= - z[17] + n<T>(1,2)*z[22] - z[1];
    z[22]=z[22]*z[19];
    z[24]=n<T>(39,8)*z[8];
    z[22]=z[22] + z[24] + static_cast<T>(1)- n<T>(7,8)*z[7];
    z[22]=z[3]*z[22];
    z[25]=z[7] - z[8];
    z[25]=z[4]*z[25];
    z[15]=n<T>(3,4)*z[25] + n<T>(1,4)*z[15] + z[22];
    z[15]=z[4]*z[15];
    z[16]= - z[21]*z[16];
    z[10]=z[16] - z[1] - z[10];
    z[16]=z[1] + n<T>(11,2);
    z[16]=z[16]*z[1];
    z[22]=z[17] + n<T>(19,2) + z[16];
    z[22]=z[22]*z[19];
    z[22]=z[22] - z[24] - n<T>(19,4) - z[1];
    z[22]=z[3]*z[22];
    z[10]=n<T>(1,2)*z[15] + n<T>(1,4)*z[10] + z[22];
    z[10]=z[4]*z[10];
    z[15]= - n<T>(13,2) - z[1];
    z[15]=z[1]*z[15];
    z[15]= - static_cast<T>(15)+ z[15];
    z[15]=z[1]*z[15];
    z[15]= - z[17] - n<T>(59,4) + z[15];
    z[15]=z[15]*z[19];
    z[15]=z[15] + z[24] + n<T>(37,4) + z[16];
    z[15]=z[3]*z[15];
    z[16]= - static_cast<T>(1)+ n<T>(1,4)*z[5];
    z[16]=z[16]*z[11];
    z[14]=z[14] + n<T>(17,2);
    z[14]=z[16] - n<T>(1,2)*z[14];
    z[15]=n<T>(1,2)*z[14] + z[15];
    z[10]=n<T>(1,2)*z[15] + z[10];
    z[9]=n<T>(1,4)*z[10] + z[9];
    z[9]=z[2]*z[9];
    z[10]= - z[11] - z[23];
    z[10]=z[3]*z[10];
    z[13]=z[13] + static_cast<T>(3)- z[7];
    z[10]=n<T>(1,2)*z[13] + z[10];
    z[10]=z[3]*z[10];
    z[11]=7*z[7] - z[11];
    z[11]=z[11]*z[20];
    z[13]=n<T>(1,2) + z[6];
    z[13]=z[7]*z[13];
    z[10]=z[11] + z[10] - z[12] + n<T>(1,2) + z[13];
    z[10]=z[10]*z[20];
    z[11]=static_cast<T>(21)- 5*z[5];
    z[13]= - n<T>(1,8)*z[5] + z[18];
    z[13]=z[1]*z[13];
    z[15]=z[8]*z[21];
    z[11]= - n<T>(3,16)*z[15] + n<T>(1,16)*z[11] + z[13];
    z[11]=z[3]*z[11];
    z[11]=n<T>(1,4)*z[14] + z[11];
    z[11]=z[3]*z[11];
    z[12]= - z[12] - 1;
    z[12]=z[6]*z[12];
    z[13]=z[7]*npow(z[6],2);
    z[12]=z[13] + z[12];
    z[12]=n<T>(1,2)*z[12] - z[1];
    z[10]=z[10] + n<T>(1,8)*z[12] + z[11];

    r += z[9] + n<T>(1,4)*z[10];
 
    return r;
}

template double qqb_2lha_r1347(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1347(const std::array<dd_real,30>&);
#endif
