#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r671(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[20];
    z[5]=k[4];
    z[6]=k[3];
    z[7]=k[5];
    z[8]=k[9];
    z[9]=k[21];
    z[10]=k[28];
    z[11]=k[18];
    z[12]=k[15];
    z[13]=npow(z[8],2);
    z[14]=z[13]*z[5];
    z[15]= - static_cast<T>(3)+ 7*z[8];
    z[16]=z[8]*z[15];
    z[16]=z[14] + static_cast<T>(1)+ z[16];
    z[17]=n<T>(1,4)*z[5];
    z[16]=z[16]*z[17];
    z[18]=z[2]*z[3];
    z[19]= - z[18] + 3*z[3];
    z[20]=n<T>(1,2)*z[8];
    z[21]=z[20] - 1;
    z[21]=z[21]*z[8];
    z[22]=z[21] + n<T>(1,2);
    z[19]=z[22]*z[19];
    z[23]=z[8] - n<T>(1,2);
    z[24]=z[23]*z[8];
    z[19]= - static_cast<T>(1)- z[24] + z[19];
    z[25]=n<T>(1,2)*z[2];
    z[19]=z[19]*z[25];
    z[26]=z[22]*z[3];
    z[27]= - static_cast<T>(5)+ 3*z[8];
    z[27]=z[8]*z[27];
    z[16]=z[19] - n<T>(3,2)*z[26] + z[16] + static_cast<T>(1)+ n<T>(3,4)*z[27];
    z[16]=z[16]*z[25];
    z[19]=static_cast<T>(7)- 5*z[8];
    z[19]=z[19]*z[20];
    z[19]=z[26] - static_cast<T>(1)+ z[19];
    z[15]= - z[15]*z[20];
    z[15]=z[15] - z[14];
    z[15]=z[15]*z[17];
    z[26]=static_cast<T>(1)- n<T>(9,8)*z[8];
    z[26]=z[8]*z[26];
    z[15]=z[15] - n<T>(1,8) + z[26];
    z[15]=z[5]*z[15];
    z[15]=z[16] + z[15] + n<T>(1,4)*z[19];
    z[15]=z[10]*z[15];
    z[16]=z[8] - 1;
    z[19]=z[16]*z[20];
    z[26]=static_cast<T>(1)+ z[19];
    z[27]=n<T>(5,2) - z[8];
    z[27]=z[8]*z[27];
    z[27]= - n<T>(3,4) + z[27];
    z[27]=z[27]*z[25];
    z[28]=n<T>(1,4)*z[8];
    z[29]=z[28] - 1;
    z[30]=z[29]*z[8];
    z[31]=n<T>(3,8) + z[30];
    z[31]=z[5]*z[31];
    z[26]=z[27] + n<T>(1,4)*z[26] + z[31];
    z[26]=z[2]*z[26];
    z[27]=z[8] - 5;
    z[31]=z[27]*z[28];
    z[32]=z[22]*z[25];
    z[31]=z[32] - n<T>(3,4)*z[5] + static_cast<T>(1)+ z[31];
    z[31]=z[2]*z[31];
    z[32]= - static_cast<T>(1)+ n<T>(3,2)*z[5];
    z[32]=z[5]*z[32];
    z[32]=z[32] - z[16];
    z[31]=n<T>(1,2)*z[32] + z[31];
    z[31]=z[2]*z[31];
    z[32]=npow(z[5],3);
    z[31]= - n<T>(1,4)*z[32] + z[31];
    z[31]=z[10]*z[31];
    z[23]=z[5]*z[23];
    z[23]=n<T>(1,2)*z[16] + z[23];
    z[23]=z[23]*z[17];
    z[23]=n<T>(1,2)*z[31] + z[23] + z[26];
    z[23]=z[7]*z[23];
    z[15]=z[15] + z[23];
    z[23]=z[5] + n<T>(5,2);
    z[26]=n<T>(1,3)*z[5];
    z[31]=n<T>(1,2) + z[26];
    z[31]=z[3]*z[31];
    z[31]=n<T>(1,6)*z[23] + z[31];
    z[31]=z[3]*z[31];
    z[33]= - static_cast<T>(1)- z[3];
    z[33]=z[33]*z[18];
    z[31]=z[31] + n<T>(1,6)*z[33];
    z[31]=z[31]*z[25];
    z[33]= - static_cast<T>(1)- z[26];
    z[34]= - n<T>(1,4) - z[26];
    z[34]=z[3]*z[34];
    z[33]=n<T>(1,4)*z[33] + z[34];
    z[33]=z[3]*z[33];
    z[34]=n<T>(1,2) + z[5];
    z[34]=z[3]*z[34];
    z[34]=z[34] + n<T>(5,4) + z[5];
    z[34]=z[3]*z[34];
    z[23]=n<T>(1,2)*z[23] + z[34];
    z[23]=z[4]*z[23];
    z[34]=n<T>(1,3) + z[3];
    z[34]=z[3]*z[34];
    z[35]= - static_cast<T>(1)+ n<T>(1,3)*z[2];
    z[35]=z[2]*z[35]*npow(z[3],2);
    z[34]=z[34] + z[35];
    z[35]= - static_cast<T>(1)- n<T>(1,2)*z[3];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(1,2) + z[35];
    z[35]=z[4]*z[35];
    z[34]=n<T>(1,2)*z[34] + n<T>(1,3)*z[35];
    z[34]=z[1]*z[34];
    z[23]=n<T>(1,4)*z[34] + n<T>(1,6)*z[23] + z[31] - n<T>(1,12) + z[33];
    z[23]=z[1]*z[23];
    z[31]=z[16]*z[28];
    z[33]=n<T>(1,2)*z[5];
    z[34]=z[33]*z[13];
    z[35]= - static_cast<T>(1)+ n<T>(5,4)*z[8];
    z[35]=z[8]*z[35];
    z[35]=z[35] + z[34];
    z[35]=z[35]*z[26];
    z[31]=z[31] + z[35];
    z[31]=z[5]*z[31];
    z[35]=n<T>(1,6)*z[8];
    z[36]=static_cast<T>(1)- z[35];
    z[36]=z[8]*z[36];
    z[36]= - n<T>(3,2) + z[36];
    z[31]=n<T>(1,2)*z[36] + z[31];
    z[31]=z[5]*z[31];
    z[35]= - z[27]*z[35];
    z[35]= - static_cast<T>(1)+ z[35];
    z[31]=n<T>(1,2)*z[35] + z[31];
    z[35]=n<T>(1,3)*z[8];
    z[36]=z[35] - 1;
    z[37]=z[36]*z[20];
    z[38]=z[37] + n<T>(1,3);
    z[17]=z[17]*z[13];
    z[16]=z[16]*z[8];
    z[39]= - z[16] - z[17];
    z[39]=z[39]*z[26];
    z[39]=z[39] - z[22];
    z[39]=z[39]*z[33];
    z[39]=z[39] - z[38];
    z[39]=z[5]*z[39];
    z[29]=z[29]*z[35];
    z[29]=z[29] + n<T>(1,4);
    z[39]= - n<T>(1,2)*z[29] + z[39];
    z[39]=z[3]*z[39];
    z[31]=n<T>(1,2)*z[31] + z[39];
    z[31]=z[3]*z[31];
    z[39]= - n<T>(1,3) + z[8];
    z[39]=z[39]*z[20];
    z[40]=z[8] - z[34];
    z[40]=z[40]*z[26];
    z[39]=z[40] - n<T>(1,3) + z[39];
    z[39]=z[39]*z[33];
    z[40]= - static_cast<T>(1)+ z[19];
    z[39]=n<T>(1,3)*z[40] + z[39];
    z[39]=z[5]*z[39];
    z[27]=n<T>(1,12)*z[27] + z[39];
    z[27]=n<T>(1,2)*z[27] + z[31];
    z[27]=z[4]*z[27];
    z[23]=z[27] + z[23];
    z[27]=static_cast<T>(59)- 47*z[8];
    z[27]=z[27]*z[35];
    z[27]= - static_cast<T>(17)+ z[27];
    z[31]=n<T>(19,2) - 10*z[8];
    z[31]=z[31]*z[35];
    z[31]= - n<T>(13,4) + z[31];
    z[31]=z[5]*z[31];
    z[38]= - z[5]*z[38];
    z[30]=z[30] + n<T>(3,4);
    z[30]=n<T>(1,2)*z[30];
    z[38]= - z[30] + z[38];
    z[39]=13*z[3];
    z[38]=z[38]*z[39];
    z[27]=z[38] + n<T>(1,4)*z[27] + z[31];
    z[27]=z[3]*z[27];
    z[31]= - static_cast<T>(23)+ n<T>(67,4)*z[8];
    z[31]=z[31]*z[35];
    z[29]=z[29]*z[39];
    z[29]=z[29] + n<T>(17,4) + z[31];
    z[29]=z[3]*z[29];
    z[29]=n<T>(13,12) + z[29];
    z[25]=z[29]*z[25];
    z[29]= - n<T>(11,3) - z[37];
    z[31]= - n<T>(3,4) + z[35];
    z[31]=z[8]*z[31];
    z[31]= - n<T>(13,12) + z[31];
    z[31]=z[5]*z[31];
    z[25]=z[25] + z[27] + n<T>(1,4)*z[29] + z[31];
    z[25]=z[2]*z[25];
    z[27]=static_cast<T>(1)+ z[8];
    z[27]=z[8]*z[27];
    z[27]=z[27] - z[14];
    z[27]=z[27]*z[33];
    z[18]=z[22]*z[18];
    z[18]=z[18] + n<T>(1,2) - z[24];
    z[18]=z[2]*z[18];
    z[18]=z[18] + z[16] + z[27];
    z[18]=z[2]*z[18];
    z[27]=z[16] + z[34];
    z[27]=z[5]*z[27];
    z[27]=z[27] + z[22];
    z[27]=z[5]*z[27];
    z[29]= - z[2] + 1;
    z[29]=z[7]*npow(z[2],2)*z[22]*z[29];
    z[18]=z[29] + z[27] + z[18];
    z[18]=z[9]*z[18];
    z[27]=n<T>(5,3) - z[8];
    z[20]=z[27]*z[20];
    z[20]= - n<T>(1,3) + z[20];
    z[17]= - z[24] - z[17];
    z[17]=z[17]*z[26];
    z[17]=n<T>(1,2)*z[20] + z[17];
    z[20]=npow(z[5],2);
    z[17]=z[17]*z[20];
    z[24]=z[24] + z[34];
    z[24]=z[5]*z[24];
    z[19]=z[19] + z[24];
    z[19]=z[6]*z[5]*z[19];
    z[24]= - n<T>(1,3) + z[28];
    z[24]=z[8]*z[24];
    z[14]=z[24] + n<T>(1,6)*z[14];
    z[14]=z[14]*z[32];
    z[13]=z[3]*z[13]*npow(z[5],4);
    z[13]=z[14] - n<T>(1,12)*z[13];
    z[13]=z[3]*z[13];
    z[13]=n<T>(1,6)*z[19] + z[17] + z[13];
    z[13]=z[12]*z[13];
    z[14]=n<T>(3,2)*z[8];
    z[17]= - n<T>(5,3) + z[14];
    z[17]=z[8]*z[17];
    z[17]=n<T>(13,4) + z[17];
    z[17]=z[17]*z[33];
    z[19]=n<T>(1,4) + 4*z[8];
    z[19]=z[8]*z[19];
    z[19]=n<T>(91,8) + z[19];
    z[17]=n<T>(1,3)*z[19] + z[17];
    z[17]=z[5]*z[17];
    z[19]=z[22]*z[33];
    z[22]=z[36]*z[8];
    z[19]=z[19] + n<T>(2,3) + z[22];
    z[19]=z[5]*z[19];
    z[19]=z[30] + z[19];
    z[19]=z[19]*z[39];
    z[14]= - static_cast<T>(2)+ z[14];
    z[14]=z[8]*z[14];
    z[14]=z[19] + z[17] + n<T>(15,4) + z[14];
    z[14]=z[3]*z[14];
    z[17]= - static_cast<T>(2)+ n<T>(17,8)*z[8];
    z[17]=z[17]*z[35];
    z[19]=static_cast<T>(1)+ n<T>(13,6)*z[8];
    z[19]=z[5]*z[19]*z[28];
    z[17]=z[17] + z[19];
    z[17]=z[5]*z[17];
    z[19]= - z[21] - z[34];
    z[19]=z[19]*z[26];
    z[19]=n<T>(1,2)*z[36] + z[19];
    z[19]=z[5]*z[19];
    z[19]= - n<T>(1,2) + z[19];
    z[19]= - n<T>(13,24)*z[6] + n<T>(13,12)*z[1] + n<T>(13,4)*z[19];
    z[19]=z[4]*z[19];
    z[16]=n<T>(13,4) + z[16];
    z[16]=n<T>(1,6)*z[16] + z[17] + z[19];
    z[16]=z[6]*z[16];
    z[17]= - static_cast<T>(1)- z[5];
    z[17]=z[6]*z[17]*z[33];
    z[19]= - z[3]*z[20];
    z[19]=z[5] + z[19];
    z[20]= - z[2]*z[5];
    z[17]=z[17] + n<T>(1,2)*z[19] + z[20];
    z[17]=z[11]*z[17];
    z[19]=static_cast<T>(29)- n<T>(37,2)*z[8];
    z[19]=z[8]*z[19];
    z[19]=n<T>(13,2) + z[19];
    z[20]=static_cast<T>(3)- n<T>(19,3)*z[8];
    z[20]=z[8]*z[20];
    z[20]=n<T>(13,6) + z[20];
    z[20]=z[5]*z[20];
    z[19]=n<T>(1,3)*z[19] + z[20];
    z[19]=z[5]*z[19];
    z[20]=n<T>(35,3) + z[22];
    z[19]=n<T>(1,2)*z[20] + z[19];

    r += n<T>(13,2)*z[13] + z[14] + 3*z[15] + z[16] + n<T>(1,2)*z[17] + n<T>(3,4)*
      z[18] + n<T>(1,4)*z[19] + 13*z[23] + z[25];
 
    return r;
}

template double qqb_2lha_r671(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r671(const std::array<dd_real,30>&);
#endif
